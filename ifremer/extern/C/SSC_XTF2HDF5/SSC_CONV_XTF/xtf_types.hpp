#ifndef XTF_TYPES_HPP
#define XTF_TYPES_HPP

#include <map>

using BYTE = unsigned char;
using WORD = unsigned short;
using DWORD = unsigned int;
using UINT = unsigned int;
using LONG = signed int;

// Some compile-time verifications
static_assert (sizeof(BYTE) == 1, "Invalid length for BYTE type");
static_assert (sizeof(WORD) == 2, "Invalid length for WORD type");
static_assert (sizeof(DWORD) == 4, "Invalid length for DWORD type");
static_assert (sizeof(LONG) == 4, "Invalid length for LONG type");

/*****************************************************************************
 *                                                                           *
 *                                                                           *
 *                     Define datatypes found in XTF files                   *
 *                                                                           *
 *              This mostly is a verbatim copy of the documentation          *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/
#pragma pack(1)

static constexpr BYTE CHANNEL_TYPE_SUBBOTTOM = 0;
static constexpr BYTE CHANNEL_TYPE_PORT = 1;
static constexpr BYTE CHANNEL_TYPE_STBD = 2;
static constexpr BYTE CHANNEL_TYPE_BATHY = 3;

/** \brief Chan info
 */
struct CHANINFO {
   BYTE TypeOfChannel;     // PORT, STBD, SBOT or BATH
   BYTE SubChannelNumber;
   WORD CorrectionFlags;   // 1=raw, 2=Corrected
   WORD UniPolar;          // 0=data is bipolar, 1=data is unipolar
   WORD BytesPerSample;    // 1 or 2
   // Release 25 : DWORD SamplesPerChannel;// Usually a multiple of 1024 unless bathymetry
   DWORD Reserved;
   char ChannelName[16];   // Text describing channel.  i.e., "Port 500"

   float VoltScale;        // How many volts is represented by max sample value.  Typically 5.0.
   float Frequency;        // Center transmit frequency
   float HorizBeamAngle;   // Typically 1 degree or so
   float TiltAngle;        // Typically 30 degrees
   float BeamWidth;        // 3dB beam width, Typically 50 degrees

                           // Orientation of these offsets:
                           // Positive Y is forward
                           // Positive X is to starboard
                           // Positive Z is down.  Just like depth.
                           // Positive roll is lean to starboard
                           // Positive pitch is nose up
                           // Positive yaw is turn to right

   float OffsetX;          // These offsets are entered in the
   float OffsetY;          // Multibeam setup dialog box.
   float OffsetZ;

   float OffsetYaw;        // If the multibeam sensor is reverse
                           // mounted (facing backwards), then
                           // OffsetYaw will be around 180 degrees.
   float OffsetPitch;
   float OffsetRoll;
   // Deb Release 25
   WORD BeamsPerArray;
   BYTE SamplePerFormat;
   //char ReservedArea2[56];
   char ReservedArea2[53];
   // Fin Release 25

};
static_assert (sizeof(CHANINFO) == 128, "Invalid size for type CHANINFO");

struct XTFFILEHEADER {
   BYTE FileFormat;        // 50 for Q-MIPS file format, 51 for Isis format
   BYTE SystemType;        // Type of system used to record this file.  202=Isis
   char RecordingProgramName[8];    // Example: "Isis"
   char RecordingProgramVersion[8]; // Example: "1.72"
   char SonarName[16];     // Name of server used to access sonar.  Example: "C31_SERV.EXE"
   WORD SonarType;         // K2000=5, DF1000=7, SEABAT=8
   char NoteString[64];    // Notes as entered in the Sonar Setup dialog box
   char ThisFileName[64];  // Name of this file. Example: "LINE12-B.SNR"

   WORD NavUnits;          // 0=METERS or 3=DEGREES

   WORD NumberOfSonarChannels;  // if > 60, header goes to 8K in size
   WORD NumberOfBathymetryChannels;
   // Deb Release 25
   BYTE NumberOfSnippetsChannels;
   BYTE NumBerOfForwardLookArrays;
   WORD NumberOfEchoStrengthChannels;
   BYTE NumberOfInterferometryChannels;
   BYTE Reserved1;
   WORD Reserved2;
   float ReferencePointHeight;

//   WORD Reserved1;
//   WORD Reserved2;
//   WORD Reserved3;
//   WORD Reserved4;
//   WORD Reserved5;
//   WORD Reserved6;
   // Fin Release 25
   // nav system parameters
   ///////////////////////////
   BYTE     ProjectionType[12];       // Not currently used
   BYTE     SpheroidType[10];         // Not currently used
   LONG     NavigationLatency;        // milliseconds, latency of nav system (usually GPS)
                                      // This value is entered on the
                                      // Serial port setup dialog box.
                                      // When computing a position, Isis will
                                      // take the time of the navigation
                                      // and subtract this value.

   float    OriginY;                  // Not currently used
   float    OriginX;                  // Not currently used

                                      // Orientation of these offsets:
                                      // Positive Y is forward
                                      // Positive X is to starboard
                                      // Positive Z is down.  Just like depth.
                                      // Positive roll is lean to starboard
                                      // Positive pitch is nose up
                                      // Positive yaw is turn to right

   float    NavOffsetY;               // These offsets are entered in
   float    NavOffsetX;               // the multibeam setup dialog box.
   float    NavOffsetZ;
   float    NavOffsetYaw;

   float    MRUOffsetY;               // These offsets are entered in
   float    MRUOffsetX;               // the multibeam setup dialog box
   float    MRUOffsetZ;

   float    MRUOffsetYaw;
   float    MRUOffsetPitch;
   float    MRUOffsetRoll;

                           // note: even 128-byte boundary to here

   CHANINFO ChanInfo[6];  // Each CHANINFO struct is 128 bytes.
                          // If more than 6 channels needed, header record
                          // grows 1K in size for each additional 8 channels.
};
static_assert (sizeof(XTFFILEHEADER) == 1024, "Invalid size for XTFFILEHEADER type");

static const std::map<WORD, std::string> sonar_types = {
    {0, "NONE"},
    {1, "JAMSTEC, Jamstec chirp 2-channel subbottom"},
    {2, "ANALOG_C31, PC31 8-channel"},
    {3, "SIS1000, Chirp SIS-1000 sonar"},
    {4, "ANALOG_32CHAN, Spectrum with 32-channel DSPlink card"},
    {5, "KLEIN2000, Klein system 2000 with digital interface"},
    {6, "RWS, Standard PC31 analog with special nav code"},
    {7, "DF1000, EG&G DF1000 digital interface"},
    {8, "SEABAT, Reson SEABAT 900x analog/serial"},
    {9, "KLEIN595, 4-chan Klein 595, same as ANALOG_C31"},
    {10, "EGG260, 2-channel EGG260, same as ANALOG_C31"},
    {11, "SONATECH_DDS, Sonatech Diver Detection System on Spectrum DSP32C"},
    {12, "ECHOSCAN, Odom EchoScanII multibeam (with simultaneous analog sidescan)"},
    {13, "ELAC, Elac multibeam system"},
    {14, "KLEIN5000, Klein system 5000 with digital interface"},
    {15, "Reson Seabat 8101"},
    {16, "Imagenex model 858"},
    {17, "USN SILOS with 3-channel analog"},
    {18, "Sonatech Super-high res sidescan sonar"},
    {19, "Delph AU32 Analog input (2 channel)"},
    {20, "Generic sonar using the memory-mapped file interface"},
    {21, "Simrad SM2000 Multibeam Echo Sounder"},
    {22, "Standard multimedia audio"},
    {23, "Edgetech (EG&G) ACI card for 260 sonar through PC31 card"},
    {24, "Edgetech Black Box"},
    {25, "Fugro deeptow"},
    {26, "C&C's Edgetech Chirp conversion program"},
    {27, "DTI SAS Synthetic Aperture processor (memmap file)"},
    {28, "Fugro's Osiris AUV Sidescan data"},
    {29, "Fugro's Osiris AUV Multibeam data"},
    {30, "Geoacoustics SLS"},
    {31, "Simrad EM2000/EM3000"},
    {32, "Klein system 3000.33 = SHRSSS Chirp system"},
    {34, "Benthos C3D SARA/CAATI"},
    {35, "Edgetech MP-X"},
    {36, "CMAX"},
    {37, "Benthos sis1624"},
    {38, "Edgetech 4200"},
    {39, "Benthos SIS1500"},
    {40, "Benthos SIS1502"},
    {41, "Benthos SIS3000"},
    {42, "Benthos SIS7000"},
    {43, "DF1000 DCU"},
    {44, "NONE_SIDESCAN"},
    {45, "NONE_MULTIBEAM"},
    {46, "Reson 7125"},
    {47, "CODA Echoscope"},
    {48, "Kongsberg SAS"},
    {49, "QINSy"},
    {50, "GeoAcoustics DSSS"},
    {51, "CMAX_USB"},
    {52, "SwathPlus Bathy"},
    {53, "R2Sonic QINSy"},
    {55, "R2Sonic Triton"},
    {54, "Converted SwathPlus Bathy"},
    {56, "Edgetech 4600"},
    {57, "Klein 3500"},
    {58, "Klein 5900"}
};

struct XTFCHUNKPRELUDE {
    WORD magic_number;
    BYTE header_type;
};
static_assert (sizeof(XTFCHUNKPRELUDE) == 3, "Invalid size for XTFCHUNKPRELUDE type");

struct XTFUNKNOWNHEADER {
    // Field arleady decoded in XTFCHUNKPRELUDE
    // WORD magic;
    // BYTE header_type
    BYTE padb;
    WORD pad[3];
    DWORD rec_size;
};
static_assert(sizeof(XTFUNKNOWNHEADER) == 11, "Invalid size for UnknownHeader type");

struct XTFEXTENTEDCHUNKPRELUDE {
    WORD magic_number;
    BYTE header_type;
    BYTE padb;
    WORD pad[3];
    DWORD rec_size;
};
static_assert (sizeof(XTFEXTENTEDCHUNKPRELUDE) == 14, "Invalid size for XTFEXTENTEDCHUNKPRELUDE type");

struct XTFATTITUDEDATA {
    WORD MagicNumber;
    BYTE HeaderType;
    BYTE SubChannelNumber;
    WORD NumChansToFollow;
    WORD Reserved1[2];
    DWORD NumBytesThisRecord;
    DWORD Reserved2[2];
    DWORD EpochMicroseconds;
    DWORD SourceEpoch;
    float Pitch;
    float Roll;
    float Heave;
    float Yaw;
    DWORD TimeTag;
    float Heading;
    WORD Year;
    BYTE Month;
    BYTE Day;
    BYTE Hour;
    BYTE Minutes;
    BYTE Seconds;
    WORD Milliseconds;
    BYTE Reserved3[1];
};
static_assert (sizeof(XTFATTITUDEDATA) == 64, "Invalid size for XTFATTITUDEDATA type");

struct XTFPOSRAWDATAHEADER {
    WORD MagicNumber;
    BYTE HeaderType;
    BYTE SubChannelNumber;
    WORD NumChansToFollow;
    WORD Reserved1[2];
    DWORD NumBytesThisRecord;
    WORD Year;
    BYTE Month;
    BYTE Day;
    BYTE Hour;
    BYTE Minutes;
    BYTE Seconds;
    WORD MicroSeconds;
    double RawYcoordinate;
    double RawXcoordinate;
    double RawAltitude;
    float Pitch;
    float Roll;
    float Heave;
    float Heading;
    BYTE Reserved2;
};
static_assert (sizeof(XTFPOSRAWDATAHEADER) == 64, "Invalid size for XTFHEADERPOSRAWDATA type");

struct XTFPINGHEADER {
    WORD MagicNumber; /**< Must be set to 0xFACE (hexadecimal value). */
    BYTE HeaderType;
    BYTE SubChannelNumber; /**< If HeaderType is bathymetry, this indicates
                                 which head; if HeaderType is forward-looking
                                 sonar, and then this indicates which array. Also,
                                 Klein 5000 beam numbers are logged here. */
    WORD NumChansToFollow; /**< If HeaderType is sonar, number of channels to
                                follow. */
    WORD Reserved1[2];
    DWORD NumBytesThisRecord; /**< Total byte count for this ping including this ping
                                   header. Isis Note: Isis records data packets in
                                   multiples of 64 bytes. If the size of the data packet
                                   is not an exact multiple of 64 bytes, zeros are
                                   padded at the end packet and this value will be
                                   promoted to the next 64-byte granularity. In all
                                   cases, this value will be the EXACT size of this
                                   packet. */
    WORD Year;      /**< Ping year */
    BYTE Month;     /**< Ping month */
    BYTE Day;       /**< Ping day */
    BYTE Hour;      /**< Ping hour */
    BYTE Minute;    /**< Ping minute */
    BYTE Second;    /**< Ping seconds */
    BYTE HSeconds;  /**< Ping hundredths of seconds (0-99) */
    WORD JulianDay; /**< Julian day of a pingâ€™s occurrence. */

    DWORD EventNumber; /**< Last logged event number; nav interface template
                            token=O
                            NOTE: In Isis v4.30 and earlier this field was
                            located at byte 26-27 and was a two byte WORD.
                            At byte 24-25 there used to be a WORD
                            CurrentLineID. The CurrentLineID field no
                            longer exists in the .XTF format. Therefore, to
                            read the event number correctly an application
                            MUST check the Isis version string starting at
                            byte 10 of the XTFFILEHEADER structure. */
    DWORD PingNumber; /**< Counts consecutively (usually from 0) and
                           increments for each update. Isis Note: The
                           counters are different between sonar and
                           bathymetry updates. */
    float SoundVelocity; /**< m/s, Isis uses 750 (one way), some XTF files use
                              1500. Note: Can be changed on Isis menu. This
                              value is never computed and can only be changed
                              manually by the user. See
                              ComputedSoundVelocity below. */
    float OceanTide; /**< Altitude above Geoide (from RTK), if present;
                          ELSE Ocean tide in meters; nav interface
                          template token = {t} Isis Note: Can be changed by
                          the user on the Configure menu in Isis. */
    DWORD Reserved2;
    float ConductivityFreq; /**< Conductivity frequency in Hz. nav interface
                                 template token = Q Raw CTD information. The
                                 Freq values are those sent up by the Seabird CTD.
                                 The Falmouth Scientific CTD sends up computed
                                 data. */
    float TemperatureFreq; /**< Temperature frequency in Hz. nav interface
                                template token = b Raw CTD information. The
                                Freq values are those sent up by the Seabird CTD.
                                The Falmouth Scientific CTD sends up computed
                                data. */
    float PressureFreq; /**< Pressure frequency in Hz. nav interface template
                             token = 0. Raw CTD information. The Freq
                             values are those sent up by the Seabird CTD. The
                             Falmouth Scientific CTD sends up computed data. */
    float PressureTemp; /**< Pressure temperature (Degrees C); nav interface
                             template token = ; Raw CTD information. The
                             Freq values are those sent up by the Seabird CTD.
                             The Falmouth Scientific CTD sends up computed
                             data. */
    float Conductivity; /**< Conductivity in Siemens/m; nav interface token =
                             {c}; can be computed from Q Computed CTD
                             information. When using a Seabird CTD, these
                             values are computed from the raw Freq values
                             (above). */
    float WaterTemperature; /**< Water temperature in Celsius. nav interface token
                                 = {w}; can be computed from b. Computed CTD
                                 information. When using a Seabird CTD, these
                                 values are computed from the raw Freq values
                                 (above). */
    float Pressure; /**< Water pressure in psia; nav interface token = {p};
                         can be computed from 0. Computed CTD
                         information. When using a Seabird CTD, these
                         values are computed from the raw Freq values
                         (above). */
    float ComputedSoundVelocity; /**< Meters/second computed from Conductivity,
                                      WaterTemperature, and Pressure using the Chen
                                      Millero formula (1977), formula (JASA, 62,
                                      1129-1135) */
    float MagX; /**< X-axis magnetometer data in mgauss. Nav
                     interface template token = e. Sensors Information. */
    float MagY; /**< Y-axis magnetometer data in mgauss. Nav
                     interface template token = w. Sensors
                     Information. */
    float MagZ; /**< Z-axis magnetometer data in mgauss. Nav
                     interface template token = z. Sensors Information. */
    float AuxVal1; /**< Sensors Information. Nav interface template token
                        = 1. Auxiliary values can be used to store and
                        display any value at the user's discretion. Not used
                        in any calculation in Isis or Target. Isis Note:
                        Displayed in the “Sensors” window by selecting
                        “Window Text Sensors” */
    float AuxVal2; /**< Sensors Information. Nav interface template token
                        = 2. Auxiliary values can be used to store and
                        display any value at the user's discretion. These
                        are not used in any calculation in Isis or Target.
                        Isis Note: Displayed in the “Sensors” window by
                        selecting “Window Text Sensors” */
    float AuxVal3; /**< Sensors Information. Nav interface template token
                        = 3. Auxiliary values can be used to store and
                        display any value at the user's discretion. These
                        are not used in any calculation in Isis or Target.
                        Isis Note: Displayed in the “Sensors” window by
                        selecting “Window Text Sensors” */
    float AuxVal4; /**< Sensors Information. Nav interface template token
                        = 4. Auxiliary values can be used to store and
                        display any value at the user's discretion. These
                        are not used in any calculation in Isis or Target.
                        Isis Note: Displayed in the “Sensors” window by
                        selecting “Window Text Sensors” */
    float AuxVal5; /**< Sensors Information. Nav interface template token
                        = 5. Auxiliary values can be used to store and
                        display any value at the user's discretion. These
                        are not used in any calculation in Isis or Target.
                        Isis Note: Displayed in the “Sensors” window by
                        selecting “Window Text Sensors” */
    float AuxVal6; /**< Sensors Information. Nav interface template token
                        = 6. Auxiliary values can be used to store and
                        display any value at the user's discretion. These
                        are not used in any calculation in Isis or Target.
                        Isis Note: Displayed in the “Sensors” window by
                        selecting “Window Text Sensors” */
    float SpeedLog; /**< Sensors Information. Speed log sensor on towfish
                         in knots; Note: This is not fish speed. Nav
                         interface template token = s. */
    float Turbidity; /**< Sensors Information. Turbidity sensor (0 to +5
                          volts) multiplied by 10000. nav interface template
                          token = | (the “pipe” symbol). */
    float ShipSpeed; /**< Ship Navigation information. Ship speed in knots.
                          nav interface template token = v. Isis Note: These
                          values are stored only and are not part of any
                          equation or computation in Isis. */
    float ShipGyro; /**< Ship Navigation information. Ship gyro in
                         degrees. nav interface template token = G. Isis
                         Note: This is used as the directional sensor for
                         Multibeam Bathymetry data. */
    double ShipYcoordinate; /**< Ship Navigation information. Ship latitude or
                                 northing in degrees. nav interface template token
                                 = y. Isis Note: These values are stored only and
                                 are not part of any equation or computation in
                                 Isis. */
    double ShipXcoordinate; /**< Ship Navigation information. Ship longitude or
                                 easting in degrees. nav interface template token =
                                 x. Isis Note: These values are stored only and are
                                 not part of any equation or computation in Isis. */
    WORD ShipAltitude; /**< Ship altitude in decimeters */
    WORD ShipDepth; /**< Ship depth in decimeters. */
    BYTE FixTimeHour; /**< Sensor Navigation information. Hour of most
                           recent nav update. nav interface template token =
                           H. Isis Note: The time of the nav is adjusted by
                           the NavLatency stored in the XTF file header. */
    BYTE FixTimeMinute; /**< Sensor Navigation information. Minute of most
                             recent nav update. nav interface template token =
                             I. Isis Note: The time of the nav is adjusted by the
                             NavLatency stored in the XTF file header. */
    BYTE FixTimeSecond; /**< Sensor Navigation information. Second of most
                             recent nav update. nav interface template token =
                             S. Isis Note: The time of the nav is adjusted by the
                             NavLatency stored in the XTF file header. */
    BYTE FixTimeHsecond; /**< Sensor Navigation information. Hundredth of a
                              Second of most recent nav update. Isis Note: The
                              time of the nav is adjusted by the NavLatency
                              stored in the XTF file header. */
    float SensorSpeed; /**< Sensor Navigation information. Speed of towfish
                            in knots. Used for speed correction and position
                            calculation; nav interface template token = V. */
    float KP; /**< Sensor Navigation information. Kilometers Pipe;
                   nav interface template token = {K}. */
    double SensorYcoordinate; /**< Sensor Navigation information. Sensor latitude or
                                   northing; nav interface template token = E. Note:
                                   when NavUnits in the file header is 0, values are
                                   in meters (northings and eastings). When
                                   NavUnits is 3, values are in Lat/Long. Also see
                                   the Layback value, below. */
    double SensorXcoordinate; /**< Sensor Navigation information. Sensor longitude
                                   or easting; nav interface template token = N.
                                   Note: when NavUnits in the file header is 0,
                                   values are in meters (northings and eastings).
                                   When NavUnits is 3, values are in Lat/Long.
                                   Also see the Layback value, below. */
    WORD SonarStatus; /**< Tow Cable information. System status value,
                           sonar dependant (displayed in Status window). */
    WORD RangeToFish; /**< Slant range to sensor in decimeters; nav interface
                           template token = ? (question mark). Stored only â€“
                           not used in any computation. */
    WORD BearingToFish; /**< Bearing to towfish from ship, stored in degrees
                             multiplied by 100; nav interface template token =
                             > (greater-than sign). Stored only â€“ not used in
                             any computation in Isis. */
    WORD CableOut; /**< Tow Cable information. Amount of cable payed
                        out in meters; nav interface template token = o. */
    float Layback; /**< Tow Cable information. Distance over ground
                        from ship to fish.; nav interface template token =
                        l. Isis Note: When this value is non-zero, Isis
                        assumes that SensorYcoordinate and
                        SensorXcoordinate need to be adjusted with the
                        Layback. The sensor position is then computed
                        using the current sensor heading and this layback
                        value. The result is displayed when a position is
                        computed in Isis. */
    float CableTension; /**< Tow Cable information Cable tension from serial
                             port. Stored only; nav interface template token =
                             P */
    float SensorDepth; /**< Sensor Attitude information. Distance (m) from
                            sea surface to sensor. The deeper the sensor goes,
                            the bigger (positive) this value becomes. nav
                            interface template token = 0 (zero) */
    float SensorPrimaryAltitude; /**< Sensor Attitude information. Distance from
                                      towfish to the sea floor; nav interface template
                                      token = 7. Isis Note: This is the primary altitude
                                      as tracked by the Isis bottom tracker or entered
                                      manually by the user. Although not
                                      recommended, the user can override the Isis
                                      bottom tracker by sending the primary altitude
                                      over the serial port. The user should turn the Isis
                                      bottom tracker Off when this is done. */
    float SensorAuxAltitude; /**< Sensor Attitude information. Auxiliary altitude;
                                  nav interface template token = a. Isis Note: This is
                                  an auxiliary altitude as transmitted by an altimeter
                                  and received over a serial port. The user can
                                  switch between the Primary and Aux altitudes via
                                  the "options" button in the Isis bottom track
                                  window. */
    float SensorPitch; /**< Sensor Attitude information. Pitch in degrees
                            (positive=nose up); nav interface template token =
                            8. */
    float SensorRoll; /**< Sensor Attitude information. Roll in degrees
                           (positive=roll to starboard); nav interface template
                           token = 9. */
    float SensorHeading; /**< Sensor Attitude information. Sensor heading in
                              degrees; nav interface template token = h. */
    float Heave; /**< Attitude information. Sensors heave at start of
                      ping. Positive value means sensor moved up.
                      Note: These Pitch, Roll, Heading, Heave and Yaw
                      values are those received closest in time to this
                      sonar or bathymetry update. If a TSS or MRU is
                      being used with a multibeam/bathymetry sensor,
                      the user should use the higher-resolution attitude
                      data found in the XTFATTITUDEDATA
                      structures. */
    float Yaw; /**< Attitude information. Sensor yaw. Positive means
                    turn to right. Note: These Pitch, Roll, Heading,
                    Heave and Yaw values are those received closest
                    in time to this sonar or bathymetry update. If a
                    TSS or MRU is being used with a
                    multibeam/bathymetry sensor, the user should use
                    the higher-resolution attitude data found in the
                    XTFATTITUDEDATA structures. Since the
                    heading information is updated in high resolution,
                    it is not necessary to log or use Yaw in any
                    processing. Isis does not use Yaw. */
    DWORD AttitudeTimeTag; /**< Attitude information. In milliseconds - used to
                                coordinate with millisecond time value in Attitude
                                packets. (M)andatory when logging
                                XTFATTITUDE packets. */
    float DOT; /**< Misc. Distance Off Track */
    DWORD NavFixMilliseconds; /**< Misc. millisecond clock value when nav received. */
    BYTE ComputerClockHour; /**< Isis Note: The Isis computer clock time when this
                                 ping was received. May be different from ping
                                 time at start of this record if the sonar time-
                                 stamped the data and the two systems aren't
                                 synched. This time should be ignored in most
                                 cases. */
    BYTE ComputerClockMinute; /**< Isis Note: see above Isis Note */
    BYTE ComputerClockSecond; /**< Isis Note: see above Isis Note */
    BYTE ComputerClockHsec; /**< Isis Note: see above Isis Note */
    short FishPositionDeltaX; /**< Additional Tow Cable and Fish information from
                                   Trackpoint. Stored as meters multiplied by 3.0,
                                   supporting +/- 10000.0m (usually from
                                   trackpoint); nav interface template token = {DX}. */
    short FishPositionDeltaY; /**< Additional Tow Cable and Fish information from
                                   Trackpoint. X, Y offsets can be used instead of
                                   logged layback.; nav interface template token =
                                   {DY}. */
    unsigned char FishPositionErrorCode; /**< Additional Tow Cable and Fish information from
                                              Trackpoint. Error code for FishPosition delta x,y.
                                              (typically reported by Trackpoint). */
    unsigned int OptionalOffsey; /**< OptionalOffsey (Triton 7125 only) */
    BYTE CableOutHundredths; /**< Hundredths of a meter of cable out, to be added
                                  to the CableOut field. */
    BYTE ReservedSpace2[6]; /**< Unused. Set to 0. */
};
static_assert (sizeof (XTFPINGHEADER) == 256,
               "Invalid size for XTFPINGHEADER type");

using XTFBATHHEADER = XTFPINGHEADER;

struct XTFPINGCHANHEADER {
    WORD ChannelNumber;
    WORD DownsampleMethod;
    float SlantRange;
    float GroundRange;
    float TimeDelay;
    float TimeDuration;
    float SecondsPerPing;
    WORD ProcessingFlags;
    WORD Frequency;
    WORD InitialGainCode;
    WORD GainCode;
    WORD BandWidth;
    DWORD ContactNumber;
    WORD ContactClassification;
    BYTE ContactSubNumber;
    BYTE ContactType;
    DWORD NumSamples;
    WORD MillivoltScale;
    float ContactTimeOffTrack;
    BYTE ContactCloseNumber;
    BYTE Reserved2;
    float FixedVSOP;
    short Weight;
    BYTE ReservedSpace[4];
};
static_assert (sizeof (XTFPINGCHANHEADER) == 64,
               "Invalid size for XTFPINGCHANHEADER type");

// RAW ASCII data received over serial port
// These packets are stored in the XTF file on a per-serial-port
// basis.  To store the raw ASCII data for a given serial port, add
// the token "{SAVEALL}" to the serial port template.  Use of this
// option is not generally recommended, since Isis already parses the
// data for all usefull information.

///////////////////////////////////////////////////////////////////////////////
struct XTFRAWSERIALHEADER {

   WORD MagicNumber;      // Set to 0xFACE
   BYTE HeaderType;       // will be XTF_HEADER_RAW_SERIAL (7)
   BYTE SerialPort;
   WORD NumChansToFollow;
   WORD Reserved2[2];
   DWORD NumBytesThisRecord; // Total byte count for this update

   //
   // Date and time raw ASCII data was posted to disk
   //
   WORD  Year;
   BYTE  Month;
   BYTE  Day;
   BYTE  Hour;
   BYTE  Minute;
   BYTE  Second;
   BYTE  HSeconds;      // hundredth of seconds (0-99)
   WORD  JulianDay;     // days since Jan 1.

   DWORD TimeTag;       // millisecond timer value
   WORD  StringSize;    // Number of valid chars in RawAsciiData string
   char  RawAsciiData[64-30]; // will be padded in 64-byte increments to make
                              // structure an even multiple of 64 bytes

};

// Source time-stamped navigation data, holds updates of any nav data. (Type 42 navigation)
struct XTFNAVIGATIONHEADER
{
    WORD MagicNumber;           // Must be set to 0xFACE (hexadecimal value).
    BYTE HeaderType;            // 42 = XTF_HEADER_NAVIGATION (defined in Xtf.h)
    BYTE Reserved[7];           // Must be here!
    DWORD NumBytesThisRecord;   // Total byte count for this ping including this
                                // ping header. Isis Note: Isis records data
                                // packets in multiples of 64 bytes. If the size of
                                // the data packet is not an exact multiple of 64
                                // bytes, zeros are padded at the end packet and
                                // this value will be promoted to the next 64-byte
                                // granularity. In all cases, this value will be the
                                // EXACT size of this packet.
    WORD Year;                  // Source time Year
    BYTE Month;                 // Source time Month
    BYTE Day;                   //Source time Day
    BYTE Hour;                  // Source time Hour
    BYTE Minute;                // Source time Minute
    BYTE Second;                // Source time Seconds
    DWORD Microseconds;         // 0 - 999999
    DWORD SourceEpoch;          // Source Epoch Seconds since 1/1/1970
    DWORD TimeTag;              // System Reference time in milliseconds
    double RawYCoordinate;      // Raw position from POSMV or other time
                                // stamped navigation source
    double RawXCoordinate;      // Raw position from POSMV or other time
                                // stamped navigation source
    double RawAltitude;         // Altitude, can hold real-time kinematics altitude
    BYTE TimeFlag;              // Time stamp validity:
                                // 0 = only receive time valid
                                // 1 = only source time valid
                                // 3 = both valid
    BYTE Reserved1[6];          // Padding to make the structure 64 bytes
};

#pragma pack()
#endif // XTF_TYPES_HPP
