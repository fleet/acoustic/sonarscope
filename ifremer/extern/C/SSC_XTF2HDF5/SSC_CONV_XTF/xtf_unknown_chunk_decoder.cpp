#include "xtf_unknown_chunk_decoder.hpp"

#include <iostream>

void
ssc::xtf::UnknownChunkDecoder::_preprocess (std::fstream & stream,
                                            BYTE header_type)
{
    // Record that the type of datagram we just met
    auto it = _count.find (header_type);
    if (it != _count.end ()) {
        (*it).second += 1;
    } else {
        _count[header_type] = 1u;
    }

    // Here, process just jumps at the end of the datagram
    _skip_current (stream);
}

void
ssc::xtf::UnknownChunkDecoder::_preprocess_done ()
{
}

void
ssc::xtf::UnknownChunkDecoder::_process (std::fstream & stream,
                                         BYTE header_type)
{
    _skip_current (stream);
}

void
ssc::xtf::UnknownChunkDecoder::_terminate ()
{
    if (_count.size () > 0)
    {
        std::cout << "Unknown datagrams : " << std::endl;
        for (auto it : _count) {
            std::cout << "\tHeader type '" << static_cast<int>(it.first) << "' met " << it.second << " times." << std::endl;
        }
    }
}

ssc::xtf::UnknownChunkDecoder::~UnknownChunkDecoder ()
{
}
