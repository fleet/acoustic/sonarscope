#ifndef XTF_UNKNOWN_CHUNK_DECODER_HPP
#define XTF_UNKNOWN_CHUNK_DECODER_HPP

#include "ssc_abstract_decoder.hpp"

#include <map>

namespace ssc {
    namespace xtf {
        class UnknownChunkDecoder : public AbstractDecoder {
          private:
            std::map<BYTE, size_t> _count;
          public:
            explicit UnknownChunkDecoder () : _count {} {};
            ~UnknownChunkDecoder ();
          protected:
            void _preprocess (std::fstream &, BYTE);
            void _preprocess_done ();
            void _process (std::fstream &, BYTE);
            void _terminate ();
        };
    } // ssc::decoder
} // ssc

#endif // XTF_UNKNOWN_CHUNK_DECODER_HPP
