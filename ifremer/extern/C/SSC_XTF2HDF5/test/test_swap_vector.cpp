#include "ssc_utils.h"

#include <vector>

int
main (int argc, char**argv)
{
    std::vector <int> ints {1, 2, 3, 4};
    std::vector <int> reversed {4, 3, 2, 1};
    
    // create a char vector, that contain the same values than the ints vector
    std::vector <char> like_ints;
    char const * const ints_char_view {reinterpret_cast<char *>(&ints[0])};
    like_ints.resize (ints.size () * sizeof (int));
    for (size_t i = 0; i < like_ints.size (); i++) {
        like_ints[i] = ints_char_view[i];
    }
    int const * const like_ints_int_view {reinterpret_cast<int*>(&like_ints[0])};

    ssc::utils::swap_vector (ints, sizeof(int));
    ssc::utils::swap_vector (like_ints, sizeof(int));
   
    for (size_t i = 0; i < reversed.size (); i++) {
        if (ints[i] != reversed[i])
            return -1;
        if (like_ints_int_view[i] != reversed[i])
            return -1;
    }

    try {
        std::vector<char> tmp;
        tmp.resize (5);
        ssc::utils::swap_vector (tmp, 3);
        return -1;
    } catch (ssc::utils::swap_error) {};

    return 0;
}
