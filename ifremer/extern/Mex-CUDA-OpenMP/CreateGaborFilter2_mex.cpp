/*
		G = CreateGaborFilter2_mex(f0, theta, gamma, eta, N)
		theta en radians
*/



#include "mex.h"
#include <vector>
#include <omp.h>
#include <float.h>
#include <algorithm>


#include <math.h>


#define __PI       3.14159265358979323846
#define PT	0.998
#define PF	0.998



double carre(double a) {return a * a;};

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
 if(nrhs != 5) mexErrMsgTxt("CreateGaborFilter2_mex requiert 5 arguments");
 if(nlhs != 1) mexErrMsgTxt("CreateGaborFilter2_mex requiert 1 argument en sortie");

 double f0 = mxGetScalar(prhs[0]);
 double theta = mxGetScalar(prhs[1]);
 double gamma = mxGetScalar(prhs[2]);
 double eta = mxGetScalar(prhs[3]);
 double n = mxGetScalar(prhs[4]); 
 int N = (int)n;

 double *Gr; // partie r�elle de G
 double *Gi; // partie imaginaire de G
 mwSize dims[2];
 dims[0] = N;
 dims[1] = N;
 plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxCOMPLEX);
 Gr = (double*)mxGetPr(plhs[0]);
 Gi = (double*)mxGetPi(plhs[0]);
 

 double alpha = f0 / gamma;
 double beta = f0 / eta;
 
 double *X;
 X = (double*)malloc(sizeof(double) * N);
 
 if(N % 2 > 0) for(int i = 0; i < N; i++) X[i] = -(N - 1) / 2 + i;
 else for(int i = 0; i < N; i++) X[i] = -N / 2 + i;
 
 
 for(int i = 0; i < N; i++) for(int j = 0; j < N; j++)
 {
	
	/*      G = alpha * beta / __PI * cexp(-carre(alpha) * carre(X[i] * cos(theta) + X[j] * sin(theta))
											- carre(beta) * carre(-X[i] * sin(theta) + X[j] * cos(theta))
											+ I * 2 * __PI * f0 * (X[i] * cos(theta) + Y * sin(theta))); 
			  = m * exp(a + I * b)
			  = m * exp(a) * (cos b + I * sin b)
			  = m * exp(a) * cos b + I * exp(a) * sin b											*/
    double mexpa = alpha * beta / __PI * exp(-carre(alpha) * carre(X[i] * cos(theta) + X[j] * sin(theta)) - carre(beta) * carre(-X[i] * sin(theta) + X[j] * cos(theta)));
	double b = 2 * __PI * f0 * (X[i] * cos(theta) + X[j] * sin(theta));
	Gr[j + i * N] = mexpa * cos(b); 
	Gi[j + i * N] = mexpa * sin(b);
 } 
 
 
 
}

