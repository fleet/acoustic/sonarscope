% nomFic1 = getNomFicDatabase('textureSonar02.png');
% nomFic2 = getNomFicDatabase('textureSonar01.png');
% nomFic3 = getNomFicDatabase('textureSonar03.png');
% nomFic4 = getNomFicDatabase('textureSonar04.png');
% I1 = imread(nomFic1);
% I2 = imread(nomFic2);
% I3 = imread(nomFic3);
% I4 = imread(nomFic4);
% I14 = [I1 I2; I3 I4];
% figure; imagesc(I14); colormap(gray(256))
%
% nomFic5 = getNomFicDatabase('textureSonarMean100Std16_01.png');
% nomFic6 = getNomFicDatabase('textureSonarMean100Std16_02.png');
% nomFic7 = getNomFicDatabase('textureSonarMean100Std16_03.png');
% nomFic8 = getNomFicDatabase('textureSonarMean100Std16_04.png');
% I5 = imread(nomFic5);
% I6 = imread(nomFic6);
% I7 = imread(nomFic7);
% I8 = imread(nomFic8);
% I58 = [I5 I6; I7 I8];
% figure; imagesc(I58); colormap(gray(256))
%
% learnSegmMat({I1, I2, I3, I4}, 1, 0)
% learnSegmMat({I5, I6, I7, I8}, 1, 0)
%
% CmatModele = learnSegmMat({I5, I6, I7, I8}, 1, 0, 128);

function varargout = learnSegmMat(IModele, dX, dY, nbNiveaux)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

warning off
% flagKullman = 1;

if ~iscell(IModele)
    my_warndlg('texture:learnSegmMat Il faut donner plusieurs images de reference', 0);
end

% --------------------------------
% Calcul des matrices de reference

for k=1:length(IModele)
    if isempty(IModele{k})
        CmatModele{k} =[]; %#ok<AGROW>
    else

        % On filtre systematiquement les matrices d'apprentissage
%         CmatModele{k} = coocMat(IModele{k}, dX, dY, 'Filtre', 'nbNiveaux', nbNiveaux); %#ok<AGROW>
        CmatModele{k} = coocMatNbNiveauxFiltre_mexmc(uint8(IModele{k}), int32(dX), int32(dY), int32(nbNiveaux), int32(NUMBER_OF_PROCESSORS)); %#ok<AGROW>

        % Pour calculer au mieux la distance de Kullback, il faut
        % ajouter un epsilon aux matrices d'apprentissage
        p2NonNul = CmatModele{k};
        p2NonNul = p2NonNul(p2NonNul ~= 0);
        if ~isempty(p2NonNul)
            coef = min(p2NonNul) / 100;
            CmatModele{k} = CmatModele{k} + coef; %#ok<AGROW>
        end
    end
end


if nargout == 0
    figure;
    n = length(IModele);
    for k=1:n
        subplot(n,1,k); imagesc(CmatModele{k});
    end
else
    varargout{1} = CmatModele';
end
