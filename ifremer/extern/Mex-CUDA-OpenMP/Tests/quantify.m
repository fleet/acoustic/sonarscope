% Quantification d'une image par une loi lineaire
%
% Syntax
%   [Y, bins] = quantify_mex(X, ...)
%
% Input Arguments
%   X : Image de depart
%
% Name-Value Pair Arguments
%   rangeIn  : Bornes debut et fin de la fonction de transfert [min max] par
%              defaut
%   rangeOut : Bornes debut et fin de l'image quantifiee [1 255] par defaut
%              En inversant les bornes, on realise une inversion video
%   N        : Nombre de valeurs (255 par defaut)
%
% Output Arguments
%   Y    : Image de d'arrivee
%   bins : Centres des classes formees
%
% Remarks : En inversant les bornes de rangeOut, on realise une inversion video
%
% Examples
%   X = 1:255;
%   Y = quantify_mex(X, 'rangeIn', [50 200]);
%   plot(X, Y, '*');
%   Y = quantify_mex(X, 'rangeIn', [50 200], 'N', 10);
%   plot(X, Y, '*');
%   Y = quantify_mex(X, 'rangeIn', [50 200], 'N', 10, 'rangeOut', [50 200]);
%   plot(X, Y, '*');
%   Y = quantify_mex(X, 'rangeIn', [50 200], 'N', 10, 'rangeOut', [200 50]);
%   plot(X, Y, '*');
%
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   X = reflec_Enr2dB(double(imread(nomFic)));
%   [Resultats, str] = stats(X)
%   Y = quantify(X, 'rangeIn', Resultats(8:9));
%   imagesc(Y); colorbar; colormap(gray(256))
%
% See also stats Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function [X, binsOut] = quantify(X, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[varargin, Step]                 = getPropertyValue(varargin, 'Step', []);
[varargin, N]                    = getPropertyValue(varargin, 'N', 255);
[varargin, rangeIn]              = getPropertyValue(varargin, 'rangeIn', []);
[varargin, rangeOut]             = getPropertyValue(varargin, 'rangeOut', []);
[varargin, flagCentrageMagnetic] = getPropertyValue(varargin, 'CentrageMagnetic', 1); %#ok<ASGLU>

% if isa(X, 'uint8')
%     X = single(X);
% end

% Ajout� le 21/01/2009 pour pouvoir faire une compensation statistique sur
% donn�es Auror image 12000x10000
if isa(X, 'uint8')
    X = single(X(:,:,:));
else
    X = X(:,:,:);
end


if isempty(rangeIn)
    rangeIn(1) = min(X(:));
    rangeIn(2) = max(X(:));
end

sub = (X < rangeIn(1));
X(sub) = rangeIn(1);
sub = (X > rangeIn(2));
X(sub) = rangeIn(2);

if isempty(Step)
    bins = linspace(rangeIn(1), rangeIn(2), N+1);
    bins = (bins(1:end-1) + bins(2:end)) / 2;
    if isempty(rangeOut)
        rangeOut = [1 255];
    end
    binsOut = bins;
else
    binsOut = rangeIn(1):Step:rangeIn(2);
    if flagCentrageMagnetic
        bins = centrage_magnetique(rangeIn(1):Step:rangeIn(2));
    else
        bins = rangeIn(1):Step:rangeIn(2);
    end
    %     if bins(1) > rangeIn(1)
    %     if bins(1) > (rangeIn(1) + eps) % Bricol� pour stats =f(X, Detection), Detection de 0 � 4
    %         bins = bins - Step;
    %     end
    rangeIn(1) = bins(1);
    rangeIn(2) = bins(end);
    if isempty(rangeOut)
        rangeOut = [1 length(bins)];
    end
    N = length(bins);
end

if rangeOut(2) == rangeOut(1)
    X(~isnan(X)) = rangeOut(1);
    return
end


X = quantify_private_mexmc(X, bins(1), double(rangeIn), double(rangeOut), N, NUMBER_OF_PROCESSORS);

% dans le mex-file :
% SZ = size(X);
% a = (N-1) / diff(rangeIn);
% b = - a * bins(1);
%
%
% for i=1:SZ(1)
%     x = floor(a * double(X(i,:,:)) +  b);
%     x = rangeOut(1) +x * diff(rangeOut)/(N-1);
%     x(x < 1) = 1;
%     x(x > N) = N;
%     X(i,:,:) = x;
% end
