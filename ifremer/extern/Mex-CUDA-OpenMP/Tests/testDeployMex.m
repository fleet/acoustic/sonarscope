% Compilation 
%     mcc -v -m testDeployMex.m ...
%         -a C:\SonarScope\SScTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\dll\interpHorzImage_mexmc.mexw64 ...
%         -a C:\SonarScope\SScTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\dll\interp1Linear_mex.mexw64 ...
%         -d 'C:\Users\rgallou\Desktop\testMexDeploy'
% -------
function flag = testDeployMex

flag = -1;

%% Check mex functions
try
    xq = interp1Linear_mex(1:100, rand(1,100), 0:0.5:100); %#ok<NASGU>
    whos xq
catch ME %#ok<NASGU>
    str = sprintf('SonarScope implements mex functions in order to use compiled computations on specific tasks.');
    str = sprintf('%s It seems these functions crash on your computer. Do not worry, SSc automatically switches to the "matlab" versions o these functions.', str);
    str = sprintf('%s\n\nCould you please install "Microsoft Studio Visual C++ Run-Time" that is delivered in the "R20xxy_SetUpSonarScope_External_yyyymmdd.exe" and launch SSc again ?', str);
    str = sprintf('%s\n\nPlease, let us know if this installation has resolved the problem. Email to sonarscope@ifremer.fr', str);
    str = sprintf('%s\n\nNote : you must use an "External" setup generated after 2019/12/18. Download a new version in case you do not have it.', str);
    my_warndlg(str, 1);
    NUMBER_OF_PROCESSORS = 0; %#ok<*NASGU>
    return
end

try
    X = ones(50);
    NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS')));
    Y = interpHorzImage_mexmc(single(X), NUMBER_OF_PROCESSORS, 1); %#ok<NASGU>
    whos Y
catch ME %#ok<NASGU>
    str = sprintf('SonarScope implements OpenMP mex functions in order to use multithreading computations on specific tasks.');
    str = sprintf('%s It seems these functions crash on your computer. Do not worry, SSc automatically switches to the "matlab" versions o these functions.', str);
    str = sprintf('%s\n\nCould you please install "Microsoft Studio Visual C++ Run-Time" that is delivered in the "R20xxy_SetUpSonarScope_External_yyyymmdd.exe" and launch SSc again ?', str);
    str = sprintf('%s\n\nPlease, let us know if this installation has resolved the problem. Email to sonarscope@ifremer.fr', str);
    str = sprintf('%s\n\nNote : you must use an "External" setup generated after 2019/12/18. Download a new version in case you do not have it.', str);
    my_warndlg(str, 1);
    NUMBER_OF_PROCESSORS = 0;
    return
end

pppp = dbstack;
fprintf('==> end of %s\n', pppp.name); 
flag = 0;