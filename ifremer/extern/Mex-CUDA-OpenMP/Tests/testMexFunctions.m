%% Initialisations
% Compilation d'un fichier mex par :
% mex <fileMex.cpp>
global IfrTbx %#ok<GVMIS>

load(fullfile(IfrTbx, 'ifremer', 'extern', 'mex', 'Mex-CUDA-OpenMP', 'Tests', 'Array.mat'));
t = 5;
%%
tic;
ImageFmatlab = fillNaN_mean(Image, [t t]);
toc;

% tic;
% ImageFmex = fillNaN_mean_mex(Image, t, 8);
% toc;
%% Tests des fonctions Mex-OpenMP
NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS')));

whos Image
tic;
ImageFmexmc2 = fillNaN_mean_mexmc(single(Image), double([t t]), double(NUMBER_OF_PROCESSORS));
toc;

%% Tests des fonctions CUDA
%tic;
%ImageFmexcuda = fillNaN_mean_mexcuda(Image, t, t);
%toc;

s = mod(0:2:1800, 360);
X = repmat(s, length(s), 1);
X(100:250,100:250) = NaN;
N = floor(length(s)^2 / 2);
d = randi(length(s), [N 2]);
for k=1:size(d,1)
    X(d(k,2),d(k,1)) = NaN;
end
figure; imagesc(X)

s = mod(0:2:80,20);
X = repmat(s, length(s), 1);
X(10:25,10:25) = NaN;
N = floor(length(s)^2 / 2);
d = randi(length(s), [N 2]);
for k=1:size(d,1)
    X(d(k,2),d(k,1)) = NaN;
end
figure; imagesc(X)

%%
NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS')));
% load('C:\IfremerToolbox_R2008b\ifremer\extern\mex\Mex-CUDA-OpenMP\Tests\data_x.mat');
load(fullfile(IfrTbx, 'ifremer', 'extern', 'mex', 'Mex-CUDA-OpenMP', 'Tests','dataFillNanMex.mat'));
window = [3 3];
Id = fillNaN_mean_double_mexmc(x, double(window), double(NUMBER_OF_PROCESSORS));
Is = fillNaN_mean_mexmc(single(x), single(window), double(NUMBER_OF_PROCESSORS));
figure(1); imagesc(x); colorbar
figure(2); imagesc(Id); colorbar
figure(3); imagesc(Is); colorbar

%% Test Fct Horz Image Range 
range = 5;
SE = true(1,1+2*range);
x = true(1,100);
x(1:4:40)   = false;
x(60:75)    = false;
x(96:98)    = false;

%% Interpolation lin�aire horizontale

NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS')));
range = 5;
figure; imagesc(X)
Y1 = interpHorzImage(X);
figure; imagesc(Y1);
YD = interpHorzImage_mexmc(single(X), NUMBER_OF_PROCESSORS, 1);
figure; imagesc(YD);
figure; imagesc(YD - Y1);
range = 76; % 400;

Y1 = interpHorzImageRange(single(X), range, 'linear');
figure; imagesc(Y1);

YD =  interpHorzImageRange_mexmc(single(X), range,  NUMBER_OF_PROCESSORS, 0);
figure; imagesc(YD);

%% Reproduit le fctt de imopen qui y est appel�)


ximdilate   = imdilate(x, SE);
ximerode    = imerode(x, SE);
ximopen     = imopen(x, SE);

x2steps     = imdilate(ximerode, SE);

figure(111);
h(1) = subplot(5,1,1); plot(x, '*'); grid; title('x');
h(2) = subplot(5,1,3); plot(ximerode, '*'); grid; title('ximerode');
h(3) = subplot(5,1,2); plot(ximdilate, '*'); grid; title('ximdilate');
h(4) = subplot(5,1,4); plot(ximopen, '*'); grid; title('ximopen');
h(5) = subplot(5,1,5); plot(x2steps, '*'); grid; title('x2steps');

linkaxes(h, 'x')

%% Appel de la fonction Dilate
compilALL_mexOpenMP('option', '-v', 'filesource', {'interpHorzImageRange_mexmc.cpp'});
clc; 
[YD, YE] =  interpHorzImageRange_mexmc(single(x), range,  NUMBER_OF_PROCESSORS, 0);


figure(112);
h(1) = subplot(3,1,1); plot(x, '*'); grid; title('x');
h(2) = subplot(3,1,2); plot(YD, '*'); grid; title('ximdilate');
h(3) = subplot(3,1,3); plot(YE, '*'); grid; title('ximerode');

linkaxes(h, 'x') 