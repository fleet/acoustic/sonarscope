NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS')));

I = imread('board.tif');
J = rgb2gray(I);
figure, imshow(I)
figure, imshow(J);


Image = I;

tic;
CmatF = coocMatNbNiveauxFiltre(Image, 1, 0, 255);
toc;

tic;
CmatF = coocMatNbNiveauxFiltre_mexmc(uint8(Image), int32(1), int32(0), int32(255), int32(NUMBER_OF_PROCESSORS));
toc;
