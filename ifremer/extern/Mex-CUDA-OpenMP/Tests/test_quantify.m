%% Compil 32 bits ou 64 bits
clear functions;
cd 'C:\IfremerToolbox_R2008b\ifremer\extern\mex\Mex-CUDA-OpenMP'

compilALL_mexOpenMP('platform','Win64','option', '-v', 'filesource',{'quantify_private_mexmc.cpp'});

%% Pour le débuggage avec Visual C++

copyfile('C:\IfremerToolbox_R2008b\ifremer\extern\mex\Mex-CUDA-OpenMP\dll\quantify_private_mexmc.mexw32', ...
        'C:\IfremerToolbox_R2008b\ifremer\extern\mex\Mex-CUDA-OpenMP\quantify_private_mexmc.mexw32');
copyfile('C:\IfremerToolbox_R2008b\ifremer\extern\mex\Mex-CUDA-OpenMP\dll\quantify_private_mexmc.mexw32.pdb', ...
        'C:\IfremerToolbox_R2008b\ifremer\extern\mex\Mex-CUDA-OpenMP\quantify_private_mexmc.mexw32.pdb');

%%
J = imread('241.bmp');
I = rgb2gray(J);
%load 'Image.mat'
Image = I;
tic;
Iqold = quantify_old(Image, 'rangeIn', [0 255], 'N', 100, 'rangeOut', [50 80]);
disp 'matlab';
toc;

% tic;
% n = quantify_mex(Image, 'rangeIn', [0 255], 'N', 100, 'rangeOut', [50 80]);
% toc;

tic;
Iqmexmc = quantify(Image, 'rangeIn', [0 255], 'N', 100, 'rangeOut', [50 80]);
disp 'mexmc';
toc;
%%
X = loadmat('X.mat');
bins(1) = 1.4980;
rangeIn = [1 255];
rangeOut = [1 255];
N = 255;
NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS')));
pppp = quantify_private_mexmc(X, bins(1), double(rangeIn), double(rangeOut), N, NUMBER_OF_PROCESSORS);
%%
pppp = quantify_private_mexmc([2 3], 2, double([2 3]), double([1 2]), 2, 2);
%%
bins(1) = 4.2345;
N = 64;
rangeIn = [2.2700  253.7300];
rangeOut = [1    64];
NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS'))); 
fileName = fullfile(my_tempdir, 'pppp.mat'); 
load(fileName, 'pppp');
pppp2 = quantify_private_mexmc(pppp, bins(1), double(rangeIn), double(rangeOut), N, NUMBER_OF_PROCESSORS);
