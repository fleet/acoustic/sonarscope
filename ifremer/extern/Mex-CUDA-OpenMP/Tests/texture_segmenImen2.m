% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_segmenImen2(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%   b = texture_segmenImen2(a)
%   imagesc(a)
%   plot(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = texture_segmenImen2(this, LayerAngle, varargin)

nbImages = length(this);
for i=1:nbImages
    [Segm, Dist, Dist12] = unitaire_texture_segmenImen2(this(i), LayerAngle, varargin{:});
    that(i,1) = Dist; %#ok<AGROW>
    that(i,2) = Dist12; %#ok<AGROW>
    that(i,3) = Segm; %#ok<AGROW>
end

function [Segm, Dist, Dist12] = unitaire_texture_segmenImen2(this, LayerAngle, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[varargin, sub]            = getPropertyValue(varargin, 'sub', []);
[varargin, subx]           = getPropertyValue(varargin, 'subx', []);
[varargin, suby]           = getPropertyValue(varargin, 'suby', []);
[varargin, iClique]        = getPropertyValue(varargin, 'iClique', []);
[varargin, Win]            = getPropertyValue(varargin, 'Win', []);
[varargin, SeuilConfusion] = getPropertyValue(varargin, 'SeuilConfusion', 0);
[varargin, SeuilRejet]     = getPropertyValue(varargin, 'SeuilRejet', Inf); %#ok<ASGLU>

if isempty(iClique)
    iClique = 1;
end

if isempty(subx)
    subx = 1:length(this.x);
end

if isempty(suby)
    suby = 1:length(this.y);
end

if isempty(sub)
    sub  = selectionSegmSignature(this);
end

if isempty(Win)
    Win = [32 32];
else
    if length(Win) ~= 2
        Win = [Win(1) Win(1)];
    end
end

% ------------------------------------------
% Recuperation de la definition des textures

Texture = this.Texture(sub);

% -------------------------
% Quantification de l'image

rangeIn  = Texture.binsImage.CLim;
N        = Texture.binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(this.Image(suby, subx), 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

% -------------------------------------------------------------
% Lecture et Quantification de l'image conditionnelle angulaire

flagAngle = ~isempty(LayerAngle);
if flagAngle
    val = get_val_xy(LayerAngle, this.x(subx), this.y(suby));
    indAngle = quantify_byBins(val,  Texture.binsLayerAngle);
    %     figure(99999); imagesc(indAngle); colorbar; colormap(jet(256)); axis xy; pixval
end







CmatModeleAngle = Texture.CmatModeleAngle;
Clique          = Texture.cliques(iClique,:);
NbNiveaux       = Texture.binsImage.NbNiveaux;

dX = Clique(1);
dY = Clique(2);

flagFiltrageCooc = 0; % à parametrer

nbFacies = length(CmatModeleAngle);
listeFaciesPossibles = [];
for iFacies=1:nbFacies
    CmatFacies = CmatModeleAngle{iFacies};
    if ~isempty(CmatFacies)
        listeFaciesPossibles(end+1) = iFacies; %#ok<AGROW>
    end
end
nbFaciesPossibles = length(listeFaciesPossibles);
if isempty(listeFaciesPossibles)
    disp('beurk')
end

for m = 1 : length(CmatModeleAngle) % rajouté par kevin : les mex-files n'aiment pas les structures contenant des éléments = []
    for i = 1 : length(CmatModeleAngle{1,1})
        if ~isempty(CmatModeleAngle{1,m})
            if isempty(CmatModeleAngle{1,m}{1,i})
                CmatModeleAngle{1,m}{1,i} = 1; % élément d'une dimension
                
            end
        end
    end
end

% sz = size(Imagettes);

[nbL, nbC] = size(I);
segm = zeros(nbL, nbC, 'uint8');
dist = NaN(1, nbFacies, 'single');
Dist = zeros(nbL, nbC, 'single');
Dist12 = zeros(nbL, nbC, 'single');
W = floor(Win/2);

str1 = 'Etape 2 de la segmentation';
str2 = 'Segmentation processing step 2';
hw = create_waitbar(Lang(str1,str2), 'N', nbL);
for il=1:nbL
    my_waitbar(il, nbL, hw);
    subl = (il-W(1)+1):(il+W(1));
    subl(subl < 1) = [];
    subl(subl > nbL) = [];
    ImageL = I(subl, :);
    
    %     if flagAngle
    %         indAngleImagetteL = indAngle(il, :);
    %         [segmlocal Distlocal Dist12local] = coocMatNbNiveauxKullbackAnglesSortSegmDist_mexmc(uint8(ImageL), dX, dY, NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), indAngleImagetteL, flagFiltrageCooc, str2double(getenv('NUMBER_OF_PROCESSORS')));
    %     else
    %         [segmlocal Distlocal Dist12local] = coocMatNbNiveauxKullbackSortSegmDist_mexmc(uint8(ImageL), dX, dY, NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), flagFiltrageCooc, str2double(getenv('NUMBER_OF_PROCESSORS')));
    %     end
    %
    
    if flagAngle
        indAngleImagetteL = indAngle(il, :);
    else
        indAngleImagetteL = 1;
    end
    
    [segmlocal, Distlocal, Dist12local] = texture_segmenImen2_private_mexmc(uint8(ImageL), dX, dY, NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), indAngleImagetteL, flagFiltrageCooc, NUMBER_OF_PROCESSORS);
    
    segm(il, :) = segmlocal(:);
    Dist(il, :) = Distlocal(:);
    Dist12(il, :) = Dist12local(:);
    
    %     for ic=1:nbC
    %         subc = (ic-W(2)+1):(ic+W(2));
    %         subc(subc < 1) = [];
    %         subc(subc > nbC) = [];
    %
    %         Imagette = ImageL(:, subc);
    %
    %         if sum(isnan(Imagette(:))) == numel(Imagette)
    %             continue
    %         end
    %
    %         if flagAngle
    %             iAngle = indAngleImagetteL(W(1), ic);% A VERIFIER
    %             if isnan(iAngle)
    %                 continue
    %             end
    %         else
    %             iAngle = 1;
    %         end
    %
    %         if sum(Imagette(:)) == 0
    %             continue
    %         end
    %
    %         if Filtre
    %             Cmat = coocMatNbNiveauxFiltre(Imagette, dX, dY, NbNiveaux); %#ok<UNRCH>
    %         else
    %             Cmat = coocMatNbNiveaux(Imagette, dX, dY, NbNiveaux);
    %         end
    %
    %         for k=1:nbFaciesPossibles
    %             iFacies = listeFaciesPossibles(k);
    %             CmatFacies = CmatModeleAngle{iFacies}{iClique,iAngle};
    %             if isempty(CmatFacies)
    %                 dist(iFacies) = Inf;
    %             else
    %                 dist(iFacies) = kullback(Cmat, CmatFacies);
    %             end
    %         end
    % %         [d, s] = min(dist(:));
    % %         segm(il,ic) = s;
    % %         Dist(il,ic) = d;
    %
    %         [d, ordre] = sort(dist(:));
    %         separabilite = abs(d(1)-d(2));
    %         if d(1) == 0
    %             continue
    %         elseif d(1) > SeuilRejet
    %             segm(il,ic) = nbFaciesPossibles + 2;
    %         elseif separabilite < SeuilConfusion
    %             segm(il,ic) = nbFaciesPossibles + 1;
    %         else
    %             segm(il,ic) = ordre(1);
    %         end
    %         Dist(il,ic) = d(1);
    %         Dist12(il,ic) = separabilite;
    %     end
end
my_close(hw, 'MsgEnd');

% -----------------------------

Segm = replace_Image(this, segm);
subx = linspace(subx(1), subx(end), size(segm, 2));
suby = linspace(suby(1), suby(end), size(segm, 1));
Segm = majCoordonnees(Segm, subx, suby);

Segm.ValNaN = 0;
Segm = compute_stats(Segm);

Segm.TagSynchroContrast = num2str(rand(1));
% CLim = [-0.5 Segm.StatValues.Max+0.5];
CLim = [-0.5 nbFaciesPossibles+2+0.5];
Segm.CLim       = CLim;
% map = [1 1 1; Texture.Coul];
map = [0 0 0; Texture.Coul; 0.8 0.8 0.8; 0.9 0.9 0.9];
Segm.ColormapCustom = map;
Segm.ColormapIndex  = 1;
Segm.ImageType      = 3;
Segm.Video          = 1;
Segm.Unit           = 'num';

Segm.DataType = cl_image.indDataType('Segmentation');
Segm = update_Name(Segm);

% -----------------------------

Dist = replace_Image(this, Dist);
Dist = majCoordonnees(Dist, subx, suby);
Dist.ValNaN = NaN;
Dist = compute_stats(Dist);
CLim = [0 Dist.StatValues.Max];
Dist.TagSynchroContrast = num2str(rand(1));
Dist.CLim          = CLim;
Dist.ColormapIndex = 3;
Dist.ImageType     = 1;
Dist.Unit          = ' ';
Dist.Video         = 1;
Dist.DataType      = cl_image.indDataType('Kullback');
Dist = update_Name(Dist);

% -----------------------------

Dist12 = replace_Image(this, Dist12);
Dist12 = majCoordonnees(Dist12, subx, suby);
Dist12.ValNaN = NaN;
Dist12 = compute_stats(Dist12);
CLim = [0 Dist12.StatValues.Max];
Dist12.TagSynchroContrast = num2str(rand(1));
Dist12.CLim               = CLim;
Dist12.ColormapIndex      = 3;
Dist12.ImageType          = 1;
Dist12.Unit               = ' ';
Dist12.Video              = 1;
Dist12.DataType          = cl_image.indDataType('Kullback');
Dist12 = update_Name(Dist12, 'Append', 'Diff 2');


%{
% -----------------------------
% Remise a l'echelle de l'image

this = replace_Image(this, segm);

% --------------------------
% Mise a jour de coordonnees

% nbLNew = nbLI * Win(1);
% nbCNew = nbCI * Win(2);
% x =
% Il faudrait faire ça correctement car il y a un pb de multiple : a faire
% avec une image très petite Ex 65x63 pour une fenêtre 32x32 genre
% subx = subx(1)+Win(2)/2 : Win(2): xxxxx;

subx = linspace(subx(1), subx(end), size(segm, 2));
suby = linspace(suby(1), suby(end), size(segm, 1));
this = majCoordonnees(this, subx, suby);

this.ValNaN = 0;

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));

% CLim = [0 this.StatValues.Max];
% this.CLim       = CLim;
% this.ImageType  = 1;    % Image indexee
% this.ColormapIndex = 1;
% map = jet(nbFacies);
% this.ColormapCustom = [0 0 0; map];

CLim = [-0.5 this.StatValues.Max+0.5];
this.CLim       = CLim;
map = [0 0 0; Texture.Coul];
this.ColormapCustom = map;
this.ColormapIndex = 1;
this.ImageType  = 1;

this.Unit = 'num';

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('Segmentation');
this = update_Name(this, 'Append', 'Segmentation');
%}
