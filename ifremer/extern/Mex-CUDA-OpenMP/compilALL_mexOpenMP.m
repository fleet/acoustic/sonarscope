% Compilation des fonctions mex/openMP cr��es par Kevin Le COQ (Ete 2009).
% Tous les fichiers sont compil�s avec l'option openmp.
%
% Syntax
%   compilALL_mexOpenMP(file1, file2, ...)
%   compilALL_mexOpenMP(all)
%
% Input Arguments
%   nom du fichier � compiler,
%   option : soit verbose, soit debuggage, soit ...
%   all : tous les fichiers sont � compiler.
%
% Name-Value Pair Arguments
%   ...
%
% Output Arguments
%   Sans objet
%
% Examples
%   compilALL_mexOpenMP('option', '-v', 'filesource',...
%               {'kullback_mexmc.cpp' 'quantify_private_mexmc.cpp'});
%   compilALL_mexOpenMP('option', '-g');
%
%   ou en ligne de commande directe :
%     mex -v C:\SonarScopeTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\fillNaN_mean_mexmc.cpp ...
%     OPTIMFLAGS='$OPTIMFLAGS -openmp' -outdir C:\SonarScopeTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\dll
%
% REMARQUE:
%   Ne pas oublier de changer de Compilateur par mex -setup si l'option de
%   compilation change.
%
% See also compil_CIBDOU
% Authors : GLU
% ----------------------------------------------------------------------------

function compilALL_mexOpenMP(varargin)

global IfrTbx %#ok<GVMIS>

[varargin, option]     = getPropertyValue(varargin, 'option',     []);
[varargin, filesource] = getPropertyValue(varargin, 'filesource', {'all'}); %#ok<ASGLU>

IfrDirSrcMexOpenMP = fullfile(IfrTbx, 'ifremer', 'extern', 'Mex-CUDA-OpenMP');
IfrDirOptMexOpenMP = IfrDirSrcMexOpenMP;
IfrDirDllMexOpenMP = fullfile(IfrDirSrcMexOpenMP, 'dll');

% Depuis R2016a
nameMexOpts = 'mex_C++_win64.xml'; % 'mexopts.bat"';

pathMexOpts = fullfile(IfrDirOptMexOpenMP, 'compil_options', nameMexOpts);

% Construction de la liste des fichiers � compiler
NameSources = {};
if ~strcmp(filesource, 'all')
    NameSources = {};
    for i=1:numel(filesource)
        NameSources{i} = fullfile(IfrDirSrcMexOpenMP, cell2str(filesource(i)));   %#ok<AGROW>
    end
else
    FileSources = dir( fullfile(IfrDirSrcMexOpenMP, '*mex*.cpp'));
    for i=1:numel(FileSources)
        NameSources{i} = fullfile(IfrDirSrcMexOpenMP, FileSources(i).name); %#ok<AGROW>
    end
end

if nargin ~= 0
    if isempty(NameSources)
        errordlg(Lang('Aucun fichier source trouv�, v�rifier le contenu du r�pertoire','None source files found. Check the content of the directory'),'Erreur');
    end
    %Compilation successives des diff�rentes fonctions
    for i=1:numel (NameSources)
        % cmd = ['mex ' option ' -f ' pathMexOpts ' ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS -openmp''  LDOPTIMFLAGS="$LDOPTIMFLAGS -fopenmp" ' ' -outdir ' IfrDirDllMexOpenMP];
        cmd = ['mex ' option ' -f ' pathMexOpts ' -liomp5md -L"' matlabroot '\bin\win64" ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS ''  LDOPTIMFLAGS="$LDOPTIMFLAGS" ' ' -outdir ' IfrDirDllMexOpenMP];
        eval(cmd);
    end 
end
