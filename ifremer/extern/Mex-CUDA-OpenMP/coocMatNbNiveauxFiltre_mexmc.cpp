#include "mex.h"
#include <math.h>
#include <omp.h>
#define AFF 0


/* [Cmat] = coocMatNbNiveauxFiltre_mexmc(Imagette, Tx, Ty, nbNiveaux, nbCores)
	Imagette est une matrice d'entiers (uint8)
	Tx, Ty et nbNiveaux sont des entiers
	Cmat est de type single
*/

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
 /******************************************************************************************
	RECUPERATION DES ARGUMENTS
 ******************************************************************************************/
 
 int paspxthread, nbCores;
 int Tx, Ty, nbNiveaux, N, M;
 unsigned char *Imagette;
 float *Cmat, *CmatF;
 /*unsigned int hTimer;
 double gpuTimer;
 cutCreateTimer(&hTimer);*/

 // initialisation :
 /*cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 if(nrhs != 5) mexErrMsgTxt("coocMatNbNiveauxFiltre_mexmc requiert 5 arguments");
 if(nlhs != 1) mexErrMsgTxt("coocMatNbNiveauxFiltre_mexmc requiert un argument en sortie");

 mxClassID classID_mat_entree = mxGetClassID(prhs[0]);
 if(classID_mat_entree != mxUINT8_CLASS) mexErrMsgTxt("La matrice d'entr�e doit �tre de type uint8");

 // r�cup�ration des arguments :
 M = mxGetM(prhs[0]); // dimensions de la matrice image
 N = mxGetN(prhs[0]);

 Imagette = (unsigned char*) mxGetData(prhs[0]);
 
 Tx = mxGetScalar(prhs[1]);
 Ty = mxGetScalar(prhs[2]);
 nbNiveaux = mxGetScalar(prhs[3]); // tester si nbNiveaux est bien uint8
 nbCores = mxGetScalar(prhs[4]);
 
 mwSize dims[2];
 dims[0] = nbNiveaux;
 dims[1] = nbNiveaux;
 /*plhs[0] = mxCreateNumericArray(2, dims, mxSINGLE_CLASS, mxREAL);
 Cmat = (float*)mxGetPr(plhs[0]);
 // pour virer Cmat des sorties, Cmat = (float*)mxCreateNumericArray fait tout planter
 plhs[1] = mxCreateNumericArray(2, dims, mxSINGLE_CLASS, mxREAL);
 CmatF = (float*)mxGetPr(plhs[1]);*/

 Cmat = (float*)malloc(sizeof(float*) * nbNiveaux * nbNiveaux);
 plhs[0] = mxCreateNumericArray(2, dims, mxSINGLE_CLASS, mxREAL);
 CmatF = (float*)mxGetPr(plhs[0]);

 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("temps init : %f\n", gpuTimer);
 
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 
 // matrice de cooccurrence Cmat :
 for(int i = 0; i < nbNiveaux * nbNiveaux; i++) Cmat[i] = 0;
 

 /******************************************************************************************
	calcul de nbtr et moyenne
 ******************************************************************************************/
 

 float moyenne = 0;
 int nbtr = 0;
 float*moyenne_par_cpu = (float*)malloc(sizeof(float) * nbCores);
 int*nbtr_par_cpu = (int*)malloc(sizeof(int) * nbCores);
 for(int i = 0; i < nbCores; i++) {moyenne_par_cpu[i] = 0; nbtr_par_cpu[i] = 0;};
 
 int cpu_id;
 #pragma omp parallel private(cpu_id)
 { 
	nbCores = omp_get_num_threads();
	paspxthread = (int)((M - Ty) / nbCores);
  if(cpu_id == 0) // seulement un CPU
  {
	if(omp_get_num_threads() != nbCores)
	{
		printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
		printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
		mexErrMsgTxt("coocMatNbNiveauxKullbackAnglesSortSegmDist_mexmc refuse de continuer");
	} 
  }
  cpu_id = omp_get_thread_num();
  int stop = (cpu_id + 1) * paspxthread;
  if(cpu_id == nbCores - 1) stop = M - Ty;
  for(int i = cpu_id * paspxthread; i < stop; i++) for(int j = 0; j < N - Tx; j++) if((Imagette[i + j * M] != 0) && (Imagette[i + Ty + (j + Tx) * M] != 0)) 
  {
	nbtr_par_cpu[cpu_id]++;
	moyenne_par_cpu[cpu_id] += Imagette[i + j * M];
  }
 }
 //retour en monocore :
 for(int i = 0; i < nbCores; i++)
 {
	moyenne += moyenne_par_cpu[i]; 
	nbtr += nbtr_par_cpu[i];
 }	
 moyenne /= nbtr;
 
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("moyenne + nbtr : %f\n", gpuTimer);
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/

 if(nbtr == 0) return; // return Cmat remplie de zeros
  
 /******************************************************************************************
	calcul de la matrice de cooccurrence Cmat
 ******************************************************************************************/

 float inc = 1 / (float)nbtr;

 #pragma omp parallel private(cpu_id)
 { 
  cpu_id = omp_get_thread_num();
  nbCores = omp_get_num_threads();
  paspxthread = (int)((M - Ty) / nbCores);

  int stop = (cpu_id + 1) * paspxthread;
  if(cpu_id == nbCores - 1) stop = M - Ty;
  
  for(int i = cpu_id * paspxthread; i < stop; i++) 
	for(int j = 0; j < N - Tx; j++) 
		if((Imagette[i + j * M] > 0) && (Imagette[i + Ty + (j + Tx) * M] > 0)) 
			#pragma omp atomic
			Cmat[Imagette[i + j * M] - 1 + (Imagette[i + Ty + (j + Tx) * M] - 1) * nbNiveaux] += inc;
 }
 
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("Cmat : %f\n", gpuTimer);
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/

 /******************************************************************************************
	standard deviation s
 ******************************************************************************************/

 float s = 0;
  float *s_par_cpu = (float*)malloc(sizeof(float) * nbCores);
  for(int i = 0; i < nbCores; i++) s_par_cpu[i] = 0;

 #pragma omp parallel private(cpu_id)
 { 
  cpu_id = omp_get_thread_num();
  nbCores = omp_get_num_threads();
  paspxthread = (int)((M - Ty) / nbCores);

  int stop = (cpu_id + 1) * paspxthread;
  if(cpu_id == nbCores - 1) stop = M - Ty;
  for(int ii = cpu_id * paspxthread; ii < stop; ii++) for(int jj = 0; jj < N - Tx; jj++)
  {
			if((Imagette[ii + jj * M] != 0) && (Imagette[ii + Ty + (jj + Tx) * M] != 0))
			{
				s_par_cpu[cpu_id] += (Imagette[ii + jj * M] - moyenne) * (Imagette[ii + jj * M] - moyenne);
			}	   
  }
 }
 // retour en monocore :
 for(int i = 0; i < nbCores; i++) s +=s_par_cpu[i];

 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("s : %f\n", gpuTimer);

 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
  s /= nbtr;
  s = sqrtf(s);
 /******************************************************************************************
	largeur du filtre
 ******************************************************************************************/

 float truc = 0.5 * logf(227850 / (float)nbtr) * sqrtf(s / (float)14.35) / logf(float(2));
  float sigma;
  if(truc > 0.1) sigma = truc;
  else sigma = 0.1;
  int largeurFiltre = 1 + 2 * (int)(2 * sigma);
  //printf("nbNiveaux : %d taille_imagettes %d nbtr : %d moyenne : %f s : %f sigma : %f largeurFiltre : %d\n", nbNiveaux, taille_fenetre, nbtr, moyenne, s, sigma, largeurFiltre);
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("largeurFiltre : %d temps : %f\n", largeurFiltre, gpuTimer);
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 

 
 
 /******************************************************************************************
	creation du filtre gaussien 2D h
 ******************************************************************************************/

  float*h;
  h = (float*)malloc(sizeof(float) * largeurFiltre * largeurFiltre);
  float sumh = 0;
  for(int x = 0; x < largeurFiltre; x++)
  {
 	for(int y = 0; y < largeurFiltre; y++)
	{
		h[x + y * largeurFiltre] = expf(-((x+1 - (float)(largeurFiltre + 1) / 2) * (x+1 - (float)(largeurFiltre + 1) / 2) + (y+1 - (float)(largeurFiltre + 1) / 2) * (y+1 - (float)(largeurFiltre + 1) / 2)) / (float)(2 * sigma * sigma));
		sumh += h[x + y * largeurFiltre];
	}
  }
  for(int x = 0; x < largeurFiltre; x++)
  {
	for(int y = 0; y < largeurFiltre; y++)
	{
		h[x + y * largeurFiltre] /= sumh;
	}
  }
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("h : %f\n", gpuTimer);
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/

  /*for(int ii = 0; ii <= largeurFiltre - 1; ii++) //pour verifier h
  {
   for(int jj = 0; jj <= largeurFiltre - 1; jj++)
   {
	CmatF[ii + jj * nbNiveaux] = h[ii + jj * largeurFiltre];
   }
  }*/

 /******************************************************************************************
	produit de convolution 2D : CmatF = Cmat * h
 ******************************************************************************************/

  int kernelradius = (int)(2*sigma);
  int dx, dy; float sommediv; int kx, ky, x, y;
  for(x = kernelradius; x < nbNiveaux - kernelradius; x++)
  {
    for(y = kernelradius; y < nbNiveaux - kernelradius; y++)
	{
	 if(Cmat[x + y * nbNiveaux] != 0)
	 {
      sommediv = 0;
  	  for( kx = -kernelradius; kx <= kernelradius; kx++)
	  {
		for( ky = -kernelradius; ky <= kernelradius; ky++)
		{
			dx = x + kx;
			dy = y + ky;
    		 CmatF[dx + dy * nbNiveaux] += Cmat[x + y * nbNiveaux] * h[kernelradius + kx + (kernelradius + ky) * largeurFiltre]; 
			 sommediv += h[kernelradius + kx + (kernelradius + ky) * largeurFiltre];
		}
	   }	
 	  CmatF[x + y * nbNiveaux] /= sommediv;
	  }
	}
  }
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("convolution : %f\n", gpuTimer);
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 
 free(Cmat);
 free(h);
 free(s_par_cpu);
 free(nbtr_par_cpu);
 free(moyenne_par_cpu);
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 if(AFF) printf("free mem : %f\n", gpuTimer);*/
}


