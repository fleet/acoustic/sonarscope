//            [ImageF] = fillNaN_mean_double_mexmc(Image, [windowl  windowc], nbCores);

//				Image et ImageF sont de type double


#include "mex.h"
#include <vector>
#include <cmath>
#include <omp.h>

double *Image, *ImageF;
int windowl, windowc, M, N, wl, wc, paspxthread, nbCores;

struct stParam
{
 int cpu_id;
};

void thRecopie(void*p)
{
 stParam *param = (stParam*)p;
 int cpu_id = param->cpu_id;
 int stop = (cpu_id + 1) * paspxthread;
 if(cpu_id == nbCores - 1) stop = M;
 for(int i = cpu_id * paspxthread; i < stop; i++) 
     for(int j = 0; j < N; j++) 
         ImageF[i + j * M] = Image[i + j * M];

 
 register double nbpx;
 register double somme;
 int start = cpu_id * paspxthread;
 if(cpu_id == 0) start = wl;
 if(cpu_id == nbCores - 1) stop = M - wl;
 for(int il = start; il < stop; il++)
	for(int ic = wc; ic < N - wc; ic++)
	{
		if(std::isnan(Image[il + ic * M]))
		{
			nbpx = 0;
			for(int subl = il - wl; subl <= il + wl; subl++) 
                for(int subc = ic - wc; subc <= ic + wc; subc++) 
                    if(!(std::isnan(Image[subl + subc * M]))) 
                        nbpx ++;
			if(nbpx > 0)
			{
				somme = 0;
				for(int subl = il - wl; subl <= il + wl; subl++) 
                    for(int subc = ic - wc; subc <= ic + wc; subc++) 
                        if(!(std::isnan(Image[subl + subc * M]))) 
                            somme += Image[subl + subc * M];
				ImageF[il + ic * M] = somme / nbpx;
			}	
		}
	}
}



void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
 if(nrhs != 3) mexErrMsgTxt("fillNaN_mean_mexmc requiert 3 arguments");
 if(nlhs != 1) mexErrMsgTxt("fillNaN_mean_mexmc requiert 1 argument en sortie");
 
 #include "mex.h"
 
 Image = (double*) mxGetData(prhs[0]);
 M = mxGetM(prhs[0]);
 N = mxGetN(prhs[0]);
 mwSize dims[2];
 dims[0] = M;
 dims[1] = N;

 double *sizes;
 sizes = (double*) mxGetData(prhs[1]);
 windowl = (int)sizes[0]; 
 windowc = (int)sizes[1]; 
 nbCores = mxGetScalar(prhs[2]);

 plhs[0] = mxCreateDoubleMatrix(M, N, mxREAL);
 ImageF = (double*)mxGetPr(plhs[0]);
 
 wl = (int)(windowl / 2);
 wc = (int)(windowc / 2);
 std::vector<stParam> threadData(nbCores);
 #pragma omp parallel
 {
  int cpu_id = omp_get_thread_num();
  nbCores = omp_get_num_threads();
  paspxthread = (int)(M / nbCores);

  if(cpu_id == 0) // seulement un CPU
  {
	if(omp_get_num_threads() != nbCores)
	{
		printf("OpenMP est configuré pour utiliser %d CPU(s)\n", omp_get_num_threads());
		printf("%d CPU(s) sont installés sur cet ordinateur\n", omp_get_max_threads());
		mexErrMsgTxt("fillNaN_mexmc8 refuse de continuer");
	} 
  }
  threadData[cpu_id].cpu_id = cpu_id;
  thRecopie(&threadData[cpu_id]);
 }
}


// solution avec Imdilate dans matlab et 3e argument
/*  for(int il = wl; il < M - wl; il++)
	for(int ic = wc; ic < N - wc; ic++)
	{
		if(_isnan(Image[il + ic * M]) * ImageD[il + ic * M])
		{
			// pt virer le test de la nbpx ...
			nbpx = 0;
			for(int subl = il - wl; subl <= il + wl; subl++) for(int subc = ic - wc; subc <= ic + wc; subc++) if(!(_isnan(Image[subl + subc * M]))) nbpx ++;
			if(nbpx == 0) break;			
			//printf("%f\n", nbpx);
			mean = 0;
			for(int subl = il - wl; subl <= il + wl; subl++) for(int subc = ic - wc; subc <= ic + wc; subc++) if(!(_isnan(Image[subl + subc * M]))) mean += Image[subl + subc * M];
			mean /= nbpx;
			printf("%f\t%f\n", nbpx, mean);
			ImageF[il + ic * M] = mean;
		}
	}
 */
