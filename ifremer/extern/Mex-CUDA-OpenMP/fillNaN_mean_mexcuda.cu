//            [ImageF] = fillNaN_mean_mexcuda(Image, [windowl  windowc], nbCores);




#include "mex.h"
#include "cuda.h"
#include "cutil.h"
#include <stdarg.h>

bool cudamexCheckError(const char *errorMessage, ...);


__global__ void kernelFillNaN_mean(float* Image_gpu, float* ImageF_gpu, int M, int N, int wl, int wc)
{
 unsigned int i = __umul24(blockIdx.x,blockDim.x) + threadIdx.x;
 unsigned int j = __umul24(blockIdx.y,blockDim.y) + threadIdx.y;

 // on recopie l'image dans ImageF
 if((i < M) && (j < N))
 {
  //ImageF_gpu[thread] = 4;
  ImageF_gpu[i + __umul24(j, M)] = Image_gpu[i + __umul24(j, M)];
 }
 
 if((i < M - wl) && (i > wl) && (j < N - wc) && (j > wc))
 {
  float nbpx;
  float somme;
		if(isnan(Image_gpu[i + __umul24(j, M)]))
		{
			nbpx = 0;
			
//			for(int subl = i - wl; subl <= i + wl; subl++) 
//				for(int subc = j - wc; subc <= j + wc; subc++) 
//					if(!(isnan(Image_gpu[subl + __umul24(subc, M)]))) 
			for(int u = __umul24(2, wl); u; u--) 
				for(int v = __umul24(2, wc); v; v--) 
					if(!(isnan(Image_gpu[i - wl + u + __umul24(j - wc + v, M)]))) 
						nbpx++;
			if(nbpx > 0)
			{
				somme = 0;
				
				for(int u = __umul24(2, wl); u; u--) 	
					for(int v = __umul24(2, wc); v; v--)
						if(!(isnan(Image_gpu[i - wl + u + __umul24(j - wc + v, M)]))) 
							somme = __fadd_rn(somme, Image_gpu[i - wl + u + __umul24(j - wc + v, M)]);
				ImageF_gpu[i + __umul24(j, M)] = __fdividef(somme, nbpx);
			}	
		}
 }	
}



void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
 /******************************************************************************************
	RECUPERATION DES ARGUMENTS
 ******************************************************************************************/
 
 float *Image, *ImageF, *Image_gpu, *ImageF_gpu;
 int windowl, windowc, M, N, wl, wc;
 /*unsigned int hTimer;
 double gpuTimer;
 cutCreateTimer(&hTimer);
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 if(nrhs != 2) mexErrMsgTxt("fillNaN_mean_mexcuda requiert 2 arguments");
 if(nlhs != 1) mexErrMsgTxt("fillNaN_mean_mexcuda requiert 1 argument en sortie");
 Image = (float*) mxGetData(prhs[0]);
 M = mxGetM(prhs[0]);
 N = mxGetN(prhs[0]);
 int dims[2];
 dims[0] = M;
 dims[1] = N;
 
 double *sizes;
 sizes = (double*) mxGetData(prhs[1]);
 windowl = (int)sizes[0]; 
 windowc = (int)sizes[1]; 

 plhs[0] = mxCreateNumericArray(2, dims, mxSINGLE_CLASS, mxREAL);
 ImageF = (float*)mxGetPr(plhs[0]);
 
 wl = (int)(windowl / 2);
 wc = (int)(windowc / 2);
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 printf("temps arg : %f\n", gpuTimer);
 
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 // allocation de la memoire GPU :
 int size = sizeof(float) * M * N;
 cudaMalloc((void **) &Image_gpu, size);
    cudamexCheckError("cudaMalloc failed for Image_gpu allocating %d bytes", size);
 cudaMalloc((void **) &ImageF_gpu, size);
    cudamexCheckError("cudaMalloc failed for ImageF_gpu allocating %d bytes", size);
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 printf("temps alloc : %f\n", gpuTimer);
 
 // envoi de l'image vers la memoire GPU :
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 cudaMemcpy(Image_gpu, Image, size, cudaMemcpyHostToDevice);
 cudamexCheckError("cudaMemcpy [Host->GPU] failed copying %d bytes", size);
 //cudaMemcpy(ImageF_gpu, Image, size, cudaMemcpyHostToDevice);
 //cudamexCheckError("cudaMemcpy [Host->GPU] failed copying %d bytes", size);
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 printf("temps ->gpu : %f\n", gpuTimer);
 
 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 dim3 dimBlock(16, 16); // max : 512 threads par block sur la Quadro FX 3800
 dim3 dimGrid(M / 16, N / 16);
 if(M % dimBlock.x != 0) dimGrid.x++;
 if(N % dimBlock.y != 0) dimGrid.y++;

 kernelFillNaN_mean<<<dimGrid, dimBlock>>>(Image_gpu, ImageF_gpu, M, N, wl, wc);
 cudamexCheckError("kernel execution failed");
 //cudaThreadSynchronize();
  /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 printf("temps fillNaN : %f\n", gpuTimer);

 cutResetTimer(hTimer);
 cutStartTimer(hTimer);*/
 //cudaMemcpyAsync(ImageF, ImageF_gpu, size, cudaMemcpyDeviceToHost, 0);
 cudaMemcpy(ImageF, ImageF_gpu, size, cudaMemcpyDeviceToHost);
 cudamexCheckError("cudaMemcpyAsync [GPU->Host] failed copying %d bytes", size);
 cudaFree(Image_gpu);
 cudamexCheckError("cudaFree failed while freeing Image_gpu");
 cudaFree(ImageF_gpu);
 cudamexCheckError("cudaFree failed while freeing ImageF_gpu");
 /*cutStopTimer(hTimer);
 gpuTimer = cutGetTimerValue(hTimer);
 printf("temps gpu-> : %f\n", gpuTimer);*/
}


// CUDAMEXCHECKERROR issues an error in MATLAB if a CUDA error is detected
bool cudamexCheckError(const char *errorMessage, ...)
{
    bool    result = true;
    char   *buf    = NULL;
    size_t  len    = 0;  
    
    va_list argp;

    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess)
        result = false;
    else if ((err = cudaThreadSynchronize()) != cudaSuccess)
        result = false;
    else
        result = true;

    if (!result)
    {
        // Allocate a reasonable amount of memory to hold the string
        len = 2*strlen(errorMessage);
        buf = (char*)mxMalloc(len);
        va_start(argp, errorMessage);
        vsnprintf(buf,len,errorMessage,argp);
        va_end(argp);
        mexErrMsgIdAndTxt("CUDA:Failure",
                          "Cuda error \'%s\': %s.\n",
                          buf,
                          cudaGetErrorString(err) );
        mxFree((void*)buf);
    }    
    return (result);
}


