// Mex File appelant l'API Windows GetDiskFreeSpaceEx pour d�tecter lesplaces Totales et libres
// d'un lecteur de disque
//
// INPUT :
//   Disk : nom du lecteur ou de la partition de disque
//
// OUTPUT :
//   T    : taille totale du disuque en m�ga-octets.
//   F    : taille disponible du disuque en m�ga-octets.
//
// SYNTAX :
//   [T, F] =  getDiskFreeSpace_mex(Disk);
//
// COMPILATION :
//	pour Windows 32
//	compilALL_mexOpenMP('platform','Win32','option', '-v', 'filesource',{'getDiskFreeSpace_mex.cpp'});
//	pour Windows 64
//	compilALL_mexOpenMP('platform','Win64','option', '-v', 'filesource',{'getDiskFreeSpace_mex.cpp'});
//
// EXAMPLES :
//   [t, f] = getDiskFreeSpace_mex('C:');
//
// SEE ALSO : Authors
// AUTHORS  : LECOQ + GLU
//-------------------------------------------------------------------------------
#include "mex.h"
#include <windows.h>
#include <stdio.h>


typedef BOOL (WINAPI *P_GDFSE)(LPCTSTR, PULARGE_INTEGER,
        PULARGE_INTEGER, PULARGE_INTEGER);

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    BOOL  bResult;
    
    unsigned __int64    i64FreeBytesToCaller,
                        i64TotalBytes,
                        i64FreeBytes;
    
    WCHAR  *pszDrive  = NULL;
    
    char    *input_buf,
            cDummy[500];
   
    int     nInput  = 1,
            nOutput = 2;
    
    DWORD   dwSectPerClust,
            dwBytesPerSect,
            dwFreeClusters,
            dwTotalClusters;
    
    
    // input must be a string.
    if ( mxIsChar(prhs[0]) != 1)
        mexErrMsgIdAndTxt( "MATLAB:getDiskDFreeSpace:inputNotString",
                "Input must be a string.");
    
    // copy the string data from prhs[0] into a C string input_ buf.
    input_buf = mxArrayToString(prhs[0]);
    
    if(input_buf == NULL)
        mexErrMsgIdAndTxt( "MATLAB:revord:conversionFailed",
                "Could not convert input to string.");
    
    if(nrhs != nInput)
    {
        sprintf(cDummy, "my_reduce_mexmc requiert %d arguments", nInput);
        mexErrMsgTxt(cDummy);
    }
    
    if(nlhs != nOutput)
    {
        sprintf(cDummy, "my_reduce_mexmc requiert %d arguments", nOutput);
        mexErrMsgTxt(cDummy);    
    }
    
    // Appel de l'API Windows valable en 32 et 64 bits. 
    bResult = GetDiskFreeSpaceEx (input_buf,
            (PULARGE_INTEGER)&i64FreeBytesToCaller,
            (PULARGE_INTEGER)&i64TotalBytes,
            (PULARGE_INTEGER)&i64FreeBytes);
    
//     if (bResult)
//     {
//         printf ("\n\nGetDiskFreeSpaceEx reports\n\n");
//         printf ("Available space to caller = %I64u MB\n",
//                 i64FreeBytesToCaller / (1024*1024));
//         printf ("Total space               = %I64u MB\n",
//                 i64TotalBytes / (1024*1024));
//         printf ("Free space on drive       = %I64u MB\n",
//                 i64FreeBytes / (1024*1024));
//     }
    
    plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
    plhs[1] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
   	// plhs[2] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
    
    //i64FreeBytesToCaller   = (unsigned __int64) mxGetData(plhs[0]);
    // Assigne les variables de sorties par les tailles en Mo.
    *mxGetPr(plhs[0])       = (double)(i64TotalBytes/(1024*1024));
    *mxGetPr(plhs[1])       = (double)(i64FreeBytes/(1024*1024));
    
    mxFree(input_buf);
    
    
}

