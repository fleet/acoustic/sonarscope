/*
 * dY2 = interp1Linear_mex(dX1, dY1, dX2)
 *
 * il ne doit pas y avoir de NaN dans dX1, dY1 et dX2
 * dX1, dY1 et dX2 sont de type double
 *
 * dX1 et dY1 doivent etre de meme dimension
 *
 * dX1 doit etre croissants ou decroissants
 * dX2 aussi
 */



#include "mex.h"
#include <omp.h>
#include <float.h>
#include <limits>
int DimMax(int M, int N)
{
    if(M > N) return M;
    else return N;
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    double  *dX1,
            *dY1,
            *dX2,
            *dY2,
            dDiffavant,
            dDiffapres;

    mwSize  dims[2];
    
    int     iDimdX1,
            iDimdX2,
            iDimdY1,
            iLoop,
            jLoop;
    
    bool bFlagNaN;;

    char    cMsg[500];

    float fNan = std::numeric_limits<float>::quiet_NaN(); //produces 1.#QNAN
    
    if(nrhs != 3) mexErrMsgTxt("interp1Linear_mex requiert 3 arguments");
    if(nlhs != 1) mexErrMsgTxt("interp1Linear_mex requiert un argument en sortie");
    
    // V�rification des param�tres d'entr�es.
    for(jLoop = 0; jLoop < 2; jLoop++)
    {
        mxClassID cid = mxGetClassID(prhs[jLoop]);
        if(cid != mxDOUBLE_CLASS)
        {
            sprintf(cMsg, "L'argument %d doit etre de type double\n.", jLoop);
            mexErrMsgTxt(cMsg);
        }
    }
    
    // Identification des dimensions et des variables d'entr�es.
    iDimdX1 = DimMax((int)mxGetM(prhs[0]), (int)mxGetN(prhs[0]));
    iDimdY1 = DimMax((int)mxGetM(prhs[1]), (int)mxGetN(prhs[1]));
    iDimdX2 = DimMax((int)mxGetM(prhs[2]), (int)mxGetN(prhs[2]));
    
    dX1     = (double*)mxGetData(prhs[0]);
    dY1     = (double*)mxGetData(prhs[1]);
    dX2     = (double*)mxGetData(prhs[2]);
    //printf("%f\n", dX2[0]);
    
    dims[0] = (int)mxGetM(prhs[2]);
    dims[1] = (int)mxGetN(prhs[2]);
    plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
    dY2     = (double*)mxGetPr(plhs[0]);
    

    // TODO : GLU le 09/09/2011, traiter les bornes plut�t selon les Min et Max que [0, end].
    // Pour l'instant, la fonction accepte un dX1 continue.
    if (dX1[0] < dX1[1])
    {
        if (dX2[0] < dX2[1])
        { // cas dX1 et dX2 croissants
            int istart = 1; // on commence a 1 a cause du dX1[iLoop-1]
            for(jLoop = 0; jLoop < iDimdX2; jLoop++)
            {
                bFlagNaN = true;
                // Si la valeur � interpoler est comprise dans l'ensemble des abscisses connues.
                if (dX2[jLoop] >= dX1[0] && dX2[jLoop] <= dX1[iDimdX1-1])
                {
                    for(iLoop = istart; iLoop < iDimdX1; iLoop++)
                    {
                        if(dX1[iLoop] >= dX2[jLoop])
                        {
                            dDiffapres = dX1[iLoop] - dX2[jLoop];
                            dDiffavant = dX2[jLoop] - dX1[iLoop - 1]; // les dDiffavant et apres sont forc�ment positives
                            dY2[jLoop] = (dY1[iLoop - 1] * dDiffapres + dY1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                            bFlagNaN=false;
                            break;
                        }
                    } // Fin de la boucle sur iLioop, dX1
                }     
                // Valeur d'interpolation non rencontr�e --> NaN
                if (bFlagNaN==true)
                {
                    dY2[jLoop] = (double)fNan;
                }
             } // Fin de la boucle sur jLoop, dX2
        }
        else
        { // cas dX1 croissant et dX2 decroissant
            int istart = 1; // on commence a 1 a cause du dX1[iLoop-1]
            for(jLoop = iDimdX2 - 1; jLoop >= 0; jLoop--)
            {
                bFlagNaN = true;
                // Si la valeur � interpoler est comprise dans l'ensemble des abscisses connues.
                if (dX2[jLoop] >= dX1[0] && dX2[jLoop] <= dX1[iDimdX1-1])
                {
                    for(iLoop = istart; iLoop < iDimdX1; iLoop++)
                    {
                        if(dX1[iLoop] >= dX2[jLoop])
                        {
                            dDiffapres = dX1[iLoop] - dX2[jLoop];
                            dDiffavant = dX2[jLoop] - dX1[iLoop - 1]; // les dDiffavant et apres sont forc�ment positives
                            dY2[jLoop] = (dY1[iLoop - 1] * dDiffapres + dY1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                            bFlagNaN=false;
                            break;
                        }
                    } // Fin de la boucle sur iLoop, dX1
                }     
                // Valeur d'interpolation non rencontr�e --> NaN
                if (bFlagNaN==true)
                {
                    dY2[jLoop] = (double)fNan;
                }
            } // Fin de la boucle sur jLoop, dX2
        }
    }
    else
    {
        if(dX2[0] < dX2[1])
        { // cas dX1 d�croissant et dX2 croissant
            int istart = iDimdX1 - 2;
            for(jLoop = 0; jLoop < iDimdX2; jLoop++)
            {
                bFlagNaN = true;
                // Si la valeur � interpoler est comprise dans l'ensemble des abscisses connues.
                if (dX2[jLoop] >= dX1[iDimdX1-1] && dX2[jLoop] <= dX1[0])
                {
                    for(iLoop = istart; iLoop >= 0; iLoop--)
                    {
                        if(dX1[iLoop] >= dX2[jLoop])
                        {
                            dDiffapres = dX1[iLoop] - dX2[jLoop];
                            dDiffavant = dX2[jLoop] - dX1[iLoop+1]; // les dDiffavant et apres sont forc�ment positives
                            dY2[jLoop] = (dY1[iLoop + 1] * dDiffapres + dY1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                            bFlagNaN=false;
                            break;
                        }
                    } // Fin de la boucle sur iLoop, dX1
                }     
                // Valeur d'interpolation non rencontr�e --> NaN
                if (bFlagNaN==true)
                {
                    dY2[jLoop] = (double)fNan;
                }
            } // Fin de la boucle sur jLoop, dX2
        }
        else
        { // cas dX1 dX2 decroissants
            int istart = iDimdX1 - 2;
            for(jLoop = iDimdX2 - 1; jLoop >= 0; jLoop--)
            {
                bFlagNaN = true;
                // Si la valeur � interpoler est comprise dans l'ensemble des abscisses connues.
                if (dX2[jLoop] >= dX1[iDimdX1-1] && dX2[jLoop] <= dX1[0])
                {
                    for(iLoop = istart; iLoop >= 0; iLoop--)
                    {
                        if(dX1[iLoop] >= dX2[jLoop])
                        {
                            dDiffapres = dX1[iLoop] - dX2[jLoop];
                            dDiffavant = dX2[jLoop] - dX1[iLoop+1]; // les dDiffavant et apres sont forc�ment positives
                            dY2[jLoop] = (dY1[iLoop + 1] * dDiffapres + dY1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                            bFlagNaN=false;
                            break;
                        }
                    } // Fin de la boucle sur iLoop, dX1
                }     
                // Valeur d'interpolation non rencontr�e --> NaN
                if (bFlagNaN==true)
                {
                    dY2[jLoop] = (double)fNan;
                }
            } // Fin de la boucle sur jLoop, dX2

        } // Fin du test (dX2[0] < dX2[1])

    } // Fin du test (dX1[0] < dX1[1])
}
