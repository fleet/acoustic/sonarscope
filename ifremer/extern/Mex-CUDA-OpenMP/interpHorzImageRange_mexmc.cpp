#include "mex.h"
#include <vector>
#include <omp.h>
#include <float.h>
#include <string.h>
#include <matrix.h>
#include <math.h>
#include <stdlib.h>

//# define _isinf(a) ((a) == mxGetInf() || (a) == -mxGetInf())
//# define _isnan(a) ((a) != (a))
//# define _isfinite(a) !((a) != (a) || (a) == mxGetInf() || (a) == -mxGetInf())
#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)


float *Image, *ImageF, *ImageF2, *ImageE;
int     M, N,
        paspxthread,
        nbCores,
        typeInterp,
        Range;

struct stParam
{
    int cpu_id;
};

// Recherche de min et max d'un tableau
void findMinMaxFromTab (int *tab, int nb, int *iPtrMin, int *iPtrMax)
{
	int iValMin, iValMax;
	int i;
	iValMin = tab[0];
	iValMax = tab[0];
	for (i=0; i<nb; i++)
	{
		if (tab[i] < iValMin)
		{
			iValMin = tab[i];
		}
		else if (tab[i] > iValMax)
		{
			iValMax = tab[i];
		}
	}
	*iPtrMin = iValMin;
	*iPtrMax = iValMax;
}

void imDilateErodeC(float *img2Process, int ligneNum, int iRange)
{
    int     iColNotNaN=0;
    float   *fValPix;
    int     *iMaskDilate = NULL, 
            *iMaskErode = NULL, 
            idxB, idxE,
            iValMaskMin, iValMaskMax;
    
    // Cr�ation du masque glissant de dilatation
    iMaskDilate = (int*)calloc(iRange*2+1, sizeof(int));
    iMaskErode  = (int*)calloc(iRange*2+1, sizeof(int));
    for(int colNum=0; colNum<N; colNum++)
    {
        // Traitement de d�but du buffer
        for (int iIdx=-iRange; iIdx<=iRange; iIdx++)
        {
            // iMaskErode[iIdx+iRange] == (int) 1;
            if ((colNum + iIdx < 0) || (colNum + iIdx) >= N)
            {
                iMaskDilate[iIdx+iRange] == (int) 1;
                // iMaskErode[iIdx+iRange] == (int) 1;
            }
            else
            {
                // On ne teste le pixel du masque que si celui-ci n'a pas �t� d�j� affect�.
                // iMaskDilate[iIdx+iRange] == (Image[colNum*M + (ligneNum+iIdx)]== 0)? (int)0 : (int)1; // (int)!mxIsNaN(Image[colNum*M + (ligneNum+iIdx)]);
                if (img2Process[(colNum+iIdx)*M + ligneNum] ==0)
                {
                    iMaskDilate[iIdx+iRange] = (int)0;
                    // iMaskErode[iIdx+iRange] = (int)0;
                }
                else
                {
                    iMaskDilate[iIdx+iRange] = (int)1;
                    // iMaskErode[iIdx+iRange] = (int)1;
                }
            }
            if (colNum > 42 && colNum < 55)
            {   
                //printf("ligne(%d)-col(%d) / iMaskDilate[%d] = %d\n", ligneNum, colNum, iIdx+iRange, iMaskDilate[iIdx+iRange]);
                printf("ligne(%d)-col(%d) / iMaskErode[%d] = %d\n", ligneNum, colNum, iIdx+iRange, iMaskErode[iIdx+iRange]);
            }
        }
        // D�termination du niveau haut et bas du masque.
        findMinMaxFromTab (iMaskDilate, iRange*2+1, &iValMaskMin, &iValMaskMax);
        //printf("ligne(%d)-col(%d) / idx(%d) = %d\n", ligneNum, colNum, colNum*M + ligneNum, iValMaskMax);
        ImageF[colNum*M + ligneNum] = float(iValMaskMax);
        // findMinMaxFromTab (iMaskErode, iRange*2+1, &iValMaskMin, &iValMaskMax);
        ImageF2[colNum*M + ligneNum] = float(iValMaskMin);

    }
    free(iMaskDilate);
    free(iMaskErode);
}

void imDilateC(float *img2Process, int ligneNum, int iRange)
{
    int     iColNotNaN=0;
    float   *fValPix;
    int     *iMaskDilate = NULL, 
            idxB, idxE,
            iValMaskMin, iValMaskMax;
    
    // Cr�ation du masque glissant de dilatation
    iMaskDilate = (int*)calloc(iRange*2+1, sizeof(int));
    for(int colNum=0; colNum<N; colNum++)
    {
        // Traitement de d�but du buffer
        for (int iIdx=-iRange; iIdx<=iRange; iIdx++)
        {
            if ((colNum + iIdx < 0) || (colNum + iIdx >= N))
            {
                iMaskDilate[iIdx+iRange] == (int) 1;
            }
            else
            {
                // On ne teste le pixel du masque que si celui-ci n'a pas �t� d�j� affect�.
                // iMaskDilate[iIdx+iRange] == (Image[colNum*M + (ligneNum+iIdx)]== 0)? (int)0 : (int)1; // (int)!mxIsNaN(Image[colNum*M + (ligneNum+iIdx)]);
                if (img2Process[(colNum+iIdx)*M + ligneNum] ==0)
                {
                    iMaskDilate[iIdx+iRange] = (int)0;
                }
                else
                {
                    iMaskDilate[iIdx+iRange] = (int)1;
                }
            }
            if (colNum > 1 && colNum < 5)
                printf("ligne(%d)-col(%d) / iMaskDilate[%d] = %d / ImageF = %f\n", ligneNum, colNum, iIdx+iRange, iMaskDilate[iIdx+iRange], img2Process[colNum*M + ligneNum]);
        }
        // D�termination du niveau haut et bas du masque.
        findMinMaxFromTab (iMaskDilate, iRange*2+1, &iValMaskMin, &iValMaskMax);
        //printf("ligne(%d)-col(%d) / idx(%d) = %d\n", ligneNum, colNum, colNum*M + ligneNum, iValMaskMax);
        ImageF2[colNum*M + ligneNum] = float(iValMaskMax);
// // //         if (colNum > 68 && colNum < 75)
// // //         {
// // //             printf("ligne(%d)-col(%d) / ImageF[%d] : %d\n", ligneNum, colNum, colNum*M + ligneNum, ImageF[colNum*M + ligneNum]);
// // //             printf("ligne(%d)-col(%d) / min : %d - max : %d\n", ligneNum, colNum, iValMaskMin, iValMaskMax);
// // //         }

    }
    free(iMaskDilate);
}

void imErodeC(float *img2Process, int ligneNum, int iRange)
{
    int     iColNotNaN=0;
    float   *fValPix;
    int     *iMaskErode = NULL, 
            idxB, idxE,
            iValMaskMin, iValMaskMax;
    
    // Cr�ation du masque glissant de dilatation
    iMaskErode = (int*)calloc(iRange*2+1, sizeof(int));
    for(int colNum=0; colNum<N; colNum++)
    {
        // Traitement de d�but du buffer
        for (int iIdx=-iRange; iIdx<=iRange; iIdx++)
        {
            if ((colNum + iIdx < 0) || (colNum + iIdx >= N))
            {
                iMaskErode[iIdx+iRange] == (int) 1;
            }
            else
            {
                // On ne teste le pixel du masque que si celui-ci n'a pas �t� d�j� affect�.
                // iMaskErode[iIdx+iRange] == (Image[colNum*M + (ligneNum+iIdx)]== 0)? (int)0 : (int)1; // (int)!mxIsNaN(Image[colNum*M + (ligneNum+iIdx)]);
                if (img2Process[(colNum+iIdx)*M + ligneNum] ==0)
                {
                    iMaskErode[iIdx+iRange] = (int)0;
                }
                else
                {
                    iMaskErode[iIdx+iRange] = (int)1;
                }
            }
        }
        // D�termination du niveau haut et bas du masque.
        findMinMaxFromTab (iMaskErode, iRange*2+1, &iValMaskMin, &iValMaskMax);
        //printf("ligne(%d)-col(%d) / idx(%d) = %d\n", ligneNum, colNum, colNum*M + ligneNum, iValMaskMax);
        ImageF[colNum*M + ligneNum] = float(iValMaskMax);
        img2Process[colNum*M + ligneNum] = float(iValMaskMax);


    }
    free(iMaskErode);
}


void interpLineaire(int ligneNum, int firstColNotNan, int lastColNotNan, int *colNotNaN)
{
    int iColNotNaN=0;
    for(int colNum=0; colNum<N; colNum++)
    {
        
        //if(colNum<firstColNotNan || colNum>lastColNotNan || !_isnan(Image[colNum*M + ligneNum]))
        if(colNum<firstColNotNan || colNum>lastColNotNan || !mxIsNaN(Image[colNum*M + ligneNum]))
            ImageF[colNum*M + ligneNum] = Image[colNum*M + ligneNum];
        else
        {
            while(colNotNaN[iColNotNaN+1] < colNum)
                iColNotNaN++;
            ImageF[colNum*M + ligneNum] = (float)((((double)(colNotNaN[iColNotNaN+1]-colNum)) * ((double)Image[colNotNaN[iColNotNaN]*M + ligneNum]) + ((double)(colNum-colNotNaN[iColNotNaN])) * ((double)Image[colNotNaN[iColNotNaN+1]*M + ligneNum]))/((double)(colNotNaN[iColNotNaN+1]-colNotNaN[iColNotNaN])));
        }
    }
}

void interpNearest(int ligneNum, int firstColNotNan, int lastColNotNan, int *colNotNaN)
{
    int iColNotNaN=0;
    for(int colNum=0; colNum<N; colNum++)
    {
        if(colNum<firstColNotNan || colNum>lastColNotNan || !mxIsNaN(Image[colNum*M + ligneNum]))
            ImageF[colNum*M + ligneNum] = Image[colNum*M + ligneNum];
        else
        {
            while(colNotNaN[iColNotNaN+1] < colNum)
                iColNotNaN++;
            if(colNotNaN[iColNotNaN+1]-colNum > colNum-colNotNaN[iColNotNaN])
                ImageF[colNum*M + ligneNum] = Image[colNotNaN[iColNotNaN]*M + ligneNum];
            else
                ImageF[colNum*M + ligneNum] = Image[colNotNaN[iColNotNaN+1]*M + ligneNum];
        }
    }
}

void noInterp(int ligneNum)
{
    int iColNotNaN=0;
    for(int colNum=0; colNum<N; colNum++)
    {
        ImageF[colNum*M + ligneNum] = Image[colNum*M + ligneNum];
    }
}

void thRecopie(void*p)
{
    stParam *param = (stParam*)p;
    int cpu_id = param->cpu_id;
    
    bool firstNotNaN = false;
    int firstColNotNan = 0;
    int lastColNotNan = 0;
    
    int *colNotNaN = (int*) malloc(N*sizeof(int));
    int iColNotNaN=0;
    int dim0, dim1;

    // float *ImageE = (float*) malloc(dim0*dim0*sizeof(float));
    for(int il = 0; il < M; il++)
    {
        memset(colNotNaN, 0, N*sizeof(int));
        iColNotNaN      = 0;
        firstNotNaN     = false;
        firstColNotNan  = 0;
        lastColNotNan   = 0;
        
        if(il%nbCores == cpu_id)//on decale d'un processeur pour eviter la division par zero
        {
            imDilateErodeC(Image, il, Range);
        }
    }
    // free(ImageE);
      
}



void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    mwSize dims[2];
    
    if(nrhs < 3 || nrhs > 4)
        mexErrMsgTxt("interpHorzImage_mexmc requiert 3 a 4 arguments");
    if(nlhs != 2)
        mexErrMsgTxt("interpHorzImage_mexmc requiert 1 argument en sortie");
    
    nbCores    = (int)mxGetScalar(prhs[2]);

    Image      = (float*) mxGetData(prhs[0]);
    M          = (int)mxGetM(prhs[0]);
    N          = (int)mxGetN(prhs[0]);
    dims[0]    = M;
    dims[1]    = N;
    
    Range      = (int) mxGetScalar(prhs[1]);
    
    typeInterp = 0;//par defaut interpolation lineaire
    if(nrhs == 4)
        typeInterp = (int) mxGetScalar(prhs[3]);
        
    plhs[0] = mxCreateNumericMatrix((mwSize)dims[0], (mwSize)dims[1], mxSINGLE_CLASS, mxREAL);
    ImageF  = (float*)mxGetPr(plhs[0]);
    plhs[1] = mxCreateNumericMatrix((mwSize)dims[0], (mwSize)dims[1], mxSINGLE_CLASS, mxREAL);
    ImageF2 = (float*)mxGetPr(plhs[1]);

    std::vector<stParam> threadData(nbCores);
    
    #pragma omp parallel
    {
        double starttime = omp_get_wtime();

        int cpu_id = omp_get_thread_num();
        nbCores = omp_get_num_threads();
        paspxthread = (int)(M / nbCores);
                
        if(cpu_id == 0) // seulement un CPU
        {
            if(omp_get_num_threads() != nbCores)
            {
                printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
                printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
                mexErrMsgTxt("fillNaN_mexmc8 refuse de continuer");
            }
        }
        threadData[cpu_id].cpu_id = cpu_id;
        thRecopie(&threadData[cpu_id]);
        double endtime = omp_get_wtime();
        printf("Total time: %lf\n", (endtime-starttime));
    }
    

}