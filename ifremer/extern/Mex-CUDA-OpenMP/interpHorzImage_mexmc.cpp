#include "mex.h"
#include <vector>
#include <omp.h>
#include <float.h>
#include <string.h>
#include <matrix.h>


float *Image, *ImageF;
int M, N, paspxthread, nbCores;
int typeInterp;

struct stParam
{
    int cpu_id;
};


void interpLineaire(int ligneNum, int firstColNotNan, int lastColNotNan, int *colNotNaN)
{
    int iColNotNaN=0;
    for(int colNum=0; colNum<N; colNum++)
    {
        if(colNum<firstColNotNan || colNum>lastColNotNan || !mxIsNaN(Image[colNum*M + ligneNum]))
            ImageF[colNum*M + ligneNum] = Image[colNum*M + ligneNum];
        else
        {
            while(colNotNaN[iColNotNaN+1] < colNum)
                iColNotNaN++;
            ImageF[colNum*M + ligneNum] = (float)((((double)(colNotNaN[iColNotNaN+1]-colNum)) * ((double)Image[colNotNaN[iColNotNaN]*M + ligneNum]) + ((double)(colNum-colNotNaN[iColNotNaN])) * ((double)Image[colNotNaN[iColNotNaN+1]*M + ligneNum]))/((double)(colNotNaN[iColNotNaN+1]-colNotNaN[iColNotNaN])));
        }
    }
}

void interpNearest(int ligneNum, int firstColNotNan, int lastColNotNan, int *colNotNaN)
{
    int iColNotNaN=0;
    for(int colNum=0; colNum<N; colNum++)
    {
        if(colNum<firstColNotNan || colNum>lastColNotNan || !mxIsNaN(Image[colNum*M + ligneNum]))
            ImageF[colNum*M + ligneNum] = Image[colNum*M + ligneNum];
        else
        {
            while(colNotNaN[iColNotNaN+1] < colNum)
                iColNotNaN++;
            if(colNotNaN[iColNotNaN+1]-colNum > colNum-colNotNaN[iColNotNaN])
                ImageF[colNum*M + ligneNum] = Image[colNotNaN[iColNotNaN]*M + ligneNum];
            else
                ImageF[colNum*M + ligneNum] = Image[colNotNaN[iColNotNaN+1]*M + ligneNum];
        }
    }
}

void thRecopie(void*p)
{
    stParam *param = (stParam*)p;
    int cpu_id = param->cpu_id;
    
    bool firstNotNaN = false;
    int firstColNotNan = 0;
    int lastColNotNan = 0;
    
    int *colNotNaN = (int*) malloc(N*sizeof(int));
    int iColNotNaN=0;
    
    for(int il = 0; il < M; il++)
    {
        memset(colNotNaN, 0, N*sizeof(int));
        iColNotNaN=0;
        firstNotNaN = false;
        firstColNotNan = 0;
        lastColNotNan = 0;
        
        if(il%nbCores == cpu_id)//on decale d'un processeur pour eviter la division par zero
        {
            for(int colNum=0; colNum<N; colNum++)
            {
                if(!mxIsNaN(Image[il + colNum*M]))
                {
                    if(!firstNotNaN)
                    {
                        firstColNotNan = colNum;
                        firstNotNaN = true;
                    }
                    lastColNotNan = colNum;
                    colNotNaN[iColNotNaN] = colNum;
                    iColNotNaN++;
                }
            }
            
            if(typeInterp ==  0)//interpolatin lineaire
                interpLineaire(il, firstColNotNan, lastColNotNan, colNotNaN);
            else
                interpNearest(il, firstColNotNan, lastColNotNan, colNotNaN);
        }
    }
    free(colNotNaN);
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    if(nrhs < 2 || nrhs > 3) 
        mexErrMsgTxt("interpHorzImage_mexmc requiert 2 a 3 arguments");
    if(nlhs != 1) 
        mexErrMsgTxt("interpHorzImage_mexmc requiert 1 argument en sortie");
    
    nbCores = (int)mxGetScalar(prhs[1]);
    
    Image       = (float*) mxGetData(prhs[0]);
    M           = (int)mxGetM(prhs[0]);
    N           = (int)mxGetN(prhs[0]);
    mwSize dims[2];
    dims[0] = M;
    dims[1] = N;
    
    typeInterp = 0;//par defaut interpolation lineaire
    if(nrhs == 3)
        typeInterp = (int) mxGetScalar(prhs[2]);
        
    plhs[0] = mxCreateNumericMatrix((mwSize)dims[0], (mwSize)dims[1], mxSINGLE_CLASS, mxREAL);
    ImageF  = (float*)mxGetPr(plhs[0]);
    
    std::vector<stParam> threadData(nbCores);
    
#pragma omp parallel
    {
        int cpu_id = omp_get_thread_num();
        nbCores = omp_get_num_threads();
        paspxthread = (int)(M / nbCores);
        
        
        if(cpu_id == 0) // seulement un CPU
        {
            if(omp_get_num_threads() != nbCores)
            {
                printf("OpenMP est configuré pour utiliser %d CPU(s)\n", omp_get_num_threads());
                printf("%d CPU(s) sont installés sur cet ordinateur\n", omp_get_max_threads());
                mexErrMsgTxt("fillNaN_mexmc8 refuse de continuer");
            }
        }
        threadData[cpu_id].cpu_id = cpu_id;
        thRecopie(&threadData[cpu_id]);
    }
}