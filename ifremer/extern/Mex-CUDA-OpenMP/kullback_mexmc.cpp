// COMPILATION :
//	pour Windows 32
//	compilALL_mexOpenMP('platform','Win32','option', '-v', 'filesource',{'kullback_mexmc.cpp'});
//-------------------------------------------------------------------------------
/* 
  dist = kullback_mexmc(Cmat, CmatRef, nbCores)
	Cmat et CmatRef sont des matrices de type single ou double

*/


#include "mex.h"
#include <omp.h>
#include <float.h>
#include <math.h>

#define EPS 2e-16 // eps('double') de MatLab X 10


void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
int nbCores, DIM;

int pasthread;

 if(nrhs != 3) mexErrMsgTxt("kullback_mexmc requiert 3 arguments");
 if(nlhs != 1) mexErrMsgTxt("kullback_mexmc requiert un argument en sortie");

 DIM = (int)mxGetM(prhs[0]) * (int)mxGetN(prhs[0]);
 if((mxGetM(prhs[1]) * mxGetN(prhs[1])) != DIM) mexErrMsgTxt("Les matrices en entr�e doit �tre de m�mes dimensions");

 mxClassID classID_mat_entree1 = mxGetClassID(prhs[0]);
 mxClassID classID_mat_entree2 = mxGetClassID(prhs[1]);
 
 if(classID_mat_entree1 != classID_mat_entree2) mexErrMsgTxt("Les matrices d'entr�e doivent �tre de meme type (single ou double)");
 
 switch(classID_mat_entree1)
 {
	case mxDOUBLE_CLASS :
	{
		const double __LOG2 = log(double(2));
        double *Cmat, *CmatRef;
		double*dist, *dist_par_cpu;
		Cmat = (double*) mxGetData(prhs[0]);
		CmatRef = (double*) mxGetData(prhs[1]);
		nbCores = (int)mxGetScalar(prhs[2]);
        
		dist_par_cpu = (double*)malloc(sizeof(double) * nbCores);
		
		mwSize dims[1];
		dims[0] = 1;
		plhs[0] = mxCreateNumericArray(1, dims, mxDOUBLE_CLASS, mxREAL);
		dist = (double*)mxGetPr(plhs[0]);

		// calcul de la distance de kullback :
		dist[0] = 0;
		#pragma omp parallel
		{
 		 int cpu_id = omp_get_thread_num();
		 nbCores = omp_get_num_threads();
		 pasthread = (int)(DIM / nbCores);
  
		 if(cpu_id == 0) // seulement un CPU
		 {
		  if(omp_get_num_threads() != nbCores)
		  {
			printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
			printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
			mexErrMsgTxt("kullback_mexmc refuse de continuer");
		  } 
		 }
		dist_par_cpu[cpu_id] = 0;
		int stop = (cpu_id + 1) * pasthread;
		if(cpu_id == nbCores - 1) stop = DIM;
		for(int i = cpu_id * pasthread; i < stop; i++) 
			if(Cmat[i] > 0) 
				// dist_par_cpu[cpu_id] += (Cmat[i] + 0.00001) * log((Cmat[i] + 0.00001) / (CmatRef[i] + 0.00001)) / __LOG2; 
				dist_par_cpu[cpu_id] += (Cmat[i] + EPS) * log((Cmat[i] + EPS) / (CmatRef[i] + EPS)) / __LOG2; 
		}
		// retour en monocore :
		for(int i = 0; i < nbCores; i++) dist[0] += dist_par_cpu[i];
	}
	break;
	
	case mxSINGLE_CLASS :
	{
		const float __LOGF2 = logf(float(2));
		float *Cmat, *CmatRef;
		float*dist, *dist_par_cpu;
		Cmat = (float*) mxGetData(prhs[0]);
		CmatRef = (float*) mxGetData(prhs[1]);
		nbCores = (int)mxGetScalar(prhs[2]);
        
        if(omp_get_num_threads() != nbCores)
            nbCores = omp_get_num_threads();
        
		dist_par_cpu = (float*)malloc(sizeof(float) * nbCores);
		
		mwSize dims[1];
		dims[0] = 1;
		plhs[0] = mxCreateNumericArray(1, dims, mxSINGLE_CLASS, mxREAL);
		dist = (float*)mxGetPr(plhs[0]);

		// calcul de la distance de kullback :
		dist[0] = 0;
		#pragma omp parallel
		{
 		 int cpu_id = omp_get_thread_num();
		 nbCores = omp_get_num_threads();
		 pasthread = (int)(DIM / nbCores);
		 if(cpu_id == 0) // seulement un CPU
		 {
		  if(omp_get_num_threads() != nbCores)
		  {
			printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
			printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
			mexErrMsgTxt("kullback_mexmc refuse de continuer");
		  } 
		 }
		dist_par_cpu[cpu_id] = 0;
		int stop = (cpu_id + 1) * pasthread;
		if(cpu_id == nbCores - 1) stop = DIM;
		for(int i = cpu_id * pasthread; i < stop; i++) 
			if(Cmat[i] > 0)// * CmatRef[i] > 0) 
				// dist_par_cpu[cpu_id] += (Cmat[i] + 0.00001) * logf((Cmat[i] + 0.00001) / (CmatRef[i] + 0.00001)) / __LOGF2; 
				dist_par_cpu[cpu_id] += (Cmat[i] + EPS) * logf((Cmat[i] + EPS) / (CmatRef[i] + EPS)) / __LOGF2; 
		}
		// retour en monocore :
		for(int i = 0; i < nbCores; i++) dist[0] += dist_par_cpu[i];
	}
	break;
	default :
		mexErrMsgTxt("Les matrices d'entr�e doivent de type single ou double");
 }
}

