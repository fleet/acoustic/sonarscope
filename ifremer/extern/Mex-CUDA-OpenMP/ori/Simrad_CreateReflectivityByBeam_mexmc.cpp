// Mex-file appel� uniquement par ADU
// (voir l'entete de ce fichier pour son utilisation de l'environnement matlab).
// La fonciton appel�e par MatLab est d�sign�e par le nom du fichier de type mex.
//
// SYNTAX :
//   ReflectivityByBeam = Simrad_CreateReflectivityByBeam_mexmc(ReflectivityByBeam, ...
//                          Entries, InfoBeamNbSamples, NbCPUCores);
//
// PARAMETERS :
//   - ReflectivityByBeam : tableau de r�sultats (2D)
//   - Entries : valeurs de signaux (vecteurs de NbPing X NbBeams X NbSamples)
//   - InfoBeamNbSamples : tableaux d'indexation des �chantillons par faisceau
//
// COMPILATION :
//	pour Windows 32
//	compilALL_mexOpenMP('platform','Win32','option', '-v', 'filesource',{'Simrad_CreateReflectivityByBeam_mexmc.cpp'});
//	pour Windows 64
//	compilALL_mexOpenMP('platform','Win64','option', '-v', 'filesource',{'Simrad_CreateReflectivityByBeam_mexmc.cpp'});
//
// EXAMPLES :
//    NUMBER_OF_PROCESSORS = 2;
//    ReflectivityByBeam = NaN(5,5);
//    dInfoBeamNbSamples = [1:5; 1:5; 1:5;1:5; 1:5];
//    fInfoBeamNbSamples = single(dInfoBeamNbSamples);
//    dEntries = [-10:-1:-120];
//    fEntries = single(dEntries);
//    pppp = Simrad_CreateReflectivityByBeam_mexmc(ReflectivityByBeam, fEntries, fInfoBeamNbSamples, NUMBER_OF_PROCESSORS)
//
// SEE ALSO : Authors
// AUTHORS  : LECOQ + GLU
//-------------------------------------------------------------------------------
#include "mex.h"

#include <vector>
#include <omp.h>
#include <float.h>
#include <math.h>
#include <limits>

/*** Variables globales ***/
float	*fReflectivityByBeam,
        *fEntries,
        *fInfoBeamNbSamples;
        
 
mwSize 	iNbPing, 
		iNbBeam;

int		iPasPxThread, 
		iNbCores,
        iClassType,
        *iDebEntriesPing;


FILE	*fpFID = NULL;
  
struct stParam
{
	int cpu_id;
};

void thCreateReflectivityByBeam(void*p) 
{
    // D�clarations d'un Nan de type single.
    float fNan = std::numeric_limits<float>::quiet_NaN(); //produces 1.#QNAN    
    int kLoop,
        cpu_id,
        iIndexStop,
        iBeam,
        iPing,
        iSubEntriesPing,
        iNbSamplesBeam;

    double  dTotX,
            dX;
   
	stParam *param = (stParam*)p;
    cpu_id = param->cpu_id;
    iIndexStop = (cpu_id + 1) * iPasPxThread;
    if	(cpu_id == iNbCores - 1) 
	{
		iIndexStop = iNbPing ;
    }
 
    iSubEntriesPing = 0;
    for(iPing = cpu_id * iPasPxThread; iPing < iIndexStop; iPing++) 
    {   
        // Traitement sur les N Samples du paquet d'entries.
        iSubEntriesPing = iDebEntriesPing[iPing];
        for(iBeam = 0; iBeam < iNbBeam; iBeam++) 
        {
            
            iNbSamplesBeam = (int)fInfoBeamNbSamples[iPing + iBeam * iNbPing];
            if (iNbSamplesBeam != 0 && !_isnan(fInfoBeamNbSamples[iPing + iBeam * iNbPing])) {
                dTotX = 0;
                for (kLoop=iSubEntriesPing;kLoop<iSubEntriesPing+ iNbSamplesBeam; kLoop++) {
                    dX = fEntries[kLoop];
                    dX = pow(10, (double)dX/10);
                    dTotX = dTotX + dX;
                } // Sortie de la boucle sur N Entries (kLoop)
                dTotX = dTotX/iNbSamplesBeam;
                dTotX = 10 * log10(dTotX);
                fReflectivityByBeam[iPing + iBeam * iNbPing] = (float)dTotX;
                iSubEntriesPing = iSubEntriesPing + iNbSamplesBeam;
            } // Fin du filtrage sur NbSamples = 0 et NbSamples = Nan.
            else
            {
                fReflectivityByBeam[iPing + iBeam * iNbPing] = fNan;
            }
            
        } // fin du traitement par faisceau
    } // fin de la boucle sur les Ping trait�s en multi-threads
    
 } //thCreateReflectivityByBeam

/*** Programme Principal ***/
/*** =================== ***/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) 
{
int n,
    prevN,
    iDummy;

const mwSize    *dims;
mwSize          iNbDims = 0;

//    fpFID = fopen("C:\\temp\\CreateReflectivityByBeam.log", "w+");
//    fprintf(fpFID, "D�marrage de CreateReflectivityByBeam \n");
//    fclose(fpFID);


	if(nrhs != 4) mexErrMsgTxt("Simrad_CreateReflectivityByBeam_mexmc requiert 4 arguments");
    if(nlhs != 1) mexErrMsgTxt("Simrad_CreateReflectivityByBeam_mexmc requiert un argument en sortie");
    
    
    iNbPing = (mwSize)mxGetM(prhs[0]);
    iNbBeam = (mwSize)mxGetN(prhs[0]);

    iNbDims = (mwSize)mxGetNumberOfDimensions(prhs[0]);
    dims = (mwSize *)mxGetDimensions(prhs[0]);
    
	iClassType              = mxGetClassID(prhs[0]);
	fReflectivityByBeam     = (float *)mxGetData(prhs[0]);
	fEntries                = (float *)mxGetData(prhs[1]);		
	fInfoBeamNbSamples      = (float *)mxGetData(prhs[2]);		
    iNbCores                = (int)mxGetScalar(prhs[3]);

    
    // Pr�paration des donn�es de sortie.
    plhs[0]     = mxCreateNumericMatrix(iNbPing, iNbBeam, mxSINGLE_CLASS, mxREAL);
    //plhs[0]     = mxCreateNumericArray(iNbDims, dims, mxSINGLE_CLASS, mxREAL);
    fReflectivityByBeam    = (float *)mxGetPr(plhs[0]);
    
    iDebEntriesPing = (int*)calloc(iNbPing, sizeof(int));
    prevN = 0;
    for(int iPing = 0; iPing < iNbPing; iPing++) 
    {
        // Allocation et initialisation � 0.
        n = 0;
        for(int iBeam = 0; iBeam < iNbBeam; iBeam++) 
        {
            iDummy = 0;
            // Sommation des samples pour l'ensemble des Beams d'un Ping.
            if (!_isnan(fInfoBeamNbSamples[iPing + iBeam * iNbPing]))
            {
                iDummy = (int)fInfoBeamNbSamples[iPing + iBeam * iNbPing];
                n = n + iDummy;
            }
        } // Fin de la boucle sur les faisceaux.
        if (iPing >0)
        {
        	iDebEntriesPing[iPing] = iDebEntriesPing[iPing-1] + prevN;
        }

        prevN = n;
        //DEBUG printf("iDebEntriesPing(%d) = %d\n", iPing, iDebEntriesPing[iPing]);
    } // fin de l'indexation du tableau de Samples par Ping. 
    
    iPasPxThread = (int)(iNbPing / iNbCores);
    std::vector<stParam> threadData(iNbCores);
    int cpu_id;
   

	#pragma omp parallel private(cpu_id) 
	{
		if(cpu_id == 0) // seulement un CPU
		{
			if(omp_get_num_threads() != iNbCores) {
				printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
				printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
				mexErrMsgTxt("Erreur : Simrad_CreateReflectivityByBeam_mexmc refuse de continuer !!!");
			}
		}
		cpu_id = omp_get_thread_num();

		threadData[cpu_id].cpu_id = cpu_id;
		thCreateReflectivityByBeam(&threadData[cpu_id]);
	} // pragma omp parallel
    
    
    free(iDebEntriesPing);

} // mexFunction


