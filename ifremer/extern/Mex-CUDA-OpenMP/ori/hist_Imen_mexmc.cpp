/*
 * x = hist_Imen_mexmc(image, nbValHist, nbCores)
 * image est de type single
 * x est de type double
 *
 */


#include "mex.h"
#include <omp.h>
#include <float.h>
#include <math.h>
#include <string.h>


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if(nrhs != 3) mexErrMsgTxt("coocMatNbNiveauxFiltre_mexmc requiert 3 arguments");
    if(nlhs != 1) mexErrMsgTxt("coocMatNbNiveauxFiltre_mexmc requiert un argument en sortie");
    
    
    int M = mxGetM(prhs[0]); // dimensions de la matrice image
    int N = mxGetN(prhs[0]);
    float*image = (float*)mxGetData(prhs[0]);
    mxClassID classID_mat_entree = mxGetClassID(prhs[0]);
    if(classID_mat_entree != mxSINGLE_CLASS) mexErrMsgTxt("La matrice d'entr�e doit �tre de type single");
    
    int nbValHist = mxGetScalar(prhs[1]);
    int nbCores = mxGetScalar(prhs[2]);
    
    double*x;
    mwSize dims[2];
    dims[0] = 1;
    dims[1] = nbValHist;
    plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
    x = (double*)mxGetPr(plhs[0]);
    
    
    int pasthread = (int)(M * N / nbCores);
    int cpu_id;
    double*x_par_cpu;
    x_par_cpu = (double*)malloc(sizeof(double) * nbValHist * nbCores);
    memset(x_par_cpu, 0, sizeof(double) * nbValHist * nbCores); //GLUGLUGLU
#pragma omp parallel private(cpu_id) 
{
    if(cpu_id == 0) // seulement un CPU
    {
        if(omp_get_num_threads() != nbCores) {
            printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
            printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
            mexErrMsgTxt("kullback_mexmc refuse de continuer");
        }
    }
    cpu_id = omp_get_thread_num();

    int tmp;
    int stop = (cpu_id + 1) * pasthread;
    if(cpu_id == nbCores - 1) stop = M * N;

    for(int i = cpu_id * pasthread; i < stop; i++) {
        if(!_isnan(image[i])) {
            tmp = ceil(image[i]); // image = ceil(image)
            if (tmp <=0) tmp = 1;
            if (tmp > nbValHist) tmp = nbValHist; // im(im > nbValHist) = nbValHist;
    //#pragma omp atomic
            x_par_cpu[(tmp - 1) + nbValHist * cpu_id]++;
        }
    }
    }

    // retour en monocore
    for(int j = 0; j < nbValHist; j++) for(cpu_id = 0; cpu_id < nbCores; cpu_id++) {
        x[j] += x_par_cpu[j + nbValHist * cpu_id];
    }

    // filtrage pour N = 3 :
    // en Matlab : y = filtfilt(ones(1, N) / N, [1 zeros(1, N - 1)], x);
    /*double *y = (double*)malloc(sizeof(double) * nbValHist);
     * y[0] = (x[0] + x[1]) / 2;
     * y[1] = (x[0] + x[1] + x[2]) / 3;
     * for(int i = 2; i < (nbValHist - 2); i++)
     * {
     * double moy = 0;
     * for(int j = i - 2; j <= (i + 2); j++) moy += x[j];
     * y[i] = moy / 5;
     * }
     * y[nbValHist - 2] = (x[nbValHist - 1] + x[nbValHist - 2] + x[nbValHist - 3]) / 3;
     * y[nbValHist - 1] = (x[nbValHist - 1] + x[nbValHist - 2]) / 2;
     */

    int sommex = 0;
    for(int j = 0; j < nbValHist; j++) {
        sommex += x[j];
    }
    if(sommex > 0) {
        for(int j = 0; j < nbValHist; j++) {
            x[j] /= sommex;
        }
    }
} // #pragma omp parallel private


/*
 * int tmp;
 * for(int i = 0; i < M*N; i++)
 * {
 * if(!_isnan(image[i]))
 * {
 * tmp = (int)image[i] + 1; // image = ceil(image)
 * if(tmp > nbValHist) tmp = nbValHist; // im(im > nbValHist) = nbValHist;
 * //printf("tmp : %d\n", tmp);
 * x[tmp - 1]++;
 * }
 * }
 *
 * int sommex = 0;
 * for(int j = 0; j < nbValHist; j++) sommex += x[j];
 * for(int j = 0; j < nbValHist; j++) x[j] /= sommex;
 */


