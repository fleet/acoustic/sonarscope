/*
		y2 = interp1Linear_mex(x1, y1, x2)
		
		il ne doit pas y avoir de NaN dans x1, y1 et x2
		x1, y1 et x2 sont de type double

		x1 et y1 doivent etre de meme dimension
		
		x1 doit etre croissants ou decroissants
		x2 aussi
*/



#include "mex.h"
#include <omp.h>
#include <float.h>

int DimMax(int M, int N)
{
	if(M > N) return M;
	else return N;
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
 if(nrhs != 3) mexErrMsgTxt("interp1Linear_mex requiert 3 arguments");
 if(nlhs != 1) mexErrMsgTxt("interp1Linear_mex requiert un argument en sortie");
 
 mxClassID cid = mxGetClassID(prhs[0]);
 if(cid != mxDOUBLE_CLASS) mexErrMsgTxt("Les arguments doivent etre de type double");
 cid = mxGetClassID(prhs[1]);
 if(cid != mxDOUBLE_CLASS) mexErrMsgTxt("Les arguments doivent etre de type double");
 cid = mxGetClassID(prhs[2]);
 if(cid != mxDOUBLE_CLASS) mexErrMsgTxt("Les arguments doivent etre de type double");
 
 int dimX1 = DimMax((int)mxGetM(prhs[0]), (int)mxGetN(prhs[0]));
 int dimY1 = DimMax((int)mxGetM(prhs[1]), (int)mxGetN(prhs[1]));
 int dimX2 = DimMax((int)mxGetM(prhs[2]), (int)mxGetN(prhs[2]));
 
 
 double *x1, *y1, *x2, *y2;

 x1 = (double*)mxGetData(prhs[0]);
 y1 = (double*)mxGetData(prhs[1]);
 x2 = (double*)mxGetData(prhs[2]);
 //printf("%f\n", x2[0]);
 
 mwSize dims[2];
 dims[0] = (int)mxGetM(prhs[2]);
 dims[1] = (int)mxGetN(prhs[2]);
 plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
 y2 = (double*)mxGetPr(plhs[0]);
 
 // NaNs ?
 
 double diffavant;
 double diffapres;

 if(x1[0] < x1[1])
 {
	if(x2[0] < x2[1])
	{ // cas x1 et x2 croissants
		int istart = 1; // on commence a 1 a cause du x1[i-1]
		for(int j = 0; j < dimX2; j++)
		{
			for(int i = istart; i < dimX1; i++) 
			{
				if(x1[i] > x2[j])
				{
					diffapres = x1[i] - x2[j];
					diffavant = x2[j] - x1[i - 1]; // les diffavant et apres sont forc�ment positives
					y2[j] = (y1[i - 1] * diffapres + y1[i] * diffavant) / (diffapres + diffavant);
					istart = i;
					break;
				} 
			}
		}
	}
	else
	{ // cas x1 croissant et x2 decroissant
		int istart = 1; // on commence a 1 a cause du x1[i-1]
		for(int j = dimX2 - 1; j >= 0; j--)
		{
			for(int i = istart; i < dimX1; i++) 
			{
				if(x1[i] > x2[j])
				{
					diffapres = x1[i] - x2[j];
					diffavant = x2[j] - x1[i - 1]; // les diffavant et apres sont forc�ment positives
					y2[j] = (y1[i - 1] * diffapres + y1[i] * diffavant) / (diffapres + diffavant);
					istart = i;
					break;
				} 
			}
		}
	}
 }
 else
 {
	if(x2[0] < x2[1])
	{ // cas x1 d�croissant et x2 croissant
		int istart = dimX1 - 2; 
		for(int j = 0; j < dimX2; j++)
		{
			for(int i = istart; i >= 0; i--) 
			{
				if(x1[i] > x2[j])
				{
					diffapres = x1[i] - x2[j];
					diffavant = x2[j] - x1[i+1]; // les diffavant et apres sont forc�ment positives
					y2[j] = (y1[i + 1] * diffapres + y1[i] * diffavant) / (diffapres + diffavant);
					istart = i;
					break;
				} 
			}
		}
	}
	else
	{ // cas x1 x2 decroissants
		int istart = dimX1 - 2; 
		for(int j = dimX2 - 1; j >= 0; j--)
		{
			for(int i = istart; i >= 0; i--) 
			{
				if(x1[i] > x2[j])
				{
					diffapres = x1[i] - x2[j];
					diffavant = x2[j] - x1[i+1]; // les diffavant et apres sont forc�ment positives
					y2[j] = (y1[i + 1] * diffapres + y1[i] * diffavant) / (diffapres + diffavant);
					istart = i;
					break;
				} 
			}
		}
	}
 }
 
 
} 
