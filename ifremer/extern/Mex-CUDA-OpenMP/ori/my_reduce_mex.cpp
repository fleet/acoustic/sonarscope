#include "mex.h"
#include <math.h>
#include <vector>
#include <float.h>

double coef;
int lengthy2;
double Offset1;
double Offset2;
int M;
int wl;

double* y2D;double* y1D;double* subx1D;
float* y2S;float* y1S;float* subx1S;
float* n2;
bool isDouble,isDoubleSub;

float* XMax2;float* XMin2;float* XMin1;


void my_reduce();

struct stParam
{
 int cpu_id;
};

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
	 if(nrhs != 7) mexErrMsgTxt("my_reduce_mexmc requiert 7 arguments");
		if(nlhs != 2) mexErrMsgTxt("my_reduce_mexmc requiert 2 arguments en sortie");
	
	double* deltax1D;
	float* deltax1S;
	double* deltax2D;
	float* deltax2S;

	if(mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
		deltax1D = (double*)mxGetData(prhs[1]);
	else
		deltax1S = (float*)mxGetData(prhs[1]);

	if(mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
		deltax2D = (double*)mxGetData(prhs[2]);
	else
		deltax2S = (float*)mxGetData(prhs[2]);

	XMin1 = (float*)mxGetData(prhs[3]); 
	XMin2 = (float*)mxGetData(prhs[4]); 
	XMax2 = (float*)mxGetData(prhs[5]); 
	
	if(mxGetClassID(prhs[1]) == mxDOUBLE_CLASS && mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
		coef = deltax1D[0] / deltax2D[0];
	else if(mxGetClassID(prhs[1]) == mxDOUBLE_CLASS && mxGetClassID(prhs[2]) == mxSINGLE_CLASS)
		coef = deltax1D[0] / deltax2S[0];
	else if(mxGetClassID(prhs[1]) == mxSINGLE_CLASS && mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
		coef = deltax1S[0] / deltax2D[0];
	else 
		coef = deltax1S[0] / deltax2S[0];

	if(mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
	{
		// GLU le 21/11/2011 : peut-�tre � l'origine de pb !!! ("+1" � affiner) 
		lengthy2 = (int)(1 + ceil((XMax2[0] - XMin2[0]) / deltax2D[0]));		
	}
	else
	{
		// GLU le 21/11/2011 : peut-�tre � l'origine de pb !!! ("+1" � affiner) 
		lengthy2 = (int)(1 + ceil((XMax2[0] - XMin2[0]) / deltax2S[0]));
	}

	if(mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
		Offset1 = (XMin1[0] / deltax1D[0]) - 1;
	else
		Offset1 = (XMin1[0] / deltax1S[0]) - 1;
	

	if(mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
		Offset2 = (XMin2[0] / deltax2D[0]) - 1;
	else
		Offset2 = (XMin2[0] / deltax2S[0]) - 1;
	
	
	mxClassID classx = mxGetClassID(prhs[0]);
	mxClassID classSubX = mxGetClassID(prhs[6]);
	M = (int)mxGetN(prhs[6]);
 
	mwSize dims[2];
	dims[0] = 1;
	dims[1] = lengthy2;

	 switch(classx)
	 {
		case mxDOUBLE_CLASS :
		{
			plhs[0] = mxCreateNumericMatrix(1, lengthy2, mxDOUBLE_CLASS, mxREAL);
			y2D = (double*) mxGetData(plhs[0]);
			y1D = (double*) mxGetData(prhs[0]); 
			isDouble = true;
			break;
		}
		case mxSINGLE_CLASS :
		{
			plhs[0] = mxCreateNumericMatrix(1, lengthy2, mxSINGLE_CLASS, mxREAL);
			y2S = (float*) mxGetData(plhs[0]);
			y1S = (float*) mxGetData(prhs[0]); 
			isDouble = false;
			break;
		}
	 }

	 switch(classSubX)
	 {
		case mxDOUBLE_CLASS :
		{ 
			subx1D = (double*) mxGetData(prhs[6]); 
			isDoubleSub = true;
			break;
		}
		case mxSINGLE_CLASS :
		{
			subx1S = (float*) mxGetData(prhs[6]); 
			isDoubleSub = false;
			break;
		}
	 }
	
	plhs[1] = mxCreateNumericMatrix(1, lengthy2, mxSINGLE_CLASS, mxREAL);
	 
	
	n2 = (float*) mxGetData(plhs[1]);
	 
	my_reduce();
}

void my_reduce()
{
	float NaNF = 0;
	double NaND = 0;

	for(int il = 0; il < M; il++)
	{
		if(isDouble)
		{
			int k1;
			if(isDoubleSub)
				k1 = (int)(subx1D[il]);
			else
				k1 = (int)(subx1S[il]);
			double val = y1D[k1-1];
			if(!_isnan(val))
			{
				int k2 = (int)(1 + floor((k1 + Offset1) * coef - Offset2));
				if (k2 <= lengthy2)
				{
					if(_isnan(y2D[k2-1]))
						y2D[k2-1] = 0;
					if(_isnan(n2[k2-1]))
					{
						NaNF = n2[k2-1];
						n2[k2-1] = 0;
					}

					y2D[k2-1] = y2D[k2-1] + val;
					n2[k2-1] = n2[k2-1] + 1;
				}
			}
			else
				NaND = val;
		}
		else
		{
			int k1;
			if(isDoubleSub)
				k1 = (int)(subx1D[il]);
			else
				k1 = (int)(subx1S[il]);
			float val = y1S[k1-1];
			if(!_isnan(val))
			{
				int k2 = (int)(1 + floor((k1 + Offset1) * coef - Offset2));
				if (k2 <= lengthy2)
				{
					if(_isnan(y2S[k2-1]))
						y2S[k2-1] = 0;
					if(_isnan(n2[k2-1]))
						n2[k2-1] = 0;

					y2S[k2-1] = y2S[k2-1] + val;
					n2[k2-1] = n2[k2-1] + 1;
				}
			}
			else
				NaNF = val;
		}
	}

	for(int il = 0; il < lengthy2; il++)
	{
		if(isDouble)
		{
			if(n2[il] == 0)
			{
				y2D[il] = NaND;
				n2[il] = NaNF;
			}
			else 
				y2D[il] /= n2[il];
		}
		else
		{
			if(n2[il] == 0)
			{
				y2S[il] = NaNF;
				n2[il] = NaNF;
			}
			else 
				y2S[il] /= n2[il];
		}
	}
}