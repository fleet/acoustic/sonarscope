// Mex-file appel� uniquement par ADU
// (voir l'entete de ce fichier pour son utilisation de l'environnement matlab).
// La fonciton appel�e par MatLab est d�sign�e par le nom du fichier de type mex.
//
// SYNTAX :
//   ReflectivityByBeam = qsort_mexmc(ReflectivityByBeam, ...
//                          Entries, InfoBeamNbSamples, NbCPUCores, MeanCompType);
//
// PARAMETERS :
//   - ReflectivityByBeam : tableau de r�sultats (2D)
//   - Entries : valeurs de signaux (vecteurs de NbPing X NbBeams X NbSamples)
//   - InfoBeamNbSamples : tableaux d'indexation des �chantillons par faisceau
//
// COMPILATION :
//	pour Windows 32
//	compilALL_mexOpenMP('platform','Win32','option', '-v', 'filesource',{'Simrad_CreateReflectivityByBeam_mexmc.cpp'});
//	pour Windows 64
//	compilALL_mexOpenMP('platform','Win64','option', '-v', 'filesource',{'Simrad_CreateReflectivityByBeam_mexmc.cpp'});
//
// EXAMPLES :
//    NUMBER_OF_PROCESSORS = 2;
//    ReflectivityByBeam = NaN(5,5);
//    dInfoBeamNbSamples = [1:5; 1:5; 1:5;1:5; 1:5];
//    fInfoBeamNbSamples = single(dInfoBeamNbSamples);
//    dEntries = [-10:-1:-120];
//    fEntries = single(dEntries);
//    pppp = Simrad_CreateReflectivityByBeam_mexmc(ReflectivityByBeam, fEntries, fInfoBeamNbSamples, NUMBER_OF_PROCESSORS, MeanCompType)
//
// SEE ALSO : Authors
// AUTHORS  : LECOQ + GLU
//-------------------------------------------------------------------------------

#include "mex.h"

#include <vector>
#include <omp.h>
#include <float.h>
#include <math.h>
#include <limits>
#include <time.h>

/*** Variables globales ***/
double	*dX;
         
mwSize 	iNbItem;

int		iClassType;


FILE	*fpFID = NULL;
  
int compare(const void *a, const void *b) {
  const double *fa = (const double *) a;
  const double *fb = (const double *) b;
  if (*fa > *fb) return 1;
  if (*fa < *fb) return -1;

  if (*fa == *fb) {
    //return -memcmp(fa, fb, sizeof *fa); if -0.0, 0.0 order important.
    return 0;
  }
  // At least one of *fa or *fb is NaN
  // is *fa a non-NaN?
  if (!isnan(*fa)) return -1;
  if (!isnan(*fb)) return 1;

  // both NaN
  return 0;
  // return -memcmp(fa, fb, tbd size); if NaN order important.
}


/*** Programme Principal ***/
/*** =================== ***/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) 
{
int n,
    prevN,
    iDummy,
    iMeanCompType,
        i;

const mwSize    *dims;
mwSize          iNbDims = 0;
mxArray *b_out;
double  *b; 

//    fpFID = fopen("C:\\temp\\CreateReflectivityByBeam.log", "w+");
//    fprintf(fpFID, "D�marrage de CreateReflectivityByBeam \n");
//    fclose(fpFID);


	if(nrhs != 1) mexErrMsgTxt("Simrad_CreateReflectivityByBeam_mexmc requiert 1 arguments");
    if(nlhs != 1) mexErrMsgTxt("Simrad_CreateReflectivityByBeam_mexmc requiert un argument en sortie");
      
    iNbItem = (mwSize)mxGetNumberOfElements(prhs[0]);
    iNbDims = (mwSize)mxGetNumberOfDimensions(prhs[0]);
    dims    = (mwSize *)mxGetDimensions(prhs[0]);
    //mexPrintf("\n\NbItem :%d/ NbDims : %d",iNbItem, iNbDims);
    
	//iClassType  = mxGetClassID(prhs[0]);
 
	dX          = (double *)mxGetData(prhs[0]);

    // Appel de la fct Tri
    qsort (dX, iNbItem, sizeof(double), compare);

    b_out = plhs[0] = mxCreateDoubleMatrix(iNbItem, 1, mxREAL);
        
    // Access the contents of the input and output arrays:
    b = mxGetPr(b_out);

    // Compute some elementwise function of the input array:
    for (int i = 0; i < iNbItem; i++) {
         b[i] = dX[i];
}


} // mexFunction

