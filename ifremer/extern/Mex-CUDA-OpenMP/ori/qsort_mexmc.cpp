#include <stdio.h>
#include <stdlib.h>
#include "mex.h"

#include <vector>
#include <omp.h>
#include <float.h>
#include <math.h>
#include <limits>
#include <time.h>


int partition(int * a, int p, int r)
{
    int lt[r-p];
    int gt[r-p];
    int i;
    int j;
    int key = a[r];
    int lt_n = 0;
    int gt_n = 0;

#pragma omp parallel for
    for(i = p; i < r; i++){
        if(a[i] < a[r]){
            lt[lt_n++] = a[i];
        }else{
            gt[gt_n++] = a[i];
        }   
    }   

    for(i = 0; i < lt_n; i++){
        a[p + i] = lt[i];
    }   

    a[p + lt_n] = key;

    for(j = 0; j < gt_n; j++){
        a[p + lt_n + j + 1] = gt[j];
    }   

    return p + lt_n;
}

void quicksort(int * a, int p, int r)
{
    int div;

    if(p < r){ 
        div = partition(a, p, r); 
#pragma omp parallel sections
        {   
#pragma omp section
            quicksort(a, p, div - 1); 
#pragma omp section
            quicksort(a, div + 1, r); 

        }
    }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) 
{
int n,
    prevN,
    iDummy,
    iMeanCompType;
mwSize 	iNbItem;


const mwSize    *dims;
mwSize          iNbDims = 0;
mxArray *b_out;
double  *b; 
int *a;
    int i;

    if(nrhs != 1) mexErrMsgTxt("qsort_mexmc requiert 1 arguments");
    if(nlhs != 1) mexErrMsgTxt("qsort_mexmc requiert un argument en sortie");
      
    iNbItem = (mwSize)mxGetNumberOfElements(prhs[0]);
    iNbDims = (mwSize)mxGetNumberOfDimensions(prhs[0]);
    dims    = (mwSize *)mxGetDimensions(prhs[0]);
    //mexPrintf("\n\NbItem :%d/ NbDims : %d",iNbItem, iNbDims);
    
	//iClassType  = mxGetClassID(prhs[0]);
 
	a          = (int *)mxGetData(prhs[0]);


    quicksort(a, 0, iNbItem);

    //for(i = 0;i < iNbItem; i++){
    //    printf("%d\t", a[i]);
    //}

    b_out = plhs[0] = mxCreateDoubleMatrix(iNbItem, 1, mxREAL);
        
    // Access the contents of the input and output arrays:
    b = mxGetPr(b_out);

    // Compute some elementwise function of the input array:
    for (int i = 0; i < iNbItem; i++) {
         b[i] = double(a[i]);
    }
}