// Mex-file appel� uniquement par quantify_mex.iM (voir l'entete de ce fichier pour son utilisation)
// de l'environnement matlab).
// La fonciton appel�e par MatLab est d�sign�e par le nom du fichier de type mex.
//
// SYNTAX :
//   fXout = quantify_private_mex(fXin, bins(1), dRangeIn, dRangeOut, uiN, iNbCores);
//
// COMPILATION :
//	pour Windows 32
//	compilALL_mexOpenMP('platform','Win32','option', '-v', 'filesource',{'quantify_private_mexmc.cpp'});
//
// EXAMPLES :
//   pppp = quantify_private_mexmc([2 3], 2, double([2 3]), double([1 2]), 2, 2);
//
//		X = loadmat('C:\temp\X.mat');
//		bins(1) = 1.4980;
//		rangeIn = [1 255];
//		rangeOut = [1 255];
//		N = 255;
//		NUMBER_OF_PROCESSORS = 2;
//		pppp = quantify_private_mexmc(X, bins(1), double(rangeIn), double(rangeOut), N, NUMBER_OF_PROCESSORS);
//
// SEE ALSO : Authors
// AUTHORS  : LECOQ + GLU
//-------------------------------------------------------------------------------
#include "mex.h"

#include <vector>
#include <omp.h>
#include <float.h>
#include <math.h>
#include <matrix.h>
 
float				*fXin;
int					*iXin;
char				*cXin;
long int			*ilXin;
short				*sXin;
unsigned char		*ucXin;
unsigned int		*uiXin;
unsigned short		*usXin;
unsigned long int	*uilXin;
double				*dXin;
 
int 	iM, 
		iN, 
		iPasPxThread, 
		iNbCores;
		
int		iType;

float	*fXout; // GLU le 08/09/2011 : Passage en SINGLE (au lieu de DOUBLE) syst�matique de la sortie.
double  dDiff,
		dA, 
		dB,
		*dRangeIn, 
		*dRangeOut,
		dBins0;
 
 unsigned int uiN;

 FILE	*fpFID = NULL;
  
struct stParam
{
	int cpu_id;
};

void thQuantify(void*p) 
{
int iFlagNaN = 0;

	stParam *param = (stParam*)p;
    int cpu_id = param->cpu_id;
    int stop = (cpu_id + 1) * iPasPxThread;
    if	(cpu_id == iNbCores - 1) 
	{
		stop = iM * iN;
    }

   
    for(int i = cpu_id * iPasPxThread; i < stop; i++) {
        // if ((i%10000) == 0)
        // {
            // fpFID = fopen("C:\\temp\\quantify.log", "a+");
			// switch (iType)
			// {
				// case mxUINT8_CLASS:
					// fprintf(fpFID, "ucXin[%d] : %d\n", i, ucXin[i]);
					// break;
				// case mxSINGLE_CLASS:
					// fprintf(fpFID, "fXin[%d] : %f\n", i, fXin[i]);
					// break;
				// case mxDOUBLE_CLASS:
					// fprintf(fpFID, "dXin[%d] : %f\n", i, dXin[i]);
					// break;
			// }
            // fclose(fpFID);
        // }
        // Test du Nan seulement sur les Single et Double.
		if (iType == mxSINGLE_CLASS)
		{
			if (mxIsNaN((double)fXin[i])) 
			{
				fXout[i] = (float)0;
				iFlagNaN = 1;
			}
		}		
		else if (iType == mxDOUBLE_CLASS)
		{
			if (mxIsNaN(dXin[i])) 
			{
				fXout[i] = (float)0;
				iFlagNaN = 1;
			}			
		}		
		if (iFlagNaN == 0) {		
			switch (iType)
			{
				case mxSINGLE_CLASS:
					fXout[i] = floor(dA * fXin[i] + dB);
					break;
				case mxDOUBLE_CLASS:
					fXout[i] = floor(dA * dXin[i] + dB);
					break;
				case mxUINT8_CLASS:
					fXout[i] = floor(dA * ucXin[i] + dB);
					break;
				case mxINT8_CLASS:
					fXout[i] = floor(dA * cXin[i] + dB);
					break;
				case mxINT16_CLASS:
					fXout[i] = floor(dA * sXin[i] + dB);
					break;
				case mxUINT16_CLASS:
					fXout[i] = floor(dA * usXin[i] + dB);
					break;
				case mxINT32_CLASS:
					fXout[i] = floor(dA * iXin[i] + dB);
					break;
				case mxUINT32_CLASS:
					fXout[i] = floor(dA * uiXin[i] + dB);
					break;
				case mxINT64_CLASS:
					fXout[i] = floor(dA * ilXin[i] + dB);
					break;
				case mxUINT64_CLASS:
					fXout[i] = floor(dA * uilXin[i] + dB);
					break;
			}
            fXout[i] = (float)dRangeOut[0] + fXout[i] * dDiff / (uiN - 1);
            if (fXout[i] < 1) 
			{
				fXout[i] = (float)1;
  			}
           if(fXout[i] > (uiN + dRangeOut[0])) {
                fXout[i] = uiN + dRangeOut[0];
           }
		} // Fin du isnan
		iFlagNaN = 0;
		
        // if ((i%10000) == 0 ) {
			// fpFID = fopen("C:\\temp\\quantify.log", "a+");
            // fprintf(fpFID, "fXout[%d] : %f\n", i, fXout[i]);
            // fclose(fpFID);
        // }
    } // fin de la boucle for.
    
 } //thQuantify

/*** Programme Principal ***/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) 
{
    // fpFID = fopen("C:\\temp\\quantify.log", "w+");
	// fprintf(fpFID, "D�marrage de quantify_mexmc \n");
	// fclose(fpFID);
    
	if(nrhs != 6) mexErrMsgTxt("quantify_private_mex requiert 6 arguments");
    if(nlhs != 1) mexErrMsgTxt("quantify_private_mex requiert un argument en sortie");
    
    
    iM = mxGetM(prhs[0]);
    iN = mxGetN(prhs[0]);
 
	iType = mxGetClassID(prhs[0]);
	switch (iType)
	{
		case mxUINT8_CLASS:
			ucXin = (unsigned char *)mxGetData(prhs[0]);
			break;
		case mxSINGLE_CLASS:
			fXin = (float *)mxGetData(prhs[0]);
			break;
		case mxDOUBLE_CLASS:
			dXin = (double *)mxGetData(prhs[0]);
			break;
		case mxINT8_CLASS:
			cXin = (char *)mxGetData(prhs[0]);
			break;
		case mxINT16_CLASS:
			sXin = (short *)mxGetData(prhs[0]);
			break;
		case mxUINT16_CLASS:
			usXin = (unsigned short *)mxGetData(prhs[0]);
			break;
		case mxINT32_CLASS:
			iXin = (int *)mxGetData(prhs[0]);
			break;
		case mxUINT32_CLASS:
			uiXin = (unsigned int *)mxGetData(prhs[0]);
			break;
		case mxINT64_CLASS:
			uilXin = (unsigned long int *)mxGetData(prhs[0]);
			break;
		case mxUINT64_CLASS:
			ilXin = (long int *)mxGetData(prhs[0]);
			break;
	}
			
    // R�cup�ration des donn�es d'entr�es.
	dBins0      = (double) mxGetScalar(prhs[1]);
    dRangeIn    = (double*) mxGetData(prhs[2]);
    dRangeOut   = (double*) mxGetData(prhs[3]);
    uiN         = (unsigned int) mxGetScalar(prhs[4]);
    iNbCores    = (int)mxGetScalar(prhs[5]);

    mwSize dims[2];
    dims[0]     = iM;
    dims[1]     = iN;
   
	// Assignation du tableau de sortie.
	plhs[0] = mxCreateNumericArray(2, dims, mxSINGLE_CLASS, mxREAL);
    fXout = (float *)mxGetPr(plhs[0]);

    dDiff = (dRangeOut[1] - dRangeOut[0]);
    dA = (double)(uiN - 1) / (dRangeIn[1] - dRangeIn[0]);
    dB = - dA * dBins0;
       
    std::vector<stParam> threadData(iNbCores);
    int cpu_id;

	#pragma omp parallel private(cpu_id) 
	{
		cpu_id = omp_get_thread_num();
		iNbCores = omp_get_num_threads();
		iPasPxThread = (int)(iM * iN / iNbCores);
		if(cpu_id == 0) // seulement un CPU
		{
			if(omp_get_num_threads() != iNbCores) {
				printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
				printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
				mexErrMsgTxt("Erreur : quantify_private_mexmc refuse de continuer !!!");
			}
		}
		threadData[cpu_id].cpu_id = cpu_id;
		thQuantify(&threadData[cpu_id]);
	} // pragma omp parallel

} // mexFunction


