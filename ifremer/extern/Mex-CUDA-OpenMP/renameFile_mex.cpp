// Mex File appelant l'API Windows MoveFile pour renommer un fichier.
//
// INPUT :
//   oldName : ancien nom du fichier
//   newName : nouveau nom du fichier
//
// OUTPUT :
//   N�ant
//
// SYNTAX :
//   renameFile_mex('F:\SonarScopeData\Level2\Sonar\20110214_070620.s7k', ...
//                  'F:\SonarScopeData\Level2\Sonar\20110214_070620.super');
//
// COMPILATION :
//	pour Windows 32
//	compilALL_mexOpenMP('platform','Win32','option', '-v', 'filesource',{'renameFile_mex.cpp'});
//	pour Windows 64
//	compilALL_mexOpenMP('platform','Win64','option', '-v', 'filesource',{'renameFile_mex.cpp'});
//
// EXAMPLES :
//   [t, f] = getDiskFreeSpace_mex('C:');
//
// SEE ALSO : Authors
// AUTHORS  : GLU
//-------------------------------------------------------------------------------
#include "mex.h"
#include <windows.h>
#include <stdio.h>

typedef BOOL (WINAPI *P_GDFSE)(LPCTSTR, PULARGE_INTEGER,
        PULARGE_INTEGER, PULARGE_INTEGER);

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    BOOL  bResult;
        
    char    *oldName,
            *newName,
            cDummy[500];
   
    int     nInput  = 2;
        
    
    // input must be a string.
    if ( mxIsChar(prhs[0]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:renameFile:inputNotString",
                "Input must be a string.");
    
    // output must be a string.
    if ( mxIsChar(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:renameFile:outputNotString",
                "Output must be a string.");
    
    // copy the old string data from prhs[0] into a C string input_ buf.
    oldName = mxArrayToString(prhs[0]);
    // copy the new string data from prhs[0] into a C string input_ buf.
    newName = mxArrayToString(prhs[1]);
        
    if(nrhs != nInput)
    {
        sprintf(cDummy, "my_reduce_mexmc requiert %d arguments", nInput);
        mexErrMsgTxt(cDummy);
    }
    
    // Appel de l'API Windows valable en 32 et 64 bits. 
    bResult             = MoveFile (oldName, newName);
    // printf ("R�sultat du renommage = %d\n", bResult);
    // printf ("Ancien Nom = %s\n", oldName);
    // printf ("Nouveau Nom = %s\n", newName);
    
    // R�cup�ration du flag de r�sultat.
    plhs[0] = mxCreateNumericMatrix(1, 1, mxLOGICAL_CLASS, mxREAL);
    *mxGetPr(plhs[0])   = (BOOL)bResult;
    
    mxFree(oldName);
    mxFree(newName);
    
    
}

