#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    float * fAmp;
    double* outputDetection;
    _raw_data_type * data;

    // Get the input parameters
    fAmp=(float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);
    m_iOutputSamples = mxGetM(prhs[0]);
    data = (_raw_data_type*)mxCalloc(m_dwReceiveBeams*m_iOutputSamples, sizeof(_raw_data_type)); 
	m_pRAWData = (char*)data;
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            data->amp = (short)(fAmp[(iB*m_iOutputSamples)+iS]);
        }
    }

	// Allocate datas
	m_pfBeamsA = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsB = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));

	
    // Call the C function _barycentre
    _BDA_compensAmpByMHorzMedian(m_pRAWData, m_dwReceiveBeams, m_iOutputSamples);
    
    // Set the output parameters
	data = (_raw_data_type*)m_pRAWData;
	plhs[0] = mxCreateDoubleMatrix(m_iOutputSamples,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
	for (int iS=0; iS < m_iOutputSamples; iS++) {
		for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            outputDetection[(iB*m_iOutputSamples)+iS] = data->amp;
        }
    }    

	// free memory
	mxFree(m_pRAWData); 
	m_pRAWData = 0;
	mxFree(m_pfBeamsA); 
	m_pfBeamsA = 0;
	mxFree(m_pfBeamsB); 
	m_pfBeamsB = 0;

}
