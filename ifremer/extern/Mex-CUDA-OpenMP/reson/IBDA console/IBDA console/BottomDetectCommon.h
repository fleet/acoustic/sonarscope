/***************************************************************************
****************************************************************************

   FILE: BottomDetectCommon.h: provides common declarations for various 
							   bottom detection algorithms.

   Copyright � 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

****************************************************************************
***************************************************************************/

#pragma once

typedef struct tagbdresults {
	float    fBD_Sample;    // raw bottom detect result (fractional sample number)
	float    fAmplitude;    // Amplitude @ detection sample
	float    fMaxAmplitude; // Maximum amplitude around BD point
	BYTE     ucQuality;     // bottom detect quality/process flag
							// Bit 0:  Brightness:  1 = pass, 0 = fail
							// Bit 1:  Colinearity: 1 = pass, 0 = fail
							// Bit 2:  Bottom Detect Process (Magnitude):  1 = used, 0 = not used.
							// Bit 3:  Bottom Detect Process (Phase):      1 = used, 0 = not used.
							// Bit 4:  Used internally - ???
							// Bit 5:  Used internally for custom PDS Nadir Filter - ???
							// Bit 6-7: Reserved
	float	 fQuality;		// IFremer floating point quality values
	int      iBeamNumber;   // beam for this bottom detect point
	int      Hmin, Hmax;    // min and max SAMPLE number for gate
	int      minWin, maxWin;  // min and max SAMPLE number of detection window
	double   dCorrelation;  // phase fit correlation
	float    uncertainty;   // relative uncertainty
} bdresults;

