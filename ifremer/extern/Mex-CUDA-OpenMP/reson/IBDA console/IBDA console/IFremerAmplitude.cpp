/***************************************************************************=
****************************************************************************=


   FILE: IFremerAmplitude.cpp

   Implementation of amplitude detection functions for CIFremerBottomDetect class.

   Copyright =A9 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    March 2008
   Author:  Yevgeniy Shafirovich
   Change:  Initial implementation

****************************************************************************
***************************************************************************/

#include "stdafx.h"
#include "IFremerBottomDetect.h"
#include "float.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// --------------------------------------------------------------------
// _amplitude_single - implementation of detectionAmplitudePing
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT] m_pRangeAmplitude[beam]
// [OUTPUT] m_pSamplesAmp[beam]
// [OUTPUT] m_pQualityAmplitude[beam]
// [OUTPUT] m_pQF2Amplitude[beam]
// [TEMP]   m_pfSamplesB
//
// [MATLAB] x                === ampIndex
// [MATLAB] Amp              === amp
// [MATLAB] R0               === m_R0 (not used)
// [MATLAB] iBeam            === beam
// [MATLAB] BeamAngles       === m_pBeamAngles (not used)
// [MATLAB] nbSamples        === m_iOutputSamples
// [MATLAB] AmPingRange      === m_pRangeAmplitude[beam]
// [MATLAB] AmpPingQF        === m_pQualityAmplitude[beam]
// [MATLAB] AmpPingSamplesNb === m_pSamplesAmp[beam]
// [MATLAB] AmpPingQF2       === m_pQF2Amplitude[beam]
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_amplitude_detection
//
// MatLab reference:
//		detectionAmplitudePing
//
// !ALTRAN - 5 begin
bool CIFremerBottomDetect::_amplitude_single (WORD beam, DWORD *ampIndex, float * amp, int samples, int nbAllSamples)
// !ALTRAN end
{
	int iS;
	double AmpBeamRange;
	int subAmpIndex, subAmpsize;
	bool result;

// !ALTRAN - 6 begin
// %% Second test (15/12/2010) : if the last sample of the signal corresponds
// % to the maximum sample reachable in range then do not compute the
// % sounding because it will be either a biased one or a false detection
	if (ampIndex[samples-1] == (nbAllSamples - 1)){
		m_pRangeAmplitude[beam]     = -1; // %will be used in ResonSyntheseBottomDetector.m to determine which amplitude sounding to delete
		NANf(m_pQualityAmplitude[beam]);
		/*AmpBeamSamplesNb = NaN;
		AmpBeamQF2       = NaN;
		N=NaN;*/
		return true;
	}
// !ALTRAN end


// %% First center of gravity computed on energy
// [AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp);

	double Sigma, Sigma1 = 0, AmpBeamRange1, Sigma2 = 0, AmpBeamRange2 ;
	bool val_qf = false, val_qf_1 = false, val_qf_2 = false; // EPM 2/19/14 in response to run-time error

	// !ALTRAN - 42 begin
	if(m_IdentAlgo == 2)
	{
		AmpBeamRange = _barycentre(amp, ampIndex, samples, Sigma, m_pfdSamples) ; // as per Jean-Marie EPM 1/29/14
		result = _amplitude_test_double_shape(ampIndex, amp, samples, m_R0, AmpBeamRange, 1, &subAmpIndex, &subAmpsize);
		if(result == true)
		{
			AmpBeamRange = _barycentre(amp, ampIndex+subAmpIndex, subAmpsize, Sigma, m_pfdSamples);
		}
	}
	else
	{
		AmpBeamRange = _barycentre(amp, ampIndex, samples, Sigma, m_pfdSamples, 4) ; // as per Jean-Marie EPM 1/29/14
	}
	// !ALTRAN End


// %% Testing the signal length : if its standard deviation is lower
//	% than the physical length of the signal then we don't need to compute
//	% another center of gravity and we keep this sounding
//		
// if 2*Sigma0 +1 <= (N_0 + 2*(AmpBeamRange0 * (1 / cosd(theta/2) - 1)))
//    AmpBeamRange = AmpBeamRange0;
//    Sigma1 = Sigma0;
//    val_qf_0 = 1;

	double th = 0.0;

	if (m_dwDeviceId == 7111) {
		th = 123.376397069386e-006; 
	} else if (m_dwDeviceId == 7150) {
		if (m_fCenterFrequency < 18000) {
			th = 38.0777815958133e-006;
		} else {
			th = 9.51933212567369e-006;
		}
	}


	int iMin = 0, iMax = samples;

	if (4.0 * Sigma + 1 <= (m_N0 + th * AmpBeamRange)) {
		val_qf = true;
	} else {
// %% Restriction to the new domain
// sub1 = find((x >= (AmpBeamRange0-2*Sigma0)) & (x <= (AmpBeamRange0+2*Sigma0)));
		//double fMin = AmpBeamRange-4*Sigma;
		//double fMax = AmpBeamRange+4*Sigma;
		double fMin = AmpBeamRange-2*Sigma; // EPM as per Jean-Marie 02-05-14
		double fMax = AmpBeamRange+2*Sigma; // EPM as per Jean-Marie 02-05-14

		for (iS = 0;    iS < samples && ampIndex[iS] < fMin; iS++); iMin = iS;
		for (iS = samples-1; iS >= 0 && ampIndex[iS] > fMax; iS--); iMax = iS+1;

// %% Test of the shape of the enveloppe : if the remaining signal is not
//	% approximatively bell-shaped, then we dissmiss the sounding since its QF
//	% will be biased. We test the signal before and afetr restriction
// val_qf_0=test_shape(Amp,x);
// val_qf_1=test_shape(Amp(sub1),x(sub1));
		val_qf   = _amplitude_test_shape(ampIndex,      amp,      samples);
		val_qf_1 = _amplitude_test_shape(ampIndex+iMin, amp+iMin, iMax-iMin);

// %% Computation of the second center of gravity on amplitude
// [AmpBeamRange1, Sigma1] = calcul_barycentre1(x(sub1), Amp(sub1));
		AmpBeamRange1 = _barycentre(amp+iMin, ampIndex+iMin, iMax-iMin, Sigma1, m_pfdSamples, 1);
		// !ALTRAN - 17 begin
		val_qf_2 = _amplitude_test_shape(ampIndex+iMin, amp+iMin, iMax-iMin);
		// !ALTRAN end
			
		// Appel du 3�me calcul EPM as per Jean-Marie and Xavier
		fMin = AmpBeamRange1-2*Sigma1;

		fMax = AmpBeamRange1+2*Sigma1;

		for (iS = 0;    iS < samples && ampIndex[iS] < fMin; iS++); iMin = iS;

		for (iS = samples-1; iS >= 0 && ampIndex[iS] > fMax; iS--); iMax = iS+1;

		// !ALTRAN - 18 begin
		//val_qf_2 = _amplitude_test_shape(ampIndex+iMin, amp+iMin, iMax-iMin);
		// !ALTRAN end
		
		AmpBeamRange2 = _barycentre(amp+iMin, ampIndex+iMin, iMax-iMin, Sigma2, m_pfdSamples, 1);

	
	}

// %% Definition of the QF parameters and tests
// 
// if (2*Sigma1+1)/N_corr > 1 && val_qf_1%remaining signal long enough and bell shaped
//     val_qf=val_qf_1;
//     AmpBeamRange = AmpBeamRange1;
//     N  = (2*Sigma1+1)/N_corr;
//     dTAmp = B * sqrt(((4/pi-1) / 12) * (N-1) * (N+1) / N)* N_corr;
// elseif (2*Sigma0+1)/N_corr > 1 && val_qf_0%previous signal long enough and bell shaped
//     val_qf=val_qf_0;
//     AmpBeamRange = AmpBeamRange0;
//     N  = (2*Sigma0+1)/N_corr;
//     dTAmp = B * sqrt(((4/pi-1) / 12) * (N-1) * (N+1) / N)* N_corr;
// else%none of the above
//     AmpBeamRange = AmpBeamRange0;
//     dTAmp = -1;
// end

	/* EPM removed and replace with below code as per Jean-Marie and Xavier
	double dTAmp, N;

	m_pAmpWinMin[beam] = ampIndex[0]-1;
	m_pAmpWinMax[beam] = ampIndex[samples-1]-1;

	if (val_qf_1 && (2.0f * Sigma1 + 1.0f) > m_N0Corr) {
		val_qf_1 = true;
		AmpBeamRange = AmpBeamRange1;
		m_pAmpWinMin[beam] = ampIndex[iMin]-1;
		m_pAmpWinMax[beam] = ampIndex[iMax-1]-1;
		N = (2.0f * Sigma1 + 1.0f) / m_N0Corr;
		dTAmp = m_B * sqrt((N-1) * (N+1) / N)* m_N0Corr;
	} else if (val_qf && (4.0f * Sigma + 1.0f) > m_N0Corr) {
		N = (4.0f * Sigma + 1.0f) / m_N0Corr;
		dTAmp = m_B * sqrt((N-1) * (N+1) / N)* m_N0Corr;
	} else {
		dTAmp = -1.0;
	}
*/
	double dTAmp, N;



	m_pAmpWinMin[beam] = ampIndex[0]-1;

	m_pAmpWinMax[beam] = ampIndex[samples-1]-1;



//	Proposition de calcul par analogie avec le code MatLab.
//	La constante 0.4 remplace de fa�on homog�ne les facteurs 0.35 et 0.44 de la version MatLab.
	if (val_qf_2 && (2.0f * Sigma2 + 1.0f) > m_N0Corr) {

		// ??? Int�r�t : val_qf_2 = true;

		AmpBeamRange = AmpBeamRange2;

		m_pAmpWinMin[beam] = ampIndex[iMin]-1;

		m_pAmpWinMax[beam] = ampIndex[iMax-1]-1;

		N = (Sigma2 + 1.0f);

		dTAmp = 0.4 * sqrt(N * m_N0);	// On utilise bien m_N0 dans la version MatLab
	}
	else if (val_qf_1 && (2.0f * Sigma1 + 1.0f) > m_N0Corr) {

		val_qf_1 = true;

		AmpBeamRange = AmpBeamRange1;

		m_pAmpWinMin[beam] = ampIndex[iMin]-1;

		m_pAmpWinMax[beam] = ampIndex[iMax-1]-1;

		N = (Sigma1 + 1.0f);

		dTAmp = 0.4 * sqrt(N * m_N0);	// On utilise bien m_N0 dans la version MatLab

	} else if (val_qf && (4.0f * Sigma + 1.0f) > m_N0Corr) {

        // !ALTRAN - 7 begin
		//N = (Sigma + 1.0f);

		//dTAmp = 0.4 * sqrt(N * m_N0);	// On utilise bien m_N0 dans la version MatLab
        /* Matlab
        UnSurN = N_0/Sigma;
        dTAmp = Coeff * Sigma * sqrt(UnSurN);
        */
        dTAmp = 0.4 * Sigma * sqrt(m_N0/Sigma);
        // !ALTRAN end


	} else {

		dTAmp = -1.0;

	}

// if dTAmp > 0
//     AmpBeamQF = AmpBeamRange / dTAmp;
//     AmpBeamQF = log10(AmpBeamQF);
// else
//     AmpBeamQF = NaN;
// end
	m_pRangeAmplitude[beam] = (float)(AmpBeamRange-1.0);
	if (dTAmp > 0.0) {
		m_pQualityAmplitude[beam] = (float)log10(AmpBeamRange / dTAmp);
	} else {
		m_pQualityAmplitude[beam] = 0.0f;
	}

	return true;
}



// %% Function testing the shape of the enveloppe
// function val_qf=test_shape(Amp,x) 
//     level_abs   = 0.7;
//     bell_int_1  = cumsum(Amp.^2/sum(Amp.^2));
//     x_bell_1    = (x-(x(end)+x(1))/2);
//     x_bell_1    = x_bell_1/x_bell_1(end)/2;
//     [~,u_min_1] = min(abs(x_bell_1+0.3));
//     [~,u_max_1] = min(abs(x_bell_1-0.3));
//     
//     level_temp = abs(bell_int_1(u_max_1) - bell_int_1(u_min_1));
//     if level_temp > level_abs
//         val_qf = 1;
//     else
//         val_qf = 0;
//     end
bool CIFremerBottomDetect::_amplitude_test_shape (DWORD *ampIndex, float * amp, int samples)
{
	//% 	bell_int = cumsum(Amp.^2/sum(Amp.^2));
	//% 	x_bell    = (x-(x(end)+x(1))/2);
	//% 	x_bell    = x_bell/x_bell(end)/2;
	//% 	[ppp,u_min] = min(abs(x_bell+0.3));
	//% 	[ppp,u_max] = min(abs(x_bell-0.3));
	//% 	
	//% 	if abs(bell_int(u_max)-bell_int(u_min))>0.78
	//% 		val_qf = 1;
	//% 	else
	//% 		val_qf = 0;
	//% 	end
	
	float xS = (float) ampIndex[0], xE = (float)ampIndex[samples-1];
	float xD = xE-xS, xM = (xE+xS)/(xE-xS)/2.0f;
	float uMin = 1.0f, uMax = 1.0f;
	float aMin = 0.0f, aMax = 0.0f, aSum = 0.0f;
	float x_bell;
	
	for (int iS = 0; iS < samples; iS++)
	{
		aSum += amp[iS]*amp[iS];
		x_bell = ampIndex[iS]/xD-xM;
		if (fabsf(x_bell+0.3f) < uMin) { uMin = fabsf(x_bell+0.3f); aMin = aSum; }
		if (fabsf(x_bell-0.3f) < uMax) { uMax = fabsf(x_bell-0.3f); aMax = aSum; }
	}
    
	return (fabs(aMax-aMin) > 0.75f*aSum);
}


// !ALTRAN - 42 begin
bool CIFremerBottomDetect::_amplitude_test_double_shape (DWORD *ampIndex, float * amp, int samples, float R0, float AmBeamRange, float lineWidth, int *subAmpIndex, int *subAmpsize)
{

/* Matlab code
nAmp  = length(Amp);
nAmp2 = floor(nAmp/2);
subAmp1 = 1:min(nAmp2+3-1,nAmp);
subAmp2 = max(nAmp2-3, 1):nAmp;
testAmp0 = test_shape(Amp, x);
testAmp1 = test_shape(Amp(subAmp1), x(subAmp1));
testAmp2 = test_shape(Amp(subAmp2), x(subAmp2));

testProcheSpeculaire = ((AmpBeamRange/R0) < 1.03);


if all([testAmp0, testAmp1, testAmp2 testProcheSpeculaire])
    Max1 = max(Amp(subAmp1));
    Max2 = max(Amp(subAmp2));
    
    if (Max2 > (Max1/20))
        subOut = subAmp2;
    elseif (Max1 > (Max2/20))
        subOut = subAmp1;
    else
        subOut = [];
    end
else
    subOut = [];
end
*/

	int nAmp2 = int(floorf(samples/2) - 1);
	bool testAmp0;
	bool testAmp1;
	bool testAmp2;
	bool testProcheSpeculaire;
	float Max1, Max2;
	float subAmp1Limit, subAmp2Limit;
	bool result;

	subAmp1Limit = samples;
	if((nAmp2+3-1) < samples) subAmp1Limit=nAmp2+3-1;

	subAmp2Limit = 0;
	if((nAmp2-3) > 0) subAmp2Limit=nAmp2-3;

	testAmp0 = _amplitude_test_shape(ampIndex, amp, samples);
	testAmp1 = _amplitude_test_shape(ampIndex, amp, subAmp1Limit);
	testAmp2 = _amplitude_test_shape(ampIndex+nAmp2, amp+nAmp2, samples-nAmp2);

	testProcheSpeculaire = ((AmBeamRange/R0) < 1.03);

	if(testAmp0==true && testAmp1==true && testAmp2==true && testProcheSpeculaire==true){
		Max1 = -FLT_MAX;
		for(int i=0;i<nAmp2;i++)
		{
			if(Max1 < amp[i])
			{
				Max1 = amp[i];
			}
		}

		Max2 = -FLT_MAX;
		for(int i=nAmp2;i<samples;i++)
		{
			if(Max2 < amp[i])
			{
				Max2 = amp[i];
			}
		}

		if (Max2 > (Max1/20))
		{
			*subAmpIndex = subAmp2Limit;
			*subAmpsize = samples - subAmp2Limit;
			result = true;
		}
		else if(Max1 > (Max2/20))
		{
			*subAmpIndex = subAmp1Limit;
			*subAmpsize = subAmp1Limit;
			result = true;
		}
		else
		{
			result = false;
		}
	}
	else
	{
		result = false;
	}

	return result; 

}
// !ALTRAN End



// --------------------------------------------------------------------
// _amplitude_detection - implementation of ResonDetectionAmplitudeImage
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT] m_pRangeAmplitude
// [OUTPUT] m_pSamplesAmp
// [OUTPUT] m_pQualityAmplitude
// [OUTPUT] m_pQF2Amplitude
//
// [INPUT]  m_pMatrix    - Amplitude Mask
// [INPUT]  m_MaskMin    - Minimum valid sample
// [INPUT]  m_MaskMax    - Maximum valid sample
// [TEMP]   m_pfSamplesG, m_pfSamplesH - Beam Amplitude
// [TEMP]   m_pdSamplesA - Valid Indices
//
// [MATLAB] Amp                === m_pMatrix
// [MATLAB] R0                 === m_R0 (not used)
// [MATLAB] BeamAngles         === m_pBeamAngles (not used)
// [MATLAB] SystemSerialNumber === m_dwDeviceId (not used)
// [MATLAB] SampleRate         === m_fSampleRate (not used)
// [MATLAB] TxPulseWidth       === m_fTransmitPulseLength (not used)
// [MATLAB] iBeam0             === m_IncidentBeam (not used)
// [MATLAB] AmPingRange        === m_pRangeAmplitude
// [MATLAB] AmpPingQF          === m_pQualityAmplitude
// [MATLAB] AmpPingSamplesNb   === (not used / not calculated)
// [MATLAB] AmpPingQF2         === m_pQF2Amplitude
//
// Sub-functions calls:
//		_amplitude_single
//		
// Used in sub-functions:
//		m_pfSamplesB
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonDetectionAmplitudeImage
//
bool CIFremerBottomDetect::_amplitude_detection (void)
{
	bool retValue = false;
	bool retBeam;

	int iB, iS, iNS;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	DWORD *Mask = (DWORD *)m_pMatrix[0];
	DWORD * NaNRange = (DWORD*) m_pRangeAmplitude;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		m_pMaskMin[iB] = 0;
		m_pMaskMax[iB] = 0;

		if (iB == 100)
			int toto=1 ;


//%	subSample = find(~isnan(Amp) & (Amp ~=0 ));
		for (iNS = 0, iS = m_MaskMin; iS < m_MaskMax; iS++)
			if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB])/* && fabs(m_pMatrix[iS][iB]) > FLT_EPSILON*2*/)
			{
				if (!m_pMaskMin[iB]) m_pMaskMin[iB] = iS;
				m_pMaskMax[iB] = iS;

				m_pfSamplesA[iNS] = m_pMatrix[iS][iB];
				m_pdSamplesA[iNS] = iS+1;
				iNS++;
			}

// %% First test : checking if the signal is not empty

		if (!iNS)
		{
			retBeam = false;
		}

		// !ALTRAN - 37 Begin
				// %% Second test (15/12/2010) : if the last sample of the signal corresponds
				// 	% to the maximum sample reachable in range then do not compute the
				//	% sounding because it will be either a biased one or a false detection
		/*else if (m_pMaskMax[iB] == (DWORD)m_iOutputSamples-1) {
			m_pRangeAmplitude[iB]	= -1.0f;
			m_pSamplesAmp[iB]       = 0;
			m_pQualityAmplitude[iB] = 0.0f;
			m_pQF2Amplitude[iB]     = 0.0f;
			continue;
		}*/ 
		// !ALTRAN End
		else
		{
			// !ALTRAN begin
			retBeam = _amplitude_single(iB, m_pdSamplesA, m_pfSamplesA, iNS, m_iOutputSamples);
			// !ALTRAN end
		}

		if (!retBeam)
		{
			NAN(NaNRange[iB]);
			m_pSamplesAmp[iB]       = 0;
			m_pQualityAmplitude[iB] = 0.0f;
			m_pQF2Amplitude[iB]     = 0.0f;
			continue;
		}

		retValue = true;
	}

	return retValue;
}



// !ALTRAN - 41 Begin
// --------------------------------------------------------------------
// _BDA_compensAmpByMHorzMedian - Normalization by median value 
//
// Return value: no return
//
// [INPUT/OUPUT]  p_matrix     === matrix to normalize
// [INPUT]	      p_nbBeams    === Number of beams
// [INPUT]	      p_nbSamples  === Number of samples
// [TEMP]	      m_pfBeamsA, m_pfBeamsA
//
// [MATLAB] AmpImage           === amp (full beam)
//
// Used in functions:
//		_detection_estimate
//
// MatLab reference:
//		BDA_compensAmpByMHorzMedian
//
void CIFremerBottomDetect::_BDA_compensAmpByMHorzMedian(char* p_RAWData, int p_nbBeams, int p_nbSamples)
{
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	_raw_data_type * dataPrevious;
	float med;

	for(int i=0; i<p_nbSamples; i++)
	{
		// Compute the median on current sample
		dataPrevious = data;
		for(int j=0; j<p_nbBeams; j++,data++)
		{
			m_pfBeamsA[j] = data->amp;
		}
		med =  _median(m_pfBeamsA, m_pfBeamsB, p_nbBeams);

		// Normalize the amplitude data on current sample
		if(med > 0)
		{
			for(int j=0; j<p_nbBeams; j++,dataPrevious++)
			{
				dataPrevious->amp = dataPrevious->amp / med;
			}
		}
		else
		{
			for(int j=0; j<p_nbBeams; j++,dataPrevious++)
			{
				dataPrevious->amp = 0;
			}
		}
	}
}
// !ALTRAN End
