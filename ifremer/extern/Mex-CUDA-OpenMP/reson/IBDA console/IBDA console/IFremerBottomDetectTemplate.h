/***************************************************************************
****************************************************************************

   FILE: IFremerBottomDetect.h: declaration of the CIFremerBottomDetect class.

   Copyright � 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    March-April 2008
   Author:  Yevgeniy Shafirovich
   Change:  Initial Implementation of Bottom Detection Methods Proposed by IFremer

****************************************************************************
***************************************************************************/

#if !defined(_IFREMERBOTTOMDETECT_H__INCLUDED_)
#define _IFREMERBOTTOMDETECT_H__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if !defined(COMPILE_7K_CENTER)
#pragma message (">>> Compile stand-alone.")
#	define RUN_STAND_ALONE
#endif

#include "BottomDetectCommon.h"
#include <math.h>
#include "MathUtils.h"
//#include "Storage.h"

#ifdef RUN_STAND_ALONE
	typedef struct tagBeamRecord					// Used for 7058
	{
		WORD             beam_number;
		DWORD            min_sample;				// First Sample in the Beam
		DWORD            bd_sample;					// Bottom Detect Sample
		DWORD            max_sample;				// Last Sample in the Beam
	} BEAM_RECORD;

#define PI 3.141592653589793f
#define D2R (PI / 180)
#define R2D (180 / PI)
#define limit(x, lo, hi)  ((x) < (lo) ? (lo) : ((x) > (hi) ? (hi) : (x)))

#else
#	include "7k.h"
#endif

#define between(x, lo, hi)  ((x) >= (lo) && (x) <= (hi))

#ifndef STRING2
#define STRING2(x) #x
#define STRING(x) STRING2(x)
#endif

#define REDUCTION_SAMPLE_SIZE 200
#define REDUCTION_MATRIX_SIZE 210
#define RANGE_CUTOFF_7111     160	// 4/5 of above

// convert sample # to depth or vice-versa
// S  = Sample number
// B  = Beam number
// #define S2D(S,B)     (((S) * m_fSoundVelocity * cosf(m_pBeamAngles[(B)])) / (2.0f * m_fSampleRate))
// #define D2S(D,B)     (((D) * 2.0f * m_fSampleRate) / (cosf(m_pBeamAngles[(B)]) * m_fSoundVelocity))
// #define D2SR(D,B,A)  (((D) * 2.0f * m_fSampleRate) / (cosf(m_pBeamAngles[(B)]+(A)) * m_fSoundVelocity))

#define S2D(S,B)     (((S) * 1500.0f * cosf(m_pBeamAngles[(B)])) / (2.0f * m_fSampleRate))
#define D2S(D,B)     (((D) * 2.0f * m_fSampleRate) / (cosf(m_pBeamAngles[(B)]) * 1500.0f))
#define D2SR(D,B,A)  (((D) * 2.0f * m_fSampleRate) / (cosf(m_pBeamAngles[(B)]+(A)) * 1500.0f))

// convert sample # to range or vice-versa
#define S2R(S)  (((S) * m_fSoundVelocity) / (2.0f * m_fSampleRate))
#define R2S(R)  (((R) * 2.0f * m_fSampleRate) / (m_fSoundVelocity))

#define INRANGE(x,a,b) (((x)>=(a))&&((x)<=(b)))

// Converts RDC phase value to degrees
#define PHS2DEG(p)      ((p)*5.4931640625e-003f)
#define PHS2DEG_FLIP(p) (((p) == -32768) ? 180.0f : (p)*5.4931640625e-003f)
// Converts RDC phase value to radians
#define PHS2RAD(p) ((p)*9.587379924285257e-005f)

#define NAN(a)		a = 0xffffffff
#define NANf(a)		*(DWORD*)&(a) = 0xffffffff
#define ISNAN(a)	(a == 0xffffffff)
#define ISNANf(a)	(*(DWORD*)&(a) == 0xffffffff)
#define ISEQUAL(a,b)    (fabsf((a)-(b)) < FLT_EPSILON*2)

#define SIGN(a,b)	a = (a & 0x7fffffff) | (b & 0x80000000)
#define INFP(a)	    a = 0x7f800000
#define INFM(a)	    a = 0xff800000
#define INFS(a,b)   a = 0x7f800000 | (b & 0x80000000)
#define ISINF(a)    ((a & 0x7fffffff) == 0x7f800000)

#define PHASE_UNWRAP_SEUIL 270

#define PIf (3.1415926535f)
#define D2Rf(a) ((a) * PIf / 180.0f)
#define R2Df(a) ((a) * 180.0f / PIf)

#define INTERP2(x1,x2,x,y) ((y[x1]) + ((y[x2])-(y[x1]))/((x2)-(x1))*((x)-(x1)))

#define IF1_DETECTION_PHASE		0x8
#define IF1_DETECTION_AMP		0x4
#define IF1_QUALITY_QF1_PASS	0x2
#define IF1_QUALITY_QF2_PASS	0x1

typedef struct  
{
	unsigned short amp;
	         short phs;
} _raw_data_type;


#define type_float float

// DEBUG Flags

#define DEBUG_OUTPUT_INPUT		        0x0001
#define DEBUG_OUTPUT_REDUCTION		    0x0002
#define DEBUG_OUTPUT_PREPROC_SHORT		0x0004
#define DEBUG_OUTPUT_PREPROC_LONG		0x0008
#define DEBUG_OUTPUT_MASKAMP_SHORT		0x0010
#define DEBUG_OUTPUT_MASKAMP_LONG		0x0020
#define DEBUG_OUTPUT_DETECTION_AMP		0x0040
#define DEBUG_OUTPUT_FILTER_PHS			0x0080
#define DEBUG_OUTPUT_MASKPHS_SHORT		0x0100
#define DEBUG_OUTPUT_MASKPHS_LONG		0x0200
#define DEBUG_OUTPUT_DETECTION_PHS		0x0400
#define DEBUG_OUTPUT_SYNTHESIS			0x0800

#define DEBUG_OUTPUT_NONE				0x4000

struct Sfloat {
	unsigned int fraction : 23; // fractional part
	unsigned int exponent : 8;	// exponent + 0x7F
	unsigned int sign : 1;		// sign bit
};

class C7kDataProcess;

class CIFremerBottomDetect  
{
public:
	// Required sequence of calls for each ping (* marks optional calls):
	//
	// 1) PreloadProcessing - must be the first call for the ping. Otherwise invalid settings may be used.
	// 2) * SetDepthGates
	//    * SetRangeGates
	// 3) 
	CIFremerBottomDetect();
	~CIFremerBottomDetect();

	// PreloadProcessing must be called in the beginning of each ping. It resets all parameters,
	// but preserves memory allocations that can be reused.
	// This function copies some ping parameters and sores pointers to the following data:
	// R7010 - Gains array
	// R7004 - Beam angles
	// - If any of these records are not yet build, this call will fail and return false.
	// This function also stores pointers to the following data:
	// C7kDataProcess::m_pRAW
	//
	// CIFremerBottomDetect does not modify any data referenced by these pointers, however,
	// changing any of data referenced by these pointers in between subsequent calls to
	// CIFremerBottomDetect members, may lead to the undefined behavior.
#ifdef RUN_STAND_ALONE
	bool PreloadProcessing  (char * filename);
#else
	bool PreloadProcessing  (C7kDataProcess * pPO);
#endif

	void SetDepthGates (float minDepth, float maxDepth, float roll);
	void SetRangeGates (float minRange, float maxRange);

	bool CalculateMask   ( void );
	bool BottomDetection (bdresults * BDResults, int &BeamNumber);

	void CreateState ();
	void DestroyState();
	void SaveState (char * filename);
	
private:
	void _matrix_reduction();
	void _matrix_compensation();

	bool  _r0_estimation   (WORD sample_start, float SeuilAmp);
	WORD  _strip_estimation(WORD beam_start, WORD beam_end, WORD sample_start, WORD sample_end, float SeuilAmp);
	bool  _strip_amplitude (float * MoyHorz, WORD sample_start, int R0, WORD nbSamples, float SeuilAmp);
	float _strip_slope     (float * S, WORD nbSamples);

	bool _detection_estimate( void );
	bool _mask_beam_single( WORD Beam, float CenterPoint, DWORD WMin, DWORD Width1, DWORD Width2, float R0Seuil, WORD step, float R0, bool checkR0 = false );
	float _amplitude_single_reduit ( float *amp, int iSampleBeg, int iSampleEnd, int iBeam, int index);
	bool _mask_7111 (void);
	bool _mask_7150 (void);
	void _suppress_steps (float * SamplesFiltre);
	bool _nominal_beams_7111 (void);
	void _mask_dilate(DWORD * beams, WORD nBeams, WORD dilate);

	bool _amplitude_detection (void);
	bool _amplitude_single (WORD beam, DWORD *ampIndex, float * amp, int samples, int allSamples);
	bool _amplitude_test_shape (DWORD *ampIndex, float * amp, int samples);

	bool _phase_mask( void );
	void _phase_mask_line ( int sample, float * amp, DWORD * subBeams, int subLength );
	bool _phase_filtering( void );
	void _phase_clean_mask ( void );
	bool _phase_detection( );
	bool _phase_detection_beam( DWORD * subSamples, float * Amp, float * Amp2, float * Phs, int nSamples, int Beam );
	bool _phase_detection_beam_single( DWORD *subSamples, float *Phase, float *Amp, float *Amp2, int iter, int &nSamples, const int Beam, const float NechRes, float &R, float &QF, float &Pente, float &Eqm, int &NbSamples);
	bool _phase_regression( DWORD * subSamples, float * Phs, float * Weights, int nSamples, float& A, float& B, float& X0, float& Eqm );
	bool _phase_calcul( float * subSamples, float * Phs, float * Weights, int nSamples, float& A, float& B, float& X0, float& Eqm );
	bool _phase_test(const DWORD * subSamples, const int nSamples, const float NechRes) const;

	bool _detection_synthese( void );

	void CleanUpBeam();
	void CleanUpAll ();

	bool MasqueAmp(int beam, int sample) const;
	void MasqueAmp(void);
	bool MasquePhs(int beam, int sample) const;
	void MasquePhs(int beam, int sample, bool value);
	void MasquePhs(int beam, int sample1, int sample2, bool value);
	void MasquePhs(bool value);

	// Math Functions
	void  _gradient  (const float *X, float *G,         const int nX);
	float _nanStd    (const float *X,                   const int nX, const DWORD * filter);
	DWORD _median    (      DWORD *X, DWORD *T,         const int nX);
	float _median    (      float *X, float *T,         const int nX);
// 	template <class DATA> float _barycentre(const DATA *X, const int MinX,   const int nX, float &Sigma, float *T, int order = 2);
// 	template <class DATA> float _barycentre(const DATA *X, const DWORD * iX, const int nX, float &Sigma, float *T, int order = 2);
// 	template <class DATA> DATA  _cumsum    (const DATA *X, float *S, const int nX, const bool normalize);
	float _mean      (const float *X,                   const int nX);
	float _nanmean   (const float *X,                   const int nX);

public:
	int  m_Debug;
	LARGE_INTEGER liStart;
	LARGE_INTEGER liCounter;
	char*	m_probe_path; //!IFREMER directory path to store the software probes

	//C7kStorage * _storage;

private:

	// Notes on variables notation:
	// [INPUT]     - variables are populated via calls by DataProcess
	// [OUTPUT]    - variables are designated as resulting data and returned/copied to DataProcess
	// [INTERNAL]  - variables are designated for internal interprocess parameters
	// [REFERENCE] - pointer variables that directly reference data within the parent DataProcess. DO NOT alter any data referenced by these variables
	// [COPY]      - variables are direct copy of the data from the parent DataProcess. It is assumed that format of the data is preserved. DO NOT alter
	// [TEMPORARY] - variables designated to be used as a temporary data storage. The values can be altered by any function.

	// Temporary Variables
	DWORD   m_nAllocSamples;				// [INTERNAL]  Number of Allocated Samples. Used to reduce number of allocations.
	DWORD   m_nMaskSamples;					// [INTERNAL]  Number of Allocated Samples. Used to reduce number of allocations.
	float * m_pfSamplesA;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE)
	float * m_pfSamplesB;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
	float * m_pfSamplesC;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
	float * m_pfSamplesD;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
	float * m_pfSamplesE;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
	float * m_pfSamplesF;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
	float * m_pfSamplesG;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
	float * m_pfSamplesH;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
	double* m_pfdSamples;					// [TEMPORARY] Union with m_pfSamplesG and m_pfSamplesH
	DWORD * m_pdSamplesA;					// [TEMPORARY] Per Sample DWORD vector
	DWORD * m_pdSamplesB;					// [TEMPORARY] Per Sample DWORD vector
	DWORD * m_pdBeamsA;						// [TEMPORARY] Per Beam   DWORD vector
	DWORD * m_pdBeamsB;						// [TEMPORARY] Per Beam   DWORD vector
	DWORD * m_pdBeamsC;						// [TEMPORARY] Per Beam   DWORD vector
	DWORD * m_pdBeamsD;						// [TEMPORARY] Per Beam   DWORD vector
	float * m_pfBeamsA;						// [TEMPORARY] Per Beam   float vector
	float * m_pfBeamsB;						// [TEMPORARY] Per Beam   float vector
	float * m_pfBeamsC;						// [TEMPORARY] Per Beam   float vector
	float * m_pfBeamsD;						// [TEMPORARY] Per Beam   float vector
	float ** m_pfReduite;					// [TEMPORARY] Reduced Series (Beams * REDUCTION_SAMPLE_SIZE float matrix)

	// Gates
	// !ALTRAN - 1 Begin
	float * m_pGatesMin, * m_pGatesMax;		// [INPUT]     Absolute limits for detection.   Per Beam in Samples
	// !ALTRAN End

	// Amplitude Mask variables
	float ** m_pAmpReduite;					// [INTERNAL]  Reduced Amplitude series
	int      m_ReduiteStep;					// [INTERNAL]  Step for Amplitude series reduction
	WORD     m_ReduiteSize;					// [INTERNAL]  Number of Samples in Amplitude series reduction
	float *  m_pR1, * m_pR2;				// [INTERNAL]  Approximate Center of the Signal. Per Beam in Samples
	// !ALTRAN - 29 Begin
	float *  m_pMax;                  // [INTERNAL]  max of the Signal. Per Beam in Samples
	int m_iBeamBeg;                  		// [INTERNAL]  max of the Signal. Per Beam in Samples
	int m_iBeamEnd;							// [INTERNAL]  max of the Signal. Per Beam in Samples
	// !ALTRAN End
	bool  *  m_pFlagBeams;					// [INTERNAL]  Estimation validity. Per Beam.
	int      m_nStrips;						// [INTERNAL]  Number of strips for mask processing

	// Phase Mask variables
	float ** m_pMatrix;						// [INTERNAL]  Phase series in degrees (FLT_MAX == NaN)
	DWORD *  m_pMaskMin,  * m_pMaskMax;		// [OUTPUT]    Calculated limits for detection. Per Beam in Samples
	DWORD *  m_pMaskWidth;					// [INTERNAL]  Calculated limits for detection. Per Beam in Samples
	int      m_MaskMin,     m_MaskMax;		// [INTERNAL]  Min and Max Samples included in the mask for all beams

	// Interprocess variables
	float    m_R0;							// [INTERNAL]  First appearance of the signal. In Samples
	WORD     m_IncidentBeam;				// [INTERNAL]  Number of the Beam of Normal Incidence on Sea Floor
	float    m_NechRes0;
	float	 m_dTPulse;
	float	 m_N0;
	float	 m_N0Corr;
	double   m_B;

	// 7111 Specific Processing (not defined for other systems)
	bool  *  m_pNominalBeams;				// [INTERNAL]  Nominal beams
	
	// Amplitude Detection
	float *  m_pRangeAmplitude;				// [INTERNAL]  Amplitude Detection. In Samples, per Beam
	DWORD *  m_pSamplesAmp;					// [INTERNAL]  Number of samples used for Amplitude Detection, per Beam
	float *  m_pQualityAmplitude;			// [INTERNAL]  Quality of Amplitude Detection. Per Beam
	float *  m_pQF2Amplitude;				// [INTERNAL]  Amplitude Detection QF2. Per Beam
	DWORD *  m_pAmpWinMin, * m_pAmpWinMax;	// [OUTPUT]    Amplitude Detection Window.

	// Phase Detection
	float    m_DeltaTeta;
	float *  m_pRangePhase;					// [INTERNAL]  Phase Detection. In Samples, per Beam
	float *  m_pQualityPhase;				// [INTERNAL]  Quality of Phase Detection. Per Beam
	DWORD *  m_pSamplesPhase;				// [INTERNAL]  Number of samples used for Phase Detection, per Beam
	float *  m_pERMPhase;					// [INTERNAL]  Mean Square Error for Phase Detection. Per Beam
	float *  m_pSlopePhase;					// [INTERNAL]  Slope for Phase Detection. Per Beam
	DWORD *  m_pPhsWinMin, * m_pPhsWinMax;	// [OUTPUT]    Phase Detection Window.

	// Bottom Detection
	float *  m_pDetectionRange;				// [INTERNAL]  Bottom Detection. In Samples, per Beam
	float *  m_pDetectionQuality;			// [INTERNAL]  Quality of Detection. Per Beam
	char  *  m_pDetectionType;				// [INTERNAL]  Detection Type. Per Beam

	// Ping Parameters
	DWORD   m_dwDeviceId;					// [COPY]      Device ID
	float * m_pBeamAngles;					// [REFERENCE] Beam Angles. Per Beam in radians
	char  * m_pRAWData;						// [REFERENCE] Amplitude & Phase series
//	float * m_pGains;						// [REFERENCE] Calibrated Gains. Per Sample in dB
	DWORD   m_dwReceiveBeams;				// [COPY]      Number of Beams
	float   m_fSampleRate;					// [COPY]      Rx Sampling Rate
	float   m_fSoundVelocity;				// [COPY]      Surface Sound Velocity
	int     m_iOutputSamples;				// [COPY]      Number of Samples in the ping
	float   m_fAbsorption;					// [INTERNAL]  Nominal Absorption
	float   m_fCenterFrequency;				// [COPY]      Tx/Rx Center Frequency
	float	m_fReceiveBeamWidth;			// [COPY]      Rx Beam Width in radians
	float	m_fTransmitPulseLength;			// [COPY]      Tx Pulse Length in seconds
	DWORD   m_dwPingNumber;
	
// 	DECLARE##VAR		(X, WORD,			m_wSystemEnumerator)																							
// 	DECLARE##VAR		(X, ULARGE_INTEGER,	m_ulnSerialNumber)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwReceiveElements)																							
// 	DECLARE##VAR		(X, DWORD,			m_dwReceiveBeamRows)																							
// 	DECLARE##VAR		(X, WORD,			m_wMultiPing)																									
// 	DECLARE##VAR		(X, DWORD,			m_bForwardLooker)			DECLARE##COMMENT("TRUE for fwd looking sonars")										
// 																																							
// 	DECLARE##COMMENT	("Records 7008, 7018, 7028")																										
// 	DECLARE##VAR		(X, DWORD,			m_dw70x8MinBeam)			DECLARE##COMMENT("0 to N-1, N = Beams")												
// 	DECLARE##VAR		(X, DWORD,			m_dw70x8MaxBeam)			DECLARE##COMMENT("0 to N-1, N = Beams")												
// 																																							
// 	DECLARE##COMMENT	("Record 7008")																														
// 	DECLARE##VAR		(X, DWORD,			m_dwSnippetEnable)			DECLARE##COMMENT("0 - Off (all samples), 1 - On (Snippet window size samples)")		
// 	DECLARE##VAR		(X, DWORD,			m_dwSnippetMaxWindowSize)	DECLARE##COMMENT("In samples")														
// 	DECLARE##VAR		(X, DWORD,			m_dwSnippetMinWindowSize)	DECLARE##COMMENT("In samples")														
// 	DECLARE##VAR		(X, DWORD,			m_dwCalSnippetFlags)		DECLARE##COMMENT("See FLAG_7058_XXX")												
// 	DECLARE##VAR		(X, float,			m_fCalSnippetWindowSize)	DECLARE##COMMENT("In dB")															
// 	DECLARE##VAR		(X, BYTE,			m_dw7008RowColFlag)			DECLARE##COMMENT("0 - Beam followed by samples, 1 - Sample follows by beams")		
// 																																							
// 	DECLARE##VAR		(X, DWORD,			m_dwControlFlags)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwReceiveFlags)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwTransmitFlags)																								
// 																																							
// 																																							
// 	DECLARE##VAR		(X, float,			m_fAppliedFrequency)																							
// 	DECLARE##VAR		(X, float,			m_fStartChirpFrequency)																							
// 	DECLARE##VAR		(X, float,			m_fStopChirpFrequency)																							
// 																																							
// 	DECLARE##VAR		(X, DWORD,			m_dwTransmitPulseType)																							
// 	DECLARE##VAR		(X, DWORD,			m_dwTxPulseEnvelopeType)																						
// 	DECLARE##VAR		(X, float,			m_fTxPulseEnvelopeParameter)																					
// 																																							
// 	DECLARE##VAR		(X, float,			m_fTransmitBeamSteeringZ)																						
// 																																							
// 	DECLARE##VAR		(X, float,			m_fUniformReceiveBeamWidthAlong)																				
// 	DECLARE##VAR		(X, float,			m_fUniformTransmitBeamWidthZ)																					
// 																																							
// 	DECLARE##VAR		(X, float,			m_fTransmitBeamFocalPoint)																						
// 																																							
// 	DECLARE##VAR		(X, DWORD,			m_dwTransmitBeamWeightingType)																					
// 	DECLARE##VAR		(X, float,			m_fTransmitBeamWeightingParameter)																				
// 																																							
// 	DECLARE##ARRAY		(X, float,			m_fUniformReceiveBeamWidthAcross, 1024)																			
// 																																							
// 	DECLARE##VAR		(X, float,			m_fBottomDetectionInfo1)					DECLARE##COMMENT("User input (range min)")							
// 	DECLARE##VAR		(X, float,			m_fBottomDetectionInfo2)					DECLARE##COMMENT("User input (range max)")							
// 	DECLARE##VAR		(X, float,			m_fBottomDetectionInfo3)					DECLARE##COMMENT("User input (depth min)")							
// 	DECLARE##VAR		(X, float,			m_fBottomDetectionInfo4)					DECLARE##COMMENT("User input (depth max)")							
// 	DECLARE##VAR		(X, float,			m_fBottomDetectionInfo5)					DECLARE##COMMENT("User input Adaptive Gate (depth min)")			
// 	DECLARE##VAR		(X, float,			m_fBottomDetectionInfo6)					DECLARE##COMMENT("User input Adaptive Gate (depth max)")			
// 	DECLARE##VAR		(X, WORD,			m_iAGWindow)								DECLARE##COMMENT("Adaptive gate Window 1 - 100")					
// 																																							
// 	DECLARE##COMMENT	("tx array location relative to rx array, meters, relative to: rx facing +y, projector on top")										
// 	DECLARE##VAR		(X, float,			m_fRxToTxOffsetX)							DECLARE##COMMENT("positive: tx right of rx")						
// 	DECLARE##VAR		(X, float,			m_fRxToTxOffsetY)							DECLARE##COMMENT("positive: tx in front of rx")						
// 	DECLARE##VAR		(X, float,			m_fRxToTxOffsetZ)							DECLARE##COMMENT("positive: tx above rx")							
// 																																							
// 	DECLARE##VAR		(X, float,			m_fHeadTiltX)																									
// 	DECLARE##VAR		(X, float,			m_fHeadTiltY)																									
// 	DECLARE##VAR		(X, float,			m_fHeadTiltZ)																									
// 																																							
// 	DECLARE##VAR		(X, int,			m_iPingControl)																									
// 	DECLARE##VAR		(X, SYSMODE,		m_eCenterMode)																									
// 																																							
// 	DECLARE##VAR		(X, BEAMSPACINGTYPE,m_eBeamSpacingMode)							DECLARE##COMMENT("_BS_EQUIANGLE_ or _BS_EQUIDISTANT_")				
// 																																							
// 	DECLARE##VAR		(X, int,			m_iTxSkip)																										
// 	DECLARE##VAR		(X, int,			m_iRxSkip)																										
// 																																							
// 	DECLARE##ARRAY		(X, BYTE,			m_703X_channel_reduction,	RX_MAX_ELEMENTS)																	
// 	DECLARE##VAR		(X, DWORD,			m_703X_start_sample)																							
// 	DECLARE##VAR		(X, DWORD,			m_703X_stop_sample)																								
// 	DECLARE##VAR		(X, WORD,			m_7038_data_type)																								
// 																																							
// 	DECLARE##COMMENT	("Trigger Out")																														
// 	DECLARE##VAR		(X, double,			m_dTriggerLength)																								
// 	DECLARE##VAR		(X, double,			m_dTriggerOffset)																								
// 																																							
// 	DECLARE##VAR		(X, tagCALIB_STATUS,m_eCalibrationStatus)																							
// 																																							
// 	DECLARE##VAR		(X, float,			m_fRange)																										
// 	DECLARE##VAR		(X, float,			m_fMaxPingRate)																									
// 	DECLARE##VAR		(X, float,			m_fPower)																										
// 	DECLARE##VAR		(X, float,			m_fUniformTransmitBeamWidthX)																					
// 	DECLARE##VAR		(X, DWORD,			m_dwUniformTransmitBeamWidthModify)																				
// 	DECLARE##VAR		(X, float,			m_fGain)																										
// 	DECLARE##VAR		(X, WORD,			m_iMP_FreqSpacing)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwMP_FreqBegin)																								
// 	DECLARE##VAR		(X, float,			m_fTxPowerTweak)																								
// 	DECLARE##VAR		(X, TXTYPE,			m_eTxType)																										
// 	DECLARE##VAR		(X, int,			m_iWSetting)																									
// 	DECLARE##VAR		(X, double,			m_dWedgeCoverage)			DECLARE##COMMENT("Size in DEGREES = beam spacing * (#beams-1) in EA mode only")		
// 	DECLARE##VAR		(X, double,			m_dCenterBeamWidth)			DECLARE##COMMENT("In degrees!")														
// 	DECLARE##VAR		(X, BOOL,			m_bPowerShared)				DECLARE##COMMENT("License input  : eg 7150 shares power cabinet")					
// 	DECLARE##VAR		(X, bool,			m_bMPFocusOverride)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwTransmitBeamSteerable)																						
// 	DECLARE##VAR		(X, float,			m_fTransmitBeamSteeringX)																						
// 	DECLARE##VAR		(X, float,			m_fTransmitBeamSteeringXRatio)																					
// 	DECLARE##VAR		(X, float,			m_fTxOffsetFrequency)																							
// 	DECLARE##VAR		(X, DWORD,			m_dwInputMux)																									
// 	DECLARE##VAR		(X, DWORD,			m_dwInputMuxBypass)																								
// 	DECLARE##VAR		(X, int,			m_iTVGLimit)																									
// 	DECLARE##VAR		(X, CCalibrationCalc*, m_pCalibrationCalcObj)	DECLARE##COMMENT("The status of the CalObject changes only between pings...")		
// 	DECLARE##VAR		(X, DWORD,			m_dwUniformTransmitBeamWidthXModify)																			
// 	DECLARE##VAR		(X, double,			m_dRxCeramicSpacing)		DECLARE##COMMENT("Rx Ceramics element spacing (meters!)")							
// 	DECLARE##VAR		(X, RX_SHADE_VECTOR_SOURCE,	m_eRxShadingSource)																						
// 	DECLARE##VAR		(X, int,			m_bRxShadingSourceChanged)																						
// 	DECLARE##VAR		(X, float,			m_fPingPeriod)																									
// 	DECLARE##VAR		(X, BOOL,			m_bRollEnabled)																									
// 	DECLARE##VAR		(X, DWORD,			m_dwRollScaleFactor)																							
// 	DECLARE##VAR		(X, BOOL,			m_bPitchEnabled)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwPitchScaleFactor)																							
// 	DECLARE##VAR		(X, DWORD,			m_dwMultiPingEnabled)																							
// 	DECLARE##VAR		(X, float,			m_fMeasuredPulseLength)																							
// 	DECLARE##VAR		(X, float,			m_fRxCeramicRadius)																								
// 	DECLARE##VAR		(X, float,			m_fRxCeramicCompensation)																						
// 	DECLARE##VAR		(X, float,			m_fLoFactor)																									
// 	DECLARE##VAR		(X, double,			m_dTransmitBaseDelay)																							
// 	DECLARE##VAR		(X, CHIRPTYPE,		m_eChirpSystem)																									
// 	DECLARE##VAR		(X, double,			m_dTxCeramicSpacing)																							
// 	DECLARE##VAR		(X, DWORD,			m_dwTransmitElements)																							
// 	DECLARE##VAR		(X, float,			m_fDroop_dB)            DECLARE##COMMENT("TVG Droop - droop amount")		               
// 	DECLARE##VAR		(X, float,			m_fDroopDurationFactor) DECLARE##COMMENT("TVG Droop - duration multiplier")		      
//    DECLARE##VAR		(X, float,			m_fDroopStartTime)      DECLARE##COMMENT("TVG Droop - start time (seconds)")		      
//    DECLARE##VAR		(X, float,			m_fDroopTolerance)      DECLARE##COMMENT("TVG Droop - tolerance (percent/100)")		      
//    DECLARE##VAR		(X, float,			m_fFPGADelaySeconds)    DECLARE##COMMENT("FPGA delay (seconds)")          		      
// 	DECLARE##VAR		(X, float,			m_fMinGain)																										
// 	DECLARE##VAR		(X, float,			m_fMaxGain)																										
// 	DECLARE##VAR		(X, float,			m_fDepthCorrectionMeters)																										
// 	DECLARE##VAR		(X, ARRAYTYPE,			m_eArrayType)																										
// 	DECLARE##VAR		(X, float,			m_fUniformTransmitBeamWidthXMax)																				
// 	DECLARE##VAR		(X, float,			m_fUniformTransmitBeamWidthXMin)																				
//    DECLARE##VAR		(X, float,			m_fMinPhaseCorrlation)    DECLARE##COMMENT("Phase fit correlation threshold")          		      
// 	DECLARE##VAR		(X, DWORD,			m_dwReceiveBeamWeightingType)																					
// 	DECLARE##VAR		(X, float,			m_fReceiveBeamWeightingParameter)																				
// 	DECLARE##VAR		(X, BYTE,			m_bBottomDetectQualityType)	    																				
// 	DECLARE##VAR		(X, WORD,			m_bBottomDetectQualityThreshold)																				
// 
// 
// 
// 
// #define PING_INFO_SYSTEMS_DECLARATION(X,VAR,ARRAY,COMMENT)																									
// 																																							
// 	DECLARE##COMMENT	("Systems parameters")																												
// 	DECLARE##VAR		(X, float,			m_fSpreadingLoss)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwFirmwareOptions)																							
// 	DECLARE##VAR		(X, BOOL,			m_bAUV)						DECLARE##COMMENT("TRUE for AUVs")													
//     DECLARE##VAR		(X, CTargetStrength *, m_pTargetStrength)		DECLARE##COMMENT("Object required to calculate calibrated backscatter...")			
// 	DECLARE##VAR		(X, float,			m_fVelocity_X)																								
// 	DECLARE##VAR		(X, float,			m_fVelocity_Y)																								
// 	DECLARE##VAR		(X, BOOL,			m_bRecord)					DECLARE##COMMENT("RDR Recording status")											
// 
// 
// #define PING_INFO_PCI_DECLARATION(X,VAR,ARRAY,COMMENT)																										
// 																																							
// 	DECLARE##COMMENT	("System parameters")																												
// 	DECLARE##VAR		(X, TIME_7K,		m_7kPingTime)																									
// 	DECLARE##VAR		(X, DWORD,			m_dwPingNumber)																									
// 	DECLARE##VAR		(X, double,			m_dFWTimeStamp)																									
// 	DECLARE##VAR		(X, int,			m_iStage)																										
// 	DECLARE##VAR		(X, float,			m_fPingPeriodCalc)																								
// 	DECLARE##VAR		(X, float,			m_fRollForDepthGate)																							
// 	DECLARE##VAR		(X, int,			m_iMultipingSequenceNum)																						
// 	DECLARE##VAR		(X, int,			m_iSampleShift)																									
// 	DECLARE##VAR		(X, double,			m_dDepthShift)																									
// 	DECLARE##ARRAY		(X, float,			m_fAngles,	MAX_BEAMS*4)																						
// 	DECLARE##VAR		(X, int,			m_iOutputScaling)																								
// 	DECLARE##VAR		(X, double,			m_dLastPing)																									
// 	DECLARE##VAR		(X, float,			m_fRollAtPingTime)																								
// 	DECLARE##VAR		(X, DWORD,			m_dwRawDataSize)																								
// 	DECLARE##VAR		(X, int,			m_nAlphaDataCards)																								
// 	DECLARE##VAR		(X, int,			m_nDelayMemoryDepth)																							
// 	DECLARE##VAR		(X, BOOL,			m_bCombinedPhase)																								
// 	DECLARE##VAR		(X, float,			m_fRxBandwidth)																									

};

static inline int floori (float x)
{
	return (int) floorf(x);
}

static inline DWORD floordw (float x)
{
	return (x<=0) ? 0 : (DWORD) floorf(x);
}

static inline DWORD floordw (double x)
{
	return (x<=0) ? 0 : (DWORD) floor(x);
}

static inline WORD floorw (float x)
{
	return (x<=0) ? 0 : (WORD) floorf(x);
}

static inline int ceili (float x)
{
	return (x<=0) ? 0 : (int) ceilf(x);
}

static inline DWORD ceildw (float x)
{
	return (x<=0) ? 0 : (DWORD) ceilf(x);
}

static inline DWORD ceildw (double x)
{
	return (x<=0) ? 0 : (DWORD) ceil(x);
}

static inline WORD ceilw (float x)
{
	return (x<=0) ? 0 : (WORD) ceilf(x);
}

#define INCLUDE_IFREMER_MATH_TEMPLATE
#include "IFremerMath.cpp"
#undef  INCLUDE_IFREMER_MATH_TEMPLATE

#endif // _IFREMERBOTTOMDETECT_H__INCLUDED_
