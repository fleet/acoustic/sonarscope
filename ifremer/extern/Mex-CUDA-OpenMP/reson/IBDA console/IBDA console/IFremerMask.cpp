/***************************************************************************
****************************************************************************

   FILE: IFremerMask.cpp

   Implementation of mask calculations functions for CIFremerBottomDetect class.

   Copyright � 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    March 2008
   Author:  Yevgeniy Shafirovich
   Change:  Initial implementation

****************************************************************************
***************************************************************************/

#include "stdafx.h"
#include "IFremerBottomDetect.h"
#include "MathUtils.h"
#include <float.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifdef RUN_STAND_ALONE
#include <stdio.h>
#endif

#define SEUIL_VARIANT (2.0f)

//------------------------------------------------------------------------------
// Preparation of amplitude mask.
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData;
// [OUTPUT] m_pMaskMin,  m_pMaskMax
// [OUTPUT] maskMin,	 maskMax
//
// Subfunctions calls:
//		_detection_estimate
//		_mask_7111
//		_mask_7150
//		
// Used in sub functions:
bool CIFremerBottomDetect::CalculateMask ( void )
{
	if (m_dwDeviceId != 7111 && m_dwDeviceId != 7150)
		return false;

	// !ALTRAN - 41 Begin
	if(m_IdentAlgo == 3)
	{
		_BDA_compensAmpByMHorzMedian(m_pRAWData, m_dwReceiveBeams, m_iOutputSamples);
	}
	// !ALTRAN End

	if (!_detection_estimate()) return false;

	switch (m_dwDeviceId)
	{
	case 7111:
		if (!_mask_7111())            return false;
		break;
	case 7150:
		if (!_mask_7150())			  return false;
		break;
	}

	return true;
}

// --------------------------------------------------------------------
// _matrix_reduction   - Reduces original Amplitude to REDUCTION_SAMPLE_SIZE 
//                       into m_pAmpReduite matrix. If m_iOutputSamples is less
//                       then REDUCTION_SAMPLE_SIZE, pads data with zeros.
//
// Return value: NONE
//
// [INPUT]  m_pRAWData;
// [OUTPUT] m_pAmpReduite
// [OUTPUT] m_ReduiteStep
// [TEMP]   m_pfBeamsA,  m_pfBeamsB
//
// Used in functions:
//		_detection_estimate
//
// MatLab reference:
//		SampleBeam_ReduceMatrixAmplitude
//
void CIFremerBottomDetect::_matrix_reduction()
{
	DWORD iB;
	int   iS, iRS, nRC;
	double N_m;

	
	// !ALTRAN - 4 begin
	//m_ReduiteStep = max(1, floori(((float)m_iOutputSamples)/((float)REDUCTION_SAMPLE_SIZE)+0.5f));
	N_m=0.04*m_iOutputSamples+190;
	m_ReduiteStep =max(1,floori((m_iOutputSamples/N_m)+0.5));
	// !ALTRAN end

	m_ReduiteSize = floorw(((float)m_iOutputSamples)/((float)m_ReduiteStep));
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;

//	if (m_ReduiteStep == 1)		// Nothing to reduce, just copy.
	if (m_iOutputSamples <= REDUCTION_SAMPLE_SIZE)		// Nothing to reduce, just copy.
	{
		for (iS=0; iS < m_iOutputSamples; iS++)
		{
			for (iB=0;iB<m_dwReceiveBeams;iB++,data++)
			{
				m_pAmpReduite[iB][iS] = data->amp;
			}
		}

		m_ReduiteSize = m_iOutputSamples;

		for (; iS < REDUCTION_MATRIX_SIZE; iS++)
		{
			for (iB=0;iB<m_dwReceiveBeams;iB++)
			{
				m_pAmpReduite[iB][iS] = 0;
			}
		}

		return;
	}

	for (iS=iRS=0; iRS < m_ReduiteSize; iRS++)
	{
		nRC = (iRS+1)*m_ReduiteStep;

		if (nRC > m_iOutputSamples)
			break;
		
		for (;iS<nRC; iS++)
		{
			for (iB=0;iB<m_dwReceiveBeams;iB++,data++)
			{
				m_pfReduite[iB][iRS] += data->amp;
			}
		}

		for (iB=0;iB<m_dwReceiveBeams;iB++)
		{
			m_pfReduite[iB][iRS] /= m_ReduiteStep;
		}
	}

	m_ReduiteSize = iRS;

	for (; iRS < REDUCTION_MATRIX_SIZE; iRS++)
	{
		for (iB=0;iB<m_dwReceiveBeams;iB++)
		{
			m_pfReduite[iB][iRS] = 0.0f;
		}
	}

	for (iRS = 0; iRS < m_ReduiteSize; iRS++)		// Copy first and last beams
	{
		m_pAmpReduite[0][iRS] = m_pfReduite[0][iRS];
		m_pAmpReduite[m_dwReceiveBeams-1][iRS] = m_pfReduite[m_dwReceiveBeams-1][iRS];
	}

	for (iRS = 0; iRS < m_ReduiteSize; iRS++)
	{
		for (iB=1; iB<m_dwReceiveBeams-1; iB++)
		{
			m_pAmpReduite[iB][iRS] = (m_pfReduite[iB-1][iRS]+m_pfReduite[iB][iRS]+m_pfReduite[iB+1][iRS])/3.0f;
		}
	}
}

// --------------------------------------------------------------------
// _matrix_compensation - Normalizes each sample to its noise via median value. 
//
// Return value: NONE
//
// [INPUT]  m_pAmpReduite
// [OUTPUT] m_pAmpReduite
// [TEMP]   m_pfBeamsA
//
void CIFremerBottomDetect::_matrix_compensation()
{
	int   iB, iS;
	
	float fMed; // EPM fix, whole function works on float
	
	for (iS=0; iS < m_ReduiteSize; iS++)
	{
		for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
		{
			m_pfBeamsA[iB] = m_pAmpReduite[iB][iS]; //EPM fix
		}
			
		fMed = _median(m_pfBeamsA, NULL, m_dwReceiveBeams);
			
		if (fabs(fMed) > 2*FLT_EPSILON)
		{
			for (iB=0;iB<(int)m_dwReceiveBeams;iB++)
			{
				m_pAmpReduite[iB][iS] /= fMed;
			}
		}
		else
		{
			for (iB=0;iB<(int)m_dwReceiveBeams;iB++)
			{
				m_pAmpReduite[iB][iS] = 0;
			}
		}
	}
}


// --------------------------------------------------------------------
// _r0_estimation - rough estimation of weighted averaged
//                  bottom detect point on the reduced amplitude matrix.
//
// Return value: Success(true)/Fail(false)
//
// [INPUT]  m_pR1 === EndSample
// [INPUT]  m_pAmpReduite, m_ReduiteStep
// [OUTPUT] m_pR1, m_R0
// [TEMP]   m_pdBeamsA, m_pdBeamsB, m_pdBeamsC
// [TEMP]   m_pfBeamsA, m_pfBeamsB, m_pfBeamsC, m_pfBeamsD
//
// Sub-functions calls:
//		_strip_estimation
//		
// Used in sub-functions:
// [TEMP]   m_pfSamplesA, m_pfSamplesB
// [TEMP]   m_pfSamplesC, m_pfSamplesD
// [TEMP]   m_pfSamplesE
//
// [MATLAB] Amp           === m_pAmpReduite
// [MATLAB] NbStrips      === (hardcoded)
// [MATLAB] Start         === sample_start
// [MATLAB] EndSample     === m_pR1 (in)
// [MATLAB] SeuilAmp      === SeuilAmp
// [MATLAB] SeuilCum      === (hardcoded in subfunctions)
// [MATLAB] Rmin          === m_pGatesMin / m_ReduiteStep
// [MATLAB] Rmax          === m_pGatesMax / m_ReduiteStep
// [MATLAB] R0            === m_R0
// [MATLAB] R0Strip       === m_pR1 (out)
// [MATLAB] flagBeams     === m_pFlagBeams
// [MATLAB] R0StripBefore === (not used in MatLab / not returned)
// [MATLAB] R0StripAfter  === (not used in MatLab / not returned)
//
bool CIFremerBottomDetect::_r0_estimation( WORD sample_start, float SeuilAmp )
{
	float nbBeams5 = (float)m_dwReceiveBeams / m_nStrips;
	
	int iS, iB, k1, k2;
	float R0;
	WORD SampleEnd, SampleStart = sample_start;
	DWORD minGate = m_iOutputSamples, maxGate = 0;
	DWORD *R1NaN    = (DWORD*) m_pR1;
	DWORD *StripNaN = (DWORD*) m_pfBeamsB;
	
	NANf(m_R0);
	memset(m_pFlagBeams,false,sizeof(bool)*m_dwReceiveBeams);

	// m_pfBeamsB === r0Strip
	// m_R0       === min(r0Strip)

	for (iS = 0; iS < m_nStrips; iS++)
	{
//%		k1 = 1 + floor((iStrip-1) * nbBeams5);
//%		k2 = floor(iStrip * nbBeams5);
//%		sub = k1:k2;
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);

//%		RminStrip = min(Rmin(sub));
//%		RmaxStrip = max(Rmax(sub));
//%		MaxEndSample = max(EndSample(sub));
		minGate     = 0xffffffff;
		maxGate     = 0;
		SampleEnd   = 0;
		
		for (iB = k1; iB < k2; iB++)
		{
			if (minGate   > m_pGatesMin[iB])   minGate   = m_pGatesMin[iB];
			if (maxGate   < m_pGatesMax[iB])   maxGate   = m_pGatesMax[iB];
			if (!ISNAN(R1NaN[iB]) && (float)SampleEnd < m_pR1[iB])  SampleEnd = floorw(m_pR1[iB]);
			m_pdBeamsA[iB] = iS;
		}		
		
        // !ALTRAN - 3 begin
		minGate = floordw((float)minGate/m_ReduiteStep-1.0f);
		maxGate = min(floordw((float)maxGate/m_ReduiteStep+1.0f), m_ReduiteSize);
		//maxGate = min(maxGate, m_ReduiteSize);
        // !ALTRAN end

//%		MaxEndSample = min(MaxEndSample, ceil(RmaxStrip));
//%		StartStrip = max(Start, floor(RminStrip));
//%		if StartStrip == nbSamples
//%			continue
//%		end

		if (!SampleEnd)
			SampleEnd = (WORD)maxGate;
		else
			SampleEnd   = min(SampleEnd,   (WORD)maxGate);

		SampleStart = max(sample_start,(WORD)minGate);
		
		if (SampleStart >= m_ReduiteSize-1 || SampleEnd == 0 || SampleEnd < SampleStart)
		{
			for (iB = k1; iB < k2; iB++)
			{
				NAN(R1NaN[iB]);
			}
			NAN(StripNaN[iS]);
			continue;
		}
		
		SampleEnd = min(SampleEnd,m_ReduiteSize);

//%		[R1Max(iStrip), R2Max(iStrip), R3Max(iStrip), S1, S2, S3, TypeSignal, flagR1, flagR2, flagR3,  r0Strip(iStrip)] = ...
//%			estimationR0OnStrip(Amp(1:min(nbSamples,MaxEndSample),sub), StartStrip, SeuilAmp, SeuilCum, Display);
		R0 = _strip_estimation(k1,k2,SampleStart,SampleEnd, SeuilAmp);

		if (R0 >= 10 && R0 < m_ReduiteSize)
		{
			if (ISNANf(m_R0) || m_R0 > R0)
				m_R0 = R0;
			m_pfBeamsB[iS] = R0;
		}
		else
		{
			for (iB = k1; iB < k2; iB++)
			{
				NAN(R1NaN[iB]);
			}
			NAN(StripNaN[iS]);
		}
	}

//%	Rap = r0Strip / min(r0Strip);
//%	subEqual = find(Rap == 1);
//%
//%	for k=1:length(subEqual)
//%	    if (subEqual(k) > 2) && (Rap(subEqual(k)-1) > 1.9)
//%	        r0Strip(subEqual(k)-1) = (r0Strip(subEqual(k)-2) + r0Strip(subEqual(k))) / 2;
//%	        r0Strip(subEqual(k)-1) = r0Strip(subEqual(k)-1)-1;
//%	    end
//%	    if (subEqual(k) < (NbStrips-1)) && (Rap(subEqual(k)+1) > 1.9)
//%	        r0Strip(subEqual(k)+1) = (r0Strip(subEqual(k)+2) + r0Strip(subEqual(k))) / 2;
//%	        r0Strip(subEqual(k)+1) = r0Strip(subEqual(k)+1)-1;
//%	    end
//%	end

	memcpy(m_pfBeamsA, m_pfBeamsB, sizeof(float) * m_nStrips);
	DWORD *StripANaN = (DWORD*) m_pfBeamsA;
	R0 = m_R0;

	for (iS = 0; iS < m_nStrips; iS++)
	{
		if (ISNAN(StripANaN[iS]) || !ISEQUAL(m_pfBeamsA[iS], m_R0))
			continue;

		if (iS > 1 && !ISNAN(StripANaN[iS-1]) && m_pfBeamsA[iS-1] > (m_R0 * 1.9f + 0.9f))
		{
			if (ISNAN(StripNaN[iS-2]))
			{
				NAN(StripNaN[iS-1]);
			}
			else
			{
				m_pfBeamsB[iS-1] = (m_pfBeamsB[iS-2] + m_pfBeamsB[iS]) / 2.0f - 1.0f;
				if (ISNANf(R0) || R0 > m_pfBeamsB[iS-1])
					R0 = m_pfBeamsB[iS-1];
			}
		}

		if (iS < m_nStrips-2 && !ISNAN(StripANaN[iS+1]) && m_pfBeamsA[iS+1] > (m_R0 * 1.9f + 0.9f))
		{
			if (ISNAN(StripNaN[iS+2]))
			{
				NAN(StripNaN[iS+1]);
			}
			else
			{
				m_pfBeamsB[iS+1] = (m_pfBeamsB[iS+2] + m_pfBeamsB[iS]) / 2.0f - 1.0f;
				if (ISNANf(R0) || R0 > m_pfBeamsB[iS+1])
					R0 = m_pfBeamsB[iS+1];
			}
		}
	}

	m_R0 = floorf(R0);

//%	flagBeams = imdilate(flagBeams, ones(1,length(sub)));

	bool * buf = (bool *) &m_pdBeamsC[1];		// Since m_pdBeamsB was allocated as DWORD we should not encounter any memory problems
	m_pdBeamsC[0] = 0;
	int hbn = floori(nbBeams5/2.0f);
	int shift = (floori(nbBeams5/2.0f) == floori(nbBeams5/2.0f+0.5f)) ? 0 : 1;
	memcpy(buf,m_pFlagBeams,sizeof(bool)*m_dwReceiveBeams);
	for (iB = hbn; iB < (int)m_dwReceiveBeams - hbn; iB++)
	{
		m_pFlagBeams[iB] = buf[iB] | buf[iB-hbn- shift] | buf[iB+hbn];
	}

//%	subNan = find(isnan(r0Strip));
//%	if ~isempty(subNan)
//%	    subNotNan = find(~isnan(r0Strip));
//%	    if length(subNotNan) < 2
//%	        R0 = NaN;
//%	        return
//%	    end
//%	    r0Strip(subNan) = floor(interp1(subNotNan, r0Strip(subNotNan), subNan));
//%	end

	int vStrips = 0, inS, inSn;
	int iSB = -1, iSE;		// First and last valid strip

	for (iS = inS = 0; iS < m_nStrips; iS++)
	{
		m_pfBeamsA[iS] = (float)iS;
		if (!ISNAN(StripNaN[iS]))
		{
			m_pfBeamsC[vStrips] = (float)iS;
			m_pfBeamsD[vStrips] = m_pfBeamsB[iS];
			vStrips++;
			if (iSB == -1) iSB = iS;
			iSE = iS;
		}
		else
		{
			m_pdBeamsA[inS++] = (DWORD)iS;
		}
	}

	inSn = inS;

	if (vStrips < 2)
	{
		NANf(m_R0);
		return false;
	}

	if (vStrips < m_nStrips)
	{
		Interp1 (m_pfBeamsC, m_pfBeamsD, vStrips, m_pfBeamsA, m_pfBeamsB, m_nStrips);

		for (iS = 0; iS < inSn; iS++)
		{
			m_pfBeamsB[m_pdBeamsA[iS]] = floorf(m_pfBeamsB[m_pdBeamsA[iS]]);
		}
	}

//%	for iStrip=1:NbStrips
//%	    k1 = 1 + floor((iStrip-1) * nbBeams5);
//%	    k2 = floor(iStrip * nbBeams5);
//%	    sub = k1:k2;
//%	    R0Strip(sub) = r0Strip(iStrip);
//%	    R0StripBefore(sub) = r0Strip(max(1,iStrip-1));
//%	    R0StripAfter(sub)  = r0Strip(min(NbStrips, iStrip+1));
//%	end

	for (iS = 0; iS < iSB; iS++)
	{
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);
		
		for (iB = k1; iB < k2; iB++)
		{
			NAN(R1NaN[iB]);
		}
	}

	for (; iS <= iSE; iS++)
	{
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);
		
		for (iB = k1; iB < k2; iB++)
		{
			m_pR1[iB] = m_pfBeamsB[iS];
			if (m_pR1[iB] >= m_ReduiteSize)
				NAN(R1NaN[iB]);
		}
	}

	for (; iS < m_nStrips; iS++)
	{
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);
		
		for (iB = k1; iB < k2; iB++)
		{
			NAN(R1NaN[iB]);
		}
	}

	return true;
}

// --------------------------------------------------------------------
// _strip_estimation - rough estimation of weighted averaged
//                     bottom detect point on the group of the beams.
//
// [IN] beam_start - first beam in the strip
// [IN] beam_end - first beam in the next strip
// [IN] sample_start - first sample of the reduced matrix to process
// [IN] SeuilAmp - amplitude threshold for detection
//
// Return value: estimated R0 on the strip
//
// [INPUT]  m_pAmpReduite
// [TEMP]   m_pfSamplesA, m_pfSamplesB
// [TEMP]   m_pfSamplesC, m_pfSamplesD
// [TEMP]   m_pfSamplesE
//
// [MATLAB] Amp           === m_pAmpReduite(beam_start:beam_end)
// [MATLAB] Start         === sample_start
// [MATLAB] SeuilAmp      === SeuilAmp
// [MATLAB] SeuilCum      === (hardcoded to 0.5)
// [MATLAB] R1            === (not used in MatLab / not returned)
// [MATLAB] R2            === (not used in MatLab / not returned)
// [MATLAB] R3            === (not used in MatLab / not returned)
// [MATLAB] S1Cum         === (not used in MatLab / not returned)
// [MATLAB] S2Cum         === (not used in MatLab / not returned)
// [MATLAB] S3Cum         === (not used in MatLab / not returned)
// [MATLAB] TypeSignal    === (not used in MatLab / not returned)
// [MATLAB] flagR1Amp     === m_pFlagBeams(beam_start:beam_end)
// [MATLAB] flagR2Amp     ===           NB: flags are returned as ORed value
// [MATLAB] flagR3Amp     ===
// [MATLAB] R0Strip       === (returned value)
//
// Sub-functions calls:
//		_strip_amplitude
//		_strip_slope
//		
// Used in functions:
//		_r0_estimation

WORD CIFremerBottomDetect::_strip_estimation(WORD beam_start, WORD beam_end, WORD sample_start, WORD sample_end, float SeuilAmp)
{
	register int iB, iS;

	float SeuilCum = 0.5f;

	WORD SampleSize = sample_end - sample_start;

//%	MoyHorz = nanmean(Amp(Start:end,:), 2)';
//%	MedHorz = nanmedian(Amp(Start:end,:), 2)';
//%	MoyMinusMed = MoyHorz - MedHorz;

	float meanMin = FLT_MAX;	// min(MoyHorz)

	// !ALTRAN - 35 Begin
	float meanMax = -FLT_MAX;	// min(MoyHorz)
	// !ALTRAN End
	
	// m_pfSamplesA - Strip Mean		(MoyHorz)
	// m_pfSamplesB - Strip Median		(MedHorz)
	// m_pfSamplesD - MoyMinusMed
	for (iS = 0; iS < SampleSize; iS++)
	{
		for (iB = beam_start; iB < beam_end; iB++)
		{
			m_pfSamplesE[iB-beam_start] = m_pAmpReduite[iB][iS+sample_start];
		}


		m_pfSamplesA[iS] = _mean(m_pfSamplesE, beam_end-beam_start);
		m_pfSamplesB[iS] = _median(m_pfSamplesE, NULL, beam_end-beam_start);
		if (m_pfSamplesA[iS] < meanMin) meanMin = m_pfSamplesA[iS];
		// !ALTRAN - 35 Begin
		if (m_pfSamplesA[iS] > meanMax) meanMax = m_pfSamplesA[iS];
		// !ALTRAN End
		m_pfSamplesD[iS] = m_pfSamplesA[iS]-m_pfSamplesB[iS];
	}

	// m_pfSamplesE - Temporary
	float medianMean  = _median(m_pfSamplesA, m_pfSamplesE, SampleSize);
//%	Med = median(MoyMinusMed(subNonNaN)); % =0 en principe
	float medianMinus = _median(m_pfSamplesD, m_pfSamplesE, SampleSize);
 	float calc;			// Temporary calculations

	// m_pfSamplesC - S1
	// m_pfSamplesE - S2
	// m_pfSamplesD - S3
	for (iS = 0; iS < SampleSize; iS++)
	{
//%		S1 = MoyHorz - min(MoyHorz);
//%		S1 = S1 / max(MoyHorz);			--- NB: devision is redundant because of the normalization later
//%		S1 = S1 .^ 2;
		calc = m_pfSamplesA[iS] - meanMin;
		// !ALTRAN - 35 Begin
		calc = calc / meanMax;
		// !ALTRAN End
		m_pfSamplesC[iS] = calc*calc;

//%		S2 = NaN(1,nbSamples);
//%		S2(subNonNaN) = (MoyHorz(subNonNaN) - median(MoyHorz(subNonNaN)));
//%		S2 = S2 / max(S2);
		m_pfSamplesE[iS] = m_pfSamplesA[iS] - medianMean;

//%		S3 = NaN(1,nbSamples);
//%		S3(subNonNaN) = (MoyMinusMed(subNonNaN) - Med);
		m_pfSamplesD[iS] -= medianMinus;
	}	
	
	// m_pfSamplesC - S1Cum
	_cumsum(m_pfSamplesC, m_pfSamplesC, SampleSize, true);
	// m_pfSamplesE - S2Cum
	_cumsum(m_pfSamplesE, m_pfSamplesE, SampleSize, true);
	// m_pfSamplesD - S3Cum
	_cumsum(m_pfSamplesD, m_pfSamplesD, SampleSize, true);

//%	R1 = find(S1Cum >= SeuilCum, 1, 'first') + (Start-1);   --- NB: Calculation vectors are aligned from sample_start
//%	R2 = find(S2Cum >= SeuilCum, 1, 'first') + (Start-1);   ---     so it is not added back in for the calculations,
//%	R3 = find(S3Cum >= SeuilCum, 1, 'first') + (Start-1);   ---     but rather in the final results only
//%	flagR1Noise = (max(abs(S1Cum)) < 2);
//%	flagR2Noise = (min(S2Cum) > -0.6) && (max(abs(S2Cum)) < 2);
//%	flagR3Noise = (min(S3Cum) > -0.6) && (max(S3Cum) < 1.6);

	int R1 = m_ReduiteSize, R2 = m_ReduiteSize, R3 = m_ReduiteSize, R0;
	bool flagR1Noise = true, flagR2Noise = true, flagR3Noise = true;

	for (iS = 0; iS < SampleSize; iS++)
	{
		if (m_pfSamplesC[iS] >= SeuilCum && R1 == m_ReduiteSize) R1 = iS;
		if (m_pfSamplesE[iS] >= SeuilCum && R2 == m_ReduiteSize) R2 = iS;
		if (m_pfSamplesD[iS] >= SeuilCum && R3 == m_ReduiteSize) R3 = iS;

		if (fabs(m_pfSamplesC[iS]) > 2)                             flagR1Noise = false;
		if (fabs(m_pfSamplesE[iS]) > 2 || m_pfSamplesE[iS] < -0.6f) flagR2Noise = false;
		if (m_pfSamplesD[iS] < -0.6f || m_pfSamplesD[iS] > 1.6f)    flagR3Noise = false;
	}
	
	float Slope1 = 0.0f, Slope2 = 0.0f, Slope3 = 0.0f, Slope = 0.0f;

	bool flagR1Amp = _strip_amplitude(m_pfSamplesA, sample_start, R1, SampleSize, SeuilAmp);
	bool flagR2Amp = _strip_amplitude(m_pfSamplesA, sample_start, R2, SampleSize, SeuilAmp);
	bool flagR3Amp = _strip_amplitude(m_pfSamplesA, sample_start, R3, SampleSize, SeuilAmp);

	if (flagR1Noise && flagR1Amp)
		Slope1 = _strip_slope(m_pfSamplesC, SampleSize);
	else
		Slope1 = 0;
	
	if (flagR2Noise && flagR2Amp )
		Slope2 = _strip_slope(m_pfSamplesE, SampleSize);
	else
		Slope2 = 0;

	if (flagR3Noise && flagR3Amp )
		Slope3 = _strip_slope(m_pfSamplesD, SampleSize);
	else
		Slope3 = 0;

	int TypeSignal = 0;

	if (Slope2 > Slope1)
	{
		Slope = Slope2;
		R0 = R2;
		TypeSignal = 2;
	}
	else if (Slope1 > 0)
	{
		Slope = Slope1;
		R0 = R1;
		TypeSignal = 1;
	}

	if (Slope3 > Slope)
	{
		Slope = Slope3;
		R0 = R3;
		TypeSignal = 3;
	}

	if ((flagR1Amp|flagR2Amp|flagR3Amp) && (Slope>=0.01f))
	{
		for (iS = beam_start; iS < beam_end; iS++)
		{
			m_pFlagBeams[iS] = true;						// flagBeams
		}
	}
	else
	{
		for (iS = beam_start; iS < beam_end; iS++)
		{
			m_pFlagBeams[iS] = false;						// flagBeams
		}

		return 0;
	}

	if (SampleSize < 21) 
		return R0 + sample_start;

	// Final detection
	int K1 = (R0 < 10) ? 0 : (R0 >= SampleSize - 11) ? SampleSize - 21 : R0 - 10;
	Slope1 = Slope2 = 0;

	meanMin = FLT_MAX;
	for (iS = K1; iS < K1+21; iS++)
	{
		if (meanMin > m_pfSamplesA[iS]) meanMin = m_pfSamplesA[iS];
	}

	for (iS = K1; iS < K1+21; iS++)
	{
		Slope1 = m_pfSamplesA[iS] - meanMin;
		m_pfSamplesB[iS] = Slope1*Slope1;
	}

	_cumsum(m_pfSamplesB+K1, m_pfSamplesB+K1, 21, false);

	Slope1 =         FLT_MAX; // min(yf)
	Slope2 = (-1.0f)*FLT_MAX; // max(yf)

	for (iS = K1; iS < K1+21; iS++)
	{
		if (Slope1 > m_pfSamplesB[iS]) Slope1 = m_pfSamplesB[iS];
		if (Slope2 < m_pfSamplesB[iS]) Slope2 = m_pfSamplesB[iS];
	}

	Slope2 -= Slope1;

	for (iS = K1; iS < K1+21; iS++)
	{
		m_pfSamplesB[iS] = (m_pfSamplesB[iS] - Slope1) / Slope2;
		if (m_pfSamplesB[iS] > 0.10f)
			return iS + sample_start - 1;
	}

	return 0;
}


// --------------------------------------------------------------------
// _strip_estimation - tests validity of estimated R0 point according
//                     to strip average amplitude.
//
// [IN] MoyHorz   - first beam in the strip
// [IN] R0        - estimated R0 point
// [IN] nbSamples - strip length in samples
// [IN] SeuilAmp  - amplitude threshold for detection
//
// Return value: true is estimation is valid
//
// [MATLAB] MoyHorz       === MoyHorz
// [MATLAB] Start         === beam_start
// [MATLAB] R0            === R0
// [MATLAB] nbSamples     === nbSamples
// [MATLAB] SeuilAmp      === SeuilAmp
// [MATLAB] flag          === (returned value)
//
// Used in functions:
//		_strip_estimation
//
// MatLab reference:
//		Reson_R0_estimation_V6 -> testAmplitude

bool CIFremerBottomDetect::_strip_amplitude(float * MoyHorz, WORD sample_start, int R0, WORD nbSamples, float SeuilAmp)
{
	float A;

	if (R0 <= 0)
		A = MoyHorz[0];
	else if (R0 >= nbSamples-1)
		A = MoyHorz[nbSamples-1];
	else 
	{
		A = max(MoyHorz[R0-1],MoyHorz[R0+1]);
		A = max(A, MoyHorz[R0]);
	}

	if (sample_start)
//%		SeuilAmp = interp1([1 nbSamples], [SeuilAmp SeuilAmp/2], R0);
		SeuilAmp = SeuilAmp * ( 1 - (float)R0 / (2 * (float)(nbSamples-1)) );
	
	return (A < SeuilAmp) ? false : true;
}


// --------------------------------------------------------------------
// _strip_estimation - tests detection slope of to validate estimation.
//
// [IN] S         - signal
// [IN] nbSamples - signal length in samples
//
// Return value: true is estimation is valid
//
// [MATLAB] x             === (defined by nbSamples, since points are continuous from start)
// [MATLAB] S             === S
// [MATLAB] Slope         === (returned value) --- NB: Actually maximum of consequent tries is returned as it is what is used in MatLab
//
// Used in functions:
//		_strip_estimation
//
// MatLab reference:
//		Reson_R0_estimation_V6 -> testHeaviside

float CIFremerBottomDetect::_strip_slope(float * S, WORD nbSamples)
{
	float L1, L2, Slope = 0, S1, S2;
	int i,j, K1, K2, I50, k;

	for (k = 1; k <= 25; k++)
	{
		L1 = ((float)k)/100.0f;
		L2 = 1-L1;
		K1 = K2 = I50 = -1;

//%		iBeg = find(S >= L1, 1, 'first');	=== K1
//%		iEnd = find(S <= L2, 1, 'last');	=== K2
//%		i50  = find(S >= 0.5, 1, 'first');	=== I50
		for (i=0, j=nbSamples-1; i < nbSamples; i++, j--)
		{
			if (S[i] >= L1   && K1  == -1) K1  = i;
			if (S[j] <= L2   && K2  == -1) K2  = j;
			if (S[i] >= 0.5f && I50 == -1) I50 = i;

			if (K1>0 && K2>0 && I50>0)
				break;
		}

		if (K2 > I50)
			S1 = (0.5f - L1) / (K2-I50);
		else
			S1 = 0.0f;

		if (I50 > K1)
			S2 = (0.5f - L1) / (I50-K1);
		else
			S2 = 0.0f;

		S1 = max(S1,S2);

		if (S1 > Slope) Slope = S1;
	}

	return Slope;
}


// --------------------------------------------------------------------
// _detection_estimate   - rough estimation of weighted averaged per beam
//                         bottom detect points on the reduced amplitude matrix.
//
// Return value: Success(true)/Fail(false)
//
// [INPUT]  m_pRAWData;
// [OUTPUT] m_pMaskMin,  m_pMaskMax
// [OUTPUT] m_pR1, m_pR2, m_R0
// [OUTPUT] m_IncidentBeam
// [TEMP]   m_pfBeamsA, m_pfBeamsB
// [TEMP]   m_pfSamplesA
//
// [MATLAB] Amp                === m_pRAWData
// [MATLAB] step               === m_ReduiteStep (calculated inside)
// [MATLAB] Teta               === m_pBeamAngles (in radians)
// [MATLAB] ReceiveBeamWidth   === m_fReceiveBeamWidth
// [MATLAB] SampleRate         === m_fSampleRate
// [MATLAB] TxPulseWidth       === m_fTransmitPulseLength
// [MATLAB] DepthMin           === m_pGatesMin (in samples per beam)
// [MATLAB] DepthMax           === m_pGatesMax (in samples per beam)
// [MATLAB] R0                 === m_R0
// [MATLAB] RMax1              === m_pR1
// [MATLAB] RMax2              === m_pR2
// [MATLAB] R1SamplesFiltre    === calculated at later stage
// [MATLAB] iBeamMax0          === m_IncidentBeam
// [MATLAB] AmpPingNormHorzMax === not used in MatLab / not calculated
// [MATLAB] MaskWidth          === (calculated at later time)
// [MATLAB] iBeamBeg           === Returned as IF1_QUALITY_QF2_PASS in m_pDetectionType
// [MATLAB] iBeamEnd           === Returned as IF1_QUALITY_QF2_PASS in m_pDetectionType
//
// Sub-functions calls:
//		_r0_estimation
//		_matrix_reduction
//		_matrix_compensation
//		_amplitude_single_reduit
//		
// Used in sub-functions:
// [TEMP]   m_pdBeamsA, m_pdBeamsB
// [TEMP]   m_pfBeamsA, m_pfBeamsB
// [TEMP]   m_pfSamplesA, m_pfSamplesB
// [TEMP]   m_pfSamplesC, m_pfSamplesD
// [TEMP]   m_pfSamplesE
//
// Used in functions:
//		CalculateMask
//
// MatLab reference:
//		SampleBeam_ReduceMatrixAmplitude
//		SampleBeam_PreprocAmpMask_7111_V6


bool CIFremerBottomDetect::_detection_estimate( void )
{
	int iB, iS;
	float R0Marge, coefEnlargeMask;

	// !ALTRAN - 43 begin
	if(m_dwDeviceId == 7150)
	{
		coefEnlargeMask = 1;
		R0Marge = 4;
	}
	else if(m_dwDeviceId == 7111)
	{
		coefEnlargeMask = 4;
		R0Marge = 0;
	}
	else  // 7125
	{
		coefEnlargeMask = 4;
		R0Marge = 0;
	}

	// !ALTRAN End

	_matrix_reduction();
    
//%	Amp = Amp(1:end-2,:);
	m_ReduiteSize -= 2;
	
	float R0_Original;
	DWORD * R1NaN = (DWORD *) m_pR1;
	DWORD * R2NaN = (DWORD *) m_pR2;
	
	NANf(R0_Original);
	NANf(m_R0);

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		NAN(R1NaN[iB]);
	}

	if (_r0_estimation(1,10.0f))
	{
		R0_Original = m_R0;
		memcpy(m_pR2, m_pR1, sizeof(float)*m_dwReceiveBeams);
	}
    
	_matrix_compensation();
    
	
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		NAN(R1NaN[iB]);
	}
	
	if (!_r0_estimation(0,4.0f))
		NANf(m_R0);

	// !ALTRAN - 43 begin
	if(m_IdentAlgo != 3)
	{
		//if R0_OnOriginal > (R0+R0Marge)
        //	R0 = R0_OnOriginal;
        //	subFaux = find(R0Strip < R0_OnOriginal);
        //	R0Strip(subFaux) = R0StripOriginal(subFaux);
		//end
		if(R0_Original > (m_R0+R0Marge))
		{
			m_R0 = R0_Original;
			for(int i=0; i<m_dwReceiveBeams;i++)
			{
				if(m_pR1[i] < m_pR2[i])
				{
					m_pR1[i] = m_pR2[i];
				}
			}
		}

	}
	// !ALTRAN End

	
	bool flagMerge = false;

	if (m_dwDeviceId == 7111)
	{
		flagMerge = !ISNANf(R0_Original) && !ISNANf(m_R0) && R0_Original > m_R0;
	}
	else if (m_dwDeviceId == 7150)
	{
		if (!ISNANf(R0_Original) && ISNANf(m_R0))
			m_R0 = R0_Original;
		else
			flagMerge = !ISNANf(R0_Original) && !ISNANf(m_R0) && R0_Original > m_R0+4;
	}

	if (flagMerge)
	{
		m_R0 = R0_Original;
		
		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
		{
			if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < R0_Original) 
				m_pR1[iB] = m_pR2[iB];
		}
	}

	if (ISNANf(m_R0))
		return false;
	
/*
	Sfloat * testR1 = (Sfloat *)m_pR1;
	Sfloat * testR2 = (Sfloat *)m_pR2;
	float    f1 = 2.1f, f2 = 1.1f;
	Sfloat * testF1 = (Sfloat *)&f1;
	Sfloat * testF2 = (Sfloat *)&f2;
*/


	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
/*
		f1 = 1.9f*m_pR2[iB]+0.9f;
		f2 = 2.1f*m_pR2[iB]+1.1f;
*/
		if (!ISNAN(R1NaN[iB]) && !ISNAN(R2NaN[iB]) 
			&& (m_pR1[iB] >= 1.9f*m_pR2[iB]+0.89f) && (m_pR1[iB] <= 2.1f*m_pR2[iB]+1.11f))
		{
			m_pR1[iB] = m_pR2[iB];
		}
	}

	if (m_Debug&DEBUG_OUTPUT_PREPROC_LONG)
	{
		m_pRangePhase[0] = R0_Original;
	}
	
	R0_Original = m_R0;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		m_pR2[iB] = m_pR1[iB];
		if (!ISNAN(R1NaN[iB]))
		{
			m_pR1[iB] = m_pR1[iB]*2.0f+2.0f;
		}
	}

	WORD ss = max((WORD)m_R0-2,2);

	if (!_r0_estimation(ss,2.0f))
		NANf(m_R0);

	if (m_Debug&DEBUG_OUTPUT_PREPROC_LONG)
	{
		m_pRangePhase[0] = R0_Original;
	}

	if (m_dwDeviceId == 7111)
	{
//% if R0 < R0Strip2
//%     sub = find(R0Strip < R02);
//%     R0Strip(sub) = R0Strip2(sub);
//%     R0 = R02;
//% end
// 		float R2Min = REDUCTION_MATRIX_SIZE;
// 		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
// 			if (ISNAN(R2NaN[iB]))
// 				R2Min = 0;
// 			else if (m_pR2[iB] < R2Min)
// 				R2Min = m_pR2[iB];

		if (m_R0 < R0_Original)
		{
			for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
			{
				if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < R0_Original) 
					m_pR1[iB] = m_pR2[iB];
			}

			m_R0 = R0_Original;
		}

	}

//%======================================(*1*)
//% AMedian = Inf(1,nbBeams);
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         AMedian(iBeam) = nanmedian(Amp(:,iBeam));
//%     end
//% end
//% 
//%======================================(*2*)
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         iSampleBeg = max(floor(R0-3 + (R0Strip(iBeam)-R0)/3), R0-3);
//%         iSampleEnd = min(floor(1.4*R0Strip(iBeam)), nbSamples);
//%         iSampleBeg = max(iSampleBeg, floor(Rmin(iBeam)));
//%         iSampleEnd = min(iSampleEnd, ceil(Rmax(iBeam)));
//%         
//%         Amp(iSampleEnd:end,iBeam) = NaN;
//%         Amp(1:iSampleBeg,iBeam) = NaN;
//%     else
//%         Amp(:,iBeam) = NaN;
//%     end
//% end
//% 
//%======================================(*3*)
//% Offset = R0-1;
//% Amp = Amp(Offset:end,:);
//% nbSamples = size(Amp,1);
//% 
//% %% Search for the maximum amplitude along each beam starting from R0
//% 
//% RMax1 = ones(1,nbBeams) * nbSamples;
//% AMax = zeros(1,nbBeams);
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         [aMax, rMax1] = max(Amp(:,iBeam));
//%         if aMax > 2*AMedian(iBeam)
//%             AMax(iBeam) =  aMax;
//%             RMax1(iBeam) = rMax1;
//%         end
//%     end
//% end
//% 
//%======================================(*4*)
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         a = Amp(:,iBeam);
//%         iDeb = find(isnan(a(1:RMax1(iBeam))), 1, 'last');
//%         X = min(RMax1(iBeam)-iDeb, nbSamples-RMax1(iBeam));
//%         sub = RMax1(iBeam)-X:RMax1(iBeam)+X;
//%         sub(sub < 1) = [];
//%         sub(sub>length(a)) = [];
//%         if length(sub) < 5
//%             continue
//%         end
//%         rMax1 = detectionAmplitudePingOnCompensatedImage(1:size(Amp,1), a(sub), 1, iPing, iBeam, Teta(iBeam), DisplayLevel, nbSamples);
//%         if ~isnan(rMax1)
//%             RMax1(iBeam) = rMax1 + (sub(1)-1);
//%         end
//%     end
//% end
//% 
//%======================================(*5*)
//% RMax1 = floor(RMax1 + (Offset-1) - 0.5); % -0.5 apparently necessary (view on file 20080222_014920
//% RMax1(flagBeams == 0) = NaN;

	float BeamMedian, BeamMax;
	int   iSampleBeg, iSampleEnd, iSampleLength, R1Max;
	int   iSubBeg, iSubEnd, index, iSE, X;
	float R0     = m_R0;
	float rMax1;
	int   Offset = (int)m_R0-1;

//%	AMaxMoyen = nanmean(AMax);
//%	AMax2 = AMax .^ 2;				=== m_pfBeamsB
	float BeamMaxMean = 0;

	// m_pfBeamsA === AMax
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (m_pFlagBeams[iB])
		{
			BeamMedian = _median(m_pAmpReduite[iB], m_pfSamplesA, m_ReduiteSize);							// === (*1*)

			if (!ISNAN(R1NaN[iB]))
			{
				iSampleBeg = max(floori(R0-3 + ((float)m_pR1[iB]-R0)/3.0f), (int)m_R0-3);						// === (*2*)
				iSampleEnd = min(floori(1.4f*(m_pR1[iB]+1)-1), (int)m_ReduiteSize-1);			// +/-1 needed to match MatLab 1-based indexing
			}
			else
			{
				iSampleBeg = (int)m_R0-3;						// === (*2*)
				iSampleEnd = (int)m_ReduiteSize-1;				// +/-1 needed to match MatLab 1-based indexing
			}
			// !ALTRAN - 8 begin
			//iSampleBeg = max(iSampleBeg, floori((float)m_pGatesMin[iB]));
			//iSampleEnd = min(iSampleEnd, floori((float)m_pGatesMax[iB] + 1.0f));
			iSampleBeg = max(iSampleBeg, floori((float)m_pGatesMin[iB]/m_ReduiteStep));
			iSampleEnd = min(iSampleEnd, floori((float)m_pGatesMax[iB]/m_ReduiteStep + 1.0f));
			// !ALTRAN End

			BeamMax = 0.0f;																					// === (*3*)
			for (iS = max(iSampleBeg+1,Offset); iS < iSampleEnd; iS++)
			{
				if (m_pAmpReduite[iB][iS] > BeamMax)
				{
					BeamMax = m_pAmpReduite[iB][iS];
					R1Max   = iS;
				}
			}
		
			if (BeamMax > 2.0f*BeamMedian)
			{
				m_pfBeamsA[iB] = BeamMax;				// AMax
				m_pfBeamsB[iB] = BeamMax * BeamMax;		// AMax ^ 2
				BeamMaxMean   += BeamMax;
				m_pR1[iB]      = (float)R1Max-0.5f;
				R1Max++;
			}
			else
			{
				m_pfBeamsA[iB] = 0.0f;		// AMax
				m_pfBeamsB[iB] = 0.0f;		// AMax ^ 2

				if (m_dwDeviceId == 7150)
				{
					NAN(R1NaN[iB]);
					m_pFlagBeams[iB] = false;
				}
				else
				{
					m_pR1[iB]      = (float)(m_ReduiteSize-2);
				}

				continue;
			}

			// !ALTRAN - 30 Begin
			m_pMax[iB] = m_pAmpReduite[iB][R1Max-1];
			// !ALTRAN End

			// iDeb      === iSampleBeg - Offset + 1														// === (*4*)
			// RMax1     === R1Max - Offset
			// nbSamples === m_ReduiteSize - Offset
			iSampleLength = R1Max - iSampleBeg - 1;
			if (iSampleLength > m_ReduiteSize - R1Max)
			{
				iSampleLength = m_ReduiteSize - R1Max;
				index = 0;
			}
			else
			{
				index = 1;
			}

			// !ALTRAN - 28 begin
			//X = min(R1Max-iSampleBeg-1,m_ReduiteSize-R1Max-1);
			iSubBeg       = R1Max - iSampleLength-1+index;
			iSubEnd       = min(R1Max + iSampleLength-1+index, iSampleEnd);
			iSE           = min(R1Max + iSampleLength-1+index+1, m_ReduiteSize);
			// !ALTRAN end
			

			// !ALTRAN - 14 begin
 			//if (iSE - iSubBeg >= 5-index && iSampleBeg >= Offset)		// Need second condition, since if all NaNs are cut down by Amp = Amp(Offset:end,:)
 			//{														// statement find returns nothing.
				if ((rMax1 = _amplitude_single_reduit(m_pAmpReduite[iB], iSubBeg, iSubEnd, iB, index)) >= 0)
				//if ((rMax1 = _amplitude_single_reduit(m_pAmpReduite[iB], R1Max-X, R1Max+X, iB, index)) >= 0)
					// !ALTRAN - 12 begin
				{
					m_pR1[iB] = rMax1-(float)index;
				}
					// !ALTRAN end
 			//}
			// !ALTRAN end

			// !ALTRAN - 13 begin
			//if (m_dwDeviceId == 7150)
			//{
			//	m_pR1[iB] += 0.5f;
			//}
			// !ALTRAN end

			m_pR1[iB] = floorf(m_pR1[iB]);
		}
		else
		{
			NAN(R1NaN[iB]);
			m_pfBeamsA[iB] = 0.0f;
			m_pfBeamsB[iB] = 0.0f;		// AMax ^ 2
		}
	}

	BeamMaxMean /= m_dwReceiveBeams;


	// Identification of the beam number corresponding to R0
//%	pppp = RMax1;
//%	AMaxMoyen = nanmean(AMax);			=== BeamMaxMean
//%	pppp(AMax < AMaxMoyen) = Inf;
//%	[pppp, iBeamMax0] = min(pppp);
// Old Way
// 	m_IncidentBeam = (WORD)m_dwReceiveBeams;
// 	int iBeamMax0  = (int)m_dwReceiveBeams, iSFull = REDUCTION_MATRIX_SIZE;
// 	iS = REDUCTION_MATRIX_SIZE;
// 
// 	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
// 	{
// 		if (m_pR1[iB] > m_ReduiteSize) 
// 			NAN(R1NaN[iB]);
// 
// 		if (!ISNAN(R1NaN[iB]) && m_pfBeamsA[iB] >= BeamMaxMean && m_pR1[iB] < (float)iS)
// 		{
// 			iS = floori(m_pR1[iB]);
// 			m_IncidentBeam = iB;
// 		}
// 
// 		if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < (float)iSFull)
// 		{
// 			iSFull = floori(m_pR1[iB]);
// 			iBeamMax0 = iB;
// 		}
// 	}


//% [pppp, iBeamMax0Full] = min(RMax1);
//% sub = find(RMax1 == pppp);

	int BeamMax0 = (int)m_dwReceiveBeams;
	int BeamMax1 = (int)m_dwReceiveBeams;

	// !ALTRAN - 44 begin
	float pR1min = FLT_MAX;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++) 
	{
		if(m_pR1[iB] < pR1min) pR1min = m_pR1[iB];
	}
	// !ALTRAN End


	for (int iB = 0; iB < (int)m_dwReceiveBeams; iB++) 
	{
 		if (m_pR1[iB] > m_ReduiteSize) 
 			NAN(R1NaN[iB]);

		if (ISNAN(R1NaN[iB])) continue;
		
		// !ALTRAN - 44 begin
		//if (floori(m_pR1[iB]) < iS) {
		//	BeamMax0 = BeamMax1 = iB;
		//	iS = floori(m_pR1[iB]);
		//} else if (floori(m_pR1[iB]) == iS) {
		//	BeamMax1 = iB;
		//}
		if(m_pR1[iB] == pR1min && BeamMax0 == m_dwReceiveBeams) BeamMax0=iB;
		if(m_pR1[iB] == pR1min) BeamMax1=iB;
	}

	// !ALTRAN - 44 begin
	if(BeamMax0 == m_dwReceiveBeams && BeamMax1 == m_dwReceiveBeams)
	{
		// sub = find(RMax1 == pppp) empty
		NANf(m_R0);
		for(int i=0; i<m_dwReceiveBeams; i++) NANf(m_pR1[i]);
		for(int i=0; i<m_dwReceiveBeams; i++) NANf(m_pR2[i]);
		return false;
	}
	// !ALTRAN End

//% iBeamMax0Full = floor((sub(1) + sub(end))/2);
	WORD iBeamMax0Full = floorw((BeamMax0 + BeamMax1) / 2.0f);

//% pppp = RMax1;
//% AMaxMoyen = nanmean(AMax);			=== BeamMaxMean
//% pppp(AMax < AMaxMoyen) = Inf;
//% [qqqq, iBeamMax0] = min(pppp);
//% sub = find(pppp == qqqq);

	BeamMax0 = (int)m_dwReceiveBeams;
	BeamMax1 = (int)m_dwReceiveBeams;
	
	for (iB = 0, iS = REDUCTION_MATRIX_SIZE; iB < (int)m_dwReceiveBeams; iB++) 
	{
		if (m_pR1[iB] > m_ReduiteSize) 
			NAN(R1NaN[iB]);
		
		if (ISNAN(R1NaN[iB]) || (m_pfBeamsA[iB] < BeamMaxMean)) continue;
		
		if (floori(m_pR1[iB]) < iS) {
			BeamMax0 = BeamMax1 = iB;
			iS = floori(m_pR1[iB]);
		} else if (floori(m_pR1[iB]) == iS) {
			BeamMax1 = iB;
		}
	}

	// !ALTRAN - 45 begin
	//if (m_dwDeviceId == 7150) {
	// !ALTRAN End

//%		pppp = RMax1;
//%		AMaxMoyen = nanmean(AMax);
//%		pppp(AMax < AMaxMoyen) = Inf;
//%		[pppp, iBeamMax0] = min(pppp);
	//	m_IncidentBeam = BeamMax0;
	// !ALTRAN - 45 begin
	//} else {
	// !ALTRAN End

//% iBeamMax0 = floor((sub(1) + sub(end))/2);
	m_IncidentBeam = floorw((BeamMax0 + BeamMax1) / 2.0f);
//% MS2 = nbBeams / 2;
//% if abs(iBeamMax0Full-MS2) < abs(iBeamMax0-MS2)
//%     iBeamMax0 = iBeamMax0Full;
//% end

	float MS2 = m_dwReceiveBeams / 2.0f - 1.0f;
	if (fabs(iBeamMax0Full-MS2) < fabs(m_IncidentBeam-MS2))
		m_IncidentBeam = iBeamMax0Full;
	// !ALTRAN - 45 begin
	//}
	// !ALTRAN End
	

	if (m_IncidentBeam == 0 || m_IncidentBeam >= m_dwReceiveBeams-1)
		m_IncidentBeam = (WORD)(m_dwReceiveBeams/2-1);
	else
	{
		WORD k = m_IncidentBeam;
		for (k = m_IncidentBeam + 1; k < m_dwReceiveBeams-1; k++)
		{
			if (m_pR1[k] && m_pR1[k+1] && m_pR1[k] <= m_pR1[k+1])
				m_IncidentBeam = k + 1;
			else
				break;
		}

		for (k = m_IncidentBeam - 1; k > 0; k--)
		{
			if (m_pR1[k-1] && m_pR1[k] && m_pR1[k-1] <= m_pR1[k])
				m_IncidentBeam = k - 1;
			else
				break;
		}
	}


//%	pppp = cumsum(AMax2(1:iBeamMax0));
//%	pppp = pppp / max(pppp);
//%	iBeamBeg = find(pppp >= 0.005, 1, 'first');
//%	
//%	pppp = cumsum(AMax2(iBeamMax0:end));
//%	pppp = pppp / max(pppp);
//%	iBeamEnd = iBeamMax0 - 1 + find(pppp > 0.995, 1, 'first');

	_cumsum(m_pfBeamsB,                m_pfBeamsC,                m_IncidentBeam+1,                true);
	_cumsum(m_pfBeamsB+m_IncidentBeam, m_pfBeamsC+m_IncidentBeam, m_dwReceiveBeams-m_IncidentBeam, true);

	for (iB = 0; iB < (int)m_dwReceiveBeams-1 && m_pfBeamsC[iB] < 0.005f; iB++)
	{
		m_pDetectionType[iB] = 0;
	}

	m_pDetectionType[iB] = 0;			// Has to do with how find works

	// !ALTRAN - 31 Begin
	m_iBeamBeg = iB;
	// !ALTRAN End

	for (iB = m_dwReceiveBeams-1; iB >= m_IncidentBeam && m_pfBeamsC[iB] > 0.995f; iB--)
	{
		m_pDetectionType[iB] = 0;
	}

	// !ALTRAN - 32 Begin
	m_iBeamEnd = iB;
	// !ALTRAN End

	// Second serie of maximals
//%	RMax2 = RMax1;
//%	for iBeam=2:(nbBeams-1)
//%		RMax2(iBeam) = (RMax1(iBeam-1)+RMax1(iBeam+1)) / 2;
//%	end
//%	R0 = max(R0,min(RMax1));

	iS = REDUCTION_MATRIX_SIZE;
	m_pR2[0] = m_pR1[0];
	m_pR2[m_dwReceiveBeams-1] = m_pR1[m_dwReceiveBeams-1];
	for (iB=1; iB<(int)m_dwReceiveBeams-1;iB++)
	{
		if (ISNAN(R1NaN[iB-1]) || ISNAN(R1NaN[iB+1]))
			NAN(R2NaN[iB]);
		else
			m_pR2[iB] = (m_pR1[iB-1] + m_pR1[iB+1]) / 2.0f;

		if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < (float)iS) 
			iS = floori(m_pR1[iB]);
	}

	if (iS < REDUCTION_MATRIX_SIZE)
	{
		m_R0 = min(m_R0, (float)iS);
	}

	return true;
}


#define min2(a,b)              (((a) == 0) ? (b) : ((a) < (b)) ? (a) : (b))

#define min3(a,b,c)            (((a) == 0) ? min2((b),(c)) : ((a) < (b)) ? min2((a),(c)) : min2((b),(c)))
#define max3(a,b,c)            (((a) > (b)) ? (((a)>(c))?(a):(c)) : (((b)>(c))?(b):(c)))

// --------------------------------------------------------------------
// _mask_beam_single - adjusting mask around estimation point
//
// Return value: none
//
// [INPUT]  m_R0

bool CIFremerBottomDetect::_mask_beam_single( WORD Beam, float CenterPoint, DWORD WMin, DWORD Width1, DWORD Width2, float R0Seuil, WORD step, float R0, bool checkR0 )
{
//%	sub = RMax1(iBeam)-w1:RMax1(iBeam)+w2;
	int iMin = floori(CenterPoint) - (int)Width1;
	int iMax = floori(CenterPoint) + (int)Width2+1;


	if (iMin < 0)
		iMin = 0;

//%	if (RMax1(iBeam) / R0)  < 1.1
//%		sub = RMax1(iBeam)-w1:RMax1(iBeam)+2*w2;
//%	end

	if (checkR0 && CenterPoint < R0 * 1.1f )
		iMax = floori(CenterPoint) + (int)Width2*2+1;

//%	sub(sub > size(Amp,1)) = [];
	if (iMax > m_iOutputSamples) 
		iMax = m_iOutputSamples;

//%	sub(sub < (R0-step)) = [];
	if (iMin < R0 - step)		 
		iMin = ceili(R0 - step);

//%	if RMax1(iBeam) > (R0*1.25)
//%	    sub(sub < (R0+WMin)) = [];
//%	end
	if (CenterPoint > (DWORD)((R0+1)*R0Seuil-1.0f) && iMin < R0 + WMin )
		iMin = ceili(R0 + WMin);

//%	Masque(sub, iBeam) = 1;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	int iS;

	if (m_dwDeviceId != 7111) {
		for (iS = iMin; iS < iMax; iS++)
		{
			m_pMatrix[iS][Beam] = data[iS*m_dwReceiveBeams+Beam].amp;
		}
	}

	m_pMaskMin[Beam] = min(m_pMaskMin[Beam], (DWORD)iMin);
	m_pMaskMax[Beam] = max(m_pMaskMax[Beam], (DWORD)iMax);
	m_MaskMin = min(m_MaskMin, iMin);
	m_MaskMax = max(m_MaskMax, iMax);

	return true;
}


// --------------------------------------------------------------------
// _nominal_beams_7111 - index determination for semi nominal beams
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT]  m_pNominalBeams

bool CIFremerBottomDetect::_nominal_beams_7111(  )
{

#include "params7111_201_v2.h"
	
	int iB, iNB;

	if (m_dwReceiveBeams < 300)		// 201 or 101 mode. All beams are nominal
	{
		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++ )
			m_pNominalBeams[iB] = true;
	}	
	else
	{
		for (iB = 0; iB < 201; iB++ )
			m_pfBeamsC[iB] = (float) Steering_Angles[iB];

		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++ )
		{
			m_pfBeamsB[iB] = (float)iB;
			m_pNominalBeams[iB] = false;
		}

		Interp1 (m_pBeamAngles, m_pfBeamsB, m_dwReceiveBeams, m_pfBeamsC, m_pfBeamsA, 201);
		
		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++ )
		{
			iNB = floori(m_pfBeamsA[iB]);
			if (iNB > 0 && iNB < 301)
				m_pNominalBeams[iNB] = true;
		}
	}

	return true;
}


// --------------------------------------------------------------------
// _suppress_steps - removes "stacking" from estimation
//
// Return value: none
//
// [INPUT]   m_pR1 (on reduced matrix)
// [TEMP]    m_pfBeamsA, m_pfBeamsB, m_pfBeamsC
//
// [MATLAB] y      === SamplesFiltre
// [MATLAB] R      === m_pR1 (on reduced matrix)
// [MATLAB] Teta   === not used in MatLab / not calculated
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_mask_7111
//
// MatLab reference:
//		suppres_steps_V2
//
void CIFremerBottomDetect::_suppress_steps (float * SamplesFiltre)
{
	float * iCenter	= m_pfBeamsA;
	float * rCenter	= m_pfBeamsB;
	int     nCenter = 0;
	float * indeces = m_pfBeamsC;

	DWORD * R1NaN   = (DWORD*) m_pR1;
	DWORD * FiltNaN = (DWORD*) SamplesFiltre;

	int iBeg = -1, iEnd, iSub;

	for (iSub = 0; iSub < (int)m_dwReceiveBeams; iSub++)
	{
		indeces[iSub] = (float)iSub;

//%	sub = find(~isnan(R)); 
		if (ISNAN(R1NaN[iSub]))
			continue;

		if (iBeg == -1)
		{
//%	iBeg = sub(1);
//%	iEnd = sub(1);
			iBeg = iSub;
			iEnd = iSub;
			continue;
		}

		if (m_pR1[iEnd] == m_pR1[iSub])
		{
			iEnd = iSub;
			continue;
		}

//%	iCenter = (iBeg + iEnd) / 2;
		iCenter[nCenter] = ((float)(iBeg + iEnd)) / 2.0f;
//%	R1Samples = floor((RMax1 -0.5) * step);

		// !ALTRAN - 9 begin
		rCenter[nCenter] = ((float)m_pR1[iBeg] - 0.5f) * m_ReduiteStep - 1.0f;
		// !ALTRAN end
		nCenter++;
		iBeg = iSub;
		iEnd = iSub;
	}

//%	if length(iBeg) == 1
//%	    y = R;
//%	    return
//%	end

	if (nCenter == 0)
	{
		for (iSub = 0; iSub < (int)m_dwReceiveBeams; iSub++)
		{
			if (m_pR1[iSub] == 0)
				NAN(FiltNaN[iSub]);
			else
				// !ALTRAN - 10 begin
				SamplesFiltre[iSub] = ((float)m_pR1[iBeg] - 0.5f) * m_ReduiteStep - 1.0f;
				// !ALTRAN end
		}

		return;
	}

	iCenter[nCenter] = ((float)(iBeg + iEnd)) / 2.0f;
	// !ALTRAN - 11 begin
	rCenter[nCenter] = ((float)m_pR1[iBeg] - 0.5f) * m_ReduiteStep - 1.0f;
	// !ALTRAN
	nCenter++;

//%	nx = length(R);
//%	y = interp1(iCenter, R(iBeg), 1:nx, 'linear', 'extrap');
	Interp1 (iCenter, rCenter, nCenter, indeces, SamplesFiltre, m_dwReceiveBeams);

//%	y(1:(sub(1)-1)) = NaN;
	for (iSub = 0; iSub < (int)m_dwReceiveBeams && ISNAN(R1NaN[iSub]); iSub++)
	{
		NAN(FiltNaN[iSub]);
	}

//%	y((sub(end)+1):nx) = NaN;
	for (iSub = m_dwReceiveBeams-1; iSub >= 0 && ISNAN(R1NaN[iSub]); iSub--)
	{
		NAN(FiltNaN[iSub]);
	}

	return;
}


// --------------------------------------------------------------------
// _mask_7111 - final determination for the mask for 7111
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT]  m_pMaskMin, m_pMaskMax
// [INPUT]   m_pR1, m_pR2
// [INPUT]   m_pNominalBeams
// [TEMP]    m_pfBeamsA, m_pfBeamsB, m_pfBeamsC, 
// [TEMP]    m_pfBeamsD             - R1SamplesFiltre
// [TEMP]    m_pdBeamsA				- MaskWidth
// [TEMP]    m_pdBeamsB, m_pdBeamsC - min/max natural mask
//
// [MATLAB] SystemSerialNumber      === Not relevant since separated functions are used
// [MATLAB] AmpImage                === 
// [MATLAB] BeamAngles              === m_pBeamAngles
// [MATLAB] ReceiveBeamWidth        === m_fReceiveBeamWidth
// [MATLAB] R0                      === m_R0       -- still on reduced matrix
// [MATLAB] RMax1                   === m_pR1      -- still on reduced matrix
// [MATLAB] RMax2                   === m_pR2      -- still on reduced matrix, double of actual value
// [MATLAB] R1SamplesFiltre         === calculated inside
// [MATLAB] AmpPingNormHorzMax      === not used in MatLab / not calculated
// [MATLAB] NbSamplesForPulseLength === calculated inside
// [MATLAB] MaskWidth               === calculated inside
// [MATLAB] MasqueAmp               === as m_pMaskMin/m_pMaskMax limits
//
// Sub-functions calls:
//		
// Used in sub-functions:
//      _suppress_steps - m_pfBeamsA, m_pfBeamsB, m_pfBeamsC
//
// Used in functions:
//		CalculateMask
//
// MatLab reference:
//		SampleBeam_PreprocAmpMask_7111_V6 (309:337)
//		ResonMasqueAmplitude_7111_V2
//
bool CIFremerBottomDetect::_mask_7111(  )
{
	int iB, iNB, iS;
	DWORD *R1NaN    = (DWORD*) m_pR1;
	DWORD *R2NaN    = (DWORD*) m_pR2;
	DWORD *FiltNaN  = (DWORD*) m_pfBeamsD;

//	REFERENCE:	SampleBeam_PreprocAmpMask_7111_V6 (309:337)
	
	// Local Time spreading of the signal around the maximum
	
//%	R1Samples = floor((RMax1 -0.5) * step);
//%	R1SamplesFiltre = suppres_steps_V2(R1Samples, Teta);

	float * R1SamplesFiltre = m_pfBeamsD;
	_suppress_steps(R1SamplesFiltre);

	DWORD * MaskWidth       = m_pMaskWidth;

//%	NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth);
	DWORD NbSamplesForPulseLength = ceildw(m_fTransmitPulseLength*m_fSampleRate);
	double MaskWidth1, MaskWidth2;
	double RBW2 = (double)m_fReceiveBeamWidth/2.0;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if(!ISNAN(FiltNaN[iB]))
			R1SamplesFiltre[iB] = floorf(R1SamplesFiltre[iB]);

//%	RMax1 = floor((RMax1 -0.5) * step);
		if (!ISNAN(R1NaN[iB]))
		{
			// !ALTRAN - 15 begin
			MaskWidth1 = ((double)m_pR1[iB] - 0.5) * m_ReduiteStep - 1.0;
			// !ALTRAN end
			m_pR1[iB]= floorf((float)MaskWidth1);
		}
		else
		{
			MaskWidth1 = 0.0;
		}

//%	RMax2 = floor((RMax2 -0.5) * step);
		if (!ISNAN(R2NaN[iB]))
		{
			// !ALTRAN - 15 begin
			MaskWidth2 = ((double)m_pR2[iB] - 0.5) * m_ReduiteStep - 1.0;
			// !ALTRAN end
			m_pR2[iB]= floorf((float)MaskWidth2);
		}
		else
		{
			MaskWidth2 = 0.0;
		}

		MaskWidth1 = max(MaskWidth1, MaskWidth2);

		if (MaskWidth1)
		{
//%	RxSamples = OriginalRange(RMaxx, 0, step);	=== R = (r+(0-1)-0.5) * step;
			MaskWidth1 -= m_ReduiteStep - 1;
//%	Zx = RxSamples .* cosd(Teta);
			MaskWidth1 *= cos(m_pBeamAngles[iB]);
//% MaskWidthx = ceil(Zx.* abs(( 1./ cosd(abs(Teta) + ReceiveBeamWidth/2))  - ( 1./ cosd(abs(Teta) - ReceiveBeamWidth/2))));
			MaskWidth1 *= fabs( (1/cos(fabs(m_pBeamAngles[iB]) + RBW2)) - ( 1/cos(fabs(m_pBeamAngles[iB]) - RBW2) ) );
		}

//%	MaskWidth = max(step, MaskWidth);
		MaskWidth[iB] = ceildw(MaskWidth1);
		MaskWidth[iB] = max((DWORD)m_ReduiteStep, MaskWidth[iB]);

		// Increase of 4 times the pulse width
		
//%	MaskWidth = MaskWidth + 4 * NbSamplesForPulseLength;
		MaskWidth[iB] += 4 * NbSamplesForPulseLength;
	}

//%	R0 = OriginalRange(R0, 0, step);
	float R0 = ((float)m_R0 - 0.5f) * m_ReduiteStep - 1.0f;
	m_R0 = R0;

	WORD nominalBeams;
	DWORD maskMin = m_iOutputSamples;
	WORD step    = ceilw((float)m_iOutputSamples/(float)REDUCTION_SAMPLE_SIZE);

	for (iB = nominalBeams = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (m_pNominalBeams[iB])
		{
			if (m_dwReceiveBeams == 301 && !m_pR1[iB])
				continue;

			m_pdBeamsD[nominalBeams]   = (DWORD)iB;
			m_pfBeamsA[nominalBeams++] = (float)m_pR1[iB];
	
			if (!ISNAN(R1NaN[iB]) && MaskWidth[iB] < maskMin) 
				maskMin = MaskWidth[iB];
		}
	}

	if (!nominalBeams)
		return false;

	_gradient(m_pfBeamsA, m_pfBeamsB, nominalBeams);

	if (m_Debug&DEBUG_OUTPUT_MASKAMP_LONG)
	{
		m_pRangePhase[0] = (float)maskMin;
		m_pRangePhase[1] = step;
		m_pRangePhase[2] = nominalBeams;
	}

	DWORD Grad, Width;
	m_MaskMin = m_iOutputSamples;
	m_MaskMax = 0;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	DWORD iBeam;

	for (iB = 0; iB < nominalBeams; iB++)
	{
		iBeam = m_pdBeamsD[iB];

		if (m_pfBeamsB[iB] < 0)
			Grad = 0;
		else
		{
			Grad = floordw(2.0f*m_pfBeamsB[iB]+0.5f);
				
			if (iB == 0 && ISNAN(R1NaN[m_pdBeamsD[iB]]))
				Grad = 0;
			else if (iB > 0 && ISNAN(R1NaN[m_pdBeamsD[iB-1]]))
				Grad = 0;
				
			if (iB == nominalBeams-1 && ISNAN(R1NaN[m_pdBeamsD[iB]]))
				Grad = 0;
			else if (iB < nominalBeams-1 && ISNAN(R1NaN[m_pdBeamsD[iB+1]]))
				Grad = 0;
		}

		Width = max(MaskWidth[iBeam], Grad);
		m_pMaskMin[iBeam] = m_iOutputSamples;
		m_pMaskMax[iBeam] = 0;

		if (Width)
		{
			if (!ISNAN(R1NaN[iBeam]))
				_mask_beam_single((WORD)iBeam, m_pR1[iBeam], maskMin, Width, Width, 1.25f, step, R0, true);

			if (!ISNAN(R2NaN[iBeam]))
				_mask_beam_single((WORD)iBeam, m_pR2[iBeam], maskMin, Width, Width, 1.25f, step, R0);

			if (!ISNAN(FiltNaN[iBeam]))
			{
				R1SamplesFiltre[iBeam] = floorf(R1SamplesFiltre[iBeam]);
				_mask_beam_single((WORD)iBeam, R1SamplesFiltre[iBeam],  maskMin, Width, Width, 1.25f, step, R0);
			}

			//%	iDeb = find(Masque(:, iBeam), 1, 'first');
			//%	iFin = find(Masque(:, iBeam), 1, 'last');
			//%	Masque(iDeb:iFin, iBeam)	= 1;
			
			for (iS = (int)m_pMaskMin[iBeam]; iS < (int)m_pMaskMax[iBeam]; iS++)
				m_pMatrix[iS][iBeam] = data[iS*m_dwReceiveBeams+iBeam].amp;
		}

		m_pMaskMin[iBeam] = m_pMaskMax[iBeam] = 0;
	}

	_mask_dilate(m_pdBeamsD, nominalBeams, 5);

	if (m_dwReceiveBeams < 300)
		return true;

	_raw_data_type * RawSample;
	float * AmpSample;
	DWORD * MaskSample;

	for (iS = m_MaskMin; iS < m_MaskMax; iS++)
	{
		AmpSample = m_pMatrix[iS];
		MaskSample = (DWORD*)m_pMatrix[iS];
		RawSample = &data[iS*m_dwReceiveBeams];
		
//%	k = subBeamsNominaux(1);
//%	for i=(k-1):-1:1
//%		Masque(:,i) = Masque(:,k);
//%	end

		if (!ISNAN(MaskSample[m_pdBeamsD[0]]))
		{
			for (iB = 0; iB < (int)m_pdBeamsD[0]; iB++)
				AmpSample[iB] = (float)(RawSample[iB].amp);
		}

//% for i=1:(length(subBeamsNominaux)-1)
//%     k1 = subBeamsNominaux(i)+1;
//%     k2 = subBeamsNominaux(i+1)-1;
//%     for k=k1:k2
//%         Masque(:,k) = Masque(:,subBeamsNominaux(i)) | Masque(:,subBeamsNominaux(i+1));
//%     end
//% end

		for (iNB = 1; iNB < nominalBeams; iNB++)
			if (!ISNAN(MaskSample[m_pdBeamsD[iNB-1]]) || !ISNAN(MaskSample[m_pdBeamsD[iNB]]))
			{
				for (iB = (int)m_pdBeamsD[iNB-1]+1; iB < (int)m_pdBeamsD[iNB]; iB++)
					AmpSample[iB] = (float)(RawSample[iB].amp);
			}

//% k = subBeamsNominaux(end);
//% for i=(k+1):301
//%     Masque(:,i) = Masque(:,k);
//% end

		if (!ISNAN(MaskSample[m_pdBeamsD[nominalBeams-1]]))
		{
			for (iB = (int)m_pdBeamsD[nominalBeams-1]+1; iB < (int)m_dwReceiveBeams; iB++)
				AmpSample[iB] = (float)(RawSample[iB].amp);
		}
	}

	return true;
}


// --------------------------------------------------------------------
// _mask_7111 - final determination for the mask for 7111
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT]  m_pMaskMin, m_pMaskMax
// [INPUT]   m_pR1, m_pR2
// [INPUT]   m_pNominalBeams
// [TEMP]    m_pfBeamsA, m_pfBeamsB, m_pfBeamsC, 
// [TEMP]    m_pfBeamsD             - R1SamplesFiltre
// [TEMP]    m_pdBeamsA				- MaskWidth
// [TEMP]    m_pdBeamsB, m_pdBeamsC - min/max natural mask
//
// [MATLAB] SystemSerialNumber      === Not relevant since separated functions are used
// [MATLAB] AmpImage                === 
// [MATLAB] BeamAngles              === m_pBeamAngles
// [MATLAB] ReceiveBeamWidth        === m_fReceiveBeamWidth
// [MATLAB] R0                      === m_R0       -- still on reduced matrix
// [MATLAB] RMax1                   === m_pR1      -- still on reduced matrix
// [MATLAB] RMax2                   === m_pR2      -- still on reduced matrix, double of actual value
// [MATLAB] R1SamplesFiltre         === calculated inside
// [MATLAB] AmpPingNormHorzMax      === not used in MatLab / not calculated
// [MATLAB] NbSamplesForPulseLength === calculated inside
// [MATLAB] MaskWidth               === calculated inside
// [MATLAB] MasqueAmp               === as m_pMaskMin/m_pMaskMax limits
//
// Sub-functions calls:
//		
// Used in sub-functions:
//      _suppress_steps - m_pfBeamsA, m_pfBeamsB, m_pfBeamsC
//
// Used in functions:
//		CalculateMask
//
// MatLab reference:
//		SampleBeam_PreprocAmpMask_7150_V4 (423:537)
//		ResonMasqueAmplitude_7111_V2 -- ???
//
bool CIFremerBottomDetect::_mask_7150(  )
{
	int iB, iNB;
	DWORD *R1NaN    = (DWORD*) m_pR1;
	DWORD *R2NaN    = (DWORD*) m_pR2;
	DWORD *FiltNaN  = (DWORD*) m_pfBeamsD;

//	REFERENCE:	SampleBeam_PreprocAmpMask_7150_V4 (423:537)
	
	// Local Time spreading of the signal around the maximum
	
//%	R1Samples = floor((RMax1 -0.5) * step);
//%	R1SamplesFiltre = suppres_steps_V2(R1Samples, Teta);

	float * R1SamplesFiltre = m_pfBeamsD;
	_suppress_steps(R1SamplesFiltre);

	DWORD * MaskWidth = m_pMaskWidth;
	float   RBW2      = m_fReceiveBeamWidth/2.0f;

	DWORD * NaNA = (DWORD *)m_pfBeamsA;
	DWORD * NaNB = (DWORD *)m_pfBeamsB;
	DWORD * NaNC = (DWORD *)m_pfBeamsC;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (iB == 100)
			int toto=1 ;
		if (!ISNAN(R1NaN[iB]))
			// !ALTRAN - 15 begin
			m_pR1[iB]= floorf((m_pR1[iB] - 0.5f) * m_ReduiteStep - 1.0f);
			// !ALTRAN end

		if (!ISNAN(R2NaN[iB]))
			// !ALTRAN - 16 begin
			m_pR2[iB]= floorf((m_pR2[iB] - 0.5f) * m_ReduiteStep - 1.0f);
			// !ALTRAN end

		if (!ISNAN(FiltNaN[iB]))
		{
//%	Z = -R1SamplesFiltre .* cosd(Teta);
//% X = R1SamplesFiltre .* sind(Teta);
			m_pfBeamsA[iB] = - (R1SamplesFiltre[iB] + 1) * cosf(m_pBeamAngles[iB]);	// m_pfBeamsA === Z
			m_pfBeamsB[iB] =   (R1SamplesFiltre[iB] + 1) * sinf(m_pBeamAngles[iB]);	// m_pfBeamsB === X
			
//%	MaskWidth = ceil(-Z.* abs(( 1./ cosd(abs(Teta) + ReceiveBeamWidth/2))  - ( 1./ cosd(abs(Teta) - ReceiveBeamWidth/2))));
			MaskWidth[iB] = ceildw(-m_pfBeamsA[iB] * fabsf( (1/cosf(fabsf(m_pBeamAngles[iB]) + RBW2)) - ( 1/cosf(fabsf(m_pBeamAngles[iB]) - RBW2) ) ) );
		}
		else
		{
			NAN(MaskWidth[iB]);
			NAN(NaNA[iB]);
			NAN(NaNB[iB]);
		}
	}

//%	R0 = OriginalRange(R0, 0, step);
	float R0 = ((float)m_R0 - 0.5f) * m_ReduiteStep - 1.0f;
	m_R0 = R0;
	
//%	Slope = atan2(gradient(Z), gradient(X)) * (180/pi);
	_gradient(m_pfBeamsA, m_pfBeamsC, m_dwReceiveBeams);	// m_pfBeamsC === gradient(Z)
	_gradient(m_pfBeamsB, m_pfBeamsA, m_dwReceiveBeams);	// m_pfBeamsA === gradient(A)

//%	NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth);
	DWORD NbSamplesForPulseLength = ceildw(m_fTransmitPulseLength*m_fSampleRate);
	float MaskWidth1, Slope;
	float * DiffMaskWidth  = m_pfBeamsB;
	float * DiffMaskWidth2 = m_pfBeamsA;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (!ISNAN(NaNC[iB]) && !ISNAN(NaNA[iB]) && !ISNAN(MaskWidth[iB]))
		{
//% MaskWidth  = ceil(MaskWidth .* cosd(Teta) ./ cosd(min(85, abs(Teta-Slope))));
//% MaskWidth(isnan(Slope)) = NaN;
//% DiffMaskWidth = MaskWidth - MaskWidth0;
//% DiffMaskWidth(DiffMaskWidth < 0) = 0;
			Slope              = min(D2Rf(85.0f), fabsf(m_pBeamAngles[iB] - atan2f(m_pfBeamsC[iB], m_pfBeamsA[iB])));
			MaskWidth1         = ceilf(MaskWidth[iB] * cosf(m_pBeamAngles[iB]) / cosf(Slope));
			DiffMaskWidth[iB]  = max(0.0f, MaskWidth1 - (float)MaskWidth[iB]);
		}
		else
			NAN(NaNB[iB]);

//%	DiffMaskWidth2 = DiffMaskWidth;
		DiffMaskWidth2[iB] = DiffMaskWidth[iB];
	}

//%	L = 10;
//%	for iBeam=1+L:nbBeams-L
//%		S = DiffMaskWidth(iBeam-L:iBeam+L);
//%	    DiffMaskWidth2(iBeam) = max(abs(S));
//%	end

	int L = 10;
	for (iB = L; iB < (int)m_dwReceiveBeams-L-1; iB++)
	{
		for (iNB = iB-L; iNB <= iB+L; iNB++)
		{
			if (ISNAN(NaNB[iNB]))
				continue;

			if (ISNAN(NaNA[iB]) || DiffMaskWidth2[iB] < DiffMaskWidth[iNB])
				DiffMaskWidth2[iB] = DiffMaskWidth[iNB];
		}
	}

//% MaskWidth = MaskWidth0 + DiffMaskWidth2;
//% MaskWidth = max(MaskWidth, MaskWidth0);
//% MaskWidth = max(step, MaskWidth);
//% MaskWidth = MaskWidth + 1 * NbSamplesForPulseLength;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (ISNAN(MaskWidth[iB]))
		{
			MaskWidth[iB] = m_ReduiteStep+NbSamplesForPulseLength;
			continue;
		}

		if (!ISNAN(NaNA[iB]))
			MaskWidth[iB] += ceildw(DiffMaskWidth2[iB]);

		MaskWidth[iB]  = max(MaskWidth[iB], (DWORD)m_ReduiteStep);
		MaskWidth[iB] += NbSamplesForPulseLength;
	}


	DWORD maskMin = m_iOutputSamples;
	WORD step    = ceilw((float)m_iOutputSamples/(float)REDUCTION_SAMPLE_SIZE);
	float width; //, beam, beammax = (float)m_dwReceiveBeams-1;

	_gradient(R1SamplesFiltre, m_pfBeamsA, m_dwReceiveBeams);		// m_pfBeamsA === gradient(R1SamplesFiltre)

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		m_pdBeamsD[iB] = (DWORD) iB;
//		beam = (float)iB;
//		beam -= ((beam > beammax/2.0f) ? 1.0f : 0.0f);
//		beam /= beammax;
		width = (float)MaskWidth[iB];
		width += 4*NbSamplesForPulseLength;
//		width *= fabsf(beam - 0.5f) * 6.0f + 2.0f;

		if (!ISNAN(NaNA[iB]))												// m_pfBeamsB === w
			m_pfBeamsB[iB] = max(width, 2*m_pfBeamsA[iB]);
		else
			m_pfBeamsB[iB] = width;

		if (MaskWidth[iB] < maskMin) 
			maskMin = MaskWidth[iB];
	}

	m_MaskMin = m_iOutputSamples;
	m_MaskMax = 0;
	DWORD w1, w2;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (iB==100)
			int toto=1 ;

		w1 = ceildw(m_pfBeamsB[iB]);
		//w2 = ceildw(2*m_pfBeamsB[iB]);
		w2 = w1 ; //EPM just trying to improve the symetrie of the mask

		if (!ISNAN(R1NaN[iB]))
			_mask_beam_single((WORD)iB, m_pR1[iB], maskMin, w1, w2, 1.15f, step, R0);

		if (!ISNAN(R2NaN[iB]))
			_mask_beam_single((WORD)iB, m_pR2[iB], maskMin, w1, w2, 1.15f, step, R0);

		if (!ISNAN(FiltNaN[iB]))
		{
			R1SamplesFiltre[iB] = floorf(R1SamplesFiltre[iB]);
			_mask_beam_single((WORD)iB, R1SamplesFiltre[iB], maskMin, w1, w2, 1.15f, step, R0);
		}
	}


	_mask_dilate(m_pdBeamsD, (WORD)m_dwReceiveBeams, 5);

	return true;
}



// --------------------------------------------------------------------
// _mask_7111 - implementation of imdilate on AmplitudeMask
//
// Return value: none
//
// [OUTPUT]  m_pMatrix   - Amplitude Mask
// [INPUT]   m_pRAWData  - Original Amplitude Image
// [INPUT]   m_MaskMin   - Minimum valid sample
// [INPUT]   m_MaskMax   - Maximum valid sample
// [TEMP]    m_pfBeamsD
//
// [MATLAB] Masque      === m_pMatrix
// [MATLAB] se          === dilate (= length(se))
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_mask_7111
//
// MatLab reference:
//		ResonMasqueAmplitude_7111_V2(199:200)
//
void CIFremerBottomDetect::_mask_dilate(DWORD * beams, WORD nBeams, WORD dilate)
{
	int iB, iS, iNB;
	int nB = dilate/2;

	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	_raw_data_type * RawSample;
	float * AmpSample;

	for (iS = m_MaskMin; iS < m_MaskMax; iS++)
	{
		AmpSample = m_pMatrix[iS];
		memcpy(m_pdBeamsB, AmpSample, sizeof(float) * m_dwReceiveBeams);
		RawSample = &data[iS*m_dwReceiveBeams];
		
		for (iB = 0; iB < nBeams; iB++)
		{
			if (!ISNAN(m_pdBeamsB[beams[iB]]))
			{
				for (iNB = max(iB-nB,0); iNB < min(iB+nB+1, nBeams); iNB++)
				{
					AmpSample[beams[iNB]] = (float)(RawSample[beams[iNB]].amp);
				}
			}
		}
	}
}


// --------------------------------------------------------------------
// _amplitude_single_reduit - Detection sur le barycentre  on the reduced
//                            amplitude matrix.
//
// Return value: detection point
//
// [INPUT]  m_pAmpReduite;
// [TEMP]   m_pfBeamsA, m_pfBeamsB
// [TEMP]   m_pfSamplesA
//
// [MATLAB] x                  === 
// [MATLAB] Amp                === amp (full beam)
// [MATLAB] R0                 ===
// [MATLAB] iBeam
// [MATLAB] BeamAngles
// [MATLAB] nbSamples
// [MATLAB] AmpBeamRange       === (returned value)
// [MATLAB] AmpBeamQF          === (not used in MatLab / not returned)
// [MATLAB] AmpBeamSamplesNb   === (not used in MatLab / not returned)
// [MATLAB] AmpBeamQF2         === (not used in MatLab / not returned)
//
// Used in functions:
//		_detection_estimate
//
// MatLab reference:
//		SampleBeam_ReduceMatrixAmplitude
//
float CIFremerBottomDetect::_amplitude_single_reduit (float *amp, int iSampleBeg, int iSampleEnd, int iBeam, int index)
{
	int iMaskMin = iSampleBeg;
	int iMaskMax = iSampleEnd;
	
	// --------------------
	// Calcul du barycentre
	
	float Sigma;
	float AmpBeamRange = _barycentre(amp+iMaskMin, iMaskMin-iSampleBeg+1+index, iMaskMax-iMaskMin, Sigma, m_pfSamplesA)+iSampleBeg;
	
	// ------------------------------------------------
	// Restriction du domaine et recalcul du barycentre
	
	iMaskMin = max(floori(AmpBeamRange-2*Sigma)-index, iSampleBeg);
	iMaskMax = min(floori(AmpBeamRange+2*Sigma)-index, iSampleEnd);
	
	if (iMaskMax <= iMaskMin) return AmpBeamRange+0.5f;
	AmpBeamRange = _barycentre(amp+iMaskMin, iMaskMin-iSampleBeg+1+index, iMaskMax-iMaskMin, Sigma, m_pfSamplesA, (iMaskMax == iSampleEnd) ? 1 : 2)+iSampleBeg;
	
	// ---------------------------------------------------------------
	// Restriction du domaine et recalcul du barycentre
	
	iMaskMin = max(floori(AmpBeamRange-Sigma)-index, iSampleBeg);
	iMaskMax = min(floori(AmpBeamRange+Sigma)-index, iSampleEnd);
	
	if (iMaskMax <= iMaskMin) return (float)iSampleBeg + 0.5f; //EPM as per Jean-Marie 02-05-14
	
	AmpBeamRange = _barycentre(amp+iMaskMin, iMaskMin-iSampleBeg+1+index, iMaskMax-iMaskMin, Sigma, m_pfSamplesA) + iSampleBeg;
	
	//return AmpBeamRange;
	return AmpBeamRange + 0.5f; //EPM as per Jean-Marie 02-05-14
}

