/***************************************************************************
****************************************************************************

   FILE: IFremerMask.cpp

   Implementation of mask calculations functions for CIFremerBottomDetect class.

   Copyright � 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    March 2008
   Author:  Yevgeniy Shafirovich
   Change:  Initial implementation

****************************************************************************
***************************************************************************/

#include "stdafx.h"
#include "IFremerBottomDetect.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

static bool * MaskPhase = NULL;

void CIFremerBottomDetect::CreateState ()
{
	if (!m_Debug) return;

	if (!MaskPhase) MaskPhase = (bool*)malloc(m_iOutputSamples*m_dwReceiveBeams);

	if (_storage) delete _storage;
	_storage = new CStorage("CIFremerBottomDetect", dmxSTRUCT_CLASS);
    
	*_storage<< * new CStorage("m_nAllocSamples",		dmxUINT32_CLASS, 1, &m_nAllocSamples)
			 << * new CStorage("m_pfSamplesA",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesA)
			 << * new CStorage("m_pfSamplesB",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesB)
			 << * new CStorage("m_pfSamplesC",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesC)
			 << * new CStorage("m_pfSamplesD",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesD)
			 << * new CStorage("m_pfSamplesE",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesE)
			 << * new CStorage("m_pfSamplesF",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesF)
			 << * new CStorage("m_pfSamplesG",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesG)
			 << * new CStorage("m_pfSamplesH",			dmxSINGLE_CLASS, m_nAllocSamples, m_pfSamplesH)
			 << * new CStorage("m_pdSamplesA",			dmxUINT32_CLASS, m_nAllocSamples, m_pdSamplesA)
			 << * new CStorage("m_pdSamplesB",			dmxUINT32_CLASS, m_nAllocSamples, m_pdSamplesB)
			 << * new CStorage("m_pdBeamsA",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pdBeamsA)
			 << * new CStorage("m_pdBeamsB",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pdBeamsB)
			 << * new CStorage("m_pdBeamsC",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pdBeamsC)
			 << * new CStorage("m_pdBeamsD",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pdBeamsD)
			 << * new CStorage("m_pfBeamsA",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pfBeamsA)
			 << * new CStorage("m_pfBeamsB",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pfBeamsB)
			 << * new CStorage("m_pfBeamsC",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pfBeamsC)
			 << * new CStorage("m_pfBeamsD",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pfBeamsD)
			 << * new CStorage("m_pGatesMin",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pGatesMin)
			 << * new CStorage("m_pGatesMax",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pGatesMax)
			 << * new CStorage("m_pMaskMin",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pMaskMin)
			 << * new CStorage("m_pMaskMax",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pMaskMax)
			 << * new CStorage("m_pMaskWidth",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pMaskWidth)
			 << * new CStorage("m_pMatrix",				dmxSINGLE_CLASS, m_iOutputSamples, m_dwReceiveBeams, m_pMatrix[0])
			 << * new CStorage("m_pPhaseMask",			dmxLOGICAL_CLASS,m_iOutputSamples, m_dwReceiveBeams, MaskPhase)
			 << * new CStorage("m_pAmpReduite",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_nMaskSamples, m_pAmpReduite[0])
			 << * new CStorage("m_ReduiteSize",			dmxUINT16_CLASS, 1, &m_ReduiteSize)
			 << * new CStorage("m_ReduiteStep",			dmxUINT32_CLASS, 1, &m_ReduiteStep)
			 << * new CStorage("m_pR1",					dmxSINGLE_CLASS, m_dwReceiveBeams, m_pR1)
			 << * new CStorage("m_pR2",					dmxSINGLE_CLASS, m_dwReceiveBeams, m_pR2)
			 << * new CStorage("m_pFlagBeams",			dmxLOGICAL_CLASS,m_dwReceiveBeams, m_pFlagBeams)
			 << * new CStorage("m_R0",					dmxSINGLE_CLASS, 1, &m_R0)
			 << * new CStorage("m_IncidentBeam",		dmxUINT16_CLASS, 1, &m_IncidentBeam)
			 << * new CStorage("m_pNominalBeams",		dmxLOGICAL_CLASS,m_dwReceiveBeams, m_pNominalBeams)
			 << * new CStorage("m_pRangeAmplitude",		dmxSINGLE_CLASS, m_dwReceiveBeams, m_pRangeAmplitude)
			 << * new CStorage("m_pSamplesAmp",			dmxUINT32_CLASS, m_dwReceiveBeams, m_pSamplesAmp)
			 << * new CStorage("m_pQualityAmplitude",	dmxSINGLE_CLASS, m_dwReceiveBeams, m_pQualityAmplitude)
			 << * new CStorage("m_pQF2Amplitude",		dmxSINGLE_CLASS, m_dwReceiveBeams, m_pQF2Amplitude)
			 << * new CStorage("m_DeltaTeta",			dmxSINGLE_CLASS, 1, &m_DeltaTeta)
			 << * new CStorage("m_pRangePhase",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pRangePhase)
			 << * new CStorage("m_pQualityPhase",		dmxSINGLE_CLASS, m_dwReceiveBeams, m_pQualityPhase)
			 << * new CStorage("m_pSamplesPhase",		dmxUINT32_CLASS, m_dwReceiveBeams, m_pSamplesPhase)
			 << * new CStorage("m_pERMPhase",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pERMPhase)
			 << * new CStorage("m_pSlopePhase",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pSlopePhase)
			 << * new CStorage("m_pDetectionRange",		dmxSINGLE_CLASS, m_dwReceiveBeams, m_pDetectionRange)
			 << * new CStorage("m_pDetectionQuality",	dmxSINGLE_CLASS, m_dwReceiveBeams, m_pDetectionQuality)
			 << * new CStorage("m_pDetectionType",		dmxINT8_CLASS, m_dwReceiveBeams, m_pDetectionType)
			 << * new CStorage("m_dwDeviceId",			dmxUINT32_CLASS, 1, &m_dwDeviceId)
			 << * new CStorage("m_pBeamAngles",			dmxSINGLE_CLASS, m_dwReceiveBeams, m_pBeamAngles)
			 << * new CStorage("m_pRAWData",			dmxUINT16_CLASS, m_dwReceiveBeams*m_iOutputSamples, 2, m_pRAWData)
			 << * new CStorage("m_dwReceiveBeams",		dmxUINT32_CLASS, 1, &m_dwReceiveBeams)
			 << * new CStorage("m_dwDeviceId",			dmxUINT32_CLASS, 1, &m_dwDeviceId)
			 << * new CStorage("m_fSampleRate",			dmxSINGLE_CLASS, 1, &m_fSampleRate)
			 << * new CStorage("m_fSoundVelocity",		dmxSINGLE_CLASS, 1, &m_fSoundVelocity)
			 << * new CStorage("m_iOutputSamples",		dmxINT32_CLASS, 1, &m_iOutputSamples)
			 << * new CStorage("m_fAbsorption",			dmxSINGLE_CLASS, 1, &m_fAbsorption)
			 << * new CStorage("m_fCenterFrequency",	dmxSINGLE_CLASS, 1, &m_fCenterFrequency)
			 << * new CStorage("m_fReceiveBeamWidth",	dmxSINGLE_CLASS, 1, &m_fReceiveBeamWidth)
			 << * new CStorage("m_fTransmitPulseLength",dmxSINGLE_CLASS, 1, &m_fTransmitPulseLength)
			 << * new CStorage("m_dwPingNumber",		dmxUINT32_CLASS, 1, &m_dwPingNumber);
}

void CIFremerBottomDetect::SaveState (char * filename) {
	if (!_storage || !m_Debug) return;

	// Update Phase Mask...
	for (int s = 0; s < m_iOutputSamples; s++)
		for (int b = 0; b < (int)m_dwReceiveBeams; b++)
			MaskPhase[s*m_dwReceiveBeams+b] = MasquePhs(b, s);

	FILE * fout;
	char name[256];
	sprintf(name, "%s.struct", filename);
	if ( fopen(&fout, name, "wb") == NULL ) {
		int a;
		a = errno;

		a = _doserrno;

		return;
	}


	_storage->Save(fout);
	fclose(fout);
}

void CIFremerBottomDetect::DestroyState () {
	if (_storage) delete _storage;
}
