/***************************************************************************
****************************************************************************

   FILE: IFremerMath.cpp

   Implementation of supplemental math functions for CIFremerBottomDetect class.

   Copyright � 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    March 2008
   Author:  Yevgeniy Shafirovich
   Change:  Initial implementation and adaptation

****************************************************************************
***************************************************************************/

#include "stdafx.h"

#if !defined INCLUDE_IFREMER_MATH_TEMPLATE

#include "stdio.h"
#include "stdlib.h"
#include "search.h"

#define INCLUDE_IFREMER_OVERWRITES
#include "IFremerBottomDetect.h"
#include <float.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//------------------------------------------------------------------------------
// Subtracts mean value of the array from the array
//
// Arguments:
// [IN]  X      - input array
// [OUT] D      - resulting array
// [IN]  nX     - size of X and D arrays and filter
// [IN]  filter - "NaN filter" with false corresponding to NaN elements.
//
// Return Value: subtracted mean value.
//
/*
float CIFremerBottomDetect::_centralize(const float *X, float *D, const int nX, const bool * filter)
{	
	float m = 0.0;
	int np = nX, i;
	float *Di;
	
	for (i=0; i<nX; i++)
	{
		if (!(filter[i])) 
			np--;
		else
			m += X[i];
	}
	
	if (!np) return 0.0f;

	m /= np;

	for (i=0, Di=D; i<nX; i++, Di++)
	{
		if (!(filter[i])) 
			*Di = 0.0f;
		else
			*Di = X[i]-m;
	}

	return m;
}
*/
//------------------------------------------------------------------------------
// Calculates standard deviation assuming that mean value is zero
//
// Arguments:
// [IN]  X      - input array
// [IN]  nX     - size of X array and filter
// [IN]  filter - "NaN filter" with false corresponding to NaN elements.
//
// Return Value: STD.
//
/*
float CIFremerBottomDetect::_normalStd (const float *X, const int nX, const bool * filter)
{
	float std = 0.0;
	int i;
	
	int np = nX;
	
	for (i=0; i<nX; i++)
	{
		if (!(filter[i]))
			np--;
		else
			std += X[i]*X[i];
	}

	std = (float) sqrt(std / (float) (np - 1));
	
	return(std);
}
*/
//------------------------------------------------------------------------------
// Calculates standard deviation
//
// Arguments:
// [IN]  X      - input array
// [IN]  nX     - size of X array and filter
// [IN]  filter - "NaN filter" with false corresponding to NaN elements.
//
// Return Value: STD.
//
float CIFremerBottomDetect::_nanStd (const float *X, const int nX, const DWORD * filter)
{
	float std = 0.0;
	int i, np = nX;
	
	if (nX <= 1)
		return 0;
	
	float m = 0.0f;

	for (i=0; i<nX; i++)
	{
		if (!(filter[i]))
		{
			np--;
		}
		else
		{
			m += X[i];
		}
	}

	m /= np;

	for (i=0; i<nX; i++)
	{
		if (filter[i])
		{
			std += (X[i]-m)*(X[i]-m);
		}
	}
	
	std = (float)sqrt(std / (double) (np - 1));
	
	return(std);
}


//------------------------------------------------------------------------------
// Calculates gradient of the vector
//
// Arguments:
// [IN]  X      - input array
// [OUT] G      - computed gradient
// [IN]  nX     - size of X array and filter
//
void CIFremerBottomDetect::_gradient (const float *X, float *G, const int nX)
{
	DWORD * NaNX = (DWORD *)X;
	DWORD * NaNG = (DWORD *)G;

	if (nX == 1)
	{
		G[0] = 0;
		return;
	}

	if (ISNAN(NaNX[0]) || ISNAN(NaNX[1]))
		NAN(NaNG[0]);
	else
		G[0] = (X[1]-X[0]);

	if (ISNAN(NaNX[nX-1]) || ISNAN(NaNX[nX-2]))
		NAN(NaNG[nX-1]);
	else
		G[nX-1] = (X[nX-1]-X[nX-2]); 

	for (int i = 1; i < nX - 1; i++)
	{
		if (ISNAN(NaNX[i+1]) || ISNAN(NaNX[i-1]))
		{
			NAN(NaNG[i]);
		}
		else
		{
			G[i] = (X[i+1]-X[i-1])/2;
		}
	}
}


/*---------------------------------------------------------------------------
Function : kth_smallest()
In : array of elements, # of elements in the array, rank k
Out : one element
Job : find the kth smallest element in the array
Notice : use the median() macro defined below to get the median.
Reference:
Author: Wirth, Niklaus
Title: Algorithms + data structures = programs
Publisher: Englewood Cliffs: Prentice-Hall, 1976
Physical description: 366 p.
Series: Prentice-Hall Series in Automatic Computation
---------------------------------------------------------------------------*/
#define ELEM_SWAP(a,b) { t=(a);(a)=(b);(b)=t; }

//------------------------------------------------------------------------------
// Calculates median value of the vector
//
// Arguments:
// [IN]  X      - input array. NOTE: array WILL be altered by the function if T is not provided
// [IN/OUT] T   - Temporal vector of size nX, will be used instead of X if needed
// [IN]  nX     - size of X array and filter
//
// Return Value: approximate Median. The value MAY fluctuate few elements from the central
//               does not work on small number of elements, but much faster then the regular
//               sorting median.
//
DWORD CIFremerBottomDetect::_median(DWORD *X, DWORD *T, int nX)
{
	register int i,j,l,m;
	register DWORD x, t;

	DWORD * XX;

	if (T)
	{
		memcpy (T, X, sizeof(DWORD)*nX);
		XX = T;
	}
	else
		XX = X;

	int k = nX/2;		// due to integer division rules, k=nX/2 for even nX, and k=(nX-1)/2 for odd nX
	l=0; m=nX-1;
	while (l<m) {
		x=XX[k];
		i=l;
		j=m;
		do {
			while (XX[i]<x) i++;
			while (x<XX[j]) j--;
			if (i<=j) {
				ELEM_SWAP(XX[i],XX[j]);
				i++; j--;
			}
		} while (i<=j);
		if (j<k) l=i;
		if (k<i) m=j;
	}
	return XX[k];
}



static int fcompare( const void *arg1, const void *arg2 )
{
	/* Compare all of both strings: */
	return (*(float*)arg1 == *(float*)arg2) ? 0 : (*(float*)arg1 < *(float*)arg2) ? -1 : 1;
}

float CIFremerBottomDetect::_median(float *X, float *T, int nX)
{
	float * XX;
	
	if (T)
	{
		memcpy (T, X, sizeof(float)*nX);
		XX = T;
	}
	else
		XX = X;

	qsort((void *) XX, nX, sizeof(float), fcompare);

	if (nX%2 == 1)
		return XX[(nX-1)/2];
	else
		return (XX[(nX-1)/2] + XX[(nX+1)/2]) / 2.0f;

/*
	register i,j,l,m;
	register float x, t;
	
	float * XX;
	
	if (T)
	{
		memcpy (T, X, sizeof(float)*nX);
		XX = T;
	}
	else
		XX = X;
	
	int k = nX/2;		// due to integer division rules, k=nX/2 for even nX, and k=(nX-1)/2 for odd nX
	l=0; m=nX-1;
	while (l<m) {
		x=XX[k];
		i=l;
		j=m;
		do {
			while (XX[i]<x) i++;
			while (x<XX[j]) j--;
			if (i<=j) {
				ELEM_SWAP(XX[i],XX[j]);
				i++; j--;
			}
		} while (i<=j);
		if (j<k) l=i;
		if (k<i) m=j;
	}
	return XX[k];
*/
}


//------------------------------------------------------------------------------
// Calculates mean of the vector
//
// Arguments:
// [IN]  X      - input array.
// [IN]  nX     - size of X and S arrays
//
// Return Value: mean of X
//
float CIFremerBottomDetect::_mean(const float *X, const int nX)
{	
	float m = 0.0;
	
	if (nX <= 0)
		return(0.0f); 
	
	for (int i=0; i<nX; i++)
	{
		m += X[i];
	}
	
	return(m / nX);
}


//------------------------------------------------------------------------------
// Calculates mean of the vector, ignoring NaN
//
// Arguments:
// [IN]  X      - input array.
// [IN]  nX     - size of X and S arrays
//
// Return Value: mean of X
//
float CIFremerBottomDetect::_nanmean(const float *X, const int nX)
{	
	const DWORD * nanX = (DWORD *)X;
	float m = 0.0;
	int nI = 0;
	
	if (nX <= 0)
		return(0.0f); 
	
	for (int i=0; i<nX; i++)
	{
		if (!ISNAN(nanX[i]))
		{
			m += X[i];
			nI++;
		}
	}

	if (!nI)
	{
		NANf(m);
		return m;
	}

	return(m / nI);
}


#else	// INCLUDE_IFREMER_MATH_TEMPLATE

//------------------------------------------------------------------------------
// Calculates center of gravity and STD of the vector
//
// Arguments:
// [IN]  X      - input array.
// [IN]  MinX   - first continuous index
// [IN]  iX     - indices array.
// [IN]  nX     - size of X and iX arrays
// [OUT] Sigma  - STD
// [IN/OUT] T   - Temporal vector of size nX;
//
// Return Value: position of the center of gravity
//
template <class DATA> 
float _barycentre(const DATA *X, const int MinX, const int nX, float &Sigma, float *T, int order = 2)
{
	int iX, iO;
	float *pT;
	
	float CumEnr = 0;
	
	
	for (iX=0, pT = T; iX<nX; iX++, pT++)
	{
		for (iO = 0, *pT = 1.0f; iO < order; iO++, *pT *= (float)X[iX]);		// Enr = Amp .^ 2;
		CumEnr += *pT;
	}

	// !ALTRAN - 22 Begin
	if(CumEnr == 0){
		Sigma=0;
		return -1;
	}
	// !ALTRAN end
	
	float BC0, BC = 0, M2 = 0, VX = 0;
	
	for (iX=0, pT = T; iX<nX; iX++, pT++)
	{
		*pT /= CumEnr;															// Enr = Enr / sum(Enr);
		BC0 = (iX + MinX) * (*pT);												
		BC += BC0;																// R0 = sum(R .* Enr);
	}

	for (iX=0, pT = T; iX<nX; iX++, pT++)
	{
		M2 = (iX + MinX) - BC;
		VX += M2 * M2 * (*pT);
	}
	
	if (VX > 0)
		Sigma = (float)sqrt(VX);
	else
		Sigma = -1.0f;
	
	// !ALTRAN - 46 Begin
	if(BC < MinX)
	{
		BC = MinX;
	}
	if(BC > MinX + nX)
	{
		BC = MinX + nX;
	}
	// !ALTRAN End

	return BC;
}


template <class DATA1, class DATA2> 
DATA2 _barycentre(const DATA1 *X, const DWORD * iX, const int nX, DATA2 &Sigma, DATA2 *T, int order = 2)
{
	int i, iO;
	DATA2 *pT;
	
	DATA2 CumEnr = 0;
	DATA2 BC0, BC = 0, M2 = 0, VX = 0;
	
	for (i=0, pT = T; i<nX; i++, pT++)
	{
		for (iO = 0, *pT = 1.0; iO < order; iO++, *pT *= X[i]);
		CumEnr += *pT;
	}

	// !ALTRAN - 23 Begin
	if(CumEnr == 0){
		Sigma=0;
		return -1;
	}
	// !ALTRAN end
	
	for (i=0, pT = T; i<nX; i++, pT++)
	{
		*pT /= CumEnr;
		BC0 = iX[i] * (*pT);
		BC += BC0;
	}

	for (i=0, pT = T; i<nX; i++, pT++)
	{
		M2 = iX[i] - BC;
		VX += M2 * M2 * (*pT);
	}
	
	if (VX > 0)
		Sigma = sqrt(VX);
	else
		Sigma = -1.0;
	
	// !ALTRAN - 46 Begin
	if(BC < iX[0])
	{
		BC = iX[0];
	}
	if(BC > iX[nX-1])
	{
		BC = iX[nX-1];
	}
	// !ALTRAN End

	return BC;
}


//------------------------------------------------------------------------------
// Calculates cumulative sum of the vector
//
// Arguments:
// [IN]  X      - input array.
// [OUT] S      - cumulative sum vector
// [IN]  nX     - size of X and S arrays
// [IN]  norm...- if true S is normalized between 0 and 1
//
// Return Value: original sum of X
//
template <class DATA> 
DATA _cumsum(const DATA *X, float *S, const int nX, const bool normalize)
{
	int iX;
	DATA cumSum = 0;
	float * iS;
	DWORD * iD;
	
	for (iX=0, iS = S; iX<nX; iX++, iS++)
	{
		// !ALTRAN - 33 Begin
		if(ISNANf(X[iX])) // cum only if not NAN
		{
			cumSum += 0;
		}
		else
		{
			cumSum += X[iX];
		}
		// !ALTRAN End
		*iS = (float) cumSum;
	}
	
	if (normalize && cumSum != 0)
	{
		for (iX=0, iS = S; iX<nX; iX++, iS++)
		{
			*iS /= cumSum;
		}
	}
	else if (normalize && cumSum == 0)
	{
		for (iX=0, iD = (DWORD *)S; iX<nX; iX++, iD++)
		{
			if (*iD == 0)
				NAN(*iD);
			else
				INFS(*iD, *iD);
		}
	}
	
	return cumSum;
}


#endif  // INCLUDE_IFREMER_MATH_TEMPLATE