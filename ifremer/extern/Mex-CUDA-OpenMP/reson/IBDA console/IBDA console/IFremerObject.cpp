/***************************************************************************
****************************************************************************

   FILE: IFremerObject.cpp

   Implementation of generic handling functions for CIFremerBottomDetect class.

   Copyright � 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    March 2008
   Author:  Yevgeniy Shafirovich
   Change:  Initial implementation

****************************************************************************
***************************************************************************/

#include "stdafx.h"
#include "IFremerBottomDetect.h"
#include <memory>

#ifdef _DEBUG
#	undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#	ifndef RUN_STAND_ALONE
#		define new DEBUG_NEW
#	endif
#endif

CIFremerBottomDetect::CIFremerBottomDetect()
{
	m_dwReceiveBeams = 0;
	m_dwDeviceId     = 0;
	m_nAllocSamples  = 0;
	m_nMaskSamples   = 0;
	m_nStrips        = 0;
	m_ReduiteSize	 = 0;

	m_pGatesMin		 = NULL;
	m_pGatesMax		 = NULL;
	m_pAmpReduite	 = NULL;
	m_pfReduite	     = NULL;
	
	m_pMatrix        = NULL;
	m_pR1			 = NULL;
	m_pR2			 = NULL;
	m_pNominalBeams	 = NULL;
	m_pMaskMin		 = NULL;
	m_pMaskMax		 = NULL;
	m_pMaskWidth	 = NULL;
	m_pFlagBeams	 = NULL;
	
	m_pRangeAmplitude   = NULL;
	m_pSamplesAmp       = NULL;
	m_pQualityAmplitude = NULL;
	m_pQF2Amplitude		= NULL;
	m_pRangePhase		= NULL;
	m_pQualityPhase		= NULL;
	m_pSamplesPhase     = NULL;
	m_pERMPhase			= NULL;
	m_pSlopePhase		= NULL;
	m_pDetectionRange	= NULL;
	m_pDetectionQuality	= NULL;
	m_pDetectionType	= NULL;
	m_pAmpWinMin		= NULL;
	m_pAmpWinMax		= NULL;
	m_pPhsWinMin		= NULL;
	m_pPhsWinMax		= NULL;

	m_pdBeamsA       = NULL;
	m_pdBeamsB       = NULL;
	m_pdBeamsC       = NULL;
	m_pdBeamsD       = NULL;
	m_pfBeamsA       = NULL;
	m_pfBeamsB       = NULL;
	m_pfBeamsC       = NULL;
	m_pfBeamsD       = NULL;

	m_pfSamplesA     = NULL;
	m_pfSamplesB     = NULL;
	m_pfSamplesC     = NULL;
	m_pfSamplesD     = NULL;
	m_pfSamplesE     = NULL;
	m_pfSamplesF     = NULL;
	m_pfSamplesG     = NULL;
	m_pfSamplesH     = NULL;
	m_pfdSamples     = NULL;
	m_pdSamplesA     = NULL;
	m_pdSamplesB     = NULL;
	//_storage		 = NULL;
}

CIFremerBottomDetect::~CIFremerBottomDetect()
{
	if (m_dwDeviceId)
		CleanUpAll();	

	DestroyState();
}

#ifdef RUN_STAND_ALONE

#include <stdio.h>

#pragma message (">>> Compile stand-alone.")

int themain ()
{
	int m_Debug = 0xffff;

		CIFremerBottomDetect bd;
		bd.m_Debug = m_Debug;
		if (!bd.PreloadProcessing ("pingdata.dat"))
		{
			printf ("Failed to instantiate Bottom Detect object.\n");
			return 1;
		}


		if (!bd.CalculateMask())
		{
			printf ("Failed to process Mask.\n");
			return 1;
		}

		int beams;
		if (!bd.BottomDetection (NULL, beams))
		{
			printf ("Bottom Detection Failed.\n");
			return 1;
		}

	return 0;
}

bool CIFremerBottomDetect::PreloadProcessing (char * filename)
{
	FILE * fp;

	CleanUpBeam();
	
	if ( (fp = fopen(filename, "rb")) == NULL )
	{
		printf ("Failed to open data file \'%s\'\n", filename);
		return false;
	}

	int iB;
	DWORD dwTemp;

	fread (&m_dwDeviceId, sizeof(DWORD), 1, fp);
	fread (&m_dwPingNumber, sizeof(DWORD), 1, fp);

	fread (&m_dwReceiveBeams, sizeof(DWORD), 1, fp);
	fread (&dwTemp,           sizeof(DWORD), 1, fp);
	m_iOutputSamples = (int)dwTemp;

	if (!m_dwDeviceId || m_dwReceiveBeams < 10 || m_iOutputSamples < 10)
	{
		printf ("Invalid file format or misformed ping data: \'%s\'\n", filename);
		fclose(fp);
		return false;
	}

	fread (&m_fSoundVelocity,       sizeof(float), 1, fp);
	fread (&m_fSampleRate,          sizeof(float), 1, fp);
	fread (&m_fCenterFrequency,     sizeof(float), 1, fp);
	fread (&m_fReceiveBeamWidth,    sizeof(float), 1, fp);
	fread (&m_fTransmitPulseLength, sizeof(float), 1, fp);

	// across track, radians.
	m_pBeamAngles = new float[m_dwReceiveBeams];
	fread (m_pBeamAngles, sizeof(float), m_dwReceiveBeams, fp);

	float DepthMin, DepthMax;

	fread (&DepthMin, sizeof(float), 1, fp);
	fread (&DepthMax, sizeof(float), 1, fp);

	m_pRAWData = new char[sizeof(short)*2*m_dwReceiveBeams*m_iOutputSamples];

	if (fread (m_pRAWData, 2*sizeof(short), m_dwReceiveBeams*m_iOutputSamples, fp) < m_dwReceiveBeams*m_iOutputSamples)
	{
		printf ("Invalid file format or misformed ping data: \'%s\'\n", filename);
		fclose(fp);
		return false;
	}
	
	fclose(fp);

	m_pGatesMin       = new DWORD[m_dwReceiveBeams];
	m_pGatesMax       = new DWORD[m_dwReceiveBeams];
	m_pMaskMin        = new DWORD[m_dwReceiveBeams];
	m_pMaskMax        = new DWORD[m_dwReceiveBeams];
	m_pMaskWidth      = new DWORD[m_dwReceiveBeams];
	m_pFlagBeams	  = new bool[m_dwReceiveBeams];

	DWORD NbSamplesForPulseLength = max(floordw(m_fTransmitPulseLength*m_fSampleRate),1);
//	int allocSamples = max(m_iOutputSamples / 32 + 1, REDUCTION_MATRIX_SIZE);
	int allocSamples = max(m_iOutputSamples / 32 + 1, ceili(0.0375f*m_iOutputSamples + 190.0f));
	allocSamples = max(allocSamples, m_iOutputSamples/NbSamplesForPulseLength+1);
	m_nMaskSamples = allocSamples;

	m_pAmpReduite     = _2DArrayAlloc(m_dwReceiveBeams, allocSamples, (float)0.0f, true);
	m_pfReduite       = _2DArrayAlloc(m_dwReceiveBeams, allocSamples, (float)0.0f, true);
	m_pR1             = new float[m_dwReceiveBeams];
	m_pR2             = new float[m_dwReceiveBeams];

	m_pRangeAmplitude   = new float[m_dwReceiveBeams];
	m_pSamplesAmp       = new DWORD[m_dwReceiveBeams];
	m_pQualityAmplitude = new float[m_dwReceiveBeams];
	m_pQF2Amplitude		= new float[m_dwReceiveBeams];
	m_pRangePhase		= new float[m_dwReceiveBeams];
	m_pQualityPhase		= new float[m_dwReceiveBeams];
	m_pSamplesPhase     = new DWORD[m_dwReceiveBeams];
	m_pERMPhase			= new float[m_dwReceiveBeams];
	m_pSlopePhase		= new float[m_dwReceiveBeams];
	m_pDetectionRange	= new float[m_dwReceiveBeams];
	m_pDetectionQuality	= new float[m_dwReceiveBeams];
	m_pDetectionType	= new char [m_dwReceiveBeams];
	m_pAmpWinMin		= new DWORD[m_dwReceiveBeams];
	m_pAmpWinMax		= new DWORD[m_dwReceiveBeams];
	m_pPhsWinMin		= new DWORD[m_dwReceiveBeams];
	m_pPhsWinMax		= new DWORD[m_dwReceiveBeams];

	m_pdBeamsA        = new DWORD[m_dwReceiveBeams];
	m_pdBeamsB        = new DWORD[m_dwReceiveBeams];
	m_pdBeamsC        = new DWORD[m_dwReceiveBeams];
	m_pdBeamsD        = new DWORD[m_dwReceiveBeams];

	m_pfBeamsA        = new float[m_dwReceiveBeams];
	m_pfBeamsB        = new float[m_dwReceiveBeams];
	m_pfBeamsC        = new float[m_dwReceiveBeams];
	m_pfBeamsD        = new float[m_dwReceiveBeams];

	m_pNominalBeams	  = new bool[m_dwReceiveBeams];

	if (m_dwDeviceId == 7111)
		_nominal_beams_7111();

	if (m_iOutputSamples > (int)m_nAllocSamples)
	{
		delete [] m_pfSamplesA;
		delete [] m_pfSamplesB;
		delete [] m_pfSamplesC;
		delete [] m_pfSamplesD;
		delete [] m_pfSamplesE;
		delete [] m_pfSamplesF;
		delete [] m_pfdSamples;
		delete [] m_pdSamplesA;
		delete [] m_pdSamplesB;

		if (m_iOutputSamples < REDUCTION_MATRIX_SIZE)
			m_nAllocSamples = REDUCTION_MATRIX_SIZE;
		else
			m_nAllocSamples = m_iOutputSamples;

		m_pfSamplesA = new float[m_nAllocSamples];
		m_pfSamplesB = new float[m_nAllocSamples];
		m_pfSamplesC = new float[m_nAllocSamples];
		m_pfSamplesD = new float[m_nAllocSamples];
		m_pfSamplesE = new float[m_nAllocSamples];
		m_pfSamplesF = new float[m_nAllocSamples];
		m_pfdSamples = new double[m_nAllocSamples];
		m_pfSamplesG = (float *)m_pfdSamples;
		m_pfSamplesH = m_pfSamplesG+m_nAllocSamples;
		m_pdSamplesA = new DWORD[m_nAllocSamples];
		m_pdSamplesB = new DWORD[m_nAllocSamples];
	}

	if (m_pMatrix) delete [] m_pMatrix;
	m_pMatrix         = _2DArrayAlloc(m_nAllocSamples, m_dwReceiveBeams, 0.0f, false);
	memset(m_pMatrix[0], 0xff, m_nAllocSamples*m_dwReceiveBeams*sizeof(float));


	m_NechRes0 = 4 * tanf(m_pBeamAngles[m_dwReceiveBeams-1]) / m_dwReceiveBeams;
	m_dTPulse  = m_fTransmitPulseLength * m_fSampleRate * m_fTransmitPulseLength * m_fSampleRate / 12.0f;
	m_N0	   = m_fTransmitPulseLength * m_fSampleRate;
	m_N0Corr   = m_fTransmitPulseLength * m_fSampleRate * sqrtf(2.0f) / 2.0f;
	m_B		   = 1.04 * sqrt((4.0/PI-1.0) / 12.0);


	switch (m_dwDeviceId)
	{
	case 7111:
		m_fAbsorption = 35.0f;
		m_DeltaTeta = D2Rf(1.8f);
		m_nStrips     = 62; //31;
		break;

	case 7150:
		if (m_fCenterFrequency < 18000)			// 12 kHz
		{
			m_fAbsorption = 1.1f;
            m_DeltaTeta = D2Rf(1.0f);
		}
		else									// 24 kHz
		{
			m_fAbsorption = 4.0f;
            m_DeltaTeta = D2Rf(0.5f);
		}
		m_nStrips     = 81;
		break;

	default:
		return false;
	}

//	m_dwPingNumber = 1;

	for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
	{
		m_pDetectionRange[iB]   = 0.0f;
		m_pDetectionQuality[iB] = 0.0f;
		m_pDetectionType[iB]	= IF1_QUALITY_QF2_PASS;
	}

	for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
	{
		m_pGatesMin[iB] = 0;
		m_pGatesMax[iB] = 0xffffffff;
		m_pMaskMin[iB]  = 0;
		m_pMaskMax[iB]  = 0xffffffff;
	}

	if (DepthMin > 1.0f || DepthMax > 1.0f)
		SetDepthGates(DepthMin, DepthMax, 0.0f);

	QueryPerformanceCounter(&liStart);
	liCounter.QuadPart = 0;

	return true;
}

#else

#include "../7kDataProcess.h"

bool CIFremerBottomDetect::PreloadProcessing (C7kDataProcess * pPO)
{
	int iB;
	bool redoPhase = false;
	bool redoMask  = false;

	m_dwDeviceId = pPO->m_dwDeviceId;

	if (!m_dwDeviceId) return false;
	m_iOutputSamples = pPO->m_iOutputSamples;
	if (m_iOutputSamples < 10) return false;
	

	// across track, radians. 2nd array of angles
	//if (!(pPO->m_b7004Built)) return false;

	m_pBeamAngles = (float *) (pPO->m_pR7004 + sizeof(RECORD_FRAME) + sizeof(R7004) + pPO->m_dwReceiveBeams * sizeof(float));
	
	int NbSamplesForPulseLength = max(floordw(m_fTransmitPulseLength*m_fSampleRate),1);
	int allocSamples = max(m_iOutputSamples / 32 + 1, ceili(0.375f*m_iOutputSamples + 190.0f));
	allocSamples = max(allocSamples, m_iOutputSamples/NbSamplesForPulseLength+1);
	allocSamples = max(allocSamples, m_iOutputSamples/NbSamplesForPulseLength+1);

	if (m_dwReceiveBeams != pPO->m_dwReceiveBeams)
	{
		CleanUpBeam();
		redoPhase = true;

		m_dwReceiveBeams  = pPO->m_dwReceiveBeams;
		m_pGatesMin       = new DWORD[m_dwReceiveBeams];
		m_pGatesMax       = new DWORD[m_dwReceiveBeams];
		m_pMaskMin        = new DWORD[m_dwReceiveBeams];
		m_pMaskMax        = new DWORD[m_dwReceiveBeams];
		m_pMaskWidth      = new DWORD[m_dwReceiveBeams];
		m_pFlagBeams	  = new bool[m_dwReceiveBeams];

		m_pAmpReduite     = _2DArrayAlloc(m_dwReceiveBeams, allocSamples, (float)0.0f, false);
		m_pfReduite       = _2DArrayAlloc(m_dwReceiveBeams, allocSamples, (float)0.0f, false);

		m_nMaskSamples    = allocSamples;

		m_pR1             = new float[m_dwReceiveBeams];
		m_pR2             = new float[m_dwReceiveBeams];

		m_pRangeAmplitude   = new float[m_dwReceiveBeams];
		m_pSamplesAmp       = new DWORD[m_dwReceiveBeams];
		m_pQualityAmplitude = new float[m_dwReceiveBeams];
		m_pQF2Amplitude		= new float[m_dwReceiveBeams];
		m_pRangePhase		= new float[m_dwReceiveBeams];
		m_pQualityPhase		= new float[m_dwReceiveBeams];
		m_pSamplesPhase     = new DWORD[m_dwReceiveBeams];
		m_pERMPhase			= new float[m_dwReceiveBeams];
		m_pSlopePhase		= new float[m_dwReceiveBeams];
		m_pDetectionRange	= new float[m_dwReceiveBeams];
		m_pDetectionQuality	= new float[m_dwReceiveBeams];
		m_pDetectionType	= new char [m_dwReceiveBeams];
		m_pAmpWinMin		= new DWORD[m_dwReceiveBeams];
		m_pAmpWinMax		= new DWORD[m_dwReceiveBeams];
		m_pPhsWinMin		= new DWORD[m_dwReceiveBeams];
		m_pPhsWinMax		= new DWORD[m_dwReceiveBeams];

		m_pdBeamsA        = new DWORD[m_dwReceiveBeams];
		m_pdBeamsB        = new DWORD[m_dwReceiveBeams];
		m_pdBeamsC        = new DWORD[m_dwReceiveBeams];
		m_pdBeamsD        = new DWORD[m_dwReceiveBeams];

		m_pfBeamsA        = new float[m_dwReceiveBeams];
		m_pfBeamsB        = new float[m_dwReceiveBeams];
		m_pfBeamsC        = new float[m_dwReceiveBeams];
		m_pfBeamsD        = new float[m_dwReceiveBeams];


		m_pNominalBeams	  = new bool[m_dwReceiveBeams];
	}
	else if (allocSamples > (int)m_nMaskSamples)
	{
		delete [] m_pAmpReduite;
		delete [] m_pfReduite;
		m_pAmpReduite     = _2DArrayAlloc(m_dwReceiveBeams, allocSamples,          (float)0.0f, false);
		m_pfReduite       = _2DArrayAlloc(m_dwReceiveBeams, allocSamples,          (float)0.0f, false);
		m_nMaskSamples = allocSamples;
	}

	memset(m_pAmpReduite[0], 0, m_nMaskSamples*m_dwReceiveBeams*sizeof(float));
	memset(m_pfReduite[0],   0, m_nMaskSamples*m_dwReceiveBeams*sizeof(float));

// 	float * ar = m_pAmpReduite[0];
// 	float * af = m_pfReduite[0];
// 	for (iB = 0; iB < m_dwReceiveBeams*m_nMaskSamples; iB++) // This is a cumulative array. It must be reset to zero.
// 		*af++ = *ar++ = 0.0f;
	
	if (m_dwDeviceId == 7111)
		_nominal_beams_7111();

	if (m_iOutputSamples > (int)m_nAllocSamples)
	{
		delete [] m_pfSamplesA;
		delete [] m_pfSamplesB;
		delete [] m_pfSamplesC;
		delete [] m_pfSamplesD;
		delete [] m_pfSamplesE;
		delete [] m_pfSamplesF;
		delete [] m_pfdSamples;
		delete [] m_pdSamplesA;
		delete [] m_pdSamplesB;

		if (m_iOutputSamples < REDUCTION_MATRIX_SIZE)
			m_nAllocSamples = REDUCTION_MATRIX_SIZE;
		else
			m_nAllocSamples = m_iOutputSamples;

		redoPhase = true;

		m_pfSamplesA = new float[m_nAllocSamples];
		m_pfSamplesB = new float[m_nAllocSamples];
		m_pfSamplesC = new float[m_nAllocSamples];
		m_pfSamplesD = new float[m_nAllocSamples];
		m_pfSamplesE = new float[m_nAllocSamples];
		m_pfSamplesF = new float[m_nAllocSamples];
		m_pfdSamples = new double[m_nAllocSamples];
		m_pfSamplesG = (float *)m_pfdSamples;
		m_pfSamplesH = m_pfSamplesG+m_nAllocSamples;
		m_pdSamplesA = new DWORD[m_nAllocSamples];
		m_pdSamplesB = new DWORD[m_nAllocSamples];
	}

	if (redoPhase)
	{
		delete [] m_pMatrix;
		m_pMatrix          = _2DArrayAlloc(m_nAllocSamples, m_dwReceiveBeams, 0.0f, false);
	}

	memset(m_pMatrix[0], 0xff, m_nAllocSamples*m_dwReceiveBeams*sizeof(float));

	for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
	{
		m_pGatesMin[iB] = 1;
		m_pGatesMax[iB] = 0xffffffff;
		m_pMaskMin[iB]  = 1;
		m_pMaskMax[iB]  = 0xffffffff;
	}

	m_pRAWData = pPO->m_pRAW;

	m_fSoundVelocity = pPO->m_fSoundVelocity;
	m_fSampleRate = pPO->m_fSampleRate;
	m_fCenterFrequency = pPO->m_fCenterFrequency;

	m_NechRes0 = 4 * tanf(m_pBeamAngles[m_dwReceiveBeams-1]) / m_dwReceiveBeams;
	m_dTPulse  = m_fTransmitPulseLength * m_fSampleRate * m_fTransmitPulseLength * m_fSampleRate / 12.0f;
	m_N0	   = m_fTransmitPulseLength * m_fSampleRate;
	m_N0Corr   = m_fTransmitPulseLength * m_fSampleRate * sqrtf(2.0f) / 2.0f;
	m_B		   = 1.04 * sqrt((4.0/PI-1.0) / 12.0);

	switch (m_dwDeviceId)
	{
	case 7111:
		m_fAbsorption = 35.0f;
		m_DeltaTeta   = D2Rf(1.8f);
		m_nStrips     = 62; //31;
		break;

	case 7150:
		if (m_fCenterFrequency < 18000)			// 12 kHz
		{
			m_fAbsorption = 1.1f;
            m_DeltaTeta = D2Rf(1.0f);
		}
		else									// 24 kHz
		{
			m_fAbsorption = 4.0f;
            m_DeltaTeta = D2Rf(0.5f);
		}
		m_nStrips     = 81;
		break;

	default:
		return false;
	}

	m_fReceiveBeamWidth = pPO->m_fUniformReceiveBeamWidthAcross[0];

	if (m_dwDeviceId == 7150 && pPO->m_dwReceiveElements == 144)	// Single Rack
		m_fReceiveBeamWidth *= 2;

	m_fTransmitPulseLength = pPO->m_fTransmitPulseLength;
	m_dwPingNumber = pPO->m_dwPingNumber;

	for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
	{
		m_pDetectionRange[iB]   = 0.0f;
		m_pDetectionQuality[iB] = 0.0f;
		m_pDetectionType[iB]	= IF1_QUALITY_QF2_PASS;
	}

	return true;
}

#endif

#define CLEAN(px)	delete [] px; px = NULL;

void CIFremerBottomDetect::CleanUpBeam()
{
	CLEAN(m_pGatesMin);
	CLEAN(m_pGatesMax);
	CLEAN(m_pAmpReduite);
	CLEAN(m_pfReduite);
	CLEAN(m_pR1);
	CLEAN(m_pR2);
	CLEAN(m_pMaskMin);
	CLEAN(m_pMaskMax);
	CLEAN(m_pMaskWidth);
	CLEAN(m_pFlagBeams);
	CLEAN(m_pRangeAmplitude);
	CLEAN(m_pSamplesAmp);
	CLEAN(m_pQualityAmplitude);
	CLEAN(m_pQF2Amplitude);
	CLEAN(m_pRangePhase);
	CLEAN(m_pQualityPhase);
	CLEAN(m_pSamplesPhase);
	CLEAN(m_pERMPhase);
	CLEAN(m_pSlopePhase);
	CLEAN(m_pDetectionRange);
	CLEAN(m_pDetectionQuality);
	CLEAN(m_pDetectionType);

	CLEAN(m_pAmpWinMin);
	CLEAN(m_pAmpWinMax);
	CLEAN(m_pPhsWinMin);
	CLEAN(m_pPhsWinMax);

	CLEAN(m_pdBeamsA);
	CLEAN(m_pdBeamsB);
	CLEAN(m_pdBeamsC);
	CLEAN(m_pdBeamsD);
	CLEAN(m_pfBeamsA);
	CLEAN(m_pfBeamsB);
	CLEAN(m_pfBeamsC);
	CLEAN(m_pfBeamsD);

	CLEAN(m_pNominalBeams);
	
	m_dwReceiveBeams = 0;
}

void CIFremerBottomDetect::CleanUpAll()
{
	CleanUpBeam();

	CLEAN(m_pfSamplesA);
	CLEAN(m_pfSamplesB);
	CLEAN(m_pfSamplesC);
	CLEAN(m_pfSamplesD);
	CLEAN(m_pfSamplesE);
	CLEAN(m_pfSamplesF);
	CLEAN(m_pfdSamples);
	CLEAN(m_pdSamplesA);
	CLEAN(m_pdSamplesB);

	CLEAN(m_pMatrix);

	m_nAllocSamples = 0;
	m_nMaskSamples  = 0;
	m_dwDeviceId    = 0;
}

void CIFremerBottomDetect::SetDepthGates (float minDepth, float maxDepth, float roll)
{
//	printf ("Ping #%d: Setting Depth Gates [%f %f]\n", m_dwPingNumber, minDepth, maxDepth);

	register int gateMin, gateMax;

	for (register int si=0; si < (int)m_dwReceiveBeams; si++)
	{
		gateMin = floori(D2SR(minDepth,si,roll));
		gateMax = ceili(D2SR(maxDepth,si,roll));
		if (m_pGatesMin[si] < (DWORD)gateMin) m_pGatesMin[si] = max(gateMin,5);
		if (m_pGatesMax[si] > (DWORD)gateMax) m_pGatesMax[si] = gateMax;
		if (m_pGatesMax[si] < m_pGatesMin[si]) m_pGatesMax[si] = m_pGatesMin[si] = 0;
	}
}

void CIFremerBottomDetect::SetRangeGates (float minRange, float maxRange)
{
//	printf ("Ping #%d: Setting Range Gates [%f %f]\n", m_dwPingNumber, minRange, maxRange);
	
	register int gateMin = (int)R2S(minRange);
	register int gateMax = (int)R2S(maxRange);

	for (register int si=0; si<(int)m_dwReceiveBeams; si++)
	{
		if (m_pGatesMin[si] < (DWORD)gateMin) m_pGatesMin[si] = max((DWORD)gateMin,5);
		if (m_pGatesMax[si] > (DWORD)gateMax) m_pGatesMax[si] = (DWORD)gateMax;
	}
}

bool CIFremerBottomDetect::BottomDetection (bdresults * BDResults, int &BeamNumber)
{
	_amplitude_detection();

	if (m_Debug&(DEBUG_OUTPUT_DETECTION_AMP|DEBUG_OUTPUT_FILTER_PHS|DEBUG_OUTPUT_MASKPHS_SHORT|DEBUG_OUTPUT_DETECTION_PHS))
		SaveState("ampdetection");

	_phase_filtering();

	if (m_Debug&(DEBUG_OUTPUT_FILTER_PHS|DEBUG_OUTPUT_MASKPHS_SHORT))
		SaveState("phsfiltering");
	
	_phase_mask();

	if (m_Debug&DEBUG_OUTPUT_MASKPHS_SHORT)
		SaveState("phsmaskfinal");
	
	_phase_detection();

	if (m_Debug&DEBUG_OUTPUT_DETECTION_PHS)
		SaveState("phsdetection");
	
	if (m_Debug&DEBUG_OUTPUT_SYNTHESIS)
		memcpy(m_pfBeamsA, m_pRangeAmplitude, sizeof(float)*m_dwReceiveBeams);

	_detection_synthese();

	if (m_Debug&DEBUG_OUTPUT_SYNTHESIS)
		SaveState("synthesis");

// 	LARGE_INTEGER liTic, liFreq;
// 
// 	QueryPerformanceCounter(&liTic);
// 	QueryPerformanceFrequency(&liFreq);
// 	liCounter.QuadPart += liTic.QuadPart - liStart.QuadPart;
// 	double dCounts = (double)liCounter.QuadPart; 
// 	double dFreq   = (double)liFreq.QuadPart;
// 	double dSecs   = dCounts / dFreq;
// 	printf("Execution time = %.0fms\n", dSecs * 1000.0);
	float qThreshold;

	if (m_dwDeviceId == 7150)
		qThreshold = 2.5f;
	else if (m_dwDeviceId == 7111)
		qThreshold = 2.0f;
	else
		qThreshold = 0.0f;

#if !defined(RUN_STAND_ALONE)

	int iB;
	BeamNumber = 0;
	DWORD *   RangeNaN    = (DWORD*) m_pDetectionRange;
	
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (m_pDetectionType[iB] && !ISNAN(RangeNaN[iB]))
		{
			BDResults[BeamNumber].fBD_Sample	= m_pDetectionRange[iB];	    // raw bottom detect result (fractional sample number)
			BDResults[BeamNumber].fAmplitude;    // Amplitude @ detection sample
																				// bottom detect quality/process flag
			BDResults[BeamNumber].ucQuality     = (m_pDetectionType[iB] & ~0x3) | ( (m_pDetectionQuality[iB] > qThreshold) ? IF1_QUALITY_QF1_PASS|IF1_QUALITY_QF2_PASS : 0x0 );
			BDResults[BeamNumber].fQuality      = m_pDetectionQuality[iB];		// IFremer floating point quality values
			BDResults[BeamNumber].iBeamNumber   = iB;							// beam for this bottom detect point
			BDResults[BeamNumber].Hmin			   = m_pMaskMin[iB];    // min and max SAMPLE number for gate
			BDResults[BeamNumber].Hmax			   = m_pMaskMax[iB];
			BDResults[BeamNumber].minWin		   = (m_pDetectionType[iB] & IF1_DETECTION_AMP) ? m_pAmpWinMin[iB] : (m_pDetectionType[iB] & IF1_DETECTION_PHASE) ? m_pPhsWinMin[iB] : 0;
			BDResults[BeamNumber].maxWin		   = (m_pDetectionType[iB] & IF1_DETECTION_AMP) ? m_pAmpWinMax[iB] : (m_pDetectionType[iB] & IF1_DETECTION_PHASE) ? m_pPhsWinMax[iB] : 0;;  // min and max SAMPLE number of detection window
			BDResults[BeamNumber].dCorrelation	= 0.0;  // phase fit correlation
			BDResults[BeamNumber].uncertainty	= 0.0f;   // relative uncertainty

			BeamNumber++;
		}
	}

#endif

	// EPM adding saving of results for Jean-Marie to analyze, begin
	/*
	char fina[256] ;
	sprintf(fina,"results_ping_%08d.dat",(int)m_dwPingNumber) ;
	FILE *fp;
	fopen(&fp,fina,"w") ;
	for (DWORD i=0; i<m_dwReceiveBeams; i++)
		fprintf(fp,"%d %d %f\n",m_pMaskMin[i], m_pMaskMax[i],m_pfBeamsD[i]) ;
	fclose(fp) ;
	*/
	// EPM adding saving of results for Jean-Marie to analyze, end

	return true;
}


bool CIFremerBottomDetect::_detection_synthese ( )
{
	int iB;
	DWORD *AmpRangeNaN    = (DWORD*) m_pRangeAmplitude;
	DWORD *PhsRangeNaN    = (DWORD*) m_pRangePhase;
	DWORD *   RangeNaN    = (DWORD*) m_pDetectionRange;
	
	if (ISNAN(AmpRangeNaN[m_IncidentBeam]))		// Safety
		return false;

	// Remove detections outside gates...

	for (iB = 0; iB < (int) m_dwReceiveBeams; iB++)
	{
		if (!ISNAN(AmpRangeNaN[iB]) && m_pRangeAmplitude[iB] > -0.5f && !between(m_pRangeAmplitude[iB], m_pGatesMin[iB], m_pGatesMax[iB])) {
			NAN(AmpRangeNaN[iB]);
			m_pSamplesAmp[iB]       = 0;
			m_pQualityAmplitude[iB] = 0.0f;
			m_pQF2Amplitude[iB]     = 0.0f;
		}

		if (!ISNAN(PhsRangeNaN[iB]) && !between(m_pRangePhase[iB], m_pGatesMin[iB], m_pGatesMax[iB])) {
			NAN(PhsRangeNaN[iB]);
			NANf(m_pQualityPhase[iB]);
			NANf(m_pERMPhase[iB]);
			NANf(m_pSlopePhase[iB]);
			m_pSamplesPhase[iB]   = 0;
		}
	}


	float NbSamplesForPulseLength = ceilf(m_fTransmitPulseLength * m_fSampleRate);
	float ThauOn2 = NbSamplesForPulseLength / 2.0f;
	float Ang     = acosf((m_pRangeAmplitude[m_IncidentBeam] - ThauOn2) / (m_pRangeAmplitude[m_IncidentBeam] + ThauOn2));
	float QF_threshold = 2.1f;

// 	float MinRange = (float)m_iOutputSamples;
// 	int   iBeam0   = -1;
// 
// 	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
// 	{
// 		if (!ISNAN(AmpRangeNaN[iB]) && m_pBeamAngles[iB] > m_pBeamAngles[m_IncidentBeam] - Ang && m_pBeamAngles[iB] < m_pBeamAngles[m_IncidentBeam] + Ang
// 			&& MinRange > m_pRangeAmplitude[iB])
// 		{
// 			MinRange = m_pRangeAmplitude[iB];
// 			iBeam0   = iB;
// 		}
// 
// 		NAN(RangeNaN[iB]);					// Clean up
// 	}
// 
// 	if (iBeam0 == -1)						// Safety
// 		return false;
// 
// 	Ang = acosf((MinRange - ThauOn2) / (MinRange + ThauOn2));
// 	MinRange += NbSamplesForPulseLength;

// 	bool * subAmpTrouve = m_pFlagBeams;
// 
// 	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
// 	{
// //		if (!ISNAN(AmpRangeNaN[iB]) && (m_pBeamAngles[iB] < m_pBeamAngles[iBeam0] - Ang || m_pBeamAngles[iB] > m_pBeamAngles[iBeam0] + Ang)
// //			&& m_pRangeAmplitude[iB] < MinRange)
// //			NAN(AmpRangeNaN[iB]);
// 
// 		if (!ISNAN(AmpRangeNaN[iB]) /*&& m_pQF2Amplitude[iB] >= 1*/)
// 			subAmpTrouve[iB] = true;
// 		else
// 			subAmpTrouve[iB] = false;
// 	}

	int PhasePingPenteSeuil = 36;
	int PhasePingEqmSeuil   = 45;

	bool * subPhase = m_pNominalBeams;

	for (iB = 0; iB < (int) m_dwReceiveBeams; iB++)
	{
		subPhase[iB] = !ISNAN(AmpRangeNaN[iB]) && !ISNAN(PhsRangeNaN[iB]) && (m_pQualityPhase[iB] > 2.0f) && (m_pSlopePhase[iB] <= PhasePingPenteSeuil) && (m_pERMPhase[iB] <= PhasePingEqmSeuil);
		if ((m_pDetectionType[iB] & IF1_QUALITY_QF2_PASS) == 0)
			subPhase[iB] &= (m_pQualityPhase[iB] > QF_threshold);
	}

	// Suppression of isolated phase detections
	for (iB = 1; iB < (int) m_dwReceiveBeams-1; iB++)
		if (!subPhase[iB-1] && subPhase[iB] && !subPhase[iB+1])
			subPhase[iB] = false;

	// Suppression of 2 isolated phase detections
	for (iB = 0; iB < (int) m_dwReceiveBeams-3; iB++)
		if (!subPhase[iB] && subPhase[iB+1] && subPhase[iB+2] && !subPhase[iB+3])
			subPhase[iB+1] = subPhase[iB+2] = false;


	for (iB = 0; iB < (int) m_dwReceiveBeams; iB++)
	{
		if (subPhase[iB])
		{
			m_pDetectionRange[iB]   = m_pRangePhase[iB];
			m_pDetectionQuality[iB] = m_pQualityPhase[iB];
			m_pDetectionType[iB]   |= IF1_DETECTION_PHASE;
		}
		else if (!ISNAN(AmpRangeNaN[iB]) && m_pRangeAmplitude[iB] > QF_threshold && m_pQualityAmplitude[iB] >= QF_threshold /* && m_pQF2Amplitude[iB] >= 1.4*/)	// On compl�te avec l'amplitude
		{
			m_pDetectionRange[iB]   = m_pRangeAmplitude[iB];
			m_pDetectionQuality[iB] = m_pQualityAmplitude[iB];
			m_pDetectionType[iB]   |= IF1_DETECTION_AMP;
		}
		else if (!ISNAN(AmpRangeNaN[iB]) && !ISNAN(PhsRangeNaN[iB]) && m_pQualityPhase[iB] >= QF_threshold && m_pSlopePhase[iB] <= PhasePingPenteSeuil)
		{									// On recomplete avec la detection de phase sans la contrainte sur l'EQM
			m_pDetectionRange[iB]   = m_pRangePhase[iB];
			m_pDetectionQuality[iB] = m_pQualityPhase[iB];
			m_pDetectionType[iB]   |= IF1_DETECTION_PHASE;
		}
		else
		{
			m_pDetectionType[iB]    = 0;
		}
	}


	
	// !ALTRAN - 27  Begin
	/*bool eot = false;
	while (!eot) {
		eot = true;

		for (iB = 1; iB < (int) m_dwReceiveBeams-1; iB++) {
			if ((m_pDetectionType[iB]&IF1_DETECTION_AMP) && !ISNAN(PhsRangeNaN[iB]) && fabs(m_pRangeAmplitude[iB] - m_pRangePhase[iB]) > 20.0f 
				&& ((m_pDetectionType[iB-1]&IF1_DETECTION_PHASE) || (m_pDetectionType[iB+1]&IF1_DETECTION_PHASE))) {
				m_pDetectionType[iB]   &= ~IF1_DETECTION_AMP;
				m_pDetectionType[iB]   |= IF1_DETECTION_PHASE;
				m_pDetectionRange[iB]   = m_pRangePhase[iB];
				m_pDetectionQuality[iB] = m_pQualityPhase[iB];
				eot = false;
			}
		}
	}*/
	// !ALTRAN end



	return true;
}

bool CIFremerBottomDetect::MasquePhs(int beam, int sample) const
{
	DWORD *Mask = (DWORD *)m_pfReduite[0];
	
	return (Mask[beam*m_nMaskSamples+(sample >> 5)] & (1 << (sample & 0x1f))) ? true : false;
}

void CIFremerBottomDetect::MasquePhs(int beam, int sample, bool value)
{
	DWORD *Mask = (DWORD *)m_pfReduite[0];

	if (sample == -1) {
		char mask0 = (value) ? 0xff : 0;
		memset (Mask + beam*m_nMaskSamples, mask0, m_nMaskSamples * sizeof(DWORD));
	} else {
		if (value)
			Mask[beam*m_nMaskSamples+(sample >> 5)] |= 1 << (sample & 0x1f);
		else
			Mask[beam*m_nMaskSamples+(sample >> 5)] &= ~(1 << (sample & 0x1f));
	}
}

void CIFremerBottomDetect::MasquePhs(bool value)
{
	DWORD mask = (value) ? 0xff : 0;
	memset(m_pfReduite[0], mask, sizeof(float) * m_nMaskSamples * m_dwReceiveBeams);
}

void CIFremerBottomDetect::MasquePhs(int beam, int sample1, int sample2, bool value)
{
	DWORD *Mask = (DWORD *)m_pfReduite[0];

	DWORD mask1 = 0xffffffff;
	mask1 >>= (sample1 & 0x1f);
	mask1 <<= (sample1 & 0x1f);
	
	DWORD mask2 = 0xffffffff;
	mask2 <<= (sample2 & 0x1f);
	mask2 >>= (sample2 & 0x1f);

	char mask0 = (value) ? 0xff : 0;

	if ((sample1 >> 5) == (sample2 >> 5))
	{
		mask1 = mask1 & mask2;

		if (value)
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] |= mask1;
		else
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] &= ~mask1;
	}
	else
	{
		if (value)
		{
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] |= mask1;
			Mask[beam*m_nMaskSamples+(sample2 >> 5)] |= mask2;
		}
		else
		{
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] &= ~mask1;
			Mask[beam*m_nMaskSamples+(sample2 >> 5)] &= ~mask2;
		}
		
		memset (Mask + beam*m_nMaskSamples + (sample1 >> 5) + 1, mask0, ((sample2 >> 5) - (sample1 >> 5) - 1) * sizeof(DWORD));
	}
}

bool CIFremerBottomDetect::MasqueAmp(int beam, int sample) const
{
	DWORD *Mask = (DWORD *)m_pAmpReduite[0];
	
	return (Mask[beam*m_nMaskSamples+(sample >> 5)] & (1 << (sample & 0x1f))) ? true : false;
}

void CIFremerBottomDetect::MasqueAmp(void)
{
	DWORD *MaskFrom = (DWORD *)m_pMatrix[0];
	DWORD *MaskTo   = (DWORD *)m_pAmpReduite[0];
	DWORD  mask;

	int iB, iS, jS, nj;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		for (iS = 0; iS < m_iOutputSamples; iS += 32)
		{
			nj = min (32, m_iOutputSamples - iS);

			mask = 0;

			for (jS = 0; jS < nj; jS++)
			{
				if (!ISNAN(MaskFrom[(iS+jS)*m_dwReceiveBeams+iB]))
					mask |= 1 << jS;
			}

			MaskTo[iB*m_nMaskSamples+(iS >> 5)] = mask;
		}
	}
}

#ifndef MATLAB_OUTPUT
void  CIFremerBottomDetect::SaveState(char *)
{
}
void  CIFremerBottomDetect::DestroyState ()
{
}
#endif