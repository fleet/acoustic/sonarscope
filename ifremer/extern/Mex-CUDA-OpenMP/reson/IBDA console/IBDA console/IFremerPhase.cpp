/***************************************************************************
****************************************************************************

   FILE: IFremerPhaseMask.cpp

   Implementation of phase mask functions for CIFremerBottomDetect class.

   Copyright � 2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    March 2008
   Author:  Yevgeniy Shafirovich
   Change:  Initial implementation

****************************************************************************
***************************************************************************/

#include "stdafx.h"
#include "IFremerBottomDetect.h"
#include <float.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//------------------------------------------------------------------------------
// Preparation of phase.
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData;
// [INPUT]  m_MaskMin,  m_MaskMax
// [INPUT]  m_pMatrix (on Amplitude)
// [OUTPUT] m_pMatrix (on Phase)
//
// [TEMP] m_pfSamplesA   === aF
// [TEMP] m_pfSamplesB   === bF
//
bool CIFremerBottomDetect::_phase_filtering( void )
{
	int iB, iS;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	DWORD *Mask = (DWORD *)m_pMatrix[0];
	DWORD * R1NaN = (DWORD *) m_pR1;
	DWORD * aFNaN = (DWORD *) m_pfSamplesA;
	DWORD * bFNaN = (DWORD *) m_pfSamplesB;
	int iDeb, iFin, iBeg, iEnd;
	int n = 4;
	bool wasNaN;

	MasqueAmp();		// Preserve Amplitude Masque

	m_MaskMin = m_iOutputSamples;
	m_MaskMax = 0;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (ISNAN(R1NaN[iB]))
		{
			for (iS = 0; iS < m_iOutputSamples; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
			}
		}
		else
		{
			iDeb = max(floori(m_pR1[iB]) - 2*(int)m_pMaskWidth[iB], n);
			iFin = min(floori(m_pR1[iB]) + 2*(int)m_pMaskWidth[iB] + 1, m_iOutputSamples-n);

			iBeg = iDeb - n;
			iEnd = iFin + n;

			for (iS = 0; iS < iBeg; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
			}

			for (      ; iS < iDeb; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}

			for (      ; iS < iFin; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}
			
			for (      ; iS < iEnd; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}

			for (      ; iS < m_iOutputSamples; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}

// Something in here does not compute <RESON>
			

			for (iS = iDeb; iS < iFin; iS++)
			{
				wasNaN = ISNAN(aFNaN[iS]) || ISNAN(bFNaN[iS]);
				m_pfSamplesA[iS] = _nanmean(m_pfSamplesA+iS-n, n*2+1);
				m_pfSamplesB[iS] = _nanmean(m_pfSamplesB+iS-n, n*2+1);

				if (wasNaN)
					NAN(Mask[iS*m_dwReceiveBeams+iB]);
				else
					m_pMatrix[iS][iB] = R2Df(atan2f(m_pfSamplesB[iS], m_pfSamplesA[iS]));
			}
		}
	}

	return true;
}


//------------------------------------------------------------------------------
// Determination of a mask on phase
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [INPUT]  m_pMaskMin,  m_pMaskMax
// [OUTPUT] m_pMatrix  (Phase Mask)
//
// [TEMP]   m_pdBeamsA  === subSample
// [TEMP]   m_pfBeamsA  === amp
//
// [MATLAB] Ph (input)  === m_pMatrix (input)
// [MATLAB] Amp         === m_pRAWData
// [MATLAB] R0          === m_R0
// [MATLAB] iBeam0      === m_IncidentBeam
// [MATLAB] RMax1       === m_pR1
// [MATLAB] Masque      === Masque()
// [MATLAB] Ph (output) === m_pMatrix (output)
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonMasquePhase_V5
//

bool CIFremerBottomDetect::_phase_mask( void )
{
	int iB, iS, nB;
	float * phase = m_pMatrix[0];
	DWORD * Mask  = (DWORD *)m_pMatrix[0];
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;

	float R1Min = (float)m_iOutputSamples;
	DWORD * R1NaN = (DWORD *) m_pR1;

//%	Masque = repmat(false, size(Ph));
	MasquePhs(false);

//%	R0 = max(R0, min(RMax1));
//%	R0 = floor(R0 * 1.02);
// !ALTRAN - 19 Begin
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++){
		if (!ISNAN(R1NaN[iB]) && R1Min > m_pR1[iB]){
			R1Min = m_pR1[iB];
		}
	}
// !ALTRAN end

	R1Min  = max(m_R0, R1Min);
// !ALTRAN - 20 Begin
	int R0 = floori(R1Min * 1.02f);
// !ALTRAN end

	for (iS = R0; iS < m_iOutputSamples; iS++)
	{

//%	subSample = find(~isnan(Ph(iSample,subBab)));
		for (iB = nB = 0; iB <= m_IncidentBeam; iB++)
		{
			if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
			{
				m_pdBeamsA[nB++] = iB;
			}
			m_pfBeamsA[iB] = data[iS*m_dwReceiveBeams+iB].amp;
		}

		_phase_mask_line(iS, m_pfBeamsA, m_pdBeamsA, nB);			

//%	subSample = find(~isnan(Ph(iSample,subTri)));
		for (iB = m_IncidentBeam+1, nB = 0; iB < (int)m_dwReceiveBeams; iB++)
		{
			if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
			{
				m_pdBeamsA[nB++] = iB;
			}
			m_pfBeamsA[iB] = data[iS*m_dwReceiveBeams+iB].amp;
		}
			
		_phase_mask_line(iS, m_pfBeamsA, m_pdBeamsA, nB);		
	}


	bool allNaN;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (iB == 29)
		{
			int aaaa = 1;
		}

		allNaN = true;

		for (nB = max(0, iB-10); nB < min((int)m_dwReceiveBeams, iB+11); nB++)
			if (!ISNAN(R1NaN[nB]))
			{
				allNaN = false;
				break;
			}
		
		if (allNaN || (!ISNAN(R1NaN[iB]) && floori(m_pR1[iB]) <= R0))
		{
			MasquePhs(iB, false);
		}
	}

	
	_phase_clean_mask();

	return true;
}


//------------------------------------------------------------------------------
// Determination of a mask on phase - single sample
//
// Return Value: none
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [INPUT]  m_pMaskMin,  m_pMaskMax
// [OUTPUT] m_pMatrix  (Phase Mask)
//
// [TEMP]   m_pfBeamsB
// [TEMP]   m_pdBeamsD === masque
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_phase_mask
//
// MatLab reference:
//		ResonMasquePhase_V5(20:117)
//		masquePh2
//		masquePh3
//
void CIFremerBottomDetect::_phase_mask_line ( int sample, float * amp, DWORD * subBeams, int subLength  )
{
	int iS, iB;
	DWORD *NaNPhase  = (DWORD *)m_pMatrix[sample];
	float *Phase = m_pMatrix[sample];
	bool * mask = (bool *)m_pdBeamsD;
	memset (mask, 0, sizeof(bool)*m_dwReceiveBeams);

	subBeams[subLength] = m_dwReceiveBeams*2;

	float Sigma, indiceR;
	int iBeg, iEnd, iMax;
	int sBeg = 0, sEnd, nP;
	bool flagFirstOrLastBeam;
	float PhaseSeuil;
	bool allMinus, allPlus;

	if (sample == 7505)
	{
		int aaaa = 1;
	}

	for (iS = 0; iS < subLength; iS++)
	{
		if (subBeams[iS+1] - subBeams[iS] > 1)
		{
			sEnd = iS+1;
			nP = sEnd - sBeg;
			allPlus = true;
			for (iB = subBeams[sBeg]; iB <= (int)subBeams[sEnd-1]; iB++)
			{
				// !ALTRAN - 38 Begin
				//allPlus &= MasqueAmp(iB, sample);
				if((amp[iB] == amp[iB]) == false)
				{
					allPlus = false;
				}
				// !ALTRAN End
			}

			if (nP <= 5)
			{
				for (iB=sBeg; iB<sEnd; iB++)
				{
					mask[iB] = true;
				}
			}
			else if (allPlus)
			{
				indiceR = _barycentre(amp+subBeams[sBeg], 1, nP, Sigma, m_pfBeamsB) + sBeg - 1;
				// !ALTRAN - 36 Begin
				if(indiceR >= 0)
				{
				// !ALTRAN End

					iMax = floori(indiceR);

					if (Sigma > 2.0f)
					{
						iBeg = floori(iMax - 2*Sigma);
						iEnd = floori(iMax + 2*Sigma + 1);
						
						if (iBeg < sBeg)
						{
							iEnd = sBeg+floori(4*Sigma);
							iBeg = sBeg;
							flagFirstOrLastBeam = true;
						}
						else if (iEnd >= sEnd)
						{
							iBeg = iEnd-sEnd-1+sBeg;
							iEnd = sEnd;
							flagFirstOrLastBeam = true;
						}
						else
							flagFirstOrLastBeam = false;
						
						iBeg = max(iBeg, sBeg);
						iEnd = min(iEnd, sEnd);
						
						PhaseSeuil = 80.0f;

						for (iB = sBeg; iB < iBeg; iB++) NAN(NaNPhase[subBeams[iB]]);
						for (iB = iEnd; iB < sEnd; iB++) NAN(NaNPhase[subBeams[iB]]);
					}
					else
					{
						iBeg = sBeg;
						iEnd = sEnd;
						flagFirstOrLastBeam = true;
						PhaseSeuil = 160.0f;
					}

					for (iB=iMax; iB < iEnd-1; iB++)
					{
						if (fabsf(Phase[subBeams[iB+1]]-Phase[subBeams[iB]]) < PhaseSeuil)
						{
							mask[iB]   = true;
							mask[iB+1] = true;
						}
						else
							break;
					}

					for (iB=iMax-1; iB >= iBeg; iB--)
					{
						if (fabsf(Phase[subBeams[iB+1]]-Phase[subBeams[iB]]) < PhaseSeuil)
						{
							mask[iB]   = true;
							mask[iB+1] = true;
						}
						else
							break;
					}
						
					if (Sigma > 2.0f)
					{
						for (iB=iBeg; iB<iEnd; iB++)
						{
							if (fabsf(Phase[subBeams[iB]]) > 150.0f)
							{
								NAN(NaNPhase[subBeams[iB]]);
								mask[iB] = false;
							}
						}
					}

					if (!flagFirstOrLastBeam)
					{
						allMinus = allPlus = true;

						for (iB=iBeg; iB<iEnd; iB++)
						{
							if (!mask[iB])             continue;
							if (Phase[subBeams[iB]] == 0.0f) 
							{ allMinus = false; allPlus  = false; break; }
							else if (Phase[subBeams[iB]] > 0) allMinus = false;
							else                              allPlus  = false;
						}

						if (allPlus || allMinus)
							for (iB=iBeg; iB<iEnd; iB++)
							{
								mask[iB] = false;
							}
					}
				// !ALTRAN - 36 Begin
				}
				// !ALTRAN End
			}	

			sBeg = sEnd;

		}	// subBeams[iS+1] - subBeams[iS] > 1
	}		// for

	for (iS = 0; iS < subLength; iS++)
		if (mask[iS])
			MasquePhs(subBeams[iS], sample, true);
}


//------------------------------------------------------------------------------
// Final cleaning of the phase mask
//
// Return Value: none
//
// [INPUT]  MasquePhs()
// [OUTPUT] m_pMatrix  (Phase Mask)
// [OUTPUT] MasquePhs()
//
// Used in functions:
//		_phase_mask
//
// MatLab reference:
//		ResonMasquePhase_V5->cleanMasque
//
void CIFremerBottomDetect::_phase_clean_mask ( void )
{
	DWORD *NaNPhase  = (DWORD *)m_pMatrix[0];
	int iB, iS, nS, jS;
	int iBeg;
	
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
//%	sub = find(mIn(:,iBeam));
//%	n = min(5,ceil(length(sub) / 20));
		for (iS = nS = 0; iS < m_iOutputSamples; iS++)
			if (MasquePhs(iB,iS)) nS++;


		nS = min(5, ceili((float)nS/20.0f));

//% mOut(:, iBeam) = imopen(mOut(:, iBeam), ones(n,1));
		for (iS = iBeg = 0; iS < m_iOutputSamples; iS++)
		{
			if (!MasquePhs(iB,iS))
			{
				if (iS - iBeg < nS)
				{
					for (jS = iBeg; jS <= iS; jS++)
					{
						NAN(NaNPhase[jS*m_dwReceiveBeams+iB]);
						MasquePhs(iB, jS, false);
					}
				}
				else
				{
					NAN(NaNPhase[iS*m_dwReceiveBeams+iB]);
				}

				iBeg = iS+1;
			}
		}
		
		if (iBeg < m_iOutputSamples)
		{
			if (m_iOutputSamples - iBeg < ceili((float)nS/2.0f))
			{
				for (jS = iBeg; jS < m_iOutputSamples; jS++)
				{
					NAN(NaNPhase[jS*m_dwReceiveBeams+iB]);
					MasquePhs(iB, jS, false);
				}
			}
		}
	}
}


//------------------------------------------------------------------------------
// Phase Detection
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [OUTPUT] m_pRangePhase
// [OUTPUT] m_pQualityPhase
// [OUTPUT] m_pSamplesPhase
// [OUTPUT] m_pERMPhase
// [OUTPUT] m_pSlopePhase
//
// [MATLAB] Amp                === m_pRAWData
// [MATLAB] Ph                 === m_pMatrix
// [MATLAB] BeamAngles         === m_pBeamAngles
// [MATLAB] R0                 === m_R0
// [MATLAB] SoundVelocity      === not used
// [MATLAB] SampleRate         === not used
// [MATLAB] Frequency          === (m_DeltaTeta)
// [MATLAB] SystemSerialNumber === (m_DeltaTeta)
// [MATLAB] PhasePingRange     === m_pRangePhase
// [MATLAB] PhasePingNbSamples === m_pSamplesPhase
// [MATLAB] PhasePingPente     === m_pSlopePhase
// [MATLAB] PhasePingEqm       === m_pERMPhase
// [MATLAB] PhasePingQF        === m_pQualityPhase
//
// [TEMP] m_pfSamplesA   = phase
// [TEMP] m_pfSamplesB   = amplitude
// [TEMP] m_pfSamplesC   = amplitude ^ 2
// [TEMP] m_pdSamplesA   = subSample
//
// Sub-functions calls:
//		_phase_detection_beam
//		
// Used in sub-functions:
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonDetectionPhaseImage_3steps
//
bool CIFremerBottomDetect::_phase_detection( )
{
	int iS, nS, iB;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	float * Phase = m_pfSamplesA;
	float * Amp   = m_pfSamplesB;
	float * Amp2  = m_pfSamplesC;
	DWORD * Sub   = m_pdSamplesA;
	DWORD * NaNRange = (DWORD *)m_pRangePhase;
	int subBeg, subEnd;

	
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		for (iS = nS = 0; iS < m_iOutputSamples; iS++)
		{
			// !ALTRAN - 39 Begin
			//if (MasqueAmp(iB, iS) && MasquePhs(iB, iS))
			if (ISNANf(m_pMatrix[iS][iB])== false && 
				data[iS*m_dwReceiveBeams+iB].amp != 0xffff)
			// !ALTRAN End
			{
				// not NAN on Amp and Phase
				Sub[nS]    = iS+1;
				Phase[nS]  = m_pMatrix[iS][iB];
				Amp[nS]    = (float) data[iS*m_dwReceiveBeams+iB].amp;
				Amp2[nS++] = Amp[nS] * Amp[nS];
			}
		}
	
		subBeg = Sub[0]-1;
		subEnd = Sub[nS-1]-1;

		_phase_detection_beam(Sub, Amp, Amp2, Phase, nS, iB);

		if (ISNANf(m_pRangePhase[iB]) || m_pRangePhase[iB] > subEnd || m_pRangePhase[iB] < subBeg)
		{
			NANf(m_pRangePhase[iB]);
			NANf(m_pQualityPhase[iB]);
			NANf(m_pERMPhase[iB]);
			NANf(m_pSlopePhase[iB]);
			m_pSamplesPhase[iB]   = 0;
		}
	}

	return true;
}


//------------------------------------------------------------------------------
// Phase Detection - per beam
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [OUTPUT] m_pRangePhase
// [OUTPUT] m_pQualityPhase
// [OUTPUT] m_pSamplesPhase
// [OUTPUT] m_pERMPhase
// [OUTPUT] m_pSlopePhase
//
// [TEMP] m_pfSamplesA   = phase
// [TEMP] m_pfSamplesB   = amplitude
// [TEMP] m_pdSamplesA   = subSample
//
// Sub-functions calls:
//		_phase_detection_beam
//		
// Used in sub-functions:
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonDetectionPhaseImage_3steps
//
bool CIFremerBottomDetect::_phase_detection_beam( DWORD * subSamples, float * Amp, float * Amp2, float * Phs, int nbSamples, int Beam )
{
	NANf(m_pRangePhase[Beam]);
	NANf(m_pQualityPhase[Beam]);
	NANf(m_pERMPhase[Beam]);
	NANf(m_pSlopePhase[Beam]);
	m_pSamplesPhase[Beam]   = 0;

//%	% Sample number inside the whole beam
//%	Beta = 1.2;
//%	NechBeam = R0 * DeltaTeta * (pi/180) * sind(abs(BeamAngle)) / (2 * Beta * cosd(BeamAngle)^2);

	float NechBeam = (m_R0+1) * m_DeltaTeta * sinf(fabsf(m_pBeamAngles[Beam])) / (2.4f * cosf(m_pBeamAngles[Beam]) * cosf(m_pBeamAngles[Beam]));

//%	% Sample number inside the resolution cell (sounding horizontal spacing)
//%	NechRes = ceil(2 * abs(2 * R0 .* sind(BeamAngle) * (tand(MaxBeamAngle) / nbBeams)));
	float NechRes = ceilf(4.0f * fabsf((m_R0+1) * sinf(m_pBeamAngles[Beam]) * tanf(m_pBeamAngles[m_dwReceiveBeams-1]) / m_dwReceiveBeams));

//%	NechRes = min(NechBeam, NechRes);
//%	NechRes = max(NechRes, 6);
//%	NechRes = min(NechRes, 50);
	NechRes = min (NechBeam, NechRes);
	// !ALTRAN - 25  Begin
	//NechRes = limit(NechRes, 6, 50);
	NechRes = max(NechRes,6);
	// !ALTRAN end

	WORD NechRampMin = 7;
	int nSamples = nbSamples;

	// If number of samples is less than 11 we do not try to process the sounding
	if (nSamples < NechRampMin)
		return false;

	float NechSondeMin = ceilf((float)nSamples / 20.0f);
	NechSondeMin = max(NechSondeMin, (float)NechRampMin);
	NechSondeMin = max(NechSondeMin, NechRes);

	float R, QF, Pente, Eqm;
	int   NbSamples, i;

	for (i = 1; i < 4; i++)
	{
		if (!_phase_detection_beam_single( subSamples, Phs, Amp, Amp2, i, nSamples, Beam, NechSondeMin, R, QF, Pente, Eqm, NbSamples ))
		{
			if (i == 1 && QF > 3.0f)
			{
				m_pRangePhase[Beam]   = R-1;
				m_pQualityPhase[Beam] = QF;
				m_pERMPhase[Beam]     = Eqm;
				m_pSlopePhase[Beam]   = Pente;
				m_pSamplesPhase[Beam] = NbSamples;
			}

			return true;
		}
		
		m_pRangePhase[Beam]   = R-1;
		m_pQualityPhase[Beam] = QF;
		m_pERMPhase[Beam]     = Eqm;
		m_pSlopePhase[Beam]   = Pente;
		m_pSamplesPhase[Beam] = NbSamples;
	}

	return true;
}

//------------------------------------------------------------------------------
// Phase Detection per Beam. Single Iteration
//
// Arguments:
// [IN]  side   - true = Port(Babord); false = Starboard(Tribord)
//
// Return Value: success / failure
//
// [OUTPUT] m_pPhaseMin, m_pPhaseMax
//
// [TEMP] m_pfSamplesD   - temporary
// [TEMP] m_pfSamplesE   - temporary

bool CIFremerBottomDetect::_phase_detection_beam_single( DWORD *subSamples, float *Phase, float *Amp, float *Amp2, int iter, 
														 int &nSamples, const int Beam, const float NechRes,
														 float &R, float &QF, float &Pente, float &Eqm, int &NbSamples)
{
	int iS, nS;

	NANf(R);
	NANf(QF);
	NANf(Pente);
	NANf(Eqm);
	NbSamples = 0;

//%	M = x(floor(length(x)/2));
//%	if (M > (R0*1.9)) && (M < (R0*2.1))
//%		amp(:) = 1;
//%	end

	float M = (float)subSamples[nSamples/2-1];
	float R0 = m_R0+1;
	if (M > (R0*1.9f) && M < (R0*2.1f))
	{
		for (iS = 0; iS < nSamples; iS++)
			Amp[iS] = 1.0f;
	}

//% subNonNaN = find(~isnan(phase) & ~isnan(amp));
//  At this point there should be no NaNs either in amplitude or phase data.

	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;

	bool flag;
	float A, B;

	switch (iter)
	{
	case 1:
		flag = _phase_regression(subSamples, Phase, Amp2, nSamples, A, B, R, Eqm);
		break;

	case 2:
		flag = _phase_regression(subSamples, Phase, Amp,  nSamples, A, B, R, Eqm);
		break;
		
	case 3:
		flag = _phase_regression(subSamples, Phase, NULL, nSamples, A, B, R, Eqm);
		break;
	}

	if (!flag)
		return false;

	Pente = fabsf(A);

//	QF = Pente * R * sqrtf((float)nSamples) / Eqm;

//%	dTPhi   = (Eqm / sqrt(length(x)/nbSamplesMoyennes)) / Pente;
//%	dTPulse = (TxPulseWidth * SampleRate) / sqrt(12);
//%	dT = sqrt(dTPhi*dTPhi + dTPulse*dTPulse);
//%	QF = R / dT;

//% From Reson_phaseFilter.m: nbSamplesMoyennes=2*n+1; n=4; ==> 9

	float dTPhi = (Eqm / sqrtf((float)nSamples/9.0f)) / Pente;
	// !ALTRAN - 24  Begin
	//QF = R / sqrtf(dTPhi*dTPhi + m_dTPulse);
	QF = R / sqrtf(dTPhi*dTPhi + m_dTPulse*m_dTPulse);
	// !ALTRAN end

	if (QF > 0)
		QF = log10f(QF);
	else
 		QF = 0;

	float * PhasePoly = m_pfSamplesE;

	for (iS = 0; iS < nSamples; iS++)
	{
		PhasePoly[iS]    = A*subSamples[iS]+B;			// phase_poly
		m_pfSamplesD[iS] = Phase[iS] - PhasePoly[iS];	// Diff
	}

	Eqm = _nanStd(m_pfSamplesD, nSamples, subSamples);
	NbSamples = nSamples;

	// Suppression of outliers
//%	subNonOutliers = find(abs(Diff) <= (2*Eqm));

	for (iS = nS = 0; iS < nSamples; iS++)
	{
		if (fabsf(m_pfSamplesD[iS]) <= 2.0f*Eqm)
		{
			subSamples[nS]  = subSamples[iS];
			Phase[nS]       = Phase[iS];
			Amp[nS]         = Amp[iS];
			Amp2[nS]        = Amp2[iS];
			PhasePoly[nS++] = PhasePoly[iS];
		}
	}

	nSamples = nS;

	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;

	// Centrage autour de R
	int   r = floori(R);
	int   d = min(r-(int)subSamples[0], (int)subSamples[nSamples-1]-r);

	if (d <= 1)
		return false;

//% sub = find((x > (R-d)) & (x < R+d) & (phase_poly > -180) & (phase_poly < 180));

	for (iS = nS = 0; iS < nSamples; iS++)
	{
		if ((subSamples[iS] > R-d) && (subSamples[iS] < R+d) && (PhasePoly[iS] > -180) && (PhasePoly[iS] < 180))
		{
			subSamples[nS]  = subSamples[iS];
			Phase[nS]       = Phase[iS];
			Amp[nS]         = Amp[iS];
			Amp2[nS++]      = Amp2[iS];
		}
	}

	nSamples = nS;
	
	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;
	
	int S1 = ceili ((float)nSamples/6.0f)-1;
	int S2 = floori((float)nSamples*5.0f/6.0f)-1;

	// Reduction de 1/8 de part et d'autre
//%	n = length(x);
//%	sub = ceil(n/6):floor(n*5/6);
	for (iS = nS = 0; iS < nSamples; iS++)
	{
		if (iS >= S1 && iS <= S2)
		{
			subSamples[nS]  = subSamples[iS];
			Phase[nS]       = Phase[iS];
			Amp[nS]         = Amp[iS];
			Amp2[nS++]      = Amp2[iS];
		}
	}
	
	nSamples = nS;
	
	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;

	return true;
}


bool CIFremerBottomDetect::_phase_test(const DWORD * subSamples, const int nSamples, const float NechRes) const
{
	if ( (nSamples < NechRes) || ((nSamples < 50) && (((float)subSamples[nSamples-1] - (float)subSamples[0])/nSamples > 2.0f) ) )
		return true;
	else
		return false;
}

//------------------------------------------------------------------------------
// Linear Estimation of Phase Detection
//
// Arguments:
// [IN]  subSamples = samples indexes
// [IN]  Phs        = corresponding phase
// [IN]  Weights    = corresponding amplitude weight (or NULL)
// [IN]  nSamples   = number of samples in estimation (size of subSamples, Phs and Weights vectors)
// [OUT] A, B		= polynomial parameters of the fit (Ax+B)
// [OUT] X0         = point of zero crossing
// [OUT] Eqm        = mean-square error
// [OUT] nX         = actual number of points in the calculations
//
// Return Value: success / failure
//
// [TEMP] m_pfSamplesD   - temporary
// [TEMP] m_pfSamplesE   - temporary
// [TEMP] m_pfSamplesG   - "sample number"
//
bool CIFremerBottomDetect::_phase_regression( DWORD * subSamples, float * Phs, float * Weights, const int nSamples, 
											  float& A, float& B, float& X0, float& Eqm )
{
	int deltax = subSamples[nSamples-1] - subSamples[0];
	
	if (deltax == 0)
		return false;

	int iS;
	float MaxWeight = 0.0f;

	if (Weights)
		for (iS = 0; iS < nSamples; iS++)
			if (MaxWeight < Weights[iS]) MaxWeight = Weights[iS];

	float Alpha = 1.0f / (float)deltax;

	for (iS = 0; iS < nSamples; iS++)
	{
		if (Weights)
			m_pfSamplesD[iS] = Weights[iS] / MaxWeight;
		else
			m_pfSamplesD[iS] = 1.0f;

		m_pfSamplesE[iS] = Alpha * (subSamples[iS] - subSamples[0]);
	}

	float a, b, x0;

	if (!_phase_calcul(m_pfSamplesE, Phs, m_pfSamplesD, nSamples, a, b, x0, Eqm))
		return false;

	A  = Alpha * a;
	B  = b - A * subSamples[0];
	X0 = x0 / Alpha + subSamples[0];

	return true;
}


//------------------------------------------------------------------------------
// Weighted Linear Interpolation
//
// Arguments:
// [IN]  subSamples = samples indexes
// [IN]  Phs        = corresponding phase
// [IN]  Weights    = corresponding amplitude weight (or NULL)
// [IN]  nSamples   = number of samples in estimation (size of subSamples, Phs and Weights vectors)
// [OUT] A, B		= polynomial parameters of the fit (Ax+B)
// [OUT] X0         = point of zero crossing
// [OUT] Eqm        = mean-square error
//
// Return Value: success / failure
//
bool CIFremerBottomDetect::_phase_calcul( float * subSamples, float * Phs, float * Weights, int nSamples, 
										  float& A, float& B, float& X0, float& Eqm )
{
	float SumW = 0.0f, SumX = 0.0f, SumX2 = 0.0f, SumXY = 0.0f, SumY = 0.0f;
	int iS;

	float *phase, *w, *x;

	for (iS = 0, phase = Phs, w = Weights, x = subSamples; iS < nSamples; iS++, phase++, w++, x++)
	{
//%		SumW  = sum(w);
		SumW  += *w;
//%		SumX  = sum(w .* x);
		SumX  += *w**x;
//%		SumX2 = sum(w .* x.^2);
		SumX2 += *w**x**x;
//%		SumXY = sum(w .* x .* phase);
		SumXY += *w**x**phase;
//%		SumY  = sum(w .* phase);
		SumY  += *w**phase;
	}

	float delta = SumW * SumX2 - SumX * SumX;

	if (fabsf(delta) > FLT_EPSILON*2)
	{
		// Calculate slope, intercept and correlation of linear fit
		B = (SumX2 * SumY - SumX * SumXY) / delta;
		A = (SumW * SumXY - SumX * SumY) / delta;

		X0 = -(SumX2 * SumY - SumX * SumXY) / (SumW * SumXY - SumX * SumY);		// X0 = -B/A; --- ?????

		Eqm = 0.0f;
		for (iS = 0, phase = Phs, w = Weights, x = subSamples; iS < nSamples; iS++, phase++, w++, x++)
		{
			SumX = *phase - (A**x+B);
			Eqm += *w * SumX * SumX;
		}

		Eqm = sqrtf(Eqm / SumW);
	}
	else
		return false;

	return true;
}
