// Transcribed from Matlab to C code
// See 'TPE according to Ifremer.doc'


#include "stdafx.h"
#include "IfremerUncertainty.h"

#include <math.h>
#include <float.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Compute relative Amplitude uncertainty using Ifremer's formula
//
// minsamp: Minimum sample number
// maxsamp: Maximum sample number
// Amp    : Magnitude samples
double IfremerUncertainty::AmpUncertainty(int minsamp, int maxsamp, const double* Amp)
{
	// Weighted mean computation
	double AmpBeamRange = 0.0;
	double Sigma = 0.0;
	calcul_barycentre(minsamp, maxsamp, Amp, AmpBeamRange, Sigma);

	// Select valid data set and re-compute
	double minRange = AmpBeamRange - 2.0 * Sigma;
	double maxRange = AmpBeamRange + 2.0 * Sigma;
	int first = (int) ceil(minRange);
	if (first < minsamp)
		first = minsamp;
	int last = (int) floor(maxRange);
	if (last > maxsamp)
		last = maxsamp;

	Sigma = 0.0;
	calcul_barycentre(first, last, &Amp[first - minsamp], AmpBeamRange, Sigma);

	// Divide by zero?
	if (Sigma == 0.0)
		return 0.0;

	double AmpBeamQF = 5.0 * AmpBeamRange / sqrt(2.0 * Sigma);
	double U = 1.0 / AmpBeamQF;

	return U;
}


void IfremerUncertainty::calcul_barycentre(
		int first, int last, const double* Amp,
		double& R0, double& S)
{
	int nSamples = last - first + 1;

	double sumEnr = 0.0;
	int i;
	for (i = 0; i < nSamples; i++)
	{
		double Enr = Amp[i] * Amp[i];
		sumEnr += Enr;
	}

	// Divide by zero?
	if (!sumEnr)
		return;

	R0 = 0.0;
	double M2 = 0.0;
	for (i = 0; i < nSamples; i++)
	{
		double Enr = Amp[i] * Amp[i];
		Enr /= sumEnr;

		double R = first + i;
		R0 += R * Enr;
		M2 += R * R * Enr;
	}

	double VX = M2 - R0 * R0;
	if (VX > 0.0)
		S = sqrt(VX);
}


// Compute relative Phase detection uncertainty using Ifremer's formula
//
// minsamp: Minimum sample number
// maxsamp: Maximum sample number
// phase  : Phase samples.
// amp    : Magnitude samples.
double IfremerUncertainty::PhaseUncertainty(int minsamp, int maxsamp,
		const double* phase, const double* amp)
{
	int nSamples = maxsamp - minsamp + 1;
	if (nSamples <= 0)
		return 0.0;

	// Square magnitude samples
	double* amp2 = new double[nSamples];
	if (!amp2) return 0.0;

	for (int i = 0; i < nSamples; i++)
	{
		amp2[i] = amp[i] * amp[i];
	}

	double a = 0.0, b = 0.0, R = 0.0, Eqm = 0.0;
	regressionLineairePonderee(minsamp, maxsamp, phase, amp2, a, b, R, Eqm);

	delete[] amp2;

	double pente = fabs(a);
	double QF = pente * R * sqrt((double) nSamples) / Eqm;
	double U = 1.0 / QF;

	return U;
}


void IfremerUncertainty::regressionLineairePonderee(
		int minsamp, int maxsamp, const double* phase, double* w,
		double& a, double& b, double& x0, double& Eqm)
{
	int nSamples = maxsamp - minsamp + 1;

	// Find maximum
	double wMax = -DBL_MAX;
	int i;
	for (i = 0; i < nSamples; i++)
	{
		if (w[i] > wMax)
			wMax = w[i];
	}

	// Divide by zero?
	if (wMax == 0.0)
		return;

	for (i = 0; i < nSamples; i++)
	{
		w[i] /= wMax;
	}

	int xd = minsamp;
	int deltax = maxsamp - xd;
	if (deltax <= 0)
		return;

	double Alpha = 1.0 / deltax;

	double A = 0.0, B = 0.0, X0 = 0.0;
	calcul(deltax, phase, w, A, B, X0, Eqm);

	a = Alpha * A;
	b = B - a * xd;
	x0 = X0 / Alpha + xd;
}


void IfremerUncertainty::calcul(
		int deltax, const double* phase, const double* w,
		double& A, double& B, double& X0, double& Eqm)
{
	int nSamples = deltax + 1;
	double Alpha = 1.0 / deltax;

	double SumW = 0.0;
	double SumX = 0.0;
	double SumX2 = 0.0;
	double SumXY = 0.0;
	double SumY = 0.0;
	int i;
	for (i = 0; i < nSamples; i++)
	{
		double xVal = Alpha * i;
		double wVal = w[i];

		SumW += wVal;
		SumX += wVal * xVal;
		SumX2 += wVal * xVal * xVal;
		SumXY += wVal * xVal * phase[i];
		SumY += wVal * phase[i];
	}

	double delta = SumW * SumX2 - SumX * SumX;
	if (delta == 0.0)
		return;

	// Calculate slope, intercept and correlation of linear fit
	B = (SumX2 * SumY - SumX * SumXY) / delta;
	A = (SumW * SumXY - SumX * SumY) / delta;

	X0 = -(SumX2 * SumY - SumX * SumXY) / (SumW * SumXY - SumX * SumY);

	double SumE = 0.0;
	for (i = 0; i < nSamples; i++)
	{
		double xVal = Alpha * i;
		double wVal = w[i];

		double temp = phase[i] - (A * xVal + B);
		SumE += wVal * temp * temp;
	}

	Eqm = sqrt(SumE / SumW);
}


// Compute blended uncertainty
//
// ampU        : Uncertainty for amplitude detection
// phaseU      : Uncertainty for phase detection
// dCorrelation: Correlation coefficient
// ddLo        : Threshold for amplitude processing (typically 0.60)
// ddHi        : Threshold for magnitude processing (typically 0.95)
double IfremerUncertainty::BlendUncertainty(double ampU, double phaseU,
		double dCorrelation, double ddLo, double ddHi)
{
	double alpha = (dCorrelation - ddLo) / (ddHi - ddLo);
	double ampPart = (1.0 - alpha) * ampU;
	double phasePart = alpha * phaseU;
	double U = sqrt(ampPart * ampPart + phasePart * phasePart);

	return U;
}
