#ifndef IFREMER_UNCERTAINTY_H
#define IFREMER_UNCERTAINTY_H


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class IfremerUncertainty
{
public:
	static double AmpUncertainty(int minsamp, int maxsamp, const double* Amp);
	static double PhaseUncertainty(int minsamp, int maxsamp,
		const double* phase, const double* amp);
	static double BlendUncertainty(double ampU, double phaseU,
		double dCorrelation, double ddLo, double ddHi);

private:
	static void calcul_barycentre(
		int minsamp, int maxsamp, const double* Amp,
		double& R0, double& S);
	static void regressionLineairePonderee(
		int minsamp, int maxsamp, const double* phase, double* w,
		double& a, double& b, double& x0, double& Eqm);
	static void calcul(
		int deltax, const double* phase, const double* w,
		double& A, double& B, double& X0, double& Eqm);
};


#endif  // IFREMER_UNCERTAINTY_H
