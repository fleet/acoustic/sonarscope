/***************************************************************************
****************************************************************************

   MathUtils.h:  
   
   A collection of math utility functions, wrapped into a DLL.  Most are
   templatized so that they can work with int, float, double, etc.

   Copyright � 2004-2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    16-Dec-2004
   Author:  Jim King
   Change:  Initial Draft of DLL from original source by Eric M.
   
***************************************************************************
**************************************************************************/

#include "stdafx.h"
#include "MathUtils.h"
#include <math.h>


template<typename T> double Norm_ (T *X, int nX);


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


/***************************************************************************
   Rounds to floating point value 'x' to the nearest integral value.
   Return value is an int.
***************************************************************************/
MATHUTILS_API int Round (double x)
{
	return((int) floor(x+0.5));
}


/***************************************************************************
   Rounds floating point value to specified precision.  If 'precision' is
   1.0, then result is same as Round() above, but return value is double.
   Examples:  
   RoundWithPrecision(2.6, 0.5)    = 2.5
   RoundWithPrecision(1.77, 0.1)   = 1.8
   RoundWithPrecision(124.5, 10.0) = 120.0
   'presicion' must be > zero.
   JSK 23-May-07 - function added
***************************************************************************/
MATHUTILS_API double RoundWithPrecision (double x, double precision)
{
   if (precision < 1.0e-20)
      return(x);

   double dMulDiv = 1.0 / precision;

   return(floor(x * dMulDiv + 0.5) / dMulDiv);
}


/***************************************************************************
   Implements MatLab [n,xout] = hist(Y,nbins) function.  Returns vectors  
   BinCount[] and BinLocations[] containing the frequency counts and the 
   bin locations.  See MatLab help on hist() function for more details.
   NOTE:  Not implemented as template, due to fundamental differences in
   how bin boundaries are handled for integers verses floating point.  This
   version is for integer data.
   'iLength'  = length of 'pData' array.
   'iNumBins' = length of 'piBinCount' and 'pdBinLocations' arrays.
***************************************************************************/
MATHUTILS_API void  Hist (double *pData, int iLength, int iNumBins, int *piBinCount, double *pdBinLocations)
{
   int    i;
   double dMin   = Vmin(pData, iLength);
   double dMax   = Vmax(pData, iLength);
   double dWidth = (dMax - dMin) / iNumBins;
   double dTemp  = dMin + dWidth;
   
   for (i=0; i<iNumBins; i++,dTemp+=dWidth)  // set bin max boundaries
      pdBinLocations[i] = dTemp;

   memset(piBinCount, 0, sizeof(int)*iNumBins);

   for (i=0; i<iLength; i++)                 // bin up the data 
      for (int j=0; j<iNumBins; j++) {
         if (pData[i] < pdBinLocations[j]) {
            piBinCount[j]++;
            break;
         }
      }

   for (i=0; i<iNumBins; i++)                // set bin location to *middle* of bin
      pdBinLocations[i] -= (dWidth / 2.0);
}


/***************************************************************************
   Finds all values common to both piSet1 and piSet2.  The result is in
   piSet1.  piSet1 and piSet2 must be terminated by a -1 value.
   In other words, removes all elements from piSet1 that DO NOT appear in
   piSet2.
***************************************************************************/
MATHUTILS_API void  Common (int *piSet1, const int *piSet2)
{
   int *P1      = piSet1;
   int *PCommon = piSet1;
   const int *P2;

   for (;*P1>=0; P1++) {
      P2 = piSet2;
      while (*P2 >= 0) {
         if (*P1 == *P2) 
            break;
         P2++;
      }
      if (*P2 >= 0)
         *PCommon++ = *P2;
   }
   *PCommon = -1;
}


/***************************************************************************
// Linear interpolation using Least Mean Square approach
***************************************************************************/
MATHUTILS_API void PolyFit1 (const double *x, const double *y, int nx, double *p)
{
	double sumx = 0 ;
	double sumy = 0 ;
	double sumxy = 0 ;
	double sumx2 = 0 ;

	for ( int i = 0; i < nx; i++) {
		sumx += x[i] ;
		sumy += y[i] ;
		sumxy += x[i] * y[i] ;
		sumx2 += x[i] * x[i] ;
	}
	double delta = nx*sumx2 - sumx * sumx ;
	p[0] = (nx * sumxy - sumx * sumy) / delta ;
	p[1] = (sumx2 * sumy - sumx * sumxy) / delta ;
}


/***************************************************************************
//	PolyVal()
//	---------
//
//	n = number of points to evaluate
//	m = degree of the polynom
***************************************************************************/
MATHUTILS_API void PolyVal (const double *x, double *y, int n, double *p, int m)
{
	int i, k;

	for(i = 0; i < n; i++)
	{
		y[i] = p[0];				   	// 12 Jan 04 - added per Eric
		for(k = 1; k <= m; k++)			// 12 Jan 04 - made k <= m      
			y[i] = y[i] * x[i] + p[k];
	}
}





// TEST!!
/*
MATHUTILS_API int **_2DArrayAlloc (int x, int y, int initValue, bool bInit)
{
   int i;
   DWORD dwSize = (sizeof(int *) * x) + (x * y * sizeof(int));
   int   **Buf = (int **) new char[dwSize];
   int    *Ptr;
   if (!Buf)
      return(NULL);
                                          // set ptr to first COLUMN
   Ptr = (int *) (((char *) Buf) + (sizeof(int *) * x));
 
   for (i=0; i<x; i++,Ptr+=y)             // set up column pointers
      Buf[i] = Ptr;

   int iTEST = 0;

   if (bInit) {
      for (int i=0; i<x; i++)             // loop thru rows
         for (int j=0; j<y; j++)          // loop thru columns
            //Buf[i][j] = initValue;
            Buf[i][j] = iTEST++;
   }
   return(Buf);
}
*/
