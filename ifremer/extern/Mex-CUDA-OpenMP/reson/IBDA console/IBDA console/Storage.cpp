#include "stdafx.h"

#include <stdlib.h>
#include <string.h>
#include "Storage.h"
/*
typedef enum {
	dmxUNKNOWN_CLASS = 0,
	dmxCELL_CLASS,
	dmxSTRUCT_CLASS,
	dmxLOGICAL_CLASS,
	dmxCHAR_CLASS,
	dmxDOUBLE_CLASS,
	dmxSINGLE_CLASS,
	dmxINT8_CLASS,
	dmxUINT8_CLASS,
	dmxINT16_CLASS,
	dmxUINT16_CLASS,
	dmxINT32_CLASS,
	dmxUINT32_CLASS,
} DMX_TYPE;
*/

CStorage::CStorage() 
	: data(NULL),rows(0),cols(0),name(NULL),type(dmxUNKNOWN_CLASS),child(NULL),subling(NULL),dynamic(false),dynamic_data(false) {}

CStorage::CStorage(char *stname, DMX_TYPE dmxtype)
	: data(NULL),rows(0),cols(0),name(NULL),type(dmxtype),child(NULL),subling(NULL),dynamic(false),dynamic_data(false) {
	if (/*type != dmxCELL_CLASS && */type != dmxSTRUCT_CLASS) throw (0);
	if (stname) name = strdup(stname);
}

CStorage::CStorage(char *stname, DMX_TYPE dmxtype, int stcols, void * stdata)
	: data(stdata),rows(1),cols(stcols),name(NULL),type(dmxtype),child(NULL),subling(NULL),dynamic(false),dynamic_data(false) {
	if (type == dmxCELL_CLASS || type == dmxSTRUCT_CLASS) throw (0);
	if (stname) name = strdup(stname);
}

CStorage::CStorage(char *stname, DMX_TYPE dmxtype, int stcols, int strows, void * stdata)
	: data(stdata),rows(strows),cols(stcols),name(NULL),type(dmxtype),child(NULL),subling(NULL),dynamic(false),dynamic_data(false) {
	if (type == dmxCELL_CLASS || type == dmxSTRUCT_CLASS) throw (0);
	if (stname) name = strdup(stname);
}

CStorage::CStorage(const CStorage &copy) { *this = copy; }
CStorage& CStorage::operator = (const CStorage &copy) {
	data = copy.data;
	rows = copy.rows;
	cols = copy.cols;
	if (name) free(name);
	name = (copy.name) ? _strdup(copy.name) : NULL;
	type = copy.type;
	child = copy.child;
	subling = copy.subling;
	return *this;
}

CStorage::CStorage(FILE *fp)
	: data(NULL),rows(0),cols(0),name(NULL),type(dmxUNKNOWN_CLASS),child(NULL),subling(NULL),dynamic(false),dynamic_data(false) {
	Load(fp);
}


CStorage::~CStorage() {
	if (name) { free(name); name = (char *)0; }
	rows = cols = 0;
	type = dmxUNKNOWN_CLASS;
	if (dynamic_data) {
		if (data) free(data);
		if (child) {
			CStorage * s;
			while ((s = _last()) != this) delete s;
		}
	}
	data = child = subling = NULL;
	dynamic_data = dynamic = false;
}

void *CStorage::operator new   ( size_t datasize ) {
	void * mem = malloc(datasize);
	if (!mem) throw(0);

	CStorage * s = (CStorage *) mem;
	s->name = (char *)0;
	s->data = NULL;
	s->child = s->subling = (CStorage *)0;
	s->rows = s->cols = 0;
	s->type = dmxUNKNOWN_CLASS;
	s->dynamic_data = false;
	s->dynamic = true;
	return s;
}

void  CStorage::operator delete( void * obj) {
	CStorage * object = (CStorage *) obj;
	if (object->name) { free(object->name); object->name = NULL; }
	object->rows = object->cols = 0;
	object->type = dmxUNKNOWN_CLASS;
	if (object->dynamic_data) {
		if (object->data) free(object->data);
		if (object->child) {
			CStorage * s;
			while ((s = object->_last()) != object) delete s;
		}
	}
	object->data = object->child = object->subling = NULL;
	object->dynamic_data = false;
	if (object->dynamic) free(object);
}

CStorage& CStorage::_set(void * stdata, DMX_TYPE dmxtype) {
	if (dmxtype != dmxUNKNOWN_CLASS && type != dmxtype) throw (0);
	if (rows >= 1 && cols >= 1) throw(0);
	data = stdata;
	return *this;
}

CStorage& CStorage::operator = (         bool     stdata) { return _set(&stdata, dmxLOGICAL_CLASS); }
CStorage& CStorage::operator = (         bool   * stdata) { return _set( stdata, dmxLOGICAL_CLASS); }

CStorage& CStorage::operator = (         char     stdata) {
	if (type != dmxCHAR_CLASS && type != dmxINT8_CLASS) throw (0);
	return _set( &stdata, dmxUNKNOWN_CLASS);
}

CStorage& CStorage::operator = (         char   * stdata) {
	if (type != dmxCHAR_CLASS && type != dmxINT8_CLASS) throw (0);
	return _set( stdata, dmxUNKNOWN_CLASS);
}

CStorage& CStorage::operator = (unsigned char     stdata) { return _set(&stdata, dmxUINT8_CLASS); }
CStorage& CStorage::operator = (unsigned char   * stdata) { return _set( stdata, dmxUINT8_CLASS); }
CStorage& CStorage::operator = (         double   stdata) { return _set(&stdata, dmxDOUBLE_CLASS); }
CStorage& CStorage::operator = (         double * stdata) { return _set( stdata, dmxDOUBLE_CLASS); }
CStorage& CStorage::operator = (         float    stdata) { return _set(&stdata, dmxSINGLE_CLASS); }
CStorage& CStorage::operator = (         float  * stdata) { return _set( stdata, dmxSINGLE_CLASS); }
CStorage& CStorage::operator = (         short    stdata) { return _set(&stdata, dmxINT16_CLASS); }
CStorage& CStorage::operator = (         short  * stdata) { return _set( stdata, dmxINT16_CLASS); }
CStorage& CStorage::operator = (unsigned short    stdata) { return _set(&stdata, dmxUINT16_CLASS); }
CStorage& CStorage::operator = (unsigned short  * stdata) { return _set( stdata, dmxUINT16_CLASS); }
CStorage& CStorage::operator = (         long     stdata) { return _set(&stdata, dmxINT32_CLASS); }
CStorage& CStorage::operator = (         long   * stdata) { return _set( stdata, dmxINT32_CLASS); }
CStorage& CStorage::operator = (unsigned long     stdata) { return _set(&stdata, dmxUINT32_CLASS); }
CStorage& CStorage::operator = (unsigned long   * stdata) { return _set( stdata, dmxUINT32_CLASS); }

void * CStorage::_get(DMX_TYPE dmxtype) {
	if (dmxtype != dmxUNKNOWN_CLASS && type != dmxtype) throw (0);
	return data;
}

CStorage::operator          bool   * () { return (bool *)			_get(dmxLOGICAL_CLASS); }
CStorage::operator          char   * () {
	if (type != dmxCHAR_CLASS && type != dmxINT8_CLASS) throw (0);
	return (char *) _get( dmxUNKNOWN_CLASS);
}

CStorage::operator unsigned char   * () { return (unsigned char *)	_get(dmxUINT8_CLASS); }
CStorage::operator          double * () { return (double *)			_get(dmxDOUBLE_CLASS); }
CStorage::operator          float  * () { return (float *)			_get(dmxSINGLE_CLASS); }
CStorage::operator          short  * () { return (short *)			_get(dmxINT16_CLASS); }
CStorage::operator unsigned short  * () { return (unsigned short *)	_get(dmxUINT16_CLASS); }
CStorage::operator          long   * () { return (long *)			_get(dmxINT32_CLASS); }
CStorage::operator unsigned long   * () { return (unsigned long *)	_get(dmxUINT32_CLASS); }

CStorage& CStorage::_first(){
	if (!Valid()) throw(0);
	if (type == dmxCELL_CLASS || type == dmxSTRUCT_CLASS) return child->_first();
	else return *this;
}

CStorage::operator          bool     () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return      *(bool*)get.data;
	case dmxCHAR_CLASS:		return 0 != *(char*)get.data;
	case dmxDOUBLE_CLASS:	return 0 != *(double*)get.data;
	case dmxSINGLE_CLASS:	return 0 != *(float*)get.data;
	case dmxINT8_CLASS:		return 0 != *(char*)get.data;
	case dmxUINT8_CLASS:	return 0 != *(unsigned char*)get.data;
	case dmxINT16_CLASS:	return 0 != *(short*)get.data;
	case dmxUINT16_CLASS:	return 0 != *(unsigned short*)get.data;
	case dmxINT32_CLASS:	return 0 != *(long*)get.data;
	case dmxUINT32_CLASS:	return 0 != *(unsigned long*)get.data;
	default: throw(0);
	}
}

CStorage::operator          char     () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (char)*(bool*)get.data;
	case dmxCHAR_CLASS:		return       *(char*)get.data;
	case dmxDOUBLE_CLASS:	return (char)*(double*)get.data;
	case dmxSINGLE_CLASS:	return (char)*(float*)get.data;
	case dmxINT8_CLASS:		return (char)*(char*)get.data;
	case dmxUINT8_CLASS:	return (char)*(unsigned char*)get.data;
	case dmxINT16_CLASS:	return (char)*(short*)get.data;
	case dmxUINT16_CLASS:	return (char)*(unsigned short*)get.data;
	case dmxINT32_CLASS:	return (char)*(long*)get.data;
	case dmxUINT32_CLASS:	return (char)*(unsigned long*)get.data;
	default: throw(0);
	}
}

CStorage::operator unsigned char     () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (unsigned char)*(bool*)get.data;
	case dmxCHAR_CLASS:		return (unsigned char)*(char*)get.data;
	case dmxDOUBLE_CLASS:	return (unsigned char)*(double*)get.data;
	case dmxSINGLE_CLASS:	return (unsigned char)*(float*)get.data;
	case dmxINT8_CLASS:		return (unsigned char)*(char*)get.data;
	case dmxUINT8_CLASS:	return                *(unsigned char*)get.data;
	case dmxINT16_CLASS:	return (unsigned char)*(short*)get.data;
	case dmxUINT16_CLASS:	return (unsigned char)*(unsigned short*)get.data;
	case dmxINT32_CLASS:	return (unsigned char)*(long*)get.data;
	case dmxUINT32_CLASS:	return (unsigned char)*(unsigned long*)get.data;
	default: throw(0);
	}
}

CStorage::operator          double   () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (double)*(bool*)get.data;
	case dmxCHAR_CLASS:		return (double)*(char*)get.data;
	case dmxDOUBLE_CLASS:	return         *(double*)get.data;
	case dmxSINGLE_CLASS:	return (double)*(float*)get.data;
	case dmxINT8_CLASS:		return (double)*(char*)get.data;
	case dmxUINT8_CLASS:	return (double)*(unsigned char*)get.data;
	case dmxINT16_CLASS:	return (double)*(short*)get.data;
	case dmxUINT16_CLASS:	return (double)*(unsigned short*)get.data;
	case dmxINT32_CLASS:	return (double)*(long*)get.data;
	case dmxUINT32_CLASS:	return (double)*(unsigned long*)get.data;
	default: throw(0);
	}
}

CStorage::operator          float    () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (float)*(bool*)get.data;
	case dmxCHAR_CLASS:		return (float)*(char*)get.data;
	case dmxDOUBLE_CLASS:	return (float)*(double*)get.data;
	case dmxSINGLE_CLASS:	return        *(float*)get.data;
	case dmxINT8_CLASS:		return (float)*(char*)get.data;
	case dmxUINT8_CLASS:	return (float)*(unsigned char*)get.data;
	case dmxINT16_CLASS:	return (float)*(short*)get.data;
	case dmxUINT16_CLASS:	return (float)*(unsigned short*)get.data;
	case dmxINT32_CLASS:	return (float)*(long*)get.data;
	case dmxUINT32_CLASS:	return (float)*(unsigned long*)get.data;
	default: throw(0);
	}
}
CStorage::operator          short    () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (short)*(bool*)get.data;
	case dmxCHAR_CLASS:		return (short)*(char*)get.data;
	case dmxDOUBLE_CLASS:	return (short)*(double*)get.data;
	case dmxSINGLE_CLASS:	return (short)*(float*)get.data;
	case dmxINT8_CLASS:		return (short)*(char*)get.data;
	case dmxUINT8_CLASS:	return (short)*(unsigned char*)get.data;
	case dmxINT16_CLASS:	return        *(short*)get.data;
	case dmxUINT16_CLASS:	return (short)*(unsigned short*)get.data;
	case dmxINT32_CLASS:	return (short)*(long*)get.data;
	case dmxUINT32_CLASS:	return (short)*(unsigned long*)get.data;
	default: throw(0);
	}
}
CStorage::operator unsigned short    () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (unsigned short)*(bool*)get.data;
	case dmxCHAR_CLASS:		return (unsigned short)*(char*)get.data;
	case dmxDOUBLE_CLASS:	return (unsigned short)*(double*)get.data;
	case dmxSINGLE_CLASS:	return (unsigned short)*(float*)get.data;
	case dmxINT8_CLASS:		return (unsigned short)*(char*)get.data;
	case dmxUINT8_CLASS:	return (unsigned short)*(unsigned char*)get.data;
	case dmxINT16_CLASS:	return (unsigned short)*(short*)get.data;
	case dmxUINT16_CLASS:	return                 *(unsigned short*)get.data;
	case dmxINT32_CLASS:	return (unsigned short)*(long*)get.data;
	case dmxUINT32_CLASS:	return (unsigned short)*(unsigned long*)get.data;
	default: throw(0);
	}
}
CStorage::operator          long     () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (long)*(bool*)get.data;
	case dmxCHAR_CLASS:		return (long)*(char*)get.data;
	case dmxDOUBLE_CLASS:	return (long)*(double*)get.data;
	case dmxSINGLE_CLASS:	return (long)*(float*)get.data;
	case dmxINT8_CLASS:		return (long)*(char*)get.data;
	case dmxUINT8_CLASS:	return (long)*(unsigned char*)get.data;
	case dmxINT16_CLASS:	return (long)*(short*)get.data;
	case dmxUINT16_CLASS:	return (long)*(unsigned short*)get.data;
	case dmxINT32_CLASS:	return       *(long*)get.data;
	case dmxUINT32_CLASS:	return (long)*(unsigned long*)get.data;
	default: throw(0);
	}
}
CStorage::operator unsigned long     () {
	CStorage get = _first();
	switch (get.type) {
	case dmxLOGICAL_CLASS:	return (unsigned long)*(bool*)get.data;
	case dmxCHAR_CLASS:		return (unsigned long)*(char*)get.data;
	case dmxDOUBLE_CLASS:	return (unsigned long)*(double*)get.data;
	case dmxSINGLE_CLASS:	return (unsigned long)*(float*)get.data;
	case dmxINT8_CLASS:		return (unsigned long)*(char*)get.data;
	case dmxUINT8_CLASS:	return (unsigned long)*(unsigned char*)get.data;
	case dmxINT16_CLASS:	return (unsigned long)*(short*)get.data;
	case dmxUINT16_CLASS:	return (unsigned long)*(unsigned short*)get.data;
	case dmxINT32_CLASS:	return (unsigned long)*(long*)get.data;
	case dmxUINT32_CLASS:	return                *(unsigned long*)get.data;
	default: throw(0);
	}
}

const bool CStorage::Valid() {
	if (type == dmxCELL_CLASS || type == dmxSTRUCT_CLASS) {
		return (child != NULL && rows != 0);
	} else if (type != dmxUNKNOWN_CLASS) {
		return (data != NULL && rows >= 1 && cols >= 1);
	} else {
		return false;
	}
}

CStorage* CStorage::_last() {
	if (type != dmxCELL_CLASS && type != dmxSTRUCT_CLASS) throw(0);
	if (child) {
		CStorage * scan = child;
		while (scan->subling) scan = scan->subling;
		return scan;
	} else
		return this;
}

CStorage& CStorage::operator <<(CStorage &in) {
	CStorage * last = _last();
	if (last == this)
		child = &in;
	else
		last->subling = &in;
	rows++;
	if (in.dynamic || in.dynamic_data)
		dynamic_data = true;

	return *this;
}

CStorage& CStorage::operator[] (int index) {
	if (!Valid()) throw(0);
	if (rows == 1)						// Vector
		if (index >= cols)	throw(0);
	else								// Matrix
		if (index >= rows)	throw(0);

	int esize;
	switch (type) {
	case dmxINT8_CLASS:
	case dmxUINT8_CLASS:
	case dmxCHAR_CLASS:
	case dmxLOGICAL_CLASS:	esize = 1; break;
	case dmxINT16_CLASS:
	case dmxUINT16_CLASS:	esize = 2; break;
	case dmxSINGLE_CLASS:
	case dmxINT32_CLASS:
	case dmxUINT32_CLASS:	esize = 4; break;
	case dmxDOUBLE_CLASS:	esize = 8; break;
	default: throw(0);
	}

	CStorage * ret = new CStorage;
	
	if (rows == 1) {			// Vector
		ret->rows = 1;
		ret->cols = 1;
		ret->data = (void *)(((char *)data)+index*esize);
	} else {
		ret->rows = 1;
		ret->cols = cols;
		ret->data = (void *)(((char *)data)+index*cols*esize);
	}

	ret->type	 = type;

	return *ret;
}

CStorage& CStorage::operator() (char* stname) {
	if (type != dmxCELL_CLASS && type != dmxSTRUCT_CLASS) throw(0);
	if (!child) throw (0);

	CStorage *scan = child;
	do {
		if (strcmp(scan->name, stname) == 0) return *scan;
		scan = scan->subling;
	} while (scan);
	throw(0);
}

CStorage& CStorage::Save(FILE * fp) {
	if (!Valid()) throw(0);
	unsigned short nlen;
	unsigned long  len;
	if (!name) throw(0);
	nlen = strlen(name);
	fwrite(&nlen, sizeof(unsigned short), 1, fp);
	if (!nlen) throw(0);
	fwrite(name, sizeof(char), nlen, fp);
	nlen = (unsigned short)type;
	fwrite(&nlen, sizeof(unsigned short), 1, fp);

	if (type == dmxCELL_CLASS || type == dmxSTRUCT_CLASS) {
		len = (unsigned long)rows;
		fwrite(&len, sizeof(unsigned long), 1, fp);
		if (rows) {
			CStorage * s;
			int i;
			for (i=0, s = child; i<rows; i++, s=s->subling) {
				if (!s) throw(0);
				s->Save(fp);
			}
		}
	} else {
		int esize;
		switch (type) {
		case dmxINT8_CLASS:
		case dmxUINT8_CLASS:
		case dmxCHAR_CLASS:
		case dmxLOGICAL_CLASS:	esize = 1; break;
		case dmxINT16_CLASS:
		case dmxUINT16_CLASS:	esize = 2; break;
		case dmxSINGLE_CLASS:
		case dmxINT32_CLASS:
		case dmxUINT32_CLASS:	esize = 4; break;
		case dmxDOUBLE_CLASS:	esize = 8; break;
		default: throw(0);
		}

		len = (unsigned long)rows; fwrite(&len, sizeof(unsigned long), 1, fp);
		len = (unsigned long)cols; fwrite(&len, sizeof(unsigned long), 1, fp);
		fwrite(data, rows*cols, esize, fp);
	}

	return *this;
}

CStorage& CStorage::Load(FILE * fp) {
	unsigned short len;
	fread(&len, sizeof(unsigned short), 1, fp);
	if (!len) throw(0);
	if (name) free(name);
	name = (char *)malloc(len+1);
	fread(name, sizeof(char), len, fp);
	name[len] = '\0';

	fwrite(&len, sizeof(unsigned short), 1, fp);
	type = (DMX_TYPE)len;
	
	if (type == dmxCELL_CLASS || type == dmxSTRUCT_CLASS) {
		fwrite(&len, sizeof(unsigned short), 1, fp);
		rows = (int)len;
		cols = 1;
		if (rows) {
			for (int i=0; i<rows; i++) {
				*this << *new CStorage(fp);
			}
		}
	} else {
		int esize;
		switch (type) {
		case dmxINT8_CLASS:
		case dmxUINT8_CLASS:
		case dmxCHAR_CLASS:
		case dmxLOGICAL_CLASS:	esize = 1; break;
		case dmxINT16_CLASS:
		case dmxUINT16_CLASS:	esize = 2; break;
		case dmxSINGLE_CLASS:
		case dmxINT32_CLASS:
		case dmxUINT32_CLASS:	esize = 4; break;
		case dmxDOUBLE_CLASS:	esize = 8; break;
		default: throw(0);
		}
		
		fread(&len, sizeof(unsigned short), 1, fp); rows = (int)len;
		fread(&len, sizeof(unsigned short), 1, fp); cols = (int)len;
		fread(data, rows*cols, esize, fp);
	}

	dynamic_data = true;
	return *this;
}
