
#include <stdio.h>

typedef enum {
	dmxUNKNOWN_CLASS = 0,
	dmxCELL_CLASS,
	dmxSTRUCT_CLASS,
	dmxLOGICAL_CLASS,
	dmxCHAR_CLASS,
	dmxDOUBLE_CLASS,
	dmxSINGLE_CLASS,
	dmxINT8_CLASS,
	dmxUINT8_CLASS,
	dmxINT16_CLASS,
	dmxUINT16_CLASS,
	dmxINT32_CLASS,
	dmxUINT32_CLASS,
} DMX_TYPE;


class CStorage
{
public:
	CStorage(char *name, DMX_TYPE type);									// Struct/Cell
	CStorage(char *name, DMX_TYPE type, int cols, void * data);				// Vector
	CStorage(char *name, DMX_TYPE type, int cols, int rows, void * data);	// Matrix
	CStorage(const CStorage &copy);
	CStorage(FILE *fp);														// Create from file
	~CStorage();

	void *operator new   ( size_t );
	void  operator delete( void * );

	CStorage& operator = (const CStorage &);
	CStorage& operator <<(CStorage &);										// Valid only for Struct/Cell types

	const bool Valid();

	CStorage& operator[] (int index);
	CStorage& operator() (char* name);
	
	CStorage& operator = (         bool     );
	CStorage& operator = (         bool   * );
	CStorage& operator = (         char     );
	CStorage& operator = (         char   * );
	CStorage& operator = (unsigned char     );
	CStorage& operator = (unsigned char   * );
	CStorage& operator = (         double   );
	CStorage& operator = (         double * );
	CStorage& operator = (         float    );
	CStorage& operator = (         float  * );
	CStorage& operator = (         short    );
	CStorage& operator = (         short  * );
	CStorage& operator = (unsigned short    );
	CStorage& operator = (unsigned short  * );
	CStorage& operator = (         long     );
	CStorage& operator = (         long   * );
	CStorage& operator = (unsigned long     );
	CStorage& operator = (unsigned long   * );

	operator          bool     ();
	operator          bool   * ();
	operator          char     ();
	operator          char   * ();
	operator unsigned char     ();
	operator unsigned char   * ();
	operator          double   ();
	operator          double * ();
	operator          float    ();
	operator          float  * ();
	operator          short    ();
	operator          short  * ();
	operator unsigned short    ();
	operator unsigned short  * ();
	operator          long     ();
	operator          long   * ();
	operator unsigned long     ();
	operator unsigned long   * ();

	CStorage& Save(FILE * fp);
	CStorage& Load(FILE * fp);

protected:
	CStorage();																	// Empty Storage
	CStorage& AddSubling (const CStorage &);
	CStorage& _set(void *, DMX_TYPE);
	void *    _get(DMX_TYPE);
	CStorage& _first();								// Finds first non struct/cell storage
	CStorage* _last();								// Finds last child storage

private:
	void          * data;		// pointer to the data
	int				rows;		// size of the data (matrix)
	int				cols;
	char		  * name;		// variable name
	DMX_TYPE		type;

	CStorage	  * child;
	CStorage	  * subling;
	
	bool			dynamic;
	bool			dynamic_data;
};

