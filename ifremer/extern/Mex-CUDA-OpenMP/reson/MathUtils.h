/***************************************************************************
****************************************************************************

   MathUtils.h:  
   
   A collection of math utility functions, wrapped into a DLL.  Most are
   templatized so that they can work with int, float, double, etc.

   Copyright � 2004-2005
   RESON, Inc.
   Goleta, CA  93117
   All right reserved.

   Revision History:
   -------------------------------------------------------
   Date:    16-Dec-2004
   Author:  Jim King
   Change:  Initial Draft of DLL from original source by Eric M.
   
   Date:    23-May-2005
   Author:  Jim King
   Change:  Add Interp1() MatLab function.
   
***************************************************************************
**************************************************************************/

#if !defined(AFX_MATHUTILS_H__93AB3596_1772_AD12_0F5A_CC82FD4EC758__INCLUDED_)
#define AFX_MATHUTILS_H__93AB3596_1772_AD12_0F5A_CC82FD4EC758__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <math.h>

#ifdef MATHUTILS_EXPORTS
#define MATHUTILS_API __declspec(dllexport)
#else
#define MATHUTILS_API __declspec(dllimport)
#endif

#define     OP_NONE                    0
#define     OP_GREATER_THAN            1
#define     OP_LESS_THAN               2
#define     OP_EQUAL                   3
#define     OP_GREATER_THAN_OR_EQUAL   4
#define     OP_LESS_THAN_OR_EQUAL      5
#define     OP_NOT_EQUAL               6


MATHUTILS_API int   Round   (double x);
MATHUTILS_API double RoundWithPrecision (double x, double precision);
MATHUTILS_API void  Hist    (double *Data, int iLength, int iNumBins, int *piBinCount, double *pdBinLocation);
MATHUTILS_API void  Common  (int *piSet1, const int *piSet2);
MATHUTILS_API void  PolyFit1(const double *x, const double *y, int nx, double *p);
MATHUTILS_API void  PolyVal (const double *x, double *y, int n, double *p, int m);

/***************************************************************************
   For vector 'x' of length 'nx', return the smallest value.
***************************************************************************/
template<typename T>
T Vmin (T *x, int nx)
{
	T sx = x[0] ;
	for (int i = 1 ; i < nx ; i++)
		if (x[i] < sx)
			sx = x[i] ;
	return sx ;
}

/***************************************************************************
   For vector 'x' of length 'nx', return the largest value.
***************************************************************************/
template<typename T>
T Vmax (T *x, int nx)
{
	T sx = x[0] ;
	for (int i = 1 ; i < nx ; i++)
		if (x[i] > sx)
			sx = x[i] ;
	return sx ;
}

/***************************************************************************
   For vector 'x', of length 'nx', returns the mean value.  If 'nx' is <= 0,
   then returns zero.  Return value will ALWAYS be a double.
***************************************************************************/
template<typename T>
double Mean(T *X, int nX)
{	
	double m = 0.0;

	if (nX <= 0)
		return(0.0); 

	for (int i=0; i<nX; i++)
		m += X[i];

	return(m / nX);
}

/***************************************************************************
   For vector 'x', we return the mean (average) of all elements specified
   by the list of nX indices 'pIndices'.   If 'nX' is <= 0, then returns zero.  
   For example, pIndices may be [0, 3, 6, 12, 2, 35, 17, 22 ] and nX would
   be 8.  The length of X must be at least as long as the largest index in
   the pIndices array (plus 1).
   Return value will ALWAYS be a double.
***************************************************************************/
template<typename T>
double Mean(T *X, int *pIndices, int nX)
{	
	double m = 0.0;

	if (nX <= 0)
		return(0.0); 

	for (int i=0; i<nX; i++)
		m += X[*pIndices++];

	return(m / nX);
}

/***************************************************************************
   For vector 'x' of length 'nx', return the standard deviation of the
   values.  If 'nx' is <= 1, then return value is zero.  Return value will
   ALWAYS be a double, regardless of type of 'x'.
***************************************************************************/
/***************************************************************************
   For vector 'x' of length 'nx', return the standard deviation of the
   values.  If 'nx' is <= 1, then return value is zero.  Return value will
   ALWAYS be a double, regardless of type of 'x'.
***************************************************************************/
template<typename T>
double Std (T *X, int nX, double *M = NULL)
{
	double std = 0.0;
	int i;

	if (nX <= 1)
		return 0;

	double m = Mean(X, nX);
	if (M) *M = m;
	T * x = X;

	for (i=0; i<nX; i++, x++)
		std += (*x-m)*(*x-m);

	std = sqrt(std / (double) (nX - 1));

	return(std);
}

/***************************************************************************
   For vector 'x' of length 'nx', return the max value found (in 'vmx') and
   the index of that value (in 'imx').
   Either 'vmx' or 'imx' may be NULL if that info is not needed.
***************************************************************************/
template<typename T>
void Fmax (T *X, int nX, T *vmx, int *imx)
{
   T   max       = X[0];
   int iMaxIndex = 0;

	for (int i=1; i<nX; i++) {
		if (X[i] > max) {
         max       = X[i];
         iMaxIndex = i;
		}
	}
   if (vmx) *vmx = max;
   if (imx) *imx = iMaxIndex;
}


/***************************************************************************
   For vector 'x' of length 'nx', return the min value found (in 'vmx') and
   the index of that value (in 'imx').
   Either 'vmin' or 'imin' may be NULL if that info is not needed.
***************************************************************************/
template<typename T>
void Fmin (T *X, int nX, T *vmin, int *imin)
{
   T   min       = X[0];
   int iMinIndex = 0;

	for (int i=1; i<nX; i++) {
		if (X[i] < min) {
         min       = X[i];
         iMinIndex = i;
		}
	}
   if (vmin) *vmin = min;
   if (imin) *imin = iMinIndex;
}


/***************************************************************************
   Return sum of elments of vector 'x' of length 'nx'.
***************************************************************************/
template<typename T> 
T Sum (T *X, int nX)
{
	T sum = 0; 
	for (int i=0; i<nX; i++) 
		sum += X[i] ;

	return(sum);
}

/***************************************************************************
   Return the Euclidian length of vector 'x'.  See Matlab documentation
   for the "norm" function for more details.
***************************************************************************/
template<typename T> 
double Norm (T *X, int nX)
{
   double dSum = 0.0;
   int    i;

   for (i=0; i<nX; i++)
      dSum += X[i] * X[i];
   return(sqrt(dSum));
}


/***************************************************************************
   Construct a 2-dimensional array of type 'T', using single allocation.
***************************************************************************/
template<typename T>
T **_2DArrayAlloc (int x, int y, T initValue, bool bInit)
{
   int i;
   DWORD dwSize = (sizeof(T*) * x) + (x * y * sizeof(T));
   T   **Buf = (T**) new char[dwSize];
   T    *Ptr;
   if (!Buf)
      return(NULL);
                                          // set ptr to first COLUMN
   Ptr = (T *) (&Buf[x]);
 
   for (i=0; i<x; i++,Ptr+=y)             // set up column pointers
      Buf[i] = Ptr;

   if (bInit) {
      for (int i=0; i<x; i++)             // loop thru rows
         for (int j=0; j<y; j++)          // loop thru columns
            Buf[i][j] = initValue;
   }
   return(Buf);
}


/***************************************************************************
   Find max value in 2-dimensional array of elements of type 'T'.  
   'x' is primary dimension, 'y' is secondary dimension.  I.e. 'Data' is
   an x by y array.  Optionally return x,y coordinate of value in 'peak',
   which is NULL or array or int[2].
***************************************************************************/
template<typename T>
T Max2 (T **Data, int x, int y, int *peak)
{
   int i, j,
       iMax = 0,
       jMax = 0;
   T max = Data[0][0];

   for (i=0; i<x; i++)
      for (j=0; j<y; j++)
         if (Data[i][j] > max) {
            max = Data[i][j];
            iMax = i;
            jMax = j;
         }
          
   if (peak) {                            // if 'peak' not NULL, return x,y
      peak[0] = iMax;                     // coordinate of the max value found.
      peak[1] = jMax;
   }
        
   return(max);
}


/***************************************************************************
   Implement MatLab Find() function w/ binary logical expression.  I.e. 
   piIndices will be filled in with the indices of the values in Data[] for 
   which the expression (Data[i] iLogicalOp Operand) is true.  iLogicalOp 
   can be '<', '>' '=', etc.
   See MatLab docuemntation for more details.
***************************************************************************/
template<typename T>
void  Find (const T *Data, int iLength, int *piIndices, int iLogicalOp, T Operand, int *piNumFound = NULL)
{
   int i, j = 0;

   for (i=0; i<iLength; i++) {
      switch (iLogicalOp) {
         case OP_GREATER_THAN :
            if (Data[i] > Operand)
               piIndices[j++] = i;
            break;
         case OP_LESS_THAN :
            if (Data[i] < Operand)
               piIndices[j++] = i;
            break;
         case OP_EQUAL :               // may be dodgy for floating point
            if (Data[i] == Operand)
               piIndices[j++] = i;
            break;
         case OP_GREATER_THAN_OR_EQUAL :
            if (Data[i] >= Operand)
               piIndices[j++] = i;
            break;
         case OP_LESS_THAN_OR_EQUAL :
            if (Data[i] <= Operand)
               piIndices[j++] = i;
            break;
         case OP_NOT_EQUAL :           // may be dodgy for floating point
            if (Data[i] != Operand)
               piIndices[j++] = i;
            break;
      }
   }
   piIndices[j] = -1;                  // minus one terminates list

   if (piNumFound)                     // optionally return number found
      *piNumFound = j;
}


/***************************************************************************
   Implements MatLab yi = interp1(x,Y,xi) function.  Returns yy[] array, the
   result of interpolating all of the xx[] values on the linear "function" 
   defined by points defined by (x,y).
   'ni' is the length of the x and y arrays, which must be the same length,
   and must be >= 2.  (TODO - ni not used! OK??)
   'nj' is the length of the xi[] array.  yi[] array must be at least
   this long to receive the results (for each value in xi[]).
***************************************************************************/
template<typename T>
/*
int   Interp1 (const T *x, const T *Y, const T *xi, int iLen, int iLenXi, T *yi)
{
   int i, j;

   if (iLen < 2)                          // need at least 2 points!
      return(1);
                                          // loop through all pts in xi[]
   for (i=0; i<iLenXi; i++) {
      j = 1;
      while (xi[i] > x[j] && j<iLen-1)    // find segment to interpolate in
         j++;
                                          // interpolate between points j-1 and j by
                                          // getting slope and intercept of that segment
                                          // and plugging into y = mx + b formula
      if (fabs(x[j] - x[j-1]) > FLT_MIN) {
         T dSlope     = (Y[j] - Y[j-1]) / (x[j] - x[j-1]);
         T dIntercept = Y[j-1] - (x[j-1] * dSlope); 
         yi[i] = (dSlope * xi[i]) + dIntercept;
      }
      else                                // for infinte slope (vertical) segment, we will
                                          // just assign avg of two Y values.
         yi[i] = (Y[j] + Y[j-1]) / 2.0;
   }
   return(0);
}
*/

// linear interpolation - Eric's method
void Interp1 (T *x, T *y, int ni, T *xx, T *yy, int nj)
{
	int i ;
	int next = 1 ;
	int prev ;
	float slope;

	for (i = 0 ; i < nj ; i++)
	{
      // MODIFIED JSK 22-Aug-06 - prevent next from overindexing x[]
		while (x[next] < xx[i] && next < ni-1) {
			next++ ;
      }
		prev = next - 1 ;
      // MODIFIED JSK 22-Aug-06 - protect against DIV 0 error
      if (fabs(x[next]-x[prev]) < 1e-10)
	  {
         yy[i] = 0;
	  }
      else
	  {
		// !ALTRAN - 101 begin
		//yy[i] = (y[next] * (xx[i]-x[prev]) + y[prev] * (x[next]-xx[i])) / (x[next]-x[prev]) ;
		slope = (y[next]-y[prev])/(x[next]-x[prev]);
		yy[i] =  y[prev] +(slope * (xx[i]-x[prev]));
		// !ALTRAN end
	  }
	}
}



/***************************************************************************
   Implement array transpose operation.  Equivalent in Matlab to B = A'
   'x' is array to be transposed, 'y' is result.  'x' and 'y' must be ptrs
   to buffers of nx1 * nx2 elements of type 'T'.  Row major ordering.
***************************************************************************/
template<typename T>
void  Transp (T *x, T *y, int nx1, int nx2)
{
	int i, j ;

	for( i = 0 ; i < nx1 ; i++ )
		for (j = 0 ; j < nx2 ; j++ )
			*(y + j * nx1 + i) = *(x + i * nx2 + j) ;
}

/***************************************************************************
   Compare function used by qsort() in Median() implemenation.

mcompare needs to be instantiated for the type you using (as a part of the template)

  For Example:

template<> int mcompare<double>(const void *arg1, const void *arg2)
{
	return ( *(double*) arg1 > *(double*) arg2) ? (1) :
		   ( *(double*) arg1 < *(double*) arg2) ? (-1) : 0;
}

template<> int mcompare<float>  - defined in TargetStrength.cpp
template<> int mcompare<double> - defined in CalibrationCalc.cpp

***************************************************************************/
typedef int COMPARE (const void *arg1, const void *arg2);

/***************************************************************************
   Implement Median functions. The data array is altered (sorted) by this function!!! 
   Does NOT support 64bit quantities!
***************************************************************************/
template<typename T>
T Median (T *x, int nx, COMPARE compare)
{
   qsort((void *) x, nx, sizeof(T), compare);
    
   if ( nx % 2 == 1 ) 
      return(x[nx / 2]);

   return((x[(nx - 1) / 2] + x[nx / 2]) / 2.0);
}

/***************************************************************************
   Implement filter1c functions.
   Does NOT support 64bit quantities!
***************************************************************************/
template<typename T>
void  filter1c (T *b, int nb, T *x, int nx, T *y)
{
   int n, m;
                                          // perform FIR b on vector x
   for (n=0; n<nx; n++) {
      y[n] = 0;

      for (m=0; m<nb; m++) {
         if (n-m >= 0) {
            y[n] += b[m] * x[n-m];
         }
      }
   }
}


#endif
