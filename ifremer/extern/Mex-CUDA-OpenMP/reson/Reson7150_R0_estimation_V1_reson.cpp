#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double *Dsample_start;
    WORD sample_start;
    double* DSeuilAmp;
    float SeuilAmp;
    double *EndSample;
    double *Rmin;
    double *Rmax;
    float * fAmpReduite;
    double* outputDetection;
	unsigned int nan;

    // Get the input parameters
	m_ReduiteStep = (int)(*(mxGetPr(prhs[7])));
    fAmpReduite = (float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);    
	m_ReduiteSize = mxGetM(prhs[0]);
	m_pAmpReduite = _2DArrayAlloc(m_dwReceiveBeams, m_ReduiteSize,(float)0.0f, true);
    for (int iB=0; iB<m_dwReceiveBeams; iB++){
        for (int iS=0; iS < m_ReduiteSize; iS++) {
			if(fAmpReduite[(iB*m_ReduiteSize) +iS] == fAmpReduite[(iB*m_ReduiteSize) +iS])
			{
				// Not NAN
				m_pAmpReduite[iB][iS] = fAmpReduite[(iB*m_ReduiteSize) +iS];
			}
			else
			{
				// NAN
				NANf(m_pAmpReduite[iB][iS]);
			}
        }
    }
    m_nStrips = (int)(*(mxGetPr(prhs[1])));
    Dsample_start = mxGetPr(prhs[2]);
    sample_start = ((WORD)(*Dsample_start) - 1);
    EndSample = mxGetPr(prhs[3]);
    DSeuilAmp = mxGetPr(prhs[4]);
    SeuilAmp = (float)(*DSeuilAmp);
    Rmin = mxGetPr(prhs[5]);
    Rmax = mxGetPr(prhs[6]);
    
    // Initializations
    m_pR1 = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsB = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pGatesMin = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pGatesMax = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++) m_pGatesMin[i] = (float)((Rmin[i] * m_ReduiteStep) -1.0);
    for(int i=0; i<m_dwReceiveBeams;i++) m_pGatesMax[i] = (float)((Rmax[i] * m_ReduiteStep) -1.0);
    for(int i=0; i<m_dwReceiveBeams;i++) 
	{
		if(EndSample[i] == EndSample[i])
		{
			// Not NAN
			m_pR1[i] = (DWORD)(EndSample[i]-1.0);
		}
		else
		{
			// NAN
			NANf(m_pR1[i]);
		}
	}
    m_pfBeamsA = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pFlagBeams = (bool *)mxCalloc(m_dwReceiveBeams, sizeof(bool));
    m_pdBeamsA = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsB = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsC = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsD = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pfSamplesA = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesB = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesC = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesD = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesE = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfBeamsC = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsD = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
        

    // Call the C function _barycentre
    _r0_estimation(sample_start, SeuilAmp);
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    outputDetection[0] = m_R0+1.0;
    plhs[1] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
    for (int iB=0; iB < m_dwReceiveBeams; iB++) {
        outputDetection[iB] = (double)(m_pR1[iB]+1.0);
    }
    plhs[2] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[2]);
    for (int iB=0; iB < m_dwReceiveBeams; iB++) {
        outputDetection[iB] = (double)(m_pFlagBeams[iB]);
    }

	// free memory
	delete(m_pAmpReduite);
	m_pAmpReduite = 0;
    mxFree(m_pR1);
	m_pR1 = 0;
    mxFree(m_pfBeamsB);
	m_pfBeamsB = 0;
    mxFree(m_pGatesMin);
	m_pGatesMin = 0;
    mxFree(m_pGatesMax);
	m_pGatesMax = 0;
    mxFree(m_pfBeamsA);
	m_pfBeamsA = 0;
    mxFree(m_pFlagBeams);
	m_pFlagBeams = 0;
    mxFree(m_pdBeamsB);
	m_pdBeamsB = 0;
    mxFree(m_pdBeamsC);
	m_pdBeamsC = 0;
    mxFree(m_pdBeamsD);
	m_pdBeamsD = 0;
    mxFree(m_pfSamplesA);
	m_pdBeamsD = 0;
    mxFree(m_pfSamplesB);
	m_pfSamplesB = 0;
    mxFree(m_pfSamplesC);
	m_pfSamplesC = 0;
    mxFree(m_pfSamplesD);
	m_pfSamplesD = 0;
    mxFree(m_pfSamplesE);
	m_pfSamplesE = 0;
    mxFree(m_pfBeamsC);
	m_pfBeamsC = 0;
    mxFree(m_pfBeamsD);
	m_pfBeamsD = 0;

}
