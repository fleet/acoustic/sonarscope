#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* dIn;
    DWORD * subSamples;
    float * fIn;
    int dataSize;
    double* outputDetection;
	float * Phase;
	float * Amp;
	float * Amp2;
	int nSamples;
	float R;
	float QF;
	float Pente;
	float Eqm;
	int NbSamples;
	float NechRes;
	int Beam;
	int iter;
	bool result;
	float sampleRate;
	float val;

    // Get the input parameters
    //p_iBeam = (double*)(mxGetPr(prhs[0]));
    //iBeam = (int)(*p_iBeam)-1;
    
    dIn=(double*)mxGetPr(prhs[0]);
    dataSize = (int)mxGetM(prhs[0]);
    if(dataSize == 1){
        dataSize = (int)mxGetN(prhs[0]);
    }
    subSamples = (DWORD *)mxCalloc(dataSize, sizeof(DWORD));
    for(int i=0; i<dataSize;i++)
    {
        subSamples[i] = (DWORD)(dIn[i]);
    }

	Phase = (float *)mxCalloc(dataSize, sizeof(float));
    for(int i=0; i<dataSize;i++)
    {
        Phase[i] = ((float*)mxGetPr(prhs[1]))[i];
    }

	Amp = (float *)mxCalloc(dataSize, sizeof(float));
    for(int i=0; i<dataSize;i++)
    {
		Amp[i] = ((float*)mxGetPr(prhs[2]))[i];
    }

	Amp2 = (float *)mxCalloc(dataSize, sizeof(float));
    for(int i=0; i<dataSize;i++)
    {
		Amp2[i] = (Amp[i])*(Amp[i]);
    }

	dIn = (double*)mxGetPr(prhs[4]);
	m_R0 = (float)(*dIn)-1;

	dIn = (double*)mxGetPr(prhs[6]);
	iter = (int)(*dIn);

	dIn = (double*)mxGetPr(prhs[8]);
	Beam = (int)(*dIn)-1;

	dIn = (double*)mxGetPr(prhs[10]);
	NechRes = (float)(*dIn);

	sampleRate = (float)(*(mxGetPr(prhs[12])));

	m_dTPulse = (float)(*(mxGetPr(prhs[13])));
	m_dTPulse = (m_dTPulse*sampleRate)/sqrtf(12.0);
	
            
    // Initializations
	nSamples = dataSize;
	m_pfSamplesD = (float *)mxCalloc(dataSize, sizeof(float));
	m_pfSamplesE = (float *)mxCalloc(dataSize, sizeof(float));
        
    // Call the C function _barycentre
    result = _phase_detection_beam_single( subSamples, Phase, Amp, Amp2, iter, 
								nSamples, Beam, NechRes,
								R, QF, Pente, Eqm, NbSamples);
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    outputDetection[0] = (double)result;    
    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
    outputDetection[0] = (float)R;    
    plhs[2] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[2]);
    outputDetection[0] = (float)QF;    
    plhs[3] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[3]);
    outputDetection[0] = (float)Pente;    
    plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[4]);
    outputDetection[0] = (float)Eqm;    
    plhs[5] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[5]);
    outputDetection[0] = (float)NbSamples;    
    plhs[6] = mxCreateDoubleMatrix(nSamples,1,mxREAL);
    outputDetection = mxGetPr(plhs[6]);
    for(int i=0; i<nSamples;i++)
    {
		outputDetection[i] = (float)(subSamples[i]); 
	}
    plhs[7] = mxCreateDoubleMatrix(nSamples,1,mxREAL);
    outputDetection = mxGetPr(plhs[7]);
    for(int i=0; i<nSamples;i++)
    {
		outputDetection[i] = (float)(Phase[i]); 
	}
    plhs[8] = mxCreateDoubleMatrix(nSamples,1,mxREAL);
    outputDetection = mxGetPr(plhs[8]);
    for(int i=0; i<nSamples;i++)
    {
		outputDetection[i] = (float)(Amp[i]); 
	}
    plhs[9] = mxCreateDoubleMatrix(nSamples,1,mxREAL);
    outputDetection = mxGetPr(plhs[9]);
    for(int i=0; i<nSamples;i++)
    {
		outputDetection[i] = (float)(Amp2[i]); 
	}

	// free memory
	mxFree(subSamples); 
	subSamples = 0;
	mxFree(Phase); 
	Phase = 0;
	mxFree(Amp); 
	Amp = 0;
	mxFree(Amp2); 
	Amp2 = 0;
	mxFree(m_pfSamplesD); 
	m_pfSamplesD = 0;
	mxFree(m_pfSamplesE); 
	m_pfSamplesE = 0;

}
