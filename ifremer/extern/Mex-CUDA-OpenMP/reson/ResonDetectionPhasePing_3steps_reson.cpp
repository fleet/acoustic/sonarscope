#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* dIn;
    DWORD * subSamples;
    float * fIn;
    int dataSize;
    double* outputDetection;
	float * Phase;
	float * Amp;
	float * Amp2;
	int nSamples;
	float R;
	float QF;
	float Pente;
	float Eqm;
	int NbSamples;
	float NechRes;
	int Beam;
	int iter;
	bool result;
	float sampleRate;
	double* inputData;

    // Get the input parameters
    //p_iBeam = (double*)(mxGetPr(prhs[0]));
    //iBeam = (int)(*p_iBeam)-1;
    
    dIn=(double*)mxGetPr(prhs[0]);
    dataSize = (int)mxGetM(prhs[0]);
    if(dataSize == 1){
        dataSize = (int)mxGetN(prhs[0]);
    }
    subSamples = (DWORD *)mxCalloc(dataSize, sizeof(DWORD));
    for(int i=0; i<dataSize;i++)
    {
        subSamples[i] = (DWORD)(dIn[i]);
    }

	Phase = (float *)mxCalloc(dataSize, sizeof(float));
    for(int i=0; i<dataSize;i++)
    {
        Phase[i] = ((float*)mxGetPr(prhs[1]))[i];
    }

	Amp = (float *)mxCalloc(dataSize, sizeof(float));
    for(int i=0; i<dataSize;i++)
    {
		Amp[i] = ((float*)mxGetPr(prhs[2]))[i];
    }

	Amp2 = (float *)mxCalloc(dataSize, sizeof(float));
    for(int i=0; i<dataSize;i++)
    {
        Amp2[i] = (Amp[i])*(Amp[i]);
    }

	dIn = (double*)mxGetPr(prhs[4]);
	Beam = (int)(*dIn)-1;

	dIn = (double*)mxGetPr(prhs[7]);
	m_DeltaTeta = (float)(*dIn);
	m_DeltaTeta = m_DeltaTeta/180.0*PIf;

	dIn = (double*)mxGetPr(prhs[8]);
	m_R0 = (float)(*dIn)-1;

	dIn = (double*)mxGetPr(prhs[9]);
	m_dwReceiveBeams = (DWORD)(*dIn);

	sampleRate = (float)(*(mxGetPr(prhs[11])));

	m_dTPulse = (float)(*(mxGetPr(prhs[12])));
	m_dTPulse = (m_dTPulse*sampleRate)/sqrtf(12.0);
	
	m_pBeamAngles = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	inputData = mxGetPr(prhs[13]);
	for(int i=0;i<m_dwReceiveBeams;i++){
		m_pBeamAngles[i] = (float)(inputData[i]*PIf/180);
	}
            
    // Initializations
	nSamples = dataSize;
	m_pfSamplesD = (float *)mxCalloc(100000, sizeof(float));
	m_pfSamplesE = (float *)mxCalloc(100000, sizeof(float));
	m_pQualityPhase = (float *)mxCalloc(100000, sizeof(float));
	m_pERMPhase = (float *)mxCalloc(100000, sizeof(float));
	m_pSlopePhase = (float *)mxCalloc(100000, sizeof(float));
	m_pSamplesPhase = (DWORD *)mxCalloc(100000, sizeof(DWORD));
	m_pRangePhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));

        
    // Call the C function _barycentre
    result = _phase_detection_beam(subSamples, Amp, Amp2, Phase, dataSize, Beam);
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    outputDetection[0] = (double)(m_pRangePhase[Beam]+1);    

	plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
    outputDetection[0] = (double)m_pQualityPhase[Beam];    

	plhs[2] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[2]);
    outputDetection[0] = (double)m_pSlopePhase[Beam];    

	plhs[3] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[3]);
    outputDetection[0] = (double)m_pSamplesPhase[Beam];    

	plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[4]);
    outputDetection[0] = (double)m_pERMPhase[Beam];  

	// free memory
	mxFree(subSamples); 
	subSamples = 0;
	mxFree(Phase);
	Phase = 0;
	mxFree(Amp);
	Amp = 0;
	mxFree(Amp2);
	Amp2 = 0;
	mxFree(m_pBeamAngles);
	m_pBeamAngles = 0;
	mxFree(m_pfSamplesD);
	m_pfSamplesD = 0;
	mxFree(m_pfSamplesE);
	m_pfSamplesE = 0;
	mxFree(m_pQualityPhase);
	m_pQualityPhase = 0;
	mxFree(m_pERMPhase);
	m_pERMPhase = 0;
	mxFree(m_pSlopePhase);
	m_pSlopePhase = 0;
	mxFree(m_pSamplesPhase);
	m_pSamplesPhase = 0;
	mxFree(m_pRangePhase);
	m_pRangePhase = 0;

}
