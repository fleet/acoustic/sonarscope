#include "math.h"
#include "string.h"
#include "MathUtils.h"
#include <float.h>

#define ISNAN(a)	(a == 0xffffffff)
#define ISNANf(a)	(*(DWORD*)&(a) == 0xffffffff)
#define NAN(a)		a = 0xffffffff
#define NANf(a)		*(DWORD*)&(a) = 0xffffffff
#define REDUCTION_SAMPLE_SIZE 200
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#define REDUCTION_MATRIX_SIZE 210
#define ELEM_SWAP(a,b) { t=(a);(a)=(b);(b)=t; }
#define ISEQUAL(a,b)    (fabsf((a)-(b)) < FLT_EPSILON*2)
#define INFS(a,b)   a = 0x7f800000 | (b & 0x80000000)
#define DEBUG_OUTPUT_PREPROC_LONG		0x0008
#define DEBUG_OUTPUT_MASKAMP_LONG		0x0020
#define D2Rf(a) ((a) * PIf / 180.0f)
#define PIf (3.1415926535f)
#define PHS2DEG_FLIP(p) (((p) == -32768) ? 180.0f : (p)*5.4931640625e-003f)
#define PHS2RAD(p) ((p)*9.587379924285257e-005f)
#define R2Df(a) ((a) * 180.0f / PIf)
#define limit(x, lo, hi)  ((x) < (lo) ? (lo) : ((x) > (hi) ? (hi) : (x)))
#define between(x, lo, hi)  ((x) >= (lo) && (x) <= (hi))
#define IF1_QUALITY_QF2_PASS	0x1
#define IF1_DETECTION_PHASE		0x8
#define IF1_DETECTION_AMP		0x4


typedef unsigned long DWORD;
typedef unsigned short int WORD;
typedef struct  
{
	unsigned short amp;
	         short phs;
} _raw_data_type;

double* m_pfdSamples;                   // [TEMPORARY] Union with m_pfSamplesG and m_pfSamplesH
DWORD m_dwDeviceId;                     // [COPY]      Device ID
float m_fCenterFrequency = 12500;       // [COPY]      Tx/Rx Center Frequency
float m_N0;
float m_N0Corr;
DWORD* m_pAmpWinMin, * m_pAmpWinMax;    // [OUTPUT]    Amplitude Detection Window.
float* m_pRangeAmplitude;               // [INTERNAL]  Amplitude Detection. In Samples, per Beam
float* m_pQualityAmplitude;             // [INTERNAL]  Quality of Amplitude Detection. Per Beam
int    m_ReduiteStep;                   // [INTERNAL]  Step for Amplitude series reduction
int    m_iOutputSamples;                // [COPY]      Number of Samples in the ping
WORD    m_ReduiteSize;                  // [INTERNAL]  Number of Samples in Amplitude series reduction
DWORD   m_dwReceiveBeams;               // [COPY]      Number of Beams
float ** m_pAmpReduite;                 // [INTERNAL]  Reduced Amplitude series
float ** m_pfReduite;                   // [TEMPORARY] Reduced Series (Beams * REDUCTION_SAMPLE_SIZE float matrix)
int      m_nStrips;						// [INTERNAL]  Number of strips for mask processing
float *  m_pR1;							// [INTERNAL]  Approximate Center of the Signal. Per Beam in Samples
float *  m_pR2;							// [INTERNAL]  Approximate Center of the Signal. Per Beam in Samples

// !ALTRAN - 29 Begin
float *  m_pMax;						// [INTERNAL]  max of the Signal. Per Beam in Samples
int m_iBeamBeg;                  		// [INTERNAL]  max of the Signal. Per Beam in Samples
int m_iBeamEnd;							// [INTERNAL]  max of the Signal. Per Beam in Samples
char  * m_pRAWData;                     // [REFERENCE] Amplitude & Phase series.										// Angle coded in short value (-max short to +max short) -> (-180° +180°)
// !ALTRAN End

float * m_pfBeamsA;                     // [TEMPORARY] Per Beam   float vector
float * m_pfBeamsB;                     // [TEMPORARY] Per Beam   float vector
float    m_R0;							// [INTERNAL]  First appearance of the signal. In Samples
bool  *  m_pFlagBeams;					// [INTERNAL]  Estimation validity. Per Beam.

// !ALTRAN - 1 Begin
float * m_pGatesMin, * m_pGatesMax;		// [INPUT]     Absolute limits for detection.   Per Beam in Samples
// !ALTRAN End

// !ALTRAN - 41 Begin
int     m_IdentAlgo;
// !ALTRAN End

DWORD * m_pdBeamsA;						// [TEMPORARY] Per Beam   DWORD vector
DWORD * m_pdBeamsB;						// [TEMPORARY] Per Beam   DWORD vector
DWORD * m_pdBeamsC;						// [TEMPORARY] Per Beam   DWORD vector
DWORD * m_pdBeamsD;						// [TEMPORARY] Per Beam   DWORD vector
float * m_pfSamplesA;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE)
float * m_pfSamplesB;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
float * m_pfSamplesC;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
float * m_pfSamplesD;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
float * m_pfSamplesE;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
float * m_pfBeamsC;						// [TEMPORARY] Per Beam   float vector
float * m_pfBeamsD;						// [TEMPORARY] Per Beam   float vector
float *  m_pRangePhase;					// [INTERNAL]  Phase Detection. In Samples, per Beam
int  m_Debug;
WORD     m_IncidentBeam;				// [INTERNAL]  Number of the Beam of Normal Incidence on Sea Floor
char  *  m_pDetectionType;				// [INTERNAL]  Detection Type. Per Beam
DWORD *  m_pMaskWidth;					// [INTERNAL]  Calculated limits for detection. Per Beam in Samples
float ** m_pMatrix;						// [INTERNAL]  Phase series in degrees (FLT_MAX == NaN)
DWORD *  m_pMaskMin,  * m_pMaskMax;		// [OUTPUT]    Calculated limits for detection. Per Beam in Samples
int      m_MaskMin,     m_MaskMax;		// [INTERNAL]  Min and Max Samples included in the mask for all beams
float	m_fReceiveBeamWidth;			// [COPY]      Rx Beam Width in radians
float * m_pBeamAngles;					// [REFERENCE] Beam Angles. Per Beam in radians
float	m_fTransmitPulseLength;			// [COPY]      Tx Pulse Length in seconds
float   m_fSampleRate;					// [COPY]      Rx Sampling Rate
DWORD   m_nMaskSamples;					// [INTERNAL]  Number of Allocated Samples. Used to reduce number of allocations.
float	m_dTPulse;
float * m_pQualityPhase;				// [INTERNAL]  Quality of Phase Detection. Per Beam
float * m_pERMPhase;					// [INTERNAL]  Mean Square Error for Phase Detection. Per Beam
float * m_pSlopePhase;					// [INTERNAL]  Slope for Phase Detection. Per Beam
DWORD * m_pSamplesPhase;				// [INTERNAL]  Number of samples used for Phase Detection, per Beam
float   m_DeltaTeta;
float * m_pDetectionRange;				// [INTERNAL]  Bottom Detection. In Samples, per Beam
DWORD *  m_pSamplesAmp;					// [INTERNAL]  Number of samples used for Amplitude Detection, per Beam
float *  m_pQF2Amplitude;				// [INTERNAL]  Amplitude Detection QF2. Per Beam
bool  *  m_pNominalBeams;				// [INTERNAL]  Nominal beams
float *  m_pDetectionQuality;			// [INTERNAL]  Quality of Detection. Per Beam

//------------------------------------------------------------------------------
// variables for sonar_BDA_reson mex function
//------------------------------------------------------------------------------
#define PI 3.141592653589793f
#define D2SR(D,B,A)  (((D) * 2.0f * m_fSampleRate) / (cosf(m_pBeamAngles[(B)]+(A)) * 1500.0f))

DWORD *  m_pPhsWinMin, * m_pPhsWinMax;	// [OUTPUT]    Phase Detection Window.
float * m_pfSamplesF;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
float * m_pfSamplesG;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
float * m_pfSamplesH;					// [TEMPORARY] Per Sample float vector (min.size = REDUCTION_SAMPLE_SIZE -- ONLY?)
DWORD * m_pdSamplesA;					// [TEMPORARY] Per Sample DWORD vector
DWORD * m_pdSamplesB;					// [TEMPORARY] Per Sample DWORD vector
float   m_fSoundVelocity;				// [COPY]      Surface Sound Velocity
DWORD   m_nAllocSamples;				// [INTERNAL]  Number of Allocated Samples. Used to reduce number of allocations.
float    m_NechRes0;
double   m_B;
float   m_fAbsorption;					// [INTERNAL]  Nominal Absorption

//------------------------------------------------------------------------------
// Calculates standard deviation
//
// Arguments:
// [IN]  X      - input array
// [IN]  nX     - size of X array and filter
// [IN]  filter - "NaN filter" with false corresponding to NaN elements.
//
// Return Value: STD.
//
float _nanStd (const float *X, const int nX, const DWORD * filter)
{
	float std = 0.0;
	int i, np = nX;
	
	if (nX <= 1)
		return 0;
	
	float m = 0.0f;

	for (i=0; i<nX; i++)
	{
		if (!(filter[i]))
		{
			np--;
		}
		else
		{
			m += X[i];
		}
	}

	m /= np;

	for (i=0; i<nX; i++)
	{
		if (filter[i])
		{
			std += (X[i]-m)*(X[i]-m);
		}
	}
	
	std = (float)sqrt(std / (double) (np - 1));
	
	return(std);
}


//------------------------------------------------------------------------------
// Calculates mean of the vector, ignoring NaN
//
// Arguments:
// [IN]  X      - input array.
// [IN]  nX     - size of X and S arrays
//
// Return Value: mean of X
//
float _nanmean(const float *X, const int nX)
{	
	const DWORD * nanX = (DWORD *)X;
	float m = 0.0;
	int nI = 0;
	
	if (nX <= 0)
		return(0.0f); 
	
	for (int i=0; i<nX; i++)
	{
		if (!ISNAN(nanX[i]))
		{
			m += X[i];
			nI++;
		}
	}

	if (!nI)
	{
		NANf(m);
		return m;
	}

	return(m / nI);
}

bool MasquePhs(int beam, int sample)
{
	DWORD *Mask = (DWORD *)m_pfReduite[0];
	
	return (Mask[beam*m_nMaskSamples+(sample >> 5)] & (1 << (sample & 0x1f))) ? true : false;
}


void MasquePhs(int beam, int sample, bool value)
{
	DWORD *Mask = (DWORD *)m_pfReduite[0];

	if (sample == -1) {
		char mask0 = (value) ? 0xff : 0;
		memset (Mask + beam*m_nMaskSamples, mask0, m_nMaskSamples * sizeof(DWORD));
	} else {
		if (value)
			Mask[beam*m_nMaskSamples+(sample >> 5)] |= 1 << (sample & 0x1f);
		else
			Mask[beam*m_nMaskSamples+(sample >> 5)] &= ~(1 << (sample & 0x1f));
	}
}


void MasquePhs(bool value)
{
	DWORD mask = (value) ? 0xff : 0;
	memset(m_pfReduite[0], mask, sizeof(float) * m_nMaskSamples * m_dwReceiveBeams);
}


void MasquePhs(int beam, int sample1, int sample2, bool value)
{
	DWORD *Mask = (DWORD *)m_pfReduite[0];

	DWORD mask1 = 0xffffffff;
	mask1 >>= (sample1 & 0x1f);
	mask1 <<= (sample1 & 0x1f);
	
	DWORD mask2 = 0xffffffff;
	mask2 <<= (sample2 & 0x1f);
	mask2 >>= (sample2 & 0x1f);

	char mask0 = (value) ? 0xff : 0;

	if ((sample1 >> 5) == (sample2 >> 5))
	{
		mask1 = mask1 & mask2;

		if (value)
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] |= mask1;
		else
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] &= ~mask1;
	}
	else
	{
		if (value)
		{
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] |= mask1;
			Mask[beam*m_nMaskSamples+(sample2 >> 5)] |= mask2;
		}
		else
		{
			Mask[beam*m_nMaskSamples+(sample1 >> 5)] &= ~mask1;
			Mask[beam*m_nMaskSamples+(sample2 >> 5)] &= ~mask2;
		}
		
		memset (Mask + beam*m_nMaskSamples + (sample1 >> 5) + 1, mask0, ((sample2 >> 5) - (sample1 >> 5) - 1) * sizeof(DWORD));
	}
}


static int fcompare( const void *arg1, const void *arg2 )
{
	/* Compare all of both strings: */
	return (*(float*)arg1 == *(float*)arg2) ? 0 : (*(float*)arg1 < *(float*)arg2) ? -1 : 1;
}


static inline int floori (float x)
{
	return (int) floorf(x);
}


static inline DWORD floordw (float x)
{
	return (x<=0) ? 0 : (DWORD) floorf(x);
}


static inline DWORD floordw (double x)
{
	return (x<=0) ? 0 : (DWORD) floor(x);
}


static inline WORD floorw (float x)
{
	return (x<=0) ? 0 : (WORD) floorf(x);
}


static inline int ceili (float x)
{
	return (x<=0) ? 0 : (int) ceilf(x);
}


static inline DWORD ceildw (float x)
{
	return (x<=0) ? 0 : (DWORD) ceilf(x);
}


static inline DWORD ceildw (double x)
{
	return (x<=0) ? 0 : (DWORD) ceil(x);
}


static inline WORD ceilw (float x)
{
	return (x<=0) ? 0 : (WORD) ceilf(x);
}


//------------------------------------------------------------------------------
// Calculates gradient of the vector
//
// Arguments:
// [IN]  X      - input array
// [OUT] G      - computed gradient
// [IN]  nX     - size of X array and filter
//
void _gradient (const float *X, float *G, const int nX)
{
	DWORD * NaNX = (DWORD *)X;
	DWORD * NaNG = (DWORD *)G;

	if (nX == 1)
	{
		G[0] = 0;
		return;
	}

	if (ISNAN(NaNX[0]) || ISNAN(NaNX[1]))
		NAN(NaNG[0]);
	else
		G[0] = (X[1]-X[0]);

	if (ISNAN(NaNX[nX-1]) || ISNAN(NaNX[nX-2]))
		NAN(NaNG[nX-1]);
	else
		G[nX-1] = (X[nX-1]-X[nX-2]); 

	for (int i = 1; i < nX - 1; i++)
	{
		if (ISNAN(NaNX[i+1]) || ISNAN(NaNX[i-1]))
		{
			NAN(NaNG[i]);
		}
		else
		{
			G[i] = (X[i+1]-X[i-1])/2;
		}
	}
}


//------------------------------------------------------------------------------
// Calculates cumulative sum of the vector
//
// Arguments:
// [IN]  X      - input array.
// [OUT] S      - cumulative sum vector
// [IN]  nX     - size of X and S arrays
// [IN]  norm...- if true S is normalized between 0 and 1
//
// Return Value: original sum of X
//
template <class DATA> 
DATA _cumsum(const DATA *X, float *S, const int nX, const bool normalize)
{
	int iX;
	DATA cumSum = 0;
	float * iS;
	DWORD * iD;
	
	for (iX=0, iS = S; iX<nX; iX++, iS++)
	{
		// !ALTRAN - 33 Begin
		if(ISNANf(X[iX])) // cum only if not NAN
		{
			cumSum += 0;
		}
		else
		{
			cumSum += X[iX];
		}
		// !ALTRAN End
		*iS = (float) cumSum;
	}
	
	if (normalize && cumSum != 0)
	{
		for (iX=0, iS = S; iX<nX; iX++, iS++)
		{
			*iS /= cumSum;
		}
	}
	else if (normalize && cumSum == 0)
	{
		for (iX=0, iD = (DWORD *)S; iX<nX; iX++, iD++)
		{
			if (*iD == 0)
				NAN(*iD);
			else
				INFS(*iD, *iD);
		}
	}
	
	return cumSum;
}


//------------------------------------------------------------------------------
// Calculates mean of the vector
//
// Arguments:
// [IN]  X      - input array.
// [IN]  nX     - size of X and S arrays
//
// Return Value: mean of X
//
float _mean(const float *X, const int nX)
{	
	float m = 0.0;
	
	if (nX <= 0)
		return(0.0f); 
	
	for (int i=0; i<nX; i++)
	{
		m += X[i];
	}
	
	return(m / nX);
}

//------------------------------------------------------------------------------
// Calculates median value of the vector
//
// Arguments:
// [IN]  X      - input array. NOTE: array WILL be altered by the function if T is not provided
// [IN/OUT] T   - Temporal vector of size nX, will be used instead of X if needed
// [IN]  nX     - size of X array and filter
//
// Return Value: approximate Median. The value MAY fluctuate few elements from the central
//               does not work on small number of elements, but much faster then the regular
//               sorting median.
//
DWORD _median(DWORD *X, DWORD *T, int nX)
{
	register int i,j,l,m;
	register DWORD x, t;

	DWORD * XX;

	if (T)
	{
		memcpy (T, X, sizeof(DWORD)*nX);
		XX = T;
	}
	else
		XX = X;

	int k = nX/2;		// due to integer division rules, k=nX/2 for even nX, and k=(nX-1)/2 for odd nX
	l=0; m=nX-1;
	while (l<m) {
		x=XX[k];
		i=l;
		j=m;
		do {
			while (XX[i]<x) i++;
			while (x<XX[j]) j--;
			if (i<=j) {
				ELEM_SWAP(XX[i],XX[j]);
				i++; j--;
			}
		} while (i<=j);
		if (j<k) l=i;
		if (k<i) m=j;
	}
	return XX[k];
}

float _median(float *X, float *T, int nX)
{
	float * XX;
	
	if (T)
	{
		memcpy (T, X, sizeof(float)*nX);
		XX = T;
	}
	else
		XX = X;

	qsort((void *) XX, nX, sizeof(float), fcompare);

	if (nX%2 == 1)
		return XX[(nX-1)/2];
	else
		return (XX[(nX-1)/2] + XX[(nX+1)/2]) / 2.0f;

/*
	register i,j,l,m;
	register float x, t;
	
	float * XX;
	
	if (T)
	{
		memcpy (T, X, sizeof(float)*nX);
		XX = T;
	}
	else
		XX = X;
	
	int k = nX/2;		// due to integer division rules, k=nX/2 for even nX, and k=(nX-1)/2 for odd nX
	l=0; m=nX-1;
	while (l<m) {
		x=XX[k];
		i=l;
		j=m;
		do {
			while (XX[i]<x) i++;
			while (x<XX[j]) j--;
			if (i<=j) {
				ELEM_SWAP(XX[i],XX[j]);
				i++; j--;
			}
		} while (i<=j);
		if (j<k) l=i;
		if (k<i) m=j;
	}
	return XX[k];
*/
}


//------------------------------------------------------------------------------
// Calculates center of gravity and STD of the vector
//
// Arguments:
// [IN]  X      - input array.
// [IN]  MinX   - first continuous index
// [IN]  iX     - indices array.
// [IN]  nX     - size of X and iX arrays
// [OUT] Sigma  - STD
// [IN/OUT] T   - Temporal vector of size nX;
//
// Return Value: position of the center of gravity
//------------------------------------------------------------------------------

template <class DATA> 
float _barycentre(const DATA *X, const int MinX, const int nX, float &Sigma, float *T, int order = 2)
{
	int iX, iO;
	float *pT;
	
	float CumEnr = 0;
	
	
	for (iX=0, pT = T; iX<nX; iX++, pT++)
	{
		for (iO = 0, *pT = 1.0f; iO < order; iO++, *pT *= (float)X[iX]);		// Enr = Amp .^ 2;
		CumEnr += *pT;
	}

	// !ALTRAN - 22 Begin
	if(CumEnr == 0){
		Sigma=0;
		return -1;
	}
	// !ALTRAN end
	
	float BC0, BC = 0, M2 = 0, VX = 0;
	
	for (iX=0, pT = T; iX<nX; iX++, pT++)
	{
		*pT /= CumEnr;															// Enr = Enr / sum(Enr);
		BC0 = (iX + MinX) * (*pT);												
		BC += BC0;																// R0 = sum(R .* Enr);
	}

	for (iX=0, pT = T; iX<nX; iX++, pT++)
	{
		M2 = (iX + MinX) - BC;
		VX += M2 * M2 * (*pT);
	}
	
	if (VX > 0)
		Sigma = (float)sqrt(VX);
	else
		Sigma = -1.0f;

	// !ALTRAN - 46 Begin
	if(BC < MinX)
	{
		BC = MinX;
	}
	if(BC > MinX + nX)
	{
		BC = MinX + nX;
	}
	// !ALTRAN End

	
	return BC;
}

template <class DATA1, class DATA2> 
DATA2 _barycentre(const DATA1 *X, const DWORD * iX, const int nX, DATA2 &Sigma, DATA2 *T, int order = 2)
{
	int i, iO;
	DATA2 *pT;
	
	DATA2 CumEnr = 0;
	DATA2 BC0, BC = 0, M2 = 0, VX = 0;
	
	for (i=0, pT = T; i<nX; i++, pT++)
	{
		for (iO = 0, *pT = 1.0; iO < order; iO++, *pT *= X[i]);
		CumEnr += *pT;
	}

	// !ALTRAN - 23 Begin
	if(CumEnr == 0){
		Sigma=0;
		return -1;
	}
	// !ALTRAN end
	
	for (i=0, pT = T; i<nX; i++, pT++)
	{
		*pT /= CumEnr;
		BC0 = iX[i] * (*pT);
		BC += BC0;
	}

	for (i=0, pT = T; i<nX; i++, pT++)
	{
		M2 = iX[i] - BC;
		VX += M2 * M2 * (*pT);
	}
	
	if (VX > 0)
		Sigma = sqrt(VX);
	else
		Sigma = -1.0;

	// !ALTRAN - 46 Begin
	if(BC < iX[0])
	{
		BC = iX[0];
	}
	if(BC > iX[nX-1])
	{
		BC = iX[nX-1];
	}
	// !ALTRAN End

	return BC;
}


// !ALTRAN - 41 Begin
// --------------------------------------------------------------------
// _BDA_compensAmpByMHorzMedian - Normalization by median value 
//
// Return value: no return
//
// [INPUT/OUPUT]  p_matrix     === matrix to normalize
// [INPUT]	      p_nbBeams    === Number of beams
// [INPUT]	      p_nbSamples  === Number of samples
// [TEMP]	      m_pfBeamsA, m_pfBeamsA
//
// [MATLAB] AmpImage           === amp (full beam)
//
// Used in functions:
//		_detection_estimate
//
// MatLab reference:
//		BDA_compensAmpByMHorzMedian
//
void _BDA_compensAmpByMHorzMedian(char* p_RAWData, int p_nbBeams, int p_nbSamples)
{
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	_raw_data_type * dataPrevious;
	float med;

	for(int i=0; i<p_nbSamples; i++)
	{
		// Compute the median on current sample
		dataPrevious = data;
		for(int j=0; j<p_nbBeams; j++,data++)
		{
			m_pfBeamsA[j] = data->amp;
		}
		med =  _median(m_pfBeamsA, m_pfBeamsB, p_nbBeams);

		// Normalize the amplitude data on current sample
		if(med > 0)
		{
			for(int j=0; j<p_nbBeams; j++,dataPrevious++)
			{
				dataPrevious->amp = dataPrevious->amp / med;
			}
		}
		else
		{
			for(int j=0; j<p_nbBeams; j++,dataPrevious++)
			{
				dataPrevious->amp = 0;
			}
		}
	}
}
// !ALTRAN End


// --------------------------------------------------------------------
// _amplitude_single_reduit - Detection sur le barycentre  on the reduced
//                            amplitude matrix.
//
// Return value: detection point
//
// [INPUT]  m_pAmpReduite;
// [TEMP]   m_pfBeamsA, m_pfBeamsB
// [TEMP]   m_pfSamplesA
//
// [MATLAB] x                  === 
// [MATLAB] Amp                === amp (full beam)
// [MATLAB] R0                 ===
// [MATLAB] iBeam
// [MATLAB] BeamAngles
// [MATLAB] nbSamples
// [MATLAB] AmpBeamRange       === (returned value)
// [MATLAB] AmpBeamQF          === (not used in MatLab / not returned)
// [MATLAB] AmpBeamSamplesNb   === (not used in MatLab / not returned)
// [MATLAB] AmpBeamQF2         === (not used in MatLab / not returned)
//
// Used in functions:
//		_detection_estimate
//
// MatLab reference:
//		SampleBeam_ReduceMatrixAmplitude
//
float _amplitude_single_reduit (float *amp, int iSampleBeg, int iSampleEnd, int iBeam, int index)
{
	int iMaskMin = iSampleBeg;
	int iMaskMax = iSampleEnd;
	
	// --------------------
	// Calcul du barycentre
	
	float Sigma;
	float AmpBeamRange = _barycentre(amp+iMaskMin, iMaskMin-iSampleBeg+1+index, iMaskMax-iMaskMin, Sigma, m_pfSamplesA)+iSampleBeg;
	
	// ------------------------------------------------
	// Restriction du domaine et recalcul du barycentre
	
	iMaskMin = max(floori(AmpBeamRange-2*Sigma)-index, iSampleBeg);
	iMaskMax = min(floori(AmpBeamRange+2*Sigma)-index, iSampleEnd);
	
	if (iMaskMax <= iMaskMin) return AmpBeamRange+0.5f;
	AmpBeamRange = _barycentre(amp+iMaskMin, iMaskMin-iSampleBeg+1+index, iMaskMax-iMaskMin, Sigma, m_pfSamplesA, (iMaskMax == iSampleEnd) ? 1 : 2)+iSampleBeg;
	
	// ---------------------------------------------------------------
	// Restriction du domaine et recalcul du barycentre
	
	iMaskMin = max(floori(AmpBeamRange-Sigma)-index, iSampleBeg);
	iMaskMax = min(floori(AmpBeamRange+Sigma)-index, iSampleEnd);
	
	if (iMaskMax <= iMaskMin) return (float)iSampleBeg + 0.5f; //EPM as per Jean-Marie 02-05-14
	
	AmpBeamRange = _barycentre(amp+iMaskMin, iMaskMin-iSampleBeg+1+index, iMaskMax-iMaskMin, Sigma, m_pfSamplesA) + iSampleBeg;
	
	//return AmpBeamRange;
	return AmpBeamRange + 0.5f; //EPM as per Jean-Marie 02-05-14
}


// --------------------------------------------------------------------
// _strip_estimation - tests validity of estimated R0 point according
//                     to strip average amplitude.
//
// [IN] MoyHorz   - first beam in the strip
// [IN] R0        - estimated R0 point
// [IN] nbSamples - strip length in samples
// [IN] SeuilAmp  - amplitude threshold for detection
//
// Return value: true is estimation is valid
//
// [MATLAB] MoyHorz       === MoyHorz
// [MATLAB] Start         === beam_start
// [MATLAB] R0            === R0
// [MATLAB] nbSamples     === nbSamples
// [MATLAB] SeuilAmp      === SeuilAmp
// [MATLAB] flag          === (returned value)
//
// Used in functions:
//		_strip_estimation
//
// MatLab reference:
//		Reson_R0_estimation_V6 -> testAmplitude

bool _strip_amplitude(float * MoyHorz, WORD sample_start, int R0, WORD nbSamples, float SeuilAmp)
{
	float A;

	if (R0 <= 0)
		A = MoyHorz[0];
	else if (R0 >= nbSamples-1)
		A = MoyHorz[nbSamples-1];
	else 
	{
		A = max(MoyHorz[R0-1],MoyHorz[R0+1]);
		A = max(A, MoyHorz[R0]);
	}

	if (sample_start)
//%		SeuilAmp = interp1([1 nbSamples], [SeuilAmp SeuilAmp/2], R0);
		SeuilAmp = SeuilAmp * ( 1 - (float)R0 / (2 * (float)(nbSamples-1)) );
	
	return (A < SeuilAmp) ? false : true;
}


// --------------------------------------------------------------------
// _strip_estimation - tests detection slope of to validate estimation.
//
// [IN] S         - signal
// [IN] nbSamples - signal length in samples
//
// Return value: true is estimation is valid
//
// [MATLAB] x             === (defined by nbSamples, since points are continuous from start)
// [MATLAB] S             === S
// [MATLAB] Slope         === (returned value) --- NB: Actually maximum of consequent tries is returned as it is what is used in MatLab
//
// Used in functions:
//		_strip_estimation
//
// MatLab reference:
//		Reson_R0_estimation_V6 -> testHeaviside

float _strip_slope(float * S, WORD nbSamples)
{
	float L1, L2, Slope = 0, S1, S2;
	int i,j, K1, K2, I50, k;

	for (k = 1; k <= 25; k++)
	{
		L1 = ((float)k)/100.0f;
		L2 = 1-L1;
		K1 = K2 = I50 = -1;

//%		iBeg = find(S >= L1, 1, 'first');	=== K1
//%		iEnd = find(S <= L2, 1, 'last');	=== K2
//%		i50  = find(S >= 0.5, 1, 'first');	=== I50
		for (i=0, j=nbSamples-1; i < nbSamples; i++, j--)
		{
			if (S[i] >= L1   && K1  == -1) K1  = i;
			if (S[j] <= L2   && K2  == -1) K2  = j;
			if (S[i] >= 0.5f && I50 == -1) I50 = i;

			if (K1>0 && K2>0 && I50>0)
				break;
		}

		if (K2 > I50)
			S1 = (0.5f - L1) / (K2-I50);
		else
			S1 = 0.0f;

		if (I50 > K1)
			S2 = (0.5f - L1) / (I50-K1);
		else
			S2 = 0.0f;

		S1 = max(S1,S2);

		if (S1 > Slope) Slope = S1;
	}

	return Slope;
}


// --------------------------------------------------------------------
// _strip_estimation - rough estimation of weighted averaged
//                     bottom detect point on the group of the beams.
//
// [IN] beam_start - first beam in the strip
// [IN] beam_end - first beam in the next strip
// [IN] sample_start - first sample of the reduced matrix to process
// [IN] SeuilAmp - amplitude threshold for detection
//
// Return value: estimated R0 on the strip
//
// [INPUT]  m_pAmpReduite
// [TEMP]   m_pfSamplesA, m_pfSamplesB
// [TEMP]   m_pfSamplesC, m_pfSamplesD
// [TEMP]   m_pfSamplesE
//
// [MATLAB] Amp           === m_pAmpReduite(beam_start:beam_end)
// [MATLAB] Start         === sample_start
// [MATLAB] SeuilAmp      === SeuilAmp
// [MATLAB] SeuilCum      === (hardcoded to 0.5)
// [MATLAB] R1            === (not used in MatLab / not returned)
// [MATLAB] R2            === (not used in MatLab / not returned)
// [MATLAB] R3            === (not used in MatLab / not returned)
// [MATLAB] S1Cum         === (not used in MatLab / not returned)
// [MATLAB] S2Cum         === (not used in MatLab / not returned)
// [MATLAB] S3Cum         === (not used in MatLab / not returned)
// [MATLAB] TypeSignal    === (not used in MatLab / not returned)
// [MATLAB] flagR1Amp     === m_pFlagBeams(beam_start:beam_end)
// [MATLAB] flagR2Amp     ===           NB: flags are returned as ORed value
// [MATLAB] flagR3Amp     ===
// [MATLAB] R0Strip       === (returned value)
//
// Sub-functions calls:
//		_strip_amplitude
//		_strip_slope
//		
// Used in functions:
//		_r0_estimation

WORD _strip_estimation(WORD beam_start, WORD beam_end, WORD sample_start, WORD sample_end, float SeuilAmp)
{
	register int iB, iS;

	float SeuilCum = 0.5f;

	WORD SampleSize = sample_end - sample_start;

//%	MoyHorz = nanmean(Amp(Start:end,:), 2)';
//%	MedHorz = nanmedian(Amp(Start:end,:), 2)';
//%	MoyMinusMed = MoyHorz - MedHorz;

	float meanMin = FLT_MAX;	// min(MoyHorz)

	// !ALTRAN - 35 Begin
	float meanMax = -FLT_MAX;	// min(MoyHorz)
	// !ALTRAN End
	
	// m_pfSamplesA - Strip Mean		(MoyHorz)
	// m_pfSamplesB - Strip Median		(MedHorz)
	// m_pfSamplesD - MoyMinusMed
	for (iS = 0; iS < SampleSize; iS++)
	{
		for (iB = beam_start; iB < beam_end; iB++)
		{
			m_pfSamplesE[iB-beam_start] = m_pAmpReduite[iB][iS+sample_start];
		}


		m_pfSamplesA[iS] = _mean(m_pfSamplesE, beam_end-beam_start);
		m_pfSamplesB[iS] = _median(m_pfSamplesE, NULL, beam_end-beam_start);
		if (m_pfSamplesA[iS] < meanMin) meanMin = m_pfSamplesA[iS];
		// !ALTRAN - 35 Begin
		if (m_pfSamplesA[iS] > meanMax) meanMax = m_pfSamplesA[iS];
		// !ALTRAN End
		m_pfSamplesD[iS] = m_pfSamplesA[iS]-m_pfSamplesB[iS];
	}

	// m_pfSamplesE - Temporary
	float medianMean  = _median(m_pfSamplesA, m_pfSamplesE, SampleSize);
//%	Med = median(MoyMinusMed(subNonNaN)); % =0 en principe
	float medianMinus = _median(m_pfSamplesD, m_pfSamplesE, SampleSize);
 	float calc;			// Temporary calculations

	// m_pfSamplesC - S1
	// m_pfSamplesE - S2
	// m_pfSamplesD - S3
	for (iS = 0; iS < SampleSize; iS++)
	{
//%		S1 = MoyHorz - min(MoyHorz);
//%		S1 = S1 / max(MoyHorz);			--- NB: devision is redundant because of the normalization later
//%		S1 = S1 .^ 2;
		calc = m_pfSamplesA[iS] - meanMin;
		// !ALTRAN - 35 Begin
		calc = calc / meanMax;
		// !ALTRAN End
		m_pfSamplesC[iS] = calc*calc;

//%		S2 = NaN(1,nbSamples);
//%		S2(subNonNaN) = (MoyHorz(subNonNaN) - median(MoyHorz(subNonNaN)));
//%		S2 = S2 / max(S2);
		m_pfSamplesE[iS] = m_pfSamplesA[iS] - medianMean;

//%		S3 = NaN(1,nbSamples);
//%		S3(subNonNaN) = (MoyMinusMed(subNonNaN) - Med);
		m_pfSamplesD[iS] -= medianMinus;
	}	
	
	// m_pfSamplesC - S1Cum
	_cumsum(m_pfSamplesC, m_pfSamplesC, SampleSize, true);
	// m_pfSamplesE - S2Cum
	_cumsum(m_pfSamplesE, m_pfSamplesE, SampleSize, true);
	// m_pfSamplesD - S3Cum
	_cumsum(m_pfSamplesD, m_pfSamplesD, SampleSize, true);

//%	R1 = find(S1Cum >= SeuilCum, 1, 'first') + (Start-1);   --- NB: Calculation vectors are aligned from sample_start
//%	R2 = find(S2Cum >= SeuilCum, 1, 'first') + (Start-1);   ---     so it is not added back in for the calculations,
//%	R3 = find(S3Cum >= SeuilCum, 1, 'first') + (Start-1);   ---     but rather in the final results only
//%	flagR1Noise = (max(abs(S1Cum)) < 2);
//%	flagR2Noise = (min(S2Cum) > -0.6) && (max(abs(S2Cum)) < 2);
//%	flagR3Noise = (min(S3Cum) > -0.6) && (max(S3Cum) < 1.6);

	int R1 = m_ReduiteSize, R2 = m_ReduiteSize, R3 = m_ReduiteSize, R0;
	bool flagR1Noise = true, flagR2Noise = true, flagR3Noise = true;

	for (iS = 0; iS < SampleSize; iS++)
	{
		if (m_pfSamplesC[iS] >= SeuilCum && R1 == m_ReduiteSize) R1 = iS;
		if (m_pfSamplesE[iS] >= SeuilCum && R2 == m_ReduiteSize) R2 = iS;
		if (m_pfSamplesD[iS] >= SeuilCum && R3 == m_ReduiteSize) R3 = iS;

		if (fabs(m_pfSamplesC[iS]) > 2)                             flagR1Noise = false;
		if (fabs(m_pfSamplesE[iS]) > 2 || m_pfSamplesE[iS] < -0.6f) flagR2Noise = false;
		if (m_pfSamplesD[iS] < -0.6f || m_pfSamplesD[iS] > 1.6f)    flagR3Noise = false;
	}
	
	float Slope1 = 0.0f, Slope2 = 0.0f, Slope3 = 0.0f, Slope = 0.0f;

	bool flagR1Amp = _strip_amplitude(m_pfSamplesA, sample_start, R1, SampleSize, SeuilAmp);
	bool flagR2Amp = _strip_amplitude(m_pfSamplesA, sample_start, R2, SampleSize, SeuilAmp);
	bool flagR3Amp = _strip_amplitude(m_pfSamplesA, sample_start, R3, SampleSize, SeuilAmp);

	if (flagR1Noise && flagR1Amp)
		Slope1 = _strip_slope(m_pfSamplesC, SampleSize);
	else
		Slope1 = 0;
	
	if (flagR2Noise && flagR2Amp )
		Slope2 = _strip_slope(m_pfSamplesE, SampleSize);
	else
		Slope2 = 0;

	if (flagR3Noise && flagR3Amp )
		Slope3 = _strip_slope(m_pfSamplesD, SampleSize);
	else
		Slope3 = 0;

	int TypeSignal = 0;

	if (Slope2 > Slope1)
	{
		Slope = Slope2;
		R0 = R2;
		TypeSignal = 2;
	}
	else if (Slope1 > 0)
	{
		Slope = Slope1;
		R0 = R1;
		TypeSignal = 1;
	}

	if (Slope3 > Slope)
	{
		Slope = Slope3;
		R0 = R3;
		TypeSignal = 3;
	}

	if ((flagR1Amp|flagR2Amp|flagR3Amp) && (Slope>=0.01f))
	{
		for (iS = beam_start; iS < beam_end; iS++)
		{
			m_pFlagBeams[iS] = true;						// flagBeams
		}
	}
	else
	{
		for (iS = beam_start; iS < beam_end; iS++)
		{
			m_pFlagBeams[iS] = false;						// flagBeams
		}

		return 0;
	}

	if (SampleSize < 21) 
		return R0 + sample_start;

	// Final detection
	int K1 = (R0 < 10) ? 0 : (R0 >= SampleSize - 11) ? SampleSize - 21 : R0 - 10;
	Slope1 = Slope2 = 0;

	meanMin = FLT_MAX;
	for (iS = K1; iS < K1+21; iS++)
	{
		if (meanMin > m_pfSamplesA[iS]) meanMin = m_pfSamplesA[iS];
	}

	for (iS = K1; iS < K1+21; iS++)
	{
		Slope1 = m_pfSamplesA[iS] - meanMin;
		m_pfSamplesB[iS] = Slope1*Slope1;
	}

	_cumsum(m_pfSamplesB+K1, m_pfSamplesB+K1, 21, false);

	Slope1 =         FLT_MAX; // min(yf)
	Slope2 = (-1.0f)*FLT_MAX; // max(yf)

	for (iS = K1; iS < K1+21; iS++)
	{
		if (Slope1 > m_pfSamplesB[iS]) Slope1 = m_pfSamplesB[iS];
		if (Slope2 < m_pfSamplesB[iS]) Slope2 = m_pfSamplesB[iS];
	}

	Slope2 -= Slope1;

	for (iS = K1; iS < K1+21; iS++)
	{
		m_pfSamplesB[iS] = (m_pfSamplesB[iS] - Slope1) / Slope2;
		if (m_pfSamplesB[iS] > 0.10f)
			return iS + sample_start - 1;
	}

	return 0;
}


// --------------------------------------------------------------------
// _r0_estimation - rough estimation of weighted averaged
//                  bottom detect point on the reduced amplitude matrix.
//
// Return value: Success(true)/Fail(false)
//
// [INPUT]  m_pR1 === EndSample
// [INPUT]  m_pAmpReduite, m_ReduiteStep
// [OUTPUT] m_pR1, m_R0
// [TEMP]   m_pdBeamsA, m_pdBeamsB, m_pdBeamsC
// [TEMP]   m_pfBeamsA, m_pfBeamsB, m_pfBeamsC, m_pfBeamsD
//
// Sub-functions calls:
//		_strip_estimation
//		
// Used in sub-functions:
// [TEMP]   m_pfSamplesA, m_pfSamplesB
// [TEMP]   m_pfSamplesC, m_pfSamplesD
// [TEMP]   m_pfSamplesE
//
// [MATLAB] Amp           === m_pAmpReduite
// [MATLAB] NbStrips      === (hardcoded)
// [MATLAB] Start         === sample_start
// [MATLAB] EndSample     === m_pR1 (in)
// [MATLAB] SeuilAmp      === SeuilAmp
// [MATLAB] SeuilCum      === (hardcoded in subfunctions)
// [MATLAB] Rmin          === m_pGatesMin / m_ReduiteStep
// [MATLAB] Rmax          === m_pGatesMax / m_ReduiteStep
// [MATLAB] R0            === m_R0
// [MATLAB] R0Strip       === m_pR1 (out)
// [MATLAB] flagBeams     === m_pFlagBeams
// [MATLAB] R0StripBefore === (not used in MatLab / not returned)
// [MATLAB] R0StripAfter  === (not used in MatLab / not returned)
//
bool _r0_estimation( WORD sample_start, float SeuilAmp )
{
	float nbBeams5 = (float)m_dwReceiveBeams / m_nStrips;
	
	int iS, iB, k1, k2;
	float R0;
	WORD SampleEnd, SampleStart = sample_start;
	DWORD minGate = m_iOutputSamples, maxGate = 0;
	DWORD *R1NaN    = (DWORD*) m_pR1;
	DWORD *StripNaN = (DWORD*) m_pfBeamsB;
	
	NANf(m_R0);
	memset(m_pFlagBeams,false,sizeof(bool)*m_dwReceiveBeams);

	// m_pfBeamsB === r0Strip
	// m_R0       === min(r0Strip)

	for (iS = 0; iS < m_nStrips; iS++)
	{
//%		k1 = 1 + floor((iStrip-1) * nbBeams5);
//%		k2 = floor(iStrip * nbBeams5);
//%		sub = k1:k2;
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);

//%		RminStrip = min(Rmin(sub));
//%		RmaxStrip = max(Rmax(sub));
//%		MaxEndSample = max(EndSample(sub));
		minGate     = 0xffffffff;
		maxGate     = 0;
		SampleEnd   = 0;
		
		for (iB = k1; iB < k2; iB++)
		{
			if (minGate   > m_pGatesMin[iB])   minGate   = m_pGatesMin[iB];
			if (maxGate   < m_pGatesMax[iB])   maxGate   = m_pGatesMax[iB];
			if (!ISNAN(R1NaN[iB]) && (float)SampleEnd < m_pR1[iB])  SampleEnd = floorw(m_pR1[iB]);
			m_pdBeamsA[iB] = iS;
		}		
		
        // !ALTRAN - 3 begin
		minGate = floordw((float)minGate/m_ReduiteStep-1.0f);
		maxGate = min(floordw((float)maxGate/m_ReduiteStep+1.0f), m_ReduiteSize);
		//maxGate = min(maxGate, m_ReduiteSize);
        // !ALTRAN end

//%		MaxEndSample = min(MaxEndSample, ceil(RmaxStrip));
//%		StartStrip = max(Start, floor(RminStrip));
//%		if StartStrip == nbSamples
//%			continue
//%		end

		if (!SampleEnd)
			SampleEnd = (WORD)maxGate;
		else
			SampleEnd   = min(SampleEnd,   (WORD)maxGate);

		SampleStart = max(sample_start,(WORD)minGate);
		
		if (SampleStart >= m_ReduiteSize-1 || SampleEnd == 0 || SampleEnd < SampleStart)
		{
			for (iB = k1; iB < k2; iB++)
			{
				NAN(R1NaN[iB]);
			}
			NAN(StripNaN[iS]);
			continue;
		}
		
		SampleEnd = min(SampleEnd,m_ReduiteSize);

//%		[R1Max(iStrip), R2Max(iStrip), R3Max(iStrip), S1, S2, S3, TypeSignal, flagR1, flagR2, flagR3,  r0Strip(iStrip)] = ...
//%			estimationR0OnStrip(Amp(1:min(nbSamples,MaxEndSample),sub), StartStrip, SeuilAmp, SeuilCum, Display);
		R0 = _strip_estimation(k1,k2,SampleStart,SampleEnd, SeuilAmp);

		if (R0 >= 10 && R0 < m_ReduiteSize)
		{
			if (ISNANf(m_R0) || m_R0 > R0)
				m_R0 = R0;
			m_pfBeamsB[iS] = R0;
		}
		else
		{
			for (iB = k1; iB < k2; iB++)
			{
				NAN(R1NaN[iB]);
			}
			NAN(StripNaN[iS]);
		}
	}

//%	Rap = r0Strip / min(r0Strip);
//%	subEqual = find(Rap == 1);
//%
//%	for k=1:length(subEqual)
//%	    if (subEqual(k) > 2) && (Rap(subEqual(k)-1) > 1.9)
//%	        r0Strip(subEqual(k)-1) = (r0Strip(subEqual(k)-2) + r0Strip(subEqual(k))) / 2;
//%	        r0Strip(subEqual(k)-1) = r0Strip(subEqual(k)-1)-1;
//%	    end
//%	    if (subEqual(k) < (NbStrips-1)) && (Rap(subEqual(k)+1) > 1.9)
//%	        r0Strip(subEqual(k)+1) = (r0Strip(subEqual(k)+2) + r0Strip(subEqual(k))) / 2;
//%	        r0Strip(subEqual(k)+1) = r0Strip(subEqual(k)+1)-1;
//%	    end
//%	end

	memcpy(m_pfBeamsA, m_pfBeamsB, sizeof(float) * m_nStrips);
	DWORD *StripANaN = (DWORD*) m_pfBeamsA;
	R0 = m_R0;

	for (iS = 0; iS < m_nStrips; iS++)
	{
		if (ISNAN(StripANaN[iS]) || !ISEQUAL(m_pfBeamsA[iS], m_R0))
			continue;

		if (iS > 1 && !ISNAN(StripANaN[iS-1]) && m_pfBeamsA[iS-1] > (m_R0 * 1.9f + 0.9f))
		{
			if (ISNAN(StripNaN[iS-2]))
			{
				NAN(StripNaN[iS-1]);
			}
			else
			{
				m_pfBeamsB[iS-1] = (m_pfBeamsB[iS-2] + m_pfBeamsB[iS]) / 2.0f - 1.0f;
				if (ISNANf(R0) || R0 > m_pfBeamsB[iS-1])
					R0 = m_pfBeamsB[iS-1];
			}
		}

		if (iS < m_nStrips-2 && !ISNAN(StripANaN[iS+1]) && m_pfBeamsA[iS+1] > (m_R0 * 1.9f + 0.9f))
		{
			if (ISNAN(StripNaN[iS+2]))
			{
				NAN(StripNaN[iS+1]);
			}
			else
			{
				m_pfBeamsB[iS+1] = (m_pfBeamsB[iS+2] + m_pfBeamsB[iS]) / 2.0f - 1.0f;
				if (ISNANf(R0) || R0 > m_pfBeamsB[iS+1])
					R0 = m_pfBeamsB[iS+1];
			}
		}
	}

	m_R0 = floorf(R0);

//%	flagBeams = imdilate(flagBeams, ones(1,length(sub)));

	bool * buf = (bool *) &m_pdBeamsC[1];		// Since m_pdBeamsB was allocated as DWORD we should not encounter any memory problems
	m_pdBeamsC[0] = 0;
	int hbn = floori(nbBeams5/2.0f);
	int shift = (floori(nbBeams5/2.0f) == floori(nbBeams5/2.0f+0.5f)) ? 0 : 1;
	memcpy(buf,m_pFlagBeams,sizeof(bool)*m_dwReceiveBeams);
	for (iB = hbn; iB < (int)m_dwReceiveBeams - hbn; iB++)
	{
		m_pFlagBeams[iB] = buf[iB] | buf[iB-hbn- shift] | buf[iB+hbn];
	}

//%	subNan = find(isnan(r0Strip));
//%	if ~isempty(subNan)
//%	    subNotNan = find(~isnan(r0Strip));
//%	    if length(subNotNan) < 2
//%	        R0 = NaN;
//%	        return
//%	    end
//%	    r0Strip(subNan) = floor(interp1(subNotNan, r0Strip(subNotNan), subNan));
//%	end

	int vStrips = 0, inS, inSn;
	int iSB = -1, iSE;		// First and last valid strip

	for (iS = inS = 0; iS < m_nStrips; iS++)
	{
		m_pfBeamsA[iS] = (float)iS;
		if (!ISNAN(StripNaN[iS]))
		{
			m_pfBeamsC[vStrips] = (float)iS;
			m_pfBeamsD[vStrips] = m_pfBeamsB[iS];
			vStrips++;
			if (iSB == -1) iSB = iS;
			iSE = iS;
		}
		else
		{
			m_pdBeamsA[inS++] = (DWORD)iS;
		}
	}

	inSn = inS;

	if (vStrips < 2)
	{
		NANf(m_R0);
		return false;
	}

	if (vStrips < m_nStrips)
	{
		Interp1 (m_pfBeamsC, m_pfBeamsD, vStrips, m_pfBeamsA, m_pfBeamsB, m_nStrips);

		for (iS = 0; iS < inSn; iS++)
		{
			m_pfBeamsB[m_pdBeamsA[iS]] = floorf(m_pfBeamsB[m_pdBeamsA[iS]]);
		}
	}

//%	for iStrip=1:NbStrips
//%	    k1 = 1 + floor((iStrip-1) * nbBeams5);
//%	    k2 = floor(iStrip * nbBeams5);
//%	    sub = k1:k2;
//%	    R0Strip(sub) = r0Strip(iStrip);
//%	    R0StripBefore(sub) = r0Strip(max(1,iStrip-1));
//%	    R0StripAfter(sub)  = r0Strip(min(NbStrips, iStrip+1));
//%	end

	for (iS = 0; iS < iSB; iS++)
	{
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);
		
		for (iB = k1; iB < k2; iB++)
		{
			NAN(R1NaN[iB]);
		}
	}

	for (; iS <= iSE; iS++)
	{
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);
		
		for (iB = k1; iB < k2; iB++)
		{
			m_pR1[iB] = m_pfBeamsB[iS];
			if (m_pR1[iB] >= m_ReduiteSize)
				NAN(R1NaN[iB]);
		}
	}

	for (; iS < m_nStrips; iS++)
	{
		k1 = floori(iS * nbBeams5);
		k2 = floori((iS+1) * nbBeams5);
		
		for (iB = k1; iB < k2; iB++)
		{
			NAN(R1NaN[iB]);
		}
	}

	return true;
}

    
// --------------------------------------------------------------------
// _matrix_reduction   - Reduces original Amplitude to REDUCTION_SAMPLE_SIZE 
//                       into m_pAmpReduite matrix. If m_iOutputSamples is less
//                       then REDUCTION_SAMPLE_SIZE, pads data with zeros.
//
// Return value: NONE
//
// [INPUT]  m_pRAWData;
// [OUTPUT] m_pAmpReduite
// [OUTPUT] m_ReduiteStep
// [TEMP]   m_pfBeamsA,  m_pfBeamsB
//
// Used in functions:
//		_detection_estimate
//
// MatLab reference:
//		SampleBeam_ReduceMatrixAmplitude
//
void _matrix_reduction()
{
	DWORD iB;
	int   iS, iRS, nRC;
	double N_m;

	
	// !ALTRAN - 4 begin
	//m_ReduiteStep = max(1, floori(((float)m_iOutputSamples)/((float)REDUCTION_SAMPLE_SIZE)+0.5f));
	N_m=0.04*m_iOutputSamples+190;
	m_ReduiteStep =max(1,floori((m_iOutputSamples/N_m)+0.5));
	// !ALTRAN end

	m_ReduiteSize = floorw(((float)m_iOutputSamples)/((float)m_ReduiteStep));
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;

//	if (m_ReduiteStep == 1)		// Nothing to reduce, just copy.
	if (m_iOutputSamples <= REDUCTION_SAMPLE_SIZE)		// Nothing to reduce, just copy.
	{
		for (iS=0; iS < m_iOutputSamples; iS++)
		{
			for (iB=0;iB<m_dwReceiveBeams;iB++,data++)
			{
				m_pAmpReduite[iB][iS] = data->amp;
			}
		}

		m_ReduiteSize = m_iOutputSamples;

		for (; iS < REDUCTION_MATRIX_SIZE; iS++)
		{
			for (iB=0;iB<m_dwReceiveBeams;iB++)
			{
				m_pAmpReduite[iB][iS] = 0;
			}
		}

		return;
	}

	for (iS=iRS=0; iRS < m_ReduiteSize; iRS++)
	{
		nRC = (iRS+1)*m_ReduiteStep;

		if (nRC > m_iOutputSamples)
			break;
		
		for (;iS<nRC; iS++)
		{
			for (iB=0;iB<m_dwReceiveBeams;iB++,data++)
			{
				m_pfReduite[iB][iRS] += data->amp;
			}
		}

		for (iB=0;iB<m_dwReceiveBeams;iB++)
		{
			m_pfReduite[iB][iRS] /= m_ReduiteStep;
		}
	}

	m_ReduiteSize = iRS;

	for (; iRS < REDUCTION_MATRIX_SIZE; iRS++)
	{
		for (iB=0;iB<m_dwReceiveBeams;iB++)
		{
			m_pfReduite[iB][iRS] = 0.0f;
		}
	}

	for (iRS = 0; iRS < m_ReduiteSize; iRS++)		// Copy first and last beams
	{
		m_pAmpReduite[0][iRS] = m_pfReduite[0][iRS];
		m_pAmpReduite[m_dwReceiveBeams-1][iRS] = m_pfReduite[m_dwReceiveBeams-1][iRS];
	}

	for (iRS = 0; iRS < m_ReduiteSize; iRS++)
	{
		for (iB=1; iB<m_dwReceiveBeams-1; iB++)
		{
			m_pAmpReduite[iB][iRS] = (m_pfReduite[iB-1][iRS]+m_pfReduite[iB][iRS]+m_pfReduite[iB+1][iRS])/3.0f;
		}
	}
}


// %% Function testing the shape of the enveloppe
// function val_qf=test_shape(Amp,x) 
//     level_abs   = 0.7;
//     bell_int_1  = cumsum(Amp.^2/sum(Amp.^2));
//     x_bell_1    = (x-(x(end)+x(1))/2);
//     x_bell_1    = x_bell_1/x_bell_1(end)/2;
//     [~,u_min_1] = min(abs(x_bell_1+0.3));
//     [~,u_max_1] = min(abs(x_bell_1-0.3));
//     
//     level_temp = abs(bell_int_1(u_max_1) - bell_int_1(u_min_1));
//     if level_temp > level_abs
//         val_qf = 1;
//     else
//         val_qf = 0;
//     end

bool _amplitude_test_shape (DWORD *ampIndex, float * amp, int samples)
{
	//% 	bell_int = cumsum(Amp.^2/sum(Amp.^2));
	//% 	x_bell    = (x-(x(end)+x(1))/2);
	//% 	x_bell    = x_bell/x_bell(end)/2;
	//% 	[ppp,u_min] = min(abs(x_bell+0.3));
	//% 	[ppp,u_max] = min(abs(x_bell-0.3));
	//% 	
	//% 	if abs(bell_int(u_max)-bell_int(u_min))>0.78
	//% 		val_qf = 1;
	//% 	else
	//% 		val_qf = 0;
	//% 	end
	
	float xS = (float) ampIndex[0], xE = (float)ampIndex[samples-1];
	float xD = xE-xS, xM = (xE+xS)/(xE-xS)/2.0f;
	float uMin = 1.0f, uMax = 1.0f;
	float aMin = 0.0f, aMax = 0.0f, aSum = 0.0f;
	float x_bell;
	
	for (int iS = 0; iS < samples; iS++)
	{
		aSum += amp[iS]*amp[iS];
		x_bell = ampIndex[iS]/xD-xM;
		if (fabsf(x_bell+0.3f) < uMin) { uMin = fabsf(x_bell+0.3f); aMin = aSum; }
		if (fabsf(x_bell-0.3f) < uMax) { uMax = fabsf(x_bell-0.3f); aMax = aSum; }
	}
    
	return (fabs(aMax-aMin) > 0.75f*aSum);
}


// !ALTRAN - 42 begin
bool _amplitude_test_double_shape (DWORD *ampIndex, float * amp, int samples, float R0, float AmBeamRange, float lineWidth, int *subAmpIndex, int *subAmpsize)
{

/* Matlab code
nAmp  = length(Amp);
nAmp2 = floor(nAmp/2);
subAmp1 = 1:min(nAmp2+3-1,nAmp);
subAmp2 = max(nAmp2-3, 1):nAmp;
testAmp0 = test_shape(Amp, x);
testAmp1 = test_shape(Amp(subAmp1), x(subAmp1));
testAmp2 = test_shape(Amp(subAmp2), x(subAmp2));

testProcheSpeculaire = ((AmpBeamRange/R0) < 1.03);


if all([testAmp0, testAmp1, testAmp2 testProcheSpeculaire])
    Max1 = max(Amp(subAmp1));
    Max2 = max(Amp(subAmp2));
    
    if (Max2 > (Max1/20))
        subOut = subAmp2;
    elseif (Max1 > (Max2/20))
        subOut = subAmp1;
    else
        subOut = [];
    end
else
    subOut = [];
end
*/

	int nAmp2 = int(floorf(samples/2) - 1);
	bool testAmp0;
	bool testAmp1;
	bool testAmp2;
	bool testProcheSpeculaire;
	float Max1, Max2;
	float subAmp1Limit, subAmp2Limit;
	bool result;

	subAmp1Limit = samples;
	if((nAmp2+3-1) < samples) subAmp1Limit=nAmp2+3-1;

	subAmp2Limit = 0;
	if((nAmp2-3) > 0) subAmp2Limit=nAmp2-3;

	testAmp0 = _amplitude_test_shape(ampIndex, amp, samples);
	testAmp1 = _amplitude_test_shape(ampIndex, amp, subAmp1Limit);
	testAmp2 = _amplitude_test_shape(ampIndex+nAmp2, amp+nAmp2, samples-nAmp2);

	testProcheSpeculaire = ((AmBeamRange/R0) < 1.03);

	if(testAmp0==true && testAmp1==true && testAmp2==true && testProcheSpeculaire==true){
		Max1 = -FLT_MAX;
		for(int i=0;i<nAmp2;i++)
		{
			if(Max1 < amp[i])
			{
				Max1 = amp[i];
			}
		}

		Max2 = -FLT_MAX;
		for(int i=nAmp2;i<samples;i++)
		{
			if(Max2 < amp[i])
			{
				Max2 = amp[i];
			}
		}

		if (Max2 > (Max1/20))
		{
			*subAmpIndex = subAmp2Limit;
			*subAmpsize = samples - subAmp2Limit;
			result = true;
		}
		else if(Max1 > (Max2/20))
		{
			*subAmpIndex = subAmp1Limit;
			*subAmpsize = subAmp1Limit;
			result = true;
		}
		else
		{
			result = false;
		}
	}
	else
	{
		result = false;
	}

	return result; 

}
// !ALTRAN End


// --------------------------------------------------------------------
// _amplitude_single - implementation of detectionAmplitudePing
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT] m_pRangeAmplitude[beam]
// [OUTPUT] m_pSamplesAmp[beam]
// [OUTPUT] m_pQualityAmplitude[beam]
// [OUTPUT] m_pQF2Amplitude[beam]
// [TEMP]   m_pfSamplesB
//
// [MATLAB] x                === ampIndex
// [MATLAB] Amp              === amp
// [MATLAB] R0               === m_R0 (not used)
// [MATLAB] iBeam            === beam
// [MATLAB] BeamAngles       === m_pBeamAngles (not used)
// [MATLAB] nbSamples        === m_iOutputSamples
// [MATLAB] AmPingRange      === m_pRangeAmplitude[beam]
// [MATLAB] AmpPingQF        === m_pQualityAmplitude[beam]
// [MATLAB] AmpPingSamplesNb === m_pSamplesAmp[beam]
// [MATLAB] AmpPingQF2       === m_pQF2Amplitude[beam]
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_amplitude_detection
//
// MatLab reference:
//		detectionAmplitudePing
//
// !ALTRAN - 5 begin
bool _amplitude_single (WORD beam, DWORD *ampIndex, float * amp, int samples, int nbAllSamples)
// !ALTRAN end
{
	int iS;
	double AmpBeamRange;
	int subAmpIndex, subAmpsize;
	bool result;

// !ALTRAN - 6 begin
// %% Second test (15/12/2010) : if the last sample of the signal corresponds
// % to the maximum sample reachable in range then do not compute the
// % sounding because it will be either a biased one or a false detection
	if (ampIndex[samples-1] == (nbAllSamples - 1)){
		m_pRangeAmplitude[beam]     = -1; // %will be used in ResonSyntheseBottomDetector.m to determine which amplitude sounding to delete
		NANf(m_pQualityAmplitude[beam]);
		/*AmpBeamSamplesNb = NaN;
		AmpBeamQF2       = NaN;
		N=NaN;*/
		return true;
	}
// !ALTRAN end


// %% First center of gravity computed on energy
// [AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp);

	double Sigma, Sigma1 = 0, AmpBeamRange1, Sigma2 = 0, AmpBeamRange2 ;
	bool val_qf = false, val_qf_1 = false, val_qf_2 = false; // EPM 2/19/14 in response to run-time error

	// !ALTRAN - 42 begin
	if(m_IdentAlgo == 2)
	{
		AmpBeamRange = _barycentre(amp, ampIndex, samples, Sigma, m_pfdSamples) ; // as per Jean-Marie EPM 1/29/14
		result = _amplitude_test_double_shape(ampIndex, amp, samples, m_R0, AmpBeamRange, 1, &subAmpIndex, &subAmpsize);
		if(result == true)
		{
			AmpBeamRange = _barycentre(amp, ampIndex+subAmpIndex, subAmpsize, Sigma, m_pfdSamples);
		}
	}
	else
	{
		AmpBeamRange = _barycentre(amp, ampIndex, samples, Sigma, m_pfdSamples, 4) ; // as per Jean-Marie EPM 1/29/14
	}
	// !ALTRAN End


// %% Testing the signal length : if its standard deviation is lower
//	% than the physical length of the signal then we don't need to compute
//	% another center of gravity and we keep this sounding
//		
// if 2*Sigma0 +1 <= (N_0 + 2*(AmpBeamRange0 * (1 / cosd(theta/2) - 1)))
//    AmpBeamRange = AmpBeamRange0;
//    Sigma1 = Sigma0;
//    val_qf_0 = 1;

	double th = 0.0;

	if (m_dwDeviceId == 7111) {
		th = 123.376397069386e-006; 
	} else if (m_dwDeviceId == 7150) {
		if (m_fCenterFrequency < 18000) {
			th = 38.0777815958133e-006;
		} else {
			th = 9.51933212567369e-006;
		}
	}


	int iMin = 0, iMax = samples;

	if (4.0 * Sigma + 1 <= (m_N0 + th * AmpBeamRange)) {
		val_qf = true;
	} else {
// %% Restriction to the new domain
// sub1 = find((x >= (AmpBeamRange0-2*Sigma0)) & (x <= (AmpBeamRange0+2*Sigma0)));
		//double fMin = AmpBeamRange-4*Sigma;
		//double fMax = AmpBeamRange+4*Sigma;
		double fMin = AmpBeamRange-2*Sigma; // EPM as per Jean-Marie 02-05-14
		double fMax = AmpBeamRange+2*Sigma; // EPM as per Jean-Marie 02-05-14

		for (iS = 0;    iS < samples && ampIndex[iS] < fMin; iS++); iMin = iS;
		for (iS = samples-1; iS >= 0 && ampIndex[iS] > fMax; iS--); iMax = iS+1;

// %% Test of the shape of the enveloppe : if the remaining signal is not
//	% approximatively bell-shaped, then we dissmiss the sounding since its QF
//	% will be biased. We test the signal before and afetr restriction
// val_qf_0=test_shape(Amp,x);
// val_qf_1=test_shape(Amp(sub1),x(sub1));
		val_qf   = _amplitude_test_shape(ampIndex,      amp,      samples);
		val_qf_1 = _amplitude_test_shape(ampIndex+iMin, amp+iMin, iMax-iMin);

// %% Computation of the second center of gravity on amplitude
// [AmpBeamRange1, Sigma1] = calcul_barycentre1(x(sub1), Amp(sub1));
		AmpBeamRange1 = _barycentre(amp+iMin, ampIndex+iMin, iMax-iMin, Sigma1, m_pfdSamples, 1);
		// !ALTRAN - 17 begin
		val_qf_2 = _amplitude_test_shape(ampIndex+iMin, amp+iMin, iMax-iMin);
		// !ALTRAN end
			
		// Appel du 3ème calcul EPM as per Jean-Marie and Xavier
		fMin = AmpBeamRange1-2*Sigma1;

		fMax = AmpBeamRange1+2*Sigma1;

		for (iS = 0;    iS < samples && ampIndex[iS] < fMin; iS++); iMin = iS;

		for (iS = samples-1; iS >= 0 && ampIndex[iS] > fMax; iS--); iMax = iS+1;

		// !ALTRAN - 18 begin
		//val_qf_2 = _amplitude_test_shape(ampIndex+iMin, amp+iMin, iMax-iMin);
		// !ALTRAN end
		
		AmpBeamRange2 = _barycentre(amp+iMin, ampIndex+iMin, iMax-iMin, Sigma2, m_pfdSamples, 1);

	
	}

// %% Definition of the QF parameters and tests
// 
// if (2*Sigma1+1)/N_corr > 1 && val_qf_1%remaining signal long enough and bell shaped
//     val_qf=val_qf_1;
//     AmpBeamRange = AmpBeamRange1;
//     N  = (2*Sigma1+1)/N_corr;
//     dTAmp = B * sqrt(((4/pi-1) / 12) * (N-1) * (N+1) / N)* N_corr;
// elseif (2*Sigma0+1)/N_corr > 1 && val_qf_0%previous signal long enough and bell shaped
//     val_qf=val_qf_0;
//     AmpBeamRange = AmpBeamRange0;
//     N  = (2*Sigma0+1)/N_corr;
//     dTAmp = B * sqrt(((4/pi-1) / 12) * (N-1) * (N+1) / N)* N_corr;
// else%none of the above
//     AmpBeamRange = AmpBeamRange0;
//     dTAmp = -1;
// end

	/* EPM removed and replace with below code as per Jean-Marie and Xavier
	double dTAmp, N;

	m_pAmpWinMin[beam] = ampIndex[0]-1;
	m_pAmpWinMax[beam] = ampIndex[samples-1]-1;

	if (val_qf_1 && (2.0f * Sigma1 + 1.0f) > m_N0Corr) {
		val_qf_1 = true;
		AmpBeamRange = AmpBeamRange1;
		m_pAmpWinMin[beam] = ampIndex[iMin]-1;
		m_pAmpWinMax[beam] = ampIndex[iMax-1]-1;
		N = (2.0f * Sigma1 + 1.0f) / m_N0Corr;
		dTAmp = m_B * sqrt((N-1) * (N+1) / N)* m_N0Corr;
	} else if (val_qf && (4.0f * Sigma + 1.0f) > m_N0Corr) {
		N = (4.0f * Sigma + 1.0f) / m_N0Corr;
		dTAmp = m_B * sqrt((N-1) * (N+1) / N)* m_N0Corr;
	} else {
		dTAmp = -1.0;
	}
*/
	double dTAmp, N;



	m_pAmpWinMin[beam] = ampIndex[0]-1;

	m_pAmpWinMax[beam] = ampIndex[samples-1]-1;



//	Proposition de calcul par analogie avec le code MatLab.
//	La constante 0.4 remplace de façon homogène les facteurs 0.35 et 0.44 de la version MatLab.
	if (val_qf_2 && (2.0f * Sigma2 + 1.0f) > m_N0Corr) {

		// ??? Intérêt : val_qf_2 = true;

		AmpBeamRange = AmpBeamRange2;

		m_pAmpWinMin[beam] = ampIndex[iMin]-1;

		m_pAmpWinMax[beam] = ampIndex[iMax-1]-1;

		N = (Sigma2 + 1.0f);

		dTAmp = 0.4 * sqrt(N * m_N0);	// On utilise bien m_N0 dans la version MatLab
	}
	else if (val_qf_1 && (2.0f * Sigma1 + 1.0f) > m_N0Corr) {

		val_qf_1 = true;

		AmpBeamRange = AmpBeamRange1;

		m_pAmpWinMin[beam] = ampIndex[iMin]-1;

		m_pAmpWinMax[beam] = ampIndex[iMax-1]-1;

		N = (Sigma1 + 1.0f);

		dTAmp = 0.4 * sqrt(N * m_N0);	// On utilise bien m_N0 dans la version MatLab

	} else if (val_qf && (4.0f * Sigma + 1.0f) > m_N0Corr) {

        // !ALTRAN - 7 begin
		//N = (Sigma + 1.0f);

		//dTAmp = 0.4 * sqrt(N * m_N0);	// On utilise bien m_N0 dans la version MatLab
        /* Matlab
        UnSurN = N_0/Sigma;
        dTAmp = Coeff * Sigma * sqrt(UnSurN);
        */
        dTAmp = 0.4 * Sigma * sqrt(m_N0/Sigma);
        // !ALTRAN end


	} else {

		dTAmp = -1.0;

	}

// if dTAmp > 0
//     AmpBeamQF = AmpBeamRange / dTAmp;
//     AmpBeamQF = log10(AmpBeamQF);
// else
//     AmpBeamQF = NaN;
// end
	m_pRangeAmplitude[beam] = (float)(AmpBeamRange-1.0);
	if (dTAmp > 0.0) {
		m_pQualityAmplitude[beam] = (float)log10(AmpBeamRange / dTAmp);
	} else {
		m_pQualityAmplitude[beam] = 0.0f;
	}

	return true;
}



// --------------------------------------------------------------------
// _matrix_compensation - Normalizes each sample to its noise via median value. 
//
// Return value: NONE
//
// [INPUT]  m_pAmpReduite
// [OUTPUT] m_pAmpReduite
// [TEMP]   m_pfBeamsA
//
void _matrix_compensation()
{
	int   iB, iS;
	
	float fMed; // EPM fix, whole function works on float
	
	for (iS=0; iS < m_ReduiteSize; iS++)
	{
		for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
		{
			m_pfBeamsA[iB] = m_pAmpReduite[iB][iS]; //EPM fix
		}
			
		fMed = _median(m_pfBeamsA, NULL, m_dwReceiveBeams);
			
		if (fabs(fMed) > 2*FLT_EPSILON)
		{
			for (iB=0;iB<(int)m_dwReceiveBeams;iB++)
			{
				m_pAmpReduite[iB][iS] /= fMed;
			}
		}
		else
		{
			for (iB=0;iB<(int)m_dwReceiveBeams;iB++)
			{
				m_pAmpReduite[iB][iS] = 0;
			}
		}
	}
}



// --------------------------------------------------------------------
// _mask_beam_single - adjusting mask around estimation point
//
// Return value: none
//
// [INPUT]  m_R0

bool _mask_beam_single( WORD Beam, float CenterPoint, DWORD WMin, DWORD Width1, DWORD Width2, float R0Seuil, WORD step, float R0, bool checkR0=false )
{
//%	sub = RMax1(iBeam)-w1:RMax1(iBeam)+w2;
	int iMin = floori(CenterPoint) - (int)Width1;
	int iMax = floori(CenterPoint) + (int)Width2+1;


	if (iMin < 0)
		iMin = 0;

//%	if (RMax1(iBeam) / R0)  < 1.1
//%		sub = RMax1(iBeam)-w1:RMax1(iBeam)+2*w2;
//%	end

	if (checkR0 && CenterPoint < R0 * 1.1f )
		iMax = floori(CenterPoint) + (int)Width2*2+1;

//%	sub(sub > size(Amp,1)) = [];
	if (iMax > m_iOutputSamples) 
		iMax = m_iOutputSamples;

//%	sub(sub < (R0-step)) = [];
	if (iMin < R0 - step)		 
		iMin = ceili(R0 - step);

//%	if RMax1(iBeam) > (R0*1.25)
//%	    sub(sub < (R0+WMin)) = [];
//%	end
	if (CenterPoint > (DWORD)((R0+1)*R0Seuil-1.0f) && iMin < R0 + WMin )
		iMin = ceili(R0 + WMin);

//%	Masque(sub, iBeam) = 1;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	int iS;

	if (m_dwDeviceId != 7111) {
		for (iS = iMin; iS < iMax; iS++)
		{
			m_pMatrix[iS][Beam] = data[iS*m_dwReceiveBeams+Beam].amp;
		}
	}

	m_pMaskMin[Beam] = min(m_pMaskMin[Beam], (DWORD)iMin);
	m_pMaskMax[Beam] = max(m_pMaskMax[Beam], (DWORD)iMax);
	m_MaskMin = min(m_MaskMin, iMin);
	m_MaskMax = max(m_MaskMax, iMax);

	return true;
}


// --------------------------------------------------------------------
// _suppress_steps - removes "stacking" from estimation
//
// Return value: none
//
// [INPUT]   m_pR1 (on reduced matrix)
// [TEMP]    m_pfBeamsA, m_pfBeamsB, m_pfBeamsC
//
// [MATLAB] y      === SamplesFiltre
// [MATLAB] R      === m_pR1 (on reduced matrix)
// [MATLAB] Teta   === not used in MatLab / not calculated
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_mask_7111
//
// MatLab reference:
//		suppres_steps_V2
//
void _suppress_steps (float * SamplesFiltre)
{
	float * iCenter	= m_pfBeamsA;
	float * rCenter	= m_pfBeamsB;
	int     nCenter = 0;
	float * indeces = m_pfBeamsC;

	DWORD * R1NaN   = (DWORD*) m_pR1;
	DWORD * FiltNaN = (DWORD*) SamplesFiltre;

	int iBeg = -1, iEnd, iSub;

	for (iSub = 0; iSub < (int)m_dwReceiveBeams; iSub++)
	{
		indeces[iSub] = (float)iSub;

//%	sub = find(~isnan(R)); 
		if (ISNAN(R1NaN[iSub]))
			continue;

		if (iBeg == -1)
		{
//%	iBeg = sub(1);
//%	iEnd = sub(1);
			iBeg = iSub;
			iEnd = iSub;
			continue;
		}

		if (m_pR1[iEnd] == m_pR1[iSub])
		{
			iEnd = iSub;
			continue;
		}

//%	iCenter = (iBeg + iEnd) / 2;
		iCenter[nCenter] = ((float)(iBeg + iEnd)) / 2.0f;
//%	R1Samples = floor((RMax1 -0.5) * step);

		// !ALTRAN - 9 begin
		rCenter[nCenter] = ((float)m_pR1[iBeg] - 0.5f) * m_ReduiteStep - 1.0f;
		// !ALTRAN end
		nCenter++;
		iBeg = iSub;
		iEnd = iSub;
	}

//%	if length(iBeg) == 1
//%	    y = R;
//%	    return
//%	end

	if (nCenter == 0)
	{
		for (iSub = 0; iSub < (int)m_dwReceiveBeams; iSub++)
		{
			if (m_pR1[iSub] == 0)
				NAN(FiltNaN[iSub]);
			else
				// !ALTRAN - 10 begin
				SamplesFiltre[iSub] = ((float)m_pR1[iBeg] - 0.5f) * m_ReduiteStep - 1.0f;
				// !ALTRAN end
		}

		return;
	}

	iCenter[nCenter] = ((float)(iBeg + iEnd)) / 2.0f;
	// !ALTRAN - 11 begin
	rCenter[nCenter] = ((float)m_pR1[iBeg] - 0.5f) * m_ReduiteStep - 1.0f;
	// !ALTRAN
	nCenter++;

//%	nx = length(R);
//%	y = interp1(iCenter, R(iBeg), 1:nx, 'linear', 'extrap');
	Interp1 (iCenter, rCenter, nCenter, indeces, SamplesFiltre, m_dwReceiveBeams);

//%	y(1:(sub(1)-1)) = NaN;
	for (iSub = 0; iSub < (int)m_dwReceiveBeams && ISNAN(R1NaN[iSub]); iSub++)
	{
		NAN(FiltNaN[iSub]);
	}

//%	y((sub(end)+1):nx) = NaN;
	for (iSub = m_dwReceiveBeams-1; iSub >= 0 && ISNAN(R1NaN[iSub]); iSub--)
	{
		NAN(FiltNaN[iSub]);
	}

	return;
}




// --------------------------------------------------------------------
// _mask_7111 - implementation of imdilate on AmplitudeMask
//
// Return value: none
//
// [OUTPUT]  m_pMatrix   - Amplitude Mask
// [INPUT]   m_pRAWData  - Original Amplitude Image
// [INPUT]   m_MaskMin   - Minimum valid sample
// [INPUT]   m_MaskMax   - Maximum valid sample
// [TEMP]    m_pfBeamsD
//
// [MATLAB] Masque      === m_pMatrix
// [MATLAB] se          === dilate (= length(se))
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_mask_7111
//
// MatLab reference:
//		ResonMasqueAmplitude_7111_V2(199:200)
//
void _mask_dilate(DWORD * beams, WORD nBeams, WORD dilate)
{
	int iB, iS, iNB;
	int nB = dilate/2;

	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	_raw_data_type * RawSample;
	float * AmpSample;

	for (iS = m_MaskMin; iS < m_MaskMax; iS++)
	{
		AmpSample = m_pMatrix[iS];
		memcpy(m_pdBeamsB, AmpSample, sizeof(float) * m_dwReceiveBeams);
		RawSample = &data[iS*m_dwReceiveBeams];
		
		for (iB = 0; iB < nBeams; iB++)
		{
			if (!ISNAN(m_pdBeamsB[beams[iB]]))
			{
				for (iNB = max(iB-nB,0); iNB < min(iB+nB+1, nBeams); iNB++)
				{
					AmpSample[beams[iNB]] = (float)(RawSample[beams[iNB]].amp);
				}
			}
		}
	}
}



// --------------------------------------------------------------------
// _mask_7111 - final determination for the mask for 7111
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT]  m_pMaskMin, m_pMaskMax
// [INPUT]   m_pR1, m_pR2
// [INPUT]   m_pNominalBeams
// [TEMP]    m_pfBeamsA, m_pfBeamsB, m_pfBeamsC, 
// [TEMP]    m_pfBeamsD             - R1SamplesFiltre
// [TEMP]    m_pdBeamsA				- MaskWidth
// [TEMP]    m_pdBeamsB, m_pdBeamsC - min/max natural mask
//
// [MATLAB] SystemSerialNumber      === Not relevant since separated functions are used
// [MATLAB] AmpImage                === 
// [MATLAB] BeamAngles              === m_pBeamAngles
// [MATLAB] ReceiveBeamWidth        === m_fReceiveBeamWidth
// [MATLAB] R0                      === m_R0       -- still on reduced matrix
// [MATLAB] RMax1                   === m_pR1      -- still on reduced matrix
// [MATLAB] RMax2                   === m_pR2      -- still on reduced matrix, double of actual value
// [MATLAB] R1SamplesFiltre         === calculated inside
// [MATLAB] AmpPingNormHorzMax      === not used in MatLab / not calculated
// [MATLAB] NbSamplesForPulseLength === calculated inside
// [MATLAB] MaskWidth               === calculated inside
// [MATLAB] MasqueAmp               === as m_pMaskMin/m_pMaskMax limits
//
// Sub-functions calls:
//		
// Used in sub-functions:
//      _suppress_steps - m_pfBeamsA, m_pfBeamsB, m_pfBeamsC
//
// Used in functions:
//		CalculateMask
//
// MatLab reference:
//		SampleBeam_PreprocAmpMask_7111_V6 (309:337)
//		ResonMasqueAmplitude_7111_V2
//
bool _mask_7111(  )
{
	int iB, iNB, iS;
	DWORD *R1NaN    = (DWORD*) m_pR1;
	DWORD *R2NaN    = (DWORD*) m_pR2;
	DWORD *FiltNaN  = (DWORD*) m_pfBeamsD;

//	REFERENCE:	SampleBeam_PreprocAmpMask_7111_V6 (309:337)
	
	// Local Time spreading of the signal around the maximum
	
//%	R1Samples = floor((RMax1 -0.5) * step);
//%	R1SamplesFiltre = suppres_steps_V2(R1Samples, Teta);

	float * R1SamplesFiltre = m_pfBeamsD;
	_suppress_steps(R1SamplesFiltre);

	DWORD * MaskWidth       = m_pMaskWidth;

//%	NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth);
	DWORD NbSamplesForPulseLength = ceildw(m_fTransmitPulseLength*m_fSampleRate);
	double MaskWidth1, MaskWidth2;
	double RBW2 = (double)m_fReceiveBeamWidth/2.0;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if(!ISNAN(FiltNaN[iB]))
			R1SamplesFiltre[iB] = floorf(R1SamplesFiltre[iB]);

//%	RMax1 = floor((RMax1 -0.5) * step);
		if (!ISNAN(R1NaN[iB]))
		{
			// !ALTRAN - 15 begin
			MaskWidth1 = ((double)m_pR1[iB] - 0.5) * m_ReduiteStep - 1.0;
			// !ALTRAN end
			m_pR1[iB]= floorf((float)MaskWidth1);
		}
		else
		{
			MaskWidth1 = 0.0;
		}

//%	RMax2 = floor((RMax2 -0.5) * step);
		if (!ISNAN(R2NaN[iB]))
		{
			// !ALTRAN - 15 begin
			MaskWidth2 = ((double)m_pR2[iB] - 0.5) * m_ReduiteStep - 1.0;
			// !ALTRAN end
			m_pR2[iB]= floorf((float)MaskWidth2);
		}
		else
		{
			MaskWidth2 = 0.0;
		}

		MaskWidth1 = max(MaskWidth1, MaskWidth2);

		if (MaskWidth1)
		{
//%	RxSamples = OriginalRange(RMaxx, 0, step);	=== R = (r+(0-1)-0.5) * step;
			MaskWidth1 -= m_ReduiteStep - 1;
//%	Zx = RxSamples .* cosd(Teta);
			MaskWidth1 *= cos(m_pBeamAngles[iB]);
//% MaskWidthx = ceil(Zx.* abs(( 1./ cosd(abs(Teta) + ReceiveBeamWidth/2))  - ( 1./ cosd(abs(Teta) - ReceiveBeamWidth/2))));
			MaskWidth1 *= fabs( (1/cos(fabs(m_pBeamAngles[iB]) + RBW2)) - ( 1/cos(fabs(m_pBeamAngles[iB]) - RBW2) ) );
		}

//%	MaskWidth = max(step, MaskWidth);
		MaskWidth[iB] = ceildw(MaskWidth1);
		MaskWidth[iB] = max((DWORD)m_ReduiteStep, MaskWidth[iB]);

		// Increase of 4 times the pulse width
		
//%	MaskWidth = MaskWidth + 4 * NbSamplesForPulseLength;
		MaskWidth[iB] += 4 * NbSamplesForPulseLength;
	}

//%	R0 = OriginalRange(R0, 0, step);
	float R0 = ((float)m_R0 - 0.5f) * m_ReduiteStep - 1.0f;
	m_R0 = R0;

	WORD nominalBeams;
	DWORD maskMin = m_iOutputSamples;
	WORD step    = ceilw((float)m_iOutputSamples/(float)REDUCTION_SAMPLE_SIZE);

	for (iB = nominalBeams = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (m_pNominalBeams[iB])
		{
			if (m_dwReceiveBeams == 301 && !m_pR1[iB])
				continue;

			m_pdBeamsD[nominalBeams]   = (DWORD)iB;
			m_pfBeamsA[nominalBeams++] = (float)m_pR1[iB];
	
			if (!ISNAN(R1NaN[iB]) && MaskWidth[iB] < maskMin) 
				maskMin = MaskWidth[iB];
		}
	}

	if (!nominalBeams)
		return false;

	_gradient(m_pfBeamsA, m_pfBeamsB, nominalBeams);

	if (m_Debug&DEBUG_OUTPUT_MASKAMP_LONG)
	{
		m_pRangePhase[0] = (float)maskMin;
		m_pRangePhase[1] = step;
		m_pRangePhase[2] = nominalBeams;
	}

	DWORD Grad, Width;
	m_MaskMin = m_iOutputSamples;
	m_MaskMax = 0;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	DWORD iBeam;

	for (iB = 0; iB < nominalBeams; iB++)
	{
		iBeam = m_pdBeamsD[iB];

		if (m_pfBeamsB[iB] < 0)
			Grad = 0;
		else
		{
			Grad = floordw(2.0f*m_pfBeamsB[iB]+0.5f);
				
			if (iB == 0 && ISNAN(R1NaN[m_pdBeamsD[iB]]))
				Grad = 0;
			else if (iB > 0 && ISNAN(R1NaN[m_pdBeamsD[iB-1]]))
				Grad = 0;
				
			if (iB == nominalBeams-1 && ISNAN(R1NaN[m_pdBeamsD[iB]]))
				Grad = 0;
			else if (iB < nominalBeams-1 && ISNAN(R1NaN[m_pdBeamsD[iB+1]]))
				Grad = 0;
		}

		Width = max(MaskWidth[iBeam], Grad);
		m_pMaskMin[iBeam] = m_iOutputSamples;
		m_pMaskMax[iBeam] = 0;

		if (Width)
		{
			if (!ISNAN(R1NaN[iBeam]))
				_mask_beam_single((WORD)iBeam, m_pR1[iBeam], maskMin, Width, Width, 1.25f, step, R0, true);

			if (!ISNAN(R2NaN[iBeam]))
				_mask_beam_single((WORD)iBeam, m_pR2[iBeam], maskMin, Width, Width, 1.25f, step, R0);

			if (!ISNAN(FiltNaN[iBeam]))
			{
				R1SamplesFiltre[iBeam] = floorf(R1SamplesFiltre[iBeam]);
				_mask_beam_single((WORD)iBeam, R1SamplesFiltre[iBeam],  maskMin, Width, Width, 1.25f, step, R0);
			}

			//%	iDeb = find(Masque(:, iBeam), 1, 'first');
			//%	iFin = find(Masque(:, iBeam), 1, 'last');
			//%	Masque(iDeb:iFin, iBeam)	= 1;
			
			for (iS = (int)m_pMaskMin[iBeam]; iS < (int)m_pMaskMax[iBeam]; iS++)
				m_pMatrix[iS][iBeam] = data[iS*m_dwReceiveBeams+iBeam].amp;
		}

		m_pMaskMin[iBeam] = m_pMaskMax[iBeam] = 0;
	}

	_mask_dilate(m_pdBeamsD, nominalBeams, 5);

	if (m_dwReceiveBeams < 300)
		return true;

	_raw_data_type * RawSample;
	float * AmpSample;
	DWORD * MaskSample;

	for (iS = m_MaskMin; iS < m_MaskMax; iS++)
	{
		AmpSample = m_pMatrix[iS];
		MaskSample = (DWORD*)m_pMatrix[iS];
		RawSample = &data[iS*m_dwReceiveBeams];
		
//%	k = subBeamsNominaux(1);
//%	for i=(k-1):-1:1
//%		Masque(:,i) = Masque(:,k);
//%	end

		if (!ISNAN(MaskSample[m_pdBeamsD[0]]))
		{
			for (iB = 0; iB < (int)m_pdBeamsD[0]; iB++)
				AmpSample[iB] = (float)(RawSample[iB].amp);
		}

//% for i=1:(length(subBeamsNominaux)-1)
//%     k1 = subBeamsNominaux(i)+1;
//%     k2 = subBeamsNominaux(i+1)-1;
//%     for k=k1:k2
//%         Masque(:,k) = Masque(:,subBeamsNominaux(i)) | Masque(:,subBeamsNominaux(i+1));
//%     end
//% end

		for (iNB = 1; iNB < nominalBeams; iNB++)
			if (!ISNAN(MaskSample[m_pdBeamsD[iNB-1]]) || !ISNAN(MaskSample[m_pdBeamsD[iNB]]))
			{
				for (iB = (int)m_pdBeamsD[iNB-1]+1; iB < (int)m_pdBeamsD[iNB]; iB++)
					AmpSample[iB] = (float)(RawSample[iB].amp);
			}

//% k = subBeamsNominaux(end);
//% for i=(k+1):301
//%     Masque(:,i) = Masque(:,k);
//% end

		if (!ISNAN(MaskSample[m_pdBeamsD[nominalBeams-1]]))
		{
			for (iB = (int)m_pdBeamsD[nominalBeams-1]+1; iB < (int)m_dwReceiveBeams; iB++)
				AmpSample[iB] = (float)(RawSample[iB].amp);
		}
	}

	return true;
}



// --------------------------------------------------------------------
// _mask_7111 - final determination for the mask for 7111
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT]  m_pMaskMin, m_pMaskMax
// [INPUT]   m_pR1, m_pR2
// [INPUT]   m_pNominalBeams
// [TEMP]    m_pfBeamsA, m_pfBeamsB, m_pfBeamsC, 
// [TEMP]    m_pfBeamsD             - R1SamplesFiltre
// [TEMP]    m_pdBeamsA				- MaskWidth
// [TEMP]    m_pdBeamsB, m_pdBeamsC - min/max natural mask
//
// [MATLAB] SystemSerialNumber      === Not relevant since separated functions are used
// [MATLAB] AmpImage                === 
// [MATLAB] BeamAngles              === m_pBeamAngles
// [MATLAB] ReceiveBeamWidth        === m_fReceiveBeamWidth
// [MATLAB] R0                      === m_R0       -- still on reduced matrix
// [MATLAB] RMax1                   === m_pR1      -- still on reduced matrix
// [MATLAB] RMax2                   === m_pR2      -- still on reduced matrix, double of actual value
// [MATLAB] R1SamplesFiltre         === calculated inside
// [MATLAB] AmpPingNormHorzMax      === not used in MatLab / not calculated
// [MATLAB] NbSamplesForPulseLength === calculated inside
// [MATLAB] MaskWidth               === calculated inside
// [MATLAB] MasqueAmp               === as m_pMaskMin/m_pMaskMax limits
//
// Sub-functions calls:
//		
// Used in sub-functions:
//      _suppress_steps - m_pfBeamsA, m_pfBeamsB, m_pfBeamsC
//
// Used in functions:
//		CalculateMask
//
// MatLab reference:
//		SampleBeam_PreprocAmpMask_7150_V4 (423:537)
//		ResonMasqueAmplitude_7111_V2 -- ???
//
bool _mask_7150(  )
{
	int iB, iNB;
	DWORD *R1NaN    = (DWORD*) m_pR1;
	DWORD *R2NaN    = (DWORD*) m_pR2;
	DWORD *FiltNaN  = (DWORD*) m_pfBeamsD;

//	REFERENCE:	SampleBeam_PreprocAmpMask_7150_V4 (423:537)
	
	// Local Time spreading of the signal around the maximum
	
//%	R1Samples = floor((RMax1 -0.5) * step);
//%	R1SamplesFiltre = suppres_steps_V2(R1Samples, Teta);

	float * R1SamplesFiltre = m_pfBeamsD;
	_suppress_steps(R1SamplesFiltre);

	DWORD * MaskWidth = m_pMaskWidth;
	float   RBW2      = m_fReceiveBeamWidth/2.0f;

	DWORD * NaNA = (DWORD *)m_pfBeamsA;
	DWORD * NaNB = (DWORD *)m_pfBeamsB;
	DWORD * NaNC = (DWORD *)m_pfBeamsC;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (iB == 100)
			int toto=1 ;
		if (!ISNAN(R1NaN[iB]))
			// !ALTRAN - 15 begin
			m_pR1[iB]= floorf((m_pR1[iB] - 0.5f) * m_ReduiteStep - 1.0f);
			// !ALTRAN end

		if (!ISNAN(R2NaN[iB]))
			// !ALTRAN - 16 begin
			m_pR2[iB]= floorf((m_pR2[iB] - 0.5f) * m_ReduiteStep - 1.0f);
			// !ALTRAN end

		if (!ISNAN(FiltNaN[iB]))
		{
//%	Z = -R1SamplesFiltre .* cosd(Teta);
//% X = R1SamplesFiltre .* sind(Teta);
			m_pfBeamsA[iB] = - (R1SamplesFiltre[iB] + 1) * cosf(m_pBeamAngles[iB]);	// m_pfBeamsA === Z
			m_pfBeamsB[iB] =   (R1SamplesFiltre[iB] + 1) * sinf(m_pBeamAngles[iB]);	// m_pfBeamsB === X
			
//%	MaskWidth = ceil(-Z.* abs(( 1./ cosd(abs(Teta) + ReceiveBeamWidth/2))  - ( 1./ cosd(abs(Teta) - ReceiveBeamWidth/2))));
			MaskWidth[iB] = ceildw(-m_pfBeamsA[iB] * fabsf( (1/cosf(fabsf(m_pBeamAngles[iB]) + RBW2)) - ( 1/cosf(fabsf(m_pBeamAngles[iB]) - RBW2) ) ) );
		}
		else
		{
			NAN(MaskWidth[iB]);
			NAN(NaNA[iB]);
			NAN(NaNB[iB]);
		}
	}

//%	R0 = OriginalRange(R0, 0, step);
	float R0 = ((float)m_R0 - 0.5f) * m_ReduiteStep - 1.0f;
	m_R0 = R0;
	
//%	Slope = atan2(gradient(Z), gradient(X)) * (180/pi);
	_gradient(m_pfBeamsA, m_pfBeamsC, m_dwReceiveBeams);	// m_pfBeamsC === gradient(Z)
	_gradient(m_pfBeamsB, m_pfBeamsA, m_dwReceiveBeams);	// m_pfBeamsA === gradient(A)

//%	NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth);
	DWORD NbSamplesForPulseLength = ceildw(m_fTransmitPulseLength*m_fSampleRate);
	float MaskWidth1, Slope;
	float * DiffMaskWidth  = m_pfBeamsB;
	float * DiffMaskWidth2 = m_pfBeamsA;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (!ISNAN(NaNC[iB]) && !ISNAN(NaNA[iB]) && !ISNAN(MaskWidth[iB]))
		{
//% MaskWidth  = ceil(MaskWidth .* cosd(Teta) ./ cosd(min(85, abs(Teta-Slope))));
//% MaskWidth(isnan(Slope)) = NaN;
//% DiffMaskWidth = MaskWidth - MaskWidth0;
//% DiffMaskWidth(DiffMaskWidth < 0) = 0;
			Slope              = min(D2Rf(85.0f), fabsf(m_pBeamAngles[iB] - atan2f(m_pfBeamsC[iB], m_pfBeamsA[iB])));
			MaskWidth1         = ceilf(MaskWidth[iB] * cosf(m_pBeamAngles[iB]) / cosf(Slope));
			DiffMaskWidth[iB]  = max(0.0f, MaskWidth1 - (float)MaskWidth[iB]);
		}
		else
			NAN(NaNB[iB]);

//%	DiffMaskWidth2 = DiffMaskWidth;
		DiffMaskWidth2[iB] = DiffMaskWidth[iB];
	}

//%	L = 10;
//%	for iBeam=1+L:nbBeams-L
//%		S = DiffMaskWidth(iBeam-L:iBeam+L);
//%	    DiffMaskWidth2(iBeam) = max(abs(S));
//%	end

	int L = 10;
	for (iB = L; iB < (int)m_dwReceiveBeams-L-1; iB++)
	{
		for (iNB = iB-L; iNB <= iB+L; iNB++)
		{
			if (ISNAN(NaNB[iNB]))
				continue;

			if (ISNAN(NaNA[iB]) || DiffMaskWidth2[iB] < DiffMaskWidth[iNB])
				DiffMaskWidth2[iB] = DiffMaskWidth[iNB];
		}
	}

//% MaskWidth = MaskWidth0 + DiffMaskWidth2;
//% MaskWidth = max(MaskWidth, MaskWidth0);
//% MaskWidth = max(step, MaskWidth);
//% MaskWidth = MaskWidth + 1 * NbSamplesForPulseLength;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (ISNAN(MaskWidth[iB]))
		{
			MaskWidth[iB] = m_ReduiteStep+NbSamplesForPulseLength;
			continue;
		}

		if (!ISNAN(NaNA[iB]))
			MaskWidth[iB] += ceildw(DiffMaskWidth2[iB]);

		MaskWidth[iB]  = max(MaskWidth[iB], (DWORD)m_ReduiteStep);
		MaskWidth[iB] += NbSamplesForPulseLength;
	}


	DWORD maskMin = m_iOutputSamples;
	WORD step    = ceilw((float)m_iOutputSamples/(float)REDUCTION_SAMPLE_SIZE);
	float width; //, beam, beammax = (float)m_dwReceiveBeams-1;

	_gradient(R1SamplesFiltre, m_pfBeamsA, m_dwReceiveBeams);		// m_pfBeamsA === gradient(R1SamplesFiltre)

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		m_pdBeamsD[iB] = (DWORD) iB;
//		beam = (float)iB;
//		beam -= ((beam > beammax/2.0f) ? 1.0f : 0.0f);
//		beam /= beammax;
		width = (float)MaskWidth[iB];
		width += 4*NbSamplesForPulseLength;
//		width *= fabsf(beam - 0.5f) * 6.0f + 2.0f;

		if (!ISNAN(NaNA[iB]))												// m_pfBeamsB === w
			m_pfBeamsB[iB] = max(width, 2*m_pfBeamsA[iB]);
		else
			m_pfBeamsB[iB] = width;

		if (MaskWidth[iB] < maskMin) 
			maskMin = MaskWidth[iB];
	}

	m_MaskMin = m_iOutputSamples;
	m_MaskMax = 0;
	DWORD w1, w2;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (iB==100)
			int toto=1 ;

		w1 = ceildw(m_pfBeamsB[iB]);
		//w2 = ceildw(2*m_pfBeamsB[iB]);
		w2 = w1 ; //EPM just trying to improve the symetrie of the mask

		if (!ISNAN(R1NaN[iB]))
			_mask_beam_single((WORD)iB, m_pR1[iB], maskMin, w1, w2, 1.15f, step, R0);

		if (!ISNAN(R2NaN[iB]))
			_mask_beam_single((WORD)iB, m_pR2[iB], maskMin, w1, w2, 1.15f, step, R0);

		if (!ISNAN(FiltNaN[iB]))
		{
			R1SamplesFiltre[iB] = floorf(R1SamplesFiltre[iB]);
			_mask_beam_single((WORD)iB, R1SamplesFiltre[iB], maskMin, w1, w2, 1.15f, step, R0);
		}
	}


	_mask_dilate(m_pdBeamsD, (WORD)m_dwReceiveBeams, 5);

	return true;
}


// --------------------------------------------------------------------
// _detection_estimate   - rough estimation of weighted averaged per beam
//                         bottom detect points on the reduced amplitude matrix.
//
// Return value: Success(true)/Fail(false)
//
// [INPUT]  m_pRAWData;
// [OUTPUT] m_pMaskMin,  m_pMaskMax
// [OUTPUT] m_pR1, m_pR2, m_R0
// [OUTPUT] m_IncidentBeam
// [TEMP]   m_pfBeamsA, m_pfBeamsB
// [TEMP]   m_pfSamplesA
//
// [MATLAB] Amp                === m_pRAWData
// [MATLAB] step               === m_ReduiteStep (calculated inside)
// [MATLAB] Teta               === m_pBeamAngles (in radians)
// [MATLAB] ReceiveBeamWidth   === m_fReceiveBeamWidth
// [MATLAB] SampleRate         === m_fSampleRate
// [MATLAB] TxPulseWidth       === m_fTransmitPulseLength
// [MATLAB] DepthMin           === m_pGatesMin (in samples per beam)
// [MATLAB] DepthMax           === m_pGatesMax (in samples per beam)
// [MATLAB] R0                 === m_R0
// [MATLAB] RMax1              === m_pR1
// [MATLAB] RMax2              === m_pR2
// [MATLAB] R1SamplesFiltre    === calculated at later stage
// [MATLAB] iBeamMax0          === m_IncidentBeam
// [MATLAB] AmpPingNormHorzMax === not used in MatLab / not calculated
// [MATLAB] MaskWidth          === (calculated at later time)
// [MATLAB] iBeamBeg           === Returned as IF1_QUALITY_QF2_PASS in m_pDetectionType
// [MATLAB] iBeamEnd           === Returned as IF1_QUALITY_QF2_PASS in m_pDetectionType
//
// Sub-functions calls:
//		_r0_estimation
//		_matrix_reduction
//		_matrix_compensation
//		_amplitude_single_reduit
//		
// Used in sub-functions:
// [TEMP]   m_pdBeamsA, m_pdBeamsB
// [TEMP]   m_pfBeamsA, m_pfBeamsB
// [TEMP]   m_pfSamplesA, m_pfSamplesB
// [TEMP]   m_pfSamplesC, m_pfSamplesD
// [TEMP]   m_pfSamplesE
//
// Used in functions:
//		CalculateMask
//
// MatLab reference:
//		SampleBeam_ReduceMatrixAmplitude
//		SampleBeam_PreprocAmpMask_7111_V6


bool _detection_estimate( void )
{
	int iB, iS;
	float R0Marge, coefEnlargeMask;

	// !ALTRAN - 43 begin
	if(m_dwDeviceId == 7150)
	{
		coefEnlargeMask = 1;
		R0Marge = 4;
	}
	else if(m_dwDeviceId == 7111)
	{
		coefEnlargeMask = 4;
		R0Marge = 0;
	}
	else  // 7125
	{
		coefEnlargeMask = 4;
		R0Marge = 0;
	}

	// !ALTRAN End

	_matrix_reduction();
    
//%	Amp = Amp(1:end-2,:);
	m_ReduiteSize -= 2;
	
	float R0_Original;
	DWORD * R1NaN = (DWORD *) m_pR1;
	DWORD * R2NaN = (DWORD *) m_pR2;
	
	NANf(R0_Original);
	NANf(m_R0);

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		NAN(R1NaN[iB]);
	}

	if (_r0_estimation(1,10.0f))
	{
		R0_Original = m_R0;
		memcpy(m_pR2, m_pR1, sizeof(float)*m_dwReceiveBeams);
	}
    
	_matrix_compensation();
    
	
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		NAN(R1NaN[iB]);
	}
	
	if (!_r0_estimation(0,4.0f))
		NANf(m_R0);

	// !ALTRAN - 43 begin
	if(m_IdentAlgo != 3)
	{
		//if R0_OnOriginal > (R0+R0Marge)
        //	R0 = R0_OnOriginal;
        //	subFaux = find(R0Strip < R0_OnOriginal);
        //	R0Strip(subFaux) = R0StripOriginal(subFaux);
		//end
		if(R0_Original > (m_R0+R0Marge))
		{
			m_R0 = R0_Original;
			for(int i=0; i<m_dwReceiveBeams;i++)
			{
				if(m_pR1[i] < m_pR2[i])
				{
					m_pR1[i] = m_pR2[i];
				}
			}
		}

	}
	// !ALTRAN End

	
	bool flagMerge = false;

	if (m_dwDeviceId == 7111)
	{
		flagMerge = !ISNANf(R0_Original) && !ISNANf(m_R0) && R0_Original > m_R0;
	}
	else if (m_dwDeviceId == 7150)
	{
		if (!ISNANf(R0_Original) && ISNANf(m_R0))
			m_R0 = R0_Original;
		else
			flagMerge = !ISNANf(R0_Original) && !ISNANf(m_R0) && R0_Original > m_R0+4;
	}

	if (flagMerge)
	{
		m_R0 = R0_Original;
		
		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
		{
			if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < R0_Original) 
				m_pR1[iB] = m_pR2[iB];
		}
	}

	if (ISNANf(m_R0))
		return false;
	
/*
	Sfloat * testR1 = (Sfloat *)m_pR1;
	Sfloat * testR2 = (Sfloat *)m_pR2;
	float    f1 = 2.1f, f2 = 1.1f;
	Sfloat * testF1 = (Sfloat *)&f1;
	Sfloat * testF2 = (Sfloat *)&f2;
*/


	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
/*
		f1 = 1.9f*m_pR2[iB]+0.9f;
		f2 = 2.1f*m_pR2[iB]+1.1f;
*/
		if (!ISNAN(R1NaN[iB]) && !ISNAN(R2NaN[iB]) 
			&& (m_pR1[iB] >= 1.9f*m_pR2[iB]+0.89f) && (m_pR1[iB] <= 2.1f*m_pR2[iB]+1.11f))
		{
			m_pR1[iB] = m_pR2[iB];
		}
	}

	if (m_Debug&DEBUG_OUTPUT_PREPROC_LONG)
	{
		m_pRangePhase[0] = R0_Original;
	}
	
	R0_Original = m_R0;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		m_pR2[iB] = m_pR1[iB];
		if (!ISNAN(R1NaN[iB]))
		{
			m_pR1[iB] = m_pR1[iB]*2.0f+2.0f;
		}
	}

	WORD ss = max((WORD)m_R0-2,2);

	if (!_r0_estimation(ss,2.0f))
		NANf(m_R0);

	if (m_Debug&DEBUG_OUTPUT_PREPROC_LONG)
	{
		m_pRangePhase[0] = R0_Original;
	}

	if (m_dwDeviceId == 7111)
	{
//% if R0 < R0Strip2
//%     sub = find(R0Strip < R02);
//%     R0Strip(sub) = R0Strip2(sub);
//%     R0 = R02;
//% end
// 		float R2Min = REDUCTION_MATRIX_SIZE;
// 		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
// 			if (ISNAN(R2NaN[iB]))
// 				R2Min = 0;
// 			else if (m_pR2[iB] < R2Min)
// 				R2Min = m_pR2[iB];

		if (m_R0 < R0_Original)
		{
			for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
			{
				if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < R0_Original) 
					m_pR1[iB] = m_pR2[iB];
			}

			m_R0 = R0_Original;
		}

	}

//%======================================(*1*)
//% AMedian = Inf(1,nbBeams);
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         AMedian(iBeam) = nanmedian(Amp(:,iBeam));
//%     end
//% end
//% 
//%======================================(*2*)
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         iSampleBeg = max(floor(R0-3 + (R0Strip(iBeam)-R0)/3), R0-3);
//%         iSampleEnd = min(floor(1.4*R0Strip(iBeam)), nbSamples);
//%         iSampleBeg = max(iSampleBeg, floor(Rmin(iBeam)));
//%         iSampleEnd = min(iSampleEnd, ceil(Rmax(iBeam)));
//%         
//%         Amp(iSampleEnd:end,iBeam) = NaN;
//%         Amp(1:iSampleBeg,iBeam) = NaN;
//%     else
//%         Amp(:,iBeam) = NaN;
//%     end
//% end
//% 
//%======================================(*3*)
//% Offset = R0-1;
//% Amp = Amp(Offset:end,:);
//% nbSamples = size(Amp,1);
//% 
//% %% Search for the maximum amplitude along each beam starting from R0
//% 
//% RMax1 = ones(1,nbBeams) * nbSamples;
//% AMax = zeros(1,nbBeams);
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         [aMax, rMax1] = max(Amp(:,iBeam));
//%         if aMax > 2*AMedian(iBeam)
//%             AMax(iBeam) =  aMax;
//%             RMax1(iBeam) = rMax1;
//%         end
//%     end
//% end
//% 
//%======================================(*4*)
//% for iBeam=1:nbBeams
//%     if flagBeams(iBeam)
//%         a = Amp(:,iBeam);
//%         iDeb = find(isnan(a(1:RMax1(iBeam))), 1, 'last');
//%         X = min(RMax1(iBeam)-iDeb, nbSamples-RMax1(iBeam));
//%         sub = RMax1(iBeam)-X:RMax1(iBeam)+X;
//%         sub(sub < 1) = [];
//%         sub(sub>length(a)) = [];
//%         if length(sub) < 5
//%             continue
//%         end
//%         rMax1 = detectionAmplitudePingOnCompensatedImage(1:size(Amp,1), a(sub), 1, iPing, iBeam, Teta(iBeam), DisplayLevel, nbSamples);
//%         if ~isnan(rMax1)
//%             RMax1(iBeam) = rMax1 + (sub(1)-1);
//%         end
//%     end
//% end
//% 
//%======================================(*5*)
//% RMax1 = floor(RMax1 + (Offset-1) - 0.5); % -0.5 apparently necessary (view on file 20080222_014920
//% RMax1(flagBeams == 0) = NaN;

	float BeamMedian, BeamMax;
	int   iSampleBeg, iSampleEnd, iSampleLength, R1Max;
	int   iSubBeg, iSubEnd, index, iSE, X;
	float R0     = m_R0;
	float rMax1;
	int   Offset = (int)m_R0-1;

//%	AMaxMoyen = nanmean(AMax);
//%	AMax2 = AMax .^ 2;				=== m_pfBeamsB
	float BeamMaxMean = 0;

	// m_pfBeamsA === AMax
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (m_pFlagBeams[iB])
		{
			BeamMedian = _median(m_pAmpReduite[iB], m_pfSamplesA, m_ReduiteSize);							// === (*1*)

			if (!ISNAN(R1NaN[iB]))
			{
				iSampleBeg = max(floori(R0-3 + ((float)m_pR1[iB]-R0)/3.0f), (int)m_R0-3);						// === (*2*)
				iSampleEnd = min(floori(1.4f*(m_pR1[iB]+1)-1), (int)m_ReduiteSize-1);			// +/-1 needed to match MatLab 1-based indexing
			}
			else
			{
				iSampleBeg = (int)m_R0-3;						// === (*2*)
				iSampleEnd = (int)m_ReduiteSize-1;				// +/-1 needed to match MatLab 1-based indexing
			}
			// !ALTRAN - 8 begin
			//iSampleBeg = max(iSampleBeg, floori((float)m_pGatesMin[iB]));
			//iSampleEnd = min(iSampleEnd, floori((float)m_pGatesMax[iB] + 1.0f));
			iSampleBeg = max(iSampleBeg, floori((float)m_pGatesMin[iB]/m_ReduiteStep));
			iSampleEnd = min(iSampleEnd, floori((float)m_pGatesMax[iB]/m_ReduiteStep + 1.0f));
			// !ALTRAN End

			BeamMax = 0.0f;																					// === (*3*)
			for (iS = max(iSampleBeg+1,Offset); iS < iSampleEnd; iS++)
			{
				if (m_pAmpReduite[iB][iS] > BeamMax)
				{
					BeamMax = m_pAmpReduite[iB][iS];
					R1Max   = iS;
				}
			}
		
			if (BeamMax > 2.0f*BeamMedian)
			{
				m_pfBeamsA[iB] = BeamMax;				// AMax
				m_pfBeamsB[iB] = BeamMax * BeamMax;		// AMax ^ 2
				BeamMaxMean   += BeamMax;
				m_pR1[iB]      = (float)R1Max-0.5f;
				R1Max++;
			}
			else
			{
				m_pfBeamsA[iB] = 0.0f;		// AMax
				m_pfBeamsB[iB] = 0.0f;		// AMax ^ 2

				if (m_dwDeviceId == 7150)
				{
					NAN(R1NaN[iB]);
					m_pFlagBeams[iB] = false;
				}
				else
				{
					m_pR1[iB]      = (float)(m_ReduiteSize-2);
				}

				continue;
			}

			// !ALTRAN - 30 Begin
			m_pMax[iB] = m_pAmpReduite[iB][R1Max-1];
			// !ALTRAN End

			// iDeb      === iSampleBeg - Offset + 1														// === (*4*)
			// RMax1     === R1Max - Offset
			// nbSamples === m_ReduiteSize - Offset
			iSampleLength = R1Max - iSampleBeg - 1;
			if (iSampleLength > m_ReduiteSize - R1Max)
			{
				iSampleLength = m_ReduiteSize - R1Max;
				index = 0;
			}
			else
			{
				index = 1;
			}

			// !ALTRAN - 28 begin
			//X = min(R1Max-iSampleBeg-1,m_ReduiteSize-R1Max-1);
			iSubBeg       = R1Max - iSampleLength-1+index;
			iSubEnd       = min(R1Max + iSampleLength-1+index, iSampleEnd);
			iSE           = min(R1Max + iSampleLength-1+index+1, m_ReduiteSize);
			// !ALTRAN end
			

			// !ALTRAN - 14 begin
 			//if (iSE - iSubBeg >= 5-index && iSampleBeg >= Offset)		// Need second condition, since if all NaNs are cut down by Amp = Amp(Offset:end,:)
 			//{														// statement find returns nothing.
				if ((rMax1 = _amplitude_single_reduit(m_pAmpReduite[iB], iSubBeg, iSubEnd, iB, index)) >= 0)
				//if ((rMax1 = _amplitude_single_reduit(m_pAmpReduite[iB], R1Max-X, R1Max+X, iB, index)) >= 0)
					// !ALTRAN - 12 begin
				{
					m_pR1[iB] = rMax1-(float)index;
				}
					// !ALTRAN end
 			//}
			// !ALTRAN end

			// !ALTRAN - 13 begin
			//if (m_dwDeviceId == 7150)
			//{
			//	m_pR1[iB] += 0.5f;
			//}
			// !ALTRAN end

			m_pR1[iB] = floorf(m_pR1[iB]);
		}
		else
		{
			NAN(R1NaN[iB]);
			m_pfBeamsA[iB] = 0.0f;
			m_pfBeamsB[iB] = 0.0f;		// AMax ^ 2
		}
	}

	BeamMaxMean /= m_dwReceiveBeams;


	// Identification of the beam number corresponding to R0
//%	pppp = RMax1;
//%	AMaxMoyen = nanmean(AMax);			=== BeamMaxMean
//%	pppp(AMax < AMaxMoyen) = Inf;
//%	[pppp, iBeamMax0] = min(pppp);
// Old Way
// 	m_IncidentBeam = (WORD)m_dwReceiveBeams;
// 	int iBeamMax0  = (int)m_dwReceiveBeams, iSFull = REDUCTION_MATRIX_SIZE;
// 	iS = REDUCTION_MATRIX_SIZE;
// 
// 	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
// 	{
// 		if (m_pR1[iB] > m_ReduiteSize) 
// 			NAN(R1NaN[iB]);
// 
// 		if (!ISNAN(R1NaN[iB]) && m_pfBeamsA[iB] >= BeamMaxMean && m_pR1[iB] < (float)iS)
// 		{
// 			iS = floori(m_pR1[iB]);
// 			m_IncidentBeam = iB;
// 		}
// 
// 		if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < (float)iSFull)
// 		{
// 			iSFull = floori(m_pR1[iB]);
// 			iBeamMax0 = iB;
// 		}
// 	}


//% [pppp, iBeamMax0Full] = min(RMax1);
//% sub = find(RMax1 == pppp);

	int BeamMax0 = (int)m_dwReceiveBeams;
	int BeamMax1 = (int)m_dwReceiveBeams;

	// !ALTRAN - 44 begin
	float pR1min = FLT_MAX;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++) 
	{
		if(m_pR1[iB] < pR1min) pR1min = m_pR1[iB];
	}
	// !ALTRAN End


	for (int iB = 0; iB < (int)m_dwReceiveBeams; iB++) 
	{
 		if (m_pR1[iB] > m_ReduiteSize) 
 			NAN(R1NaN[iB]);

		if (ISNAN(R1NaN[iB])) continue;
		
		// !ALTRAN - 44 begin
		//if (floori(m_pR1[iB]) < iS) {
		//	BeamMax0 = BeamMax1 = iB;
		//	iS = floori(m_pR1[iB]);
		//} else if (floori(m_pR1[iB]) == iS) {
		//	BeamMax1 = iB;
		//}
		if(m_pR1[iB] == pR1min && BeamMax0 == m_dwReceiveBeams) BeamMax0=iB;
		if(m_pR1[iB] == pR1min) BeamMax1=iB;
	}

	// !ALTRAN - 44 begin
	if(BeamMax0 == m_dwReceiveBeams && BeamMax1 == m_dwReceiveBeams)
	{
		// sub = find(RMax1 == pppp) empty
		NANf(m_R0);
		for(int i=0; i<m_dwReceiveBeams; i++) NANf(m_pR1[i]);
		for(int i=0; i<m_dwReceiveBeams; i++) NANf(m_pR2[i]);
		return false;
	}
	// !ALTRAN End

//% iBeamMax0Full = floor((sub(1) + sub(end))/2);
	WORD iBeamMax0Full = floorw((BeamMax0 + BeamMax1) / 2.0f);

//% pppp = RMax1;
//% AMaxMoyen = nanmean(AMax);			=== BeamMaxMean
//% pppp(AMax < AMaxMoyen) = Inf;
//% [qqqq, iBeamMax0] = min(pppp);
//% sub = find(pppp == qqqq);

	BeamMax0 = (int)m_dwReceiveBeams;
	BeamMax1 = (int)m_dwReceiveBeams;
	
	for (iB = 0, iS = REDUCTION_MATRIX_SIZE; iB < (int)m_dwReceiveBeams; iB++) 
	{
		if (m_pR1[iB] > m_ReduiteSize) 
			NAN(R1NaN[iB]);
		
		if (ISNAN(R1NaN[iB]) || (m_pfBeamsA[iB] < BeamMaxMean)) continue;
		
		if (floori(m_pR1[iB]) < iS) {
			BeamMax0 = BeamMax1 = iB;
			iS = floori(m_pR1[iB]);
		} else if (floori(m_pR1[iB]) == iS) {
			BeamMax1 = iB;
		}
	}

	// !ALTRAN - 45 begin
	//if (m_dwDeviceId == 7150) {
	// !ALTRAN End

//%		pppp = RMax1;
//%		AMaxMoyen = nanmean(AMax);
//%		pppp(AMax < AMaxMoyen) = Inf;
//%		[pppp, iBeamMax0] = min(pppp);
	//	m_IncidentBeam = BeamMax0;
	// !ALTRAN - 45 begin
	//} else {
	// !ALTRAN End

//% iBeamMax0 = floor((sub(1) + sub(end))/2);
	m_IncidentBeam = floorw((BeamMax0 + BeamMax1) / 2.0f);
//% MS2 = nbBeams / 2;
//% if abs(iBeamMax0Full-MS2) < abs(iBeamMax0-MS2)
//%     iBeamMax0 = iBeamMax0Full;
//% end

	float MS2 = m_dwReceiveBeams / 2.0f - 1.0f;
	if (fabs(iBeamMax0Full-MS2) < fabs(m_IncidentBeam-MS2))
		m_IncidentBeam = iBeamMax0Full;
	// !ALTRAN - 45 begin
	//}
	// !ALTRAN End
	

	if (m_IncidentBeam == 0 || m_IncidentBeam >= m_dwReceiveBeams-1)
		m_IncidentBeam = (WORD)(m_dwReceiveBeams/2-1);
	else
	{
		WORD k = m_IncidentBeam;
		for (k = m_IncidentBeam + 1; k < m_dwReceiveBeams-1; k++)
		{
			if (m_pR1[k] && m_pR1[k+1] && m_pR1[k] <= m_pR1[k+1])
				m_IncidentBeam = k + 1;
			else
				break;
		}

		for (k = m_IncidentBeam - 1; k > 0; k--)
		{
			if (m_pR1[k-1] && m_pR1[k] && m_pR1[k-1] <= m_pR1[k])
				m_IncidentBeam = k - 1;
			else
				break;
		}
	}


//%	pppp = cumsum(AMax2(1:iBeamMax0));
//%	pppp = pppp / max(pppp);
//%	iBeamBeg = find(pppp >= 0.005, 1, 'first');
//%	
//%	pppp = cumsum(AMax2(iBeamMax0:end));
//%	pppp = pppp / max(pppp);
//%	iBeamEnd = iBeamMax0 - 1 + find(pppp > 0.995, 1, 'first');

	_cumsum(m_pfBeamsB,                m_pfBeamsC,                m_IncidentBeam+1,                true);
	_cumsum(m_pfBeamsB+m_IncidentBeam, m_pfBeamsC+m_IncidentBeam, m_dwReceiveBeams-m_IncidentBeam, true);

	for (iB = 0; iB < (int)m_dwReceiveBeams-1 && m_pfBeamsC[iB] < 0.005f; iB++)
	{
		m_pDetectionType[iB] = 0;
	}

	m_pDetectionType[iB] = 0;			// Has to do with how find works

	// !ALTRAN - 31 Begin
	m_iBeamBeg = iB;
	// !ALTRAN End

	for (iB = m_dwReceiveBeams-1; iB >= m_IncidentBeam && m_pfBeamsC[iB] > 0.995f; iB--)
	{
		m_pDetectionType[iB] = 0;
	}

	// !ALTRAN - 32 Begin
	m_iBeamEnd = iB;
	// !ALTRAN End

	// Second serie of maximals
//%	RMax2 = RMax1;
//%	for iBeam=2:(nbBeams-1)
//%		RMax2(iBeam) = (RMax1(iBeam-1)+RMax1(iBeam+1)) / 2;
//%	end
//%	R0 = max(R0,min(RMax1));

	iS = REDUCTION_MATRIX_SIZE;
	m_pR2[0] = m_pR1[0];
	m_pR2[m_dwReceiveBeams-1] = m_pR1[m_dwReceiveBeams-1];
	for (iB=1; iB<(int)m_dwReceiveBeams-1;iB++)
	{
		if (ISNAN(R1NaN[iB-1]) || ISNAN(R1NaN[iB+1]))
			NAN(R2NaN[iB]);
		else
			m_pR2[iB] = (m_pR1[iB-1] + m_pR1[iB+1]) / 2.0f;

		if (!ISNAN(R1NaN[iB]) && m_pR1[iB] < (float)iS) 
			iS = floori(m_pR1[iB]);
	}

	if (iS < REDUCTION_MATRIX_SIZE)
	{
		m_R0 = min(m_R0, (float)iS);
	}

	return true;
}


void MasqueAmp(void)
{
	DWORD *MaskFrom = (DWORD *)m_pMatrix[0];
	DWORD *MaskTo   = (DWORD *)m_pAmpReduite[0];
	DWORD  mask;

	int iB, iS, jS, nj;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		for (iS = 0; iS < m_iOutputSamples; iS += 32)
		{
			nj = min (32, m_iOutputSamples - iS);

			mask = 0;

			for (jS = 0; jS < nj; jS++)
			{
				if (!ISNAN(MaskFrom[(iS+jS)*m_dwReceiveBeams+iB]))
					mask |= 1 << jS;
			}

			MaskTo[iB*m_nMaskSamples+(iS >> 5)] = mask;
		}
	}
}

bool MasqueAmp(int beam, int sample)
{
	DWORD *Mask = (DWORD *)m_pAmpReduite[0];
	
	// !ALTRAN - 34 Begin
	return (Mask[beam*m_nMaskSamples+(sample >> 5)] & (1 << (sample & 0x1f))) ? true : false;
	//return true;
	// !ALTRAN End
}

//------------------------------------------------------------------------------
// Preparation of phase.
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData;
// [INPUT]  m_MaskMin,  m_MaskMax
// [INPUT]  m_pMatrix (on Amplitude)
// [OUTPUT] m_pMatrix (on Phase)
//
// [TEMP] m_pfSamplesA   === aF
// [TEMP] m_pfSamplesB   === bF
//
bool _phase_filtering( void )
{
	int iB, iS;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	DWORD *Mask = (DWORD *)m_pMatrix[0];
	DWORD * R1NaN = (DWORD *) m_pR1;
	DWORD * aFNaN = (DWORD *) m_pfSamplesA;
	DWORD * bFNaN = (DWORD *) m_pfSamplesB;
	int iDeb, iFin, iBeg, iEnd;
	int n = 4;
	bool wasNaN;

	MasqueAmp();		// Preserve Amplitude Masque

	m_MaskMin = m_iOutputSamples;
	m_MaskMax = 0;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (ISNAN(R1NaN[iB]))
		{
			for (iS = 0; iS < m_iOutputSamples; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
			}
		}
		else
		{
			iDeb = max(floori(m_pR1[iB]) - 2*(int)m_pMaskWidth[iB], n);
			iFin = min(floori(m_pR1[iB]) + 2*(int)m_pMaskWidth[iB] + 1, m_iOutputSamples-n);

			iBeg = iDeb - n;
			iEnd = iFin + n;

			for (iS = 0; iS < iBeg; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
			}

			for (      ; iS < iDeb; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}

			for (      ; iS < iFin; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}
			
			for (      ; iS < iEnd; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}

			for (      ; iS < m_iOutputSamples; iS++)
			{
				if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
				{
					m_pfSamplesA[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * cosf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					m_pfSamplesB[iS] = (float)data[iS*m_dwReceiveBeams+iB].amp * sinf(PHS2RAD(data[iS*m_dwReceiveBeams+iB].phs));
					if (data[iS*m_dwReceiveBeams+iB].amp)
						m_pMatrix[iS][iB] = PHS2DEG_FLIP(data[iS*m_dwReceiveBeams+iB].phs);
					if (m_MaskMin > iS) m_MaskMin = iS;
					if (m_MaskMax < iS) m_MaskMax = iS;
				}
				else
				{
					NAN(aFNaN[iS]);
					NAN(bFNaN[iS]);
				}
			}

// Something in here does not compute <RESON>
			

			for (iS = iDeb; iS < iFin; iS++)
			{
				wasNaN = ISNAN(aFNaN[iS]) || ISNAN(bFNaN[iS]);
				m_pfSamplesA[iS] = _nanmean(m_pfSamplesA+iS-n, n*2+1);
				m_pfSamplesB[iS] = _nanmean(m_pfSamplesB+iS-n, n*2+1);

				if (wasNaN)
					NAN(Mask[iS*m_dwReceiveBeams+iB]);
				else
					m_pMatrix[iS][iB] = R2Df(atan2f(m_pfSamplesB[iS], m_pfSamplesA[iS]));
			}
		}
	}

	return true;
}


//------------------------------------------------------------------------------
// Determination of a mask on phase - single sample
//
// Return Value: none
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [INPUT]  m_pMaskMin,  m_pMaskMax
// [OUTPUT] m_pMatrix  (Phase Mask)
//
// [TEMP]   m_pfBeamsB
// [TEMP]   m_pdBeamsD === masque
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		_phase_mask
//
// MatLab reference:
//		ResonMasquePhase_V5(20:117)
//		masquePh2
//		masquePh3
//
void _phase_mask_line ( int sample, float * amp, DWORD * subBeams, int subLength  )
{
	int iS, iB;
	DWORD *NaNPhase  = (DWORD *)m_pMatrix[sample];
	float *Phase = m_pMatrix[sample];
	bool * mask = (bool *)m_pdBeamsD;
	memset (mask, 0, sizeof(bool)*m_dwReceiveBeams);

	subBeams[subLength] = m_dwReceiveBeams*2;

	float Sigma, indiceR;
	int iBeg, iEnd, iMax;
	int sBeg = 0, sEnd, nP;
	bool flagFirstOrLastBeam;
	float PhaseSeuil;
	bool allMinus, allPlus;

	if (sample == 7505)
	{
		int aaaa = 1;
	}

	for (iS = 0; iS < subLength; iS++)
	{
		if (subBeams[iS+1] - subBeams[iS] > 1)
		{
			sEnd = iS+1;
			nP = sEnd - sBeg;
			allPlus = true;
			for (iB = subBeams[sBeg]; iB <= (int)subBeams[sEnd-1]; iB++)
			{
				// !ALTRAN - 38 Begin
				//allPlus &= MasqueAmp(iB, sample);
				if((amp[iB] == amp[iB]) == false)
				{
					allPlus = false;
				}
				// !ALTRAN End
			}

			if (nP <= 5)
			{
				for (iB=sBeg; iB<sEnd; iB++)
				{
					mask[iB] = true;
				}
			}
			else if (allPlus)
			{
				indiceR = _barycentre(amp+subBeams[sBeg], 1, nP, Sigma, m_pfBeamsB) + sBeg - 1;
				// !ALTRAN - 36 Begin
				if(indiceR >= 0)
				{
				// !ALTRAN End

					iMax = floori(indiceR);

					if (Sigma > 2.0f)
					{
						iBeg = floori(iMax - 2*Sigma);
						iEnd = floori(iMax + 2*Sigma + 1);
						
						if (iBeg < sBeg)
						{
							iEnd = sBeg+floori(4*Sigma);
							iBeg = sBeg;
							flagFirstOrLastBeam = true;
						}
						else if (iEnd >= sEnd)
						{
							iBeg = iEnd-sEnd-1+sBeg;
							iEnd = sEnd;
							flagFirstOrLastBeam = true;
						}
						else
							flagFirstOrLastBeam = false;
						
						iBeg = max(iBeg, sBeg);
						iEnd = min(iEnd, sEnd);
						
						PhaseSeuil = 80.0f;

						for (iB = sBeg; iB < iBeg; iB++) NAN(NaNPhase[subBeams[iB]]);
						for (iB = iEnd; iB < sEnd; iB++) NAN(NaNPhase[subBeams[iB]]);
					}
					else
					{
						iBeg = sBeg;
						iEnd = sEnd;
						flagFirstOrLastBeam = true;
						PhaseSeuil = 160.0f;
					}

					for (iB=iMax; iB < iEnd-1; iB++)
					{
						if (fabsf(Phase[subBeams[iB+1]]-Phase[subBeams[iB]]) < PhaseSeuil)
						{
							mask[iB]   = true;
							mask[iB+1] = true;
						}
						else
							break;
					}

					for (iB=iMax-1; iB >= iBeg; iB--)
					{
						if (fabsf(Phase[subBeams[iB+1]]-Phase[subBeams[iB]]) < PhaseSeuil)
						{
							mask[iB]   = true;
							mask[iB+1] = true;
						}
						else
							break;
					}
						
					if (Sigma > 2.0f)
					{
						for (iB=iBeg; iB<iEnd; iB++)
						{
							if (fabsf(Phase[subBeams[iB]]) > 150.0f)
							{
								NAN(NaNPhase[subBeams[iB]]);
								mask[iB] = false;
							}
						}
					}

					if (!flagFirstOrLastBeam)
					{
						allMinus = allPlus = true;

						for (iB=iBeg; iB<iEnd; iB++)
						{
							if (!mask[iB])             continue;
							if (Phase[subBeams[iB]] == 0.0f) 
							{ allMinus = false; allPlus  = false; break; }
							else if (Phase[subBeams[iB]] > 0) allMinus = false;
							else                              allPlus  = false;
						}

						if (allPlus || allMinus)
							for (iB=iBeg; iB<iEnd; iB++)
							{
								mask[iB] = false;
							}
					}
				// !ALTRAN - 36 Begin
				}
				// !ALTRAN End
			}	

			sBeg = sEnd;

		}	// subBeams[iS+1] - subBeams[iS] > 1
	}		// for

	for (iS = 0; iS < subLength; iS++)
		if (mask[iS])
			MasquePhs(subBeams[iS], sample, true);
}



//------------------------------------------------------------------------------
// Final cleaning of the phase mask
//
// Return Value: none
//
// [INPUT]  MasquePhs()
// [OUTPUT] m_pMatrix  (Phase Mask)
// [OUTPUT] MasquePhs()
//
// Used in functions:
//		_phase_mask
//
// MatLab reference:
//		ResonMasquePhase_V5->cleanMasque
//
void _phase_clean_mask ( void )
{
	DWORD *NaNPhase  = (DWORD *)m_pMatrix[0];
	int iB, iS, nS, jS;
	int iBeg;
	
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
//%	sub = find(mIn(:,iBeam));
//%	n = min(5,ceil(length(sub) / 20));
		for (iS = nS = 0; iS < m_iOutputSamples; iS++)
			if (MasquePhs(iB,iS)) nS++;


		nS = min(5, ceili((float)nS/20.0f));

//% mOut(:, iBeam) = imopen(mOut(:, iBeam), ones(n,1));
		for (iS = iBeg = 0; iS < m_iOutputSamples; iS++)
		{
			if (!MasquePhs(iB,iS))
			{
				if (iS - iBeg < nS)
				{
					for (jS = iBeg; jS <= iS; jS++)
					{
						NAN(NaNPhase[jS*m_dwReceiveBeams+iB]);
						MasquePhs(iB, jS, false);
					}
				}
				else
				{
					NAN(NaNPhase[iS*m_dwReceiveBeams+iB]);
				}

				iBeg = iS+1;
			}
		}
		
		if (iBeg < m_iOutputSamples)
		{
			if (m_iOutputSamples - iBeg < ceili((float)nS/2.0f))
			{
				for (jS = iBeg; jS < m_iOutputSamples; jS++)
				{
					NAN(NaNPhase[jS*m_dwReceiveBeams+iB]);
					MasquePhs(iB, jS, false);
				}
			}
		}
	}
}



//------------------------------------------------------------------------------
// Determination of a mask on phase
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [INPUT]  m_pMaskMin,  m_pMaskMax
// [OUTPUT] m_pMatrix  (Phase Mask)
//
// [TEMP]   m_pdBeamsA  === subSample
// [TEMP]   m_pfBeamsA  === amp
//
// [MATLAB] Ph (input)  === m_pMatrix (input)
// [MATLAB] Amp         === m_pRAWData
// [MATLAB] R0          === m_R0
// [MATLAB] iBeam0      === m_IncidentBeam
// [MATLAB] RMax1       === m_pR1
// [MATLAB] Masque      === Masque()
// [MATLAB] Ph (output) === m_pMatrix (output)
//
// Sub-functions calls:
//		
// Used in sub-functions:
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonMasquePhase_V5
//

bool _phase_mask( void )
{
	int iB, iS, nB;
	float * phase = m_pMatrix[0];
	DWORD * Mask  = (DWORD *)m_pMatrix[0];
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;

	float R1Min = (float)m_iOutputSamples;
	DWORD * R1NaN = (DWORD *) m_pR1;

//%	Masque = repmat(false, size(Ph));
	MasquePhs(false);

//%	R0 = max(R0, min(RMax1));
//%	R0 = floor(R0 * 1.02);
// !ALTRAN - 19 Begin
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++){
		if (!ISNAN(R1NaN[iB]) && R1Min > m_pR1[iB]){
			R1Min = m_pR1[iB];
		}
	}
// !ALTRAN end

	R1Min  = max(m_R0, R1Min);
// !ALTRAN - 20 Begin
	int R0 = floori(R1Min * 1.02f);
// !ALTRAN end

	for (iS = R0; iS < m_iOutputSamples; iS++)
	{

//%	subSample = find(~isnan(Ph(iSample,subBab)));
		for (iB = nB = 0; iB <= m_IncidentBeam; iB++)
		{
			if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
			{
				m_pdBeamsA[nB++] = iB;
			}
			m_pfBeamsA[iB] = data[iS*m_dwReceiveBeams+iB].amp;
		}

		_phase_mask_line(iS, m_pfBeamsA, m_pdBeamsA, nB);			

//%	subSample = find(~isnan(Ph(iSample,subTri)));
		for (iB = m_IncidentBeam+1, nB = 0; iB < (int)m_dwReceiveBeams; iB++)
		{
			if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB]))
			{
				m_pdBeamsA[nB++] = iB;
			}
			m_pfBeamsA[iB] = data[iS*m_dwReceiveBeams+iB].amp;
		}
			
		_phase_mask_line(iS, m_pfBeamsA, m_pdBeamsA, nB);		
	}


	bool allNaN;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		if (iB == 29)
		{
			int aaaa = 1;
		}

		allNaN = true;

		for (nB = max(0, iB-10); nB < min((int)m_dwReceiveBeams, iB+11); nB++)
			if (!ISNAN(R1NaN[nB]))
			{
				allNaN = false;
				break;
			}
		
		if (allNaN || (!ISNAN(R1NaN[iB]) && floori(m_pR1[iB]) <= R0))
		{
			MasquePhs(iB, false);
		}
	}

	
	_phase_clean_mask();

	return true;
}



bool _phase_test(const DWORD * subSamples, const int nSamples, const float NechRes)
{
	if ( (nSamples < NechRes) || ((nSamples < 50) && (((float)subSamples[nSamples-1] - (float)subSamples[0])/nSamples > 2.0f) ) )
		return true;
	else
		return false;
}





//------------------------------------------------------------------------------
// Weighted Linear Interpolation
//
// Arguments:
// [IN]  subSamples = samples indexes
// [IN]  Phs        = corresponding phase
// [IN]  Weights    = corresponding amplitude weight (or NULL)
// [IN]  nSamples   = number of samples in estimation (size of subSamples, Phs and Weights vectors)
// [OUT] A, B		= polynomial parameters of the fit (Ax+B)
// [OUT] X0         = point of zero crossing
// [OUT] Eqm        = mean-square error
//
// Return Value: success / failure
//
bool _phase_calcul( float * subSamples, float * Phs, float * Weights, int nSamples, 
										  float& A, float& B, float& X0, float& Eqm )
{
	float SumW = 0.0f, SumX = 0.0f, SumX2 = 0.0f, SumXY = 0.0f, SumY = 0.0f;
	int iS;

	float *phase, *w, *x;

	for (iS = 0, phase = Phs, w = Weights, x = subSamples; iS < nSamples; iS++, phase++, w++, x++)
	{
//%		SumW  = sum(w);
		SumW  += *w;
//%		SumX  = sum(w .* x);
		SumX  += *w**x;
//%		SumX2 = sum(w .* x.^2);
		SumX2 += *w**x**x;
//%		SumXY = sum(w .* x .* phase);
		SumXY += *w**x**phase;
//%		SumY  = sum(w .* phase);
		SumY  += *w**phase;
	}

	float delta = SumW * SumX2 - SumX * SumX;

	if (fabsf(delta) > FLT_EPSILON*2)
	{
		// Calculate slope, intercept and correlation of linear fit
		B = (SumX2 * SumY - SumX * SumXY) / delta;
		A = (SumW * SumXY - SumX * SumY) / delta;

		X0 = -(SumX2 * SumY - SumX * SumXY) / (SumW * SumXY - SumX * SumY);		// X0 = -B/A; --- ?????

		Eqm = 0.0f;
		for (iS = 0, phase = Phs, w = Weights, x = subSamples; iS < nSamples; iS++, phase++, w++, x++)
		{
			SumX = *phase - (A**x+B);
			Eqm += *w * SumX * SumX;
		}

		Eqm = sqrtf(Eqm / SumW);
	}
	else
		return false;

	return true;
}



//------------------------------------------------------------------------------
// Linear Estimation of Phase Detection
//
// Arguments:
// [IN]  subSamples = samples indexes
// [IN]  Phs        = corresponding phase
// [IN]  Weights    = corresponding amplitude weight (or NULL)
// [IN]  nSamples   = number of samples in estimation (size of subSamples, Phs and Weights vectors)
// [OUT] A, B		= polynomial parameters of the fit (Ax+B)
// [OUT] X0         = point of zero crossing
// [OUT] Eqm        = mean-square error
// [OUT] nX         = actual number of points in the calculations
//
// Return Value: success / failure
//
// [TEMP] m_pfSamplesD   - temporary
// [TEMP] m_pfSamplesE   - temporary
// [TEMP] m_pfSamplesG   - "sample number"
//
bool _phase_regression( DWORD * subSamples, float * Phs, float * Weights, const int nSamples, 
											  float& A, float& B, float& X0, float& Eqm )
{
	int deltax = subSamples[nSamples-1] - subSamples[0];
	
	if (deltax == 0)
		return false;

	int iS;
	float MaxWeight = 0.0f;

	if (Weights)
		for (iS = 0; iS < nSamples; iS++)
			if (MaxWeight < Weights[iS]) MaxWeight = Weights[iS];

	float Alpha = 1.0f / (float)deltax;

	for (iS = 0; iS < nSamples; iS++)
	{
		if (Weights)
			m_pfSamplesD[iS] = Weights[iS] / MaxWeight;
		else
			m_pfSamplesD[iS] = 1.0f;

		m_pfSamplesE[iS] = Alpha * (subSamples[iS] - subSamples[0]);
	}

	float a, b, x0;

	if (!_phase_calcul(m_pfSamplesE, Phs, m_pfSamplesD, nSamples, a, b, x0, Eqm))
		return false;

	A  = Alpha * a;
	B  = b - A * subSamples[0];
	X0 = x0 / Alpha + subSamples[0];

	return true;
}



//------------------------------------------------------------------------------
// Phase Detection per Beam. Single Iteration
//
// Arguments:
// [IN]  side   - true = Port(Babord); false = Starboard(Tribord)
//
// Return Value: success / failure
//
// [OUTPUT] m_pPhaseMin, m_pPhaseMax
//
// [TEMP] m_pfSamplesD   - temporary
// [TEMP] m_pfSamplesE   - temporary

bool _phase_detection_beam_single( DWORD *subSamples, float *Phase, float *Amp, float *Amp2, int iter, 
														 int &nSamples, const int Beam, const float NechRes,
														 float &R, float &QF, float &Pente, float &Eqm, int &NbSamples)
{
	int iS, nS;

	NANf(R);
	NANf(QF);
	NANf(Pente);
	NANf(Eqm);
	NbSamples = 0;

//%	M = x(floor(length(x)/2));
//%	if (M > (R0*1.9)) && (M < (R0*2.1))
//%		amp(:) = 1;
//%	end

	float M = (float)subSamples[nSamples/2-1];
	float R0 = m_R0+1;
	if (M > (R0*1.9f) && M < (R0*2.1f))
	{
		for (iS = 0; iS < nSamples; iS++)
			Amp[iS] = 1.0f;
	}

//% subNonNaN = find(~isnan(phase) & ~isnan(amp));
//  At this point there should be no NaNs either in amplitude or phase data.

	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;

	bool flag;
	float A, B;

	switch (iter)
	{
	case 1:
		flag = _phase_regression(subSamples, Phase, Amp2, nSamples, A, B, R, Eqm);
		break;

	case 2:
		flag = _phase_regression(subSamples, Phase, Amp,  nSamples, A, B, R, Eqm);
		break;
		
	case 3:
		flag = _phase_regression(subSamples, Phase, NULL, nSamples, A, B, R, Eqm);
		break;
	}

	if (!flag)
		return false;

	Pente = fabsf(A);

//	QF = Pente * R * sqrtf((float)nSamples) / Eqm;

//%	dTPhi   = (Eqm / sqrt(length(x)/nbSamplesMoyennes)) / Pente;
//%	dTPulse = (TxPulseWidth * SampleRate) / sqrt(12);
//%	dT = sqrt(dTPhi*dTPhi + dTPulse*dTPulse);
//%	QF = R / dT;

//% From Reson_phaseFilter.m: nbSamplesMoyennes=2*n+1; n=4; ==> 9

	float dTPhi = (Eqm / sqrtf((float)nSamples/9.0f)) / Pente;
	// !ALTRAN - 24  Begin
	//QF = R / sqrtf(dTPhi*dTPhi + m_dTPulse);
	QF = R / sqrtf(dTPhi*dTPhi + m_dTPulse*m_dTPulse);
	// !ALTRAN end

	if (QF > 0)
		QF = log10f(QF);
	else
 		QF = 0;

	float * PhasePoly = m_pfSamplesE;

	for (iS = 0; iS < nSamples; iS++)
	{
		PhasePoly[iS]    = A*subSamples[iS]+B;			// phase_poly
		m_pfSamplesD[iS] = Phase[iS] - PhasePoly[iS];	// Diff
	}

	Eqm = _nanStd(m_pfSamplesD, nSamples, subSamples);
	NbSamples = nSamples;

	// Suppression of outliers
//%	subNonOutliers = find(abs(Diff) <= (2*Eqm));

	for (iS = nS = 0; iS < nSamples; iS++)
	{
		if (fabsf(m_pfSamplesD[iS]) <= 2.0f*Eqm)
		{
			subSamples[nS]  = subSamples[iS];
			Phase[nS]       = Phase[iS];
			Amp[nS]         = Amp[iS];
			Amp2[nS]        = Amp2[iS];
			PhasePoly[nS++] = PhasePoly[iS];
		}
	}

	nSamples = nS;

	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;

	// Centrage autour de R
	int   r = floori(R);
	int   d = min(r-(int)subSamples[0], (int)subSamples[nSamples-1]-r);

	if (d <= 1)
		return false;

//% sub = find((x > (R-d)) & (x < R+d) & (phase_poly > -180) & (phase_poly < 180));

	for (iS = nS = 0; iS < nSamples; iS++)
	{
		if ((subSamples[iS] > R-d) && (subSamples[iS] < R+d) && (PhasePoly[iS] > -180) && (PhasePoly[iS] < 180))
		{
			subSamples[nS]  = subSamples[iS];
			Phase[nS]       = Phase[iS];
			Amp[nS]         = Amp[iS];
			Amp2[nS++]      = Amp2[iS];
		}
	}

	nSamples = nS;
	
	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;
	
	int S1 = ceili ((float)nSamples/6.0f)-1;
	int S2 = floori((float)nSamples*5.0f/6.0f)-1;

	// Reduction de 1/8 de part et d'autre
//%	n = length(x);
//%	sub = ceil(n/6):floor(n*5/6);
	for (iS = nS = 0; iS < nSamples; iS++)
	{
		if (iS >= S1 && iS <= S2)
		{
			subSamples[nS]  = subSamples[iS];
			Phase[nS]       = Phase[iS];
			Amp[nS]         = Amp[iS];
			Amp2[nS++]      = Amp2[iS];
		}
	}
	
	nSamples = nS;
	
	if ( _phase_test(subSamples, nSamples, NechRes) )
		return false;

	return true;
}



//------------------------------------------------------------------------------
// Phase Detection - per beam
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [OUTPUT] m_pRangePhase
// [OUTPUT] m_pQualityPhase
// [OUTPUT] m_pSamplesPhase
// [OUTPUT] m_pERMPhase
// [OUTPUT] m_pSlopePhase
//
// [TEMP] m_pfSamplesA   = phase
// [TEMP] m_pfSamplesB   = amplitude
// [TEMP] m_pdSamplesA   = subSample
//
// Sub-functions calls:
//		_phase_detection_beam
//		
// Used in sub-functions:
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonDetectionPhaseImage_3steps
//
bool _phase_detection_beam( DWORD * subSamples, float * Amp, float * Amp2, float * Phs, int nbSamples, int Beam )
{
	NANf(m_pRangePhase[Beam]);
	NANf(m_pQualityPhase[Beam]);
	NANf(m_pERMPhase[Beam]);
	NANf(m_pSlopePhase[Beam]);
	m_pSamplesPhase[Beam]   = 0;

//%	% Sample number inside the whole beam
//%	Beta = 1.2;
//%	NechBeam = R0 * DeltaTeta * (pi/180) * sind(abs(BeamAngle)) / (2 * Beta * cosd(BeamAngle)^2);

	float NechBeam = (m_R0+1) * m_DeltaTeta * sinf(fabsf(m_pBeamAngles[Beam])) / (2.4f * cosf(m_pBeamAngles[Beam]) * cosf(m_pBeamAngles[Beam]));

//%	% Sample number inside the resolution cell (sounding horizontal spacing)
//%	NechRes = ceil(2 * abs(2 * R0 .* sind(BeamAngle) * (tand(MaxBeamAngle) / nbBeams)));
	float NechRes = ceilf(4.0f * fabsf((m_R0+1) * sinf(m_pBeamAngles[Beam]) * tanf(m_pBeamAngles[m_dwReceiveBeams-1]) / m_dwReceiveBeams));

//%	NechRes = min(NechBeam, NechRes);
//%	NechRes = max(NechRes, 6);
//%	NechRes = min(NechRes, 50);
	NechRes = min (NechBeam, NechRes);
	// !ALTRAN - 25  Begin
	//NechRes = limit(NechRes, 6, 50);
	NechRes = max(NechRes,6);
	// !ALTRAN end

	WORD NechRampMin = 7;
	int nSamples = nbSamples;

	// If number of samples is less than 11 we do not try to process the sounding
	if (nSamples < NechRampMin)
		return false;

	float NechSondeMin = ceilf((float)nSamples / 20.0f);
	NechSondeMin = max(NechSondeMin, (float)NechRampMin);
	NechSondeMin = max(NechSondeMin, NechRes);

	float R, QF, Pente, Eqm;
	int   NbSamples, i;

	for (i = 1; i < 4; i++)
	{
		if (!_phase_detection_beam_single( subSamples, Phs, Amp, Amp2, i, nSamples, Beam, NechSondeMin, R, QF, Pente, Eqm, NbSamples ))
		{
			if (i == 1 && QF > 3.0f)
			{
				m_pRangePhase[Beam]   = R-1;
				m_pQualityPhase[Beam] = QF;
				m_pERMPhase[Beam]     = Eqm;
				m_pSlopePhase[Beam]   = Pente;
				m_pSamplesPhase[Beam] = NbSamples;
			}

			return true;
		}
		
		m_pRangePhase[Beam]   = R-1;
		m_pQualityPhase[Beam] = QF;
		m_pERMPhase[Beam]     = Eqm;
		m_pSlopePhase[Beam]   = Pente;
		m_pSamplesPhase[Beam] = NbSamples;
	}

	return true;
}




bool _detection_synthese ( )
{
	int iB;
	DWORD *AmpRangeNaN    = (DWORD*) m_pRangeAmplitude;
	DWORD *PhsRangeNaN    = (DWORD*) m_pRangePhase;
	DWORD *   RangeNaN    = (DWORD*) m_pDetectionRange;
	
	if (ISNAN(AmpRangeNaN[m_IncidentBeam]))		// Safety
		return false;

	// Remove detections outside gates...

	for (iB = 0; iB < (int) m_dwReceiveBeams; iB++)
	{
		if (!ISNAN(AmpRangeNaN[iB]) && m_pRangeAmplitude[iB] > -0.5f && !between(m_pRangeAmplitude[iB], m_pGatesMin[iB], m_pGatesMax[iB])) {
			NAN(AmpRangeNaN[iB]);
			m_pSamplesAmp[iB]       = 0;
			m_pQualityAmplitude[iB] = 0.0f;
			m_pQF2Amplitude[iB]     = 0.0f;
		}

		if (!ISNAN(PhsRangeNaN[iB]) && !between(m_pRangePhase[iB], m_pGatesMin[iB], m_pGatesMax[iB])) {
			NAN(PhsRangeNaN[iB]);
			NANf(m_pQualityPhase[iB]);
			NANf(m_pERMPhase[iB]);
			NANf(m_pSlopePhase[iB]);
			m_pSamplesPhase[iB]   = 0;
		}
	}


	float NbSamplesForPulseLength = ceilf(m_fTransmitPulseLength * m_fSampleRate);
	float ThauOn2 = NbSamplesForPulseLength / 2.0f;
	float Ang     = acosf((m_pRangeAmplitude[m_IncidentBeam] - ThauOn2) / (m_pRangeAmplitude[m_IncidentBeam] + ThauOn2));
	float QF_threshold = 2.1f;

	int PhasePingPenteSeuil = 36;
	int PhasePingEqmSeuil   = 45;

	bool * subPhase = m_pNominalBeams;

	for (iB = 0; iB < (int) m_dwReceiveBeams; iB++)
	{
		subPhase[iB] = !ISNAN(AmpRangeNaN[iB]) && !ISNAN(PhsRangeNaN[iB]) && (m_pQualityPhase[iB] > 2.0f) && (m_pSlopePhase[iB] <= PhasePingPenteSeuil) && (m_pERMPhase[iB] <= PhasePingEqmSeuil);
		if ((m_pDetectionType[iB] & IF1_QUALITY_QF2_PASS) == 0)
			subPhase[iB] &= (m_pQualityPhase[iB] > QF_threshold);
	}

	// Suppression of isolated phase detections
	for (iB = 1; iB < (int) m_dwReceiveBeams-1; iB++)
		if (!subPhase[iB-1] && subPhase[iB] && !subPhase[iB+1])
			subPhase[iB] = false;

	// Suppression of 2 isolated phase detections
	for (iB = 0; iB < (int) m_dwReceiveBeams-3; iB++)
		if (!subPhase[iB] && subPhase[iB+1] && subPhase[iB+2] && !subPhase[iB+3])
			subPhase[iB+1] = subPhase[iB+2] = false;


	for (iB = 0; iB < (int) m_dwReceiveBeams; iB++)
	{
		if (subPhase[iB])
		{
			m_pDetectionRange[iB]   = m_pRangePhase[iB];
			m_pDetectionQuality[iB] = m_pQualityPhase[iB];
			m_pDetectionType[iB]   |= IF1_DETECTION_PHASE;
		}
		else if (!ISNAN(AmpRangeNaN[iB]) && m_pRangeAmplitude[iB] > QF_threshold && m_pQualityAmplitude[iB] >= QF_threshold /* && m_pQF2Amplitude[iB] >= 1.4*/)	// On complète avec l'amplitude
		{
			m_pDetectionRange[iB]   = m_pRangeAmplitude[iB];
			m_pDetectionQuality[iB] = m_pQualityAmplitude[iB];
			m_pDetectionType[iB]   |= IF1_DETECTION_AMP;
		}
		else if (!ISNAN(AmpRangeNaN[iB]) && !ISNAN(PhsRangeNaN[iB]) && m_pQualityPhase[iB] >= QF_threshold && m_pSlopePhase[iB] <= PhasePingPenteSeuil)
		{									// On recomplete avec la detection de phase sans la contrainte sur l'EQM
			m_pDetectionRange[iB]   = m_pRangePhase[iB];
			m_pDetectionQuality[iB] = m_pQualityPhase[iB];
			m_pDetectionType[iB]   |= IF1_DETECTION_PHASE;
		}
		else
		{
			m_pDetectionType[iB]    = 0;
		}
	}


	// !ALTRAN - 27  Begin
	/*bool eot = false;
	while (!eot) {
		eot = true;

		for (iB = 1; iB < (int) m_dwReceiveBeams-1; iB++) {
			if ((m_pDetectionType[iB]&IF1_DETECTION_AMP) && !ISNAN(PhsRangeNaN[iB]) && fabs(m_pRangeAmplitude[iB] - m_pRangePhase[iB]) > 20.0f 
				&& ((m_pDetectionType[iB-1]&IF1_DETECTION_PHASE) || (m_pDetectionType[iB+1]&IF1_DETECTION_PHASE))) {
				m_pDetectionType[iB]   &= ~IF1_DETECTION_AMP;
				m_pDetectionType[iB]   |= IF1_DETECTION_PHASE;
				m_pDetectionRange[iB]   = m_pRangePhase[iB];
				m_pDetectionQuality[iB] = m_pQualityPhase[iB];
				eot = false;
			}
		}
	}*/
	// !ALTRAN end



	return true;
}


//------------------------------------------------------------------------------
// clean memory
//------------------------------------------------------------------------------
void cleanData()
{
	mxFree(m_pGatesMin);
	m_pGatesMin = 0;
	mxFree(m_pGatesMax);
	m_pGatesMax = 0;
	mxFree(m_pMaskMin);
	m_pMaskMin = 0;
	mxFree(m_pMaskMax);
	m_pMaskMax = 0;
	mxFree(m_pMaskWidth);
	m_pMaskWidth = 0;
    mxFree(m_pFlagBeams);
	m_pFlagBeams = 0;
	delete(m_pAmpReduite);
	m_pAmpReduite = 0;
	delete(m_pfReduite);
	m_pfReduite = 0;
	mxFree(m_pR1);
	m_pR1 = 0;
	mxFree(m_pR2);
	m_pR2 = 0;
    mxFree(m_pRangeAmplitude);
	m_pRangeAmplitude = 0;
	delete(m_pSamplesAmp);
	m_pSamplesAmp = 0;
	mxFree(m_pQualityAmplitude);
	m_pQualityAmplitude = 0;
	mxFree(m_pQF2Amplitude);
	m_pQF2Amplitude = 0;
	mxFree(m_pRangePhase);
	m_pRangePhase = 0;
	mxFree(m_pQualityPhase);
	m_pQualityPhase = 0;
	mxFree(m_pSamplesPhase);
	m_pSamplesPhase = 0;
	mxFree(m_pERMPhase);
	m_pERMPhase = 0;
	mxFree(m_pSlopePhase);
	m_pSlopePhase = 0;
	mxFree(m_pDetectionRange);
	m_pDetectionRange = 0;
	mxFree(m_pDetectionQuality);
	m_pDetectionQuality = 0;
	mxFree(m_pMax);
	m_pMax = 0;
	mxFree(m_pDetectionType);
	m_pDetectionType = 0;
    mxFree(m_pAmpWinMin);
	m_pAmpWinMin = 0;
    mxFree(m_pAmpWinMax);
	m_pAmpWinMax = 0;
    mxFree(m_pPhsWinMin);
	m_pPhsWinMin = 0;
    mxFree(m_pPhsWinMax);
	m_pPhsWinMax = 0;
    mxFree(m_pdBeamsA);
	m_pdBeamsA = 0;
    mxFree(m_pdBeamsB);
	m_pdBeamsB = 0;
    mxFree(m_pdBeamsC);
	m_pdBeamsC = 0;
    mxFree(m_pdBeamsD);
	m_pdBeamsD = 0;
    mxFree(m_pfBeamsA);
	m_pfBeamsA = 0;
    mxFree(m_pfBeamsB);
	m_pfBeamsB = 0;
    mxFree(m_pfBeamsC);
	m_pfBeamsC = 0;
    mxFree(m_pfBeamsD);
	m_pfBeamsD = 0;
	mxFree(m_pNominalBeams);
	m_pNominalBeams = 0;
    mxFree(m_pfSamplesA);
	m_pfSamplesA = 0;
    mxFree(m_pfSamplesB);
	m_pfSamplesB = 0;
    mxFree(m_pfSamplesC);
	m_pfSamplesC = 0;
    mxFree(m_pfSamplesD);
	m_pfSamplesD = 0;
    mxFree(m_pfSamplesE);
	m_pfSamplesE = 0;
    mxFree(m_pfSamplesF);
	m_pfSamplesF = 0;
    mxFree(m_pfSamplesG);
	m_pfSamplesG = 0;
    mxFree(m_pfSamplesH);
	m_pfSamplesH = 0;
	mxFree(m_pfdSamples);
	m_pfdSamples = 0;
	mxFree(m_pdSamplesA);
	m_pdSamplesA = 0;
	mxFree(m_pdSamplesB);
	m_pdSamplesB = 0;
	delete(m_pMatrix);
	m_pMatrix = 0;
}




// --------------------------------------------------------------------
// _nominal_beams_7111 - index determination for semi nominal beams
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT]  m_pNominalBeams

bool _nominal_beams_7111(  )
{

#include "params7111_201_v2.h"
	
	int iB, iNB;

	if (m_dwReceiveBeams < 300)		// 201 or 101 mode. All beams are nominal
	{
		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++ )
			m_pNominalBeams[iB] = true;
	}	
	else
	{
		for (iB = 0; iB < 201; iB++ )
			m_pfBeamsC[iB] = (float) Steering_Angles[iB];

		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++ )
		{
			m_pfBeamsB[iB] = (float)iB;
			m_pNominalBeams[iB] = false;
		}

		Interp1 (m_pBeamAngles, m_pfBeamsB, m_dwReceiveBeams, m_pfBeamsC, m_pfBeamsA, 201);
		
		for (iB = 0; iB < (int)m_dwReceiveBeams; iB++ )
		{
			iNB = floori(m_pfBeamsA[iB]);
			if (iNB > 0 && iNB < 301)
				m_pNominalBeams[iNB] = true;
		}
	}

	return true;
}



//------------------------------------------------------------------------------
// m_pGatesMin, m_pGatesMax filling
//------------------------------------------------------------------------------
void SetDepthGates (float minDepth, float maxDepth, float roll)
{
//	printf ("Ping #%d: Setting Depth Gates [%f %f]\n", m_dwPingNumber, minDepth, maxDepth);

	register int gateMin, gateMax;
	double N_m;


	for (register int si=0; si < (int)m_dwReceiveBeams; si++)
	{
		gateMin = floori(D2SR(minDepth,si,roll));
		gateMax = ceili(D2SR(maxDepth,si,roll));
		if (m_pGatesMin[si] < (DWORD)gateMin) m_pGatesMin[si] = max(gateMin,5);
		if (m_pGatesMax[si] > (DWORD)gateMax) m_pGatesMax[si] = gateMax;
		if (m_pGatesMax[si] < m_pGatesMin[si]) m_pGatesMax[si] = m_pGatesMin[si] = 0;
	}
}



//------------------------------------------------------------------------------
// Preparation of amplitude mask.
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData;
// [OUTPUT] m_pMaskMin,  m_pMaskMax
// [OUTPUT] maskMin,	 maskMax
//
// Subfunctions calls:
//		_detection_estimate
//		_mask_7111
//		_mask_7150
//		
// Used in sub functions:
bool CalculateMask ( void )
{
	if (m_dwDeviceId != 7111 && m_dwDeviceId != 7150)
		return false;

	// !ALTRAN - 41 Begin
	if(m_IdentAlgo == 3)
	{
		_BDA_compensAmpByMHorzMedian(m_pRAWData, m_dwReceiveBeams, m_iOutputSamples);
	}
	// !ALTRAN End

	if (!_detection_estimate()) return false;

	switch (m_dwDeviceId)
	{
	case 7111:
		if (!_mask_7111())            return false;
		break;
	case 7150:
		if (!_mask_7150())			  return false;
		break;
	}

	return true;
}


// --------------------------------------------------------------------
// _amplitude_detection - implementation of ResonDetectionAmplitudeImage
//
// Return value: Success(true)/Fail(false)
//
// [OUTPUT] m_pRangeAmplitude
// [OUTPUT] m_pSamplesAmp
// [OUTPUT] m_pQualityAmplitude
// [OUTPUT] m_pQF2Amplitude
//
// [INPUT]  m_pMatrix    - Amplitude Mask
// [INPUT]  m_MaskMin    - Minimum valid sample
// [INPUT]  m_MaskMax    - Maximum valid sample
// [TEMP]   m_pfSamplesG, m_pfSamplesH - Beam Amplitude
// [TEMP]   m_pdSamplesA - Valid Indices
//
// [MATLAB] Amp                === m_pMatrix
// [MATLAB] R0                 === m_R0 (not used)
// [MATLAB] BeamAngles         === m_pBeamAngles (not used)
// [MATLAB] SystemSerialNumber === m_dwDeviceId (not used)
// [MATLAB] SampleRate         === m_fSampleRate (not used)
// [MATLAB] TxPulseWidth       === m_fTransmitPulseLength (not used)
// [MATLAB] iBeam0             === m_IncidentBeam (not used)
// [MATLAB] AmPingRange        === m_pRangeAmplitude
// [MATLAB] AmpPingQF          === m_pQualityAmplitude
// [MATLAB] AmpPingSamplesNb   === (not used / not calculated)
// [MATLAB] AmpPingQF2         === m_pQF2Amplitude
//
// Sub-functions calls:
//		_amplitude_single
//		
// Used in sub-functions:
//		m_pfSamplesB
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonDetectionAmplitudeImage
//
bool _amplitude_detection (void)
{
	bool retValue = false;
	bool retBeam;

	int iB, iS, iNS;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	DWORD *Mask = (DWORD *)m_pMatrix[0];
	DWORD * NaNRange = (DWORD*) m_pRangeAmplitude;

	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		m_pMaskMin[iB] = 0;
		m_pMaskMax[iB] = 0;

		if (iB == 100)
			int toto=1 ;


//%	subSample = find(~isnan(Amp) & (Amp ~=0 ));
		for (iNS = 0, iS = m_MaskMin; iS < m_MaskMax; iS++)
			if (!ISNAN(Mask[iS*m_dwReceiveBeams+iB])/* && fabs(m_pMatrix[iS][iB]) > FLT_EPSILON*2*/)
			{
				if (!m_pMaskMin[iB]) m_pMaskMin[iB] = iS;
				m_pMaskMax[iB] = iS;

				m_pfSamplesA[iNS] = m_pMatrix[iS][iB];
				m_pdSamplesA[iNS] = iS+1;
				iNS++;
			}

// %% First test : checking if the signal is not empty

		if (!iNS)
		{
			retBeam = false;
		}

		// !ALTRAN - 37 Begin
				// %% Second test (15/12/2010) : if the last sample of the signal corresponds
				// 	% to the maximum sample reachable in range then do not compute the
				//	% sounding because it will be either a biased one or a false detection
		/*else if (m_pMaskMax[iB] == (DWORD)m_iOutputSamples-1) {
			m_pRangeAmplitude[iB]	= -1.0f;
			m_pSamplesAmp[iB]       = 0;
			m_pQualityAmplitude[iB] = 0.0f;
			m_pQF2Amplitude[iB]     = 0.0f;
			continue;
		}*/ 
		// !ALTRAN End
		else
		{
			// !ALTRAN begin
			retBeam = _amplitude_single(iB, m_pdSamplesA, m_pfSamplesA, iNS, m_iOutputSamples);
			// !ALTRAN end
		}

		if (!retBeam)
		{
			NAN(NaNRange[iB]);
			m_pSamplesAmp[iB]       = 0;
			m_pQualityAmplitude[iB] = 0.0f;
			m_pQF2Amplitude[iB]     = 0.0f;
			continue;
		}

		retValue = true;
	}

	return retValue;
}



//------------------------------------------------------------------------------
// Phase Detection
//
// Return Value: success / failure
//
// [INPUT]  m_pRAWData (Amplitude)
// [INPUT]  m_pMatrix  (Phase & Mask)
// [OUTPUT] m_pRangePhase
// [OUTPUT] m_pQualityPhase
// [OUTPUT] m_pSamplesPhase
// [OUTPUT] m_pERMPhase
// [OUTPUT] m_pSlopePhase
//
// [MATLAB] Amp                === m_pRAWData
// [MATLAB] Ph                 === m_pMatrix
// [MATLAB] BeamAngles         === m_pBeamAngles
// [MATLAB] R0                 === m_R0
// [MATLAB] SoundVelocity      === not used
// [MATLAB] SampleRate         === not used
// [MATLAB] Frequency          === (m_DeltaTeta)
// [MATLAB] SystemSerialNumber === (m_DeltaTeta)
// [MATLAB] PhasePingRange     === m_pRangePhase
// [MATLAB] PhasePingNbSamples === m_pSamplesPhase
// [MATLAB] PhasePingPente     === m_pSlopePhase
// [MATLAB] PhasePingEqm       === m_pERMPhase
// [MATLAB] PhasePingQF        === m_pQualityPhase
//
// [TEMP] m_pfSamplesA   = phase
// [TEMP] m_pfSamplesB   = amplitude
// [TEMP] m_pfSamplesC   = amplitude ^ 2
// [TEMP] m_pdSamplesA   = subSample
//
// Sub-functions calls:
//		_phase_detection_beam
//		
// Used in sub-functions:
//
// Used in functions:
//		BottomDetection
//
// MatLab reference:
//		ResonDetectionPhaseImage_3steps
//
bool _phase_detection( )
{
	int iS, nS, iB;
	_raw_data_type * data = (_raw_data_type *)m_pRAWData;
	float * Phase = m_pfSamplesA;
	float * Amp   = m_pfSamplesB;
	float * Amp2  = m_pfSamplesC;
	DWORD * Sub   = m_pdSamplesA;
	DWORD * NaNRange = (DWORD *)m_pRangePhase;
	int subBeg, subEnd;

	
	for (iB = 0; iB < (int)m_dwReceiveBeams; iB++)
	{
		for (iS = nS = 0; iS < m_iOutputSamples; iS++)
		{
			// !ALTRAN - 39 Begin
			//if (MasqueAmp(iB, iS) && MasquePhs(iB, iS))
			if (ISNANf(m_pMatrix[iS][iB])== false && 
				data[iS*m_dwReceiveBeams+iB].amp != 0xffff)
			// !ALTRAN End
			{
				// not NAN on Amp and Phase
				Sub[nS]    = iS+1;
				Phase[nS]  = m_pMatrix[iS][iB];
				Amp[nS]    = (float) data[iS*m_dwReceiveBeams+iB].amp;
				Amp2[nS++] = Amp[nS] * Amp[nS];
			}
		}
	
		subBeg = Sub[0]-1;
		subEnd = Sub[nS-1]-1;

		_phase_detection_beam(Sub, Amp, Amp2, Phase, nS, iB);

		if (ISNANf(m_pRangePhase[iB]) || m_pRangePhase[iB] > subEnd || m_pRangePhase[iB] < subBeg)
		{
			NANf(m_pRangePhase[iB]);
			NANf(m_pQualityPhase[iB]);
			NANf(m_pERMPhase[iB]);
			NANf(m_pSlopePhase[iB]);
			m_pSamplesPhase[iB]   = 0;
		}
	}

	return true;
}


//------------------------------------------------------------------------------
// bottom detection computation
//------------------------------------------------------------------------------
void BottomDetection ()
{
	// !ALTRAN - 40 Begin
	if(m_MaskMax >= m_iOutputSamples) m_MaskMax = m_iOutputSamples-1;
	// !ALTRAN End

	_amplitude_detection();

	_phase_filtering();

	_phase_mask();

	_phase_detection();

	_detection_synthese();
}

