#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* dX;
    DWORD * iX;
    float * fAmp;
    int ampSize;
    double* outputDetection;
    double* outputQuality;
    double* p_dwDeviceId;
    double* p_iBeam;
    int iBeam;
    double* pSampleRate;
    double* pTxPulseWidth;
    double* pNbSamples;
    int NbSamples;
    _raw_data_type * data;

    // Get the input parameters
    fAmp=(float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);
    m_iOutputSamples = mxGetM(prhs[0]);
    data = (_raw_data_type*)mxCalloc(m_dwReceiveBeams*m_iOutputSamples, sizeof(_raw_data_type)); 
	m_pRAWData = (char*)data;
	m_pMatrix = _2DArrayAlloc(m_iOutputSamples, m_dwReceiveBeams, (float)0.0f, true);
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++){
			if(fAmp[(iB*m_iOutputSamples)+iS] == fAmp[(iB*m_iOutputSamples)+iS])
			{
				m_pMatrix[iS][iB] = (float)(fAmp[(iB*m_iOutputSamples)+iS]);
			}
			else 
			{
				memset(&(m_pMatrix[iS][iB]),0xff,sizeof(float));
			}
        }
    }
	
	fAmp=(float*)mxGetPr(prhs[1]);
    m_dwReceiveBeams = mxGetN(prhs[1]);
    m_iOutputSamples = mxGetM(prhs[1]);
	data = (_raw_data_type*)m_pRAWData;
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            data->amp = (unsigned short)(fAmp[(iB*m_iOutputSamples)+iS]);
        }
    }

	m_R0 = (float)((*(mxGetPr(prhs[2])))-1);

	m_IncidentBeam = (float)((*(mxGetPr(prhs[3])))-1);

	m_pR1 = (float *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	dX = mxGetPr(prhs[4]);
	for(int i=0;i<m_dwReceiveBeams;i++){
		m_pR1[i] = (float)(dX[i] - 1);
	}

	/*m_pMaskWidth = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	dX = (double*)(mxGetPr(prhs[5]));
	for(int i=0;i<m_dwReceiveBeams;i++){
		m_pMaskWidth[i] = (DWORD)(dX[i]);
	}*/

	m_fTransmitPulseLength = (float)(*(mxGetPr(prhs[6])));

	m_fSampleRate = (float)(*(mxGetPr(prhs[7])));

    
            
    // Initializations
	DWORD NbSamplesForPulseLength = max(floordw(m_fTransmitPulseLength*m_fSampleRate),1);
	int allocSamples = max(m_iOutputSamples / 32 + 1, ceili(0.0375f*m_iOutputSamples + 190.0f));
	allocSamples = max(allocSamples, m_iOutputSamples/NbSamplesForPulseLength+1);
	m_nMaskSamples = allocSamples;
	if(m_nMaskSamples>=m_iOutputSamples) m_nMaskSamples = m_iOutputSamples;
	m_pfSamplesA = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesB = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
	m_pfReduite = _2DArrayAlloc(m_iOutputSamples, m_dwReceiveBeams, (float)0.0f, true);
	m_pAmpReduite = _2DArrayAlloc(m_iOutputSamples, m_dwReceiveBeams, (float)0.0f, true);
	m_pfBeamsA = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pdBeamsA = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	m_pdBeamsD = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	m_pfBeamsB = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));

    
    // Call the C function
    _phase_mask();
    
    // Set the output parameters
	plhs[0] = mxCreateDoubleMatrix(m_iOutputSamples,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
	for (int iS=0; iS < m_iOutputSamples; iS++) {
		for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
			if(ISNANf(m_pMatrix[iS][iB]))
			{
				outputDetection[(iB*m_iOutputSamples)+iS] = (double)(0);
			}
			else
			{
				outputDetection[(iB*m_iOutputSamples)+iS] = (double)(1);
			}
        }
    }    

	plhs[1] = mxCreateDoubleMatrix(m_iOutputSamples,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
	for (int iS=0; iS < m_iOutputSamples; iS++) {
		for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            outputDetection[(iB*m_iOutputSamples)+iS] = (double)(m_pMatrix[iS][iB]);
        }
    }    

	// free memory
	delete(m_pMatrix);
	m_pMatrix = 0;
	delete(m_pfReduite);
	m_pfReduite = 0;
	delete(m_pAmpReduite);
	m_pAmpReduite = 0;
	mxFree(m_pRAWData); 
	m_pRAWData = 0;
	mxFree(m_pR1); 
	m_pR1 = 0;
	/*mxFree(m_pMaskWidth); 
	m_pMaskWidth = 0;*/
	mxFree(m_pfSamplesA);
	m_pfSamplesA = 0;
    mxFree(m_pfSamplesB);
	m_pfSamplesB = 0;
	mxFree(m_pfBeamsA);
	m_pfBeamsA = 0;
	mxFree(m_pdBeamsA);
	m_pdBeamsA = 0;
	mxFree(m_pdBeamsD);
	m_pdBeamsD = 0;
	mxFree(m_pfBeamsB);
	m_pfBeamsB = 0;

}
