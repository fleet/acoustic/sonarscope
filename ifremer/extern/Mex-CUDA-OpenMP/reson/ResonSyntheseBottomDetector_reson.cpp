#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* dIn;
    DWORD subSamples[10000];
    float * fIn;
    double* outputDetection;
	float Phase[10000];
	float Amp[10000];
	float Amp2[10000];
	float R;
	float QF;
	float Pente;
	float Eqm;
	int NbSamples;
	float NechRes;
	int Beam;
	int iter;
	bool result;
	float sampleRate;

    // Get the input parameters
    //p_iBeam = (double*)(mxGetPr(prhs[0]));
    //iBeam = (int)(*p_iBeam)-1;
    
    dIn=(double*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);
    if(m_dwReceiveBeams == 1){
        m_dwReceiveBeams = (int)mxGetN(prhs[0]);
    }

    m_pRangeAmplitude = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
        m_pRangeAmplitude[i] = (float)(dIn[i]);
    }

	dIn=(double*)mxGetPr(prhs[1]);
    m_pQualityAmplitude = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
        m_pQualityAmplitude[i] = (float)(dIn[i]);
    }

	/* Not used
	dIn=(double*)mxGetPr(prhs[2]);
    m_pQF2Amplitude = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
        m_pQF2Amplitude[i] = (float)(dIn[i]);
    }
	*/

	dIn=(double*)mxGetPr(prhs[2]);
    m_pRangePhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
        m_pRangePhase[i] = (float)(dIn[i]);
    }

	dIn=(double*)mxGetPr(prhs[3]);
    m_pQualityPhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
        m_pQualityPhase[i] = (float)(dIn[i]);
    }
	
	dIn=(double*)mxGetPr(prhs[4]);
    m_pSlopePhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
        m_pSlopePhase[i] = (float)(dIn[i]);
    }
	
	dIn=(double*)mxGetPr(prhs[5]);
    m_pERMPhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
        m_pERMPhase[i] = (float)(dIn[i]);
    }

    dIn=(double*)mxGetPr(prhs[6]);
	m_fSampleRate = (float)(*dIn); // 376,39264; //Normaly read in input file

    dIn=(double*)mxGetPr(prhs[7]);
	m_fTransmitPulseLength = (float)(*dIn); //0.02; //Normaly read in input file


    // Initializations
	m_pGatesMin = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pGatesMax = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	for(int i=0;i<m_dwReceiveBeams;i++) m_pGatesMax[i]=200000;
	m_pSamplesPhase = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	m_pDetectionType = (char *)mxCalloc(m_dwReceiveBeams, sizeof(char));
	m_pNominalBeams = (bool *)mxCalloc(m_dwReceiveBeams, sizeof(bool));
	m_pDetectionRange = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pDetectionQuality = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));

        
    // Call the C function _barycentre
    result = _detection_synthese();
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pDetectionRange[i] == 0)
		{
			outputDetection[i] = sqrt((double)(-1.0));
		}
		else
		{
			outputDetection[i] = m_pDetectionRange[i];
		}
	}

    plhs[1] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pDetectionType[i] == 0)
		{
			outputDetection[i] = sqrt((double)(-1.0));
		}
		else
		{
			outputDetection[i] = m_pDetectionType[i]/4.0;
		}
	}

    plhs[2] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[2]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pDetectionQuality[i] == 0)
		{
			outputDetection[i] = sqrt((double)(-1.0)); 
		}
		else
		{
			outputDetection[i] = m_pDetectionQuality[i]; 
		}
	}


	// free memory
	mxFree(m_pRangeAmplitude); 
	m_pRangeAmplitude = 0;
	mxFree(m_pQualityAmplitude);
	m_pQualityAmplitude = 0;
	mxFree(m_pQF2Amplitude);
	m_pQF2Amplitude = 0;
	mxFree(m_pRangePhase);
	m_pRangePhase = 0;
	mxFree(m_pQualityPhase);
	m_pQualityPhase = 0;
	mxFree(m_pSlopePhase);
	m_pSlopePhase = 0;
	mxFree(m_pERMPhase);
	m_pERMPhase = 0;
	mxFree(m_pGatesMin);
	m_pGatesMin = 0;
	mxFree(m_pGatesMax);
	m_pGatesMax = 0;
	mxFree(m_pSamplesPhase);
	m_pSamplesPhase = 0;
	mxFree(m_pDetectionType);
	m_pDetectionType = 0;
	mxFree(m_pNominalBeams);
	m_pNominalBeams = 0;
	mxFree(m_pDetectionRange);
	m_pDetectionRange = 0;
	mxFree(m_pDetectionQuality);
	m_pDetectionQuality = 0;

}
