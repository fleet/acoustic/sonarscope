#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* dX;
    DWORD * iX;
    float * fAmp;
    int ampSize;
    double* outputDetection;
    double* outputQuality;
    double* p_dwDeviceId;
    double* p_iBeam;
    int iBeam;
    double* pSampleRate;
    double* pTxPulseWidth;
    double* pNbSamples;
    int NbSamples;
    _raw_data_type * data;

    // Get the input parameters
    fAmp=(float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);
    m_iOutputSamples = mxGetM(prhs[0]);
    data = (_raw_data_type*)mxCalloc(m_dwReceiveBeams*m_iOutputSamples, sizeof(_raw_data_type)); 
	m_pRAWData = (char*)data;
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            data->phs = (short)(fAmp[(iB*m_iOutputSamples)+iS]/5.4931640625e-003f);
        }
    }
	
	fAmp=(float*)mxGetPr(prhs[1]);
    m_dwReceiveBeams = mxGetN(prhs[1]);
    m_iOutputSamples = mxGetM(prhs[1]);
	data = (_raw_data_type*)m_pRAWData;
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            data->amp = (unsigned short)(fAmp[(iB*m_iOutputSamples)+iS]);
        }
    }

	m_pMatrix = _2DArrayAlloc(m_iOutputSamples, m_dwReceiveBeams, (float)0.0f, true);
	//memset(m_pMatrix[0], 0xff, m_iOutputSamples*m_dwReceiveBeams*sizeof(float));
	m_pR1 = (float *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));

	dX = mxGetPr(prhs[2]);
	for(int i=0;i<m_dwReceiveBeams;i++){
		if(dX[i] <= m_iOutputSamples)
		{
			m_pR1[i] = (float)(dX[i] - 1);
		}
		else
		{
			m_pR1[i] = m_iOutputSamples - 1;
		}
	}
	m_pMaskWidth = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));

	dX = (double*)(mxGetPr(prhs[3]));
	for(int i=0;i<m_dwReceiveBeams;i++){
		if(dX[i] == dX[i])
		{
			// Not NAN
			m_pMaskWidth[i] = (DWORD)(dX[i]);
		}
		else
		{
			// NAN
			m_pMaskWidth[i] = 0;
		}
	}

	dX = (double*)(mxGetPr(prhs[4]));
	m_fSampleRate = (float)(*dX);
    
	dX = (double*)(mxGetPr(prhs[5]));
	m_fTransmitPulseLength = (float)(*dX);
            
    // Initializations
	DWORD NbSamplesForPulseLength = max(floordw(m_fTransmitPulseLength*m_fSampleRate),1);
	int allocSamples = max(m_iOutputSamples / 32 + 1, ceili(0.0375f*m_iOutputSamples + 190.0f));
	m_nMaskSamples = max(allocSamples, m_iOutputSamples/NbSamplesForPulseLength+1);
	
	m_pfSamplesA = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesB = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
	m_pAmpReduite = _2DArrayAlloc(m_iOutputSamples, m_dwReceiveBeams, (float)0.0f, true);

    
    // Call the C function _barycentre
    _phase_filtering();
    
    // Set the output parameters
	plhs[0] = mxCreateDoubleMatrix(m_iOutputSamples,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
	for (int iS=0; iS < m_iOutputSamples; iS++) {
		for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            outputDetection[(iB*m_iOutputSamples)+iS] = (double)(m_pMatrix[iS][iB]);
        }
    }  

    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
	outputDetection[0] = 9;

	// free memory
	mxFree(m_pRAWData); 
	m_pRAWData = 0;
	delete(m_pMatrix); 
	m_pMatrix = 0;
	mxFree(m_pR1); 
	m_pR1 = 0;
	mxFree(m_pMaskWidth); 
	m_pMaskWidth = 0;
	mxFree(m_pfSamplesA); 
	m_pfSamplesA = 0;
	mxFree(m_pfSamplesB); 
	m_pfSamplesB = 0;
	delete(m_pAmpReduite); 
	m_pAmpReduite = 0;

}
