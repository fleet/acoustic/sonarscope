#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* outputDetection;
    _raw_data_type * data;
    float * fAmp;
    double *Rmin;
    double *Rmax;
    double* DR0;
	double* Fcurrent;
	double* inputData;
	bool allNan;

    // Get the input parameters
    fAmp=(float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);
    m_iOutputSamples = mxGetM(prhs[0]);
    m_ReduiteStep = (int)(*(mxGetPr(prhs[1])));
    Rmin = mxGetPr(prhs[2]);
    Rmax = mxGetPr(prhs[3]);
    Fcurrent = mxGetPr(prhs[4]);
	m_fTransmitPulseLength = (float)(*Fcurrent);
    Fcurrent = mxGetPr(prhs[5]);
	m_fReceiveBeamWidth = (float)(*Fcurrent);
    Fcurrent = mxGetPr(prhs[6]);
	m_fSampleRate = (float)(*Fcurrent);

    
    // Initializations
    m_dwDeviceId=7150;
	m_nStrips = m_dwReceiveBeams / 14;
    
    data = (_raw_data_type*)mxCalloc(m_dwReceiveBeams*m_iOutputSamples, sizeof(_raw_data_type)); 
	m_pRAWData = (char*)data;
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            data->amp = (unsigned short)(fAmp[(iB*m_iOutputSamples)+iS]);
        }
    }

	inputData = mxGetPr(prhs[7]);
	m_pBeamAngles = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	for(int i=0;i<m_dwReceiveBeams;i++){
		m_pBeamAngles[i] = (float)(inputData[i]*PIf/180);
	}

	m_IdentAlgo = (float)(*(mxGetPr(prhs[8])));

    m_pAmpReduite = _2DArrayAlloc(m_dwReceiveBeams, m_iOutputSamples, (float)0.0f, true);
    m_pfReduite = _2DArrayAlloc(m_dwReceiveBeams, m_iOutputSamples, (float)0.0f, true);
    m_pGatesMin = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pGatesMax = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	for(int i=0; i<m_dwReceiveBeams;i++){
		m_pGatesMin[i] = (float)(Rmin[i]-1.0);
	}
    for(int i=0; i<m_dwReceiveBeams;i++) m_pGatesMax[i] = (float)(Rmax[i]-1.0);
	m_pMatrix = _2DArrayAlloc(m_iOutputSamples,m_dwReceiveBeams, (float)0.0f, true);
    m_pR1 = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pR2 = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pMax = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsB = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsA = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsB = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pFlagBeams = (bool *)mxCalloc(m_dwReceiveBeams, sizeof(bool));
    m_pdBeamsA = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsB = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsC = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsD = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pfSamplesA = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesB = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesC = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesD = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesE = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfBeamsC = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsD = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pDetectionType = (char *)mxCalloc(m_dwReceiveBeams, sizeof(char));
	m_pMaskWidth = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	m_pMaskMin = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	m_pMaskMax = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
        

    // Call the C function _detection_estimate and _mask_7150
    _detection_estimate();
    
    // Set the output parameters for _detection_estimate
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    outputDetection[0] = (m_R0-0.5)*m_ReduiteStep;

    plhs[1] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
	allNan = true;
    for (int iB=0; iB < m_dwReceiveBeams; iB++) {
        outputDetection[iB] = (double)(floorf((m_pR1[iB]-0.5)*m_ReduiteStep));
		if(outputDetection[iB] == outputDetection[iB])
		{
			allNan = false; //at least one value different of NaN
		}
    }    

    plhs[2] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[2]);
    for (int iB=0; iB < m_dwReceiveBeams; iB++) {
		if(m_pR1[iB] == m_pR1[iB])
		{
			outputDetection[iB] = (double)(floorf((m_pR2[iB]-0.5)*m_ReduiteStep));
		}
		else
		{
			outputDetection[iB] = sqrt(-1.0);
		}
    }

    plhs[3] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[3]);
    for (int iB=0; iB < m_dwReceiveBeams; iB++) {
		if(m_pR1[iB] == m_pR1[iB])
		{
			outputDetection[iB] = (double)(m_pMax[iB]);
		}
		else
		{
			outputDetection[iB] = sqrt(-1.0);
		}
    }

    plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[4]);
	if(allNan == false)
	{
		outputDetection[0] = m_IncidentBeam+1;
	}
	else
	{
		outputDetection[0] = sqrt(-1.0);
	}

	// call the _mask_7150 function
	_mask_7150();

    // Set the output parameters for _mask_7150
    plhs[5] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[5]);
    for (int iB=0; iB < m_dwReceiveBeams; iB++) {
		if(m_pR1[iB] == m_pR1[iB])
		{
			outputDetection[iB] = (double)(m_pfBeamsD[iB]+1);
		}
		else
		{
			outputDetection[iB] = sqrt(-1.0);
		}
    }

    plhs[6] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[6]);
    for (int iB=0; iB < m_dwReceiveBeams; iB++) {
		if(m_pR1[iB] == m_pR1[iB])
		{
			outputDetection[iB] = (double)(m_pMaskWidth[iB]);
		}
		else
		{
			outputDetection[iB] = sqrt(-1.0);
		}
    }

	if(allNan == false)
	{
		plhs[7] = mxCreateDoubleMatrix(1,1,mxREAL);
		outputDetection = mxGetPr(plhs[7]);
		outputDetection[0] = (double)(m_iBeamBeg+1);
	}
	else
	{
		plhs[7] = mxCreateDoubleMatrix(0,0,mxREAL);
	}

	if(allNan == false)
	{
		plhs[8] = mxCreateDoubleMatrix(1,1,mxREAL);
		outputDetection = mxGetPr(plhs[8]);
		outputDetection[0] = (double)(m_iBeamEnd+1);
	}
	else
	{
		plhs[8] = mxCreateDoubleMatrix(0,0,mxREAL);
	}

	// free memory allocated by _2DArrayAlloc
	delete(m_pAmpReduite);
	m_pAmpReduite = 0;
	delete(m_pfReduite);
	m_pfReduite = 0;
	delete(m_pMatrix);
	m_pMatrix = 0;
	mxFree(m_pRAWData);
	m_pRAWData = 0;
	mxFree(m_pGatesMin);
	m_pGatesMin = 0;
	mxFree(m_pGatesMax);
	m_pGatesMax = 0;
	mxFree(m_pR1);
	m_pR1 = 0;
	mxFree(m_pR2);
	m_pR2 = 0;
	mxFree(m_pMax);
	m_pMax = 0;
    mxFree(m_pfBeamsB);
	m_pfBeamsB = 0;
    mxFree(m_pfBeamsA);
	m_pfBeamsA = 0;
    mxFree(m_pfBeamsB);
	m_pfBeamsB = 0;
    mxFree(m_pFlagBeams);
	m_pFlagBeams = 0;
    mxFree(m_pdBeamsA);
	m_pdBeamsA = 0;
    mxFree(m_pdBeamsB);
	m_pdBeamsB = 0;
    mxFree(m_pdBeamsC);
	m_pdBeamsC = 0;
    mxFree(m_pdBeamsD);
	m_pdBeamsD = 0;
    mxFree(m_pfSamplesA);
	m_pfSamplesA = 0;
    mxFree(m_pfSamplesB);
	m_pfSamplesB = 0;
    mxFree(m_pfSamplesC);
	m_pfSamplesC = 0;
    mxFree(m_pfSamplesD);
	m_pfSamplesD = 0;
    mxFree(m_pfSamplesE);
	m_pfSamplesE = 0;
    mxFree(m_pfBeamsC);
	m_pfBeamsC = 0;
    mxFree(m_pfBeamsD);
	m_pfBeamsD = 0;
	mxFree(m_pDetectionType);
	m_pDetectionType = 0;
	mxFree(m_pMaskWidth);
	m_pMaskWidth = 0;
	mxFree(m_pMaskMin);
	m_pMaskMin = 0;
	mxFree(m_pMaskMax);
	m_pMaskMax = 0;
	mxFree(m_pBeamAngles);
	m_pBeamAngles = 0;

}
