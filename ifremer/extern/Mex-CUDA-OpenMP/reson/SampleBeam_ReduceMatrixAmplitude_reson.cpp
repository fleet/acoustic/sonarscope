#include "mex.h"
#include "ResonFunctions.cpp"


//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    float * fAmp;
    _raw_data_type * data;
    double* outputDetection;
    

    // Get the input parameters
    fAmp=(float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);
    m_iOutputSamples = mxGetM(prhs[0]);
    
    // Initializations
	m_pAmpReduite = _2DArrayAlloc(m_dwReceiveBeams, m_iOutputSamples, (float)0.0f, true);
	m_pfReduite = _2DArrayAlloc(m_dwReceiveBeams, m_iOutputSamples, (float)0.0f, true);
    data = (_raw_data_type*)mxCalloc(m_dwReceiveBeams*m_iOutputSamples, sizeof(_raw_data_type)); 
	m_pRAWData = (char*)data;
	for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
            data->amp = (unsigned short)(fAmp[(iB*m_iOutputSamples)+iS]);
        }
    }
    
    // Call the C function _matrix_reduction
    _matrix_reduction();
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(m_ReduiteSize,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
        for (int iS=0; iS < m_ReduiteSize; iS++) {
            outputDetection[(iB*m_ReduiteSize) +iS] = (double)(m_pAmpReduite[iB][iS]);
        }
    }    
    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
    outputDetection[0] = m_ReduiteStep;


	// free memory
	delete(m_pAmpReduite);
	m_pAmpReduite = 0;
	delete(m_pfReduite);
	m_pfReduite = 0;
	mxFree(m_pRAWData); 
	m_pRAWData = 0;

}
