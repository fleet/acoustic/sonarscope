#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    float* X;
    DWORD* iX;
    double* iXF;
    double* intTmp;
    int order;
    float sigma;
    float* T;
    int nbSamples;
    double barycentre;
    double* outputBarycentre;
    double* outputSigma;
    
    // Get the input parameters
    X = (float*)mxGetPr(prhs[0]);
    nbSamples = (int)mxGetM(prhs[0]);
    if(nbSamples == 1){
        nbSamples = (int)mxGetN(prhs[0]);
    }
    iXF=(double*)mxGetPr(prhs[1]);
    intTmp = mxGetPr(prhs[2]);
    order = (int)*intTmp;

        
    // Initializations
    T = (float *)mxCalloc(nbSamples, sizeof(float));
    iX = (DWORD *)mxCalloc(nbSamples, sizeof(DWORD));
    for(int i=0; i<nbSamples;i++)
    {
        iX[i] = (int)(iXF[i]);
    }
    
    // Call the C function _barycentre
    barycentre = _barycentre(X, iX, nbSamples, sigma, T, order);
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputBarycentre = mxGetPr(plhs[0]);
    outputBarycentre[0] = barycentre;
    outputSigma = mxGetPr(plhs[1]);
    outputSigma[0] = sigma;

	// free memory
	mxFree(T); 
	T = 0;
	mxFree(iX);
	iX = 0;
}
