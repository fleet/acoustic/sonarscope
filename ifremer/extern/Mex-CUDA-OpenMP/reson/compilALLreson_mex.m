% Compilation des fonctions mex/openMP cr��es par Kevin Le COQ (Ete 2009).
% Tous les fichiers sont compil�s avec l'option openmp.
%
% Syntax
%   compilALLreson_mex(file1, file2, ...)
%   compilALLreson_mex(all)
%
% Input Arguments
%   nom du fichier � compiler,
%   option : soit verbose, soit debuggage, soit ...
%   all : tous les fichiers sont � compiler.
%
% Name-Value Pair Arguments
%   ...
%
% Output Arguments
%   Sans objet
%
% Examples
%   compilALLreson_mex('platform','Win64', 'option', '-v', 'filesource',...
%               {'Reson7150_R0_estimation_V1_reson.cpp' 'SampleBeam_PreprocAmpMask_7150_V4_reson.cpp'});
%   compilALLreson_mex('platform', 'Win64', 'option', '-g');
%
%   % ou en ligne de commande directe :
%     TODO!!!! mex -v C:\SonarScopeTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\fillNaN_mean_mexmc.cpp ...
%     OPTIMFLAGS='$OPTIMFLAGS -openmp' -outdir C:\SonarScopeTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\dll
%
% REMARQUE:
%   Ne pas oublier de changer de Compilateur par mex -setup si l'option de
%   compilation change.
%
% See also compilALL_mexOpenMP
% Authors : ROU
% ----------------------------------------------------------------------------

function compilALLreson_mex(varargin)

global IfrTbx %#ok<GVMIS>

[varargin, platform]   = getPropertyValue(varargin, 'platform', 'Win32');
[varargin, option]     = getPropertyValue(varargin, 'option', []);
[varargin, filesource] = getPropertyValue(varargin, 'filesource', {'all'}); %#ok<ASGLU>

IfrDirSrcMexOpenMP = fullfile(IfrTbx, 'ifremer', 'extern', 'mex', 'Mex-CUDA-OpenMP','reson');
IfrDirDllMexOpenMP = fullfile(IfrDirSrcMexOpenMP, 'dll');

% if ~strcmp(system_dependent('getos'), 'Microsoft Windows 7') %  system_dependent('getwinsys')
    pathMexOpts = ['"C:\Documents and Settings\' getenv('username') '\Application Data\MathWorks\MATLAB\R2011b'];
% else
%     pathMexOpts = ['"C:\Users\' getenv('username') '\AppData\Roaming\MathWorks\MATLAB\R' version('-release') ''];
% end
if strcmpi(platform,'Win32')
    nameMexOpts = 'mexopts32.bat"';
else
    nameMexOpts = 'mexopts.bat"';
end

pathMexOpts = fullfile(pathMexOpts, nameMexOpts);

% Construction de la liste des fichiers � compiler
NameSources = {};
if ~strcmp(filesource, 'all')
    NameSources = {};
    for i=1:numel(filesource)
        NameSources{i} = fullfile(IfrDirSrcMexOpenMP, cell2str(filesource(i)));   %#ok<AGROW>
    end
else
    FileSources = dir( fullfile(IfrDirSrcMexOpenMP, '*_reson.cpp'));
    for i=1:numel(FileSources)
        NameSources{i} = fullfile(IfrDirSrcMexOpenMP, FileSources(i).name); %#ok<AGROW>
    end
end

if nargin ~= 0
    if strcmp(filesource, 'all')
        if isempty(NameSources)
            errordlg(Lang('Aucun fichier source trouv�, v�rifier le contenu du r�pertoire','None source files found. Check the content of the directory'),'Erreur');
        end
        %Compilation successives des diff�rentes fonctions
        for i=1:numel (NameSources)
            if strcmpi(platform,'Win32')
                % OLD cmd = ['mex ' option ' -f "C:\Documents and Settings\' getenv('username') '\Application Data\MathWorks\MATLAB\R2010a\mexopts32.bat" ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS -openmp''' ' -outdir ' IfrDirDllMexOpenMP];
                cmd = ['mex ' option ' -f ' pathMexOpts ' ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS -openmp''' ' -outdir ' IfrDirDllMexOpenMP];
            else
                cmd = ['mex ' option ' ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS -openmp''' ' -outdir ' IfrDirDllMexOpenMP];
            end
            eval(cmd);
        end
    else
        if isempty(filesource)
            errordlg(Lang('Aucun fichier source trouv�, v�rifier le contenu du r�pertoire ou la commande','None source files found. Check the content of the directory or the command'),'Erreur');
        end
        %Compilation unitaire"
        for i = 1:numel(filesource)
            if strcmpi(platform,'Win32')
                % OLD cmd = ['mex ' option ' -f "C:\Documents and Settings\' getenv('username') '\Application Data\MathWorks\MATLAB\R2010a\mexopts32.bat" ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS -openmp''' ' -outdir ' IfrDirDllMexOpenMP];
                cmd = ['mex ' option ' -f '  pathMexOpts ' ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS -openmp''' ' -outdir ' IfrDirDllMexOpenMP];
            else
                cmd = ['mex ' option ' ' cell2str(NameSources(i)) ' OPTIMFLAGS=''$OPTIMFLAGS -openmp''' ' -outdir ' IfrDirDllMexOpenMP];
            end
            eval(cmd);
        end
    end
end
