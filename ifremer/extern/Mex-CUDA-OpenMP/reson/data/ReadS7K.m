function [varargout] = ReadS7K (file, urecord, index, fp, offset)
% ReadS7K - Parser for S7K files
%
%    ReadS7K('file.s7k')                      prints out records and respective counts
%    [records counts] = ReadS7K('file.s7k')   reads records and respective counts
%
%    [variable] = ReadS7K('file.s7k', record, index)
%                      Reads specified record at the location specified by
%                      index. If record does not exists or index exceeds
%                      that record's count execution is stopped with an
%                      error. For output parameters, see record's
%                      description.
%
%    [variable, iterator] = ReadS7K('file.s7k', record, position, iterator)
%                      Reads specified record at the location specified by
%                      the position and iterator. If record does not exists
%                      or iterator is passed the last record execution is
%                      stopped with an error. 
%                      Position can be either 'first' for the first record
%                      in the file, or 'next'. If iterator is not passed in
%                      or is empty, the new iterator is created. If iterator
%                      is not read back it is destroyed after the execution.
%                      To manually destroy an iterator, call ReadS7K with
%                      position set to 'close'.
%                      NOTE, if several record types are read, you need to
%                      created an iterator for each record type.
%                      For output parameters, see record's description.
%
%    EXAMPLE (Reading one record):
%
%            [header] = ReadS7K('file.s7k', 7000, 1);
%    
%    EXAMPLE (Reading all particular records in the file):
%
%            s7kfile = 'file.s7k';
%            [records counts] = ReadS7K(s7kfile);
%            
%            count = min([counts(records == 7000) counts(records == 7004)]);
%
%            i7000 = [];
%            i7004 = [];
%
%            for i = 1:count
%               [h7000, i7000]                                 = ReadS7K(s7kfile, 7000, 'next', i7000);
%               [angleV, angleH, widthY, widthX, h7004, i7004] = ReadS7K(s7kfile, 7004, 'next', i7004);
%
%               % ... processing of the data
%
%            end
%
%            ReadS7K(s7kfile, 7000, 'close', i7000);
%            ReadS7K(s7kfile, 7004, 'close', i7004);
%
%    Following records are available at this time:
%
%         <a href="matlab: help ReadS7K1010">R1010</a>: CTD Profile
%         <a href="matlab: help ReadS7K1016">R1016</a>: Attitude
%         <a href="matlab: help ReadS7K7000">R7000</a>: Sonar Settings
%         <a href="matlab: help ReadS7K7004">R7004</a>: Beam Geometry
%         <a href="matlab: help ReadS7K7006">R7006</a>: Bathymetric Data - OBSOLETE. Use <a href="matlab: help ReadS7K7017">R7017</a>, <a href="matlab: help ReadS7K7026">R7026</a> or <a href="matlab: help ReadS7K7027">R7027</a> instead
%         <a href="matlab: help ReadS7K7008">R7008</a>: Generic Data - OBSOLETE. Use <a href="matlab: help ReadS7K7018">R7018</a> or <a href="matlab: help ReadS7K7028">R7028</a> instead
%         <a href="matlab: help ReadS7K7010">R7010</a>: TVG Gain Values
%         <a href="matlab: help ReadS7K7012">R7012</a>: Ping Motion Data
%         <a href="matlab: help ReadS7K7017">R7017</a>: Bathymetry Data Setup
%         <a href="matlab: help ReadS7K7018">R7018</a>: Beamformed Data (Also: <a href="matlab: help ReadS7K7018IFremer">R7018IFremer</a>)
%         <a href="matlab: help ReadS7K7026">R7026</a>: Compensated Bathymetry Data
%         <a href="matlab: help ReadS7K7027">R7027</a>: RAW Bathymetry Data
%         <a href="matlab: help ReadS7K7028">R7028</a>: Snippets Data
%         <a href="matlab: help ReadS7K7038">R7038</a>: Element I&Q Data
%         <a href="matlab: help ReadS7K7039">R7039</a>: Noise Data
%         <a href="matlab: help ReadS7K7048">R7048</a>: Calibrated Target Strength Data
%         <a href="matlab: help ReadS7K7057">R7057</a>: Calibrated SideScan Data
%         <a href="matlab: help ReadS7K7058">R7058</a>: Calibrated Snippets (Bacscattering Strength) Data
%         <a href="matlab: help ReadS7K7503">R7503</a>: Remote Control Sonar Settings
%
%    ---------------------------------------------------------------------
%    - For more information on each particular record's output arguments -
%    - click on the record above or type "help ReadS7K<record>"          -
%    - For example, type "help ReadS7K7006" to get explanation of record - 
%    - 7006 output arguments.                                            -
%    ---------------------------------------------------------------------
%    - <a href="matlab: help ReadS7KRecord">Click here</a> for instructions on creating record handler fuctions   -
%    - or type "help ReadS7KRecord"                                      -
%    ---------------------------------------------------------------------

returnhandler = false;
usehandler = false;
useoffset = false ;
callargs = '';

if nargin >= 2
    
    if ischar(urecord)
        srecord = urecord;
        urecord = sscanf(srecord, '%d');
    else
        srecord = num2str(urecord);
    end
        
    Handler = which(['ReadS7K' num2str(srecord) '.m']);
    if length(Handler) < 7
        srecord = 'Record';
        Handler = which(['ReadS7K' num2str(srecord) '.m']);
    end

    callargs = 'fp';
    
    if ( nargin(Handler) >= 2 )
        callargs = horzcat(callargs, ', size');
    end

    if ( nargin(Handler) >= 3 )
        callargs = horzcat(callargs, ', odof');
    end
    
    if nargin == 2
        index = 1;
    elseif ~isnumeric(index)
        switch(index)
            case 'first'
                index = 1;
                returnhandler = true;
                
            case 'next'
                if nargin == 3 || isempty(fp)
                    index = 1;
                    returnhandler = true;
                elseif nargin == 5
                    index = 1;
                    returnhandler = true;
                    usehandler = true;
                    useoffset = true;
                else
                    index = 1;
                    returnhandler = true;
                    usehandler = true;
                end
                
            case 'close'
                if nargin > 3 || ~isempty(fp)
                    fclose(fp)
                end
                
                return;
        end
    end
    
    nsubargs = nargout(['ReadS7K' srecord '.m']);

    subargs = '';
    for e=1:nsubargs
        subargs = [subargs 'arg' num2str(e) ' '];
    end
else
    urecord = 0;
end

if ~usehandler
    fp = fopen(file,'rb');

    if fp == -1
        strfile = strrep(file, '\', '\\');
        error ('ReadS7K:FileNotFound',['Failed to open file "' strfile '"']);
    end

    fseek(fp,0,-1);
end

rdrs = zeros (9999,1);
record = 0;
i = 0;
r = 0;
time = zeros(1,6,'double');

if useoffset == true
    fseek(fp,offset,'bof') ;
end

while record ~= 7300 && feof(fp) == 0
    i = i + 1;
    if fseek(fp, 8, 0) == 0
        size = fread(fp, 1, 'uint32');
        odof = fread(fp, 1, 'uint32');
        fseek(fp, 4,0);
        time(1) = fread(fp, 1, 'uint16');
        time(2) = 0;
        time(3) = fread(fp, 1, 'uint16');
        time(6) = fread(fp, 1, 'single');
        time(4) = fread(fp, 1, 'uint8');
        time(5) = fread(fp, 1, 'uint8');
        rtime = datenum(time);
        fseek(fp, 2, 0);
        record = fread(fp, 1, 'uint32');
        device = fread(fp, 1, 'uint32');
        try
        rdrs(record) = rdrs(record) + 1;
        catch
            toto=1;
        end
        if (record == urecord)
            r = r + 1;
            if r == index
                pos = ftell(fp);
                fseek(fp, 24, 0);
                eval(['[' subargs '] = ReadS7K' srecord '(' callargs ');']);
                for e=1:min(nsubargs,nargout)-1
                    varargout(e) = eval(['{arg' num2str(e) '}']);
                end
                if nargout >= nsubargs
                    header = eval(['arg' num2str(nsubargs)]);
                    header.device = device;
                    header.index = i;
                    header.time = rtime;
                    varargout(nsubargs) = {header};
                end
                
                if ~returnhandler
                    fclose(fp);
                else
                    pos2 = ftell(fp);
                    fseek(fp, size - 40 - (pos2-pos), 0);
                    if pos2 - pos ~= size - 44
                        disp(['Record ' num2str(record) ' - inconsistent size: ' num2str(pos2-pos) '/' num2str(size-44)]);
                    end
                    if nargout > nsubargs
                        varargout(nsubargs+1) = {fp};
                    else
                        fclose(fp);
                    end
                end
                return;
            else
                fseek(fp, size - 40, 0);
            end
        else
            fseek(fp, size - 40, 0);
        end
    else
        record = 7300;
    end
end

fclose (fp);

if (urecord > 0)
    error ('ReadS7K:RecordNotFound',['Failed to locate record ' num2str(urecord) ', index ' num2str(index)]);
end

lrecords = find(rdrs);
lcounts  = rdrs(find(rdrs));

if nargout == 0
    disp (' ');
    disp ([lrecords, lcounts]);
else
    varargout{1} = lrecords;
    varargout{2} = lcounts;
end
