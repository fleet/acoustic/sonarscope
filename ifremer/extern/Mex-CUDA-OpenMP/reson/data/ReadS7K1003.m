function [position, header] = ReadS7K1003(fd)
% ReadS7K1003 - 1003 record handler function
%
% [position, header] = ReadS7K(file, 1003, index)
%
% Output Arguments:
% position
% header
%

header = [];

position.datum = fread(fd,[1,1],'uint32') ;
position.latency = fread(fd,[1,1],'float32') ; % In Seconds
position.LatNorh = fread(fd,[1,1],'float64') ; % Latitude in radians or Northing in meters
position.LongEast = fread(fd,[1,1],'float64') ;% Longitude in radians or Easting in meters
position.Height = fread(fd,[1,1],'float64') ;% In meters
data = fread(fd,[1,1],'uint8') ; 
if data == 0
    position.CoordinateSystem = 'Geographical' ;
else
    position.CoordinateSystem = 'Grid' ;
end
position.UTMZone = fread(fd,[1,1],'uint8') ;
data = fread(fd,[1,1],'uint8') ;
if data == 0
    position.Quality = 'Navigation data' ;
else
    position.Quality = 'Dead Reckoning' ;
end




