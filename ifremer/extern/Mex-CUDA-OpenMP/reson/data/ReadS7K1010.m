function [conductivity, salinity, temperature, pressure, depth, velocity, absorption, header] = ReadS7K1010(fd)
% ReadS7K1010 - 1010 record handler function
%
% [conductivity, salinity, temperature, pressure, depth, velocity, absorption, header] = ReadS7K(file, 1010, index)
%
% Output Arguments:
% conductivity - conductivity profile over depth, either conductivity or salinity is present, if at all;
% salinity     - salinity profile over depth, either salinity or conductivity is present, if at all;
% temperature  - temperature profile over depth, if present;
% pressure     - pressure profile for depth determination, either pressure or depth is present;
% depth        - depth profile, either depth or pressure is present;
% velocity     - sound velocity profile over depth, if present;
% absorption   - calculated absorption profile over depth;
% header       - structure describing the record;
%


header.frequency = fread(fd, 1, '*single');
svsf = fread(fd, 1, '*integer*1');
if svsf == 1
    header.source = 'CTD';
elseif svsf == 2
    header.source = 'User';
else
    header.source = 'Not computed';
end
svsf = fread(fd, 1, '*integer*1');
if svsf == 1
    header.algorithm = 'Chen Millero';
elseif svsf == 2
    header.algorithm = 'Del Grosso';
else
    header.algorithm = 'Not computed';
end
fcond = fread(fd, 1, '*integer*1');
fdeep = fread(fd, 1, '*integer*1');
fseek (fd, 1, 0);
fcont = fread(fd, 1, 'integer*1');
fseek (fd, 2, 0);
header.latitude = fread(fd, 1, '*double');
header.longitude = fread(fd, 1, '*double');
header.samplerate = fread(fd, 1, '*single');

samples = fread(fd, 1, 'integer*4');

data = fread(fd, 5*samples, '*single');

if bitand(fcont,1) == 1 && fcond == 0
    conductivity = data(1:5:end);
else
    conductivity = single([]);
end

if bitand(fcont,1) == 1 && fcond == 1
    salinity = data(1:5:end);
else
    salinity = single([]);
end

if bitand(fcont,2) == 2
    temperature = data(2:5:end);
else
    temperature = single([]);
end

if bitand(fcont,4) == 4 && fdeep == 0
    pressure = data(3:5:end);
else
    pressure = single([]);
end

if bitand(fcont,4) == 4 && fdeep == 1
    depth = data(3:5:end);
else
    depth = single([]);
end

if bitand(fcont,8) == 8
    velocity = data(4:5:end);
else
    velocity = single([]);
end

if bitand(fcont,16) == 16
    absorption = data(5:5:end);
else
    absorption = single([]);
end
