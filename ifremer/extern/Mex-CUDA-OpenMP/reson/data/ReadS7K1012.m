function [roll, pitch, heave, header] = ReadS7K1012(fd)
% ReadS7K1010 - 1010 record handler function
%
% [roll, pitch, heave, header] = ReadS7K(file, 1010, index)
%
% Output Arguments:
% roll
% pitch
% heave
%

header = [];

data = fread(fd,[1,3],'float32') ;
roll = data(1) ;
pitch = data(2) ;
heave = data(3) ;


