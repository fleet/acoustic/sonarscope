function [heading, header] = ReadS7K1013(fd)
% ReadS7K1013 - 1013 record handler function
%
% [heading, header] = ReadS7K(file, 1010, index)
%
% Output Arguments:
% heading
% header
%

header = [];

heading = fread(fd,[1,1],'float32') ;


