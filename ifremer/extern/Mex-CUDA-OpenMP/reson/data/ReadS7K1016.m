function [offset, roll, pitch, heave, heading, header] = ReadS7K1016(fd)
% ReadS7K1016 - 1016 record handler function
%
% [offset, roll, pitch, heave, heading, header] = ReadS7K(file, 1016, index)
%
% Output Arguments:
% offset                      - offset in msec from header timestamp
% roll, pitch, heave, heading - in radians
% header                      - structure describing the record:
% header.index                - actual record index in the s7k file
% header.time                 - record timestamp
%

header = [];

sets    = fread(fd, 1, 'integer*1=>single');           % Number of attitude data sets
offset  = zeros(1, sets);
roll    = zeros(1, sets);
pitch   = zeros(1, sets);
heave   = zeros(1, sets);
heading = zeros(1, sets);

for att = 1:sets
    offset(att)  = fread (fd, 1, 'integer*2=>single');  % Time difference in seconds
    attitude     = fread (fd, 4, 'single');
    roll(att)    = attitude(1);
    pitch(att)   = attitude(2);
    heave(att)   = attitude(3);
    heading(att) = attitude(4);
end
    