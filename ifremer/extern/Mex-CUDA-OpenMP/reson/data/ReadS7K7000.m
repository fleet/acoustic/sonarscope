function [header] = ReadS7K7000(fd)
% ReadS7K - 7000 record handler function
%
% [header, ...] = ReadS7K(file, 7000, ...)
%
% Output Arguments:
% header                    - Sonar Settings and Record Data (structure)
% |                           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
% +-- <a href="matlab: disp('header.ping');">ping</a>                  - sequential ping number
% +-- <a href="matlab: disp('header.index');">index</a>                 - actual record index in the s7k file
% +-- <a href="matlab: disp('header.device');">device</a>                - system ID
% +-- <a href="matlab: disp('header.time');">time</a>                  - record's serial date number (see <a href="matlab: help datestr">DATESTR</a> for further details)
% |                           
% +-- <a href="matlab: disp('header.multiping');">multiping</a>             - multiping sequence number, 0 if no multiping
% +-- <a href="matlab: disp('header.frequency');">frequency</a>             - transmit frequency in Hz
% +-- <a href="matlab: disp('header.samplerate');">samplerate</a>            - sampling rate in Hz
% |
% +-- rx                    - receiver setting (structure)
% |   +-- <a href="matlab: disp('header.rx.bandwidth');">bandwidth</a>         - receiver's bandwidth
% |   +-- <a href="matlab: disp('header.rx.weighting');">weighting</a>         - receive beam weighting window ('Kaiser' or 'Chebychev' - string)
% |   +-- <a href="matlab: disp('header.rx.beamwidth');">beamwidth</a>         - receive beam width in radians
% |
% +-- tx                    - transmitter setting (structure)
% |   +-- pulse             - transmitter pulse settings (structure)
% |   |   +-- <a href="matlab: disp('header.tx.pulse.width');">width</a>         - Tx pulse width in sec
% |   |   +-- <a href="matlab: disp('header.tx.pulse.type');">type</a>          - pulse type - 'CW' or 'FM' (string)
% |   |   +-- <a href="matlab: disp('header.tx.pulse.envelope');">envelope</a>      - envelope type - 'Tapered rectangular' or 'Tukey' (string)
% |   |
% |   +-- steering          - structure describing transmitter steering:
% |   |   +-- <a href="matlab: disp('header.tx.steering.vertical');">vertical</a>      - vertical beam steering angle in radians
% |   |   +-- <a href="matlab: disp('header.tx.steering.horizontal');">horizontal</a>    - horizontal beam steering angle in radians
% |   |
% |   +-- beamwidth         - structure describing transmitter beam width:
% |   |   +-- <a href="matlab: disp('header.tx.beamwidth.vertical');">vertical</a>      - vertical -3dB beam width in radians
% |   |   +-- <a href="matlab: disp('header.tx.beamwidth.horizontal');">horizontal</a>    - horizontal -3dB beam width in radians
% |   |
% |   +-- <a href="matlab: disp('header.tx.focalpoint');">focalpoint</a>        - projector beam focal point in meters
% |   +-- <a href="matlab: disp('header.tx.weighting');">weighting</a>         - projector beam weighting window type - 'Chebychev' or 'Rectangular' (string)
% |
% +-- <a href="matlab: disp('header.pingrate');">pingrate</a>              - maximum system ping rate in Hz
% +-- <a href="matlab: disp('header.pingperiod');">pingperiod</a>            - seconds since previous ping
% +-- <a href="matlab: disp('header.range');">range</a>                 - range in meters
% +-- <a href="matlab: disp('header.power');">power</a>                 - output power setting
% +-- <a href="matlab: disp('header.gainselection');">gainselection</a>         - receiver gain setting
% |
% +-- stabilization         - motion stabilization setting (structure)
% |   +-- <a href="matlab: disp('header.stabilization.pitch');">pitch</a>             - indicates if pitch stabilization is enabled (logical)
% |   +-- <a href="matlab: disp('header.stabilization.roll');">roll</a>              - indicates if roll stabilization is enabled (logical)
% |   +-- <a href="matlab: disp('header.stabilization.yaw');">yaw</a>               - indicates if yaw stabilization is enabled (logical)
% |
% +-- bottomdetect          - bottom detection filter settings (structure)
% |   |                       (Note: if particular filter is not used,
% |   |                              all its settings are set to NaN)
% |   +-- adaptivegates     - B.D. adaptive filter settings (structure)
% |   |   +-- <a href="matlab: disp('header.bottomdetect.adaptivegates.mindepth');">mindepth</a>      - adaptive filter minimum depth
% |   |   +-- <a href="matlab: disp('header.bottomdetect.adaptivegates.maxdepth');">maxdepth</a>      - adaptive filter maximum depth
% |   |
% |   +-- absolutegates     - B.D. absolute filter settings (structure)
% |       +-- <a href="matlab: disp('header.bottomdetect.adaptivegates.minrange');">minrange</a>      - absolute filter minimum range
% |       +-- <a href="matlab: disp('header.bottomdetect.adaptivegates.maxrange');">maxrange</a>      - absolute filter maximum range
% |       +-- <a href="matlab: disp('header.bottomdetect.adaptivegates.mindepth');">mindepth</a>      - absolute filter minimum depth
% |       +-- <a href="matlab: disp('header.bottomdetect.adaptivegates.maxdepth');">maxdepth</a>      - absolute filter maximum depth
% |
% +-- <a href="matlab: disp('header.absorption');">absorption</a>            - absorption setting
% +-- <a href="matlab: disp('header.soundvelocity');">soundvelocity</a>         - speed of sound
% +-- <a href="matlab: disp('header.spreading');">spreading</a>             - spreading loss setting
% +-- <a href="matlab: disp('header.pps');">pps</a>                   - pps code
%
% See Also: <a href="matlab: help ReadS7K">ReadS7K</a>, <a href="matlab: help ReadS7K7503">R7503</a>

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread(fd, 1, '*integer*2');
header.frequency = fread(fd, 1, '*single');
header.samplerate = fread(fd, 1, '*single');
header.rx.bandwidth = fread(fd, 1, '*single');
header.tx.pulse.width = fread(fd, 1, '*single');
temp = fread(fd, 1, '*integer*4');
if temp == 1
    header.tx.pulse.type = 'FM';
else
    header.tx.pulse.type = 'CW';
end
temp = fread(fd, 1, '*integer*4');
if temp == 1
    header.tx.pulse.envelope = 'Tukey';
else
    header.tx.pulse.envelope = 'Tapered rectangular';
end
fseek (fd, 8, 0);
header.pingrate = fread(fd, 1, '*single');
header.pingperiod  = fread(fd, 1, '*single');
header.range = fread(fd, 1, '*single');
header.power = fread(fd, 1, '*single');
header.gainselection = fread(fd, 1, '*single');
controlFlags = fread(fd, 1, '*uint32');
fseek(fd, 4, 0);    % Projector identifier
header.tx.steering.vertical    = fread(fd, 1, '*single');
header.tx.steering.horizontal  = fread(fd, 1, '*single');
header.tx.beamwidth.vertical   = fread(fd, 1, '*single');
header.tx.beamwidth.horizontal = fread(fd, 1, '*single');
header.tx.focalpoint           = fread(fd, 1, '*single');
temp = fread(fd, 1, '*integer*4');
if temp == 1
    header.tx.weighting = 'Chebychev';
else
    header.tx.weighting = 'Rectangular';
end

fseek(fd, 4, 0);

txFlags = fread(fd, 1, '*uint32');

if bitand(txFlags, 15) > 0
    header.stabilization.pitch = true;
else 
    header.stabilization.pitch = false;
end

if bitand(txFlags, 240) > 0
    header.stabilization.yaw = true;
else 
    header.stabilization.yaw = false;
end

fseek(fd, 4, 0);    % Hydrophone identifier

temp = fread(fd, 1, '*uint32');
if temp == 1
    header.rx.weighting = 'Kaiser';
else
    header.rx.weighting = 'Chebychev';
end

fseek(fd, 4, 0);    % Receive beam weighting parameter

rxFlags = fread(fd, 1, '*uint32');

if bitand(rxFlags, 15) > 0
    header.stabilization.roll = true;
else 
    header.stabilization.roll = false;
end

header.rx.beamwidth = fread(fd, 1, '*single');

minrange = fread(fd, 1, '*single');
maxrange = fread(fd, 1, '*single');
mindepth = fread(fd, 1, '*single');
maxdepth = fread(fd, 1, '*single');
header.absorption = fread(fd, 1, '*single');
header.soundvelocity = fread(fd, 1, '*single');
header.spreading = fread(fd, 1, '*single');
dum = fread(fd,1,'uint16') ;

header.bottomdetect.adaptivegates.mindepth = NaN;
header.bottomdetect.adaptivegates.maxdepth = NaN;
header.bottomdetect.absolutegates.minrange = NaN;
header.bottomdetect.absolutegates.maxrange = NaN;
header.bottomdetect.absolutegates.mindepth = NaN;
header.bottomdetect.absolutegates.maxdepth = NaN;

if mod(floor(controlFlags / 2^22),2)        % Adaptive gates enabled
    header.bottomdetect.adaptivegates.mindepth = mindepth;
    header.bottomdetect.adaptivegates.maxdepth = maxdepth;
else
    if mod(floor(controlFlags / 2^8),2)        % Range gates enabled
        header.bottomdetect.absolutegates.minrange = minrange;
        header.bottomdetect.absolutegates.maxrange = maxrange;
    end
    
    if mod(floor(controlFlags / 2^9),2)        % Depth gates enabled
        header.bottomdetect.absolutegates.mindepth = mindepth;
        header.bottomdetect.absolutegates.maxdepth = maxdepth;
    end
end

header.pps = bitand(controlFlags, 402653184) / 134217728;
