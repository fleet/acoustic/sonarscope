function [angleY, angleX, widthY, widthX, header] = ReadS7K7004(fd)
% ReadS7K - 7004 record handler function
%
% [angleV, angleH, widthY, widthX, header, ...] = <a href="matlab: help ReadS7K">ReadS7K</a>(file, 7004, ...)
%
% Output Arguments:
% angleY        - The receiver along-track beam steering angle in radians                     [beams]
% angleX        - The receiver across-track beam steering angle in radians                    [beams]
% widthY        - The receiver along-track beam width measured at the -3dB points in radians  [beams]
% widthX        - The receiver across-track beam width measured at the -3dB points in radians [beams]
%
% header        - Record Data (structure)
% |               ^^^^^^^^^^^
% +-- <a href="matlab: disp('header.ping');">ping</a>      - sequential ping number
% +-- <a href="matlab: disp('header.index');">index</a>     - actual record index in the s7k file
% +-- <a href="matlab: disp('header.device');">device</a>    - system ID
% +-- <a href="matlab: disp('header.time');">time</a>      - record's serial date number (see <a href="matlab: help datestr">DATESTR</a> for further details)
%
% See Also: <a href="matlab: help ReadS7K">ReadS7K</a>
%

fseek (fd, 8, 0);
beams = fread(fd, 1, 'integer*4=>double');
angleY = fread(fd, beams, '*single');
angleX = fread(fd, beams, '*single');
widthY = fread(fd, beams, '*single');
widthX = fread(fd, beams, '*single');

header = [];
