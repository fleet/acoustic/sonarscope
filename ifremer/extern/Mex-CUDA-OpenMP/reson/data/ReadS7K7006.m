function [range, quality, detection, intensity, filter, header] = ReadS7K7006(fd)
% ReadS7K7006 - 7006 record handler function
%
% [range, quality, detection, intensity, filter, header] = ReadS7K(file, 7006, index)
%
% Output Arguments:
% range            - two-way travel time in seconds
% quality          - quality flags:
%                    0 - brightness and colinearity failed
%                    1 - brightness passed / colinearity failed
%                    2 - brightness failed / colinearity passed
%                    3 - brightness and colinearity passed
% intensity        - for non-calibrated system signal strength at bottom detect
%                    for calibrated system backscatter strength at bottom detect
% filter           - structure describing gates:
% filter.min       - two-way travel time to minimum depth gate point in seconds
% filter.max       - two-way travel time to maximum depth gate point in seconds
% header           - structure describing the record:
% header.ping      - sequential ping number
% header.multiping - multiping sequence number, 0 if no multiping
% header.beams     - number of beams
% header.velocity  - speed of sound
% header.index     - actual record index in the s7k file
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
header.beams = fread(fd, 1, '*integer*4');
beams = double(header.beams);
fseek (fd, 2, 0);
header.velocity = fread(fd, 1, '*single');
range = fread(fd, beams, '*single');
temp  = fread(fd, beams, 'uint8');
quality = bitand(temp,3);
detection = bitand(temp,12) ./ 4;
intensity = fread(fd, beams, '*single');
filter.min = fread(fd, beams, '*single');
filter.max = fread(fd, beams, '*single');
