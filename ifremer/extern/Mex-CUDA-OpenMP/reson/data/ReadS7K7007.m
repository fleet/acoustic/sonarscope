function [port, starboard , header] = ReadS7K7007(fd)
% ReadS7K70078 - 7007 record handler function
%
% [port, starboard , header] = ReadS7K(file, 7007, index)
%
% Output Arguments:
% port             - port side magnitude time series
% starboard        - starboard side magnitude time series
% header           - structure describing the record:
% header.ping      - sequential ping number
% header.multiping - multiping sequence number, 0 if no multiping
% header.index     - actual record index in the s7k file
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
header.beam_position = fread(fd, 1, 'integer*4');
header.control_flags = fread(fd, 1, 'uint32');
header.samples  = fread(fd, 1, 'uint32');
header.port_beamwidth_Y = fread(fd, 1, 'integer*4');
header.port_beamwidth_Z = fread(fd, 1, 'integer*4');
header.starboard_beamwidth_Y = fread(fd, 1, 'integer*4');
header.starboard_beamwidth_Z = fread(fd, 1, 'integer*4');
header.port_steering_Y = fread(fd, 1, 'integer*4');
header.port_steering_Z = fread(fd, 1, 'integer*4');
header.starboard_steering_Y = fread(fd, 1, 'integer*4');
header.starboard_steering_Z = fread(fd, 1, 'integer*4');
header.beam_number = fread(fd, 1, 'uint16');
header.beam_id = fread(fd, 1, 'uint16');
header.byte_per_sample = fread(fd, 1, 'uint8');
header.data_type = fread(fd, 1, 'uint8');
switch header.byte_per_sample
    case 1
        port  = fread(fd,[1 header.samples],'uint8') ;
        starboard = fread(fd,[1 header.samples],'uint8') ;
    case 2
        port  = fread(fd,[1 header.samples],'uint16') ;
        starboard = fread(fd,[1 header.samples],'uint16') ;
end
end
