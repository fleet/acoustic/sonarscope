function [magnitude, phase, mask, header] = ReadS7K7008(fd)
% ReadS7K7008 - 7008 record handler function
%
% [magnitude, phase, mask, header] = ReadS7K(file, 7008, index)
%
% Output Arguments:
% magnitude        - magnitude values for the ping         [samples, beams]
% phase            - phase values for the ping             [samples, beams]
%                    phase is in radians scaled by 10431
% mask             - snippets data mask                    [samples, beams]
%                    1 for valid data points, 0 otherwise
% header           - structure describing the record:
% header.ping      - sequential ping number
% header.multiping - multiping sequence number, 0 if no multiping
% header.index     - actual record index in the s7k file
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
beams = fread(fd, 1, 'integer*2');
fseek (fd, 2, 0);
samples = fread(fd, 1, 'integer*4');
subset = fread(fd, 1, 'integer*1=>single');
flow   = fread(fd, 1, 'integer*1=>single');
fseek (fd, 2, 0);
type   = fread(fd, 1, 'integer*4=>single');
beam_array = fread(fd, beams*5, '*integer*2');
beam_number   = beam_array(1:5:end);
first_sample  = beam_array(3:5:end)*65536+beam_array(2:5:end);
last_sample   = beam_array(5:5:end)*65536+beam_array(4:5:end);

if subset == 0
    max_sample = samples;
    max_beam   = beams;
else
    max_sample = max(last_sample)+1;
    max_beam   = max(beam_number)+1;
end

magnitude = zeros(max_sample, max_beam);
phase     = zeros(max_sample, max_beam);
mask      = zeros(max_sample, max_beam);

switch type
    case  2
        mag = 1; phs = 0;   % Mag only
    case 32
        mag = 0; phs = 1;   % Phase only
    case 34
        mag = 1; phs = 1;   % Mag & Phase series
    otherwise
        return;             % Neither mag nor phase (or I&Q)
end

typeset = mag + phs;

if flow == 0
    disp('R7008 Snippets!!!');
    for i=1:beams
        dataset = fread(fd, double(last_sample(i)-first_sample(i)+1)*typeset, '*integer*2');
        if mag == 1
            magnitude((first_sample(i):last_sample(i))+1, beam_number(i)+1) = single(dataset(1:typeset:end));
            magnitude(magnitude<0) = 65535 + magnitude(magnitude<0);
        end
        
        if phs == 1
            phase((first_sample(i):last_sample(i))+1, beam_number(i)+1) = single(dataset(1+mag:typeset:end));
        end
        
        mask((first_sample(i):last_sample(i))+1, beam_number(i)+1) = 1;
    end
else
    dataset = fread(fd, double((last_sample(1)-first_sample(1)+1))*beams*typeset, '*integer*2');
    
    if mag == 1
        magnitude((first_sample(1):last_sample(1))+1, beam_number+1) = reshape(single(dataset(1:typeset:end)), beams, max_sample)';
        magnitude(magnitude<0) = 65535 + magnitude(magnitude<0);
    end
    
    if phs == 1
        phase((first_sample(1):last_sample(1))+1, beam_number+1) = reshape(single(dataset(1+mag:typeset:end)), beams, max_sample)';
    end
    
    mask((first_sample(1):last_sample(1))+1, beam_number+1) = 1;
end
    