function [gains, header] = ReadS7K7010(fd)
% ReadS7K7010 - 7010 record handler function
%
% DO NOT call this function directly, use
%
% [gains, header] = ReadS7K(file, 7010, index)
%
% instead.
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
header.samples = fread(fd, 1, '*integer*4');
samples = double(header.samples);
fseek (fd, 4*8, 0);
gains = fread(fd, samples, '*single');
