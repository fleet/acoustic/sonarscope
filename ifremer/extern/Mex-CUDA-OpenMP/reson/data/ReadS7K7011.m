function [magnitude, header] = ReadS7K7011(fd)
% ReadS7K - 7011 record handler function
%
% [magnitude, header, ...] = <a href="matlab: help ReadS7K">ReadS7K</a>(file, 7018, ...)
%
% Output Arguments:
% magnitude        - magnitude values for the ping         [beams, samples]
%


header.ping = fread(fd, 1, '*integer*4');           % Ping Number
header.multiping = fread (fd, 1, '*integer*2');     % M-Ping Sequence
header.width = fread(fd, 1, 'integer*4');                  % Image Width
header.height = fread(fd, 1, 'integer*4');                 % Image Height
header.depth = fread(fd, 1, 'integer*2');                 % Color depth
reserved = fread(fd, 1, 'integer*2');                 % 
header.compression = fread(fd, 1, 'integer*2');                 % algorithm
header.samples = fread(fd, 1, 'integer*4');                 % Sample number before compression
fseek (fd, 8*4, 0);                                 % Reserved

switch header.depth
    case 1
    ima = fread(fd, double(header.width*header.height), 'uint8');
    case 2
    ima = fread(fd, double(header.width*header.height), 'uint16');
    case 4
    ima = fread(fd, double(header.width*header.height), 'uint32');
    otherwise
        error('format not supported')
end
magnitude = reshape(ima,header.width,header.height)' ;
    