function [roll, pitch, heading, heave, header] = ReadS7K7012(fd)
% ReadS7K7010 - 7010 record handler function
%
% DO NOT call this function directly, use
%
% [roll, pitch, heading, heave, header] = ReadS7K(file, 7012, index)
%
% instead.
%

roll    = [];
pitch   = [];
heading = [];
heave   = [];

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
header.samples = fread(fd, 1, '*integer*4');
temp = fread(fd, 1, '*uint16');
header.errorflag  = fread(fd, 1, '*integer*4');
header.samplerate = fread(fd, 1, '*single');
samp = double(header.samples);

if bitand(temp, 1)
    pitch = fread(fd, 1, '*single');
end

if bitand(temp, 2)
    roll = fread(fd, samp, '*single');
end

if bitand(temp, 4)
    heading = fread(fd, samp, '*single');
end

if bitand(temp, 8)
    heave = fread(fd, samp, '*single');
end

