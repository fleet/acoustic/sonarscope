function [range, beam, quality, limits, header] = ReadS7K7017(fd)
% ReadS7K - 7017 record handler function
%
% [range, beam, quality, limits, header, ...] = <a href="matlab: help ReadS7K">ReadS7K</a>(file, 7017, ...)
%
% Output Arguments:
% range              - fractional sample number
% beam               - integer beam index from which detection is taken (1-based)
% quality            - quality flags:
%                      0 - brightness and colinearity failed
%                      1 - brightness passed / colinearity failed
%                      2 - brightness failed / colinearity passed
%                      3 - brightness and colinearity passed
% limits             - 4 by N matrix defining consideration limits for the detection, where N
% |                    is a total number of detections (Note, if any of these are not available,
% |                    correspondings values are set to NaN):
% +-- limits(1,:)    - Minimum sample number for automatic limits
% +-- limits(2,:)    - Maximum sample number for automatic limits
% +-- limits(3,:)    - Minimum sample number for user-defined limits
% +-- limits(4,:)    - Maximum sample number for user-defined limits
%
% header             - Record Data (structure)
% |                    ^^^^^^^^^^^
% +-- <a href="matlab: disp('header.ping');">ping</a>           - sequential ping number
% +-- <a href="matlab: disp('header.index');">index</a>          - actual record index in the s7k file
% +-- <a href="matlab: disp('header.device');">device</a>         - system ID
% +-- <a href="matlab: disp('header.time');">time</a>           - record's serial date number (see <a href="matlab: help datestr">DATESTR</a> for further details)
% +-- <a href="matlab: disp('header.multiping');">multiping</a>      - multiping sequence number, 0 if no multiping
% +-- filter         - detection limits (structure)
%     |                (Note, if any of these are not available, correspondings values are set to NaN)
%     |
%     +-- user       - user-defined detection limits (structure)
%     |   +-- <a href="matlab: disp('header.filter.user.depth');">depth</a>  - Depth limits for user-defined filter in meters (2 by 1 vector)
%     |   +-- <a href="matlab: disp('header.filter.user.range');">range</a>  - Range limits for user-defined filter in meters (2 by 1 vector)
%     +-- auto       - user-defined detection limits (structure)
%         +-- <a href="matlab: disp('header.filter.auto.depth');">depth</a>  - Depth limits for automatic filter in meters (2 by 1 vector)
%         +-- <a href="matlab: disp('header.filter.auto.window');">window</a> - Detection window size for automatic filter in per-cents
%
% See Also: <a href="matlab: help ReadS7K">ReadS7K</a>, <a href="matlab: help ReadS7K7006">R7006</a>, <a href="matlab: help ReadS7K7026">R7026</a>, <a href="matlab: help ReadS7K7027">R7027</a>

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping  = fread (fd, 1, '*integer*2');
points = fread(fd, 1, 'integer*4=>double');
size   = fread(fd, 1, 'integer*4=>double');
fseek (fd, 1, 0);
flags  = fread(fd, 1, '*uint32');
header.filter.user.depth = fread(fd, 2, '*single');
header.filter.user.range = fread(fd, 2, '*single');
header.filter.auto.depth = fread(fd, 2, '*single');
header.filter.auto.window = fread(fd, 1, 'integer*1=>single');

if bitand(flags, 1) == 0
    header.filter.user.depth = NaN;
end

if bitand(flags, 2) == 0
    header.filter.user.range = NaN;
end

if bitand(flags, 4) == 0
    header.filter.auto.depth = NaN;
    header.filter.auto.window = NaN;
else
    if bitand(flags, 8) == 0
        header.filter.auto.depth = NaN;
    end
    
    if bitand(flags, 16) == 0
        header.filter.auto.window = NaN;
    end
end
    
fseek (fd, 16*4, 0);

if size < 30
    range     = [];
    beam      = [];
    quality   = [];
    limits    = [];
    return;
end

range     = zeros(1, points, 'single');
beam      = zeros(1, points, 'single');
quality   = zeros(1, points, 'single');
limits    = zeros(4, points, 'single');

for i=1:points
    beam(i)      = fread(fd, 1, 'integer*2=>single');
    range(i)     = fread(fd, 1, '*single');
    temp         = fread(fd, 1, '*uint32');
    limits(:,i)  = fread(fd, 4, '*single');
    qtype        = bitand(temp,508) ./ 4;
    
    switch qtype
        case 1
            quality(i) = fread(fd, 1, 'uint32=>single');
        otherwise
            quality(i) = NaN;
            fseek(fd, 4, 0);
    end

    if bitand(temp, 1) == 0
        limits(1:2,i) = NaN;
    end
    
    if bitand(temp, 2) == 0
        limits(3:4,i) = NaN;
    end
    
    if size > 30
        fseek(fd, size-30, 0);
    end
end
