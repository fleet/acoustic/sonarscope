function [magnitude, phase, header] = ReadS7K7018(fd)
% ReadS7K - 7018 record handler function
%
% [magnitude, phase, header, ...] = <a href="matlab: help ReadS7K">ReadS7K</a>(file, 7018, ...)
%
% Output Arguments:
% magnitude        - magnitude values for the ping         [samples, beams]
% phase            - phase values in radians for the ping  [samples, beams]
%
% header           - Record Data (structure)
% |                  ^^^^^^^^^^^
% +-- <a href="matlab: disp('header.ping');">ping</a>         - sequential ping number
% +-- <a href="matlab: disp('header.index');">index</a>        - actual record index in the s7k file
% +-- <a href="matlab: disp('header.device');">device</a>       - system ID
% +-- <a href="matlab: disp('header.time');">time</a>         - record's serial date number (see <a href="matlab: help datestr">DATESTR</a> for further details)
% +-- <a href="matlab: disp('header.multiping');">multiping</a>    - multiping sequence number, 0 if no multiping
%
% See Also: <a href="matlab: help ReadS7K">ReadS7K</a>, <a href="matlab: help ReadS7K7008">R7008</a>

flagOld = 0;

fseek (fd, 8, 0);                                   % Serial Number
header.ping = fread(fd, 1, '*integer*4');           % Ping Number
header.multiping = fread (fd, 1, '*integer*2');     % M-Ping Sequence
beams = fread(fd, 1, 'integer*2');                  % Beam Number
if beams == 0
    % Old datagrams for 7111 and 7150
    flagOld = 1;
    beams = fread(fd, 1, 'integer*2');                  % Beam Number
end
samples = fread(fd, 1, 'integer*4');                % Samples

if(flagOld == 1)
    fseek (fd, 35, 0);                                 % Reserved
else
    fseek (fd, 32, 0);                                 % Reserved
end

pos = ftell(fd);

dataset = fread(fd, double(samples*beams*2), 'int16=>single');

magnitude = reshape(double(dataset(1:2:end)), beams, samples)';
magnitude(magnitude<0) = 65535 + magnitude(magnitude<0);

phase = reshape(double(dataset(2:2:end)), beams, samples)';
phase = phase * (pi/32768);
    