function [range, beam, angle, quality, detection, tpe, header] = ReadS7K7027(fd)
% ReadS7K - 7027 record handler function
%
% [range, beam, angle, quality, detection, tpe, header, ...] = ReadS7K(file, 7027, ...)
%
% Output Arguments:
% range            - fractional sample number
% angle            - receive steering angle at the sample
% quality          - quality flags:
%                    0 - brightness and colinearity failed
%                    1 - brightness passed / colinearity failed
%                    2 - brightness failed / colinearity passed
%                    3 - brightness and colinearity passed
% detection        - detection type:
%                    0 - no valid detection
%                    1 - amplitude detection
%                    2 - phase detection
%                    3 - blend detection (amplitude and phase)
% tpe              - total propagation error normalized to the detection point
%
% header           - Record Data (structure)
% |                  ^^^^^^^^^^^
% +-- <a href="matlab: disp('header.ping');">ping</a>         - sequential ping number
% +-- <a href="matlab: disp('header.index');">index</a>        - actual record index in the s7k file
% +-- <a href="matlab: disp('header.device');">device</a>       - system ID
% +-- <a href="matlab: disp('header.time');">time</a>         - record's serial date number (see <a href="matlab: help datestr">DATESTR</a> for further details)
% +-- <a href="matlab: disp('header.multiping');">multiping</a>    - multiping sequence number, 0 if no multiping
% +-- <a href="matlab: disp('header.samplerate');">samplerate</a>   - sampling rate in Hz
% +-- <a href="matlab: disp('header.txangle');">txangle</a>      - transmit steering angle at the ping time
%
% See Also: <a href="matlab: help ReadS7K">ReadS7K</a>, <a href="matlab: help ReadS7K7006">R7006</a>, <a href="matlab: help ReadS7K7017">R7017</a>, <a href="matlab: help ReadS7K7026">R7026</a>

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping  = fread (fd, 1, '*integer*2');
points = fread(fd, 1, 'integer*4=>double');
size   = fread(fd, 1, 'integer*4=>double');
fseek (fd, 5, 0);
header.samplerate = fread(fd, 1, '*single');
header.txangle    = fread(fd, 1, '*single');
fseek (fd, 16*4, 0);

if size < 22
    range     = [];
    beam      = [];
    angle     = [];
    quality   = [];
    detection = [];
    tpe       = [];
    return;
end

range     = zeros(1, points, 'single');
beam      = zeros(1, points, 'single');
angle     = zeros(1, points, 'single');
detection = zeros(1, points, 'single');
quality   = zeros(1, points, 'single');
tpe       = zeros(1, points, 'single');

for i=1:points
    beam(i)      = fread(fd, 1, 'uint16=>single');
    range(i)     = fread(fd, 1, '*single');
    angle(i)     = fread(fd, 1, '*single');
    temp         = fread(fd, 1, '*uint32');
    detection(i) = bitand(temp,3);
    qtype        = bitand(temp,508) ./ 4;
    
    switch qtype
        case 1
            quality(i) = fread(fd, 1, 'uint32=>single');
        otherwise
            quality(i) = NaN;
            fseek(fd, 4, 0);
    end
    
    tpe(i)       = fread(fd, 1, '*single');
    
    if size > 22
        fseek(fd, size-22, 0);
    end
end
