function [magnitude, beam, start, det, stop, header] = ReadS7K7028(fd)
% ReadS7K - 7028 record handler function
%
% [magnitude, beam, start, det, stop, header, ...] = <a href="matlab: help ReadS7K">ReadS7K</a>(file, 7028, ...)
%
% Output Arguments:
% magnitude        - magnitude values for the ping         [samples, beams]
%                    values not available in the data are set to NaN
%
% header           - Record Data (structure)
% |                  ^^^^^^^^^^^
% +-- <a href="matlab: disp('header.ping');">ping</a>         - sequential ping number
% +-- <a href="matlab: disp('header.index');">index</a>        - actual record index in the s7k file
% +-- <a href="matlab: disp('header.device');">device</a>       - system ID
% +-- <a href="matlab: disp('header.time');">time</a>         - record's serial date number (see <a href="matlab: help datestr">DATESTR</a> for further details)
% +-- <a href="matlab: disp('header.multiping');">multiping</a>    - multiping sequence number, 0 if no multiping
%
% See Also: <a href="matlab: help ReadS7K">ReadS7K</a>, <a href="matlab: help ReadS7K7008">R7008</a>
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
detections = fread(fd, 1, 'integer*2=>double');

if detections <= 0
    magnitude = [];
    return;
end

fseek (fd, 4*7+2, 0);

beam  = NaN(1, detections);
start = NaN(1, detections);
stop  = NaN(1, detections);

for i=1:detections
    beam(i)  = fread(fd, 1, 'uint16=>double');
    points   = fread(fd, 3, 'uint32=>double');
    start(i) = points(1);
    det(i)   = points(2);
    stop(i)  = points(3);
end

beam  = beam+1;     % Adjust for MatLab 1-based indexing
start = start+1;
stop  = stop+1;

beams   = max(beam);
samples = max([start stop]);

magnitude = NaN(samples, beams, 'single');

for i=1:detections
    magnitude(start(i):stop(i),beam(i)) = fread(fd, stop(i)-start(i)+1, 'uint16=>single');
end
    