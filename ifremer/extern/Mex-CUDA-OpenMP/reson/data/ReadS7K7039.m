function [wedgenoise, channelnoise, samplenoise, header] = ReadS7K7039(fd)
% ReadS7K7039 - 7039 record handler function
%
% [wedgenoise, channelnoise, samplenoise, header] = ReadS7K(file, 7039, index)
%
% wedgenoise       - total ambient noise
% channelnoise     - average channel noise    [       1, channels ]
% samplenoise      - sample noise values      [ samples, channels ]
% header           - structure describing the record:
% header.ping      - sequential ping number
% header.index     - actual record index in the s7k file
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
fseek (fd, 2, 0);
channels = fread(fd, 1, 'integer*2');
samples  = fread(fd, 1, 'integer*4');
actual_channels = fread(fd, 1, 'integer*2');
min_sample   = fread(fd, 1, 'integer*4');
max_sample   = fread(fd, 1, 'integer*4');
wedgenoise   = fread(fd, 1, '*single');
fseek (fd, 26, 0);
chan_array   = fread(fd, actual_channels, 'integer*2');

actual_samples = max_sample-min_sample+1;
channelnoise   = zeros(       channels,       1      );
samplenoise    = zeros(       channels,       samples);

actual_data    = fread (fd, actual_channels, '*single');
channelnoise(chan_array+1) = actual_data;
channelnoise = channelnoise';

actual_data    = fread (fd, actual_channels*actual_samples, '*single');
samplenoise(chan_array+1,min_sample+1:max_sample+1) = reshape(actual_data,actual_channels,[]);
samplenoise = samplenoise';

header.start = min_sample+1;
header.stop  = max_sample+1;
header.channels = zeros(channels,1);
header.channels(chan_array+1) = 1;



