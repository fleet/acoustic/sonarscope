function [image, header] = ReadS7K7048(fd)
% ReadS7K7048 - 7048 record handler function
%
% [image, header] = ReadS7K(file, 7048, index)
%
% image            - target strength data      [samples, beams]
% header           - structure describing the record:
% header.ping      - sequential ping number
% header.multiping - multiping sequence number, 0 if no multiping
% header.error     - calibration error (zero if none)
% header.index     - actual record index in the s7k file
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
min_beam = fread(fd, 1, 'integer*2');
beams = fread(fd, 1, 'integer*2');
samples = fread(fd, 1, 'integer*4');
fseek (fd, 1, 0);
header.error = fread(fd, 1, '*integer*1');
fseek (fd, 4*8, 0);

image = zeros(samples, beams+min_beam);

data = fread(fd, samples*beams, '*single');

image(:,(1:beams)+min_beam) = reshape(data,samples,beams);
