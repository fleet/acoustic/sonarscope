function [port, starboard, portindex, starboardindex, header] = ReadS7K7057(fd)
% ReadS7K7058 - 7058 record handler function
%
% DO NOT call this function directly, use
%
% [port, starboard, portindex, starboardindex, header] = ReadS7K(file, 7057, index)
%
% instead.
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
fseek (fd, 8, 0);
samples = fread(fd, 1, 'integer*4');
header.samples = samples;
fseek (fd, 4*9+2, 0);
header.error = fread(fd, 1, '*integer*1');

port           = fread(fd, samples, 'real*4');
starboard      = fread(fd, samples, 'real*4');
portindex      = fread(fd, samples, 'integer*2');
starboardindex = fread(fd, samples, 'integer*2');
