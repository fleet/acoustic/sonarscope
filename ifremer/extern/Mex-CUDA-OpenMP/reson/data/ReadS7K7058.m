function [data, image, header] = ReadS7K7058(fd)
% ReadS7K7058 - 7058 record handler function
%
% [data, header] = ReadS7K(file, 7058, index)
%
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.multiping = fread (fd, 1, '*integer*2');
beams = fread(fd, 1, 'integer*2');
header.error = fread(fd, 1, '*integer*1');
fseek (fd, 4*8, 0);
beam_array = fread(fd, beams*7, '*integer*2');
beam_number   = beam_array(1:7:end);
first_sample  = beam_array(3:7:end)*65536+beam_array(2:7:end);
bottom_detect = beam_array(5:7:end)*65536+beam_array(4:7:end);
last_sample   = beam_array(7:7:end)*65536+beam_array(6:7:end);

max_sample = max(last_sample);
max_beam   = max(beam_number);

data = cell(beams,1);
image = zeros(max_sample, max_beam);

for i=1:beams
    data{i}.beam    = beam_number(i);
    data{i}.start   = first_sample(i);
    data{i}.stop    = last_sample(i);
    data{i}.bottom  = bottom_detect(i);
    data{i}.snippet = fread(fd, double(last_sample(i)-first_sample(i)+1), '*single');
    image(data{i}.start+1:data{i}.stop+1, data{i}.beam+1) = data{i}.snippet;
end
