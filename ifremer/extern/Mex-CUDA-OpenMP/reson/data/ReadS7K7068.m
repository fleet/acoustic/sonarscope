function [magnitude, phase, header] = ReadS7K7068(fid)
%
%
%
%
header.serial_number            = fread(fid, 1, 'uint64'); % 8 bytes,
header.ping_number              = fread(fid, 1, 'uint32'); % 4 bytes,
header.beams                    = fread(fid, 1, 'uint16'); % 2 bytes,
header.BeamRows                 = fread(fid, 1, 'uint16'); % 2 bytes,
header.samples                  = fread(fid, 1, 'uint32'); % 4 bytes,
header.sample_header_identifier = fread(fid, 1, 'uint16'); % 2 bytes,
if header.beams == 64       % NEW 7068 HEADER
    data = fread(fid, (header.samples*header.beams*header.BeamRows*2), 'int16'); % read remaining part of the record. Header size is 64 bytes
elseif header.beams == 256  % OLD 7068 HEADER
    data = fread(fid, (header.samples*header.beams*2), 'int16'); % for old header with 256 beams
else
    error('Something wrong with the number of beams...')
end
pingAmp   = data(1:2:end);
pingPhase = data(2:2:end)*(pi/32768);
if header.beams == 256
    magnitude   = reshape(pingAmp,   header.beams/header.BeamRows, header.BeamRows, header.samples);
    phase = reshape(pingPhase, header.beams/header.BeamRows, header.BeamRows, header.samples);
elseif header.beams == 64
    magnitude   = reshape(pingAmp,   header.beams, header.BeamRows, header.samples);
    phase = reshape(pingPhase, header.beams, header.BeamRows, header.samples);
end

