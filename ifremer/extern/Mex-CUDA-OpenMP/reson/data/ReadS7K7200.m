function [header] = ReadS7K7200(fd)
%
%    BYTE        file_identifier[16];
%    WORD        version_number;
%    WORD        reserved;
%    BYTE        session_identifier[16];
%    DWORD       rd_size;
%    DWORD       subsystems;
%    BYTE        recording_name[64];
%    BYTE        recording_program_version[16];
%    BYTE        user_name[64];
%    BYTE        notes[128];
%
header.file_identifier = fread(fd,[1 16],'char') ;
header.version_number = fread(fd,1,'integer*2') ;
header.reserved = fread(fd,1,'integer*2') ;
header.session_identifier = fread(fd,[1 16],'char') ;
header.rd_size = fread(fd,1,'int') ;
header.subsystems = fread(fd,1,'int') ;
header.recording_name = fread(fd,[1 16],'char') ;
header.recording_program_version = fread(fd,[1 16],'char') ;
header.user_name = fread(fd,[1 64],'char') ;
header.notes = fread(fd,[1 128],'char') ;