function [header] = ReadS7K7503(fd)
% ReadS7K7503 - 7503 record handler function
%
% DO NOT call this function directly, use
%
% [header] = ReadS7K(file, 7503, index)
%
% instead.
%

fseek (fd, 8, 0);
header.ping = fread(fd, 1, '*integer*4');
header.frequency = fread(fd, 1, '*single');
header.samplerate = fread(fd, 1, '*single');
header.bandwidth = fread(fd, 1, '*single');
header.txpulse.width = fread(fd, 1, '*single');
temp = fread(fd, 1, '*integer*4');
if temp == 1
    header.txpulse.type = 'Linear chirp';
else
    header.txpulse.type = 'CW';
end
temp = fread(fd, 1, '*integer*4');
if temp == 1
    header.txpulse.envelope = 'Tukey';
else
    header.txpulse.envelope = 'Tapered rectangular';
end
header.txpulse.envelopeparameter = fread(fd, 1, '*single');
fseek (fd, 4, 0);
header.pingrate = fread(fd, 1, '*single');
header.pingperiod  = fread(fd, 1, '*single');
header.range = fread(fd, 1, '*single');
header.power = fread(fd, 1, '*single');
header.gainselection = fread(fd, 1, '*single');
fseek(fd, 8, 0);
header.txsteering.vertical = fread(fd, 1, '*single');
header.txsteering.horizontal = fread(fd, 1, '*single');
fseek(fd, 9*4, 0);
temp = fread(fd, 1, 'integer*4');
header.multiping = bitand(temp, 15728640) / 65536;
header.bottomdetect.minrange = fread(fd, 1, '*single');
header.bottomdetect.maxrange = fread(fd, 1, '*single');
header.bottomdetect.mindepth = fread(fd, 1, '*single');
header.bottomdetect.maxdepth = fread(fd, 1, '*single');
header.absorption = fread(fd, 1, '*single');
header.soundvelocity = fread(fd, 1, '*single');
header.spreading = fread(fd, 1, '*single');
fseek(fd,6*4+2,0);
temp = fread(fd, 1, '*integer*4');
if temp == 1
    header.system.pingstate = 'ON';
else
    header.system.pingstate = 'OFF';
end
fseek(fd,4,0);
temp = fread(fd, 1, '*integer*1');
if temp == 1
    header.system.txorientation = 'Up';
else
    header.system.txorientation = 'Down';
end
temp = fread(fd, 1, '*integer*2');
if temp == 1
    header.system.beammode = 'Equiangle';
else
    header.system.beammode = 'Equidistant';
end
temp = fread(fd, 1, '*integer*2');
if temp == 0
    header.system.mode = 'Manual';
elseif temp == 1
    header.system.mode = 'Autopilot';
elseif temp == 2
    header.system.mode = 'Calibration (IQ)';
else
    header.system.mode = 'Undefined';
end
header.bottomdetect.adaptivemindepth = fread(fd, 1, '*single');
header.bottomdetect.adaptivemaxdepth = fread(fd, 1, '*single');

