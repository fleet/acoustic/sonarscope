% absolute location of the s7k file
fina = '\\sbafip02\SurveyData\712820080619_151442.s7k' ;

% if needed scan the file for record information
[r,n] = ReadS7K(fina); % r: ID of the records, n: number of records

np = n(r==7008) ; % Number of pings containing full water column data

fp7008 = [] ; % Water column data
fp7004 = [] ; % Beam geometry

% then loop through the 7008 record containing the water column data and
% the corresponding beam geometry record
for ip = 1:np
    [angleV, angleH, widthY, widthX, header4, fp7004] = ReadS7K(file, 7004, 'next',fp7004) ;
    [mag,phs,mask,header8,fp7008]  = ReadS7K(fina, 7008, 'next',fp7008);
    imagesc(mag)
    pause
end

%NOTES:
% ReadS7K also allows absolute indexing but reading gets quite slow by end of file
% e.g. :
% [mag,phs,mask,header,fp]  = ReadS7K(fina, 7008, 42); read 42nd record in file
%
% Help ReadS7K70XX provides details about the output content for record
% 70XX
