fina = 'C:\SonarScopeTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\reson\data\20080301_133316.s7k' ;
[magnitude, phase, header8] = ReadS7K(fina, 7018, 1) ;
[angleV, tet, widthY, widthX, header4] = ReadS7K(fina, 7004, 1) ;
header = ReadS7K(fina,7000,1) ;

fp = fopen('C:\SonarScopeTbx\ifremer\extern\mex\Mex-CUDA-OpenMP\reson\data\pingdata.dat','wb') ;

fwrite(fp,7150,'int32') ; % m_dwDeviceId
fwrite(fp,header.ping,'int32') ; % m_dwPingNumber
fwrite(fp,length(tet),'int32') ; %m_dwReceiveBeams
fwrite(fp,size(magnitude,1),'int32') ; %dwTemp
fwrite (fp,header.soundvelocity,'float'); % m_fSoundVelocity
fwrite (fp,header.samplerate,'float'); % m_fSampleRate
fwrite (fp,header.frequency,'float'); % m_fCenterFrequency
fwrite (fp,header.rx.beamwidth,'float'); % m_fReceiveBeamWidth
fwrite (fp, header.tx.pulse.width,'float'); % m_fTransmitPulseLength
fwrite (fp, tet,'float');    % m_pBeamAngles
fwrite(fp,0.0,'float') ; % DepthMin
fwrite(fp,100000.0,'float') ; % DepthMax

for isa=1:size(magnitude,1)
    for ibe=1:size(magnitude,2)
        fwrite(fp,uint32(magnitude(isa,ibe)),'uint16') ;
        fwrite(fp,int32(phase(isa,ibe)),'int16') ;
    end
end
fclose(fp) ;
