#include "mex.h"
#include "ResonFunctions.cpp"


//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    float* AmpReduite;
    int nbSamples;
	int Ibeg, Iend, iSampleLength, index;
	double* intTmp;
    double* outputRmax1;
	float rMax1, R1Max;

    
    // Get the input parameters
    AmpReduite = (float*)mxGetPr(prhs[0]);
    nbSamples = (int)mxGetM(prhs[0]);
    if(nbSamples == 1){
        nbSamples = (int)mxGetN(prhs[0]);
    }
    intTmp=(double*)mxGetPr(prhs[1]);
    Ibeg = (int)*intTmp;
	Ibeg--;
    intTmp=(double*)mxGetPr(prhs[2]);
    Iend = (int)*intTmp;
	Iend--;
    intTmp=(double*)mxGetPr(prhs[3]);
    R1Max = (int)*intTmp;

        
    // Initializations
	iSampleLength = R1Max - Ibeg - 1;
	if (iSampleLength > nbSamples - R1Max)
	{
		index = 0;
	}
	else
	{
		index = 1;
	}
    m_pfSamplesA = (float *)mxCalloc(nbSamples, sizeof(float));
    
    // Call the C function _barycentre
    rMax1 = _amplitude_single_reduit(AmpReduite, Ibeg, Iend, 0, index);
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputRmax1 = mxGetPr(plhs[0]);
	outputRmax1[0] = rMax1;


 	// free memory
	mxFree(m_pfSamplesA); 
	m_pfSamplesA = 0;
   
}
