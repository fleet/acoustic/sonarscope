#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* dX;
    DWORD * iX;
    float * fAmp;
    int ampSize;
    double* outputDetection;
    double* outputQuality;
    double* p_dwDeviceId;
    double* p_iBeam;
    int iBeam;
    double* pSampleRate;
    double* pTxPulseWidth;
    double* pNbSamples;
    int NbSamples;
	double* dDataIn;

    // Get the input parameters
    p_iBeam = (double*)(mxGetPr(prhs[0]));
    iBeam = (int)(*p_iBeam)-1;
    
    dX=(double*)mxGetPr(prhs[1]);
    ampSize = (int)mxGetM(prhs[1]);
    if(ampSize == 1){
        ampSize = (int)mxGetN(prhs[1]);
    }
    fAmp=(float*)mxGetPr(prhs[2]);
    p_dwDeviceId = (double*)(mxGetPr(prhs[3]));
    m_dwDeviceId = (int)(*p_dwDeviceId);
    pSampleRate = (double*)(mxGetPr(prhs[4]));
    pTxPulseWidth = (double*)(mxGetPr(prhs[5]));
    pNbSamples = (double*)(mxGetPr(prhs[6]));
    NbSamples = (int)(*pNbSamples);
    dDataIn = mxGetPr(prhs[7]);
	m_IdentAlgo = (int)(*dDataIn);
    dDataIn = mxGetPr(prhs[8]);
	m_R0 = (float)(*dDataIn);
            
    // Initializations
    m_pfdSamples = (double *)mxCalloc(ampSize, sizeof(double));
    iX = (DWORD *)mxCalloc(ampSize, sizeof(DWORD));
    for(int i=0; i<ampSize;i++)
    {
        iX[i] = (float)(dX[i]-1);
    }
    
    m_pAmpWinMin = (DWORD *)mxCalloc(1000, sizeof(DWORD));
    m_pAmpWinMax = (DWORD *)mxCalloc(1000, sizeof(DWORD));
    m_pRangeAmplitude = (float *)mxCalloc(1000, sizeof(float));
    m_pQualityAmplitude = (float *)mxCalloc(1000, sizeof(float));
	m_N0 = (*pSampleRate) * (*pTxPulseWidth);
	m_N0Corr = m_N0 * sqrt(2.0)/2;
    
    // Call the C function _barycentre
    _amplitude_single(iBeam,iX,fAmp,ampSize,NbSamples);
    
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
	if(m_pRangeAmplitude[iBeam] > 0)
	{
		outputDetection[0] = m_pRangeAmplitude[iBeam]+1;  
	}
	else
	{
		outputDetection[0] = -1;  
	}
    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputQuality = mxGetPr(plhs[1]);
	if(m_pQualityAmplitude[iBeam] > 0)
	{
		outputQuality[0] = m_pQualityAmplitude[iBeam];    
	}
	else
	{
		outputQuality[0] = sqrt((double)(-1.0));    
	}


	// free memory
	mxFree(m_pfdSamples); 
	m_pfdSamples = 0;
	mxFree(iX); 
	iX = 0;
	mxFree(iX); 
	iX = 0;
    mxFree(m_pAmpWinMin);
	m_pAmpWinMin = 0;
    mxFree(m_pAmpWinMax);
	m_pAmpWinMax = 0;
    mxFree(m_pRangeAmplitude);
	m_pRangeAmplitude = 0;
    mxFree(m_pQualityAmplitude);
	m_pQualityAmplitude = 0;

}
