#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double *Dsample_start;
    WORD sample_start;
    double* DSeuilAmp;
    float SeuilAmp;
    double *EndSample;
    double *Rmin;
    double *Rmax;
    float * fAmpReduite;
    double* outputDetection;
	unsigned int nan;
	WORD R0;

    // Get the input parameters
    fAmpReduite = (float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);    
	m_ReduiteSize = mxGetM(prhs[0]);
	m_pAmpReduite = _2DArrayAlloc(m_dwReceiveBeams, m_ReduiteSize,(float)0.0f, true);
    for (int iB=0; iB<m_dwReceiveBeams; iB++){
        for (int iS=0; iS < m_ReduiteSize; iS++) {
			if(fAmpReduite[(iB*m_ReduiteSize) +iS] == fAmpReduite[(iB*m_ReduiteSize) +iS])
			{
				// Not NAN
				m_pAmpReduite[iB][iS] = fAmpReduite[(iB*m_ReduiteSize) +iS];
			}
			else
			{
				// NAN
				NANf(m_pAmpReduite[iB][iS]);
			}
        }
    }
    Dsample_start = mxGetPr(prhs[1]);
    sample_start = ((WORD)(*Dsample_start) - 1);
    DSeuilAmp = mxGetPr(prhs[2]);
    SeuilAmp = (float)(*DSeuilAmp);
    
    // Initializations
    m_nStrips = 81;

    m_pFlagBeams = (bool *)mxCalloc(m_dwReceiveBeams, sizeof(bool));
    m_pfSamplesA = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesB = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesC = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesD = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
    m_pfSamplesE = (float *)mxCalloc(m_ReduiteSize, sizeof(float));
        

    // Call the C function _strip_estimation
	if(sample_start <m_ReduiteSize)
	{
		R0 = _strip_estimation(0,m_dwReceiveBeams-1,sample_start,m_ReduiteSize-1, SeuilAmp);
	}
	else
	{
		R0 = 0;  // The sample_start is out of redced matrix size
	}

    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
	if(R0 != 0)
	{
		outputDetection[0] = (double)(R0+1);
	}
	else
	{
		outputDetection[0] = sqrt((double)(-1.0));
	}

    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
    outputDetection[0] = (double)(m_pFlagBeams[0]);


	// free memory
	delete(m_pAmpReduite);
	m_pAmpReduite = 0;
    mxFree(m_pFlagBeams);
	m_pFlagBeams = 0;
    mxFree(m_pfSamplesA);
	m_pfSamplesA = 0;
    mxFree(m_pfSamplesB);
	m_pfSamplesB = 0;
    mxFree(m_pfSamplesC);
	m_pfSamplesC = 0;
    mxFree(m_pfSamplesD);
	m_pfSamplesD = 0;
    mxFree(m_pfSamplesE);
	m_pfSamplesE = 0;

}
