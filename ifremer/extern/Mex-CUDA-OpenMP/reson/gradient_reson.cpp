#include "mex.h"
#include "ResonFunctions.cpp"


//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    float* fX;
    double* dX;
    float* fG;
    float* Amp;
    int nbSamples;
    bool result;
    double* outputResult;
    bool l_1xN = true;
    
    // Get the input parameters
    dX=(double*)mxGetPr(prhs[0]);
    nbSamples = (int)mxGetM(prhs[0]);
    if(nbSamples == 1){
        l_1xN = false;
        nbSamples = (int)mxGetN(prhs[0]);
    }
        
    // Initializations
    fX = (float *)mxCalloc(nbSamples, sizeof(float));
    fG = (float *)mxCalloc(nbSamples, sizeof(float));
    for(int i=0; i<nbSamples;i++)
    {
        fX[i] = (float)(dX[i]);
    }
    
    // Call the C function _barycentre
    _gradient(fX, fG, nbSamples);
        
    // Set the output parameters
    if(l_1xN == false){
        plhs[0] = mxCreateDoubleMatrix(1,nbSamples,mxREAL);
    }
    else
    {
        plhs[0] = mxCreateDoubleMatrix(nbSamples,1,mxREAL);
    }
    outputResult = mxGetPr(plhs[0]);
    for(int i=0; i<nbSamples; i++){
        outputResult[i] = (float)(fG[i]);
    }

	// free memory
	mxFree(fX); 
	fX = 0;
	mxFree(fG); 
	fG = 0;

    
}
