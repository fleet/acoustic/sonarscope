#include "mex.h"
#include "ResonFunctions.cpp"



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    float * fAmpReduite;
    double* outputDetection;
    
    // Get the input parameters
    fAmpReduite = (float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);    
	m_ReduiteSize = mxGetM(prhs[0]);
	m_pAmpReduite = _2DArrayAlloc(m_dwReceiveBeams, m_ReduiteSize,(float)0.0f, true);
    for (int iB=0; iB<m_dwReceiveBeams; iB++){
        for (int iS=0; iS < m_ReduiteSize; iS++) {
            m_pAmpReduite[iB][iS] = fAmpReduite[(iB*m_ReduiteSize) +iS];
        }
    }        
    // Initializations
    m_pfBeamsA = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    
    // Call the C function _barycentre
    _matrix_compensation();
        
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(m_ReduiteSize, m_dwReceiveBeams, mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    for (int iB=0; iB<m_dwReceiveBeams; iB++){
        for (int iS=0; iS < m_ReduiteSize; iS++) {
            outputDetection[(iB*m_ReduiteSize) +iS] = (double)(m_pAmpReduite[iB][iS]);
        }
    }    

	// free memory
	delete(m_pAmpReduite);
	m_pAmpReduite = 0;
	mxFree(m_pfBeamsA); 
	m_pfBeamsA = 0;

    
}
