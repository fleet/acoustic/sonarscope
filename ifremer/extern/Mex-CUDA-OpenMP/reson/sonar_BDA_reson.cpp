#include "mex.h"
#include "ResonFunctions.cpp"












//------------------------------------------------------------------------------
// data storage in global variable (attributs of class CIFremerBottomDetect)
//------------------------------------------------------------------------------
void DataLoad ( int nlhs, int nrhs, const mxArray *prhs[] )
{
	int iB;
	DWORD dwTemp;
	double* dDataIn;
	float* fDataIn;
	float DepthMin, DepthMax;
	_raw_data_type * data;

	// read data
    fDataIn=(float*)mxGetPr(prhs[0]);
    m_dwReceiveBeams = mxGetN(prhs[0]);
    m_iOutputSamples = mxGetM(prhs[0]);
    data = (_raw_data_type*)mxCalloc(m_dwReceiveBeams*m_iOutputSamples, sizeof(_raw_data_type)); 
	m_pRAWData = (char*)data;
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
			data->amp = (unsigned short)(fDataIn[(iB*m_iOutputSamples)+iS]);
        }
    }

    fDataIn=(float*)mxGetPr(prhs[1]);
	data = (_raw_data_type*)m_pRAWData;
    for (int iS=0; iS < m_iOutputSamples; iS++) {
        for (int iB=0; iB<m_dwReceiveBeams; iB++,data++){
			data->phs = (short)(fDataIn[(iB*m_iOutputSamples)+iS]/5.4931640625e-003f);
        }
    }

	dDataIn = mxGetPr(prhs[2]);
	DepthMin = (float)(*dDataIn);
	//DepthMin = 0.0;

	dDataIn = mxGetPr(prhs[3]);
	DepthMax = (float)(*dDataIn);
	//DepthMax = 12000.0;

	dDataIn = mxGetPr(prhs[4]);
	m_pBeamAngles = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	for(int i=0;i<m_dwReceiveBeams;i++){
		m_pBeamAngles[i] = (float)(dDataIn[i]*PIf/180);
	}

	dDataIn = mxGetPr(prhs[5]);
	m_fCenterFrequency = (float)(*dDataIn);

	dDataIn = mxGetPr(prhs[6]);
	m_fSampleRate = (float)(*dDataIn);

	dDataIn = mxGetPr(prhs[7]);
	m_fTransmitPulseLength = (float)(*dDataIn);

	dDataIn = mxGetPr(prhs[8]);
	m_fReceiveBeamWidth = (float)(*dDataIn);

	dDataIn = mxGetPr(prhs[9]);
	m_fSoundVelocity = (float)(*dDataIn);

    dDataIn = mxGetPr(prhs[10]);
	m_dwDeviceId = (DWORD)(*dDataIn);

    dDataIn = mxGetPr(prhs[11]);
	// !ALTRAN - 41 Begin
	m_IdentAlgo = (int)(*dDataIn);
	// !ALTRAN End


	// Allocate datas
    m_pGatesMin = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pGatesMax = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pMaskMin = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	m_pMaskMax = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
	m_pMaskWidth = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pFlagBeams = (bool *)mxCalloc(m_dwReceiveBeams, sizeof(bool));
    m_pAmpReduite = _2DArrayAlloc(m_dwReceiveBeams, m_iOutputSamples, (float)0.0f, true);
    m_pfReduite = _2DArrayAlloc(m_dwReceiveBeams, m_iOutputSamples, (float)0.0f, true);
    m_pR1 = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pR2 = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pRangeAmplitude = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pSamplesAmp       = new DWORD[m_dwReceiveBeams];
    m_pQualityAmplitude = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pQF2Amplitude = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pRangePhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pQualityPhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pSamplesPhase = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pERMPhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pSlopePhase = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pDetectionRange = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pDetectionQuality = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pMax = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
	m_pDetectionType = (char *)mxCalloc(m_dwReceiveBeams, sizeof(char));
    m_pAmpWinMin = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pAmpWinMax = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pPhsWinMin = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pPhsWinMax = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsA = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsB = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsC = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pdBeamsD = (DWORD *)mxCalloc(m_dwReceiveBeams, sizeof(DWORD));
    m_pfBeamsA = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsB = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsC = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pfBeamsD = (float *)mxCalloc(m_dwReceiveBeams, sizeof(float));
    m_pNominalBeams = (bool *)mxCalloc(m_dwReceiveBeams, sizeof(bool));

	if (m_dwDeviceId == 7111)
		_nominal_beams_7111();

    m_pfSamplesA = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesB = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesC = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesD = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesE = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesF = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesG = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfSamplesH = (float *)mxCalloc(m_iOutputSamples, sizeof(float));
    m_pfdSamples = (double *)mxCalloc(m_iOutputSamples, sizeof(double));
	m_pdSamplesA = (DWORD *)mxCalloc(m_iOutputSamples, sizeof(DWORD));
	m_pdSamplesB = (DWORD *)mxCalloc(m_iOutputSamples, sizeof(DWORD));
	m_pMatrix = _2DArrayAlloc(m_iOutputSamples,m_dwReceiveBeams, (float)0.0f, true);

	if (m_iOutputSamples < REDUCTION_MATRIX_SIZE)
		m_nAllocSamples = REDUCTION_MATRIX_SIZE;
	else
		m_nAllocSamples = m_iOutputSamples;

	memset(m_pMatrix[0], 0xff, m_nAllocSamples*m_dwReceiveBeams*sizeof(float));


	m_NechRes0 = 4 * tanf(m_pBeamAngles[m_dwReceiveBeams-1]) / m_dwReceiveBeams;
	// !ALTRAN - 42 Begin
	m_dTPulse  = m_fTransmitPulseLength * m_fSampleRate / sqrtf(12.0f);
	// !ALTRAN End
	m_N0	   = m_fTransmitPulseLength * m_fSampleRate;
	m_N0Corr   = m_fTransmitPulseLength * m_fSampleRate * sqrtf(2.0f) / 2.0f;
	m_B		   = 1.04 * sqrt((4.0/PI-1.0) / 12.0);


	switch (m_dwDeviceId)
	{
	case 7111:
		m_fAbsorption = 35.0f;
		m_DeltaTeta = D2Rf(1.8f);
		// !ALTRAN - 43 Begin
		m_nStrips = m_dwReceiveBeams / 8;
		// !ALTRAN End
		break;

	case 7150:
		if (m_fCenterFrequency < 18000)			// 12 kHz
		{
			m_fAbsorption = 1.1f;
            m_DeltaTeta = D2Rf(1.0f);
		}
		else									// 24 kHz
		{
			m_fAbsorption = 4.0f;
            m_DeltaTeta = D2Rf(0.5f);
		}
		// !ALTRAN - 43 Begin
		m_nStrips = m_dwReceiveBeams / 14;
		// !ALTRAN End
		break;
	}


//	m_dwPingNumber = 1;

	for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
	{
		m_pDetectionRange[iB]   = 0.0f;
		m_pDetectionQuality[iB] = 0.0f;
		m_pDetectionType[iB]	= IF1_QUALITY_QF2_PASS;
	}

	for (iB=0; iB<(int)m_dwReceiveBeams; iB++)
	{
		m_pGatesMin[iB] = 0;
		m_pGatesMax[iB] = 0xffffffff;
		m_pMaskMin[iB]  = 0;
		m_pMaskMax[iB]  = 0xffffffff;
	}

	if (DepthMin > 1.0f || DepthMax > 1.0f)
	{
		SetDepthGates(DepthMin, DepthMax, 0.0f);
	}

}



//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    double* outputDetection;

    // read and initialize input data
	DataLoad(nlhs, nrhs, prhs);


    // compute detection
	CalculateMask();

	BottomDetection();


    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[0]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pDetectionRange[i] == 0)
		{
			outputDetection[i] = sqrt((double)(-1.0));
		}
		else
		{
			outputDetection[i] = m_pDetectionRange[i];
		}
	}
    plhs[1] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[1]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pDetectionType[i] == m_pDetectionType[i])
		{
			outputDetection[i] = floorf(m_pDetectionType[i]/4);
		}
		else
		{
			outputDetection[i] = sqrt((double)(-1.0));
		}
	}

    plhs[2] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[2]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pDetectionQuality[i] == 0)
		{
			outputDetection[i] = sqrt((double)(-1.0)); 
		}
		else
		{
			outputDetection[i] = m_pDetectionQuality[i]; 
		}
	}

    plhs[3] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[3]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pRangeAmplitude[i] == m_pRangeAmplitude[i])
		{
			outputDetection[i] = m_pRangeAmplitude[i]; 
		}
		else
		{
			outputDetection[i] = sqrt((double)(-1.0)); 
		}
	}

    plhs[4] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[4]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pRangePhase[i] == m_pRangePhase[i])
		{
			outputDetection[i] = m_pRangePhase[i]+1; 
		}
		else
		{
			outputDetection[i] = sqrt((double)(-1.0)); 
		}
	}

    plhs[5] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[5]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pQualityAmplitude[i] == m_pQualityAmplitude[i])
		{
			outputDetection[i] = m_pQualityAmplitude[i]; 
		}
		else
		{
			outputDetection[i] = sqrt((double)(-1.0)); 
		}
	}

    plhs[6] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[6]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		if(m_pQualityPhase[i] == m_pQualityPhase[i])
		{
			outputDetection[i] = m_pQualityPhase[i]; 
		}
		else
		{
			outputDetection[i] = sqrt((double)(-1.0)); 
		}
	}

    plhs[7] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[7]);
	outputDetection[0] = m_fSampleRate;

    plhs[8] = mxCreateDoubleMatrix(1,m_dwReceiveBeams,mxREAL);
    outputDetection = mxGetPr(plhs[8]);
    for(int i=0; i<m_dwReceiveBeams;i++)
    {
		outputDetection[i] = m_pBeamAngles[i]/PIf*180; 
	}

    plhs[9] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[9]);
	outputDetection[0] = m_iBeamBeg+1;

    plhs[10] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[10]);
	outputDetection[0] = m_iBeamEnd+1;

	
    plhs[11] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputDetection = mxGetPr(plhs[11]);
	outputDetection[0] = m_dwDeviceId;

	cleanData();

}
