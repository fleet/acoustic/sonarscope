#include "mex.h"
#include "ResonFunctions.cpp"





//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    DWORD* iX;
    double* iXF;
	double * dIn;
    float* Amp;
	float *fIn;
    int nbSamples;
    DWORD result;
    double* outputResult;
	float R0, lineWidth, iBeamRange, AmBeamRange;
	int subAmpIndex, subAmpsize;

    // Get the input parameters
    iXF=(double*)mxGetPr(prhs[0]);
    Amp=(float*)mxGetPr(prhs[1]);
    nbSamples = (int)mxGetM(prhs[1]);
    if(nbSamples == 1){
        nbSamples = (int)mxGetN(prhs[1]);
    }

	dIn = (double*)mxGetPr(prhs[2]);
	iBeamRange = (float)(*dIn);
	dIn = (double*)mxGetPr(prhs[3]);
	R0 = (float)(*dIn);
	fIn = (float*)(mxGetPr(prhs[4]));
	AmBeamRange = *fIn;
	dIn = (double*)mxGetPr(prhs[5]);
	lineWidth = (float)(*dIn);
        
    // Initializations
    iX = (DWORD *)mxCalloc(nbSamples, sizeof(DWORD));
    for(int i=0; i<nbSamples;i++)
    {
        iX[i] = (int)(iXF[i]);
    }
    
    // Call the C function _barycentre
    result = _amplitude_test_double_shape(iX, Amp, nbSamples, R0, AmBeamRange, lineWidth, &subAmpIndex, &subAmpsize);
        
    // Set the output parameters
	if(result == true)
	{
		plhs[0] = mxCreateDoubleMatrix(1,subAmpsize,mxREAL);
		outputResult = mxGetPr(plhs[0]);
		for(int i=0;i<subAmpsize;i++){
			outputResult[i] = subAmpIndex+i+1;
		}
	}
	else
	{
		plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
	}

	// free memory
	mxFree(iX); 
	iX = 0;

    
}
