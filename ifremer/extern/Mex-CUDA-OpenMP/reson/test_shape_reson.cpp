#include "mex.h"
#include "ResonFunctions.cpp"





//------------------------------------------------------------------------------
// standard mexFunction
//------------------------------------------------------------------------------

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) {
    
    DWORD* iX;
    double* iXF;
    float* Amp;
    int nbSamples;
    bool result;
    double* outputResult;
    
    // Get the input parameters
    Amp=(float*)mxGetPr(prhs[0]);
    iXF=(double*)mxGetPr(prhs[1]);
    nbSamples = (int)mxGetM(prhs[1]);
    if(nbSamples == 1){
        nbSamples = (int)mxGetN(prhs[1]);
    }
        
    // Initializations
    iX = (DWORD *)mxCalloc(nbSamples, sizeof(DWORD));
    for(int i=0; i<nbSamples;i++)
    {
        iX[i] = (int)(iXF[i]);
    }
    
    // Call the C function _barycentre
    result = _amplitude_test_shape(iX, Amp, nbSamples);
        
    // Set the output parameters
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    outputResult = mxGetPr(plhs[0]);
    outputResult[0] = result;    

	// free memory
	mxFree(iX);
	iX = 0;

    
}
