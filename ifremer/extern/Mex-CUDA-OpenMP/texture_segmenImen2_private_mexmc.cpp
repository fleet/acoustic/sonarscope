/*
 * [segmlocal Distlocal Dist12local] = texture_segmenImen2_private_mexmc(unit8(ImageL), dX, dY, NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), indAngleImagetteL, Filtre, nbCores)
 * Cmat et CmatRef sont des matrices carr�es de type single
 */


// version avec filtrage des Cmats (Filtre = 0 en argument d�sactive le filtrage).

#include "mex.h"
#include <math.h>
#include <vector>
#include <omp.h>
#include <float.h>
#include <algorithm>
#include <limits.h>

using namespace std;

// variables gloables :

int paspxthread, nbCores;
int Tx, Ty, nbNiveaux, M, N, taille_fenetre;
unsigned char *ImageL;
unsigned char *segm;
float *Dist, *indAngleImagetteL, *Dist12;
float SeuilRejet, SeuilConfusion;


struct stCmatModeleAngle
{
    float*CmatRef;
    unsigned char size;
};




void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    const float __LOG2F = logf(float(2));
    /******************************************************************************************
    RECUPERATION DES ARGUMENTS
     ******************************************************************************************/
    
    mxArray *Celltmp1, *Celltmp2;
    
    if(nrhs != 11)
        mexErrMsgTxt("texture_segmenImen2_private_mexmc requiert 11 arguments");
    
    if(nlhs != 3)
        mexErrMsgTxt("texture_segmenImen2_private_mexmc requiert 3 arguments en sortie");
    
    M = mxGetM(prhs[0]); // dimensions de la matrice image
    N = mxGetN(prhs[0]);
    
    ImageL     = (unsigned char*) mxGetData(prhs[0]);
    
    Tx         = (int)mxGetScalar(prhs[1]);
    Ty         = (int)mxGetScalar(prhs[2]);
    nbNiveaux  = (int)mxGetScalar(prhs[3]);
    
    SeuilRejet         = (float)mxGetScalar(prhs[5]);
    SeuilConfusion     = (float)mxGetScalar(prhs[6]);
    taille_fenetre     = (int)mxGetScalar(prhs[7]);
    indAngleImagetteL  = (float*) mxGetData(prhs[8]);
    
    int flagAngle;
    if((mxGetM(prhs[8]) * mxGetN(prhs[8])) == 1)
        flagAngle = 0;
    else
        flagAngle = 1;
    
    int Filtre = (int)mxGetScalar(prhs[9]);
    nbCores    = (int)mxGetScalar(prhs[10]);
    
    // CmatModeleAngle :
    int nbCmatRef = mxGetNumberOfElements(prhs[4]);
    int nbCmatRefSecteurAngle;
    mwIndex subs[] = {0, 0};
    mwIndex index;
    
    if(flagAngle)
    {
        subs[1]     = 1;
        index       = mxCalcSingleSubscript(prhs[4], 2, subs);
        Celltmp1    = mxGetCell(prhs[4], index);
        nbCmatRefSecteurAngle = (int)mxGetNumberOfElements(Celltmp1);
    }
    else
    {
        nbCmatRefSecteurAngle = 1;
    }
    
    std::vector<stCmatModeleAngle> CmatModeleAngle(nbCmatRef * nbCmatRefSecteurAngle);
    
    if(flagAngle)
    {
        for(int i = 0; i < nbCmatRef; i++)
        {
            subs[1]     = i;
            index       = mxCalcSingleSubscript(prhs[4], 2, subs);
            Celltmp1    = mxGetCell(prhs[4], index);
            for(int j = 0; j < nbCmatRefSecteurAngle; j++)
            {
                subs[1]     = j;
                index       = mxCalcSingleSubscript(Celltmp1, 2, subs);
                Celltmp2    = mxGetCell(Celltmp1, index);
                CmatModeleAngle[j + i * nbCmatRefSecteurAngle].CmatRef  = (float*)mxGetData(Celltmp2);
                CmatModeleAngle[j + i * nbCmatRefSecteurAngle].size     = mxGetM(Celltmp2);
            }
        }
    }
    else
    {
        for(int i = 0; i < nbCmatRef; i++)
        {
            subs[1]     = i;
            index       = mxCalcSingleSubscript(prhs[4], 2, subs);
            Celltmp1    = mxGetCell(prhs[4], index);
            subs[1]     = 0;
            index       = mxCalcSingleSubscript(Celltmp1, 2, subs);
            Celltmp2    = mxGetCell(Celltmp1, index);
            CmatModeleAngle[i].CmatRef = (float*)mxGetData(Celltmp2);
            //CmatModeleAngle[i].size = 1; // on sen fout
        }
    }
    
    // sorties :
    mwSize dims[1];
    dims[0]   = N;
    plhs[0]   = mxCreateNumericArray(1, dims, mxUINT8_CLASS, mxREAL);
    segm      = (unsigned char*)mxGetPr(plhs[0]);
    plhs[1]   = mxCreateNumericArray(1, dims, mxSINGLE_CLASS, mxREAL);
    Dist      = (float*)mxGetPr(plhs[1]);
    plhs[2]   = mxCreateNumericArray(1, dims, mxSINGLE_CLASS, mxREAL);
    Dist12    = (float*)mxGetPr(plhs[2]);
    
    
    
    /******************************************************************************************
    TRAITEMENT MULTICORE
     ******************************************************************************************/
    
    int cpu_id;
    #pragma omp parallel private(cpu_id)
    {
        cpu_id = omp_get_thread_num();
        nbCores = omp_get_num_threads();
        
        if(cpu_id == 0) // seulement un CPU
        {
            if(omp_get_num_threads() != nbCores)
            {
                printf("OpenMP est configur� pour utiliser %d CPU(s)\n", omp_get_num_threads());
                printf("%d CPU(s) sont install�s sur cet ordinateur\n", omp_get_max_threads());
                mexErrMsgTxt("texture_segmenImen2_private_mexmc refuse de continuer");
            }
        }
        
        
        float iAngle = 0;
        float *Cmat, *CmatF;
        Cmat = (float*)malloc(sizeof(float) * nbNiveaux * nbNiveaux);
        
        if(Filtre)
        {
            CmatF = (float*)malloc(sizeof(float) * nbNiveaux * nbNiveaux);
        }
        
        float *dist,
              *d;
        unsigned char *ordre;
        
        dist    = (float*)malloc(sizeof(float) * nbCmatRef);
        d       = (float*)malloc(sizeof(float) * nbCmatRef);
        ordre   = (unsigned char*)malloc(sizeof(unsigned char) * nbCmatRef);
        
        
        int stop    = (cpu_id + 1) * paspxthread;
        
        if (cpu_id == nbCores - 1) 
            stop = N;
        for(int ic = cpu_id * paspxthread; ic < stop; ic++) // boucle principale
        {
            // si NaN, on passe au pixel suivant
            int coord = (taille_fenetre - 1) + ic * M;
            if (coord < 0)
                coord = 0;
            if (flagAngle)
                iAngle = indAngleImagetteL[ic];
            
            if ((ImageL[coord] == 0) || mxIsNaN(iAngle))  // en uint8, les NaN sont des z�ros
            {
                Dist[ic] = 0;
                segm[ic] = 0;
                continue;
            }
            
            if (flagAngle)
                iAngle--;
            
            // d�coupage en imagettes :
            //en matlab :
            //        subc = (ic-W(2)+1):(ic+W(2));
            //		subc(subc < 1) = [];
            //		subc(subc > nbC) = [];
            //		Imagette = ImageL(:, subc);
            
            int N_imagette = taille_fenetre * 2;
            int min = ic - taille_fenetre + 1;
            if (min < 0)
            {
                N_imagette = ic + taille_fenetre;
                min = 0;
            }
            if (ic + taille_fenetre > N - 1)
                N_imagette = N - 1 - ic + taille_fenetre;
            
            unsigned char*Image; // imagette
            Image = (unsigned char*)malloc(sizeof(unsigned char) * M * N_imagette);
            for (int m = 0; m < M; m++)
                for (int n = 0; n < N_imagette ; n++)
                    Image[m + n * M] = ImageL[m + (n + min) * M];
            
            // Calcul de la matrice de cooccurrence de l'imagette
            // mise � zero de Cmat
            
            for (int n = 0; n < nbNiveaux * nbNiveaux; n++)
                Cmat[n] = 0;
            
            if (Filtre)
                for (int n = 0; n < nbNiveaux * nbNiveaux; n++)
                    CmatF[n] = 0;
            
            
            // calcul de nbtr et moyenne:
            float moyenne = 0;
            int nbtr = 0;
            for (int i = 0; i < M - Ty; i++)
                for(int j = 0; j < N_imagette - Tx; j++)
                    if((Image[i + j * M] != 0) && (Image[i + Ty + (j + Tx) * M] != 0))
                    {
                        nbtr++;
                        moyenne += Image[i + j * M];
                    }
            
            if(nbtr != 0) // sinon Cmat reste a zero
            { // calcul de Cmat :
                float inc = 1 / (float)nbtr;
                for(int i = 0; i < M - Ty; i++)
                    for(int j = 0; j < N_imagette - Tx; j++)
                        if((Image[i + j * M] > 0) && (Image[i + Ty + (j + Tx) * M] > 0)) 
                            Cmat[Image[i + j * M] - 1 + (Image[i + Ty + (j + Tx) * M] - 1) * nbNiveaux] += inc;
                
                if(Filtre)
                {
                    // calcul de la largeur du filtre h
                    moyenne /= nbtr;
                    float s = 0;
                    for(int ii = 0; ii < M - Ty; ii++)
                        for(int jj = 0; jj < N_imagette - Tx; jj++)
                            if((Image[ii + jj * M] != 0) && (Image[ii + Ty + (jj + Tx) * M] != 0))
                                s += (Image[ii + jj * M] - moyenne) * (Image[ii + jj * M] - moyenne);
                    
                    s /= nbtr;
                    s = sqrtf(s);
                    float truc = 0.5 * logf(227850 / (float)nbtr) * sqrtf(s / (float)14.35) / logf(float(2));
                    float sigma;
                    if (truc > 0.1)
                        sigma = truc;
                    else 
                        sigma = 0.1;
                    int largeurFiltre = 1 + 2 * (int)(2 * sigma);
                    
                    // filtre gaussien 2D h :
                    float*h;
                    h = (float*)malloc(sizeof(float) * largeurFiltre * largeurFiltre);
                    float sumh = 0;
                    for (int x = 0; x < largeurFiltre; x++)
                    {
                        for (int y = 0; y < largeurFiltre; y++)
                        {
                            h[x + y * largeurFiltre] = expf(-((x+1 - (float)(largeurFiltre + 1) / 2) * (x+1 - (float)(largeurFiltre + 1) / 2) + (y+1 - (float)(largeurFiltre + 1) / 2) * (y+1 - (float)(largeurFiltre + 1) / 2)) / (float)(2 * sigma * sigma));
                            sumh += h[x + y * largeurFiltre];
                        }
                    }
                    for(int x = 0; x < largeurFiltre; x++)
                    {
                        for(int y = 0; y < largeurFiltre; y++)
                        {
                            h[x + y * largeurFiltre] /= sumh;
                        }
                    }
                    
                    // produit de convolution 2D : CmatF = Cmat * h
                    
                    int kernelradius = (int)(2*sigma);
                    int dx, dy; float sommediv; int kx, ky, x, y;
                    for(x = kernelradius; x < nbNiveaux - kernelradius; x++)
                    {
                        for(y = kernelradius; y < nbNiveaux - kernelradius; y++)
                        {
                            if(Cmat[x + y * nbNiveaux] != 0)
                            {
                                sommediv = 0;
                                for( kx = -kernelradius; kx <= kernelradius; kx++)
                                {
                                    for( ky = -kernelradius; ky <= kernelradius; ky++)
                                    {
                                        dx = x + kx;
                                        dy = y + ky;
                                        CmatF[dx + dy * nbNiveaux] += Cmat[x + y * nbNiveaux] * h[kernelradius + kx + (kernelradius + ky) * largeurFiltre];
                                        sommediv += h[kernelradius + kx + (kernelradius + ky) * largeurFiltre];
                                    }
                                }
                                CmatF[x + y * nbNiveaux] /= sommediv;
                            }
                        }
                    }
                    free(h);
                } // fin  if(Filtre)
            } // fin if(nbtr != 0)
            
            
            
            // calcul de la distance de kullback par rapport aux matrices de reference :
            for(int n = 0; n < nbCmatRef; n++)
            {
                dist[n] = 0;
                if(flagAngle)
                {
                    if (CmatModeleAngle[(unsigned char)iAngle + n * nbCmatRefSecteurAngle].size == 1)
                    {
                        dist[n] = (float)INT_MAX;
                    }
                    else
                    {
                        if(Filtre)
                        {
                            for(int i = 0; i < nbNiveaux * nbNiveaux; i++)
                                if((CmatF[i] > 0))// && (CmatModeleAngle[iAngle + n * nbCmatRefSecteurAngle].CmatRef[i] > 0))
                                    dist[n] += (CmatF[i] + 0.00001) * logf((CmatF[i] + 0.00001) / (CmatModeleAngle[iAngle + n * nbCmatRefSecteurAngle].CmatRef[i] + 0.00001)) / __LOG2F;
                        }
                        else
                        {
                            for(int i = 0; i < nbNiveaux * nbNiveaux; i++)
                                if((Cmat[i] > 0))// && (CmatModeleAngle[iAngle + n * nbCmatRefSecteurAngle].CmatRef[i] > 0))
                                    dist[n] += (Cmat[i] + 0.00001) * logf((Cmat[i] + 0.00001) / (CmatModeleAngle[iAngle + n * nbCmatRefSecteurAngle].CmatRef[i] + 0.00001)) / __LOG2F;
                        }
                    }
                }
                else
                {
                    if(Filtre)
                    {
                        for(int i = 0; i < nbNiveaux * nbNiveaux; i++)
                            if((CmatF[i] > 0))// && (CmatModeleAngle[n].CmatRef[i] > 0))
                                dist[n] += (CmatF[i] + 0.00001) * logf((CmatF[i] + 0.00001) / (CmatModeleAngle[n].CmatRef[i] + 0.00001)) / __LOG2F;
                        //dist[n] += (CmatF[i]) * logf((CmatF[i]) / (CmatModeleAngle[n].CmatRef[i])) / __LOG2F;
                    }
                    else
                    {
                        for(int i = 0; i < nbNiveaux * nbNiveaux; i++)
                            if((Cmat[i] > 0))// && (CmatModeleAngle[n].CmatRef[i] > 0))
                                dist[n] += (Cmat[i] + 0.00001) * logf((Cmat[i] + 0.00001) / (CmatModeleAngle[n].CmatRef[i] + 0.00001)) / __LOG2F;
                    }
                }
            }
            
            // [d, ordre] = sort(dist(:))
            for (int n = 0; n < nbCmatRef; n++)
                d[n] = dist[n];
            
            std::sort(d, d + nbCmatRef);
            for (int n = 0; n < nbCmatRef; n++)
                for (int m = 0; m < nbCmatRef; m++)
                    if(d[n] == dist[m])
                    {
                        ordre[n] = m + 1;
                        break;
                    }
            
            
            float separabilite = abs(d[0] - d[1]);
            
            if(d[0] == 0)
            {
                free(Image);
                continue;
            }
            else
            {
                if(d[0] > SeuilRejet)
                {
                    segm[ic] = nbCmatRef + 2;
                }
                else
                {
                    if(separabilite < SeuilConfusion)
                    {
                        segm[ic] = nbCmatRef + 1;
                    }
                    else
                    {
                        segm[ic] = ordre[0];
                    }
                }
            }
            Dist[ic] = d[0];
            Dist12[ic] = separabilite;
            
            // lib�ration de la m�moire locale :
            free(Image);
            
        } // fin for ic = ...
        //printf("%d : jai fini\n", cpu_id);
        free(Cmat);
        if(Filtre) //GLU le 23/02/2012 : correction � l'identique du Pb d�tect� sur TextureImen2
            free(CmatF);
        free(dist);
        free(d);
        free(ordre);
        
    }
    //if(flagAngle) free(indAngleImagetteL);
    //free(ImageL);
}



