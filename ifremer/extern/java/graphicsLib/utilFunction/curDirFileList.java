package utilFunction;

import java.io.File;

// Classe cr��e pour lister tous les fichiers d'un r�pertoire courant
// r�pondant � un filtre.
// Impossiblit� de filtrer directement par les fonctions basiques
// (curDir.listFiles ???? : pb de prototype du fileFilterChooser2).

//Cf : uicomponent.m

public class curDirFileList {

	static public File[] listFiles(File directory, filtreFileChooser2 filtre)
	{
		return directory.listFiles(filtre);
	}
}
