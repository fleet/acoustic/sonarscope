package utilFunction;

import javax.swing.*;
import java.beans.*;
import java.io.*;
//import java.io.IOException;

/* FileSelectedName.java by FileChooserDemo2.java. */
public class fileSelectedName extends JComponent
                          		implements PropertyChangeListener {
 	public File file = null;

    public fileSelectedName(JFileChooser fc) {
        fc.addPropertyChangeListener(this);
    }

    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();

        //If the directory changed, don't show an image.
        if (JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(prop)) {
            file = null;
 
        //If a file became selected, find out which one.
        } else if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(prop)) {
            file = (File) e.getNewValue();
            String myString = file.getAbsolutePath();
            JOptionPane.showMessageDialog(null, "alert", myString, JOptionPane.INFORMATION_MESSAGE); 
        }

    }
}
