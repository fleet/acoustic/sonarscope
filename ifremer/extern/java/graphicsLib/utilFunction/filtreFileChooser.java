package utilFunction;

import java.io.*;
import javax.swing.filechooser.FileFilter;

// Classe cr��e pour manipuler les filtres de fichiers d'un browser de fichiers
// affich� sous Matlab gr�ce � "uicomponent".
// Cf : uicomponent.m
public class filtreFileChooser extends FileFilter{
	   //Description et extension accept�e par le filtre
	   private String description;
	   private String extension;
	   //Constructeur � partir de la description et de l'extension accept�e
	   public filtreFileChooser(String strDescription, String strExtension){		   
	      if(description == null || extension ==null){
	         throw new NullPointerException("La description (ou extension) ne peut �tre null.");
	      }
	      this.description = strDescription;
	      this.extension = strExtension;
	   }
	   
		//Impl�mentation de FileFilter
		public boolean accept(File file){
		      if(file.isDirectory()) { 
		         return true; 
		      } 
		      String nomFichier = file.getName().toLowerCase(); 
		
		      return nomFichier.endsWith(extension);
		}
		
		
		public String getDescription(){
			return description;
		}
	}
