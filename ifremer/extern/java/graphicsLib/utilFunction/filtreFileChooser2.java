package utilFunction;

import java.io.File;
//import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import java.util.StringTokenizer;

// Classe cr��e pour manipuler les filtres de fichiers  dans l'application 
// cla_repertoire de l'arborescence IfremerToolbox.
// D�cline la classe FileFilter de tri par l'extension pour filtrer par des
// cha�nes sp�cifiques : Geometry Type, Data Type, Cha�ne incluse, Cha�ne exclue.
// Cf : uicomponent.mf
public class filtreFileChooser2 extends FileFilter implements java.io.FileFilter {
	   //Description et extension accept�e par le filtre
	   private String Description;
	   private String Extension;
	   private String GeoToFind;
	   private String DataToFind;
	   private String Included;
	   private String Excluded;
	   private JFileChooser jfc; // Composant graphique Java comprenant les filtres.
	   
	   
	   //Constructeur � partir de la description et de l'extension accept�e
	   public filtreFileChooser2(JFileChooser jfc, String strDescription, String strExtension, String strGeoToFind, String strDataToFind,  
			   String strIncluded, String strExcluded){		   
	      //if(description == null || strGeoToFind ==null){
	      //   throw new NullPointerException("La description (ou cha�ne � rechercher) ne peut �tre null.");
	      //}
		   	  this.jfc = jfc;
	          this.Description = strDescription;
		      this.Extension = strExtension;
		      this.GeoToFind = strGeoToFind;
		      this.DataToFind = strDataToFind;
		      this.Included = strIncluded;
		      this.Excluded = strExcluded;

	   }
	   
		//Impl�mentation de FileFilter
		public boolean accept(File file){
		      if(file.isDirectory()) { 
		         return true; 
		      } 
		      String nomFichier = file.getName().toLowerCase(); 
		      String strGeoFilter, 
		      		 strDataFilter,
		      		 strInFilter, 
		      		 strExFilter,
		      		 ficExtension,
		      		 prevToken;
		      
		      StringTokenizer nomFicToken;
		      
		      boolean 	flagOK,
		      			flagExtension,
		      			flagGeoFilter,
		      			flagDataFilter,
		      			flagExFilter,
		      			flagInFilter;
		      
			  
		      if (GeoToFind != null) {
		    	  strGeoFilter = ".*" + GeoToFind + ".*";
		    	  strGeoFilter = strGeoFilter.toLowerCase();
		    	  // Limitation de la chaine � sa taille r�elle.
		    	  strGeoFilter = strGeoFilter.replaceAll(" ", "");
		    	  flagGeoFilter = nomFichier.matches(strGeoFilter);
		      }
		      else {
		    	  flagGeoFilter = true;
		      }
		
		      if (DataToFind != null) {
		    	  strDataFilter = ".*" + DataToFind + ".*";
		    	  strDataFilter = strDataFilter.toLowerCase();
		    	  // Limitation de la chaine � sa taille r�elle.
		      	  strDataFilter = strDataFilter.replaceAll(" ", "");
		      	  flagDataFilter = nomFichier.matches(strDataFilter);
		      }
		      else {
		    	  flagDataFilter = true;
		      }
		      
		      if (Included != null) {
		    	  strInFilter = Included.toLowerCase();
			      strInFilter = ".*" + strInFilter + ".*";
			      flagInFilter = nomFichier.matches(strInFilter);
		      }
		      else {
		    	  flagInFilter = true;
		      }
		      
		      if (Excluded != null) {
		    	  strExFilter = Excluded.toLowerCase();
			      strExFilter = ".*" + strExFilter + ".*";
			      flagExFilter = nomFichier.matches(strExFilter);
		      }
		      else {
		    	  flagExFilter = false;
		      }
		      
    		  nomFicToken = new StringTokenizer(nomFichier, ".");
    		  ficExtension = "";
    		  prevToken = "";
    		  while (nomFicToken.hasMoreTokens()) {
    			  prevToken = ficExtension;
    			  ficExtension = nomFicToken.nextToken();
    			  // Traitement des fichiers en .<ext>.gz
    			  if (ficExtension.equals("gz")) {
        			  ficExtension = prevToken;
    			  }
    		  }
    		  
		      // S�lection des fichiers respectant les filtres.
		      if (Extension.compareTo("*") != 0) {
		    	  // Si on est positionn� sur un filtre sp�cifique
		    	  if (Extension.compareTo("xxx") != 0) {
		    		  flagExtension = (ficExtension.equals(Extension));
		   		      //JOptionPane.showMessageDialog(null, 	"Extension du filtre : " + Extension + " Extension du fichier : " + ficExtension + " et flagExtension : " + String.valueOf(flagExtension));
		      	  }
		    	  // Sinon on est positionn� sur le filtre g�n�rique.
		      	  else {
		      		  flagExtension = false;
		      		  //JOptionPane.showMessageDialog(null, "Taille de la liste : " + String.valueOf(jfc.getChoosableFileFilters().length));
					  FileFilter fileFilter[] = jfc.getChoosableFileFilters();
		      		  for(int i = 0; i <= jfc.getChoosableFileFilters().length -1; i++) {
			      		  // Instanciation du filtre en filtreFileChooser2 pour utiliser
		      			  // la m�thode getExtension.
		      			  if (fileFilter[i] instanceof filtreFileChooser2)
		      			  {
			      			  String extFiltre = ((filtreFileChooser2) fileFilter[i]).getExtension();
				      		  if ((extFiltre.compareTo("xxx") != 0) && (extFiltre.compareTo("*") != 0)) {
					    		  flagExtension = flagExtension || (ficExtension.equals(extFiltre));
					   		      //JOptionPane.showMessageDialog(null, 	"i : " + String.valueOf(i) + " et ficExtension : " + ficExtension + " Extension : " + extFiltre + " et flagExtension : " + String.valueOf(flagExtension));
		      			      }
		      			  }			      		  
		      		  }
		          }
			  }
			  else {				  
				  flagExtension = true;
			  }
		      
		      /*
		      JOptionPane.showMessageDialog(null, "NomFichier : " + nomFichier  + " et Flag str Excluded : " + String.valueOf(nomFichier.matches(strExFilter)));		      
		      JOptionPane.showMessageDialog(null, "NomFichier : " + nomFichier  + " et Flag str Geo : " + String.valueOf(nomFichier.matches(strGeoFilter)));		      
		      JOptionPane.showMessageDialog(null, "NomFichier : " + nomFichier  + " et Flag str Data : " + String.valueOf(nomFichier.matches(strDataFilter)));		      
		      JOptionPane.showMessageDialog(null, "NomFichier : " + nomFichier  + " et Flag str Included : " + String.valueOf(nomFichier.matches(strIncluded)));		      
		       */
		      if (Excluded != null) 
		      {
		      	  flagOK = flagExtension & flagGeoFilter & flagDataFilter & flagInFilter & !flagExFilter;
		      }
		      else 
		      {
		      	  flagOK = flagExtension & flagGeoFilter & flagDataFilter & flagInFilter;
		      }
		      
		      /*
		       JOptionPane.showMessageDialog(null, 	"NomFichier : " + nomFichier + " et flagOK : " + String.valueOf(flagOK) + 
		    		  								" et Flag str Excluded : " + !flagExFilter + 
		    		  								" et Flag str Included : " + flagInFilter + 
		    		  								" et Flag str Geo : " + flagGeoFilter + 
		    		  								" et Flag str Data : " + flagDataFilter +
		    		  								" et Flag Extension : " + nomFichier.endsWith(Extension));		      
		      */
		      return flagOK;
		}
		
	    public void replaceFilters(String strGeoToFindNew, String strDataToFindNew, String strContenantNew, String strExcluantNew){
	    	  // L'appel de cette m�thode r�active la m�thode accept.
		      this.GeoToFind = strGeoToFindNew;
		      this.DataToFind = strDataToFindNew;
		      this.Included = strContenantNew;
		      this.Excluded = strExcluantNew;
	    }
			   
		public String getDescription(){
			return Description;
		}

		public String getExtension(){ 
			return Extension;
		}		
	}
