package testInspect;

import java.util.*;


/**
 * Classe de donn�es Attitude des fichiers SimRad .all
 * 
 * @author <a href="mailto:roger.gallou@ago.fr">Roger GALLOU</a>
 */   
public class myClasse {
	public static final boolean DEBUG = true;

	public float 	EmModel, 
					SystemSerialNumber;
	
	public ArrayList<String> myString;
	public int 	[] myInteger;
	public String myChaine;
	public mySousClasse clBidon;
	
	public mySousClasse getClBidon() {
		return clBidon;
	}

	public void setClBidon(mySousClasse clBidon) {
		this.clBidon = clBidon;
	}

	public String getMyChaine() {
		return myChaine;
	}

	public void setMyChaine(String myChaine) {
		this.myChaine = myChaine;
	}

	public int[] getMyInteger() {
		return myInteger;
	}

	public void setMyInteger(int[] myInteger) {
		this.myInteger = myInteger;
	}

	public void initMyInteger(int index) {
		this.myInteger[index] = index+1;
	}

	public ArrayList<String> getMyString() {
		return myString;
	}

	public void setMyString(ArrayList<String> myString) {
		this.myString = myString;
	}

	public void initMyString(ArrayList<String> myString) {
		this.myString.add("toto");
		this.myString.add("titi");
		this.myString.add("tutu");
	}

	public float getEmModel() {
		return EmModel;
	}

	public void setEmModel(float emModel) {
		EmModel = emModel;
	}

	public float getSystemSerialNumber() {
		return SystemSerialNumber;
	}

	public void setSystemSerialNumber(float systemSerialNumber) {
		SystemSerialNumber = systemSerialNumber;
	}

	
	/**
   * Byte swap a single short value.
   * 
   * @param 	Sans objet  Constructeur vide.
   * @return    Sans objet	Constructeur vide.
   */
	public myClasse()
	{
		// Classe vierge pour les retours en erreur.			
	}
	
	/**
	   * Byte swap a single short value.
	   * 
	   * @param 	Row de la classe Attitude (EmModel, ...)
	   * @return    clData Structure de type Attitude.
	   */
	public myClasse(	float fEmModel, float fSerialNumber) {
		
		//constructeur
		this.setEmModel(fEmModel);
		this.setSystemSerialNumber(fSerialNumber);
	}


	
}