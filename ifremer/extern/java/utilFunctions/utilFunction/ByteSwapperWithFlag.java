package utilFunction;

	/*
	 * (C) 2004 - Geotechnical Software Services
	 * 
	 * This code is free software; you can redistribute it and/or
	 * modify it under the terms of the GNU Lesser General Public 
	 * License as published by the Free Software Foundation; either 
	 * version 2.1 of the License, or (at your option) any later version.
	 *
	 * This code is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU Lesser General Public License for more details.
	 *
	 * You should have received a copy of the GNU Lesser General Public 
	 * License along with this program; if not, write to the Free 
	 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, 
	 * MA  02111-1307, USA.
	 * 
	 *  Modif.	:	R.GALLOU (ALTRAN O.)
	 *  Date	:	15/05/07
	 *  Comment.:	Ajout du Flag de positionnement L.Endian (false) ou 
	 *  			B.Endian (True).
	 */


	/**
	 * Utility class for doing byte swapping (i.e. conversion between
	 * little-endian and big-endian representations) of different data types.
	 * Byte swapping is typically used when data is read from a stream 
	 * delivered by a system of different endian type as the present one.
	 * 
	 * @author <a href="mailto:jacob.dreyer@geosoft.no">Jacob Dreyer</a>
	 */   
	public class ByteSwapperWithFlag
	{
	  /**
	   * Byte swap a single short value.
	   * 
	   * @param value  Value to byte swap.
	   * @return       Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static short swap (short a_value, boolean flagEndian)
	  {
	    if (!flagEndian)
    	{
		    int b1 = a_value & 0xff;
		    int b2 = (a_value >> 8) & 0xff;
	
		    return (short) (b1 << 8 | b2 << 0);
    	}
	    else
	    {
	    	return a_value;
	    }
	  }

	  

	  /**
	   * Byte swap a single int a_value.
	   * 
	   * @param a_value  a_value to byte swap.
	   * @return       Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static int swap (int a_value, boolean flagEndian)
	  {
	    if (!flagEndian)
    	{
		    int b1 = (a_value >>  0) & 0xff;
		    int b2 = (a_value >>  8) & 0xff;
		    int b3 = (a_value >> 16) & 0xff;
		    int b4 = (a_value >> 24) & 0xff;
	
		    return b1 << 24 | b2 << 16 | b3 << 8 | b4 << 0;
    	}
	    else
	    {
	    	return a_value;
	    }
	  }

	  

	  /**
	   * Byte swap a single long a_value.
	   * 
	   * @param a_value  a_value to byte swap.
	   * @return       Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static long swap (long a_value, boolean flagEndian)
	  {
	    if (!flagEndian)
    	{
		    long b1 = (a_value >>  0) & 0xff;
		    long b2 = (a_value >>  8) & 0xff;
		    long b3 = (a_value >> 16) & 0xff;
		    long b4 = (a_value >> 24) & 0xff;
		    long b5 = (a_value >> 32) & 0xff;
		    long b6 = (a_value >> 40) & 0xff;
		    long b7 = (a_value >> 48) & 0xff;
		    long b8 = (a_value >> 56) & 0xff;
	
		    return b1 << 56 | b2 << 48 | b3 << 40 | b4 << 32 |
		           b5 << 24 | b6 << 16 | b7 <<  8 | b8 <<  0;
    	}
	    else
	    {
	    	return a_value;
	    }
	  }
	  

	  
	  /**
	   * Byte swap a single float a_value.
	   * 
	   * @param a_value  a_value to byte swap.
	   * @return       Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static float swap (float a_value, boolean flagEndian)
	  {
	    int intValue = Float.floatToIntBits (a_value);
	    intValue = swap (intValue, flagEndian);
	    return Float.intBitsToFloat (intValue);
	  }

	  

	  /**
	   * Byte swap a single double a_value.
	   * 
	   * @param 	a_value to byte swap.
	   * @return       		Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static double swap (double a_value, boolean a_flagEndian)
	  {
	    long longValue = Double.doubleToLongBits (a_value);
	    longValue = swap (longValue, a_flagEndian);
	    return Double.longBitsToDouble (longValue);
	  }


	  
	  /**
	   * Byte swap an array of shorts. The result of the swapping
	   * is put back into the specified array.
	   *
	   * @param 	array  Array of values to swap
	   * @return       		Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static void swap (short[] a_array, boolean a_flagEndian)
	  {
	    for (int i = 0; i < a_array.length; i++)
	      a_array[i] = swap (a_array[i], a_flagEndian);
	  }

	  
	  
	  /**
	   * Byte swap an array of ints. The result of the swapping
	   * is put back into the specified array.
	   * 
	   * @param a_array  Array of values to swap
	   * @return       		Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static void swap (int[] a_array, boolean a_flagEndian)
	  {
	    for (int i = 0; i < a_array.length; i++)
	      a_array[i] = swap (a_array[i], a_flagEndian);
	  }


	  
	  /**
	   * Byte swap an array of longs. The result of the swapping
	   * is put back into the specified array.
	   * 
	   * @param a_array  Array of values to swap
	   * @return       		Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static void swap (long[] a_array, boolean a_flagEndian)
	  {
	    for (int i = 0; i < a_array.length; i++)
	      a_array[i] = swap (a_array[i], a_flagEndian);
	  }


	  
	  /**
	   * Byte swap an array of floats. The result of the swapping
	   * is put back into the specified array.
	   * 
	   * @param a_array  Array of values to swap
	   */
	  public static void swap (float[] a_array, boolean a_flagEndian)
	  {
	    for (int i = 0; i < a_array.length; i++)
	      a_array[i] = swap (a_array[i], a_flagEndian);
	  }


	  
	  /**
	   * Byte swap an array of doubles. The result of the swapping
	   * is put back into the specified array.
	   * 
	   * @param a_array  Array of values to swap
	   * @return       		Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static void swap (double[] a_array, boolean a_flagEndian)
	  {
	    for (int i = 0; i < a_array.length; i++)
	      a_array[i] = swap (a_array[i], a_flagEndian);
	  }
		/**
	   * Byte swap a single short value.
	   * 
	   * @param value  Value to byte swap.
	   * @return       Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static byte[] swap2byte(byte []u8value, boolean flagEndian)
	  {
	    if (!flagEndian)
		{
		    byte tmp = u8value[0];
		    u8value[0] = u8value[1];
		    u8value[1] = tmp;
	
		    return u8value;
		}
	    else
	    {
	    	return u8value;
	    }
	  }



	/**
	   * Byte swap a single short value.
	   * 
	   * @param value  Value to byte swap.
	   * @return       Byte swapped representation.
	   * @exception
	   * @see		
	   */
	  public static byte[] swap4byte(byte []u8value, boolean flagEndian)
	  {
	    if (!flagEndian)
		{
		    byte tmp = u8value[0];
		    u8value[0] = u8value[3];
		    u8value[3] = tmp;
		    tmp = u8value[1];
		    u8value[1] = u8value[2];
		    u8value[2] = tmp;
		    
		    return u8value;
		}
	    else
	    {
	    	return u8value;
	    }
	  }


	}


