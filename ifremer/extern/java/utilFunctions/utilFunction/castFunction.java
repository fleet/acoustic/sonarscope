package utilFunction;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class castFunction {
	/*
	 * R�cup�re un entier de type Short non-sign� de 2 octets.
	 * @param		tabBuf	Tableau d'octets
	 * @return				Entier de type short non-sign�
	 * @exceptions			
	 * @see			
	 */
	/*
	public static short recupUnsignedByte(byte []tabBuf )
	{
		short anUnsignedByte = 0;

        int firstByte = 0;

		// Check to make sure we have enough bytes
		if(tabBuf.length < 1) 
			System.out.println("Erreur de conversion en Unsigned Byte");
		int index = 0;
		
		firstByte = (0x000000FF & ((int)tabBuf[index]));
		index++;
		anUnsignedByte = (short)firstByte;

		return anUnsignedByte;
	      
	}
	 */
	public static short[] recupUnsignedByte(byte []tabByte )
	{
		short []anUnsignedByte = new short[tabByte.length];
		int firstByte = 0;

		// Check to make sure we have enough bytes
		if(tabByte.length < 1) 
			System.out.println("Erreur de conversion en Unsigned Byte");
		
		for (int iLoop=0; iLoop<tabByte.length; iLoop++)
		{
			firstByte = (0x000000FF & ((int)tabByte[iLoop]));
			anUnsignedByte[iLoop] = (short)firstByte;
		}

		return anUnsignedByte;
	      
	}

	public static short recupUnsignedByte(byte byteBuf )
	{
		short anUnsignedByte = 0;

        int firstByte = 0;
		
		firstByte = (0x000000FF & ((int)byteBuf));
		anUnsignedByte = (short)firstByte;

		return anUnsignedByte;
	      
	}

	/*
	 * R�cup�re un entier de type Long non-sign� d'une tableau de 4 octets.
	 * @param		tabByte	Tableau d'octets
	 * @return				Entier de type Long non-sign�
	 * @exceptions			
	 * @see			
	 */
	public static long recupUnsignedInt(byte []tabByte )
	{
		long anUnsignedInt = 0;

	        int firstByte = 0;
	        int secondByte = 0;
	        int thirdByte = 0;
	        int fourthByte = 0;

		// V�rification du nombre suffisants d'octets.
		if(tabByte.length < 4) 
			System.out.println("Erreur de conversion en Unsigned Int");
		int index = 0;
		
        firstByte = (0x000000FF & ((int)tabByte[index]));
        secondByte = (0x000000FF & ((int)tabByte[index+1]));
        thirdByte = (0x000000FF & ((int)tabByte[index+2]));
        fourthByte = (0x000000FF & ((int)tabByte[index+3]));
        index = index+4;
		anUnsignedInt  = ((long) (firstByte << 24
		                	| secondByte << 16
	                        | thirdByte << 8
	                        | fourthByte))
	                       & 0xFFFFFFFFL;
		return anUnsignedInt;
	      
	}
	
	/*
	 * R�cup�re un entier de type Char non-sign� d'une tableau de 2 octets.
	 * @param		tabByte	Tableau d'octets
	 * @return				Entier de type Long non-sign�
	 * @exceptions			
	 * @see			
	 */
	public static char recupUnsignedShort(byte []tabByte )
	{
		char anUnsignedShort = 0; // un Char en Unicode.

        int firstByte = 0;
        int secondByte = 0;

		// V�rification du nombre suffisants d'octets.
		if(tabByte.length < 2) 
			System.out.println("Erreur de conversion en Unsigned Short");
		int index = 0;
		
	    firstByte = (0x000000FF & ((int)tabByte[index]));
	    secondByte = (0x000000FF & ((int)tabByte[index+1]));
		index = index+2;
		anUnsignedShort  = (char) (firstByte << 8 | secondByte);

		return anUnsignedShort;
	      
	}

	public static long recupLongFromByte(byte []tabByte, boolean flagEndian)
	  {
		    // Par d�finition, le type Long sous Java, est non-sign�.
			ByteBuffer buf = ByteBuffer.wrap(tabByte);
			if (flagEndian == false)
			{
				buf.order(ByteOrder.LITTLE_ENDIAN);
			}
			else
			{
				buf.order(ByteOrder.BIG_ENDIAN);
			}
			return buf.getLong();
	  }

	public static float recupFloatFromByte(byte []tabByte, boolean flagEndian)
	  {
		    ByteBuffer buf = ByteBuffer.wrap(tabByte);
			if (flagEndian == false)
			{
				buf.order(ByteOrder.LITTLE_ENDIAN);
			}
			else
			{
				buf.order(ByteOrder.BIG_ENDIAN);
			}
			return buf.getFloat();
	  }

	public static int recupIntFromByte(byte []tabByte, boolean flagEndian)
	  {
		    ByteBuffer buf = ByteBuffer.wrap(tabByte);
			if (flagEndian == false)
			{
				buf.order(ByteOrder.LITTLE_ENDIAN);
			}
			else
			{
				buf.order(ByteOrder.BIG_ENDIAN);
			}
			return buf.getInt();
	  }

	public static double recupDoubleFromByte(byte []tabByte, boolean flagEndian)
	  {
		    ByteBuffer buf = ByteBuffer.wrap(tabByte);
			if (flagEndian == false)
			{
				buf.order(ByteOrder.LITTLE_ENDIAN);
			}
			else
			{
				buf.order(ByteOrder.BIG_ENDIAN);
			}
			return buf.getDouble();
	  }

}
