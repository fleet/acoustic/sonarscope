package utilFunction;

public class dateCompute {
	/*
	 * Conversion d'une date codee "Jour, Mois, Annee" en date codee "Ifremer"
	 * @param		Jour	: Jour du mois
	 * 				Mois	: Mois
	 *				Annee	: Annee
	 * @return		dIfr 	: Nombre de jours depuis le 24/11/-4713		
	 * @exceptions			
	 * @see			
	 * @examples
	 * 	dateCompute.dayJma2Ifr(13, 9, 1998);			
	 */
	public static double dayJma2Ifr(int iDay, int iMonth, int iYear )
	{
		float 	fKS;
		
		double	dDate;
		

		if (iMonth > 2) {
			iMonth = iMonth - 3;
		}
		else if (iMonth <= 2) {
			iMonth = iMonth + 9;
			iYear= iYear -1;
		}
		
		fKS = (float)Math.floor((double)(iYear/100));
		iYear = iYear - 100 * (int)fKS;
		
		dDate = Math.floor((float)146097 * fKS / 4) + Math.floor((float)1461 * iYear / 4) + Math.floor((153 * iMonth + 2) / 5) + iDay + 1721119;

		return dDate;
	      
	}

}
