package utilFunction;


/**
 * Classe de description du flag de retour d'une procÚdure de lecture.
 * 
 * @author <a href="mailto:roger.gallou@ago.fr">Roger GALLOU</a>
 */   
public class flagRetour {
	private boolean flagRetour;

	public boolean isFlagRetour() {
		return flagRetour;
	}

	public void setFlagRetour(boolean flagRetour) {
		this.flagRetour = flagRetour;
	}
}
