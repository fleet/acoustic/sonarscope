package utilFunction;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;


	public class myProgressBar extends Frame implements ChangeListener 
	{
		private static final long serialVersionUID = 42L;
		public JProgressBar maBarre;
	
		public myProgressBar(String cTitre) 
		{
			setLayout(new BorderLayout());
			maBarre = new JProgressBar(0,99);
			maBarre.setString(cTitre);
			maBarre.setStringPainted(true);
			maBarre.addChangeListener(this);
			add("Center", maBarre);
			setBounds(320,240,450,90);
			setVisible(true);
		}

		public void stateChanged(ChangeEvent e) 
		{
			maBarre.setString ("Progression: " + (int)(maBarre.getPercentComplete()*100) + "%");
		}

		public static void main(String[] args) 
		{
			myProgressBar monTest = new myProgressBar("Progression");
			int i = 0;
			Object o = new Object();
			while (i<100) 
			{
				i = i + 10;
				monTest.maBarre.setValue( i);
				try {
					synchronized(o) {o.wait(1000);}
				}
				catch(InterruptedException ex) {}
			//Thread.yield();
			}
			monTest.setVisible(false);
		}
	}
