package utilFunction;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class tabDataFromFile {
	public static float[] recupTabFloatFromFile(RandomAccessFile rfid,
												int iSizeTab,
												boolean flagEndian,
												Object	scaleFactor) throws IOException
	{
		ByteBuffer buf;
		byte [] 	tabByte,
					tab4Byte = new byte[4];
		
		float []	fTabDataOut = new float[iSizeTab];
		
		tabByte = new byte[iSizeTab * 4];
		buf = ByteBuffer.allocateDirect(iSizeTab * 4);
		if (flagEndian == false) {
			buf.order(ByteOrder.LITTLE_ENDIAN);
		} else {
			buf.order(ByteOrder.BIG_ENDIAN);
		}
		
		try {
			rfid.read(tabByte, 0,iSizeTab * 4);
		} catch (Exception e) {
			fTabDataOut = null;
			rfid.close();
			return fTabDataOut;
		}
		buf = ByteBuffer.wrap(tabByte);
		buf.asFloatBuffer();
		for (int iLoop = 0; iLoop < iSizeTab; iLoop++) {
			buf.get(tab4Byte);
			// Prendre en compte le Scale Factor pour généraliser la
			// fonction.
			fTabDataOut[iLoop] = castFunction
					.recupFloatFromByte(tab4Byte, flagEndian);
		}
		
		return fTabDataOut;
		
	}

}
