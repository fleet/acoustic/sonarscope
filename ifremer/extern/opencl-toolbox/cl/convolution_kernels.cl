__kernel void convolution(__global float *odata, __global float* idata, __constant float* filter, int width, int height, int filterSize)
{
    unsigned int yIndex = get_global_id(0);
    int filterRadius = filterSize/2;
    
    if (yIndex>=filterRadius && yIndex < width-filterRadius)
    {
        for (uint xIndex = filterRadius; xIndex < height-filterRadius; ++xIndex)
        {
            unsigned int index_in  = xIndex + height * yIndex;
			odata[index_in] = 0;
			float div = 0;
			int index = 0;
            for(int i=-filterRadius; i<=filterRadius; i++)
			{
				for(int j=-filterRadius; j<=filterRadius; j++)
				{
					//odata[index_in] = idata[xIndex + j + height * (yIndex+i)] * filter[(i+filterRadius)*filterRadius + (j+filterRadius)];
					odata[index_in] += idata[xIndex + j + height * (yIndex+i)] * filter[index];
					div += filter[index++];
				}
			}
			odata[index_in] /= div;
        }
    }
}

__kernel void convolution2D(__global float *odata, __global float* idata, __constant float* filter, int width, int height, int filterSize)
{
    unsigned int yIndex = get_global_id(0);
	unsigned int xIndex = get_global_id(1);
    int filterRadius = filterSize/2;
    
    if (yIndex>=filterRadius && yIndex < width-filterRadius && xIndex>=filterRadius && xIndex < height-filterRadius)
    {
		unsigned int index_in  = xIndex + height * yIndex;
		odata[index_in] = 0;
		float div = 0;
		int index = 0;
		for(int i=-filterRadius; i<=filterRadius; i++)
		{
			for(int j=-filterRadius; j<=filterRadius; j++)
			{
				//odata[index_in] = idata[xIndex + j + height * (yIndex+i)] * filter[(i+filterRadius)*filterRadius + (j+filterRadius)];
				odata[index_in] += idata[xIndex + j + height * (yIndex+i)] * filter[index];
				div += filter[index++];
			}
		}
		odata[index_in] /= div;
    }
}

__kernel void convolution2DLocale(__global float *odata, __global float* idata, __constant float* filter, int width, int height, int filterSize)
{
    unsigned int yIndex = get_global_id(0);
	unsigned int xIndex = get_global_id(1);
    int filterRadius = filterSize/2;
    
    if (yIndex>=filterRadius && yIndex < width-filterRadius && xIndex>=filterRadius && xIndex < height-filterRadius)
    {
		unsigned int index_in  = xIndex + height * yIndex;
		odata[index_in] = 0;
		float div = 0;
		int index = 0;
		for(int i=-filterRadius; i<=filterRadius; i++)
		{
			for(int j=-filterRadius; j<=filterRadius; j++)
			{
				//odata[index_in] = idata[xIndex + j + height * (yIndex+i)] * filter[(i+filterRadius)*filterRadius + (j+filterRadius)];
				odata[index_in] += idata[xIndex + j + height * (yIndex+i)] * filter[index];
				div += filter[index++];
			}
		}
		odata[index_in] /= div;
    }
}