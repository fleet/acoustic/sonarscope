
#ifdef cl_khr_fp64
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
#else
    #error "Double precision floating point not supported by OpenCL implementation."
#endif

inline int get_index(int nelems, int index ) {
/* Fetch a linear index to work on given # of elements
 * and previous index. -1 if start
 */

  if (index == -1)  {
    index = get_global_id(0);
  } else {
    index += get_global_size(0);
  }

  if (index >= nelems) index = -1;

  return index;
}


__kernel void add(__global float *out, __global const float *x, __global const float *y, int N) {

  int id = get_global_id(0);
  // Nouvelle formulation apr�s recompilation de Mikael MVN du fichier OCLCommandQueue.h
  if(id < N) {
    out[id] = x[id] + y[id];
  }
}

__kernel void interp1Linear( __global double *dY2, __global const double *dX1,__global const double *dY1, __global const double *dX2, const int iSizeX1, const int N) {
    
    bool    bFlagNaN = true;
    
    const int   istart = 1; // on commence a 1 a cause du dX1[iLoop-1]

    int    	iLoop,
            jLoop,
            iDimdX1 = iSizeX1;

    double  dDiffapres,
            dDiffavant;
    
    int id = get_global_id(0);
    

    // Traitement du cas unique ou les deux axes sont croissants.
	if(id < N)      //for(jLoop = 0; jLoop < iDimdX2; jLoop++)
	{	
        bFlagNaN = true;
        // Si la valeur � interpoler est comprise dans l'ensemble des abscisses connues.
        if (dX2[id] >= dX1[0] && dX2[id] <= dX1[iDimdX1-1])
        {
            for(iLoop = istart; iLoop < iDimdX1; iLoop++)
            {
                if(dX1[iLoop] >= dX2[id])
                {
                    dDiffapres = dX1[iLoop] - dX2[id];
                    dDiffavant = dX2[id] - dX1[iLoop - 1]; // les dDiffavant et apres sont forc�ment positives
                    dY2[id] = (dY1[iLoop - 1] * dDiffapres + dY1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                    bFlagNaN=false;
                    break;
                }
            } // Fin de la boucle sur iLoop, dX1
        }     
        // Valeur d'interpolation non rencontr�e --> NaN
        if (bFlagNaN==true)
        {
            dY2[id] = (double)NAN; 
        }
    } // Fin de la boucle (id < N)
 
}  // Fin de interp1_Linear

__kernel void interp3Linear( __global double *dReflec2,  __global double *dY2,  __global double *dX2,
                            __global const double *dSub1,__global const double *dReflec1, __global const double *dY1, 
                            __global const double *dX1, __global const double *dSub2, const int iSizeX1, const int N) {
    
    bool        bFlagNaN = true;
    
    const int   istart = 1; // on commence a 1 a cause du dSub1[iLoop-1]

    int         iLoop,
                jLoop,
                iDimdSub1 = iSizeX1;

    double      dDiffapres,
                dDiffavant;
    
    int id = get_global_id(0);
    

    // Traitement du cas unique ou les deux axes sont croissants.
	if(id < N)      //for(jLoop = 0; jLoop < iDimdSub2; jLoop++)
	{	
        bFlagNaN = true;
        // Si la valeur � interpoler est comprise dans l'ensemble des abscisses connues.
        if (dSub2[id] >= dSub1[0] && dSub2[id] <= dSub1[iDimdSub1-1])
        {
            for(iLoop = istart; iLoop < iDimdSub1; iLoop++)
            {
                if(dSub1[iLoop] >= dSub2[id])
                {
                    dDiffapres = dSub1[iLoop] - dSub2[id];
                    dDiffavant = dSub2[id] - dSub1[iLoop - 1]; // les dDiffavant et apres sont forc�ment positives
                    dX2[id] = (dX1[iLoop - 1] * dDiffapres + dX1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                    dY2[id] = (dY1[iLoop - 1] * dDiffapres + dY1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                    dReflec2[id] = (dReflec1[iLoop - 1] * dDiffapres + dReflec1[iLoop] * dDiffavant) / (dDiffapres + dDiffavant);
                    bFlagNaN=false;
                    break;
                }
            } // Fin de la boucle sur iLoop, dSub1
        }     
        // Valeur d'interpolation non rencontr�e --> NaN
        if (bFlagNaN==true)
        {
            dX2[id] = (double)NAN; 
            dY2[id] = (double)NAN; 
            dReflec2[id] = (double)NAN; 
        }
    } // Fin de la boucle (id < N)
 
}  // Fin de interp3_Linear


