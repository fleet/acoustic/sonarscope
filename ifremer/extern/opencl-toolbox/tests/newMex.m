clear filtre img
filtre = ones(3,3);
img = ones(10,10);
out = ones(10,10);
%%
img = openclMex('C:\SonarScopeTbx\ifremer\extern\opencl-toolbox\cl/transpose_kernels.cl', [128,0,0], [128,0,0], 'ro', filtre, 'ro', img, 'wo', out);
% ocl.initialize(1,1);
% ocl.addfile('C:\SonarScopeTbx\ifremer\extern\opencl-toolbox\cl/transpose_kernels.cl');
% ocl.build();
%
% % % nElems              = zeros((BLOCKDIM + 1)*BLOCKDIM,1, 'single');
% % % BLOCKDIM            = single(16);
% % % block               = single((BLOCKDIM + 1)* BLOCKDIM * 4); % 4 = sizeof(float)
% %%
% %[A, label] = Lena;
% IM = imread('C:\SScTbxResources\images.png');
% %IM = imread('C:\SScTbxResources\Desert.jpg');
% A = IM(:,:,1);
% %A = [1,0,0,0;0,2,0,0;0,0,3,0;0,0,0,4;5,0,0,0];
% %A = zeros(sizeMagic,sizeMagic);
% sizeX               = int32(size(A,2));
% sizeY               = int32(size(A,1));
% hostA               = single(A);
% hostB               = zeros(size(hostA,2),size(hostA,1), 'single');
% offset              = int32(0);
% BLOCKDIM            = single(16);
% block               = single((BLOCKDIM + 1)* BLOCKDIM * 4); % 4 = sizeof(float)
% %% transpose_naive
% clear devA devB devLocal
% %clear devFilter
%
% %devFilter   = clobject(single(fi));
% devA        = clobject(hostA);
% devB        = clobject(hostB);
%
% %clTranspose = clkernel('transpose', [32,32,0], [32,32,0]);
% %clTranspose = clkernel('transpose_naive', [32,32,0], [32,32,0]);
% clTranspose = clkernel('transpose_naive2', [128,0,0], [128,0,0]);
%
% %% transpose optim : ne fonctionne pas pour l'instant !!!!
% %allouer un block de memoire locale
% % clear devA devB
% %
% % devA        = clobject(hostA);
% % devB        = clobject(hostB);
% %
% % clTranspose = clkernel('transpose', [64,64,0], [64,64,0]);
%
% %% MatLab natif
% tic;
% Am = my_transpose(hostA, 'ForceOpenCL', false);
% toc;
% figure; imagesc(A); title('Image origine');
% figure; imagesc(Am); title('Transpose - MatLab');
% %% OpenCL
% tic;
% %localSize = int32(1000);
% %clTranspose(devB, devA, offset, sizeX, sizeY, localSize);
% clTranspose(devB, devA, offset, sizeX, sizeY);
% toc;
% Acl = devB.get();
% %max(max(abs(Acl - Am)))
% figure; imagesc(Acl); title('Transpose - OpenCL');


