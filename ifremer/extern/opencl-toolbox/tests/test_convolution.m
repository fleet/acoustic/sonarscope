%%
close all;
clear ocl % Get platform info
platforms = openclcmd();

platforms %#ok<NOPTS>
platforms(1).devices

ocl = opencl();
ocl.initialize(1,1);
ocl.addfile('C:\SonarScopeTbx\ifremer\extern\opencl-toolbox\cl/convolution_kernels.cl');
ocl.build();

%%
sizeMagic           = 4096;
sizeX               = int32(sizeMagic);
% sizeY               = int32(sizeMagic);
% BLOCKDIM            = 16;
A                   = (magic(sizeX)); %#ok<NASGU>
% hostA               = single(A);
% hostB               = zeros(size(hostA,1),size(hostA,2), 'single');
% % nElems              = zeros((BLOCKDIM + 1)*BLOCKDIM,1, 'single');
% % BLOCKDIM            = single(16);
% % block               = single((BLOCKDIM + 1)* BLOCKDIM * 4); % 4 = sizeof(float)
%%
global IfrTbxResources %#ok<GVMIS>
%[A, label] = Lena;
%IM = imread('C:\SScTbxResources\Wellington.jpg');
IM = imread(fullfile(IfrTbxResources, 'images.png'));
%IM = imread('C:\IfremerToolboxResources\Desert.jpg');
A = IM(:,:,1);
%A = [1,0,0,0;0,2,0,0;0,0,3,0;0,0,0,4;5,0,0,0];
%A = [1,0,0,0,0;0,2,0,0,0;0,0,3,0,0;0,0,0,4,0;5,0,0,0,0];
%A = zeros(sizeMagic,sizeMagic);
sizeX               = int32(size(A,2));
sizeY               = int32(size(A,1));
hostA               = single(A);
hostB               = zeros(size(hostA,1),size(hostA,2), 'single');
offset              = int32(0);
BLOCKDIM            = single(16);
block               = single((BLOCKDIM + 1)* BLOCKDIM * 4); % 4 = sizeof(float)
%filt = [2 8 4 3 1; 2 3 4 5 6; 1 2 1 4 1; 4 5 6 1 4; 2 4 1 4 1];
filt = ones(3,3, 'single');
%filt = [1];
%% transpose_naive
clear devA devB devFilter
devFilter   = clobject(filt);
devA        = clobject(hostA);
devB        = clobject(hostB);


clConvolution = clkernel('convolution2D', [32,32,0], [32,32,0]);
%clConvolution = clkernel('convolution', [128,0,0], [128,0,0]);

%% MatLab natif
tic;
Am = filter2(filt,hostA);
toc;
figure; imagesc(A); title('Image origine');
figure; imagesc(Am); title('Convolution - MatLab');

%% OpenCL
tic;
filterSize = int32(size(filt,1));
clConvolution(devB, devA, devFilter, sizeX, sizeY, filterSize);
Acl = devB.get();
toc;
%max(max(abs(Acl - Am)))
figure; imagesc(Acl); title('Convolution - OpenCL');


