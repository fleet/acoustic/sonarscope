%% Cette partie remplace le calcul de gridBeam � la ligne 191 sur les trois valeurs X, Y et Reflec
% en trois op�rations d'interpolations.
clear hostX1 hostX2 hostY1 hostX2
%% Calcul de x.
hostSub1    = sub1;
hostSub2    = sub2;
hostx       = double(x);
hostY2      = zeros(size(sub2,2),1, 'double');

dimSub1     = int32(numel(hostSub1));
nElems      = int32(numel(hostSub2));

%
devSub1 = clobject(hostSub1);
devSub2 = clobject(hostSub2);
devY1   = clobject(hostx);
devY2   = clobject(hostY2);
interpLinear_ocl(devY2, devSub1, devY1, devSub2, dimSub1, nElems);
x       = devY2.get();
clear hostx;
% Calcul de y.
hostx   = double(y);
devY1   = clobject(hosty);
interp1Linear_ocl(devY2, devSub1, devY1, devSub2, dimSub1, nElems);
y       = devY2.get();
clear hosty;
% Calcul de y.
hostReflec  = double(reflec);
devReflec   = clobject(hostReflec);
interp1Linear_ocl(devReflec, devSub1, devY1, devSub2, dimSub1, nElems);
reflec      = devReflec.get();
%% Cette partie remplace le calcul de gridBeam � la ligne 191 sur les trois valeurs X, Y et Reflec
% en une seule op�ration d'interpolation.
tic
xMex      = interp1Linear_mex(sub1, double(x), sub2);
yMex      = interp1Linear_mex(sub1, double(y), sub2);
reflecMex = interp1Linear_mex(sub1, double(reflec), sub2);
toc
sub = ~isnan(reflecMex) & (yMex >= Depth(1)) & (yMex <= Depth(end)) & (xMex >= Across(1)) & (xMex <= Across(end));

%% Calcul de x.
clear hostx hosty hostReflec hostY2 hostX2 hostReflec ;

hostSub1    = sub1;
hostSub2    = sub2;
hostx       = double(x);
hosty       = double(y);
hostReflec  = double(reflec);
hostY2      = zeros(size(sub2,2),1, 'double');
hostX2      = zeros(size(sub2,2),1, 'double');
hostReflec2 = zeros(size(sub2,2),1, 'double');

dimSub1     = int32(numel(hostSub1));
nElems      = int32(numel(hostSub2));

%
devSub1     = clobject(hostSub1);
devSub2     = clobject(hostSub2);
devX1       = clobject(hostx);
devY1       = clobject(hosty);
devReflec1  = clobject(hostReflec);
devX2       = clobject(hostY2);
devY2       = clobject(hostY2);
devReflec2  = clobject(hostReflec2);

%
interp3Linear_ocl(devReflec2, devY2, devX2, devSub1, devReflec1, devY1, devX1, devSub2, dimSub1, nElems);
xOcl        = devX2.get()';
yOcl        = devY2.get()';
reflecOcl   = devReflec2.get()';

sub = ~isnan(reflecOcl) & (yOcl >= Depth(1)) & (yOcl <= Depth(end)) & (xOcl >= Across(1)) & (xOcl <= Across(end));
%%
% Comparaison des m�thodes.
figure; plot(xMex, sub2, '+b'); grid on; hold on; plot(xOcl', sub2, 'or');
figure; plot(yMex, sub2, '+b'); grid on; hold on; plot(yOcl, sub2, 'or');
figure; plot(reflecMex, sub2, '+b'); grid on; hold on; plot(reflecOcl, sub2, 'or');