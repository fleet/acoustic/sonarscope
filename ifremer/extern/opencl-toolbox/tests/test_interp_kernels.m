%%

fileName = 'C:\SonarScopeTbxGLU\Correction_Interp1\TestInterp1Linear.mat';
S=load(fileName);
%%%%figure; plot(S.sub2, S.xm, '+b'); grid on; hold on; plot(S.sub2, S.xc, 'xr');
S.x1    = S.sub1';
S.y1  = S.x;
S.x2  = S.sub2;
S.y2m = S.xm;
S.y2c = S.xc;
S = rmfield(S, 'x');
S = rmfield(S, 'sub1');
S = rmfield(S, 'sub2');
S = rmfield(S, 'xm');
S = rmfield(S, 'xc');

%%
clear ocl devX1 devX2 devY1 devY2
%%

% Get platform info
platforms = openclcmd();

platforms %#ok<*NOPTS>
platforms(1).devices

ocl = opencl();
ocl.initialize(1,1);
ocl.addfile('C:\SonarScopeTbx\ifremer\extern\opencl-toolbox/cl/interp_kernels.cl');
ocl.build();
interp1Linear_ocl = clkernel('testPolar', [128, 0,0], [128,0,0]);

%%
hostX1 = S.x1;
hostX2 = S.x2;
hostY1 = double(S.y1);
hostY2 = zeros(size(S.x2,2),1, 'double');

dimX1   = int32(numel(hostX1));
dimX2   = int32(numel(hostX2));

%%
devX1 = clobject(hostX1);
devX2 = clobject(hostX2);
devY1 = clobject(hostY1);
devY2 = clobject(hostY2);

%%

tic;
S.y2m = interp1Linear_mex(S.x1, double(S.y1), S.x2);
toc;
clear pppp;
tic;
interp1Linear_ocl(devY2, devX1, devY1, devX2, dimX1, dimX2);
toc;
pppp = devY2.get()'
max(abs(pppp - S.y2m))
%%
figure; plot(S.x2, pppp, '+b'); grid on; hold on; plot(S.x1, S.y1, 'or');
figure; PlotUtils.createSScPlot(S.x2, S.y2m, '+b'); grid on; hold on; PlotUtils.createSScPlot(S.x1, S.y1, 'or'); PlotUtils.createSScPlot(S.x2, S.y2c, '*g');
%%
% Tests d'interpolation Mex
tic;
S.y2c = interp1Nearest_mex2(S.x1, double(S.y1), S.x2);
toc;
tic;
S.y2m = interp1(S.x1, double(S.y1), S.x2, 'nearest');
toc;
max(abs(S.y2c - S.y2m))
figure; PlotUtils.createSScPlot(S.x2, S.y2m, '+b'); grid on; hold on; PlotUtils.createSScPlot(S.x1, S.y1, 'or'); PlotUtils.createSScPlot(S.x2, S.y2c, '*g');
