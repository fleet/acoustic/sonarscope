clear ocl devA devB devC

%%
ocl = opencl();
ocl.initialize(1,1);
ocl.addfile('C:\SonarScopeTbx\ifremer\extern\opencl-toolbox\cl/matlab_kernels.cl');
ocl.build();

%%
hostA = single(1:1:10000*10000);
hostB = single(hostA+0.5);
hostC = zeros(size(hostA), 'single');

nElems = uint32(numel(hostA));

devA = clobject(hostA);
devB = clobject(hostB);
devC = clobject(hostC);
%%
add = clkernel('add', [128, 0,0], [128,0,0]);
% sub = clkernel('minus', [128, 0,0], [128,0,0]);
% div = clkernel('divide', [128, 0,0], [128,0,0]);
% mul = clkernel('times', [128, 0,0], [128,0,0]);
% fexp = clkernel('exponential', [128,0,0], [128,0,0]);

%%
tic;
refC = hostA + hostB;
toc;
tic;
add(devC, devA, devB, nElems);
toc;
max(abs(devC.get() - refC))

% pppp = devC.get();
% pppp(128)
% refC(128)
% pppp(129)
% refC(129)

% sub(devC, devA, devB, nElems); refC = hostA - hostB;
% max(abs(devC.get() - refC))
%
% div(devC, devA, devB, nElems); refC = hostA ./ hostB;
% max(abs(devC.get() - refC))
%
% mul(devC, devA, devB, nElems); refC = hostA .* hostB;
% max(abs(devC.get() - refC))
%
% fexp(devC, devA, nElems); refC = exp(hostA);
% max(abs(devC.get() - refC))


