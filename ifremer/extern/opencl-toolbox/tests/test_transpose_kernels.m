%%
clear ocl % Get platform info
platforms = openclcmd();

platforms %#ok<NOPTS>
platforms(1).devices

ocl = opencl();
ocl.initialize(1,1);
ocl.addfile('C:\SonarScopeTbx\ifremer\extern\opencl-toolbox\cl/transpose_kernels.cl');
ocl.build();

%%
sizeMagic           = 4096;
sizeX               = int32(sizeMagic);
% sizeY               = int32(sizeMagic);
% BLOCKDIM            = 16;
A                   = (magic(sizeX)); %#ok<NASGU>
% hostA               = single(A);
% hostB               = zeros(size(hostA,2),size(hostA,1), 'single');
% offset              = int32(0);
fi = [0 0 0; 0 1 0; 0 0 0];
% % nElems              = zeros((BLOCKDIM + 1)*BLOCKDIM,1, 'single');
% % BLOCKDIM            = single(16);
% % block               = single((BLOCKDIM + 1)* BLOCKDIM * 4); % 4 = sizeof(float)
%%
%[A, label] = Lena;
%IM = imread('C:\SScTbxResources\images.png');
%IM = imread('C:\SScTbxResources\Desert.jpg');
%A = IM(:,:,1);
A = [1,0,0,0;0,2,0,0;0,0,3,0;0,0,0,4;5,0,0,0];
%A = zeros(sizeMagic,sizeMagic);
A = A(:,50:200);
sizeX               = int32(size(A,2));
sizeY               = int32(size(A,1));
hostA               = single(A);
hostB               = zeros(size(hostA,2),size(hostA,1), 'single');
offset              = int32(0);
BLOCKDIM            = single(16);
block               = single((BLOCKDIM + 1)* BLOCKDIM * 4); % 4 = sizeof(float)
%% transpose_naive
clear devA devB devFilter

devFilter   = clobject(single(fi));
devA        = clobject(hostA);
devB        = clobject(hostB);

%clTranspose = clkernel('transpose_naive', [16,16,0], [16,16,0]);
clTranspose = clkernel('convolution', [128,0,0], [128,0,0]);

%% transpose optim : ne fonctionne pas pour l'instant !!!!
%allouer un block de memoire locale
% clear devA devB
%
% devA        = clobject(hostA);
% devB        = clobject(hostB);
%
% clTranspose = clkernel('transpose', [64,64,0], [64,64,0]);

%% MatLab natif
tic;
Am = my_transpose(hostA, 'ForceOpenCL', false);
toc;
figure; imagesc(A); title('Image origine');
figure; imagesc(Am); title('Transpose - MatLab');
%% OpenCL
tic;
%devLocal = clbuffer('wo','local',BLOCKDIM*64*8*BLOCKDIM*64*8);
filterSize = int32(3);
clTranspose(devB, devA, devFilter, sizeX, sizeY, filterSize);
Acl = devB.get();
toc;
figure; imagesc(Acl); title('Transpose - OpenCL');


