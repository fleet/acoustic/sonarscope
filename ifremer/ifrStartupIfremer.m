% Definition des paths de la toolbox "ifremer"
%
% ifrStartupIfremer(IfrTbxIfr)
% 
% Input Arguments 
%   IfrTbxIfr : repertoire ou se trouve les developpements "Ifremer"
%
% Remarks : Nous vous conseillons d'appeler cette fonction dans votre fonction startup.m
%           qui doit se trouver dans LATLABRXX/work dans l'environnement windows ou
%           ~/matlab sous unix (Si ce repertoire n'existe pas, creez-le
%           Creez ensuite le fichier statup.m qui doit contenir
%               TmsiAsToolbox = '/Le/Repertoire/que/nous/Vous/Indiquerons/MatlabToolboxIfremer';
%               path(path, IfrTbxIfr);
%               ifrStartupIfremer(IfrTbxIfr)
% 
% Examples
%   ifrStartupIfremer(IfrTbxIfr)
%
% See also matlab_startup acoustics_startup Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function ifrStartupIfremer(IfrTbxIfr)


% Repertoire d'exemples
nomDir = fullfile(IfrTbxIfr, 'examples');
if exist(nomDir, 'dir')
    path(path, nomDir);
end

% Initialisation de la toolbox "matlab"
nomDir = fullfile(IfrTbxIfr, 'matlab');
if exist(nomDir, 'dir')
    path(path, nomDir);
    matlab_startup(IfrTbxIfr)
end

% Initialisation de la toolbox "acoustics"
nomDir = fullfile(IfrTbxIfr, 'acoustics');
if exist(nomDir, 'dir')
    path(path, nomDir);
    acoustics_startup(IfrTbxIfr)
end
