% Estimation du temps de fin d'un traitement
%
% Syntax
%   [Compteur, str] = estimation_temps_traitement(k, N, Compteur)
%
% Input Arguments
%   k        : indice de la boucle
%   N        : Nombre d'iterations
%   Compteur : Compteur des temps
%
% Output Arguments
%   Compteur : Compteur des temps
%   str      : Estimation du temps de fin des itérations
%
% Examples
%   Compteur = [];
%   N = 100;
%   for k=1:N
%       [Compteur, str] = estimation_temps_traitement(k, N, Compteur);
%       disp(sprintf('%d/%d - %s', k, N, str))
%       pause(1)
%   end
%
% See also Authors
% Authors    : JMA
% version    : $id: entete.m,v 1.2 2003/11/12 12:46:11 augustin exp $
%-------------------------------------------------------------------------------

function [Compteur, str] = estimation_temps_traitement(k, N, Compteur)

n = 10;
if k == 1
    Compteur = ones(n,1) * now;
else
    k1 = mod(k-1,n) + 1;
    k2 = mod(k,n) + 1;
    Compteur(k1) = now;
    Tend = Compteur(k1) + (N-k+1) * (Compteur(k1) - Compteur(k2)) / (n-1);
end
if k < 10
    str = 'Time estimation will be done after 10 iterations';
else
    str = ['End at ' datestr(Tend(1), 'HH:MM:SS.FFF')];
end

