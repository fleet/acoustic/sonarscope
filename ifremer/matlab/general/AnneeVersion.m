function Annee = AnneeVersion

Ver = version;
k = strfind(Ver, 'R');
Annee = Ver((k+1):(k+4));
Annee = str2double(Annee);