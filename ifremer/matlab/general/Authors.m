% Liste et references des auteurs cites dans les fonctions
%
% JMA  : Jean-Marie Augustin - Ifremer NSE-ASTI - Jean.Marie.Augustin@ifremer.fr
% RF   : Ridha Fezzani       - Ifremer NSE-ASTI - Ridha.Fezzani@ifremer.fr
% AG   : Arnauid Gaillot     - Ifremer DRO-GM   - Arnaud.Gaillot@ifremer.fr
% XL   : Xavier Lurton       - Ifremer NSE-ASTI
% YHDR : Yann-Herve De Roeck - Ifremer NSE-ASTI
% LG   : Laurent Guillon     - Thesard 1995-1998
% YL   : Yoann Ladroit       - Thesard 2008-2011
% DCF  : Denis Croize-Fillon - CDD Cosmos 2000 - dcroizef@ifremer.fr
% IK   : Imen Karaoui        - Stagiaire ENST 2003
% RG   : Roger Gallou        - Soustraitance ALTRAN-Ouest
% ROU  : Didier Riou         - Soustraitance ALTRAN-Ouest
% MHO  : Pierre Mahoudo      - Soustraitance ALTRAN-Ouest

function Authors

if nargin == 0
    help Authors %#ok<MCHLP>
end
