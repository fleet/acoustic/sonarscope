% Function to extract curves from binary edge map, if the endpoint of a
% contour is nearly connected to another endpoint, fill the gap and continue
% the extraction. The default gap size is 1 pixles.
%
% Publications:
% =============
% 1. M. Awrangjeb and G. Lu, �An Improved Curvature Scale-Space Corner Detector and a Robust Corner Matching Approach for Transformed Image Identification,� IEEE Transactions on Image Processing, 17(12), 2425�2441, 2008.
% 2. M. Awrangjeb, G. Lu, and M. M. Murshed, �An Affine Resilient Curvature Scale-Space Corner Detector,� 32nd IEEE International Conference on Acoustics, Speech, and Signal Processing (ICASSP 2007), Hawaii, USA, 1233�1236, 2007.
% 
% Better results will be found using following corner detectors:
% ==============================================================
% 1.  M. Awrangjeb, G. Lu and C. S. Fraser, �A Fast Corner Detector Based on the Chord-to-Point Distance Accumulation Technique,� Digital Image Computing: Techniques and Applications (DICTA 2009), 519-525, 2009, Melbourne, Australia.
% 2.  M. Awrangjeb and G. Lu, �Robust Image Corner Detection Based on the Chord-to-Point Distance Accumulation Technique,� IEEE Transactions on Multimedia, 10(6), 1059�1072, 2008.
% 
% Source codes available:
% =======================
% http://www.mathworks.com/matlabcentral/fileexchange/authors/39158

function [curve, curve_start, curve_end, curve_mode, cur_num, TJ] = BW2Curves(BW, Gap_size)

str1 = sprintf('L''algorithme de d�tection des coins est issu de :\nhttp://www.crcsi.com.au/getattachment/c2e6e952-0bcb-4a77-b3ac-2f1056401f07/.aspx\nhttps://www.mathworks.com/matlabcentral/fileexchange/28207-a-fast-corner-detector-based-on-the-chord-to-point-distance-accumulation-technique');
str2 = sprintf('The corner detection algorithm is coming from :\nhttp://www.crcsi.com.au/getattachment/c2e6e952-0bcb-4a77-b3ac-2f1056401f07/.aspx\nhttps://www.mathworks.com/matlabcentral/fileexchange/28207-a-fast-corner-detector-based-on-the-chord-to-point-distance-accumulation-technique');
my_warndlg(Lang(str1,str2), 0, 'Tag', 'MessageMohammadAwrangjeb1', 'TimeDelay', 10);

[L,W] = size(BW);
BW1 = false(L+2*Gap_size,W+2*Gap_size);
BW_edge = false(L,W);
% try
    BW1(Gap_size+1:Gap_size+L,Gap_size+1:Gap_size+W) = BW;
% catch ME % ME.identifier : MATLAB:nologicalnan
%     BW1(Gap_size+1:Gap_size+L,Gap_size+1:Gap_size+W) = isnan(BW);
% end
[r,c] = find(BW1); %returns indices of non-zero elements
cur_num = 0;

while size(r,1) > 0 %when number of rows > 0
    point = [r(1),c(1)];
    cur = point;
    BW1(point(1),point(2)) = false; %make the pixel black
    [I,J] = find(BW1(point(1)-Gap_size:point(1)+Gap_size,point(2)-Gap_size:point(2)+Gap_size) == 1);
    %find if any pixel around the current point is an edge pixel
    while size(I,1) > 0 %if number of row > 0
        dist = (I-Gap_size-1).^2+(J-Gap_size-1).^2;
        [~, index] = min(dist);
        p = point + [I(index),J(index)];
        point = p - Gap_size-1; % next is the current point
        cur = [cur; point]; %#ok<AGROW> %add point to curve
        BW1(point(1),point(2)) = false;%make the pixel black
        [I,J] = find(BW1(point(1)-Gap_size:point(1)+Gap_size,point(2)-Gap_size:point(2)+Gap_size) == 1);
        %find if any pixel around the current point
        %is an edge pixel
    end
    
    % Extract edge towards another direction
    point = [r(1),c(1)];
    BW1(point(1),point(2)) = false;
    [I,J] = find(BW1(point(1)-Gap_size:point(1)+Gap_size,point(2)-Gap_size:point(2)+Gap_size) == 1);
    
    while size(I,1)>0
        dist = (I-Gap_size-1).^2+(J-Gap_size-1).^2;
        [~, index] = min(dist);
        point = point+[I(index),J(index)]-Gap_size-1;
        cur = [point; cur]; %#ok<AGROW>
        BW1(point(1),point(2)) = false;
        [I,J] = find(BW1(point(1)-Gap_size:point(1)+Gap_size,point(2)-Gap_size:point(2)+Gap_size) == 1);
    end
    
    if size(cur,1)>(size(BW,1)+size(BW,2))/15 %for 512 by 512 image, choose curve if its length > 40
        cur_num = cur_num+1;
        curve{cur_num} = cur - Gap_size; %#ok<AGROW>
    end
    [r,c] = find(BW1);
end

nbCurves = length(curve);
curve_start = zeros(nbCurves,2);
curve_end   = zeros(nbCurves,2);
curve_mode  = repmat(' ', nbCurves, 4);
for i=1:cur_num
    curve_start(i,:) = curve{i}(1,:);
    curve_end(i,:) = curve{i}(size(curve{i},1),:);
    if (curve_start(i,1)-curve_end(i,1))^2+...
            (curve_start(i,2)-curve_end(i,2))^2 <= 32  %if curve's ends are within sqrt(32) pixels
        curve_mode(i,:) = 'loop';
    else
        curve_mode(i,:) = 'line';
    end
    BW_edge(curve{i}(:,1)+(curve{i}(:,2)-1)*L) = true;
end

TJ = Curves2TJunctions(curve, cur_num, Gap_size+1); % if a contour goes just outsize of ends, i.e., outside of gapsize
