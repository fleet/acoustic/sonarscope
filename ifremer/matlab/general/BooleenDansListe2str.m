% Affichage d'un choix boolen avec des accolades autour de l'item selectionne
%
% Syntax
%   str = BooleenDansListe2str(choix)
%   str = BooleenDansListe2str(choix, liste)
% 
% Input Arguments 
%   choix : Item selectionn
%   liste : Signification du 0 et du 1
%
% Output Arguments
%   str : Chaine de caractere.
% 
% Examples
%   str = BooleenDansListe2str( 0 )
%   str = BooleenDansListe2str( 1 )
%   str = BooleenDansListe2str( 0, {'Non';'Oui'} )
%
% See also SelectionDansListe2str Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function str = BooleenDansListe2str(choix, varargin)

if nargin == 1
   str1 = 'off';
   str2 = 'on';
else
   liste = varargin{1};
   str1 = liste{1};
   str2 = liste{2};
end

if isempty(choix)
    str = sprintf('0=%s | 1=%s', str1, str2);
else
    if choix
        str = sprintf('0=%s | {1=%s}', str1, str2);
    else
        str = sprintf('{0=%s} | 1=%s', str1, str2);
    end
end
