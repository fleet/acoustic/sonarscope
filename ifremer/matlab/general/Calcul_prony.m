%	Modelisation de prony(ordre P) d'un signal Signal_depart 
%
%	ENTREES
%         Signal_depart          : signal original => Vecteur de donnees
%         ordre                  : ordre du modele pour la modelisation
%         seuil_amplitude
%         seuil_pulsation        : (ex 0.05,0.8) : seuils qui permettent de rejeter des modes non significatifs
%
%   SORTIES
%         parametres_correlation : matrice de parametres. Il y a autant de ligne que de modes detectes
%		                           parametres_correlation(:,i_col) = [ amplitude amortissement pulsation phase]
%
%   DATE
%         Janvier 98
% -----------------------------------------------------------------------------------------------

function parametres_correlation = Calcul_prony(Signal_depart, ordre, seuil_amplitude, seuil_pulsation )

Signal_non_nul = min(Signal_depart ~= zeros(size(Signal_depart)));

if Signal_non_nul
    
%     parametres_correlation = [];
    Signal_depart = Signal_depart(:)';
    P             = ordre;                      % changement de notation
    N             = length(Signal_depart);       % Nombre d'echantillons
    continu       = 1;
    
    % ---------------------------------------------------------------------------------------------------------
    % Estimation de la covariance => on est oblige de rajouter un peu de bruit pour rendre la matrice inversible
    
    l_boucle = 1;
    
    % ---------------------------------------------
    % Calcul de la covariance par la methode de Kay
    
    while continu
        l_boucle = l_boucle + 1;
%         covariance = zeros(P+1, P+1);
        Matrice_transitoire = zeros(N-P, P+1);
        
        for i_col = 1 : P+1
            Matrice_transitoire( : , i_col ) = Signal_depart( P+2 - i_col : N+1 - i_col)';
        end
        
        covariance        = Matrice_transitoire' * Matrice_transitoire ;
        % On enleve la premiere ligne et premiere colonne
        covariance_reduit = covariance(2:P+1,2:P+1); 
        
        % on regarde si la matrice de covariance est bien conditionnee
        
        if (rcond(covariance_reduit) > 1e-12)
            
            C_inv   = inv(covariance_reduit);
            continu = 0;
            
        else
            
            % si la matrice est non inversible, la solution consiste a la bruiter legerement 
            % pour extraire le point d'instabilite
            
            bruit         = normrnd(0,1,1,length(Signal_depart));
            Signal_depart = Signal_depart + bruit * sqrt(var(Signal_depart))*0.001; 
            continu       = 1;
            
            if l_boucle > 10
                continu   = 0; 
                C_inv     = zeros(P,P); 
            end
        end	
        
        
    end
    
    % ------------------------------------
    % Fin de l estimation de la covariance
    
    ligne           = - covariance(2:P+1,1);
    coefficients_AR = [1 (C_inv * ligne)' ];
    R               = roots(coefficients_AR);             % Racines complexes
    
    % ---------------------------------
    % Amortissement et pulsation propre
    
    Module   = max( abs(R), 1e-50);
    amp_R    = - 1 ./ log( Module );                      % indice de l amortissement
    W_estime = angle(R);	                              % indice d'une pulsation propre
    
    % ----------------------------------------------
    % Phase et amplitude : matrice de Van der monde
    
    taille        = length(Signal_depart);
    Van_der_monde = zeros(taille,P);
    
    for i_ligne = 1:taille
        Van_der_monde(i_ligne, :) = R' .^ (i_ligne-1);
    end
    
    % -----------------------------------------
    % Inversion de la matrice de Van der Monde
    
    Matrice_tampon        = Van_der_monde' * Van_der_monde;
    Taille_Matrice_tampon = size(Matrice_tampon, 1);
    l_boucle              = 1;
    
    while (rcond(Matrice_tampon) < 1e-12) && (l_boucle < 10)
        variance = var( Matrice_tampon(:) );
        Matrice_tampon = Matrice_tampon + normrnd( 0 , 1, Taille_Matrice_tampon, Taille_Matrice_tampon) * sqrt(variance)*0.001;
        l_boucle = l_boucle +1;
    end
    
    B = zeros( ordre, 1 );
    
    if l_boucle < 10
        B = (inv(Matrice_tampon) * Van_der_monde') * Signal_depart';
    end
    
    amp_B                    = ( abs(B) ./ Module );               % Attention au decalage d un echantillon a l origine
    indice_complexe          = find( imag(R) ~= 0);
    amp_B( indice_complexe ) = 2* amp_B( indice_complexe );               % Dans le cas de racines complexes conjuguees
    phase_B                  = -(angle(B) + angle(R));              % Attention au decalage d un echantillon a l origine
    
    % -----------------------------------------------
    % Detection des modes significatifs => seuillages
    
    Indice_valable       = ones( 1, length(R) );
    
    Indice_amplitude     = find( amp_B < seuil_amplitude );
    Indice_amortissement = find( amp_R <= 0 ); % Amortissement negatifs
    Indice_pulsation     = find( abs(W_estime) > seuil_pulsation );
    
    Indice_valable( Indice_amplitude )     = zeros( 1, length(Indice_amplitude) );
    Indice_valable( Indice_pulsation )     = zeros( 1, length(Indice_pulsation) );
    Indice_valable( Indice_amortissement ) = zeros( 1, length(Indice_amortissement) );
    
    % ----------------------------------------
    %  Traitement des modes complexes et reels
    
    taille                 = length(R); 
    continu                = 1 ; 
    ii                     = 0 ; 
    mode_significatif      = 0 ;
    parametres_correlation = [];
    
    while continu
        ii = ii + 1;
        if Indice_valable(ii) ~= 0
            C_complexe    = imag  (R(ii)) ~= 0 ;
            pulsation     = W_estime(ii) * C_complexe;
            amortissement = amp_R   (ii);
            phase         = phase_B (ii) * C_complexe;
            amplitude     = amp_B   (ii);
            
            mode_significatif = mode_significatif + 1;
            parametres_correlation(mode_significatif, 1:4) = [amplitude amortissement pulsation  phase]; %#ok<AGROW>
            
            if( C_complexe == 1) 
                ii = ii + 1; 
            end
        end
        
        if ii >= taille
            continu = 0; 
        end
    end %while
    
    if isempty(parametres_correlation)
        parametres_correlation = [0 0 0 0]; 
    end
else % Signal non nul
    parametres_correlation = [0 0 0 0];
end
