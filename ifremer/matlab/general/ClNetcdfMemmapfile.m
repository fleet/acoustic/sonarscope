% TODO : cette class �tait un essai d'acc�l�ration des lecture Netcdf mais
% �a n'a pas �t� probant

classdef ClNetcdfMemmapfile < handle
    % Summary of this class goes here
    %   Detailed explanation goes here
    %
    %   ncFileName = 'D:\Temp\ExData\ExRDF\SonarScope\ESSTECH19020.nc';
    %   [flag, Data, Bin] = NetcdfUtils.readGrpData(ncFileName, 'Sample', 'Memmapfile', 0);
    %   figure; imagesc(Data.Depth(:,:)); colormap(jet(256)); colorbar;
    %
    %   [flag, Data, Bin] = NetcdfUtils.readGrpData(ncFileName, 'Sample', 'Memmapfile', 1);
    %   figure; imagesc(Data.Depth(:,:)); colormap(jet(256)); colorbar;
    %
    %   [flag, Data, Bin] = NetcdfUtils.readGrpData(ncFileName, 'Sample', 'Memmapfile', -3);
    %   Data.Depth = get(Data.Depth);
    %   figure; imagesc(Data.Depth(:,:)); colormap(jet(256)); colorbar;
    
    properties
        ncFileName
        grpID
        grpName
        varID
        varName
        varSize
        varType
        Unit
        Memmapfile
    end
    
    methods
        function this = ClNetcdfMemmapfile(ncFileName, grpName, grpID, varName, varID, varSize, varType, Unit, Memmapfile)
            this.ncFileName = ncFileName;
            this.grpName    = grpName;
            this.grpID      = grpID;
            this.varName    = varName;
            this.varID      = varID;
            this.varSize    = varSize;
            this.varType    = varType;
            this.Unit       = Unit;
            this.Memmapfile = Memmapfile;
        end
        
        function Value = subsref(this, theStruct)
            global sessionId %#ok<GVMIS> 

            switch theStruct.type
                case '()'
                    [~, fileName] = fileparts(this.ncFileName);
                    fileNameMemmapfile = fullfile(my_tempdir, [fileName '_' this.grpName '_' this.varName '_' num2str(sessionId) '.memmapfile']);
                    if exist(fileNameMemmapfile, 'file')
                        Value = cl_memmapfile('FileName', fileNameMemmapfile, 'Size', this.varSize, ...
                            'FileCreation', 0, 'Writable', false, ...
                            'Format', this.varType, 'DoNotDelete2', true);
                    else
                        ncID = netcdf.open(this.ncFileName);
                        Value = netcdf.getVar(this.grpID, this.varID);
                        netcdf.close(ncID);
                        cl_memmapfile('FileName', fileNameMemmapfile, 'Value', Value', 'DoNotDelete2', true);
                    end
                    
                    nbSub = length(theStruct.subs);
                    for k=1:length(theStruct.subs)
                        subsk = theStruct.subs{k};
                        n = size(Value,k);
                        if isequal(subsk, ':')
                            if nbSub == 1
                                Value = Value(:);
                                return
                            else
                                sub{k} = 1:n; %#ok<AGROW>
                            end
                        else
                            sub{k} = subsk; %#ok<AGROW>
                        end
                    end
                    Value = Value(sub{:});
                otherwise
                    Value = [];
            end
        end
        
        
        function sz = size(this, varargin)
            if isempty(varargin)
                sz = this.varSize;
            else
                sz = this.varSize(varargin{:});
            end
        end
        
        
        function sz = length(this)
        	sz = max(this.varSize);
        end
        
        
        function sz = numel(this)
        	sz = prod(this.varSize);
        end
    end
end
