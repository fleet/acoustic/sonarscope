% Curves2Index
%
% Publications:
% =============
% 1. M. Awrangjeb and G. Lu, “An Improved Curvature Scale-Space Corner Detector and a Robust Corner Matching Approach for Transformed Image Identification,” IEEE Transactions on Image Processing, 17(12), 2425–2441, 2008.
% 2. M. Awrangjeb, G. Lu, and M. M. Murshed, “An Affine Resilient Curvature Scale-Space Corner Detector,” 32nd IEEE International Conference on Acoustics, Speech, and Signal Processing (ICASSP 2007), Hawaii, USA, 1233–1236, 2007.
% 
% Better results will be found using following corner detectors:
% ==============================================================
% 1.  M. Awrangjeb, G. Lu and C. S. Fraser, “A Fast Corner Detector Based on the Chord-to-Point Distance Accumulation Technique,” Digital Image Computing: Techniques and Applications (DICTA 2009), 519-525, 2009, Melbourne, Australia.
% 2.  M. Awrangjeb and G. Lu, “Robust Image Corner Detection Based on the Chord-to-Point Distance Accumulation Technique,” IEEE Transactions on Multimedia, 10(6), 1059–1072, 2008.
% 
% Source codes available:
% =======================
% http://www.mathworks.com/matlabcentral/fileexchange/authors/39158

function [index, S, curveAL, IND, Curvature] = Curves2Index(curve, curve_mode, curve_num)

[index, S, curveAL, IND, Curvature] = Curves2Index_A(curve, curve_mode, curve_num);

%{
% TODO ! Tentative de retournement des courbes pour que l'algo trouve la
% fin des coins au lieu du début. Si ça avait fonctionné on aurait pu faire
% l'union des deux
for k=1:length(curve)
    curve{k} = flipud(curve{k});
end
[index, S, curveAL, IND, Curvature] = Curves2Index_A(curve, curve_mode, curve_num);
for k=1:length(curve)
    N1 = length(IND{k});
    index{k}     = flipud((N1+1) - index{k});
    curveAL{k}   = flipud(curveAL{k});
    N2 = length(curve{k});
    IND{k}       = flipud((N2+1) - IND{k});
    Curvature{k} = flipud(Curvature{k});
end
%}

function [index, S, curveAL, IND, Curvature] = Curves2Index_A(curve, curve_mode, curve_num)

GFP{1} = [300 5 0.03]; % col1 = affine-length, col2 = sigma, col3 = Threshold
GFP{2} = [250 4 0.03];
GFP{3} = [200 3 0.03];

%GFP{1} = [300 7 0.02]; % col1 = affine-length, col2 = sigma, col3 = Threshold
%GFP{2} = [250 6 0.03];
%GFP{3} = [200 5 0.04];
%GFP{4} = [150 4 0.05];
%GFP{5} = [100 3 0.06];
%GFP{6} = [50 2 0.07];
%GFP{7} = [4 1 0.08];

[GF, width] = makeGFilter();
curveAL = cell(curve_num,1);
IND     = cell(curve_num,1);
S       = zeros(curve_num,1);
for k=1:curve_num
    x = curve{k}(:,1);
    y = curve{k}(:,2);
    L = size(x,1);
    [xL, yL, L_aff, ind] = affine_length(x, y, L);
    curveAL{k} = [xL yL];
    IND{k} = ind;
    if L_aff > GFP{1}(1,1)
        gau = GF{1};
        W = width(1,1);
        Thresh = GFP{1}(1,3);
        S(k,1) = GFP{1}(1,2);
    elseif L_aff > GFP{2}(1,1)
        gau = GF{2};
        W = width(2,1);
        Thresh = GFP{2}(1,3);
        S(k,1) = GFP{2}(1,2);
    else %if L_aff>GFP{3}(1,1)
        gau = GF{3};
        W = width(3,1);
        Thresh = GFP{3}(1,3);
        S(k,1) = GFP{3}(1,2);
        %elseif L_aff>GFP{4}(1,1)
        %    gau = GF{4};
        %    W = width(4,1);
        %    Thresh = GFP{4}(1,3);
        %    S(k,1) = GFP{4}(1,2);
        %elseif L_aff>GFP{5}(1,1)
        %    gau = GF{5};
        %    W = width(5,1);
        %    Thresh = GFP{5}(1,3);
        %    S(k,1) = GFP{5}(1,2);
        %elseif L_aff>GFP{6}(1,1)
        %    gau = GF{6};
        %    W = width(6,1);
        %    Thresh = GFP{6}(1,3);
        %    S(k,1) = GFP{6}(1,2);
        %else
        %    gau = GF{7};
        %    W = width(7,1);
        %    Thresh = GFP{7}(1,3);
        %    S(k,1) = GFP{7}(1,2);
    end
    if L_aff > W
        if strcmp(curve_mode(k,:), 'loop') % wrap around the curve by W pixles at both ends
            x1 = [xL(L_aff-W+1:L_aff) ; xL;xL(1:W)];
            y1 = [yL(L_aff-W+1:L_aff) ; yL;yL(1:W)];
        else % extend each line curve by W pixels at both ends
            x1 = [ones(W,1)*2*xL(1)-xL(W+1:-1:2) ; xL ; ones(W,1)*2*xL(L_aff)-xL(L_aff-1:-1:L_aff-W)];
            y1 = [ones(W,1)*2*yL(1)-yL(W+1:-1:2) ; yL ; ones(W,1)*2*yL(L_aff)-yL(L_aff-1:-1:L_aff-W)];
        end
        
        xx = conv(x1,gau);
        xx = xx(W+1:L_aff+3*W);
        yy = conv(y1,gau);
        yy = yy(W+1:L_aff+3*W);
        
        Xu =   [xx(2)-xx(1) ;   (xx(3:L_aff+2*W)-xx(1:L_aff+2*W-2))/2 ;   xx(L_aff+2*W)-xx(L_aff+2*W-1)];
        Yu =   [yy(2)-yy(1) ;   (yy(3:L_aff+2*W)-yy(1:L_aff+2*W-2))/2 ;   yy(L_aff+2*W)-yy(L_aff+2*W-1)];
        Xuu =  [Xu(2)-Xu(1) ;   (Xu(3:L_aff+2*W)-Xu(1:L_aff+2*W-2))/2 ;   Xu(L_aff+2*W)-Xu(L_aff+2*W-1)];
        Yuu =  [Yu(2)-Yu(1) ;   (Yu(3:L_aff+2*W)-Yu(1:L_aff+2*W-2))/2 ;   Yu(L_aff+2*W)-Yu(L_aff+2*W-1)];
        Xuuu = [Xuu(2)-Xuu(1) ; (Xuu(3:L_aff+2*W)-Xuu(1:L_aff+2*W-2))/2 ; Xuu(L_aff+2*W)-Xuu(L_aff+2*W-1)];
        Yuuu = [Yuu(2)-Yuu(1) ; (Yuu(3:L_aff+2*W)-Yuu(1:L_aff+2*W-2))/2 ; Yuu(L_aff+2*W)-Yuu(L_aff+2*W-1)];
        
        a = Xu.*Yuu - Xuu.*Yu;
        b = Xuuu.*Yu - Xu.*Yuuu;
        
        Xt = Xu ./ (a.^(1/3));
        Yt = Yu ./ (a.^(1/3));
        Xtt = ((Xu.*b)./(3*(a.^(5/3)))) + (Xuu./(a.^(2/3)));
        Ytt = ((Yu.*b)./(3*(a.^(5/3)))) + (Yuu./(a.^(2/3)));
        
        K = abs((Xt.*Ytt-Xtt.*Yt) ./ ((Xt.*Xt+Yt.*Yt).^1.5));
        K = ceil(K*100) / 100;
        
        % Find curvature local maxima as corner candidates
        extremum = [];
        N = size(K,1);
        n = 0;
        Search = 1;
        
        for j=1:N-1
            if (K(j+1)-K(j))*Search>0
                n = n+1;
                extremum(n) = j;  %#ok<AGROW> % In extremum, odd points are minima and even points are maxima
                Search = -Search; % minima: when K starts to go up; maxima: when K starts to go down
            end
        end
        if mod(size(extremum,2),2) == 0 %to make odd number of extrema
            n = n+1;
            extremum(n) = N; %#ok<AGROW>
        end
        
        n = size(extremum,2);
        flag = true(size(extremum));
        
        % Compare each maxima with its two local minima to remove false corners
        for j=2:2:n % if the maxima is less than local minima, remove it as flase corner
            if (K(extremum(j)) < 2*K(extremum(j-1))) ||( K(extremum(j)) < 2*K(extremum(j+1)))
                flag(j) = false;
            end
        end
        extremum = extremum(2:2:n); % only maxima are corners, not minima
        flag = flag(2:2:n);
        extremum = extremum(flag);
        
        % Compare each selected maxima with global Thresh to remove round
        % corners
        n = size(extremum,2);
        flag = true(size(extremum));
        
        % Compare each maxima with its two local minima to remove false corners
        for j=1:n % if the maxima is less than local minima, remove it as flase corner
            if (K(extremum(j)) <= Thresh)
                flag(j) = false;
            end
        end
        extremum = extremum(flag);
        extremum = extremum - W;
        extremum = extremum((extremum > 0) & (extremum <= L_aff)); % find corners which are not endpoints of the curve
        %index{k} = ind(extremum);
        Curvature{k} = K(extremum+W); %#ok<AGROW>
        index{k} = extremum'; %#ok<AGROW>
    else
        Curvature{k} = []; %#ok<AGROW>
        index{k} = []; %#ok<AGROW>
    end
    
end


function [G, W] = makeGFilter()

GaussianDieOff = .0001;
pw = 1:100;
sig = 5;
for k=1:3
    ssq = sig*sig;
    width = find((exp(-(pw.*pw)/(2*ssq)) > GaussianDieOff), 1, 'last');
    if isempty(width)
        width = 1;
    end
    t = (-width:width);
    gau = exp(-(t.*t)/(2*ssq))/(2*pi*ssq);
    gau = gau/sum(gau);
    G{k} = gau; %#ok<AGROW>
    W(k,1) = width; %#ok<AGROW>
    sig = sig - 1;
end


function [xl, yl, L_aff, ind] = affine_length(x, y, L)

xu = [x(2)-x(1) ; (x(3:L)-x(1:L-2))/2 ; x(L)-x(L-1)];
yu = [y(2)-y(1) ; (y(3:L)-y(1:L-2))/2 ; y(L)-y(L-1)];
xuu = [xu(2)-xu(1) ; (xu(3:L)-xu(1:L-2))/2 ; xu(L)-xu(L-1)];
yuu = [yu(2)-yu(1) ; (yu(3:L)-yu(1:L-2))/2 ; yu(L)-yu(L-1)];

L_aff = floor(abs(sum((xu.*yuu - xuu.*yu) .^ (1/3))));
al     = 0;
j      = 1;
seg_al = 1;
t      = 1;
xl     = [];
yl     = [];
ind    = [];
for k=1:L
    al = al + (xu(k,1)*yuu(k,1)-xuu(k,1)*yu(k,1))^(1/3); % Rayon de courbure
    if (abs(al) - t) >= 0
        xl(j,1) = x(k,1); %#ok<AGROW>
        yl(j,1) = y(k,1); %#ok<AGROW>
        ind(j,1) = k; %#ok<AGROW>
        j = j+1;
        t = seg_al*j;
    end
end
%if L_aff>size(ind,1)
%    xl(L_aff,1) = x(L,1);
%    yl(L_aff,1) = y(L,1);
%    ind(L_aff,1) = L;
%end
