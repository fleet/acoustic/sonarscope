% find T-junctions within (gap by gap) neighborhood, where gap = Gap_size +
% 1; edges were continued (see edge_extraction procedure) when ends are within (Gap_size by Gap_size)
%
% Publications:
% =============
% 1. M. Awrangjeb and G. Lu, �An Improved Curvature Scale-Space Corner Detector and a Robust Corner Matching Approach for Transformed Image Identification,� IEEE Transactions on Image Processing, 17(12), 2425�2441, 2008.
% 2. M. Awrangjeb, G. Lu, and M. M. Murshed, �An Affine Resilient Curvature Scale-Space Corner Detector,� 32nd IEEE International Conference on Acoustics, Speech, and Signal Processing (ICASSP 2007), Hawaii, USA, 1233�1236, 2007.
% 
% Better results will be found using following corner detectors:
% ==============================================================
% 1.  M. Awrangjeb, G. Lu and C. S. Fraser, �A Fast Corner Detector Based on the Chord-to-Point Distance Accumulation Technique,� Digital Image Computing: Techniques and Applications (DICTA 2009), 519-525, 2009, Melbourne, Australia.
% 2.  M. Awrangjeb and G. Lu, �Robust Image Corner Detection Based on the Chord-to-Point Distance Accumulation Technique,� IEEE Transactions on Multimedia, 10(6), 1059�1072, 2008.
% 
% Source codes available:
% =======================
% http://www.mathworks.com/matlabcentral/fileexchange/authors/39158

function TJ = Curves2TJunctions(curve, cur_num, gap) % finds T-Junctions in planar curves
TJ = [];
gap2 = gap * gap;
for k1=1:cur_num
    cur = curve{k1};
    szi = size(cur,1);
    for k2=1:cur_num
        if k1 ~= k2
            temp_cur = curve{k2};
            compare_send = temp_cur - ones(size(temp_cur, 1),1) * cur(1,:);
            compare_send = compare_send .^ 2;
            compare_send = compare_send(:,1) + compare_send(:,2);
            if min(compare_send) <= gap2       % Add curve strat-points as T-junctions using a (gap by gap) neighborhood
                TJ = [TJ; cur(1,:)]; %#ok<AGROW>
            end
            
            compare_eend = temp_cur - ones(size(temp_cur, 1),1) * cur(szi,:);
            compare_eend = compare_eend .^ 2;
            compare_eend = compare_eend(:,1) + compare_eend(:,2);
            if min(compare_eend) <= gap2      % Add end-points T-junctions using a (gap by gap) neighborhood
                TJ = [TJ; cur(szi,:)]; %#ok<AGROW>
            end
        end
    end
end
