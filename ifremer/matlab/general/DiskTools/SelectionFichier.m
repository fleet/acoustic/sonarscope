% Sélection d'un fichier dans l'arborescence des répertoires
%
% Syntax
%   nomFic = SelectionFichier(filtre)
%
% Input Arguments 
%   filtre : Filtre des fichiers
%
% Output Arguments
%   nomFic : Liste des indices sélectionnés
%
% Examples
%   nomDir = fileparts(getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv'))
%   rep = SelectionFichier(nomDir)
%   rep = SelectionFichier(fullfile(nomDir,'*csv'))
%   rep = SelectionFichier('')
%   rep = SelectionFichier(my_tempdir)
%
% See also my_uigetfile Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function nomFic = SelectionFichier(filtre)

if nargin == 0
    return
end

% Test si le nom de fichier est correct

if isfolder(filtre)
    [flag, nomFic] = my_uigetfile(fullfile(filtre, '*'));
    if ~flag
        return
    end
else
    fid = fopen(filtre, 'r');
    if fid == -1
        [flag, nomFic] = my_uigetfile(filtre);
        if ~flag
            return
        end
    else
        nomFic = filtre;
        fclose(fid);
    end
end





