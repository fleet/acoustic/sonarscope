% Copie d'un fichier texte en fin d'un autre fichier texte
%
% Syntax
%   append_file(nomFic1, nomFic2, ...)
%
% Input Arguments
%   nomFic1 : Nom du fichier � copier dans nomFic2
%   nomFic2 : Nom du fichier r�cepteur
%
% Name-Value Pair Arguments
%   SkipBegin : Nombre de lignes a sauter en d�but de fichier nomFic1
%   SkipEnd   : Nombre de lignes a sauter en fin de fichier nomFic1
%
% Examples
%   append_file(nomFic1, nomFic2)
%
% See also write_LogFile Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function append_file(nomFic1, nomFic2, varargin)

[varargin, SkipBegin] = getPropertyValue(varargin, 'SkipBegin', 0);
[varargin, SkipEnd]   = getPropertyValue(varargin, 'SkipEnd',   0); %#ok<ASGLU>

fid2 = fopen(nomFic2, 'r');
if fid2 == -1
    nbEspaces = 0;
else
    fseek(fid2, -1000, 'eof');
    while 1
        tline = fgetl(fid2);
        if ~ischar(tline)
            break
        end
        LastLine = tline;
    end
    fclose(fid2);
    
    ind = strfind(' ', LastLine);
    nbEspaces = length(ind);
    if ~contains(LastLine, '</')
        nbEspaces = nbEspaces + 1;
    end
end



fid1 = fopen(nomFic1, 'r');
if fid1 == -1
    return
end
Lines = {};
while 1
    tline = fgetl(fid1);
    if ~ischar(tline), break, end
    Lines{end+1} = tline; %#ok<AGROW>
end
fclose(fid1);
Lines = Lines(SkipBegin+1:length(Lines)-SkipEnd);

Tab = sprintf('\t');
fid2 = fopen(nomFic2, 'a+');
if fid2 == -1
    str1 = sprintf('Impossible d''�crire le fichier "%s", v�rifier ses droits en �criture (Il se peut qu''il ait �t� cr�� sous le compte administrateur, dans ce cas logguez vous "Administrateur" et supprimez le).', nomFic2);
    str2 = sprintf('It is impossible to create file "%s", check its writing permission (it is possible this file has been created with "Administrator" logging,  if it is the case log again administrator and delete it.', nomFic2);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SonarScope.logNonWritable');
    return
end
for k1=1:length(Lines)
    for k2=1:nbEspaces
        fprintf(fid2, '%s', ' ');
    end
    ind = strfind(Tab, Lines{k1});
    if ~isempty(ind)
        Lines{k1}(ind) = ' ';
    end
    fprintf(fid2, '%s\n', Lines{k1});
end
fclose(fid2);
