 % Vérification si il y a assez d'espace sur le disque pour traiter un fichier
 %
 % Syntax
 %   flag = checkSpaceBeforeProcessing(nomFic, Coef)
 % 
 % Input Arguments 
 %   nomFic : Nom du fichier
 %   Coef   : Taille libre sur disque (N * Taille du fichier)
 %
 % Output Arguments 
 %   flag : 1=OK, 0 = KO
 % 
 % Examples
 %   nomFic = getNomFicDatabase('0034_20070406_081642_raw.all')
 %   flag = checkSpaceBeforeProcessing(nomFic, 2)
 %
 % See also sizeFic getDiskFreeSpace_mex Authors
 % Authors    : JMA + GLU
 % version    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin exp $
 %-------------------------------------------------------------------------------


function flag = checkSpaceBeforeProcessing(nomFic, Coef)
        
nbOcFichier = sizeFic(nomFic);
 
nomDir = fileparts(nomFic);
[~, FreeSize] =  getDiskFreeSpace_mex(nomDir);
 FreeSize = FreeSize * 1024^2;
 
 flag = ((nbOcFichier * Coef) <= FreeSize);

