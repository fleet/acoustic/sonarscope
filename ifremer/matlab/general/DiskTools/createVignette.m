% Constitution d'images miniatures
% On fait correspondre a chaque fichier image un fichier jpeg en faisant
% une reduction de donnees par moyennage.
%
% Syntax
%   createVignette(w, ...)
%
% Input Arguments
%   w : Liste des fichiers a traiter
%
% Name-Value Pair Arguments
%   reduc  : Coefficient de reduction de la donnee (4 par defaut)
%   repJpg : Repertoire de stockage des fichiers jpeg (miniatures)
%
% Name-only Arguments
%   Segmentation :
%
% Examples
%   w = listeFicOnDir('/home/fresnel/Titanic/Tif', '.tif')
%   createVignette(w, 'repJpg', '/tmp')
%
% See also listeFicOnDir createVignette LecFicSarSpeedy Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function createVignette(w, varargin)

% -----------------------------------------------
% Cas ou on donne le nom du fichier en caracteres

if ~iscell(w)
    w = {w};
end

% -----------------------------------------
% Coefficient de reduction de la resolution

[varargin, reduc] = getPropertyValue(varargin, 'reduc', 4);
[varargin, Segmentation] = getFlag(varargin, 'Segmentation');

% ---------------------------------------
% Répertoire de stockage des fichiers tif

rep = fileparts(w{1});
[varargin, repJpg] = getPropertyValue(varargin, 'repJpg', rep); %#ok<ASGLU>

% -----------------------
% Traitement des fichiers

N = length(w);
hw = create_waitbar('Processing', 'N', N);
for i=1:N
    my_waitbar(i, N, hw)
    [~, nomFic, ext] = fileparts(w{i});
    
    fprintf('%d/%d %s\n', i, length(w), [nomFic ext]);
    
    [I, map] = imread(w{i});
    
    if Segmentation
        if reduc ~= 1
            I = downsize(I, 'pasy', reduc, 'pasx', reduc);
        end
        nomFicJpg = fullfile(repJpg, [nomFic '.jpg'] );
        imwrite(uint8(I), map, nomFicJpg, 'jpg')
    else
        if reduc ~= 1
            I = downsize(I, 'pasy', reduc, 'pasx', reduc, 'Method', 'mean');
        end
        sub = I >= 20;
        Resultats = stats(I(sub), 'Seuil', [2 98]);
        
        Imin = Resultats(8);
        Imax = Resultats(9);
        I = (I - Imin) / (Imax-Imin);
        
        nomFicJpg = fullfile(repJpg, [nomFic '.jpg'] );
        blanc = ones(size(I,1), 50);
        imwrite([1-I blanc I], nomFicJpg, 'jpg')
    end
    
    %             imagesc(I); colormap(flipud(gray));
end
my_close(hw, 'MsgEnd')
