% Copie d'une liste de fichiers sur un repertoire
%
% Syntax
%   exportFiles(liste, rep)
% 
% Input Arguments 
%   liste : Liste des fichiers
%   rep   : Repertoire ou doivent etre copies les fichiers 
%
% Examples
%   liste = listeMFiles('celeriteChen', 'TrajetRayon');
%   for i=1:length(liste), liste{i}, end
%   exportFiles(liste, my_tempdir)
%
% See also listeMFiles Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function exportFiles(liste, rep)

if nargin == 0
	return
end
  
if exist(rep) ~= 7 %#ok<EXIST>
    my_warndlg('Le repertoire specifie n''existe pas', 1);
    return
end

for i=1:length(liste)
    [status, msg] = copyfile(liste{i}, rep);
    if ~status
        fprintf('Pb Fichier %s : %s', liste{i}, msg);
    end
end