% Liste des sous-repertoires presents dans un repertoire
%
% Syntax
%   listeNomDir = findDirInDir(Rep)
%
% Input Arguments 
%   Rep    : Repertoire
%
% Output Arguments
%   listeNomDir : Repertoires trouves.
% 
% Examples
%   listeNomDir = findDirInDir(my_tempdir)
%
% See also dir findDirInDir Authors
% Authors : JMA
%-------------------------------------------------------------------------------


function listeNomDir = findDirInDir(Rep)

liste = dir(Rep);

k = 0;
listeNomDir = [];
for i=1:length(liste)
    switch liste(i).name
    case {'.'; '..'}
    otherwise
        nomrep = fullfile(Rep, liste(i).name);
        if isfolder(nomrep)
            k = k + 1;
            listeNomDir{k, 1} = nomrep; %#ok<AGROW>
        end
    end
end
