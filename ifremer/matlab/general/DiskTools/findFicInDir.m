% Liste des fichiers presents dans un repertoire.
%
% Syntax
%   listeNomFic = findFicInDir(Rep)
%   listeNomFic = findFicInDir(Rep, filtre)
%
% Input Arguments
%   Rep    : Répertoire
%   Filtre : Filtre
%
% Output Arguments
%   listeNomFic : Fichiers trouves.
%
% Examples
%   listeFic = findFicInDir(fullfile(SonarScopeData, 'Public'), 'texture*.png')
%
% See also listeFicOnDir listeDirOnDir Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function listeNomFic = findFicInDir(Rep, varargin)

[varargin, Exclude] = getPropertyValue(varargin, 'Exclude', []);

if isempty(varargin)
    if isfolder(Rep)
        repertoire = Rep;
    else
        repertoire = fileparts(Rep);
    end
else
    repertoire = Rep;
    Rep = fullfile(Rep, varargin{1});
end


liste = dir(Rep);

if isempty(liste)
    listeNomFic = [];
else
    listeNomFic = {};
    for k=1:length(liste)
        if isempty(Exclude)
            listeNomFic{end+1, 1} = fullfile(repertoire, liste(k).name); %#ok<AGROW>
        else
            [~, nomFic, ext] = fileparts(liste(k).name);
            if strcmp(Exclude(1), '.')
                if ~strcmpi(ext, Exclude)
                    listeNomFic{end+1, 1} = fullfile(repertoire, liste(k).name); %#ok<AGROW>
                end
            else
                nomFic2 = [nomFic ext];
                if ~contains(nomFic2, Exclude)
                    listeNomFic{end+1, 1} = fullfile(repertoire, liste(k).name); %#ok<AGROW>
                end
            end
        end
    end
end
