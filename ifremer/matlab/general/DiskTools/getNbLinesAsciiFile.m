% Get the number of lines of an ASCII file
%
% Syntax
%   N = getNbLinesAsciiFile(nomFic)
%
% Input Arguments
%   nomFic : File name of the ASCII file
%
% Output Arguments
%   N : Number of lines
%
% Examples 
%   nomFic = which('startup')
%	nbOc = getNbLinesAsciiFile(nomFic)
%
% See also sizeFic Authors 
% Authors : JMA
%--------------------------------------------------------------------------------

function n = getNbLinesAsciiFile(nomFic)

n = 0;
if ~exist(nomFic, 'file')
    return
end

fid = fopen(nomFic, 'r');
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    n = n + 1;
end

fclose(fid);


