% Identification du format d'un fichier
%
% Syntax
%   [flag, typeFichier, SonarIdent] = identifyFileFormat(nomFic, SonarIdent)
%
% Input Arguments
%   nomFic     : Nom du fichier
%   SonarIdent : Identification du sonar si connu
%
% Output Arguments
%   flag        :
%   typeFichier :
%   SonarIdent  :
%
% Examples
%   nomFic = getNomFicDatabase('Aslanian.grd');
%   nomFic = getNomFicDatabase('EM12D_Ex.mbb');
%   nomFic = getNomFicDatabase('DF1000_ExStr.imo')
%   nomFic = getNomFicDatabase('EM1000_rectiligne_1.2.imo')
%   nomFic = getNomFicDatabase('EM300_Ex2.mnt')
%   nomFic = getNomFicDatabase('EM1002_MARCROCHE_Profil1_MOS.imo')
%   [typeFichier, SonarIdent] = identifyFileFormat(nomFic, 0)
%
% See also is_GMT_Format is_CARAIBES_Bathy_Format is_CARAIBES_SonarLateral_file Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, typeFichier, SonarIdent] = identifyFileFormat(nomFic, SonarIdent)

typeFichier = 'Unknown';

if ~exist(nomFic, 'file')
    msg = sprintf('%s doesn''t exist', nomFic);
    my_warndlg(msg, 1);
    flag = 0;
    return
end

%% Test si fichier Netcdf

fid = fopen(nomFic, 'r');
if fid == -1
    [~, ~, ext] = fileparts(nomFic);
    if strcmpi(ext, '.hac')
        msg = sprintf('%s not readable. Be sure this file is not open in HacView or Movies', nomFic);
    else
        msg = sprintf('%s not readable.', nomFic);
    end
    my_warndlg(msg, 1);
    flag = 0;
    return
else
    flag = 1;
end

str = fread(fid, 3, '*char')';
fclose(fid);
% if strcmp(str, 'CDF') % || strcmp(str, '�HD')
if strcmp(str, 'CDF') || strcmp(str, '�HD') % Decomment� le 29/06/2021 pour MBG calcul� par GLOBE
    
    %% R�cuperation du skelette du fichier netcdf
    
    a = cl_netcdf('fileName', nomFic);
    sk = get(a, 'skeleton');
    if isempty(sk)
        str = sprintf('CDF file "%s" seems corrupted.', nomFic);
        my_warndlg(str, 1);
        flag = 0;
        return
    end
    
    %% Test si c'est un fichier GMT
    
    numAtt1 = find_numAtt(a, 'title');
    numAtt2 = find_numAtt(a, 'source');
    numVar1 = find_numVar(a, 'x_range');
    numVar2 = find_numVar(a, 'y_range');
    numVar3 = find_numVar(a, 'spacing');
    numVar4 = find_numVar(a, 'dimension');
    if ~isempty(numAtt1) &&  ~isempty(numAtt2) && ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3) && ~isempty(numVar4)
        typeFichier = 'GMT_cf';
        return
    else
        numAtt1 = find_numAtt(a, 'title');
        numAtt2 = find_numAtt(a, 'source');
        numVar1 = find_numVar(a, 'x');
        numVar2 = find_numVar(a, 'y');
        numVar3 = find_numVar(a, 'z');
        if ~isempty(numAtt1) &&  ~isempty(numAtt2) && ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3)
            typeFichier = 'GMT_nf';
            return
        end
    end
    
    numAtt1 = find_numAtt(a, 'title');
    numAtt2 = find_numAtt(a, 'Conventions');
    numAtt3 = find_numAtt(a, 'description');
    %     numAtt4 = find_numAtt(a, 'node_offset');
    numVar1 = find_numVar(a, 'x');
    numVar2 = find_numVar(a, 'y');
    numVar3 = find_numVar(a, 'z');
    if ~isempty(numAtt1) &&  ~isempty(numAtt2)&&  ~isempty(numAtt3) && ...
            ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3)
        typeFichier = 'GMT_NIWA';
        return
    end
    
    numAtt1 = find_numAtt(a, 'title');
    numAtt2 = find_numAtt(a, 'Conventions');
    numAtt3 = find_numAtt(a, 'GMT_version');
    numAtt4 = find_numAtt(a, 'history');
    numVar1 = find_numVar(a, 'lon');
    numVar2 = find_numVar(a, 'lat');
    numVar3 = find_numVar(a, 'z');
    if ~isempty(numAtt1) && ~isempty(numAtt2) && ~isempty(numAtt3) && ~isempty(numAtt4) && ...
            ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3)
        typeFichier = 'GMT_LatLong';
        return
    end
    
    numAtt1 = find_numAtt(a, 'title');
    numAtt2 = find_numAtt(a, 'Conventions');
    numAtt3 = find_numAtt(a, 'GMT_version');
    %     numAtt4 = find_numAtt(a, 'node_offset');
    numDim1 = find_numDim(a, 'x');
    numDim2 = find_numDim(a, 'y');
    numVar1 = find_numVar(a, 'x');
    numVar2 = find_numVar(a, 'y');
    numVar3 = find_numVar(a, 'z');
    if ~isempty(numAtt1) && ~isempty(numAtt2) && ~isempty(numAtt3) &&  ...
            ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3) && ...
            ~isempty(numDim1) && ~isempty(numDim2)
        typeFichier = 'GMT_GEBCO';
        return
    end
    
    %% Test si c'est un fichier Caraibes de bathymetrie
    
    numVar = find_numVar(a, 'mbCycle');
    if ~isempty(numVar)
        numAtt = find_numAtt(a, 'mbSounder');
        if ~isempty(numAtt)
            mbSounder = sk.att(numAtt).value;
            SonarIdent = SonarIdent_caraibes2sonarScope(mbSounder);
        end
        typeFichier = 'CaraibesMbb';
        return
    end
    
    %% Test si c'est un fichier Caraibes Sonar Lat�ral
    
    numVar = find_numVar(a, 'grRawImage');
    if ~isempty(numVar)
        typeFichier = 'CaraibesSonarLateral';
        numAtt      = find_numAtt(a, 'grStrSensor');
        mbSounder   = sk.att(numAtt).value;
        if ~isempty(mbSounder)
            SonarIdent = SonarIdent_caraibes2sonarScope(mbSounder);
        end
        % Impossible de determiner le nom du sondeur dans ce cas
        return
    end
    
    %% Sinon
    
    numVar1 = find_numVar(a, 'Type');
    numVar2 = find_numVar(a, 'Format');
    numVar3 = find_numVar(a, 'Mosaic_type');
    if ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3)
        Type        = get_value(a, 'Type');
        Mosaic_type = get_value(a, 'Mosaic_type');
        switch Type
            case 'MIX'
                typeFichier = 'CaraibesMNT';
                % Impossible de determiner le nom du sondeur dans ce cas
                return
            case 'DTM'
                typeFichier = 'CaraibesMNT';
                % Impossible de determiner le nom du sondeur dans ce cas
                return
            case 'MOS'
                % switch strMosaic_type
                switch Mosaic_type
                    case 'GEO'
                        numVar = find_numVar(a, 'Sounder_type');
                        if ~isempty(numVar)
                            Sounder_type = get_value(a, numVar);
                            SonarIdent = SonarIdent_caraibes2sonarScope(Sounder_type);
                        end
                        typeFichier = 'CaraibesMOSImage';
                        return
                    case 'STR'
                        numVar = find_numVar(a, 'PortMode');
                        if ~isempty(numVar)
                            PortMode = get_value(a, numVar);
                            SonarIdent = modeCar2Sim(PortMode(1));
                        end
                        typeFichier = 'CaraibesSTRImage';
                        return
                end
        end
    end
    
    % Sinon : format CARAIBES dela version 3.6
    numAtt1 = find_numAtt(a, 'Type');
    numAtt2 = find_numAtt(a, 'Format');
    numAtt3 = find_numAtt(a, 'Mosaic_type');
    if ~isempty(numAtt1) && ~isempty(numAtt2) && ~isempty(numAtt3)
        Type        = get_valAtt(a, 'Type');
        Mosaic_type = get_valAtt(a, 'Mosaic_type');
        switch Type
            case 'MIX'
                typeFichier = 'CaraibesMNT_3_6';
                % Impossible de determiner le nom du sondeur dans ce cas
                return
            case 'DTM'
                typeFichier = 'CaraibesMNT_3_6';
                % Impossible de determiner le nom du sondeur dans ce cas
                return
            case 'MOS'
                % switch strMosaic_type
                switch Mosaic_type
                    case 'GEO'
                        numAtt = find_numAtt(a, 'Sounder_type');
                        if ~isempty(numVar)
                            Sounder_type = get_valAtt(a, numAtt);
                            SonarIdent = SonarIdent_caraibes2sonarScope(Sounder_type);
                        end
                        typeFichier = 'CaraibesMOSImage_3_6';
                        return
                    case 'STR'
                        numVar = find_numVar(a, 'PortMode');
                        if ~isempty(numVar)
                            PortMode = get_value(a, numVar);
                            SonarIdent = modeCar2Sim(PortMode(1));
                        end
                        typeFichier = 'CaraibesSTRImage_3_6';
                        return
                end
        end
    end
    
end

%% Analyse de l'extension

[nomDir, name, ext] = fileparts(nomFic);
if isempty(ext)
    nomFic = fullfile(nomDir, [name '.ers']);
    if exist(nomFic, 'file')
        typeFichier = 'ERMAPPER';
        return
    end
end

%% Test si fichier Fledermaus

if is_FLEDERMAUS(nomFic)
    typeFichier = 'FLEDERMAUS';
    return
end

%% Test si fichier ESRI Grid ascii

if is_EsriGridAscii(nomFic)
    typeFichier = 'EsriGridAscii';
    return
end

%% Sinon deduction a partir de l'extednsion

switch lower(ext)
    case {'.bag'}
        typeFichier = 'BAG'; % Bathy. Attributed Grid';
    case {'.tif'; '.tiff'; '.jpg'; '.jpeg'; '.jp2'; '.bmp'; '.png'; '.pgm'; '.gif'; '.ppm'}
        if isGeotiff(nomFic)
            typeFichier = 'GEOTIFF';
        else
            typeFichier = 'IMAGE';
        end
    case {'.hdr'; '.flt'}
        typeFichier = 'HDR-FLT';
    case '.rdl'
        typeFichier = 'SimradRDL';
    case '.xtf'
        typeFichier = 'XTF';
    case '.lgz'
        typeFichier = 'LGZ';
        
    case '.mat'
        typeFichier = 'MATLAB';
        
    case '.ers'
        typeFichier = 'ERMAPPER';
    case '.imo'
        typeFichier = 'TRIAS';
    case {'.all'; '.kmall'}
        typeFichier = 'SimradAll';
    case {'.txt'; '.csv'}
        if (length(name) >= 7) && strcmp(name(1:7), 'Amp0Phs')
            typeFichier = 'SimradAmpPhase';
        else
            typeFichier = identifyFormatAscii(nomFic);
        end
    case '.irap'
        typeFichier = 'IRAP';
    case '.grd'
        typeFichier = 'SURFER';
    case {'.sonarmosaic'; '.class'}
        typeFichier = 'TRITON';
    case '.vmascii'
        typeFichier = 'VMAPPERASCII';
    case '.tomo'
        typeFichier = 'TomographyOBS';
    case '.xyz'
        typeFichier = identifyFormatAscii(nomFic);
    case '.s7k'
        typeFichier = 'ResonS7k';
    case '.hgt'
        typeFichier = 'SRTM';
    case '.im'
        SonarIdent = 5; % SAR
        typeFichier = 'SonarSAR';
        
    case '.rdf'
        SonarIdent = 16; % GeoAcoustics
        typeFichier = 'GeoSwathRDF';
        
    case '.xml'
        Data = xml_mat_read(nomFic);
        if ~isfield(Data, 'Signature1') || ~isfield(Data, 'Signature2')
            return
        end
        switch Data.Signature1
            case 'SonarScope'
                switch Data.Signature2
                    case 'Image'
                        typeFichier = 'XML';
                    case 'ImagesAlongNavigation' % Subbottom
                        typeFichier = 'XML_ImagesAlongNavigation';
                    case 'WaterColumnCube3D'
                        typeFichier = 'WaterColumnCube3D';
                    case '3DSeismic'
                        typeFichier = 'XML_3DSeismic';
                    otherwise
                        return
                end
            case {'Movies', 'MOVIES'}
                switch Data.Signature2
                    case {'BOB', 'Bob'}
                        typeFichier = 'BOB';
                    otherwise
                        return
                end
            otherwise
                return
        end
        
    case '.bob'
        typeFichier = 'BOB';
    case {'.seg'; '.sgy'; '.segy'}
        typeFichier = 'SEGY';
    case '.sdf'
        typeFichier = 'KleinSDF';
    case '.shp'
        typeFichier = 'ESRIshapeFile';
    case '.hac'
        typeFichier = 'ICESHAC';
    case '.odv'
        typeFichier = 'ODV';        
        SonarIdent  = 'ADCP';        
    case '.raw'
%         if testER60(nomFic)
            typeFichier = 'Simrad_ExRaw';
%         end
    case '.gxf'
        typeFichier = 'GXF';
end
