function [typeFichier, ScaleFactor, GeometryType, DataType, IndedexColumns] = identifyFormatAscii(nomFic)

ScaleFactor    = [];
typeFichier    = 'ASCII_XYZ';
GeometryType   = [];
DataType       = [];
IndedexColumns = [];

fid = fopen(nomFic);
Ligne1 = fgetl(fid);
mots1 = strsplit(Ligne1(3:end));
Ligne2 = fgetl(fid);
mots2 = strsplit(Ligne2);
fclose(fid);
if strcmp(Ligne1(1:2), '//')
    if (length(mots2) == 4)
        typeFichier = 'ASCII_CARIS';
        if strcmp(mots1{end}, 'Depth')
            ScaleFactor = -1;
            DataType = cl_image.indDataType('Bathymetry');
        end
        if strcmp(mots1{end}, 'Reflectivity')
            ScaleFactor = -1;
            DataType = cl_image.indDataType('Reflectivity');
        end
        for i=1:length(mots1)
            if strcmp(mots1{i}, 'Lat')
                GeometryType = cl_image.indGeometryType('LatLong');
                IndedexColumns = [2 3 4];
                break
            end
            if strcmp(mots1{i}, 'Y')
                GeometryType = cl_image.indGeometryType('GeoYX');
                IndedexColumns = [3 2 4];
                break
            end
        end
    elseif strcmp(Ligne1, '//Lat (DM) Long (DM) Depth')
        typeFichier = 'ASCII_CARIS_LatLon_DM';
        ScaleFactor = -1;
        DataType = cl_image.indDataType('Bathymetry');
    end
end
