% Liste des variables contenues dans un fichier .mat
%
% Syntax
%   [liste, info] = list_var_matfile(nomFic)
% 
% Input Arguments 
%   nomFic : Nom du fichier .mat
%
% Output Arguments 
%   liste : Liste des variables
%   info  : Informations sur les variables
% 
% Examples
%   nomFic = getNomFicDatabase('sismique.mat');
%   [liste, info] = list_var_matfile(nomFic)
%
% See also load save Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [listeVars, Info] = list_var_matfile(nomFic)

listeVars = [];
Info = [];
if exist(nomFic ,'file') == 0
    my_warndlg(sprintf('Le fichier %s n''existe pas', nomFic), 1);
    return
end

liste = whos('-file', nomFic);
str{1} = 'Name      Size            Bytes  Class';
for i=1:length(liste)
    if liste(i).complex
        str{end+1} = sprintf('%s\t%s\t%d\t%s (complex)', ...
            liste(i).name, num2strCode(liste(i).size), liste(i).bytes, liste(i).class); %#ok
    else
        str{end+1} = sprintf('%s\t%s\t%d\t%s', ...
            liste(i).name, num2strCode(liste(i).size), liste(i).bytes, liste(i).class); %#ok
    end
    listeVars{i} = liste(i).name; %#ok<AGROW>
end

Info = cell2str(str);
