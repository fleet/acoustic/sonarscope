% Liste des repertoires presents dans un repertoire
%
% Syntax
%   liste = listeDirOnDir(nomDir)
%
% Input Arguments 
%   nomDir : Repertoire de recherche
%
% Output Arguments
%   liste  : Liste des repertoires trouves dans le repertoire  avec les extensions Extx.
%
% Examples
%   nomDir = IfrTbxIfr;
%   listeDir = listeDirOnDir(nomDir)
%   for i=1:length(listeDir)
%       listeDir(i)
%   end
%
% See also findFicInDir Authors
% Authors  : JMA
%-------------------------------------------------------------------------------------------

function listeDir = listeDirOnDir(nomDir)

if isequal(nomDir, 0)
    listeDir = [];
    return
end

listeDir = dir(nomDir);
sub = [];
for k=1:length(listeDir)
    if listeDir(k).isdir
        if ~strcmp(listeDir(k).name, '.') && ~strcmp(listeDir(k).name, '..')
            sub(k) = 1; %#ok<AGROW>
        end
    end
end
sub = find(sub);
listeDir = listeDir(sub); %#ok<FNDSB>
