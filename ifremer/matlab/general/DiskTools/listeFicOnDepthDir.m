% Liste des fichiers presents dans une arborescence de repertoires avec plusieurs extensions
% Renvoie la liste de ces fichiers avec l'extension Ext1 dans lExt1, Ext2 dans lExt2,...
%
% Syntax
%   [lExt1, lExt2, ...] = listeFicOnDepthDir(nomDir, Ext1, Ext2, ...)
%
% Input Arguments
%   nomDir : Repertoire racine de recherche
%   Extx   : Extensions recherchees
%
% Output Arguments
%   lExtx  : Liste des fichiers trouves avec les extensions Ext.
%
% Examples
%   liste = listeFicOnDepthDir('C:\IfremerToolboxDataPublic\Level1', '.png')
%   [L1, L2] = listeFicOnDepthDir('C:\IfremerToolboxDataPublic\Level1', '.png', '.gz')
%   liste = listeFicOnDepthDir('C:\IfremerToolboxDataPublic\Level1', '.png', 'Filtre', 'Sonar')
%   liste = listeFicOnDepthDir('C:\IfremerToolboxDataPublic\Level1')
%
% See also findFicInDir listeDirOnDir listeFicOnDir Authors
% Authors  : JMA
%-------------------------------------------------------------------------------------------

function varargout = listeFicOnDepthDir(nomDir, varargin)

[varargin, Filtre] = getPropertyValue(varargin, 'Filtre', []);

if isempty(varargin)
    listeDir = depthDir(nomDir);
else
    if strcmp(varargin{1}, '.m') % Appel spécifique de MaintenanceSSc
        listeDir = depthDir(nomDir, 'isMatlabPath', 1);
    else
        listeDir = depthDir(nomDir);
    end
end

listeDir = [{nomDir}; listeDir(:)];
if isempty(varargin)
    varargout{1} = {};
    for k=1:length(listeDir)
        liste = listeFicOnDir(listeDir{k}, 'Filtre', Filtre);
        if ~isempty(liste)
            varargout{1} = [varargout{1}; liste];
        end
    end
else
    for k1=1:length(varargin)
        varargout{k1} = {}; %#ok<AGROW>
        for k2=2:length(listeDir)
            liste = listeFicOnDir(listeDir{k2}, varargin{k1}, 'Filtre', Filtre);
            if ~isempty(liste)
                varargout{k1} = [varargout{k1}; liste];
            end
        end
    end
end
