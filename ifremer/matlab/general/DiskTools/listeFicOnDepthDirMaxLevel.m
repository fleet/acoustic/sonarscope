% Liste des fichiers presents dans une arborescence de repertoires avec plusieurs extensions
% Renvoie la liste de ces fichiers avec l'extension Ext1 dans lExt1, Ext2 dans lExt2,...
% en indiquant un nombre maximal de sous-niveaux
%
% Syntax
%   [lExt1, lExt2, ...] = listeFicOnDepthDirMaxLevel(MaxLevel, nomDir, Ext1, Ext2, ...)
%
% Input Arguments
%   MaxLevel : Nombre max de sous-niveaux
%   nomDir   : Repertoire racine de recherche
%   Extx     : Extensions recherchees
%
% Output Arguments
%   lExtx  : Liste des fichiers trouves avec les extensions Ext.
%
% Examples
%   liste = listeFicOnDepthDirMaxLevel(1, 'C:\IfremerToolboxDataPublic\Level1', '.png')
%   [L1, L2] = listeFicOnDepthDirMaxLevel(1, 'C:\IfremerToolboxDataPublic\Level1', '.png', '.gz')
%   liste = listeFicOnDepthDirMaxLevel(1, 'C:\IfremerToolboxDataPublic\Level1', '.png', 'Filtre', 'Sonar')
%   liste = listeFicOnDepthDirMaxLevel(1, 'C:\IfremerToolboxDataPublic\Level1')
%
% See also findFicInDir listeDirOnDir listeFicOnDir Authors
% Authors  : JMA
%-------------------------------------------------------------------------------------------

%{
nomDir = 'F:\ShallowSurvey2012\Kongsberg\corrected_latest_data\Optional_Areas'
liste = listeFicOnDepthDirMaxLevel(4, nomDir, '.all', 'nomDirFinal', 'SonarScope');
% for kk=1:length(liste), disp(kk), disp(liste{kk}), end
for kk=1:length(liste), disp(liste{kk}), end
whos liste

nomDir = 'F:\ShallowSurvey2012\Kongsberg\corrected_latest_data';
liste = listeFicOnDepthDirMaxLevel(5, nomDir, '.all', 'nomDirFinal', 'SonarScope');
% for kk=1:length(liste), disp(kk), disp(liste{kk}), end
for kk=1:length(liste), disp(liste{kk}), end
whos liste

nomDir = 'F:\ShallowSurvey2012\Kongsberg';
liste = listeFicOnDepthDirMaxLevel(6, nomDir, '.all', 'nomDirFinal', 'SonarScope');
% for kk=1:length(liste), disp(kk), disp(liste{kk}), end
for kk=1:length(liste), disp(liste{kk}), end
whos liste



nomDir = 'F:\ShallowSurvey2012\Kongsberg\corrected_latest_data\Optional_Areas'
liste = listeFicOnDepthDirMaxLevel(4, nomDir, 'nomDirFinal', 'SonarScope');
% for kk=1:length(liste), disp(kk), disp(liste{kk}), end
for kk=1:length(liste), disp(liste{kk}), end
whos liste

nomDir = 'F:\ShallowSurvey2012\Kongsberg\corrected_latest_data';
liste = listeFicOnDepthDirMaxLevel(5, nomDir, 'nomDirFinal', 'SonarScope');
% for kk=1:length(liste), disp(kk), disp(liste{kk}), end
for kk=1:length(liste), disp(liste{kk}), end
whos liste

nomDir = 'F:\ShallowSurvey2012\Kongsberg';
liste = listeFicOnDepthDirMaxLevel(6, nomDir, 'nomDirFinal', 'SonarScope');
% for kk=1:length(liste), disp(kk), disp(liste{kk}), end
for kk=1:length(liste), disp(liste{kk}), end
whos liste

%}

% TODO : jamais test� autrement que avec MaxLevel=1
% TODO : il y a certainement plus simple avec les regexp, ...
function varargout = listeFicOnDepthDirMaxLevel(MaxLevel, nomDir, varargin)

[varargin, nomDirFinal] = getPropertyValue(varargin, 'nomDirFinal', []);
[varargin, Filtre]      = getPropertyValue(varargin, 'Filtre',      []);

Extensions = varargin;
for kExt=1:length(Extensions)
    if ~isempty(Extensions{kExt})
        if ~strcmp(Extensions{kExt}(1), '.')
            Extensions{kExt} = ['.' Extensions{kExt}];
        end
    end
end

str1 = 'Recherche des sous-r�&pertoires en cours.';
str2 = 'Looking for sub directories.';
WorkInProgress(Lang(str1,str2))

listeDir = depthDirMaxLevel(MaxLevel, 0, nomDir, 'nomDirFinal', nomDirFinal);

listeDir = [{nomDir}; listeDir(:)];
if isempty(Extensions)
    varargout{1} = {};
    N = length(listeDir);
    str1 = 'Recherche des fichiers dans les r�pertoires';
    str2 = 'Looking for files in directories';
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        liste = listeFicOnDir(listeDir{k}, 'Filtre', Filtre);
        if ~isempty(liste)
            %                 liste
            varargout{1} = [varargout{1}; liste]; % TODO : c'est tr�s maladroit �a !
        end
    end
%     my_close(hw, 'MsgEnd', 'TimeDelay', 10)
    my_close(hw)
else
    for kExt=1:length(Extensions)
        varargout{kExt} = {}; %#ok<AGROW>
        N = length(listeDir);
        str1 = sprintf('Recherche des fichiers, extention "%s" dans les r�pertoires', Extensions{kExt});
        str2 = sprintf('Looking for files with extention "%s" in directories', Extensions{kExt});
        hw = create_waitbar(Lang(str1,str2), 'N', N);
        for k=1:N
            my_waitbar(k, N, hw)
            if isempty(Extensions{kExt})
                liste = listeFicOnDir(listeDir{k}, 'Filtre', Filtre);
            else
                liste = listeFicOnDir(listeDir{k}, Extensions{kExt}, 'Filtre', Filtre);
            end
            if ~isempty(liste)
                %                 liste
                varargout{kExt} = [varargout{kExt}; liste]; % TODO : c'est tr�s maladroit �a !
            end
        end
%         my_close(hw, 'MsgEnd', 'TimeDelay', 10)
        my_close(hw)
    end
end
