% Liste des fichiers presents dans un repertoire avec plusieurs extensions
% Renvoie la liste de ces fichiers avec l'extension Ext1 dans lExt1, Ext2 dans lExt2,...
%
% Syntax
%   [lExt1, lExt2, ...] = listeFicOnDir(nomDir, Ext1, Ext2, ...)
%
% Input Arguments
%   nomDir : Repertoire de recherche
%   Extx   : Extensions recherch�es
%
% Output Arguments
%   lExtx  : Liste des fichiers trouv�s avec les extensions Extx.
%
% Examples
%   liste = listeFicOnDir('D:\SonarScopeData\Level1\Public', '.png')
%   [L1, L2] = listeFicOnDir('D:\SonarScopeData\Level1\Public', '.png', '.gz')
%   liste = listeFicOnDir('D:\SonarScopeData\Level1\Public', '.png', 'Filtre', 'Sonar')
%   liste = listeFicOnDir('D:\SonarScopeData\Level1\Public', '.png', 'FiltreExclude', 'Sonar')
%   liste = listeFicOnDir('D:\SonarScopeData\Level1\Public')
%
% See also findFicInDir listeDirOnDir listeFicOnDepthDir Authors
% Authors  : JMA
%-------------------------------------------------------------------------------------------

function varargout = listeFicOnDir(nomDir, varargin)

[varargin, Filtre]        = getPropertyValue(varargin, 'Filtre',        []);
[varargin, FiltreExclude] = getPropertyValue(varargin, 'FiltreExclude', []);

for k=1:length(varargin)
    varargout{k} = {}; %#ok<AGROW>
end

w = dir(nomDir);
sub = [];
for k2=1:length(w)
    if ~w(k2).isdir
        sub(k2) = 1; %#ok<AGROW>
    end
end
sub = find(sub);
w = w(sub); %#ok<FNDSB>

if isempty(varargin)
    liste = {};
    for k2=1:length(w)
        [~, nom] = fileparts(w(k2).name);
        if ~isempty(Filtre)
            if ~contains(nom, Filtre)
                continue
            end
        end
        if ~isempty(FiltreExclude)
            if contains(nom, FiltreExclude)
                continue
            end
        end
        
        nomExt = fullfile(nomDir, w(k2).name);
        if exist(nomExt, 'file')
            liste{end+1,1} = nomExt; %#ok<AGROW>
        end
    end
    varargout{1} = unique(liste);
    
else
    for k1=1:length(varargin)
        liste = {};
        for k2=1:length(w)
            [~, nom] = fileparts(w(k2).name);
            if ~isempty(Filtre)
                if iscell(Filtre)
                    flag = 0;
                    for k3=1:length(Filtre)
                        if ~isempty(strfind(nom, Filtre{k3}))
                            flag = 1;
                            continue
                        end
                    end
                    if ~flag
                        continue
                    end
                else
                    if ~contains(nom, Filtre)
                        continue
                    end
                end
            end
            if ~isempty(FiltreExclude)
                if iscell(FiltreExclude)
                    flag = 0;
                    for k3=1:length(FiltreExclude)
                        if ~isempty(strfind(nom, FiltreExclude{k3}))
                            flag = 1;
                            continue
                        end
                    end
                    if flag
                        continue
                    end
                else
                    if contains(nom, FiltreExclude)
                        continue
                    end
                end
            end
            
            nomExt = fullfile(nomDir, [nom, varargin{k1}]);
            if exist(nomExt, 'file')
                liste{end+1,1} = nomExt; %#ok<AGROW>
            end
        end
        varargout{k1} = unique(liste);
    end
end
