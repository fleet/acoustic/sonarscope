% Liste des fichiers presents dans un repertoire avec plusieurs extensions
% Renvoie la liste de ces fichiers avec l'extension Ext1 dans lExt1, Ext2 dans lExt2,...
%
% Syntax
%   [lExt1, lExt2, ...] = listeFicOnDir(nomDir, Ext1, Ext2, ...)
%
% Input Arguments 
%   nomDir : Repertoire de recherche
%   Extx   : Extensions recherchees
%
% Output Arguments
%   lExtx  : Liste des fichiers trouves avec les extensions Extx.
%
% Examples
%   liste = listeFicOnDir(fullfile(SonarScopeData, 'Public'), '.png')
%
% See also findFicInDir listeDirOnDir Authors
% Authors  : JMA
%-------------------------------------------------------------------------------------------

function listeFic = listeFicOnDir2(nomDir, varargin)

if nargin < 2
	return
end 

if ischar(nomDir)
    nomDir = {nomDir};
end

listeFic = [];
for k1=1:length(nomDir)
    w = dir(nomDir{k1});
    for k2=1:length(w)
        if ~w(k2).isdir
            nomFic = fullfile(nomDir{k1}, w(k2).name);
            [~, ~, E] = fileparts(nomFic);
            if nargin == 1
                listeFic{end+1} = nomFic; %#ok<AGROW>
            else
                for k3=1:length(varargin)
                    if strcmp(E, varargin{k3})
                        listeFic{end+1} = nomFic; %#ok<AGROW>
                    end
                end
            end
        end
    end
end
