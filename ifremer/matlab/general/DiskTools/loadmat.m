% Lecture d'une variable contenue dans un fichier .mat
%
% Syntax
%   loadmat(nomFic)
%   [x, ...] = loadmat(nomFic, ...)
% 
% Input Arguments 
%   nomFic : Nom du fichier .mat
%
% Name-Value Pair Arguments
%   nomVar : nom de la variable
% 
% Output Arguments 
%   []     : Affichage des noms de variables contenues dans le fichier
%   x, ... : Variables lues
%
% Remarks : Si le fichier ne contient qu'une seule variable : nomVar n''est pas utilis�
% 
% Examples
%   nomFic = getNomFicDatabase('sismique.mat');
%   [liste, info] = list_var_matfile(nomFic)
%
%   loadmat(nomFic);
%   x = loadmat(nomFic);
%   x = loadmat(nomFic, 'nomVar', 'Image');
%   imagesc(x); colormap(gray(256)); colorbar;
%
%   x = 1:10;
%   y = rand(size(x));
%   Tag = 'toto';
%   S.q = pi;
%   S.s = 'hh';
%   nomFicTest = my_tempname('.mat');
%   save(nomFicTest, 'x', 'y', 'Tag', 'S');
%   xx = loadmat(nomFicTest, 'nomVar', 'x')
%   yy = loadmat(nomFicTest, 'nomVar', 'y')
%   TT = loadmat(nomFicTest, 'nomVar', 'Tag')
%   SS = loadmat(nomFicTest, 'nomVar', 'S')
% 
%   [yy, xx, SS, TT] = loadmat(nomFicTest, 'nomVar', {'y';'x';'S';'Tag'})
% 
%   nomFicTest = my_tempname('.mat');
%   save(nomFicTest, 'y');
%   yy = loadmat(nomFicTest, 'nomVar', 'y')
%   yy = loadmat(nomFicTest)
%
% See also load save Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = loadmat(nomFic, varargin)

if iscell(nomFic)
    nomFic = nomFic{1};
end

if ~exist(nomFic ,'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" do not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    varargout{1} = [];
    return
end

matObj = load(nomFic); % Avant. L� aussi �a ouvre des figures !!!
% matObj = matfile(nomFic); % Apr�s mais �a ouvre des figures !!!

Names = fieldnames(matObj);
if (nargin == 1) && (length(Names) == 1)
    varargout{1} = matObj.(Names{1});
    return
end

[varargin, nomVar] = getPropertyValue(varargin, 'nomVar', []); %#ok<ASGLU>
if isempty(nomVar)
    my_warndlg('loadmat : correction must be done', 0);
    varargout{1} = matObj.(Names{1});
    return
end

try
    if ischar(nomVar)
        varargout{1} = matObj.(nomVar);
    else
        for k=length(nomVar):-1:1
            varargout{k} = matObj.(nomVar{k});
        end
    end
catch ME
    varargout{1} = [];
    fprintf('ReadFailure : %s\n', ME.message)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end
