% Rename a file using dos command
%
% Syntax
%   my_rename(fileNameOld, fileNameNew)
%
% Input Arguments
%   fileNameOld : Name of the file to be renamed
%   fileNameNew : New file name
%
% Examples
%   my_rename(fileNameOld, fileNameNew)
%
% See also Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function my_rename(fileNameOld, fileNameNew)

flagFichierIntermediaire = false;

% Bidouille pour �viter des pbs quand le nom des fichiers est trop grand
PWD = pwd;
[nomDir1, nomFic1, ext1] = fileparts(fileNameOld);
[nomDir2, nomFic2, ext2] = fileparts(fileNameNew);
if strcmp(nomDir1, nomDir2)
    cd(nomDir1)
    fileNameOld = [nomFic1 ext1];
    if strcmpi(ext1, ext2)
        fileNameTmp = [nomFic2 ext2 'TMP'];
        fileNameNew = [nomFic2 ext2];
        str = sprintf('rename "%s" "%s"', fileNameOld, fileNameTmp);
        strIntermediaire = sprintf('rename "%s" "%s"', fileNameTmp, fileNameNew);
        flagFichierIntermediaire = true;
    else
        fileNameNew = [nomFic2 ext2];
        str = sprintf('rename "%s" "%s"', fileNameOld, fileNameNew);
    end
else
    str = sprintf('rename "%s" "%s"', fileNameOld, fileNameNew);
end
fprintf('  --> %s\n', str)
flag = dos(str);
if flag == 1
    str1 = sprintf('La commande dos %s a �chou�.\n\nIl vous faut peut-�tre fermer GLOBE s''il est ouvert avant de lancer la g�n�ration des �chogrammes polaires.', str);
    str2 = sprintf('The dos command %s failed.\n\nPerhaps you should close GLOBE before launching the polar echograms (in case you are recomputing files that are already loaded in GLOBE)', str);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'CommandeDosFailed');
    
    pause(1)
    try % again
        str1 = sprintf('Nouvel essai de : %s', str);
        str2 = sprintf('Attempt do redo the command : %s', str);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'CommandeDosFailed');
        flag = dos(str);
    catch ME
        my_breakpoint
    end
end

if (flag ~= 1) && flagFichierIntermediaire
    fprintf('  --> %s\n', strIntermediaire)
    flag = dos(strIntermediaire);
    if flag == 1
        str1 = sprintf('La commande dos %s a �chou�.', strIntermediaire);
        str2 = sprintf('The dos command %s failed.', strIntermediaire);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'CommandeDosFailed');
    end
end

cd(PWD)
