function renameFiles(listeFic, motif1, motif2)

logFileId = getLogFileId;

% Remplacement du caractere * par la chaine appropriee
% Identification d'une s�rie de caract�res
motifToRep 	= regexprep(motif1, '\*', '(\\w+)');
% Identification d'un caract�re (sauf Espace)
motifToRep 	= regexprep(motifToRep, '\?', '\\S');

%% Simulation

str = {};
listFicIn  = {};
listFicOut = {};
N = length(listeFic);
for k=1:N
    index = regexp(listeFic{k}, motifToRep, 'match');
    if ~isempty(index)
        nom2 = regexprep(listeFic{k}, motifToRep, motif2);
        str{end+1} = sprintf('%s -> %s', listeFic{k}, nom2); %#ok<AGROW>
        listFicIn{end+1}  = listeFic{k}; %#ok<AGROW>
        listFicOut{end+1} = nom2; %#ok<AGROW>
    end
end
N = length(str);
[sub, flag] = my_listdlgMultiple('List of possible rename actions. Select the ones you want to execute.', str, 'InitialValue', 1:N);
if ~flag
    return
end

%% Renomage

N = length(sub);
hw = create_waitbar(Lang('Renommage des fichiers en cours', 'Rename files'), 'N', N);
for k=1:N
    my_waitbar(k,N,hw)
    if strcmpi(listFicIn{sub(k)}, listFicOut{sub(k)})
        try
            my_rename(listFicIn{sub(k)}, listFicOut{sub(k)})
            str = sprintf('\n\t%s\n\t\trenamed into\n\t%s\n', listFicIn{sub(k)}, listFicOut{sub(k)});
        catch
            str = sprintf('\n\t%s\n\t\tNOT renamed into\n\t%s\n', listFicIn{sub(k)}, listFicOut{sub(k)});
        end
    else
        [flag, message] = movefile(listFicIn{sub(k)}, listFicOut{sub(k)});
        if flag
            str = sprintf('\n\t%s\n\t\trenamed into\n\t%s\n', listFicIn{sub(k)}, listFicOut{sub(k)});
        else
            str = sprintf('\n\t%s\n\t\tNOT renamed into\n\t%s\n\t\tReason : %s\n', listFicIn{sub(k)}, listFicOut{sub(k)}, message);
        end
    end
    logFileId.info('RenameFiles', str);
end
my_close(hw)
