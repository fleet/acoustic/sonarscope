% Save data in a .mat file with a specific variable name
%
% Syntax
%    flag = savemat(nomFic, nomVar, Value, ...)
% 
% Input Arguments 
%   Filename : Matlab filename
%   nomVar   : Name of the variable in the .mat file
%   Value    : Values
%
% Name-Value Pair Arguments 
%   version : '-v7.3' | '-v7' | '-v6' | '-v4'
%
% Output Arguments 
%   flag :1=OK, 0=KO
%
% Examples 
%   Filename = my_tempname('.mat')
%   savemat(Filename, 'Foo', rand(2,4), '-v6')
%
% See also loadmat Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = savemat(nomFic, nomVar, Value, varargin) %#ok<INUSL>

try
    cmd = sprintf('%s=Value;', nomVar);
    eval(cmd);
    save(nomFic, nomVar, varargin{:});
    flag = 1;
catch
    flag = 0;
end
