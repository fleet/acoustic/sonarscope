% Taille d'un fichier (en octets)
%
% Syntax
%   nbOc = sizeFic(nomFic)
%
% Input Arguments
%   nomFic  : nom du fichier
%
% Output Arguments
%   nbOc : Nombre d'octets du fichier
%
% Examples 
%   nomFic = which('startup')
%	nbOc = sizeFic(nomFic)
%
% See also getNbLinesAsciiFile Authors 
% Authors : JMA
%--------------------------------------------------------------------------------

function nbOc = sizeFic(nomFic)

if exist(nomFic, 'file')
    pppp = dir(nomFic);
    nbOc = pppp.bytes;
else
    nbOc = 0;
end

