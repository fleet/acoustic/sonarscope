function test_cl_nmatrix

%% Create and initialize a cl_nmatrix with default mapped file

nm = cl_nmatrixfactory.createInstance('Value', [1 2 3 4; 5 6 7 8]);

s = struct(nm) %#ok<*NOPRT>
%{
     s =
  struct with fields:
             ValNaN: NaN
                mat: [2×4 nmatrix]
             sizeIn: [2 4]
    dimensionVector: [1 2]
    directionVector: [1 1]
%}

s.mat
%{
     s.mat
ans =
  2×4 nmatrix array with properties:
      Filename: 'C:\Temp\tmp_20200310_164329_736340.memmapfile'
    FileFormat: 'binary'
       DataSet: ''
          Mode: 'memmapfile'
    TargetType: @double
           Map: [1×1 memmapfile]
       GPUMode: 'off'
     GPUTarget: 0
%}

size(nm) %  [2     4]
length(nm) % 1 mais 4 avec cl_memmafile !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
numel(nm) % 8
nm(:,:) % OK
nm(2,:) % OK
nm(:,3) % OK
nm(:)   % OK
nm(6)   % OK
nm + 10 % OK
nm == 3 %#ok<EQEFF> % OK
nm(6) = pi % BUG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

methods(nm)
%{
Methods for class cl_nmatrix:

cl_nmatrix         ge                 lt                 setDeleteOnDelete  size
cos                get                ne                 setSizeIn          subsref
eq                 gt                 numel              setWritable        tan
evalOperator2      le                 plus               sin

Methods of cl_nmatrix inherited from handle.
%}

exist(s.mat.Filename, 'file')
clear nm
exist(s.mat.Filename, 'file') % TODO : pas de suppression immédiate du fichier !!!!!!!!!!!!



%% Create and initialize a cl_nmatrix with a . mat temporary mapped file with myVar variable name

nm = cl_nmatrixfactory.createInstance('Value', [1 2 3 4; 5 6 7 8], 'Ext', '.mat', 'VarName', 'myVar');

nm % OK
s = struct(nm);
s.mat
%{
ans = 
  2×4 nmatrix array with properties:
      Filename: 'C:\Temp\tmp_20200310_165344_683415.mat'
    FileFormat: 'MAT-File -v6'
       DataSet: '/myVar'
          Mode: 'memmapfile'
    TargetType: 'double'
           Map: [1×1 memmapfile]
       GPUMode: 'off'
     GPUTarget: 0
%}

%% Create and initialize a cl_nmatrix with default mapped file and Dimension order vector (invert col/row)

nm = cl_nmatrixfactory.createInstance('Value', [1 2 3 4; 5 6 7 8], 'Dim', [2 1]);

% TODO : tester avec fichier Er-Mapper


%% Create and initalize a cl_matrix with a specific mapped file

nm = cl_memmapfile('FileName', my_tempname('.nmatrix'), 'Value', [1 2 3 4; 5 6 7 8]);

s = struct(nm)
%{
s = 
  struct with fields:
       FileName: 'C:\Temp\tmp_20200310_170254_270270.nmatrix'
       Writable: 1
         ValNaN: NaN
              m: [1×1 memmapfile]
           Size: [2 4]
         Format: 'double'
       ErMapper: 0
    DoNotDelete: 0
    %}

%% Create a cl_nmatrix with default value and size, format double (default)

nm = cl_nmatrixfactory.createInstance('Value', 0, 'Size', [2, 4]);
nm % OK

%% Create a cl_nmatrix with default value and size, format single

nm = cl_nmatrixfactory.createInstance('Value', 0, 'Size', [20 20], 'Format', 'single');

s = struct(nm);
s.mat
%{
ans = 
  20×20 nmatrix array with properties:
      Filename: 'C:\Temp\tmp_20200310_170422_821721.memmapfile'
    FileFormat: 'binary'
       DataSet: ''
          Mode: 'memmapfile'
    TargetType: @single
           Map: [1×1 memmapfile]
       GPUMode: 'off'
     GPUTarget: 0
     %}

%% Create a cl_nmatrix from variable name 'vals' of an existing 'vals.mat' file

nm = cl_nmatrixfactory.createInstance('FileName', 'vals.mat', 'VarName', 'vals')

% TODO

%% Create a cl_nmatrix from a Image.bin file: 358x901 'single' data and invert rows/cols

nm = cl_nmatrixfactory.createInstance('FileName', 'Image.bin', 'Format', 'single', 'Size', [358 901], 'Dim', [2 1]) %#ok<*NASGU>

% TODO

%% Create a cl_nmatrix from a .bil file, reorder dimension  and invert

flip direction on dimension 1
nm = cl_nmatrixfactory.createInstance('FileName', filenameBil, 'Format', class(X), 'Size', size(X), 'Dim', [2 3 1], 'Dimension', [-1 1 1])

% TODO
