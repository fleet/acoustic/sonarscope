% Methode de gestion des callback du composant
%
% Syntax
%   [flag, ListeFic, lastDir] = uiSelectFiles(...)
%
% Name-Value Pair Arguments
%   Entete           : (IFREMER - SonarScoprpe par d�faut)
%   RepDefaut        : r�pertoire par d�faut
%   ExtensionFiles   : tableau de cellules des extensions de fichiers.
%   InitGeometryType : Type de g�om�trie (Unknown par d�faut)
%   InitDataType     : Type de donn�e (Unknown par d�faut)
%   ChaineIncluse    : 
%   ChaineExclue     : 
%
% Output Arguments
%   flag     : Validation (1 ou 0)
%   ListeFic : Liste des fichiers
%   lastDir  : Dernier r�pertoire prospect�
%
% EXAMPLE :
%   [flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.tif', 'RepDefaut', my_tempdir)
%   [flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'}) 
%   [flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'}, 'AllFilters', 1) 
%
% See also survey_uiSelectFiles Authors
% Authors : GLU, JMA
% ----------------------------------------------------------------------------

function [flag, ListeFic, lastDir] = uiSelectFiles(varargin)

Entete = Lang('Explorateur de fichiers SonarScope', 'SonarScope File Browser');

[varargin, InitGeometryType]   = getPropertyValue(varargin, 'InitGeometryType',   1);
[varargin, InitDataType]       = getPropertyValue(varargin, 'InitDataType',       1);
[varargin, ChaineIncluse]      = getPropertyValue(varargin, 'ChaineIncluse',      []);
[varargin, ChaineExclue]       = getPropertyValue(varargin, 'ChaineExclue',       []);
[varargin, flagAllFilters]     = getPropertyValue(varargin, 'AllFilters',         0);
[varargin, RepDef]             = getPropertyValue(varargin, 'RepDefaut',          []);
[varargin, ExtensionFiles]     = getPropertyValue(varargin, 'ExtensionFiles',     []);
[varargin, FileDefaut]         = getPropertyValue(varargin, 'FileDefaut',         '');
[varargin, MultiSelection]     = getPropertyValue(varargin, 'MultiSelection',     1);
[varargin, EnableSelection]    = getPropertyValue(varargin, 'EnableSelection',    1);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0);
[varargin, MaxLevel]           = getPropertyValue(varargin, 'MaxLevel',           1);
[varargin, flagFalseIfNoFiles] = getPropertyValue(varargin, 'FalseIfNoFiles',     1);
[varargin, Entete]             = getPropertyValue(varargin, 'Entete',             Entete);
[varargin, SubTitle]           = getPropertyValue(varargin, 'SubTitle',           []);
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []); %#ok<ASGLU> 
% [uiAccess, varargin] = getPropertyValue(varargin, 'Access', 'Start');

dosNetUse('Init')

if ischar(ExtensionFiles)
    ExtensionFiles = {ExtensionFiles};
end

ExtFiles = {};
for k=1:length(ExtensionFiles)
    ExtFiles{end+1} = ['*' ExtensionFiles{k}]; %#ok<AGROW>
    ExtFiles{end+1} = ExtensionFiles{k}(2:end); %#ok<AGROW>
end

if flagAllFilters && (length(ExtFiles) > 2)
    ExtFiles{end+1} = Lang('Tous les filtres','All Filters');
    ExtFiles{end+1} = 'xxx';
end

if ~isempty(SubTitle2)
    SubTitle = [SubTitle ' - ' SubTitle2];
end
if ~isempty(SubTitle)
    Entete = [Entete ' - ' SubTitle];
end

a = cla_repertoire('ExtensionFiles', ExtFiles, 'RepDefaut', RepDef, ...
     'LabelTitle',      Entete, ...
     'GeometryType',    InitGeometryType, ...
     'DataType',        InitDataType, ...
     'ChaineIncluse',   ChaineIncluse, ...
     'ChaineExclue',    ChaineExclue, ...
     'FileDefaut',      FileDefaut, ...
     'MultiSelection',  MultiSelection, ...
     'EnableSelection', EnableSelection, ...
     'MaxLevel',        MaxLevel, ...
     'FileNameVisible', 1);
a = editobj(a);

flag = get(a, 'flagValid');
if flag
    ListeFic = get(a, 'listeFic');
    
    for k=1:length(ListeFic)
        ListeFic{k} = windows_replaceVolumeLetter(ListeFic{k});
    end
    
    if isempty(ListeFic)
        lastDir  = get(a, 'lastDir');
    else
        lastDir = fileparts(ListeFic{end});
    end
else
    ListeFic = [];
    lastDir  = RepDef;
end
drawnow
dosNetUse('Clear')

if MessageParallelTbx
    flag = messageParallelTbx(length(ListeFic) > 1, 'Potentially', MessageParallelTbx == 2);
end

warning_blankInFileName(ListeFic)
drawnow

if flagFalseIfNoFiles && isempty(ListeFic)
    flag = 0;
end