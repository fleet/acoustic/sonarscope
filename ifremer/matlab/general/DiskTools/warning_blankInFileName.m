function warning_blankInFileName(ListeFic)

if ischar(ListeFic)
    ListeFic = {ListeFic};
end

for k=1:length(ListeFic)
    if ~isempty(strfind(ListeFic{k}, ' '))
        str1 = sprintf('Attention, le fichier "%s"\ncontient un blanc. Ceci n''est pas recommand�. Je vous recommande de le renommer.\nSi vous avez tout un ensemble de fichiers � renommer, vous pouvez utiliser l''utilitaire de SonarScope : Info / Utilities / Rename a set of file.\nVous pouvez cependant continuer sans prendre cette remarque en consid�ration.', ListeFic{k});
        str2 = sprintf('Warning : the file name "%s"\ncontents a blank. This is not recommended. I suggest that you rename it.\nIf you have a set of files to rename you can use SonarScope : Info / Utilities / Rename a set of file.\nNevertheless you can continue without taking into account this warning.', ListeFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'warningBlankInFileName');
        break
    end
end
