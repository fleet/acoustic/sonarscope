% Remplacement de la lettre d'un volume r�seau sous windows
%
% Syntax
%   nomFicOut = windows_replaceVolumeLetter(nomFicIn)
%
% Input Arguments
%   nomFicIn : Nom du fichier ou du r�pertoire en entr�e
%
% Name-Value Pair Arguments
%   NetUseWords : Answer of the doc command "net use" splitted in words
%
% Output Arguments
%   nomFicOut   : Nom du fichier ou du r�pertoire en sortie
%   NetUseWords : Answer of the doc command "net use" splitted in words
%
% Remarks : NetUseWords is used in a loop just to avoid to recall the dos
%           commant N times because it is time comsuming
%
% Examples
%	nomFicOut = windows_replaceVolumeLetter('C:')
%	nomFicOut = windows_replaceVolumeLetter('X:')
%	[nomFicOut, NetUseWords] = windows_replaceVolumeLetter(nomFicIn, 'NetUseWords', NetUseWords);
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function [nomFicOut, NetUseWords] = windows_replaceVolumeLetter(nomFicIn, varargin)

global DosNetUseValue %#ok<GVMIS> 

if isempty(nomFicIn)
    nomFicOut = nomFicIn;
    return
end

[varargin, NetUseWords] = getPropertyValue(varargin, 'NetUseWords', []); %#ok<ASGLU>

nomFicOut = nomFicIn;
Lettre = nomFicIn(1:2);
if strcmp(Lettre, '\\')
    return
end
if isempty(NetUseWords)
    if isempty(DosNetUseValue)
        dosNetUse('Init')
    end
    NetUseWords = DosNetUseValue;
end
for k=1:length(NetUseWords)
    if ~isempty(find(contains(NetUseWords{k}, Lettre), 1))
        mots = strsplit(NetUseWords{k});
        ind = find(contains(mots, Lettre));
        if ~isempty(ind)
            nomFicOut = [mots{ind+1} nomFicIn(3:end)];
            break
        end
    end
end
