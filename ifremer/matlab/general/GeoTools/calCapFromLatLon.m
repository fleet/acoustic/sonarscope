% Cap le long d'une route (cap(i) calcule entre Pt(i) et Pt(i+1)
%
% Syntax
%   cap = calCapFromLatLon(lat, lon, ...)
%
% Input Arguments
%   lat : latitudes
%   lon : longitudes
%
% Name-Value Pair Arguments
%   time : Temps associ� � chaque point de navigation (utile si la
%   navigation provient d'un fichier ErMapper o� le premier enregistrement
%   correspond au dernier ping, le cap est alors d�cal� de 180 degr�s)
%
% Output Arguments
%   cap : Cap le long du chemin.
%
% Examples
%   cap = calCapFromLatLon([0 0 1 1 0], [0 1 1 0 0])
%
% See also calCapFromXY Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [cap, speed, distance, acceleration] = calCapFromLatLon(lat, lon, varargin)

[varargin, Time]             = getPropertyValue(varargin, 'Time',             []);
[varargin, method]           = getPropertyValue(varargin, 'method',           1);
[varargin, TempsIntegration] = getPropertyValue(varargin, 'TempsIntegration', []); %#ok<ASGLU>

if length(lat) < 2
    cap          = NaN;
    speed        = NaN;
    distance     = NaN;
    acceleration = NaN;
    return
end

lat1 = lat(1:end-1);
lon1 = lon(1:end-1);

lat2 = lat(2:end);
lon2 = lon(2:end);
l    = length(lat);


if ~isempty(Time)
    switch class(Time)
        case 'datetime'
            t = datenum(Time);
        case 'cl_time'
            t = Time.timeMat;
        case 'double'
            t = Time;
        otherwise
            t = Time;
    end
end

switch method
    case 1
        cap = azimuth([lat1(:) lon1(:)],[lat2(:) lon2(:)]);
        cap(end+1) = cap(end);
        
    case 2
        % Modif Glt : calcul sur fen�tre glissante
                
        temps = abs((t(2:end) - t(1:end-1)) * (3600*24));
        
        cap = NaN(size(lat), 'single');
        for i=2:l-1
            %Pour �tre s�r qu'il existe au moins 1 M1 et 1 M2
            %et ne pas flaguer le premier et le dernier point � chaque fois
            
            
            %Coordonn�es du point courant
            latM = lat(i);
            lonM = lon(i);
            
            %Recherche du point Pr�c�dent
            dt      = 0;
            iM1     = 0;
            j       = 0;
            while (dt <= TempsIntegration) && ((i-1-j) >= 1) %On reste dans la fen�tre et on ne va plus loin en arri�re qu'on ne peut
                dt = dt + temps(i-1-j);
                if dt <= TempsIntegration
                    iM1 = j+1;
                end
                j = j + 1;
            end
            
            % Recherche du point Suivant
            dt  = 0;
            iM2 = 0;
            k   = 0;
            while (dt <= TempsIntegration) && ((i+k) <= length(temps)) %On reste dans la fen�tre et on ne va plus loin en avant qu'on ne peut
                dt = dt + temps(i+k);
                if dt <= TempsIntegration
                    iM2 = k+1;
                end
                k = k + 1;
            end
            
            %Interruption
            if iM1 == 0 %On a pas trouv� de point M1 dans la fenetre
                
            end
            
            %Interruption
            if iM2 == 0 %On a pas trouv� de point M2 dans la fenetre
                
            end
            
            if (iM1 > 0) && (iM2 > 0)
                %Coordonn�es du point M1
                latM1 = lat(i-iM1);
                lonM1 = lon(i-iM1);
                
                %Coordonn�es du point M2
                latM2 = lat(i+iM2);
                lonM2 = lon(i+iM2);
                
                capM1M = azimuth([latM1 lonM1],[latM lonM]);
                capMM2 = azimuth([latM lonM], [latM2 lonM2]);
                cap(i) = mean([capM1M capMM2]);
            end
            
        end
        cap(1)   = cap(2);
        cap(end) = cap(end-1);
end

%% Calcul des distances inter pings

distance = distLatLon(lat, lon);
distance = [distance 0]';

%% Fin

if isempty(Time)
    speed = [];
    acceleration = [];
else
    if t(end) < t(1)
        cap = mod(cap + 180, 360);
    end
    t1 = t(1:end-1);
    t2 = t(2:end);
    deltaT  = [(t2(:) - t1(:)) * (24*3600); 0]; % en s.
    speed = (distance ./ deltaT) * 1.9438612860586; % en m/s
    acceleration = (gradient(speed) ./ deltaT);
end
