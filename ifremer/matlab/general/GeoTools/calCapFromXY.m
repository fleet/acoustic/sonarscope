% Cap le long d'une route (cap(i) calcule entre Pt(i) et Pt(i+1)
%
% Syntax
%   cap = calCapFromXY(x, y)
%
% Input Arguments
%   x : abscisses
%   y : ordonnees
%
% Output Arguments
%   cap : Cap le long du chemin.
%
% Examples
%   cap = calCapFromXY([0 0 1 1 0], [0 1 1 0 0])
%
% See also calCapFromLatLon Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function cap = calCapFromXY(x, y)

dx = diff(x, 1);
dy = diff(y, 1);
dx(end+1, end) = dx(end, end);   % Pas tres heureux
dy(end+1, end) = dy(end, end);   % Pas tres heureux

cap = mod(360 + 90 - atan2(dy, dx) * 180 / pi, 360);
cap = cap';
