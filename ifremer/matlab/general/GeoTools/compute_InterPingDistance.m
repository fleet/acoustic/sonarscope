function D = compute_InterPingDistance(Lat, Lon, varargin)

[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []);
[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>


[~, D] = legs(Lat, Lon);
D = D * (nm2km(1) * 1000);
D(end+1) = D(end);

% subNonNaN = find(~isnan(xFish));
subNonNaN = find(~isnan(D));
% D(subNonNaN) = sqrt(gradient(xFish(subNonNaN)).^2 + gradient(yFish(subNonNaN)).^2);
% D(subNonNaN(2:end)) = D(subNonNaN(2:end)) ./ diff(subNonNaN);

n = 5;
if length(subNonNaN) > (3*n)
    D(subNonNaN) = filtfilt(ones(1,n)/n,1, D(subNonNaN));
    D(subNonNaN(1:n)) = D(subNonNaN(n));
    D(subNonNaN(end-(n-1):end)) = D(subNonNaN(end-(n-1)));
end

if ~isempty(Fig)
    figure(Fig)
    D(D > 200) = NaN;
    [N, bins] = histo1D(D);
    sub = (N ~= 0);
    figure(87698); semilogy(bins, N, '-');
    grid on; hold on;
    title('Histogram of Distance Inter Pings')
    semilogy(bins(sub), N(sub), '*');
end
