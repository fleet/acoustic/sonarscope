% Transformation geodesique de coordonnees geographiques en utm
%
% Syntax
%   [x_utm, y_utm] = geo2utm(lon, lat)
% 
% Input Arguments 
%   lat : Latitude
%   lon : Longitude
%
% Output Arguments
%   x_utm : Absisse.
%   y_utm : Ordonnee
% 
% Examples
%   [x_utm, y_utm] = geo2utm(-5, 48)
%
% See also latlon2xy_approx xy2LatLon Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [x_utm, y_utm] = geo2utm(lon, lat)
 
mstruct       = defaultm('utm');
mstruct.zone  = utmzoneui;
mstruct.geoid = utmgeoid(mstruct.zone);
mstruct       = defaultm(utm(mstruct));

[x_utm, ~] = mfwdtran(mstruct, lat(1), lon);
[~, y_utm] = mfwdtran(mstruct, lat ,lon(1));
