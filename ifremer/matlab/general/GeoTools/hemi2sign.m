function sig = hemi2sign(H)

sig = ones(size(H));
for k=1:length(H)
    if strcmpi(H{k}, 'S') || strcmpi(H{k}, 'W') || strcmpi(H{k}, 'O')
        sig(k) = -1;
    end
end
