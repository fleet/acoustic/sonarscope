function s = lat2strDELPHINS(lat)

%{
UTC Date	Time			Latitude			Longitude			Depth		Heading		Pitch		Roll		Heave
10/10/2013 12:15:48.903900	48�18.239635918721	-004�24.263290360084	-11.17638	283.49878	-2.01303	-0.00086	0.13602
10/10/2013 12:15:49.403900	48�18.239770752816	-004�24.264103996104	-11.13996	283.42835	-1.98589	-0.02135	0.14184
10/10/2013 12:15:49.903900	48�18.239905347938	-004�24.264917364573	-11.10531	283.36205	-1.94953	-0.02763	0.14615
%}

if lat >= 0
    hemisphere = '';
else
    hemisphere = '-';
    lat = -lat;
end
degre = floor(lat);
minute = (lat - degre) * 60;

s = sprintf('%s%02d�%15.12f', hemisphere, degre, minute);
