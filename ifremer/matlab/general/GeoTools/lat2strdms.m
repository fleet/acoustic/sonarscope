% Conversion d'une latitude en string
%
% Syntax
%   s = lat2strdms(lat)
%   s = lat2strdms(lat, separateur)
%
% Input Arguments
%   lat : latitude de degres decimaux
%
% Name-Value Pair Arguments
%   separateur : Separation souhaitee : ' ' par defaut, '\t' pour tabulation
%
% Examples 
%   lat2strdms(45.25)
%   lat2strdms(45.25:0.01:45.50 )
%   lat2strdms((45.25:0.01:45.50)', ',')
%   lat2strdms((45.25:0.01:45.50)', '\t')
%
%   lat2strdms(45.25, ' ', '09.6f')
%
% See also lat2str lon2strdms str2lat lon2str str2lon Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function s = lat2strdms(lat, varargin)

if iscell(lat)
    lat = lat{:};
end

if isempty(lat)
    s = '[]';
    return
end

switch nargin
    case 1
        separateur = ' ';
        FormatSecondes = '07.4f';
    case 2
        separateur = varargin{1};
        FormatSecondes = '07.4f';
    case 3
        separateur     = varargin{1};
        FormatSecondes = varargin{2};
end

s = cell(size(lat));
for i=1:numel(lat)
    x = lat(i);
    if x >= 0
        hemisphere = 'N';
    else
        hemisphere = 'S';
        x = -x;
    end
    
    dms = my_degrees2dms(x+1000*eps);
    
    if strcmp(separateur, '\t')
        Format = ['%s\t%02d\t%02d\t%' FormatSecondes];
        s{i} = sprintf(Format, hemisphere, dms(1), dms(2), dms(3));
    else
        Format = ['%s%c%02d%c%02d%c%' FormatSecondes];
        s{i} = sprintf(Format, hemisphere, separateur, dms(1), separateur, dms(2), separateur, dms(3));
    end
end


if length(s) == 1
    s = s{1};
end
