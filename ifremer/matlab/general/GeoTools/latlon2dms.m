% Conversion d'un angle en degr�, minute, seconde d�cimale
%
% Syntax
%   [degre, minute, seconde] = latlon2dms(x)
%
% Input Arguments
%   x : Valeur num�rique d'un angle
%
% Output Arguments
%   degre   : Degr� entier
%   minute  : Minute enti�re
%   seconde : seconde d�cimale
%
% Examples 
%   [degre, minute, seconde] = latlon2dms(pi)
%
% See also str2lat lon2str str2lon Authors
% Authors : JMA
% VERSION  : $Id: lat2str.m,v 1.3 2002/06/06 12:00:13 augustin Exp $
%--------------------------------------------------------------------------

function [degre, minute, seconde] = latlon2dms(x)

signe = sign(x);
x = abs(x);
degre = floor(x);
minute = (x - degre) * 60;

x = minute;
minute = floor(x);
seconde = (x - minute) * 60;
if nearlyEqual(seconde, 60, 1e-6)
    minute = minute + 1;
    seconde = seconde - 60;
end

degre = signe * degre;
