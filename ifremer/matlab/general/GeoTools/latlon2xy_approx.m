% Conversion de coordonnees geographiques en coordonnees metriques (Formule ultra simpliste)
%
% Syntax
%   [x, y] = latlon2xy_approx(lat, lon)
%   [x, y] = latlon2xy_approx(lat, lon, 'latRef', latRef)
%
% Input Arguments
%   lat : Latitude (deg).
%   lon : Longitude (deg).
%
% Name-Value Pair Arguments
%   'latRef' : Latitude d'echelle conservee. (Moyenne des latitudes par defaut)
%
% Output Arguments
%   x : Abscisses (m) (origine = greenwitch)
%   y : Ordonnees (m) (origine = equateur)
%
% Examples
%   lat = 0:5:50; lon = lat; latRef = mean(lat);
%   [x, y] = latlon2xy_approx(lat, lon, 'latRef', latRef)
%   [lat, lon] = xy2latlon_approx(x, y, 'latRef', latRef)
%
% See also xy2latlon_approx geo2utm Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [x, y] = latlon2xy_approx(lat, lon, varargin)

[varargin, latRef] = getPropertyValue(varargin, 'latRef', mean(lat, 'omitnan')); %#ok<ASGLU>

RayonTerre = 6378137;
D2R = pi / 180;

x = RayonTerre * cos(latRef .* D2R) .* lon * D2R;
y = RayonTerre * (lat * D2R);
