% Conversion d'une longitude en chaine de caract�res avec 6 chiffres derri�re la virgule pour les minutes d�cimales
%
% Syntax
%   s = lon2str( longitude, *separateur )
%
% Input Arguments
%   longitude  : longitudeitude de degres decimaux
%
% Name-Value Pair Arguments
%   separateur : Separation souhaitee : ' ' par defaut, '\t' pour tabulation
%
% Examples 
%   lon2str(45.25)
%   lon2str(0:50:360)
%   lon2str((0:50:360)', ';')
%   lon2str((0:50:360)', '\t')
%
%   lon2str(45.25, ' ', '09.6f')
%
% See also str2lon lat2str Authors
% Authors : JMA
% VESION   : $Id: lon2str.m,v 1.2 2002/06/06 12:00:13 augustin Exp $
%--------------------------------------------------------------------------------

function s = lon2str6( longitude, varargin )

if iscell(longitude)
    longitude = longitude{:};
end

if isempty(longitude)
    s = '[]';
    return
end

switch nargin
    case 1
        separateur = ' ';
        FormatSecondes = '09.6f';
    case 2
        separateur = varargin{1};
        FormatSecondes = '09.6f';
    case 3
        separateur     = varargin{1};
        FormatSecondes = varargin{2};
end

longitude = mod(longitude+360, 360);

s = cell(size(longitude));
for i=1:numel(longitude)
    x = longitude(i);
    if x <= 180
        greenwicht = 'E';
    else
        greenwicht = 'W';
        x = 360 - x;
    end
    degre = floor( x );
    minute = (x - degre) * 60.;
    
    if strcmp(separateur, '\t')
        Format = ['%s\t%03d\t%' FormatSecondes];
        s{i} = sprintf(Format, greenwicht, degre, minute);
    else
        Format = ['%s%c%03d%c%' FormatSecondes];
        s{i} = sprintf(Format, greenwicht, separateur, degre, separateur, minute);
    end
end

if length(s) == 1
    s = s{1};
end
