% Conversion d'une lonitude en string
%
% Syntax
%   s = lon2strdms(lon)
%   s = lon2strdms(lon, separateur)
%
% Input Arguments
%   lon : lonitude de degres decimaux
%
% Name-Value Pair Arguments
%   separateur : Separation souhaitee : ' ' par defaut, '\t' pour tabulonion
%
% Examples 
%   lon2strdms(45.25 )
%   lon2strdms(45.25:0.01:45.50 )
%   lon2strdms((45.25:0.01:45.50)', ',')
%   lon2strdms((45.25:0.01:45.50)', '\t')
%
%   lon2strdms(45.25, ' ', '09.6f')
%
% See also lon2str lat2strdms str2lon lon2str str2lon Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function s = lon2strdms(lon, varargin)

if iscell(lon)
    lon = lon{:};
end

if isempty(lon)
    s = '[]';
    return
end

switch nargin
    case 1
        separateur = ' ';
        FormatSecondes = '07.4f';
    case 2
        separateur = varargin{1};
        FormatSecondes = '07.4f';
    case 3
        separateur     = varargin{1};
        FormatSecondes = varargin{2};
end

s = cell(size(lon));
for i=1:numel(lon)
    x = lon(i);
    if x < 0
        x = x + 360;
    end
    if x <= 180
        greenwicht = 'E';
    else
        greenwicht = 'W';
        x = 360 - x;
    end
    
    dms = my_degrees2dms(x);
    if dms(3) == 60
        dms(2) = dms(2) + 1;
        dms(3) = 0;
    end
    
    if strcmp(separateur, '\t')
        Format = ['%s\t%02d\t%02d\t%' FormatSecondes];
        s{i} = sprintf(Format, greenwicht, dms(1), dms(2), dms(3));
    else
        Format = ['%s%c%02d%c%02d%c%' FormatSecondes];
        s{i} = sprintf(Format, greenwicht, separateur, dms(1), separateur, dms(2), separateur, dms(3));
    end
end


if length(s) == 1
    s = s{1};
end
