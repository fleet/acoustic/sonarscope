% Conversion de coordonnees geographiques en coordonnees metriques (projection Mercator).
%
% Syntax
%   [x, y, scaleFactor] = mercat_geo2xy(lat, lon, ...)
%
% Input Arguments
%   lat : Latitude (deg).
%   lon : Longitude (deg).
%
% Name-Value Pair Arguments
%   latRef : Latitude  d'echelle conservee. (0 par defaut)
%   lonRef : Longitude d'echelle conservee. (0 par defaut)
%   geoid  : [demi-grand_axe  excentricite] ([6378.13700000000   0.08181919104] par defaut.
%                                           Valeurs obtenues par : almanac('earth', 'geoid'))
%   angle  : angle de rotation apres projection (donner le cap des profils en degres)
%
% Output Arguments
%   x           : Abscisses (km)
%   y           : Ordonnees (km)
%   scaleFactor : facteur d'echelle
%
% Examples
%   lat = 45; lon = 30;
%   [x, y, scaleFactor] = mercat_geo2xy(lat, lon)
%   [x, y, scaleFactor] = mercat_geo2xy(lat, lon, 'LatRef', 45)
%
%   for i=1:1000
%       [x, y] = mercat_geo2xy(lat, lon);
%       [lat, lon] = mercat_xy2geo(x, y);
%   end
%   lat2str(lat)
%   lon2str(lon)
%
%   lat = -80:80; lon = zeros(size(lat));
%   [x, y] = mercat_geo2xy(lat, lon)
%   plot(y); grid on;
%   [x, y] = mercat_geo2xy(lat, lon, 'latRef', 45)
%   hold on; plot(y, 'r'); grid on;
%
% See also mercat_xy2geo Authors
% References : Code provenant du logiciel Caraibes IFREMER/DNIS
% Authors : JMA
%-------------------------------------------------------------------------------

function [x, y, scaleFactor] = mercat_geo2xy(lat, lon, varargin)

npt = length(lat);
if npt == 0
    x = [];
    y = [];
    return
end
if npt ~= length(lon)
    my_warndlg('mercat_geo2xy', 'Les 2 vecteurs doivent avoir la meme taille', 1);
    x = [];
    y = [];
    return
end

[varargin, latitudeCentral] = getPropertyValue(varargin, 'latRef', 0);
[varargin, meridienCentral] = getPropertyValue(varargin, 'lonRef', 0);
[varargin, geoid]           = getPropertyValue(varargin, 'geoid',  [6378.137 0.08181919104]);
[varargin, angle]           = getPropertyValue(varargin, 'angle',  0); %#ok<ASGLU>

CIB_CCO_DEGREE_RADIAN = pi / 180;

l_rLat = lat * CIB_CCO_DEGREE_RADIAN;

half_A  = geoid(1);
excent2 = geoid(2) ^ 2;
CIB_CCO_PI_S_4_RADIAN = pi / 4;
CIB_CCO_PI_DEGREE = 180;
CIB_CCO_2_PI_DEGREE = 360;
uE = geoid(2); % uE = sqrt(excent2);
uED = uE / 2.0;
l_dSL = sin(latitudeCentral * CIB_CCO_DEGREE_RADIAN);
l_dSL = l_dSL ^ 2;
denorm = sqrt((1.0 - (excent2 * l_dSL)) / (1.0 - l_dSL));
uA = half_A / denorm;

%% Calcul de x

l_uL = lon - meridienCentral;
sub = find(l_uL < -CIB_CCO_PI_DEGREE);
l_uL(sub) = l_uL(sub) + CIB_CCO_2_PI_DEGREE;
sub = find(l_uL > CIB_CCO_PI_DEGREE);
l_uL(sub) = l_uL(sub) - CIB_CCO_2_PI_DEGREE;
x = uA * (l_uL * CIB_CCO_DEGREE_RADIAN);

%% Calcul de y

luEDS = uE * sin(l_rLat);
l_rLat = CIB_CCO_PI_S_4_RADIAN + (l_rLat / 2.0);
y = uA * (log(sin(l_rLat) ./ cos(l_rLat)) + (uED * log((1.0 - luEDS) ./ (1.0 + luEDS))));


%% Passage en m

% x = x * 1e3;
% y = y * 1e3;

%% Rotation du repere

if angle~=0
    if angle > 0
        alpha = (90 - angle) * CIB_CCO_DEGREE_RADIAN;
    else
        alpha = -angle * CIB_CCO_DEGREE_RADIAN;
    end
    cosi = cos(alpha);
    sinu = sin(alpha);
    xr =  cosi * x + sinu * y;
    yr = -sinu * x + cosi * y;
    x = xr;
    y = yr;
end

%% Calcul du facteur d'echelle

l_lat = lat * CIB_CCO_DEGREE_RADIAN;
l_lat = sin(l_lat);
l_lat = l_lat .* l_lat;
l_norm = sqrt((1.0 - (excent2 * l_lat)) / (1.0 - l_lat));
scaleFactor = l_norm / denorm;
