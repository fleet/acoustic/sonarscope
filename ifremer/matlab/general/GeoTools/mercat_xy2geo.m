% Conversion de coordonnees geographiques en coordonnees metriques (projection Mercator).
%
% Syntax
%   [lat, lon, scaleFactor] = mercat_xy2geo(x, y, ...)
%
% Input Arguments
%   x : Abscisses (km)
%   y : Ordonnees (km)
%
% Name-Value Pair Arguments
%   latRef : Latitude  d'echelle conservee. (0 par defaut)
%   lonRef : Longitude d'echelle conservee. (0 par defaut)
%   geoid  : [demi-grand_axe  excentricite] ([6378.13700000000   0.08181919104] par defaut.
%                                           Valeurs obtenues par : almanac('earth', 'geoid'))
%   angle  : angle de rotation apres projection (donner le cap des profils en degres)
%
% Output Arguments
%   lat         : Latitude (deg).
%   lon         : Longitude (deg).
%   scaleFactor : facteur d'echelle
%
% Examples
%   lat = [45 45]; lon = [0 1/60]; latRef = 0;
%   [x, y] = mercat_geo2xy(lat, lon, 'latRef', latRef)
%   [lat, lon, scaleFactor] = mercat_xy2geo(x, y, 'latRef', latRef)
%
% See also mercat_geo2xy Authors
% References : Code provenant du logiciel Caraibes IFREMER/DNIS
% Authors : JMA
%-------------------------------------------------------------------------------

function [lat, lon, scaleFactor] = mercat_xy2geo(x, y, varargin)

npt = length(x);
if npt == 0
    lat = [];
    lon = [];
    return
end
if npt ~= length(y)
    my_warndlg('mercat_xy2geo', 'Les 2 vecteurs doivent avoir la meme taille', 1);
    lat = [];
    lon = [];
    return
end

[varargin, latitudeCentral] = getPropertyValue(varargin, 'latRef', 0);
[varargin, meridienCentral] = getPropertyValue(varargin, 'lonRef', 0);
[varargin, geoid]           = getPropertyValue(varargin, 'geoid',  [6378.137 0.08181919104]);
[varargin, angle]           = getPropertyValue(varargin, 'angle',  0); %#ok<ASGLU>

half_A  = geoid(1);
excent2 = geoid(2) ^ 2;
CIB_CCO_DEGREE_RADIAN = pi / 180;
CIB_CCO_RADIAN_DEGREE = 180 / pi;
CIB_CCO_PI_DEGREE = 180;
CIB_CCO_2_PI_DEGREE = 360;
l_dSL = sin(latitudeCentral * CIB_CCO_DEGREE_RADIAN);
l_dSL = l_dSL .^ 2;
denorm = sqrt((1.0 - (excent2 * l_dSL)) / (1.0 - l_dSL));
uA = half_A / denorm;
uA2 = 2.0 * uA;
l_dSL = excent2 * excent2;
uB1 = excent2 * (1.0 + excent2 + l_dSL) / 2.0;
uB3 = -l_dSL * (1.0 + (21.0 / 8.0 * excent2)) / 3.0;
uB5 = 13.0 / 3e1 * l_dSL * excent2;

%% Conversion en m

% x = x / 1e3;
% y = y / 1e3;

%% Rotation du repere

if angle~=0
    if angle > 0
        alpha = (90 - angle) * CIB_CCO_DEGREE_RADIAN;
    else
        alpha = -angle * CIB_CCO_DEGREE_RADIAN;
    end
    cosi = cos(alpha);
    sinu = sin(alpha);
    xr = cosi * x - sinu * y;
    yr = sinu * x + cosi * y;
    x = xr;
    y = yr;
end

%% Calcul de la longitude en fonction de l'abscisse x

l_long = ((x / uA) * CIB_CCO_RADIAN_DEGREE) + meridienCentral;
sub = find(l_long < -CIB_CCO_PI_DEGREE);
l_long(sub) = l_long(sub) + CIB_CCO_2_PI_DEGREE;
sub = find(l_long > CIB_CCO_PI_DEGREE);
l_long(sub) = l_long(sub) - CIB_CCO_2_PI_DEGREE;
lon = l_long;

%% Calcul de la latitude en fonction de y

l_uT = tanh(y / uA2);
l_uS = 2.0 * l_uT ./ (1.0 + (l_uT .* l_uT));
luB = (uB1 * l_uS) + (uB3 * (l_uS .^ 3)) + (uB5 * (l_uS .^ 5));
l_lat = 2.0 * atan2(luB + l_uT, 1.0 + (luB .* l_uT));
lat = l_lat * CIB_CCO_RADIAN_DEGREE;

%% Calcul du facteur d'echelle

l_lat = sin (l_lat);
l_lat = l_lat .* l_lat;
l_norm = sqrt((1.0 - (excent2 * l_lat)) / (1.0 - l_lat));
scaleFactor = l_norm / denorm;

