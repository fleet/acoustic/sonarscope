% Conversion d'une latitude ou longitude donn�e en �l�ments s�par�s
%
% Syntax
%   x = num2latlon(hemi, deg, min)
%
% Input Arguments 
%   hemi : 'N', 'n', 'S' , 's', 'W', 'w', 'E' et 'e'
%   deg  : degre
%   min  : minutes reelles
% 
% Output Arguments
%   x : Latitude ou longitude.
%
% Examples
%   lat = num2latlon('N', 48, 55.380)
%
% See also str2lat str2lon Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function lat = num2latlon(hemi, deg, min, varargin)

if isempty(hemi) || isempty(deg) || isempty(min)
    lat = NaN;
    return
end

switch nargin
    case 3
        lat = deg + min / 60;
    case 4
        lat = deg + min / 60 + varargin{1} / 3600;
end

switch lower(hemi)
case 'n'
	return
case 'e'
	return
case 's'
	lat = -lat;
	return
case 'w'
	lat = -lat;
	return;
otherwise
	disp('Code de latitude non reconnu dans num2latlon');
	lat = NaN;
end
