function [hCurve, hAxe, hFig] = plotNavMercator(Lat, Lon, varargin)

[varargin, Time]     = getPropertyValue(varargin, 'Time',     []);
[varargin, FileName] = getPropertyValue(varargin, 'FileName', []); %#ok<ASGLU>

latLim = [min(Lat) max(Lat)];
lonLim = [min(Lon) max(Lon)];

%% A l'ancienne

coast = load('coast.mat');
hFig = FigUtils.createSScFigure;

if isempty(Time)
    hCurve = plot(Lon, Lat, '-*'); grid on; hold on;
else
    hFig = plot_navigation(FileName, [], Lon, Lat, Time.timeMat, []);
    hCurve = gco;
end
hAxe = gca;
plot(coast.long, coast.lat, 'k');
hold off;
set(hAxe, 'XLim', lonLim, 'YLim', latLim);
return


%% Trac� du fond de carte

PLabelRound = round(log10(diff(latLim))) - 2;
MLabelRound = round(log10(diff(lonLim))) - 2;

figure;
axesm('mercator', 'MapLatLimit', latLim, 'MapLonLimit', lonLim,...
    'Frame', 'on', 'Grid', 'on', 'MeridianLabel', 'on', 'ParallelLabel', 'on')
axis off
GCA = gca;
setm(GCA,'MLabelLocation', 1, 'MLabelRound', MLabelRound, 'PLabelRound', PLabelRound)

coast = load('coast.mat');
plotm(coast.lat, coast.long, 'k');
setm(GCA, 'plabellocation',0.5)
tightmap

%% Trac� GSHHS

nomFicGSHHS = getNomFicDatabase('gshhs_f.b');
if exist(nomFicGSHHS, 'file')
    S = gshhs(nomFicGSHHS, latLim, lonLim);
    axesm('mercator',...
        'MapLatLimit', latLim, ...
        'MapLonLimit', lonLim,...
        'Frame','on', ...
        'fLatLimit', latLim, ...
        'fLonLimit', lonLim)
    geoshow([S.Lat], [S.Lon]);
    
    xlabel('Longitude (�)')
    ylabel('Latitude (�)')
    
    % Colorisation des attributs
    levels  = [S.Level];
    land    = (levels == 1);
    lake    = (levels == 2);
    % % island  = (levels == 3);
    geoshow(S(land), 'FaceColor', [0 1 0]) % Topographie en Vert
    geoshow(S(lake), 'FaceColor', [0 0 1]) % Topographie en Blue
    
    % Customisation de l'affichage
    ylim                = getm(GCA, 'maplatlimit');
    xlim                = getm(GCA, 'maplonlimit');
    nbStepTicks         = 10;
    
    stepPLabelLocation 	= abs(ylim(2) - ylim(1))/nbStepTicks;
    stepPLabelLocation  = fix(stepPLabelLocation * 1000)/1000; % Arrondi au 1/100
    stepMLabelLocation  = abs(xlim(2) - xlim(1))/nbStepTicks;
    stepMLabelLocation  = fix(stepMLabelLocation * 1000)/1000; % Arrondi au 1/100
    
    setm(GCA, 'plabellocation', stepPLabelLocation)
    setm(GCA, 'mlabellocation', stepMLabelLocation)
    
    setm(GCA, 'parallellabel', 'on')
    setm(GCA, 'meridianlabel', 'on')
    setm(GCA, 'labelrotation', 'on')
    setm(GCA, 'plabelmeridian', lonLim(1))
    setm(GCA, 'mlabelparallel', latLim(2))
    setm(GCA, 'plinelocation', stepPLabelLocation)
    setm(GCA, 'mlinelocation', stepMLabelLocation)
    setm(GCA, 'FFaceColor', 'cyan');
    setm(GCA, 'grid',  'on')
    setm(GCA, 'frame', 'off')
    
    tightmap
end

%% Trac� de la navigation

hLine = plotm(Lat, Lon);

