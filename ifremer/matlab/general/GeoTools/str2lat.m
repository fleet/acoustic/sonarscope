% Conversion d'une cha�ne de caract�res en latitude
%
% Syntax
%   lat = str2lat(s)
%
% Input Arguments
%	s : chaine de caract�res
%
% Output Arguments
%   lat : Latitude
%
% Examples
%   str2lat('N,43,30')
%   str2lat(['N,43,30'; 'S 43 45'])
%   str2lat('N 43.5')
%   str2lat('N43.5')
%   str2lat('43.5')
%   str2lat('-43 30')
%
% See also lat2str str2lon lon2str Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function d = str2lat(s)

s = cellstr(s);
d = zeros(size(s));
for k=1:numel(s)
    str = strtrim(s{k});
    str = replace(str, ',', ' ');
    
    switch lower(str(1))
        case {'n'; 's'}
            str = [str(1) ' ' str(2:end)];
    end
    str = replace(str, '  ', ' ');
    
    mots = strsplit(str);
    nbMots = length(mots);
    
    if nbMots == 1
        val = str2num(str); %#ok<ST2NM>
        if isempty(val)
            d(k) = NaN;
        else
            d(k) = val;
        end
    else
        switch mots{1}(1)
            case {'N';'n'}
                mots{1} = 'N';
                if nbMots == 2
                    mots{3} = '00';
                end
            case {'S';'s'}
                mots{1} = 'S';
                if nbMots == 2
                    mots{3} = '00';
                end
            otherwise
                x = str2num(mots{1}); %#ok
                if nbMots == 1
                    mots{2} = '00';
                end
                mots{3} = mots{2};
                mots{2} = mots{1};
                if x >= 0
                    mots{1} = 'N';
                else
                    mots{1} = 'S';
                end
        end
        
        d(k) = num2latlon(mots{1}, abs(str2num(mots{2})), abs(str2num(mots{3}))); %#ok
    end
end
