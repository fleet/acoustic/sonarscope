% Conversion d'une cha�ne de caract�res en longitude
%
% Syntax
%   lon = str2lon(s)
%
% Input Arguments
%	s : cha�ne de caract�res
%
% Output Arguments
%   lon : Longitude
%
% Examples
%   str2lon('E,007,30')
%   str2lon(['E,007,30'; 'W 043 45'])
%   str2lon('W 43.5')
%   str2lon('W43.5')
%   str2lon('43.5')
%   str2lon('-43 30')
%
% See also lon2str lat2str str2lat Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function d = str2lon(s)

s = cellstr(s);
d = zeros(size(s));
for k=1:numel(s)
    str = replace(s{k}, ',', ' ');
    
    switch lower(str(1))
        case {'w'; 'e'}
            str = [str(1) ' ' str(2:end)];
    end
    str = replace(str, '  ', ' ');
    
    mots = strsplit(str);
    nbMots = length(mots);
    
    if nbMots == 1
        val = str2num(str); %#ok<ST2NM>
        if isempty(val)
            d(k) = NaN;
        else
            d(k) = val;
        end
    else
        switch mots{1}(1)
            case {'E';'e'}
                mots{1} = 'e';
                if nbMots == 2
                    mots{3} = '00';
                end
            case {'W';'w'}
                mots{1} = 'W';
                if nbMots == 2
                    mots{3} = '00';
                end
            otherwise
                x = str2num(mots{1}); %#ok<ST2NM>
                if nbMots == 1
                    mots{2} = '00';
                end
                mots{3} = mots{2};
                mots{2} = mots{1};
                if x >= 0
                    mots{1} = 'E';
                else
                    mots{1} = 'W';
                end
        end
        d(k) = num2latlon(mots{1}, abs(str2num(mots{2})), abs(str2num(mots{3}))); %#ok<ST2NM>
    end
end
