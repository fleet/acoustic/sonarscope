% Conversion d'une chaine de caracteres en longitude
%
% Syntax
%   lon = str2lonlat(s)
%
% Input Arguments
%	s : chaine de caracteres
%
% Output Arguments
%   lon : Longitude
%
% Examples
%   str2lonlat('E,007,25.19500')
%   str2lonlat(['E,007,25.19500'; 'E 043 28.03600'])
%   str2lonlat('W 43.5')
%   str2lonlat('43.5')
%   str2lonlat('-43 30')
%
% See also lon2str lat2str str2lat Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function d = str2lonlat(s)

% A REFAIRE DE MANIERE BEAUCOUP PLUS INTELLIGENTE : regexp ???
% De m�me sur toutes les fonctions de ce style

s = cellstr(s);
d = zeros(size(s));
for i=1:numel(s)
    str = s{i};
    str = strrep(str, ',', ' ');
    mots = strsplit(str);
    nbMots = length(mots);

    V = str2num(mots{1}); %#ok<ST2NM>
    if isempty(V) || isnan(V)
        if length(mots{1}) > 1
            mots = {mots{1}(1) mots{1}(2:end) mots{2:end}};
            nbMots = nbMots+1;
        end
    else
        if str2num(mots{1}) >= 0 %#ok<ST2NM>
            mots = [{'N'} mots]; %#ok<AGROW>
        else
            mots = [{'S'} mots]; %#ok<AGROW>
        end
        nbMots = nbMots+1;
    end

    switch mots{1}(1)
        case {'E';'e';'N';'n'}
            mots{1} = 'e';
            if nbMots == 2
                mots{3} = '00';
            end
        case {'W';'w';'S';'s'}
            mots{1} = 'W';
            if nbMots == 2
                mots{3} = '00';
            end
        otherwise
            x = str2num(mots{1}); %#ok<ST2NM>
            if nbMots == 1
                mots{2} = '00';
            end
            mots{3} = mots{2};
            mots{2} = mots{1};
            if x >= 0
                mots{1} = 'E';
            else
                mots{1} = 'W';
            end
    end

    if length(mots) == 3
        d(i) = num2latlon(mots{1}, abs(str2num(mots{2})), abs(str2num(mots{3}))); %#ok<ST2NM>
    else
        d(i) = num2latlon(mots{1}, abs(str2num(mots{2})), abs(str2num(mots{3})), abs(str2num(mots{4}))); %#ok<ST2NM>
    end
end
