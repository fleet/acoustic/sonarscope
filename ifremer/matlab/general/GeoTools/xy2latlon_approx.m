% Conversion de coordonnees metriques en coordonnees geographiques (formule ultra-simpliste)
%
% Syntax
%   [lat, lon] = xy2latlon_approx(x, y)
%   [lat, lon] = xy2latlon_approx(x, y, 'latRef', latRef)
%
% Input Arguments
%   x : Abscisses (m) (origine = greenwitch)
%   y : Ordonnees (m) (origine = equateur)
%
% Name-Value Pair Arguments
%   'latRef' : Latitude d'echelle conservee. (Moyenne des latitudes par defaut)
%
% Output Arguments
%   lat : Latitude (deg).
%   lon : Longitude (deg).
%
% Examples
%   lat = 0:5:50; lon = [0:5:50]; latRef = mean(lat);
%   [x, y] = latlon2xy_approx(lat, lon, 'latRef', latRef)
%   [lat, lon] = xy2latlon_approx(x, y, 'latRef', latRef)
%
% See also latlon2xy_approx Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [lat, lon] = xy2latlon_approx(x, y, varargin)

RayonTerre = 6371000;
D2R = pi / 180;

lat = (y / RayonTerre) / D2R;

[varargin, latRef] = getPropertyValue(varargin, 'latRef', mean(lat, 'omitnan')); %#ok<ASGLU>

lon = x / (RayonTerre * cos(latRef .* D2R) * D2R);
