function ModifySizePhysTotalMemory

global SizePhysTotalMemory %#ok<GVMIS> 

if isempty(SizePhysTotalMemory)
    loadVariablesEnv
end

str1 = 'Taille maximale d''une image en m�moire. Valeur conseill�e = 1/4 de la RAM ';
str2 = 'Maximum size of an image in RAM. Suggestion=1/4 RAM';
p = ClParametre('Name', Lang('Taille', 'Size'), ...
    'Unit', Lang('Go', 'Gb'),  'MinValue', 0.25, 'MaxValue', 32, 'Value', SizePhysTotalMemory*1e-6);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Val = a.getParamsValue;
SizePhysTotalMemory = Val * 1e6;

saveVariablesEnv
