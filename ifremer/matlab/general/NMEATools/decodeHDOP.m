function HorizontalDilutionOfPrecision = decodeHDOP(sentenceFormatter, GPSQualityIndicator, HorizontalDilutionOfPrecision)

switch sentenceFormatter
    case 'GGA'
        switch GPSQualityIndicator
            case 1 % (SPS or standard GPS) => 1000
                GPSQualityIndicator = 1000;
            case 2 % (differential GPS) => 100
                GPSQualityIndicator = 100;
            case 3 %(PPS or precise GPS) => 200, but 10 if GGA is treated as RTK. (See Note 2)
                GPSQualityIndicator = 200;
            case 4 %(kinematic GPS with fixed integers) => 10
                GPSQualityIndicator = 10;
            case 5 %(kinematic GPS with floating integers) => 50
                GPSQualityIndicator = 50;
            case 6 %(estimated or dead reckoning mode) => 1000
                GPSQualityIndicator = 1000;
            case 7 %(manual input mode) => 1000
                GPSQualityIndicator = 1000;
            case 8 %(test mode) => 1000, but 10 if GGA is treated as RTK. (See Note 2) 
                GPSQualityIndicator = 1000;
            case 0
            % The �Measure of position fix quality� field will be set to 65534 (largest valid
            % number) if the indicator is zero (non-valid position).
            GPSQualityIndicator = 65534;
        end
        % TODO : reste � traite la note 2 pg 13 
        
    case 'GGK'
        switch GPSQualityIndicator
            case 1 % (SPS or standard GPS) => 1000
                GPSQualityIndicator = 1000;
            case 2 % (differential GPS) => 100
                GPSQualityIndicator = 100;
            case 3 %(kinematic GPS with fixed integers) => 10
                GPSQualityIndicator = 10;
            case 0
                % The �Measure of position fix quality� field will be set to 65534 (largest valid
                % number) if the indicator is zero (non-valid position).
                GPSQualityIndicator = 65534;
        end
        % TODO : reste � traite la note 2 pg 15 

    otherwise
        str1 = 'Ce type de s�quence NMEA n''est pas encore trait�. SVP, contactez JMA.';
        str2 = 'This NMEA sequence is not processed yet. Please contact JMA.';
        my_warndlg(Lang(str1,str2), 1);
end        

HorizontalDilutionOfPrecision = GPSQualityIndicator .* HorizontalDilutionOfPrecision;