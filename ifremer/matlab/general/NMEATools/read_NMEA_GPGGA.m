% Décodage d'une trame NMEA de position GPS
%
% Syntax
%   [Heure, Lat, Lon, Height] = read_NMEA_GPGGA(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   Heure  : Nombre de millisecondes depuis minuit
%   Lat    : Latitude (deg)
%   Lon    : Longitude (deg)
%   Height : Hauteur (m).
%
% Examples
%   str = '$GPGGA,035344.00,0114.74433,N,10352.78390,E,5,10,0.9,13.21,M,0.00,M,12,0001*65'
%   [Heure, Lat, Lon, Height] = read_NMEA_GPGGA(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ https://fr.wikipedia.org/wiki/NMEA_0183
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGA(str)

if strcmp(str(1), '$')
    str = str(2:end);
end

% Test bug rencontré sur fichier 0001_20120918_092148_SimonStevin.all 
% GPGGA,093249.00 5117.62998,N,00237.93614,E,4,14,0.8,9.18,M,47.12,M,4.0,0081*43
% une virgule manque entre l'heure et la latitude
if strcmp(str(16), ' ')
    str(16) = ',';
end
if ~strcmp(str(6), ',') % GPGGA(093513.00,5117.83683,N,00238.11336,E,4,14,0.8,9.26,M,47.12,M,1.0,0081*49 
    str(6) = ',';
end

mots = regexp(str, ',', 'split');

HH = str2double(mots{2}(1:2));
MM = str2double(mots{2}(3:4));
SS = str2double(mots{2}(5:end));
Heure = 1000 * (SS + MM * 60 + HH * 3600);

% t = cl_time('timeIfr', 0, Heure);
% t = t.timeMat;

DD = str2double(mots{3}(1:2));
MM = str2double(mots{3}(3:end));
Lat = DD + MM/60;
if strcmp(mots{4}, 'S')
    Lat = -Lat;
end

DD = str2double(mots{5}(1:3));
MM = str2double(mots{5}(4:end));
Lon = DD + MM/60;
if strcmp(mots{6}, 'W')
    Lon = -Lon;
end

% Test bug rencontré sur fichier 0004_20120918_100632_SimonStevin.all 
% GPGGA,101455.00,5117.80362,N,0  $GPZDA,101456.00,18,09,2012,,*60'
% On teste si Lon est NaN
if isnan(Lon)
    flag = 0;
    Heure = NaN;
    Lat   = NaN;
    PosData = [];
    PosData.QualityIndicator              = NaN;
    PosData.NumberSatellitesUsed          = NaN;
    PosData.HorizontalDilutionOfPrecision = NaN;
    PosData.AltitudeOfIMU                 = NaN;
    PosData.GeoidalSeparation             = NaN;
    PosData.AgeOfDifferentialCorrections  = NaN;
    PosData.DGPSRefStationIdentity        = newline;
    PosData.HorizontalDilutionOfPrecision = NaN;
    return
end

PosData.QualityIndicator              = str2double(mots{7});
PosData.NumberSatellitesUsed          = str2double(mots{8});
PosData.HorizontalDilutionOfPrecision = str2double(mots{9});
PosData.AltitudeOfIMU                 = str2double(mots{10});
PosData.GeoidalSeparation             = str2double(mots{12});
if length(mots) > 13
    PosData.AgeOfDifferentialCorrections =  str2double(mots{14});
    % Si le mot ne contient pas le * indiquant le CheckSum.
    if ~strfind(mots{end}, '*')
        PosData.DGPSRefStationIdentity = sprintf('%s\n', mots{15});
    else
        PosData.DGPSRefStationIdentity = newline;
    end
else
    PosData.AgeOfDifferentialCorrections = NaN;
    PosData.DGPSRefStationIdentity       = newline;
end
    
% Facteur d'échelle de HDOP.
PosData.HorizontalDilutionOfPrecision = decodeHDOP('GGA', PosData.QualityIndicator, PosData.HorizontalDilutionOfPrecision); 

flag = 1;
