% D�codage d'une trame NMEA de position GPS
%
% Syntax
%   [Heure, Lat, Lon, Height] = read_NMEA_GPGGK(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   Heure  : Nombre de millisecondes depuis minuit
%   Lat    : Latitude (deg)
%   Lon    : Longitude (deg)
%   Height : Hauteur (m).
%
% Examples
%   str = 'GPGGK,103721.437,032316,4817.37964198,N,00428.04255956,W,3,16,2.1,EHT49.100,M*5C'
%   [Heure, Lat, Lon, Height] = read_NMEA_GPGGK(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGK(str)

mots = strsplit(str, ',');

HH = str2double(mots{2}(1:2));
MM = str2double(mots{2}(3:4));
SS = str2double(mots{2}(5:end));
Heure = 1000 * (SS + MM * 60 + HH * 3600);

% Month = str2double(mots{3}(1:2));
% Jour  = str2double(mots{3}(3:4));
% Annee = str2double(mots{3}(5:end)) + 2000;

% t = cl_time('timeIfr', 0, Heure);
% t = t.timeMat;

DD = str2double(mots{4}(1:2));
MM = str2double(mots{4}(3:end));
Lat = DD + MM/60;
if strcmp(mots{5}, 'S')
    Lat = -Lat;
end

DD = str2double(mots{6}(1:3));
MM = str2double(mots{6}(4:end));
Lon = DD + MM/60;
if strcmp(mots{7}, 'W')
    Lon = -Lon;
    
end

% Test bug rencontr� sur fichier 0004_20120918_100632_SimonStevin.all 
% GPGGA,101455.00,5117.80362,N,0  $GPZDA,101456.00,18,09,2012,,*60'
% On teste si Lon est NaN
if isnan(Lon)
    flag = 0;
    Heure = NaN;
    Lat   = NaN;
    PosData = [];
    PosData.QualityIndicator              = NaN;
    PosData.NumberSatellitesUsed          = NaN;
    PosData.HorizontalDilutionOfPrecision = NaN;
    PosData.AltitudeOfIMU                 = NaN;
    PosData.GeoidalSeparation             = NaN;
    PosData.AgeOfDifferentialCorrections  = NaN;
    PosData.DGPSRefStationIdentity        = newline;
    PosData.HorizontalDilutionOfPrecision = NaN;
    return
end

PosData.QualityIndicator                 =  str2double(mots{8});
PosData.NumberSatellitesUsed             =  str2double(mots{9});
PosData.HorizontalDilutionOfPrecision    =  str2double(mots{10});
% PosData.AltitudeOfIMU                    =  str2double(mots{11});
% PosData.GeoidalSeparation                =  str2double(mots{12});
PosData.AltitudeOfIMU                    =  NaN;
PosData.GeoidalSeparation                =  NaN;
    PosData.AgeOfDifferentialCorrections =  NaN;
    PosData.DGPSRefStationIdentity       =  newline;


% if length(mots) > 13
%     PosData.AgeOfDifferentialCorrections =  str2double(mots{14});
%     % Si le mot ne contient pas le * indiquant le CheckSum.
%     if ~strfind(mots{end}, '*')
%         PosData.DGPSRefStationIdentity   =  sprintf('%s\n', mots{15});
%     else
%         PosData.DGPSRefStationIdentity   =  newline;
%     end
% else
%     PosData.AgeOfDifferentialCorrections =  NaN;
%     PosData.DGPSRefStationIdentity       =  newline;
% end
    
% Facteur d'�chelle de HDOP.
PosData.HorizontalDilutionOfPrecision = decodeHDOP('GGA', PosData.QualityIndicator, PosData.HorizontalDilutionOfPrecision); 

flag = 1;




