% D�codage d'une trame NMEA de type $GPGLL
%
% Syntax
%   [flag, Time] = read_NMEA_GPGLL(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag : 0/1
%   Heure : Nb od milliseconds since midnight
%   Lat   : Latitude (deg)
%   Lon   : Longitude (deg)
%   GPS   : Structure discribing the GPS acquisition
%
% Examples
%   str = '$GPGLL,3740.120,S,17622.035,E'
%   [flag, Heure, Lat, Lon, GPS]  = read_NMEA_GPGLL(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ & http://pages.uoregon.edu/drt/MGL0910_sup/html_ref/formats/posmv.html
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Heure, Lat, Lon] = read_NMEA_GPGLL(str)
    
mots = strsplit(str, ',');
flag = (length(mots) >= 5);
if ~flag
    Lat   = [];
    Lon   = [];
    Heure = [];
    return
end

try
    DD = str2double(mots{2}(1:2));
    MM = str2double(mots{2}(3:end));
    Lat = DD + MM/60;
    if strcmp(mots{3}, 'S')
        Lat = -Lat;
    end
    
    DD = str2double(mots{4}(1:3));
    MM = str2double(mots{4}(4:end));
    Lon = DD + MM/60;
    if strcmp(mots{5}(1), 'W')
        Lon = -Lon;
    end
    
    HH = str2double(mots{6}(1:2));
    MM = str2double(mots{6}(3:4));
    SS = str2double(mots{6}(5:end));
    Heure = 1000 * (SS + MM * 60 + HH * 3600);    
    
catch ME %#ok<NASGU>
    flag  = 0;
    Lat   = [];
    Lon   = [];
    Heure = [];
    return
end
