% D�codage d'une trame NMEA de position GPS
%
% Syntax
%   [Heure, Lat, Lon, Height] = read_NMEA_GPRMC(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   Heure  : Nombre de millisecondes depuis minuit
%   Lat    : Latitude (deg)
%   Lon    : Longitude (deg)
%   Height : Hauteur (m).
%
% Examples
%   str = '$GPRMC,133059,A,3750.0263,S,17701.5251,E,007.3,206.5,270310,020.0,E*63  '
%   [Heure, Lat, Lon, Height] = read_NMEA_GPRMC(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Heure, Lat, Lon, Speed, Heading] = read_NMEA_GPRMC(str)

mots = regexp(str, ',', 'split');

HH = str2double(mots{2}(1:2));
MM = str2double(mots{2}(3:4));
SS = str2double(mots{2}(5:end));
Heure = 1000 * (SS + MM * 60 + HH * 3600);

% t = cl_time('timeIfr', 0, Heure);
% t = t.timeMat;

DD = str2double(mots{4}(1:2));
MM = str2double(mots{4}(3:end));
Lat = DD + MM/60;
if strcmp(mots{5}, 'S')
    Lat = -Lat;
end

DD = str2double(mots{6}(1:3));
MM = str2double(mots{6}(4:end));
Lon = DD + MM/60;
if strcmp(mots{7}, 'W')
    Lon = -Lon;
end

Speed   = str2double(mots{8});
Heading = str2double(mots{9});

flag = 1;




