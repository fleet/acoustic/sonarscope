% D�codage d'une trame NMEA de type $INZDA
%
% Syntax
%   [flag, Time] = read_NMEA_INGGA(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag : 0/1
%   Heure : Nb od milliseconds since midnight
%   Lat   : Latitude (deg)
%   Lon   : Longitude (deg)
%   GPS   : Structure discribing the GPS acquisition
%
% Examples
%   str = '$INGGA,154039.51,5926.801098,N,01023.057438,E,2,12,0.6,0.40,M,40.65,M,1.2,0444'
%   [flag, Heure, Lat, Lon, GPS]  = read_NMEA_INGGA(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ & http://pages.uoregon.edu/drt/MGL0910_sup/html_ref/formats/posmv.html
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Heure, Lat, Lon, PosData] = read_NMEA_INGGA(str)
    
mots = strsplit(str, ',');
flag = (length(mots) >= 12);
if ~flag
    Heure   = [];
    Lat     = [];
    Lon     = [];
    PosData = [];
    return
end

try
    HH = str2double(mots{2}(1:2));
    MM = str2double(mots{2}(3:4));
    SS = str2double(mots{2}(5:end));
    Heure = 1000 * (SS + MM * 60 + HH * 3600);
    
    DD = str2double(mots{3}(1:2));
    MM = str2double(mots{3}(3:end));
    Lat = DD + MM/60;
    if strcmp(mots{4}, 'S')
        Lat = -Lat;
    end
    
    DD = str2double(mots{5}(1:3));
    MM = str2double(mots{5}(4:end));
    Lon = DD + MM/60;
    if strcmp(mots{6}, 'W')
        Lon = -Lon;
    end
    
    PosData.QualityIndicator                 =  str2double(mots{7});
    PosData.NumberSatellitesUsed             =  str2double(mots{8});
    PosData.HorizontalDilutionOfPrecision    =  str2double(mots{9});
    PosData.AltitudeOfIMU                    =  str2double(mots{10});
    PosData.GeoidalSeparation                =  str2double(mots{12});
    if length(mots) > 13
        PosData.AgeOfDifferentialCorrections =  str2double(mots{14});
        % Si le mot ne contient pas le * indiquant le CheckSum.
        if ~strfind(mots{end}, '*')
            PosData.DGPSRefStationIdentity =  sprintf('%s\n', mots{15});
        else
            PosData.DGPSRefStationIdentity =  newline;
        end
    else
        PosData.AgeOfDifferentialCorrections =  NaN;
        PosData.DGPSRefStationIdentity       =  newline;
    end
    
    % Facteur d'�chelle de HDOP.
    PosData.HorizontalDilutionOfPrecision = decodeHDOP('GGA', PosData.QualityIndicator, PosData.HorizontalDilutionOfPrecision); 
    
catch ME %#ok<NASGU>
    flag = 0;
    Heure = [];
    Lat   = [];
    Lon   = [];
    PosData   = [];
    return
end
