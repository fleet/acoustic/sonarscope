% D�codage d'une trame NMEA de position GPS
%
% Syntax
%   [t, Lat, Lon, Height] = read_NMEA_INGGK(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   t      : Instance de cl_time
%   Lat    : Latitude (deg)
%   Lon    : Longitude (deg)
%   Height : Hauteur (m).
%
% Examples
%   str = '$INGGK,035344.00,0114.74433,N,10352.78390,E,5,10,0.9,13.21,M,0.00,M,12,0001*65'
%   [Heure, Lat, Lon, Height] = read_NMEA_INGGK(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%--------------------------------------------------------------------------

function [flag, t, Lat, Lon, PosData] = read_NMEA_INGGK(str)

flag = 0;
Heure               = [];
Lat                 = [];
Lon                 = [];
% FixQuality          = [];
% NumberOfSatellites  = [];
% HDOP                = [];
Height              = [];
% HeureLastDGPS       = [];
% DGPSStationId       = [];

mots = strsplit(str, ',');

HH = str2double(mots{2}(1:2));
MM = str2double(mots{2}(3:4));
SS = str2double(mots{2}(5:end));
heure = 1000 * (SS + MM * 60 + HH * 3600);

Annee = str2double(mots{3}(5:6));
if Annee > 90
    Annee = Annee + 1900;
else
    Annee = Annee + 2000;
end

Jour = str2double(mots{3}(3:4));
Mois = str2double(mots{3}(1:2));

date = dayJma2Ifr(Jour, Mois, Annee);
t = cl_time('timeIfr', date, heure);
t = t.timeMat;

DD = str2double(mots{4}(1:2));
MM = str2double(mots{4}(3:end));
Lat = DD + MM/60;
if strcmp(mots{5}, 'S')
    Lat = -Lat;
end

DD = str2double(mots{6}(1:3));
MM = str2double(mots{6}(4:end));
Lon = DD + MM/60;
if strcmp(mots{7}, 'W')
    Lon = -Lon;
end

PosData.QualityIndicator              =  str2double(mots{8});
PosData.NumberSatellitesUsed          =  str2double(mots{9});
PosData.HorizontalDilutionOfPrecision =  str2double(mots{10});
PosData.AltitudeOfIMU                 =  str2double(regexprep(mots{11}, 'EHT', ''));
PosData.GeoidalSeparation             =  NaN;
PosData.AgeOfDifferentialCorrections  =  NaN;
PosData.DGPSRefStationIdentity        =  newline;

% Facteur d'�chelle de HDOP.
PosData.HorizontalDilutionOfPrecision = decodeHDOP('GGK', PosData.QualityIndicator, PosData.HorizontalDilutionOfPrecision); 

flag = 1;

