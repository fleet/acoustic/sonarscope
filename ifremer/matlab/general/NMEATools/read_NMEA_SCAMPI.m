function [flag, Date, Heure, Lat, Lon, Heading, Speed, Height, CableOut, CableTension] = read_NMEA_SCAMPI(str)

mots = strsplit(str, ',');
if ~strcmp(mots{1}, '$SCAMPI')
    mots = strsplit(str);
end

if length(mots) < 10
    Date   = [];
    Heure  = [];
    Lat    = [];
    Lon    = [];
    Heading      = [];
    Speed        = [];
    Height       = [];
    CableOut     = [];
    CableTension = [];
    flag = 0;
    return
end

try
    switch length(mots)
        case 10
            Jour = str2double(mots{2}(1:2));
            Mois = str2double(mots{2}(4:5));
            Annee = str2double(mots{2}(7:end));
            if Annee < 2000
                Annee = Annee + 2000;
            end
            Date = dayJma2Ifr(Jour, Mois, Annee);
            
            HH = str2double(mots{3}(1:2));
            MM = str2double(mots{3}(4:5));
            SS = str2double(mots{3}(7:end));
            Heure = 1000 * (SS + MM * 60 + HH * 3600);
            
            for k=4:9
                mots{k} = strrep(mots{k}, ',', '.');
            end
            Lat = str2double(mots{4});  
            Lon = str2double(mots{5});
            
            Heading      = str2double(mots{6});
            Speed        = str2double(mots{7});
            Height       = str2double(mots{8});
            CableOut     = str2double(mots{9});
            CableTension = str2double(mots{10});
            
        case 14
            Jour = str2double(mots{2}(1:2));
            Mois = str2double(mots{2}(4:5));
            Annee = str2double(mots{2}(7:end));
            if Annee < 2000
                Annee = Annee + 2000;
            end
            Date = dayJma2Ifr(Jour, Mois, Annee);
            
            HH = str2double(mots{3}(1:2));
            MM = str2double(mots{3}(4:5));
            SS = str2double(mots{3}(7:end));
            Heure = 1000 * (SS + MM * 60 + HH * 3600);
            
            DD = str2double(mots{5});
            MM = str2double(mots{6});
            Lat = DD + MM/60;
            if strcmp(mots{4}, 'S')
                Lat = -Lat;
            end
            
            DD = str2double(mots{8});
            MM = str2double(mots{9});
            Lon = DD + MM/60;
            if strcmp(mots{7}, 'W')
                Lon = -Lon;
            end
            
            Heading      = str2double(mots{10});
            Speed        = str2double(mots{11});
            Height       = str2double(mots{12});
            CableOut     = str2double(mots{13});
            CableTension = str2double(mots{14});
            
        otherwise
            Date   = [];
            Heure  = [];
            Lat    = [];
            Lon    = [];
            Heading      = [];
            Speed        = [];
            Height       = [];
            CableOut     = [];
            CableTension = [];
            flag   = 0;
            return
    end

catch ME %#ok<NASGU>
    Date   = [];
    Heure  = [];
    Lat    = [];
    Lon    = [];
    Heading      = [];
    Speed        = [];
    Height       = [];
    CableOut     = [];
    CableTension = [];
    flag   = 0;
    return
end

flag   = 1;

