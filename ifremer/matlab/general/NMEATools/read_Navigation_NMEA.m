% D�codage d'une trame NMEA de position GPS
%
% Syntax
%   [flag, Heure, Lat, Lon, Height] = read_Navigation_NMEA(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag   : 1=OK, 0=KO
%   Heure  : Nombre de millisecondes depuis minuit
%   Lat    : Latitude (deg)
%   Lon    : Longitude (deg)
%   Height : Hauteur (m).
%
% Examples
%   str = '$GPGGA,035344.00,0114.74433,N,10352.78390,E,5,10,0.9,13.21,M,0.00,M,12,0001*65'
%   [flag, Heure, Lat, Lon, Height] = read_Navigation_NMEA(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Heure, Lat, Lon, PosData, Height] = read_Navigation_NMEA(str)

sz     = size(str);
Height = [];
% PosData     = [];

if ischar(str)
    if sz(1) == 1
        [flag, Heure, Lat, Lon, PosData, Height] = decodeNMEA(str);
        if ~flag
%             PosData = [];
            return
        end
    elseif sz(2) == 1
        [flag, Heure, Lat, Lon, PosData, Height] = decodeNMEA(str');
        if ~flag
%             PosData = [];
            return
        end
    else
        N = size(str,1);
        Heure = NaN(N,1);
        Lat   = NaN(N,1);
        Lon   = NaN(N,1);
        for k=1:N
            [flag, HH, lat, lon, GPSData, h] = decodeNMEA(str(k,:));
            if ~flag
%                 PosData = [];
                return
            end
            Heure(k,1) = HH;
            Lat(k,1) = lat;
            Lon(k,1) = lon;
            if k == 1
                PosData(N,1) = GPSData; %#ok<AGROW>
                if ~isempty(h)
                    Height(N,1) = h; %#ok<AGROW>
                end
            end
            PosData(k,1) = GPSData; %#ok<AGROW>
            if ~isempty(h)
                Height(k,1) = h; %#ok<AGROW>
            end
        end
    end
else
    N = size(str,1);
    Heure = NaN(N,1);
    Lat   = NaN(N,1);
    Lon   = NaN(N,1);
    for k=1:N
        [flag, HH, lat, lon, GPSData, h] = decodeNMEA(str{k,:});
        if ~flag
        % Comment� par JMA le 10/12/2014 pour fichier 0004_20120918_100632_SimonStevin.all
%             PosData = [];
%             return
        end

        Heure(k,1) = HH;
        Lat(k,1) = lat;
        Lon(k,1) = lon;
        if k == 1
            PosData(N,1) = GPSData; %#ok<AGROW>
            if ~isempty(h)
                Height(N,1) = h; %#ok<AGROW>
            end
        end
        PosData(k,1) = GPSData; %#ok<AGROW>
        if ~isempty(h)
            Height(k,1) = h; %#ok<AGROW>
        end
    end
end


function [flag, Heure, Lat, Lon, PosData, Height] = decodeNMEA(str)

if iscell(str)
    str = str{:};
    if isvector(str)
        str = str';
    end
end

mots = strsplit(str, ',');
PosData = [];
Height  = [];
if regexp(mots{1}, 'INGGK') % Permet de s'affranchir du "$"
    [flag, Heure, Lat, Lon, PosData] = read_NMEA_INGGK(str); % Ex : '$INGGK,035344.00,0114.74433,N,10352.78390,E,5,10,0.9,13.21,M,0.00,M,12,0001*65'
elseif regexp(mots{1}, 'GPGGK') 
    [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGK(str); % Ex : '$GPGGK,103721.437,032316,4817.37964198,N,00428.04255956,W,3,16,2.1,EHT49.100,M*5C'
elseif regexp(mots{1}, 'GPGGA') 
    [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGA(str); % Ex : '$GPGGA,035344.00,0114.74433,N,10352.78390,E,5,10,0.9,13.21,M,0.00,M,12,0001*65'
elseif regexp(mots{1}, 'PHGGA')
    [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGA(str); % Ex : '$INGGA,154039.51,5926.801098,N,01023.057438,E,2,12,0.6,0.40,M,40.65,M,1.2,0444'
elseif regexp(mots{1}, 'PTNL') % Ajout JMA le 05/11/2018 pour donn�es SHOM 0033_20180214_032244_BBP.all o� str='PTNL,GGK,032244.95,021418,4820.5433855,N,00428.7912998,W,4,14,0.0,EHT54.077,M*53 '
    [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGK(str(6:end)); % Ex : 
elseif regexp(mots{1}, '[INGGA|isGGA]') % Tr�s bizarre. Voir mails Roger du 05/11/2018
    [flag, Heure, Lat, Lon, PosData] = read_NMEA_INGGA(str); % Ex : '$INGGA,154039.51,5926.801098,N,01023.057438,E,2,12,0.6,0.40,M,40.65,M,1.2,0444'
elseif (length(mots{1}) >= 10) && strcmp(mots{1}(1:10) , 'NAVIGATION')
    [flag, Heure, Lat, Lon, Height] = read_NMEA_UNH(str); % !!!
else
    Heure               = [];
    Lat                 = [];
    Lon                 = [];
    % FixQuality          = [];
    % NumberOfSatellites  = [];
    % HDOP                = [];
    Height              = [];
    PosData             = [];
    % HeureLastDGPS       = [];
    % DGPSStationId       = [];
    flag = 0;
end
