function [flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirImport, varargin)

[varargin, strQuestion] = getPropertyValue(varargin, 'strQuestion', []);
[varargin, AskQuestion] = getPropertyValue(varargin, 'AskQuestion', 1); %#ok<ASGLU>

CorFile       = [];
ModeDependant = 0;
UseModel      = [];
PreserveMeanValueIfModel = [];

if AskQuestion
    if isempty(strQuestion)
        str1 = 'Voulez-vous faire une compensation ?';
        str2 = 'Do you want to do a compensation ?';
        strQuestion = Lang(str1,str2);
    end
    [rep, flag] = my_questdlg(strQuestion, 'Init', 2);
    if ~flag
        return
    end
else
    rep = 1;
end

if rep == 1
    [flag, CorFile] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', nomDirImport);
    if ~flag
        return
    end
    for k2=1:length(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile{k2});
        if isempty(CourbeConditionnelle)
            flag = 0;
            return
        end
        bilan = CourbeConditionnelle.bilan;
        if isfield(bilan{1}(1), 'Mode') && ~isempty(bilan{1}(1).Mode) && (bilan{1}(1).Mode ~= 0)
            ModeDependant = 1;
            UseModel(k2)  = 0; %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = 0; %#ok<AGROW>
%             break
        end
        if isfield(bilan{1}(1), 'model') && ~isempty(bilan{1}(1).model)
            str1 = sprintf('La courbe "%s" poss�de un model, que voulez-vous utiliser pour r�aliser la compensation ?', bilan{1}(1).nomZoneEtude);
            str2 = sprintf('The curve "%s" has a model, what to you want to use to realize the compensation ?', bilan{1}(1).nomZoneEtude);
            [rep, flag] = my_questdlg(Lang(str1,str2), Lang('Le mod�le', 'The model'), Lang('Les donn�es', 'The data'));
            if ~flag
                return
            end
            UseModel(k2) = (rep == 1); %#ok<AGROW>
            
            if UseModel(k2)
                str1 = sprintf('Quel type de compensation voulez-vous r�aliser pour "%s" ? ', bilan{1}(1).nomZoneEtude);
                str2 = sprintf('What type of compensation do you want to apply for "%s" ?',   bilan{1}(1).nomZoneEtude);
                [rep, flag] = my_questdlg(Lang(str1,str2), Lang('Totale', 'Full'), ...
                    Lang('Pr�servation de la valeur moyenne','Preserve the mean value'));
                if ~flag
                    return
                end
                PreserveMeanValueIfModel(k2) = (rep == 2); %#ok<AGROW>
            end
            
        else
            UseModel(k2) = 0; %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = 0; %#ok<AGROW>
        end
    end
    if ModeDependant
        if isfield(bilan{1}(1), 'Mode') && ~isempty(bilan{1}(1).Mode) && (bilan{1}(1).Mode ~= 0)
%             SelectMode =  bilan{1}(1).Mode;
            str1 = 'Cette(s) courbe(s) statistique a/ont une signature de mode de fonctionnement du sondeur. Voulez-vous r�aliser ce traitement uniquement sur les pings correspondants ? ';
            str2 = 'Some of your statistic curves have a sounder mode signature. Do you want to process only the pings set to the corresponding modes ? ';
            [ModeDependant, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            ModeDependant = (ModeDependant == 1);
        end
    end
else
    CorFile = {};
end
