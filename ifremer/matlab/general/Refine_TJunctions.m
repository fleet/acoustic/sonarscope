% Compare T-junctions with obtained corners and add T-junctions to corners
% which are far away (outside a 5 by 5 neighborhood) from detected corners
%
% Publications:
% =============
% 1. M. Awrangjeb and G. Lu, “An Improved Curvature Scale-Space Corner Detector and a Robust Corner Matching Approach for Transformed Image Identification,” IEEE Transactions on Image Processing, 17(12), 2425–2441, 2008.
% 2. M. Awrangjeb, G. Lu, and M. M. Murshed, “An Affine Resilient Curvature Scale-Space Corner Detector,” 32nd IEEE International Conference on Acoustics, Speech, and Signal Processing (ICASSP 2007), Hawaii, USA, 1233–1236, 2007.
% 
% Better results will be found using following corner detectors:
% ==============================================================
% 1.  M. Awrangjeb, G. Lu and C. S. Fraser, “A Fast Corner Detector Based on the Chord-to-Point Distance Accumulation Technique,” Digital Image Computing: Techniques and Applications (DICTA 2009), 519-525, 2009, Melbourne, Australia.
% 2.  M. Awrangjeb and G. Lu, “Robust Image Corner Detection Based on the Chord-to-Point Distance Accumulation Technique,” IEEE Transactions on Multimedia, 10(6), 1059–1072, 2008.
% 
% Source codes available:
% =======================
% http://www.mathworks.com/matlabcentral/fileexchange/authors/39158

function [corner_final, c3] = Refine_TJunctions(corner_out, TJ, c2,curve, curve_num, curve_start, curve_end, curve_mode, EP)

[corner_final, c3] = RefineCorners_A(corner_out, TJ, c2, curve, curve_num, curve_start, curve_end, curve_mode, EP);

%{
% TODO ! Tentative de retournement des courbes pour que l'algo trouve la
% fin des coins au lieu du début. Si ça avait fonctionné on aurait pu faire
% l'union des deuxfor k=1:length(curve)
    curve{k} = flipud(curve{k});
    corner_out   = flipud(corner_out);
    TJ           = flipud(TJ);
    c2           = flipud(c2);
    
    pppp = curve_end(k,:);
    curve_start(k,:) = curve_end(k,:)
    curve_end(k,:)   = pppp;
end
[corner_final, c3] = RefineCorners_A(corner_out, TJ, c2,curve, curve_num, curve_start, curve_end, curve_mode, EP);
for k=1:length(c2)
    corner_final = flipud(corner_final);
    c3         = flipud(c3);
end
%}


function [corner_final, c3] = RefineCorners_A(corner_out, TJ, c2,curve, curve_num, curve_start, curve_end, curve_mode, EP)

%corner_final = corner_out;
c3 = c2;

%%%%% add end points
if EP
    corner_num = size(corner_out,1);
    for k=1:curve_num
        if (size(curve{k},1) > 0) && strcmp(curve_mode(k,:), 'line')
            
            % Start point compare with detected corners
            compare_corner = corner_out - ones(size(corner_out,1),1) * curve_start(k,:);
            compare_corner = compare_corner .^ 2;
            compare_corner = compare_corner(:,1) + compare_corner(:,2);
            if min(compare_corner) > 100       % Add end points far from detected corners
                corner_num = corner_num+1;
                corner_out(corner_num,:) = curve_start(k,:);
                c3 = [c3; 8]; %#ok<AGROW>
            end
            
            % End point compare with detected corners
            compare_corner = corner_out-ones(size(corner_out,1),1)*curve_end(k,:);
            compare_corner = compare_corner.^2;
            compare_corner = compare_corner(:,1)+compare_corner(:,2);
            if min(compare_corner) > 100
                corner_num = corner_num+1;
                corner_out(corner_num,:) = curve_end(k,:);
                c3 = [c3; 9]; %#ok<AGROW>
            end
        end
    end
end
%%%%%%%%%%%%%%%5

%%%%%Add T-junctions
corner_final = corner_out;
for k=1:size(TJ,1)
    % T-junctions compared with detected corners
    if size(corner_final)>0
        compare_corner = corner_final-ones(size(corner_final,1),1)*TJ(k,:);
        compare_corner = compare_corner.^2;
        compare_corner = compare_corner(:,1)+compare_corner(:,2);
        if min(compare_corner)>100       % Add end points far from detected corners, i.e. outside of 5 by 5 neighbor
            corner_final = [corner_final; TJ(k,:)]; %#ok<AGROW>
            c3 = [c3;10]; %#ok<AGROW>
        end
    else
        corner_final = [corner_final; TJ(k,:)]; %#ok<AGROW>
        c3 = [c3;10]; %#ok<AGROW>
    end
end
