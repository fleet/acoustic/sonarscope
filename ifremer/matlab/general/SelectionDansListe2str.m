% Affichage d'une liste avec des accolades autour de l'item selectionne
%
% Syntax
%   str = SelectionDansListe2str( liste, choix )
%
% Input Arguments 
%   choix : Item selectionne (0==pas d'item selectionne)
%   liste : Liste des items
%
% PROPERTY NAMES :
%  'Num' : Si present --> Numerotation des choix
%
% Output Arguments
%   str : Chaine de caractères
% 
% Examples
%   str = SelectionDansListe2str(1, {'I'; 'am'; 'the'; 'best'})
%   str = SelectionDansListe2str(3, {'I'; 'am'; 'the'; 'best'})
%   str = SelectionDansListe2str(3, {'Hiver'; 'Printemps'; 'Ete'; 'Automne'}, 'Num')
%
% See also BooleenDansListe2str Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function str = SelectionDansListe2str(choix, liste, varargin)

[varargin, Num] = getFlag(varargin, 'Num'); %#ok<ASGLU>

if isempty(choix)
	choix = 0;
end

str = '';
for k=1:length(liste)
    if k ~= 1
        str = sprintf('%s | ', str);
    end
    if choix == k
        if Num
            str = sprintf('%s{''%d=%s''}', str, k, liste{k});
        else
            str = sprintf('%s{''%s''}', str, liste{k});
        end
    else
        if Num
            str = sprintf('%s''%d=%s''', str, k, liste{k});
        elseif isempty(str)
            str = sprintf('%s''%s''', str, liste{k});
        else
            str = sprintf('%s''%s''', str, liste{k});
        end
    end
end
