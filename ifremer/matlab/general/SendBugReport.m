% Description
%   Send a bug report to SonarScope support team
%
% Syntax
%   SendBugReport(ME)
%
% Input Arguments
%   ME : MException instance
%
% Examples
%   try
%       xxxxx
%   catch ME
%       SendBugReport(ME)
%   end
%
% Authors  : JMA
% See also : SendEmailSupport
%-------------------------------------------------------------------------------

function SendBugReport(ME)

message = ME.message;
stack   = ME.stack;

report = getReport(ME, 'extended', 'hyperlinks', 'off');
fprintf('%s\n', report);

setParamsMailServer;

SonarScopeMailSupport = 'sonarscope@ifremer.fr';

try
    str = {message; ''};
    for k=1:length(stack)
        str{end+1} = sprintf('Error in %s (line %d) : %s', stack(k).name, stack(k).line, stack(k).file); %#ok<AGROW>
    end
    
%     sendmail(SonarScopeMailSupport, 'SonarScope bug report 1', str);
    sendmail(SonarScopeMailSupport, 'SonarScope bug report 2', report);
catch ME
    ME.getReport
end
