% Description
%   Send a message to SonarScope support team
%
% Syntax
%   SendEmailSupport(str)
%
% Input Arguments
%   str : Message to be sent (can be a cell array of chars)
%
% Examples
%   SendEmailSupport('Hello world')
%
% Authors  : JMA
% See also : SendBugReport
%-------------------------------------------------------------------------------

function SendEmailSupport(str)

setParamsMailServer;
SonarScopeMailSupport = 'sonarscope@ifremer.fr';

try
    sendmail(SonarScopeMailSupport, 'SonarScope end user message', str);
catch ME
    ME.getReport
end
