function flag = SonarScopeHelp(NomDoc)

global SonarScopeDocumentation %#ok<GVMIS>

logFileId = getLogFileId;
logFileId.info('SonarScopeHelp', NomDoc);

flag = 0;

if strcmp(NomDoc, 'TODO')
    str1 = 'La documentation de cette section n''est pas encore �crite, d�sol�.';
    str2 = 'The documentation of this section is not written yet, sorry.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'SScHelp-ADOC', NomDoc);
if exist(nomFicHelp, 'file')
    my_web(['file://' nomFicHelp])
    flag = 1;
    return
end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', 'SScDoc-Tutorial-ALL', NomDoc);
if exist(nomFicHelp, 'file')
    my_web(['file://' nomFicHelp])
    flag = 1;
    return
end

% nomFicHelp = fullfile(SonarScopeDocumentation, 'DocOther', NomDoc);
% if exist(nomFicHelp, 'file')
%     winopen(nomFicHelp)
%     flag = 1;
%     return
% end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', NomDoc);
if exist(nomFicHelp, 'file')
    winopen(nomFicHelp)
    flag = 1;
    return
end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', 'SScDoc-Tutorial-ALL', NomDoc);
if exist(nomFicHelp, 'file')
    winopen(nomFicHelp)
    flag = 1;
    return
end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', 'SScDoc-Tutorial-RDF', NomDoc);
if exist(nomFicHelp, 'file')
    winopen(nomFicHelp)
    flag = 1;
    return
end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', 'SScDoc-Tutorial-RAW', NomDoc);
if exist(nomFicHelp, 'file')
    winopen(nomFicHelp)
    flag = 1;
    return
end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', 'SScDoc-Tutorial-SEG', NomDoc);
if exist(nomFicHelp, 'file')
    winopen(nomFicHelp)
    flag = 1;
    return
end

nomFicHelp = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'DevCorner-MLX', NomDoc);
if exist(nomFicHelp, 'file')
    winopen(nomFicHelp)
    flag = 1;
    return
end

str1 = sprintf('Le lien "%s" est cass�. V�rifiez que vous disposez de la derni�re version de la documentation.\n\nSi le probl�me persiste apr�s avoir install� la derni�re version, alors envoyez ce message � sonarscope@ifremer.fr SVP', NomDoc);
str2 = sprintf('The link "%s" is broken. Check if you have the latest version of the documentation.\n\nIf the issue is not solved, please send this message to sonarscope@ifremer.fr', NomDoc);
my_warndlg(Lang(str1,str2), 1);
