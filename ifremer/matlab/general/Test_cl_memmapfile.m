% ATTENTION : Il ne faut jamais �tre dans un r�pertoire de classe quand
% vous ex�cutez des fonctions sinon matlab confond fonctions et m�thodes
% surcharg�es. En clair v�rifiez que vous n'�tes pas dans le r�pertoire
% @cl_memmapfile

function Test_cl_memmapfile

m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]); %#ok<*NASGU>
m = cl_memmapfile('FileName', '/tmp/pppp.mem', 'Value', [1 2 3 4; 5 6 7 8]);
get(m, 'FileName')
get(m, 'Value')
m(1)
m(1,2)
m(1) = pi %#ok<*NOPRT>
m(1,2)
m(1, 2:3) = [10 11]
m(1:2,2) = [10; 11]
imagesc(m(1:2,1:4))
imagesc(m)
length(m)
size(m)
min(m)
max(m)

% Adapter N � la largeur max d'une matrice carr�e
% Ici, j'ai 2GO de RAm je peux donc d�clarer une matrice de 15000x15000 en
% single
N = 15000;
try
    m1 = zeros(N, 'single');
    m2 = zeros(N, 'single'); %#ok<*PREALL>
    m3 = zeros(N, 'single');
    m1(1001:2000, 2001:3000) = rand(1000);
    figure; imagesc(m1(901:2100, 1901:3100));
    free(m1);
    clear m1 m2 m3
    'Operation r�ussie'
catch %#ok<CTCH>
    'Il est impossible de travailler avec plusieurs grosses matrices'
end


try
    m1 = cl_memmapfile('Value', single(0), 'Size',  [N N]);
    m2 = cl_memmapfile('Value', single(0), 'Size',  [N N]);
    m3 = cl_memmapfile('Value', single(0), 'Size',  [N N]);
    m1(1001:2000, 2001:3000) = rand(1000);
    figure; imagesc(m1(901:2100, 1901:3100));
    free(m1);
    clear m1
    'Operation r�ussie gr�ce � cl_memmapfile'
catch
    'Pour allouer une matrice de taille sup�rieure � la m�moire vive, il faudrait perfectionner cl_memmapfile en d�clarant plusieurs zones de swap sur le m�me fichier'
end
