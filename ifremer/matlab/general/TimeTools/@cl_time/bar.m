% Trace de y en fonction d'une instance "cl_time"
%
% Syntax
%   h = bar(t, y, ...)
% 
% Input Arguments 
%   t : instance de cl_time
%   y : variable fonction du temps date
%
% Name-Value Pair Arguments 
%   Les parametres de la fonction bar
%
% Output Arguments 
%   h : handle de la courbe cree
%
% Examples
%    % visu sur un an tous les 10 jours
%    t = cl_time('timeIfr', 2452233+ (1:10:365), 0); t2str(t)
%    y = rand(1, length (t));
%    bar (t, y); title ('annee');
%
%    % visu sur un mois tous les jours
%    t = cl_time('timeIfr', 2452233+ (1:1:21), 0); t2str(t)
%    y = rand(1, length (t));
%    bar (t, y); title ('mois');
%
%    % visu sur un jour toutes les heures
%    t = cl_time('timeIfr', 2452233, (0:23)*3600000); t2str(t)
%    y = rand(1, length (t));
%    bar (t, y); title ('jour');
%
%    % visu sur une heure toutes les minutes
%    t = cl_time('timeIfr', 2452233, (0:60)*60000); t2str(t)
%    y = rand(1, length (t));
%    bar (t, y); title ('heure');
%
%    % visu sur une minute toutes les secondes
%    t = cl_time('timeIfr', 2452233, (0:60)*1000); t2str(t)
%    y = rand(1, length (t));
%    bar (t, y); title ('minute');
%
%    % visu sur une seconde tous les 100 eme de seconde
%    t = cl_time('timeIfr', 2452233, (0:600)*10); t2str(t)
%    y = rand(1, length (t));
%    bar (t, y) ; title ('seconde') ;
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function h = bar(t, y, varargin)

% ----------------------------------------------------------------------------
% Realise le bar avec les valeurs numeriques des dates

if isa(t, 'cl_time')&& isnumeric(y)
%     date = floor(t.timeMat);
%     secondes = (t.timeMat - date) * (24*3600);
%     b = timeseries(y, secondes, zeros(size(date)))
%     h = bar(b, varargin{:});

    T = t.timeMat;
    h = bar(T(1:length(y)), y, varargin{:});

    hz = zoom;
%     set(hz,'ActionPreCallback',@myprecallback);
    set(hz,'ActionPostCallback',@mypostcallback);
    set(hz, 'Enable','off');

elseif isa(t, 'cl_time')&& isa(y, 'cl_time')
    h = bar(t.timeMat, y.timeMat, varargin{:});
    
elseif isnumeric(t)&& isa(y, 'cl_time')
    h = bar(t, y.timeMat, varargin{:}) ;
end

% ----------------------------------------------------------------------------
% utilise la mise en forme automatique des dates

try
    datetick ;
catch %#ok<CTCH>
end


% Pas encore parfait,le d�placement (avec la main ne remet pas � jour l'axe x)
function mypostcallback(obj, evd) %#ok<INUSL>
datetick('keeplimits')

KEY = 'graphics_linkaxes';
PPPP = getappdata(evd.Axes, KEY);
haxes = findobj(get(evd.Axes, 'parent'), 'Type', 'Axes');
for i=1:length(haxes)
    if isequal(getappdata(haxes(i), KEY), PPPP)
        datetick(haxes(i), 'keeplimits')
    end
end
