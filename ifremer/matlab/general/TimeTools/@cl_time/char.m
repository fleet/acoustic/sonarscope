% Resume d'une instance "cl_time" 
%
% Syntax
%   str = char( a ) 
%
% Input Arguments
%   a : Un objet de type cl_time
%
% Output Arguments
%   str : une chaine de caracteres representative de l'objet
%
% Examples
%   t = cl_time
%   s = char(t);
%       date  = dayStr2Ifr('14/07/2000')
%       heure = (0:9) * 60000
%   t = cl_time('timeIfr', date, heure)
%   s = char(t);
%   t = cl_time('timeIfr', repmat(date, size(heure)), heure)
%   s = char(t);
%       heure = (0:9) * 20000000
%   t = cl_time('timeIfr', date, heure)
%   s = char(t);
%
%   t = cl_time('timeMat', now+1.5)
%   s = char(t);
%   t = cl_time('timeIfr', dayStr2Ifr('14/07/2000'), (0:9) * 60000)
%   s = char(t);
%
% See also cl_time cl_time/display Authors
% Authors : RL
%-------------------------------------------------------------------------------

% function str = char(a)
function str = char(this)

N = numel(this);
str = cell(2*N,1);
for k=1:N
    if N > 1
        str{2*k-1} = sprintf('-----Element (%d/%d) -------', k, N);
    end
    str1 = char_instance(this(k));
    if N > 1
        str1 = [repmat(' ', size(str1,1), 2) str1]; %#ok<AGROW>
    end
    str{2*k} = str1;
end
str = cell2str(str);
