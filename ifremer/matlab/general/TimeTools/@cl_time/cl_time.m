% Constructeur de la classe "cl_time"
%
% Syntax
%   t = cl_time
%   t = cl_time('timeIfr', date, heure)
% 
% Name-only Arguments
%   codage : 'timeIfr' | 'timemat' | 'datenum' | 'datestr' | 'datevec'
%
% Input Arguments 
%   Si codage = 'timeIfr'
%       date  : Date Ifremerienne (1= 25/11/-4713)
%       heure : Nombre de millisecondes depuis minuit 
%   Si codage = 'timeMat'
%       tMat : Temps au format matlab (Nombre de jours depuis le 00-Jan-0000 + Fraction d'une journee
%
% Output Arguments 
%   t : Instance de type cl_time.
%
% Remarks : Si pas d'arguments d'entree, c'est la date et l'heure actuelle qui est rentree.
%           Sinon, on peut donner une seule date et un tableau d'heures ou des
%           tableaux de dates et d'heures de meme dimension.
% 
% Examples
%   t = cl_time
%       date  = dayStr2Ifr('14/07/2000')
%       heure = (0:9) * 60000
%   t = cl_time('timeIfr', date, heure)
%   t = cl_time('timeIfr', date(1), heure)
%
%   t = cl_time('timeMat', now)
%   t = cl_time('timeMat', (now:0.25:now+1))
%
%   tu = 86400*(datenum('2017/05/01', 'yyyy/mm/dd')-datenum('1970/01/01', 'yyyy/mm/dd'));
%   t = cl_time('timeUnix', tu)
%
% See also Authors timeIfr2Mat datetimeTransition
% Authors : JMA
%--------------------------------------------------------------------------

function obj = cl_time(varargin)

OK = 1;

switch nargin
case 0
    s.timeMat = now;
    
case 2
    if strcmp(varargin{1}, 'timeMat')
        if length(varargin) == 2
            t = varargin{2};
            if isnumeric(t)
                s.timeMat  = double(t);
            else
                OK = 0;
            end
        else
            OK = 0;
        end
    elseif strcmp(varargin{1}, 'timeUnix')
        if length(varargin) == 2
            t = varargin{2};
            if isnumeric(t)
%                 date  = dayStr2Ifr('01/01/1970');
%                 tdeb = cl_time('timeIfr', date, 0);
%                 offset = tdeb.timeMat;
                offset = 719529;
                s.timeMat  = offset + double(t)/(24*3600);
            else
                OK = 0;
            end
        else
            OK = 0;
        end

    else
        OK = 0;
    end
    
case 3
    if strcmp(varargin{1}, 'timeIfr')
        if length(varargin) == 3
            nbMilisUnJour = 24*3600*1000;
            date  = double(varargin{2});
            heure = double(varargin{3});
%             date  = date(:);
%             heure = heure(:);
            
            J = floor(heure / nbMilisUnJour);
            heure = heure - J * nbMilisUnJour;
            date  = date + J;
            
            if length(date) == 1
                s.timeMat  = timeIfr2Mat(ones(size(heure)) * date, heure);
            else
                if length(heure) == 1
                    s.timeMat  = timeIfr2Mat(date, ones(size(date)) * heure);
                elseif length(date) == length(heure)
                    s.timeMat  = timeIfr2Mat(date, heure);
                else
                    my_warndlg('cl_time:cl_time Nombre d arguments ou taille de ceux ci incorrects', 1) ;
                    s.timeMat  = [];
                end
            end
        else
            OK = 0;
        end
    else
        OK = 0;
    end
end

if OK == 0
    disp('Directive pas comprise')
    my_warndlg('cl_time:cl_time Beurk', 1);
    return
end

% s.Datetime = datetime(s.timeMat', 'ConvertFrom', 'datenum'); % Remplac� le 06/01/2020 par m�thode dans subsref
obj = class(s, 'cl_time');
