% Date en ASCII d'une instance "cl_time"
%
% Syntax
%   date2str(t)
%
% Input Arguments
%   t : un objet de la classe cl_time
%
% Output Arguments
%   s : Chaine de caracteres
%
% Examples
%   t = cl_time('timeMat', (now:0.25:now+1))
%   date2str(t)
%
% See also cl_time cl_time/hourIfr2Str cl_time/t2str Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function s = date2str(t, varargin)

[varargin, Format] = getPropertyValue(varargin, 'Format', 1); %#ok<ASGLU>

switch Format
    case 1
        s = dayMat2str(t.timeMat);
    case 2
        s = dayMat2strUK(t.timeMat);
    otherwise
end
