% Ecart entre valeurs d'une serie temporelle contenue dans une instance "cl_time"
%
% Syntax
%   dt = diff(t)
%
% Input Arguments
%   t  : Serie temporelle (Instance de type cl_time)
%
% Output Arguments
%   dt : Ecarts temporels en millisecondes.
%
% Examples
%   t = cl_time('timeMat', (now:0.25:now+1))
%   dt = diff(t)
%
% See also cl_time/min cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function dt = diff(t)

tmt1 = t.timeMat - min(t.timeMat);
nbMilisUnJour = 24*3600*1000;
dt = diff(tmt1) * nbMilisUnJour;
