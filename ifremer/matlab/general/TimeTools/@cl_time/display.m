% Affichage du resume d'une instance "cl_time"
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : un objet de la classe cl_time
%
% Examples
%   t = cl_time('timeMat', (now:0.25:now+1))
%   display(t);
%
% See also cl_time cl_time/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));

%{
s = whos('a');
sz = s.size;
fprintf('Name\tSize\tClass');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
fprintf('%s', char(a));
%}
