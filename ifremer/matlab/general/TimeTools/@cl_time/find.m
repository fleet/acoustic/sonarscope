% Recherche du plus proche voisin d'un temps particulier dans une serie temporelle contenue dans une instance "cl_time"
%
% Syntax
%   index = find(t)
%
% Input Arguments
%   t  : Serie temporelle (Instance de type cl_time)
%   t1 : Temps a rechercher (Instance de type cl_time)
%
% Output Arguments
%   index : Index du temps maximal.
%
% Examples
%   date  = dayStr2Ifr('14/07/2000')
%   heure = (0:9) * 60000
%   t = cl_time('timeIfr', date, heure)
%   index = find(t, t(3))
%   t(index)
%
% See also cl_time/max cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function index = find(t, t1)

dt = t.timeMat - t1.timeMat;
[~, index] = min(abs(dt));
