% Heure en ASCII d'une instance "cl_time"
%
% Syntax
%   s = hourIfr2Str( t )
% 
% Input Arguments 
%   t : un objet de la classe cl_time
%
% Output Arguments 
%   s : Chaine de caracteres
%
% Examples
%   t = cl_time('timeIfr', dayStr2Ifr('14/07/2000'), (0:9) * 60000)
%   heure2str(t)
%
% See also cl_time cl_time/dayIfr2Str cl_time/t2str Authors
% Authors : JMA
% VERSION  : $Id: heure2str.m,v 1.2 2002/06/06 11:57:46 augustin Exp $
% ----------------------------------------------------------------------------

function s = heure2str(t)

[date, heure] = timeMat2Ifr(t.timeMat);

s = hourIfr2str(heure);
if iscell(s)
    s = sprintf('%s', s{:});
    s = reshape(s, 12, length(heure))';
end