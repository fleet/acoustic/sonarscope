% Fonction de concatenation horizontale de deux temps (signe [])
%
% Syntax
%   t1 == t2 
% 
% Input Arguments 
%   t1 : un objet de la classe cl_time
%   t2 : un objet de la classe cl_time
%
% Output Arguments 
%   r : Booleen
%
% Examples
%   t1 = cl_time;
%   t2 = cl_time;
%   [t1 t2]
%
% See also cl_time  Authors
% Authors : JMA
% VERSION  : $Id: horzcat.m,v 1.2 2002/06/06 11:57:46 augustin Exp $
% ----------------------------------------------------------------------------

function r = horzcat(a,b)

if isempty(a)
    r = b;
elseif isempty(b)
    r = a;
else
    r = cl_time('timeMat', [a.timeMat; b.timeMat]);
end
