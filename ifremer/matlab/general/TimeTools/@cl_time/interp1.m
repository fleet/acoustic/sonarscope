% Interpolation d'une donnee en datee
%
% Syntax
%   y2 = interp1(t1, y1, t2, ...)
%
% Input Arguments
%   t1 : Premiere serie temporelle (Instance de type cl_time)
%   y1 : Valeurs de la donnee aux abscisses t1
%   t2 : Deuxieme serie temporelle (Instance de type cl_time)
%
% Name-Value Pair Arguments
%   Parametres de la fonction interp1 (method,et 'extrap')
%
% Output Arguments
%   y2 : Valeurs de la donnee aux abscisses t2
%
% Examples
%   date  = dayStr2Ifr('14/07/2000')
%   heure = (0:9) * 60000
%   t1 = cl_time('timeIfr', date, heure)
%   t2 = cl_time('timeIfr', date, heure(4:6))
%   y2 = interp1(t1, 0:9, t2)
%
% See also cl_time/max cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y2 = interp1(t1, y1, t2, varargin)

D = diff(t1.timeMat);
sub = find(D == 0);
if ~isempty(sub)
    sub = unique([sub sub+1]);
    sub(sub> length(D)) = [];
    t1.timeMat(sub) = [];
    y1(sub) = [];
end

% [varargin, Previous] = getFlag(varargin, 'previous', 'extrap');
% if Previous
% [varargin, extrap] = getFlag(varargin, 'extrap');
%
%     ind1 = 1:length(t1.timeMat);
%     ind2 = my_interp1(t1.timeMat, ind1, t2.timeMat, 'linear', 'extrap');
%     ind2 = floor(ind2+eps('single'));
%
%     if extrap
%         ind2(ind2 < 1) = 1;
%         N = length(y1);
%         ind2(ind2 > N) = N;
%     end
%
% %     figure; plot(t1.timeMat, ind1, '+k'); grid on; hold on; plot(t2.timeMat, ind2,'xr')
%
%     y2 = y1(ind2);
% else
y2 = my_interp1(t1.timeMat, y1, t2.timeMat, varargin{:});
% end
