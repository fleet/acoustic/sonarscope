% Intersection de 2 series temporelles
%
% Syntax
%   [sub1, sub2, tdeb, tfin] = intersect(t1, t2)
%
% Input Arguments
%   t1  : Premiere serie temporelle (Instance de type cl_time)
%   t2  : Deuxieme serie temporelle (Instance de type cl_time)
%
% Output Arguments
%   sub1 : Index des temps communs dans la serie t1.
%   sub2 : Index des temps communs dans la serie t2.
%   tdeb : Temps du debut de l'intersection
%   tfin : Temps de la fin de l'intersection
%
% Examples
%   date  = dayStr2Ifr('14/07/2000')
%   heure = (0:9) * 60000
%   t1 = cl_time('timeIfr', date, heure)
%   t2 = cl_time('timeIfr', date, heure(4:6))
%   [sub1, sub2, tdeb, tfin] = intersect(t1, t2)
%
% See also cl_time/max cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [sub1, sub2, tdeb, tfin] = intersect(t1, t2, varargin)

[varargin, Precision] = getPropertyValue(varargin, 'Precision', 0); %#ok<ASGLU>

if Precision ~= 0
    P = hourIfr2Mat(Precision*1000);
    
    %    TimeMat2str(t1.timeMat)
    T1 = round(t1.timeMat / P);
    T1 = T1 * P;
    %     TimeMat2str(T1)
    
    T2 = round(t2.timeMat / P);
    T2 = T2* P;
    %     TimeMat2str(T2)
    
    [c, sub1, sub2] = intersect(T1, T2);
    tdeb = c(1);
    tfin = c(end);
else
    [c, sub1, sub2] = intersect(t1.timeMat, t2.timeMat);
    if isempty(c)
        tdeb = [];
        tfin = [];
        return
    end
    tdeb = c(1);
    tfin = c(end);
end
