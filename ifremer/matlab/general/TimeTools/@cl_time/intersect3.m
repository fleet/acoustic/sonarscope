% Intersection de 3 series temporelles
%
% Syntax
%   [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, t2, t3)
% 
% Input Arguments 
%   t1  : Premi�re serie temporelle (Instance de type cl_time)
%   t2  : Deuxi�me serie temporelle (Instance de type cl_time)
%   t3  : Troisi�me serie temporelle (Instance de type cl_time)
%
% Output Arguments 
%   sub1 : Index des temps communs dans la serie t1.
%   sub2 : Index des temps communs dans la serie t2.
%   sub3 : Index des temps communs dans la serie t3.
%   tdeb : Temps du debut de l'intersection
%   tfin : Temps de la fin de l'intersection
%
% Examples
%   date  = dayStr2Ifr('14/07/2000')
%   heure = (0:9) * 60000
%   t1 = cl_time('timeIfr', date, heure)
%   t2 = cl_time('timeIfr', date, heure(4:6))
%   t3 = cl_time('timeIfr', date, heure(5:7))
%   [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, t2, t3)
%
% See also cl_time/max cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, t2, t3)

[c, sub1_12, sub2_12] = intersect(t1.timeMat, t2.timeMat);
if isempty(c)
    sub1 = [];
    sub2 = [];
    sub3 = [];
    tdeb = [];
    tfin = [];
    return
end

[c, sub1_13, sub3_13] = intersect(t1.timeMat(sub1_12), t3.timeMat);
if isempty(c)
    sub1 = [];
    sub2 = [];
    sub3 = [];
    tdeb = [];
    tfin = [];
    return
end

sub1 = sub1_12(sub1_13);
sub2 = sub2_12(sub1_13);
sub3 = sub3_13;

tdeb = c(1);
tfin = c(end);
