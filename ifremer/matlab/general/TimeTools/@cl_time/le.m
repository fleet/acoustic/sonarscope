% Test "<=" de 2 instances "cl_time"
%
% Syntax
%   t1 <= t2
%
% Input Arguments
%   t1 : un objet de la classe cl_time
%   t2 : un objet de la classe cl_time
%
% Output Arguments
%   r : Booleen
%
% Examples
%   t1 = cl_time;
%   t2 = cl_time;
%   t1 <= t2
%   t1 <= t1
%   t2 <= t1
%
% See also cl_time cl_time/eq cl_time/ne cl_time/lt cl_time/ge cl_time/gt Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function r = le(a,b)

r =  ((a.timeMat - b.timeMat) <= 0);
