% Longueur de la serie temporelle d'une instance "cl_time"
%
% Syntax
%   n = length( a )  
%
% Input Arguments 
%   a : Un objet de la classe cl_time
%
% Output Arguments
%   n : Nombre de temps de l'objet a
%
% Examples
%   t = cl_time('timeIfr', dayStr2Ifr('14/07/2000'), (0:9) * 60000)
%   length(t)
%
% See also cl_time Authors
% Authors : JMA
% VERSION  : $Id: length.m,v 1.2 2002/06/06 11:57:46 augustin Exp $
%-------------------------------------------------------------------------------
  
% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   14/11/01 - JMA - Creation.
% ----------------------------------------------------------------------------

function n = length( a )

n = length(a.timeMat);
