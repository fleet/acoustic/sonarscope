% Maximum d'une serie temporelle contenue dans une instance "cl_time"
%
% Syntax
%   t2 = max(t)
%   [t2, index] = max(t)
%
% Input Arguments
%   t : Serie temporelle a trier (Instance de type cl_time)
%
% Output Arguments
%   t2 : Temps maximal (Instance de type cl_time).
%   index : Index du temps maximal.
%
% Examples
%   date = floor(rand(1,10) * 10) + 2451740;
%   heure = rand(1,10) * 6000000;
%   t = cl_time('timeIfr', date, heure);
%   t2str(t)
%   [t2, index] = max(t)
%   t2str(t2)
%
% See also cl_time/max cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [t2, index] = max(t)

[~, index] = max(t.timeMat);
t2 = cl_time('timeMat', t.timeMat(index));
