% Minimum d'une serie temporelle contenue dans une instance "cl_time"
%
% Syntax
%   t2 = min(t)
%   [t2, index] = min(t)
%
% Input Arguments
%   t : Serie temporelle a trier (Instance de type cl_time)
%
% Output Arguments
%   t2 : Temps minimal (Instance de type cl_time).
%   index : Index du temps minimal.
%
% Examples
%   date = floor(rand(1,10) * 10) + 2451740;
%   heure = rand(1,10) * 6000000;
%   t = cl_time('timeIfr', date, heure);
%   t2str(t)
%   [t2, index] = min(t)
%   t2str(t2)
%
% See also cl_time/max cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [t2, index] = min(t)

[~, index] = min(t.timeMat);
t2 = cl_time('timeMat', t.timeMat(index));
