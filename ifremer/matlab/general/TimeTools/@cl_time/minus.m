% Difference de temps : t2=t1-dt ou dt=t1-t2
%
% Syntax
%   t2 = t - dt 
%   dt = t - t1 
% 
% Input Arguments 
%   t  : un objet de la classe cl_time
%   dt : Nombre en millisecondes ou 1 temps particulier
%
% Output Arguments 
%   t2 : un objet de la classe cl_time ou un nombre de millisecondes
%
% Examples
%   t = cl_time
%   t - 1000
%   t - 1000*3600*24
%   date  = dayStr2Ifr('14/07/2000')
%   heure = (0:9) * 20000000
%   t = cl_time('timeIfr', date, heure)
%   t - 1000
%   t - t(1)
%
% See also cl_time cl_time/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function r = minus(t, dt)

if isobject(t) && isnumeric(dt)
    r = cl_time('timeMat', t.timeMat - (double(dt) / 86400000));
    return
end

if(isobject(t) && isobject(dt))
    r = (t.timeMat - dt.timeMat) * 86400000;
    return
end

my_warndlg('cl_time/minus : Probleme de type de variable', 1);
r = [];
