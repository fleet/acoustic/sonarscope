% Addition d'un scalaire a une instance "cl_time" (t2=t1+dt) ou
% de 2 instances "cl_time"
%
% Syntax
%   t2 = t1 + dt 
% 
% Input Arguments 
%   t1 : un objet de la classe cl_time
%   dt : Nombre en millisecondes
%
% Output Arguments 
%   t2 : un objet de la classe cl_time
%
% Examples
%   t = cl_time
%   t + 1000
%   t + 1000*3600*24
%   date  = dayStr2Ifr('14/07/2000')
%   heure = (0:9) * 20000000
%   t = cl_time('timeIfr', date, heure)
%   t + 1000
%   2000 + t
%   t + cl_time
%
% See also cl_time cl_time/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function r = plus(a, b)

if isobject(a) && isnumeric(b)
    r = cl_time('timeMat', a.timeMat + double(b) / 86400000);
elseif isnumeric(a) && isobject(b)
    r = cl_time('timeMat', b.timeMat + double(a) / 86400000);
elseif isobject(a) && isobject(b)
    r = cl_time('timeMat', a.timeMat + b.timeMat);
else
    my_warndlg('cl_time:plus On ne peut pas additionner 2 temps', 1);
    r = [];
    return
end
