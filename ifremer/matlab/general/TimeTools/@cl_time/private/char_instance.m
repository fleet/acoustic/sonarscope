% Resume d'une instance "cl_time" 
%
% Syntax
%   str = char( a ) 
%
% Input Arguments
%   a : Un objet de type cl_time
%
% Output Arguments
%   str : une chaine de caracteres representative de l'objet
%
% Examples
%   t = cl_time
%   s = char(t);
%       date  = dayStr2Ifr('14/07/2000')
%       heure = (0:9) * 60000
%   t = cl_time('timeIfr', date, heure)
%   s = char(t);
%   t = cl_time('timeIfr', repmat(date, size(heure)), heure)
%   s = char(t);
%       heure = (0:9) * 20000000
%   t = cl_time('timeIfr', date, heure)
%   s = char(t);
%
%   t = cl_time('timeMat', now+1.5)
%   s = char(t);
%   t = cl_time('timeIfr', dayStr2Ifr('14/07/2000'), (0:9) * 60000)
%   s = char(t);
%
% See also cl_time cl_time/display Authors
% Authors : RL
%-------------------------------------------------------------------------------

function str = char_instance(a)

str = [];

[date, heure] = timeMat2Ifr(a.timeMat);
% str{end+1} = sprintf(' date <-> %s', num2strCode(date) );
% str{end+1} = sprintf('heure <-> %s', num2strCode(heure) );
switch length(a.timeMat)
    case 0
        %             str{end+1} = sprintf('[]');
    case 1
        str{end+1} = sprintf('%s  %s', dayIfr2str(date(1)), hourIfr2str(heure(1)));
    otherwise
        str{end+1} = sprintf('%d values from : %s  %s to : %s  %s', ...
            length(date), ...
            dayIfr2str(date(1)),   hourIfr2str(heure(1)), ...
            dayIfr2str(date(end)), hourIfr2str(heure(end)) );
end

str = sprintf('%s', str{:});
