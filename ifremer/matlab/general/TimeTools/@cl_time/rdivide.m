% Division d'un temps : t2=t1./x
%
% Syntax
%   t2 = t / x
% 
% Input Arguments 
%   t : un objet de la classe cl_time
%   x : Scalaire 
%
% Output Arguments 
%   t2 : un objet de la classe cl_time
%
% Examples
%   date  = dayStr2Ifr('14/07/2000');
%   heure = (0:9) * 60000;
%   t = cl_time('timeIfr', date, heure);
%   t2str(t)
%   t2 = t(1) + t(10);
%   t3 = t2 ./ 2;
%   t2str(t3)
%
% See also cl_time cl_time/plus Authors
% Authors : JMA
% VERSION  : $Id: rdivide.m,v 1.2 2002/06/06 11:57:46 augustin Exp $
% ----------------------------------------------------------------------------

function r = rdivide(t, x)

if isobject(t) && isnumeric(x)
    tmatlab = t.timeMat ./ double(x);
    r = cl_time('timeMat', tmatlab);
    return
end

my_warndlg('cl_time/rdivide : Probleme de type de variable', 1);
r = [];

