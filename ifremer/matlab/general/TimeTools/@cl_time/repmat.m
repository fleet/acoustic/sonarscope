% Repmat ddu contenu d'une instances "cl_time"
%
% Syntax
%   t2 = repmat(t1, nL, nR)
%
% Input Arguments
%   t1 : un objet de la classe cl_time
%   nL : Nombre de lignes
%   nC : Nombre de colonnes
%
% Output Arguments
%   t2 : un objet de la classe cl_time
%
% Examples
%   t1 = cl_time
%   t2 = repmat(t1, 2, 5)
%
% See also cl_time Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = repmat(this, nL, nR)

this.timeMat = repmat(this.timeMat, nL, nR);