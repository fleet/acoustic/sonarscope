% Tri d'une serie temporelle contenue dans une instance cl_time
%
% Syntax
%   t2 = sort(t)
%   [t2, ordre] = sort(t)
%
% Input Arguments
%   t : Serie temporelle a trier (Instance de type cl_time)
%
% Output Arguments
%   t2    : Serie temporelle triee (Instance de type cl_time).
%   ordre : Serie temporelle triee (Instance de type cl_time).
%
% Examples
%   heure = (0:9) * 60000
%   t = cl_time('timeIfr', 2451740, fliplr(heure));
%   t2str(t)
%   [t2, ordre] = sort(t)
%   t2str(t2)
%
% See also dayStr2Ifr dayIfr2Str hourStr2Ifr hourIfr2Str cl_time/ Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [t2, ordre] = sort(t)

[t2, ordre] = sort(t.timeMat);
t2 = cl_time('timeMat', t2);
