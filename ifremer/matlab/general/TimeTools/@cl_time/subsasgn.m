% Accesseur en ecriture aux attributs date heure et element d'un tableau
%
% Syntax
%   t.date = date
%   t.heure = heure
%   t(indice) = t2
% 
% Input Arguments 
%   date  : les dates de l'objet t
%   heure : les dates de l'objet t
%   t2    : une instance de la classe cl_time ou un nombre de millisecondes
%
% Output Arguments 
%   t  : une instance de la classe cl_time
%
% Examples
%   heure = (0:9) * 20000000;
%   t = cl_time('timeIfr', 2451740, heure);
%   t2str(t)
%   d = t.date;
%   d(2) = 2451730;
%   t.date = d;
%   t2str(t)
%   h = t.heure;
%   h(3) = hourStr2Ifr('13:13:13.130');
%   t.heure = h;
%   t2str(t)
%   t2 = t(4);
%   t(4) = t(1);
%   t2str(t)
%
% See also cl_time cl_time/subsasgn Authors
% Authors : JMA
% VERSION  : $Id: subsasgn.m,v 1.2 2002/06/06 11:57:46 augustin Exp $
% ----------------------------------------------------------------------------

function A = subsasgn(obj, S, B)

if isempty(S)
	return 
end


theType = S(1).type;
theSubs = S(1).subs;

switch theType
case '()'
    theSubs = theSubs{:};
    t = obj.timeMat;
    if length(S) == 1
        A = B;
    else
        A = subsasgn(cl_time('timeMat', t(theSubs)), S(2:end), B);
    end
    t(theSubs) = A.timeMat;
    A = cl_time('timeMat', t);
case '.' 
    [dIfr, hIfr] = timeMat2Ifr(obj.timeMat);
    switch theSubs
    case 'date'
        A = cl_time('timeIfr', B, hIfr);
    case 'heure'
        A = cl_time('timeIfr', dIfr, B);
    otherwise
        A = [];
    end
end
