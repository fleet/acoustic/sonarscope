% Accesseur en ecriture aux attributs date heure et element d'un tableau
%
% Syntax
%   t2 = t(index)
% 
% Input Arguments 
%   t  : une instance de la classe cl_time
%
% Output Arguments 
%   t2 : une instance de la classe cl_time
%
% Examples
%   heure = (0:9) * 20000000;
%   t = cl_time('timeIfr', 2451740, heure);
%   t2str(t)
%   d = t.date;
%   d(2) = 2451730;
%   t.date = d;
%   t2str(t)
%   h = t.heure;
%   h(3) = hourStr2Ifr('13:13:13.130');
%   t.heure = h;
%   t2str(t)
%   t2 = t(4);
%   t(4) = t(1);
%   t2str(t)
%
% See also cl_time cl_time/subsref cl_time/subsasgn Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function A = subsindex(A, S)

if isempty(S)
    return 
end

theSubs = S(1).subs;
theSubs = theSubs{1};

A = cl_time('timeMat', A.timeMat(theSubs));
