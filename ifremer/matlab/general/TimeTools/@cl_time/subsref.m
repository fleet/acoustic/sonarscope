% Accesseur en lecture aux attributs date heure et element d'un tableau
%
% Syntax
%   date = t.date 
%   heure = t.heure 
%   t2 = t(indice)
% 
% Input Arguments 
%   t  : un objet de la classe cl_time
%
% Output Arguments 
%   date  : les dates de l'objet t
%   heure : les dates de l'objet t
%   t2    : un objet de la classe cl_time ou un nombre de millisecondes
%
% Examples
%   heure = (0:9) * 20000000;
%   t = cl_time(2451740, heure)
%   t.date
%   t.heure
%   t(2)
%   t(2).date
%
% See also cl_time cl_time/subsasgnAuthors
% Authors : JMA
% ----------------------------------------------------------------------------

function theResult = subsref(obj, theStruct)

if isempty(theStruct)
    theResult = obj;
    return 
end

theType = theStruct(1).type;
theSubs = theStruct(1).subs;

switch theType
case '()'
%     theSubs = theSubs{:};
%     theResult = subsref(cl_time(obj.date(theSubs), obj.heure(theSubs)), theStruct(2:end));
    theResult = subsindex(obj, theStruct);
    if length(theStruct) == 2
        theResult = subsref(theResult, theStruct(2:end));
    end
case '.' 
    [dIfr, hIfr] = timeMat2Ifr(obj.timeMat);
    switch theSubs
    case 'date'
        theResult = dIfr;
    case 'heure'
        theResult = hIfr;
    case 'timeMat'
        theResult = obj.timeMat;
    case 'Datetime'
%         theResult = obj.Datetime; % Remplac� le 06/01/2020 par m�thode dans subsref
        theResult = datetime(obj.timeMat', 'ConvertFrom', 'datenum');
    otherwise
        theResult = [];
    end
end
