function str = summary(this)

[date, heure] = timeMat2Ifr(this.timeMat);
switch length(date)
    case 0
        %             str{end+1} = sprintf('[]');
        str = '';
    case 1
        str = sprintf('(%s  %s)', dayIfr2str(date(1)), hourIfr2str(heure(1)) );
    otherwise
        str = sprintf('%s  %s  -  %s  %s', ...
            dayIfr2str(date(1)),   hourIfr2str(heure(1)), ...
            dayIfr2str(date(end)), hourIfr2str(heure(end)) );
end


