% Date et heure en ASCII d'une instance "cl_time" : PAS TERMINE pour ERmapper
%
% Syntax
%   s = t2str(t, ...)
%
% Input Arguments
%   t : un objet de la classe cl_time
%
% Name-Value Pair Arguments
%   Format : 1=A la francaise : "28/07/2004  11:01:50.925"
%            2=A l'anglaise   : "2004/07/08  11:01:50.925"
%
% Output Arguments
%   s : Chaine de caracteres
%
% Examples
%   t = cl_time('timeIfr', dayStr2Ifr('14/07/2000'), (0:9) * 60000)
%   t2str(t)
%
% See also cl_time cl_time/hourIfr2Str cl_time/dayIfr2Str Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function s = t2str(t, varargin)

[varargin, Format] = getPropertyValue(varargin, 'Format', 1); %#ok<ASGLU>

s = [date2str(t, 'Format', Format) repmat(' ', length(t.timeMat), 2) heure2str(t)];
