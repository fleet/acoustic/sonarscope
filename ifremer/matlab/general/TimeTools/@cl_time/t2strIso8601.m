% Formattage du cl_time � la norme ISO8601
%
% Syntax
%   str = t2strIso8601(T);
%
% Name-only Arguments
%   NULL
%
% Input Arguments
%   T de type cl_time.
%
% Output Arguments
%   str : Cha�ne.
%
% Remarks :
%   Formattage de temps reconnu automatiquement sous Java par des m�thodes automatiques.
% Examples
%   t   = cl_time
%         date  = dayStr2Ifr('14/07/2000')
%         heure = (0:9) * 60000
%   t   = cl_time('timeIfr', date, heure)
%   str = t2strIso8601(t)
%
% See also Authors
% Authors : GLU
%--------------------------------------------------------------------------

function str = t2strIso8601(T)

% TODO : peut certainement �tre am�lior� en utilisant la classe datetime

dateT = date2str(T);
heureT = heure2str(T);
n = size(dateT,1);
strseparator(1:n,1) = '-';
strT(1:n,1) = 'T';
str = [dateT(:,end-3:end) strseparator dateT(:,4:5) strseparator dateT(:,1:2) strT heureT(:,:)];
