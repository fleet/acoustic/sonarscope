% Tranzposition du contenu d'une instance "cl_time"
%
% Syntax
%   t2 = t1'
%
% Input Arguments
%   t1 : un objet de la classe cl_time
%
% Output Arguments
%   t2 : un objet de la classe cl_time
%
% Examples
%   t = cl_time
%   t = repmat(t1, 2, 5)
%   t2 = t'
%
% See also cl_time cl_time/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = transpose(this)

this.timeMat = (this.timeMat).';