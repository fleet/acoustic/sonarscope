% Unique d'une serie temporelle contenue dans une instance cl_time
%
% Syntax
%   t2 = unique(t)
%   [t2, m, n] = unique(t)
%
% Input Arguments
%   t : Serie temporelle a trier (Instance de type cl_time)
%
% Output Arguments
%   t2 : Serie temporelle triee (Instance de type cl_time).
%   m  : Serie temporelle triee (Instance de type cl_time).
%   n  : Voir fonction unique
%
% Examples
%   heure = fliplr((0:9) * 60000);
%   t = cl_time('timeIfr', 2451740, fliplr(heure));
%   unique(t)
%   [t2, ordre] = unique(t)
%   t2str(t2)
%
% See also cl_time/sort Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [t2, m, n] = unique(t, varargin)

[b, m, n] = unique(t.timeMat, varargin{:});
t2 = cl_time('timeMat', b);
