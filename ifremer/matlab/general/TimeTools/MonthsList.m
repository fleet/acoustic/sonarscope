% Liste des mois de l'annee OU nom du mois
%
% Syntax
%   liste = MonthsList
%   liste = MonthsList(listeMois)
%
% Name-Value Pair Arguments
%   listeMois : Numeros des mois demandes
%
% Output Arguments
%   liste : Liste des mois demandes.
%
% Examples
%   liste = MonthsList
%   liste = MonthsList(2)
%   liste = MonthsList(7:9)
%
% See also SeasonsList Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function liste = MonthsList(varargin)

liste = {'January';'February';'March';'April';'May';'June';'July'; ...
    'August';'September';'October';'November';'December'};

if nargin == 1
    ident = varargin{1};
    if length(ident) == 1
        liste = liste{ident};
    else
        liste = liste(ident);
    end
end
