function [flag, tMat] = OTUS_getTimeFromFileName(nomFic)

[~, nomFic1] = fileparts(nomFic);
nomFic1(strfind(nomFic1, '_')) = '-';

mots = strsplit(nomFic1, '-');

if length(mots) > 6
    Jour    = str2double(mots{2});
    Mois    = str2double(mots{3});
    Annee   = str2double(mots{4});
    Heure   = str2double(mots{5});
    Minute  = str2double(mots{6});
    Seconde = str2double(mots{7});
    Milli   = str2double(mots{8});
else % $SCAMPI,04/11/11,19:21:44.000,N, 47,15.29797,W, 004,23.41007,108.7,11.84,122,112.8,1044
    Jour    = str2double(mots{4}(7:8));
    Mois    = str2double(mots{4}(5:6));
    Annee   = str2double(mots{4}(1:4));
    Heure   = str2double(mots{5}(1:2));
    Minute  = str2double(mots{5}(3:4));
    Seconde = str2double(mots{5}(5:6));
    Milli   = str2double(mots{6});
end

date  = dayJma2Ifr(Jour, Mois, Annee);
heure = (Heure*3600 + Minute*60 + Seconde)*1000 + Milli;

tMat = timeIfr2Datetime(date, heure);
flag = 1;
