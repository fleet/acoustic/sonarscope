function [flag, Datetime, MillisecondsInImageName] = SCAMPI_getTimeFromFileName(nomFic)

[~, nomFic1] = fileparts(nomFic);
% nomFic1(strfind(nomFic1, '_')) = '-';

mots = strsplit(nomFic1, '-');

nbMots = length(mots);
if length(mots{end}) == 6
    k1 = nbMots - 1;
    k2 = nbMots;
    k3 = [];
    MillisecondsInImageName = 0;
else
    k1 = nbMots - 2;
    k2 = nbMots - 1;
    k3 = nbMots;
    MillisecondsInImageName = 1;
end

if (length(mots{k1}) ~= 8) || (length(mots{k2}) ~= 6)
    Datetime  = [];
    MillisecondsInImageName = [];
    flag = 0;
    return
end

Annee   = str2double(mots{k1}(1:4));
Mois    = str2double(mots{k1}(5:6));
Jour    = str2double(mots{k1}(7:8));
Heure   = str2double(mots{k2}(1:2));
Minute  = str2double(mots{k2}(3:4));
Seconde = str2double(mots{k2}(5:6));
if isempty(k3)
    Milli = 0;
else
    Milli = str2double(mots{k3});
end

date  = dayJma2Ifr(Jour, Mois, Annee);
heure = (Heure*3600 + Minute*60 + Seconde)*1000 + Milli;

Datetime = timeIfr2Datetime(date, heure);
% Datetime = datetime([mots{k1} ' ' mots{k2}], 'InputFormat', 'yyyyMMdd HHmmss') + duration(0, 0, 0, Milli);

flag = 1;
