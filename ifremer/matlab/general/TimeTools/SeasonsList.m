% Saisons de l'annee
%
% Syntax
%   liste = SeasonsList
%   liste = SeasonsList(listeSeasons)
%
% Name-Value Pair Arguments
%   listeSeasons : Numeros des saisons demandee
%
% Output Arguments
%   liste : Liste des mois demandes.
%
% Examples
%   liste = SeasonsList
%   liste = SeasonsList(2)
%   liste = SeasonsList([2 4])
%
% See also MonthsList Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function liste = SeasonsList(varargin)

liste = {'Winter'; 'Spring'; 'Summer'; 'Autumn'};

if nargin == 1
    ident = varargin{1};
    if length(ident) == 1
        liste = liste{ident};
    else
        liste = liste(ident);
    end
end
