% V�rifie si la date de cr�ation d'un fichier est post�rieure � une date fix�e
%
% Syntax
%   flag = checkFileCreation(nomFic, strDate)
%
% Input Arguments
%   nomFic  : Nom du fichier
%   strDate : Date (dd/mm/yyyy)
%
% Output Arguments
%   flag : 1 le fichier a �t� cr�e apr�s la date sp�cifi�e
%          0 le fichier a �t� cr�e avant la date sp�cifi�e
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   flag = checkFileCreation(nomFic, '26/09/2008')
%   flag = checkFileCreation(nomFic, 2008, 11, 25, 16, 21, 23)
%   flag = checkFileCreation(nomFic, 2008, 11, 25, 16, 21, 24)
%
% See also toto Authors
% Authors    : JMA
%-------------------------------------------------------------------------------

function flag = checkFileCreation(nomFic, strDate, varargin)

flag = 0;
if ~exist(nomFic, 'file')
    return
end

files = dir(nomFic);
if isempty(files)
    return
end
DateFic = files.datenum;

%{
[nomDir, nomFicSeul, ext] = fileparts(nomFic);
files = dir(nomDir);

iFic = [];
for i=1:length(files)
    k = strfind(files(i).name, '.');
    if isempty(k)
        nom = files(i).name;
    else
        nom = [files(i).name(1:k(1)-1), ext];
    end
    if strcmp(nom, [nomFicSeul, ext])
        iFic = i;
        break
    end
end

if isempty(iFic)
    flag = 0;
    return
end

% DateFic = floor(files(i).datenum);
DateFic = files(i).datenum;
%}

if ischar(strDate)
    DateComparaison = datenum(strDate, 'dd/mm/yyyy');
else
    DateComparaison = datenum(strDate, varargin{:});
end
flag = DateFic > DateComparaison;
