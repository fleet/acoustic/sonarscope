% V�rifie si la date de modification d'un fichier (nomFic1) est post�rieure � un
% autre fichier (nomFic2)
%
% Syntax
%   flag = checkFilePosterior(nomFic1, nomFic2)
%
% Input Arguments
%   nomFic1 : Nom du fichier
%   nomFic2 : Nom du fichier
%
% Output Arguments
%   flag : 1 le fichier nomFic1 a �t� modifi� apr�s le fichier nomFic2
%          0 sinon
%
% Examples
%   nomFic1 = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   flag = checkFileCreation(nomFic, '26/09/2008')
%   flag = checkFileCreation(nomFic, 2008, 11, 25, 16, 21, 23)
%   flag = checkFileCreation(nomFic, 2008, 11, 25, 16, 21, 24)
%
% See also toto Authors
% Authors    : JMA
%-------------------------------------------------------------------------------

function flag = checkFilePosterior(nomFic1, nomFic2)

if ~exist(nomFic1, 'file') || ~exist(nomFic2, 'file')
    flag = 0;
    return
end

f1 = dir(nomFic1);
f2 = dir(nomFic2);
flag = (f1.datenum > f2.datenum);
