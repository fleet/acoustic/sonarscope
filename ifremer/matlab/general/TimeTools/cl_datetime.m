classdef cl_datetime
    % Description
    %   Component cl_datetime who is subclass of datetime
    %
    % Syntax
    %   a = cl_datetime(...)
    %
    % cl_datetime properties :
    %   TODO
    %
    % Output Arguments
    %   a : One cl_datetime instance
    %
    % Examples
    %   % Cas du date Julian de MatLab
    %   a  = cl_datetime(datenum('2017/05/01', 'yyyy/mm/dd'), 'TypeDate', 'TimeMat');
    %
    %   % Vecteur de temps :
    %   vt = datenum('2017/05/01', 'yyyy/mm/dd'):0.25:datenum('2017/05/01', 'yyyy/mm/dd')+1; 
    %   a  = cl_datetime('TimeMat', vt);
    %   
    %   % Cas du temps Posix
    %   nbSecSincePosix = 86400*(datenum('2017/05/01', 'yyyy/mm/dd')-datenum('1970/01/01', 'yyyy/mm/dd'));
    %   a = cl_datetime('TimeUnix', nbSecSincePosix);
    %
    % t = cl_datetime
    % date  = dayStr2Ifr('14/07/2000')
    % heure = (0:9) * 60000
    % t = cl_datetime('timeIfr', date, heure)
    % t = cl_datetime('timeIfr', repmat(date, size(heure)), heure)
    % heure = (0:9) * 20000000
    % t = cl_datetime('timeIfr', date, heure)
    % 
    % t = cl_datetime('timeMat', now+1.5)
    % t = cl_datetime('timeMat', (now:0.25:now+1))

    % Authors : GLU
    % ----------------------------------------------------------------------------
    
    properties
        dt % Objet de type datetime
    end
        
    
    methods
        function this = cl_datetime(varargin)                               
            OK = 1;
            switch nargin
                case 0
                    this.dt = datetime("now");
                case 2
                    if strcmpi(varargin{1}, 'timeMat')
                        if length(varargin) == 2
                            t = varargin{2};
                            if isnumeric(t)
                                this.dt = datetime(t, 'ConvertFrom', 'datenum');
                            else
                                OK = 0;
                            end
                        else
                            OK = 0;
                        end
                    elseif strcmpi(varargin{1}, 'timeUnix')
                        if length(varargin) == 2
                            t = varargin{2};
                            if isnumeric(t)
                                this.dt = datetime(t, 'ConvertFrom', 'posixtime');
                            else
                                OK = 0;
                            end
                        else
                            OK = 0;
                        end

                    else
                        OK = 0;
                    end
                case 3
                    if strcmp(varargin{1}, 'timeIfr')
                        if length(varargin) == 3
                            nbMilisUnJour = 24*3600*1000;
                            
                            date  = double(varargin{2});
                            heure = double(varargin{3});
                            
                            J     = floor(heure / nbMilisUnJour);
                            heure = heure - J * nbMilisUnJour;
                            date  = date + J;
                            
                            if length(date) == 1
                                this.dt  = timeIfr2Mat(ones(size(heure)) * date, heure);
                            else
                                if length(heure) == 1
                                    this.dt  = timeIfr2Mat(date, ones(size(date)) * heure);
                                elseif length(date) == length(heure)
                                    this.dt  = timeIfr2Mat(date, heure);
                                else
                                    my_warndlg('cl_time:cl_time Nombre d arguments ou taille de ceux ci incorrects', 1) ;
                                    this.dt  = [];
                                end
                            end
                        else
                            OK = 0;
                        end
                    else
                        OK = 0;
                    end
            end

            if OK == 0
                disp('Directive pas comprise')
                my_warndlg('cl_time:cl_time Beurk', 1) ;
                return
            end
        end
        %{
        function t2str(this)
            % Caduque car remplacé par 
            % this.dt;  
        end
        %}
         
        function date2str(this)
            % Caduque car remplacé par :
            % a.date2str
            datestr(this.dt, 'dd/mm/yyyy') 
        end
        
        function str = summary(this)
            str = [datestr(this.dt(1), 'yyyy-mm-dd HH:MM:SS.FFF') ' - ' datestr(this.dt(end), 'yyyy-mm-dd HH:MM:SS.FFF')];
        end
       
        function t2strIso8601(this)
            datestr(this.dt, 'yyyy-mm-ddTHH:MM:SS:FFF') 
        end
        
        function r = ge(this, b)
            r = (this.dt - b.dt) >= 0;
        end
        
        function this = transpose(this)
            this.dt = this.dt';
        end
    end
end