function dt = datetimeTransition(varargin)
% Description
%   Fonction temporaire pour remplacer cl_time. Les paramètres d'entrées
%   sont identiques à cl_time mais la variable retournée est un "datetime".
%   Cela permettra d'adapter toutes les utilisations de cl_time et lorsque
%   ce travail laborieux sera fait on pourra se pencher sur le passage
%   direct des paramètres d'entrée de la classe datetime
%
% Syntax
%   a = datetimeTransition(...)
%
% datetimeTransition properties :
%   TODO
%
% Output Arguments
%   a : One datetimeTransition instance
%
% Examples
%
%   % Cas du temps Posix
%   nbSecSincePosix = 86400*(datenum('2017/05/01', 'yyyy/mm/dd')-datenum('1970/01/01', 'yyyy/mm/dd'));
%   a = datetimeTransition('TimeUnix', nbSecSincePosix);
%
%   t = datetimeTransition
%   date  = dayStr2Ifr('14/07/2000')
%   heure = (0:9) * 60000
%   t = datetimeTransition('timeIfr', date, heure)
%   t = datetimeTransition('timeIfr', date(1), heure)
%
%   t = datetimeTransition('timeMat', now)
%   t = datetimeTransition('timeMat', (now:0.25:now+1))
%
%   tu = 86400*(datenum('2017/05/01', 'yyyy/mm/dd')-datenum('1970/01/01', 'yyyy/mm/dd'));
%   t = datetimeTransition('timeUnix', tu)
%
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : en cas de pb voir cl_datetime.m créé par Roger

OK = 1;
switch nargin
    case 0
        dt = datetime("now");
    case 2
        if strcmpi(varargin{1}, 'timeMat')
            if length(varargin) == 2
                t = varargin{2};
                if isnumeric(t)
                    dt = datetime(t, 'ConvertFrom', 'datenum');
                else
                    OK = 0;
                end
            else
                OK = 0;
            end
        elseif strcmpi(varargin{1}, 'timeUnix')
            if length(varargin) == 2
                t = varargin{2};
                if isnumeric(t)
                    dt = datetime(t, 'ConvertFrom', 'posixtime');
                else
                    OK = 0;
                end
            else
                OK = 0;
            end
            
        else
            OK = 0;
        end
    case 3
        if strcmp(varargin{1}, 'timeIfr')
            if length(varargin) == 3
                nbMilisUnJour = 24*3600*1000;
                date  = double(varargin{2});
                heure = double(varargin{3});
                J     = floor(heure / nbMilisUnJour);
                heure = heure - J * nbMilisUnJour;
                date  = date + J;
                
                if length(date) == 1
                    dt = timeIfr2Mat(ones(size(heure)) * date, heure);
                else
                    if length(heure) == 1
                        dt  = timeIfr2Mat(date, ones(size(date)) * heure);
                    elseif length(date) == length(heure)
                        dt  = timeIfr2Mat(date, heure);
                    else
                        pppp = dbstack;
                        my_warndlg(sprintf('%s:%s Nombre ''arguments ou taille de ceux-ci incorrects', mfilename, pppp(1).name), 1) ;
                        dt  = [];
                    end
                end
                dt = datetime(dt, 'ConvertFrom', 'datenum');
            else
                OK = 0;
            end
        else
            OK = 0;
        end
end

if OK == 0
    disp('Directive pas comprise')
    pppp = dbstack;
    my_warndlg(sprintf('%s:%s Beurk', mfilename, pppp(1).name), 1) ;
    return
end
