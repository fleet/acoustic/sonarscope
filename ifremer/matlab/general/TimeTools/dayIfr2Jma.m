% Conversion d'une date cod�e "Ifremer" en "Jour, Mois, Annee"
%
% Syntax
%   [Jour, Mois, Annee] = dayIfr2Jma(dIfr)
%
% Input Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Output Arguments
%   Jour  : C'est comme le
%   Mois  : Port Salut
%   Annee : c'est �crit dessus
%
% Examples
%   [Jour, Mois, Annee] = dayIfr2Jma(2451070)
%   [Jour, Mois, Annee] = dayIfr2Jma([2451070 2451071])
%   [Jour, Mois, Annee] = dayIfr2Jma([2451070; 2451071])
%
% See also dayJma2Ifr dayIfr2str dayStr2Ifr hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [Jour, Mois, Annee] = dayIfr2Jma(d)

j  = floor(d - 1721119);
in = floor(4*j - 1);
Annee = floor(in / 146097);

j  = in - 146097 * Annee;
Jour = floor(j / 4);
in = 4 * Jour + 3;

j  = floor(in / 1461);
Jour = floor(((in-1461*j)+4)/4);
in = 5 * Jour - 3;
Mois = floor(in / 153);
Jour = floor(((in-153*Mois)+5) / 5);
Annee = 100 * Annee + j;

subscript1 = find( Mois >= 10 );
subscript2 = find( Mois < 10 );

Mois(subscript1)  = Mois(subscript1)  - 9;
Annee(subscript1) = Annee(subscript1) + 1;
Mois(subscript2)  = Mois(subscript2)  + 3;
