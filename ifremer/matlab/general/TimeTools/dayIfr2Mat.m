% Conversion d'une date codee "Ifremer" en date codee "Matlab"
%
% Syntax
%   dMat = dayIfr2Mat( dIfr )
%
% Input Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Output Arguments
%	dMat : Nombre de jours depuis le 00-Jan-0000
%
% Examples
%   dayIfr2Mat(dayStr2Ifr('20/11/2001'))
%   dayIfr2Mat(2452234:2452240)
%
% See also dayMat2Ifr hourIfr2Mat hourMat2Ifr timeIfr2Mat timeMat2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function dMat = dayIfr2Mat(dIfr)
dMat = dIfr - 1721059;
