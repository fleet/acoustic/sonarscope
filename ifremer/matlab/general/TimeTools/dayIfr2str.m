% Conversion d'une date code� "Ifremer" en cha�ne de caract�res "JJ/MM/AAAA"
%
% Syntax
%   s = dayIfr2str(dIfr)
%
% Input Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Output Arguments
%   s  : Date en clair
%
% Examples
%   dayIfr2str(2451070)
%   dayIfr2str(2451070:2451080)
%
% See also dayIfr2strUK dayIfr2Jma dayJma2Ifr dayStr2Ifr hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function s = dayIfr2str(d)

if iscell(d)
    d = d{:};
end

[Jour, Mois, Annee] = dayIfr2Jma(round(d));

% Il n'y a en principe pas lieu d'utiliser de fonction de conversion en entier.
% Si cela avait ete necessaire, il aurait ete naturel d'utiliser la partie entiere (floor).
% Nous utilisons ici la fonction round (valeur entiere la plus proche) uniquement
% pour resoudre un probleme avec la fonction chgLabelsColorbar.m  lorqu'on visualise
% une navigation comportant 2 journees avec representation en couleur des jours.

s = cell(size(d));
for k=1:numel(d)
    s{k} = sprintf('%02d/%02d/%d', Jour(k), Mois(k), Annee(k));
end

if length(s) == 1
    s = s{1};
end
