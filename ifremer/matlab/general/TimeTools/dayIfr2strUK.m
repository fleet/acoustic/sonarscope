% Conversion d'une date codee "Ifremer" en chaine de caracteres "YYYY/MM/DD"
%
% Syntax
%   s = dayIfr2strUK( dIfr )
%
% Input Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Output Arguments
%   s  : Date en clair
%
% Examples
%   dayIfr2strUK(2451070)
%   dayIfr2strUK(2451070:2451080)
%
% See also dayIfr2Jma dayJma2Ifr dayStr2Ifr hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function s = dayIfr2strUK(d)

if iscell(d)
    d = d{:};
end

[Jour, Mois, Annee] = dayIfr2Jma(round(d));

% Il n'y a en principe pas lieu d'utiliser de fonction de conversion en entier.
% Si cela avait �t� n�cessaire, il aurait �t� naturel d'utiliser la partie enti�re (floor).
% Nous utilisons ici la fonction round (valeur enti�re la plus proche) uniquement
% pour resoudre un probleme avec la fonction chgLabelsColorbar.m  lorqu'on visualise
% une navigation comportant 2 journ�es avec representation en couleur des jours.

s = cell(size(d));
for k=1:numel(d)
    s{k} = sprintf('%04d/%02d/%02d', Annee(k), Mois(k), Jour(k));
end

if length(s) == 1
    s = s{1};
end
