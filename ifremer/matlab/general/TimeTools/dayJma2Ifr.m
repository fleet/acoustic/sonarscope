% Conversion d'une date codee "Jour, Mois, Annee" en date cod�e "Ifremer"
%
% Syntax
%   dIfr = dayJma2Ifr(Jour, Mois, Annee)
%
% Input Arguments
%   Jour	: Jour du mois
%   Mois	: Mois
%   Annee	: Annee
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Examples
%   dIfr = dayJma2Ifr(13, 9, 1998)
%   dIfr = dayJma2Ifr([13 14], [9 9], [1998 1998])
%
% See also dayYear2Ifr dayIfr2Jma dayIfr2str dayStr2Ifr hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function d = dayJma2Ifr(Jour, Mois, Annee)

subscript1 = (Mois > 2);
subscript2 = (Mois <= 2);

Mois(subscript1)  = Mois(subscript1) - 3;
Mois(subscript2)  = Mois(subscript2) + 9;
Annee(subscript2) = Annee(subscript2) - 1;

ks    = floor(Annee / 100);
Annee = Annee - 100 * ks;

d = floor(146097 * ks / 4) + floor(1461 * Annee / 4) + floor((153 * Mois + 2) / 5) + Jour + 1721119;
