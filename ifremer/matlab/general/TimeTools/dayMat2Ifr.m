% Conversion d'une date codee "Ifremer" en date codee "Matlab"
%
% Syntax
%   dIfr = dayMat2Ifr(dMat)
%
% Input Arguments
%	dMat : Nombre de jours depuis le 00-Jan-0000
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Examples
%   dayMat2Ifr(now)
%   dayMat2Ifr(now:(now+5))
%
% See also dayIfr2Mat hourIfr2Mat hourMat2Ifr timeIfr2Mat timeMat2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function dIfr = dayMat2Ifr(dMat)
dIfr = floor(dMat) + 1721059;
