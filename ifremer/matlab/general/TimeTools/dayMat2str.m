% Conversion d'une date codee "Matlab" en ASCII
%
% Syntax
%   s = dayMat2str( dMat)
%
% Input Arguments
%	dMat : Nombre de jours depuis le 00-Jan-0000
%
% Output Arguments
%	s : String au format HH:MM:SS.mmm ou xxxxJ-HH:MM:SS.mmm
% 
% Examples 
%   dayMat2str(now)
%   dayMat2str(now:0.25:(now+2))
%
% See also hourMat2str dayIfr2Mat dayMat2Ifr hourIfr2Mat hourMat2Ifr timeIfr2Mat timeMat2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function s = dayMat2str(dMat)

sd = dayIfr2str(dayMat2Ifr(dMat));

if iscell(sd)
    s = repmat('', length(sd), 10);
    for k=1:length(sd)
        s(k,:) = sd{k};
    end
else
    s = sd;
end
