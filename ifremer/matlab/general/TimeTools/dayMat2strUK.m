% Conversion d'une date codee "Matlab" en ASCII au format "YYYY/MM/DD"
%
% Syntax
%   s = dayMat2strUK( dMat)
%
% Input Arguments
%	dMat : Nombre de jours depuis le 00-Jan-0000
%
% Output Arguments
%	s : String au format HH:MM:SS.mmm ou xxxxJ-HH:MM:SS.mmm
% 
% Examples 
%   dayMat2strUK(now)
%   dayMat2strUK(now:0.25:(now+2))
%
% See also hourMat2str dayIfr2Mat dayMat2Ifr hourIfr2Mat hourMat2Ifr timeIfr2Mat timeMat2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function s = dayMat2strUK( dMat )

sd = dayIfr2strUK(dayMat2Ifr(dMat));

if iscell(sd)
    for k=length(sd):-1:1
        s(k,:) = sd{k};
    end
else
    s = sd;
end
