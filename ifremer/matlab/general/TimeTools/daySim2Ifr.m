% Conversion d'une date codee "Simrad" en date codee "Ifremer"
%
% Syntax
%   dIfr = daySim2Ifr(dSim)
%
% Input Arguments
%   dSim : Date au format SIMRAD (YYYYMMDD)
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Examples
%   dIfr = daySim2Ifr(19991102)
%   dayIfr2str(dIfr)
%
% See also dayJma2Ifr dayIfr2Jma dayIfr2str dayStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function dIfr = daySim2Ifr(dSim)

Year = floor(dSim / 10000);
dSim = dSim - Year * 10000;
Month = floor(dSim / 100);
Day = dSim - Month * 100;
dIfr = dayJma2Ifr(Day, Month, Year);
