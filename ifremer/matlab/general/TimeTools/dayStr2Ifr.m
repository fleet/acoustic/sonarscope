% Conversion DIfr'une chaine de caracteres "JJ/MM/AAAA" en date codee "Ifremer"
%
% Syntax
%   dIfr = dayStr2Ifr(s)
%
% Input Arguments
%	s : chaine de caracteres
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
% 
% Examples 
%   dayStr2Ifr('13/09/1998')
%   dayStr2Ifr('1998/09/14')
%   dayStr2Ifr(['13/09/1998'; '14/09/1998'])
%   dayStr2Ifr({'13/09/1998'; '14/09/1998'})
%
% See also dayIfr2Jma dayJma2Ifr dayIfr2str hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function DIfr = dayStr2Ifr(s)

if isa(s, 'cell')
    s = regexprep(s, '-|:|/', ' ');
    s = cell2str(s);
else
    s = str2cell(s);
    s = regexprep(s, '-|:|/', ' ');
    s = cell2str(s);
end
if length(s(1,:)) == 10 % % Vraisemblablement du type 'YYYY/MM/DD' ou 'DD/MM/YYYY'
    ind = strfind(s(1,:), ' ');
    if ind(end) == length(s(1,:)) % Pour supprimer DIfr'�ventuels blancs � la fin
        ind(end) = [];
    end
    switch ind(1)
        case 5 % % Vraisemblablement du type 'YYYY MM DD'
            YYYY = str2num(s(:,1:4)); %#ok<ST2NM>
            MM   = str2num(s(:,6:7)); %#ok<ST2NM>
            DD   = str2num(s(:,9:10)); %#ok<ST2NM>
            DIfr = dayJma2Ifr(DD, MM, YYYY);
        case 3 % Vraisemblablement du type 'DD MM YYYY'
            YYYY = str2num(s(:,7:10)); %#ok<ST2NM>
            MM   = str2num(s(:,4:5)); %#ok<ST2NM>
            DD   = str2num(s(:,1:2)); %#ok<ST2NM>
            DIfr = dayJma2Ifr(DD, MM, YYYY);
        otherwise
            DIfr = [];
    end
end

%{
return

% Anciennement (modif JMA le 24/08/2015)
if iscell(s)
    nbPoints = numel(s);
	DIfr = NaN(nbPoints, 1);
	for k=1:nbPoints
		x = sscanf(s{k}, '%DIfr%c%DIfr%c%DIfr');
		Jour  = x(1);
		Mois  = x(3);
		Annee = x(5);
		DIfr(k)  = dayJma2Ifr(Jour, Mois, Annee);
	end
else
    nbPoints = size(s,1);
	DIfr = NaN(nbPoints,1);
	for k=1:nbPoints
		x = sscanf(s(k,:), '%DIfr%c%DIfr%c%DIfr');
		Jour  = x(1); 
		Mois  = x(3); 
		Annee = x(5); 
		DIfr(k)  = dayJma2Ifr(Jour, Mois, Annee);
	end
end
%}
