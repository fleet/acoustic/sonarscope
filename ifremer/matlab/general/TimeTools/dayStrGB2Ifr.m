% Conversion d'une chaine de caracteres "YYY/MM/DD" en date codee "Ifremer"
%
% Syntax
%   dIfr = dayStrGB2Ifr( s )
%
% Input Arguments
%	s : chaine de caracteres
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
% 
% Examples 
%   dayStrGB2Ifr('1998/09/13')
%   dayStrGB2Ifr(['1998/09/13'; '1998/09/14'])
%
% See also dayIfr2Jma dayJma2Ifr dayIfr2str hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function d = dayStrGB2Ifr(s)

if iscell(s)
    N = numel(s);
    d = NaN(1,N);
    for k=1:N
        x = sscanf(s{k}, '%d%c%d%c%d');
        Annee = x(1);
        Mois  = x(3);
        Jour  = x(5);
        d(k)  = dayJma2Ifr(Jour, Mois, Annee);
    end
else
    d = NaN(size(s,1), 1);
    for k=1:size(s,1)
        x = sscanf(s(k,:), '%d%c%d%c%d');
        Annee = x(1);
        Mois  = x(3);
        Jour  = x(5);
        d(k)  = dayJma2Ifr(Jour, Mois, Annee);
    end
end
