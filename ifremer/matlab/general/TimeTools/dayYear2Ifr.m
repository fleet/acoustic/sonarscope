% Conversion d'une date d�crite par le num�ro du jour dans l'ann�e et de l'ann�e en date cod�e "Ifremer"
%
% Syntax
%   dIfr = dayYear2Ifr(Day, Annee)
%
% Input Arguments
%   Day   : Day of the year [1 to 365]
%   Annee : Annee
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Examples
%   dIfr = dayJma2Ifr(1, 1, 2016)
%   dIfr = dayYear2Ifr(1, 2016)
%   dIfr = dayJma2Ifr(31, 12, 2015)
%   dIfr = dayYear2Ifr(365, 2015)
%   dIfr = dayJma2Ifr(31, 12, 2016)
%   dIfr = dayYear2Ifr(365, 2016) % Ann�e bisextile : 366 jours :
%
% See also dayJma2Ifr dayIfr2Jma dayIfr2str dayStr2Ifr hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function d = dayYear2Ifr(Day, Annee)

Annee = double(Annee) - 1;
ks    = floor(Annee / 100);
Annee = Annee - 100 * ks;

% d = floor(146097 * ks / 4) + floor(1461 * Annee / 4) + floor((153 * 10 + 2) / 5) + 1 + 1721119;
% d = floor(146097 * ks / 4) + floor(1461 * Annee / 4) + floor((1532) / 5) + 1721119 + double(Day);
% d = floor(146097 * ks / 4) + floor(1461 * Annee / 4) + 306 + 1721119 + double(Day);
d = floor(146097 * ks / 4) + floor(1461 * Annee / 4) + 1721425 + double(Day);
