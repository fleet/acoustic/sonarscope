% Conversion d'un temps donnee en milisecondes en chaine de caracteres NbJours-HH:MM:SS.mmm
%
% Syntax
%   s = duration2str( x )
%   s = duration2str( x, format )
%	
% Input Arguments
%   x	   : matrice contenant les heures
% 
% Name-Value Pair Arguments 
%   format : Format : 1='J-HH:MM:SS.MMM' (implicite)   2='J-HH:MM:SS'  3='J-HH:MM'   4='J-HH'
%
% Output Arguments
%   s : chaine de carateres
%
% Examples 
%   duration2str( linspace(0,1e10, 5) )
%   duration2str( linspace(0,1e10, 5), 2 )
%   duration2str( linspace(0,1e10, 5), 3 )
%
% See also hourHms2Ifr hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
% VERSION  : $Id: duration2str.m,v 1.4 2004/02/10 10:03:25 augustin Exp $
%--------------------------------------------------------------------------------

function s = duration2str( x, varargin )

if iscell(x)
	x = x{:};
end

if nargin == 1
    format = 1;
else
    format = varargin{1};
end

nbMiliSecDans1Jour = 3600000*24;
nbJours = floor(x / nbMiliSecDans1Jour);

x = x - nbJours * nbMiliSecDans1Jour;
[Heure, Minute, Seconde] = hourIfr2Hms( x );

s = cell(size(Heure));
for i=1:numel(Heure)
    switch format
    case 1
        s{i} = sprintf('%d-%02d:%02d:%06.3f', nbJours(i), Heure(i), Minute(i), Seconde(i));
    case 2
        s{i} = sprintf('%d-%02d:%02d:%02d', nbJours(i), Heure(i), Minute(i), floor(Seconde(i)));
    case 3
        s{i} = sprintf('%d-%02d:%02d', nbJours(i), Heure(i), Minute(i));
    case 4
        s{i} = sprintf('%d-%02d', nbJours(i), Heure(i));
    end
end

if length(s) == 1
	s = s{1};
end
