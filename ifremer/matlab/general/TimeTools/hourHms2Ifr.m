% Conversion d'une heure donnee en Heure, minute, seconde en heure codee "Ifremer"
%
% Syntax
%   hIfr = hourHms2Ifr( Heure, Minute, Seconde )
%
% Input Arguments
%	Heure	: valeur entiere
%	Minute	: valeur entiere
%	Seconde	: valeur reelle avec les millisecondes
%
% Output Arguments
%	hIfr : Nombre de millisecondes depuis minuit
%
% Examples
%   hIfr = hourHms2Ifr(11, 2, 55.380)
%   hIfr = hourHms2Ifr([11 13], [1 11], [55.380 5.999])
%
% See also hourIfr2Hms hourIfr2str hourStr2Ifr dayIfr2Jma dayJma2Ifr dayIfr2str dayStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function h = hourHms2Ifr(Heure, Minute, Seconde)
h = 1000 * (Seconde + Minute * 60 + Heure * 3600);
