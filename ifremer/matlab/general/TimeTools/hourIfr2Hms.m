% Conversion d'une heure codee "Ifremer" en valeurs : Heure, Minute, Seconde
%
% Syntax
%   [Heure, Minute, Seconde] = hourIfr2Hms( hIfr )
%
% Input Arguments
%	hIfr : Nombre de millisecondes depuis minuit
%
% Output Arguments
%   Heure   : C'est comme le
%   Minute  : Port Salut
%   Seconde : c'est ecrit dessus
%
% Examples
%   [Heure, Minute, Seconde] = hourIfr2Hms( 0:1000000:10000000 )
%
% See also hourHms2Ifr hourIfr2str hourStr2Ifr dayIfr2Jma dayJma2Ifr dayIfr2str dayStr2Ifr Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [Heure, Minute, Seconde] = hourIfr2Hms(x)

Heure = floor(x) / 1000;

Seconde = mod(Heure, 60);
Heure = (Heure - Seconde) / 60;

Minute = mod(Heure, 60);
Heure = (Heure - Minute) / 60;
