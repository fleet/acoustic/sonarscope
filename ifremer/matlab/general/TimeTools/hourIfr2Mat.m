% Conversion d'une heure codee "Ifremer" en heure codee "Matlab"
%
% Syntax
%   hMat = hourIfr2Mat( hIfr )
%
% Input Arguments
%	hIfr : Nombre de millisecondes depuis minuit
%
% Output Arguments
%	hMat : Fraction d'une journee (0.5 -> 12H)
%
% Examples
%   hourIfr2Mat(6*3600*1000)
%   hourIfr2Mat([0:6:24]*3600*1000)
%
% See also dayMat2Ifr dayIfr2Mat hourMat2Ifr timeIfr2Mat timeMat2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function hMat = hourIfr2Mat(hIfr)
hMat = hIfr / 86400000;
