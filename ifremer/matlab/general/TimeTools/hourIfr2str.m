% Conversion d'une heure codee "Ifremer" en chaine de caracteres "HH:MM:SS.MMM"
%
% Syntax
%   s = hourIfr2str( hIfr )
%
% Input Arguments
%	hIfr : Nombre de millisecondes depuis minuit
%
% Output Arguments
%	s : chaine de caracteres "HH:MM:SS.MMM"
%
% Examples
%   hourIfr2str( 0:12345:100000 )
%
% See also hourIfr2Hms hourHms2Ifr hourStr2Ifr dayIfr2Jma dayJma2Ifr dayIfr2str dayStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function s = hourIfr2str(x)

if iscell(x)
    x = x{:};
end

[Heure, Minute, Seconde] = hourIfr2Hms(x);

s = cell(size(Heure));
for k=1:numel(Heure)
    s{k} = sprintf('%02d:%02d:%06.3f', Heure(k), Minute(k), Seconde(k));
end

if length(s) == 1
    s = s{1};
end
