% Conversion d'une heure codee "Matlab" en heure codee "Ifremer"
%
% Syntax
%   hIfr = hourMat2Ifr( hMat )
%
% Input Arguments
%	hMat : Fraction d'une journee (0.5 -> 12H)
%
% Output Arguments
%	hIfr : Nombre de millisecondes depuis minuit
%
% Examples
%   hourMat2Ifr(0.25)
%   hourMat2Ifr(0:0.25:1)
%
% See also dayMat2Ifr dayIfr2Mat hourIfr2mat timeIfr2Mat timeMat2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function hIfr = hourMat2Ifr( hMat )
hIfr = hMat * 86400000;
