% Conversion d'une heure codee "Matlab" en ASCII
%
% Syntax
%   s = hourMat2str( hMat)
%
% Input Arguments
%	hMat : fraction d'une journee (0.5 -> 12H)
%
% Output Arguments
%	s : String au format HH:MM:SS.mmm ou xxxxJ-HH:MM:SS.mmm
% 
% Examples 
%   hourMat2str(0.25)
%   hourMat2str(0:0.25:2)
%
% See also dayIfr2Mat dayMat2Ifr hourIfr2Mat hourMat2Ifr timeIfr2Mat timeMat2Ifr Authors
% Authors : JMA
% VERSION  : $Id: hourMat2str.m,v 1.3 2004/02/10 10:03:25 augustin Exp $
%--------------------------------------------------------------------------------

function s = hourMat2str( hMat )

signhMat = sign(hMat+eps);
hMat = abs(hMat);

hIfr = hourMat2Ifr(hMat);
nbJ = floor(hMat);
x = hIfr - nbJ*86400000;

Heure = floor(x) / 1000;
% milliseconde = floor(x) - ( Heure * 1000 );

Seconde = mod(Heure, 60);
Heure = ( Heure - Seconde ) / 60;

Minute = mod(Heure, 60);
Heure = ( Heure - Minute ) / 60;

sd = cell(size(Heure));

sub = find(signhMat == 1 & hMat < 1);
for i=1:length(sub)
    sd{sub(i)} = sprintf('%02d:%02d:%06.3f', Heure(sub(i)), Minute(sub(i)), Seconde(sub(i)));
end

sub = find(signhMat == -1 & hMat < 1);
for i=1:length(sub)
    sd{sub(i)} = sprintf('%02d:%02d:%06.3f', -Heure(sub(i)), Minute(sub(i)), Seconde(sub(i)));
end

sub = find(signhMat == 1 & hMat >= 1);
for i=1:length(sub)
    sd{sub(i)} = sprintf('%dJ/%02d:%02d:%06.3f', nbJ(sub(i)), Heure(sub(i)), Minute(sub(i)), Seconde(sub(i)));
end

sub = find(signhMat == -1 & hMat >= 1);
for i=1:length(sub)
    sd{sub(i)} = sprintf('-%dJ/%02d:%02d:%06.3f', nbJ(sub(i)), Heure(sub(i)), Minute(sub(i)), Seconde(sub(i)));
end

% -----------------------

for i=1:length(sd)
    n = length(sd{i});
    s(i,1:n) = sd{i};
end
