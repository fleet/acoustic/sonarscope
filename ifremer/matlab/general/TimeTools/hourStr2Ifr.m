% Conversion d'une chaine de caracteres "HH:MM:SS.MMM" en heure codee "Ifremer"
%
% Syntax
%   hIfr = hourStr2Ifr(s)
%
% Input Arguments
%	s : chaine de caracteres "HH:MM:SS.MMM"
%
% Output Arguments
%	hIfr : Nombre de millisecondes depuis minuit
% 
% Examples 
%   hourStr2Ifr('11:01:55.380')
%   hourStr2Ifr('11:01:55')
%   hourStr2Ifr('11:01')
%   hourStr2Ifr(['11:01:55.380'; '13:11:05.999'])
%   hourStr2Ifr({'11:01:55.380'; '13:11:05.999'})
%
% See also hourIfr2Hms hourHms2Ifr hourIfr2str dayIfr2Jma dayJma2Ifr dayIfr2str dayStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function hIfr = hourStr2Ifr(s)

if isa(s, 'cell')
    s = regexprep(s, '-|:|/', ' ');
    s = cell2str(s);
else
    s = str2cell(s);
    s = regexprep(s, '-|:|/', ' ');
    s = cell2str(s);
end

s = strtrim(s);

ind = strfind(s(1,:), ' ');
if ind(end) == length(s(1,:)) % Pour supprimer d'�ventuels blancs � la fin
    ind(end) = [];
end
switch length(ind)
    case 2 % Vraisemblablement du type 'HH MM SS' ou 'HH MM SS.SSS'
        Heure   = str2num(s(:,1:2)); %#ok<ST2NM>
        Minute  = str2num(s(:,4:5)); %#ok<ST2NM>
        Seconde = str2num(s(:,7:end)); %#ok<ST2NM>
        hIfr = hourHms2Ifr(Heure, Minute, Seconde);
    case 1 % Vraisemblablement du type 'HH MM'
        Heure   = str2num(s(:,1:2)); %#ok<ST2NM>
        Minute  = str2num(s(:,4:5)); %#ok<ST2NM>
        hIfr = hourHms2Ifr(Heure, Minute, 0);
    otherwise
        hIfr = [];
end
   
%{
return               
% Anciennement (modif JMA le 24/08/2015)
if iscell(s)
    nbPoints = numel(s);
    hIfr = NaN(nbPoints, 1);
    for k=1:nbPoints
        x = sscanf(s{k}, '%d%c%d%c%f');
        Heure = x(1);
        Minute = x(3);
        Seconde = x(5);
        hIfr(k) = hourHms2Ifr(Heure, Minute, Seconde);
    end
else
    nbPoints = size(s,1);
    hIfr = NaN(nbPoints, 1);
    for k=1:nbPoints
        x = sscanf(s(k,:), '%d%c%d%c%f');
        Heure   = x(1);
        Minute  = x(3);
        Seconde = x(5);
        hIfr(k) = hourHms2Ifr(Heure, Minute, Seconde);
    end
end
%}
