function Datetime = str2datetime(str)

try
    Datetime = datetime(str, 'InputFormat', 'yyyy-MM-dd''T''HH:mm:ss.SSS''Z''');
catch
    Datetime = datetime(str, 'InputFormat', 'yyyy-MM-dd HH:mm:ss.SSS');
end
