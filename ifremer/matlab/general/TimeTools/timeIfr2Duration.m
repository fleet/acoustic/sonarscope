% Duree entre 2 temps (T1-T2) donnes au format "Ifremer"
%
% Syntax
%   nbMilisecondes = timeIfr2Duration( [d1Ifr h1Ifr], [d1Ifr h1Ifr])
%
% Input Arguments
%	d1Ifr : Nombre de jours depuis le 24/11/-4713
%	h1Ifr : Nombre de millisecondes depuis minuit
%	d2Ifr : Nombre de jours depuis le 24/11/-4713
%	h2Ifr : Nombre de millisecondes depuis minuit
%
% Output Arguments
%	nbMilisecondes : T1-T2 en millisecondes
%
% Examples
%    nbMilisecondes = timeIfr2Duration([dayStr2Ifr( '13/09/1998' ), 1000], [dayStr2Ifr( '12/09/1998' ), hourStr2Ifr( '23:59:59' )])
%    nbMilisecondes = timeIfr2Duration([2451070, 39715380], [2451071, 39715000])
%
% See also dayIfr2str dayIfr2Jma dayStr2Ifr dayJma2Ifr t2Ifrstr t2Ifrnum hourStr2Ifr hourHms2Ifr
% Authors : JMA
%--------------------------------------------------------------------------------

function nbMilisecondes = timeIfr2Duration(t1Ifr, t2Ifr)
nbMilisecondes = (t1Ifr(:,1) - t2Ifr(:,1)) * 86400000 + t1Ifr(:,2) - t2Ifr(:,2);
