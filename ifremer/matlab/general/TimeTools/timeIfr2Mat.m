% Conversion d'un temps code "Ifremer" en temps code "Matlab"
%
% Syntax
%   tMat = timeIfr2Mat(dIfr, hIfr)
%
% Input Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%	hIfr : Nombre de millisecondes depuis minuit
%
% Output Arguments
%	tMat : Temps au mormat matlab (Nombre de jours depuis le 00-Jan-0000 + Fraction d'une journee
%
% Examples
%   format long
%   timeIfr2Mat(2452234, 6*3600*1000)
%   timeIfr2Mat(2452234, [0:6:24]*3600*1000)
%   timeIfr2Mat(2452234:2452238, 6*3600*1000)
%   timeIfr2Mat(2452234:2452238, [0:6:24]*3600*1000)
%
% See also dayMat2Ifr dayIfr2Mat hourMat2Ifr timeMat2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function tMat = timeIfr2Mat(dIfr, hIfr)
tMat = (double(dIfr) - 1721059) + (double(hIfr) / 86400000);
