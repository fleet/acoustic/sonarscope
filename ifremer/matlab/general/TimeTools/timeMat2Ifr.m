% Conversion d'un temps code "Matlab" en temps code "Ifremer"
%
% Syntax
%   [dIfr, hIfr] = timeMat2Ifr( tMat)
%
% Input Arguments
%	tMat : Temps au mormat matlab (Nombre de jours depuis le 00-Jan-0000 + Fraction d'une journee
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%	hIfr : Nombre de millisecondes depuis minuit
%
% Examples
%   [dIfr, hIfr] = timeMat2Ifr(now)
%   [dIfr, hIfr] = timeMat2Ifr(now:0.25:(now+2))
%
% See also dayIfr2Mat dayMat2Ifr hourMat2Ifr hourIfr2Mat timeIfr2Mat Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function [dIfr, hIfr] = timeMat2Ifr(t)

dMat = floor(t);
hIfr = (t - dMat) * 86400000;
dIfr = dMat + 1721059;