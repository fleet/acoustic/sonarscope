% Conversion d'une date matlab en une serie temporelle contenue dans une instance "cl_time"
%
% Syntax
%   t = timeMat2cl_time(tmatlab)
%
% Input Arguments
%   tmatlab : Serie temporelle au format matlab.
%
% Output Arguments
%   t : Serie temporelle a trier (Instance de type cl_time)
%
% Examples
%   T = now
%   t2 = timeMat2cl_time(T)
%   t2str(t2)
%
% See also cl_time/t2matlab Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function t = timeMat2cl_time(tmatlab)

date  = floor(tmatlab)  + 1721059;
heure = rem(tmatlab, 1) * 86400000;
t = cl_time('timeIfr', date, heure);