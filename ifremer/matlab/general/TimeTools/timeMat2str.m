% Conversion d'un temps code "Matlab" en ASCII
%
% Syntax
%   s = timeMat2str(tMat)
%
% Input Arguments
%	tMat : Temps au mormat matlab (Nombre de jours depuis le 00-Jan-0000 + Fraction d'une journee
%
% Output Arguments
%	s : String au format JJ/MM/AAAA  HH:MM:SS.mmm
% 
% Examples 
%   timeMat2str(now)
%   timeMat2str(now:0.25:(now+2))
%
% See also hourMat2str dayMat2str Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function s = timeMat2str(tMat)

[dIfr, hIfr] = timeMat2Ifr(tMat);
sd = dayIfr2str(dIfr);
sh = hourIfr2str(hIfr);

if iscell(sd)
    for k=1:length(sd)
        pppp = [sd{k} '  ' sh{k}];
        s(k,1:length(pppp)) = pppp; %#ok
    end
else
    s = [sd '  ' sh];
end
