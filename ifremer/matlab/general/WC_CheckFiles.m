% TODO : a priori plus utile

function flag = WC_CheckFiles(nomFicXml)

if ischar(nomFicXml)
    nomFicXml = {nomFicXml};
end

N = length(nomFicXml);
str1 = 'V�rification des fichiers des fichiers XML';
str2 = 'Checking XML files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, Data] = XMLBinUtils.readGrpData(nomFicXml{k});
    if ~flag
        continue
    end
    
    if isfield(Data, 'Software') && strcmp(Data.Software, 'SonarScope')
        flag = checkImagesWC(Data, nomFicXml{k});
        if ~flag
            my_close(hw)
            return
        end
        
    elseif isfield(Data, 'Signature1') && strcmp(Data.Signature1, 'SonarScope') ...
            && isfield(Data, 'Signature2') && strcmp(Data.Signature2, 'WaterColumnCube3D')
        flag = checkImagesMatrix(Data, nomFicXml{k});
        if ~flag
            my_close(hw)
            return
        end
        
    else
        if ~isfield(Data, 'Signature1') || ~strcmp(Data.Signature1, 'SonarScope') || ~isfield(Data, 'Signature2')
            str1 = sprintf('Le fichier "%s" n''est pas un fichier pour 3DViewer.', nomFicXml{k});
            str2 = sprintf('"%s" is not a 3DViewer''one.', nomFicXml{k});
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'XMLSignature1NonValide');
            continue
        end
        
        switch Data.Signature2
            case 'ImagesAlongNavigation'
                flag = checkImagesAlongNavigation(Data, nomFicXml{k});
                if ~flag
                    my_close(hw)
                    return
                end
            otherwise
                str1 = sprintf('Le type de donn�es "%s" n''est pas encore pr�vu dans la fonction "WC_CheckFiles". Cas rencontr� pour le fichier %s".', Data.Signature2, nomFicXml{k});
                str2 = sprintf('Data Type "%s" is not set in "WC_CheckFiles". Encountered for %s".', Data.Signature2, nomFicXml{k});
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'XMLSignature1NonValide');
                continue
        end
    end
end
my_close(hw, 'MsgEnd')


function flag = checkImagesAlongNavigation(Data, nomFicXml)

persistent W1 W2

flag = 1;

%% Signaux

fig1 = FigUtils.createSScFigure('Name', Data.Name);
ha(1)  = subplot(6,2,1); plotNaN(Data.Date,            'Title', 'Date');            grid on; axis tight;
ha(2)  = subplot(5,2,3); plotNaN(Data.Hour,            'Title', 'Hour');            grid on; axis tight;
ha(3)  = subplot(5,2,5); plotNaN(Data.PingNumber,      'Title', 'PingNumber');      grid on; axis tight;
ha(4)  = subplot(5,2,7); plotNaN(Data.Tide,            'Title', 'Tide');            grid on; axis tight;
ha(5)  = subplot(5,2,9); plotNaN(Data.Heave,           'Title', 'Heave');           grid on; axis tight;
ha(6)  = subplot(5,2,2); plotNaN(Data.LongitudeTop,    'Title', 'LongitudeTop');    grid on; axis tight;
ha(7)  = subplot(5,2,4); plotNaN(Data.LatitudeTop,     'Title', 'LatitudeTop');     grid on; axis tight;
ha(8)  = subplot(5,2,6); plotNaN(Data.LongitudeBottom, 'Title', 'LongitudeBottom'); grid on; axis tight;
ha(9)  = subplot(5,2,8); plotNaN(Data.LatitudeBottom,  'Title', 'LatitudeBottom');  grid on; axis tight;
ha(10) = subplot(5,2,10); plotNaN(Data.DepthTop); grid on; hold on;
plotNaN(Data.DepthBottom,  'Title', 'DepthTop & DepthBottom'); axis tight; axis tight;
linkaxes(ha, 'x')
if ~isempty(W1)
    set(fig1, 'Position', W1)
end

%% Images

[nomDir, nomFic] = fileparts(nomFicXml);
for ks=1:Data.Dimensions.nbSlices
    nomPNG = fullfile(nomDir, nomFic, '000', [num2str(ks,'%05d') '.png']);
    NomSlice = Data.SliceName{ks};
    if exist(nomPNG, 'file')
        [I,map] = imread(nomPNG);
        
        fig2 = FigUtils.createSScFigure('Name', [num2str(ks) ' - ' Data.Name]);
        [nlI,ncI] = size(I);
        
        nc = Data.Dimensions.nbPings;
        nl = Data.Dimensions.nbRows;
        
        I = I(1:nl,1:nc);
        
        x = repmat(1:nc, nl, 1);
        X = zeros(nl,nc);
        Y = zeros(nl,nc);
        Z = zeros(nl,nc);
        for kc=1:nc
            He = Data.Heave(kc);
            X(:,kc) = linspace(Data.LongitudeBottom(ks,kc), Data.LongitudeTop(ks,kc), nl);
            Y(:,kc) = linspace(Data.LatitudeBottom(ks,kc),  Data.LatitudeTop(ks,kc),  nl);
            Z(:,kc) = linspace(Data.DepthBottom(ks,kc)+He, Data.DepthTop(ks,kc)+He, nl);
        end
        subplot(1,2,1); pcolor(x, Z, double(flipud(I)));  colormap(map); shading flat; title(rmblank(NomSlice), 'Interpreter', 'None')
        subplot(1,2,2); surf(X, Y, Z, double(flipud(I))); colormap(map); shading flat;
        if ~isempty(W2)
            set(fig2, 'Position', W2)
        end
        
        if isempty(W2)
            pause(10)
        end
        
        % Message si la taille de l'image est diff�rente de ce qui est attendu
        
        if (nl ~= nlI) || (nc ~= ncI)
            str1 = sprintf('Incompatibilit� de taille de l''image et XML pour "%s"', nomPNG);
            str2 = sprintf('XML info and image size are different for "%s"', nomPNG);
            my_warndlg(Lang(str1,str2), 1);
        end
        
        % On continue ?
        
        str1 = 'Voulez-vous poursuivre le contr�le des donn�es WC ?';
        str2 = 'Do you want to continue to check the WC data ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag || (rep == 2)
            %             delete(fig1)
            %             delete(fig2)
            flag = 0;
            return
        end
        
        if ishandle(fig2)
            W2 = get(fig2, 'Position');
            delete(fig2)
        end
    end
end
if ishandle(fig1)
    W1 = get(fig1, 'Position');
    delete(fig1)
end

%{
FormatVersion: 20100624
Dimensions: [1x1 struct]
SliceExists: [6x1 single]
AcrossDistance: [6x1 single]
SliceName: {6x1 cell}
%}

function h = plotNaN(varargin)

[varargin, Title] = getPropertyValue(varargin, 'Title', []);

if length(varargin) == 1
    y = varargin{1};
    %     x = 1 : size(y,2);
    x = 1:length(y);
else
    x = varargin{1};
    y = varargin{2};
end

h = plot(x', y'); grid on; hold on
N = size(y,1);
for k=1:N
    subNonNaN = ~isnan(y(k,:));
    subIsoles = (~imerode(subNonNaN, [1 1 1]) & subNonNaN);
    subIsoles = find(subIsoles);
    if ~isempty(subIsoles)
        valIsoles = NaN(1, size(y,2));
        valIsoles(subIsoles) =  y(k,subIsoles);
        
        if size(x,1) == 1
            xx = x;
        else
            xx = x(k,:);
        end
        h(end+1) = plot(double(xx), double(valIsoles), '.'); %#ok<AGROW>
    end
end
if ~isempty(Title)
    title(Title, 'Interpreter', 'None')
end




function flag = checkImagesWC(Data, nomFicXml)

persistent W1 W2

flag = 1;

%% Signaux

fig1 = FigUtils.createSScFigure('Name', Data.Name);
ha(1) = subplot(6,2,1); plotNaN(Data.Date,         'Title', 'Date');       grid on; axis tight;
ha(2) = subplot(5,2,3); plotNaN(Data.Hour,         'Title', 'Hour');       grid on; axis tight;
ha(3) = subplot(5,2,5); plotNaN(Data.iPing,        'Title', 'iPing');      grid on; axis tight;
ha(4) = subplot(5,2,7); plotNaN(Data.Tide,         'Title', 'Tide');       grid on; axis tight;
ha(5) = subplot(5,2,9); plotNaN(Data.Depth,        'Title', 'Depth');      grid on; axis tight;
ha(6) = subplot(5,2,2); plotNaN(Data.Immersion, 'Title', 'Immersion'); grid on; axis tight;
ha(7) = subplot(5,2,4); plotNaN(Data.latCentre,  'Title', 'latCentre');  grid on; axis tight;
ha(8) = subplot(5,2,6); plotNaN(Data.lonCentre, 'Title', 'lonCentre'); grid on; axis tight;

ha(9) = subplot(5,2,8); plotNaN(Data.xBab,  'Title', 'xBab');  grid on; axis tight;
ha(10) = subplot(5,2,10); plotNaN(Data.xTri,  'Title', 'xTri'); grid on; axis tight; axis tight;
linkaxes(ha, 'x')
if ~isempty(W1)
    set(fig1, 'Position', W1)
end

%{
lonBab: [1096�1 double]
latBab: [1096�1 double]
lonTri: [1096�1 double]
latTri: [1096�1 double]
lonBabDepointe: [1096�1 double]
latBabDepointe: [1096�1 double]
lonTriDepointe: [1096�1 double]
latTriDepointe: [1096�1 double]
%}

%% Images

W2 = [];
FigUtils.createSScFigure('Name', Data.Name);
[nomDir, nomFic] = fileparts(nomFicXml);
for ks=1:Data.Dimensions.nbPings
    nomPNG = fullfile(nomDir, nomFic, '000', [num2str(ks,'%05d') '.png']);
    %     NomSlice = Data.SliceName{ks};
    if exist(nomPNG, 'file')
        [I,map] = imread(nomPNG);
        
        x = [Data.xBab(ks) Data.xTri(ks)];
        z = [Data.Depth(ks) 0] + Data.Immersion(ks);
        
        image(x, z, flipud(I)); colormap(map); axis xy; axis tight; axis square;
        xlabel('Across distance (m)')
        ylabel('z (m)')
        title([num2str(ks,'%05d') ' / ' num2str(Data.Dimensions.nbPings,'%05d')])
        
        pause(0.2)
        drawnow
    end
end
% if ishandle(fig1)
%     W1 = get(fig1, 'Position');
%     delete(fig1)
% end





function flag = checkImagesMatrix(Data, nomFicXml)

persistent W1 W2

flag = 1;

%% Signaux

fig1 = FigUtils.createSScFigure('Name', Data.Name);
ha(1) = subplot(6,2,1); plotNaN(Data.Date,      'Title', 'Date');      grid on; axis tight;
ha(2) = subplot(5,2,3); plotNaN(Data.Hour,      'Title', 'Hour');      grid on; axis tight;
ha(3) = subplot(5,2,5); plotNaN(Data.iPing,     'Title', 'iPing');     grid on; axis tight;
ha(4) = subplot(5,2,7); plotNaN(Data.Tide,      'Title', 'Tide');      grid on; axis tight;
ha(5) = subplot(5,2,9); plotNaN(Data.Depth,     'Title', 'Depth');     grid on; axis tight;
ha(6) = subplot(5,2,2); plotNaN(Data.Immersion, 'Title', 'Immersion'); grid on; axis tight;
ha(7) = subplot(5,2,4); plotNaN(Data.latCentre, 'Title', 'latCentre'); grid on; axis tight;
ha(8) = subplot(5,2,6); plotNaN(Data.lonCentre, 'Title', 'lonCentre'); grid on; axis tight;

ha(9) =  subplot(5,2,8);  plotNaN(Data.xBab, 'Title', 'xBab'); grid on; axis tight;
ha(10) = subplot(5,2,10); plotNaN(Data.xTri, 'Title', 'xTri'); grid on; axis tight; axis tight;
linkaxes(ha, 'x')
if ~isempty(W1)
    set(fig1, 'Position', W1)
end

%{
lonBab: [1096�1 double]
latBab: [1096�1 double]
lonTri: [1096�1 double]
latTri: [1096�1 double]
lonBabDepointe: [1096�1 double]
latBabDepointe: [1096�1 double]
lonTriDepointe: [1096�1 double]
latTriDepointe: [1096�1 double]
%}

%% Images

W2 = [];
FigUtils.createSScFigure('Name', Data.Name);
% [nomDir, nomFic] = fileparts(nomFicXml);

for ks=1:Data.Dimensions.nbPings
    I = Data.Reflectivity(:,:,ks);
    
    x = [Data.xBab(ks) Data.xTri(ks)];
    z = [Data.Depth(ks) 0] + Data.Immersion(ks);
    
    imagesc(x, z, I); colormap(jet(256)); axis xy; axis tight; axis equal;
    xlabel('Across distance (m)')
    ylabel('z (m)')
    title([num2str(ks,'%05d') ' / ' num2str(Data.Dimensions.nbPings,'%05d')])
    
    pause(0.2)
    drawnow
end
% if ishandle(fig1)
%     W1 = get(fig1, 'Position');
%     delete(fig1)
% end
