function about_Sonarscope(varargin)

V = VersionsMatlab;
switch V
    case 'R2017b'
        MCR = 'MCR 9.3';
    case 'R2018b'
        MCR = 'MCR 9.5';
    case 'R2019a'
        MCR = 'MCR 9.6';
    case 'R2019b'
        MCR = 'MCR 9.7';
    otherwise
        my_warndlg('Message for support@ifremer.fr : function "about_Sonarscope" to be completed.', 1);
        MCR = 'MCR ???';
end

str{1} = sprintf('SonarScope version based on Matlab %s / %s', V, MCR);
str{end+1} = ' ';
str{end+1} = 'Web site : http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/Logiciels-embarques/SonarScope';
str{end+1} = ' ';
str{end+1} = 'Author : Jean-Marie Augustin';
str{end+1} = 'DFO-NSE-ASTI';
str{end+1} = 'IFREMER - France';
str{end+1} = 'Jean.Marie.Augustin@ifremer.fr';
str{end+1} = ' ';
str{end+1} = 'Assistance : sonarscope@ifremer.fr';
str{end+1} = ' ';
str{end+1} = 'Note : Lot of GUIs are based on GUI Layout Toolbox :';
str{end+1} = 'David Sampson (2019). GUI Layout Toolbox (https://www.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox), MATLAB Central File Exchange. Retrieved September 23, 2019. ';

my_warndlg(cell2str(str), 1);
