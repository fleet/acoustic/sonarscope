% TODO 
%
% Syntax
%   [y, pas, ymin, ymax] = centrage_magnetique(x)
%
% Input Arguments
%   x : 
%
% Output Arguments
%   y    : 
%   pas  : 
%   ymin : 
%   ymax : 
%
% Examples
%   x1 = -pi:2*pi
%   x2 = centrage_magnetique(x1)
%   x1-x2
%   x1 = -3:3
%   x2 = centrage_magnetique(x1)
%   x1-x2
%   x1 = linspace(-15,15,256)
%   x2 = centrage_magnetique(x1)
%   x1-x2
%   unique(diff(x1-x2))
%
% See also cl_image/sqrt Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [y, pas, ymin, ymax] = centrage_magnetique(x)

x = double(x);
switch length(x)
    case 0
        y = [];
        pas  = NaN;
        ymin = NaN;
        ymax = NaN;
    case 1
        y    = x;
        pas  = 1;
        ymin = y;
        ymax = y;
    otherwise
        y = x;
        resolution = median(diff(y), 'omitnan');
        N_Entiers = round(y/resolution);
        y = N_Entiers * resolution;
        
        % TODO : pourquoi avais-je mis cet Offset ? Attention, sa
        % suppression peut avoir un effet de bord sur beaucoup de choses !!!
%         offset = mean(y - x); % Comment� par JMA le 04/03/2014 pour pb BathyElli 
%         y = x + offset;       % Comment� par JMA le 04/03/2014 pour pb BathyElli 
        
        % figure; plot(x, '+b'); grid on; hold on; plot(y, 'xr')
        
        pas =  resolution;
        ymin = y(1);
        ymax = y(end);
end