function flag = checkFileName(nomFic)

flag = ~contains(nomFic, '&');
if ~flag
    str1 = sprintf('ATTENTION : le fichier "%s" comporte le caractère "&" qui est incompatible avec la technologie XML qui est utilisée dans SonarScope. Renommez le répertoire ou les fichiers et recommencez le traitement.', nomFic);
    str2 = sprintf('WARNING : Character "&" is present in "%s".\n\nThat is incompatible with the XML technologie which is used inside SonarScope. Please rename the directory or the file name and try again.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[nomDir1, nomFic1] = fileparts(nomFic);
nomFic2 = fullfile(nomDir1, 'SonarScope', nomFic1);
N = length(nomFic2);
if N > 200
    str1 = sprintf('ATTENTION : la longueur du nom du fichier "%s" semble excessive, cela peut poser problème pour les commandes DOS, je vous conseille de raccourcir le chemin ou de renommer les répertoires intermédiaires.', nomFic);
    str2 = sprintf('WARNING : the length of file "%s" could generate bugs for DOS commands. I heavy recommand that you shorten the name or the intermediate folders.', nomFic);
    my_warndlg(Lang(str1,str2), 0);
    flag = 0;
    return
end