% flag = checkIfOneElement(1)
% flag = checkIfOneElement(2)

function flag = checkIfOneElement(n)

switch n
    case 0
        flag = 0;
        str1 = 'Un �l�ment SVP.';
        str2 = 'One element please.';
        my_warndlg(Lang(str1,str2), 1);
    case 1
        flag = 1;
    otherwise
        flag = 0;
        str1 = 'Un seul �l�ment � la fois SVP.';
        str2 = 'Only one element please.';
        my_warndlg(Lang(str1,str2), 1);
end
