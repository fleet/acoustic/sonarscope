% Description
%   Check if N equal to one.
%
% Syntax
%   flag = checkIfOnlyOneInstance(N)
%
% Input Arguments
%   N : Cell array
%
% Output Arguments
%   flag : trie is empty, false otherwise
%
% More About : This function is principaly used in the methods that
% accept only one instance (not an array)
%
% Examples
%   flag = checkIfOnlyOneInstance(1)
%   flag = checkIfOnlyOneInstance(2)
%
% Authors : JMA
% See Also checkIfVararginIsEmpty Authors
%
% Reference pages in Help browser
%   <a href="matlab:doc checkIfOnlyOneInstance">checkIfOnlyOneInstance</a>
%-------------------------------------------------------------------------------

function flag = checkIfOnlyOneInstance(N)

if N == 1
    flag = true;
else
    s = dbstack;
    FunctionName = s(end).name;
    str1 = sprintf('"%s" ne peut traiter qu''une instance � la fois (%d ici).', FunctionName, N);
    str2 = sprintf('"%s" can process only one instance at a time (%d here).',   FunctionName, N);
    my_warndlg(Lang(str1,str2), 1);
    flag = false;
end