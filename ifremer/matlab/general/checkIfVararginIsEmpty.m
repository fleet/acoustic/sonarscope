% Description
%   Check if VarList is empty. If not, an error message is displayed to the developper
%
% Syntax
%   flag = checkIfVararginIsEmpty(PropertyName)
%
% Input Arguments
%   PropertyName : Cell array
%
% Output Arguments
%   flag : trie is empty, false otherwise
%
% More About : This function is principaly used in the functions that
% accept "Name-Value Pair Arguments" and "Name-only Arguments" to check if
% all the variables transmitted in varargin have been correctly
% interpretated
%
% Examples
%   List{1} = 'Foo';
%   flag = checkIfVararginIsEmpty(List)
%   List(1) = [];
%   flag = checkIfVararginIsEmpty(List)
%
% Authors : JMA
% See Also checkIfOnlyOneInstance Authors
%
% Reference pages in Help browser
%   <a href="matlab:doc checkIfVararginIsEmpty">checkIfVararginIsEmpty</a>
%-------------------------------------------------------------------------------

function flag = checkIfVararginIsEmpty(PropertyName)

if isempty(PropertyName)
    flag = false;
else
    s = dbstack;
    FunctionName = s(end).name;
    str1 = sprintf('L''argument "%s" n''est pas interpreté dans "%s".', PropertyName{1}, FunctionName);
    str2 = sprintf('The argument "%s" is not expected in "%s".', PropertyName{1}, FunctionName);
    my_warndlg(Lang(str1,str2), 1);
    flag = true;
end