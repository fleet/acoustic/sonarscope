% Controle de positionnement et d'existence des r�pertoires li�s aux
% variables d'environnement.
%
% Syntax
%   flag = checkInstallation
%
% Input Arguments
%   /
%
% Output Arguments
%   flag : 1 ou 0 selon l'existence de certaines variables.
%
% Examples
%   flag = checkInstallation
%
% See also SonarScope Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = checkInstallation

global IfrTbxResources %#ok<GVMIS>
global SonarScopeDocumentation %#ok<GVMIS>
global SonarScopeData %#ok<GVMIS>

OK   = 1;
flag = 1;

if isempty(IfrTbxResources)
    str1 = 'La variable "IfrTbxResources" n''est pas d�finie, v�rifiez l''installation.';
    str2 = '"IfrTbxResources" is not defined, check the installation.';
    my_warndlg(Lang(str1,str2), 0);
    flag = 0;
    return
end

if isempty(SonarScopeDocumentation)
    str1 = 'La variable "SonarScopeDocumentation" n''est pas d�finie, v�rifiez l''installation de la documentation.';
    str2 = '"SonarScopeDocumentation" is not defined, check the installation of the documentation.';
    my_warndlg(Lang(str1,str2), 0);
    OK = 0;
end

if isempty(SonarScopeData)
    str1 = 'La variable "SonarScopeData" n''est pas d�finie, v�rifiez l''installation des donn�es d''exemple.';
    str2 = '"SonarScopeData" is not defined, check the installation of the SonarScopeData.';
    my_warndlg(Lang(str1,str2), 0);
    OK = 0;
end

if OK
    if ~exist(IfrTbxResources, 'dir')
        str1 = sprintf('Le r�pertoire "%s" n''existe pas, v�rifiez l''installation.', IfrTbxResources);
        str2 = sprintf('The directory "%s" does not exist, check the installation.', IfrTbxResources);
        my_warndlg(Lang(str1,str2), 0);
        flag = 0;
        return
    end

    if ~exist(SonarScopeDocumentation, 'dir')
        str1 = sprintf('Le r�pertoire "%s" n''existe pas, v�rifiez l''installation.', SonarScopeDocumentation);
        str2 = sprintf('The directory "%s" does not exist, check the installation.', SonarScopeDocumentation);
        my_warndlg(Lang(str1,str2), 0);
    end

    if ~exist(SonarScopeData, 'dir')
        str1 = sprintf('Le r�pertoire "%s" n''existe pas, v�rifiez l''installation.', SonarScopeData);
        str2 = sprintf('The directory "%s" does not exist, check the installation.', SonarScopeData);
        my_warndlg(Lang(str1,str2), 0);
    end
end
