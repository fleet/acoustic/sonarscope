function checkInstallationDataDocAndMex

global SonarScopeDocumentation %#ok<GVMIS>
global NUMBER_OF_PROCESSORS %#ok<GVMIS>

%% Check SonarScopeData

nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.xml', 'NoMessage');
if isempty(nomFic)
    str1 = 'SonarScopeData ne semble pas �tre install�. Relancez SetUpSonarScope_Data_yyyymmdd_R20xxv.exe SVP.';
    str2 = 'The SonarScopeData does not seem to be installed. Please rerun SetUpSonarScope_Data_yyyymmdd_R20xxv.exe.';
    my_warndlg(Lang(str1,str2), 1);
end

%% Check SonarScopeDoc

nomFic = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'SScHelp-ADOC', 'Accueil.html');
if ~exist(nomFic, 'file')
    str1 = 'SonarScopeDoc ne semble pas �tre install�. Relancez SetUpSonarScope_Doc_yyyymmdd_R20xxv.exe SVP.';
    str2 = 'The SonarScopeDoc does not seem to be installed. Please rerun SetUpSonarScope_Doc_yyyymmdd_R20xxv.exe.';
    my_warndlg(Lang(str1,str2), 1);
end

% Check if recent version of SonarScopeDoc

nomFic = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', 'SScDoc-Tutorial-ALL', 'SScDoc-Tutorial-ALL-EM710BscorrSHOM-AppendixA.html');
if ~exist(nomFic, 'file')
    str1 = 'Une nouvelle version de SonarScopeDoc est disponible. T�l�charger cette le nouveau SetUpSonarScope_Doc_yyyymmdd_R20xxv.exe et relancez-leSVP.';
    str2 = 'A new release of SonarScopeDoc is available. Please download this new SetUpSonarScope_Doc_yyyymmdd_R20xxv.exe version and rerun it.';
    my_warndlg(Lang(str1,str2), 1);
end

%% Check mex functions

try
    xq = interp1Linear_mex(1:100, rand(1,100), 0:0.5:100); %#ok<NASGU>
catch ME %#ok<NASGU>
    str = sprintf('SonarScope implements mex functions in order to use compiled computations on specific tasks.');
    str = sprintf('%s It seems these functions crash on your computer. Do not worry, SSc automatically switches to the "matlab" versions o these functions.', str);
    str = sprintf('%s\n\nCould you please install "Microsoft Studio Visual C++ Run-Time" that is delivered in the "R20xxy_SetUpSonarScope_External_yyyymmdd.exe" and launch SSc again ?', str);
    str = sprintf('%s\n\nPlease, let us know if this installation has resolved the problem. Email to sonarscope@ifremer.fr', str);
    str = sprintf('%s\n\nNote : you must use an "External" setup generated after 2019/12/18. Download a new version in case you do not have it.', str);
    my_warndlg(str, 1);
    NUMBER_OF_PROCESSORS = 0;
    return
end

try
    X = ones(50);
    NUMBER_OF_PROCESSORS = int32(str2double(getenv('NUMBER_OF_PROCESSORS')));
    Y = interpHorzImage_mexmc(single(X), NUMBER_OF_PROCESSORS, 1); %#ok<NASGU>
catch ME %#ok<NASGU>
    str = sprintf('SonarScope implements OpenMP mex functions in order to use multithreading computations on specific tasks.');
    str = sprintf('%s It seems these functions crash on your computer. Do not worry, SSc automatically switches to the "matlab" versions o these functions.', str);
    str = sprintf('%s\n\nCould you please install "Microsoft Studio Visual C++ Run-Time" that is delivered in the "R20xxy_SetUpSonarScope_External_yyyymmdd.exe" and launch SSc again ?', str);
    str = sprintf('%s\n\nPlease, let us know if this installation has resolved the problem. Email to sonarscope@ifremer.fr', str);
    str = sprintf('%s\n\nNote : you must use an "External" setup generated after 2019/12/18. Download a new version in case you do not have it.', str);
    my_warndlg(str, 1);
    NUMBER_OF_PROCESSORS = 0;
    return
end