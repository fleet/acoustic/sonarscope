% Check if the paches for Matlab have been installed. Do it otherwise
%
% Syntax
%   flag = checkPatchsMatLab
%
% Output Arguments
%   flag : 1 ou 0
%
% More About : The patches are stored in IfremerToolboxRessource (SScTbxResources soon)
%
% Examples
%   flag = checkPatchsMatLab
%
% Authors  : GLU
% See also : ifrStartup Authors
%-------------------------------------------------------------------------------

function flag = checkPatchsMatLab

global IfrTbxResources %#ok<GVMIS>

flag = 1;

verMatLab = ver('MatLab');
rel = verMatLab.Release;
rel = regexprep(rel, '[(|)]', '');

% Localisation des travaux de check � effectuer
dirPatch   = fullfile(IfrTbxResources, 'PatchsMatLab', rel);
listMFiles = dir(fullfile(dirPatch, '*.m'));

for k=1:numel(listMFiles)
    [~, rootFic] = fileparts(listMFiles(k).name);
    fileM = [rootFic '_MathWorks.m'];
    
    % On teste la pr�sence dans les r�pertoires Public.    
    whichFileMatLab = which(listMFiles(k).name);
    
    % On teste la pr�sence dans les r�pertoires Private (op�rations en deux temps obligatoires).    
    if isempty(whichFileMatLab)
        whichFileMatLab = which(fullfile('private', listMFiles(k).name));
    end
    if isempty(whichFileMatLab)
        whichFileMatLab = dir(fullfile(toolboxdir('images'), '**', listMFiles(k).name));
        whichFileMatLab = fullfile(whichFileMatLab.folder, whichFileMatLab.name);
    end
    pathFileMatLab = fileparts(whichFileMatLab);
    
    if isempty(pathFileMatLab)
        continue
    end
    
    % Test de pr�sence du fichier d�j� patch�.
    if exist(fullfile(pathFileMatLab, fileM), 'file')
        continue
    end
    
    try
        % Sauvegarde du fichier d'origine dans un fichier
        % _MathWorks.
        fileSource1      = fullfile(pathFileMatLab, listMFiles(k).name);
        fileDestination1 = fullfile(pathFileMatLab, fileM);
        fileSource2      = fullfile(dirPatch,       listMFiles(k).name);
        fileDestination2 = fullfile(pathFileMatLab, listMFiles(k).name);
        
        strFr = sprintf(['Installation d''un patch pour Matlab :\n\nBackup :\nSource : %s\nDestination : %s\n\nPatch:\nSource : %s\nDestination : %s\n\n', ...
            'Voulez-vous accepter cette op�ration ?\n'], ...
            fileSource1, fileDestination1, fileSource2, fileDestination2);
        strUs = sprintf(['Install a patch for Matlab :\n\nBackup :\nSource : %s\nDestination : %s\n\nPatch:\nSource : %s\nDestination : %s\n\n', ...
            'Do you accept this operation ?\n'], ...
            fileSource1, fileDestination1, fileSource2, fileDestination2);
        [rep, flag] = my_questdlg(Lang(strFr, strUs));
        if ~flag
            return
        end
        if rep == 1
            copyfile(fileSource1, fileDestination1);
        
            % Copie du fichier Patch dans le r�pertoire cible � la place du fichier
            % du m�me nom existant.
            copyfile(fileSource2, fileDestination2);
        end
        
    catch ME %#ok<NASGU>
        % Message d'avertissement pour indiquer d'aller voir
        % l'admin syst�me en cas d'�chec de copie.
        strFr = sprintf(['Merci de v�rifier que vous avez les droits de copie du source dans l''arborescence MatLab.' ...
            '\nA d�faut, contacter votre administrateur syst�me or IFREMER-JMA : 02 98 22 47 03.']);
        strUs = sprintf(['Please, check that you have grants to copy files in MatLab directories.' ...
            '\nContact your admin system or IFREMER-JMA : 02 98 22 47 03.']);
        my_warndlg(Lang(strFr, strUs), 1);
        flag = 0;
    end
end
