% Create a variable in memmory mapping
%
% Syntax
%   X = cl_memmapfile(...)
%
% Name-Value Pair Arguments
%   FileName     : File name (one is created by default)
%   Value        : Matrix to be mapped on a file
%   Size         : Size of the matrix to be mapped
%   Format       : Data storage type : uint8, int8, ...(default : 'double')
%   ValNaN       : Value to be used for NoData (to be used if Format is integer)
%   FileCreation : 1=Create a file, 0=mapping on an existing file
%
% Name-only Arguments
%   ChangeFileName : Change the file name
%
% Output Arguments
%   X : Variable
%
% Remarks : Use X(:,:) each time you want to use the values outside a
% cl_memmapfile method
%
% Examples
%   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
%   m = cl_memmapfile('FileName', tempname, 'Value', [1 2 3 4; 5 6 7 8]);
%   get(m, 'FileName')
%   get(m, 'Value')
%   m
%   m(1)
%   m(1,2)
%   m([2 3 7 3])
%   m(1) = pi
%   m(1,2)
%   m(1, 2:3) = [10 11]
%   m(1:2,2) = [10; 11]
%   m(1,:)
%   m(:,1)
%   imagesc(m(1:2,1:4))
%   imagesc(m(:,:))
%   length(m)
%   size(m)
%   min(m)
%   max(m)
%   clear m
%
%   Value = rand(2,3,4)
%   m = cl_memmapfile('Value', Value);
%   m
%   Value([1 7 13 14 19])
%   m([1 7 13 14 19])
%   m(1,1,1)
%   clear m
%
%   m = cl_memmapfile('Value', 0, 'Size', [2, 4]);
%   m(1) = pi
%   m([2 8]) = -5
%   m([2 8]) = [2 3]
%
%   N = 20000;
%   m = cl_memmapfile('Value', 0, 'Size', [N N], 'Format', 'single');
%   m(N,N) = 1;
%
% See also FormationMemmapfile Authors
% Authors : JMA, GLU
%-------------------------------------------------------------------------------

classdef cl_memmapfile < handle

    properties(GetAccess = 'public', SetAccess = 'public')
        FileName    = [];
        Writable    = 0;
        ValNaN      = NaN;
        m           = [];
        Size        = [0 0];
        Format      = 'double';
        ErMapper    = false;
        DoNotDelete = false;
        DoNotDelete2 = false;
    end


    methods(Access = 'public')
        function this = cl_memmapfile(varargin)
            % ---------------------
            % Constructeur
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            % ---------------------

            if isempty(varargin{1})
                return
            end

            [varargin, Struct] = getPropertyValue(varargin, 'Struct', []);
            if ~isempty(Struct)
                this = class(Struct, 'cl_memmapfile');
                return
            end

            %{
            % D�but test si encore appel� : cette partie devrait �tre
            % supprim�e si on ne passe jamais 
            % Look at if the instance was created only in order to reach a
            % method of the class Ex : MethodX(cl_memmapfile([]))
            % (this is done systematically on all my constructors)
            if (nargin == 1) && isempty(varargin{1})
                my_breakpoint
                return
            end
            % Fin test si encore appel�
            %}


            % Init the instance with the parameters passed when one creates an instance
            [varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 0);
            this = set(this, varargin{:});
            if isempty(this)
                return
            end

            if ~LogSilence
                % JMA : d�commenter pour pouvoir trouver les anomalies de delete
                %{
                disp('---------------------------------------------------------')
                fprintf('Creating file %s\n', this.FileName);
                fprintf('    Matrix size : [%s]\n', num2str(this.Size));
                if ~my_isdeployed
                    printStack(this, dbstack)
                end
                %}
            end
        end % End of constructor


        function x = plus(this1, this2, varargin)
            % Addition de 2 matrices en memory-mapping
            %
            % Syntax
            %   x = a + b
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %   b : Instance de cl_memmapfile
            %
            % Output Arguments
            %   X : Valeur minimale de la matrice
            %
            % Remarks : Cette fonction est une adaptation minimaliste de la fonction min de
            %           MATLAB, elle est �quivalente � min(a(:))
            %
            % Examples
            %   m1 = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   m2 = cl_memmapfile('Value', [5 6 7 8; 1 2 3 4]);
            %   x = m1 + m2;
            %   x = m1 + pi;
            %   x = pi + m1;
            %   x = [1 2 3 4; 5 6 7 8] + m1;
            %   x = m1 + m2 + m1;
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            if isa(this1, 'cl_memmapfile')
                if isa(this2, 'cl_memmapfile')
                    try
                        x = this1.m.Data.x + this2.m.Data.x;
                        x = cl_memmapfile('Value', x);
                    catch %#ok<CTCH>
                        % TODO : here, a lot of cases can appear, the size of the 2 matrices should be checked and the following code should be adapted to the
                        % various situations
                        x = cl_memmapfile('Value', NaN, 'Size', this1.Size, 'Format', this1.Format);
                        for k1=1:this1.Size(1)
                            x.m.Data.x(k1,:,:) = this1.m.Data.x(k1,:,:) + this2.m.Data.x(k1,:,:);
                        end
                    end
                else
                    try
                        x = this1.m.Data.x + this2;
                        x = cl_memmapfile('Value', x);
                    catch %#ok<CTCH>
                        % TODO : here, a lot of cases can appear, the size of the 2 matrices should be checked and the following code should be adapted to the
                        % various situations
                        x = cl_memmapfile('Value', NaN, 'Size', this1.Size, 'Format', this1.Format);
                        for k1=1:this1.Size(1)
                            x.m.Data.x(k1,:,:) = this1.m.Data.x(k1,:,:) + this2(k1,:,:);
                        end
                    end
                end
            else
                if isa(this2, 'cl_memmapfile')
                    try
                        x = this1 + this2.m.Data.x;
                        x = cl_memmapfile('Value', x);
                    catch %#ok<CTCH>
                        % TODO : here, a lot of cases can appear, the size of the 2 matrices should be checked and the following code should be adapted to the
                        % various situations
                        x = cl_memmapfile('Value', NaN, 'Size', this1.Size, 'Format', this1.Format);
                        for k1=1:this2.Size(1)
                            x.m.Data.x(k1,:,:) = this1 + this2.m.Data.x(k1,:,:);
                        end

                    end
                else % We should never find this situation
                    x = this1 + this2;
                end
            end
        end % Fonction plus


        function printStack(this, ST) %#ok<INUSL>
            % ---------------------
            % Fonction printStack
            % Examples
            %   printStack(m, dbstack)
            % ---------------------
            if ~my_isdeployed
                for k=1:length(ST)
                    fprintf('\tFunction : %s\t - line : %d\n', ST(k).name, ST(k).line);
                    logFileId = getLogFileId;
                    logFileId.error('cl_memmapfile', 'addFileToBeDestroyedOnListe fopen failure');
                end
            end
        end % Fin de printStack


        function varargout = set(this, varargin)
            %------------------------------------------------------------------
            % Set property values
            %
            % Syntax
            %   a = set(a, PN, PV)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfile
            %   PN : Property name
            %   PV : Property value
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = set(m, 'Writable', 1)
            %------------------------------------------------------------------

            % ----------------------------------------------------------
            % Read or initialyse Property values
            [varargin, ChangeFileName] = getFlag(varargin, 'ChangeFileName');

            [varargin, FileNameIn]     = getPropertyValue(varargin, 'FileName',     []);
            [varargin, Value]          = getPropertyValue(varargin, 'Value',        []);
            [varargin, SizeIn]         = getPropertyValue(varargin, 'Size',         []);
            [varargin, FormatIn]       = getPropertyValue(varargin, 'Format',       []);
            [varargin, FileCreation]   = getPropertyValue(varargin, 'FileCreation', 1);
            [varargin, this.ValNaN]    = getPropertyValue(varargin, 'ValNaN',       this.ValNaN);
            [varargin, WritableIn]     = getPropertyValue(varargin, 'Writable',     []);
            [varargin, ErMapperIn]     = getPropertyValue(varargin, 'ErMapper',     []);
            [varargin, Offset]         = getPropertyValue(varargin, 'Offset',       0);
            [varargin, DoNotDeleteIn]  = getPropertyValue(varargin, 'DoNotDelete',  []);
            [varargin, DoNotDelete2In] = getPropertyValue(varargin, 'DoNotDelete2', []);

            if ~isempty(FileNameIn)
                this.FileName = FileNameIn;
            end

            if ChangeFileName
                this.m.Filename = FileNameIn;
                this.Writable   = 1;
                this.m.Writable = true;
                varargout{1} = this;
                return
            end

            if ~isempty(SizeIn)
                this.Size = SizeIn;
            end

            if ~isempty(ErMapperIn)
                this.ErMapper = ErMapperIn;
            end

            if ~isempty(DoNotDeleteIn)
                this.DoNotDelete = DoNotDeleteIn;
                if isempty(varargin)
                    varargout{1} = this;
                    return
                end
            end

            if ~isempty(DoNotDelete2In)
                this.DoNotDelete2 = DoNotDelete2In;
                if isempty(varargin)
                    varargout{1} = this;
                    return
                end
            end

            if ~isempty(WritableIn)
                this.Writable = WritableIn;
                if WritableIn
                    this.m.Writable = true;
                else
                    this.m.Writable = false;
                end
                %                 if isempty(varargin)
                %                     varargout{1} = this;
                %                     return
                %                 end
            end

            if FileCreation
                if isa(Value, 'cl_memmapfile')
                    Value = get(Value, 'Value');
                end

                % Garbage collector
                %     garbage_collector

                if isempty(SizeIn) && ~isempty(Value)
                    this.Size = size(Value);

                    % Appel sous la forme : m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);

                    if isempty(this.FileName)
                        Name = my_tempname;
                        [pathName, Name] = fileparts(Name);
                        this.FileName = fullfile(pathName, [Name '.memmapfile']);
                    end

                    if isempty(FormatIn)
                        FormatIn = class(Value(1));
                    end

                    this.m = [];
                    fid = fopen(this.FileName, 'w+');
                    if fid == -1
                        this = Value;
                    end

                    count = fwrite(fid, Value, FormatIn);
                    if count ~= numel(Value)
                        fclose(fid);
                        logFileId = getLogFileId;
                        logFileId.error('cl_memmapfile/set', 'fwrite failure');
                        varargout{1} = this;
                        return
                    end
                    fclose(fid);

                    % Call this way : m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]); size(V) = [4 2]
                    if strcmp(FormatIn, 'logical')
                        FormatIn = 'uint8';
                    end
                    if strcmp(FormatIn, 'char')
                        FormatIn = 'uint8';
                    end
                    this.m = memmapfile(this.FileName, ...
                        'Format', {FormatIn size(Value) 'x'}, ...
                        'Writable', true);
                    this.Writable = 1;

                elseif ~isempty(SizeIn) && (length(Value) <= 1)
                    % Call this way : m = cl_memmapfile('Value', NaN, 'Size', [100 200]);
                    if (length(SizeIn) == 3) && (SizeIn(3) == 1)
                        SizeIn = SizeIn(1:2);
                    end
                    this.Size = double(SizeIn);

                    if isempty(Value)
                        Value = 0;
                    end

                    if isempty(this.FileName)
                        Name = my_tempname;
                        [pathName, Name] = fileparts(Name);
                        this.FileName = fullfile(pathName, [Name '.memmapfile']);
                    end

                    if isempty(FormatIn)
                        FormatIn = 'single';
                    end

                    this.m = [];
                    fid = fopen(this.FileName, 'w+');
                    if fid == -1
                        logFileId = getLogFileId;
                        logFileId.error('cl_memmapfile/set', 'fopen w+ failure');
                        varargout{1} = [];
                    end

                    if numel(Value) == 1
                        Nb = min(prod(this.Size), 10000);
                        Value = repmat(Value, 1, Nb);
                        N = floor(prod(this.Size) / Nb);
                        for k=1:N
                            count = fwrite(fid, Value, FormatIn);
                            if count ~= Nb
                                fclose(fid);
                                logFileId = getLogFileId;
                                logFileId.error('cl_memmapfile/set', 'fwrite failure');
                                varargout{1} = this;
                                return
                            end
                        end
                        Nb = prod(this.Size) - N*Nb;
                        if Nb > 0
                            Value = repmat(Value(1), 1, Nb);
                            count = fwrite(fid, Value, FormatIn);
                            if count ~= Nb
                                fclose(fid);
                                logFileId = getLogFileId;
                                logFileId.error('cl_memmapfile/set', 'fwrite failure');
                                varargout{1} = this;
                                return
                            end
                        end
                    else
                        count = fwrite(fid, Value, FormatIn);
                        if count ~= prod(this.Size)
                            fclose(fid);
                            logFileId = getLogFileId;
                            logFileId.error('cl_memmapfile/set', 'fwrite failure');
                            varargout{1} = this;
                            return
                        end
                    end
                    fclose(fid);


                    this.m = memmapfile(this.FileName, ...
                        'Format', {FormatIn this.Size 'x'}, ...
                        'Writable', true, 'Repeat', 1);
                    this.Writable = 1;

                elseif isempty(SizeIn)
                    this.Writable = 0;

                else
                    if (length(SizeIn) == 3) && (SizeIn(3) == 1)
                        SizeIn = SizeIn(1:2);
                    end
                    this.Size = SizeIn;
                    this.m = memmapfile(this.FileName, ...
                        'Format', {this.Format this.Size 'x'}, ...
                        'Writable', false, 'Repeat', 1);
                    this.Writable = 0;
                end

            else % Cas de l'ouverture d'un fichier ErMapper en lecture
                if strcmp(FormatIn, 'logical')
                    FormatIn = 'uint8';
                end
                if strcmp(FormatIn, 'char')
                    FormatIn = 'uint8';
                end


                if (length(SizeIn) == 3) && (SizeIn(3) == 1)
                    SizeIn = SizeIn(1:2);
                end
                this.Size = SizeIn;
                switch length(SizeIn)
                    case 2
                        this.m = memmapfile(this.FileName, ...
                            'Format', {FormatIn [SizeIn(2) SizeIn(1)] 'x'}, ...
                            'Writable', false, 'Repeat', 1, 'Offset', Offset);

                    case 3
                        if this.ErMapper
                            this.m = memmapfile(this.FileName, ...
                                'Format', {FormatIn [SizeIn(2) SizeIn(3) SizeIn(1)] 'x'}, ...
                                'Writable', false, 'Repeat', 1);
                        else
                            this.m = memmapfile(this.FileName, ...
                                'Format', {FormatIn [SizeIn(2) SizeIn(1) SizeIn(3)] 'x'}, ...
                                'Writable', false, 'Repeat', 1);
                        end

                    case 4
                        if this.ErMapper
                            this.m = memmapfile(this.FileName, ...
                                'Format', {FormatIn [SizeIn(2) SizeIn(3) SizeIn(1) SizeIn(4)] 'x'}, ...
                                'Writable', false, 'Repeat', 1);
                        else
                            this.m = memmapfile(this.FileName, ...
                                'Format', {FormatIn [SizeIn(2) SizeIn(1) SizeIn(3:4)] 'x'}, ...
                                'Writable', false, 'Repeat', 1);
                        end
                end
                try
                    length(this.m.Data);
                catch %#ok<CTCH>
                    str1 = 'Je vous invite � contr�ler la taille du fichier par rapport aux carat�ristiques attendues de l''image.';
                    str2 = 'Please check the size of your file according to the expected caracteristics of the image.';
                    messageErreurFichier(this.FileName, 'ReadFailure', 'Message', Lang(str1,str2));
                    this.Size(:) = 0;
                end
                this.Writable = 0;
                this.Format = FormatIn;

                % Not anymore usefull since we have the destructor
                % addFileToBeDestroyed(this.FileName)
            end

            this.Format = FormatIn;

            if ~isempty(DoNotDelete2In)
                this.DoNotDelete2 = DoNotDelete2In;
            end

            % Check if varargin is empty
            if ~isempty(varargin)
                str = sprintf('%s is not a property', num2str(varargin{1}));
                logFileId = getLogFileId;
                logFileId.error('cl_memmapfile/set', str);
            end

            % Bye bye
            if nargout == 0
                assignin('caller', inputname(1), this);
            else
                varargout{1} = this;
            end

        end % End of set


        function display(this) %#ok<DISPLAY>
            % Display
            %
            % Syntax
            %   display(a)
            %
            % Input Arguments
            %   a : cl_memmapfile instance
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   m
            %
            % See also cl_memmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------
            disp(getValue(this))

        end % End of display


        function x = eq(a, b)
            % Isequal overloading
            %
            % Syntax
            %   c = a == b
            %
            % Input Arguments
            %   a  : cl_memmapfile instance or scalar
            %   b  : cl_memmapfile instance or scalar
            %
            % Output Arguments
            %   c : Boolean matrix
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfile('Value', [2 6 1 3; 5 4 8 7]);
            %   m == n
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            flaga = isa(a, 'cl_memmapfile');
            flagb = isa(b, 'cl_memmapfile');

            if flaga && flagb
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            x(k1,:,k3) = (getValue(a, k1, k2 ,k3) ==  getValue(b, k1, k2 ,k3));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        x(k1,:) =  (getValue(a, k1, k2) ==  getValue(b, k1, k2));
                    end
                end

            elseif flaga
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            if length(b) == 1
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) ==  b;
                            else
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) ==  b(k1,:,k3);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        if length(b) == 1
                            x(k1,:) =  getValue(a, k1, k2) ==  b;
                        else
                            x(k1,:) =  getValue(a, k1, k2) ==  b(k1,:);
                        end
                    end
                end
            else
                k2 = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k3=1:b.size(3)
                        for k1=1:b.size(1)
                            if length(a) == 1
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) ==  a;
                            else
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) ==  a(k1,:,k3);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for k1=1:b.size(1)
                        if length(a) == 1
                            x(k1,:) =  getValue(b, k1, k2) ==  a;
                        else
                            x(k1,:) =  getValue(b, k1, k2) ==  a(k1,:);
                        end
                    end
                end
            end

        end % Enf method eq


        function PV = get(this, PN)
            % get a property value
            %
            % Syntax
            %   PV = get(a, PN)
            %
            % Input Arguments
            %   a  : cl_memmapfile instance
            %   PN : property name
            %
            % Output Arguments
            %   PV : Property value
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   FileName = get(m, 'FileName')
            %   Writable = get(m, 'Writable')
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------
            switch PN
                case 'FileName'
                    PV = this.FileName;

                case 'Value'
                    PV = getValue(this);

                case 'Writable'
                    PV = this.Writable;

                otherwise
                    PV = [];
            end

        end % End method get


        function x = gt(a, b)
            % Overload ">" operator
            %
            % Syntax
            %   c = a > b
            %
            % Input Arguments
            %   a : cl_memmapfile instance
            %   b : cl_memmapfile instance
            %
            % Output Arguments
            %   c : Boolean matrix
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfile('Value', [2 6 1 3; 5 4 8 7]);
            %   m > n
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            flaga = isa(a, 'cl_memmapfile');
            flagb = isa(b, 'cl_memmapfile');

            if flaga && flagb
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            x(k1,:,k3) = (getValue(a, k1, k2 ,k3) >  getValue(b, k1, k2 ,k3));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        x(k1,:) =  (getValue(a, k1, k2) >  getValue(b, k1, k2));
                    end
                end

            elseif flaga
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            if length(b) == 1
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) >  b;
                            else
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) >  b(k1,:,k3);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        if length(b) == 1
                            x(k1,:) =  getValue(a, k1, k2) >  b;
                        else
                            x(k1,:) =  getValue(a, k1, k2) >  b(k1,:);
                        end
                    end
                end
            else
                k2 = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k3=1:b.size(3)
                        for k1=1:b.size(1)
                            if length(a) == 1
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) >  a;
                            else
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) >  a(k1,:,k3);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for k1=1:b.size(1)
                        if length(a) == 1
                            x(k1,:) =  getValue(b, k1, k2) >  a;
                        else
                            x(k1,:) =  getValue(b, k1, k2) >  a(k1,:);
                        end
                    end
                end
            end

        end % End method gt


        function flag = isnan(this)
            % Test de valeur NaN d'une matrice en memory-mapping
            %
            % Syntax
            %   flag = isnan(a)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfile
            %
            % Output Arguments
            %   flag : Matrice de bool�ens
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   flag = isnan(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            k2 = 1:this.Size(2);
            flag = 1==0;
            if length(this.Size) == 3
                flag(this.Size(1), this.Size(2), this.Size(3)) = 0;
                for k3=1:this.Size(3)
                    for k1=1:this.Size(1)
                        flag(k1,:,k3) = isnan(getValue(this, k1, k2 ,k3));
                    end
                end
            else
                flag(this.Size(1), this.Size(2)) = 0;
                for k1=1:this.Size(1)
                    flag(k1,:) =  isnan(getValue(this, k1, k2));
                end
            end

        end % isnan


        function x = le(a, b)
            % Test "<=" entre une d'une matrice en memory-mapping et une valeur ou
            % entre deux matrices
            %
            % Syntax
            %   c = a <= b
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfile ou scalaire
            %   b  : Instance de cl_memmapfile ou scalaire
            %
            % Output Arguments
            %   c : Matrice de boul�ens
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfile('Value', [2 6 1 3; 5 4 8 7]);
            %   m <= n
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            flaga = isa(a, 'cl_memmapfile');
            flagb = isa(b, 'cl_memmapfile');

            if flaga && flagb
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            x(k1,:,k3) = (getValue(a, k1, k2 ,k3) <=  getValue(b, k1, k2 ,k3));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        x(k1,:) =  (getValue(a, k1, k2) <=  getValue(b, k1, k2));
                    end
                end

            elseif flaga
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            if length(b) == 1
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) <=  b;
                            else
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) <=  b(k1,:,k3);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        if length(b) == 1
                            x(k1,:) =  getValue(a, k1, k2) <=  b;
                        else
                            x(k1,:) =  getValue(a, k1, k2) <=  b(k1,:);
                        end
                    end
                end
            else
                k2 = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k3=1:b.size(3)
                        for k1=1:b.size(1)
                            if length(a) == 1
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) <=  a;
                            else
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) <=  a(k1,:,k3);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for k1=1:b.size(1)
                        if length(a) == 1
                            x(k1,:) =  getValue(b, k1, k2) <=  a;
                        else
                            x(k1,:) =  getValue(b, k1, k2) <=  a(k1,:);
                        end
                    end
                end
            end

        end % Fonction le


        function l = length(this)
            % Dimension maximale d'une matrice en memory-mapping
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Syntax
            %   n = length(a)
            %
            % Output Arguments
            %   n : Taille maximale d'une matrice
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = length(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            l = max(this.Size);
        end % Fonction length


        function l = ndims(this)
            % Nombre de dimensions d'une matrice en memory-mapping
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Syntax
            %   n = length(a)
            %
            % Output Arguments
            %   n : Nombre de dimensions d'une matrice
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = ndims(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            l = length(this.Size);
        end % Fonction ndims


        function x = lt(a, b)
            % Test "<" entre une d'une matrice en memory-mapping et une valeur ou
            % entre deux matrices
            %
            % Syntax
            %   c = a < b
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfile ou scalaire
            %   b  : Instance de cl_memmapfile ou scalaire
            %
            % Output Arguments
            %   c : Matrice de boul�ens
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfile('Value', [2 6 1 3; 5 4 8 7]);
            %   m < n
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            flaga = isa(a, 'cl_memmapfile');
            flagb = isa(b, 'cl_memmapfile');

            if flaga && flagb
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            x(k1,:,k3) = (getValue(a, k1, k2 ,k3) <  getValue(b, k1, k2 ,k3));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        x(k1,:) =  (getValue(a, k1, k2) <  getValue(b, k1, k2));
                    end
                end

            elseif flaga
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            if length(b) == 1
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) <  b;
                            else
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) <  b(k1,:,k3);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        if length(b) == 1
                            x(k1,:) =  getValue(a, k1, k2) <  b;
                        else
                            x(k1,:) =  getValue(a, k1, k2) <  b(k1,:);
                        end
                    end
                end
            else
                k2 = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k3=1:b.size(3)
                        for k1=1:b.size(1)
                            if length(a) == 1
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) <  a;
                            else
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) <  a(k1,:,k3);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for k1=1:b.size(1)
                        if length(a) == 1
                            x(k1,:) =  getValue(b, k1, k2) <  a;
                        else
                            x(k1,:) =  getValue(b, k1, k2) <  a(k1,:);
                        end
                    end
                end
            end

        end % Fonction lt


        function x = max(this)
            % Valeur max d'une matrice en memory-mapping
            %
            % Syntax
            %   x = max(a)
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Output Arguments
            %   X : Valeur maximale de la matrice
            %
            % Remarks : This is not exactly the max matlab behaviour, this
            % is equivalent to max(a(:))
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   x = max(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            % WARNING, this is a minimalist adaptation of matlab max
            % function : should be improved
            x = max(this.m.Data.x(:));
        end % Fonction max


        function x = min(this, varargin)
            % Valeur min d'une matrice en memory-mapping
            %
            % Syntax
            %   x = min(a)
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Output Arguments
            %   X : Valeur minimale de la matrice
            %
            % Remarks : Cette fonction est une adaptation minimaliste de la fonction min de
            %           MATLAB, elle est �quivalente � min(a(:))
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   x = min(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------
            % ATTENTION, ceci est une adaptation minimaliste de la fonction min de
            % MATLAB

            x = min(this.m.Data.x(:));
        end % Fonction min


        function x = ne(a, b)
            % Test "~=" entre une d'une matrice en memory-mapping et une valeur ou
            % entre deux matrices
            %
            % Syntax
            %   c = a ~= b
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfile ou scalaire
            %   b  : Instance de cl_memmapfile ou scalaire
            %
            % Output Arguments
            %   c : Matrice de boul�ens
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfile('Value', [2 6 1 3; 5 4 8 7]);
            %   m ~= n
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            flaga = isa(a, 'cl_memmapfile');
            flagb = isa(b, 'cl_memmapfile');

            if flaga && flagb
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            x(k1,:,k3) = (getValue(a, k1, k2 ,k3) ~=  getValue(b, k1, k2 ,k3));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        x(k1,:) =  (getValue(a, k1, k2) ~=  getValue(b, k1, k2));
                    end
                end

            elseif flaga
                k2 = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k3=1:a.size(3)
                        for k1=1:a.size(1)
                            if length(b) == 1
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) ~=  b;
                            else
                                x(k1,:,k3) =  getValue(a, k1, k2 ,k3) ~=  b(k1,:,k3);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for k1=1:a.size(1)
                        if length(b) == 1
                            x(k1,:) =  getValue(a, k1, k2) ~=  b;
                        else
                            x(k1,:) =  getValue(a, k1, k2) ~=  b(k1,:);
                        end
                    end
                end
            else
                k2 = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k3=1:b.size(3)
                        for k1=1:b.size(1)
                            if length(a) == 1
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) ~=  a;
                            else
                                x(k1,:,k3) =  getValue(b, k1, k2 ,k3) ~=  a(k1,:,k3);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for k1=1:b.size(1)
                        if length(a) == 1
                            x(k1,:) =  getValue(b, k1, k2) ~=  a;
                        else
                            x(k1,:) =  getValue(b, k1, k2) ~=  a(k1,:);
                        end
                    end
                end
            end

        end % Fonction ne


        function n = numel(this, varargin)
            % Nombre de valeurs d'une matrice en memory-mapping
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Syntax
            %   x = numel(a)
            %
            % Output Arguments
            %   X : Nombre de valeurs de la matrice
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   n = numel(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            sz = size(this, varargin{:});
            n = prod(sz);
        end % Fonction numel


        function flag = isempty(this, varargin)
            % La matrice en memory-mapping est-elle vide
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Syntax
            %   flag = isempty(a)
            %
            % Output Arguments
            %   X : Nombre de valeurs de la matrice
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   flag = isempty(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            n = numel(this, varargin{:});
            flag = (n == 0);
        end % Fonction numel


        function this = set_ErMapperOnly(this, ErMapper)
            % Method to be used in case the loaded matrix is common image
            % order (Er-Mapper order)
            %
            % Syntax
            %   a = set_ErMapperOnly(a, ErMapper)
            %
            % Input Arguments
            %   a        : Instance de cl_memmapfile
            %   ErMapper : 0=Matrice Matlab, 1=Matrice ErMapper
            %
            % Output Arguments
            %   a        : Instance de cl_memmapfile
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8])
            %   m = set_ErMapperOnly(m, true)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            this.ErMapper = ErMapper;
            SZ = this.m.Format{2};
            if length(SZ) == 2
                this.m.Format{2} = [SZ(2) SZ(1)];
            else
                this.m.Format{2} = [SZ(3) SZ(2) SZ(1)] ;
            end
        end % End of set_ErMapperOnly


        function varargout = size(this, varargin)
            % Taille d'une matrice en memory-mapping
            %
            % Syntax
            %   [...] = size(a, ...)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfile
            %
            % Name-Value Pair Arguments
            %   dim : Numero de la dimension demand�e
            %
            % Output Arguments
            %   sz : Taille de la matrice ou taille de la dimension demand�e
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8])
            %   size(m)
            %   size(m,1)
            %   size(m,2)
            %   size(m,3)
            %   [R,C] = size(m)
            %  [ R,C,N] = size(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            sz = this.Size;

            if (length(sz) >=3) && all(sz(3:end) == 1)
                sz = sz(1:2);
            end

            if nargin > 1
                n = varargin{1};
                if n > length(sz)
                    sz = 1;
                else
                    sz = sz(n);
                end
            end

            switch nargout
                case {0;1}
                    varargout{1} = sz;
                otherwise
                    for k=1:nargout
                        if k > length(sz)
                            varargout{k} = 1; %#ok<AGROW>
                        else
                            varargout{k} = sz(k); %#ok<AGROW>
                        end
                    end
            end
        end % Fonction size


        function this = subsasgn(this, theStruct, Value)
            % Acc�s en �critude des �l�ments d'une matrice en memory-mapping
            %
            % Syntax
            %   a = subsasgn(a, theStruct, Value)
            %
            % Input Arguments
            %   a         : Instance de cl_memmapfile
            %   theStruct : (sub) ou (sub1, sub2) ou (sub1, sub2, sub3)
            %   Value : Valeurs demand�es
            %
            % Output Arguments
            %   a : Instance de cl_memmapfile
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   m(1)
            %   m(1,2)
            %   m([2 3 7 3])
            %   m(1,:)
            %   m(:,2)
            %   m(1, 2:3) = [10 11]
            %   m(1:2,2) = [10; 11]
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            theSubs = theStruct(1).subs;
            this = setValue(this, Value, theSubs{:});
        end % Fonction subasgn


        function Value = subsref(this, theStruct)
            % Acc�s en lecture des �l�ments d'une matrice en memory-mapping
            %
            % Syntax
            %   Value = subsref(a, theStruct)
            %
            % Input Arguments
            %   a         : Instance de cl_memmapfile
            %   theStruct : (sub) ou (sub1, sub2) ou (sub1, sub2, sub3)
            %
            % Output Arguments
            %   Value : Valeurs demand�es
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   m(1)
            %   m(1,2)
            %   m([2 3 7 3])
            %   m(1,:)
            %   m(:,2)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            theSubs = theStruct(1).subs;
            if (length(theSubs) == 1) && ischar(theSubs{1}) && strcmp(theSubs{1}, ':')
                theSubs{2} = ':';
                flagVecteur = 1;
            else
                sz = this.Size;
                %                 sz(1:2) = fliplr(sz(1:2));
                for k=1:length(theSubs)
                    if ischar(theSubs{k}) && strcmp(theSubs{k}, ':')
                        if length(sz) >= k
                            theSubs{k} = 1:sz(k);
                        else
                            theSubs{k} = 1;
                        end
                    end
                end
                flagVecteur = 0;
            end

            sz = size(this);
            if (sz(1) == sz(2)) && (sz(1) ~= 1) % TODO BugMatricesCarr�es
                % Ajout JMA le 02/09/2014 pour matrices carr�es : ATTENTION DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                Value = getValue(this);
                Value = Value(theSubs{:});
                %{
                % Plus besoin de cet avertissement car le pb a l'air d'�tre r�gl�
                if ~my_isdeployed
                    str = 'cl_memmapfile/subsref : bidouille JMA pour �viter transposition matrices carr�es, il est possible que �a produise un effet de bord malencontreux. Si vous avez ce message et que le r�sultat de la matrice est transpos� alors il faut que je supprime ce correctif.';
                    my_warndlg(str, 0, 'Tag', 'cl_memmapfile/subsref : bidouille JMA');
                end
                %}
            else
                Value = getValue(this, theSubs{:});
            end

            if flagVecteur
                Value = Value(:);
            end

        end % Fonction subsref


        function x = uint8(this)
            % Conversion d'une matrice en memory-mapping en uint8
            %
            % Syntax
            %   x = uint8(a)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfile
            %
            % Output Arguments
            %   x : Valeur de la matrice en uint8
            %
            % Examples
            %   m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8]);
            %   x = uint8(m)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %--------------------------------------------------------------------------

            x = zeros(this.Size, 'uint8');
            k2 = 1:this.Size(2);
            if length(this.Size) == 2
                for k1=1:this.Size(1)
                    x(k1,:) = uint8(getValue(this, k1, k2));
                end
            else
                for k3=1:this.Size(3)
                    for k1=1:this.Size(1)
                        x(k1,:,k3) = uint8(getValue(this, k1, k2 ,k3));
                    end
                end
            end
        end % Fonction uint8


        function delete(this)
            % Destructeur appel� lors de la destruction de l'instance de l'objet
            % par clear this.
            % Examples
            %   clear m;
            % -------------------------------------------------------------

            if this.DoNotDelete2
                return
            end

            if this.DoNotDelete
                addFileToBeDestroyedOnListe(this, this.FileName)
                return
            end

            try
                if isempty(this)
                    return
                end
                nomFic = this.FileName;
                if isempty(nomFic)
                    return
                end

                % On verifie si c'est bien un fichier avec l'extension
                % ".memmapfile" car in ne faudrait pas detruire des fichiers
                % ErMapper

                [Dir, Name, Ext] = fileparts(nomFic); %#ok<ASGLU>
                if ~strcmp(Ext, '.memmapfile')
                    return
                end

                if exist(nomFic, 'file')
                    this = set(this, 'Writable', 0);
                    S = recycle;
                    recycle('off')
                    w = warning;
                    warning('Off')
                    delete(nomFic);
                    warning(w)
                    recycle(S)

                    if exist(nomFic, 'file')
                        %                         fprintf('It was impossible to delete %s\n', nomFic);
                        addFileToBeDestroyedOnListe(this, nomFic)
                    end
                end

            catch ME
                logFileId = getLogFileId;
                logFileId.error('cl_memmapfile/delete', 'failure');
                report = getReport(ME, 'extended', 'hyperlinks', 'off');
                fprintf('%s\n', report);
            end
        end % Fonction Delete


        % This method was useful with the previous object programming
        % version (no destructor existed, I had to try to do it by myself,
        % it was a mess)
        function garbageCollector(this) %#ok<MANU>
            SemaineDerniere = datetime('today') - days(7);
            X = dir(my_tempdir);
            for k=1:length(X)
                if k == 1
                    logFileId = getLogFileId;
                end
                [~, nomFic, ext] = fileparts(X(k).name);
                if strcmp(ext, '.memmapfile')
                    pppp = strfind(X(k).name, 'tmp_');
                    if ~isempty(pppp) && (length(nomFic) > 12)
                        TimeFic = datetime(X(k).name(5:12), 'InputFormat', 'yyyyMMdd');
                        if TimeFic < SemaineDerniere
                            try
                                delete(fullfile(my_tempdir, X(k).name))
                                msg = sprintf('"%s" is deleted.', X(k).name);
                                logFileId.warn('cl_memmapfile/garbageCollector', msg);
                            catch %#ok<CTCH>
                                msg = sprintf('"%s" could not be deleted.', X(k).name);
                                logFileId.warn('cl_memmapfile/garbageCollector', msg);
                            end
                        end
                    end
                end

                if strcmp(ext, '.mat') % Rajout� par JMA le 10/06/2013.
                    % TODO : laisser ce truc en place pour une version et
                    % le supprimer ensuite car ces fichiers ne sont en
                    % principe plus g�n�r�s (cela provenait d'un
                    % xml_mat_read � la place d'un xml_read dans
                    % "cl_ermapper_esr/export_alg"
                    if ~isempty(strfind(X(k).name, 'tmp_')) && ~isempty(strfind(X(k).name, '_xml')) && (length(nomFic) > 12)
                        TimeFic = datetime(X(k).name(5:12), 'InputFormat', 'yyyyMMdd');
                        if TimeFic < SemaineDerniere
                            try
                                delete(fullfile(my_tempdir, X(k).name))
                                msg = sprintf('"%s" is deleted.', X(k).name);
                                logFileId.warn('cl_memmapfile/garbageCollector', msg);
                            catch %#ok<CTCH>
                                msg = sprintf('"%s" could not be deleted.', X(k).name);
                                logFileId.warn('cl_memmapfile/garbageCollector', msg);
                            end
                        end
                    end
                end
            end
        end


        function deleteFileOnListe(~, GCF)
            % Nettoyage des fichiers de memory-mapping
            %
            % Syntax
            %   deleteFileOnListe(a, GCF)
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Examples
            %   deleteFileOnListe(cl_memmapfile.empty, gcbf)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            nomFicTemp = fullfile(my_tempdir, 'ListeFicMemmapfileToDelete.dat');
            fid = fopen(nomFicTemp, 'r');
            if fid == -1
                return
            end

            Ligne = {};
            while 1
                str = fgetl(fid);
                if ~ischar(str)
                    break
                end
                Ligne{end+1} = str; %#ok<AGROW>
            end
            fclose(fid);

            S = warning;
            warning('off', 'all')
            nomFic = {};
            H_GCBF = [];

            Ligne = unique(Ligne);
            for k=1:length(Ligne)
                mots = strsplit(Ligne{k});
                if length(mots) == 2
                    GCBF = str2double(mots{1});
                    Nom  = mots{2};

                    %     msg = sprintf('Trying to delete file "%s"\nIf this takes more than few milliseconds you should delete file "%s" by hands. This is a known bug I will try to fix.', Nom, nomFicTemp);
                    %     disp(msg)

                    if isempty(GCF)
                        if exist(Nom, 'file')
                            delete(Nom)
                            if ~exist(Nom, 'file')
                                %                                 if ~my_isdeployed
                                %                                     disp('------------------------------------------')
                                %                                     disp(['Delete file : ' Nom])
                                %                                     printStack(this, dbstack)
                                %                                 end
                                continue
                            end
                        end
                    else
                        if exist(Nom, 'file') && (GCBF == GCF)
                            delete(Nom)
                            if ~exist(Nom, 'file')
                                %                                 if ~my_isdeployed
                                %                                     disp('------------------------------------------')
                                %                                     disp(['Delete file : ' Nom])
                                %                                     printStack(this, dbstack)
                                %                                 end
                                continue
                            end
                        end
                    end

                    % Si le fichier est vieux de plus de 2 jours on ne le met plus dans la liste des fichiers � d�truire
                    % if ~is_my_tempname_old(Nom)
                    if exist(Nom, 'file')
                        nomFic{end+1} = Nom;  %#ok<AGROW>
                        H_GCBF(end+1) = GCBF; %#ok<AGROW>
                    end
                else
                    delete(nomFicTemp) % Il s'agit d'un ancien fichier
                end

            end
            warning(S)

            [nomFic, sub] = unique(nomFic);
            H_GCBF = H_GCBF(sub);

            fid = fopen(nomFicTemp, 'w+');
            if fid == -1
                logFileId = getLogFileId;
                logFileId.error('cl_memmapfile/deleteFileOnListe', 'failure');
                return
            end
            for k=1:length(nomFic)
                fprintf(fid, '%f %s\n', H_GCBF(k), nomFic{k});
            end
            fclose(fid);
        end % end deleteFileOnListe


        function this = my_transpose(this)
            % Transposition de matrices cl_memmapfile.
            %
            % Syntax
            %   my_tranpose(a)
            %
            % Input Arguments
            %   a : Instance de cl_memmapfile
            %
            % Examples
            %   b = my_tranpose(a)
            %
            % See also cl_memmapfile FormationMemmapfile Authors
            % Authors : JMA
            %-------------------------------------------------------------------------------

            nbDims = ndims(this);
            try
                switch nbDims
                    case 2
                        Value = this.m.Data.x(:,:)';
                    case 3
                        Value = my_transpose(this.m.Data.x(:,:,:));
                    otherwise
                        Value = my_transpose(this.m.Data.x(:,:,:,:,:,:,:,:));
                end
                this = cl_memmapfile('Value', Value);
            catch
                sz = this.Size;
                switch nbDims
                    case 2
                        x = cl_memmapfile('Size', sz([2 1]), 'Format', this.Format);
                    otherwise
                        x = cl_memmapfile('Size', sz([2 1 3:end]), 'Format', this.Format);
                end
                switch nbDims
                    case 2
                        for k2=1:sz(1)
                            x.m.Data.x(:,k2) = this.m.Data.x(k2,:)';
                        end
                    case 3
                        for k3=1:size(x,3)
                            x.m.Data.x(:,:,k3) = this.m.Data.x(:,:,k3)';
                        end
                    case 4 % On ne transpose que les 2 premi�res dimensions.
                        for k3=1:size(x,3)
                            for k4=1:size(x,4)
                                x.m.Data.x(:,:,k3,k4) = this.m.Data.x(:,:,k3,k4)';
                            end
                        end
                    case 5 % On ne transpose que les 2 premi�res dimensions.
                        for k3=1:size(x,3)
                            for k4=1:size(x,4)
                                for k5=1:size(x,5)
                                    x.m.Data.x(:,:,k3,k4,k5) = this.m.Data.x(:,:,k3,k4,k5)';
                                end
                            end
                        end
                    case 6 % On ne transpose que les 2 premi�res dimensions.
                        for k3=1:size(x,3)
                            for k4=1:size(x,4)
                                for k5=1:size(x,5)
                                    for k6=1:size(x,6)
                                        x.m.Data.x(:,:,k3,k4,k5,k6) = this.m.Data.x(:,:,k3,k4,k5,k6)';
                                    end
                                end
                            end
                        end
                    case 7 % On ne transpose que les 2 premi�res dimensions.
                        for k3=1:size(x,3)
                            for k4=1:size(x,4)
                                for k5=1:size(x,5)
                                    for k6=1:size(x,6)
                                        for k7=1:size(x,7)
                                            x.m.Data.x(:,:,k3,k4,k5,k6,k7) = this.m.Data.x(:,:,k3,k4,k5,k6,k7)';
                                        end
                                    end
                                end
                            end
                        end
                    case 8 % On ne transpose que les 2 premi�res dimensions.
                        for k3=1:size(x,3)
                            for k4=1:size(x,4)
                                for k5=1:size(x,5)
                                    for k6=1:size(x,6)
                                        for k7=1:size(x,7)
                                            for k8=1:size(x,8)
                                                x.m.Data.x(:,:,k3,k4,k5,k6,k7,k8) = this.m.Data.x(:,:,k3,k4,k5,k6,k7,k8)';
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    otherwise
                        fprintf('my_transpose : not yet written.\n');
                end
                this = x;
            end
        end % fonction transpose
    end % methods(Access='public', Access='public')


    methods(Access = 'private')
        function subCan = decode_sub(this, identDim, code)
            SZ = ones(1,8);
            SZ(1:length(this.Size)) = this.Size;
            if ischar(code) && strcmp(code, ':')
                subCan = 1:SZ(identDim);
            elseif isnumeric(code)
                subCan = code;
            else
                L = regexprep(code, 'end|/', num2str(SZ(identDim)));
                subCan = evalin('caller', L);
            end
        end % Fonction decode_sub


        function Value = getValue(this, varargin)
            if this.ErMapper
                switch length(varargin)
                    case 0
                        Value = this.m.Data.x(:,:,:,:,:,:,:,:)';
                    case 1
                        if length(this.Size) == 2
                            if islogical(varargin{1})
                                subOne = find(varargin{1});
                                [I,J] = ind2sub(this.Size, subOne);
                            else
                                [I,J] = ind2sub(this.Size, varargin{1});
                            end
                            IND = sub2ind([this.Size(2) this.Size(1)],J,I);
                        else
                            [I,J,K] = ind2sub(this.Size, varargin{1});
                            IND = sub2ind([this.Size(2) this.Size(1)],J,K,I);
                        end
                        Value = this.m.Data.x(IND);
                    case 2
                        Value = this.m.Data.x(varargin{2}, varargin{1});
                        Value = Value';
                    case 3
                        if length(this.Size) == 2
                            Value = this.m.Data.x(varargin{2}, varargin{1});
                            Value = Value';
                        else
                            subCan = decode_sub(this, 3, varargin{3});
                            for k=1:length(subCan)
                                X  = this.m.Data.x(varargin{2}, subCan(k), varargin{1});
                                Value(:,:,k) = (squeeze(X))'; %#ok<AGROW>
                            end
                        end
                end
            else
                pppp1 = this.Size;
                pppp2 = this.m.Format{2};
                if isequal(pppp1, fliplr(pppp2))
                    'ici';
                end
                if isequal(pppp1, pppp2) % TODO BugMatricesCarr�es
                    if isempty(varargin)
                        Value = this.m.Data.x(:,:,:,:,:,:);
                    else
                        Value = this.m.Data.x(varargin{:});
                    end
                    % Modif JMA le 20/05/2020 : ne pas faire la transpos�e
                    % pour donn�es issues du cache en Netcdf
                    % donn�es WC de "C:\MAYOBS13\ppppppppppppp\0034_20200505_063953_FG_EM122.wcd" en cache Netcdf ^mais pas en cache XMLBin)
                    if (length(pppp1) == 2) && (pppp1(1) == pppp1(2))
                        Value = Value'; % Pose un pb si la matrice est carr�e, on devrait passer dans la boucle suivante mais pas � tous les coups : c'est le bordel !
                        % Ca fonctionne pour les fichiers 0273_20130510_031703_EM122.all et 0273_20130510_031703_EM122.all.wcd
                        % Ceci est un platre sur une jambe de bois, il faut
                        % trouver l'origine du probl�me (this.Size == this.m.Format{2})
                    end
                else
                    nbDim = length(varargin);
                    if (nbDim > 2) && (length(this.Size) == 2)
                        nbDim = 2;
                    end
                    switch nbDim
                        case 0
                            Value = this.m.Data.x(:);
                            Value = Value';
                        case 1
                            Value = this.m.Data.x(varargin{1});
                            Value = Value';
                        case 2
                            Value = this.m.Data.x(varargin{2}, varargin{1});
                            Value = Value';
                        otherwise
                            Value = this.m.Data.x(varargin{2}, varargin{1}, varargin{3:end});
                            Value = my_transpose(Value);
                    end
                end
            end

            if ~isnan(this.ValNaN)
                subNaN = (Value == this.ValNaN);
                if ~isempty(subNaN)
                    switch class(Value)
                        case 'double'
                            Value(subNaN) = NaN;
                        otherwise
                            Value = single(Value);
                            Value(subNaN) = NaN;
                    end
                end
            end
        end % Fonction getValue


        function this = setValue(this, Value, varargin)

            % ---------------------------------------------------
            % Check if the file is writable

            if ~this.m.Writable
                % In case the file is not writable one copies the file in a
                % new one and one modifies the values in this new file

                Name = [my_tempname '.memmapfile'];

                str = ['Saving original file on tmp file : ' this.FileName ' --> ' Name];
                disp(str)
                if ~my_isdeployed
                    printStack(this, dbstack)
                end


                copyfile(this.FileName, Name);

                this.FileName = Name;

                this.m = memmapfile(Name, ...
                    'Format', this.m.Format, ...
                    'Writable', true);
                this.Writable = 1;
            end

            % No Data processing

            if ~isnan(this.ValNaN)
                subNaN = find(isnan(Value));
                if ~isempty(subNaN)
                    switch class(Value)
                        case 'double'
                            Value(subNaN) = this.ValNaN;
                        otherwise
                            Value = single(Value);
                            Value(subNaN) = this.ValNaN;
                    end
                end
            end

            % Set values

            if this.ErMapper
                switch length(varargin)
                    case 1
                        sz = size(this.m.Data.x);
                        switch sum(sz ~= 1)
                            case 1
                                this.m.Data.x(varargin{1}) = Value;
                            case 2
                                if islogical(varargin{1})
                                    subOne =  find(varargin{1});
                                    [I1,I2] = ind2sub([sz(2) sz(1)], subOne);
                                else
                                    [I1,I2] = ind2sub([sz(2) sz(1)], varargin{1});
                                end
                                I1 = sub2ind(sz,I2,I1);
                                this.m.Data.x(I1) = Value;
                            case 3
                                [I1,I2, I3] = ind2sub([sz(2) sz(1) sz(3)], varargin{1});
                                I1 = sub2ind(sz,I2,I1,I3);
                                this.m.Data.x(I1) = Value;
                        end
                    case 2
                        Value = Value';
                        this.m.Data.x(varargin{2}, varargin{1}) = Value;
                    case 3
                        if length(this.Size) == 2
                            Value = Value';
                            this.m.Data.x(varargin{2}, varargin{1}) = Value;
                        else
                            subCan = decode_sub(this, 3, varargin{3});
                            subCol = decode_sub(this, 2, varargin{2});
                            subLig = decode_sub(this, 3, varargin{1});
                            for k=1:length(subCan)
                                X =  Value(:,:,k);
                                this.m.Data.x(subCol, subCan(k), subLig) = X';
                            end
                        end
                end
            else
                try
                    pppp1 = this.Size;
                    pppp2 = size(this.m.Data.x);
                    %  pppp2 = this.m.Format{2}; % Pr�f�rable apparemment car this.m.Data.x lit tout le fichier
                    if isequal(pppp1, pppp2)
                        if (length(pppp1) == 2) && (pppp1(1) == pppp1(2))
                            % pppp1;
                            % Value = Value'; % Pose peut-�tre un pb si la matrice est carr�e, on devrait passer dans la boucle suivante mais pas � tous les coups : c'est le bordel !
                            % Ca fonctionne pour les fichiers 0273_20130510_031703_EM122.all et 0273_20130510_031703_EM122.all.wcd
                            % Ceci est un platre sur une jambe de bois, il faut
                            % trouver l'origine du probl�me (this.size == this.m.Format{2})
                        end
                        this.m.Data.x(varargin{:}) = Value;
                    else
                        switch length(varargin)
                            case 0
                                Value = Value';
                                this.m.Data.x(:) = Value;
                            case 1
                                Value = Value';
                                this.m.Data.x(varargin{1}) = Value;
                            case 2
                                Value = Value';
                                this.m.Data.x(varargin{2}, varargin{1}) = Value;
                            otherwise
                                Value = my_transpose(Value);
                                this.m.Data.x(varargin{2}, varargin{1}, varargin{3:end}) = Value;
                        end
                    end

                catch %#ok<CTCH>
                    logFileId = getLogFileId;
                    logFileId.error('cl_memmapfile/setValue', 'failure');
                end
            end
        end % fonction setValue


        function addFileToBeDestroyedOnListe(~, nomFic)
            nomFicTemp = fullfile(my_tempdir, 'ListeFicMemmapfileToDelete.dat');
            for k=1:100
                fid = fopen(nomFicTemp, 'a');
                if fid == -1
                    pause(0.1)
                else
                    break
                end
            end
            if fid == -1
                logFileId = getLogFileId;
                logFileId.error('cl_memmapfile', 'addFileToBeDestroyedOnListe fopen failure');
                return
            end
            GCBF = gcbf;
            if isempty(GCBF)
                fprintf(fid, '%f %s\n', -1, nomFic);
            else
                fprintf(fid, '%f %s\n', GCBF, nomFic);
            end
            fclose(fid);
        end
    end % methods(Access = 'private', Access = 'private')
end
