% Create a variable in memmory mapping
%
% Syntax
%   X = cl_memmapfileNew(...)
%
% Name-Value Pair Arguments
%   FileName     : File name (one is created by default)
%   Value        : Matrix to be mapped on a file
%   Size         : Size of the matrix to be mapped
%   Format       : Data storage type : uint8, int8, ...(default : 'double')
%   ValNaN       : Value to be used for NoData (to be used if Format is integer)
%   FileCreation : 1=Create a file, 0=mapping on an existing file
%
% Name-only Arguments
%   ChangeFileName : Change the file name
%
% Output Arguments
%   X : Variable
%
% Remarks : Use X(:,:) each time you want to use the values outside a
% cl_memmapfileNew method
%
% Examples
%   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
%   m = cl_memmapfileNew('FileName', tempname, 'Value', [1 2 3 4; 5 6 7 8]);
%   get(m, 'FileName')
%   get(m, 'Value')
%   m
%   m(1)
%   m(1,2)
%   m([2 3 7 3])
%   m(1) = pi
%   m(1,2)
%   m(1, 2:3) = [10 11]
%   m(1:2,2) = [10; 11]
%   m(1,:)
%   m(:,1)
%   imagesc(m(1:2,1:4))
%   imagesc(m(:,:))
%   length(m)
%   size(m)
%   min(m)
%   max(m)
%   clear m
%
%   Value = rand(2,3,4)
%   m = cl_memmapfileNew('Value', Value);
%   m
%   Value([1 7 13 14 19])
%   m([1 7 13 14 19])
%   m(1,1,1)
%   clear m
%
%   m = cl_memmapfileNew('Value', 0, 'Size', [2, 4]);
%   m(1) = pi
%   m([2 8]) = -5
%   m([2 8]) = [2 3]
%
%   N = 20000;
%   m = cl_memmapfileNew('Value', 0, 'Size', [N N], 'Format', 'single');
%   m(N,N) = 1;

% See also FormationMemmapfile Authors
% Authors : JMA, GLU
%-------------------------------------------------------------------------------

classdef cl_memmapfileNew < handle
    
    properties(GetAccess='public', SetAccess='public')
        FileName        = [];
        Writable        = 0;
        ValNaN          = NaN;
        m               = [];
        Size            = [0 0];
        Format          = 'double';
        DimOrder        = [];
    end
    
    methods(Access='public')
        function obj = cl_memmapfileNew(varargin)
            % ---------------------
            % Constructeur
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            % ---------------------
            if isempty(varargin{1})
                return
            end
            
            [varargin, Struct] = getPropertyValue(varargin, 'Struct', []);
            if ~isempty(Struct)
                obj = class(Struct, 'cl_memmapfileNew');
                return
            end
            
            % Look at if the instance was created only in order to reach a
            % method of the class Ex : MethodX(cl_memmapfileNew([]))
            % (this is done systematically on all my constructors)
            if (nargin == 1) && isempty(varargin{1})
                return
            end
            
            % Init the instance with the parameters passed when one creates
            % an instance
            [varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 0);
            obj = set(obj, varargin{:});
            
            if obj.Writable == 0
                % Writes some information on a log file, you can simply
                % comment these 7 lines or set LogSilence=true
                str = sprintf('Linking matrix "%s" [%s] mapped on %s', obj.Format, num2str(obj.Size), obj.FileName);

            else
                % Writes some information on a log file, you can simply
                % comment these 7 lines or set LogSilence=true
                str = sprintf('Creation matrix "%s" [%s] mapped on %s', obj.Format, num2str(obj.Size), obj.FileName);
            end
            
            if ~LogSilence
                disp('---------------------------------------------------------')
                disp(str)
                if ~isdeployed
                    printStack(obj, dbstack)
                end
            end
        end % End of constructor
        
        function printStack(obj, ST)%#ok
            % ---------------------
            % Fonction printStack
            % Examples
            %   printStack(m, dbstack)
            % ---------------------
            if ~isdeployed
                for i=1:length(ST)
                    fprintf('\tFunction : %s\t - line : %d\n', ST(i).name, ST(i).line);
                end
            end
        end % Fin de printStack
        
        function varargout = set(obj, varargin)
            %------------------------------------------------------------------
            % Set property values
            %
            % Syntax
            %   a = set(a, PN, PV)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfileNew
            %   PN : Property name
            %   PV : Property value
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = set(m, 'Writable', 1)
            %------------------------------------------------------------------
            
            % ----------------------------------------------------------
            % Read or initialyse Property values
            [varargin, ChangeFileName] = getFlag(varargin, 'ChangeFileName');
            
            [varargin, HereFileName]   = getPropertyValue(varargin, 'FileName',     []);
            [varargin, Value]          = getPropertyValue(varargin, 'Value',        []);
            [varargin, HereSize]       = getPropertyValue(varargin, 'Size',         []);
            [varargin, HereFormat]     = getPropertyValue(varargin, 'Format',       []);
            [varargin, FileCreation]   = getPropertyValue(varargin, 'FileCreation', 1);
            [varargin, obj.ValNaN]     = getPropertyValue(varargin, 'ValNaN',       obj.ValNaN);
            [varargin, HereWritable]   = getPropertyValue(varargin, 'Writable',     []);
            [varargin, HereDimOrder]   = getPropertyValue(varargin, 'DimOrder',     [1 2 3]);
            
            if ~isempty(HereFileName)
                obj.FileName = HereFileName;
            end
            
            if ChangeFileName
                obj.m.Filename = HereFileName;
                obj.Writable = 1;
                obj.m.Writable = true;
                varargout{1} = obj;
                return
            end
            if ~isempty(HereWritable)
                obj.Writable = HereWritable;
                if HereWritable
                    obj.m.Writable = true;
                else
                    obj.m.Writable = false;
                end
                if isempty(varargin)
                    varargout{1} = obj;
                    return
                end
            end
            obj.DimOrder = HereDimOrder;
            
            
            if FileCreation
                if isa(Value, 'cl_memmapfileNew')
                    Value = get(Value, 'Value');
                end
                
                % Garbage collector
                %     garbage_collector
                
                if isempty(HereSize) && ~isempty(Value)
                    obj.Size = size(Value);
                    
                    % Appel sous la forme : m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
                    
                    if isempty(obj.FileName)
                        Name = my_tempname;
                        [pathName, Name] = fileparts(Name);
                        obj.FileName = fullfile(pathName, [Name '.memmapfile']);
                    end
                    
                    if isempty(HereFormat)
                        HereFormat = class(Value(1));
                    end
                    
                    obj.m = [];
                    fid = fopen(obj.FileName, 'w+');
                    if fid == -1
                        obj = Value;
                    end
                    
                    count = fwrite(fid, Value, HereFormat);
                    if count ~= numel(Value)
                        fclose(fid);
                        my_warndlg('cl_memmapfileNew/set writing problems', 1);
                        varargout{1} = obj;
                        return
                    end
                    fclose(fid);
                    
                    % Call this way : m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]); size(V) = [4 2]
                    if strcmp(HereFormat, 'logical')
                        HereFormat = 'uint8';
                    end
                    if strcmp(HereFormat, 'char')
                        HereFormat = 'uint8';
                    end
                    
                    sz = size(Value);
                    if isequal(obj.DimOrder, [1 2 3])
                        obj.m = memmapfile(obj.FileName, ...
                            'Format', {HereFormat sz  'x'}, ...
                            'Writable', true);
                    elseif isequal(obj.DimOrder, [2 1 3])
                        obj.m = memmapfile(obj.FileName, ...
                            'Format', {HereFormat [sz(2) sz(1) sz(3:end)] 'x'}, ...
                            'Writable', true);
                    elseif isequal(obj.DimOrder, [2 3 1])
                        obj.m = memmapfile(obj.FileName, ...
                            'Format', {HereFormat sz([2 3 1]) 'x'}, ...
                            'Writable', true);
                    end
                    obj.Writable = 1;
                    
                elseif ~isempty(HereSize) && (length(Value) <= 1)
                    % Call this way : m = cl_memmapfileNew('Value', NaN, 'Size', [100 200]);
                    if (length(HereSize) == 3) && (HereSize(3) == 1)
                        HereSize = HereSize(1:2);
                    end
                    obj.Size = double(HereSize);
                    
                    if isempty(Value)
                        Value = 0;
                    end
                    
                    if isempty(obj.FileName)
                        Name = my_tempname;
                        [pathName, Name] = fileparts(Name);
                        obj.FileName = fullfile(pathName, [Name '.memmapfile']);
                    end
                    
                    if isempty(HereFormat)
                        HereFormat = 'single';
                    end
                    
                    obj.m = [];
                    fid = fopen(obj.FileName, 'w+');
                    if fid == -1
                        my_warndlg('cl_memmapfileNew/set - does not work here', 1);
                        varargout{1} = [];
                    end
                    
                    if numel(Value) == 1
                        Nb = min(prod(obj.Size), 10000);
                        Value = repmat(Value, 1, Nb);
                        N = floor(prod(obj.Size) / Nb);
                        for i=1:N
                            count = fwrite(fid, Value, HereFormat);
                            if count ~= Nb
                                fclose(fid);
                                my_warndlg('cl_memmapfileNew/set writing failure', 1);
                                varargout{1} = obj;
                                return
                            end
                        end
                        Nb = prod(obj.Size) - N*Nb;
                        if Nb > 0
                            Value = repmat(Value(1), 1, Nb);
                            count = fwrite(fid, Value, HereFormat);
                            if count ~= Nb
                                fclose(fid);
                                my_warndlg('cl_memmapfileNew/set Pb d''ecriture', 1);
                                varargout{1} = obj;
                                return
                            end
                        end
                    else
                        count = fwrite(fid, Value, HereFormat);
                        if count ~= prod(obj.Size)
                            fclose(fid);
                            my_warndlg('cl_memmapfileNew/set Pb d''ecriture', 1);
                            varargout{1} = obj;
                            return
                        end
                    end
                    fclose(fid);
                    
                    
                    obj.m = memmapfile(obj.FileName, ...
                        'Format', {HereFormat obj.Size 'x'}, ...
                        'Writable', true, 'Repeat', 1);
                    obj.Writable = 1;
                    
                else
                    if (length(HereSize) == 3) && (HereSize(3) == 1)
                        HereSize = HereSize(1:2);
                    end
                    obj.Size = HereSize;
                    obj.m = memmapfile(obj.FileName, ...
                        'Format', {obj.Format obj.Size 'x'}, ...
                        'Writable', false, 'Repeat', 1);
                    obj.Writable = 0;
                end
                
            else % Cas de l'ouverture d'un fichier DimOrder en lecture
                if (length(HereSize) == 3) && (HereSize(3) == 1)
                    HereSize = HereSize(1:2);
                end
                obj.Size = HereSize;
                if isequal(obj.DimOrder, [2 3 1])
                    obj.m = memmapfile(obj.FileName, ...
                        'Format', {HereFormat [HereSize(2) HereSize(3) HereSize(1)] 'x'}, ...
                        'Writable', false, 'Repeat', 1);
                elseif isequal(obj.DimOrder, [2 1 3])
                    obj.m = memmapfile(obj.FileName, ...
                        'Format', {HereFormat [HereSize(2) HereSize(1) HereSize(3)] 'x'}, ...
                        'Writable', false, 'Repeat', 1);
                elseif isequal(obj.DimOrder, [1 2 3])
                    obj.m = memmapfile(obj.FileName, ...
                        'Format', {HereFormat [HereSize(1) HereSize(2) HereSize(3)] 'x'}, ...
                        'Writable', false, 'Repeat', 1);
                else
                    disp('beurk')
                end
                obj.Writable = 0;
                obj.Format = HereFormat;
                
                % Not anymore usefull since we have the destructor
                % addFileToBeDestroyed(obj.FileName)
            end
            
            obj.Format = HereFormat;
            
            
            % Check if varargin is empty
            if ~isempty(varargin)
                str = sprintf('%s is not a property', num2str(varargin{1}));
                my_warndlg(['cl_memmapfileNew/set - ' str], 1);
            end
            
            % Bye bye
            if nargout == 0
                assignin('caller', inputname(1), obj);
            else
                varargout{1} = obj;
            end
            
        end % End of set
        
        function display(obj)
            % Display
            %
            % Syntax
            %   display(a)
            %
            % Input Arguments
            %   a : cl_memmapfileNew instance
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   m
            %
            % See also cl_memmapfileNew Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            disp(getValue(obj))
            
        end % End of display
        
        function x = eq(a, b)
            % Isequal overloading
            %
            % Syntax
            %   c = a == b
            %
            % Input Arguments
            %   a  : cl_memmapfileNew instance or scalar
            %   b  : cl_memmapfileNew instance or scalar
            %
            % Output Arguments
            %   c : Boolean matrix
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfileNew('Value', [2 6 1 3; 5 4 8 7]);
            %   m == n
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            
            flaga = isa(a, 'cl_memmapfileNew');
            flagb = isa(b, 'cl_memmapfileNew');
            
            if flaga && flagb
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            x(i,:,k) = (getValue(a, i, j ,k) ==  getValue(b, i, j ,k));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        x(i,:) =  (getValue(a, i, j) ==  getValue(b, i, j));
                    end
                end
                
            elseif flaga
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            if length(b) == 1
                                x(i,:,k) =  getValue(a, i, j ,k) ==  b;
                            else
                                x(i,:,k) =  getValue(a, i, j ,k) ==  b(i,:,k);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        if length(b) == 1
                            x(i,:) =  getValue(a, i, j) ==  b;
                        else
                            x(i,:) =  getValue(a, i, j) ==  b(i,:);
                        end
                    end
                end
            else
                j = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k=1:b.size(3)
                        for i=1:b.size(1)
                            if length(a) == 1
                                x(i,:,k) =  getValue(b, i, j ,k) ==  a;
                            else
                                x(i,:,k) =  getValue(b, i, j ,k) ==  a(i,:,k);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for i=1:b.size(1)
                        if length(a) == 1
                            x(i,:) =  getValue(b, i, j) ==  a;
                        else
                            x(i,:) =  getValue(b, i, j) ==  a(i,:);
                        end
                    end
                end
            end
            
        end % Enf method eq
        
        function PV = get(obj, PN)
            % get a property value
            %
            % Syntax
            %   PV = get(a, PN)
            %
            % Input Arguments
            %   a  : cl_memmapfileNew instance
            %   PN : property name
            %
            % Output Arguments
            %   PV : Property value
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   FileName = get(m, 'FileName')
            %   Writable = get(m, 'Writable')
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            switch PN
                case 'FileName'
                    PV = obj.FileName;
                    
                case 'Value'
                    PV = getValue(obj);
                    
                case 'Writable'
                    PV = obj.Writable;
                    
                otherwise
                    PV = [];
                    str = sprintf('cl_memapfileNew/get %s : this property name doesn not exist', PV);
                    my_warndlg(['cl_image:get ', str], 0);
            end
            
        end % End method get
        
        function x = gt(a, b)
            % Overload ">" operator
            %
            % Syntax
            %   c = a > b
            %
            % Input Arguments
            %   a : cl_memmapfileNew instance
            %   b : cl_memmapfileNew instance
            %
            % Output Arguments
            %   c : Boolean matrix
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfileNew('Value', [2 6 1 3; 5 4 8 7]);
            %   m > n
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            
            flaga = isa(a, 'cl_memmapfileNew');
            flagb = isa(b, 'cl_memmapfileNew');
            
            if flaga && flagb
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            x(i,:,k) = (getValue(a, i, j ,k) >  getValue(b, i, j ,k));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        x(i,:) =  (getValue(a, i, j) >  getValue(b, i, j));
                    end
                end
                
            elseif flaga
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            if length(b) == 1
                                x(i,:,k) =  getValue(a, i, j ,k) >  b;
                            else
                                x(i,:,k) =  getValue(a, i, j ,k) >  b(i,:,k);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        if length(b) == 1
                            x(i,:) =  getValue(a, i, j) >  b;
                        else
                            x(i,:) =  getValue(a, i, j) >  b(i,:);
                        end
                    end
                end
            else
                j = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k=1:b.size(3)
                        for i=1:b.size(1)
                            if length(a) == 1
                                x(i,:,k) =  getValue(b, i, j ,k) >  a;
                            else
                                x(i,:,k) =  getValue(b, i, j ,k) >  a(i,:,k);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for i=1:b.size(1)
                        if length(a) == 1
                            x(i,:) =  getValue(b, i, j) >  a;
                        else
                            x(i,:) =  getValue(b, i, j) >  a(i,:);
                        end
                    end
                end
            end
            
        end % End method gt
        
        function flag = isnan(obj)
            % Test de valeur NaN d'une matrice en memory-mapping
            %
            % Syntax
            %   flag = isnan(a)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfileNew
            %
            % Output Arguments
            %   flag : Matrice de bool�ens
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   flag = isnan(m)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            j = 1:obj.Size(2);
            flag = 1==0;
            if length(obj.Size) == 3
                flag(obj.Size(1), obj.Size(2), obj.Size(3)) = 0;
                for k=1:obj.Size(3)
                    for i=1:obj.Size(1)
                        flag(i,:,k) = isnan(getValue(obj, i, j ,k));
                    end
                end
            else
                flag(obj.Size(1), obj.Size(2)) = 0;
                for i=1:obj.Size(1)
                    flag(i,:) =  isnan(getValue(obj, i, j));
                end
            end
            
        end % isnan
        
        function x = le(a, b)
            % Test "<=" entre une d'une matrice en memory-mapping et une valeur ou
            % entre deux matrices
            %
            % Syntax
            %   c = a <= b
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfileNew ou scalaire
            %   b  : Instance de cl_memmapfileNew ou scalaire
            %
            % Output Arguments
            %   c : Matrice de boul�ens
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfileNew('Value', [2 6 1 3; 5 4 8 7]);
            %   m <= n
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            flaga = isa(a, 'cl_memmapfileNew');
            flagb = isa(b, 'cl_memmapfileNew');
            
            if flaga && flagb
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            x(i,:,k) = (getValue(a, i, j ,k) <=  getValue(b, i, j ,k));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        x(i,:) =  (getValue(a, i, j) <=  getValue(b, i, j));
                    end
                end
                
            elseif flaga
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            if length(b) == 1
                                x(i,:,k) =  getValue(a, i, j ,k) <=  b;
                            else
                                x(i,:,k) =  getValue(a, i, j ,k) <=  b(i,:,k);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        if length(b) == 1
                            x(i,:) =  getValue(a, i, j) <=  b;
                        else
                            x(i,:) =  getValue(a, i, j) <=  b(i,:);
                        end
                    end
                end
            else
                j = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k=1:b.size(3)
                        for i=1:b.size(1)
                            if length(a) == 1
                                x(i,:,k) =  getValue(b, i, j ,k) <=  a;
                            else
                                x(i,:,k) =  getValue(b, i, j ,k) <=  a(i,:,k);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for i=1:b.size(1)
                        if length(a) == 1
                            x(i,:) =  getValue(b, i, j) <=  a;
                        else
                            x(i,:) =  getValue(b, i, j) <=  a(i,:);
                        end
                    end
                end
            end
            
        end % Fonction le
        
        function l = length(obj)
            % Dimension maximale d'une matrice en memory-mapping
            %
            % Input Arguments
            %   a : Instance de cl_memmapfileNew
            %
            % Syntax
            %   n = length(a)
            %
            % Output Arguments
            %   n : Taille maximale d'une matrice
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = length(m)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            l = max(obj.Size);
            
        end % Fonction length
        
        function x = lt(a, b)
            % Test "<" entre une d'une matrice en memory-mapping et une valeur ou
            % entre deux matrices
            %
            % Syntax
            %   c = a < b
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfileNew ou scalaire
            %   b  : Instance de cl_memmapfileNew ou scalaire
            %
            % Output Arguments
            %   c : Matrice de boul�ens
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfileNew('Value', [2 6 1 3; 5 4 8 7]);
            %   m < n
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            
            flaga = isa(a, 'cl_memmapfileNew');
            flagb = isa(b, 'cl_memmapfileNew');
            
            if flaga && flagb
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            x(i,:,k) = (getValue(a, i, j ,k) <  getValue(b, i, j ,k));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        x(i,:) =  (getValue(a, i, j) <  getValue(b, i, j));
                    end
                end
                
            elseif flaga
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            if length(b) == 1
                                x(i,:,k) =  getValue(a, i, j ,k) <  b;
                            else
                                x(i,:,k) =  getValue(a, i, j ,k) <  b(i,:,k);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        if length(b) == 1
                            x(i,:) =  getValue(a, i, j) <  b;
                        else
                            x(i,:) =  getValue(a, i, j) <  b(i,:);
                        end
                    end
                end
            else
                j = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k=1:b.size(3)
                        for i=1:b.size(1)
                            if length(a) == 1
                                x(i,:,k) =  getValue(b, i, j ,k) <  a;
                            else
                                x(i,:,k) =  getValue(b, i, j ,k) <  a(i,:,k);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for i=1:b.size(1)
                        if length(a) == 1
                            x(i,:) =  getValue(b, i, j) <  a;
                        else
                            x(i,:) =  getValue(b, i, j) <  a(i,:);
                        end
                    end
                end
            end
            
        end % Fonction lt
        
        function x = max(obj)
            % Valeur max d'une matrice en memory-mapping
            %
            % Syntax
            %   x = max(a)
            %
            % Input Arguments
            %   a : Instance de cl_memmapfileNew
            %
            % Output Arguments
            %   X : Valeur maximale de la matrice
            %
            % Remarks : This is not exactly the max matlab behaviour, this
            % is equivalent to max(a(:))
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   x = max(m)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            
            % WARNING, this is a minimalist adaptation of matlab max
            % function : should be improved
            x = max(obj.m.Data.x(:));
            
        end % Fonction max
        
        function x = min(obj, varargin)
            % Valeur min d'une matrice en memory-mapping
            %
            % Syntax
            %   x = min(a)
            %
            % Input Arguments
            %   a : Instance de cl_memmapfileNew
            %
            % Output Arguments
            %   X : Valeur minimale de la matrice
            %
            % Remarks : Cette fonction est une adaptation minimaliste de la fonction min de
            %           MATLAB, elle est �quivalente � min(a(:))
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   x = min(m)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            % ATTENTION, ceci est une adaptation minimaliste de la fonction min de
            % MATLAB
            x = min(obj.m.Data.x(:));
        end % Fonction min
        
        function x = ne(a, b)
            % Test "~=" entre une d'une matrice en memory-mapping et une valeur ou
            % entre deux matrices
            %
            % Syntax
            %   c = a ~= b
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfileNew ou scalaire
            %   b  : Instance de cl_memmapfileNew ou scalaire
            %
            % Output Arguments
            %   c : Matrice de boul�ens
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = cl_memmapfileNew('Value', [2 6 1 3; 5 4 8 7]);
            %   m ~= n
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            flaga = isa(a, 'cl_memmapfileNew');
            flagb = isa(b, 'cl_memmapfileNew');
            
            if flaga && flagb
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            x(i,:,k) = (getValue(a, i, j ,k) ~=  getValue(b, i, j ,k));
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        x(i,:) =  (getValue(a, i, j) ~=  getValue(b, i, j));
                    end
                end
                
            elseif flaga
                j = 1:a.size(2);
                x = 1==0;
                if length(a.size) == 3
                    x(a.size(1), a.size(2), a.size(3)) = 0;
                    for k=1:a.size(3)
                        for i=1:a.size(1)
                            if length(b) == 1
                                x(i,:,k) =  getValue(a, i, j ,k) ~=  b;
                            else
                                x(i,:,k) =  getValue(a, i, j ,k) ~=  b(i,:,k);
                            end
                        end
                    end
                else
                    x(a.size(1), a.size(2)) = 0;
                    for i=1:a.size(1)
                        if length(b) == 1
                            x(i,:) =  getValue(a, i, j) ~=  b;
                        else
                            x(i,:) =  getValue(a, i, j) ~=  b(i,:);
                        end
                    end
                end
            else
                j = 1:b.size(2);
                x = 1==0;
                if length(b.size) == 3
                    x(b.size(1), b.size(2), b.size(3)) = 0;
                    for k=1:b.size(3)
                        for i=1:b.size(1)
                            if length(a) == 1
                                x(i,:,k) =  getValue(b, i, j ,k) ~=  a;
                            else
                                x(i,:,k) =  getValue(b, i, j ,k) ~=  a(i,:,k);
                            end
                        end
                    end
                else
                    x(b.size(1), b.size(2)) = 0;
                    for i=1:b.size(1)
                        if length(a) == 1
                            x(i,:) =  getValue(b, i, j) ~=  a;
                        else
                            x(i,:) =  getValue(b, i, j) ~=  a(i,:);
                        end
                    end
                end
            end
            
        end % Fonction ne
        
        function n = numel(obj, varargin)
            % Nombre de valeurs d'une matrice en memory-mapping
            %
            % Input Arguments
            %   a : Instance de cl_memmapfileNew
            %
            % Syntax
            %   x = numel(a)
            %
            % Output Arguments
            %   X : Nombre de valeurs de la matrice
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   n = numel(m)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            sz = size(obj, varargin{:});
            n = prod(sz); 
            
        end % Fonction numel
        
        function obj = set_ErMapperOnly(obj, HereDimOrder)
            % Method to be used in case the loaded matrix is common image
            % order (Er-Mapper order)
            %
            % Syntax
            %   a = set_ErMapperOnly(a, HereDimOrder)
            %
            % Input Arguments
            %   a        : Instance de cl_memmapfileNew
            %   HereDimOrder : 0=Matrice Matlab, 1=Matrice HereDimOrder
            %
            % Output Arguments
            %   a        : Instance de cl_memmapfileNew
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8])
            %   m = set_ErMapperOnly(m, true)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            obj.DimOrder = HereDimOrder;
            SZ = obj.m.Format{2};
            if length(SZ) == 2
                obj.m.Format{2} = [SZ(2) SZ(1)];
            else
                obj.m.Format{2} = [SZ(3) SZ(2) SZ(1)] ;
            end
            
        end % End of set_ErMapperOnly
        
        function varargout = size(obj, varargin)
            % Taille d'une matrice en memory-mapping
            %
            % Syntax
            %   [...] = size(a, ...)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfileNew
            %
            % Name-Value Pair Arguments
            %   dim : Numero de la dimension demand�e
            %
            % Output Arguments
            %   sz : Taille de la matrice ou taille de la dimension demand�e
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8])
            %   size(m)
            %   size(m,1)
            %   size(m,2)
            %   size(m,3)
            %   [R,C] = size(m)
            %  [ R,C,N] = size(m)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            sz = obj.Size;
            
            if (length(sz) >=3) && all(sz(3:end) == 1)
                sz = sz(1:2);
            end
            
            if nargin > 1
                n = varargin{1};
                if n > length(sz)
                    sz = 1;
                else
                    sz = sz(n);
                end
            end
            
            switch nargout
                case {0;1}
                    varargout{1} = sz;
                otherwise
                    for i=1:nargout
                        if i > length(sz)
                            varargout{i} = 1; %#ok<AGROW>
                        else
                            varargout{i} = sz(i); %#ok<AGROW>
                        end
                    end
            end
            
        end % Fonction size
        
        function obj = subsasgn(obj, theStruct, Value)
            % Acc�s en �critude des �l�ments d'une matrice en memory-mapping
            %
            % Syntax
            %   a = subsref(a, theStruct, Value)
            %
            % Input Arguments
            %   a         : Instance de cl_memmapfileNew
            %   theStruct : (sub) ou (sub1, sub2) ou (sub1, sub2, sub3)
            %   Value : Valeurs demand�es
            %
            % Output Arguments
            %   a : Instance de cl_memmapfileNew
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   m(1)
            %   m(1,2)
            %   m([2 3 7 3])
            %   m(1,:)
            %   m(:,2)
            %   m(1, 2:3) = [10 11]
            %   m(1:2,2) = [10; 11]
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            
            theSubs = theStruct(1).subs;
            obj = setValue(obj, Value, theSubs{:});
            
        end % Fonction subasgn
        
        function Value = subsref(obj, theStruct)
            % Acc�s en lecture des �l�ments d'une matrice en memory-mapping
            %
            % Syntax
            %   Value = subsref(a, theStruct)
            %
            % Input Arguments
            %   a         : Instance de cl_memmapfileNew
            %   theStruct : (sub) ou (sub1, sub2) ou (sub1, sub2, sub3)
            %
            % Output Arguments
            %   Value : Valeurs demand�es
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   m(1)
            %   m(1,2)
            %   m([2 3 7 3])
            %   m(1,:)
            %   m(:,2)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %-------------------------------------------------------------------------------
            theSubs = theStruct(1).subs;
            if (length(theSubs) == 1) && ischar(theSubs{1}) && strcmp(theSubs{1}, ':')
                theSubs{2} = ':';
                flagVecteur = 1;
            else
                sz = obj.Size;
                %                 sz(1:2) = fliplr(sz(1:2));
                for k=1:length(theSubs)
                    if ischar(theSubs{k}) && strcmp(theSubs{k}, ':')
                        if length(sz) >= k
                            theSubs{k} = 1:sz(k);
                        else
                            theSubs{k} = 1;
                        end
                    end
                end
                flagVecteur = 0;
            end
            
            Value = getValue(obj, theSubs{:});
            
            if flagVecteur
                Value = Value(:);
            end
            
        end % Fonction subsref
        
        function x = uint8(obj)
            % Conversion d'une matrice en memory-mapping en uint8
            %
            % Syntax
            %   x = uint8(a)
            %
            % Input Arguments
            %   a  : Instance de cl_memmapfileNew
            %
            % Output Arguments
            %   x : Valeur de la matrice en uint8
            %
            % Examples
            %   m = cl_memmapfileNew('Value', [1 2 3 4; 5 6 7 8]);
            %   x = uint8(m)
            %
            % See also cl_memmapfileNew FormationMemmapfile Authors
            % Authors : JMA
            % VERSION    :
            %--------------------------------------------------------------------------
            x = zeros(obj.Size, 'uint8');
            j = 1:obj.Size(2);
            if length(obj.Size) == 2
                for i=1:obj.Size(1)
                    x(i,:) = uint8(getValue(obj, i, j));
                end
            else
                for k=1:obj.Size(3)
                    for i=1:obj.Size(1)
                        x(i,:,k) = uint8(getValue(obj, i, j ,k));
                    end
                end
            end
            
        end % Fonction uint8
        
        function delete(obj)
            % -------------------------------------------------------------
            % Destructeur appel� lors de la destruction de l'instance de l'objet
            % par clear obj.
            % Examples
            %   clear m;
            % -------------------------------------------------------------
            try
                if isempty(obj)
                    return
                end
                nomFic = obj.FileName;
                if isempty(nomFic)
                    return
                end
                
                % On verifie si c'est bien un fichier avec l'extension
                % ".memmapfile" car in ne faudrait pas detruire des fichiers
                % DimOrder
                
                [Dir, Name, Ext] = fileparts(nomFic); %#ok<ASGLU>
                if ~strcmp(Ext, '.memmapfile')
                    return
                end
                
                if exist(nomFic, 'file')
                    %                     obj = set(obj, 'writable', 0);
                    S = recycle;
                    recycle('Off')
                    delete(nomFic);
                    recycle(S)
                    
                    if exist(nomFic, 'file')
                        fprintf('It was impossible to delete %s\n', nomFic);
                        %                         addFileToBeDestroyedOnListe(obj, nomFic)
                    end
                end
                
            catch ME
                ME.stack
                my_warndlg('Echec dans la destruction du FileID de l'' object cl_memmapfileNew', 0);
            end
        end % Fonction Delete
    end
    
    
    
    
    methods(Access='private')
        function subCan = decode_sub(obj, identDim, code)
            SZ = ones(1,8);
            SZ(1:length(obj.Size)) = obj.Size;
            if ischar(code) && strcmp(code, ':')
                subCan = 1:SZ(identDim);
            elseif isnumeric(code)
                subCan = code;
            else
                L = regexprep(code, 'end|/', num2str(SZ(identDim)));
                subCan = evalin('caller', L);
            end
            
        end % Fonction decode_sub
        
        function Value = getValue(obj, varargin)
            if isequal(obj.DimOrder, [1 2 3])
                Value = obj.m.Data.x(varargin{:});
            elseif isequal(obj.DimOrder, [2 1 3])
                if isempty(varargin)
                    subx = 1:obj.Size(2);
                    suby = 1:obj.Size(1);
                    sub3 = 1:obj.Size(3);
                    Value = obj.m.Data.x(subx, suby, sub3);
                    Value = reshape(Value, obj.Size);
                else
                    if islogical(varargin{1})
                        switch length(varargin)
                            case 0
                                Value = obj.m.Data.x;
                            case 1
                                pppp = varargin{1};
                                pppp= reshape(my_transpose(pppp), size(varargin{1}));
                                Value = obj.m.Data.x(pppp);
                            case 2
                                % Pas encore v�rifi�
                                pppp = varargin{1};
                                pppp= reshape(my_transpose(pppp), size(varargin{1}));
                                Value = obj.m.Data.x(pppp);
                            otherwise
                                % Pas encore v�rifi�
                                pppp = varargin{1};
                                pppp= reshape(my_transpose(pppp), size(varargin{1}));
                                Value = obj.m.Data.x(pppp);
                        end
                    else
                        switch length(varargin)
                            case 0
                                Value = obj.m.Data.x;
                                Value = my_transpose(Value);
                            case 1
                                [I,J] = ind2sub(obj.Size, varargin{1});
                                IND = sub2ind([obj.Size(2) obj.Size(1)],J,I);
                                Value = obj.m.Data.x(IND);
                                Value = reshape(Value, size(varargin{1}));
                            case 2
                                Value = obj.m.Data.x(varargin{2}, varargin{1});
                                Value = my_transpose(Value);
                            otherwise
                                Value = obj.m.Data.x(varargin{2}, varargin{1}, varargin{3:end});
                                Value = my_transpose(Value);
                        end
                    end
                end
            elseif isequal(obj.DimOrder, [2 3 1])
                Value = obj.m.Data.x(varargin{:});
            else
                disp('beurk')
            end
            
            %{
if obj.DimOrder
switch length(varargin)
case 0
Value = obj.m.Data.x(:,:,:,:,:,:,:,:)';
case 1
if length(obj.Size) == 2
if islogical(varargin{1})
subOne = find(varargin{1});
[I,J] = ind2sub(obj.Size, subOne);
else
[I,J] = ind2sub(obj.Size, varargin{1});
end
IND = sub2ind([obj.Size(2) obj.Size(1)],J,I);
else
[I,J,K] = ind2sub(obj.Size, varargin{1});
IND = sub2ind([obj.Size(2) obj.Size(1)],J,K,I);
end
Value = obj.m.Data.x(IND);
case 2
Value = obj.m.Data.x(varargin{2}, varargin{1});
Value = Value';
case 3
if length(obj.Size) == 2
Value = obj.m.Data.x(varargin{2}, varargin{1});
Value = Value';
else
subCan = decode_sub(obj, 3, varargin{3});
for i=1:length(subCan)
X  = obj.m.Data.x(varargin{2}, subCan(i), varargin{1});
Value(:,:,i) = (squeeze(X))'; %#ok<AGROW>
end
end
end
else
pppp1 = obj.size;
pppp2 = size(obj.m.Data.x);
if isequal(pppp1, pppp2)
Value = obj.m.Data.x(varargin{:});
else
switch length(varargin)
case 0
Value = obj.m.Data.x(:);
Value = Value';
case 1
Value = obj.m.Data.x(varargin{1});
Value = Value';
case 2
Value = obj.m.Data.x(varargin{2}, varargin{1});
Value = Value';
otherwise
Value = obj.m.Data.x(varargin{2}, varargin{1}, varargin{3:end});
Value = my_transpose(Value);
end
end
end
            %}
            
            if ~isnan(obj.ValNaN)
                subNaN = (Value == obj.ValNaN);
                if ~isempty(subNaN)
                    switch class(Value)
                        case 'double'
                            Value(subNaN) = NaN;
                        otherwise
                            Value = single(Value);
                            Value(subNaN) = NaN;
                    end
                end
            end
            
            
        end % Fonction getValue
        
        function obj = setValue(obj, Value, varargin)
            
            % ---------------------------------------------------
            % Check if the file is writable
            
            if ~obj.m.Writable
                % In case the file is not writable one copies the file in a
                % new one and one modifies the values in this new file
                
                Name = [my_tempname '.memmapfile'];
                
                str = ['Saving original file on tmp file : ' obj.FileName ' --> ' Name];
                disp(str)
                if ~isdeployed
                    printStack(obj, dbstack)
                end
                
                
                copyfile(obj.FileName, Name);
                
                obj.FileName = Name;
                
                obj.m = memmapfile(Name, ...
                    'Format', obj.m.Format, ...
                    'Writable', true);
                obj.Writable = 1;
            end
            
            % ------------------
            % No Data processing
            
            if ~isnan(obj.ValNaN)
                subNaN = find(isnan(Value));
                if ~isempty(subNaN)
                    switch class(Value)
                        case 'double'
                            Value(subNaN) = obj.ValNaN;
                        otherwise
                            Value = single(Value);
                            Value(subNaN) = obj.ValNaN;
                    end
                end
            end
            
            % -----------------------
            % Set values
            
            if isequal(obj.DimOrder, [1 2 3])
                obj.m.Data.x(varargin{:}) = Value;
            elseif isequal(obj.DimOrder, [2 1 3])
                obj.m.Data.x(varargin{:}) = Value;
            elseif isequal(obj.DimOrder, [2 3 1])
                obj.m.Data.x(varargin{:}) = Value;
            else
                disp('beurk')
            end
            
            %{
if obj.DimOrder
switch length(varargin)
case 1
sz = size(obj.m.Data.x);
switch sum(sz ~= 1)
case 1
obj.m.Data.x(varargin{1}) = Value;
case 2
if islogical(varargin{1})
subOne =  find(varargin{1});
[I1,I2] = ind2sub([sz(2) sz(1)], subOne);
else
[I1,I2] = ind2sub([sz(2) sz(1)], varargin{1});
end
I1 = sub2ind(sz,I2,I1);
obj.m.Data.x(I1) = Value;
case 3
[I1,I2, I3] = ind2sub([sz(2) sz(1) sz(3)], varargin{1});
I1 = sub2ind(sz,I2,I1,I3);
obj.m.Data.x(I1) = Value;
end
case 2
Value = Value';
obj.m.Data.x(varargin{2}, varargin{1}) = Value;
case 3
if length(obj.Size) == 2
Value = Value';
obj.m.Data.x(varargin{2}, varargin{1}) = Value;
else
subCan = decode_sub(obj, 3, varargin{3});
subCol = decode_sub(obj, 2, varargin{2});
subLig = decode_sub(obj, 3, varargin{1});
for i=1:length(subCan)
X =  Value(:,:,i);
obj.m.Data.x(subCol, subCan(i), subLig) = X';
end
end
end
else
obj.m.Data.x(varargin{:}) = Value;
end
            %}
            
        end % fonction setValue
        
    end % methods(Access='private', Access='private')
end
