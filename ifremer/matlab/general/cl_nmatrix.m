classdef cl_nmatrix < handle & matlab.mixin.CustomDisplay
    
    %
    % Encapsulate nmatrix objet. nmatrix data is accessible like a standart matlab matrix
    %
    % Syntax
    %  created from cl_nmatrixfactory.createInstance(...)
    %
    % Name-Value Pair Arguments
    %   mat:                nmatrix object
    %   sizeIn:             size of data
    %   dimension vector:   vector for reorder dimension if needed.
    %                       Default:[1 2 .. N]
    %
    % Output Arguments
    %
    %
    % Examples
    %
    %   cf cl_nmatrixfactory and test_cl_nmatrix
    %
    
    properties
        
        % Default NaN value
        ValNaN = NaN;
        
        % nmatrix object
        mat
        
        % dim vector
        sizeIn
        
        % vector of dimension order
        dimensionVector
        
        % vector of direction
        directionVector
    end
    
    
    methods (Access = 'public')
        
        %------------------------------------------------------------------
        % Constructeur
        % Syntax
        %   m = cl_nmatrix(varargin)
        %
        % Input Arguments
        %   varargin : i.e nmatrix constructor documentation
        %
        % Examples
        %   n = cl_nmatrix('vars.mat', '/vars', 'mat')
        %------------------------------------------------------------------
        function obj = cl_nmatrix(varargin)
            
            % instanciate nmatrix object
            obj.mat = nmatrix(varargin{:});
            
            % create default dim vector dim = [1 2 .. N] (N number of
            % matrix dimensions)
            ndim = numel(obj.mat.Map.Format{2});
            obj.dimensionVector = 1:ndim;
            obj.directionVector = ones(1, numel(size(obj.mat.Map.Data.Adc)));
            obj.sizeIn = size(obj.mat.Map.Data.Adc);
        end
        
        %------------------------------------------------------------------
        % setDeleteOnDelete
        %
        % If the DeleteOnDelete property is true, mapped file associated to
        % nmatrix object will be deleted when the instance is destroyed
        %
        % Syntax
        %   setDeleteOnDelete(flag)
        %
        % Input Arguments
        %   flag
        %
        % Examples
        %
        %------------------------------------------------------------------
        function setDeleteOnDelete(obj, flag)
            obj.mat.setDeleteOnDelete(flag);
        end
        
        %------------------------------------------------------------------
        % setWritable
        %
        % Set nmatrix writable status
        %
        % Syntax
        %   setWritable(flag)
        %
        % Input Arguments
        %   flag
        %
        % Examples
        %
        %------------------------------------------------------------------
        function setWritable(obj, flag)
            obj.mat.setWritable(flag);
        end
        
        %------------------------------------------------------------------
        % setSizeInOut
        %
        %  Set sizeIn (size of data) and size Out (stored data size)
        %
        %
        % Syntax
        %   setSizeInOut(szIn, szOut)
        %
        % Input Arguments
        %   szIn  : size In
        %   szOut : size Out
        %
        % Examples
        %
        %------------------------------------------------------------------
        function setSizeIn(obj, szIn)
            obj.sizeIn = szIn;
        end
        
        %------------------------------------------------------------------
        % subsref overloading
        %
        % Syntax
        %   subsref(obj, s)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %   s   : structure
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = subsref(obj, s)
            
            if isempty(s)
                % Process nm without index
                s = obj.emptySubsref();
                %varargout{1} = permute(obj.mat.subsref(s), obj.dimensionVector);
                varargout{1} = obj.flipDimensions( permute(obj.mat.subsref(s), obj.dimensionVector) );
            else
                % Check if s.type contain '.': i.e. nm.get(xx) called
                theType = s.type;
                
                if ~strcmp(theType, '.')
                    
                    if isa(s.subs{1}, 'cl_nmatrix')
                        % Process the nm1(nm2)case, nm1 & 2 being two cl_nmatrix
                        % instances
                        varargout{1} = obj.processLogicalArray(s.subs{1}.mat.Map.Data.Adc);
                        
                    elseif islogical(s.subs{1})
                        % Process the nm1(b)case, b being a logical array
                        varargout{1} = obj.processLogicalArray(s.subs{1});
                        
                    else
                        
                        % Check if s.subs index contain ':'
                        dots = find(strcmp(s.subs, ':'), 1);
                        
                        if isempty(dots)
                            if numel(s.subs) > 1
                                % Request like nm(3,2) case.
                                vectorIndex = obj.computeIndex(s.subs);
                            else
                                % Request like nm(18) nm([1:2:10]), nm([1 3 4 8 11]) cases.
                                vectorIndex = obj.computeListIndex(s.subs);
                            end
                            
                            % Replace with new vector index
                            if isa(vectorIndex, 'cell')
                                s.subs = vectorIndex;
                            else
                                s.subs = { vectorIndex };
                            end
                            
                            varargout{1} = obj.mat.subsref(s);
                            
                        else
                            
                            if numel(s.subs) > 1
                                % Process request like nm(1,:), nm(:,2) ...
                                
                                % Version sans tenir compte des directions
                                % D�comment� par JMA le 27/01/2015 car tr�s
                                s.subs = obj.permuteDimensions(s.subs);
                                varargout{1} = permute(obj.mat.subsref(s), obj.dimensionVector);
                                
                                % Prise en compte du vecteur 'Direction'
                                % Version 1 ne fonctionne pas dans tous
                                % les cas
                                % Comment� par JMA le 27/01/2015 car tr�s
                                % tr�s lent lorsque l'on fait une lecture
                                % ligne par ligne (�a lit toute la matrice
                                % � chaque fois)
%                                 %varargout{1} = obj.flipDimensions( permute(obj.mat.subsref(s), obj.dimensionVector) );
%                                 X = subsref(obj, []);
%                                 varargout{1} = subsref(X, s);
                                
                            else
                                % Process request like nm(:)
                                s.subs{1} = obj.computeListIndex( {(1:numel(obj))} );
                                varargout{1} =  reshape(obj.mat.subsref(s), numel(obj), 1);
                            end
                        end
                    end
                else
                    varargout{1} = obj.mat.subsref(s);
                end
                
            end
        end
        
        %------------------------------------------------------------------
        % Display overloading
        %
        % Syntax
        %   display(obj)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %
        % Examples
        %
        %------------------------------------------------------------------
        %         function display(obj)
        %
        %             disp( subsref(obj, []));
        %
        %         end % End of display
        
        
        %------------------------------------------------------------------
        % size overloading
        %
        % Syntax
        %   size(obj)
        %   size(obj, dim)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = size(obj, dim)
            if nargin == 1
                sizeTmp = obj.sizeIn;
            else
                sizeTmp = obj.sizeIn(dim);
            end
            
            varargout{1} = sizeTmp;
        end  % size
        
        %------------------------------------------------------------------
        % numel overloading
        %
        % Syntax
        %   numel(obj)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = numel(obj)
            varargout{1} = numel( obj.mat.Map.Data.Adc);
        end  % size
        
        %------------------------------------------------------------------
        % sin overloading
        %
        % Syntax
        %   sin(obj)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = sin(obj)
            varargout{1} = sin(subsref(obj, []));
        end  % sin
        
        %------------------------------------------------------------------
        % cos overloading
        %
        % Syntax
        %   sin(obj)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = cos(obj)
            varargout{1} = cos(subsref(obj, []));
        end  % cos
        
        %------------------------------------------------------------------
        % sin overloading
        %
        % Syntax
        %   sin(obj)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = tan(obj)
            varargout{1} = tan(subsref(obj, []));
        end  % tan
        
        %------------------------------------------------------------------
        % operators overloading
        %
        % Syntax
        %   <operator>(obj1, obj2)
        %
        % Input Arguments
        %   obj1 : operand 1 (could be a cl_nmatrix instance)
        %   obj2 : operand 2 (could be a cl_nmatrix instance)
        %
        % Examples
        %   m1 = cl_nmatrix('f1.mat', '/val1');
        %   m2 = cl_nmatrix('f2.mat', '/val2');
        %
        %   x = m1 + m2;
        %   x = m1 > pi;
        %   x = [1 2 3 4; 5 6 7 8] + m1;
        %   x = m1 <= m2
        %------------------------------------------------------------------
        function x = plus(obj1, obj2)
            x = evalOperator2(@plus, obj1, obj2);
        end % plus
        
        %------------------------------------------------------------------
        function x = eq(obj1, obj2)
            x = evalOperator2(@eq, obj1, obj2);
        end % eq
        
        %------------------------------------------------------------------
        function x = ne(obj1, obj2)
            x = evalOperator2(@ne, obj1, obj2);
        end % ne
        
        %------------------------------------------------------------------
        function x = gt(obj1, obj2)
            x = evalOperator2(@gt, obj1, obj2);
        end % gt
        
        %------------------------------------------------------------------
        function x = ge(obj1, obj2)
            x = evalOperator2(@ge, obj1, obj2);
        end % ge
        
        %------------------------------------------------------------------
        function x = lt(obj1, obj2)
            x = evalOperator2(@lt, obj1, obj2);
        end % lt
        
        %------------------------------------------------------------------
        function x = le(obj1, obj2)
            x = evalOperator2(@le, obj1, obj2);
        end % le
        
        
        %------------------------------------------------------------------
        % operator with 2 parameters overloading
        %
        % TODO M�thode � am�liorer. Le r�sultat est retourn� en m�moire
        %
        % Syntax
        %   evalOperator2(ptrOp, obj1, obj2)
        %
        % Input Arguments
        %   ptrOp : function pointer
        %   obj1  : operand 1 (could be a cl_nmatrix instance)
        %   obj2  : operand 2 (could be a cl_nmatrix instance)
        %
        % Examples
        %
        %------------------------------------------------------------------
        function x = evalOperator2(ptrOp, obj1, obj2)
            
            szOut = [];
            
            if isa(obj1, 'cl_nmatrix')
                op1 = obj1.mat.Map.Data.Adc;
                szOut = obj1.dimensionVector;
            else
                op1 = obj1;
            end
            
            if isa(obj2, 'cl_nmatrix')
                op2 = obj2.mat.Map.Data.Adc;
                if isempty(szOut)
                    szOut = obj2.dimensionVector;
                end
            else
                op2 = obj2;
            end
            
            x = cl_nmatrixfactory.createInstance('Value', permute(ptrOp(op1,op2), szOut));
        end
    end % methods (Access=public)
    
    
    methods %No method attributes
        
        %------------------------------------------------------------------
        % set dimension vector
        %
        % Syntax
        %   set.dimensionVector(obj, vDim)
        %
        % Input Arguments
        %   obj  : cl_nmatrix instance
        %   vDim : dimension vector
        %
        % Examples
        %
        %  nm.dimensionVector = [3 1 2];
        %------------------------------------------------------------------
        function set.dimensionVector(obj, vdim)
            % Dimension number of nmatrix
            ndim = numel(obj.mat.Map.Format{2});
            
            if numel(vdim) ~= ndim
                error('Matrix dimension and dimension vector must be agree');
            else
                obj.dimensionVector = vdim;
            end
        end
        
        %------------------------------------------------------------------
        % set direction vector
        %
        % Syntax
        %   set.directionVector(obj, vDim)
        %
        % Input Arguments
        %   obj  : cl_nmatrix instance
        %   vDir : direction vector
        %
        % Examples
        %
        %  nm.directionVector = [3 1 2];
        %------------------------------------------------------------------
        function set.directionVector(obj, vdir)
            % Index of dimension to flip
            idx = find(vdir == -1);
            
            % Check if flip above the 3rd dim
            if isempty(find(idx > 3, 1))
                obj.directionVector = vdir;
            else
                error('Inversion above the third dimension is not supported !')
            end
        end
        
        %------------------------------------------------------------------
        % get
        %
        % Syntax
        %   get(obj, attributeName)
        %
        % Input Arguments
        %   obj  : cl_nmatrix instance
        %   attributeName : name of the attribute
        %
        % Examples
        %
        %  nm.get('Filename')
        %------------------------------------------------------------------
        function attr = get(obj, attributeName)
            attr = [];
            
            switch attributeName
                case 'Filename'
                    attr = obj.mat.Filename;
            end
        end
    end %methods %No method attributes
    
    
    methods (Access = protected)
        
        %------------------------------------------------------------------
        % displayNonScalarObject overloading
        % Customize nmatrix displaying to perform like a standard matrix
        %
        %
        % Syntax
        %   displayNonScalarObject(obj)
        %
        % Input Arguments
        %   obj : cl_nmatrix instance
        %
        % Examples
        %
        %------------------------------------------------------------------
        function displayNonScalarObject(obj)
            disp(subsref(obj, []));
        end
        
        
        %------------------------------------------------------------------
        % emptySubsref: Initialise an empty structure for subsref function
        %   Called from subsref function
        %
        % Syntax
        %   emptySubsref()
        %
        % Input Arguments
        %
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = emptySubsref(obj)
            subs(1).type = '()';
            
            % Construct sequence to display matrix with all dimensions
            separator = {':'};
            ndim = numel(size(obj.mat.Map.Data.Adc));
            subs(1).subs = repmat(separator,[1 ndim]);
            
            varargout{1} = subs;
        end
        
        %------------------------------------------------------------------
        % computeIndex: compute index in memory vector storage according to
        %   dimension vector. Called from subsref function
        %
        % Syntax
        %   computeIndex(s.subs)
        %
        % Input Arguments
        %   Ssubs : s.subs structure from subsref function
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = computeIndex(obj, Ssubs)
            
            % Initialize temporary dimension vector same size of index
            % number entries. Ex request nm(2,3,1) and nmatrix
            % size is only defined as [2 4]
            dimVectorIn = (1 : max(numel(obj.dimensionVector), numel(Ssubs)));
            dimVectorIn(1:numel(obj.dimensionVector)) = obj.dimensionVector;
            
            % Initialize subs vector to [1] for each dimension
            A = {1};
            subs = repmat(A, [1 numel(dimVectorIn)]);
            
            % Reorders input indexes and size vector according to the
            % dimension vector
            sz = ones(1, numel(dimVectorIn));
            for dim=1:numel(Ssubs)
                subs(dimVectorIn(dim)) = Ssubs(dim);
            end
            
            % Reorder size vector according to the
            % dimension vector
            for dim=1:numel(size(obj))
                sz(dimVectorIn(dim)) = size(obj, dim);
            end
            
            % Compute indexes in vector stored memory
            % Ex: for nm(i, j, k) in dimension [d1 d2 d3]
            % index = (k-1)*(d2*d1) + (j-1)*d1 + i
            vectorIndex = { 0 };
            idxV = 1;
            dim  = numel(dimVectorIn);
            
            % Compute index on N-1 dim : ([N ... 2])
            while dim > 0
                
                if dim > 1
                    
                    % Prise en compte du vecteur direction
                    if obj.directionVector(dimVectorIn(dim)) == -1
                        valIndex = sz(dim) - subs{dim} + 1;
                    else
                        valIndex = subs{dim};
                    end
                    
                    vectorIndex{idxV} = vectorIndex{idxV} + ...
                        ( (valIndex - 1) * prod(sz(1:dim-1)) );
                    
                    % Remplace la version initiale suivante ...
                    %vectorIndex{idxV} = vectorIndex{idxV} + ...
                    %    ( (subs{dim} - 1) * prod(sz(1:dim-1)) );
                else
                    
                    % Prise en compte du vecteur direction
                    if obj.directionVector(dimVectorIn(dim)) == -1
                        valIndex = sz(dim) - subs{dim};
                    else
                        valIndex = subs{dim};
                    end
                    
                    vectorIndex{idxV} = vectorIndex{idxV} + valIndex;
                    
                    % Remplace la version initiale suivante ...
                    % finalize calculation with index on first dim
                    %vectorIndex{idxV} = vectorIndex{idxV} + subs{dim};
                end
                
                dim = dim -1;
            end
            
            varargout = vectorIndex;
            
        end % computeIndex
        
        %------------------------------------------------------------------
        % computeListIndex: compute indexes in memory vector storage according to
        %   dimension vector. Called from subsref function
        %
        % Syntax
        %   computeListIndex(s.subs)
        %
        % Input Arguments
        %   Ssubs : s.subs structure from subsref function
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = computeListIndex(obj, Ssubs)
            
            vectorIndex = zeros(1, numel(Ssubs{:}));
            
            for idx=1:numel(Ssubs{:})
                
                % Get current index
                currentIndex = Ssubs{:}(idx);
                
                % Translate memory vector index in row/col indexes
                RC = cell(1, numel(obj.dimensionVector));
                [RC{:}] = ind2sub(size(obj), currentIndex);
                
                % Compute new memory vector index according to dimension
                % vector
                vectorIndex(idx) = obj.computeIndex(RC);
                
            end %for
            
            varargout{1} =  vectorIndex;
        end
        
        %------------------------------------------------------------------
        % processLogicalArray: extract each element of nmatrix where
        %                      arrayIn(idx) ~= 0
        %
        % TODO
        % 1ere version: r�sultat en m�moire !!! (peut mieux faire)
        %
        % Syntax
        %   processLogicalArray(arrayIn)
        %
        % Input Arguments
        %   arrayIn : logical array
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = processLogicalArray(obj, arrayIn)
            res = [];
            idx = 1;
            for k=numel(obj.mat.Map.Data.Adc):-1:1
                if arrayIn(k)
                    res(idx) = obj.mat.Map.Data.Adc(k); % TODO
                    idx = idx+1;
                end
            end
            varargout{1} = res';
        end % processLogicalArray
        
        %------------------------------------------------------------------
        % permuteDimensions: reorder index in according to dimension vector
        %   Called from subsref function
        %
        % Syntax
        %   permuteDimensions(obj, sSubs)
        %
        % Input Arguments
        %   sSubs: subsref s.subs structure element
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = permuteDimensions(obj, sSubs)
            nsSubs = numel(sSubs);
            if nsSubs <= numel(obj.dimensionVector)
                % Initialize subs vector to [1] for each dimension
                A = {1};
                subs = repmat(A, [1 numel(obj.dimensionVector)]);
            else
                % Copy initial structure
                subs = sSubs;
            end
            
            % Permute according to the dimension vector for the
            % requested number of dimensions
            for dim=1:min(numel(sSubs),numel(obj.dimensionVector))
                subs(obj.dimensionVector(dim)) = sSubs(dim);
            end
            varargout{1} = subs;
        end
        
        %------------------------------------------------------------------
        % flipDimensions: flip matrix in according to direction vector
        %   Called from subsref function. Only 3 dimensions supported
        %
        % Syntax
        %   flipDimensions(obj, array)
        %
        % Input Arguments
        %   array: array to flip
        %
        % Examples
        %
        %------------------------------------------------------------------
        function varargout = flipDimensions(obj, array)
            flip1 = (1:size(array, 1));
            flip2 = (1:size(array, 2));
            flip3 = (1:size(array, 3));
            needflip = false;
            
            % Get dimension index to flip
            IdxDim = find(obj.directionVector == -1);
            
            if ~isempty(find(IdxDim == 1, 1))
                flip1 = (size(array,1):-1:1);
                needflip = true;
            end
            
            if ~isempty(find(IdxDim == 2, 1))
                flip2 = (size(array,2):-1:1);
                needflip = true;
            end
            
            if ~isempty(find(IdxDim == 3, 1))
                flip3 = (size(array,3):-1:1);
                needflip = true;
            end
            
            if needflip
                varargout{1} = array(flip1, flip2, flip3);
            else
                % No flip
                varargout{1} = array;
            end
        end
    end
end
