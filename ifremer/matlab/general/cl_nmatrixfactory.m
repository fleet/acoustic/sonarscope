classdef cl_nmatrixfactory
    %
    % Instanciate a cl_nmatrix objet via the static method createInstance.
    %
    % Syntax
    % nm = cl_nmatrixfactory.createInstance( ... )
    %
    % Name-Value Pair Arguments
    %   FileName     : File name (one is created by default)
    %   Value        : Matrix to be mapped on a file
    %   Size         : Size of the matrix to be mapped
    %   Format       : Data storage type : uint8, int8, ...(default : 'double')
    %   ValNaN       : Value to be used for NoData (to be used if Format is integer)
    %   FileCreation : 1=Create a file, 0=mapping on an existing file
    %   Dim          : Dimension order vector
    %   Direction    : Direction for each dimension
    %
    % Output Arguments
    %   nm : cl_nmatrix instance
    %
    % Examples
    %
    %  % Create and initialize a cl_nmatrix with default mapped file
    %    nm = cl_nmatrixfactory.createInstance('Value', [1 2 3 4; 5 6 7 8]);
    %
    %  % Create and initialize a cl_nmatrix with a . mat temporary mapped file with myVar variable name
    %   nm = cl_nmatrixfactory.createInstance('Value', [1 2 3 4; 5 6 7 8], 'Ext', '.mat', 'VarName', 'myVar');
    %
    %  % Create and initialize a cl_nmatrix with default mapped file and Dimension order vector (invert col/row)
    %   nm = cl_nmatrixfactory.createInstance('Value', [1 2 3 4; 5 6 7 8], 'Dim', [2 1]);
    %
    %  % Create and initalize a cl_matrix with a specific mapped file
    %    nm = cl_memmapfile('FileName', tempname, 'Value', [1 2 3 4; 5 6 7 8]);
    %
    %  % Create a cl_nmatrix with default value and size, format double (default)
    %    nm = cl_nmatrixfactory.createInstance('Value', 0, 'Size', [2, 4]);
    %
    %  % Create a cl_nmatrix with default value and size, format single
    %    nm = cl_nmatrixfactory.createInstance('Value', 0, 'Size', [20 20], 'Format', 'single')
    %
    %
    %  % Create a cl_nmatrix from variable name 'vals' of an existing 'vals.mat' file
    %    nm = cl_nmatrixfactory.createInstance('FileName', 'vals.mat', 'VarName', 'vals')
    %
    %  % Create a cl_nmatrix from a Image.bin file: 358x901 'single' data and invert rows/cols
    %    nm = cl_nmatrixfactory.createInstance('FileName', 'Image.bin', 'Format', 'single', 'Size', [358 901], 'Dim', [2 1])
    %
    %  % Create a cl_nmatrix from a .bil file, reorder dimension  and invert
    %    flip direction on dimension 1
    %   nm = cl_nmatrixfactory.createInstance('FileName', filenameBil, 'Format', class(X), 'Size', size(X), 'Dim', [2 3 1], 'Dimension', [-1 1 1])
    %
    
    properties
    end
    
    methods (Static)
        
        function varargout = test()
            disp('TEST');
            varargout{1} = 1;
        end
        
        %------------------------------------------------------------------
        % cl_nmatrix creation
        %
        %------------------------------------------------------------------
        function nm = createInstance(varargin)
            
            [varargin, filenameIn]  = getPropertyValue(varargin, 'FileName',  []);
            [varargin, varnameIn]   = getPropertyValue(varargin, 'VarName',   []);
            [varargin, valueIn]     = getPropertyValue(varargin, 'Value',     []);
            [varargin, formatIn]    = getPropertyValue(varargin, 'Format',    []);
            [varargin, sizeIn]      = getPropertyValue(varargin, 'Size',      []);
            [varargin, dimIn]       = getPropertyValue(varargin, 'Dim',       []);
            [varargin, directionIn] = getPropertyValue(varargin, 'Direction', []);
            [varargin, extensionIn] = getPropertyValue(varargin, 'Ext',       []); %#ok<ASGLU>
            
            % Default value if not fileIn
            if isempty(filenameIn) && isempty(valueIn)
                valueIn = 0;
            end
            
            % Create cl_nmatrix instance with 'Value' argument
            if ~isempty(valueIn)
                
                % Data format
                if isempty(formatIn)
                    formatIn = class(valueIn(1));
                end
                
                if strcmp(formatIn, 'logical') || strcmp(formatIn, 'char')
                    formatIn = 'uint8';
                end
                
                % set matrix values
                if isempty(sizeIn)
                    % Create from value: m = cl_nmatrixfactory.createInstance('Value', [1 2 3 4; 5 6 7 8]);
                    value = valueIn;
                else
                    % Create from value: m = cl_nmatrixfactory.createInstance('Value', 0, 'Size', [2, 4]);
                    value = valueIn * ones(sizeIn);
                end
                
                if isempty(extensionIn) || ~strcmp(extensionIn, '.mat')
                    % Create a memmapfile temporary name
                    
                    % initialize mem map file  file name
                    if isempty(filenameIn)
                        [pathName, Name] = fileparts(my_tempname);
                        mmFileName = fullfile(pathName, [Name '.memmapfile']);
                    else
                        mmFileName = filenameIn;
                    end
                    
                    % Create temporary file
                    fid = fopen(mmFileName, 'w+');
                    fwrite(fid, value, formatIn);
                    fclose(fid);
                    
                    %Reorder size if nedded
                    [reorderedSize, reorderedDimVec] = cl_nmatrixfactory.reorderVec(dimIn, size(value));
                    
                    % memmapfile from temporary file
                    mmf = memmapfile( mmFileName, 'Format', ...
                        {formatIn reorderedSize 'Adc'}, ...
                        'Writable', true);
                    
                    % instanciate cl_nmatrix from memmapfile object
                    nm = cl_nmatrix(mmf);
                    
                    % set writable
                    setWritable(nm, true);
                    
                    % Delete mapped file when cl_nmatrix will be deleted
                    setDeleteOnDelete(nm, true);
                    
                    % Save sizes in nm instance
                    setSizeIn(nm, size(value));
                    
                    % Initialize dimension vector if needed
                    if ~isempty(dimIn)
                        %nm.dimensionVector = dimIn;
                        nm.dimensionVector = reorderedDimVec;
                    end
                    
                else
                    % Create a .mat temporary name
                    if isempty(varnameIn)
                        varnameIn = 'Data';
                    end
                    
                    [pathName, Name] = fileparts(my_tempname);
                    matFileName = fullfile(pathName, [Name '.mat']);
                    
                    savemat(matFileName, varnameIn, value, '-v6');
                    
                    nm = cl_nmatrix(matFileName, varnameIn, 'mat');
                    
                    % Save sizes in nm instance
                    setSizeIn(nm, size(value));
                end
                
            else
                
                % Check file extension
                [~, ~, ext] = fileparts(filenameIn);
                
                % In case of erMapper file, no extension -> .bin
                if isempty(ext)
                    ext = '.bin';
                end
                
                switch ext
                    
                    case '.bin'
                        % Create cl_matrix instance from .bin file
                        if ~isempty(formatIn) && ~isempty(sizeIn)
                            
                            %Reorder size if nedded
                            [reorderedSize, reorderedDimVec] = cl_nmatrixfactory.reorderVec(dimIn, sizeIn);
                            
                            % Create memmapfile from .bin file
                            mmf =  memmapfile(filenameIn, 'Format', ...
                                { formatIn reorderedSize 'Adc'},  'Writable', false, 'Repeat', 1);
                            
                            % instanciate cl_nmatrix from memmapfile object
                            nm = cl_nmatrix(mmf);
                            
                            % Save sizes in nm instance
                            setSizeIn(nm, sizeIn);
                            
                        else
                            error('''Format'' and/or ''Size'' argument not specified. Ex: nm = cl_nmatrixfactory.createInstance(''FileName'', ''Image.bin'', ''Format'', ''single'', ''Size'', [358 901])');
                        end
                        
                        % Initialize dimension vector if needed
                        if ~isempty(dimIn)
                            nm.dimensionVector = reorderedDimVec;
                        end
                        
                    case '.mat'
                        
                        if ~isempty(varnameIn)
                            
                            %Get variable size
                            matObj = matfile(filenameIn);
                            info = whos(matObj, varnameIn(2:end));
                            
                            % Create instance
                            nm = cl_nmatrix(filenameIn, varnameIn, 'mat');
                            
                            % Save sizes in nm instance
                            setSizeIn(nm, info.size);
                            
                        else
                            error('''VarName'' argument not specified. Ex:  cl_nmatrixfactory.createInstance(''FileName'', ''vals.mat'', ''VarName'', ''/vals'')')
                        end
                        
                    otherwise
                        message = 'Unrecognized file format';
                        disp(message);
                end
                
                % set writable
                setWritable(nm, false);
                
                % Don't delete mapped file when cl_nmatrix will be deleted
                setDeleteOnDelete(nm, false);
            end
            
            % Set direction vector
            if ~isempty(directionIn)
                
                % Version avec reordonnancement du vecteur direction en
                % fonction du vecteur dimension. Exemple
                % dimIn = [2 3 1] et directionIn[-1 1 1], l'inversion se
                % fera suivant la 2�me dimension conform�ment � l'�l�ment
                % 1 de dimIn
                % Fonctionnalit� non activ�e
                % direction = cl_nmatrixfactory.reorderDir(dimIn, directionIn);
                
                % Remplac�e par la version ci dessous. Exemple si
                % directionIn[-1 1 1] l'inversion se fera suivant la 1ere
                % dimension de la matrice apr�s reordonnacment �ventuel des
                % dimensions.
                direction = directionIn;
                
                nm.directionVector = direction;
            end
        end
        
        %------------------------------------------------------------------
        % subsref reorderVec
        %
        % Reorder size vector and eventuelly dimension vector in according
        % to dimension vector)
        %
        % Syntax
        %   reorderVec(dimVector, sizeIn)
        %
        % Input Arguments
        %   dimVector : dimension vector
        %   sizeIn    : size vector
        %
        % Examples
        %
        %------------------------------------------------------------------
        function [reorderedSz, reorderedDim] = reorderVec(dimVector, sizeIn)
            
            if ~isempty(dimVector)
                reorderedSz  = ones(1, numel(sizeIn));
                reorderedDim = (1 : numel(dimVector));
                reorderDim   = false;
                
                %Check how many dim swapped
                if numel(dimVector) > 2
                    DiffDim = reorderedDim - dimVector;
                    imDim = find(DiffDim == 0);
                    
                    if isempty(imDim) ||numel(imDim) > 2
                        % At least 2 dim swapped. Reorder dimension vector too
                        reorderDim = true;
                    end
                end
                
                if ~reorderDim
                    reorderedDim = dimVector;
                end
                
                for dim=1:numel(dimVector)
                    
                    reorderedSz(dim) = sizeIn(dimVector(dim));
                    
                    if reorderDim
                        reorderedDim(dim) = dimVector(dimVector(dim));
                    end
                    
                end
            else
                reorderedSz = sizeIn;
                reorderedDim = sizeIn;
            end
            
        end
        
        %------------------------------------------------------------------
        % subsref reorderDir
        %
        % Reorder direction vector in according to dimension vector)
        %
        %
        % Syntax
        %   reorderDir(dimVector, dirVector)
        %
        % Input Arguments
        %   dimVector : dimension vector
        %   dirVector : direction vector
        %
        % Examples
        %
        %------------------------------------------------------------------
        function reorderedDirection = reorderDir(dimVector, dirVector)
            
            direction = ones(1, numel(dirVector));
            
            if isempty(dimVector)
                % Set directly
                direction  = dirVector;
            else
                % Set after reorder directions
                if numel(dimVector) ~= numel(dirVector)
                    error('''Dim'' and ''Direction'' vectors must have same number of elements.');
                end
                
                % Set after reorder directions
                for dim=1:numel(dirVector)
                    direction(dimVector(dim)) = dirVector(dim);
                end
                
            end
            
            reorderedDirection = direction;
        end
    end
end
