% Affichage d'un courtesy
%
% Syntax
%   courtesy(nomFic)
%
% Input Arguments
%   nomFic : Nom d'un fichier
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   courtesy(nomFic)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function courtesy(nomFic)

[~, nomFic, ext] = fileparts(nomFic);
nomFic = lower([nomFic ext]);

if contains(nomFic,'prismed')
    msg = sprintf('This image is a courtesy of Geosciences Azur.');
    WebSite = 'http://www-geoazur.unice.fr/';
    
elseif contains(nomFic,'.hgt')
    msg = sprintf('This image is a courtesy of NASA.');
    WebSite = 'http://www.dlr.de/srtm/copyright/copyright_en.htm';
    
elseif contains(nomFic,'belgica') || contains(nomFic,'marcroche')
    msg = sprintf('This image is a courtesy of Marc Roche, Service Public F�d�ral de l''Economie, Belgique.');
    WebSite = 'http://economie.fgov.be/';
    
elseif contains(nomFic,'niwa')
    msg = sprintf('This image is a courtesy of NIWA (NZ)');
    WebSite = 'http://www.niwa.cri.nz/';
    
elseif contains(nomFic,'bmo')
    msg = sprintf('This image is a courtesy of Brest-M�tropole-Oc�ane (FR)');
    WebSite = 'http://www.brest-metropole-oceane.fr/vueduciel/droitsetusages.html';
    
else
    return
end

str1 = 'TODO';
str2 = sprintf('%s\nPlease contact "%s" before any use of this data.', msg, WebSite);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'courtesy', 'TimeDelay', 10);
