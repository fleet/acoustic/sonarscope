% NavFileName = fullfile(my_tempdir, 'Wrecks.xml');
% createExampleFileNav(NavFileName)
% type(NavFileName)

function flag = createExampleFileNav(NavFileName)

Y = str2lat('N36 05.996');
Y(end+1) = str2lat('N36 04.560');
Y(end+1) = str2lat('N36 22.300');
Y(end+1) = str2lat('N37 33.000');
Y(end+1) = str2lat('N42 54.000');
Y(end+1) = str2lat('N42 43.600');
Y(end+1) = str2lat('N43 01.560');
Y(end+1) = str2lat('N43 01.000');
Y(end+1) = str2lat('N43 04.541');
Y(end+1) = str2lat('N43 05.700');
Y(end+1) = str2lat('N43 05.330');
X = str2lon('W005 22.003');
X(end+1) = str2lon('W005 20.700');
X(end+1) = str2lon('W002 11.100');
X(end+1) = str2lon('W000 30.700');
X(end+1) = str2lon('E005 23.110');
X(end+1) = str2lon('E005 50.600');
X(end+1) = str2lon('E005 55.470');
X(end+1) = str2lon('E005 56.400');
X(end+1) = str2lon('E005 59.673');
X(end+1) = str2lon('E005 58.500');
X(end+1) = str2lon('E005 57.130');

X = X';
Y = Y';

fid = fopen(NavFileName, 'w+');
if fid == -1
    return
end
for k=1:length(X)
    fprintf(fid, '%s %s\n', lat2str(Y(k)), lon2str(X(k)));
end
fclose(fid);
flag = 1;
