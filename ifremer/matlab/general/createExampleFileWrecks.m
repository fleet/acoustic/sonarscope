% WrecksFileName = fullfile(my_tempdir, 'Wrecks.xml');
% createExampleFileWrecks(WrecksFileName)
% type(WrecksFileName)
% Wrecks = xml_read(WrecksFileName)

function createExampleFileWrecks(WrecksFileName)

% TODO : possibilit� de sauver en .csv abandonn� pour l'instant car il faut faire une fonction de lecture : la flemme
% WrecksFileName = fullfile(my_tempdir, 'Wrecks.csv');
% createExampleFileWrecks(WrecksFileName)
% Wrecks = xml_read(WrecksFileName)
% M = csvread(WrecksFileName)

Wrecks(1).Name   = 'L''Embla';
Wrecks(end).Lat    = 43 + 24.70/60;
Wrecks(end).Lon    =  4 +  9.81/60;
Wrecks(end).Ellips =  'Europe50';
Wrecks(end).Min    = 24;
Wrecks(end).Depth  = 34;
Wrecks(end).Type   = 'Cargo';
Wrecks(end).Length = 69;
Wrecks(end).Width  = 10;
Wrecks(end).Built  = 1908;
Wrecks(end).Sunk   = 1944;
Wrecks(end).web    = '';

Wrecks(end+1).Name = 'Le Derna';
Wrecks(end).Lat    = 43 + 24.3577/60;
Wrecks(end).Lon    =  4 + 11.9688/60 ;
Wrecks(end).Ellips =  'wgs84';
Wrecks(end).Min    = 18;
Wrecks(end).Depth  = 34;
Wrecks(end).Type   = 'Cargo';
Wrecks(end).Length = 84;
Wrecks(end).Width  = 0;
Wrecks(end).Built  = 1912;
Wrecks(end).Sunk   = 1943;
Wrecks(end).web    = 'http://www.scubadata.com/site-de-plongee/451-Epave-DERNA-Saintes-Marie-de-la-mer.html';

Wrecks(end+1).Name = 'Le Prot�e';
Wrecks(end).Lat    = 43 +  4.260/60;
Wrecks(end).Lon    =  5 + 32.230/60;
Wrecks(end).Ellips =  'wgs84';
Wrecks(end).Min    = 0;
Wrecks(end).Depth  = 127;
Wrecks(end).Type   = 'Submarine';
Wrecks(end).Length = 92;
Wrecks(end).Width  = 0;
Wrecks(end).Built  = 1930;
Wrecks(end).Sunk   = 1943;
Wrecks(end).web    = 'http://matostek.over-blog.com/';

Wrecks(end+1).Name = 'Venus des Iles';
Wrecks(end).Lat    = 43 + 3.6321/60;
Wrecks(end).Lon    =  6 + 0.527/60;
Wrecks(end).Ellips = 'wgs84';
Wrecks(end).Min    = 72.1;
Wrecks(end).Depth  = 79;
Wrecks(end).Type   = 'Vedette';
Wrecks(end).Length = 35;
Wrecks(end).Width  = 8;
Wrecks(end).Built  = 0;
Wrecks(end).Sunk   = 1975;
Wrecks(end).web    = '';

Wrecks(end+1).Name = 'Heinkel 111';
Wrecks(end).Lat    = 43 + 3.5851/60;
Wrecks(end).Lon    =  6 + 0.6414/60;
Wrecks(end).Ellips =  'wgs84';
Wrecks(end).Min    = 75.1;
Wrecks(end).Depth  = 79;
Wrecks(end).Type   = 'Aeronef';
Wrecks(end).Length = 22;
Wrecks(end).Width  = 22;
Wrecks(end).Built  = 0;
Wrecks(end).Sunk   = 1945;
Wrecks(end).web    = '';

Wrecks(end+1).Name = 'Niedersachsen';
Wrecks(end).Lat    = 43 + 3.095/60;
Wrecks(end).Lon    =  6 + 0.737/60;
Wrecks(end).Ellips = 'wgs84';
Wrecks(end).Min    = 84.9;
Wrecks(end).Depth  = 95;
Wrecks(end).Type   = 'Cargo';
Wrecks(end).Length = 100;
Wrecks(end).Width  = 13;
Wrecks(end).Built  = 1934;
Wrecks(end).Sunk   = 1944;
Wrecks(end).web    = '';

Wrecks(end+1).Name = 'Etoile';
Wrecks(end).Lat    = 43 + 3.6419/60;
Wrecks(end).Lon    =  6 + 3.6419/60;
Wrecks(end).Ellips = 'wgs84';
Wrecks(end).Min    = 77;
Wrecks(end).Depth  = 81;
Wrecks(end).Type   = 'Torpilleur';
Wrecks(end).Length = 0;
Wrecks(end).Width  = 0;
Wrecks(end).Built  = 0;
Wrecks(end).Sunk   = 1913;
Wrecks(end).web    = '';

[~, ~, ext] = fileparts(WrecksFileName);
switch ext
    case '.xml'
        xml_write(WrecksFileName, Wrecks);
        type(WrecksFileName)
    case '.csv'
        F = fieldnames(Wrecks);
        nbFields = length(F);
        for i=1:nbFields
            str{1,i} = F{i}; %#ok<AGROW>
        end
        for k=1:length(Wrecks)
            for i=1:nbFields
                str{1+k,i} = Wrecks(k).(F{i});
            end
        end
        flag = my_csvwrite(WrecksFileName, str);
        if flag
            type(WrecksFileName)
            winopen(WrecksFileName)
            disp(str)
        else
            messageErreurFichier(WrecksFileName, 'WriteFailure');
        end
        
    otherwise
        str1 = 'Seuls les extensions ".xml" et ".csv" sont admises.';
        str2 = 'Only ".xml" and ".csv" are allowed.';
        my_warndlg(Lang(str1,str2), 1);
end
