% Cr�ation automatique du Format � utiliser dans fprintf pour conserver la
% meilleure pr�cision d'une donn�e
%
% Syntax
%   Format = create_fprintf_Format(x)
%
% Input Arguments
%   x : Ensemble des valeurs
%
% Output Arguments
%   Format : Format du style %10.5f
%
% Examples 
%   Format = create_fprintf_Format(pi)
%   Format = create_fprintf_Format([pi*1e-10 pi])
%   Format = create_fprintf_Format([pi       pi*1e10])
%   Format = create_fprintf_Format([pi*1e-10 pi*1e10])
%
% See also str2lat lon2str str2lon create_fprintf_Format Authors
% Authors : JMA
%--------------------------------------------------------------------------

function Format = create_fprintf_Format(x)

xmin = min(x);
xmax = max(x);

if xmin == 0
    % Bidouille rajout�e par JMA le 21/03/2011 car 333 chiffres apr�s la virgule pour x=1.0e+006 * [0   8.2962]
    xmin = xmin + x(2);
    xmax = xmax + x(2);
end

precisionApresVirgule = max(abs(floor(log10(eps((abs([xmin xmax]))))))) + 1;
precisionAvantVirgule = max(ceil(log10(floor([xmin xmax]))) + 1);

Format = ['%' sprintf('%d.%d', precisionAvantVirgule+precisionApresVirgule, precisionApresVirgule) 'f'];
