function Y = cryptPLP(X)

for i=1:size(X,1)
    for j=1:size(X,2)
        if ~isnan(X{i,j})
            Y{i,j} = cryptstr2num(X{i,j}, i, j); %#ok<AGROW>
        end
    end
end
