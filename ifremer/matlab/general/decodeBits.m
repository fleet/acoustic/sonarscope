% x = uint8(0:255);
% y = decodeBits(x, [2 1])
% y = decodeBits(x, [8 1])
% y = decodeBits(x, 3)
% y = decodeBits(x, 8)

function y = decodeBits(x, bits)

x = uint8(x);

nBits = numel(bits);
for k=nBits:-1:1
    B(:,k) = bitget(x(:), bits(k));
end

sz = size(x);
y = zeros(sz, 'uint8');
for k=1:nBits
    V =  B(:,k) * (2 .^ (nBits-k));
    y = y + reshape(V, sz);
end
