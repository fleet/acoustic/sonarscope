% Calcul de la derivee d'ordre 1 a 4 ! A REVOIR AVEC YHDR
%
% Syntax
%   yp = derivee4(x, y, n)
%
% Input Arguments
%   x : Abscisses
%   y : Ordonnees
%   n : ordre de la derivee
%
% Name-Value Pair Arguments
%
% Output Arguments
%   [] : Auto-plot activation
%   yp : Derivee premiere
%
% Remarks : A REVOIR AVEC YHDR
%
% Examples
%   t = 0:.01:2*pi;
%   x = sin(t);
%   x = x + 0.001 * (rand(size(t))-0.5);
%   y = derivee4(x, 2);
%   plot(t, x, '+b', t, y, '+r')
%
% See also Authors
% Authors : YHDR + JMA
%-------------------------------------------------------------------------------

function y = derivee4(x, ordre)

ordrePoly = 4;

% Matrice pour calculer les quadratures sur 5 points

A = [ 1 1 1 1 1 ; 2 1 0 -1 -2 ; 4 1 0 1 4 ; 8 1 0 -1 -8 ; 16 1 0 1 16 ] ;

% Seconds membres pour calculer :

% la d�riv�e premi�re :
b{1} = [ 0 ; 1 ; 0 ; 0 ; 0 ] ;

% la d�riv�e seconde :
b{2} = [ 0 ; 0 ; 2 ; 0 ; 0 ] ;

% la d�riv�e troisi�me :
b{3} = [ 0 ; 0 ; 0 ; 6 ; 0 ] ;

% la d�riv�e quatri�me :
b{4} =  [ 0 ; 0 ; 0 ; 0 ; 24 ] ;

% calcul sous forme d'un filtre :

BB = A \ b{ordre} ;

y = filter(BB, 1, x) / ((x(2) - x(1))^ordre);

% --------
% Decalage

y(1:ordrePoly) = NaN;
y = y((ordrePoly/2+1):end);
y(end+1:end+ordrePoly/2) = NaN;
