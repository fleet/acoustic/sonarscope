function display_LogFile(Fig)

if ~get_UseLogFile
    return
end

LogNameGlobal = fullfile(my_tempdir, ['SonarScopeLog_' userdomain '_' username '.xml']);
fid = fopen(LogNameGlobal, 'r');
Lines = {};
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    msg = sprintf('<Window>Figure %d', Fig);
    if contains(tline, msg) && ~isempty(strfind(Lines{end}, 'SonarScope Window Opening'))
        if length(Lines) > 2
            Lines = Lines(end-1:end);
        end
    end
    Lines{end+1} = tline; %#ok<AGROW>
end
fclose(fid);

% Lines = ['<?xml version="1.0" encoding="UTF-8"?>'; ...
%     '<Root xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\TEMP\SonarScope.xsd">'; 
%     Lines(:); '</Root>'];
Lines = ['<?xml version="1.0" encoding="UTF-8"?>'; ...
    '<Root>'; Lines(:); '</Root>'];

mots = strsplit(Lines{6});
if length(mots) < 2
    disp('Ma dou� beniguet, qu''est-ce qui se passe ici ?')
    return
end


str = mots{2};
LogName = fullfile(my_tempdir, ['SonarScopeLog_' userdomain '_' username '_' str(1:2) str(4:5) str(7:8) '.xml']);
fid = fopen(LogName, 'w+');
for i=1:length(Lines)
	fprintf(fid, '%s\n', Lines{i});
end
% fprintf(fid, '\n-----------------------------------\n');
% fprintf(fid, 'Think to empty %s sometimes.\nDo the same whith files ended by .memmapfile.', LogNameGlobal);
fclose(fid);

[rep, flag] = my_questdlg(['Open ' LogName ' ?'], 'Init', 2);
if ~flag || (rep == 2)
    return
end

winopen(LogName)
eval(cmd);
