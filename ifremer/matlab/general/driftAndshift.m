% Apply a drift and an offset to some coordinates (X,Y) on a sonar image
%
% Syntax
%   xy2 = driftAndshift(xy1, Param, sz)
%
% Input Arguments
%   xy1   : Vector obtained by xy1(:)'
%           Orignal xy1 is a matrix of size (N,4) with
%           xy1(:,1) = X0, xy1(:,2) = Y0, xy1(:,3) = X, xy1(:,4) = Y
%   Param : [Angle (deg), OffsetX (m), OffsetY (m)]
%   sz    : size of xy1 before transformation to a vector
%
% Output Arguments
%   xy2 : rehape(xy2, sz) gives NewX = xy2(:,1), NewY = xy2(:,2)
%
% Examples
%   xy2 = driftAndshift(xy1, Param, sz)
%
% See also sonar_rectificationPingAcrossDist Authors
% Authors    : JMA
%-------------------------------------------------------------------------------

function xy2 = driftAndshift(xy1, Param, sz)

Teta   = Param(1);
DeltaX = Param(2);
DeltaY = Param(3);

xy1 = reshape(xy1, sz);

xSDF0 = xy1(:,1);
ySDF0 = xy1(:,2);
xSDF1 = xy1(:,3);
ySDF1 = xy1(:,4);
% xSDF2 = xy1(:,5);
% ySDF2 = xy1(:,6);

c = cosd(Teta);
s = sind(Teta);

R = [c s; -s c];

xy2 = R * [(xSDF1-xSDF0)'; (ySDF1-ySDF0)'];
xy2 = xy2';
xy2(:,1) = xSDF0 + xy2(:,1) + DeltaX;
xy2(:,2) = ySDF0 + xy2(:,2) + DeltaY;

xy2 = xy2(:)';
