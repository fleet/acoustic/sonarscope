function eraseSonarScopeCache(nomFic)

logFileId = getLogFileId;

if ischar(nomFic)
    nomFic = {nomFic};
end

nbFic = length(nomFic);
str1 = 'Suppression des r�pertoires SonarScope.';
str2 = 'Erase SonarScope directories.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw)
    [nomDirALL, nomFicSeul, ~] = fileparts(nomFic{k});
    
    %% R�pertoire XML-Bin
    
    nomDir = fullfile(nomDirALL,'SonarScope', nomFicSeul);
    if exist(nomDir, 'dir')
        try
            fprintf('Erase %s.\n', nomDir);
            flag = rmdir(nomDir, 's');
            if ~flag
                str1 = sprintf('Il est impossible de supprimer le r�pertoire "%s". V�rifiez si des images de ce profil sont charg�es dans SonarScope. Si oui, supprimer les toutes et recommencez l''op�ration.', nomDir);
                str2 = sprintf('It is impossible to suppress directory "%s". Please check if somes images of this line are loaded in SonarScope. If so, please suppress all of them and redo this op�ration.', nomDir);
                my_warndlg(Lang(str1,str2), 0);
            end
        catch ME
            ErrorReport = getReport(ME) %#ok<NOPRT>
            fprintf('%s\n', ErrorReport);
            str1 = sprintf('Erreur lors de la suppression du r�pertoire "%s".', nomDir);
            str2 = sprintf('Error when erasing directory "%s".', nomDir);
            my_warndlg(Lang(str1,str2), 1);
            my_close(hw)
        end
    end
    
    %% Fichier Netcdf
    
    nomFicNc = fullfile(nomDirALL,'SonarScope', [nomFicSeul '.nc']);
    if exist(nomFicNc, 'file')
        try
            delete(nomFicNc)
            msg = sprintf('"%s" deleted.',nomFicNc);
            logFileId.warn('eraseSonarScopeCache', msg);
        catch %#ok<CTCH>
            msg = sprintf('"%s" could not be deleted.',nomFicNc);
            logFileId.warn('eraseSonarScopeCache', msg);
        end
    end
end
my_close(hw)
