% listeIn = {'abcd'; 'bcde'; 'cdefg'; 'ghij'; 'jklm'; 'pppp'}
% StrIncluse = [];
% StrExclue = [];
% ListeOut = filterListFiles(listeIn, StrIncluse, StrExclue)
%
% StrIncluse = 'c';
% StrExclue = [];
% ListeOut = filterListFiles(listeIn, StrIncluse, StrExclue)
%
% StrIncluse = {'b'; 'c'};
% StrExclue = [];
% ListeOut = filterListFiles(listeIn, StrIncluse, StrExclue)
%
% StrIncluse = [];
% StrExclue = 'e';
% ListeOut = filterListFiles(listeIn, StrIncluse, StrExclue)
%
% StrIncluse = 'j';
% StrExclue = 'e';
% ListeOut = filterListFiles(listeIn, StrIncluse, StrExclue)

function ListeOut = filterListFiles(listeIn, StrIncluse, StrExclue)

N = numel(listeIn);
ListeOut = cell(N, 1);

if ischar(StrIncluse)
    StrIncluse = {StrIncluse};
end

if ischar(StrExclue)
    StrExclue = {StrExclue};
end

iFic = 0;
for k1=1:N
    nomFic = listeIn{k1};
    flagOK = true;
    for k2=1:length(StrIncluse)
        if ~isempty(StrIncluse{k2})
            flagDataTypeOK = false;
            k = strfind(nomFic, StrIncluse{k2});
            if ~isempty(k)
                flagDataTypeOK = true;
            end
            flagOK = flagOK && flagDataTypeOK;
        end
    end
    for k2=1:length(StrExclue)
        if ~isempty(StrExclue{k2})
            k = strfind(nomFic, StrExclue{k2});
            if ~isempty(k)
                flagOK = false;
            end
        end
    end
    
    if flagOK
        iFic = iFic + 1;
        ListeOut{iFic} = nomFic;
    end
end

ListeOut = ListeOut(1:iFic);
