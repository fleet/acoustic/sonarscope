% Nom complet d'un fichier contenu dans les repertoires de donnees d'exemple
% Le fichier est recherche dans le repertoire defini par SonarScopeData
%
% Syntax
%   listeFic = findInDatabase(filtre)
%
% INTPUT PARAMETERS : 
%   filtre : 'toto*.imo' ou '*.xyz' ou '*titi*', etc ...
%
% Output Arguments 
%   listeFic : Nom complet du fichier ([] si fichier pas trouve)
%
% Examples
%   listeFic = findInDatabase('*.tif')
%   I = imread(listeFic{1});
%   figure; imagesc(I); colormap(gray(256)); colorbar;
%
%   listeFic = findInDatabase('*.tif', '*.jpg', '*.png')
%
% See also Lena Prismed ImageSonar Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function listeFic = findInDatabase(varargin)

global SonarScopeData %#ok<GVMIS>
global IfrTbxResources %#ok<GVMIS>

listeFic = [];

if isempty(IfrTbxResources)
    nomFicSave = fullfile(IfrTbxResources, 'Resources.ini');
%     verifNameLength(nomFicSave)
    if exist(nomFicSave, 'file')
        loadVariablesEnv;
    end
end

if ischar(SonarScopeData)
    for k=1:nargin
        liste = findFicInDir(fullfile(SonarScopeData, 'Public'), varargin{k});
        listeFic = [listeFic; liste]; %#ok<AGROW>
    end

    for k=1:nargin
        liste = findFicInDir(fullfile(SonarScopeData, 'Sonar'), varargin{k});
        listeFic = [listeFic; liste]; %#ok<AGROW>
    end

    for k=1:nargin
        liste = findFicInDir(fullfile(SonarScopeData, 'Levitus'), varargin{k});
        listeFic = [listeFic; liste]; %#ok<AGROW>
    end
end
