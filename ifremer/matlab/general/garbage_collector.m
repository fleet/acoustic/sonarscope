function garbage_collector(varargin)

[varargin, All] = getFlag(varargin, 'All'); %#ok<ASGLU>

S = warning;

liste = listeFicOnDir(my_tempdir, '.mem');
warning('off')
for i=1:length(liste)
    delete(liste{i})
end

if All
    liste = listeFicOnDir(my_tempdir, '.SonarScope');
    warning('off')
    for i=1:length(liste)
        delete(liste{i})
    end
end

warning(S)
