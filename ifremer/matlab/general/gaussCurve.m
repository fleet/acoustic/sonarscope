% Description
%   Calcul d'une courbe de gauss
%
% Syntax
%   y = gaussCurve(x, Parametres)
%
% Input Arguments
%   x : abscissa
%   Parametres  : 
%		meanVal  : Center of the curve
%		sigmaVal : Standars deviation
%		minVal   : Min value of the curve
%		maxVal   : Max value of the curve
%
% Output Arguments
%   [] : Auto-plot activation
%   y  : Ordinates
%
% Examples
%   x = -100:100;
%   gaussCurve(x, [0 20 0 10]);
%
%   y = gaussCurve(x, [0 20 0 10]);
%   plot(x, y); grid on; zoom on; hold on;
%
%   x = 100:400;
%   gaussCurve(x, [200 30 20 80]);
%
% Authors : JMA
% See also gaussCurveFit Authors
%
% Reference pages in Help browser
%   <a href="matlab:doc gaussCurve">gaussCurve</a>
%------------------------------------------------------------------------------

function [y, composantes] = gaussCurve(x, Parametres)

% Pr�paration de la liste des x si elle est 2 D

[m, n] = size(x);
if m > 1
    x = reshape(x, 1, m*n);
end

nbCourbes = size(Parametres, 1);

meanVal  = Parametres(:,1);
sigmaVal = Parametres(:,2);
minVal   = Parametres(:,3);
maxVal   = Parametres(:,4);

%% Lambert

y = normpdf(x, meanVal, sigmaVal);
y = y / max(y);
y = minVal + (maxVal - minVal) * y;

composantes = y;

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    figure
    if nbCourbes == 1
        plot(x, y, 'k'); grid on; hold on;
        xlabel('x'); ylabel('y');
        title('gaussCurve');
    else
        plot(x, y); grid on; hold on;
    end
end
