% Description
%   Fit of a gaussian curve
%
% Syntax
%   [params, str] = gaussCurveFit(x, y, ...)
%
% Input Arguments
%   x : abscissa
% 
% Name-Value Pair Arguments
%   GUI          : 1:open a OptimDialog GUI, 0:do not ...
%   ValParams    : Init values of the parameters [meanVal sigmaVal minVal maxVal]
%   ValMinParams : Init values of min values of the parameters
%   ValMaxParams : Init values of max values of the parameters
%   Enable       : Default [1 1 1 1]
%
% Output Arguments
%   Parametres  : 
%		meanVal  : Center of the curve
%		sigmaVal : Standars deviation
%		minVal   : Min value of the curve
%		maxVal   : Max value of the curve
%
% Examples
%   x = 100:400;
%   y = gaussCurve(x, [200 30 20 80]);
%   y = y + 10 * (randn(size(x)) - 0.5);
%   fig = figure; plot(x, y); grid on;
% [params, str] = gaussCurveFit(x, y)
% [params, str] = gaussCurveFit(x, y, 'ValParams', [200 30 20 80])
%   yFitted = gaussCurve(x, params);
%   figure(fig); hold on; plot(x, yFitted, 'r'); grid on; title(str)

%
% Authors : JMA
% See also gaussCurve Authors
%
% Reference pages in Help browser
%   <a href="matlab:doc gaussCurveFit">gaussCurve</a>
%------------------------------------------------------------------------------

function [params, str] = gaussCurveFit(x, y, varargin)

[varargin, flagGUI]      = getPropertyValue(varargin, 'GUI',          0);
[varargin, ValParams]    = getPropertyValue(varargin, 'ValParams',    []);
[varargin, ValMinParams] = getPropertyValue(varargin, 'ValMinParams', []);
[varargin, ValMaxParams] = getPropertyValue(varargin, 'ValMaxParams', []);
[varargin, Enable]       = getPropertyValue(varargin, 'Enable',       [1 1 1 1]); %#ok<ASGLU>

if isempty(ValParams)
    ValParams(1) = median(x);
    ValParams(2) = range(x) / 5;
    ValParams(3) = min(y);
    ValParams(4) = max(y);
end

if isempty(ValMinParams)
    ValMinParams(1) = min(x);
    ValMinParams(2) = range(x) / 100;
    ValMinParams(3) = min(y);
    ValMinParams(4) = min(y);
end

if isempty(ValMaxParams)
    ValMaxParams(1) = max(x);
    ValMaxParams(2) = range(x) * 5;
    ValMaxParams(3) = max(y);
    ValMaxParams(4) = max(y);
end

Params(1) = ClParametre(...
    'Name',     'meanVal', ...
    'MinValue', ValMinParams(1), ...
    'MaxValue', ValMaxParams(1), ...
    'Value',    ValParams(1), ...
    'Enable',   Enable(1));

Params(2) = ClParametre(...
    'Name',     'sigmaVal', ...
    'MinValue', ValMinParams(2), ...
    'MaxValue', ValMaxParams(2), ...
    'Value',    ValParams(2), ...
    'Enable',   Enable(2));

Params(3) = ClParametre(...
    'Name',     'minVal', ...
    'MinValue', ValMinParams(3), ...
    'MaxValue', ValMaxParams(3), ...
    'Value',    ValParams(3), ...
    'Enable',   Enable(3));

Params(4) = ClParametre(...
    'Name',     'maxVal', ...
    'MinValue', ValMinParams(4), ...
    'MaxValue', ValMaxParams(4), ...
    'Value',    ValParams(4), ...
    'Enable',   Enable(4));

%% Create the model

m = ClModel('Name', 'gaussCurve', ...
    'Params', Params, ...
    'XData', x, 'YData', y, ...
    'xLabel', 'x', 'yLabel', 'y');

%% Create the optim instance

optim = ClOptim('XData', x, 'YData', y, 'models', m);
% optim.plot()
for k=20:-1:1
    optim = random(optim);
    optim = optimize(optim);
%     optim.plot()
    rms(k) = optim.models.rms(1);
    p(k,:) = optim.getParamsValue;
end
% figure; plot(rms); grid on
[~, ind] = min(rms);
optim.setParamsValue(p(ind,:));

%% Create the OptimDialog instance and open the gui

if flagGUI
    a = OptimUiDialog(optim, 'Title', 'Optim gaussCurve', 'windowStyle', 'normal');
    a.openDialog();
end
    
%% Get the fitted parameters

params = optim.getParamsValue;

%% Summary

str = sprintf('mean=%s  sigma=%s  min=%s  max=%s  Height=%s', ...
   num2str(params(1),2), num2str(params(2),2), num2str(params(3),2), num2str(params(4),2), num2str(params(4)- params(3),2));
