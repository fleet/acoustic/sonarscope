% Grids the geoid data
%
% Syntax
%   [Lon, Lat, F, G] = geoidmodel_grid(lon, lat, f, g)
%
% Input Arguments
%   lon  : List of longitudes (deg)
%   lat  : List of latitudes (deg)
%   f    : List of distances from the ellipsoid to the geoid (m)
%   g    : List of distances from thr geoid to the vertical reference (m)
%
% Output Arguments
%   Lon  : Longitudes of the grid (deg)
%   Lat  : Latitudes of the grid (deg)
%   F    : Grid of distances from the ellipsoid to the geoid (m)
%   G    : Grid of distances from thr geoid to the vertical reference (m)
%
% Examples
%   nomFicGeoid = 'D:\Temp\SPFE\geoidmodel.geoid';
%   [flag, lon, lat, f, g] = geoidmodel_read(nomFicGeoid);
%
%   [Lon, Lat, F, G] = geoidmodel_grid(lon, lat, f, g);
%   figure; mesh(Lon, Lat , F); title('F');
%   figure; mesh(Lon, Lat , G); title('G');
%
% See also geoidmodel_read geoidmodel_interp Authors
% References : 342682ac_sis_reference_manual_em2040.pdf pages 494-496
% Authors    : JMA
%-------------------------------------------------------------------------------

function [Lon, Lat, F, G] = geoidmodel_grid(lon, lat, f, g)

warning('off', 'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

Lon = unique(lon);
Lat = unique(lat);
[qx,qy] = meshgrid(Lon,Lat);

TSI = scatteredInterpolant(lon, lat, f);
F = TSI(qx,qy);

TSI = scatteredInterpolant(lon, lat, g);
G = TSI(qx,qy);

warning('on', 'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');
