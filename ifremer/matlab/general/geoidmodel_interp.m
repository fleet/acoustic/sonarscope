% Grids the geoid data
%
% Syntax
%   [Lon, Lat, F, G] = geoidmodel_grid(lon, lat, f, g)
%
% Input Arguments
%   lon  : List of longitudes (deg)
%   lat  : List of latitudes (deg)
%   f    : List of distances from the ellipsoid to the geoid (m)
%   g    : List of distances from thr geoid to the vertical reference (m)
%
% Output Arguments
%   Lon  : Longitudes of the grid (deg)
%   Lat  : Latitudes of the grid (deg)
%   F    : Grid of distances from the ellipsoid to the geoid (m)
%   G    : Grid of distances from thr geoid to the vertical reference (m)
%
% Examples
%   nomFicGeoid = 'D:\Temp\SPFE\geoidmodel.geoid';
%   [flag, lon, lat, f, g] = geoidmodel_read(nomFicGeoid);
%
%   Lon = lon(1:11:end);
%   Lat = lat(1:11:end);
%   [F, G] = geoidmodel_interp(lon, lat, f, g, Lon, Lat);
%
%   [Lon2, Lat2, F2, G2] = geoidmodel_grid(lon, lat, f, g);
%   figure; mesh(Lon2, Lat2 , F2); title('F'); hold on; plot3(Lon, Lat, F, 'or')
%   figure; mesh(Lon2, Lat2 , G2); title('G'); hold on; plot3(Lon, Lat, G, 'or')
%
% See also geoidmodel_read geoidmodel_grid Authors
% References : 342682ac_sis_reference_manual_em2040.pdf pages 494-496
% Authors    : JMA
%-------------------------------------------------------------------------------

function [F, G] = geoidmodel_interp(lon, lat, f, g, Lon, Lat)

warning('off', 'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

TSI = scatteredInterpolant(lon, lat, f);
F = TSI(Lon, Lat);

TSI = scatteredInterpolant(lon, lat, g);
G = TSI(Lon, Lat);

warning('on', 'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');
