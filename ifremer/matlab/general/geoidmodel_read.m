% Reads the geodif file produced by SIS
%
% Syntax
%   [flag, Lon, Lat, F, G] = geoidmodel_read(nomFicGeoid)
%
% Input Arguments
%   nomFicGeoid : File name of the geoide description (delivered by SIS)
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   Lon  : Longitudes (deg)
%   Lat  : Latitudes (deg)
%   F    : Distance from the ellipsoid to the geoid (m)
%   G    : Distance from thr geoid to the vertical reference (m)
%
% Examples
%   nomFicGeoid = 'D:\Temp\SPFE\geoidmodel.geoid';
%   [flag, Lon, Lat, F, G] = geoidmodel_read(nomFicGeoid);
%
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(Lon, Lat, '*'); grid on; title('X&Y');
%   FigUtils.createSScFigure; plot3(Lon, Lat, F, '*'); grid on; title('F');
%   FigUtils.createSScFigure; plot3(Lon, Lat, G, '*'); grid on; title('G');
%
% See also geoidmodel_grid geoidmodel_interp Authors
% References : 342682ac_sis_reference_manual_em2040.pdf pages 494-496
% Authors    : geoidmodel_grid geoidmodel_interp JMA
%-------------------------------------------------------------------------------

function [flag, Lon, Lat, F, G] = geoidmodel_read(nomFicGeoid)

Lon = [];
Lat = [];
F   = [];
G   = [];

if iscell(nomFicGeoid)
    nomFicGeoid = nomFicGeoid{1};
end

flag = exist(nomFicGeoid, 'file');
if ~flag
    return
    
    %{
    nomFicGeoidEmpty = [nomFicGeoid '_empty'];
    if exist(nomFicGeoidEmpty, 'file');
        return
    end
    
    nomDir = fileparts(nomFicGeoid);
    strListe{1} = Lang('Traiter ce fichier sans RTK', 'Process the file without RTK');
    strListe{2} = Lang('Tenter de trouver le fichier et poursuivre le traitement avec RTK (Cliquez sur OK seulement lorsque vous aurez copi� le fichier)', 'Try to find the file, and continue the processing with RTK (clik OK once you have copied the file)');
    strListe{3} = Lang('Traiter tous les fichiers de ce r�pertoire sans RTK', 'Process all the files of this directory without RTK');
    str1 = sprintf('Le fichier "geoidmodel.geoid" n''a �t� trouv� dans le r�pertoire "%s", il n''est pas possible d''utiliser l''information RTK sans lui. Que voulez-vous faire ?', nomDir);
    str2 = sprintf('"geoidmodel.geoid" was not found in folder "%s", it is not possible to use the RTK data. What do you want to do ?', nomDir);
    [repGeoid, flag] = my_listdlg(Lang(str1,str2), strListe, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    switch repGeoid
        case 1
            flag = 0;
            return
        case 2
            flag = exist(nomFicGeoid, 'file');
            if ~flag
                return
            end
        case 3
            fid = fopen(nomFicGeoidEmpty, 'w+');
            fclose(fid);
            flag = 0;
            return
    end
    %}
end

[flag, Lon, Lat, V] = read_ASCII(nomFicGeoid, 'DisplayEntete', 0, 'NbLigEntete', 0, ...
    'AskSeparator', 0, 'Separator', [], 'AskValNaN', 0, 'ValNaN', [], ...
    'nColumns', 5, 'AskGeometryType', 0, 'GeometryType', 3, ...
    'IndexColumns', [3 2 4 5 1], 'DataType', 2, 'Unit', 'm', 'ScaleFactors', [1 1 1 1 1]);
if ~flag
    messageErreurFichier(nomFicGeoid, 'ReadFailure');
    return
end

% Bidouille pour fichier Geoid du FSK G:\FSK\Course\Geoid\gcg05_kueste.txt
if isempty(Lon)
    [flag, Lon, Lat, V] = read_ASCII(nomFicGeoid, 'DisplayEntete', 0, 'NbLigEntete', 0, ...
        'AskSeparator', 0, 'Separator', [], 'AskValNaN', 0, 'ValNaN', [], ...
        'nColumns', 5, 'AskGeometryType', 0, 'GeometryType', 3, ...
        'IndexColumns', [2 1 3], 'DataType', 2, 'Unit', 'm', 'ScaleFactors', [1 1 1 1 1]);
    F = V(:,1);
    G = zeros(size(F));
else
    F = V(:,1);
    G = V(:,2);
end
