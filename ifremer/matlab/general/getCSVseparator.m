function sep = getCSVseparator

codeLang = winqueryreg('HKEY_CURRENT_USER', 'Control Panel\International', 'LocaleName');
switch codeLang
    case 'fr-FR'
        sep = ';';
    otherwise
        sep = ',';
end
