% Creates the "Parameters" structure for the History of an image
%
% Syntax
%   Parameters = getHistoryParameters(...)
%
% Name-Value Pair Arguments
%   Variables containing the values of the parameters
%
% Output Arguments
%   Parameters : Structure containing Name and Value for every input name
%
% Remarks : The variables have to be simple variables (not values or fields of structures, 
%           see example for tete.bug, 3.14 and pi) 
%
% Examples
%   toto = 'Foo';
%   tata = 2;
%   titi = 1:10;
%   tete.bug = 9999;
%   Parameters = getHistoryParameters(toto, tata, titi, tete.bug, 3.14, pi)
%   for k=1:length(Parameters), Parameters(k), end
%
% See also cl_image/inherit cl_image/private/char_display Authors
% Authors  : JMA + GLU
%-------------------------------------------------------------------------------

function Parameters = getHistoryParameters(varargin)

for k=length(varargin):-1:1
    Parameters(k).Name  = inputname(k);
    Parameters(k).Value = varargin{k};
end