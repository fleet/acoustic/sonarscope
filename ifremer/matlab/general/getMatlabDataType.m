
function s2 = getMatlabDataType(s1)

switch s1
    case 'float32'
        s2 = 'single';
    otherwise
        s2 = s1;
end
