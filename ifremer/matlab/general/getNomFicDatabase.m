% Nom complet d'un fichier contenu dans les repertoires de donnees d'exemple
% Le fichier est recherche dans le repertoire defini par SonarScopeData
%
% Syntax
%   nomFullFic = getNomFicDatabase(nomFic)
%
% INTPUT PARAMETERS : 
%   nomFic : Nom du fichier recherche
%
% Output Arguments 
%   nomFullFic : Nom complet du fichier ([] si fichier pas trouve)
%
% Examples
%   nomFic = getNomFicDatabase('lena.tif')
%   I = imread(nomFic);
%   figure; imagesc(I); colormap(gray(256)); colorbar;
%
% See also Lena Prismed ImageSonar Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [nomFullFic, flag] = getNomFicDatabase(nomFic, varargin)

global IfrTbxResources %#ok<GVMIS>
global SonarScopeDocumentation %#ok<GVMIS>
global SonarScopeData %#ok<GVMIS>

% SonarScopeDocumentation
% nomFic

[varargin, NoMessage] = getFlag(varargin, 'NoMessage'); %#ok<ASGLU>

flag = 1;

%% D�finition des chemins

if isempty(IfrTbxResources)
    if isdeployed
        IfrTbxResources = pwd;
    else
        IfrTbxResources = 'C:\SScTbx\SScTbxResources';
    end

    if isfolder(IfrTbxResources)
        nomFicSave = fullfile(IfrTbxResources, 'Config.txt');
    else
        str1 = sprintf('R�pertoire "%s" inexistant (SonarScope ne peut fonctionner sans le fichier "Resources.ini" qu''il devrait se trouver dans ce r�pertoire).', nomDir);
        str2 = sprintf('Directory "%s" does not exist (SonarScope needs "Resources.ini" file that should be in this directory).', nomDir);
        my_warndlg(Lang(str1,str2),1);
        return
    end
    
    %verifNameLength(nomFicSave)
    if exist(nomFicSave, 'file')
        loadVariablesEnv;
        if strcmp(IfrTbxResources, 'NULL')
            str1 = 'Le r�pertoire SScTbxResources, n�cessaire, n''a pas �t� correctement copi� sur la machine, veuillez red�marrer l''installation ou contactez sonarscope@ifremer.fr';
            str2 = 'The mandatory directory IfremerToolBoxDataPublic was not correctly copied in this machine.Please, restart the installation or contact  sonarscope@ifremer.fr';
            my_warndlg(Lang(str1,str2),1);
            exit
        end
        if ~exist(IfrTbxResources, 'dir')
            loadVariablesEnv;
        end
    else
        loadVariablesEnv;
    end
end

%% IfrTbxResources

if ischar(IfrTbxResources)
    nomFullFic = fullfile(IfrTbxResources, nomFic);
    if exist(nomFullFic, 'file')
        return
    elseif exist([nomFullFic '.gz'], 'file')
        disp(['Uncompress : ' [nomFullFic '.gz'] ' Please wait'])
        gunzip([nomFullFic '.gz'], IfrTbxResources);
        delete([nomFullFic '.gz'])
        return
    else
        nomFullFic = fullfile(IfrTbxResources, 'IconesLogiciel', nomFic);
        if exist(nomFullFic, 'file')
            return
        end
        nomFullFic = fullfile(IfrTbxResources, 'ErMapper', nomFic);
        if exist(nomFullFic, 'file')
            return
        end
        nomFullFic = fullfile(IfrTbxResources, 'XML_sounder', nomFic);
        if exist(nomFullFic, 'file')
            return
        end
        nomFullFic = fullfile(IfrTbxResources, 'KongsbergBSCorr', nomFic);
        if exist(nomFullFic, 'file')
            return
        end
        nomFullFic = fullfile(IfrTbxResources, 'MapTbx', nomFic);
        if exist(nomFullFic, 'file')
            return
        end
        nomFullFic = fullfile(IfrTbxResources, 'Sounds', nomFic);
        if exist(nomFullFic, 'file')
            return
        end
        nomFullFic = fullfile(IfrTbxResources, 'GifAnimated', nomFic);
        if exist(nomFullFic, 'file')
            return
        end
    end
end

%% SonarScopeDocumentation

if ischar(SonarScopeDocumentation)
    nomFullFic = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'SScHelp-ADOC', nomFic);
    if strcmp(SonarScopeDocumentation, 'NULL')
        str1 = 'Le r�pertoire IfremerToolBoxDoc n''a pas �t� correctement copi� sur la machine, veuillez red�marrer l''installation ou contactez sonarscope@ifremer.fr';
        str2 = 'The directory IfremerToolBoxDoc was not correctly copied in this machine.Please, restart the installation or contact sonarscope@ifremer.fr';
        str = Lang(str1,str2);
        my_warndlg(str,1);
    end
    %verifNameLength(nomFullFic)
    if exist(nomFullFic, 'file')
        return
    end
    nomFullFic = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'PAMES-ADOC', nomFic);
    %verifNameLength(nomFullFic)
    if exist(nomFullFic, 'file')
        return
    end
end

%% SonarScopeData

if isempty(SonarScopeData)
    nomFicSave = fullfile(IfrTbxResources, 'Resources.ini');
    %verifNameLength(nomFicSave)
    if exist(nomFicSave, 'file')
        loadVariablesEnv;
        if strcmp(SonarScopeData, 'NULL')
            str1 = 'Le r�pertoire IfremerToolBoxDataPublic, n�cessaire, n''a pas �t� correctement copi� sur la machine, veuillez red�marrer l''installation ou contactez sonarscope@ifremer.fr';
            str2 = 'The mandatory directory IfremerToolBoxDataPublic was not correctly copied in this machine.Please, restart the installation or contact sonarscope@ifremer.fr';
            str = Lang(str1,str2);
            my_warndlg(str,1);
            exit;
        end
        if ~exist(SonarScopeData, 'dir')
            loadVariablesEnv;
        end
    else
        loadVariablesEnv;
    end
end

%% SonarScopeData/Public

if ischar(SonarScopeData)
    nomFullFic = fullfile(SonarScopeData, 'Public', nomFic);
    if exist(nomFullFic, 'file')
        return
    elseif exist([nomFullFic '.gz'], 'file')
        disp(['Uncompress : ' [nomFullFic '.gz'] ' Please wait'])
        gunzip([nomFullFic '.gz'], fullfile(SonarScopeData, 'Public'));
        delete([nomFullFic '.gz'])
        return
    end
end

%% SonarScopeData/Sonar

if ischar(SonarScopeData)
    nomFullFic = fullfile(SonarScopeData, 'Sonar', nomFic);
    if exist(nomFullFic, 'file')
        return
    elseif exist([nomFullFic '.gz'], 'file')
        disp(['Uncompress : ' [nomFullFic '.gz'] ' Please wait'])
        gunzip([nomFullFic '.gz'], fullfile(SonarScopeData, 'Sonar'));
        delete([nomFullFic '.gz'])
        return
    end
end

%% SonarScopeData/Levitus

if ischar(SonarScopeData)
    if strcmp(SonarScopeData,'NULL')
        str1 = 'Le r�pertoire IfremerToolBoxPublicSonar n''a pas �t� correctement copi� sur la machine, veuillez red�marrer l''installation ou contactez sonarscope@ifremer.fr';
        str2 = 'The directory IfremerToolBoxPublicSonar was not correctly copied in this machine.Please, restart the installation or contact sonarscope@ifremer.fr';
        str = Lang(str1,str2);
        my_warndlg(str,1);
    end
    
    nomFullFic = fullfile(SonarScopeData, 'Levitus', nomFic);
    %verifNameLength(nomFullFic)
    if exist(nomFullFic, 'file')
        return
    elseif exist([nomFullFic '.gz'], 'file')
        disp(['Uncompress : ' [nomFullFic '.gz'] ' Please wait'])
        gunzip([nomFullFic '.gz'], fullfile(SonarScopeData, 'Levitus'));
        delete([nomFullFic '.gz'])
        return
    end
    
    nomFullFic = fullfile(SonarScopeData, 'Levitus', nomFic);
    %verifNameLength(nomFullFic)
    if exist(nomFullFic, 'file')
        return
    elseif exist([nomFullFic '.gz'], 'file')
        disp(['Uncompress : ' [nomFullFic '.gz'] ' Please wait'])
        gunzip([nomFullFic '.gz'], fullfile(SonarScopeData, 'Levitus'));
        delete([nomFullFic '.gz'])
        return
    end
end

%% SonarScopeData/WOA13

if ischar(SonarScopeData)
    if strcmp(SonarScopeData,'NULL')
        str1 = 'Le r�pertoire IfremerToolBoxPublicSonar n''a pas �t� correctement copi� sur la machine, veuillez red�marrer l''installation ou contactez sonarscope@ifremer.fr';
        str2 = 'The directory IfremerToolBoxPublicSonar was not correctly copied in this machine.Please, restart the installation or contact sonarscope@ifremer.fr';
        str = Lang(str1,str2);
        my_warndlg(str,1);
    end
    
    nomFullFic = fullfile(SonarScopeData, 'WOA13', nomFic);
    %verifNameLength(nomFullFic)
    if exist(nomFullFic, 'file')
        return
    elseif exist([nomFullFic '.gz'], 'file')
        disp(['Uncompress : ' [nomFullFic '.gz'] ' Please wait'])
        gunzip([nomFullFic '.gz'], fullfile(SonarScopeData, 'WOA13'));
        delete([nomFullFic '.gz'])
        return
    end
    
    nomFullFic = fullfile(SonarScopeData, 'WOA13', nomFic);
    %verifNameLength(nomFullFic)
    if exist(nomFullFic, 'file')
        return
    elseif exist([nomFullFic '.gz'], 'file')
        disp(['Uncompress : ' [nomFullFic '.gz'] ' Please wait'])
        gunzip([nomFullFic '.gz'], fullfile(SonarScopeData, 'WOA13'));
        delete([nomFullFic '.gz'])
        return
    end
end

%% Pas trouv�

nomFullFic = [];
if ~NoMessage
    str1 = sprintf('Le fichier "%s" n''est pas pr�sent dans la base de donnees d''exemples.', nomFic);
    str2 = sprintf('File "%s" was not found in the SonarScope Data directories.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
end
flag = 0;
