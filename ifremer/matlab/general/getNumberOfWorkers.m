function MaxValue = getNumberOfWorkers

c = parcluster;
nbProc = str2double(getenv('NUMBER_OF_PROCESSORS'));
MaxValue = max(floor(nbProc / 2), c.NumWorkers);

% Ex Intel Xeon Silver 4214 du MDII (2 processeurs):
% Nb de coeurs  : 24
% Nb de processeurs logiques (threads) : 48
% Nb de workers propos� par Matlab : 48, nb propos� par SSc au d�part ; 24
% mais r�duction manuelle � 12 pour �viter des plantage Netcdf

% Mon PC portable Lenovo :
% Nb de coeurs : 4
% Nb de processeurs logiques : 8
% Nb de workers propos� par Matlab : 4

% Quelle est la logiques de Matlab ?

%% Des pb ont �t� rencontr�s sur les I/O Netcdf sur le MDII sur des stations de travail trop puissantes

% Pour voir les infos, afficher le gestionnaire de t�ches puis le panneau Performance, 
% les infos sont �crites en dessous du graphe

if MaxValue >= 24
    MaxValue = floor(MaxValue / 2);
end
