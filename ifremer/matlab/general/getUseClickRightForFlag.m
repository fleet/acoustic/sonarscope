function rep = getUseClickRightForFlag

global IfrTbxUseClickRightForFlag %#ok<GVMIS>

if strcmpi(IfrTbxUseClickRightForFlag, 'on')
    rep = true;
else
    rep = false;
end


