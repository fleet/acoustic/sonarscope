function Astuce = get_Astuce(varargin)

[varargin, index] = getPropertyValue(varargin, 'index', []); %#ok<ASGLU>

nomFicAstuce = getNomFicDatabase('Astuces.xml');
if isempty(nomFicAstuce)
    str1 = 'Le fichier "Astuces.xml" n''a pas �t� trouv�.';
    str2 = 'File "Astuces.xml" was not found.';
    Astuce = Lang(str1,str2);
    my_warndlg(Astuce,1);
else
    try
        X = xml_read(nomFicAstuce);
        if isempty(index)
            k = randi(length(X.Astuces));
        else
            k = mod(index-1, length(X.Astuces)) + 1;
        end
        Astuce = Lang(X.Astuces(k).FR, X.Astuces(k).US);
    catch %#ok<CTCH>
        messageErreurFichier(nomFicAstuce, 'ReadFailure');
    end
end

%% Test de SonarScopeData

global SonarScopeData %#ok<GVMIS>

if exist(SonarScopeData, 'dir')
    nomDir1 = fullfile(SonarScopeData, 'Levitus');
    nomDir2 = fullfile(SonarScopeData, 'Public');
    nomDir3 = fullfile(SonarScopeData, 'Sonar');
    if ~exist(nomDir1, 'dir') || ~exist(nomDir2, 'dir') || ~exist(nomDir3, 'dir')
        str1 = 'L''organisation et le contenu du r�pertoire SonarScopeData ont �t� modifi�s recemment. Refaites une installation compl�te des data (t�l�chargement du .zip et ex�cution du SetUpSonarScope_Data_20yymmdd_R20yyv.exe � partir du site ftp)';
        str2 = 'The organization and the contents of SonarScopeData directory changed recently. Please redo a full installation (download SonarScopeData.zip and run SetUpSonarScope_Data_20yymmdd_R20yyv.exe';
        my_warndlg(Lang(str1,str2), 1);
    end
else
    str1 = 'Le r�pertoire "SonarScopeData" ne semble pas bien install�. SetUpSonarScope_Data_20161021_R2016b.exe';
    str2 = '"SonarScopeData" does not seem to be installed';
    my_warndlg(Lang(str1,str2), 1);
end
