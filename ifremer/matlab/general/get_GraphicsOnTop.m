function val = get_GraphicsOnTop

global IfrTbxGraphicsOnTop %#ok<GVMIS>
val = str2double(IfrTbxGraphicsOnTop);
