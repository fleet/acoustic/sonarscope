function val = get_LevelQuestion

global IfrTbxLevelQuestions %#ok<GVMIS>
val = str2double(IfrTbxLevelQuestions);
