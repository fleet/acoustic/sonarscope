function I = get_PingBeamMatrixFromPingSignal(Signal, TransmitSectorNumber)

sz = size(TransmitSectorNumber);
I = NaN(sz);
for k=1:sz(1)
    ind = TransmitSectorNumber(k,:);
    subNonNan = ~isnan(ind);
    STD = Signal(k,:);
    I(k,subNonNan) = STD(ind(subNonNan));
end
