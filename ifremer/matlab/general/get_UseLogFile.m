function val = get_UseLogFile

global IfrTbxUseLogFile %#ok<GVMIS>
val = strcmp(IfrTbxUseLogFile, 'On');
