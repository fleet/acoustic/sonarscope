function val = get_UseParallelTbx

global useParallel %#ok<GVMIS>

val = useParallel;

if isempty(val)
    [~, val] = question_NumberOfWorkers;
    useParallel = val;
end
