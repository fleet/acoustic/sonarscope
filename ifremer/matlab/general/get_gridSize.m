% Description
%   Input of a grid size to create a mosaic or a DTM
%
% Syntax
%   [flag, resol] = get_gridSize(...)
%
% Name-Value Pair Arguments
%   Value       : Initial value. if Value < 0 see More about section
%   Initial     : Value to be used if the function is used for the first time
%   significant : Number of significant digits [16]
%
% Output Arguments
%   flag  : 0=KO, 1=OK
%   resol : grid size (m)
%
% More About : If Value <0, -Value is returned without editing the value
%
% Examples
%   [flag, resol] = get_gridSize
%   [flag, resol] = get_gridSize
%   [flag, resol] = get_gridSize('Value', [])
%   [flag, resol] = get_gridSize('Value', [], 'Initial', 2)
%   [flag, resol] = get_gridSize('Value', 33)
%   [flag, resol] = get_gridSize('Value', -33)
%   [flag, resol] = get_gridSize('Value', pi*100)
%   [flag, resol] = get_gridSize('Value', pi*100, 'significant', 2)
%   [flag, resol] = get_gridSize('Value', pi*100, 'significant', 16)
%
% Authors  : JMA
% See also : round
%
% Reference pages in Help browser <a href="matlab:doc get_gridSize">get_gridSize</a>
%-------------------------------------------------------------------------------

function [flag, resol] = get_gridSize(varargin)

persistent persistent_resol

[varargin, Value]       = getPropertyValue(varargin, 'Value',       []);
[varargin, Initial]     = getPropertyValue(varargin, 'Initial',     []);
[varargin, significant] = getPropertyValue(varargin, 'significant', 2); %#ok<ASGLU> % Passage de 3 � 2 par JMA le 22/01/2019

if isempty(Value)
    if isempty(persistent_resol)
        if isempty(Initial)
            Value = 0;
        else
            Value = Initial;
        end
    else
        Value = persistent_resol;
    end
elseif Value < 0
    resol = -Value;
    resol = round(resol, significant, 'significant');
    flag = 1;
    return
end

Value = round(Value, significant, 'significant'); % TODO : � supprimer quand il sera int�gr� dans ClParametre

resol = 0;
while resol == 0
    str1 = 'Taille de la maille de la mosa�que';
    str2 = 'Grid size of the mosaic';
    p = ClParametre('Name', Lang('Taille de la maille','Grid size'), ...
        'Unit', 'm',  'MinValue', 0, 'MaxValue', Inf, 'Value', Value);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 0 0 0 -2 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    resol = a.getParamsValue;
    
    if resol == 0
        str1 = 'La valeur ne peut pas �tre �gale � z�ro.';
        str2 = 'The value cannot be 0';
        my_warndlg(Lang(str1,str2), 1);
    end
end
resol = round(resol, significant, 'significant'); % TODO : � supprimer quand il sera int�gr� dans ClParametre

persistent_resol = resol;
