% Obtention du nombre d'octets occup�s par une variable
%
% Syntax
%   n = get_nbBytes(x)
%
% INTPUT PARAMETERS :
%   x : Variable
%
% Output Arguments
%   n : Nombre d'octets occup�s par une variable
%
% Examples
%   x = zeros(5)
%   n = get_nbBytes(x)
%   x = zeros(5, 'single')
%   n = get_nbBytes(x)
%
% See also whos Authors
% Authors : JMA
%--------------------------------------------------------------------------

function n = get_nbBytes(x) %#ok

s = whos('x');
n = s.bytes;
