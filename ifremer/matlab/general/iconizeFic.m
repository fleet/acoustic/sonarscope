% -------------------------------------------------------------------------
% Fonction iconize pour int�grer un fichier type jpg en icone.
%
% Syntax
%   b = iconizeFic(nomFic)
%
% Input Arguments
%   nomFic : nom et localisation du fichier image.
%
% Output Arguments
%   b : Data icone du fichier image
%
% Examples
%    nomFic = getNomFicDatabase('MoveRightAll.jpg');
%    Icon = iconizeFic(nomFic);
%    Icon(Icon==255) = .8*255;
%
% See also Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function outIcon = iconizeFic(filename)

[pathstr, name, ext] = fileparts(filename);
switch lower(ext)
    case '.gif'
        [x,map] = imread(filename);
        outIcon = ind2rgb(x,map);

        idx = 1;

        % The GIFs we have seem to have the transparent pixels as an index
        % into the last non [0 0 0] entry in the colormap which has the color
        % [1 1 1]. Find this index and make these pixels transparent, using
        % NaNs.
        %
        % Some also have the [1 1 1] entry as the last entry in the
        % colormap. (helpicon.gif
        for k=1:size(map,1)
            if all(map(k,:) == [1 1 1]) && ...
                    (k == size(map,1) || all(map(k+1,:) == [0 0 0]))
                idx = k;
                break
            end
        end

        mask = x==(idx-1); % Zero based.
        [r,c] = find(mask);

        for k=1:length(r)
            outIcon(r(k),c(k),:) = NaN;
        end

    case '.png' % inspir� du makeToolbarIconFromPNG  Creates an icon with transparent
        %   background from a PNG image.

        %   Copyright 2004 The MathWorks, Inc.
        %   $Revision: 1.1.8.1 $  $Date: 2004/08/10 01:50:31 $

        % Read image and alpha channel if there is one.
        [icon, map, alpha] = imread(filename);

        % If there's an alpha channel, the transparent values are 0.  For an RGB
        % image the transparent pixels are [0, 0, 0].  Otherwise the background is
        % cyan for indexed images.
        if ndims(icon) == 3 % RGB
            idx = 0;
            if ~isempty(alpha)
                mask = alpha == idx;
            else
                mask = (icon == idx);
            end

        else % indexed
            % Look through the colormap for the background color.
            for k=1:size(map,1)
                if all(map(k,:) == [0 1 1])
                    idx = k;
                    break
                end
            end
            mask = (icon == (idx-1)); % Zero based.
            icon = ind2rgb(icon,map);
        end

        % Apply the mask.
        icon = im2double(icon);

        for p=1:3
            tmp = icon(:,:,p);
            tmp(mask) = NaN;
            icon(:,:,p) = tmp;
        end
        outIcon = icon;
        
    case '.jpg'
        a = imread(filename);
        % Find the size of the acquired image and determine how much data will need
        % to be lost in order to form a 18x18 icon
        [r,c,d] = size(a); %#ok
        r_skip = ceil(r/18);
        c_skip = ceil(c/18);

        % Create the 18x18 icon (RGB data)
        outIcon = a(1:r_skip:end,1:c_skip:end,:);
        if ismatrix(outIcon)
            outIcon(:,:,2) = outIcon(:,:,1);
            outIcon(:,:,3) = outIcon(:,:,1);
        end
        
    otherwise
        a = imread(filename);
        % Find the size of the acquired image and determine how much data will need
        % to be lost in order to form a 18x18 icon
        [r,c,d] = size(a); %#ok
        r_skip = ceil(r/18);
        c_skip = ceil(c/18);

        % Create the 18x18 icon (RGB data)
        outIcon = a(1:r_skip:end,1:c_skip:end,:);
        if ismatrix(outIcon)
            outIcon(:,:,2) = outIcon(:,:,1);
            outIcon(:,:,3) = outIcon(:,:,1);
        end
end
