% Fonction permettant de passer d'une designation par id en 
% scs (start, count, stride) par combinaison de deux dimensions.
% utilisation dans cl_nccaraibes
% 
% Syntax
%   [start, count, stride] = id2scs(sizescs1, sizescs2, iddeb, idpas, idfin)
%
% Syntax 
%
% Input Arguments
%   - sizescs1 : la taille suivant la premiere dim
%   - sizescs2 : la taille suivant le seconde dim
%   - iddeb : le numero de l'echant de debut
%   - idpas : le pas
%   - idfin : le numero de l'echent de fin
%
% Output Arguments
%    - start : tableau des debuts dans le referentiel 2D
%    - count : tableau des nb elem dans le referentiel 2D
%    - stride : tanbleau de pas dans le referentiel 2D
% 
% Examples
%   [sa, co, st] = id2scs(12, 1024, 1, 1, 10)
%   [sa, co, st] = id2scs(12, 1024, 1023, 1, 2049)
% 
% See also scs2id
% Authors : PP
% VERSION  : $Id: id2scs.m,v 1.2 2002/06/06 11:50:16 augustin Exp $
% ----------------------------------------------------------------------------
function [start, count, stride] = id2scs(d1, d2, deb, pas, fin)

sz = [d2, d1];
[id1, id2] = ind2sub(sz, deb:pas:fin);

uid2 = unique(id2);

for i=1:length(uid2)
    id = find( id2 == uid2(i));
    valid1 = id1(id); %#ok
    start{i} = [uid2(i), valid1(1)]; %#ok
    count{i} = [1, length(valid1)]; %#ok
    stride{i} = [1, pas]; %#ok
end
