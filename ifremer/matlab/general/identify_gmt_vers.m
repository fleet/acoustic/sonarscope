function [flag, keyGMT, pathGMT, versGMT] = identify_gmt_vers

flag = 0;
    try
        keyGMT = getenv('GMT6_SHAREDIR'); % winqueryreg('HKEY_CURRENT_USER', 'Environment', 'GMT6_SHAREDIR');
        pathGMT = regexprep(keyGMT,'share','');
        pathGMT = fullfile(pathGMT, 'bin');
        versGMT = 6;
    catch ME %#ok<NASGU>
        try
            keyGMT  = getenv('GMT5_SHAREDIR'); % winqueryreg('HKEY_CURRENT_USER', 'Environment', 'GMT5_SHAREDIR');
            pathGMT = regexprep(keyGMT5,'share','');
            pathGMT = fullfile(pathGMT, 'bin');
            versGMT = 5;
        catch ME %#ok<NASGU>
            try
                keyGMT = getenv('GMT4_SHAREDIR'); % winqueryreg('HKEY_CURRENT_USER', 'Environment', 'GMT');
                pathGMT = regexprep(keyGMT4,'share','');
                pathGMT = fullfile(pathGMT, 'bin');
                versGMT = 4;
            catch ME %#ok<NASGU>
                my_warndlg(Lang('Impossible de continuer : GMT 4, 5 ou 6 ne sont installÚs sur cet ordinateur !', 'Unable to complete : GMT 4, 5 ou 6 not installed !'),1);
                return;
            end
        end
    end