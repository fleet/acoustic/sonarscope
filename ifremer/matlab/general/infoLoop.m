% infoLoop('test', 311, 2000, 'pings')

function infoLoop(strLoop, k, N, Unit)

persistent persistentTime persistent_kReduit

Latency = 5;
coef = 100 / double(N);
k = double(k);

kReduit = floor(k*coef);
if isempty(persistent_kReduit)
    persistent_kReduit = kReduit;
end
% fprintf('%d  %d - %d  %d - %f  %f\n', floor(k*coef), floor((k-1)*coef), k, N, k*coef, (k-1)*coef);
if kReduit ~= persistent_kReduit
    if isempty(persistentTime)
        persistentTime = now;
    else
        NOW = now;
        diff = NOW - persistentTime;
        if diff < (Latency / (24*3600)) % 3 secondes : 3.5e-05 ~= 3 / (24*3600)
            return
        end
        persistentTime = NOW;
    end
    
    pourcentage = floor(100 * double(k) / double(N));
    strLoop = sprintf('SonarScope:%s : %d / %d %s (%d %%)', strLoop, k, N, Unit, pourcentage);
%     my_warndlg(strLoop, 0);
    fprintf('%s\n', strLoop);
    persistent_kReduit = kReduit;
end
