% Interpolation only when the data is not surrounded by NaN
%
% Syntax
%   y2 = interp0(x1, y1, x2, ...)
%
% Input Arguments
%   x1 : Input abscissa
%   y1 : Input data
%   x2 : Abscissa for interpolation
%
% Optional Input Arguments
%   parameters accepted by interp1
%
% Output Arguments
%   y2 : Interpolated data
%
% Examples
%   y2 = interp0(x1, y1, x2)
%
% See also my_interp1 Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function y2 = interp0(x1, y1, x2, varargin)

z1 = interp1_nearestLeft( x1, y1, x2);
z2 = interp1_nearestRight(x1, y1, x2);
y2 = my_interp1(x1, y1, x2, varargin{:});
sub = isnan(z1) | isnan(z2);
y2(sub) = NaN;
% figure; x0=x1(1); plot(x1-x0,y1, '-ok'); grid on; hold on; plot(x2-x0, z1, '+b'); plot(x2-x0, z2, 'xr'); plot(x2-x0, y2, 'oy')

% TODO : pourrait �tre am�lior� mais l&iss� tel que si besoin
% d'externaliser ces 2 fonctions
function y2 = interp1_nearestLeft(x1, y1, x2)
if isa(y1, 'double')
    y2 = NaN(size(x2), 'double');
else
    y2 = NaN(size(x2), 'single');
end
for k=1:length(x2)
    indx1 = find(x1 <= x2(k), 1, 'last');
    if ~isempty(indx1)
        y2(k) = y1(indx1);
    else
        y2(k) = NaN;
    end
end

function y2 = interp1_nearestRight(x1, y1, x2)
if isa(y1, 'double')
    y2 = NaN(size(x2), 'double');
else
    y2 = NaN(size(x2), 'single');
end
for k=1:length(x2)
    indx1 = find(x1 > x2(k), 1, 'first');
    if ~isempty(indx1)
        y2(k) = y1(indx1);
    else
        y2(k) = NaN;
    end
end
