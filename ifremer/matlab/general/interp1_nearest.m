function y2 = interp1_nearest(x1, y1, x2)

y2 = interp1(x1, y1, x2, 'nearest');

%{
y2 = NaN(size(x2));
for i=1:length(x2)
    dif = abs(x1-x2(i));
    [pppp, ordre] = sort(dif);
    y2(i) = y1(ordre(1));
end
%}
