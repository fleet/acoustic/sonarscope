%{
N1 = 10;
N2 = 19;
x1 = linspace(100, 108, N1);
x2 = linspace(102, 105, N2);
y1 = linspace(21, 37, N1);
y2 = linspace(23, 35, N2);
[x,y] = interpCube(x1, x2, y1, y2)
%}

function [x, y, w] = interpCube(x1, x2, y1, y2)

N1 = length(x1);
a = (N1 - 1) / (x1(end) - x1(1));
b = 1 - a * x1(1);
x3 = a * x2 + b;
k = floor(x3);
xL = x3 - k;
xR = 1 - xL;

x(4,:) = k+1;
x(3,:) = k;
x(2,:) = k+1;
x(1,:) = k;

N2 = length(y1);
a = (N2 - 1) / (y1(end) - y1(1));
b = 1 - a * y1(1);
y3 = a * y2 + b;
k = floor(y3);
yB = y3 - k;
yT = 1 - yB;

y(4,:) = k+1;
y(3,:) = k+1;
y(2,:) = k;
y(1,:) = k;

%{
[X1,Y1] = meshgrid(x1, y1);
[X2,Y2] = meshgrid(x2, y2);
figure; plot(X1, Y1, '*b'); grid on
hold on;  plot(X2, Y2, 'or');


[X1,Y1] = meshgrid(1:length(x1), 1:length(y1));
[X2,Y2] = meshgrid(x3, y3);
figure; plot(X1, Y1, '*b'); grid on
hold on;  plot(X2, Y2, 'or');
%}

%% Calcul des poids (aire du cadrant oppos�)

w(4,:) = xL .* yB;
w(3,:) = xR .* yB;
w(2,:) = xL .* yT;
w(1,:) = xR .* yT;
