% Intersection de 3 series de nombres
%
% Syntax
%   [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, t2, t3)
% 
% Input Arguments 
%   t1  : Premi�re serie
%   t2  : Deuxi�me serie
%   t3  : Troisi�me serie
%
% Output Arguments 
%   sub1 : Index des temps communs dans la serie t1.
%   sub2 : Index des temps communs dans la serie t2.
%   sub3 : Index des temps communs dans la serie t3.
%   tdeb : Temps du debut de l'intersection
%   tfin : Temps de la fin de l'intersection
%
% Examples
%   t1 = 3:10
%   t2 = 2:8
%   t3 = 1:100;
%   [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, t2, t3)
%     % sub1 = 1     2     3     4     5     6
%     % sub2 = 2     3     4     5     6     7
%     % sub3 = 3     4     5     6     7     8
%     % tdeb = 3
%     % tfin = 8
%  t1(sub1)
%     % 3     4     5     6     7     8
%  t2(sub2)
%     % 3     4     5     6     7     8
%  t3(sub3)
%     % 3     4     5     6     7     8
%
%   T1 = fliplr(t1)
%   % 10     9     8     7     6     5     4     3
%   [sub1, sub2, sub3, tdeb, tfin] = intersect3(T1, t2, t3)
%     % sub1 = 8     7     6     5     4     3
%     % sub2 = 2     3     4     5     6     7
%     % sub3 = 3     4     5     6     7     8
%     % tdeb = 3
%     % tfin = 8
%  T1(sub1)
%     % 3     4     5     6     7     8
%  t2(sub2)
%     % 3     4     5     6     7     8
%  t3(sub3)
%     % 3     4     5     6     7     8
%
%   T2 = fliplr(t2)
%   [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, T2, t3)
%     % sub1 = 1     2     3     4     5     6
%     % sub2 = 6     5     4     3     2     1
%     % sub3 = 3     4     5     6     7     8
%     % tdeb = 3
%     % tfin = 8
%  t1(sub1)
%     % 3     4     5     6     7     8
%  T2(sub2)
%     % 3     4     5     6     7     8
%  t3(sub3)
%     % 3     4     5     6     7     8
%
% See also lin_intersect cl_time/max cl_time/sort cl_time/t2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, t2, t3, varargin)

[varargin, flagApprox] = getPropertyValue(varargin, 'Approx', 1); %#ok<ASGLU>

if isa(t1, 'datetime')
    sub1NoNaN = find(~isnat(t1));
    sub2NoNaN = find(~isnat(t2));
    sub3NoNaN = find(~isnat(t3));
    [sub1, sub2, sub3, tdeb, tfin] = intersect3_withoutNaT(datenum(t1(sub1NoNaN)), datenum(t2(sub2NoNaN)), datenum(t3(sub3NoNaN)), flagApprox);
    tdeb = datetime(tdeb, 'ConvertFrom', 'datenum');
    tfin = datetime(tfin, 'ConvertFrom', 'datenum');
else
    sub1NoNaN = find(~isnan(t1));
    sub2NoNaN = find(~isnan(t2));
    sub3NoNaN = find(~isnan(t3));
    [sub1, sub2, sub3, tdeb, tfin] = intersect3_withoutNaN(t1(sub1NoNaN), t2(sub2NoNaN), t3(sub3NoNaN), flagApprox);
end

sub1 = sub1NoNaN(sub1);
sub2 = sub2NoNaN(sub2);
sub3 = sub3NoNaN(sub3);


function [sub1, sub2, sub3, tdeb, tfin] = intersect3_withoutNaN(t1, t2, t3, flagApprox)

sub1 = [];
sub2 = [];
sub3 = [];
tdeb = [];
tfin = [];

if flagApprox && (~isequal(t1, floor(t1)) || ~isequal(t2, floor(t2)) || ~isequal(t3, floor(t3)))
    Deltat1 = mean(diff(t1), 'omitnan') * 1e-1;
    Deltat2 = mean(diff(t2), 'omitnan') * 1e-1;
    Deltat3 = mean(diff(t3), 'omitnan') * 1e-1;
    
    Deltat = min(abs([Deltat1 Deltat2 Deltat3]));
    
    S = warning;
    warning('off')
    t1 = double(int64(t1 / Deltat)) * Deltat;
    t2 = double(int64(t2 / Deltat)) * Deltat;
    t3 = double(int64(t3 / Deltat)) * Deltat;
    warning(S)
end

[~, sub1_12, sub2_12] = intersect(t1, t2);
if isempty(sub1_12)
    return
end

[c, sub1_13, sub3_13] = intersect(t1(sub1_12), t3);
if isempty(sub1_13)
    return
end

sub1 = sub1_12(sub1_13);
sub2 = sub2_12(sub1_13);
sub3 = sub3_13;

tdeb = c(1);
tfin = c(end);



function [sub1, sub2, sub3, tdeb, tfin] = intersect3_withoutNaT(t1, t2, t3, flagApprox)

sub1 = [];
sub2 = [];
sub3 = [];
tdeb = [];
tfin = [];

if flagApprox && (~isequal(t1, floor(t1)) || ~isequal(t2, floor(t2)) || ~isequal(t3, floor(t3)))
    Deltat1 = mean(diff(t1), 'omitnan') * 1e-1;
    Deltat2 = mean(diff(t2), 'omitnan') * 1e-1;
    Deltat3 = mean(diff(t3), 'omitnan') * 1e-1;
    
    Deltat = min(abs([Deltat1 Deltat2 Deltat3]));
    
    S = warning;
    warning('off')
    t1 = double(int64(t1 / Deltat)) * Deltat;
    t2 = double(int64(t2 / Deltat)) * Deltat;
    t3 = double(int64(t3 / Deltat)) * Deltat;
    warning(S)
end

[~, sub1_12, sub2_12] = intersect(t1, t2);
if isempty(sub1_12)
    return
end

[c, sub1_13, sub3_13] = intersect(t1(sub1_12), t3);
if isempty(sub1_13)
    return
end

sub1 = sub1_12(sub1_13);
sub2 = sub2_12(sub1_13);
sub3 = sub3_13;

tdeb = c(1);
tfin = c(end);

% TODO : transformer tdeb et tfin en datetime ? Pas fait ici car les
% variables ne sont pas appel�es dans la foncrion n-2 test�e