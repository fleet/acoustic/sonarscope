% T1 = datetime(2021, 1, 5, 15, 0, 0, 0:15)
% T2 = datetime(2021, 1, 5, 15, 0, 0, 1:17)
% [sub1, sub2] = intersect3_datetime(T1, T2, T2)

function [sub1, sub2, sub3, tdeb, tfin] = intersect3_datetime(T1, T2, T3, varargin)

[varargin, Format] = getPropertyValue(varargin, 'Format', 'yyyy-MM-dd HH:mm:ss.SSS'); %#ok<ASGLU>

T1.Format = Format;
sT1 = char(T1);
T1 = datetime(sT1);

T2.Format = Format;
sT2 = char(T2);
T2 = datetime(sT2);

T3.Format = Format;
sT3 = char(T3);
T3 = datetime(sT3);

[sub1, sub2, sub3, tdeb, tfin] = intersect3(T1, T2, T3, 'Approx', 0);
