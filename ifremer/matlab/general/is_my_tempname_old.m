% Test si un nom de fichier obtenu par la fonction "my_memmapfile" est vieux de plus 2 jours
%
% Syntax
%   flag = is_my_tempname_old(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : true | false
%
% Examples
%   Nom = my_tempname('.bzh')
%   flag = is_my_tempname_old(Nom)
%
% See also my_tempname Authors
% Authors : JMA
%-------------------------------------------------------------------

 function flag = is_my_tempname_old(Nom)

%% Décomposition du nom en mots élémentaires
 
[~, Nom] = fileparts(Nom);
mots  = strsplit(Nom, '_');
if length(mots) < 4
    flag = false;
    return
end

%% Date du fichier

TimeFichier = datenum([mots{2} '_' mots{3}], 'yyyymmdd_HHMMSS');

%% On teste si le fichier est vieux de 2 jours

% flag = ((TimeFichier + 2) <= floor(now)); % Instruction OK mais plus
% facile à contrôler avec les lignes suivantes

NOW = datetime("now");
D = datetime(TimeFichier, 'convertfrom', 'datenum');
flag = ((D + 2) <= NOW);
