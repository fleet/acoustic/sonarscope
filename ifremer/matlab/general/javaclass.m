% Return java.lang.Class instance for MatLab type.
%
% Input arguments:
% mtype:
%    the MatLab name of the type for which to return the java.lang.Class
%    instance
% ndims:
%    the number of dimensions of the MatLab data type
%
% See also: class

% Copyright 2009-2010 Levente Hunyadi
% Modification : GLU -QO , 2017/02/23 : comptabilité R2016b
function jclass = javaclass(mtype, ndims)

validateattributes(mtype, {'char'}, {'nonempty','row'});
if nargin < 2
    ndims = 0;
else
    validateattributes(ndims, {'numeric'}, {'nonnegative','integer','scalar'});
end

if ndims == 1 && strcmp(mtype, 'char')  % a character vector converts into a string
    jclassname = 'java.lang.String';
else
    % The static property .class applied to a Java type returns a string in
    % MatLab rather than an instance of java.lang.Class. For this reason,
    % use a string and java.lang.Class.forName to instantiate a
    % java.lang.Class object; the syntax java.lang.Boolean.class will not
    % do so.
    switch mtype
        case 'logical'  % logical vaule (true or false)
            jclassname = 'java.lang.Boolean';
        case 'char'  % a singe character
            jclassname = 'java.lang.Character';
        case {'int8','uint8'}  % 8-bit signed and unsigned integer
            jclassname = 'java.lang.Byte';
        case {'int16','uint16'}  % 16-bit signed and unsigned integer
            jclassname = 'java.lang.Short';
        case {'int32','uint32'}  % 32-bit signed and unsigned integer
            jclassname = 'java.lang.Integer';
        case {'int64','uint64'}  % 64-bit signed and unsigned integer
            jclassname = 'java.lang.Long';
        case 'single'  % single-precision floating-point number
            jclassname = 'java.lang.Float';
        case 'double'  % double-precision floating-point number
            jclassname = 'java.lang.Double';
        case 'cellstr'  % a single cell or a character array
            jclassname = 'java.lang.String';
        otherwise
            error('java:javaclass:InvalidArgumentValue', ...
                'MatLab type "%s" is not recognized or supported in Java.', mtype);
    end
        
end
% Note: When querying a java.lang.Class object by name with the method
% jclass = java.lang.Class.forName(jclassname);
% MatLab generates an error. For the Class.forName method to work, MatLab
% requires class loader to be specified explicitly.
% Deprecated dans R2016b :
% Dep jclass = java.lang.Class.forName(jclassname, true, java.lang.Thread.currentThread().getContextClassLoader());
if ndims > 0
    jObj    = javaArray(jclassname, ndims);
    jclass  = jObj.getClass;
else
    jObj    = javaObject(jclassname,1);
    jclass  = jObj.getClass;
end

