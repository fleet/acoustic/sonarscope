% Recherche la longeur des elements d'une cellule
%
% Syntax 
%   r = lengthcell(v)
%
% Input Arguments 
%   v : Tableau de cellules.
%
% Output Arguments 
%   r : "length" de chaque element du tableau de cellules
%
% Examples
%    v = {'TModeleAngle', [0:10], 'TModeleCoef', [10 11 34], 'TModele', 'Anten'}
%    r = lengthcell(v)
%
% See also Authors
% Authors : JMA
% VERSION  : $Id: lengthcell.m,v 1.3 2002/06/06 11:50:16 augustin Exp $
%-------------------------------------------------------------------------------

function r = lengthcell(v)

for i=1:length(v)
    r(i) = length(v{i}); %#ok
end
