% Intersection de 2 � n series de nombres strictement croissants ou d�croissants, 
% les indices sont rendus par rapport au sens de v1
%
% Syntax
%   [vdeb, vfin, sub1, sub2, ...] = lin_intersect(v1, v2, ...)
% 
% Input Arguments 
%   v1  : Premi�re serie
%   v2  : Deuxi�me serie
% 
% Name-Value Pair Arguments 
%   t3  : Troisi�me serie, ...
%
% Output Arguments 
%   vdeb : Valeur du debut de l'intersection
%   vfin : Valeur de la fin de l'intersection
%   sub1 : Index des temps communs dans la serie v1.
%   sub2 : Index des temps communs dans la serie v2.
%   ...
%
% Examples
%   v1 = 3:10
%   v2 = 2:8
%   t3 = 1:100;
%   [vdeb, vfin, sub1, sub2, sub3] = lin_intersect(v1, v2, t3)
%     % vdeb = 3
%     % vfin = 8
%     % sub1 = 1     2     3     4     5     6
%     % sub2 = 2     3     4     5     6     7
%     % sub3 = 3     4     5     6     7     8
%  v1(sub1)
%     % 3     4     5     6     7     8
%  v2(sub2)
%     % 3     4     5     6     7     8
%  t3(sub3)
%     % 3     4     5     6     7     8
%
%   T1 = fliplr(v1)
%   % 10     9     8     7     6     5     4     3
%   [vdeb, vfin, sub1, sub2, sub3] = lin_intersect(T1, v2, t3)
%     % vdeb = 3
%     % vfin = 8
%     % sub1 = 3     4     5     6     7     8
%     % sub2 = 7     6     5     4     3     2
%     % sub3 = 8     7     6     5     4     3
%  T1(sub1)
%     % 8     7     6     5     4     3
%  v2(sub2)
%     % 8     7     6     5     4     3
%  t3(sub3)
%     % 8     7     6     5     4     3
%
%   T2 = fliplr(v2)
%   [vdeb, vfin, sub1, sub2, sub3] = lin_intersect(v1, T2, t3)
%     % vdeb = 3
%     % vfin = 8
%     % sub1 = 1     2     3     4     5     6
%     % sub2 = 6     5     4     3     2     1
%     % sub3 = 3     4     5     6     7     8
%  v1(sub1)
%     % 3     4     5     6     7     8
%  T2(sub2)
%     % 3     4     5     6     7     8
%  t3(sub3)
%     % 3     4     5     6     7     8
%
%  [vdeb, vfin, sub1] = lin_intersect(1:10)
%
% See also cl_time/max cl_time/sort cl_time/v2date Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = lin_intersect(v1, varargin)

if nargin == 1
    varargout{1} = min(v1);
    varargout{2} = max(v1);
    varargout{3} = v1;
    return
end

v1 = v1(:)';
flagFliplr = (v1(end) < v1(1));

sub1 = 1:length(v1);
for k=1:(nargin-1)
    p2 = varargin{k}(:);
    [~, ~, pppp] = lin_intersect_unitaire(v1, p2');
    v1 = v1(pppp);
    sub1 = sub1(pppp);
end

for k=1:(nargin-1)
    [vdeb, vfin, ~, sub2] = lin_intersect_unitaire(v1, varargin{k});
    varargout{1} = vdeb;
    varargout{2} = vfin;
    varargout{3} = sub1;
    varargout{3+k} = sub2; %#ok<AGROW>
end

if flagFliplr
    for k=1:nargin
        varargout{2+k} = fliplr(varargout{2+k});
    end
end


function [vdeb, vfin, sub1, sub2] = lin_intersect_unitaire(v1, v2)

v1 = v1(:)';
v2 = v2(:)';

subNonNaNv1 = find(~isnan(v1));
subNonNaNv2 = find(~isnan(v2));

% if median(diff(v1)) < 0
    v1 = v1(subNonNaNv1);
% else
%     v1 = v1(subNonNaNv1);
% end
% if median(diff(v2)) < 0
    v2 = v2(subNonNaNv2);
% else
%     v2 = v2(subNonNaNv2);
% end

sub1 = [];
sub2 = [];
vdeb = [];
vfin = [];

if ~isequal(v1, floor(v1)) || ~isequal(v2, floor(v2))
    Deltav1 = mean(diff(v1), 'omitnan') * 1e-1;
    Deltav2 = mean(diff(v2), 'omitnan') * 1e-1;
    
    Deltat = min(abs([Deltav1 Deltav2]));

    S = warning;
    warning('off')
    v1 = double(int64(v1 / Deltat)) * Deltat;
    v2 = double(int64(v2 / Deltat)) * Deltat;
    warning(S)
end

[c, sub1_12, sub2_12] = intersect(v1, v2);
if isempty(sub1_12)
    return
end

sub1 = subNonNaNv1(sub1_12);
sub2 = subNonNaNv2(sub2_12);

vdeb = c(1);
vfin = c(end);
