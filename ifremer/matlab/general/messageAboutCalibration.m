function messageAboutCalibration(varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);
[varargin, Tag] = getPropertyValue(varargin, 'Tag',  []); %#ok<ASGLU>

if Mute
    return
end

if isempty(Tag)
    str1 = 'Pas de cr�ation d''image car elle est d�j� conforme � ce que vous demandez.';
    str2 = 'No image created because the current one is already what you want.';
else
    str1 = sprintf('%s : Pas de cr�ation d''image car elle est d�j� conforme � ce que vous demandez.', Tag);
    str2 = sprintf('%s : No image created because the current one is what you want.', Tag);
end
my_warndlg(Lang(str1,str2), 1);




























