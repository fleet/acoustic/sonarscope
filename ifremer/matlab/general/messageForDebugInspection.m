function messageForDebugInspection

if ~my_isdeployed
    fprintf('Inspection nedded here : \n')
    ST = dbstack;
    for k=length(ST):-1:2
        fprintf('\tline %04d in %s\n', ST(k).line, ST(k).name);
    end
end
