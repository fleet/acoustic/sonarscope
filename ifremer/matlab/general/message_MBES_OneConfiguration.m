function message_MBES_OneConfiguration

str = sprintf('WARNING : Be sure to select the .all files that have the same configuration as the current image (same mode, same swath mode).\nIf you do a summary, SonarScope proposes to create file lists of .all files that have the same configuration.');
my_warndlg(str, 1);
