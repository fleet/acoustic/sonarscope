% 1/(X - x0) * Integrale [Y(x) dx] de x0 a X
%
% Syntax
%   Ymoy = moyx0(X, Y)
%   Ymoy = moyx0(X, Y, x0)
%  
% Input Arguments
%   X : Abscisse des points
%   Y : Valeur des points
%
% Name-Value Pair Arguments
%   x0 : x0 Origine de l'integration (X(1) par defaut)
%
% Output Arguments
%   []    : Auto-plot activation
%   Alpha : Valeurs de X 
%   Ymoy  : Valeur moyenne integree
%
% Examples
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   Alpha = AttenuationGarrison(95, Z, T, S );
%
%   Beta = moyx0(Z, Alpha);
%       plot(Alpha, Z); grid on; zoom on;
%       hold on; plot(Beta, Z, 'r');
%
% See also cumtrapz AttenuationGarrison Authors
% Authors : YHDR + JMA
%---------------------------------------------------------------------------------------

function ymoy = moyx0(z, alpha, varargin)

if nargin == 2
    z0 = z(1);
else
    z0 = varargin{1};
end

if length(z) == 2
    bidouille = 1;
    z     = [z(1)     mean(z)     z(2)];
    alpha = [alpha(1) mean(alpha) alpha(2)];
else
    bidouille = 0;
end

beta = cumtrapz(z, alpha);

sqrteps = sqrt(eps);
z1 = z0 - sqrteps * min(abs(diff(z)));
% whos z beta z1
beta1 = interp1(z, beta, z1, 'spline');
% TODO : voir si on peut faire 'linear' car on gagne un facteur 2
% beta1 = interp1(z, beta, z1, 'linear', 'extrap');

ymoy = (beta - beta1) ./ (z - z1);

if bidouille
    z     = [z(1); z(3)];
    alpha = [alpha(1); alpha(3)];
    ymoy  = [ymoy(1); ymoy(3)];
end

%% Sortie des parametre ou traces graphiques

if nargout == 0
	FigUtils.createSScFigure;
   	PlotUtils.createSScPlot(alpha, z); grid on; zoom on; hold on;
   	PlotUtils.createSScPlot(ymoy, z, 'r'); grid on;
	legend({'y'; 'ymoy'});
end
