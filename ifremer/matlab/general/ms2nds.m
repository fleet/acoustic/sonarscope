% Conversion de la vitesse en m/s en Noeuds
%
% Syntax
%   vitEnNds = ms2nds(vitEnms)
%
% Input Arguments
%   vitEnms  : vitesse en m/s
%
% Output Arguments
%   vitEnNds : vitesse en noeuds
%
% Examples
%   ms2nds(1)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function vitEnNds = ms2nds(vitEnms)
vitEnNds = vitEnms * 3600 / 1852;
