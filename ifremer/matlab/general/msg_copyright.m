% Cr�ation d'un message de copyright
%
% Syntax
%   msg_copyright(dataName, dataOwner)
%
% Input Arguments
%   dataName  : Identifiant de la donn�e
%   dataOwner : Nom du propri�taire
%
% Examples 
%   msg_copyright('Prismed', 'Geosciences Azur')
%
% See also Prismed import_Prismed Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function msg_copyright(dataName, dataOwner)

Tag = [dataName 'Copyright'];
h = findobj(allchild(0), 'flat', 'Tag', Tag);
if isempty(h)
    str = sprintf('%s is a courtesy of %s.', dataName, dataOwner);
    my_warndlg(str, 0, 'Tag', 'Courtesy', 'TimeDelay', 30);
end
