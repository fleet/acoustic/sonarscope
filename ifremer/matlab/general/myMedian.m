% Un median qui fonctionne correctement
% median([1 10])
% myMedian([1 10])
% myMedian([1 10 NaN 11])

function y = myMedian(x, dim) %#ok<INUSD>

[m,n] = size(x); %#ok<ASGLU>
x = sort(x); % NaNs are forced to the bottom of each column

% Replace NaNs with zeros.
nans = isnan(x);
i = find(nans);
x(i) = zeros(size(i));
if min(size(x)) == 1
    n = length(x)-sum(nans);
    if n == 0
        y = NaN;
    else
        y = x(floor((n+1)/2));
    end
else
    n = size(x,1) - sum(nans);
    y = zeros(size(n));
    
    % Odd columns
    odd = find(rem(n,2)==1 & n>0);
    idx =(n(odd)+1)/2 + (odd-1)*m;
    y(odd) = x(idx);
    
    % Even columns
    even = find(rem(n,2)==0 & n>0);
    idx1 = n(even)/2 + (even-1)*m;
    y(even) = x(idx1);
    
    % All NaN columns
    i = find(n==0);
    y(i) = i + nan;
end
