function my_breakpoint(varargin)

if ~my_isdeployed
    [varargin, Message] = getPropertyValue(varargin, 'Message', '');
    [varargin, FctName] = getPropertyValue(varargin, 'FctName', ''); %#ok<ASGLU>
    
    fprintf('\n-----------------------------------------------------------\nbreakpoint here : %s\n', FctName);
    if ~isempty(Message)
        fprintf('%s\n', Message);
    end
    keyboard
end
