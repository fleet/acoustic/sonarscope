% fliplr d'une matrice m�me sur une image RGB
%
% Syntax
%   y = my_fliplr(x)
%
% Input Arguments
%   x : Matrice a fliplrr
%
% Output Arguments
%   y : Matrice a fliplre
%
% Remarks : Dans le cas d'une matrice de dimension 3 (Image en RGB par
%           exemple), my_fliplr r�alse la transpos�e de l'image
%
% Examples
%   x = 1:5
%   y = my_fliplr(x)
%
%   x = rand(2,3)
%   y = my_fliplr(x)
%
%   x = rand(2,3,2)
%   y = my_fliplr(x)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y = my_fliplr(x)

switch ndims(x)
    case 2
        y = fliplr(squeeze(x));
    case 3
        y = x;
        for i=1:size(x,3)
            y(:,:,i) = fliplr(x(:,:,i));
        end
    otherwise
        my_warndlg(Lang('my_fliplr : Operation pas encore ecrite', 'my_fliplr : not written yet'), 1);
end
