% flipud d'une matrice m�me sur une image RGB
%
% Syntax
%   y = my_flipud(x)
%
% Input Arguments
%   x : Matrice a flipudr
%
% Output Arguments
%   y : Matrice a flipude
%
% Remarks : Dans le cas d'une matrice de dimension 3 (Image en RGB par
%           exemple), my_flipud r�alse la transpos�e de l'image
%
% Examples
%   x = 1:5
%   y = my_flipud(x)
%
%   x = rand(2,3)
%   y = my_flipud(x)
%
%   x = rand(2,3,2)
%   y = my_flipud(x)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y = my_flipud(x)

switch ndims(x)
    case 2
        y = flipud(x);
    case 3
        y = x;
        for i=1:size(x,3)
            y(:,:,i) = flipud(x(:,:,i));
        end
    otherwise
        my_warndlg(Lang('my_flipud : Operation pas encore ecrite', 'my_flipud : not written yet'), 1);
end
