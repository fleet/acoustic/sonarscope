function y2 = my_interp1(x1, y1, x2, varargin)

if isempty(x2)
    y2 = [];
    return
end

if isinteger(x1)
    x1 = double(x1);
end

if isinteger(x2)
    x2 = double(x2);
end


size_x2 = size(x2);

if length(x1) == 1
    y2 = repmat(y1, size_x2);
    return
end

if isdatetime(x1)
    x1 = datenum(x1);
end

if isdatetime(x2)
    x2 = datenum(x2);
end

x1 = x1(:);
y1 = y1(:);
x2 = x2(:);

y1 = singleUnlessDouble(y1, NaN);
sub = isnan(x1) | isnan(y1);
x1(sub) = [];
% if isempty(x1)
if length(x1) <= 1
    y2 = NaN(size_x2); % TODO 'like', x2
    return
end

y1(sub) = [];

D = diff(x1);
D(end+1) = D(end);
sub = (D <= 0);
if sum(sub(:)) == 0
	sub = find(~isnan(x1(:)) & ~isnan(y1(:)));
    if isempty(sub)
        y2 = NaN(size_x2);
    else
        sub1 = ~isnan(y1(sub));
        sub2 = ~isnan(x1(sub));
        suby = sub1(:) & sub2(:);
        sub  = sub(suby);
 
        if length(sub) <= 1
            y2 = NaN(size_x2);
        else
            try
                y2 = interp1(x1, y1, x2, varargin{:});
            catch
                [~, subOrdre] = unique(x1(sub));
                x1 = x1(sub(subOrdre));
                y1 = y1(sub(subOrdre));
                y2 = interp1(x1, y1, x2, varargin{:});
            end
        end
    end
    
    y2((isinf(y2))) = NaN; % Ajout JMA le 10/02/2022 pour interpolation nav apr�s nettoyage par NavigationDialog
    % ATTENTION : cette modif peut avoir des effets de bords insoup�onn�s
    
else
    [~, sub] = unique(x1);
    x1 = x1(sub);
    y1 = y1(sub);
    sub = ~isnan(x1(:)) & ~isnan(y1(:));
    if sum(sub(:)) <= 1
        if any(strcmp(varargin, 'previous'))
            y2 = zeros(size_x2) + y1;
        else
            y2 = NaN(size_x2);
        end
    else
        [~, subOrdre] = unique(x1(sub));
        x1 = x1(sub(subOrdre));
        y1 = y1(sub(subOrdre));
        y2 = interp1(x1, y1, x2, varargin{:});
    end
end
y2 = reshape(y2, size_x2);

%% ATTENTION : ce code est n�cessaire pour assurer la non r�gression avec ce qui �tait fait avant.
% il est utile pour l'interpolation du mode de fonctionnement des sondeurs pour affecter la valeur des premiers pings

if any(strcmpi(varargin, 'previous'))
    firstNotNaN = find(~isnan(y2(:)), 1, 'first');
    if ~isempty(firstNotNaN)
        y2(1:firstNotNaN-1) = y2(firstNotNaN);
    end
end
