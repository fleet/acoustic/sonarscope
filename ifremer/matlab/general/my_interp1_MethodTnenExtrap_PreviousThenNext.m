function y2 = my_interp1_MethodTnenExtrap_PreviousThenNext(x1, y1, x2, varargin)

y2 = my_interp1(x1, y1, x2, varargin{:});
sub = find(isnan(y2));
y2(sub) = my_interp1_Extrap_PreviousThenNext(x1, y1, x2(sub));