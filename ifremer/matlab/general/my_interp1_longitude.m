% Linear interpolation of a longitude signal. The function takes into account the phase rotation
%
% Syntax
%   y2  = my_interp1_headingDeg(x1, y1, x2, ...)
%
% Input Arguments
%   x1 : Abscissa
%   y1 : Longitude (deg)
%   x2 : Abscissa where the values have to be interpolated
%
% Name-Value Pair Arguments
%   Same as "interp1" : 'linear', 'extrap', ...
%
% Output Arguments
%   y2 : Interpolated values(deg).
%
% Examples
%   x1 = 1:3600;
%   y1 = mod(x1,360) - 180;
%   x2 = 0:0.5:3600
%   y2  = my_interp1_longitude(x1, y1, x2, 'linear', 'extrap');
%   figure; plot(x1,y1(:), '-ok'); grid on; hold on; plot(x2,y2, '-*r')
%   y2  = my_interp1_longitude(x1, -y1, x2, 'linear', 'extrap');
%   figure; plot(x1,-y1(:), '-ok'); grid on; hold on; plot(x2,y2, '-*r')
%
% See also interp1 my_interp1 my_interp1_headingDeg Authors
% Authors    : JMA
%-------------------------------------------------------------------------------

function y2  = my_interp1_longitude(x1, y1, x2, varargin)

y2 = y1(:);
% y2 = y2 + (rand(size(y2)) - 0.5) * 20
% figure; plot(y2, '-*'); grid
y2 = unwrap(y2 * (pi/180)) * (180/pi);
% figure; plot(y2, '-*'); grid
y2   = my_interp1(x1, y2, x2, varargin{:});
% figure; plot(y2, '-*'); grid

sub = find(y2 >= 180);
while ~isempty(sub)
    y2(sub) = y2(sub) - 360;
    sub = find(y2 >= 180);
end

sub = find(y2 < -180);
while ~isempty(sub)
    y2(sub) = y2(sub) + 360;
    sub = find(y2 < -180);
end

% figure; plot(x1,y1(:), '-+k'); grid on; hold on; plot(x2,y2, '-xr')
