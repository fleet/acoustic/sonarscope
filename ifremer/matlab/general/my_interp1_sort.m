function y2 = my_interp1_sort(x1, y1, x2, varargin)

if isempty(x2) == 1
    y2 = [];
    return
end

if length(x1) == 1
    y2 = repmat(y1, size(x2));
    return
end

D = diff(x1);
D(end+1) = D(end);
sub = (D <= 0);
if sum(sub(:)) == 0
    sub = ~isnan(x1(:)) & ~isnan(y1(:));
    if sum(sub(:)) == 0
        y2 = NaN(size(x2));
    else
        sub1 = ~isnan(y1(sub));
        sub2 = ~isnan(x1(sub));
        suby = sub1(:) & sub2(:);
        sub = find(sub(suby));
        
        
        
        % Suppression des abscisses identiques. Rajouté le 04/09/2013 par
        % JMA pour traitement WC en SimpleBeam fichiers EM2040 de Gascogne1
        % 0408_20130802_134702_Lesuroit_EM2040 ping 1694
        
        x1 = x1(sub);
        y1 = y1(sub);
        [~, subOrder] = sort(x1);
        x1 = x1(subOrder);
        y1 = y1(subOrder);
        
        D = diff(x1);
        if mean(D) > 0
            subConst = 1 + find(D <= 0);
        else
            subConst = 1 + find(D >= 0);
        end
        x1(subConst) = [];
        y1(subConst) = [];
        
        

        if length(sub) <= 1
            y2 = NaN(size(x2));
        else
        	y2 = interp1(x1, y1, x2, varargin{:});
        end
    end
else
    [~, sub] = unique(x1);
    x1 = x1(sub);
    y1 = y1(sub);
    sub = ~isnan(x1(:)) & ~isnan(y1(:));
    if sum(sub(:)) == 0
        y2 = NaN(size(x2));
    else
        y2 = interp1(x1(sub), y1(sub), x2, varargin{:});
    end
end
