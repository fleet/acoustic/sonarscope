function y2 = my_interpUsual(y1, varargin)

x1 = 1:length(y1);
subNaN = isnan(y1);
y2 = y1;
if any(subNaN)
    y2(subNaN) = my_interp1(x1(~subNaN), y1(~subNaN), x1(subNaN), varargin{:});
end
