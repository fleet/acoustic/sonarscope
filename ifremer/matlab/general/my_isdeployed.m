% Remarque : cette fonction semble inutile, c'est vrai mais elle permet de
% simuler un fonctionnement "isdeployed" dans le debugger en changeant la
% valeur de flag = 1, sinon il n'est jamais possible de passer dans les
% boucles "isdeployed" en mode "developpement".

function flag = my_isdeployed
flag = isdeployed;
