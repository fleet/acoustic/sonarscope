% flag = my_isequal(0, 1e-7)
% flag = my_isequal(0, 1e-5)

function flag = my_isequal(x, y, varargin)

[varargin, epsilon] = getPropertyValue(varargin, 'epsilon', 1e-6); %#ok<ASGLU>

z = abs(x-y);
if z <= epsilon
    flag = 1;
else
    flag = 0;
end

% if x == y
%     flag = 1;
% else
%     z = abs((x-y) / (x+y));
%     if (z >= (1-epsilon)) && (z <= (1+epsilon))
%         flag = 1;
%     else
%         flag = 0;
%     end
% end
