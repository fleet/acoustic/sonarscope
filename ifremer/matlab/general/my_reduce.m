function [y2, n2] = my_reduce(y1, deltax1, deltax2, XMin1, XMin2, XMax2, subx1)

coef = deltax1 / deltax2;
lengthy2 = 1 + ceil((XMax2 - XMin2) / deltax2);
Offset1 = (XMin1 / deltax1) - 1;
Offset2 = (XMin2 / deltax2) - 1;
classx = class(y1);
y2 = zeros(1, lengthy2, classx);
n2 = zeros(1, lengthy2, 'single');
for k=1:length(subx1)
    k1 = subx1(k);
    val = y1(k1);
    if ~isnan(val)
        k2 = 1 + floor((k1 + Offset1) * coef - Offset2);
        if k2 <= lengthy2
            y2(k2) = y2(k2) + val;
            n2(k2) = n2(k2) + 1;
        end
    end
end
subNaN = (n2 == 0);
y2(subNaN) = NaN;
n2(subNaN) = NaN;

y2 = y2 ./ n2;