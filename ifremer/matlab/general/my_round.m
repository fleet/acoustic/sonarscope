function y = my_round(x, varargin) 

if my_verLessThanR2014b
    y = round(x);
else
    y = round(x, varargin{:});
end
