function ndx = my_sub2ind(siz, varargin)

n = numel(varargin{1});
ndx = zeros(n, 1);
% Dans le cas de pb m�moire, activer la ligne suivante (solution GLU-QO le
% 16/03/2017). Attention : peut-�tre impactant sur des images tr�s grosses
% (avec index de pixels tr�s grands) support�es par des PC capables de les traiter.
% ndx = zeros(n, 1, 'uint32');
for k=1:10000:n
    subi = k:min(k-1+10000, n);
    switch length(varargin)
        case 1
            ndx(subi) = sub2ind(siz, double(varargin{1}(subi)));
        case 2
            ndx(subi) = sub2ind(siz, double(varargin{1}(subi)), double(varargin{2}(subi)));
        case 3
            ndx(subi) = sub2ind(siz, double(varargin{1}(subi)), double(varargin{2}(subi)), double(varargin{3}(subi)));
        case 4
            ndx(subi) = sub2ind(siz, double(varargin{1}(subi)), double(varargin{2}(subi)), double(varargin{3}(subi)), double(varargin{4}(subi)));
        otherwise
            ndx(subi) = sub2ind(siz, varargin{:});
    end
end
ndx = reshape(ndx, size(varargin{1}));
