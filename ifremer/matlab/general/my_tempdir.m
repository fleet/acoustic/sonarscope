% Nom du répertoire temporaire (/tmp sous unix ou C:\TEMP sous windows)
%
% Syntax
%   nomDir = my_tempdir
%
% Output Arguments
%   nomDir : Nom du répertoire
%
% Examples 
%   nomDir = my_tempdir
%
% See also tempdir my_tempname Authors
% Authors : JMA
%--------------------------------------------------------------------------

function nomDir = my_tempdir

global IfrTbxTempDir %#ok<GVMIS>

% nomDir = 'C:\TEMP';
nomDir = IfrTbxTempDir;
if exist(nomDir, 'dir')
    return
else
    nomDir = tempdir;
    %my_warndlg(['C:\TEMP n''existant pas sur votre disque, on prend le repertoire par defaut : ' nomDir], 0);
end
