% Nom du fichier temporaire (sous /tmp sous unix ou C:\TEMP sous windows)
%
% Syntax
%   nomFic = my_tempname(...)
%
% Name-Value Pair Arguments 
%   ext : Extension (aucune extension par defaut)
%
% Name-only Arguments
%   NoRandom : Pas de champ random
%   DirName  : Nom du répertoire
%
% Output Arguments
%   nomFic : Nom du repertoire
%
% Examples 
%   nomFic = my_tempname
%   nomFic = my_tempname('.xyz')
%   nomFic = my_tempname('.dat', 'NoRandom')
%   nomFic = my_tempname('.dat', 'Prefixe', 'Session', 'NoRandom')
%
% See also tempname my_tempdir Authors
% Authors : JMA
%--------------------------------------------------------------------------

function nomFic = my_tempname(varargin)

[varargin, Prefixe]  = getPropertyValue(varargin, 'Prefixe', 'tmp');
[varargin, DirName]  = getPropertyValue(varargin, 'DirName', my_tempdir);
[varargin, NoRandom] = getFlag(varargin, 'NoRandom');

[pppp, nomFic] = fileparts(tempname); %#ok
str = datestr(now, 'yyyymmdd_HHMMSS');
if NoRandom
    nomFic = fullfile(DirName, [Prefixe '_' str]);
    if ~isempty(varargin)
        nomFic = [nomFic varargin{1}];
    end
else
    while 1
        nomFic = fullfile(DirName, [Prefixe '_' str '_' num2str(floor(rand(1)*1000000))]);
        if ~isempty(varargin)
            nomFic = [nomFic varargin{1}]; %#ok<AGROW>
        end
        if ~exist(nomFic, 'file')
            break
        end
    end
end
