% Transpos�e d'une matrice m�me sur une image RGB
%
% Syntax
%   y = my_transpose(x)
%
% Input Arguments
%   x : Matrice a transposer
%
% Output Arguments
%   y : Matrice a transposee
%
% Remarks : Dans le cas d'une matrice de dimension 3 (Image en RGB par
%           exemple), my_transpose r�alse la transpos�e de l'image
%
% Examples
%   x = 1:5
%   y = my_transpose(x)
%
%   x = rand(2,3)
%   y = my_transpose(x)
%
%   x = rand(2,3,2)
%   y = my_transpose(x)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y = my_transpose(x)

sz = size(x);
classx = class(x);
if islogical(classx)
    %     classx
    %     y = zeros([sz(2) sz(1) sz(3:end)], 'uint8');
else
    if length(sz) > 2
        y = zeros([sz(2) sz(1) sz(3:end)], classx);
    else
        y = x';
    end
end

switch ndims(x)
    case 2
        % On ne traite pour l'instant que des images cons�quentes.
        y = x';
    case 3
        for i=1:size(x,3)
            y(:,:,i) = x(:,:,i)';
        end
    case 4 % On ne transpose que les 2 premi�res dimensions.
        for i=1:size(x,3)
            for j=1:size(x,4)
                y(:,:,i,j) = x(:,:,i,j)';
            end
        end
    case 5 % On ne transpose que les 2 premi�res dimensions.
        for i=1:size(x,3)
            for j=1:size(x,4)
                for k=1:size(x,5)
                    y(:,:,i,j,k) = x(:,:,i,j,k)';
                end
            end
        end
    case 6 % On ne transpose que les 2 premi�res dimensions.
        for i=1:size(x,3)
            for j=1:size(x,4)
                for k=1:size(x,5)
                    for l=1:size(x,6)
                        y(:,:,i,j,k,l) = x(:,:,i,j,k,l)';
                    end
                end
            end
        end
    case 7 % On ne transpose que les 2 premi�res dimensions.
        for i=1:size(x,3)
            for j=1:size(x,4)
                for k=1:size(x,5)
                    for l=1:size(x,6)
                        for m=1:size(x,7)
                            y(:,:,i,j,k,l,m) = x(:,:,i,j,k,l,m)';
                        end
                    end
                end
            end
        end
    case 8 % On ne transpose que les 2 premi�res dimensions.
        for i=1:size(x,3)
            for j=1:size(x,4)
                for k=1:size(x,5)
                    for l=1:size(x,6)
                        for m=1:size(x,7)
                            for n=1:size(x,8)
                                y(:,:,i,j,k,l,m,n) = x(:,:,i,j,k,l,m,n)';
                            end
                        end
                    end
                end
            end
        end
    otherwise
        my_warndlg(Lang('my_transpose : Operation pas encore �crite', 'my_transpose : not yet written'), 1);
end
