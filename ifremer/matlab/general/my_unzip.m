function my_unzip(nomFic)

if ~exist(nomFic, 'file')
    return
end

nomDir = fileparts(nomFic);
try
    str1 = sprintf('Décompression du fichier "%s", en cours', nomFic);
    str2 = sprintf('Unzipping "%s"', nomFic);
    WorkInProgress(Lang(str1,str2))
    unzip(nomFic, nomDir)
    delete(nomFic);
catch %#ok<CTCH>
end
