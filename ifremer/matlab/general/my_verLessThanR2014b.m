function flag = my_verLessThanR2014b

persistent rep_persistent

if isempty(rep_persistent)
    flag = verLessThan('MatLab', '8.4.0');
    rep_persistent = flag;
else
    flag = rep_persistent;
end

