% Description
%   Opens fileName in the associated Microsoft� Windows� application or a web site.
%   For a web site, my_web opens the page in your default web browser.
%   For a file, the application is associated with the extension in fileName in the Windows operating system. 
%   filename is a string enclosed in single quotes. winopen uses a Windows shell command, 
%   and performs the same action as double-clicking the file in the Windows Explorer program. 
%   That is, winopen calls the application associated the file extension to open the file. 
%   Use an absolute or relative path for fileName.
%
% Syntax
%   my_web(str)
%
% Input Arguments
%   str : A file name of a web name
%
% Examples
%   my_web('http://www.ifremer.fr')
%   my_web(getNomFicDatabase('NasaWorldWind_readme.txt'))
%
% Authors  : JMA
% See also : web winopen
%
% Reference pages in Help browser
%   <a href="matlab:doc my_web">my_web</a>
%-------------------------------------------------------------------------------

function my_web(URL)

URL = strrep(URL, 'file://', '');
if exist(URL, 'file')
    winopen(URL)
else
    try
        % Comment� car �a prend �norm�ment de temps sur le site Ifremer
        % par exemple. Le try ne doit donc plus �tre utile mais bon !
        %             webread(URL); % Juste pour voir si on peut lire cette url et tomber dans le catch sinon
        
        web(URL, '-browser');
    catch ME
        str1 = sprintf('L''url "%s" ne semble pas exister ou n''est pas accessible.\nMessage Matlab : %s', URL, ME.message);
        str2 = sprintf('url "%s" does not exist or is not reachable.\nMatlab message : %s', URL, ME.message);
        my_warndlg(Lang(str1,str2), 1);
    end
end
