function my_winopen(NomProgramme, InitialFileName, varargin)

global IfrTbxResources %#ok<GVMIS>

[varargin, NomDirExe] = getPropertyValue(varargin, 'NomDirExe', []); %#ok<ASGLU>

try
    winopen(InitialFileName)
catch %#ok<CTCH>
    if isempty(NomDirExe)
        nomExe = fullfile(IfrTbxResources, NomProgramme);
    else
        nomExe = fullfile(NomDirExe,NomProgramme);
    end
    if isdeployed
        nomWinOpen = fullfile(IfrTbxResources, 'winopen.exe');
        nomExe = [nomWinOpen ' "' nomExe '"'];
    else
        nomExe = ['C:\SScTbx' '\Installation\Viewers\winopen.exe "' nomExe '"'];
    end
    cmd = ['!' nomExe ' ' InitialFileName];
    str1 = 'Lancement de la commande :';
    str2 = 'Lauch DOS command :';
    fprintf('%s\n%s\n', Lang(str1,str2), cmd);
    eval(cmd)
end
