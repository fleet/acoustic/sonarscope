% y = nanmode([1 2 3 NaN 3 5])

function y = nanmode(x)

y = mode(x(~isnan(x)));
