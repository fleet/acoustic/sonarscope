% nearestExpectedContourStep(3)
function y = nearestExpectedContourStep(x)

x = double(x);
x = log10(x);
e = floor(x);
x2 = x - e;
x1 = [log10([1 2 2.5 5 10])];
y2 = interp1(x1, x1, x2, 'nearest');
y = 10 .^ (y2+e);


