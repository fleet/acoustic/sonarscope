% nearestExpectedGridSize(3)
function y = nearestExpectedGridSize(x)

x = double(x);
x = log10(x);
e = floor(x);
x2 = x - e;
x1 = [log10([1 1.5 2 2.5 3 4 5 6 7 8 10])];
y2 = interp1(x1, x1, x2, 'nearest');
y = 10 .^ (y2+e);


