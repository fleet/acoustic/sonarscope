% X = rand(1,5)
% x = X(3);
% nearlyEqual(X, x, 0.001)
% x = X(3) + eps*1e10;
% nearlyEqual(X, x, 0.001)

function y = nearlyEqual(X, x, PourCent)

if x == 0
    y = abs(X) < (PourCent/100);
else
    W = warning;
    warning('off')
    y = abs((X-x) ./ (X + x));
    y = isnan(y) | (y < (PourCent/100));
    warning(W)
end
