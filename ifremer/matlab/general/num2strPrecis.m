% Conversion d'une latitude en string
%
% Syntax
%   str = num2strPrecis(x)
%
% Input Arguments
%   x : Valeur num�rique
%
% Output Arguments
%   str : repr�sentation ascii la plus pr�cise possible de x
%
% Examples 
%   str1 = num2str(pi)
%   str2 = num2strPrecis(pi)
%   num2strPrecis([0 1 3.14 pi pi*1e-8 pi*1e8 Inf NaN])
%
% See also str2lat lon2str str2lon create_fprintf_Format Authors
% Authors : JMA
%--------------------------------------------------------------------------

function str = num2strPrecis(var)

% Remarque : attention, cette fonction a �norm�ment de side effects sur les
% GLOBE, Surfer, ... Toute modification doit �tre test�e � fond

if isa(var, 'datetime')
    var = datenum(var);
end

if isequal(floor(var), var)
    str = num2str(var);
else
    switch class(var)
        case 'double'
%             PrecisionMax = 16;
            str = sprintf('%16.16g ', var);
        otherwise
%             PrecisionMax = 9;
            str = sprintf('%9.9g ', var);
    end
%     str = num2str(var, PrecisionMax);
end
% Remplacement de plusieurs blancs par un seul.
str = rmblank(str);
