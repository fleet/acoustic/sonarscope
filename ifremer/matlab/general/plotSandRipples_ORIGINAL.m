% function [this.plotFigure, this.hPlot] = plotSandRipples(this.plotFigure, this.hPlot, [this.paramDialog.params(:).Value])
function [hfig, hPlot] = plotSandRipples_ORIGINAL(hfig, hPlot, valParams)

%% Create the figure of synchronized signals

nbCu1 = 4;
nbCu2 = 4;
if (length(hfig) ~= 2) || ~ishandle(hfig(1)) || ~ishandle(hfig(2))
    hfig(1) = figure;
    for k=1:nbCu1
        hPlot(k) = subplot(nbCu1,1,k);
        grid(hPlot(k), 'On');
    end
    linkaxes(hPlot(1:nbCu1), 'x')
    
    hfig(2) = figure;
    for k=1:nbCu2
        hPlot(nbCu1+k) = subplot(nbCu2,1,k);
        grid(hPlot(nbCu1+k), 'On');
    end
    linkaxes(hPlot(nbCu1+(1:nbCu2)), 'x')
end

%% Get the parametres

depth           = valParams(1);
h               = valParams(2); % height of trough to peak of ripple in m
T               = valParams(3); % period of sand ripple in m
as              = valParams(4);
coefNoiseHeight = valParams(5);
TetaMax         = valParams(6);

%% Define pulse length

c  = 1500; % m/s
Tp = 70;   % µs pulse length

%% Set the coefficients that randomize the periods, the asymetry and height of ripples (0 : no perturbation, 1 lot of perturbation)

coefNoisePeriodicity = 0.25;
coefNoiseRipple      = 0.1;
coefNoiseAs          = 0.5;

%% Create the ripples

[acrossDist, y] = sandRipples(depth, TetaMax, T, h, as, coefNoiseHeight, coefNoisePeriodicity, ...
    coefNoiseRipple, coefNoiseAs);
% figure; plot(acrossDist, y); grid on;
Angles = atand(acrossDist ./ depth);

y=y+h;
%%  Compute the slopes and the real incidence angle

slopes = atan2(gradient(y), gradient(acrossDist));
real_inc = NaN(size(acrossDist));
for k=1:length(acrossDist)
    Mx = acrossDist(k);
    My = y(k);
    nr = -slopes(k);
    DirVector1 = [0,depth]-[Mx,My] ;
    DirVector2 = [nr 1];
    %         Angle = acos(dot(DirVector1,DirVector2) / (norm(DirVector1) * norm(DirVector2)));
    %         real_inc(k) = atand(Angle);
    
    CosTheta = dot(DirVector1,DirVector2)/(norm(DirVector1)*norm(DirVector2));
    Angle = acosd(CosTheta);
    if Mx<0
        real_inc(k) = -Angle;
    else
        real_inc(k) = Angle;
    end
end
slopes = slopes * (180/pi);


%% Define the Lurton model

BsParameters = [-22.0712 20 -26.7109 2.2181 0 0];
bs = BSLurton(real_inc, BsParameters);

% {
%% Shadowing

tol = abs(diff(acrossDist(1:2)));
parfor k=1:length(acrossDist)
    xADi = acrossDist(k);
    x1 = [0, xADi];
    y1 = [depth, y(k)];
    
    wind_size = round(3*(T/tol));
    
    %                             [xa, ~] = polyxpoly(x1, y1, acrossDist, y);
    %                             if length(xa) > 1
    %                                 pi;
    %                             end
    
    if xADi>=0
        [xa, ~] = polyxpoly(x1, y1, acrossDist(k-wind_size:k), y(k-wind_size:k));
        if isempty(xa)
        else
            comp1 = min(xa);
            if (comp1 >= (xADi-tol)) && (comp1 <= (xADi+tol))
                bs(k)=bs(k);
            else
                bs(k) = NaN;
            end
        end
    else
        [xa, ~] = polyxpoly(x1, y1, acrossDist(k:k+wind_size), y(k:k+wind_size));
        if isempty(xa)
        else
            comp2 = max(xa);
            if (comp2 >= (xADi-tol)) && (comp2 <= (xADi+tol))
                bs(k) = bs(k);
            else
                bs(k) = NaN;
            end
        end
    end
end
% }


th  = atand(acrossDist/depth);

%% Calculating Average Based on Signal Footprint

Signal_foot = c * (Tp*10^-6) ./ (2*sind(abs(th)));

bs_int = NaN(1, length(Signal_foot));

for k=1:length(Signal_foot)
    
    idx = find(acrossDist>acrossDist(k)-(Signal_foot(k))/2 & acrossDist<acrossDist(k)+(Signal_foot(k))/2);
    
    ValEnr = reflec_dB2Amp(bs(idx));
    ValEnr(isnan(ValEnr)) = 0;
    bs_int(k) = reflec_Amp2dB(mean(ValEnr, 'omitnan'));
    
    %     bs_int(k) =nanmean(bs(idx));
end


AngAvg = -60:1:60;
n = length(AngAvg);
BSavg = NaN(1, n);

for k=1:(n-1)
    idx = find((th >= AngAvg(k)) & (th <= (AngAvg(k+1))));
    if ~isempty(idx)
        BSavg(k) = reflec_Amp2dB(mean(reflec_dB2Amp(bs_int(idx)), 'omitnan'));
        %         BSavg(k) =nanmean(bs_int(idx));
    end
end



%% Calculating Average Based on TIME (xavier)
format long

W=1;
H=depth;
Beam_foot = H * (W*(pi/180)) ./ (cosd(th).^2); % calculate beam footprint


for k=1:length(acrossDist)   % find Range
    Sonar = [0, depth];
    point = [acrossDist(k), y(k)];
    s = [Sonar; point];
    Range(k) = pdist(s,'euclidean'); %#ok<AGROW>
end

Samples_time=(2/c)*Range; % convert range to time


port=find(acrossDist<0);
starboard=find(acrossDist>=0);

bs_int2_1=NaN(1, length(port));

for aa=1:length(port)
    
    idxx = find(Samples_time(port(1):length(port))>Samples_time(aa)-(Tp*10^-6) & Samples_time(port(1):length(port))<Samples_time(aa)+(Tp*10^-6));
    idx = find(acrossDist>acrossDist(aa)-(Beam_foot(aa))/2 & acrossDist<acrossDist(aa)+(Beam_foot(aa))/2);
    common=intersect(idxx,idx);
    ValEnr2 = reflec_dB2Amp(bs(common));
    ValEnr2(isnan(ValEnr2)) = 0;
    bs_int2_1(aa) = reflec_Amp2dB(mean(ValEnr2, 'omitnan'));
    %         bs_int2_1(aa) =nanmean(bs(common));
    
end

bs_int2_2=NaN(1, length(starboard));

for ab=1:length(starboard)
    
    idxx = find(Samples_time(min(starboard):max(starboard))>Samples_time(starboard(ab))-(Tp/2*10^-6) & Samples_time(min(starboard):max(starboard))<Samples_time(starboard(ab))+(Tp/2*10^-6));
    idx = find(acrossDist>acrossDist(starboard(ab))-(Beam_foot(starboard(ab)))/2 & acrossDist<acrossDist(starboard(ab))+(Beam_foot(starboard(ab)))/2);
    common=intersect(idxx+starboard(1),idx);
    ValEnr2 = reflec_dB2Amp(bs(common));
    ValEnr2(isnan(ValEnr2)) = 0;
    bs_int2_2(ab) = reflec_Amp2dB(mean(ValEnr2, 'omitnan'));
    %             bs_int2_2(ab) =nanmean(bs(common));
    
end


bs_int2=[bs_int2_1 bs_int2_2];

AngAvg2 = -60:0.5:60;
n = length(AngAvg2);
BSavg2 = NaN(1, n);

for k=1:(n-1)
    idx = find((th >= AngAvg2(k)) & (th <= (AngAvg2(k+1))));
    if ~isempty(idx)
        BSavg2(k) = reflec_Amp2dB(mean(reflec_dB2Amp(bs_int2(idx)), 'omitnan'));
        %         BSavg2(k) =nanmean(bs_int2(idx));
    end
end


%% Calculating simple Average



AngAvg = -60:1:60;
n = length(AngAvg);
BSavg3 = NaN(1, n);

for k=1:(n-1)
    idx = find((th >= AngAvg(k)) & (th <= (AngAvg(k+1))));
    if ~isempty(idx)
        BSavg3(k) = reflec_Amp2dB(mean(reflec_dB2Amp(bs(idx)), 'omitnan'));
    end
end





%% Make plots vs across distance

subNoShadow = find(~isnan(bs));
% plot(hPlot(1), acrossDist, y, 'k'); title(hPlot(1), 'Sand ripples (m) vs across distance'); %axis equal;
% hold(hPlot(1), 'on');
% plot(hPlot(1), acrossDist(subNoShadow), y(subNoShadow), '.b')
% hold(hPlot(1), 'off');
%
% plot(hPlot(2), acrossDist, slopes, 'k'); title(hPlot(2), 'Slopes (deg) vs across distance');
% hold(hPlot(2), 'on');
% plot(hPlot(2), acrossDist(subNoShadow), slopes(subNoShadow), '.b')
% hold(hPlot(2), 'off');
%
% plot(hPlot(3), acrossDist, real_inc, 'k'); title(hPlot(3), 'Incidence angles (deg) vs across distance');
% hold(hPlot(3), 'on');
% plot(hPlot(3), acrossDist(subNoShadow), real_inc(subNoShadow), '.b')
% hold(hPlot(3), 'off');
%
% plot(hPlot(4), acrossDist, bs_int, 'k'); title(hPlot(4), 'BSLurton vs across distance');
% hold(hPlot(4), 'on');
% plot(hPlot(4), acrossDist(subNoShadow), bs_int(subNoShadow), '.b')
% hold(hPlot(4), 'off');
%
%% Make plots vs angles

plot(hPlot(5), Angles, y, 'k'); title(hPlot(5), 'Sand ripples (m) vs Angles'); %axis equal;
hold(hPlot(5), 'on');
plot(hPlot(5), Angles(subNoShadow), y(subNoShadow), '.b')
hold(hPlot(5), 'off');

plot(hPlot(6), Angles, slopes, 'k'); title(hPlot(6), 'Slopes (deg) vs Angles');
hold(hPlot(6), 'on');
plot(hPlot(6), Angles(subNoShadow), slopes(subNoShadow), '.b')
hold(hPlot(6), 'off');

plot(hPlot(7), Angles, real_inc, 'k'); title(hPlot(7), 'Incidence angles (deg) vs Angles');
hold(hPlot(7), 'on');
plot(hPlot(7), Angles(subNoShadow), real_inc(subNoShadow), '.b')
hold(hPlot(7), 'off');

plot(hPlot(8), Angles, bs, 'b'); title(hPlot(8), 'BS vs Angles');
hold(hPlot(8), 'on');
% plot(hPlot(8), Angles(subNoShadow), bs_int(subNoShadow), '.b')
% hold(hPlot(8), 'off');

% plot(hPlot(9), Angles, bs_int2, 'k'); title(hPlot(9), 'BSLurton vs Angles');
% hold(hPlot(9), 'on');
% plot(hPlot(9), Angles(subNoShadow), bs(subNoShadow), '.b')
% plot(hPlot(9), AngAvg, BSavg, 'r');
% hold(hPlot(9), 'off');

% plot(hPlot(10), AngAvg, BSavg2); title(hPlot(10), 'BSLurton vs Angles');
% hold(hPlot(10), 'on');
% plot(hPlot(10), AngAvg, BSLurton(AngAvg, BsParameters), 'k');
% hold(hPlot(10), 'off');

%% Grid on

for k=1:(nbCu1+nbCu2)
    grid(hPlot(k), 'on');
end

%% Other figures

figure(65); PlotUtils.createSScPlot(AngAvg, BSavg);hold on; PlotUtils.createSScPlot(AngAvg, BSLurton(AngAvg, BsParameters), 'k');  grid on ; title('BS average (average on signal footprint and then bin 1 deg)');
figure(75); PlotUtils.createSScPlot(AngAvg2, BSavg2);hold on; PlotUtils.createSScPlot(AngAvg, BSLurton(AngAvg, BsParameters), 'k');  grid on ; title('BS average (average on time and then bin 1 deg)');
figure(85); PlotUtils.createSScPlot(AngAvg, BSavg3);hold on; PlotUtils.createSScPlot(AngAvg, BSLurton(AngAvg, BsParameters), 'k');  grid on ; title('BS average (simple average bin 1 deg)');
figure(95); PlotUtils.createSScPlot(th, bs);  grid on ; title('BS');

end
