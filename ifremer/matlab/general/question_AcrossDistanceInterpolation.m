%% Across distance interpolation method

function [flag, AcrossInterpolation] = question_AcrossDistanceInterpolation(varargin)

[varargin, QL] = getPropertyValue(varargin, 'QL', 2); %#ok<ASGLU>

if get_LevelQuestion >= QL
    str1 = 'Interpolation lat�rale';
    str2 = 'Across distance Interpolation';
    [AcrossInterpolation, flag] = my_listdlg(Lang(str1,str2), ...
        {Lang('Aucune','None'); Lang('Lin�raire','Linear'); Lang('Lin�aire sauf sur �pave','Linear except on wrecks')}, ...
        'SelectionMode', 'Single', ...
        'InitialValue', 2, 'ColorLevel', QL);
else
    AcrossInterpolation = 2;
    flag = 1;
end
