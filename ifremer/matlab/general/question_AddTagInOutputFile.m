function [flag, Tag] = question_AddTagInOutputFile

Tag = [];

str1 = 'Voulez-vous rajouter un TAG personnel dans les noms de fichiers qui seront g�n�r�s ?';
str2 = 'Do you want to add your own TAG into the file names that will be created ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end

if rep == 1
    str1 = 'Tag � inclure dans les noms de fichiers.';
    str2 = 'Tag to include in file names.';
    nomVar = {Lang(str1,str2)};
    value = {''};
    [value, flag] = my_inputdlg(nomVar, value);
    if ~flag
        return
    end
    Tag = value{1};
end
