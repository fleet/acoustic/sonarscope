function [flag, LimitAngles] = question_AngularLimits(varargin)

persistent persistentLimitAngles

[varargin, QL]           = getPropertyValue(varargin, 'QL', 3);
[varargin, AnglesMinMax] = getPropertyValue(varargin, 'AnglesMinMax', [-89 89]);
[varargin, LimitAngles]  = getPropertyValue(varargin, 'LimitAngles',  []); %#ok<ASGLU>

flag = 1;

if get_LevelQuestion < QL
    return
end

str1 = 'Voulez-vous limiter angulairement la mosa�que ?';
str2 = 'Do you want to limitate the mosaic between two angles  ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    if isempty(persistentLimitAngles)
        if isempty(LimitAngles)
            LimitAngles = [-85 85];
        end
    else
        LimitAngles = persistentLimitAngles;
    end
    str1 = 'Limitation angulaire.';
    str2 = 'Angular limitation';
    p   = ClParametre('Name', Lang('Angle min','Min angle'), 'Value', LimitAngles(1), ...
        'Unit', 'deg', 'MinValue', AnglesMinMax(1), 'MaxValue', 1);
    p(2) = ClParametre('Name', Lang('Angle max','Max angle'), 'Value', LimitAngles(2), ...
        'Unit', 'deg', 'MinValue', -1, 'MaxValue', AnglesMinMax(2));
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    LimitAngles = a.getParamsValue;
    persistentLimitAngles = LimitAngles;
end
