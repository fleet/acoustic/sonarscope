% [flag, question_CompensationTVG_WC] = question_CompensationTVG_WC

function [flag, CorrectionTVG, indAnswer] = question_CompensationTVG_WC(varargin)

[varargin, LogOnly]      = getPropertyValue(varargin, 'LogOnly',      false);
[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 2); %#ok<ASGLU>

flag          =  1;
CorrectionTVG = 20;
indAnswer     = [];

QL = get_LevelQuestion;
if QL == 1
    return
end

str1 = 'Type de compensation de la TVG de la WC';
str2 = 'Type of compensation of the WC TVG';
if LogOnly
    str = {'20 * log10(r)'; '30 * log10(r)'; '40 * log10(r)'};
else
    str = {'20 * log10(r)'; '30 * log10(r)'; '40 * log10(r)'; 'Ts'; 'Sv'};
end
[indAnswer, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', InitialValue, 'SelectionMode', 'Single');
if ~flag
    return
end
switch indAnswer
    case 1
        CorrectionTVG = 20;
    case 2
        CorrectionTVG = 30;
    case 3
        CorrectionTVG = 40;
    otherwise
        CorrectionTVG = str{indAnswer};
end