function [flag, rep] = question_CompleteExistingMosaic

str1 = 'Que voulez-vous faire ?';
str2 = 'What do you want to do ?';
str3 = 'Compl�ter la mosa�que existante';
str4 = 'Complete the existing mosaic';
str5 = 'Repartir � z�ro';
str6 = 'Redo the mosaic';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));