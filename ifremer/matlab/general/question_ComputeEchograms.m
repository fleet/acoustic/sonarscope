function [flag, StepCompute] = question_ComputeEchograms(varargin)

persistent persistent_StepDisplay

if isempty(persistent_StepDisplay)
    persistent_StepDisplay = 1;
end

[varargin, Title]       = getPropertyValue(varargin, 'Title',       []);
[varargin, StepCompute] = getPropertyValue(varargin, 'StepCompute', persistent_StepDisplay); %#ok<ASGLU>

QL = get_LevelQuestion;
if QL == 1
    flag =  1;
    return
end

if isempty(Title)
    str1 = 'Fréquence de calcul des échogrammes polaires (1=tous les pings)';
    str2 = 'Ping computation subsampling rate (1 = all the pings)';
    Title = Lang(str1,str2);
end

str1 = 'Récurrence';
str2 = 'Recurrence';
p = ClParametre('Name', Lang(str1,str2), 'Unit', 'Pings',  'MinValue', 1, 'MaxValue', 100, 'Value', StepCompute, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Title);
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
StepCompute = a.getParamsValue;

persistent_StepDisplay = StepCompute;
