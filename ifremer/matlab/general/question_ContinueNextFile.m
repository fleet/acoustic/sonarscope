% flag = question_ContinueNextFile

function flag = question_ContinueNextFile(varargin)

[varargin, k] = getPropertyValue(varargin, 'k', []);
[varargin, N] = getPropertyValue(varargin, 'N', []); %#ok<ASGLU>

if ~isempty(k) && ~isempty(N)
    if k == N
        flag = 1;
        return
    end
    str1 = sprintf('On continue avec le prochain fichier : %d/%d ?', k+1, N);
    str2 = sprintf('Do we continue with the next file : %d/%d ?', k+1, N);
else
    str1 = 'On continue avec le prochain fichier ?';
    str2 = 'Do we continue with the next file ?';
end

[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
flag = (rep == 1);
