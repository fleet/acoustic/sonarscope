function [flag, DisplayLevel] = question_DetailLevelBDA(varargin)

% Remarque : y'avait "DisplayLevel = DisplayLevel + 1;" pour le Kongsberg, pourquoi ?

[varargin, Default]  = getPropertyValue(varargin, 'Default',  1);
[varargin, Passport] = getPropertyValue(varargin, 'Passport', 0); %#ok<ASGLU>

if ~Passport || (get_LevelQuestion >= 3)
    str1 = 'Visualisations intermédiaires';
    str2 = 'Level of Intermediate display';
    str{1} = 'None';
    str{2} = 'Medium';
    str{3} = 'Full';
    [DisplayLevel, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', Default, 'SelectionMode', 'Single');
    if ~flag
        return
    end
else
    DisplayLevel = 2;
    flag = 1;
end
