function [flag, Priority, SpecularLimitAngle] = question_LinesOverlapping(varargin)

persistent persistent_SpecularLimitAngle

[varargin, QL] = getPropertyValue(varargin, 'QL', 3); %#ok<ASGLU>

flag               = 1;
Priority           = 1;
SpecularLimitAngle = [];

if get_LevelQuestion < QL
    return
end

str1 = 'Priorit� donn�e pour le recouvrement de profil : ';
str2 = 'Priority to be used for recovering profiles : ';
clear str
str{1} = Lang('Centre', 'Center');
str{2} = Lang('Extr�mit�s', 'Borders');
str{3} = Lang('Centre except� le sp�culaire', 'Center except specular');
[Priority, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'ColorLevel', 3);
if ~flag
    return
end

if Priority == 3
    if isempty(persistent_SpecularLimitAngle)
        SpecularLimitAngle = 20;
    else
        SpecularLimitAngle = persistent_SpecularLimitAngle;
    end
    
    str1 = 'Gestion du recouvrement : centre except� le sp�culaire';
    str2 = 'Overlapping : center except specular';
    p = ClParametre('Name', Lang('Limite angulaire', 'Angular limit'), ...
        'Unit', 'deg',  'MinValue', 0, 'MaxValue', 90, 'Value', SpecularLimitAngle);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    SpecularLimitAngle = a.getParamsValue;
    
    persistent_SpecularLimitAngle = SpecularLimitAngle;
end
