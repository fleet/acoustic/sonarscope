% [flag, MaskAfterDetection] = question_MaskWCBeyondDetection
% [flag, MaskAfterDetection] = question_MaskWCBeyondDetection('InitialValue', 2)

function [flag, MaskAfterDetection] = question_MaskWCBeyondDetection(varargin)

[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 1); %#ok<ASGLU>

str1 = 'Masquage au del� de la d�tection du fond ?';
str2 = 'Mask data after the bottom detection ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    MaskAfterDetection = [];
    return
end
MaskAfterDetection = (rep == 1);
