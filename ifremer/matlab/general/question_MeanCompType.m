% [flag, MeanCompType] = question_MeanCompType
% [flag, MeanCompType] = question_MeanCompType('InitialValue', 2)
% [flag, MeanCompType] = question_MeanCompType('QFIfremer', 1) 

function [flag, MeanCompType] = question_MeanCompType(varargin)

[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 1);
[varargin, QFIfremer]    = getPropertyValue(varargin, 'QFIfremer',    0); %#ok<ASGLU>

str1 = 'Moyennage des valeurs en :';
str2 = 'Averaging values on : ';
if QFIfremer
    str{1} = 'QF';
    str{2} = 'Uncertainty';
    [MeanCompType, flag] = my_listdlg(Lang(str1,str2), str, 'Init', InitialValue);
    if ~flag
        return
    end
    MeanCompType = MeanCompType + 4;
else
    str{1} = 'dB';
    str{2} = 'Amplitude';
    str{3} = 'Energy';
    str{4} = 'Median';
    [MeanCompType, flag] = my_listdlg(Lang(str1,str2), str, 'Init', InitialValue);
    if ~flag
        return
    end
end
