function [flag, window] = question_MosaicInterpolationWindow(window, varargin)

[varargin, InitValue] = getPropertyValue(varargin, 'InitValue', [5 5]); %#ok<ASGLU>

flag = 1;
if isempty(window)
    if get_LevelQuestion >= 2
        str1 = 'Fen�tre d''interpolation des mosaiques';
        str2 = 'Mosaic interpolation window';
        [flag, window] = saisie_window(InitValue, 'Titre', Lang(str1,str2));
        if ~flag
            return
        end
    else
        window = [5 5];
    end
end
