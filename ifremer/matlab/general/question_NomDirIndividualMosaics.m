function [flag, repExport] = question_NomDirIndividualMosaics(repExport)

if isempty(repExport)
    repExport = my_tempdir;
end

str1 = 'S�lectionnez le r�pertoire o� seront cr��s les mosa�ques individuelles.';
str2 = 'Selec the output directory for the individual mosaiks. ';
[flag, repExport] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
