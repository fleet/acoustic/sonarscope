function [flag, useParallel] = question_NumberOfWorkers

MaxValue = getNumberOfWorkers;

Value = MaxValue;

str1 = 'Nombre de "workers"';
str2 = 'Number of workers';
str3 = 'Parallel toolbox : définition du nombre de "workers"';
str4 = 'Parallel toolbox : set the number of workers';
p = ClParametre('Name', Lang(str1,str2), 'MinValue', 2, 'MaxValue', MaxValue, 'Value', Value, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str3,str4));
a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -0 -2 0 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    useParallel = [];
    return
end
useParallel = a.getParamsValue;
