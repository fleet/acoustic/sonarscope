% [flag, SameReflectivityStatus] = question_SameReflectivityStatus
% [flag, SameReflectivityStatus] = question_SameReflectivityStatus('InitialValue', 2)

function [flag, SameReflectivityStatus] = question_SameReflectivityStatus(varargin)

[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 1); %#ok<ASGLU>

SameReflectivityStatus = [];

str1 = 'Voulez-vous ramener la donn�e de r�flectivit� des .all dans le m�me �tat que celui de l''image courante (BS, DiagTx, TVG, etc ...) ?';
str2 = 'Do you want to set the reflectivity data to the same status as the current image (BS, DiagTx, TVG, etc ...) ?';
[choix, flag] = my_questdlg(Lang(str1,str2), 'Init', InitialValue);
if ~flag
    return
end
SameReflectivityStatus = (choix == 1);
