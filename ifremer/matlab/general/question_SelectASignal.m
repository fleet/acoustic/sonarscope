function [flag, identSignal, signalNames, step] = question_SelectASignal(ListeSignaux, varargin)

[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
[varargin, InitialValue]  = getPropertyValue(varargin, 'InitialValue',  1); %#ok<ASGLU>

signalNames = '';
step        = 1;

%% Sélection du signal

str1 = 'Sélectionnez les "datagrammes"';
str2 = 'Select the "datagram" packets';
[identSignal, flag] = my_listdlg(Lang(str1,str2), ListeSignaux, 'InitialValue', InitialValue, 'SelectionMode', SelectionMode);
if ~flag
    return
end
signalNames = ListeSignaux(identSignal);
