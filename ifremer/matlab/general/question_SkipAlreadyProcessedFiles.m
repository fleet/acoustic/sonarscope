% N = length(listeFic);
% [flag, skipAlreadyProcessedFiles] = question_SkipAlreadyProcessedFiles(N);

function [flag, skipAlreadyProcessedFiles] = question_SkipAlreadyProcessedFiles(N, varargin)

persistent persistent_skipExistingMosaic

[varargin, Title] = getPropertyValue(varargin, 'Title', []); %#ok<ASGLU>

if N > 1 % TODO : faut-il faire cette condition ???
    if isempty(Title)
        str1 = 'Passer les fichiers d�j� trait�s ?';
        str2 = 'Skip already processed files ?';
        Title = Lang(str1,str2);
    end
    if isempty(persistent_skipExistingMosaic)
        skipAlreadyProcessedFiles = 2;
    else
        skipAlreadyProcessedFiles = persistent_skipExistingMosaic;
    end
    [rep, flag] = my_questdlg(Title, 'Init', skipAlreadyProcessedFiles);
    if ~flag
        return
    end
    skipAlreadyProcessedFiles = (rep == 1);
    persistent_skipExistingMosaic = skipAlreadyProcessedFiles;
else
    flag = 1;
    skipAlreadyProcessedFiles = 0;
end
