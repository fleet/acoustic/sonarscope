% [flag, flagTideCorrection] = question_TideCorrection
% [flag, flagTideCorrection] = question_TideCorrection('InitialValue', 2)

function [flag, flagTideCorrection] = question_TideCorrection(varargin)

[varargin, flagS7K]      = getPropertyValue(varargin, 'flagS7K',      0);
[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 1); %#ok<ASGLU>

flagTideCorrection = [];

if flagS7K
    str1 = 'Voulez-vous faire une correction de mar�e ?';
    str2 = 'Do you want to apply the tide correction ?';
else
    str1 = 'Voulez-vous faire une correction de mar�e ? (� condition qu''elle ait �t� import�e pr�alablement)';
    str2 = 'Do you want to apply the tide correction (if it has been imported previously) ?';
end
[choix, flag] = my_questdlg(Lang(str1,str2), 'Init', InitialValue);
if ~flag
    return
end
flagTideCorrection = (choix == 1);
