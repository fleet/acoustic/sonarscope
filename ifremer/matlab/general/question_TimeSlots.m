function [flag, SlotFilename, SonarTime] = question_TimeSlots(varargin)

[varargin, QL]           = getPropertyValue(varargin, 'QL', 3);
[varargin, nomDirImport] = getPropertyValue(varargin, 'repImport', my_tempdir); %#ok<ASGLU>

flag         = 1;
SlotFilename = [];
SonarTime    = [];

if get_LevelQuestion < QL
    return
end

str1 = 'Voulez-vous utiliser une d�coupe horaire ?';
str2 = 'Consider only some time intervals ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    [flag, decFile] = uiSelectFiles('ExtensionFiles', {'.txt'; '.dec'}, 'AllFilters', 1, 'RepDefaut', nomDirImport);
    if ~flag || isempty(decFile)
        return
    end
    try
        SonarTime = lecFicDecCar(decFile{1});
        SlotFilename = decFile{1};
    catch
        messageErreurFichier(decFile{1}, 'ReadFailure');
        flag = 0;
    end
end
