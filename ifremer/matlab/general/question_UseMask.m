function [flag, MasqueActif] = question_UseMask(MasqueActif, varargin)

[varargin, QL] = getPropertyValue(varargin, 'QL', 2); %#ok<ASGLU>

flag = 1;

if get_LevelQuestion >= QL
    if isempty(MasqueActif)
        str1 = 'Voulez-vous utiliser le masque des faisceaux (obtenu apr�s un nettoyage ou un import des flags de Caris, etc ...)?';
        str2 = 'Use the soundings Mask (obtained after a data cleaning or import of CARIS flags, etc ...)?';
        [MasqueActif, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel', QL, 'Init', 1);
        if ~flag
            return
        end
    end
else
    MasqueActif = 1;
end
