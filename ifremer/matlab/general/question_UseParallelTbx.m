function [flag, useParallel] = question_UseParallelTbx(varargin)

[varargin, N] = getPropertyValue(varargin, 'N', []); %#ok<ASGLU>

if ~isempty(N) && (N <= 1) % <= est justifi� car on peut faire un appel avec N-1 si par example on veut poser la question que si N > 2
    flag = 1;
    useParallel = 0;
    return
end

str1 = 'Voulez-vous utiliser la Parallel Toolbox pour ce traitement ?';
str2 = 'Do you want to use the Parallel Toolbox for this processing ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2); %, 'WaitingTime', 10);
if ~flag
    useParallel = [];
    return
end
useParallel = (rep == 1);

if useParallel
    [flag, X] = question_NumberOfWorkers;
    if ~flag
        return
    end
    useParallel = X;
end
