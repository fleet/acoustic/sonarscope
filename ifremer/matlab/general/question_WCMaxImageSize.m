% [flag, MaxImageSize] = question_WCMaxImageSize
% [flag, MaxImageSize] = question_WCMaxImageSize('InitialValue', 1234)

function [flag, MaxImageSize] = question_WCMaxImageSize(varargin)

persistent LastMaxImageSize

[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', []); %#ok<ASGLU>

if isempty(InitialValue)
    if isempty(LastMaxImageSize)
        MaxImageSize = 1000;
    else
        MaxImageSize = LastMaxImageSize;
    end
else
    MaxImageSize = InitialValue;
end

QL = get_LevelQuestion;
if QL == 1
    flag =  1;
    return
end

str1 = 'Taille max des images WC (sqrt)';
str2 = 'Max size of WC images (sqrt)';
[flag, MaxImageSize] = inputOneParametre(Lang(str1,str2), Lang('Taille max', 'Max Image Size'), ...
    'Unit', 'pixel', 'Value', MaxImageSize, 'MinValue', 500, 'MaxValue', 2000);
if ~flag
    return
end
LastMaxImageSize = MaxImageSize;
