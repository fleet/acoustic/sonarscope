% [flag, typeAlgoWC] = question_WCRaytracingAlgorithm
% [flag, typeAlgoWC] = question_WCRaytracingAlgorithm('InitialValue', 2)

function [flag, typeAlgoWC] = question_WCRaytracingAlgorithm(varargin)

[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 1); %#ok<ASGLU>
% [varargin, QL] = getPropertyValue(varargin, 'QL', 3);

strTitre1 = 'Quel algorithme voulez-vous utiliser pour calculer la g�om�trie ?';
strTitre2 = 'What algorithm do you want to use to compute the ray tracing ?';
str{1} = 'Based on Depth and AcrossDist';
str{2} = 'Based on Angles and Ranges    (Recommanded for Kongsberg MBES in Hight density)';
if ~isdeployed
    str{3} = 'Second but optimized by first one';
end
[typeAlgoWC, flag] = my_listdlg(Lang(strTitre1, strTitre2), str, 'SelectionMode', 'Single', 'InitialValue', InitialValue);
if ~flag
    return
end
