% Liste des utilisateurs de licenses
%
% Syntax
%   qui
%
% Examples 
%   qui
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function qui

cmd = ['!' matlabroot '/etc/lmstat -f'];
eval(cmd)
