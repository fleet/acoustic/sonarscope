%{
xx = [1:.1:10];

aa= 0.6223 ; % raideur de la courbe
bb = -7.7691 ; % position horizontale de l'axe de sym~A
cc = 0.4538 ; % amplitude verticale de la courbe
dd = 1.9634 ; % translation verticale de la courbe
yy = cc * tanh( aa * (xx + bb) ) + dd ;
FigUtils.createSScFigure; h(1) = subplot(2,1,1) ; PlotUtils.createSScPlot(xx,yy, '*-'); grid on;

% yy2 = rayonCourbure(1:length(xx), xx, yy);
yy2 = rayonCourbure(xx, yy);

h(2) = subplot(2,1,2); PlotUtils.createSScPlot(xx, yy2, '*-'); grid on
linkaxes(h, 'x')
%}

function r = rayonCourbure(x, y, varargin)

[varargin, sig]  = getPropertyValue(varargin, 'Sigma', 1);
[varargin, Loop] = getPropertyValue(varargin, 'Loop',  0); %#ok<ASGLU>

pw = 1:30;
GaussianDieOff = 0.0001;
ssq = sig * sig;
width = find(exp(-(pw.*pw)/(2*ssq)) > GaussianDieOff, 1, 'last');
if isempty(width)
    width = 1;
end
W = width;
t = (-width:width);
gau = exp(-(t.*t)/(2*ssq)) / (2*pi*ssq);
gau = gau/sum(gau);

x = x(:);
y = y(:);
sz = size(y);

L = length(x);
if Loop % wrap around the curve by W pixles at both ends
    x2 = [x(L-W+1:L) ; x ; x(1:W)];
    y2 = [y(L-W+1:L) ; y ; y(1:W)];
else % extend each line curve by W pixels at both ends
    x2 = [ones(W,1)*2*x(1)-x(W+1:-1:2) ; x ; ones(W,1)*2*x(L)-x(L-1:-1:L-W)];
    y2 = [ones(W,1)*2*y(1)-y(W+1:-1:2) ; y ; ones(W,1)*2*y(L)-y(L-1:-1:L-W)];
end

xx2  = conv(x2, gau);
xx2  = xx2((W+1):(L+3*W));
yy2  = conv(y2, gau);
yy2  = yy2((W+1):(L+3*W));
Xu2  = [xx2(2)-xx2(1) ; (xx2(3:L+2*W)-xx2(1:L+2*W-2))/2 ; xx2(L+2*W)-xx2(L+2*W-1)];
Yu2  = [yy2(2)-yy2(1) ; (yy2(3:L+2*W)-yy2(1:L+2*W-2))/2 ; yy2(L+2*W)-yy2(L+2*W-1)];
Xuu2 = [Xu2(2)-Xu2(1) ; (Xu2(3:L+2*W)-Xu2(1:L+2*W-2))/2 ; Xu2(L+2*W)-Xu2(L+2*W-1)];
Yuu2 = [Yu2(2)-Yu2(1) ; (Yu2(3:L+2*W)-Yu2(1:L+2*W-2))/2 ; Yu2(L+2*W)-Yu2(L+2*W-1)];

% Rayon de courbure
r = ((Xu2.*Yuu2 - Xuu2.*Yu2) ./ ((Xu2.*Xu2 + Yu2.*Yu2) .^1.5 ));
if ~Loop
    r = r((width+1):(end-width));
end
r = reshape(r, sz);
r(1)   = r(2);     % TODO : a améliorer
r(end) = r(end-1); % TODO : a améliorer
