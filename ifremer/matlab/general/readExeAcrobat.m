% Détermination de la version Acrobat Reader en cours
%
% Syntax
%   nomExe = readExeAcrobat
%
% Input Arguments
%
% Name-Value Pair Arguments
%
% Output Arguments
%   nomExe : nom de l'exécutable
%
% Examples
%   nomExe = readExeAcrobat;
%
% See also /
% Authors : GLU
% VERSION  : $Id: get.m,v 1.3 2007/12/04 09:59:25 rgallou Exp rgallou $
% ----------------------------------------------------------------------------
function nomExeAcrobat = readExeAcrobat


pathAdobeReader = [];
for iLoop=5:10  % Versions supposées existantes.
    pathDirReg = sprintf('SOFTWARE\\Adobe\\Acrobat Reader\\%1i.0\\InstallPath', iLoop);
    try
        pathAdobeReader = winqueryreg('HKEY_LOCAL_MACHINE', pathDirReg, '');
        if ~isempty(pathAdobeReader)
            break; % Sortie de la boucle avec le chemin de la version.
        end
    catch %#ok<CTCH>
        continue
    end
end


nomExeAcrobat = 'AcroRd32.exe';
if ~isempty(pathAdobeReader)
    exeFullAcrobat = fullfile(pathAdobeReader, nomExeAcrobat);
    if ~exist(exeFullAcrobat, 'file')
        nomExeAcrobat = 'Acrobat.exe';
        nomExeAcrobat = fullfile(pathAdobeReader, nomExeAcrobat);
    end
end




