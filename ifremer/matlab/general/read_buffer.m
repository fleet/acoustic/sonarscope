% Lecture dans un buffer � la mani�re d'un fread
%
% Syntax
%   [x, adr] = read_buffer(tempo, adr, flagEndian, precision)
%
% INTPUT PARAMETERS :
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%   flagEndian : 1 si Little endian, 0 sinon
%   precision  : 'uint8', 'int8', 'uint16', 'int16', 'uint32' ou 'uint32'
%
% Output Arguments
%   x   : Valeur
%   adr : Offset remis � jour
%
% Examples 
%   [str, maxsize, endian]  = computer
%   flagEndian = strcmp(endian, 'L')
%
%   tempo = 200
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'uint8')
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'int8')
%
%   tempo = [130 11]
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'uint16')
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'int16')
%
%   tempo = [130 0 3 4]
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'uint32')
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'int32')
%
% See also write_buffer read_uint* read_int* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [x, adr] = read_buffer(tempo, adr, flagEndian, precision)

switch precision
    case 'uint8'
        [x, adr] = read_uint8(tempo, adr);
    case 'int8'
        [x, adr] = read_int8(tempo, adr);
    case 'uint16'
        [x, adr] = read_uint16(tempo, adr, flagEndian);
    case 'int16'
        [x, adr] = read_int16(tempo, adr, flagEndian);
    case 'uint32'
        [x, adr] = read_uint32(tempo, adr, flagEndian);
    case 'int32'
        [x, adr] = read_int32(tempo, adr, flagEndian);
    otherwise
end
