function [flag, shapes] = read_coastLines(nomFic)

flag = 0;
shapes = [];

fid = fopen(nomFic, 'r');
if fid == -1
    return
end

Titre = '';
label = 0;
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    mots = strsplit(tline);
    if (length(mots) >= 2) && strcmp(mots{1}, '#')
    elseif strcmp(mots{1}, '>')
        label = label + 1;
        shapes(end+1).labels = label; %#ok<AGROW>
        shapes(end).nomCourbe = Titre;
        shapes(end).X = [];
        shapes(end).Y = [];
    elseif (length(mots) == 2) && strcmp(mots{1}, '<')
        Titre = tline(3:end);
    elseif length(mots) == 2 || length(mots) == 3
         shapes(end).X(end+1) = str2double(mots{1});
         shapes(end).Y(end+1) = str2double(mots{2});
    end
end
fclose(fid);
flag = 1;

%{
figure
color = 'brgkymc';
for k=1:length(shapes)
    kmod = 1 + mod(k-1,length(color));
    plot(shapes(k).X, shapes(k).Y, color(kmod)); grid on; hold on;
    plot(shapes(k).X+180, shapes(k).Y, color(kmod)); grid on; hold on;
    plot(shapes(k).X-180, shapes(k).Y, color(kmod)); grid on; hold on;
end
%}
