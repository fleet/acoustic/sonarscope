% Cast en entier 16 bits non signe d'une serie de 2 octets
%
% Syntax
%   [x, adr] = read_int16(tempo, adr, flagEndian)
%
% INTPUT PARAMETERS :
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%   flagEndian : 1 si Little endian, 0 sinon
%
% Output Arguments
%   x   : Valeur
%   adr : Offset remis � jour
%
% Examples 
%   [str, maxsize, endian]  = computer
%   flagEndian = strcmp(endian, 'L')
%   tempo = [1 2]
%   [x, adr] = read_int16(tempo, 0, flagEndian)
%
% See also read_buffer read_int* read_uint* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [x, adr] = read_int16(tempo, adr, flagEndian)
if flagEndian
    o1 = tempo(adr+2);
    o2 = tempo(adr+1);
else
    o1 = tempo(adr+1);
    o2 = tempo(adr+2);
end
if o2 >= 128
    x = (o2-256)*256 + o1;
else
    x =       o2*256 + o1;
end
adr = adr + 2;
