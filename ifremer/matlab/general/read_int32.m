% Cast en entier 32 bits signe d'une serie de 4 octets
%
% Syntax
%   [x, adr] = read_int32(tempo, adr, flagEndian)
%
% INTPUT PARAMETERS :
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%   flagEndian : 1 si Little endian, 0 sinon
%
% Output Arguments
%   x   : Valeur
%   adr : Offset remis � jour
%
% Examples 
%   [str, maxsize, endian]  = computer
%   flagEndian = strcmp(endian, 'L')
%   tempo = [1 2 3 4]
%   [x, adr] = read_int32(tempo, 0, flagEndian)
%
% See also read_buffer read_int* read_uint* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [x, adr] = read_int32(tempo, adr, flagEndian)
if flagEndian
    o1 = tempo(adr+4);
    o2 = tempo(adr+3);
    o3 = tempo(adr+2);
    o4 = tempo(adr+1);
else
    o1 = tempo(adr+1);
    o2 = tempo(adr+2);
    o3 = tempo(adr+3);
    o4 = tempo(adr+4);
end
if o4 >= 128
    x = (o4-256)* 16777216 + o3*65536 + o2*256 + o1;
else
    x =      o4 * 16777216 + o3*65536 + o2*256 + o1;
end
adr = adr + 4;
