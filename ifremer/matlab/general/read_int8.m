% Cast en entier 8 bits non signe d'une serie de 2 octets
%
% Syntax
%   [x, adr] = read_int8(tempo, adr)
%
% INTPUT PARAMETERS :
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%
% Output Arguments
%   x   : Valeur
%   adr : Offset remis � jour
%
% Examples 
%   tempo = 200
%   [x, adr] = read_int8(tempo, 0)
%
% See also read_buffer read_int* read_uint* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [x, adr] = read_int8(tempo, adr)
x = tempo(adr+1);
if x >= 128
    x = x - 256;
end
adr = adr + 1;
