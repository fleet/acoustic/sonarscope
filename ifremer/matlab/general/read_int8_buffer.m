% Cast en entier 8 bits non signe d'une serie de 2 octets
%
% Syntax
%   [x, adr] = read_int8_buffer(tempo, adr)
%
% INTPUT PARAMETERS :
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%
% Output Arguments
%   x   : Valeur
%   adr : Offset remis � jour
%
% Examples
%   tempo = 200
%   [x, adr] = read_int8_buffer(tempo, 0)
%
% See also read_buffer read_int* read_uint* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [x, adr] = read_int8_buffer(tempo, adr, n)
x = tempo(adr+1:adr+n);
sub = find(x >= 128);
x(sub) = x(sub) - 256;
adr = adr + n;
