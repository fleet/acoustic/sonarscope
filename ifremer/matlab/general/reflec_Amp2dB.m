% Transforms Amplitude values in dB : xdB = 20 * log10(xNat)
%
% Syntax
%   xdB = reflec_Amp2dB(xNat)
%
% Input Arguments
%   xNat : Values in Amplitude
%
% Output Arguments
%   xdB : Values in dB
%
% Examples
%   reflec_Amp2dB(0.0001)
%   reflec_Amp2dB([0 0.1 0.01 0.001 0.0001])
%
% See also reflec_dB2Amp cl_image/reflec_Amp2dB sum_dB reflec_Enr2dB Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function xdB = reflec_Amp2dB(xNat)

if isempty(xNat)
    xdB = [];
else
    xNat(xNat <= 0) = NaN;
    xdB = 20 * log10(xNat);
end
