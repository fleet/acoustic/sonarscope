% Transforms Energy values in dB : xdB = 10 * log10(xNat)
%
% Syntax
%   xdB = reflec_Enr2dB(xNat)
%
% Input Arguments
%   xNat : Values in Energy
%
% Output Arguments
%   xdB : Values in dB
%
% Examples
%   reflec_Enr2dB(0.0001)
%   reflec_Enr2dB([0 0.1 0.01 0.001 0.0001])
%
% See also reflec_dB2Enr cl_image/reflec_Enr2dB sum_dB reflec_Amp2dB Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function xdB = reflec_Enr2dB(xNat)

if isempty(xNat)
    xdB = [];
else
    xNat(xNat <= 0) = NaN;
    xdB = 10 * log10(xNat);
end
