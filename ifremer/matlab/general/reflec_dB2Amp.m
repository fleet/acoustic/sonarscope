% Transforms Amplitude values in dB : xNat = 10 .^ (xdB / 20)
%
% Syntax
%   xNat = reflec_dB2Amp(xdB)
%
% Input Arguments
%	xdB : Values in dB
%
% Output Arguments
%   xNat : Values in Amplitude
%
% Examples
% 	reflec_dB2Amp(-30)
% 	reflec_dB2Amp(-40:5:-20)
%
% See also reflec_Amp2dB reflec_dB2Enr cl_image/reflec_dB2Amp sum_dB reflec_dB2Enr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function xNat = reflec_dB2Amp(xdB)
xNat = 10 .^ (xdB / 20);
