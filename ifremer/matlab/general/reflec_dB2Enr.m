% Transforms Energy values in dB : xNat = 10 .^ (xdB / 10)
%
% Syntax
%   xNat = reflec_dB2Enr(xdB)
%
% Input Arguments
%	xdB : Values in dB
%
% Output Arguments
%   xNat : Values in Amplitude
%
% Examples
% 	reflec_dB2Enr(-30)
% 	reflec_dB2Enr(-40:5:-20)
%
% See also reflec_Enr2dB cl_image/reflec_dB2Enr sum_dB reflec_Amp2dB Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function xNat = reflec_dB2Enr(xdB)
xNat = 10 .^ (xdB / 10);
