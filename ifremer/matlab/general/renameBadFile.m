% TODO utiliser la fonction Matlab native dont je ne me souviens plus le
% nom (voir mail Roger)
function renameBadFile(nomFicIn, varargin)

[varargin, Message] = getPropertyValue(varargin, 'Message', []); %#ok<ASGLU>

if ~isempty(Message)
%     fprintf('%s\n', Message);
    my_warndlg(Message, 0, 'TimeDelay', 60);
end

[nomDir, nomFic, ext] = fileparts(nomFicIn);
nomFicAllRenamed = fullfile(nomDir, [nomFic '_' ext(2:end) '.bad']);

if exist(nomFicIn, 'file')
    try
        movefile(nomFicIn, nomFicAllRenamed)
    catch ME
        my_warndlg(ME.message, 0);
    end
end

