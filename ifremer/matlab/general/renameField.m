function S = renameField(S, fieldToRename, newFieldName)
% S = renameField(S, fieldToRename, newFieldName)

    if ~isfield(S, newFieldName) && isfield(S, fieldToRename)
        S.(newFieldName)    = S.(fieldToRename);
        S                   = rmfield(S, fieldToRename);
    end
        