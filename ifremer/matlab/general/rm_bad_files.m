% Suppression des fichiers ind�sirables
%
% Syntax
%   rm_bad_files
%
% Input Arguments
%   fcnx : liste de fonctions ou methodes a convertir
%
% Examples
%   rm_bad_files
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function rm_bad_files

global IfrTbx %#ok<GVMIS>

WorkInProgress('Recherche des fichiers .asv, .mat, .gmtdefaults4 et .gmtcommands4')

[ListeAsv, ListeMat, ListeGmt1, ListeGmt2] = listeFicOnDepthDir(IfrTbx, '.asv', '.mat', '.gmtdefaults4', '.gmtcommands4');

%% Suppression des fichiers .asv

for i=1:length(ListeAsv)
    try
        delete(ListeAsv{i})
    catch %#ok<CTCH>
    end
end

%% Suppression des fichiers .gmtdefaults4

for i=1:length(ListeGmt1)
    try
        delete(ListeGmt1{i})
    catch %#ok<CTCH>
    end
end

%% Suppression des fichiers .gmtcommands4

for i=1:length(ListeGmt2)
    try
        delete(ListeGmt2{i})
    catch %#ok<CTCH>
    end
end

%% Suppression des fichiers .mat

if ~isempty(ListeMat)
    [sub, flag] = my_listdlg('Fichiers .mat � supprimeer', ListeMat, 'InitialValue', []);
    if ~flag
        return
    end
    if ~isempty(sub)
        msg1 = sprintf('Est-tu certain de vouloir supprimer ces %d fichiers ?', length(sub));
        msg2 = 'TODO';
        [rep, flag] = my_questdlg(Lang(msg1,msg2), 'Init', 2);
        if ~flag || (rep == 2)
            return
        end
        [rep, flag] = my_questdlg(Lang('Sans regrets ?', 'TODO'), 'Init', 2);
        if ~flag || (rep == 2)
            return
        end
    end
    for i=1:length(sub)
        delete(ListeMat{sub(i)})
    end
end
