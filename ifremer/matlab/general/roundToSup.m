% Arrondi (unit�, dizaine, centaine, d�cimale, ...)
%
% Syntax
%   Y = roundToSup(X)
%
% Input Arguments
%   X : valeur � arrondir
%
% Output Arguments
%   Y : caleur arrondie
%
% Examples
%   Y = roundToSup(3370)      # 4000
%   y = roundToSup(-0.003370) # -0.0040
%
% See also cl_image/plotGMT Authors
% Authors : GLU
%--------------------------------------------------------------------------

function Y = roundToSup(X)

iLoop = 0;
A = X;
if (abs(A) > 1)
    while abs(A) > 10
        A = abs(A) /10;
        iLoop = iLoop+1;
    end
    Y = sign(X)*((fix(A)+1)*10^iLoop);
elseif (abs(A) >0)
    while abs(A) < 1
        A = abs(A) *10;
        iLoop = iLoop+1;
    end
    Y = sign(X)*((fix(A)+1)/10^iLoop);
end
