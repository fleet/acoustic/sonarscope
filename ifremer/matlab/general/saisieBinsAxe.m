function [bins, flag] = saisieBinsAxe(x, Titre, Unit) 

x = unique(x); % RAJOUTE le 13/11/2006 : donn�es RESON SonarFrequency
if length(x) == 1
    bins = x;
    flag = 1;
    return
end

Pas = abs(x(2)-x(1));
% Pas = round(Pas, 2, 'significant'); % TODO or NotTODO ?

ValMin = min(x);
ValMax = max(x);

if Pas == 0
    Pas = 1;
end

PasMax = ValMax - ValMin;
if PasMax == 0
    PasMax = 1;
end
if isinf(PasMax)
    PasMax = 1;
end

str1 = sprintf('Valeurs extr�mes et intervalle de\n%s', Titre);
str2 = sprintf('Extreme values and step of\n%s', Titre);
p    = ClParametre('Name', 'Max',               'Unit', Unit, 'Value', ValMax, 'MinValue', ValMin, 'MaxValue', ValMax);
p(2) = ClParametre('Name', 'Min',               'Unit', Unit, 'Value', ValMin, 'MinValue', ValMin, 'MaxValue', ValMax);
p(3) = ClParametre('Name', Lang('Pas', 'Step'), 'Unit', Unit, 'Value', Pas,    'MinValue', Pas,    'MaxValue', PasMax);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    bins = [];
    return
end
value = a.getParamsValue;
if ~flag
    return
end
Max = value(1);
Min = value(2);
Pas = value(3);
bins = Min:Pas:Max;
