% depth   = 20;
% TetaMax = 60;
% T       = 0.15; % length of ripple in meters
% h       = 0.02; % height of ripple in meters
% as      = 0.25; % assymetry from left side
%
% coefNoiseHeight      = 0;
% coefNoisePeriodicity = 0;
% coefNoiseRipple      = 0;
% coefNoiseAs          = 0;
% [x, y] = sandRipples(depth, TetaMax, T, h, as, coefNoiseHeight, coefNoisePeriodicity, coefNoiseRipple, coefNoiseAs);
% figure; plot(x, y); grid on
%
% coefNoiseHeight      = 1;
% coefNoisePeriodicity = 1;
% coefNoiseRipple      = 1;
% coefNoiseAs          = 0;
% [x, y] = sandRipples(depth, TetaMax, T, h, as, coefNoiseHeight, coefNoisePeriodicity, coefNoiseRipple, coefNoiseAs);
% figure; plot(x, y); grid on
% 
% coefNoiseHeight      = 0.5;
% coefNoisePeriodicity = 0.25;
% coefNoiseRipple      = 0.1;
% coefNoiseAs          = 0.3;
% [x, y] = sandRipples(depth, TetaMax, T, h, as, coefNoiseHeight, coefNoisePeriodicity, coefNoiseRipple, coefNoiseAs);
% figure; plot(x, y); grid on

function [x, y] = sandRipples(depth, TetaMax, T, h, as, coefNoiseHeight, coefNoisePeriodicity, coefNoiseRipple, coefNoiseAs)

Tpoints = 50;
step = T / Tpoints;

acrossDist = depth * tand(TetaMax);
nbripples  = 2 * ceil(max(acrossDist) / T); % number of ripples
nbripples = nbripples + 2;

for k=nbripples:-1:1
    TPointsRipple = floor(Tpoints * rand1(1, coefNoisePeriodicity));
    y3{k} = oneRipple(TPointsRipple, h, as * rand1(1, coefNoiseAs)) * rand1(1, coefNoiseRipple);
%     y3{k} = oneSinusRipple(TPointsRipple, h, as * rand1(1, coefNoiseAs)) * rand1(1, coefNoiseRipple);
%     y3{k} = oneRectangleRipple(TPointsRipple, h, as * rand1(1, coefNoiseAs)) * rand1(1, coefNoiseRipple);
end

y = [y3{:}];
XMax = step*length(y)/2;
x = -XMax:step:XMax;
x = x(1:length(y));

% wc = 0.05 * 50/Tpoints;
% [b, a] = butter(1, wc);
% y = filtfilt(b, a, y);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(x, y, 'b') ; grid on;

% w = 7 * Tpoints/50;
% w = gausswin(w); % 200 10
% y = filtfilt(w'/sum(w), 1, y);

y = y .* rand1(size(y), coefNoiseHeight);
w = ceil(7 * Tpoints/50);
w = gausswin(w); % 200 10
y = filtfilt(w'/sum(w), 1, y);

% y = y .* rand1(size(y), coefNoiseHeight);
% w = ceil(7 * Tpoints/50);
% y = filtfilt(w'/sum(w), 1, y);
 
% wc = 0.1 * 50/Tpoints;
% [b, a] = butter(1, wc);
% % figure; freqz(b,a)
% y = y .* rand1(size(y), coefNoiseHeight*3);
% y2 = filter(b, a, y);
% % FigUtils.createSScFigure; PlotUtils.createSScPlot(x, y, 'b') ; grid on; hold on; PlotUtils.createSScPlot(x, y2, 'r')
% FigUtils.createSScFigure; PlotUtils.createSScPlot(x, y2, 'b') ; grid on;

x = x((Tpoints+1):(end-Tpoints));
y = y((Tpoints+1):(end-Tpoints));

% To be done if oneRipple is not the cos one
% s = std(y);
% y = y * (2*h / (s * 1.2));
   
function y3 = oneTrianglesRipple(Tpoints, h, as)
asp = floor(Tpoints * as);
n1  = Tpoints - asp;
n2  = Tpoints - n1 + 2;
hs2 = h/2;
y1 = linspace(-hs2, hs2, n1);
y2 = linspace(hs2, -hs2, n2);
y3 = [y1 y2(2:end-1)];
      

function y3 = oneRipple(Tpoints, h, as)
asp = floor(Tpoints * as);
n1  = Tpoints - asp;
n2  = Tpoints - n1 + 2;
x1 = linspace(0, pi, n1);
x2 = linspace(pi, 2*pi, n2);
hs2 = h / 2;

x = [x1 x2(2:end-1)];
y3 = hs2 * -cos(x);

function y3 = oneSinusRippleOld(Tpoints, h, as)
asp = floor(Tpoints * as);
hs2 = h / 2;
x = linspace(0, 2*pi, Tpoints);
y3 = hs2 * sin(x);

function y3 = oneRectangleRipple(Tpoints, h, as)
asp = floor(Tpoints * as);
hs2 = h / 2;
x = linspace(0, 2*pi, Tpoints);
ns2 = floor(Tpoints / 2);
y3 = hs2 * [zeros(1,ns2) ones(1,Tpoints - ns2)];

function r = rand1(sz, alpha)
r = 1 + alpha * (rand(sz) - 0.5);