% Decomposition d'une liste d'indices en plusieurs sous-listes de progression aritmetiques
%
% Syntax
%  [start, count, stride] = sepsub(subl)
%
% Input Arguments 
%   subl : Un liste d'indices
% 
% Output Arguments
%   start  : Indice du debut de chaque sous-liste
%   count  : Nombre d'indices dans chaque sous-liste
%   stride : Increments des indices dans chaque sous-liste
%
% Examples
%   subl = 10:20
%   [start, count, stride] = sepsub(subl)
%   subl = linspace(1,30,10)
%   [start, count, stride] = sepsub(subl)
%
% See also Authors
% Authors : JMA
% VERSION  : $Id: sepsub.m,v 1.3 2002/06/06 11:50:16 augustin Exp $
%-------------------------------------------------------------------------------
  
function [start, count, stride, sub] = sepsub(subl)

subl =  floor(subl);
d = unique(diff(subl));
if length(d) == 1

    % Cas simple, l'increment est constant

    start(1)  = subl(1);
    count(1)  = length(subl);
    stride(1) = d;
    sub{1} = subl;
else

    % Plusieurs increments sont presents

    start(1)  = subl(1);
    count(1)  = 1;
    stride(1) = 0;

    for i=2:length(subl)
        if stride(end) == 0 || (subl(i) - subl(i-1)) == stride(end)
            count(end)  = count(end) + 1; 
            stride(end) = subl(i) - subl(i-1); 
        else
            start(end+1)  = subl(i); %#ok<AGROW>
            count(end+1)  = 1; %#ok<AGROW>
            stride(end+1) = 0; %#ok<AGROW>
        end
    end
end

for i=1:length(start)
    sub{i} = (0:count(i)-1) * stride(i) + start(i); %#ok<AGROW>
end
