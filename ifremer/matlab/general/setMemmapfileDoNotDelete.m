function Data = setMemmapfileDoNotDelete(Data, status)

for k=1:length(Data.Images)
    name = Data.Images(k).Name;
    if isa(Data.(name), 'cl_memmapfile')
        Data.(name) = set(Data.(name), 'DoNotDelete', status);
    end
end
