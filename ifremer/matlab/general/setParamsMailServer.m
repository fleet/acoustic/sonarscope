% Description
%   Set the parameters to be able to send an email from Matlab
%
% Syntax
%   setParamsMailServer
%
% Examples
%   setParamsMailServer
%
% Authors  : JMA
% See also : SendEmailSupport SendBugReport
%-------------------------------------------------------------------------------

% TODO : en cas de complication (serveur avec mot de passe, etc : voir 
% https://fr.mathworks.com/matlabcentral/answers/95708-how-do-i-use-sendmail-to-send-email-from-a-smtp-server-that-uses-authorization-in-matlab-7-10-r2010

function setParamsMailServer

global SMTP_Server %#ok<GVMIS> 
global EndUserEmail %#ok<GVMIS> 

if isempty(SMTP_Server)
    SMTP_Server = 'smtp';
end

if isempty(EndUserEmail)
    EndUserEmail = 'sonarscope@ifremer.fr';
end

setpref('Internet', 'SMTP_Server', SMTP_Server);
setpref('Internet', 'E_mail',      EndUserEmail);
