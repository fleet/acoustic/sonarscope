% Transformation d'une valeur "double" en "single". Si la variable x n'est pas en "double", elle n'est pas modifiee
% Cette transformation est utile pour gerer de grosses matrices mais n'est utilisable
% en realite qu'a partir de la version 7 de Matlab (un grand nombre de fonctions
% n'acceptent que des "double" dans les versions precedantes).
%
% Syntax
%   y = singleUnlessDouble(x, ValNaN)
%
% Input Arguments
%   x      : Valeur d'entree
%   ValNaN : 
%
% Output Arguments
%   y : Valeur de sortie
% 
% Examples
%    format long
%    pi
%    y = singleUnlessDouble(pi, NaN)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function x = singleUnlessDouble(x, ValNaN)

if isnan(ValNaN)
    subNaN = [];
else
    subNaN = (x == ValNaN);
end

if ~isfloat(x)
	x = single(x(:,:,:));
end
x(subNaN) = NaN;
