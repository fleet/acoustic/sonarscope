function special_message(Tag, varargin)

[varargin, NoWindow] = getFlag(varargin, 'NoWindow');
[varargin, NoStop]   = getFlag(varargin, 'NoStop');

switch Tag
    case 'CreationRepertoireReussie'
        str1 = sprintf('Le r�pertoire "%s" a �t� cr�e avec succes.', varargin{1});
        str2 = sprintf('Directory "%s" has been created successfully.', varargin{1});
        fprintf('%s\n', Lang(str1,str2));
        
    case 'CreationRepertoireEchec'
        str1 = sprintf('Echec cr�ation r�pertoire "%s".\nJe vous conseille de rendre cela possible, �a permet de formater la donn�e en fichiers matlab pour des acc�s futurs plus rapides.', varargin{1});
        str2 = sprintf('Impossible to create directory  "%s". It is recommended to make it possible to improve the data time access.', varargin{1});
        if NoWindow
            fprintf('%s\n', Lang(str1,str2));
        else
            my_warndlg(Lang(str1,str2), NoStop, 'Tag', 'special_message');
        end
        
    case 'DeleteRepertoireEchec'
        str1 = sprintf('Echec suppression r�pertoire "%s"', varargin{1});
        str2 = sprintf('Impossible to delete directory  "%s".', varargin{1});
        if NoWindow
            fprintf('%s\n', Lang(str1,str2));
        else
            my_warndlg(Lang(str1,str2), NoStop, 'Tag', 'special_message');
        end
        
    case 'FichierNonExistant'
        str1 = sprintf('Le fichier "%s" n''existe pas.', varargin{1});
        str2 = sprintf('File "%s" does not exist.', varargin{1});
        if NoWindow
            fprintf('%s\n', Lang(str1,str2));
        else
            my_warndlg(Lang(str1,str2), NoStop, 'Tag', 'special_message');
        end

    case 'FichierCacheAbsent'
        str1 = sprintf('Le fichier "%s" n''existe pas dans le cache SSC. Veuillez supprimer ce dernier et r�it�rer l''op�ration initale', varargin{1});
        str2 = sprintf('File "%s" does not exist in the SSC Cache. Please, erase this one and redo the first process you try', varargin{1});
        if NoWindow
            fprintf('%s\n', Lang(str1,str2));
        else
            my_warndlg(Lang(str1,str2), NoStop, 'Tag', 'special_message');
        end
        
    otherwise
        my_warndlg(sprintf('Tag "%s" undone in function "special_message".', varargin{1}), NoStop, 'Tag', 'special_message');
end
