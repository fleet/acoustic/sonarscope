% Distorsion d'une matrice
%
% Syntax
%   y = stretchmat(x, n, ...)
%   y = stretchmat(x, [n, m], ...)
%
% Input Arguments
%   x : Matrice de depart
%   n : Nombre de lignes de la matrice y
%
% Optional Input Arguments
%   m : Nombre de colonnes de la matrice y
%   method : Methode d'interpolation 'nearest', linear', 'cubic' ou 'spline'
%
% Output Arguments
%   y : Matrice deformee
%
% Examples 
%   stretchmat([1 2;3 4], 16)
%   stretchmat([1 2;3 4], [8 16], 'nearest')
%
% See also interp2 reshape Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function I = stretchmat(z, n, varargin)

% MAINTENANCE A FAIRE
% stretchmat pourrait �tre purement et simplement remplac� par imresize(z, [n m],varargin{:});
% A faire au fur et � mesure et supprimer la fonction lorsqu'il n'y aura
% plus d'appel

if length(n) == 1
    m = n;
else
    m = n(2);
    n = n(1);
end

[ny, nx] = size(z);
x  = linspace(1, nx, nx);
y  = linspace(1, ny, ny);
xi = linspace(1, nx, m);
yi = linspace(1, ny, n);
if ny == 1
    I = interp1(x, z, xi, varargin{:});
    I = repmat(I, n, 1);
elseif nx == 1
    I = interp1(y, z, yi, varargin{:});
    I = repmat(I', 1, m);
else
    I = imresize(z, [n m],varargin{:});
    
%     if strcmpi(varargin{1}, 'nearest') || strcmpi(varargin{1}, '*nearest')
%         subLig = round(linspace(1,ny,n));
%         subcol = round(linspace(1,nx,m));
%         I = zeros(n, m, class(z));
%         for i=1:n
%             I(i,:) = z(subLig(i),subcol);
%         end
%     else
%         I = interp2(x, y, z, xi, yi', varargin{:});
%     end
end
