function str = struct2table(S, varargin)

[varargin, Title] = getPropertyValue(varargin, 'Title', []); %#ok<ASGLU>

str = {};
names = fieldnames(S);
for k=1:length(names)
    str{end+1,1} = names{k}; %#ok<AGROW>
    val = S.(names{k});
    switch class(val)
        case 'char'
            str{end,2} = val;
        case {'double'; 'single'}
            str{end,2} = num2strCode(val);
        otherwise
            str{end,2} = sprintf('Data type = "%s", noy yet set in struct2table', class(val));
    end
end

if nargout == 0
    if isempty(Title)
        figure;
    else
        figure('Name', Title);
    end
    t = uitable;
    set(t, 'Data',  str)
    set(t, 'RearrangeableColumns', 'On')
    set(t, 'Unit', 'normalized', 'Position', [0 0 1 1])
end
