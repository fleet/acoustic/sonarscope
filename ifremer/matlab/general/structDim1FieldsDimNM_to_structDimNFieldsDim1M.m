function DataOut = structDim1FieldsDimNM_to_structDimNFieldsDim1M(DataIn)

N = length(DataIn);
if N ~= 1
    DataOut = DataIn;
    return
end

listFieldNames = fieldnames(DataIn);
nbFields = length(listFieldNames);
if any(contains(listFieldNames, 'DimM'))
    DimM = DataIn.DimM;
    DataIn = rmfield(DataIn, 'DimM');
else
    name = listFieldNames{1};
    [N, M] = size(DataIn.(name));
    DimM(1:N,1:nbFields) = M; % TODO : non test�
end

listFieldNames = fieldnames(DataIn);
nbFields = length(listFieldNames);
for k2=1:nbFields
    name = listFieldNames{k2};
    [N, M] = size(DataIn(1).(name));
%     if (M == 1) && (N > 1)
%         DataIn.(name) = DataIn.(name)';
%         [N, M] = size(DataIn(1).(name));
%     end
    for k1=1:N
        buffer = DataIn.(name)(k1,:);
%         if isa(buffer, 'datetime')
%             buffer = cl_time('timeMat', datenum(buffer(1:DimM(k1,k2))));
%             DataOut(k1).(name) = buffer(1:DimM(k1,k2)); %#ok<AGROW>
%         else
            DataOut(k1).(name) = buffer(1:DimM(k1,k2)); %#ok<AGROW>
%         end
%         DataOut.DimM(k1,k2) = M;
    end
end
