function [DataOut, nbFields] = structDimNFieldsDim1M_to_structDim1FieldsDimNM(DataIn)

N = length(DataIn);
if N == 1
    DataOut  = DataIn;
    nbFields = 0;
    return
end

listFieldNames = fieldnames(DataIn(1));
nbFields = length(listFieldNames);

for k1=1:N
    for k2=1:nbFields
        name = listFieldNames{k2};
        buffer = DataIn(k1).(name)(1,:);
        if isa(buffer, 'cl_time')
            buffer = datetime(buffer.timeMat, 'ConvertFrom', 'datenum');
        end
        M = length(buffer);
        DataOut.(name)(k1,1:M) = buffer;
        DataOut.DimM(k1,k2) = M;
    end
end
