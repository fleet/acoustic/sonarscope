function str = supressEmptyCell(str)

if ~iscell(str)
    return
end

for k=length(str):-1:1
    if isempty(str{k})
        str(k) = [];
    end
end
