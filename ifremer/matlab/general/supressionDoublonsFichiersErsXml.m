% listeFic{1} = 'toto';
% listeFic{2} = 'ooo1.ers';
% listeFic{3} = 'titi';
% listeFic{4} = 'ooo1.xml'
% listeFic{5} = 'tutu';
% listeFic{6} = 'ooo2.xml';
% listeFic{7} = 'azerty';
% listeFic{8} = 'ooo2.ers';
% listeFic{9} = 'ooo3.ers';
% listeFic{10} = 'ooo4.xml';
% supressionDoublonsFichiersErsXml(listeFic)

function listeFicOut = supressionDoublonsFichiersErsXml(listeFic)

if ischar(listeFic)
    listeFicOut = listeFic;
    return
end

nbFic = length(listeFic);
IsErsOrXml = false(nbFic, 1);
listeFicOut    = cell(0);
listeFicErsXml = cell(0);
nbFicErs = 0;
nbFicXml = 0;
for k=1:nbFic
    [nomDir, nomFic, ext] = fileparts(listeFic{k});
    switch lower(ext)
        case '.ers'
            listeFicErsXml{end+1} = fullfile(nomDir, nomFic); %#ok<AGROW>
            IsErsOrXml(k) = true;
            nbFicErs = nbFicErs + 1;
        case '.xml'
            listeFicErsXml{end+1} = fullfile(nomDir, nomFic); %#ok<AGROW>
            IsErsOrXml(k) = true;
            nbFicXml = nbFicXml + 1;
        otherwise
            listeFicOut{end+1} = fullfile(nomDir, [nomFic ext]); %#ok<AGROW>
    end
end

if ~isempty(listeFicErsXml)
    [~, m ,n] = unique(listeFicErsXml, 'first');
    subDouble = true(length(n),1);
    subDouble(m) = false;
    IsErsOrXml = find(IsErsOrXml);
    listeFic(IsErsOrXml(subDouble)) = [];
    listeFicOut = listeFic;
    
    if (nbFicErs > 1) && (nbFicXml > 1)
        str1 = 'Il semble que vous ayez donn� une liste de fichiers qui sont redondants (.ers et .xml). Je prend le parti de supprimer les doublons.';
        str2 = 'It seems you selected redondant files (.ers and .xml). I took the decision to eliminate doubles.';
        my_warndlg(Lang(str1,str2), 1);
    end
end


