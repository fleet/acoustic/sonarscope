% Test générique du gradient analytique d'une fonction
% Ce teste s'applique aux fonctions "nomFct" pour lesquelles existent les fonctions "nomFctGrad"
% Exemple : AntenneSinc AntenneSincGrad
%
% Syntax
%   testGradient(nomFonc, x, parametres)
%
% Input Arguments
%   nomFonc : Nom de la fonction
%   x       : Abscisses ou doit etre evaluee la fonction
%   params  : Parametres autour desquels doit etre evaluee la derivation
%
% Name-Value Pair Arguments
%   indParams  : Indices des parametres a tester (tous par defaut)
%
% Examples
%   x = -75:75;
%
%   nomFonc = 'AntenneSinc'; params  = [1, 10.5,  10 ]; figure;
%   testGradient(nomFonc, x, params)
%   hold on; for i=1:3, testGradient(nomFonc, x, params, 'indParams', i), end; hold off
%
%   nomFonc = 'AntenneSincDep'; params = [1, 10.5,  10, 40 ]; figure;
%   testGradient(nomFonc, x, params)
%   hold on; for i=1:4, testGradient(nomFonc, x, params, 'indParams', i), end; hold off
%
%   nomFonc = 'AntennePoly'; params = [1, 10.5,  10 ]; figure;
%   testGradient(nomFonc, x, params)
%   hold on; for i=1:3, testGradient(nomFonc, x, params, 'indParams', i), end; hold off
%
%   nomFonc = 'AntenneTcheby'; params = [1, 10.5,  10,  0.4333,  4.4 ]; figure;
%   testGradient(nomFonc, x, params)
%   hold on; for i=1:5, testGradient(nomFonc, x, params, 'indParams', i), end; hold off
%
%   nomFonc = 'BSLurton'; params = [-5, 3, -30, 2., -20, 10]; figure;
%   testGradient(nomFonc, x, params)
%   hold on; for i=1:6, testGradient(nomFonc, x, params, 'indParams', i), end; hold off
%
% See also Authors
% Authors : YHDR + JMA
%-------------------------------------------------------------------------------

function testGradient(nomFonc, x, parametres, varargin)


%% Nombre de parametres

nparam = length(parametres);

%% Indices des parametres a etudier

[varargin, param0] = getPropertyValue(varargin, 'indParams', 1:nparam); %#ok<ASGLU>

%% Nom de la fonction du gradient analytique

nomFoncGrad = [nomFonc , 'Grad'];

%% Precision de calcul

prec = sqrt(eps);

%% Formation des deltas sur les parametres, ordre de grandeur "optimal"

dparam0 = max([prec * abs(parametres); prec * ones(1,nparam)]);

%% calcul valeur centrale
%
diagr = feval( nomFonc, x, parametres );

%% Calcul du gradient à la valeur centrale

[diagr2,JJ] = feval( nomFoncGrad, x, parametres);

%% Vérification valeur de la fonction

fprintf('Norme de la fonction : %d\n', norm(diagr-diagr2,'inf'))

for iv = 1:10
    jv = 10 ^ (5-iv);
    dparam = jv * dparam0;
    testparam = ones(nparam,1) * parametres + diag(dparam);
    
    testdiagr = feval( nomFonc, x, testparam );
    
    testJ = ( testdiagr - ones(nparam,1) * diagr ) ./ ( dparam' * ones(1,length(x)) );
    
    xx(iv) = jv; %#ok
    tdif = (JJ-testJ') ./ JJ;
    tt(iv) = norm( tdif(:, param0) ,'inf'); %#ok
end

%     figure;
h = plot(log10(xx), log10(tt)); grid on;
xlabel('Ordre de grandeur en \epsilon^{1/2}');
ylabel('Erreur relative maximale');
title(nomFonc)

if length(param0) == 1
    set(h, 'color', [0 0 0])
end
