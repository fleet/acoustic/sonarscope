% Cast en entier 32 bits signe d'une serie de 4 octets
%
% Syntax
%   [x, adr] = typecast_int32(tempo, adr, flagEndian)
%
% INTPUT PARAMETERS :
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%   flagEndian : 1 si Little endian, 0 sinon
%
% Output Arguments
%   x   : Valeur
%   adr : Offset remis � jour
%
% Examples
%   [str, maxsize, endian]  = computer
%   flagEndian = strcmp(endian, 'L')
%   tempo = [1 2 3 4]
%   [x, adr] = typecast_int32(tempo, 0, flagEndian)
%
% See also typecast_buffer typecast_int* typecast_uint* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [x, adr] = typecast_int32(tempo, adr, flagEndian) %#ok<INUSD>
x = typecast(tempo(adr+4:-1:adr+1), 'int32');
x = double(x);
adr = adr + 4;
