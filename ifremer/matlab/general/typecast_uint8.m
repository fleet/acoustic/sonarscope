% Cast en entier 8 bits non signe d'un octet
%
% Syntax
%   [x, adr] = typecast_uint8(tempo, adr, flagEndian)
%
% INTPUT PARAMETERS :
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%
% Output Arguments
%   x   : Valeur
%   adr : Offset remis � jour
%
% Examples
%   tempo = 200
%   [x, adr] = typecast_uint8(tempo, 0)
%
% See also read_buffer read_int* read_uint* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [x, adr] = typecast_uint8(tempo, adr)
x = typecast(tempo(adr+1), 'uint8');
x = double(x);
adr = adr + 1;

