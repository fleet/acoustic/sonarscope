% Methode de gestion des callback du composant
%
% Syntax
%   [flag, ListeOut] = uiOrdreItems(...)
%
% Name-Value Pair Arguments
%   RepDefaut       : R�pertoire par d�faut
%   ListeIn : Tableau de cellules des extensions de fichiers.
%
% Output Arguments
%   flag     : Validation (1 ou 0)
%   ListeOut : Liste des fichiers
%
% EXAMPLE :
%   [flag, ListeOut] = uiOrdreItems('ListeIn', {'A';'B';'C';'D'})
%
% See also
% Authors : GLU, JMA
% ----------------------------------------------------------------------------

function [Flag, ImagesSelect] = uiOrdreItems(varargin)

% Feel free to modify this file according to your needs !
% Fabrice.Pabois@ncl.ac.uk - University of Newcastle - 2000

[varargin, Intitule] = getPropertyValue(varargin, 'Intitule', Lang('Ordre des fichiers s�lectionn�s', 'Order of the selected Images'));

[varargin, uiAccess]  = getPropertyValue(varargin, 'Access', 'Start');
[varargin, ExtImages] = getPropertyValue(varargin, 'ListeIn', {}); %#ok<ASGLU>

if ischar(ExtImages)
    ExtImages = {ExtImages};
end

switch uiAccess
    case 'Start'
        h = LocalInit(ExtImages, Intitule);
        uiwait(h); % waitfor(h, 'Selected', 'on');
        x = get(h, 'UserData');
        ImagesSelect = getFromUserData('ImagesSelect', x);
        Flag = getFromUserData('Flag', x);
        delete(h);
        
    case 'AddFile'
        listOrigImages = findobj(gcbf,'Tag','listOrigImages');
        NewSelection = get(listOrigImages,'Value');
        
        if ~isempty(NewSelection)
            FileList = get(listOrigImages,'String');
            FullImages = FileList(NewSelection);
            
            listSelectImages = findobj(gcbf,'Tag','listSelectImages');
            % Option 'rows' non fonctionnelle sur des cha�nes de caract�res
            % de taille diff�rentes.
            newList = union(get(listSelectImages,'String'), FullImages);
            set(listSelectImages, 'String', newList, 'Value', []);
        end
        
    case 'AddAllImages'
        listOrigImages = findobj(gcbf,'Tag','listOrigImages');
        FileList = get(listOrigImages,'String');
        FullImages = FileList(:);
        
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        % Option 'rows' non fonctionnelle sur des cha�nes de caract�res
        % de taille diff�rentes.
        newList = union(get(listSelectImages,'String'), FullImages);
        % newList';
        set(listSelectImages, 'String', newList, 'Value', []);
        
    case 'RemFile'
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        ToRemove = get(listSelectImages,'Value');
        if any(ToRemove)
            FileList = cellstr(get(listSelectImages,'String'));
            FileList(ToRemove)=[];
            set(listSelectImages, 'String', FileList, 'Value',[]);
        end
        
    case 'RemAllImages'
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        FileList = cellstr(get(listSelectImages,'String'));
        FileList(:)=[];
        set(listSelectImages, 'String', FileList, 'Value',[]);
        
    case 'ShiftUp'
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        ToShift = get(listSelectImages,'Value');
        if ~isempty(ToShift)
            if ToShift(1)>1		% Can shift up then
                % Keep first contiguous block
                NonContiguous = find(diff(ToShift)~=1);
                StartPos = ToShift(1);
                if isempty(NonContiguous)	% Just 1 contiguous block
                    EndPos = ToShift(end);
                else
                    EndPos = NonContiguous(1)+1;
                end
                % Shift Up
                FileList = cellstr(get(listSelectImages,'String'));
                set(listSelectImages,'String',char(FileList([1:StartPos-2 StartPos:EndPos StartPos-1 EndPos+1:end],:)), 'Value',(StartPos:EndPos)-1);
            end
        end
        
    case 'ShiftDown'
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        ToShift = get(listSelectImages,'Value');
        if ~isempty(ToShift)
            FileList = cellstr(get(listSelectImages,'String'));
            FileLength = size(FileList,1);
            if ToShift(end)<FileLength		% Can shift down then
                % Keep first contiguous block
                NonContiguous = find(diff(ToShift)~=1);
                StartPos = ToShift(1);
                if isempty(NonContiguous)	% Just 1 contiguous block
                    EndPos = ToShift(end);
                else
                    EndPos = NonContiguous(1)+1;
                end
                % Shift down
                set(listSelectImages,'String',char(FileList([1:StartPos-1 EndPos+1 StartPos:EndPos EndPos+2:end],:)), 'Value',(StartPos:EndPos)+1);
            end
        end
        
    case 'InverseList'
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        FileList = cellstr(get(listSelectImages,'String'));
        set(listSelectImages, 'String', flipud(FileList), 'Value',[]);
        
    case 'OK'
        UserData = [];
        UserData = putInUserData('ImagesSelect', cellstr(get(findobj(gcf,'Tag','listSelectImages'),'String')), UserData );
        UserData = putInUserData('Flag', 1, UserData );
        
        %% Attention maintenance pour R2014b : ne pas suivre la recommendation du Matlab Graphics Updater
        %set(gcbf, 'Selected', 'On', 'UserData', UserData); % TODO : NE PAS supprimer le 'Selected','On'
        set(gcbf, 'UserData', UserData);
        uiresume(gcbf);
        
    case 'Cancel'
        UserData = [];
        UserData = putInUserData('Flag', 0, UserData );
        set(gcbf, 'UserData', UserData);
        uiresume(gcbf);
end


function hFig = LocalInit(ExtImages, Intitule)

hFig = figure(...
    'Units','normalized',...
    'Color',[0.925490196078431 0.913725490196078 0.847058823529412],...
    'IntegerHandle','off',...
    'MenuBar','none',...
    'Name',Lang('S�lection de fichiers','Images Selection'),...
    'NumberTitle','off',...
    'Position',[0.00625 0.1181640625 0.703125 0.48828125],...
    'Resize','on',...
    'HandleVisibility','callback',...
    'Tag','figure1',...
    'CloseRequestFcn','uiOrdreItems(''Access'',''Cancel'')',...
    'UserData',[],...
    'Visible','on');

hPnlMain = uipanel(...
    'Parent',hFig,...
    'Units','normalized',...
    'Title','',...
    'Tag','pnlMain',...
    'Clipping','on',...
    'Position',[0.0177 0.026 0.956 0.948]);

uicontrol(...
    'Parent',hPnlMain,...
    'Units','normalized',...
    'Position',[0.02 0.16 0.90 0.7],...
    'String', ExtImages, ...
    'Style','listbox',...
    'Tag','listSelectImages',...
    'Max',2);

uicontrol(...
    'Parent', hPnlMain,...
    'Units', 'normalized',...
    'HorizontalAlignment', 'left',...
    'Position', [0.2 0.9 0.6 0.033],...
    'String', Intitule,...
    'FontWeight', 'bold',...
    'Style', 'text',...
    'Tag', 'labelSelectImages');

uipanel(...
    'Parent',hPnlMain,...
    'Units','normalized',...
    'Title','',...
    'Tag','hPnl2',...
    'Clipping','on',...
    'BackgroundColor',[0.753 0.753 0.753],...
    'Position',[0.026 0.115 0.96 0.0088]);

uicontrol(...
    'Parent',hPnlMain,...
    'Units','normalized',...
    'Position',[0.4 0.03 0.076 0.066],...
    'String','OK',...
    'Tag','btnOK',...
    'CallBack','uiOrdreItems(''Access'',''OK'')');

uicontrol(...
    'Parent',hPnlMain,...
    'Units','normalized',...
    'Position',[0.5 0.03 0.076 0.066], ...
    'String', Lang('TODO','Cancel'), ...
    'Tag','btnCancel',...
    'CallBack','uiOrdreItems(''Access'',''Cancel'')');

nomFic = getNomFicDatabase('MoveUp.jpg');
Icon = iconize(imread(nomFic));
Icon(Icon==255) = .8*255;
str1 = 'Remonter l''item s�lectionn� d''une palce vers le haut.';
str2 = 'Push up the selected item.';
uicontrol(...
    'Parent',hPnlMain,...
    'Units','normalized',...
    'FontSize',14,...
    'TooltipString', Lang(str1,str2), ...
    'Position',[0.93 0.5 0.056 0.066],...
    'Tag','btnShiftUp',...
    'CData',Icon, ...
    'CallBack','uiOrdreItems(''Access'',''ShiftUp'')');

nomFic = getNomFicDatabase('MoveDown.jpg');
Icon = iconize(imread(nomFic));
Icon(Icon==255) = .8*255;
str1 = 'Descendre l''items s�lectionn� d''une palce vers le bas.';
str2 = 'Push down the selected item.';
uicontrol(...
    'Parent',hPnlMain,...
    'Units','normalized',...
    'TooltipString', Lang(str1,str2), ...
    'Position',[0.93 0.4 0.056 0.066],...
    'Tag','btnShiftDown',...
    'CData',Icon, ...
    'CallBack','uiOrdreItems(''Access'',''ShiftDown'')');

nomFic = getNomFicDatabase('MoveDown.jpg');
Icon = imread(nomFic);
n = size(Icon, 1);
n2 = floor(n/2);
Icon(1:n2,:) = flipud(Icon(n2+1:end,:));
Icon = iconize(Icon);
Icon(Icon==255) = .8*255;
str1 = 'Inverser l''ordre de tous les items.';
str2 = 'Inverse the order of all items.';
uicontrol(...
    'Parent',hPnlMain,...
    'Units','normalized',...
    'TooltipString', Lang(str1,str2), ...
    'Position',[0.93 0.3 0.056 0.066],...
    'Tag','btnInverseList',...
    'CData',Icon, ...
    'CallBack','uiOrdreItems(''Access'',''InverseList'')');


% -------------------------------------------------------------------------
% Fonction iconize pour int�grer un fichier type jpg en icone.
function outIcon = iconize(a)

% Find the size of the acquired image and determine how much data will need
% to be lost in order to form a 18x18 icon
[r,c,d] = size(a); %#ok
r_skip = ceil(r/18);
c_skip = ceil(c/18);

% Create the 18x18 icon (RGB data)
outIcon = a(1:r_skip:end,1:c_skip:end,:);
if ndims(outIcon) == 2 %#ok<ISMAT>
    outIcon(:,:,2) = outIcon(:,:,1);
    outIcon(:,:,3) = outIcon(:,:,1);
end
