% Methode de gestion des callback du composant
%
% Syntax
%   [flag, ListeOut, Ordre] = uiOrdreItemsPlus(ListeIn)
%
% Input Arguments
%   ListeIn : Liste des Items
%
% Output Arguments
%   flag     : Validation (1 ou 0)
%   ListeOut : Liste des fichiers
%   Ordre    : Ordre en sortie
%
% EXAMPLE :
%   [flag, ListeOut, Ordre] = uiOrdreItemsPlus({'A';'B';'C';'D'})
%
% See also uiOrdreItems
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, ListeOut, Ordre] = uiOrdreItemsPlus(ListeIn, varargin)

[varargin, Intitule] = getPropertyValue(varargin, 'Intitule', Lang('Ordre des fichiers sélectionnés', 'Order of the selected Images')); %#ok<ASGLU>

[flag, ListeOut] = uiOrdreItems('ListeIn', ListeIn, 'Intitule', Intitule);
if ~flag
    ListeOut = [];
    Ordre = [];
    return
end

N = length(ListeIn);
for i=1:N
    for k=1:N
        if strcmp(ListeIn{i}, ListeOut{k})
            Ordre(k) = i; %#ok<AGROW>
        end
    end
end
