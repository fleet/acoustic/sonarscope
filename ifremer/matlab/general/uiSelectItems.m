% M�thode de gestion des callback du composant
%
% Syntax
%   [flag, ImagesSelect, ItemsSelect] = uiSelectItems(...)
%
% Name-Value Pair Arguments
%   uiAccess      : ????
%   ItemsToSelect : filtre sur les extensions d'image
%   Titre         : label de titre
%   InitValue     : positionne le focus sur cet indice.
%
% Output Arguments
%   Flag         : Validation (1 ou 0)
%   ImagesSelect : Liste des fichiers
%   ItemsSelect  :
%
% EXAMPLE :
%   [flag, ImagesSelect, ItemsSelect] = uiSelectItems('ItemsToSelect', {'A';'B';'C';'D'})
%   [flag, ImagesSelect, ItemsSelect] = uiSelectItems('ItemsToSelect', {'A';'B';'C';'D'}, 'InitValue', [2 4])
%   [flag, ImagesSelect, ItemsSelect] = uiSelectItems('ItemsToSelect', {'A';'B';'C';'D'}, 'InitValue', [2 4], 'ItemsOnRightPanel', [2 4])
%
% See also
% Authors : GLU, JMA
% ----------------------------------------------------------------------------

% TODO : g�n�raliset le getappdata comme pour handle_listSelectImages et handle_listOrigImages

function [Flag, ImagesSelect, ItemsSelect] = uiSelectItems(varargin)

    function pushItemsOnTheRight(NewSelection)
        GCF = gcf;
        handle_listSelectImages = getappdata(GCF, 'handle_listSelectImages');
        handle_listOrigImages   = getappdata(GCF, 'handle_listOrigImages');
        if ~isempty(NewSelection)
            FileList = get(handle_listOrigImages, 'String');
            FullImages = FileList(NewSelection);
            
            % Option 'rows' non fonctionnelle sur des cha�nes de caract�res
            % de taille diff�rentes.
            
            newList = [get(handle_listSelectImages, 'String'); FullImages];
            foo = unique(newList);
            if length(foo) ~= length(newList)
                return
            end
            set(handle_listSelectImages, 'String', newList, 'Value', []);
        end
    end

Flag         = 0;
ImagesSelect = [];
ItemsSelect  = [];

% Feel free to modify this file according to your needs !
% Fabrice.Pabois@ncl.ac.uk - University of Newcastle - 2000

[varargin, uiAccess]          = getPropertyValue(varargin, 'Access',            'Start');
[varargin, ExtImages]         = getPropertyValue(varargin, 'ItemsToSelect',     {});
[varargin, Titre]             = getPropertyValue(varargin, 'Titre',             []);
[varargin, InitValue]         = getPropertyValue(varargin, 'InitValue',         1);
[varargin, ItemNames]         = getPropertyValue(varargin, 'ItemNames',         'Images');
[varargin, ItemsOnRightPanel] = getPropertyValue(varargin, 'ItemsOnRightPanel', []); %#ok<ASGLU>

if ischar(ExtImages)
    ExtImages = {ExtImages};
end

switch uiAccess
    case 'Start'
        if isempty(Titre)
            Titre = Lang('S�lection des Items', 'Items Selection');
        end
        h = LocalInit(ExtImages, Titre, InitValue, ItemNames);
        
        %         pushItemsOnTheRight(2)
        for k=1:length(ItemsOnRightPanel)
            pushItemsOnTheRight(ItemsOnRightPanel(k))
        end
        
        uiwait(h); % waitfor(h, 'Selected', 'on');
        x = get(h, 'UserData');
        ImagesSelect = getFromUserData('ImagesSelect', x);
        ItemsSelect = [];
        for i=1:length(ImagesSelect)
            for k=1:length(ExtImages)
                if strcmp(ImagesSelect{i}, ExtImages{k})
                    ItemsSelect(i) = k; %#ok<AGROW>
                end
            end
        end
        Flag = getFromUserData('Flag', x);
        delete(h);
        
    case 'AddFile'
        listOrigImages = findobj(gcbf, 'Tag', 'listOrigImages');
        NewSelection = get(listOrigImages, 'Value');
        pushItemsOnTheRight(NewSelection)
        
    case 'AddAllImages'
        listOrigImages = findobj(gcbf, 'Tag', 'listOrigImages');
        FileList = get(listOrigImages, 'String');
        FullImages = FileList(:);
        
        listSelectImages = findobj(gcbf, 'Tag', 'listSelectImages');
        % Option 'rows' non fonctionnelle sur des cha�nes de caract�res
        % de taille diff�rentes.
        
%         AlreadyHere = get(listSelectImages, 'String');
%         if isempty(AlreadyHere)
            newList = FullImages;
%         else
%             [~,ia] = setdiff(FullImages, AlreadyHere);
%             newList = [AlreadyHere; FullImages(ia)];
%         end

        set(listSelectImages, 'String', newList, 'Value', []);
        
    case 'RemFile'
        listSelectImages = findobj(gcbf, 'Tag', 'listSelectImages');
        ToRemove = get(listSelectImages,'Value');
        if any(ToRemove)
            FileList = cellstr(get(listSelectImages, 'String'));
            FileList(ToRemove)=[];
            set(listSelectImages, 'String', FileList, 'Value',[]);
        end
        
    case 'RemAllImages'
        listSelectImages = findobj(gcbf, 'Tag', 'listSelectImages');
        FileList = cellstr(get(listSelectImages, 'String'));
        FileList(:) = [];
        set(listSelectImages, 'String', FileList, 'Value',[]);
        
    case 'ShiftUp'
        listSelectImages = findobj(gcbf, 'Tag', 'listSelectImages');
        ToShift = get(listSelectImages, 'Value');
        if ~isempty(ToShift)
            if ToShift(1) > 1		% Can shift up then
                % Keep first contiguous block
                NonContiguous = find(diff(ToShift)~=1);
                StartPos = ToShift(1);
                if isempty(NonContiguous)	% Just 1 contiguous block
                    EndPos = ToShift(end);
                else
                    EndPos = NonContiguous(1) + 1;
                end
                % Shift Up
                FileList = cellstr(get(listSelectImages, 'String'));
                set(listSelectImages,'String',char(FileList([1:StartPos-2 StartPos:EndPos StartPos-1 EndPos+1:end],:)), 'Value',(StartPos:EndPos)-1);
            end
        end
        
    case 'ShiftTop'
        listSelectImages = findobj(gcbf, 'Tag', 'listSelectImages');
        ToShift = get(listSelectImages,'Value');
        if ~isempty(ToShift)
            if ToShift(1)>1		% Can shift up then
                % Keep first contiguous block
                NonContiguous = find(diff(ToShift)~=1);
                StartPos = ToShift(1);
                if isempty(NonContiguous)	% Just 1 contiguous block
                    EndPos = ToShift(end);
                else
                    EndPos = NonContiguous(1)+1;
                end
                % Shift Top
                FileList = cellstr(get(listSelectImages,'String'));
                set(listSelectImages,'String',char(FileList([StartPos:EndPos 1:StartPos-1 EndPos+1:numel(FileList)],:)), 'Value',[StartPos:EndPos 1:StartPos-1 EndPos+1:numel(FileList)]);
            end
        end
        
    case 'ShiftBottom'
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        ToShift = get(listSelectImages,'Value');
        if ~isempty(ToShift)
            FileList = cellstr(get(listSelectImages,'String'));
            FileLength = size(FileList,1);
            if ToShift(end)<FileLength		% Can shift down then
                % Keep first contiguous block
                NonContiguous = find(diff(ToShift)~=1);
                StartPos = ToShift(1);
                if isempty(NonContiguous)	% Just 1 contiguous block
                    EndPos = ToShift(end);
                else
                    EndPos = NonContiguous(1)+1;
                end
                % Shift Bottom
                set(listSelectImages,'String',char(FileList([1:StartPos-1 EndPos+1:numel(FileList) StartPos:EndPos],:)), 'Value',[1:StartPos-1 EndPos+1:numel(FileList) StartPos:EndPos]);
            end
        end
        
    case 'ShiftDown'
        listSelectImages = findobj(gcbf,'Tag','listSelectImages');
        ToShift = get(listSelectImages,'Value');
        if ~isempty(ToShift)
            FileList = cellstr(get(listSelectImages,'String'));
            FileLength = size(FileList,1);
            if ToShift(end)<FileLength		% Can shift down then
                % Keep first contiguous block
                NonContiguous = find(diff(ToShift)~=1);
                StartPos = ToShift(1);
                if isempty(NonContiguous)	% Just 1 contiguous block
                    EndPos = ToShift(end);
                else
                    EndPos = NonContiguous(1)+1;
                end
                % Shift down
                set(listSelectImages,'String',char(FileList([1:StartPos-1 EndPos+1 StartPos:EndPos EndPos+2:end],:)), 'Value',(StartPos:EndPos)+1);
            end
        end
        
    case 'OK'
        UserData = [];
        UserData = putInUserData( 'ImagesSelect', cellstr(get(findobj(gcf,'Tag','listSelectImages'),'String')), UserData );
        UserData = putInUserData( 'Flag', 1, UserData );
        
        %% Attention maintenance pour R2014b : ne pas suivre la recommendation du Matlab Graphics Updater
        % set(gcbf, 'Selected','On', 'UserData', UserData); % TODO : NE PAS supprimer le 'Selected','On'
        set(gcbf, 'UserData', UserData);
        uiresume(gcbf);
        
    case 'Cancel'
        UserData = [];
        UserData = putInUserData('Flag', 0, UserData );
        % set(gcbf, 'Selected','On', 'UserData', UserData);
        set(gcbf, 'UserData', UserData);
        uiresume(gcbf)
        listOrigImages = get(findobj(gcf, 'Tag', 'listOrigImages'), 'String');
end

    function hFig = LocalInit(ExtImages, Titre, InitValue, ItemNames)
        
        hFig = figure(...
            'Units', 'normalized',...
            'Color', [0.925490196078431 0.913725490196078 0.847058823529412],...
            'IntegerHandle', 'off',...
            'MenuBar', 'none',...
            'Name', Titre, ...
            'NumberTitle', 'off',...
            'Position', [0.00625 0.1181640625 0.703125 0.48828125],...
            'Resize', 'on',...
            'Tag', 'figure1',...
            'CloseRequestFcn', 'uiSelectItems(''Access'',''Cancel'')',...
            'UserData', [],...
            'Visible', 'on');
        % 'HandleVisibility', 'callback', ...
        
        
        hPnlMain = uipanel(...
            'Parent',hFig,...
            'Units','normalized',...
            'Title','',...
            'Tag','pnlMain',...
            'Clipping','on',...
            'Position',[0.0177 0.026 0.956 0.948]);
        
        handle_listOrigImages = uicontrol(...
            'Parent', hPnlMain,...
            'Units', 'normalized',...
            'Position', [0.03 0.16 0.4 0.7],...
            'Style', 'listbox',...
            'Tag', 'listOrigImages', ...
            'Max', 2, ...
            'String', ExtImages, 'Value', InitValue);
        
        uicontrol(...
            'Parent', hPnlMain,...
            'Units', 'normalized',...
            'HorizontalAlignment', 'left',...
            'Position', [0.03 0.9 0.1 0.033],...
            'String', Lang('Fichiers', ItemNames),...
            'FontWeight', 'bold',...
            'Style', 'text',...
            'Tag', 'labelOrigImages',...
            'Max', 2);
        
        handle_listSelectImages = uicontrol(...
            'Parent', hPnlMain,...
            'Units', 'normalized',...
            'Position', [0.55 0.16 0.36 0.7],...
            'String', {},...
            'Style', 'listbox',...
            'Tag', 'listSelectImages',...
            'Max', 2);
        
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'HorizontalAlignment','left',...
            'Position',[0.55 0.9 0.1 0.033],...
            'String',Lang('Fichiers s�lectionn�s', ['Selected ' ItemNames]),...
            'FontWeight','bold',...
            'Style','text',...
            'Tag','labelSelectImages');
        
        uipanel(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Title','',...
            'Tag','hPnl2',...
            'Clipping','on',...
            'BackgroundColor',[0.753 0.753 0.753],...
            'Position',[0.026 0.115 0.96 0.0088]);
        
        nomFic = getNomFicDatabase('MoveRight.jpg');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Position',[0.45 0.6 0.076 0.066],...
            'Tag','btnSelect',...
            'CData',Icon,...
            'CallBack','uiSelectItems(''Access'',''AddFile'')');
        
        nomFic = getNomFicDatabase('MoveLeft.jpg');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Position',[0.45 0.5 0.076 0.066],...
            'Tag','btnDeselect',...
            'CData',Icon, ...
            'CallBack','uiSelectItems(''Access'',''RemFile'')');
        
        nomFic = getNomFicDatabase('MoveRightAll.jpg');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Position',[0.45 0.4 0.076 0.066],...
            'Tag','btnAllSelect',...
            'CData',Icon, ...
            'CallBack','uiSelectItems(''Access'',''AddAllImages'')');
        
        nomFic = getNomFicDatabase('MoveLeftAll.jpg');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Position',[0.45 0.3 0.076 0.066],...
            'Tag','btnAllDeselect',...
            'CData',Icon, ...
            'CallBack','uiSelectItems(''Access'',''RemAllImages'')');
        
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Position',[0.4 0.03 0.076 0.066],...
            'String','OK',...
            'Tag','btnOK',...
            'CallBack','uiSelectItems(''Access'',''OK'')');
        
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Position',[0.5 0.03 0.076 0.066],...
            'String', Lang('TODO','Cancel'), ...
            'Tag','btnCancel',...
            'CallBack','uiSelectItems(''Access'',''Cancel'')');
        
        nomFic = getNomFicDatabase('MoveUp.jpg');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'FontSize',14,...
            'Position',[0.93 0.5 0.056 0.066],...
            'Tag','btnShiftUp',...
            'CData',Icon, ...
            'CallBack','uiSelectItems(''Access'',''ShiftUp'')');
        
        nomFic = getNomFicDatabase('MoveDown.jpg');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'Position',[0.93 0.4 0.056 0.066],...
            'Tag','btnShiftDown',...
            'CData',Icon, ...
            'CallBack','uiSelectItems(''Access'',''ShiftDown'')');
        
        nomFic = getNomFicDatabase('MoveTop.tif');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent',hPnlMain,...
            'Units','normalized',...
            'FontSize',14,...
            'Position',[0.93 0.6 0.056 0.066],...
            'Tag','btnShiftUp',...
            'CData',Icon, ...
            'CallBack','uiSelectItems(''Access'',''ShiftTop'')');
        
        nomFic = getNomFicDatabase('MoveBottom.tif');
        Icon = iconize(imread(nomFic));
        Icon(Icon==255) = .8*255;
        uicontrol(...
            'Parent', hPnlMain,...
            'Units', 'normalized',...
            'FontSize', 14,...
            'Position', [0.93 0.3 0.056 0.066],...
            'Tag', 'btnShiftUp',...
            'CData', Icon, ...
            'CallBack', 'uiSelectItems(''Access'',''ShiftBottom'')');
        
        setappdata(hFig, 'handle_listSelectImages', handle_listSelectImages)
        setappdata(hFig, 'handle_listOrigImages',   handle_listOrigImages)
        
    end

end
