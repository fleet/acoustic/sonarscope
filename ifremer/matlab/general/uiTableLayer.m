% S�lection des layers � consid�rer comme index dans l'export de Multidata.
%
% Syntax
%   [flag, S1, S2] = uiTableLayer(listLayerObligatoire, listLayerIndex)
% 
% Input Arguments
%   TODO
%
% Name-only Arguments
%   TODO 
%
% Output Arguments
%   selectLayers : Layer s�lectionn�s.
%   selectIndex  : Layer indiqu�s comme Index.
%
% Examples
%     clear list1
%     list1(1) = struct(  'type', 'Reflectivity', ...
%         'name', 'Reflectivity_0001_12032013', ...
%         'Index', 0);
%     list1(2) = struct(  'type', 'ReflectivityFromSnippets', ...
%         'name', 'ReflectivityFromSnippets_0001_12032013', ...
%         'Index', 0);
%     list1(3) = struct(  'type', 'Bathymetry', ...
%         'name', 'Bathymetry_0001_12032013', ...
%         'Index', 0);
%     list1(4) = struct(  'type', 'TxAngle', ...
%         'name', 'TxAngle_0001_12032013', ...
%         'Index', 1);
%   [flag, selectLayer] = uiTableLayer(list1)
%
% See also tableDatagram  Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, selectLayer] = uiTableLayer(listLayer)

flag        = 0;
selectLayer = [];
str1 = 'S�lectionner les layers que vous voulez pointer en index';
str2 = 'Select Layers you want to consider as index';
hdlFig   = figure('Position',[300 300 1010 400], 'Menubar', 'none',...
    'Units', 'normalized', ...
    'Name', Lang(str1,str2), ...
    'DeleteFcn', @cb_DeleteUiTable);

% Layers pressentis
data = [];
for k=1:size(listLayer,2)
    data = [data;{listLayer(k).type, listLayer(k).name, logical(listLayer(k).Index)}, true]; %#ok<AGROW>
end

% Appel via la Toolbox uiExtras de sous-panneaux :
% voir Contributions\GUIayoutToolbox
vBox = uiextras.VBox('Parent', hdlFig);

% D�finition des Sous-panneaux container
hPnlHaut   = uipanel('Parent', vBox, 'Title','');
hPnlMedium = uipanel('Parent', vBox, 'Title','');
hPnlBas    = uipanel('Parent', vBox, 'Title','');
          
set(vBox, 'Sizes', [-0.7 -0.15 -0.15], 'Spacing', 2);          

% Cr�ation du container de la liste de Layers.
colName   = {'Type', 'Name', Lang('Layer Index','Index Layer'), Lang('S�lection','Selected')};
colFormat = {'char', 'char', 'logical', 'logical'};
colEdit   = [false false true true];

colSize   = {200, 400, 200, 200};
hTabLayer = uitable('Parent',         hPnlHaut, ...
                    'Units',          'normalized', ...
                    'Tag',            'uiTabLayer', ...
                    'Position',       [0.0 0.0 1.0 1.0], ...
                    'Data',           data,...
                    'ColumnName',     colName,...
                    'ColumnFormat',   colFormat,...
                    'ColumnEditable', colEdit,...
                    'RowName',        [], ...
                    'ColumnWidth',    colSize);
                        
hBtnSelect = uicontrol('parent',   hPnlMedium, ...
                       'tag',      'btnCheckAll', ...
                       'Style',    'pushbutton', ...
                       'Units',    'normalized', ...
                       'String',   'Unselect All Layers', ...
                       'Position', [0.75 0.025 0.25 0.95], ...
                       'Callback', @cbBtn_selectAllLayers); %#ok<NASGU>

hBtnOK     = uicontrol('parent',   hPnlBas, ...
                       'tag',      'btnOK', ...
                       'Style',    'pushbutton',...
                       'Units',    'normalized', ...
                       'String',   'OK',...
                       'Position', [0 0 0.5 1],...
                       'Callback', @cbBtn_OK); %#ok<NASGU>

hBtnCancel  = uicontrol('parent',   hPnlBas, ...
                        'tag',      'btnCancel', ...
                        'Style',    'pushbutton',...
                        'Units',    'normalized', ...
                        'String',   'Cancel',...
                        'Position', [0.5 0 0.5 1],...
                        'Callback', @cbBtn_Cancel); %#ok<NASGU>

% Make the GUI blocking
uiwait(hdlFig);
if ~isempty(selectLayer)
    flag = 1;
end


%% Sous-fonctions

    function cb_DeleteUiTable(src, evt) %#ok<INUSD>
        hTabLayer    = findobj('tag', 'uiTabLayer');
        selectLayer  = get(hTabLayer,'Data');
    end

    function cbBtn_OK(src, evt) %#ok<INUSD>
        hTabLayer    = findobj('tag', 'uiTabLayer');
        selectLayer  = get(hTabLayer,'Data');
        % Fermeture de la fen�tre.
        hFig = ancestor(src,'figure','toplevel');
        my_close(hFig);
    end

    function cbBtn_Cancel(src, evt) %#ok<INUSD>
        % S�lection de tous les Layermes.
        hTabLayer = findobj('tag', 'uiTabLayer');
        set(hTabLayer,'Data', []);      
        % Fermeture de la fen�tre.
        hFig = ancestor(src,'figure','toplevel');
        my_close(hFig);
    end

    function cbBtn_selectAllLayers(src, evt) %#ok<INUSD>
        
        % S�lection de tous les Layermes.
        hTabLayer = findobj('tag', 'uiTabLayer');
        pppp      = get(hTabLayer,'Data');
        
        strBtn = get(src, 'String');
        if strcmp(strBtn, 'Select All Layers')
            set(src, 'String', 'Uncheck All Layers');
            for k2=1:size(pppp,1)
                pppp{k2,end} = true;
            end
        else
            set(src, 'String', 'Select All Layers');
            for k2=1:size(pppp,1)
                pppp{k2,end} = false;
            end
        end
        set(hTabLayer,'Data', pppp);
        selectLayer  = get(handle(hTabLayer),'Data');
    end
end