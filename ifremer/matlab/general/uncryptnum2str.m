function y = uncryptnum2str(x, i, j)
for k=1:length(x)
    y(k) = (x(k) - (-i)^3)/2 - j; %#ok<AGROW>
end
y = char(y);
