function unzipSonarScopeCache(nomDir)

listeFicZip = listeFicOnDir(nomDir,'.zip');
for k=1:length(listeFicZip)
    fprintf('Unzipping %s.\n', listeFicZip{k});
    try
        unzip(listeFicZip{k}, nomDir)
        delete(listeFicZip{k});
    catch %#ok<CTCH>
    end
end
