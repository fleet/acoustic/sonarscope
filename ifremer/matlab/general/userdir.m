% User dir
%
% Syntax
%   str = userdir
%
% Output Arguments
%   str : User name
%
% Examples
%   userdir
%
% See also userdir Authors
% Authors  : JMA + AG
%-------------------------------------------------------------------------------

function str = userdir
str = char(java.lang.System.getProperty('user.dir'));