% User domain
%
% Syntax
%   str = userdomain
%
% Output Arguments
%   str : User domain
%
% Examples
%   username
%
% See also userhome username Authors
% Authors  : JMA + AG
%-------------------------------------------------------------------------------

function str = userdomain
str = getenv('UserDomain');