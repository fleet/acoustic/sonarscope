% User home
%
% Syntax
%   str = userhome
%
% Output Arguments
%   str : User name
%
% Examples
%   userhome
%
% See also userhome Authors
% Authors  : JMA + AG
%-------------------------------------------------------------------------------

function str = userhome
str = char(java.lang.System.getProperty('user.home'));
