% User name
%
% Syntax
%   str = username
%
% Output Arguments
%   str : User name
%
% Examples
%   username
%
% See also userhome Authors
% Authors  : JMA + AG
%-------------------------------------------------------------------------------

function str = username
str = char(java.lang.System.getProperty('user.name'));
