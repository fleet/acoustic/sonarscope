% Verification de la longueur d'un nom de fichier sous windows.
% Des probl�mes ont �t� rencontr�s au CIDCO avec des noms modifi�s par
% windows (des ~1 � la plade des "Local Settings", ...
%
% Syntax
%   verifNameLength(nomFic)
%
% INTPUT PARAMETERS :
%   nomFic : Nom de fichiezr � v�rifier
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   verifNameLength(nomFic)
%
% See also getNomFicDatabase Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function verifNameLength(nomFic)

N = length(nomFic);
if N > namelengthmax
    str = sprintf('Le nom du fichier %s semble trop long (%d caract�res). Sur windows, il est conseill� de ne pas d�passer %d caract�res. Demandez conseil � sonarscope@ifremer.fr', nomFic, N, namelengthmax);
    my_warndlg(str, 0);
end
