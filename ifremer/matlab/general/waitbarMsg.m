% Create a message for my_waitbar
%
% Syntax
%   str = waitbarMsg
%   str = waitbarMsg(ProcessingName)
%
% Name-Value Pair Arguments 
%   ProcessingName : Name of the processing
%
% Output Arguments
%   str : Message
%
% Examples
%   waitbarMsg
%   waitbarMsg('sqrt')
%
%   N = 1000;
%   hw = create_waitbar(waitbarMsg('Hello World'), 'N', N);
%   for k=1:N
%       my_waitbar(k, N, hw)
%       pause(0.01)
%   end
%   my_close(hw)
%
% See also cl_image/sqrt Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = waitbarMsg(varargin)

if nargin == 0
    str1 = 'Traitement en cours';
    str2 = 'Processing';
else
    Name = varargin{1};
    str1 = sprintf('Traitement "%s" en cours', Name);
    str2 = sprintf('Processing "%s"', Name);
end
str = Lang(str1,str2);
