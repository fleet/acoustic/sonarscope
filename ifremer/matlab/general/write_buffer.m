% Ecriture dans un buffer � la mani�re d'un fwrite
%
% Syntax
%   [tempo, adr] = write_buffer(x, tempo, adr, flagEndian, precision)
%
% INTPUT PARAMETERS :
%   x          : Valeur
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%   flagEndian : 1 si Little endian, 0 sinon
%   precision  : 'uint8', 'int8', 'uint16', 'int16', 'uint32' ou 'uint32'
%
% Output Arguments
%   tempo : Tableau contenant les octets
%   adr   : Offset remis � jour
%
% Examples 
%   [str, maxsize, endian]  = computer
%   flagEndian = strcmp(endian, 'L')
%
%   tempo = 200
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'uint8')
%   [tempo, adr] = write_buffer(x, tempo, 0, flagEndian, 'uint8')
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'int8')
%   [tempo, adr] = write_buffer(x, tempo, 0, flagEndian, 'int8')
%
%   tempo = [130 11]
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'uint16')
%   [tempo, adr] = write_buffer(x, tempo, 0, flagEndian, 'uint16')
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'int16')
%   [tempo, adr] = write_buffer(x, tempo, 0, flagEndian, 'int16')
%
%   tempo = [130 0 3 4]
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'uint32')
%   [tempo, adr] = write_buffer(x, tempo, 0, flagEndian, 'uint32')
%   [x, adr] = read_buffer(tempo, 0, flagEndian, 'int32')
%   [tempo, adr] = write_buffer(x, tempo, 0, flagEndian, 'int32')
%
% See also write_uint* write_int* read_buffer read_uint* read_int* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [tempo, adr] = write_buffer(x, tempo, adr, flagEndian, precision)

switch precision
    case 'uint8'
        [tempo, adr] = write_uint8( x, tempo, adr);
    case 'int8'
        [tempo, adr] = write_int8(  x, tempo, adr);
    case 'uint16'
        [tempo, adr] = write_uint16(x, tempo, adr, flagEndian);
    case 'int16'
        [tempo, adr] = write_int16( x, tempo, adr, flagEndian);
    case 'uint32'
        [tempo, adr] = write_uint32(x, tempo, adr, flagEndian);
    case 'int32'
        [tempo, adr] = write_int32( x, tempo, adr, flagEndian);
    otherwise
end
