% Ecriture d'un uint8 dans un buffer � la mani�re d'un fwrite. Ceci �tait
% un test r�alis� dans test_flush_depth mais �a ne marche pas
%
% Syntax
%   [tempo, adr] = write_int8(x, tempo, adr)
%
% INTPUT PARAMETERS :
%   x          : Valeur
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%
% Output Arguments
%   tempo : Tableau contenant les octets
%   adr   : Offset remis � jour
%
% Examples 
%   tempo = 200
%   [x, adr] = read_int8(tempo, 0)
%   [tempo, adr] = write_int8(x, tempo, 0)
%
% See also write_buffer read_buffer write_uint* write_int* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [tempo, adr] = write_int8(x, tempo, adr)
adr = adr + 1;
x = max(-128, min(x, 127));
if x >= 0
    tempo(adr) = uint8(x);
else
    tempo(adr) = uint8(256 + x);
end
