% Ecriture d'un uint16 dans un buffer � la mani�re d'un fwrite. Ceci �tait
% un test r�alis� dans test_flush_depth mais �a ne marche pas
%
% Syntax
%   [tempo, adr] = write_uint16(x, tempo, adr, flagEndian, precision)
%
% INTPUT PARAMETERS :
%   x          : Valeur
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%   flagEndian : 1 si Little endian, 0 sinon
%
% Output Arguments
%   tempo : Tableau contenant les octets
%   adr   : Offset remis � jour
%
% Examples 
%   [str, maxsize, endian]  = computer
%   flagEndian = strcmp(endian, 'L')
%
%   tempo = [130 11]
%   [x, adr] = read_uint16(tempo, 0, flagEndian)
%   [tempo, adr] = write_uint16(x, tempo, 0, flagEndian)
%
% See also write_buffer read_buffer write_uint* write_int* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [tempo, adr] = write_uint16(x, tempo, adr, flagEndian)
% x = max(0, min(x, 65535));
x = uint16(x);
o1 = mod(x, 256);
o2 = (x-o1) / 256;
if flagEndian
    adr = adr + 1;
    tempo(adr) = uint8(o2);
    adr = adr + 1;
    tempo(adr) = uint8(o1);
else
    adr = adr + 1;
    tempo(adr) = uint8(o1);
    adr = adr + 1;
    tempo(adr) = uint8(o2);
end
