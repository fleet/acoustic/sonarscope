% Ecriture d'un uint8 dans un buffer � la mani�re d'un fwrite
%
% Syntax
%   [tempo, adr] = write_uint8(x, tempo, adr)
%
% INTPUT PARAMETERS :
%   x          : Valeur
%   tempo      : Tableau contenant les octets
%   adr        : Offset � partir du d�but (0=d�but)
%
% Output Arguments
%   tempo : Tableau contenant les octets
%   adr   : Offset remis � jour
%
% Examples
%   tempo = 200
%   [x, adr] = read_uint8(tempo, 0)
%   [tempo, adr] = write_uint8(x, tempo, 0)
%
% See also write_buffer read_buffer write_uint* write_int* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [tempo, adr] = write_uint8(x, tempo, adr)
% x = max(0, min(x, 255));
% x = uint8(x);
adr = adr + 1;
tempo(adr) = uint8(x);
