function flag = xml_check_signatures(Data, nomFic, Tag)

flag = 0;
if ~isfield(Data, 'Signature1')
    str1 = sprintf('Le fichier %s ne contient pas la balise "Signature1"', nomFic);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end
if ~isfield(Data, 'Signature2')
    str1 = sprintf('Le fichier %s ne contient pas la balise "Signature2"', nomFic);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end
if ~strcmp(Data.Signature1, 'SonarScope')
    str1 = sprintf('La balise "Signature1" du fichier %s n''est pas "SonarScope"', nomFic);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end
if ~strcmp(Data.Signature2, Tag)
    str1 = sprintf('La balise "Signature2" du fichier %s n''est pas "%s"', nomFic, Tag);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end

flag = 1;
