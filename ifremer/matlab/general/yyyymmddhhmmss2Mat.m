% Decode a date given as yyyymmddhhmmss.xxx
%
% Syntax
%   T = yyyymmddhhmmss2Mat(X)
%
% Input Arguments
%   X : a value or a string
%
% Output Arguments
%   a : Instance of cl_time
%
% Examples
%   T = yyyymmddhhmmss2Mat(20131122135957)
%   T = yyyymmddhhmmss2Mat(20131122135957.500)
%   T = yyyymmddhhmmss2Mat('20131122135957')
%   T = yyyymmddhhmmss2Mat('20131122135957.500')
%
% See also dayJma2Ifr hourHms2Ifr cl_time Authors
% Authors  : JMA
%--------------------------------------------------------------------------
 
function T = yyyymmddhhmmss2Mat(T)

if ischar(T)
    T = str2double(T);
end

Annee = floor(T / 1e10);
T = T - Annee*1e10;

Mois = floor(T / 1e8);
T = T - Mois*1e8;

Jour = floor(T / 1e6);
T = T - Jour*1e6;

Heure = floor(T / 1e4);
T = T - Heure*1e4;

Min = floor(T / 1e2);
T = T - Min*1e2;

Sec = T;

date = dayJma2Ifr(Jour, Mois, Annee);
heure = hourHms2Ifr(Heure, Min, Sec);
T = cl_time('timeIfr', date, heure);
