function flag = zipAssociatedDirectotyToXmlFile(nomFicXml)

if ischar(nomFicXml)
    nomFicXml = {nomFicXml};
end

N = length(nomFicXml);
str1 = 'Compression des r�pertoires associ�s � des fichiers XML';
str2 = 'Zipping directories associated to XML files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)

    [nomDir, nomFic] = fileparts(nomFicXml{k});
    nomDir = fullfile(nomDir, nomFic);
    flag = zipDirectory(nomDir);
    if ~flag
%         break
    end
end
my_close(hw, 'MsgEnd');
