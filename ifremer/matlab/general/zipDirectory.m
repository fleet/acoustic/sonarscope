function flag = zipDirectory(nomDir)

if ~exist(nomDir, 'dir')
    flag = 1;
    return
end

nomFicZip = [nomDir, '.zip'];
if ~exist(nomDir, 'dir') && exist(nomFicZip, 'file')
    str1 = sprintf('Le r�pertoire "%s" est d�j� compress�.', nomDir);
    str2 = sprintf('Directory "%s is already zipped".', nomDir);
    mes = Lang(str1,str2);
    fprintf('%s\n', mes);
    flag = 1;
    return
end

if exist(nomDir, 'dir') && exist(nomFicZip, 'file')
%     delete(nomFicZip)
    str1 = sprintf('Le r�pertoire "%s" existe toujours sur disque bien que le fichier zipp� "%s" existe. Ceci ne devrait pas arriver. Je ne fais rien dans ces circonstances.', nomDir, nomFicZip);
    str2 = sprintf('Folder "%s" still exists although the zipped file "%s" exists on the directory. This should not happend. I do not do anything in theses circumstances.', nomDir, nomFicZip);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ZipDirectoryFolderStillExists', 'TimeDelay', 60);
    flag = 1;
    return
end

try
    str1 = sprintf('Compression du r�pertoire "%s".', nomDir);
    str2 = sprintf('Zipping directory "%s".', nomDir);
    mes = Lang(str1,str2);
    fprintf('%s\n', mes);
    WorkInProgress(mes)
    listZippedFiles = zip(nomFicZip, nomDir);
    if isempty(listZippedFiles)
        str1 = sprintf('Le r�pertoire "%s" n''a pas pu �tre zipp�. On conserve le r�pertoire en entier, tout va bien.', nomDir);
        str2 = sprintf('Directory "%s" could not be zipped. The unzipped directory will be used, do not panic.', nomDir);
        my_warndlg(Lang(str1,str2), 0);
    end
    flag = rmdir(nomDir, 's');
    if ~flag
        str1 = sprintf('Le r�pertoire "%s" n''a pas pu �tre supprim�. Je vous conseille de le faire manuellement.', nomDir);
        str2 = sprintf('Directory "%s" could not be suppressed. I recommand you delete it by hand.', nomDir);
        my_warndlg(Lang(str1,str2), 0);
        return
    end
catch ME
    flag = 0;
    ErrorReport = getReport(ME) %#ok<NOPRT>
    fprintf('%s\n', ErrorReport);
    if ~strcmp(ME.identifier, 'MATLAB:zip:noEntries')
        return
    end
end
