function zipSonarScopeCache(nomFicALL)

if ischar(nomFicALL)
    nomFicALL = {nomFicALL};
end

nbFic = length(nomFicALL);
str1 = 'Compression des r�pertoires SonarScope.';
str2 = 'Zipping SonarScope directories.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw)
    [nomDir, nomFic] = fileparts(nomFicALL{k});
    nomDir = fullfile(nomDir, 'SonarScope', nomFic);
    if exist(nomDir, 'dir')
        listeFic = listeFicOnDir(nomDir,'.xml');
        for k2=1:length(listeFic)
            [nomDir2, nomFicXml] = fileparts(listeFic{k2});
            nomFicZip = fullfile(nomDir2, [nomFicXml '.zip']);
            nomDir2 = fullfile(nomDir2, nomFicXml);
            
            % TODO : faire appel � zipDirectory
            
            if exist(nomDir2, 'dir')
                try
                    fprintf('Zipping %s.\n', nomDir2);
                    zip(nomFicZip, nomDir2)
                    flag = rmdir(nomDir2, 's');
                    if ~flag
                        str1 = sprintf('Il est impossible de supprimer le r�pertoire "%s".', nomDir2);
                        str2 = sprintf('It is impossible to suppress directory "%s".', nomDir2);
                        my_warndlg(Lang(str1,str2), 0);
                    end
                catch ME
                    if strcmp(ME.identifier, 'MATLAB:zip:noEntries') % Cas o� le r�pertoire est vide
                        str1 = sprintf('Le r�pertoire "%s" est vide.', nomDir2);
                        str2 = sprintf('Directory "%s" is empty.', nomDir2);
%                         my_warndlg(Lang(str1,str2), 0);
                        fprintf('%s\n', Lang(str1,str2));
                    else
                        ErrorReport = getReport(ME) %#ok<NOPRT>
                        fprintf('%s\n', ErrorReport);
                        str1 = sprintf('Erreur lors du zippage du r�pertoire "%s".', nomDir2);
                        str2 = sprintf('Error when zipping directory "%s".', nomDir2);
                        my_warndlg(Lang(str1,str2), 1);
                        my_close(hw)
                        return
                    end
                end
            end
        end
    end
end
my_close(hw, 'MsgEnd')
