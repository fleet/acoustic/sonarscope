% Special Log10 
%
% Syntax
%   y = zlog10(x, Abs)
%
% Input Arguments
%   x   : Array of values
%   Abs : Boolean : false -> log10(x), true -> sign(x)*abs(log10(x)) (Default : 0)
%
% Output Arguments
%   y : Result
%
% Remarks : If Abs=0 negative values are set to NaN before computing log10
%
% Examples
%   x = single(Lena) - 128;
%   figure; imagesc(x); colormap(jet(256)); colorbar;
%
%   y = zlog10(x, 0);
%   figure; imagesc(y); colormap(jet(256)); colorbar;
%
%   y = zlog10(x, 1);
%   figure; imagesc(y); colormap(jet(256)); colorbar;
%
% See also cl_image/log10 Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function I = zlog10(I, Abs)

if Abs
    I = sign(I) .* abs(log10(I));
else
    I(I <= 0) = NaN;
    I = log10(I);
end