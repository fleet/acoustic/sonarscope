% Affectation des proprietes d'une instance cl_style a une courbe
%
% Syntax
%   apply(h, a)
%
% Input Arguments
%   h : Handle de la courbe
%   a : Instance de la classe
%
% Examples
%   h = plot(1:10);
%   a = cl_style('Color', [1 0 0], 'LineStyle', '--', 'LineWidth', 1, 'Marker', '*')
%   apply(h, a)
%
%   h = plot(rand(20,4))
%   a(1) = cl_style('Color', [1.00 0 0]);
%   a(2) = cl_style('Color', [0.75 0 0]);
%   a(3) = cl_style('Color', [0.50 0 0]);
%   a(4) = cl_style('Color', [0.25 0 0]);
%   apply(h, a)
%
% See also cl_style cl_style/plot Authors
% Authors : JMA
% VERSION  : $Id: apply.m,v 1.2 2003/04/07 08:51:39 augustin Exp $
% ----------------------------------------------------------------------------

function apply(h, this)

if length(this) == 1
    
    % ----------------------------------------
    % Memes proprietes pour toutes les courbes
    
    set(h, 'Color',     this.Color);
    set(h, 'LineStyle', this.LineStyle);
    set(h, 'LineWidth', this.LineWidth);
    set(h, 'Marker',    this.Marker);
else
    
    % ------------------------------------------------------
    % Proprietes differentes pour chaque courbe. Si il y a
    % plus de courbes que de proprietes, on revient au debut
    
    for i=1:length(h)
        k = 1 + mod(i-1, length(this));
        set(h(i), 'Color',     this(k).Color);
        set(h(i), 'LineStyle', this(k).LineStyle);
        set(h(i), 'LineWidth', this(k).LineWidth);
        set(h(i), 'Marker',    this(k).Marker);
    end
end
