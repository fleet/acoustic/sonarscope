% Representation externe d'instances cl_style dans une chaine de caracteres
%
% Syntax
%   s = char(a)
%
% Input Arguments
%   a : Instance de classe cl_style
%
% Output Arguments
%   s : Chaine de caracteres
%
% Examples
%   a = cl_style;
%   a(2) = cl_style('Color', 'm', 'LineStyle', '--', 'LineWidth', 1, 'Marker', '*');
%   s = char(a)
%   
% See also cl_style cl_style/char_instance cl_style/display Authors
% Authors : JMA
% VERSION  : $Id: char.m,v 1.2 2003/04/07 08:51:39 augustin Exp $
% ----------------------------------------------------------------------------

function str = char(this)

str = [];

% ------------------------------------
% Traitement tableau ou instance seule

[m, n] = size(this);

if (m*n) > 1
    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
            str1 = char(this(i, j));
            str{end+1} = str1;
        end
    end
    str = cell2str(str);
    
else
    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
