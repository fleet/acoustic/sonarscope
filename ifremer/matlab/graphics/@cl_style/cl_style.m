% Constructeur de la classe cl_style (Style a associer a des courbes)
%
% Syntax
%   a = cl_style(...)
%
% Name-Value Pair Arguments
%   Color     : 'r' | 'g' | {'b'} | 'k' | 'y' | 'm' | 'c' | [r g b]   'b' par defaut
%   LineStyle : {'-'} | '--' | ':' | '-.' | 'none'
%   LineWidth : 0.5 par defaut
%   Marker    : '+' | 'o' | '*' | '.' | 'x' | 's' | 'd' | 'v' | '^' | '>' | '<' |
%               'p' | 'h' | {'none'}
%    
% Output Arguments
%   a : une instance de cl_style
%
% Remarks : Les convention pour Color, LineStyle, LineWidth et Marker sont
%           celles de matlab : help plot
% 
% Examples
%   a = cl_style('Color', [1 0 0], 'LineWidth', 10)
%   h = plot(a, 1:10);
%
%   set(a, 'Color', 'm', 'LineStyle', '--', 'LineWidth', 1, 'Marker', '*')
%   apply(h, a)
%   
%   a(1) = cl_style('Color', [1.00 0 0]);
%   a(2) = cl_style('Color', [0.75 0 0]);
%   a(3) = cl_style('Color', [0.50 0 0]);
%   a(4) = cl_style('Color', [0.25 0 0]);
%   plot(a, rand(20,4))
%
% See also plot cl_cooc Authors
% Authors : JMA
% VERSION  : $Id: cl_style.m,v 1.2 2003/04/07 08:51:39 augustin Exp $
% ----------------------------------------------------------------------------

function this = cl_style(varargin)

this.Color      = [0 0 1];
this.LineStyle  = '-';
this.LineWidth  = 0.5;
this.Marker     = 'none';

% -------------------
% Creation de l'objet

this = class(this, 'cl_style') ;

% ----------------------------------------
% Test si construction sans initialisation

if (nargin == 1) && isempty(varargin{1})
    return
end

% -------------------------------------------------
% Passage des parametres transmis a la construction

this = set(this, varargin{:}) ;
