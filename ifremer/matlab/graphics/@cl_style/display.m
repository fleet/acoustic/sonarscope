% Representation externe d'instances cl_style sur le terminal
% C'est cette fonction aui est appeee quant on fait "a (return)"
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : Instances de la classe cl_style
%
% Examples
%   a = cl_style;
%   display(a)
%
%   a(2) = cl_style('Color', 'm', 'LineStyle', '--', 'LineWidth', 1, 'Marker', '*');
%   a
%
% See also cl_style/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
