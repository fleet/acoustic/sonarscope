% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(a, ...)
%
% Input Arguments
%   a : instance de cl_style
%
% Name-only Arguments
%   Color     : Couleur (red green blue) 
%   LineStyle : Style de la ligne
%   LineWidth : Epaisseur de la ligne
%   Marker    : Type de marqueur des points de la courbe
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
%
% Examples
%   a = cl_style
%   Color     = get(a, 'Color')
%   LineStyle = get(a, 'LineStyle')
%   LineWidth = get(a, 'LineWidth')
%   Marker    = get(a, 'Marker')
% 
%   [Color, LineStyle, LineWidth] = get(a, 'Color', 'LineStyle', 'LineWidth')
%
% See also cl_style cl_style/set Authors
% Authors : TLT
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)

    switch varargin{i}
        case 'Color'
            varargout{end+1} = this.Color; %#ok<AGROW>
        case 'LineStyle'
            varargout{end+1} = this.LineStyle; %#ok<AGROW>
        case 'LineWidth'
            varargout{end+1} = this.LineWidth; %#ok<AGROW>
        case 'Marker'
            varargout{end+1} = this.Marker; %#ok<AGROW>
        otherwise
            my_warndlg('cl_style/get : Invalid property name', 1);
    end
end
