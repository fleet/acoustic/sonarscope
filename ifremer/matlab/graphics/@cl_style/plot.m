% Trace d'une courbe avec les proprietes d'une instance cl_style
%
% Syntax
%   h = plot(a, Y, ...)
%   h = plot(a, X, Y, ...)
%
% Input Arguments
%   a : Instance de la classe
%   Y : Ordonnees de la courbe
%
% Name-Value Pair Arguments
%   X : Abscisses de la courbe
%   Parametres a transmettre a la fonction plot de Matlab
%
% Output Arguments
%   h : Handle de la courbe
%
% Examples
%   a = cl_style('Color', [1 0 0], 'LineStyle', '--', 'LineWidth', 1, 'Marker', '*')
%   plot(a, rand(1,20))
%
%   a(1) = cl_style('Color', [1.00 0 0]);
%   a(2) = cl_style('Color', [0.75 0 0]);
%   a(3) = cl_style('Color', [0.50 0 0]);
%   a(4) = cl_style('Color', [0.25 0 0]);
%   plot(a, rand(20,4))
%
% See also cl_style cl_style/apply Authors
% Authors : JMA
% VERSION  : $Id: plot.m,v 1.2 2003/04/07 08:51:39 augustin Exp $
% ----------------------------------------------------------------------------

function varargout = plot(this, varargin)

h = plot(varargin{:});
apply(h, this);

if nargout == 1
    varargout{1} = h;
end
