% Representation textuelle d'une instance cl_style
%
% Syntax
%   s = char_instance(a)
%
% Input Arguments
%   a : Instance de cl_style
%
% Output Arguments
%   s : Chaine de caracteres representative de l'instance
%
% Examples
%   a = cl_style;
%   s = char_instance(a)
%
% See also cl_style/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

% ----------
% Traitement

str {end+1} = sprintf('Color     <-> %s\t(choix possible : [r g b] ''r'' ''g'' ''b'' ''k'' ''y'' ''m'' ''c'')', num2strCode(this.Color));
str {end+1} = sprintf('LineStyle <-> %s\t(choix possible : ''-'' '':'' ''.-'' ''--'')', this.LineStyle);
str {end+1} = sprintf('LineWidth <-> %f', this.LineWidth);
str {end+1} = sprintf('Marker    <-> %s\t(choix possible : ''+'' ''o'' ''*'' ''.'' ''x'' ''s'' ''d'' ''v'' ''^'' ''>'' ''<'' ''p'' ''h'' ''none'')', this.Marker);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
