% Acces en ecriture des proprietes d'une instance cl_style
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : instance de la classe
%
% Name-only Arguments
%   Color     : 'r' | 'g' | {'b'} | 'k' | 'y' | 'm' | 'c' | [r g b]   'b' par defaut
%   LineStyle : {'-'} | '--' | ':' | '-.' | 'none'
%   LineWidth : 0.5 par defaut
%   Marker    : '+' | 'o' | '*' | '.' | 'x' | 's' | 'd' | 'v' | '^' | '>' | '<' |
%               'p' | 'h' | {'none'}
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   a = cl_style
%   set(a, 'Color', [1 0 0], 'LineStyle', '--', 'LineWidth', 1, 'Marker', '*')
%   a
%
% See also cl_style cla_param Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

% ---------------------
% Lecture des arguments

[varargin, this.LineStyle] = getPropertyValue(varargin, 'LineStyle', this.LineStyle);
[varargin, this.LineWidth] = getPropertyValue(varargin, 'LineWidth', this.LineWidth);
[varargin, this.Marker]    = getPropertyValue(varargin, 'Marker', this.Marker);
[varargin, Color]          = getPropertyValue(varargin, 'Color', this.Color); %#ok<ASGLU>

if ischar(Color)
    switch Color
        case 'y'
            this.Color = [1 1 0];
        case 'm'
            this.Color = [1 0 1];
        case 'c'
            this.Color = [0 1 1];
        case 'r'
            this.Color = [1 0 0];
        case 'g'
            this.Color = [0 1 0];
        case 'b'
            this.Color = [0 0 1];
        case 'w'
            this.Color = [1 1 1];
        case 'k'
            this.Color = [0 0 0];
    end
else
    this.Color = Color;
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
