% Affichage d'une colorbar interactive.
%
% Syntax
%   h = AScolorbar(...)
%
% Input Arguments
%   help colorbar
%
% Output Arguments
%   help colorbar
%
% Examples
%   imagesc(rand(10,10));
%   AScolorbar;
%
% See also colorbar Authors
% Authors : JMA
% VERSION  : $Id: AScolorbar.m,v 1.2 2002/06/04 14:42:25 augustin Exp $
%-------------------------------------------------------------------------------

function varargout = AScolorbar(varargin)

h0 = gcf;
hColobarParent = findobj(h0, 'Tag', 'AScolorbar');
if ~isempty(hColobarParent)
    %delete(hColobarParent);
    %%hColobarAxe = get(hColobarParent, 'Children');
    %%delete(hColobarAxe);
end

hImageAssociee = findobj(h0, 'Type', 'image');

if nargout == 0
    hColobarParent = colorbar(varargin{:});
else
    varargout = {AScolorbar(varargin{:})};
    hColobarParent = varargout{1};
end
set(hColobarParent, 'Tag', 'AScolorbar');


hColobarAxe = findobj(hColobarParent, 'Type', 'image');

commande = sprintf('editColorbar(%20.14f, %20.14f)', get(hImageAssociee(1), 'Parent'), get(hColobarAxe, 'Parent'));
set(hColobarAxe, 'ButtonDownFcn', commande);
set_HitTest_Off(hColobarAxe)
