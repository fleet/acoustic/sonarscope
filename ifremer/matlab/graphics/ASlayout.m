% FONCTION MORIBONDE
% Calcul automatique de la position d'uicontrols dans un contenant
%
% ASlayout( x, 'PropertyName', PropertyValue)
%
% Input Arguments
%   theControls : Handles des uicontrols
%   ihm       : Structure decrivant les proprietes des uicontrols
%     ihm.Lig : Lignes de placement de l'element graphique (cellule excel)
%     ihm.Col : Colonnes de placement de l'element graphique (cellule excel)
%
% Name-Value Pair Arguments
%   'Position' : Position du contenant
%   'XMargin'  : Marge a laisser entre les boutons en largeur (%)
%   'YMargin'  : Marge a laisser entre les boutons en hauteur (%)
%
% Examples
%   % A Faire
%
% See also fillFrame createUicontrols Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function ASlayout(theControls, ihm, varargin)

[varargin, Position	] = getPropertyValue(varargin, 'Position', [0 0 1 1]);
[varargin, XMargin]   = getPropertyValue(varargin, 'XMargin',  0.01);
[varargin, YMargin]   = getPropertyValue(varargin, 'YMargin',  0.01); %#ok<ASGLU>

%% Cas o� aucune indication de position des uicontrols n'est donnee :
% On aligne les boutons en vertical

if isempty(ihm)
    for k=1:length(theControls)
        ihm{k}.Col = 1;
        ihm{k}.Lig = k;
    end
end

%% R�cuperation du nombre max de cellules elementaires en colonne et en ligne

n = -Inf;
m = -Inf;
for k=1:length(ihm)
    n = max([n ihm{k}.Col]);
    m = max([m ihm{k}.Lig]);
end

set(theControls, 'Units', 'Normalized')
for k=1:length(ihm)
    
    %% Calcul de la position relative des uicontrols dans le contenant
    
    xmin = min(ihm{k}.Col - 1) / n;
    xmax = max(ihm{k}.Col)     / n;
    ymin = min(ihm{k}.Lig - 1) / m;
    ymax = max(ihm{k}.Lig)     / m;
    tempo = ymin;
    ymin = 1 - ymax;
    ymax = 1 - tempo;
    thePos = [xmin ymin (xmax-xmin) (ymax-ymin)] + [XMargin YMargin -2*XMargin -2*YMargin];
    
    %% Calcul de la position absolue des uicontrols dans la figure
    
    thePos = thePos .* Position([3 4 3 4]);
    thePos(1:2) = thePos(1:2) + Position(1:2);
    if(thePos(3) <= 0)
        disp('Pb avec la marge en X');
    end
    if(thePos(4) <= 0)
        disp('Pb avec la marge en Y');
    end
    
    %% Positionnement des uicontrols
    
    set(theControls(k), 'Position', thePos);
end
