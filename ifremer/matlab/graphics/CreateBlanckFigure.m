function CreateBlanckFigure

p    = ClParametre('Name', Lang('Verticalement', 'Vertical'), 'Unit', 'axis', 'Format', '%d', ...
    'MinValue', 1, 'MaxValue', 10, 'Value', 1);
p(2) = ClParametre('Name', Lang('Horizontalement','Horizontal'), 'Unit', 'axis', 'Format', '%d', ...
    'MinValue', 1, 'MaxValue', 10, 'Value', 1);

str1 = 'Nombre d''axes en hauteur et en largeur.';
str2 = 'Number of axis in height and width.';
a = SpinnerParametreDialog('params', p, 'Title', Lang(str1,str2));
style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
a.titleStyle = style1;
a.paramStyle = style2;
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-3 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
W = a.getParamsValue;

if prod(W) > 1
    str1 = 'Avant de faire des drag&drop sur les axes, il faut les rendre actifs en cliquant une fois dessus.';
    str2 = 'Before droping curves on these axes you have to select the destination one BEFORE the drag&drop action, sorry for the inconvenient.';
    my_warndlg(Lang(str1,str2), 1);
end

FigUtils.createSScFigure;
k = 0;
for i=1:W(1)
    for j=1:W(2)
        k = k + 1;
        subplot(W(1),W(2),k);
        h = PlotUtils.createSScPlot(rand(10,2));
        delete(h)
        grid on
    end
end
