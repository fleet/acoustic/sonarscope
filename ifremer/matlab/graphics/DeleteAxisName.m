% Supression des annotations des axes sur un graphique.
% Utile pour mettre des courbes cote a cote sans surcharger la figure.
%
% Syntax
%   DeleteAxisName('PropertyName', PropertyValue)
%
% PROPERTY NAMES :
%   HandleAxe  : Handle de l'axe. (gca par defaut)
%
% Name-only Arguments
%   Horz       : Suppression des annotations de l'axe X
%   Vert       : Suppression des annotations de l'axe Y
%
% Examples
%   figure; HandleAxe = axes;
%   DeleteAxisName('HandleAxe', HandleAxe, 'Horz')
%   DeleteAxisName('Vert')
%
% See also RefreshAxesZoom Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function DeleteAxisName(varargin)

[varargin, Horz] = getFlag(varargin, 'Horz');
[varargin, Vert] = getFlag(varargin, 'Vert');

[varargin, HandleAxe] = getPropertyValue(varargin, 'HandleAxe', []); %#ok<ASGLU>

if isempty(HandleAxe)
    HandleAxe = gca;
end

TableauDeBlanc = [];
if Horz
    NombreGraduation = size(get(HandleAxe, 'xtick'));
    for i=1:NombreGraduation
        tempo = ' ';
        TableauDeBlanc{i} = tempo;
    end
    set(HandleAxe, 'xticklabel', TableauDeBlanc);
    TableauDeBlanc = [];
end

if Vert
    NombreGraduation = size(get(HandleAxe, 'ytick'));
    for i=1:NombreGraduation
        tempo = ' ';
        TableauDeBlanc{i} = tempo;
    end
    set(HandleAxe, 'yticklabel', TableauDeBlanc);
end

