function [Fig, hAxe, hImage, hPing, hCurve, firstDisplay] = EI_DisplayResults(Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
    I, map, xEIImage, z, BW, ...
    AcrossDistPing, DepthPing, ...
    nomFicRaw, subEchogram, y, k, nbPings, iPing, ...
    DataEchogram, Immer, ...
    CercleAcross1, CercleDepth1, CercleAcross2, CercleDepth2) % EI_DisplayResults

Color = 'k';

Bathy = DepthPing + Immer;

set(hPing, 'YData', [y y])

figure(Fig)
set(Fig, 'CurrentAxes', hAxe(2))
if isa(I, 'uint8')
    RGB = ind2rgb(I, map);
    image(xEIImage, z, RGB);
else
    %                 infoG = imfinfo(nomFic2); % Infos graphiques.
    if firstDisplay
        hImage(1) = imagesc(xEIImage, z, I);
        colorbar;
        hold on;
        hCurve(1) = PlotUtils.createSScPlot(AcrossDistPing, Bathy,        Color);
        hCurve(2) = PlotUtils.createSScPlot(CercleAcross1,  CercleDepth1, Color);
        hCurve(3) = PlotUtils.createSScPlot(CercleAcross2,  CercleDepth2, Color);
        hold off
    else
        set(hImage(1), 'XData', xEIImage,       'YData', z, 'CData', I);
        set(hCurve(1), 'XData', AcrossDistPing, 'YData', Bathy);
        set(hCurve(2), 'XData', CercleAcross1,  'YData', CercleDepth1);
        set(hCurve(3), 'XData', CercleAcross2,  'YData', CercleDepth2);
    end
    s = stats(I);
    CLim = s.Quant_25_75;
%     CLim = [-5 35]
    set(hAxe(2), 'CLim', CLim)
end
Title = sprintf('%s - %d/%d - Ping=%d', nomFicRaw, k, nbPings, iPing);
axis xy; axis equal; axis tight; title(Title, 'Interpreter', 'none');

set(Fig, 'CurrentAxes', hAxe(3))
if isa(I, 'uint8')
    for k2=1:3
        RGB(:,:,k2) = RGB(:,:,k2) .* BW;
    end
    image([DataEchogram.xBab(subEchogram(k)) DataEchogram.xTri(subEchogram(k))], [Immer DataEchogram.Depth(subEchogram(k))], RGB);
else
    J = I;
    J(~BW) = NaN;
    xImage = [DataEchogram.xBab(subEchogram(k)) DataEchogram.xTri(subEchogram(k))];
    yImage = [Immer DataEchogram.Depth(subEchogram(k))];
    if firstDisplay
        hImage(2) = imagesc(xImage, yImage, J);
        colorbar
        hold on;
        hCurve(4) = PlotUtils.createSScPlot(AcrossDistPing, Bathy,        Color);
        hCurve(5) = PlotUtils.createSScPlot(CercleAcross1,  CercleDepth1, Color);
        hCurve(6) = PlotUtils.createSScPlot(CercleAcross2,  CercleDepth2, Color);
        hold off
    else
        set(hImage(2), 'XData', xImage, 'YData', yImage, 'CData', J);
        set(hCurve(4), 'XData', AcrossDistPing, 'YData', Bathy);
        set(hCurve(5), 'XData', CercleAcross1,  'YData', CercleDepth1);
        set(hCurve(6), 'XData', CercleAcross2,  'YData', CercleDepth2);
    end
end
axis xy; axis equal; axis tight; title(Title, 'Interpreter', 'none');