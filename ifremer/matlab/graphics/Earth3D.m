function Earth3D(lon, lat, Altitude, varargin)

[varargin, C]       = getPropertyValue(varargin, 'Texture', []);
[varargin, map]     = getPropertyValue(varargin, 'map', jet(256));
[varargin, ExaVert] = getPropertyValue(varargin, 'ExaVert', 1); %#ok<ASGLU>

% Altitude(isnan(Altitude)) = 6400*1e3;

R = 6400*1e3 + ExaVert * Altitude;
nbLon = length(lon);
nbLat = length(lat);
Lon = repmat(lon, nbLat, 1);
Lat = repmat(lat', 1, nbLon);
Z = R .* sin(Lat*(pi/180));
X = R .* cos(Lat*(pi/180)) .* cos(Lon*(pi/180));
Y = R .* cos(Lat*(pi/180)) .* sin(Lon*(pi/180));
figure;
f = uimenu('Text', 'Shadow');

f1 = uimenu(f, 'Text', 'On',  'Checked', 'Off',  'Callback', @CallbackShawOn);
f2 = uimenu(f, 'Text', 'Off', 'Checked', 'On', 'Callback', @CallbackShawOff);

hLight = [];

    function CallbackShawOn(varargin)
        hLight = light('Style','infinite');
        lighting flat
        [AZ,EL] = view;
        %         lightangle(hLight, AZ+180,EL+30)
        lightangle(hLight, AZ+150, -EL)
        %         set(findobj(gca,'type','surface'),...
        %     'FaceLighting','phong',...
        %     'AmbientStrength',.3,'DiffuseStrength',.8,...
        %     'SpecularStrength',0,'SpecularExponent',25,...
        %     'BackFaceLighting', 'unlit')
        
        %         lighting gouraud
        %         lighting phong
        set(f1, 'Checked', 'On')
        set(f2, 'Checked', 'Off')
    end

    function CallbackShawOff(varargin)
        delete(hLight)
        lighting none
        set(f1, 'Checked', 'Off')
        set(f2, 'Checked', 'On')
    end

if isempty(C)
    hs = surf(X, Y, Z, double(Altitude));
    colormap(map)
else
    if size(C,3) == 1
        hs = surf(X, Y, Z, double(C));
        colormap(map)
    else
        if max(C(:)) <= 1
            hs = surf(X, Y, Z, double(C));
        else
            hs = surf(X, Y, Z, double(C/256));
        end
    end
end
axis equal;
shading flat;
view(90,0);
axis off

set(hs,...
    'FaceLighting', 'flat',...
    'AmbientStrength',.3,'DiffuseStrength',.8,...
    'SpecularStrength',1,'SpecularExponent',25,...
    'BackFaceLighting', 'unlit')

end
