% Recopie du contenu d'une figure dans une autre figure.
%
% Syntax
%   Fig1Fig2(fig1, fig2)
%
% Input Arguments
%   fig1 : Figure receptrice
%   fig2 : Figure a recopier dans fig1
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex1.nvi')
%   a = cl_car_gen(nomFic); fig1 = edit(a);
%   a = cl_car_geo(nomFic); fig2 = edit(a);
%   Fig1Fig2(fig1, fig2)
%
% Remarks :
%   Dans sa version actuelle, cette fonction de recopie que les uicontrols.
%   C'est elle qui est utilisee dans ClCarBat, ClCarNav, etc...
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function Fig1Fig2(fig1, fig2)

% -------------------------------------
% On recupere la hauteur de la figure 1

T1 = get(fig1, 'Position');
T1 = T1(4);

% -------------------------------------
% On recupere la hauteur de la figure 2

T2 = get(fig2, 'Position');
T2 = T2(4);

% ------------------------------------
% On recupere tous les enfants de fig1

childen1 = get(fig1, 'Children');
N1 = length(childen1);
Position1 = get(childen1, 'Position');

if iscell(Position1)
    for i=1:length(Position1)
        P1(i,:) = Position1{i}; %#ok
    end
else
    P1 = Position1;
end

[gauche1, droite1, bas1, haut1] = position2coordonnees( P1 );

bas1  =  bas1 * T1 / (T1+T2);
haut1 = haut1 * T1 / (T1+T2);
offset = 1 - max(haut1);
bas1  =  bas1 + offset;
haut1 = haut1 + offset;

P1 = coordonnees2position( gauche1, droite1, bas1, haut1 );
for i=1:N1
    set(childen1(i), 'Position', P1(i, :))
end

% ------------------------------------
% On recupere tous les enfants de fig2

childen2 = get(fig2, 'Children');
N2 = length(childen2);
Position2 = get(childen2, 'Position');
if iscell(Position1)
    for i=1:length(Position2)
        P2(i, :) = Position2{i}; %#ok
    end
else
    P2 = Position2;
end


[gauche2, droite2, bas2, haut2] = position2coordonnees( P2 );

bas2  = bas2  * T2 / (T1+T2);
haut2 = haut2 * T2 / (T1+T2);

P2 = coordonnees2position( gauche2, droite2, bas2, haut2 );
for i=1:N2
    set(childen2(i), 'Position', P2(i, :))
end

% ----------------------------------------
% On recopie les boutons de fig2 dans fig1

for i=1:N2
    Type = get(childen2(i), 'Type');
    switch Type
        case 'uicontrol'
            Style = get(childen2(i), 'Style');
            String = get(childen2(i), 'String');
            BackgroundColor = get(childen2(i), 'BackgroundColor');
            TooltipString = get(childen2(i), 'TooltipString');
            Callback = get(childen2(i), 'Callback');
            Position = get(childen2(i), 'Position');
            Tag = get(childen2(i), 'Tag');
            CData = get(childen2(i), 'CData');
            
            figure(fig1);
            uicontrol('Style', Style, 'Units', 'normalized', ...
                'String', String, ...
                'BackgroundColor', BackgroundColor, ...
                'TooltipString', TooltipString, ...
                'Callback', Callback, ...
                'Position', Position, ...
                'Tag', Tag, 'CData', CData);
        otherwise
            fprintf('%s pas recopie pour l''instant dans Fig1Fig2.m\n', Type);
    end
end


% ----------------------------------------
% On recopie le UserData de fig2 dans fig1

UserData = get(fig2, 'UserData');
set(fig1, 'UserData', UserData);


% ---------------------------------------------------------------------
% On redimensionne la fenetre afin que les boutons aient le meme aspect

Position = get(fig1, 'Position');
%Position(4) = Position(4) * coeff;
Position(4) = T1 + T2;
set(fig1, 'Position', Position);





% --------------------------------------------------------------

function [xgauche, xdroite, ybas, yhaut] = position2coordonnees( Position )

xgauche = Position(:,1);
xdroite = Position(:,1) + Position(:,3);
ybas    = Position(:,2);
yhaut   = Position(:,2) + Position(:,4);


% --------------------------------------------------------------

function Position = coordonnees2position( xgauche, xdroite, ybas, yhaut )

Position(:,1) = xgauche;
Position(:,2) = ybas;
Position(:,3) = xdroite - Position(:,1);
Position(:,4) = yhaut   - Position(:,2);
