% Couleur grise par defaut
%
% Syntax
%   coul = Gris
%
% Examples
%    Gris
%
% See also GrisClair GrisFond JauneIfremer Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function coul = Gris
coul = [0.7 0.7 0.7];
