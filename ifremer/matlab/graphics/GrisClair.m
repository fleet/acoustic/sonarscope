% Couleur gris-clair
%
% Syntax
%   coul = GrisClair
%
% Examples
%    GrisClair
%
% See also Gris GrisFond JauneIfremer Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function coul = GrisClair
coul = [0.9 0.9 0.9];
