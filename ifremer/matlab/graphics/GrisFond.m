% Couleur grise pour background d'ecran de saisie
%
% Syntax
%   coul = GrisFond
%
% Examples
%   GrisFond
%
% See also Gris GrisClair JauneIfremer Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function coul = GrisFond
coul = [0.8 0.8 0.8];
