% Couleur jaune de la charte Ifremer
%
% Syntax
%   coul = JauneIfremer
%
% Examples
%    JauneIfremer
%
% See also Gris GrisClair GrisFond Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function coul = JauneIfremer
coul = [0.9961 0.9961 0.0039];
