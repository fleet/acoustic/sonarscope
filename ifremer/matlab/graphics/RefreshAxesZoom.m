% Affichage de tous les axes aux memes limites que l'axe courant
%
% Syntax
%   RefreshAxesZoom('PropertyName', PropertyValue)
%
% Input Arguments
%   listeAxes : liste des handles des axes
%
% PROPERTY NAMES :
%  'Vert' : Clonage vertical
%  'Horz' : Clonage horizontal
%  'axes' : Liste des axes devant etre clones
%
% Examples
%   subplot(2,1,1); plot(rand(1,100)); grid on; zoom on;
%   subplot(2,1,2); plot(rand(1,100)); grid on; zoom on;
%   RefreshAxesZoom('Horz', 'Vert')
%
%   figure; h1 = axes;
%   plot(rand(1,100)); grid on; zoom on;  % Faire un zoom
%   figure; h2 = axes;
%   plot(rand(1,100)); grid on; zoom on;
%   RefreshAxesZoom('Horz', 'Vert', 'axes', [h1 h2])
%
% See also XLim = get( gca, 'YLim'), set( gca, 'XLim', XLim);Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function   RefreshAxesZoom(varargin)

[varargin, Vert] = getFlag(varargin, 'Vert');
[varargin, Horz] = getFlag(varargin, 'Horz');

[varargin, axes] = getPropertyValue(varargin, 'axes', []); %#ok<ASGLU>

if isempty(axes)
    axes = findobj(gcf, 'Type', 'axes');
end

YLim = get( gca, 'YLim');
XLim = get( gca, 'XLim');

if Vert
    set( axes, 'YLim', YLim);
end

if Horz
    set( axes, 'XLim', XLim);
end
