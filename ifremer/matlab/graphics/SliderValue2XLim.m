% Calcul des limites d'un axe relativement a la valeur d'un slider qui lui est associe
% avec le meme facteur de zoom (ecart entre limites conserve).
%
% Syntax
%   SliderValue2XLim(Value, 'hAxe', hAxe, 'hCourbe', hCourbe)
%
% Input Arguments
%   Value   : Valeur du slider
%
% Name-Value Pair Arguments
%   hAxe    : Handle de l'axe concerne
%   hCourbe : Handle de la courbe concernee
%   Axe     : Indique si le slider est asservi a l'axe des X ou des Y  : {'X'} | 'Y'
%
% Examples
%   x=0:pi/100:20*pi; plot(exp(-(x/5.^2)) .* sin(x)); zoom on; grid on;
%   uicontrol('Style', 'slider', 'min', 0, 'max', 100, 'Tag', 'MonSlider', ...
%             'Units', 'Normalized', 'Position', [.1 .01 .8 .05]);
%
%   commande = sprintf('SliderValue2XLim(get(findobj(gcf, ''Tag'', ''MonSlider''), ''Value''))');
%   set(findobj(gcf, 'Tag', 'MonSlider'), 'Callback', commande);
%
%   UserData = [];
%   commande = sprintf('XLim2SliderValue(''hSlider'', findobj(gcf, ''Tag'', ''MonSlider''))');
%   putInUserData( 'CallbackZoomPersonnalise', commande, UserData );
%   set(gcf, 'UserData', UserData);
%
%   XLim2SliderValue('hSlider', findobj(gcf, 'Tag', 'MonSlider'));
%
% See also XLim2SliderValue Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function SliderValue2XLim(Value, varargin)

[varargin, hAxe] = getPropertyValue(varargin, 'hAxe', []);
if isempty(hAxe)
    hAxe = gca;
end

[varargin, Axe] = getPropertyValue(varargin, 'Axe', 'X');
if strcmp(Axe, 'X')
    strData = 'XData';
    strLim  = 'XLim';
else
    strData = 'YData';
    strLim  = 'YLim';
end

[varargin, hCourbe] = getPropertyValue(varargin, 'hCourbe', []); %#ok<ASGLU>

if isempty(hCourbe)
    hCourbes = findobj(hAxe, 'Type', 'line');
    minData = Inf;
    maxData = -Inf;
    for i=1:length(hCourbes)
        Data = get(hCourbes(i), strData);
        minData = min([Data, minData]);
        maxData = max([Data, maxData]);
    end
else
    Data = get(hCourbe, strData);
    minData = min(Data);
    maxData = max(Data);
end

% -------------------------------------------
% Recuperation des anciennes limites du trace

AxeLim = get(hAxe, strLim);

% -------------------------------------
% Calcul des nouvelles limites du trace

ecart = diff(AxeLim);
AxeLim = [Value Value+ecart];

if AxeLim(1) < minData
    AxeLim(1) = minData;
    AxeLim(2) = minData + ecart;
end
if AxeLim(2) > maxData
    AxeLim(2) = maxData;
    AxeLim(1) = maxData - ecart;
end

% ------------------------------------------
% Affectation des nouvelles limites du trace

set(hAxe, strLim, AxeLim);
