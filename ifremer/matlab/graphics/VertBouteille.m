% Couleur Vert-Bouteille
%
% Syntax
%   coul = VertBouteille
%
% Examples
%    VertBouteille
%
% See also Gris GrisFond JauneIfremer Authors
% Authors : GLC
%--------------------------------------------------------------------------------

function coul = VertBouteille
coul = [0 0.5 0];
