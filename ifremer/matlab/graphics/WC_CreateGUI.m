function [Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUI(Z, x, y, StepDisplay)

% TODO : rendre x optionel

Color = 'k';

hImage = [];
hCurve = [];

if StepDisplay == 0
    Fig   = [];
    hAxe  = [];
    hPing = [];
    return
end

Fig = FigUtils.createSScFigure('Name', ' - Water Column display - SonarScope - IFREMER');
ScreenSize = get(0, 'ScreenSize');
Position = floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4;
set(Fig, 'Position', Position);
hAxe(1) = subplot(2,4,[4 8]);
Stats = stats(Z);
Clim = Stats.Quant_03_97;
Z2 = NaN(y(end)-y(1)+1, length(x)); % Modif JMA le 25/02/2020 (utile si nettoyage pings)
% Z2(y-y(1)+1,:) = Z;   % Comment� par JMA le 23/03/2020 pour fichier "D:\Temp\Herve\20200320\7150\20200308_185945_PP_7150_12kHz.s7k"
% Z2(1:size(Z2,1),:) = Z; % Remplac� par JMA le 23/03/2020
Z2(1:size(Z,1),:) = Z; % Remplac� par JMA le 22/04/2020

imagesc(x, y, Z2, Clim); colormap(jet(256)); colorbar(hAxe(1)); hold on; axis xy;

hPing = plot([x(1) x(end)], [y(1) y(1)], Color); hold off;
hAxe(2) = subplot(2,4,1:3);
hAxe(3) = subplot(2,4,5:7);
