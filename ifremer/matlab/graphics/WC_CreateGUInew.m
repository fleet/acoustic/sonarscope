function [Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUInew(Z, StepDisplay, varargin)

[varargin, y]          = getPropertyValue(varargin, 'y',          []);
[varargin, ImageTitle] = getPropertyValue(varargin, 'ImageTitle', 'Depth (m)'); %#ok<ASGLU>

Color = 'k';

hImage = [];
hCurve = [];

if StepDisplay == 0
    Fig   = [];
    hAxe  = [];
    hPing = [];
    return
end

Fig = FigUtils.createSScFigure('Name', ' - Water Column display - SonarScope - IFREMER');
ScreenSize = get(0, 'ScreenSize');
Position = floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4;
set(Fig, 'Position', Position);
hAxe(1) = subplot(2,4,[4 8]);
Stats = stats(Z);

if Stats.NbData == 0
    my_close(Fig);
    Fig   = [];
    hAxe  = [];
    hPing = [];
    return
end

Clim = Stats.Quant_03_97;

x = 1:size(Z, 2);
if isempty(y)
    y = 1:size(Z, 1);
end
imagesc(x, y, Z, Clim); colormap(jet(256)); colorbar(hAxe(1)); hold on; axis xy;
xlabel('Beams #'); ylabel('Pings #'); title(ImageTitle);

nbBeams = size(Z,2);
hPing = plot([1 nbBeams], [1 1], Color); hold off;
hAxe(2) = subplot(2,4,1:3);
hAxe(3) = subplot(2,4,5:7);
