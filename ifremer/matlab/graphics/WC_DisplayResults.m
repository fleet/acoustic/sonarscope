function [Fig, hAxe, hImage, hPing, hCurve, firstDisplay] = ...
    WC_DisplayResults(Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
    TimePing, y, c, comp, Detection, CLimRaw, CLimComp)

strTime = t2str(TimePing);

set(hPing, 'YData', [y y])
%     set(Fig,'CurrentAxes', hAxe(2))
%     set(hAxe(2), 'Visible', 'Off')
%     set(hAxe(2), 'Visible', 'On')

figure(Fig)
subplot(hAxe(2))
x1 = get(c(end), 'x');
y1 = get(c(end), 'y');
Titre = c(end).Name;
if firstDisplay || ~ishandle(hImage(1))
    hImage(1) = imagesc(x1, y1, get(c(end), 'Image'), CLimRaw);
    xlabel('Across distance (m)'); ylabel('Depth vs antenna (m)');
    colormap(jet(256)); colorbar(hAxe(2))
    hold on;
    hCurve(1) = plot(Detection.X, Detection.Z, 'w');
    hold off
else
    set(hImage(1), 'XData', x1, 'YData', y1, 'CData', get(c(end), 'Image'))
    set(get(hImage(1), 'Parent'), 'CLim', CLimRaw)
    set(hCurve(1), 'XData', Detection.X, 'YData', Detection.Z)
end
axis('equal', 'tight', 'xy')
hTitle = get(gca, 'Title');
set(hTitle, 'String', [Titre ' - ' strTime], 'Interpreter', 'none')

%     [~, Axe] = imagesc(comp(1), 'Axe', hAxe(3)); axis equal; axis tight;
%     hTitle = get(Axe, 'Title');
%     Titre = get(hTitle, 'String');
%     set(hTitle, 'String', [Titre ' - ' strTime])
%     hold on; plot(Detection.X, Detection.Z, 'w')
%     plot(Points.AcrossDistance, Points.Z, 'wo'); hold off;
%     drawnow

%     set(Fig,'CurrentAxes', hAxe(3))
figure(Fig)
subplot(hAxe(3));
x1 = get(comp(1), 'x');
y1 = get(comp(1), 'y');
Titre = comp(1).Name;
if firstDisplay || ~ishandle(hImage(2))
    hImage(2) = imagesc(x1, y1, get(comp(1), 'Image'), CLimComp);
    xlabel('Across distance (m)'); ylabel('Depth vs antenna (m)');
    colormap(jet(256)); colorbar(hAxe(3))
    if ~verLessThan('MatLab', '8.1')
        % Pb avec effet de bord sur le statut Handle de l'image.
        colormap(jet(256)); colorbar(hAxe(3))
    end
    hold on;
    hCurve(2) = plot(Detection.X, Detection.Z, 'w');
    hold off;
    firstDisplay = 0;
else
    set(hImage(2), 'XData', x1, 'YData', y1, 'CData', get(comp(1), 'Image'))
    set(hCurve(2), 'XData', Detection.X, 'YData', Detection.Z)
end
axis('equal', 'tight', 'xy')
hTitle = get(gca, 'Title');
set(hTitle, 'String', [Titre ' - ' strTime], 'Interpreter', 'none')
drawnow

%{
incColor =  max(1, min(nbColors, floor(Points.Energie)-30));
uniqueIncColor = unique(incColor);
for ic=1:length(uniqueIncColor)
sunIc = (incColor == uniqueIncColor(ic));
hc = plot(Points.AcrossDistance(sunIc), Points.Z(sunIc), 'wo');
set(hc, 'Color', ColorPoints(incColor(ic),:));
end
hold off;
%}
