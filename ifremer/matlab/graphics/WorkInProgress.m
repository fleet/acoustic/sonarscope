% Affiche une fen�tre "Work in Progres" qui reste affich�e au bon vouloir
% de Matlab
%
% Syntax
%   WorkInProgress(...)
%
% Name-Value Pair Arguments
%   Message : Message particulier
%
% Examples
%   WorkInProgress
%   WorkInProgress('Vivement le WE')
%
% Remarks : Cette fonction permet de remplacer une fen�tre qui devrait avoir disparu
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function WorkInProgress(varargin)

global isUsingParfor %#ok<GVMIS>

logFileId = getLogFileId;

% D�but ajout JMA le 08/04/2013
% L'affichage des messages prenait pas mal de temps lors du chargement de
% nombreuses images ou pendant les d�codages de fichiers all, ...
% On se limite � un affichage par seconde
persistent persistentTime

if ~isempty(isUsingParfor) && isUsingParfor
    return
end

[varargin, Latency] = getPropertyValue(varargin, 'Latency', 3);
[varargin, Gap]     = getPropertyValue(varargin, 'Gap',     3); % 3 secondes

if ~isempty(Latency)
    if isempty(persistentTime)
        persistentTime = now;
    else
        NOW = now;
        diff = NOW - persistentTime;
        if diff <  (Gap / (24*3600))
            return
        end
        persistentTime = NOW;
    end
end
% Fin ajout JMA le 08/04/2013

str1 = 'Travail en cours ............................';
str2 = 'Work in progress .........................';
if nargin ~= 0
    str1 = varargin{1};
    str2 = varargin{1};
end

%% Message dans la fen�tre principale : � l'essai (activ� que si on est en version R2013b)

GCBF = gcbf;
if ~isempty(GCBF) && ishandle(GCBF)
    this = handle2obj(GCBF);
    if isa(this, 'cli_image')
        hMessage = findobj(GCBF, 'UserData', 'ProcessingMessages');
        if ~isempty(hMessage) && ishandle(hMessage(1))
            msgPrevString  = get(hMessage(1), 'String');
            msgToWrite     = Lang(str1,str2);
            
            % Eclatement sur plusieurs lignes du m�me message.
            pppp        = regexp(msgToWrite, '\n', 'split');
            newMsgString = {};
            for k=1:numel(pppp)
                if ~isempty(pppp{k})
                    newMsgString{end+1,1} = ['WiP - ' pppp{k}]; %#ok<AGROW>
                end
            end
            
            % Eclatement sur plusieurs lignes du m�me message si il
            % comporte des retour chariots. Et �clatement �galement si le
            % message est compos� de N Lignes X M Colonnes.
            newMsgString   = {};
            for k=1:size(msgToWrite,1)
                splitString = regexp(msgToWrite(k,:), '\n', 'split');
                for kk=1:numel(splitString)
                    if ~isempty(splitString{kk})
                        % Troncature de la ligne si elle d�passe N
                        % caract�res (150).
                        sizeLineMax = 150;
                        if numel(splitString{kk}) > sizeLineMax
                            iTrunc = 1:sizeLineMax:numel(splitString{kk});
                            for ii=1:numel(iTrunc)-1
                                newMsgString{end+1,1} = ['WiP - ' splitString{kk}(iTrunc(ii):iTrunc(ii+1))]; %#ok<AGROW>
                            end
                        else
                            newMsgString{end+1,1} = ['WiP - ' splitString{kk}]; %#ok<AGROW>
                        end
                    end
                end
            end
            
            % Purge des messages non-significatifs (type WorkInProgress).
            for k=length(msgPrevString):-1:1
                % On conserve la cha�ne si elle n'est pas indic�e par WiP.
                if (length(msgPrevString{k}) >= 2) && strcmp(msgPrevString{k}(1:3), 'WiP')
                    msgPrevString(k) = [];
                end
            end
            
            % Enrichissement du buffer de messages existant.
            if ~isempty(msgPrevString) && ~strcmp(msgPrevString(end), '')
                msgString = msgPrevString;
                for k=1:size(newMsgString,1)
                    msgString{end+1,1}  = newMsgString{k}; %#ok<AGROW>
                end
            else
                msgString = {['WiP - ' Lang(str1,str2)]};
            end
            
            
            set(hMessage, 'String', msgString)
            % Positionnement de la liste de messages sur le dernier.
            set(hMessage, 'Value', size(msgString, 1))
            
            logFileId.info('SonarScope', msgString{end});
            return
        end
    end
end

%% Fen�tre classique

logFileId.info('SonarScope', Lang(str1,str2));

% h = msgbox(Lang(str1,str2), 'IFREMER - SonarScope');
% % delete(findobj(h, 'Tag', 'OKButton')) % Comment� car �a prend beaucoup de ressources CPU
% % delete(findobj(h, 'Style', 'frame')) % Comment� car �a prend beaucoup de ressources CPU
% drawnow
% if ishandle(h)
%     if ishandle(h)
%         delete(h) % Delete but the windows stay alive until next graphic action, that answers to my need (it prevents me to be obliged to delete the window)
%     end
% end
