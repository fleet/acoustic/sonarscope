% Calcul des parametres d'un slider associe a un zoom sur un graphe.
% Si le handle du slider est passe en parametre (hSlider), la mise
% a jour du slider est faite automatiquement. Dans ce cas, il est
% inutile de demander les parametres de sortie.
%
% Syntax
%   [Value, minSlider, maxSlider, SliderStep] = XLim2SliderValue('hSlider', hSlider, 'hAxe', hAxe, 'hCourbe', hCourbe)
%
% Name-Value Pair Arguments
%   hSlider : Handle du slider
%   hAxe    : Handle de l'axe concerne
%   hCourbe : Handle de la courbe concernee
%   Axe     : Indique si le slider est asservi a l'axe des X ou des Y  : {'X'} | 'Y'
%
% Output Arguments
%   Value      : PropertyValue for Slider's PropertyName = 'Value'.
%   minSlider  : PropertyValue for Slider's PropertyName = 'minSlider'.
%   maxSlider  : PropertyValue for Slider's PropertyName = 'maxSlider'.
%   SliderStep : PropertyValue for Slider's PropertyName = 'SliderStep'.
%
% Examples
%   x=0:pi/100:20*pi; plot(exp(-(x/5.^2)) .* sin(x)); zoom on; grid on;
%   uicontrol('Style', 'slider', 'min', 0, 'max', 100, 'Tag', 'MonSlider', ...
%             'Units', 'Normalized', 'Position', [.1 .01 .8 .05]);
%
%   commande = sprintf('SliderValue2XLim(get(findobj(gcf, ''Tag'', ''MonSlider''), ''Value''))');
%   set(findobj(gcf, 'Tag', 'MonSlider'), 'Callback', commande);
%
%   UserData = [];
%   commande = sprintf('XLim2SliderValue(''hSlider'', findobj(gcf, ''Tag'', ''MonSlider''))');
%   putInUserData( 'CallbackZoomPersonnalise', commande, UserData );
%   set(gcf, 'UserData', UserData);
%
%   XLim2SliderValue('hSlider', findobj(gcf, 'Tag', 'MonSlider'));
%
% See also SliderValue2XLim Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [Value, minSlider, maxSlider, SliderStep] = XLim2SliderValue(varargin)

[varargin, hSlider] = getPropertyValue(varargin, 'hSlider', []);

[varargin, hAxe] = getPropertyValue(varargin, 'hAxe', []);
if isempty(hAxe)
    hAxe = gca;
end

[varargin, Axe] = getPropertyValue(varargin, 'Axe', 'X');
if strcmp(Axe, 'X')
    strData = 'XData';
    strLim  = 'XLim';
else
    strData = 'YData';
    strLim  = 'YLim';
end

[varargin, hCourbe] = getPropertyValue(varargin, 'hCourbe', []); %#ok<ASGLU>

if isempty(hCourbe)
    hCourbes = findobj(hAxe, 'Type', 'line');
    minData = Inf;
    maxData = -Inf;
    for i=1:length(hCourbes)
        Data = get(hCourbes(i), strData);
        minData = min([Data, minData]);
        maxData = max([Data, maxData]);
    end
else
    XData = get(hCourbe, strData);
    minData = min(XData);
    maxData = max(XData);
end


difXData = maxData - minData;

AxeLim = get(hAxe, strLim);
if AxeLim(1) < minData
    AxeLim(1) = minData;
end
if AxeLim(2) > maxData
    AxeLim(2) = maxData;
end

if diff(AxeLim) ~= 0
    set(hAxe, strLim, AxeLim);
end

difAxeLim = diff(AxeLim);

% --------------------------------------
% Calcul de la position du slider

Value = AxeLim(1);
Value = max(Value, minData);
Value = min(Value, maxData);



maxSlider = maxData-difAxeLim;
if maxSlider <= minData
    maxSlider = minData + eps;
end

% --------------------------------------------
% Calcul des pas : definit la taille du slider

if difXData <= difAxeLim
    minstep = 1;
    maxstep = 1000000000;
else
    minstep = (difAxeLim / (difXData-difAxeLim)) / 2;
    if(minstep >= 1)
        minstep = 1;
        maxstep = 1000000000;
    else
        maxstep = minstep * 2;
    end
end

SliderStep = [minstep maxstep];
minSlider = minData;


% ----------------------
% Modification du slider

if ~isempty(hSlider)
    if strcmp(Axe, 'Y')
        Value = maxSlider - Value + minSlider;
        if minSlider == maxSlider
            %             minSlider = maxSlider - 1;
        end
        
    end
    
    set(hSlider, 'Value', Value, 'Min', minSlider, 'Max', maxSlider, 'SliderStep', SliderStep);
    drawnow
end
