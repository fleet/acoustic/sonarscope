% Reactivation de tous les uicontrols d'une figure (FONCTION MORIBONDE)
%
% Syntax
%   activeFig
%
% PROPERTY NAMES :
%    Children : Liste des uicontrols
%
% See also desactiveFig Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function activeFig(varargin)

%fonction qui rend actifs tous les boutons d'une figure

h0 = gcf;

switch nargin
    case 0
        Children = get(h0, 'Children');
        axes = findobj(h0, 'Type', 'axes');
        for i=1:length(Children)
            if isempty(find(axes == Children(i), 1))
                set( Children(i) , 'Enable', 'on');
                % 			bouton{i} =  Children(i);
            else
                set(axes(i), 'Visible', 'on');
                % 			graphes{i} =   axes(i);
            end
        end
    case 2
        Children = varargin{1};
        axes = varargin{2};
        set( Children , 'Enable', 'on');
        set( axes, 'Visible', 'on');
    case 1
        Children = varargin{1};
        set( Children , 'Enable', 'on');
end



