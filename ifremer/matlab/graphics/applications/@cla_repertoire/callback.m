% Gestion des callbacks d'une IHM d'une instance
%
% Syntax
%   b = callback(a, ...)
%
% Input Arguments
%   a : Instance de cla_repertoire
%
% Name-Value Pair Arguments
%   Les valeurs sont dynamiques, elles sont lues par :
%     get_msg_cancel(this.ihm.cancel)
%     get_msg_ok(this.ihm.cancel)
%     this.ihm.msg...
%
% Output Arguments
%   b : L'instance modifi�e
%
% See also cla_repertoire Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function callback(this, msg, varargin) %#ok

if isempty(msg)
    return
end

%% On recup�re l'instance dans le UserData de la figure

% this = handle2obj(gcbf);
this = handle2objJAVA(gcbo);
if ~isa(this, 'cla_repertoire')
    return
end


switch msg
    case  'Quit'    % Message provenant du bouton Quit
        % InstanceSauvee = handle2savedObj(this.ihm.figure);
        close(this.ihm.figure)
        
    case 'MsgMoveRight'
        NewSelection = get(this.ihm.OrigSelection.cpn.Browser, 'file');
        if ~isempty(NewSelection)
            listSelectFiles = findobj(gcbf, 'Tag','AncreListFileSelection');
            % Suppression pr�alable de la num�rotation dans la liste.
            newList = get(listSelectFiles, 'String');
            if isempty(newList)
                newList = NewSelection;
            else
                newList = UnNumerateListNames(newList);
                newList = my_union(newList, NewSelection);
            end
            % Pr�fixe des noms des fichiers par le nombre et l'indice.
            pppp = NumerateListNames(newList);
            set(listSelectFiles, 'String', pppp);
        end
        
    case 'MsgMoveRightAll'
        h = findobj('tag', 'Browser_clc_browser');
        javaComposant = get(h, 'JavaComponent');
        curDir = javaComposant.getCurrentDirectory;
        % Filtrage customis� du contenu du r�pertoire.
        % Impossiblit� de filtrer directement par les fonctions basiques
        % (curDir.listFiles ???? : pb de prototype du fileFilterChooser2).
        myFiltreCurrent = javaComposant.getFileFilter;
        listOrigFiles = utilFunction.curDirFileList.listFiles(curDir, myFiltreCurrent);
        
        listSelectFiles = findobj(gcbf,'Tag','AncreListFileSelection');
        if ~isempty(listOrigFiles)
            fileToTransfert = {};
            j = 1;
            for k=1:numel(listOrigFiles)
                if listOrigFiles(k).isFile
                    fileToTransfert{j} = listOrigFiles(k).getAbsolutePath.toCharArray'; %#ok<AGROW>
                    j = j + 1;
                end
            end
            % Suppression pr�alable de la num�rotation dans la liste.
            listeFic = get(listSelectFiles,'String');
            listeFic = UnNumerateListNames(listeFic);
            newList = my_union(listeFic, fileToTransfert);
            % Pr�fixe des noms des fichiers par le nombre et l'indice.
            pppp = NumerateListNames(newList);
            set(listSelectFiles, 'String', pppp);
        end
%         my_close(hWarn);
        
    case 'MsgMoveLeft'
        listSelectFiles = findobj(gcbf,'Tag','AncreListFileSelection');
        ToRemove = get(listSelectFiles,'Value');
        if any(ToRemove)
            FileList = get(listSelectFiles,'String');
            if isempty(FileList)
                return
            end
            switch length(ToRemove)
                case 0
                    return
                case 1
                    if (ToRemove ~= 1) && (ToRemove == numel(FileList))
                        % Changement du Focus sinon l'objet Listbox dispara�t
                        % lorsqu'on supprime le dernier item.
                        set(listSelectFiles, 'Value', ToRemove-1);
                    end
                otherwise
                    set(listSelectFiles, 'Value', 1);
            end
            
            FileList(ToRemove)=[];
            % Suppression de la num�rotation pr�alable des noms de fichiers
            FileList = UnNumerateListNames(FileList);
            % Renum�rotation des fichiers dans la liste.
            pppp = NumerateListNames(FileList);
            set(listSelectFiles, 'String', pppp);
        end
        
    case 'MsgMoveLeftAll'
        listSelectFiles = findobj(gcbf,'Tag','AncreListFileSelection');
        FileList = cellstr(get(listSelectFiles,'String'));
        FileList(:)=[];
        set(listSelectFiles, 'String', FileList);
        
    case 'MsgMoveTop'
        listSelectFiles = findobj(gcbf,'Tag','AncreListFileSelection');
        ToShift = get(listSelectFiles,'Value');
        if ~isempty(ToShift)
            FileList = get(listSelectFiles,'String');
            FileLength = size(FileList,1);
            Ordre = [ToShift setdiff(1:FileLength, ToShift)];
            % Suppression de la num�rotation pr�alable des noms de fichiers
            FileList = UnNumerateListNames(FileList);
            % R�ordonnancement de la liste du pr�fixe d'indice des fichiers dans la liste.
            FileList = FileList(Ordre);
            % Ajout du pr�fixe d'indice des fichiers dans la liste.
            pppp = NumerateListNames(FileList);
            set(listSelectFiles, 'String', pppp, 'Value', 1:length(ToShift));
        end
        
    case 'MsgMoveBottom'
        listSelectFiles = findobj(gcbf,'Tag','AncreListFileSelection');
        ToShift = get(listSelectFiles,'Value');
        if ~isempty(ToShift)
            FileList = get(listSelectFiles,'String');
            FileLength = size(FileList,1);
            Ordre = [setdiff(1:FileLength, ToShift) ToShift];
            % Suppression de la num�rotation pr�alable des noms de fichiers
            FileList = UnNumerateListNames(FileList);
            % R�ordonnancement de la liste du pr�fixe d'indice des fichiers dans la liste.
            FileList = FileList(Ordre);
            % Ajout du pr�fixe d'indice des fichiers dans la liste.
            pppp = NumerateListNames(FileList);
            set(listSelectFiles, 'String', pppp, 'Value', FileLength-length(ToShift)+1:FileLength );
        end
        
    case 'MsgMoveDown'
        listSelectFiles = findobj(gcbf, 'Tag', 'AncreListFileSelection');
        ToShift = get(listSelectFiles, 'Value');
        if ~isempty(ToShift)
            % FileList = cellstr(get(listSelectFiles, 'String'));
            FileList = get(listSelectFiles, 'String');
            FileLength = size(FileList,1);
            Ordre = 1:FileLength;
            for k=length(ToShift):-1:1
                if ToShift(k) < FileLength
                    sub = ToShift(k):(ToShift(k)+1);
                    Ordre(ToShift(k):(ToShift(k)+1)) = fliplr(sub);
                    Shifted(k) = ToShift(k)+1;
                else
                    Shifted(k) = ToShift(k);
                end
            end
            % Suppression de la num�rotation pr�alable des noms de fichiers
            FileList = UnNumerateListNames(FileList);
            % R�ordonnancement de la liste du pr�fixe d'indice des fichiers dans la liste.
            FileList = FileList(Ordre);
            % Ajout du pr�fixe d'indice des fichiers dans la liste.
            pppp = NumerateListNames(FileList);
            set(listSelectFiles, 'String', pppp, 'Value', Shifted);
        end
        
    case 'MsgMoveUp'
        listSelectFiles = findobj(gcbf, 'Tag', 'AncreListFileSelection');
        ToShift = get(listSelectFiles, 'Value');
        if ~isempty(ToShift)
            FileList = cellstr(get(listSelectFiles, 'String'));
            FileLength = size(FileList,1);
            Ordre = 1:FileLength;
            for k=1:length(ToShift)
                if ToShift(k) > 1
                    sub = (ToShift(k)-1):ToShift(k);
                    Ordre((ToShift(k)-1):ToShift(k)) = fliplr(sub);
                    Shifted(k) = ToShift(k)-1; %#ok
                else
                    Shifted(k) = ToShift(k); %#ok
                end
            end
            % Suppression de la num�rotation pr�alable des noms de fichiers
            FileList = UnNumerateListNames(FileList);
            % R�ordonnancement de la liste du pr�fixe d'indice des fichiers dans la liste.
            FileList = FileList(Ordre);
            % Ajout du pr�fixe d'indice des fichiers dans la liste.
            pppp = NumerateListNames(FileList);
            set(listSelectFiles, 'String', pppp, 'Value', Shifted);
        end
        
    case 'MsgMoveFlipud'
        listSelectFiles = findobj(gcbf, 'Tag', 'AncreListFileSelection');
        ToShift = get(listSelectFiles, 'Value');
        if ~isempty(ToShift)
            FileList = cellstr(get(listSelectFiles, 'String'));
            FileList = UnNumerateListNames(FileList);
            FileList = flipud( FileList);
            FileList = NumerateListNames(FileList);
            Shifted = length(FileList)+ 1 - ToShift;
            set(listSelectFiles, 'String', FileList, 'Value', Shifted);
        end
        
        
    case 'MsgFilter' % Gestion des filtres
        hdlGeometryTypeFilter = findobj(gcbf, 'Tag', 'AncreGeoTypeVal');
        strGeometryType = get(hdlGeometryTypeFilter, 'String');
        GeometryType  = get(hdlGeometryTypeFilter, 'Value');
        strGeoFiltre = strGeometryType(GeometryType,:);
        
        hdlDataTypeFilter = findobj(gcbf, 'Tag', 'AncreDataTypeVal');
        strDataType = get(hdlDataTypeFilter, 'String');
        DataType  = get(hdlDataTypeFilter, 'Value');
        strDataFiltre = strDataType(DataType,:);
        
        hdlStrContenant = findobj(gcbf, 'Tag', 'AncreContenantVal');
        strContenantVal = get(hdlStrContenant, 'String');
        
        hdlStrExcluant = findobj(gcbf, 'Tag', 'AncreExcluantVal');
        strExcluantVal = get(hdlStrExcluant, 'String');
        
        h = findobj('tag', 'Browser_clc_browser');
        javaComposant = get(h, 'JavaComponent');
        
        % Traitement d'un seul coup des occurrences de filtrage des noms de fichiers
        if strcmpi(deblank(strGeoFiltre), 'Unknown')
            strGeoFiltre = [];
        else
            strGeoFiltre = ['_' strGeoFiltre];
        end
        if strcmpi(deblank(strDataFiltre), 'Unknown')
            strDataFiltre = [];
        else
            strDataFiltre = ['_' strDataFiltre];
        end
        if isempty(strContenantVal)
            strContenantVal = [];
        end
        if isempty(strExcluantVal)
            strExcluantVal = [];
        end
        this.GeometryType = GeometryType;
        this.DataType = DataType;
        this.StrIncluse = strContenantVal;
        this.StrExclue = strExcluantVal;
        
        listeFiltres = javaComposant.getChoosableFileFilters;
        % On modifie tous les filtres, sinon on le cr��.
        for k=1:numel(listeFiltres)
            myFiltreDef = listeFiltres(k);

            if ~strcmp(listeFiltres(k).getDescription, 'Tous les fichiers')
                myFiltreDef.replaceFilters(strGeoFiltre, strDataFiltre, ...
                    strContenantVal, strExcluantVal);
            end
        end

        pppp = javaComposant.getComponents;
        filePanel = pppp(3);
        hdl = filePanel.getComponents;
        filePane = hdl(2);
        filePane.rescanCurrentDirectory;
        
    case 'SaveInFile'
        h = findobj('tag', 'Browser_clc_browser');
        javaComposant = get(h, 'JavaComponent');
        curDir = javaComposant.getCurrentDirectory;
        this.RepDef = char(curDir.toString);
        
        listeFic = get(findobj(gcf, 'Tag', 'AncreListFileSelection'), 'String');
        [flag, nomFic] = my_uiputfile( ...
            {'*.txt', 'List file (*.txt)'; ...
            '*.*',    'All Files (*.*)'}, ...
            'Save as', this.RepDef);
        if ~flag
            return
        end
        this.RepDef       = fileparts(nomFic);
        % Suppression de la num�rotation des noms de fichiers
        [token, listeFic] = strtok(listeFic, '|');  %#ok<ASGLU>
        listeFic          = strrep(listeFic, '| ', '');
        fid = fopen(nomFic, 'wt');
        NetUseWords = [];
        for k=1:length(listeFic)
            [filename, NetUseWords] = windows_replaceVolumeLetter(listeFic{k}, 'NetUseWords', NetUseWords);
            fprintf(fid, '%s\n', filename);
        end
        fclose(fid);
        
    case 'LoadFromFile'
        h = findobj('tag', 'Browser_clc_browser');
        javaComposant = get(h, 'JavaComponent');
        curDir = javaComposant.getCurrentDirectory;
        this.RepDef = char(curDir.toString);
        [flag, nomFic] = my_uigetfile( ...
            {'*.txt', 'List file (*.txt)'; ...
            '*.*', 'All Files (*.*)'}, ...
            'Pick a file', this.RepDef);
        if ~flag
            return
        end
        this.RepDef = fileparts(nomFic);
        listeFic = {};
        fid = fopen(nomFic);
        NetUseWords = [];
        while 1
            tline = fgetl(fid);
            if ~ischar(tline)
                break
            end

            V = this.ExtFiles;
            [listeFic, NetUseWords] = filtreExtensions(tline, V, listeFic, NetUseWords, false);
%{
            [V, ~] = getPropertyValue(V, 'All Filters', []);
            ListeOfExtensions = V(1:2:end);
            if ~isempty(tline) && ~strcmp(tline(1), '%')
                % On observe de plus pr�s la correspondance des extensions.
                [~, ~, extLine] = fileparts(tline);
                nbExpectedExtensions = length(ListeOfExtensions);
                
                % Si l'extension '*.*' est dans la liste des extensions
                % possibles, on retient syst�matiquement le fichier.
                [C, ~, ~] =  intersect(ListeOfExtensions, '*.*');
                
                if nbExpectedExtensions == 0 || ~isempty(C)
                    ExpectedExtension = 1;
                else
                    ExpectedExtension = 0;
                    for k=1:nbExpectedExtensions
                        if strcmpi(extLine, ListeOfExtensions{k}(2:end))
                            ExpectedExtension = 1;
                        end
                    end
                end
                if ExpectedExtension
                    [tline, NetUseWords] = windows_replaceVolumeLetter(tline, 'NetUseWords', NetUseWords);
                    listeFic{end+1} = tline; %#ok<AGROW>
                else
                    str1 = sprintf('Attention, le fichier "%s" ne correspond pas aux extensions attendues.', tline);
                    str2 = sprintf('Warning, file "%s" do not correspond to the expected extensions.', tline);
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'CeFichierNeCorrespondPasAuxExtensionsAttendues');
                end
            end
            %}
        end
        fclose(fid);
        
        for k2=1:length(listeFic)
            if ~exist(listeFic{k2}, 'file')
                str1 = sprintf('ATTENTION : le fichier "%s" n''existe pas. V�rifiez la lettre du volume si c''est un disque dur externe.\n\nVouleez-vous malgr� tout ajouter cette liste ?', listeFic{k2});
                str2 = sprintf('WARNING : file "%s" does not exist (and possibly others). Check the letter of the hard drive if it is an external unit.\n\n Do you want to add this list ?', listeFic{k2});
%                 my_warndlg(Lang(str1,str2), 1);
                [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
                if ~flag || (rep == 2)
                    return
                end
                break
            end
        end
        
        listeFicBefore = get(findobj(gcf, 'Tag','AncreListFileSelection'), 'String');
        [token, listeFicBefore] = strtok(listeFicBefore, '|');  %#ok<ASGLU>
        listeFicBefore = strrep(listeFicBefore, '| ', '');
        newList = my_union(listeFic, listeFicBefore);
        pppp = NumerateListNames(newList);
        set(findobj(gcf, 'Tag','AncreListFileSelection'), 'String', pppp);
        
    case 'OK'
        FileList = {};
        if this.EnableSelection
            FileList = get(findobj(gcf, 'Tag', 'AncreListFileSelection'), 'String');
            % Suppression de la num�rotation pr�alable des noms de fichiers
            FileList = UnNumerateListNames(FileList);
        else
            FileList{1} = get(this.ihm.OrigSelection.cpn.Browser, 'ficName');
        end
        if isempty(FileList)
            str1 = 'Aucun �l�ment n''a �t� entr�. Cliquer sur annuler pour sortir';
            str2 = 'No item has been selected. Use "Cancel" if you want to quit.';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        this.listeFic = FileList;
        if isempty(FileList)
            this.lastDir = get(this.ihm.OrigSelection.cpn.Browser, 'currentDir');
        else
            nomDir = fileparts(FileList{end});
            if exist(nomDir, 'dir')
                this.lastDir = fileparts(nomDir);
            else
                this.lastDir = get(this.ihm.OrigSelection.cpn.Browser, 'currentDir');
            end
        end
        if isempty(this.lastDir)
            this.lastDir = my_tempdir;
        end
        
        hdlGeometryTypeFilter = findobj(gcbf, 'Tag', 'AncreGeoTypeVal');
        %         strGeometryType = get(hdlGeometryTypeFilter, 'String');
        GeometryType  = get(hdlGeometryTypeFilter, 'Value');
        % strGeoFiltre = strGeometryType(GeometryType,:);
        
        hdlDataTypeFilter = findobj(gcbf, 'Tag', 'AncreDataTypeVal');
        %         strDataType = get(hdlDataTypeFilter, 'String');
        DataType  = get(hdlDataTypeFilter, 'Value');
        % strDataFiltre = strDataType(DataType,:);
        
        hdlStrContenant = findobj(gcbf, 'Tag', 'AncreContenantVal');
        strContenantVal = get(hdlStrContenant, 'String');
        
        hdlStrExcluant = findobj(gcbf, 'Tag', 'AncreExcluantVal');
        strExcluantVal = get(hdlStrExcluant, 'String');
        
        this.GeometryType = GeometryType;
        this.DataType = DataType;
        this.StrIncluse = strContenantVal;
        this.StrExclue = strExcluantVal;
        
        this.flagValid = 1;
        
        var2handle('Exit', 'Exit', this.ihm.figure);
        savedObj2handle(this, this.ihm.figure);
        InstanceSauvee = handle2savedObj(this.ihm.figure);%#ok
        uiresume(this.ihm.figure);
        return
        
    case 'Cancel'
        this.lastDir = get(this.ihm.OrigSelection.cpn.Browser,'currentDir');
        this.listeFic = '';
        this.flagValid = 0;
        
        close(this.ihm.figure);
        
    case 'MsgMoveSubDirectories'
        h = findobj('tag', 'Browser_clc_browser');
        javaComposant = get(h, 'JavaComponent');
        
        % methods(javaComposant)
        % javaComposant.getSelectedFile
        % javaComposant.getSelectedFiles
        
        curDir = javaComposant.getCurrentDirectory;
        % Filtrage customis� du contenu du r�pertoire.
        % Impossiblit� de filtrer directement par les fonctions basiques
        % (curDir.listFiles ???? : pb de prototype du fileFilterChooser2).
        myFiltreCurrent = javaComposant.getFileFilter;
        listOrigFiles = utilFunction.curDirFileList.listFiles(curDir, myFiltreCurrent);
        
        listSelectFiles = findobj(gcbf, 'Tag', 'AncreListFileSelection');
        if ~isempty(listOrigFiles)
            str1 = 'Nombre max de niveaux de sous-r�pertoires � explorer.';
            str2 = 'Maximum number of sub-directories to browse.';
            [flag, this.MaxLevel] = inputOneParametre(Lang(str1,str2), 'Value', ...
                'Value', this.MaxLevel, 'Unit', 'nb', 'MinValue', 1, 'Format', '%d');
            if ~flag
                return
            end
            
            str{1} = Lang('Un r�pertoire cache "SonarScope"', 'A "SonarScope" cache directory');
            str{2} = Lang('Associ� � un fichier .xml', 'Associated to a .xml files');
            str{3} = Lang('Associ� � un fichier .ers', 'Associated to a .ers files');
            str1 = 'Arr�ter la recherche de sous-r�pertoires lorsque le r�pertoire est :';
            str2 = 'Stop the exploration of subdirectories when the directory is :';
            [rep, flag] = my_listdlgMultiple(Lang(str1,str2), str, 'InitialValue', 1:length(str));
            if ~flag
                return
            end
            nomDirFinal = {'SonarScope'; '.xml'; '.ers'};
            nomDirFinal = nomDirFinal(rep);
            
            str1 = 'Recherche des fichiers en cours';
            str2 = 'Looking for files';
            WorkInProgress(Lang(str1,str2))

            Ext = char(myFiltreCurrent.getExtension);
            if strcmp(Ext, 'xxx')
                Ext = [];
            end
            fileToTransfert = listeFicOnDepthDirMaxLevel(this.MaxLevel, char(curDir.toString),  Ext, ...
                'nomDirFinal', nomDirFinal);
            
            hdlGeometryTypeFilter = findobj(gcbf, 'Tag', 'AncreGeoTypeVal');
            GeometryType  = get(hdlGeometryTypeFilter, 'Value');
            if GeometryType == 1
                strGeoFiltre = [];
            else
                strGeometryType = get(hdlGeometryTypeFilter, 'String');
                strGeoFiltre = rmblank(strGeometryType(GeometryType,:));
            end
            
            hdlDataTypeFilter = findobj(gcbf, 'Tag', 'AncreDataTypeVal');
            DataType  = get(hdlDataTypeFilter, 'Value');
            if DataType == 1
                strTypeFiltre = [];
            else
                strDataType = get(hdlDataTypeFilter, 'String');
                strTypeFiltre = rmblank(strDataType(DataType,:));
            end
        
            hdlStrContenant = findobj(gcbf, 'Tag', 'AncreContenantVal');
            strContenantVal = get(hdlStrContenant, 'String');
            hdlStrExcluant  = findobj(gcbf, 'Tag', 'AncreExcluantVal');
            strExcluantVal  = get(hdlStrExcluant, 'String');
            
            fileToTransfert = filterListFiles(fileToTransfert, ...
                {strGeoFiltre, strTypeFiltre, strContenantVal}, strExcluantVal);
            
            % Suppression pr�alable de la num�rotation dans la liste.
            listeFic = get(listSelectFiles, 'String');
            listeFic = UnNumerateListNames(listeFic);
            newList = my_union(listeFic, fileToTransfert);
            % Pr�fixe des noms des fichiers par le nombre et l'indice.

            listeFic = {};
            NetUseWords = [];
            V = this.ExtFiles;
            for k=1:length(newList)
                [listeFic, NetUseWords] = filtreExtensions(newList{k}, V, listeFic, NetUseWords, true);
            end
            newList = listeFic;

            pppp = NumerateListNames(newList);
            set(listSelectFiles, 'String', pppp);
        end
            
    case 'MsgHelp'
        SonarScopeHelp('SScDoc-Tutorial-FileBrowser.html');
        
    otherwise
        str = sprintf('Message ''%s'' non interprete', msg);
        my_warndlg(['cla_repertoire:callback ', str], 0);
end


function ListNames = NumerateListNames(list)
if ~isempty(list)
    % Ajout de l'indice de liste devant la cha�ne.
    for k=1:numel(list)
        ListNames{k} = [num2str(k) ' | ' list{k}]; %#ok<AGROW>
    end
else
    ListNames = [];
end


function ListNames = UnNumerateListNames(list)
if ~isempty(list)
    % Suppression de la num�rotation des noms de fichiers
    [token, list] = strtok(list, '|');  %#ok<ASGLU>
    ListNames = strrep(list, '| ', '');
else
    ListNames = [];
end


function newList = my_union(listeFic, fileToTransfert)

listeFic = listeFic(:);
fileToTransfert = fileToTransfert(:);
% TODO : bug avec la R2013a ici si listeFic est vide
if isempty(listeFic)
    newList = fileToTransfert;
else
    [~, ~, ib] = intersect(listeFic, fileToTransfert);
    fileToTransfert(ib) = [];
    [~, ia, ib] = union(listeFic, fileToTransfert);
    newList = [listeFic(ia); fileToTransfert(ib)];
end


function [listeFic, NetUseWords] = filtreExtensions(tline, V, listeFic, NetUseWords, Mute)

[V, ~] = getPropertyValue(V, 'All Filters', []);
ListeOfExtensions = V(1:2:end);
if ~isempty(tline) && ~strcmp(tline(1), '%')
    % On observe de plus pr�s la correspondance des extensions.
    [~, ~, extLine] = fileparts(tline);
    nbExpectedExtensions = length(ListeOfExtensions);

    % Si l'extension '*.*' est dans la liste des extensions
    % possibles, on retient syst�matiquement le fichier.
    [C, ~, ~] =  intersect(ListeOfExtensions, '*.*');

    if nbExpectedExtensions == 0 || ~isempty(C)
        ExpectedExtension = 1;
    else
        ExpectedExtension = 0;
        for k=1:nbExpectedExtensions
            if strcmpi(extLine, ListeOfExtensions{k}(2:end))
                ExpectedExtension = 1;
            end
        end
    end
    if ExpectedExtension
        [tline, NetUseWords] = windows_replaceVolumeLetter(tline, 'NetUseWords', NetUseWords);
        listeFic{end+1} = tline;
    else
        if ~Mute
            str1 = sprintf('Attention, le fichier "%s" ne correspond pas aux extensions attendues.', tline);
            str2 = sprintf('Warning, file "%s" do not correspond to the expected extensions.', tline);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'CeFichierNeCorrespondPasAuxExtensionsAttendues');
        end
    end
end
