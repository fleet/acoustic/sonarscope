% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(a)
%
% Input Arguments
%   a : Une instance de classe cla_repertoire
%
% Output Arguments
%   s : Chaine de caracteres
%
% Examples
%   a = cla_repertoire;
%   s = char(a)
%
% See also cla_repertoire cla_repertoire/display Authors
% Authors : LUR, GLU
% VERSION  : $Id: char.m,v 1.2 2002/07/11 12:21:55 tletoux Exp $
%-------------------------------------------------------------------------------


function str = char(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);%#ok
            str1 = char(this(i, j));
            str{end+1} = str1;%#ok
        end
    end
    str = cell2str(str);

else
    if (m*n) == 0
        str = '';
        return
    end

    % ---------------
    % Instance unique

    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end




