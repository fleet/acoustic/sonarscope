% Application bas� sur le composant cla_repertoire : (Multi)-S�lection  de
% fichiers dans un ou plusieurs r�pertoires. Mode Browser simple pour
% entrer uniquement un nom de fichier
%
% Syntax
%   a = cla_repertoire
%
% PROPERTY NAME :
%  'GeometryType'   : num�rique (pointeur d'occurrence de Geo dans le nom des fichiers)
%  'DataType'       : num�rique (pointeur d'occurrence de type de Data dans le nom des fichiers)
%  'StrIncluse'     : Occurrence n�cessaire dans le nom des fichiers
%  'StrExclue'      : Filtre d'exclusion du nom des fichiers
%  'RepDef'         : R�pertoire par d�faut du browser
%  'ExtensionFiles' : Extension des fichiers et libell� du filtre.
%  'LabelTitle'     : Titre de la figure
%  'EnableSelection': Affichage et s�lection via un simple browser de
%                     fichiers
%  'MultiSelection' : Autorise la multi-s�lection
%  'FileNameVisible': Visibilit� du Champ FileName 
%  'Entete'         : Propri�t�s de l'ent�te
%  'flagValid'      : Indicateur de sortie par l'action OK ou Cancel.
%  'defFilename'	: Nom du fichier affich� par d�faut
%  'lastDir'        : Dernier r�pertoire parcouru
%  'listeFic'       : Liste des fichiers s�lectionn�s.
%
% Output Arguments
%   a : une instance de cla_repertoire
%
% Examples
%   a = cla_repertoire
%   a = editobj(a)
%
%   a = cla_repertoire('RepDefaut', 'C:\Temp\Testcla_repertoire', 'FileNameVisible', 0, 'ExtensionFiles', {'*.m', 'm', '*.ers', 'ers'}, 'EnableSelection', 0);
%   a = cla_repertoire('RepDefaut', 'C:\Temp\Testcla_repertoire', 'ExtensionFiles', {'*.m', 'm', '*.ers', 'ers', '*.*', '*'}, 'EnableSelection', 1);
%   a = cla_repertoire('RepDefaut', 'C:\Temp\Testcla_repertoire', 'ExtensionFiles', {'*.m', 'm', '*.ers', 'ers', '*.*', '*', 'All filters', 'xxx'}, 'EnableSelection', 1);
%   a = editobj(a)
%
% See also Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = cla_repertoire(varargin)

[varargin, this.GeometryType]    = getPropertyValue(varargin, 'GeometryType',    1);
[varargin, this.DataType]        = getPropertyValue(varargin, 'DataType',        1);
[varargin, this.StrIncluse]      = getPropertyValue(varargin, 'ChaineIncluse',   '');
[varargin, this.StrExclue]       = getPropertyValue(varargin, 'ChaineExclue',    '');
[varargin, this.RepDef]          = getPropertyValue(varargin, 'RepDefaut',       pwd);
[varargin, filtres]              = getPropertyValue(varargin, 'ExtensionFiles',  {'*.m', 'm'});
[varargin, this.LabelTitle]      = getPropertyValue(varargin, 'LabelTitle',      'IFREMER-SonarScope');
[varargin, this.EnableSelection] = getPropertyValue(varargin, 'EnableSelection', 1);
[varargin, this.MultiSelection]  = getPropertyValue(varargin, 'MultiSelection',  1);
[varargin, this.FileNameVisible] = getPropertyValue(varargin, 'FileNameVisible', 0);
[varargin, this.defFilename]     = getPropertyValue(varargin, 'FileDefaut',      '');
[varargin, this.MaxLevel]        = getPropertyValue(varargin, 'MaxLevel',        1);

% Pour voir les fichiers quelque soit la casse de l'extension.
filtres(2:2:end) = lower(filtres(2:2:end));
this.ExtFiles    = filtres;
this.flagValid   = 0;
this.lastDir     = '';
this.listeFic    = {};
this.ihm         = [];

%% Cr�ation de la classe

this = class(this, 'cla_repertoire');

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Transmission des param�tres passes � la methode set

this = set(this, varargin{:});
