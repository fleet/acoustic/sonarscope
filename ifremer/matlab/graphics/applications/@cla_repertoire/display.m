% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : Instance de la classe cla_repertoire
%
% Examples
%   a = cla_repertoire;
%   display(a);
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
