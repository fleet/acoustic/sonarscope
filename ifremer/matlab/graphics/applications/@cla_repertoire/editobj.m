% Interface-Homme-Machine (IHM) d'une instance
% 
% Syntax
%   b = editobj(a)
%
% Input Arguments
%   a : instance de cla_repertoire
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : instance de cla_repertoire
% 
% Remarks : L'instance n'est modifiee que si l'utilisateur est sorti de l'application par le bouton OK
%
% Examples
%   a = cla_repertoire
%   [a, ret] = editobj(a)
%   val = get(a, 'value');
%
% See also Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = editobj(this)

%% Construction de l'IHM

this = construire_ihm(this);

h0 = this.ihm.figure;

%% Boucle d'attente

retour = 0;

UserData = get(h0, 'UserData');
while ishandle(h0)

    % Instruction qui attend  un signal envoye par la fonction uiresume
    uiwait(h0);

    if ishandle(h0)
        % On recup�re le UserData qu'un callback vient de mettre a jour
        UserData = get(h0, 'UserData');

        % On teste si le reveil de la boucle d'attente provient du bouton OK,
        % ce qui veut dire que l'on veut detruire la fenetre et sortir
        Exit = handle2var('Exit', h0);
        if ~isempty(Exit)
            retour = 1;
            delete(h0)
            break
        end
    end
end

%% Sauvegarde de l'instance

InstanceSauvee = getFromUserData('InstanceSauvee', UserData);
if ~isempty(InstanceSauvee)
    this = InstanceSauvee;
end

%% Sortie

switch nargout
    case 0 % On modifie automatiquement l'instance passee
        assignin('caller', inputname(1), this);
    case 1 % On passe l'instance a l'argument de sortie
        varargout{1} = this;
    case 2 % On passe l'instance a l'argument de sortie + flag de validation
        varargout{1} = this;
        varargout{2} = retour;
end
