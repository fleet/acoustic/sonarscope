% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(a, ... )
% 
% Name-only Arguments
%   Format : 
%
% Output Arguments
%    this : une instance de cla_repertoire
%
% Examples
%   a = cla_repertoire
%   a = editobj(a)
%   lastDir = get(a, 'lastDir')
%
% 
% See also cla_repertoire
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};
for i=1:length(varargin)
    switch varargin{i}
        case {'RepDef'; 'RepDefaut'}
            varargout{end+1} = this.RepDef;  %#ok<AGROW>
        case 'FileDef'
            varargout{end+1} = this.defFilename;   %#ok<AGROW>
        case 'Entete'
            varargout{end+1} = this.Entete;   %#ok<AGROW>
        case 'ExtFiles'
            varargout{end+1} = this.ExtFiles;   %#ok<AGROW>
        case 'listeFic'
            varargout{end+1} = this.listeFic;   %#ok<AGROW>
        case 'flagValid'
            varargout{end+1} = this.flagValid;   %#ok<AGROW>
        case 'lastDir'
            varargout{end+1} = this.lastDir;   %#ok<AGROW>
        case 'GeometryType'
            varargout{end+1} = this.GeometryType;   %#ok<AGROW>
        case 'DataType'
            varargout{end+1} = this.DataType;   %#ok<AGROW>
        case 'StrIncluse'
            varargout{end+1} = this.StrIncluse;   %#ok<AGROW>
        case 'StrExclue'
            varargout{end+1} = this.StrExclue;   %#ok<AGROW>
        case 'defFileName'
            varargout{end+1} = this.defFileName;   %#ok<AGROW>
        otherwise
            str = sprintf('cla_repertoire : PropertyName invalide %s', varargin{i});
            my_warndlg(str, 1);
    end
end
