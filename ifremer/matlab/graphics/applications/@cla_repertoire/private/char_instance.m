% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de cla_repertoire
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cla_repertoire Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

% ----------
% Traitement

str{end+1} = sprintf('RepDefaut    <-> %s',  this.RepDef);
str{end+1} = sprintf('defFilename  <-> %s',  this.defFilename);
for i=1:2:numel(this.ExtFiles)
    if i==1
        str{end+1} = sprintf('ExtFiles     <-> %s',  cell2str(this.ExtFiles(1))); %#ok<AGROW>
    else
        str{end+1} = sprintf('             <-> %s',  cell2str(this.ExtFiles(i))); %#ok<AGROW>
    end
end
% str{end+1} = sprintf('Entete       <-> %s',  this.Entete);
str{end+1} = sprintf('lastDir      <-> %s',  this.lastDir);
str{end+1} = sprintf('flagValid    --> %s',  num2str(this.flagValid));
if isa(this.listeFic, 'cell')
    for i=1:numel(this.listeFic)
        if i==1
            str{end+1} = sprintf('listeFic     --> %s',  cell2str(this.listeFic(1))); %#ok<AGROW>
        else
            str{end+1} = sprintf('             --> %s',  cell2str(this.listeFic(i))); %#ok<AGROW>
        end
    end
else
    str{end+1} = sprintf('listeFic     --> %s',  this.listeFic);
end

str{end+1} = sprintf('StrIncluse   <-> %s',  this.StrIncluse);
str{end+1} = sprintf('StrExclue    <-> %s',  this.StrExclue);

if this.EnableSelection
    str{end+1} = sprintf('GeometryType <-> %d', this.GeometryType);
    strGeometryType = cl_image.strGeometryType;
    for i=1:length(strGeometryType)
        if i == this.GeometryType
            str{end+1} = sprintf('        {%d <--> %s}', i, strGeometryType{i}); %#ok
        else
            str{end+1} = sprintf('         %d <--> %s', i, strGeometryType{i}); %#ok
        end
    end

    str{end+1} = sprintf('DataType     <-> %d', this.DataType);
    strDataType = cl_image.strDataType;
    for i=1:length(strDataType)
        if i == this.DataType
            str{end+1} = sprintf('        {%d <--> %s}', i, strDataType{i}); %#ok
        else
            str{end+1} = sprintf('         %d <--> %s', i, strDataType{i}); %#ok
        end
    end
end

%% Concatenation en une chaine

str = cell2str(str);
