% Creation de l'IHM d'une instance
%
% Syntax
%   b = construire_ihm(a)
%
% Input Arguments
%   a : Une instance de cla_repertoire
%
% Output Arguments
%   b : Instance de cla_repertoire modifiee
%
% See also cla_carto Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation de la fenetre

this = creer_figure(this);

%% Cr�ation des panels

this = creer_panel_global(this);

%% C�ation des filtres customis�s du browser de fichiers.

this = creer_filtres_JBrowser(this);

%% Sauvegarde de l'objet dans le UserData de la figure

obj2handle(this, this.ihm.figure);
