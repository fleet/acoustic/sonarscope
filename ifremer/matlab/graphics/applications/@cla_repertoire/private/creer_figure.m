% Creation de l'IHM d'une instance
% 
% Syntax
%   b = creer_figure(a)
%
% Input Arguments
%   a : Une instance de cla_repertoire
%
% Output Arguments
%   b : Instance de cla_repertoire modifiee
%
% See also cla_carto Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = creer_figure(this)

if this.EnableSelection
    Largeur = 950;
    Hauteur = 550;
else
    Largeur = 600;
    Hauteur = 400;
end

this.ihm.figure = figure (...
    'NumberTitle',  'off', ...
    'name',         this.LabelTitle, ...
    'Color',        [0.8 0.8 0.8], ...
    'Position',     centrageFig(Largeur, Hauteur), ...
    'tag',         'mainFigure', ...
    'WindowStyle', 'modal', ...
    'menubar',     'none');
