% Creation de l'IHM d'une instance
% 
% Syntax
%   b = creer_filtres_JBrowser(a)
%
% Input Arguments
%   a : Une instance de cla_repertoire
%
% Output Arguments
%   b : Instance de cla_repertoire modifiee
%
% See also cla_carto Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = creer_filtres_JBrowser(this)

%% Gestion des filtres

GCF = gcf;
if this.EnableSelection
    hdlGeometryTypeFilter = findobj(GCF, 'Tag', 'AncreGeoTypeVal');
    hdlDataTypeFilter     = findobj(GCF, 'Tag', 'AncreDataTypeVal');
    hdlStrContenant       = findobj(GCF, 'Tag', 'AncreContenantVal');
    hdlStrExcluant        = findobj(GCF, 'Tag', 'AncreExcluantVal');
    
    strGeometryType = get(hdlGeometryTypeFilter, 'String');
    GeometryType = this.GeometryType;
    strGeoFiltre = strGeometryType(GeometryType,:);

    strDataType = get(hdlDataTypeFilter, 'String');
    DataType = this.DataType;
    strDataFiltre = strDataType(DataType,:);
    
    if strcmpi(deblank(strGeoFiltre), 'Unknown')
        strGeoFiltre = [];
    else
        strGeoFiltre = ['_' strGeoFiltre];
    end
    if strcmpi(deblank(strDataFiltre), 'Unknown')
        strDataFiltre = [];
    else
        strDataFiltre = ['_' strDataFiltre];            
    end

    strContenantVal = get(hdlStrContenant, 'String');
    strExcluantVal  = get(hdlStrExcluant,  'String');
    % Pour traiter le cas de cha�ne vide sous forme de filtres JAVA.
    if isempty(strContenantVal)
        strContenantVal = [];
    end
    if isempty(strExcluantVal)
        strExcluantVal = [];
    end
else
    strGeoFiltre    = [];
    strDataFiltre   = [];
    strContenantVal = [];
    strExcluantVal  = [];
end

h = findobj(GCF, 'tag', 'Browser_clc_browser');
javaComposant = get(h, 'JavaComponent');

%Inhibition du filtre par d�faut :
javaComposant.setAcceptAllFileFilterUsed(0);
if ~isempty(this.ExtFiles)
    k2 = 1;
    for k=1:(length(this.ExtFiles)/2)
        myFiltreDef = utilFunction.filtreFileChooser2(javaComposant, this.ExtFiles(k2), ...
            this.ExtFiles(k2+1), strGeoFiltre, strDataFiltre, strContenantVal, strExcluantVal);
        javaComposant.addChoosableFileFilter(myFiltreDef);
        javaComposant.setFileFilter(myFiltreDef);
        k2 = k2 + 2;
    end
end
