% Creation du panel BtnSelection
% 
% this = creer_panel_BtnSelection(this)
%
% Input Arguments
%    this : instance de cla_repertoire
%
% Output Arguments
%    this : instance de cla_repertoire
%
% See also cla_repertoire Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = creer_panel_BtnSelection(this)

ihm = {};

if this.MultiSelection == 1
    multiSelectionFlag = 'on';
else
    multiSelectionFlag = 'off';
end

%% Ancre du bouton Selection One file

ihm{end+1}.Lig         = 2;
ihm{end}.Col           = 2:3;
ihm{end}.Style         = 'pushbutton';
ihm{end}.Parent		   = 'AncrePanelBtnSelection';
ihm{end}.Tag		   = 'AncreBtnMoveRight';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgMoveRight'')';
nomFic          = getNomFicDatabase('MoveRight.jpg');
Icon            = iconizeFic(nomFic);
Icon(Icon==255) = .8*255;
ihm{end}.CData         = Icon;

%% Ancre du bouton Selection All file

ihm{end+1}.Lig    = 3;
ihm{end}.Col      = 2:3;
ihm{end}.Style    = 'pushbutton';
ihm{end}.Parent	  = 'AncrePanelBtnSelection';
ihm{end}.Tag	  = 'AncreBtnMoveRightAll';
ihm{end}.Enable	  = multiSelectionFlag;
ihm{end}.Callback = 'callback(cla_repertoire([]), ''MsgMoveRightAll'')';
nomFic            = getNomFicDatabase('MoveRightAll.jpg');
Icon              = iconizeFic(nomFic);
Icon(Icon==255)   = .8*255;
ihm{end}.CData    = Icon;

%% Ancre du bouton de déselection un file

ihm{end+1}.Lig    = 4;
ihm{end}.Col      = 2:3;
ihm{end}.Style    = 'pushbutton';
ihm{end}.Parent	  = 'AncrePanelBtnSelection';
ihm{end}.Tag	  = 'AncreBtnMoveLeft';
ihm{end}.Callback = 'callback(cla_repertoire([]), ''MsgMoveLeft'')';
nomFic            = getNomFicDatabase('MoveLeft.jpg');
Icon              = iconizeFic(nomFic);
Icon(Icon==255)   = .8*255;
ihm{end}.CData    = Icon;

%% Ancre du bouton Selection All file

ihm{end+1}.Lig    = 5;
ihm{end}.Col      = 2:3;
ihm{end}.Style    = 'pushbutton';
ihm{end}.Parent	  = 'AncrePanelBtnSelection';
ihm{end}.Tag	  = 'AncreBtnMoveLeftAll';
ihm{end}.Enable	  = multiSelectionFlag;
ihm{end}.Callback = 'callback(cla_repertoire([]), ''MsgMoveLeftAll'')';
nomFic            = getNomFicDatabase('MoveLeftAll.jpg');
Icon              = iconizeFic(nomFic);
Icon(Icon==255)   = .8*255;
ihm{end}.CData    = Icon;

%% Ancre du bouton Selection All file

ihm{end+1}.Lig    = 6;
ihm{end}.Col      = 2:3;
ihm{end}.Style    = 'pushbutton';
ihm{end}.Parent	  = 'AncrePanelBtnSelection';
ihm{end}.Tag      = 'AncreBtnMoveSubDirectories';
ihm{end}.Enable   = multiSelectionFlag;
ihm{end}.Tooltip  = Lang('Tous les fichiers des sous-répertoires', 'All files of all sub-directories');
ihm{end}.Callback = 'callback(cla_repertoire([]), ''MsgMoveSubDirectories'')';
nomFic            = getNomFicDatabase('MoveRightAll.jpg');
Icon              = iconizeFic(nomFic);
Icon              = [Icon(:,1:2:end,:) Icon(:,1:2:end,:)];
Icon(Icon==255)   = .8*255;
ihm{end}.CData    = Icon;

%% Création du panel

this.ihm.PanelBtnSelection = cl_panel('nomPanel','AncrePanelBtnSelection');

this.ihm.PanelBtnSelection = set(this.ihm.PanelBtnSelection, 'nbRows',  6);
this.ihm.PanelBtnSelection = set(this.ihm.PanelBtnSelection, 'nbCol',  4);
this.ihm.PanelBtnSelection = set(this.ihm.PanelBtnSelection, 'InsetY', 0.01);
this.ihm.PanelBtnSelection = set(this.ihm.PanelBtnSelection, 'ListeUiComposants', ihm);
