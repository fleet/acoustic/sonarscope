% Creation du panel BtnTri
% 
% this = creer_panel_BtnTri(this)
%
% Input Arguments
%    this : instance de cla_repertoire
%
% Output Arguments
%    this : instance de cla_repertoire
%
% See also cla_repertoire Authors
% Authors : GLU
% -------------------------------------------------------------------------

function this = creer_panel_BtnTri(this)

ihm = {};

if this.MultiSelection == 1
    multiSelectionFlag = 'on';
else
    multiSelectionFlag = 'off';
end

%% Ancre du bouton Tri One file

ihm{end+1}.Lig         = 1;
ihm{end}.Col           = 2:3;
ihm{end}.Style         = 'pushbutton';
ihm{end}.Parent		   = 'AncrePanelBtnTri';
ihm{end}.Tag		   = 'AncreBtnHelp';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgHelp'')';
% nomFic                 = getNomFicDatabase('Orb-info-icon.png');
nomFic                 = getNomFicDatabase('Orb-info-icon.gif');
Icon                   = iconizeFic(nomFic);
Icon(Icon==255)        = .8*255;
ihm{end}.CData         = Icon;

%% Ancre du bouton Tri One file

ihm{end+1}.Lig         = 2;
ihm{end}.Col           = 2:3;
ihm{end}.Style         = 'pushbutton';
ihm{end}.Parent		   = 'AncrePanelBtnTri';
ihm{end}.Tag		   = 'AncreBtnTriUp';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgMoveUp'')';
nomFic                 = getNomFicDatabase('MoveUp.jpg');
Icon                   = iconizeFic(nomFic);
Icon(Icon==255)        = .8*255;
ihm{end}.CData         = Icon;

%% Ancre du bouton Tri All file

ihm{end+1}.Lig         = 3;
ihm{end}.Col           = 2:3;
ihm{end}.Style         = 'pushbutton';
ihm{end}.Parent		   = 'AncrePanelBtnTri';
ihm{end}.Tag		   = 'AncreBtnTriTop';
ihm{end}.Enable        = multiSelectionFlag;
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgMoveTop'')';
nomFic                 = getNomFicDatabase('MoveTop.tif');
Icon                   = iconizeFic(nomFic);
Icon(Icon==255)        = .8*255;
ihm{end}.CData         = Icon;

%% Ancre du bouton de d�Tri un file

ihm{end+1}.Lig         = 4;
ihm{end}.Col           = 2:3;
ihm{end}.Style         = 'pushbutton';
ihm{end}.Parent		   = 'AncrePanelBtnTri';
ihm{end}.Tag		   = 'AncreBtnTriDown';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgMoveDown'')';
nomFic                 = getNomFicDatabase('MoveDown.jpg');
Icon                   = iconizeFic(nomFic);
Icon(Icon==255)        = .8*255;
ihm{end}.CData         = Icon;

%% Ancre du bouton Tri All file

ihm{end+1}.Lig         = 5;
ihm{end}.Col           = 2:3;
ihm{end}.Style         = 'pushbutton';
ihm{end}.Parent		   = 'AncrePanelBtnTri';
ihm{end}.Tag		   = 'AncreBtnTriBottom';
ihm{end}.Enable        = multiSelectionFlag;
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgMoveBottom'')';
nomFic                 = getNomFicDatabase('MoveBottom.tif');
Icon                   = iconizeFic(nomFic);
Icon(Icon==255)        = .8*255;
ihm{end}.CData         = Icon;

%% Ancre du bouton Flipud

ihm{end+1}.Lig         = 6;
ihm{end}.Col           = 2:3;
ihm{end}.Style         = 'pushbutton';
ihm{end}.Parent		   = 'AncrePanelBtnTri';
ihm{end}.Tag		   = 'AncreBtnTriFlipud';
ihm{end}.Enable        = multiSelectionFlag;
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgMoveFlipud'')';
ihm{end}.Tooltip       = Lang('Inverser la liste', 'Flip up down the list');
nomFic                 = getNomFicDatabase('MoveUp.tif');
Icon1                   = iconizeFic(nomFic);
nomFic                 = getNomFicDatabase('MoveDown.tif');
Icon2                  = iconizeFic(nomFic);
Icon = [Icon1(:,1:2:end,:) Icon2(:,1:2:end,:)];
Icon(Icon==255)        = .8*255;
ihm{end}.CData         = Icon;

%% Cr�ation du panel

this.ihm.PanelBtnTri = cl_panel('nomPanel','AncrePanelBtnTri');

this.ihm.PanelBtnTri = set(this.ihm.PanelBtnTri, 'nbRows', 6);
this.ihm.PanelBtnTri = set(this.ihm.PanelBtnTri, 'nbCol', 4);
this.ihm.PanelBtnTri = set(this.ihm.PanelBtnTri, 'InsetY', 0.01);

this.ihm.PanelBtnTri = set(this.ihm.PanelBtnTri, 'ListeUiComposants', ihm);
