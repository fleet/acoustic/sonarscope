% Creation du panel FileSelection
%
% this = creer_panel_FileSelection(this)
%
% Input Arguments
%    this : instance de cla_repertoire
%
% Output Arguments
%    this : instance de cla_repertoire
%
% See also cla_repertoire Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = creer_panel_FileSelection(this)

ihm = {};

%% Ancre du titre

ihm{end+1}.Lig      = 1;
ihm{end}.Col        = 1;
ihm{end}.Style      = 'panel';
ihm{end}.Parent     = 'AncrePanelFileSelection';
ihm{end}.Tag        = 'AncrePanelTitreFileSelection';
ihm{end}.Background = [0.6 0.6 0.6];

%% Ancre du composant texte qui affiche les elements import�s

ihm{end+1}.Lig      = 2:8;
ihm{end}.Col        = 1;
ihm{end}.Style      = 'listbox';
ihm{end}.Parent     = 'AncrePanelFileSelection';
ihm{end}.String     = {};
ihm{end}.Tag        = 'AncreListFileSelection';
ihm{end}.Background = 'white';
ihm{end}.max        = 2;

%% Cr�ation du panel

this.ihm.FileSelection = cl_panel('nomPanel','AncrePanelFileSelection');
this.ihm.FileSelection = set(this.ihm.FileSelection, 'ListeUiComposants', ihm);

this = creer_titre_FileSelection(this);
