% Creation du panel Browser
% 
% this = creer_panel_Geo_Data_type(this)
%
% Input Arguments
%    this : instance de cla_repertoire
%
% Output Arguments
%    this : instance de cla_repertoire
%
% See also cla_repertoire Authors
% Authors : GLU
% -------------------------------------------------------------------------

function this = creer_panel_Geo_Data_Type(this)

ihm = {};

ihm{end+1}.Lig         = 3:6;
ihm{end}.Col           = 1:2;
ihm{end}.Style         = 'text';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreGeoTypeTitre';
ihm{end}.String        = Lang('Type de Geometrie', 'Geometry type');

ihm{end+1}.Lig         = 2:8;
ihm{end}.Col           = 3:4;
ihm{end}.Style         = 'popupmenu';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreGeoTypeVal';
ihm{end}.String        = cell2str(cl_image.strGeometryType);
ihm{end}.Value         = this.GeometryType;
%ihm{end}.tooltip       = 'A COMPLETER';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgFilter'')';
ihm{end}.Background    = 'white';

ihm{end+1}.Lig         = 3:6;
ihm{end}.Col           = 5:6;
ihm{end}.Style         = 'text';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreDataTypeTitre';
ihm{end}.String        = Lang('Type de donn�es', 'Data type');

ihm{end+1}.Lig         = 2:8;
ihm{end}.Col           = 7:8;
ihm{end}.Style         = 'popupmenu';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreDataTypeVal';
ihm{end}.String        = cell2str(cl_image.strDataType);
ihm{end}.Value         = this.DataType;
%ihm{end}.tooltip      = 'A COMPLETER';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgFilter'')';
ihm{end}.Background    = 'white';

ihm{end+1}.Lig         = 10:13;
ihm{end}.Col           = 1:2;
ihm{end}.Style         = 'text';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreContenantTitre';
ihm{end}.String        = Lang('Les noms de fichiers contiennent', 'Filenames contain');

ihm{end+1}.Lig         = 9:14;
ihm{end}.Col           = 3:4;
ihm{end}.Style         = 'edit';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreContenantVal';
ihm{end}.String        = this.StrIncluse;
%ihm{end}.tooltip      = 'A COMPLETER';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgFilter'')';
ihm{end}.Background    = 'white';

ihm{end+1}.Lig         = 10:13;
ihm{end}.Col           = 5:6;
ihm{end}.Style         = 'text';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreExcluantTitre';
ihm{end}.String        = Lang('Les noms de fichiers ne contiennent pas', 'Filenames do no contain');

ihm{end+1}.Lig         = 9:14;
ihm{end}.Col           = 7:8;
ihm{end}.Style         = 'edit';
ihm{end}.Parent        = 'AncrePanelGeoDataType';
ihm{end}.Tag           = 'AncreExcluantVal';
ihm{end}.String        = this.StrExclue;
%ihm{end}.tooltip      = 'A COMPLETER';
ihm{end}.Callback      = 'callback(cla_repertoire([]), ''MsgFilter'')';
ihm{end}.Background    = 'white';

this.ihm.GeoDataType = cl_panel('nomPanel', 'AncrePanelGeoDataType');
this.ihm.GeoDataType = set(this.ihm.GeoDataType, 'ListeUiComposants', ihm);

%-----------------------------------------
%permet un acces rapide aux champs du panel pour la MAJ IHM
this.ihm.GeoDataType = set_structure(this.ihm.GeoDataType);
 