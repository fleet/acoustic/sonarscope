% Creation du panel OrigSelection
% 
% this = creer_panel_OrigSelection(this)
%
% Input Arguments
%    this : instance de cla_repertoire
%
% Output Arguments
%    this : instance de cla_repertoire
%
% See also cla_repertoire Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = creer_panel_OrigSelection(this)

ihm = {};

%% Ancre du composant Browser

ihm{end+1}.Lig                      = 1:16;
ihm{end}.Col                        = 1:8;
ihm{end}.Style                      = 'clc_browser';
ihm{end}.Name                       = 'Browser';
ihm{end}.Parent                     = 'AncrePanelOrigSelection';
ihm{end}.UserName                   = 'cla_repertoire';
ihm{end}.UserCb                     = 'callback';

ihm{end}.ControlButtonsAreShown     = 0;
ihm{end}.AcceptAllFileFilterUsed    = 1;
ihm{end}.MultiSelectionEnabled      = this.MultiSelection;

if ~isempty(this.lastDir)
    ihm{end}.currentDir             = this.lastDir;
else
    ihm{end}.currentDir             = this.RepDef;
end
ihm{end}.defFilename                = this.defFilename;
ihm{end}.labelParcourirVisible      = 0;
ihm{end}.FileNameVisible            = this.FileNameVisible;

this.ihm.OrigSelection = cl_panel('nomPanel','AncrePanelOrigSelection');
this.ihm.OrigSelection = set(this.ihm.OrigSelection, 'ListeUiComposants', ihm);

