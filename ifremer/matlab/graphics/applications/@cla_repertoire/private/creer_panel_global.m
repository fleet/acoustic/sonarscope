% Creation des panels principaux de l'IHM d'une instance
%
% Syntax
%   b = creer_panel_global(a)
%
% Input Arguments
%   a : instance de la classe cla_repertoire
%
% Output Arguments
%   b : instance de la classe cla_repertoire
%
% See also cla_carto Authors
% Authors : LUR
% ----------------------------------------------------------------------------

function this = creer_panel_global (this)

ihm = [];

%% Ancre du composant Panel de selection source

if this.EnableSelection % Si l'application n'est pas en mode Browser seul
    nbColBrowser = 5;
else
    nbColBrowser = 12;
end
ihm{end+1}.Lig = 1:11;
ihm{end}.Col   = 1:nbColBrowser;
ihm{end}.Style = 'panel';
ihm{end}.Tag   = 'AncrePanelOrigSelection';

%% Ancre du composant Panel de Boutons de s�lection

if this.EnableSelection % Si l'application n'est pas en mode Browser seul
    %% Ancre du filtre de types de Geometrie et de Data.
    
    ihm{end+1}.Lig       = 12:13;
    ihm{end}.Col         = 1:12;
    ihm{end}.Style       = 'panel';
    ihm{end}.Tag         = 'AncrePanelGeoDataType';
    ihm{end}.border      = 1;
    ihm{end}.borderColor = [0.8 0.8 0.8];

    %% Affichage des boutons de s�lection
    
    ihm{end+1}.Lig = 1:11;
    ihm{end}.Col   = 6:6;
    ihm{end}.Style = 'panel';
    ihm{end}.Tag   = 'AncrePanelBtnSelection';
    
    %% Ancre du composant Panel de selection source

    ihm{end+1}.Lig = 1:11;
    ihm{end}.Col   = 7:11;
    ihm{end}.Style = 'panel';
    ihm{end}.Tag   = 'AncrePanelFileSelection';

    %% Ancre du composant Panel de Boutons de s�lection

    ihm{end+1}.Lig = 1:11;
    ihm{end}.Col   = 12:12;
    ihm{end}.Style = 'panel';
    ihm{end}.Tag   = 'AncrePanelBtnTri';
end

%% Ancre du composant OK

if this.EnableSelection % Si l'application n'est pas en mode Browser seul
    nbColBtnOK     = 3;
    nbColBtnCancel = 6;
    nbLigBtn       = 14;
else
    nbColBtnOK     = 6;
    nbColBtnCancel = 12;
    nbLigBtn       = 12;
end

ihm{end+1}.Lig    = nbLigBtn;
ihm{end}.Col      = 1:nbColBtnOK;
ihm{end}.Style    = 'pushbutton';
ihm{end}.Tag	  = 'AncreBtnOk';
ihm{end}.String   = 'Ok';
ihm{end}.Callback = 'callback(cla_repertoire([]), ''OK'')';

%% Ancre du bouton Cancel

ihm{end+1}.Lig    = nbLigBtn;
ihm{end}.Col      = nbColBtnOK+1:nbColBtnCancel;
ihm{end}.Style    = 'pushbutton';
ihm{end}.Tag	  = 'AncreBtnCancel';
ihm{end}.String   = Lang('TODO','Cancel');
ihm{end}.Callback = 'callback(cla_repertoire([]), ''Cancel'')';

%% Ancre du bouton Load From file

if this.EnableSelection % Si l'application n'est pas en mode Browser seul
    ihm{end+1}.Lig    = nbLigBtn;
    ihm{end}.Col      = 7:9;
    ihm{end}.Style    = 'pushbutton';
    ihm{end}.Tag	  = 'AncreBtnLoadFile';
    ihm{end}.String   = Lang('Liste de fichiers', 'Load File List');
    ihm{end}.Callback = 'callback(cla_repertoire([]), ''LoadFromFile'')';

    % Ancre du bouton Save in File
    
    ihm{end+1}.Lig    = nbLigBtn;
    ihm{end}.Col      = 10:12;
    ihm{end}.Style    = 'pushbutton';
    ihm{end}.Tag	  = 'AncreBtnSaveFile';
    ihm{end}.String   = Lang('Sauver la liste des fichiers', 'Save File List');
    ihm{end}.Callback = 'callback(cla_repertoire([]), ''SaveInFile'')';
end

%% Cr�ation du panel

this.ihm.globale = cl_panel([]);
this.ihm.globale = set_tab_uiComposant(this.ihm.globale, ihm);

%% Cr�ation des sous-panels

this = creer_panel_OrigSelection(this);
if this.EnableSelection % Si l'application n'est pas en mode Browser seul
    this = creer_panel_Geo_Data_Type(this);
    this = creer_panel_FileSelection(this);
    this = creer_panel_BtnSelection(this);
    this = creer_panel_BtnTri(this);
end

