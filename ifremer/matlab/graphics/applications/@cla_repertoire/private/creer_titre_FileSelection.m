% Creation du panel titre_FileSelection
%
% this=creer_titre_FileSelection(this)
%
% Input Arguments
%    this : instance de cla_repertoire
%
% Output Arguments
%    this : instance de cla_repertoire
%
% See also cla_carto Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = creer_titre_FileSelection(this)

ihm = {};

ihm{end+1}.Lig      = 2:3;
ihm{end}.Col        = 1;
ihm{end}.Style      = 'text';
ihm{end}.Parent     = 'AncrePanelTitreFileSelection';
ihm{end}.tag        = 'titre_FileSelection';
ihm{end}.String     = Lang('Fichiers sélectionnés','Selected files');
ihm{end}.Background = [0.6 0.6 0.6];

this.ihm.FileSelectionTitre = cl_panel('nomPanel', 'AncrePanelTitreFileSelection');
this.ihm.FileSelectionTitre = set(this.ihm.FileSelectionTitre, 'ListeUiComposants', ihm);
