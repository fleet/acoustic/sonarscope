% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set (a, ...)
%
% Input Arguments:
%   a : instance de cla_repertoire
%
% PROPERTY NAME :
%   nom1         : Texte affiche en titre
%   value2       : Valeur initiale du parametre du composant edit_value
%
% Examples
%   a = cla_repertoire
%   a = set(a, 'Format', 5)
%
% See also cla_repertoire cla_repertoire/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, this.GeometryType]       = getPropertyValue(varargin, 'GeometryType', this.GeometryType);
[varargin, this.DataType]           = getPropertyValue(varargin, 'DataType', this.DataType);
[varargin, this.StrIncluse]         = getPropertyValue(varargin, 'ChaineIncluse', this.StrIncluse);
[varargin, this.StrExclue]          = getPropertyValue(varargin, 'ChaineExclue', this.StrExclue);
[varargin, this.RepDef]             = getPropertyValue(varargin, 'RepDefaut', this.RepDef);
[varargin, this.ExtFiles]           = getPropertyValue(varargin, 'ExtensionFiles', this.ExtFiles);
[varargin, this.MultiSelection]     = getPropertyValue(varargin, 'MultiSelection', this.MultiSelection);
[varargin, this.FileNameVisible]    = getPropertyValue(varargin, 'FileNameVisible', this.FileNameVisible);
[varargin, this.EnableSelection]    = getPropertyValue(varargin, 'EnableSelection', this.EnableSelection);
[varargin, this.LabelTitle]         = getPropertyValue(varargin, 'LabelTitle', this.LabelTitle);
[varargin, this.defFilename]        = getPropertyValue(varargin, 'FileDefaut', this.defFilename); %#ok<ASGLU>

if nargout == 0          % On modifie automatiquement l'instance passee
    assignin('caller', inputname(1), this);
else
    varargout{1} = this; % On passe l'instance a l'argument de sortie
end
