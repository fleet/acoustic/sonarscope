% Activation du zoom
%
% Syntax
%   a = activer(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments 
%   a : instance de cl_virtual_zoom
%
% Examples
%    figure; plot(1:100, rand([1 100]) * 100)
%    z = cl_virtual_zoom('hFig', gcf, 'hAxes', gca, 'asserv', [1])
%    set(z, 'cbName', 'cb', 'cbObj', 'cl_virtual_zoom')
%    activer(z)
%    % effectuer un zoom souris (definir une zone), resultat a la console
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = activer(this)

% --------------------------------------------------------------------------
% Verifie que les conditions d application du zoom sont reunies :
% - handle  sur figure valide
% - handles sur axes valides
% - asservissement initialise

if ishandle(this.hFig) && ~isempty(this.asserv) && ~isempty(this.cbName) && any(ishandle(this.hAxes))

    % ------------------------------------------------------------------------
    % Verifie que la definition de la callback est viable (pour une
    % fonction comme pour une methode).
    % Si invalide, aucune action, retour systeme appelant

    if isempty(this.cbObj)
        s = which(this.cbName);
    else
        s = which([this.cbObj, '.', this.cbName]);
    end
    if strcmp(s, '')
        my_warndlg('cl_virtual_zoom/active : Invalid callback definition', 1);
        return
    end

    %% Construction de la callback

%     cb = [];
%     cb{end+1} = 'eval(''';
%     cb{end+1} = this.cbName;
%     cb{end+1} = '( ';
%     if ~isempty(this.cbObj)
%         cb{end+1} = this.cbObj;
%         cb{end+1} = ', ';
%     end
%     cb{end+1} = ['''''', this.msg, ''''''];
%     cb{end+1} = ', ';
%     cb{end+1} = 'overobj(''''axes'''')';
%     cb{end+1} = ', ';
%     cb{end+1} = 'rbbox';
%     cb{end+1} = ');';
%     cb{end+1} = ''');';
%     cbOld = strcat(cb{:})
%  % R�sultat :'eval('callback(cli_image,''zoom'',overobj(''axes''),rbbox);');'
    
% Modif JMA le 24/02/2022
%     cb = @cli_image.callback('zoom', overobj('axes'), rbbox); % Pas concluant
    cb = 'eval(''cli_image.callback(''''zoom'''', overobj(''''axes''''), rbbox);'');';
    
    % ------------------------------------------------------------------------
    % Mise en place de la callback :
    % - sur chaque axe
    % - sur chaque objet compris de l axe courant (sinon l objet telle qu
    %   une courbe defini une zone sans zoom dans le graphique)
    % - sauvegarde du contenu de la call back modifiee pour remise en place

    this.lstCb = [];
    [m, n] = size(this.hAxes);

    for i=1:m
        for j=1:n
            scb.hCb = this.hAxes(i,j);
            scb.cCb = get(scb.hCb, 'ButtonDownFcn');
            this.lstCb{end+1} = scb;

            set(this.hAxes(i, j), 'ButtonDownFcn', cb);

            listeHandle = get(this.hAxes(i, j), 'Children');
            [o, p] = size(listeHandle);

            for k=1:o
                for l=1:p
                    scb.hCb = listeHandle(o,p);
                    scb.cCb = get(scb.hCb, 'ButtonDownFcn');
                    this.lstCb{end+1} = scb;
                    if ishandle(listeHandle(k, l))
                        set(listeHandle(k, l), 'ButtonDownFcn', cb);
                    end
                end
            end
        end
    end

    %% Positionne le flag d'activit�

    this.isOn = 1;

else
    my_warndlg('cl_virtual_zoom/activer : Can''t activate zoom, invalid attributes', 1);
end
