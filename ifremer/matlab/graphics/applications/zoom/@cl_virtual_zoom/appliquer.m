% Application du zoom (doit etre appele par la call back)
%
% this = appliquer(this, hAxe, bbox)
% 
% Input Arguments
%   this : instance de cl_virtual_zoom
%   hAxe : handle de l axe sur lequel a eu lieu le zoom
%   bbox : rectangle (en pixels) definissant le zoom
%
% Input Arguments
%   this : instance de cl_virtual_zoom initialisee
%
% Examples
%    z = cl_virtual_zoom('hFig', figure, 'hAxes', axes, 'asserv', [1]);
%    set(z, 'cbName', 'cb', 'cbObj', 'cl_virtual_zoom');
%    activer(z)
%    % effectuer un zoom souris (definir une zone), resultat a la console
%
% See also cl_virtual_zoom
% Authors : DCF
% ----------------------------------------------------------------------------

function this = appliquer(this, hAxe, bbox, varargin)

%% Test la validit� des arguments

if ~ishandle(hAxe)
    my_warndlg('cl_virtual_zoom/appliquer : Invalid handle');
elseif isempty(this.asserv)
    my_warndlg('cl_virtual_zoom/appliquer : Asservissement not defined');
elseif ~isAGraphicElement(hAxe, 'axes')
    my_warndlg('cl_virtual_zoom/appliquer : C''est pas un axe');
else
    if ~this.isOn % Rajout� par JMA le 26/12/12 pour tenter de supprimer des zooms non attendus
        return
    end
    
    %% Lecture des echelles de l'axe

    echelle = axis(hAxe);

    % -----------------------------------------
    % Lecture des dimensions de l axe en pixels

    units    = get(hAxe, 'Units');
    set(hAxe, 'Units', 'pixels');
    position = get(hAxe, 'Position');
    set(hAxe, 'Units', units);

    if strcmp(get(hAxe, 'YDir'), 'reverse')
        % RAJOUT JMA car Pb de zoom dans le cas ou la visu est en
        % "reverse (axis xy)
        bbox(2) =  position(4) - bbox(2) + 2 * position(2);
        if bbox(4) ~= 0
            bbox(2) =  bbox(2) - bbox(4);
        end
    end
    if strcmp(get(hAxe, 'XDir'), 'reverse')
        % RAJOUT JMA car Pb de zoom dans le cas ou la visu est en
        % "reverse (axis xy)
        bbox(1) =  position(3) - bbox(1) + 2 * position(1);
        if bbox(3) ~= 0
            bbox(1) =  bbox(1) - bbox(3);
        end
    end

    % --------------------------------
    % Lecture de l action souris faite
    % - normal   : bouton gauche
    % - extended : bouton du milieu
    % - alt      : bouton de droite
    % - open     : double click

    userAction = get(get(hAxe, 'Parent'), 'SelectionType');

    % -----------------------------------------------------
    % Choix de l action :
    % - action == In
    %   - bt gche  : si action == In  => zoom in, sinon out
    %   - bt drte  : si action == Out => zoom out, sinon in
    %   - bt mili  : rien
    %   - dbl clic : retour sans aucun zoom
    % - action == Out
    %   - bt gche  : si action == In  => zoom out, sinon in
    %   - bt drte  : si action == Out => zoom in, sinon out
    %   - bt mili  : rien
    %   - dbl clic : retour sans aucun zoom

    switch userAction
        case 'normal'
            if strcmp(this.action, 'In')
                this = zoom_in(this, hAxe, bbox, echelle, position, varargin{:});
            else
                this = zoom_out(this, hAxe, echelle, varargin{:});
            end
        case 'extend'
        case 'alt'
            if strcmp(this.action, 'Out')
                this = zoom_in(this, hAxe, bbox, echelle, position, varargin{:});
            else
                this = zoom_out(this, hAxe, echelle, varargin{:});
            end
        case 'open'
            this = no_zoom(this);
        otherwise
            my_warndlg(['cl_virtual_zoom/appliquer : Unknown mouse action : ', action]);
    end
end
