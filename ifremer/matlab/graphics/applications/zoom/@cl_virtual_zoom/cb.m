% callback exemple de cl_virtual_zoom, cas d une methode
%
% Syntax
%   cb(a, ident, hAxe, bbox)
%
% Input Arguments
%   a     : instance de cl_virtual_zoom
%   Ident : identificateur de la call back == 'zoom'
%   hAxe  : handle de l axe ou se realise le zoom (par l utilisateur)
%   bbox  : delimitation du rectangle, en pixels, defini lors du zoom
%
% Examples
%   z = cl_virtual_zoom('hFig', figure, 'hAxes', axes, 'asserv', [1]);
%   set(z, 'cbName', 'cb', 'cbObj', 'cl_virtual_zoom');
%   activer(z)
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function cb(this, ~, hAxe, bbox)

% -------------------------------------
% Precise la realisation de la callback

disp('cl_virtual_zoom::cb');

% ----------------------------------------------------------
% Impose l asservissement a ce niveau (cas de demonstration)
%
% asserv == 0 : pas de redimensionnement
% asserv == 1 : redimensionnement en x seulement
% asserv == 2 : redimensionnement en y seulement
% asserv == 3 : redimensionnement en x et en y

set(this, 'hFig', gcf, 'hAxes', gca, 'asserv', 3, 'action', 'In');

% ----------------
% Applique le zoom

appliquer(this, hAxe, bbox);
