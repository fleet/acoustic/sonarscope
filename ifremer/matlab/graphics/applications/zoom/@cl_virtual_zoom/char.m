% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char(a) 
%
% Input Arguments
%   a : instance ou tableau d instance de cl_virtual_zoom
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%    char(cl_virtual_zoom)
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.3 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    % -------------------------
    % Intialisation des locales
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);
        
    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'char', lasterr);
% Maintenance Auto : end
