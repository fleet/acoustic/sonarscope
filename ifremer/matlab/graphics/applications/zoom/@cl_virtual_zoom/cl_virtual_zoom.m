% Constructeur de cl_virtual_zoom
%
% Syntax
%   a = cl_virtual_zoom(...)
% 
% Name-Value Pair Arguments
%    hFig   : handle de la figure contenant les axes
%    hAxes  : handle (tableau) des axes permettant le zoom
%    asserv : tableau descriptif de l asservissement entre les axes
%    cbName : definition du nom de la call-back
%    cbObj  : definition de l instance de la call-back
%    action : action du zoom {'In', 'Out'}
%
% Output Arguments 
%    a : instance de cl_virtual_zoom
%
% Remarks : 
%    Cette instance place, au niveau des axes, des call back qui sont 
%    automatiquement supprimees par matlab lors de certaines operations
%    telles que plot. Il faut alors indiquer a l instance de zoom d
%    effectuer une mise a jour des axes
%
%    La call back (fonction) est definie par 4 arguments :
%    - nom de la fonction a appeler
%    - identifiant de la call back == 'zoom'
%    - handle de l axe
%    - rectangle de zoom
%    La call back (methode) est definie par 5 arguments :
%    - nom de la methode a appeler
%    - instance executant la methode
%    - identifiant de la call back == 'zoom'
%    - handle de l axe
%    - rectangle de zoom
%    Cette call back doit appeler la methode 'appliquer' de l'instance de
%    cl_virtual_zoom permettant l application du zoom
%
%    Asservissement : pour nbAxes le nombre d axes, le tableau 
%    d asservissement est de dimensions (nbAxes x nbAxes). Les lignes, comme
%    les colonnes correspondent aux handles des axes (dans le meme ordre)
%    Le contenu de ce tableau est une combinaison binaire de 1 et 2 :
%    1 : permet le redimensionnement en x
%    2 : permet le redimensionnement en y
%    Ainsi, 0 ne permet pas de redimensionnement alors que 3 permet de 
%    redimensionner en x et y.
%    Pour trois graphes, voici un tableau exemple
%       | h1 | h2 | h3 |
%    ---|----|----|----|
%    h1 |  3 |  1 |  2 |
%    ---|----|----|----|
%    h2 |  1 |  3 |  3 |
%    ---|----|----|----|
%    h3 |  2 |  3 |  3 |
%    ---|----|----|----|
%    Lecture : pour une modif sur h1 (en ligne), on redimensionne h1 en x et 
%    y, on redimensionne h2 en x et on redimensionne h3 en y
%    
%    Cette classe calcul tout ce qu il faut pour faire le zoom sans le
%    realiser, ce qui permet de realiser un zoom specifique en utilisant
%    les resultats des calculs.
%
% Examples
%    f = cl_virtual_zoom
%
% See also cl_zoom
% Authors : DCF
% ----------------------------------------------------------------------------

function this = cl_virtual_zoom(varargin)

% --------------------------------------------------------------------------
% D�finition de la structure et initialisation
%
% hFig   : handle de la figure contenant les axes supportant le zoom
% hAxes  : handle des axes ou ce zoom est mis en place
% asserv : description des asservissement entre les axes lors d un zoom
% cbName : nom de methode lors de l'execution de la call back de zoom
% cbObj  : instance de la classe appelee pour execution de cbName
% action : {'In', 'Out'}, action du zoom, entrant ou sortant
% lstCb  : liste de couple call back / handle
% isOn   : 1 si zoom active, 0 sinon
% msg    : message envoye (zoom par defaut)

this.hFig   = [];
this.hAxes  = [];
this.asserv = [];
this.cbName = [];
this.cbObj  = [];
this.action = 'In';
this.lstCb  = [];
this.isOn   = 0;
this.lstCmd = [];
this.msg    = 'zoom';

%% Cr�ation de l'objet

this = class(this, 'cl_virtual_zoom');

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
