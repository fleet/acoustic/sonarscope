% Desactivation du zoom
%
% Syntax
%   a = desactiver(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments 
%    a : instance de cl_virtual_zoom
%
% Examples
%    figure; plot(1:100, rand([1 100]) * 100);
%    z = cl_virtual_zoom('hFig', gcf, 'hAxes', gca, 'asserv', [1]);
%    set(z, 'cbName', 'cb', 'cbObj', 'cl_virtual_zoom');
%    activer(z)
%    % effectuer un zoom souris (definir une zone), resultat a la console
%    desactiver(z)
%    % le zoom n est plus actif
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% -------------------------------------------------------------------------

function this = desactiver(this)

% ----------------------------------------------------------------
% Verifie que les conditions de suppression du zoom sont reunies :
% - handle  sur figure valide
% - handles sur axes valides

if ishandle(this.hFig) && all(ishandle(this.hAxes))

    % -----------------------------------------------------------------
    % Suppression de la callback
    % - sur chaque axe
    % - sur chaque objet compris de l axe courant (sinon l objet telle qu
    %   une courbe defini une zone sans zoom dans le graphique)

    [m, n] = size(this.hAxes);
    for i=1:m
        for j=1:n
            set(this.hAxes(i, j), 'ButtonDownFcn', '');
            listeHandle = get(this.hAxes(i, j), 'Children');

            [o, p] = size(listeHandle);
            for k=1:o
                for l=1:p
                    set(listeHandle(k, l), 'ButtonDownFcn', '');
                end
            end

        end
    end

    %% Remise en place en place des anciennes cb

    for k=1:length(this.lstCb)
        scb  = this.lstCb{k};
        if ishandle(scb.hCb)
            set(scb.hCb, 'ButtonDownFcn', scb.cCb);
        end
    end

    %% Positionne le flag d'activit�

    this.isOn = 0;

else
    my_warndlg('cl_virtual_zoom/desactiver : Can''t desactivate zoom, invalid attributes', 0);
end
