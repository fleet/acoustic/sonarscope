% Affichage des donnees d'une instance de cl_virtual_zoom
%
% display(this)
%
% Input Arguments 
%   this : instance de cl_virtual_zoom
%
% Examples
%   display(cl_virtual_zoom)
%
% See also help cl_virtual_zoom
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)
    
s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
