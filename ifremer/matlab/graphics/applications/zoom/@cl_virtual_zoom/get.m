% Lecture des attributs de l'instance
%
% Syntax
%   [...] = get(a, ...)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Name-only Arguments
%    hFig   : handle de la figure contenant les axes
%    hAxes  : handle (tableau) des axes permettant le zoom
%    asserv : tableau descriptif de l asservissement entre les axes
%    cbName : definition du nom de la call-back
%    cbObj  : definition de l instance de la call-back
%    action : action du zoom {'In', 'Out'}
%    isOn   : 1 : zoom actif, 0 : zoom inactif
%    lstCmd : liste des cmd a appliquer pour effectuer le zoom
%    msg    : message
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    f = cl_virtual_zoom('hFig', figure, 'hAxes', axes, 'asserv', [1]);
%    [hf, ha, a] = get(f, 'hFig', 'hAxes', 'asserv')
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'hFig'
            varargout {end+1} = get_hfig(this);
        case 'hAxes'
            varargout {end+1} = get_haxes(this);
        case 'asserv'
            varargout {end+1} = get_asserv(this);
        case 'isOn'
            varargout {end+1} = get_ison(this);
        case 'action'
            varargout {end+1} = get_action(this);
        case 'lstCmd'
            varargout {end+1} = get_lstcmd(this);
        case 'msg'
            varargout {end+1} = get_msg(this);
        otherwise
            my_warndlg('cl_virtual_zoom/get : Invalid property name', 0);
    end
end
