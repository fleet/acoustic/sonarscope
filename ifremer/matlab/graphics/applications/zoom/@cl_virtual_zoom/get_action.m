% Accesseur en lecture du type de l'action du zoom
%
% Syntax
%   action = get_action(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   action : type d action du zoom {'In', 'Out'}
% 
% Examples
%   z = cl_virtual_zoom;
%   get_action(z)
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: get_action.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function action = get_action(this)

% Maintenance Auto : try
    action = this.action;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'get_action', lasterr);
% Maintenance Auto : end
