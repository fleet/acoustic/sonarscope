% Accesseur en lecture de la matrice d'asservissement
%
% Syntax
%   asserv = get_asserv(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   asserv   : matrice des asservissements des axes
% 
% Examples
%    z = cl_virtual_zoom;
%    get_asserv(z)
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: get_asserv.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function asserv = get_asserv(this)

% Maintenance Auto : try
    asserv = this.asserv;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'get_asserv', lasterr);
% Maintenance Auto : end
