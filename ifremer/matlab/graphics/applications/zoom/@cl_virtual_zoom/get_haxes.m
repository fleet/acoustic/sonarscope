% Accesseur en lecture des handles de la figure
%
% Syntax
%   hAxes = get_haxes(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   hAxes : handle de la figure d'application du zoom
% 
% Examples
%    z = cl_virtual_zoom;
%    get_haxes(z)
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: get_haxes.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function hAxes = get_haxes(this)

% Maintenance Auto : try
    hAxes = this.hAxes;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'get_haxes', lasterr);
% Maintenance Auto : end
