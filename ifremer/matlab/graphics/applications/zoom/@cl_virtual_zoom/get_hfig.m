% Accesseur en lecture du handle de la figure
%
% Syntax
%   hFig = get_hfig(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   hFig : handle de la figure d application du zoom
% 
% Examples
%    z = cl_virtual_zoom;
%    get_hfig(z);
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: get_hfig.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function hFig = get_hfig(this)

% Maintenance Auto : try
    hFig = this.hFig;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'get_hfig', lasterr);
% Maintenance Auto : end
