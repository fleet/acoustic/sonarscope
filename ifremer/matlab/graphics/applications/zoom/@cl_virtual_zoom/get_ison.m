% Accesseur en lecture du flag indiquant la mise en place du zoom
%
% Syntax
%   isOn = get_ison(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   isOn : flag indiquant la mise en place du zoom
% 
% Examples
%    z = cl_virtual_zoom ;
%    get_ison(z) ;
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: get_ison.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function isOn = get_ison(this)

% Maintenance Auto : try
    isOn = this.isOn ;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'get_ison', '') ;
% Maintenance Auto : end
