% Accesseur en lecture de la liste des cmd de realisation du zoom
%
% Syntax
%   lstCmd = get_lstcmd(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   lstCmd : liste des commandes de realisation du zoom
% 
% Examples
%    z = cl_virtual_zoom;
%    get_lstcmd(z)
%
% See also help cl_virtual_zoom
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function lstCmd = get_lstcmd(this)


% Maintenance Auto : try
    lstCmd = this.lstCmd;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'get_lstcmd', lasterr);
% Maintenance Auto : end
