% Accesseur en lecture du message
%
% Syntax
%   msg = get_msg(a)
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   msg   : message
% 
% Examples
%    z = cl_virtual_zoom;
%    get_msg(z);
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: get_msg.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    20/03/2002 - DCF - creation
% ----------------------------------------------------------------------------

function msg = get_msg(this)

% Maintenance Auto : try
    msg = this.msg;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'get_msg', lasterr);
% Maintenance Auto : end
