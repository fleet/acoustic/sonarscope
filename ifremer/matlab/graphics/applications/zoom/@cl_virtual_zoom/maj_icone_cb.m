% Mise a jour des callbacks pour la barre d'icones de la fenetre matlab
%
% maj_icone_cb(a, cbIn, cbOut)
% 
% Les callback deux icones de zoom sont remplacees par les callback
% donnees en argument
%
% Input Arguments
%   a     : instance de cl_virtual_zoom
%   cbIn  : callback pour le Zoom In
%   cbOut : callback pour le Zoom Out
%
% Examples
%    figure; plot(1:100, rand([1 100]) * 100);
%    z = cl_virtual_zoom('hFig', gcf, 'hAxes', gca, 'asserv', [1]);
%    cbIn  = 'disp(''In'');';
%    cbOut = 'disp(''Out'');';
%    maj_icone_cb(z, cbIn, cbOut);
%
%    % Cliquer sur les icones de zoom
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: maj_icone_cb.m,v 1.3 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

function maj_icone_cb(this, cbIn, cbOut)

% ---------------------------------------------------------
% Verifie que les conditions de changement sont correctes :
% - handle  sur figure valide
% - presence d une toolbar dans la figure

if ishandle(this.hFig) && ~strcmp(get(this.hFig, 'toolbar'), 'none')
    
    % ---------------------------
    % Determination de la toolbar
    
    hChildren = allchild(this.hFig);
    hToolBar  = findobj(hChildren, 'Type', 'uitoolbar');
    
    % ------------------------------------------
    % Verification : on a bien trouve la toolbar
    
    if ~isempty(hToolBar)
        
        % ---------------------------------
        % Determination des icones de zoom
        
        hChildren = allchild(hToolBar);
        hZoomOut = findobj(hChildren, 'tooltipstring', 'Zoom Out');
        hZoomIn  = findobj(hChildren, 'tooltipstring', 'Zoom In');
        
        % ------------------------------
        % Impose les nouvelles callbacks
        
        if ~isempty(hZoomOut)
            set(hZoomOut, 'ClickedCallback', cbOut);
        end
        
        if ~isempty(hZoomIn)
            set(hZoomIn, 'ClickedCallback', cbIn);
        end
        
    else
        my_warndlg('cl_virtual_zoom/maj_icone_cb : no toolbar to modify', 0);
    end
    
else
    my_warndlg('cl_virtual_zoom/maj_icone_cb : invalid attributes', 0);
end
