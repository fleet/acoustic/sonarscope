% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)
    
str = [];

str{end+1} = sprintf('hFig    <-> %d', this.hFig);
str{end+1} = sprintf('hAxes   <-> %s', mat2str(this.hAxes));
str{end+1} = sprintf('asserv  <-> %s', mat2str(this.asserv));
str{end+1} = sprintf('isOn    <-- %s', mat2str(this.isOn));
str{end+1} = sprintf('action  <-> %s', this.action);
str{end+1} = sprintf('msg     <-> %s', this.msg);
str{end+1} = sprintf('cbName  <-> %s', this.cbName);
str{end+1} = sprintf('cbObj   <-> %s', this.cbObj);

%% Concatenation en une chaine

str = cell2str(str);
