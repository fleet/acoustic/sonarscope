% Suppression des modifications graphiques dues au zoom
%
% Syntax
%   a = no_zoom(a);
% 
% Input Arguments
%   a : instance de cl_virtual_zoom
% 
% Output Arguments
%   a : instance de cl_virtual_zoom
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: no_zoom.m,v 1.2 2003/07/24 14:50:52 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    13/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function this = no_zoom(this)

% Maintenance Auto : try
    
    % --------------------------
    % Vide la liste de commandes
    
    this.lstCmd = [];
    
    % ----------------------------------------------------------------
    % Pour chaque graph, Construction de la cmd de suppression du zoom
    
    [m, n] = size(this.hAxes); 
    for i = 1:1:m
        for j = 1:1:n
            this.lstCmd {end+1} = {this.hAxes(i, j), 'XLimMode', 'auto', 'YLimMode', 'auto'};
            % set(this.hAxes(i, j), 'XLimMode', 'auto', 'YLimMode', 'auto');
        end
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'no_zoom', lasterr);
% Maintenance Auto : end
