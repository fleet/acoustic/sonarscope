% Zoom entrant
%
% Syntax
%   a = zoom_in(a, hAxe, bbox, echelle, position, ...)
%
% Input Arguments
%   a     : instance de cl_virtual_zoom
%   hAxe     : handle de l axe sur lequel a eu lieu le zoom
%   bbox     : rectangle (en pixels) definissant le zoom
%   echelle  : echelles (min et max) de chaque axe de l'axe
%   position : position en pixel, dans la figure, de l'axe
%
% Name-only Arguments
%   recadrage : Recadrage sur les limites des courbes contenues dans l'axe
%
% Output Arguments
%   a : instance de cl_virtual_zoom
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = zoom_in(this, hAxe, bbox, echelle, position, varargin)

[varargin, recadrage] = getFlag(varargin, 'recadrage'); %#ok<ASGLU>

% ---------------------------------------------------------------------
% Deux types de zoom :
% - click simple sans selection de zone
% - click avec selection de zone
%
% Pour une zone inferieure ou egale a 5 pixels (en x et y), on estime la
% zone a un point

if (bbox(3) <= 5) && (bbox(4) <= 5)
    
    % ------------------------------------------------------------------
    % Zoom par 2 en centrant sur le point : on definit une boite puis on
    % appel de nouveau zoom_in (execute l autre partie du code)
    
    % point xp et yp
    xp = bbox(1);
    yp = bbox(2);
    
    % taille, en pixels, de l axe selon x et y
    lx = position(3);
    ly = position(4);
    
    % Definition d une boite de centre xp et yp
    bbox(1) = xp - round(lx/4);
    bbox(2) = yp - round(ly/4);
    bbox(3) = round(lx/2);
    bbox(4) = round(ly/2);
    
    % Si la boite est assez grande : appel recursif pour zoomer sur la boite
    % sinon aucune action de zoom et avertissement
    
    if (bbox(3) <= 5) && (bbox(4) <= 5)
        my_warndlg('cl_virtual_zoom/zoom_in : Can''t zoom in, box too small');
    else
        if recadrage
            this = zoom_in(this, hAxe, bbox, echelle, position, 'recadrage');
        else
            this = zoom_in(this, hAxe, bbox, echelle, position);
        end
    end
    
else
    
    % ---------------------------------
    % Zoom sur la zone definie par bbox
    
    % bbox : xpmin, ypmin : coin inf gauche de bbox
    %        xpmax, ypmax : coin sup droit de bbox
    xpmin  = bbox(1);
    ypmin  = bbox(2);
    xpmax  = bbox(3) + xpmin;
    ypmax  = bbox(4) + ypmin;
    
    % axe   : xmin, ymin   : coin inf gauche de l axe
    %         xvmin, yvmin : valeurs min selon x et y
    %         xmax, ymax   : coin sup droit de l axe
    %         xvmax, yvmax : valeurs max selon x et y
    xmin = position(1);
    ymin = position(2);
    xmax = xmin + position(3);
    ymax = ymin + position(4);
    
    xvmin = echelle(1);
    yvmin = echelle(3);
    xvmax = echelle(2);
    yvmax = echelle(4);
    
    % nouvelle echelle (de valeurs)
    % nxvmin et nyvmin : valeurs min selon x et y
    % nxvmax et nyvmax : valeurs max selon x et y
    nxvmin = xvmin + (xvmax-xvmin) / (xmax-xmin) * (xpmin-xmin);
    nxvmax = xvmin + (xvmax-xvmin) / (xmax-xmin) * (xpmax-xmin);
    nyvmin = yvmin + (yvmax-yvmin) / (ymax-ymin) * (ypmin-ymin);
    nyvmax = yvmin + (yvmax-yvmin) / (ymax-ymin) * (ypmax-ymin);
    
    if recadrage
        deltax = nxvmax - nxvmin;
        deltay = nyvmax - nyvmin;
        
        % DEBUT AJOUT JMA 01/04/03
        XLim =  get(hAxe, 'XLim');
        %         [DataLim, AxeLim] = getParamZoom('X', 'hAxe', hAxe);
        DataLim = getParamZoom('X', 'hAxe', hAxe);
        if isinf(DataLim(1))
            bordGauche = XLim(1);
        else
            bordGauche = DataLim(1);
        end
        nxvmin = max(nxvmin, bordGauche);
        nxvmax = nxvmin + deltax;
        
        if isinf(DataLim(2))
            bordDroit = XLim(2);
        else
            bordDroit = DataLim(2);
        end
        nxvmax = min(nxvmax, bordDroit);
        nxvmin = nxvmax - deltax;
        
        YLim =  get(hAxe, 'YLim');
        %         [DataLim, AxeLim] = getParamZoom('Y', 'hAxe', hAxe);
        DataLim = getParamZoom('Y', 'hAxe', hAxe);
        if isinf(DataLim(1))
            bordBas = YLim(1);
        else
            bordBas = DataLim(1);
        end
        nyvmin = max(nyvmin, bordBas);
        nyvmax = nyvmin + deltay;
        
        if isinf(DataLim(2))
            bordHaut = YLim(2);
        else
            bordHaut = DataLim(2);
        end
        nyvmax = min(nyvmax, bordHaut);
        nyvmin = nyvmax - deltay;
        
        nxvmin = max(nxvmin, bordGauche);
        nxvmax = min(nxvmax, bordDroit);
        nyvmin = max(nyvmin, bordBas);
        nyvmax = min(nyvmax, bordHaut);
        
        % FIN AJOUT JMA 01/04/03
    end
    
    % ------------------------------------------
    % Determination de la ligne d asservissement
    
    %     ligne  = find(this.hAxes == hAxe);
    ligne  = (this.hAxes == hAxe);
    asserv = this.asserv(ligne, :);
    
    % --------------------------
    % Vide la liste de commandes
    
    this.lstCmd = [];
    
    % ----------------------------------
    % Application de la nouvelle echelle
    
    [m, n] = size(this.hAxes);
    for i=1:m
        for j=1:n
            if bitand(asserv(1,j), 1) && bitand(asserv(1,j), 2)  % redimensionne en x et y
                this.lstCmd{end+1} = {this.hAxes(i, j), ...
                    'XLim', [nxvmin nxvmax], ...
                    'YLim', [nyvmin nyvmax]};
                
            elseif bitand(asserv(1,j), 1)   % redimensionne en x seulement
                this.lstCmd{end+1} = {this.hAxes(i, j), ...
                    'XLim', [nxvmin nxvmax]};
                
            elseif bitand(asserv(1,j), 2)   % redimensionne en y seulement
                this.lstCmd{end+1} = {this.hAxes(i, j), ...
                    'YLim', [nyvmin nyvmax]};
            end
        end
    end
end
