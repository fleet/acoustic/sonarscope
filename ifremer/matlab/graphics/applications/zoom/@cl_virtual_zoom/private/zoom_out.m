% Zoom sortant
%
% Syntax
%   a = zoom_out(a, hAxe, bbox, echelle, position)
%
% Input Arguments
%   a : instance de cl_virtual_zoom
%   hAxe     : handle de l axe sur lequel a eu lieu le zoom
%   bbox     : rectangle (en pixels) definissant le zoom
%   echelle  : echelles (min et max) de chaque axe de l axe
%   position : position en pixel, dans la figure, de l axe
%
% Output Arguments
%   a : instance de cl_virtual_zoom
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = zoom_out(this, hAxe, echelle, varargin)

[varargin, recadrage] = getFlag(varargin, 'recadrage'); %#ok<ASGLU>

% ------------------------------------------------------------------
% Determination de nouvelles limites de valeurs pour les axes x et y

dx = echelle(2) - echelle(1);
dy = echelle(4) - echelle(3);

nxvmin = echelle(1) - dx/2;
nxvmax = echelle(2) + dx/2;
nyvmin = echelle(3) - dy/2;
nyvmax = echelle(4) + dy/2;

axis([nxvmin nxvmax nyvmin nyvmax])

% ------------------------------------------
% Determination de la ligne d'asservissement

ligne  = find(this.hAxes == hAxe);
if isempty(ligne)
    return
end
asserv = this.asserv(ligne, :);

% --------------------------
% Vide la liste de commandes

this.lstCmd = [];

% --------------------------
% Application de ces valeurs

[m, n] = size(this.hAxes);
for i=1:1:m
    for j=1:1:n
        
        % calcul des limites du zoom out
        [minAxe_x, maxAxe_x, minAxe_y, maxAxe_y] = determinerMinMaxXY ...
            (this.hAxes(i, j));
        
        
        if recadrage
            deltax = nxvmax - nxvmin;
            deltay = nyvmax - nyvmin;
            
            %             XLim =  get(hAxe, 'XLim');
            %             [DataLim, AxeLim] = getParamZoom('X', 'hAxe', hAxe);
            %             if isinf(DataLim(1))
            %                 bordGauche = XLim(1);
            %             else
            %                 bordGauche = DataLim(1);
            %             end
            nxvmax = nxvmin + deltax;
            
            %             if isinf(DataLim(2))
            %                 bordDroit = XLim(2);
            %             else
            %                 bordDroit = DataLim(2);
            %             end
            nxvmin = nxvmax - deltax;
            
            %             YLim =  get(hAxe, 'YLim');
            %             [DataLim, AxeLim] = getParamZoom('Y', 'hAxe', hAxe);
            %             if isinf(DataLim(1))
            %                 bordBas = YLim(1);
            %             else
            %                 bordBas = DataLim(1);
            %             end
            
            nyvmax = nyvmin + deltay;
            
            %             if isinf(DataLim(2))
            %                 bordHaut = YLim(2);
            %             else
            %                 bordHaut = DataLim(2);
            %             end
            nyvmin = nyvmax - deltay;
            
            minAxe_x = nxvmin;
            maxAxe_x = nxvmax;
            minAxe_y = nyvmin;
            maxAxe_y = nyvmax;
        end
        
        if bitand(asserv(1,j), 1) && bitand(asserv(1,j), 2)
            % redimensionne en x et y
            this.lstCmd{end+1} = {this.hAxes(i, j), ...
                'XLim', [minAxe_x maxAxe_x], 'YLim', [minAxe_y maxAxe_y]};
            
        elseif bitand(asserv(1,j), 1)
            % redimensionne en x seulement
            this.lstCmd{end+1} = {this.hAxes(i, j), 'XLim', [minAxe_x maxAxe_x]};
            
        elseif bitand(asserv(1,j), 2)
            % redimensionne en y seulement
            this.lstCmd{end+1} = {this.hAxes(i, j), 'YLim', [minAxe_y maxAxe_y]};
            
        end
    end
end


% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Dertermination du min et max en x et y pour les axes geres par l'instance

function [minx, maxx, miny, maxy] = determinerMinMaxXY(hAxe)

currentAxe = gca;

% ----------------------------
% Initialisation du min et max

axes(hAxe);
minMaxXY = axis;
minx = minMaxXY(1);
maxx = minMaxXY(2);
miny = minMaxXY(3);
maxy = minMaxXY(4);

% ------------------------------------------
% Recuperation des 'enfants' : courbes, ...

listLines    = findobj(hAxe, 'Type', 'line');
listImages   = findobj(hAxe, 'Type', 'Image');
listChildren = [listImages(:); listLines(:)];

% ---------------------------------------------
% Parcours de tous les enfants de l'axe courant

[o, p] = size(listChildren);

% ----------------------------------------
% Recherche des min et max pour chaque axe

for k = 1:1:o
    for l = 1:1:p
        if min(get(listChildren(k, l), 'XData')) < minx
            minx = min(get(listChildren(k, l), 'XData'));
        end
        if min(get(listChildren(k, l), 'YData')) < miny
            miny = min(get(listChildren(k, l), 'YData'));
        end
        if max(get(listChildren(k, l), 'XData')) > maxx
            maxx = max(get(listChildren(k, l), 'XData'));
        end
        if max(get(listChildren(k, l), 'YData')) > maxy
            maxy = max(get(listChildren(k, l), 'YData'));
        end
    end
end

% ----------------------------
% Se replace sur l'axe courant

axes(currentAxe);
