% Modification des attributs de l instance
% 
% Syntax
%   a = set(a, ...)
%
% Input Arguments
%   a : instance de cl_virtual_zoom
%
% Name-Value Pair Arguments
%   hFig             : handle de la figure contenant les axes
%   hAxes            : handle (tableau) des axes permettant le zoom
%   asserv           : tableau descriptif de l asservissement entre les axes
%   cbName           : definition du nom de la call-back
%   cbObj            : definition de l instance de la call-back
%   action           : action du zoom {'In', 'Out'}
%   msg              : message
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% Remarks : 
%
% Examples
%   z = cl_virtual_zoom('hFig', figure); 
%   set(z, 'hAxes', axes)
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

%% Lecture des arguments

[varargin, hFig]   = getPropertyValue(varargin, 'hFig',   []);
[varargin, hAxes]  = getPropertyValue(varargin, 'hAxes',  []);
[varargin, asserv] = getPropertyValue(varargin, 'asserv', []);
[varargin, cbName] = getPropertyValue(varargin, 'cbName', []);
[varargin, cbObj]  = getPropertyValue(varargin, 'cbObj',  []);
[varargin, action] = getPropertyValue(varargin, 'action', []);
[varargin, msg]    = getPropertyValue(varargin, 'msg',    []); %#ok<ASGLU>

%% Initialisation des attributs

if ~isempty(hFig)
    this = set_h_fig(this, hFig);
end
if ~isempty(hAxes)
    this = set_h_axes(this, hAxes);
end
if ~isempty(asserv)
    this = set_asserv(this, asserv);
end
if ~isempty(cbName)
    this = set_cb_name(this, cbName);
end
if ~isempty(cbObj)
    this = set_cb_obj(this, cbObj);
end
if ~isempty(action)
    this = set_action(this, action);
end
if ~isempty(msg)
    this = set_msg(this, msg);
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
