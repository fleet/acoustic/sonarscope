% Initialisation du type d'action du zoom
%
% Syntax
%   a = set_action(a, action)
%
% Input Arguments
%   a      : instance de cl_virtual_zoom
%   action : type d action {'In', 'Out'}
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_action(this, action)
this.action = action;
