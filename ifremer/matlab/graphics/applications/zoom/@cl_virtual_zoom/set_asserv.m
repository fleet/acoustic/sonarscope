% Initialisation de l'asservissement des axes
% 
% a = set_asserv(a, asserv)
%
% Input Arguments
%   a      : instance de cl_virtual_zoom
%   asserv : description de l asservissement des axes
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: set_asserv.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    13/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function this = set_asserv(this, asserv)

% Maintenance Auto : try
    this.asserv = asserv;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'set_asserv', lasterr);
% Maintenance Auto : end
