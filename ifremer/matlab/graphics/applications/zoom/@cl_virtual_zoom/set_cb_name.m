% Initialisation du nom de la methode ou fonction de callback
% 
% Syntax
%   a = set_cb_name(a, cbName)
%
% Input Arguments
%   a      : instance de cl_virtual_zoom
%   cbName : nom de methode ou fonction de la call back
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: set_cb_name.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    16/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function this = set_cb_name(this, cbName)

% Maintenance Auto : try
    
    % --------------
    % Initialisation
    
    this.cbName = cbName;
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_virtual_zoom', 'set_cb_name', lasterr);
% Maintenance Auto : end
