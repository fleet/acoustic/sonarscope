% Initialisation de l'instance de la call back
%
% Syntax
%   a = set_cb_obj(a, cbObj)
%
% Input Arguments
%   a     : instance de cl_virtual_zoom
%   cbObj : instance de la call back
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_cb_obj(this, cbObj)
this.cbObj = cbObj;
