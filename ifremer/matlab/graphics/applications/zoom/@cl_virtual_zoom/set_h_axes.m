% Initialisation du tableau de handles des axes
%
% Syntax
%   a = set_h_axes(a, handle)
%
% Input Arguments
%   a      : instance de cl_virtual_zoom
%   handle : tableau de handles des axes
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: set_h_axes.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_h_axes(this, handle)

isAxes = 1;

% ----------------------------------------------------------------
% Test validite des arguments, initialisation
% - vide                 : initialisation a vide
% - non vide et valide   : initialisation a handle si Type == axes
% - non vide et invalide : initialisation a vide

% Init a vide
if isempty(handle)
    this.hAxes = handle;
    
elseif ishandle(handle)
    % Init a handle s'ils correspondent a des axes, sinon vide
    [m, n] = size(handle);
    for i = 1:1:m
        for j = 1:1:n
            isAxes = isAxes & (isAGraphicElement(handle(i, j), 'axes') == 1);
        end
    end
    if isAxes
        this.hAxes = handle;
    else
        this.hAxes = [];
        my_warndlg('cl_virtual_zoom/set_h_axe : Not an axes handle', 0);
    end
    
    % Init a vide, handle non valide
else
    this.hAxes = [];
    my_warndlg('cl_virtual_zoom/set_h_axes : Invalid handle', 0);
end
