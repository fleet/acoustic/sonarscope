% Initialisation du handle de la figure
% 
% Syntax
%   a = set_h_fig(a, handle)
%
% Input Arguments
%   a      : instance de cl_virtual_zoom
%   handle : handle de la figure
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% VERSION  : $Id: set_h_fig.m,v 1.2 2003/07/24 14:51:20 augustin Exp $
% ----------------------------------------------------------------------------


function this = set_h_fig(this, handle)

% ------------------------------------------------
% Test validite des arguments, initialisation
% - vide                 : initialisation a vide
% - non vide et valide   : initialisation a handle
% - non vide et invalide : initialisation a vide

if isempty(handle)
    this.hFig = handle;
elseif ishandle(handle)
    this.hFig = handle;
else
    this.hFig = [];
    my_warndlg('cl_virtual_zoom/set_h_fig : Invalid handle', 0);
end
