% Initialisation du message
%
% Syntax
%   a = set_msg(a, msg)
%
% Input Arguments
%   a   : instance de cl_virtual_zoom
%   msg : message ('zoom' par defaut)
%
% Output Arguments
%   a : instance de cl_virtual_zoom initialisee
%
% See also cl_virtual_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_msg(this, msg)
this.msg = msg;
