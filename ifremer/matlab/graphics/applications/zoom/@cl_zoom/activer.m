% Activation du zoom
%
% Syntax
%   a = activer(a)
%
% Input Arguments
%   a : instance de cl_zoom
%
% Output Arguments
%   a : instance de cl_zoom
%
% Examples
%    figure ; plot(1:100, rand([1 100]) * 100)
%    z = cl_zoom('hFig', gcf, 'hAxes', gca, 'asserv', [1]);
%    set(z, 'cbName', 'cb', 'cbObj', 'cl_zoom');
%    activer(z)
%    % effectuer un zoom souris (definir une zone), resultat a la console
%
% See also cl_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = activer(this)

%% Appel m�thode super-classe

this.cl_virtual_zoom = activer(this.cl_virtual_zoom);
