% Application du zoom (doit etre appele par la call back)
%
% Syntax
%   appliquer(a, hAxe, bbox)
% 
% Input Arguments
%   a    : instance de cl_zoom
%   hAxe : handle de l axe sur lequel a eu lieu le zoom
%   bbox : rectangle (en pixels) definissant le zoom
%
% Examples
%    z = cl_zoom('hFig', figure, 'hAxes', axes, 'asserv', [1]);
%    set(z, 'cbName', 'cb', 'cbObj', 'cl_zoom');
%    activer(z)
%    % effectuer un zoom souris (definir une zone), resultat a la console
%
% See also cl_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function appliquer(this, hAxe, bbox, varargin)

%% Détermination des parametres du zoom par la super-classe

this.cl_virtual_zoom = appliquer(this.cl_virtual_zoom, hAxe, bbox, varargin{:});

%% Réalisation du zoom

lstCmd = get_lstcmd(this);
for i=1:length(lstCmd)
    lst = lstCmd{i};
    set(lst{:});
end
