% Transformation en chaine de caracteres d'une d'instance
%
% Syntax
%   str = char(a) 
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_zoom
%
% Output Arguments
%    str : une chaine de caracteres representative
%
% Examples
%   a = cl_zoom;
%   str = char(a)
%
% See also cl_zoom Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.3 2003/07/24 15:34:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    % --------------------------
    % Appel methode super classe
    
    str = char(this.cl_virtual_zoom);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_zoom', 'char', lasterr);
% Maintenance Auto : end
