% Constructeur de cl_zoom
%
% a = cl_zoom(...)
% 
% Name-Value Pair Arguments
%    hFig   : handle de la figure contenant les axes
%    hAxes  : handle (tableau) des axes permettant le zoom
%    asserv : tableau descriptif de l asservissement entre les axes
%    cbName : definition du nom de la call-back
%    cbObj  : definition de l'instance de la call-back
%    action : action du zoom {'In', 'Out'}
%
% Output Arguments 
%    a : instance de cl_zoom
%
% Remarks : 
%    Cette instance place, au niveau des axes, des call back qui sont 
%    automatiquement supprimees par matlab lors de certaines operations
%    telles que plot. Il faut alors indiquer a l instance de zoom d
%    effectuer une mise a jour des axes
%
%    La call back (fonction) est definie par 4 arguments :
%    - nom de la fonction a appeler
%    - identifiant de la call back == 'zoom'
%    - handle de l axe
%    - rectangle de zoom
%    La call back (methode) est definie par 5 arguments :
%    - nom de la methode a appeler
%    - instance executant la methode
%    - identifiant de la call back == 'zoom'
%    - handle de l axe
%    - rectangle de zoom
%    Cette call back doit appeler la methode 'appliquer' de l'instance de
%    cl_zoom permettant l application du zoom
%
%    Asservissement : pour nbAxes le nombre d axes, le tableau 
%    d asservissement est de dimensions (nbAxes x nbAxes). Les lignes, comme
%    les colonnes correspondent aux handles des axes (dans le meme ordre)
%    Le contenu de ce tableau est une combinaison binaire de 1 et 2 :
%    1 : permet le redimensionnement en x
%    2 : permet le redimensionnement en y
%    Ainsi, 0 ne permet pas de redimensionnement alors que 3 permet de 
%    redimensionner en x et y.
%    Pour trois graphes, voici un tableau exemple
%       | h1 | h2 | h3 |
%    ---|----|----|----|
%    h1 |  3 |  1 |  2 |
%    ---|----|----|----|
%    h2 |  1 |  3 |  3 |
%    ---|----|----|----|
%    h3 |  2 |  3 |  3 |
%    ---|----|----|----|
%    Lecture : pour une modif sur h1 (en ligne), on redimensionne h1 en x et 
%    y, on redimensionne h2 en x et on redimensionne h3 en y
%    
% Examples
%    f = cl_zoom
%
% See also cl_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = cl_zoom(varargin)
    
%% D�finition de la structure et initialisation

this.none   = [];

%% Super-classe

if (length(varargin) == 1) && isempty(varargin{1})
    superClasse = cl_virtual_zoom([]);
else
    superClasse = cl_virtual_zoom;
end

%% Cr�ation de l'objet

this = class(this, 'cl_zoom', superClasse);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
