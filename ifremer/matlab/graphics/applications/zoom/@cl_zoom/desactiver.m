% Desactivation du zoom
%
% Syntax
%   a = desactiver(a)
%
% Input Arguments
%   a : instance de cl_zoom
%
% Output Arguments
%   a : instance de cl_zoom
%
% Examples
%    figure; plot(1:100, rand([1 100]) * 100);
%    z = cl_zoom('hFig', gcf, 'hAxes', gca, 'asserv', [1]);
%    set(z, 'cbName', 'cb', 'cbObj', 'cl_zoom');
%    activer(z)
%    % effectuer un zoom souris (definir une zone), resultat a la console
%    desactiver(z)
%    % le zoom n est plus actif
%
% See also cl_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = desactiver(this)

%% Appel methode super-classe

this.cl_virtual_zoom = desactiver(this.cl_virtual_zoom);
