% Lecture des attributs de l'instance
%
% Syntax
%   [...] = get (a, ...)
% 
% Input Arguments
%   a : instance de cl_zoom
%
% Name-only Arguments
%    hFig   : handle de la figure contenant les axes
%    hAxes  : handle (tableau) des axes permettant le zoom
%    asserv : tableau descriptif de l asservissement entre les axes
%    cbName : definition du nom de la call-back
%    cbObj  : definition de l instance de la call-back
%    action : action du zoom {'In', 'Out'}
%    isOn   : 1 : zoom actif, 0 : zoom inactif
%    lstCmd : liste des commandes de realisation du zoom
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    f = cl_zoom ('hFig', figure, 'hAxes', axes, 'asserv', [1]);
%    [hf, ha, a] = get (f, 'hFig', 'hAxes', 'asserv')
%
% See also cl_zoom Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get (instance, varargin)

varargout = {};

for i=1:length(varargin)
    varargout{end+1} = get (instance.cl_virtual_zoom, varargin {i}); %#ok<AGROW>
end
