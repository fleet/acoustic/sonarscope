% Mise a jour des callback pour la barre d icone de la fenetre matlab
%
% Syntax
%   maj_icone_cb(a, cbIn, cbOut)
% 
% Les call back deux icones de zoom sont remplacees par les callbacks
% donnees en argument
%
% Input Arguments
%   a     : instance de cl_zoom
%   cbIn  : callback pour le Zoom In
%   cbOut : callback pour le Zoom Out
%
% Examples
%    figure; plot(1:100, rand([1 100]) * 100);
%    z = cl_zoom('hFig', gcf, 'hAxes', gca, 'asserv', [1]);
%    cbIn  = 'disp(''In'');';
%    cbOut = 'disp(''Out'');';
%    maj_icone_cb(z, cbIn, cbOut);
%
%    % Cliquer sur les icones de zoom
%
% See also help cl_zoom
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function maj_icone_cb(instance, cbIn, cbOut)

% Maintenance Auto : try
    
    % --------------------------
    % Appel methode super-classe
    
    maj_icone_cb(instance.cl_virtual_zoom, cbIn, cbOut);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_zoom', 'maj_icone_cb', lasterr);
% Maintenance Auto : end
