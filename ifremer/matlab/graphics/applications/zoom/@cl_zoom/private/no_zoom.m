% Suppression des modifications graphiques dues au zoom
%
% Syntax
%   no_zoom(a)
% 
% Input Arguments
%   a : instance de cl_zoom
%
% See also cl_zoom Authors
% Authors : DCF
% VERSION  : $Id: no_zoom.m,v 1.2 2003/07/24 15:33:48 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    19/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
%    18/06/2001 - DCF - Accede a la liste des commandes et realise le zoom
% ----------------------------------------------------------------------------

function no_zoom(this)

% Maintenance Auto : try
    
    % ------------------------------
    % Acces a la liste des commandes
    
    lstCmd = get_lstcmd(this);
    
    % -------------------
    % Realisation du zoom
    
    for i = 1:length(lstCmd)
        set(lstCmd{:});
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_zoom', 'no_zoom', lasterr);
% Maintenance Auto : end
