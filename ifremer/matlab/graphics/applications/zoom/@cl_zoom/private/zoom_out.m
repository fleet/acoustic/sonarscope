% Zoom sortant
%
% Syntax
%   zoom_out(a, hAxe, bbox, echelle, position)
% 
% Input Arguments
%   a        : instance de cl_zoom
%   hAxe     : handle de l axe sur lequel a eu lieu le zoom
%   bbox     : rectangle (en pixels) definissant le zoom
%   echelle  : echelles (min et max) de chaque axe de l axe
%   position : position en pixel, dans la figure, de l axe
%
% See also cl_zoom Authors
% Authors : DCF
% VERSION  : $Id: zoom_out.m,v 1.2 2003/07/24 15:33:48 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    19/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function zoom_out(this, hAxe, bbox, echelle, position)

% Maintenance Auto : try
    
    % ------------------------------------------------------------------
    % Determination de nouvelles limites de valeurs pour les axes x et y
    
    dx = echelle(2) - echelle(1);
    dy = echelle(4) - echelle(3);
    
    nxvmin = echelle(1) - dx/2;
    nxvmax = echelle(2) + dx/2;
    nyvmin = echelle(3) - dy/2;
    nyvmax = echelle(4) + dy/2;
    
    % --------------------------------------------------------------------------
    % Verification : ne pas sortir des valeurs des donnees representees
    % Pour cela, on recherche le min et max de tous les enfants de tous les axes
    
    % DCF  [minAxe_x, maxAxe_x, minAxe_y, maxAxe_y] = determinerMinMaxXY(this);
    % DCF  if (nxvmin < minAxe_x) nxvmin = minAxe_x; end
    % DCF  if (nxvmax > maxAxe_x) nxvmax = maxAxe_x; end
    % DCF  if (nyvmin < minAxe_y) nyvmin = minAxe_y; end
    % DCF  if (nyvmax > maxAxe_y) nyvmax = maxAxe_y; end
    
    % ------------------------------------------
    % Determination de la ligne d'asservissement
    
    ligne  = find(this.hAxes == hAxe);
    asserv = this.asserv(ligne, :);
    
    % --------------------------
    % Application de ces valeurs
    
    [m, n] = size(this.hAxes);
    for i = 1:1:m
        for j = 1:1:n
            
            % calcul des limites du zoom out
            [minAxe_x, maxAxe_x, minAxe_y, maxAxe_y] = ...
                determinerMinMaxXY(this.hAxes(i, j));
            if (nxvmin > minAxe_x) minAxe_x = nxvmin; end
            if (nxvmax < maxAxe_x) maxAxe_x = nxvmax; end
            if (nyvmin > minAxe_y) minAxe_y = nyvmin; end
            if (nyvmax < maxAxe_y) maxAxe_y = nyvmax; end
            
            if bitand(asserv(1,j), 1) & bitand(asserv(1,j), 2)
                % redimensionne en x et y
                set( this.hAxes(i, j)   , ...
                    'XLim', [minAxe_x maxAxe_x] , ...
                    'YLim', [minAxe_y maxAxe_y] );
                
            elseif bitand(asserv(1,j), 1)
                % redimensionne en x seulement
                set( this.hAxes(i, j), 'XLim', [minAxe_x maxAxe_x]);
                
            elseif bitand(asserv(1,j), 2)
                % redimensionne en y seulement
                set(this.hAxes(i, j), 'YLim', [minAxe_y maxAxe_y]);
            end      
            
        end
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_zoom', 'zoom_out', lasterr);
% Maintenance Auto : end

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Fonctions Locales
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Dertermination du min et max en x et y pour les axes geres par l'instance

function [minx, maxx, miny, maxy] = determinerMinMaxXY(hAxe)

% --------------------------------------------------------------------------
% Locales

currentAxe = gca;

% --------------------------------------------------------------------------
% Initialisation du min et max

axes(hAxe);
minMaxXY = axis;
minx = minMaxXY(1);
maxx = minMaxXY(2);
miny = minMaxXY(3);
maxy = minMaxXY(4);

% ------------------------------------------
% Recuperation des 'enfants' : courbes, ...

listChildren = get(hAxe, 'children');

% ---------------------------------------------
% Parcours de tous les enfants de l'axe courant

[o, p] = size(listChildren);

% ----------------------------------------
% Recherche des min et max pour chaque axe

for k = 1:1:o
    for l = 1:1:p
        if (min(get(listChildren(k, l), 'XData')) < minx)
            minx = min(get(listChildren(k, l), 'XData'));
        end
        if (min(get(listChildren(k, l), 'YData')) < miny)
            miny = min(get(listChildren(k, l), 'YData'));
        end
        if (max(get(listChildren(k, l), 'XData')) > maxx)
            maxx = max(get(listChildren(k, l), 'XData'));
        end
        if (max(get(listChildren(k, l), 'YData')) > maxy)
            maxy = max(get(listChildren(k, l), 'YData'));
        end
    end
end

% ----------------------------
% Se replace sur l'axe courant

axes(currentAxe);
