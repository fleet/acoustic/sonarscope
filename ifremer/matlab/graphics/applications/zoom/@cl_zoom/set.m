% Modification des attributs de l instance
% 
% Syntax
%   a = set(a, varargin)
%
% Input Arguments
%   a : instance de cl_zoom
%
% Name-Value Pair Arguments
%   hFig   : handle de la figure contenant les axes
%   hAxes  : handle (tableau) des axes permettant le zoom
%   asserv : tableau descriptif de l asservissement entre les axes
%   cbName : definition du nom de la call-back
%   cbObj  : definition de l instance de la call-back
%   action : action du zoom {'In', 'Out'}
%
% Output Arguments
%    a : instance de cl_zoom initialisee
%
% Examples
%    z = cl_zoom('hFig', figure) ; 
%    set(z, 'hAxes', axes)
%
% See also cl_zoom Authors
% Authors : DCF
% VERSION  : $Id: set.m,v 1.3 2003/07/24 15:34:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% Maintenance Auto : try
    
    % --------------------------
    % Appel methode super classe
    
    this.cl_virtual_zoom = set(this.cl_virtual_zoom, varargin{:});
    
    % --------------------
    % Traitement du retour
    
    if nargout == 0
        assignin('caller', inputname(1), this);
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_zoom', 'set', lasterr);
% Maintenance Auto : end
