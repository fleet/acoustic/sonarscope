% Gestion du rapport d'echelle pour une representation pseudo-geographique
%
% Syntax
%   axisCarto('PropertuNames', PropertyValues)
%
% PROPERTY NAMES :
%   Axe       : Handle de l'axe concerne (axe courant par defaut)
%   NomZone   : Nom de la zone traitee (Pour completer la legende)
%   Scale     : Denominateur de l'echelle
%   PaperType : PaperType [ A0 | A1 | A2 | A3 | {A4} ]
%   latRef    : Latitude d'echelle conservee
%
% Examples
%   liste = findFicInDir('/home1/doppler/Cosmos/Blanes/abc/*.index');
%   plotNavCosmos(liste)
%   axisCarto('Scale', 20000)
%   axisCarto('PaperType', 'A0', 'latRef', str2lat('N41 40'), 'Scale', 20000)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function axisCarto(varargin)

[varargin, Scale]     = getPropertyValue(varargin, 'Scale', 		[]);
[varargin, PaperType] = getPropertyValue(varargin, 'PaperType', 'A4');
[varargin, ax]        = getPropertyValue(varargin, 'Axe', 		gca);
[varargin, NomZone]   = getPropertyValue(varargin, 'NomZone', []);

D2R = pi / 180;

YLim = get(ax, 'YLim');
XLim = get(ax, 'XLim');

[varargin, latRef] = getPropertyValue(varargin, 'latRef', mean(YLim)); %#ok<ASGLU>

set(ax,'DataAspectRatio', [1 cos(latRef * D2R) 1]);
set(ax, 'PlotBoxAspectRatioMode', 'auto');

fig = get(ax, 'parent');
set(fig, 'PaperType', PaperType)
set(fig, 'PaperUnits', 'centimeters');
set(fig, 'PaperOrientation', 'landscape');
PaperSize = get(fig, 'PaperSize');

LegendeAxisCarto = findobj(gcf, 'Tag', 'LegendeAxisCarto');

R = 6371000 ;           % rayon de la terre en m
heihgtMap = R * diff(YLim) * D2R;
widthMap  = R * diff(XLim) * D2R * cos(latRef * D2R);
fprintf('                  Taille de l''image : L(m) =%.2f\tH(m) =%.2f\n', widthMap, heihgtMap);
if ~isempty(Scale)
    heightAxe = heihgtMap * 100 / Scale;
    widthAxe  = widthMap  * 100 / Scale;
    
    PaperPosition = get(fig, 'PaperPosition');
    widthPaper =  PaperPosition(3);
    heightPaper = PaperPosition(4);
    
    set(ax, 'Units', 'normalized');
    PositionAxe = get(ax, 'Position');
    
    coefWidth   =  widthAxe  / (widthPaper  * PositionAxe(3));
    coefHeight  =  heightAxe / (heightPaper * PositionAxe(4));
    
    widthPaper =  PaperPosition(3) * coefWidth;
    heightPaper = PaperPosition(4) * coefHeight;
    
    gauche = (PaperSize(1) - widthPaper) / 2;
    haut   = (PaperSize(2) - heightPaper) / 2;
    set(fig, 'PaperPosition', [gauche haut widthPaper heightPaper]);
    fprintf('Echelle : 1/%d Taille du trace   : L(cm)=%.2f\tH(cm)=%.2f\n', Scale, widthPaper, heightPaper);
end

XLim = get(gca, 'XLim');
YLim = get(gca, 'YLim');

if isempty(NomZone)
    legende = '';
else
    legende = sprintf('Zone\t\t: %s\n', NomZone);
end
legende = sprintf('%sCentral Meridien\t: %s\nNorth Latitude\t: %s\nSouth Latitude\t: %s\nWest Longitude\t: %s\nEast Longitude\t: %s',...
    legende, lat2str(latRef), lat2str(YLim(2)), lat2str(YLim(1)), lon2str(XLim(1)), lon2str(XLim(2)));
if ~isempty(Scale)
    legende = sprintf('%s\nScale\t\t: 1/%d', legende, Scale);
end

if isempty(LegendeAxisCarto)
    LegendeAxisCarto = text(XLim(1) + diff(XLim) / 50, YLim(1) + diff(YLim) / 50, 'toto', ...
        'VerticalAlignment', 'bottom', 'FontSize', 8);
    set_HitTest_On(LegendeAxisCarto)
    set(LegendeAxisCarto, 'Tag', 'LegendeAxisCarto');
end
set(LegendeAxisCarto, 'Position', [XLim(1) + diff(XLim) / 50, YLim(1) + diff(YLim) / 50, 0])
set(LegendeAxisCarto, 'String', legende)
