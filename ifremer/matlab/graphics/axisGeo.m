% Donne un aspect orthonorme metrique a un axe defini en latitudes, longitudes
%
% Syntax
%   axisGeo
%
% Examples
%   [Z, lon, lat] = Etopo('LatLim', [43 50], 'LonLim', [-5 8]);
%   figure; imagesc(lon, lat, Z, [0 500]); colorbar; axis xy
%   figure; imagesc(lon, lat, Z, [0 500]); colorbar; axis xy; axis equal
%   figure; imagesc(lon, lat, Z, [0 500]); colorbar; axis xy; axisGeo
%
% See also Authors
% Authors : JMA
%------------------------------------------------------------------------------

function axisGeo(varargin)

[varargin, Rot90] = getPropertyValue(varargin, 'Rot90', 0);

if length(varargin) == 1
    hAxe = varargin{1};
else
    hAxe = gca;
end


if Rot90
    XLim = get(hAxe, 'XLim');
    if (XLim(1) >= -91) && (XLim(1) <= 91)
        LatMilieu = mean(XLim);
        xfac = cosd(LatMilieu);
        %         set(hAxe, 'DataAspectRatio', [1 1/xfac 1]);
        set(hAxe, 'DataAspectRatio', [xfac 1 1]);
        %         axis(hAxe, 'ij') % Comment� par JMA le 21/01/2015
    else
        str = 'Votre axe des latitudes n''est pas dans l''intervale [-90 90]';
        disp(str)
        my_warndlg(str, 1);
    end
else
    YLim = get(hAxe, 'YLim');
    if (YLim(1) >= -91) && (YLim(1) <= 91)
        LatMilieu = mean(YLim);
        xfac = cosd(LatMilieu);
        %         set(hAxe, 'DataAspectRatio', [1/xfac 1 1]);
        set(hAxe, 'DataAspectRatio', [1 xfac 1]);
        %         axis(hAxe, 'xy') % Comment� par JMA le 21/01/2015
    else
        str1 = 'L''axe des latitudes n''est pas dans l''intervale [-90 90]. (Essayez Axis Tight" avant ?';
        str2 = 'The Y axis is not in the range [-90 90]. (Try axis "Tight" before ?).';
        my_warndlg(Lang(str1,str2), 1);
    end
end
