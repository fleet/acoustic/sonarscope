% Set the watch-cursor.
%
% Syntax
%   busy(...)
%
% Name-Value Pair Arguments
%   fig : Handle of the figure (default gcf ) 
%
% Examples 
%   figure; plot(1:10);
%   busy
%   idle
%
% See also idle Authors
% Authors : Copyright (C) 1996 Dr. Charles R. Denham, ZYDECO
%--------------------------------------------------------------------------------

function busy(theFigure)

if (nargin == 1) && ishandle(theFigure)
    set(theFigure, 'Pointer', 'watch');
else
    figs = findobj(0, 'Type', 'figure');
    if isa(figs, 'matlab.ui.Figure')
        if isempty(figs) % TODO GLU R2014b : v�rifier si c'est correct voir http://www.mathworks.fr/fr/help/matlab/graphics_transition/graphics-handles-are-now-objects-not-doubles.html
            return
        end
    else
        if ~any(figs)
            return
        end
    end
    set(figs, 'Pointer', 'watch');
end

