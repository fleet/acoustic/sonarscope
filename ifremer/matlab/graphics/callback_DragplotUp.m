function callback_DragplotUp(varargin)

[varargin, GCF]    = getPropertyValue(varargin, 'GCF', gcf);
[varargin, GCA_In] = getPropertyValue(varargin, 'GCA', gca);

PW = getPointerWindow;
%         PW = matlab.ui.internal.getPointerWindow;  % HG2: R2014b or newer; Pb rend la pointeur
%                                                      m�me si la figure est iconis�e
[varargin, figReceptrice] = getPropertyValue(varargin, 'figReceptrice', PW); %#ok<ASGLU>

newFig = false;
if figReceptrice == 0 % Si pointer sur �cran on cr�e un nouvelle figure
    if ~ishandle(GCA_In)
        return
    end
    newFig = true;
    figReceptrice = FigUtils.createSScFigure;
    Title = get(get(GCA_In, 'Title'), 'String');
    if isempty(Title)
        title('Clone');
    else
        title(['Clone of ' Title(1,:)], 'Interpreter', 'none')
    end
    grid on
    GCA_Out = gca;
    YDir = get(GCA_In, 'YDir');
    set(GCA_Out, 'YDir', YDir);
    XDir = get(GCA_In, 'XDir');
    set(GCA_Out, 'XDir', XDir);
    dragplot
end

h = getappdata(GCF, 'dragplot');
if isempty(h)
    %             pause(0.5)
    h = getappdata(GCF, 'dragplot');
end
if length(h) < 2
    if newFig
        close(figReceptrice)% rajout� par JMA le 15/02/2013
    end
    return
end

if figReceptrice == h(end) %h(2) % Si on est sur la m�me figure on ne fait rien
    if ishandle(h(1))
        %                 set(h(1), 'selected', 'off'); %GLA
    end
else % Si on est sur une autre figure
    testFigReceptrice = (figReceptrice.Number > 0);
    if testFigReceptrice
        axeRecepteur = get(figReceptrice, 'currentaxes');
        if (axeRecepteur == GCA_In)
            return
        end
        if ishandle(h(1))
            s = get(h(1), 'selected');
            if strcmp(s, 'on') % 1iere courbe s�l�ctionnn�e = on recopie LA ou LES courbes
                t = get(GCF, 'selectiontype');
                if strcmp(t, 'alt') % ctrl + click
                    h = findobj(GCA_In, 'Type', 'Line','-and', 'selected', 'on');
                    for k=1:length(h)
                        n = copyobj(h(k), axeRecepteur);
                        cloneProperties(h(k), n);
                        set(n, 'selected', 'off'); %GLA
                        axis tight
                    end
                    % set(h(1), 'parent', axeRecepteur);
                    % set(h(1), 'selected', 'off');
                    % axis tight
                else
                    n = copyobj(h(1), axeRecepteur);
                    cloneProperties(h(1), n);
                    set(n, 'selected', 'off');
                    set(h(1), 'selected', 'off');
                    %                             axis tight
                end
            else % 1iere courbe non s�l�ctionnn�e = on recopie TOUTES les courbes
                h = findobj(GCA_In, 'Type', 'Line');
                for k=1:length(h)
                    n = copyobj(h(k), axeRecepteur);
                    cloneProperties(h(k), n);
                    axis tight
                end
                h = findobj(GCA_In, 'Type', 'Patch');
                for k=1:length(h)
                    n = copyobj(h(k), axeRecepteur);
                    cloneProperties(h(k), n);
                    axis tight
                end
                h = findobj(GCA_In, 'Type', 'Text');
                for k=1:length(h)
                    n = copyobj(h(k), axeRecepteur);
                    cloneProperties(h(k), n);
                    axis tight
                end
            end
        else %?
            h = findobj(GCA_In, 'Type', 'Line');
            for k=1:length(h)
                n = copyobj(h(k), axeRecepteur);
                cloneProperties(h(k), n);
                set(n, 'selected', 'off'); %GLA
                axis tight
            end
        end
    else
        delete(h(1));
    end
end
set(GCF, 'pointer', 'arrow');
end
