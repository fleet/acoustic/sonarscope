% Calcul de la position d'une figure centr�e sur l'�cran
% Si les tailles sont trop grandes, la figure est ramen�e � la taille de
% l'�cran
%
% Syntax
%   Position = centrageFig(Width, Height)
%
% Input Arguments
%   Width  : Largeur souhait�e de la fen�tre
%   Height : Hauteur souhait�e de la fen�tre
%
% Name-Value Pair Arguments
%   Fig       : Num�ro de la figure si elle existe d�j�
%   colDepMin :
%   ligDepMin :
%
% Output Arguments
%   []       : Cr�ation de la figure
%   Position : Position de la figure
%
% Examples
%   figure;
%   centrageFig(800, 600)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Position = centrageFig(Width, Height, varargin)

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
[varargin, colDepMin] = getPropertyValue(varargin, 'colDepMin', 200);
[varargin, ligDepMin] = getPropertyValue(varargin, 'ligDepMin', 200); %#ok<ASGLU>

%% R�cup�ration de la taille de l'�cran

ScreenSize = get(0, 'ScreenSize');

%% Controle de positionnement

Position(3) = min(Width,  ScreenSize(3)-colDepMin);
Position(4) = min(Height, ScreenSize(4)-ligDepMin);

milieuEcran   = floor((ScreenSize(3)-colDepMin) / 2);
milieuFenetre = floor(Position(3) / 2);
Position(1)   = milieuEcran - milieuFenetre + colDepMin - 50;

milieuEcran   = floor((ScreenSize(4)-ligDepMin) / 2);
milieuFenetre = floor(Position(4) / 2);
Position(2)   = milieuEcran - milieuFenetre + 50;

%% Par ici la sortie

if nargout == 0
    if isempty(Fig)
        figure('Position', Position);
    else
        figure(Fig);
        set(Fig, 'Position', Position);
    end
end
