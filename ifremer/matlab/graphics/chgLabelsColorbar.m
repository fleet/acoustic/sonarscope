% Changement des labels d'une colorbar par interpolation lineaire entre valMin et valMax.
%
% Syntax
%   chgLabelsColorbar(handleColorbar, valMin, valMax)
%
% Input Arguments
%   handleColorbar : handle de la colormap
%   valMin         : valeur correspondant a la premiere couleur
%   valMax         : valeur correspondant a la derniere couleur
%
% Name-Value Pair Arguments
%   fctConv        : Fonction de conversion binaire --> ASCII : 'dayStr2Ifr', 'hourStr2Ifr', etc ...
%
% Examples
%   figure; imagesc(rand(10));
%   h = colorbar;
%   chgLabelsColorbar(h, -200, 1000)
%
%   figure; imagesc(rand(10));
%   h = colorbar;
%   chgLabelsColorbar(h, 1, 2, 'fctConv', 'exp')
%
%   % chgLabelsColorbar is used in cl_car_nav_data/plot
%   nomFic = getNomFicDatabase('EM300_Ex1.nvi')
%   a = cl_car_nav_data(nomFic);
%   plot(a, 'coul', 'mbTime', '+');
%
% See also cl_car_nav_data/plot Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function chgLabelsColorbar(handleColorbar, valMin, valMax, varargin)

[varargin, fctConv] = getPropertyValue(varargin, 'fctConv', []); %#ok<ASGLU>

couleur = colormap;
Ncoul = size(couleur, 1);
pente = (Ncoul - 1) / (valMax - valMin);

s = get(handleColorbar, 'YTickLabel');
for k=1:size(s,1)
    i = str2double(s(k,:));
    zbas  = valMin + (1/pente) * (i - 1);
    zhaut = valMin + (1/pente) * i;
    snew{k} = (zbas + zhaut) / 2; %#ok
    
    if ~isempty(fctConv)
        commande = sprintf('%s(%f)', fctConv, snew{k});
        snew{k} = evalin('caller', commande);
    end
end
set(handleColorbar, 'YTickLabel', snew);
