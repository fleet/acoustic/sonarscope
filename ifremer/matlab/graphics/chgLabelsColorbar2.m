% Changement des labels d'une colorbar par interpolation lineaire entre valMin et valMax.
%
% Syntax
%   chgLabelsColorbar2(handleColorbar, fctConv)
% 
% Input Arguments 
%   handleColorbar : handle de la colormap
%   valMin         : valeur correspondant a la premiere couleur 
%   valMax         : valeur correspondant a la derniere couleur 
% 
% Name-Value Pair Arguments
%   fctConv        : Fonction de conversion binaire --> ASCII : 'dayStr2Ifr', 'hourStr2Ifr', etc ...
%
% Examples
%   figure; imagesc(log10(rand(10)*1000));
%   h = colorbar;
%   chgLabelsColorbar2(h, 'exp')
%
% See also cl_car_nav_data/plot Authors
% Authors : JMA
% VERSION  : $Id: chgLabelsColorbar2.m,v 1.6 2002/06/04 14:42:25 augustin Exp $
%-------------------------------------------------------------------------------

function chgLabelsColorbar2(handleColorbar, fctConv)

s = get(handleColorbar, 'YTickLabel');
for k=1:size(s,1)
    commande = sprintf('%s(%s)', fctConv, s(k,:));
    pppp = evalin('caller', commande);
    pppp = num2str(pppp);
    s2(k,1:length(pppp)) = pppp;
end
set(handleColorbar, 'YTickLabel', s2);
