% Exportation d'une courbe dans une nouvele figure
%
% Syntax
%   cloneCourbe(hIn)
%
% Input Arguments
%   hIn : Handle de la courbe � exporter
%
% Examples
%   figure;
%   hIn = plot(1:10, 'r+');
%   cloneCourbe(hIn)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function cloneCourbe(hIn, varargin)

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',     []);
[varargin, flag_time] = getPropertyValue(varargin, 'cl_time', 0);

[varargin, noZData]   = getFlag(varargin, 'noZData'); %#ok<ASGLU>

hAxeIn = get(hIn, 'parent');
Titre = get(get(hAxeIn, 'Title'), 'String');
XLim = get(hAxeIn, 'XLim');

XData = get(hIn, 'XData');
YData = get(hIn, 'YData');
ZData = get(hIn, 'ZData');

sub = (XData >= XLim(1)) & (XData <= XLim(2));
XData = XData(sub);
YData = YData(sub);

if ~isempty(ZData)
    ZData = ZData(sub);
end

switch get(hIn, 'Type')
    case 'patch'
        isPatch = true;
        Proprietes = {'EdgeColor'; 'FaceColor'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'; 'ButtonDownFcn'};
    otherwise
        isPatch = false;
        Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'; 'ButtonDownFcn'};
end


if isempty(Fig)
    figure;
else
    figure(Fig); hold on;
end
if isPatch
    h = my_patch(XData, YData, [1 1 1]);
else
    if isempty(ZData) || noZData
        h = PlotUtils.createSScPlot(XData, YData, 'cl_time', flag_time);
    else
        h = PlotUtils.createSScPlot3(XData, YData, ZData);
    end
end

grid on;
title(Titre, 'Interpreter', 'none')
set(gca, 'XLim', XLim)

for i=1:length(Proprietes)
    P = get(hIn, Proprietes{i});
    set(h, Proprietes{i}, P)
end

% for i=1:length(Proprietes)
%     i
%     Proprietes{i}
%     P = get(hIn, Proprietes{i})
% end

%{
EraseMode: 'normal'
MarkerEdgeColor: 'auto'
MarkerFaceColor: 'none'
BeingDeleted: 'off'
Children: [0x1 double]
Clipping: 'on'
CreateFcn: []
DeleteFcn: []
BusyAction: 'queue'
HandleVisibility: 'on'
HitTest: 'on'
Interruptible: 'on'
Selected: 'off'
SelectionHighlight: 'on'
Tag: ''
Type: 'line'
UIContextMenu: 1.5200e+003
UserData: []
Visible: 'on'
Parent: 1.1700e+003
DisplayName: ''
XDataMode: 'manual'
XDataSource: ''
YDataSource: ''
ZDataSource: ''
%}
