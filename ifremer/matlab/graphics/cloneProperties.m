function cloneProperties(hIn, hOut)
Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'
    'ButtonDownFcn'};

for i=1:length(Proprietes)
    try
        P = get(hIn, Proprietes{i});
        set(hOut, Proprietes{i}, P)
    catch %#ok<CTCH>
    end
end

set(0,'CurrentFigure', ancestor(hOut, 'Figure'))
cmenu = uicontextmenu;
pppp = get(hIn, 'UIContextMenu');

cmenu = copyProprietes(pppp, cmenu, []);
set(hOut, 'UIContextMenu', cmenu);

function [cmenu, hOut] = copyProprietes(pppp, cmenu, Parent)
hOut = [];
if ishandle(pppp)
    hIn = get(pppp, 'Children');
    for i=length(hIn):-1:1
%         Label = get(hIn(i), 'Label');
        Label = get(hIn(i), 'Text'); % Modif JMA le 20/05/2021
        Callback = get(hIn(i), 'Callback');

        Children = get(hIn(i), 'Children');
        if isempty(Children)
            if isempty(Parent)
                hOut = uimenu(cmenu, 'Text', Text, 'Callback', Callback);
            else
                hOut = uimenu(cmenu, 'Parent', Parent, 'Text', Label, 'Callback', Callback);
            end
        else
            if isempty(Parent)
                hOut = uimenu(cmenu, 'Text', Label);
            else
                hOut = uimenu(cmenu, 'Parent', Parent, 'Text', Label);
            end
            cmenu = copyProprietes(hIn(i), cmenu, hOut);
        end
    end
end