% FONCTION MORIBONDE
% UTILISEE DANS CLCar... SERA SUPRIMEE DANS V2
% Clonage d'un objet graphique
%
%  hOut = cloneUicontrol(hIn, 'PropertyName', PropertyValue)
%
% Input Arguments
%   hIn : Objet graphique devant etre clone
%
% PROPERTY NAMES :
%   'Saison' : {'hiver'} | 'printemps' | 'ete' | 'AUTOMNE'
%
% Output Arguments
%   hOut : Le clown. Oh pardon, le clone.
%
% Examples
%   hOut = cloneUicontrol(hIn, 'tag', 'CloneDeLAutreClown')
%
% See also help uicontrol Authors
% Authors : JMA
% VERSION : $Id: cloneUicontrol.m,v 1.2 2001/03/28 13:19:23 augustin Exp $
%-------------------------------------------------------------------------------

function hOut = cloneUicontrol(hIn, varargin)

Type = get(hIn, 'Type');
switch Type
    case 'uicontrol'
        hOut = uicontrol('Units', get(hIn, 'Units'));
        s = get(hIn);
        names = fieldnames(s);
        for i=1:length(names)
            switch names{i}
                case {'Extent'; 'Type'}
                otherwise
                    valeur = get(hIn, names{i});
                    set(hOut, names{i}, valeur);
            end
        end
    case 'axes'
        hOut = axes;

        s = set(hIn);
        names = fieldnames(s);
        for i=1:length(names)
            valeur = get(hIn, names{i});

            switch names{i}
                case 'Title'
                    htextOut = title('');
                    cloneText(htextOut, valeur);
                case 'XLabel'
                    htextOut = xlabel('');
                    cloneText(htextOut, valeur);
                case 'YLabel'
                    htextOut = ylabel('');
                    cloneText(htextOut, valeur);
                case 'ZLabel'
                    htextOut = zlabel('');
                    cloneText(htextOut, valeur);
                case {'CurrentPoint'; 'Type'}
                case {'Children'}
                    for j=1:length(valeur)
                        childAxeType = get(valeur(j), 'Type');
                        switch childAxeType
                            case 'text'
                                htextOut = text;
                                cloneText(htextOut, valeur(j));
                            case 'line'
                                hlineOut = line;
                                cloneLine(hlineOut, valeur(j));
                            otherwise
                                disp('erreur ghjkrt')
                        end
                    end
                otherwise
                    % 			valeur = get(hIn, names{i});
                    set(hOut, names{i}, valeur)
            end
        end
    otherwise
        % Uimenu, Uicontextmenu pas traites
        disp('erreur')
        disp(Type)
end

% --------------------------------------------------
% Reaffectation des proprietes passees en parametres

set(hOut, varargin{:});



function cloneText(htextOut, valeur)

handleStr = get(valeur);
namesText = fieldnames(handleStr);
for j=1:length(namesText)
    switch namesText{j}
        %case {'Extent'; 'Type'; 'Children'}
        case 'BeingDeleted'
        case {'Extent'; 'Type'}
        otherwise
            valeurText = get(valeur,namesText{j});
            set(htextOut, namesText{j}, valeurText)
    end
end



function cloneLine(htextOut, valeur)

handleStr = get(valeur);
namesText = fieldnames(handleStr);
for j=1:length(namesText)
    switch namesText{j}
        %case {'Extent'; 'Type'; 'Children'}
        case 'BeingDeleted'
        case {'Extent'; 'Type'; 'parent'}
        case {'XData'; 'YData'}
            XData = get(valeur, 'XData');
            YData = get(valeur, 'YData');
            set(htextOut, 'XData', XData, 'YData', YData);
        otherwise
            valeurText = get(valeur,namesText{j});
            set(htextOut, namesText{j}, valeurText)
    end
end
