% Ferme une fen�tre contenant un GIF anim�.
%
% Syntax
%   closeGifPlayer(handles)
%
% Examples
%   [flag, hdl] = gifplayer('title', Lang('Import en cours', 'Import in Progress'));
%   if flag
%       closeGifPlayer(hdl);
%   end
%
% See also gifplayer Authors
% Authors : GLU
%-------------------------------------------------------------------------------

function closeGifPlayer(handles)
    
if isempty(handles)
    return
end

if ishandle(handles) || ~isfield(handles, 'tmr') || ~isfield(handles, 'guifig')
    close(handles);
    return
end

% Stop and removes the timer from memory
% Si l'uilisateur n'a pas ferm� la fen�tre
if isvalid(handles.tmr)
    stop(handles.tmr);
    delete(handles.tmr);
    % Fermeture de la fen�tre.
    my_close(handles.guifig);
end


