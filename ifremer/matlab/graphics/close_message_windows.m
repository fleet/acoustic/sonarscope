% Effacement des fen�tres ouvertes
%
% Syntax
%   close_message_windows
%
% Remarks : Les fen�tres qui ont un num�ro qui ne sont pas entiers sont
% d�truites automatiquement. Pour les autres seules celles qui ne poss�dent
% pas de "UserData" sont propos�es � la destruction
%
% Examples
%   close_message_windows
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function close_message_windows

%% Suppression automatique de toutes les fen�tres qui n'ont pas de num�ro de figures entier

h = allchild(0);
for k=1:length(h)
    if ishandle(h(k))
        flag = get(h(k), 'IntegerHandle');
        % Si c'est pas suffisant y'a d'autres pistes
        % BackingStore = off DockControls = off
        if strcmp(flag, 'off')
            close(h(k))
            continue
        end
    end
end

%% Recherche de toutes les figures ne poss�dant pas de UserData

hFig = matlab.ui.Figure.empty;
Title = {};
h = allchild(0);
for k=1:length(h)
    flag = get(h(k), 'UserData'); 
    if isempty(flag)
        hFig(end+1) = h(k); %#ok<AGROW>
        Name = get(h(k), 'Name');
        if isempty(Name)
            if my_verLessThanR2014b
                Title{end+1} = sprintf('Figure %d', h(k)); %#ok<AGROW>
            else
                Title{end+1} = sprintf('Figure %d', h(k).Number); %#ok<AGROW>
            end
        else
            Title{end+1} = Name; %#ok<AGROW>
        end
    end
end

%% S�lection de figures � d�truire

str1 = 'Figures � fermer';
str2 = 'Figures to close';
[rep, flag] = my_listdlgMultiple(Lang(str1,str2), Title, 'InitialValue', 1:length(hFig));
if ~flag
    return
end

%% Destruction des figures s�lectionn�es

try
    close(hFig(rep))
catch ME
    ME
end
