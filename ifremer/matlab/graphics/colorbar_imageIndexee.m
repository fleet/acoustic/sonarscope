% figure;
% image(repmat(uint8(0:4),5,1)); colormap(jet(5));
% colorbar
% colorbar_imageIndexee
%
% figure;
% image(repmat(uint8(1:5),5,1)); colormap(jet(6));
% colorbar
% colorbar_imageIndexee

function h = colorbar_imageIndexee(varargin)

[varargin, Offset] = getPropertyValue(varargin, 'Offset', 0);
[varargin, Names] = getPropertyValue(varargin, 'Names', []);

h = colorbar(varargin{:});
hi = get(h, 'Children');
if isempty(hi)
    return
end

ZLim = get(hi, 'YData');
% ZLim = ZLim -1;
set(hi, 'YData', ZLim+Offset)

if my_verLessThanR2014b
    set(h, 'YLim', [ZLim(1)-0.5+Offset ZLim(2)+0.5+Offset])
else
    set(h, 'Limits', [ZLim(1)-0.5+Offset ZLim(2)+0.5+Offset])
end

if ~isempty(Names)
    legend(Names)
end
