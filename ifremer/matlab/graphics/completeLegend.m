function str = completeLegend(str)

ha = gca;
if ~isappdata(double(ha), 'LegendPeerHandle')
    return
end
leg = getappdata(double(ha), 'LegendPeerHandle');

% if ~ishandle(leg) || ~isequal(get(leg,'Axes'), handle(ha))
%     rmappdata(double(ha),'LegendPeerHandle');
%     leg = [];
% end

if ishandle(leg)
    legString = leg.String; % TODO : v�rifier en R2011b
    if ischar(legString)
        legString = {legString};
    end
    
    if ischar(str)
        str = {str};
    end
    
    str = [legString(:); str(:)];
end
