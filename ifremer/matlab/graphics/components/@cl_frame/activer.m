% Activation des uicontrols de la frame et des frames contenues
% 
% Syntax
%   activer(this)
%
% Input Arguments
%    this : instance de cl_frame
%
% Examples
%   h = figure; 
%   clear uic axe
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%   uic{2}.Lig   = 1;
%   uic{2}.Col   = 2;
%   uic{2}.Style = 'pushbutton';
%   axe{1}.Lig   = 2;
%   axe{1}.Col   = 2;
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'ListeUicontrols', uic);
%   set(f, 'ListeAxes', axe);
%   ff = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 5, 'InsetY', 5);
%   set(ff, 'HandleFrame', findobj(h, 'Tag', 'container'));
%   set(ff, 'ListeUicontrols', uic);
%   set(f, 'ListeClFrames', ff);
%   activer(f)
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: activer.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    17/01/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function activer(this)

% Maintenance Auto : try
    
    % ----------------------------
    % Desactivation des uicontrols
    
    uic = this.tabUicontrols;
    [m, n] = size(uic);
    
    for i = 1:1:m
        for j = 1:1:n
            set(uic(i, j), 'Enable', 'on');
        end
    end
    
    % --------------------------
    % Desactivation de cl_frames
    
    clf = this.tabClFrames;
    [m, n] = size(clf);
    
    for i = 1:1:m
        for j = 1:1:n
            activer(clf(i, j));
        end
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'activer', lasterr);
% Maintenance Auto : end
