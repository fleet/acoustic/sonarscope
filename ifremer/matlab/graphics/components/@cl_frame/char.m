% Transformation en chaine de caracteres d'un objet de classe cl_frame.
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%    char(cl_frame)
%
% See also cl_frame Authors
% Authors : JMA
% VERSION  : $Id: char.m,v 1.3 2003/03/24 16:35:50 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00   - JMA - creation
%   30/11/00   - DCF - normalisation
%   21/03/2001 - DCF - gestion des erreurs
%   21/03/2001 - DCF - optimisation code
%   23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    % -------------------------
    % Intialisation des locales
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'char', lasterr);
% Maintenance Auto : end
