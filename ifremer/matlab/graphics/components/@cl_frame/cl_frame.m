% Constructeur de cl_frame
%
% Syntax
%   this = cl_frame(...)
% 
% Name-Value Pair Arguments
%   HandleFrame      : handle de la frame
%   ListeUicontrols  : liste des uicontrols
%   ListeAxes        : liste des axes
%   NomFrame         : nom de la frame
%   PositionFrame    : position de la frame
%   Visible          : visibilite de la frame
%   Verification     : verification des valeurs de proprietes
%
% Output Arguments 
%   this : instance de cl_frame
%
% Examples
%    f = cl_frame
%
% See also cl_frame Authors
% Authors : DCF JMA
% ----------------------------------------------------------------------------

function this = cl_frame(varargin)

% --------------------------------------------------------------------------
% Definition de la structure et initialisation
%
% alias             : nom d alias de la frame
% nomFrame          : nom de la frame, tag de la frame matlab
% handleFrame       : handle matlab de la frame
% positionFrame     : position de la frame
% tabUicontrols     : tableau des uicontrols "contenus" par la frame
% ligUicontrols     : liste des positions en ligne des uicontrols contenus
%                     par la frame
% colUicontrols     : liste des positions en colonne des uicontrols
%                     contenus par la frame
% visibleUicontrols : tableau des flags de visibilite des uicontrols
%                     contenus par la frame
% enableuicontrols  : tableau des flags d accessibilite des uicontrols
%                     contenus par la frame
% tabAxes           : tableau des axes contenus par la frame
% ligAxes           : liste des positions en ligne des axes contenus
%                     par la frame
% colAxes           : liste des positions en colonne des axes contenus
%                     par la frame
% visibleAxes       : tableau des flags de visibilite des axes contenus
%                     par la frame
% tabClFrames       : tableau des instances de cl_frame contenues
%                     par la frame
% visibleClFrames   : tableau des flags de visibilite des instances de
%                     cl_frame contenues par la frame
% enableClFrames    : tableau des flags d accessibilite des instances de
%                     cl_frame contenues par la frame
% visible           : visibilite courante de l instance
% enable            : accessibilite courante de l instance
% verification      : verification des proprietes (0 : non; 1 : oui)
% inset             : marge entre composants contenus (entre lig et col)

this.alias             = [];
this.nomFrame          = [];
this.handleFrame       = [];
this.positionFrame     = [];
this.tabUicontrols     = [];
this.ligUicontrols     = [];
this.colUicontrols     = [];
this.visibleUicontrols = [];
this.enableUicontrols  = [];
this.tabAxes           = [];
this.ligAxes           = [];
this.colAxes           = [];
this.visibleAxes       = [];
this.tabClFrames       = [];
this.visibleClFrames   = [];
this.enableClFrames    = [];
this.visible           = [];
this.enable            = [];
this.verification      = 0;
this.inset             = 5;
% this.ihm = []; % Ajout JMA le 10/11/2015

%% Super classe

if (length(varargin) == 1) && isempty(varargin{1})
    layout = cl_abstract_layout([]);
else
    layout = cl_abstract_layout;
end

%% Création de l'objet

this = class(this, 'cl_frame', layout);

%% Préinitialisation des champs

if (length(varargin) == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
