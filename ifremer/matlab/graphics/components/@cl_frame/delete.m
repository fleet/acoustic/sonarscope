% Suppression des instances contenues par cette instance
% 
% Syntax
%   this = delete(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Input Arguments
%   this : instance de cl_frame initialisee
%
% Remarks : ne detruit pas la frame ayant servie de support
%
% Examples
%   h = figure; 
%   clear uic
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%   uic{2}.Lig   = 1;
%   uic{2}.Col   = 2;
%   uic{2}.Style = 'pushbutton';
%
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'Nom', 'f');
%   set(f, 'ListeUicontrols', uic)
%   delete(f)
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % --------------------------
    % Destruction des uicontrols
    
    [m, n] = size(this.tabUicontrols);
    for i = 1:m
        for j=1:n
            delete(this.tabUicontrols(i, j));
        end
    end
    this.tabUicontrols = [];
    
    % --------------------
    % Destruction des axes
    
    [m, n] = size(this.tabAxes);
    for i=1:m
        for j=1:n
            delete(this.tabAxes(i, j));
        end
    end
    this.tabAxes = [];
    
    % -------------------------------
    % Oubli des instances de cl_frame
    
    [m, n] = size(this.tabClFrames);
    for i=1:m
        for j=1:n
            delete(this.tabClFrames(i, j));
        end
    end
    this.tabClFrames = [];
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'delete', lasterr);
% Maintenance Auto : end
