% Desactivation des uicontrols de la frame et des frames contenues
% 
% Syntax
%   desactiver(this)
%
%
% Input Arguments
%   this : instance de cl_frame
%
% Examples
%   h = figure; 
%   clear uic axe
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1  ;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%   uic{2}.Lig   = 1  ;
%   uic{2}.Col   = 2  ;
%   uic{2}.Style = 'pushbutton';
%   axe{1}.Lig   = 2;
%   axe{1}.Col   = 2;
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'ListeUicontrols', uic);
%   set(f, 'ListeAxes', axe);
%   ff = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 5, 'InsetY', 5);
%   set(ff, 'HandleFrame', findobj(h, 'Tag', 'container'));
%   set(ff, 'ListeUicontrols', uic);
%   set(f, 'ListeClFrames', ff);
%   desactiver(f)
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: desactiver.m,v 1.4 2003/05/20 15:57:01 augustin Exp $
% ----------------------------------------------------------------------------

function desactiver(this)
% ----------------------------
% Desactivation des uicontrols

uic = this.tabUicontrols;
[m, n] = size(uic);

for i = 1:1:m
    for j = 1:1:n
        set(uic(i, j), 'Enable', 'off');
    end
end

% --------------------------
% Desactivation de cl_frames

clf = this.tabClFrames;
[m, n] = size(clf);

for i = 1:1:m
    for j = 1:1:n
        desactiver(clf(i, j));
    end
end
