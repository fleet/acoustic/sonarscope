% Affichage des donnees d'une instance de cl_frame
%
% Syntax
%   display(this)
% 
% Input Arguments 
%   this : instance de cl_frame
%
% Examples
%   a = cl_frame
%   display(a)
%
% See also cl_frame Authors
% Authors : NS
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
