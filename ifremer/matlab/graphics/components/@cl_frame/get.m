% Lecture des attributs de l'instance
%
% Syntax
%   varargout = get(this, ...)
%
% Input Arguments
%   this : instance de cl_frame
%
% Name-only Arguments
%   Alias           : nom d'alias de la frame
%   NomFrame        : nom de tag de la frame matlab
%   PositionFrame   : position de la frame
%   HandleFrame     : handle de la frame
%   Visible         : visibilite de la frame
%   Verification    : flag de verification des noms de proprietes des
%                     instances matlab supportees par la frame
%   MaxLig          : nombre maximal de lignes
%   MaxCol          : nombre maximal de colonnes
%   InsetX          : marge selon l axe x
%   InsetY          : marge selon l axe y
%   Enable          : accessibilite de la frame
%   ListeUicontrols : liste des instances des uicontrol
%   ListeAxes       : liste des instances des axes
%   ListeClFrames   : liste des instances des cl_frame
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
%
% Examples
%   f = cl_frame('NomFrame', 'nomTestFrame');
%   get(f, 'NomFrame')
%
% See also cl_frame Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case {'Alias','Nom'}
            varargout{end+1} = this.alias; %#ok<AGROW>
        case 'NomFrame'
            varargout{end+1} = this.nomFrame; %#ok<AGROW>
        case 'PositionFrame'
            varargout{end+1} = this.positionFrame; %#ok<AGROW>
        case 'HandleFrame'
            varargout{end+1} = this.handleFrame; %#ok<AGROW>
        case 'Visible'
            varargout{end+1} = this.visible; %#ok<AGROW>
        case 'Verification'
            varargout{end+1} = this.verification; %#ok<AGROW>
        case 'MaxLig'
            varargout{end+1} = get_max_lig(this.cl_abstract_layout); %#ok<AGROW>
        case 'MaxCol'
            varargout{end+1} = get_max_col(this.cl_abstract_layout); %#ok<AGROW>
        case 'InsetX'
            varargout{end+1} = get_inset_x(this.cl_abstract_layout); %#ok<AGROW>
        case 'InsetY'
            varargout{end+1} = get_inset_y(this.cl_abstract_layout); %#ok<AGROW>
        case 'Enable'
            varargout{end+1} = this.enable; %#ok<AGROW>
        case 'ListeUicontrols'
            varargout{end+1} = this.tabUicontrols; %#ok<AGROW>

        case 'ListeAxes'
            varargout{end+1} = this.tabAxes; %#ok<AGROW>
        case 'ListeClFrames'
            varargout{end+1} = this.tabFrames; %#ok<AGROW>
        otherwise
            my_warndlg('cl_frame/get : too many arguments', 0);
    end
end
