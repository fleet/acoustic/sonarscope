% Accesseur en lecture de l alias de la frame
% 
% Syntax
%   alias = get_alias(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   alias : alias
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: get_alias.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function alias = get_alias(this)

% Maintenance Auto : try
    alias = this.alias;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_alias', lasterr);
% Maintenance Auto : end
