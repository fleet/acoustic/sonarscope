% Accesseur en lecture de l accessibilite de la frame
% 
% Syntax
%   enable = get_enable(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   enable : enable
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: get_enable.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function enable = get_enable(this)

% Maintenance Auto : try
    enable = this.enable;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_enable', lasterr);
% Maintenance Auto : end
