% Accesseur en lecture du handle matlab de la frame
%
% Syntax
%   handleFrame = get_handle_frame(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   handleFrame : handleFrame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function handleFrame = get_handle_frame(this)
handleFrame = this.handleFrame;
