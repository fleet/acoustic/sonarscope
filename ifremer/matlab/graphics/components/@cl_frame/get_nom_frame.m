% Accesseur en lecture du nom de la frame (tag matlab)
%
% Syntax
%   nomFrame = get_nom_frame(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   nomFrame : nomFrame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function nomFrame = get_nom_frame(this)
nomFrame = this.nomFrame;
