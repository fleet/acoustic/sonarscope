% Accesseur en lecture de la position de la frame
% 
% Syntax
%   positionFrame = get_position_frame(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   positionFrame : positionFrame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: get_position_frame.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function positionFrame = get_position_frame(this)

% Maintenance Auto : try
    positionFrame = this.positionFrame;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_position_frame', lasterr);
% Maintenance Auto : end
