% Accesseur en lecture du tableau des axes de la frame
% 
% Syntax
%   tabAxes = get_tab_axes(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   tabAxes : tabAxes
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: get_tab_axes.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tabAxes = get_tab_axes(this)

% Maintenance Auto : try
    tabAxes = this.tabAxes;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_tab_axes', lasterr);
% Maintenance Auto : end
