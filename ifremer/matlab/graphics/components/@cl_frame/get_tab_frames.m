% Accesseur en lecture du tableau des instances de cl_frame
% 
% Syntax
%   tabFrames = get_tab_frames(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   tabFrames : tabFrames
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: get_tab_frames.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tabFrames = get_tab_frames(this)

% Maintenance Auto : try
    tabFrames = this.tabFrames;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_tab_frames', lasterr);
% Maintenance Auto : end
