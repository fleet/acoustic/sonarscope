% Accesseur en lecture du tableau des uicontrols de la frame
% 
% Syntax
%   tabUicontrols = get_tab_uicontrols(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   tabUicontrols : tabUicontrols
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: get_tab_uicontrols.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tabUicontrols = get_tab_uicontrols(this)

% Maintenance Auto : try
    tabUicontrols = this.tabUicontrols;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_tab_uicontrols', lasterr);
% Maintenance Auto : end
