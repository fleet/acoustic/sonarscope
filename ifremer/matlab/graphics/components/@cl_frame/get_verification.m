% Accesseur en lecture du flag de verification de la frame
% 
% verification = get_verification(this)
%
% Input Arguments
%    this : instance de cl_frame
%
% Output Arguments
%    verification : verification
%
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function verification = get_verification(this)

% Maintenance Auto : try
    verification = this.verification;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_verification', lasterr);
% Maintenance Auto : end
