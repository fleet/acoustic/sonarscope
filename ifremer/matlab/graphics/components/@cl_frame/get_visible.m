% Accesseur en lecture de la visibilite de la frame
% 
% Syntax
%   visible = get_visible(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   visible : visible
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: get_visible.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function visible = get_visible(this)

% Maintenance Auto : try
    visible = this.visible;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'get_visible', lasterr);
% Maintenance Auto : end
