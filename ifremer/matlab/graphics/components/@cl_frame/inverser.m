% Inversion des lignes et colonnes
% 
% Syntax
%   this = inverser(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% OUPUT PARAMETERS :
%   this : instance de cl_frame initialisee
%
% Examples
%   h = figure; 
%   clear uic
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'Nom', 'f');
%   set(f, 'ListeUicontrols', uic);
%   f = inverser(f);
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: inverser.m,v 1.4 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    21/03/2001 - DCF - maj optimisation code
%    13/04/2001 - DCF - correction appel set_max_lig et set_max_col
%    23/04/2001 - DCF - mise en reference
%    24/04/2001 - DCF - correction appel set_max_lig et set_max_col,
%                       inversion du nombre de lignes et colonnes
% ----------------------------------------------------------------------------

function this = inverser(this)

% Maintenance Auto : try
    
    % ----------------------------------------
    % Inversion des coordonnees des composants
    
    maxLig        = get_max_lig(this);
    maxCol        = get_max_col(this);
    ligUicontrols = this.ligUicontrols;
    colUicontrols = this.colUicontrols;
    ligAxes       = this.ligAxes;
    colAxes       = this.colAxes;
    
    this = set_max_lig(this, maxCol);
    this = set_max_col(this, maxLig);
    this.ligUicontrols = colUicontrols;
    this.colUicontrols = ligUicontrols;
    this.ligAxes       = colAxes;
    this.colAxes       = ligAxes;
    
    % ------------------------------
    % Inversion des frames contenues
    
    n = size(this.tabClFrames, 2);
    for i=1:n
        this.tabClFrames(i) = inverser(this.tabClFrames(i));
    end
    
    % -----------
    % Mise a jour
    
    this = update(this);
    
    % --------------------
    % Traitement du retour
    
    if nargout == 0
        assignin('caller', inputname(1), this);
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'inverser', lasterr);
% Maintenance Auto : end
