% Déplacement d'une instance sur une nouvelle position
%
% Syntax
%   move(this, handle, lig, col)
%
% Input Arguments
%   this   : instance de cl_frame
%   handle : handle de l instance a deplacer
%   lig    : indices de lignes
%   col    : indices des colonnes
%
% Examples
%   h = figure;
%   hf = uicontrol('Style', 'frame', 'Position', [10 10 400 300]);
%   set(hf, 'BackgroundColor', [.6 .8 .6]);
%   ho = uicontrol('Style', 'pushbutton', 'String', 'OK');
%   f = cl_frame('HandleFrame', hf, 'InsetX', 10, 'InsetY', 10);
%   set(f, 'MaxLig', 10, 'MaxCol', 10);
%   move(f, ho, 3:5, 3:7);
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function move(this, handle, lig, col)

%% Test de validite du handle

% if ~ishandle(handle)
%     my_warndlg('cl_frame/move : invalid handle', 0);
% elseif get_max_lig(this) < lig
%     my_warndlg('cl_frame/move : invalid value for lig', 0);
% elseif get_max_col(this) < col
%     my_warndlg('cl_frame/move : invalid value for col', 0);
% else

    %% Lecture de l'unité de la frame

    units = get(this.handleFrame, 'Units');

    %% Redétermination de la position actuelle en pixels (de la frame de ref)

    set(this.handleFrame, 'Units', 'pixels');
    p = get(this.handleFrame, 'Position');
    set(this.handleFrame, 'Units', units);
    if isAGraphicElement(this.handleFrame, 'figure') == 1
        p(1:2) = 0;
    end
    this = set_position_frame(this, p);

    %% Calcul de la position et repositionnement

    p = calculer_position(this, lig, col);
%     if isempty(p)
%         msg = 'cl_frame::move, invalid position';
%         warndlg(msg, 'Warning');
%     else
        set(handle, 'Units', 'pixels', 'Position', p, 'Units', 'normalized');
%     end
% end
