% Initialisation pour la frame dont le nom est specifie (instance inclue)
%
% Syntax
%   this = named_set(this, name, ...)
%
% Input Arguments
%   this : instance de cl_frame
%   name     : nom de l instance de cl_frame devant executer le set
%   varargin : proprietes (nom, valeur) telles que precisees dans set
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% Examples
%   h = figure;
%   clear uic axe
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%   uic{2}.Lig   = 1;
%   uic{2}.Col   = 2;
%   uic{2}.Style = 'pushbutton';
%   axe{1}.Lig   = 2;
%   axe{1}.Col   = 2;
%
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'Nom', 'f');
%   set(f, 'ListeUicontrols', uic);
%   set(f, 'ListeAxes', axe);
%
%   ff = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 5, 'InsetY', 5);
%   set(ff, 'Nom', 'ff');
%   set(ff, 'HandleFrame', findobj(h, 'Tag', 'container'));
%   set(ff, 'ListeUicontrols', uic);
%
%   set(f, 'ListeClFrames', ff);
%
%   f = named_set(f, 'f', 'Visible', 'off');
%   f = named_set(f, 'ff', 'Visible', 'on');
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = named_set(this, name, varargin)

% ------------------------------------------------------------------------
% Traitement de la visibilite a part : traite la visibilite d une instance
% si l instance courante est visible

[varargin, visible] = getPropertyValue(varargin, 'Visible', []);
if ~isempty(visible)
    if (strcmp(this.visible, 'on') == 1)
        this = internal_set(this, name, 'Visible', visible);
    elseif (strcmp(this.alias, name) == 1)
        this = set_visible(this, visible);
    end
else
    this = internal_set(this, name, varargin{:});
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
