% Applique l enable aux instances graphiques "contenues"
%
% Syntax
%   this = appliquer_enable(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: appliquer_enable.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    29/01/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = appliquer_enable(this)

% Maintenance Auto : try
    
    % -------------------------
    % Traitement des uicontrols
    
    this = appliquer_enable_uicontrol(this);
    
    % -----------------------
    % Traitement des cl_frame
    
    this = appliquer_enable_cl_frame(this);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'appliquer_enable', lasterr);
% Maintenance Auto : end
