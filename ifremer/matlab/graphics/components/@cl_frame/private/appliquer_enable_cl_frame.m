% Applique l enable aux instances de cl_frame "contenues"
%
% Syntax
%   this = appliquer_enable_cl_frame(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: appliquer_enable_cl_frame.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    29/01/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = appliquer_enable_cl_frame(this)

% Maintenance Auto : try
    
    % ------------------------------------
    % Traitement des instances de cl_frame
    
    [m, n] = size(this.tabClFrames);
    
    for i = 1:m
        for j = 1:n
            if this.enableClFrames(i, j)
                enable = 'on';
            else
                enable = 'off';
            end
            this.tabClFrames(i, j) = set(this.tabClFrames(i, j), 'enable', enable);
        end
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'appliquer_enable_cl_frame', lasterr);
% Maintenance Auto : end
