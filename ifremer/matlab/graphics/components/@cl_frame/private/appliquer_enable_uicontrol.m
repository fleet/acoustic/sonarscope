% Applique l enable aux uicontrols "contenus"
%
% Syntax
%   this = appliquer_enable_uicontrol(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = appliquer_enable_uicontrol(this)

for k=1:length(this.tabUicontrols)
    if this.enableUicontrols(k)
        enable = 'on';
    else
        enable = 'off';
    end
    set(this.tabUicontrols(k), 'enable', enable);
end
