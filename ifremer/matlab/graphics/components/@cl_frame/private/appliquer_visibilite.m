% Applique la visibilite aux instances graphiques "contenues"
%
% Syntax
%   this = appliquer_visibilite(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = appliquer_visibilite(this)

%% Traitement des uicontrols

this = appliquer_visibilite_uicontrol(this);

%% Traitement des axes

this = appliquer_visibilite_axe(this);

%% Traitement des cl_frame

this = appliquer_visibilite_cl_frame(this);
