% Applique la visibilite aux axes "contenus"
%
% Syntax
%   this = appliquer_visibilite_axe(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = appliquer_visibilite_axe(this)

%% Traitement des axes

for k=1:length(this.tabAxes)
    if this.visibleAxes(k)
        visibilite = 'on';
    else
        visibilite = 'off';
    end

    set(this.tabAxes(k), 'Visible', visibilite);
    set(get(this.tabAxes(k), 'Children'), 'Visible', visibilite);
end
