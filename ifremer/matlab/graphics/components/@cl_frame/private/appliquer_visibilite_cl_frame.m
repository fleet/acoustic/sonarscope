% Applique la visibilite aux instances de cl_frame "contenues"
%
% Syntax
%   this = appliquer_visibilite_cl_frame(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = appliquer_visibilite_cl_frame(this)

%% Traitement des instances de cl_frame

[m, n] = size(this.tabClFrames);
for k1=1:m
    for k2=1:n
        if this.visibleClFrames(k1, k2)
            visibilite = 'on';
        else
            visibilite = 'off';
        end
        this.tabClFrames(k1, k2) = set(this.tabClFrames(k1, k2), 'Visible', visibilite);
    end
end
