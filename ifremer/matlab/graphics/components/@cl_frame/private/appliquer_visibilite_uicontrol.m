% Applique la visibilite aux uicontrols "contenus"
%
% Syntax
%   this = appliquer_visibilite_uicontrol(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = appliquer_visibilite_uicontrol(this)

for k=1:length(this.tabUicontrols)
    if this.visibleUicontrols(k)
        visibilite = 'on';
    else
        visibilite = 'off';
    end
    set(this.tabUicontrols(k), 'Visible', visibilite);
end
