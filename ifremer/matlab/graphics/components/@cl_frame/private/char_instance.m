% Transformation en chaine de caracteres d'un objet de classe cl_frame.
%
% Syntax
%   str = char_instance(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% See also cl_frame Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

%% Traitement

str{end+1} = sprintf('alias             <-> %s', this.alias);
str{end+1} = sprintf('nomFrame          <-> %s', this.nomFrame);
% str{end+1} = sprintf('handleFrame       <-> %f', this.handleFrame);
str{end+1} = sprintf('positionFrame     <-> %s', num2strCode(this.positionFrame));
% str{end+1} = sprintf('tabUicontrols     <-> %s', num2strCode(this.tabUicontrols));
str{end+1} = sprintf('visibleUicontrols <-> %s', num2strCode(this.visibleUicontrols));
str{end+1} = sprintf('enableUicontrols  <-> %s', num2strCode(this.enableUicontrols));
% str{end+1} = sprintf('tabAxes           <-> %s', num2strCode(this.tabAxes));
str{end+1} = sprintf('visibleAxes       <-> %s', num2strCode(this.visibleAxes));
str{end+1} = sprintf('visible           <-> %s', this.visible);
str{end+1} = sprintf('enable            <-> %s', this.enable);
str{end+1} = sprintf('verification      <-> %s', num2strCode(this.verification));

str{end+1} = sprintf('--- heritage cl_abstract_layout ---');
str{end+1} = char(this.cl_abstract_layout);

%% Concatenation en une chaine

str = cell2str(str);
