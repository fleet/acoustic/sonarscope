% Creation des axes (instances matlab) a partir d une liste descriptive
%
% Syntax
%   this = create_axe(this, liste)
%
% Input Arguments
%   this  : instance de cl_frame
%   liste : liste de structure decrivant les axes
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: create_axe.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = create_axe(this, liste)

% --------------------------
% Raz de la liste precedente

this.tabAxes     = [];
this.visibleAxes = [];

% ---------------------------------------------------------------------
% Liste descriptive des axes : parcours et creation pour chaque element

while ~isempty(liste)

    % ------------------------------------------------------------------------
    % Creation du premier element de la liste Deux procedures sont
    % disponibles :
    % - sans verification : pas de verification de la valeur de la propriete
    %   et pas de verification minuscule/majuscule
    % - avec verification : verification de la valeur de la propriete et
    %   verification minuscule/majuscule du nom de la propriete et de sa valeur

    if this.verification
        handle = construireAxeVerif(liste {1});
    else
        handle = construireAxeFast(liste{1});
    end

    % --------------------------------------------------------------
    % Test de creation,
    % ajout a la liste des axes "contenus",
    % ajout du flag de visibilite a la liste des flags de visibilite

    if ishandle(handle)
        this.tabAxes = [this.tabAxes, handle];
        this.visibleAxes = ...
            [this.visibleAxes, strcmp(get(handle,'Visible'),'on')];
    else
        msg = 'cl_frame::create_axe, can''t create an axe';
        my_warndlg(msg, 1);
    end

    % ------------------------------------------------------
    % condition d arret : vide la liste de l element courant
    if length(liste) > 2
        liste = liste(2:length(liste));
    elseif length(liste) == 2
        liste = liste(2);
    else
        liste = {};
    end

end



% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Fonctions locales
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------



% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% construction d un axe a partir de sa structure descriptive
% Version sans test pour accelerer le processus de creation

function handle = construireAxeFast(structure)

% ----------------------------------------------------------------------------
% Acces aux noms des champs de la structure

names  = fieldnames(structure);
values = struct2cell(structure);

% ----------------------------------------------------------------------------
% creation de l axe

handle = axes;

% ----------------------------------------------------------------------------
% parcours de la structure pour initialisation. On verifie la validite
% du champ que l on renseigne

lstProperties = [];
for i = 1:length(names)
    if ~strcmpi(names{i}, 'lig') && ~strcmpi(names{i}, 'col')
        lstProperties {end+1} = names{i};
        lstProperties {end+1} = values {i};
    end
end
if ~isempty(lstProperties)
    set(handle, lstProperties{:});
end



% % ----------------------------------------------------------------------------
% % ----------------------------------------------------------------------------
% % construction d un axe a partir de sa structure descriptive
%
% function handle = construireAxe(structure)
%
% % ----------------------------------------------------------------------------
% % Acces aux noms des champs de la structure
%
% names = fieldnames(structure);
%
% % ----------------------------------------------------------------------------
% % creation de l axe
%
% handle = axes;
%
% % ----------------------------------------------------------------------------
% % lecture des proprietes accessibles en ecriture
%
% propertyNames = lower(fieldnames(set(handle)));
%
% % ----------------------------------------------------------------------------
% % parcours de la structure pour initialisation. On verifie la validite
% % du champ que l on renseigne
%
% for i = 1:length(names)
%
%     switch lower(names{i})
%
%     case propertyNames
%         set(handle, names{i}, structure.(names{i}));
%
%     case {'lig', 'col'}
%         % rien a faire !
%
%     otherwise
%         msg ='cl_frame::create_axe, construireAxe, invalid property : ';
%         msg = [msg, names {i}];
%         warndlg(msg, 'Warning');
%     end
%
% end
%


% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% construction d un axe a partir de sa structure descriptive

function handle = construireAxeVerif(structure)

% ----------------------------------------------------------------------------
% Acces aux noms des champs de la structure

names = fieldnames(structure);

% ----------------------------------------------------------------------------
% creation de l axe

handle = axes;

% ----------------------------------------------------------------------------
% lecture des proprietes accessibles en ecriture

properties    = set(handle);
propertyNames = fieldnames(properties);

% ----------------------------------------------------------------------------
% parcours de la structure pour initialisation. On verifie la validite
% du champ que l on renseigne

for i = 1:length(names)

    switch names{i}
        case propertyNames

            % ------------------------------------------------------------------------
            % Test de reconnaissance de la valeur

            valeursPossibles = properties.(names{i});
            if ~isempty(valeursPossibles)
                switch structure.(names{i})
                    case valeursPossibles
                        set(handle, names{i}, structure.(names{i}));
                    otherwise
                        msg = 'cl_frame::create_axe, construireAxe, ';
                        msg = [msg, 'invalid property value : '];
                        msg = [msg, structure.(names{i})];
                        my_warndlg(msg, 1);
                end
            else
                set(handle, names{i}, structure.(names{i}));
            end

            set(handle, names{i}, structure.(names{i}));

        case {'Lig', 'Col'}
            % rien a faire !

        otherwise
            msg ='cl_frame::create_axe, construireAxe, invalid property : ';
            msg = [msg, names{i}];
            my_warndlg(msg, 1);
    end

end
