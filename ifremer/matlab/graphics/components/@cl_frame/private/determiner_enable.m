% Determine l enable des instances graphiques "contenues"
%
% Syntax
%   this = determiner_enable(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = determiner_enable(this)

% --------------------
% Enable de uicontrols

this = determiner_enable_uicontrol(this);

% -------------------
% Enable des cl_frame

this = determiner_enable_cl_frame(this);
