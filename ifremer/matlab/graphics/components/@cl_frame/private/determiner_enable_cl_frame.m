% Determine enable des instances de cl_frame "contenues"
%
% Syntax
%   this = determiner_enable_cl_frame(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: determiner_enable_cl_frame.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = determiner_enable_cl_frame(this)

if isempty(this.enable) || strcmp(this.enable, 'none')
    this.enable = 'on';
end

tabEnable = [];
[m, n] = size(this.tabClFrames);

for i=1:m
    for j=1:n
        enable = get(this.tabClFrames(i, j), 'Enable');
        if strcmp(enable, 'off')
            tabEnable(i, j) = 0;
        else
            tabEnable(i, j) = 1;
        end
    end
end

this.enableClFrames = tabEnable;
