% Determine l enable des uicontrols "contenus"
%
% Syntax
%   this = determiner_enable_uicontrol(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: determiner_enable_uicontrol.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = determiner_enable_uicontrol(this)

tabEnable = [];
for i=1:length(this.tabUicontrols)
    enable = get(this.tabUicontrols(i), 'Enable');
    if strcmp(enable, 'on') == 1
        tabEnable(i) = 1;
    else
        tabEnable(i) = 0;
    end
end
this.enableUicontrols = tabEnable;
