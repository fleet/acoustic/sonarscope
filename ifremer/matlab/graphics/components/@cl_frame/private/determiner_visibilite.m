% Determine la visibilite des instances graphiques "contenues"
%
% Syntax
%   this = determiner_visibilite(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this : instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: determiner_visibilite.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/01/2001 - DCF - creation
%    16/01/2001 - DCF - ajout d element de type cl_frame dans les contenus
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = determiner_visibilite(this)

% Maintenance Auto : try
    
    % ------------------------
    % Visibilite de uicontrols
    
    this = determiner_visibilite_uicontrol(this);
    
    % -------------------
    % Visibilite des axes
    
    this = determiner_visibilite_axe(this);
    
    % -----------------------
    % Visibilite des cl_frame
    
    this = determiner_visibilite_cl_frame(this);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'determiner_visibilite', lasterr);
% Maintenance Auto : end
