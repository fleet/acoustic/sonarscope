% Determine la visibilite des axes "contenus"
%
% Syntax
%   this = determiner_visibilite_axe(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: determiner_visibilite_axe.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/01/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = determiner_visibilite_axe(this)

% Maintenance Auto : try
    
    tabVisibilite = [];
    for i=1:length(this.tabAxes)
        
        visibilite = get(this.tabAxes(i), 'Visible');
        if strcmp(visibilite, 'on') == 1
            tabVisibilite(i) = 1;
        else
            tabVisibilite(i) = 0;
        end
        
    end
    this.visibleAxes = tabVisibilite;
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'determiner_visibilite_axe', lasterr);
% Maintenance Auto : end
