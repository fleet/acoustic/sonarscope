% Determine la visibilite des uicontrols "contenus"
%
% Syntax
%   this = determiner_visibilite_uicontrol(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame 
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: determiner_visibilite_uicontrol.m,v 1.4 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/01/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = determiner_visibilite_uicontrol(this)

% Maintenance Auto : try
    
    tabVisibilite = [];
    for i=1:length(this.tabUicontrols)
        
        visibilite = get(this.tabUicontrols(i), 'Visible');
        if strcmp(visibilite, 'on')
            tabVisibilite(i) = 1;
        else
            tabVisibilite(i) = 0;
        end
    end
    this.visibleUicontrols = tabVisibilite;
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'determiner_visibilite_uicontrol', lasterr);
% Maintenance Auto : end

