% Set sur l'instance et les instances incluses
% 
% Syntax
%   this = internal_set(this, name, ...)
%
% Input Arguments
%   this : instance de cl_frame
%   name     : nom de l'instance de cl_frame devant executer le set
%
% Name-Value Pair Arguments
%   Cf. cl_frame/set
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: internal_set.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = internal_set(this, name, varargin)

% Maintenance Auto : try
    
    if strcmp(name, this.alias) == 1
        this = set(this, varargin {:});
    else
        [m, n] = size(this.tabClFrames);
        for i = 1:m
            for j = 1:n
                this.tabClFrames(i, j) = named_set(this.tabClFrames(i, j), name, varargin {:});
            end
        end
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'internal_set', lasterr);
% Maintenance Auto : end
