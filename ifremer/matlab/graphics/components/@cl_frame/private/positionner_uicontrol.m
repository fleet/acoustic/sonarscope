% Positionne les uicontrols
%
% Syntax
%   this = positionner_uicontrol(this, liste)
%
% Input Arguments
%   this  : instance de cl_frame
%   liste : liste de structure decrivant les uicontrols
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = positionner_uicontrol(this, liste)

%% Si nombre max de ligne et colonne non initialises, determination a partir de la liste

maxLig = 1;
maxCol = 1;
for k=1:length(liste)
    if isfield(liste{k}, 'Lig') && isfield(liste{k}, 'Col')
        if maxLig < max(liste{k}.Lig)
            maxLig = max(liste{k}.Lig);
        end
        if maxCol < max(liste{k}.Col)
            maxCol = max(liste{k}.Col);
        end
    else
        msg = 'cl_frame::positionner_uicontrol, Lig or Col missing';
        warndlg(msg, 'Warning');
    end
    if get_max_lig(this) < maxLig
        this = set_max_lig(this, maxLig);
    end
    if get_max_col(this) < maxCol
        this = set_max_col(this, maxCol);
    end
end

%% Parcours de la liste, positionnement pour chaque element de la liste

for k=1:length(liste)
    if isfield(liste{k}, 'Lig') && isfield(liste{k}, 'Col')
        this.ligUicontrols{end+1} = liste{k}.Lig;
        this.colUicontrols{end+1} = liste{k}.Col;
        move(this, this.tabUicontrols(k), liste{k}.Lig, liste{k}.Col);
%         drawnow
    else
        my_warndlg('cl_frame/positionner_uicontrol : Lig or Col missing', 1);
    end
end
