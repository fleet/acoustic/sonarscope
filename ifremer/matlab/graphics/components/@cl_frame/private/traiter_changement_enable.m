% Traite un changement d enable de l'instance
%
% Syntax
%   this = traiter_changement_enable(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: traiter_changement_enable.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = traiter_changement_enable(this)

% --------------------------------------------------------------------------
% Changement d enable :
%    - la frame devient non access. : lecture de l access. des elements
%      "contenus" (uicontrols et axes), rend tous les elements "contenus"
%      inaccessibles
%    - la frame devient access. : application de l access. des elements
%      "contenus" (uicontrols et axes) en fonction de leur access. avant
%      que la frame ne soit inaccessible.

if strcmp(this.enable, 'off')
    this = determiner_enable(this);
    set(this.tabUicontrols, 'Enable', 'off');
    [m, n] = size(this.tabClFrames);
    tabClFrames = this.tabClFrames;
    for i = 1:1:m
        for j = 1:1:n
            tabClFrames(i, j) = set(tabClFrames(i, j), 'Enable', 'off');
        end
    end
    this.tabClFrames = tabClFrames;
else
    this = appliquer_enable(this);
end
