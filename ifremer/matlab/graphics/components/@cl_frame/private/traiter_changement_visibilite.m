% Traite un changement de visibilite de l'instance
%
% Syntax
%   this = traiter_changement_visibilite(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_changement_visibilite(this)

%% Changement de visibilite :
%    - la frame devient invisible : lecture de la visibilite des elements
%      "contenus" (uicontrols et axes), rend tous les elements "contenus"
%      invisibles
%    - la frame devient visible : application de la visibilite des elements
%      "contenus" (uicontrols et axes) en fonction de leur visibilite avant
%      que la frame ne soit invisible

if strcmp(this.visible, 'off') == 1
    set(this.handleFrame, 'Visible', 'off')
    
    this = determiner_visibilite(this);
    set(this.tabUicontrols, 'Visible', 'off');
    set(this.tabAxes, 'Visible', 'off');
    if ~isempty(get(this.tabAxes, 'Children'))
        set(get(this.tabAxes, 'Children'), 'Visible', 'off');
    end
    [m, n] = size(this.tabClFrames);
    tabClFrames = this.tabClFrames;
    for i=1:m
        for j=1:n
            tabClFrames(i, j) = set(tabClFrames(i, j), 'Visible', 'off');
        end
    end
    this.tabClFrames = tabClFrames;
else
    set(this.handleFrame, 'Visible', 'on')
    this = appliquer_visibilite(this);
end
