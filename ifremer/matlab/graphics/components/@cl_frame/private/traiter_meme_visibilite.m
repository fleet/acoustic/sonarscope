% Traite une demande de visibilite (impose de nouveau cette visibilite)
%
% Syntax
%   this = traiter_meme_visibilite(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: traiter_meme_visibilite.m,v 1.5 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = traiter_meme_visibilite(this)

% ----------------------------------
% Impose "brutalement" la visibilite

if strcmp(this.visible, 'off') == 1
    set(this.tabUicontrols, 'Visible', 'off');
    set(this.tabAxes, 'Visible', 'off');
    set(get(this.tabAxes, 'Children'), 'Visible', 'off');
    [m, n] = size(this.tabClFrames);
    tabClFrames = this.tabClFrames;
    for i = 1:1:m
        for j = 1:1:n
            tabClFrames(i, j) = set(tabClFrames(i, j), 'Visible', 'off');
        end
    end
    this.tabClFrames = tabClFrames;
else
    this = appliquer_visibilite(this);
end
