% Modification des attributs de l instance
% 
% Syntax
%   instance = set(this, ...)
%
% Input Arguments
%   this : instance de cl_frame
%
% Name-Value Pair Arguments
%   Alias           : nom d alias de la frame
%   NomFrame        : nom de tag de la frame matlab
%   PositionFrame   : position de la frame
%   HandleFrame     : handle de la frame
%   Visible         : visibilite de la frame
%   Verification    : flag de verification des noms de proprietes des 
%                      instances matlab supportees par la frame
%   MaxLig          : nombre maximal de lignes
%   MaxCol          : nombre maximal de colonnes
%   InsetX          : marge selon l axe x
%   InsetY          : marge selon l axe y
%   Enable          : accessibilite de la frame
%   ListeUicontrols : liste des instances des uicontrol
%   ListeAxes       : liste des instances des axes
%   ListeClFrames   : liste des instances des cl_frame
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% Remarks : par defaut (variable de retour non precisee), l'instance donnee en 
%    arguments est initialisee
%
% Examples
%    f = cl_frame; set(f)
%
% See also cl_frame Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set(this, varargin)

[varargin, alias]         = getPropertyValue(varargin, 'Alias',         []);
[varargin, nomFrame]      = getPropertyValue(varargin, 'NomFrame',      []);
[varargin, positionFrame] = getPropertyValue(varargin, 'PositionFrame', []);
[varargin, handleFrame]   = getPropertyValue(varargin, 'HandleFrame',   []);
[varargin, visible]       = getPropertyValue(varargin, 'Visible',       []);
[varargin, verification]  = getPropertyValue(varargin, 'Verification',  0);
[varargin, maxLig]        = getPropertyValue(varargin, 'MaxLig',        []);
[varargin, maxCol]        = getPropertyValue(varargin, 'MaxCol',        []);
[varargin, insetX]        = getPropertyValue(varargin, 'InsetX',        []);
[varargin, insetY]        = getPropertyValue(varargin, 'InsetY',        []);
[varargin, enable]        = getPropertyValue(varargin, 'Enable',        []);

%% Les mots cles des suivants sont conserves pour compatibilite ascendante
% et homogeneite des mots cles

[varargin, tabUicontrols] = getPropertyValue(varargin, 'ListeUicontrols', []);
[varargin, tabAxes]       = getPropertyValue(varargin, 'ListeAxes',       []);
[varargin, tabClFrames]   = getPropertyValue(varargin, 'ListeClFrames',   []);

%% Mot cle conserve pour compatibilité temporaire mais posant problème avec nomFrame

[varargin, nom] = getPropertyValue(varargin, 'nom', []);

%% Initialisation de la super classe

if ~isempty(maxLig)
    this.cl_abstract_layout = set_max_lig(this.cl_abstract_layout, maxLig);
end
if ~isempty(maxCol)
    this.cl_abstract_layout = set_max_col(this.cl_abstract_layout, maxCol);
end
if ~isempty(insetX)
    this.cl_abstract_layout = set_inset_x(this.cl_abstract_layout, insetX);
end
if ~isempty(insetY)
    this.cl_abstract_layout = set_inset_y(this.cl_abstract_layout, insetY);
end

%% Initialisation des attributs

if ~isempty(alias)
    this = set_alias(this, alias);
end
if ~isempty(nom)
    this = set_alias(this, nom);
end
if ~isempty(verification)
    this = set_verification(this, verification);
end
if ~isempty(nomFrame)
    this = set_nom_frame(this, nomFrame);
end
if ~isempty(handleFrame)
    this = set_handle_frame(this, handleFrame);
end
if ~isempty(positionFrame)
    this = set_position_frame(this, positionFrame);
end
if ~isempty(visible)
    this = set_visible(this, visible);
end
if ~isempty(tabUicontrols)
    this = set_tab_uicontrols(this, tabUicontrols);
end
if ~isempty(tabAxes)
    this = set_tab_axes(this, tabAxes);
end
if ~isempty(tabClFrames)
    this = set_tab_frames(this, tabClFrames);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end

%% Test analyse des arguments

if ~isempty(varargin)
    my_warndlg('cl_frame/set: Argument error', 1);
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
