% Accesseur en ecriture du nom d alias de la frame
% 
% Syntax
%   instance = set_alias(instance, alias)
%
% Input Arguments
%   instance : instance de cl_frame
%   alias    : alias
%
% Output Arguments
%   instance : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: set_alias.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_alias(instance, alias)

% Maintenance Auto : try
    instance.alias = alias;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'set_alias', lasterr);
% Maintenance Auto : end
