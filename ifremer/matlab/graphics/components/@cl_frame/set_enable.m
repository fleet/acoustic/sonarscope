% Accesseur en ecriture de l'accessibilite de l instance
% 
% Syntax
%   this = set_enable(instance, enable)
%
% Input Arguments
%   this : instance de cl_frame
%   enable   : flag de enable
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% ---------------------------------
% Initialisation de l'accessibilite

if isempty(this.enable) || (strcmp(enable, this.enable) == 0)
    this.enable = enable;
    this = traiter_changement_enable(this);
end
