% Accesseur en ecriture du handle matlab de la frame
%
% Syntax
%   this = set_handle_frame (this, handle)
%
% Input Arguments
%   this   : instance de cl_frame
%   handle : tag de l instance frame matlab
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_handle_frame(this, handle)

%% handle valide ?

if ishandle(handle)

    % -------------------------------
    % handle valide :
    % - initialisation du handle
    % - initialisation de la position

    this.handleFrame = handle;
    %
    units = get(handle, 'Units');
    set(handle, 'Units', 'pixels');
    position = get(handle, 'Position');
    set(handle, 'Units', units);
    this = set_position_frame(this, position);

elseif isempty(handle)

    % ---------------------------------------------------------
    % handle invalide et vide
    % - determination du handle a partir du nom de la frame
    % - determination du handle a partir de la fenetre courante

    if isempty(this.nomFrame)

        handle = gcf;
        if ~ishandle(handle)
            my_warndlg ('cl_frame/set_handle_frame : invalid figure handle', 1);
        else
            units = get(handle, 'Units');
            set(handle, 'Units', 'pixels');
            position = get(handle, 'Position');
            set(handle, 'Units', units);
            position(1:2) = 0;
            this = set_handle_frame(this, handle);
            this = set_position_frame (this, position);
        end

    else

        handle = findobj(gcf, 'Tag', this.nomFrame);
        if isempty(handle)
            my_warndlg('cl_frame/set_handle_frame : invalid object tag', 1);
        elseif ~ishandle(handle)
            my_warndlg('cl_frame/set_handle_frame : invalid object handle', 1);
        else
            units = get(handle, 'Units');
            set(handle, 'Units', 'pixels');
            position = get (handle, 'Position');
            set(handle, 'Units', units);
            this = set_handle_frame(this, handle);
            this = set_position_frame(this, position);
        end
    end
else
    my_warndlg('cl_frame/set_handle_frame : invalid handle', 1);
end

%% Gestion de la visibilité : force la nouvelle visibilite

this.visible = 'none';
