% Accesseur en ecriture du nom de la frame, le tag matlab correspondant
% 
% Syntax
%   this = set_nom_frame(this, nomFrame)
%
% Input Arguments
%   this : instance de cl_frame
%   nomFrame : tag de l instance frame matlab
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: set_nom_frame.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_nom_frame(this, nomFrame)

% --------
% Locales

isOk = 1;

% --------------------------
% Test de validite arguments

if ~ischar(nomFrame)
    isOk = 0;
    my_warndlg('cl_frame/set_nom_frame : Invalide argument type', 1);
end

% ----------------------------
% Initialisation de l instance

if isOk
    this.nomFrame = nomFrame;
end
