% Initialisation de la position de la frame
%
% Syntax
%   this = set_position_frame(this, positionFrame)
%
% Input Arguments
%   this          : instance de cl_frame
%   positionFrame : position de la frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_position_frame(this, positionFrame)

%% Initialisation de l attribut

this.positionFrame = positionFrame;

%% Initialisation de la super classe

this.cl_abstract_layout = set_position(this.cl_abstract_layout, positionFrame);
