% Accesseur en ecriture de la liste des axes
% 
% Syntax
%   this = set_tab_axes(this, tabAxes)
%
% Input Arguments
%   this    : instance de cl_frame
%   tabAxes : liste de structure decrivant les axes ou
%               tableau de handles
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: set_tab_axes.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_tab_axes(this, tabAxes)

% ------------------------------------------------
% Pas de position => essai avec la figure courante

if isempty(this.positionFrame)
    this = set_handle_frame(this, []);
end

% -------------------------------------------------
% Initialisation de l attribut :
%    . si liste de structures, creation des axes
%    . si tableau de handle, initialisation directe

if iscell(tabAxes)
    this = create_axe(this, tabAxes);
    this = positionner_axe(this, tabAxes);
elseif ishandle(tabAxes)
    this.tabAxes = tabAxes;
    this = determiner_visibilite_axe(this);
else
    my_warndlg('cl_frame/set_tab_axes : invalid list', 0);
end
this = appliquer_visibilite_axe(this);
