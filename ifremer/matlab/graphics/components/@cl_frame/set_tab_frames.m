% Accesseur en ecriture de la liste des frames
% 
% Syntax
%   this = set_tab_frames(this, tabFrames)
%
% Input Arguments
%   this      : instance de cl_frame
%   tabFrames : liste de structure decrivant les frames (instances)
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: set_tab_frames.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_tab_frames(this, tabFrames)

% ------------------------------------------------
% Pas de position => essai avec la figure courante

if isempty(this.positionFrame)
    this = set_handle_frame(this, []);
end

% ------------------------------
% Initialisation de l attribut :

if iscell(tabFrames)
    my_warndlg('cl_frame/set_tab_frames : Array format require', 0);
else
    this.tabClFrames = tabFrames;
    this = determiner_visibilite_cl_frame(this);
    this = determiner_enable_cl_frame(this);
end
this = appliquer_visibilite_cl_frame(this);
this = appliquer_enable_cl_frame(this);
