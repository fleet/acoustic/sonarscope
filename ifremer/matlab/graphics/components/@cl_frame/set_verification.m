% Accesseur en ecriture du flag de verification des noms de propriete
% 
% Syntax
%   this = set_verification(this, verification)
%
% Input Arguments
%   this         : instance de cl_frame
%   verification : flag de verification
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: set_verification.m,v 1.3 2003/03/31 16:52:10 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_verification(this, verification)

% Maintenance Auto : try
    this.verification = verification;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'set_verification', lasterr);
% Maintenance Auto : end
