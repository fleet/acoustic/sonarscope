% Accesseur en ecriture de la visibilite de l'instance
% 
% Syntax
%   this = set_visible(this, visible)
%
% Input Arguments
%   this     : instance de cl_frame
%   visible : flag de visible
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_visible(this, visible)

%% Lecture format numerique

if isnumeric(visible)
    if visible
        visible = 'on';
    else
        visible = 'off';
    end
end

%% Initialisation de l attribut si non initialise ou si different de la
% nouvelle valeur

if isempty(this.visible) || (strcmp(visible, this.visible) == 0)
    this.visible = visible;
    this = traiter_changement_visibilite(this);
else
    this.visible = visible;
    this = traiter_meme_visibilite(this);
end
