% Translation d'une instance de dx et dy
% (La translation du frame contenant n est pas faite)
% 
% Syntax
%   translate(this, dx, dy)
%
% Input Arguments
%    this : instance de cl_frame
%    dx   : composante selon x de la translation en pixels
%    dy   : composante selon y de la translation en pixels
%
% Examples
%    h = figure; 
%    clear uic axe
%    uic{1}.Lig   = 1:2;
%    uic{1}.Col   = 1;
%    uic{1}.Style = 'frame';
%    uic{1}.Tag   = 'container';
%    uic{2}.Lig   = 1;
%    uic{2}.Col   = 2;
%    uic{2}.Style = 'pushbutton';
%    axe{1}.Lig   = 2;
%    axe{1}.Col   = 2;
%
%    f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%    set(f, 'ListeUicontrols', uic);
%    set(f, 'ListeAxes', axe);
%
%    ff = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 5, 'InsetY', 5);
%    set(ff, 'HandleFrame', findobj(h, 'Tag', 'container'));
%    set(ff, 'ListeUicontrols', uic);
%
%    set(f, 'ListeClFrames', ff);
%
%    % translation vers la droite, haut
%    translate(f, 10, 10);
%    
%    % retour en position origine
%    translate(f, -10, -10);
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    17/01/2001 - DCF - creation
%    21/03/2001 - DCF - maj gestion erreurs
%    23/04/2001 - DCF - mise en reference
%    20/05/2003 - JMA - modif position
% ----------------------------------------------------------------------------

function translate(this, dx, dy)

% Maintenance Auto : try
    
    % -------------------------------------------------
    % Lecture de l'unite de la frame, passage en pixels
    
    units = get(this.handleFrame, 'Units');
    
    % ---------------------------
    % Translation des uicontrols
    
    uic = this.tabUicontrols;
    [m, n] = size(uic);
    
    for i = 1:1:m
        for j = 1:1:n
            set(uic(i, j), 'Units', 'pixels');
            p = get(uic(i, j), 'Position');
%             p = [p(1) + dx, p(2) + dy];
            p = [p(1) + dx, p(2) + dy p(3) p(4)];   % Modif JMA 20/05/03
            set(uic(i, j), 'Position', p, 'Units', units);
        end
    end
    
    % --------------------
    % Translation des axes
    
    axe = this.tabAxes;
    [m, n] = size(axe);
    
    for i = 1:1:m
        for j = 1:1:n
            set(axe(i, j), 'Units', 'pixels');
            p = get(axe(i, j), 'Position');
%             p = [p(1) + dx, p(2) + dy];
            p = [p(1) + dx, p(2) + dy p(3) p(4)];   % Modif JMA 20/05/03
            set(axe(i, j), 'Position', p, 'Units', units);
        end
    end
    
    % ----------------------------------
    % Translation des cl_frame contenues
    
    clf = this.tabClFrames;
    [m, n] = size(clf);
    
    for i = 1:1:m
        for j = 1:1:n
            translate(clf(i, j), dx, dy);
        end
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_frame', 'translate', lasterr);
% Maintenance Auto : end
