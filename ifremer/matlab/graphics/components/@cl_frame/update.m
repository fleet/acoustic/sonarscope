% Mise a jour des positions des elements contenus
%
% Syntax
%   this = update(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Examples
%   h = figure;
%   clear uic
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%   uic{2}.Lig   = 1;
%   uic{2}.Col   = 2;
%   uic{2}.Style = 'pushbutton';
%
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'Nom', 'f');
%   set(f, 'ListeUicontrols', uic);
%   f = update(f);
%
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = update(this)

%% Repositionne chaque �l�ment de la frame : uicontrols

n = size(this.tabUicontrols, 2);
for k = 1:1:n
    move(this, this.tabUicontrols(k), this.ligUicontrols{k}, this.colUicontrols{k});
end

%% Repositionne chaque �l�ment de la frame : axes

n = size(this.tabAxes, 2);
for k = 1:1:n
    move(this, this.tabAxes(k), this.ligAxes {k}, this.colAxes{k});
end

%% Repositionne chaque �l�ment de la frame : cl_frame

n = size(this.tabClFrames, 2);
for k = 1:1:n
    this.tabClFrames(k) = update(this.tabClFrames(k));
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
