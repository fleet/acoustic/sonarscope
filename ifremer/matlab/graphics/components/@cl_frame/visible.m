% Initialisation de la visibilite d un element contenu de la frame
% 
% Syntax
%   this = visible(this, type, indice, visibilite)
%
% Input Arguments
%   this       : instance de cl_frame
%   type       : type du contenu {'uicontrol', 'axe', 'cl_frame'}
%   indice     : indice de l element
%   visibilite : visibilite de l element {'on', 'off'}
%
% Output Arguments
%   this : instance de cl_frame initialisee
%
% Examples
%   h = figure; 
%   clear uic
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%   uic{2}.Lig   = 1;
%   uic{2}.Col   = 2;
%   uic{2}.Style = 'pushbutton';
%
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'Nom', 'f');
%   set(f, 'ListeUicontrols', uic);
%
%   f = visible(f, 'uicontrol', 1, 'off')
%    
% See also cl_frame Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = visible(this, type, indice, visibilite)

%% S�lection du type d element

switch type
    case 'uicontrol'
        if (indice > 0) && (indice <= size(this.tabUicontrols, 2))
            if strcmp(visibilite, 'on')
                this.visibleUicontrols(indice) = 1;
            else
                this.visibleUicontrols(indice) = 0;
            end
            this = appliquer_visibilite_uicontrol(this);
        end

    case 'axe'
        if (indice > 0) && (indice <= size(this.tabAxes, 2))
            if strcmp(visibilite, 'on')
                this.visibleAxes(indice) = 1;
            else
                this.visibleAxes(indice) = 0;
            end
            this = appliquer_visibilite_axe(this);
        end

    case 'cl_frame'
        if (indice > 0) && (indice <= size(this.tabClFrames, 2))
            if strcmp(visibilite, 'on')
                this.visibleClFrames(indice) = 1;
            else
                this.visibleClFrames(indice) = 0;
            end
            this = appliquer_visibilite_cl_frame(this);
        end

    otherwise
        my_warndlg('cl_frame/visible : Invalid type', 1);
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
