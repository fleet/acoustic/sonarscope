% Constructeur de clc_axe, classe composant
%
% Syntax
%   this = clc_axe(...)
% 
% Name-Value Pair Arguments
%   tag               : tag de l axe
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments 
%   this         : this de clc_axe
%
% Remarks : ne peut etre employe que sous forme d un composant associe a une
%    this graphique frame matlab
%
% Examples
%   quit = clc_axe
%
% See also clc_axe Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_axe(varargin)

%% D�finition de la structure et initialisation
%
% globalFrame : this de cl_frame, frame globale du composant
% tag         : tag de l'axe

this.globalFrame    = [];
this.tag            = '';
this.ancienneFigure = [];
this.ancienAxe      = [];
this.handleAnchor   = [];

%% Super-classe

if (nargin == 1) && isempty(varargin{1})
    composant = cl_component([]);
else
    composant = cl_component;
end

%% Cr�ation de l'objet

this = class(this, 'clc_axe', composant);

%% Pr�-initialisation des champs

if (nargin == 1) & isempty(varargin{1})
    return;
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
