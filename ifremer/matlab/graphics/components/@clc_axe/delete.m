% Destruction du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_axe
%
% Remarks : doit etre surchargee
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % ----------------------------------------
    % Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    % -----------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(this.globalFrame)
        this.globalFrame = delete (this.globalFrame);
        this.globalFrame = [];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'delete', lasterr);
% Maintenance Auto : end
