% Affichage des donnees d'une instance de clc_axe
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : instance de clc_axe
%
% Examples
%   display(clc_axe)
%
% See also clc_axe Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
