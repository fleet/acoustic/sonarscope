% Methode de gestion des callback du composant
%
% Syntax
%   this = gerer_callback(this, cbName)
% 
% Input Arguments
%   this     : instance de cl_component
%   cbName   : nom de la call back
%   varargin : parametres optionnels de la call back
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also cl_component clc_axe Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

if length(varargin) ~= 1
    my_warndlg('clc_axe/gerer_callback : Invalid arguments number', 0);
end
