% Lecture des attributs de l'instance
%
% Syntax
%   varargout = get(this, varargin)
% 
% Input Arguments
%   this : instance de clc_axe
%   varargin : noms des proprietes
%    tag               : tag de l axe
%    handle            : handle de l axe (matlab)
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant
%    componentEnable   : accessibilite du composant
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%    componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    c = clc_axe('tag', 'Tag de l axe') ;
%    get(c, 'tag')
%
% See also clc_axe Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'tag'
            varargout{end+1} = get_tag(this); %#ok<AGROW>
        case 'handle'
            varargout{end+1} = get_handle(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.cl_component, varargin{i}); %#ok<AGROW>
    end
end
