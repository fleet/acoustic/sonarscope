% Retourne le handle de l'axe s il a une representation graphique, [] sinon
% 
% Syntax
%   handle = get_handle(this)
%
% Input Arguments
%   this : instance de clc_axe
%
% Output Arguments
%   handle : handle de l axe
%
% See also clc_axe Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function handle = get_handle(this)

%% Test validite des arguments, initialisation

if is_edit(this)
    handle = get_tab_axes(this.globalFrame);
else
    my_warndlg('clc_axe/get_handle :No graphic representation', 0);
end
