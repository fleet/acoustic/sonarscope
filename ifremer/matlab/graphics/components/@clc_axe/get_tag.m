% Retourne le tag de l'axe
% 
% Syntax
%   tag = get_tag(this)
%
% Input Arguments
%   this : instance de clc_axe
%
% Output Arguments
%   tag      : tag de l axe
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: get_tag.m,v 1.3 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tag = get_tag(this)

% Maintenance Auto : try
    
    % -------------------------------------------
    % Test validite des arguments, initialisation
    
    tag = this.tag;
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'get_tag', lasterr);
% Maintenance Auto : end
