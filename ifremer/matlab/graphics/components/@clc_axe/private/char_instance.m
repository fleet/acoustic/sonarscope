% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_axe
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.4 2003/07/09 16:42:22 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/02/2001 - DCF - creation
%   23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
    
    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('tag            <-> %s', mat2str(this.tag));
    str{end+1} = sprintf('ancienneFigure <-> %s', mat2str(this.ancienneFigure));
    str{end+1} = sprintf('ancienAxe      <-> %s', mat2str(this.ancienAxe));
    str{end+1} = sprintf('--- agregation globalFrame ---');
    str{end+1} = char(this.globalFrame);
   
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'char_instance', lasterr);
% Maintenance Auto : end
