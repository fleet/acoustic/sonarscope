% Construction de base de l'IHM
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_axe
%
% Output Arguments
%   this : instance de clc_axe
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: construire_ihm.m,v 1.4 2003/07/09 16:42:22 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

% Maintenance Auto : try
    
    % ---------------------------------
    % Creation de la frame du composant
    
    this = creer_frame_globale(this);
    
    % ------------------------------
    % Mise a jour de l accessibilite
    
    if strcmp(get_enable (this), 'off')
        this.globalFrame = set_enable(this.globalFrame, 'off');
    end
    
    % ----------------------------
    % Mise a jour de la visibilite
    
    if strcmp(get_visible (this), 'off') 
        this.globalFrame = set_visible(this.globalFrame, 'off');
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'construire_ihm', lasterr);
% Maintenance Auto : end
