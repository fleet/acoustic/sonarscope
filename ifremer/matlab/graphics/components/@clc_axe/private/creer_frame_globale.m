% Creation de la frame globale du composant
%
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_axe
%
% Output Arguments
%   this : instance de clc_axe initialisee
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: creer_frame_globale.m,v 1.3 2003/07/09 16:42:22 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    26/02/2001 - DCF - conserve toujours l ancre d un axe invisible
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

% Maintenance Auto : try
    ihm = {};
    
    % ---
    % Axe
    
    ihm{end+1}.Lig = 1;
    ihm{end}.Col = 1;
    ihm{end}.Tag = get(this, 'tag');
    
    % -------------------
    % Creation des frames
    
    this.globalFrame = cl_frame([]) ;
    this.globalFrame = set_handle_frame(this.globalFrame, get_anchor(this));
    this.globalFrame = set_inset_x(     this.globalFrame, get_inset_x(this));
    this.globalFrame = set_inset_y(     this.globalFrame, get_inset_y(this));
    this.globalFrame = set_max_lig(     this.globalFrame, 1);
    this.globalFrame = set_max_col(     this.globalFrame, 1);
    this.globalFrame = set_tab_axes(    this.globalFrame, ihm);
    
    % ----------------------
    % Ancre rendue invisible
    
    set(get_anchor(this), 'Visible', 'off');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'creer_frame_globale', lasterr);
% Maintenance Auto : end
