% Modification des attributs de l'instance
%
% Syntax
%   this = set(this, varargin)
%
% Input Arguments
%   this : this de clc_axe
%
% Name-Value Pair Arguments
%   tag               : tag de l axe
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments
%   this : this de clc_axe initialisee
%
% Examples
%    c = clc_axe;
%    set(c, 'tag', 'tag de l axe');
%
% See also clc_axe Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% --------------------------------------------------------------------------
% Lecture des arguments

[varargin, tag] = getPropertyValue(varargin, 'tag', []);
[varargin, visible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, enable] = getPropertyValue(varargin, 'componentEnable', []);

% --------------------------------------------------------------------------
% Initialisation des attributs

if ~isempty(tag)
    this = set_tag(this, tag);
end
if ~isempty(visible)
    this = set_visible(this, visible);
    if is_edit(this)
        set(get_anchor(this), 'Visible', 'off');
    end
end
if ~isempty(enable)
    this = set_enable(this, enable);
end

% -----------------------------------------
% Traitement des message de la super classe

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
