% Rend courant l'axe de l'instance
%
% Syntax
%   this = set_axe2courant(this)
% 
% Input Arguments
%   this : instance de clc_axe
%
% OUPUT PARAMETERS :
%   this : instance de clc_axe initialisee
%
% See also clc_axe clc_axe/RestaureCourant Authors
% Authors : LD
% VERSION  : $Id: set_axe2courant.m,v 1.4 2003/03/31 14:06:37 augustin Exp $
% ----------------------------------------------------------------------------


function this = set_axe2courant(this)

% ----------------------
% Initialisation interne

this.ancienneFigure = [];
this.ancienAxe      = [];

% ----------------------------------------------
% Recherche l'axe courant et le stocke le handle

currentFigure = get(0, 'CurrentFigure');
if ~isempty(currentFigure) && ishandle(currentFigure)
    currentAxes  = get(currentFigure, 'CurrentAxes');
    if ~isempty(currentAxes) && ishandle(currentAxes)
        this.ancienneFigure = currentFigure;
        this.ancienAxe      = currentAxes;
    end
end

% ---------------------------------------------------------------------
% Passage � l'axe courant ssi l'instance a une representation graphique

if is_edit(this)
    if ~isempty(this.ancienneFigure) && ~isempty(this.ancienAxe)
        axes(get_handle(this));
    else
        my_warndlg('clc_axe/set_back_current_axe : No figure or axes, invalid case', 0);
    end
end

