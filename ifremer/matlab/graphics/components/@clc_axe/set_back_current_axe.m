% Restaure l'ancien axe
%
% Syntax
%   this = set_back_current_axe(this)
% 
% Input Arguments
%   this : instance de clc_axe
%
% OUPUT PARAMETERS :
%   this : instance de clc_axe initialisee
%
% See also clc_axe clc_axe/AxeCourant Authors
% Authors : LD
% VERSION  : $Id: set_back_current_axe.m,v 1.4 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_back_current_axe(this)

isBack = 0;

% --------------
% Restaure l'axe

if ~isempty(this.ancienneFigure) && ~isempty(this.ancienAxe)
    if ishandle(this.ancienneFigure) && ishandle(this.ancienAxe)
        figure(this.ancienneFigure);
        axes(this.ancienAxe);
        isBack = 1;
    end
end

% --------------------
% Oublie figure et axe

this.ancienneFigure = [];
this.ancienAxe      = [];

% --------------------------------------
% Avertit que le retour ne peut se faire

if ~isBack
    my_warndlg('clc_axe/set_back_current_axe : Can''t set current axes back to old one', 0);
end
