% Initialisation de l'accessibilite du composant
% 
% Syntax
%   this = set_enable(this, enable)
%
% Input Arguments
%   this   : instance de clc_axe
%   enable : accessibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_axe initialisee
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.3 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    if is_edit(this)
        this.globalFrame  = set_enable(this.globalFrame, enable);
    end
    this.cl_component = set_enable(this.cl_component, enable);
else
    my_warndlg('clc_axe/set_enable : Invalid value', 0);
end
