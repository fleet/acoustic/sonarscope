% Initialisation du tag de l axe
% 
% Syntax
%   this = set_tag(this, tag)
%
% Input Arguments
%   this : instance de clc_axe
%   tag  : tag pour l axe
%
% Output Arguments
%   this : instance de clc_axe initialisee
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: set_tag.m,v 1.3 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_tag(this, tag)

% Maintenance Auto : try
    
    % -------------------------------------------
    % Test validite des arguments, initialisation
    
    this.tag = tag;
    if is_edit(this)
        set(get_anchor(this), 'Tag', tag);
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'set_tag', lasterr);
% Maintenance Auto : end
