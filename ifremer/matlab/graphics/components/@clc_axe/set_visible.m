% Initialisation de la visibilite du composant
% 
% Syntax
%   this = set_visible(this, visible)
%
% Input Arguments
%   this    : instance de clc_axe
%   visible : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_axe initialisee
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: set_visible.m,v 1.3 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_visible(this, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    if is_edit(this)
        this.globalFrame = set_visible(this.globalFrame, visible);
        set(get_anchor(this), 'Visible', 'off') ;
    end
    this.cl_component = set_visible(this.cl_component, visible);
else
    my_warndlg('clc_axe/set_visible : Invalid value', 0);
end
