% Test graphique du composant
%
% Syntax
%   this = test(this)
% 
% Input Arguments
%   this   : instance de clc_axe
%
% Output Arguments
%   this : instance de clc_axe
% 
% Examples
%    this = test(clc_axe)
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this)

% Maintenance Auto : try
    
    % -------------------------------------------------------
    % Creation d une figure ainsi que de l'ancre du composant
    
    figure;
    h = uicontrol ( 'Visible'  , 'on', 'Tag', 'ancre_clc_axe', 'Position' , [20 20 300 300]);
    
    % ---------------------------------------
    % Creation et initialisation du composant
    
    this = clc_axe([]);
    this = set_tag(this, 'Axe');
    this = set_name(this,'ancre_clc_axe' );
    this = set_user_name(this,  'clc_axe');
    this = set_user_cb(this, 'gerer_callback');
    
    % -------------------------------
    % Creation graphique du composant
    
    this = editobj(this);
    
    % ------------------------
    % Rend le resultat visible
    
    set(h, 'Visible', 'off');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'test', lasterr);
% Maintenance Auto : end
