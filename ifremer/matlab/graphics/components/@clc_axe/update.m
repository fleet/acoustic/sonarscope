% Mise a jour du composant
%
% Syntax
%   this = update(this)
% 
% Input Arguments
%   this : instance de clc_axe
%
% Remarks : doit etre surchargee
%
% See also clc_axe Authors
% Authors : DCF
% VERSION  : $Id: update.m,v 1.3 2003/07/09 16:42:41 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = update(this)

% Maintenance Auto : try
    
    % -----------------------------------
    % Update graphique de la super classe
    
    this.cl_component = update(this.cl_component);
    
    % -----------------------------------------------------
    % Update graphique des elements graphiques du composant
    
    if ~isempty(this.globalFrame)
        this.globalFrame = update(this.globalFrame);
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'update', lasterr);
% Maintenance Auto : end
