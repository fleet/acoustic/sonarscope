% Ajout d un composant
%
% Syntax
%   instance = add (instance, indiceElement, element)
% 
% Input Arguments
%   instance      : instance de clc_axev
%   indiceElement : indice de l element concerne par l operation
%   element       : composant ajoute (de type de base cl_component)
%
% OUPUT PARAMETERS :
%   instance      : instance de clc_axev initialisee
%
% See also clc_axev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = add(instance, indiceElement, element)

% -----------------------------
% Test de la valeur de l indice

if (indiceElement > 0) && (indiceElement <= get (instance, 'nbElements'))
    
    % --------------------------
    % Test le type de l instance
    
    if isa(element, instance.acceptedType)
        
        % --------------------
        % Methode super classe
        
        instance.clc_cv = add (instance.clc_cv, indiceElement, element) ;
    else
        my_warndlg('clc_axev/add : Invalid component type', 0);
    end
else
    my_warndlg('clc_axev/rem : Indice out of range', 0);
end
