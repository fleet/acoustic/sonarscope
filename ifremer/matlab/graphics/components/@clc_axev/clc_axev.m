% Constructeur de clc_axev, classe composant
%
% Syntax
%   instance = clc_axev (varargin)
% 
% Name-Value Pair Arguments
%    nbElements        : nombre de frames du vecteur de frames
%    tag               : base du tag des frames
%    orientation       : orientation du vecteur : 'lig' ou 'col'
%    cListe            : liste de composants
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant    {'on', 'off'}
%    componentEnable   : accessibilite du composant {'on', 'off'}
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments 
%    instance         : instance de clc_axev
%
% Remarks : ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    quit = clc_axev
%
% See also clc_axev Authors
% Authors : DCF
% VERSION  : $Id: clc_axev.m,v 1.2 2002/06/04 14:37:38 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    01/02/2000 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = clc_axev (varargin)

% Maintenance Auto : try
    
    % --------------------------------------------
    % Definition de la structure et initialisation
    %
    % Type de composants acceptes
    
    instance.acceptedType = 'clc_axe' ;
    
    % ------------
    % Super-classe
    
    componentVector = clc_cv ;
    
    % -------------------
    % Creation de l'objet
    
    instance = class (instance, 'clc_axev', componentVector) ;
    
    % ----------------------------
    % Pre-nitialisation des champs 
    
    if (nargin == 1) & isempty (varargin {1})
        return ;
    end
    instance = set (instance, varargin {:}) ;
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_axev', 'clc_axev', '') ;
% Maintenance Auto : end
