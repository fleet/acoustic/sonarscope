% Suppression virtuelle d un element du vecteur
%
% Syntax
%   instance = collapse (instance, indiceElement, value)
% 
% Input Arguments
%   instance      : instance de clc_axev
%   indiceElement : indice de l element concerne par l operation
%   value         : realite de l element {'on', 'off'}
%
% Remarks : doit �tre surcharg�e
%
% See also clc_axev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = collapse (instance, indiceElement, value)

%% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get (instance, 'nbElements'))
    
    %% Test de la visibilit�
    
    if strcmp(value, 'on') || strcmp(value, 'off')
        
        %% Appel methode super classe
        
        instance.clc_cv = collapse(instance.clc_cv, indiceElement, value) ;
        
        %% Rend la sous frame invisible
        
        instance.clc_cv = frame_visible(instance.clc_cv, indiceElement, 'off') ;
    else
        my_warndlg('clc_axev/collapse : Invalid visibility value', 0);
    end
else
    my_warndlg('clc_axev/collapse : Invalid element indice', 0);
end
