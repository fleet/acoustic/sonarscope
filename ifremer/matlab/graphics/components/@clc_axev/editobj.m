% Représentation de l'instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_axev
%
% OUPUT PARAMETERS :
%   this : instance de clc_axev
%
% See also clc_axev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Realité graphique si le nombre d'élément est strictement positif

if get(this, 'nbElements') > 0
    
    %% Traitement de la super classe
    
    this.clc_cv = editobj(this.clc_cv);
    
    %% Rend les frames invisibles
    
    for k=1:get(this, 'nbElements')
        this.clc_cv = frame_visible(this.clc_cv, k, 'off');
    end
    
end
