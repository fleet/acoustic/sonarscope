% Initialisation du nombre de composants (elements de la liste)
%
% Syntax
%   instance = set_nb_elements (instance, nbElements)
%
% Input Arguments
%   instance   : instance de clc_axev
%   nbElements : nombre d elements du vecteur de composants
%
% Output Arguments
%   instance : instance de clc_axev initialisee
%
% See also clc_axev Authors
% Authors : DCF
% VERSION  : $Id: set_nb_elements.m,v 1.2 2002/06/04 14:34:37 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_nb_elements (instance, nbElements)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(nbElements)
    
    % -----------------------------------------
    % Mise a jour du nombre par la super classe
    
    instance.clc_cv = set (instance.clc_cv, 'nbElements', nbElements);
    
    % ---------------------------------
    % Rend toutes les frames invisibles
    
    for i = 1:nbElements
        instance = frame_visible(instance, i, 'off') ;
    end
else
    my_warndlg('clc_axev/set_nbElements : Invalid argument format', 0);
end
