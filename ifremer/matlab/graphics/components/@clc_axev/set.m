% Modification des attributs de l instance
%
% Syntax
%   instance = set (instance, varargin)
%
% Input Arguments
%   instance : instance de clc_axev
%
% Name-Value Pair Arguments
%   cListe            : liste de composants (nbElements elements)
%   nbElements        : nombre de frames du vecteur de frames
%   tag               : base du tag des frames
%   orientation       : orientation du vecteur {'lig', 'col'}
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments
%    instance : instance de clc_axev initialisee
%
% Examples
%    c = clc_axev ;
%    set (c, 'nbElements', 3, 'tag', 'clc_axev_tag')
%
% See also clc_axev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = set (instance, varargin)

% ---------------------
% Lecture des arguments

[varargin, nbElements] = getPropertyValue(varargin, 'nbElements', []);

% ----------------------------
% Initialisation des attributs

if ~isempty (nbElements)
    instance = set_nb_elements (instance, nbElements) ;
end

% -----------------------------------------
% Traitement des message de la super classe

if ~isempty (varargin)
    instance.clc_cv = set (instance.clc_cv, varargin{:}) ;
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), instance) ;
end
