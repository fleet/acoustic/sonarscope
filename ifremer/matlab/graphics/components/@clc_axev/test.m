% Test graphique du composant
%
% Syntax
%   instance = test (instance, varargin)
% 
% Input Arguments
%   instance : instance de clc_axev
%   varargin : message de call back
%
% Output Arguments
%   instance : instance de clc_axev
% 
% Examples
%    instance = test (clc_axev) ;
%
% See also clc_axev Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.2 2002/06/04 14:37:38 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = test (instance, varargin)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Gestion des messages de call back (pour visualiser l arrivee des messages)
    
    % -------------------------------------------------------
    % Creation d une figure ainsi que de l ancre du composant
    
    figure ('Visible', 'off') ;
    h = uicontrol (                        ...
        'Visible'  , 'on'                , ...
        'Tag'      , 'ancre_clc_axev'      , ...
        'Position' , [20 20 500 400]     ) ;
    
    % ---------------------------------------
    % Creation et initialisation du composant
    
    instance = clc_axev ;
    instance = set ( instance                    , ...
        'nbElements'        , 4                  , ...
        'tag'               , 'clc_axev tag'     , ...
        'orientation'       , 'lig'              , ...
        'componentName'     , 'ancre_clc_axev'   , ...
        'componentUserName' , 'clc_axev'         , ...
        'componentUserCb'   , ''                 ) ;
    
    
    axe = clc_axe ;
    instance = add (instance, 1, axe) ;
    instance = add (instance, 2, axe) ;
    instance = add (instance, 3, axe) ;
    instance = add (instance, 4, axe) ;
    
    % -------------------------------
    % Creation graphique du composant
    
    instance = editobj (instance) ;
    
    % ------------------------
    % Rend le resultat visible
    
    set (gcf, 'Visible', 'on') ;
    set (h, 'Visible', 'off') ;
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_axev', 'test', '') ;
% Maintenance Auto : end
