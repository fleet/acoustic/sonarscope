% Initialisation de la visibilite d un element du vecteur
%
% Syntax
%   instance = visible (instance, indiceElement, visibilite)
% 
% Input Arguments
%   instance      : instance de clc_axev
%   indiceElement : indice de l element concerne par l operation
%   visibilite    : visibilite de l element {'on', 'off'}
%
% Remarks : doit etre surchargee
%
% See also clc_axev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = visible (instance, indiceElement, visibilite)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get (instance, 'nbElements'))
    
    % ---------------------
    % Test de la visibilite
    
    if strcmp (visibilite, 'on') || strcmp(visibilite, 'off')
        
        % -----------------------------
        % Traitement de la super classe
        
        instance.clc_cv = visible ( ...
            instance.clc_cv, indiceElement, visibilite) ;
        
        % --------------------------
        % Rend les frames invisibles
        
        instance.clc_cv = frame_visible(instance.clc_cv, indiceElement, 'off') ;
    else
        my_warndlg('clc_axev/visible : Invalid visibility value', 0);
    end
else
    my_warndlg('clc_axev/visible : Invalid element indice', 0);
end
