% Transformation en chaine de caracteres d'un tableau d'instances
%
% Syntax
%   str = char(a) 
%
% Input Arguments
%   a : instance ou tableau d instance de clc_colorbar
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%   a = clc_colorbar;
%   str = char(s)
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    str = [];
    [m, n] = size(this);
    
    if (m*n) > 1
        
        % -------------------
        % Tableau d'instances
        
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        % ---------------
        % Instance unique
        
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
        
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_colorbar', 'char', lasterr);
% Maintenance Auto : end
