% Constructeur de clc_colorbar
%
% Syntax
%   a = clc_colorbar
% 
% Output Arguments
%   this : instance de clc_colorbar
% 
% Examples
%    a = clc_colorbar
%
% See also clc_colorbar/set Authors
% Authors : DCF
%------------------------------------------------------------------------------

function this = clc_colorbar(varargin)

% --------------------------------------------
% Definition de la structure et initialisation
%
% . orientation : vertical ou horizontal
% . tick        : position des labels
% . tickLabel   : texte des labels
% . posLabel    : positionnement des labels
% . axis        : min max en x et en y

this.orientation  = 'col';
this.tick         = [];
this.tickLabel    = [];
this.posLabel     = [];
this.axis         = [];
this.handleAnchor = [];

%% Super-classe

if (length(varargin) == 1) && (isempty(varargin {1}))
    superClasse = clc_axe([]);
else
    superClasse = clc_axe;
end

%% Cr�ation de l'instance

this = class(this, 'clc_colorbar', superClasse);
