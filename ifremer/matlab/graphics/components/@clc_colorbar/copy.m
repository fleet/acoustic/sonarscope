% Copy de la colorbar matlab en une instance cd clc_colorbar
%
% Syntax
%   a = copy(a, hColorBar)
%
% Input Arguments
%   a         : instance de clc_colorbar
%   hColorBar : handle valide de colorbar matlab
%
% Output Arguments
%   a : instance de clc_colorbar initialisee
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = copy(this, hColorBar, varargin)

[varargin, ImageIndexee] = getPropertyValue(varargin, 'ImageIndexee', 0); %#ok<ASGLU>

isOk = 1;

%% Test des arguments

if ~ishandle(hColorBar)
    isOk = 0;
    my_warndlg('clc_colorbar/copy : Invalid colorbar handle', 0);
end

%% Initialisation de l'instance en fonction de la colorbar

if isOk
    p = get(hColorBar, 'Position');
    if p(3) > p(4)
        orientation = 'hor';
    else
        orientation = 'ver';
    end
    
    if p(3) > p(4)
        tick = get(hColorBar, 'XTick');
        tl   = get(hColorBar, 'XTickLabel');
    else
        tick = get(hColorBar, 'YTick');
        tl   = get(hColorBar, 'YTickLabel');
    end
    
    sub = [];
    for k=1:size(tl, 1)
        if ImageIndexee
            num = str2double(tl(k,:));
            if num == floor(num)
                tickLabel{k} = num2str(num); %#ok<AGROW>
            else
                tickLabel{k} = []; %#ok<AGROW>
                sub(end+1) = k;  %#ok<AGROW>
            end
        else
            tickLabel{k} = tl(k,:); %#ok<AGROW>
        end
    end
    
    tickLabel(sub) = [];
    tick(sub) = [];
    
    xLim = get(hColorBar, 'XLim');
    yLim = get(hColorBar, 'YLim');
    
    this = set_orientation(this, orientation);
    this = set_tick(this, tick);
    this = set_tick_label(this, tickLabel);
    this = set_axis(this, [xLim, yLim]);
    
    this = update_colormap(this);
end
