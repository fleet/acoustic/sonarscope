% Destruction du composant
%
% Syntax
%   a = delete(a)
% 
% Input Arguments
%   a : instance de clc_axe
%
% Remarks : doit etre surchargee
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    % --------------------------------------------------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(this.globalFrame)
        this.globalFrame = delete(this.globalFrame);
        this.globalFrame = [];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_axe', 'delete', lasterr);
% Maintenance Auto : end
