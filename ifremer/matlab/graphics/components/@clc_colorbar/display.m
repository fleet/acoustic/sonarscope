% Affichage des donnees d'une instance de clc_colorbar
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : instance de clc_colorbar
%
% Examples
%   a = clc_colorbar;
%   display(a)
%   a
%
% See also clc_colorbar Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
