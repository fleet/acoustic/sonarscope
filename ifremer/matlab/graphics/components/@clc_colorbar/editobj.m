% Representation de l'instance
%
% Syntax
%   a = editobj(a) 
%
% Input Arguments
%   a : instance de clc_colorbar
%
% OUPUT PARAMETERS :
%   a : instance de clc_colorbar
%
% See also clc_colorbar Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Cr�ation de l'interface graphique

if ~is_edit(this)
    
    %% Appel methode super-classe
    
    this.clc_axe = editobj(this.clc_axe);
    
    %% Remplissage de la colorbar
    
    this = update_colormap(this); 
end
