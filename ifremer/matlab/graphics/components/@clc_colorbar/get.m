% Lecture des attributs de l'instance
%
% Syntax
%   [...] = get(this, ...)
%
% Input Arguments 
%   a : instance de clc_colorbar
%
% Name-Value Pair Arguments
%   orientation : orientation de l axe {'hor', 'ver'}
%   tick        : valeur de position des labels
%   tickLabel   : valeur texte des labels
%   posLabel    : positionnement des labels :
%                       hor, {'down', 'up'}
%                       ver, {'left', 'right'}
%   axis        : min et max en x et en y
%
% Output Arguments
%   valeurs des attributs correspondant aux noms de
%            proprietes specifiees en arguments, dans le meme ordre
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i = 1:length(varargin)
    switch varargin{i}
        case 'orientation'
            varargout{end+1} = get_orientation(this); %#ok<AGROW>
        case 'tick'
            varargout{end+1} = get_tick(this); %#ok<AGROW>
        case 'tickLabel'
            varargout{end+1} = get_tick_label(this); %#ok<AGROW>
        case 'axis'
            varargout{end+1} = get_axis(this); %#ok<AGROW>
        case 'posLabel'
            varargout{end+1} = get_pos_label(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.clc_axe, varargin{i}); %#ok<AGROW>
    end
end
