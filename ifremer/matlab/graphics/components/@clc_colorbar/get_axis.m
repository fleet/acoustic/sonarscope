% Retourne les min et max selon x et y
%
% Syntax
%   axis = get_axis(a)
%
% Input Arguments 
%   a : instance de clc_colorbar
%
% Output Arguments
%   axis : min et max en x et y
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: get_axis.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function axis = get_axis(this)

% Maintenance Auto : try
    axis = this.axis;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_colorbar', 'get_axis', lasterr);
% Maintenance Auto : end
