% Retourne l orientation de la colorbar
%
% Syntax
%   orientation = get_orientation(a)
%
% Input Arguments 
%   a : instance de clc_colorbar
%
% Output Arguments
%   orientation :  orientation de l axe {'hor', 'ver'}
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: get_orientation.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function orientation = get_orientation(this)

% Maintenance Auto : try
    orientation = this.orientation;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_colorbar', 'get_orientation', lasterr);
% Maintenance Auto : end
