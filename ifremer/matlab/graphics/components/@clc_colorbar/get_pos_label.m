% Retourne la position des labels sur la colorbar
%
% Syntax
%   posLabel = get_pos_label(a)
%
% Input Arguments 
%   a : instance de clc_colorbar
%
% Output Arguments
%   posLabel :  position des labels {'left', 'right', 'down', 'up'}
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: get_pos_label.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function posLabel = get_pos_label(this)

% Maintenance Auto : try
    posLabel = this.posLabel;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_colorbar', 'get_pos_label', lasterr);
% Maintenance Auto : end
