% Retourne les ticks de la colorbar
%
% Syntax
%   tick = get_tick(this)
%a
% Input Arguments 
%   a        : instance de clc_colorbar
%
% Output Arguments
%   tick :  tick de la colorbar
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: get_tick.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function tick = get_tick(this)

% Maintenance Auto : try
    tick = this.tick;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_colorbar', 'get_tick', lasterr);
% Maintenance Auto : end
