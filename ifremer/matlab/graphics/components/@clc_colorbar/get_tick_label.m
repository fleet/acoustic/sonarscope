% Retourne les labels de la colorbar
%
% Syntax
%   tickLabel = get_tick_label(a)
%
% Input Arguments 
%   a : instance de clc_colorbar
%
% Output Arguments
%   tickLabel :  tick_label de l axe 'hor' | 'ver'
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: get_tick_label.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function tickLabel = get_tick_label(this)

% Maintenance Auto : try
    tickLabel = this.tickLabel;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_colorbar', 'get_tick_label', lasterr);
% Maintenance Auto : end
