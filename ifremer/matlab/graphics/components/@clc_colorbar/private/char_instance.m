% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de clc_colorbar
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.3 2003/07/21 15:28:19 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
    
    % -------------------------
    % Intialisation des locales
    
    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('orientation  <-> %s', this.orientation);
    str{end+1} = sprintf('tick         <-> %s', num2strCode(this.tick));
    if isempty(this.tickLabel)
        str{end+1} = sprintf('tickLabel    <-> [ ]');
    else
        str{end+1} = sprintf('tickLabel    <-> [%s]', sprintf('%s ', this.tickLabel{:}));
    end
    str{end+1} = sprintf('posLabel     <-> %s', this.posLabel);
    str{end+1} = sprintf('axis         <-> %s', mat2str(this.axis));

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_colorbar', 'char_instance', lasterr);
% Maintenance Auto : end
