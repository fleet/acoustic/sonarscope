% Initialisation des attributs de l'instance
%
% Syntax
%   a = set(a, ...)
%
% a PARAMETERS :
%   this : instance de clc_colorbar
%
% Name-Value Pair Arguments
%   orientation : orientation de l axe {'hor', 'ver'}
%   tick        : valeur de position des labels
%   tickLabel   : valeur texte des labels
%   posLabel    : positionnement des labels :
%                       hor, {'down', 'up'}
%                       ver, {'left', 'right'}
%   axis        : min et max en x et en y
%
% Output Arguments
%   a : instance de clc_colorbar initialisee
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% -------------------------------------------
% Lecture des proprietes donnees en arguments

[varargin, orientation] = getPropertyValue(varargin, 'orientation', []);
[varargin, tick] = getPropertyValue(varargin, 'tick', []);
[varargin, tickLabel] = getPropertyValue(varargin, 'tickLabel', []);
[varargin, posLabel] = getPropertyValue(varargin, 'posLabel', []);
[varargin, axis] = getPropertyValue(varargin, 'axis', []);

% ----------------------------
% Initialisation de l'instance

if ~isempty(orientation)
    this = set_orientation(this, orientation);
end
if ~isempty(tick)
    this = set_tick(this, tick);
end
if ~isempty(tickLabel)
    this = set_tick_label(this, tickLabel);
end
if ~isempty(posLabel)
    this = set_pos_label(this, posLabel);
end
if ~isempty(axis)
    this = set_axis(this, axis);
end

% ------------------
% Appel super-classe

if ~isempty(varargin)
    this.clc_axe = set(this.clc_axe, varargin {:});
end
