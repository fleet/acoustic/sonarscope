% Applique les min et max selon x et y
%
% Syntax
%   a = set_axis(a, axis)
%
% Input Arguments 
%   a        : instance de clc_colorbar
%   axis        : min et max en x et y
%
% Output Arguments
%   a : instance de clc_colorbar initialisee
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: set_axis.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_axis(this, axis)

isOk = 1;

% ------------------
% Test des arguments

if ~isnumeric(axis)
    isOk = 0;
    my_warndlg('clc_colorbar/set_axis : Invalid argument type', 0);
end

% ----------------------------------
% Impose les min et max a l'instance

if isOk
    this.axis = axis;
    if is_edit(this.clc_axe)
        hAxe = get_handle(this.clc_axe);
        set(hAxe, 'XLim', axis(1:2), 'YLim', axis(3:4), 'ZLim', [0 1]);
    end
end
