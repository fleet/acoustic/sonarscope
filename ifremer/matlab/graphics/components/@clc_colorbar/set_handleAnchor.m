% Stockage de l'ancre du composant
%
% Syntax
%   this = set_handleAnchor(this, h)
%
% Input Arguments
%   this : instance de clc_pushbutton
%   h    : Handle de l'ancre
%
% Output Arguments
%   this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_handleAnchor(this, h)

this.handleAnchor = h;
this.clc_axe = set_handleAnchor(this.clc_axe, h);