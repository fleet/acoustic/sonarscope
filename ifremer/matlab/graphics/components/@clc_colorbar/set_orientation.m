% Applique l'orientation de la colorbar : implique la position des labels
%
% Syntax
%   this = set_orientation(this, orientation)
%
% Input Arguments 
%   this        : instance de clc_colorbar
%   orientation : orientation de l axe {'hor', 'ver'}
%
% Output Arguments
%   this : instance de clc_colorbar initialisee
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_orientation(this, orientation)

isOk = 1;

%% Test des arguments

if ~ischar(orientation)
    isOk = 0;
    my_warndlg('clc_colorbar/set_orientation : Invalid argument type', 0);
elseif (strcmp(orientation, 'hor') == 0) && (strcmp(orientation, 'ver') == 0)
    isOk = 0;
    my_warndlg('clc_colorbar/set_orientation : Invalid argument value', 0);
end

%% Impose les ticks et labels sur l axe x ou l axe y

if isOk
    this.orientation = orientation;
    if strcmp(orientation, 'hor')
        this.posLabel = 'down';
    else
        this.posLabel = 'right';
    end
    if is_edit(this.clc_axe)
        hAxe = get_handle(this.clc_axe);
        if strcmp(orientation, 'hor')
            set(hAxe, 'XTick',      this.tick);
            set(hAxe, 'XTickLabel', this.tickLabel);
            set(hAxe, 'YTick',      []);
            set(hAxe, 'YTickLabel', []);
        else
            set(hAxe, 'XTick',      []);
            set(hAxe, 'XTickLabel', []);
            set(hAxe, 'YTick', this.tick);
            if isa(hAxe, 'matlab.graphics.axis.Axes')
                for k=1:length(this.tickLabel)
                    if iscell(this.tickLabel{k})
                        this.tickLabel{k} = this.tickLabel{k}{1};
                    end
                end
                hAxe.YTickLabel = this.tickLabel;
            else
                set(hAxe, 'YTickLabel', this.tickLabel);
            end
        end
    end
end
