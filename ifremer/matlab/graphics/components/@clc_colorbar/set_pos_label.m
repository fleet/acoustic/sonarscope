% Impose la position des labels par rapport a la colorbar
%
% Syntax
%   a = set_pos_label(a)
%
% Input Arguments 
%   a        : instance de clc_colorbar
%   posLabel : positionnement des labels par rapport a la colorbar
%
% Output Arguments
%   a : instance de clc_colorbar initialisee
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: set_pos_label.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_pos_label(this, posLabel)

% ----------------------------------------------
% Ne travail que si il y a quelque chose a faire

if isempty(posLabel)
    return
end

% ------
% Locale

isOk = 1;

% ------------------
% Test des arguments

if ~ischar(posLabel)
    isOk = 0;
    my_warndlg('clc_colorbar/set_pos_label : Invalid argument type', 0);
else
    if strcmp(this.orientation, 'lig')
        if strcmp(posLabel, 'left') || strcmp(posLabel, 'right')
            isOk = 0;
            my_warndlg('clc_colorbar/set_pos_label : Invalid position', 0);
        end
    else
        if strcmp(posLabel, 'bottom') || strcmp(posLabel, 'top')
            isOk = 0;
            my_warndlg('clc_colorbar/set_pos_label : Invalid position', 0);
        end
    end
end

% ----------------------------
% Initialisation de l'instance

if isOk
    switch posLabel
        case 'down'
            this.orientation = 'hor';
            this.posLabel = posLabel;
            if is_edit(this.clc_axe)
                set(get_handle(this.clc_axe), 'XAxisLocation', 'bottom');
            end
        case 'up'
            this.orientation = 'hor';
            this.posLabel = posLabel;
            if is_edit(this.clc_axe)
                set(get_handle(this.clc_axe), 'XAxisLocation', 'top');
            end
        case 'left'
            this.orientation = 'ver';
            this.posLabel = posLabel;
            if is_edit(this.clc_axe)
                set(get_handle(this.clc_axe), 'YAxisLocation', 'left');
            end
        case 'right'
            this.orientation = 'ver';
            this.posLabel = posLabel;
            if is_edit(this.clc_axe)
                set(get_handle(this.clc_axe), 'YAxisLocation', 'right');
            end
        otherwise
            my_warndlg('clc_colorbar/set_pos_label : Invalid argument value', 0);
    end
end
