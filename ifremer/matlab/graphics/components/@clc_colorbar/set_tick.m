% Applique les ticks a l'instance : position des labels
%
% Syntax
%   a = set_tick(a, tick)
%
% Input Arguments 
%   a    : instance de clc_colorbar
%   tick : tick de l axe
%
% Output Arguments
%   a : instance de clc_colorbar initialisee
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: set_tick.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_tick(this, tick)

isOk = 1;

% ------------------
% Test des arguments

if ~isnumeric(tick)
    isOk = 0;
    my_warndlg('clc_colorbar/set_tick : Invalid argument type', 0);
end

% -------------------------------------------------
% Impose les ticks et labels sur l'axe x ou l'axe y

if isOk
    
    this.tick = tick;
    if length(this.tickLabel) ~= length(this.tick)
        this.tickLabel = [];
    end
    this = set_orientation(this, this.orientation);  
end
