% Applique les labels aux ticks a l'instance
%
% Syntax
%   a = set_tick_label(a, tickLabel)
%
% Input Arguments 
%   a         : instance de clc_colorbar
%   tickLabel : labels pour les tick de l instance
%
% Output Arguments
%   a : instance de clc_colorbar initialisee
%
% Examples
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: set_tick_label.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_tick_label(this, tickLabel)

isOk = 1;

% -------------------
% Test des arguments

if ~iscell(tickLabel)
    isOk = 0;
    my_warndlg('clc_colorbar/set_tick_label : Invalid argument type', 0);
end

% -------------------------------------------------
% Impose les ticks et labels sur l'axe x ou l'axe y

if isOk
    this.tickLabel = tickLabel;
    if length(this.tickLabel) ~= length(this.tick)
        this.tick = [];
    end
    this = set_orientation(this, this.orientation);
end
