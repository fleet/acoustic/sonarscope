% Test graphique du composant
%
% Syntax
%   a = test(a)
% 
% Input Arguments
%   a : instance de clc_colorbar
%
% Output Arguments
%   a : instance de clc_colorbar
% 
% Examples
%    this = test(clc_colorbar);
%
% See also clc_colorbar Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/07/21 15:29:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/06/2001 - DCF - creation
% ----------------------------------------------------------------------------

function this = test(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Gestion des messages de callback (pour visualiser l'arrivee des messages)
    
    % --------------------------------------------------------------------------
    % Creation d une figure ainsi que de l ancre du composant
    
    figure;
    h = uicontrol(  'Visible'  , 'on', ...
                    'Tag'      , 'ancre_clc_colorbar' , ...
                    'Position' , [20 20 300 300]);
    
    % ---------------------------------------
    % Creation et initialisation du composant
    
    this = clc_colorbar([]);
    this = set_tag(this, 'Axe');
    this = set_name(this,'ancre_clc_colorbar' );
    this = set_user_name(this,  'clc_colorbar');
    this = set_user_cb(this, 'gerer_callback');
    this = set_orientation(this, 'hor');
    this = set_axis(this, [-5 10 0 1]);
    % this = set_orientation(this, 'ver');
    % this = set_axis(this, [0 1 -5 10]);
    this = set_tick(this, -5:3:10);
    label = [];
    for i = -5:3:10
        label {end+1} = num2str(i);
    end
    this = set_tick_label(this, label);
    
    % -------------------------------
    % Creation graphique du composant
    
    this = editobj(this);
    
    % ------------------------
    % Rend le resultat visible
    
    set(h, 'Visible', 'off');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_colorbar', 'test', lasterr);
% Maintenance Auto : end
