% Remplissage de la colorbar par la colormap
% En fonction ssi le composant est edite
%
% Syntax
%   a = update_colormap(a) 
%
% Input Arguments
%   a : instance de clc_colorbar
%
% OUPUT PARAMETERS :
%   a : instance de clc_colorbar
%
% See also clc_colorbar Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = update_colormap(this)

%% Lecture d une valeur par defaut si les infos de dimension ne sont pas
% dispo

if isempty(this.axis)
    hAxe = get_handle(this.clc_axe);
    XLim = get(hAxe, 'XLim');
    YLim = get(hAxe, 'YLim');
    limAxis = [XLim, YLim];
    this = set_axis(this, limAxis);
end

%% Construction de l'image de la colormap

if strcmp(this.orientation, 'hor')
    colormap   = linspace(this.axis(2), this.axis(1), 256);
    colormapX  = colormap;
    colormapY  = [0 1];
    colormap   = repmat(colormap, 2, 1);
else
    colormap   = linspace(this.axis(4), this.axis(3), 256);
    colormapX  = [0 1];
    colormapY  = colormap;
    colormap   = repmat(colormap', 1, 2);
end

%% Mise � jour de l'image de la colormap

if is_edit(this.clc_axe)
    hAxe = get_handle(this.clc_axe);
    hfig = get(hAxe, 'parent');
    currentAxes = get(hfig, 'CurrentAxes');
    
    hChildren = get(hAxe, 'Children');
    if (length(hChildren) == 1) && isAGraphicElement(hChildren,'image')
        % Rajout� par JMA le 19/12/2012 pour minimiser les effets de flicks
        set(hChildren, 'XData', colormapX, 'YData', colormapY, 'CData', colormap)
    else
        set(hfig, 'CurrentAxes', hAxe);
        cla(hAxe, 'reset')
        CurrentFig = gcf; % Rajout� par JMA le 18/12/12 pour �viter que la colorbar ne se dessine dans une autre figure
        figure(hfig)
        set(hfig, 'CurrentAxes', hAxe);
        imagesc(colormapX, colormapY, colormap);
        figure(CurrentFig) % Rajout� par JMA le 18/12/12 pour �viter que la colorbar ne se dessine dans une autre figure
    end
    
    axis(hAxe, 'xy', 'tight');
    this = set_pos_label(this, this.posLabel);
    if strcmp(this.orientation, 'hor')
        set(hAxe, 'XTick', this.tick);
        set(hAxe, 'XTickLabel', this.tickLabel);
        set(hAxe, 'YTick', []);
        set(hAxe, 'YTickLabel', []);
    else
        set(hAxe, 'XTick', []);
        set(hAxe, 'XTickLabel', []);
        set(hAxe, 'YTick', this.tick);
        set(hAxe, 'YTickLabel', this.tickLabel);
    end
    if ishandle(currentAxes)
        set(hfig, 'CurrentAxes', currentAxes);
    end
end
