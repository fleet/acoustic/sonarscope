% Ajout d'un composant
%
% Syntax
%   a = add(a, indiceElement, element)
% 
% Input Arguments
%   a             : instance de clc_cv
%   indiceElement : indice de l element concerne par l operation
%   element       : composant ajoute (de type de base cl_component)
%
% OUPUT PARAMETERS :
%   a : instance de clc_cv initialisee
%
% See also clc_cv
% Authors : DCF
% VERSION  : $Id: add.m,v 1.4 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = add(this, indiceElement, element)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get_nb_elements(this.clc_framev))
    
    % --------------------------
    % Test le type de l'instance
    
    if isa(element, 'cl_component')
        
        % -------------------------------------------------------------
        % Ajout de l'element a la liste et mise a jour de ses proprietes
        
        this.cListe{indiceElement} = element;
        
        if is_collapse(this, indiceElement)
            visibilite  = 'off';
        else
            if is_visible(this, indiceElement)
                visibilite = 'on';
            else
                visibilite = 'off';
            end
        end
        
        elt = this.cListe{indiceElement};
        elt = set_name(elt, get_tag(this.clc_framev, indiceElement));
        elt = set_user_name(elt, get_user_name(this));
        elt = set_user_cb(elt, get_user_cb(this));
        elt = set(elt, 'componentEnable', get_enable(this), 'componentVisible', visibilite);
        this.cListe{indiceElement} = elt;
        
        % ---------------------------------------------------------
        % Si representation graphique courante : construction de la
        % representation grahique de l element
        
        if is_edit(this) && ~is_edit(this.cListe{indiceElement})
            this.cListe{indiceElement} = editobj(this.cListe{indiceElement});
        end
        
    else
        my_warndlg('clc_cv/add : Invalid component type', 0);
    end
    
else
    my_warndlg('clc_cv/rem : Indice out of range', 0);
end
