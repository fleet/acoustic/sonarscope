% Transformation en chaine de caracteres d un tableau d instances
%
% Syntax
%   str = char(a) 
%
% Input Arguments
%   a : instance ou tableau d'instances de clc_cv
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%    a = clc_cv;
%    str = char(a)
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    str = [];
    [m, n] = size(this);
    
    if (m*n) > 1
        
        % -------------------
        % Tableau d'instances
        
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        % ---------------
        % Instance unique
        
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'char', lasterr);
% Maintenance Auto : end
