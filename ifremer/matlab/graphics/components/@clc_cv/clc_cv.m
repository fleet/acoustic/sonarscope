% Constructeur de clc_cv, classe composant
%
% Syntax
%   a = clc_cv(...)
% 
% Name-Value Pair Arguments
%   nbElements        : nombre de frames du vecteur de frames
%   tag               : base du tag des frames
%   orientation       : orientation du vecteur : 'lig' ou 'col'
%   cListe            : liste de composants
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments 
%   a : instance de clc_cv
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    a = clc_cv
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: clc_cv.m,v 1.4 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    01/02/2000 - DCF - creation
%    23/04/2001 - DCF - mise en reference
%    05/06/2001 - DCF - mise a jour code et optimisation
% ----------------------------------------------------------------------------

function this = clc_cv(varargin)

% Maintenance Auto : try
    
    % --------------------------------------------
    % Definition de la structure et initialisation
    %
    % cListe            : liste de composants (de longeur nbElements)
    % fComponentVisible : visibilite des composants
    
    this.cListe            = [];
    this.fComponentVisible = [];
    
    % ------------
    % Super-classe
    
    if (nargin == 1) & isempty(varargin{1})
        vectorFrames = clc_framev([]);
    else
        vectorFrames = clc_framev;
    end
    
    % -------------------
    % Creation de l'objet
    
    this = class(this, 'clc_cv', vectorFrames);
    
    % ----------------------------
    % Pre-nitialisation des champs 
    
    if (nargin == 1) & isempty(varargin{1})
        return
    end
    
    % ---------------------------------------------------------------------
    % Initialisation des valeurs transmises lors de l'appel au constructeur
    
    this = set(this, varargin{:});
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'clc_cv', lasterr);
% Maintenance Auto : end
