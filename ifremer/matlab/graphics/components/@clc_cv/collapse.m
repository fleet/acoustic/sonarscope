% Suppression virtuelle d'un element du vecteur
%
% Syntax
%   a = collapse(a, indiceElement, value)
% 
% Input Arguments
%   a             : instance de clc_cv
%   indiceElement : indice de l element concerne par l operation
%   value         : realite de l element {'on', 'off'}
%
% Input Arguments
%   a : instance de clc_cv
%
% Remarks : doit etre surchargee
%
% See also clc_cv Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = collapse(this, indiceElement, value)

%% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get_nb_elements(this.clc_framev))
    
    %% Test de la visibilite
    
    if strcmp(value, 'on') || strcmp(value, 'off')
        
        % --------------------------
        % Appel methode super classe
        
        this.clc_framev = collapse(this.clc_framev, indiceElement, value);
        
        %% Traitement du composant
        
        if ~isempty(this.cListe {indiceElement})
            
            if is_collapse(this, indiceElement)
                visibilite = 'off';
            else
                if this.fComponentVisible(indiceElement)
                    visibilite = 'on';
                else
                    visibilite = 'off';
                end
            end
            
            this.cListe {indiceElement} = set_visible(this.cListe {indiceElement}, visibilite);
        end
        
        %% Mise � jour generale
        
        this = update(this);
        
    else
        my_warndlg('clc_cv/collapse : Invalid visibility value', 0);
    end
    
else
    my_warndlg('clc_cv/collapse : Invalid element indice', 0);
end
