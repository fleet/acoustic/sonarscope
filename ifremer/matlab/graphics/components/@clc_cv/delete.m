% Destruction du composant
%
% Syntax
%   a = delete(a)
% 
% Input Arguments
%   a : instance de clc_cv
%
% Remarks : doit etre surchargee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % ------------------------------------
    % Destruction de chacun des composants
    
    for i=1:length(this.cListe)
        if ~isempty(this.cListe {i})
            this.cListe {i} = delete(this.cListe {i});
        end
    end
    
    % ----------------------------------------
    % Destruction graphique de la super classe
    
    this.clc_framev = delete(this.clc_framev);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'delete', lasterr);
% Maintenance Auto : end
