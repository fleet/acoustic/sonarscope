% Affichage des donnees d'une instance de clc_cv
%
% Syntax
%   display(a)
%
% Input Arguments 
%   a : instance de clc_cv
%
% Examples
%   a = clc_cv;
%   display(a)
%   a
%
% See also clc_cv Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
