% Representation de l'instance
%
% Syntax
%   a = editobj(a) 
%
% Input Arguments
%   a : instance de clc_cv
%
% OUPUT PARAMETERS :
%   a : instance de clc_cv
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: editobj.m,v 1.4 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
%    05/06/2001 - DCF - mise a jour code et optimisation
% ----------------------------------------------------------------------------

function this = editobj(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Realite graphique ssi le nombre d element est strictement positif
    
    if (get_nb_elements(this) > 0) & ~is_edit(this)
        
        % ---------------------------------------
        % Traitement graphique de la super classe
        
        this.clc_framev = editobj(this.clc_framev);
        
        % ---------------------------------------------
        % Traitement graphique de chacun des composants
        
        this = editobj_component(this);
        
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'editobj', lasterr);
% Maintenance Auto : end
