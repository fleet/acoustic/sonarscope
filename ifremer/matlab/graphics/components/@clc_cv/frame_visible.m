% Initialisation de la visibilite de la frame d'un element du vecteur
%
% Syntax
%   a = frame_visible(a, indiceElement, visibilite)
% 
% Input Arguments
%   a             : instance de clc_cv
%   indiceElement : indice de l element concerne par l operation
%   visibilite    : visibilite de l element {'on', 'off'}
% 
% Output Arguments
%   a : instance de clc_cv
%
% Remarks : doit etre surchargee
%
% See also clc_cv Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = frame_visible(this, indiceElement, visibilite)

%% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get_nb_elements(this.clc_framev))
    
    %% Test de la visibilité
    
    if strcmp(visibilite, 'on') || strcmp(visibilite, 'off')
        
        %% Traitement de la super classe
        
        this.clc_framev = visible(this.clc_framev, indiceElement, visibilite);
        
    else
        my_warndlg('clc_cv/frame_visible : Invalid visibility value', 0);
    end
    
else
    my_warndlg('clc_cv/frame_visible : Invalid element indice', 0);
end
