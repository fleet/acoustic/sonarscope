% Lecture des attributs de l'instance
%
% Syntax
%   [...] = get(a, ...)
% 
% Input Arguments
%   a : instance de clc_cv
%
% Name-Value Pair Arguments
%   cListe            : liste de composants (nbElements elements)
%   nbElements        : nombre de frames du vecteur de frames
%   tag               : base du tag des frames
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant
%   componentEnable   : accessibilite du composant
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    c = clc_cv('nbElements', 3);
%    get(c, 'nbElements')
%
% See also clc_cv Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'cListe'
            varargout{end+1} = get_c_liste(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.clc_framev, varargin{i}); %#ok<AGROW>
    end
end
