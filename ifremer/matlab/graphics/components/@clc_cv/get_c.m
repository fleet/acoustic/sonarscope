% Retourne le composant specifie par indice
%
% Syntax
%   b = get_c(a, indiceElement)
% 
% Input Arguments
%   a             : instance de clc_cv
%   indiceElement : indice de l element concerne par l operation
%
% OUPUT PARAMETERS :
%   b : instance de clc_cv
%
% See also clc_cv
% Authors : DCF
% VERSION  : $Id: get_c.m,v 1.4 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function composant = get_c(this, indiceElement)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get_nb_elements(this.clc_framev))
    composant = this.cListe {indiceElement};
else
    composant = [];
    my_warndlg('clc_cv/get_c : Indice out of range', 0);
end
