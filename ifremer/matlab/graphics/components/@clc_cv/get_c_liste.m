% Retourne la liste des composants
% 
% Syntax
%   cListe = get_c_liste(a)
%
% Input Arguments
%   a : instance de clc_cv
%
% Output Arguments
%   cListe   : liste des composants
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: get_c_liste.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function cListe = get_c_liste(this)

% Maintenance Auto : try
    cListe = this.cListe;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'get_c_liste', lasterr);
% Maintenance Auto : end
