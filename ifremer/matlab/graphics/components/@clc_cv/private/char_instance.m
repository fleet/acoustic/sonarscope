% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de clc_cv
%
% Output Arguments
%   str : une chaine de caracteres representative de l'instance
%
% Examples
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.4 2003/07/22 08:45:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
 
    str = [];
    
    % ----------
    % Traitement
    
    for i=1:length(this.cListe)
        str{end+1} = sprintf('cListe(%d) : ----------------------------------------------', i);
        str{end+1} = char(this.cListe{i});
    end
    str{end+1} = sprintf('fComponentVisible <-> %s', mat2str(this.fComponentVisible));
    
    str{end+1} = sprintf('--- heritage clc_framev ---');
    str{end+1} = char(this.clc_framev);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'char_instance', lasterr);
% Maintenance Auto : end
