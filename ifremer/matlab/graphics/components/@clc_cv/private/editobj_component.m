% Construction de base de l'IHM
% 
% Syntax
%   a = construire_component(a)
%
% Input Arguments
%   a : instance de clc_cv
%
% Output Arguments
%   a : instance de clc_cv
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: editobj_component.m,v 1.3 2003/07/21 16:27:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    02/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = editobj_component(this)

% Maintenance Auto : try
    
    % -----------------------------------
    % Parcours de la liste des composants
    
    for i=1:length(this.cListe)
        
        % -----------------------------------------------
        % Ne traite que les elements non vide de la liste
        
        if ~isempty(this.cListe{i})
            
            % ---------------------------------------------------------------
            % Impose au composant :
            % - son ancre
            % - la classe utilisatrice
            % - la methode de gestion des call back de la classe utilisatrice
            % - la visibilite du composant
            % - l accessibilite du composant
            
            if is_collapse(this, i)
                visibilite  = 'off';
            else
                
                if this.fComponentVisible
                    visibilite = 'on';
                else
                    visibilite = 'off';
                end
                
            end
            
            this.cListe{i} = set(this.cListe{i}, ...
                'componentName'    , get_tag(this, i), ...
                'componentUserName', get(this, 'componentUserName'), ...
                'componentUserCb'  , get(this, 'componentUserCb'), ...
                'componentEnable'  , get(this, 'componentEnable'), ...
                'componentVisible' , visibilite);
            
            % -----------------------------------
            % Construction graphique du composant
            
            this.cListe{i} = editobj(this.cListe{i});
            
        end
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'editobj_component', lasterr);
% Maintenance Auto : end
