% Suppression d'un composant
%
% Syntax
%   a = rem(a, indiceElement)
% 
% Input Arguments
%   a             : instance de clc_cv
%   indiceElement : indice de l'element concerne par l'operation
%
% OUPUT PARAMETERS :
%   a : instance de clc_cv
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: rem.m,v 1.4 2003/07/21 16:28:03 augustin Exp $
% -------------------------------------------------------------------------

function this = rem(this, indiceElement)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get_nb_elements(this.clc_framev))
    this.cListe {indiceElement} = delete(this.cListe{indiceElement});
    this.cListe {indiceElement} = [];
else
    my_warndlg('clc_cv/rem : Indice out of range', 0);
end
