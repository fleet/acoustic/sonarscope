% Modification des attributs de l'instance
%
% Syntax
%   a = set(a, varargin)
%
% Input Arguments
%   a : instance de clc_cv
%
% Name-Value Pair Arguments
%   cListe            : liste de composants (nbElements elements)
%   nbElements        : nombre de frames du vecteur de frames
%   tag               : base du tag des frames
%   orientation       : orientation du vecteur {'lig', 'col'}
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments
%   a : instance de clc_cv initialisee
%
% Examples
%    c = clc_cv;
%    set(c, 'nbElements', 3, 'tag', 'clc_cv_tag')
%
% See also clc_cv Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% Maintenance Auto : try

% ---------------------
% Lecture des arguments

[varargin, cListe] = getPropertyValue(varargin, 'cListe', []);
[varargin, nbElements] = getPropertyValue(varargin, 'nbElements', []);
[varargin, orientation] = getPropertyValue(varargin, 'orientation', []);
[varargin, visible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, enable] = getPropertyValue(varargin, 'componentEnable', []);

% ----------------------------
% Initialisation des attributs

if ~isempty(cListe)
    this = set_c_liste(this, cListe);
end
if ~isempty(nbElements)
    this = set_nb_elements(this, nbElements);
end
if ~isempty(orientation)
    this = set_orientation(this, orientation);
end
if ~isempty(visible)
    this = set_visible(this, visible);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end

% -----------------------------------------
% Traitement des message de la super classe

if ~isempty(varargin)
    this.clc_framev = set(this.clc_framev, varargin{:});
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
