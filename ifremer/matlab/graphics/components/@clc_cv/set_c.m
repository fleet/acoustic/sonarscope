% Initialise le composant specifie par indice
% Aucun test ni mise a jour n est faite (edition, ...). Pour effectuer les 
% tests de modification de la liste, utiliser set_c_liste.
%
% Syntax
%   a = set_c(a, indiceElement, composant)
% 
% Input Arguments
%   a             : instance de clc_cv
%   indiceElement : indice de l element concerne par l operation
%   composant     : composant devant etre ajoute
%
% OUPUT PARAMETERS :
%   a : instance de clc_cv initialisee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: set_c.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_c(this, indiceElement, composant)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get_nb_elements(this.clc_framev))
    this.cListe {indiceElement} = composant;
else
    my_warndlg('clc_cv/set_c : Indice out of range', 0);
end
