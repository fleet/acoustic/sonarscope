% Initialisation de la liste des composants
%
% Syntax
%   a = set_c_liste(a, cListe)
%
% Input Arguments
%   a      : instance de clc_cv
%   cListe : liste des composants
%
% Output Arguments
%   a : instance de clc_cv initialisee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: set_c_liste.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_c_liste(this, cListe)

% -------------------------------------------
% Test validite des arguments, initialisation

if iscell(cListe)
    
    % ------------------------------------
    % Destruction des composants existants
    
    for i=1:length(this.cListe)
        if ~isempty(this.cListe {i})
            this.cListe {i} = delete(this.cListe {i});
            this.cListe {i} = [];
        end
    end
    
    % --------------------
    % Ajout des composants
    
    for i=1:length(cListe)
        if ~isempty(cListe {i})
            this = add(this, i, cListe{i});
        end
    end
    
else
    my_warndlg('clc_cv/set_cListe : Invalid argument format', 0);
end
