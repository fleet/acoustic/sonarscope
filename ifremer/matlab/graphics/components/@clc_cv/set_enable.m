% Initialisation de l accessibilite du composant
%
% Syntax
%   a = set_enable(a, enable)
%
% Input Arguments
%   a      : instance de clc_cv
%   enable : accessibilite du composant {'on', 'off'}
%
% Output Arguments
%   a : instance de clc_cv initialisee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    
    % ---------------------------------------------
    % Initialise l accessibilite de la super classe
    
    this.clc_framev = set_enable(this.clc_framev, enable);
    
    % -------------------------------------------------------------
    % Initialise l accessibilite de tous les composants de la liste
    
    for i=1:length(this.cListe)
        if ~isempty(this.cListe{i})
            this.cListe{i} = set(this.cListe{i}, 'componentEnable', enable);
        end
    end
    
else
    my_warndlg('clc_cv/set_enable : Invalid value', 0);
end
