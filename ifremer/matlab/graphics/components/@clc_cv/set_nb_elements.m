% Initialisation du nombre de composants (elements de la liste)
%
% Syntax
%   a = set_nb_elements(a, nbElements)
%
% Input Arguments
%   a          : instance de clc_cv
%   nbElements : nombre d elements du vecteur de composants
%
% Output Arguments
%   a : instance de clc_cv initialisee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: set_nb_elements.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_nb_elements(instance, nbElements)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(nbElements)
    
    % -----------------------------------------
    % Mise a jour du nombre par la super classe
    
    instance.clc_framev = set_nb_elements(instance.clc_framev, nbElements);
    
    % -----------------------------------------
    % Reconstruction de la liste des composants
    
    instance.fComponentVisible  = ones(1, nbElements);
    newListe = cell(1, nbElements);
    nbMin = min([length(newListe), length(instance.cListe)]);
    for i = 1:length(instance.cListe)
        if ~isempty(instance.cListe{i})
            instance.cListe{i} = delete(instance.cListe{i});
        end
    end
    for i=1:nbMin
        newListe{i} = instance.cListe{i};
    end
    instance.cListe = newListe;
    if is_edit(instance)
        for i = 1:length(instance.cListe)
            if ~isempty(instance.cListe{i})
                instance.cListe{i} = editobj(instance.cListe{i});
            end
        end
    end
    
else
    my_warndlg('clc_framev/set_nbElements : Invalid argument format', 0);
end
