% Initialisation de l'orientation du vecteur
%
% Syntax
%   a = set_orientation(a, orientation)
%
% Input Arguments
%   a           : instance de clc_cv
%   orientation : orientation du vecteur de frames
%
% Output Arguments
%   a : instance de clc_cv initialisee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: set_orientation.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% -------------------------------------------------------------------------

function this = set_orientation(this, orientation)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(orientation)
    
    if strcmp(orientation, 'lig') || strcmp(orientation, 'col') || strcmp(orientation, 'mat')
        
        % --------------------------
        % Appel methode super classe
        
        this.clc_framev = set_orientation(this.clc_framev, orientation);
        
        % -----------
        % Mise a jour
        
        this = update(this);
        
    else
        my_warndlg('clc_cv/set_orientation : Invalid orientation value', 0);
    end
else
    my_warndlg('clc_cv/set_orientation : Invalid argument format', 0);
end
