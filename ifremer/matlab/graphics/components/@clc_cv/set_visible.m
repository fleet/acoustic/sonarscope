% Initialisation de la visibilite du composant
% 
% Syntax
%   a = set_visible(a, fVisible)
%
% Input Arguments
%   a        : instance de clc_cv
%   fVisible : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   a : instance de clc_cv initialisee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: set_visible.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_visible(this, fVisible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(fVisible)
    
    % ---------------------------------------------
    % Initialise l'accessibilite de la super classe
    
    this.clc_framev = set_visible(this.clc_framev, fVisible);
    
    % -------------------------------------------------------------
    % Initialise l'accessibilite de tous les composants de la liste
    
    for i = 1:length(this.cListe)
        if ~isempty(this.cListe {i})
            
            if strcmp(fVisible, 'on') && ~is_collapse(this, i)
                this = visible(this, i, fVisible);
            elseif is_collapse(this, i)
                if strcmp(fVisible, 'on')
                    this.fComponentVisible(i) = 1;
                else
                    this.fComponentVisible(i) = 0;
                end
            else
                this = visible(this, i, 'off');
            end
            
        end
    end
    
else
    my_warndlg('clc_cv/set_visible : Invalid value', 0);
end
