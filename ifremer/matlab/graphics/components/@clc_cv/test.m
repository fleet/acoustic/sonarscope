% Test graphique du composant
%
% Syntax
%   a = test(a)
% 
% Input Arguments
%   a : instance de clc_cv
%
% Output Arguments
%   instance : instance de clc_cv
% 
% Examples
%    a = test(clc_cv);
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this, varargin)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Gestion des messages de call back (pour visualiser l arrivee des messages)
    
    % --------------------------------------------------------------------------
    % Creation d une figure ainsi que de l ancre du composant
    
    figure('Visible', 'off');
    h = uicontrol(  'Visible',  'on', ...
                    'Tag',      'ancre_clc_cv', ...
                    'Position', [20 20 500 100]);
    
    % ---------------------------------------
    % Creation et initialisation du composant
    
    this = clc_cv;
    this = set(this, ...
        'nbElements',           3, ...
        'tag',                  'clc_cv tag', ...
        'orientation',          'lig', ...
        'componentName',        'ancre_clc_cv', ...
        'componentUserName',    'clc_cv', ...
        'componentUserCb',      '');
    
    % -------------------------------
    % Creation graphique du composant
    
    this = editobj(this);
    
    % ------------------------
    % Rend le resultat visible
    
    set(gcf, 'Visible', 'on');
    set(h, 'Visible', 'off');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_cv', 'test', lasterr);
% Maintenance Auto : end
