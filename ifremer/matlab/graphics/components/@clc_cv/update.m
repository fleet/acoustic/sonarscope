% Mise a jour du composant
%
% Syntax
%   a = update(a)
% 
% Input Arguments
%   a : instance de clc_cv
%
% Remarks : doit etre surchargee
%
% See also clc_cv
% Authors : DCF
% ----------------------------------------------------------------------------

function this = update(this)

%% Update graphique de la super classe

this.clc_framev = update(this.clc_framev);

%% Update graphique des elements graphiques du composant

for k=1:length(this.cListe)
    if ~isempty(this.cListe{k})
        this.cListe{k} = update(this.cListe{k});
    end
end
