% Initialisation de la visibilite d un element du vecteur
%
% Syntax
%   a = visible(a, indiceElement, visibilite)
%
% Input Arguments
%   a             : instance de clc_cv
%   indiceElement : indice de l element concerne par l operation
%   visibilite    : visibilite de l element {'on', 'off'}
%
% Output Arguments
%   a : instance de clc_cv
%
% Remarks : doit etre surchargee
%
% See also clc_cv Authors
% Authors : DCF
% VERSION  : $Id: visible.m,v 1.4 2003/07/21 16:28:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = visible(this, indiceElement, visibilite)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get_nb_elements(this.clc_framev))
    
    % ---------------------
    % Test de la visibilite
    
    if strcmp(visibilite, 'on') || strcmp(visibilite, 'off')
        
        % -----------------------
        % Traitement du composant
        
        if ~isempty(this.cListe{indiceElement})
            if strcmp(visibilite, 'on')
                this.fComponentVisible(indiceElement) = 1;
            else
                this.fComponentVisible(indiceElement)= 0;
            end
            this.cListe {indiceElement} = set( ...
                this.cListe{indiceElement}, 'componentVisible', visibilite);
        end
    else
        my_warndlg('clc_cv/visible : Invalid visibility value', 0);
    end
else
    my_warndlg('clc_cv/visible : Invalid element indice', 0);
end
