% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char(a) 
%
% IPUT PARAMETERS :
%    a : instance ou tableau d instance de clc_edit_inc_value
%
% Output Arguments
%   str : chaine de caracteres
%
% Examples
%    char(clc_edit_inc_value)
%
% See also clc_edit_inc_value clc_edit_inc_value/display Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.5 2003/04/28 16:51:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_inc_value', 'char', lasterr);
% Maintenance Auto : end
