% Constructeur du composant clc_edit_inc_value (zone a valeur modifiable par increment)
%
% Syntax
%   a = clc_edit_inc_value(...)
% 
% Name-Value Pair Arguments
%    increment         : valeur d incrementation automatique
%    value             : valeur reelle affichee dans la zone de valeur
%    format            : format d affichage de la valeur
%    minValue          : valeur minimale admissible
%    maxValue          : valeur maximale admissible
%    nbColValue        : nombre de colonnes pour la valeur (en proportion par rapport aux boutons d'increment)
%    tag               : tag du label d affichage de la valeur
%    tooltip           : info-bulle du label d affichage de la valeur
%    editable          : flag indiquant si la valeur est editable
%    msgEdit           : msg de call back en cas d edition de la valeur
%    msgPlus           : msg de call back en cas de click sur +
%    msgMoins          : msg de call back en cas de click sur -
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant    'on' | 'off'
%    componentEnable   : accessibilite du composant 'on' | 'off'
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments 
%    a : instance de clc_edit_inc_value
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    v = clc_edit_inc_value
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_edit_inc_value(varargin)

% --------------------------------------------
% Definition de la structure et initialisation
%
% globalFrame : instance de cl_frame, frame globale du composant
% c_moins     : composant de decrementation de la valeur
% c_value     : composant d edition de la valeur
% c_plus      : composant d incrementation de la valeur
% increment   : valeur de l increment
% nbColValue  : nombre de colonnes pour la valeur

this.globalFrame  = [];
this.c_value      = clc_edit_value;
this.c_plus       = clc_edit_texte;
this.c_moins      = clc_edit_texte;
this.increment    = 1;
this.nbColValue   = 1;
this.handleAnchor = [];   % Internal property

%% Super-classe

composant = cl_component;

%% Cr�ation de l'objet

this = class(this, 'clc_edit_inc_value', composant);

%% Initialisation des composants

this.c_plus = set(this.c_plus, ...
    'texte'      , '+', ...
    'tag'        , [], ...
    'tooltip'    , ['+ ', num2str(this.increment)], ...
    'editable'   , 0, ...
    'actionnable', 1, ...
    'msgEdit'    , '', ...
    'msgAction'  , [get(this,'componentName'),' +']);

this.c_moins = set(this.c_moins, ...
    'texte'      , '-', ...
    'tag'        , [], ...
    'tooltip'    , ['- ', num2str(this.increment)], ...
    'editable'   , 0, ...
    'actionnable', 1, ...
    'msgEdit'    , '', ...
    'msgAction'  , [get(this,'componentName'),' -']);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin {1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
