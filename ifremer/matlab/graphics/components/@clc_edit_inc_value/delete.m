% Destruction graphique du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   this : instance de clc_edit_inc_value
%
% Remarks : doit etre surchargee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.4 2003/04/28 16:51:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % ----------------------------------------
    % Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    % -----------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(this.globalFrame)
        this.c_moins     = delete(this.c_moins);
        this.c_plus      = delete(this.c_plus);
        this.c_value     = delete(this.c_value);
        this.globalFrame = delete(this.globalFrame);
        this.globalFrame = [];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_inc_value', 'delete', lasterr);
% Maintenance Auto : end
