% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Name-Value Pair Arguments
%   cf. clc_edit_inc_value
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
%
% Examples
%    v = clc_edit_inc_value;
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Initialisation des variables

varargout = {};

%% Boucle de parcours de l ensemble des proprietes donnees en arguments

for k=1:length(varargin)
    switch varargin{k}
        case 'increment'
            varargout{end+1} = get_increment(this); %#ok<AGROW>
        case 'nbColValue'
            varargout{end+1} = get_nb_col_value(this); %#ok<AGROW>
        case { 'value', 'format', 'minValue', 'maxValue', ...
                'tooltip', 'tag', 'handle', 'editable',    ...
                'msgEdit' }
            varargout{end+1} = get(this.c_value, varargin {1}); %#ok<AGROW>
        case 'msgPlus'
            varargout{end+1} = get(this.c_plus, 'msgAction'); %#ok<AGROW>
        case 'msgMoins'
            varargout{end+1} = get(this.c_moins, 'msgAction'); %#ok<AGROW>
        case{ 'componentVisible', 'componentEnable', 'componentName', ...
                'componentAnchor' , 'componentUserName', 'componentUserCb'}
            varargout{end+1} = get(this.cl_component, varargin{k}); %#ok<AGROW>
        otherwise
            my_warndlg('clc_edit_inc_value/get : Invalid property name', 0);
    end
end
