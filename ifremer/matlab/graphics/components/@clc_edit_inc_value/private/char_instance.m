% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   str : une chaine de caracteres representative de l'instance
%
% Examples
%
% See also clc_edit_inc_value clc_edit_inc_value/char Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.6 2003/09/16 13:23:43 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('increment <-> %f', this.increment);
    
    str{end+1} = sprintf('--- aggregation c_value (clc_edit_value) ---');
    str{end+1} = char(this.c_value);
    
    str{end+1} = sprintf('--- aggregation c_plus (clc_edit_texte) ---');
    str{end+1} = char(this.c_plus);
    
    str{end+1} = sprintf('--- aggregation c_plus (clc_edit_texte) ---');
    str{end+1} = char(this.c_plus);
    
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :   err ('clc_edit_inc_value', 'char_instance', lasterr);
% Maintenance Auto : end
