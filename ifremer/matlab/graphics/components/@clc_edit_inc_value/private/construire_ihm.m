% Creation de l'IHM d'une instance
%
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   this : instance de clc_edit_inc_value
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = construire_ihm(this)


%% Cr�ation de la frame du composant

this = creer_frame_globale(this);
tab_uicontrols = get_tab_uicontrols(this.globalFrame);

%% Cr�ation des composants

Tag = [get_name(this), ' ', 'clc_edit_texte -'];
this.c_moins = set_name(this.c_moins, Tag);
this.c_moins = set_handleAnchor(this.c_moins, tab_uicontrols(1));

Tag = [get_name(this), ' ', 'clc_edit_value'];
this.c_value = set_name(this.c_value, Tag);
this.c_value = set_handleAnchor(this.c_value, tab_uicontrols(2));

Tag = [get_name(this), ' ', 'clc_edit_texte +'];
this.c_plus  = set_name(this.c_plus, Tag);
this.c_plus  = set_handleAnchor(this.c_plus, tab_uicontrols(3));

InsetX = get(this.cl_component, 'componentInsetX');
InsetY = get(this.cl_component, 'componentInsetY');
this.c_moins = set_inset_x(this.c_moins, InsetX);
this.c_moins = set_inset_y(this.c_moins, InsetY);
this.c_value = set_inset_x(this.c_value, InsetX);
this.c_value = set_inset_y(this.c_value, InsetY);
this.c_plus  = set_inset_x(this.c_plus, InsetX);
this.c_plus  = set_inset_y(this.c_plus, InsetY);

this.c_moins = editobj(this.c_moins);
this.c_value = editobj(this.c_value);
this.c_plus  = editobj(this.c_plus);

%% Positionnement des composants

this = positionner(this);

%% Mise � jour de l'accessibilit�

if strcmp(get(this, 'componentEnable'), 'off')
    this.globalFrame = set(this.globalFrame, 'Enable', 'off');
    this.c_moins     = set(this.c_moins, 'componentEnable', 'off');
    this.c_value     = set(this.c_value, 'componentEnable', 'off');
    this.c_plus      = set(this.c_plus, 'componentEnable', 'off');
end

%% Mise � jour de la visibilit�

if strcmp(get(this, 'componentVisible'), 'off')
    this.globalFrame = set(this.globalFrame, 'Visible', 'off');
    this.c_moins     = set(this.c_moins, 'componentVisible', 'off');
    this.c_value     = set(this.c_value, 'componentVisible', 'off');
    this.c_plus      = set(this.c_plus,  'componentVisible', 'off');
end
