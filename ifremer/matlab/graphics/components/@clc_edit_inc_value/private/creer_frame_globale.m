% Creation des frames principales de l'IHM d'une instance
% 
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   this : instance de clc_edit_inc_value initialisee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

ihm      = [];

%% Ancre du composant bouton 1

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = [get_name(this), ' ', 'clc_edit_texte -'];

%% Ancre du composant value

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 2:4;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = [get_name(this), ' ', 'clc_edit_value'];

%% Ancre du composant clc_edit_texte unite

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 5;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = [get_name(this), ' ', 'clc_edit_texte +'];

%% Cr�ation des frames

this.globalFrame = cl_frame('handleFrame', get(this, 'componentAnchor'), ...
    'InsetX', 5, 'InsetY', 5, 'MaxLig', 1, 'MaxCol', 5);
this.globalFrame = set(this.globalFrame, 'ListeUicontrols', ihm);
