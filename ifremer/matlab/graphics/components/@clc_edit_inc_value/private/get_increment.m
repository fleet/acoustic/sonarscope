% Retourne l'increment
% 
% Syntax
%   increment = get_increment(this)
%
% Input Arguments
%   this  : instance de clc_edit_inc_value
%
% Output Arguments
%   increment : increment
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: get_increment.m,v 1.4 2003/04/28 17:14:24 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function increment = get_increment(this)

% Maintenance Auto : try
    increment = this.increment;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_inc_value', 'get_increment', lasterr);
% Maintenance Auto : end
