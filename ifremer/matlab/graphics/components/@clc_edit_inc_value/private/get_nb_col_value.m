% Retourne le nombre de colonnes occupees par la valeur
% 
% Syntax
%   nbColValue = get_nb_col_value(this)
%
% Input Arguments
%   this  : instance de clc_edit_inc_value
%
% Output Arguments
%   nbColValue : nombre de colonnes occupees par la valeur
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: get_nb_col_value.m,v 1.4 2003/04/28 17:14:24 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbColValue = get_nb_col_value(this)

% Maintenance Auto : try
    nbColValue = this.nbColValue;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_inc_value', 'get_nb_col_value', lasterr);
% Maintenance Auto : end
