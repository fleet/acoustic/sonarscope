% Positionne les elements du composant
%
% Syntax
%   this = positionner(this)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   this : instance de clc_edit_inc_value initialisee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: positionner.m,v 1.4 2003/04/28 17:14:24 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = positionner(this)

% -------
% Locales

colMin = 0;
colMax = 0;
col    = 0;

% --------------------------------------------------------
% Lecture des handles des differents elements du composant

handles = get(this.globalFrame, 'ListeUicontrols');

% -------------------------------------
% Indique le nombre maximum de colonnes

this.globalFrame = set(this.globalFrame, ...
    'MaxCol', this.nbColValue + 2, ...
    'MaxLig', 1);

% -------------------
% Positionne le moins

colMin = colMax + 1;
colMax = colMin;
col    = colMin:colMax;
this.globalFrame = replacer(this.globalFrame, 'uicontrol', 1, 1, col);
move(this.globalFrame, handles(1), 1, col);
%drawnow
this.c_moins = update(this.c_moins);

% --------------------------------
% Positionne la frame du composant

if this.nbColValue > 0
    colMin = colMax + 1;
    colMax = colMin + this.nbColValue - 1;
    col    = colMin:colMax;
    move(this.globalFrame, handles(2), 1, col);
    %drawnow
    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 2, 1, col);
    set(handles(2), 'visible', 'on');
    this.c_value = set(this.c_value, 'componentVisible', 'on');
    this.c_value = update(this.c_value);
else
    set(handles(2), 'visible', 'off');
    this.c_value = set(this.c_value, 'componentVisible', 'off');
end

% ------------------
% Positionne le plus

colMin = colMax + 1;
colMax = colMin;
col    = colMin:colMax;
this.globalFrame = replacer(this.globalFrame, ...
    'uicontrol', 3, 1, col);
move(this.globalFrame, handles(3), 1, col);
%drawnow
this.c_plus = update(this.c_plus);
