% Initialisation de l accessibilite du composant
% 
% Syntax
%   this = set_cenable(this, enable)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%   enable   : accessibilite du composant  'on' | 'off'
%
% Output Arguments
%   this : instance de clc_edit_inc_value initialisee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_cenable.m,v 1.5 2003/04/28 17:14:24 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_cenable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    this.c_plus  = set(this.c_plus,  'componentEnable', enable);
    this.c_moins = set(this.c_moins, 'componentEnable', enable);
    this.c_value = set(this.c_value, 'componentEnable', enable);
    this.globalFrame  = set(this.globalFrame, 'Enable', enable);
    this.cl_component = set(this.cl_component, 'componentEnable', enable);
else
    my_warndlg('clc_edit_inc_value/set_cenable : Invalid value', 0);
end
