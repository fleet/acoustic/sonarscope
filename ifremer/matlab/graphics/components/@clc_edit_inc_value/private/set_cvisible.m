% Initialisation de la visibilite du composant
% 
% Syntax
%   this = set_cvisible(this, visible)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%   visible  : visibilite du composant  'on' | 'off'
%
% Output Arguments
%   this : instance de clc_edit_inc_value initialisee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_cvisible.m,v 1.5 2003/04/28 17:14:24 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_cvisible(this, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    this.c_plus  = set(this.c_plus,  'componentVisible', visible) ;
    this.c_moins = set(this.c_moins, 'componentVisible', visible);
    this.c_value = set(this.c_value, 'componentVisible', visible);
    this.globalFrame  = set(this.globalFrame, 'Visible', visible);
    this.cl_component = set(this.cl_component, 'componentVisible', visible);
else
    my_warndlg('clc_edit_inc_value/set_cvisible : Invalid value', 0);
end
