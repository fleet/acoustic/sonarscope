% Initialisation du flag indiquant la possibilite d editer la valeur
% 
% Syntax
%   this = set_editable(this, editable)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%   editable : flag indiquant la possibilite d editer la valeur
%
% Output Arguments
%    this : instance de clc_edit_inc_value initialisee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_editable.m,v 1.4 2003/04/28 17:14:24 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_editable(this, editable)

isOk = 1;

% -----------------------------
% Test validite des arguments :
% - doit etre une valeur
% - doit etre 0 ou 1

if ~isnumeric(editable)
    my_warndlg('clc_edit_inc_value/set_editable : Invalid format', 0);
    isOk = 0;
elseif (editable ~= 0) && (editable ~= 1)
    my_warndlg('clc_edit_inc_value/set_editable : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.c_value = set(this.c_value, 'editable', editable);
    if editable == 1
        this.c_plus = set(this.c_plus, 'componentEnable', 'on');
        this.c_moins = set(this.c_moins, 'componentEnable', 'on');
    else
        this.c_plus = set(this.c_plus, 'componentEnable', 'off');
        this.c_moins = set(this.c_moins, 'componentEnable', 'off');
    end
end
