% Initialisation de la valeur de l'increment
% 
% Syntax
%   this = set_increment(this, increment)
%
% Input Arguments
%   this  : instance de clc_edit_inc_value
%   increment : valeur de l increment
%
% Output Arguments
%   this  : instance de clc_edit_inc_value initialisee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_increment(this, increment)

isOk = 1;

% ---------------------------------------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - doit etre positif sinon les boutons + et - perdent leur sens

if ~isnumeric(increment)
    my_warndlg('clc_edit_inc_value/set_increment : Invalid format', 0);
    isOk = 0;
elseif increment < 0
    my_warndlg('clc_edit_inc_value/set_increment : negative value', 0);
    increment = -increment;
end

%% Si ok, initialisation

if isOk
    this.increment = increment;
else
    this.increment = 1;
end

%% Initialisation des boutons plus et moins

incTooltip = ['+ ', num2str(this.increment)];
decTooltip = ['- ', num2str(this.increment)];
this.c_plus = set(this.c_plus, 'tooltip', incTooltip);
this.c_moins = set(this.c_moins, 'tooltip', decTooltip);
