% Initialisation du nombre de colonnes occupees par la valeur
% 
% Syntax
%   this = set_nb_col_value(this, nbColValue)
%
% Input Arguments
%   this   : instance de clc_edit_inc_value
%   nbColValue : nombre de colonnes occupees par la valeur
%
% Output Arguments
%   this  : instance de clc_edit_inc_value initialisee
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_nb_col_value.m,v 1.4 2003/04/28 17:14:24 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_nb_col_value(this, nbColValue)

isOk = 1;

% --------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - doit etre un scalaire

if ~isnumeric(nbColValue)
    my_warndlg('clc_edit_inc_value/set_nb_col_value : Invalid format', 1);
    isOk = 0;
end
[m, n] = size(nbColValue);
if (m*n > 1)
    my_warndlg('clc_edit_inc_value/set_nb_col_value : Invalid value', 1);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.nbColValue = nbColValue;
else
    this.nbColValue = 1;
end

% ----------------------------------------
% Initialisation des boutons plus et moins

if is_edit(this)
    this = positionner(this);
end
