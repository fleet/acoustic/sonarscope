% Representation textuelle d'une instance de clc_edit_inc_value
%
% Syntax
%   this = traiter_edit(this) 
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   this : instance de clc_edit_inc_value initialisee
%
% Examples
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_edit(this)
    
%% Lecture a partir de l IHM et initialisation du texte

if is_edit(this)
    this.c_value = gerer_callback(this.c_value, get(this, 'msgEdit'));
end
