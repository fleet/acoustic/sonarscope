% Decremente la valeur
%
% Syntax
%   this = traiter_moins(this) 
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   this : instance de clc_edit_inc_value initialisee
%
% Examples
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_moins(this)

value = get(this, 'value') - get(this, 'increment');

minValue = get(this.c_value, 'minValue');
if value >= minValue
    this         = set(this, 'value', value);
    this.c_value = set(this.c_value, 'value', value);
end
