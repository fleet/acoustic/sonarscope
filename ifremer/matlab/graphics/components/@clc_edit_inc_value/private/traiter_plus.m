% Incremente la valeur
%
% Syntax
%   this = traiter_plus(this) 
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Output Arguments
%   this : instance de clc_edit_inc_value initialisee
%
% Examples
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_plus(this)

value = get(this, 'value') + get(this, 'increment');

maxValue = get(this.c_value, 'maxValue');
if value <= maxValue
    this         = set(this, 'value', value);
    this.c_value = set(this.c_value, 'value', value);
end
