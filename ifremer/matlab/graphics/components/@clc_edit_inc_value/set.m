% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : instance de clc_edit_inc_value
%
% Name-Value Pair Arguments
%   cf. clc_edit_inc_value
%
% Output Arguments
%   b : instance de clc_edit_inc_value initialisee
%
% Examples
%    v = clc_edit_inc_value;
%    set(v, 'value', 3.14, 'tooltip', 'PI')
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

%% Lecture des arguments

[varargin, cvisible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, cenable] = getPropertyValue(varargin, 'componentEnable', []);

[varargin, increment] = getPropertyValue(varargin, 'increment', []);
[varargin, value] = getPropertyValue(varargin, 'value', []);
[varargin, format] = getPropertyValue(varargin, 'format', []);
[varargin, minValue] = getPropertyValue(varargin, 'minValue', []);
[varargin, maxValue] = getPropertyValue(varargin, 'maxValue', []);
[varargin, nbColValue] = getPropertyValue(varargin, 'nbColValue', []);
[varargin, tag] = getPropertyValue(varargin, 'tag', []);
[varargin, tooltip] = getPropertyValue(varargin, 'tooltip', []);
[varargin, editable] = getPropertyValue(varargin, 'editable', []);
[varargin, msgEdit] = getPropertyValue(varargin, 'msgEdit', []);
[varargin, msgPlus] = getPropertyValue(varargin, 'msgPlus', []);
[varargin, msgMoins] = getPropertyValue(varargin, 'msgMoins', []);

%% Initialisation des attributs

if ~isempty(cvisible)
    this = set_cvisible(this, cvisible);
end

if ~isempty(cenable)
    this = set_cenable(this, cenable);
end

if ~isempty(increment)
    this = set_increment(this, increment);
end

if ~isempty(value)
    this.c_value = set(this.c_value, 'value', value);
end

if ~isempty(format)
    this.c_value = set(this.c_value, 'format', format);
end

if ~isempty(minValue)
    this.c_value = set_minValue(this.c_value, minValue);
end

if ~isempty(maxValue)
    this.c_value = set_maxValue(this.c_value, maxValue);
end

if ~isempty(nbColValue)
    this = set_nb_col_value(this, nbColValue);
end

if ~isempty(tag)
    this.c_value = set(this.c_value, 'tag', tag);
    % la fonction existe mais il faut v�rifier si �a fait la m�me chose sue set(...
    % this.c_value = set_tag(this.c_value, tag);
end

if ~isempty(tooltip)
    this.c_value = set(this.c_value, 'tooltip', tooltip);
    %this.c_value = set_tooltip(this.c_value, tooltip);
end

if ~isempty(editable)
    this = set_editable(this, editable);
end

if ~isempty(msgEdit)
    this.c_value = set(this.c_value, 'msgEdit', msgEdit);
    %this.c_value = set_msgEdit(this.c_value, msgEdit);
end

if ~isempty(msgPlus)
    this.c_plus = set(this.c_plus, 'msgAction', msgPlus);
    % Attention : un peu diff�rent : msgAction
    %this.c_plus = set_msgPlusthis.c_plus, msgPlus);
end

if ~isempty(msgMoins)
    this.c_moins = set(this.c_moins, 'msgAction', msgMoins);
    % Attention : un peu diff�rent : msgAction
    %this.c_moins = set_msgMoins(this.c_moins, 'msgMoins);
end

%% Traitement des message de la super classe

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

%% Traitement specifique des composants agreges

if ~is_edit(this)
    this.c_value = set(this.c_value, ...
        'componentName', [get_name(this),' ','clc_edit_value'], ...
        'componentUserName', get_user_name(this), ...
        'componentUserCb', get_user_cb(this));
    this.c_moins = set(this.c_moins, ...
        'componentName', [get_name(this),' ','clc_edit_texte -'], ...
        'componentUserName', get_user_name(this), ...
        'componentUserCb', get_user_cb(this));
    this.c_plus = set(this.c_plus, ...
        'componentName', [get_name(this),' ','clc_edit_texte +'], ...
        'componentUserName', get_user_name(this), ...
        'componentUserCb', get_user_cb(this));
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
