% Passage direct d'une valeur au composant
%
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Examples
%   this = set_increment(this, value)
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_increment(this, value)
this.increment = value;
