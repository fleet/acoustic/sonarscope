% Passage direct d'une valeur au composant
%
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%   this : instance de clc_edit_inc_value
%
% Examples
%   this = set_value(this, value)
%
% See also clc_edit_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_value(this, value)
this.c_value = set_value(this.c_value, value);