% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char(a) 
%
% Input Arguments
%    a : instance ou tableau d'instances de clc_edit_texte
%
% Output Arguments
%    str : chaine de caracteres
%
% Examples
%    char(clc_edit_texte)
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.6 2003/04/07 14:23:50 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    str = [];
    
    [m, n] = size(this);
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'char', lasterr);
% Maintenance Auto : end
