% Constructeur du composant clc_edit_texte (Zone d'affichage de texte)
%
% Syntax
%   a = clc_edit_texte(...)
% 
% Name-Value Pair Arguments
%   texte             : contenu texte (chaine de caracteres)
%   tag               : tag du label d affichage du texte
%   tooltip           : info-bulle du label d affichage du texte
%   editable          : flag indiquant si le texte est editable
%   actionnable       : flag indiquant si le texte est actionnable (click)
%   typeButton        : flag indiquant le type de bouton
%                        - 0 : pushbutton
%                        - 1 : togglebutton
%   msgEdit           : msg de call back en cas d edition du texte
%   msgAction         : msg de call back en cas de click du texte
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments 
%   a          : instance de clc_edit_texte
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    v = clc_edit_texte
%
% See also clc_edit_texte cla_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_edit_texte(varargin)

%% D�finition de la structure et initialisation
%
% globalFrame : instance de cl_frame, frame globale du composant
% texte       : contenu texte
% tag         : tag du label d affichage du texte
% tooltip     : tooltip du label d affichage du texte
% editable    : flag indiquant si la valeur est editable
% actionnable : flag indiquant si le texte est actionnable
% typeButton  : flag indiquant le type de bouton
%               - 0 : pushbutton
%               - 1 : togglebutton, etat relache
%               - 2 : togglebutton, etat enfonce
% msgEdit     : msg de call back en cas d edition du texte
% msgAction   : msg de call back en cas de click du texte

this.globalFrame  = [];
this.texte        = '';
this.tag          = [];%'defaultTag_clc_edit_texte';
this.UserData     = [];%'defaultTag_clc_edit_texte';
this.tooltip      = '';
this.actionnable  = 0;
this.editable     = 0;
this.typeButton   = 0;
this.msgEdit      = 'clc_edit_texte edit';
this.msgAction    = 'clc_edit_texte action';
this.handleAnchor = [];   % Internal property

%% Super-classe

% if (nargin == 1) && isempty(varargin{1})
%     composant = cl_component([]);
% else
    composant = cl_component([]);
% end

%% Cr�ation de l'objet

this = class(this, 'clc_edit_texte', composant);

%% Pr�-initialisation des champs

% if (nargin == 0)
%     return
% elseif (nargin == 1) && isempty(varargin{1})
%     return
% end
% Modif JMA le 02/10/2015
if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
