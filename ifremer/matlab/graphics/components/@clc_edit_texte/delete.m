% Destruction graphique du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte
%
% Remarks : doit etre surchargee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.6 2003/04/14 17:05:30 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % ----------------------------------------
    % Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    % -----------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(this.globalFrame)
        this.globalFrame = delete(this.globalFrame);
        this.globalFrame = [];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'delete', lasterr);
% Maintenance Auto : end
