% Interface-Homme-Machine (IHM) d'une instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% OUPUT PARAMETERS :
%   this : instance de clc_edit_texte
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Cr�ation de l'interface graphique

if ~is_edit(this)
    this.cl_component = editobj(this.cl_component, 'handleAnchor', this.handleAnchor);
    this = construire_ihm(this);
end
