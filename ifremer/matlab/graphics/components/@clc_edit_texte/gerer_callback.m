% Gestion des callbacks d'une IHM d'une instance
%
% Syntax
%   this = gerer_callback(this, message)
% 
% Input Arguments
%   this    : instance de clc_edit_texte
%   message : message d'identification de l'action
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% Remarks : doit etre surchargee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: gerer_callback.m,v 1.4 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

% --------------------
% Selection du message

if length(varargin) == 1
    switch varargin{1}
        case get(this, 'msgEdit')
            this = traiter_edit(this);
            
        case get(this, 'msgAction')
            this = traiter_action(this);
        otherwise
            my_warndlg('clc_edit_texte/gerer_callback : Invalid message', 0);
    end
else
    my_warndlg('clc_edit_texte/gerer_callback : Invalid arguments number', 0);
end
