% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de clc_edit_texte
%
% Name-only Arguments
%   cf. clc_edit_texte
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    v = clc_edit_texte; 
%    set(v, 'texte', 'un texte possible', 'tooltip', 'Texte');
%    get(v, 'tooltip')
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

%% Boucle de parcours de l ensemble des proprietes donnees en arguments

for k=1:length(varargin)
    switch varargin{k}
        case 'texte'
            varargout{end+1} = get_texte(this); %#ok<AGROW>
        case 'tooltip'
            varargout{end+1} = get_tooltip(this); %#ok<AGROW>
        case 'tag'
            varargout{end+1} = get_tag(this); %#ok<AGROW>
        case 'handle'
            varargout{end+1} = get_handle(this); %#ok<AGROW>
        case 'editable'
            varargout{end+1} = get_editable(this); %#ok<AGROW>
        case 'actionnable'
            varargout{end+1} = get_actionnable(this); %#ok<AGROW>
        case 'typeButton'
            varargout{end+1} = get_type_button(this); %#ok<AGROW>
        case 'msgEdit'
            varargout{end+1} = get_msg_edit(this); %#ok<AGROW>
        case 'msgAction'
            varargout{end+1} = get_msg_action(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.cl_component, varargin{k}); %#ok<AGROW>
    end
end
