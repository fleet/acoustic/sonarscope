% Retourne le flag indiquant si la valeur est cliquable
% 
% Syntax
%   actionnable = get_actionnable(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   actionnable : flag indiquant si la valeur est actionnable
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_actionnable.m,v 1.4 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function actionnable = get_actionnable(this)

% Maintenance Auto : try
    actionnable = this.actionnable;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_edit_texte', 'get_actionnable', lasterr);
% Maintenance Auto : end
