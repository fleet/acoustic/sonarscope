% Retourne le handle de l afficheur de la valeur
% 
% Syntax
%   handle = get_handle(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   handle   : handle de l afficheur de la valeur, [] si non represente
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_handle.m,v 1.5 2003/04/07 14:23:50 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function handle = get_handle(this)

% Maintenance Auto : try
    handle = get(this.globalFrame, 'ListeUicontrols');
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_handle', lasterr);
% Maintenance Auto : end
