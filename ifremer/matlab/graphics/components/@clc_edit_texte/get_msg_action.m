% Retourne le message envoye lors du click sur le texte
% 
% Syntax
%   msgAction = get_msg_action(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   msgAction : message envoye lors du click sur le texte
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function msgAction = get_msg_action(this)

msgAction = this.msgAction;
