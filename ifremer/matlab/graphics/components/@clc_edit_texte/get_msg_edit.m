% Retourne le message envoye lors de l edition de la valeur
%
% Syntax
%   msgEdit = get_msg_edit(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   msgEdit : message envoye lors de l edition de la valeur
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function msgEdit = get_msg_edit(this)

msgEdit = this.msgEdit;
