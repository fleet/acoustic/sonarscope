% Retourne le tag
% 
% Syntax
%   tag = get_tag(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   tag      : tag
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_tag.m,v 1.4 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tag = get_tag(this)

% Maintenance Auto : try
    tag = this.tag;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_tag', lasterr);
% Maintenance Auto : end
