% Retourne la valeur affichee dans la zone de texte
% 
% Syntax
%   texte = get_texte(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   texte    : texte
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_texte.m,v 1.5 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function texte = get_texte(this)

% Maintenance Auto : try
    texte = this.texte;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_texte', lasterr);
% Maintenance Auto : end
