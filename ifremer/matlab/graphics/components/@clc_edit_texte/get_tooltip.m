% Retourne l'info-bulle
% 
% Syntax
%   tooltip = get_tooltip(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   tooltip  : info-bulle
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_tooltip.m,v 1.5 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tooltip = get_tooltip(this)

% Maintenance Auto : try
    tooltip = this.tooltip;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_tooltip', lasterr);
% Maintenance Auto : end
