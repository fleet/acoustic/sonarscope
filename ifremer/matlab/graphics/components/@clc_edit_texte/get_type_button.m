% Retourne le tag
% 
% Syntax
%   typeButton = get_type_button(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   typeButton : type de bouton, 0 : pushbutton, 1 : togglebutton
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_type_button.m,v 1.4 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function typeButton = get_type_button(this)

% Maintenance Auto : try
    typeButton = this.typeButton;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_type_button', lasterr);
% Maintenance Auto : end
