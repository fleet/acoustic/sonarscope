% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.6 2003/04/07 14:15:01 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   08/02/2001 - DCF - creation
%   08/03/2001 - DCF - ajout choix entre pushbutton et togglebutton
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try

    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('texte       <-> %s', mat2str(this.texte));
    str{end+1} = sprintf('tag         <-> %s', mat2str(this.tag));
    str{end+1} = sprintf('tooltip     <-> %s', mat2str(this.tooltip));
    str{end+1} = sprintf('editable    <-> %s', mat2str(this.editable));
    str{end+1} = sprintf('actionnable <-> %s', mat2str(this.actionnable));
    str{end+1} = sprintf('typeButton  <-> %s', mat2str(this.typeButton));
    str{end+1} = sprintf('msgEdit     <-> %s', mat2str(this.msgEdit));
    str{end+1} = sprintf('msgAction   <-> %s', mat2str(this.msgAction));
    
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);
    
    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);

% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'char_instance', lasterr);
% Maintenance Auto : end
