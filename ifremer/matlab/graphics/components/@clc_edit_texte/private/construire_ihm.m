% Creation de l'IHM d'une instance
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte
%
% See also clc_edit_texte
% Authors : DCF
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation de la frame du composant

this = creer_frame_globale(this);

%% Mise � jour de l'accessibilit�

if strcmp(get(this, 'componentEnable'), 'off')
    this.globalFrame = set(this.globalFrame, 'Enable', 'off');
end

%% Mise � jour de la visibilit�

if strcmp(get(this, 'componentVisible'), 'off')
    this.globalFrame = set(this.globalFrame, 'Visible', 'off');
end
