% Création des frames principales de l'IHM d'une instance
%
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

%% Définition générale

ihm{1}.Lig           = 1;
ihm{1}.Col           = 1;
ihm{1}.TooltipString = this.tooltip;
ihm{1}.Tag           = [get(this,'componentParent') '/clc_edit_texte'];
if ~isempty(get(this, 'componentParent'))
    ihm{1}.Parent    = get(this,'componentParent');
end
ihm{1}.String        = this.texte;
ihm{1}.UserData      = this.UserData;

%% Choix selon que le texte est editable ou non

if this.editable
    ihm{1}.Style           = 'edit';
    ihm{1}.BackgroundColor = GrisClair;
    ihm{1}.Callback        = built_cb(this, this.msgEdit);
elseif this.actionnable
    if this.typeButton == 0
        ihm{1}.Style        = 'pushbutton';
    elseif this.typeButton == 1
        ihm{1}.Style        = 'togglebutton';
        ihm{1}.Value        = 0;
    else
        ihm{1}.Style        = 'togglebutton';
        ihm{1}.Value        = 1;
    end
    ihm{1}.Callback = built_cb(this, this.msgAction);
else
    ihm{1}.Style            = 'text';
    ihm{1}.BackgroundColor  = Gris;
    ihm{1}.Callback         = [];
end

%% Création des frames

this.globalFrame = cl_frame([]);
this.globalFrame = set_max_lig(       this.globalFrame, 1);
this.globalFrame = set_max_col(       this.globalFrame, 1);

%this.globalFrame = set(this.globalFrame,'MaxLig',6);
this.globalFrame = set_inset_x(       this.globalFrame, get_inset_x(this));
this.globalFrame = set_inset_y(       this.globalFrame, get_inset_y(this));
this.globalFrame = set_handle_frame(  this.globalFrame, get_anchor(this));
this.globalFrame = set_tab_uicontrols(this.globalFrame, ihm);
