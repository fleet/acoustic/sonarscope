% Traitement apres un click sur le texte
%
% Syntax
%   this = traiter_action(this) 
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------


function this = traiter_action(this)

% --------------------------------------
% Mise a jour de l etat du toggle button

if this.typeButton == 1
    this.typeButton = 2;
elseif this.typeButton == 2
    this.typeButton = 1;
end
    