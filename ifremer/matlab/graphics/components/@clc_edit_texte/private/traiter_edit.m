% Initialisation du texte
%
% Syntax
%   this = traiter_edit(this) 
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_edit(this)

% ----------------------------------------------------
% Lecture a partir de l IHM et initialisation du texte

if is_edit(this)
    h = get_handle(this);
    this = set_texte(this, get(h, 'String'));
end
