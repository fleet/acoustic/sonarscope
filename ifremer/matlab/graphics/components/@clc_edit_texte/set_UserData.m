% Initialisation du UserData
% 
% Syntax
%   this = set_UserData(this, UserData)
%
% Input Arguments
%   this : instance de clc_edit_texte
%   UserData  : UserData
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_UserData(this, UserData)

this.UserData = UserData;

%% Si affichage en cours, mise � jour de l'affichage

if is_edit(this)
    h = get(this, 'handle');
    set(h, 'UserData', this.UserData);
end
