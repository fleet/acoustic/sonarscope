% Initialisation de l'accessibilite du composant
% 
% Syntax
%   this = set_enable(this, enable)
%
% Input Arguments
%   this   : instance de clc_edit_texte
%   enable : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.7 2003/04/07 14:28:11 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    if is_edit(this)
        this.globalFrame  = set(this.globalFrame, 'Enable', enable);
    end
    this.cl_component = set_enable(this.cl_component, enable);
else
    my_warndlg('clc_edit_texte/set_enable : Invalid value', 0);
end
