% Initialisation du message envoye en cas de click du texte
% 
% Syntax
%   this = set_msg_action(this, msgAction)
%
% Input Arguments
%   this      : instance de clc_edit_texte
%   msgAction : message envoye en cas de click du texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_msg_action.m,v 1.5 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_msg_action(this, msgAction)

this.msgAction = msgAction;
    
% -------------------------------------------------
% Si affichage en cours, mise a jour de l affichage

if is_edit(this)
    h = get(this, 'handle');
    if this.actionnable
        set(h, 'Callback', built_cb(this, this.msgAction));
    else
        set(h, 'Callback', []);
    end
end
