% Initialisation du message envoye en cas d edition de la valeur
% 
% Syntax
%   this = set_msg_edit(this, msgEdit)
%
% Input Arguments
%   this    : instance de clc_edit_texte
%   msgEdit : message envoye en cas d edition de la valeur
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_msg_edit.m,v 1.5 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_msg_edit(this, msgEdit)

this.msgEdit = msgEdit;

% -------------------------------------------------
% Si affichage en cours, mise a jour de l affichage

if is_edit(this)
    h = get(this, 'handle');
    if this.editable
        set(h, 'Callback', built_cb(this, this.msgEdit));
    else
        set(h, 'Callback', []);
    end
end
