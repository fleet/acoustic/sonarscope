% Initialisation du tag
% 
% Syntax
%   this = set_tag(this, tag)
%
% Input Arguments
%   this : instance de clc_edit_texte
%   tag  : tag
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_tag(this, tag)

isOk = 1;

%% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(tag)
    my_warndlg('clc_edit_texte/set_tag : Invalid format', 0);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.tag = tag;
else
    this.tag = [];
end

%% Si affichage en cours, mise � jour de l'affichage

if is_edit(this)
    h = get(this, 'handle');
    set(h, 'Tag', this.tag);
end
