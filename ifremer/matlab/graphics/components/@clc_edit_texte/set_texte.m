% Initialisation de la valeur de l'instance
% 
% Syntax
%   this = set_texte(this, texte)
%
% Input Arguments
%   this  : instance de clc_edit_texte
%   texte : texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_texte(this, texte)

isOk = 1;

%% Test validite des arguments :
% - doit etre une chaine de caracteres
% - doit etre dans l intervalle des valeurs admissibles

if ~isempty(texte) && ~ischar(texte)
    my_warndlg('clc_edit_texte/set_texte : Invalid format', 0);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.texte = texte;
else
    this.texte = '';
end

%% Si affichage en cours, mise a jour de l affichage

if is_edit(this)
    h = get(this, 'handle');
    if ishandle(h)
        set(h, 'String', this.texte);
        set(h, 'Enable', 'on');
    end
end
