% Initialisation du tag
% 
% Syntax
%   this = set_type_button(this, typeButton)
%
% Input Arguments
%   this   : instance de clc_edit_texte
%   typeButton : type de bouton, 0 : pushbutton, 1 : toggle button relache, 
%                 2 : toggle button enfonce
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_type_button.m,v 1.4 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_type_button(this, typeButton)

isOk = 1;

% -------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~isnumeric(typeButton)
    my_warndlg('clc_edit_texte/set_type_button : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.typeButton = typeButton;
else
    this.typeButton = 0;
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l affichage

if is_edit(this)
    this = set_actionnable(this, this.actionnable);
end
