% Representation externe d'une instance dans une chaine de caracteres
%
% str = char(this) 
%
% Input Arguments
%    this : instance ou tableau d instance de clc_edit_value
%
% Output Arguments
%    str : une chaine de caracteres representative
%
% Examples
%    char(clc_edit_value)
%
% See also help clc_edit_value
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_value', 'char', lasterr);
% Maintenance Auto : end
  
