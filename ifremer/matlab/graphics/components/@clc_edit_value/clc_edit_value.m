% Constructeur du composant clc_edit_value (zone d'edition de valeurs)
%
% a = clc_edit_value(...)
% 
% Input Arguments 
%    varargin : propertyName / propertyValue
%
% Name-Value Pair Arguments
%    value             : valeur reelle
%    format            : format d affichage de la valeur
%    minValue          : valeur minimale admissible
%    maxValue          : valeur maximale admissible
%    tag               : tag du label d affichage de la valeur
%    tooltip           : info-bulle du label d affichage de la valeur
%    editable          : flag indiquant si la valeur est editable
%    msgEdit           : msg de call back en cas d edition de la valeur
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant    'on' | 'off'
%    componentEnable   : accessibilite du composant 'on' | 'off'
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments 
%    a : instance de clc_edit_value
%
% Remarks :
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    v = clc_edit_value
%
% See also clc_edit_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_edit_value(varargin)

%% D�finition de la structure et initialisation

this.value    = [];
this.format   = '%f';
this.minValue = [];
this.maxValue = [];
this.tag      = 'defaultTag_clc_editValue';

%% Super-classe

if (nargin == 1) && isempty(varargin {1})
    clc_edit_texte([]);
else
    clc_edit_texte;
end

%% Cr�ation de l'objet

this = class(this, 'clc_edit_value', clc_edit_texte);

%% Initialisation de la super classe

if nargin == 0
    return
elseif (nargin == 1) && isempty(varargin {1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this.clc_edit_texte = set(this.clc_edit_texte, ...
    'msgEdit',      'clc_edit_value edit', ...
    'actionnable',  0, ...
    'msgAction',    []);

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
