% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de clc_edit_value
%
% Name-only Arguments
%    cf. clc_edit_value
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    v = clc_edit_value; 
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also help clc_edit_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'value'
            varargout{end+1} = get_value(this); %#ok<AGROW>
        case 'format'
            varargout{end+1} = get_format(this); %#ok<AGROW>
        case 'minValue'
            varargout{end+1} = get_min_value(this); %#ok<AGROW>
        case 'maxValue'
            varargout{end+1} = get_max_value(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.clc_edit_texte, varargin{i}); %#ok<AGROW>
    end
end
