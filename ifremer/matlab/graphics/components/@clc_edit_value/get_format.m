% Retourne le format d affichage de la valeur
% 
% Syntax
%   format = get_format(this)
%
% Input Arguments
%    his : instance de clc_edit_value
%
% Output Arguments
%   format : format d affichage de la valeur reelle
%
% See also help clc_edit_value Authors
% Authors : DCF
% VERSION  : $Id: get_format.m,v 1.3 2003/04/04 14:22:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function format = get_format(this)

% Maintenance Auto : try
    format = this.format;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_edit_value', 'get_format', lasterr);
% Maintenance Auto : end
