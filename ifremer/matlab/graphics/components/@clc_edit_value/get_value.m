% Retourne la valeur reelle
% 
% Syntax
%   value = get_value(this)
%
% Input Arguments
%    this : instance de clc_edit_value
%
% Output Arguments
%    value : valeur reelle
%
% See also help clc_edit_value Authors
% Authors : DCF
% VERSION  : $Id: get_value.m,v 1.3 2003/04/04 14:22:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function value = get_value(this)

% Maintenance Auto : try
    value = this.value;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_value', 'get_value', lasterr);
% Maintenance Auto : end
