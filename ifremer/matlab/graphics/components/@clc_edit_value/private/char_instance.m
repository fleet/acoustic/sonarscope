% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%    a : instance de clc_edit_value
%
% Output Arguments
%    str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also help clc_edit_value
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    07/02/2001 - DCF - creation
%    08/02/2001 - DCF - heritage a partir de clc_edit_texte
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('value    <-> %f', this.value);
    str{end+1} = sprintf('format   <-> %s', this.format);
    str{end+1} = sprintf('minValue <-> %f', this.minValue);
    str{end+1} = sprintf('maxValue <-> %f', this.maxValue);
    
    str{end+1} = sprintf('--- heritage clc_edit_texte ---');
    str{end+1} = char(this.clc_edit_texte);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :   err ('clc_edit_value', 'char_instance', lasterr);
% Maintenance Auto : end
