% Traitement apres modification de la valeur
%
% Syntax
%   this = traiter_edit(this) 
%
% Input Arguments
%   this : instance de clc_edit_value
%
% Output Arguments
%   this : instance de clc_edit_value initialisee
%
% Examples
%
% See also clc_edit_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_edit(this)

%% Appel méthode super class

this.clc_edit_texte = gerer_callback(this.clc_edit_texte, get(this, 'msgEdit'));

%% Initialisation de la valeur

format long
str = get_texte(this);
x = str2num(str); %#ok<ST2NM>

if isempty(x)
    x = str2lat(str);
    if isnan(x)
        x = str2lon(str);
    end
end

% x = str2double(get_texte(this)); % Modif JMA le 29/01/2014 pour centrage carte BICOSE
if ~isempty(x)
    if x < this.minValue
        x = this.minValue;
    end
    if x > this.maxValue
        x = this.maxValue;
    end
    this = set_value(this, x);
else
    my_warndlg('clc_edit_value/traiter_edit : La valeur n''est pas interprétable', 1);
end

