% Initialisation du format d'affichage de la valeur
%
% Syntax
%   this = set_format(this, format)
%
% Input Arguments
%    this : instance de clc_edit_value
%    format   : format d affichage de la valeur
%
% Output Arguments
%    this : instance de clc_edit_value initialisee
%
% See also clc_edit_value
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_format(this, format)

this.format = format;

%% Si affichage en cours, mise a jour de l'affichage

this = set_value(this, this.value);
