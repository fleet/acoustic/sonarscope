% Initialisation de la valeur maximale
% 
% b = set_max_value(a, maxValue)
%
% Input Arguments
%    a : instance de clc_edit_value
%    maxValue : valeur maximale
%
% Output Arguments
%    b : instance de clc_edit_value initialisee
%
% See also help clc_edit_value
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function instance = set_max_value(instance, maxValue)

isOk = 1;

% -----------------------------
% Test validite des arguments :
% - doit etre un reel

if ~isnumeric(maxValue)
    my_warndlg('clc_edit_value/set_max_value : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    instance.maxValue = maxValue;
else
    instance.maxValue = [];
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l'affichage

instance = set_value(instance, instance.value);
