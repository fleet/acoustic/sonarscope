% Initialisation de la valeur minimale
%
% this = set_min_value(this, minValue)
%
% Input Arguments
%    this     : instance de clc_edit_value
%    minValue : valeur minimale
%
% Output Arguments
%    this : instance de clc_edit_value initialisee
%
% See also clc_edit_value
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_min_value(this, minValue)

this.minValue = minValue;

%% Si affichage en cours, mise a jour de l'affichage

this = set_value(this, this.value);
