% Test graphique du composant
%
% Syntax
%   this = test(this)
% 
% Input Arguments
%   this : instance de clc_edit_value
%
% Output Arguments
%   this : instance de clc_edit_value
% 
% Examples
%    a = test(clc_edit_value)
%
% See also help clc_edit_value Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/04/04 14:22:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this)

% Maintenance Auto : try
    
    % -------------------------------------------------------
    % Creation d une figure ainsi que de l ancre du composant
    
    figure('Visible', 'off');
    h = uicontrol(  'Visible'  , 'on', ...
                    'Tag'      , 'ancre_clc_edit_value', ...
                    'Position' , [20 20 100 30]);
    
    % ----------------------------------------
    % Creation et initialisation du composant
    
    this = clc_edit_value([]);
    this = set_value(this,      3.14);
    this = set_format(this,     '%f');
    this = set_min_value(this,  3);
    this = set_max_value(this,  4);
    this = set_tag(this,        'Pi');
    this = set_tooltip(this,    'Pi');
    this = set_name(this,       'ancre_clc_edit_value');
    this = set_user_name(this,  'clc_edit_value');
    this = set_user_cb(this,    'gerer_callback');
    
    % -------------------------------
    % Creation graphique du composant
    
    this = editobj(this);
    
    % ------------------------
    % Rend le resultat visible
    
    set(gcf, 'Visible', 'on');
    set(h, 'Visible', 'off');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_value', 'test', lasterr);
% Maintenance Auto : end
