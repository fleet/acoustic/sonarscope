% Transformation en chaine de caracteres d un tableau d'instances
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance ou tableau d'instances de clc_entete
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%   char(clc_entete)
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.3 2003/03/28 13:48:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    % -------------------------
    % Intialisation des locales
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_entete', 'char', lasterr);
% Maintenance Auto : end
