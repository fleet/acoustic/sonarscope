% Constructeur de clc_entete, classe composant
%
% Syntax
%   this = clc_entete(...)
%
% Name-Value Pair Arguments
%   titre             : titre de l'entete
%   msgIfremer        : message cb du bouton ifremer
%   msgService        : message cb du bouton service
%   msgTitre          : message cb du bouton titre
%   msgCopyright      : message cb du bouton copyright
%   tagIfremer        : tag cb du bouton ifremer
%   tagService        : tag cb du bouton service
%   tagTitre          : tag cb du bouton titre
%   tagCopyright      : tag cb du bouton copyright
%   NomFicHelp        : Nom du fichier d'aide associ� au bouton Titre
%                       Ce fichier doit �tre dans le repertoire IfrTbxDocumentation
%
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments
%   this : instance de clc_entete
%
% Remarks :
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    entete = clc_entete
%
% See also clc_entete Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_entete(varargin)

%% D�finition de la structure et initialisation

this.titre              = '';
this.TitreTooltipString = '';
this.globalFrame        = [];
this.msgIfremer         = 'Msg clc_entete Ifremer';
this.msgService         = 'Msg clc_entete Service';
this.msgTitre           = 'Msg clc_entete Titre';
this.msgCopyright       = 'Msg clc_entete Copyright';
this.tagIfremer         = this.msgIfremer;
this.tagService         = this.msgService;
this.tagTitre           = this.msgTitre;
this.tagCopyright       = this.msgCopyright;

this.NomFicHelp         = 'SonarScope.html';
this.handleAnchor       = [];   % Internal property

%% Super-classe

composant = cl_component([]);

%% Cr�ation de l'objet

this = class(this, 'clc_entete', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
