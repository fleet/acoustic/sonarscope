% M�thode de gestion des callbacks du composant
%
% Syntax
%   this = gerer_callback(this, cbName, ...)
% 
% Input Arguments
%   this   : instance de cl_component
%   cbName : nom de la callback
%
% Output Arguments
%   this : instance de cl_component initialis�e
%
% Remarks : doit �tre surcharg�e
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

global SonarScopeDocumentation %#ok<GVMIS>
global IfrTbxResources %#ok<GVMIS>
global IfrTbxLang %#ok<GVMIS>

switch varargin{1}
    case get_msg_ifremer(this)
        switch IfrTbxLang
            case 'US'
                my_web('https://wwz.ifremer.fr/en/');
            otherwise
                my_web('https://wwz.ifremer.fr/');
        end
        
    case get_msg_service(this)
        switch IfrTbxLang
            case 'US'
                my_web('https://www.flotteoceanographique.fr/en');
            otherwise
                my_web('https://www.flotteoceanographique.fr/');
        end
        
    case get_msg_copyright(this)
        if isempty(SonarScopeDocumentation)
            nomFic = fullfile(IfrTbxResources, 'Resources.ini');
            %verifNameLength(nomFic)
            if exist(nomFic, 'file')
                loadVariablesEnv;
            end
        end
        nomHtml = getNomFicDatabase('Copyright.html');
        cmd = ['file://' nomHtml];
        my_web(cmd);
end
