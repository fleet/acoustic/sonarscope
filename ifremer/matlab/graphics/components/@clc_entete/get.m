% Lecture des attributs de l'instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de clc_entete
%
% Name-Value Pair Arguments
%   titre             : titre de l entete
%   msgIfremer        : message cb du bouton Ifremer
%   msgService        : message cb du bouton Service
%   msgTitre          : message cb du bouton Titre
%   msgCopyright      : message cb du bouton Copyright
%   tagIfremer        : tag du bouton Ifremer
%   tagService        : tag du bouton Service
%   tagTitre          : tag du bouton Titre
%   tagCopyright      : tag du bouton Copyright
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant
%   componentEnable   : accessibilite du composant
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    c = clc_entete('tagIfremer', 'IFREMER') ;
%    get(c, 'tagIfremer')
%
% See also clc_entete Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'titre'
            varargout{end+1} = this.titre; %#ok<AGROW>
        case 'TitreTooltipString'
            varargout{end+1} = this.TitreTooltipString; %#ok<AGROW>
        case 'msgIfremer'
            varargout{end+1} = this.msgIfremer; %#ok<AGROW>
        case 'msgService'
            varargout{end+1} = this.msgService; %#ok<AGROW>
        case 'msgTitre'
            varargout{end+1} = this.msgTitre; %#ok<AGROW>
        case 'msgCopyright'
            varargout{end+1} = this.tagCopyright; %#ok<AGROW>
        case 'tagIfremer'
            varargout{end+1} = this.tagIfremer; %#ok<AGROW>
        case 'tagService'
            varargout{end+1} = this.tagService; %#ok<AGROW>
        case 'tagTitre'
            varargout{end+1} = this.tagTitre; %#ok<AGROW>
        case 'tagCopyright'
            varargout{end+1} = this.tagCopyright; %#ok<AGROW>
        case 'NomFicHelp'
            varargout{end+1} = this.NomFicHelp; %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.cl_component, varargin{i}); %#ok<AGROW>
    end
end
