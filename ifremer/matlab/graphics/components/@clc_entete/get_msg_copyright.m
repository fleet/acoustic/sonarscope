% Retourne le message de la call back pour Copyright
% 
% Syntax
%   msgCopyright = get_msg_Copyright(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   msgCopyright  : message de la call back pour Copyright
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_msg_copyright.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgCopyright = get_msg_copyright(this)

% Maintenance Auto : try
    msgCopyright = this.msgCopyright;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_msg_copyright', lasterr);
% Maintenance Auto : end
