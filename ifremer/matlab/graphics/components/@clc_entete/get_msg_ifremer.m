% Retourne le message de la call back pour Ifremer
% 
% Syntax
%   msgIfremer = get_msg_Ifremer(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   msgIfremer  : message de la call back pour Ifremer
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_msg_ifremer.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgIfremer = get_msg_ifremer(this)

% Maintenance Auto : try
    msgIfremer = this.msgIfremer;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_msg_ifremer', lasterr);
% Maintenance Auto : end
