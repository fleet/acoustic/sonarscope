% Retourne le message de la call back pour Service
% 
% Syntax
%   msgService = get_msg_Service(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   msgService  : message de la call back pour Service
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_msg_service.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgService = get_msg_service(this)

% Maintenance Auto : try
    msgService = this.msgService;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_msg_service', lasterr);
% Maintenance Auto : end
