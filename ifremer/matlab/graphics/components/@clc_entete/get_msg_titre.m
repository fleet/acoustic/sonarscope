% Retourne le message de la call back pour Titre
% 
% Syntax
%   msgTitre = get_msg_Titre(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   msgTitre  : message de la call back pour Titre
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_msg_titre.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgTitre = get_msg_titre(this)

% Maintenance Auto : try
    msgTitre = this.msgTitre;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_msg_titre', lasterr);
% Maintenance Auto : end
