% Retourne le tag du bouton Copyright
% 
% Syntax
%   tagCopyright = get_tag_Copyright(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   tagCopyright  : tag du bouton Copyright
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_tag_copyright.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagCopyright = get_tag_copyright(this)

% Maintenance Auto : try
    tagCopyright = this.tagCopyright;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_tag_copyright', lasterr);
% Maintenance Auto : end
