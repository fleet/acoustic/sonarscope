% Retourne le tag du bouton Ifremer
% 
% Syntax
%   tagIfremer = get_tag_Ifremer(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   tagIfremer  : tag du bouton Ifremer
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_tag_ifremer.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagIfremer = get_tag_ifremer(this)

% Maintenance Auto : try
    tagIfremer = this.tagIfremer;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_tag_ifremer', lasterr);
% Maintenance Auto : end
