% Retourne le tag du bouton Service
% 
% Syntax
%   tagService = get_tag_Service(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   tagService  : tag du bouton Service
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_tag_service.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagService = get_tag_service(this)

% Maintenance Auto : try
    tagService = this.tagService;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_tag_service', lasterr);
% Maintenance Auto : end
