% Retourne le tag du bouton Titre
% 
% Syntax
%   tagTitre = get_tag_Titre(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   tagTitre  : tag du bouton Titre
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_tag_titre.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagTitre = get_tag_titre(this)

% Maintenance Auto : try
    tagTitre = this.tagTitre;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_tag_titre', lasterr);
% Maintenance Auto : end
