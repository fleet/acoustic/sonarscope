% Retourne le titre de l entete
% 
% Syntax
%   titre = get_titre(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   titre    : titre de l entete
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: get_titre.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function titre = get_titre(this)

% Maintenance Auto : try
    titre = this.titre;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'get_titre', lasterr);
% Maintenance Auto : end
