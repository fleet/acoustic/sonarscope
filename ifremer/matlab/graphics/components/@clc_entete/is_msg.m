% Test si message est un message du composant
% 
% Syntax
%   flag = is_msg(this, message)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   flag : 1=le message est un message du composant
%          0=le message n'est pas  un message du composant
%
% Examples
%   c = clc_entete
%   flag = is_msg(c, 'toto')
%   flag = is_msg(c, 'Msg clc_entete Service')
%
% See also clc_entete Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function flag = is_msg(this, message)

flag = 0;
if strcmp(message, this.msgIfremer)
    flag = 1;
elseif strcmp(message, this.msgService)
    flag = 1;
elseif strcmp(message, this.msgTitre)
    flag = 1;
elseif strcmp(message, this.msgCopyright)
    flag = 1;
end
