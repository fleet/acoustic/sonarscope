% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   str : une chaine de caracteres representative de l'instance
%
% Examples
%
% See also clc_entete Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

% ----------
% Traitement

str{end+1} = sprintf('titre              <-> %s', this.titre);
str{end+1} = sprintf('TitreTooltipString <-> %s', this.TitreTooltipString);
str{end+1} = sprintf('msgIfremer         <-> %s', this.msgIfremer);
str{end+1} = sprintf('msgService         <-> %s', this.msgService);
str{end+1} = sprintf('msgTitre           <-> %s', this.msgTitre);
str{end+1} = sprintf('msgCopyright       <-> %s', this.msgCopyright);
str{end+1} = sprintf('tagIfremer         <-> %s', this.tagIfremer);
str{end+1} = sprintf('tagService         <-> %s', this.tagService);
str{end+1} = sprintf('tagTitre           <-> %s', this.tagTitre);
str{end+1} = sprintf('tagCopyright       <-> %s', this.tagCopyright);
str{end+1} = sprintf('NomFicHelp         <-> %s', this.NomFicHelp);

str{end+1} = sprintf('--- heritage cl_component ---');
str{end+1} = char(this.cl_component);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
