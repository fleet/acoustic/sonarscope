% Création de la frame globale du composant
%
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

ihm = {};

%% Bouton Ifremer

ihm{end+1}.Lig           = 1;
ihm{end}.Col             = 1;
ihm{end}.Style           = 'pushbutton';
ihm{end}.Tag             = get(this, 'msgIfremer');
ihm{end}.String          = '';
ihm{end}.TooltipString   = Lang('Internet de l''Ifremer', 'To Ifremer web site');
ihm{end}.Callback        = built_cb(this, this.msgIfremer);
ihm{end}.CData           = logo(this);

%% Bouton Service

ihm{end+1}.Lig           = 1;
ihm{end}.Col             = 2:3;
ihm{end}.Style           = 'pushbutton';
ihm{end}.Tag             = get(this, 'msgService');
ihm{end}.String          = Lang('DFO-NSE-ASTI', 'DFO-NSE-ASTI');
ihm{end}.TooltipString   = Lang('Internet dde la flotte océanographique', 'To IFREMER fleet web site');
ihm{end}.Callback        = built_cb(this, this.msgService);

%% Bouton Titre

ihm{end+1}.Lig           = 1;
ihm{end}.Col             = 4:6;
ihm{end}.Style           = 'pushbutton';
ihm{end}.Tag             = get(this, 'msgTitre');
ihm{end}.String          = get(this, 'titre');
% ihm{end}.TooltipString = get(this, 'TitreTooltipString');
ihm{end}.Callback        = [];
ihm{end}.FontWeight      = 'bold';

%% Bouton Copyright

ihm{end+1}.Lig           = 1;
ihm{end}.Col             = 7;
ihm{end}.Style           = 'pushbutton';
ihm{end}.Tag             = get(this, 'msgCopyright');
ihm{end}.String          = 'Copyright';
ihm{end}.TooltipString   = Lang('Page internet décrivant les termes de Copyright pour SonarScope', 'To SonarScope Copyright document');
ihm{end}.Callback        = built_cb(this, this.msgCopyright);
ihm{end}.BackgroundColor = JauneIfremer;

%% Creation des frames

this.globalFrame = cl_frame ([]) ;
this.globalFrame = set_handle_frame(this.globalFrame, get_anchor (this));
this.globalFrame = set_inset_x(this.globalFrame, get_inset_x (this));
this.globalFrame = set_inset_y(this.globalFrame, get_inset_y (this));
this.globalFrame = set_max_lig(this.globalFrame, 1);
this.globalFrame = set_max_col(this.globalFrame, 7);
this.globalFrame = set_tab_uicontrols(this.globalFrame, ihm);
