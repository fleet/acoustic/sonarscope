% Lecture du logo de l IFREMER
%
% Syntax
%   img = logo(this)
%
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   img      : image du logo
%
% See also clc_entete Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function img = logo(~)

pas = 2.5;

%% Lecture du fichier

nomFichier = getNomFicDatabase('logo.tif');
img        = double(imread (nomFichier)) / 255;
sz         = size(img);

%% Construction finale de l'image

img = img(floor(1:pas:sz(1)), floor(1:pas:sz(2)), :);
