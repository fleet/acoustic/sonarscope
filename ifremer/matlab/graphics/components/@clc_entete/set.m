% Modification des attributs de l'instance
% 
% Syntax
%   instance = set(this, ...)
%
% Input Arguments
%   this : instance de clc_entete
%
% Name-Value Pair Arguments
%   titre             : titre de l entete
%   msgIfremer        : message cb du bouton Ifremer
%   msgService        : message cb du bouton Service
%   msgTitre          : message cb du bouton Titre
%   msgCopyright      : message cb du bouton Copyright
%   tagIfremer        : tag du bouton Ifremer
%   tagService        : tag du bouton Service
%   tagTitre          : tag du bouton Titre
%   tagCopyright      : tag du bouton Copyright
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant
%   componentEnable   : accessibilite du composant
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentAnchor   : ancre du composant
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% Examples
%    c = clc_entete ; 
%    set(c, 'tagIfremer', 'IFREMER') ;
%    set(c, 'componentName', 'zoneEntete')
%
% See also clc_entete Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

%% Lecture des arguments

[varargin, titre]                   = getPropertyValue(varargin, 'titre', []);
[varargin, this.TitreTooltipString] = getPropertyValue(varargin, 'TitreTooltipString', this.TitreTooltipString);
[varargin, msgIfremer]              = getPropertyValue(varargin, 'msgIfremer', []);
[varargin, msgService]              = getPropertyValue(varargin, 'msgService', []);
[varargin, msgTitre]                = getPropertyValue(varargin, 'msgTitre', []);
[varargin, msgCopyright]            = getPropertyValue(varargin, 'msgCopyright', []);
[varargin, tagIfremer]              = getPropertyValue(varargin, 'tagIfremer', []);
[varargin, tagService]              = getPropertyValue(varargin, 'tagService', []);
[varargin, tagTitre]                = getPropertyValue(varargin, 'tagTitre', []);
[varargin, tagCopyright]            = getPropertyValue(varargin, 'tagCopyright', []);
[varargin, visible]                 = getPropertyValue(varargin, 'componentVisible', []);
[varargin, enable]                  = getPropertyValue(varargin, 'componentEnable', []);
[varargin, this.NomFicHelp]         = getPropertyValue(varargin, 'NomFicHelp', this.NomFicHelp);

%% Initialisation des attributs

if ~isempty(titre)
    this = set_titre(this, titre);
end
if ~isempty(msgIfremer)
    this = set_msg_ifremer(this, msgIfremer);
end
if ~isempty(msgService)
    this = set_msg_service(this, msgService);
end
if ~isempty(msgTitre)
    this = set_msg_titre(this, msgTitre);
end
if ~isempty(msgCopyright)
    this = set_msg_copyright(this, msgCopyright);
end
if ~isempty(tagIfremer)
    this = set_tag_ifremer(this, tagIfremer);
end
if ~isempty(tagService)
    this = set_tag_service(this, tagService);
end
if ~isempty(tagTitre)
    this = set_tag_titre(this, tagTitre);
end
if ~isempty(tagCopyright)
    this = set_tag_copyright(this, tagCopyright);
end
if ~isempty(visible)
    this = set_visible(this, visible);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end

%% Traitement des messages de la super classe

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
