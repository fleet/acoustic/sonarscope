% Initialisation de l'accessibilite du composant
% 
% Syntax
%   this = set_enable(this, enable)
%
% Input Arguments
%   this   : instance de clc_entete
%   enable : accessibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    if is_edit(this)
        this.globalFrame  = set_enable(this.globalFrame, enable);
    end
    this.cl_component = set_enable(this.cl_component, enable);
else
    my_warndlg('clc_entete/set_enable : Invalid value', 1);
end
