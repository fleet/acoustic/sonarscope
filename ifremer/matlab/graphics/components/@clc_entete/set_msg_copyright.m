% Initialisation du message de la call back pour le bouton Copyright
% 
% Syntax
%   this = set_msg_copyright(this, msgCopyright)
%
% Input Arguments
%   this         : instance de clc_entete
%   msgCopyright : message de la call back pour le bouton Copyright
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_msg_copyright.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_msg_copyright(this, msgCopyright)

% Maintenance Auto : try
    this.msgCopyright = msgCopyright ;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'set_msg_copyright', lasterr);
% Maintenance Auto : end
