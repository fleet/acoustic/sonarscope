% Initialisation du message de la call back pour le bouton Service
% 
% Syntax
%   this = set_msg_service(this, msgService)
%
% Input Arguments
%   this       : Instance de clc_entete
%   msgService : Message de la call back pour le bouton Service
%
% Output Arguments
%   this : Instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_msg_service.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_msg_service(this, msgService)

% Maintenance Auto : try
    this.msgService = msgService;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'set_msg_service', lasterr);
% Maintenance Auto : end
