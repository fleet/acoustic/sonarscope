% Initialisation du tag du bouton Copyright
% 
% Syntax
%   this = set_tag_copyright(this, tagCopyright)
%
% Input Arguments
%   this         : instance de clc_entete
%   tagCopyright : tag pour le bouton Copyright
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_tag_copyright.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_tag_copyright(this, tagCopyright)

% Maintenance Auto : try
    this.tagCopyright = tagCopyright;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'set_tag_copyright', lasterr);
% Maintenance Auto : end
