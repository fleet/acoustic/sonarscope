% Initialisation du tag du bouton Ifremer
% 
% Syntax
%   this = set_tag_ifremer(this, tagIfremer)
%
% Input Arguments
%   this       : Instance de clc_entete
%   tagIfremer : Tag pour le bouton Ifremer
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_tag_ifremer.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_tag_ifremer(this, tagIfremer)

% Maintenance Auto : try
    this.tagIfremer = tagIfremer;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'set_tag_ifremer', lasterr);
% Maintenance Auto : end
