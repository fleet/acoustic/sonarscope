% Initialisation du tag du bouton Service
% 
% Syntax
%   this = set_tag_service(this, tagService)
%
% Input Arguments
%   this       : Instance de clc_entete
%   tagService : Tag pour le bouton Service
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_tag_service.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_tag_service(this, tagService)

% Maintenance Auto : try
    this.tagService = tagService;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'set_tag_service', lasterr);
% Maintenance Auto : end
