% Initialisation du tag du bouton Titre
% 
% Syntax
%   this = set_tag_titre(this, tagTitre)
%
% Input Arguments
%   this     : instance de clc_entete
%   tagTitre : tag pour le bouton Titre
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_tag_titre.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_tag_titre(this, tagTitre)

% Maintenance Auto : try
    this.tagTitre = tagTitre;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'set_tag_titre', lasterr);
% Maintenance Auto : end
