% Initialisation du titre de l entete
% 
% Syntax
%   this = set_titre(this, titre)
%
% Input Arguments
%   this : instance de clc_entete
%   titre    : titre de l entete
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_titre.m,v 1.4 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_titre(this, titre)

if ischar(titre)
    this.titre = titre;
    if is_edit(this)
        h = findobj(gcf, 'Tag', get (this, 'msgTitre'));
        if ishandle(h)
            set(h, 'String', this.titre);
        end
    end
else
    my_warndlg('clc_entete/set_titre : Invalid argument type', 1);
end
