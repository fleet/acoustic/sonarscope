% Initialisation de la visibilite du composant
% 
% Syntax
%   this = set_visible(this, visible)
%
% Input Arguments
%   this    : instance de clc_entete
%   visible : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_entete initialisee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: set_visible.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% -------------------------------------------------------------------------

function this = set_visible(this, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    if is_edit(this)
        this.globalFrame  = set_visible(this.globalFrame, visible);
    end
    this.cl_component = set_visible(this.cl_component, visible);
else
    my_warndlg('clc_entete/set_visible : Invalid value', 0);
end
