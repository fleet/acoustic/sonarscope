% Test graphique du composant
%
% Syntax
%   this = test(this)
% 
% Input Arguments
%   this : instance de clc_entete
%
% Output Arguments
%   this : instance de clc_entete
% 
% Examples
%    this = test(clc_entete)
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/03/28 13:45:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this)

% Maintenance Auto : try
    
    % -------------------------------------------------------
    % Creation d'une figure ainsi que de l ancre du composant
    
    figure('Visible', 'off');
    uicontrol( 'Visible', 'on', 'Tag', 'ancre_clc_entete', 'Position', [20 20 500 30]);
    
    % ---------------------------------------
    % Creation et initialisation du composant
    
    this = clc_entete([]);
    this = set_titre(this,      'Mon Titre A Moi');
    this = set_name(this,       'ancre_clc_entete');
    this = set_user_name(this,  'clc_entete');
%     this = set_user_cb(this,    'gerer_callback');
    
    % -------------------------------
    % Creation graphique du composant
    
    this = editobj(this);
    
    % ------------------------
    % Rend le resultat visible
    
    set(gcf, 'Visible', 'on');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'test', lasterr);
% Maintenance Auto : end
