% Mise a jour du composant
%
% Syntax
%   this = update(this)
% 
% Input Arguments
%   this : instance de clc_entete
%
% Remarks : doit etre surchargee
%
% See also clc_entete Authors
% Authors : DCF
% VERSION  : $Id: update.m,v 1.3 2003/07/07 16:12:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = update(this)

% Maintenance Auto : try
    
    % -----------------------------------
    % Update graphique de la super classe
    
    this.cl_component = update(this.cl_component);
    
    % -----------------------------------------------------
    % Update graphique des elements graphiques du composant
    
    if ~isempty(this.globalFrame)
        this.globalFrame = update(this.globalFrame);
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_entete', 'update', lasterr);
% Maintenance Auto : end
