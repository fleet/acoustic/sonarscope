% Constructeur de clc_framev, classe composant
%
% Syntax
%   a = clc_framev(...)
% 
% Name-Value Pair Arguments
%   nbElements        : nombre de frames du vecteur de frames
%   tag               : base du tag des frames
%   orientation       : orientation du vecteur : 'lig', 'col' ou 'mat'
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments 
%   a : instance de clc_framev
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    quit = clc_framev
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: clc_framev.m,v 1.4 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    01/02/2000 - DCF - creation
%    23/04/2001 - DCF - mise en reference
%    05/06/2001 - DCF - mise a jour code et optimisation
% ----------------------------------------------------------------------------

function this = clc_framev(varargin)

% Maintenance Auto : try
    
    % ----------------------------------------------------------------
    % Definition de la structure et initialisation
    %
    % globalFrame   : instance de cl_frame, frame globale du composant
    % nbElements    : nombre de frames du vecteur de frames
    % tag           : base du tag des frames
    % orientation   : orientation du vecteur : en ligne, en colonne ou en
    %                 matrice
    % size          : taille de la matrice si orientation == 'mat'
    % fVisible      : flag de visibilite des frames
    % fCollapse     : flag de presence des frames 
    
    this.globalFrame    = [];
    this.nbElements     = [];
    this.tag            = [];
    this.orientation    = 'lig';
    this.size           = [];
    this.fVisible       = [];
    this.fCollapse      = [];
    
    % ------------
    % Super-classe
    
    if (nargin == 1) & isempty(varargin {1})
        composant = cl_component([]);
    else
        composant = cl_component;
    end
    
    % -------------------
    % Creation de l'objet
    
    this = class(this, 'clc_framev', composant);
    
    % ----------------------------
    % Pre-nitialisation des champs 
    
    if (nargin == 1) & isempty(varargin{1})
        return
    end

    % ---------------------------------------------------------------------
    % Initialisation des valeurs transmises lors de l'appel au constructeur
    
    this = set(this, varargin {:});
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'clc_framev', lasterr);
% Maintenance Auto : end
