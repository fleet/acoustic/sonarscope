% Suppression virtuelle d'un element du vecteur
%
% Syntax
%   a = collapse(a, indiceElement, value)
% 
% Input Arguments
%   a             : instance de clc_framev
%   indiceElement : indice de l element concerne par l operation
%   value         : realite de l element {'on', 'off'}
%
% Remarks : doit etre surchargee
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = collapse(this, indiceElement, value)

%% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= this.nbElements)
    
    %% Test de la visibilit�
    
    if strcmp(value, 'on') || strcmp(value, 'off')
        
        %% Test l'existance des frames
        
        if ~isempty(this.globalFrame)
            
            %% Element virtuellement supprim� : value == 'on'
            %% Element non supprim�           : value == 'off'
            
            if strcmp(value, 'on')
                this.fCollapse(indiceElement) = 1;
            else
                this.fCollapse(indiceElement) = 0;
            end
            this = maj_collapse(this);
        else
            if strcmp(value, 'on')
                this.fCollapse(indiceElement) = 1;
            else
                this.fCollapse(indiceElement) = 0;
            end
        end
    else
        my_warndlg('clc_framev/collapse : Invalid visibility value', 0);
    end
else
    my_warndlg('clc_framev/collapse : Invalid element indice', 0);
end
