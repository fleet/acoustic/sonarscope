% Destruction du composant
%
% Syntax
%   a = delete(a)
% 
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   a : instance de clc_framev
%
% Remarks : doit etre surchargee
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    % --------------------------------------------------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(this.globalFrame)
        this.globalFrame = delete(this.globalFrame);
        this.globalFrame = [];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'delete', lasterr);
% Maintenance Auto : end
