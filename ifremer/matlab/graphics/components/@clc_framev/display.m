% Affichage des donnees d'une instance de clc_framev
%
% Syntax
%   display(a)
%
% Input Arguments 
%   instance : instance de clc_framev
%
% Examples
%   a = clc_framev;
%   display(a)
%   a
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
