% Representation de l'instance
%
% Syntax
%   a = editobj(a) 
%
% Input Arguments
%   a : instance de clc_framev
%
% OUPUT PARAMETERS :
%   a : instance de clc_framev
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: editobj.m,v 1.4 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = editobj(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Realite graphique ssi le nombre d element est strictement positif
    
    if (get(this, 'nbElements') > 0) & ~is_edit(this)
        
        % ------------------------------------------------------------------------
        % Creation de l interface graphique
        
        this.cl_component = editobj(this.cl_component);
        this = construire_ihm(this);
        
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'editobj', lasterr);
% Maintenance Auto : end
