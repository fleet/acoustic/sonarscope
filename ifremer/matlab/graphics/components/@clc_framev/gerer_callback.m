% Methode de gestion des callbacks du composant
%
% Syntax
%   a = gerer_callback(a, cbName)
% 
% Input Arguments
%   a      : instance de cl_component
%   cbName : nom de la callback
%
% Output Arguments
%   a : a de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also cl_component Authors
% Authors : DCF
% VERSION  : $Id: gerer_callback.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

% --------------------
% Selection du message

if length(varargin) == 1
    
else
    my_warndlg('clc_framev/gerer_callback : Invalid arguments number', 0);
end
