% Lecture des attributs de l'instance
%
% Syntax
%   [...] = get(a, ...)
% 
% Input Arguments
%   a : instance de clc_framev
%
% Name-Value Pair Arguments
%   nbElements        : nombre de frames du vecteur de frames
%   tag               : base du tag des frames
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant
%   componentEnable   : accessibilite du composant
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    c = clc_framev('nbElements', 3);
%    get(c, 'nbElements')
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'nbElements'
            varargout{end+1} = get_nb_elements(this); %#ok<AGROW>
        case 'tag'
            varargout{end+1} = get_base_tag(this); %#ok<AGROW>
        case 'orientation'
            varargout{end+1} = get_orientation(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.cl_component, varargin{i}); %#ok<AGROW>
    end
end
