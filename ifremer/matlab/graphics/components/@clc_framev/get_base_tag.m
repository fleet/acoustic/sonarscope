% Retourne la base du tag des frames du vecteur
% 
% Syntax
%   tag = get_base_tag(a)
%
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   tag      : base du tag des frames du vecteur
%
% See also clc_framev
% Authors : DCF
% VERSION  : $Id: get_base_tag.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tag = get_base_tag(this)

% Maintenance Auto : try
    tag = this.tag ;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'get_base_tag', lasterr);
% Maintenance Auto : end
