% Retourne le nombre d'elements du vecteur
% 
% Syntax
%   nbElements = get_nb_elements(a)
%
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   nbElements : nombre d elements du vecteur
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: get_nb_elements.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbElements = get_nb_elements(this)

% Maintenance Auto : try
    nbElements = this.nbElements;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'get_nb_elements', lasterr);
% Maintenance Auto : end
