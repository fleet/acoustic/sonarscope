% Retourne l'orientation du vecteur de frames
% 
% Syntax
%   orientation = get_orientation(a)
%
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   orientation : orientation du vecteur de frames
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: get_orientation.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function orientation = get_orientation(this)

% Maintenance Auto : try
    orientation = this.orientation;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'get_orientation', lasterr);
% Maintenance Auto : end
