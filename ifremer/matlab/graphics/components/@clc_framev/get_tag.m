% Construit le tag d'un element du vecteur
%
% Syntax
%   tag = get_tag(a, indiceElement)
% 
% Input Arguments
%   a             : instance de clc_framev
%   indiceElement : indice de l element pour lequel le tag est demande
%
% Output Arguments
%   tag : tag de l'element specifie par son indice
% 
% Examples
%    c = clc_framev('nbElements', 3);
%    get_tag(c, 2)
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function  tag = get_tag(this, indiceElement)

% ------------------------------
% Test sur la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= this.nbElements)
    tag = [this.tag, ' ', num2str(indiceElement)];
else
    my_warndlg('clc_framev/get_tag : Indice value out of range', 0);
end
