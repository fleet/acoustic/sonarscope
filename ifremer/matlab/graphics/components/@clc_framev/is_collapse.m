% Indique si l'element specifie est virtuellement supprime
%
% Syntax
%   flag = is_collapse(a, indiceElement)
% 
% Input Arguments
%   a             : instance de clc_framev
%   indiceElement : indice de l element
%
% OUPUT PARAMETERS :
%   flag : booleen indiquant la suppression virtuelle
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: is_collapse.m,v 1.4 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function flag = is_collapse(this, indiceElement)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= this.nbElements)
    flag = this.fCollapse(indiceElement);
else
    my_warndlg('clc_framev/is_collapse : Indice out of range', 0);
end
