% Indique si l'element specifie est visible
%
% Syntax
%   flag = is_visible(a, indiceElement)
% 
% Input Arguments
%   a             : instance de clc_framev
%   indiceElement : indice de l element
%
% OUPUT PARAMETERS :
%   flag : booleen indiquant la visibilite
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: is_visible.m,v 1.4 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function flag = is_visible(this, indiceElement)

% -----------------------------
% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= this.nbElements)
    flag = this.fVisible(indiceElement);
else
    my_warndlg('clc_framev/is_visible : Indice out of range', 0);
end
