% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   str : une chaine de caracteres representative de l'instance
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.5 2003/07/22 09:08:57 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
%    05/06/2001 - DCF - mise a jour code et optimisation
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try

    str = [];
    
    % ----------
    % Traitement
    
   str{end+1} = sprintf ('nbElements  <-> %d', this.nbElements);
   str{end+1} = sprintf ('tag         <-> %s', this.tag);
   str{end+1} = sprintf ('orientation <-> %s', this.orientation);
   str{end+1} = sprintf ('size        <-> %s', num2strCode(this.size));
   str{end+1} = sprintf ('fVisible    <-> %s', num2strCode(this.fVisible));
   str{end+1} = sprintf ('fCollapse   <-> %s', num2strCode(this.fCollapse));
   
   str{end+1} = sprintf('--- heritage cl_component ---');
   str{end+1} = char(this.cl_component);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
  
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'char_instance', lasterr);
% Maintenance Auto : end
