% Construction de base de l IHM
% 
% Syntax
%   a = construire_ihm(a)
%
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   a : instance de clc_framev
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: construire_ihm.m,v 1.4 2003/07/21 17:11:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
%    05/06/2001 - DCF - mise a jour code et optimisation
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

% Maintenance Auto : try
    
    % ---------------------------------
    % Creation de la frame du composant
    
    this = creer_frame_globale(this);
    
    % ----------------------------------------
    % Prise en compte des composants collapses
    
    this = maj_collapse(this);
    
    % ------------------------------
    % Mise a jour de l'accessibilite
    
    if strcmp(get_enable(this), 'off')
        this.globalFrame = set_enable(this.globalFrame, 'off');
    end
    
    % ----------------------------
    % Mise a jour de la visibilite
    
    if strcmp(get_visible(this), 'off')
        this.globalFrame = set_visible(this.globalFrame, 'off');
    end 
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'construire_ihm', lasterr);
% Maintenance Auto : end
