% Creation de la frame globale du composant
% 
% Syntax
%   a = creer_frame_globale(a)
%
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   a : instance de clc_framev initialisee
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: creer_frame_globale.m,v 1.4 2003/07/21 17:11:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

% -------
% Locales

ib       = 0;
ihm      = [];
maxLig   = [];
maxCol   = [];

for i=1:this.nbElements

    % -------------------------
    % Gestion de la disposition

    switch this.orientation
        case 'lig'
            ihm{i}.Lig      = 1;
            ihm{i}.Col      = i;
        case 'col'
            ihm{i}.Lig      = i;
            ihm{i}.Col      = 1;
        case 'mat'
            ihm{i}.Lig      = 1 + floor((i-1) / this.size(2));
            ihm{i}.Col      = 1 + mod(i-1, this.size(2));
    end

    % ------------
    % Style et tag

    ihm{i}.Style    = 'frame';
    ihm{i}.Tag      = get_tag(this, i);

    % ------------------------
    % Gestion de la visibilite

    if this.fVisible(i)
        ihm{i}.Visible = 'on';
    else
        ihm{i}.Visible = 'off';
    end

end

% ------------------------------------------------------
% Determination du nombre total de lignes et de colonnes

switch this.orientation
    case 'lig'
        maxLig = 1;
        maxCol = this.nbElements;
    case 'col'
        maxLig = this.nbElements;
        maxCol = 1;
    case 'mat'
        maxLig = this.size(1);
        maxCol = this.size(2);
end

% -------------------
% Creation des frames

this.globalFrame = cl_frame([]);
this.globalFrame = set_handle_frame(    this.globalFrame, get_anchor(this));
this.globalFrame = set_inset_x(         this.globalFrame, get_inset_x(this));
this.globalFrame = set_inset_y(         this.globalFrame, get_inset_y(this));
this.globalFrame = set_max_lig(         this.globalFrame, maxLig);
this.globalFrame = set_max_col(         this.globalFrame, maxCol);
this.globalFrame = set_tab_uicontrols(  this.globalFrame, ihm);
