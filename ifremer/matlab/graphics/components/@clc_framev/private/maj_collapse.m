% Replace les frames du vecteur en tenant compte du collapse
%
% Syntax
%   a = maj_collapse(a)
%
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   a : instance de clc_framev
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = maj_collapse(this)

%% Collapse pas traite dans le cas d'une matrice Besoin aide DCF pour faire �a

if strcmp(this.orientation, 'mat')
    return
end

%% Locales

% nbEle  = 0;
indice = 0;

%% Nombre d elements a representer (non collapses)

nbEle = this.nbElements - (this.fCollapse) * (this.fCollapse)';

%% Maintient le nombre d elements strictement positif (sinon calcul en x et en y non possible)

if ~nbEle
    nbEle = 1;
end

%% L'instance doit avoir une repr�sentation graphique

if ~isempty(this.globalFrame)

    %% Mise � jour des caract�ristiques de la frame

    if strcmp(this.orientation, 'col')
        this.globalFrame = set_max_lig(this.globalFrame, nbEle);
        this.globalFrame = set_max_col(this.globalFrame, 1);
    else
        this.globalFrame = set_max_lig(this.globalFrame, 1);
        this.globalFrame = set_max_col(this.globalFrame, nbEle);
    end

    %% Handle des frames � repositionner

    handles = get_tab_uicontrols(this.globalFrame);

    %% Repositionne tous les �l�ments

    for k=1:this.nbElements
        if this.fCollapse(k)

            %% Rend la frame invisible

            this.globalFrame = visible(this.globalFrame, 'uicontrol', k, 'off');
        else

            %% Incr�mente le compteur d'�l�ments non collaps�s

            indice = indice + 1;

            %% Repositionne l'�l�ment non collaps�

            if strcmp(this.orientation, 'col')
                move(this.globalFrame, handles(k), indice, 1);
                drawnow
            else
                move(this.globalFrame, handles(k), 1, indice);
                drawnow
            end

            %% Traite la visibilit� de l'�l�ment non collaps�

            if this.fVisible(k)
                this.globalFrame = visible(this.globalFrame, 'uicontrol', k, 'on');
            else
                this.globalFrame = visible(this.globalFrame, 'uicontrol', k, 'off');
            end
        end
    end
else
    my_warndlg('clc_framev/maj_collapse : No matlab instances to collapse', 0);
end
