% Modification des attributs de l'instance
%
% Syntax
%   a = set(a, ...)
%
% Input Arguments
%   a : instance de clc_framev
%
% Name-Value Pair Arguments
%   nbElements        : nombre de frames du vecteur de frames
%   tag               : base du tag des frames
%   orientation       : orientation du vecteur {'lig', 'col'}
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments
%   a : instance de clc_framev initialisee
%
% Examples
%    c = clc_framev;
%    set(c, 'nbElements', 3, 'tag', 'clc_framev_tag')
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% ---------------------
% Lecture des arguments

[varargin, nbElements] = getPropertyValue(varargin, 'nbElements', []);
[varargin, baseTag] = getPropertyValue(varargin, 'tag', []);
[varargin, orientation] = getPropertyValue(varargin, 'orientation', []);
[varargin, size] = getPropertyValue(varargin, 'size', []);
[varargin, visible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, enable] = getPropertyValue(varargin, 'componentEnable', []);

% ----------------------------
% Initialisation des attributs

if ~isempty(nbElements)
    this = set_nb_elements(this, nbElements);
end
if ~isempty(baseTag)
    this = set_base_tag(this, baseTag);
end
if ~isempty(orientation)
    this = set_orientation(this, orientation);
end
if ~isempty(size)
    this.size = size;
end
if ~isempty(visible)
    this = set_visible(this, visible);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end

% -----------------------------------------
% Traitement des message de la super classe

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
