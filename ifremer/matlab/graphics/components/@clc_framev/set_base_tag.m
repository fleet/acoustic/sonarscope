% Initialisation de la base du tag des frames
% 
% Syntax
%   a = set_base_tag(a, tag)
%
% Input Arguments
%   a   : instance de clc_framev
%   tag : base du tag des frames
%
% Output Arguments
%   a : instance de clc_framev initialisee
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: set_base_tag.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_base_tag(this, tag)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(tag)
    if ~isempty(this.globalFrame)
        this = delete(this);
        this.tag = tag;
        this = editobj(this);
    else
        this.tag = tag;
    end
else
    my_warndlg('clc_framev/set_base_tag : Invalide argument format', 0);
end
