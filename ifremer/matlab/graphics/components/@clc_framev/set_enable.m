% Initialisation de l'accessibilite du composant
% 
% Syntax
%   a = set_enable(a, enable)
%
% Input Arguments
%   a      : instance de clc_framev
%   enable : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   a : instance de clc_framev initialisee
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% --------------------------------------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    this.globalFrame  = set_enable(this.globalFrame, enable);
    this.cl_component = set_enable(this.cl_component, enable);
else
    my_warndlg('clc_framev/set_enable : Invalid value', 0);
end
