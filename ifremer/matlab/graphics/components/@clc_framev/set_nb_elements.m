% Initialisation du nombre d'elements du vecteur
% 
% Syntax
%   a = set_nb_elements(a, nbElements)
%
% Input Arguments
%   a          : instance de clc_framev
%   nbElements : nombre d'elements (frames) du vecteur
%
% Output Arguments
%   a : instance de clc_framev initialisee
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: set_nb_elements.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_nb_elements(this, nbElements)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(nbElements)
    
    % --------------------------------
    % Mise a jour du nombre d elements
    
    if ~isempty(this.globalFrame)
        this = delete(this);
        this.nbElements = nbElements;
        this.fVisible  = ones(1, nbElements);
        this.fCollapse = this.fVisible * 0;
        this = editobj(this);
    else
        this.nbElements = nbElements;
        this.fVisible  = ones(1, nbElements);
        this.fCollapse = this.fVisible * 0;
    end
    
else
    my_warndlg('clc_framev/set_nb_elements : Invalid argument format', 0);
end
