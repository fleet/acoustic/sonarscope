% Initialisation de l'orientation du vecteur
% 
% Syntax
%   a = set_orientation(a, orientation)
%
% Input Arguments
%   a           : instance de clc_framev
%   orientation : orientation du vecteur de frames
%
% Output Arguments
%   a : instance de clc_framev initialisee
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: set_orientation.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_orientation(this, orientation)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(orientation)
    if strcmp(orientation, 'lig') || strcmp(orientation, 'col') || strcmp(orientation, 'mat')
        if ~isempty(this.globalFrame)
            
            % --------------------------------------------------------------
            % Inversion de l orientation ? si oui, realisation sur la frame
            
            if ~strcmp(this.orientation, orientation)
                
                % ------------------------------------------------------------------
                % Lors de l inversion, les elements collapses modifient le nombre
                % de lignes et de colonnes de la frame, ce qui pose probleme. On
                % change donc ces informations le temps de l inversion
                
                switch this.orientation
                    case 'lig'
                        this.globalFrame = set_max_col(this.globalFrame, this.nbElements);
                    case 'col'
                        this.globalFrame = set_max_lig(this.globalFrame, this.nbElements);
                    case 'mat'
                        this.globalFrame = set_max_lig(this.globalFrame, this.size(1));
                        this.globalFrame = set_max_col(this.globalFrame, this.size(2));
                end
                
                this.globalFrame  = inverser(this.globalFrame);
                this.orientation  = orientation;
            end
            
            % -------------------------------------------------------
            % Repositionnement en tenant compte des frames collapsees
            
            this = maj_collapse(this);
        else
            this.orientation = orientation;
        end
    else
        my_warndlg('clc_framev/set_orientation : Invalid orientation value', 0);
    end
else
    my_warndlg('clc_framev/set_orientation : Invalid argument format', 0);
end
