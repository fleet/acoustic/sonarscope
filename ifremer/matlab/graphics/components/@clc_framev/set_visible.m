% Initialisation de la visibilite du composant
% 
% Syntax
%   a = set_visible(a, visible)
%
% Input Arguments
%   a       : instance de clc_framev
%   visible : visibilite du composant 'on' | 'off'
%
% Output Arguments
%   a : instance de clc_framev initialisee
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: set_visible.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_visible(this, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    this.globalFrame  = set_visible(this.globalFrame, visible);
    this.cl_component = set_visible(this.cl_component, visible);
else
    my_warndlg('clc_framev/set_visible : Invalid value', 0);
end
