% Test graphique du composant
%
% Syntax
%   a = test(a)
% 
% Input Arguments
%   a : instance de clc_framev
%
% Output Arguments
%   a : instance de clc_framev
% 
% Examples
%    a = test(clc_framev);
%
% See also clc_framev Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/07/21 17:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this, varargin)

% Maintenance Auto : try
    
    % -------------------------------------------------------
    % Creation d une figure ainsi que de l'ancre du composant
    
    figure('Visible', 'off');
    uicontrol(  'Visible'  , 'on', ...
                'Tag'      , 'ancre_clc_framev', ...
                'Position' , [20 20 200 200]);
    
    % ---------------------------------------
    % Creation et initialisation du composant
    
    this = clc_framev;
    this = set( this,   'nbElements',           3, ...
                        'tag',                  'clc_framev tag', ...
                        'orientation',          'lig', ...
                        'componentName',        'ancre_clc_framev', ...
                        'componentUserName',    'clc_framev', ...
                        'componentUserCb',      '' );
    
    % -------------------------------
    % Creation graphique du composant
    
    this = editobj(this);
    
    % ------------------------
    % Rend le resultat visible
    
    set(gcf, 'Visible', 'on');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_framev', 'test', lasterr);
% Maintenance Auto : end
