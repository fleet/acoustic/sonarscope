% Mise � jour du composant
%
% Syntax
%   a = update(a)
% 
% Input Arguments
%   a : instance de clc_framev
%
% Remarks : doit �tre surcharg�e
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = update(this)

%% Update graphique de la super classe

this.cl_component = update(this.cl_component);

%% Update graphique des elements graphiques du composant. Traitement
% different si presence d'�l�ment collaps�

if ~isempty(this.globalFrame)
    if find(this.fCollapse == 1)
        this = maj_collapse(this);
    else
        this.globalFrame = update(this.globalFrame);
    end
end
