% Initialisation de la visibilite d un element du vecteur
%
% Syntax
%   a = visible(a, indiceElement, visibilite)
% 
% Input Arguments
%   a      : instance de clc_framev
%   indiceElement : indice de l element concerne par l operation
%   visibilite    : visibilite de l'element 'on' | 'off'
% 
% Output Arguments
%   a      : instance de clc_framev
%
% Remarks : doit etre surchargee
%
% See also clc_framev Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = visible(this, indiceElement, visibilite)

%% Test de la valeur de l'indice

if (indiceElement > 0) && (indiceElement <= get(this, 'nbElements'))
    
    %% Test de la visibilité
    
    if strcmp(visibilite, 'on') || strcmp(visibilite, 'off')
        
        % ---------------------------
        % Test l existance des frames
        
        if ~isempty(this.globalFrame)
            
            %% Traitement graphique
            
            this.globalFrame = visible(this.globalFrame, ...
                'uicontrol', indiceElement, visibilite);
            
            %% Initialisation de l'instance
            
            if strcmp(visibilite, 'on')
                this.fVisible(indiceElement) = 1;
            else
                this.fVisible(indiceElement) = 0;
            end
        else
            
            %% Initialisation de l'instance
            
            if strcmp(visibilite, 'on')
                this.fVisible(indiceElement) = 1;
            else
                this.fVisible(indiceElement) = 0;
            end
        end
    else
        my_warndlg('clc_framev/visible : Invalid visibility value', 0);
    end
else
    my_warndlg('clc_framev/visible : Invalid element indice', 0);
end
