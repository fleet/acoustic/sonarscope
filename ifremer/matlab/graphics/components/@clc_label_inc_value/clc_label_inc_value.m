% Constructeur du composant clc_label_inc_value (Zone d'edition de valeur,
%   avec affichage du nom et de l'unite du parametre, et possibilite d'incrementation)
%
% Syntax
%   a = clc_label_inc_value(...)
% 
% Name-Value Pair Arguments
%   titre             : texte de titre precisant la variable
%   unite             : texte d unite precisant l unite
%   nbColTitre        : nombre de colonnes occupees par le titre
%   nbColValue        : nombre de colonnes occupees par la valeur
%   nbColIncValue     : nombre de colonnes occupees par le composant value
%   nbColUnite        : nombre de colonnes occupees par l unite
%   value             : valeur reelle
%   format            : format d affichage de la valeur
%   minValue          : valeur minimale admissible
%   maxValue          : valeur maximale admissible
%   tag               : tag du label d affichage de la valeur
%   tooltip           : info-bulle du label d affichage de la valeur
%   editable          : flag indiquant si la valeur est editable
%   actionnable       : flag indiquant si le titre est cliquable
%   msgEdit           : msg de call back en cas d edition de la valeur
%   msgAction         : msg de call back en cas de click du titre
%   msgPlus           : msg de call back en cas de click sur le plus
%   msgMoins          : msg de call back en cas de click sur le moins
%   enable            : positionne l accessibilite de la valeur
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant     'on' | 'off'
%   componentEnable   : accessibilite du composant  'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments 
%    a : instance de clc_label_inc_value
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    v = clc_label_inc_value
%
% See also clc_label_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_label_inc_value(varargin)

%% D�finition de la structure et initialisation
%
% globalFrame : instance de cl_frame, frame globale du composant
% c_titre     : composant d edition du titre
% c_editValue : composant d edition de la valeur
% c_unite     : composant d edition de l unite
% nbColTitre  : nombre de colonnes occupees par le titre
% nbColValue  : nombre de colonnes occupees par la valeur
% nbColUnite  : nombre de colonnes occupees par l unite

this.globalFrame = [];
this.c_editValue = clc_edit_inc_value;
this.c_titre     = clc_edit_texte;
this.c_unite     = clc_edit_texte;
this.nbColTitre  =  1;
this.nbColValue  =  1;
this.nbColUnite  =  1;
this.handleAnchor       = [];   % Internal property

%% Super-classe

composant = cl_component;

%% Cr�ation de l'objet

this = class(this, 'clc_label_inc_value', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
