% Gestion des call back d'une IHM d'une instance
%
% Syntax
%   this = gerer_callback(this, cbName)
% 
% Input Arguments
%   this : instance de cl_edit_value
%   cbName : nom de la call back
%
% Name-Value Pair Arguments
%   Les valeurs sont dynamiques, elles sont lues par :
%     this.msg...
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also clc_edit_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

% if length(varargin) == 1
    switch varargin{1}
        case get(this, 'msgEdit')
            this = traiter_edit(this);
        case get(this, 'msgAction')
            this = traiter_action(this);
        case get(this, 'msgPlus')
            this = traiter_plus(this);
        case get(this, 'msgMoins')
            this = traiter_moins(this);
        otherwise
            my_warndlg('clc_edit_value/gerer_callback : Invalid message', 0);
    end
% else
%     my_warndlg('clc_edit_value/gerer_callback : Invalid arguments number', 0);
% end
