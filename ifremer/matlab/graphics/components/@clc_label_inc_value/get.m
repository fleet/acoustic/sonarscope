% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Name-Value Pair Arguments
%   cf. clc_label_inc_value
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    v = clc_label_inc_value; 
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also clc_label_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for k=1:length(varargin)
    switch varargin{k}
        case 'titre'
            varargout{end+1} = get_titre(this); %#ok<AGROW>
        case 'unite'
            varargout{end+1} = get_unite(this); %#ok<AGROW>
        case 'nbColTitre'
            varargout{end+1} = get_nb_col_titre(this); %#ok<AGROW>
        case 'nbColIncValue'
            varargout{end+1} = get_nb_col_value(this); %#ok<AGROW>
        case 'nbColUnite'
            varargout{end+1} = get_nb_col_unite(this); %#ok<AGROW>
        case 'enable'
            varargout{end+1} = get_enable(this); %#ok<AGROW>
        case {'value', 'format', 'minValue', 'maxValue', ...
                'tooltip', 'tag', 'handle', 'editable',    ...
                'msgEdit', 'nbColValue', 'msgPlus', 'msgMoins' }
            varargout{end+1} = get(this.c_editValue, varargin {1}); %#ok<AGROW>
        case {'actionnable', 'msgAction'}
            varargout{end+1} = get(this.c_titre, varargin {1}); %#ok<AGROW>
        case {'componentVisible', 'componentEnable', 'componentName', ...
                'componentAnchor' , 'componentUserName', 'componentUserCb' }
            varargout{end+1} = get(this.cl_component, varargin{k}); %#ok<AGROW>
        otherwise
            my_warndlg('clc_label_inc_value/get : Invalid property name', 0);
    end
end
