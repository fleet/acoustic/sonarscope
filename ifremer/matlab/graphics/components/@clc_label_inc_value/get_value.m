% Lecture de la valeur dans l'instance
% 
% Syntax
%   value = get_value(a)
%
% Input Arguments
%   a  : instance de clc_label_inc_value
%
% Output Arguments
%   value : Valeur 
%
% Examples
%    v = clc_label_inc_value; 
%    set_value(v, 3.14)
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_value.m,v 1.4 2003/04/14 16:28:07 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    16/09/2003 - JMA - creation
% ----------------------------------------------------------------------------

function value = get_value(this)

% Maintenance Auto : try
    
    value = get(this.c_editValue, 'value');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_inc_value', 'set_value', lasterr);
% Maintenance Auto : end
