% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.5 2003/09/15 16:56:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    07/02/2001 - DCF - creation
%    08/02/2001 - DCF - utilisation de clc_edit_texte pour le titre et l unite
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
 
    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('nbColTitre  <-> %d', this.nbColTitre);
    str{end+1} = sprintf('nbColValue  <-> %d', this.nbColValue);
    str{end+1} = sprintf('nbColUnite  <-> %d', this.nbColUnite);
    
    str{end+1} = sprintf('--- aggregation c_editValue ---');
    str{end+1} = char(this.c_editValue);
    
    str{end+1} = sprintf('--- aggregation c_titre ---');
    str{end+1} = char(this.c_titre);
    
    str{end+1} = sprintf('--- aggregation c_unite ---');
    str{end+1} = char(this.c_unite);
    
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_inc_value', 'char_instance', lasterr);
% Maintenance Auto : end
