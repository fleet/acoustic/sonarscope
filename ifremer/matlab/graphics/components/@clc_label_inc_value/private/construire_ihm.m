% Construction de base de l IHM
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   this : instance de clc_label_inc_value
%
% See also clc_label_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation de la frame du composant

this = creer_frame_globale(this);
tab_uicontrols = get_tab_uicontrols(this.globalFrame);

%% Creation des composants

Tag = [get(this, 'componentName'), ' ', 'clc_edit_texte titre'];
this.c_titre     = set_name(this.c_titre, Tag);
this.c_titre     = set_handleAnchor(this.c_titre, tab_uicontrols(1));

Tag = [get(this, 'componentName'), ' ', 'clc_edit_value'];
this.c_editValue = set_name(this.c_editValue, Tag);
this.c_editValue = set_handleAnchor(this.c_editValue, tab_uicontrols(2));

Tag = [get(this, 'componentName'), ' ', 'clc_edit_texte unite'];
this.c_unite     = set_name(this.c_unite, Tag);
this.c_unite     = set_handleAnchor(this.c_unite, tab_uicontrols(3));

InsetX = get(this.cl_component, 'componentInsetX');
InsetY = get(this.cl_component, 'componentInsetY');
this.c_titre     = set_inset_x(this.c_titre, InsetX);
this.c_titre     = set_inset_y(this.c_titre, InsetY);
this.c_editValue = set_inset_x(this.c_editValue, InsetX);
this.c_editValue = set_inset_y(this.c_editValue, InsetY);
this.c_unite     = set_inset_x(this.c_unite, InsetX);
this.c_unite     = set_inset_y(this.c_unite, InsetY);

this.c_titre     = editobj(this.c_titre);
this.c_editValue = editobj(this.c_editValue);
this.c_unite     = editobj(this.c_unite);

%% Positionnement de l'ensemble

this = positionner(this);

%% Mise � jour de l'accessibilit�

if strcmp(get(this, 'componentEnable'), 'off')
    this.globalFrame = set(this.globalFrame,    'Enable',           'off');
    this.c_titre     = set(this.c_titre,        'componentEnable', 'off');
    this.c_editValue = set(this.c_editValue,    'componentEnable', 'off');
    this.c_unite     = set(this.c_unite,        'componentEnable', 'off');
end

%% Mise � jour de la visibilit�

if strcmp(get(this, 'componentVisible'), 'off')
    this.globalFrame = set(this.globalFrame,    'Visible',          'off');
    this.c_titre     = set(this.titre,          'componentVisible', 'off');
    this.c_editValue = set(this.editValue,      'componentVisible', 'off');
    this.c_unite     = set(this.unite,          'componentVisible', 'off');
end
