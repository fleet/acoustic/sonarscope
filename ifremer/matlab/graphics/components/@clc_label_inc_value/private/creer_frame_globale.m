% Creation de la frame globale du composant
% 
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% See also clc_label_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

ihm = [];

%% Ancre du composant clc_edit_texte titre

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = [get(this, 'componentName'), ' ', 'clc_edit_texte titre'];

%% Ancre du composant clc_edit_value

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = [get(this, 'componentName'), ' ', 'clc_edit_value'];

%% Ancre du composant clc_edit_texte unite

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = [get(this, 'componentName'), ' ', 'clc_edit_texte unite'];

%% Cr�ation des frames

this.globalFrame = cl_frame(...
    'handleFrame' , get(this, 'componentAnchor'), ...
    'InsetX'      , get(this.cl_component, 'componentInsetX'), ...
    'InsetY'      , get(this.cl_component, 'componentInsetY'), ...
    'MaxLig'      , 1, ...
    'MaxCol'      , 1);
this.globalFrame = set(this.globalFrame, 'ListeUicontrols', ihm);
