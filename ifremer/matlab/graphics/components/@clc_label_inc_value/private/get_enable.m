% Retourne l accessibilite de la valeur du composant
% 
% Syntax
%   enable = get_enable(this)
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   enable   : accessibilite de la valeur du composant
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: get_enable.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function enable = get_enable(this)

% Maintenance Auto : try
    enable = get(this.c_editValue, 'componentEnable');
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_inc_value', 'get_enable', lasterr);
% Maintenance Auto : end
