% Retourne le nombre de colonnes occupees par la valeur
% 
% Syntax
%   nbColValue = get_nb_col_value(this)
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   nbColValue : nombre de colonnes occupees par la valeur
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: get_nb_col_value.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbColValue = get_nb_col_value(this)

% Maintenance Auto : try
    nbColValue = this.nbColValue;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_label_inc_value', 'get_nb_col_value', lasterr);
% Maintenance Auto : end
