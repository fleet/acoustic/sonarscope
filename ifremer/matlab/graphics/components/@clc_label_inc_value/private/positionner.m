% Positionne les elements du composant
%
% Syntax
%   this = positionner(this)
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% See also clc_label_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = positionner(this)

colMax = 0;

%% Lecture des handles des differents elements du composant

handles = get(this.globalFrame, 'ListeUicontrols');

%% Indique le nombre maximum de colonnes

this.globalFrame = set(this.globalFrame, ...
    'MaxCol', this.nbColTitre + this.nbColValue + this.nbColUnite , ...
    'MaxLig', 1);

%% Positionne le titre

if this.nbColTitre > 0
    colMin = colMax + 1;
    colMax = colMin + this.nbColTitre - 1;
    col    = colMin:colMax;
    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 1, 1, col);
    move(this.globalFrame, handles(1), 1, col);
    %drawnow
    set(handles(1), 'visible', 'on');
    this.c_titre = set(this.c_titre, 'componentVisible', 'on');
    this.c_titre = update(this.c_titre);
else
    set(handles(1), 'visible', 'off');
    this.c_titre = set(this.c_titre, 'componentVisible', 'off');
end

%% Positionne la frame du composant

if this.nbColValue > 0
    colMin = colMax + 1;
    colMax = colMin + this.nbColValue - 1;
    col    = colMin:colMax;
    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 2, 1, col);
    move(this.globalFrame, handles(2), 1, col);
    %drawnow
    set(handles(2), 'visible', 'on');
    this.c_editValue = set(this.c_editValue, 'componentVisible', 'on');
    this.c_editValue = update(this.c_editValue);

else
    set(handles(2), 'visible', 'off');
    this.c_editValue = set(this.c_editValue, 'componentVisible', 'off');
end

%% Positionne l'unite

if this.nbColUnite > 0
    colMin = colMax + 1;
    colMax = colMin + this.nbColUnite - 1;
    col    = colMin:colMax;
    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 3, 1, col);
    move(this.globalFrame, handles(3), 1, col);
    %drawnow
    set(handles(3), 'visible', 'on');
    this.c_unite = set(this.c_unite, 'componentVisible', 'on');
    this.c_unite = update(this.c_unite);
else
    set(handles(3), 'visible', 'off');
    this.c_unite = set(this.c_unite, 'componentVisible', 'off');
end
