% Initialisation de l accessibilite du composant
% 
% Syntax
%   this = set_cenable(this, enable)
%
% Input Arguments
%   this   : instance de clc_label_inc_value
%   enable : accessibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_cenable.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_cenable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    this.c_titre      = set(this.c_titre,       'componentEnable', enable);
    this.c_unite      = set(this.c_unite,       'componentEnable', enable);
    this.c_editValue  = set(this.c_editValue,   'componentEnable', enable);
    this.globalFrame  = set(this.globalFrame,   'Enable',          enable);
    this.cl_component = set(this.cl_component,  'componentEnable', enable);
else
    my_warndlg('clc_label_inc_value/set_cenable : Invalid value', 0);
end
