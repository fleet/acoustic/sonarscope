% Initialisation de la visibilite du composant
% 
% Syntax
%   instance = set_cvisible(instance, visible)
%
% Input Arguments
%   instance : instance de clc_label_inc_value
%   visible  : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   instance : instance de clc_label_inc_value initialisee
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_cvisible.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_cvisible(instance, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    instance.c_titre      = set(instance.c_titre,       'componentVisible', visible);
    instance.c_unite      = set(instance.c_unite,       'componentVisible', visible);
    instance.c_editValue  = set(instance.editValue,     'componentVisible', visible);
    instance.globalFrame  = set(instance.globalFrame,   'Visible',          visible);
    instance.cl_component = set(instance.cl_component,  'componentVisible', visible);
else
    my_warndlg('clc_label_inc_value/set_cvisible : Invalid value', 0);
end
