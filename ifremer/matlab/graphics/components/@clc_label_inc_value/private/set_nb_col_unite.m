% Initialisation du nombre de colonnes de l unite
% 
% Syntax
%   this = set_nb_col_unite(this, nbColUnite)
%
% Input Arguments
%   this       : instance de clc_label_inc_value
%   nbColUnite : nombre de colonnes de l unite
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_nb_col_unite.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_nb_col_unite(this, nbColUnite)

isOk = 1;

% --------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - ne doit pas etre une matrice

if ~isnumeric(nbColUnite)
    my_warndlg('clc_label_inc_value/set_nb_col_unite : Invalid format', 0);
    isOk = 0;
end
[m, n] = size(nbColUnite);
if (m*n) > 1
    my_warndlg('clc_label_inc_value/set_nb_col_unite : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.nbColUnite = nbColUnite;
else
    this.nbColUnite = 0;
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l'affichage

if is_edit(this)
    this = positionner(this);
end
