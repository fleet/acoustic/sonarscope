% Initialisation du titre
% 
% Syntax
%   this = set_titre(this, titre)
%
% Input Arguments
%   this  : instance de clc_label_inc_value
%   titre : titre
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_titre.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_titre(this, titre)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(titre)
    my_warndlg('clc_label_inc_value/set_titre : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.c_titre = set(this.c_titre, 'texte', titre);
else
    this.c_titre = set(this.c_titre, 'texte', '');
end
