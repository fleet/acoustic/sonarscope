% Initialisation de l'unite de la valeur
% 
% Syntax
%   instance = set_unite(instance, unite)
%
% Input Arguments
%   instance : instance de clc_label_inc_value
%   unite    : unite
%
% Output Arguments
%   instance : instance de clc_label_inc_value initialisee
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_unite.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_unite(instance, unite)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(unite)
    my_warndlg('clc_label_inc_value/set_unite : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    instance.c_unite = set(instance.c_unite, 'texte', unite);
else
    instance.c_unite = set(instance.c_unite, 'texte', '');
end
