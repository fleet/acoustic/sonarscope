% Traite l action de click sur le titre
%
% Syntax
%   this = traiter_action(this) 
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% Examples
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: traiter_action.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = traiter_action (this)

% Maintenance Auto : try
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_label_inc_value', 'traiter_action', lasterr);
% Maintenance Auto : end
