% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   this = traiter_moins(this) 
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% Examples
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: traiter_moins.m,v 1.3 2003/04/14 16:57:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = traiter_moins(this)

% Maintenance Auto : try
    
    % ----------------------------------------------------
    % Lecture a partir de l IHM et initialisation du texte
    
    if is_edit(this)
        this.c_editValue = gerer_callback(this.c_editValue, get(this, 'msgMoins'));
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_inc_value', 'traiter_moins', lasterr);
% Maintenance Auto : end
