% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   this = traiter_plus(this) 
%
% Input Arguments
%   this : instance de clc_label_inc_value
%
% Output Arguments
%   this : instance de clc_label_inc_value initialisee
%
% Examples
%
% See also clc_label_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_plus(this)

if is_edit(this)
    this.c_editValue = gerer_callback(this.c_editValue, get(this, 'msgPlus'));
end
