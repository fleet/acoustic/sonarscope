% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : instance de clc_label_inc_value
%
% Name-Value Pair Arguments
%   cf. clc_label_inc_value
%
% Output Arguments
%   b : instance de clc_label_inc_value initialisee
%
% Examples
%    v = clc_label_inc_value;
%    set(v, 'value', 3.14, 'tooltip', 'PI')
%
% See also clc_label_inc_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

%% Lecture des arguments

[varargin, titre] = getPropertyValue(varargin, 'titre', []);
[varargin, unite] = getPropertyValue(varargin, 'unite', []);
[varargin, nbColTitre] = getPropertyValue(varargin, 'nbColTitre', []);
[varargin, nbColValue] = getPropertyValue(varargin, 'nbColValue', []);
[varargin, nbColIncValue] = getPropertyValue(varargin, 'nbColIncValue', []);
[varargin, nbColUnite] = getPropertyValue(varargin, 'nbColUnite', []);
[varargin, enable] = getPropertyValue(varargin, 'enable', []);
[varargin, cvisible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, cenable] = getPropertyValue(varargin, 'componentEnable', []);

[varargin, value] = getPropertyValue(varargin, 'value', []);
[varargin, increment] = getPropertyValue(varargin, 'increment', 1);
[varargin, format] = getPropertyValue(varargin, 'format', []);
[varargin, minValue] = getPropertyValue(varargin, 'minValue', []);
[varargin, maxValue] = getPropertyValue(varargin, 'maxValue', []);
[varargin, tag] = getPropertyValue(varargin, 'tag', []);
[varargin, tooltip] = getPropertyValue(varargin, 'tooltip', []);
[varargin, editable] = getPropertyValue(varargin, 'editable', []);
[varargin, actionnable] = getPropertyValue(varargin, 'actionnable', []);
[varargin, msgEdit] = getPropertyValue(varargin, 'msgEdit', []);
[varargin, msgAction] = getPropertyValue(varargin, 'msgAction', []);
[varargin, msgPlus] = getPropertyValue(varargin, 'msgPlus', []);
[varargin, msgMoins] = getPropertyValue(varargin, 'msgMoins', []);

%% Initialisation des attributs

if ~isempty(titre)
    this = set_titre(this, titre);
end
if ~isempty(unite)
    this = set_unite(this, unite);
end
if ~isempty(nbColTitre)
    this = set_nb_col_titre(this, nbColTitre);
end
if ~isempty(nbColValue)
    this.c_editValue = set(this.c_editValue, 'nbColValue', nbColValue);
end
if ~isempty(nbColIncValue)
    this = set_nb_col_value(this, nbColIncValue);
end
if ~isempty(nbColUnite)
    this = set_nb_col_unite(this, nbColUnite);
end
if ~isempty(cvisible)
    this = set_cvisible(this, cvisible);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end
if ~isempty(cenable)
    this = set_cenable(this, cenable);
end

if ~isempty(value)
    this.c_editValue = set_value(this.c_editValue, value);
end
if ~isempty(increment)
    this.c_editValue = set_increment(this.c_editValue, increment);
end
if ~isempty(format)
    this.c_editValue = set_format(this.c_editValue, format);
end
if ~isempty(minValue)
    this.c_editValue = set_minValue(this.c_editValue, minValue);
end
if ~isempty(maxValue)
    this.c_editValue = set_maxValue(this.c_editValue, maxValue);
end
if ~isempty(tag)
    this.c_editValue = set(this.c_editValue, 'tag', tag);
    %this.c_editValue = set_tag(this.c_editValue, tag);
end
if ~isempty(tooltip)
    this.c_editValue = set(this.c_editValue, 'tooltip', tooltip);
    %this.c_editValue = set_tooltip(this.c_editValue, tooltip);
end
if ~isempty(editable)
    this.c_editValue = set(this.c_editValue, 'editable', editable);
    %this.c_editValue = set_editable(this.c_editValue, editable);
end
if ~isempty(actionnable)
    this.c_titre = set(this.c_titre, 'actionnable', actionnable);
    %this.c_titre = set_actionnable(this.c_titre, actionnable);
end
if ~isempty(msgEdit)
    this.c_editValue = set(this.c_editValue, 'msgEdit', msgEdit);
    %this.c_editValue = set_msgEdit(this.c_editValue, msgEdit);
end
if ~isempty(msgPlus)
    this.c_editValue = set(this.c_editValue, 'msgPlus', msgPlus);
    %this.c_editValue = set_msgPlus(this.c_editValue, msgPlus);
end
if ~isempty(msgEdit)
    this.c_editValue = set(this.c_editValue, 'msgMoins', msgMoins);
    %this.c_editValue = set_msgMoins(this.c_editValue, msgMoins);
end
if ~isempty(msgAction)
    this.c_titre = set(this.c_titre, 'msgAction', msgAction);
    %this.c_titre = set_msgAction(this.c_titre, msgAction);
end

%% Traitement des message de la super classe

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

%% Traitement specifique des composants agreges

if ~is_edit(this)
    this.c_editValue = set(this.c_editValue, ...
        'componentName',       [get(this,'componentName'),' ','clc_edit_value'], ...
        'componentUserName',    get(this, 'componentUserName'), ...
        'componentUserCb',      get(this, 'componentUserCb'));
    this.c_titre = set(this.c_titre, ...
        'componentName',       [get(this,'componentName'),' ','clc_edit_texte titre'], ...
        'componentUserName',    get(this, 'componentUserName'), ...
        'componentUserCb',      get(this, 'componentUserCb'));
    this.c_unite = set(this.c_unite, ...
        'componentName',       [get(this,'componentName'),' ','clc_edit_texte unite'], ...
        'componentUserName',    get(this, 'componentUserName'), ...
        'componentUserCb',      get(this, 'componentUserCb'));
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
