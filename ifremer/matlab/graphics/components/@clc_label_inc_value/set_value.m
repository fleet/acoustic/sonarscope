% Acces en ecriture des proprietes d'une instance
% 
% Syntax
%   a = set_value(a, value)
%
% Input Arguments
%   a     : instance de clc_label_inc_value
%   value : Valeur 
%
% Output Arguments
%   a : instance de clc_label_inc_value initialisee
%
% Examples
%    v = clc_label_inc_value; 
%    set_value(v, 3.14)
%
% See also clc_label_inc_value Authors
% Authors : DCF
% VERSION  : $Id: set_value.m,v 1.4 2003/04/14 16:28:07 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_value(this, value)

this.c_editValue = set_value(this.c_editValue, value);

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
