% Representation textuelle d un tableau d instances de clc_label_value2
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance ou tableau d instance de clc_label_value2
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%    char(clc_label_value2)
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.4 2003/03/26 12:50:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    % -------------------------
    % Intialisation des locales
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'char', lasterr);
% Maintenance Auto : end
