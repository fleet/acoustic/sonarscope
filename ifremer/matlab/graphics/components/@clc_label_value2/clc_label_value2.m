% Constructeur de clc_label_value2 (Composant d'edition de valeur avec zone descriptive de texte)
%
% Syntax
%   this = clc_label_value2(...)
% 
% Name-Value Pair Arguments
%   titre             : texte de titre precisant la variable ou le nom de la valeur
%   unite             : texte d unite precisant l unite de la valeur
%   nbColTitre        : nombre de colonnes occupees par le titre
%   nbColValue        : nombre de colonnes occupees par la valeur
%   nbColUnite        : nombre de colonnes occupees par l unite
%   value             : valeur reelle
%   format            : format d affichage de la valeur
%   minValue          : valeur minimale admissible
%   maxValue          : valeur maximale admissible
%   tag               : tag du label d affichage de la valeur
%   tooltip           : info-bulle du label d affichage de la valeur
%   editable          : flag indiquant si la valeur est editable
%   actionnable       : flag indiquant si le titre est cliquable
%   msgEdit           : msg de call back en cas d edition de la valeur
%   msgAction         : msg de call back en cas de click du titre
%   enable            : positionne l accessibilite de la valeur
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%   componentInsetX   : marge en X du composant (en pixels)
%   componentInsetY   : marge en Y du composant (en pixels)
%
% Output Arguments 
%   this          : instance de clc_label_value2
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab.
%    cette version, tres bas niveau, permet d optimiser l utilisation
%    du composant
%
% Examples
%    v = clc_label_value2
%
% See also clc_label_value2 cla_label_value2 Authors
% Authors : DCF
% ----------------------------------------------------------------------------


function this = clc_label_value2(varargin)

% --------------------------------------------------------------------------
% Definition de la structure et initialisation
%
% globalFrame : instance de cl_frame, frame globale du composant
% titre       : titre du composant
% unite       : unite de la valeur presentee
% value       : valeur
% format      : format de presentation de la valeur
% minValue    : minimum admissible de la valeur
% maxValue    : maximum admissible de la valeur
% tooltip     : tooltip de la valeur
% editable    : flag precisant si la valeur est editable
% actionnable : flag precisant si le titre est clicable
% msgEdit     : message d edition de la valeur
% msgAction   : message d action sur le titre
% enable      : accessibilite de la valeur
% nbColTitre  : nombre de colonnes occupees par le titre
% nbColValue  : nombre de colonnes occupees par la valeur
% nbColUnite  : nombre de colonnes occupees par l unite
%
% hUicTitre   : handle de l uicontrol de titre
% hUicValue   : handle de l uicontrol de valeur
% hUicUnite   : handle de l uicontrol de l unite

this.globalFrame          = [];
this.titre                = '';
this.unite                = '';
this.value                = [];
this.format               = '%f';
this.minValue             = -inf;
this.maxValue             = +inf;
this.tooltip              = '';
this.editable             = 1;
this.actionnable          = 0;
this.msgEdit              = 'clc_label_value2 edit';
this.msgAction            = 'clc_label_value2 action';
this.enable               = 'on';
this.nbColTitre           =  1;
this.nbColValue           =  1;
this.nbColUnite           =  1;

this.hUicTitre            = [];
this.hUicValue            = [];
this.hUicUnite            = [];
this.handleAnchor       = [];   % Internal property

%% Super-classe

% if (nargin == 1) && isempty(varargin{1})
%     composant = cl_component([]);
% else
    composant = cl_component([]);
% end

%% Cr�ation de l'objet

this = class(this, 'clc_label_value2', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return;
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
