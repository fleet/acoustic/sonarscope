% Destruction graphique du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_label_value2
%
% Remarks : doit etre surchargee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    15/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------
    % Destruction si l'instance a une representation graphique
    
    if is_edit(this)
        
        % ----------------------------------------
        % Destruction graphique de la super classe
        
        this.cl_component = delete(this.cl_component);
        
        % -----------------------------------
        % Destruction des elements graphiques
        
        if ~isempty(this.globalFrame)
            this.globalFrame = delete(this.globalFrame);
            this.globalFrame = [];
            this.hUicTitre   = [];
            this.hUicValue   = [];
            this.hUicUnite   = [];
        end
        
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'delete', lasterr);
% Maintenance Auto : end
