% Appel d'une representation graphique de l'instance de clc_label_value2
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_label_value2
%
% OUPUT PARAMETERS :
%   this : instance de clc_label_value2
%
% See also clc_label_value2 Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

if ~is_edit(this)
    
    %% Cr�ation de l interface graphique
    
    this.cl_component = editobj(this.cl_component, 'handleAnchor', this.handleAnchor);
    this = construire_ihm(this);
    
    %% Acces aux handles : permet une gestion plus rapide des modifications
    
    handles = get(this.globalFrame, 'ListeUicontrols');
    this.hUicTitre = handles(1);
    this.hUicValue = handles(2);
    this.hUicUnite = handles(3);
end
