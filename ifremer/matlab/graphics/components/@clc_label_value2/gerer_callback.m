% Methode de gestion des call back du composant
%
% Syntax
%   this = gerer_callback(this, cbName, ...)
% 
% Input Arguments
%   this   : instance de clc_label_value2
%   cbName : nom de la callback
%
% Name-Value Pair Arguments
%   
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: gerer_callback.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

if length(varargin) == 1
    switch varargin{1}
        case get(this, 'msgEdit')
            this = traiter_edit(this);
        case get(this, 'msgAction')
            this = traiter_action(this);
        otherwise
            my_warndlg('clc_label_value2/gerer_callback : Invalid message', 1);
    end

else
    my_warndlg('clc_label_value2/gerer_callback : Invalid arguments number', 1);
end
