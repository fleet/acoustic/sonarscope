% Lecture des attributs et des proprietes de l'instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de clc_label_value2
%
% Name-Value Pair Arguments
%   titre             : texte de titre precisant la variable
%   unite             : texte d unite precisant l unite
%   nbColTitre        : nombre de colonnes occupees par le titre
%   nbColValue        : nombre de colonnes occupees par la valeur
%   nbColUnite        : nombre de colonnes occupees par l unite
%   value             : valeur reelle
%   format            : format d affichage de la valeur
%   minValue          : valeur minimale admissible
%   maxValue          : valeur maximale admissible
%   tooltip           : tooltip du label d affichage de la valeur
%   editable          : flag indiquant si la valeur est editable
%   actionnable       : flag indiquant si le titre est cliquable
%   msgEdit           : msg de call back en cas d edition de la valeur
%   msgAction         : msg de call back en cas de click sur le titre
%   enable            : positionne l accessibilite de la valeur
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant
%   componentEnable   : accessibilite du composant
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    v = clc_label_value2; 
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also clc_label_value2 Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'titre'
            varargout{end+1} = this.titre; %#ok<AGROW>
        case 'unite'
            varargout{end+1} = this.unite; %#ok<AGROW>
        case 'nbColTitre'
            varargout{end+1} = this.nbColUnite; %#ok<AGROW>
        case 'nbColValue'
            varargout{end+1} = this.nbColValue; %#ok<AGROW>
        case 'nbColUnite'
            varargout{end+1} = this.nbColUnite; %#ok<AGROW>
        case 'enable'
            varargout{end+1} = this.enable; %#ok<AGROW>
        case 'value'
            varargout{end+1} = this.value; %#ok<AGROW>
        case 'format'
            varargout{end+1} = this.format; %#ok<AGROW>
        case 'minValue'
            varargout{end+1} = this.minValue; %#ok<AGROW>
        case 'maxValue'
            varargout{end+1} = this.maxValue; %#ok<AGROW>
        case 'tooltip'
            varargout{end+1} = this.tooltip; %#ok<AGROW>
        case 'editable'
            varargout{end+1} = this.editable; %#ok<AGROW>
        case 'actionnable'
            varargout{end+1} = this.actionnable; %#ok<AGROW>
        case 'msgEdit'
            varargout{end+1} = this.msgEdit; %#ok<AGROW>
        case 'msgAction'
            varargout{end+1} = this.msgAction; %#ok<AGROW>
        case{'componentVisible', 'componentEnable', 'componentName', ...
                'componentAnchor' , 'componentUserName', 'componentUserCb', ...
                'componentInsetX', 'componentInsetY'}
            varargout{end+1} = get(this.cl_component, varargin{i}); %#ok<AGROW>
        otherwise
            my_warndlg('clc_label_value2/get : Invalid property name', 0);
    end
end
