% Retourne la capacite de cliquer sur le titre
% 
% Syntax
%   actionnable = get_actionnable(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   actionnable    : actionnable
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_actionnable.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function actionnable = get_actionnable(this)

% Maintenance Auto : try
    actionnable = this.actionnable;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_actionnable', lasterr);
% Maintenance Auto : end
