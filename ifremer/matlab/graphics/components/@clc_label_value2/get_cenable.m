% Retourne l'accessibilite du composant
% 
% Syntax
%   enable = get_cenable(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   enable    : enable
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_cenable.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function enable = get_cenable(this)

% Maintenance Auto : try
    enable = get_enable(this.cl_component);
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_cenable', lasterr);
% Maintenance Auto : end
