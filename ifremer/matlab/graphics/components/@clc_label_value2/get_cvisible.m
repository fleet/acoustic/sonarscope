% Retourne la visibilite du composant
% 
% Syntax
%   visible = get_cvisible(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   visible    : visible
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_cvisible.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function visible = get_cvisible(this)

% Maintenance Auto : try
    visible = get_visible(this.cl_component);
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_cvisible', lasterr);
% Maintenance Auto : end
