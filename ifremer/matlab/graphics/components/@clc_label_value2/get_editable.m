% Retourne la capacite d'edition de la valeur
% 
% Syntax
%   editable = get_editable(this)
%
% Input Arguments
%    this : instance de clc_label_value2
%
% Output Arguments
%    editable    : editable
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_editable.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function editable = get_editable(this)

% Maintenance Auto : try
    editable = this.editable;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_editable', lasterr);
% Maintenance Auto : end
