% Retourne l accessibilite de la valeur
% 
% Syntax
%   enable = get_enable(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   enable : enable
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_enable.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function enable = get_enable(this)

% Maintenance Auto : try
    enable = this.enable;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_enable', lasterr);
% Maintenance Auto : end
