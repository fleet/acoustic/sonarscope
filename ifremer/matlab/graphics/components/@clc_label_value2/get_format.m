% Retourne le format d affichage de la valeur
% 
% Syntax
%   format = get_format(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   format    : format
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_format.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function format = get_format(this)

% Maintenance Auto : try
    format = this.format;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_format', lasterr);
% Maintenance Auto : end
