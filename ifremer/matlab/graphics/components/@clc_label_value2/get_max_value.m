% Retourne la valeur maximale
% 
% Syntax
%   maxValue = get_max_value(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   maxValue    : maxValue
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_max_value.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function maxValue = get_max_value(this)

% Maintenance Auto : try
    maxValue = this.maxValue;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_max_value', lasterr);
% Maintenance Auto : end
