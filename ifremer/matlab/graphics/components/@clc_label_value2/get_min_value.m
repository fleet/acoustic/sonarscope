% Retourne la valeur minimale
% 
% Syntax
%   minValue = get_min_value(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   minValue    : minValue
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_min_value.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function minValue = get_min_value(this)

% Maintenance Auto : try
    minValue = this.minValue;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_min_value', lasterr);
% Maintenance Auto : end
