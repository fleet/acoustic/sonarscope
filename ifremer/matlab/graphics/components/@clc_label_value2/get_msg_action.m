% Retourne le message envoye en cas de click sur le titre
% 
% Syntax
%   msgAction = get_msg_action(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   msgAction : msgAction
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_msg_action.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgAction = get_msg_action(this)

% Maintenance Auto : try
    msgAction = this.msgAction;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_msg_action', lasterr);
% Maintenance Auto : end
