% Retourne le message envoye en cas d'edition de la valeur
% 
% Syntax
%   msgEdit = get_msg_edit(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   msgEdit    : msgEdit
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_msg_edit.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgEdit = get_msg_edit(this)

% Maintenance Auto : try
    msgEdit = this.msgEdit;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_msg_edit', lasterr);
% Maintenance Auto : end
