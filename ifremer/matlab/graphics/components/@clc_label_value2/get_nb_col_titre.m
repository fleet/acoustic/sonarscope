% Retourne le nombre de colonnes du titre
% 
% Syntax
%   nbColTitre = get_nb_col_titre(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   nbColTitre    : nbColTitre
%
% See also clc_label_value2
% Authors : DCF
% VERSION  : $Id: get_nb_col_titre.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbColTitre = get_nb_col_titre(this)

% Maintenance Auto : try
    nbColTitre = this.nbColTitre;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_nb_col_titre', lasterr);
% Maintenance Auto : end
