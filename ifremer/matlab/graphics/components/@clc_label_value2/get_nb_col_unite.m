% Retourne le nombre de colonnes de l unite
% 
% Syntax
%   nbColUnite = get_nb_col_unite(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   nbColUnite    : nbColUnite
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_nb_col_unite.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbColUnite = get_nb_col_unite(this)

% Maintenance Auto : try
    nbColUnite = this.nbColUnite;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_nb_col_unite', lasterr);
% Maintenance Auto : end
