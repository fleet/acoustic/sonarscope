% Retourne le nombre de colonnes de la valeur
% 
% Syntax
%   nbColValue = get_nb_col_value(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%    nbColValue    : nbColValue
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_nb_col_value.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbColValue = get_nb_col_value(this)

% Maintenance Auto : try
    nbColValue = this.nbColValue;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_nb_col_value', lasterr);
% Maintenance Auto : end
