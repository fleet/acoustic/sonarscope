% Retourne le titre
% 
% Syntax
%   titre = get_titre(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   titre    : titre
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_titre.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function titre = get_titre(this)

% Maintenance Auto : try
    titre = this.titre;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_titre', lasterr);
% Maintenance Auto : end
