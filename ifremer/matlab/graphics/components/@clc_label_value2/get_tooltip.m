% Retourne l'info-bulle
% 
% Syntax
%   tooltip = get_tooltip(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   tooltip    : info-bulle
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: get_tooltip.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tooltip = get_tooltip(this)

% Maintenance Auto : try
    tooltip = this.tooltip;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_tooltip', lasterr);
% Maintenance Auto : end
