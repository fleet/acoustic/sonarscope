% Retourne la valeur
% 
% Syntax
%   value = get_value(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   value    : value
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function value = get_value(this)

% Maintenance Auto : try
    value = this.value;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'get_value', lasterr);
% Maintenance Auto : end
