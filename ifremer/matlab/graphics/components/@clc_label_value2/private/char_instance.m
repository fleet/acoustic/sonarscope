% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.5 2003/05/02 16:08:34 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    14/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('titre       <-> %s', this.titre);
    str{end+1} = sprintf('unite       <-> %s', this.unite);
    str{end+1} = sprintf('value       <-> %f', this.value);
    str{end+1} = sprintf('format      <-> %s', this.format);
    str{end+1} = sprintf('minValue    <-> %f', this.minValue);
    str{end+1} = sprintf('maxValue    <-> %f', this.maxValue);
    str{end+1} = sprintf('tooltip     <-> %s', this.tooltip);
    str{end+1} = sprintf('editable    <-> %d', this.editable);
    str{end+1} = sprintf('actionnable <-> %d', this.actionnable);
    str{end+1} = sprintf('msgEdit     <-> %s', this.msgEdit);
    str{end+1} = sprintf('msgAction   <-> %s', this.msgAction);
    str{end+1} = sprintf('enable      <-> %s', this.enable);
    str{end+1} = sprintf('nbColTitre  <-> %d', this.nbColTitre);
    str{end+1} = sprintf('nbColValue  <-> %d', this.nbColValue);
    str{end+1} = sprintf('nbColUnite  <-> %d', this.nbColUnite);
    
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'char_instance', lasterr);
% Maintenance Auto : end
