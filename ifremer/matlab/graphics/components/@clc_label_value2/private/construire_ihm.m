% Construction de base de l IHM
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   this : instance de clc_label_value2
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: construire_ihm.m,v 1.3 2003/04/09 14:40:06 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    14/02/2001 - DCF - creation
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

% Maintenance Auto : try
    
    % ---------------------------------
    % Creation de la frame du composant
    
    this = creer_frame_globale(this);
    
    % ----------------------------
    % Positionnement de l ensemble
    
    this = positionner(this);
    
    % ------------------------------
    % Mise a jour de l accessibilite
    
    if strcmp(get(this, 'componentEnable'), 'off')
        this.globalFrame = set_enable(this.globalFrame, 'off');
    end
    
    % ----------------------------
    % Mise a jour de la visibilite
    
    if strcmp(get(this, 'componentVisible'), 'off')
        this.globalFrame = set_visible(this.globalFrame, 'off');
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'construire_ihm', lasterr);
% Maintenance Auto : end
