% Creation de la frame globale du composant
% 
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: creer_frame_globale.m,v 1.3 2003/04/09 14:40:06 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    14/02/2001 - DCF - creation
%    02/03/2001 - DCF - rend les call back non interruptibles
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

% Maintenance Auto : try
    
    ihm      = [];
    
    % ---------------------------------------
    % Ancre du composant clc_edit_texte titre
    ihm{end+1}.Lig  = 1;
    ihm{end}.Col    = 1;
    ihm{end}.Tag    = [get(this, 'componentName'), ' ', 'clc_edit_texte titre'];
    if this.actionnable
        ihm{end}.Style = 'pushbutton';
        if ~isempty(this.msgAction)
            ihm{end}.Callback = built_cb(this, this.msgAction);
        end
    else
        ihm{end}.Style = 'text';
    end
    ihm{end}.String        = this.titre;
    ihm{end}.Interruptible = 'off';
    
    % ---------------------------------
    % Ancre du composant clc_edit_value
    
    ihm{end+1}.Lig = 1;
    ihm{end}.Col = 1;
    ihm{end}.Tag = [get(this, 'componentName'), ' ', 'clc_edit_value'];
    if this.editable
        ihm{end}.Style = 'edit';
        ihm{end}.BackgroundColor = GrisClair;
        if ~isempty(this.msgEdit)
            ihm{end}.Callback = built_cb(this, this.msgEdit);
        end
    else
        ihm{end}.Style = 'text';
    end
    if ~isempty(this.value)
        ihm{end}.String = sprintf(this.format, this.value);
    else
        ihm{end}.String = '';
    end
    ihm{end}.TooltipString = this.tooltip;
    if ~isempty(this.enable)
        ihm{end}.Enable = this.enable;
    end
    ihm{end}.Interruptible = 'off';
    
    % ---------------------------------------
    % Ancre du composant clc_edit_texte unite

    ihm{end+1}.Lig         = 1;
    ihm{end}.Col           = 1;
    ihm{end}.Style         = 'text';
    ihm{end}.Tag           = [get(this, 'componentName'), ' ', 'clc_edit_texte unite'];
    ihm{end}.String        = this.unite;
    ihm{end}.Interruptible = 'off';
    
    % -------------------
    % Creation des frames
    
    this.globalFrame = cl_frame([]);
    this.globalFrame = set_handle_frame(this.globalFrame, get_anchor(this));
    this.globalFrame = set_inset_x(this.globalFrame, get_inset_x(this));
    this.globalFrame = set_inset_y(this.globalFrame, get_inset_y(this));
    this.globalFrame = set_max_lig(this.globalFrame, 1);
    this.globalFrame = set_max_col(this.globalFrame, 1);
    this.globalFrame = set_tab_uicontrols(this.globalFrame, ihm);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'creer_frame_globale', lasterr);
% Maintenance Auto : end
