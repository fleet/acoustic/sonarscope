% Positionne les elements du composant
%
% Syntax
%   this = positionner(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: positionner.m,v 1.3 2003/04/09 14:40:06 augustin Exp $
% ----------------------------------------------------------------------------

function this = positionner(this)

% -------
% Locales

colMin = 0;
colMax = 0;
col    = 0;

% --------------------------------------------------------
% Lecture des handles des differents elements du composant

handles = get_tab_uicontrols(this.globalFrame);

% -------------------------------------
% Indique le nombre maximum de colonnes

this.globalFrame = set_max_col(this.globalFrame, ...
    this.nbColTitre + this.nbColValue + this.nbColUnite);
this.globalFrame = set_max_lig(this.globalFrame, 1);

% -------------------
% Positionne le titre

if this.nbColTitre > 0

    colMin = colMax + 1;
    colMax = colMin + this.nbColTitre - 1;
    col    = colMin:colMax;
    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 1, 1, col);
    move(this.globalFrame, handles(1), 1, col);
    %drawnow
    
    if strcmp(get_cvisible(this), 'off') == 0
        set(handles(1), 'visible', 'on');
    end

else
    set(handles(1), 'visible', 'off');
end

% --------------------------------
% Positionne la frame du composant

if this.nbColValue > 0

    colMin = colMax + 1;
    colMax = colMin + this.nbColValue - 1;
    col    = colMin:colMax;
    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 2, 1, col);
    move(this.globalFrame, handles(2), 1, col);
    %drawnow

    if strcmp(get_cvisible(this), 'off') == 0
        set(handles(2), 'visible', 'on');
    end

else
    set(handles(2), 'visible', 'off');
end

% ------------------
% Positionne l'unite

if this.nbColUnite > 0

    colMin = colMax + 1;
    colMax = colMin + this.nbColUnite - 1;
    col    = colMin:colMax;
    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 3, 1, col);
    move(this.globalFrame, handles(3), 1, col);
    %drawnow

    if strcmp(get_cvisible(this), 'off') == 0
        set(handles(3), 'visible', 'on');
    end

else
    set(handles(3), 'visible', 'off');
end
