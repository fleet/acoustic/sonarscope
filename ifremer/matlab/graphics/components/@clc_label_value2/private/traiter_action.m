% Traitement de l'action en cas de clic sur le titre
%
% Syntax
%   this = traiter_action(this)
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% Examples
%
% See also clc_label_value2 Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_action(this)
