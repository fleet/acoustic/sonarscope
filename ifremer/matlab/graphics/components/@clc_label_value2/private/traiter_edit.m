% Traitement de l'action en cas d'edition de la valeur
%
% Syntax
%   this = traiter_edit(this) 
%
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% Examples
%
% See also clc_label_value2 Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_edit(this)

if is_edit(this)
    value = get(this.hUicValue, 'String');
%     value = str2double(value);
    try
        value = eval(value); % Modifi� par JMA le 16/04/2012 � Madrid pour pouvoir interpr�ter 48+45/60
    catch %#ok<CTCH>
        str1 = sprintf('"%s" est non interpr�table.', value);
        str2 = sprintf('"%s" is not interpretable.', value);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    if isnan(value)
        my_warndlg(Lang('Ceci n''est pas un nombre.', 'This is not a number.'), 1)
        value = 0;
%         return
    end
    this = set_value(this, value);
end
