% Modification des attributs et des proprietes de l'instance
%
% Syntax
%   instance = set(this, ...)
%
% Input Arguments
%    this : instance de clc_label_value2
%
% Name-Value Pair Arguments
%   titre             : texte de titre precisant la variable
%   unite             : texte d unite precisant l unite
%   nbColTitre        : nombre de colonnes occupees par le titre
%   nbColValue        : nombre de colonnes occupees par la valeur
%   nbColUnite        : nombre de colonnes occupees par l unite
%   value             : valeur reelle
%   format            : format d affichage de la valeur
%   minValue          : valeur minimale admissible
%   maxValue          : valeur maximale admissible
%   tooltip           : info-bulle du label d affichage de la valeur
%   editable          : flag indiquant si la valeur est editable
%   actionnable       : flag indiquant si le titre est cliquable
%   msgAction         : msg de call back en cas de click du titre
%   msgEdit           : msg de call back en cas d edition de la valeur
%   enable            : positionne l accessibilite de la valeur
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% Examples
%    v = clc_label_value2;
%    set(v, 'value', 3.14, 'tooltip', 'PI')
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set.m,v 1.5 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    14/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% Maintenance Auto : try

% --------------------------------------------------------------------------
% Lecture des arguments

[varargin, titre] = getPropertyValue(varargin, 'titre', []);
[varargin, unite] = getPropertyValue(varargin, 'unite', []);
[varargin, value] = getPropertyValue(varargin, 'value', []);
[varargin, format] = getPropertyValue(varargin, 'format', []);
[varargin, minValue] = getPropertyValue(varargin, 'minValue', []);
[varargin, maxValue] = getPropertyValue(varargin, 'maxValue', []);
[varargin, tooltip] = getPropertyValue(varargin, 'tooltip', []);
[varargin, editable] = getPropertyValue(varargin, 'editable', []);
[varargin, actionnable] = getPropertyValue(varargin, 'actionnable', []);
[varargin, msgEdit] = getPropertyValue(varargin, 'msgEdit', []);
[varargin, msgAction] = getPropertyValue(varargin, 'msgAction', []);
[varargin, enable] = getPropertyValue(varargin, 'enable', []);
[varargin, nbColTitre] = getPropertyValue(varargin, 'nbColTitre', []);
[varargin, nbColValue] = getPropertyValue(varargin, 'nbColValue', []);
[varargin, nbColUnite] = getPropertyValue(varargin, 'nbColUnite', []);

[varargin, cvisible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, cenable] = getPropertyValue(varargin, 'componentEnable', []);

% --------------------------------------------------------------------------
% Initialisation des attributs

if ~isempty(titre)
    this = set_titre(this, titre);
end
if ~isempty(unite)
    this = set_unite(this, unite);
end
if ~isempty(nbColTitre)
    this = set_nb_col_titre(this, nbColTitre);
end
if ~isempty(nbColValue)
    this = set_nb_col_value(this, nbColValue);
end
if ~isempty(nbColUnite)
    this = set_nb_col_unite(this, nbColUnite);
end
if ~isempty(cvisible)
    this = set_cvisible(this, cvisible);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end
if ~isempty(cenable)
    this = set_cenable(this, cenable);
end
if ~isempty(value)
    this = set_value(this, value);
end
if ~isempty(format)
    this = set_format(this, format);
end
if ~isempty(minValue)
    this = set_min_value(this, minValue);
end
if ~isempty(maxValue)
    this = set_max_value(this, maxValue);
end
if ~isempty(tooltip)
    this = set_tooltip(this, tooltip);
end
if ~isempty(editable)
    this = set_editable(this, editable);
end
if ~isempty(actionnable)
    this = set_actionnable(this, actionnable);
end
if ~isempty(msgEdit)
    this = set_msg_edit(this, msgEdit);
end
if ~isempty(msgAction)
    this = set_msg_action(this, msgAction);
end

% -----------------------------------------
% Traitement des message de la super classe

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
