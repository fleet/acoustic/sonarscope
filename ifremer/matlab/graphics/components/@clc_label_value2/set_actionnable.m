% Initialisation de la possibilite de cliquer sur le titre
% 
% Syntax
%   this = set_actionnable(this, actionnable)
%
% Input Arguments
%   this        : instance de clc_label_value2
%   actionnable : flag indiquant si le titre est cliquable
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_actionnable.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_actionnable(this, actionnable)

isOk = 1;

% ----------------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - doit etre comprise entre 0 et 1 inclus

if ~isnumeric(actionnable)
    my_warndlg('clc_label_value2/set_actionnable : Invalid format', 0);
    isOk = 0;
elseif (actionnable ~= 0) && (actionnable ~= 1)
    my_warndlg('clc_label_value2/set_actionnable : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.actionnable = actionnable;
else
    this.actionnable = 0;
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    if this.actionnable
        set(this.hUicTitre, 'Style', 'pushbutton');
        set(this.hUicTitre, 'Callback', built_cb(this, this.msgAction));
    else
        set(this.hUicTitre, 'Style', 'text');
        set(this.hUicTitre, 'Callback', '');
    end
end
