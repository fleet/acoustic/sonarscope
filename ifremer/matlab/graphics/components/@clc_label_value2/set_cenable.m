% Initialisation de l'accessibilite du composant
% 
% Syntax
%   this = set_cenable(this, enable)
%
% Input Arguments
%   this   : instance de clc_label_value2
%   enable : accessibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_cenable.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_cenable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    if is_edit(this)
        this.globalFrame  = set_enable(this.globalFrame, enable) ;
    end
    this.cl_component = set_enable(this.cl_component, enable);
else
    my_warndlg('clc_label_value2/set_cenable : Invalid value', 0);
end
