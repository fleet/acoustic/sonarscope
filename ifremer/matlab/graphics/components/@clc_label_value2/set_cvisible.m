% Initialisation de la visibilite du composant
% 
% Syntax
%   this = set_cvisible(this, visible)
%
% Input Arguments
%   this    : instance de clc_label_value2
%   visible : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_cvisible.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_cvisible(this, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    if is_edit(this)
        this.globalFrame  = set_visible(this.globalFrame, visible);
        if this.nbColTitre == 0
            set(this.hUicTitre, 'Visible', 'off');
        end
        if this.nbColValue == 0
            set(this.hUicValue, 'Visible', 'off');
        end
        if this.nbColUnite == 0
            set(this.hUicUnite, 'Visible', 'off');
        end
    end
    this.cl_component = set_visible(this.cl_component, visible);
else
    my_warndlg('clc_label_value2/set_cvisible : Invalid value', 0);
end
