% Initialisation de la possibilite d'editer la valeur
% 
% Syntax
%   this = set_editable(this, editable)
%
% Input Arguments
%   this     : instance de clc_label_value2
%   editable : flag indiquant si la valeur est editable
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_editable.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_editable(this, editable)

isOk = 1;

% ----------------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - doit etre comprise entre 0 et 1 inclus

if ~isnumeric(editable)
    my_warndlg('clc_label_value2/set_editable : Invalid format', 0);
    isOk = 0;
elseif (editable ~= 0) && (editable ~= 1)
    my_warndlg('clc_label_value2/set_editable : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.editable = editable;
else
    this.editable = 0;
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    if this.editable
        set(this.hUicValue, 'Style', 'edit', 'BackgroundColor', GrisClair);
        set(this.hUicValue, 'Callback', built_cb(this, this.msgEdit));
    else
        set(this.hUicValue, 'Style', 'text', 'BackgroundColor', Gris);
        set(this.hUicValue, 'Callback', '');
    end
end
