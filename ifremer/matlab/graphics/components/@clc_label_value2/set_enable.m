% Initialisation de l accessibilite de la valeur du composant
% 
% Syntax
%   instance = set_enable(instance, enable)
%
% Input Arguments
%   instance : instance de clc_label_value2
%   enable   : accessibilite de la valeur du composant 'on' | 'off'
%
% Output Arguments
%   instance : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = set_enable (instance, enable)

isOk = 1 ;

% --------------------------------------------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres
% - doit etre 'on' ou 'off'

if ~ischar(enable)
    my_warndlg('clc_label_value2/set_enable : Invalid format', 0);
    isOk = 0 ;
elseif ((strcmp (enable, 'on') == 0) && (strcmp (enable, 'off') == 0))
    my_warndlg('clc_label_value2/set_enable : Invalid value', 0);
    isOk = 0 ;
end

% ---------------------
% Si ok, initialisation

if isOk
    instance.enable = enable ;
else
    instance.enable = 'on' ;
end

% ---------------------
% Mise a jour graphique

if is_edit(instance)
    set (instance.hUicValue, 'Enable', instance.enable) ;
end
