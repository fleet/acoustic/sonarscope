% Initialisation du format d affichage de la valeur
% 
% Syntax
%   this = set_format(this, format)
%
% Input Arguments
%   this   : instance de clc_label_value2
%   format : format d affichage de la valeur
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_format.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_format(this, format)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(format)
    my_warndlg('clc_label_value2/set_format : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.format = format;
else
    this.format = '%f';
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    this = set_value(this, this.value);
end
