% Initialisation de la valeur maximale admissible
% 
% Syntax
%   this = set_max_value(this, maxValue)
%
% Input Arguments
%   this     : instance de clc_label_value2
%   minValue : valeur maximale admissible
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_max_value.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_max_value(this, maxValue)

isOk = 1;

% -----------------------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - doit etre comprise entre minValue et maxValue

if ~isnumeric(maxValue)
    my_warndlg('clc_label_value2/set_max_value : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.maxValue = maxValue;
else
    this.maxValue = +inf;
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    this = set_value(this, this.value);
end
