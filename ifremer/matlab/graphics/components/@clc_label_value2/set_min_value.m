% Initialisation de la valeur minimale admissible
% 
% Syntax
%   this = set_min_value(this, minValue)
%
% Input Arguments
%   this     : instance de clc_label_value2
%   minValue : valeur minimale admissible
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_min_value.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_min_value(this, minValue)

isOk = 1;

% -----------------------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - doit etre comprise entre minValue et maxValue

if ~isnumeric(minValue)
    my_warndlg('clc_label_value2/set_min_value : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.minValue = minValue;
else
    this.minValue = -inf;
end

% ----------------------
% Mise a jour graphique

if is_edit(this)
    this = set_value(this, this.value);
end
