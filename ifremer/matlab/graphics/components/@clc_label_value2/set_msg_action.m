% Initialisation du message de call back en cas de click sur le titre
% 
% Syntax
%   this = set_msg_action(this, msgAction)
%
% Input Arguments
%   this      : instance de clc_label_value2
%   msgAction : message de la call back
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_msg_action.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_msg_action(this, msgAction)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(msgAction)
    my_warndlg('clc_label_value2/set_msg_action : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.msgAction = msgAction;
else
    this.msgAction = [];
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.hUicValue, 'Callback', built_cb(this, this.msgAction));
end
