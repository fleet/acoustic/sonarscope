% Initialisation du message de call back en cas d'edition de la valeur
% 
% Syntax
%   this = set_msg_edi(this, msgEdit)
%
% Input Arguments
%   this : instance de clc_label_value2
%   msgEdit  : message de la call back
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_msg_edit.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_msg_edit(this, msgEdit)

this.msgEdit = msgEdit;

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.hUicValue, 'Callback', built_cb(this, this.msgEdit));
end
