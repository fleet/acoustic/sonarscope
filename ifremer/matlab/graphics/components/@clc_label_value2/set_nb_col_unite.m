% Initialisation du nombre de colonnes de l unite
% 
% Syntax
%   this = set_nb_col_unite(this, nbColUnite)
%
% Input Arguments
%   this       : instance de clc_label_value2
%   nbColUnite : nombre de colonnes de l unite
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_nb_col_unite.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_nb_col_unite(this, nbColUnite)

isOk = 1;

% --------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - ne doit pas etre une matrice

if ~isnumeric(nbColUnite)
    my_warndlg('clc_label_value2/set_nb_col_unite : Invalid format', 0);
    isOk = 0;
end
[m, n] = size(nbColUnite);
if (m*n) > 1
    my_warndlg('clc_label_value2/set_nb_col_unite : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.nbColUnite = nbColUnite;
else
    this.nbColUnite = 0;
end

% ---------------
% Positionnement

if is_edit(this)
    if (this.nbColTitre ~= 0) || (this.nbColValue ~= 0) || (this.nbColUnite~=0)
        this = positionner(this);
    end
end
