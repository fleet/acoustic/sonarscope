% Initialisation du nombre de colonnes de la valeur
% 
% Syntax
%   this = set_nb_col_value(this, nbColValue)
%
% Input Arguments
%   this       : instance de clc_label_value2
%   nbColValue : nombre de colonnes de la valeur
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_nb_col_value.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_nb_col_value(this, nbColValue)

isOk = 1;

% --------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - ne doit pas etre une matrice

if ~isnumeric(nbColValue)
    my_warndlg('clc_label_value2/set_nb_col_value : Invalid format', 0);
    isOk = 0;
end
[m, n] = size(nbColValue);
if (m*n) > 1
    my_warndlg('clc_label_value2/set_nb_col_value : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.nbColValue = nbColValue;
else
    this.nbColValue = 0;
end

% --------------
% Positionnement

if is_edit(this)
    if (this.nbColTitre ~= 0) || (this.nbColValue ~= 0) || (this.nbColUnite~=0)
        this = positionner(this);
    end
end
