% Initialisation du titre
% 
% Syntax
%   this = set_titre(this, titre)
%
% Input Arguments
%   this  : instance de clc_label_value2
%   titre : titre
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_titre.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_titre(this, titre)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(titre)
    my_warndlg('clc_label_value2/set_titre : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.titre = titre;
else
    this.titre = '';
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    if ~isempty(this.titre)
        set(this.hUicTitre, 'String', this.titre);
    else
        set(this.hUicTitre, 'String', '');
    end
end
