% Initialisation de l'info-bulle de la valeur
% 
% Syntax
%   this = set_tooltip(this, tooltip);
%
% Input Arguments
%   this    : instance de clc_label_value2
%   tooltip : tootip de la valeur
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_tooltip.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_tooltip(this, tooltip)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(tooltip)
    my_warndlg('clc_label_value2/set_tooltip : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.tooltip = tooltip;
else
    this.tooltip = '';
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.hUicValue, 'TooltipString', this.tooltip);
end
