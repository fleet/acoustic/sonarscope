% Initialisation de l unite de la valeur
% 
% Syntax
%   this = set_unite(this, unite)
%
% Input Arguments
%   this  : instance de clc_label_value2
%   unite : unite
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: set_unite.m,v 1.4 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_unite(this, unite)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if isempty(unite)
    return
end

if ~ischar(unite)
    my_warndlg('clc_label_value2/set_unite : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.unite = unite;
else
    this.unite = '';
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    if ~isempty(this.unite)
        set(this.hUicUnite, 'String', this.unite);
    else
        set(this.hUicUnite, 'String', '');
    end
end
