% Initialisation de la valeur
% 
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%   this  : instance de clc_label_value2
%   value : valeur
%
% Output Arguments
%   this : instance de clc_label_value2 initialisee
%
% See also clc_label_value2 Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_value(this, value)

isOk = 1;

isDatetime = isa(value, 'datetime');

if ~isDatetime
    if ~isnumeric(value)
        my_warndlg('clc_label_value2/set_value : Invalid format', 0);
        isOk = 0;
    elseif value < this.minValue
        my_warndlg(['clc_label_value2/set_value : value less than ', num2str(this.minValue)], 0);
        value = this.minValue;
    elseif value > this.maxValue
        my_warndlg(['clc_label_value2/set_value : value greater than ', num2str(this.maxValue)], 0);
        value = this.maxValue;
    end
end

%% Si ok, initialisation

if ~isDatetime
    if isOk
        if strcmp(this.format, '%d')
            this.value = round(value);
        else
            this.value = value;
        end
    else
        this.value = [];
        this = set_enable(this, 'off');
    end
end

%% Mise � jour graphique

if is_edit(this)
    if isDatetime
        set(this.hUicValue, 'String', char(value));
    else
        if ~isempty(this.value) && ishandle(this.hUicValue)
            set(this.hUicValue, 'String', sprintf(this.format, double(this.value)));
        end
    end
end
