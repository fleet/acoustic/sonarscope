% Test graphique du composant
%
% Syntax
%   this = test(this)
% 
% Input Arguments
%   this : instance de clc_label_value2
%
% Output Arguments
%   this : instance de clc_label_value2
% 
% Examples
%    this = test(clc_label_value2)
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    14/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this)

% Maintenance Auto : try
    
    % -------------------------------------------------------
    % Creation d une figure ainsi que de l'ancre du composant
    
    figure;
    h(1) = uicontrol(                          ...
        'Visible'  , 'on'                      , ...
        'Tag'      , 'ancre_clc_label_value2 1' , ...
        'Position' , [20 20 300 30]          );
    h(2) = uicontrol(                          ...
        'Visible'  , 'on'                      , ...
        'Tag'      , 'ancre_clc_label_value2 2' , ...
        'Position' , [20 70 300 30]          );
    h(3) = uicontrol(                          ...
        'Visible'  , 'on'                      , ...
        'Tag'      , 'ancre_clc_label_value2 3' , ...
        'Position' , [20 120 300 30]         );
    h(4) = uicontrol(                          ...
        'Visible'  , 'on'                      , ...
        'Tag'      , 'ancre_clc_label_value2 4' , ...
        'Position' , [20 170 300 30]         );
    h(5) = uicontrol(                          ...
        'Visible'  , 'on'                      , ...
        'Tag'      , 'ancre_clc_label_value2 5' , ...
        'Position' , [20 220 300 30]         );
    
    % --------------------------------------------------------------------------
    % Creation et initialisation du composant
    
    i(1)    = clc_label_value2([]);
    i(2)    = clc_label_value2([]);
    i(3)    = clc_label_value2([]);
    i(4)    = clc_label_value2([]);
    i(5)    = clc_label_value2([]);
    
    i(1) = set_titre(i(1), 'Titre');
    i(1) = set_unite(i(1), 'Unite');
    i(1) = set_nb_col_titre(i(1), 2);
    i(1) = set_nb_col_value(i(1), 1);
    i(1) = set_nb_col_unite(i(1), 1);
    i(1) = set_value(i(1), pi);
    i(1) = set_format(i(1), '%1.2f');
    i(1) = set_min_value(i(1), 3);
    i(1) = set_max_value(i(1), 4);
    i(1) = set_tooltip(i(1), 'Pi');
    i(1) = set_editable(i(1), 0);
    i(1) = set_actionnable(i(1), 1);
    i(1) = set_msg_edit(i(1), 'clc_label_value2 edit 1');
    i(1) = set_msg_action(i(1), 'clc_label_value2 action 1');
    i(1) = set_name(i(1), 'ancre_clc_label_value2 1');
    i(1) = set_user_name(i(1), 'clc_label_value2');
    i(1) = set_user_cb(i(1), 'gerer_callback');
    
    i(2) = set_titre(i(2), 'Titre');
    i(2) = set_unite(i(2), 'Unite');
    i(2) = set_nb_col_titre(i(2), 1);
    i(2) = set_nb_col_value(i(2), 3);
    i(2) = set_nb_col_unite(i(2), 1);
    i(2) = set_value(i(2), pi);
    i(2) = set_format(i(2), '%4.4f');
    i(2) = set_min_value(i(2), 3);
    i(2) = set_max_value(i(2), 4);
    i(2) = set_tooltip(i(2), 'Pi');
    i(2) = set_editable(i(2), 1);
    i(2) = set_actionnable(i(2), 1);
    i(2) = set_msg_edit(i(2), 'clc_label_value2 edit 2');
    i(2) = set_msg_action(i(2), 'clc_label_value2 action 2');
    i(2) = set_name(i(2), 'ancre_clc_label_value2 2');
    i(2) = set_user_name(i(2), 'clc_label_value2');
    i(2) = set_user_cb(i(2), 'gerer_callback');  
    
    i(3) = set_titre(i(3), 'Titre');
    i(3) = set_unite(i(3), 'Unite');
    i(3) = set_nb_col_titre(i(3), 0);
    i(3) = set_nb_col_value(i(3), 1);
    i(3) = set_nb_col_unite(i(3), 1);
    i(3) = set_value(i(3), pi);
    i(3) = set_format(i(3), '%4.2f');
    i(3) = set_min_value(i(3), 3);
    i(3) = set_max_value(i(3), 4);
    i(3) = set_tooltip(i(3), 'Pi');
    i(3) = set_editable(i(3), 1);
    i(3) = set_actionnable(i(3), 0);
    i(3) = set_msg_edit(i(3), 'clc_label_value2 edit 3');
    i(3) = set_msg_action(i(3), 'clc_label_value2 action 3');
    i(3) = set_name(i(3), 'ancre_clc_label_value2 3');
    i(3) = set_user_name(i(3), 'clc_label_value2');
    i(3) = set_user_cb(i(3), 'gerer_callback');  
    
    i(4) = set_titre(i(4), 'Titre');
    i(4) = set_unite(i(4), 'Unite');
    i(4) = set_nb_col_titre(i(4), 1);
    i(4) = set_nb_col_value(i(4), 1);
    i(4) = set_nb_col_unite(i(4), 0);
    i(4) = set_value(i(4), pi);
    i(4) = set_format(i(4), '%4.3f');
    i(4) = set_min_value(i(4), 3);
    i(4) = set_max_value(i(4), 4);
    i(4) = set_tooltip(i(4), 'Pi');
    i(4) = set_editable(i(4), 1);
    i(4) = set_actionnable(i(4), 0);
    i(4) = set_msg_edit(i(4), 'clc_label_value2 edit 4');
    i(4) = set_msg_action(i(4), 'clc_label_value2 action 4');
    i(4) = set_name(i(4), 'ancre_clc_label_value2 4');
    i(4) = set_user_name(i(4), 'clc_label_value2');
    i(4) = set_user_cb(i(4), 'gerer_callback');  
    
    i(5) = set_titre(i(5), 'Titre');
    i(5) = set_unite(i(5), 'Unite');
    i(5) = set_nb_col_titre(i(5), 0);
    i(5) = set_nb_col_value(i(5), 1);
    i(5) = set_nb_col_unite(i(5), 0);
    i(5) = set_value(i(5), pi);
    i(5) = set_format(i(5), '%4.6f');
    i(5) = set_min_value(i(5), 3);
    i(5) = set_max_value(i(5), 4);
    i(5) = set_tooltip(i(5), 'Pi');
    i(5) = set_editable(i(5), 0);
    i(5) = set_actionnable(i(5), 0);
    i(5) = set_msg_edit(i(5), 'clc_label_value2 edit 5');
    i(5) = set_msg_action(i(5), 'clc_label_value2 action 5');
    i(5) = set_name(i(5), 'ancre_clc_label_value2 5');
    i(5) = set_user_name(i(5), 'clc_label_value2');
    i(5) = set_user_cb(i(5), 'gerer_callback');  
    
    this = i;
    
    % -------------------------------
    % Creation graphique du composant
    
    this(1) = editobj(this(1));
    this(2) = editobj(this(2));
    this(3) = editobj(this(3));
    this(4) = editobj(this(4));
    this(5) = editobj(this(5));
    
    % ------------------------
    % Rend le resultat visible
    
    set(gcf, 'Visible', 'on');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'test', lasterr);
% Maintenance Auto : end
