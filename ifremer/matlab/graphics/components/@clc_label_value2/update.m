% Mise a jour graphique du composant
%
% Syntax
%   this = update(this)
% 
% Input Arguments
%   this : instance de clc_label_value2
%
% Remarks : doit etre surchargee
%
% See also clc_label_value2 Authors
% Authors : DCF
% VERSION  : $Id: update.m,v 1.3 2003/04/09 15:39:37 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    14/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = update(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------
    % Mise a jour si l'instance a une representation graphique
    
    if is_edit(this)
        
        % -----------------------------------
        % Update graphique de la super classe
        
        this.cl_component = update(this.cl_component);
        
        % -----------------------------------------------------
        % Update graphique des elements graphiques du composant
        
        if ~isempty(this.globalFrame)
            this.globalFrame = update(this.globalFrame);
            this = positionner(this);
        end
        
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value2', 'update', lasterr);
% Maintenance Auto : end
