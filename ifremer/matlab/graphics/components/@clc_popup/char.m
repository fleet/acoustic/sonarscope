% Representation textuelle d'une instance de clc_popup
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance ou tableau d'instances de clc_popup
%
% Output Arguments
%   str : une chaine de caracteres representant l'instance
%
% Examples
%   char(clc_popup)
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.5 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    13/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    % -------------------------
    % Intialisation des locales
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'char', lasterr);
% Maintenance Auto : end
