% Constructeur de clc_popup (liste deroulante)
%
% Syntax
%   this = clc_popup(...)
%
% Name-Value Pair Arguments
%   texte             : liste des textes du popup (de 1 a n)
%   etat              : etat du composant, (de 1 a n)
%   tooltip           : info-bulle du composant
%   msgClick          : message envoye en cas de click souris
%
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des callback
%                        de la classe utilisatrice du composant
%   componentInsetX   : marge en X du composant (en pixels)
%   componentInsetY   : marge en Y du composant (en pixels)
%
% Output Arguments
%   this : instance de clc_popup
%
% Remarks :
%    ne peut etre employe que sous forme d un composant associe a une
%    this graphique frame matlab.
%    cette version, tres bas niveau, permet d optimiser l utilisation
%    du composant
%
% Examples
%    v = test(clc_popup)
%
% See also clc_popup Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_popup(varargin)


%% D�finition de la structure et initialisation
%
% texte             : liste des textes du popup (de 1 a n)
% etat              : etat du composant, (de 1 a n)
% tooltip           : tooltip du composant
% msgClick          : message envoye en cas de click souris
%
% f_globale         : this de cl_frame, frame globale du composant
% h_uic             : handle d acces rapide a l this graphique matlab

this.texte              = {''};   % External property
this.etat               = 1;   % External property
this.tooltip            = '';   % External property
this.msgClick           = '';   % External property
this.UserData           = [];%'defaultTag_clc_edit_texte';
this.f_globale          = [];   % Internal property
this.h_uic              = [];   % Internal property
this.handleAnchor       = [];   % Internal property

%% Super-classe

% if (nargin == 1) && isempty(varargin {1})
%     composant = cl_component([]);
% else
    composant = cl_component([]);
% end

%% Cr�ation de l'objet

this = class(this, 'clc_popup', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

this = set(this, varargin{:});

