% Destruction graphique du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_popup
%
% Remarks : doit etre surchargee
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    13/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % ----------------------------------------------------
    % Destruction si l this a une representation graphique
    
    if is_edit(this)
        
        % ----------------------------------------
        % Destruction graphique de la super classe
        
        this.cl_component = delete(this.cl_component);
        
        % -----------------------------------
        % Destruction des elements graphiques
        
        if ~isempty(this.f_globale)
            this.f_globale = delete(this.f_globale);
            this.f_globale = [];
            this.h_uic     = [];
        end
        
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'delete', lasterr);
% Maintenance Auto : end
