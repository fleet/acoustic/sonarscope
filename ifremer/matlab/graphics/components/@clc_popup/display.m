% Affichage des donnees d'une instance de clc_popup
%
% Syntax
%   display (this)
%
% Input Arguments 
%   this : instance de clc_popup
%
% Examples
%   display(clc_popup)
%
% See also clc_popup Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
