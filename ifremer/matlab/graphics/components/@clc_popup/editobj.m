% Appel de la representation graphique de l'instance clc_popup (liste deroulante)
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_popup
%
% OUPUT PARAMETERS :
%   this : instance de clc_popup
%
% See also clc_popup Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Cr�ation de l interface graphique

this.cl_component = editobj(this.cl_component, 'handleAnchor', this.handleAnchor);
this = construire_ihm(this);
set(get_anchor(this), 'Visible', 'off');
