% Methode de gestion des callbacks du composant clc_popup
%
% Syntax
%   this = gerer_callback(this, cbName, ...)
% 
% Input Arguments
%   this   : instance de clc_popup
%   cbName : nom de la call back
%
% Name-Value Pair Arguments
%   
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: gerer_callback.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

% --------------------
% Selection du message

if length(varargin) == 1
    switch varargin{1}
        case this.msgClick
            this.etat = get(this.h_uic, 'Value');
        otherwise
            my_warndlg('clc_popup/gerer_callbac : Invalid message', 1);
    end
else
    my_warndlg('clc_popup/gerer_callback : Invalid arguments number', 1);
end
