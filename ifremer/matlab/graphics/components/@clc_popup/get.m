% Lecture des attributs et proprietes de l'instance de clc_popup
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de clc_popup
%
% Name-Value Pair Arguments
%   texte             : liste des textes du popup (de 1 a n)
%   etat              : etat du composant, (de 1 a n)
%   tooltip           : info-bulle du composant
%   msgClick          : message envoye en cas de click souris
%
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant 'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%   componentInsetX   : marges selon x
%   componentInsetY   : marges selon y
%   componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des noms de proprietes indiquees en arguments
% 
% Examples
%    v = clc_popup;
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also clc_popup Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'texte'
            varargout{end+1} = this.texte; %#ok<AGROW>
        case 'etat'
            varargout{end+1} = this.etat; %#ok<AGROW>
        case 'tooltip'
            varargout{end+1} = this.tooltip; %#ok<AGROW>
        case 'msgClick'
            varargout{end+1} = this.msgClick; %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.cl_component, varargin{i}); %#ok<AGROW>
    end
end
