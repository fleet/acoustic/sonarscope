% Renvoie la valeur de l'accessibilite du composant
% 
% Syntax
%   enable = get_cenable(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   enable   : enable
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: get_cenable.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function enable = get_cenable(this)

% Maintenance Auto : try
    enable = get_enable(this.cl_component);
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'get_cenable', lasterr);
% Maintenance Auto : end
