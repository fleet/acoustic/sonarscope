% Renvoie la valeur de la visibilite du composant
% 
% Syntax
%   visible = get_cvisible(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   visible : visible
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: get_cvisible.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function visible = get_cvisible(this)

% Maintenance Auto : try
    visible = get_visible(this.cl_component);
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'get_cvisible', lasterr);
% Maintenance Auto : end
