% Retourne l'etat de selection du composant (de 1 a n)
% 
% Syntax
%   etat = get_etat(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   etat   : etat
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: get_etat.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function etat = get_etat(this)

% Maintenance Auto : try
    etat = this.etat;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'get_etat', lasterr);
% Maintenance Auto : end
