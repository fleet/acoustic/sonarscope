% Retourne le msg renvoye en cas de click sur le composant
% 
% Syntax
%   msgClick = get_msg_click(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   msgClick   : msgClick
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: get_msg_click.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgClick = get_msg_click(this)

% Maintenance Auto : try
    msgClick = this.msgClick;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'get_msg_click', lasterr);
% Maintenance Auto : end
