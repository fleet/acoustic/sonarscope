% Retourne le texte (liste) du composant
% 
% Syntax
%   texte = get_texte(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   texte   : texte sous forme d'une liste
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: get_texte.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function texte = get_texte(this)

% Maintenance Auto : try
    texte = this.texte;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_popup', 'get_texte', lasterr);
% Maintenance Auto : end
