% Retourne l'info-bulle d'un composant clc_popup
% 
% Syntax
%   tooltip = get_tooltip(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   tooltip : l'info-bulle du composant (chaine de caracteres)
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: get_tooltip.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tooltip = get_tooltip (this)

% Maintenance Auto : try
    tooltip = this.tooltip;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'get_tooltip', lasterr);
% Maintenance Auto : end
