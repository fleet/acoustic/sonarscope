% Representation textuelle d'une instance de clc_popup
%
% Syntax
%   str = char_this(this) 
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   str : une chaine de caracteres representative de l'instance
%
% Examples
%
% See also Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

% ----------
% Traitement

str{end+1} = sprintf('texte    <-> %s', SelectionDansListe2str(0, this.texte));
str{end+1} = sprintf('etat     <-> %d', this.etat);
str{end+1} = sprintf('tooltip  <-> %s', this.tooltip);
s = [];

if iscell(this.msgClick)
    for i = 1:length(this.msgClick)
        s = [s ' / ' this.msgClick{i}];
    end
else
    s = this.msgClick;
end
str{end+1} = sprintf('msgClick <-> %s', s);

str{end+1} = sprintf('--- heritage cl_component ---');
str{end+1} = char(this.cl_component);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
