% Construction de base de l'IHM
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   this : instance de clc_popup
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: construire_ihm.m,v 1.3 2003/05/02 16:10:32 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    13/03/2001 - DCF - creation
%    23/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

% Maintenance Auto : try
    
    % ---------------------------------
    % Creation de la frame du composant
    
    this = creer_f_globale(this);
    
    % ------------------------------
    % Mise a jour de l accessibilite
    
    if strcmp(get(this, 'componentEnable'), 'off')
        this.f_globale = set_enable(this.f_globale, 'off');
    end
    
    % ----------------------------
    % Mise a jour de la visibilite
    
    if strcmp(get(this, 'componentVisible'), 'off')
        this.f_globale = set_visible(this.f_globale, 'off');
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'construire_ihm', lasterr);
% Maintenance Auto : end
