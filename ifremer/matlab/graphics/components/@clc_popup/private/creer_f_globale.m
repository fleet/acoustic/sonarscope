% Creation de la frame globale du composant
%
% Syntax
%   this = creer_f_globale(this)
%
% Input Arguments
%   this : instance de clc_popup
%
% Output Arguments
%   this : instance de clc_popup initialisee
%
% See also clc_popup Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_globale(this)

ihm      = [];

% ---------------------------------------
% Ancre du composant clc_edit_texte titre

ihm{end+1}.Lig         = 1;
ihm{end}.Col           = 1;
ihm{end}.Style         = 'popupmenu';
ihm{end}.String        = this.texte;
ihm{end}.Value         = this.etat;
ihm{end}.TooltipString = this.tooltip;
ihm{end}.BackgroundColor = GrisClair;
ihm{end}.Callback      = built_cb(this, this.msgClick);
ihm{end}.UserData      = this.UserData;

% -------------------
% Creation des frames

this.f_globale = cl_frame([]);
this.f_globale = set_handle_frame(  this.f_globale, get_anchor(this));
this.f_globale = set_inset_x(       this.f_globale, get_inset_x(this));
this.f_globale = set_inset_y(       this.f_globale, get_inset_y(this));
this.f_globale = set_max_lig(       this.f_globale, 1);
this.f_globale = set_max_col(       this.f_globale, 1);
this.f_globale = set_tab_uicontrols(this.f_globale, ihm);

% -------------------------------
% Determination des acces rapides

handles        = get_tab_uicontrols(this.f_globale);
this.h_uic = handles(1);
fc = get(this.h_uic, 'ForegroundColor');
set(this.h_uic, 'ForegroundColor', [1 1 1]);
set(this.h_uic, 'ForegroundColor', fc);


