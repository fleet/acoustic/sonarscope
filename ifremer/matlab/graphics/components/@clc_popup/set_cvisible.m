% Initialisation de la visibilite du composant
% 
% Syntax
%   this = set_cvisible(this, visible)
%
% Input Arguments
%   this    : instance de clc_popup
%   visible : visibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_popup initialisee
%
% See also clc_popup
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_cvisible(this, visible)

if ischar(visible)
    if is_edit(this)
        this.f_globale  = set_visible(this.f_globale, visible);
    end
    this.cl_component = set_visible(this.cl_component, visible);
    set(get_anchor(this), 'visible', 'off');
else
    my_warndlg('clc_popup/set_cvisible : Invalid value', 0);
end
