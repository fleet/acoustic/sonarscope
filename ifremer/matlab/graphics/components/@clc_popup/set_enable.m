% Initialisation de l'accessibilite d'un composant clc_popup
% 
% Syntax
%   this = set_cenable (this, enable)
%
% Input Arguments
%   this   : instance de clc_popup
%   enable : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_popup initialisee
%
% See also clc_popup Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    if is_edit(this)
        this.f_globale  = set_enable(this.f_globale, enable);
    end
    this.cl_component = set_enable(this.cl_component, enable);
else
    my_warndlg('clc_popup/set_cenable : Invalid value', 0);
end
