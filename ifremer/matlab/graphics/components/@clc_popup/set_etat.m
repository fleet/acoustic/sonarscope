% Initialisation de l'etat de selection du composant
% 
% Syntax
%   this = set_etat(this, etat);
%
% Input Arguments
%   this : instance de clc_popup
%   etat : etat de (pre)selection du composant
%
% Output Arguments
%   this : instance de clc_popup initialisee
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: set_etat.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_etat(this, etat)

isOk = 1;

% -----------------------------------------
% Test validite des arguments :
% - doit etre une une valeur numerique >= 1

if ~isnumeric(etat)
    my_warndlg('clc_popup/set_etat : Invalid format', 0);
    isOk = 0;
elseif (etat < 1)
    my_warndlg(['clc_popup/set_etat : Invalid value : ', mat2str(etat)], 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.etat = etat;
else
    this.etat = 1;
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'Value', this.etat);
end
