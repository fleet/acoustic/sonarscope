% Initialisation du message de callback pour un click souris
% 
% Syntax
%   this = set_msg_click(this, msgClick)
%
% Input Arguments
%   this  : this de clc_popup
%   msgClick  : message de la call back
%
% Output Arguments
%   this : instance de clc_popup initialisee
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: set_msg_click.m,v 1.3 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_msg_click(this, msgClick)

this.msgClick = msgClick;

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'Callback', built_cb(this, this.msgClick));
end
