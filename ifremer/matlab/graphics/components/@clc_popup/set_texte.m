% Initialisation du texte (liste) de la valeur
% 
% Syntax
%   this = set_texte(this, texte);
%
% Input Arguments
%   this  : instance de clc_popup
%   texte : liste des textes affichee dans la liste du clc_popup
%
% Output Arguments
%   this : instance de clc_popup initialisee
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: set_texte.m,v 1.4 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_texte(this, texte)

isOk = 1;

% ----------------------------------------------
% Test validite des arguments :
% - doit etre une liste de chaines de caracteres

if ~iscell(texte)
    my_warndlg('clc_popup/set_texte : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.texte = texte;
else
    this.texte = {''};
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'String', this.texte);
end
