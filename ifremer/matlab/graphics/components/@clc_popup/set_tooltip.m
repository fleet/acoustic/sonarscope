% Initialisation de l'info-bulle du composant
% 
% Syntax
%   this = set_tooltip(this, tooltip);
%
% Input Arguments
%   this    : instance de clc_popup
%   tooltip : info-bulle du composant (chaine de caracteres)
%
% Output Arguments
%    this : instance de clc_popup initialisee
%
% See also clc_popup Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_tooltip(this, tooltip)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(tooltip)
    my_warndlg('clc_popup/set_tooltip : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.tooltip = tooltip;
else
    this.tooltip = '';
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'TooltipString', this.tooltip);
end
