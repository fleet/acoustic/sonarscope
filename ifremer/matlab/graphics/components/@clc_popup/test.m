% Test graphique d'un composant clc_popup
%
% Syntax
%   this = test(this, ...)
% 
% Input Arguments
%   this : instance de clc_popup
%
% Name-Value Pair Arguments
%   
%
% Output Arguments
%   this : instance de clc_popup
% 
% Examples
%    this = test(clc_popup);
%
% See also clc_popup Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.3 2003/05/02 16:10:53 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    13/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this, varargin)

% Maintenance Auto : try
    
    % -------------------------------------------------------
    % Creation d une figure ainsi que de l'ancre du composant
    
    figure;
    h(1) = uicontrol(   'Visible'  , 'on', ...
                        'Tag'      , 'ancre_clc_popup 1', ...
                        'Position' , [20 20 300 30]);
    h(2) = uicontrol(   'Visible'  , 'on', ...
                        'Tag'      , 'ancre_clc_popup 2', ...
                        'Position' , [20 70 300 30]);
    h(3) = uicontrol(   'Visible'  , 'on', ...
                        'Tag'      , 'ancre_clc_popup 3', ...
                        'Position' , [20 120 300 30]);
    h(4) = uicontrol(   'Visible'  , 'on', ...
                        'Tag'      , 'ancre_clc_popup 4', ...
                        'Position' , [20 170 300 30]);
    h(5) = uicontrol(   'Visible'  , 'on', ...
                        'Tag'      , 'ancre_clc_popup 5', ...
                        'Position' , [20 220 300 30]);
    
    % ---------------------------------------
    % Creation et initialisation du composant
    
    i(1)    = clc_popup([]);
    i(2)    = clc_popup([]);
    i(3)    = clc_popup([]);
    i(4)    = clc_popup([]);
    i(5)    = clc_popup([]);
    
    i(1) = set_texte(       i(1), {'1', 'deux', '3'});
    i(1) = set_etat(        i(1), 1);
    i(1) = set_tooltip(     i(1), 'On peut cliquer dessus');
    i(1) = set_msg_click(   i(1), 'A que ClIcK 1');
    i(1) = set_name(        i(1), 'ancre_clc_popup 1');
    i(1) = set_user_name(   i(1), 'clc_popup');
    i(1) = set_user_cb(     i(1), 'gerer_callback');
    i(1) = set_inset_x(     i(1), 2);
    i(1) = set_inset_y(     i(1), 2);
    
    i(2) = set_texte(       i(2), {'1', 'deux', '3'});
    i(2) = set_etat(        i(2), 1);
    i(2) = set_tooltip(     i(2), 'On peut cliquer dessus');
    i(2) = set_msg_click(   i(2), 'A que ClIcK 2');
    i(2) = set_name(        i(2), 'ancre_clc_popup 2');
    i(2) = set_user_name(   i(2), 'clc_popup');
    i(2) = set_user_cb(     i(2), 'gerer_callback');
    i(2) = set_inset_x(     i(2), 2);
    i(2) = set_inset_y(     i(2), 2);
    
    i(3) = set_texte(       i(3), {'1', 'deux', '3'});
    i(3) = set_etat(        i(3), 2);
    i(3) = set_tooltip(     i(3), 'On peut cliquer dessus');
    i(3) = set_msg_click(   i(3), 'A que ClIcK 3');
    i(3) = set_name(        i(3), 'ancre_clc_popup 3');
    i(3) = set_user_name(   i(3), 'clc_popup');
    i(3) = set_user_cb(     i(3), 'gerer_callback');
    i(3) = set_inset_x(     i(3), 0);
    i(3) = set_inset_y(     i(3), 0);
    
    i(4) = set_texte(       i(4), {'1', 'deux', '3'});
    i(4) = set_etat(        i(4), 2);
    i(4) = set_tooltip(     i(4), 'On peut cliquer dessus');
    i(4) = set_msg_click(   i(4), 'A que ClIcK 4');
    i(4) = set_name(        i(4), 'ancre_clc_popup 4');
    i(4) = set_user_name(   i(4), 'clc_popup');
    i(4) = set_user_cb(     i(4), 'gerer_callback');
    i(4) = set_inset_x(     i(4), 5);
    i(4) = set_inset_y(     i(4), 5);
    
    i(5) = set_texte(       i(5), {'1', 'deux', '3'});
    i(5) = set_etat(        i(5), 3);
    i(5) = set_tooltip(     i(5), 'On peut cliquer dessus');
    i(5) = set_msg_click(   i(5), 'A que ClIcK 5');
    i(5) = set_name(        i(5), 'ancre_clc_popup 5');
    i(5) = set_user_name(   i(5), 'clc_popup');
    i(5) = set_user_cb(     i(5), 'gerer_callback');
    i(5) = set_inset_x(     i(5), 0);
    i(5) = set_inset_y(     i(5), 7);
    
    this = i;
    
    % -------------------------------
    % Creation graphique du composant
    
    this(1) = editobj(this(1));
    this(2) = editobj(this(2));
    this(3) = editobj(this(3));
    this(4) = editobj(this(4));
    this(5) = editobj(this(5));
    
    % ------------------------
    % Rend le resultat visible
    
    set(gcf, 'Visible', 'on');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_popup', 'test', lasterr);
% Maintenance Auto : end
