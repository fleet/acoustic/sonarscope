% Constructeur du composant clc_pushbutton (bouton actionnable a deux etats, ou case a cocher)
%
% Syntax
%   this = clc_pushbutton(...)
% 
% Name-Value Pair Arguments
%   texte             : texte affiche sur le bouton
%   etat              : etat du composant, 0 : relache, 1 : enfonce
%   tooltip           : info-bulle du composant
%   msgClick          : message envoye en cas de click souris sur le bouton
%
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%   componentInsetX   : marge en X du composant (en pixels)
%   componentInsetY   : marge en Y du composant (en pixels)
%
% Output Arguments 
%   this : une instance de clc_pushbutton
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab.
%    cette version, tres bas niveau, permet d optimiser l utilisation
%    du composant
%
% Examples
%    v = test(clc_pushbutton)
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_pushbutton(varargin)

%% D�finition de la structure et initialisation

this.texte        = '';   % External property
this.etat         = 0;    % External property
this.tooltip      = '';   % External property
this.msgClick     = '';   % External property
this.CData        = [];   % External property
this.BusyAction   = [];   % External property
this.HitTest      = 'on'; % EN test LUR!

this.f_globale    = [];   % Internal property
this.h_uic        = [];   % Internal property
this.handleAnchor = [];   % Internal property

%% Super-classe

% if (nargin == 1) && isempty(varargin{1})
%     composant = cl_component([]);
% else
    composant = cl_component([]);
% end

%% Cr�ation de l'objet

this = class(this, 'clc_pushbutton', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
