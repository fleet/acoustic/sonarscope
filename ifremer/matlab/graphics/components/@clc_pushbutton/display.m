% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments 
%   a : instance de clc_pushbutton
%
% Examples
%   a = clc_pushbutton
%   display(a) ;
%
% See also clc_pushbutton clc_pushbutton/char Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
