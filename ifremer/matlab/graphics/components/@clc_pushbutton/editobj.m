% Interface-Homme-Machine (IHM) d'une instance
%
% Syntax
%   this = editobj(this)
%
% Input Arguments
%   this : instance de clc_pushbutton
%
% OUPUT PARAMETERS :
%   this : instance de clc_pushbutton
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

this.cl_component = editobj(this.cl_component, 'handleAnchor', this.handleAnchor);
this = construire_ihm(this);
