% Gestion des call back d'une IHM d'une instance
%
% Syntax
%   this = gerer_callback(this, cbName, ...)
% 
% Input Arguments
%   this : instance de clc_pushbutton
%   cbName   : nom de la call back
%
% Name-Value Pair Arguments
%   Les valeurs sont dynamiques, elles sont lues par :
%     this.msgClick
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also clc_pushbutton Authors
% Authors : DCF
% VERSION  : $Id: gerer_callback.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

% --------------------
% Selection du message

if length(varargin) == 1
    
    switch varargin{1}
        case this.msgClick
            this.etat = get(this.h_uic, 'Value');
        otherwise
            my_warndlg('clc_pushbutton/gerer_callback : Invalid message', 0);
    end
else
    my_warndlg('clc_pushbutton/gerer_callback : Invalid arguments number', 0);
end
