% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de clc_pushbutton
%
% Name-only Arguments
%   texte             : texte affiche sur le bouton
%   etat              : etat du composant, 0 : relache, 1 : enfonce
%   tooltip           : info-bulle du composant
%   msgClick          : message envoye en cas de click souris sur le bouton
%   CData             : icone
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentInsetX   : marges selon x
%   componentInsetY   : marges selon y
%   componentAnchor   : ancre du composant
%
% Output Arguments 
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%    v = clc_pushbutton; 
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'texte'
            varargout{end+1} = get_texte(this); %#ok<AGROW>
        case 'etat'
            varargout{end+1} = get_etat(this); %#ok<AGROW>
        case 'tooltip'
            varargout{end+1} = get_tooltip(this); %#ok<AGROW>
        case 'CData'
            varargout{end+1} = get_CData(this); %#ok<AGROW>
        case 'msgClick'
            varargout{end+1} = get_msg_click(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.cl_component, varargin{i}); %#ok<AGROW>
    end
end
