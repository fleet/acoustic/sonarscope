% Acces en lecture des proprietes d'une instance : retourne l'icone du composant
% 
% Syntax
%   CData = get_CData(this)
%
% Input Arguments
%   this : instance de clc_pushbutton
%
% Output Arguments
%   CData : Icone du composant
%
% See also clc_pushbutton Authors
% Authors : JMA
% VERSION  : $Id: get_CData.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    03/10/2003 - JMA - creation
% ----------------------------------------------------------------------------

function CData = get_CData(this)

% Maintenance Auto : try
    CData = this.CData;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_pushbutton', 'get_CData', lasterr);
% Maintenance Auto : end
