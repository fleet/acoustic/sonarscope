% Acces au handle du bouton (
% Fonctionnalite necessaire pour y attacher un uicontextmenu, voir clc_image
%
% Syntax
%   handle = get_handle(this)
%
% Input Arguments
%   this : instance de clc_pushbutton
%
% Output Arguments
%   handle : handle du composant
%
% See also clc_pushbutton Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function handle = get_handle(this)
handle = this.h_uic;
