% Retourne le texte affiche par le composant
% 
% Syntax
%   texte = get_texte(this)
%
% Input Arguments
%   this : instance de clc_pushbutton
%
% Output Arguments
%   texte : texte affiche par le composant
%
% See also clc_pushbutton Authors
% Authors : DCF
% VERSION  : $Id: get_texte.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function texte = get_texte(this)

% Maintenance Auto : try
    texte = this.texte;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_pushbutton', 'get_texte', lasterr);
% Maintenance Auto : end
