% Retourne l'info-bulle du composant
% 
% Syntax
%   tooltip = get_tooltip(this)
%
% Input Arguments
%    this : instance de clc_pushbutton
%
% Output Arguments
%    tooltip : info-bulle
%
% See also clc_pushbutton Authors
% Authors : DCF
% VERSION  : $Id: get_tooltip.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tooltip = get_tooltip(this)

% Maintenance Auto : try
    tooltip = this.tooltip;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_pushbutton', 'get_tooltip', lasterr);
% Maintenance Auto : end
