% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%    a : instance de clc_pushbutton
%
% Output Arguments
%    str : une chaine de caracteres representative de l instance
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

%% Traitement

str{end+1} = sprintf('texte    <-> %s', this.texte);
str{end+1} = sprintf('etat     <-> %d', this.etat);
str{end+1} = sprintf('tooltip  <-> %s', this.tooltip);
str{end+1} = sprintf('msgClick <-> %s', this.msgClick{end});
str{end+1} = sprintf('CData    <-> %s', num2strCode(this.CData));
str{end+1} = sprintf('handleAnchor <-> %f', this.handleAnchor);

str{end+1} = sprintf('--- heritage cl_component ---');
str{end+1} = char(this.cl_component);

%% Concatenation en une chaine

str = cell2str(str);
