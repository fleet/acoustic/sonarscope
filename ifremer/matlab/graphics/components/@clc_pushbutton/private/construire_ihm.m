% Creation de l'IHM d'une instance
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   athis : instance de clc_pushbutton
%
% Output Arguments
%   this : instance de clc_pushbutton
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation de la frame du composant

this = creer_f_globale(this);

%% Mise � jour de l'accessibilit�

if strcmp(get_cenable(this), 'off')
    this.f_globale = set_enable(this.f_globale, 'off');
end

%% Mise � jour de la visibilit�

if strcmp(get_cvisible(this), 'off')
    this.f_globale = set_visible(this.f_globale, 'off');
end
