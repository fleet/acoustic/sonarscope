% Création des frames principales de l'IHM d'une instance
%
% Syntax
%   this = creer_f_globale(this)
%
% Input Arguments
%   this : instance de clc_pushbutton
%
% Output Arguments
%   this : instance de clc_pushbutton initialisée
%
% See also clc_pushbutton Authors
% Authors : DCF
% -------------------------------------------------------------------------

function this = creer_f_globale(this)

ihm = [];

%% Ancre du composant clc_edit_texte titre

ihm{end+1}.Lig         = 1;
ihm{end}.Col           = 1;
ihm{end}.Style         = 'pushbutton';
ihm{end}.HitTest       = this.HitTest;
ihm{end}.String        = this.texte;
ihm{end}.Value         = this.etat;
ihm{end}.TooltipString = this.tooltip;
ihm{end}.CData         = this.CData;
ihm{end}.Callback      = built_cb(this, this.msgClick);

%% Création des frames

this.f_globale = cl_frame([]);
this.f_globale = set_handle_frame(this.f_globale, get_anchor(this));
this.f_globale = set_inset_x(this.f_globale, get_inset_x(this));
this.f_globale = set_inset_y(this.f_globale, get_inset_y(this));
this.f_globale = set_max_lig(this.f_globale, 1);
this.f_globale = set_max_col(this.f_globale, 1);
this.f_globale = set_tab_uicontrols(this.f_globale, ihm);

%% Détermination des acces rapides

handles = get_tab_uicontrols(this.f_globale);
this.h_uic = handles(1);

%{
for k=1:length(this.tabUicontrols)
    this.ihm.Handles.pppp = this.tabUicontrols(k);
end
%}

%  this.tabUicontrols
%{
for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end
%}


