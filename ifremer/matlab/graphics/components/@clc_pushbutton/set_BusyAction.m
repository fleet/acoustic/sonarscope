% Initialisation de l'icone du composant
% 
% Syntax
%   this = set_BusyAction(this, BusyAction)
%
% Input Arguments
%   this  : instance de clc_pushbutton
%   BusyAction : Icone
%
% Output Arguments
%   this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : JMA
% VERSION  : $Id: set_BusyAction.m,v 1.6 2003/05/05 16:25:47 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_BusyAction(this, BusyAction)

this.BusyAction = BusyAction;

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'BusyAction', this.BusyAction);
end
