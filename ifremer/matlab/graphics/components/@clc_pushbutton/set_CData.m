% Initialisation de l'icone du composant
% 
% Syntax
%   this = set_CData(this, CData)
%
% Input Arguments
%   this  : instance de clc_pushbutton
%   CData : Icone
%
% Output Arguments
%   this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_CData(this, CData)

isOk = 1;

%% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~isnumeric(CData)
    my_warndlg('clc_pushbutton/set_CData : Invalid format', 1);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.CData = CData;
else
    this.CData = [];
end

%% Mise 0 jour graphique

if is_edit(this)
    set(this.h_uic, 'CData', this.CData);
end
