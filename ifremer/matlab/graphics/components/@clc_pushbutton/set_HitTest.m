% Initialisation de l accessibilite du composant
%
% Syntax
%   this = set_cenable(this, enable)
%
% Input Arguments
%   this : instance de clc_pushbutton
%   enable : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_HitTest(this,HitTest)

this.HitTest = HitTest;