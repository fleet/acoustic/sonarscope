% Initialisation du texte affiche sur le bouton
% 
% Syntax
%   this = set_color(this, texte);
%
% Input Arguments
%    this  : instance de clc_pushbutton
%    texte : texte affiche sur le bouton
%
% Output Arguments
%    this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_color(this, Color)

if is_edit(this)
    set(this.h_uic, 'BackgroundColor', Color);
end
