% Initialisation de la visibilite du composant
% 
% Syntax
%   this = set_cvisible(this, visible)
%
% Input Arguments
%    this : instance de clc_pushbutton
%    visible : visibilite du composant 'on' | 'off'
%
% Output Arguments
%    this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : DCF
% VERSION  : $Id: set_cvisible.m,v 1.4 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_cvisible(this, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    if is_edit(this)
        this.f_globale  = set_visible(this.f_globale, visible);
    end
    this.cl_component = set_visible(this.cl_component, visible);
else
    my_warndlg('clc_pushbutton/set_cvisible : Invalid value', 1);
end
