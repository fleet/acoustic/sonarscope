% Initialisation de l etat du composant
% 
% Syntax
%   this = set_etat(this, etat)
%
% Input Arguments
%   this : instance de clc_pushbutton
%   etat : etat du composant (0 ou 1)
%
% Output Arguments
%   this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_etat(this, etat)

isOk = 1;

%% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~isnumeric(etat)
    my_warndlg('clc_pushbutton/set_etat : Invalid format');
    isOk = 0;
elseif (etat ~= 0) && (etat ~= 1)
    my_warndlg(['clc_pushbutton/set_etat : Invalid value : ', mat2str(etat)]);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.etat = etat;
else
    this.etat = 0;
end

%% Mise � jour graphique

if is_edit(this)
    set(this.h_uic, 'Value', this.etat);
end
