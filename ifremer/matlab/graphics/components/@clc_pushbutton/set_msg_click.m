% Initialisation du message de call back pour un click souris
% 
% Syntax
%   this = set_msg_click(this, msgClick)
%
% Input Arguments
%   this  : instance de clc_pushbutton
%   msgClick : message de la call back
%
% Output Arguments
%   this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : DCF
% VERSION  : $Id: set_msg_click.m,v 1.4 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_msg_click(this, msgClick)

this.msgClick = msgClick;

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'Callback', built_cb(this, this.msgClick));
end
