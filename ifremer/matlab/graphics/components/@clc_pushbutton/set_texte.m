% Initialisation du texte affiche sur le bouton
% 
% Syntax
%   this = set_texte(this, texte);
%
% Input Arguments
%    this  : instance de clc_pushbutton
%    texte : texte affiche sur le bouton
%
% Output Arguments
%    this : instance de clc_pushbutton initialisee
%
% See also clc_pushbutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_texte(this, texte)

isOk = 1;

%% Test validite des arguments : doit etre une chaine de caracteres

if ~ischar(texte)
    my_warndlg('clc_pushbutton/set_texte : Invalid format', 0);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.texte = texte;
else
    this.texte = '';
end

%% Mise � jour graphique

if is_edit(this)
    set(this.h_uic, 'String', this.texte);
end
