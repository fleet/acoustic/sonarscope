% Mise a jour graphique du composant
%
% Syntax
%   this = update(this)
% 
% Input Arguments
%   this : instance de clc_pushbutton
%
% Output Arguments
%   this : instance de clc_pushbutton modifiee
%
% Remarks : doit etre surchargee
%
% See also clc_pushbutton Authors
% Authors : DCF
% VERSION  : $Id: update.m,v 1.5 2003/05/05 16:25:47 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = update(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------
    % Mise a jour si l'instance a une representation graphique
    
    if is_edit(this)
        
        % -----------------------------------
        % Update graphique de la super classe
        
        this.cl_component = update(this.cl_component);
        
        % -----------------------------------------------------
        % Update graphique des elements graphiques du composant
        
        if ~isempty(this.f_globale)
            this.f_globale = update(this.f_globale);
        end
        
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_pushbutton', 'update', lasterr);
% Maintenance Auto : end
