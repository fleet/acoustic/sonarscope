% Transformation en chaine de caracteres d un tableau d instances
%
% Syntax
%   str = char(instance) 
%
% Input Arguments
%   instance : instance ou tableau d instance de clc_quit
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%   char (clc_quit)
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char (instance)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Intialisation des locales
    
    str = [] ;
    
    % --------------------------------------------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size (instance) ;
    
    if (m*n) > 1
        for i=1:m
            for j=1:n
                str {end+1} = char (instance (i, j)) ;
            end
        end
        str = strcat (str {:}) ;
    else
        str = char_instance (instance) ;
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'char', '') ;
% Maintenance Auto : end
