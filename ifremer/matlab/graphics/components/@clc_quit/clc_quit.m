% Constructeur de clc_quit, classe composant
%
% Syntax
%   instance = clc_quit(...)
% 
% Name-Value Pair Arguments
%   fQuit             : flag de presence du bouton Quit 
%                       (1 : present, 0 : absent)
%   fSave             : flag de presence du bouton Save 
%                       (1 : present, 0 : absent)
%   fHelp             : flag de presence du bouton Help 
%                       (1 : present, 0 : absent)
%   fLoadFile         : flag de presence du bouton LoadFile
%                       (1 : present, 0 : absent)
%   fSaveFile         : flag de presence du bouton SaveFile
%                       (1 : present, 0 : absent)
%   msgQuit           : message cb de Quit ('clc_quit__Quit')
%   msgSave           : message cb de Save ('clc_quit__Save')
%   msgHelp           : message cb de Help ('clc_quit__Help')
%   msgLoadFile       : message cb de Load ('clc_quit__LoadFile')
%   msgSaveFile       : message cb de Load ('clc_quit__SaveFile')
%   tagQuit           : tag du bouton Quit ('clc_quit__Quit')
%   tagSave           : tag du bouton Save ('clc_quit__Save')
%   tagHelp           : tag du bouton Help ('clc_quit__Help')
%   tagLoadFile       : tag du bouton Load ('clc_quit__LoadFile')
%   tagSaveFile       : tag du bouton Load ('clc_quit__SaveFile')
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments 
%   instance         : instance de clc_quit
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%    quit = clc_quit
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: clc_quit.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    30/01/2000 - DCF - creation
%    23/03/2001 - DCF - maj optimisation code
%    06/04/2001 - DCF - ajout de la fonctionnalite de chargement
%    06/04/2001 - DCF - ajout du nom de fichier load / save
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = clc_quit (varargin)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Definition de la structure et initialisation
    %
    % globalFrame : instance de cl_frame, frame globale du composant
    %
    % fQuit       : presence du bouton quit (0 : absent, 1 : present)
    % fSave       : presence du bouton save (0 : absent, 1 : present)
    % fHelp       : presence du bouton help (0 : absent, 1 : present)
    % fLoadFile   : presence du bouton LoadFile (0 : absent, 1 : present)
    % fSaveFile   : presence du bouton SaveFile (0 : absent, 1 : present)
    % msgQuit     : message de la call back pour Quit
    % msgSave     : message de la call back pour Save
    % msgHelp     : message de la call back pour Help
    % msgLoadFile : message de la call back pour LoadFile
    % msgSaveFile : message de la call back pour SaveFile
    % tagQuit     : tag du bouton Quit (le meme que le message)
    % tagSave     : tag du bouton Save (le meme que le message)
    % tagHelp     : tag du bouton Help (le meme que le message)
    % tagLoadFile : tag du bouton LoadFile (le meme que le message)
    % tagSaveFile : tag du bouton SaveFile (le meme que le message)
    % fileName    : nom du fichier pour load / save
    
    instance.globalFrame          = [] ;
    instance.fQuit                = 0  ;
    instance.fSave                = 0  ;
    instance.fHelp                = 0  ;
    instance.fLoadFile            = 0  ;
    instance.fSaveFile            = 0  ;
    instance.msgQuit              = 'clc_quit__Quit' ;
    instance.msgSave              = 'clc_quit__Save' ;
    instance.msgHelp              = 'clc_quit__Help' ;
    instance.msgLoadFile          = 'clc_quit__LoadFile' ;
    instance.msgSaveFile          = 'clc_quit__SaveFile' ;
    instance.tagQuit              = instance.msgQuit ;
    instance.tagSave              = instance.msgSave ;
    instance.tagHelp              = instance.msgHelp ;
    instance.tagLoadFile          = instance.msgLoadFile ;
    instance.tagSaveFile          = instance.msgSaveFile ;
    instance.fileName             = ''               ;
    
    % --------------------------------------------------------------------------
    % Super-classe
    
    if (nargin == 1) & isempty(varargin{1})
        composant = cl_component ([]) ;
    else
        composant = cl_component ;
    end
    
    % --------------------------------------------------------------------------
    % Creation de l'objet
    
    instance = class (instance, 'clc_quit', composant) ;
    
    % --------------------------------------------------------------------------
    % Pre-nitialisation des champs 
    
    if (nargin == 1) & isempty(varargin{1})
        return
    end
    instance = set(instance, varargin{:}) ;
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'clc_quit', '') ;
% Maintenance Auto : end
