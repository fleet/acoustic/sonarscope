% Destruction du composant
%
% Syntax
%   instance = delete(instance)
% 
% Input Arguments
%   instance : instance de clc_quit
%
% Remarks : doit etre surchargee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = delete (instance)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Destruction graphique de la super classe
    
    instance.cl_component = delete (instance.cl_component) ;
    
    % --------------------------------------------------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(instance.globalFrame)
        instance.globalFrame = delete (instance.globalFrame) ;
        instance.globalFrame = [] ;
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'delete', '') ;
% Maintenance Auto : end
