% Affichage des donnees d'une instance de clc_quit
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : instance de clc_quit
%
% Examples
%   display(clc_quit) ;
%
% See also clc_quit Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this') ;
sz = s.size   ;
fprintf ('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this)) ;
