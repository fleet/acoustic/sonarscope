% Representation de l'instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_quit
%
% OUPUT PARAMETERS :
%   this : instance de clc_quit
%
% See also clc_quit Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Cr�ation de l interface graphique

if ~is_edit(this)
    this.cl_component = editobj(this.cl_component);
    this = construire_ihm(this);
end
