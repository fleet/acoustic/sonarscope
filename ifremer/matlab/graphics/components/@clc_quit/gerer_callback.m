% Methode de gestion des call back du composant
%
% Syntax
%   this = gerer_callback(this, cbName, varargin)
% 
% Input Arguments
%   this : instance de cl_component
%   cbName   : nom de la call back
%
% Name-Value Pair Arguments
%   
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

if length(varargin) == 1
    switch varargin{1}
        case get_msg_quit(this)
            delete(gcf);

        case get_msg_save(this)

        case get_msg_load_file(this)
            this = traiter_load_file(this);

        case get_msg_save_file(this)
            this = traiter_save_file(this);

        case get_msg_help(this)
            my_web(['http://w3.ifremer.fr/tmsi-as/', get_name(this)]);

        otherwise
            my_warndlg('clc_quit/gerer_callback : Unknown message', 0);
    end
else
    my_warndlg('clc_quit/gerer_callback : Invalid arguments number', 0);
end
