% Lecture des attributs de l instance
%
% Syntax
%   [...] = get (instance, ...)
% 
% Input Arguments
%   instance : instance de clc_quit
%
% Name-Value Pair Arguments
%   fQuit             : flag de presence du bouton Quit 
%   fSave             : flag de presence du bouton Save 
%   fHelp             : flag de presence du bouton Help 
%   fLoadFile         : flag de presence du bouton LoadFile
%   fSaveFile         : flag de presence du bouton SaveFile
%   msgQuit           : message cb de Quit
%   msgSave           : message cb de Save
%   msgHelp           : message cb de Help
%   msgLoadFile       : message cb de LoadFile
%   msgSaveFile       : message cb de SaveFile
%   tagQuit           : tag cb de Quit
%   tagSave           : tag cb de Save
%   tagHelp           : tag cb de Help
%   tagLoadFile       : tag cb de LoadFile
%   tagSaveFile       : tag cb de SaveFile
%   fileName          : fichier de sauvegarde ou lecture
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant
%   componentEnable   : accessibilite du composant
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    c = clc_quit('tagQuit', 'Quit') ;
%    get(c, 'tagQuit')
%
% See also clc_quit Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get (instance, varargin)

varargout = {};

%% Boucle de parcours de l ensemble des proprietes donnees en arguments

for i=1:length(varargin)
    switch varargin{i}
        case 'fQuit'
            varargout {end+1} = get_fquit (instance)   ;
        case 'fSave'
            varargout {end+1} = get_fsave (instance)   ;
        case 'fHelp'
            varargout {end+1} = get_fhelp (instance)   ;
        case 'fLoadFile'
            varargout {end+1} = get_fload_file (instance)   ;
        case 'fSaveFile'
            varargout {end+1} = get_fsave_file (instance)   ;
        case 'msgQuit'
            varargout {end+1} = get_msg_quit (instance) ;
        case 'msgSave'
            varargout {end+1} = get_msg_save (instance) ;
        case 'msgHelp'
            varargout {end+1} = get_msg_help (instance) ;
        case 'msgLoadFile'
            varargout {end+1} = get_msg_load_file (instance) ;
        case 'msgSaveFile'
            varargout {end+1} = get_msg_save_file (instance) ;
        case 'tagQuit'
            varargout {end+1} = get_tag_quit (instance);
        case 'tagSave'
            varargout {end+1} = get_tag_save (instance) ;
        case 'tagHelp'
            varargout {end+1} = get_tag_help (instance) ;
        case 'tagLoadFile'
            varargout {end+1} = get_tag_load_file (instance) ;
        case 'tagSaveFile'
            varargout {end+1} = get_tag_save_file (instance) ;
        case 'fileName'
            varargout {end+1} = get_file_name (instance) ;
        case {'componentVisible', 'componentEnable', 'componentName', ...
                'componentAnchor' , 'componentUserName', 'componentUserCb', ...
                'componentInsetX', 'componentInsetY'}
            varargout {end+1} = get (instance.cl_component, varargin{i}) ;
        otherwise
            my_warndlg('clc_quit/get : Invalid property name', 0);
    end
end
