% Retourne le flag d'affichage du bouton Help
% 
% Syntax
%   fHelp = get_fhelp(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   fHelp : flag d'affichage du bouton Help
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_fhelp.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function fHelp = get_fhelp(instance)

% Maintenance Auto : try
    fHelp = instance.fHelp ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_fhelp', '') ;
% Maintenance Auto : end
