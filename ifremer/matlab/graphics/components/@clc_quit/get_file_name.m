% Retourne le nom du fichier pour load / save
% 
% Syntax
%   fileName = get_file_name(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   fileName : nom du fichier pour load / save
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_file_name.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function fileName = get_file_name(instance)

% Maintenance Auto : try
    fileName = instance.fileName ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_file_name', '') ;
% Maintenance Auto : end
