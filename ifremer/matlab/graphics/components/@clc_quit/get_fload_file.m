% Retourne le flag d affichage du bouton Load
% 
% Syntax
%   fLoad = get_fload_file(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   fLoad : flag d affichage du bouton Load
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_fload_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function fLoad = get_fload_file (instance)

% Maintenance Auto : try
    fLoad = instance.fLoadFile ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_fload_file', '') ;
% Maintenance Auto : end
