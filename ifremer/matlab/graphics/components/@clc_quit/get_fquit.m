% Retourne le flag d affichage du bouton Quit
% 
% Syntax
%   fQuit = get_fquit(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   fQuit : flag d affichage du bouton Quit
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_fquit.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function fQuit = get_fquit(instance)

% Maintenance Auto : try
    fQuit = instance.fQuit ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_fquit', '') ;
% Maintenance Auto : end
