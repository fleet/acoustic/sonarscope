% Retourne le flag d affichage du bouton Save
% 
% Syntax
%   fSave = get_fsave_file(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   fSave : flag d affichage du bouton Save
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_fsave_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function fSave = get_fsave_file (instance)

% Maintenance Auto : try
    fSave = instance.fSaveFile ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_fsave_file', '') ;
% Maintenance Auto : end
