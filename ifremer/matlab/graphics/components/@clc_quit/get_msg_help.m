% Retourne le message de la call back pour Help
% 
% Syntax
%   msgHelp = get_msg_help(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   msgHelp  : message de la call back pour help
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_msg_help.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgHelp = get_msg_help (instance)

% Maintenance Auto : try
    msgHelp = instance.msgHelp ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_msg_help', '') ;
% Maintenance Auto : end
