% Retourne le message de la call back pour Quit
% 
% Syntax
%   msgQuit = get_msg_quit(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   msgQuit  : message de la call back pour quit
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_msg_quit.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgQuit = get_msg_quit(instance)

% Maintenance Auto : try
    msgQuit = instance.msgQuit ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_msg_quit', '') ;
% Maintenance Auto : end
