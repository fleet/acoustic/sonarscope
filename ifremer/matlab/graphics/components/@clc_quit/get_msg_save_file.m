% Retourne le message de la call back pour Save
% 
% Syntax
%   msgSave = get_msg_save_file(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   msgSave  : message de la call back pour save
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_msg_save_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgSave = get_msg_save_file (instance)

% Maintenance Auto : try
    msgSave = instance.msgSaveFile ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_msg_save_file', '') ;
% Maintenance Auto : end
