% Retourne le tag du bouton Help
% 
% Syntax
%   taghelp = get_tag_help(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   tagHelp  : tag du bouton Help
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_tag_help.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagHelp = get_tag_help(instance)

% Maintenance Auto : try
    tagHelp = instance.tagHelp ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_tag_help', '') ;
% Maintenance Auto : end
