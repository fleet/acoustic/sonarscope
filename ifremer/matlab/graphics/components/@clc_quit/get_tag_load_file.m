% Retourne le tag du bouton Load
% 
% Syntax
%   tagLoad = get_tag_load_file(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   tagLoad  : tag du bouton Load
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_tag_load_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagLoad = get_tag_load_file (instance)

% Maintenance Auto : try
    tagLoad = instance.tagLoadFile ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_tag_load_file', '') ;
% Maintenance Auto : end
