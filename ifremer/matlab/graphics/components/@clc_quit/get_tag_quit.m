% Retourne le tag du bouton Quit
% 
% Syntax
%   tagQuit = get_tag_quit(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   tagQuit  : tag du bouton Quit
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_tag_quit.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagQuit = get_tag_quit (instance)

% Maintenance Auto : try
    tagQuit = instance.tagQuit ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_tag_quit', '') ;
% Maintenance Auto : end
