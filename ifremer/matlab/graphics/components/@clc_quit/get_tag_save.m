% Retourne le tag du bouton Save
% 
% Syntax
%   tagSave = get_tag_save(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   tagSave  : tag du bouton Save
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_tag_save.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagSave = get_tag_save (instance)

% Maintenance Auto : try
    tagSave = instance.tagSave ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_tag_save', '') ;
% Maintenance Auto : end
