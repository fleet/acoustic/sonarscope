% Retourne le tag du bouton Save
% 
% Syntax
%   tagSave = get_tag_save_file(instance)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   tagSave  : tag du bouton Save
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: get_tag_save_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function tagSave = get_tag_save_file (instance)

% Maintenance Auto : try
    tagSave = instance.tagSaveFile ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'get_tag_save_file', '') ;
% Maintenance Auto : end
