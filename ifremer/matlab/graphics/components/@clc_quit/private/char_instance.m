% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(instance) 
%
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.2 2002/07/08 13:44:24 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance (instance)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Intialisation des locales
    
    str = [] ;
    
    % --------------------------------------------------------------------------
    % Traitement
    
    str {end+1} = sprintf ('\nclc_quit::fQuit          <-> %s', ...
        mat2str (instance.fQuit)) ;
    str {end+1} = sprintf ('\nclc_quit::fSave          <-> %s', ...
        mat2str (instance.fSave)) ;
    str {end+1} = sprintf ('\nclc_quit::fHelp          <-> %s', ...
        mat2str (instance.fHelp)) ;
    str {end+1} = sprintf ('\nclc_quit::fLoadFile      <-> %s', ...
        mat2str (instance.fLoadFile)) ;
    str {end+1} = sprintf ('\nclc_quit::fSaveFile      <-> %s', ...
        mat2str (instance.fSaveFile)) ;
    str {end+1} = sprintf ('\nclc_quit::msgQuit        <-> %s', ...
        mat2str (instance.msgQuit)) ;
    str {end+1} = sprintf ('\nclc_quit::msgSave        <-> %s', ...
        mat2str (instance.msgSave)) ;
    str {end+1} = sprintf ('\nclc_quit::msgHelp        <-> %s', ...
        mat2str (instance.msgHelp)) ;
    str {end+1} = sprintf ('\nclc_quit::msgLoadFile    <-> %s', ...
        mat2str (instance.msgLoadFile)) ;
    str {end+1} = sprintf ('\nclc_quit::msgSaveFile    <-> %s', ...
        mat2str (instance.msgSaveFile)) ;
    str {end+1} = sprintf ('\nclc_quit::tagQuit        <-> %s', ...
        mat2str (instance.tagQuit)) ;
    str {end+1} = sprintf ('\nclc_quit::tagSave        <-> %s', ...
        mat2str (instance.tagSave)) ;
    str {end+1} = sprintf ('\nclc_quit::tagHelp        <-> %s', ...
        mat2str (instance.tagHelp)) ;
    str {end+1} = sprintf ('\nclc_quit::tagLoadFile    <-> %s', ...
        mat2str (instance.tagLoadFile)) ;
    str {end+1} = sprintf ('\nclc_quit::tagSaveFile    <-> %s', ...
        mat2str (instance.tagSaveFile)) ;
    
    % --------------------------------------------------------------------------
    % Concatenation en une chaine
    
    str = char (strcat (str)) ;
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'char_instance', '') ;
% Maintenance Auto : end
