% Construction de base de l'IHM
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_quit
%
% Output Arguments
%   this : instance de clc_quit
%
% See also clc_quit Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation de la frame du composant

this = creer_frame_globale(this);

%% Mise � jour de l'accessibilit�

if strcmp(get_enable(this.cl_component), 'off')
    this.globalFrame = set_enable(this.globalFrame, 'off');
end

%% Mise � jour de la visibilit�

if strcmp(get_visible(this.cl_component), 'off')
    this.globalFrame = set_visible(this.globalFrame, 'off');
end
