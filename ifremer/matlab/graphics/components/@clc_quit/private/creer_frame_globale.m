% Creation de la frame globale du composant
%
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_quit
%
% Output Arguments
%   this : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

ib  = 0;
ihm = [];

%% Bouton quit

if this.fQuit
    ib = ib + 1;
    ihm{ib}.Lig      = 1;
    ihm{ib}.Col      = ib;
    ihm{ib}.Style    = 'pushbutton';
    ihm{ib}.Tag      = this.tagQuit;
    ihm{ib}.String   = Lang('Quitter', 'Quit');
    ihm{ib}.Callback = built_cb(this, this.msgQuit);
end

%% Bouton save

if this.fSave
    ib = ib + 1;
    ihm{ib}.Lig    = 1;
    ihm{ib}.Col    = ib;
    ihm{ib}.Style  = 'pushbutton';
    ihm{ib}.Tag    = this.tagSave;
    ihm{ib}.String = Lang('Sauver', 'Save');
    ihm{ib}.Callback = built_cb(this, this.msgSave);
end

%% Bouton loadFile

if this.fLoadFile
    ib = ib + 1;
    ihm{ib}.Lig    = 1;
    ihm{ib}.Col    = ib;
    ihm{ib}.Style  = 'pushbutton';
    ihm{ib}.Tag    = this.tagLoadFile;
    ihm{ib}.String = 'LoadFile';
    ihm{ib}.Callback = built_cb(this, this.msgLoadFile);
end

%% Bouton saveFile

if this.fSaveFile
    ib = ib + 1;
    ihm{ib}.Lig    = 1;
    ihm{ib}.Col    = ib;
    ihm{ib}.Style  = 'pushbutton';
    ihm{ib}.Tag    = this.tagSaveFile;
    ihm{ib}.String = 'SaveFile';
    ihm{ib}.Callback = built_cb(this, this.msgSaveFile);
end

%% Bouton help

if this.fHelp
    ib = ib + 1;
    ihm{ib}.Lig    = 1;
    ihm{ib}.Col    = ib;
    ihm{ib}.Style  = 'pushbutton';
    ihm{ib}.Tag    = this.tagHelp;
    ihm{ib}.String = 'Help';
    ihm{ib}.Callback = built_cb(this, this.msgHelp);
end

%% Cr�ation des frames

this.globalFrame = cl_frame([]);
this.globalFrame = set_handle_frame(this.globalFrame, get_anchor(this));
this.globalFrame = set_inset_x(this.globalFrame, get_inset_x(this));
this.globalFrame = set_inset_y(this.globalFrame, get_inset_y(this));
this.globalFrame = set_max_lig(this.globalFrame, 1);
this.globalFrame = set_max_col(this.globalFrame, ib);
this.globalFrame = set_tab_uicontrols(this.globalFrame, ihm);
