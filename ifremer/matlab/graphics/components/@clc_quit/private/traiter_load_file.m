% Traitement IHM pour choix du fichier � charger
% 
% Syntax
%   this = traiter_load_file(this)
%
% Input Arguments
%   this : instance de clc_quit
%
% Output Arguments
%   this : instance de clc_quit
%
% See also clc_quit Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_load_file(this)
