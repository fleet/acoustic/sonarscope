% Traitement IHM pour choix du fichier a enregistrer
% 
% Syntax
%   this = traiter_save_file(this)
%
% Input Arguments
%   this : instance de clc_quit
%
% Output Arguments
%   this : instance de clc_quit
%
% See also clc_quit Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_save_file(this)
