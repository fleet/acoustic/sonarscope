% Modification des attributs de l instance
%
% Syntax
%   instance = set(instance, ...)
%
% Input Arguments
%   instance : instance de clc_quit
%
% Name-Value Pair Arguments
%   fQuit             : flag de presence du bouton Quit
%                       (1 : present, 0 : absent)
%   fSave             : flag de presence du bouton Save
%                       (1 : present, 0 : absent)
%   fHelp             : flag de presence du bouton Help
%                       (1 : present, 0 : absent)
%   fLoadFile         : flag de presence du bouton LoadFile
%                       (1 : present, 0 : absent)
%   fSaveFile         : flag de presence du bouton SaveFile
%                       (1 : present, 0 : absent)
%   msgQuit           : message cb de Quit
%   msgSave           : message cb de Save
%   msgHelp           : message cb de Help
%   msgLoadFile       : message cb de LoadFile
%   msgSaveFile       : message cb de SaveFile
%   tagQuit           : tag cb de Quit
%   tagSave           : tag cb de Save
%   tagHelp           : tag cb de Help
%   tagLoadFile       : tag du bouton LoadFile
%   tagSaveFile       : tag du bouton SaveFile
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% Examples
%    c = clc_quit ;
%    set(c, 'fQuit', 1, 'fSave', 0, 'fHelp', 1) ;
%    set(c, 'componentName', 'zoneQuit')
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    06/04/2001 - DCF - ajout de la fonctionnalite load
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set (instance, varargin)

% Maintenance Auto : try

% --------------------------------------------------------------------------
% Lecture des arguments

[varargin, fQuit] = getPropertyValue(varargin, 'fQuit', []);
[varargin, fSave] = getPropertyValue(varargin, 'fSave', []);
[varargin, fHelp] = getPropertyValue(varargin, 'fHelp', []);
[varargin, fLoadFile] = getPropertyValue(varargin, 'fLoadFile', []);
[varargin, fSaveFile] = getPropertyValue(varargin, 'fSaveFile', []);
[varargin, msgQuit] = getPropertyValue(varargin, 'msgQuit', []);
[varargin, msgSave] = getPropertyValue(varargin, 'msgSave', []);
[varargin, msgHelp] = getPropertyValue(varargin, 'msgHelp', []);
[varargin, msgLoadFile] = getPropertyValue(varargin, 'msgLoadFile', []);
[varargin, msgSaveFile] = getPropertyValue(varargin, 'msgSaveFile', []);
[varargin, tagQuit] = getPropertyValue(varargin, 'tagQuit', []);
[varargin, tagSave] = getPropertyValue(varargin, 'tagSave', []);
[varargin, tagHelp] = getPropertyValue(varargin, 'tagHelp', []);
[varargin, tagLoadFile] = getPropertyValue(varargin, 'tagLoadFile', []);
[varargin, tagSaveFile] = getPropertyValue(varargin, 'tagSaveFile', []);
[varargin, visible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, enable] = getPropertyValue(varargin, 'componentEnable', []);

% --------------------------------------------------------------------------
% Initialisation des attributs

if (~(isempty (fQuit)))
    instance = set_fquit (instance, fQuit) ;
end
if (~(isempty (fSave)))
    instance = set_fsave (instance, fSave) ;
end
if (~(isempty (fHelp)))
    instance = set_fhelp (instance, fHelp) ;
end
if (~(isempty (fLoadFile)))
    instance = set_fload_file (instance, fLoadFile) ;
end
if (~(isempty (fSaveFile)))
    instance = set_fsave_file (instance, fSaveFile) ;
end
if (~(isempty (msgQuit)))
    instance = set_msg_quit (instance, msgQuit) ;
end
if (~(isempty (msgSave)))
    instance = set_msg_save (instance, msgSave) ;
end
if (~(isempty (msgHelp)))
    instance = set_msg_help (instance, msgHelp) ;
end
if (~(isempty (msgLoadFile)))
    instance = set_msg_load_file (instance, msgLoadFile) ;
end
if (~(isempty (msgSaveFile)))
    instance = set_msg_save_file (instance, msgSaveFile) ;
end
if (~(isempty (tagQuit)))
    instance = set_tag_quit (instance, tagQuit) ;
end
if (~(isempty (tagSave)))
    instance = set_tag_save (instance, tagSave) ;
end
if (~(isempty (tagHelp)))
    instance = set_tag_help (instance, tagHelp) ;
end
if (~(isempty (tagLoadFile)))
    instance = set_tag_load_file (instance, tagLoadFile) ;
end
if (~(isempty (tagSaveFile)))
    instance = set_tag_save_file (instance, tagSaveFile) ;
end
if (~(isempty (visible)))
    instance = set_visible (instance, visible) ;
end
if (~(isempty (enable)))
    instance = set_enable (instance, enable) ;
end

% --------------------------------------------------------------------------
% Traitement des message de la super classe

if ~isempty(varargin)
    instance.cl_component = set (instance.cl_component, varargin{:}) ;
end

% --------------------------------------------------------------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), instance) ;
end
