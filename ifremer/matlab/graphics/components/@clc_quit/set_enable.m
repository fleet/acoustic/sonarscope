% Initialisation de l accessibilite du composant
% 
% Syntax
%   instance = set_enable(instance, enable)
%
% Input Arguments
%   instance : instance de clc_quit
%   enable   : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_enable (instance, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    if is_edit(instance)
        instance.globalFrame  = set_enable (instance.globalFrame, enable) ;
    end
    instance.cl_component = set_enable (instance.cl_component, enable) ;
else
    my_warndlg('clc_quit/set_enable : Invalid value', 0);
end
