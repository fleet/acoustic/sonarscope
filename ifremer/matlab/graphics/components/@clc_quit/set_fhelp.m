% Initialisation du flag d affichage du bouton Help
% 
% Syntax
%   instance = set_fhelp(instance, fHelp)
%
% Input Arguments
%   instance : instance de clc_quit
%   fHelp    : flag d affichage du bouton help
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_fhelp.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_fhelp (instance, fHelp)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(fHelp)
    switch fHelp
        case {0, 1}
            % Si representation graphique, destruction, initialisation et
            % reconstruction
            if ~isempty(instance.globalFrame)
                instance = delete (instance)  ;
                instance.fHelp = fHelp        ;
                instance = editobj (instance) ;
            else
                instance.fHelp = fHelp        ;
            end
            
        otherwise
            my_warndlg('clc_quit/set_fhelp : Invalide argument value', 0);
    end
else
    my_warndlg('clc_quit/set_fhelp : Invalide argument format', 0);
end
