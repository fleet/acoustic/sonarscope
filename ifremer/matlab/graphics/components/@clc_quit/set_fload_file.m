% Initialisation du flag d affichage du bouton Load
% 
% Syntax
%   instance = set_fload_file(instance, fLoad)
%
% Input Arguments
%   instance : instance de clc_quit
%   fLoad    : flag d affichage du bouton load
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_fload_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_fload_file (instance, fLoad)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(fLoad)
    switch fLoad
        case {0, 1}
            % Si representation graphique, destruction, initialisation et
            % reconstruction
            if ~isempty (instance.globalFrame)
                instance = delete (instance)  ;
                instance.fLoadFile = fLoad        ;
                instance = editobj (instance) ;
            else
                instance.fLoadFile = fLoad        ;
            end
            
        otherwise
            my_warndlg('clc_quit/set_fload_file : Invalide argument value', 0);
    end
else
    my_warndlg('clc_quit/set_fload_file : Invalide argument format', 0);
end
