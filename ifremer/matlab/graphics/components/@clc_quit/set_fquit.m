% Initialisation du flag d affichage du bouton Quit
% 
% Syntax
%   instance = set_fquit(instance, fQuit)
%
% Input Arguments
%   instance : instance de clc_quit
%   fQuit    : flag d affichage du bouton quit
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_fquit.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_fquit (instance, fQuit)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(fQuit)
    switch fQuit
        case {0, 1}
            % Si representation graphique, destruction, initialisation et
            % reconstruction
            if ~isempty (instance.globalFrame)
                instance = delete (instance)  ;
                instance.fQuit = fQuit        ;
                instance = editobj (instance) ;
            else
                instance.fQuit = fQuit        ;
            end
            
        otherwise
            my_warndlg('clc_quit/set_fquit : Invalide argument value', 0);
    end
else
    my_warndlg('clc_quit/set_fquit : Invalide argument format', 0);
end
