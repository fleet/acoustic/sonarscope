% Initialisation du flag d affichage du bouton Save
% 
% Syntax
%   instance = set_fsave(instance, fSave)
%
% Input Arguments
%   instance : instance de clc_quit
%   fSave    : flag d affichage du bouton save
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_fsave.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

function instance = set_fsave (instance, fSave)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(fSave)
    
    switch fSave
        case {0, 1}
            % Si representation graphique, destruction, initialisation et
            % reconstruction
            if ~isempty (instance.globalFrame)
                instance = delete (instance)  ;
                instance.fSave = fSave        ;
                instance = editobj (instance) ;
            else
                instance.fSave = fSave        ;
            end
            
        otherwise
            my_warndlg('clc_quit/set_fsave : Invalide argument value', 0);
    end
else
    my_warndlg('clc_quit/set_fsave : Invalide argument format', 0);
end
