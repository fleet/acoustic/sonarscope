% Initialisation du flag d affichage du bouton Save
% 
% Syntax
%   instance = set_fsave_file(instance, fSave)
%
% Input Arguments
%   instance : instance de clc_quit
%   fSave    : flag d affichage du bouton save
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_fsave_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_fsave_file (instance, fSave)

% -------------------------------------------
% Test validite des arguments, initialisation

if isnumeric(fSave)
    switch fSave
        case {0, 1}
            % Si representation graphique, destruction, initialisation et
            % reconstruction
            if (~(isempty (instance.globalFrame)))
                instance = delete (instance)  ;
                instance.fSaveFile = fSave    ;
                instance = editobj (instance) ;
            else
                instance.fSaveFile = fSave    ;
            end
        otherwise
            my_warndlg('clc_quit/set_fsave_file : Invalide argument value', 0);
    end
else
    my_warndlg('clc_quit/set_fsave_file : Invalide argument format', 0);
end
