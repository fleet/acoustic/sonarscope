% Initialisation du message de la call back pour Help
% 
% Syntax
%   instance = set_msg_help(instance, msgHelp)
%
% Input Arguments
%   instance : instance de clc_quit
%   msgHelp  : message de la call back pour help
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_msg_help.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_msg_help (instance, msgHelp)

% Maintenance Auto : try
    instance.msgHelp = msgHelp ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_msg_help', '') ;
% Maintenance Auto : end
