% Initialisation du message de la call back pour Load
% 
% Syntax
%   instance = set_msg_load_file(instance, msgLoad)
%
% Input Arguments
%   instance : instance de clc_quit
%   msgLoad  : message de la call back pour load
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_msg_load_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_msg_load_file (instance, msgLoad)

% Maintenance Auto : try
    instance.msgLoadFile = msgLoad ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_msg_load_file', '') ;
% Maintenance Auto : end
