% Initialisation du message de la call back pour Quit
% 
% Syntax
%   instance = set_msg_quit (instance, msgQuit)
%
% Input Arguments
%   instance : instance de clc_quit
%   msgQuit  : message de la call back pour quit
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_msg_quit.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_msg_quit (instance, msgQuit)

% Maintenance Auto : try
    instance.msgQuit = msgQuit ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_msg_quit', '') ;
% Maintenance Auto : end
