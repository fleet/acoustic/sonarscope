% Initialisation du message de la callback pour Save
% 
% Syntax
%   instance = set_msg_save_file(instance, msgSave)
%
% Input Arguments
%   instance : instance de clc_quit
%   msgSave  : message de la call back pour save
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit
% Authors : DCF
% VERSION  : $Id: set_msg_save_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_msg_save_file (instance, msgSave)

% Maintenance Auto : try
    instance.msgSaveFile = msgSave ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_msg_save_file', '') ;
% Maintenance Auto : end
