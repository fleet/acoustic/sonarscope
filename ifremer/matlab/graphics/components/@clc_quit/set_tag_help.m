% Initialisation du tag du bouton Help
% 
% Syntax
%   instance = set_tag_help(instance, tagHelp)
%
% Input Arguments
%   instance : instance de clc_quit
%   tagHelp  : tag pour le bouton help
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_tag_help.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_tag_help (instance, tagHelp)

% Maintenance Auto : try
    instance.tagHelp = tagHelp ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_tag_help', '') ;
% Maintenance Auto : end
