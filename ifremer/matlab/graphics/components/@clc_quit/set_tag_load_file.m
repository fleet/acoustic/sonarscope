% Initialisation du tag du bouton Load
% 
% Syntax
%   instance = set_tag_load_file(instance, tagLoad)
%
% Input Arguments
%   instance : instance de clc_quit
%   tagLoad  : tag pour le bouton load
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_tag_load_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_tag_load_file (instance, tagLoad)

% Maintenance Auto : try
    instance.tagLoadFile = tagLoad ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_tag_load_file', '') ;
% Maintenance Auto : end
