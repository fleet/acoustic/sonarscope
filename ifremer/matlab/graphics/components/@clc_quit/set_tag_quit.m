% Initialisation du tag du bouton Quit
% 
% Syntax
%   instance = set_tag_quit(instance, tagQuit)
%
% Input Arguments
%   instance : instance de clc_quit
%   tagQuit  : tag pour le bouton quit
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_tag_quit (instance, tagQuit)

% Maintenance Auto : try
    instance.tagQuit = tagQuit ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_tag_quit', '') ;
% Maintenance Auto : end
