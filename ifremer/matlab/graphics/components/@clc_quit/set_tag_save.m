% Initialisation du tag du bouton Save
% 
% Syntax
%   instance = set_tag_save (instance, tagSave)
%
% Input Arguments
%   instance : instance de clc_quit
%   tagSave  : tag pour le bouton save
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_tag_save.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_tag_save (instance, tagSave)

% Maintenance Auto : try
    instance.tagSave = tagSave ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_tag_save', '') ;
% Maintenance Auto : end
