% Initialisation du tag du bouton Save
% 
% Syntax
%   instance = set_tag_save_file(instance, tagSave)
%
% Input Arguments
%   instance : instance de clc_quit
%   tagSave  : tag pour le bouton save
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: set_tag_save_file.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/04/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = set_tag_save_file (instance, tagSave)

% Maintenance Auto : try
    instance.tagSaveFile = tagSave ;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'set_tag_save_file', '') ;
% Maintenance Auto : end
