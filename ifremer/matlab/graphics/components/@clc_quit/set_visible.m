% Initialisation de la visibilite du composant
% 
% Syntax
%   instance = set_visible(instance, visible)
%
% Input Arguments
%   instance : instance de clc_quit
%   visible  : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   instance : instance de clc_quit initialisee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function instance = set_visible(instance, visible)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(visible)
    if is_edit(instance)
        instance.globalFrame  = set_visible (instance.globalFrame, visible) ;
    end
    instance.cl_component = set_visible (instance.cl_frame, visible) ;
else
    my_warndlg('clc_quit/set_visible : Invalid value', 0);
end
