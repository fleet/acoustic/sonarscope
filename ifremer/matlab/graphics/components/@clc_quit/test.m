% Test graphique du composant
%
% Syntax
%   instance = test(instance)
% 
% Input Arguments
%   instance : instance de clc_quit
%
% Output Arguments
%   instance : instance de clc_quit
% 
% Examples
%    instance = test(clc_quit) ;
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    31/01/2001 - DCF - creation
%    23/03/2001 - DCF - maj optimisation code
%    06/04/2001 - DCF - ajout de la fonctionnalite load
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = test(instance)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Gestion des messages de call back (pour visualiser l arrivee des messages)
    
    % --------------------------------------------------------------------------
    % Creation d une figure ainsi que de l ancre du composant
    
    figure ('Visible', 'off') ;
    uicontrol (                            ...
        'Visible'  , 'on'                , ...
        'Tag'      , 'ancre_clc_quit'    , ...
        'Position' , [20 20 300 30]      ) ;
    
    % --------------------------------------------------------------------------
    % Creation et initialisation du composant
    
    instance = clc_quit ([]) ;
    instance = set_fquit (instance, 1) ;
    instance = set_fsave (instance, 1) ;
    instance = set_fhelp (instance, 1) ;
    instance = set_fload_file (instance, 1) ;
    instance = set_fsave_file (instance, 1) ;
    instance = set_name (instance, 'ancre_clc_quit') ;
    instance = set_user_name (instance, 'clc_quit') ;
    instance = set_user_cb (instance, 'gerer_callback') ;
    
    % --------------------------------------------------------------------------
    % Creation graphique du composant
    
    instance = editobj(instance) ;
    
    % --------------------------------------------------------------------------
    % Rend le resultat visible
    
    set(gcf, 'Visible', 'on') ;
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_quit', 'test', '') ;
% Maintenance Auto : end
