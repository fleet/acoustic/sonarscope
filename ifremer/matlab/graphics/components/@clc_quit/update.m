% Mise a jour du composant
%
% Syntax
%   instance = update(instance)
% 
% Input Arguments
%   instance : instance de clc_quit
%
% Remarks : doit etre surchargee
%
% See also clc_quit Authors
% Authors : DCF
% VERSION  : $Id: update.m,v 1.2 2002/07/08 13:44:47 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    30/01/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = update(instance)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Update graphique de la super classe
    
    instance.cl_component = update(instance.cl_component) ;
    
    % --------------------------------------------------------------------------
    % Update graphique des elements graphiques du composant
    
    if ~isempty (instance.globalFrame)
        instance.globalFrame = update (instance.globalFrame) ;
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_quit', 'update', '') ;
% Maintenance Auto : end
