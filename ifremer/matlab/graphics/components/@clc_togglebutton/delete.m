% Destruction graphique du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_togglebutton
%
% Output Arguments
%   this : instance de clc_togglebutton
%
% Remarks  : doit etre surchargee
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.4 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------
    % Destruction si l instance a une representation graphique
    
    if is_edit(this)
        
        % ----------------------------------------
        % Destruction graphique de la super classe
        
        this.cl_component = delete(this.cl_component);
        
        % -----------------------------------
        % Destruction des elements graphiques
        
        if ~isempty(this.f_globale)
            this.f_globale = delete(this.f_globale);
            this.f_globale = [];
            this.h_uic     = [];
        end
        
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_togglebutton', 'delete', lasterr);
% Maintenance Auto : end
