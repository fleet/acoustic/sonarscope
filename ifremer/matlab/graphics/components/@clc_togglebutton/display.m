% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments 
%   a : instance de clc_togglebutton
%
% Examples
%   a = clc_togglebutton
%   display(a) ;
%
% See also clc_togglebutton clc_togglebutton/char Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
