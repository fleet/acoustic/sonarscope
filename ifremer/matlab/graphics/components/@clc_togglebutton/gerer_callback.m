% Gestion des call back d'une IHM d'une instance
%
% Syntax
%   this = gerer_callback(this, cbName, ...)
% 
% Input Arguments
%   this : instance de clc_togglebutton
%   cbName   : nom de la call back
%
% Name-Value Pair Arguments
%   Les valeurs sont dynamiques, elles sont lues par :
%     this.msgClick
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks : doit etre surchargee
%
% See also clc_togglebutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

if length(varargin) == 1
    switch varargin{1}
        case this.msgClick
            this.etat = get(this.h_uic, 'Value');

        otherwise
            my_warndlg('clc_togglebutton/gerer_callback : Invalid message.', 1);
    end
else
    my_warndlg('clc_togglebutton/gerer_callback : Invalid arguments number.', 1);
end
