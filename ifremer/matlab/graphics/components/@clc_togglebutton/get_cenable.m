% Acces en lecture des proprietes d'une instance : renvoie la valeur de l'accessibilite du composant
% 
% Syntax
%   enable = get_cenable(this)
%
% Input Arguments
%   this : instance de clc_togglebutton
%
% Output Arguments
%   enable : accessibilite
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: get_cenable.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function enable = get_cenable(this)

% Maintenance Auto : try
    enable = get_enable(this.cl_component);
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_togglebutton', 'get_cenable', lasterr);
% Maintenance Auto : end
