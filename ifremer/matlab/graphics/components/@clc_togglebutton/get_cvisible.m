% Acces en lecture des proprietes d'une instance : renvoie la valeur de la visibilite du composant
% 
% Syntax
%   visible = get_cvisible(this)
%
% Input Arguments
%   this : instance de clc_togglebutton
%
% Output Arguments
%   visible : visibilite
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: get_cvisible.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function visible = get_cvisible(this)

% Maintenance Auto : try
    visible = get_visible(this.cl_component);
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_togglebutton', 'get_cvisible', lasterr);
% Maintenance Auto : end
