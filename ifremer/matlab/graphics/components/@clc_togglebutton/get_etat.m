% Acces en lecture des proprietes d'une instance : retourne l'etat du composant (0 ou 1)
% 
% Syntax
%   etat = get_etat(this)
%
% Input Arguments
%   this : instance de clc_togglebutton
%
% Output Arguments
%   etat : etat du composant
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: get_etat.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function etat = get_etat(this)

% Maintenance Auto : try
    etat = this.etat;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_togglebutton', 'get_etat', lasterr);
% Maintenance Auto : end
