% Acces au handle du bouton (
% Fonctionnalite necessaire pour y attacher un uicontextmenu, voir clc_image
% 
% Syntax
%   handle = get_handle(this)
%
% Input Arguments
%   this : instance de clc_togglebutton
%
% Output Arguments
%   handle : handle du composant
%
% See also clc_togglebutton Authors
% Authors : JMA
% VERSION  : $Id: get_handle.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    18/09/2003 - JMA - creation
% ----------------------------------------------------------------------------

function handle = get_handle(this)

% Maintenance Auto : try
    handle = this.h_uic;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_togglebutton', 'get_handle', lasterr);
% Maintenance Auto : end
