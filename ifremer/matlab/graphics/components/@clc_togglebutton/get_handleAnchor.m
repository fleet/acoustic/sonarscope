% Acces au handle de l'ancre du bouton (
%
% Syntax
%   handle = get_handleAnchor(this)
%
% Input Arguments
%   this : instance de clc_pushbutton
%
% Output Arguments
%   handle : handle de l'ancre du composant
%
% See also clc_pushbutton Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function handle = get_handleAnchor(this)
handle = this.handleAnchor;