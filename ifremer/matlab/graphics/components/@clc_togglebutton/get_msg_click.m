% Retourne le msg en cas de click sur le composant
% 
% Syntax
%   msgClick = get_msg_click(this)
%
% Input Arguments
%   this : instance de clc_togglebutton
%
% Output Arguments
%   msgClick : message en cas de clic
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: get_msg_click.m,v 1.6 2003/05/05 16:18:07 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    23/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgClick = get_msg_click(this)

% Maintenance Auto : try
    msgClick = this.msgClick;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_togglebutton', 'get_msg_click', lasterr);
% Maintenance Auto : end
