% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%    a : instance de clc_togglebutton
%
% Output Arguments
%    str : une chaine de caracteres representative de l instance
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: char_instance.m,v 1.5 2003/04/04 13:09:52 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    09/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
    str = [];
    
    % ----------
    % Traitement
    
    str{end+1} = sprintf('texte    <-> %s', this.texte);
    str{end+1} = sprintf('etat     <-> %d', this.etat);
    str{end+1} = sprintf('tooltip  <-> %s', this.tooltip);
    str{end+1} = sprintf('msgClick <-> %s', this.msgClick);
    str{end+1} = sprintf('CData    <-> %s', num2strCode(this.CData));
    
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);

    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_togglebutton', 'char_instance', lasterr);
% Maintenance Auto : end
