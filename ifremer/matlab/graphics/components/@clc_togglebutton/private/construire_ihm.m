% Creation de l'IHM d'une instance
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   athis : instance de clc_togglebutton
%
% Output Arguments
%   this : instance de clc_togglebutton
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: construire_ihm.m,v 1.4 2003/04/04 13:09:52 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/03/2001 - DCF - creation
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

% Maintenance Auto : try
    
    % ---------------------------------
    % Creation de la frame du composant
    
    this = creer_f_globale(this);
    
    % ------------------------------
    % Mise a jour de l accessibilite
    
    if strcmp(get_cenable(this), 'off')
        this.f_globale = set_enable(this.f_globale, 'off');
    end
    
    % ----------------------------
    % Mise a jour de la visibilite
    
    if strcmp(get_cvisible(this), 'off')
        this.f_globale = set_visible(this.f_globale, 'off');
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_togglebutton', 'construire_ihm', lasterr);
% Maintenance Auto : end
