% Creation des frames principales de l'IHM d'une instance
%
% Syntax
%   this = creer_f_globale(this)
%
% Input Arguments
%   this : instance de clc_togglebutton
%
% Output Arguments
%   this : instance de clc_togglebutton initialisee
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: creer_f_globale.m,v 1.4 2003/04/04 13:09:52 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    09/03/2001 - DCF - creation
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = creer_f_globale(this)

% Maintenance Auto : try
    
    % -------
    % Locales
    
    ihm      = [];
    
    % ---------------------------------------
    % Ancre du composant clc_edit_texte titre
    
    ihm{end+1}.Lig         = 1;
    ihm{end}.Col           = 1;
    ihm{end}.Style         = 'togglebutton';
    ihm{end}.String        = this.texte;
    ihm{end}.Value         = this.etat;
    ihm{end}.TooltipString = this.tooltip;
    ihm{end}.CData         = this.CData;
    ihm{end}.Callback      = built_cb(this, this.msgClick);
    
    % -------------------
    % Creation des frames
    
    this.f_globale = cl_frame([]);
    this.f_globale = set_handle_frame(this.f_globale, get_anchor(this));
    this.f_globale = set_inset_x(this.f_globale, get_inset_x(this));
    this.f_globale = set_inset_y(this.f_globale, get_inset_y(this));
    this.f_globale = set_max_lig(this.f_globale, 1);
    this.f_globale = set_max_col(this.f_globale, 1);
    this.f_globale = set_tab_uicontrols(this.f_globale, ihm);
    
    % -------------------------------
    % Determination des acces rapides
    
    handles = get_tab_uicontrols(this.f_globale);
    this.h_uic = handles(1);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_togglebutton', 'creer_f_globale', lasterr);
% Maintenance Auto : end
