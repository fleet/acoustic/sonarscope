% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%   this : instance de clc_togglebutton
%
% Name-Value Pair Arguments
%   texte             : texte affiche par le composant
%   etat              : etat du composant, 0 : relache, 1 : enfonce
%   tooltip           : info-bulle du composant
%   msgClick          : message envoye en cas de click souris
%   CData             : icone
%
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%   componentInsetX   : marges selon x
%   componentInsetY   : marges selon y
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   this  : Instance de la classe
%
% Examples
%    v = clc_togglebutton;
%    set(v, 'value', 3.14, 'tooltip', 'PI')
%
% See also clc_togglebutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

%% Lecture des arguments

[varargin, texte] = getPropertyValue(varargin, 'texte', []);
[varargin, etat] = getPropertyValue(varargin, 'etat', []);
[varargin, tooltip] = getPropertyValue(varargin, 'tooltip', []);
[varargin, msgClick] = getPropertyValue(varargin, 'msgClick', []);
[varargin, BusyAction] = getPropertyValue(varargin, 'BusyAction', []);
[varargin, CData] = getPropertyValue(varargin, 'CData', []);

[varargin, cvisible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, cenable] = getPropertyValue(varargin, 'componentEnable', []);

%% Initialisation des attributs

if ~isempty(texte)
    this = set_texte(this, texte);
end
if ~isempty(etat)
    this = set_etat(this, etat);
end
if ~isempty(tooltip)
    this = set_tooltip(this, tooltip);
end
if ~isempty(msgClick)
    this = set_msg_click(this, msgClick);
end
if ~isempty(CData)
    this = set_CData(this, CData);
end
if ~isempty(BusyAction)
    this = set_BusyAction(this, BusyAction);
end

if ~isempty(cvisible)
    this = set_cvisible(this, cvisible);
end
if ~isempty(cenable)
    this = set_cenable(this, cenable);
end

%% Traitement des message de la super classe

this.cl_component = set(this.cl_component, varargin{:});

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
