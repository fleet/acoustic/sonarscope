% Initialisation de l'icone du composant
% 
% Syntax
%   this = set_BusyAction(this, BusyAction)
%
% Input Arguments
%   this  : instance de clc_togglebutton
%   BusyAction : Icone
%
% Output Arguments
%   this : instance de clc_togglebutton initialisee
%
% See also clc_togglebutton Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_BusyAction(this, BusyAction)

this.BusyAction = BusyAction;

%% Mise � jour graphique

if is_edit(this)
    set(this.h_uic, 'BusyAction', this.BusyAction);
end
