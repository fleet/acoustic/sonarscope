% Initialisation de l'icone du composant
% 
% Syntax
%   this = set_CData(this, CData)
%
% Input Arguments
%   this  : instance de clc_togglebutton
%   CData : Icone
%
% Output Arguments
%   this : instance de clc_togglebutton initialisee
%
% See also clc_togglebutton Authors
% Authors : JMA
% VERSION  : $Id: set_CData.m,v 1.6 2003/05/05 16:25:47 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_CData(this, CData)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~isnumeric(CData)
    my_warndlg('clc_togglebutton/set_CData : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.CData = CData;
else
    this.CData = [];
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'CData', this.CData);
end
