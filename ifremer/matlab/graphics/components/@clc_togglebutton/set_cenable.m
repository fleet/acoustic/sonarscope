% Initialisation de l accessibilite du composant
% 
% Syntax
%   this = set_cenable(this, enable)
%
% Input Arguments
%   this : instance de clc_togglebutton
%   enable : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_togglebutton initialisee
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: set_cenable.m,v 1.5 2003/04/04 13:41:30 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_cenable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    if is_edit(this)
        this.f_globale  = set_enable(this.f_globale, enable);
    end
    this.cl_component = set_enable(this.cl_component, enable);
else
    my_warndlg('clc_togglebutton/set_cenable : Invalid value', 1);
end
