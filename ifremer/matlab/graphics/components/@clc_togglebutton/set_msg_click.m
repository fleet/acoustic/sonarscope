% Initialisation du message de call back pour un click souris
% 
% Syntax
%   this = set_msg_click(this, msgClick)
%
% Input Arguments
%   this  : instance de clc_togglebutton
%   msgClick : message de la call back
%
% Output Arguments
%   this : instance de clc_togglebutton initialisee
%
% See also clc_togglebutton Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_msg_click(this, msgClick)

isOk = 1;

%% Test validite des arguments :

if ~ischar(msgClick)
    my_warndlg('clc_togglebutton/set_msg_click : Invalid format', 1);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.msgClick = msgClick;
else
    this.msgClick = '';
end

%% Mise à jour graphique

if is_edit(this)
    set(this.h_uic, 'Callback', built_cb(this, this.msgClick));
end
