% Initialisation du texte affiche sur le bouton
% 
% Syntax
%   this = set_texte(this, texte);
%
% Input Arguments
%    this  : instance de clc_togglebutton
%    texte : texte affiche sur le bouton
%
% Output Arguments
%    this : instance de clc_togglebutton initialisee
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: set_texte.m,v 1.6 2003/05/05 16:25:47 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_texte(this, texte)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(texte)
    my_warndlg('clc_togglebutton/set_texte : Invalid format');
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.texte = texte;
else
    this.texte = '';
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'String', this.texte);
end
