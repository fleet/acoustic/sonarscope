% Initialisation de l'info-bulle du composant
% 
% Syntax
%   this = set_tooltip(this, tooltip);
%
% Input Arguments
%    this : instance de clc_togglebutton
%    tooltip : info-bulle du composant
%
% Output Arguments
%    this : instance de clc_togglebutton initialisee
%
% See also clc_togglebutton Authors
% Authors : DCF
% VERSION  : $Id: set_tooltip.m,v 1.6 2003/05/05 16:25:47 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_tooltip(this, tooltip)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(tooltip)
    my_warndlg('clc_togglebutton/set_tooltip : Invalid format', 1);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.tooltip = tooltip;
else
    this.tooltip = '';
end

% ---------------------
% Mise a jour graphique

if is_edit(this)
    set(this.h_uic, 'TooltipString', this.tooltip);
end
