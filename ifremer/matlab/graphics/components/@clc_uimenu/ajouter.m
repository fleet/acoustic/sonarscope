% Associe � un objet le menu
%
% Syntax
%   this = ajouter(this, ...)
%
% Input Arguments
%    handle : handle de l'objet associ� (axes, line, ...)
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = ajouter(this, varargin)

[varargin, tag] = getPropertyValue(varargin, 'tag', []);
if isempty(tag)
    str1 = 'clc_uimenu/ajouter : La ligne de menu ne peut �tre definie, il n''y  a pas de Tag.';
    str2 = 'clc_uimenu/ajouter : The menu line cannot be filled, there is no Tag.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% V�rification du message (unique)

if ~isdeployed
    ind = find(strcmp(tag, this.tag), 1, 'first');
    if ~isempty(ind)
        str = sprintf('Le tag "%s" existe d�j�.', tag);
        my_warndlg(str, 1);
        return
    end
end

%% Ajout d'une ligne

this.index{end+1}           = [];
this.message{end+1}         = [];
this.label{end+1}           = [];
this.checked{end+1}         = [];
this.separator{end+1}       = [];
this.ForegroundColor{end+1} = [];
this.IconData{end+1}        = [];
this.parent{end+1}          = [];

%% Sauvegarde du tag

this.tag{end+1} = tag;

%% Gestion des autres variables

this = set(this, get_indice(this, tag), varargin{:});

if isempty(this.checked{end})
    this.checked{end} = 'off';
end

if isempty(this.separator{end})
    this.separator{end} = 'off';
end

if isempty(this.ForegroundColor{end})
    this.ForegroundColor{end} = 'k';
end
