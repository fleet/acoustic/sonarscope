% Associe le menu � un objet graphique
%
% Syntax
%   this = associer(this, handle)
%
% Input Arguments
%    handle : handle de l'objet associ� (axes, line, ...)
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = associer(this, handle)

if ishandle(this.h.UserData)
    this = handle2obj(this.h.UserData);
end

this.h.associe = handle;

if ~isempty(this.h.menu)
    set(this.h.associe, 'UIcontextMenu', this.h.menu);
end

if ~isempty(this.h.UserData)
    obj2handle(this, this.h.UserData);
end
