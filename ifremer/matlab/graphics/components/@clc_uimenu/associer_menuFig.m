function [Handles, hMenu] = associer_menuFig(this, Label, hFig, hpremier, Handles, varargin)

[varargin, ExcludeTag] = getPropertyValue(varargin, 'ExcludeTag', []);
[varargin, UniqueTag]  = getPropertyValue(varargin, 'UniqueTag',  []); %#ok<ASGLU>

if isempty(Label)
    hMenu = hFig;
else
    hMenu = uimenu(hFig, 'Text', Label);
end

n = length(this.index);
for k=1:length(this.index)
    h(k) = this.index{k}; %#ok<AGROW>
    if k == 1
        h(n) = h(1); %#ok<AGROW> % Pour gagner du temps A L'ESSAI
    end
end

hpAll = get(h, 'Parent');
hpAll = [hpAll{:}];

c.parent = 0;
c.h = 0;
c.children = {};

%% Recherche des premiers niveaux

tree = c;
tree(1) = [];
for k=1:length(h)
    hp = get(h(k), 'Parent');
    if ~isempty(hp)
        k2 = find(hp == h); %#ok<EFIND>
        if isempty(k2)
            d = c;
            d.h = h(k);
            d.parent = hpremier;
            tree(end+1) = d; %#ok<AGROW>
        end
    end
end

%% Recherche des deuxi�mes niveaux

flag = [];
for k1=1:length(tree)
    [tree(k1), flag(end+1)] = getLevel(h, hpAll, tree(k1)); %#ok<AGROW>
end
flag = any(flag);

%% Recherche des troisi�mes niveaux

if flag
    flag = [];
    for k1=1:length(tree)
        for k2=1:length(tree(k1).children)
            [tree(k1).children{k2}, flag(end+1)] = ...
                getLevel(h, hpAll, tree(k1).children{k2}); %#ok<AGROW>
        end
    end
    flag = any(flag);
end

%% Recherche des quatri�mes niveaux

if flag
    flag = [];
    for k1=1:length(tree)
        for k2=1:length(tree(k1).children)
            for k3=1:length(tree(k1).children{k2}.children)
                [tree(k1).children{k2}.children{k3}, flag(end+1)] = ...
                    getLevel(h, hpAll, tree(k1).children{k2}.children{k3}); %#ok<AGROW>
            end
        end
    end
    flag = any(flag);
end

%% Recherche des cinqui�mes niveaux

if flag
    flag = [];
    for k1=1:length(tree)
        for k2=1:length(tree(k1).children)
            for k3=1:length(tree(k1).children{k2}.children)
                for k4=1:length(tree(k1).children{k2}.children{k3}.children)
                    [tree(k1).children{k2}.children{k3}.children{k4}, flag(end+1)] = ...
                        getLevel(h, hpAll, tree(k1).children{k2}.children{k3}.children{k4}); %#ok<AGROW>
                end
            end
        end
    end
    flag = any(flag);
end

%% Recherche des sixi�mes niveaux

if flag
    flag = [];
    for k1=1:length(tree)
        for k2=1:length(tree(k1).children)
            for k3=1:length(tree(k1).children{k2}.children)
                for k4=1:length(tree(k1).children{k2}.children{k3}.children)
                    for k5=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children)
                        [tree(k1).children{k2}.children{k3}.children{k4}.children{k5}, flag(end+1)] = ...
                            getLevel(h, hpAll, tree(k1).children{k2}.children{k3}.children{k4}.children{k5}); %#ok<AGROW>
                    end
                end
            end
        end
    end
    flag = any(flag);
end

%% Recherche des septi�mes niveaux

if flag
    flag = [];
    for k1=1:length(tree)
        for k2=1:length(tree(k1).children)
            for k3=1:length(tree(k1).children{k2}.children)
                for k4=1:length(tree(k1).children{k2}.children{k3}.children)
                    for k5=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children)
                        for k6=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children)
                            [tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}, flag(end+1)] = ...
                                getLevel(h, hpAll, tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}); %#ok<AGROW>
                        end
                    end
                end
            end
        end
    end
    flag = any(flag);
end

%% Recherche des huiti�me niveaux

if flag
    flag = [];
    for k1=1:length(tree)
        for k2=1:length(tree(k1).children)
            for k3=1:length(tree(k1).children{k2}.children)
                for k4=1:length(tree(k1).children{k2}.children{k3}.children)
                    for k5=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children)
                        for k6=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children)
                            for k7=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}.children)
                                [tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}.children{k7}, flag(end+1)] = ...
                                    getLevel(h, hpAll, tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}.children{k7}); %#ok<AGROW>
                            end
                        end
                    end
                end
            end
        end
    end
    %     flag = any(flag);
end

%% Filtre eventuel sur les Tag

if ~isempty(UniqueTag) || ~isempty(ExcludeTag)
    sub = true(1, length(tree));
    for k1=1:length(tree)
        Tag = get(tree(k1).h, 'Tag');
        
        if ~isempty(UniqueTag)
            if strcmp(Tag, UniqueTag)
                sub = k1;
                break
            end
        end
        
        if ~isempty(ExcludeTag)
            if strcmp(Tag, ExcludeTag)
                sub(k1) = 0;
            end
        end
    end
    tree = tree(sub);
end

%% Recopie des attributs

for k1=1:length(tree)
    [h1, Handles] = copieLevel(h, hMenu, tree(k1), Handles);
    
    for k2=1:length(tree(k1).children)
        [h2, Handles]  = copieLevel(h, h1, tree(k1).children{k2}, Handles);
        
        for k3=1:length(tree(k1).children{k2}.children)
            [h3, Handles]  = copieLevel(h, h2, tree(k1).children{k2}.children{k3}, Handles);
            
            for k4=1:length(tree(k1).children{k2}.children{k3}.children)
                [h4, Handles]  = copieLevel(h, h3, tree(k1).children{k2}.children{k3}.children{k4}, Handles);
                
                for k5=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children)
                    [h5, Handles] = copieLevel(h, h4, tree(k1).children{k2}.children{k3}.children{k4}.children{k5}, Handles);
                    
                    for k6=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children)
                        [h6, Handles]  = copieLevel(h, h5, tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}, Handles);
                        
                        for k7=1:length(tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}.children)
                            [~, Handles]  = copieLevel(h, h6, tree(k1).children{k2}.children{k3}.children{k4}.children{k5}.children{k6}.children{k7}, Handles);
                        end
                    end
                end
            end
        end
    end
end


function  [tree, flag] = getLevel(h, hpAll, tree)

flag = 0;
c.parent = 0;
c.h = 0;
c.children = {};
sub = find(hpAll == tree.h);
for k=1:length(sub)
    d = c;
    d.h = h(sub(k));
    d.parent = hpAll;
    n = length(tree.children);
    tree.children{n+1} = d;
    flag = 1;
end


function [h2, Handles] = copieLevel(h, h1, tree, Handles)

k = find(h == tree.h);
Callback        = get(h(k), 'Callback');
% Label           = get(h(k), 'Label');
Label           = get(h(k), 'Text'); % Modif JMA le 20/05/2021
Separator       = get(h(k), 'Separator');
ForegroundColor = get(h(k), 'ForegroundColor');
Checked         = get(h(k), 'Checked');
Enable          = get(h(k), 'Enable');
Tag             = get(h(k), 'Tag');
IconData        = getappdata(h(k), 'IconData');
h2 = uimenu(h1, ...
    'Text',            Label, ...
    'Callback',        Callback, ...
    'Separator',       Separator, ...
    'ForegroundColor', ForegroundColor, ...
    'Checked',         Checked, ...
    'Enable',          Enable, ...
    'Tag',             Tag);
Handles.(Tag)(2) = h2;
setappdata(h2, 'IconData', IconData);

% 	Position = [1]
% 	BeingDeleted = off
% 	ButtonDownFcn =
% 	Clipping = on
% 	CreateFcn =
% 	DeleteFcn =
% 	BusyAction = cancel
% 	HandleVisibility = on
% 	HitTest = on
% 	Interruptible = on
% 	Selected = off
% 	SelectionHighlight = on
% 	Tag = ImportationFiles
% 	Type = uimenu
% 	UIContextMenu = []
% 	UserData = []
% 	Visible = on
