% Construction d'une call back
%
% Syntax
%   cb = built_cb (this, msgCb)
% 
% Input Arguments
%   this : instance de clc_uimenu
%   msgCb    : message associe a la call back
%
% Output Arguments
%   cb       : chaine de caracteres permettant d initialiser la call back
%
% See ALSO : clc_uimenu
% Authors  : LD
% ----------------------------------------------------------------------------

function cb = built_cbb (this, msgCb)

my_warndlg('une erreur', 1);
cb = [] ;

% --------------------------------------------------------------------------
% Ne peut construire une call back que si le nom de la classe utilisatrice
% ainsi que le nom de son manager sont connus

if ~isempty (this.componentUserName) && ~isempty(this.componentUserCb)
    cb {end+1} = 'eval (''';
    cb {end+1} = this.componentUserCb;
    cb {end+1} = ' (';
    cb {end+1} = this.componentUserName;
    cb {end+1} = '([]), ';
    cb {end+1} = '''''';
    cb {end+1} = msgCb;
    cb {end+1} = '''''';
    cb {end+1} = ') ; '');'                 ;

    cb = strcat (cb{:});
else
    my_warndlg('clc_uimenu/built_cb : Nom du composant ou callback non renseigné.', 1);
end
