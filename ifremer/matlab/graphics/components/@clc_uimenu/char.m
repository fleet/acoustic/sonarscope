% Transformation en chaine de caracteres d'un objet de classe clc_uimenu
%
% Syntax 
%   s = char(this)
%
% Input Arguments
%   this : Une instance de classe clc_uimenu
%
% Output Arguments
%   s : Chaine de caracteres
%
% Examples
%   this = clc_uimenu;
%   s = char(this);
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function str = char (this)

str = [];

%% Traitement tableau ou instance seule

[m, n] = size(this);

if (m*n) > 1
    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);
    
else
    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
