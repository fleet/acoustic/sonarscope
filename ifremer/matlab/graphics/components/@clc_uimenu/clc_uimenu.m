% Constructeur de clc_uimenu, classe composant
%
% Syntax
%   this = clc_uimenu(varargin)
%
% Input Arguments
%    varargin : propertyName / propertyValue
%
% Name-Value Pair Arguments
%   label       : textes affiches dans le menu
%   parent      : propriete identifiant les sous menus
%   separator   : separateur
%   checked     : propriete booleene pour les menus
%   message     : messages renvoye a la fonction de l'application
%   tag         : tag des IUmenu
%   ForegroundColor : Couleur du texte
%   index       : handle des iumenu
%
% Output Arguments
%    this : instance de clc_uimenu
%
% Examples
%    s = clc_uimenu([]);
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = clc_uimenu(varargin)

% Appel� dans :
% creer_cpn_Import                  : Bouton et menu, le bouton peut �tre supprim�
% creer_cpn_Export                  : Bouton et menu, le bouton peut �tre supprim�
% creer_cpn_Stats                   : Bouton cach� et menu, le bouton peut �tre supprim�
% creer_cpn_ComputeSonar            : Bouton cach� et menu, le bouton peut �tre supprim�
% creer_cpn_Compute                 : Bouton cach� et menu, le bouton peut �tre supprim�
% creer_cpn_ZoneEtude               : Bouton uniquement
% creer_cpn_Contraste               : Bouton uniquement
% creer_cpn_TypeInteractionSouris   : Bouton uniquement
% creer_cpn_Colormap                : Bouton uniquement
% creer_cpn_Info                    : Bouton uniquement

this.index       = [];
this.tag         = [];

this.message     = [];
this.label       = [];
this.parent      = [];
this.checked     = [];
this.separator   = [];
this.IconData    = [];
this.ForegroundColor = [];

this.typeMenu = 'uicontextmenu'; %  uimenu / uicontextmenu

%% handle

this.h.menu     = [];
this.h.associe  = [];
this.h.UserData = [];
this.Handles    = [];

%% Super-classe

if (nargin == 1) && isempty(varargin{1})
    composant = cl_component([]);
else
    composant = cl_component;
end

%% Cr�ation de l'objet

this = class(this, 'clc_uimenu', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

%% TODO : mettre ceci dans le set ?

[varargin, typeMenu] = getPropertyValue(varargin, 'typeMenu', []);
if ~isempty(typeMenu)
    this = set_typeMenu(this, typeMenu);
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this.cl_component = set(this.cl_component, varargin{:});
