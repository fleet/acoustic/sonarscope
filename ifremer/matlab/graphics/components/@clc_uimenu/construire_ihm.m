% Construction de base de l IHM
%
% Syntax
% this = construire_ihm(this)
%
% Input Arguments
%    this : instance de clc_uimenu
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% V�rification de l'existence d'un objet graphique associe

if isempty(this.h.associe)
    my_warndlg('clc_uimenu/editobj : Le menu n''est pas associe.', 1);
    return
end
    

% Trouver un tag coherent!!!!    MODIFS LUR...
% on donne une visibilit� off au uicontextmenu
tag = cell2str(strcat('essaiuicontextmenu', this.tag(1)));

switch this.typeMenu
    case 'uimenu'
        this.h.menu = uimenu('visible', 'off', 'tag', tag);
    case 'uicontextmenu'
        this.h.menu = uicontextmenu('visible', 'off', 'tag', tag);
end
% TODO : cr�er une propri�t� typeMenu : uimenu / uicontextmenu et faire un if
this.h.UserData = this.h.associe;

%% D�finition des uimenus

taille = size(this.tag,2);
for k=1:taille
    cb = built_cb(this, this.message{k});
    this.index{k} = uimenu(this.h.menu, ...
        'tag',             this.tag{k}, ...
        'callback',        cb, ...
        'checked',         this.checked{k}, ...
        'separator',       this.separator{k}, ...
        'ForegroundColor', this.ForegroundColor{k}, ...
        'Text',            this.label{k}, ...
        'BusyAction',      'cancel');
    
    this.Handles.(this.tag{k}) = this.index{k};
    
    if ~isempty(this.parent{k})
        set(this.index{k}, 'parent', this.index{get_indice(this, this.parent{k})});
    end
    
    % Positionnement d'une variable Relai associ�e au Menu
    iconData = this.IconData{k};
    if ~isempty(iconData)
        setappdata(this.index{k}, 'IconData', iconData);
    end
    this.index{k} = this.index{k};
    if k == 1
        this.index{taille} = this.index{k}; % Pour gagner du temps A L'ESSAI
    end
end

%% Mise � jour de l'accessibilit�

if strcmp(get_enable(this.cl_component), 'off')
    s = get(this.h.menu);
    if isfield(s, 'Enable')
        set(this.h.menu, 'Enable', 'off');
    end
end

%% Mise � jour de la visibilit�

if strcmp(get_visible(this.cl_component), 'off')
    set(this.h.menu, 'visible', 'off');
end
