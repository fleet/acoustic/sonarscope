function cmenu = creer_menu_help(cmenu, parent, msg1, nomFicHtml, varargin)

[varargin, Tag] = getPropertyValue(varargin, 'Tag', []); %#ok<ASGLU>

msg{1} = msg1;
msg{2} = 'Help';
msg{3} = nomFicHtml;
[~, TagIHM] = fileparts(nomFicHtml);

if ~isempty(Tag)
    TagIHM = [TagIHM Tag];
end

TagIHM = strrep(TagIHM, '-', '');
cmenu = ajouter(cmenu, 'parent', parent, 'tag', TagIHM, 'message', msg, ...
    'label', Lang('Aide', 'Help'), 'separator', 'on', 'ForegroundColor', 'm');
