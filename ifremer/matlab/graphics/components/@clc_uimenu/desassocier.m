% desassocie le menu � l'objet graphique
%
% Syntax
%   this = desassocier (handle)
%
% Input Arguments
%    handle : handle de l'objet associ� (axes, line, ...)
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = desassocier(this)

if ~isempty(this.h.UserData)
    this = handle2obj(this.h.UserData);
end

if ~isempty(this.h.associe)
    set(this.h.associe, 'UIcontextMenu', []);
end
this.h.associe = [];

obj2handle(this, this.h.UserData);
