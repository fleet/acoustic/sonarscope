% Affiche des informations sur l'instance de la classe clc_uimenu
%
% Syntax 
%   display(this)
%
% Input Arguments
%   this : instance de la classe clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
