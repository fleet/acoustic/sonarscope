% Representation graphique de l'instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_uimenu
%
% OUPUT PARAMETERS :
%   this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Cr�ation de l interface graphique

if ~is_edit(this)
    this.cl_component = editobj(this.cl_component);
    this = construire_ihm(this);
end

obj2handle(this, this.h.UserData);
