% Retourne l'adresse du choix
%
% Syntax
% this = get_adresse(this, indice)
%
% Input Arguments
%    message : message du choix
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% VERSION  : $Id$
% ----------------------------------------------------------------------------

function adr = get_adresse(this, indice)

adr = [];

if indice <= size(this.tag, 2) && indice>0
    adr = this.index{indice};
else
    my_warndlg('clc_uimenu/get_adresse : Indice invalide.', 1) ;
    return
end
