% Retourne le nom du handle
%
% Syntax
%   s = get_handleFieldName(this, k)
%
% Input Arguments
%     indice : indice dans le tableau
%
% Output Arguments
%    s : Nom du hadle d'indice k de l'instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function msg = get_handleFieldName(this, indice)

msg = [];
F = fieldnames(this.Handles);
if (indice <= length(F)) && (indice > 0)
    msg = F{indice};
else
    my_warndlg('clc_uimenu/get_message : L''indice depasse les bornes.', 1) ;
    return
end
