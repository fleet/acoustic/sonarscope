% Retourne la valeur du handle
%
% Syntax
%   h = get_handleValue(this, k)
%
% Input Arguments
%     indice : indice dans le tableau
%
% Output Arguments
%    h : Valeur du handle d'indice k de l'instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function h = get_handleValue(this, indice)

F = fieldnames(this.Handles);
if (indice <= length(F)) && (indice > 0)
    h = this.Handles.(F{indice});
else
    my_warndlg('clc_uimenu/get_message : L''indice d�passe les bornes.', 1) ;
    return
end
