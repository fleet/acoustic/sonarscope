% Supprime un choix du menu relativement a son indice
%
% Syntax
% this = get_indice(this, message)
%
% Input Arguments
%    message : message du choix
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% -------------------------------------------------------------------------

function ind = get_indice(this, tag)
ind = find(strcmp(tag, this.tag), 1, 'first');
