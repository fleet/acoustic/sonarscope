% Retourne l'adresse du choix
%
% Syntax
% this = get_adresse(this, indice)
%
% Input Arguments
%    message : message du choix
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% VERSION  : $Id$
% ----------------------------------------------------------------------------

function msg = get_message(this, indice)

msg = [];
if indice <= size(this.index, 2) && indice>0
    msg = this.message{indice};
else
    my_warndlg('clc_uimenu/get_message : L''indice depasse les bornes.', 1) ;
    return
end
