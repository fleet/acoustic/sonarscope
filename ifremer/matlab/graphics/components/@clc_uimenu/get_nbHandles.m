% Retourne le nombre de handles
%
% Syntax
%   n = get_nbHandles(this)
%
% Output Arguments
%    n : nombre de handles de l'instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function n = get_nbHandles(this)
n = length(fieldnames(this.Handles));
