% Retourne l'adresse du choix
%
% Syntax
% v = get_tag(this, indice)
%
% Input Arguments
%     indice : indice dans le tableau
%
% Output Arguments
%    v : tag de l'instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function msg = get_tag(this, indice)

msg = [];
if indice <= size(this.tag, 2) && indice>0
    msg = this.tag{indice};
else
    my_warndlg('clc_uimenu/get_message : L''indice depasse les bornes.', 1) ;
    return
end
