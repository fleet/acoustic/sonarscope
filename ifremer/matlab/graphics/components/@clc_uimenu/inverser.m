% Supprime un choix du menu relativement a son indice
%
% Syntax
% this = inverser (this, tag1, tag2)
%
% Input Arguments
%   indice1 : indice du choix dans le menu
%   indice2 : indice du choix dans le menu
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% VERSION  : $Id$
% ----------------------------------------------------------------------------

function this = inverser(this, tag1, tag2)

if ~isempty(get_indice(this, tag1)) && ~isempty(get_indice(this, tag2))
    indice1 = get_indice(this, tag1);
    indice2 = get_indice(this, tag2);
else
    indice1 = 0;
end

taille = size(this.tag,2);

if (indice1 > taille) || (indice1 < 1)
    my_warndlg('clc_uimenu/supprime : Premier indice invalide.', 1);
    return
end

if (indice2 > taille) || (indice2 < 1)
    my_warndlg('clc_uimenu/supprime : Second indice invalide.', 1);
    return
end

tempindex = this.index{indice1};
temptag = this.tag{indice1};
tempmessage = this.message{indice1};
templabel = this.label{indice1};
tempchecked = this.checked{indice1};
tempparent = this.parent{indice1};
tempseparator = this.separator{indice1};
tempForegroundColor = this.ForegroundColor{indice1};

this.index{indice1} = this.index{indice2};
this.tag{indice1} = this.tag{indice2};
this.message{indice1} = this.message{indice2};
this.label{indice1} = this.label{indice2};
this.checked{indice1} = this.checked{indice2};
this.parent{indice1} = this.parent{indice2};
this.separator{indice1} = this.separator{indice2};
this.ForegroundColor{indice1} = this.ForegroundColor{indice2};

this.index{indice2} = tempindex;
this.tag{indice2} = temptag;
this.message{indice2} = tempmessage;
this.label{indice2} = templabel;
this.checked{indice2} = tempchecked;
this.parent{indice2} = tempparent;
this.separator{indice2} = tempseparator;
this.ForegroundColor{indice2} = tempForegroundColor;
