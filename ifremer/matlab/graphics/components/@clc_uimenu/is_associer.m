% Savoir si le menu est associe
%
% Syntax 
%   v = is_associer(this)
%
% Input Arguments
%   this : instance de classe clc_uimenu
%
% Output Arguments
%   v : booleen {0} non associe / 1 associe
%
% See also clc_uimenu Authors
% Authors : LD
% VERSION  : $Id$
%-------------------------------------------------------------------------------

function v = is_associer(this)

% Maintenance Auto : try
    
    if ~isempty(this.h.associe)
        v = 1;
    else
        v = 0;
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_uimenu','is_associer','');
% Maintenance Auto : end
