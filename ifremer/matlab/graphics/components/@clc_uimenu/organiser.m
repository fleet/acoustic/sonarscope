% mets en ordre les sous menu
%
% Syntax
% this = organiser (this)
%
% Input Arguments
%   this : instance de clc_uimenu
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = organiser(this)
