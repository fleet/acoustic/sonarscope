% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this)
%
% Input Arguments
%   this : instance de clc_uimenu
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% See also clc_uimenu Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

if isempty(this.Handles)
    F = [];
else
    F = fieldnames(this.Handles);
end

%% Traitement

if my_verLessThanR2014b
    str{end+1} = sprintf('h.menu      <-> %f', this.h.menu);
    str{end+1} = sprintf('h.associe   <-> %f', this.h.associe);
    str{end+1} = sprintf('h.UserData  <-> %f', this.h.UserData);
else
    %str{end+1} = sprintf('h.menu      <-> %f', this.h.menu);
    str{end+1} = sprintf('h.associe   <-> %f', this.h.associe);
    if ~isempty(this.h.UserData)
        str{end+1} = sprintf('h.UserData  <-> %f', this.h.UserData.Value);
    end
end

nbligne = size(this.index,2);
for k=1:nbligne
    str{end+1} = sprintf('-----Element (%d) -------', k); %#ok<AGROW>
    if my_verLessThanR2014b
        str{end+1} = sprintf('index           <-> %f', this.index{k}); %#ok<AGROW>
    else
%         str{end+1} = sprintf('index           <-> %f', this.index{k}); %#ok<AGROW>
    end
    str{end+1} = sprintf('tag             <-> %s', this.tag{k}); %#ok<AGROW>
    pppp = this.message{k};
    if ~isempty(pppp)
        str{end+1} = sprintf('message         <-> %s', pppp{1}); %#ok<AGROW>
        for k2=2:length(pppp)
            str{end+1} = sprintf('                <-> %s', pppp{k2}); %#ok<AGROW>
        end
    end
    str{end+1} = sprintf('label           <-> %s', this.label{k}); %#ok<AGROW>
    str{end+1} = sprintf('parent          <-> %s', num2str(this.parent{k})); %#ok<AGROW>
    str{end+1} = sprintf('checked         <-> %s', this.checked{k}); %#ok<AGROW>
    str{end+1} = sprintf('separator       <-> %s', this.separator{k}); %#ok<AGROW>
    str{end+1} = sprintf('ForegroundColor <-> %s', this.ForegroundColor{k}); %#ok<AGROW>
    if ~isempty(F)
        str{end+1} = sprintf('handle          <-> %s', F{k}); %#ok<AGROW>
    end
%     CommentÚ car pas synchronisÚ
end

str{end+1} = sprintf('--- heritage cl_component ---');
str{end+1} = char(this.cl_component);

%% ConcatÚnation en une chaine

str{end+1} = sprintf('--- heritage cl_component ---');
str{end+1} = char(this.cl_component);
%str = cell2str(str);

%% ConcatÚnation en une chaine

str = cell2str(str);
