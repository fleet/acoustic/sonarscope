% Recuperation des valeurs des membres de l'objet
%
% Syntax
%   this = set(this, indice, 'PropertyName', ...)
%
% Input Arguments
%   this : Une instance de classe clc_uimenu
%   indice : indice du choix � modifier
%   'PropertyName' : un ou plusieurs nom de propri�t�
%
% Output Arguments
%   this : instance de clc_uimenu
%
% PROPERTY NAME / PROPERTY VALUE :
%   message : chaine de caract�res envoy� a la routine de gestion
%   label : texte visible sur le menu
%   separator : pour separer les labels
%   parent : pour faire des sous menus
%   checked : pour verifier l'etat du choix
%
% See also clc_uimenu get methodsview('clc_uimenu') Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = set(this, indice, varargin)

% taille = size(this.tag, 2);

[varargin, hUserData] = getPropertyValue(varargin, 'hUserData', []);
if ~isempty(hUserData)
    this.h.UserData = hUserData;
    indice = 1;
end

% if (taille < indice) || (taille < 1)
%     my_warndlg('clc_uimenu/set : Indice invalide.', 1);
%     return
% end

%% R�cuperation des valeurs transmises

[varargin, this.label{indice}] = getPropertyValue(varargin, 'label', this.label{indice});
if ~isempty(varargin)
    [varargin, this.message{indice}] = getPropertyValue(varargin, 'message', this.message{indice});
    if ~isempty(varargin)
        [varargin, this.parent{indice}] = getPropertyValue(varargin, 'parent', this.parent{indice});
        if ~isempty(varargin)
            [varargin, this.ForegroundColor{indice}] = getPropertyValue(varargin, 'ForegroundColor', this.ForegroundColor{indice});
            if ~isempty(varargin)
                [varargin, this.separator{indice}] = getPropertyValue(varargin, 'separator', this.separator{indice});
                if ~isempty(varargin)
                    [varargin, this.checked{indice}] = getPropertyValue(varargin, 'checked', this.checked{indice});
                    if ~isempty(varargin)
                        [varargin, this.IconData{indice}] = getPropertyValue(varargin, 'IconData', this.IconData{indice});
                        if ~isempty(varargin)
                            [varargin, this.tag{indice}] = getPropertyValue(varargin, 'tag', this.tag{indice}); %#ok<ASGLU>
                        end
                    end
                end
            end
        end
    end
end

if nargout == 0
    assignin('caller', inputname(1), this);
end
