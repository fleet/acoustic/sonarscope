% Supprime un choix du menu en fonction de son indice
%
% Syntax
% this = supprime (this, tag)
%
% Input Arguments
%    indice : indice du choix dans le menu
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% VERSION  : $Id$
% ----------------------------------------------------------------------------

function this = supprimer(this, varargin)

if isempty(varargin)
    this.index = [];
    this.tag = [];

    this.message = [];
    this.label = [];
    this.parent = [];
    this.checked = [];
    this.separator = [];
    this.ForegroundColor = [];
    return
end

% ----------------------------------------------------
% recursivite dans le cas de menu ayant des 'children'

if ~isempty(get_indice(this, varargin(1)))
    indice = get_indice(this, varargin(1));
%     parent = get(this.index{indice}, 'parent');
    enfant = get(this.index{indice}, 'children');

    if ~isempty(enfant)

        % ---------------------------------------
        % recherche du handle dans la table index et son indice (i1)

        for i=1:length(enfant)
            i1 = 1;
            while (enfant(i) ~= this.index{i1}) && i1<length(this.tag)
                i1 = i1+1;
            end

            % --------------
            % appel recursif

            this = supprimer(this, get_tag(this, i1));
        end
    end
else
    indice = 0;
end

taille = size(this.tag, 2);
if (indice > taille) || (indice < 1)
    my_warndlg('clc_uimenu/supprime : Indice invalide.', 1);
    return
else

    % -----------------
    % decalage a droite

    for i=indice:(taille-1)
        this.index{i} = this.index{i+1};
        this.tag{i} = this.tag{i+1};
        this.message{i} = this.message{i+1};
        this.label{i} = this.label{i+1};
        this.checked{i} = this.checked{i+1};
        this.parent{i} = this.parent{i+1};
        this.separator{i} = this.separator{i+1};
        this.ForegroundColor{i} = this.ForegroundColor{i+1};
    end

    % ----------------------------
    % effacement du dernier champs

    this.index(end) = [];
    this.tag(end) = [];
    this.message(end) = [];
    this.label(end) = [];
    this.checked(end) = [];
    this.parent(end) = [];
    this.separator(end) = [];
    this.ForegroundColor(end) = [];

end
