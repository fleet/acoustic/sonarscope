% mise a jour graphique
%
% Syntax
%   this = update (this)
%
% Input Arguments
%    this : instance de clc_uimenu
%
% Output Arguments
%    this : instance de clc_uimenu
%
% See also clc_uimenu Authors
% Authors : LD
% ----------------------------------------------------------------------------

function this = update (this)

%% V�rification de l'existence d'un objet associ�

if isempty(this.h.associe)
    my_warndlg('clc_uimenu/editobj : Le menu n''est pas associe');
    return
end

%% Remaniement des handle du menu

taille = size(this.tag,2);
nvhandle = [];


for k=1:taille
    if isempty(this.parent{k})
        nvhandle = [nvhandle ; this.index{k}]; %#ok<AGROW>
    end
end

%% Mise � jour graphique

set(this.h.menu, 'children', nvhandle);

%% Mise � jour des champs

taille = size(this.index,2);
for k=1:taille
    set(this.index{k}, ...
        'tag',             this.tag{k}, ...
        'callback',        built_cb(this.cl_component,this.message{k}), ...
        'checked',         this.checked{k}, ...
        'separator',       this.separator{k}, ...
        'ForegroundColor', this.ForegroundColor{k}, ...
        'label',           this.label{k});

    if ~isempty(this.parent{k})
        set(this.index{k}, 'parent', this.index{this.parent{k}});
    else
        set(this.index{k}, 'parent', this.h.menu);
    end
end
