% D�termine la position en pixel � partir d'une position en ligne, colonne
%
% Syntax
%   position = calculer_position(this, lig, col)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%   lig  : indices de ligne
%   col  : indices de colonne
%
% Output Arguments
%   position : matrice de position [x y w h]
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function position = calculer_position(this, lig, col)

%% Test sur le nombre de lignes et le nombre de colonnes

minLig = min(lig);
maxLig = max(lig);
minCol = min(col);
maxCol = max(col);

%% Positionnement en Y (calcul sur les lignes) inversion du Y car origine matlab : coin BAS gauche : symetrie utilisee

% if isempty(this.epaisseurY)
%     this.epaisseurY = 1;
% end
y(1) = this.position(2) + this.insetY + (this.insetY + this.epaisseurY) * (minLig-1);
y(2) = this.position(2) + (this.insetY + this.epaisseurY) * (maxLig);
ySym = this.position(2) + this.position(4) / 2;
y    = 2*ySym - y;

%% Positionnement en X (calcul sur les colonnes)

x(1) = this.position(1) + this.insetX + (this.insetX + this.epaisseurX) * (minCol-1);
x(2) = this.position(1) + (this.insetX + this.epaisseurX) * (maxCol);

%% Position retourn�e

position = [x(1), y(2), x(2)-x(1), y(1)-y(2)];

%% Position strictement positive !!!

% ind = find(position < 0);
% [m,n] = size(ind);
% if (m*n) > 0
%     my_warndlg('clc_abstract_layout/calculer_position : Invalid;
%     position', 1);
%     position(ind) = 0;
% end
