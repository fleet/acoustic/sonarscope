% Determine la position en pixel a partir d une position en ligne, colonne
%
% Syntax
%   position = calculer_position (instance, lig, col)
%
% Input Arguments
%   instance : instance de cl_abstract_layout
%   lig      : indices de ligne
%   col      : indices de colonne
%
% Output Arguments
%   position : matrice de position [x y w h]
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function position = calculer_position_panel(instance, lig, col)

% position = [];

%% Gestion insetX et insetY

insetX = get(instance,'InsetX');
insetY = get(instance,'InsetY');
if (insetX > 1) || (insetX < 0)
    disp('error insetX doit etre compris entre 0 et 1 ');
    insetX=0;
end
if (insetY > 1) || (insetY < 0)
    disp('error insetY doit etre compris entre 0 et 1 ');  
    insetY=0;
end

%% Vérification des conditions de travail

% if ~isempty(instance.maxLig) && ~isempty(instance.maxCol) && ~isempty(instance.position)

    %% Test sur le nombre de lignes et le nombre de colonnes

    minLig = min(lig);
    maxLig = max(lig);
    minCol = min(col);
    maxCol = max(col);

%     if (instance.maxLig >= maxLig) && (instance.maxCol >= maxCol)
    
        % ----------------------------------------------------------------------
        % Positionnement en Y (calcul sur les lignes)
        % inversion du Y car origine matlab : coin BAS gauche : symetrie
        % utilisee
        
        y(1) = 1-(1/instance.maxLig*(maxLig)-insetY/instance.maxLig*(maxLig));
        
        y(2) = (1-(insetY*(instance.maxLig+1)))/instance.maxLig*(maxLig-minLig+1)+(maxLig-minLig)*insetY;
        
        % ---------------------------------------------
        % Positionnement en X (calcul sur les colonnes)
        %norm=1/instance.maxCol;

        x(1) = 1/instance.maxCol*(minCol-1)+insetX-insetX/instance.maxCol*(minCol-1);

        x(2) = (1-(insetX*(instance.maxCol+1)))/instance.maxCol*(maxCol-minCol+1)+(maxCol-minCol)*insetX;

        % ------------------
        % Position retournee

        position = [x(1), y(1), x(2), y(2)];
        % ---------------------------------
        % Position strictement positive !!!

        ind = find(position < 0);
        [m,n] = size(ind);
        if (m*n) > 0
            my_warndlg('clc_abstract_layout/calculer_position : Invalid position', 1);
            position(ind) = 0;
        end

%     else
%         my_warndlg('clc_abstract_layout/calculer_position : Invalid;
%         dimensions', 1);
%     end
% end
