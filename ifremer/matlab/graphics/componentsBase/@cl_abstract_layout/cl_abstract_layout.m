% Constructeur de cl_abstract_layout
%
% Syntax
%   instance = cl_abstract_layout(...)
% 
% Name-Value Pair Arguments
%    Position         : matrice de position en pixel
%    MaxLig           : nombre maximal de ligne
%    MaxCol           : nombre maximal de colonne
%    InsetX           : espacement en x (cf java)
%    InsetY           : espacement en y (cf java)
%
% Output Arguments 
%    instance : instance de cl_abstract_layout
%
% Remarks : 
%    Classe "abstraite" : a ne pas employer directement ! Cette classe 
%    implemente simplement la gestion d'un positionnement en lignes et 
%    colonnes.
% 
% Examples
%    al = cl_abstract_layout
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = cl_abstract_layout(varargin)

% --------------------------------------------
% Definition de la structure et initialisation
%
% surface (rectangle) sur laquelle s applique la gestion de ce layout
% nombre max de lignes du layout
% nombre max de colonnes du layout
% marge en X entre colonnes et entre bord de surface et colonnes
% marge en Y entre lignes et entre bord de surface et lignes
% epaisseur d une ligne (hauteur selon y)
% epaisseur d une colonne (largeur selon x)

instance.position   = [];
instance.maxLig     = 1;
instance.maxCol     = 1;
instance.insetX     = 0;
instance.insetY     = 0;
instance.epaisseurX = [];
instance.epaisseurY = [];

%% Cr�ation de l'objet

instance = class(instance, 'cl_abstract_layout');

%% Pr�-nitialisation des champs

if (length(varargin) == 1) && isempty(varargin{1})
    return
end
instance = set(instance, varargin {:});
