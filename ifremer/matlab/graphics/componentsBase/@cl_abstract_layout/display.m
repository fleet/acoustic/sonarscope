% Affichage des donnees d'une instance de cl_abstract_layout
%
% Syntax
%   display(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Examples
%   cl_abstract_layout
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
