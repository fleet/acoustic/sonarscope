% Lecture des attributs de l'instance
%
% Syntax
%   varargout = get(this, ...)
% 
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Name-only Arguments
%      Position : matrice de position en pixel
%      MaxLig   : nombre maximal de ligne
%      MaxCol   : nombre maximal de colonne
%      InsetX   : espacement en x (cf java)
%      InsetY   : espacement en y (cf java)
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    al = cl_abstract_layout ('Position', [1 2 20 30])
%    get(f, 'Position')
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'Position'
            varargout{end+1} = this.position; %#ok<AGROW>
        case 'MaxLig'
            varargout{end+1} = this.maxLig; %#ok<AGROW>
        case 'MaxCol'
            varargout{end+1} = this.maxCol; %#ok<AGROW>
        case 'InsetX'
            varargout{end+1} = this.insetX; %#ok<AGROW>
        case 'InsetY'
            varargout{end+1} = this.insetY; %#ok<AGROW>
        otherwise
            my_warndlg('cl_abstract_layou/get : Invalid property name', 0);
    end
end
