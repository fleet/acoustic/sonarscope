% Accesseur en lecture de la marge en x
%
% Syntax
%   insetX = get_inset_x(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Output Arguments
%   insetX : Nombre de pixels de la marge en x
%
% See also cl_abstract_layout Authors
% Authors : DCF
% VERSION  : $Id: get_inset_x.m,v 1.4 2003/04/07 12:22:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function insetX = get_inset_x(this)

% Maintenance Auto : try
    insetX = this.insetX;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_abstract_layout', 'get_inset_x', lasterr);
% Maintenance Auto : end
