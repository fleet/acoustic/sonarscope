% Accesseur en lecture de la marge en y
%
% Syntax
%   insetY = get_inset_y(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Output Arguments
%   insetY : Nombre de pixels de la marge en y
%
% See also cl_abstract_layout Authors
% Authors : DCF
% VERSION  : $Id: get_inset_y.m,v 1.4 2003/04/07 12:22:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function insetY = get_inset_y(this)

% Maintenance Auto : try
    insetY = this.insetY;
% Maintenance Auto : catch
% Maintenance Auto :     err ('cl_abstract_layout', 'get_inset_y', lasterr);
% Maintenance Auto : end
