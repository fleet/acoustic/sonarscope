% Accesseur en lecture du nombre maximal de colonnes
%
% Syntax
%   maxCol = get_max_col(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Output Arguments
%   maxCol : Nombre max de colonnes
%
% See also cl_abstract_layout Authors
% Authors : DCF
% VERSION  : $Id: get_max_col.m,v 1.4 2003/04/07 12:22:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function maxCol = get_max_col(this)

% Maintenance Auto : try
    maxCol = this.maxCol;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_abstract_layout', 'get_max_col', lasterr);
% Maintenance Auto : end
