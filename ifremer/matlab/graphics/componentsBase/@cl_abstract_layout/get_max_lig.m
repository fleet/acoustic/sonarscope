% Accesseur en lecture du nombre maximal de lignes
%
% Syntax
%   maxLig = get_max_lig(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Output Arguments
%   maxLig : Nombre maximul de lignes
%
% See also cl_abstract_layout Authors
% Authors : DCF
% VERSION  : $Id: get_max_lig.m,v 1.4 2003/04/07 12:22:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function maxLig = get_max_lig(this)

% Maintenance Auto : try
    maxLig = this.maxLig;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_abstract_layout', 'get_max_lig', lasterr);
% Maintenance Auto : end
