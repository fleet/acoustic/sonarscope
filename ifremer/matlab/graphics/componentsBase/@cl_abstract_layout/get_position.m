% Accesseur en lecture de la position
%
% Syntax
%   position = get_position(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Output Arguments
%   position : position
%
% See also cl_abstract_layout Authors
% Authors : DCF
% VERSION  : $Id: get_position.m,v 1.4 2003/04/07 12:22:49 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    21/03/2001 - DCF - Creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function position = get_position(this)

% Maintenance Auto : try
    position = this.position;
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_abstract_layout', 'get_position', lasterr);
% Maintenance Auto : end
