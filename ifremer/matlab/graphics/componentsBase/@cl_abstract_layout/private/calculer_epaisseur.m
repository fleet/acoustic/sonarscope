% Determine l epaisseur, en pixels, pour une ligne et une colonne
%
% Syntax
%   this = calculer_epaisseur(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Output Arguments
%   this = instance de cl_abstract_layout
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = calculer_epaisseur(this)

%% Vérification des conditions de travail

if ~isempty(this.maxLig) && ~isempty(this.maxCol) && ~isempty(this.position)

    %% Calcul de l'épaisseur selon x (largeur de colonne)

    deltax = this.position(3);
    this.epaisseurX = (deltax - (this.maxCol+1)*this.insetX) / this.maxCol;

    if this.epaisseurX <= 0
        this.epaisseurX = 1;
        my_warndlg('cl_abstract_layout/calculer_epaisseur : épaisseur nulle en x', 0, 'Tag', 'cl_abstract_layout/calculer_epaisseur');
    end

    %% Calcul de l'épaisseur selon y (hauteur de ligne)

    deltay = this.position(4);
    this.epaisseurY = (deltay - (this.maxLig+1)*this.insetY) / this.maxLig;

    if this.epaisseurY <= 0
        this.epaisseurY = 1;
    end
end
