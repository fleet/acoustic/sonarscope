% Transformation en caracteres d'une instance de cl_abstract_layout.
%
% Syntax
%   str = char_instance(this)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%   char(cl_abstract_layout)
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

% ----------
% Traitement

str{end+1} = sprintf('position   <-> %s', num2strCode(this.position));
str{end+1} = sprintf('maxLig     <-> %d', this.maxLig);
str{end+1} = sprintf('maxCol     <-> %d', this.maxCol);
str{end+1} = sprintf('insetX     <-> %d', this.insetX);
str{end+1} = sprintf('insetY     <-> %d', this.insetY);
str{end+1} = sprintf('epaisseurX <-> %f', this.epaisseurX);
str{end+1} = sprintf('epaisseurY <-> %f', this.epaisseurY);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
