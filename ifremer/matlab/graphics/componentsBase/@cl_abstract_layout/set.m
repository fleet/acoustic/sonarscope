% Modification des attributs de l'instance
%
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%   this : instance de cl_abstract_layout
%
% Name-Value Pair Arguments
%   Position         : matrice de position en pixel
%   MaxLig           : nombre maximal de ligne
%   MaxCol           : nombre maximal de colonne
%   InsetX           : espacement en x (cf java)
%   InsetY           : espacement en y (cf java)
%
% Output Arguments
%   this : instance de cl_abstract_layout initialisee
%
% Examples
%    al = cl_abstract_layout; set(al)
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% ---------------------
% Lecture des arguments

[varargin, position] = getPropertyValue(varargin, 'Position', []);
[varargin, maxLig] = getPropertyValue(varargin, 'MaxLig', []);
[varargin, maxCol] = getPropertyValue(varargin, 'MaxCol', []);
[varargin, insetX] = getPropertyValue(varargin, 'InsetX', []);
[varargin, insetY] = getPropertyValue(varargin, 'InsetY', []);

% ----------------------------
% Initialisation des attributs

if ~isempty(position)
    this = set_position(this, position);
end
if ~isempty(maxLig)
    this = set_max_lig(this, maxLig);
end
if ~isempty(maxCol)
    this = set_max_col(this, maxCol);
end
if ~isempty(insetX)
    this = set_inset_x(this, insetX);
end
if ~isempty(insetY)
    this = set_inset_y(this, insetY);
end

% --------------------------
% Test analyse des arguments

if ~isempty(varargin)
    my_warndlg('clc_abstract_layout/set : Too many arguments', 0);
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
