% Accesseur en ecriture la marge selon l axe x
%
% Syntax
%   this = set_inset_x(this, insetX)
%
% Input Arguments
%   this   : instance de cl_abstract_layout
%   insetX : insetX
%
% Output Arguments
%   this : instance de cl_abstract_layout initialisee
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_inset_x(this, insetX)

this.insetX = insetX;
this = calculer_epaisseur(this);
