% Accesseur en ecriture la marge selon l axe y
%
% Syntax
%   this = set_inset_y(this, insetY)
%
% Input Arguments
%   this   : instance de cl_abstract_layout
%   insetY : insetY
%
% Output Arguments
%   this : instance de cl_abstract_layout initialisee
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_inset_y(this, insetY)

this.insetY = insetY;
this = calculer_epaisseur(this);
