% Accesseur en ecriture du nombre max de colonnes
%
% Syntax
%   this = set_max_col(this, maxCol)
%
% Input Arguments
%   this   : instance de cl_abstract_layout
%   maxCol : max de lignes
%
% Output Arguments
%   this : instance de cl_abstract_layout initialisee
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_max_col(this, maxCol)

this.maxCol = maxCol;
this = calculer_epaisseur(this);
