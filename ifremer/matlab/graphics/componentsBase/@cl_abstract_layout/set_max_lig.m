% Accesseur en ecriture du nombre max de lignes
%
% Syntax
%   this = set_max_lig(this, maxLig)
%
% Input Arguments
%   this   : instance de cl_abstract_layout
%   maxLig : max de lignes
%
% Output Arguments
%   this : instance de cl_abstract_layout initialisee
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_max_lig(this, maxLig)

this.maxLig = maxLig;
this = calculer_epaisseur(this);
