% Accesseur en ecriture de la position du composant
%
% Syntax
%   this = set_position(this, position)
%
% Input Arguments
%   this     : instance de cl_abstract_layout
%   position : position
%
% Output Arguments
%   this : instance de cl_abstract_layout initialisee
%
% See also cl_abstract_layout Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_position(this, position)

this.position = position;
this = calculer_epaisseur(this);
