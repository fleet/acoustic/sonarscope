% Construction d'une callback
%
% Syntax
%   cb = built_cb(this, msgCb)
%
% Input Arguments
%   this  : instance de cl_component
%   msgCb : message associe a la call back
%
% Output Arguments
%   cb : chaine de caracteres permettant d initialiser la call back
%
% See ALSO : cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function cb = built_cb(this, msgCb, varargin)

[varargin, Interruptible] = getPropertyValue(varargin, 'Interruptible', 'off'); %#ok<ASGLU>

if ~iscell(msgCb)
    msgCb = {msgCb};
end

cb = {};

% ------------------------------------------------------------------------
% Ne peut construire une callback que si le nom de la classe utilisatrice
% ainsi que le nom de son manager sont connus

% Ancien code
%{
    N = length(cb);
    nbMes = length(msgCb);
    
    k = N + 5 + nbMes * 4;
    cb{k+3} = '); '');';
    
    cb{N+1} = 'eval(''';
    cb{N+2} = this.componentUserCb;
    cb{N+3} = '(';
    cb{N+4} = this.componentUserName;
    cb{N+5} = '([])';
    for i=1:nbMes
        k = N + 5 + (i-1) * 4;
        cb{k+1} = ', ';
        cb{k+2} = '''''';
        cb{k+3} = msgCb{i};
        cb{k+4} = '''''';
    end
    k = N + 5 + nbMes * 4;
    
    switch Interruptible
        case 'off'
            cb{k+1} = ', ''''Interruptible'''', ''''off''''';
            cb{k+2} = ', ''''BusyAction'''', ''''cancel''''';
        otherwise
            cb{k+1} = ', ''''Interruptible'''', ''''on''''';
            cb{k+2} = ', ''''BusyAction'''', ''''cancel''''';
    end
    
    %     mais sans r�sultat, tous les mouvements de la souris sont empil�s dans la pile d'appel de la
    %     callback. La seule diff�rence, c'est qu'il n'affiche pas les plots, donc c'est encore plus troublant.
    %     Comment g�rer cela ?
    cb = [cb{:}];
    cb
%}




% Test nouvelle formule
nbMes = length(msgCb);

cb{1} = [this.componentUserName '.' this.componentUserCb '(']; % TODOcallback
for i=1:nbMes
    cb{end+1} = ['''' msgCb{i} '''']; %#ok<AGROW>
    cb{end+1} = ', '; %#ok<AGROW>
end

%{
    switch Interruptible
        case 'off'
            cb{end+1} = ['''''Interruptible''' ''',''' '''off''' ''',']; % ??????????????????????
            cb{end+1} = ['''''BusyAction''' ''',''' '''cancel''' ''',']; % ??????????????????????
        otherwise
            cb{end+1} = 'Interruptible'', ''on'', '; % ??????????????????????
            cb{end+1} = 'BusyAction'', ''cancel'', '; % ??????????????????????
    end
%}

cb = [cb{:}];
cb = [cb(1:end-2) ');'];
% fprintf('%s\n', cb);
