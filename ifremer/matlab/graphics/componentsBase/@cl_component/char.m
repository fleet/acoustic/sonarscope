% Transformation en chaine de caracteres d'un tableau d'instances
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance ou tableau d'instances de cl_component
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%   char(cl_component)
%
% See also cl_component Authors
% Authors : DCF
% VERSION  : $Id: char.m,v 1.4 2003/04/07 13:49:47 augustin Exp $
% ----------------------------------------------------------------------------

function str = char(this)

str = [];

[m, n] = size(this);
if (m*n) > 1
    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok
        end
    end
    str = cell2str(str);

else
    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
