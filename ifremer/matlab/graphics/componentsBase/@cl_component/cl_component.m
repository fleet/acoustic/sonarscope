% Constructeur de cl_component
%
% Syntax
%   this = cl_component(...)
% 
% Name-Value Pair Arguments
%    componentName     : nom de la propriete
%    componentVisible  : visibilite du composant
%    componentEnable   : accessibilite du composant
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                          de la classe utilisatrice du composant
%    componentInsetX   : marge en X du composant (en pixels)
%    componentInsetY   : marge en Y du composant (en pixels)
%
% Output Arguments 
%   this : instance de cl_component
%
% Remarks : Doit etre vue comme une classe abstraite dont seules les heritieres
%           sont instanciables
%
% Examples
%    c = cl_component
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = cl_component(varargin)

%% Définition de la structure et initialisation
%
% componentName        : nom du composant
% componentVisible     : visibilite du composant
% componentEnable      : accessibilite du composant
% componentUserName    : nom de la classe utilisatrice du composant
% componentUserCb      : nom de la methode de gestion des Cb de la classe
%                        utilisatrice du composant
% componentAnchor      : ancre du composant (handle, si representation
%                        graphique uniquement)
% componentFlagEdit    : flag indiquant la representation graphique
% componentInsetX      : marge en X du composant
% componentInsetY      : marge en Y du composant

this.componentName     = [];
this.componentVisible  = 'on';
this.componentEnable   = 'on';
this.componentUserName = [];
this.componentUserCb   = 'gerer_callback';
this.componentAnchor   = [];
this.componentFlagEdit = 0;
this.componentInsetX   = 5;
this.componentInsetY   = 5;
this.componentParent   = [];

%% Création de l'objet

this = class(this, 'cl_component');

%% Préinitialisation des champs

if (length(varargin) == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
