% Creation d'un composant � partir de ses proprietes (ihm)
% 
% a = create_component(cl_component,ihm);
%
% Input Arguments
%    ihm : tableau de cellules comprenant le nom des attributs du
%    composant et les valeurs correspondantes
%
% Output Arguments
%    a : instance du composant initialise
%
% EXEMPLE:
%
% Authors : LUR
% -------------------------------------------------------------------------

function retour = create_component(~, ihm)

%% Initialisation des variables

lstProperties = [];
str = [];
names  = fieldnames(ihm{1,1});
values = struct2cell(ihm{1,1});

%% R�cup�ration des propri�t�s du composant

for k=1:length(names)
    if strcmpi(names{k}, 'Style')
        cpn_name=values{k};
    elseif strcmpi(names{k},'Parent')
        lstProperties{end+1} = 'componentParent';  %#ok<AGROW>
        lstProperties{end+1} = values{k};  %#ok<AGROW>
    elseif strcmp(names{k},'Name')
        lstProperties{end+1} = 'componentName';  %#ok<AGROW>
        lstProperties{end+1} = values{k};  %#ok<AGROW>
    elseif strcmpi(names{k},'UserName')
        lstProperties{end+1} = 'componentUserName';  %#ok<AGROW>
        lstProperties{end+1} = values{k};  %#ok<AGROW>
    elseif strcmpi(names{k},'UserCb')
        lstProperties{end+1} = 'componentUserCb';  %#ok<AGROW>
        lstProperties{end+1} = values{k};  %#ok<AGROW>
    else
        lstProperties{end+1} = names{k};  %#ok<AGROW>
        if ~iscell(values{k})
            lstProperties{end+1} = values{k};  %#ok<AGROW>
        else
            tab = '{''';
            for j=1:(length(values{k})-1)
                % Cast de la valeur si on l'a saisi sous forme num�rique.
                if ~ischar(values{k}{j})
                    values{k}{j} = num2str(values{k}{j});
                end
                tab=strcat(tab,values{k}{j},''',''');
            end
            % Cast de la valeur si on l'a saisi sous forme num�rique.
            if ~ischar(values{k}{j+1})
                values{k}{j+1} = num2str(values{k}{j+1});
            end
            lstProperties{end+1} = strcat(tab,values{k}{j+1},'''}'); %#ok<AGROW>
        end
    end
end

%% Concat�nation et �valuation de la chaine de caract�res

str{end+1} = 'temp=';
str{end+1} = cpn_name;
str{end+1} = '(';
str{end+1} = '''';
str{end+1} = lstProperties{1};
str{end+1} = '''';

for k=2:length(lstProperties)
    str{end+1} = ', ';  %#ok<AGROW>

    if ischar(lstProperties{k})
        if isempty(strfind(lstProperties{k},'{'))
            str{end+1} = '''';  %#ok<AGROW>
            str{end+1} = lstProperties{k};  %#ok<AGROW>
            str{end+1} = '''';  %#ok<AGROW>
        else
            str{end+1} = '';  %#ok<AGROW>
            str{end+1} = lstProperties{k};  %#ok<AGROW>
            str{end+1} = '';  %#ok<AGROW>
        end
    else
        str{end+1} = '';  %#ok<AGROW>
        str{end+1} = num2str(lstProperties{k});  %#ok<AGROW>
        str{end+1} = '';  %#ok<AGROW>
    end
end

str{end+1} = ');';
str = strcat(str{:});
eval(str);

%% Cr�ation du composant et retour de l'instance

retour = editobj(temp);
