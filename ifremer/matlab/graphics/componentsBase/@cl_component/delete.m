% Destruction du composant
%
% Syntax
%   this = delete(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Remarks : Cette methode doit etre surchargee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = delete(this)

this.componentAnchor   = [];
this.componentFlagEdit = 0;
