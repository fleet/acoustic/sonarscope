% Representation de l'instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de cl_component
%
% OUPUT PARAMETERS :
%   this : instance de cl_component
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this, varargin)

[varargin, handleAnchor] = getPropertyValue(varargin, 'handleAnchor', []); %#ok<ASGLU>

if ishandle(handleAnchor)
    handle = handleAnchor;
else
    if isempty(this.componentName)
        handle = [];
    else
        handle = findobj(gcf, 'Tag', this.componentName); % TODO : Ca prend beaucoup de temps
        % S'inspirer de ce qui est fait dans cl_panel/private/create_uicontrol
    end
end

if ishandle(handle)
    this.componentAnchor   = handle;
    this.componentFlagEdit = 1;
else
    this.componentAnchor   = [];
    this.componentFlagEdit = 0;
end
