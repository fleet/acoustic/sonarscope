% Retourne le handle de la figure courante du composant
%
% Syntax
%   handle = gcf(this, handle)
% 
% Input Arguments
%   this : instance de cl_component
%   handle   : handle d'un element graphique de la figure recherchee
%
% Output Arguments
%   handle de la figure courante du composant
% 
% See also cl_component Authors
% Authors : DCF
% VERSION  : $Id: gcf.m,v 1.3 2003/03/21 14:37:53 augustin Exp $
% ----------------------------------------------------------------------------

function handle = gcf(this, handle) %#ok

% ------------------
% Test des arguments

if ishandle(handle)

    % --------------------------------
    % Boucle de recherche de la figure

    while (isAGraphicElement(handle, 'figure') == 0) && (handle ~= 0)
        handle = get(handle, 'Parent');
    end

    % ---------------------------------------
    % Traitement d une recherche infructueuse

    if handle == 0
        handle = [];
    end

else
    handle = [];
    my_warndlg('cl_component/gcf : Invalid handle');
end
