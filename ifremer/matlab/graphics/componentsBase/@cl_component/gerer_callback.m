% Methode de gestion des callback du composant
%
% Syntax
%   this = gerer_callback (this, cbName, ...)
%
% Input Arguments
%   this     : instance de cl_component
%   cbName   : nom de la call back
%
% Name-Value Pair Arguments
%   xxx : parametres optionnels de la callback
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Remarks :Cette m�thode doit etre surchargee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)
