% Lecture des attributs de l'instance
%
% Syntax
%   varargout = get(this, ...)
% 
% Input Arguments
%   this : instance de cl_component
%
% Name-Value Pair Arguments 
%   componentName     : nom de la propriete
%   componentVisible  : visibilite du composant
%   componentEnable   : accessibilite du composant
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentInsetX   : marge en X du composant (en pixels)
%   componentInsetY   : marge en Y du composant (en pixels)
%   componentAnchor   : ancre du composant (handle)
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    c = cl_component('visible', 'on');
%    get(c, 'visible')
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'componentVisible'
            varargout{end+1} = this.componentVisible; %#ok
        case 'componentEnable'
            varargout{end+1} = this.componentEnable; %#ok
        case 'componentName'
            varargout{end+1} = this.componentName; %#ok
        case 'componentUserName'
            varargout{end+1} = this.componentUserName; %#ok
        case 'componentUserCb'
            varargout{end+1} = this.componentUserCb; %#ok
        case 'componentInsetX'
            varargout{end+1} = this.componentInsetX; %#ok
        case 'componentInsetY'
            varargout{end+1} = this.componentInsetY; %#ok
        case 'componentAnchor'
            varargout{end+1} = this.componentAnchor; %#ok
        case 'componentParent'
            varargout{end+1} = this.componentParent; %#ok
            
        otherwise
            my_warndlg(['cl_component/get : ' varargin{i} ' Invalid property name'], 1);
            varargout{end+1} = []; %#ok
    end
end
