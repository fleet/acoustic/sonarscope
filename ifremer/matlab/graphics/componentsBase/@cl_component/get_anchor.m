% Retourne l'ancre du composant (handle)
%
% Syntax
%   anchor = get_anchor(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   anchor   : ancre du composant (handle)
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function anchor = get_anchor(this)

anchor = this.componentAnchor;
