% Retourne l'accessibilite du composant
%
% Syntax
%   enable = get_enable(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   enable : accessibilite du composant
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function enable = get_enable(this)

enable = this.componentEnable;
