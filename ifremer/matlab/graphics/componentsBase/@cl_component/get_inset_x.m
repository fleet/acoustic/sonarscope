% Retourne l'ancre du composant (handle)
%
% Syntax
%   insetX = get_inset_x(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   insetX   : marge en x appliquee pour le composant
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function insetX = get_inset_x(this)

insetX = this.componentInsetX;
