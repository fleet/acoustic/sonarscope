% Retourne l'ancre du composant (handle)
%
% Syntax
%   insetY = get_inset_y(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   insetY   : marge en y appliquee pour le composant
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function insetY = get_inset_y(this)

insetY = this.componentInsetY;
