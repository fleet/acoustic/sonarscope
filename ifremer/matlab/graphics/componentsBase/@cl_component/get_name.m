% Retourne le nom du composant
%
% Syntax
%   name = get_name(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   name     : nom du composant
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function name = get_name(this)
name = this.componentName;
