% Retourne le nom du gestionnaire de callback de la classe utilisatrice
%
% Syntax
%   userCb = get_user_cb(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   userCb   : nom du gestionnaire de cb de la classe utilisatrice de ce composant
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function userCb = get_user_cb(this)

userCb = this.componentUserCb;
