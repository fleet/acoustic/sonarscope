% Retourne le nom de la classe utilisant le composant
%
% Syntax
%   userName = get_user_name(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   userName : nom de la classe utilisant le composant
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function userName = get_user_name(this)

userName = this.componentUserName;
