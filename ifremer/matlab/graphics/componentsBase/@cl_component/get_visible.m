% Retourne la visibilite du composant
%
% Syntax
%   visible = get_visible(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   visible  : visibilite du composant
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function visible = get_visible(this)

visible = this.componentVisible;
