% Indique si le composant a une representation graphique
%
% Syntax
%   flag = is_edit(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   flag : booleen 0 : non affiche, 1 : affiche
%
% Remarks :
%   doit etre surchargee
%
% See also cl_component Authors
% Authors : DCF
% -------------------------------------------------------------------------

function flag = is_edit(this)

%% Retourne le flag de representation graphique

flag = this.componentFlagEdit;
