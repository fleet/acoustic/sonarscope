% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

%% Traitement

%str{end+1} = sprintf('gcf               <-- %d', this.gcf);
str{end+1} = sprintf('componentName     <-> %s', mat2str(this.componentName));
str{end+1} = sprintf('componentVisible  <-> %s', mat2str(this.componentVisible));
str{end+1} = sprintf('componentEnable   <-> %s', mat2str(this.componentEnable));
str{end+1} = sprintf('componentUserName <-> %s', mat2str(this.componentUserName));
str{end+1} = sprintf('componentUserCb   <-> %s', mat2str(this.componentUserCb));
% str{end+1} = sprintf('componentAnchor   <-- %s', mat2str(this.componentAnchor));
str{end+1} = sprintf('componentFlagEdit <-> %d', this.componentFlagEdit);
str{end+1} = sprintf('componentInsetX   <-> %d', this.componentInsetX);
str{end+1} = sprintf('componentInsetY   <-> %d', this.componentInsetY);
str{end+1} = sprintf('componentParent   <-> %s', num2str(this.componentParent));

%% Concatenation en une chaine

str = cell2str(str);
