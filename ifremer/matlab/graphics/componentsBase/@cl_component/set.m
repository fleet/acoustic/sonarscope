% Modification des attributs de l'instance
%
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%   this : instance de cl_component
%
% Name-Value Pair Arguments
%    componentName       : nom de la propriete
%    componentVisible    : visibilite du composant
%    componentEnable     : accessibilite du composant
%    componentUserName   : nom de la classe utilisatrice du composant
%    componentUserCb     : nom de la methode de gestion des call back
%                          de la classe utilisatrice du composant
%    componentInsetX     : marge en X du composant (en pixels)
%    componentInsetY     : marge en Y du composant (en pixels)
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% Examples
%    c = cl_component;
%    set(c, 'componentVisible', 'on', ...
%            'componentEnable', 'on',  ...
%            'componentName', 'John Smith')
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

if isempty(varargin)
    return
end

%% Lecture des arguments

[varargin, name]     = getPropertyValue(varargin, 'componentName', []);
[varargin, userName] = getPropertyValue(varargin, 'componentUserName', []);
[varargin, userCb]   = getPropertyValue(varargin, 'componentUserCb', []);
[varargin, visible]  = getPropertyValue(varargin, 'componentVisible', []);
[varargin, enable]   = getPropertyValue(varargin, 'componentEnable', []);
[varargin, insetX]   = getPropertyValue(varargin, 'componentInsetX', []);
[varargin, insetY]   = getPropertyValue(varargin, 'componentInsetY', []);
[varargin, parent]   = getPropertyValue(varargin, 'componentParent', []); %#ok<ASGLU>

%% Initialisation des attributs

if ~isempty(name)
    this = set_name(this, name);
end
if ~isempty(visible)
    this = set_visible(this, visible);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end
if ~isempty(userName)
    this = set_user_name(this, userName);
end
if ~isempty(userCb)
    this = set_user_cb(this, userCb);
end
if ~isempty(insetX)
    this = set_inset_x(this, insetX);
end
if ~isempty(insetY)
    this = set_inset_y(this, insetY);
end
if ~isempty(parent)
    this = set_parent(this, parent);
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
