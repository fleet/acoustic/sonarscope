% Initialisation de l'accessibilite du composant
%
% Syntax
%   this = set_enable(this, enable)
%
% Input Arguments
%   this   : instance de cl_component
%   enable : accessibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.4 2003/04/07 13:49:47 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    this.componentEnable = enable;
else
    my_warndlg('cl_component/set_enable : Invalid value', 1);
end
