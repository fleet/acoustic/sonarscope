% Initialisation du nom du composant
%
% Syntax
%   this = set_inset_x(this, insetX)
%
% Input Arguments
%   this   : instance de cl_component
%   insetX : marge en x appliquee au composant
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_inset_x(this, insetX)
this.componentInsetX = insetX;
