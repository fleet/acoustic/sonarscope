% Initialisation du nom du composant
%
% Syntax
%   instance = set_inset_y(this, insetY)
%
% Input Arguments
%   this   : instance de cl_component
%   insetY : marge en y appliquee au composant
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_inset_y(this, insetY)
this.componentInsetY = insetY;
