% Initialisation du nom du composant
%
% Syntax
%   this = set_name(this, name)
%
% Input Arguments
%   this : instance de cl_component
%   name : nom du composant
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_name(this, name)
this.componentName = name;
