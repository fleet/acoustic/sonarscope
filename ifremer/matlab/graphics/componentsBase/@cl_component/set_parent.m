% Initialisation du nom du composant
%
% Syntax
%   this = set_inset_x(this, insetX)
%
% Input Arguments
%   this   : instance de cl_component
%   insetX : marge en x appliquee au composant
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% VERSION  : $Id: set_inset_x.m,v 1.4 2003/04/07 13:49:47 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_parent(this, parent)

% -------------------------------------------
% Test validite des arguments, initialisation

if ~isempty(parent)
  
    this.componentParent = parent;
   
else
    my_warndlg('cl_component/set_parent : Invalid parent', 1);
end
