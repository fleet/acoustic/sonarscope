% Initialisation du nom du gestionnaire de call back de la classe utilisatrice
%
% Syntax
%   this = set_user_cb(this, userCb)
%
% Input Arguments
%   this   : instance de cl_component
%   userCb : nom du gestionnaire de cb de la class utilisatrice
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_user_cb(this, userCb)
this.componentUserCb = userCb;
