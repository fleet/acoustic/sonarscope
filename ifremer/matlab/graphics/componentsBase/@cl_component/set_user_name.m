% Initialisation du nom de la classe utilisatrice du composant
%
% Syntax
%   this = set_user_name(this, userName)
%
% Input Arguments
%   this     : instance de cl_component
%   userName : nom de la classe utilisatrice du composant
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_user_name(this, userName)
this.componentUserName = userName;
