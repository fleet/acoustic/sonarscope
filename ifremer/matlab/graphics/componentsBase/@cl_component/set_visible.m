% Initialisation de la visibilite du composant
%
% Syntax
%   this = set_visible(this, visible)
%
% Input Arguments
%   this    : instance de cl_component
%   visible : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de cl_component initialisee
%
% See also cl_component Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_visible(this, visible)
this.componentVisible = visible;
