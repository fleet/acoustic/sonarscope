% Mise a jour du composant
%
% Syntax
%   this = update(this)
%
% Input Arguments
%   this : instance de cl_component
%
% Remarks :
%   Doit etre surchargee
%
% See also cl_component Authors
% Authors : DCF
% VERSION  : $Id: update.m,v 1.4 2003/04/07 13:49:47 augustin Exp $
% ----------------------------------------------------------------------------

function this = update(this)

% Il y a duperie car rien n'est fait ici
