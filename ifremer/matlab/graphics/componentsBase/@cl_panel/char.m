% Transformation en chaine de caracteres d'un objet de classe cl_panel
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance de cl_panel
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%    char(cl_panel)
%
% See also cl_panel Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char(this)

% TODO : reprogrammer par boucle
if isempty(this)
    str = 'Empty';
    return
end

%% Intialisation des locales

str = [];

%% Traitement tableau ou instance seule

[m, n] = size(this);

if (m*n) > 1
    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1;
        end
    end
    str = cell2str(str);
    
else
    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
