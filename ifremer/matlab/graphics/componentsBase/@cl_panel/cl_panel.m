% Constructeur de cl_panel
%
% Syntax
%   this = cl_panel(...)
% 
% Name-Value Pair Arguments
%   HandlePanel      : handle du panel
%   ListeUicontrols  : liste des uicontrols
%   ListeAxes        : liste des axes
%   NomPanel         : nom du panel
%   PositionPanel    : position du panel

function this = cl_panel(varargin)

% --------------------------------------------------------------------------
% Definition de la structure et initialisation
%
% alias             : nom d alias de la frame
% nomFrame          : nom de la frame, tag de la frame matlab
% handleFrame       : handle matlab de la frame
% positionFrame     : position de la frame
% tabUicontrols     : tableau des uicontrols "contenus" par la frame
% ligUicontrols     : liste des positions en ligne des uicontrols contenus
%                     par la frame
% colUicontrols     : liste des positions en colonne des uicontrols
%                     contenus par la frame
% visibleUicontrols : tableau des flags de visibilite des uicontrols
%                     contenus par la frame
% enableuicontrols  : tableau des flags d accessibilite des uicontrols
%                     contenus par la frame
% tabAxes           : tableau des axes contenus par la frame
% ligAxes           : liste des positions en ligne des axes contenus
%                     par la frame
% colAxes           : liste des positions en colonne des axes contenus
%                     par la frame
% visibleAxes       : tableau des flags de visibilite des axes contenus
%                     par la frame
% tabClFrames       : tableau des instances de cl_frame contenues
%                     par la frame
% visibleClFrames   : tableau des flags de visibilite des instances de
%                     cl_frame contenues par la frame
% enableClFrames    : tableau des flags d accessibilite des instances de
%                     cl_frame contenues par la frame
% visible           : visibilite courante de l instance
% enable            : accessibilite courante de l instance
% verification      : verification des proprietes (0 : non; 1 : oui)
% inset             : marge entre composants contenus (entre lig et col)


 this.nomPanel          = [];
 this.handlePanel       = [];
 this.positionPanel     = [];
 this.visible           = [];
 this.tabUicontrols     = [];
 this.tabUipanels       = [];
 this.tabUiComposants   = [];
 this.visibleUicontrols = [];
 this.enableUicontrols  = [];
 this.visibleUipanels   = [];
 this.enable            = [];
 this.parent            = {};
 this.ligUicontrols     = [];
 this.colUicontrols     = [];
 this.ligUipanels       = [];
 this.colUipanels       = [];
 this.tabAxes           = [];
 this.visibleAxes       = [];
 this.tabClPanel        = [];
 this.visibleClPanel    = [];
 this.verification      = 0;
 
 this.nbCol             = 0; %on fixe arbitrairement le nombre de colonnes
 this.nbRows             = 0; %on fixe arbitrairement le nombre de lignes
 
 %this.cpn               = [];
 
% this.alias             = [];
% this.ligAxes           = [];
% this.colAxes           = [];
% this.enableClPanel    = [];
% this.enable            = [];
%this.inset             = 5;

% ------------
% Super classe

if (length(varargin) == 1) && isempty(varargin{1})
    layout = cl_abstract_layout([]);
else
    layout = cl_abstract_layout('InsetX',0,'InsetY',0);
end

% -------------------
% Creation de l'objet

this = class(this, 'cl_panel', layout);

% ----------------------------
% Pre-nitialisation des champs

if (length(varargin) == 1) && isempty(varargin{1})
    return
end

% ---------------------------------------------------------------------
% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
