% Creation d'un composant � partir de ses proprietes (ihm)
%
% a = create_component(cl_component,ihm);
%
% Input Arguments
%    ihm : tableau de cellules comprenant le nom des attributs du
%    composant et les valeurs correspondantes
%
% Output Arguments
%    a : instance du composant initialise
%
% EXEMPLE:
%
% Authors : LUR
% -------------------------------------------------------------------------

function this = create_component2(this, tabUiCpn)

% D�clarations n�cessaires � la compilation pour d�ploiement.
%#function clc_entete
%#function clc_label_value_pnl
%#function clc_edit_texte_pnl
%#function clc_edit_value_pnl
%#function clc_browser
%#function clc_onglet
%#function clc_list_value_pnl

sauv_instance  = this;
this           = [];
this.MainPanel = sauv_instance;
GCF            = gcf;

for t=1:length(tabUiCpn)
    
    %% Initialisation des variables
    
    lstProperties = [];
    lig = [];
    col = [];
    cpn_name = [];
    ihm = tabUiCpn{1,t};
    names  = fieldnames(ihm);
    values = struct2cell(ihm);
    TagParent = ancestor(GCF, 'figure', 'toplevel');
    
    %% R�cuperation des propri�t�s du composant

    for k=1:length(names)
        if strcmpi(names{k}, 'Style')
            cpn_name = values{k};
        elseif strcmpi(names{k}, 'Parent')
            TagParent = values{k};
        elseif strcmp(names{k}, 'Name')
            lstProperties{end+1} = 'componentName'; %#ok
            lstProperties{end+1} = values{k}; %#ok
            nameCpn = values{k};
        elseif strcmpi(names{k}, 'UserName')
            lstProperties{end+1} = 'componentUserName'; %#ok
            lstProperties{end+1} = values{k}; %#ok
        elseif strcmpi(names{k}, 'UserCb')
            lstProperties{end+1} = 'componentUserCb'; %#ok
            lstProperties{end+1} = values{k}; %#ok
        elseif strcmpi(names{k}, 'lig')
            lig = values{k};
        elseif strcmpi(names{k}, 'col')
            col = values{k};
        elseif strcmpi(names{k}, 'BorderColor')
            lstProperties{end+1}= names{k}; %#ok<AGROW>
            lstProperties{end+1} = values{k}; %#ok<AGROW>
        else
            lstProperties{end+1} = names{k}; %#ok
            if ~iscell(values{k})
                lstProperties{end+1} = values{k}; %#ok
            else
                tab = '{''';
                for j=1:(length(values{k})-1)
                    tab = strcat(tab,values{k}{j},''',''');
                end
                lstProperties{end+1} = strcat(tab,values{k}{j+1},'''}'); %#ok
            end
        end
    end

    %% Cr�ation du panel du composant
    
    if strcmp(cpn_name, 'clc_entete') %a completer si utilisation d'anciens composants
        oldCpn = 1;
    else
        oldCpn = 0;
        lstProperties{end+1} = 'componentParent'; %#ok
        lstProperties{end+1} = ['Tag' nameCpn '_panel']; %#ok
    end
    
   if ~isfield(this,'sspanels')
        this.sspanels = [];
   end
    
    sspanels = create_panel_component(this, lig, col, TagParent, nameCpn, oldCpn);
    this.sspanels.(nameCpn) = sspanels;

    %% Concat�nation et evaluation de la chaine de caract�res

    str = [];
    str{end+1} = cpn_name; %#ok
    
    str{end+1} = '('; %#ok
    str{end+1} = ''''; %#ok
    str{end+1} = lstProperties{1}; %#ok
    str{end+1} = ''''; %#ok

    for k=2:length(lstProperties)
        str{end+1} = ', '; %#ok
        L = lstProperties{k};
        if ischar(L)
            if ~contains(L,'{')
                str{end+1} = ''''; %#ok
                str{end+1} = L; %#ok
                str{end+1} = ''''; %#ok
            else
                str{end+1} = ''; %#ok
                str{end+1} = L; %#ok
                str{end+1} = ''; %#ok
            end
        else
            str{end+1} = ''; %#ok
            if numel(L) ~= 1
                str{end+1} = mat2str(L); %#ok
            else
                str{end+1} = num2str(L); %#ok
            end
            str{end+1} = ''; %#ok
        end

    end

    str{end+1} = ');'; %#ok
    str = strcat(str{:});
    try
         temp = eval(str);
    catch %#ok<CTCH>
%         my_warndlg(['Message pour JMA : verifier la syntaxe des attributs de votre composant: ' nameCpn], 1);
        my_warndlg('There is an error here. The access to Java from SonarScope failed. A user who got this error just reinstalled SonarScope and it work', 1);
        disp(['Chaine traitee:' str]);
       return
    end
    
    %% Cr�ation du composant et retour de l'instance

    if ~isfield(this, 'cpn')
        this.cpn = [];
    end
    
    c = editobj(temp);
    this.cpn.(nameCpn) = c;
end
