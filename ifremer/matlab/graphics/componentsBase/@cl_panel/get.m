% Lecture des attributs de l'instance
%
% Syntax
%   varargout = get(this, ...)
% 
% Input Arguments
%   this : instance de cl_panel
%
% Name-only Arguments
%   Alias           : nom d'alias du panel
%   NomPanel        : nom de tag du panel matlab
%   PositionPanel   : position du panel
%   HandlePanel    : handle du panel
%   Visible         : visibilite du panel
%   Verification    : flag de verification des noms de proprietes des 
%                     instances matlab supportees par le panel
%   MaxLig          : nombre maximal de lignes
%   MaxCol          : nombre maximal de colonnes
%   InsetX          : marge selon l axe x
%   InsetY          : marge selon l axe y
%   Enable          : accessibilite du panel
%   ListeUicontrols : liste des instances des uicontrol

%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%   f = cl_panel('NomPanel', 'nomTestPanel');
%   get(f, 'NomPanel')
%
% See also cl_panel Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case{'Alias','Nom'}
            varargout{end+1} = this.alias; %#ok<AGROW>
        case 'NomPanel'
            varargout{end+1} = this.nomPanel; %#ok<AGROW>
        case 'PositionPanel'
            varargout{end+1} = this.positionPanel; %#ok<AGROW>
        case 'HandlePanel'
            varargout{end+1} = this.handlePanel; %#ok<AGROW>
        case 'Visible'
            varargout{end+1} = this.visible; %#ok<AGROW>
        case 'Verification'
            varargout{end+1} = this.verification; %#ok<AGROW>
        case 'MaxLig'
            varargout{end+1} = get_max_lig(this.cl_abstract_layout); %#ok<AGROW>
        case 'MaxCol'
            varargout{end+1} = get_max_col(this.cl_abstract_layout); %#ok<AGROW>
        case 'InsetX'
            varargout{end+1} = get_inset_x(this.cl_abstract_layout); %#ok<AGROW>
        case 'InsetY'
            varargout{end+1} = get_inset_y(this.cl_abstract_layout); %#ok<AGROW>
        case 'Enable'
            varargout{end+1} = this.enable; %#ok<AGROW>
        case 'ListeUiComposants'
            varargout{end+1} = this.tabUiComposants; %#ok<AGROW>
        case 'ListeUicontrols'
            varargout{end+1} = this.tabUicontrols; %#ok<AGROW>
        case 'ListeUipanels'
            varargout{end+1} = this.tabUipanels; %#ok<AGROW>
        case 'ListeAxes'
            varargout{end+1} = this.tabAxes; %#ok<AGROW>
        case 'ListeClPanel'
            varargout{end+1} = this.tabPanel; %#ok<AGROW>
        case 'Children'
            varargout{end+1} = this.children; %#ok<AGROW>
        case 'Parent'
            varargout{end+1} = this.parent; %#ok<AGROW>
        otherwise
            my_warndlg('cl_panel/get : too many arguments', 0);
    end
end
