% Deplacement d'une instance sur une nouvelle position
%
% Syntax
%   move(this, handle, lig, col)
%
% Input Arguments
%   this   : instance de cl_panel
%   handle : handle de l instance a deplacer
%   lig    : indices de lignes
%   col    : indices des colonnes
%
% See also cl_panel Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function move(this, handle, lig, col)

% -----------------------------------
% Test de validite du handle
% Test de validite max lig et max col

if ~ishandle(handle)
    my_warndlg('cl_panel/move : invalid handle', 0);
elseif get_max_lig(this) < lig
    my_warndlg('cl_panel/move : invalid value for lig', 0);
elseif get_max_col(this) < col
    my_warndlg('cl_panel/move : invalid value for col', 0);
else


    % -----------------------------------------
    % Calcul de la position et repositionnement
    
    if isAGraphicElement(this.handlePanel, 'figure') == 1
        set(this.handlePanel, 'Units', 'pixels');
        p = get(this.handlePanel, 'Position');
        p(1:2) = 0;
        % ----------------------------------------------------------------------
        % Redetermination de la position actuelle en pixels
        this = set_position_panel(this, p);
    end
    Parent=get(handle,'Parent');
    if Parent==gcf
        p = calculer_position(this, lig, col);
    else
        set(handle, 'Units', 'Normalized');
        p = calculer_position_panel(this, lig, col);
    end
    set(this.handlePanel, 'Units', 'Normalized');          % permet de redimensionner la fenetre sur manip opérateur

    if isempty(p)
        msg = 'cl_panel::move, invalid position';
        warndlg(msg, 'Warning');
    else
        if Parent==gcf
            set(handle, 'Units', 'pixels', 'Position', p, 'Units', 'normalized');
        else
            set(handle,'Position',p);
        end
    end
end
