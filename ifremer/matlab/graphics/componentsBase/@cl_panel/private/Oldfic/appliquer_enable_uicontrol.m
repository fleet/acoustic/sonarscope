% Applique l enable aux uicontrols "contenus"
%
% Syntax
%   this = appliquer_enable_uicontrol(this)
%
% Input Arguments
%   this : instance de cl_panel
%
% Output Arguments
%   this = instance de cl_panel
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: appliquer_enable_uicontrol.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = appliquer_enable_uicontrol(this)

for i=1:length(this.tabUicontrols)
    if this.enableUicontrols(i)
        enable = 'on';
    else
        enable = 'off';
    end
    set(this.tabUicontrols(i), 'enable', enable);
end
