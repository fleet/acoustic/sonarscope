% Applique la visibilite aux uicontrols "contenus"
%
% Syntax
%   this = appliquer_visibilite_uicontrol(this)
%
% Input Arguments
%   this : instance de cl_frame
%
% Output Arguments
%   this = instance de cl_frame
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: appliquer_visibilite_uicontrol.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = appliquer_visibilite_uicontrol(this)


for i=1:length(this.tabUicontrols)
    if this.visibleUicontrols(i)
        visibilite = 'on';
    else
        visibilite = 'off';
    end
    set(this.tabUicontrols(i), 'Visible', visibilite);
end
