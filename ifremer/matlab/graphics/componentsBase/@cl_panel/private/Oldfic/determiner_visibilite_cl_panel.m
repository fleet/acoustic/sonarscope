% Determine la visibilite des instances de cl_panel "contenues"
%
% Syntax
%   this = determiner_visibilite_cl_panel(this)
%
% Input Arguments
%   this : instance de cl_panel
%
% Output Arguments
%   this = instance de cl_panel
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: determiner_visibilite_cl_frame.m,v 1.3 2003/06/02 15:24:59 augustin Exp $
% ----------------------------------------------------------------------------

function this = determiner_visibilite_cl_panel(this)

if isempty(this.visible) || strcmp(this.visible, 'none')
    this.visible = 'on';
end

tabVisibilite = [];
[m, n] = size(this.tabClPanel);

for i = 1:m
    for j = 1:n
        visibilite = get(this.tabClPanel(i, j), 'Visible');
        if strcmp(visibilite, 'off')
            tabVisibilite(i, j) = 0;
        else
            tabVisibilite(i, j) = 1;
        end
    end
end

this.visibleClPanel = tabVisibilite;
