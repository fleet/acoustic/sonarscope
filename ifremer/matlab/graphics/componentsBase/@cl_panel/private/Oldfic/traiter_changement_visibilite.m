% Traite un changement de visibilite de l'instance
%
% Syntax
%   this = traiter_changement_visibilite(this)
%
% Input Arguments
%   this : instance de cl_panel
%
% Output Arguments
%   this = instance de cl_panel

function this = traiter_changement_visibilite(this)

% --------------------------------------------------------------------------
% Changement de visibilit� :
%    - la frame devient invisible : lecture de la visibilit� des �le�ments
%      "contenus" (uicontrols et axes), rend tous les �l�ments "contenus"
%      invisibles
%    - la frame devient visible : application de la visibilit� des �l�ments
%      "contenus" (uicontrols et axes) en fonction de leur visibilit� avant
%      que la frame ne soit invisible

if strcmp(this.visible, 'off') == 1
    set(this.handlePanel, 'Visible', 'off')
    
    this = determiner_visibilite(this);
    set(this.tabUicontrols, 'Visible', 'off');
    set(this.tabAxes, 'Visible', 'off');
    if ~isempty(get(this.tabAxes, 'Children'))
        set(get(this.tabAxes, 'Children'), 'Visible', 'off');
    end
    [m, n] = size(this.tabClPanel);
    tabClPanel = this.tabClPanel;
    for i=1:m
        for j=1:n
            tabClPanel(i, j) = set(tabClPanel(i, j), 'Visible', 'off');
        end
    end
    this.tabClPanel = tabClPanel;
else
    
    set(this.handlePanel, 'Visible', 'on')
    this = appliquer_visibilite(this);
end
