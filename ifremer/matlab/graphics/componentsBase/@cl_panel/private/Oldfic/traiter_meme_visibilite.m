% Traite une demande de visibilite (impose de nouveau cette visibilite)
%
% Syntax
%   this = traiter_meme_visibilite(this)
%
% Input Arguments
%   this : instance de cl_panel
%
% Output Arguments
%   this = instance de cl_panel
% ----------------------------------------------------------------------------

function this = traiter_meme_visibilite(this)

% ----------------------------------
% Impose "brutalement" la visibilite

if strcmp(this.visible, 'off') == 1
    set(this.tabUicontrols, 'Visible', 'off');
    set(this.tabAxes, 'Visible', 'off');
    set(get(this.tabAxes, 'Children'), 'Visible', 'off');
    [m, n] = size(this.tabClPanel);
    tabClPanel = this.tabClPanel;
    for i = 1:1:m
        for j = 1:1:n
            tabClPanel(i, j) = set(tabClPanel(i, j), 'Visible', 'off');
        end
    end
    this.tabClPanel = tabClPanel;
else
    this = appliquer_visibilite(this);
end
