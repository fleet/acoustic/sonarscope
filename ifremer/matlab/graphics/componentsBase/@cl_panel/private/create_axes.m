% Cr�ation des uiAxes (instances matlab) � partir d'une liste descriptive
%
% Syntax
%   this = create_uiAxes(this, liste)
%
% Input Arguments
%   this     : instance de cl_panel
%   liste    : liste de structure decrivant les axes
%
% Output Arguments
%   this = instance de cl_panel
%
% Authors : LUR
% ----------------------------------------------------------------------------

function this = create_axes(this, liste)

%% Raz de la liste precedente

this.tabAxes     = [];
this.visibleAxes = [];

%% Liste descriptive des uicontrols : parcours et creation pour chaque element

while ~isempty(liste)
    handle = construireUicontrolFast(this, liste{1});

    % -------------------------------------------------------------
    % Test de cr�ation,
    % ajout a la liste des uicontrols "contenus"
    % ajout du flag de visibilite a la liste des flag de visibilite

    if ishandle(handle)
        this.tabAxes = [this.tabAxes, handle];
        %INUTILE
        %this.visibleAxes = ...
        %[this.visibleAxes, strcmp(get(handle,'Visible'),'on')];
        %this.enableUiAxes = ...
        %    [this.enableUiAxes, strcmp(get(handle, 'Enable'), 'on')];
    else
        msg = 'cl_panel/create_uipanel : can''t create an uipanel';
        my_warndlg(msg, 1);
    end

    %% condition d'arret : vide la liste de l'�l�ment courant

    if length(liste) > 2
        liste = liste(2:length(liste));
    elseif length(liste) == 2
        liste = liste(2);
    else
        liste = {};
    end

end

%% construction d'un axes a partir de sa structure descriptive

function handle = construireUicontrolFast(this, structure)

GCF = gcf;

%% Acces aux noms des champs de la structure

names  = fieldnames(structure);
values = struct2cell(structure);

%% creation du composant axes

handle = axes;

%% initialisation par defaut si certains champs ne sont pas specifies

% initialiserDefaut(handle);

%% Parcours de la structure pour initialisation. On verifie la validite
% du champ que l on renseigne

lstProperties = [];
for k=1:length(names)
    if ~strcmpi(names{k}, 'lig') && ~strcmpi(names{k}, 'col')
        if ~strcmpi(names{k}, 'Style')
            lstProperties{end+1} = names{k}; %#ok<AGROW>
            if ~strcmpi(names{k},'Parent')
                lstProperties{end+1} = values{k}; %#ok<AGROW>
            else
%               hparent = findobj(GCF, 'tag', values{k});
                ListeHandlePanel = getappdata(GCF, 'ListeHandlePanel');
                if isfield(ListeHandlePanel, str2VarName(values{k})) % Ajout� par JMA le 20/09/2015 : A l'essai
                    hparent = ListeHandlePanel.(str2VarName(values{k}));
                else
                    hparent = findobj(GCF, 'tag', values{k});
                end
                lstProperties{end+1} = hparent; %#ok<AGROW>
            end
        end
    end
end
if ~isempty(lstProperties)
    set(handle, lstProperties{:});
end


%% Initialisation par defaut de certaines proprietes de l'axe

% function initialiserDefaut(handle)

%Pas d'initialisation par defaut pour l'instant
