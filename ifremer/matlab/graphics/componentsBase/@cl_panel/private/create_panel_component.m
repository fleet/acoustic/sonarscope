function ret = create_panel_component(this, lig, col, Tagparent, name, oldCpn)

GCF = gcf;

ihm = [];
ret = [];

%% Ancre du panel

ihm{end+1}.Lig   = lig;
ihm{end}.Col     = col;
ihm{end}.Style   = 'panel';
ihm{end}.Parent  = Tagparent;

if oldCpn
    ihm{end}.Tag = name;
else
    ihm{end}.Tag = ['Tag' name '_panel'];
end

%% Cr�ation du panel

retour = create_uipanel(this.MainPanel, ihm);
retour = positionner_uipanel(retour, ihm); %#ok<NASGU>

ListeHandlePanel = getappdata(GCF, 'ListeHandlePanel');
ret.NamePanel = name;
ret.Parent = Tagparent;
if oldCpn
    ret.Tag = name;
%   ret.handlePanel = findobj(GCF, 'tag', name);
    if isfield(ListeHandlePanel, str2VarName(name)) % Ajout� par JMA le 20/09/2015 : A l'essai
        ret.handlePanel = ListeHandlePanel.(str2VarName(name));
    else
        ret.handlePanel = findobj(GCF, 'tag', name);
    end 
else
    ret.Tag = ['Tag' name '_panel'];
%     ret.handlePanel = findobj(GCF, 'tag', ['Tag' name '_panel']);
    
    name = ['Tag' name '_panel'];
    if isfield(ListeHandlePanel, str2VarName(name)) % Ajout� par JMA le 20/09/2015 : A l'essai
        ret.handlePanel = ListeHandlePanel.(str2VarName(name));
    else
        ret.handlePanel = findobj(GCF, 'tag', name);
    end 
end
