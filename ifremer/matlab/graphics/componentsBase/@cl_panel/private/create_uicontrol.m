% Creation des uicontrols (instances matlab) a partir d'une liste descriptive
%
% Syntax
%   this = create_uicontrol(this, liste)
%
% Input Arguments
%   this : instance de cl_panel
%   liste    : liste de structure decrivant les uicontrols
%
% Output Arguments
%   this = instance de cl_panel
%
% ----------------------------------------------------------------------------

function this = create_uicontrol(this, liste)

%% Raz de la liste precedente

this.tabUicontrols     = [];
this.visibleUicontrols = [];

%% Liste descriptive des uicontrols : parcours et creation pour chaque element

while ~isempty(liste)

    % ------------------------------------------------------------------------
    % Cr�ation du premier element de la liste. Deux procedures sont
    % disponibles :
    % - sans verification : pas de verification de la valeur de la propriete
    %   et pas de verification minuscule/majuscule
    % - avec verification : verification de la valeur de la propriete et
    %   verification minuscule/majuscule du nom de la propriete et de sa valeur

    if this.verification
        handle = construireUicontrolVerif(liste{1});
    else
        handle = construireUicontrolFast(this, liste{1});
    end

    % -------------------------------------------------------------
    % Test de cr�ation,
    % ajout a la liste des uicontrols "contenus"
    % ajout du flag de visibilite a la liste des flag de visibilite

    if ishandle(handle)
        this.tabUicontrols = [this.tabUicontrols, handle];
        this.visibleUicontrols = ...
            [this.visibleUicontrols, strcmp(get(handle,'Visible'),'on')];
        this.enableUicontrols = ...
            [this.enableUicontrols, strcmp(get(handle, 'Enable'), 'on')];
    else
        msg = 'cl_panel/create_uicontrol : can''t create an uicontrol';
        my_warndlg(msg, 1);
    end

    %% Condition d'arr�t : vide la liste de l'�lement courant

    if length(liste) > 2
        liste = liste(2:length(liste));
    elseif length(liste) == 2
        liste = liste(2);
    else
        liste = {};
    end
end


% ----------------------------------------------------------------------------
% Construction d'un uicontrol a partir de sa structure descriptive
% Version sans test pour accelerer le processus de creation

function handle = construireUicontrolFast(~, structure)

GCF = gcf;

%% Acces aux noms des champs de la structure

names  = fieldnames(structure);
values = struct2cell(structure);

handle = uicontrol;%('Parent',values{lig});

%% Initialisation par defaut si certains champs ne sont pas specifi�s

initialiserDefaut(handle, structure);

%% Parcours de la structure pour initialisation. On verifie la validite�
% du champ que l'on renseigne

lstProperties = [];

for k=1:length(names)
    if ~strcmpi(names{k}, 'lig') && ~strcmpi(names{k}, 'col')
        lstProperties{end+1} = names{k}; %#ok<AGROW>
        if ~strcmpi(names{k}, 'Parent')
            lstProperties{end+1} = values{k}; %#ok<AGROW>
        else
%           hparent = findobj(GCF, 'tag', values{k});
            ListeHandlePanel = getappdata(GCF, 'ListeHandlePanel');
            if isfield(ListeHandlePanel, str2VarName(values{k})) % Ajout� par JMA le 20/09/2015 : A l'essai
                hparent = ListeHandlePanel.(str2VarName(values{k}));
            else
                hparent = findobj(GCF, 'tag', values{k});
            end
            lstProperties{end+1} = hparent; %#ok<AGROW>
        end
    end
end

if ~isempty(lstProperties)
    try
        set(handle, lstProperties{:});
    catch
        disp('erreur dans la liste de propriete a affecter a un uicontrol')
        disp('handle de l uicontrol')
        handle %#ok<NOPRT>
        disp('liste des attributs et valeurs')
        lstProperties{:} %#ok<NOPRT>
        warndlg('relancez l application');
        return
    end
end




%% Construction d'un uicontrol a partir de sa structure descriptive

function handle = construireUicontrolVerif(structure)

%% Acces aux noms des champs de la structure

names = fieldnames(structure);

%% Test existance du style de l uicontrol (obligatoire)

switch 'Style'
    case names

        %% cr�ation de l uicontrol

        handle = uicontrol;

        %% Initialisation par defaut si certains champs ne sont pas specifies

        initialiserDefaut(handle, structure);

        %% Lecture des proprietes accessibles en ecriture

        properties    = set(handle);
        propertyNames = fieldnames(properties);

        %% Parcours de la structure pour initialisation. On verifie la validite
        % du champ que l on renseigne

        for k=1:length(names)

            %% Test de reconnaissance de l attribut

            switch names{k}
                case {'Lig', 'Col'}
                    % rien a faire !

                case propertyNames

                    %% Test de reconnaissance de la valeur

                    valeursPossibles = properties.(names{k});
                    if ~isempty(valeursPossibles)
                        switch structure.(names{k})
                            case valeursPossibles
                                set(handle, names{k}, structure.(names{k}));

                            otherwise
                                msg = 'cl_panel::create_uicontrol, construireUicontrol, ';
                                msg = [msg, 'invalid property value : ']; %#ok<AGROW>
                                msg = [msg, structure.(names{k})]; %#ok<AGROW>
                                my_warndlg(msg, 1);
                        end

                    else
                        set(handle, names{k}, structure.(names{k}));
                    end

                otherwise
                    msg= 'cl_panel::create_uicontrol, construireUicontrol, ';
                    msg = [msg, 'invalid property : ', names{k}]; %#ok<AGROW>
                    my_warndlg(msg, 1);
            end
        end

    otherwise
        handle = [];
        msg = 'cl_panel::create_uicontrol, construireUicontrol, Style not defined';
        my_warndlg(msg, 1);
end



% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Initialisation par defaut de certaines proprietes de l uicontrol si
% ces proprietes ne sont pas specifiees dans la structure descriptive

function initialiserDefaut(handle, structure)

% ----------------------------------------------------------------------------
% Acces aux noms des champs de la structure

names = fieldnames(structure);

%% Traitement de la couleur

switch 'backgroundcolor'
    case lower(names)
        % couleur specifiee : pas de mise en defaut
    otherwise
        % couleur non specifiee, initialisation en fonction du style

        % recherche du style
        for k=1:length(names)
            if strcmpi(names{k}, 'Style')
                style = names {k};
            end
        end

        % initialisation en fonction du sytle
%         switch getfield(structure, style)
        switch structure.(style)
            case {'listbox', 'frame', 'pushbutton', 'slider', 'text', 'checkbox', 'togglebutton', 'radiobutton'}
                set(handle, 'BackgroundColor', Gris);
            
            case 'popupmenu'
                set(handle, 'BackgroundColor', GrisClair);

            case 'edit'
                set(handle, 'BackgroundColor', [1 1 1]);

            otherwise
                msg = 'cl_panel/create_uicontrol : initialiserDefaut, ';
                msg = [msg, 'unknown style, background color set to default'];
                my_warndlg(msg, 1);
        end
end
