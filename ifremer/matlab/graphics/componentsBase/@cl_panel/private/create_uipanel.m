% Creation des uipanels (instances matlab) a partir d'une liste descriptive
%
% Syntax
%   this = create_uipanel(this, liste)
%
% Input Arguments
%   this : instance de cl_panel
%   liste    : liste de structure decrivant les uicontrols
%
% Output Arguments
%   this = instance de cl_panel
%
% Authors : LUR (ATL)
% ----------------------------------------------------------------------------

function this = create_uipanel(this, liste)

%% Raz de la liste precedente

this.tabUipanels     = [];
this.visibleUipanels = [];

%% Liste descriptive des uipanels : parcours et cr�ation pour chaque �l�ment

GCF = gcf;
while ~isempty(liste)

    handle = construireUicontrolFast(this, liste{1}, GCF);

    % -------------------------------------------------------------
    % Test de creation,
    % ajout a la liste des uipanels "contenus"
    % ajout du flag de visibilite a la liste des flag de visibilite
    if ishandle(handle)
        this.tabUipanels = [this.tabUipanels, handle];
        %INUTILE
        %this.visibleUipanels = ...
        %[this.visibleUipanels, strcmp(get(handle,'Visible'),'on')];
        %this.enableUipanels = ...
        %[this.enableUipanels, strcmp(get(handle, 'Enable'), 'on')];
    else
        msg = 'cl_panel/create_uipanel : can''t create an uipanel';
        my_warndlg(msg, 1);
    end

    %% Condition d'arr�t : vide la liste de l'�l�ment courant

    if length(liste) > 2
        liste = liste(2:length(liste));
    elseif length(liste) == 2
        liste = liste(2);
    else
        liste = {};
    end

end

%% Construction d'un uipanel � partir de sa structure descriptive
% Version sans test pour accelerer le processus de creation

function handle = construireUicontrolFast(this, structure, GCF)

%% Acc�s aux noms des champs de la structure

names  = fieldnames(structure);
values = struct2cell(structure);

%% Cr�ation de l'uicontrol

handle = uipanel;

ListeHandlePanel = getappdata(GCF, 'ListeHandlePanel');
for k=1:length(names)
    if strcmp(names{k}, 'Tag')
        ListeHandlePanel.(str2VarName(values{k})) = handle;
    end
end
setappdata(GCF, 'ListeHandlePanel', ListeHandlePanel);

%% Initialisation par d�faut si certains champs ne sont pas sp�cifi�s

initialiserDefaut(handle);

%% Parcours de la structure pour initialisation

BorderColor   = [];
lstProperties = [];

for k=1:length(names)
    if strcmpi(names{k}, 'BorderColor')
        BorderColor = values{k};
    end
end

for k=1:length(names)
    if ~strcmpi(names{k}, 'lig') && ~strcmpi(names{k}, 'col')
        if ~strcmpi(names{k}, 'Style') && ~strcmpi(names{k}, 'BorderColor')
            lstProperties{end+1} = names{k}; %#ok<AGROW>
            if ~strcmpi(names{k}, 'Parent')
                lstProperties{end+1} = values{k}; %#ok<AGROW>
            else
                if ischar(values{k})
                    if isfield(ListeHandlePanel, str2VarName(values{k})) % Ajout� par JMA le 20/09/2015 : A l'essai
                        hparent = ListeHandlePanel.(str2VarName(values{k}));
                    else
                        hparent = findobj(GCF, 'tag', values{k});
                    end
                else
                    hparent = values{k};
                end
                lstProperties{end+1} = hparent; %#ok<AGROW>
            end
            
            %% Ajout d'une propriete Border aux panels et gestion de la
            %couleur A l'essai LUR
            if strcmpi(names{k},'Border')
                if values{k} == 1
                    lstProperties{end-1 }= 'HighlightColor'; %#ok<AGROW>
                    if isempty(BorderColor)
                        lstProperties{end} = [0.8 0.8 0.8];
                    else
                        lstProperties{end} = BorderColor;
                    end
                    lstProperties{end+1} = 'ShadowColor'; %#ok<AGROW>
                    if isempty(BorderColor)
                        lstProperties{end+1} = [0.8 0.8 0.8]; %#ok<AGROW>
                    else
                        lstProperties{end+1} = BorderColor; %#ok<AGROW>
                    end
                end
            end
        end
    end
end

if ~isempty(lstProperties)
    try
        set(handle, lstProperties{:});
    catch
        disp('erreur sur les proprietes du composant');
%         lstProperties{:}
        return
        %LURLURLUR
    end
end

%% Initialisation par defaut de certaines proprietes des uipanels si
% ces proprietes ne sont pas specifiees dans la structure descriptive

function initialiserDefaut(handle)

%% traitement de l'aspect global des panels

set(handle, 'BackgroundColor', Gris);
set(handle, 'BorderType',      'beveledin');
set(handle, 'BorderWidth',     1);
set(handle, 'ForegroundColor', Gris);
set(handle, 'HighlightColor',  Gris);
set(handle, 'ShadowColor',     Gris);

%set(handle, 'ShadowColor', [0.8 0.8 0.8]);
%set(handle, 'BorderType', 'beveledout');
%set(handle, 'BorderWidth', 1);
%set(handle, 'ForegroundColor', 'black');
%set(handle, 'ShadowColor', 'black');
