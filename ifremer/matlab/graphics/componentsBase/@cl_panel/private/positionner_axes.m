% Positionne les axes
%
% Syntax
%   this = positionner_axes(this, liste)
%
% Input Arguments
%   this  : instance de cl_panel
%   liste : liste de structure decrivant les axes
%
% Output Arguments
%   this = instance de cl_panel
%
% Authors : LUR
% ----------------------------------------------------------------------------

function this = positionner_axes(this, liste)

% --------------------------------------------------------------------------
% Si nombre max de ligne et colonne non initialises, determination a partir
% de la liste

maxLig = 1;
maxCol = 1;
for i=1:length(liste)
    if isfield(liste{i}, 'Lig') && isfield(liste{i}, 'Col')
        if maxLig < max(liste{i}.Lig)
            maxLig = max(liste{i}.Lig);
        end
        if maxCol < max(liste{i}.Col)
            maxCol = max(liste{i}.Col);
        end
    else
        msg = 'cl_panel::positionner_axes, Lig or Col missing';
        warndlg(msg, 'Warning');
    end
    if get_max_lig(this) < maxLig
        this = set_max_lig(this, maxLig);
    end
    if get_max_col(this) < maxCol
        this = set_max_col(this, maxCol);
    end
end

% --------------------------------------------------------------------
% Parcours de la liste, positionnement pour chaque element de la liste

for i=1:length(liste)

    % --------------------------------------------------------------------
    % Test de la presence des champs Lig et Col dans la structure courante
    % positionnement
    if isfield(liste{i}, 'Lig') && isfield(liste{i}, 'Col')
        this.ligUipanels {end+1} = liste{i}.Lig;
        this.colUipanels {end+1} = liste{i}.Col;
        move(this, this.tabAxes(i), liste{i}.Lig, liste{i}.Col);
    else
        my_warndlg('cl_panel/positionner_axes : Lig or Col missing', 1);
    end

end
