% Accesseur en ecriture de la liste des uicontrols
%
% Syntax
%   this = set_tab_uicontrols(this, tabUicontrols)
%
% Input Arguments
%   this      : this de cl_panel
%   tabUicontrols : liste de structure decrivant les uicontrols ou
%                    tableau de handles
%
% Output Arguments
%   this : this de cl_panel initialisee
%
% ----------------------------------------------------------------------------

function this = set_tab_uicontrols(this, tabUicontrols)

% ------------------------------------------------
% Pas de position => essai avec la figure courante


if isempty(this.positionPanel)
    this = set_handle_panel(this, []);
end

% ----------------------------------------------------
% Initialisation de l attribut :
%    . si liste de structures, creation des uicontrols
%    . si tableau de handle, initialisation directe

if iscell(tabUicontrols)
    this = create_uicontrol(this, tabUicontrols);
    this = positionner_uicontrol(this, tabUicontrols);
elseif ishandle(tabUicontrols)
    this.tabUicontrols = tabUicontrols;
    this = determiner_visibilite_uicontrol(this);
    this = determiner_enable_uicontrol(this);
else
    my_warndlg('cl_panel/set_tab_uicontrols : invalid list', 1);
end
this = appliquer_visibilite_uicontrol(this);
this = appliquer_enable_uicontrol(this);
