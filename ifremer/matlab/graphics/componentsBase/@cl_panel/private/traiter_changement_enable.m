function this = traiter_changement_enable(this,varargin)

if strcmp(varargin(1),'on')
  State = 'on';
elseif strcmp(varargin(1),'off')
  State = 'off';
else
  my_warndlg('cl_panel/traiter_changement_enable.m : State invalide', 1);    
end

switch length(varargin)
    case 1
        Tag    = get(this, 'NomPanel');
        hpanel = findobj('tag', Tag);
    case 2
        hpanel = varargin(2);
        hpanel = hpanel{1};
    otherwise
        my_warndlg('cl_panel/traiter_changement_enable.m : trop de paramètres', 1);
end


handle = get(hpanel, 'children');
if ~isempty(handle)    
    children = allchild(hpanel);
    for k=1:length(children)   
        type = get(children(k),'type');

        if strcmp(type, 'uicontrol')
            if strcmp(State, 'on') %|| strcmp(State, 'off')     
                tab = get(children(k), 'UserData');
                if isfield(tab, 'enable')
                    if ~isempty(tab.enable)
                        lastState = tab.enable;
                        %--------------------------------------------------
                        if strcmp(lastState, 'on')    %on modifie l'etat
                            
                            set(children(k), 'enable', 'on');   %OK!
                            %On vide la sauvegarde
                            tab.enable = [];
                            set(children(k), 'UserData', tab);
      
                        end
                        %--------------------------------------------------
                        if strcmp(lastState, 'off')

                            set(children(k), 'enable', 'off'); %on ne modifie rien 
                            
                        end   
                        %--------------------------------------------------
                    else
                        set(children(k), 'enable', State);
                    end
                else                 
                    %application du nouvel etat
                    set(children(k), 'enable', State);

                end
                
            elseif strcmp(State, 'off')  
                %sauvegarde de l'etat actuel
                tab.enable = get(children(k), 'enable');
                if ~strcmp(tab.enable, 'off')
                    set(children(k), 'UserData', tab);
                end
                %on modifie l'etat
                set(children(k), 'enable', State);
            else         
                my_warndlg('cl_panel/traiter_changement_enable.m : State invalide', 1);       
            end
        elseif strcmp(type, 'uipanel')        
            traiter_changement_enable(this,State,children(k));      
        else
            my_warndlg('cl_panel/traiter_changement_enable.m : Type invalide', 1);
        end
    end
end
