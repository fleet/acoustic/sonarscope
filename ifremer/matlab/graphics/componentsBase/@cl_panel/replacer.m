% Place l'element sur une nouvelle position et conserve cette position
% 
% Syntax
%   this = deplacer(this, type, indice, lig, col)
%
% Input Arguments
%   this   : instance de cl_frame
%   type   : type de l element ('uicontrol' ou 'axe')
%   indice : indice de l element (ordre de creation)
%   lig    : position en ligne
%   col    : position en colonne
%
% Examples
%   h = figure; 
%   clear uic
%   uic{1}.Lig   = 1:2;
%   uic{1}.Col   = 1;
%   uic{1}.Style = 'frame';
%   uic{1}.Tag   = 'container';
%   uic{2}.Lig   = 1;
%   uic{2}.Col   = 2;
%   uic{2}.Style = 'pushbutton';
%
%   f = cl_frame('MaxLig', 2, 'MaxCol', 2, 'InsetX', 20, 'InsetY', 20);
%   set(f, 'Nom', 'f');
%   set(f, 'ListeUicontrols', uic);
%   replacer(f, 'uicontrol', 1, 1, 1:2);
%   update(f);
%    
%
% See also cl_frame Authors
% Authors : DCF
% VERSION  : $Id: replacer.m,v 1.4 2003/05/20 15:47:41 augustin Exp $
% ----------------------------------------------------------------------------

function this = replacer(this, type, indice, lig, col)

% --------------------------
% Selection uicontrol ou axe

switch type
    case 'uicontrol'
        this.ligUicontrols {indice} = lig;
        this.colUicontrols {indice} = col;
    case 'axe'
        this.ligAxes {indice} = lig;
        this.colAxes {indice} = col;
    otherwise
        my_warndlg('cl_frame/replacer : unknown type', 1);
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
