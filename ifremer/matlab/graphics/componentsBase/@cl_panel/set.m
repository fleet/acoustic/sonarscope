% Modification des attributs de l instance
%
% Syntax
%   instance = set(this, ...)
%
% Input Arguments
%   this : instance de cl_panel
%
% Name-Value Pair Arguments
%   Alias           : nom d alias du panel
%   NomPanel        : nom de tag du panel matlab
%   PositionPanel   : position du panel
%   HandlePanel     : handle du panel
%   Visible         : visibilite du panel
%   Verification    : flag de verification des noms de proprietes des
%                      instances matlab supportees par le panel
%   MaxLig          : nombre maximal de lignes
%   MaxCol          : nombre maximal de colonnes
%   InsetX          : marge selon l axe x
%   InsetY          : marge selon l axe y
%   Enable          : accessibilite du panel
%   ListeUicontrols : liste des instances des uicontrol
%
% Output Arguments
%   this : instance de cl_panel initialise
%
% Remarks : par defaut (variable de retour non precisee), l'instance donnee en
%    arguments est initialisee
% ----------------------------------------------------------------------------

function this = set(this, varargin)

%% Lecture des arguments

[varargin, alias] = getPropertyValue(varargin, 'Alias', []);
[varargin, nomPanel] = getPropertyValue(varargin, 'NomPanel', []);
[varargin, positionPanel] = getPropertyValue(varargin, 'PositionPanel', []);
[varargin, handlePanel] = getPropertyValue(varargin, 'HandlePanel', []);
[varargin, verification] = getPropertyValue(varargin, 'Verification', 0);
[varargin, maxLig] = getPropertyValue(varargin, 'MaxLig', []);
[varargin, maxCol] = getPropertyValue(varargin, 'MaxCol', []);
[varargin, insetX] = getPropertyValue(varargin, 'InsetX', []);
[varargin, insetY] = getPropertyValue(varargin, 'InsetY', []);
[varargin, enable] = getPropertyValue(varargin, 'Enable', []);
[varargin, visible] = getPropertyValue(varargin, 'Visible', []);
[varargin, nbRows] = getPropertyValue(varargin, 'nbRows', []);
[varargin, nbCol] = getPropertyValue(varargin, 'nbCol', []);
[varargin, parent] = getPropertyValue(varargin, 'Parent', {});

%% Les mots cles des suivants sont conserves pour compatibilite ascendante
% et homogeneite des mots cles

[varargin, tabUicontrols] = getPropertyValue(varargin, 'ListeUicontrols', []);
% [varargin, tabUipanels] = getPropertyValue(varargin, 'ListeUipanels', []);
[varargin, tabUiComposants] = getPropertyValue(varargin, 'ListeUiComposants', []);
[varargin, tabAxes] = getPropertyValue(varargin, 'ListeAxes', []);
[varargin, tabClPanel] = getPropertyValue(varargin, 'ListeClPanel', []);

%% Mot cle conserve pour compatibilite temporaire mais posant probleme
% avec nomFrame

[varargin, nom] = getPropertyValue(varargin, 'nom', []);

%% Initialisation de la super classe

if ~isempty(maxLig)
    this.cl_abstract_layout = set_max_lig(this.cl_abstract_layout, maxLig);
end
if ~isempty(maxCol)
    this.cl_abstract_layout = set_max_col(this.cl_abstract_layout, maxCol);
end
if ~isempty(insetX)
    this.cl_abstract_layout = set_inset_x(this.cl_abstract_layout, insetX);
end
if ~isempty(insetY)
    this.cl_abstract_layout = set_inset_y(this.cl_abstract_layout, insetY);
end

%% Initialisation des attributs

if ~isempty(alias)
    this = set_alias(this, alias);
end
if ~isempty(nom)
    this = set_alias(this, nom);
end
if ~isempty(verification)
    this = set_verification(this, verification);
end
if ~isempty(nomPanel)
    this = set_nom_panel(this, nomPanel);
end
if ~isempty(handlePanel)
    this = set_handle_panel(this, handlePanel);
end
if ~isempty(positionPanel)
    this = set_position_panel(this, positionPanel);
end
if ~isempty(visible)
    this = set_visible(this, visible);
end

if ~isempty(tabUicontrols)
    this = set_tab_uicontrols(this, tabUicontrols);
end

if ~isempty(tabUiComposants)
    this = set_tab_uiComposant(this, tabUiComposants);
end

if ~isempty(tabAxes)
    this = set_tab_axes(this, tabAxes);
end
if ~isempty(tabClPanel)
    this = set_tab_panel(this, tabClPanel);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end

% if ~isempty(children)
%     this = set_children(this, children);
% end
%
if ~isempty(parent)
    this = set_parent(this, parent);
end

if ~isempty(nbRows)
    this = set_nbLig(this, nbRows);
end

if ~isempty(nbCol)
    this = set_nbCol(this, nbCol);
end

%% Test analyse des arguments

if ~isempty(varargin)
    my_warndlg('cl_panel/set: Argument error', 1);
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
