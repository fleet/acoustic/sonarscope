% Accesseur en ecriture du handle matlab du panel
%
% Syntax
%   this = set_handle_panel(this, handle)
%
% Input Arguments
%   this   : instance de cl_panel
%   handle : tag de l instance panel matlab
%
% Output Arguments
%   this : instance de cl_panel initialise

function this = set_handle_panel(this, handle)

GCF = gcf;

%% handle valide ?

if ishandle(handle)

    % -------------------------------
    % handle valide :
    % - initialisation du handle
    % - initialisation de la position

    this.handlePanel = handle;
    %
    units = get(handle, 'Units');
    set(handle, 'Units', 'pixels');
    position = get(handle, 'Position');
    set(handle, 'Units', units);
    this = set_position_panel(this, position);

elseif isempty(handle)

    % ---------------------------------------------------------
    % handle invalide et vide
    % - determination du handle a partir du nom de la frame
    % - determination du handle a partir de la fenetre courante

    if isempty(this.nomPanel)
        handle = GCF;
        if ~ishandle(handle)
            my_warndlg('cl_panel/set_handle_panel : invalid figure handle', 0);
        else
            units = get(handle, 'Units');
            set(handle, 'Units', 'pixels');
            position = get(handle, 'Position');
            set(handle, 'Units', units);
            position(1:2) = 0;
            
            this = set_handle_panel(this, handle);
            this = set_position_panel(this, position);
        end

    else
%       handle = findobj(GCF, 'Tag', this.nomPanel);
        ListeHandlePanel = getappdata(GCF, 'ListeHandlePanel');
        if isfield(ListeHandlePanel, str2VarName(this.nomPanel)) % Ajout� par JMA le 20/09/2015 : A l'essai
            handle = ListeHandlePanel.(str2VarName(this.nomPanel));
        else
            handle = findobj(GCF, 'tag', this.nomPanel);
        end
        
        if isempty(handle)
            my_warndlg('cl_panel/set_handle_panel : invalid object tag', 0);
        elseif ~ishandle(handle)
            my_warndlg('cl_panel/set_handle_panel : invalid object handle', 0);
        else
            units = get(handle, 'Units');
            set(handle, 'Units', 'pixels');
            position = get(handle, 'Position');
            set(handle, 'Units', units);
            this = set_handle_panel(this, handle);
            this = set_position_panel(this, position);
        end

    end
else
    my_warndlg('cl_panel/set_handle_panel : invalid handle', 1);
end

%% Gestion de la visibilit� : force la nouvelle visibilite

% visibilite = get(this.handlePanel, 'Visible');
this.visible = 'none';
