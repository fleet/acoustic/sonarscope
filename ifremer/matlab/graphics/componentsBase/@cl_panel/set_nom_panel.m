% Accesseur en ecriture du nom du panel, le tag matlab correspondant
% 
% Syntax
%   this = set_nom_panel(this, nomPanel)
%
% Input Arguments
%   this : instance de cl_panel
%   nomFrame : tag de l instance frame matlab
%
% Output Arguments
%   this : instance de cl_panel initialise
%
% ----------------------------------------------------------------------------

function this = set_nom_panel(this, nomPanel)

% --------
% Locales

isOk = 1;

% --------------------------
% Test de validite arguments

if ~ischar(nomPanel)
    isOk = 0;
    my_warndlg('cl_panel/set_nom_panel : Invalide argument type', 1);
end

% ----------------------------
% Initialisation de l instance

if isOk
    this.nomPanel = nomPanel;
end
