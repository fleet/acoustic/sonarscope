% Initialisation de la position du panel
%
% Syntax
%   this = set_position_panel(this, positionPanel)
%
% Input Arguments
%   this          : instance de cl_panel
%   positionPanel : position du panel
%
% Output Arguments
%   this = instance de cl_panel

% ----------------------------------------------------------------------------


function this = set_position_panel(this, positionPanel)
    
    % ----------------------------
    % Initialisation de l attribut
    
    this.positionPanel = positionPanel;
    
    % ---------------------------------
    % Initialisation de la super classe
    
    this.cl_abstract_layout = set_position(this.cl_abstract_layout, positionPanel);
    