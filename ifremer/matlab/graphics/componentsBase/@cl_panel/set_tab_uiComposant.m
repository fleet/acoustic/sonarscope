% Accesseur en ecriture de la liste des uiComposants
%
% Syntax
%   this = set_tab_uiComposant(this, tabUiComposants)
%
% Input Arguments
%   this            : this de cl_panel
%   tabUiComposants : liste de structures decrivant les composants
%   (uicontrols, uipanels et axes)
%
% Output Arguments
%   this : this de cl_panel initialisee
% ----------------------------------------------------------------------------

function this = set_tab_uiComposant(this, tabUiComposants)

this.tabUiComposants = tabUiComposants;

%% Initialisation des listes de composants

tabUiPanels   = [];
tabUiControls = [];
tabUiAxes     = [];
tabUiCpn      = [];

%% Pas de position => essai avec la figure courante

if isempty(this.positionPanel)
    this = set_handle_panel(this, []);
end

%% D�tection du style des composants uipanels/uicontrols/axes

if iscell(tabUiComposants)
    j = 1;
    k = 1;
    l = 1;
    m = 1;
    for k2=1:size(tabUiComposants,2)
        if strcmpi(tabUiComposants{1,k2}.Style, 'panel')
            tabUiPanels{1,j} = tabUiComposants{1,k2}; %#ok<AGROW>
            j = j+1;
        elseif strcmpi(tabUiComposants{1,k2}.Style, 'axes')
            tabUiAxes{1,l} = tabUiComposants{1,k2}; %#ok<AGROW>
            l = l + 1;
        elseif  strfind(tabUiComposants{1,k2}.Style,'clc')
            tabUiCpn{1,m} = tabUiComposants{1,k2}; %#ok<AGROW>
            m = m + 1;
        else
            tabUiControls{1,k} = tabUiComposants{1,k2}; %#ok<AGROW>
            k = k + 1;
        end
    end
    
    %% On fixe MaxLig et MaxCol dans le cas o� on a des composants de plusieurs
    %types. (Permet le positionnement correct des composants meme si on ne les
    %traite pas ensemble
    
    emptyControls   = ~isempty(tabUiControls);
    emptyPanels     = ~isempty(tabUiPanels);
    emptyAxes       = ~isempty(tabUiAxes);
    emptyCpn        = ~isempty(tabUiCpn);
    
    % condition = emptyControls+emptyPanels+emptyAxes+emptyCpn;
    
    % if condition > 1
    for k2=1:length(tabUiComposants)
        maxi_col = max(tabUiComposants{k2}.Col);
        maxi_lig = max(tabUiComposants{k2}.Lig);
        %         l=get(this,'MaxLig');
        %         c=get(this,'MaxCol');
        l = get_max_lig(this);
        c = get_max_col(this);
        if maxi_col > c
            this = set_max_col(this, maxi_col);
            %this = set(this,'MaxCol',maxi_col);
        end
        if maxi_lig > l
            this = set_max_lig(this, maxi_lig);
            %this = set(this,'MaxLig',maxi_lig);
        end
    end
    % end
    
    %% On peut fixer un nombre de lignes et de colonnes arbitrairement
    
    if this.nbCol > maxi_col
        this = set_max_col(this,this.nbCol);
    end
    
    if this.nbRows > maxi_lig
        this = set_max_lig(this, this.nbRows);
    end
    
    %% On cr�e et on positionne les uiControls
    
    if emptyControls
        this = create_uicontrol(this, tabUiControls);
        this = positionner_uicontrol(this, tabUiControls);
    end
    
    %% On cr�e et on positionne les uiPanels
    
    if emptyPanels
        this = create_uipanel(this, tabUiPanels);
        this = positionner_uipanel(this, tabUiPanels);
    end
    
    %% On cr�e et on positionne les uiAxes
    
    if emptyAxes
        this = create_axes(this, tabUiAxes);
        this = positionner_axes(this, tabUiAxes);
    end 
    
    %% On cr�e et on positionne les composants
    
    if emptyCpn
        this = create_component2(this, tabUiCpn);
    end
    
    %% On met � jour la visibilit� des uiControls
    % A REVOIR SI BESOIN pas de modifications depuis cl_frame
    
    % elseif ishandle(tabUicontrols)
    %     this.tabUicontrols = tabUicontrols;
    %     this = determiner_visibilite_uicontrol(this);
    %     this = determiner_enable_uicontrol(this);
else
    my_warndlg('cl_panel/set_tab_uicontrols : invalid list', 1);
end
% if emptyControls
% this = appliquer_visibilite_uicontrol(this);
% this = appliquer_enable_uicontrol(this);
% end
%this