% Accesseur en ecriture du flag de verification des noms de propriete
%
% Syntax
%   this = set_verification(this, verification)
%
% Input Arguments
%   this         : instance de cl_panel
%   verification : flag de verification
%
% Output Arguments
%   this : instance de cl_panel initialisee
% ----------------------------------------------------------------------------

function this = set_verification(this, verification)
this.verification = verification;
