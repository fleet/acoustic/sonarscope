% Methode de gestion des call back du composant
%
% Syntax
%   this = callback(this, cbName)
% 
% Input Arguments
%   this     : instance de clc_slider
%   cbName   : nom de la call back
%   varargin : parametres optionnels de la call back
%
% Output Arguments
%   this : instance de cl_component initialisée
%
% Remarks :
%   doit etre surchargée
%
% Authors : LUR
% ----------------------------------------------------------------------------

function this = callback(this, varargin)

if length(varargin) == 1
    switch varargin{1}
        case 'File'
            h = get(this, 'HandleCpnBrowser');
            file = get(h, 'SelectedFile');
            this = set_file(this,file);
        otherwise
            my_warndlg('clc_browser/callback : Invalid message', 0);
    end
else
    my_warndlg('clc_browser/callback : Invalid arguments number', 0);
end
