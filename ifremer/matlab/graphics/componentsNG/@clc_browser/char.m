% Representation textuelle d un tableau d'instances de clc_slider
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance ou tableau d instance de clc_slider
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%    char (clc_slider)
%
% Authors : LUR
% VERSION  : $Id: char.m,v 1 2007/07/26
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    26/07/2007 - LUR - creation
% ----------------------------------------------------------------------------

function str = char(this)
    
    % -------------------------
    % Intialisation des locales
    
    str = [];
    
    % ------------------------------------
    % Traitement tableau ou instance seule
    
    [m, n] = size(this);
    
    if (m*n) > 1
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
                str1 = char(this(i, j));
                str{end+1} = str1;
            end
        end
        str = cell2str(str);

    else
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
