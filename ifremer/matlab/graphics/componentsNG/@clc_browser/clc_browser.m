% Constructeur du composant clc_brower : composant java d'exploration et
% d'importation, exploration, sauvegarde de fichiers.
%
% Syntax
%     this = clc_browser(...)
% 
% Name-Value Pair Arguments
%     componentName : nom associ� � ce composant
%
% Output Arguments 
%     this : instance de clc_browser
%
% Remarks : 
%
% Examples
%
%     ihm = {};
%     
%     % Attributs g�n�riques d'un composant
%     ihm{end+1}.Style        = 'clc_browser';
%     ihm{end}.Lig            = 2:4;
%     ihm{end}.Col            = 2:4;
%     ihm{end}.Name           = 'BrowserExport';
%     ihm{end}.Parent         = 'AncrePanel';
%     ihm{end}.UserName       = 'cla_carto';
%     ihm{end}.UserCb         = 'callback';
%  
%     %Attributs specifiques du composant
%     ihm{end}.ControlButtonsAreShown     = 0;
%     ihm{end}.AcceptAllFileFilterUsed    = 0;
%     ihm{end}.filter                     = {'*.def', 'def','*.jpg', 'jpg','*.txt', 'txt'};
%     
%     %this.ihm.BrowserExport = create_component(cl_component,ihm);
%     
%     ihm{end+1}.Lig            = 1:5;
%     ihm{end}.Col              = 1:5;
%     ihm{end}.Style            = 'panel';
%     ihm{end}.Tag              = 'AncrePanel';
%     ihm{end}.BackGroundColor  = grisclair;
% 
%     ihm_global = cl_panel([]);
%     ihm_global = set_tab_uiComposant(ihm_global, ihm);
%
% Authors : LUR
% -------------------------------------------------------------------------

function this = clc_browser(varargin)

%% D�finition de la structure et initialisation

this.globalPanel             = [];    % Internal property
this.HandleCpnBrowser        = [];    % Internal property

this.file                    = [];   % nom complet du fichier selectionne 
this.ficName                 = [];   % nom du fichier entr� dans le champ du browser;
this.filter                  = 0;    % filtre
this.FileSelectionMode       = 0;    % 0 affiche le nom de fichier, 1 affiche le chemin en +
this.ControlButtonsAreShown  = 1;    % pas de boutons en bas a droite (0)  
this.MultiSelectionEnabled   = 0;    % autorise la multiselection
this.AcceptAllFileFilterUsed = 1;    % (1) posibilite de selctionner nimporte quel type de fichier
this.currentDir              = '';
this.msgBrowser              = [];   % message associ� au callbacks du browser
this.labelParcourirVisible   = 'on';
this.defFilename             = '';
this.FileNameVisible         = 1;    % Affiche la zone d'affichage du fichier s�lectionn� 
                                     % (interne au composant JFileChooser)

%this.ApproveButtonText        = ''; % change le texte sur le bouton ouvrir
%this.ApproveButtonToolTipText = '';
%this.DialogType               = 0;  % 1=enregister, 0= ouvrir

%% Super-classe

if (nargin == 1) && isempty(varargin{1})
    composant = cl_component([]);
else
    composant = cl_component;
end

%% Creation de l'objet

this = class(this, 'clc_browser', composant);

%% Pre-nitialisation des champs

if (nargin == 1) && isempty(varargin {1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
