% Destruction graphique du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_slider
%
% Remarks :
%   doit etre surchargee
%
% Authors : LUR
% VERSION  : $Id: delete.m,v 1 2007/07/26
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    26/07/2007 - LUR - creation
% ----------------------------------------------------------------------------

function this = delete(this)


    % ----------------------------------------
    % Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    % -----------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(this.globalPanel)
        this.globalPanel = delete(this.globaloPanel);
        this.globalPanel = [];
    end
    

