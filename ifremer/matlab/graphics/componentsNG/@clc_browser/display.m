% Affichage des donnees d'une instance de clc_slider
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : instance de clc_slider
%
% Examples
%   display(clc_slider)
%
% Authors : LUR
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
