% Appel de la representation graphique de l'instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_slider
%
% OUPUT PARAMETERS :
%   this : instance de clc_slider
%
% Authors : LUR
% VERSION  : $Id: editobj.m,v 1 2007/07/26
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Creation de l'interface graphique

if ~is_edit(this)
    this.cl_component = editobj(this.cl_component);
    this = construire_ihm(this);
end
