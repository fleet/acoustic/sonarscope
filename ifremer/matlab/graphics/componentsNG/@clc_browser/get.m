% Lecture des attributs et proprietes de l'instance
%
% Syntax
%   varargout = get(this, ...)
% 
% Input Arguments
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    v = clc_browser;
%    v = editobj(v);
%
% Authors : LUR
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'HandleCpnBrowser'
            varargout{end+1} = get_HandleCpnBrowser(this); %#ok<AGROW>
        case 'file'
            varargout{end+1} = get_file(this); %#ok<AGROW>
        case 'currentDir'
            varargout{end+1} = get_currentDir(this); %#ok<AGROW>
        case 'defFilename'
            varargout{end+1} = this.defFilename; %#ok<AGROW>
        case 'filter'
            varargout{end+1} = get_filter(this); %#ok<AGROW>
        case 'ficName'
            varargout{end+1} = get_ficName(this); %#ok<AGROW>
        case 'flagValid'
            varargout{end+1} = this.flagValid; %#ok<AGROW>
        case{ 'componentVisible', 'componentEnable', 'componentName', ...
                'componentAnchor','componentParent' , 'componentUserName', 'componentUserCb' }
            varargout{end+1} = get(this.cl_component, varargin{i}); %#ok<AGROW>
        otherwise
            my_warndlg('clc_browser/get : Invalid property name', 0);
    end
end
