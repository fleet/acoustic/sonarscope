% Retourne le handle du composant graphique
%
% Syntax
%   HandleCpnSlider= get_HandleCpnSlider(this)
%
% Input Arguments
%   this : instance de clc_slider
%
% Output Arguments
%   HandleCpnSlider : handle du composant
%
% Authors : LUR
% VERSION  : $Id: get_HandleCpnSlider.m,v 1 2007/07/26
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    26/07/2007 - LUR - creation
% ----------------------------------------------------------------------------

function HandleCpnBrowser= get_HandleCpnBrowser(this)


    HandleCpnBrowser = this.HandleCpnBrowser;
