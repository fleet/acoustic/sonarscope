function currentDir = get_currentDir(this)

pppp = get(this.HandleCpnBrowser, 'JavaComponent');
Dir = getCurrentDirectory(pppp);

%% transformation du repertoire java en chaine de caractere MatLab

currentDir = Dir.toString.toCharArray';

%% MAJ du repertoire courant dans l'instance (facultatif)

this.currentDir = currentDir;
