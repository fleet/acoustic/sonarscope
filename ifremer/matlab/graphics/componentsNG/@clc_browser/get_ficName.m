% TODO : ne fonctionne pas si look unix : javax.swing.UIManager.setLookAndFeel('com.jgoodies.looks.plastic.Plastic3DLookAndFeel');

function ficName = get_ficName(this)

pppp = get(this.HandleCpnBrowser, 'JavaComponent');

%% Récuperation de la zone de texte dans l'arborescence du cpn

panel1      = pppp.Components(3);
panel2      = get(panel1,'Components');
panel3      = get(panel2(3),'Components');
panel4      = get(panel3(3),'Components');
contenuText = panel4(2).getText;

%% Transformation du repertoire java en chaine de caractere MatLab

ficName = contenuText.toString.toCharArray';

%% MAJ du nom de fichier dans l'instance (facultatif)

this.ficName = ficName;
