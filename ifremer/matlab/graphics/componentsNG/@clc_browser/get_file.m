% Lecture des fichiers sélectionnés dans le File Browser
%
% Syntax
%   varargout = get(this, ...)
%
% Input Arguments
%    this              : instance de clc_browser
%
% Output Arguments
%   Liste du(des) fichier(s) sélectionné(s)
%
% Examples
% figure;
% ihm={};
% Ancre du composant BrowserImport
% ihm{end+1}.Lig                      = 1;
% ihm{end}.Col                        = 1;
% ihm{end}.Style                      = 'clc_browser';
% ihm{end}.Name                       = 'BrowserImport';
% ihm{end}.UserName                   = 'cla_carto';
% ihm{end}.UserCb                     = 'callback';
% ihm{end}.ControlButtonsAreShown     = 0;
% ihm{end}.AcceptAllFileFilterUsed    = 0;
% ihm{end}.filter                     = {'*.def', 'def'};
% ihm{end}.currentDir                 = 'C:\TEMP';
% ihm{end}.msgBrowser                 = 'MsgBrowserImport';
%
% -------------------
% Creation du panel
%    this.ihm.globale = cl_panel([]);
%    this.ihm.globale = set_tab_uiComposant(this.ihm.globale, ihm);
%    get_file(this)
%
% Authors : LUR, GLU
% ----------------------------------------------------------------------------

function newFiles = get_file(this)

hfic = get(this, 'HandleCpnBrowser');
newFiles = {};
if this.MultiSelectionEnabled
    files = get(hfic, 'SelectedFiles');
    
    % Transformation du nom java en chaine de caractere MatLab
    if ~isempty(files)
        N = numel(files);
        newFiles = cell(N,1);
        for i=1:N
            newFiles{i} = cell2str(cell(files(i).toString));
        end
    end
else
    files = get(hfic,'SelectedFile');
    if ~isempty(files)
        newFiles = cell2str(cell(files.toString));
    end
end

%% MAJ du nom du fichier dans l'instance (facultatif)

this.file = newFiles;
