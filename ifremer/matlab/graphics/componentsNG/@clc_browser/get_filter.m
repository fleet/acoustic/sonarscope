% Initialisation de la sauvegarde du handle du composant
%
% Syntax
%   filter = get_filter(this)
%
% Input Arguments
%   this        : instance de clc_browser
%
% Output Arguments
%   Str du filtre de fichiers.
%
% Authors : GLU
% -------------------------------------------------------------------------

function filter = get_filter(this)

filter = this.filter;