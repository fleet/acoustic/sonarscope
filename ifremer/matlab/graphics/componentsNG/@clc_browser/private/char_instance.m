% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_slider
%
% Output Arguments
%   str : une chaine de caracteres representative de l'instance
%
% Examples
%
% Authors : LUR
% VERSION  : $Id: char_instance.m,v 1 2007/07/26
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   26/07/2007 - LUR - creation
% ----------------------------------------------------------------------------

function str = char_instance(this)

    str = [];
    j=1;
    
    % ----------
    % Traitement
    str{end+1} = sprintf('HandleCpnBrowser          <-> %d', this.HandleCpnBrowser);
    str{end+1} = sprintf('file                      <-> %s', this.file);%  
    str{end+1} = sprintf('filtered extension        <-> %s', SelectionDansListe2str([],this.filter(1:2:end)));
    str{end+1} = sprintf('Corresponding file type   <-> %s', SelectionDansListe2str([],this.filter(2:2:end)));
    str{end+1} = sprintf('FileSelectionMode         <-> %d', this.FileSelectionMode);
    str{end+1} = sprintf('ControlButtonsAreShown    <-> %d', this.ControlButtonsAreShown);
    str{end+1} = sprintf('MultiSelectionEnabled     <-> %d', this.MultiSelectionEnabled);
    str{end+1} = sprintf('AcceptAllFileFilterUsed   <-> %d', this.AcceptAllFileFilterUsed);
    str{end+1} = sprintf('Default File Name         <-> %s', this.defFilename);
    str{end+1} = sprintf('currentDir                <-> %s', this.currentDir);
    str{end+1} = sprintf('msgBrowser                <-> %s', this.msgBrowser);
    
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);
    
    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
    % ---------------------------
    % Concatenation en une chaine
    
    str = char(strcat(str));
    
