% Construction de base de l'IHM
%
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_slider
%
% Output Arguments
%   this : instance de clc_slider
%
% Authors : LUR
%----------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation du panel du composant

this = creer_panel_global(this);

tp = [get(this, 'componentName'), ' ', 'clc_browser'];
hp = findobj(gcf, 'tag', tp);

%% Cr�ation des attributs du composant

tag = [get(this,'componentName'), '_clc_browser'];

h = uicomponent('style', 'JFileChooser', ...
    'Units',                   'normalized', ...
    'FileSelectionMode',        this.FileSelectionMode,...
    'ControlButtonsAreShown',   this.ControlButtonsAreShown, ...
    'AcceptAllFileFilterUsed',  this.AcceptAllFileFilterUsed, ...
    'MultiSelectionEnabled',    this.MultiSelectionEnabled, ...
    'Name',                     get(this,'componentName'), ...
    'Parent',                   hp, ...
    'position',                 [0 0 1 1], ...
    'Tag',                      tag);

% GLU : le 25/09/2015, for�age car l'assignation du Parent dans
% l'instruction pr�c�dente est sans effet en R2014b !
set(h, 'Parent', hp);

%% Gestion des callbacks

if ~isempty(this.msgBrowser)
    h.ActionPerformedCallback = built_cb(this, this.msgBrowser);
    h.PropertyChangeCallback  = built_cb(this, this.msgBrowser);
end

%% Cr�ation graphique du composant

uicomponent(h);
javaComposant = get(h, 'JavaComponent');
traduire_JavaCpn(javaComposant);

%% On sauvegarde le handle du composant

handle = get(h,'MatlabHGContainer');
this = set_HandleCpnBrowser(this,handle);

%% Positionnement du composant dans le panel

set(h, 'Position', [0 0 1 1]);

%% Traitement des couleurs des panels du composant

colFig = Gris;
colJava = java.awt.Color(colFig(1), colFig(2), colFig(3));
% Pause obligatoire puor �viter une erreur lors de la manip sur
% javaComposant (???)
pause(0.2); % TODO : voir si c'est toujours utile
set(javaComposant, 'Background', colJava);
setPropComposant(javaComposant, 'colBackground', colFig);
% Inhibition de la barre de boutons : probl�me sur plate-forme Windows US,
% ...

X = javaComposant.getComponents;
X(1).setVisible(1);

%% Affichage ou non du champ Filename et de son label.

set_labelParcourirVisible(this, 'visible',this.labelParcourirVisible);

jPanelListAndChoix = javaComposant.getComponents;
jPanelListAndChoix = jPanelListAndChoix(3);
jCompChoixFiltre   = jPanelListAndChoix.getComponents;
try
    jCompChoixFiltre  = jCompChoixFiltre(3);
catch % Pour utilisation  javax.swing.UIManager.setLookAndFeel('com.jgoodies.looks.plastic.Plastic3DLookAndFeel');
    jCompChoixFiltre  = jCompChoixFiltre(1);
end
X = jCompChoixFiltre.getComponents;

% Traitement du label
Xc = X(1).getComponents;
setVisible(Xc(1), this.FileNameVisible);
setVisible(Xc(2), this.FileNameVisible);
% Traitement du champ �ditable
try
    Xc = X(3).getComponents;
catch % Pour utilisation  javax.swing.UIManager.setLookAndFeel('com.jgoodies.looks.plastic.Plastic3DLookAndFeel');
    Xc = X(1).getComponents;
end
setVisible(Xc(1), this.FileNameVisible);
setVisible(Xc(2), this.FileNameVisible);

% https://forge.ifremer.fr/tracker/index.php?func=detail&aid=485&group_id=126&atid=535
% On ajoute la gestion du Retour Chariot dans la classe JFileChooser.
% R�sultat  :on se d�place bien dans l'arborescence. 
% Ce pb est d� � la version Java (1.7 qui appara�t � partir de la R2013b).

enter = javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER, 0);
map   = javaComposant.getInputMap(javax.swing.JFileChooser.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
map.put(enter, 'approveSelection');

%% Gestion des filtres

if ~isempty(this.filter)
    k1 = 1;
    for k2=1:(length(this.filter)/2)
        % myFiltreDef = utilFunction.filtreFileChooser(this.filter(k1), this.filter(k1+1));
        myFiltreDef = utilFunction.filtreFileChooser2(javaComposant, this.filter(k1), ...
            this.filter(k1+1), [], [], [], []);

        javaComposant.addChoosableFileFilter(myFiltreDef);
        javaComposant.setFileFilter(myFiltreDef);
        k1 = k1 + 2;
    end
end

%% R�pertoire et nom de fichier par d�faut

if ~isempty(this.currentDir)
    if ~isempty(this.defFilename)
        newFile = java.io.File(this.defFilename);
        f = java.io.File(newFile.getCanonicalPath);
        javaComposant.setSelectedFile(f);
    end
    X = java.io.File(this.currentDir);
    javaComposant.setCurrentDirectory(X);
end
