% Creation du panel global du composant
%
% Syntax
%   this = creer_panel_global(this)
%
% Input Arguments
%   this : instance de clc_slider
%
% Output Arguments
%   this : instance de clc_slider initialise
%
% Authors : LUR
% VERSION  : $Id: creer_panel_global.m,v 1 2007/07/26
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - LUR - creation
% -------------------------------------------------------------------------


function this = creer_panel_global(this)

    ihm      = [];
    
    ihm{end+1}.Lig      = 1;
    ihm{end}.Col        = 1;
    ihm{end}.Style      = 'panel';
    ihm{end}.Tag        = [get(this, 'componentName'), ' ', 'clc_browser'];
    ihm{end}.Parent     = get(this,'componentParent');
    ihm{end}.BorderType = 'line';
    ihm{end}.BorderWidth= 1;

    % -------------------
    % Creation du panel
    
    this.globalPanel = cl_panel([]);
    this.globalPanel = set_handle_panel(this.globalPanel, get_anchor(this));
%     this.globalPanel = set_inset_x(this.globalPanel, get_inset_x(this));
%     this.globalPanel = set_inset_y(this.globalPanel, get_inset_y(this));
    this.globalPanel = set_max_lig(this.globalPanel, 1);
    this.globalPanel = set_max_col(this.globalPanel, 1);
    this.globalPanel = set_tab_uiComposant(this.globalPanel, ihm);

   

