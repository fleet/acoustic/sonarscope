% Positionnement de la couleur de fond de composants Java.
% Rmq : appels r�cursifs !!!
%
% Output Arguments
%   Sans objet
%
% Examples
%
%     fig1 = figure;
%     h =  uicomponent('Parent', fig1,'style','JFileChooser','tag','myObj');
%     javaComposant = get(h, 'JavaComponent');
%     colFig = get(fig1, 'Color');
%     colJava = java.awt.Color(colFig(1), colFig(2), colFig(3));
%     javaComposant = get(h, 'JavaComponent');
%     set(javaComposant,'Background', colJava);
%     % setPropComposant(javaComposant, 'colBackground', colFig);
%
% See also uicomponent
% Authors : GLU
%-------------------------------------------------------------

function setPropComposant(hdlComponent, varargin)

[varargin, colBackground] = getPropertyValue(varargin, 'colBackground', Gris); % Gris = fonction
[varargin, visible]       = getPropertyValue(varargin, 'visible',       'on'); %#ok<ASGLU> % Gris = fonction

if contains(class(hdlComponent), 'javax')
    if strcmp(visible, 'on')
        hdlComponent.setVisible(1);
    else
        hdlComponent.setVisible(0);
    end
else
    hdlComponent.setVisible(strcmpi(visible, 'On'))
end

if ishandle(hdlComponent)
    colJava = java.awt.Color(colBackground(1), colBackground(2), colBackground(3));
    
    % Traitement de la barre d'outil sup�rieure du JFileChooser
    if isa(hdlComponent,'javahandle_withcallbacks.javax.swing.JToolBar') || isa(hdlComponent, 'javax.swing.JToolBar')
        set(hdlComponent, 'Background', colJava);
            
        % Traitement de la barre de menus Windows lat�rale du JFileChooser
    elseif isa(hdlComponent,'javahandle_withcallbacks.javax.swing.JPanel') || isa(hdlComponent, 'javax.swing.JPanel')
        set(hdlComponent, 'Background', colJava);
            
        % Traitement de boutons de la barre d'outil sup�rieure du JFileChooser
    elseif isa(hdlComponent,'javahandle_withcallbacks.javax.swing.JButton') || isa(hdlComponent, 'javax.swing.JButton')
        if  strcmp(get(hdlComponent, 'actionCommand'), 'Go Up')
            set(hdlComponent, 'Background', colJava);
                
        elseif  strcmp(get(hdlComponent, 'actionCommand'), 'New Folder')
            set(hdlComponent, 'Background', colJava);
        end
        
        % Traitement des ent�tes de colonnes dans la vue de d�tails des fichiers
    elseif isa(hdlComponent.getParent,'javax.swing.table.JTableHeader')
        if isa(hdlComponent, 'javax.swing.CellRendererPane')
            set(hdlComponent, 'Background', colJava);
        end
        
        % Traitement du Slider dans la vue de d�tails des fichiers
    elseif isa(hdlComponent, 'javahandle_withcallbacks.javax.swing.JSlider') || isa(hdlComponent, 'javax.swing.JSlider')
        set(hdlComponent, 'Background', colJava);
    end
    
    % Appel r�cursif pour le cas des sous panels (panel inf�rieur du
    % JFileChooser)
    X = hdlComponent.getComponents;
    for k=1:hdlComponent.getComponentCount
        setPropComposant(X(k), 'Background', colJava);
    end
end
