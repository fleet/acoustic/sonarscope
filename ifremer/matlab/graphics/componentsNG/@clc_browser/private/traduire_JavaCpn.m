% Lecture des fichiers s�lectionn�s dans le File Browser
%
% Syntax
%   traduire_JavaCpn(h)
%
% Input Arguments
%    h              : Handle du container du composant Java du Browser
%
% Output Arguments
%   Liste du(des) fichier(s) s�lectionn�(s)
%
% Examples
% figure;
% ihm={};
% % Ancre du composant BrowserImport
% ihm{end+1}.Lig                      = 1;
% ihm{end}.Col                        = 1;
% ihm{end}.Style                      = 'clc_browser';
% ihm{end}.Name                       = 'BrowserImport';
% ihm{end}.UserName                   = 'cla_carto';
% ihm{end}.UserCb                     = 'callback';
% ihm{end}.ControlButtonsAreShown     = 0;
% ihm{end}.AcceptAllFileFilterUsed    = 0;
% ihm{end}.filter                     = {'*.def', 'def'};
% ihm{end}.currentDir                 = 'C:\TEMP';
% ihm{end}.msgBrowser                 = 'MsgBrowserImport';
%
%    this.ihm.globale = cl_panel([]);
%    this.ihm.globale = set_tab_uiComposant(this.ihm.globale, ihm);
%
% Authors : LUR, GLU
% ----------------------------------------------------------------------------

function traduire_JavaCpn(javaCpn)

global IfrTbxLang %#ok<GVMIS>
if strcmp(IfrTbxLang, 'FR')
    javaLangLocale = java.util.Locale.FRENCH;
else
    javaLangLocale = java.util.Locale.ENGLISH;
end
java.util.Locale.setDefault(javaLangLocale);

% Application de la traduction sur la fen�tre principale
javaCpn.setDefaultLocale(javaLangLocale);
javaCpn.setDefaultLocale(javaLangLocale);
javaCpn.setLocale(javaLangLocale);
javaCpn.updateUI;
