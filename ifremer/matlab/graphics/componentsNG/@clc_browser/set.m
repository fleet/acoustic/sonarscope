% Modification des attributs et proprietes de l'instance
%
% Syntax
%   instance = set(this, varargin)
%
% Input Arguments
%   this : instance de clc_browser
%
% Name-Value Pair Arguments
%
% Output Arguments
%    this : instance de clc_slider initialisee
%
% Remarks :
%
% Examples
%    v = clc_browser;
%
% Authors : LUR
% -------------------------------------------------------------------------
function this = set(this, varargin)

% ---------------------
% Lecture des arguments

[varargin, HandleCpnBrowser] = getPropertyValue(varargin, 'HandleCpnBrowser', []);
[varargin, ControlButtonsAreShown] = getPropertyValue(varargin, 'ControlButtonsAreShown', []);
[varargin, FileSelectionMode] = getPropertyValue(varargin, 'FileSelectionMode', []);
[varargin, File] = getPropertyValue(varargin, 'File', []);
[varargin, filter] = getPropertyValue(varargin, 'filter', []);
[varargin, MultiSelectionEnabled] = getPropertyValue(varargin, 'MultiSelectionEnabled', []);
[varargin, AcceptAllFileFilterUsed] = getPropertyValue(varargin, 'AcceptAllFileFilterUsed', []);
[varargin, currentDir] = getPropertyValue(varargin, 'currentDir', []);
[varargin, defFilename] = getPropertyValue(varargin, 'defFilename', []);
[varargin, msgBrowser] = getPropertyValue(varargin, 'msgBrowser', []);
[varargin, labelParcourirVisible] = getPropertyValue(varargin, 'labelParcourirVisible', []);
[varargin, fileNameVisible] = getPropertyValue(varargin, 'FileNameVisible', []);

% ----------------------------
% Initialisation des attributs

if ~isempty(HandleCpnBrowser)
    this = set_HandleCpnBrowser(this, HandleCpnBrowser);
end

if ~isempty(ControlButtonsAreShown)
    this = set_ControlButtonsAreShown(this, ControlButtonsAreShown);
end

if ~isempty(FileSelectionMode)
    this = set_FileSelectionMode(this, FileSelectionMode);
end

if ~isempty(File)
    this = set_File(this, File);
end

if ~isempty(filter)
    this = set_filter(this, filter);
end

if ~isempty(MultiSelectionEnabled)
    this = set_MultiSelectionEnabled(this, MultiSelectionEnabled);
end

if ~isempty(AcceptAllFileFilterUsed)
    this = set_AcceptAllFileFilterUsed(this, AcceptAllFileFilterUsed);
end

if ~isempty(currentDir)
    this = set_currentDir(this, currentDir);
end

if ~isempty(defFilename)
    this = set_defFilename(this, defFilename);
end


if ~isempty(msgBrowser)
    this = set_msgBrowser(this, msgBrowser);
end

if ~isempty(labelParcourirVisible)
    this = set_labelParcourirVisibleVal(this, labelParcourirVisible);
end

if ~isempty(fileNameVisible)
    this = set_FileNameVisible(this, fileNameVisible);
end

% --------------------------------------------
% Traitement des message de la super classe
% Traitement specifique des composants agreges

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
