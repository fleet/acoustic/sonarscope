% Initialisation de la sauvegarde du handle du composant
%
% Syntax
%   this = set_defFilename(this, 'toto')
%
% Input Arguments
%   this        : instance de clc_slider
%
% Output Arguments
%   this : instance de clc_browser initialise
%
% Authors : LUR
% -------------------------------------------------------------------------

function this = set_defFilename(this, defFilename)

this.defFilename = defFilename;
