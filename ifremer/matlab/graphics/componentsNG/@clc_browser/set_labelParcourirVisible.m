function set_labelParcourirVisible(this, varargin)

[varargin, visible] = getPropertyValue(varargin, 'visible', 'on'); %#ok<ASGLU> % Gris = fonction

pppp = get(this.HandleCpnBrowser, 'JavaComponent');

%% recuperation de la zone de texte dans l'arborescence du cpn

panel1=pppp.Components(2);
panel2=get(panel1,'Components');
if strcmp(visible, 'on')
    valVisible = 1;
else
    valVisible = 0;
end

if ~isempty(panel2)
    panel2(1).setVisible(valVisible)
end
