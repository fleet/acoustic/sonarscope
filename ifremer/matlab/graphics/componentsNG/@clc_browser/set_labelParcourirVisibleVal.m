% Initialisation de la sauvegarde du handle du composant
%
% Syntax
%   this = set_HandleCpnSlider(this, HandleCpnSlider)
%
% Input Arguments
%   this        : instance de clc_slider
%   HandleCpnSlider  : HandleCpnSlider
%
% Output Arguments
%   this : instance de clc_slider initialise
%
% Authors : LUR
% -------------------------------------------------------------------------

function this = set_labelParcourirVisibleVal(this, labelParcourirVisible)

this.labelParcourirVisible = labelParcourirVisible;
