% Mise � jour du contenu de la fen�tre de fichiers.
%
% Syntax
%   update(this, ...)
%
% Input Arguments
%    this              : instance de clc_browser
%
% Output Arguments
%   Liste du(des) fichier(s) s�lectionn�(s)
%
% Examples
% figure;
% ihm={};
% % Ancre du composant BrowserImport
% ihm{end+1}.Lig                      = 1;
% ihm{end}.Col                        = 1;
% ihm{end}.Style                      = 'clc_browser';
% ihm{end}.Name                       = 'BrowserImport';
% ihm{end}.UserName                   = 'cla_carto';
% ihm{end}.UserCb                     = 'callback';
% ihm{end}.ControlButtonsAreShown     = 0;
% ihm{end}.AcceptAllFileFilterUsed    = 0;
% ihm{end}.filter                     = {'*.def', 'def'};
% ihm{end}.currentDir                 = 'C:\TEMP';
% ihm{end}.msgBrowser                 = 'MsgBrowserImport';
%
% % -------------------
% % Creation du panel
%    this.ihm.globale = cl_panel([]);
%    this.ihm.globale = set_tab_uiComposant(this.ihm.globale, ihm);
%    this.update;
%
% Authors : LUR, GLU
% ----------------------------------------------------------------------------

function update(this)

h = get(this, 'HandleCpnBrowser');
fc = get(h, 'JavaComponent');
pppp = fc.getComponents;
filePanel = pppp(3);
hdl = filePanel.getComponents;
filePane = hdl(2);
filePane.rescanCurrentDirectory;
pppp = get(h,'JavaComponent');
