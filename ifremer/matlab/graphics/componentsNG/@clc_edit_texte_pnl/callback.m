% Gestion des callbacks d'une IHM d'une instance
%
% Syntax
%   this = gerer_callback(this, message)
% 
% Input Arguments
%   this    : instance de clc_edit_texte
%   message : message d'identification de l'action
%
% Output Arguments
%   this : instance de clc_edit_texte initialisée
%
% Remarks : doit etre surchargée
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = callback(this, varargin)

if length(varargin) == 1
    switch varargin{1}
        case get(this, 'msgEdit')
            this = traiter_edit(this);
            
        case get(this, 'msgAction')
            this = traiter_action(this);
        otherwise
            my_warndlg('clc_edit_texte_pnl/callback : Invalid message', 0);
    end
else
    my_warndlg('clc_edit_texte_pnl/callback : Invalid arguments number', 0);
end

