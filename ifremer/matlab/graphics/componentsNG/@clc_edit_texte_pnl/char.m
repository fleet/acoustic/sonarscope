% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char(a)
%
% Input Arguments
%    a : instance ou tableau d'instances de clc_edit_texte
%
% Output Arguments
%    str : chaine de caracteres
%
% Examples
%    char(clc_edit_texte)
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char(this)

str = [];

[m, n] = size(this);
if (m*n) > 1
    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);
else
    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end

