% Constructeur du composant clc_edit_texte (Zone d'affichage de texte)
%
% Syntax
%   a = clc_edit_texte(...)
% 
% Name-Value Pair Arguments
%   texte             : contenu texte (chaine de caracteres)
%   tag               : tag du label d affichage du texte
%   tooltip           : info-bulle du label d affichage du texte
%   editable          : flag indiquant si le texte est editable
%   actionnable       : flag indiquant si le texte est actionnable (click)
%   typeButton        : flag indiquant le type de bouton
%                        - 0 : pushbutton
%                        - 1 : togglebutton
%   msgEdit           : msg de call back en cas d edition du texte
%   msgAction         : msg de call back en cas de click du texte
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    'on' | 'off'
%   componentEnable   : accessibilite du composant 'on' | 'off'
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%
% Output Arguments 
%   a          : instance de clc_edit_texte
%
% Remarks : 
%    
% Examples
%    v = clc_edit_texte('texte','ceci est un clc_edit_texte_pnl')
%    v = editobj(v);
%    
%    v = clc_edit_texte('texte','ceci est un clc_edit_texte_pnl clickable','actionnable',1);
%    v = editobj(v)
%
%    % EXEMPLE "type" cla_pames
%    ihm = [];
%    ihm{end+1}.Lig         = 3;
%    ihm{end}.Col           = 2;
%    ihm{end}.Style         = 'clc_edit_texte_pnl';
%    ihm{end}.texte         = 'clc_edit_texte_pnl 1';
%    ihm{end}.Name          = 'texte_pnl_1';
%    ihm{end}.Parent        = 'AncrePanel';
%    ihm{end}.Tag		     = 'Ancre1';
%    ihm{end}.border        = 1;
% 
%    ihm{end+1}.Lig         = 3;
%    ihm{end}.Col           = 3;
%    ihm{end}.Style         = 'clc_edit_texte_pnl';
%    ihm{end}.texte         = 'clc_edit_texte_pnl 2';              
%    ihm{end}.Name          = 'texte_pnl_2';
%    ihm{end}.Parent        = 'AncrePanel';
%	 ihm{end}.Tag		     = 'Ancre2';
%
%    ihm{end+1}.Lig         = 4;
%    ihm{end}.Col           = 2;
%    ihm{end}.Style         = 'clc_edit_texte_pnl';
%    ihm{end}.texte         = 'clc_edit_texte_pnl 3';              
%    ihm{end}.Name          = 'texte_pnl_3';
%    ihm{end}.Parent        = 'AncrePanel';
%    ihm{end}.Tag		     = 'Ancre3';
%    ihm{end}.Actionnable   = 1;
%
%    ihm{end+1}.Lig         = 4;
%    ihm{end}.Col           = 3;
%    ihm{end}.Style         = 'clc_edit_texte_pnl';
%    ihm{end}.texte         = 'clc_edit_texte_pnl 4';              
%    ihm{end}.Name          = 'texte_pnl_4';
%    ihm{end}.Parent        = 'AncrePanel';
%    ihm{end}.Tag		     = 'Ancre4';
%    ihm{end}.Actionnable   = 1;
%    ihm{end}.TypeButton    = 1;
%
%    ihm{end+1}.Lig         = 1:6;
%    ihm{end}.Col           = 1:4;
%    ihm{end}.Style         = 'panel';
%    ihm{end}.Tag		     = 'AncrePanel';
% 
%    ihm_global = cl_panel([]);
%    ihm_global = set_tab_uiComposant(ihm_global, ihm);
%
% See also clc_edit_texte cla_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_edit_texte_pnl(varargin)

%% D�finition de la structure et initialisation

% globalFrame : instance de cl_frame, frame globale du composant
% texte       : contenu texte
% tag         : tag du label d affichage du texte
% tooltip     : tooltip du label d affichage du texte
% editable    : flag indiquant si la valeur est editable
% actionnable : flag indiquant si le texte est actionnable
% typeButton  : flag indiquant le type de bouton
%               - 0 : pushbutton
%               - 1 : togglebutton, etat relache
%               - 2 : togglebutton, etat enfonce
% msgEdit     : msg de call back en cas d edition du texte
% msgAction   : msg de call back en cas de click du texte
% border      : 0 pas de bordure, 1 une bordure autour du texte 

this.globalPanel = [];
this.texte       = '';
this.value       = 0; % modif GLT
this.tag         = [];
this.tooltip     = '';
this.actionnable = 0;
this.editable    = 0;
this.choosable   = 0; %modif GLT
this.typeButton  = 0;
this.msgEdit     = [];%'clc_edit_texte edit';
this.msgAction   = [];%'clc_edit_texte action';
this.cbAction    = [];
this.border      = 0;
this.borderColor = [0.8 0.8 0.8];

%% Super-classe

% if (nargin == 1) && isempty(varargin{1})
    composant = cl_component([]);
% else
%     composant = cl_component;
% end

%% Cr�ation de l'objet

this = class(this, 'clc_edit_texte_pnl', composant);

%% Pre-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
