% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Examples
%   display(clc_edit_texte);
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
