% Interface-Homme-Machine (IHM) d'une instance
%
% Syntax
%   this = editobj(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% OUPUT PARAMETERS :
%   this : instance de clc_edit_texte
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

this.cl_component = editobj(this.cl_component);
this = creer_panel_global(this);
