% Retourne le flag indiquant si la valeur est editable
% 
% Syntax
%   editable = get_editable(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   editable : flag indiquant si la valeur est editable
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_editable.m,v 1.4 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function editable = get_editable(this)

% Maintenance Auto : try
    editable = this.editable;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_editable', lasterr);
% Maintenance Auto : end
