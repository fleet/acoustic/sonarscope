% Retourne le nombre de colonnes occupees par l unite
%
% Syntax
%   nbColUnite = get_nb_col_unite(this)
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   nbColUnite : nombre de colonnes occupees par l unite
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function  globalPanel =get_globalPanel(this)
globalPanel = this.globalPanel;
