% Retourne le handle de l afficheur de la valeur
%
% Syntax
%   handle = get_handle(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   handle   : handle de l afficheur de la valeur, [] si non represente
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function handle = get_handle(this)
handle = get(this.globalPanel, 'ListeUiComposants');
