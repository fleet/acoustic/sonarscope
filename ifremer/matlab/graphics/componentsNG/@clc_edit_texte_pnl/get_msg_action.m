% Retourne le message envoye lors du click sur le texte
% 
% Syntax
%   msgAction = get_msg_action(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   msgAction : message envoye lors du click sur le texte
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_msg_action.m,v 1.5 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgAction = get_msg_action(this)

% Maintenance Auto : try
    msgAction = this.msgAction;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_msg_action', lasterr);
% Maintenance Auto : end
