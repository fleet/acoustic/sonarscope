% Retourne le message envoye lors de l edition de la valeur
% 
% Syntax
%   msgEdit = get_msg_edit(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   msgEdit : message envoye lors de l edition de la valeur
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: get_msg_edit.m,v 1.5 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function msgEdit = get_msg_edit(this)

% Maintenance Auto : try
    msgEdit = this.msgEdit;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'get_msg_edit', lasterr);
% Maintenance Auto : end
