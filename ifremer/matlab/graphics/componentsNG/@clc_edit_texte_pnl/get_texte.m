% Retourne la valeur affichee dans la zone de texte
% 
% Syntax
%   texte = get_texte(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   texte    : texte
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function texte = get_texte(this)

if isempty(this.globalPanel)
    texte = this.texte;
else
    h = get(this.globalPanel, 'ListeUicontrols');
    texte = get(h,'string');
    this.texte = texte;
end