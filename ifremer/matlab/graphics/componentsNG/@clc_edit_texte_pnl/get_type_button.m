% Retourne le tag
%
% Syntax
%   typeButton = get_type_button(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
% typeButton  : flag indiquant le type de bouton
%               - 0 : pushbutton
%               - 1 : togglebutton, etat relache
%               - 2 : togglebutton, etat enfonce
% See also clc_edit_texte Authors
% Authors : DCF
% -------------------------------------------------------------------------

function typeButton = get_type_button(this)
typeButton = this.typeButton;
