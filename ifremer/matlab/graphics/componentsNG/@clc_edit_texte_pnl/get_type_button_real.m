% Retourne le tag
%
% Syntax
%   typeButton = get_type_button(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
% typeButton  : flag indiquant le type de bouton
%               - 0 : pushbutton
%               - 1 : togglebutton, etat relache
%               - 2 : togglebutton, etat enfonce
% See also clc_edit_texte Authors
% Authors : DCF
% -------------------------------------------------------------------------

function typeButton = get_type_button_real(this)

huic = get(this.globalPanel,'ListeUicontrols');
s = get(huic,'Style');

if strcmp(s,'pushbutton')
    typeButton=0;
elseif strcmp(s,'togglebutton')
    if get(huic,'value')==1
        typeButton=2;
    else
        typeButton=1;
    end
end

this.typeButton = typeButton;