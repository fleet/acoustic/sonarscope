% Retourne la valeur de l'uicontrol (utile pour les popupmenu et *button)
% 
% Syntax
%   value = get_value(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   value    : entier
%
% See also clc_edit_texte Authors
% Authors : GLT (ATL)
% ----------------------------------------------------------------------------

function value = get_value(this)

if isempty(this.globalPanel)
    value = this.value;
else
    h = get(this.globalPanel, 'ListeUicontrols');
    value = get(h, 'value');
    this.value = value;
end