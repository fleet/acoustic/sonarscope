% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

%% Traitement

if ischar(this.texte)
    this.texte = {this.texte};
end
str{end+1} = sprintf('texte       <-> %s', SelectionDansListe2str(this.value, this.texte, 'Num'));
str{end+1} = sprintf('value       <-> %s', num2str(this.value));
str{end+1} = sprintf('tag         <-> %s', mat2str(this.tag));
str{end+1} = sprintf('tooltip     <-> %s', mat2str(this.tooltip));
str{end+1} = sprintf('editable    <-> %s', mat2str(this.editable));
str{end+1} = sprintf('actionnable <-> %s', mat2str(this.actionnable));
str{end+1} = sprintf('choosable   <-> %s', mat2str(this.choosable));
str{end+1} = sprintf('typeButton  <-> %s', mat2str(this.typeButton));
str{end+1} = sprintf('msgEdit     <-> %s', mat2str(this.msgEdit));
str{end+1} = sprintf('msgAction   <-> %s', mat2str(this.msgAction));

str{end+1} = sprintf('--- heritage cl_component ---');
str{end+1} = char(this.cl_component);

%% Concatenation en une chaine

str = cell2str(str);

