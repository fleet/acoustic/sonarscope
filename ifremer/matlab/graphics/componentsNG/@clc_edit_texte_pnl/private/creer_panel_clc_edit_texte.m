function this = creer_panel_clc_edit_texte(this)

GCF = gcf;

ihm    = [];
Parent = [get(this, 'componentName'), ' ', 'clc_edit_texte_pnl'];

%% Positionnement du texte A l'ESSAI LUR
% centrage du texte dans le cas d'un edit GLT

tp = get(this, 'componentParent');

% hp = findobj(gcf, 'tag', tp);
ListeHandlePanel = getappdata(GCF, 'ListeHandlePanel');
if isfield(ListeHandlePanel, clean(tp)) % Ajout� par JMA le 20/09/2015 : A l'essai
    hp = ListeHandlePanel.(clean(tp));
else
    hp = findobj(GCF, 'tag', tp);
end

p  = get(hp, 'Position');
set(hp, 'units', 'pixels');
w  = get(hp, 'Position');
set(hp, 'units', 'normalized');
%w(4)
if (this.editable == 0) && (this.actionnable ~= 1) && (this.choosable ~= 1) 
    if p(4)==1 && w(4)>20
        ihm{1}.Lig = 4:8;
        ihm{1}.Col = 1;
    elseif p(4)<0.5 || w(4)<=20
        ihm{1}.Lig = 2:4;
        ihm{1}.Col = 1;
    else
        ihm{1}.Lig = 3:5;
        ihm{1}.Col = 1;
    end
else
    ihm{1}.Lig = 1;
    ihm{1}.Col = 1;
end

%% D�finition g�n�rale

% ihm{1}.Lig           = 1;
% ihm{1}.Col           = 1;
ihm{1}.TooltipString = this.tooltip;
ihm{1}.Tag           = [get(this,'componentParent') '/clc_edit_texte_pnl'];
ihm{1}.Parent        = Parent;
ihm{1}.String        = this.texte;

%% Choix selon la valeur du param�tre �ditable

if this.editable
    ihm{1}.Style            = 'edit';
    ihm{1}.BackgroundColor  = GrisClair;
    ihm{1}.Callback         = built_cb(this, this.msgEdit);
    
elseif this.actionnable
    if this.typeButton == 0
        ihm{1}.Style = 'pushbutton';
    elseif this.typeButton == 1
        ihm{1}.Style = 'togglebutton';
        ihm{1}.Value = 0;
    else
        ihm{1}.Style = 'togglebutton';
        ihm{1}.Value = 1;
    end
    if ~isempty(this.msgAction) && isempty(this.cbAction)
        ihm{1}.Callback = built_cb(this, this.msgAction);
    elseif ~isempty(this.cbAction)&& isempty(this.msgAction)
        ihm{1}.Callback = this.cbAction;
    elseif ~isempty(this.cbAction)&& ~isempty(this.msgAction)
        my_warndlg('vous ne pouvez pas mettre un msgAction et une cbAction', 0);
    end
    
elseif this.choosable %modif GLT
    ihm{1}.Style           = 'popupmenu';
    ihm{1}.BackgroundColor = GrisClair;
    ihm{1}.Callback        = built_cb(this, this.msgEdit);
    
else
    ihm{1}.Style           = 'text';
    ihm{1}.BackgroundColor = Gris;
    ihm{1}.Callback        = [];
end

this.globalPanel = cl_panel('nomPanel', Parent);
this.globalPanel = set_tab_uiComposant(this.globalPanel, ihm);


function strOut = clean(strIn)
strOut = strrep(strIn, ' ', '_');
