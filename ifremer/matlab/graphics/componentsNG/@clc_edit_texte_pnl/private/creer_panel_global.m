% Creation des frames principales de l'IHM d'une instance
%
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% -------------------------------------------------------------------------

function this = creer_panel_global(this)

ihm = [];

ihm{end+1}.Lig                  = 1;
ihm{end}.Col                    = 1;
ihm{end}.Style                  = 'panel';
ihm{end}.Parent                 = get(this,'componentParent');
ihm{end}.Tag                    = [get(this, 'componentName'), ' ', 'clc_edit_texte_pnl'];
ihm{end}.BorderType             = 'line';
ihm{end}.BorderWidth            = 1;
if this.border
    ihm{end}.HighlightColor     = this.borderColor;
    ihm{end}.ShadowColor        = this.borderColor;
else
    ihm{end}.HighlightColor     = Gris;
    ihm{end}.ShadowColor        = Gris;
end

%% Cr�ation du panel

this.globalPanel = cl_panel([]);
this.globalPanel = set_handle_panel(this.globalPanel, get_anchor(this));
this.globalPanel = set_max_lig(this.globalPanel, 1);
this.globalPanel = set_max_col(this.globalPanel, 1);
this.globalPanel = set_tab_uiComposant(this.globalPanel, ihm);

this = creer_panel_clc_edit_texte(this);

