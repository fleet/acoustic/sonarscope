% Traitement apres un click sur le texte
%
% Syntax
%   this = traiter_action(this) 
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: traiter_action.m,v 1.5 2003/04/04 09:15:24 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = traiter_action(this)

    % --------------------------------------
    % Mise a jour de l etat du toggle button
    
    if this.typeButton == 1
        this.typeButton = 2;
    elseif this.typeButton == 2
        this.typeButton = 1;
    end
    
