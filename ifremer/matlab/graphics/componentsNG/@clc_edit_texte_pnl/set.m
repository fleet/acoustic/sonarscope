% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Name-Value Pair Arguments
%   cf. clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% Examples
%    v = clc_edit_texte;
%    set(v, 'texte', 'un texte possible', 'tooltip', 'Texte')
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

%% Lecture des arguments

[varargin, texte] = getPropertyValue(varargin, 'texte', []);
[varargin, value] = getPropertyValue(varargin, 'value', []);
[varargin, tag] = getPropertyValue(varargin, 'tag', []);
[varargin, tooltip] = getPropertyValue(varargin, 'tooltip', []);
[varargin, editable] = getPropertyValue(varargin, 'editable', []);
[varargin, actionnable] = getPropertyValue(varargin, 'actionnable', []);
[varargin, choosable] = getPropertyValue(varargin, 'choosable', []);
[varargin, typeButton] = getPropertyValue(varargin, 'typeButton', []);
[varargin, msgEdit] = getPropertyValue(varargin, 'msgEdit', []);
[varargin, msgAction] = getPropertyValue(varargin, 'msgAction', []);
[varargin, visible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, enable] = getPropertyValue(varargin, 'componentEnable', []);
[varargin, border] = getPropertyValue(varargin, 'border', []);
[varargin, borderColor] = getPropertyValue(varargin, 'borderColor', []);
[varargin, cbAction] = getPropertyValue(varargin, 'cbAction', []);

%% Initialisation des attributs

if ~isempty(texte)
    this = set_texte(this, texte);
end
if ~isempty(value)
    this = set_value(this, value);
end
if ~isempty(tag)
    this = set_tag(this, tag);
end
if ~isempty(tooltip)
    this = set_tooltip(this, tooltip);
end
if ~isempty(editable)
    this = set_editable(this, editable);
end
if ~isempty(actionnable)
    this = set_actionnable(this, actionnable);
end
if ~isempty(choosable)
    this = set_choosable(this, choosable);
end
if ~isempty(typeButton)
    this = set_type_button(this, typeButton);
end
if ~isempty(msgEdit)
    this = set_msg_edit(this, msgEdit);
end
if ~isempty(msgAction)
    this = set_msg_action(this, msgAction);
end
if ~isempty(visible)
    this = set_visible(this, visible);
end
if ~isempty(enable)
    this = set_enable(this, enable);
end

if ~isempty(border)
    this = set_border(this, border);
end
if ~isempty(borderColor)
    this = set_borderColor(this, borderColor);
end
if ~isempty(cbAction)
    this = set_cbAction(this, cbAction);
end

%% Traitement des messages de la super classe

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
