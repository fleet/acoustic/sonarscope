% Initialisation du flag indiquant la possibilite de cliquer sur le texte
%
% Syntax
%   this = set_actionnable(this, actionnable)
%
% Input Arguments
%   this        : instance de clc_edit_texte
%   actionnable : flag indiquant la possibilite de cliquer sur le texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_actionnable.m,v 1.5 2003/04/07 14:23:50 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_actionnable(this, actionnable)

isOk = 1;

% ------------------------------
% Test validite des arguments :
% - doit etre  0 ou 1

if ~isnumeric(actionnable)
    my_warndlg('clc_edit_texte/set_actionnable : Invalid format', 0);
    isOk = 0;
elseif (actionnable ~= 0) && (actionnable ~= 1)
    my_warndlg('clc_edit_texte/set_actionnable : Invalid flag value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.actionnable = actionnable;
    if this.actionnable
        if this.editable
            this.editable = 0;
        elseif this.choosable %modif GLT
            this.choosable = 0;
        end
    end
else
    this.actionnable = 0;
end

%% Si affichage en cours, mise a jour de l affichage

%     if is_edit(this)

%h = get(this, 'handle');
h = get(this.globalPanel,'ListeUicontrols');
if this.actionnable
    if this.typeButton == 0
        set(h,  'Style', 'pushbutton', ...
            'callback' , built_cb(this, this.msgAction));
    elseif this.typeButton == 1
        set(h,  'Style', 'togglebutton', ...
            'Value', 0, ...
            'callback', built_cb(this, this.msgAction));
    else
        set(h,  'Style', 'togglebutton', ...
            'Value', 1, ...
            'callback', built_cb(this, this.msgAction));
    end
else
    set(h,  'Style', 'text', 'callback', []);
end

