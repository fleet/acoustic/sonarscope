% Initialisation de l'accessibilite du composant
% 
% Syntax
%   this = set_enable(this, enable)
%
% Input Arguments
%   this   : instance de clc_edit_texte
%   enable : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.7 2003/04/07 14:28:11 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    21/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_border(this, border)


        this.border = border;

