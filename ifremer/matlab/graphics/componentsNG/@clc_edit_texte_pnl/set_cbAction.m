% Initialisation de l'accessibilite du composant
%
% Syntax
%   this = set_enable(this, enable)
%
% Input Arguments
%   this   : instance de clc_edit_texte
%   enable : accessibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : LUR (ATL)
% -------------------------------------------------------------------------

function this = set_cbAction(this, cbAction)

this.cbAction = cbAction;
if this.actionnable ==1
    handle = get(this.globalPanel,'ListeUicontrols');
    set(handle, 'Callback', cbAction);
end
