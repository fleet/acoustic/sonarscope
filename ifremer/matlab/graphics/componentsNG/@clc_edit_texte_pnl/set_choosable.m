% Initialisation du flag indiquant la possibilite de choisir la valeur
% 
% Syntax
%   this = set_choosable(this, choosable)
%
% Input Arguments
%   this     : instance de clc_edit_texte
%   choosable : flag indiquant la possibilite de choisir la valeur
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_choosable(this, choosable)

isOk = 1;

%% Test validite des arguments :
% - doit etre  0 ou 1

if ~isnumeric(choosable)
    my_warndlg('clc_edit_texte_pnl/set_choosable : Invalid format', 0);
    isOk = 0;
elseif (choosable ~= 0) && (choosable ~= 1)
    my_warndlg('clc_edit_texte_pnl/set_choosable : Invalid flag value', 0);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.choosable = choosable;
    if this.choosable
        if this.actionnable
            this.actionnable = 0;
        elseif this.editable
            this.editable = 0;
        end
    end
else
    this.choosable = 0;
end

%% Si affichage en cours, mise � jour de l'affichage

if this.choosable
    h = get(this.globalPanel, 'ListeUicontrols');
    % set(h,'enable','on');
    set(h,  'Style', 'popupmenu', ...
        'BackgroundColor', GrisClair , ...
        'callback', built_cb(this, this.msgEdit),...
        'Position', [0 0 1 1]);
    oldstr = get(h,'userdata');
    % r�cup�ration de l'ancien tableau de celle dans le user data
    if ~isempty(oldstr)
        set(h,'string',oldstr);
    end    
 
else
    h = get(this.globalPanel, 'ListeUicontrols');
    % set(h,'enable','off');
    oldstr = get(h,'userdata');
    curval = get(h,'value');
    curstr = get(h,'string');
    if ~isempty(oldstr)
        curstr = oldstr;
    end  
    % affichage de la string courante, et sauvedarde du tableau de cell
    % dans le user data
    set(h,  'Style', 'text', ...
        'String', curstr(curval),...
        'userdata',curstr,...
        'BackgroundColor', Gris, ...
        'callback', [],...
        'Position', [0 0 1 0.625]); % Lig = 3:5;
end
