% Initialisation du flag indiquant la possibilite d editer la valeur
% 
% Syntax
%   this = set_editable(this, editable)
%
% Input Arguments
%   this     : instance de clc_edit_texte
%   editable : flag indiquant la possibilite d editer la valeur
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_editable(this, editable)

isOk = 1;

%% Test validite des arguments :
% - doit etre  0 ou 1

if ~isnumeric(editable)
    my_warndlg('clc_edit_texte_pnl/set_editable : Invalid format', 0);
    isOk = 0;
elseif (editable ~= 0) && (editable ~= 1)
    my_warndlg('clc_edit_texte_pnl/set_editable : Invalid flag value', 0);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.editable = editable;
    if this.editable
        if this.actionnable
            this.actionnable = 0;
        elseif this.choosable %modif GLT
            this.choosable = 0;
        end
    end
else
    this.editable = 0;
end

%% Si affichage en cours, mise � jour de l'affichage

if this.editable
    h = get(this.globalPanel, 'ListeUicontrols');
    set(h,  'Style', 'edit', ...
        'BackgroundColor', GrisClair , ...
        'callback', built_cb(this, this.msgEdit),...
        'Position', [0 0 1 1]); % A L'Essai LUR
else
    h = get(this.globalPanel, 'ListeUicontrols');
    set(h,  'Style', 'text', ...
        'BackgroundColor', Gris, ...
        'callback', [],...
        'Position', [0 0 1 0.625]); % Lig = 3:5;
end

