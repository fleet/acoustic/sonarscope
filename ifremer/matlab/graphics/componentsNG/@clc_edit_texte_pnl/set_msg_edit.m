% Initialisation du message envoye en cas d edition de la valeur
%
% Syntax
%   this = set_msg_edit(this, msgEdit)
%
% Input Arguments
%   this    : instance de clc_edit_texte
%   msgEdit : message envoye en cas d edition de la valeur
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : LUR (ATL)
% ----------------------------------------------------------------------------

function this = set_msg_edit(this, msgEdit)
this.msgEdit = msgEdit;