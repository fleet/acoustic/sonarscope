% Initialisation de la valeur de l'instance
% 
% Syntax
%   this = set_texte(this, texte)
%
% Input Arguments
%   this  : instance de clc_edit_texte
%   texte : texte
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_texte.m,v 1.5 2003/06/18 12:30:10 tmakanda Exp $
% ----------------------------------------------------------------------------

function this = set_texte(this, texte)

isOk = 1;

%% Test validite des arguments :
% - doit etre une chaine de caracteres ou un tableau de cellule de
% caract�re
% - doit etre dans l intervalle des valeurs admissibles

if ~ischar(texte) && ~iscellstr(texte)
    my_warndlg('clc_edit_texte_pnl/set_texte : Invalid format', 0);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.texte = texte;
else
    this.texte = '';
end

%% Si affichage en cours, mise a jour de l affichage

%     if is_edit(this)
%         h = get(this, 'handle');

if ~isempty(this.globalPanel)
    h = get(this.globalPanel, 'ListeUicontrols');
    set(h, 'String', this.texte);
end

%         set(h, 'Enable', 'on');
