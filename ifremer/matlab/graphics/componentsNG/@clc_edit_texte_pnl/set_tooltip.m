% Initialisation de l'info-bulle
% 
% Syntax
%   this = set_tooltip(this, tooltip)
%
% Input Arguments
%   this : instance de clc_edit_texte
%   tooltip  : info-bulle
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: set_tooltip.m,v 1.6 2003/04/07 14:23:50 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_tooltip(this, tooltip)

isOk = 1;

% ------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(tooltip)
    my_warndlg('clc_edit_texte/set_tooltip : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.tooltip = tooltip;
else
    this.tooltip = [];
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l'affichage

if is_edit(this)
    h = get(this, 'handle');
    set(h, 'TooltipString', this.tooltip);
end
