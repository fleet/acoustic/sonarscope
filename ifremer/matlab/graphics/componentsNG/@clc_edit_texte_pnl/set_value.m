% Initialisation de la valeur de l'instance
% 
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%   this  : instance de clc_edit_texte
%   value : valeur enti�re
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_value(this, value)

isOk = 1;

%% Test validite des arguments :
% - doit etre une chaine de caracteres ou un tableau de cellule de
% caract�re
% - doit etre dans l intervalle des valeurs admissibles

if ~isnumeric(value)
    my_warndlg('clc_edit_texte_pnl/set_value : Invalid format', 0);
    isOk = 0;
end

%% Si ok, initialisation

if isOk
    this.value = value;
else
    this.value = 0;
end

%% Si affichage en cours, mise a jour de l affichage

h = get(this.globalPanel, 'ListeUicontrols');
set(h, 'value', this.value);
