% Initialisation de la visibilite du composant
%
% Syntax
%   this = set_visible(this, visible)
%
% Input Arguments
%   this    : instance de clc_edit_texte
%   visible : visibilite du composant 'on' | 'off'
%
% Output Arguments
%   this : instance de clc_edit_texte initialisee
%
% See also clc_edit_texte Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_visible(this, visible)

handlePanel = get(this.globalPanel, 'HandlePanel');
set(handlePanel, 'visible', visible)
