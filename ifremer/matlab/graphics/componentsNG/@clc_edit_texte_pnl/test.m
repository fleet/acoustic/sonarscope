% Test graphique du composant
%
% Syntax
%   this = test(this, varargin)
% 
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte
% 
% Examples
%    a = test(clc_edit_texte_pnl)
%
% See also clc_edit_texte_pnl Authors
% Authors : DCF
% VERSION  : $Id: test.m,v 1.5 2003/04/07 14:23:50 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = test(this)

% Maintenance Auto : try
    
    % --------------------------------------------------------------------------
    % Creation d une figure ainsi que de l ancre du composant
     figure('visible','on')
%    figure('Visible', 'off');
%     h(1) = uicontrol(                         ...
%         'Visible'  , 'on'                     , ...
%         'Tag'      , 'ancre_clc_edit_texte 1' , ...
%         'Position' , [20 20 100 30]           );
%     h(2) = uicontrol(                         ...
%         'Visible'  , 'on'                     , ...
%         'Tag'      , 'ancre_clc_edit_texte 2' , ...
%         'Position' , [20 60 100 30]           );
%     h(3) = uicontrol(                         ...
%         'Visible'  , 'on'                     , ...
%         'Tag'      , 'ancre_clc_edit_texte 3' , ...
%         'Position' , [20 100 100 30]          );
%     h(4) = uicontrol(                         ...
%         'Visible'  , 'on'                     , ...
%         'Tag'      , 'ancre_clc_edit_texte 4' , ...
%         'Position' , [20 140 100 30]          );
    
    % --------------------------------------------------------------------------
    % Creation et initialisation du composant
    
    i(1)    = clc_edit_texte_pnl([]);
%     i(2)    = clc_edit_texte_pnl([]);
%     i(3)    = clc_edit_texte_pnl([]);
%     i(4)    = clc_edit_texte_pnl([]);
    this = i;
    this(1) = set_texte(this(1),        'Mon texte 1 a moi!');
    this(1) = set_tag(this(1),          'Coucou');
    this(1) = set_tooltip(this(1),      'Coucou');
    this(1) = set_editable(this(1),     0);
    this(1) = set_actionnable(this(1),  0);
    this(1) = set_name(this(1),         'ancre_clc_edit_texte 1');
    this(1) = set_user_name(this(1),    'clc_edit_texte_pnl');
    this(1) = set_user_cb(this(1),      'gerer_callback');
    
%     this(2) = set_texte(this(2),        'Mon texte 2 a moi!');
%     this(2) = set_tag(this(2),          'Coucou');
%     this(2) = set_tooltip(this(2),      'Coucou');
%     this(2) = set_editable(this(2),     1);
%     this(2) = set_actionnable(this(2),  0);
%     this(2) = set_name(this(2),         'ancre_clc_edit_texte 2');
%     this(2) = set_user_name(this(2),    'clc_edit_texte_pnl');
%     this(2) = set_user_cb(this(2),      'gerer_callback');
%     
%     this(3) = set_texte(this(3),        'Mon texte 3 a moi!');
%     this(3) = set_tag(this(3),          'Coucou');
%     this(3) = set_tooltip(this(3),      'Coucou');
%     this(3) = set_editable(this(3),     0);
%     this(3) = set_choosable(this(3),  1);
%     this(3) = set_name(this(3),         'ancre_clc_edit_texte 3');
%     this(3) = set_user_name(this(3),    'clc_edit_texte_pnl');
%     this(3) = set_user_cb(this(3),      'gerer_callback');
%     
%     this(4) = set_texte(this(4),        'Mon texte 4 a moi!');
%     this(4) = set_tag(this(4),          'Coucou');
%     this(4) = set_tooltip(this(4),      'Coucou');
%     this(4) = set_editable(this(4),     0);
%     this(4) = set_actionnable(this(4),  1);
%     this(4) = set_type_button(this(4),  1);
%     this(4) = set_name(this(4),         'ancre_clc_edit_texte 4');
%     this(4) = set_user_name(this(4),    'clc_edit_texte_pnl');
%     this(4) = set_user_cb(this(4),      'gerer_callback');
    
    % -------------------------------
    % Creation graphique du composant
    
    this(1) = editobj(this(1));
%     this(2) = editobj(this(2));
%     this(3) = editobj(this(3));
%     this(4) = editobj(this(4));
    
    % ------------------------
    % Rend le resultat visible
    
    %set(gcf, 'Visible', 'on');
    %set(h,   'Visible', 'off');
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte_pnl', 'test', lasterr);
% Maintenance Auto : end
