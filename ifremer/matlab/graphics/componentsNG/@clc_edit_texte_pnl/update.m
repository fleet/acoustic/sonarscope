% Mise a jour graphique du composant
%
% Syntax
%   this = update(this)
% 
% Input Arguments
%   this : instance de clc_edit_texte
%
% Output Arguments
%   this : instance de clc_edit_texte
%
% Remarks : doit etre surchargee
%
% See also clc_edit_texte Authors
% Authors : DCF
% VERSION  : $Id: update.m,v 1.4 2003/04/04 09:11:16 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = update(this)

% Maintenance Auto : try
    
    % -----------------------------------
    % Update graphique de la super classe
    
    this.cl_component = update(this.cl_component);
    
    % -----------------------------------------------------
    % Update graphique des elements graphiques du composant
    
    if ~isempty(this.globalFrame)
        this.globalFrame = update(this.globalFrame);
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_edit_texte', 'update', lasterr);
% Maintenance Auto : end
