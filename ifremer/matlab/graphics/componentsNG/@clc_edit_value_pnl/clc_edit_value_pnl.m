% Constructeur du composant clc_edit_value_pnl : zone d'edition de valeurs
%
% Syntax
%    a = clc_edit_value_pnl(varargin)
% 
% Input Arguments 
%    varargin : propertyName / propertyValue
%
% Name-Value Pair Arguments
%    value             : valeur reelle
%    format            : format d affichage de la valeur
%    minValue          : valeur minimale admissible
%    maxValue          : valeur maximale admissible
%    tag               : tag du label d affichage de la valeur
%    tooltip           : info-bulle du label d affichage de la valeur
%    editable          : flag indiquant si la valeur est editable
%    msgEdit           : msg de call back en cas d edition de la valeur
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant    'on' | 'off'
%    componentEnable   : accessibilite du composant 'on' | 'off'
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments 
%    a : instance de clc_edit_value_pnl
%
% Remarks :
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%       
%   ihm=[]; %   
%   ihm{end+1}.Lig         = 3;
%   ihm{end}.Col           = 2;
%   ihm{end}.Style         = 'clc_edit_value_pnl';
%   ihm{end}.Tooltip       = 'ceci est un clc_edit_value_pnl';
%   ihm{end}.Name		   = 'Names_clc_edit_value_pnl';
%   ihm{end}.Parent		   = 'AncrePanel';
%   ihm{end}.Tag		   = 'Tag_clc_edit_value_pnl';
%   ihm{end}.editable      = 1;
%   ihm{end}.value         = 50;
%
%   ihm{end+1}.Lig         = 1:5;
%   ihm{end}.Col           = 1:3;
%   ihm{end}.Style         = 'panel';
%   ihm{end}.Tag		   = 'AncrePanel';
% 
%   ihm_global = cl_panel();
%   ihm_global = set_tab_uiComposant(ihm_global, ihm);
%      

%
% See also clc_edit_value cla_edit_value_pnl Authors
% Authors : LUR (ATL)
% ----------------------------------------------------------------------------

function this = clc_edit_value_pnl(varargin)

%% D�finition de la structure et initialisation

this.value      = [];
this.format     = '%f';
this.minValue   = [];
this.maxValue   = [];
this.tag        ='defaultTag_clc_editValue';

%% Super-classe

if (nargin == 1) && isempty(varargin {1})
    clc_edit_texte_pnl([]);
else
    clc_edit_texte_pnl;
end

%% Cr�ation de l'objet

this = class(this, 'clc_edit_value_pnl', clc_edit_texte_pnl);

%% Initialisation de la super classe
% Pre-nitialisation des champs

if (nargin == 1) && isempty(varargin {1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this.clc_edit_texte_pnl = set(this.clc_edit_texte_pnl, ...
    'msgEdit',      'clc_edit_value edit', ...
    'actionnable',  0, ...
    'msgAction',    []);

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
