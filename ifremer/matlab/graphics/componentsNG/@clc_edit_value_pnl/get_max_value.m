% Retourne la valeur maximale admissible
% 
% Syntax
%   maxValue = get_max_value(this)
%
% Input Arguments
%   this : instance de clc_edit_value
%
% Output Arguments
%   maxValue : valeur maximale admissible
%
% See also help clc_edit_value Authors
% Authors : DCF
% VERSION  : $Id: get_max_value.m,v 1.3 2003/04/04 14:22:03 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function maxValue = get_max_value(this)

% Maintenance Auto : try
    maxValue = this.maxValue;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_edit_value', 'get_max_value', lasterr);
% Maintenance Auto : end
