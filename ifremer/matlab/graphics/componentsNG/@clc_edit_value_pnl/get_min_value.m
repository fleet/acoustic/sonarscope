% Retourne la valeur minimale admissible
% 
% Syntax
%   minValue = get_min_value(this)
%
% Input Arguments
%   this : instance de clc_edit_value
%
% Output Arguments
%   minValue : valeur minimale admissible
%
% See also help clc_edit_value Authors
% Authors : DCF
% VERSION  : $Id: get_min_value.m,v 1.4 2003/04/28 16:11:23 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function minValue = get_min_value(this)

% Maintenance Auto : try
    minValue = this.minValue;
% Maintenance Auto : catch
% Maintenance Auto :     err ('clc_edit_value', 'get_min_value', lasterr);
% Maintenance Auto : end
