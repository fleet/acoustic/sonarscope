% Traitement apres modification de la valeur
%
% Syntax
%   this = traiter_edit(this) 
%
% Input Arguments
%   this : instance de clc_edit_value
%
% Output Arguments
%   this : instance de clc_edit_value initialisee
%
% Examples
%
% See also clc_edit_value Authors
% Authors : DCF
% VERSION  : $Id: traiter_edit.m,v 1.6 2003/04/14 17:07:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = traiter_edit(this)

% -------------------------
% Appel methode super class

this.clc_edit_texte_pnl = callback(this.clc_edit_texte_pnl, get(this, 'msgEdit'));

% ----------------------------
% Initialisation de la valeur

x = str2double(get_texte(this));
if ~isempty(x)
    if x < this.minValue
        x = this.minValue;
    end
    if x > this.maxValue
        x = this.maxValue;
    end
    
    this = set_value(this, x);
else
    my_warndlg('clc_edit_value_pnl/traiter_edit : La valeur n''est pas interprétable', 0);
end

