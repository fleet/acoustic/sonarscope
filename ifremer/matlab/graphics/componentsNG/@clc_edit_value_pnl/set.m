% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%    this : instance de clc_edit_value
%
% Name-Value Pair Arguments
%    cf. clc_edit_value
%
% Output Arguments
%    this : instance de clc_edit_value initialisee
%
% Remarks :
%
% Examples
%    v = clc_edit_value;
%    set(v, 'value', 3.14, 'tooltip', 'PI')
%
% See also help clc_edit_value
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set(this, varargin)

% ---------------------
% Lecture des arguments

[varargin, value] = getPropertyValue(varargin, 'value', []);
[varargin, format] = getPropertyValue(varargin, 'format', []);
[varargin, minValue] = getPropertyValue(varargin, 'minValue', []);
[varargin, maxValue] = getPropertyValue(varargin, 'maxValue', []);
[varargin, actionnable] = getPropertyValue(varargin, 'actionnable', []);
[varargin, msgAction] = getPropertyValue(varargin, 'msgAction', []);

% ----------------------------
% Initialisation des attributs

if ~isempty(value)
    this = set_value(this, value);
end
if ~isempty(format)
    this = set_format(this, format);
end
if ~isempty(minValue)
    this = set_min_value(this, minValue);
end
if ~isempty(maxValue)
    this = set_max_value(this, maxValue);
end

% ------------------------------------------
% Traitement des messages de la super classe

if ~isempty(varargin)
    this.clc_edit_texte_pnl = set(this.clc_edit_texte_pnl, varargin{:});
end

% -------------------------------------
% Traitement des messages non autorises

if ~isempty(actionnable) || ~isempty(msgAction)
    my_warndlg('clc_edit_value_pnl/set : Can''t click on it', 0);
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end

