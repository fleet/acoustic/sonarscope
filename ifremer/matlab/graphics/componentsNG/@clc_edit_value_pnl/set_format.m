% Initialisation du format d affichage de la valeur
% 
% Syntax
%   this = set_format(this, format)
%
% Input Arguments
%    this : instance de clc_edit_value
%    format   : format d affichage de la valeur
%
% Output Arguments
%    this : instance de clc_edit_value initialisee
%
% See also help clc_edit_value
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function this = set_format(this, format)

isOk = 1;

% -------------------------------------
% Test validite des arguments :
% - doit etre une chaine de caracteres

if ~ischar(format)
    my_warndlg('clc_edit_value/set_format : Invalid format', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.format = format;
else
    this.format = '%f';
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l'affichage

this = set_value(this, this.value);
