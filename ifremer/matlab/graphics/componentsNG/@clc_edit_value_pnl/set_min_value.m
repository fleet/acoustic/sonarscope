% Initialisation de la valeur minimale
% 
% this = set_min_value(this, minValue)
%
% Input Arguments
%    this     : instance de clc_edit_value
%    minValue : valeur minimale
%
% Output Arguments
%    this : instance de clc_edit_value initialisee
%
% See also help clc_edit_value
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_min_value(this, minValue)

isOk = 1;

% -----------------------------
% Test validite des arguments :
% - doit etre un reel

if ~isnumeric(minValue)
    my_warndlg('clc_edit_value/set_min_value : Invalid format', 1);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.minValue = minValue;
else
    this.minValue = [];
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l'affichage

this = set_value(this, this.value);
