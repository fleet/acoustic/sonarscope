% Initialisation de la valeur de l instance
% 
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%    this : instance de clc_edit_value
%    value : valeur reelle
%
% Output Arguments
%    this : instance de clc_edit_value initialisee
%
% See also help clc_edit_value Authors
% Authors : DCF
% VERSION  : $Id: set_value.m,v 1.3 2003/04/04 14:22:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_value(this, value)

isOk = 1;

% -----------------------------------------------------
% Test validite des arguments :
% - doit etre une valeur reelle
% - doit etre dans l intervalle des valeurs admissibles

if ~isnumeric(value)
    my_warndlg('clc_edit_value_pnl/set_value : Invalid format', 1);
    isOk = 0;
else

    if ~isempty(this.minValue)
        if value < this.minValue
            my_warndlg(['clc_edit_value_pnl/set_value : value less than ' num2str(this.minValue)], 1);
            isOk = 0;
        end
    end

    if ~isempty(this.maxValue)
        if value > this.maxValue
            my_warndlg(['clc_edit_value_pnl/set_value : value greater than ' num2str(this.maxValue)], 1);
            isOk = 0;
        end
    end

end

% ---------------------
% Si ok, initialisation

if isOk
    this.value = value;
else
    this.value = [];
end

% ---------------------------------
% Initialisation de la super classe

if ~isempty(this.value)
    this = set_texte(this, sprintf(this.format, this.value));
else
    this = set_texte(this, ' ');
end
