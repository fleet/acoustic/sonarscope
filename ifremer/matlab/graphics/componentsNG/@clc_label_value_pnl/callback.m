% Methode de gestion des call back du composant
%
% Syntax
%   this = gerer_callback(this, cbName)
% 
% Input Arguments
%   this     : instance de cl_edit_value
%   cbName   : nom de la call back
%   varargin : parametres optionnels de la call back
%
% Output Arguments
%   this : instance de cl_component initialise�e
%
% Remarks :
%   doit �tre surcharg�e
%
% See also clc_edit_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = callback(this, varargin)

if ~isempty(varargin)
    switch varargin{1}
        case get(this, 'msgEdit')
            this = traiter_edit(this);
        case get(this, 'msgAction')
            this = traiter_action(this);
        otherwise
            my_warndlg('clc_label_value_pnl/callback : Invalid message', 0);
    end
else
    my_warndlg('clc_label_value_pnl/callback : Invalid arguments number', 0);
end

