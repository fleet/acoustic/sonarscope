% Constructeur de clc_label_value_pnl :
% composant d'edition de valeur avec zone descriptive de texte.
% clc_label_value_pnl est compos� d'autres composants :
%   1 clc_edit_texte_pnl | 1 clc_edit_value_pnl | 1 clc_edit_texte_pnl
%
% Syntax
%   this = clc_label_value(...)
% 
% Name-Value Pair Arguments
%    titre             : texte de titre precisant la variable ou le nom de la valeur
%    unite             : texte d unite precisant l unite de la valeur
%    nbColTitre        : nombre de colonnes occupees par le titre
%    nbColValue        : nombre de colonnes occupees par la valeur
%    nbColUnite        : nombre de colonnes occupees par l unite
%    value             : valeur reelle
%    format            : format d affichage de la valeur
%    minValue          : valeur minimale admissible
%    maxValue          : valeur maximale admissible
%    tag               : tag du label d affichage de la valeur
%    tooltip           : info-bulle du label d affichage de la valeur
%    editable          : flag indiquant si la valeur est editable
%    actionnable       : flag indiquant si le titre est cliquable
%    msgEdit           : msg de call back en cas d edition de la valeur
%    msgAction         : msg de call back en cas de click du titre
%    enable            : positionne l accessibilite de la valeur
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant    'on' | 'off'
%    componentEnable   : accessibilite du composant {on' | 'off'
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments 
%    this          : instance de clc_label_value
%
% Remarks : 
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab
%
% Examples
%     v = clc_label_value
%
%     % EXEMPLE "type" cla_pames
%     ihm = [];
%     ihm{end+1}.Lig         = 2;
%     ihm{end}.Col           = 2:4;
%     ihm{end}.Style         = 'clc_label_value_pnl';
%     ihm{end}.Name          = 'label_value_pnl_Name1';
%     ihm{end}.Parent        = 'AncrePanel';
%     ihm{end}.tag           = 'label_value_pnl_Tag1';
%     ihm{end}.value         = 1035;
%     ihm{end}.titre         = 'Pression';
%     ihm{end}.format        = '%5.0f';
%     ihm{end}.maxvalue      = Inf;
%     ihm{end}.minvalue      = -Inf;
%     ihm{end}.unite         = 'hPa';
%     ihm{end}.editable      = 1;
%     ihm{end}.nbColTitre    = 1;
%     ihm{end}.nbColValue    = 1;
%     ihm{end}.nbColUnite    = 1;
%     ihm{end}.actionnable   = 1;
%     ihm{end}.tooltip       = 'A �diter';
%     ihm{end}.globalBorder  = 1;
%     ihm{end}.borderColor   = 'black';
%
%     ihm{end+1}.Lig         = 3;
%     ihm{end}.Col           = 2:4;
%     ihm{end}.Style         = 'clc_label_value_pnl';
%     ihm{end}.Name          = 'label_value_pnl_Name2';
%     ihm{end}.Parent        = 'AncrePanel';
%     ihm{end}.tag           = 'label_value_pnl_Tag2';
%     ihm{end}.value         = 28;
%     ihm{end}.titre         = 'Temp�rature';
%     ihm{end}.format        = '%5.0f';
%     ihm{end}.maxvalue      = Inf;
%     ihm{end}.minvalue      = -Inf;
%     ihm{end}.unite         = '�C';
%     ihm{end}.editable      = 1;
%     ihm{end}.nbColTitre    = 1;
%     ihm{end}.nbColValue    = 1;
%     ihm{end}.nbColUnite    = 1;
%     ihm{end}.actionnable   = 0;
%     ihm{end}.tooltip       = 'A �diter';
%     ihm{end}.globalBorder  = 1;
%     ihm{end}.borderColor   = 'black';
%
%     ihm{end+1}.Lig         = 1:5;
%     ihm{end}.Col           = 1:5;
%     ihm{end}.Style         = 'panel';
%     ihm{end}.Tag		     = 'AncrePanel';
% 
%     ihm_global = cl_panel([]);
%     ihm_global = set_tab_uiComposant(ihm_global, ihm);
%
% See also clc_label_value cla_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = clc_label_value_pnl(varargin)

% --------------------------------------------------------------------------
% Definition de la structure et initialisation
%
% globalFrame : instance de cl_frame, frame globale du composant
% c_titre     : composant d'edition du titre
% c_editValue : composant d'edition de la valeur
% c_unite     : composant d'edition de l'unite
% nbColTitre  : nombre de colonnes occupees par le titre
% nbColValue  : nombre de colonnes occupees par la valeur
% nbColUnite  : nombre de colonnes occupees par l' unite

this.globalPanel        = [];
%this.c_editValue = clc_edit_value([]);
%this.c_titre     = clc_edit_texte_pnl([]);
%this.c_unite     = clc_edit_texte_pnl([]);
this.nbColTitre         = 1;
this.nbColValue         = 1;
this.nbColUnite         = 1;
this.globalBorder       = 0;
this.labelBorder        = 0;
this.borderColor        = []; %[0.8, 0.8, 0.8];
this.titre              = '';
this.unite              = '';
this.value              = 0;
this.format             = '%f';
this.minValue           = [];
this.maxValue           = [];
this.tag                = [];
this.editable           = 0;
this.msgEdit            = [];
this.msgAction          = [];
this.cbAction           = [];
this.actionnable        = 0;
this.visible            ='on';

% ------------
% Super-classe

% if (nargin == 1) && isempty(varargin {1})
%     composant = cl_component([]);
% else
    composant = cl_component([]);
% end

% -------------------
% Creation de l'objet

this = class(this, 'clc_label_value_pnl', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin{1})
    return;
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin {:});
