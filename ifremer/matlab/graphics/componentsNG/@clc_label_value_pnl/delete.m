% Destruction graphique du composant
%
% Syntax
%   this = delete(this)
% 
% Input Arguments
%   this : instance de clc_label_value
%
% Remarks :
%   doit etre surchargee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: delete.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = delete(this)

% Maintenance Auto : try
    
    % ----------------------------------------
    % Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    % -----------------------------------
    % Destruction des elements graphiques
    
    if ~isempty(this.globalFrame)
        this.c_titre     = delete(this.c_titre);
        this.c_unite     = delete(this.c_unite);
        this.c_editValue = delete(this.c_editValue);
        this.globalFrame = delete(this.globalFrame);
        this.globalFrame = [];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value', 'delete', lasterr);
% Maintenance Auto : end
