% Appel de la representation graphique de l'instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_label_value
%
% OUPUT PARAMETERS :
%   this : instance de clc_label_value
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: editobj.m,v 1.4 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

function this = editobj(this)

% ---------------------------------
% Creation de l'interface graphique

if ~is_edit(this)
    this.cl_component = editobj(this.cl_component);
    this = construire_ihm(this);
end
