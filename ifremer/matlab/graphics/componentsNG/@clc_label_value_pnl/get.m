% Lecture des attributs et proprietes de l'instance
%
% Syntax
%   varargout = get(this, ...)
% 
% Input Arguments
%    this              : instance de clc_label_value
%    varargin          : noms des proprietes
%    titre             : texte de titre precisant la variable
%    unite             : texte d unite precisant l unite
%    nbColTitre        : nombre de colonnes occupees par le titre
%    nbColValue        : nombre de colonnes occupees par la valeur
%    nbColUnite        : nombre de colonnes occupees par l unite
%    value             : valeur reelle
%    format            : format d affichage de la valeur
%    minValue          : valeur minimale admissible
%    maxValue          : valeur maximale admissible
%    tag               : tag du label d affichage de la valeur
%    tooltip           : tooltip du label d affichage de la valeur
%    handle            : handle du label d affichage de la valeur
%    editable          : flag indiquant si la valeur est editable
%    actionnable       : flag indiquant si le titre est cliquable
%    msgEdit           : msg de call back en cas d edition de la valeur
%    msgAction         : msg de call back en cas de click sur le titre
%    enable            : positionne l accessibilite de la valeur
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant    'on' | 'off'
%    componentEnable   : accessibilite du composant 'on' | 'off'
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%    componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    v = clc_label_value; 
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'titre'
            varargout{end+1} = get_titre(this); %#ok<AGROW>
        case 'unite'
            varargout{end+1} = get_unite(this); %#ok<AGROW>
        case 'nbColTitre'
            varargout{end+1} = get_nb_col_titre(this); %#ok<AGROW>
        case 'nbColValue'
            varargout{end+1} = get_nb_col_value(this); %#ok<AGROW>
        case 'nbColUnite'
            varargout{end+1} = get_nb_col_unite(this); %#ok<AGROW>
        case 'enable'
            varargout{end+1} = get_enable(this); %#ok<AGROW>
        case 'value'
            varargout{end+1} = get_value(this); %#ok<AGROW>
        case 'format'
            varargout{end+1} = get_format(this); %#ok<AGROW>
        case 'msgEdit'
            varargout{end+1} = get_msgEdit(this); %#ok<AGROW>
        case {'minValue', 'maxValue', ...
                'tooltip', 'tag', 'handle', 'editable'}
            varargout {end+1} = get(this.c_editValue, varargin {1});
        case { 'actionnable', 'msgAction'}
            varargout {end+1} = '';%get(this.c_titre, varargin {1});
        case{ 'componentVisible', 'componentEnable', 'componentName', ...
                'componentAnchor','componentParent' , 'componentUserName', 'componentUserCb' }
            varargout {end+1} = get(this.cl_component, varargin{i});
        case 'globalPanel'
            varargout {end+1} = get_globalPanel(this);
            
        otherwise
            my_warndlg('clc_label_value/get : Invalid property name', 0);
    end
end
