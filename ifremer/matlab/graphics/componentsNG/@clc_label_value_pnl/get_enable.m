% Retourne l accessibilite de la valeur du composant
%
% Syntax
%   enable = get_enable(this)
%
% Input Arguments
%    this : instance de clc_label_value
%
% Output Arguments
%    enable   : accessibilite de la valeur du composant
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: get_enable.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    08/02/2001 - DCF - creation
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function enable = get_enable(this)

% Maintenance Auto : try
    enable = get_enable(this.c_editValue);
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value', 'get_enable', lasterr);
% Maintenance Auto : end
