% Retourne le nombre de colonnes occupees par le titre
%
% Syntax
%   nbColTitre = get_nb_col_titre(this)
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   nbColTitre : nombre de colonnes occupees par le titre
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: get_nb_col_titre.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbColTitre = get_nb_col_titre(this)

% Maintenance Auto : try
    nbColTitre = this.nbColTitre;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value', 'get_nb_col_titre', lasterr);
% Maintenance Auto : end
