% Retourne le nombre de colonnes occupees par l unite
%
% Syntax
%   nbColUnite = get_nb_col_unite(this)
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   nbColUnite : nombre de colonnes occupees par l unite
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: get_nb_col_unite.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function nbColUnite = get_nb_col_unite(this)

% Maintenance Auto : try
    nbColUnite = this.nbColUnite;
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value', 'get_nb_col_unite', lasterr);
% Maintenance Auto : end
