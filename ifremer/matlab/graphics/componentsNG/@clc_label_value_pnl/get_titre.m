% Retourne le titre
%
% Syntax
%   titre = get_titre(this)
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   titre : titre
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: get_titre.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    08/02/2001 - DCF - utilisation de clc_edit_texte pour le titre et l unite
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function titre = get_titre(this)

% Maintenance Auto : try
    titre = get_texte(this.c_titre);
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_label_value', 'get_titre', lasterr);
% Maintenance Auto : end
