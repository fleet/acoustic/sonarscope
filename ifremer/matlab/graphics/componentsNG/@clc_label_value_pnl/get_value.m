function value = get_value(this)
cpn_edit_value = getfield(this.globalPanel.cpn,[get(this,'componentName') '_value']);
value=get(cpn_edit_value,'value');
this.value = value;
