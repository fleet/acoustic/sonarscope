% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this)
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   str : une chaine de caracteres representative de l'instance
%
% Examples
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

% ----------
% Traitement
str{end+1} = sprintf('tag         <-> %s', mat2str(this.tag));
str{end+1} = sprintf('nbColTitre  <-> %d', this.nbColTitre);
str{end+1} = sprintf('nbColValue  <-> %d', this.nbColValue);
str{end+1} = sprintf('nbColUnite  <-> %d', this.nbColUnite);
str{end+1} = sprintf('Titre       <-> %s', this.titre);
str{end+1} = sprintf('value       <-> %f', this.value);
str{end+1} = sprintf('format      <-> %s', this.format);
str{end+1} = sprintf('Unit�       <-> %s', this.unite);
str{end+1} = sprintf('minValue    <-> %f', this.minValue);
str{end+1} = sprintf('maxValue    <-> %f', this.maxValue);
str{end+1} = sprintf('editable    <-> %s', mat2str(this.editable));
str{end+1} = sprintf('actionnable <-> %s', mat2str(this.actionnable));
str{end+1} = sprintf('msgEdit     <-> %s', mat2str(this.msgEdit));
str{end+1} = sprintf('msgAction   <-> %s', mat2str(this.msgAction));

str{end+1} = sprintf('--- heritage cl_component ---');
str{end+1} = char(this.cl_component);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);

% ---------------------------
% Concatenation en une chaine

str = char(strcat(str));

