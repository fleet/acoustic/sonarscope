% Construction de base de l'IHM
%
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   this : instance de clc_label_value
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: construire_ihm.m,v 1.3 2003/04/14 11:47:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    07/02/2001 - DCF - creation
%    08/02/2001 - DCF - utilisation de clc_edit_texte pour le titre
%                       et l unite
%    22/03/2001 - DCF - maj optimisation code
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

    
    % ---------------------------------
    % Creation du panel du composant
    
    this = creer_panel_global(this);
