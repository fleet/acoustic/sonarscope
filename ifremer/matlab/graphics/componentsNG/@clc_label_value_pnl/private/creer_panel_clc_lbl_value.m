
function this = creer_panel_clc_lbl_value(this)

ihm         = [];
Parent      =[get(this, 'componentName'), ' ', 'clc_label_value_pnl'];

% ---------------------------------------
% Ancre du composant clc_edit_texte titre

if this.nbColTitre~=0

    ihm{end+1}.Lig         = 1;
    ihm{end}.Col           = 1:this.nbColTitre; %1;
    ihm{end}.Style         = 'clc_edit_texte_pnl';
    ihm{end}.texte         = this.titre;
    ihm{end}.Name          = [get(this, 'componentName'),'_titre'];
    ihm{end}.Parent        = Parent;
    ihm{end}.Tag		   = [get(this, 'componentName'),'_titre'];
    ihm{end}.actionnable   = this.actionnable;
    if this.actionnable==1
        if ~isempty(this.cbAction)
            ihm{end}.cbAction      = this.cbAction;
        elseif ~isempty(this.msgAction)
            ihm{end}.msgAction     = this.msgAction;
            ihm{end}.UserCb              = get(this,'componentUserCb');
            ihm{end}.UserName            = get(this,'componentUserName');
%         else
%            disp(['pas de callback associe au bouton',get(this, 'componentName'), ' ', 'clc_edit_texte titre']);
        end
    end
    if this.labelBorder
        ihm{end}.border        = 1;
        if ~isempty(this.borderColor)
            ihm{end}.borderColor   = this.borderColor;
        end
    end

end

% ---------------------------------
% Ancre du composant clc_edit_value

if this.nbColValue~=0
    
    ihm{end+1}.Lig              = 1;
    ihm{end}.Col                = (this.nbColTitre+1):(this.nbColTitre+this.nbColValue);%2;
    ihm{end}.Style              = 'clc_edit_value_pnl';
    ihm{end}.Name               = [get(this, 'componentName'),'_value'];
    ihm{end}.value              = this.value;
    ihm{end}.format             = this.format;
    ihm{end}.UserCb              = get(this,'componentUserCb');
    ihm{end}.UserName            = get(this,'componentUserName');
    if this.editable
        ihm{end}.editable       =1;
    end
    if ~isempty(this.minValue)
        ihm{end}.minValue       = this.minValue;
    end
    if ~isempty(this.maxValue)
        ihm{end}.maxValue       = this.maxValue;
    end
    ihm{end}.Tag                = this.tag;
    ihm{end}.Parent             = Parent;
    if ~isempty(this.msgEdit)
        ihm{end}.msgEdit            = this.msgEdit;
    end
    if this.labelBorder
        ihm{end}.border        = 1;
        if ~isempty(this.borderColor)
            ihm{end}.borderColor   = this.borderColor;
        end
    end 

end

% ---------------------------------------
% Ancre du composant clc_edit_texte unite

if this.nbColUnite~=0

    ihm{end+1}.Lig         = 1;
    ihm{end}.Col           = (this.nbColTitre+this.nbColValue+1):(this.nbColTitre+this.nbColValue+this.nbColUnite);
    ihm{end}.Style         = 'clc_edit_texte_pnl';
    ihm{end}.texte         = this.unite;
    ihm{end}.Name          = [get(this, 'componentName'), '_unite'];
    ihm{end}.Parent        = Parent;
    ihm{end}.Tag		   = [get(this, 'componentName'),'_unite'];
    if this.labelBorder
        ihm{end}.border        = 1;
        if ~isempty(this.borderColor)
            ihm{end}.borderColor   = this.borderColor;
        end
    end

end

this.globalPanel = cl_panel('nomPanel', Parent);
this.globalPanel = set_tab_uiComposant(this.globalPanel, ihm);
%this.globalPanel = set(this.globalPanel, 'ListeUiComposants', ihm);
