% Creation des panels principaux du composant
%
% Syntax
%   this = creer_panel_global(this)
%
% Input Arguments
%   this : instance de clc_label_value_pnl
%
% Output Arguments
%   this : instance de clc_label_value_pnl initialisee
%
% See also clc_label_value_pnl Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = creer_panel_global(this)

ihm      = [];

ihm{end+1}.Lig   = 1;
ihm{end}.Col     = 1;
ihm{end}.Style   = 'panel';
ihm{end}.Parent = get(this,'componentParent');
ihm{end}.Tag     = [get(this, 'componentName'), ' ', 'clc_label_value_pnl'];
ihm{end}.BorderType= 'line';
ihm{end}.BorderWidth= 1;
if this.globalBorder
    if ~isempty(this.borderColor)
        ihm{end}.HighlightColor         = this.borderColor;
    else
        ihm{end}.HighlightColor         = [0.8 0.8 0.8];
    end
else
    ihm{end}.HighlightColor         = Gris;
end


% -------------------
% Creation du panel

this.globalPanel = cl_panel([]);
this.globalPanel = set_handle_panel(this.globalPanel, get_anchor(this));
this.globalPanel = set_max_lig(this.globalPanel, 1);
this.globalPanel = set_max_col(this.globalPanel, 1);
this.globalPanel = set_tab_uiComposant(this.globalPanel, ihm);

this = creer_panel_clc_lbl_value(this);

