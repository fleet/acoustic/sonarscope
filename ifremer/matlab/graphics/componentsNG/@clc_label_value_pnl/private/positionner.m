% Positionne les elements du composant
%
% Syntax
%   this = positionner(this)
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: positionner.m,v 1.3 2003/04/14 11:47:00 augustin Exp $
% ----------------------------------------------------------------------------

function this = positionner(this)

colMin = 0;
colMax = 0;
col    = 0;

% --------------------------------------------------------
% Lecture des handles des differents elements du composant

handles = get_tab_uicontrols(this.globalPanel);

% -------------------------------------
% Indique le nombre maximum de colonnes

this.globalPanel = set_max_col(this.globalPanel, ...
    this.nbColTitre + this.nbColValue + this.nbColUnite);
this.globalPanel = set_max_lig(this.globalPanel, 1);

% -------------------
% Positionne le titre

if this.nbColTitre > 0
    colMin = colMax + 1;
    colMax = colMin + this.nbColTitre - 1;
    col    = colMin:colMax;
    this.globalPanel = replacer(this.globalPanel, 'uicontrol', 1, 1, col);
    move(this.globalPanel, handles(1), 1, col);
    %drawnow
    set(handles(1), 'visible', 'on');
    this.c_titre = set_visible(this.c_titre, 'on');
    this.c_titre = update(this.c_titre);
else
    set(handles(1), 'visible', 'off');
    this.c_titre = set_visible(this.c_titre, 'off');
end

% --------------------------------
% Positionne la Panel du composant

if this.nbColValue > 0
    colMin = colMax + 1;
    colMax = colMin + this.nbColValue - 1;
    col    = colMin:colMax;
    this.globalPanel = replacer(this.globalPanel, ...
        'uicontrol', 2, 1, col);
    move(this.globalPanel, handles(2), 1, col);
    %drawnow
    set(handles(2), 'visible', 'on');
    this.c_editValue = set_visible(this.c_editValue, 'on');
    this.c_editValue = update(this.c_editValue);
else
    set(handles(2), 'visible', 'off');
    this.c_editValue = set_visible(this.c_editValue, 'off');
end

% ------------------
% Positionne l'unite

if this.nbColUnite > 0
    colMin = colMax + 1;
    colMax = colMin + this.nbColUnite - 1;
    col    = colMin:colMax;
    this.globalPanel = replacer(this.globalPanel, 'uicontrol', 3, 1, col);
    move(this.globalPanel, handles(3), 1, col);
    %drawnow
    set(handles(3), 'visible', 'on');
    this.c_unite = set_visible(this.c_unite, 'on');
    this.c_unite = update(this.c_unite);
else
    set(handles(3), 'visible', 'off');
    this.c_unite = set_visible(this.c_unite, 'off');
end
