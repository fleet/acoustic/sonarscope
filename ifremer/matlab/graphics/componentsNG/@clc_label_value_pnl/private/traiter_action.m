% Traitement apres click sur le titre
%
% Syntax
%   this = traiter_action(this) 
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% Examples
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: traiter_action.m,v 1.4 2003/04/14 11:47:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = traiter_action(this)


% Maintenance Auto : try
    % On peur se demander pourquoi cette methode existe non ?
% Maintenance Auto : catch
% Maintenance Auto :   err('clc_label_value', 'traiter_action', lasterr);
% Maintenance Auto : end
