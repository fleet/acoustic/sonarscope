% Traitement apres edition de la valeur
%
% Syntax
%   this = traiter_edit(this) 
%
% Input Arguments
%   this : instance de clc_label_value
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% Examples
%
% See also clc_label_value Authors
% Authors : JMA
% VERSION  : $Id: traiter_edit.m,v 1.4 2003/04/14 11:47:00 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    08/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = traiter_edit(this)

    % ----------------------------------------------------
    % Lecture a partir de l IHM et initialisation du texte
    
    %this.globalPanel.cpn{2} = callback(this.globalPanel.cpn{2}, this.msgEdit);
    cpn_edit_value = getfield(this.globalPanel.cpn,[get(this,'componentName') '_value']);
    cpn_edit_value = callback(cpn_edit_value, this.msgEdit);
    this.globalPanel.cpn = setfield(this.globalPanel.cpn,[get(this,'componentName') '_value'],cpn_edit_value);
    
    %if is_edit(this)
        %this.c_editValue = gerer_callback(this.c_editValue, get_msg_edit(this.c_editValue));
   % end

