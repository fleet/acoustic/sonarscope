% Initialisation du actionnable
%
% Syntax
%   instance = set_actionnable(this, actionnable)
%
% Input Arguments
%   this        : instance de clc_label_actionnable
%   actionnable : actionnable
%
% Output Arguments
%   this : instance de clc_label_actionnable initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_actionnable(this, actionnable)

this.actionnable=actionnable;
