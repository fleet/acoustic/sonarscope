% Initialisation du editable
%
% Syntax
%   this = set_editable(this, editable)
%
% Input Arguments
%   this : instance de clc_label_editable
%   editable : editable
%
% Output Arguments
%   this : instance de clc_label_editable initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_editable.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_editable(this, editable)

if ~isempty(this.globalPanel)
    cpn_edit_value = getfield(this.globalPanel.cpn,[get(this,'componentName') '_value']);
    cpn_edit_value = set_editable( cpn_edit_value,editable);
    this.globalPanel.cpn = setfield(this.globalPanel.cpn,[get(this,'componentName') '_value'],cpn_edit_value);
    
    
%     this.globalPanel.cpn{2}=set_editable( this.globalPanel.cpn{2},editable);
%     this.globalPanel.cpn{2}=set( this.globalPanel.cpn{2},'Value',value);
end
this.editable = editable;
