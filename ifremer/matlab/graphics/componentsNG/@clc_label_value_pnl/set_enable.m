% Initialisation de l accessibilite de la valeur du composant
%
% Syntax
%   this = set_enable(this, enable)
%
% Input Arguments
%   this   : instance de clc_label_value
%   enable : accessibilite de la valeur du composant {'on', 'off'}
%
% Output Arguments
%    this : instance de clc_label_value initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_enable.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% -------------------------------------------
% Test validite des arguments, initialisation

if ischar(enable)
    this.c_editValue  = set_enable(this.c_editValue, enable);
else
    my_warndlg('clc_label_value/set_enable : Invalid value', 0);
end
