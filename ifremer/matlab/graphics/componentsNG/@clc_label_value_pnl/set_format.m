% Initialisation du format
%
% Syntax
%   this = set_format(this, format)
%
% Input Arguments
%   this   : instance de clc_label_format
%   format : format
%
% Output Arguments
%   this : instance de clc_label_format initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_format(this, format)

this.format = format;
