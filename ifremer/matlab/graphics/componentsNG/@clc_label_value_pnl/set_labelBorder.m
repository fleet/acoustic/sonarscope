% Initialisation de la valeur
%
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%   this  : instance de clc_label_value
%   value : value
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_value.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_labelBorder(this, labelBorder)

    this.labelBorder = labelBorder;
    
