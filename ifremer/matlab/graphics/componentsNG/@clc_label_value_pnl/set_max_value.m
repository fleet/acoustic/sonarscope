% Initialisation de la valeur maximale
%
% Syntax
%   instance = set_max_value(this, maxValue)
%
% Input Arguments
%   this     : instance de clc_label_maxValue
%   maxValue : maxValue
%
% Output Arguments
%    this : instance de clc_label_maxValue initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_max_value.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_max_value(this, maxValue)


this.maxValue = maxValue;



   % this.c_editValue = set_max_value(this.c_editValue, maxValue);
