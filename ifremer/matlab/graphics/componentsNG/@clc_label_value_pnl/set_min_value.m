% Initialisation de la valeur minimale
%
% Syntax
%   this = set_min_value(this, minValue)
%
% Input Arguments
%   this     : instance de clc_label_minValue
%   minValue : minValue
%
% Output Arguments
%   this : instance de clc_label_minValue initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_min_value(this, minValue)

this.minValue = minValue;
