% Initialisation du msgAction (en cas de click sur le titre)
%
% Syntax
%   this = set_msg_action(this, msgAction)
%
% Input Arguments
%   this      : instance de clc_label_msgAction
%   msgAction : msgAction
%
% Output Arguments
%   this : instance de clc_label_msgAction initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_msg_action.m,v 1.4 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    22/03/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function this = set_msg_action(this, msgAction)

this.msgAction=msgAction;
   % this.c_titre = set_msg_action(this.c_titre, msgAction);
