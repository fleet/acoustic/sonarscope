% Initialisation du msgEdit (en cas de modification de la valeur)
%
% Syntax
%   this = set_msg_edit(this, msgEdit)
%
% Input Arguments
%   this    : instance de clc_label_msgEdit
%   msgEdit : msgEdit
%
% Output Arguments
%   this : instance de clc_label_msgEdit initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_msg_edit(this, msgEdit)

this.msgEdit = msgEdit;
