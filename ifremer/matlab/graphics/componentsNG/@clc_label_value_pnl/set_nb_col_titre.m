% Initialisation du nombre de colonnes du titre
%
% Syntax
%   this = set_nb_col_titre(this, nbColTitre)
%
% Input Arguments
%   this       : instance de clc_label_value
%   nbColTitre : nombre de colonnes du titre
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_nb_col_titre.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_nb_col_titre(this, nbColTitre)

isOk = 1;

% --------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - ne doit pas etre une matrice

if ~isnumeric(nbColTitre)
    my_warndlg('clc_label_value/set_nb_col_titre : Invalid format', 0);
    isOk = 0;
end
[m, n] = size(nbColTitre);
if (m*n > 1)
    my_warndlg('clc_label_value/set_nb_col_titre : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.nbColTitre = nbColTitre;
else
    this.nbColTitre = 0;
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l affichage

if is_edit(this)
    this = positionner(this);
end
