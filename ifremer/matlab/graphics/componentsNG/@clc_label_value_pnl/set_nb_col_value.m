% Initialisation du nombre de colonnes de la valeur
%
% Syntax
%   this = set_nb_col_value(this, nbColValue)
%
% Input Arguments
%   this       : instance de clc_label_value
%   nbColValue : nombre de colonnes de la valeur
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_nb_col_value.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_nb_col_value(this, nbColValue)

isOk = 1;

% --------------------------------
% Test validite des arguments :
% - doit etre une valeur numerique
% - ne doit pas etre une matrice

if ~isnumeric(nbColValue)
    my_warndlg('clc_label_value/set_nb_col_value : Invalid format', 0);
    isOk = 0;
end
[m, n] = size(nbColValue);
if (m*n > 1)
    my_warndlg('clc_label_value/set_nb_col_value : Invalid value', 0);
    isOk = 0;
end

% ---------------------
% Si ok, initialisation

if isOk
    this.nbColValue = nbColValue;
else
    this.nbColValue = 0;
end

% -------------------------------------------------
% Si affichage en cours, mise a jour de l affichage

if is_edit(this)
    this = positionner(this);
end
