% Initialisation de l'info-bulle
%
% Syntax
%   this = set_tooltip(this, tooltip)
%
% Input Arguments
%   this    : instance de clc_label_tooltip
%   tooltip : info-bulle
%
% Output Arguments
%   this : instance de clc_label_tooltip initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_tooltip(this, ~)
