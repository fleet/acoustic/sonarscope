% Initialisation de l unite de la valeur
%
% Syntax
%   this = set_unite(this, unite)
%
% Input Arguments
%   this  : instance de clc_label_value
%   unite : unite
%

function this = set_unite(this, unite)

this.unite = unite;
