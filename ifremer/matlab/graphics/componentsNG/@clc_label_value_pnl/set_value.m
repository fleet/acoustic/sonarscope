% Initialisation de la valeur
%
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%   this  : instance de clc_label_value
%   value : value
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% VERSION  : $Id: set_value.m,v 1.3 2003/04/14 15:14:33 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_value(this, value)
if ~isempty(this.globalPanel)
    cpn_edit_value = getfield(this.globalPanel.cpn,[get(this,'componentName') '_value']);
    cpn_edit_value = set_value( cpn_edit_value,value);
    this.globalPanel.cpn = setfield(this.globalPanel.cpn,[get(this,'componentName') '_value'],cpn_edit_value);
    %     this.globalPanel.cpn{2}=set( this.globalPanel.cpn{2},'Value',value);
end

this.value = value;



