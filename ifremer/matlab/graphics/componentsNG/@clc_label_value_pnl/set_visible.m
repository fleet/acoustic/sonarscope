% Initialisation de la visibilite du composant
%
% Syntax
%   this = set_cvisible(this, visible)
%
% Input Arguments
%   this    : instance de clc_label_value
%   visible : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   this : instance de clc_label_value initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_visible(this, visible)

handlePanel=get(this.globalPanel.MainPanel,'HandlePanel');
set(handlePanel,'visible',visible)
this.visible=visible;
