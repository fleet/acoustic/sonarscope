% Gestion des call back d'une IHM d'une instance
%
% Syntax
%   this = gerer_callback(this, message)
% 
% Input Arguments
%   this    : instance de clc_list_value
%   message : Message pour identifier l'action
%
% Name-Value Pair Arguments
%   Les valeurs sont dynamiques, elles sont lues par :
%     this.msgEdit
%
% Output Arguments
%   b : instance de clc_list_value initialisée
%
% See also clc_list_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = callback(this, varargin)

if length(varargin) == 1
    switch varargin{1}
        case get(this, 'msgEdit')
            this = traiter_Edit(this);
        otherwise
            my_warndlg('clc_list_value_pnl/callback : Invalid message', 0);
    end
else
    my_warndlg('clc_list_value_pnl/callback : Invalid arguments number', 0);
end
    
