% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments 
%   a : instance de clc_list_value
%
% Examples
%   display(clc_list_value) ;
%
% See also help clc_list_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size  ;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
