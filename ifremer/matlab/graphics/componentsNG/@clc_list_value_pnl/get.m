% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
%
% Input Arguments
%   this : instance de clc_list_value
%
% Name-only Arguments
%    cf. clc_list_value
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
%
% Examples
%    v = clc_list_value;
%    set(v, 'value', 3.14, 'tooltip', 'PI');
%    get(v, 'tooltip')
%
% See also clc_list_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'value'
            varargout{end+1} = get_value(this); %#ok<AGROW>
        case 'liste'
            varargout{end+1} = get_liste(this);      %#ok<AGROW>   
        case 'globalPanel'
            varargout {end+1} = get_globalPanel(this); %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.clc_edit_texte_pnl, varargin{i}); %#ok<AGROW>
    end
end

