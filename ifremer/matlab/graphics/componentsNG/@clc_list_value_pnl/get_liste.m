% Retourne la liste du popupmenu
%
% Syntax
%   liste = get_liste(this)
%
% Input Arguments
%    this : instance de clc_list_value
%
% Output Arguments
%    value : valeur reelle
%
% See also clc_list_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function liste = get_liste(this)

liste = get(this.clc_edit_texte_pnl, 'texte');
this.liste = liste;
