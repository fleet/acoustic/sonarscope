% Retourne la valeur reelle
%
% Syntax
%   value = get_value(this)
%
% Input Arguments
%    this : instance de clc_list_value
%
% Output Arguments
%    value : valeur reelle
%
% See also clc_list_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function value = get_value(this)

value = get(this.clc_edit_texte_pnl, 'value');
this.value = value;
