% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%    a : instance de clc_list_value
%
% Output Arguments
%    str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also help clc_list_value
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------


function str = char_instance(this)

str = [];

% ----------
% Traitement

str{end+1} = sprintf('value    <-> %f', this.value);
str{end+1} = sprintf('liste    <-> %s', SelectionDansListe2str(this.value,this.liste,'Num'));

str{end+1} = sprintf('--- heritage clc_edit_texte ---');
str{end+1} = char(this.clc_edit_texte_pnl);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
    
