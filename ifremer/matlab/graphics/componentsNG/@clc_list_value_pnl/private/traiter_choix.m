% Traitement apres modification de la valeur
%
% Syntax
%   this = traiter_edit(this) 
%
% Input Arguments
%   this : instance de clc_edit_value
%
% Output Arguments
%   this : instance de clc_list_value initialisee
%
% Examples
%
% See also clc_list_value Authors
% Authors : DCF
% VERSION  : $Id: traiter_edit.m,v 1.6 2003/04/14 17:07:37 augustin Exp $
% ----------------------------------------------------------------------------

function this = traiter_choix(this)
  
% -------------------------
% Appel methode super class

this.clc_edit_texte_pnl = callback(this.clc_edit_texte_pnl, get(this, 'msgEdit'));
