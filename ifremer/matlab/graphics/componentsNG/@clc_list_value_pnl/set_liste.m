% Initialisation de la liste du popupmenu
% 
% Syntax
%   this = set_list(this, value)
%
% Input Arguments
%    this : instance de clc_list_value
%    value : cha�ne de caract�re (tableau de cellule possible
%
% Output Arguments
%    this : instance de clc_list_value initialisee
%
% See also clc_list_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_liste(this, liste)

this.liste = liste;

%% Initialisation de la super classe

if ~isempty(this.liste)
    this = set_texte(this, this.liste); %sprintf(this.format, this.value));
else
    this = set_texte(this, ' ');
end
