% Initialisation de la valeur de l instance
% 
% Syntax
%   this = set_value(this, value)
%
% Input Arguments
%    this : instance de clc_list_value
%    value : cha�ne de caract�re (tableau de cellule possible
%
% Output Arguments
%    this : instance de clc_list_value initialisee
%
% See also help clc_list_value Authors
% Authors : DCF
% VERSION  : $Id: set_value.m,v 1.3 2003/04/04 14:22:03 augustin Exp $
% ----------------------------------------------------------------------------

function this = set_value(this, value)

this.value = value;

% ---------------------------------
% Initialisation de la super classe

if ~isempty(this.value)
    this = set_val(this, this.value); %sprintf(this.format, this.value));
else
    this = set_val(this, 0);
end
