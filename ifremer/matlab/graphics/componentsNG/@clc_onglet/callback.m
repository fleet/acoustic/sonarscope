% Methode de gestion des call back du composant clc_onglet
%
% Syntax
%   this = gerer_callback(this, varargin)
%
% Authors : LUR
% ----------------------------------------------------------------------------

function this = callback(this, varargin)

sauv_state_onglet  = [];   %init
tag                = varargin{1};
nbOnglets          = varargin{2};
nbOnglets          = str2double(nbOnglets);
Numero             = varargin{3};
msg                = varargin{4};
Usr                = varargin(5);
this.onglet_actif  = Numero;

%% S�curit� LUR verouillage des onglets pendant le chargement du panel

GCF = gcf;
for k=1:nbOnglets
    handle_onglet = findobj(GCF, 'tag',[tag ' Ancre_onglet' num2str(k)]);
    sauv_state_onglet{k} = get(handle_onglet,'enable'); %#ok<AGROW>
    set(handle_onglet, 'enable', 'inactive');
end

%%

for k=1:nbOnglets
    handle_onglet       = findobj(GCF, 'tag',[tag ' Ancre_onglet' num2str(k)]);
    handle_panel_onglet = findobj(GCF, 'tag',[tag ' Ancre_panel_onglet' num2str(k)]);

    if num2str(k)~=Numero
        set(handle_onglet,'background',Gris);
        set(handle_onglet,'value',0);
        set(handle_panel_onglet,'visible','off');
        set_visible_JAVA_component(handle_panel_onglet,'off'); % Trouver une meilleure solution
    else
        %set(handle_onglet,'background',[0.3 0.3 0.3]);
        set(handle_onglet, 'value', 1);
        set(handle_panel_onglet, 'visible', 'on');
        set_visible_JAVA_component(handle_panel_onglet, 'on'); % Trouver une meilleure solution
        if ~strcmp(msg,'test')
            callback(eval(cell2str(Usr)), msg);
        end
    end
end

%% S�curit� LUR d�verouillage des onglets apr�s le chargement du panel

for k=1:nbOnglets
    handle_onglet = findobj(GCF, 'tag', [tag ' Ancre_onglet' num2str(k)]);
    state_onglet  = get(handle_onglet, 'enable');
    if strcmp(state_onglet, 'inactive')
        set(handle_onglet, 'enable', sauv_state_onglet{k}); 
    else
        set(handle_onglet, 'enable', state_onglet); 
    end
end


