% Representation textuelle d un tableau d'instances de clc_label_value
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance ou tableau d instance de clc_label_value
%
% Output Arguments
%   str : une chaine de caracteres representative
%
% Examples
%    char (clc_label_value)
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function str = char(this)

% -------------------------
% Intialisation des locales

str = [];

% ------------------------------------
% Traitement tableau ou instance seule

[m, n] = size(this);

if (m*n) > 1
    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
            str1 = char(this(i, j));
            str{end+1} = str1;
        end
    end
    str = cell2str(str);
    
else
    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
