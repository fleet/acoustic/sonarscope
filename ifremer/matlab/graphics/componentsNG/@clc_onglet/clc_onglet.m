% composant clc_onglet permettant de creer une IHM avec plusieurs panels
% superposes
%
% Syntax
%   this = clc_onglet(...)
%
% Name-Value Pair Arguments
%
%    names             : noms associes aux onglets
%    Onglets           : handles des onglets (pas des panels)
%    onglet_parent     : pour les sous-onglets c'est l'onglet parent
%    handles           : handles des panels (accessibles avec get(this,'Handles_panels'))
%    onglet_actif      : handle de l'onglet actif
%    nb_col            : nombre de colonnes dans le composant (determine la
%                        largeur des onglets)
%    nb_lig            : nombre de lignes dans le composant (determine la
%                        hauteur des onglets)
%    msgEdit           : messages de call back en cas de changement d'onglet
%    enable            : positionne l accessibilite de la valeur
%    componentName     : nom associe a ce composant
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments
%    this               : instance de clc_onglet
%
% EXAMPLE:
%
% ihm = {};
% %--------------------------------------------------------
% %Attributs generiques d'un composant
% ihm{end+1}.Style        = 'clc_onglet';
% ihm{end}.Name           = 'Projection';
% ihm{end}.Parent         = 'AncrePanelProjection';
% ihm{end}.UserName       = 'cla_carto';
% ihm{end}.UserCb         = 'callback';
% %--------------------------------------------------------
% %Attributs specifiques du composant
% ihm{end}.names          = {'Mercator','UTM','Lambert'};
% ihm{end}.nb_Col         = 8;
% ihm{end}.nb_lig         = 8;
% ihm{end}.msgEdit        = {'MsgMercator','MsgUTM','MsgLambert'};
% ihm{end}.enable_onglet  = {'on','off','on'};
% %--------------------------------------------------------
%
% this.ihm.Projection = create_component(cl_component,ihm);
%
% See also clc_onglet Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = clc_onglet(varargin)

%% D�finition de la structure et initialisation

this.globalPanel   = [];
this.names         = {''};
this.Onglets       = [];
this.onglet_parent = [];
this.nb_col        = [];
this.nb_lig        = [];
this.handles       = [];
this.enable        = [];
this.onglet_actif  = [];
this.enable_onglet = [];
this.msgEdit       = '';
this.tooltip       = '';

%% Super-classe

% if (nargin == 1) && isempty(varargin {1})
    composant = cl_component([]);
% else
%     composant = cl_component;
% end

%% Cr�ation de l'objet

this = class(this, 'clc_onglet', composant);

%% Pr�-nitialisation des champs

if (nargin == 1) && isempty(varargin {1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
