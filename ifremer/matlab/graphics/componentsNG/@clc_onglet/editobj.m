% Appel de la representation graphique de l'instance
%
% Syntax
%   this = editobj(this) 
%
% Input Arguments
%   this : instance de clc_label_value
%
% OUPUT PARAMETERS :
%   this : instance de clc_label_value
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Cr�ation de l'interface graphique

if ~is_edit(this)
    this.cl_component = editobj(this.cl_component);
    this = construire_ihm(this);
end
