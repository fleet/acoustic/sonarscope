% Lecture des attributs et proprietes de l'instance
%
% Syntax
%   varargout = get(this, ...)

function varargout = get(this, varargin)

varargout = {};

for k=1:length(varargin)
    switch varargin{k}
        case 'Names'
            varargout{end+1} = get_names(this); %#ok<AGROW>
        case 'Handles_panels'
            varargout{end+1} = get_handles_panels(this); %#ok<AGROW>
        case 'Handles_onglets'
            varargout{end+1} = get_handles_onglets(this); %#ok<AGROW>
        case{ 'componentVisible', 'componentEnable', 'componentName', ...
                'componentAnchor','componentParent' , 'componentUserName', 'componentUserCb' }
            varargout {end+1} = get(this.cl_component, varargin{k}); %#ok<AGROW>
        case 'globalPanel'
            varargout{end+1} = get_globalPanel(this); %#ok<AGROW>
        case 'onglet_parent'
            varargout{end+1} = get_onglet_parent(this); %#ok<AGROW>
        case 'onglet_actif'
            varargout{end+1} = get_onglet_actif(this); %#ok<AGROW>
        case 'msgEdit'
            varargout {end+1} = get_msg_edit(this); %#ok<AGROW>
        case 'enable_onglet'
            varargout{end+1} = get_enable_onglet(this); %#ok<AGROW>
        otherwise
            my_warndlg('clc_onglet/get : Invalid property name', 0);
    end
end
