% M�thode de r�cup�ration du flag Enable d'un onglet
%
% Syntax
%   this = clc_onglet(...)
%
% Name-Value Pair Arguments
%
%    names             : noms associes aux onglets
%    Onglets           : handles des onglets (pas des panels)
%    onglet_parent     : pour les sous-onglets c'est l'onglet parent
%    handles           : handles des panels (accessibles avec get(this,'Handles_panels'))
%    onglet_actif      : handle de l'onglet actif
%    nb_col            : nombre de colonnes dans le composant (determine la
%                        largeur des onglets)
%    nb_lig            : nombre de lignes dans le composant (determine la
%                        hauteur des onglets)
%    msgEdit           : messages de call back en cas de changement d'onglet
%    enable            : positionne l accessibilite de la valeur
%    componentName     : nom associe a ce composant
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%
% Output Arguments
%    this               : instance de clc_onglet
%
% EXAMPLE:
%
% ihm = {};
% %--------------------------------------------------------
% %Attributs generiques d'un composant
% ihm{end+1}.Style        = 'clc_onglet';
% ihm{end}.Name           = 'Projection';
% ihm{end}.Parent         = 'AncrePanelProjection';
% ihm{end}.UserName       = 'cla_carto';
% ihm{end}.UserCb         = 'callback';
% %--------------------------------------------------------
% %Attributs specifiques du composant
% ihm{end}.names          = {'Mercator','UTM','Lambert'};
% ihm{end}.nb_Col         = 8;
% ihm{end}.nb_lig         = 8;
% ihm{end}.msgEdit        = {'MsgMercator','MsgUTM','MsgLambert'};
% ihm{end}.enable_onglet  = {'on','off','on'};
% %--------------------------------------------------------
%
% this.ihm.Projection = create_component(cl_component,ihm);
% flag = get_enable_onglet(this.ihm.Projection)
%
% See also clc_onglet Authors
% Authors : LUR
% -------------------------------------------------------------------------

function enable_onglet = get_enable_onglet(this)

    enable_onglet = this.enable_onglet;

