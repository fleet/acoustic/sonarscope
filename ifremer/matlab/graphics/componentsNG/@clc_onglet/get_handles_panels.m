function handles = get_handles_panels(this)

GCF = gcf;
for i=1:length(this.names)
    handles{i} = findobj(GCF, 'tag', [get(this,'componentParent') ' Ancre_panel_onglet' num2str(i)]); %#ok<AGROW>
end