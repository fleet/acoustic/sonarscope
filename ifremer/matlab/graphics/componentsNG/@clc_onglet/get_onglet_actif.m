function actif = get_onglet_actif(this)

GCF = gcf;
for k=1:length(this.names)
    handles = findobj(GCF, 'tag',[get(this,'componentParent') ' Ancre_panel_onglet' num2str(k)]);
    state = get(handles,'visible');
    if strcmp(state,'on')
        actif = 1;
    end
end
