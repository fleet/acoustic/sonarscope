% Construction d'une call back
%
% Syntax
%   cb = built_cb(this, msgCb)
% 
% Input Arguments
%   this  : instance de cl_component
%   msgCb : message associe a la call back
%
% Output Arguments
%   cb : chaine de caracteres permettant d initialiser la call back
%
% See ALSO : cl_component Authors
% Authors  : DCF
% ----------------------------------------------------------------------------

function cb = built_cb_onglet(this, msgCb)

if ~iscell(msgCb)
    msgCb = {msgCb};
end

cb = [];

% ------------------------------------------------------------------------
% Ne peut construire une call back que si le nom de la classe utilisatrice
% ainsi que le nom de son manager sont connus

if ~isempty(get(this,'componentUserCb'))
    cb{end+1} = 'eval(''';
    cb{end+1} = get(this,'componentUserCb');
    cb{end+1} = '(';
    cb{end+1} = 'clc_onglet'; %this.componentUserName;
    cb{end+1} = '([])';
    for i=1:length(msgCb)
        cb{end+1} = ', ';
        cb{end+1} = '''''';
        cb{end+1} = msgCb{i};
        cb{end+1} = '''''';
    end
    
    % DEBUT : A l'essai
    cb{end+1} = ', ''''Interruptible'''', ''''off''''';
    cb{end+1} = ', ''''BusyAction'''', ''''cancel''''';
    % FIN : A l'essai
    
    cb{end+1} = '); '');';

    cb = strcat(cb{:});
    cb=cb{:};
end
