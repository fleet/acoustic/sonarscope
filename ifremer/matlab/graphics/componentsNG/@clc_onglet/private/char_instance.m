% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this) 
%
% Input Arguments
%   this : instance de clc_label_value
%


function str = char_instance(this)

% Maintenance Auto : try
    str = [];
    
    % ----------
    % Traitement
    for i=1:length(this.names)
        str{end+1} = sprintf('names         <-> %s', this.names{i} );
    end
    str{end+1} = sprintf('Onglets       <-> %d', this.Onglets );
    str{end+1} = sprintf('onglet_parent <-> %s', this.onglet_parent );
    str{end+1} = sprintf('nb_col        <-> %d', this.nb_col );
    str{end+1} = sprintf('nb_lig        <-> %d', this.nb_lig );
    str{end+1} = sprintf('handles       <-> %d', this.handles );
    
%     this.globalPanel        = [];
% this.names              = {''};
% this.Onglets            = [];
% this.onglet_parent      = [];
% this.nb_col             = [];
% this.nb_lig             = [];
% this.handles            = [];
    str{end+1} = sprintf('--- heritage cl_component ---');
    str{end+1} = char(this.cl_component);
    
    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
    % ---------------------------
    % Concatenation en une chaine
    
    str = char(strcat(str));
