% Creation de l'IHM d'une instance
%
% Syntax
%   b = construire_ihm(a)
%
% Input Arguments
%   a : Une instance de clc_onglet
%
% Output Arguments
%   b : Instance de clc_onglet modifiee
%
% See also clc_onglet Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation du panel du composant

this = creer_panel_global(this);

%% Cr�ation de l'accessibilit� des onglets

this = set_enable_onglet(this, this.enable_onglet, 0);
