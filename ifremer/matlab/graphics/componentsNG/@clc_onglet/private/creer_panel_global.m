% Creation des panels principaux de l'IHM d'une instance
%
% Syntax
%   b = creer_panel_global(a)
%
% Input Arguments
%   a : instance de la classe clc_onglet
%
% Output Arguments
%   b : instance de la classe clc_onglet
%
% See also clc_onglet Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = creer_panel_global(this)

ihm = [];

ihm{end+1}.Lig       = 1;
ihm{end}.Col         = 1;
ihm{end}.Style       = 'panel';
ihm{end}.Parent      = get(this,'componentParent');
ihm{end}.Tag         = [get(this,'componentParent') '_AncrePanelOnglets'];
ihm{end}.BorderType  = 'line';
ihm{end}.BorderWidth = 1;

%% Cr�ation du panel global

this.globalPanel = cl_panel([]);
this.globalPanel = set_handle_panel(this.globalPanel, get_anchor(this));
this.globalPanel = set_tab_uiComposant(this.globalPanel, ihm);

%% Cr�ation du sous-panel

this = creer_panel_onglets(this);
