% Creation des panels du composant clc_onglet
%
% this = creer_panel_onglets(this)
%
% Input Arguments
%    this : instance de clc_onglet
%
% Output Arguments
%    this : instance de clc_onglet
%
% See also clc_onglet Authors
% Authors : LUR
% -------------------------------------------------------------------------

function this = creer_panel_onglets(this)

%% Initialisation des variables

ihm = {};
taille = length(this.names);
Usr = get(this,'componentUserName');
msg = get(this,'msgEdit');

%% Cr�ation des onglets

for k=1:taille
    ihm{end+1}.Lig   = 1; %#ok<AGROW>
    ihm{end}.Col     = k;
    ihm{end}.Style   = 'togglebutton';
    ihm{end}.String  = this.names{k};
    ihm{end}.Tag     = [get(this,'componentParent') ' Ancre_onglet' num2str(k)];
    ihm{end}.Parent  = [get(this,'componentParent') '_AncrePanelOnglets'];
    if ~isempty(msg) && ~strcmp(msg{1},'test')
        ihm{end}.Callback = built_cb_onglet(this, {get(this,'componentParent'),num2str(taille),num2str(k), msg(k),Usr});
    else
        if k == 1
            msg{1} = 'test';
        end
        ihm{end}.Callback = built_cb_onglet(this, {get(this,'componentParent'),num2str(taille),num2str(k), msg(1),Usr});
    end

    if k == 1
        ihm{end}.value = 1;
    end
    if ~isempty(this.tooltip)
        ihm{end}.tooltip = this.tooltip{k};
    end

%% Cr�ation des panels

    ihm{end+1}.Lig          = 2:this.nb_lig;
    ihm{end}.Col            = 1:this.nb_col;
    ihm{end}.Style          = 'panel';
    ihm{end}.Tag            = [get(this,'componentParent') ' Ancre_panel_onglet' num2str(k)];
    ihm{end}.Parent         = [get(this,'componentParent') '_AncrePanelOnglets'];
    ihm{end}.HighlightColor = [1 1 1];
    if k == 1
        ihm{end}.visible = 'on';
    else
        ihm{end}.visible = 'off';
    end
end

if k ~= this.nb_col

    ihm{end+1}.Lig      = 1;
    ihm{end}.Col        = (k+1):this.nb_col;
    ihm{end}.Style      = 'panel';
    ihm{end}.Tag        = [get(this,'componentParent') ' Ancre_onglet_space'];
    ihm{end}.Parent     = [get(this,'componentParent') '_AncrePanelOnglets'];
    ihm{end}.Background = Gris;

end

this.globalPanel = cl_panel('nomPanel', [get(this,'componentParent') '_AncrePanelOnglets']);
this.globalPanel = set(this.globalPanel, 'ListeUiComposants', ihm);
