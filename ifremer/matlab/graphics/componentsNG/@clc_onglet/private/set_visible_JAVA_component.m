function set_visible_JAVA_component(hpanel, State)

handle = get(hpanel, 'children');
if ~isempty(handle)
    children = allchild(hpanel);

    for k=1:length(children)
        type = get(children(k), 'type');
        if strcmp(type, 'hgjavacomponent')
            set(handle, 'visible', State);
        else
            set_visible_JAVA_component(children(k), State)
        end
    end
end
