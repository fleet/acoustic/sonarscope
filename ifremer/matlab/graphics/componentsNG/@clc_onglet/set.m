% Modification des attributs et proprietes de l'instance
%
% Syntax
%   instance = set(this, varargin)

function this = set(this, varargin)

%% Lecture des arguments

[varargin, names] = getPropertyValue(varargin, 'names', []);
[varargin, nb_col] = getPropertyValue(varargin, 'nb_col', []);
[varargin, nb_lig] = getPropertyValue(varargin, 'nb_lig', []);
[varargin, onglet_parent] = getPropertyValue(varargin, 'onglet_parent', []);
[varargin, enable] = getPropertyValue(varargin, 'enable', []);
[varargin, msgEdit] = getPropertyValue(varargin, 'msgEdit', []);
[varargin, tooltip] = getPropertyValue(varargin, 'tooltip', []);
[varargin, enable_onglet] = getPropertyValue(varargin, 'enable_onglet', []);

%% Initialisation des attributs

if ~isempty(names)
    this = set_names(this, names);
end

if ~isempty(nb_col)
    this = set_nb_col(this, nb_col);
end

if ~isempty(nb_lig)
    this = set_nb_lig(this,nb_lig);
end

if ~isempty(onglet_parent)
    this = set_onglet_parent(this,onglet_parent);
end

if ~isempty(enable)
    this = set_enable(this,enable);
end

if ~isempty(msgEdit)
    this = set_msg_edit(this,msgEdit);
end

if ~isempty(tooltip)
    this = set_tooltip(this,tooltip);
end

if ~isempty(enable_onglet)
    this.enable_onglet =enable_onglet;
end

%% Traitement des message de la super classe
% Traitement specifique des composants agreges

if ~isempty(varargin)
    this.cl_component = set(this.cl_component, varargin{:});
end

%
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
