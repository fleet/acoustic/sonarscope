% Accesseur en ecriture de l'accessibilite de l instance
% 
% Syntax
%   this = set_enable(instance, enable)
%
% Input Arguments
%   this : instance de cl_panel
%   enable   : flag de enable
%
% Output Arguments
%   this : instance de cl_panel initialisee
% ----------------------------------------------------------------------------

function this = set_enable(this, enable)

% ---------------------------------
% Initialisation de l'accessibilite

%if isempty(this.enable) || (strcmp(enable, this.enable) == 0)
    this.enable = enable;
    set_enable(this.globalPanel,enable);
%end
