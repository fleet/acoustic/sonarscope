% Accesseur en ecriture de l'accessibilite des boutons des onglets du
% composant clc_onglet
% 
% Syntax
%   this = set_enable_onglet(this,enable,number)
%
% Input Arguments
%   this : instance de clc_onglet
%   enable: flag de enable, 'on' ou 'off'
%   number: numero de l'onglet concerne
% 
% EXEMPLE:
%  this.ihm.cpnonglet=set_enable_onglet(this.ihm.cpnonglet,0,3);
%  (exemple pour @cla_demo_onglet)
% ou alors un tableau de 'on' et 'off' dans ce cas mettre a 0 la propriete "number"
% this.ihm.cpnonglet=set_enable_onglet(this.ihm.cpnonglet,{0, 1, 0},0);
%
% Output Arguments
%   this : instance de clc_onglet initialisee
%
% AUTHOR: LUR 26/11/07
% ----------------------------------------------------------------------------

function this = set_enable_onglet(this,enable,number)
 % ---------------------------------
 % Recuperation des handles des onglets
 onglets=get_handles_onglets(this);
 % ---------------------------------
 % Application de l'accessibilité
    
if (number~=0)
    set(onglets{number},'Enable',enable);  
    this.enable_onglet{number}=enable;
else
    this.enable_onglet=enable;
    for i=1:length(enable)
        if ~isempty(enable(i))
            if strcmp(enable{i},'1')
                set(onglets{i},'Enable','on');
            else
                set(onglets{i},'Enable','off');
            end
        end
    end
end