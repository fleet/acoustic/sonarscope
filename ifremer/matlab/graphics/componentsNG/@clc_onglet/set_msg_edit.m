function this = set_msg_edit(this, msgEdit)

this.msgEdit = msgEdit;

% -------------------------------------------------
% Si affichage en cours, mise a jour de l affichage

if is_edit(this)
    h = get(this, 'handle');
    if this.editable
        set(h, 'Callback', built_cb(this, this.msgEdit));
    else
        set(h, 'Callback', []);
    end
end
