% Initialisation du editable
%
% Syntax
%   this = set_editable(this, editable)
%
% Input Arguments
%   this : instance de clc_label_editable
%   editable : editable
%
% Output Arguments
%   this : instance de clc_label_editable initialisee
%
% See also clc_label_value Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = set_names(this, names)

this.names = names;
