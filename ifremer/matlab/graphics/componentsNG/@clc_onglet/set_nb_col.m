

function this = set_nb_col(this, nbCol)


if isempty(nbCol)
    this.nb_col = length(this.names);
else
    this.nb_col = nbCol;
end
