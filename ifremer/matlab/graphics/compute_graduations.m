% compute_graduations([0 10])
% compute_graduations([0 35])
% compute_graduations([5 50])
% compute_graduations([-5 130])
% compute_graduations([120 130])
% compute_graduations([5 130])
% compute_graduations([2 4])
% compute_graduations([-8 -4])
% compute_graduations([25 50])
% compute_graduations([25 150])
% compute_graduations([20 30])
% compute_graduations([-320 0])
% compute_graduations([-104.13282 -90.151375])
% compute_graduations([90.151375 104.13282])

function Val = compute_graduations(XLim)

% TODO : regarder round(XXX, 1, 'significant')


% nb1     = 1;
% nbint1  = 5;
% nb2     = 2;
% nbint2  = 10;
% nb3     = 5;
% nbint3  = 15;
% [varargin, NbGrad] = getPropertyValue(varargin, 'NbGrad', 10);

Val     = [];
arrondi = 0.00005;

%% calcul du minimum et du maximum

if XLim(2)>0
    Xmax = ceil(exp((log10(abs(XLim(2)))-floor(log10(abs(XLim(2)))))*log(10))-arrondi)*exp((floor(log10(abs(XLim(2)))))*log(10));
else
    if XLim(2)<0
        Xmax = sign(XLim(2))* floor(exp((log10(abs(XLim(2)))-floor(log10(abs(XLim(2)))))*log(10)))*exp((floor(log10(abs(XLim(2)))))*log(10));
    else
        Xmax = 0;
    end
end
if XLim(1)>0
    Xmin = floor(exp((log10(abs(XLim(1)))-floor(log10(abs(XLim(1)))))*log(10)))*exp((floor(log10(abs(XLim(1)))))*log(10));
else
    if XLim(1)<0
        Xmin = sign(XLim(1))*ceil(exp((log10(abs(XLim(1)))-floor(log10(abs(XLim(1)))))*log(10))-arrondi)*exp((floor(log10(abs(XLim(1)))))*log(10));
    else
        Xmin = 0;
    end
end

if sign(Xmax) == sign(Xmin)
    if exp((floor(log10(abs(Xmax))))*log(10)) > exp((floor(log10(abs(Xmin))))*log(10))
        Xmin = 0;
    end
    if exp((floor(log10(abs(Xmax))))*log(10)) < exp((floor(log10(abs(Xmin))))*log(10))
        Xmax = 0;
    end
end
if Xmin == 1
    Xmin = 0;
end

if exp((floor(log10(abs(Xmin))))*log(10)) > exp((floor(log10(abs(Xmax))))*log(10))
    if Xmax > 0
        Xmax = exp((floor(log10(abs(Xmax))+1))*log(10));
    end
    if Xmax < 0
        Xmax = sign(Xmax)*exp((floor(log10(abs(Xmax))))*log(10));
    end
end


if sign(Xmax) == sign(Xmin) %|| Xmin ==0 || Xmax ==0
    x = abs(Xmax - Xmin);
else
    if exp(floor(log10(Xmax))*log(10)) > exp(floor(log10(abs(Xmin)))*log(10))
        x = Xmax;
    else
        x = abs(Xmin);
    end
end

%% calcul de la longueur de l'intervalle

if log10(x) <= floor(log10(x))
    x = 2*exp(floor(log10(x))*log(10));
end
Nbintervalles = ceil(x/exp(floor(log10(x))*log(10)));

while Nbintervalles < 5
    Nbintervalles = 2*Nbintervalles;
end

% while Nbintervalles > 20
%     Nbintervalles = 2/Nbintervalles;
% end

% if parity(Nbintervalles) == 0
%     Nbintervalles = Nbintervalles+1;
% end

Lintervalle = x/Nbintervalles;

%% determination des valeurs de sortie (Val)

if sign(Xmax) == sign(Xmin) || Xmax == 0 || Xmin == 0
    vali = Xmin;
    while vali+Lintervalle <= XLim(1)+arrondi
        vali = vali+Lintervalle;
    end
    %     Xmin = vali;
    Val  = [Val;vali];
    while max(Val) <= XLim(2)-arrondi
        vali = vali+Lintervalle;
        if log10(abs(vali)) < -10
            vali = 0;
        end
        Val = [Val; vali]; %#ok<AGROW>
    end
    %     Xmax = vali;
else
    vali = 0;
    Val  = [Val;0];
    while min(Val) >= XLim(1)+arrondi
        vali = vali-Lintervalle;
        Val = [Val; vali]; %#ok<AGROW>
    end
    %     Xmin = vali;
    vali = 0;
    while max(Val) <= XLim(2)-arrondi
        vali = vali+Lintervalle;
        Val  = [Val; vali]; %#ok<AGROW>
    end
    %     Xmax = vali;
    Val = sort(Val);
end
