% FONCTION MORIBONDE :
% Creation d'un ensemble d'elements graphiques (uicontrols)
%
% Input Arguments
%    ihm       : Structure decrivant les proprietes des uicontrols
%    Champs de cette structure
%     ihm.Lig             : Lignes de placement de l'element graphique (cellule excel)
%     ihm.Col             : Colonnes de placement de l'element graphique (cellule excel)
%     ihm.Callback        : Callback associe au bouton
%     ihm.String          : String associe au bouton
%     ihm.BackgroundColor : BackgroundColor associe au bouton
%     ihm.TooltipString   : TooltipString associe au bouton
%     ihm.CData           : CData associe au bouton
%     ihm.FontWeight      : FontWeight associe au bouton
%     ihm.Tag             : Tag associe au bouton
%     ihm.ButtonDownFcn   : ButtonDownFcn associe au bouton
%     ihm.Enable          : Enable associe au bouton
%     ihm.Value           : Value associee au bouton
%     ihm.UserData        : UserData associee au bouton
%     ihm.Visible         : Visibilite
%
% Output Arguments
%   theUicontrols : Handles des uicontrols.
%
% Examples
%
% See also ASlayout Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function theUicontrols = createUicontrols(ihm)

if nargin == 0
    help createUicontrols
    return
end


% ------------------------------------------------------------------------
% Creation des uicontrols a partir des tableaux de proprietes des boutons

for i = 1:length(ihm)
    if ~isfield(ihm{i}, 'BackgroundColor') | isempty(ihm{i}.BackgroundColor)
        if isfield(ihm{i}, 'Style') & ~isempty(ihm{i}.Style)
            switch lower(ihm{i}.Style)
    			case 'listbox'
    				ihm{i}.BackgroundColor = Gris;
    			case 'frame'
    				ihm{i}.BackgroundColor = Gris;
    			case 'checkbox'
    				ihm{i}.BackgroundColor = Gris;
    			case 'pushbutton'
    				ihm{i}.BackgroundColor = Gris;
    			case 'togglebutton'
    				ihm{i}.BackgroundColor = Gris;
    			case 'slider'
    				ihm{i}.BackgroundColor = Gris;
    			case 'popupmenu'
    				ihm{i}.BackgroundColor = GrisClair;
    			case 'text'
    				ihm{i}.BackgroundColor = Gris;
    			case 'edit'
    				ihm{i}.BackgroundColor = [1 1 1]; %GrisClair;
    			otherwise
    				disp(['Pas de couleur attribuee pour :' ihm{i}.Style])
    				ihm{i}.BackgroundColor = Gris;
            end
        else
            ihm{i}.BackgroundColor = Gris;
        end
    end
    if ~isfield(ihm{i}, 'String') | isempty(ihm{i}.String)
        ihm{i}.String = '';
    end

    theUicontrols(i) = uicontrol(	'Style', ihm{i}.Style, ...
    	'String', ihm{i}.String, 'BackgroundColor', ihm{i}.BackgroundColor);

    if isfield(ihm{i}, 'TooltipString') & ~isempty(ihm{i}.TooltipString)
        set(theUicontrols(i), 'TooltipString', ihm{i}.TooltipString);
    end
    if isfield(ihm{i}, 'Callback') & ~isempty(ihm{i}.Callback)
        set(theUicontrols(i), 'Callback', ihm{i}.Callback);
    end
    if isfield(ihm{i}, 'CData') & ~isempty(ihm{i}.CData)
        set(theUicontrols(i), 'CData', ihm{i}.CData);
    end
    if isfield(ihm{i}, 'FontWeight') & ~isempty(ihm{i}.FontWeight)
        set(theUicontrols(i), 'FontWeight', ihm{i}.FontWeight);
    end
    if isfield(ihm{i}, 'Tag') & ~isempty(ihm{i}.Tag)
        set(theUicontrols(i), 'Tag', ihm{i}.Tag);
    end
    if isfield(ihm{i}, 'ButtonDownFcn') & ~isempty(ihm{i}.ButtonDownFcn)
        set(theUicontrols(i), 'ButtonDownFcn', ihm{i}.ButtonDownFcn);
    end
    if isfield(ihm{i}, 'Enable') & ~isempty(ihm{i}.Enable)
        set(theUicontrols(i),  'Enable', ihm{i}.Enable);
    end
    if isfield(ihm{i}, 'Value') & ~isempty(ihm{i}.Value)
        set(theUicontrols(i),  'Value', ihm{i}.Value);
    end
    if isfield(ihm{i}, 'UserData') & ~isempty(ihm{i}.UserData)
        set(theUicontrols(i),  'UserData', ihm{i}.UserData);
    end
    if isfield(ihm{i}, 'Visible') & ~isempty(ihm{i}.Visible)
        set(theUicontrols(i),  'Visible', ihm{i}.Visible);
    end
    if isfield(ihm{i}, 'FontName') & ~isempty(ihm{i}.FontName)
        set(theUicontrols(i),  'FontName', ihm{i}.FontName);
    end
    if isfield(ihm{i}, 'FontWeight') & ~isempty(ihm{i}.FontWeight)
        set(theUicontrols(i),  'FontWeight', ihm{i}.FontWeight);
    end
    if isfield(ihm{i}, 'FontAngle') & ~isempty(ihm{i}.FontAngle)
        set(theUicontrols(i),  'FontAngle', ihm{i}.FontAngle);
    end
    if isfield(ihm{i}, 'FontSize') & ~isempty(ihm{i}.FontSize)
        set(theUicontrols(i),  'FontSize', ihm{i}.FontSize);
    end
    if isfield(ihm{i}, 'ForegroundColor') & ~isempty(ihm{i}.ForegroundColor)
        set(theUicontrols(i),  'ForegroundColor', ihm{i}.ForegroundColor);
    end
    if isfield(ihm{i}, 'Interruptible') & ~isempty(ihm{i}.Interruptible)
        set(theUicontrols(i),  'Interruptible', ihm{i}.Interruptible);
    end
end
