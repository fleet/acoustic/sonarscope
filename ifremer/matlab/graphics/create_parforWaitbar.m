function [h, D] = create_parforWaitbar(N, Title)

D = parallel.pool.DataQueue;

w = warning;
warning('Off');

% Title = strrep(Title, '\', '/');

h = waitbar(0, Title);

hTitre = get(findobj(h, 'Type', 'axes'), 'Title');
set(hTitre, 'Interpreter', 'none', 'String', Title);

warning(w);

afterEach(D, @nUpdateWaitbar);
p = 1;

    function nUpdateWaitbar(~)
        waitbar(p/N, h);
        p = p + 1;
    end
end