% Cr�ation d'une waitbar
%
% Syntax
%   hw = create_waitbar(str, ...)
% 
% Input Arguments 
%   str : Intitule de la fen�tre
%
% Name-Value Pair Arguments
%   N      : nombre de pas (si <= 1 alors hw = [])
%   Entete : (Defaut : 'Entete', 'IFREMER - SonarScope')
% 
% Output Arguments 
%   hw : Handle
% 
% Examples 
%   N = 1000
%   str1 = 'Traitement en cours';
%   str2 = 'Processing';
%   hw = create_waitbar(Lang(str1,str2), 'N', N);
%   for k=1:N
%       my_waitbar(k, N, hw)
%   end
%   my_close(hw)% Or my_close(hw, 'MsgEnd')
%
% See also create_waitbar my_waitbar close_waitbar waitbarMsg Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function hw = create_waitbar(str, varargin)

[varargin, N] = getPropertyValue(varargin, 'N', []);

if ~isempty(N) && (N <= 1)
    hw = [];
    return
end

[varargin, NoWaitbar] = getPropertyValue(varargin, 'NoWaitbar', 0);
if NoWaitbar == 1
    hw = [];
    return
end

[varargin, Entete] = getPropertyValue(varargin, 'Entete', 'IFREMER - SonarScope'); %#ok<ASGLU>

S = warning;
warning off
hw = waitbar(0, str, 'CreateCancelBtn', 'setappdata(gcbf,''canceling'',1)');
hTitre = get(findobj(hw, 'Type', 'axes'), 'Title');
set(hTitre, 'Interpreter', 'none', 'String', str);
set(hw, 'Name', Entete, 'HandleVisibility', 'Off')

set(hw, 'CloseRequestFcn', @ConfirmationArret)

hc = findobj(hw, 'Type', 'uicontrol');
set(hc, 'Callback', @ConfirmationArret)

warning(S)

    function ConfirmationArret(varargin)
        str1 = 'Voulez-vous vraiment fermer cette fen�tre ? Sachez que cela risque de mettre fin au traitement en cours.';
        str2 = 'Do yo really want to close this waitbar ? The current processing may be cancelled.';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if flag && (rep == 1)
            set(hw, 'CloseRequestFcn', [])
            % TODO : pourquoi le close ne fonctionne pas ????
%             close(hw);
            if ishandle(hw)
                delete(hw);
            end
        end
    end
end
