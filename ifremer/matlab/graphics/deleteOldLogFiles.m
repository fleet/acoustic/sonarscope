function deleteOldLogFiles

logFileId = getLogFileId;
nomDir = my_tempdir;
SemaineDerniere = datetime('today') - days(7);
AvantHier       = datetime('today') - days(2);
X = dir(nomDir);
for k=1:length(X)
    [~, nomFic, ext] = fileparts(X(k).name);
    if contains(nomFic, 'SonarScopeLogFile') && strcmp(ext, '.txt')
        if length(nomFic) >= 26
            TimeFic = datetime(nomFic(19:26), 'InputFormat', 'yyyyMMdd');
            if (TimeFic < SemaineDerniere) || ((TimeFic < AvantHier) && (X(k).bytes == 0))
                try
                    delete(fullfile(nomDir, X(k).name))
                    msg = sprintf('"%s" deleted.', X(k).name);
                    logFileId.warn('deleteOldLogFiles', msg);
                catch %#ok<CTCH>
                    msg = sprintf('"%s" could not be deleted.', X(k).name);
                    logFileId.warn('deleteOldLogFiles', msg);
                end
            end
        end
    end
end
