% Derivation d'une courbe dans une nouvele figure
%
% Syntax
%   derivateCourbe(hIn)
%
% Input Arguments
%   hIn : Handle de la courbe � exporter
%
% Examples
%   figure;
%   hIn = plot(1:10, 'r+');
%   derivateCourbe(hIn)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function derivateCourbe(hIn)

hAxeIn = get(hIn, 'parent');
Titre = get(get(hAxeIn, 'Title'), 'String');

XData = get(hIn, 'XData');
YData = get(hIn, 'YData');
ZData = get(hIn, 'ZData');

figure;
if isempty(ZData)
    h = PlotUtils.createSScPlot(XData, gradient(YData));
else
    h = PlotUtils.createSScPlot(XData, gradient(YData), ZData);
end
grid on;
title(['Derivation of ' Titre(1,:)], 'Interpreter', 'none')

Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'
    'ButtonDownFcn'};

for i=1:length(Proprietes)
    P = get(hIn, Proprietes{i});
    set(h, Proprietes{i}, P)
end

CallbackSelfCorrelation = 'traiter_signal(get(get(gca, ''children''), ''XData''), get(get(gca, ''children''), ''YData''), get(get(gca, ''Title''), ''String''), ''Process'', 5);';
uimenu(cmenu, 'Text', 'Self-Correlation', 'Separator', 'on', 'Callback', CallbackSelfCorrelation);



%{
             EraseMode: 'normal'
       MarkerEdgeColor: 'auto'
       MarkerFaceColor: 'none'
          BeingDeleted: 'off'
              Children: [0x1 double]
              Clipping: 'on'
             CreateFcn: []
             DeleteFcn: []
            BusyAction: 'queue'
      HandleVisibility: 'on'
               HitTest: 'on'
         Interruptible: 'on'
              Selected: 'off'
    SelectionHighlight: 'on'
                   Tag: ''
                  Type: 'line'
         UIContextMenu: 1.5200e+003
              UserData: []
               Visible: 'on'
                Parent: 1.1700e+003
           DisplayName: ''
             XDataMode: 'manual'
           XDataSource: ''
           YDataSource: ''
           ZDataSource: ''
%}
