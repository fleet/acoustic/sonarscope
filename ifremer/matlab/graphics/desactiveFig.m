% Desactivation de tous les uicontrols d'une figure
%
% Syntax
%   [bouton, graphes] = desactiveFig
%
% Name-only Arguments
%   LetAxe : Pour laisser les axes
%
% See also activeFig Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function varargout = desactiveFig(varargin)

%fonction qui rend inactifs tous les boutons d'une figure
[varargin, LetAxe] = getFlag(varargin, 'LetAxe'); %#ok<ASGLU>

h0 = gcf;
Children = get(h0, 'Children');
axes = findobj(h0, 'Type', 'axes');
bouton = [];
% graphes = [];
n = 0;
m =0;
graphesCourbes = [];
graphesAxes = [];

for i=1:length(Children)
	if isempty(find( axes == Children(i), 1) )
		m = m+1;
		set( Children(i) , 'Enable', 'off');
      bouton(m) =  Children(i);
 	else
		n = n+1;
		graphesAxes(n) =   axes(n);
		tempo = get(axes(n), 'Children');
		graphesCourbes = [graphesCourbes;  tempo(:)];
	end
end

if ~LetAxe
	graphes = [graphesAxes(:); graphesCourbes];
	set( graphes, 'Visible', 'off');
	if nargout==2
		varargout{1} = bouton;
		varargout{2} = graphes;
	end
else
	if nargout==2
		varargout{1} = bouton;
		varargout{2} = [];
	end
end
