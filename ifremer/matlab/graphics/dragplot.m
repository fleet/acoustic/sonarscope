% DRAGPLOT allows plotted lines to be dragged from one figure window and
% dropped into another.  Type DRAGPLOT to enable lines in the CURRENT
% figure window to be moved.  Press and hold the left mouse button to
% select a line object and drag it to the new figure window.  When you
% release the mouse button, the line will be moved.  It will be removed
% from the old window. If you hold down the CRTRL key while you drag and
% drop, the line object will be copied to the new window.  If you drop
% the line object outside of a figure window, it will be deleted. You
% can enable DRAGPLOT on as many open figure windows as you desire.
% The DRAGPLOT feature is suppressed while the figure window is in zoom
% mode.  It will be enabled when zoom is turned off.
% MECANISME Ctrl INVERSE PAR JMA
%
% Example:
%   figure; plot(rand(1,30),'r'); dragplot
%   figure; plot(cos(2*pi*[0:32]/11),'b'); dragplot
% Now drag and drop the cosine curve or the random plot from one figure
% window to the other.

% NOTES:
% This script modifies the "WindowButtonDownFcn" and "WindowButtonUpFcn"
% properties, and creates an application data object named "dragplot" in
% the figure window to which it is applied.
%
% If the receiving figure contains subplots, the CURRENT subplot will
% receive the dragged plot.  If you desire to drop the plot into a
% different subplot, issue a SUBPLOT command to select the desired subplot
% before dragging and dropping.

% Version 1.0
% Mark W. Brown
% mwbrown@ieee.org

function dragplot(varargin)

[varargin, GCF] = getPropertyValue(varargin, 'Fig', gcf); %#ok<ASGLU>

% windowbuttondownfcnEnPlace = get(GCF, 'windowbuttondownfcn');
set(GCF, 'windowbuttondownfcn', @windowbuttondownfcn)
set(GCF, 'windowbuttonupfcn',   @callback_DragplotUp)
hText = findobj(allchild(gca), 'Type', 'Text');
for k=1:length(hText)
    set(hText(k), 'ButtonDownFcn', @StringDownFcn)
end

    function StringDownFcn(varargin)
        GCBO = gcbo;
        String = get(GCBO, 'String');
        str1 = 'Ecrivez votre nouveau titre ici';
        str2 = 'Write your new title here';
        nbLines = size(String,1);
        for kLine=1:(nbLines-1)
            L = sprintf('%s\n', String(kLine,:));
            strLines(kLine,1:length(L)) = L; %#ok<AGROW>
        end
        L = String(nbLines,:);
        strLines(nbLines,1:length(L)) = L;
        String = my_inputdlg(Lang(str1,str2), strLines, 'numlines', nbLines);
        if ~isempty(String)
            set(GCBO, 'String', String{1})
        end
        return
    end

%{
function fs = getPointFontFromHandle(fhandle)
tempunits = get(fhandle,'FontUnits');
set(fhandle, 'FontUnits', 'points');
fs = [];
try
fs.FontName = get(fhandle, 'FontName');
fs.FontWeight = get(fhandle, 'FontWeight');
fs.FontAngle = get(fhandle, 'FontAngle');
fs.FontUnits = get(fhandle, 'FontUnits');
fs.FontSize = get(fhandle, 'FontSize');
catch ex %#ok<NASGU>
end
set(fhandle, 'FontUnits', tempunits);
end
%}

    function windowbuttondownfcn(varargin)
        GCO  = gco;
        GCF2 = gcf;
        
        t   = get(GCO, 'type');
        sel = get(GCF2, 'selectionType'); %R�cup�ration de la touche press�e 'undocumented' GLA
        
        if isempty(t)
            return
        end
        
        % Sinon on a cliqu� sur une courbe ou un axes
        GCA = gca;
        switch t
            case 'line'
                set(GCF2, 'pointer', 'fleur');
                h = findobj(GCA, 'type', 'line', '-and', 'selected', 'on'); % On r�cup�re toutes les courbes s�l�ctionn�es
                if ~isempty(h) && ~strcmp(sel, 'alt') % = ctrl + click
                    set(h,'selected', 'off');
                    h = [];
                end
                h = unique([h(:)' GCO]);
                set(h,'selected', 'on');
                setappdata(GCF2, 'dragplot', [h GCF2]);
                
            case 'axes'
                h = findobj(GCA, 'type', 'line');
                set(h, 'selected', 'off'); %GLA
                setappdata(GCF2, 'dragplot', [h(:)' GCF2]);
              
            case 'image'
                set(GCF2, 'pointer', 'fleur');
                h = findobj(GCA, 'type', 'image', '-and', 'selected', 'on'); % On r�cup�re toutes les courbes s�l�ctionn�es
                if ~isempty(h) && ~strcmp(sel, 'alt') % = ctrl + click
                    set(h,'selected', 'off');
                    h = [];
                end
                h = unique([h(:)' GCO]);
                set(h,'selected', 'on');
                setappdata(GCF2, 'dragplot', [h GCF2]);
        end
    end
end
