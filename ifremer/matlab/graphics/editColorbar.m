% Creation d'un menu interactif donnant le choix de la palette de couleurs
% et de la gestion des NaN
% Callback associe a une colorbar creee par la fonction AScolorbar.
%
% Syntax
%   editColorbar;   % si appelee dans une callback
%   editColorbar(handleImage, hColobarAxe)
%
% Input Arguments 
%   handleImage : handle de l'image
%   hColobarAxe : handle de le la colorbar
%
% Output Arguments
%   consulter le help de la fonction colorbar
% 
% Examples
%   imagesc(rand(10));
%   AScolorbar;
%
% See also AScolorbar Authors
% Authors : JMA
% VERSION  : $Id: editColorbar.m,v 1.3 2002/06/04 14:42:25 augustin Exp $
%-------------------------------------------------------------------------------

function editColorbar(varargin)

argument = varargin{1};

if argument > 0
    
    % -------------------------------------------------
    % Appel de la fonction pour creation de l'interface
    
    handleImage = varargin{1};
    hImageAssociee = get(handleImage, 'Children');
    
    CLimOrigine = get(handleImage, 'CLim');
    
    hColobarAxe = varargin{2};
    figParent = gcf;
    
    % ---------------------------------
    % Controle si la figure existe deja. Ceci meriterait d'etre simplifie car c'est plutot complique
    
    listeFigures = get(0, 'Children');
    for i=1:length(listeFigures)
        if listeFigures(i) ~= figParent
            UserDataFigure = get(listeFigures(i), 'UserData');
            [handleImageFigure, indice] = getFromUserData( 'handleImage', UserDataFigure );
            if ~isempty(indice)
                hImageAssocieeFigure = get(handleImageFigure, 'Children');
                nb_hImageAssocieeFigure = length(hImageAssocieeFigure);
                if nb_hImageAssocieeFigure == 1
                    tmp{1} = hImageAssocieeFigure;
                else
                    tmp = hImageAssocieeFigure;
                end
                if ~iscell(tmp)
                    tmp = {tmp};
                end
                for j=1:nb_hImageAssocieeFigure
                    if tmp{j} == hImageAssociee
                        figure(listeFigures(i));
                        map = getFromUserData( 'map', UserDataFigure);
                        set(figParent, 'Colormap', map);
                        return
                    end
                end
            end
        end
    end
    
    % ---------------------
    % Creation de la figure	
    
    handleFigBoutons = figure;
    
    largeur = 140;
    hauteur = 23;
    pas = 25;
    colonne = 5;
    ligne = 125;
    
    % ------------------------------------
    % Affichage du nom de l'image associee
    
    titreImage = get(get(handleImage, 'Title'), 'String');
    titre = sprintf('%s', titreImage);
    uicontrol('units', 'pixels', 'string', titre, ...
        'style', 'text', 'position', [colonne, ligne, (2*largeur + 1*5), hauteur]);
    
    % ------------------------------------------------------------------------------
    % Choix de la table de couleurs
    
    colonne = 5;
    ligne = 100;
    uicontrol('units', 'pixels', 'string', 'Couleur', ...
        'style', 'text', 'position', [colonne, ligne, largeur, hauteur]);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-1)');
    handleButton(1, 1) = uicontrol('units', 'pixels', 'string', 'Inv. Video', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-2)');
    handleButton(2, 1) = uicontrol('units', 'pixels', 'string', 'jet', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-3)');
    handleButton(3, 1) = uicontrol('units', 'pixels', 'string', 'gray', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-4)');
    handleButton(4, 1) = uicontrol('units', 'pixels', 'string', 'bone', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-5)');
    handleButton(5, 1) = uicontrol('units', 'pixels', 'string', 'copper', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-6)');
    handleButton(6, 1) = uicontrol('units', 'pixels', 'string', 'pink', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0);
    
    % ------------------------------------------------------------------------------
    % Choix de la couleur pour les non-valeurs
    
    colonne = colonne + largeur + 5;
    ligne = 100;
    uicontrol('units', 'pixels', 'string', 'Non valeurs', ...
        'style', 'text', 'position', [colonne, ligne, largeur, hauteur]);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-7)');
    handleButton(1, 2) = uicontrol('units', 'pixels', 'string', 'Couleur particuliere', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0);
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-8)');
    handleButton(2, 2) = uicontrol('units', 'pixels', 'string', 'Blanc', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 1, 'Visible', 'off');
    
    ligne = ligne - pas;
    commande = sprintf('editColorbar(-9)');
    handleButton(3, 2) = uicontrol('units', 'pixels', 'string', 'Noir', 'style',...
        'radio', 'callback', commande,...
        'position', [colonne, ligne, largeur, hauteur], 'value', 0, 'Visible', 'off');
    
    % ------------------------------------------------------------------------------
    % Partie de peche a la ligne : on tente de retrouver dans quelle configuration on
    % se trouve au cas ou la fonction est rappelee
    
    map = get(figParent, 'Colormap');
    if isequal(size(map), size(jet(256)))
        set(handleButton(1, 2), 'Value', 0);
        set(handleButton(2:3, 2), 'Visible', 'off');
    else
        set(handleButton(1, 2), 'Value', 1);
        set(handleButton(2:3, 2), 'Visible', 'on');
        CoulNaN = map(1,:);
        map = map(2:end,:);
        if isequal(CoulNaN, [0 0 0])
            set(handleButton(2, 2), 'Value', 0);
            set(handleButton(3, 2), 'Value', 1);
        else
            set(handleButton(2, 2), 'Value', 1);
            set(handleButton(3, 2), 'Value', 0);
        end
    end
    
    if isequal(map, jet(256))
        set(handleButton(1, 1), 'Value', 0);
        set(handleButton(2, 1), 'Value', 1);
    elseif isequal(map, gray(256))
        set(handleButton(1, 1), 'Value', 0);
        set(handleButton(3, 1), 'Value', 1);
    elseif isequal(map, bone(256))
        set(handleButton(1, 1), 'Value', 0);
        set(handleButton(4, 1), 'Value', 1);
    elseif isequal(map, copper(256))
        set(handleButton(1, 1), 'Value', 0);
        set(handleButton(5, 1), 'Value', 1);
    elseif isequal(map, pink(256))
        set(handleButton(1, 1), 'Value', 0);
        set(handleButton(6, 1), 'Value', 1);
    elseif isequal(flipud(map), jet(256))
        set(handleButton(1, 1), 'Value', 1);
        set(handleButton(2, 1), 'Value', 1);
    elseif isequal(flipud(map), gray(256))
        set(handleButton(1, 1), 'Value', 1);
        set(handleButton(3, 1), 'Value', 1);
    elseif isequal(flipud(map), bone(256))
        set(handleButton(1, 1), 'Value', 1);
        set(handleButton(4, 1), 'Value', 1);
    elseif isequal(flipud(map), copper(256))
        set(handleButton(1, 1), 'Value', 1);
        set(handleButton(5, 1), 'Value', 1);
    elseif isequal(flipud(map), pink(256))
        set(handleButton(1, 1), 'Value', 1);
        set(handleButton(6, 1), 'Value', 1);
    else
        my_warndlg('editColorbar : Configuration non trouvee', 1);
    end
    
    
    % ------------------------------------------------------------------------------
    % Stockage des infos dans le UserData de la figure
    
    majFig(handleFigBoutons);
    
    UserData = [];
    putInUserData( 'figParent', figParent, UserData );
    putInUserData( 'handleImage', handleImage, UserData );
    putInUserData( 'hColobarAxe', hColobarAxe, UserData );
    putInUserData( 'handleButton', handleButton, UserData );
    putInUserData( 'CLimOrigine', CLimOrigine, UserData );
    putInUserData( 'map', map, UserData );
    set(handleFigBoutons, 'UserData', UserData);
else
    
    % -----------------------------------------
    % Appel de la fonction en tant que Callback
    
    UserData		= get(gcf, 'UserData');
    figParent		= getFromUserData( 'figParent', UserData );
%     hColobarAxe		= getFromUserData( 'hColobarAxe', UserData );
    handleButton	= getFromUserData( 'handleButton', UserData );
    CLimOrigine		= getFromUserData( 'CLimOrigine', UserData );
    map				= getFromUserData( 'map', UserData );
    set(figParent, 'Colormap', map);
    
    handleImage	= getFromUserData( 'handleImage', UserData );
%     hImageAssociee = get(handleImage, 'Children');
    
    CoulNaN = [];
    if get(handleButton(1,2), 'Value')	% Couleur particuliere pour les NaN
        if get(handleButton(2,2), 'Value')	% Couleur blanche
            CoulNaN = [1 1 1];
        else
            CoulNaN = [0 0 0];
        end
    end
    
    switch argument
    case -1
        map = get(figParent, 'Colormap');
        if get(handleButton(1,2), 'Value')
            map = map(2:end,:);
        end
        map = flipud(map);
        set(figParent, 'Colormap', [CoulNaN; map]);
        
    case -2	% Couleur jet
        set(handleButton(2:6, 1), 'Value', 0);
        set(gcbo, 'Value', 1);
        map = jet(256);
        set(figParent, 'Colormap', [CoulNaN; map]);
        
    case -3	% Couleur gray
        set(handleButton(2:6, 1), 'Value', 0);
        set(gcbo, 'Value', 1);
        map = gray(256);
        set(figParent, 'Colormap', [CoulNaN; map]);
        
    case -4	% Couleur bone
        set(handleButton(2:6, 1), 'Value', 0);
        set(gcbo, 'Value', 1);
        map = bone(256);
        set(figParent, 'Colormap', [CoulNaN; map]);
        
    case -5	% Couleur copper
        set(handleButton(2:6, 1), 'Value', 0);
        set(gcbo, 'Value', 1);
        map = copper(256);
        set(figParent, 'Colormap', [CoulNaN; map]);
        
    case -6	% Couleur pink
        set(handleButton(2:6, 1), 'Value', 0);
        set(gcbo, 'Value', 1);
        map = pink(256);
        set(figParent, 'Colormap', [CoulNaN; map]);
        
    case -7 % gesttion d'une couleur particuliere pour les NaN
        if get(gcbo, 'Value')
            
            % Couleur particuliere
            
            set(handleButton(2:3, 2), 'Visible', 'on');
            map = get(figParent, 'Colormap');
            map = [CoulNaN; map];
            CLim = get(handleImage, 'CLim');
            difCLim = CLim(2) - CLim(1);
            CLim(1) = CLim(1) - difCLim / (size(map, 1)-2);
            
            set(handleImage, 'CLim', CLim);
            set(figParent, 'Colormap', map);
        else
            
            % Pas de couleur particuliere
            
            set(handleButton(2:3, 2), 'Visible', 'off');
            set(handleImage, 'CLim', CLimOrigine);
            map = get(figParent, 'Colormap');
            map = map(2:end,:);
            set(figParent, 'Colormap', map);
        end
        
    case -8	% Couleur Blanche pour les NaN
        set(handleButton(2:3, 2), 'Value', 0);
        set(gcbo, 'Value', 1);
        map = get(figParent, 'Colormap');
        CoulNaN = [1 1 1];
        map(1,:) = CoulNaN;
        set(figParent, 'Colormap', map);
        
    case -9	% Couleur Noire pour les NaN
        set(handleButton(2:3, 2), 'Value', 0);
        set(gcbo, 'Value', 1);
        map = get(figParent, 'Colormap');
        CoulNaN = [0 0 0];
        map(1,:) = CoulNaN;
        set(figParent, 'Colormap', map);
    end
    UserData = get(gcf, 'UserData');
    putInUserData( 'map', map, UserData );
    set(gcf, 'UserData', UserData);
end

