% TODO : a finir avec plot_xyz (appel� dans EchoesSScBrush)

function [X2, Y2, Z2, sub] = edit_xyz(X, Y, Z, varargin)

[varargin, XLabel] = getPropertyValue(varargin, 'XLabel', 'X');
[varargin, YLabel] = getPropertyValue(varargin, 'YLabel', 'Y');
[varargin, ZLabel] = getPropertyValue(varargin, 'ZLabel', 'Z'); %#ok<ASGLU>

fig = FigUtils.createSScFigure;
ha(1) = subplot(2,2,1);
hp(1) = plot(X, Z, 'o');
grid on; xlabel(XLabel); ylabel(ZLabel); title('Z=f(X)');

ha(2) = subplot(2,2,2);
hp(2) = plot(Y, Z, 'o');
grid on; xlabel(YLabel); ylabel(ZLabel); title('Z=f(y)');

ha(3) = subplot(2,2,3);
hp(3) = plot(X, Y, 'o');
grid on; xlabel(XLabel); ylabel(YLabel); title('Y=f(X)');

ha(4) = subplot(2,2,4);
hp(4) = plot3(X, Y, Z, 'o');
grid on; xlabel(XLabel); ylabel(YLabel); zlabel(ZLabel); title('Z=f(X,Y)');


% hp1 = plot3(h1, iPing, AcrossDistance, Z, '*'); grid(h1, 'on'); view(h1, 0, 0)
% hp2 = plot3(h2, iPing, AcrossDistance, Z, '*'); grid(h2, 'on'); view(h2, 90, 0)
% hp3 = plot3(h3, iPing, AcrossDistance, Z, '*'); grid(h3, 'on'); view(h3, 0, 90)
% brush on
% % BrushData = false(size(iPing));
% % set(hp1, 'BrushData', BrushData)
% % set(hp2, 'BrushData', BrushData)
% % set(hp3, 'BrushData', BrushData)
% linkdata

set(hp, 'MarkerFaceColor', [0 0 1], 'MarkerSize', 4);
linkaxes(ha([1 3 4]), 'x')
% BrushData = false(size(iPing));
% set(hp1, 'BrushData', BrushData)
% set(hp2, 'BrushData', BrushData)
% set(hp3, 'BrushData', BrushData)
linkdata
% brush on

set(fig, 'DeleteFcn', @DeleteFcnEcoesSScBrush, 'UserData', {ha; hp; X; Y; Z})

% Pour pouvoir se servir de l'appli, il faut mettre un point d'arr�t avant
% la fin de la fonction (sur l'instruction pi par exemple), sinon, le linkdata n'est plus op�rationnel. J'ai
% essay� de ruser (while, pause, etc ...), rien n'y fait.

% % % % waitfor(fig)

X2 = [];
Y2 = [];
Z2 = [];
sub = [];

function DeleteFcnEcoesSScBrush(fig, varargin)

UserData = get(fig, 'UserData');
ha = UserData{1};
hp = UserData{2};
X  = UserData{3};
Y  = UserData{4};
Z  = UserData{5};

% TODO : �a ne fonctionne pas : percer le myst�re des BrushData
sub = false(size(X'));
for k=1:length(hp)
    BrushData = get(hp(k), 'BrushData');
    sub = sub | (sum(BrushData) > 0);
end
% FigUtils.createSScFigure; plot(sub, '*'); grid on
% FigUtils.createSScFigure;
% PlotUtils.createSScPlot(BrushData(1,:), '*'); grid on; hold on;
% PlotUtils.createSScPlot(BrushData(2,:), 'or');

sub = ~sub;
X = X(sub);
Y = Y(sub);
Z = Z(sub);

figure; plot3(X, Y, Z, 'o');
grid on; title('Z=f(X,Y)'); % xlabel(XLabel); ylabel(YLabel); zlabel(ZLabel);
% % % % uiresume(fig)


