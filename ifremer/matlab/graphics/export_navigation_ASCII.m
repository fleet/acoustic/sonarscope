function export_navigation_ASCII(nomFicTxt, T, Latitude, Longitude, SonarSpeed, Heading, Name, Label, Color, nbPoints)

fid = fopen(nomFicTxt, 'w+t');
if fid == -1
     messageErreurFichier('MyFile.xyz', 'WriteFailure');
    return
end

if isempty(Heading)
    Heading = zeros(size(T));
end

fprintf(fid, 'Header; Name; Nb Points; Summary; Red; Green; Blue\n');
for k=1:length(nbPoints)
    fprintf(fid, 'Header; %s; %d; %s; %5.3f; %5.3f; %5.3f \n', Name{k}, nbPoints(k), Label{k}, Color{k}(1), Color{k}(2), Color{k}(3));
end

fprintf(fid, 'Header; Latitude; Longitude; Heading; Speed (m/s)\n');

T.Format = 'dd/MM/yyyy HH:mm:ss.SSS';
str1 = 'Export de la navigation au format ASCII';
str2 = 'Export navigation in ASCII';
N = length(Latitude);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf(fid, '%s; %s; %s; %8.3f; %f\n', char(T(k)), lat2str(Latitude(k)), lon2str(Longitude(k)), Heading(k), SonarSpeed(k));
end
my_close(hw, 'MsgEnd')
fclose(fid);
