function export_navigation_ASCII_AUV(nomFicTxt, T, Latitude, Longitude, Immersion)

fid = fopen(nomFicTxt, 'w+t');
if fid == -1
     messageErreurFichier('MyFile.xyz', 'WriteFailure');
    return
end

if isempty(Immersion)
    fprintf(fid, 'UTC	date	Latitude	Longitude\n');
else
    fprintf(fid, 'UTC	date	Latitude	Longitude	Immersion\n');
end

T.Format = 'dd/MM/yyyy HH:mm:ss.SSS';
str1 = 'Export de la navigation au format ASCII AUV';
str2 = 'Export navigation in ASCII-AUV';
N = length(Latitude);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    if isempty(Immersion)
        fprintf(fid, '%s  %s  %s\n', char(T(k)), num2strPrecis(Latitude(k)), num2strPrecis(Longitude(k)));
    else
        fprintf(fid, '%s  %s  %s  %f\n', char(T(k)), num2strPrecis(Latitude(k)), num2strPrecis(Longitude(k)), Immersion(k));
    end
end
my_close(hw, 'MsgEnd')
fclose(fid);
