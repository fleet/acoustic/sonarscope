% Capture d'une figure et ecriture dans un fichier au format tif, jpeg, ...
%
% Syntax
%   fig2fic( fig,  nomfic options )
% 
% Input Arguments 
%   fig    : Numero de la figure
%   nomfic : Nom du fichier
%
% Name-Value Pair Arguments 
% 	Options de imwrite ('jpg', etc...)
%
% Examples
%   plot(rand(1,100));
%   fig2fic(gcf, fullfile(my_tempdir, 'toto.tif'))
%   fig2fic(gcf, fullfile(my_tempdir, 'toto.jpg'), 'jpg', 'Quality', 100);
%
% See also Authors
% Authors : JMA
%------------------------------------------------------------------------------

function fig2fic(fig, nomfic, varargin)

%% Capture du contenu de la figure

if ~ishandle(fig)
    str = sprintf('Le handle %f n''existe pas', fig);
    my_warndlg(str, 1);
    return
end

Type = get(fig, 'Type');
switch Type
    case 'figure'
        figure(fig);
end

% figure(fig) % Test pour voir si la souris reste � sa place : echec%
% pause(0.5) % Test pour voir si la souris reste � sa place : echec

x = getframe(fig);
% figure; image(x.cdata)

% Pour corriger anom&lie apparue � partir de la R2014b
% W = axis; % Comment� par JMA le 04/11/2019 car bug su l'axe des x est un datetime
% axis(W);  % Comment� par JMA le 04/11/2019 car bug su l'axe des x est un datetime

%% Ecriture dans un fichier image

if nargin == 2
    if ~isempty(x.colormap)
        imwrite(x.cdata, x.colormap, nomfic);
    else
        imwrite(x.cdata, nomfic);
    end
else
    if ~isempty(x.colormap)
        imwrite(x.cdata, x.colormap, nomfic, varargin{:});
    else
        imwrite(x.cdata, nomfic, varargin{:});
    end
end
