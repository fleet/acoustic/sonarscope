% Impression fichier d'une figure (fonction en ligne simplifiee)
%
% Syntax
%   fig2prn (h, fileName)
%   fig2prn (h, fileName, 'format', f)
%   fig2prn (h, fileName, 'size', sz)
%   fig2prn (h, fileName, 'marg', m)
%   fig2prn (h, fileName, 'orient', or)
%   fig2prn (h, fileName, 'out', o)
%
% Input Arguments
%    h        : handle de la figure a imprimer
%    fileName : nom du fichier (nom complet)
%
% Name-Value Pair Arguments
%    format : format du papier
%             les principaux sont :
%             {'usletter', 'uslegal', 'A0', 'A1', 'A2', 'A3', 'A4', 'A5'}
%             get (gcf, 'PaperType') donne la liste complete
%
%    size   : taille du papier en centimetres, vecteur comportant
%             la largeur puis la hauteur. Prioritaire par rapport au
%             format.
%
%    marg   : marges entourant la zone d'impression par rapport aux bords
%             du support, en centimetres. (left, right, bottom, top).
%
%    orien  : orientation du papier. Les formats admis sont :
%             {'p', 'l', 't'} (Portrait, Landscape, Ajuste)
%
%    out    : format fichier de la sortie. Les types admis sont :
%             {'PS2', 'PSC2', 'JPG', 'TIF'}.
%
% Remarks :
%    En l'absence d'un format de sortie (out), PSC2 est pris par defaut
%
% Examples
%    h = figure ;
%    plot (1:100) ;
%    fig2prn (h, 'fichier_exemple.jpg', 'format', 'A4', ...
%                'marg', [1,1,1,1], 'orien', 'p', 'out', 'JPG') ;
%
% See also Authors
% Authors : DCF
% VERSION  : $Id: fig2prn.m,v 1.1 2002/06/04 14:42:25 augustin Exp $
%------------------------------------------------------------------------------

function fig2prn ( fig, nomfic, varargin )

% -----
% Tests

if ~ishandle (fig)
    my_warndlg('fig2prn : Invalid figure handle', 0);
    return
end

% ---------------------
% Acces aux proprietes

[varargin, format] = getPropertyValue(varargin, 'format', []);
[varargin, size]   = getPropertyValue(varargin, 'size',   []);
[varargin, marg]   = getPropertyValue(varargin, 'marg',   []);
[varargin, orien]  = getPropertyValue(varargin, 'orien',  []);
[varargin, out]    = getPropertyValue(varargin, 'out',    []); %#ok<ASGLU>

% --------------------------------------------------------------------------
% Lecture des proprietes de la figure

formatBak = get (fig, 'PaperType') ;
sizeBak   = get (fig, 'PaperSize') ;
posBak    = get (fig, 'PaperPosition') ;
orienBak  = get (fig, 'PaperOrientation') ;
unitsBak  = get (fig, 'Units') ;

% --------------------------------------------------------------------------
% se positionne sur la figure

cFigure = get (0, 'CurrentFigure') ;
figure (fig) ;

% --------------------------------------------------------------------------
% Change successivement les proprietes

set (fig, 'PaperUnits', 'centimeters') ;

if ~isempty (format)
    set (fig, 'PaperType', format) ;
end

if ~isempty (size)
    set (fig, 'PaperSize', size) ;
end

if ~isempty (orien)
    switch (orien)
        case 'p'
            orient portrait ;
        case 'l'
            orient landscape ;
        case 't'
            orient tall ;
    end
end

if ~isempty (marg)
    sz  = get (fig, 'PaperSize') ;
    pos(1) = marg(1) ;
    pos(2) = marg(3) ;
    pos(3) = sz(1) - (marg(1) + marg(2)) ;
    pos(4) = sz(2) - (marg(3) + marg(4)) ;
    pos (pos<0) = 0 ;
    set (fig, 'PaperPosition', pos) ;
end

% --------------------------------------------------------------------------
% Impression

cmd = [] ;
cmd {end+1} = 'print ' ;
if ~isempty (out)
    switch (out)
        case 'PS2'
            cmd {end+1} = '-deps2 ' ;
        case 'PSC2'
            cmd {end+1} = '-depsc2 ' ;
        case 'JPG'
            cmd {end+1} = '-djpeg ' ;
        case 'TIF'
            cmd {end+1} = '-dtiff ' ;
    end
else
    cmd {end+1} = '-depsc2 ' ;
end
cmd {end+1} = nomfic ;
commande = '' ;
for i = 1:length (cmd)
    commande = sprintf ('%s%s', commande, cmd{i}) ;
end

eval (commande);

% ----------------------------
% Retrouve la figure d'origine

if ishandle (cFigure)
    figure (cFigure) ;
end

% ----------------------------------------
% Replace la figure comme avant impression

set (fig, 'PaperType', formatBak) ;
set (fig, 'PaperSize', sizeBak) ;
set (fig, 'PaperPosition', posBak) ;
set (fig, 'PaperOrientation', orienBak) ;
set (fig, 'Units', unitsBak) ;
