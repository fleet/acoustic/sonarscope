function h = figureOnTop(varargin)

    function my_timer_fcnRefresh(obj, event, string_arg) %#ok<INUSD>
        if ishandle(h)
            figure(h);
        end
    end

h = figure(varargin{:});

t = timer('StartDelay', 0, 'Period', 5, 'ExecutionMode', 'fixedRate');
t.TimerFcn = {@my_timer_fcnRefresh, 'Itération'};
start(t)

end
