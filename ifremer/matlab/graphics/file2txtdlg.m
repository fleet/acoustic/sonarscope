% Affichage du contenu d'un fichier ASCII dans une fen�tre
%
% Syntax
%   file2txtdlg(Titre, Entete, nomFic)
%
% Input Arguments
%   Titre  : Titre 
%   Entete : Entete de la fen�tre
%   nomFic : Nom du fichier � afficher
%
% Examples
%   nomFic = getNomFicDatabase('SAR_sarer43.ifr'); 
%   Titre = ['Contents of ' nomFic];
%   file2txtdlg(Titre, 'SonarScope - IFREMER', nomFic)
%
% See also my_txtdlg Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%--------------------------------------------------------------------------

function file2txtdlg(Titre, Entete, nomFic)

fid = fopen(nomFic);
if fid == -1
    str = sprintf('The file %s is missing', nomFic);
    my_warndlg(str, 1);
    return
end

str = {};
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    str{end+1} = tline; %#ok<AGROW>
end
fclose(fid);

my_txtdlg(Titre, str, 'Entete', Entete, 'windowstyle', 'normal');
