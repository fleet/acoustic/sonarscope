% FONCTION MORIBONDE
% Creation et remplissage d'une frame.
%
% Syntax
%   a = fillFrame(TagFrame, ihm, varargin)
%
% Input Arguments
%   TagFrame  : Tag de la frame
%   ihm       : Structure decrivant les proprietes des uicontrols
%     ihm.Lig : Lignes de placement de l'element graphique (cellule excel)
%     ihm.Col : Colonnes de placement de l'element graphique (cellule excel)
%
% Name-Value Pair Arguments
%   'Position' : Position du contenant
%   'XMargin'  : Marge a laisser entre les boutons en largeur (%)
%   'YMargin'  : Marge a laisser entre les boutons en hauteur (%)
%
% Examples
%   % A Faire
%
% See also createUicontrols ASlayout Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function a = fillFrame(TagFrame, ihm, varargin)

[varargin, XMargin] = getPropertyValue(varargin, 'XMargin', 0.01);
[varargin, YMargin] = getPropertyValue(varargin, 'YMargin', 0.01); %#ok<ASGLU>

%% Cr�ation des uicontrols

TheUicontrols = createUicontrols(ihm);

%% Positionnement automatique des boutons

if isempty(TagFrame)
    ASlayout(TheUicontrols, ihm, 'XMargin', XMargin, 'YMargin', YMargin);
else
    handleFrame = findobj(gcf, 'Tag', TagFrame);
    VisibleHandleFrame = get(handleFrame, 'Visible');
    set(handleFrame, 'Visible', 'on');
    units = get (handleFrame, 'Units') ;
    set (handleFrame, 'Units', 'normalized') ;
    Position = get(handleFrame , 'Position');
    set (handleFrame, 'Units', units) ;
    ASlayout(TheUicontrols, ihm, 'XMargin', XMargin, 'YMargin', YMargin, 'Position', Position);
end

%% Fabrication de l'objet a de type ClFrame

a = [] ;
if ~isempty(TagFrame)
    a = cl_frame('HandleFrame', handleFrame);
    set( a, 'NomFrame',			TagFrame);
    set( a, 'PositionFrame',	Position);
    set( a, 'ListeUicontrols',	TheUicontrols);
end

%% Positionne la visibilite des objets graphiques de la frame a la meme visibilite que la frame

if ~isempty (TagFrame)
    set (a, 'Visible', VisibleHandleFrame) ;
end
