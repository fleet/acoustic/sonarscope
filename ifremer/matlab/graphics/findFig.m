function [flag, hAxe, hFig] = findFig(varargin)

[varargin, Tag] = getPropertyValue(varargin, 'Tag', []); %#ok<ASGLU>

flag = 1;

hFig = findobj('Type', 'figure', 'Tag', Tag);
if isempty(hFig)
    hAxe = [];
else
    hAxe = findobj(hFig, 'Type', 'axes', 'Tag', 'AxePlotMos');
    if isempty(hAxe)
        return
    end
    
    str1 = 'Trac� dans une nouvelle figure ?';
    str2 = 'Plot in a new figure ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 1
        hAxe = [];
    else
        if length(hAxe) > 1
            str = {};
            for k=1:length(hAxe)
                str{k} = sprintf('Figure %d', get(hAxe(k), 'Parent')); %#ok<AGROW>
            end
            str1 = 'Dans quelle figure ?';
            str2 = 'In what figure ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            hAxe = hAxe(rep);
        end
    end
end
