% Temporaire en attendant la creation de la classe ClSignal
% See also Ex10_cli_image findImageFromNav Authors

function [iminGlobal, subPings] = findImageFromNav(Nav, varargin)

[varargin, tMilieu] = getPropertyValue(varargin, 'tMilieu', []);
[varargin, nbPings] = getPropertyValue(varargin, 'nbPings', 2000); %#ok<ASGLU>

N = length(Nav);
for i=1:N
    difft = Nav(i).t - tMilieu;
    [tMin, iMin] = min(abs(difft));
    tab_tMin(i) = tMin; %#ok<AGROW>
    tab_iMin(i) = iMin; %#ok<AGROW>
end
[~, iminGlobal] = min(tab_tMin);


fprintf('Fichier le plus vraisemblable : %d : %s\n', iminGlobal, Nav(iminGlobal).nomFic);
fprintf('Enr le plus vraisemblable : %d - %s\n', tab_iMin(iminGlobal), t2str(Nav(iminGlobal).t(tab_iMin(iminGlobal))));
fprintf('Ecart : %f sec\n', tab_tMin(iminGlobal) / 1000);

nbPings = floor(nbPings /2);
subPingsDeb = tab_iMin(iminGlobal) - nbPings;
subPingsFin = tab_iMin(iminGlobal) + nbPings;
subPingsDeb = max(1, subPingsDeb);
subPingsFin = min(subPingsFin, length(Nav(iminGlobal).t));
subPings = subPingsDeb:subPingsFin;
