function [DrawModeName, DrawModeValue] = getDrawModePropertyNameAndValue

if my_verLessThanR2014b
    DrawModeName  = 'DrawMode';
    DrawModeValue = 'fast';
else
    DrawModeName  = 'SortMethod';
    DrawModeValue = 'childorder';
end
