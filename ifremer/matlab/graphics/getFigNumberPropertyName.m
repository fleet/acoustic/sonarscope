function nom = getFigNumberPropertyName

if my_verLessThanR2014b
    nom = 'number';
else
    nom = 'NumberTitle';
end
