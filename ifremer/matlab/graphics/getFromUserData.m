% Lecture d'une information taguee dans une liste
% de la forme liste{:}.PropertyName liste{:}.PropertyValue
% Fonction est utile pour gerer les UserData des graphiques
%
% Syntax
%   [PropertyValue, indice] = getFromUserData( PropertyName, UserDataIn)
%
% Input Arguments 
%   PropertyName   : Identifiant de l'information
%   UserDataIn     : Liste contenant toutes les infos
%
% Output Arguments
%   PropertyValue   : Information identifiee par PropertyName ou [] si
%                     l'identifiant n'est pas trouve
%   indice          : indice du tableau de cellules ou se trouve PropertyName
% 
% Examples
%   UserData = [];
%   UserData = putInUserData('A', 1, UserData)
%   UserData = putInUserData('B', 1:4, UserData)
%   UserData{end+1} = 'Un bug s''est-il imice ici';
%   UserData = putInUserData('C', rand (2,2), UserData)
%   UserData = putInUserData('D', 'toto', UserData)
%   UserData = putInUserData('E', {1; 'x'}, UserData)
%   fig = figure;
%   set(fig, 'UserData', UserData);
%
%   clear UserData
%   UserData = get(fig, 'UserData')
%   x = getFromUserData('A', UserData)
%   x = getFromUserData('B', UserData)
%   x = getFromUserData('C', UserData)
%   x = getFromUserData('D', UserData)
%   x = getFromUserData('E', UserData)
%   x = getFromUserData('xxxx', UserData)
%
% See also putInUserData rmInUserData Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [PropertyValue, indice] = getFromUserData(PropertyName, UserDataIn)

%% Recherche si la PropertyName existe dans la liste

PropertyValue = [];
indice = [];

if isempty(UserDataIn)
	return
end

for k=1:length(UserDataIn)
	if iscell(UserDataIn(k))
		x = UserDataIn{k};
		if isstruct(x)
			if isfield(x, 'PropertyName') && isfield(x, 'PropertyValue')
				if strcmp(x.PropertyName, PropertyName)
					PropertyValue = x.PropertyValue;
					indice = k;
                    return
				end
			end
		end
	end
end
