% Limtes d'un graphe.
%
% Syntax
%   [DataLim, AxeLim] = getParamZoom(strAxe, ...)
%
% Input Arguments
%   strAxe : 'X' ou 'Y'
%
% Name-Value Pair Arguments
%   hAxe    : Handle de l'axe concerne
%   hCourbe : Handle de la courbe concernee
%
% Output Arguments
%   DataLim : Limites des donnees comprises dans l'axe
%   AxeLim  : Limites de l'axe
%
% Examples
%
% See also SliderValue2XLim Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [DataLim, AxeLim] = getParamZoom(strAxe, varargin)

[varargin, hAxe] = getPropertyValue(varargin, 'hAxe', []);
[varargin, hCourbe] = getPropertyValue(varargin, 'hCourbe', []); %#ok<ASGLU>

if isempty(hAxe)
    hAxe = gca;
end

strData = [strAxe 'Data'];
strLim  = [strAxe 'Lim'];
if isempty(hCourbe)
    hCourbes = findobj(hAxe, 'Type', 'line');
    minData = Inf;
    maxData = -Inf;
    for i=1:length(hCourbes)
        Data = get(hCourbes(i), strData);
        minData = min([Data, minData]);
        maxData = max([Data, maxData]);
    end
else
    XData = get(hCourbe, strData);
    minData = min(XData);
    maxData = max(XData);
end

DataLim(1) = minData;
DataLim(2) = maxData;

AxeLim = get(hAxe, strLim);
AxeLim(1) = max(AxeLim(1), minData);
AxeLim(2) = min(AxeLim(2), maxData);
