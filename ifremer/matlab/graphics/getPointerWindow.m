function h = getPointerWindow

% This does not account for Docked Figures.
pointerLocation = get(0, 'PointerLocation');
figs = allchild(0);
h = 0;
for k=1:numel(figs)
    figPos = getpixelposition(figs(k));
    if (pointerLocation(1) >= figPos(1)) && ...
            (pointerLocation(1) <= figPos(1) + figPos(3)) && ...
            (pointerLocation(2) >= figPos(2)) && ...
            (pointerLocation(2) <= figPos(2) + figPos(4))
        if strcmp(figs(k).WindowState, 'minimized')
            h = figs(k);
            return
        end
    end
end
