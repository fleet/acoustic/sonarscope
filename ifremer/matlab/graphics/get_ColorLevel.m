function color = get_ColorLevel(ColorLevel)

if isempty(ColorLevel)
    color = [0.8314    0.8157    0.7843];
    return
end

switch ColorLevel
    case 1
        color = [0.8314    0.8157    1];
    case 2
        color = [0.8314    0.8157    0.7843];
    case 3
        color = [0.9    0.8157    0.7843];
    otherwise
        color = [0.8314    0.8157    0.7843];
end
