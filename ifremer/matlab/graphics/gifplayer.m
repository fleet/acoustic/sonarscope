% The function displays any animated GIF's in a figure window
%
%% Demo : gifplayer; %plays the animated crystal.gif file
%
%% Usage:
% ex: gifplayer('animated.gif',0.1); %name of the gif file and the delay in
% which to update the frames in the GIF file
%
%% Vihang Patil, Oct 2006
% Copyright 2006-2007 Vihang Patil
% Email: vihang_patil@yahoo.com
% http://www.mathworks.com/matlabcentral/fileexchange/12673
% Created: 17th Oct 2006
%
%% Revision:
% Date: 19th Oct 2006..Removed the setappdata and getappdata and used
% functions handling property. Multiple Gif's can be called upon which can
% be opened in new figure window.
% ex: figure;gifplayer;
% ex: figure;gifplayer('abcd.gif',0.1); and so on
%
% GLU le 29/09/2012
% Mise en forme de l'engrenage anim� : titre, image, delai, style modal.
%% P.N: PLease make sure to close the existing window in which the gif is
% currently being played and open a separate window for another GIF
% image.If another GIF is opened in the same window then the first timer
% continues to run even if you close the figure window.
% nomFic = 'C:\SScTbxResources\GifAnimated\gearing.gif';
% nomFic = 'C:\SScTbxResources\GifAnimated\GearsGIF.gif';
% [f, hdl] = gifplayer('filenameGif', nomFic);
% ------------------------------------------------------------------------------

function [flag, handles] = gifplayer(varargin)

global IfrTbxResources %#ok<GVMIS>

flag = 0;

[varargin, fileNameGif]  = getPropertyValue(varargin, 'filenameGif',  []);
[varargin, titleGif]     = getPropertyValue(varargin, 'title',        []);
[varargin, delay_length] = getPropertyValue(varargin, 'delay_length', 0.1);
[varargin, windowStyle]  = getPropertyValue(varargin, 'styleWindow',  'normal'); %#ok<ASGLU> %'modal');

if isempty(titleGif)
    titleGif = Lang('Traitement en cours', 'Work in progress');
end

if isempty(fileNameGif)
    nomDir = fullfile(IfrTbxResources, 'GifAnimated');
    if ~exist(nomDir, 'dir')
        handles = [];
        return
    end
    listeNomGIF = listeFicOnDir(nomDir, '.gif');
    k = randi(length(listeNomGIF),1,1);
    fileNameGif = listeNomGIF{k};
end

if ~exist(fileNameGif, 'file')
    handles = [];
    return
end

[~, ~, ext] = fileparts(fileNameGif); % file information retrieved here

if strcmp(ext,'.gif')
    try
        [handles.im, handles.map] = imread(fileNameGif, 'frames', 'all'); %read all frames of an gif image
    catch ME
        WorkInProgress(titleGif)
        return
    end
    handles.len    = size(handles.im,4); % number of frames in the gif image
    handles.guifig = figure;
    % Calcul des tailles et positionnement au centre
    dummy = squeeze(handles.im(:, :, :, 1));
    sz    = size(dummy);
    centrageFig(20 + max(sz(1)+length(titleGif)*5), 50 + sz(2), 'Fig', handles.guifig )
    
    handles.h1 = image(handles.im(:,:,:,1));%loads the first image along with its colormap
    colormap(handles.map); %loads the first image along with its colormap
    axis off
    axis equal tight
    hAxes = get(handles.h1, 'Parent');
    set(hAxes, 'Position', [0 0 1 0.85]); % Pour y ins�rer le titre au-dessus du graphe.
    handles.count = 1;% intialise counter to update the next frame
    handles.tmr   = timer('TimerFcn', {@TmrFcn,handles.guifig},'BusyMode','Queue',...
        'ExecutionMode','FixedRate','Period',delay_length); %form a Timer Object
    guidata(handles.guifig, handles);
    start(handles.tmr); %starts Timer
else
    error('Not a GIF image. Load only GIF images'); %If image is not GIF, show this error
end
set(handles.guifig, 'CloseRequestFcn', {@CloseFigure,handles});

% Ajout du titre.
if ~isempty(titleGif)
    hTitle  = title(titleGif, 'Interp', 'none');
    %     set(hTitle, 'FontSize', 6);
    set(hTitle, 'Color', 'b');
end

% Customisation de la fen�tre.
set(handles.guifig, 'MenuBar',     'none');
set(handles.guifig, 'Name',        titleGif);
set(handles.guifig, 'Color',       'white');
set(handles.guifig, 'WindowStyle', windowStyle);


flag = 1;

function TmrFcn(src,event,handles) %#ok<INUSL>
%Timer Function to animate the GIF

handles = guidata(handles);
set(handles.h1,'CData',handles.im(:,:,:,handles.count)); %update the frame in the axis
handles.count = handles.count + 1; %increment to next frame
if handles.count > handles.len %if the last frame is achieved intialise to first frame
    handles.count = 1;
end

guidata(handles.guifig, handles);



function CloseFigure(src,event,handles) %#ok<INUSL>
% Function CloseFigure(varargin)
stop(handles.tmr);
delete(handles.tmr);%removes the timer from memory
closereq;


