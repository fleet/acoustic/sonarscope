% Trace d'une grille reguliere sur un axe
%
% Syntax
%   grille(resol)
%   grille(resol, handleAxe)
%
% Input Arguments
%   resol     : Pas de la grille.
%
% PROPERTY VALUES :
%   handleAxe : Handle de l'axe
%
% Examples
%   nomFic = '/home1/doppler/augustin/MatlabToolboxIfremer/data/EM300_op15.xy';
%   a = cl_car_bat_data(nomFic);
%   xy(a, 'subl', 100:200);
%   grid off; hold on; grille(20); hold off
%
% See also cl_car_bat_data/xy Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function grille(resol, handleAxe)

switch nargin
    case 0
        help grille
        return
    case 1
        handleAxe = gca;
end


XLim = get(handleAxe, 'XLim');
YLim = get(handleAxe, 'YLim');

grid off;
hold on;
N = (XLim(2) - XLim(1)) / resol;

debut = XLim(1) - mod(XLim(1), resol);
for i=1:N
    x = debut + i * resol;
    plot([x x], YLim, 'Color', [0.7 0.7 0.7]);
end

N = (YLim(2) - YLim(1)) / resol;
debut = YLim(1) - mod(YLim(1), resol);
for i=1:N
    y = debut + i * resol;
    plot(XLim, [y y], 'Color', [0.7 0.7 0.7]);
end
hold off;
