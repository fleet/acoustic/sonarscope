% Lit l'instance courante dans le UserData
%
% Syntax
%   instance = handle2obj(h0)
%
% Input Arguments
%   h0 : Numero de la figure
%
% Output Arguments
%   instance : Instance courante de la figure
%
% Examples
%   h0 = figure;
%   x = cl_backscatter;
%   obj2handle(x, h0)
%   instance = handle2obj(h0)
%
% See also obj2handle var2handle handle2obj Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function instance = handle2obj(h0)
instance = handle2var('Instance', h0);
