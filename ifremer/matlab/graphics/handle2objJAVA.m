% Lit l'instance courante dans le UserData
%
% Syntax
%   instance = handle2obj(h0)
% 
% Input Arguments 
%   h0  : Numero de la figure
% 
% Output Arguments 
%   instance : Instance courante de la figure
%
% Examples 
%   h0 = figure;
%   x = cl_backscatter;
%   obj2handle(x, h0)
%   instance = handle2obj(h0)
%
% See also obj2handle var2handle handle2obj Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function instance = handle2objJAVA(h0)

%(gcbf ne fonctionne pas avec les cpn JAVA, on recherche la figure
% ancestor)
if contains(class(h0), 'javax')
    handle = get(h0, 'MatlabHGContainer');
    if ishandle(handle)
        hparent = get(handle, 'Parent');
        ancetre=ancestor(hparent,{'figure'},'toplevel');
        instance = handle2var('Instance', ancetre);
    else
        instance =0;
        return
    end
else
    instance = handle2var('Instance', gcbf);
end

 %instance = handle2var('Instance', h0); % version precedente de handle2obj
% avant LUR

%si handle2obj ne fonctionne pas, remplacez "this = handle2obj(gcbf);" par
%"this = handle2obj(gcbo)";