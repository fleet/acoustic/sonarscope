% Lit l'instance sauvee dans le UserData
%
% Syntax
%   instanceSauvee = handle2savedObj(h0)
%
% Input Arguments
%   h0  : Numero de la figure
%
% Output Arguments
%   instanceSauvee : Instance qui a ete sauvegardee
%
% Examples
%   h0 = figure;
%   x = cl_backscatter;
%   savedObj2handle(x, h0)
%   instance = handle2savedObj(h0)
%
% See also savedObj2handle obj2handle handle2obj Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function instanceSauvee = handle2savedObj(h0)
instanceSauvee = handle2var('InstanceSauvee', h0);