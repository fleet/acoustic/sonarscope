% Lit une variable tagu�e dans le UserData
%
% Syntax
%   var = handle2var(Tag, h0)
% 
% Input Arguments 
%   Tag : Etiquette associee a la variable
%   h0  : Numero de la figure
% 
% Output Arguments 
%   var : Variable a sauver
%
% Examples 
%   h0 = figure;
%   x = cl_backscatter;
%   obj2handle(x, h0)
%   instance = handle2obj(h0)
%
% See also var2handle handle2obj obj2handle Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function var = handle2var(Tag, h0)

%% Lecture des donn�es courantes

if ~ishandle(h0)
    return
end
UserData = get(h0, 'UserData');
% UserData = h0.UserData; % R2014b

%% Lecture des donn�es

var = getFromUserData(Tag, UserData);
