% Temporaire en attendant la creation de la classe ClSignal
% See also Ex10_cli_image findImageFromNav Authors

function Nav = imoSonar2nav(nomFic)

N = length(nomFic);
for i=1:N
    a = cl_car_sonar('nomFic', nomFic{i}, 'mode', 'NC_NOWRITE');
    
    Date                = get(a, 'grRawImage_grSyncJulianDay');
    Heure               = get(a, 'grRawImage_grSyncJulianMs');
    
    Nav(i).Ping         = get(a, 'grRawImage_grSyncPingNumber');
    Cap                 = get(a, 'grRawImage_grSyncHeading');
    
    Cap = mod(Cap+360, 360);
    Nav(i).Cap          = Cap;
    Nav(i).capMoyen     = median(Cap);
    
    Nav(i).Speed        = get(a, 'grRawImage_grSyncSpeed');
    Nav(i).Immersion    = get(a, 'grRawImage_grSyncImmersion');
    Nav(i).Height       = get(a, 'grRawImage_grSyncPrtRTHeight');
    
    
    X  = get(a, 'grRawImage_grSyncLatitude');
    X(X == 0) = NaN;
    Nav(i).Lat  = X;
    
    X  = get(a, 'grRawImage_grSyncLongitude');
    X(X == 0) = NaN;
    Nav(i).Lon  = X;
    
    Nav(i).t = cl_time('timeIfr', Date, Heure);
    Nav(i).nomFic = nomFic{i};
end

