% Informations sur les dimensions de trac� possibles en repectant � peu
% pr�s le ration "1pixel papier Pour 1 pixel image"
%
% Syntax
%   info_map_dimensions(LatLim, LonLim)
%
% Input Arguments
%   LatLim : Limites en latitude (deg)
%   LonLim : Limites en longitude (deg)
%
% Examples
%   info_map_dimensions(LatLim, LonLim);
%
% See also plot_navigation_Dimensions Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function info_map_dimensions(LatLim, LonLim, varargin)

[varargin, YStepMetrique] = getPropertyValue(varargin, 'YStepMetrique', []);
[varargin, Append]        = getPropertyValue(varargin, 'Append',        []); %#ok<ASGLU>

str = {};
str{end+1} = sprintf('North Latitude : %s', lat2str(LatLim(2)));
str{end+1} = sprintf('South Latitude : %s', lat2str(LatLim(1)));
str{end+1} = sprintf('West Longitude : %s', lon2str(LonLim(1)));
str{end+1} = sprintf('East Longitude : %s', lon2str(LonLim(2)));

LargeurEnMetres = abs(diff(LonLim)) * (pi/180) * 6378137 * cosd(mean(LatLim));
HauteurEnMetres = abs(diff(LatLim)) * (pi/180) * 6378137;

if (HauteurEnMetres > 1000) || (LargeurEnMetres > 1000)
    str{end+1} = sprintf('Height (km) : %s', num2str(HauteurEnMetres/1000));
    str{end+1} = sprintf('Width (km)  : %s', num2str(LargeurEnMetres/1000));
else
    str{end+1} = sprintf('Height (m) : %s', num2str(HauteurEnMetres));
    str{end+1} = sprintf('Width (m)  : %s', num2str(LargeurEnMetres));
end
str{end+1} = ' ';
str{end+1} = 'Plot size depending on different resolutions for this frame :';


str1 = 'R�solution de l''imprimante en "Dots Per Inch"';
str2 = 'Printer Resolution in "Dots Per Inch"';
strResolImprimante = {'75'; '150'; '300'; '600'; '1200'};
[rep, flag] = my_listdlg(Lang(str1,str2), strResolImprimante, 'InitialValue', 3, 'SelectionMode', 'Single');
if ~flag
    return
end
ResolImprimante = str2double(strResolImprimante{rep});

R = [1 1.5 2 2.5 3:9];
Resol = [R*1e-3 R*1e-2 R*1e-1 R R*1e1 R*1e2 R*1e3];
str = calcul(str, Resol, HauteurEnMetres, LargeurEnMetres, ResolImprimante);

if ~isempty(YStepMetrique)
    str{end+1} = ' ';
    str{end+1} = sprintf('This image resolution : %s m', num2str(YStepMetrique));
    
    Resol = YStepMetrique;
    str = calcul(str, Resol, HauteurEnMetres, LargeurEnMetres, ResolImprimante);
end

if ~isempty(Append)
    str{end+1} = ' ';
    str{end+1} = Append;
end

str1 = 'Estimation approximative de la dimension des trac�s sur un rep�re psudo-cartographique.';
str2 = 'Rough estimation of plot dimensions on a pseudo-cartographic representation.';
my_txtdlg(Lang(str1,str2), str, 'windowstyle', 'normal');


function str = calcul(str, Resol, HauteurEnMetres, LargeurEnMetres, ResolImprimante)

Format = [];
for iResol=1:length(Resol)
    NL = ceil(HauteurEnMetres / Resol(iResol));
    NC = ceil(LargeurEnMetres / Resol(iResol));
    HauteurPapier = NL * (2.54 / ResolImprimante);
    LargeurPapier = NC * (2.54 / ResolImprimante);
    
    for k=5:-1:1
        HP = 29.7 * 2^((k-1)/2);
        LP = 21   * 2^((k-1)/2);
        if (HauteurPapier < HP) && (LargeurPapier < LP)
            Format = sprintf('A%d - Portrait', 5-k);
        elseif (HauteurPapier < LP) && (LargeurPapier < HP)
            Format = sprintf('A%d - Landscape', 5-k);
        end
    end
    
    if ~isempty(Format)
        deno = HauteurEnMetres / (HauteurPapier/100);
        str{end+1} = sprintf('Resol (m) : %s\t   NbLines : %d   nbColumns : %d  Paper size on a %d dpi printer, Height (cm) : %f, Width (cm) : %f, Scale : 1/%d, Format : %s', ...
            num2str(Resol(iResol)), NL, NC, ResolImprimante, HauteurPapier, LargeurPapier, floor(deno), Format); %#ok
        if strcmp(Format(2), '4') && ((HauteurPapier < 5) || (LargeurPapier < 5)) % Premier format A4 trouv�
            break
        end
    end
end
