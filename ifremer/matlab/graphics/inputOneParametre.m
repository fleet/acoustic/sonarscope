% Description
%   Creates a dialog box to get the value of a parametre
%
% Syntax
%   [flag, X] = inputOneParametre(TitreDialogBox, TitreParam, ...)
%
% Input Arguments
%   TitreDialogBox : Title for the dialog box
%   TitreParam     : Title for the parametre
%
% Name-Value Pair Arguments
%   Value    : Init value
%   Format   : {'%f'} | '%d'
%   Unit     : Unit
%   MinValue : Min value
%   MaxValue : Max value
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   X    : Returned value
%
% More About : This function will adapt the type of
% the dialog box according to the Format : SpinnerParametreDialog if '%d',
% StyledParametreDialog if '%f'
%
% Examples
%   [flag, X] = inputOneParametre('Hello World', 'My parameter name')
%   [flag, X] = inputOneParametre('Hello World', 'My parameter name', 'Value', 3)
%   [flag, X] = inputOneParametre('Hello World', 'My parameter name', 'Value', pi, 'Format', '%f')
%
% Authors  : JMA
% See also : SpinnerParametreDialog StyledParametreDialog
%
% Reference pages in Help browser
%   inputOneParametre
%-------------------------------------------------------------------------------

function [flag, X] = inputOneParametre(TitreDialogBox, TitreParam, varargin)

% TODO : remplacer SpinnerParametreDialog par StyledSpinnerParametreDialog
% quand �a sera dispo et enlever 'Unit', '', a.sizes = [-2 -1 0 0];

[varargin, Value]    = getPropertyValue(varargin, 'Value', []);
[varargin, Format]   = getPropertyValue(varargin, 'Format', '%f');
[varargin, Unit]     = getPropertyValue(varargin, 'Unit', []);
[varargin, MinValue] = getPropertyValue(varargin, 'MinValue', -Inf);
[varargin, MaxValue] = getPropertyValue(varargin, 'MaxValue', Inf); %#ok<ASGLU>

p = ClParametre('Name', TitreParam, 'Unit', Unit, 'Format', Format, ...
    'MinValue', MinValue, 'MaxValue', MaxValue, 'Value', Value);

switch Format
    case '%d'
        a = SpinnerParametreDialog('params', p, 'Title', TitreDialogBox);
        style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
        style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
        a.titleStyle = style1;
        a.paramStyle = style2;
        % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    otherwise
        a = StyledParametreDialog('params', p, 'Title', TitreDialogBox);
        if isempty(Unit) % TODO : supprimer quand StyledParametreDialog s'adaptera � ce qu'il trouve
            nt = 0;
        else
            nt = -1;
        end
        a.sizes = [0    -3    0    0    0    -2    nt    0];
end
a.openDialog;
flag = a.okPressedOut;
if ~flag
    X = [];
    return
end
X = a.getParamsValue;
