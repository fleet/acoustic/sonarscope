% Check if a graphic handle is of a specific one
%
% Syntax
%   flag = isAGraphicElement(h, type)
%
% Input Arguments
%   h    : Handle of a graphic element
%   type : Type of the graphic element ('figure', ...)
%
% Output Arguments
%   flag : boolean
%
% Examples
%   fig = figure;
%   ha = subplot(1,1,1);
%   hc = plot(1:10); grid on
%   flag = isAGraphicElement(fig, 'Figure')
%   flag = isAGraphicElement(ha, 'Axes')
%   flag = isAGraphicElement(hc, 'Line')
%
% See also isgraphics Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function flag = isAGraphicElement(h, Type)

% if my_verLessThanR2014b
% 	flag = strcmpi(get(h, 'Type'), Type);
% else
    flag = isgraphics(h, Type);
% end
