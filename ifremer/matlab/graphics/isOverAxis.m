function [flag, C] = isOverAxis(hAxis)

C = get(hAxis, 'currentpoint');
C = C(1,:);
% P = get(hAxis, 'position');
xlim = get(hAxis, 'xlim');
ylim = get(hAxis, 'ylim');
outX = ~any(diff([xlim(1) C(1,1) xlim(2)])<0);
outY = ~any(diff([ylim(1) C(1,2) ylim(2)])<0);
flag = outX & outY;
