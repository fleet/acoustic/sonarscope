% Lecture de l'icone Ifremer
%
% Syntax
%   image = logoIfremer
%   image = logoIfremer(pas)
%
% PROPERTY VALUES :
%   pas : Pas d'echantillonage (2.5 par defaut)
%
% Output Arguments
%   image : Icone en RGB.
%
% Examples
%   imshow(logoIfremer)
%   imshow(logoIfremer(1))
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function image = logoIfremer(pas)

if nargin == 0
    pas = 2.5;
end

NomFicLogo = getNomFicDatabase('logoIfremerJet.tif');

image = double(imread(NomFicLogo)) / 255;
sz = size(image);
image = image(floor(1:pas:sz(1)), floor(1:pas:sz(2)),:);
