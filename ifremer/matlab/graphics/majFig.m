% Redimentionnement automatique d'une figure.
% Cette fonction redimentionne une figure afin que tous les �l�ments
% qu'elle contient soient � l'interieur. 
%
% Syntax
%   majFig(fig)
%
% Input Arguments 
%   fig : Num�ro de la figure 
% 
% Examples
%   fig = figure;
%   uicontrol('Parent', fig, 'Units', 'Pixels', 'Position',[60 60 100 30], 'Style','text','String','Exemple');
%   majFig(fig)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function majFig(fig)

if ~ishandle(fig)
    return
end

% On exclut la zone cr�� pour le HTML de my_warndlg;
h1 = findobj(fig, 'Type', 'uicontrol', '-not', 'Tag', 'tagTextBoxForHTML');
h2 = findobj(fig, 'Type', 'axe');
children = [h1(:); h2(:)];

xmin = 1e10; xmax = -1e10;
ymin = 1e10; ymax = -1e10;
N = length(children);
childrenUnit  = cell(N,1);
childPosition = cell(N,1);
for k=1:N
	childrenUnit{k} = get(children(k), 'Units'); 
	set(children(k), 'Units', 'pixels');
	Position = get(children(k), 'Position');
	childPosition{k} = Position; 
	
	xdeb = Position(1);
	ydeb = Position(2);
	xfin = Position(1) +  Position(3);
	yfin = Position(2) +  Position(4);
	
	xmin = min([xmin xdeb xfin]);
	xmax = max([xmax xdeb xfin]);
	ymin = min([ymin ydeb yfin]);
	ymax = max([ymax ydeb yfin]);
end

offsetx = 135;
offsety = 20;
Units = get(fig, 'Units');
set(fig, 'Units', 'pixels');
Position = get(fig, 'Position');
% Position(3) = xmax - xmin;% + offsetx;
Position(3) = xmax - xmin + offsetx;
Position(4) = ymax - ymin + offsety;
set(fig, 'Position', Position);

for k=1:length(children)
	Position = childPosition{k};
	Position(1) = Position(1) - xmin;% + offsetx/2;
	Position(2) = Position(2) - ymin + offsety/2;
	set(children(k), 'Position', Position);
	set(children(k), 'Units', childrenUnit{k});
end
set(fig, 'Units', Units)
drawnow
