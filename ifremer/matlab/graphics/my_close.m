% Fig = figure;
% imagesc(rand(20));
% my_close(Fig, 'TimeDelay', 10)
%
% Fig = figure;
% imagesc(rand(20));
% pause(2)
% my_close(Fig, 'MsgEnd')
%
% Fig = figure;
% imagesc(rand(20));
% Fig(2) = figure;
% imagesc(rand(20));
% pause(2)
% my_close(Fig, 'MsgEnd')
%
% Fig = figure;
% imagesc(rand(20));
% Fig(2) = figure;
% imagesc(rand(20));
% my_close(Fig, 'TimeDelay', 10)

function my_close(h, varargin)

for k0=1:length(h)
    my_close_unitaire(h(k0), varargin{:})
end


    function my_close_unitaire(h, varargin)
        
        function my_start_fcn(obj, event, string_arg) %#ok<INUSD>
            if ishandle(h)
                set(h, 'Name', [Name ' - ' num2str(TimeDelay) ' s'])
            end
        end
        function my_timer_fcn(obj, event, string_arg) %#ok<INUSD>
            TimeDelay = TimeDelay - 1;
            if ishandle(h)
                set(h, 'Name', [Name ' - ' num2str(TimeDelay) ' s'])
            end
        end
        function my_stop_fcn(obj, event, string_arg) %#ok<INUSD>
            close(h)
        end
        
        [varargin, MsgEnd] = getFlag(varargin, 'MsgEnd');
        if ~isempty(h) && ishandle(h)
            [varargin, TimeDelay] = getPropertyValue(varargin, 'TimeDelay', []); %#ok<ASGLU>
            
            if MsgEnd
                pppp = findobj(h, 'Type', 'axe');
                Title = get(get(pppp, 'Title'), 'String');
            end
            
            if MsgEnd || isempty(TimeDelay)
                delete(h)
            else
                Name = get(h, 'Name');
                t = timer('StartDelay', 0, 'Period', 1, 'TasksToExecute', TimeDelay, 'ExecutionMode', 'fixedRate');
                t.StartFcn = {@my_start_fcn, 'D�but'};
                t.StopFcn  = {@my_stop_fcn,  'Fin'};
                t.TimerFcn = {@my_timer_fcn, 'It�ration'};
                start(t)
            end
            
            if MsgEnd
                if iscell(Title)
                    msg = '';
                    for k=1:length(Title)
                        msg = [msg Title{k} ' ']; %#ok<AGROW>
                    end
                    msg(end) = [];
                else
                    msg = Title;
                end
                str1 = sprintf('Le traitement  "%s" est termin�.', msg);
                str2 = sprintf('"%s" is over.', msg);
                if isempty(TimeDelay)
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'GameIsOver');
                else
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'GameIsOver', 'TimeDelay', TimeDelay);
                end
            end
        end
    end
end
