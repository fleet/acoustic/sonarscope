% Sauvegarde de la table de couleurs dans un fichier png.
%
% Syntax
%   a = my_colorBarInPng(min, max, nomFichier)
%
% Name-only Arguments
%   min : Valeur min de la table de couleurs
%   max : Valeur max de la table de couleurs
%   nomFichier : Nom du fichier
%
% Output Arguments
%   N�ant (fichier de sortie g�n�r�).
%
% Examples
%   nomColorBar = ['ColorBar' '.png'];
%   nomFicColorBar = fullfile(tempdir, nomColorBar);
%   my_colorBarInPng(-4000, 6000, nomFicColorBar);
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function my_colorBarInPng(minval, maxval, nomfile, map)

%{
% Avant 21/09/2014 : �tait op�rationnel uniquement sur des images 'intensit�
bar = 1:256;
bar = repmat(bar, 10, 1);
xbar = linspace(minval, maxval, 256);
ybar = 1:10;
h = figure;
set(gcf, 'position',[200 100 130 400], 'visible', 'on');%figure en dehors de l'ecran
pcolor(ybar, xbar, bar');
%}

% Apr�s 21/09/2014 : adapt� � des images index�es. peut-�tre que ce n'est plus adapt�
% � des images en intensit�
n = size(map,1);
bar = 0:n;
bar = repmat(bar, 10, 1);
% xbar = linspace(minval-0.5, maxval+0.5, n+1); % Avant 25/01/2016
step = (maxval - minval) / (n);
xbar = linspace(minval-step, maxval+step, n+1); % Modif JMA le 25/01/2016
ybar = 1:10;

h = figure;
set(gcf, 'position',[200 100 130 400], 'visible', 'on');%figure en dehors de l'ecran
pcolor(ybar, xbar, bar');
shading flat;
colormap(map)


shading flat;
set(gca, 'TickDir', 'out');
set(gca, 'units', 'pixels');
set(gca, 'YAxisLocation', 'right');
set(gca, 'XTickLabel', []);
set(gca, 'XTick', []);
set(gca, 'fontsize', 11);
set(gca, 'XColor', 'black');
set(gca, 'YColor', 'black');
set(gca, 'position',[47 10 30 384]);%redimentionnement
text(-30, 78, Lang('Elevation en m','Elevation in meters'),'fontsize',14,'Units','pixel','rotation',90,'Color','black');
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'Color', 'white');

% Sauvegarde de la colorbar
saveas(gcf, nomfile, 'png');
my_close(h);

% Relecture pour transcodage du Alpha
A = imread(nomfile, 'png');
R = A(:,:,1);
G = A(:,:,2);
B = A(:,:,3);
sizeR = size(R);

% On initialise le canal alpha tout opaque
alpha = zeros(sizeR);
% On d�finit le blanc et le noir
blanc = 255;
noir = 0;
% On d�finit le canal alpha avec de la transparence l� o� il y a du
% blanc
%
indexBlanc = (R==blanc) & (G==blanc) & (B==blanc);
newIndexBlanc = indexBlanc;
newIndexBlanc(indexBlanc==1) = 0;
newIndexBlanc(indexBlanc==0) = 1;
alpha(newIndexBlanc) = 1;

% On remplace le noir par le blanc dans l'image
indexNoir = (R == noir) & (G == noir) & (B == noir);
% figure; imagesc(indexNoir);
%
R(indexNoir) = 255;
G(indexNoir) = 255;
B(indexNoir) = 255;
A(:,:,1) = R;
A(:,:,2) = G;
A(:,:,3) = B;
imwrite(A, nomfile, 'png', 'Alpha', alpha);
