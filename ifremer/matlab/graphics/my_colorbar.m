% figure;
% imagesc(rand(20)*10); colormap(jet(256));
% my_colorbar

function h = my_colorbar(varargin)

[varargin, ZLim] = getPropertyValue(varargin, 'ZLim', []);

GCF = gcf;
GCA = gca;

    function CallbackColorbar(varargin)
        %         positionColorbarDansFigure = get(h, 'Position')
        %         XLimColorbar = positionColorbarDansFigure(1:2);
        %         YLimColorbar = positionColorbarDansFigure(3:4);
        %         positionCurseurDansFigure = get(GCA, 'currentPoint')
        %
        %         if (positionCurseurDansFigure(1,1) < XLimColorbar(1)) || (positionCurseurDansFigure(1,1) > XLimColorbar(2)) || ...
        %                 (positionCurseurDansFigure(1,2) < YLimColorbar(1)) || (positionCurseurDansFigure(1,2) > YLimColorbar(2))
        %             return % Comment� pour l'instant
        %         end
        
        hImage = findobj(GCA, 'Type', 'Image');
        if ~isempty(hImage)
            hImcontrast = imcontrast(GCA);
            waitfor(hImcontrast)
            %             CLim = get(GCA, 'CLim');
        end
        
        hScatter = findobj(GCA, 'Type', 'scatter');
        if ~isempty(hScatter)
            GCA = ancestor(hScatter(1), 'Axes');
            if ~isempty(hScatter)
                pppp = [hScatter(:).CData];
                figTmp = figure('Position', [0 0 2 2]);
                himTmp = imagesc(pppp);
                haxTmp = ancestor(himTmp, 'Axes');
                linkprop([haxTmp; GCA], 'CLim');
                hImcontrast = imcontrast(haxTmp);
                waitfor(hImcontrast)
                close(figTmp)
                %             CLim = get(GCA, 'CLim');
            end
        end
    end

h = colorbar(varargin{:});
WindowButtonDownFcn = get(GCF, 'WindowButtonDownFcn');
if isempty(WindowButtonDownFcn)
    set(GCF, 'WindowButtonDownFcn', @CallbackColorbar);
else
    % On n'�crase pas la callback d�j� existante
end

if isempty(ZLim)
    return
else
    hi = get(h, 'Children');
    set(hi, 'YData', ZLim)
    if my_verLessThanR2014b
        set(h, 'YLim', ZLim)
    else
        set(h, 'Limits', ZLim)
    end
end

% TODO : la callback n'est pas atteignable car il n'y a pas de waitfor(h)
end
