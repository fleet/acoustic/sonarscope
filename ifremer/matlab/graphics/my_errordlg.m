% Message d'erreur incitant l'utilisateur � faire un "Save session" et � rapporter le bug � JMA
%
% Syntax
%   my_errordlg(code)
%
% Input Arguments
%   code : Code � transmettre
%
% Examples
%   my_errordlg('CodeDeBoeuf')
%
% See also my_warndlg; Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function my_errordlg(code)

str1 = sprintf('Une op�ration non conforme avec le code "%s"s''est produite.\nJe vous conseille de sauver la session et de la transmettre � sonarscope@ifremer.fr en d�crivant ce que vous tentiez de faire.', code);
str2 = sprintf('A non conform operation occured with code "%s".\nI recommand you save the session and send it to sonarscope@ifremer.fr with the description of what you were doing.', code);
my_warndlg(Lang(str1,str2), 1);
