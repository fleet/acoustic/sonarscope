% R�ponse � une question
%
% Syntax
%   [rep, validation] = my_inputdlg(msg, rep1, ...)
%
% Input Arguments
%   nomVar : Liste des parametres (cellarray of char)
%   value  : Valeurs (cellarray of char)
%
% Name-Value Pair Arguments
%   Entete : Titre de la fen�tre ('IFREMER - SonarScope par defaut)
%
% Output Arguments
%   value      :
%   validation : 0 si sortie par "Cancel', 1 sinon
%
% Examples
%   nomVar = {'Enter the matrix size for x^2' ; 'Enter the colormap name:'};
%   value = {'20' ; 'hsv'};
%   [value, validation] = my_inputdlg(nomVar, value)
%
% See also listdlg Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [value, validation] = my_inputdlg(nomVar, value, varargin)

[varargin, Entete]      = getPropertyValue(varargin, 'Entete',      'IFREMER-SonarScope');
[varargin, numlines]    = getPropertyValue(varargin, 'numlines',    1);
[varargin, Interpreter] = getPropertyValue(varargin, 'Interpreter', 'tex'); %#ok<ASGLU>

if ischar(nomVar)
    nomVar = {nomVar};
end
if ischar(value)
    value = {value};
end

for k=1:length(value)
    if isnumeric(value{k})
        value{k} = num2str(value{k});
        flagConvertDouble(k) = 1; %#ok<AGROW>
    else
        flagConvertDouble(k) = 0; %#ok<AGROW>
    end
end

options.Resize      = 'on'; % CA NE FONCTIONNE PLUS AVEC ON.    POUQUOI ??????????????????
options.WindowStyle = 'normal';
options.Interpreter = Interpreter;
nomVar{1} = [nomVar{1} repmat(' ', 1, 100-length(nomVar{1})) '.']; % Pour donner une largeur suffisante

valueIn = value;
flag = 0;
while flag == 0
    pppp = valueIn{1};
    if size(pppp, 2) > 1
        pppp = pppp';
        pppp = pppp(:);
        pppp = pppp';
        valueIn{1} = pppp;
    end
    value = inputdlg(nomVar, Entete, numlines, valueIn, options);
    if isempty(value)
        validation = 0;
    else
        validation = 1;
    end
    
    flag = 1;
    for k=1:length(value)
        if flagConvertDouble(k)
            value{k} = str2num(value{k}); %#ok<ST2NM>
            if isempty(value{k})
                str1 = sprintf('La ligne %d doit �tre un nombre', k);
                str2 = sprintf('Line %d must be a number', k);
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
            end
        end
    end
end
