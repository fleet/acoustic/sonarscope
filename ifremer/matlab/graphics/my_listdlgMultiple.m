% Selection d'un ou plusieurs items dans une liste
%
% Syntax
%   [rep, validation] = my_listdlgMultiple(msg, ListString, ...)
%
% Input Arguments
%   msg        : Intitule de la liste
%   ListString : Liste des items (cell array of char)
%
% Name-Value Pair Arguments
%   InitialValue  : Valeur initiale (1 par defaut)
%   ListSize      : Taille de la fenetre (adaptee a ListString par defaut)
%   Entete        : Titre de la fenetre
%
% Examples
%   str{1} = 'toto';  str{2} = 'titi';  str{3} = 'tutu';
%   [rep, validation] = my_listdlgMultiple('List of words', str)
%   [rep, validation] = my_listdlgMultiple('List of words', str, 'InitialValue', 2)
%
% See also my_listdlg listdlg Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [rep, validation] = my_listdlgMultiple(msg, ListString, varargin)

drawnow
pause(0.5);

if ischar(ListString)
    for k=size(ListString,1):-1:1
        str{k} = ListString(k,:);
    end
    ListString = str;
end

N = length(ListString);
if N == 0
    rep = [];
    validation = 0;
    return
end

H = 22*(1+N);
ScreenSize = get(0, 'ScreenSize');
H = min(H, ScreenSize(4)-30*10);

L = 7*length(msg);
for k=1:N
    L = max(L, 6*(10+length(ListString{k})));
end
L = max(L, 250);
L = min(L, ScreenSize(3)-60);

[varargin, InitialValue]  = getPropertyValue(varargin, 'InitialValue',  1);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
[varargin, ListSize]      = getPropertyValue(varargin, 'ListSize',      [L H]);
[varargin, Entete]        = getPropertyValue(varargin, 'Entete',        'IFREMER-SonarScope'); %#ok<ASGLU>

if isequal(InitialValue, 0)
    InitialValue = [];
end
% InitialValue = max(1, min(InitialValue, N));

% [rep, validation] = listdlg(...
[rep, validation] = listdlg_MatlabModifiee(...
    'Name',          Entete, ...
    'PromptString',  msg, ...
    'ListString',    ListString, ...
    'InitialValue',  InitialValue, ...
    'SelectionMode', SelectionMode, ...
    'ListSize',      ListSize);
drawnow
pause(0.5);
end



% ATTENTION : Le listdlg de Matlab ne permet pas e passer une valeur initiale vide.
% La partie qui suit est un simple copi�-coll� de la fonction
% listdlg de Matlab + de 2 fonctions priv�es (toolbox\matlab\uitools\private\getnicedialoglocation et
% toolbox\matlab\uitools\private\setdefaultbutton)
% avec le modifications suivantes :
% - Mise en commentaire des lignes
%   if isempty(initialvalue)
%       initialvalue = 1;
%   end
% - Rajout de quelques end pour terminer les sous-fonctions



function [selection,value] = listdlg_MatlabModifiee(varargin)
%LISTDLG  List selection dialog box.
%   [SELECTION,OK] = LISTDLG('ListString',S) creates a modal dialog box
%   which allows you to select a string or multiple strings from a list.
%   SELECTION is a vector of indices of the selected strings (length 1 in
%   the single selection mode).  This will be [] when OK is 0.  OK is 1 if
%   you push the OK button, or 0 if you push the Cancel button or close the
%   figure.
%
%   Double-clicking on an item or pressing <CR> when multiple items are
%   selected has the same effect as clicking the OK button.  Pressing <CR>
%   is the same as clicking the OK button. Pressing <ESC> is the same as
%   clicking the Cancel button.
%
%   Inputs are in parameter,value pairs:
%
%   Parameter       Description
%   'ListString'    cell array of strings for the list box.
%   'SelectionMode' string; can be 'Single' or 'Multiple'; defaults to
%                   'Multiple'.
%   'ListSize'      [width height] of listbox in pixels; defaults
%                   to [160 300].
%   'InitialValue'  vector of indices of which items of the list box
%                   are initially selected; defaults to the first item.
%   'Name'          String for the figure's title; defaults to ''.
%   'PromptString'  string matrix or cell array of strings which appears
%                   as text above the list box; defaults to {}.
%   'OKString'      string for the OK button; defaults to 'OK'.
%   'CancelString'  string for the Cancel button; defaults to 'Cancel'.
%
%   A 'Select all' button is provided in the multiple selection case.
%
%   Example:
%     d = dir;
%     str = {d.name};
%     [s,v] = listdlg('PromptString', 'Select a file:',...
%                     'SelectionMode', 'Single',...
%                     'ListString', str)
%
%  See also DIALOG, ERRORDLG, HELPDLG, INPUTDLG,
%    MSGBOX, QUESTDLG, WARNDLG.

%   Copyright 1984-2005 The MathWorks, Inc.
%   $Revision: 1.20.4.6 $  $Date: 2005/10/28 15:54:55 $

%   'uh'            uicontrol button height, in pixels; default = 22.
%   'fus'           frame/uicontrol spacing, in pixels; default = 8.
%   'ffs'           frame/figure spacing, in pixels; default = 8.

% simple test:
%
% d = dir; [s,v] = listdlg('PromptString','Select a file:','ListString',{d.name});
%

narginchk(1,inf) % Modif JMA le 17/03/2016

figname = '';
smode = 2;   % (multiple)
promptstring = {};
liststring = [];
listsize = [160 300];
initialvalue = [];
okstring = 'OK';
% cancelstring = 'Cancel';
fus = 8;
ffs = 8;
uh = 22;

if mod(length(varargin),2) ~= 0
    % input args have not com in pairs, woe is me
    error('MATLAB:listdlg:InvalidArgument', 'Arguments to LISTDLG must come param/value in pairs.')
end
for k=1:2:length(varargin)
    switch lower(varargin{k})
        case 'name'
            figname = varargin{k+1};
        case 'promptstring'
            promptstring = varargin{k+1};
        case 'selectionmode'
            switch lower(varargin{k+1})
                case 'single'
                    smode = 1;
                case 'multiple'
                    smode = 2;
            end
        case 'listsize'
            listsize = varargin{k+1};
        case 'liststring'
            liststring = varargin{k+1};
        case 'initialvalue'
            initialvalue = varargin{k+1};
        case 'uh'
            uh = varargin{k+1};
        case 'fus'
            fus = varargin{k+1};
        case 'ffs'
            ffs = varargin{k+1};
        case 'okstring'
            okstring = varargin{k+1};
        case 'cancelstring'
            %             cancelstring = varargin{k+1};
        otherwise
            error('MATLAB:listdlg:UnknownParameter', ['Unknown parameter name passed to LISTDLG.  Name was ' varargin{k}])
    end
end

if ischar(promptstring)
    promptstring = cellstr(promptstring);
end

% if isempty(initialvalue)
%     initialvalue = 1;
% end

if isempty(liststring)
    error('MATLAB:listdlg:NeedParameter', 'ListString parameter is required.')
end
%TODO : check if each element of liststring is not empty

ex = get(0,'defaultuicontrolfontsize')*1.7;  % height extent per line of uicontrol text (approx)

fp = get(0,'defaultfigureposition');
w = 2*(fus+ffs)+listsize(1);
h = 2*ffs+6*fus+ex*length(promptstring)+listsize(2)+uh+(smode==2)*(fus+uh);
fp = [fp(1) fp(2)+fp(4)-h w h];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
    };

% isempty(liststring)
n = length(liststring);
flag = true(n,1);
for k=1:n
    flag(k) = ~isempty(liststring{k});
    if iscell(liststring{k})
        liststring{k} = liststring{k}{1}; %#ok<AGROW>
    end
end
liststring = cellstr(liststring(flag));

fig = figure(fig_props{:});

if ~isempty(promptstring)
    prompt_text = uicontrol('style', 'text', 'string', promptstring,...
        'horizontalalignment', 'left',...
        'position', [ffs+fus fp(4)-(ffs+fus+ex*length(promptstring)) ...
        listsize(1) ex*length(promptstring)]); %#ok
end

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;

listbox = uicontrol('style', 'listbox',...
    'position',[ffs+fus ffs+uh+4*fus+((smode==2)-0.5)*(fus+uh) listsize],...
    'string',liststring,...
    'backgroundcolor', 'w',...
    'max', smode,...
    'tag', 'listbox',...
    'value', initialvalue, ...
    'callback', {@doListboxClick});

ok_btn = uicontrol('style', 'pushbutton',...
    'string', okstring,...
    'position', [ffs+fus ffs+fus btn_wid uh],...
    'callback', {@doOK,listbox});

cancel_btn = uicontrol('style', 'pushbutton',...
    'string', Lang('Quitter', 'Cancel') ,...
    'position', [ffs+2*fus+btn_wid ffs+fus btn_wid uh],...
    'callback', {@doCancel,listbox});

if smode == 2
    selectNone_btn = uicontrol('style', 'pushbutton',...
        'string', Lang('Aucune','None'),...
        'position', [ffs+fus 2*fus+ffs+uh btn_wid uh],...
        'tag','selectNone_btn',...
        'callback',{@doSelectNone, listbox});
    
    if length(initialvalue) == length(liststring)
        %         set(selectNone_btn,'enable','off')
    end
    set(listbox,'callback',{@doListboxClick, selectNone_btn})
    
    selectall_btn = uicontrol('style', 'pushbutton',...
        'string',  Lang('Tout s�lectionner', 'Select all'),...
        'position', [ffs+2*fus+btn_wid 2*fus+ffs+uh btn_wid uh],...
        'tag', 'selectall_btn',...
        'callback', {@doSelectAll, listbox});
    
    if length(initialvalue) == length(liststring)
        %         set(selectall_btn,'enable','off')
    end
    set(listbox,'callback',{@doListboxClick, selectall_btn})
end

set([fig, ok_btn, cancel_btn, listbox], 'keypressfcn', {@doKeypress, listbox});

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));
% Make ok_btn the default button.
setdefaultbutton(fig, ok_btn);

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on');
drawnow;

try
    % Give default focus to the listbox *after* the figure is made visible
    uicontrol(listbox);
    uiwait(fig);
catch %#ok<CTCH>
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'ListDialogAppData__')
    ad = getappdata(0,'ListDialogAppData__');
    selection = ad.selection;
    value = ad.value;
    rmappdata(0,'ListDialogAppData__')
else
    % figure was deleted
    selection = [];
    value = 0;
end

% Ne sert a rien ici
% WorkInProgress

% figure, OK and Cancel KeyPressFcn
    function doKeypress(src, evd, listbox) %#ok
        switch evd.Key
            case 'escape'
                doCancel([],[],listbox);
        end
    end

% OK callback
    function doOK(ok_btn, evd, listbox) %#ok
        if (~isappdata(0, 'ListDialogAppData__'))
            ad.value = 1;
            ad.selection = get(listbox,'value');
            setappdata(0,'ListDialogAppData__',ad);
            delete(gcbf);
        end
    end

% Cancel callback
    function doCancel(cancel_btn, evd, listbox) %#ok
        ad.value = 0;
        ad.selection = [];
        setappdata(0,'ListDialogAppData__',ad)
        delete(gcbf);
    end

% SelectAll callback
    function doSelectAll(selectall_btn, evd, listbox) %#ok
        %         set(selectall_btn,'enable','off')
        set(listbox, 'value', 1:length(get(listbox,'string')));
    end

% SelectNone callback
    function doSelectNone(selectall_btn, evd, listbox) %#ok
        %         set(selectall_btn, 'enable','off')
        set(listbox, 'value', []);
    end

% Listbox callback
    function doListboxClick(listbox, evd, selectall_btn) %#ok
        % if this is a doubleclick, doOK
        if strcmp(get(gcbf,'SelectionType'),'open')
            doOK([],[],listbox);
        elseif nargin == 3
            if length(get(listbox,'string'))==length(get(listbox,'value'))
                set(selectall_btn,'enable','off')
            else
                set(selectall_btn,'enable','on')
            end
        end
    end
end




function figure_size = getnicedialoglocation(figure_size, figure_units)
% adjust the specified figure position to fig nicely over GCBF
% or into the upper 3rd of the screen

%  Copyright 1999-2006 The MathWorks, Inc.
%  $Revision: 1.1.6.3 $

%%% PLEASE NOTE %%%%%
%%% This file has also been copied into:
%%% matlab/toolbox/ident/idguis
%%% If this functionality is changed, please
%%% change it also in idguis.
%%% PLEASE NOTE %%%%%

parentHandle = gcbf;
propName = 'Position';
if isempty(parentHandle)
    parentHandle = 0;
    propName = 'ScreenSize';
end

old_u = get(parentHandle,'Units');
set(parentHandle,'Units',figure_units);
container_size=get(parentHandle,propName);
set(parentHandle,'Units',old_u);

figure_size(1) = container_size(1)  + 1/2*(container_size(3) - figure_size(3));
figure_size(2) = container_size(2)  + 2/3*(container_size(4) - figure_size(4));
end


function setdefaultbutton(figHandle, btnHandle)
% WARNING: This feature is not supported in MATLAB and the API and
% functionality may change in a future release.

%SETDEFAULTBUTTON Set default button for a figure.
%  SETDEFAULTBUTTON(BTNHANDLE) sets the button passed in to be the default button
%  (the button and callback used when the user hits "enter" or "return"
%  when in a dialog box.
%
%  This function is used by inputdlg.m, msgbox.m, questdlg.m and
%  uigetpref.m.
%
%  Example:
%
%  f = figure;
%  b1 = uicontrol('style', 'pushbutton', 'string', 'first', ...
%       'position', [100 100 50 20]);
%  b2 = uicontrol('style', 'pushbutton', 'string', 'second', ...
%       'position', [200 100 50 20]);
%  b3 = uicontrol('style', 'pushbutton', 'string', 'third', ...
%       'position', [300 100 50 20]);
%  setdefaultbutton(b2);
%

%  Copyright 2005 The MathWorks, Inc.

% Nargin Check
if nargin<1, error('Too few arguments for setdefaultbutton'); end
if nargin>2, error('Too many arguments for setdefaultbutton'); end

useHGDefaultButton(figHandle, btnHandle);


    function useHGDefaultButton(figHandle, btnHandle) %#ok
        % First get the position of the button.
        btnPos = getpixelposition(btnHandle);
        
        % Next calculate offsets.
        leftOffset   = btnPos(1) - 1;
        bottomOffset = btnPos(2) - 2;
        widthOffset  = btnPos(3) + 3;
        heightOffset = btnPos(4) + 3;
        
        % Create the default button look with a uipanel.
        % Use black border color even on Mac or Windows-XP (XP scheme) since
        % this is in natve figures which uses the Win2K style buttons on Windows
        % and Motif buttons on the Mac.
        h1 = uipanel(get(btnHandle, 'Parent'), 'HighlightColor', 'black', ...
            'BorderType', 'etchedout', 'units', 'pixels', ...
            'Position', [leftOffset bottomOffset widthOffset heightOffset]);
        
        % Make sure it is stacked on the bottom.
        uistack(h1, 'bottom');
    end
end
