%   figure;
%   x = -2*pi:0.01:2*pi;
%   my_patch(x, sin(x), 'r')

function h = my_patch(varargin)

%# function traiter_signal

% Fonctions qui devaient �tre d�clar�es en external avant
% %A VALIDER EN DEPLOYE
% ExportCourbe DerivateCourbe LineColorCourbe MarkerColorCourbe
% callback_SelfCorrelation LineWidthCourbe LineStyleCourbe
% MarkerWidthCourbe MarkerStyleCourbe CourbeFit

[varargin, flag_time] = getPropertyValue(varargin, 'cl_time',       0);
[varargin, Name]      = getPropertyValue(varargin, 'Name',          []);
[varargin, cmenu]     = getPropertyValue(varargin, 'UIContextMenu', []);

if isempty(cmenu)
    cmenu = uicontextmenu;
end

if isa(varargin{1}, 'cl_time') || flag_time
    CallbackOtherFigure = 'cloneCourbe(gco, ''cl_time'', ''1'')';
else
    CallbackOtherFigure = 'cloneCourbe(gco)';
end

if ~isempty(Name)
    uimenu(cmenu, 'Text', Name)
end
uimenu(cmenu, 'Text', 'Copy in a new figure', 'Callback', CallbackOtherFigure);

uimenu(cmenu, 'Text', 'Export (ascii)',       'Callback', @ExportCourbe);

h1 = uimenu(cmenu, 'Text', 'Patch');
h2 = uimenu(cmenu, 'Parent', h1, 'Text', 'Style');
uimenu(h2, 'Text', '-',       'Callback', @LineStyleCourbe);
uimenu(h2, 'Text', 'none',    'Callback', @LineStyleCourbe);

h2 = uimenu(cmenu, 'Parent', h1, 'Text', 'Width');
uimenu(h2, 'Text', '0.5',     'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '1',       'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '2',       'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '3',       'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '4',       'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '6',       'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '8',       'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '10',      'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '15',      'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '20',      'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '25',      'Callback', @LineWidthCourbe);
uimenu(h2, 'Text', '30',      'Callback', @LineWidthCourbe);

uimenu(cmenu, 'Parent', h1, 'Text', 'Color', 'Callback', @LineColorCourbe);
uimenu(cmenu, 'Parent', h1, 'Text', 'Alpha', 'Callback', @PatchAlpha);

h1 = uimenu(cmenu, 'Text', 'Marker');
h2 = uimenu(cmenu, 'Parent', h1, 'Text', 'Style');
uimenu(h2, 'Text', '+',         'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', 'o',         'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', '*',         'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', '.',         'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', 'x',         'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', 'square',    'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', 'v',         'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', '^',         'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', 'pentagram', 'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', 'hexagram',  'Callback', @MarkerStyleCourbe);
uimenu(h2, 'Text', 'none',      'Callback', @MarkerStyleCourbe);

h2 = uimenu(cmenu, 'Parent', h1, 'Text', 'Width');
uimenu(h2, 'Text', '0.5',     'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '1',       'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '2',       'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '3',       'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '4',       'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '6',       'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '8',       'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '10',      'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '15',      'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '20',      'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '25',      'Callback', @MarkerWidthCourbe);
uimenu(h2, 'Text', '30',      'Callback', @MarkerWidthCourbe);

uimenu(cmenu, 'Parent', h1, 'Text', 'Color', 'Callback', @MarkerColorCourbe);

uimenu(cmenu, 'Text', 'Delete', 'Callback', 'delete(gco)');

h = patch(varargin{:});
set(h, 'UIContextMenu', cmenu);
set(h, 'FaceAlpha', 0.25);
axis tight
dragplot

    function PatchAlpha(varargin)
        GCO = gco;
        FaceAlpha = get(GCO, 'FaceAlpha');
        
        str1 = 'TODO';
        str2 = 'Patch transparency';
        p = ClParametre('Name', Lang('Transparence', 'Transparency'), ...
            'MinValue', 0, 'MaxValue', 1, 'Value', FaceAlpha);
        a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
        % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
        a.sizes = [0 -2 0 -2 0 -1 0 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        FaceAlpha = a.getParamsValue;
        
        set(GCO, 'FaceAlpha', FaceAlpha)
    end

    function LineStyleCourbe(varargin)
        GCO  = gco;
        GCBO = gcbo;
%         Label = get(GCBO, 'Label');
        Label = get(GCBO, 'Text'); % Modif JMA le 20/05/2021
        set(GCO, 'LineStyle', Label)
    end

    function LineColorCourbe(varargin)
        set(gco, 'EdgeColor', uisetcolor)
        Color = get(gco, 'EdgeColor');
        set(gco, 'FaceColor', Color)
    end

    function LineWidthCourbe(varargin)
        GCO  = gco;
        GCBO = gcbo;
%         Label = get(GCBO, 'Label');
        Label = get(GCBO, 'Text'); % Modif JMA le 20/05/2021
        W = str2double(Label);
        set(GCO, 'LineWidth', W)
    end

    function MarkerStyleCourbe(varargin)
        GCO  = gco;
        GCBO = gcbo;
%         Label = get(GCBO, 'Label');
        Label = get(GCBO, 'Text'); % Modif JMA le 20/05/2021
        set(GCO, 'Marker', Label)
    end

    function MarkerColorCourbe(varargin)
        set(gco, 'MarkerEdgeColor', uisetcolor)
    end

    function MarkerWidthCourbe(varargin)
        GCO  = gco;
        GCBO = gcbo;
%         Label = get(GCBO, 'Label');
        Label = get(GCBO, 'Text'); % Modif JMA le 20/05/2021
        W = str2double(Label);
        set(GCO, 'MarkerSize', W)
    end

    function ExportCourbe(varargin)
        GCO = gco;
        hAxeIn = get(GCO, 'parent');
        Titre = get(get(hAxeIn, 'Title'), 'String');
        
        XData = get(GCO, 'XData');
        YData = get(GCO, 'YData');
        ZData = get(GCO, 'ZData');
        
        [flag, nomFic] = my_uiputfile('*.txt', 'Give a file name');
        if ~flag
            return
        end
        fid = fopen(nomFic, 'w+');
        if fid == -1
            str = sprintf('Impossible to write %s', nomFic);
            my_warndlg(str, 1);
            return
        end
        
        if isempty(ZData)
            fprintf(fid, 'x\t%s\n', Titre);
            for i=1:length(XData)
                fprintf(fid, '%f\t%f\n', XData(i), YData(i));
            end
        else
            fprintf(fid, 'x\ty\t%s\n', Titre);
            for i=1:length(XData)
                fprintf(fid, '%f\t%f\t%f\n', XData(i), YData(i), ZData(i));
            end
        end
        fclose(fid);
    end

end
