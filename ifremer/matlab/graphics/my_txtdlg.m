% S�lection d'un ou plusieurs items dans une liste
%
% Syntax
%   [rep, validation] = my_txtdlg(msg, ListString, ...)
%
% Input Arguments
%   msg        : Intitule de la liste
%   ListString : Liste des items (cell array of char)
%
% Name-Value Pair Arguments
%   InitialValue  : Valeur initiale (1 par defaut)
%   SelectionMode : 'Multiple' ou 'Single' ('Multiple' par defaut)
%   ListSize      : Taille de la fen�tre (adapt�e a ListString par defaut)
%   windowstyle   : {'modal'} | 'normal' --> Normal rend la main; modal ne
%                   la rend pas
%
% Examples
%   str{1} = 'toto';  str{2} = 'titi';  str{3} = 'tutu';
%   [rep, validation] = my_txtdlg('Select your favorite nickname', str)
%   [rep, validation] = my_txtdlg('Select your favorite nickname', str, 'InitialValue', 2)
%   [rep, validation] = my_txtdlg('Select your favorite nickname', str, 'SelectionMode', 'Single')
%   [rep, validation] = my_txtdlg('Select your favorite nickname', str, 'windowstyle', 'normal')
%
% See also listdlg Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [rep, validation] = my_txtdlg(msg, ListString, varargin)

[varargin, windowstyle] = getPropertyValue(varargin, 'windowstyle', 'modal');

if ischar(ListString)
    for k=1:size(ListString,1)
        str{k} = ListString(k,:); %#ok
    end
    ListString = str;
end

N = length(ListString);
if N == 0
    rep = [];
    validation = 0;
    return
end

H = 22*(1+N);
ScreenSize = get(0, 'ScreenSize');
H = min(H, ScreenSize(4)-30*10);

L = 7*length(msg);
for k=1:N
    L = max(L, 6*(10+length(ListString{k})));
end
L = max(L, 250);
L = min(L, ScreenSize(3)-60);

[varargin, Entete]        = getPropertyValue(varargin, 'Entete',        'IFREMER-SonarScope');
[varargin, InitialValue]  = getPropertyValue(varargin, 'InitialValue',  1);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
[varargin, ListSize]      = getPropertyValue(varargin, 'ListSize',      [L H]); %#ok<ASGLU>

InitialValue = max(1, min(InitialValue, N));

for k=1:length(ListString)
    if isnumeric(ListString{k})
        ListString{k} = str2double(ListString{k});
    end
    if isempty(ListString{k})
        ListString{k} = ' ';
    end
    ListString{k} = strrep(ListString{k}, char(0), ' ');
end

[rep, validation] = txtdlg(...
    'Name',         Entete, ...
    'okstring',     'Close', ...
    'PromptString', msg, ...
    'ListString',   ListString, ...
    'InitialValue', InitialValue, ...
    'SelectionMode', SelectionMode, ...
    'ListSize',      ListSize, ...
    'windowstyle',  windowstyle);
WorkInProgress


%LISTDLG  List selection dialog box.
%   [SELECTION,OK] = LISTDLG('ListString',S) creates a modal dialog box
%   which allows you to select a string or multiple strings from a list.
%   SELECTION is a vector of indices of the selected strings (length 1 in
%   the single selection mode).  This will be [] when OK is 0.  OK is 1 if
%   you push the OK button, or 0 if you push the Cancel button or close the
%   figure.
%
%   Double-clicking on an item or pressing <CR> when multiple items are
%   selected has the same effect as clicking the OK button.  Pressing <CR>
%   is the same as clicking the OK button. Pressing <ESC> is the same as
%   clicking the Cancel button.
%
%   Inputs are in parameter,value pairs:
%
%   Parameter       Description
%   'ListString'    cell array of strings for the list box.
%   'SelectionMode' string; can be 'Single' or 'Multiple'; defaults to
%                   'Multiple'.
%   'ListSize'      [width height] of listbox in pixels; defaults
%                   to [160 300].
%   'InitialValue'  vector of indices of which items of the list box
%                   are initially selected; defaults to the first item.
%   'Entete'          String for the figure's title; defaults to ''.
%   'PromptString'  string matrix or cell array of strings which appears
%                   as text above the list box; defaults to {}.
%   'OKString'      string for the OK button; defaults to 'OK'.
%
%   A 'Select all' button is provided in the multiple selection case.
%
%   Example:
%     d = dir;
%     str = {d.name};
%     [s,v] = listdlg('PromptString', 'Select a file:',...
%                     'SelectionMode', 'Single',...
%                     'ListString',str)

%   Copyright 1984-2002 The MathWorks, Inc.
%   $Revision: 1.20.4.2 $  $Date: 2002/12/24 12:09:08 $

%   'uh'            uicontrol button height, in pixels; default = 22.
%   'fus'           frame/uicontrol spacing, in pixels; default = 8.
%   'ffs'           frame/figure spacing, in pixels; default = 8.

% simple test:
%
% d = dir; [s,v] = listdlg('PromptString','Select a file:','ListString',{d.name});
%
function [selection,value] = txtdlg(varargin)

[varargin, windowstyle] = getPropertyValue(varargin, 'windowstyle', 'modal');

narginchk(1,inf)

figname = '';
smode = 1;   % (multiple)
promptstring = {};
liststring = [];
listsize = [160 300];
initialvalue = [];
okstring = 'OK';
fus = 8;
ffs = 8;
uh = 22;

if mod(length(varargin),2) ~= 0
    % input args have not com in pairs, woe is me
    error('Arguments to LISTDLG must come param/value in pairs.')
end
for k=1:2:length(varargin)
    switch lower(varargin{k})
        case 'name'
            figname = varargin{k+1};
        case 'promptstring'
            promptstring = varargin{k+1};
        case 'selectionmode'
            switch lower(varargin{k+1})
                case 'single'
                    smode = 1;
                case 'multiple'
                    smode = 2;
            end
        case 'listsize'
            listsize = varargin{k+1};
        case 'liststring'
            liststring = varargin{k+1};
        case 'initialvalue'
            initialvalue = varargin{k+1};
        case 'uh'
            uh = varargin{k+1};
        case 'fus'
            fus = varargin{k+1};
        case 'ffs'
            ffs = varargin{k+1};
        case 'okstring'
            okstring = varargin{k+1};
        otherwise
            error(['Unknown parameter name passed to LISTDLG.  Name was ' varargin{k}])
    end
end

if ischar(promptstring)
    promptstring = cellstr(promptstring);
end

if isempty(initialvalue)
    initialvalue = 1;
end

if isempty(liststring)
    error('ListString parameter is required.')
end

ex = get(0,'defaultuicontrolfontsize')*1.7;  % height extent per line of uicontrol text (approx)

fp = get(0,'defaultfigureposition');
w = max(290, 2*(fus+ffs)+listsize(1));
h = 2*ffs+6*fus+ex*length(promptstring)+listsize(2)+uh+(smode==2)*(fus+uh);
fp = [fp(1) fp(2)+fp(4)-h w h];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname, ...
    'color'                  get(0,'defaultUicontrolBackgroundColor'), ...
    'resize'                 'off', ...
    'numbertitle'            'off', ...
    'menubar'                'none', ...
    'windowstyle'            windowstyle, ...
    'visible'                'off', ...
    'createfcn'              '',    ...
    'position'               fp,   ...
    'closerequestfcn'        'delete(gcbf)'};

liststring=cellstr(liststring);

fig = figure(fig_props{:});

if ~isempty(promptstring)
    uicontrol('style','text','string',promptstring,...
        'horizontalalignment','left',...
        'position',[ffs+fus fp(4)-(ffs+fus+ex*length(promptstring)) ...
        listsize(1) ex*length(promptstring)]);
end

btn_wid = (fp(3)-2*(ffs+fus)-fus) + 2;

listbox = uicontrol('style', 'listbox',...
    'position', [ffs+fus ffs+uh+4*fus listsize],...
    'string', liststring,...
    'backgroundcolor', 'w',...
    'max', smode,...
    'tag', 'listbox',...
    'value', initialvalue, ...
    'callback', {@doListboxClick});

uicontrol('style','frame',...
    'position',[ffs+fus-1 ffs+fus-1 btn_wid uh+2],...
    'backgroundcolor', 'k');

ok_btn = uicontrol('style','pushbutton',...
    'string',okstring,...
    'position',[ffs+fus ffs+fus btn_wid uh],...
    'callback',{@doOK, listbox});

set([fig,listbox,ok_btn], 'keypressfcn', {@doKeypress, listbox})

% set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));
Position = get(fig, 'Position');
centrageFig(Position(3), Position(4), 'Fig', fig);

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on');

try
    if strcmp(windowstyle, 'modal')
        uiwait(fig);
    end
catch %#ok<CTCH>
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'ListDialogAppData')
    ad = getappdata(0,'ListDialogAppData');
    selection = ad.selection;
    value = ad.value;
    rmappdata(0,'ListDialogAppData')
else
    % figure was deleted
    selection = [];
    value = 0;
end

function doKeypress(fig_h, evd, listbox) %#ok
switch evd.Key
    case {'return','space'}
        doOK([],[],listbox);
end

function doOK(ok_btn, evd, listbox) %#ok
ad.value = 1;
ad.selection = get(listbox,'value');
setappdata(0,'ListDialogAppData',ad)
delete(gcbf);

function doClick(listbox)
selection = get(listbox,'value');
String = get(listbox, 'String');
disp(String{selection})


function doListboxClick(listbox, evd, selectall_btn) %#ok
% if this is a doubleclick, doOK
if strcmp(get(gcbf,'SelectionType'),'open')
    doClick(listbox);
elseif nargin == 3
    if length(get(listbox,'string'))==length(get(listbox,'value'))
        set(selectall_btn,'enable','off')
    else
        set(selectall_btn,'enable','on')
    end
end

