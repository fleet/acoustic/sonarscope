% Affichge d'une waitbar tous les 1/100 de pas
%
% Syntax
%    my_waitbar(k, N, hw, ...)
%
% Input Arguments
%   str : Intitule de la fenetre
%
% Name-Value Pair Arguments
%   Title2 : Compl�ment du titre
%
% Output Arguments
%   hw : Handle
%
% Examples
%   N = 1000
%   hw = create_waitbar('Processing', 'N', N);
%   for k=1:N
%     my_waitbar(k, N, hw)
%     pause(0.05)
%   end
%   my_close(hw) % Or my_close(hw, 'MsgEnd')
%
% See also create_waitbar Authors
% Authors : JMA
%
% TODO : il faudrait faire un IHM avec icone Pause, Avance et Stop

function my_waitbar(k, N, hw, varargin)

%{
% D�but ajout JMA le 08/04/2013 Comment� car �a prend encore plus de temps
% L'affichage des messages prenait pas mal de temps lors du chargement de
% nombreuses images ou pendant les d�codages de fichiers all, ...
% On se limite � un affichage par seconde
persistent persistent_Time persistent_hw

if isempty(persistent_hw)
    persistent_hw = hw;
else
    if hw == persistent_hw
        if isempty(persistent_Time)
            persistent_Time = now;
        else
            NOW = now;
            diff = NOW - persistent_Time;
            if diff < 1.1574e-06 %  0.1 seconde : 1.1574e-06 = 0.1 / (24*3600)
                return
            end
            persistent_Time = NOW;
        end
    else
        persistent_hw = hw;
    end
end
% Fin ajout JMA le 08/04/2013
%}

if (N == 1) || isempty(hw) % || ~ishandle(hw)
    return
end

if ~ishandle(hw)
    str1 = 'Op�ration arr�t�e volontairement par arr�t de la waitbar.';
    str2 = 'Processing ended by user (delete waitbar)';
    exception = MException('VerifyOutput:OutOfBounds', Lang(str1,str2));
    throw(exception);
end

% x = k/N;
x = (k-1)/N; % Modif JMA le 04/02/2021
if N < 100
    waitbar(x, hw);
    my_waitbar_info(hw, k, N, varargin{:})
else
    if mod(k-1, floor(N/50)) == 0
        %     c = min(1, 5000/N);
        %     if mod(floor((k-1)*c), floor(N*c/50)) == 0
        waitbar(x, hw);
        my_waitbar_info(hw, k, N, varargin{:})
    end
end


function my_waitbar_info(hw, k, N, varargin)

if ~ishandle(hw)
    return
end

% ht = get(hTitre, 'Title');
UserData = get(hw, 'UserData');
if isempty(UserData)
    UserData.hAxe   = findobj(hw, 'Type', 'axes');
    UserData.hTitle = get(UserData.hAxe, 'Title');
    UserData.Title  = get(UserData.hTitle, 'String');
    UserData.tStart = now;
    UserData.kDeb   = k;
    UserData.N      = N;
%     UserData.Title = sprintf('%s\n %s', UserData.Title,  datestr(UserData.tStart));
    UserData.BoutonCancel = findobj(hw, 'Type', 'uicontrol');
    set(hw, 'UserData', UserData);
else
    if k > 1
        NOW = now;
        dt = NOW - UserData.tStart;
        dtElementaire = dt / (k - UserData.kDeb);
        if ~isfinite(dtElementaire)
            return
        end
        
        %         if  (dtElementaire*N) > 0.006944444444444 %  Correspond � 10 minutes = 1/(24*6)
        tRemaining = dtElementaire * (N-k-1);
        tFin = NOW + tRemaining;
        %         Title = sprintf('%s - %s', UserData.Title, str);
        %         set(hw, 'String', Title);
        str = sprintf('Started on : %s', datestr(UserData.tStart));
        str = sprintf('%s\n%d / %d', str, k, N);
        str = sprintf('%s\nEllapsed time : %s', str, deltat2str(dt));
        str = sprintf('%s\nRemaining time : %s', str, deltat2str(tRemaining));
        str = sprintf('%s\nMean time per increment : %s', str, deltat2str(dtElementaire));
        str = sprintf('%s\nShould end on : %s', str, datestr(tFin));
        set(UserData.BoutonCancel, 'TooltipString', str)
        
        if nargin == 5
            Title = sprintf('%s - %s', UserData.Title, varargin{1});
            set(UserData.hTitle, 'String', Title);
        end
        %         end
    end
end


function str = deltat2str(dt)

[Y, M, D, H, MN, S] = datevec(dt);
% S = floor(S);

if Y ~= 0
    str = sprintf('%d years %d months %d days', Y, M, D);
elseif M ~= 0
    str = sprintf('%d months %d days %d hours', M, D, H);
elseif D ~= 0
    str = sprintf('%d days %d hours %d minutes', D, H, MN);
elseif H ~= 0
    str = sprintf('%d hours %d minutes %d seconds', H, MN, floor(S));
elseif MN ~= 0
    str = sprintf('%d minutes %d seconds', MN, floor(S));
else
    str = sprintf('%f seconds', S);
end
