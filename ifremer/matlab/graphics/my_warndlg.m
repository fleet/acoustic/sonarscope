% Affichage d'un message � l'�cran
%
% Syntax
%   h = my_warndlg(msg, flagOKModal);
%
% Input Arguments
%   msg         : Intitule de la liste
%   flagOKModal : 1=attend que l'on presse sur OK pour poursuivre, 0: on attend pas
%
% Output Arguments
%   h : Handle de la figure
%
% Name-Value Pair Arguments
%   Tag        : Identifiant permettant de reutiliser la meme fen�tre
%   TimeDelay  : Dur�e de vie de la fen�tre en secondes (valable seulement si flag=0)
%   CreateMode : structure definissant le caract�re 'modal'/non-modal, et
%                l'interpreteur de text 'none'/'tex'
%   displayItEvenIfSSc : Impose l'affichage m�me si flagOKModal=0
%
% Examples
%   my_warndlg('Message sans attente', 0);
%   my_warndlg('Message avec attente', 1);
%   my_warndlg('Message sans attente', 0, 'Timer', 1);
%   my_warndlg('Message avec attente', 1, 'Timer', 0);
%
%   my_warndlg('Premier message',  0);
%   my_warndlg('Deuxi�me message', 0);
%
%   my_warndlg('Premier message',  0, 'Tag', 'Toto');
%   my_warndlg('Deuxi�me message', 0, 'Tag', 'Toto');
%   my_warndlg('Deuxi�me message', 0, 'Tag', 'Toto', 'TimeDelay', 10);
%
% % Message de type LaTex
%       laTexStr = '$$\int_0^x\!\int_y dF(u,v)$$';
%       my_warndlg(laTexStr, 0, 'Interpreter', 'latex');
%
% % Message de type TeX :
%       TeXStr = ['\fontsize{16}black {\color{magenta}magenta '...
%                   '\color[rgb]{0 .5 .5}teal \color{red}red} black again'];
%       my_warndlg(TeXStr, 0, 'Interpreter', 'tex');
%
% % Message de type standard :
%       stdStr = 'Viva SonarScope !';
%       my_warndlg(stdStr, 0);
%
% See also warndlg errordlg str2Html Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = my_warndlg(msg, flagOKModal, varargin) % ;

    function my_start_fcnDelete(obj, event, string_arg) %#ok<INUSD>
        if ishandle(h)
            set(h, 'Name', [Name ' - ' num2str(TimeDelay) ' s'])
        end
    end
    function my_timer_fcnDelete(obj, event, string_arg) %#ok<INUSD>
        TimeDelay = TimeDelay - 1;
        if ishandle(h)
            set(h, 'Name', [Name ' - ' num2str(TimeDelay) ' s'])
        end
    end
    function my_stop_fcnDelete(obj, event, string_arg) %#ok<INUSD>
        my_close(h)
    end

    function my_timer_fcnRefresh(obj, event, string_arg) %#ok<INUSD>
        if ishandle(h)
            s = beep;
            beep off
            figure(h);
            beep(s)
        end
    end

    function str = my_filterMsgString(pattern, str)
        str = supressEmptyCell(str);
        % Recherche des messages d�butant par le pattern de 3 caract�res
        pppp = cellfun(@(x) x(1:3), str, 'UniformOutput', false);
        sub = regexp(pppp, pattern);
        % Recherche des �l�ments vides.
        sub = cellfun(@isempty, sub);
        str = str(sub);
    end

%% D�but de la fonction

global isUsingParfor %#ok<GVMIS>

persistent persistentTime

if ~isempty(isUsingParfor) && isUsingParfor
    fprintf('%s\n', msg);
    varargout{1} = [];
    return
end

%% Ecriture dans le fichier Log

[varargin, resize]             = getPropertyValue(varargin, 'resize',            'Off');
[varargin, displayItEvenIfSSc] = getPropertyValue(varargin, 'displayItEvenIfSSc', 0);

GCF = get(0, 'CurrentFigure');

logFileId = getLogFileId;
if iscell(msg)
    msg = cell2str(msg);
end
str = msg(1,:);
for k=2:size(msg,1)
    str = sprintf('%s\n%s', str, msg(k,:));
end
logFileId.warn('SonarScope', str);

%% Message dans la fen�tre principale : � l'essai (activ� que si on est en version R2013b)

GCBF = gcbf;
if ~isempty(GCBF) && ishandle(GCBF)
    this = handle2obj(GCBF);
    if isa(this, 'cli_image') && ~displayItEvenIfSSc
        hMessage = findobj(GCBF, 'UserData', 'ProcessingMessages');
        if ~isempty(hMessage) && ishandle(hMessage)
            %% On incr�mente le n� du message si le message pr�c�dent n'est
            % pas un message d'attente.
            if isappdata(hMessage, 'MsgWarningValue')
                msgPrevValue = getappdata(hMessage, 'MsgWarningValue');
            else
                msgPrevValue = 0;
            end
            
            msgPrevString  = get(hMessage, 'String');
            str1 = 'Travail en cours ............................';
            str2 = 'Work in progress .........................';
            if ~strcmp(msg, Lang(str1,str2))
                msgPrevValue = msgPrevValue + 1;
                setappdata(hMessage, 'MsgWarningValue', msgPrevValue);
            end
            
            %% Formalisation de la cha�ne de message
            
            if flagOKModal
                typeMsgWarndlg = 'b';
            else
                typeMsgWarndlg = 'a';
            end
            % Eclatement sur plusieurs lignes du m�me message si il
            % comporte des retour chariots. Et �clatement �galement si le
            % message est compos� de N Lignes X M Colonnes.
            newMsgString = {};
            for k=1:size(msg,1)
                splitString = regexp(msg(k,:), '\n', 'split');
                % splitString = regexprep(splitString, 'div >?|<\/div>', '');
                for kk=1:numel(splitString)
                    if ~isempty(splitString{kk})
                        % Troncature de la ligne si elle d�passe N
                        % caract�res (150).
                        sizeLineMax = 150;
                        if numel(splitString{kk}) > sizeLineMax
                            iTrunc = 1:sizeLineMax:numel(splitString{kk});
                            for ii=1:numel(iTrunc)-1
                                newMsgString{end+1,1} = [num2str(msgPrevValue,'%02d') typeMsgWarndlg ' - ' splitString{kk}(iTrunc(ii):iTrunc(ii+1))]; %#ok<AGROW>
                            end
                        else
                            newMsgString{end+1,1} = [num2str(msgPrevValue,'%02d') typeMsgWarndlg ' - ' splitString{kk}]; %#ok<AGROW>
                        end
                    end
                end
            end
            
            % Purge des messages non-significatifs (type WorkInProgress).
            for k=length(msgPrevString):-1:1
                % On conserve la cha�ne si elle n'est pas indic�e par WiP.
                if (length(msgPrevString{k}) >= 2) && strcmp(msgPrevString{k}(1:3), 'WiP')
                    msgPrevString(k) = [];
                end
            end
            msgString = msgPrevString;
            
            if ~isempty(msgString)
                % Bufferisation sur 30 Lignes, effacement des lignes de
                % type WiP puis ..a puis ..b.
                % pour y ajouter le contenu de msg.
                if size(msgString,1) >= 30
                    
                    % De 0 � 2 0 devant le a.
                    strToFind = 'WiP';
                    msgString = my_filterMsgString(strToFind, msgString);
                    if size(msgString,1) >= 30
                        % De 0 � 2 "0" devant le a.
                        strToFind = '0*a';
                        msgString = my_filterMsgString(strToFind, msgString);
                        if size(msgString,1) >= 30
                            % De 0 � 2 "0" devant le b.
                            strToFind = '0*b';
                            msgString = my_filterMsgString(strToFind, msgString);
                            % Si on a toujours plus de 30 messages, vidage des N 1ers
                            % lignes de la pile de la taille des des N nx lignes � entrer.
                            if size(msgString,1) >= 30
                                msgString = circshift(msgString, [size(msgString,1)-size(newMsgString,1) 1]);
                                n1 = size(newMsgString,1);
                                n2 = size(msgString,1);
                                msgString(max(1,n2-n1+1):n2) = [];
                            end
                        end
                    end
                end
            else
                msgString = [];
            end
            
            for k=1:size(newMsgString,1)
                % Suppression des balises HTML le cas �ch�ant.
                msgString{end+1,1} = my_deleteTagHTML(newMsgString{k}); %#ok<AGROW>
            end
            
            for k=size(msgString,1):-1:1
                if isempty(msgString{k})
                    msgString(k) = [];
                end
            end
            
            for k=1:size(msgString,1)
                if iscell(msgString{k})
                    msg = '';
                    for k2=1:length(msgString{k})
                        msg = [msg msgString{k}{k2}]; %#ok<AGROW>
                    end
                    msgString{k} = msg;
                end
            end
             
            set(hMessage, 'String', msgString)
            
            % Positionnement de la liste de messages sur le dernier.
            set(hMessage, 'Value', size(msgString, 1))
            drawnow
            if ~flagOKModal
                varargout{1} = hMessage;
                return
            end
        end
    end
end

%% Pr�paration de la fen�tre

[varargin, Timer]     = getPropertyValue(varargin, 'Timer',     []);
[varargin, TimeDelay] = getPropertyValue(varargin, 'TimeDelay', []);
[varargin, Tag]       = getPropertyValue(varargin, 'Tag',       []);
[varargin, Latency]   = getPropertyValue(varargin, 'Latency',   []); % Temps de latence entre fen�tre en secondes
[varargin, FigName]   = getPropertyValue(varargin, 'FigName',   []);

if isempty(Timer)
    if flagOKModal
        Timer = 1;
    else
        Timer = 0;
    end
end

W = [];
if ~isempty(Tag)
    h = findall(0, 'Type', 'figure', 'Tag', Tag);
    if ~isempty(h) && ishandle(h)
        W = get(h, 'Position');
        delete(h)
    end
end

scr = get(0, 'ScreenSize');
set(0, 'DefaultFigurePosition', [round(scr(3:4))/2-[200 0] 400 200]);

if flagOKModal
    
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    
    if isempty(FigName)
        Titre = ['IFREMER - SonarScope ' Lang('(Cliquez OK pour continuer)', '(Click OK to contine)')];
    else
        Titre = [FigName ' ' Lang('(Cliquez OK pour continuer)', '(Click OK to contine)')];
    end
    
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    % Endroit o� mettre le stop en mode debugage ---------------------------------------
    
else
    
    % D�but ajout JMA le 08/04/2013
    % L'affichage des messages prenait pas mal de temps lors du chargement de
    % nombreuses images ou pendant les d�codages de fichiers all, ...
    % On se limite � un affichage par seconde
    
    if ~isempty(Latency)
        if isempty(persistentTime)
            persistentTime = now;
        else
            NOW = now;
            diff = NOW - persistentTime;
            if diff < (Latency / (24*3600)) % 3 secondes : 3.5e-05 ~= 3 / (24*3600)
                varargout{1} = [];
                return
            end
            persistentTime = NOW;
        end
    end
    % Fin ajout JMA le 08/04/2013
    
    if isempty(FigName)
        Titre = 'IFREMER - SonarScope';
    else
        Titre = ['IFREMER - SonarScope' ' : ' FigName];
    end
end

if iscell(msg)
    msg = cell2str(msg);
end

[varargin, CreateMode]  = getPropertyValue(varargin, 'CreateMode',  []); % param par def de warndlg
[varargin, Interpreter] = getPropertyValue(varargin, 'Interpreter', []); %#ok<ASGLU> % param par def de warndlg
if isempty(CreateMode)
    if isempty(Interpreter)
        CreateMode = struct('WindowStyle', 'non-modal', 'Interpreter', 'none');
    else
        CreateMode = struct('WindowStyle', 'non-modal', 'Interpreter', Interpreter);
    end
else
    if strcmp(Interpreter, 'tex') || strcmp(CreateMode.Interpreter, 'tex')
        for k=1:size(msg,1)
            msg2 = strrep(msg(k,:), '_', '\_'); % Probl�me soulev� par la FORGE : #452
            msg(k,1:length(msg2)) = msg2;
        end
    end
end

N1 = size(msg,2);
N2 = length(Titre);
L = N2 - N1 + 20;
if L > 0
    msg(1, N1+L) = '.';
end

%% Cr�ation de la fen�tre

drawnow % Pour �viter des beeps intempestifs si un autre GUI n'est pas enti�rement termin�
if flagOKModal
    h = warndlg(msg, Titre, CreateMode);
    majFig(h)
    set(h, 'WindowStyle', 'modal', 'resize', resize)
    
    % On impose le reaffichage de la fen�tre toutes les 5 secondes pour �tre s�r qu'il n'y ait pas de fen�tres qui restent actives
    if ishandle(h) && Timer
        t = timer('StartDelay', 0, 'Period', 5, 'ExecutionMode', 'fixedRate');
        t.TimerFcn = {@my_timer_fcnRefresh, 'It�ration'};
        start(t)
    end
    
    waitfor(h);
else
    h = warndlg(msg, Titre, CreateMode);
    majFig(h)
    set(h, 'resize', resize)
    % Pour �viter les interf�rences de trac�, ex, l'ic�ne Exclamation est �cras� par un trac� de Nav.
    set(h, 'HandleVisibility', 'off')
    if ~isempty(Tag)
        set(h, 'Tag', Tag)
    end
    % On impose le reaffichage de la fen�tre toutes les 5 secondes pour �tre s�r qu'il n'y ait pas de fen�tres qui restent actives
    if ishandle(h) && Timer
        t = timer('StartDelay', 0, 'Period', 5, 'TasksToExecute', 10, 'ExecutionMode', 'fixedRate');
        t.TimerFcn = {@my_timer_fcnRefresh, 'It�ration'};
        start(t)
    end
    if ~isempty(W)
        W2 = get(h, 'Position');
        W2(1:2) = W(1:2);
        set(h, 'Position', W2)
    end
end
drawnow

%% R�affichage de la fen�tre pr�c�dente

set(0, 'DefaultFigurePosition', get(0, 'FactoryFigurePosition'));
if ishandle(GCF)
    set(0,'CurrentFigure', GCF);
end

if nargout == 1
    varargout{1} = h;
end

%% Timer

if ishandle(h) && (Timer || ~isempty(TimeDelay))
    if ~isempty(TimeDelay)
        if (TimeDelay >= 1)
            TimeDelay = floor(TimeDelay+1);
            Name = get(h, 'Name');
            t = timer('StartDelay', 0, 'Period', 1, 'TasksToExecute', TimeDelay, 'ExecutionMode', 'fixedRate');
            t.StartFcn = {@my_start_fcnDelete, 'D�but'};
            t.StopFcn  = {@my_stop_fcnDelete, 'Fin'};
            t.TimerFcn = {@my_timer_fcnDelete, 'It�ration'};
            start(t)
        else
            delete(h)
        end
    end
end
end
