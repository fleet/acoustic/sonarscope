% Lit l'instance courante dans le UserData
%
% Syntax
%   obj2handle(a, h0)
%
% Input Arguments
%   a  : instance a sauver
%   h0 : Numero de la figure
%
% Examples
%   h0 = figure;
%   x = cl_backscatter;
%   obj2handle(x, h0)
%   a = handle2obj(h0)
%
% See also handle2obj var2handle handle2obj Authors
% Authors : JMA
%--------------------------------------------------------------------------

function obj2handle(this, h0)
var2handle('Instance', this, h0);
