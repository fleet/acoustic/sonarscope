% Saisie des paramètres de vue d'un fichier exporté en COLLADA
%
% Syntax
%   [flag, vertExag, bloc] = paramCOLLADA
%
% Input Arguments
%   /
%
% Output Arguments
%   vertExag: exagération verticale
%   flag    : indicateur de bon fonctionnement.
%
% Examples
%
% See also cl_image Authors
% Authors : GLT
%--------------------------------------------------------------------------

function [flag, vertExag, bloc] = paramCOLLADA

vertExag = [];
bloc     = [];

str1 = 'Paramètres pour export COLLADA';
str2 = 'COLLADA export parameters';
p    = ClParametre('Name', Lang('Exagération verticale', 'Vertical exaggeration'), ...
    'MinValue', 1, 'MaxValue', 500, 'Value', 10);
p(2) = ClParametre('Name', Lang('Bloc', 'Bloc'), ...
    'MinValue', 0, 'MaxValue', 1, 'Value', 0, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
vertExag = val(1);
bloc     = val(2);
