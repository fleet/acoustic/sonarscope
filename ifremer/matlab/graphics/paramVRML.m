% Saisie des param�tres de vue d'un fichier export� en VRML
%
% Syntax
%   [flag, nomFic, Az, El, vertExag, radius, DisplayAxis] = paramVRML(a)
%
% Input Arguments
%   /
%
% Output Arguments
%   flag     : indicateur de bon fonctionnement.
%   nomFic   : Nom du fichier
%   Az       : angle de vue en Azimut
%   El       : angle de vue en Elevation
%   vertExag : exag�ration verticale
%   modeGrid : mode de rendu de la grille
%   radius   : rayon relatif
%   DisplayAxis : 1=visualisation des axes, 0=pas ....
%
% Examples
%
% See also cl_image Authors
% Authors : GLT
%--------------------------------------------------------------------------

function [flag, nomFic, Az, El, vertExag, modeGrid, radius, DisplayAxis] = paramVRML(ImageName)

persistent persistent_nomDirExport

nomFic   = [];
Az       = [];
El       = [];
vertExag = [];
modeGrid = [];
radius   = [];
DisplayAxis = [];

%% Type de repr�sentation : Points / Facettes

str1 = 'Dans quel mode voulez-vous exporter la grille ? ';
str2 = 'In which mode do you want to export the grid ? ';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 1, Lang('Facettes', 'Facets'), Lang('Points', 'Points Cloud'));
if ~flag
    return
end

%% Param�tres de la vue

str1 = 'Param�tres pour export VRML';
str2 = 'VRML export parameters';
p    = ClParametre('Name',  Lang('Azimut', 'Azimuth'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 360, 'Value', 0);
p(2) = ClParametre('Name', Lang('El�vation (0 = pas d''ombrage)', 'Elevation (0 = no shading)'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 90, 'Value', 45);
p(3) = ClParametre('Name', Lang('Exag�ration verticale', 'Vertical exaggeration'), ...
    'Unit', ' ',  'MinValue', 1, 'MaxValue', 500, 'Value', 1, 'Format', '%d');
if rep == 1
    p(4) = ClParametre('Name', Lang('Rayon relatif', 'Relative radius'), ...
        'Unit', ' ',  'MinValue', 0.2, 'MaxValue', 10, 'Value', 1);
end
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
Az       = val(1);
El       = val(2);
vertExag = val(3);
if rep == 1
    radius = val(4);
end

%% Repr�sentation des axes : c'est bizare que l'on d�finisse cela dans le fichier VRML.
%  On s'attendrait � ce que ce service soit propos� par le visualisateur

strFR = 'Voulez-vous afficher le rep�re des axes au centre ? ';
strUS = 'Do you want to display the axes ? ';
[rep, flag] = my_questdlg(Lang(strFR, strUS), 'Init', 2);
if ~flag
    return
end
DisplayAxis = (rep == 1);

%% Nom du fichier

if isempty(persistent_nomDirExport) || ~exist(persistent_nomDirExport, 'dir')
    filtre  = fullfile(my_tempdir, [ImageName '.wrl']);
else
    filtre  = fullfile(persistent_nomDirExport, [ImageName '.wrl']);
end

str1 = 'Nom du fichier d''export VRML';
str2 = 'Give a file name for the VRML file';
[flag, nomFic] = my_uiputfile({'*.wrl'}, Lang(str1,str2), filtre);
if ~flag
    return
end
persistent_nomDirExport = fileparts(nomFic);
