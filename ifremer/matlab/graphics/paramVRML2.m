% Saisie des param�tres de vue d'un fichier export� en VRML pour des pts.
%
% Syntax
%   [flag, flagRepere, modeExport] = paramVRML2
%
% Input Arguments
%   /
%
% Output Arguments
%   flagRepere    : indique si on afiche le rep�re au centre de la vue ou non
%   modeExport    : indique si on extrait des pts ou es facettes.
%   flag          : indicateur de bon fonctionnement.
%
% Examples
%
% See also cl_image Authors
% Authors : GLT
%--------------------------------------------------------------------------

function [flag, flagRepere, modeExport] = paramVRML2

flagRepere = 0;
modeExport = 0;

strFR = 'Voulez-vous afficher le rep�re des axes au centre  ? ';
strUS = 'Do you want to display axes reference ? ';
[rep, val] = my_questdlg(Lang(strFR, strUS), 'Init', 1, 'WaitingTime', 10);
if val == 1
    if rep == 1
        flagRepere = 1;
    end
else
    flag = 0;
    return
end

strFR = 'Sous quelle forme voulez-vous exporter les points ? ';
strUS = 'Which form do you want to export the points cloud ? ';
[rep, val] = my_questdlg(Lang(strFR, strUS), 'Init', 1, 'WaitingTime', 10, Lang('Facettes', 'Facets'), Lang('Points', 'Points'));
if val == 1
    if rep == 1
        modeExport = 'FaceSet';
    else
        modeExport = 'PointSet';
    end
else
    flag = 0;
    return
end

flag = 1;
