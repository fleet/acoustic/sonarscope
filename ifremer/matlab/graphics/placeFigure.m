% Calcul de la position d'une figure centr�e sur l'�cran
% Si les tailles sont trop grandes, la figure est ramen�e � la taille de l'�cran
%
% Syntax
%   Position = placeFigure...)
%
% Name-Value Pair Arguments
%   Fig       : Numero de la figure si elle existe deja
%   Where     : {'Central' | Left | Right}
%   Width     : Width if Central
%   Height    : Height if Central
%   colDepMin :
%   ligDepMin :
%
% Output Arguments
%   []       : Cr�ation de la figure
%   Position : Position de la figure
%
% Examples
%   Fig = figure;
%   placeFigure('Fig', Fig)
%   placeFigure('Fig', Fig, 'Where', 'Left')
%   placeFigure('Fig', Fig, 'Where', 'Right')
%
% See also centrageFig Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = placeFigure(varargin)

[varargin, Fig]       = getPropertyValue(varargin, 'Fig', []);
[varargin, colDepMin] = getPropertyValue(varargin, 'colDepMin', 10);
[varargin, ligDepMin] = getPropertyValue(varargin, 'ligDepMin', 132);
[varargin, Width]     = getPropertyValue(varargin, 'Width', 950);
[varargin, Height]    = getPropertyValue(varargin, 'Height', 760);
[varargin, Where]     = getPropertyValue(varargin, 'Where', 'Center'); %#ok<ASGLU>

%% R�cuperation de la taille de l'�cran

ScreenSize = get(0, 'ScreenSize');

%% Controle de positionnement

switch Where
    case 'Center'
        Position(3) = min(Width,  ScreenSize(3)-colDepMin);
        Position(4) = min(Height, ScreenSize(4)-ligDepMin);
        
        milieuEcran   = floor((ScreenSize(3)-colDepMin) / 2);
        milieuFenetre = floor(Position(3) / 2);
        Position(1)   = milieuEcran - milieuFenetre + colDepMin;
        
        milieuEcran   = floor((ScreenSize(4)-ligDepMin) / 2);
        milieuFenetre = floor(Position(4) / 2);
        Position(2)   = milieuEcran - milieuFenetre + 37;
        
    case 'Left'
        Position(1) = colDepMin;
        Position(2) = 50;
        Position(3) = ScreenSize(3)/2-colDepMin - 10;
        Position(4) = ScreenSize(4)-ligDepMin;
        
    case 'Right'
        Position(1) = colDepMin + ScreenSize(3)/2;
        Position(2) = 50;
        Position(3) = ScreenSize(3)/2-colDepMin - 10;
        Position(4) = ScreenSize(4)-ligDepMin;
        
    case 'Full'
        Position(1) = colDepMin;
        Position(2) = 50;
        Position(3) = ScreenSize(3)-colDepMin - 10;
        Position(4) = ScreenSize(4)-ligDepMin;
        
    case 'Top'
        Position(1) = colDepMin;
        Position(2) = 50 + ScreenSize(4)/2;
        Position(3) = ScreenSize(3)-colDepMin - 10;
        Position(4) = ScreenSize(4)/2-ligDepMin;
        
    case 'Bottom'
        Position(1) = colDepMin;
        Position(2) = 50;
        Position(3) = ScreenSize(3)-colDepMin - 10;
        Position(4) = ScreenSize(4)/2-ligDepMin;
        
    otherwise
        Position(3) = min(Width,  ScreenSize(3)-colDepMin);
        Position(4) = min(Height, ScreenSize(4)-ligDepMin);
        
        milieuEcran   = floor((ScreenSize(3)-colDepMin) / 2);
        milieuFenetre = floor(Position(3) / 2);
        Position(1)   = milieuEcran - milieuFenetre + colDepMin;
        
        milieuEcran   = floor((ScreenSize(4)-ligDepMin) / 2);
        milieuFenetre = floor(Position(4) / 2);
        Position(2)   = milieuEcran - milieuFenetre + 37;
end

%% Par ici la sortie

if nargout == 0
    if isempty(Fig)
        figure('Position', Position);
    else
        figure(Fig);
        set(Fig, 'Position', Position);
    end
else
    varargout{1} = Position;
end
