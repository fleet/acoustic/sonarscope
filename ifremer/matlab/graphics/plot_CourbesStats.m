function [flag, fig] = plot_CourbesStats(CourbesStatistiques, varargin)

[varargin, fig]          = getPropertyValue(varargin, 'Fig',          []);
[varargin, fig2]         = getPropertyValue(varargin, 'Fig2',         []);
[varargin, flagStats]    = getPropertyValue(varargin, 'Stats',        1);
[varargin, Tag]          = getPropertyValue(varargin, 'Tag',          []);
[varargin, Titre]        = getPropertyValue(varargin, 'Titre',        []);
[varargin, flagMedian]   = getPropertyValue(varargin, 'Median',       0);
[varargin, Offsets]      = getPropertyValue(varargin, 'Offsets',      []);
[varargin, LineStyle]    = getPropertyValue(varargin, 'LineStyle',    '-');
[varargin, LineWidth]    = getPropertyValue(varargin, 'LineWidth',    0.5);
[varargin, Marker]       = getPropertyValue(varargin, 'Marker',       'none');
[varargin, subTypeCurve] = getPropertyValue(varargin, 'subTypeCurve', 1:2);
[varargin, Coul]         = getPropertyValue(varargin, 'Coul',         []);
[varargin, sub]          = getPropertyValue(varargin, 'sub',          1:numel(CourbesStatistiques));
[varargin, Position]     = getPropertyValue(varargin, 'Position',     []);
[varargin, Mute]         = getPropertyValue(varargin, 'Position',     0);

flag = 0;

if isempty(sub) || ((length(sub) == 1) && (sub == 0))
    return
end

if sub(1) < 0 % Astuce pour dire les N derni�res courbes
    sub = length(CourbesStatistiques) + sub + 1;
end

if ~isempty(Tag)
    for k=1:length(sub)
        if strcmp(CourbesStatistiques(sub(k)).bilan{1}(1).Tag, Tag)
            subsub(k) = true; %#ok<AGROW>
        else
            subsub(k) = false; %#ok<AGROW>
        end
    end
    sub = sub(subsub);
end

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if isempty(sub)
    return
end

% if isempty(fig)
%     fig = FigUtils.createSScFigure;
% end

if ishandle(fig)
    figure(fig)
    ha = findobj(fig, 'Type', 'axe');
    if isempty(ha)
        str = {};
    else
        lgd = legend(ha(1));
        if isempty(lgd)
            str = {};
        else
            str = lgd.get.String;
        end
    end
else
    str = {};
end
strAllCurves = {};

N = length(sub);
str1 = 'Trac� des courbes';
str2 = 'Plotting curves';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    if ~ishandle(fig)
        flag = -1;
        return
    end
    if isempty(CourbesStatistiques(sub(k)).bilan)
        continue
    end
%     [strLegend, fig, fig2, strLegend2] = CourbesStatsPlotWithNbOfPoints(CourbesStatistiques(sub(k)).bilan, 'Fig', fig, 'Fig2', fig2, ...
    [~, fig, fig2, strLegend2] = CourbesStatsPlot(CourbesStatistiques(sub(k)).bilan, 'Fig', fig, 'Fig2', fig2, ...
        'Stats', flagStats, 'Coul', Coul, 'iCoul', sub(k), 'Median', flagMedian, 'Offsets', Offsets, ...
        'LineStyle', LineStyle, 'LineWidth', LineWidth, 'Marker', Marker, ...
        'subTypeCurve', subTypeCurve, 'Position', Position, 'TitleAxeCurve', Titre, 'Mute', Mute);
    strLegend = CourbesStatsNames(CourbesStatistiques(sub(k)).bilan);
    
    str  = [str(:);  strLegend(:)];
    strAllCurves = [strAllCurves(:); strLegend2(:)];
    if isempty(Titre)
        Titre = CourbesStatistiques(sub(k)).bilan{1}.titreV;
    end
end
my_close(hw)

if ~isempty(fig)
    FigUtils.createSScFigure(fig);
    if isempty(str)
        messagePasDeCourbeATracer
    else
        str = completeLegend(str);
        legend(str, 'interpreter', 'none'); %, 'Location', 'eastoutside')
    end
%     set(fig, 'Name', ['  Curve coming from ' Titre])
    set(fig, 'Name', Titre)
end

if ~isempty(fig2)
    FigUtils.createSScFigure(fig2);
    if length(strAllCurves) > 10
        %     my_warndlg('Legend too big, I don''t draw it.', 0);
    else
        if isempty(strAllCurves)
            messagePasDeCourbeATracer
        else
            legend(strAllCurves, 'interpreter', 'none')
        end
    end
    
    if isempty(Titre)
        Titre = CourbesStatistiques(sub(k)).bilan{1}.titreV;
    end
%     set(fig2, 'Name', ['  Curve coming from ' Titre])
    set(fig2, 'Name', Titre)
end

flag = 1;


function messagePasDeCourbeATracer
str1 = 'Pas de courbe � tracer.';
str2 = 'No curve to plot.';
my_warndlg(Lang(str1,str2), 1);
