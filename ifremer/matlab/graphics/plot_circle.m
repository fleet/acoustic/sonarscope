function h = plot_circle(x0, y0, radius, varargin)

teta = 0:360;
h = PlotUtils.createSScPlot(x0+radius*cosd(teta), y0+radius*sind(teta), varargin{:});
