function plot_colorbarOnly(hc, CLim, varargin)

[varargin, map] = getPropertyValue(varargin, 'Colormap', []); %#ok<ASGLU>

if isempty(map)
    map = jet(256);
end
hi = imagesc(zeros(2), CLim);
set(hi, 'Visible', 'Off')
colormap(map)
colorbar
w = get(hc, 'Position');
w(3) = 0.00001;
set(hc, 'Position', w);
set(hc, 'Visible', 'Off');
