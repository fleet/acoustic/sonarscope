function plot_ellipse(a, b, cx, cy, angle, color)
%a: width in pixels
%b: height in pixels
%cx: horizontal center
%cy: vertical center
%angle: orientation ellipse in degrees
%color: color code (e.g., 'r' or [0.4 0.5 0.1])
% figure; plot_ellipse(5, 3, 100, 200, 0, 'r'); axis equal;

r = 0:0.1:2*pi+0.1;

for k=1:length(a)
    p = [(a(k)*cos(r))' (b(k)*sin(r))'];
    
    alpha = [cosd(angle(k)) -sind(angle(k))
        sind(angle(k)) cosd(angle(k))];
    
    p1 = p * alpha;
    
    patch(cx(k)+p1(:,1), cy(k)+p1(:,2), color, 'EdgeColor', color, 'FaceColor','none');
end
