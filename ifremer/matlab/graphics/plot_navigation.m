% Trac� d'un trait de navigation
%
% Syntax
%   plot_navigation(nomFic, Fig1, Longitude, Latitude, SonarTime, CourseVessel, ...)
%
% Input Arguments
%   nomFic     : Nom du fichier
%   Fig1       : Num�ro de la figure de navigation
%   Longitude  :
%   Latitude   :
%   SonarTime  : Temps en unite matlab
%   Heading    : Cap (deg)
%
% Name-Value Pair Arguments
%   CourseVessel  : Cap en deg
%
% Examples
%   plot_navigation(nomFic, Fig1, Longitude, Latitude, SonarTime, CourseVessel)
%
% See also plot_navigation_Figure Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [Fig1, hAxes] = plot_navigation(nomFic, Fig1, Longitude, Latitude, SonarTime, Heading, varargin)

[varargin, Title]        = getPropertyValue(varargin, 'Title',        []);
[varargin, CourseVessel] = getPropertyValue(varargin, 'CourseVessel', []);
[varargin, NavOTUS]      = getPropertyValue(varargin, 'NavOTUS',      0);
[varargin, ColorCurve]   = getPropertyValue(varargin, 'Color',        []);
[varargin, Immersion]    = getPropertyValue(varargin, 'Immersion',    []); %#ok<ASGLU>

if isempty(Title)
    Title = 'Position';
end

Longitude  = Longitude(:);
Latitude   = Latitude(:);
Immersion  = Immersion(:);

if ~isempty(Heading) && (size(Heading,2) == 1)
    Heading = Heading(:,1);
end

TagLine = nomFic;

%% Trac� de la navigation

if isempty(Fig1)
    Fig1 = figure;
    creation = true;
else
    if ishandle(Fig1)
        figure(Fig1);
        creation = false;
    else
        Fig1 = figure;
        creation = true;
    end
end

nomFicPosition = fullfile(my_tempdir, ['Position' num2str(Fig1.Number) '.dat']);

if creation
    [Fig1, hAxes] = plot_navigation_Figure(Fig1, 'NavOTUS', NavOTUS);
else
    figure(Fig1)
    hAxes = gca;
end

Ident = rand(1);

h = findobj(Fig1, 'Tag', 'hHeure');
if isempty(h)
    uicontrol('Style', 'text', 'Position', [10,10,1000,20], 'Tag', 'hHeure');
    set(Fig1, 'ToolBar', 'figure')
end

if isempty(SonarTime)
    SonarTime = 1:length(Latitude);
    t = SonarTime;
    TypeTime = 2;
else
    switch class(SonarTime)
        case 'datetime'
            t = SonarTime;
            subNaT = find(isnat(t));
            if ~isempty(subNaT)
                subNonNaT = find(~isnat(t));
                t(subNaT) = interp1(subNonNaT, t(subNonNaT), subNaT, 'linear', 'extrap');
            end
            TypeTime = 1;
            
        case 'cl_time'
             if all(SonarTime.timeMat == 0)
                t = 1:length(Latitude);
                TypeTime = 2;
             else
                t = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
                TypeTime = 1;
             end
            
       otherwise
            if all(SonarTime == 0)
                t = 1:length(SonarTime);
                TypeTime = 2;
            else
                TypeTime = 1;
                t = datetime(SonarTime, 'ConvertFrom', 'datenum');
           end
    end
end

set(Fig1, 'Tag', 'Trace de navigation', 'UserData', Ident)

if isempty(ColorCurve)
    hc = findobj(hAxes, 'Type', 'line');
    nbCourbes  = length(hc);
    ColorOrder = get(hAxes, 'ColorOrder');
    nbCoul     = size(ColorOrder,1);
    iCoul      = mod(nbCourbes, nbCoul) + 1;
    ColorCurve = ColorOrder(iCoul,:);
end

hold(hAxes, 'on');
if isempty(SonarTime)
    h = plot(hAxes, Longitude(:), Latitude(:));
    setappdata(h, 'Time', []);
elseif size(t(:), 2) > 1
    h = plot(hAxes, Longitude(:), Latitude(:));
    setappdata(h, 'Time', mean(t));
else
    h = plot(hAxes, Longitude(:), Latitude(:));
    setappdata(h, 'Time', t(:));
end
set(h, 'Color', ColorCurve)
title(Title)

setappdata(h, 'Immersion',    Immersion);
setappdata(h, 'Heading',      Heading);
setappdata(h, 'CourseVessel', CourseVessel);

set(hAxes, 'ButtonDownFcn', @plotCross)

[pppp, nomFicSeul] = fileparts(nomFic);
if isempty(SonarTime)
    UserData = nomFicSeul;
else
    if TypeTime == 1
        str = char([t(1), t(end)]);
        str = [str repmat(' ', 2, 2)]';
        UserData = [nomFicSeul ' - ' str(:)'];
    else
        str = [num2str(t(1)) ' - ' num2str(t(end))];
        UserData = [nomFicSeul ' - ' str];
    end
end
set(h, 'UserData', UserData)

figure(Fig1);
cmenu = uicontextmenu;
set(h, 'UIContextMenu', cmenu)
if isempty(SonarTime)
    strUicontextmenu = nomFic;
else
    strUicontextmenu = sprintf('%s - %s', nomFic, str(:));
end
uimenu(cmenu, 'Text', strUicontextmenu);
h1 = uimenu(cmenu, 'Text', 'Hide line');
h2 = uimenu(cmenu, 'Text', 'Awake line');
h3 = uimenu(cmenu, 'Text', 'Clone in a new figure');
h4 = uimenu(cmenu, 'Text', 'Edit Values');
h5 = uimenu(cmenu, 'Text', 'Create a Marker');
h6 = uimenu(cmenu, 'Text', 'Delete a Marker');

[~, ~, ext] = fileparts(nomFic);
if strcmp(ext, '.all')
    h7 = uimenu(cmenu, 'Text', 'Export in .all file');
else
    h7 = [];
end

LatLim = [min(Latitude)  max(Latitude)];
LonLim = [min(Longitude) max(Longitude)]; % Ligne de changement d'heure pas g�r�e ici
if isempty(SonarTime)
    tLim = [];
else
    tLim = [t(1), t(end)];
end
Color = get(h, 'Color');

UserData = {Color; 'On'; h; nomFic; LatLim; LonLim; tLim; nomFicPosition};
set(h1, 'Callback', @HideLine,       'UserData', UserData);
set(h2, 'Callback', @ShowLine,       'UserData', UserData);
set(h3, 'Callback', @CloneThisCurve, 'UserData', UserData);
set(h4, 'Callback', @EditValues,     'UserData', UserData);
set(h5, 'Callback', @PlotMarker,     'UserData', UserData);
set(h6, 'Callback', @DeleteMarker,   'UserData', UserData);
if ~isempty(h7)
    set(h7, 'Callback', @ExporLineInSimradAllFile, 'UserData', UserData);
end

if isempty(SonarTime)
    str = sprintf('display(''%s | %s %s | %s %s'')', ...
        nomFic, ...
        lat2str(Latitude(1)), ...
        lon2str(Longitude(1)), ...
        lat2str(Latitude(end)), ...
        lon2str(Longitude(end)));
else
    str = sprintf('display(''%s - %s | %s %s | %s %s'')', ...
        nomFic, str(:), ...
        lat2str(Latitude(1)), ...
        lon2str(Longitude(1)), ...
        lat2str(Latitude(end)), ...
        lon2str(Longitude(end)));
end
set(h, 'ButtonDownFcn', str)

if ~isempty(TagLine)
    set(h, 'Tag', TagLine)
end

axisGeo;
grid on;
% xlabel('Longitude (deg)')
% ylabel('Latitude (deg)')
drawnow

%% Definition de la callback associ�e au d�placement de la souris

set(Fig1, 'WindowButtonMotionFcn', @dispHeure, 'BusyAction', 'cancel')

%% Inner fonction d'affichage du point de la navigation le plus proche de la souris

    function dispHeure(varargin)
        global hLines XData YData ZData sub ordre2 Dist DistMin tMin % Dans le debuggueur uniquement
        GCBA = get(gcbf, 'CurrentAxes');
        
        Signature1 = getappdata(GCBA, 'Signature1');
        if ~isempty(Signature1) && strcmp(Signature1, 'colorbar')
            return
        end
        
        currentPoint = get(GCBA, 'currentPoint');
        
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        if (currentPoint(1,1) < XLim(1)) || (currentPoint(1,1) > XLim(2)) || (currentPoint(1,2) < YLim(1)) || (currentPoint(1,2) > YLim(2))
            return
        end
        
        hLines = findobj(gcbf, 'Type', 'line', 'LineStyle', '-', '-not', 'Tag', 'PositionPing');
        if isempty(hLines)
            return
        end
        for k=1:length(hLines)
            pppp = get(hLines(k), 'Tag');
            if isempty(pppp)
                subIsNav(k) = false; %#ok<AGROW>
            else
                subIsNav(k) = true; %#ok<AGROW>
            end
        end
        hLines = hLines(subIsNav);
        DistMin = [];
        if TypeTime == 1
            tMin = datetime.empty();
            tMin.Format = 'yyyy-MM-dd HH:mm:ss.SSS';
        else
            tMin = [];
        end
        hLinesConserve = [];
        ordre1 = [];
        for k=1:length(hLines)
            ZData = getappdata(hLines(k), 'Time');
            if isempty(ZData)
                continue
            end
            UserData = get(hLines(k), 'UserData');
            if isempty(UserData)
                continue
            end
            
            XData = get(hLines(k), 'XData');
            YData = get(hLines(k), 'YData');
            XData = abs(XData - currentPoint(1,1));
            YData = abs(YData - currentPoint(1,2));
            Dist = (XData .^ 2) + (YData .^ 2);
            [~, ordre] = sort(Dist);
            ordre1(end+1) = ordre(1); %#ok<AGROW>
            DistMin(end+1) = Dist(ordre(1)); %#ok
            tMin(end+1) = ZData(ordre(1)); %#ok
            hLinesConserve(end+1) = hLines(k); %#ok
        end
        [sub, ordre2] = sort(DistMin);
        if isempty(ordre2)
            return
        end
        tMin = tMin(ordre2(1));
        
        hHeure = findobj(gcbf, 'Tag', 'hHeure');
        hNearestLine = hLinesConserve(ordre2(1));
        indPoint = ordre1(ordre2(1));
        XData = get(hNearestLine, 'XData');
        YData = get(hNearestLine, 'YData');
        ZData = getappdata(hNearestLine, 'Time');
        subVoisins = max(1,indPoint-10):(min(length(XData),indPoint+10));
        cap = calCapFromLatLon(YData(subVoisins), XData(subVoisins), 'Time', ZData(subVoisins));
        cap = mode(cap);
 
        UserData = get(hNearestLine, 'UserData');
        if TypeTime == 1
            %             strt = char(tMin);
            strt = sprintf('%s - %d deg', char(tMin), round(cap));
            String = ['Point : ' strt ' - Line : ' UserData];
        else
            String = ['Point : ' num2str(tMin) ' Line : ' UserData];
            % TODO : afficher cap
        end
        
        Signal = getappdata(hLines(ordre2(1)), 'Signal');
        if ~isempty(Signal)
            try
                String = [String ' - Value=' num2str(Signal(ordre2(1)))];
            catch
                String = 'Bug';
            end
        end
        set(hHeure, 'String', String, 'HorizontalAlignment', 'left')
        
        if length(hLines) > 1
            set(hLinesConserve, 'LineWidth', 0.5)
            set(hNearestLine, 'LineWidth', 3)
        end
    end

    function PlotMarker(varargin)
        global hLines XData YData ZData sub ordre2 Dist DistMin tMin % Dans le debuggueur uniquement
        GCBO = gcbo;
        GCBA = get(gcbf, 'CurrentAxes');
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        currentPoint = get(GCBA, 'currentPoint');
        hLines = findobj(gcbf, 'Type', 'line');
        for k=1:length(hLines)
            pppp = get(hLines(k), 'Tag');
            if isempty(pppp)
                subIsNav(k) = false; %#ok<AGROW>
            else
                subIsNav(k) = true; %#ok<AGROW>
            end
        end
        hLines = hLines(subIsNav);
        DistMin = [];
        ordre1  = [];
        if TypeTime == 1
            tMin = datetime.empty();
            tMin.Format = 'yyyy-MM-dd HH:mm:ss.SSS';
        else
            tMin = [];
        end
        XDataLine = [];
        YDataLine = [];
        hLinesConserve = [];
        for k=1:length(hLines)
            ZData = getappdata(hLines(k), 'Time');
            if isempty(ZData)
                continue
            end
            UserData = get(hLines(k), 'UserData');
            if isempty(UserData)
                continue
            end
            
            XData = get(hLines(k), 'XData');
            YData = get(hLines(k), 'YData');
            XDataDist = abs(XData - currentPoint(1,1));
            YDataDist = abs(YData - currentPoint(1,2));
            Dist = XDataDist.^2 + YDataDist.^2;
            [~, ordre] = sort(Dist);
            ordre1(end+1) = ordre(1); %#ok<AGROW>
            DistMin(end+1) = Dist(ordre(1)); %#ok
            XDataLine(end+1) = XData(ordre(1)); %#ok<AGROW>
            YDataLine(end+1) = YData(ordre(1)); %#ok<AGROW>
            tMin(end+1) = ZData(ordre(1)); %#ok
            hLinesConserve(end+1) = hLines(k); %#ok
        end
        [sub, ordre2] = sort(DistMin);
        if isempty(ordre2)
            return
        end
        tMin = tMin(ordre2(1));
        UserData = get(GCBO, 'UserData');
        
        flagHold = ishold;
        if ~flagHold
            hold on
        end
        try
            hPoint = PlotUtils.createSScPlot(XDataLine(ordre2(1)), YDataLine(ordre2(1)), 'or');
        catch % For Mathworks
            hPoint = plot(XDataLine(ordre2(1)), YDataLine(ordre2(1)), 'or');
        end
        if TypeTime == 1
            set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', char(tMin))
        else
            set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', num2str(tMin))
        end
        if ~flagHold
            hold off
        end
        set(GCBA, 'XLim', XLim);
        set(GCBA, 'YLim', YLim);
    end

    function CloneThisCurve(varargin)
        GCBO = gcbo;
        GCA = gca;
        UserData = get(GCBO, 'UserData');
        nomFic = UserData{4};
        hOther = findobj(GCA, 'Tag', nomFic);
        
        if length(hOther) > 1
            str1 = 'Le nettoyage de navigation ne peut �tre r�alis� que � partir d''un trac� de navigation seul.';
            str2 = 'The navigation cleaning can be done only with a single navigation plot (no signal)';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        
        Longitude = get(hOther, 'XData');
        Latitude  = get(hOther, 'YData');
        T = getappdata(hOther, 'Time');
        Immersion = getappdata(hOther, 'Immersion');
        
        plot_navigation(nomFic, [], Longitude, Latitude, T, [], 'Immersion', Immersion)
    end

    function EditValues(varargin) % Will not work for Mathworks
        GCBO = gcbo;
        GCA  = gca;
        UserData = get(GCBO, 'UserData');
        nomFic = UserData{4};
        hOther = findobj(GCA, 'Tag', nomFic);
        
        if length(hOther) > 1
            str1 = 'Le nettoyage de navigation ne peut �tre r�alis� que � partir d''un trac� de navigation seul.';
            str2 = 'The navigation cleaning can be done only with a single navigation plot (no signal)';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        
        Longitude = get(hOther, 'XData');
        Latitude  = get(hOther, 'YData');
        T = getappdata(hOther, 'Time');
        Immersion = getappdata(hOther, 'Immersion');
        
        %% Cr�ation de l'instance ClNavigation
        
        timeSample = XSample('name', 'time', 'data', T);
        
        latSample = YSample('data', Latitude, 'marker', '.');
        latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);

        lonSample = YSample('data', Longitude, 'marker', '.');
        lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
        
        if isempty(Immersion)
            immersionSignal = ClSignal.empty;
        else
            immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion, 'marker', '.');
            immersionSignal = ClSignal('ySample', immersionSample, 'xSample', timeSample);
        end
        
        [~, filename, Ext] = fileparts(nomFic);
        nav = ClNavigation(latSignal, lonSignal, 'immersionSample', immersionSignal, 'name',  [filename, Ext] );
        
        %% Create the GUI
        
        a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
        a.openDialog();
        if ~a.okPressedOut
            return
        end
        
        %% R�cup�ration des signaux
        
        T         = nav.getTimeSignalSample().data;
        Latitude  = nav.getLatitudeSignalSample().data;
        Longitude = nav.getLongitudeSignalSample().data;
        Immersion = nav.getImmersionSignalSample().data;
        
        %% Save cleaned signals in the graphic
        
        setappdata(hOther, 'Time', T);
        set(hOther, 'XData', Longitude);
        set(hOther, 'YData', Latitude);
        if ~isempty(Immersion)
            setappdata(hOther, 'Immersion', Immersion);
        end
    end

    function ExporLineInSimradAllFile(varargin) % Will not work for Mathworks
        GCBO = gcbo;
        UserData = get(GCBO, 'UserData');
        nomFic = UserData{4};
        hOther = findobj(GCA, 'Tag', nomFic);
        hOther(hOther == h) = [];
        Longitude = get(hOther, 'XData');
        Latitude  = get(hOther, 'YData');
        simrad_saveNavigation(nomFic, Latitude, Longitude)
    end

    function PrintLine(str, varargin) %#ok
        display(str)
    end

    function HideLine(varargin)
        GCBO = gcbo;
        GCA = gca;
        UserData = get(GCBO, 'UserData');
        h = UserData{3};
        set(h, 'Color', [0.8 0.8 0.8])
        UserData{2} = 'Off';
        set(GCBO, 'UserData', UserData)
        nomFic = UserData{4};
        hOther = findobj(GCA, 'Tag', nomFic);
        hOther(hOther == h) = [];
        set(hOther, 'Visible', 'Off')
    end

    function ShowLine(varargin)
        GCBO = gcbo;
        GCA = gca;
        UserData = get(GCBO, 'UserData');
        h = UserData{3};
        Color = UserData{1};
        set(h, 'Color', Color)
        UserData{2} = 'On';
        set(GCBO, 'UserData', UserData)
        nomFic = UserData{4};
        hOther = findobj(GCA, 'Tag', nomFic);
        hOther(hOther == h) = [];
        set(hOther, 'Visible', 'On')
    end


%% plotCross

    function plotCross(varargin)
        GCBA = get(gcbf, 'CurrentAxes');
        currentPoint = get(GCBA, 'currentPoint');
        Lon = currentPoint(1,1);
        Lat = currentPoint(1,2);
        
        hCross = findobj(gcbf, 'Type', 'line', 'Tag', 'PositionPing');
        if isempty(hCross)
            XLim = get(GCBA, 'XLim');
            YLim = get(GCBA, 'YLim');
            hold on;
            hc = plot([Lon Lon], YLim, 'r');
            set(hc, 'Tag', 'PositionPing')
            hc = plot(XLim, [Lat Lat], 'r');
            set(hc, 'Tag', 'PositionPing')
            hold off
        else
            set(hCross(1), 'YData', [Lat Lat])
            set(hCross(2), 'XData', [Lon Lon])
        end
    end

    function DeleteMarker(varargin)
        GCBA = get(gcbf, 'CurrentAxes');
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        currentPoint = get(GCBA, 'currentPoint');
        hLines = findobj(gcbf, 'Tag', 'PointRemarquable');
        if isempty(hLines)
            return
        end
        for k=1:length(hLines)
            XData(k) = get(hLines(k), 'XData'); %#ok<AGROW>
            YData(k) = get(hLines(k), 'YData'); %#ok<AGROW>
        end
        XDataDist = abs(XData-currentPoint(1,1));
        YDataDist = abs(YData-currentPoint(1,2));
        Dist = XDataDist.^2 + YDataDist.^2;
        [~, ordre] = sort(Dist);
        if (XData(ordre(1)) >= XLim(1)) && (XData(ordre(1)) <= XLim(2)) && (YData(ordre(1)) >= YLim(1)) && (YData(ordre(1)) <= YLim(2))
            delete(hLines(ordre(1)))
        else
            str1 = 'Le Marker le plus rapproch� est en dehors du zoom, je ne le supprime pas.';
            str2 = 'The nearest Marker is outside the current display, I do not delete it.';
            my_warndlg(Lang(str1,str2), 1);
        end
    end
end
