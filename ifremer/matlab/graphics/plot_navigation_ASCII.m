function plot_navigation_ASCII

persistent persistent_nomDirTxt

LatPatchSwath   = [];
LonPatchSwath   = [];
% LabelPatchOther = [];
% FaceColor       = [];
% LatPatchOther =[];
% LonPatchOther =[];

% h1 = findobj(gcf, 'Type', 'Line');
h1 = findobj(gcf, 'Type', 'Line', 'Marker', 'none', '-and', '-not', 'tag', 'PositionPing');
h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
h3 = findobj(gcf, 'Type', 'patch');
[hOther, sub] = setdiff(h1, h2);
if isempty(h3)
%     hPatch_other = [];
    hPatch_swath = [];
else
%     hPatch_other = setxor(h3, h3(sub));
    hPatch_swath = h3(sub);
end

if isempty(hOther)
    return
end

for k=length(hOther):-1:1
    Tag = get(hOther(k), 'Tag');
    if strcmp(Tag, 'PositionPing')
        hOther(k) = [];
    end
end

nbProfils = length(hOther);
for k=1:nbProfils
    Type = get(hOther(k), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
%         Tag = get(hOther(k), 'Tag');
        UserData = get(hOther(k), 'UserData');
        GCL  = UserData{3};
    end
        
    Label1 = get(GCL, 'Tag');
    nomDirTxt = fileparts(Label1);
    
    if k == 1
        if ~isempty(persistent_nomDirTxt)
            nomDirTxt = persistent_nomDirTxt;
        end
        
        str1 = 'Nom du fichier de navigation Caraibes';
        str2 = 'Name of the Caraibes navigation file';
        [flag, nomFicTxt] = my_uiputfile('*.txt', Lang(str1,str2), nomDirTxt);
        if ~flag
            return
        end
        persistent_nomDirTxt = fileparts(nomFicTxt);
        
        hw = create_waitbar(Lang('Exportation des profils en cours ...', 'Exporting profils ...'), 'N', nbProfils);
    end
    my_waitbar(k, nbProfils, hw)

    Label1 = get(GCL, 'Tag');
    [~, Label1, ext] = fileparts(Label1);   
        
    Label2   = get(GCL, 'UserData');
    Color{k} = get(GCL, 'Color'); %#ok<AGROW>
    Lon{k}   = get(GCL, 'XData'); %#ok<AGROW>
    Lat{k}   = get(GCL, 'YData'); %#ok<AGROW>
%     Time{k}  = get(GCL, 'ZData'); %#ok<AGROW>
    Time{k}  = getappdata(GCL, 'Time'); %#ok<AGROW>
    
    subOK = ~isnan(Lon{k}) & ~isnan(Lat{k}) & ~isnat(Time{k}');
    Lon{k}  = Lon{k}(subOK); %#ok<AGROW>
    Lat{k}  = Lat{k}(subOK); %#ok<AGROW>
    Time{k} = Time{k}(subOK); %#ok<AGROW>
    
    HeadingVesselLine{k} = getappdata(GCL, 'Heading'); %#ok<AGROW>
    if ~isempty(HeadingVesselLine{k})
        HeadingVesselLine{k} = HeadingVesselLine{k}(~isnan(HeadingVesselLine{k})); %#ok<AGROW>
    end
   
    Immersion{k} = getappdata(GCL, 'Immersion'); %#ok<AGROW>
    if ~isempty(Immersion{k})
        Immersion{k} = Immersion{k}(~isnan(Immersion{k})); %#ok<AGROW>
    end
    
    %     ZData  = get(GCL, 'ZData');

    if ~isempty(hPatch_swath)
        LonPatchSwath{k} = get(hPatch_swath(k), 'XData'); %#ok<AGROW>
        LatPatchSwath{k} = get(hPatch_swath(k), 'YData'); %#ok<AGROW>
    end

    Name{k}  = [Label1 ext]; %#ok<AGROW>
    if contains(Label2,  Label1)
        Label2 = Label2(length(Label1)+3:end);
    end
    Label{k} = Label2; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')


% if ~isempty(hPatch_other)
%     for k=1:length(hPatch_other)
%         LonPatchOther{k} = get(hPatch_other(k), 'XData'); %#ok<AGROW>
%         LatPatchOther{k} = get(hPatch_other(k), 'YData'); %#ok<AGROW>
%         FaceColor{k} = get(hPatch_other(k), 'FaceColor'); %#ok<AGROW>
%         LabelPatchOther{k} = []; %#ok<AGROW>
%         
%         UIContextMenu = get(hPatch_other(k), 'UIContextMenu');
%         if ~isempty(UIContextMenu)
%             Children = get(UIContextMenu, 'Children');
%             if ~isempty(Children)
%                 LabelPatchOther{k} = get(Children, 'Label'); %#ok<AGROW>
%             end
%         end
%     end
% end

%% Regroupement des segments en un seul morceau

[T, Latitude, Longitude, Immersion, HeadingVessel, HeadingCourse,  SonarSpeed, Name, Label, Color, nbPoints] = ...
    groupLinesInOneSegment(Time, Lat, Lon, HeadingVesselLine, Immersion, Name, Label, Color);

%% Ecriture du fichier ASCII

export_navigation_ASCII(nomFicTxt, T, Latitude, Longitude, SonarSpeed, HeadingVessel, Name, Label, Color, nbPoints);


function [T, Latitude, Longitude, Immersion, HeadingVessel, HeadingCourse, SonarSpeed, Name, Label, Color, nbPoints] = groupLinesInOneSegment(Time, Lat, Lon, HeadingVesselLine, Immer, Name, Label, Color)

N = length(Time);
for k=1:N
    tDeb(k) = Time{k}(1); %#ok<AGROW>
end
[~, subOrdre] = sort(tDeb);

Name  = Name(subOrdre);
Label = Label(subOrdre);
Color = Color(subOrdre);

T             = [];
Latitude      = [];
Longitude     = [];
Immersion     = [];
HeadingVessel = [];
nbPoints  = zeros(1,N);
for k=1:N
    T             = [T; Time{subOrdre(k)}]; %#ok<AGROW>
    nbPoints(k)   = length(Time{subOrdre(k)});
    Latitude      = [Latitude;  Lat{subOrdre(k)}(:)]; %#ok<AGROW>
    Longitude     = [Longitude; Lon{subOrdre(k)}(:)]; %#ok<AGROW>
    Im            = Immer{subOrdre(k)};
    Immersion     = [Immersion; (Im(:))]; %#ok<AGROW>
    HeadingVessel = [HeadingVessel; HeadingVesselLine{subOrdre(k)}(:)]; %#ok<AGROW>
end

HeadingCourse = calCapFromLatLon(Latitude, Longitude, 'Time', T);
SonarSpeed    = zeros(size(Latitude));
