function plot_navigation_Caraibes

persistent persistent_nomDirNvi

LatPatchSwath   = [];
LonPatchSwath   = [];
LabelPatchOther = [];
FaceColor       = [];
LatPatchOther   = [];
LonPatchOther   = [];

% h1 = findobj(gcf, 'Type', 'Line');
h1 = findobj(gcf, 'Type', 'Line', 'Marker', 'none', '-and', '-not', 'tag', 'PositionPing');
h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
h3 = findobj(gcf, 'Type', 'patch');
[hOther, sub] = setdiff(h1, h2);
if isempty(h3)
    hPatch_other = [];
    hPatch_swath = [];
else
    hPatch_other = setxor(h3, h3(sub));
    hPatch_swath = h3(sub);
end

if isempty(hOther)
    return
end

for k=length(hOther):-1:1
    Tag = get(hOther(k), 'Tag');
    if strcmp(Tag, 'PositionPing')
        hOther(k) = [];
    end
end

nbProfils = length(hOther);
for k=1:nbProfils
    Type = get(hOther(k), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        UserData = get(hOther(k), 'UserData');
        GCL = UserData{3};
    end
        
    Label1 = get(GCL, 'Tag');
    nomDirNvi = fileparts(Label1);
    
    if k == 1
        if ~isempty(persistent_nomDirNvi)
            nomDirNvi = persistent_nomDirNvi;
        end
        
        str1 = 'Nom du fichier de navigation Caraibes';
        str2 = 'Name of the Caraibes navigation file';
        [flag, nomFicNvi] = my_uiputfile('*.nvi', Lang(str1,str2), nomDirNvi);
        if ~flag
            return
        end
        persistent_nomDirNvi = fileparts(nomFicNvi);
        
        hw = create_waitbar(Lang('Exportation des profils en cours', 'Exporting profils'), 'N', nbProfils);
    end
    my_waitbar(k, nbProfils, hw)
        
    Lon{k}  = get(GCL, 'XData'); %#ok<AGROW>
    Lat{k}  = get(GCL, 'YData'); %#ok<AGROW>
%     Time{k} = get(GCL, 'ZData'); %#ok<AGROW>
    Time{k} = getappdata(GCL, 'Time'); %#ok<AGROW>
    sub = (~isnan(Lon{k}) & ~isnan(Lat{k}) & ~isnat(Time{k}'));
    Lon{k}  = Lon{k}(sub); %#ok<AGROW>
    Lat{k}  = Lat{k}(sub); %#ok<AGROW>
    Time{k} = Time{k}(sub); %#ok<AGROW>
    
    x = getappdata(GCL, 'Heading');
    if isempty(x)
        HeadingVesselLine{k} = []; %#ok<AGROW>
    else
        HeadingVesselLine{k} = x(sub); %#ok<AGROW>
    end
    x = getappdata(GCL, 'Immersion');
    if isempty(x)
        VesselHeightLine{k} = []; %#ok<AGROW>        
    else
        VesselHeightLine{k} = x(sub); %#ok<AGROW>
    end
    %     ZData  = get(GCL, 'ZData');

    if ~isempty(hPatch_swath)
        LonPatchSwath{k} = get(hPatch_swath(k), 'XData'); %#ok<AGROW>
        LatPatchSwath{k} = get(hPatch_swath(k), 'YData'); %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')

if ~isempty(hPatch_other)
    for k=1:length(hPatch_other)
        LonPatchOther{k} = get(hPatch_other(k), 'XData'); %#ok<AGROW>
        LatPatchOther{k} = get(hPatch_other(k), 'YData'); %#ok<AGROW>
        FaceColor{k} = get(hPatch_other(k), 'FaceColor'); %#ok<AGROW>
        LabelPatchOther{k} = []; %#ok<AGROW>
        
        UIContextMenu = get(hPatch_other(k), 'UIContextMenu');
        if ~isempty(UIContextMenu)
            Children = get(UIContextMenu, 'Children');
            if ~isempty(Children)
%                 LabelPatchOther{k} = get(Children, 'Label'); %#ok<AGROW>
                LabelPatchOther{k} = get(Children, 'Text'); %#ok<AGROW> % Modif JMA le 20/05/2021
            end
        end
    end
end

%% Regroupement des segments en un seul morceau

[tNvi, Latitude, Longitude, HeadingVessel, HeadingCourse, SonarSpeed, Immersion] = groupLinesInOneSegment(Time, Lat, Lon, HeadingVesselLine, VesselHeightLine);

Height = zeros(size(HeadingCourse));
% Immersion = zeros(size(HeadingCourse));

%% Ecriture du fichier Caraibes

if isempty(HeadingVessel)
    Heading = HeadingCourse;
else
    Heading = HeadingVessel;
end

export_nav_Caraibes(nomFicNvi, tNvi, Latitude, Longitude, SonarSpeed, Heading, Height, Immersion);



function [T, Latitude, Longitude, HeadingVessel, HeadingCourse, SonarSpeed, Immersion] = groupLinesInOneSegment(Time, Lat, Lon, HeadingVesselLine, VesselHeightLine)

N = length(Time);
for k=1:N
    tDeb(k) = Time{k}(1); %#ok<AGROW>
end
[~, subOrdre] = sort(tDeb);

T             = [];
Latitude      = [];
Longitude     = [];
HeadingVessel = [];
Immersion     = [];
for k=1:N
    T             = [T             Time{subOrdre(k)}(:)']; %#ok<AGROW>
    Latitude      = [Latitude      Lat{subOrdre(k)}(:)'];  %#ok<AGROW>
    Longitude     = [Longitude     Lon{subOrdre(k)}(:)'];  %#ok<AGROW>
    HeadingVessel = [HeadingVessel HeadingVesselLine{subOrdre(k)}(:)']; %#ok<AGROW>
    Immersion     = [Immersion     VesselHeightLine{subOrdre(k)}(:)']; %#ok<AGROW>
end

HeadingCourse = calCapFromLatLon(Latitude, Longitude, 'Time', T);
SonarSpeed    = zeros(size(HeadingCourse));
