% Callback associ�e � "Awake Lines In Box" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_DetectionCorners
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_DetectionCorners

% h1 = findobj(gcf, 'Type', 'Line');
% h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);

% Modif le 26/02/2012 par JMA � Wellington
h1 = findobj(gcf, 'Type', 'Line', 'Marker', 'none');
h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8],  'Marker', 'none');

hOther = setdiff(h1, h2);

clear curve
curve = cell(0);

curve_mode = '';
kCurve = 0;
% indFin = 0;
for k=1:length(hOther)
    Tag = get(hOther(k), 'Tag');
    if ~isempty(Tag) && ~strcmp(Tag, 'PositionPing') && ~strcmp(Tag, 'PointRemarquable')
        kCurve = kCurve + 1;
        
        XData = get(hOther(k), 'XData');
        YData = get(hOther(k), 'YData');
%         ZData{k} = get(hOther(k), 'ZData'); %#ok<AGROW>
        ZData{k} = getappdata(hOther(k), 'Time'); %#ok<AGROW>
                
        curve{kCurve} = [XData' YData'];
        curve_mode(kCurve,:) = 'line';
    end
end
curve_num = length(curve);

resolution = 3600*2; % TODO : provisoire, environ 1 m Demander la saisie en m�tres de ce param�tre
for k=1:curve_num
    curve{k} = curve{k} * resolution;
    curve_start(k,:) = curve{k}(1,:); %#ok<AGROW>
    curve_end(k,:)   = curve{k}(end,:); %#ok<AGROW>
end

TJ = [];
[index, S, curveAL, IND, c1] = Curves2Index(curve, curve_mode, curve_num); % Detect corners
% FigUtils.createSScFigure; PlotUtils.createSScPlot(curveAL{1}(index{1},1) / resolution, curveAL{1}(index{1},2) / resolution, 'ob'); grid on

TimeCorner = datetime.empty();
for k=1:length(index)
    index{k} = index{k}(2:(end-1));
    if ~isempty(ZData{k}) && ~isempty(index{k})
        TimeCorner = [TimeCorner; ZData{k}(index{k})]; %#ok<AGROW>
    end
end

EP = 0; % 0/1 Th�oriquement pour dire si on veut que les points de d�but et de fin soient des coins
[corner_out, c2]   = Index2Corners(curve, curve_mode, curve_num, index, c1, S, curveAL, IND); % localize corners
[corner_final, c3] = Refine_TJunctions(corner_out, TJ, c2, curve, curve_num, curve_start, curve_end, curve_mode, EP); %#ok<ASGLU>

corner_final = corner_final / resolution;
% corner_final = corner_final(2:(end-1),:);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(corner_final(:,1), corner_final(:,2) , 'ob'); grid on
% FigUtils.createSScFigure; PlotUtils.createSScPlot(c3); grid on

%% Trac� des markers

% GCBO = gcbo;
GCBA = get(gcbf, 'CurrentAxes');
XLim = get(GCBA, 'XLim');
YLim = get(GCBA, 'YLim');

flagHold = ishold;
if ~flagHold
    hold on
end

for k=1:size(corner_final,1)
    hPoint = PlotUtils.createSScPlot(corner_final(k,1), corner_final(k,2) , 'or');
    if isa(TimeCorner(k), 'datetime')
        if k > length(TimeCorner)
            str = 'Undetermined';
        else
            str = char(TimeCorner(k));
        end
    else
        str = timeMat2str(TimeCorner(k));
    end
    set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', str)
end

% set(hPoint, 'MarkerSize', 15, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0])
if ~flagHold
    hold off
end
set(GCBA, 'XLim', XLim);
set(GCBA, 'YLim', YLim);
