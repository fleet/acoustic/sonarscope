% Callback associ�e � "Awake Lines In Box" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_DetectionCrossingLines
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_DetectionCrossingLines

% TODO : Cette fonction est tr�s gourmande en temps. Une id�e serait de
% d�composer les lignes en segments en utilisant l'algo de d�tection des
% corners et ensuite d'employer une approche multi-�chelle (Pas de 100 par exemple), c.a.d calculer
% les intersections entre macro segments et si il y a intersection alors
% raffiner la recherche pour arriver � un pas de 1.
% Arnaud, je te sens volontaire pour cette t�che.


%% R�cup�ration des courbes

% h1 = findobj(gcf, 'Type', 'Line');
% h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);

% Modif le 26/02/2012 par JMA � Wellington
h1 = findobj(gcf, 'Type', 'Line', 'Marker', 'none');
h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8],  'Marker', 'none');

hOther = setdiff(h1, h2);

kCurve = 0;
for k=1:length(hOther)
    Tag = get(hOther(k), 'Tag');
    if ~isempty(Tag) && ~strcmp(Tag, 'PositionPing') && ~strcmp(Tag, 'PointRemarquable')
        kCurve = kCurve + 1;
        
        XData{k} = get(hOther(k), 'XData'); %#ok<AGROW>
        YData{k} = get(hOther(k), 'YData'); %#ok<AGROW>
%         ZData{k} = get(hOther(k), 'ZData'); %#ok<AGROW>
        ZData{k} = getappdata(hOther(k), 'Time'); %#ok<AGROW>
    end
end

%% Recherche des croisements

NptsMax = 100;
nbCourbes = length(XData);

if nbCourbes == 1
    ShowWaitbar = true;
    [xCross, yCross, tCross1, tCross2] = my_polyxpoly(XData{1}, YData{1}, XData{1}, YData{1}, ZData{1}, ZData{1}, NptsMax, ShowWaitbar, 'Waitbar', 1);
else
    str1 = 'Analyse intra profils ?';
    str2 = 'Analysis intra lines ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    AnalyseIntraProfil = (rep == 1);
    
    xCross  = [];
    yCross  = [];
    tCross1 = datetime.empty();
    tCross2 = datetime.empty();
    k = 0;
    N = (nbCourbes^2-nbCourbes) / 2;
    str1 = 'Traitement en cours';
    str2 = 'Processing';
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k1=1:nbCourbes
        n1 = length(XData{k1});
        if n1 < NptsMax
            sub1 = 1:n1;
        else
            sub1 = floor(linspace(1, n1, NptsMax));
        end
        
        if AnalyseIntraProfil
            k2Deb = k1;
        else
            k2Deb = k1+1;
        end
        
        for k2=k2Deb:nbCourbes
            k = k +1;
            my_waitbar(k, N, hw)
            
            n2 = length(XData{k2});
            if n2 < NptsMax
                sub2 = 1:n2;
            else
                sub2 = floor(linspace(1, n2, NptsMax));
            end
            
            [xi, ~, Ind] = polyxpoly(XData{k1}(sub1), YData{k1}(sub1), XData{k2}(sub2), YData{k2}(sub2));
            if ~isempty(xi) && any(Ind(:) ~=1)
                %             [xi, yi, Ind] = polyxpoly(XData{k1}, YData{k1}, XData{k2}, YData{k2});
                %             if ~isempty(xi) && all(Ind ~=1)
                %                 xCross(end+1) = xi; %#ok<AGROW>
                %                 yCross(end+1) = yi; %#ok<AGROW>
                %                 tCross1(end+1) = ZData{k1}(Ind(1)); %#ok<AGROW>
                %                 tCross2(end+1) = ZData{k2}(Ind(2)); %#ok<AGROW>
                %             end
                
%                 figure; plot(XData{k1}(sub1), YData{k1}(sub1), 'b'); grid on; hold on; plot(XData{k2}(sub2), YData{k2}(sub2), 'r');
                
                [xi2, yi2, t1, t2] = my_polyxpoly(XData{k1}, YData{k1}, XData{k2}, YData{k2}, ZData{k1}, ZData{k2}, NptsMax, (k1 == k2), 'Waitbar', (N == 1));
                if ~isempty(xi)
                    xCross  = [xCross; xi2]; %#ok<AGROW>
                    yCross  = [yCross; yi2]; %#ok<AGROW>
                    tCross1 = [tCross1; t1]; %#ok<AGROW>
                    tCross2 = [tCross2; t2]; %#ok<AGROW>
                end
            end
        end
    end
    my_close(hw, 'MsgEnd')
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(xCross, yCross , 'ob'); grid on
end

%% Trac� des markers

% GCBO = gcbo;
GCBA = get(gcbf, 'CurrentAxes');
XLim = get(GCBA, 'XLim');
YLim = get(GCBA, 'YLim');

flagHold = ishold;
if ~flagHold
    hold on
end

nbCross = length(xCross);
for k=1:nbCross
    hPoint = plot(xCross(k), yCross(k) , 'or');
    T = tCross1(k);
    T.Format = 'dd/MM/yyyy HH:mm:ss.SSS';
    set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', T)
    T = tCross2(k);
    T.Format = 'dd/MM/yyyy HH:mm:ss.SSS';
    set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', T)
end

% set(hPoint, 'MarkerSize', 15, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0])
if ~flagHold
    hold off
end
set(GCBA, 'XLim', XLim);
set(GCBA, 'YLim', YLim);


function [xCross, yCross, tCross1, tCross2] = my_polyxpoly(x1, y1, x2, y2, t1, t2, NptsMax, SameSegment, varargin)

xCross  = [];
yCross  = [];
tCross1 = [];
tCross2 = [];

if (max(x1) < min(x2)) || (min(x1) > max(x2)) || (max(y1) < min(y2)) || (min(y1) > max(y2))
    return
end

[varargin, flagWaitar] = getPropertyValue(varargin, 'Waitbar', 0); %#ok<ASGLU>

t1 = t1(:);
t2 = t2(:);

% % Direct
% [xi, yi, Ind] = polyxpoly(x1, y1, x2, y2);
% if ~isempty(xi) && all(Ind ~=1)
%     xCross = xi;
%     yCross = yi;
%     tCross1 = t1(Ind(1));
%     tCross2 = t2(Ind(2));
% end

% % Par morceaux
n1 = length(x1);
str1 = 'Traitement en cours';
str2 = 'Processing';
hw = create_waitbar(Lang(str1,str2), 'N', n1*flagWaitar);
for k1=1:NptsMax:n1
    my_waitbar(k1, n1, hw)
    sub1  = k1:min(k1+NptsMax, n1);
    n2 = length(x2);
    for k2=1:NptsMax:n2
        if SameSegment && (k1 == k2)
            continue
        end
%         sub2 = k2:min(k2+NptsMax, n2);
        sub2 = k2:min(k2+NptsMax+1, n2); % Modif JMA le 22/06/2020 car certaines intersections n'�taient pas d�tect�es
        
%         figure; plot(x1(sub1), y1(sub1), 'b'); grid on; hold on; plot(x2(sub2), y2(sub2), 'r');
        
        [xi, yi, Ind] = polyxpoly(x1(sub1), y1(sub1), x2(sub2), y2(sub2));
        ind1 = Ind(:,1);
        ind2 = Ind(:,2);
        sub = find((ind1 ~= 1) & (ind2 ~= 1));
        xi = xi(sub);
        yi = yi(sub);
        ind1 = ind1(sub);
        ind2 = ind2(sub);
        if ~isempty(xi)
            xCross  = [xCross; xi]; %#ok<AGROW>
            yCross  = [yCross; yi]; %#ok<AGROW>
            tCross1 = [tCross1; t1(ind1)]; %#ok<AGROW>
            tCross2 = [tCross2; t2(ind2)]; %#ok<AGROW>
        end
    end
end
my_close(hw, 'MsgEnd')
