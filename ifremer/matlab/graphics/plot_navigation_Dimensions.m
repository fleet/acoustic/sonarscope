% Callback associ�e � "Dimensions du trac�" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_Dimensions
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_Dimensions

GCF = gcf;
GCA = gca;
h1 = findobj(GCF, 'Type', 'Line');
h2 = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hOther = setdiff(h1, h2);

LatLim = [Inf -Inf];
LonLim = [Inf -Inf];
for k=1:length(hOther)
    XData = get(hOther(k), 'XData');
    LonLimLine = [min(XData) max(XData)];
    YData = get(hOther(k), 'YData');
    LatLimLine = [min(YData) max(YData)];

    LatLim(1) = min(LatLim(1), LatLimLine(1));
    LatLim(2) = max(LatLim(2), LatLimLine(2));
    LonLim(1) = min(LonLim(1), LonLimLine(1));
    LonLim(2) = max(LonLim(2), LonLimLine(2));
end
XLim = get(GCA, 'XLim');
YLim = get(GCA, 'YLim');

if any(isinf(LatLim)) || any(isinf(LonLim)) || (XLim(1) > LonLim(1)) || (XLim(2) < LonLim(2)) || (YLim(1) > LatLim(1)) || (YLim(2) < LatLim(2))
    [rep, flag] = my_questdlg(Lang('Calcul des dimensions sur :', 'Compute dimensions on :'), ...
        Lang('Zoom', 'Current extent'), Lang('Tout', 'Whole graphic'));
    if ~flag
        return
    end
    if rep == 1
        LatLim = YLim;
        LonLim = XLim;
    end
end

info_map_dimensions(LatLim, LonLim)
