% Callback associ�e � "Awake All Lines" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_ExportGoogleEarth
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_ExportGoogleEarth(varargin)

persistent persistent_nomDirKmz

[varargin, NomFicKmz] = getPropertyValue(varargin, 'NomFicKmz', []);
[varargin, SCAMPI]    = getPropertyValue(varargin, 'SCAMPI',    0); %#ok<ASGLU>

LatPatchSwath   = [];
LonPatchSwath   = [];
LabelPatchOther = [];
FaceColor       = [];
LatPatchOther   = [];
LonPatchOther   = [];

if SCAMPI
    h1 = findobj(gcf, 'Type', 'Line', '-not', 'Tag', 'PositionPing');
else
    h1 = findobj(gcf, 'Type', 'Line', 'Marker', 'none', '-not', 'Tag', 'PositionPing');
end
h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hCroix = findobj(gcf, 'Type', 'Line', 'Tag', 'PositionPing');
h3 = findobj(gcf, 'Type', 'patch');
[hOther, sub] = setdiff(h1(:), [h2(:) ; hCroix(:)]);
if isempty(h3)
    hPatch_other = [];
    hPatch_swath = [];
else
    hPatch_other = setxor(h3, h3(sub));
    hPatch_swath = h3(sub);
end

if isempty(hOther)
    return
end

for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        %         Tag = get(hOther(k), 'Tag');
        UserData = get(hOther(k), 'UserData');
        GCL  = UserData{3};
    end
    
    Label1 = get(GCL, 'Tag');
    [nomDirGE, Label1, ext] = fileparts(Label1);
    
    if k == 1
        if isempty(NomFicKmz)
            if ~isempty(persistent_nomDirKmz)
                nomDirGE = persistent_nomDirKmz;
            end
            
            str1 = 'Export de la navigation au format Google-Earth, donnez le nom du fichier :';
            str2 = 'Export of navigation into Google-Earth .kmz format, give the file name :';
            if isdeployed
                extField = {'*.kmz'};
            else
                extField = {'*.kmz'; '*.kml'};
            end
            [flag, nomFicGE] = my_uiputfile(extField, Lang(str1,str2), nomDirGE);
            if ~flag
                return
            end
            persistent_nomDirKmz = fileparts(nomFicGE);
        else
            nomFicGE = NomFicKmz;
        end
    end
    
    Label2   = get(GCL, 'UserData');
    Color{k} = get(GCL, 'Color'); %#ok<AGROW>
    Lon{k}   = get(GCL, 'XData'); %#ok<AGROW>
    Lat{k}   = get(GCL, 'YData'); %#ok<AGROW>
    
    sub0 = find((Lon{k} == 0) | (Lat{k} == 0));
    if ~isempty(sub0)
        Lon{k}(sub0) = NaN; %#ok<AGROW>
        Lat{k}(sub0) = NaN; %#ok<AGROW>
    end
    
    subNonNaN = ~isnan(Lon{k}) & ~isnan(Lat{k});
    Lon{k} = Lon{k}(subNonNaN); %#ok<AGROW>
    Lat{k} = Lat{k}(subNonNaN); %#ok<AGROW>
    %     ZData  = get(GCL, 'ZData');
    
    if ~isempty(hPatch_swath)
        LonPatchSwath{k} = get(hPatch_swath(k), 'XData'); %#ok<AGROW>
        LatPatchSwath{k} = get(hPatch_swath(k), 'YData'); %#ok<AGROW>
    end
    
    Name{k} = [Label1 ext]; %#ok<AGROW>
    if contains(Label2, Label1)
        Label2 = Label2(length(Label1)+3:end);
    end
    Label{k} = Label2; %#ok<AGROW>
end

if ~isempty(hPatch_other)
    for k=1:length(hPatch_other)
        LonPatchOther{k} = get(hPatch_other(k), 'XData'); %#ok<AGROW>
        LatPatchOther{k} = get(hPatch_other(k), 'YData'); %#ok<AGROW>
        FaceColor{k} = get(hPatch_other(k), 'FaceColor'); %#ok<AGROW>
        LabelPatchOther{k} = []; %#ok<AGROW>
        
        UIContextMenu = get(hPatch_other(k), 'UIContextMenu');
        if ~isempty(UIContextMenu)
            Children = get(UIContextMenu, 'Children');
            if ~isempty(Children)
%                 LabelPatchOther{k} = get(Children, 'Label'); %#ok<AGROW>
                LabelPatchOther{k} = get(Children, 'Text'); %#ok<AGROW> % Modif JMA le 20/05/2021
            end
        end
    end
end

plot_navigationOnGoogleEarth(Lat, Lon, Color, Name, Label, nomFicGE, ...
    'LatPatchSwath', LatPatchSwath, 'LonPatchSwath', LonPatchSwath, ...
    'LatPatchOther', LatPatchOther, 'LonPatchOther', LonPatchOther, ...
    'LabelPatchOther', LabelPatchOther, 'FaceColor', FaceColor)
