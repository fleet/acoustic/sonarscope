% Callback associ�e � "Awake All Lines" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_HideAllLines
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_ExportMarkers

GCF = gcf;
GCA = gca;
h1 = findobj(GCF, 'Tag', 'PointRemarquable');

%% Cadre de recherche

XLim = get(GCA, 'XLim');
YLim = get(GCA, 'YLim');

str1 = 'Recherche des points dans :';
str2 = 'Save points from :';
str3 = 'Graphique entier';
str4 = 'Whole frame';
str5 = 'Zoom actuel';
str6 = 'Current extent';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
if ~flag
    return
end
if rep == 1
    XLim = [-Inf Inf];
    YLim = [-Inf Inf];
end

%% Recherche des points

Ligne = {};
for k=1:length(h1)
    XData = get(h1(k), 'XData');
    YData = get(h1(k), 'YData');
    if any((XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2)))
        Ligne{end+1} = get(h1(k), 'UserData'); %#ok<AGROW>
    end
end

% Suppression des doublons et tri par ordre chronologique
Ligne = unique(Ligne);
% Ligne = sort(Ligne);

if isempty(Ligne)
    return
end

%% Nom du fichier de sauvegarde

nomDirOut = my_tempdir;
hOther = findobj(gcf, 'Type', 'Line');
for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        UserData = get(hOther(1), 'UserData');
        GCL  = UserData{3};
    end
    Label1 = get(GCL, 'Tag');
    if ~isempty(Label1) && ~strcmp(Label1, 'PointRemarquable')
        nomDir = fileparts(Label1);
        if exist(nomDir, 'dir')
            nomDirOut = nomDir;
            break
        end
    end
end
str1 = 'Nom du fichier des markers :';
str2 = 'Save Markers file as :';
[flag, nomFic] = my_uiputfile('*.txt', Lang(str1,str2), nomDirOut);
if ~flag
    return
end

%% Sauvegarde du fichier

fid = fopen(nomFic, 'w+t');
for k=1:length(Ligne)
    fprintf(fid, 'SonarTime         : %s\n', Ligne{k});
end
fclose(fid);

%% On remet les limites des axes comme on les a trouv�s

set(GCA, 'XLim', XLim);
set(GCA, 'YLim', YLim);
