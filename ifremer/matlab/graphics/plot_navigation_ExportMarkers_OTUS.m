function plot_navigation_ExportMarkers_OTUS(GCF, GCA)

h1 = findobj(GCF, 'Tag', 'PointRemarquable');

persistent persistent_lastDir

%% Cadre de recherche

XLim = get(GCA, 'XLim');
YLim = get(GCA, 'YLim');

str1 = 'Recherche des points dans :';
str2 = 'Save points from :';
str3 = 'Graphique entier';
str4 = 'Whole frame';
str5 = 'Zoom actuel';
str6 = 'Current extent';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
if ~flag
    return
end
if rep == 1
    XLim = [-Inf Inf];
    YLim = [-Inf Inf];
end

%% Recherche des points

Ligne = {};
for k=1:length(h1)
    XData = get(h1(k), 'XData');
    YData = get(h1(k), 'YData');
    if any((XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2)))
        UserData = get(h1(k), 'UserData');

        Depth     = getappdata(h1(k), 'Depth');
        Height    = getappdata(h1(k), 'Height');
        Heading   = getappdata(h1(k), 'Heading');
        FileNames = getappdata(h1(k), 'FileNames');
        Comment   = getappdata(h1(k), 'Comment');
        if iscell(Comment)
            Comment = '';
        end
        if iscell(FileNames)
            FileNames = FileNames{1};
        end
        Ligne{end+1} = sprintf('%s %s  %s  %f %f %f "%s" "%s"', UserData, num2strPrecis(YData), ...
            num2strPrecis(XData), Depth, Height, Heading, FileNames, Comment); %#ok<AGROW>
    end
end

if isempty(Ligne)
    return
end

%% Recherche des lignes dupliqu�es

Ligne = unique(Ligne);

%% Nom du fichier de sauvegarde

if isempty(persistent_lastDir)
    nomDirOut = my_tempdir;
else
    nomDirOut = persistent_lastDir;
end

hOther = findobj(gcf, 'Type', 'Line');
for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        UserData = get(hOther(1), 'UserData');
        GCL  = UserData{3};
    end
    Label1 = get(GCL, 'Tag');
    if ~isempty(Label1) && ~strcmp(Label1, 'PointRemarquable')
        nomDir = fileparts(Label1);
        if exist(nomDir, 'dir')
            nomDirOut = nomDir;
            break
        end
    end
end
[flag, nomFic] = my_uiputfile('*.txt', 'Save Workspace as', nomDirOut);
if ~flag
    return
end
persistent_lastDir = fileparts(nomFic);

%% Sauvegarde du fichier

fid = fopen(nomFic, 'w+t');
fprintf(fid, 'Date Hour Latitude Longitude Depth Height Heading FileName Comment\n');
for k=1:length(Ligne)
    fprintf(fid, '%s\n', Ligne{k});
end
fclose(fid);

%% On remet les limites des axes comme on les a trouv�s

set(GCA, 'XLim', XLim);
set(GCA, 'YLim', YLim);
