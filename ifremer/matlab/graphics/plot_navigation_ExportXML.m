% Callback associ�e � "Export Navigation en XML" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_ExportXML
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_ExportXML(varargin)

persistent persistent_nomDirXML

[varargin, NomFicXml] = getPropertyValue(varargin, 'NomFicXml', []); %#ok<ASGLU>

GCF = gcf;

LatPatchSwath   = [];
LonPatchSwath   = [];
LabelPatchOther = [];
FaceColor       = [];
LatPatchOther   = [];
LonPatchOther   = [];

h1 = findobj(GCF, 'Type', 'Line',  '-not', 'Tag', 'PointRemarquable');
h2 = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
h3 = findobj(GCF, 'Type', 'patch');
[hOther, sub] = setdiff(h1, h2);
if isempty(h3)
    hPatch_other = [];
    hPatch_swath = [];
else
    hPatch_other = setxor(h3, h3(sub));
    hPatch_swath = h3(sub);
end

if isempty(hOther)
    return
end

for k=length(hOther):-1:1
    Tag = get(hOther(k), 'Tag');
    if strcmp(Tag, 'PositionPing')
        hOther(k) = [];
    end
end

k2 = 0;
nbProfils = length(hOther);
str1 = 'Exportation de la navigation des profils pour GLOBE';
str2 = 'Exporting lines navigation for GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', nbProfils);
for k1=1:nbProfils
    my_waitbar(k1, nbProfils, hw)
    
    Type = get(hOther(k1), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k1);
    else
%         Tag = get(hOther(k1), 'Tag');
        UserData = get(hOther(k1), 'UserData');
        GCL = UserData{3};
    end
    
    Tk1 = getappdata(GCL, 'Time'); % TODO : getappdata(GCL, 'Datetime') partout !!!
    if isempty(Tk1)
        Tk1 = get(GCL, 'ZData');
        if isempty(Tk1)
            continue
        end
        if ~isa(Tk1, 'datetime')
            continue
        end
    end
    
    k2 = k2 + 1;
    Time{k2} = Tk1; %#ok<AGROW>
    Label1 = get(GCL, 'Tag');
    nomDirXML = fileparts(Label1);
    
    if (k2 == 1) && isempty(NomFicXml)
        if ~isempty(persistent_nomDirXML)
            nomDirXML = persistent_nomDirXML;
        end
        
        str1 = 'Trac� de la navigation dans GLOBE (.xml)';
        str2 = 'Plot nav in GLOBE (.xml)';
        [flag, NomFicXml] = my_uiputfile('*.xml', Lang(str1,str2), nomDirXML);
        if ~flag
            return
        end
        persistent_nomDirXML = fileparts(NomFicXml);
    end

    Label1 = get(GCL, 'Tag');
    [~, Label1, ext] = fileparts(Label1);   
        
    Label2    = get(GCL, 'UserData');
    Color{k2} = get(GCL, 'Color'); %#ok<AGROW>
    Lon{k2}   = get(GCL, 'XData'); %#ok<AGROW>
    Lat{k2}   = get(GCL, 'YData'); %#ok<AGROW>
    
    X = getappdata(GCL, 'Immersion');
    if isempty(X)
        Immersion = [];
    else
        Immersion{k2} = X; %#ok<AGROW>
    end
    
    Lon{k2}  = Lon{k2}(~isnan(Lon{k2})); %#ok<AGROW>
    Lat{k2}  = Lat{k2}(~isnan(Lat{k2})); %#ok<AGROW>
    Time{k2} = Time{k2}(~isnat(Time{k2})); %#ok<AGROW>
    %     ZData  = get(GCL, 'ZData');
    
    if ~isempty(Immersion)
        Immersion{k2} = Immersion{k2}(~isnan(Immersion{k2})); %#ok<AGROW>
    end

    if ~isempty(hPatch_swath)
        LonPatchSwath{k2} = get(hPatch_swath(k2), 'XData'); %#ok<AGROW>
        LatPatchSwath{k2} = get(hPatch_swath(k2), 'YData'); %#ok<AGROW>
    end

    Name{k2} = [Label1 ext]; %#ok<AGROW>
    if ~isempty(Label2) && contains(Label2,  Label1)
        Label2 = Label2(length(Label1)+3:end);
    end
    Text{k2} = Label2; %#ok<AGROW>
end

if ~isempty(hPatch_other)
    for k=1:length(hPatch_other)
        LonPatchOther{k} = get(hPatch_other(k), 'XData'); %#ok<AGROW>
        LatPatchOther{k} = get(hPatch_other(k), 'YData'); %#ok<AGROW>
        FaceColor{k} = get(hPatch_other(k), 'FaceColor'); %#ok<AGROW>
        LabelPatchOther{k} = []; %#ok<AGROW>
        
        UIContextMenu = get(hPatch_other(k), 'UIContextMenu');
        if ~isempty(UIContextMenu)
            Children = get(UIContextMenu, 'Children');
            if ~isempty(Children)
%                 LabelPatchOther{k} = get(Children, 'Label'); %#ok<AGROW>
                LabelPatchOther{k} = get(Children, 'Text'); %#ok<AGROW> % Modif JMA le 20/05/2021
            end
        end
    end
end


% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Ajout JMA le 31/10/2019 pour mission QUOI. GLOBE ne veut pas charger la
% ligne si on a pass� l'immersion  TODO CPO
Immersion = [];
% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


% TODO : ce n'est pas tr�s heureux d'avoir une fonction qui s'appelle
% "plot_navigationExportXML" et l'autre "plot_navigation_ExportXML" : "confusant"
plot_navigationExportXML(Lat, Lon, Time, Color, Name, Text, NomFicXml, 'Immersion', Immersion);

my_close(hw, 'MsgEnd');
