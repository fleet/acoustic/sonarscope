function [Fig1, hAxes] = plot_navigation_Figure(Fig1, varargin)

[varargin, NavOTUS]  = getPropertyValue(varargin, 'NavOTUS',  0);
[varargin, Name]     = getPropertyValue(varargin, 'Name',     'Navigation');
[varargin, Position] = getPropertyValue(varargin, 'Position', []); %#ok<ASGLU>

if NavOTUS
    if isempty(Fig1) || ~ishandle(Fig1)
        Fig1 = FigUtils.createSScFigure('Name', Name);
        for k1=1:2
            hAxes(k1) = subplot(1,2,k1); %#ok<AGROW>
            xlabel('Longitude (deg)')
            ylabel('Latitude (deg)')
        end
    else
        Fig1 = FigUtils.createSScFigure(Fig1);
        hAxes = findobj(Fig1, 'Type', 'Axes');
    end
else
    if isempty(Fig1) || ~ishandle(Fig1)
        Fig1 = FigUtils.createSScFigure('Name', Name);
        hAxes = axes;
        xlabel('Longitude (deg)')
        ylabel('Latitude (deg)')
    else
        Fig1 = FigUtils.createSScFigure(Fig1);
        hAxes = findobj(Fig1, 'Type', 'Axes');
    end
end

if ~isempty(Position)
    set(Fig1, 'Position', Position)
end

if isempty(findobj(Fig1, 'Text', 'Navigation'))
    f0 = uimenu('Text', 'Navigation');
    
    f1 = uimenu(f0, 'Text', 'Lines');
    uimenu(f1, 'Text', 'Save lines list',                  'Callback', @SaveLines);
    uimenu(f1, 'Text', 'Hide all lines',                   'Callback', @HideAllLines);
    uimenu(f1, 'Text', 'Awake all lines',                  'Callback', @ShowAllLines);
    uimenu(f1, 'Text', 'Hide lines in Current extent',     'Callback', @HideLinesInBox);
    uimenu(f1, 'Text', 'Awake lines in Current extent',    'Callback', @ShowLinesInBox);
    uimenu(f1, 'Text', 'Invert selection',                 'Callback', @InvertLines);
    uimenu(f1, 'Text', 'Hide U-Turns',                     'Callback', @HideUTurns);
    uimenu(f1, 'Text', 'Hide cross lines',                 'Callback', @HideCrossLines);
    uimenu(f1, 'Text', 'Select lines from k1 to k2',       'Callback', @SelectTimeInterval);
    uimenu(f1, 'Text', 'Awake lines according to heading', 'Callback', @ShowLinesHeading);
    
    f1 = uimenu(f0, 'Text', 'Export');
    uimenu(f1, 'Text', 'Google Earth (.kmz)', 'Callback', @ExportGoogleEarth);
    uimenu(f1, 'Text', 'GLOBE (.xml)',        'Callback', @ExportNavigation);
    uimenu(f1, 'Text', 'Shape file (*.shp)',  'Callback', @ExportNavShapeFile);
    uimenu(f1, 'Text', 'CARAIBES (*.nvi)',    'Callback', @ExportNavCaraibes);
    uimenu(f1, 'Text', 'ASCII (*.txt)',       'Callback', @ExportNavASCII);
    uimenu(f1, 'Text', 'ASCII-AUV (*.txt)',   'Callback', @ExportNavASCIIAUV);
    
    f1 = uimenu(f0, 'Text', 'Import');
    uimenu(f1, 'Text', 'Shape file (*.shp)',  'Callback', @ImportNavShapeFile);
    
    % TODO : rajouter export en .nvi et en .txt, .csv
    
    f1 = uimenu(f0, 'Text', 'Markers');
    uimenu(f1, 'Text', 'Export',           'Callback', @MarkersExport);
    uimenu(f1, 'Text', 'Import',           'Callback', @MarkersImport);
    uimenu(f1, 'Text', 'Define intervals', 'Callback', @MarkersIntervals);
    
    f1 = uimenu(f0, 'Text', 'Other');
    uimenu(f1, 'Text', 'Dimensions on a printer at different scales', 'Callback', @Dimensions);
    uimenu(f1, 'Text', 'List of Overlaping Lines on Cross',           'Callback', @ListeOfOverlapingPatches);
    uimenu(f1, 'Text', 'Detection of corners',                        'Callback', @DetectionCorners);
    uimenu(f1, 'Text', 'Detection of crossing lines',                 'Callback', @DetectionCrossingLines);
    
    f1 = uimenu(f0, 'Text', 'Mask');
    uimenu(f1, 'Text', 'Create a raster mask',     'Callback', @CreateRasterMask);
    
    f0 = uimenu('Text', 'Axis');
    uimenu(f0, 'Text', 'Normal', 'Callback', @AxisNormal);
    uimenu(f0, 'Text', 'Geo',    'Callback', @AxisGeo);
end


%% Callback de sauvegarde des lignes affich�es dans un fichier

    function SaveLines(varargin)
        plot_navigation_SaveLines
    end

%% Callback de sauvegarde des lignes affich�es dans un fichier

    function InvertLines(varargin)
        plot_navigation_InvertLines
    end

%% Callback de sauvegarde des lignes affich�es dans un fichier

    function SelectTimeInterval(varargin)
        plot_navigation_SelectTimeInterval
    end

%% Callback de calcul des dimensions

    function Dimensions(varargin)
        plot_navigation_Dimensions
    end

%% Callback Hide All Lines

    function HideAllLines(varargin)
        plot_navigation_HideAllLines
    end

%% Callback Awake All Lines

    function ShowAllLines(varargin)
        plot_navigation_ShowAllLines
    end

%% Callback Hide UTurnes

    function HideUTurns(varargin)
        plot_navigation_HideUTurns
    end

%% Callback Hide CrossLines

    function HideCrossLines(varargin)
        plot_navigation_HideCrossLines
    end

%% Callback Hide UTurnes

    function ShowLinesHeading(varargin)
        plot_navigation_ShowLinesHeading
    end

%% Export .kmz

    function ExportGoogleEarth(varargin)
        plot_navigation_ExportGoogleEarth
    end

%% Export points remarquables

    function MarkersExport(varargin)
        plot_navigation_ExportMarkers
    end

%% Import points remarquables

    function MarkersImport(varargin)
        plot_navigation_ImportMarkers
    end

%% Define intervals from the markers

    function MarkersIntervals(varargin)
        plot_navigation_MarkersSetIntervals
    end

%% Export de la navigation en XML

    function ExportNavigation(varargin)
        plot_navigation_ExportXML
    end

%% Export de la navigation en fichier ESRI Shape file

    function ExportNavShapeFile(varargin)
        plot_navigation_ShapeFile
    end

%% Import de la navigation en fichier ESRI Shape file

    function ImportNavShapeFile(varargin)
        plot_navigation_ImportShapeFile
    end

%% Calcul du masque binaire

    function CreateRasterMask(varargin)
        Comment = plot_navigation_RasterMask; %#ok<NASGU>
    end

%% Export de la navigation en fichier .nvi CARAIBES

    function ExportNavCaraibes(varargin)
        plot_navigation_Caraibes
    end

%% Export de la navigationen ASCII

    function ExportNavASCII(varargin)
        plot_navigation_ASCII
    end

%% Export de la navigationen ASCII-AUV

    function ExportNavASCIIAUV(varargin)
        plot_navigation_ASCII_AUV
    end

%% Awake des lines dans le zoom

    function ShowLinesInBox(varargin)
        plot_navigation_ShowLinesInBox
    end

%% Hide des lines dans le zoom

    function HideLinesInBox(varargin)
        plot_navigation_HideLinesInBox
    end

%% Liste Of Overlaping Patches on cross

    function ListeOfOverlapingPatches(varargin)
        plot_navigation_ListeOfOverlapingPatches
    end

%% D�tection des coins

    function DetectionCorners(varargin)
        plot_navigation_DetectionCorners
    end

%% D�tection des croisements

    function DetectionCrossingLines(varargin)
        plot_navigation_DetectionCrossingLines
    end

%% Callback Axe Normal

    function AxisNormal(varargin)
        GCA = findobj(gcbf, 'Type', 'axes'); % Un peu rapide comme fa�on de faire
        axis(GCA(1))
        axis normal
    end

%% Callback Axe CartoNorm�

    function AxisGeo(varargin)
        GCA = findobj(gcbf, 'Type', 'axes'); % Un peu rapide comme fa�on de faire
        axis(GCA(1))
        axisGeo
    end

end
