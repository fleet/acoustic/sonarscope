% Callback associ�e � "Hide All Lines" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_HideAllLines
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_HideAllLines

GCF = gcf;
GCA = gca;

% hOther = findobj(GCF, 'Label', 'Hide line');
hOther = findobj(GCF, 'Text', 'Hide line'); % Modif JMA le 20/05/2021
for k=1:length(hOther)
    GCBO = hOther(k);
    UserData = get(GCBO, 'UserData');
    h = UserData{3};
    set(h, 'Color', [0.8 0.8 0.8])
    UserData{2} = 'Off';
    set(GCBO, 'UserData', UserData)
    
    nomFic = UserData{4};
    hOther2 = findobj(GCA, 'Tag', nomFic);
    hOther2(hOther2 == h) = [];
    set(hOther2, 'Visible', 'Off')
end
