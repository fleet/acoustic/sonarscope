function plot_navigation_HideCrossLines

GCF = gcf;
GCA = gca;
h1 = findobj(GCF, 'Type', 'Line', '-not', 'Tag', 'PositionPing', '-not', 'Tag', 'PointRemarquable');
h2 = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hCroix = findobj(GCF, 'Type', 'Line', 'Tag', 'PositionPing');

[hOther, ia] = setdiff(h1(:), [h2(:) ; hCroix(:)]);
if isempty(hOther)
    return
end

% hOtherLine = findobj(GCF, 'Label', 'Hide line');
hOtherLine = findobj(GCF, 'Text', 'Hide line'); % Modif JMA le 20/05/2021
hOtherLine = hOtherLine(ia);

H = [];
I = [];
for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    UserData = get(hOtherLine(k), 'UserData');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        GCL  = UserData{3};
    end
    
    Lon = get(GCL, 'XData');
    Lat = get(GCL, 'YData');
    
    sub0 = find((Lon == 0) | (Lat == 0));
    if ~isempty(sub0)
        Lon(sub0) = NaN;
        Lat(sub0) = NaN;
    end
    
    subNonNaN = ~isnan(Lon) & ~isnan(Lat);
    Lon = Lon(subNonNaN);
    Lat = Lat(subNonNaN);
    
    [Heading, speed, distance] = calCapFromLatLon(Lat, Lon); %#ok<ASGLU>
    
    H = [H; Heading]; %#ok<AGROW>
    I = [I; repmat(k,length(Heading),1)]; %#ok<AGROW>
    nbPoints(k) = length(Heading); %#ok<AGROW>
end

H = mod(H+360, 180);
idx = kmeans(H, 2);
%{
figure;
hc(1) = subplot(3,1,1); plot(H, '*'); grid on;
hc(2) = subplot(3,1,2); plot(idx, '*'); grid on;
hc(3) = subplot(3,1,3); plot(I, '*'); grid on;
linkaxes(hc, 'x')
%}
sub = (idx > 1);
ind = I(sub);
hi = my_hist(ind,1:max(ind));

% subCrossLines = find(hi ~= 0);
subCrossLines = find(hi >= (nbPoints / 2));

for k0=1:length(subCrossLines)
    k = subCrossLines(k0);
    Type = get(hOther(k), 'Type');
    UserData = get(hOtherLine(k), 'UserData');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        GCL  = UserData{3};
    end

    set(GCL, 'Color', [0.8 0.8 0.8])
    UserData{2} = 'Off';
    set(hOtherLine(k), 'UserData', UserData)
    
    nomFic = UserData{4};
    h = UserData{3};
    hOther2 = findobj(GCA, 'Tag', nomFic);
    hOther2(hOther2 == h) = [];
    set(hOther2, 'Visible', 'Off')
end
