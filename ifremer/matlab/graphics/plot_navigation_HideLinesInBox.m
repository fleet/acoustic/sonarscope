% Callback associ�e � "Awake Lines In Box" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_HideLinesInBox
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_HideLinesInBox

GCA = gca;

XLim = get(GCA, 'XLim');
YLim = get(GCA, 'YLim');

% hg = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hg = findobj(gcbf, 'Type', 'line');
for i=1:length(hg)
    pppp = get(hg(i), 'Tag');
    if isempty(pppp)
        subIsNav(i) = false; %#ok<AGROW>
    else
        subIsNav(i) = true; %#ok<AGROW>
    end
end
hg = hg(subIsNav);
        
% hOther = findobj(gcf, 'Label', 'Hide line');
hOther = findobj(gcf, 'Text', 'Hide line'); % Modif JMA le 20/05/2021
for k=1:length(hOther)
    GCBO = hOther(k);
    GCA = gca;
    
    UserData = get(GCBO, 'UserData');
    
    LonLim = UserData{6};
    if (LonLim(2) < XLim(1)) || (LonLim(1) > XLim(2))
        continue
    end
    
    LatLim = UserData{5};
    if (LatLim(2) < YLim(1)) || (LatLim(1) > YLim(2))
        continue
    end
    
    XData = get(hg(k), 'XData');
    YData = get(hg(k), 'YData');
    
    flag = (XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2));
    if all(~flag)
        continue
    end

    h = UserData{3};
    set(h, 'Color', [0.8 0.8 0.8])
    UserData{2} = 'Off';
    set(GCBO, 'UserData', UserData)
    
    nomFic = UserData{4};
    hOther2 = findobj(GCA, 'Tag', nomFic);
    hOther2(hOther2 == h) = [];
    set(hOther2, 'Visible', 'Off')
end
