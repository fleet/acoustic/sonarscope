function plot_navigation_HideUTurns

GCF = gcf;
GCA = gca;
h1 = findobj(GCF, 'Type', 'Line', '-not', 'Tag', 'PositionPing', '-not', 'Tag', 'PointRemarquable');
h2 = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hCroix = findobj(GCF, 'Type', 'Line', 'Tag', 'PositionPing');

[hOther, ia] = setdiff(h1(:), [h2(:) ; hCroix(:)]);
if isempty(hOther)
    return
end

% hOtherLine = findobj(GCF, 'Label', 'Hide line');
hOtherLine = findobj(GCF, 'Text', 'Hide line'); % Modif JMA le 20/05/2021
hOtherLine = hOtherLine(ia);

for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    UserData = get(hOtherLine(k), 'UserData');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        GCL  = UserData{3};
    end
    
    Lon = get(GCL, 'XData');
    Lat = get(GCL, 'YData');
    
    sub0 = find((Lon == 0) | (Lat == 0));
    if ~isempty(sub0)
        Lon(sub0) = NaN;
        Lat(sub0) = NaN;
    end
    
    subNonNaN = ~isnan(Lon) & ~isnan(Lat);
    Lon = Lon(subNonNaN);
    Lat = Lat(subNonNaN);
    
    Heading = calCapFromLatLon(Lat, Lon);
    typeLine = classifyProfileFromHeading(Heading);
    
    switch typeLine
        case {'U-Turn'; 'Short file'}
            set(GCL, 'Color', [0.8 0.8 0.8])
            UserData{2} = 'Off';
            set(hOtherLine(k), 'UserData', UserData)
            
            nomFic = UserData{4};
            h = UserData{3};
            hOther2 = findobj(GCA, 'Tag', nomFic);
            hOther2(hOther2 == h) = [];
            set(hOther2, 'Visible', 'Off')
    end
end
