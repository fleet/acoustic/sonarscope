% Callback associ�e � "Awake All Lines" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_HideAllLines
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_ImportMarkers(varargin)

persistent persistent_lastDir

[varargin, GCF]           = getPropertyValue(varargin, 'Fig', []);
[varargin, NomFicMarkers] = getPropertyValue(varargin, 'NomFicMarkers', []); %#ok<ASGLU>

if isempty(GCF)
    GCF = gcf;
    figure(GCF)
    GCA = gca;
    GCBF = gcbf;
    GCBA = get(GCBF, 'CurrentAxes');
else
    GCA  = gca;
    GCBA = GCA;
    GCBF = GCF;
end

% h1 = findobj(GCF, 'Tag', 'PointRemarquable');

XLim = get(GCBA, 'XLim');
YLim = get(GCBA, 'YLim');

%% Nom du fichier des Markers

if isempty(NomFicMarkers)
    if isempty(persistent_lastDir)
        nomDir = my_tempdir;
    else
        nomDir = persistent_lastDir;
    end
    [flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.txt', 'RepDefaut', nomDir);
    if ~flag
        return
    end
    persistent_lastDir = lastDir;
else
    if ischar(NomFicMarkers)
        NomFicMarkers = {NomFicMarkers};
    end
    ListeFic = NomFicMarkers;
    persistent_lastDir = fileparts(ListeFic{1});
end

%% Lecture des Markers

tMarker = datetime.empty();
for k=1:length(ListeFic)
    fid = fopen(ListeFic{k}, 'r');
    while 1
        tline = fgetl(fid);
        if ~ischar(tline)
            break
        end
        if strcmp(tline , 'Point Remarquable')
            continue
        end
        Mots = strsplit(tline);
        if strcmp(Mots{1}, '%')
            continue
        end
        if length(Mots) == 4
            dIfr = dayStr2Ifr(Mots{3});
            hIfr = hourStr2Ifr(Mots{4});
%             t = cl_time('timeIfr', dIfr, hIfr);
%             tMarker(end+1) = datetime(t.timeMat, 'ConvertFrom', 'datenum'); %#ok<AGROW>
            tMarker(end+1) = timeIfr2Datetime(dIfr, hIfr); %#ok<AGROW>
        end
    end
    fclose(fid);
end
tMarker = unique(tMarker);

%% Affichage des points

PlotMarkersOnNavigation(tMarker)

%% On remet les limites des axes comme on les a trouv�s

set(GCA, 'XLim', XLim);
set(GCA, 'YLim', YLim);

    function PlotMarkersOnNavigation(tMarker)
        %         global hLines XData YData ZData sub ordre Dist DistMin tMin% Dans le debuggueur uniquement
        global hLines ordre
        %         GCBO = gcbo;
        GCBA = get(GCF, 'CurrentAxes');
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        
        hLines = findobj(GCBF, 'Type', 'line');
        for k1=1:length(hLines)
            pppp = get(hLines(k1), 'Tag');
            if isempty(pppp) || strcmp(pppp, 'PointRemarquable')
                subIsNav(k1) = false; %#ok<AGROW>
            else
                subIsNav(k1) = true; %#ok<AGROW>
            end
        end
        hLines = hLines(subIsNav);
        
        Delta = duration(0,0,0.5);
        for k1=1:length(hLines)
            Tag = get(hLines(k1), 'Tag');
            if strcmp(Tag, 'PositionPing')
                continue
            end
            %             UserData = get(hLines(k1), 'UserData');
            ZDataLine = getappdata(hLines(k1), 'Time');
            if isempty(ZDataLine)
                continue
            end
%             ZDataLine = datenum(ZDataLine); 
            
            XDataLine = get(hLines(k1), 'XData');
            YDataLine = get(hLines(k1), 'YData');
            
%             tMarker = datetime(tMarker, 'ConvertFrom', 'datenum');

            for k2=1:length(tMarker)
                if (tMarker(k2) >= (ZDataLine(1) - Delta)) && (tMarker(k2) <= (ZDataLine(end) + Delta))
                    [~, ordre] = sort(abs(ZDataLine - tMarker(k2)));
                    hold on;
%                     hPoint = PlotUtils.createSScPlot(XDataLine(ordre(1)), YDataLine(ordre(1)), 'or', 'isLightContextMenu', 1);
                    hPoint = plot(XDataLine(ordre(1)), YDataLine(ordre(1)), 'or'); % Modif JMA le 17/06/2018 : on revient au plus simple car l'insertion du uicontextmenu prend �norm�ment de temps (m�me en version light)
                    set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', char(ZDataLine(ordre(1))))
%                     set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', char(ZDataLine(ordre(1))))
                    hold off;
                end
            end
        end
    end
end
