function plot_navigation_ImportMarkers_OTUS(GCA)

persistent persistent_lastDir

XLim = get(GCA, 'XLim');
YLim = get(GCA, 'YLim');
        
%% Nom du fichier des Markers

if isempty(persistent_lastDir)
    nomDir = my_tempdir;
else
    nomDir = persistent_lastDir;
end
[flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.txt', 'RepDefaut', nomDir);
if ~flag
    return
end
persistent_lastDir = lastDir;

%% Lecture des Markers

tMarker  = datetime.empty();
Depth    = [];
Height   = [];
Heading  = [];
Latitude = [];
Longitude = [];
FileName = {};
Comment  = {};
for k=1:length(ListeFic)
    fid = fopen(ListeFic{k}, 'r');
    tline = fgetl(fid); %#ok<NASGU> % Lecture de l'entete
    while 1
        tline = fgetl(fid);
        if ~ischar(tline)
            break
        end
        if strcmp(tline , 'Point Remarquable')
            continue
        end
        Mots = strsplit(tline);
        if strcmp(Mots{1}, '%')
            continue
        end
        if length(Mots) >= 2
            str = [Mots{1} ' ' Mots{2}];
            tMarker(end+1, 1) = datetime(str); %#ok<AGROW> 'InputFormat', 'xxxxxx'
            
            Latitude(end+1)  = str2double(Mots{5}); %#ok<AGROW>
            Longitude(end+1) = str2double(Mots{5}); %#ok<AGROW>
            
            Depth(end+1)    = str2double(Mots{5}); %#ok<AGROW>
            Height(end+1)   = str2double(Mots{6}); %#ok<AGROW>
            Heading(end+1)  = str2double(Mots{7}); %#ok<AGROW>
            FileName{end+1} = Mots{8}; %#ok<AGROW>
            Comment{end+1}  = Mots{9}; %#ok<AGROW>
            
%             setappdata(hPoint, 'Depth',     Depth)
%             setappdata(hPoint, 'Height',    Height)
%             setappdata(hPoint, 'Heading',   Heading)
%             setappdata(hPoint, 'FileNames', FileName)
            
        end
    end
    fclose(fid);
end

%% Affichage des points

SavePosition(GCA, tMarker, Latitude, Longitude, Depth, Height, Heading, FileName, Comment)

%% On remet les limites des axes comme on les a trouv�es

set(GCA, 'XLim', XLim);
set(GCA, 'YLim', YLim);



    function SavePosition(GCA, tMarker, Latitude, Longitude, Depth, Height, Heading, FileName, Comment)
        
%         global hLines ordre

        XLim = get(GCA, 'XLim');
        YLim = get(GCA, 'YLim');
        
        hLines = findobj(gcbf, 'Type', 'line');
        for k1=1:length(hLines)
            pppp = get(hLines(k1), 'Tag');
            if isempty(pppp)
                subIsNav(k1) = false; %#ok<AGROW>
            else
                subIsNav(k1) = true; %#ok<AGROW>
            end
        end
        hLines = hLines(subIsNav);
        
        for k1=1:length(hLines)
            Tag = get(hLines(k1), 'Tag');
            if strcmp(Tag, 'PositionPing')
                continue
            end
%             UserData = get(hLines(k1), 'UserData');
            ZDataLine = get(hLines(k1), 'ZData');
            if isempty(ZDataLine)
                continue
            end
            XDataLine = get(hLines(k1), 'XData');
            YDataLine = get(hLines(k1), 'YData');
            
            for k2=1:length(tMarker)
                if (tMarker(k2) >= ZDataLine(1)) && (tMarker(k2) <= ZDataLine(end))
                    [~, ordre] = sort(abs(ZDataLine - tMarker(k2)));
                    hold on;
                    hPoint = PlotUtils.createSScPlot(GCA, XDataLine(ordre(1)), YDataLine(ordre(1)), 'or');
                    set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', char(ZDataLine(ordre(1))))
                    hold off;
                    
                    setappdata(hPoint, 'Depth',     Depth(k2))
                    setappdata(hPoint, 'Height',    Height(k2))
                    setappdata(hPoint, 'Heading',   Heading(k2))
                    setappdata(hPoint, 'FileNames', FileName{k2})
                    setappdata(hPoint, 'Comment',   Comment{k2}(2:end-1))
                    setappdata(hPoint, 'Latitude',  YDataLine(ordre(1)))
                    setappdata(hPoint, 'Longitude', XDataLine(ordre(1)))
                    setappdata(hPoint, 'Datetime',  tMarker(k2))
                end
            end
        end
    end
end
