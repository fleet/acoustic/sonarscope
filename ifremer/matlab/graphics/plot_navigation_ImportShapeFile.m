function plot_navigation_ImportShapeFile(varargin)

% [varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
[varargin, nomFicShp] = getPropertyValue(varargin, 'nomFicShp', []);
[varargin, geometry]  = getPropertyValue(varargin, 'geometry',  []); %#ok<ASGLU>

GCF = gcf;
GCA = gca;
h1 = findobj(GCF, 'Type', 'Line', '-not', 'Tag', 'PositionPing', '-not', 'Tag', 'PointRemarquable');
h2 = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hCroix = findobj(GCF, 'Type', 'Line', 'Tag', 'PositionPing');

[hOther, ia] = setdiff(h1(:), [h2(:) ; hCroix(:)]);
if isempty(hOther)
    return
end

% hOtherLine = findobj(GCF, 'Label', 'Hide line');
hOtherLine = findobj(GCF, 'Text', 'Hide line'); % Modif JMA le 20/05/2021
hOtherLine = hOtherLine(ia);

LatLim = [Inf -Inf];
LonLim = [Inf -Inf];
for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    UserData = get(hOtherLine(k), 'UserData');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        GCL  = UserData{3};
    end
    
    Lon = get(GCL, 'XData');
    Lat = get(GCL, 'YData');
    
    sub0 = find((Lon == 0) | (Lat == 0));
    if ~isempty(sub0)
        Lon(sub0) = NaN;
        Lat(sub0) = NaN;
    end
    
    subNonNaN = ~isnan(Lon) & ~isnan(Lat);
    Lon = Lon(subNonNaN);
    Lat = Lat(subNonNaN);
    
    LatLim(1) = min(LatLim(1), min(Lat));
    LatLim(2) = max(LatLim(2), max(Lat));
    LonLim(1) = min(LonLim(1), min(Lon));
    LonLim(2) = max(LonLim(2), max(Lon));
end
LatLim(1) = floor(LatLim(1));
LatLim(2) = ceil(LatLim(2));
LonLim(1) = floor(LonLim(1));
LonLim(2) = ceil(LonLim(2));

I0 = cl_image;
[flag, a] = import_etopo(I0, 'LatLim', LatLim, 'LonLim', LonLim);
if ~flag
    return
end


if isempty(nomFicShp)
    str1 = 'S�lection d''un fichier shape file';
    str2 = 'Select the shape file';
    [flag, nomFicMasque] = my_uigetfile('*.shp', Lang(str1,str2), my_tempdir);
    if ~flag
        return
    end
end

[flag, a] = ROI_import_shp(a, nomFicMasque);
if flag
    RegionOfInterest = get(a, 'RegionOfInterest');
end

axes(GCA)
hold on;
hS = plot(RegionOfInterest.pX, RegionOfInterest.pY, 'k'); %#ok<NASGU>
% set(hS, 'Color', RegionOfInterest.Color);
