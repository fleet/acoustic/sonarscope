function plot_navigation_InvertLines

% hOther = findobj(gcf, 'Label', 'Hide line');
hOther = findobj(gcf, 'Text', 'Hide line'); % Modif JMA le 20/05/2021

for k=1:length(hOther)
    GCBO = hOther(k);
    GCA = gca;
    UserData = get(GCBO, 'UserData');
    status = UserData{2};
    h = UserData{3};
    
    switch status
        case 'On' %% Hide lines
            set(h, 'Color', [0.8 0.8 0.8])
            UserData{2} = 'Off';
            set(GCBO, 'UserData', UserData)
            
            nomFic = UserData{4};
            hOther2 = findobj(GCA, 'Tag', nomFic);
            hOther2(hOther2 == h) = [];
            set(hOther2, 'Visible', 'Off')
            
        case 'Off' %% Awake lines
            Color = UserData{1};
            set(h, 'Color', Color)
            UserData{2} = 'On';
            set(GCBO, 'UserData', UserData)
            
            nomFic = UserData{4};
            hOther2 = findobj(GCA, 'Tag', nomFic);
            hOther2(hOther2 == h) = [];
            set(hOther2, 'Visible', 'On')
            
        otherwise
            my_breakpoint
    end
end
