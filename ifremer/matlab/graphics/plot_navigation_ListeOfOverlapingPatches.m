% Callback associ�e � "Awake Lines In Box" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_ListeOfOverlapingPatches
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_ListeOfOverlapingPatches

GCBF = gcbf;

hPatch = findobj(GCBF, 'Type', 'patch');
if isempty(hPatch)
    str1 = 'Ce trac� n''est pas un trac� de couverture.';
    str2 = 'This is not a "coverage" plot.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

hCross = findobj(GCBF, 'Type', 'line', 'Tag', 'PositionPing');
if isempty(hCross)
    str1 = 'Il n''y a pas de croix trac�e sur cette figure.';
    str2 = 'There is no cross in this figure.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

for k=1:length(hCross)
    XData = get(hCross(k), 'XData');
    YData = get(hCross(k), 'YData');
    %     figure(1249); plot(XData, YData, 'r'); grid; hold on;
    XData = unique(XData);
    YData = unique(YData);
    if length(XData) == 1
        Longitude = XData;
    end
    if length(YData) == 1
        Latitude = YData;
    end
end

listeFicRaw = cell(0);
for k=1:length(hPatch)
    XData = get(hPatch(k), 'XData');
    YData = get(hPatch(k), 'YData');
    %     figure(1249); plot(XData, YData); grid; hold on;
    in = inpolygon(Longitude, Latitude, XData, YData);
    if in
        listeFicRaw{end+1} = get(hPatch(k), 'Tag'); %#ok<AGROW>
    end
end
if isempty(listeFicRaw)
    str1 = 'Il n''y a pas de fichier brut intersectant ce point.';
    str2 = 'There is no raw file covering this point.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
listeFicRaw = fliplr(listeFicRaw);
str1 = 'Liste des fichiers bruts intersectant le point d�fini par la croix rouge.';
str2 = 'List of raw files that intersect the red cross.';
[rep, flag] = my_listdlg(Lang(str1,str2), listeFicRaw, 'InitialValue', 1:length(listeFicRaw));
if ~flag
    return
end
listeFicRaw = listeFicRaw(rep);
if isempty(listeFicRaw)
    return
end
nomDir = fileparts(listeFicRaw{1});
[flag, nomFicASCII] = my_uiputfile('*.txt', 'IFREMER - SonarScope : Save names in a file', nomDir);
if ~flag
    return
end
fid = fopen(nomFicASCII, 'w+t');
if fid == -1
     messageErreurFichier(nomFicASCII, 'WriteFailure');
    return
end
for k=1:length(listeFicRaw)
    n = fprintf(fid, '%s\n', listeFicRaw{k}); %#ok<NASGU>
end
fclose(fid);
