function plot_navigation_MarkersSetIntervals

GCF = gcf;
GCA = gca;
h1 = findobj(GCF, 'Tag', 'PointRemarquable');

%% Recherche des points

Ligne = {};
for k=1:length(h1)
    Ligne{end+1} = get(h1(k), 'UserData'); %#ok<AGROW>
end

% Suppression des doublons et tri par ordre chronologique
Ligne = unique(Ligne);
if isempty(Ligne)
    return
end

%% D�codage de l'heure des markers

for k=1:length(Ligne)
    TimeMarkers = datetime(Ligne);
end

%% Recherche des navigations

XLimOrigin = get(GCA, 'XLim');
YLimOrigin = get(GCA, 'YLim');

W = get(GCF, 'Position');
placeFigure('Fig', GCF, 'Where', 'Full')

numprofil = 0;
delete(findobj(GCA, 'Tag', 'Segment'))
LineInterval = {};
h1 = findobj(gcf, 'Type', 'Line', 'Marker', 'none');
h2 = findobj(gcf, 'Type', 'Line', 'Color', [0.8 0.8 0.8],  'Marker', 'none');
hOther = setdiff(h1, h2);
for k=1:length(hOther)
%     Time  = get(hOther(k), 'ZData');
    Time = getappdata(hOther(k), 'Time');
    if isempty(Time)
        continue
    end
    
    XData = get(hOther(k), 'XData');
    YData = get(hOther(k), 'YData');
    
    clear TimeSegm
    TimeSegm(1) = Time(1);
    for k2=1:length(TimeMarkers)
        if ((TimeMarkers(k2)) >= Time(1)) && ((TimeMarkers(k2)) <= Time(end))
            TimeSegm(end+1) = TimeMarkers(k2); %#ok<AGROW>
        end
    end
    TimeSegm(end+1) = Time(end); %#ok<AGROW>
    
    %     if length(TimeSegm) > 2
    for k2=1:(length(TimeSegm)-1)
        sub = find((Time >= TimeSegm(k2)) & (Time <= TimeSegm(k2+1)));
        
        if (length(sub) == 1) || (length(sub) == length(Time))
            continue
        end
        
        hold on;
        x = XData(sub);
        y = YData(sub);
        hSegm = plot(GCA, x, y, 'r');
        set(hSegm, 'LineWidth', 6, 'Color', 'k', 'Marker', '*', 'Tag', 'Segment')
        
        XLim = get(GCA, 'XLim');
        YLim = get(GCA, 'YLim');
        minx = min(x);
        maxx = max(x);
        miny = min(y);
        maxy = max(y);
        margex = (maxx - minx) * 2;
        margey = (maxy - miny) * 2;
        
        if (margex / margey) > 30
            margey = margey * 10;
        end
        if (margey / margex) > 30
            margex = margex * 10;
        end
        
        set(GCA, 'XLim', [minx-margex maxx+margex]);
        set(GCA, 'YLim', [miny-margey maxy+margey]);
        
        str1 = 'Ce segment est-il valide ?';
        str2 = 'Is it a valid segment ?';
        [rep, flag] = my_questdlg(Lang(str1,str2), 'PositionWindow', 'Middle');
        if ~flag
            delete(findobj(GCA, 'Tag', 'Segment'))
            return
        end
        if rep == 1
            numprofil = numprofil + 1;
            % '15/02/2021  17:59:13.712' avant, '2016-03-28 06:54:41.191'  maintenant
            ProfilName = sprintf('Line%03d', numprofil);
            TimeSegm.Format = 'dd/MM/yyyy HH:mm:ss.SSS';
            LineInterval{end+1,1} = sprintf('> %s   %s   %s   %s  %s   %s  %s', ...
                char(TimeSegm(k2)), char(TimeSegm(k2+1)), ...
                ProfilName, ...
                lat2str(YData(sub(1))), lon2str(XData(sub(1))), ...
                lat2str(YData(sub(end))), lon2str(XData(sub(end)))); %#ok<AGROW>
        end
        delete(hSegm);
        set(GCA, 'XLim', XLim, 'YLim', YLim);
    end
end
delete(findobj(GCA, 'Tag', 'Segment'))
set(GCA, 'XLim', XLimOrigin, 'YLim', YLimOrigin);
set(GCF, 'Position', W);

%% Nom du fichier de sauvegarde

nomDirOut = my_tempdir;
hOther = findobj(gcf, 'Type', 'Line');
for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        UserData = get(hOther(1), 'UserData');
        GCL  = UserData{3};
    end
    Label1 = get(GCL, 'Tag');
    if ~isempty(Label1) && ~strcmp(Label1, 'PointRemarquable')
        nomDir = fileparts(Label1);
        if exist(nomDir, 'dir')
            nomDirOut = nomDir;
            break
        end
    end
end

%% Save file

str1 = 'Nom du fichier des intervals :';
str2 = 'Save intervals file as :';
[flag, nomFic] = my_uiputfile('*.txt', Lang(str1,str2), nomDirOut);
if ~flag
    return
end

%% Sauvegarde du fichier

fid = fopen(nomFic, 'w+t');
for k=1:length(LineInterval)
    fprintf(fid, '%s\n', LineInterval{k});
end
fclose(fid);
