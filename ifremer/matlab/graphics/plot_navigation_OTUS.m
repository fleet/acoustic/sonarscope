% Trac� d'un trait de navigation
%
% Syntax
%   plot_navigation_OTUS(nomFic, Fig, Longitude, Latitude, SonarTime, SonarSpeed, CourseVessel, ...)
%
% Input Arguments
%   nomFic     : Nom du fichier
%   Fig       : Numero de la figure de navigation
%   Longitude  :
%   Latitude   :
%   SonarTime  : Temps en unite matlab
%
% Name-Value Pair Arguments
%   CourseVessel  : Cap en deg
%
% Examples
%   plot_navigation_OTUS(nomFic, Fig, Longitude, Latitude, SonarTime, SonarSpeed, CourseVessel)
%
% See also cl_ermapper_ers/plot_nav Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [Fig, hNav, hImage] = plot_navigation_OTUS(nomFic, Longitude, Latitude, SonarTime, Capteur, NomRacine, ext, varargin)

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
[varargin, Depth]     = getPropertyValue(varargin, 'Depth',     []);
[varargin, Height]    = getPropertyValue(varargin, 'Height',    []);
[varargin, Heading]   = getPropertyValue(varargin, 'Heading',   []);
[varargin, nomCam]    = getPropertyValue(varargin, 'nomCam',    []);
[varargin, FileNames] = getPropertyValue(varargin, 'FileNames', []); %#ok<ASGLU>

Longitude = Longitude(:);
Latitude  = Latitude(:);

TagLine = nomFic;
nomFicPosition = fullfile(my_tempdir, ['Position' num2str(Fig) '.dat']);

%% Trac� de la navigation

if isempty(Fig)
    Fig = FigUtils.createSScFigure;
    for k1=1:2
        hAxes(k1) = subplot(1,2,k1); %#ok<AGROW>
    end
    hNav   = hAxes(2);
    hImage = hAxes(1);
    hOTUS = [];
else
    hAxes = findobj(Fig, 'Type', 'axes');
    hNav   = hAxes(1);
    hImage = hAxes(2);
    hOTUS = [];
end

h = findobj(Fig, 'Text', 'Import/Export');
if isempty(h)
    uiSSc = uimenu(Fig, 'Text', 'Import/Export');
    uimenu(uiSSc, 'Text', 'Plot nav in Google Earth (.kmz)', 'Callback', @ExportGoogleEarth);
    uimenu(uiSSc, 'Text', 'Plot nav in GLOBE (.xml)',        'Callback', @ExportNavigation);
    uimenu(uiSSc, 'Text', 'Change directory of images',      'Callback', @ChangeImageDirectory);
end

Ident = rand(1);

h = findobj(Fig, 'Tag', 'hHeure');
if isempty(h)
    uicontrol('Style', 'text', 'Position', [10,10,1000,20], 'Tag', 'hHeure');
    set(Fig, 'ToolBar', 'figure')
end

t = SonarTime;

set(Fig, 'Tag', 'Trace de navigation', 'UserData', Ident)
hc = findobj(hNav, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(hNav, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes, nbCoul) + 1;


% axes(hNav);
hold(hNav, 'on');
if isempty(SonarTime)
    h = plot(hNav, Longitude(:), Latitude(:));
else
    h = plot3(hNav, Longitude(:), Latitude(:), t(:));
end
set(h, 'Marker', '.')
set(h, 'Color', ColorOrder(iCoul,:))
str1 = 'Position des images (tapez "H" pour obtenir la liste des raccourcis clavier)';
str2 = 'Position of images (press "H" to obtain the list of shortcuts)';
title(Lang(str1,str2))

[nomDirImages, nomFicSeul] = fileparts(nomFic);
if isempty(SonarTime)
    UserData = nomFicSeul;
else
    switch class(t)
        case 'datetime'
            str = [char(t(1)) '  ' char(t(end))];
        case 'cl_time'
            my_breakpoint
            str = timeMat2str([t(1), t(end)]);
            str = [str repmat(' ', 2, 2)]';
            t = t.timeMat;
        otherwise
            my_breakpoint
            tBE = datetime(t([1 end]), 'ConvertFrom', 'datenum');
            str = [char(tBE(1)) '  ' char(tBE(2))];
    end
    UserData = [nomFicSeul ' - ' str(:)'];
end
set(h, 'UserData', UserData)

figure(Fig);

LatLim = [min(Latitude)  max(Latitude)];
LonLim = [min(Longitude) max(Longitude)]; % Ligne de changement d'heure pas g�r�e ici
if isempty(SonarTime)
    tLim = datetime.empty();
else
    tLim = [t(1), t(end)];
end
Color = get(h, 'Color');

UserData = {Color; 'On'; h; nomFic; LatLim; LonLim; tLim; nomFicPosition};

if isempty(SonarTime)
    str = sprintf('display(''%s | %s %s | %s %s'')', ...
        nomFic, ...
        lat2str(Latitude(1)), ...
        lon2str(Longitude(1)), ...
        lat2str(Latitude(end)), ...
        lon2str(Longitude(end)));
else
    str = sprintf('display(''%s - %s | %s %s | %s %s'')', ...
        nomFic, str(:), ...
        lat2str(Latitude(1)), ...
        lon2str(Longitude(1)), ...
        lat2str(Latitude(end)), ...
        lon2str(Longitude(end)));
end
set(h, 'ButtonDownFcn', str)

if ~isempty(TagLine)
    set(h, 'Tag', TagLine)
end

axisGeo(hNav);
grid on;
drawnow

%% Definition de la callback associ�e au d�placement de la souris

set(Fig, 'WindowButtonMotionFcn', @dispHeure, 'BusyAction', 'cancel', 'Interruptible', 'Off')

set(Fig, 'KeyPressFcn', @PressKey);

%% Inner fonction d'affichage du point de la navigation le plus proche de la souris

    function ExportGoogleEarth(varargin)
        plot_navigation_ExportGoogleEarth('SCAMPI', 1)
    end

% Export de la navigation en XML
    function ExportNavigation(varargin)
        plot_navigation_ExportXML
    end

    function ChangeImageDirectory(varargin)
        [flag, nomDir] = my_uigetdir(nomDirImages, 'Directory to put PingAcrossDist images');
        if flag
            nomDirImages = nomDir;
            hOTUS = [];
        end
    end

    function dispHeure(varargin)
        global hLines XData YData Datetime sub ordre Dist DistMin tMin% Dans le debuggueur uniquement
        
        % How Callbacks Are Interrupted :
        % MATLAB checks for queued events that can interrupt a callback function only when it encounters
        % a call to drawnow, figure, getframe, or pause in the executing callback function. When executing
        % one of these functions, MATLAB processes all pending events, including executing all waiting callback
        % functions. The interrupted callback then resumes execution
        drawnow
        
        GCBA = get(gcbf, 'CurrentAxes');
        
        if GCBA ~= hNav
            return
        end
        
        currentPoint = get(GCBA, 'currentPoint');
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        if (currentPoint(1,1) < XLim(1)) || (currentPoint(1,1) > XLim(2)) || (currentPoint(1,2) < YLim(1)) || (currentPoint(1,2) > YLim(2))
            return
        end
        hLines = findobj(gcbf, 'Type', 'line', 'LineStyle', '-');
        if isempty(hLines)
            return
        end
        for k=1:length(hLines)
            pppp = get(hLines(k), 'Tag');
            if isempty(pppp)
                subIsNav(k) = false; %#ok<AGROW>
            else
                if strcmp(pppp, 'PositionPing')
                    subIsNav(k) = false; %#ok<AGROW>
                else
                    subIsNav(k) = true; %#ok<AGROW>
                end
            end
        end
        hLines = hLines(subIsNav);
        DistMin = [];
        tMin    = datetime.empty();
        hLinesConserve = [];
        for k=1:length(hLines)
            Datetime = get(hLines(k), 'ZData');
            if isempty(Datetime)
                continue
            end
            UserData = get(hLines(k), 'UserData');
            if isempty(UserData)
                continue
            end
            
            XData = get(hLines(k), 'XData');
            YData = get(hLines(k), 'YData');
            XDataTemp = abs(XData - currentPoint(1,1));
            YDataTemp = abs(YData - currentPoint(1,2));
            Dist = XDataTemp.^2 + YDataTemp.^2;
            [~, ordreDist] = sort(Dist);
            DistMin(end+1) = Dist(ordreDist(1)); %#ok<AGROW>
            T = Datetime(ordreDist(1));
            
            tMin(end+1) = T; %#ok<AGROW>
            hLinesConserve(end+1) = hLines(k); %#ok<AGROW>
        end
        [sub, ordre] = sort(DistMin);
        if isempty(ordre)
            return
        end
        tMin = tMin(ordre(1));
        LonMin = XData(ordreDist(1));
        LatMin = YData(ordreDist(1));
        
        hHeure = findobj(gcbf, 'Tag', 'hHeure');
        UserData = get(hLinesConserve(ordre(1)), 'UserData');
        set(hHeure, 'String', ['Point : ' char(tMin) ' Line : ' UserData])
        set(hHeure, 'HorizontalAlignment', 'left')
        
        if length(hLines) > 1
            if ~strcmp(get(hLines(1), 'Tag'), 'PositionPing')
                set(hLinesConserve, 'LineWidth', 0.5)
                set(hLinesConserve(ordre(1)), 'LineWidth', 3)
            end
        end
        dispImageOTUS(GCBA, tMin, 2, LatMin, LonMin, 'nomCam', nomCam)
    end


    function PressKey(Fig0, varargin)
        CurrentCharacter = get(Fig0, 'CurrentCharacter');
        if isempty(CurrentCharacter)
            return
        end
        
        switch lower(CurrentCharacter)
            case 'h'
                str = {};
                str{end+1} = Lang('m  : Cr�er un marqueur', 'm  : Create a marker');
                str{end+1} = Lang('o  : Export des marqueurs (.txt)', 'o  : Export markers (.txt)');
                str{end+1} = Lang('i  : Import des marqueurs (.txt)', 'i  : Import markers (.txt)');
                str{end+1} = Lang('s  : Cr�ation des images correspondant aux marqueurs (.txt)', 's  : Create images corresponding to the markers (.txt)');
                str{end+1} = '+  : Zoom * 2';
                str{end+1} = '-  : Zoom / 2';
                str{end+1} = Lang('-> : 1/2 d�calage vers la droite', '-> : 1/2 Shift right');
                str{end+1} = Lang('<- : 1/2 d�calage vers la gauche', '<- : 1/2 Shift left');
                str{end+1} = Lang('   : 1/2 d�calage vers le haut',   '   : 1/2 Shift top');
                str{end+1} = Lang('   : 1/2 d�calage vers le bas',    '   : 1/2 Shift down');
                edit_infoPixel(str, [], 'PromptString', 'SonarScope Shortcuts on local image.')
            case 'm'
                PlotMarker(varargin{:})
            case 'o'
                plot_navigation_ExportMarkers_OTUS(Fig0, hNav)
                
            case 's'
                displayImages
                
            case 'i'
                plot_navigation_ImportMarkers_OTUS(hNav)
                
            case '+'
                zoom(hNav, 2)
            case '-'
                zoom(hNav, 1/2)
                
            otherwise
                if (double(CurrentCharacter) == 28)
                    % D�placement vers la gauche
                    XLim  = get(hNav, 'XLim');
                    Range = diff(XLim);
                    set(hNav, 'XLim', XLim - Range/2)
                elseif (double(CurrentCharacter) == 29)
                    % D�placement vers la droite
                    XLim  = get(hNav, 'XLim');
                    Range = diff(XLim);
                    set(hNav, 'XLim', XLim + Range/2)
                    
                elseif (double(CurrentCharacter) == 30)
                    % D�placement vers le haut
                    YLim  = get(hNav, 'YLim');
                    Range = diff(YLim);
                    set(hNav, 'YLim', YLim + Range/2)
                    
                elseif (double(CurrentCharacter) == 31)
                    % D�placement vers le bas
                    YLim  = get(hNav, 'YLim');
                    Range = diff(YLim);
                    set(hNav, 'YLim', YLim - Range/2)
                end
        end
        axis(hNav);
    end

    function PlotMarker(varargin)
        global hLines XData YData Datetime sub ordre Dist DistMin tMin% Dans le debuggueur uniquement
        
        set(Fig, 'WindowButtonMotionFcn',[])
        
        GCBO = gcbo;
        GCBA = get(gcbf, 'CurrentAxes');
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        currentPoint = get(GCBA, 'currentPoint');
        hLines = findobj(gcbf, 'Type', 'line');
        for k=1:length(hLines)
            pppp = get(hLines(k), 'Tag');
            if isempty(pppp)
                subIsNav(k) = false; %#ok<AGROW>
            else
                subIsNav(k) = true; %#ok<AGROW>
            end
        end
        hLines    = hLines(subIsNav);
        DistMin   = [];
        tMin      = datetime.empty();
        XDataLine = [];
        YDataLine = [];
        hLinesConserve    = [];
        DepthConserve     = [];
        HeightConserve    = [];
        HeadingConserve   = [];
        FileNamesConserve = {};
        
        for k=1:length(hLines)
            Datetime = get(hLines(k), 'ZData');
            if isempty(Datetime)
                continue
            end
            UserData = get(hLines(k), 'UserData');
            if isempty(UserData)
                continue
            end
            
            XData = get(hLines(k), 'XData');
            YData = get(hLines(k), 'YData');
            XDataDist = abs(XData-currentPoint(1,1));
            YDataDist = abs(YData-currentPoint(1,2));
            Dist = XDataDist.^2 + YDataDist.^2;
            [~, ordre] = sort(Dist);
            DistMin(end+1) = Dist(ordre(1)); %#ok
            XDataLine(end+1) = XData(ordre(1)); %#ok<AGROW>
            YDataLine(end+1) = YData(ordre(1)); %#ok<AGROW>
            tMin(end+1) = Datetime(ordre(1)); %#ok
            hLinesConserve(end+1) = hLines(k); %#ok
            
            if isempty(Depth)
                DepthConserve(end+1) = NaN; %#ok<AGROW>
            else
                DepthConserve(end+1) = Depth(ordre(1)); %#ok<AGROW>
            end
            if isempty(Height)
                HeightConserve(end+1) = NaN; %#ok<AGROW>
            else
                HeightConserve(end+1) = Height(ordre(1)); %#ok<AGROW>
            end
            if isempty(Heading)
                HeadingConserve(end+1) = NaN; %#ok<AGROW>
            else
                HeadingConserve(end+1) = Heading(ordre(1)); %#ok<AGROW>
            end
            if isempty(FileNames)
                FileNamesConserve{end+1} = ' '; %#ok<AGROW>
            else
                FileNamesConserve{end+1} = FileNames(ordre(1)); %#ok<AGROW>
            end
        end
        [sub, ordre] = sort(DistMin);
        if isempty(ordre)
            return
        end
        tMin = tMin(ordre(1));
        LonMin = XDataLine(ordre(1));
        LatMin = YDataLine(ordre(1));
        
        UserData = get(GCBO, 'UserData');
        
        flagHold = ishold;
        if ~flagHold
            hold on
        end
        hPoint = PlotUtils.createSScPlot(XDataLine(ordre(1)), YDataLine(ordre(1)), 'or');
        UserDataMarker = sprintf('%s', char(tMin));
        set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', UserDataMarker)
        if ~flagHold
            hold off
        end
        set(GCBA, 'XLim', XLim);
        set(GCBA, 'YLim', YLim);
        
        if isempty(Depth)
            setappdata(hPoint, 'Depth', NaN);
        else
            setappdata(hPoint, 'Depth', DepthConserve(ordre(1)))
        end
        setappdata(hPoint, 'Height',    HeightConserve(ordre(1)))
        setappdata(hPoint, 'Heading',   HeadingConserve(ordre(1)))
        setappdata(hPoint, 'FileNames', FileNamesConserve{ordre(1)})
        setappdata(hPoint, 'Latitude',  LatMin)
        setappdata(hPoint, 'Longitude', LonMin)
        setappdata(hPoint, 'Datetime',  tMin)
        
        str1 = 'Commentaire : ';
        str2 = 'Comment : ';
        nomVar = {Lang(str1,str2)};
        value = {''};
        [Comment, flag] = my_inputdlg(nomVar, value);
        if flag
            Comment = Comment{1};
        end
        setappdata(hPoint, 'Comment', Comment)
        
        dispImageOTUS(GCBA, tMin, 1, LatMin, LonMin, 'nomCam', nomCam)
        set(Fig, 'WindowButtonMotionFcn', @dispHeure, 'BusyAction', 'cancel', 'Interruptible', 'Off')
        axis(hNav);
    end

    function PrintLine(str, varargin) %#ok
        display(str)
    end

    function plotImageLocalization(GCBA, LatMin, LonMin)
        if ~isnan(LatMin)
            plotCross(GCBA, LatMin, LonMin, 2)
        end
    end


    function plotCross(GCBA, Lat, Lon, Type)
        hCross = findobj(GCBA, 'Type', 'line', 'Tag', 'PositionPing');
        if isempty(hCross)
            if Type == 1
                XLim = get(GCBA, 'XLim');
                YLim = get(GCBA, 'YLim');
                hold(GCBA, 'on');
                hc = plot(GCBA, [Lon Lon], YLim, 'r');
                set(hc, 'Tag', 'PositionPing')
                hc = plot(GCBA, XLim, [Lat Lat], 'r');
                set(hc, 'Tag', 'PositionPing')
                hold(GCBA, 'off');
            else
                hold(GCBA, 'on')
                hc = plot(GCBA, Lon, Lat, '.');
                set(hc, 'Color', 'r', 'Tag', 'PositionPing', 'MarkerSize', 20)
                hold(GCBA, 'off')
            end
        else
            if Type == 1
                set(hCross(1), 'YData', [Lat Lat])
                set(hCross(2), 'XData', [Lon Lon])
            else
                set(hCross(1), 'XData', Lon, 'YData', Lat)
            end
        end
    end


    function dispImageOTUS(GCBA, tMin, Mode, LatMin, LonMin, varargin)
        
        [varargin, Title]  = getPropertyValue(varargin, 'Title',  []);
        [varargin, nomCam] = getPropertyValue(varargin, 'nomCam', []); %#ok<ASGLU>
        
        pppp = zoom(hNav);
        if strcmpi(get(pppp, 'Enable'), 'on')
            return
        end
        
        pppp = pan(Fig);
        if strcmpi(get(pppp, 'Enable'), 'on')
            return
        end
        
        nomDir = nomDirImages;
%         t = cl_time('timeMat', tMin);
        [nomFic, CLim, nomFicJgw] = findNearestFile(nomDir, tMin, ...
            Capteur, NomRacine, ext, nomCam);
        
        if exist(nomFic, 'file')
            [~, nomFicSeul] = fileparts(nomFic);
            I = imread(nomFic);
            %             if isempty(FigOTUS) || ~ishandle(FigOTUS)
            %                 FigOTUS = figure;
            %                 warning('Off')
            %                 axes(hImage)
            %                 if isempty(CLim)
            if Mode == 1
                if size(I,3) == 3
                    figure;
                    hImageFigure = imshow(I);
                else
                    figure;
                    hImageFigure = imagesc(I, CLim); axis square; colormap(gray(256)); colorbar;
                end
                if isempty(Title)
                    Title = nomFicSeul;
                else
                    Title = [nomFicSeul ' : ' Title];
                end
                title(Title, 'Interpreter', 'none');
                specialJGW(nomFicJgw, I, gca, hImageFigure)
            else
                if isempty(hImage) || ~ishandle(hImage) || isempty(hOTUS) || ~ishandle(hOTUS) || isempty(get(hOTUS, 'CData'))
                    axes(hImage)
                    if size(I,3) == 3
                        hOTUS = imshow(I);
                    else
                        hOTUS = imagesc(I, CLim); axis equal; axis tight; colormap(gray(256)); %% colorbar('SouthOutside');
                    end
                else
                    set(hOTUS, 'CData', I);
                    set(hImage, 'CLim', CLim)
                end
                specialJGW(nomFicJgw, I, hImage, hOTUS)
                title(hImage, nomFicSeul, 'Interpreter', 'none');
                plotImageLocalization(GCBA, LatMin, LonMin)
            end
            %             axes(hImage)
            axes(hNav);
        end
    end

    function displayImages(varargin)
        h1 = findobj(Fig, 'Tag', 'PointRemarquable');
        
        %% Cadre de recherche
        
        XLim = get(hNav, 'XLim');
        YLim = get(hNav, 'YLim');
        
        str1 = 'Recherche des points dans :';
        str2 = 'Save points from :';
        str3 = 'Graphique entier';
        str4 = 'Whole frame';
        str5 = 'Zoom actuel';
        str6 = 'Current extent';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            return
        end
        if rep == 1
            XLim = [-Inf Inf];
            YLim = [-Inf Inf];
        end
        
        %% Recherche des points
        
        for k=1:length(h1)
            XData = get(h1(k), 'XData');
            YData = get(h1(k), 'YData');
            if any((XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2)))
                UserData = get(h1(k), 'UserData');
                
                DepthImage     = getappdata(h1(k), 'Depth');
                HeightImage    = getappdata(h1(k), 'Height');
                HeadingImage   = getappdata(h1(k), 'Heading');
                %                 FileNamesImage = getappdata(h1(k), 'FileNames');
                CommentImage   = getappdata(h1(k), 'Comment');
                LatitudeImage  = getappdata(h1(k), 'Latitude');
                LongitudeImage = getappdata(h1(k), 'Longitude');
                tMatImage      = getappdata(h1(k), 'Datetime');
                
                if iscell(CommentImage)
                    CommentImage = '';
                end
                Title = sprintf('Lon=%s Lat=%s Z=%f H=%f Az=%f %s', num2strPrecis(LongitudeImage), num2strPrecis(LatitudeImage), ...
                    DepthImage, HeightImage, HeadingImage, CommentImage);
                dispImageOTUS(hNav, tMatImage, 1, LatitudeImage, LongitudeImage, 'Title', Title, 'nomCam', nomCam)
            end
        end
    end
end


function specialJGW(nomFicJgw, I, hImage, hOTUS)
if ~isempty(nomFicJgw)
    [ny, nx, nc] = size(I); %#ok<ASGLU> % Laisser ~car pb si image RGB
    refmat = worldfileread(nomFicJgw); % http://webhelp.esri.com/arcgisdesktop/9.2/index.cfm?TopicName=World_files_for_raster_datasets
    
    TypeRepresentation = 2;
    if TypeRepresentation == 1
        x = linspace(refmat(3,1), refmat(3,1) + nx * refmat(2,1), nx);
        y = linspace(refmat(3,2), refmat(3,2) + ny * refmat(1,2), ny);
        set(hOTUS, 'CData', I, 'XData', x, 'YData', y)
        axis(hImage, 'tight');
        axisGeo(hImage);
    else
        x = 1:nx;
        if refmat(1,2) > 1
            y = 1:ny;
        else
            y = ny:-1:1;
        end
        set(hOTUS, 'CData', I, 'XData', x, 'YData', y)
        axis(hImage, 'equal', 'tight');
        set(hImage, 'YDir', 'Normal')
    end
end
end


function [nomFic, CLim, nomFicJgw] = findNearestFile(nomDir, Datetime, Capteur, NomRacine, ext, nomCam, varargin)

nomFicSeul = setNomFicImage(NomRacine, Capteur, Datetime, ext, nomCam);

nomFic    = fullfile(nomDir, nomFicSeul);
CLim      = [90 180];
nomFicJgw = [];

if exist(nomFic, 'file')
    X = [nomFic(1:end-4) '.jgw'];
    if exist(X, 'file') % Astuce plus ou moins discutable pour savoir si l'image est en g�om�trie LatLong (cas des mosa�ques individuelles)
        CLim = [0 255];
        nomFicJgw = X;
    end
    return
else
    nomFic = [];
    %{
    [Annee, Mois, Jour]  = ymd(Datetime);
    [Heure, Minute, Sec] = hms(Datetime);
    Milli = floor((Sec - floor(Sec) ) * 1000);
    Sec = floor(Sec);
    %}
    for kMilli=[0 -0.5 0.5 -1 1 -1.5 1.5 -2 2 -2.5 2.5 -3 3 -3.5 3.5 -4 4 -4.5 4.5 -5 5 -5.5 5.5]
        Newdatetime = Datetime + duration(0, 0, 0, kMilli);
        nomFicSeul = setNomFicImage(NomRacine, Capteur, Newdatetime, ext, nomCam);
        
        nomFic = fullfile(nomDir, nomFicSeul);
        if exist(nomFic, 'file')
            CLim = [90 180];
            %             fprintf('%d %d %d %d %03.1f %d %d %d %d %s\n', Heure, Minute, Sec, Milli, kMilli, He, Mi, Se, Mil, nomFicSeul)
            
            X = [nomFic(1:end-4) '.jgw'];
            if exist(X, 'file') % Astuce plus ou moins discutable pour savoir si l'image est en g�om�trie LatLong (cas des mosa�ques individuelles)
                CLim = [0 255];
                nomFicJgw = X; % cl_image.indGeometryType('LatLong')
            end
            return
        end
    end
end

    function nomFicSeul = setNomFicImage(NomRacine, Capteur, Datetime, ext, nomCam)

        %{
        [Annee, Mois, Jour]  = ymd(Datetime);
        [Heure, Minute, Sec] = hms(Datetime);
        Milli = floor((Sec - floor(Sec) ) * 1000);
        Sec = floor(Sec);
        
        switch Capteur
            case 'OTUS' % COR_21-01-2014_15-23-46-921.tif
                nomFicSeul = sprintf('%s%d-%02d-%04d_%02d-%02d-%02d-%03d%s', NomRacine, Jour, Mois, Annee, Heure, Minute, Sec, Milli, ext);
            case 'SCAMPI'
                nomFicSeul = sprintf('%s%04d%02d%02d-%02d%02d%02d-%03d%s', NomRacine, Annee, Mois, Jour, Heure, Minute, Sec, Milli, ext);
            case 'SCAMPISansMilliseconds'
                nomFicSeul = sprintf('%s%04d%02d%02d-%02d%02d%02d%s', NomRacine, Annee, Mois, Jour, Heure, Minute, Sec, ext);
            case 'PAGURE' % CGFS_X0315_20190917172346_CAM3.JPG
                nomFicSeul = sprintf('%s_%04d%02d%02d%02d%02d%02d_%s%s', NomRacine, Annee, Mois, Jour, Heure, Minute, Sec, nomCam, ext);
                % CGFS_X0315_20190917172346_CAM3.JPG
            case 'HROV' % 170430081713016_01_1080i.jpg
                nomFicSeul = sprintf('%02d%02d%02d%02d%02d%02d%03d_%s', Annee-2000, Mois, Jour, Heure, Minute, Sec, Milli, ['01_1080i' ext]);
        end
        %}
        
        switch Capteur
            case 'OTUS' % 'COR_21-01-2014_06-20-13-233.tif'
                Datetime.Format = 'dd-MM-yyyy_HH-mm-ss-SSS';
                nomFicSeul = sprintf('%s%s%s', NomRacine, char(Datetime), ext);
            case 'SCAMPI' % 'COR_20140121-062013-233.tif'
                Datetime.Format = 'yyyyMMdd-HHmmss-SSS';
                nomFicSeul = sprintf('%s%s%s', NomRacine, char(Datetime), ext);
            case 'SCAMPISansMilliseconds' % 'COR_20140121-062013.tif'
                Datetime.Format = 'yyyyMMdd-HHmmss';
                nomFicSeul = sprintf('%s%s%s', NomRacine, char(Datetime), ext);
            case 'PAGURE' % COR__20140121062013_OTUS.tif'    CGFS_X0315_20190917172346_CAM3.JPG
                Datetime.Format = 'yyyyMMddHHmmss';
                nomFicSeul = sprintf('%s_%s_%s%s', NomRacine, char(Datetime), nomCam, ext);
            case 'HROV' % '140121062013233_01_1080i.tif'
                Datetime.Format = 'yyMMddHHmmssSSS';
                nomFicSeul = sprintf('%s_01_1080i%s', char(Datetime), ext);
        end
    end
end

