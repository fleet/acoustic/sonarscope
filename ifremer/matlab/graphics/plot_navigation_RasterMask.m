function Comment = plot_navigation_RasterMask(varargin)

[varargin, Fig]         = getPropertyValue(varargin, 'Fig',         []);
[varargin, SurveyName]  = getPropertyValue(varargin, 'SurveyName',  'Unkown');
[varargin, SounderName] = getPropertyValue(varargin, 'SounderName', 'Unkown');
[varargin, SounderCie]  = getPropertyValue(varargin, 'SounderCie',  'EM');
[varargin, ShipName]    = getPropertyValue(varargin, 'ShipName',    'Unkown');
[varargin, nomFicKml]   = getPropertyValue(varargin, 'nomFicKml',   []); %#ok<ASGLU>

Comment = [];

%% If no name is given

if isempty(nomFicKml)
    nomFicKml = fullfile(my_tempdir, 'Test.kml');
end

[nomDir, nomFic] = fileparts(nomFicKml);
nomDir           = fileparts(nomDir);
nomFicShpBBox    = fullfile(nomDir, 'SHP', [SurveyName '-BoundingBox.shp']);
nomFicShp        = fullfile(nomDir, 'SHP', [SurveyName '-Coverage.shp']);

%% 

LatPatchSwath = [];
LonPatchSwath = [];

if isempty(Fig)
    Fig = gcf;
end

h1 = findobj(Fig, 'Type', 'Line', 'Marker', 'none', '-and', '-not', 'tag', 'PositionPing');
h2 = findobj(Fig, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
h3 = findobj(Fig, 'Type', 'patch');

if isempty(h3)
    str1 = 'TODO';
    str2 = 'This task can be executed only with a plot of navigation and coverage.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

hLine_swath = setdiff(h1, h2);
if isempty(hLine_swath)
    return
end

l1 = {hLine_swath.Tag};
l3 = {h3.Tag};
[~, ia, ib] = intersect(l1, l3);
if isempty(ia)
    return
end

hLine_swath  = hLine_swath(ia);
hPatch_swath = h3(ib);

if isempty(hLine_swath)
    return
end

minTime =  Inf;
maxTime = -Inf;
minLat  =  Inf;
maxLat  = -Inf;
minLon  =  Inf;
maxLon  = -Inf;
for k=1:length(hPatch_swath)
	LonPatchSwath{k} = get(hPatch_swath(k), 'XData'); %#ok<AGROW>
    LatPatchSwath{k} = get(hPatch_swath(k), 'YData'); %#ok<AGROW>
    
    minLat = min(minLat, min(LatPatchSwath{k}));
    maxLat = max(maxLat, max(LatPatchSwath{k}));
    minLon = min(minLon, min(LonPatchSwath{k}));
    maxLon = max(maxLon, max(LonPatchSwath{k}));
    
	Time = get(hLine_swath(k), 'ZData');
    minTime = min(minTime, min(Time));
    maxTime = max(maxTime, max(Time));
end
rangeLat = maxLat - minLat;
rangeLon = maxLon - minLon;
LatMilieu = (minLat + maxLat) / 2;
coeff = rangeLat / (rangeLon * cosd(LatMilieu));

BoundingBox.LatLim  = [minLat maxLat];
BoundingBox.LonLim  = [minLon maxLon];

nbPixMin = 5000; % 5 x ce qu'il y avait avant
if coeff > 1
%     nLon = floor(1000 / coeff);
%     nLat = 1000;
    nLon = nbPixMin;
    nLat = floor(nbPixMin * coeff);
else
%     nLon = 1000;
%     nLat = floor(1000 * coeff);
    nLon = floor(nbPixMin / coeff);
    nLat = nbPixMin;
end

%% Create the mask

x = linspace(minLon, maxLon, nLon);
y = linspace(minLat, maxLat, nLat);
coefLon = nLon / (maxLon - minLon);
coefLat = nLat / (maxLat - minLat);
for k=1:length(hPatch_swath)
    x2 = (LonPatchSwath{k}-minLon)*coefLon;
    y2 = (LatPatchSwath{k}-minLat)*coefLat;
%     figure(123767); plot(x2, y2); hold on; grid on
    
    BW = poly2mask(x2, y2, nLat, nLon);
    if k == 1
        Mask = BW;
    else
        Mask = Mask | BW;
    end
end
% figure; imagesc(x, y, Mask); colorbar; axis xy; axisGeo

%% Dilate the mask

SE = strel('rectangle', [11 11]);
% SE = strel('rectangle', [55 55]); % 5 x ce qu'il y avait avant
Mask = imdilate(Mask, SE);
Mask = imerode(Mask, SE);
% Superficie = sum(Mask(:)) * (360^2) /(coefLon * coefLat); % TODO : continuer : Rayon de la terre

%% Get contours

BW = bwperim(Mask, 8);
[B,~,N] = bwboundaries(BW);
B = B((N+1):end);
clear RegionOfInterest

% figure; 
N = length(B);
if N == 0
    str1 = 'La cr�ation du masque binaire a �chou�. La zone g�ographique des fichiers est peut-�tre trop �tendue.';
    str2 = 'Binary mask failed. May be the geographic locations of the files are too much different.';
    my_warndlg(Lang(str1,str2), 0);
    return
end

AreaKm2 = NaN(1,N);
for k=1:N
    %     coul = coul2mat(color(1+mod(k-1, length(color))));
    boundary = B{k}';

    RegionOfInterest(k).pX    = x(boundary(2,:)); %#ok<AGROW>
    RegionOfInterest(k).pY    = y(boundary(1,:)); %#ok<AGROW>
    RegionOfInterest(k).code  = k; %#ok<AGROW>
    RegionOfInterest(k).Num   = k; %#ok<AGROW>
    RegionOfInterest(k).Color = [1 1 1]; %#ok<AGROW>
    RegionOfInterest(k).Name  = nomFic; %#ok<AGROW>
    
    E = referenceEllipsoid('wgs84', 'm');
    area = areaint(y(boundary(1,:)), x(boundary(2,:)), E);
%     strArea = num2strMoney(floor(area));
    strAream2  = num2str(floor(area));
    AreaKm2(k) = area * 1e-6;
    strAreakm2 = num2str(AreaKm2(k));
    
    description = sprintf('Survey : %s\nShip : %s\nSounder : %s %s\nTime : \n%s\n%s\nSurface area : %s m2 (%s km2)', ...
    SurveyName, ...
    ShipName, ...
    SounderCie, ...
    num2str(SounderName), ...
    datetime(minTime, 'ConvertFrom', 'datenum'), ...
    datetime(maxTime, 'ConvertFrom', 'datenum'), ...
    strAream2, strAreakm2);

    RegionOfInterest(k).description = description; %#ok<AGROW>
    
%     plot(RegionOfInterest(k).pX, RegionOfInterest(k).pY, '*'); grid on; hold on; axisGeo
end
AreaKm2 = sum(AreaKm2, 'omitnan');
Comment = sprintf('Surface area : %s km2', num2str(round(AreaKm2, 3, 'significant')));

%% Exports in .kmz and .shp

flag = ROI_export_kml(RegionOfInterest, nomFicKml); %#ok<NASGU>
try
    flag = ROI_export_shp(RegionOfInterest, nomFicShp); %#ok<NASGU>
catch ME
    ME.getReport
    my_warndlg(ME.message, 1);
end

%% Export de la BoundingBox en .shp

BB.pX    = [BoundingBox.LonLim(1) BoundingBox.LonLim(2) BoundingBox.LonLim(2) BoundingBox.LonLim(1)  BoundingBox.LonLim(1)];
BB.pY    = [BoundingBox.LatLim(1) BoundingBox.LatLim(1) BoundingBox.LatLim(2) BoundingBox.LatLim(2)  BoundingBox.LatLim(1)];
BB.code  = 1;
BB.Num   = 1;
BB.Color = [1 1 1];
BB.Name  = RegionOfInterest(1).Name;
str = RegionOfInterest(1).description;
k = strfind(str, 'Surface area :');
BB.description = sprintf('%s %s km2', str(1:k+13), num2str(AreaKm2));
flag = ROI_export_shp(BB, nomFicShpBBox); %#ok<NASGU>

%% The End

str1 = 'Cr�ation mask binaire r�ussi';
str2 = 'Binary mask complete';
my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 30);
