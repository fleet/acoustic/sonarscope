% Callback associ�e � "Save Lines" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_SaveLines
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_SaveLines

persistent persistent_nomDir

listeFic = {};

GCF = gcf;
hLineAll = findobj(GCF, 'Type', 'Line', 'Marker', 'none');
hLineHiddenYes = findobj(GCF, 'Type', 'Line', 'Marker', 'none', 'Color', [0.8 0.8 0.8]);
hLineHiddenNo  = setdiff(hLineAll, hLineHiddenYes);

if isempty(hLineAll) && isempty(hLineHiddenYes)
    hLineAll = findobj(GCF, 'Type', 'Scatter');
    hLineHiddenYes = findobj(GCF, 'Type', 'Scatter', 'SizeData', 1);
    hLineHiddenNo  = setdiff(hLineAll, hLineHiddenYes);
end

if isempty(hLineHiddenNo)
    msg = 'No lines selected. If you have added markers to your lines, set  them to "none" and try again.';
    my_warndlg(msg, 1);
    return
end

for k=1:length(hLineHiddenNo)
    Tag = get(hLineHiddenNo(k), 'Tag');
    if ~isempty(Tag) && ~strcmp(Tag, 'PositionPing') && ~strcmp(Tag, 'PointRemarquable')
        listeFic{end+1} = Tag; %#ok
    end
end

if isempty(persistent_nomDir)
    nomDir = my_tempdir;
else
    nomDir = persistent_nomDir;
end

[flag, nomFic] = my_uiputfile( ...
    {'*.txt', 'List file (*.txt)'; ...
    '*.*',                   'All Files (*.*)'}, ...
    'Save as', nomDir);
if ~flag
    return
end
persistent_nomDir = fileparts(nomFic);

fid = fopen(nomFic, 'wt');
for k=1:length(listeFic)
    fprintf(fid, '%s\n', windows_replaceVolumeLetter(listeFic{k}));
end
fclose(fid);
