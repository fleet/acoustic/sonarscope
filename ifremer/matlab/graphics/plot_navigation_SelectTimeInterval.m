function plot_navigation_SelectTimeInterval

% hOther = findobj(gcf, 'Label', 'Hide line');
hOther = findobj(gcf, 'Text', 'Hide line'); % Modif JMA le 20/05/2021

N = length(hOther);
for k=N:-1:1
    UserData = get(hOther(k), 'UserData');
    T(k,:) = UserData{7};
end
[~, ordre] = sort(T(:,1));
hOther = hOther(ordre);

kDeb = 1;
kFin = N;
hOther = maj(hOther, kDeb, kFin, N);
        
p    = ClParametre('Name', 'Begin', 'MinValue', 1, 'MaxValue', N, 'Value', kDeb, 'Format', '%d');
p(2) = ClParametre('Name', 'End',   'MinValue', 1, 'MaxValue', N, 'Value', kFin, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', 'File index');
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
addlistener(a, 'OneParamValueChange', @handleValueChange);
% addlistener(a, 'OkPressedEvent',      @closePlotCallback);
% addlistener(a, 'CancelPressedEvent',  @closePlotCallback);
a.openDialog;

    function handleValueChange(this, src, eventData) %#ok<INUSD>
        paramsValue = a.getParamsValue;
        kDeb = paramsValue(1);
        kFin = paramsValue(2);
        if kDeb > kFin
            kFin = kDeb;
        end
        hOther = maj(hOther, kDeb, kFin, N);
    end

%     function closePlotCallback(this, src, eventData) %#ok<INUSD>
%         date;
%     end

%{

kDeb = 1;
kFin = N;
while 1
    p    = ClParametre('Name', 'Begin', 'MinValue', 1, 'MaxValue', N, 'Value', kDeb, 'Format', '%d');
    p(2) = ClParametre('Name', 'End',   'MinValue', 1, 'MaxValue', N, 'Value', kFin, 'Format', '%d');
    a = StyledParametreDialog('params', p, 'Title', 'File index');
    a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    paramsValue = a.getParamsValue;
    kDeb = paramsValue(1);
    kFin = paramsValue(2);
    
    hOther = maj(hOther, kDeb, kFin, N);
end
%}
end


function hOther = maj(hOther, kDeb, kFin, N)

for k=[1:(kDeb-1) (kFin+1):N]
    GCBO = hOther(k);
    GCA = gca;
    UserData = get(GCBO, 'UserData');
    h = UserData{3};
    set(h, 'Color', [0.8 0.8 0.8])
    UserData{2} = 'Off';
    set(GCBO, 'UserData', UserData)
    
    nomFic = UserData{4};
    hOther2 = findobj(GCA, 'Tag', nomFic);
    hOther2(hOther2 == h) = [];
    set(hOther2, 'Visible', 'Off')
end

for k=kDeb:kFin
    GCBO = hOther(k);
    GCA = gca;
    UserData = get(GCBO, 'UserData');
    h = UserData{3};
    Color = UserData{1};
    set(h, 'Color', Color)
    UserData{2} = 'On';
    set(GCBO, 'UserData', UserData)
    
    nomFic = UserData{4};
    hOther2 = findobj(GCA, 'Tag', nomFic);
    hOther2(hOther2 == h) = [];
    set(hOther2, 'Visible', 'On')
end
end
