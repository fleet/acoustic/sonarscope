% Callback associ�e � "Awake All Lines" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_HideAllLines
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_ShapeFile(varargin)

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
[varargin, nomFicShp] = getPropertyValue(varargin, 'nomFicShp', []);
[varargin, geometry]  = getPropertyValue(varargin, 'geometry',  []); %#ok<ASGLU>

LatPatchSwath   = [];
LonPatchSwath   = [];
LabelPatchOther = [];
FaceColor       = [];
LatPatchOther   = [];
LonPatchOther   = [];

if isempty(Fig)
    Fig = gcf;
end

h1 = findobj(Fig, 'Type', 'Line', 'Marker', 'none', '-and', '-not', 'tag', 'PositionPing');
h2 = findobj(Fig, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
h3 = findobj(Fig, 'Type', 'patch');

[hOther, sub] = setdiff(h1, h2);

if isempty(h3)
    hPatch_other = [];
    hPatch_swath = [];
else
    hPatch_other = setxor(h3, h3(sub));
    hPatch_swath = h3(sub);
end

if isempty(hOther)
    return
end

for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
%         Tag = get(hOther(k), 'Tag');
        UserData = get(hOther(k), 'UserData');
        GCL  = UserData{3};
    end
    
    Label1 = get(GCL, 'Tag');
    [nomDir, Label1, ext] = fileparts(Label1); 
    
    Label2    = get(GCL, 'UserData');
%     Color{k}  = get(GCL, 'Color'); %#ok<AGROW>
    Lon{k}    = get(GCL, 'XData'); %#ok<AGROW>
    Lat{k}    = get(GCL, 'YData'); %#ok<AGROW>
    t{k}      = get(GCL, 'ZData'); %#ok<AGROW>
    Signal{k} = double(getappdata(GCL, 'Signal')'); %#ok<AGROW>
    
    Lon{k} = Lon{k}(~isnan(Lon{k})); %#ok<AGROW>
    Lat{k} = Lat{k}(~isnan(Lat{k})); %#ok<AGROW>    
    
    if ~isempty(hPatch_swath)
        LonPatchSwath{k} = get(hPatch_swath(k), 'XData'); %#ok<AGROW>
        LatPatchSwath{k} = get(hPatch_swath(k), 'YData'); %#ok<AGROW>
    end

    Name{k} = [Label1 ext]; %#ok<AGROW>
    if contains(Label2,  Label1)
        Label2 = Label2(length(Label1)+3:end);
    end
    Label{k} = Label2; %#ok<AGROW>
end

if ~isempty(hPatch_other)
    for k=1:length(hPatch_other)
        LonPatchOther{k} = get(hPatch_other(k), 'XData'); %#ok<AGROW>
        LatPatchOther{k} = get(hPatch_other(k), 'YData'); %#ok<AGROW>
        FaceColor{k}     = get(hPatch_other(k), 'FaceColor'); %#ok<AGROW>
        LabelPatchOther{k} = []; %#ok<AGROW>
        
        UIContextMenu = get(hPatch_other(k), 'UIContextMenu');
        if ~isempty(UIContextMenu)
            Children = get(UIContextMenu, 'Children');
            if ~isempty(Children)
%                 LabelPatchOther{k} = get(Children, 'Label'); %#ok<AGROW>
                LabelPatchOther{k} = get(Children, 'Text'); %#ok<AGROW> % Modif JMA le 20/05/2021
            end
        end
    end
end

if all(isempty(Signal{k})) % 1 fichier contient plusieurs profils
    if isempty(nomFicShp)
        str1 = 'Export de la navigation au format ESRI Shape file, donnez le nom du fichier :';
        str2 = 'Navigation export to ESRI Shapefile, input a file name :';
        [flag, nomFicShp] = my_uiputfile('*.shp', Lang(str1,str2), nomDir);
        if ~flag
            return
        end
    end
    if isempty(geometry)
        % Question : points ou lignes
        Tmp{1} = 'Point';
        Tmp{2} = 'Line';
        [geometry, flag] = my_questdlg('Which type of geometry ?', Tmp{1}, Tmp{2}, 'Init', 2, 'Entete', 'ShapeFile geometry');
        if ~flag
            return
        end
    end
    if geometry == 1
        exportNavShapeFile(Lat, Lon, Name, Label, nomFicShp, 'Point', t);
    else
        exportNavShapeFile(Lat, Lon, Name, Label, nomFicShp, 'Line', t);
    end
    exportNavShapeFile(LatPatchSwath, LonPatchSwath, Name, Label, nomFicShp, 'Polygon');
else
    str1 = 'Export de la navigation au format ESRI Shapefile, donnez le nom du r�pertoire :';
    str2 = 'Navigation to ESRI Shapefile, output directory :';    
    nomDir = uigetdir(nomDir, Lang(str1,str2));
    if isequal(nomDir, 0)
        return
    end
    str1 = 'Export shapeFile...';
    str2 = 'Exporting shapefile...';
    hWait = waitbar(0,Lang(str1,str2));
    set(findall(hWait,'type','text'),'Interpreter','none');
    nFile = length(Lon);
    for k=1:nFile % en point on est oblig� de faire une fichier par profil
        nomFicShp = [strrep(Name{k},'.','_'), '.shp'];
        nomFicShpfull = fullfile(nomDir, nomFicShp);
        waitbar(k/nFile, hWait, sprintf('Exporting shapefile to %s\n%s...', nomDir, nomFicShp));
        exportNavShapeFile(...
            mat2cels(Lat{k}), ...
            mat2cels(Lon{k}), ...
            Name{k}, ...
            Label{k}, ...
            nomFicShpfull, ...
            'Point', ...            
            mat2cels(t{k}),...
            mat2cels(Signal{k}));
    end
    close(hWait)
end
str1 = 'Export ArcGis r�ussi';
str2 = 'ArcGis export complete';
my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 30);


function exportNavShapeFile(Lat, Lon, Name, Label, nomFicShp, featureType, varargin)

if isempty(Lat)
    return
end
if nargin > 7    
    t = varargin{1};
end
if nargin > 8
    Signal = varargin{2};
end
S = [];
for k=1:length(Lat)%:-1:1          
    switch featureType
        case 'Point' % TODO : �a bugue si t est vide
            % sous-echantillonnage toute les 10s
            dsec = diff(t{k})*(24*3600);
            dsec_10 = mod(cumsum(dsec),10);
            iOk = [(floor(dsec_10) == 0), true];
            iOk(1) = true;
            
            nPnt  = sum(iOk);
            lonc  = mat2cels(Lon{k}(iOk));
            latc  = mat2cels(Lat{k}(iOk));
            [S(end+1:end+nPnt).Lon]   = deal(lonc{:});
            [S(end-nPnt+1:end).Lat]   = deal(latc{:});            
            
            if ~isempty(t{k})
                heure = cellstr(datestr(t{k}(iOk), 'HH:MM:SS.FFF'));
                date  = cellstr(datestr(t{k}(iOk), 'dd/mm/yyyy'));
                [S(end-nPnt+1:end).date]  = deal(date{:});
                [S(end-nPnt+1:end).heure] = deal(heure{:});
            end
            
            [S(end-nPnt+1:end).name]  = deal(Name{k});
            if exist('Signal', 'var')
                [S(end-nPnt+1:end).Signal] = deal(Signal{:});
            end
            
        otherwise
            S(k).Lon         = Lon{k}; %#ok<AGROW>
            S(k).Lat         = Lat{k}; %#ok<AGROW>
            S(k).BoundingBox = [min(Lon{k}), min(Lat{k}); max(Lon{k}), max(Lat{k})]; %#ok<AGROW>
            S(k).Name        = Name{k}; %#ok<AGROW>
            S(k).Label       = Label{k}; %#ok<AGROW>
    end        
end
[S.Geometry] = deal(featureType);    

[path, filename, ext]  = fileparts(nomFicShp);
nomFicShp = fullfile(path, [filename ,'_', featureType, ext]);
shapewrite(S, nomFicShp);

% Ecriture du fichier PRJ, en WKT, wgs84 non projet�
[path, FileName] = fileparts(nomFicShp);
projStr = 'GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]]';
fid = fopen( fullfile( path, [FileName '.prj'] ), 'wt' );
fprintf(fid, '%s', projStr);
fclose(fid);

function y = mat2cels(x)
y = mat2cell(x,1, ones(size(x)));
