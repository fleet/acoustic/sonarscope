function plot_navigation_ShowLinesHeading

persistent persistent_HMean persistent_Margin persistent_TwoWay

if isempty(persistent_HMean)
    HMean  = 0;
    Margin = 10;
    TwoWay = 1;
else
    HMean  = persistent_HMean;
    Margin = persistent_Margin;
    TwoWay = persistent_TwoWay;
end

p    = ClParametre('Name', 'Heading',  'MinValue', 0, 'MaxValue', 360, 'Value', HMean,  'Format', '%d');
p(2) = ClParametre('Name', 'Margin',   'MinValue', 1, 'MaxValue', 30,  'Value', Margin, 'Format', '%d');
p(3) = ClParametre('Name', 'Way back', 'MinValue', 0, 'MaxValue',  1,  'Value', TwoWay, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', 'Filter Lines according to heading values');
a.sizes = [0 -2 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
paramsValue = a.getParamsValue;
HMean  = paramsValue(1);
Margin = paramsValue(2);
TwoWay = paramsValue(3);
HLim   = [HMean-Margin HMean+Margin];

persistent_HMean  = HMean;
persistent_Margin = Margin;
persistent_TwoWay = TwoWay;

GCF = gcf;
GCA = gca;
h1 = findobj(GCF, 'Type', 'Line', '-not', 'Tag', 'PositionPing', '-not', 'Tag', 'PointRemarquable');
h2 = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hCroix = findobj(GCF, 'Type', 'Line', 'Tag', 'PositionPing');

% [hOther, ia] = setdiff(h2(:), hCroix(:));
[hOther, ia] = intersect([h1(:); hCroix(:)], h2(:));
if isempty(hOther)
    return
end

% hOtherLine = findobj(GCF, 'Label', 'Hide line');
hOtherLine = findobj(GCF, 'Text', 'Hide line'); % Modif JMA le 20/05/2021
hOtherLine = hOtherLine(ia);

for k=1:length(hOther)
    Type = get(hOther(k), 'Type');
    UserData = get(hOtherLine(k), 'UserData');
    if strcmp(Type, 'line')
        GCL = hOther(k);
    else
        GCL  = UserData{3};
    end
    
    Lon = get(GCL, 'XData');
    Lat = get(GCL, 'YData');
    
    sub0 = find((Lon == 0) | (Lat == 0));
    if ~isempty(sub0)
        Lon(sub0) = NaN;
        Lat(sub0) = NaN;
    end
    
    subNonNaN = ~isnan(Lon) & ~isnan(Lat);
    Lon = Lon(subNonNaN);
    Lat = Lat(subNonNaN);
    
    [Heading, speed, distance] = calCapFromLatLon(Lat, Lon);
    [typeLine, ~, MeanHeading] = classifyProfileFromHeading(Heading);
    
    switch typeLine
        case 'Line'
            if isHeadingCompatible(MeanHeading, HLim, logical(TwoWay))
                GCBO = hOtherLine(k);
                UserData = get(GCBO, 'UserData');
                h = UserData{3};
                Color = UserData{1};
                set(h, 'Color', Color)
                UserData{2} = 'On';
                set(GCBO, 'UserData', UserData)
                
                nomFic = UserData{4};
                hOther2 = findobj(GCA, 'Tag', nomFic);
                hOther2(hOther2 == h) = [];
                set(hOther2, 'Visible', 'On')
    
%     
%     
%                 set(GCL, 'Color', [0.8 0.8 0.8])
%                 UserData{2} = 'Off';
%                 set(hOtherLine(k), 'UserData', UserData)
%                 
%                 nomFic = UserData{4};
%                 h = UserData{3};
%                 hOther2 = findobj(GCA, 'Tag', nomFic);
%                 hOther2(hOther2 == h) = [];
%                 set(hOther2, 'Visible', 'Off')
            end
    end
end


function flag = isHeadingCompatible(MeanHeading, HLim, twoDirections)

flag = (MeanHeading >= HLim(1)) && (MeanHeading <= HLim(2));
if flag 
    return
end

if twoDirections
    MeanHeading = mod((MeanHeading + 180), 360);
    flag = (MeanHeading >= HLim(1)) && (MeanHeading <= HLim(2));
    if flag
        return
    end
end

