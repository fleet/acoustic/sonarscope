% Callback associ�e � "Awake Lines In Box" des m�thodes de trac� de navigation des
% classes cl_simrad_all, cl_reson_s7k, ...
%
% Syntax
%   plot_navigation_ShowLinesInBox
%
% See also cl_simrad_all/plot_position plot_navigation Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_navigation_ShowLinesInBox

GCA = gca;
GCBF = gcbf;

XLim = get(GCA, 'XLim');
YLim = get(GCA, 'YLim');

% hg = findobj(GCF, 'Type', 'Line', 'Color', [0.8 0.8 0.8]);
hg = findobj(GCBF, 'Type', 'line');
for k=1:length(hg)
    pppp = get(hg(k), 'Tag');
    if isempty(pppp)
        subIsNav(k) = false; %#ok<AGROW>
    else
        subIsNav(k) = true; %#ok<AGROW>
    end
end
hg = hg(subIsNav);
        
% hOther = findobj(GCBF, 'Label', 'Hide line');
hOther = findobj(GCBF, 'Text', 'Hide line'); % Modif JMA le 20/05/2021
for k=1:length(hOther)
    GCBO = hOther(k);
%     GCA = gca;
    
    UserData = get(GCBO, 'UserData');
    
%     XData = get(hg(k), 'XData');
%     YData = get(hg(k), 'YData');
%     figure(5646555); plot(XData, YData); grid on; hold on;
    
    LonLim = UserData{6};
    if (LonLim(2) < XLim(1)) || (LonLim(1) > XLim(2))
        continue
    end
    
    LatLim = UserData{5};
    if (LatLim(2) < YLim(1)) || (LatLim(1) > YLim(2))
        continue
    end
    
    XData = get(hg(k), 'XData');
    YData = get(hg(k), 'YData');
    
    flag = (XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2));
    if all(~flag)
        continue
    end

    h = UserData{3};
    Color = UserData{1};
    set(h, 'Color', Color)
    UserData{2} = 'On';
    set(GCBO, 'UserData', UserData)
    
    nomFic = UserData{4};
    hOther2 = findobj(GCA, 'Tag', nomFic);
    hOther2(hOther2 == h) = [];
    set(hOther2, 'Visible', 'On')
end
