% TODO : a finir avec edit_xyz

function plot_xyz(X, Y, Z,varargin)

[varargin, XLabel] = getPropertyValue(varargin, 'XLabel', []); %#ok<ASGLU>
[varargin, YLabel] = getPropertyValue(varargin, 'YLabel', []); %#ok<ASGLU>
[varargin, ZLabel] = getPropertyValue(varargin, 'ZLabel', []); %#ok<ASGLU>

FigUtils.createSScFigure;
h1 = subplot(2,2,1); title('X - Z');
h2 = subplot(2,2,2); title('Y - Z');
h3 = subplot(2,2,3); title('X - Y');
h4 = subplot(2,2,4); title('3D');

hp1 = plot(h1, X, Z, 'o');     grid(h1, 'on');
hp2 = plot(h2, Y, Z, 'o');     grid(h2, 'on');
hp3 = plot(h3, X, Y, 'o');     grid(h3, 'on');
hp4 = plot3(h4, Y, Z, X, 'o'); grid(h4, 'on');
set([hp1 hp2 hp3 hp4], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 4);
hlinky = linkprop([h1 h2 h4], 'YLim'); %#ok<NASGU>
hlinkx = linkprop([h1 h3], 'XLim'); %#ok<NASGU>
hlinkz = linkprop([h2 h4], 'XLim'); %#ok<NASGU>



% hp1 = plot3(h1, X, Y, Z, 'o'); grid(h1, 'on');
% hp2 = plot3(h2, X, Y, Z, 'o'); grid(h2, 'on');
% hp3 = plot3(h3, X, Y, Z, 'o'); grid(h3, 'on');
% hp4 = plot3(h4, X, Y, Z, 'o'); grid(h4, 'on');
% set([hp1 hp2 hp3 hp4], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 4);
% % linkaxes([h1 h2 h4], 'y')
% hlink1 = linkprop([h1 h2 h3 h4], 'YLim')
% hlink2 = linkprop([h1 h2 h3 h4], 'YLim')
% hlink3 = linkprop([h1 h2 h3 h4], 'ZLim')

