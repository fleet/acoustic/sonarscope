% Set the arrow-cursor.
%
% Syntax
%   idle
%   idle(fig)
%
% Name-Value Pair Arguments
%   fig : numero de la figure (gcf par defaut) 
%
% Examples 
%   plot(1:10); busy
%   idle
%
% See also busy idle Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function pointer_loupe(theFigure)

if my_verLessThanR2014b
    if ~any(findobj('Type', 'figure'))
        return
    end
else
    % 'TODO - pointer_loupe : ~any(findobj(''Type'', ''figure''))'
    % mais cette instruction me semble inutile car il y a forc�ment une
    % figure ! A laisser de c�t� et attendre si bug �ventuel
end

if nargin < 1
    theFigure = gcf;
end

X = imread(getNomFicDatabase('loupe.tif'));
X = X(:,:,1);
P = NaN * zeros(16);
P(X == 0) = 1;
P(X == 255) = 2;
P = fliplr(P);

% set(theFigure, 'Pointer', 'arrow');
set(theFigure, 'Pointer', 'custom', 'PointerShapeCData', P, 'PointerShapeHotSpot', [1 1])