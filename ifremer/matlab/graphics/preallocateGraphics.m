function h = preallocateGraphics(n1, n2)

if my_verLessThanR2014b
    h = zeros(n1, n2);
else
    h = gobjects(n1, n2);
end
