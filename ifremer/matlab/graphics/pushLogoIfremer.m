% Creation d'un bouton de type "Pushbutton" contenant une icone (FONCTION MORIBONDE)
% Si le nom du fichier n'est pas fourni, c'est l'icone Ifremer qui est affiche
%
% Syntax
%   h = set(fig, x, y, 'PropertyName', PropertyValue)
%
% Input Arguments
%   fig : Numero de la figure
%    x   : Position en x
%    y   : Position en y
%
% PROPERTY NAMES :
%  'Zoom'     : Facteur de zoom de l'icone
%  'Callback' : Fonction de callback associee au bouton
%  'Tag'      : Tag associe au bouton
%  'NomFic'   : Nom du fichier contenant l'icone
%  'Units'    : Unite des x ey y {points} | normalized | inches | centimeters | points | characters
%
% Output Arguments
%   Handle du bouton.
%
% Examples
%   fig = figure;
%   pushLogoIfremer(fig, 50, 50);
%   pushLogoIfremer(fig, 50, 100, 'Zoom', 0.5);
%   pushLogoIfremer(fig, 50, 140, 'Zoom', 0.5, 'Callback', 'date');
%   nomFic = getNomFicDatabase('ZoomPlus.tif');
%   pushLogoIfremer(fig, 50, 200, 'NomFic', nomFic);
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function h = pushLogoIfremer(fig, x, y, varargin)

[varargin, Zoom]     = getPropertyValue(varargin, 'Zoom',     []);
[varargin, Callback] = getPropertyValue(varargin, 'Callback', []);
[varargin, Tag]      = getPropertyValue(varargin, 'Tag',      []);
[varargin, NomFic]   = getPropertyValue(varargin, 'NomFic',   []);
[varargin, Units]    = getPropertyValue(varargin, 'Units',    []); %#ok<ASGLU>

if isempty(Zoom)
    Zoom = 1;
end
if isempty(Callback)
    Callback = 'my_web(''http://www.ifremer.fr'')';
end
if isempty(Tag)
    Tag = 'pushLogoIfremer';
end
if isempty(NomFic)
    NomFic = getNomFicDatabase('logoIfremerJet.tif');
end
if isempty(Units)
    Units = 'points';
end

icone = double(imread( NomFic )) / 255;

[nl, nc, np] = size(icone);

if(np ~= 3)
    icone = imreadrgb( NomFic );
    [nl, nc, np] = size(icone); %#ok
end

[ix, iy]    = meshgrid(1:nc, 1:nl);
[ixi,iyi]   = meshgrid(1:1/Zoom:nc, 1:1/Zoom:nl);

logoIfremer = zeros(size(ixi, 1), size(ixi, 2), 3);
for i=1:3
    logoIfremer(:,:,i) = interp2(ix, iy, icone(:,:,i), ixi, iyi, 'nearest');
end

[nl, nc, np] = size(logoIfremer); %#ok

%largeur = nc-(40*Zoom);
largeur = nc;
hauteur = nl;

Position = get(gcf, 'Position');
largeurFig = Position(3);
hauteurFig = Position(4);

switch Units
    case 'normalized'
        largeur = 1.25 * largeur / largeurFig;
        hauteur = 1.25 * hauteur / hauteurFig;
    case 'pixel'
        largeur = largeur / largeurFig;
        hauteur = hauteur / largeurFig;
end

h = uicontrol('Parent', fig, 'Units', Units, 'Tag', Tag, ...
    'CData', logoIfremer, 'Callback', Callback, ...
    'Position', [x  y largeur hauteur]);
