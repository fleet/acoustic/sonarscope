% Stockage d'une information taguee dans une liste
% de la forme : liste{:}.PropertyName liste{:}.PropertyValue
% Si l'information (PropertyName) n'est pas presente dans la liste, elle est creee,
% si elle l'est, la valeur PropertyValue remplace la precedente.
% Cette fonction est utile pour gerer les UserData des graphiques
%
% Syntax
%   UserDataOut = putInUserData( PropertyName, PropertyValue, UserDataIn)
%
% Input Arguments
%   PropertyName  : Identifiant de l'information
%   PropertyValue : Information devant etre stockee
%   UserDataIn    : Variable contenant toutes les infos
%
% Output Arguments
%   Si pas de valeur, c'est le parametre UserDataIn qui est modifie
%   UserDataOut   : Variable contenant la liste a jour
%
% Examples
%   UserData = [];
%   UserData = putInUserData('A', 1, UserData)
%   UserData = putInUserData('B', 1:4, UserData)
%   putInUserData('C', rand (2,2), UserData)
%   putInUserData('D', 'toto', UserData)
%   putInUserData('E', {1; 'x'}, UserData)
%   UserData{:}
%   fig = figure;
%   set(fig, 'UserData', UserData)
%   x = get(fig, 'UserData')
%   x{:}
%
% See also getFromUserData rmInUserData Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = putInUserData(PropertyName, PropertyValue, UserDataIn)

%% Recherche si la PropertyName existe deja dans la liste

[~, indice] = getFromUserData(PropertyName, UserDataIn);

if isempty(indice)
    k = length(UserDataIn) + 1;
else
    k = indice;
end

UserDataIn{k}.PropertyName  = PropertyName;
UserDataIn{k}.PropertyValue = PropertyValue;

UserDataOut = UserDataIn;

%% Mise � jour de la var d'entr�e ou sortie habituelle

if nargout == 0
    assignin('caller', inputname(3), UserDataOut);
else
    varargout{1} = UserDataOut;
end
