% Suppression d'une information identifiee dans une liste
% de la forme : liste{:}.PropertyName liste{:}.PropertyValue
% Cette fonction est utile pour gerer les UserData des graphiques
%
% Syntax
%   UserDataOut = rmInUserData( PropertyName, UserDataIn)
%
% Input Arguments
%   PropertyName : Identifiant de l'information a supprimer
%   UserDataIn   : Liste contenant toutes les infos
%
% Output Arguments
%   Si pas de valeur, c'est le parametre UserDataIn qui est modifiee
%   UserDataOut   : Variable contenant la liste remise a jour
%
% Examples
%   UserData = [];
%   putInUserData( 'A', 1, UserData )
%   putInUserData( 'B', 1:4, UserData )
%   putInUserData( 'C', rand (2,2), UserData )
%   putInUserData( 'D', 'toto', UserData )
%   putInUserData( 'E', {1; 'x'}, UserData )
%   UserData{:}
%   UserData = rmInUserData( 'C', UserData )
%   rmInUserData( 'D', UserData )
%   rmInUserData( 'xxxx', UserData )
%   UserData{:}
%
% See also getFromUserData putInUserData Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = rmInUserData(PropertyName, UserDataIn)

% ------------------------------------------------------
% Recherche si la PropertyName existe dans la liste

[ancienneValue, indice] = getFromUserData( PropertyName, UserDataIn );
if isempty(indice)
    k = length(UserDataIn) + 1;
else
    k = indice;
end

j = 0;
for i=1:length(UserDataIn)
    if( i ~= k )
        j = j + 1;
        UserDataOut{j} = UserDataIn{i};
    end
end

% ---------------------------------------------------
% Mise a jour de la var d'entree ou sortie habituelle

if (nargout == 0)
    assignin('caller', inputname(2), UserDataOut);
else
    varargout{1} = UserDataOut;
end

% end rmInUserData %
