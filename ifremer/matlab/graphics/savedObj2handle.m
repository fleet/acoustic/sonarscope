% Sauve l'instance sauvee dans le UserData
%
% Syntax
%   savedObj2handle(instance, h0)
%
% Input Arguments
%   instance : instance a sauver
%   h0       : Numero de la figure
%
% Examples
%   h0 = figure;
%   x = cl_backscatter;
%   savedObj2handle(x, h0)
%   instance = handle2savedObj(h0)
%
% See also handle2savedObj handle2obj handle2obj Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function savedObj2handle(instanceSauvee, h0)

var2handle('InstanceSauvee', instanceSauvee, h0);