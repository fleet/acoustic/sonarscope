% Selection d'un certain nombre d'items dans une liste
%
% Items = selectionItems( listeItems )
% 
% Input Arguments 
%   listeItems : Liste des items
%
% Output Arguments 
%   Items : Indices selectionnes
%
% Examples
%   rep = selectionItems({'Janvier';'Fevrier';'Mars';'Avril';'Mai'})
%
% See also Authors
% Authors : JMA
% VERSION    : $Id: selectionItems.m,v 1.1 2002/02/01 15:27:51 augustin Exp $
%-------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   01/02/02 - JMA - creation
% ----------------------------------------------------------------------------

function Items = selectionItems( listeItems )

if nargin == 0
	help selectionItems
	return
end

listeItems = listeItems(:);
N = length(listeItems);
L = 1;

a = figure('toolbar', 'none');
NumFig = get(0,'CurrentFigure');

FigHeight = min(100 + 15 * N, 800);
FigWidth = min(220 + 3 * L, 400);

set(gcf,'Position', [296 50 FigWidth FigHeight]);

Items = [];

FigPos      = [20 40 100 150];
FigWidth    = 75;
FigHeight   = min(10+15 * N, 600);
FigWidth    = min(150 + 3 * L, 400);
FigPos(3:4) = [FigWidth FigHeight];

FigPos(3) = FigWidth;

b = uicontrol(  'Parent', a, ...
                'String', listeItems, ...
                'Units', 'points', ...
                'Style', 'listbox', ...
                'BackgroundColor' , [1 1 1]  , ...
                'Position', FigPos, ...
                'Max', 1000, 'Min', 1, ...
                'Value', Items );

d = uicontrol('Parent', a, 'String', 'OK', 'Callback', 'uiresume');

uiwait(NumFig);
Items = get(b, 'Value');
delete(gcf);
