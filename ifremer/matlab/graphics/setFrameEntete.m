% Remplissage de la frame Entete
%
% Syntax
%   frameEntete = setFrameEntete(h0, Titre)
%
% Input Arguments
%    h0    : Handle de la figure
%    Titre : Nom de la figure
%
% Name-Value Pair Arguments
%  'HtmlFile' : Nom du fichier d'aide
%
% Output Arguments
%   frameEntete : Objet ClFrame.
%
% Examples
%   figure('Position', [100   200   900   300])
%   uicontrol('Style', 'frame', 'Tag', 'frameEntete', ...
%             'Units', 'normalized', 'Position', [0.02 0.85 0.96 0.1]);
%   frameEntete = setFrameEntete(gcf, 'Exemple')
%
% See also uilayout Authors
% Authors : JMA
%-------------------------------------------------------------------------------

[varargin, nomFicHtml] = getPropertyValue(varargin, 'HtmlFile', []); 

handleFrame = findobj(h0, 'Tag', 'frameEntete');
Position = get(handleFrame , 'Position');

ihm = [];

% --------------
% Bouton Ifremer

ihm{end+1}.Lig = 1;
ihm{end}.Col = 1;
ihm{end}.Style = 'Pushbutton';
ihm{end}.String = '';
ihm{end}.BackgroundColor = Gris;
ihm{end}.TooltipString = 'Acces au Web Ifremer';
ihm{end}.Callback = 'my_web(''http://www.ifremer.fr'')';
ihm{end}.CData = logoIfremer;

% ------------------------------------
% Edition d'un objet CBeam	titre

ihm{end+1}.Lig = 1;
ihm{end}.Col = 5:9;
ihm{end}.Style = 'Pushbutton';
ihm{end}.String = Titre;
ihm{end}.FontWeight = 'bold';
ihm{end}.BackgroundColor = Gris;
ihm{end}.Tag = 'NomObjet';
if ~isempty(nomFicHtml)
    ihm{end}.Callback = sprintf('my_web(''file://%s'');', nomFicHtml);
end

% --------------
% Bouton TMSI/AS

ihm{end+1}.Lig = 1;
ihm{end}.Col = 2:4;
ihm{end}.Style = 'Pushbutton';
ihm{end}.String = 'TMI / Acoustics and Seismics';
ihm{end}.BackgroundColor = Gris;
ihm{end}.TooltipString = 'Acces au Web TSI-AS';
ihm{end}.Callback = 'my_web(''http://w3.ifremer.fr/tmsi-as/'')';

% ----------------
% Bouton Copyright

ihm{end+1}.Lig = 1;
ihm{end}.Col = 10;
ihm{end}.Style = 'Pushbutton';
ihm{end}.String = 'Copyright';
ihm{end}.FontWeight = 'bold';
ihm{end}.BackgroundColor = JauneIfremer;
ihm{end}.TooltipString = 'Acces au Copyright Ifremer';
ihm{end}.Callback = 'my_web(''http://www.ifremer.fr/francais/copyrigh.htm'')';


% --------------------
% Creation de la frame

frameEntete = cl_frame ( 'NomFrame', 'frameEntete', ...
    'InsetX', 4, 'InsetY', 3, 'ListeUicontrols', ihm);

