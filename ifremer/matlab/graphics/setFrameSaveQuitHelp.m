% Remplissage du frame Entete
%
% Syntax
%   frameSaveQuitHelp = setFrameSaveQuitHelp(h0, NomEditeur, NomClasse)
%
% Input Arguments
%   h0         : Handle de la figure
%   NomEditeur : Nom de la fonction d'edition appelante
%   NomClasse  : Nom de la classe del'objet
%
% Output Arguments
%   frameSaveQuitHelp : Objet ClFrame.
%
% Examples
%   figure('Position', [100   200   900   300])
%   uicontrol('Style', 'frame', 'Tag', 'frameSaveQuitHelp', ...
%             'Units', 'normalized', 'Position', [0.02 0.15 0.5 0.1]);
%   frameSaveQuitHelp = setFrameSaveQuitHelp(gcf, 'edit', 'ClCelerite', [0.7 0.7 0.7])
%
% See also fillFrame Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function frameSaveQuitHelp = setFrameSaveQuitHelp(~, NomEditeur, NomClasse, varargin)

% handleFrame = findobj(h0, 'Tag', 'frameSaveQuitHelp');
% Position = get(handleFrame , 'Position');

ib = 0;

% ----------------
% Bouton Save

ib = ib+1;
ihm{ib}.Lig = 1;
ihm{ib}.Col = 1;
ihm{ib}.Style = 'Pushbutton';
ihm{ib}.String = 'Save';
ihm{ib}.Tag = 'Save';
ihm{ib}.Callback = sprintf('%s(%s, ''Save'')', NomEditeur, NomClasse);
ihm{ib}.BackgroundColor = Gris;

% ----------------
% Bouton Quit

ib = ib+1;
ihm{ib}.Lig = 1;
ihm{ib}.Col = 2;
ihm{ib}.Style = 'Pushbutton';
ihm{ib}.String = 'Quit';
ihm{ib}.Tag = 'Quit';
ihm{ib}.Callback = 'close(gcf)';
ihm{ib}.BackgroundColor = Gris;

% ----------------
% Bouton Help

ib = ib+1;
ihm{ib}.Lig = 1;
ihm{ib}.Col = 3;
ihm{ib}.Style = 'Pushbutton';
ihm{ib}.String = 'Help';
ihm{ib}.Tag = 'Help';
% nomFicHtml = [get_ressources(NomClasse) filesep NomClasse '.htm'];
% ihm{ib}.Callback = sprintf('my_web(''file://%s'');', nomFicHtml);
ihm{ib}.BackgroundColor = Gris;

% --------------------
% Creation de la frame

frameSaveQuitHelp = fillFrame('frameSaveQuitHelp', ihm, ...
    'XMargin', 0.005, 'YMargin', 0.1);