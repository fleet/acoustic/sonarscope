% Creation d'une figure representant les symboles associes aux valeurs de 0 a 255
%
% Syntax
%   str2symbol
% 
% Examples 
%   str2symbol
%   uicontrol('Position', [0 0 30 30], 'FontName', 'Symbol', 'String', sprintf('%c', 99));
%   uicontrol('Position', [0 40 30 30], 'FontName', 'Symbol', 'String', 'd');
%
% See also uicontrol Authors
% Authors : JMA
% VERSION  : $Id: str2symbol.m,v 1.3 2002/06/04 14:42:25 augustin Exp $
%--------------------------------------------------------------------------------

function  str2symbol

close all
figure
sz = [20 20 1200, 1000];
set(gcf, 'Position', sz);
i = 0;
N = 20;
for k=1:(300/N)
    for j=1:N
        if(i <= 255)
            x = 80 * k;
            y = 40 * j;
            uicontrol('Position', [x    y 30 30], 'String', i);
            uicontrol('Position', [x+30 y 30 30], 'FontName', 'Symbol', 'String', sprintf('%c', i));
            i = i + 1;
        end 
    end
end