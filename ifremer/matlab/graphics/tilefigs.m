% Rangement des figures sur l'ecran
%
% Syntax
%   tilefigs(...)
%
% Name-Value Pair Arguments
%   fig    : Liste des figures a reordonner
%   nrows  : Nombre de lignes
%   ncols  : Nombre de colonnes
%   border : Ecart entre figures
%
% Examples
%   for i=1:7, figure, end
%   tilefigs
%
% Remarks : Fonction recuperee sur le net : auteur Charles Plum  Nichols Research Corp
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function tilefigs(varargin)

[varargin, border] = getPropertyValue(varargin, 'border', 0);
[varargin, nrows]  = getPropertyValue(varargin, 'nrows',  0);
[varargin, ncols]  = getPropertyValue(varargin, 'ncols',  0);
[varargin, hands]  = getPropertyValue(varargin, 'fig',    []); %#ok<ASGLU>

if isempty(hands)
    hands       = get (0, 'Children');  % locate fall open figure handles
    hands       = sort(hands);          % sort figure handles
end
numfigs     = length(hands);        % number of open figures

maxpos      = get (0,'screensize'); % determine terminal size in pixels
maxpos(4)   = maxpos(4) - 25;

if (nrows == 0) && (ncols == 0)
    ncols = ceil(sqrt(numfigs));
    nrows = ceil(numfigs/ ncols);
elseif (nrows == 0)
    nrows = ceil(numfigs/ ncols);
elseif (ncols == 0)
    ncols = ceil(numfigs / nrows);
end

maxpos(3) = maxpos(3) - 2*border;
maxpos(4) = maxpos(4) - 2*border;
xlen = fix(maxpos(3) / ncols) - 30; % new tiled figure width
ylen = fix(maxpos(4) / nrows) - 45; % new tiled figure height


% tile figures by postiion
% Location (1,1) is at bottom left corner
pnum = 0;
for iy = 1:nrows
    ypos = maxpos(4) - fix((iy) * maxpos(4) / nrows) + border + 25; % figure location (row)
    for ix = 1:ncols
        xpos = fix((ix-1) * maxpos(3) / ncols + 1) + border + 7;     % figure location (column)
        pnum = pnum+1;
        if pnum > numfigs
            break
        else
            figure(hands(pnum))
            set(hands(pnum), 'Position',[ xpos ypos xlen ylen ]); % move figure
        end
    end
end
