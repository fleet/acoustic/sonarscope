% S�lection d'un ou plusieurs items dans une liste ou plusieurs listes.
%
% Syntax
%   [flag, rep] = uiSelectItemsFromTableSheet(ListItems, ...)
%
% Input Arguments
%   ListItems       : Liste des items (cell array of char)
%   titleDataSet    : List des titres des diff�rentes tables.
%
% Name-Value Pair Arguments
%   Entete                     : Titre de la fenetre
%   Resize(off)                : Capacit� de redimensionnement de la fen�tre.
%   SelectionMode(Multiple)    : Mode de s�lection Simple ou Multiple dans les tables.
%   Orientation(portrait)      : Mode Portrait ou Paysage.
%   Resizable(on)              : Redimensionnement ou non de la fen�tre.
%   AdjustTabSize(on)          : Ajustement des tailles de tables ou non.
%
% Examples
%   TODO
%   [flag, rep] = uiSelectItemsFromTableSheet('Images a detruire', str)
%   [flag, rep] = uiSelectItemsFromTableSheet('Images a detruire', dataSet, ...
%                       'titleDataSet', {titleDataSet2, titleDataSet3}, ...
%                       'InitialValue2', 2, ...
%                       'Entete', 'Layers � charger')
%
%   % 3 Lignes / 1 Colonne.
%     ListeLayers2 = ListeLayers;
%     ListeLayers2.SelectionLayers(:,2)    = [];
%     % ListeLayers2.Order(:,2)            = [];
%     ListeLayers2.Units(:,2)              = [];
%     ListeLayers2.Comments(:,2)           = [];
%     [flag, strList] = uiSelectItemsFromTableSheet(ListeLayers2, ...
%         'titleDataSet', titleDataSet, ...
%         'SelectionMode', 'Single', ...
%         'Orientation', 'landscape', ...
%         'Resizable', 'off')
%
%   % Liste large de layers
%   global IfrTbx %#ok<GVMIS>
%   load(fullfile(IfrTbx, '..', 'SonarScopeTbxTests', 'largeListeLayers.mat'));
%
%   [flag, strList] = uiSelectItemsFromTableSheet(ListeItems, ...
%           'titleDataSet', titleDataSet, ...
%           'InitialValue2', 2, ...
%           'Entete', Lang(str1,str2), ...
%           'SelectionMode', 'Single', ...
%           'CheckBoxExist', false);
%
% See also tableInDatagrams listdlg Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [flagOKCancel, ListItems] = uiSelectItemsFromTableSheet(ListItems, varargin)

[varargin, titleDataSet]  = getPropertyValue(varargin, 'titleDataSet',  []);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
[varargin, Entete]        = getPropertyValue(varargin, 'Entete',        'IFREMER-SonarScope');
[varargin, Orientation]   = getPropertyValue(varargin, 'Orientation',   'portrait');
[varargin, Resizable]     = getPropertyValue(varargin, 'Resizable',     'on');
[varargin, AdjustTabSize] = getPropertyValue(varargin, 'AdjustTabSize', 'on');
[varargin, CheckBoxExist] = getPropertyValue(varargin, 'CheckBoxExist',  true); %#ok<ASGLU>

flagOKCancel = 0;

%% Formattage des donn�es pour assignation dans les tables

nbMaxItems = max(cellfun(@(x) numel(x), ListItems.nomLayers));
if nbMaxItems > 25
    % For�age du mode d'ajustement � off pour afficher des sliders lat�raux.
    AdjustTabSize = 'off';
end

if ~isfield(ListItems, 'SelectionLayers')
    for k1=1:size(ListItems.nomLayers,1)
        for k2=1:size(ListItems.nomLayers,2)
            ListItems.SelectionLayers{k1,k2} = true(1,length(ListItems.nomLayers{k1,k2}));
        end
    end
end

if ~isfield(ListItems, 'Order')
    for k1=1:size(ListItems.nomLayers,1)
        for k2=1:size(ListItems.nomLayers,2)
            ListItems.Order{k1,k2} = false(1,length(ListItems.nomLayers{k1,k2}));
        end
    end
end

if ~isfield(ListItems, 'Units')
    for k1=1:size(ListItems.nomLayers,1)
        for k2=1:size(ListItems.nomLayers,2)
            ListItems.Units{k1,k2} = cell(1,length(ListItems.nomLayers{k1,k2}));
        end
    end
end

if ~isfield(ListItems, 'Comments')
    for k1=1:size(ListItems.nomLayers,1)
        for k2=1:size(ListItems.nomLayers,2)
            ListItems.Comments{k1,k2} = cell(1,length(ListItems.nomLayers{k1,k2}));
        end
    end
end

nbRows = size(ListItems.SelectionLayers,1);
nbCol = size(ListItems.SelectionLayers,2);

dataSet = cell(nbRows, nbCol);
for iL=1:nbRows
    for iC=1:nbCol
        %         for k=1:size(ListItems.SelectionLayers{iL,iC},2)
        for k=1:length(ListItems.SelectionLayers{iL,iC}(:))
            % Composition des donn�es (concat�nation de Nom et Unit� si ce
            % dernier existe).
            if isfield(ListItems, 'Units')
                if ~isempty(ListItems.Units{iL,iC}{k})
                    strName = [ListItems.nomLayers{iL,iC}{k} ' (' ListItems.Units{iL,iC}{k} ')'];
                else
                    strName = ListItems.nomLayers{iL,iC}{k};
                end
                dataSet{iL,iC}{k} = {logical(ListItems.SelectionLayers{iL,iC}(k)), strName};
            else
                dataSet{iL,iC}{k} = {logical(ListItems.SelectionLayers{iL,iC}(k)), ListItems.nomLayers{iL,iC}{k}};
            end
        end
    end
end

%% Modif GLT opu bug si un �l�ment de dataSet vide

% Ajout JMA le 01/10/2015 : il faudrait transmettre cette info � la
% fonction qui sort dataSet.SelectionLayers({subTrucMuch(iL),iC}(k))
% for k=length(dataSet):-1:1
%     flagEmpty(k) = isempty(dataSet{k});
% end

dataSet(cellfun(@isempty, dataSet)) = [];

nbRows = size(dataSet,1);
nbCol = size(dataSet,2);

%% Cr�ation de la figure.

hdlFig = figure('Menubar', 'none',...
    'Units', 'pixels', ...
    'Position', [400 10 820 900], ...
    'Name', 'Select datagrams you want to save', ...
    'Color', [229/255 229/255 229/255], ...
    'CloseRequestFcn', {@cbCloseFcn, []}, ...
    'Name', Entete, ...
    'Visible', 'on', ...
    'Tag', 'tagLayerSelector', ...
    'Resize', Resizable);

setappdata(hdlFig, 'NbLig',         nbRows);
setappdata(hdlFig, 'NbCol',         nbCol);
setappdata(hdlFig, 'ListItems',     ListItems);
setappdata(hdlFig, 'SelectionMode', SelectionMode);
setappdata(hdlFig, 'CheckBoxExist', CheckBoxExist);

%% Cr�ation des containers principaux soit en ligne soit en ligne-colonne.
% Cr�ation du container vertical

bgColorMainPnl = [160/255 160/255 160/255];
bgColorAnxPnl  = [180/255 180/255 180/255];

% VBox parente qui contient hMainBox & boutons Select-Unselect All &K-Cancel.
hParentMainBox = uiextras.VBox('Parent', hdlFig, ...
    'Spacing', 5, ...
    'Tag', 'tagParentMainBox', ...
    'BackgroundColor', bgColorMainPnl);

hMainBox = uiextras.VBoxFlex('Parent', hParentMainBox, ...
    'Spacing', 5, ...
    'Tag', 'tagMainBox', ...
    'BackgroundColor', bgColorMainPnl);

% Cr�ation des containers Lignes-Colonnes

if strcmp(Orientation, 'portrait')
    for iL=1:nbRows
        for iC=1:nbCol
            if nbCol == 1
                hPeerLigBox(iL) = uiextras.VBox('Parent', hMainBox, 'Padding', 5, 'Spacing', 5 , 'Tag', ['tagPeerBox_Lig_' num2str(iL)], 'BackgroundColor', bgColorMainPnl); %#ok<AGROW>
            else
                % On cr�� qu'un container horizontal pour plusieurs colonnes.
                if iC == 1
                    hPeerLigBox(iL) = uiextras.HBox('Parent', hMainBox, 'Padding', 5, 'Spacing', 5, 'Tag', ['tagPeerBox_Lig_' num2str(iL) '_Col' num2str(iC)], 'BackgroundColor', bgColorAnxPnl); %#ok<AGROW>
                end
                % Box pour les Layers natifs et d�duits (colonne 2)
                hChildLigColBox(iL, iC) = uiextras.VBox('Parent', hPeerLigBox(iL), 'Tag', ['tagChildBox_Lig_' num2str(iL) '_Col' num2str(iC)], 'BackgroundColor', bgColorAnxPnl); %#ok<AGROW>
            end
        end
    end
    
else % Mode Landscape
    
    hMainBoxLS  = uiextras.HBox('Parent', hMainBox, 'Spacing', 5, 'Tag', 'tagMainBox', 'BackgroundColor', bgColorMainPnl);
    % Cr�ation des containers Lignes-Colonnes
    for iL=1:nbRows
        for iC=1:nbCol
            if nbCol == 1
                if strcmp(Orientation, 'portrait')
                    hPeerLigBox(iL) = uiextras.HBox('Parent', hMainBoxLS, 'Padding', 5, 'Spacing', 5 , 'Tag', ['tagPeerBox_Lig_' num2str(iL)], 'BackgroundColor', bgColorMainPnl); %#ok<AGROW>
                else
                    hPeerLigBox(iL) = uiextras.VBox('Parent', hMainBoxLS, 'Padding', 5, 'Spacing', 5 , 'Tag', ['tagPeerBox_Lig_' num2str(iL)], 'BackgroundColor', bgColorMainPnl); %#ok<AGROW>
                end
            else
                % On cr�� qu'un container horizontal pour plusieurs colonnes.
                if iC == 1
                    hPeerLigBox(iL) = uiextras.VBox('Parent', hMainBoxLS, 'Padding', 5, 'Spacing', 5, 'Tag', ['tagPeerBox_Lig_' num2str(iL) '_Col' num2str(iC)], 'BackgroundColor', bgColorAnxPnl); %#ok<AGROW>
                end
                % Box pour les Layers natifs et d�duits (colonne 2)
                hChildLigColBox(iL, iC) = uiextras.VBox('Parent', hPeerLigBox(iL), 'Padding', 5, 'Spacing', 5, 'Tag', ['tagChildBox_Lig_' num2str(iL) '_Col' num2str(iC)], 'BackgroundColor', bgColorAnxPnl); %#ok<AGROW>
            end
        end
    end
end

if ~strcmpi(SelectionMode, 'Single')
    hSelectUnselectBtnVBox    = uiextras.HBox('Parent', hParentMainBox, 'Tag', 'tagSelectUnSelectBtnBox', 'BackgroundColor', bgColorAnxPnl);
end

hOkCancelBtnBox  = uiextras.HBox('Parent', hParentMainBox, 'Tag', 'tagOkCancelBtnBox', 'BackgroundColor', bgColorAnxPnl);


%% Cr�ation des sous-panels

% Gestion des tailles
pnlBtnCheckHeight   = 30;
sizeWidthCheckBox   = 30;
% uiextras.set( hdlFig, 'DefaultBoxPanelTitleColor', [0.7 1.0 0.7] ); % Vert Pomme
% Cr�ation des sous-panels selon les contenu des datas;
for iL=1:nbRows
    for iC=1:nbCol
        if ~exist('hChildLigColBox', 'var')
            hBox = hPeerLigBox(iL);
        else
            hBox = hChildLigColBox(iL, iC);
        end
%         hPnlDataset(iL, iC) = uix.BoxPanel('Parent', hBox, ...
        hPnlDataset(iL, iC) = uiextras.BoxPanel('Parent', hBox, ...
            'Tag', ['tagPnl_Lig' num2str(iL) '_Col' num2str(iC)], ...
            'FontWeight', 'bold', ...
            'FontAngle', 'italic', ...
            'Title',  titleDataSet(iL,iC)); %#ok<AGROW>
        
        
        % Cr�ation des panneaux de boutons Check et Uncheck.
        if ~strcmpi(SelectionMode, 'Single')
            hPPPP                   = uiextras.HButtonBox( 'Parent', hBox, 'Tag', 'tagBtnBox','Padding', 5);
            hBtnCheckAll(iL,iC)     = uicontrol('Parent', hPPPP, ...
                'String', Lang('Tous','Check All'), ...
                'Callback', {@cb_checkAllDatagrams, 1, iL, iC}); %#ok<AGROW>
            hBtnUncheckAll(iL,iC)   = uicontrol('Parent', hPPPP, ...
                'String', Lang('Aucun','UnCheck All'), ...
                'Callback', {@cb_checkAllDatagrams, 0, iL, iC}); %#ok<AGROW>
            % % %             if iC == 2
            % % %                 set(hBox, 'Sizes', [-1,-2]);
            % % %             end
            % % %
            % Ajustement de la taille des panels d'une cellule
            set(hBox, 'Sizes', [-1 pnlBtnCheckHeight]);
        end
    end
end

% Panel des boutons de sorties.
hPnlOkCancelBtn = uipanel('Parent', hOkCancelBtnBox, 'Title','', 'Tag', 'hPnlOkCancel');
if ~strcmpi(SelectionMode, 'Single')
    % Panel des boutons de sorties.
    hPnlSelectUnselectBtn = uipanel('Parent', hSelectUnselectBtnVBox, 'Title', '', 'Tag', 'hPnlSelectUnselectBtn', 'BackgroundColor', [0.2 0.2 0.2]);
end

% Inhibition de panels compl�mentaires (parasites ?) pour afficher
% correctement les BoxPanels unitaires.
pppp = findobj(hdlFig, 'Tag', 'uiextras:BoxPanel');
set(pppp, 'Visible', 'off')

%% Mise en forme des donn�es selon que le s�lecteur soit CheckBox ou un Select d'une ligne.

if CheckBoxExist == false
    colFormat     = {'char'};
    colEditStatus = false;
    idxName       = 1;
else
    colFormat     = {'logical', 'char'};
    colEditStatus = [true false];
    idxName       = 2;
end

idxSelect  = cell(nbRows,nbCol);
cellMaxLen = zeros(nbRows,nbCol);
colWidth   = cell(nbRows,nbCol);
for iL=1:nbRows
    for iC=1:nbCol
        if ~isempty(dataSet{iL,iC})
            pppp   = [dataSet{iL,iC}{:,:}];
            if CheckBoxExist == false
                dummy           = reshape(pppp, [2 size(dataSet{iL,iC},2)])';
                dataSet{iL,iC}  = dummy(:,2);
                % D�tection des index de pre-s�lection dans le cas o� il n'y a pas de
                % CheckBox.
                idx     = [dummy{:,1}];
                [u, v]  = find(idx == 1); %#ok<ASGLU>
                idxSelect{iL,iC} = (v-1);
                
                % Calcul de la longueur max des cha�nes pour ajustement des
                % colonnes
                maxWidth = 0;
                for k=1:size(dataSet{iL,iC},1)
                    maxWidth = max(maxWidth, length(dataSet{iL,iC}{k,idxName}));
                end
                % Some calibration needed as ColumnWidth is in pixels
                % http://www.mathworks.fr/support/solutions/en/data/1-EIJGZ9/index.html?solution=1-EIJGZ9
                cellMaxLen(iL, iC) = maxWidth*7;
                colWidth{iL, iC}   = {cellMaxLen(iL, iC)};
            else
                dataSet{iL,iC}  = reshape(pppp, [2 size(dataSet{iL,iC},2)])';
                % Calcul de la longueur max des cha�nes pour ajustement des
                % colonnes
                maxWidth        = 0;
                for k=1:size(dataSet{iL,iC},1)
                    maxWidth = max(maxWidth, length(dataSet{iL,iC}{k,idxName}));
                end
                % Some calibration needed as ColumnWidth is in pixels
                % http://www.mathworks.fr/support/solutions/en/data/1-EIJGZ9/index.html?solution=1-EIJGZ9
                cellMaxLen(iL, iC) = maxWidth*7;
                colWidth{iL, iC}   = {sizeWidthCheckBox, cellMaxLen(iL, iC)};
            end
        else
            dataSet{iL,iC}= {};
        end
    end
end

%% Cr�ation des tables

jTableHeight = zeros(nbRows,nbCol);
jTableWidth  = zeros(nbRows,nbCol);
for iL=1:nbRows
    for iC=1:nbCol
        if ~isempty([dataSet{iL,iC}])
            hTabDataSet(iL, iC) = uitable('Units',           'normalized', ...
                'Tag',                  ['tagUiTable_Lig' num2str(iL) '_Col' num2str(iC)], ...
                'Parent',               hPnlDataset(iL, iC), ...
                'Data',                 dataSet{iL,iC}, ...
                'RearrangeableColumns', 'off', ...
                'Position',             [0.0 0.0 1.0 1.0], ...
                'ColumnName',           [],...
                'ColumnFormat',         colFormat,...
                'ColumnEditable',       colEditStatus,...
                'RowName',              [], ...
                'ColumnWidth',          colWidth{iL,iC}); %#ok<AGROW>
            
            unitsHdlTable       = get(hTabDataSet(iL, iC), 'units');
            set(hTabDataSet(iL, iC), 'units', 'pixels');
            posTable            = get(hTabDataSet(iL, iC), 'Extent');
            jTableHeight(iL,iC) = posTable(4) - 4; % 4, valeur empirique
            jTableWidth(iL,iC)  = posTable(3) - 4; % 4, valeur empirique
            % Remise en unit� d'origine.
            set(hTabDataSet(iL, iC), 'units', unitsHdlTable);
            
            % Sans affichage du CheckBox, on pr�s�lectionne les Items.
            if CheckBoxExist == false
                idxFieldName = 0;
                % Surlignage des lignes s�lectionn�es.
                flag = uiTable_fcnSetFocusOnIndices(hTabDataSet(iL, iC), idxSelect{iL, iC}, idxFieldName);
                if ~flag
                    return
                end
                
                % Callback de gestion du mode de Selection Single.
                % Pb � faire fonctionner le mode Single pour un UiTable.
                if strcmpi(SelectionMode, 'Single')
                    [flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTabDataSet(iL, iC)); %#ok<ASGLU>
                    h = handle(jTable,'callbackproperties');
                    % get(h) %list callbacks
                    set(h,'MouseClickedCallback',{@cbCellSelectionInJTable, hdlFig, iL, iC});
                    % uiTable_fcnSetSelectionMode(hTabDataSet(iL, iC), 0);
                    % Non-fonctionnel ! set(hTabDataSet(iL, iC), 'ButtonDownFcn', @cbCellSelectionInTable);
                end
                
            else
                jTableWidth(iL,iC) = jTableWidth(iL,iC) + sizeWidthCheckBox;
                % Callback de gestion du mode de Selection Single.
                if strcmpi(SelectionMode, 'Single')
                    set(hTabDataSet(iL, iC), 'CellEditCallback', @cbCellSelectionInTable);
                end
            end
            
        else
            % Si on est en mode Selection Multiple.
            if ~strcmpi(SelectionMode, 'Single')
                set(hBtnCheckAll(iL, iC), 'enable', 'off');
                set(hBtnUncheckAll(iL, iC), 'enable', 'off');
            end
        end
    end
end

%% Calcul des tailles de la figure.

MargeHeightBoxPanel = 40;
MargeWidthBoxPanel  = 50 + 20; % Si Checkbox + Marge
% Organisation des tailles de sous-panneaux.
V1BoxSpacing = 5;
if strcmp(Orientation, 'portrait')
    for iL=1:nbRows
        if strcmp(AdjustTabSize, 'on')
            jHeight(iL)   = max(jTableHeight(iL,:)) + MargeHeightBoxPanel + V1BoxSpacing + pnlBtnCheckHeight; %#ok<AGROW>
            sizeV1Box(iL) = -jHeight(iL); %#ok<AGROW>
        else
            sizeV1Box(iL) = -1; %#ok<AGROW>
        end
    end
else
    % On swap l'orientation des dimensions.
    nbCol = nbRows;
    % On travaille cette fois-ci en Colonne (c'est bien nbRows qui est
    % observ�).
    for iC=1:nbCol
        if strcmp(AdjustTabSize, 'on')
            jWidth(iC)      = max(jTableWidth(iC))+ 20; %#ok<AGROW>
            sizeV1LSBox(iC) = jWidth(iC); %#ok<AGROW>
        else
            sizeV1LSBox(iC) = -1; %#ok<AGROW>
        end
    end
    set(hMainBoxLS, 'Sizes', sizeV1LSBox, 'Spacing', V1BoxSpacing);
    sizeV1Box(1) = -max(max(jTableHeight(:,:))) + MargeHeightBoxPanel + V1BoxSpacing + pnlBtnCheckHeight;
end

% Taille de la VBox parente qui contient hMainBox & boutons Select-Unselect
% All &K-Cancel.
% Ajout des tailles des boutons Select-Unselect All
sizeParentV1Box(1) = -1; % Taille pour hMainBox
if ~strcmpi(SelectionMode, 'Single')
    sizeParentV1Box(end+1) = 40;
end
% Ajout des tailles des boutons OK-Cancel.
sizeParentV1Box(end+1) = 40;

set(hMainBox, 'Sizes', sizeV1Box, 'Spacing', V1BoxSpacing);
set(hParentMainBox, 'Sizes', sizeParentV1Box, 'Spacing', V1BoxSpacing);
set(hdlFig, 'Units', 'pixels');
posFig  = get(hdlFig, 'Position');
if sizeV1Box(1) ~= -1
    heightFig = -sum(sizeV1Box(1:end)) +  sum(sizeParentV1Box(2:end)) + V1BoxSpacing*3;
else
    heightFig = posFig(4);
end
if nbCol == 1
    if strcmp(Orientation, 'portrait')
        widthFig = max(max(jTableWidth(:, :))) + MargeWidthBoxPanel;
    else
        widthFig = posFig(3);
    end
else
    if strcmp(Orientation, 'portrait')
        widthFig = posFig(3);
    else
        widthFig = max(sum(jTableWidth(:, :))) + MargeWidthBoxPanel ;
    end
end
set(hdlFig, 'Position', [posFig(1) posFig(2) widthFig heightFig]);
% set(hdlFig, 'Visible', 'on');

%% Affichage des boutons Select/Unselect

if ~strcmpi(SelectionMode, 'Single')
    hBtnSelect  = uicontrol(...
        'Parent',   hPnlSelectUnselectBtn,...
        'Units',    'normalized',...
        'Position', [0.0 0.0 0.5 1.0],...
        'Tag',      'tagBtnOk',...
        'String',   Lang('Tout Selectionner','Select all'), ...
        'Tooltip',  Lang('Tout Selectionner','Select all'), ...
        'CallBack', {@cb_SelectAllDatagrams, 1}); %#ok<NASGU>
    
    hBtnUnSelect = uicontrol(...
        'Parent',   hPnlSelectUnselectBtn,...
        'Units',    'normalized',...
        'Position', [0.5 0.0 0.5 1.0],...
        'Tag',      'tagBtnCancel',...
        'String',   Lang('Tout d�selectionner','Unselect all'), ...
        'Tooltip',  Lang('Tout d�selectionner','Unselect all'), ...
        'CallBack', {@cb_SelectAllDatagrams, 0}); %#ok<NASGU>
end
%% Affichage des boutons OK/Cancel

hBtnOK = uicontrol(...
    'Parent',   hPnlOkCancelBtn,...
    'Units',    'normalized',...
    'Position', [0.0 0.0 0.5 1.0],...
    'Tag',      'tagBtnOk',...
    'String',   'OK', ...
    'Tooltip',  Lang('Valider','Validate'), ...
    'CallBack', @cbBtnOK); %#ok<NASGU>

hBtnCancel = uicontrol(...
    'Parent',   hPnlOkCancelBtn,...
    'Units',    'normalized',...
    'Position', [0.5 0.0 0.5 1.0],...
    'Tag',      'tagBtnCancel',...
    'String',   Lang('Annuler','Cancel'), ...
    'Tooltip',  Lang('Annuler','Cancel'), ...
    'CallBack', @cbBtnCancel); %#ok<NASGU>

%% Centrage de la figure.

centrageFig(widthFig, heightFig, 'Fig', hdlFig);
set(hdlFig, 'Visible', 'on');

% Make the GUI blocking
uiwait(hdlFig);

% Reprise des sorties d'IHM
if isappdata(hdlFig, 'flagOKCancel')
    flagOKCancel = getappdata(hdlFig, 'flagOKCancel');
    rmappdata(hdlFig,'flagOKCancel');
end
if isappdata(hdlFig, 'ListItems')
    ListItems = getappdata(hdlFig, 'ListItems');
    rmappdata(hdlFig, 'ListItems');
end

% Fermeture de l'IHM
delete(hdlFig);
drawnow


% = Callback ===============
function cbBtnOK(hObject, event)

hdlFig        = ancestor(hObject, 'figure', 'toplevel');
nbRows         = getappdata(hdlFig, 'NbLig');
nbCol         = getappdata(hdlFig, 'NbCol');
listItems     = getappdata(hdlFig, 'ListItems');
checkBoxExist = getappdata(hdlFig, 'CheckBoxExist');
selectionMode = getappdata(hdlFig, 'SelectionMode');

noneIsSelect   = 1;
multiIsSelect  = 0;
iL2 = 1;
for iL=1:nbRows
    if isempty(listItems.nomLayers{iL})
        iL2 = iL2 + 1;
    end
    for iC=1:nbCol
        tagTable    = ['tagUiTable_Lig' num2str(iL) '_Col' num2str(iC)];
        hTabDataSet = findobj(hdlFig, 'tag', tagTable);
        pppp        = get(hTabDataSet,  'Data');
        if checkBoxExist == true
            if ~isempty(pppp)% Impact des �tats de s�lection pour la sortie.
                dummy   = [pppp{:,1}];
                noneIsSelect = all(dummy == 0) & noneIsSelect;
                if ~isempty(pppp)
                    listItems.SelectionLayers{iL2, iC} = [pppp{:,1}];
                end
            end
        else
            % R�cup�ration du composant Java de uiTable.
            %                 [flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTabDataSet);
            [flag, jTable] = uiTable_fcnGetJTableHandle(hTabDataSet);
            if ~flag
                return
            end
            rowIndex      = jTable.getSelectedRows();
            noneIsSelect  = isempty(rowIndex) & noneIsSelect;
            multiIsSelect = (numel(rowIndex) > 1) | multiIsSelect;
            pppp = zeros(numel(pppp), 1);
            pppp(rowIndex + 1) = 1;
            listItems.SelectionLayers{iL2, iC} = pppp';
        end
    end
    iL2 = iL2 + 1;
end

if noneIsSelect
    strFR = 'Veuillez s�lectionner un �l�ment ou cliquer sur Annuler';
    strUS = 'Please, select an item or choose Cancel';
    my_warndlg(Lang(strFR, strUS), 1);
    return
end
% Pour g�rer la difficult� des s�lections simples dans le cas o� on n'a
% pas de CheckBox.
if multiIsSelect && ~strcmpi(selectionMode, 'Multiple')
    strFR = 'Veuillez s�lectionner un et un seul �l�ment';
    strUS = 'Please, select only one item or choose Cancel';
    my_warndlg(Lang(strFR, strUS), 1);
    return
end    % Stimulation de la Callback de fermeture
setappdata(hdlFig, 'flagOKCancel', 1);
setappdata(hdlFig, 'ListItems', listItems);
cbCloseFcn(hdlFig, event,       listItems);


%-- cbBtnOK

% = Callback ===============
function cbBtnCancel(hObject, event)

hdlFig          = ancestor(hObject, 'figure','toplevel');
setappdata(hdlFig,'flagOKCancel', 0);

% Stimulation de la Callback de fermeture
ListItems = [];
cbCloseFcn(hdlFig, event, ListItems);
% Ecriture dans le fichier Log

% -- cbBtnCancel

% = Callback ===============
function cbCloseFcn(hObject, event, ListItems) %#ok<INUSL>

% disp('Close request function...');
setappdata(hObject,'ListItems',ListItems);
uiresume(hObject);

% cbCloseFcn

% = Callback ===============
function cb_SelectAllDatagrams(hObject, event, flagCkeckAllDatagrams)
% Recherche de la table concern�e.
hdlFig          = ancestor(hObject, 'figure','toplevel');

% Excitation des Callbacks pour tous les tables de datagrammes.
nbRows = getappdata(hdlFig, 'NbLig');
nbCol = getappdata(hdlFig, 'NbCol');
for iL=1:nbRows
    for iC=1:nbCol
        cb_checkAllDatagrams(hObject, event, flagCkeckAllDatagrams, iL, iC);
    end
end


% cb_SelectAllDatagrams

% = Callback ===============
function cb_checkAllDatagrams(hObject, event, flagCkeckAllDatagrams, indexLig, indexCol) %#ok<INUSL>

% Recherche de la table concern�e.
hdlFig          = ancestor(hObject, 'figure','toplevel');
tagTable        = ['tagUiTable_Lig' num2str(indexLig) '_Col' num2str(indexCol)];
checkBoxExist   = getappdata(hdlFig, 'CheckBoxExist');
% S�lection de tous les datagrammes.
hTabDataSet = findobj(hdlFig, 'tag', tagTable);
pppp        = get(hTabDataSet,'Data');
if flagCkeckAllDatagrams
    if checkBoxExist == true
        for i=1:size(pppp,1)
            pppp{i,1} = true;
        end
        set(hTabDataSet,'Data', pppp);
    else
        idx                 = 1:numel(pppp(:,1));
        % Positionne le focus sur les indices s�lectionn�s.
        idxFieldImageName   = 0;
        flag                = uiTable_fcnSetFocusOnIndices(hTabDataSet, idx, idxFieldImageName);
        if ~flag
            return
        end
    end
else
    if checkBoxExist == true
        for i=1:size(pppp,1)
            pppp{i,1} = false;
        end
        set(hTabDataSet,'Data', pppp);
    else
        flag = uiTable_fcnClearSelection(hTabDataSet);
        if ~flag
            return
        end
    end
end
% --- cb_checkAllDatagrams


%% == Callback =================================
% Nettoyage dans le mode Selection Single pour le cas o� il n'y a pas de
% CheckBox : usine � Gaz !!! pour contourner la non-gestion du mode Single des
% tables.
function cbCellSelectionInJTable(hObject, event, hdlFig, idxL, idxC) %#ok<INUSL>

nbRows         = getappdata(hdlFig, 'NbLig');
nbCol         = getappdata(hdlFig, 'NbCol');
checkBoxExist = getappdata(hdlFig, 'CheckBoxExist');


tagTable        = ['tagUiTable_Lig' num2str(idxL) '_Col' num2str(idxC)];
hTabDataSetCrt  = findobj(hdlFig, 'tag', tagTable);

if checkBoxExist == false
    for iL=1:nbRows
        for iC=1:nbCol
            tagTable    = ['tagUiTable_Lig' num2str(iL) '_Col' num2str(iC)];
            hTabDataSet = findobj(hdlFig, 'tag', tagTable);
            if hTabDataSet ~= hTabDataSetCrt
                flag = uiTable_fcnClearSelection(hTabDataSet); %#ok<NASGU>
            end
        end
    end
end


% Nettoyage dans le mode Selection Single.
function cbCellSelectionInTable(hObject, event)

hdlFig = ancestor(hObject, 'figure','toplevel'); %  jFig = get(hObject, 'TopLevelAncestor')
nbRows  = getappdata(hdlFig, 'NbLig');
nbCol  = getappdata(hdlFig, 'NbCol');

for iL=1:nbRows
    for iC=1:nbCol
        % S�lection de toutes les tables
        tagTable    = ['tagUiTable_Lig' num2str(iL) '_Col' num2str(iC)];
        hTabDataSet = findobj(hdlFig, 'tag', tagTable);
        % Refor�age � 0 pour tous les �l�ments except� la table
        % courante et l'item s�lectionn�.
        pppp = get(hTabDataSet,'Data');
        for i=1:size(pppp,1)
            if hTabDataSet ~= hObject
                pppp{i,1} = false;
            else
                if event.Indices(1,1) ~= i
                    pppp{i,1} = false;
                end
            end
        end
        set(hTabDataSet,'Data', pppp);
    end
end
drawnow;
