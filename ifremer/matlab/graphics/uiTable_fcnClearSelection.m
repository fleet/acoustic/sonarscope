% = Fonction ===============
function flag = uiTable_fcnClearSelection(hTable)

flag = 0;

% Récupération du composant Java de uiTable.
[flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTable);
if ~flag
    return
end
% Sauvegarde du mode de sélection de la table
selMode = jTable.getSelectionModel().getSelectionMode();

jTable.setSelectionMode(int32(2)); % ListSelectionModel.MULTIPLE_INTERVAL_SELECTION
jTable.setColumnSelectionAllowed(true);
jTable.setRowSelectionAllowed(false);
jTable.getSelectionModel().clearSelection();

% Restauration du mode de sélection de la table
jTable.setSelectionMode(int32(selMode)); % ListSelectionModel.MULTIPLE_INTERVAL_SELECTION

jTable.repaint
jScroll.repaint

flag = 1;
