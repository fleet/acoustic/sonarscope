function [flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTable)

jScroll = [];
while isempty(jScroll)
%     jScroll = findjobj_fast(UIScrollPane, handle(hTable), 'class', 'UIScrollPane');
    jScroll = findjobj(handle(hTable), 'class', 'UIScrollPane');
end

if ~isa(jScroll, 'javahandle_withcallbacks.com.mathworks.hg.peer.utils.UIScrollPane')
   return 
end

jTable = jScroll.getViewport.getView;

flag = 1;
