function flag = uiTable_fcnSetFocusOnIndices(hTable, indices, idxFieldName)

flag = 0; %#ok<NASGU>

% R�cup�ration du composant Java de uiTable.

[flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTable);
if ~flag
    return
end

% Focus sur la s�lection

% Sauvegarde du mode de s�lection de la table
% selMode = jTable.getSelectionModel().getSelectionMode();

% J = javaObject('javax.swing.DefaultListSelectionModel');
jTable.setSelectionMode(int32(2)); % J.MULTIPLE_INTERVAL_SELECTION
jTable.setColumnSelectionAllowed(true);
jTable.setRowSelectionAllowed(false);
jTable.getSelectionModel().clearSelection();

% jScroll.changeSelection :
% toggle = true/false (pour l'�tat de la cullue)%
% extend = true/false (pour s�lectionner un intervalle ou une cellule)

% pause(0.001);
pause(0.1);
for k=1:numel(indices) % les bornes sont d�j� trait�es
    % D�s�lection des �l�ments qui ne sont pas dans la liste.
    jTable.changeSelection(indices(k)-1, idxFieldName, true, false);
end

% Restauration du mode de s�lection de la table
% jTable.setSelectionMode(int32(selMode)); % ListSelectionModel.MULTIPLE_INTERVAL_SELECTION
% For�age du r�affichage de la table.
jTable.repaint;
jScroll.repaint;

flag = 1;

end % fcnSetFocusOnIndices
