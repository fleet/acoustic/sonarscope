% = Fonction ===============
function flag = uiTable_fcnSetSelectionMode(hTable, SelectionMode)

% 0 : J.SINGLE_SELECTION
% 1 : J.SINGLE_INTERVAL_SELECTION
% 2 : J.MULTIPLE_SELECTION

flag = 0;

% Récupération du composant Java de uiTable.
[flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTable);
if ~flag
    return
end

jTable.setSelectionMode(int32(SelectionMode)); 

flag = 1;