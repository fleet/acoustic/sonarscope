% Sauve une variable dans le UserData en la taguant
%
% Syntax
%   var2handle(Tag, var, h0)
%
% Input Arguments
%   Tag : Etiquette associee a la variable
%   var : Variable a sauver
%   h0  : Numero de la figure
%
% Examples
%   h0 = figure;
%   var2handle('toto', rand(3), h0)
%   x = handle2var('toto', h0)
%
% See also handle2var handle2obj obj2handle Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function var2handle(Tag, var, h0)

%% Lecture des donnees courantes

UserData = get(h0, 'UserData');

%% Ecriture des donnees

UserData = putInUserData(Tag, var, UserData);

%% Sauvegarde des donnees

set(h0, 'UserData', UserData);
