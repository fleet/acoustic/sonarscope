% Visualisation automatisee d'une variable
%
% Syntax
%   visuResultatFcn( res, nomsDim, varargin)
% 
% Input Arguments 
%   res     : Matrice resultat
%
% Name-Value Pair Arguments 
%   nomsDim     : Noms des dimensions
%   P1, P2, ... : Valeurs des axes de la matrice
%
% Examples
%   visuResultatFcn(rand(1,10))
%   visuResultatFcn(rand(1,10), {'';'yy';''}, [], 101:110)
%
%   P=0:50; F=10:10:200; V=0:10:50;
%
%   M = rand(length(P), length(F), length(V));
%   visuResultatFcn(M, {'Depth (m)';'Frequency (kHz)';'Wind speed (m/s)'}, P, F, V)
%
%   M = rand(1, length(F), length(V));
%   visuResultatFcn(M, {'Depth (m)';'Frequency (kHz)';'Wind speed (m/s)'}, P, F, V)
%
%   M = rand(length(P), 1, length(V));
%   visuResultatFcn(M, {'Depth (m)';'Frequency (kHz)';'Wind speed (m/s)'}, P, F, V)
%
%   M = rand(length(P), length(F), 1);
%   visuResultatFcn(M, {'Depth (m)';'Frequency (kHz)';'Wind speed (m/s)'}, P, F, V)
%
%   M = rand(length(P), 1, 1);
%   visuResultatFcn(M, {'Depth (m)';'Frequency (kHz)';'Wind speed (m/s)'}, P, F, V)
%
%   M = rand(1, length(F), 1);
%   visuResultatFcn(M, {'Depth (m)';'Frequency (kHz)';'Wind speed (m/s)'}, P, F, V)
%
% See also Authors
% Authors : JMA
% VERSION  : $Id: visuResultatFcn.m,v 1.2 2002/06/04 14:42:25 augustin Exp $
%-------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   11/09/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function visuResultatFcn(M, varargin)

switch nargin
case 0
    help visuResultatFcn
    return
case 1
    P = {'X';'Y';'Z'};
    X = 1:size(M,1);
    Y = 1:size(M,2);
    Z = 1:size(M,3);
case 2
    P = varargin{1};
    X = 1:size(M,1);
    Y = 1:size(M,2);
    Z = 1:size(M,3);
otherwise
    P = varargin{1};
    switch nargin
    case 2
        X = 1:size(M,1);
        Y = 1:size(M,2);
        Z = 1:size(M,3);
   case 3  % X transmis
        X = varargin{2};
        Y = 1:size(M,2);
        Z = 1:size(M,3);
    case 4  % X et Y transmis
        X = varargin{2};
        Y = varargin{3};
        Z = 1:size(M,3);
    case 5  % X et Y transmis
        X = varargin{2};
        Y = varargin{3};
        Z = varargin{4};
    end
end


sz = size(M);
if(length(sz) == 2)
    sz(3) = 1;
end

if(sz(1) == 1)
    if(sz(2) == 1)
        if(sz(3) == 1)
            % 1 1 1
        else
            % 1 1 n
            figure;
            plot(Z, squeeze(M)); grid on;
            ylabel(P{3});
        end
    else
        if(sz(3) == 1)
            % 1 m 1
            figure;
            plot(Y, M); grid on;
            ylabel(P{2});
        else
            % 1 m n
            figure;
            imagesc(Y, Z, squeeze(M)); axis xy;
            xlabel(P{2}); ylabel(P{3});
        end
        
    end
else
    if(sz(2) == 1)
        if(sz(3) == 1)
            % p 1 1
            figure;
            plot(X, M); grid on;
            ylabel(P{1});
        else
            % p 1 n
            figure
            imagesc(X, Z, squeeze(M)); axis xy;
            xlabel(P{1}); ylabel(P{3});
        end
    else
        if(sz(3) == 1)
            % p m 1
            figure
            imagesc(X, Y, M); axis xy;
            xlabel(P{1}); ylabel(P{2});
        else
            % p m n
            figure
            p = sz(3);
            n = floor(sqrt(p));
            m = ceil(p / n);
            for i=1:sz(3)
                subplot(n, m, i);
                imagesc(X, Y, M(:,:,i)); axis xy;
                xlabel(P{1}); ylabel(P{2}); title(sprintf('%s = %f', P{3}, Z(i)))
            end
        end
        
    end
end