% Carre de l'excentricite de l'ellipsoide
%
% Syntax
%   E2 = carre_excentricite(a)
%
% Input Arguments
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   E2 : Carre de l'excentricite de l'ellipsoide
%
% Examples
%   a = cl_carto('Ellipsoide.Type', 11)
%   E2 = carre_excentricite(a)
%
% See also cl_carto cl_carto/aplatissement cl_carto/demiPetitAxe Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function E2 = carre_excentricite(this)
E2 = this.MapTbx.Geoid.Eccentricity ^ 2;
