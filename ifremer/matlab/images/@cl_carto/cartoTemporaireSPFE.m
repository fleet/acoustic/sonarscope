% TODO : supprimer quand le pb sera r�solu : action GLU
function Carto = cartoTemporaireSPFE(Carto)

% Debut ajout JMA en attente correction de cl_carto
strProjUTM = 'WGS 84 / UTM zone 31N';
[~, structEPSG, ~] = getParamsProjFromESRIFile('CsLabelName', strProjUTM);
x = strfind(structEPSG.strProj4, '+zone=');
if ~isempty(x)
    zoneUTM = str2double(structEPSG.strProj4(x+6:x+7));
    Carto = set(Carto, 'Projection.UTM.Fuseau', zoneUTM);
end

x = strfind(structEPSG.strProj4, 'south');
if isempty(x)
    Carto = set(Carto, 'Projection.UTM.Hemisphere', 'N');
else
    Carto = set(Carto, 'Projection.UTM.Hemisphere', 'S');
end

% Codage via EPSG de la projection UTM
Carto = set(Carto, 'CodeEPSG', structEPSG.code);
Carto = set(Carto, 'Proj4',    structEPSG.strProj4);

% Doit �tre r�alis� apr�s les lignes pr�c�dentes !!!
if isempty(x)
    Carto = set(Carto, 'Projection.UTM.Y0', 0);
else
    Carto = set(Carto, 'Projection.UTM.Y0', 10000000);
end
% Fin ajout JMA en attente correction de cl_carto
