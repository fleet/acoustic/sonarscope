% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_carto
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   a = cl_carto;
%   str = char(a)
%
% See also cl_carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);

else
    if (m*n) == 0
        str = '';
        return
    end

    % ---------------
    % Instance unique

    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
