% Constructeur de cl_carto
%
% Syntax
%   a = cl_carto(...)
% 
% Name-Value Pair Arguments
%   Titre    : Nom de l'image
%  
% Output Arguments 
%   a : instance de cl_carto
%
% Examples
%     a = cl_carto([])
%     a = cl_carto
%     a = set(a, 'Ellipsoide.Type', 2)
%
%     lon = -4.4378;
%     lat = 48.4172;   
%     %% Mercator
%         NewCarto_OOP1 = cl_carto( 'Ellipsoide.Type', 11, ...
%                                     'Projection.Type', 2, ...);
%                                     'Projection.Mercator.lat_ech_cons', lat, ...
%                                     'Projection.Mercator.long_merid_orig', lon);
% 
%         % Positionnement obligatoire des Lat. Echelle conserv�e et M�ridien
%         % d'origine.
%         NewCarto_OOP2 = cl_carto( 'CodeEPSG', '54004', ...
%                                     'Projection.Mercator.lat_ech_cons', lat, ...
%                                     'Projection.Mercator.long_merid_orig', lon);
% 
%     %% Lambert
%     codeEPSG    = {'2154'; '27561'; '27562'; '27563'; '27564'; '27571'; '27572'; '27573'; '27574'};
%     for k=1:9
%         if k==1
%             typeEllipsoid = 12; % Exception pour Lambert 93
%         else
%             typeEllipsoid = 14; % Ellipsoide Clarke80-ign (7012) TODO : faux, 7012 = clarke80 et non pas clarke80-IGN=NTF
%         end
%         NewCarto_OOP1 = cl_carto( 'Ellipsoide.Type', typeEllipsoid, ...
%                                     'Projection.Type', 4, ...
%                                     'Projection.Lambert.Type', k);
% 
%         NewCarto_OOP2 = cl_carto( 'CodeEPSG', codeEPSG{k});
%     end
%
%     %% UTM
%         zone       = utmzone(lat,lon);
%         fuseau     = str2double(zone(1:2));
%         if lat >= 0
%             falsenorthing       = 0;
%             falseeasting        = 500000;
%             hemisphere          = 'N';
%         else
%             falseeasting        = 500000;
%             falsenorthing       = 10000000;
%             hemisphere          = 'S';
%         end
%         % Anciennes propri�t�s
%        NewCarto_OOP1  = cl_carto('Ellipsoide.Type', 11, ...
%             'Projection.Type', 3, ...
%             'Projection.UTM.Fuseau', fuseau, ...
%             'Projection.UTM.X0', falseeasting, ...
%             'Projection.UTM.Y0', falsenorthing);
% 
%         % Avec Code EPSG
%         strProjUTM = ['WGS 84 / UTM zone ' zone(1:2) hemisphere];
%         [flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CsLabelName', strProjUTM);
%         CodeEPSG   = structEPSG.code;
%         NewCarto_OOP2 = cl_carto('CodeEPSG', CodeEPSG);
%
% See also cl_carto/imagesc Authors
% Authors : GLU
% ----------------------------------------------------------------------------

% Pour info : Clarke80 IGN (NTF) et l'ellipsoide grs80 (RGF93) TODO : supprimer les redondances

classdef cl_carto < handle
    
    %% Propri�t�s
    properties (Constant)
        % Liste de projections UTM autoris�es (UTM - wgs84)
        numCodeUTM = 32601:32760;
        strCodeUTM = strsplit(num2str(cl_carto.numCodeUTM));
    end
    
    %% Propri�t�s
    properties (Hidden = true, Constant = true)
        MapTbxEllipsoidTypeList         = { 'unitsphere'; 'wgs72'; 'wgs66'; 'international'; 'clarke66'; 
                                            'clarke80'; 'everest';  'bessel'; 'krasovsky'; 'iau65'; 'wgs84'; 
                                            'grs80'; 'NTF'; 'clarke80_IGN'; 'other'};
                                        
        MapTbxEllipsoidCodeList         = { '--'; '7043'; '--'; '7022'; '7008'; '7012'; '7015';  
                                            '7004'; '7024'; '--'; '7030'; '7019'; '--'; '7011'; 'other'};
                                        
        MapTbxProjectionTypeList        = { 'Geodetic-4326'; 'Mercator-54004'; 'UTM-32xxx'; 
                                            'Lambert'; 'Polar Stereo-54026'; 'Equi. Cyl.-54002'; 'other'};
                                        
        MapTbxProjectionCodeList        = {'4326'; '54004'; '--'; '--'; '54026'; '54002'; 'other'};
        
        MapTbxProjectionLambertCodeList = { '2154'; '27561'; '27562'; '27563'; '27564';
                                            '27571'; '27572'; '27573'; '27574'; 'other'};
        
        EllipsoidTypeList           = {'Undefined'; 'wgs72'; 'wgs66'; 'international' ; 'clarke66'; 'clarke80'; 'everest';  'bessel'; 'krasovsky'; 'iau65'; 'wgs84'; 'grs80'; 'NTF'; 'clarke80_IGN'; 'other'};
        
        ProjectionTypeList          = {'Geodetic'; 'Mercator'; 'UTM'; 'Lambert'; 'Stereographique polaire'; 'Cylyndrique Equidistant'; 'Other'};
        
        ProjectionLambertTypeList   = { 'Lambert 93';
                                        'France 1'; 'France 2'; 'France 3'; 'France 4'
                                        'France 1 Etendu'; 'France 2 Etendu'; 'France 3 Etendu'; 'France 4 Etendu'
                                        'Europe';  'Monde';  'Other Secant';  'Other Tangent'};
    end
    
    properties (GetAccess='public', SetAccess='private')
        %% Propri�t�s de l'ancienne classe cl_carto.
        
        Ellipsoide  = struct(   'Type',             11, ...
                                'strType',          {cl_carto.EllipsoidTypeList}, ...
                                'tabDemiGrandAxe',  [], ...
                                'tabExcentricite',  [], ...
                                'DemiGrandAxe',     0, ...
                                'Excentricite',     0);
        
        Projection  = struct(   'Type',        	1, ...
                                'strType',      {cl_carto.ProjectionTypeList}, ...    
                                'UTM',          struct( 'Fuseau',           1, ...
                                                        'Central_meridian', (1-31)*6, ...
                                                        'Central_parallel', 0, ...
                                                        'Hemisphere',       'N', ...
                                                        'X0',               0, ...
                                                        'Y0',               0), ...
                                'Mercator',     struct( 'long_merid_orig',  0, ...
                                                        'lat_ech_cons',     0, ...
                                                        'X0',               0, ...
                                                        'Y0',               0), ...
                                'Lambert',      struct( 'Type', 1, ...
                                                        'strType', {cl_carto.ProjectionLambertTypeList}, ...
                                                        'first_paral', 0, ...
                                                        'second_paral', 0, ...
                                                        'long_merid_orig', 0, ...
                                                        'lat_ech_cons', 0, ...
                                                        'X0', 0, ...
                                                        'Y0', 0, ...
                                                        'scale_factor', 0), ...
                                'Stereo',       struct( 'long_merid_horiz', 1, ...
                                                        'lat_ech_cons', 0, ...
                                                        'hemisphere',   'N'), ...
                                'CylEquiDist',  struct( 'long_merid_horiz', 1, ...
                                                        'lat_ech_cons', 0));
        
        %% Propri�t�s de la nouvelle classe cl_carto.
        % Par d�faut, Ellipsoide = wgs84
        
        MapTbx = struct('Ellipsoide', struct( 'CodeEPSG', '7030', ...
                                                'strType', {cl_carto.MapTbxEllipsoidTypeList},...
                                                'strCode', {cl_carto.MapTbxEllipsoidCodeList}), ...
                        'MStruct',    [],   ...
                        'Projection', struct( 'CodeEPSG', '4326', ...
                                                'strType', {cl_carto.MapTbxProjectionTypeList},...
                                                'strCode', {cl_carto.MapTbxProjectionCodeList}, ...
                                                'DeltaToWGS84', struct( 'DX', 0, 'DY', 0, 'DZ', 0, ...
                                                                        'RX', 0, 'RY', 0, 'RZ', 0, ...
                                                                        'M', 0), ...
                        'Proj4',   '', ...
                        'Lambert', struct( 'strType', 'lambertstd', ...
                                           'strCode', {cl_carto.MapTbxProjectionLambertCodeList}), ...
                        'UTM',     struct( 'strCode', {''})), ...
                        'Geoid',   referenceEllipsoid(7030)); % wgs84Ellipsoid n'existe pas en R2011b
    end
    
    %% M�thodes
    
    methods(Access='public')
        
        function obj = cl_carto(varargin)            
            [varargin, MStruct] = getPropertyValue(varargin, 'MStruct', []);
            if isempty(MStruct)
                % Cr�ation d'une structure par d�faut de type Mercator.
                % Affectation � la classe les param�tres de projection de la Mapping Toolbox.
                obj.MapTbx.MStruct = defaultm('mercator');
                obj.MapTbx.MStruct.origin = [0 0];
            else
                obj.MapTbx.MStruct = MStruct;
                [~, CodeEPSG] = convertMstruct2EPSG('MStruct', MStruct);
                obj = set(obj, 'CodeEPSG', CodeEPSG);
            end
            
            % Les listes de projection UTM possibles sont d�fini�es en
            % propri�t�s constantes.
            obj.MapTbx.Projection.UTM.strCode = cl_carto.strCodeUTM;
            
            %% Initialisation des Demi-GrandAxe et Eccentricit� des anciennes propri�t�s.
            
            n = numel(obj.MapTbx.Ellipsoide.strType);
            persistent A
            persistent E
            
            if isempty(A)
                A = zeros(1,n);
                E = zeros(1,n);
                for k=1:n
                    refEllipsoide = obj.MapTbx.Ellipsoide.strType{k};
                    refEllipsoide = lower(refEllipsoide);
                    if k == 1
                        A(k) = 0;
                        E(k) = 0;
                    else
                        switch refEllipsoide
                            case lower('clarke80_ign')
                                refEllipsoide = obj.MapTbx.Ellipsoide.strCode{k};
                                if ~strcmp(refEllipsoide, '--')
                                    refEllipsoide = str2double(refEllipsoide);
                                    refEllipsoide = referenceEllipsoid(refEllipsoide, 'meters');
                                end
                                % Affectation des propri�t�s du Geoide.
                                A(k) = refEllipsoide.SemimajorAxis;
                                E(k) = refEllipsoide.Eccentricity;

                            case lower('NTF')
                                A(k) = 6378137;
                                E(k) = sqrt(0.0066943800229);

                            case lower('other')
                                A(k) = 6378249.2;
                                E(k) = 0.08248325676;

                            otherwise
                                refEllipsoide = referenceEllipsoid(refEllipsoide, 'meters');
                                refEllipsoide = lower(refEllipsoide);
                                % Affectation des propri�t�s du Geoide.
                                A(k) = refEllipsoide.SemimajorAxis;
                                E(k) = refEllipsoide.Eccentricity;
                        end
                    end
                end    
            end
            obj.Ellipsoide.tabDemiGrandAxe = A;
            obj.Ellipsoide.tabExcentricite = E;
                        
            if isempty(varargin) || isempty(varargin{1})
                return
            end

            % Test si construction sans initialisation -> on sort
            if nargin > 0
                obj = set(obj, varargin{:});
            end
        end
    end 
end
