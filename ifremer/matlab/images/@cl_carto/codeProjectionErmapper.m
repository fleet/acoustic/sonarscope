% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   Code = codeProjectionErmapper(a)
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   Code : Code de projection correspondante dans ermapper
% 
% Examples
%   a = cl_carto('Ellipsoide.Type', 2, 'Projection.Type', 3)
%   Code = codeProjectionErmapper(a)
%
%   a = cl_carto('Ellipsoide.Type', 2, 'Projection.Type', 2, 'Projection.Mercator.lat_ech_cons', 48)
%   Code = codeProjectionErmapper(a)
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Code = codeProjectionErmapper(this)

Code = [];
switch this.Projection.Type
    case 1  % Unknown
    case 2  % Mercator
        LatEch = this.Projection.Mercator.lat_ech_cons;
        if LatEch >= 0
            Hemisphere = 'N';
        else
            Hemisphere = 'S';
        end
        NomEllipsoide = this.Ellipsoide.strType{this.Ellipsoide.Type};
        Code = sprintf('MR%5.2f%s_%s', abs(LatEch), Hemisphere, NomEllipsoide);
        
        % {
        % TODO : Peut-on commenter cela ?
        % On compl�te la d�finition des projections mercator dans les
        % fichiers de d�finition d'ErViewver et d'ErMapper
        write_mercator_ermapperFiles(this, Code);
        % }
        
    case 3  % UTM
        Code = [this.Projection.UTM.Hemisphere 'UTM' num2str(this.Projection.UTM.Fuseau)];
    case 4  % Lambert
        this.Projection.Lambert;
        Code = 'LM1FRA1D';
    case 5  % Lamcon
        my_warndlg('Lamcon projection not available in cl_carto/codeProjectionErmapper', 1);
    case 6  % Stereographique polaire
        my_warndlg('Stereographique polaire projection not available in cl_carto/codeProjectionErmapper', 1);
    otherwise
        my_warndlg('Projection not available in cl_carto/codeProjectionErmapper', 1);
end
