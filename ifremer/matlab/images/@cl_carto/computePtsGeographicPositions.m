function [flag, Lat, Lon, XGeo, YGeo] = computePtsGeographicPositions(this, FishLatitude, FishLongitude, FishHeading, AcrossD, AlongD)

flag = 0;
Lat  = [];
Lon  = [];
XGeo = [];
YGeo = [];

if isempty(FishHeading) || all(isnan(FishHeading(:)))
    if isdeployed
        str1 = '"Heading" non renseigné dans cette image';
        str2 = '"Heading" not assigned in this image';
        my_warndlg(Lang(str1,str2), 1);
        return
    else
        my_warndlg('sonarCalculCoordGeo : Heading non renseigné, on calcule le cap seulement si version de dév', 0);
        FishHeading = calCapFromLatLon(FishLatitude, FishLongitude);
    end
end

E2 = get(this, 'Ellipsoide.Excentricite') ^ 2;
A  = get(this, 'Ellipsoide.DemiGrandAxe');
[Lat, Lon] = calculLatLonFromAlongAcross(E2, A, FishLatitude, FishLongitude, FishHeading, AlongD, AcrossD);
[XGeo, YGeo] = latlon2xy(this, Lat, Lon);

flag = 1;
