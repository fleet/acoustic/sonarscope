% convertGeotiffParams2SSCParams : converiosn des pram�tres Geo d'un fichier Geotiff en instance Carto.
%
%
% Syntax
%   Carto = convertGeotiffParams2SSCParams(Carto, info)
% 
% Input Arguments 
%   a           : Une instance de la classe cl_carto
%   info        : Structure de metadata d'une image Geotiff
%
% Name-Value Pair Arguments
%  Cf. cl_carto
%
% Output Arguments
%   Carto : instance Carto modifiee
% 
% Examples
%   a       = cl_carto;
%   info    = geotiffinfo('boston.tif');
%   a       = convertGeotiffParams2SSCParams(a, info);
%
% See also cl_carto cl_carto/get Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function Carto = convertGeotiffParams2SSCParams(Carto, info)

%% Cartographie de l'�quivalence SSC-table Geotiffs des param�tres d'une image
% GLU : le 15/10/2012 if (info.SemiMajor == 0) || (info.SemiMinor == 0)
% Pb d�tect� � partir de la version R2012a pour r�cup�rer une image GeoTIFF
% de carte du SHOM : 6966_pal300.tif
if isempty(info.SemiMinor)
    % R�cup�ration du param�tre 2 mais peut-�tre peut-on r�cup�rer les
    % infos depuis EPSG de l'ellipsoide
    APL            = info.GeoTIFFTags.GeoDoubleParamsTag(7);
    info.SemiMinor = info.SemiMajor * (APL-1) / APL;
end

if info.SemiMajor == 0
    %     numEllipsoide = 11;
    %     return
else
    numEllipsoide = ident_ellipsoideGaPa(Carto, info.SemiMajor, info.SemiMinor);
    Carto         = set(Carto, 'Ellipsoide.Type', numEllipsoide);
end

% projMercatorEquivalence = { 'Projection.Lambert.first_paral'        {'ProjStdParallel1GeoKey', 'ProjNatOriginLatGeoKey'}; ...
%     'Projection.Lambert.long_merid_orig'    'ProjNatOriginLongGeoKey'; ...
%     'Projection.Lambert.X0'                 'ProjFalseEastingGeoKey'; ...
%     'Projection.Lambert.Y0'                 'ProjFalseNorthingGeoKey'};

projLambertEquivalence = {...
    'Projection.Lambert.first_paral'     {'ProjStdParallel1GeoKey', 'ProjNatOriginLatGeoKey'}; ...
    'Projection.Lambert.second_paral'    'ProjStdParallel1GeoKey'; ...
    'Projection.Lambert.long_merid_orig' 'ProjNatOriginLongGeoKey'; ...
    'Projection.Lambert.X0'              'ProjFalseEastingGeoKey'; ...
    'Projection.Lambert.Y0'              'ProjFalseNorthingGeoKey'; ...
    'Projection.Lambert.scale_factor'    'ProjScaleAtNatOriginGeoKey'};

projMercatorEquivalence = {...
    'Projection.Mercator.lat_ech_cons'      'ProjNatOriginLatGeoKey'; ...
    'Projection.Mercator.long_merid_orig'   'ProjNatOriginLongGeoKey'; ...
    'Projection.Mercator.X0'                'ProjFalseEastingGeoKey';...
    'Projection.Mercator.Y0'                'ProjFalseNorthingGeoKey'};

% TODO : UTM
listProjParamId = info.ProjParmId;
if any(ismember(info.CTProjection, {'CT_LambertConfConic_1SP', 'CT_LambertConfConic_2SP', 'CT_Mercator'}))
    switch info.CTProjection
        case {'CT_LambertConfConic_1SP', 'CT_LambertConfConic_2SP'}
            projParamEquivalence = projLambertEquivalence;
            
        case 'CT_Mercator'
            projParamEquivalence = projMercatorEquivalence;
          
        otherwise
            fprintf('==> TODO : Traitement g�n�rique des projection Param UTM Geotiffs � r�aliser : %s\n', mfilename);
    end
    
    nParams = size(projParamEquivalence, 1);
    % Initialisations du tableau des index de param.
    identParam(nParams) = 0;
    for k=1:nParams
        pppp = find(ismember(listProjParamId, projParamEquivalence{k,2}));
        if ~isempty(pppp)
            identParam(k) = pppp;
        end
    end
    % Traitement sp�cifique du cas du mode tangent de Lambert
    if identParam(2) == 0
        identParam(2) = identParam(1);
    end
end

%%

if ~isempty(info.CTProjection)
    switch info.CTProjection
        case 'CT_LambertConfConic_1SP'
            try
                Carto = set(Carto, 'Projection.Type',     4, ...
                    'Projection.Lambert.Type',            13, ...
                    'Projection.Lambert.first_paral',     info.ProjParm(identParam(1)), ...
                    'Projection.Lambert.second_paral',    info.ProjParm(identParam(2)), ...
                    'Projection.Lambert.long_merid_orig', info.ProjParm(identParam(3)), ...
                    'Projection.Lambert.X0',              info.ProjParm(identParam(4)), ...
                    'Projection.Lambert.Y0',              info.ProjParm(identParam(5)), ...
                    'Projection.Lambert.scale_factor',    info.ProjParm(identParam(6)));
            catch % Modif JMA le 04/04/2018 pour fichier D:\Temp\Boyard_32bits_GeoYX_Bathymetry.tif
                Carto = set(Carto, 'Projection.Type',     4, ...
                    'Projection.Lambert.Type',            13, ...
                    'Projection.Lambert.first_paral',     info.ProjParm(identParam(1)), ...
                    'Projection.Lambert.second_paral',    info.ProjParm(identParam(2)), ...
                    'Projection.Lambert.X0',              info.ProjParm(identParam(4)), ...
                    'Projection.Lambert.Y0',              info.ProjParm(identParam(5)));
            end
            
        case 'CT_LambertConfConic_2SP'
            try
                Carto = set(Carto, 'Projection.Type',     4, ...
                    'Projection.Lambert.Type',            12, ...
                    'Projection.Lambert.first_paral',     info.ProjParm(identParam(1)), ...
                    'Projection.Lambert.second_paral',    info.ProjParm(identParam(2)), ...
                    'Projection.Lambert.long_merid_orig', info.ProjParm(identParam(3)), ...
                    'Projection.Lambert.X0',              info.ProjParm(identParam(4)), ...
                    'Projection.Lambert.Y0',              info.ProjParm(identParam(5)));
            catch % Modif JMA le 04/04/2018 pour fichier D:\Temp\Boyard_32bits_GeoYX_Bathymetry.tif
                Carto = set(Carto, 'Projection.Type',     4, ...
                    'Projection.Lambert.Type',            12, ...
                    'Projection.Lambert.first_paral',     info.ProjParm(identParam(1)), ...
                    'Projection.Lambert.second_paral',    info.ProjParm(identParam(2)), ...
                    'Projection.Lambert.X0',              info.ProjParm(identParam(4)), ...
                    'Projection.Lambert.Y0',              info.ProjParm(identParam(5)));
            end
            
        case 'CT_Mercator'
            latEchCons = info.ProjParm(2);
            % Cas du fichier MAYOTTE : MAY1_100m_DEPTH.tif
            if isfield(info.GeoTIFFTags.GeoKeyDirectoryTag, 'ProjStdParallel1GeoKey')
                if ~isempty(info.GeoTIFFTags.GeoKeyDirectoryTag.ProjStdParallel1GeoKey)
                    latEchCons = info.GeoTIFFTags.GeoKeyDirectoryTag.ProjStdParallel1GeoKey;
                end
            end
            Carto = set(Carto, 'Projection.Type',      2,...
                'Projection.Mercator.long_merid_orig', info.ProjParm(2), ...
                'Projection.Mercator.lat_ech_cons',    latEchCons, ...
                'Projection.Mercator.Y0',              info.ProjParm(7), ...
                'Projection.Mercator.X0',              info.ProjParm(6));
            
        case 'CT_TransverseMercator'
            % NB : la projection est confirm�e par le champ
            % info.projection ou  info.PCS
            if info.ProjParm(1) >= 0
                Hemi = 'N';
            else
                Hemi = 'S';
            end
            Carto = set(Carto, 'Projection.Type', 3, ...
                'Projection.UTM.Fuseau',          round((info.ProjParm(2)+180)/6), ...
                'Projection.UTM.Hemisphere',      Hemi, ...
                'Projection.UTM.Y0',              info.ProjParm(7), ...
                'Projection.UTM.X0',              info.ProjParm(6));
            
        otherwise
            str = sprintf('Projection %s not done in function "import_geotiff".', info.CTProjection);
            my_warndlg(str, 1);
    end
end
