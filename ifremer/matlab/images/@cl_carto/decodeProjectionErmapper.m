% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   Code = codeProjectionErmapper(a)
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   Code : Code de projection correspondante dans ermapper
% 
% Examples
%   a = cl_carto('Ellipsoide.Type', 2, 'Projection.Type', 3)
%   Code = codeProjectionErmapper(a)
%
%   a = cl_carto('Ellipsoide.Type', 2, 'Projection.Type', 2, 'Projection.Mercator.lat_ech_cons', 48)
%   Code = codeProjectionErmapper(a)
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Projection = decodeProjectionErmapper(this, Code)

if (length(Code) >= 5) && strcmp(Code(2:4), 'UTM') % UTM
    Projection.Type = 3; % UTM
    Projection.Parametres.UTM.Fuseau = str2double(Code(5:end));
    Projection.Parametres.UTM.Hemisphere = Code(1);

    if strcmp(Projection.Parametres.UTM.Hemisphere, 'N')
        Projection.Parametres.UTM.Y0 = 0;
        Projection.Parametres.UTM.X0 = 500000;
    elseif strcmp(Projection.Parametres.UTM.Hemisphere, 'S')
        Projection.Parametres.UTM.Y0 = 500000;
        Projection.Parametres.UTM.X0 = 10000000;
    else
        Projection.Parametres.UTM.Y0 = 0;
        Projection.Parametres.UTM.X0 = 0;
    end
    
elseif isequal(Code, 'MR0S') % Mercator
    Projection.Type = 2; % Mercator
    Projection.Parametres.Mercator.lat_ech_cons = 0;
    
    E  = this.Ellipsoide.Excentricite;
    E2 = E ^ 2;
    L0   = Projection.Parametres.Mercator.lat_ech_cons  * (pi/180);
    SL   = sin(L0) ^ 2;
    NORM = sqrt((1 - E2*SL) / (1-SL));
                
    Projection.Parametres.Mercator.false_north    = 0;
    Projection.Parametres.Mercator.false_east     = 0;
    Projection.Parametres.Mercator.scale_factor   = 1 / NORM;
    Projection.Parametres.Mercator.centre_merid   = 0;

    Projection.Parametres.Mercator.radius         = 0;
    Projection.Parametres.Mercator.eccentricity   = 0;
    Projection.Parametres.Mercator.flattening     = 0;
    Projection.Parametres.Mercator.prime_merid    = 1;
    
elseif length(Code) >= 8 && strcmp(Code(1:2), 'MR') % Mercator
    Projection.Type = 2; % Mercator
    Projection.Parametres.Mercator.lat_ech_cons = str2double(Code(3:7));
    
    NomEllipsoide = this.Ellipsoide.strType{this.Ellipsoide.Type};
    if ~strcmp(NomEllipsoide, Code(10:end))
        str = sprintf('The ellipsoid defined for %s does not correspond to %s', Code, NomEllipsoide);
        my_warndlg(str, 0, 'Tag', 'BugDecodageEllipsoid');
    end
    
    E  = this.Ellipsoide.Excentricite;
    E2 = E ^ 2;
    L0   = Projection.Parametres.Mercator.lat_ech_cons  * (pi/180);
    SL   = sin(L0) ^ 2;
    NORM = sqrt((1 - E2*SL) / (1-SL));
                
    Projection.Parametres.Mercator.false_north    = 0;
    Projection.Parametres.Mercator.false_east     = 0;
    Projection.Parametres.Mercator.scale_factor   = 1 / NORM;
    Projection.Parametres.Mercator.centre_merid   = 0;

    Projection.Parametres.Mercator.radius         = 0;
    Projection.Parametres.Mercator.eccentricity   = 0;
    Projection.Parametres.Mercator.flattening     = 0;
    Projection.Parametres.Mercator.prime_merid    = 1;
    
elseif strcmp(Code, 'LM1FRA1D') % Non v�rifi�
    Projection.Type = 4; % Lambert
    Projection.Parametres.Lambert.Type          = 6; % France 1 Etentendu
  	Projection.Parametres.Lambert.false_north   = 1200000.000000;
  	Projection.Parametres.Lambert.false_east    = 600000.000000;
  	Projection.Parametres.Lambert.scale_factor  = 0.999877340;
  	Projection.Parametres.Lambert.centre_merid  = 0.040792;
  	Projection.Parametres.Lambert.centre_lat    = 0.863938;
  	Projection.Parametres.Lambert.radius        = 6378249.200000;
  	Projection.Parametres.Lambert.eccentricity  = 0.082483;
  	Projection.Parametres.Lambert.flattening    = 293.466021;
  	Projection.Parametres.Lambert.prime_merid   = 1.000000;
        
elseif strcmp(Code, 'LM1FRE1D') % Pas V�rifi� : rencontr� dans fichier BMO
%     LM1FRE1D,lambert1,1.0 ,6378249.2 ,0.0824832567625238,293.4660213 ,1  
    Projection.Type = 4; % Lambert
    Projection.Parametres.Lambert.Type          = 2; % France 1
  	Projection.Parametres.Lambert.false_north   = 200000.000000;
  	Projection.Parametres.Lambert.false_east    = 600000.000000;
  	Projection.Parametres.Lambert.scale_factor  = 0.999877340;
  	Projection.Parametres.Lambert.centre_merid  = 0.040792;
  	Projection.Parametres.Lambert.centre_lat    = 0.863938;
  	Projection.Parametres.Lambert.radius        = 6378249.200000;
  	Projection.Parametres.Lambert.eccentricity  = 0.082483;
  	Projection.Parametres.Lambert.flattening    = 293.466021;
  	Projection.Parametres.Lambert.prime_merid   = 1.000000;

else
    my_warndlg([Code ' not interpreted in cl_carto/decodeProjectionErmapper'], 1);
    
    % On d�finit une projection par d�faut pour que �a ne plante pas
    Projection.Type = 3; % UTM
    Projection.Parametres.UTM.Fuseau = 31;
    Projection.Parametres.UTM.Hemisphere = 'N';
    Projection.Parametres.UTM.Y0 = 0;
    Projection.Parametres.UTM.X0 = 500000;
end
return

% Code = [];
% switch this.Projection.Type
%     case 1  % Unknown
%     case 4  % Lambert
%     case 5  % Lamcon
%     case 6  % Stereographique polaire
%     otherwise
% end
