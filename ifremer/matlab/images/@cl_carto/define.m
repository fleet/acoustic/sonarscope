function [flag, this] = define(this, nomDir, LatMean, LonMean, varargin)

[varargin, NoGeodetic] = getPropertyValue(varargin, 'NoGeodetic', 0); %#ok<ASGLU>

nomFic = fullfile(nomDir, '.def');
str1 = 'D�finition des param�tres cartographiques';
str2 = 'Cartographic parametres ?';
[rep, flag] = my_questdlg(Lang(str1,str2), ...
    Lang('Manuellement', 'Manual'), Lang('Importation depuis un fichier', 'Import'));
if ~flag
    this = [];
    return
end
if rep == 2
    liste = listeFicOnDir(nomDir, '.def');
    if isempty(liste)
        nomDirUp = fileparts(nomDir);
        liste = listeFicOnDir(nomDirUp, '.def');
        if ~isempty(liste)
            nomFic = fullfile(nomDirUp, '*.def');
        end
    end
    if length(liste) == 1
        Filtre = liste{1};
    else
        Filtre = '';
    end
    [flag, nomFic] = my_uigetfile(nomFic, Lang('Nom du fichier de cartographie', 'Give a cartographic file name'), Filtre);
    if flag
        this = import_xml(cl_carto([]), nomFic);
        if isempty(this)
            
            % Au cas o� le fichier soit � l'ancienne m�thode
            
            this = import_Carto_MapTbx_OOP(cl_carto([]), nomFic);
            flag = export_xml(this, nomFic);
            if flag
                %                 delete(nomFic)
            end
        end
        % type(nomFic)
    else
        %         flag = 0;
        %         this = [];
        %         return
        rep = 1;
    end
end

if rep == 1
    if isempty(this)
        this = cl_carto([]);
    end
    
    [flag, this] = editParams(this, round(LatMean), round(LonMean), 'MsgInit', 'NoGeodetic', NoGeodetic);
    if ~flag
        return
    end
    
    %% Sauvergarde des parametres cartographiques dans un fichier ASCI
    
    str1 = 'Voulez-vous sauver les param�tres cartographiques dans un fichier ?';
    str2 = 'Save the cartographic parameters in a file ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        this = [];
        return
    end
    if rep == 1
        str1 = 'Nom du fichier de d�finition de la carto';
        str2 = 'Give a file name';
        [flag, nomFic] = my_uiputfile('*.def', Lang(str1,str2), ...
            fullfile(nomDir, 'Carto_Directory.def'));
        if ~flag
            this = [];
            return
        end
        flag = export(this, nomFic);
        % edit(nomFic)
    end
end
