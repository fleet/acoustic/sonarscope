% Demi petit axe de l'ellipsoide
%
% Syntax
%   B = demiPetitAxe(a)
%
% Input Arguments
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   B : Demi peit axe de l'ellipsoide
%
% Examples
%   a = cl_carto('Ellipsoide.Type', 11)
%   B = demiPetitAxe(a)
%
% See also cl_carto cl_carto/carre_excentricite cl_carto/aplatissement Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function B = demiPetitAxe(this)
E2  = this.Ellipsoide.Excentricite ^ 2;
B   = this.Ellipsoide.DemiGrandAxe * sqrt(1 - E2);
