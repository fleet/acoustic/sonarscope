% Representation externe d'une instance sur le terminal
%
% Syntax
%   display_Carto_MapTbx_OOP(a)
%
% Input Arguments 
%   a : instance de cl_carto
%
% Examples
%   a = cl_carto;
%   a
%
% See also cl_carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)
    
% --------------------------------------------
% Lecture et affichage des infos de l'instance

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf(1,'%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
