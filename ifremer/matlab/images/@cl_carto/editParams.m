% Selection de la projection.
%
% Syntax
%   Type = editParams(a, LatMean, LonMean)
%
% Input Arguments
%   a : Une instance de la classe cl_carto
%   LatMean : [] ou Latitude moyenne
%   LonMean : [] ou Longitude moyenne
%
% Output Arguments
%   Type : Identifiant de l'ellipsoide  '2=Mercator' | '3=UTM' | %   '4=Lambert1' | '5=Lambert2' | '6=Lambert3' |
%   '7=Lambert4' | '8=Stereographique polaire' | '9=Other'
%
% Examples
%   [flag, a] = editParams(cl_carto([]), 40, 29)
%   [flag, a] = editParams(a, 40, 29)
%
% See also cl_carto cl_carto/demiPetitAxe cl_carto/carre_excentricite Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [flag, this] = editParams(this, LatMean, LonMean, varargin)

[varargin, NoGeodetic] = getPropertyValue(varargin, 'NoGeodetic', 0);

[varargin, MsgInit] = getFlag(varargin, 'MsgInit'); %#ok<ASGLU>

if MsgInit
    str1 = 'SonarScope va vous demander de d�finir la cartographie. Faites-le SVP m�me si vous n''en avez a priori pas besoin.';
    str2 = 'SonarScope is asking you to define cartographic parameters at this step, please define them even if you do not need them now, they will be inherited from image to image.';
    %     [rep, flag] = my_txtdlg(Lang('ATTENTION', 'WARNING'), Lang(str1,str2));
    %     if ~flag
    %         return
    %     end
    my_warndlg(Lang(str1,str2), 1);
end

[Type, flag] = selectEllipsoide(this);
if ~flag
    return
end
this = set(this, 'Ellipsoide.Type', Type);

[flag, Type, sEPSG, typeProjLambert] = selectProjection(this, LonMean, 'NoGeodetic', NoGeodetic);
if ~flag
    return
end

switch Type
    case 2 % Mercator
        if isempty(LonMean)
            LonMean = 0;
        else
            % D�but encart sp�cial pour compatibilit� avec ErMapper
            LonMean = floor(LonMean * 100) / 100;
            % Fin encart sp�cial pour compatibilit� avec ErMapper
        end
        
        if isempty(LatMean)
            LatMean = 0;
        else
            % D�but encart sp�cial pour compatibilit� avec ErMapper
            LatMean = floor(LatMean * 100) / 100;
            % Fin encart sp�cial pour compatibilit� avec ErMapper
        end
        [long_merid_orig, lat_ech_cons, X0, Y0, flag] = selectProjectionMercator(this, LatMean, LonMean);
        if ~flag
            return
        end
        CodeEPSG = '54004';
        [flag, sEPSG, ~] = getParamsProjFromESRIFile('CodeEPSG', CodeEPSG);
        % Customisation de la chaine Proj4.
        sEPSG.strProj4   = regexprep(sEPSG.strProj4, '+lat_ts=0', sprintf('+lat_ts=%5.2f', lat_ech_cons));
        sEPSG.strProj4   = regexprep(sEPSG.strProj4, '+lon_0=0', sprintf('+lon_0=%5.2f', long_merid_orig));
        this = set(this, 'CodeEPSG', CodeEPSG);
        this = set(this, 'Proj4', sEPSG.strProj4);
        this = set(this, 'Projection.Mercator.long_merid_orig', long_merid_orig);
        this = set(this, 'Projection.Mercator.lat_ech_cons',    lat_ech_cons);
        this = set(this, 'Projection.Mercator.X0',              X0);
        this = set(this, 'Projection.Mercator.Y0',              Y0);
        
    case 3 % UTM
        x = strfind(sEPSG.strProj4, '+zone=');
        if ~isempty(x)
            zoneUTM = str2double(sEPSG.strProj4(x+6:x+7));
            this = set(this, 'Projection.UTM.Fuseau', zoneUTM);
        end
        
        x = strfind(sEPSG.strProj4, 'south');
        if isempty(x)
            this = set(this, 'Projection.UTM.Hemisphere', 'N');
        else
            this = set(this, 'Projection.UTM.Hemisphere', 'S');
        end
        
        % Codage via EPSG de la projection UTM
        this = set(this, 'CodeEPSG', sEPSG.code);
        this = set(this, 'Proj4',    sEPSG.strProj4);
        
        % Doit �tre r�alis� apr�s les lignes pr�c�dentes !!!
        if isempty(x)
            this = set(this, 'Projection.UTM.Y0', 0);
        else
            this = set(this, 'Projection.UTM.Y0', 10000000);
        end
        
    case 4 % Lambert
        % Codage via EPSG de la projection UTM
        % % %         this = set(this, 'CodeEPSG', sEPSG.code);
        % % %         this = set(this, 'Proj4', sEPSG.strProj4);
        % % %         this = set(this, 'Projection.Lambert.Type', typeProjLambert); % Rajout� par JMA le 27/04/2016
        
        this = set(this, 'CodeEPSG', sEPSG.code, 'Proj4', sEPSG.strProj4, ...
            'Projection.Lambert.Type', typeProjLambert); % Rajout� par JMA le 27/04/2016
        
    case 5 % Stereographique polaire
        
    otherwise
end
