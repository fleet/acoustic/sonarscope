%  	NewCarto = cl_carto( 'Projection.CodeEPSG', '54004', ...
%                                 'Projection.Mercator.lat_ech_cons', 48.5, ...
%                                 'Projection.Mercator.long_merid_orig', -4.73);
%
%   NewCarto = cl_carto( 'Projection.CodeEPSG', '32630'); % UTM Zone30N
%
%   editProperties(NewCarto);

function [Parent, this] = editProperties(this, varargin)

[varargin, Parent]      = getPropertyValue(varargin, 'Parent',      []);
[varargin, WindowStyle] = getPropertyValue(varargin, 'WindowStyle', 'normal');
[varargin, ObjectLabel] = getPropertyValue(varargin, 'Label',       []); %#ok<ASGLU>

com.mathworks.mwswing.MJUtilities.initJIDE;

JavaClassChar = javaclass('char', 1);

if isempty(Parent)
    Parent = java.util.ArrayList();
    Fig = true;
else
    Fig = false;
end

for k=1:length(this)
    p1 = com.jidesoft.grid.DefaultProperty();
    if isempty(ObjectLabel)
        p1.setName(['cl_carto(' num2str(k) ')']);
    else
        p1.setName(['cl_carto(' num2str(k) ') - ' ObjectLabel]);
    end
    p1.setEditable(false);
    
    if Fig
        Parent.add(p1);
    else
        Parent.addChild(p1);
    end
    
    %% Ellipsoide
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName(['Ellipsoid        (EPSG Code : '  this(k).MapTbx.Ellipsoide.CodeEPSG  ' -' this(k).Ellipsoide.strType{this(k).Ellipsoide.Type} ')'] );
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Ellipsoide.Type
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoide.Type');
    % p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).Ellipsoide.Type) ' (' this(k).Ellipsoide.strType{this(k).Ellipsoide.Type} ')']);
    p3.setCategory('Carto properties');
    p3.setDisplayName('Ellipsoide.Type');
    p3.setDescription('Type of data : wgs66, clarke80, ..., ....');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options0 = this(k).Ellipsoide.strType;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext('comboboxeditor_0');
    com.jidesoft.grid.CellEditorManager.registerEditor(JavaClassChar, editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoide.strType');
    % p3.setType(javaclass('char',1));
    p3.setValue(this(k).Ellipsoide.strType{this(k).Ellipsoide.Type});
    p3.setCategory('Carto properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('Ellipsoide.strType');
    p3.setDescription('Liste of types of data : Reflectivity, Bathymetry, TxAngle, ....');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Ellipsoide.DemiGrandAxe
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoide.DemiGrandAxe');
    %     p3.setType(javaclass('double',1));
    %     p3.setValue(this(k).Ellipsoide.DemiGrandAxe);
    % p3.setType(javaclass('char',1));
    p3.setValue(num2strPrecis(this(k).MapTbx.Geoid.SemimajorAxis));
    p3.setCategory('Carto properties');
    p3.setDisplayName('Ellipsoide.DemiGrandAxe');
    p3.setDescription('Demi grand Axe of the ellipsoid.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Ellipsoide.DemiPetitAxe
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoide.DemiPetitAxe');
    %     p3.setType(javaclass('double',1));
    %     p3.setValue(demiPetitAxe(this(k)));
    % p3.setType(javaclass('char',1));
    p3.setValue(num2strPrecis(this(k).MapTbx.Geoid.SemiminorAxis));
    p3.setCategory('Carto properties');
    p3.setDisplayName('Ellipsoide.DemiPetitAxe');
    p3.setDescription('Demi small Axe of the ellipsoid.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Ellipsoide.Excentricite
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoide.Excentricite');
    %     p3.setType(javaclass('double',1));
    %     p3.setValue(this(k).Ellipsoide.Excentricite);
    % p3.setType(javaclass('char',1));
    p3.setValue(num2strPrecis(this(k).MapTbx.Geoid.Eccentricity));
    p3.setCategory('Carto properties');
    p3.setDisplayName('Ellipsoide.Excentricite');
    p3.setDescription('Excentricity of the ellipsoid.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Ellipsoide.E2
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoide.E2');
    %     p3.setType(javaclass('double',1));
    %     p3.setValue(carre_excentricite(this(k)));
    % p3.setType(javaclass('char',1));
    p3.setValue(num2strPrecis(carre_excentricite(this(k))));
    p3.setCategory('Carto properties');
    p3.setDisplayName('Ellipsoide.E2');
    p3.setDescription('Square of the excentricity of the ellipsoid.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Ellipsoide.Aplatissement
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoide.Aplatissement');
    %     p3.setType(javaclass('double',1));
    %     p3.setValue(aplatissement(this(k)));
    % p3.setType(javaclass('char',1));
    p3.setValue(num2strPrecis(ecc2flat(this(k).MapTbx.Geoid.Eccentricity)));
    p3.setCategory('Carto properties');
    p3.setDisplayName('Ellipsoide.Aplatissement');
    p3.setDescription('Flatering value of the ellipsoid.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Projection
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName(['Projection     (EPSG Code : '  this(k).MapTbx.Projection.CodeEPSG  ' -' this(k).Projection.strType{this(k).Projection.Type} ')']);
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Projection.Type
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Projection.Type');
    % p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).Projection.Type) ' (' this(k).Projection.strType{this(k).Projection.Type} ')']);
    p3.setCategory('Carto properties');
    p3.setDisplayName('Projection.Type');
    p3.setDescription('Type of data : Reflectivity, Bathymetry, TxAngle, ....');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options1 = this(k).Projection.strType;
    editor1  = com.jidesoft.grid.ListComboBoxCellEditor(options1);
    context1 = com.jidesoft.grid.EditorContext('comboboxeditor_1');
    com.jidesoft.grid.CellEditorManager.registerEditor(JavaClassChar, editor1, context1);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Projection.strType');
    % p3.setType(javaclass('char',1));
    p3.setValue(this(k).Projection.strType{this(k).Projection.Type});
    p3.setCategory('Carto properties');
    p3.setEditorContext(context1);
    p3.setDisplayName('Projection.strType');
    p3.setDescription('Liste of types of data : Reflectivity, Bathymetry, TxAngle, ....');
    p3.setEditable(true);
    p2.addChild(p3);
    
    
    switch this(k).Projection.Type
        case 1
        case 2
            
            %% Mercator
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName('Mercator');
            p3.setEditable(false);
            p2.addChild(p3);
            
            %% Projection.Mercator.long_merid_orig
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Mercator.long_merid_orig');
            %             % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Mercator.long_merid_orig);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Mercator.long_merid_orig');
            p4.setDescription('Mercator origin meridian.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Mercator.lat_ech_cons
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Mercator.lat_ech_cons');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Mercator.lat_ech_cons);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Mercator.lat_ech_cons');
            p4.setDescription('Mercator latitude of conserved scale.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Mercator.Y0
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Mercator.Y0');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Mercator.Y0);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Mercator.Y0');
            p4.setDescription('Mercator Y0.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Mercator.X0
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Mercator.X0');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Mercator.X0);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Mercator.X0');
            p4.setDescription('Mercator X0.');
            p4.setEditable(true);
            p3.addChild(p4);
            
        case 3
            %% UTM
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName('UTM');
            p3.setEditable(false);
            p2.addChild(p3);
            
            %% Projection.UTM.Fuseau
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.UTM.Fuseau');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.UTM.Fuseau);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.UTM.Fuseau');
            p4.setDescription('UTM zone number.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.UTM.Longitude
            if this.Projection.UTM.Fuseau<= 60
                Longitude(1) = (this(k).Projection.UTM.Fuseau - 31) * 6;
            else
                Longitude(1) = (this(k).Projection.UTM.Fuseau - 60 - 31) * 6 - 3;
            end
            Longitude(2) = Longitude(1) + 6;
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.UTM.Longitude');
            % p4.setType(javaclass('char',1));
            p4.setValue(num2strCode(Longitude));
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.UTM.Longitude');
            p4.setDescription('UTM longitude limites.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.UTM.Hemisphere
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.UTM.Hemisphere');
            % p4.setType(javaclass('char',1));
            p4.setValue(this(k).Projection.UTM.Hemisphere);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.UTM.Hemisphere');
            p4.setDescription('UTM hemisphere.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.UTM.Y0
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.UTM.Y0');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.UTM.Y0);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.UTM.Y0');
            p4.setDescription('UTM Y0.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.UTM.X0
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.UTM.X0');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.UTM.X0);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.UTM.X0');
            p4.setDescription('UTM X0.');
            p4.setEditable(true);
            p3.addChild(p4);
            
        case {4; 6; 7; 8}
            %% Lambert
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName('Lambert');
            p3.setEditable(false);
            p2.addChild(p3);
            
            %% Projection.Lambert.Type
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.Type');
            % p4.setType(javaclass('char',1));
            p4.setValue([num2str(this(k).Projection.Lambert.Type) ' (' this(k).Projection.Lambert.strType{this(k).Projection.Lambert.Type} ')']);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.Type');
            p4.setDescription('Type of data : Reflectivity, Bathymetry, TxAngle, ....');
            p4.setEditable(false);
            p3.addChild(p4);
            
            options2 = this(k).Projection.Lambert.strType;
            editor2  = com.jidesoft.grid.ListComboBoxCellEditor(options2);
            context2 = com.jidesoft.grid.EditorContext('comboboxeditor_2');
            com.jidesoft.grid.CellEditorManager.registerEditor(JavaClassChar, editor2, context2);
            
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.strType');
            % p4.setType(javaclass('char',1));
            p4.setValue(this(k).Projection.Lambert.strType{this(k).Projection.Lambert.Type});
            p4.setCategory('Carto properties');
            p4.setEditorContext(context2);
            p4.setDisplayName('Projection.Lambert.strType');
            p4.setDescription('Liste of types of data : Reflectivity, Bathymetry, TxAngle, ....');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Lambert.first_paral
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.first_paral');
            %             % p4.setType(javaclass('double',1));
            %             p4.setValue(this(k).Projection.Lambert.first_paral);
            % p4.setType(javaclass('char',1));
            p4.setValue(num2strPrecis(this(k).Projection.Lambert.first_paral));
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.first_paral');
            p4.setDescription('Lambert first parralel.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Lambert.second_paral
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.second_paral');
            %             % p4.setType(javaclass('double',1));
            %             p4.setValue(this(k).Projection.Lambert.second_paral);
            % p4.setType(javaclass('char',1));
            p4.setValue(num2strPrecis(this(k).Projection.Lambert.second_paral));
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.second_paral');
            p4.setDescription('Lambert second parallel.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Lambert.lat_ech_cons
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.lat_ech_cons');
            p4.setValue(num2strPrecis(this(k).Projection.Lambert.lat_ech_cons));
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.lat_ech_cons');
            p4.setDescription('Lambert Latitude of true scale');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Lambert.long_merid_orig
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.long_merid_orig');
            %             % p4.setType(javaclass('double',1));
            %             p4.setValue(this(k).Projection.Lambert.long_merid_orig);
            % p4.setType(javaclass('char',1));
            p4.setValue(num2strPrecis(this(k).Projection.Lambert.long_merid_orig));
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.long_merid_orig');
            p4.setDescription('Lambert original meridian.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Lambert.X0)
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.X0)');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Lambert.X0);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.X0');
            p4.setDescription('Lambert X0.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Lambert.Y0
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.Y0');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Lambert.Y0);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.Y0');
            p4.setDescription('Lambert Y0.');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Lambert.scale_factor
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Lambert.scale_factor');
            %             % p4.setType(javaclass('double',1));
            %             p4.setValue(this(k).Projection.Lambert.scale_factor);
            % p4.setType(javaclass('char',1));
            p4.setValue(num2strPrecis(this(k).Projection.Lambert.scale_factor));
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Lambert.scale_factor');
            p4.setDescription('Lambert scale factor.');
            p4.setEditable(true);
            p3.addChild(p4);
            
        case 5
            %% UTM
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName('Stereographic polaire');
            p3.setEditable(false);
            p2.addChild(p3);
            
            %% Projection.Stereo.long_merid_horiz
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Stereo.long_merid_horiz');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Stereo.long_merid_horiz);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Stereo.long_merid_horiz');
            p4.setDescription('Stereo graphi polar .....');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Stereo.lat_ech_cons
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Stereo.lat_ech_cons');
            % p4.setType(javaclass('double',1));
            p4.setValue(this(k).Projection.Stereo.lat_ech_cons);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Stereo.lat_ech_cons');
            p4.setDescription('Stereo graphi polar .....');
            p4.setEditable(true);
            p3.addChild(p4);
            
            %% Projection.Stereo.hemisphere
            p4 = com.jidesoft.grid.DefaultProperty();
            p4.setName('Projection.Stereo.hemisphere');
            % p4.setType(javaclass('char',1));
            p4.setValue(this(k).Projection.Stereo.hemisphere);
            p4.setCategory('Carto properties');
            p4.setDisplayName('Projection.Stereo.hemisphere');
            p4.setDescription('Stereo graphi polar .....');
            p4.setEditable(true);
            p3.addChild(p4);
    end
    
    editProperties_MapTbx(this, 'Parent', p1);
end


%% Figure
if Fig
    %% Prepare a properties table containing the Parent
    model = com.jidesoft.grid.PropertyTableModel(Parent);
    % model.expandAll();
    model.expandFirstLevel();
    model.expandNextLevel();
    model.expandNextLevel();
    
    % Cablage de la Callback de changement des attributs.
    hModel = handle(model, 'CallbackProperties');
    set(hModel, 'PropertyChangeCallback', {@callback_onPropertyChange, this});
    
    grid = com.jidesoft.grid.PropertyTable(model);
    hPanelJava = com.jidesoft.grid.PropertyPane(grid);
    
    %% Display the properties hPanelJava onscreen
    hFig = figure;
    set(hFig, 'WindowStyle', WindowStyle);
    set(hFig, 'MenuBar', 'none');
    set(hFig, 'ToolBar', 'none');
    Position = get(hFig, 'Position');
    hPanelMatlab = uipanel(hFig);
    javacomponent(hPanelJava, [5 5 Position(3:4)*0.98], hPanelMatlab);
    
    % Set the resize callback function of uihPanelMatlab container
    set(hPanelMatlab, 'ResizeFcn', {@UihPanelMatlabResizeCallback, hPanelMatlab, hPanelJava});
    
    % Wait for figure window to close & display the prop value
    % uiwait(hFig);
    % disp(prop(1).getValue())
    
end

%% Callback function for resize behavior of the uihPanelMatlab container
function UihPanelMatlabResizeCallback(hObject, eventdata, handles, hMainPanel, varargin) %#ok<INUSD,INUSL>
h           = get(handles, 'Children');
hFig        = get(handles, 'Parent');
Position    = get(hFig, 'Position');
set(h, 'Position', [5 5 Position(3:4)*0.98]);

function callback_onPropertyChange(model, event, object)
%Identification de la propri�t� en cours de modification.
string   = event.getNewValue();
[value, isvalid] = str2num(string); %#ok
propName = event.getPropertyName();
prop     = model.getProperty(propName);
prop.setValue(string);

switch propName.toCharArray'
    case 'cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.strType'
        % Recherche de l'indice correspondant � la nouvelle Ellipsoide.
        sub             = strcmp(object.MapTbx.Ellipsoide.strCode,string);
        pppp            = (1:numel(object.MapTbx.Ellipsoide.strCode));
        indexCodeEllipsoid   = pppp(sub);
        
        object.MapTbx.Ellipsoide.Type = indexCodeEllipsoid;
        
        % Mise � jour des propri�t�s li�es � la projection.
        flag = fcnRefreshEllipsoidProperties(model, object, indexCodeEllipsoid);
        if ~flag
            return
        end
        
    case 'cl_carto(1).Mapping Toolbox - NEW.Projection.strType'
        % Recherche de l'indice correspondant � la nouvelle Ellipsoide.
        sub             = strcmp(object.MapTbx.Projection.strType,string);
        pppp            = (1:numel(object.MapTbx.Projection.strType));
        codeProjection  = pppp(sub);
        
        % Recherche de l'indice correspondant � la nouvelle Projection.
        [flag, Type, sEPSG] = selectProjection(object, object.MapTbx.MStruct.origin(2), ...
            'NoGeodetic', 0, ...
            'InitialProjection', codeProjection); %#ok<ASGLU>
        if ~flag || isempty(sEPSG.projName)
            return
        end
        
        pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
        set(object,'CodeEPSG', sEPSG.code);
        
        % Mise � jour des propri�t�s li�es � la projection.
        flag = fcnRefreshProjectionProperties(model, object);
        if ~flag
            return
        end
        
        % D�termination du code EPSG de l'ellipsoide.
        [flag, CodeEPSGEllipsoid] = identEllipsoidFromProjection(sEPSG.code);
        sub  = strcmp(object.MapTbx.Ellipsoide.strCode,CodeEPSGEllipsoid);
        pppp = (1:numel(object.MapTbx.Ellipsoide.strCode));
        indexCodeEllipsoid = pppp(sub);
        % For�age de la mise � jour des propri�t�s li�es � l'ellipsoide.
        flag = fcnRefreshEllipsoidProperties(model, object, indexCodeEllipsoid);
        if ~flag
            return
        end
        
    otherwise
        disp('R�gler les autres attributs de chaque Ellipsoide/Projection');
end
model.refresh();  % refresh value onscreen


function flag = fcnRefreshEllipsoidProperties(model, object, codeEllipsoid)
flag = 0;

try
    % Propri�t�s d�pendantes impact�es
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.CodeEPSG'));
    prop.setValue([num2str(codeEllipsoid) ' (' object.MapTbx.Ellipsoide.strCode{codeEllipsoid} ')']);
    
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.Type'));
    prop.setValue([num2str(codeEllipsoid) ' (' object.MapTbx.Ellipsoide.strType{codeEllipsoid} ')']);
    
    geoid = referenceEllipsoid(str2num(object.MapTbx.Ellipsoide.strCode{codeEllipsoid}));
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.DemiGrandAxe'));
    prop.setValue(num2str(geoid.SemimajorAxis));
    
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.DemiPetitAxe'));
    prop.setValue(num2str(geoid.SemiminorAxis));
    
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.Excentricite'));
    prop.setValue(num2str(geoid.Eccentricity));
    
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.E2'));
    prop.setValue(num2str(geoid.Eccentricity ^ 2));
    
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Ellipsoid.Aplatissement'));
    prop.setValue(num2str(1/ecc2flat(geoid.Eccentricity)));
    
catch ME %#ok<NASGU>
    return
end
flag = 1;

function flag = fcnRefreshProjectionProperties(model, object)

flag = 0;
try
    
    typeProj    = object.Projection.Type;
    CodeEPSG    = object.MapTbx.Projection.CodeEPSG;
    Proj4       = object.MapTbx.Projection.Proj4;
    % Propri�t�s d�pendantes impact�es
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.CodeEPSG'));
    prop.setValue(CodeEPSG);
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.Type'));
    prop.setValue([num2str(typeProj) ' (' char(object.Projection.strType(typeProj)) '-' CodeEPSG ')']);
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.Proj4'));
    prop.setValue(Proj4);
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.DeltaToWGS84.DeltaX'));
    prop.setValue(num2str(object.MapTbx.Projection.DeltaToWGS84.DX));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.DeltaToWGS84.DeltaY'));
    prop.setValue(num2str(object.MapTbx.Projection.DeltaToWGS84.DY));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.DeltaToWGS84.DeltaZ'));
    prop.setValue(num2str(object.MapTbx.Projection.DeltaToWGS84.DZ));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.DeltaToWGS84.RotationX'));
    prop.setValue(num2str(object.MapTbx.Projection.DeltaToWGS84.RX));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.DeltaToWGS84.RotationY'));
    prop.setValue(num2str(object.MapTbx.Projection.DeltaToWGS84.RY));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.DeltaToWGS84.RotationZ'));
    prop.setValue(num2str(object.MapTbx.Projection.DeltaToWGS84.RZ));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.DeltaToWGS84.M'));
    prop.setValue(num2str(object.MapTbx.Projection.DeltaToWGS84.M));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.MStruct.MapProjection'));
    prop.setValue(object.MapTbx.MStruct.mapprojection);
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.MStruct.Origin'));
    prop.setValue(num2str(object.MapTbx.MStruct.origin));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.MStruct.Origin'));
    prop.setValue(num2str(object.MapTbx.MStruct.origin));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.MStruct.mapparallels'));
    prop.setValue(num2str(object.MapTbx.MStruct.mapparallels));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.MStruct.nparallels'));
    prop.setValue(num2str(object.MapTbx.MStruct.nparallels));
    
    pause(0.05); % N�cessaire pour �viter le plantage critique de MatLab
    prop = model.getProperty(java.lang.String('cl_carto(1).Mapping Toolbox - NEW.Projection.MStruct.ScaleFactor'));
    prop.setValue(num2str(object.MapTbx.MStruct.scalefactor));
    
catch ME
    return
end
flag = 1;

