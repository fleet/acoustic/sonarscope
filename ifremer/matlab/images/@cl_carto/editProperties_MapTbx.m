function [Parent, this] = editProperties_MapTbx(this, varargin)

[varargin, Parent] = getPropertyValue(varargin, 'Parent', []); %#ok<ASGLU>
% [varargin, WindowStyle] = getPropertyValue(varargin, 'WindowStyle', 'normal');

com.mathworks.mwswing.MJUtilities.initJIDE;

ClassJavaChar = javaclass('char', 1);

if isempty(Parent)
    Parent = java.util.ArrayList();
    Fig = true;
else
    Fig = false;
end

for k=1:length(this)
    
    %% Mapping Toolbox
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Mapping Toolbox - NEW');
    p2.setEditable(false);
    
    if Fig
        Parent.add(p2);
    else
        Parent.addChild(p2);
    end
    
    %% Mapping Toolbox-Ellipsoide
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ellipsoid');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% MapTbx.Ellipsoide.Type
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('Type');
    %     % p4.setType(ClassJavaChar);
    p4.setValue([num2str(this(k).Ellipsoide.Type) ' (' this(k).MapTbx.Ellipsoide.strType{this(k).Ellipsoide.Type} ')']);
    p4.setCategory('Carto properties');
    p4.setDisplayName('Type');
    p4.setDescription('Type of data : 11 (wgs84), wgs66, ....');
    p4.setEditable(false);
    p3.addChild(p4);
    
    
    %% MapTbx.Ellipsoide.CodeEPSG
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('CodeEPSG');
    % p4.setType(ClassJavaChar);
    p4.setValue([num2str(this(k).Ellipsoide.Type) ' (' this(k).MapTbx.Ellipsoide.strCode{this(k).Ellipsoide.Type} ')']);
    p4.setCategory('Carto properties');
    p4.setDisplayName('CodeEPSG');
    p4.setDescription('Type of data : 7030 (wgs94), 7011 (clarke80), ....');
    p4.setEditable(false);
    p3.addChild(p4);
    
    options0 = this(k).MapTbx.Ellipsoide.strCode;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext('comboboxeditor_0');
    com.jidesoft.grid.CellEditorManager.registerEditor(ClassJavaChar, editor0, context0);
    
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('strType');
    % p4.setType(ClassJavaChar);
    p4.setValue( [this(k).MapTbx.Ellipsoide.strCode{this(k).Ellipsoide.Type} '-' this(k).MapTbx.Ellipsoide.strType{this(k).Ellipsoide.Type}]);
    p4.setCategory('Carto properties');
    p4.setEditorContext(context0);
    p4.setDisplayName('strType');
    p4.setDescription('Liste of types of data : 7030, 7011, ....');
    p4.setEditable(true);
    p3.addChild(p4);
    
    %% Ellipsoide.DemiGrandAxe
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('DemiGrandAxe');
    % p4.setType(ClassJavaChar);
    p4.setValue(num2strPrecis(this(k).MapTbx.Geoid.SemimajorAxis));
    p4.setCategory('Carto properties');
    p4.setDisplayName('DemiGrandAxe');
    p4.setDescription('Demi grand Axe of the ellipsoid.');
    p4.setEditable(true);
    p3.addChild(p4);
    
    %% DemiPetitAxe
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('DemiPetitAxe');
    % p4.setType(ClassJavaChar);
    p4.setValue(num2strPrecis(this(k).MapTbx.Geoid.SemiminorAxis));
    p4.setCategory('Carto properties');
    p4.setDisplayName('DemiPetitAxe');
    p4.setDescription('Demi small Axe of the ellipsoid.');
    p4.setEditable(true);
    p3.addChild(p4);
    
    %% Excentricite
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('Excentricite');
    % p4.setType(ClassJavaChar);
    p4.setValue(num2strPrecis(this(k).MapTbx.Geoid.Eccentricity));
    p4.setCategory('Carto properties');
    p4.setDisplayName('Excentricite');
    p4.setDescription('Excentricity of the ellipsoid.');
    p4.setEditable(true);
    p3.addChild(p4);
    
    %% E2
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('E2');
    % p4.setType(ClassJavaChar);
    p4.setValue(num2strPrecis(carre_excentricite(this(k))));
    p4.setCategory('Carto properties');
    p4.setDisplayName('E2');
    p4.setDescription('Sqare of the excentricity of the ellipsoid.');
    p4.setEditable(true);
    p3.addChild(p4);
    
    %% Aplatissement
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('Aplatissement');
    % p4.setType(ClassJavaChar);
    p4.setValue(num2strPrecis(ecc2flat(this(k).MapTbx.Geoid.Eccentricity)));
    p4.setCategory('Carto properties');
    p4.setDisplayName('Aplatissement');
    p4.setDescription('Flatering value of the ellipsoid.');
    p4.setEditable(true);
    p3.addChild(p4);
    
    %% Mapping Toolbox-Projection
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Projection');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Projection.Type
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('Type');
    % p4.setType(ClassJavaChar);
    p4.setValue([num2str(this(k).Projection.Type) ' (' this(k).MapTbx.Projection.CodeEPSG ')']);
    p4.setCategory('Carto properties');
    p4.setDisplayName('Type');
    p4.setDescription('Type of data : Geodetic, Mercator, ....');
    p4.setEditable(false);
    p3.addChild(p4);
    
    options1 = this(k).MapTbx.Projection.strType;
    editor1  = com.jidesoft.grid.ListComboBoxCellEditor(options1);
    context1 = com.jidesoft.grid.EditorContext('comboboxeditor_1');
    com.jidesoft.grid.CellEditorManager.registerEditor(ClassJavaChar, editor1, context1);
    
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('CodeEPSG');
    % p4.setType(ClassJavaChar);
    p4.setValue(this(k).MapTbx.Projection.CodeEPSG);
    p4.setCategory('Carto properties');
    p4.setDisplayName('CodeEPSG');
    p4.setDescription('Liste of types of data : 4326, 27561, ....');
    p4.setEditable(false);
    p3.addChild(p4);
    
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('strType');
    % p4.setType(ClassJavaChar);
    p4.setValue(this(k).MapTbx.Projection.strType{this(k).Projection.Type});
    p4.setCategory('Carto properties');
    p4.setEditorContext(context1);
    p4.setDisplayName('strType');
    p4.setDescription('Liste of types of data : Geodetic, Mercator, ....');
    p4.setEditable(true);
    p3.addChild(p4);
    
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('Proj4');
    % p4.setType(ClassJavaChar);
    p4.setValue(this(k).MapTbx.Projection.Proj4);
    p4.setCategory('Carto properties');
    p4.setDisplayName('Proj4');
    p4.setDescription('Liste of types of data : string for PROJ4 description....');
    p4.setEditable(false);
    p3.addChild(p4);
    
    %% Projection.DeltaToWGS84
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('DeltaToWGS84');
    p4.setEditable(false);
    p3.addChild(p4);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('DeltaX');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.Projection.DeltaToWGS84.DX);
    p5.setCategory('Carto properties');
    p5.setDisplayName('DeltaXToWGS84');
    p5.setDescription('Liste of types of data : delta for translation between Ellipsoids....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('DeltaY');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.Projection.DeltaToWGS84.DY);
    p5.setCategory('Carto properties');
    p5.setDisplayName('DeltaYToWGS84');
    p5.setDescription('Liste of types of data : delta for translation between Ellipsoids....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('DeltaZ');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.Projection.DeltaToWGS84.DZ);
    p5.setCategory('Carto properties');
    p5.setDisplayName('DeltaZToWGS84');
    p5.setDescription('Liste of types of data : delta for translation between Ellipsoids....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('RotationX');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.Projection.DeltaToWGS84.RX);
    p5.setCategory('Carto properties');
    p5.setDisplayName('RotXToWGS84');
    p5.setDescription('Liste of types of data : angles for rotation between Ellipsoids, ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('RotationY');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.Projection.DeltaToWGS84.RY);
    p5.setCategory('Carto properties');
    p5.setDisplayName('RotYToWGS84');
    p5.setDescription('Liste of types of data : angles for rotation between Ellipsoids, ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('RotationZ');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.Projection.DeltaToWGS84.RZ);
    p5.setCategory('Carto properties');
    p5.setDisplayName('RotZToWGS84');
    p5.setDescription('Liste of types of data : angles for rotation between Ellipsoids, ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('M');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.Projection.DeltaToWGS84.RZ);
    p5.setCategory('Carto properties');
    p5.setDisplayName('M');
    p5.setDescription('Liste of types of data : scaling for rotation, translation ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    %% Projection.Mstruct
    p4 = com.jidesoft.grid.DefaultProperty();
    p4.setName('MStruct');
    p4.setEditable(false);
    p3.addChild(p4);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('MapProjection');
    p5.setType(ClassJavaChar);
    p5.setValue(this(k).MapTbx.MStruct.mapprojection);
    p5.setCategory('Carto properties');
    p5.setDisplayName('MapProjection');
    p5.setDescription('Liste of types of data : projection for mstruct in MatLab Mapping Toolbox ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('Origin');
    p5.setType(ClassJavaChar);
    p5.setValue(num2str(this(k).MapTbx.MStruct.origin));
    p5.setCategory('Carto properties');
    p5.setDisplayName('Origin');
    p5.setDescription('Liste of types of data : origine for mstruct in MatLab Mapping Toolbox ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('mapparallels');
    p5.setType(ClassJavaChar);
    p5.setValue(num2str(this(k).MapTbx.MStruct.mapparallels));
    p5.setCategory('Carto properties');
    p5.setDisplayName('mapparallels');
    p5.setDescription('Liste of types of data : parallels for mstruct in MatLab Mapping Toolbox ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('nparallels');
    p5.setType(ClassJavaChar);
    p5.setValue(num2str(this(k).MapTbx.MStruct.nparallels));
    p5.setCategory('Carto properties');
    p5.setDisplayName('nparallels');
    p5.setDescription('Liste of types of data : nb of parallels for mstruct in MatLab Mapping Toolbox ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
    p5 = com.jidesoft.grid.DefaultProperty();
    p5.setName('ScaleFactor');
    p5.setType(ClassJavaChar);
    p5.setValue(num2str(this(k).MapTbx.MStruct.scalefactor));
    p5.setCategory('Carto properties');
    p5.setDisplayName('ScaleFactor');
    p5.setDescription('Liste of types of data : scale factor mstruct in MatLab Mapping Toolbox ....');
    p5.setEditable(false);
    p4.addChild(p5);
    
end



