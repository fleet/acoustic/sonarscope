% Exportation des parametres cartographiques dans un fichier ascii
%
% Syntax 
%   status = export(a, nomFic)
%
% Input Arguments
%   a      : Instance de cl_carto
%   nomFic : Nom du fichier
%
% Output Arguments
%   status : 1 (succesfull) ou 0 (failure)
%
% Examples
%   a = cl_carto('Projection.Type', 4);
%   nomFic =  tempname
%   status = export(a, nomFic)
%   edit(nomFic)
%
% See also cl_carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export(this, nomFic)
    
str = char(this);
fid = fopen(nomFic, 'w+t');
if fid == -1
    flag = 0;
    return
end

for i=1:size(str, 1)
    fprintf(fid, '%s\n', str(i,:));
end
fclose(fid);

%% Exportation en XML

flag = export_xml(this, nomFic);

