% Exportation de l'objet au format XML
%
% Syntax
%   flag = export_xml(a, nomFic)
%
% Input Arguments
%   a      : Instance de la classe cl_carto
%   nomFic : Nom du fichier xml
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Remarks : La fonction rajoute (ou modifie) automatiquement l'extension du fichier en  ".xml"
%
% Examples
%   a = cl_carto('Ellipsoide.Type', 2, 'Projection.Type', 3, 'Projection.UTM.Fuseau', 30)
%   ou
%   a = cl_carto( 'CodeEPSG', 32630);
%   nomFic = my_tempname('.xml')
%   flag = export_xml(a, nomFic)
%
% See also cl_carto/import_xml xml_write  Authors
% Authors : JMA
%-------------------------------------------------------------------------------

 function flag = export_xml(this, nomFic)

[nomDir, nomFic] = fileparts(nomFic);
nomFic  = fullfile(nomDir, [nomFic '.xml']);
% Restructuration de la sous-structure geoid.
warning off MATLAB:structOnObject
pppp = struct(this);
pppp = rmfield(pppp, 'MapTbxEllipsoidTypeList');
pppp = rmfield(pppp, 'MapTbxEllipsoidCodeList');
pppp = rmfield(pppp, 'MapTbxProjectionTypeList');
pppp = rmfield(pppp, 'MapTbxProjectionCodeList');
pppp = rmfield(pppp, 'MapTbxProjectionLambertCodeList');
pppp = rmfield(pppp, 'EllipsoidTypeList');
pppp = rmfield(pppp, 'ProjectionTypeList');
pppp = rmfield(pppp, 'ProjectionLambertTypeList');

pppp = rmfield(pppp, 'numCodeUTM');
pppp = rmfield(pppp, 'strCodeUTM');
if isfield(pppp.MapTbx.Projection, 'UTM')
    pppp.MapTbx.Projection = rmfield(pppp.MapTbx.Projection, 'UTM');
end
if isfield(pppp.MapTbx.Projection, 'Lambert')
    pppp.MapTbx.Projection = rmfield(pppp.MapTbx.Projection, 'Lambert');
end

Geoid = pppp.MapTbx.Geoid;
pppp.Version = '20140317';
% Impossible d'�crire un objet de type referenceEllipsoid
pppp.MapTbx = rmfield(pppp.MapTbx, 'Geoid');
pppp.MapTbx.Geoid = struct(Geoid);
pppp.MapTbx.Geoid = rmfield(pppp.MapTbx.Geoid, 'Ellipsoids');
% R�ordonnancement des champs pour avoir la version en 1er.
% pppp = orderfields(pppp, {'Version', 'Ellipsoide', 'Projection', 'MapTbx', 'numCodeUTM', 'strCodeUTM'});
pppp = orderfields(pppp, {'Version', 'Ellipsoide', 'Projection', 'MapTbx'});

% Transcription des caract�res de contr�les
if ~isempty(pppp.MapTbx.Projection.Proj4) % Ajout� par JMA le 16/01/2017 pour images g�n�r�es par Ridha
    pppp.MapTbx.Projection.Proj4 = regexprep(pppp.MapTbx.Projection.Proj4, '<', '-(');
    pppp.MapTbx.Projection.Proj4 = regexprep(pppp.MapTbx.Projection.Proj4, '>', ')-');
end
xml_write(nomFic, pppp);
warning on MATLAB:structOnObject

flag = exist(nomFic, 'file');
