% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(a, ...)
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Name-Value Pair Arguments
%  Cf. cl_carto
%
% Output Arguments 
%   PropertyValues associees aux PropertyNames passees en argument
% 
% Examples
%   a = cl_carto
%
%   Titre = a.Titre;
%
% See also cl_carto cl_carto/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% On v�rifie que l'instance est bien de demension 1

if length(this) ~= 1
    my_warndlg('cl_carto/get : L''instance doit �tre de dimension 1', 1);
    return
end

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'Ellipsoide'
            varargout{end+1} = this.Ellipsoide;  %#ok<AGROW>
        case 'Ellipsoide.Type'
            varargout{end+1} = this.Ellipsoide.Type;   %#ok<AGROW>
        case 'Ellipsoide.strType'
            varargout{end+1} = this.Ellipsoide.strType;   %#ok<AGROW>
        case 'Ellipsoide.DemiGrandAxe'
            varargout{end+1} = this.Ellipsoide.DemiGrandAxe;   %#ok<AGROW>
        case 'Ellipsoide.DemiPetitAxe'
            varargout{end+1} = demiPetitAxe(this);   %#ok<AGROW>
        case 'Ellipsoide.Excentricite'
            varargout{end+1} = this.Ellipsoide.Excentricite;   %#ok<AGROW>
        case 'Ellipsoide.E2'
            varargout{end+1} = carre_excentricite(this);   %#ok<AGROW>
        case 'Ellipsoide.Aplatissement'
            varargout{end+1} = this.MapTbx.Geoid.InverseFlattening;   %#ok<AGROW>
        case 'Projection.Type'
            varargout{end+1} = this.Projection.Type;   %#ok<AGROW>
        case 'Projection.strType'
            varargout{end+1} = this.Projection.strType;   %#ok<AGROW>
        case 'Projection.UTM.Fuseau'
            varargout{end+1} = this.Projection.UTM.Fuseau;   %#ok<AGROW>
        case 'Projection.UTM.Central_meridian'
            varargout{end+1} = (this.Projection.UTM.Fuseau - 31) * 6 + 3;   %#ok<AGROW>
        case 'Projection.UTM.Central_parallel'
            varargout{end+1} = this.Projection.UTM.Central_parallel;   %#ok<AGROW>
        case 'Projection.UTM.Longitude'
            varargout{end+1} = this.Projection.UTM.Longitude;   %#ok<AGROW>
        case 'Projection.UTM.Hemisphere'
            varargout{end+1} = this.Projection.UTM.Hemisphere;   %#ok<AGROW>
        case 'Projection.UTM.Y0'
            varargout{end+1} = this.Projection.UTM.Y0;   %#ok<AGROW>
        case 'Projection.UTM.X0'
            varargout{end+1} = this.Projection.UTM.X0;   %#ok<AGROW>
        case 'Projection.Mercator.long_merid_orig'
            varargout{end+1} = this.Projection.Mercator.long_merid_orig;   %#ok<AGROW>
        case 'Projection.Mercator.lat_ech_cons'
            varargout{end+1} = this.Projection.Mercator.lat_ech_cons;   %#ok<AGROW>
        case 'Projection.Mercator.Y0'
            varargout{end+1} = this.Projection.Mercator.Y0 ;   %#ok<AGROW>
        case 'Projection.Mercator.X0'
            varargout{end+1} = this.Projection.Mercator.X0 ;   %#ok<AGROW>
        case 'Projection.Lambert.Type'
            varargout{end+1} = this.Projection.Lambert.Type;   %#ok<AGROW>
        case 'Projection.Lambert.strType'
            varargout{end+1} = this.Projection.Lambert.strType;   %#ok<AGROW>
        case 'Projection.Lambert.first_paral'
            varargout{end+1} = this.Projection.Lambert.first_paral;   %#ok<AGROW>
        case 'Projection.Lambert.second_paral'
            varargout{end+1} = this.Projection.Lambert.second_paral;   %#ok<AGROW>
        case 'Projection.Lambert.long_merid_orig'
            varargout{end+1} = this.Projection.Lambert.long_merid_orig;   %#ok<AGROW>
        case 'Projection.Lambert.lat_ech_cons'
            varargout{end+1} = this.Projection.Lambert.lat_ech_cons;   %#ok<AGROW>
        case 'Projection.Lambert.X0'
            varargout{end+1} = this.Projection.Lambert.X0;   %#ok<AGROW>
        case 'Projection.Lambert.Y0'
            varargout{end+1} = this.Projection.Lambert.Y0;   %#ok<AGROW>
        case 'Projection.Lambert.scale_factor'
            varargout{end+1} = this.Projection.Lambert.scale_factor;   %#ok<AGROW>
        case 'Projection.Stereo.long_merid_horiz'
            varargout{end+1} = this.Projection.Stereo.long_merid_horiz;   %#ok<AGROW>
        case 'Projection.Stereo.lat_ech_cons'
            varargout{end+1} = this.Projection.Stereo.lat_ech_cons;   %#ok<AGROW>
        case 'Projection.Stereo.Hemisphere'
            varargout{end+1} = this.Projection.Stereo.Hemisphere;   %#ok<AGROW>
        % Mapping Toolbox
        case 'MapTbx'
            varargout{end+1} = this.MapTbx;   %#ok<AGROW>
        case 'MStruct'
            varargout{end+1} = this.MapTbx.MStruct;   %#ok<AGROW>
        case 'Geoid'
            varargout{end+1} = this.MapTbx.Geoid;   %#ok<AGROW>
            
        case 'Referentiel.Azimut'
            varargout{1} = [];

        otherwise
            str = sprintf('cl_carto/get %s non pris en compte', varargin{k});
            my_warndlg(['cl_carto:get ' str], 0);
            varargout{1} = [];
    end
end
