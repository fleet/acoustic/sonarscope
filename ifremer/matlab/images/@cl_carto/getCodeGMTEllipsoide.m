% Labellisation du code de l'ellipsoide selon GMT.
%
% Syntax
%   str = getCodeGMTEllipsoide(a) 
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   Type : Identifiant de l'ellipsoide dans la bibliothèque de GMT (v.
%   gmtdefaults -L)
% 
% Examples
%   a = cl_carto('Ellipsoide.Type', 11)
%   str = getCodeGMTEllipsoide(a)
%
% See also cl_carto cl_carto/demiPetitAxe cl_carto/carre_excentricite Authors
% Authors : JMA
% -------------------------------------------------------------------------

function str = getCodeGMTEllipsoide(this)

switch this.Ellipsoide.Type
    case 2
        str = 'WGS-72';
    case 3
        str = 'WGS-66';
    case 4
        str = 'International-1967';
    case 5
        str = 'Clarke-1866';
    case 6
        str = 'Clarke-1880';
    case 7
        str = 'Everest-1830 ';
    case 8
        str = 'Bessel 1841 ';
    case 9
        str = 'Krassovsky';
    case 10
        str = 'Australian 1965';
    case 11
        str = 'WGS-84';
    case 12
        str = 'GRS-80';
    otherwise
        str = 'WGS-84';
end