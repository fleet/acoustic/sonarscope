% Extrait la r�f�rence de l'ellipso�de Caraibes correspondante
% � celle de SonarScope.
%
% Syntax
%   getEllipsoidCaraibes(Carto)
%
% Input Arguments
%   Carto : object cartographique
%
% Name-Value Pair Arguments
%   Sans objet
%
% Examples
%	nomFic = getNomFicDatabase('EM12D_Ex.mbb');
%	car = cl_car_bat_data(nomFic);
%	b = view(car, 'Sonar.Ident', 1);
%	a = get(b, 'Images');
%   get(a, 'Carto');
%   [Ellip_num, Ellip_name] = getEllipsoidCaraibes(Carto)
%
% See also cl_image Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [NumEllipsoidCaraibes, Ellipsoid_name] = getEllipsoidCaraibes(Carto)

EllipsoideType = get(Carto, 'Ellipsoide.Type');
switch EllipsoideType
    case 11
        NumEllipsoidCaraibes = 14;
        Ellipsoid_name = 'WGS-84';
    case 12
        NumEllipsoidCaraibes = 16;
        Ellipsoid_name = 'grs80';
    otherwise
        str = get(Carto, 'Ellipsoide.strType');
        str = sprintf('Ellipsoide %s pas encore dig�r�e dans getEllipsoidCaraibes, je remplace temporairement par WGS-84', str{EllipsoideType});
        my_warndlg(str, 1);
        NumEllipsoidCaraibes = 14;
        Ellipsoid_name = 'WGS-84';
end
Ellipsoid_name(end+1:20) = char(0);
