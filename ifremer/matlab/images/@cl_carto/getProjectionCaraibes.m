% Extrait la r�f�rence de la projection Caraibes correspondante
% � celle de SonarScope.
%
% Syntax
%   getProjectionCaraibes(Carto)
%
% Input Arguments
%   Carto : object cartographique
%
% Name-Value Pair Arguments
%   Sans objet
%
% Examples
%	nomFic = getNomFicDatabase('EM12D_Ex.mbb');
%	car = cl_car_bat_data(nomFic);
%	b = view(car, 'Sonar.Ident', 1);
%	a = get(b, 'Images');
%   get(a, 'Carto');
%   proj = getProjectionCaraibes(Carto)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ProjectionCaraibes = getProjectionCaraibes(Carto)

Projection = get(Carto, 'Projection.Type');
switch Projection
    case 2 % Mercator
        ProjectionCaraibes = 1;
    case 3 % UTM
        ProjectionCaraibes = 3;
    case 4 % Lambert
        ProjectionCaraibes = 2;
    case 5 % St�r�ographique polaire.
        ProjectionCaraibes = 4;
    case 1 % ?????????????????????????????????????????????????????
        ProjectionCaraibes = 1; % Unknown
    case 6 % ?????????????????????????????????????????????????????
        ProjectionCaraibes = 6;  % Equicylindrique distante
    otherwise
        ProjectionCaraibes = [];
        str = get(Carto, 'Projection.strType');
        str = sprintf('Projection %s pas encore dig�r�e dans getProjectionCaraibes', str{Projection});
        my_warndlg(str, 1);
end
