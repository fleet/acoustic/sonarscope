% Extrait la r�f�rence de l'h�misph�re Caraibes correspondante
% � celle de SonarScope pour une projection de type UTM.
%
% Syntax
%   getUTMHemisphereCaraibes(Carto)
%
% Input Arguments
%   Carto : object cartographique
%
% Name-Value Pair Arguments
%   Sans objet
%
% Examples
%	nomFic = getNomFicDatabase('EM12D_Ex.mbb');
%	car = cl_car_bat_data(nomFic);
%	b = view(car, 'Sonar.Ident', 1);
%	a = get(b, 'Images');
%   get(a, 'Carto');
%   proj = getUTMHemisphereCaraibes(Carto)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function UTM_Hemisphere = getUTMHemisphereCaraibes(Carto)

H = get(Carto, 'Projection.UTM.Hemisphere');
switch H
    case 'N'
       UTM_Hemisphere = 1; 
    case 'S'
       UTM_Hemisphere = 2;
    otherwise
       UTM_Hemisphere = 3;
end
% UTM_Hemisphere = char(UTM_Hemisphere);
