% Get ellipsoid name of a cl_carto instance.
%
% Syntax
%   get_EllipsoidName(Carto)
%
% Input Arguments
%   Carto : Instance of cl_carto
%
% Examples
%	Carto = cl_carto;
%	EllipsoidName = get_EllipsoidName(Carto)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function EllipsoidName = get_EllipsoidName(this)

EllipsoidName = this.Ellipsoide.strType{this.Ellipsoide.Type};
