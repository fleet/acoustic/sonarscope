% Get projection name of a cl_carto instance.
%
% Syntax
%   get_ProjectionName(Carto)
%
% Input Arguments
%   Carto : Instance of cl_carto
%
% Examples
%	Carto = cl_carto;
%	ProjectionName = get_ProjectionName(Carto)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ProjectionName = get_ProjectionName(this)

ProjectionName = this.Projection.strType{this.Projection.Type};
