% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   numEllipsoide = ident_ellipsoide(a, GrandAxe, Excentricity)
% 
% Input Arguments 
%   a            : Une instance de la classe cl_carto
%   GrandAxe     : Demi grans axe (m)
%   Excentricity : Excentricite
%
% Name-Value Pair Arguments
%  Cf. cl_carto
%
% Output Arguments
%   numEllipsoide : Numero de l'ellipsoide dans la liste definie par cl_carto :
%                   '1=Unknown' | '2=wgs72' | '3=wgs66' | '4=international' | '5=Clarke 1866' | '6=Clarke 1880' |
%                   '7=everest' | '8=bessel' | '9=krasovsky' | '10=iau65' | '11=wgs84'
% 
% Examples
%   a = cl_carto;
%   [numEllipsoide, nomEllipsoide] = ident_ellipsoide(a, 6378206.4, 0.08227185818)
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [numEllipsoide, nomEllipsoide] = ident_ellipsoide(this, GrandAxe, Excentricity, varargin)

if isempty(this.Ellipsoide)
    numEllipsoide = [];
    nomEllipsoide = 'Unknown';
    return
end

R = this.Ellipsoide.tabDemiGrandAxe;
E = this.Ellipsoide.tabExcentricite;

%{
strType = this.Ellipsoide.strType;
strType = strType(2:end-1);
figure; grid on; hold on;
for i=1:length(R)
    cmenu = uicontextmenu;
    h = plot(R(i), E(i), '*', 'UIContextMenu', cmenu);
    uimenu(cmenu, 'Text', strType{i});
    uimenu(cmenu, 'Text', sprintf('DemiGrandAxe : %11.3f', R(i)));
    uimenu(cmenu, 'Text', sprintf('Excentricity : %17.15f', E(i)));
end
cmenu = uicontextmenu;
h = plot(GrandAxe, Excentricity, 'or', 'UIContextMenu', cmenu)
uimenu(cmenu, 'Text', sprintf('DemiGrandAxe : %11.3f', GrandAxe));
uimenu(cmenu, 'Text', sprintf('Excentricity : %17.15f', Excentricity));
xlabel('DemiGrandAxe'); ylabel('Excentricity'); title('Ellipoides')
%}

minR = min(R);
minE = min(E);
DR = max(R) - minR;
DE = max(E) - minE;

R = (R - minR) / DR;
E = (E - minE) / DE;

r = (GrandAxe - minR) / DR;
e = (Excentricity - minE) / DE;
%     figure; plot(R, E, '+k'); hold on; plot(r, e, '*r')

dist = sqrt((R-r).^2 + (E-e).^2);
[distMin, numEllipsoide] = min(dist);
% % % if distMin < 0.005
% % %     numEllipsoide = 1 + numEllipsoide;
% % % else
if distMin > 0.005
    numEllipsoide = 1;
    my_warndlg('cl_carto/ident_ellipsoide : Ellipsoide non identifie', 1);
end

nomEllipsoide = this.Ellipsoide.strType{numEllipsoide};
