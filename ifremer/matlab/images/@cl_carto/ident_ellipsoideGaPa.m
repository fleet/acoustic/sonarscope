% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   numEllipsoide = ident_ellipsoideGaPa(a, GrandAxe, PetitAxe)
% 
% Input Arguments 
%   a            : Une instance de la classe cl_carto
%   GrandAxe     : Demi grans axe (m)
%   PetitAxe : Excentricite
%
% Name-Value Pair Arguments
%  Cf. cl_carto
%
% Output Arguments
%   numEllipsoide : Numero de l'ellipsoide dans la liste definie par cl_carto :
%                   '1=Unknown' | '2=wgs72' | '3=wgs66' | '4=international' | '5=Clarke 1866' | '6=Clarke 1880' |
%                   '7=everest' | '8=bessel' | '9=krasovsky' | '10=iau65' | '11=wgs84'
% 
% Examples
%   a = cl_carto;
%   [numEllipsoide, nomEllipsoide] = ident_ellipsoideGaPa(a, 6378206.4, 0.08227185818)
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [numEllipsoide, nomEllipsoide] = ident_ellipsoideGaPa(this, GrandAxe, PetitAxe, varargin)

if isempty(this.Ellipsoide)
    numEllipsoide = [];
    nomEllipsoide = 'Unknown';
    return
end

Ga = this.Ellipsoide.tabDemiGrandAxe;
Ga = Ga(2:end-1); % Modif JMA le 10/07/2014 import fichier Y:\private\shom\7150_2009\MOCOSED2009\IMA_MOCOSED2009_PP_75m_UTM27N.tif
Carto = cl_carto;
for k=2:(length(Carto.Ellipsoide.strType)-1)
    Carto = set(Carto, 'Ellipsoide.Type', k, 'Mute', 1);
    Pa(k-1,1) = demiPetitAxe(Carto); %#ok
end

Ga = Ga(:);
Pa = Pa(:);

%{
strType = this.Ellipsoide.strType;
strType = strType(2:end-1);
figure; grid on; hold on;
for k=1:length(Ga)
    cmenu = uicontextmenu;
    h = plot(Ga(k), Pa(k), '*', 'UIContextMenu', cmenu);
    uimenu(cmenu, 'Text', strType{k});
    uimenu(cmenu, 'Text', sprintf('DemiGrandAxe : %11.3f', Ga(k)));
    uimenu(cmenu, 'Text', sprintf('PetitAxe : %17.15f', Pa(k)));
end
cmenu = uicontextmenu;
h = plot(GrandAxe, PetitAxe, 'or', 'UIContextMenu', cmenu)
uimenu(cmenu, 'Text', sprintf('DemiGrandAxe : %11.3f', GrandAxe));
uimenu(cmenu, 'Text', sprintf('PetitAxe : %17.15f', PetitAxe));
xlabel('DemiGrandAxe'); ylabel('PetitAxe'); title('Ellipoides')
%}

minGa = min(Ga);
minPa = min(Pa);
DR = max(Ga) - minGa;
DE = max(Pa) - minPa;

Ga = (Ga - minGa) / DR;
Pa = (Pa - minPa) / DE;

r = (GrandAxe - minGa) / DR;
e = (PetitAxe - minPa) / DE;
%     figure; plot(Ga, Pa, '+k'); hold on; plot(r, e, '*r')

dist = sqrt((Ga-r).^2 + (Pa-e).^2);
[distMin, numEllipsoide] = min(dist);
if distMin < 0.005
    numEllipsoide = 1 + numEllipsoide;
else
    numEllipsoide = 1;
    my_warndlg('cl_carto/ident_ellipsoideGaPa : Ellipsoide non identifie', 1);
end

nomEllipsoide = this.Ellipsoide.strType{numEllipsoide};
