% Importation des parametres cartographiques d'un fichier ascii
%
% Syntax
%	Carto = import_Carto_MapTbx_OOP(cl_carto([]), nomFic)
% 
% Input Arguments 
%   nomFic : Nom du fichier
%
% Output Arguments
%   a : Instance de la classe cl_carto
% 
% Examples
%   nomFic = 'C:\Temp\Carto.def'
%	Carto = import_Carto_MapTbx_OOP(cl_carto([]), nomFic)
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = import_Carto_MapTbx_OOP(this, nomFic)

fid = fopen(nomFic);
if fid == -1
    this = [];
    return
end

while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    
    %% Ellipsode
    
    if contains(tline, 'Ellipsoide.Type')
        k = strfind(tline, '{');
        tline = tline(k+1:end);
        k = strfind(tline, '=');
        tline = tline(2:k-1);
        X = str2double(tline);
        this = set(this, 'Ellipsoide.Type', X);
    end

    %% Type de projection
    
    if contains(tline, 'Projection.Type')
        k = strfind(tline, '{');
        tline = tline(k+1:end);
        k = strfind(tline, '=');
        tline = tline(2:k-1);
        X = str2double(tline);
        this = set(this, 'Projection.Type', X);
    end

    %% Projection.UTM.Fuseau
    
    if contains(tline, 'Projection.UTM.Fuseau')
        k = strfind(tline, '<');
        tline = tline(k+3:end);
        X = str2double(tline);
        this = set(this, 'Projection.UTM.Fuseau', X);
    end
    
    %% Projection.UTM.Hemisphere
    
    if contains(tline, 'Projection.UTM.Hemisphere')
        k = strfind(tline, '<');
        tline = tline(k+4:end);
        X = tline(1);
        this = set(this, 'Projection.UTM.Hemisphere', X);
    end


    %% Projection.Mercator.long_merid_orig
    
    if contains(tline, 'Projection.Mercator.long_merid_orig')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Mercator.long_merid_orig', X);
    end

    %% Projection.Mercator.lat_ech_cons
    
    if contains(tline, 'Projection.Mercator.lat_ech_cons')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Mercator.lat_ech_cons', X);
    end

    %% Projection.Mercator.X0
    
    if contains(tline, 'Projection.Mercator.X0')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Mercator.X0', X);
    end

    %% Projection.Mercator.Y0
    
    if contains(tline, 'Projection.Mercator.Y0')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Mercator.Y0', X);
    end

    %% Projection.Lambert.first_paral
    
    if contains(tline, 'Projection.Lambert.first_paral')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Lambert.first_paral', X);
    end
    
    %% Projection.Lambert.second_paral
    
    if contains(tline, 'Projection.Lambert.second_paral')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Lambert.second_paral', X);
    end
    
    %% Projection.Lambert.long_merid_orig
    
    if contains(tline, 'Projection.Lambert.long_merid_orig')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Lambert.long_merid_orig', X);
    end
    
    %% Projection.Lambert.lat_ech_cons
    
    if contains(tline, 'Projection.Lambert.lat_ech_cons')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Lambert.lat_ech_cons', X);
    end
    
    %% Projection.Lambert.X0
    
    if contains(tline, 'Projection.Lambert.X0')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Lambert.X0', X);
    end
    
    %% Projection.Lambert.Y0
    
    if contains(tline, 'Projection.Lambert.Y0')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Lambert.Y0', X);
    end
    
    %% Projection.Lambert.scale_factor
    
    if contains(tline, 'Projection.Lambert.scale_factor')
        k = strfind(tline, '>');
        tline = tline(k+2:end);
        X = str2double(tline);
        this = set(this, 'Projection.Lambert.scale_factor', X);
    end
    
end
fclose(fid);

