% Importation de l'objet au format XML
%
% Syntax
%   a = import_xml(a, nomFic)
%
% Input Arguments
%   a      : Instance de la classe cl_carto
%   nomFic : Nom du fichier xml
%
% Output Arguments
%   a      : Instance de la classe cl_carto
%
% Remarks : La fonction rajoute (ou modifie) automatiquement l'extension du fichier en  ".xml"
%
% Examples
%   a = cl_carto('Ellipsoide.Type', 2, 'Projection.Type', 3, 'Projection.UTM.Fuseau', 30)
%   nomFic = my_tempname('.xml')
%   flag = export_xml(a, nomFic)
%
%   b = import_xml(cl_carto([]), nomFic)
%
% See also cl_carto/import_xml xml_write  Authors
% Authors : JMA
%-------------------------------------------------------------------------------

 function this = import_xml(~, nomFic)
 
[nomDir, nomFicXml] = fileparts(nomFic);
nomFicXml           = fullfile(nomDir, [nomFicXml '.xml']);

this    = cl_carto([]);
this(:) = [];

if exist(nomFicXml, 'file')
%     tree = xml_read(nomFicXml);
    tree = xml_mat_read(nomFicXml);
    if ~isfield(tree, 'Version')
        [flag, this] = loadNewCartoFromOldCarto(tree);
        if ~flag
            return
        end
    elseif tree.Version == 20140317
        [flag, this] = loadNewCartoFromSomeProperties(tree);
        if ~flag
            return
        end
    end
else
    this = [];
end
