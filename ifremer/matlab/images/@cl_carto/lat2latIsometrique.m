%  Calcul de la latitude isometrique
%
% q = cl_carto('Ellipsoide.Type', 11, ...
%                'Projection.Type', 2, ...
%                'Projection.Mercator.long_merid_orig', 174, ...
%                'Projection.Mercator.lat_ech_cons', 36);
%  latIso = lat2latIsometrique(q, 36)
%  cos(latIso * pi/180)

function latIso = lat2latIsometrique(this, lat)

refEllipsoid = this.MapTbx.Geoid;
latIso = convertlat(refEllipsoid,lat,'geodetic','isometric', 'deg');
