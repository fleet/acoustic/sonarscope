% Transformation des coordonn�es wgs84 dans le sph�roide cible.
% Cela est permis gr�ce aux informations de translation inter-sph�roides,
% obtenues dans la description des projections par PROJ.4
%
% Syntax
%   [flag, this, lat1, lon1] = latlon2latlonInSpheroidTarget(a, lat0, lon0, sourceSpheroid, targetSpheroid)
% 
% Input Arguments 
%   a               : Une instance de la classe cl_carto
%   lat0            : latitude dans  un r�p�re Geodetic
%   lon0            : longitude dans un r�p�re Geodetic
%   sourceSpheroid  : sph�roide source
%   targetSpheroid  : sph�roide cible.
%
% Name-Value Pair Arguments
%  Cf. cl_carto
%
% Output Arguments
%   flag : comportement de la fonction
%   a    : Instance de la classe
%   lat1 : latitude dans la sph�roide cible.
%   lon1 : longitude dans la sph�roide cible.
% 
% Examples
%   Example: wgs84 to NTF (Paris) / Lambert Nord France (EPSG 27561)
%   lon      = -4.4378;
%   lat      = 48.4172;
%   NewCarto = cl_carto('CodeEPSG', '27561');
%   [x1, y1] = latlon2xy(NewCarto, lat,lon);
%
% See also cl_carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this, lat1, lon1] = latlon2latlonInSpheroidTarget(this, lat0, lon0, sourceSpheroid, targetSpheroid)

% flag = 0;

Dx_BF = this.MapTbx.Projection.DeltaToWGS84.DX;
Dy_BF = this.MapTbx.Projection.DeltaToWGS84.DY;
Dz_BF = this.MapTbx.Projection.DeltaToWGS84.DZ;
Rx_BF = this.MapTbx.Projection.DeltaToWGS84.RX;
Ry_BF = this.MapTbx.Projection.DeltaToWGS84.RY;
Rz_BF = this.MapTbx.Projection.DeltaToWGS84.RZ;
M_BF  = this.MapTbx.Projection.DeltaToWGS84.M;

height = 0;
% Formule valable si on a 7 param�tres ou 3 (pour le cas o� RX, RY, RZ et M sont bien initialis�s � 0).
translationInX = M_BF * (       lon0 - Rz_BF*lat0 + Ry_BF*height) + Dx_BF;
translationInY = M_BF * ( Rz_BF*lon0 +       lat0 - Rx_BF*height) + Dy_BF;
translationInZ = M_BF * (-Ry_BF*lon0 + Rx_BF*lat0 +       height) + Dz_BF;

% translationInX = -translationInX; % Test
% translationInY = -translationInY;
  
height = 0;
% % % % [X1,Y1,Z1] = geodetic2ecef(lat0, lon0, height, targetSpheroid);
[X,Y,Z] = geodetic2ecef(deg2rad(lat0), deg2rad(lon0), height, targetSpheroid);
% [X,Y,Z] = geodetic2ecef(deg2rad(lat0), deg2rad(lon0), height, sourceSpheroid); % Test par JMA le 03/03/2019
if targetSpheroid.Code == 7030
    % Op�ration Ellipsoide d'origine --> wgs84 
    X = X - translationInX;
    Y = Y - translationInY;
    Z = Z - translationInZ;
else
    % Op�ration wgs84 --> Ellipsoide Cible (cas de la conversion xy to LatLon)
    X = X + translationInX;
    Y = Y + translationInY;
    Z = Z + translationInZ;
end
% Projection Geod�tique des coordonn�es sur une Ellipsoide pr�vue (ex : Clarke_1880_IGN)
[lat1, lon1] = ecef2geodetic(X, Y, Z, sourceSpheroid);
% [lat1, lon1] = ecef2geodetic(X, Y, Z, targetSpheroid); % Test par JMA le 03/03/2019

lat1 = rad2deg(lat1);
lon1 = rad2deg(lon1);

parisMeridian = dms2degrees([2 20 14.025]); % 2.33722917 degrees
if targetSpheroid.Code == 7030
%     lon1 = lon1 - this.MapTbx.MStruct.origin(2); % Comment� par JMA le 02/03/2018
    centralMeridian = 0;
else
    centralMeridian = parisMeridian;
end
this.MapTbx.MStruct.origin       = [this.MapTbx.MStruct.origin(1) centralMeridian];
this.MapTbx.MStruct.mapparallels = this.MapTbx.MStruct.origin(1);

flag = 1;
