% Passage de coordonn�es g�ographiques � coordonn�es cartographiques
%
% Syntax
%   [x, y] = latlon2xy(Carto, lat, lon)
%
% Input Arguments
%   Carto : Instance de cartographie
%   lat   : Latitudes (deg d�cimaux)
%   lon   : Longitudes (deg d�cimaux)
%
% Output Arguments
%   x  : Description du parametre (m).
%   y  : Description du parametre (m).
%
% Examples
%   Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4)
%   [x, y] = latlon2xy(Carto, 46.5, 3)
%
%   [lat, lon] = xy2latlon(Carto, x, y)
%   [lat, lon] = xy2latlon(Carto, 700000, 6600000)
%   [lat, lon] = xy2latlon(Carto, 1160000, 4313000)
%
% See also cl_carto xy2latlon Authors
% References : CARAIBES(R)
% Authors : JMA
%-------------------------------------------------------------------------------

function [x, y, flag] = latlon2xy(this, lat, lon)

flag = 0;

% lat = lat(:,:);
% lon = lon(:,:);

% Translations for 3-parameter datum transformation from NTF to wgs84
% grs80 pour LAMBERT 93
% clarke80_IGN pour les autres LAMBERT
% wgs84 pour la plupart ces autres cas.
% On ne transforme les coordonn�es que si l'ellipsoid de d�part n'est pas
% wgs84 (ni grs80)
codeEPSGnum = str2double(this.MapTbx.Ellipsoide.CodeEPSG);
if codeEPSGnum ~= 7030 &&  codeEPSGnum ~= 7019
    sourceSpheroid         = referenceEllipsoid(codeEPSGnum); 
    targetSpheroid         = wgs84Ellipsoid;
    [flag, this, lat, lon] = latlon2latlonInSpheroidTarget(this, lat, lon, sourceSpheroid, targetSpheroid);
end

% Reconstruction de l'objt Type de la mapping Toolbox.
MStruct       = this.MapTbx.MStruct;
Geoid         = this.MapTbx.Geoid;
MStruct.geoid = Geoid;

switch lower(this.MapTbx.MStruct.mapprojection)
    case 'geodetic' % Geodetic
        my_warndlg('Projection is set to "Geodetic", impossible to compute metric coordinates.', 0, 'Tag', 'latlon2xyGeodetic');
        return
        
    case 'pcarree' % Coordonn�es en Lat/Lon
        my_warndlg('Projection is set to "Lat/Lon", impossible to compute metric coordinates.', 0, 'Tag', 'latlon2xyGeodetic');
        return

    case 'mercator'
        
% Modif JMA le 13/07/2016 : Qu'est-ce que c'�tait que ce bordel ?
%         medianLat = median(lat(:), 'omitnan');
%         MStruct.mapparallels = medianLat;
%         MStruct.nparallels   = 1;
%         MStruct.scalefactor  = 1;
%        	MStruct.origin       = [medianLat median(lon(:), 'omitnan') 0];

        % Fonctionne pour Mercator par exemple.
        [x, y] = projfwd(MStruct, lat, lon);

    case 'utm'
        % Fondamental pour la prise en compte de la bonne conversion.
        
% Modif JMA le 13/07/2016 : Qu'est-ce que c'�tait que ce bordel ?
%         zone             = utmzone(median(lat(:), 'omitnan'), median(lon(:), 'omitnan'));
%         [latlim, lonlim] = utmzone(zone);
%         MStruct.origin   = [latlim(1) mean(lonlim) 0];

        [x, y] = mfwdtran(MStruct, lat, lon);
        
    case 'lambert'
        % Fondamental pour la prise en compte de la bonne conversion.
        % MStruct.origin      = [lat lon 0];
        [x, y] = mfwdtran(MStruct, lat, lon);

    case 'lambertstd'
        % Fondamental pour la prise en compte de la bonne conversion.
        % MStruct.origin      = [lat lon 0];
        [x, y] = mfwdtran(MStruct, lat, lon);

    otherwise
        % Fonctionne pour Mercator par exemple.
        [x, y] = projfwd(MStruct, lat, lon);
end

flag = 1;
