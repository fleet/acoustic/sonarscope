function [lat, lon] = latlonAcrossAlong2latlon(this, Latitude, Longitude, Heading, across, along)

[x, y] = latlon2xy(this, Latitude, Longitude);

N = numel(Latitude);
sz = size(Latitude);
X = NaN(sz);
Y = NaN(sz);
for k=1:N
    Angle = Heading(k);
    c = cosd(Angle);
    s = sind(Angle);
    C = [c s; -s c] * [across ; along];
    xk = x(k);
    yk = y(k);
    X(k) = xk+C(1);
    Y(k) = yk+C(2);
end
[lat ,lon] = xy2latlon(this, X', Y');

