function [flag, zone]= my_findZone(this, varargin)

[varargin, Fuseau] = getPropertyValue(varargin, 'Fuseau', this.Projection.UTM.Fuseau);
% [varargin, LatMean] = getPropertyValue(varargin, 'LatMean', this.Projection.UTM.Central_parallel);
[varargin, LatMean] = getPropertyValue(varargin, 'LatMean', []); %#ok<ASGLU>

if Fuseau == 121 % 	NZTM2000
    zone = 'NZTM2000';
    flag = 1;
    return
end

zone = [];

if isempty(LatMean)
    str1 = 'Hémisphère';
    str2 = 'Hemisphere';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'N', 'S');
    if ~flag
        return
    end
    if rep == 1
        LatMean = 1;
    else
        LatMean = -1;
    end
else
    if this.Projection.UTM.Fuseau > 0
        LatMean = 1;
    else
        LatMean = -1;
    end
end

if Fuseau<= 60
    lon = (Fuseau - 31) * 6;
else
    lon = (Fuseau - 60 - 31) * 6 - 3;
end

checklatlon(LatMean, lon, 'UTMZONE', 'LAT', 'LON', 1, 2);

% Use geographic mean if lat and lon are non-scalar.
if ~isscalar(LatMean)
    LatMean         = LatMean(~isnan(LatMean));
    lon             = lon(~isnan(lon));
    [LatMean, lon]  = meanm(LatMean, lon);
end

% Calcul de la zone
zone = utmzone(LatMean, lon+3);

if LatMean == -1
    zone(end) = 'S';
end

centralMeridian = lon + 3;
inBounds = ((-80 <= LatMean) && (LatMean <= 84)) && ((-180 <= centralMeridian) && (centralMeridian <= 180));
if ~inBounds
    error(message('map:utm:outsideLimits'))
end
