% Importation des parametres cartographiques d'un fichier ascii
%
% Syntax
%	Carto = preinitialisation(Carto, Latitude, Longitude)
% 
% Input Arguments 
%   Carto     : Instance de cl_carto
%   Latitude  : Latitude
%   Longitude : Longitude
%
% Output Arguments
%   a : Instance de la classe cl_carto
% 
% Examples
%	Carto = preinitialisation(cl_carto([]), 48, -4.5)
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = preinitialisation(this, Latitude, Longitude)

%% Ellipsoide

TypeEllipsoide = 11;
this = set(this, 'Ellipsoide.Type', TypeEllipsoide);

%% Projection Mercator
    
this = set(this, 'Projection.Type', 2);
this = set(this, 'Projection.Mercator.long_merid_orig', 0);

this = set(this, 'Projection.Mercator.lat_ech_cons', round(Latitude));

% % % % --------------
% % % % Projection UTM
% % % TypeProjection = 3;   
% % % this = set(this, 'Projection.Type', TypeProjection);
% % % this = set(this, 'Projection.UTM.Central_meridian', Longitude);
% % % if Latitude >= 0
% % %     this = set(this, 'Projection.UTM.Hemisphere', 'N');
% % % else
% % %     this = set(this, 'Projection.UTM.Hemisphere', 'S');
% % % end

