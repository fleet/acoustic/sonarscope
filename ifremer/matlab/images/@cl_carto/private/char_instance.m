% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de cl_carto
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

% ----------
% Traitement

str{end+1} = sprintf('Ellipsoide.Type               <-> %s', SelectionDansListe2str(this.Ellipsoide.Type, this.Ellipsoide.strType, 'Num'));
str{end+1} = sprintf('Ellipsoide.DemiGrandAxe       <-- %021.11f (m)', this.Ellipsoide.DemiGrandAxe);
str{end+1} = sprintf('Ellipsoide.DemiPetitAxe       <-- %021.11f (m)', minaxis(this.Ellipsoide.DemiGrandAxe,this.Ellipsoide.Excentricite));
str{end+1} = sprintf('Ellipsoide.Excentricite       <-- %021.17f', this.Ellipsoide.Excentricite);
str{end+1} = sprintf('Ellipsoide.E2                 <-- %021.17f', carre_excentricite(this));
str{end+1} = sprintf('Ellipsoide.InvAplatissement   <-- %021.17f', 1/ecc2flat(this.Ellipsoide.Excentricite));


str{end+1} = sprintf('Projection.Type               <-> %s', SelectionDansListe2str(this.Projection.Type, this.Projection.strType, 'Num'));
switch this.Projection.Type
    case 1
    case 2
        str{end+1} = sprintf('Projection.Mercator.long_merid_orig     <-> %d', this.Projection.Mercator.long_merid_orig);
        str{end+1} = sprintf('Projection.Mercator.lat_ech_cons        <-> %d', this.Projection.Mercator.lat_ech_cons);
        str{end+1} = sprintf('Projection.Mercator.Y0                  <-> %d', this.Projection.Mercator.Y0);
        str{end+1} = sprintf('Projection.Mercator.X0                  <-> %d', this.Projection.Mercator.X0);
    case 3
        str{end+1} = sprintf('Projection.UTM.Fuseau                   <-> %d', this.Projection.UTM.Fuseau);
        
        if this.Projection.UTM.Fuseau<= 60
            Longitude(1) = (this.Projection.UTM.Fuseau - 31) * 6;
        else
            Longitude(1) = (this.Projection.UTM.Fuseau - 60 - 31) * 6 - 3;
        end
        Longitude(2) = Longitude(1) + 6;
        str{end+1} = sprintf('Projection.UTM.Longitude                <-- %s', num2strCode(Longitude));
        str{end+1} = sprintf('Projection.UTM.Central_meridian         <-> %d', this.Projection.UTM.Central_meridian);
        str{end+1} = sprintf('Projection.UTM.Central_parallel         <-> %d', this.Projection.UTM.Central_parallel);
        str{end+1} = sprintf('Projection.UTM.Hemisphere               <-> %s', this.Projection.UTM.Hemisphere);
        str{end+1} = sprintf('Projection.UTM.Y0                       <-- %d', this.Projection.UTM.Y0);
        str{end+1} = sprintf('Projection.UTM.X0                       <-- %d', this.Projection.UTM.X0);

    case {4; 6; 7; 8}
        str{end+1} = sprintf('Projection.Lambert.Type                 <-> %s', SelectionDansListe2str(this.Projection.Lambert.Type, this.Projection.Lambert.strType, 'Num'));
        str{end+1} = sprintf('Projection.Lambert.first_paral          <-> %f', this.Projection.Lambert.first_paral);
        str{end+1} = sprintf('Projection.Lambert.second_paral         <-> %f', this.Projection.Lambert.second_paral);
        str{end+1} = sprintf('Projection.Lambert.long_merid_orig      <-> %f', this.Projection.Lambert.long_merid_orig);
        str{end+1} = sprintf('Projection.Lambert.X0                   <-> %f', this.Projection.Lambert.X0);
        str{end+1} = sprintf('Projection.Lambert.Y0                   <-> %f', this.Projection.Lambert.Y0);
        str{end+1} = sprintf('Projection.Lambert.scale_factor         <-> %f', this.Projection.Lambert.scale_factor);
        try
            str{end+1} = sprintf('MapTbx.Projection.Lambert.Code          <-> %s', SelectionDansListe2str(this.Projection.Lambert.Type, this.MapTbx.Projection.Lambert.strCode, 'Num'));
        catch % Bug fichier Concarneau : corriger le pb en amont (import geotif ?)
        end
        
    case 5
        str{end+1} = sprintf('Projection.Stereo.long_merid_horiz      <-> %d',this.Projection.Stereo.long_merid_horiz);
        str{end+1} = sprintf('Projection.Stereo.lat_ech_cons          <-> %d',this.Projection.Stereo.lat_ech_cons);
        str{end+1} = sprintf('Projection.Stereo.hemisphere            <-> %s',this.Projection.Stereo.hemisphere);

    otherwise
end
str{end+1} = sprintf('####### Mapping Toolbox #######');
str{end+1} = sprintf('------- Ellipsoide -------');
% str{end+1} = sprintf('MapTbx.Ellipsoide.CodeEPSG        <-> %s', this.MapTbx.Ellipsoide.CodeEPSG);
str{end+1} = sprintf('MapTbx.Ellipsoide.Type            <-> %s', SelectionDansListe2str(this.Ellipsoide.Type, this.MapTbx.Ellipsoide.strType, 'Num'));
str{end+1} = sprintf('MapTbx.Ellipsoide.CodeEPSG        <-> %s', SelectionDansListe2str(this.Ellipsoide.Type, this.MapTbx.Ellipsoide.strCode, 'Num'));
str{end+1} = sprintf('MapTbx.Geoid                      <-> %s', 'Geoid');
str{end+1} = sprintf('MapTbx.Geoid.Code                 <-> %d', this.MapTbx.Geoid.Code);
str{end+1} = sprintf('MapTbx.Geoid.Name                 <-> %s', this.MapTbx.Geoid.Name);
str{end+1} = sprintf('MapTbx.Geoid.LengthUnit           <-> %s', this.MapTbx.Geoid.LengthUnit);
str{end+1} = sprintf('MapTbx.Geoid.SemimajorAxis        <-> %f', this.MapTbx.Geoid.SemimajorAxis);
str{end+1} = sprintf('MapTbx.Geoid.SemiminorAxis        <-> %f', this.MapTbx.Geoid.SemiminorAxis);
str{end+1} = sprintf('MapTbx.Geoid.InverseFlattening    <-> %f', this.MapTbx.Geoid.InverseFlattening);
str{end+1} = sprintf('MapTbx.Geoid.Eccentricity         <-> %f', this.MapTbx.Geoid.Eccentricity);
str{end+1} = sprintf('------- Projection -------');
str{end+1} = sprintf('MapTbx.MStruct                    <-> %s', 'Mapping Structure');
str{end+1} = sprintf('MapTbx.MStruct.mapprojection      <-> %s', this.MapTbx.MStruct.mapprojection);
str{end+1} = sprintf('MapTbx.MStruct.zone               <-> %s', this.MapTbx.MStruct.zone);
str{end+1} = sprintf('MapTbx.MStruct.angleunits         <-> %s', this.MapTbx.MStruct.angleunits);
str{end+1} = sprintf('MapTbx.MStruct.aspect             <-> %s', this.MapTbx.MStruct.aspect);
str{end+1} = sprintf('MapTbx.MStruct.falsenorthing      <-> %d', this.MapTbx.MStruct.falsenorthing);
str{end+1} = sprintf('MapTbx.MStruct.falseeasting       <-> %d', this.MapTbx.MStruct.falseeasting);
str{end+1} = sprintf('MapTbx.MStruct.fixedorient        <-> %d', this.MapTbx.MStruct.fixedorient);
str{end+1} = sprintf('MapTbx.MStruct.maplatlimit        <-> %s', num2strCode(this.MapTbx.MStruct.maplatlimit));
str{end+1} = sprintf('MapTbx.MStruct.maplonlimit        <-> %s', num2strCode(this.MapTbx.MStruct.maplonlimit));
str{end+1} = sprintf('MapTbx.MStruct.mapparallels       <-> %s', num2strCode(this.MapTbx.MStruct.mapparallels));
str{end+1} = sprintf('MapTbx.MStruct.nparallels         <-> %d', this.MapTbx.MStruct.nparallels);
str{end+1} = sprintf('MapTbx.MStruct.origin             <-> %s', num2strCode(this.MapTbx.MStruct.origin));
str{end+1} = sprintf('MapTbx.MStruct.scalefactor        <-> %d', this.MapTbx.MStruct.scalefactor);
str{end+1} = sprintf('MapTbx.MStruct.trimlat            <-> %s', num2strCode(this.MapTbx.MStruct.trimlat));
str{end+1} = sprintf('MapTbx.MStruct.trimlon            <-> %s', num2strCode(this.MapTbx.MStruct.trimlon));
str{end+1} = sprintf('------- Projection - EPSG -------');
str{end+1} = sprintf('MapTbx.Projection.CodeEPSG            <-> %s', this.MapTbx.Projection.CodeEPSG);
str{end+1} = sprintf('MapTbx.Projection.Proj4               <-> %s', this.MapTbx.Projection.Proj4);
str{end+1} = sprintf('MapTbx.Projection.DeltaToWGS84.DX     <-> %f', this.MapTbx.Projection.DeltaToWGS84.DX);
str{end+1} = sprintf('MapTbx.Projection.DeltaToWGS84.DY     <-> %f', this.MapTbx.Projection.DeltaToWGS84.DY);
str{end+1} = sprintf('MapTbx.Projection.DeltaToWGS84.DZ     <-> %f', this.MapTbx.Projection.DeltaToWGS84.DZ);
str{end+1} = sprintf('MapTbx.Projection.DeltaToWGS84.RX     <-> %f', this.MapTbx.Projection.DeltaToWGS84.RX);
str{end+1} = sprintf('MapTbx.Projection.DeltaToWGS84.RY     <-> %f', this.MapTbx.Projection.DeltaToWGS84.RY);
str{end+1} = sprintf('MapTbx.Projection.DeltaToWGS84.RZ     <-> %f', this.MapTbx.Projection.DeltaToWGS84.RZ);
str{end+1} = sprintf('MapTbx.Projection.DeltaToWGS84.M      <-> %f', this.MapTbx.Projection.DeltaToWGS84.M);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
