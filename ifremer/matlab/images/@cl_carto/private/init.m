% Initialisation des param�tres de Cartographie en s'appuyant sur la
% Mapping Toolbox de MatLab
%
% Syntax
%   a = init(a)
%
% Input Arguments
%   a : instance de cl_carto
%
% Output Arguments
%   a : instance de cl_carto
%
% Examples
%
% See also cl_carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = init(this)

persistent flagLambertNTF

% Si le champ EPSG n'est pas encore connu, la structure n'est pas encore remplie.
switch this.Projection.Type
    case 2 % Mercator
        % Tout est fait dans l'initialisation par d�faut et le set.m.
        
    case 3 % UTM
        if this.Projection.UTM.Fuseau == 121 % Cas NZGD2000 . TODO : Danger car fait � l'arrache
            this.Projection.UTM.Central_parallel = this.MapTbx.MStruct.origin(1);
            this.Projection.UTM.Central_meridian = this.MapTbx.MStruct.origin(2);
            this.Projection.UTM.X0               = this.MapTbx.MStruct.falseeasting;
            this.Projection.UTM.Y0               = this.MapTbx.MStruct.falsenorthing;
        end
        
        % D�but ajout JMA le 05/11/2015
        zone = this.MapTbx.MStruct.zone;
        if isempty(zone)
            return
        end
        this.Projection.UTM.Fuseau     = str2double(this.MapTbx.MStruct.zone(1:end-1));
        this.Projection.UTM.Hemisphere = this.MapTbx.MStruct.zone(end);
        % Fin ajout JMA le 05/11/2015
        
        %         this.Projection.UTM.Fuseau           = str2double(this.MapTbx.MStruct.zone(1:end-1)); % Comment� par JMA le 05/11/2015
        %         this.Projection.UTM.Hemisphere       = this.MapTbx.MStruct.zone(end);                 % Comment� par JMA le 05/11/2015
        this.Projection.UTM.Central_parallel = this.MapTbx.MStruct.origin(1);
        this.Projection.UTM.Central_meridian = this.MapTbx.MStruct.origin(2);
        this.Projection.UTM.X0               = this.MapTbx.MStruct.falseeasting;
        this.Projection.UTM.Y0               = this.MapTbx.MStruct.falsenorthing;
        
        % Test pour donn�es SBP NIWA NZTM2000 : https://www.linz.govt.nz/data/geodetic-system/datums-projections-and-heights/projections/new-zealand-transverse-mercator-2000
        % this.Projection.UTM.Central_meridian=-173;
        % this.Projection.UTM.X0=-1600000;
        % this.Projection.UTM.Y0=-10000000;
        % this.Projection.UTM
        
        
    case 4 % Lambert
        
        % Initialisation des tableaux de d�finition.
        % Cf : http://fr.wikipedia.org/wiki/Projection_conique_conforme_de_Lambert
        % Cf : http://geodesie.ign.fr/contenu/fichiers/Lambert93_ConiquesConformes.pdf
        % D�finition des Pojections :
        % Lambert 93, Lambert Nord, Centre, Sud, Corse, Lambert Zone I � IV �tendu
        tabX0   = [700000 600000 600000 600000 234.358 600000 600000 600000 234.358];
        tabY0   = [6602157.839 200000 200000 200000 185861.369 1200000 2200000 3200000 4185861.369];
        lon0    = dms2degrees([2 20 14.025]); % = parisMeridian = dms2degrees([2 20 14.025]);
        tabLon0 = [3 lon0 lon0 lon0 lon0 lon0 lon0 lon0 lon0];
        
        % Tableaux de Lat 0 donn�es en grades : constitue la latitude
        % d'�chelle conserv�e (phi0).
        tabLat0 = 0.9 * [46.51943204/0.9 55 52 49 46.85 55 52 49 46.85];
        
        % Facteur d'�chelle (forc� � 1 pour Lambert 93)
        % List des facteurs d'�chelles (k0) : http://spatialreference.org/ref/epsg/
        scaleFactor = [0.999051030 0.999877340 0.999877420 ...
            0.999877499 0.999944710 0.999877340 ...
            0.999877420 0.999877499 0.999944710];
        
        % La d�finition des deux parall�les imposent dans ce cas un scale Factor = 1.
        %         parallels   = [ [44 0 0 ;49 0 0], ...
        %             [48 35 54.682;50 23 45.282], ...
        %             [45 53 56.108;47 41 45.652], ...
        %             [43 11 57.449;44 59 45.938], ...
        %             [41 33 37.396;42 46 03.588], ...
        %             [48 35 54.682;50 23 45.282], ...
        %             [45 53 56.108;47 41 45.652], ...
        %             [43 11 57.449;44 59 45.938], ...
        %             [41 33 37.396;42 46 03.588]];
        
        format longG
        % % %         for k=1:size(parallels, 2)/3
        % % %             parallelsDms(k,:) = dms2degrees(parallels(:,((k-1)*3+1):((k-1)*3+1)+2))';
        % % %         end
        lat_ech_cons   = tabLat0(this.Projection.Lambert.Type);
        lon_merid_orig = tabLon0(this.Projection.Lambert.Type);
        
        % Exception pour Lambert 93 : on traite cette projection en donnant
        % les deux parall�les.
        % Assignation des anciens param�tres
        this.Projection.Lambert.X0              = tabX0(this.Projection.Lambert.Type);
        this.Projection.Lambert.Y0              = tabY0(this.Projection.Lambert.Type); % Lu dans fichier .mnt Caraibes
        this.Projection.Lambert.scale_factor    = scaleFactor(this.Projection.Lambert.Type);
        this.Projection.Lambert.first_paral     = lat_ech_cons; % parallelsDms(this.Projection.Lambert.Type, 1); % 46+31/60+09.94881/3600
        this.Projection.Lambert.second_paral    = lat_ech_cons; % parallelsDms(this.Projection.Lambert.Type, 2);
        this.Projection.Lambert.long_merid_orig = lon_merid_orig;
        this.Projection.Lambert.lat_ech_cons    = lat_ech_cons;
        
        if (this.Projection.Lambert.Type > 1) && (this.Ellipsoide.Type ~= 14)
            if isempty(flagLambertNTF)
                flagLambertNTF = 1;
                str1 = 'Attention, la projection Lambert France doit �tre accoci�e � l''ellipso�de "Clarke 1880".';
                str2 = 'Beware, Lambert projection, France must be associated to Ellipsoid "Clarke 1880".';
                my_warndlg(Lang(str1,str2), 1);
            end
        end
        format long
end
