function flag = saveCarto(Carto, nomDir, DefaultName)

str1 = 'Voulez-vous sauver les paramètres cartographiques dans un fichier ?';
str2 = 'Save the cartographic parameters in a file ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    str1 = 'Nom du fichier de définition de la carto';
    str2 = 'Give a file name';
    [flag, nomFic] = my_uiputfile('*.def', Lang(str1,str2), fullfile(nomDir, DefaultName));
    if ~flag
        return
    end
    flag = export(Carto, nomFic);
    % edit(nomFic)
end
