% Selection de l'ellipsoide
%
% Syntax
%   Type = selectEllipsoide(a) 
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   Type : Identifiant de l'ellipsoide  2='wgs72'; 3='wgs66'; 'international' ; 'Clarke 1866'; 'Clarke 1880'; 'everest';  'bessel'; 'krasovsky'; 'iau65'; 'wgs84'; 'other'
% 
% Examples
%   a = cl_carto
%   [Type, Retour] = selectEllipsoide(a)
%   a = set(a, 'Ellipsoide.Type', Type)
%
% See also cl_carto cl_carto/demiPetitAxe cl_carto/carre_excentricite Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [Type, validation] = selectEllipsoide(this)

if this.Ellipsoide.Type == 1
    InitialValue = 1;
else
    InitialValue = this.Ellipsoide.Type;
end
str1 = 'Ellipso�de';
str2 = 'Ellipsoid';
[Type, validation] = my_listdlg(Lang(str1,str2), this.MapTbx.Ellipsoide.strType(1:end-1), 'SelectionMode', 'Single', 'InitialValue', InitialValue);
