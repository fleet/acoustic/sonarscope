% Selection de la projection.
%
% Syntax
%   Type = selectProjection(a)
%
% Input Arguments
%   a       : Une instance de la classe cl_carto
%   LonMean : [] ou Longitude moyenne
%
% Output Arguments
%   typeProj : Identifiant de l'ellipsoide  '2=Mercator' | '3=UTM' | %   '4=Lambert1' | '5=Lambert2' | '6=Lambert3' |
%   '7=Lambert4' | '8=Stereographique polaire' | '9=Other'
%   structEPSG : structure comportant le code EPSG et la cha�ne PROJ4
%
% Examples
%   a = cl_carto
%
%   [typeProjection, structEPSG, flag] = selectProjection(a)
%   [typeProjection, structEPSG, flag] = selectProjection(a, 'NoGeodetic', 1)
%
% See also cl_carto cl_carto/demiPetitAxe cl_carto/carre_excentricite Authors
% Authors : JMA
% -------------------------------------------------------------------------

% typeProjLambert : rustine rajout�e par JMA le 27/04/2016
function [flag, typeProj, structEPSG, typeProjLambert] = selectProjection(this, LonMean, varargin)

[varargin, NoGeodetic] = getPropertyValue(varargin, 'NoGeodetic', 0);
[varargin, InitialProjection] = getPropertyValue(varargin, 'InitialProjection', []); %#ok<ASGLU>

structEPSG      = [];
typeProjLambert = [];

if isempty(InitialProjection)
    if NoGeodetic
        if this.Projection.Type == 1
            InitialValue = 2; % 1; % Modifi� le 4/1/2010
        else
            InitialValue = this.Projection.Type-1;
        end
        [typeProj, flag] = my_listdlg('Projection',  this.MapTbx.Projection.strType(2:end-1), 'SelectionMode', ...
            'Single', 'InitialValue', InitialValue);
        if ~flag
            return
        end
        typeProj = typeProj + 1;
    else
        if this.Projection.Type == 1
            InitialValue = 3; % 1; % Modifi� le 4/1/2010
        else
            InitialValue = this.Projection.Type;
        end
        [typeProj, flag] = my_listdlg('Projection', this.MapTbx.Projection.strType(1:end-1), 'SelectionMode', ...
            'Single', 'InitialValue', InitialValue);
        if ~flag
            return
        end
    end
else
    typeProj = InitialProjection;
end

switch typeProj
    case 3
        [flag, ~, UTMZone] = selectProjectionUTM(this, LonMean);
        if ~flag
            return
        end
        if strcmp(UTMZone, 'NZTM2000')
            strProjUTM = 'NZGD2000 / New Zealand Transverse Mercator';
        else
            strProjUTM = ['WGS 84 / UTM zone ' UTMZone];
        end
        [flag, structEPSG, ~] = getParamsProjFromESRIFile('CsLabelName', strProjUTM);
%         this = set(this, 'CodeEPSG', structEPSG.code);
%         if CentralMeridian ~= 0
%             set(this, 'MapTbx.Projection.UTM.Central_meridian', CentralMeridian);
%         end
        
    case 4
        [typeProjLambert, flag] = selectProjectionLambert(this);
        if ~flag
            return
        end
        CodeEPSG = this.MapTbx.Projection.Lambert.strCode{typeProjLambert};
        [flag, structEPSG, ~] = getParamsProjFromESRIFile('CodeEPSG', CodeEPSG);
        
    otherwise
        CodeEPSG = this.MapTbx.Projection.strCode{typeProj};
        [flag, structEPSG, ~] = getParamsProjFromESRIFile('CodeEPSG', CodeEPSG);
end
