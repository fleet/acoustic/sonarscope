% Selection de la projection.
%
% Syntax
%   Type = selectProjectionLambert(a) 
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   Type : Identifiant de la region Lambert  {'1=France 1}' | '2=France 2' | '3=France 3' | '4=France 4'                                                              
% 
% Examples
%   a = cl_carto
%   [Type,  Retour] = selectProjectionLambert(a)
%
% See also cl_carto cl_carto/demiPetitAxe cl_carto/carre_excentricite Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [Type, validation] = selectProjectionLambert(this)
[Type, validation] = my_listdlg('Region', this.Projection.Lambert.strType, 'SelectionMode', 'Single', ...
    'InitialValue', 1);
