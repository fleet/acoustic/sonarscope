% Selection de la projection.
%
% Syntax
%   Type = selectProjectionMercator(a) 
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   LatMean : latitude moyenne de la zone � reprojeter.
%   LonMean : longitude moyenne de la zone � reprojeter.                                                             
% 
% Examples
%   a = cl_carto_MaptTbx;
%   LatMean = 48.1;
%   LonMean = -4.5;
%   [long_merid_orig, lat_ech_cons, X0, Y0, flag] = selectProjectionMercator(a, LatMean, LonMean)
%
% See also cl_carto cl_carto/demiPetitAxe cl_carto/carre_excentricite Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [long_merid_orig, lat_ech_cons, X0, Y0, flag] = selectProjectionMercator(this, LatMean, LonMean) %#ok

long_merid_orig = [];
lat_ech_cons    = [];
X0              = [];
Y0              = [];

p    = ClParametre('Name', 'Central Meridien',         'Unit', 'deg', 'Value', LonMean, 'MinValue', -180, 'MaxValue', 180);
p(2) = ClParametre('Name', 'Conserved scale latitude', 'Unit', 'deg', 'Value', LatMean, 'MinValue', -90,  'MaxValue', 90);
p(3) = ClParametre('Name', 'X0',                       'Unit', 'm',   'Value', 0);
p(4) = ClParametre('Name', 'Y0',                       'Unit', 'm',   'Value', 0);
str1 = 'Saisie des param�tres de la projection Mercator';
str2 = 'Input of Mercator projection';
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

long_merid_orig = val(1);
lat_ech_cons    = val(2);
X0              = val(3);
Y0              = val(4);