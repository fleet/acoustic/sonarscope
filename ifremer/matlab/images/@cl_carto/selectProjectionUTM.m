% Selection de la projection.
%
% Syntax
%   [flag, Fuseau, UTMZone] = selectProjectionUTM(a) 
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   flag    : 1=OK, 0=KO
%   Fuseau  : 
%   UTMZone : 
% 
% Examples
%   a = cl_carto;
%   LonMean = -4.5;
%   [flag, Fuseau, UTMZone] = selectProjectionUTM(a, LonMean)
%
% See also cl_carto cl_carto/demiPetitAxe cl_carto/carre_excentricite Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [flag, Fuseau, UTMZone] = selectProjectionUTM(this, LonMean)

flag    = 0; %#ok<NASGU>
Fuseau  = []; %#ok<NASGU>
UTMZone = [];

for k=1:60
    str{k} = sprintf('Zone %d : [%d %d]', k, (k-1:k)*6 - 180);  %#ok<AGROW>
end

for k=1:60
    str{60+k} = sprintf('Zone %d : [%d %d]', 60+k, (k-1:k)*6 - 3 - 180);
end
str{end+1} = 'NZTM2000';

if isempty(LonMean)
    InitialValue = 1;
else
    InitialValue = floor((LonMean+180)/6 + 1);
end

InitialValue = 1 + mod(InitialValue-1+60, 60);

[Fuseau, flag] = my_listdlg('UTM Zone', str, 'SelectionMode', 'Single', 'InitialValue', InitialValue);

if ~flag
    return
end
% Assignation de la zone recalculée depuis les bornes UTM (Central_meridian et Fuseau).
[flag, UTMZone] = my_findZone(this, 'Fuseau', Fuseau);
