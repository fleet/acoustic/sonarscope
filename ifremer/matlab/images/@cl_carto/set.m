% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   a = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_carto
%
% Name-Value Pair Arguments
%  Cf. cl_carto
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   a  : Instance de la classe
%
% Examples
%   a = cl_carto;
%
%   set(a, )
%   a
%   a = set(a, )
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, this.Projection.Type] = getPropertyValue(varargin, 'Projection.Type', this.Projection.Type); % D�plac� ici par JMA le 02/11/2015
[varargin, CodeEPSGProjection]   = getPropertyValue(varargin, 'CodeEPSG',        []);
[varargin, mstruct]              = getPropertyValue(varargin, 'MStruct',         this.MapTbx.MStruct);
[varargin, Mute]                 = getPropertyValue(varargin, 'Mute',            0);

varargout = {};

%% Affectation des valeurs selon le code EPSG

if isempty(CodeEPSGProjection)
    % Fonctionne si on n'est pas en UTM ou Lambert avec les multiples
    % d�clinaisons de code EPSG.
    % pour assignation des propri�t�s par d�faut li�es � la projection
    % par d�faut (4326).
    
    CodeEPSGProjection = this.MapTbx.Projection.strCode{this.Projection.Type};
    
% % %     CodeEPSGProjection =  this.MapTbx.Projection.CodeEPSG;
    if isempty(CodeEPSGProjection) || strcmp(CodeEPSGProjection, '--') || strcmp(CodeEPSGProjection, 'other')
        CodeEPSGProjection = num2str(this.MapTbx.Projection.CodeEPSG);
    end        
else
    % Propri�t� en char ou double.
    if isa(CodeEPSGProjection, 'double')
        CodeEPSGProjection = num2str(CodeEPSGProjection);
    end   
    [flag, mstruct, deltaToWGS84, strProj4] = convertEPSG2Mstruct('CodeEPSG', CodeEPSGProjection);
    if ~flag || isempty(mstruct)
        str1 = 'Probl�me de positionnement de la projection via code EPSG.';
        str2 = 'Problem in EPSG assignment for projection';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    this.MapTbx.Projection.CodeEPSG     = CodeEPSGProjection;
    this.MapTbx.MStruct                 = mstruct;
    this.MapTbx.Projection.DeltaToWGS84 = deltaToWGS84;
    this.MapTbx.Projection.Proj4        = strProj4;
end

if isempty(mstruct)
    [flag, mstruct, deltaToWGS84, strProj4] = convertEPSG2Mstruct('CodeEPSG', CodeEPSGProjection);
    if ~flag || isempty(mstruct)
        str1 = 'Probl�me de positionnement de la projection via code EPSG.';
        str2 = 'Problem in EPSG assignment for projection';
        my_warndlg(Lang(str1,str2), 1);
        return
    end   
else
    deltaToWGS84 = this.MapTbx.Projection.DeltaToWGS84;
    strProj4     = this.MapTbx.Projection.Proj4;   
end

% obj.MapTbx.Projection.Proj4        = strProj4;
% obj.MapTbx.Projection.DeltaToWGS84 = deltaToWGS84;
this.MapTbx.Projection.Proj4        = strProj4; % Modifi� par JMA le 27/04/2016
this.MapTbx.Projection.DeltaToWGS84 = deltaToWGS84;

%% On v�rifie que l'instance est bien de dimension 1

varargout = {};
if length(this) ~= 1
    str1 = 'cl_carto/get : L''instance doit �tre de dimension 1';
    str2 = 'cl_carto/get : Dimension of instance must be 1';
    my_warndlg(Lang(str1,str2), 1);
    return
end

% [varargin, this.Projection.Type] = getPropertyValue(varargin, 'Projection.Type', this.Projection.Type); % Comment� par JMA le 02/11/2015

% Traitement pour le cas d'une instance o� le Geoid est distint de la Mappping Structure.
if ~isfield(this.MapTbx.MStruct, 'geoid')
    this.MapTbx.MStruct.geoid = this.MapTbx.Geoid;
end

%% Assignation des propri�t�s anciennes

[varargin, X] = getPropertyValue(varargin, 'Projection.Mercator.long_merid_orig', []);
if ~isempty(X)
    this.Projection.Mercator.long_merid_orig = X;
    if isempty(mstruct.origin)
        mstruct.origin    = [0 X 0];
    else
        mstruct.origin(2) = X;
    end
end
[varargin, X] = getPropertyValue(varargin, 'Projection.Mercator.lat_ech_cons', []);
if ~isempty(X)
    this.Projection.Mercator.lat_ech_cons = X;
    mstruct.mapparallels  = X;
    mstruct.nparallels    = 1;
    if isempty(mstruct.origin)
        mstruct.origin    = [X 0 0];
    else
        mstruct.origin(1) = X;
    end
end
[varargin, X] = getPropertyValue(varargin, 'Projection.Mercator.Y0', []);
if ~isempty(X)
    this.Projection.Mercator.Y0 = X;
    mstruct.falsenorthing       = X;
end
[varargin, X] = getPropertyValue(varargin, 'Projection.Mercator.X0', []);
if ~isempty(X)
    this.Projection.Mercator.X0 = X;
    mstruct.falseeasting        = X;
end



[varargin, X] = getPropertyValue(varargin, 'Projection.UTM.Fuseau', []);
if ~isempty(X) && (X ~= 0)
    Fuseau                               = X;
    this.Projection.UTM.Fuseau           = Fuseau;
    this.Projection.UTM.Central_meridian = (Fuseau - 31) * 6;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.UTM.Hemisphere', []);
if ~isempty(X)
    Hemi = X;
    this.Projection.UTM.Hemisphere = Hemi;
    
    % Test JMA le 14/03/2019
%     if strcmpi(Hemi, 'S')
%         this.Projection.UTM.Y0 = 10000000;
%         mstruct.falsenorthing  = 10000000;
%     end
end

[varargin, X] = getPropertyValue(varargin, 'Projection.UTM.Central_meridian', []);
if ~isempty(X)
    this.Projection.UTM.Central_meridian = X;
    this.Projection.UTM.Fuseau = floor((X/6 + 31));
end

[varargin, X] = getPropertyValue(varargin, 'Projection.UTM.Central_parallel', []);
if ~isempty(X)
    this.Projection.UTM.Central_parallel = X;
    if X > 0
        this.Projection.UTM.Hemisphere = 'N';
    else
        this.Projection.UTM.Hemisphere = 'S';
    end   
end

[varargin, X] = getPropertyValue(varargin, 'Projection.UTM.X0', []);
if ~isempty(X)
    this.Projection.UTM.X0 = X;
    mstruct.falseeasting   = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.UTM.Y0', []);
if ~isempty(X)
    this.Projection.UTM.Y0 = X;
    mstruct.falsenorthing  = X;
end

% Recherche du code EPSG en fonction des attributs de l'ancienne classe cl_carto.
if this.Projection.Type == 3 && ~isempty(this.Projection.UTM.Fuseau) && ~isempty(this.Projection.UTM.Hemisphere)
    [flag, mstruct, deltaToWGS84, strProj4, CodeEPSGUTM] = getUTMProjection(this.Projection); %#ok<ASGLU>
    CodeEPSGProjection = CodeEPSGUTM;
end
    
% Lambert 1, 2, 3, 4, 93 et 2 Etendu
[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.Type', []);
if ~isempty(X)
    this.Projection.Lambert.Type = X;
    [flag, mstruct, deltaToWGS84, strProj4, CodeEPSGLambert] = getLambertProjection(this.Projection); %#ok<ASGLU>
    CodeEPSGProjection = CodeEPSGLambert;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.strType', []);
if ~isempty(X)
    this.MapTbx.MStruct.mapprojection = X;
    mstruct.mapprojection             = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.first_paral', []);
if ~isempty(X)
    this.Projection.Lambert.first_paral = X;
    mstruct.mapparallels(1)             = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.second_paral', []);
if ~isempty(X)
    this.Projection.Lambert.second_paral = X;
    mstruct.mapparallels(2)              = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.long_merid_orig', []);
if ~isempty(X)
    this.Projection.Lambert.long_merid_orig = X;
    if isempty(mstruct.origin)
        mstruct.origin = [0 X 0];
    else
        mstruct.origin(2) = X;
    end
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.X0', []);
if ~isempty(X)
    this.Projection.Lambert.X0 = X;
    mstruct.falseeasting       = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.Y0', []);
if ~isempty(X)
    this.Projection.Lambert.Y0 = X;
    mstruct.falsenorthing      = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.scale_factor', []);
if ~isempty(X)
    this.Projection.Lambert.scale_factor = X;
    mstruct.scalefactor                  = X;
end

% D�but rajout JMA le 19/06/2017
[varargin, X] = getPropertyValue(varargin, 'Projection.Lambert.lat_ech_cons', []);
if ~isempty(X)
    mstruct.nparallels  = 1;
    this.Projection.Lambert.lat_ech_cons = X;
    mstruct.origin(1)   = X;
end
% Fin rajout JMA le 19/06/2017



% Stereo
[varargin, X] = getPropertyValue(varargin, 'Projection.Stereo.long_merid_horiz', []);
if ~isempty(X)
    this.Projection.Stereo.long_merid_horiz = X;
    mstruct.origin(2) = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Stereo.lat_ech_cons', []);
if ~isempty(X)
    this.Projection.Stereo.lat_ech_cons = X;
    mstruct.origin(1) = X;
end

[varargin, X] = getPropertyValue(varargin, 'Projection.Stereo.hemisphere', []);
if ~isempty(X)
    this.Projection.Stereo.hemisphere = X;
    if strcmp(this.Projection.Stereo.hemisphere, 'N')
        mstruct.zone = 'north';
    else
        mstruct.zone = 'south';
    end
end


% Cylindrique Equidistant
[varargin, X] = getPropertyValue(varargin, 'Projection.CylEquiDist.long_merid_orig', []);
if ~isempty(X)
    this.Projection.CylEquiDist.long_merid_orig = X;
    % TODO : mstruct.???
end

[varargin, X] = getPropertyValue(varargin, 'Projection.CylEquiDist.lat_ech_cons', []);
if ~isempty(X)
    this.Projection.CylEquiDist.lat_ech_cons  = X;
    % TODO : mstruct.???
end

%% Affectation de la structure de la Mapping Toolbox.

this.MapTbx.Projection.CodeEPSG        = CodeEPSGProjection;
this.MapTbx.Projection.DeltaToWGS84.DX = deltaToWGS84.DX;
this.MapTbx.Projection.DeltaToWGS84.DY = deltaToWGS84.DY;
this.MapTbx.Projection.DeltaToWGS84.DZ = deltaToWGS84.DZ;
this.MapTbx.Projection.DeltaToWGS84.RX = deltaToWGS84.RX;
this.MapTbx.Projection.DeltaToWGS84.RY = deltaToWGS84.RY;
this.MapTbx.Projection.DeltaToWGS84.RZ = deltaToWGS84.RZ;
this.MapTbx.Projection.DeltaToWGS84.M  = deltaToWGS84.M;
this.MapTbx.Projection.Proj4           = strProj4;

% Obligation de remettre en cha�ne de caract�re : cas du import_xml car
% interpr�tation des num�riques en tant que tel, ce qui fait planter la
% fonction intersect.
if ~iscellstr(this.MapTbx.Projection.strCode)
    for k=1:numel(this.MapTbx.Projection.strCode)
        if iscell(this.MapTbx.Projection.strCode(k))
            this.MapTbx.Projection.strCode{k} = num2str(this.MapTbx.Projection.strCode{k});
        end
    end
end

% Ajout JMA le 17/09/2017 pour fichier Marc Roche
if ~isfield(this.MapTbx.Projection, 'Lambert')
    C0 = cl_carto;
    this.MapTbx.Projection.Lambert = C0.MapTbx.Projection.Lambert;
end
% Fin ajout JMA le  17/09/2017

if ~iscellstr(this.MapTbx.Projection.Lambert.strCode)
    for k=1:numel(this.MapTbx.Projection.Lambert.strCode)
        if iscell(this.MapTbx.Projection.Lambert.strCode(k))
            this.MapTbx.Projection.Lambert.strCode{k} = num2str(this.MapTbx.Projection.Lambert.strCode{k});
        end
    end
end

%{
if ~iscellstr(this.MapTbx.Projection.UTM.strCode)
    % Ajout JMA le 08/02/2017
    % this.MapTbx.Projection.UTM.strCode a �t� relue lors de l'import du
    % fichier XML. Pourquoi le r�cup�re-t'on du XML alors que c'est une
    % constante de classe ?
    % On pourrait utiliser directement this.MapTbx.Projection.UTM.strCode = this.strCodeUTM
    if isnumeric(this.MapTbx.Projection.UTM.strCode)
        this.MapTbx.Projection.UTM.strCode = strsplit(num2str(this.MapTbx.Projection.UTM.strCode));
    else
        % Tout �a ne devrait plus servir � rien en principe si on reprend this.strCodeUTM
        for k=1:numel(this.MapTbx.Projection.UTM.strCode)
            if iscell(this.MapTbx.Projection.UTM.strCode(k))
                this.MapTbx.Projection.UTM.strCode{k} = num2str(this.MapTbx.Projection.UTM.strCode{k});
            end
        end
    end
end
%}
this.MapTbx.Projection.UTM.strCode = cl_carto.strCodeUTM;


if ~iscellstr(this.MapTbx.Ellipsoide.strCode)
    for k=1:numel(this.MapTbx.Ellipsoide.strCode)
        if iscell(this.MapTbx.Ellipsoide.strCode(k))
            this.MapTbx.Ellipsoide.strCode{k} = num2str(this.MapTbx.Ellipsoide.strCode{k});
        end
    end
end


if strcmp(CodeEPSGProjection, '2193') % cas projection NZTM2000
    this.Projection.Type                 = 3;
    this.Projection.UTM.Fuseau           = 121;
    this.Projection.UTM.Central_meridian = this.MapTbx.MStruct.origin(2);
    this.Projection.UTM.Central_parallel = this.MapTbx.MStruct.origin(1);
    this.Projection.UTM.Hemisphere       = 'S';
    this.Projection.UTM.X0               = this.MapTbx.MStruct.falseeasting;
    this.Projection.UTM.Y0               = this.MapTbx.MStruct.falsenorthing;
else
    % Recherche de l'indice de la projection dans celles autoris�es.
    % Gestion de la multiplicit� des sous-projections Lambert et UTM.
    
    % Ajout JMA le 26/11/2020
    if isfloat(CodeEPSGProjection)
        CodeEPSGProjection = num2str(CodeEPSGProjection);
    end
    
    [C, codeProj, ib] = intersect(this.MapTbx.Projection.strCode, CodeEPSGProjection); %#ok<ASGLU>
    if isempty(codeProj)
        [C, ia, ib] = intersect(this.MapTbx.Projection.Lambert.strCode, CodeEPSGProjection); %#ok<ASGLU>
        if isempty(ia)
            [C, ia, ib] = intersect(this.MapTbx.Projection.UTM.strCode, CodeEPSGProjection); %#ok<ASGLU>
            if ~isempty(ia)
                codeProj = 3; % Type UTM general;
            end
        else
            codeProj = 4; % Type Lambert general;
            this.Projection.Lambert.Type = ia; % Type Lambert sp�cifique.
        end
    end
    
    if isempty(codeProj)
        this.Projection.Type = 7; % Projection non-d�tect�e.
        % Test� pour le cas belge (EPSG =21500);
        % A adapter selon les cas si on conserve les anciennes propri�t�s.
        if strcmp(mstruct.mapprojection, 'lambertstd')
            this.Projection.Lambert.Type            = 13;
            this.Projection.Lambert.first_paral     = mstruct.mapparallels(1);
            this.Projection.Lambert.second_paral    = mstruct.mapparallels(2);
            this.Projection.Lambert.long_merid_orig = mstruct.origin(2);
            this.Projection.Lambert.X0              = mstruct.falseeasting;
            this.Projection.Lambert.Y0              = mstruct.falsenorthing;
            this.Projection.Lambert.scale_factor    = mstruct.scalefactor;
            this.MapTbx.Projection.Lambert.Code     = 10;
        end
        
% Modif JMA le 04/12/2020 pour donn�es Marc Roche (voir mail du 04/12/2020)
    else
        this.Projection.Type = codeProj; % TODO : M�me si je commente cela le r�sultat est le m�me !!!
    end
end

% Recherche de l'indice de l'ellipsoide dans celles autoris�es.
% TODO : GLU, revoir l'affectation du Geoid Code par une autre propri�t� !!!
if isfield(mstruct, 'geoid')
    if isfield(mstruct.geoid, 'Code')
        [C, ia, ib] = intersect(this.MapTbx.Ellipsoide.strCode, num2str(mstruct.geoid.Code)); %#ok<ASGLU>
        this.MapTbx.Ellipsoide.CodeEPSG  = num2str(mstruct.geoid.Code);
        this.MapTbx.Geoid                = mstruct.geoid;
        this.Ellipsoide.Type             = ia;
        this.Ellipsoide.DemiGrandAxe     = mstruct.geoid.SemimajorAxis;
        this.Ellipsoide.DemiPetitAxe     = mstruct.geoid.SemiminorAxis;
        this.Ellipsoide.Aplatissement    = mstruct.geoid.InverseFlattening;
        this.Ellipsoide.Excentricite     = mstruct.geoid.Eccentricity;
    end
end

%% Affectation des valeurs

[varargin, TypeEllipsoide] = getPropertyValue(varargin, 'Ellipsoide.Type', []);
if ~isempty(TypeEllipsoide)
    this.Ellipsoide.Type = TypeEllipsoide;
    if TypeEllipsoide == 1  %'Unknown'
        this.Ellipsoide.DemiGrandAxe = 0;
        this.Ellipsoide.Excentricite = 0;
        
    elseif (TypeEllipsoide >= length(this.Ellipsoide.strType)) || (TypeEllipsoide <= 0) %'other'
        str = ['TypeEllipsoide mot reckognized : ' num2str(TypeEllipsoide)];
        my_warndlg(['cl_carto/set : ', str], 1);
        
    else
        % Calcul de l'ellipoide de r�f�rence.
        refEllipsoide = this.Ellipsoide.strType{TypeEllipsoide};
        refEllipsoide = lower(refEllipsoide);
        % Cas de l'ellipsoide clarke80_ign qui n'est pas reconnu par son
        % label.
        
        % D�but ajout JMA le 04/11/2015
        if strcmp(refEllipsoide, 'ntf')
            refEllipsoide = 'clarke80';
            if ~Mute
                str1 = 'ATTENTION : ici j''ai remplac� l''ellipso�de "NTF" par "clarke80".';
                str2 = 'WARNING : I have replaced "NTF" ellipsoid by "clarke80".';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'cl_carto:Ellipsoid_NTF_Not_recognized');
            end
        end
        % Fin ajout JMA le 04/11/2015
        
        try
            mstruct.geoid = referenceEllipsoid(refEllipsoide, 'meters');
        catch
            refEllipsoide = this.MapTbx.Ellipsoide.strCode{TypeEllipsoide};
%             if ~strcmp(mstruct.geoid, '--')
            if ~strcmp(refEllipsoide, '--') % Modif JMA le 10/07/2014 import fichier Y:\private\shom\7150_2009\MOCOSED2009\IMA_MOCOSED2009_PP_75m_UTM27N.tif
%                 refEllipsoide = str2num(refEllipsoide);
                refEllipsoide = str2double(refEllipsoide);
                mstruct.geoid = referenceEllipsoid(refEllipsoide,'meters');
            end
        end
        
        % Affectation des propri�t�s du Geoide.
        this.Ellipsoide.DemiGrandAxe = mstruct.geoid.SemimajorAxis;
        this.Ellipsoide.Excentricite = mstruct.geoid.Eccentricity;
    end
else
    mstruct.geoid = this.MapTbx.Geoid;
end

%% Assignation des propri�t�s nouvelles

[varargin, X] = getPropertyValue(varargin, 'Proj4', []);
if ~isempty(X)
    this.MapTbx.Projection.Proj4 = X;
end

[varargin, X] = getPropertyValue(varargin, 'MapTbx.Projection.UTM.Central_meridian', []);
if ~isempty(X)
    % Pas de mise � jour du fuseau car on peut �tre parti d'une projection
    % type Total d�cal�e de 3 des zones habituelles.
    this.MapTbx.MStruct.origin(2) = X;
end

% Affectation de la structure apr�s modification.
this.MapTbx.Geoid   = mstruct.geoid;
mstruct             = rmfield(mstruct, 'geoid');
this.MapTbx.MStruct = mstruct;

%% Assignation des propri�t�s de l'objet si celui-ci est
% recharg� depuis un fichier XML.

[varargin, X] = getPropertyValue(varargin, 'Projection', []);
if ~isempty(X)
    this.Projection = X;
end 
[varargin, X] = getPropertyValue(varargin, 'Ellipsoide', []);
if ~isempty(X)
    this.Ellipsoide = X;
end 
[varargin, X] = getPropertyValue(varargin, 'MapTbx', []);
if ~isempty(X)
    this.MapTbx = X;
end 
            
%% V�rification si varargin est vide

if ~isempty(varargin)
    str1 = sprintf('"%s" n''est pas une propri�t�, envoyez le message d''erreur � sonarscope@ifremer.fr.', num2str(varargin{1}));
    str2 = sprintf('"%s" is not a property of the class, please report to sonarscope@ifremer.fr.', num2str(varargin{1}));
    my_warndlg(['cl_carto:set  :  ' Lang(str1,str2)], 0);
end

%% Mise � jour de l'instance

this = init(this);

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
