% TODO : terminer les cas Mercator, Lambert, ...

function str = shortDescription(this)

str = ['Ellipsoid=' this.Ellipsoide.strType{this.Ellipsoide.Type}];
switch this.Projection.Type
    case 2 % Mercator
        str = sprintf('%s;Projection=Mercator;Meridien=%f;Lat=%f', str, this.Projection.Mercator.long_merid_orig, this.Projection.Mercator.lat_ech_cons);
        
    case 3 % UTM
        str = sprintf('%s;Projection=%sUTM%d', str, this.Projection.UTM.Hemisphere, this.Projection.UTM.Fuseau);
        
    case 4 % Lambert
        str = sprintf('%s;Projection=Lambert;Zone=%s', str, strrep(this.Projection.Lambert.strType{this.Projection.Lambert.Type}, ' ', ''));
        
    case 5 % Stereographique polaire
        str = sprintf('Report to JMA please : cl_carto/shortDescription not terminated for projection %d', this.Projection.Type);
        my_warndlg(str, 1);
        str = [];
    case 6 % Cylyndrique Equidistant
        str = sprintf('Report to JMA please : cl_carto/shortDescription not terminated for projection %d', this.Projection.Type);
        my_warndlg(str, 1);
        str = [];
    case 7 % Other
        str = [];
    otherwise
        str = [];
end
        