% Mise � jour des fichiers ERVIEwer de recensement des projections.
%
% Syntax
%   write_mercator_ermapperFiles(a)
% 
% Input Arguments 
%   a : Une instance de la classe cl_carto
%
% Output Arguments
%   Code : Code de projection correspondante dans ermapper
% 
% Examples
%   a = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2)
%   Code = codeProjectionErmapper(a)
%   write_mercator_ermapperFiles(a, Code) % Code = MR 0.00N_wgs84
%
%   a = cl_carto('Ellipsoide.Type', 2, 'Projection.Type', 2, 'Projection.Mercator.lat_ech_cons', 48)
%   Code = codeProjectionErmapper(a)
%   write_mercator_ermapperFiles(a, Code) % Code = MR48.00N_wgs72
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function write_mercator_ermapperFiles(this, Code)

global VisualisateurImageExterne %#ok<GVMIS> 
global IfrTbxResources %#ok<GVMIS>

if isempty(VisualisateurImageExterne)
    nomFic = fullfile(IfrTbxResources, 'Resources.ini');
    %verifNameLength(nomFic)
    if exist(nomFic, 'file')
        loadVariablesEnv;
    end
end

Viewer = VisualisateurImageExterne;
if ~iscell(Viewer)
    Viewer = {Viewer};
end

%% Tentative de mise � jour des fichier du logiciel ErViewer

for k=1:length(Viewer)
    if strfind(Viewer{k}, 'ERViewer.exe')
        nomDir = fileparts(Viewer{k});
        nomDir = fileparts(nomDir);
        nomDir = fullfile(nomDir, 'GDT_Data');
        majFiles(this, nomDir, Code);
    end
end

%% Tentative de mise � jour des fichier du logiciel ErMapper

nomFic = 'C:\ERMapper70\GDT_DATA';
majFiles(this, nomFic, Code);


function flag = majFiles(this, nomDir, Code)

% ---------------------------------------------------------
% Ajout de la d�finition de la projection dans la liste des
% projections disponibles (fichier project.dat)

flag = 0;
nomFic = fullfile(nomDir, 'project.dat');
if exist(nomFic, 'file') == 2

    % --------------------------------------
    % Test si la projection est d�j� d�finie

    flag = exist_Code(nomFic, Code);

    % --------------------------------------
    % Ajout de la d�finition dans le fichier

    if ~flag
        flag = append_Code(nomFic, Code);
        if ~flag
            str = sprintf('Impossible to append "%d" in %s', Code, nomFic);
            my_warndlg(str, 0, 'Tag', 'ImpossibleToAppendCodeInErMpperFiles', 'TimeDelay', 10);
            return
        end
    end
end

% ----------------------------------------------------------
% Ajout des param�tres de la projection dans la listre des
% projections disponibles (fichier mercator.dat)

nomFic = fullfile(nomDir, 'mercator.dat');
if exist(nomFic, 'file') == 2

    % --------------------------------------
    % Test si la projection est d�j� d�finie

    flag = exist_Code(nomFic, Code);

    % --------------------------------------
    % Ajout de la d�finition dans le fichier

    if ~flag
        flag = append_CodeParams(this, nomFic, Code);
        if ~flag
            str = sprintf('Impossible to append "%d" in %s', Code, nomFic);
            my_warndlg(str, 0, 'Tag', 'ImpossibleToAppendCodeInErMpperFiles', 'TimeDelay', 10);
            return
        end
    end
end


%% Test si la projection est d�j� d�finie

function flag = exist_Code(nomFic, Code)

flag = 0;
fid = fopen(nomFic);
if fid <= 0
    return
end
while feof(fid) == 0
    L = fgetl(fid);
    if length(L) >= 1
        if contains(L, Code)
            flag = 1;
            fclose(fid);
            return
        end
    end
end
fclose(fid);


%% Ajout de la d�finition dans le fichier

function flag = append_Code(nomFic, Code)

flag = 0;
fid = fopen(nomFic, 'r+');
if fid <= 0
    return
end
flag = fseek(fid, 0, 'eof');
str = sprintf('%s ,mercator,1.0 ,0.0,0.0,0.0 ,1', Code);
count = fprintf(fid, '%s\n', str);
if count ~= 0
    flag = 1;
end
fclose(fid);
% msg = sprintf('Line\n"%s"\nhas been added to file\n"%s"\n\nIf you want to import this image in ErViewer, ErMapper or ArcGis on an other computer copy this line in the corresponding file on this computer.', ...
%     str, nomFic);
% my_warndlg(msg, 1);


%% Ajout des param�tres de la projection dans le fichier

function flag = append_CodeParams(this, nomFic, Code)

flag = 0;
fid = fopen(nomFic, 'r+');
if fid <= 0
    return
end

lat_ech_cons = str2double(Code(3:7));
    
NomEllipsoide = upper(this.MapTbx.Ellipsoide.strType{this.Ellipsoide.Type});
if ~strcmp(NomEllipsoide, Code(10:end))
    str = sprintf('The ellipsoid defined for %s does not correspond to %s', Code, NomEllipsoide);
    my_warndlg(str, 1, 'Tag', 'BugDecodageEllipsoid');
end
    
E  = this.MapTbx.Geoid.Eccentricity;
E2 = E ^ 2;
L0   = this.MapTbx.MStruct.origin(1) * (pi/180); % this.Projection.Mercator.lat_ech_cons 
SL   = sin(L0) ^ 2;
NORM = sqrt((1 - E2*SL) / (1-SL));


flag = fseek(fid, 0, 'eof');
str = sprintf('%s ,0.0 ,0.0 ,%16.14f,0.0 ,%19.16f,0.0 ,0.0,0.0,1', ...
    Code, 1/NORM, lat_ech_cons*(pi/180));
count = fprintf(fid, '%s\n', str);
if count ~= 0
    flag = 1;
end
fclose(fid);
% msg = sprintf('Line\n"%s"\nhas been added to file\n"%s"\n\nIf you want to import this image in ErViewer, ErMapper or ArcGis on another computer copy this lines in the corresponding file on this computer.', ...
%     str, nomFic);
% my_warndlg(msg, 1);
