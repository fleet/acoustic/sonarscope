% Passage de coordonn�es cartographiques � coordonn�es g�ographiques
%
% Syntax
%   [lat, lon] = xy2latlon(Carto, x, y)
%
% Input Arguments
%   Carto : Instance de cartographie
%   x  : Description du parametre (m).
%   y  : Description du parametre (m).
%
% Output Arguments
%   lat   : Latitudes (deg d�cimaux)
%   lon   : Longitudes (deg d�cimaux)
%
% Examples
%   Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4)
%   [lat, lon] = xy2latlon(Carto, 700000, 6600000)
%   [x, y] = latlon2xy(Carto, lat, lon)
%
%   [lat, lon] = xy2latlon(Carto, x, y)
%   [lat, lon] = xy2latlon(Carto, 1160000, 4313000)
%
% See also cl_carto xy2latlon Authors
% References : CARAIBES(R)
% Authors : JMA
%-------------------------------------------------------------------------------

function [lat, lon] = xy2latlon(this, x, y)

x = x(:,:);
y = y(:,:);

lat = [];
lon = [];

% Rassemblage du Geoide au syst�me de projection.
MStruct = this.MapTbx.MStruct;
Geoid   = this.MapTbx.Geoid;
MStruct.geoid = Geoid;
switch lower(this.MapTbx.MStruct.mapprojection)
    case {'geodetic'; 'utm'; 'lambertstd'; 'tranmerc'}
        [lat, lon] = minvtran(MStruct, x, y);
    case {'mercator'}
        [lat, lon] = projinv(MStruct, x, y);
    otherwise
        disp('pas fait');
end

% Translations for 3-parameter datum transformation from NTF to wgs84
% grs80 pour LAMBERT 93
% clarke80_IGN pour les autres LAMBERT
% wgs84 pour la plupart des autres cas.
codeEPSGnum = str2num(this.MapTbx.Ellipsoide.CodeEPSG); %#ok<ST2NM>
% On ne transforme les coordonn�es que si l'ellipsoid de d�part n'est pas
% wgs84 (ni grs80)
if codeEPSGnum ~= 7030 && codeEPSGnum ~= 7019
    targetSpheroid = referenceEllipsoid(codeEPSGnum); 
    sourceSpheroid = wgs84Ellipsoid;
    [flag, this, lat, lon] = latlon2latlonInSpheroidTarget(this, lat, lon, sourceSpheroid, targetSpheroid); %#ok<ASGLU>
end


%{
referenceEllipsoid('clarke80') referenceEllipsoid(7012)
referenceEllipsoid('grs80')    referenceEllipsoid(7019)
referenceEllipsoid('wgs84')    referenceEllipsoid(7030) equivalent � wgs84Ellipsoid

%   EPSG Code   Short Name        Long Name
%   ---------   ----------        ---------
%   ----        'unitsphere'     'Unit Sphere'
%   7035        'sphere'         'Spherical Earth'
%   7019        'grs80'          'Geodetic Reference System 1980'
%   7030        'wgs84',         'World Geodetic System 1984'
%   7015        'everest'        'Everest 1830'
%   7004        'bessel'         'Bessel 1841'
%   7001        'airy1830'       'Airy 1830'
%   7002        'airy1849'       'Airy Modified 1849'
%   7008        'clarke66'       'Clarke 1866'
%   7012        'clarke80'       'Clarke 1880'
%   7022        'international'  'International 1924'
%   7024        'krasovsky'      'Krasovsky 1940'
%   7043        'wgs72'          'World Geodetic System 1972'
%   ----        'wgs60'          'World Geodetic System 1960'
%   ----        'iau65'          'International Astronomical Union 1965'
%   ----        'wgs66'          'World Geodetic System 1966'
%   ----        'iau68'          'International Astronomical Union 1968'
%   7030        'earth'          'World Geodetic System 1984'
%}
