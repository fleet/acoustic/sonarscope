% Constructeur de cl_ermapper (header des fichiers ER-Mapper)
%
% Syntax
%   a = cl_ermapper(...)
% 
% Name-Value Pair Arguments
%   FicIn : Nom du fichier de description d'image ER-Mapper
%  
% Output Arguments 
%   a : instance de cl_ermapper
%
% Examples
%   a = cl_ermapper('FicIn', '/home1/langevin/perso/augustin/tmp/SPOT_XS.ERS')
%
% See also cl_ermapper/editobj cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = cl_ermapper(varargin)

%% D�finition de la structure

this.Image = [];

%% Super classe

that = cl_ermapper_ers([]);

%% Cr�ation de l'instance

this = class(this, 'cl_ermapper', that);

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
