% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : instance de cl_ermapper_ers
%
% Examples
%   a = cl_ermapper_ers('FicIn', '/home2/brekh/dataErMapper/DATASET/');
%   a
%
% See also cl_ermapper_ers Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

% --------------------------------------------
% Lecture et affichage des infos de l'instance

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
