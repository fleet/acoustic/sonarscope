% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de cl_ermapper
%
% Name-only Arguments
%    cf. cl_ermapper
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%
% See also cl_ermapper Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'Image'
            varargout{end+1} = this.Image; %#ok<AGROW>
        otherwise
            varargout{end+1} = get(this.cl_ermapper_ers, varargin{i}); %#ok<AGROW>
    end
end
