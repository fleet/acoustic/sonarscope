% Visualisation d'une image ER-Mapper
%
% Syntax
%   imagesc(a, ...)
% 
% Output Arguments 
%   a : instance de cl_ermapper
%
% Name-Value Pair Arguments
%  
% Examples
%   a = cl_ermapper('FicIn', '/home2/brekh/dataErMapper/DATASET/SPOT_XS_Minimum_Distance.ers')
%   imagesc(a)
%
% See also cl_ermapper Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function imagesc(this)

if isempty(this.Image)
    my_warndlg('cl_ermapper:imagesc L''image est vide', 0);
    return
end

nbRows          = get(this, 'NrOfLines');
nbColumns       = get(this, 'NrOfCellsPerLine');
nbSlides        = get(this, 'NrOfBands');
Eastings        = get(this, 'Eastings');
Northings       = get(this, 'Northings');
Xdimension      = get(this, 'Xdimension');
Ydimension      = get(this, 'Ydimension');
BandId          = get(this, 'BandId');
CoordinateType  = get(this, 'CoordinateType');

switch CoordinateType
    case 'EN'
        x = Eastings  + (0:(nbColumns-1)) * Xdimension;
        y = (0:(nbRows-1)) * Ydimension;
        y = Northings + y - max(y);
    case 'RAW'
        x = 1:nbColumns;
        y = 1:nbRows;
    otherwise
        str = sprintf('CoordinateType=%s pas encore traite', CoordinateType);
        my_warndlg(['cl_ermapper/imagesc' str], 1);
        return
end

if nbSlides == 3
    if  strcmpi(BandId(1).Value, 'Red') && ...
            strcmpi(BandId(2).Value, 'Green') && ...
            strcmpi(BandId(3).Value, 'Blue')
        flagRGB = 1;

    elseif  strcmpi(BandId(1).Value, 'Red Layer') && ...
            strcmpi(BandId(2).Value, 'Green Layer') && ...
            strcmpi(BandId(3).Value, 'Blue Layer')
        flagRGB = 1;

    elseif  strcmpi(BandId(1).Value, 'XS1') && ...
            strcmpi(BandId(2).Value, 'XS2') && ...
            strcmpi(BandId(3).Value, 'XS3')
        flagRGB = 1;
    else
        flagRGB = 0;
    end

else
    flagRGB = 0;
end

[~, nomFic] = fileparts(get(this.cl_ermapper_ers, 'FicIn'));
if flagRGB
    figure; image(x, y, uint8(this.Image));
    axis xy; axis equal; axis tight;
    h = title(nomFic);
    set(h, 'Interpreter', 'none')
else
    for k=1:nbSlides
        figure; imagesc(x, y, this.Image(:,:,k));
        axis xy; axis equal; axis tight;
        colormap(gray(256));
        h = title([nomFic ' : ' BandId(k).Value]);
        set(h, 'Interpreter', 'none')
    end
end

RegionInfo = get(this, 'RegionInfo');
%     figure;
for k1=1:length(RegionInfo)
    hold on

    if isempty(Eastings)
        Eastings  = 0;
        Northings = 0;
    end

    for k2=1:length(RegionInfo(k1).SubRegion)
        xy = RegionInfo(k1).SubRegion{k2};
        if ~isempty(xy)
            xy(end+1,:) = xy(1,:); %#ok<AGROW>
            X = Eastings  + xy(:,1) * Xdimension;
            Y = Northings - xy(:,2) * Ydimension;
            plot(X, Y, 'r')
            hold on
        end
    end
end
%     get(this.cl_ermapper_ers, 'RegionInfo')
