% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de cl_ermapper
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_ermapper Authors
% Authors : JMA
% VERSION  : $Id: char_instance.m,v 1.1 2003/09/08 15:35:32 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    10/07/2003 - JMA - creation
% ----------------------------------------------------------------------------

function str = char_instance(this)

% Maintenance Auto : try
 
    str = [];
    
    str{end+1} = sprintf('Image           <-> %s', num2strCode(this.Image));

    str{end+1} = sprintf('--- Heritage Images (cl_ermapper_ers) ---');
    str{end+1} = char(this.cl_ermapper_ers);
    
    % ---------------------------
    % Concatenation en une chaine
    
    str = cell2str(str);
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_ermapper', 'char_instance', lasterr);
% Maintenance Auto : end
