% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Name-Value Pair Arguments
%   cf. cl_image
%
% Output Arguments
%   this : instance initialisee
%
% Examples
%   a = cl_ermapper('FicIn', '/home2/brekh/dataErMapper/DATASET/SPOT_XS_Minimum_Distance.ers')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

% -----------------------------------
% Traitement des message de la classe

if ~isempty(varargin)
    this.cl_ermapper_ers = set(this.cl_ermapper_ers, varargin{:});
end

[varargin, Image] = getPropertyValue(varargin, 'Image', []); %#ok<ASGLU>

if isempty(Image)
    [nomDir, nomFic] = fileparts(get(this.cl_ermapper_ers, 'FicIn'));
    nomFicImage = fullfile(nomDir, nomFic);
    %     [flag, I] = import_ermapper_image(this, nomFicImage);
    [I, NullCellValue] = import_ermapper_image(this, nomFicImage);
    if ~isempty(I)
        if ~isa(I, 'double')
            I = single(I);
        end
        I(I == NullCellValue) = NaN;
        this.Image = I;
    else
        this(:) = [];
    end
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
