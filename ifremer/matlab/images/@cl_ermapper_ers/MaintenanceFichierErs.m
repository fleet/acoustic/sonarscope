function MaintenanceFichierErs(this)

fid = fopen(this.FicIn, 'r');
if fid == -1
    if ~isdeployed
        msg = sprintf('Fichier %s non trouvé.\n(message si dév uniquement)', this.FicIn);
        my_warndlg(msg,  0, 'Tag', 'FichierUserDefautPasTrouvé');
    end
    return
end

flagModif = 0;
L0 = [];
while feof(fid) == 0
    str = fgetl(fid);
    L0{end+1} = str; %#ok<AGROW>
    
    [str, flagModifLine] = processLine(str);
    
    L0{end} = str;
    if flagModifLine
        flagModif = 1;
    end
end
fclose(fid);

if flagModif
    fid = fopen(this.FicIn, 'w+');
    if fid == -1
        msg = sprintf('File %s is not writable', this.FicIn);
        my_warndlg(msg,  1);
        return
    end
    for i=1:length(L0)
        fprintf(fid, '%s\n', L0{i});
    end
    fclose(fid);
end

function [strOut, flagModifLine] = processLine(strIn)

strOut = strrep(strIn, 'TypeImage', 'ImageType');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'TypeDonnee', 'DataType');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'unitex', 'XUnit');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'unitey', 'YUnit');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'TypeCoordonnees', 'GeometryType');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'nuColormap', 'ColormapIndex');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'SensVert', 'YDir');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'SensHorz', 'XDir');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'TagSynchroV', 'TagSynchroContrast');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'HistoX', 'HistoCentralClasses');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'HistoN', 'HistoValues');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'StatsVal', 'StatValues');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'Nature', 'SpectralStatus');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'labelx', 'XLabel');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'labely', 'YLabel');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'ColormapPerso', 'ColormapCustom');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end


strOut = strrep(strIn, 'exaVertRef', 'VertExagAuto');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'exaVert', 'VertExag');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'azimut', 'Azimuth');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'Contours', 'ContourValues');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'OrigineFileName', 'InitialFileFormat');
if ~isequal(strOut, strIn)
    flagModifLine = 1;
    return
end

strOut = strrep(strIn, 'CoulIndex', '');
if isequal(strOut, strIn)
    flagModifLine = 0;
else
    flagModifLine = 1;
    strOut = '';
    return
end



% strOut = strrep(strIn, '', 'InitialImageName');
% if isequal(strOut, strIn)
%     flagModifLine = 0;
% else
%     flagModifLine = 1;
%     return
% end
% 
% strOut = strrep(strIn, '', 'Comments');
% if isequal(strOut, strIn)
%     flagModifLine = 0;
% else
%     flagModifLine = 1;
%     return
% end
% 
%     if ~isfield(UserInfo, 'NuIdentParent')
%         UserInfo.NuIdentParent   = floor(rand(1) * 1e8);
%     end

