% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_ermapper_ers
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   a = cl_ermapper_ers('FicIn', '/home2/brekh/dataErMapper/DATASET/');
%   str = char(a)
%
% See also cl_ermapper_ers Authors
% Authors : JMA
% VERSION  : $Id: char.m,v 1.1 2003/09/08 15:35:02 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    10/07/2003 - JMA - creation
% ----------------------------------------------------------------------------

function str = char(this)

% Maintenance Auto : try
    
    str = [];
    [m, n] = size(this);
    
    if (m*n) > 1
        
        % -------------------
        % Tableau d'instances
        
        for i = 1:1:m
            for j = 1:1:n
                str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
                str1 = char(this(i, j));
                str{end+1} = str1; %#ok<AGROW>
            end
        end
        str = cell2str(str);

    else
        % ---------------
        % Instance unique
        
        str = char_instance(this);
        str = [repmat(' ', size(str,1), 2) str];
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_ermapper_ers', 'char', lasterr);
% Maintenance Auto : end
