% Constructeur de cl_ermapper_ers (header des fichiers ER-Mapper)
%
% Syntax
%   a = cl_ermapper_ers(...)
% 
% Name-Value Pair Arguments
%   FicIn : Nom du fichier de description d'image ER-Mapper
%  
% Output Arguments 
%   a : instance de cl_ermapper_ers
%
% Examples
%   nomFic = '/home2/brekh/dataErMapper/DATASET/SPOT_XS_Minimum_Distance.ers';
%   nomFic = 'Z:\dataErMapper\DATASET\SPOT_XS_Minimum_Distance.ers';
%   a = cl_ermapper_ers('FicIn', nomFic)
%
% See also cl_ermapper_ers/editobj cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = cl_ermapper_ers(varargin)

%% Definition de la structure

this.FicIn              = '';
this.Version            = '';
this.Name               = '';
this.Description        = '';
this.SourceDataset      = '';
this.LastUpdated        = '';
this.SensorName         = '';
this.SenseDate          = '';
this.DataFile           = '';
this.DataSetType        = '';
this.DataType           = '';
this.ByteOrder          = '';
this.HeaderOffset       = 0;
this.Comments           = '';

this.CoordinateSpace.Datum          = '';
this.CoordinateSpace.Projection     = '';
this.CoordinateSpace.CoordinateType = '';
this.CoordinateSpace.Units          = '';
this.CoordinateSpace.Rotation       = '';

this.Ellipsoide             = [];

% Liste des projections. ATTENTION : Meme definition dans cl_carto. Ne
% pas oublier de modifier cl_carto si changement dans cl_ermapper_ers
this.Projection.strType     = {'Geodetic'; 'Mercator'; 'UTM'; 'Lambert'; 'lamcon'; 'Stereographique polaire'; 'Other'};
this.Projection.Type        = 1;

this.RasterInfo.CellType            = '';
this.RasterInfo.NrOfLines           = 0;
this.RasterInfo.NrOfCellsPerLine    = 0;
this.RasterInfo.NrOfBands           = 0;
this.RasterInfo.NullCellValue       = NaN;
this.RasterInfo.RegistrationCellX   = 0;
this.RasterInfo.RegistrationCellY   = 0;

this.RasterInfo.CellInfo.Xdimension = 0;
this.RasterInfo.CellInfo.Ydimension = 0;

this.RasterInfo.RegistrationCoord.Eastings  = 0;
this.RasterInfo.RegistrationCoord.Northings = 0;
this.RasterInfo.RegistrationCoord.MetersX   = 0;
this.RasterInfo.RegistrationCoord.MetersY   = 0;
this.RasterInfo.RegistrationCoord.MetersZ   = 0;

this.RasterInfo.BandId = [];
%     this.RasterInfo.BandId.Value = [];

this.RasterInfo.RegionInfo         = [];
%     this.RasterInfo.RegionInfo.Type         = '';
%     this.RasterInfo.RegionInfo.RegionName   = '';
%     this.RasterInfo.RegionInfo.RGBcolour.Red = [];
%     this.RasterInfo.RegionInfo.RGBcolour.Green = [];
%     this.RasterInfo.RegionInfo.RGBcolour.Blue = [];
%     this.RasterInfo.RegionInfo.ClassNumber = [];
%     this.RasterInfo.RegionInfo.SubRegion   = [];
%     this.RasterInfo.RegionInfo.Stats.SubsampleRate   = [];
%     this.RasterInfo.RegionInfo.Stats.NumberOfBands   = [];
%     this.RasterInfo.RegionInfo.Stats.NumberOfNonNullCells   = [];
%     this.RasterInfo.RegionInfo.Stats.MinimumValue   = [];
%     this.RasterInfo.RegionInfo.Stats.MaximumValue   = [];
%     this.RasterInfo.RegionInfo.Stats.MeanValue   = [];
%     this.RasterInfo.RegionInfo.Stats.MedianValue   = [];
%     this.RasterInfo.RegionInfo.Stats.CovarianceMatrix   = [];

this.UserInfo.ImageType                       = [];
this.UserInfo.DataType                        = [];
this.UserInfo.GeometryType                    = [];
this.UserInfo.XUnit                           = [];
this.UserInfo.YUnit                           = [];
this.UserInfo.XLabel                          = [];
this.UserInfo.YLabel                          = [];
this.UserInfo.SpectralStatus                  = [];
this.UserInfo.YDir                            = [];
this.UserInfo.XDir                            = [];
this.UserInfo.ColormapIndex                   = [];
this.UserInfo.Colormap                        = [];
this.UserInfo.ColormapCustom                  = [];
this.UserInfo.CLim                            = [];
this.UserInfo.Video                           = [];
this.UserInfo. VertExagAuto                   = [];
this.UserInfo.VertExag                        = [];
this.UserInfo.Azimuth                         = [];
this.UserInfo.ContourValues                   = [];
this.UserInfo.TagSynchroX                     = [];
this.UserInfo.TagSynchroY                     = [];
this.UserInfo.TagSynchroContrast              = [];
this.UserInfo.InitialFileName                 = [];
this.UserInfo.HistoCentralClasses             = [];
this.UserInfo.HistoValues                     = [];
this.UserInfo.NuIdentParent                   = [];

this.UserInfo.StatValues.Moyenne                = [];
this.UserInfo.StatValues.Variance               = [];
this.UserInfo.StatValues.Sigma                  = [];
this.UserInfo.StatValues.Mediane                = [];
this.UserInfo.StatValues.Min                    = [];
this.UserInfo.StatValues.Max                    = [];
this.UserInfo.StatValues.Range                  = [];
this.UserInfo.StatValues.Quant_03_97            = [];
this.UserInfo.StatValues.Quant_01_99            = [];
this.UserInfo.StatValues.Quant_25_75            = [];
this.UserInfo.StatValues.Sampling               = [];
this.UserInfo.StatValues.PercentNaN             = [];
this.UserInfo.StatValues.Skewness               = [];
this.UserInfo.StatValues.Kurtosis               = [];
this.UserInfo.StatValues.Entropy                = [];
this.UserInfo.StatSummary                     = [];
this.UserInfo.Sonar.Ident                     = [];
this.UserInfo.Sonar.Family                    = [];
this.UserInfo.Sonar.RawDataResol              = [];
this.UserInfo.Sonar.ResolutionD               = [];
this.UserInfo.Sonar.ResolutionX               = [];
this.UserInfo.Sonar.TVG.etat                  = [];
this.UserInfo.Sonar.TVG.origine               = [];
this.UserInfo.Sonar.TVG.ConstructTypeCompens  = [];
this.UserInfo.Sonar.TVG.ConstructAlpha        = [];
this.UserInfo.Sonar.TVG.ConstructTable        = [];
this.UserInfo.Sonar.TVG.IfremerAlpha          = [];

this.UserInfo.Sonar.NE.etat                   = [];
this.UserInfo.Sonar.GT.etat                   = [];
this.UserInfo.Sonar.SH.etat                   = [];
this.UserInfo.Sonar.TVG.ConstructCoefDiverg   = [];
this.UserInfo.Sonar.TVG.ConstructConstante    = [];
this.UserInfo.Sonar.TVG.IfremerCoefDiverg     = [];
this.UserInfo.Sonar.TVG.IfremerConstante      = [];
this.UserInfo.Sonar.DiagEmi.etat                  = [];
this.UserInfo.Sonar.DiagEmi.origine               = [];
this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens  = [];
this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens    = [];
this.UserInfo.Sonar.DiagRec.etat              = [];
this.UserInfo.Sonar.DiagRec.origine           = [];
this.UserInfo.Sonar.DiagRec.ConstructTypeCompens = [];
this.UserInfo.Sonar.DiagRec.IfremerTypeCompens   = [];
this.UserInfo.Sonar.AireInso.etat              = [];
this.UserInfo.Sonar.AireInso.origine           = [];
this.UserInfo.Sonar.BS.etat                    = [];
this.UserInfo.Sonar.BS.origine                 = [];
this.UserInfo.Sonar.BS.origineBelleImage       = [];
this.UserInfo.Sonar.BS.origineFullCompens      = [];
this.UserInfo.Sonar.BS.IfremerCompensTable     = [];

%% Creation de l'instance

this = class(this, 'cl_ermapper_ers');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
