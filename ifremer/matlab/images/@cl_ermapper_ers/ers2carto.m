% Recuperation des parametres cartographiques contenus dans une image ErMapper
%
% Syntax
%   Carto = ers2carto(a)
%
% Input Arguments
%   a : Instance de cl_ermapper_ers
%
% Output Arguments
%   Carto : Instance de cl_carto
%
% Remarks : Remarques particulieres concernant la fonction
%
% Examples
%   listeNomFic = findFicInDir('/home2/brekh/dataErMapper/DATASET', '*ers')
%   edit(listeNomFic{1})
%   a = cl_ermapper_ers('FicIn', listeNomFic{1})
%   Carto = ers2carto(a)
%   lat = 0; lon = 0;
%   [x, y] = latlon2xy(Carto, lat, lon)
%
% See also cl_carto cl_ermapper Authors
% Authors : JMA
%--------------------------------------------------------------------------
    
function Carto = ers2carto(this)

Carto = cl_carto;

this.Ellipsoide = decode_ellipsoide(this);
if isempty(this.Ellipsoide) || isempty(this.Projection)
    disp('Insufficient cartographic parameters')
    Carto = [];
    return
end

numEllipsoide = ident_ellipsoide(Carto, this.Ellipsoide.radius, this.Ellipsoide.eccentricity);
Carto = set(Carto,'Ellipsoide.Type', numEllipsoide);

% Liste des projections. ATTENTION : Meme definition dans cl_ermapper_ers. Ne
% pas oublier de modifier cl_ermapper_ers si changement dans cl_carto
if strcmp(this.CoordinateSpace.Projection, 'GEODETIC')
    Carto = set(Carto,'Projection.Type', 1);
    return
end
this.Projection = decodeProjectionErmapper(Carto, this.CoordinateSpace.Projection);
Carto = set(Carto,'Projection.Type', this.Projection.Type);


% Récupration des paramètres importants

switch this.Projection.Type
    case 1
        Carto = set(Carto,'Projection.Type', 1);
        
    case 2
        Carto = set(Carto,'Projection.Mercator.long_merid_orig', this.Projection.Parametres.Mercator.centre_merid);
        Carto = set(Carto,'Projection.Mercator.lat_ech_cons',    this.Projection.Parametres.Mercator.lat_ech_cons);
        Carto = set(Carto,'Projection.Mercator.Y0',              this.Projection.Parametres.Mercator.false_north);
        Carto = set(Carto,'Projection.Mercator.X0',              this.Projection.Parametres.Mercator.false_east);

    case 3
        Carto = set(Carto,'Projection.UTM.Fuseau',     this.Projection.Parametres.UTM.Fuseau);
        Carto = set(Carto,'Projection.UTM.Hemisphere', this.Projection.Parametres.UTM.Hemisphere);
        if strcmp(this.Projection.Parametres.UTM.Hemisphere, 'N')
            Carto = set(Carto,'Projection.UTM.Y0', 0);
            Carto = set(Carto,'Projection.UTM.X0', 500000);
        elseif strcmp(this.Projection.Parametres.UTM.Hemisphere, 'S')
%             Carto = set(Carto,'Projection.UTM.Y0',   500000);
%             Carto = set(Carto,'Projection.UTM.X0', 10000000);
          Carto = set(Carto,'Projection.UTM.Y0', 10000000); % Modif JMA le 13/03/2019
          Carto = set(Carto,'Projection.UTM.X0',   500000);
        else
            Carto = set(Carto,'Projection.UTM.Y0', 0);
            Carto = set(Carto,'Projection.UTM.X0', 0);
        end
        
    case 4
        Carto = set(Carto,'Projection.Lambert.Type',            this.Projection.Parametres.Lambert.Type);
        Carto = set(Carto,'Projection.Lambert.first_paral',     this.Projection.Parametres.Lambert.centre_lat);
        Carto = set(Carto,'Projection.Lambert.second_paral',    this.Projection.Parametres.Lambert.centre_lat);
        Carto = set(Carto,'Projection.Lambert.long_merid_orig', this.Projection.Parametres.Lambert.centre_merid);
        Carto = set(Carto,'Projection.Lambert.Y0',              this.Projection.Parametres.Lambert.false_north);
        Carto = set(Carto,'Projection.Lambert.X0',              this.Projection.Parametres.Lambert.false_east) ;
        Carto = set(Carto,'Projection.Lambert.scale_factor',    this.Projection.Parametres.Lambert.scale_factor);
        
    case 5
        Carto = set(Carto,'Projection.Stereo.long_merid_horiz', this.Projection.Parametres.Stereo.centre_merid);
        Carto = set(Carto,'Projection.Stereo.lat_ech_cons',     this.Projection.Parametres.Stereo.origin_lat);

        if this.Projection.Parametres.Stereo.origin_lat < 0
            Carto = set(Carto, 'Projection.Stereo.hemisphere', 'S');
        else
            Carto = set(Carto, 'Projection.Stereo.hemisphere', 'N');
        end
        
    otherwise
        my_warndlg('Projection pas encore faite dans cl_ermapper_ers/ers2carto', 1);
end
