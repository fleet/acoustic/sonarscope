function flag = exist_FileUserInfo(this)

[nomDir, nomFic] = fileparts(this.FicIn);
FicIn = fullfile(nomDir, nomFic, [nomFic '_UserInfo.txt']);

flag = exist(FicIn, 'file');

