%  nomFicErs = 'C:\TEMP\LaReunionAzimuth200.alg'
%  flag = export_xml(cl_ermapper_ers([]), nomFicErs)
%  type('C:\TEMP\LaReunionAzimuth200.xml')

function flag = export_UserData_xml(this, nomFicErs, varargin) %#ok<INUSL>

[varargin, nomFicXml] = getPropertyValue(varargin, 'nomFicXml', []); %#ok<ASGLU>

flag = 0;

if isempty(nomFicXml)
    [nomDir, nomFicXml] = fileparts(nomFicErs);
    nomFicXml = fullfile(nomDir, [nomFicXml '.xml']);
end

fidErs = fopen(nomFicErs, 'r');
if fidErs == -1
    messageErreurFichier(nomFicErs, 'ReadFailure');
    return
end

fidXml = fopen(nomFicXml, 'w+');
if fidXml == -1
    messageErreurFichier(nomFicXml, 'ReadFailure');
    return
end
fprintf(fidXml, '%s\n', '<?xml version="1.0" encoding="UTF-8"?>');

while 1
    tline = fgetl(fidErs);
    if ~ischar(tline)
        break
    end
    if ~((length(tline) >= 2) && strcmp(tline(1:2), '##'))
        continue
    end
    
    tline = tline(3:end);
    mots = strsplit(tline);
    
    % Test debut de paragraphe
    if (length(mots) == 2) && strcmp(mots{2}, 'Begin')
        fprintf(fidXml, '<%s>\n', mots{1});
        
        % Test fin de paragraphe
    elseif (length(mots) == 2) && strcmp(mots{2}, 'End')
        fprintf(fidXml, '</%s>\n', mots{1});
        
        % Test debut d'un tableau
    elseif (length(mots) == 3) && strcmp(mots{2}, '=') && strcmp(mots{3}, '{')
        NomTableau = mots{1};
        fprintf(fidXml, '<%s>[', NomTableau);
        flagDebutTableau = 1;
        
        % Test Fin d'un tableau
    elseif (length(mots) == 1) && strcmp(mots{1}, '}')
        fprintf(fidXml, ']</%s>\n', NomTableau);
        
        % Test d�finition d'un param�tre
    elseif (length(mots) > 2) && strcmp(mots{2}, '=')
        Value = mots{3};
        for i=4:length(mots)
            Value = [Value ' ' mots{i}]; %#ok<AGROW>
        end
        fprintf(fidXml, '<%s>%s</%s>\n', mots{1}, Value, mots{1});
        
    else
        if flagDebutTableau
            fprintf(fidXml, '%s', tline);
        else
            fprintf(fidXml, '\n%s', tline);
        end
        flagDebutTableau = 0;
    end
end

fclose(fidErs);
fclose(fidXml);
flag = 1;
