%   nomFic = getNomFicDatabase('Bretagne.ers');
%   nomFic = 'C:\TEMP\LaReunionAzimuthJMA2.ers';

%   [flag, b] = cl_image.import_ermapper(nomFic);
%   CLim = b.CLim
%   HistoCentralClasses = get(b, 'HistoCentralClasses')
%   HistoValues = get(b, 'HistoValues')
%   a = cl_ermapper_ers('FicIn', nomFic)
%
%  nomFicAlg = 'C:\TEMP\aaaa.alg'
%  flag = export_alg(a, nomFicAlg, CLim, HistoCentralClasses, HistoValues, 'SunAzimuth', 220, 'SunAngle', 80)
%  type(nomFicAlg)

function flag = export_alg(this, nomFicAlg, nomFic, CLim, HistoCentralClasses, HistoValues, varargin)

flag = 0;

[varargin, SunAzimuth]  = getPropertyValue(varargin, 'SunAzimuth', 0);
[varargin, SunAngle]    = getPropertyValue(varargin, 'SunAngle', 80);
[varargin, nomLayer]    = getPropertyValue(varargin, 'nomLayer', 'Layer');
[varargin, uniteLayer]  = getPropertyValue(varargin, 'uniteLayer', 'metres');
[varargin, LookupTable] = getPropertyValue(varargin, 'LookupTable', 'pseudocolor');
[varargin, Video]       = getPropertyValue(varargin, 'Video', 1);

[varargin, Shading] = getFlag(varargin, 'Shading'); %#ok<ASGLU>

nomDir1 = fileparts(nomFicAlg);
nomDir2 = fileparts(nomFic);

MinimumInputValue = CLim(1);
MaximumInputValue = CLim(2);
% if length(MinimumInputValue) < 2
if length(HistoCentralClasses) < 2
    return
else
    HistogramMinimumValue = HistoCentralClasses(1);
    HistogramMaximumValue = HistoCentralClasses(end);
end

% HistoValues
Y = cumsum(HistoValues(:));
x = linspace(HistogramMinimumValue, HistogramMaximumValue, 256+1);
if all(HistoCentralClasses == 0)
    y = zeros(size(x));
else
    try
        y = interp1(HistoCentralClasses, Y, x);
    catch %#ok<CTCH>
        y = zeros(size(x));
    end
end
HistoValues = diff(y);

[~, nomFicErs] = fileparts(this.FicIn);

nomFicXml = my_tempname('.xml');
flag = export_xml(cl_ermapper_ers([]), this.FicIn, 'nomFicXml', nomFicXml);
if ~flag
    return
end

% nomFicUserDataXml = my_tempname('.xml');
% flag = export_UserData_xml(cl_ermapper_ers([]), this.FicIn, 'nomFicXml', nomFicUserDataXml);
% if ~flag
%     return
% end

% ATTENTION : ne pas utiliser xml_mat_read ici sinon les fichiers renommés
% en xxx_xml.mat sont conservés
try
    tree = xml_read(nomFicXml);
catch
    messageErreurFichier(nomFicXml, 'ReadFailure');
    flag = 0;
    return
end
delete(nomFicXml)

if isfield(tree.RasterInfo, 'RegistrationCoord')
    if isfield(tree.RasterInfo.RegistrationCoord, 'Eastings')
        TopLeftCornerEastings  = tree.RasterInfo.RegistrationCoord.Eastings;
    elseif isfield(tree.RasterInfo.RegistrationCoord, 'Longitude')
        val = tree.RasterInfo.RegistrationCoord.Longitude;
        mots = strsplit(val, ':');
        val = str2double(mots);
        X = abs(val(1)) + val(2)/60 + val(3)/3600;
        val = X * sign(val(1));
        TopLeftCornerEastings  = val;
    else
        TopLeftCornerEastings = 1;
    end
    if isfield(tree.RasterInfo.RegistrationCoord, 'Northings')
        TopLeftCornerNorthings = tree.RasterInfo.RegistrationCoord.Northings;
    elseif isfield(tree.RasterInfo.RegistrationCoord, 'Latitude')
        val = tree.RasterInfo.RegistrationCoord.Latitude;
        mots = strsplit(val, ':');
        val = str2double(mots);
        X = abs(val(1)) + val(2)/60 + val(3)/3600;
        val = X * sign(val(1));
        TopLeftCornerNorthings = val;
    else
        TopLeftCornerNorthings = tree.RasterInfo.NrOfLines;
    end
    CoordinateType = 'EN';
else
    TopLeftCornerEastings = 1;
    TopLeftCornerNorthings = tree.RasterInfo.NrOfLines;
    CoordinateType = 'RAW';
end

BottomRightCornerEastings = TopLeftCornerEastings + ...
    tree.RasterInfo.CellInfo.Xdimension * (tree.RasterInfo.NrOfCellsPerLine - 1);
% 56.316351246360234;
% Xdimension = 4.819697301302650e-004 au lieu de 0.00048196973013026501

BottomRightCornerNorthings = TopLeftCornerNorthings - ...
    tree.RasterInfo.CellInfo.Ydimension * (tree.RasterInfo.NrOfLines - 1);
% -22.091237669182789 au lieu de -22.091248051621285;


Alg.Version     = '"7.0"';
Alg.Name        = tree.Name;
% Alg.Description = tree.Description;
Alg.Author      = '"SonarScope"';
Alg.LastUpdated = tree.LastUpdated;

% Alg.SensorName = tree.SensorName;
% Alg.DataFile = tree.DataFile;
% Alg.DataSetType = tree.DataSetType;
% Alg.DataType = tree.DataType;
% Alg.ByteOrder = tree.ByteOrder;
% Alg.HeaderOffset = tree.HeaderOffset;
% Alg.Comments = tree.Comments;

Alg.ViewMode            = 'Planimetric2D';
Alg.BackGroundColourSet = 'No';
Alg.BackGroundColour    = '0,0,0';
Alg.ConvertToGreyscale  = 'No';

Alg.CoordinateSpace = tree.CoordinateSpace;
Alg.CoordinateSpace.CoordinateType = CoordinateType;

if strcmp(CoordinateType, 'EN')
    Alg.TopLeftCorner.Eastings      = TopLeftCornerEastings;
    Alg.TopLeftCorner.Northings     = TopLeftCornerNorthings;
    Alg.BottomRightCorner.Eastings  = BottomRightCornerEastings;
    Alg.BottomRightCorner.Northings = BottomRightCornerNorthings;
else
    Alg.TopLeftCorner.MetersX     = 1; % TopLeftCornerEastings;
    Alg.TopLeftCorner.MetersY     = 1; % TopLeftCornerNorthings;
    Alg.BottomRightCorner.MetersX = BottomRightCornerEastings;
    Alg.BottomRightCorner.MetersY = TopLeftCornerNorthings; %BottomRightCornerNorthings;
end

Alg.SuperSampling = 'Bilinear';
Alg.MosaicType = 'Overlay';
Alg.PageSize.PageConstraint = 'None';
Alg.PageSize.PageWidth = 8.5; % ????????????
Alg.PageSize.PageHeight = 11; % ????????????
Alg.PageSize.TopBorder = 0;
Alg.PageSize.BottomBorder = 0;
Alg.PageSize.LeftBorder = 0;
Alg.PageSize.RightBorder = 0;
Alg.PageSize.Scale = 1; % 1012835.8769105059; ???????????

if strcmp(CoordinateType, 'EN')
    Alg.PageSize.PageExtents.TopLeftCorner.Eastings         = TopLeftCornerEastings;
    Alg.PageSize.PageExtents.TopLeftCorner.Northings        = TopLeftCornerNorthings;
    Alg.PageSize.PageExtents.BottomRightCorner.Eastings     = BottomRightCornerEastings;
    Alg.PageSize.PageExtents.BottomRightCorner.Northings    = BottomRightCornerNorthings;
    Alg.PageSize.ContentExtents.TopLeftCorner.Eastings      = TopLeftCornerEastings;
    Alg.PageSize.ContentExtents.TopLeftCorner.Northings     = TopLeftCornerNorthings;
    Alg.PageSize.ContentExtents.BottomRightCorner.Eastings  = BottomRightCornerEastings;
    Alg.PageSize.ContentExtents.BottomRightCorner.Northings = BottomRightCornerNorthings;
else
    Alg.PageSize.PageExtents.TopLeftCorner.MetersX       = TopLeftCornerEastings;
    Alg.PageSize.PageExtents.TopLeftCorner.MetersY       = TopLeftCornerNorthings;
    Alg.PageSize.PageExtents.BottomRightCorner.MetersX   = BottomRightCornerEastings;
    Alg.PageSize.PageExtents.BottomRightCorner.MetersY   = BottomRightCornerNorthings;
    Alg.PageSize.ContentExtents.TopLeftCorner.MetersX    = TopLeftCornerEastings;
    Alg.PageSize.ContentExtents.TopLeftCorner.MetersY    = TopLeftCornerNorthings;
    Alg.PageSize.ContentExtents.BottomRightCorner.MetersX = BottomRightCornerEastings;
    Alg.PageSize.ContentExtents.BottomRightCorner.MetersY = BottomRightCornerNorthings;
end
Alg.Surface.Name = '"Pseudocolor"';
Alg.Surface.Transparency = 0;
Alg.Surface.ColorMode = 'PSEUDO';
Alg.Surface.LookupTable = ['"' LookupTable '"'];
Alg.Surface.Stream(1).Type = 'Intensity';
Alg.Surface.Stream(1).Description = '"Intensity Layer"';

if strcmp(nomDir1, nomDir2)
    Alg.Surface.Stream(1).Dataset = ['"' nomFicErs '.ers"'];
else
    Alg.Surface.Stream(1).Dataset = ['"../' nomFicErs '.ers"'];
end
Alg.Surface.Stream(1).DoStaticShade = 'Yes';
Alg.Surface.Stream(1).SunAzimuth = [num2str(floor(SunAzimuth)) ':0:0.0'];
Alg.Surface.Stream(1).SunAngle = [num2str(floor(SunAngle)) ':0:0.0'];
Alg.Surface.Stream(1).StreamInput.StreamInputID = 1;
Alg.Surface.Stream(1).Formula.Description = '"Default Formula"';
Alg.Surface.Stream(1).Formula.Formula = '"INPUT1"';
Alg.Surface.Stream(1).Formula.FormulaArg.StreamInput = 1;
Alg.Surface.Stream(1).Formula.FormulaArg.BandNumber = 1;
Alg.Surface.Stream(1).Formula.FormulaArg.BandId.Value = ['"' nomLayer '"'];
Alg.Surface.Stream(1).Formula.FormulaArg.BandId.Units = ['"' uniteLayer '"'];
Alg.Surface.Stream(1).Formula.FormulaArg.BandId.Comment = ['"' nomLayer '"'];

Alg.Surface.Stream(1).Transform.Type = 'Linear';
Alg.Surface.Stream(1).Transform.MinimumInputValue = MinimumInputValue;
Alg.Surface.Stream(1).Transform.MaximumInputValue = MaximumInputValue;
Alg.Surface.Stream(1).Transform.MinimumOutputValue = 0;
Alg.Surface.Stream(1).Transform.MaximumOutputValue = 255;
Alg.Surface.Stream(1).Transform.LinearDef.NumberOfPoints = 2;

% if this.UserInfo.Video == 1
if Video == 1
    Alg.Surface.Stream(1).Transform.LinearDef.Points = [0 0; 1 1];
else
    Alg.Surface.Stream(1).Transform.LinearDef.Points = [0 1; 1 0];
end

Alg.Surface.Stream(1).Transform.Histogram.MinimumValue = HistogramMinimumValue;
Alg.Surface.Stream(1).Transform.Histogram.MaximumValue = HistogramMaximumValue;
Alg.Surface.Stream(1).Transform.Histogram.CellsBelowMin = 0;%161; % ???????
Alg.Surface.Stream(1).Transform.Histogram.CellsAboveMax = 0;%111; % ???????

Alg.Surface.Stream(1).Transform.Histogram.NrOfCells = floor(sum(HistoValues)); % 399573;
Alg.Surface.Stream(1).Transform.Histogram.Bins = floor(HistoValues);
Alg.Surface.Stream(1).Transform.DoInRealtime = 'No';

Alg.Surface.Stream(2) = Alg.Surface.Stream(1);
Alg.Surface.Stream(2).Type = 'Pseudo';
Alg.Surface.Stream(2).Description = '"Pseudo Layer"';
Alg.Surface.Stream(2).DoStaticShade = 'No';
Alg.Surface.Stream(2).SunAzimuth = '45:0:0.0';
Alg.Surface.Stream(2).SunAngle = '45:0:0.0';

if ~Shading
    Alg.Surface.Stream = Alg.Surface.Stream(2);
end

fidAlg = fopen(nomFicAlg, 'w+');
if fidAlg == -1
    messageErreurFichier(nomFicAlg, 'WriteFailure')
    return
end
fprintf(fidAlg, 'Algorithm Begin\n');

export_section_fichierErs(fidAlg, Alg, 1)

fprintf(fidAlg, 'Algorithm End\n');
fclose(fidAlg);
flag = 1;

