% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments
%   this : instance de cl_ermapper_ers
%
% Name-only Arguments
%    cf. cl_ermapper_ers
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%   a = cl_ermapper_ers('Image', Lena, 'Name', 'Lena')
%   get(a, 'identMap')
%   get(a, 'identVisu')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

%% Boucle de parcours de l'ensemble des proprietes donn�es en arguments

for i=1:length(varargin)
    switch varargin{i}
        case 'FicIn'
            varargout{end+1} = this.FicIn;  %#ok<AGROW>
        case 'Version'
            varargout{end+1} = this.Version;  %#ok<AGROW>
        case 'Name'
            varargout{end+1} = this.Name;  %#ok<AGROW>
        case 'Description'
            varargout{end+1} = this.Description; %#ok<AGROW>
        case 'SourceDataset'
            varargout{end+1} = this.SourceDataset; %#ok<AGROW>
        case 'LastUpdated'
            varargout{end+1} = this.LastUpdated; %#ok<AGROW>
        case 'SensorName'
            varargout{end+1} = this.SensorName; %#ok<AGROW>
        case 'SenseDate'
            varargout{end+1} = this.SenseDate; %#ok<AGROW>
        case 'DataFile'
            varargout{end+1} = this.DataFile; %#ok<AGROW>
        case 'DataSetType'
            varargout{end+1} = this.DataSetType; %#ok<AGROW>
        case 'DataType'
            varargout{end+1} = this.DataType; %#ok<AGROW>
        case 'ByteOrder'
            varargout{end+1} = this.ByteOrder; %#ok<AGROW>
        case 'HeaderOffset'
            varargout{end+1} = this.HeaderOffset; %#ok<AGROW>
        case 'Comments'
            varargout{end+1} = this.Comments; %#ok<AGROW>

        case 'CoordinateSpace'
            varargout{end+1} = this.CoordinateSpace; %#ok<AGROW>
        case 'Datum'
            varargout{end+1} = this.CoordinateSpace.Datum; %#ok<AGROW>
        case 'Projection'
            varargout{end+1} = this.CoordinateSpace.Projection; %#ok<AGROW>
        case 'CoordinateType'
            varargout{end+1} = this.CoordinateSpace.CoordinateType; %#ok<AGROW>
        case 'Units'
            varargout{end+1} = this.CoordinateSpace.Units; %#ok<AGROW>
        case 'Rotation'
            varargout{end+1} = this.CoordinateSpace.Rotation; %#ok<AGROW>

        case 'RasterInfo'
            varargout{end+1} = this.RasterInfo; %#ok<AGROW>
        case 'CellType'
            varargout{end+1} = this.RasterInfo.CellType; %#ok<AGROW>
        case 'NullCellValue'
            varargout{end+1} = this.RasterInfo.NullCellValue; %#ok<AGROW>
        case 'NrOfLines'
            varargout{end+1} = this.RasterInfo.NrOfLines; %#ok<AGROW>
        case 'NrOfCellsPerLine'
            varargout{end+1} = this.RasterInfo.NrOfCellsPerLine; %#ok<AGROW>
        case 'NrOfBands'
            varargout{end+1} = this.RasterInfo.NrOfBands; %#ok<AGROW>
        case 'RegistrationCellX'
            varargout{end+1} = this.RasterInfo.RegistrationCellX; %#ok<AGROW>
        case 'RegistrationCellY'
            varargout{end+1} = this.RasterInfo.RegistrationCellY; %#ok<AGROW>

        case 'CellInfo'
            varargout{end+1} = this.RasterInfo.CellInfo; %#ok<AGROW>
        case 'Xdimension'
            varargout{end+1} = this.RasterInfo.CellInfo.Xdimension; %#ok<AGROW>
        case 'Ydimension'
            varargout{end+1} = this.RasterInfo.CellInfo.Ydimension; %#ok<AGROW>

        case 'RegistrationCoord'
            varargout{end+1} = this.RasterInfo.RegistrationCoord; %#ok<AGROW>
        case 'Eastings'
            varargout{end+1} = this.RasterInfo.RegistrationCoord.Eastings; %#ok<AGROW>
        case 'Northings'
            varargout{end+1} = this.RasterInfo.RegistrationCoord.Northings; %#ok<AGROW>
        case 'MetersX'
            varargout{end+1} = this.RasterInfo.RegistrationCoord.MetersX; %#ok<AGROW>
        case 'MetersY'
            varargout{end+1} = this.RasterInfo.RegistrationCoord.MetersY; %#ok<AGROW>
        case 'MetersZ'
            varargout{end+1} = this.RasterInfo.RegistrationCoord.MetersZ; %#ok<AGROW>

        case 'BandId'
            varargout{end+1} = this.RasterInfo.BandId; %#ok<AGROW>

        case 'RegionInfo'
            varargout{end+1} = this.RasterInfo.RegionInfo; %#ok<AGROW>
            %         case 'RegionInfo.Type'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Type;
            %         case 'RegionInfo.RegionName'
            %             varargout{end+1} =
            %             this.RasterInfo.RegionInfo.RegionName;
            %
            %         case 'RegionInfo.RGBcolour.Red'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.RGBcolour.Red;
            %         case 'RegionInfo.RGBcolour.Green'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.RGBcolour.Green;
            %         case 'RegionInfo.RGBcolour.Blue'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.RGBcolour.Blue;
            %
            %         case 'RegionInfo.ClassNumber'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.ClassNumber;
            %
            %         case 'RegionInfo.SubRegion'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.SubRegion;
            %         case 'RegionInfo.Stats.NumberOfBands'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Stats.NumberOfBands;
            %         case 'RegionInfo.Stats.NumberOfNonNullCells'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Stats.NumberOfNonNullCells;
            %         case 'RegionInfo.Stats.MinimumValue'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Stats.MinimumValue;
            %         case 'RegionInfo.Stats.MaximumValue'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Stats.MaximumValue;
            %         case 'RegionInfo.Stats.MeanValue'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Stats.MeanValue;
            %         case 'RegionInfo.Stats.MedianValue'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Stats.MedianValue;
            %         case 'RegionInfo.Stats.CovarianceMatrix'
            %             varargout{end+1} = this.RasterInfo.RegionInfo.Stats.CovarianceMatrix;

        case 'UserInfo'
            this.UserInfo = MAJ(this.UserInfo);
            varargout{end+1} = this.UserInfo; %#ok<AGROW>

        otherwise
            str = sprintf('cl_ermapper_ers/get %s non pris en compte', varargin{i});
            my_warndlg(['cl_ermapper_ers:get' str], 0);
    end
end


function UserInfo = MAJ(UserInfo)
if ~isfield(UserInfo, 'XUnit')
    my_warndlg('Le format de cette image a chang�, resauvez la en "Source Format" SVP.', 0);
    UserInfo.XUnit               = UserInfo.unitex;
    UserInfo.YUnit               = UserInfo.unitey;
    UserInfo.GeometryType        = UserInfo.TypeCoordonnees;
    UserInfo.DataType            = UserInfo.TypeDonnee;
    UserInfo.ColormapIndex       = UserInfo.nuColormap;
    UserInfo.YDir                = UserInfo.SensVert;
    UserInfo.YDir                = UserInfo.SensHorz;
    UserInfo.TagSynchroContrast  = UserInfo.TagSynchroV;
    UserInfo.StatValues          = UserInfo.StatsVal;
    UserInfo.HistoCentralClasses = UserInfo.HistoX;
    UserInfo.HistoValues         = UserInfo.HistoN;
    if ~isfield(UserInfo, 'NuIdentParent')
        UserInfo.NuIdentParent   = floor(rand(1) * 1e8);
    end
end

%              labelx: 'X'
%              labely: 'Y'
%              Nature: 1
%            Colormap: [256x3 double]
%       ColormapPerso: [128x3 double]
%                CLim: [-370 -300]
%               Video: 1
%          exaVertRef: 0
%             exaVert: 1
%              azimut: 0
%            Contours: [1x10 double]
%         TagSynchroX: '0.68085'
%         TagSynchroY: '0.68085'
%     OrigineFileName: '/home1/doppler/IfremerToolboxDataPrivate/EM300_NIWA_IfrFlt.imo'
%         nbValNonNaN: []
%            nbValNaN: []
%       StatsSampling: []
%            StatsStr: []
%               Sonar: [1x1 struct]
%               unite: []
%               XUnit: 'm'
%               YUnit: 'm'
