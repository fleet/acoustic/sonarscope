% Identification de l'ellipsoide
%
% Syntax
%   nomFicAlg = get_nomFicAlg(a, nomFic)
% 
% Input Arguments 
%   a      : Une instance de la classe cl_ermapper_ers
%   nomFic : Nom du fichier
%
% Output Arguments
%   nomFicAlg : Nom de l'ellipsoide
% 
% Examples
%   a = cl_ermapper_ers;
%   nomEllipsoide = get_nomFicAlg(a, nomFic)
%
% See also cl_ermapper_ers cl_ermapper_ers/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function nomFicAlg = get_nomFicAlg(~, nomFic, varargin)

[varargin, flagShading] = getFlag(varargin, 'Shading'); %#ok<ASGLU>

[nomDir, nomFicAlg] = fileparts(nomFic);

nomDir2 = fullfile(nomDir, nomFicAlg);
if exist(nomDir2, 'file')
    nomDir = nomDir2;
end

if flagShading
    nomFicAlg = fullfile(nomDir, [nomFicAlg '_shading.alg']);
else
    nomFicAlg = fullfile(nomDir, [nomFicAlg '.alg']);
end

