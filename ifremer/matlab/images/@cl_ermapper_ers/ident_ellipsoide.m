% Identification de l'ellipsoide
%
% Syntax
%   nomEllipsoide = ident_ellipsoide(a, GrandAxe, Eccentricity)
% 
% Input Arguments 
%   a            : Une instance de la classe cl_ermapper_ers
%   GrandAxe     : Demi grans axe (m)
%   Eccentricity : Eccentricite
%
% Output Arguments
%   nomEllipsoide : Nom de l'ellipsoide
% 
% Examples
%   a = cl_ermapper_ers;
%   nomEllipsoide = ident_ellipsoide(a, 6378206.4, 0.08227185818)
%
% See also cl_ermapper_ers cl_ermapper_ers/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function nomEllipsoide = ident_ellipsoide(this, GrandAxe, Eccentricity) %#ok

strNomEllipsoide = {};
R = [];
E = [];

nomFic = getNomFicDatabase('datum_sp.dat');
Lines = getLines(nomFic);
for iLine = 1:length(Lines)
    mots = strsplit(Lines{iLine}, ',');
    Name = rmblank(mots{1});
    
    % Ellipsoide tr�s proche de wgs84 et qui est plus proche de wgs84 Caraibes
    if strcmp(Name, 'EGSA87') || strcmp(Name, 'HGRS87')
        continue
    end
    
    strNomEllipsoide{end+1} = Name; %#ok
    R(end+1) = str2num(mots{3}); %#ok
    E(end+1) = str2num(mots{4}); %#ok
end

sub = find((R >= 6377000) & (R <= 6379000));
strNomEllipsoide = strNomEllipsoide(sub);
R = R(sub);
E = E(sub);

minR = min(R);
minE = min(E);
DR = max(R) - minR;
DE = max(E) - minE;

R = (R - minR) / DR;
E = (E - minE) / DE;

r = (GrandAxe - minR) / DR;
e = (Eccentricity - minE) / DE;
%     figure; plot(R, E, '+k'); hold on; plot(r, e, '*r')

dist = sqrt((R-r).^2 + (E-e).^2);
[distMin, numEllipsoide] = min(dist);
if distMin < 0.005
    nomEllipsoide = strNomEllipsoide{numEllipsoide};
    nomEllipsoide = rmblank(nomEllipsoide);
else
    nomEllipsoide = 'Not identified';
    my_warndlg('cl_ermapper_ers/ident_ellipsoide : Ellipsoide non identifiee', 1);
end





% -------------------------------------------------------------------------
function Lines = getLines(nomFic)
fid = fopen(nomFic);
entete = fgets(fid); %#ok
Lines = [];
while feof(fid) == 0
    L = fgetl(fid);
    if length(L) >= 1
        Lines{end+1} = L; %#ok<AGROW>
    end
end
fclose(fid);
