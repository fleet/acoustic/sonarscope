function [Image, NullCellValue] = import_ermapper_image(a, nomFic)

Image = [];

NrOfBands     = get(a, 'NrOfBands');
NullCellValue = get(a, 'NullCellValue');
nbRows         = get(a, 'NrOfLines');
nbColumns     = get(a, 'NrOfCellsPerLine');
HeaderOffset  = get(a, 'HeaderOffset');
% Xdimension      = get(a, 'Xdimension');
% InitialFileFormat = 'ErMapper';


[nomDir, nomFicSeul, ext] = fileparts(nomFic);
if ~strcmp(ext, '.ers')
    nomFicSeul = [nomFicSeul, ext];
end

nomFicImage = fullfile(nomDir, nomFicSeul);
if exist(nomFicImage, 'dir')
    nomDir = fullfile(nomDir, nomFicSeul);
    nomFicImage = fullfile(nomDir, nomFicSeul);
    if ~exist(nomFicImage, 'file')
        [~, nomFicSeulErs, ext] = fileparts(a.DataFile);
        nomFicImageBin = fullfile(nomDir, [nomFicSeulErs ext]);
        if exist(nomFicImageBin, 'file')
            nomFicImage = nomFicImageBin;
        end
    end
end

nomFicImageGz = fullfile(nomDir, [nomFicSeul '.gz']);
if ~exist(nomFicImage, 'file')
    if exist([nomFicImage '.ecw'], 'file')
        str = sprintf('Le fichier\n%s\n est au format ecw. On ne sait pas le decoder en matlab', nomFicImage);
        my_warndlg(['cl_ermapper:lec_fic_image ' str], 0);
        return
    elseif exist(nomFicImageGz, 'file')
        disp(['Uncompress : ' nomFicImageGz ' Please wait'])
        gunzip(nomFicImageGz, nomDir)
        delete(nomFicImageGz)
    else
        str = sprintf('Le fichier\n%s\nn''existe pas', nomFicImage);
        my_warndlg(['cl_ermapper:lec_fic_image ' str], 0);
        return
    end
end

CellType = get(a, 'CellType');
if isempty(CellType)
    return
end
[CellType, nbBytes] = CellTypeERMapper2Matlab(CellType);
Format = ['*' CellType];
nbPix = nbColumns*nbRows*NrOfBands*nbBytes;

%% V�rification de la valeur de NaN si pas de UserData car pb rencontr� avec fichiers g�n�r�s par ErMapper

flagUserData = exist_FileUserInfo(a);
% if ~flagUserData
if ~flagUserData && ~strcmp(CellType, 'uint8')
    %{
    % % J'ai essay� de trouver pouquoi �a ne marche pas mais �chec total : voir StrangeBug sur Mes Documents\Matlab
        fid = fopen(a.FicIn);
        for k=1:nbRows
            X = fread(fid, [nbColumns*NrOfBands 1], Format);
            sub = find(X > 6e+034)
            X(sub)
            if ~isempty(sub)
                NullCellValue = X(sub(1));
                break
            end
        end
        fclose(fid);
    %}
    
    fid = fopen(nomFicImage);
    Header = fread(fid, [1 HeaderOffset], 'char'); %#ok<NASGU>
    I = fread(fid, [nbColumns*NrOfBands nbRows], Format);
    fclose(fid);
    ValeursHorsNorme = unique(I(I > 6e+034));
    if isempty(ValeursHorsNorme)
        NullCellValue = get(a, 'NullCellValue');
    else
        if length(ValeursHorsNorme) > 1
            ValeursHorsNorme
        end
        NullCellValue = ValeursHorsNorme(1);
    end
    NullCellValue = cast(NullCellValue, 'like', I);
    clear I ValeursHorsNorme
end

%% Lecture du fichier

if nbPix <= 1e6 %2%7
    fid = fopen(nomFicImage);
    Header = fread(fid, [1 HeaderOffset], 'char'); %#ok<NASGU>
    I = fread(fid, [nbColumns*NrOfBands nbRows], Format);

    if ~isnan(NullCellValue)
        if ~isa(I, 'double')
            I = single(I);
        end
        I(I == NullCellValue) = NaN;
        NullCellValue = NaN;
    end
    fclose(fid);
    Image = reshape(I', [nbRows nbColumns NrOfBands]);
    
else %if NrOfBands == 1
    switch NrOfBands
        case 1
            Image = cl_memmapfile('FileName', nomFicImage, 'FileCreation', 0, ...
                'Format', CellType, 'Size', [nbRows nbColumns], ...
                'ValNaN', NullCellValue, 'ErMapper', 1, 'Offset', HeaderOffset);
        otherwise
            Image = cl_memmapfile('FileName', nomFicImage, 'FileCreation', 0, ...
                'Format', CellType, 'Size', [nbRows nbColumns NrOfBands], ...
                'ValNaN', NullCellValue, 'ErMapper', 1, 'Offset', HeaderOffset);
    end

    if NrOfBands == 3
        NullCellValue = 0;
    else
        if flagUserData
            NullCellValue = NaN;
        end
    end
%     if ~isnan(NullCellValue)
%         Image(Image == NullCellValue) = NaN;
%     end
end


function [typeMatlab, nbBytes] = CellTypeERMapper2Matlab(typeERMapper)

switch typeERMapper
    case 'IEEE8ByteReal'
        typeMatlab = 'double';
        nbBytes = 8;
    case 'IEEE4ByteReal'
        typeMatlab = 'single';
        nbBytes = 4;
    case 'Unsigned8BitInteger'
        typeMatlab = 'uint8';
        nbBytes = 1;
    case 'Unsigned16BitInteger'
        typeMatlab = 'uint16';
        nbBytes = 2;
    otherwise
        typeMatlab = [];
        str = sprintf('Le type\n%s\nnon prevu dans lec_fic_image', typeERMapper);
        my_warndlg(str, 1);
end
