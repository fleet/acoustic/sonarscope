%  nomFicErs = 'C:\TEMP\LaReunionAzimuth200.alg'
%  flag = export_xml(cl_ermapper_ers([]), nomFicErs)
%  type('C:\TEMP\LaReunionAzimuth200.xml')
%
%  nomFicXml  = 'C:\TEMP\LaReunionAzimuth200.xml'
%  nomFicErs2 = 'C:\TEMP\pppp.alg'
%  flag = import_xml(cl_ermapper_ers([]), nomFicXml, nomFicErs2)
%  type(nomFicErs2)

function flag = import_xml(this, nomFicXml, nomFicErs) %#ok<INUSL>

flag = 0;

tree = xml_read(nomFicXml);

fidErs = fopen(nomFicErs, 'w+');
if fidErs == -1
    messageErreurFichier(nomFicErs, 'WriteFailure')
    return
end
fprintf(fidErs, 'Algorithm Begin\n');

export_section_fichierErs(fidErs, tree, 1)

fprintf(fidErs, 'Algorithm End\n');
fclose(fidErs);
flag = 1;
