% Creation automatique du nom d'une image .ers
%
% Syntax
%   [nomFic, flag] = initNomFicErs(a, NomGenerique, GeometryType, DataType, nomDir, ...)
%
% Input Arguments
%   a            : Une instance de la classe cl_ermapper_ers
%   NomGenerique : Nom generique de l'image
%   GeometryType : Type de donn�e (Cf. DataType de la classe cl_image)
%   DataType     : Type de donn�e (Cf. DataType de la classe cl_image)
%   nomDir       : Nom du r�pertoire
%
% Name-Value Pair Arguments
%   resol   : R�solution en m
%   confirm : 1 ou 0
%
% Output Arguments
%   nomFic : Nom du fichier
%   flag   :  1=OK, 0=Abandon
%
% Examples
%   DataType = ALL.identSonarDataType('BeamDepressionAngle');
%   GeometryType = cl_image.indGeometryType('LatLong');
%   [nomFic, flag] = initNomFicErs(cl_ermapper_ers([]), 'Mosaique', GeometryType, DataType, my_tempdir, 'resol', 2.5)
%   [nomFic, flag] = initNomFicErs(cl_ermapper_ers([]), 'Mosaique', GeometryType, DataType, my_tempdir, 'confirm', (get_LevelQuestion >= 3))
%
% See also cl_ermapper_ers cl_ermapper_ers/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [nomFic, flag] = initNomFicErs(~, NomGenerique, GeometryType, DataType, nomDir, varargin)

[varargin, resol]   = getPropertyValue(varargin, 'resol',   []);
[varargin, confirm] = getPropertyValue(varargin, 'confirm', 1); %#ok<ASGLU>

X = cl_image('Name', NomGenerique, 'GeometryType', GeometryType, 'DataType', DataType);

if ~isempty(resol)
    strResol = num2str(resol);
    strResol(strfind(strResol,'.')) = ',';
    X = update_Name(X, 'Append', [strResol 'm']);
else
    X = update_Name(X);
end

nomFic = X.Name;
nomFic = fullfile(nomDir, [nomFic '.ers']);
if confirm
    [flag, nomFic] = my_uiputfile('*.ers', 'Mosaic File', nomFic);
    if ~flag
        return
    end
end
flag = 1;
