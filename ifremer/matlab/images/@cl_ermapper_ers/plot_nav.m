% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   fig = plot_nav(a)
%
% Input Arguments
%   a : Instance de cl_ermapper_ers
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%   fig : Numero de la figure de la navigation
%
% Examples
%   nomDir = 'D:\NIWA_DATA\Haungaroa\Results'
%   [flag, liste, lastDir] = uiSelectFiles('ExtensionFiles', '.ers', 'RepDefaut', nomDir);
%   for i=1:length(liste)
%       a(i) = cl_ermapper_ers('FicIn', liste{i});
%   end
%   plot_nav(a)
%
% See also cl_ermapper_ers plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Fig = plot_nav(this, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

flagFishNavigation = 1;
for i=1:length(this)
    NrOfLines = get(this(i), 'NrOfLines');
    if NrOfLines == 0
        str = sprintf('Empty image : %d/%d', i, length(this));
        my_warndlg(str, 0, 'Tag', 'FichierErsVide');
        continue
    end
    
    a = cl_image('Image', zeros(NrOfLines, 2, 'single'), 'GeometryType', 4);
    [nomDir, nomFic] = fileparts(this(i).FicIn);
    nomFicSgV = fullfile(nomDir, nomFic);
    a = import_ermapper_sigV(a, nomFicSgV, 4);
    
    Longitude = get(a, 'SonarFishLongitude');
    Latitude  = get(a, 'SonarFishLatitude');
    
    if isempty(Longitude) || isempty(Latitude)
        Longitude = get(a, 'ShipLongitude');
        Latitude  = get(a, 'ShipLatitude');
        flagFishNavigation = 0;
    end
    
    SonarTime = get(a, 'SonarTime');
%     SonarSpeed = get(a, 'SonarSpeed');
    Heading = get(a, 'SonarHeading');
    
    plot_navigation(this(i).FicIn, Fig, Longitude, Latitude, SonarTime.timeMat, Heading);
end
if flagFishNavigation
    set(Fig, 'name', 'Fish Navigation');
else
    set(Fig, 'name', 'Ship Navigation');
end
