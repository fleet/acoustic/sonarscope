% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   plot_nav_data(nomFic, Invite)
%
% Input Arguments
%   nomFic : Liste de fichiers .all
%   Invite : Height | Heading | Speed | CourseVessel | SoundSpeed
%
% Examples
%   TODO
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_nav_data(this, nomFic, Invite, varargin) %#ok<INUSL>

[varargin, ZLim] = getPropertyValue(varargin, 'ZLim', []);
if isempty(ZLim)
    ZLimAuto = 1;
    ZLim = [Inf -Inf];
else
    ZLimAuto = 0;
end

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic = length(nomFic);
for k=1:NbFic
    b = cl_ermapper_ers('FicIn', nomFic{k});
    
    % -----------------------------------
    % Lecture des datagrams de navigation
    
    NrOfLines = get(b, 'NrOfLines');
    a(k) = cl_image('Image', zeros(NrOfLines, 2, 'single'), 'GeometryType', 4); %#ok<AGROW>
    [nomDir, nomFicSgV] = fileparts(b.FicIn);
    nomFicSgV = fullfile(nomDir, nomFicSgV);
    a(k) = import_ermapper_sigV(a(k), nomFicSgV, 4); %#ok<AGROW>
end

nbColors = 64;
Ident = rand(1);

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = figure('name', 'Position datagrams');
else
    figure(Fig);
end
% set(Fig, 'Tag', 'Trace de navigation', 'UserData', Ident)
set(Fig, 'UserData', Ident)
title(Invite)
hold on;

% --------------
% Appel recursif

XLim = [Inf -Inf];
YLim = [Inf -Inf];
for i=1:NbFic
    [flag, Z, Position, Colors] = read_Invite(a(i), Invite, nbColors);
    if ~flag
        continue
    end
    if strcmp(Invite, 'Drift')
        ZLim = [-5 5];
    else
        if ZLimAuto
            ZLim(1) = min(ZLim(1), double(min(Z.Data, [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Z.Data, [], 'omitnan')));
        end
    end
    XLim(1) = min(XLim(1), min(Position.Longitude, [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Position.Longitude, [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Position.Latitude,  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Position.Latitude,  [], 'omitnan'));
end

x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar
hold on;

for i=1:length(a)
    plot_position_data_unitaire(a(i), Invite, ZLim, Fig, nbColors, Colors);
end
FigName = sprintf('Sondeur - %s', Invite);
figure(Fig);
set(Fig, 'name', FigName)


function [flag, Z, Position, Colors] = read_Invite(a, Invite, nbColors)

Z = [];
Colors = jet(nbColors);

Position.Longitude = get(a, 'SonarFishLongitude');
Position.Latitude  = get(a, 'SonarFishLatitude');
SonarTime = get(a, 'SonarTime');

switch Invite
    case 'Time'
        Z.Data = SonarTime.timeMat;
        Z.Time = SonarTime;
    case 'Heading'
        Colors = hsv(nbColors);
        Z.Data = get(a, 'SonarHeading');
        Z.Time = SonarTime;
    case 'Speed'
        Z.Data = get(a, 'SonarSpeed');
        Z.Time = SonarTime;
    case 'Height'
        Z.Data = -abs(get(a, 'SonarHeight')); % -abs pour �tre s�r de bien traiter d'anciens fichier
        Z.Time = SonarTime;
    case 'Bathy'
        Z.Data = -abs(get(a, 'SonarHeight')) + get(a, 'SonarImmersion');
        Z.Time = SonarTime;
    case 'PingNumber'
        Z.Data = get(a, 'SonarPingNumber');
        Z.Time = SonarTime;
    case 'Immersion'
        Z.Data = get(a, 'SonarImmersion');
        Z.Time = SonarTime;
    case 'CableOut'
        Z.Data = get(a, 'SonarCableOut');
        Z.Time = SonarTime;
    case 'Roll'
        Z.Data = get(a, 'SonarRoll');
        Z.Time = SonarTime;
    case 'Pitch'
        Z.Data = get(a, 'SonarPitch');
        Z.Time = SonarTime;
    case 'Yaw'
        Z.Data = get(a, 'SonarYaw');
        Z.Time = SonarTime;
    otherwise
        str = sprintf('%s not yet available in plot_nav_data', Invite);
        my_warndlg(str);
end
flag = 1;


function plot_position_data_unitaire(a, Invite, ZLim, Fig, nbColors, Colors)

Data.Longitude = get(a, 'SonarFishLongitude');
Data.Latitude  = get(a, 'SonarFishLatitude');
Data.Time      = get(a, 'SonarTime');

figure(Fig)

[flag, Z] = read_Invite(a, Invite, nbColors);

if ~flag
    return
end
subNonNaN = find(~isnan(Z.Data));
if length(subNonNaN) <= 1
    return
end
subNaN = find(isnan(Z.Data));
Z.Data(subNaN) = interp1(subNonNaN, Z.Data(subNonNaN), subNaN, 'linear', 'extrap');
Value = interp1(Z.Time, Z.Data, Data.Time);
if ZLim(2) == ZLim(1)
    ZLim(1) = ZLim(1) - 0.5;
    ZLim(2) = ZLim(2) + 0.5;
end
coef = (nbColors-1) / (ZLim(2) - ZLim(1));
Value = ceil((Value - ZLim(1)) * coef);



% --------------------------
% Affichage de la navigation

for i=1:nbColors
    sub = find(Value == (i-1));
    if ~isempty(sub)
        h = plot(Data.Longitude(sub), Data.Latitude(sub), '+');
        set(h, 'Color', Colors(i,:))
        set_HitTest_Off(h)
    end
end

sub = find(Value < 0);
if ~isempty(sub)
    h = plot(Data.Longitude(sub), Data.Latitude(sub), '+');
    set(h, 'Color', Colors(1,:)*0.8)
    set_HitTest_Off(h)
end

sub = find(Value > (nbColors-1));
if ~isempty(sub)
    h = plot(Data.Longitude(sub), Data.Latitude(sub), '+');
    set(h, 'Color', Colors(nbColors,:)*0.8)
end

axisGeo;
grid on;
colorbar
drawnow
