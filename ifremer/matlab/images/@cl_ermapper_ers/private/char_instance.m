% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

str{end+1} = sprintf('FicIn           <-> %s', this.FicIn);
str{end+1} = sprintf('Version         <-> %s', this.Version);
str{end+1} = sprintf('Name            <-> %s', this.Name);
str{end+1} = sprintf('Description     <-> %s', this.Description);
% str{end+1} = sprintf('SourceDataset   <-> %s', this.SourceDataset);
str{end+1} = sprintf('LastUpdated     <-> %s', this.LastUpdated);
str{end+1} = sprintf('SensorName      <-> %s', this.SensorName);
str{end+1} = sprintf('SenseDate       <-> %s', this.SenseDate);
str{end+1} = sprintf('DataFile        <-> %s', this.DataFile);
str{end+1} = sprintf('DataSetType     <-> %s', this.DataSetType);
str{end+1} = sprintf('DataType        <-> %s', this.DataType);
str{end+1} = sprintf('ByteOrder       <-> %s', this.ByteOrder);
str{end+1} = sprintf('HeaderOffset    <-> %d', this.HeaderOffset);
str{end+1} = sprintf('Comments        <-> %s', this.Comments);

str{end+1} = sprintf('CoordinateSpace <->');
str{end+1} = sprintf('\tDatum          <-> %s', this.CoordinateSpace.Datum);
str{end+1} = sprintf('\tProjection     <-> %s', this.CoordinateSpace.Projection);
str{end+1} = sprintf('\tCoordinateType <-> %s', this.CoordinateSpace.CoordinateType);
str{end+1} = sprintf('\tUnits          <-> %s', this.CoordinateSpace.Units);
str{end+1} = sprintf('\tRotation       <-> %f', this.CoordinateSpace.Rotation);

str{end+1} = sprintf('RasterInfo <->');
str{end+1} = sprintf('\tCellType          <-> %s', this.RasterInfo.CellType);
str{end+1} = sprintf('\tNullCellValue     <-> %f', this.RasterInfo.NullCellValue);
str{end+1} = sprintf('\tNrOfLines         <-> %d', this.RasterInfo.NrOfLines);
str{end+1} = sprintf('\tNrOfCellsPerLine  <-> %d', this.RasterInfo.NrOfCellsPerLine);
str{end+1} = sprintf('\tNrOfBands         <-> %d', this.RasterInfo.NrOfBands);
str{end+1} = sprintf('\tRegistrationCellX <-> %d', this.RasterInfo.RegistrationCellX);
str{end+1} = sprintf('\tRegistrationCellY <-> %d', this.RasterInfo.RegistrationCellY);

str{end+1} = sprintf('\tCellInfo <->');
str{end+1} = sprintf('\t\tXdimension <-> %f', this.RasterInfo.CellInfo.Xdimension);
str{end+1} = sprintf('\t\tYdimension <-> %f', this.RasterInfo.CellInfo.Ydimension);

str{end+1} = sprintf('\tRegistrationCoord');
str{end+1} = sprintf('\t\tEastings   <-> %f', this.RasterInfo.RegistrationCoord.Eastings);
str{end+1} = sprintf('\t\tNorthings  <-> %f', this.RasterInfo.RegistrationCoord.Northings);
str{end+1} = sprintf('\t\tMetersX    <-> %f', this.RasterInfo.RegistrationCoord.MetersX);
str{end+1} = sprintf('\t\tMetersY    <-> %f', this.RasterInfo.RegistrationCoord.MetersY);
str{end+1} = sprintf('\t\tMetersZ    <-> %f', this.RasterInfo.RegistrationCoord.MetersZ);

str{end+1} = sprintf('\tUserInfo <->');
str{end+1} = sprintf('\t\tImageType                       <-> %f', this.UserInfo.ImageType);
str{end+1} = sprintf('\t\tDataType                      <-> %f', this.UserInfo.DataType);
str{end+1} = sprintf('\t\tGeometryType                 <-> %f', this.UserInfo.GeometryType);
str{end+1} = sprintf('\t\tSpectralStatus                  <-> %f', this.UserInfo.SpectralStatus);
str{end+1} = sprintf('\t\tYDir                            <-> %f', this.UserInfo.YDir);
str{end+1} = sprintf('\t\tXDir                            <-> %f', this.UserInfo.XDir);
str{end+1} = sprintf('\t\tColormapIndex                   <-> %f', this.UserInfo.ColormapIndex);
str{end+1} = sprintf('\t\tColormap                        <-> %s', num2strCode(this.UserInfo.Colormap));
str{end+1} = sprintf('\t\tColormapCustom                  <-> %s', num2strCode(this.UserInfo.ColormapCustom));
str{end+1} = sprintf('\t\tCLim                            <-> %f', this.UserInfo.CLim);
str{end+1} = sprintf('\t\tVideo                           <-> %f', this.UserInfo.Video);
str{end+1} = sprintf('\t\tVertExagAuto                    <-> %f', this.UserInfo.VertExagAuto );
str{end+1} = sprintf('\t\tVertExag                        <-> %f', this.UserInfo.VertExag);
str{end+1} = sprintf('\t\tAzimuth                         <-> %f', this.UserInfo.Azimuth);
str{end+1} = sprintf('\t\tContourValues                   <-> %s', num2strCode(this.UserInfo.ContourValues));
str{end+1} = sprintf('\t\tNuIdentParent                   <-> %d', this.UserInfo.NuIdentParent);
str{end+1} = sprintf('\t\tTagSynchroY                     <-> %s', this.UserInfo.TagSynchroY);
str{end+1} = sprintf('\t\tTagSynchroY                     <-> %s', this.UserInfo.TagSynchroY);
str{end+1} = sprintf('\t\tTagSynchroContrast              <-> %s', this.UserInfo.TagSynchroContrast);
str{end+1} = sprintf('\t\tInitialFileName                 <-> %s', this.UserInfo.InitialFileName);
str{end+1} = sprintf('\t\tHistoCentralClasses             <-> %s', num2strCode(this.UserInfo.HistoCentralClasses));
str{end+1} = sprintf('\t\tHistoValues                     <-> %s', num2strCode(this.UserInfo.HistoValues));
str{end+1} = sprintf('\t\tStatValues.Moyenne              <-> %f', this.UserInfo.StatValues.Moyenne);
str{end+1} = sprintf('\t\tStatValues.Variance             <-> %f', this.UserInfo.StatValues.Variance);
str{end+1} = sprintf('\t\tStatValues.Sigma                <-> %f', this.UserInfo.StatValues.Sigma);
str{end+1} = sprintf('\t\tStatValues.Mediane              <-> %f', this.UserInfo.StatValues.Mediane);
str{end+1} = sprintf('\t\tStatValues.Min                  <-> %f', this.UserInfo.StatValues.Min);
str{end+1} = sprintf('\t\tStatValues.Max                  <-> %f', this.UserInfo.StatValues.Max);
str{end+1} = sprintf('\t\tStatValues.Range                <-> %f', this.UserInfo.StatValues.Range);
str{end+1} = sprintf('\t\tStatValues.Quant_03_97          <-> %s', num2strCode(this.UserInfo.StatValues.Quant_03_97));
str{end+1} = sprintf('\t\tStatValues.Quant_01_99          <-> %s', num2strCode(this.UserInfo.StatValues.Quant_01_99));
str{end+1} = sprintf('\t\tStatValues.Quant_25_75          <-> %s', num2strCode(this.UserInfo.StatValues.Quant_25_75));
str{end+1} = sprintf('\t\tStatValues.Skewness             <-> %f', this.UserInfo.StatValues.Skewness);
str{end+1} = sprintf('\t\tStatValues.Kurtosis             <-> %f', this.UserInfo.StatValues.Kurtosis);
str{end+1} = sprintf('\t\tStatValues.Entropy              <-> %f', this.UserInfo.StatValues.Entropy);
str{end+1} = sprintf('\t\tSonar.Ident                     <-> %f', this.UserInfo.Sonar.Ident);
str{end+1} = sprintf('\t\tSonar.Family                    <-> %f', this.UserInfo.Sonar.Family);
str{end+1} = sprintf('\t\tSonar.RawDataResol              <-> %f', this.UserInfo.Sonar.RawDataResol);
str{end+1} = sprintf('\t\tSonar.ResolutionD               <-> %f', this.UserInfo.Sonar.ResolutionD);
str{end+1} = sprintf('\t\tSonar.ResolutionX               <-> %f', this.UserInfo.Sonar.ResolutionX );
str{end+1} = sprintf('\t\tSonar.TVG.etat                  <-> %f', this.UserInfo.Sonar.TVG.etat);
str{end+1} = sprintf('\t\tSonar.TVG.origine               <-> %f', this.UserInfo.Sonar.TVG.origine);
str{end+1} = sprintf('\t\tSonar.TVG.ConstructTypeCompens  <-> %f', this.UserInfo.Sonar.TVG.ConstructTypeCompens);
str{end+1} = sprintf('\t\tSonar.TVG.ConstructAlpha        <-> %f', this.UserInfo.Sonar.TVG.ConstructAlpha);
str{end+1} = sprintf('\t\tSonar.TVG.ConstructTable        <-> %f', this.UserInfo.Sonar.TVG.ConstructTable);
str{end+1} = sprintf('\t\tSonar.TVG.IfremerAlpha          <-> %f', this.UserInfo.Sonar.TVG.IfremerAlpha);

str{end+1} = sprintf('\t\tSonar.TVG.ConstructCoefDiverg   <-> %f', this.UserInfo.Sonar.TVG.ConstructCoefDiverg);
str{end+1} = sprintf('\t\tSonar.TVG.ConstructConstante    <-> %f', this.UserInfo.Sonar.TVG.ConstructConstante);
str{end+1} = sprintf('\t\tSonar.TVG.IfremerCoefDiverg     <-> %f', this.UserInfo.Sonar.TVG.IfremerCoefDiverg);
str{end+1} = sprintf('\t\tSonar.TVG.IfremerConstante      <-> %f', this.UserInfo.Sonar.TVG.IfremerConstante);

str{end+1} = sprintf('\t\tSonar.DiagEmi.etat              <-> %f', this.UserInfo.Sonar.DiagEmi.etat);
str{end+1} = sprintf('\t\tSonar.DiagEmi.origine           <-> %f', this.UserInfo.Sonar.DiagEmi.origine);
str{end+1} = sprintf('\t\tSonar.DiagEmi.ConstructTypeCompens  <-> %f', this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens);
str{end+1} = sprintf('\t\tSonar.DiagEmi.IfremerTypeCompens    <-> %f', this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens);

str{end+1} = sprintf('\t\tSonar.DiagRec.etat               <-> %f',   this.UserInfo.Sonar.DiagRec.etat);
str{end+1} = sprintf('\t\tSonar.DiagRec.origine            <-> %f',   this.UserInfo.Sonar.DiagRec.origine);
str{end+1} = sprintf('\t\tSonar.DiagRec.ConstructTypeCompens <-> %f', this.UserInfo.Sonar.DiagRec.ConstructTypeCompens);
str{end+1} = sprintf('\t\tSonar.DiagRec.IfremerTypeCompens   <-> %f', this.UserInfo.Sonar.DiagRec.IfremerTypeCompens);

str{end+1} = sprintf('\t\tSonar.AireInso.etat              <-> %f', this.UserInfo.Sonar.AireInso.etat);
str{end+1} = sprintf('\t\tSonar.AireInso.origine           <-> %f', this.UserInfo.Sonar.AireInso.origine);

str{end+1} = sprintf('\t\tSonar.BS.etat                    <-> %f', this.UserInfo.Sonar.BS.etat);
str{end+1} = sprintf('\t\tSonar.BS.origine                 <-> %f', this.UserInfo.Sonar.BS.origine);
str{end+1} = sprintf('\t\tSonar.BS.origineBelleImage       <-> %f', this.UserInfo.Sonar.BS.origineBelleImage);
str{end+1} = sprintf('\t\tSonar.BS.origineFullCompens      <-> %f', this.UserInfo.Sonar.BS.origineFullCompens);
str{end+1} = sprintf('\t\tSonar.BS.IfremerCompensTable     <-> %f', this.UserInfo.Sonar.BS.IfremerCompensTable);


str{end+1} = sprintf('\tBandId');
for i=1:this.RasterInfo.NrOfBands
    str{end+1} = sprintf('\t\tValue(%d)   <-> %s', i, this.RasterInfo.BandId(i).Value); %#ok<AGROW>
    %         str{end+1} = sprintf('\t\tWidth(%d)   <-> %s', i, this.RasterInfo.BandId(i).Width);
    str{end+1} = sprintf('\t\tUnits(%d)   <-> %s', i, this.RasterInfo.BandId(i).Units); %#ok<AGROW>
    str{end+1} = sprintf('\t\tComment(%d) <-> %s', i, this.RasterInfo.BandId(i).Comment); %#ok<AGROW>
end

for i=1:length(this.RasterInfo.RegionInfo)
    str{end+1} = sprintf('\tRegionInfo(%d)', i); %#ok<AGROW>
    str{end+1} = sprintf('\t\tType          <-> %s', this.RasterInfo.RegionInfo(i).Type); %#ok<AGROW>
    str{end+1} = sprintf('\t\tRegionName    <-> %s', this.RasterInfo.RegionInfo(i).RegionName); %#ok<AGROW>
    str{end+1} = sprintf('\t\tSourceDataset <-> %s', this.RasterInfo.RegionInfo(i).SourceDataset); %#ok<AGROW>

    str{end+1} = sprintf('\t\tRGBcolour'); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tRed   <-> %d', this.RasterInfo.RegionInfo(i).RGBcolour.Red); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tGreen <-> %d', this.RasterInfo.RegionInfo(i).RGBcolour.Green); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tBlue  <-> %d', this.RasterInfo.RegionInfo(i).RGBcolour.Blue); %#ok<AGROW>

    str{end+1} = sprintf('\t\tClassNumber   <-> %d', this.RasterInfo.RegionInfo(i).ClassNumber); %#ok<AGROW>

    for j=1:length(this.RasterInfo.RegionInfo(i).SubRegion)
        str{end+1} = sprintf('\t\tSubRegion (%d) <-> %s', j, num2strCode(this.RasterInfo.RegionInfo(i).SubRegion{j})); %#ok<AGROW>
    end

    str{end+1} = sprintf('\t\tStats'); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tSubsampleRate        <-> %d',             this.RasterInfo.RegionInfo(i).Stats.SubsampleRate); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tNumberOfBands        <-> %d',             this.RasterInfo.RegionInfo(i).Stats.NumberOfBands); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tNumberOfNonNullCells <-> %s', num2strCode(this.RasterInfo.RegionInfo(i).Stats.NumberOfNonNullCells)); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tMinimumValue         <-> %s', num2strCode(this.RasterInfo.RegionInfo(i).Stats.MinimumValue)); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tMaximumValue         <-> %s', num2strCode(this.RasterInfo.RegionInfo(i).Stats.MaximumValue)); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tMeanValue            <-> %s', num2strCode(this.RasterInfo.RegionInfo(i).Stats.MeanValue)); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tMedianValue          <-> %s', num2strCode(this.RasterInfo.RegionInfo(i).Stats.MedianValue)); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\tCovarianceMatrix     <-> %s', num2strCode(this.RasterInfo.RegionInfo(i).Stats.CovarianceMatrix)); %#ok<AGROW>
end

str{end+1} = sprintf('-------------------------------');
str{end+1} = sprintf('Interpretated CoordinateSpace : ');

if ~isempty(this.Ellipsoide)
    str{end+1} = sprintf('\tEllipsoide.name         <-- %s', this.Ellipsoide.name);
    str{end+1} = sprintf('\tEllipsoide.specified    <-- %f', this.Ellipsoide.specified);
    str{end+1} = sprintf('\tEllipsoide.radius       <-- %f', this.Ellipsoide.radius);
    str{end+1} = sprintf('\tEllipsoide.eccentricity <-- %f', this.Ellipsoide.eccentricity);
    str{end+1} = sprintf('\tEllipsoide.flattening   <-- %f', this.Ellipsoide.flattening);
    str{end+1} = sprintf('\tEllipsoide.metre_factor <-- %f', this.Ellipsoide.metre_factor);
    str{end+1} = sprintf('\tEllipsoide.prime_merid  <-- %f', this.Ellipsoide.prime_merid);
    str{end+1} = sprintf('\tEllipsoide.merid_value  <-- %f', this.Ellipsoide.merid_value);
end

str{end+1} = sprintf('\tProjection.Type <-- %s', SelectionDansListe2str(this.Projection.Type, this.Projection.strType, 'Num'));

if this.Projection.Type == 2
    P = this.Projection.Parametres.Mercator;
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.false_north  <-- %f', P.false_north);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.false_east   <-- %f', P.false_east);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.scale_factor <-- %f', P.scale_factor);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.centre_merid <-- %f', P.centre_merid);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.lat_ech_cons <-- %f', P.lat_ech_cons);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.radius       <-- %f', P.radius);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.eccentricity <-- %f', P.eccentricity);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.flattening   <-- %f', P.flattening);
    str{end+1} = sprintf('\tProjection.Parametres.Mercator.prime_merid  <-- %f', P.prime_merid);
end
if this.Projection.Type == 3
    P = this.Projection.Parametres.UTM;
    str{end+1} = sprintf('\tProjection.Parametres.UTM.Fuseau     <-- %d', P.Fuseau);
    str{end+1} = sprintf('\tProjection.Parametres.UTM.Hemisphere <-- %s', P.Hemisphere);
end

if this.Projection.Type == 7
    P = this.Projection.Parametres.Stereo;
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.false_north   <-- %f', P.false_north);
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.false_east    <-- %f', P.false_east);
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.scale_factor  <-- %f', P.scale_factor);
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.centre_merid  <-- %f : %s', P.centre_merid, lon2str(P.centre_merid * (180/pi)));
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.origin_lat    <-- %f : %s', P.origin_lat, lat2str(P.origin_lat * (180/pi)));
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.true_lat      <-- %f', P.true_lat);
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.radius        <-- %f', P.radius);
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.eccentricity  <-- %f', P.eccentricity);
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.flattening    <-- %f', P.flattening);
    str{end+1} = sprintf('\tProjection.Parametres.Stereo.prime_merid   <-- %f', P.prime_merid);
end

if this.Projection.Type == 4
    P = this.Projection.Parametres.Lambert;
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_north   <-- %f', P.false_north);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_east    <-- %f', P.false_east);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.scale_factor  <-- %f', P.scale_factor);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.centre_merid  <-- %f', P.centre_merid);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.centre_lat    <-- %f', P.centre_lat);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.radius        <-- %f', P.radius);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.eccentricity  <-- %f', P.eccentricity);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.flattening    <-- %f', P.flattening);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.prime_merid   <-- %f', P.prime_merid);
end

if this.Projection.Type == 5

    P = this.Projection.Parametres.Lambert;
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_north   <-- %f', P.false_north);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_east    <-- %f', P.false_east);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_olat    <-- %f', P.false_olat);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_olon    <-- %f', P.false_olon);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.scale_factor  <-- %f', P.scale_factor);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.centre_merid  <-- %f', P.centre_merid);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.first_paral   <-- %f', P.first_paral);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.second_paral  <-- %f', P.second_paral);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.centre_lat    <-- %f', P.centre_lat);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.defined       <-- %f', P.defined);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.grid_north    <-- %f', P.grid_north);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_north   <-- %f', P.false_north);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.false_east    <-- %f', P.false_east);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.radius        <-- %f', P.radius);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.eccentricity  <-- %f', P.eccentricity);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.flattening    <-- %f', P.flattening);
    str{end+1} = sprintf('\tProjection.Parametres.Lambert.prime_merid   <-- %f', P.prime_merid);
end

if this.Projection.Type == 6

    P = this.Projection.Parametres.Lamcon2;
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.false_north   <-- %f', P.false_north);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.false_east    <-- %f', P.false_east);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.false_olat    <-- %f', P.false_olat);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.false_olon    <-- %f', P.false_olon);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.scale_factor  <-- %f', P.scale_factor);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.centre_merid  <-- %f', P.centre_merid);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.first_paral   <-- %f', P.first_paral);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.second_paral  <-- %f', P.second_paral);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.centre_lat    <-- %f', P.centre_lat);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.defined       <-- %f', P.defined);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.grid_north    <-- %f', P.grid_north);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.false_north   <-- %f', P.false_north);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.false_east    <-- %f', P.false_east);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.radius        <-- %f', P.radius);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.eccentricity  <-- %f', P.eccentricity);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.flattening    <-- %f', P.flattening);
    str{end+1} = sprintf('\tProjection.Parametres.Lamcon2.prime_merid   <-- %f', P.prime_merid);


end
%     str{end+1} = sprintf('--- aggregation Images (cl_image) ---');
%     str{end+1} = char(this.Images);

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
