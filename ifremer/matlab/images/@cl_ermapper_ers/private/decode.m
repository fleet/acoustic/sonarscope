% Decodage du fichier de description d'une image ER-Mapper
%
% Syntax
%   str = decode(a)
%
% Input Arguments
%   a : instance de cl_ermapper_ers
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_ermapper_ers Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = decode(this, varargin)

% L'importation dans ArcGis ainsi que le nouvel ErViewer ne supportent plus
% les commentaires dans le fichier .ers, on est contraint de faire de la
% bidouille pour s'adapter aux anciens et aux nouveaux fichiers (on s�pare
% les fichier en deux pppp.ers et pppp/pppp_UserData.txt
[varargin, flagUserInfo] = getFlag(varargin, 'UserInfo'); %#ok<ASGLU>

% -------------------------------------------------------------------------
% On regarde si le fichier de sauvegarde cr�� dans cl_ermapper/write existe
% et s'il est posterieur au fichier .ers. Dans ce cas on ne fait pas de
% d�codage, �a acc�l�re beaucoup l'importation

[nomDir, nomFicSauve] = fileparts(this.FicIn);
nomDirImage = fullfile(nomDir, nomFicSauve);
if exist(nomDirImage, 'dir')
    nomFicSauve = fullfile(nomDirImage, [nomFicSauve '_ers.mat']);
else
    nomFicSauve = [nomDirImage '_ers.mat'];
end

% [nomDir, nomFic] = fileparts(this.FicIn);

flag = exist(nomFicSauve, 'file');
if flag
    s_mat = dir(nomFicSauve);
    s_ers = dir(this.FicIn);
    
    s_mat.date = FRMonths2USMonths(s_mat.date);
    s_ers.date = FRMonths2USMonths(s_ers.date);
    
    try %                                                   733226=03/07/07
        if (datenum(s_mat.date) >= datenum(s_ers.date)) && (datenum(s_mat.date) >  733226)
            FicIn = this.FicIn;
            this = loadmat(nomFicSauve, 'nomVar', 'this');
            this.FicIn = FicIn;
            if isequal(this.UserInfo.NuIdentParent, -1)
                this.UserInfo.NuIdentParent = [];
            end
            return
        elseif datenum(s_mat.date) >= datenum(s_ers.date) % Ajout JMA le 10/06/2020 pour Charline
            FicIn = this.FicIn;
            this = loadmat(nomFicSauve, 'nomVar', 'this');
            this.FicIn = FicIn;
            if isequal(this.UserInfo.NuIdentParent, -1)
                this.UserInfo.NuIdentParent = [];
            end
            [~, nomSeul] = fileparts(nomFicSauve);
            this.FicIn = fullfile(nomDir, nomSeul);
            return
            
        end
    catch %#ok<CTCH>
        % En Linux francais la date rendue par 'dir' n'est pas decodable
        % dans datenum
    end
end

%% Maintenance version de fichier .ers

MaintenanceFichierErs(this)

%% Cas o� il n'y a pas de fichier matlab de sauvegarde

fid = fopen(this.FicIn, 'r');
if fid == -1
    str1 = sprintf('Fichier "%s" non trouv�', this.FicIn);
    str2 = sprintf('File "%s" not found', this.FicIn);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierUserDefautPasTrouv�');
    return
end

L0 = [];
while feof(fid) == 0
    str = fgetl(fid);
    str = deblank(str);
    if isempty(str)
        continue
    end
    
    L0{end+1} = str; %#ok
end

%% Fermeture du fichier

fclose(fid);

%% Extraction des lignes du DatasetHeader

[L0, L1] = extract_struct(L0, 'DatasetHeader');
if ~isempty(L0) && ~flagUserInfo
    messageLignesNonDecodees(L0);
end

[L1, this.Version]       = extract_variable(L1, 'Version',       'char');
[L1, this.Name]          = extract_variable(L1, 'Name',          'char');
[L1, this.Description]   = extract_variable(L1, 'Description',   'char');
[L1, this.SourceDataset] = extract_variable(L1, 'SourceDataset', 'char');
[L1, this.LastUpdated]   = extract_variable(L1, 'LastUpdated',   'char');
[L1, this.SensorName]    = extract_variable(L1, 'SensorName',    'char');
[L1, this.SenseDate]     = extract_variable(L1, 'SenseDate',     'char');
[L1, this.DataFile]      = extract_variable(L1, 'DataFile',      'char');
[L1, this.DataSetType]   = extract_variable(L1, 'DataSetType',   'char');
[L1, this.DataType]      = extract_variable(L1, 'DataType',      'char');
[L1, this.ByteOrder]     = extract_variable(L1, 'ByteOrder',     'char');
[L1, this.HeaderOffset]  = extract_variable(L1, 'HeaderOffset',  'double');
[L1, this.Comments]      = extract_variable(L1, 'Comments',      'char');

%% Extraction des lignes de CoordinateSpace

[L1, L2] = extract_struct(L1, 'CoordinateSpace');
[L2, this.CoordinateSpace.Datum]          = extract_variable(L2, 'Datum',          'char');
[L2, this.CoordinateSpace.Projection]     = extract_variable(L2, 'Projection',     'char');
[L2, this.CoordinateSpace.CoordinateType] = extract_variable(L2, 'CoordinateType', 'char');
[L2, this.CoordinateSpace.Units]          = extract_variable(L2, 'Units',          'char');
[L2, this.CoordinateSpace.Rotation]       = extract_variable(L2, 'Rotation',       'char');
if ~isempty(L2)
    messageLignesNonDecodees(L2);
    return
end

%% Extraction des lignes de UserInfo

if flagUserInfo
    L1 = L0; % Bidouille pour s'adapter � la sortie des commentaires du .ers
end
[L1, L2] = extract_struct(L1, 'UserInfo');
[L2, this.UserInfo.ImageType]                       = extract_variable(L2, 'ImageType',          'double');
[L2, this.UserInfo.DataType]                        = extract_variable(L2, 'DataType',           'double');
[L2, this.UserInfo.GeometryType]                    = extract_variable(L2, 'GeometryType',       'double');
[L2, this.UserInfo.Unit]                            = extract_variable(L2, 'Unit',               'char');
[L2, this.UserInfo.XUnit]                           = extract_variable(L2, 'XUnit',              'char');
[L2, this.UserInfo.YUnit]                           = extract_variable(L2, 'YUnit',              'char');
[L2, this.UserInfo.XLabel]                          = extract_variable(L2, 'XLabel',             'char');
[L2, this.UserInfo.YLabel]                          = extract_variable(L2, 'YLabel',             'char');
[L2, this.UserInfo.SpectralStatus]                  = extract_variable(L2, 'SpectralStatus',     'double');
[L2, this.UserInfo.YDir]                            = extract_variable(L2, 'YDir',               'double');
[L2, this.UserInfo.XDir]                            = extract_variable(L2, 'XDir',               'double');
[L2, this.UserInfo.ColormapIndex]                   = extract_variable(L2, 'ColormapIndex',      'double');
[L2, this.UserInfo.CLim]                            = extract_variable(L2, 'CLim',               'double');
[L2, this.UserInfo.Video]                           = extract_variable(L2, 'Video',              'double');
[L2, this.UserInfo.VertExagAuto]                    = extract_variable(L2, 'VertExagAuto',       'double');
[L2, this.UserInfo.VertExag]                        = extract_variable(L2, 'VertExag',           'double');
[L2, this.UserInfo.Azimuth]                         = extract_variable(L2, 'Azimuth',            'double');
[L2, this.UserInfo.ContourValues]                   = extract_variable(L2, 'ContourValues',      'double');
[L2, this.UserInfo.NuIdentParent]                   = extract_variable(L2, 'NuIdentParent',      'double');
[L2, this.UserInfo.TagSynchroX]                     = extract_variable(L2, 'TagSynchroX',        'char');
[L2, this.UserInfo.TagSynchroY]                     = extract_variable(L2, 'TagSynchroY',        'char');
[L2, this.UserInfo.TagSynchroContrast]              = extract_variable(L2, 'TagSynchroContrast', 'char');
[L2, this.UserInfo.InitialFileName]                 = extract_variable(L2, 'InitialFileName',    'char');
[L2, this.UserInfo.InitialFileFormat]               = extract_variable(L2, 'InitialFileFormat',  'char');

[L2, this.UserInfo.HistoCentralClasses]            = extract_variable(L2, 'HistoCentralClasses',            'double');
[L2, this.UserInfo.HistoValues]                    = extract_variable(L2, 'HistoValues',                    'double');
[L2, this.UserInfo.Sonar.Ident]                    = extract_variable(L2, 'Sonar.Ident',                    'double');
[L2, this.UserInfo.Sonar.Family]                   = extract_variable(L2, 'Sonar.Family',                   'double');
[L2, this.UserInfo.Sonar.RawDataResol]             = extract_variable(L2, 'Sonar.RawDataResol',             'double');
[L2, this.UserInfo.Sonar.ResolutionX]              = extract_variable(L2, 'Sonar.ResolutionX',              'double');
[L2, this.UserInfo.Sonar.ResolutionD]              = extract_variable(L2, 'Sonar.ResolutionD',              'double');
[L2, this.UserInfo.Sonar.TVG.etat]                 = extract_variable(L2, 'Sonar.TVG.etat',                 'double');
[L2, this.UserInfo.Sonar.TVG.origine]              = extract_variable(L2, 'Sonar.TVG.origine',              'double');
[L2, this.UserInfo.Sonar.TVG.ConstructTypeCompens] = extract_variable(L2, 'Sonar.TVG.ConstructTypeCompens', 'double');
[L2, this.UserInfo.Sonar.TVG.ConstructAlpha]       = extract_variable(L2, 'Sonar.TVG.ConstructAlpha',       'double');
[L2, this.UserInfo.Sonar.TVG.ConstructTable]       = extract_variable(L2, 'Sonar.TVG.ConstructTable',       'double');
[L2, this.UserInfo.Sonar.TVG.ConstructTable]       = extract_variable(L2, 'Sonar.TVG.ConstructTable',       'double');
[L2, this.UserInfo.Sonar.TVG.IfremerAlpha]         = extract_variable(L2, 'Sonar.TVG.IfremerAlpha',         'double');

[L2, this.UserInfo.Sonar.NE.etat]                      = extract_variable(L2, 'Sonar.NE.etat',                     'double');
[L2, this.UserInfo.Sonar.GT.etat]                      = extract_variable(L2, 'Sonar.GT.etat',                     'double');
[L2, this.UserInfo.Sonar.SH.etat]                      = extract_variable(L2, 'Sonar.SH.etat',                     'double');
[L2, this.UserInfo.Sonar.TVG.ConstructCoefDiverg]      = extract_variable(L2, 'Sonar.TVG.ConstructCoefDiverg',     'double');
[L2, this.UserInfo.Sonar.TVG.ConstructConstante]       = extract_variable(L2, 'Sonar.TVG.ConstructConstante',      'double');
[L2, this.UserInfo.Sonar.TVG.IfremerCoefDiverg]        = extract_variable(L2, 'Sonar.TVG.IfremerCoefDiverg',       'double');
[L2, this.UserInfo.Sonar.TVG.IfremerConstante]         = extract_variable(L2, 'Sonar.TVG.IfremerConstante',        'double');
[L2, this.UserInfo.Sonar.DiagEmi.etat]                 = extract_variable(L2, 'Sonar.DiagEmi.etat',                 'double');
[L2, this.UserInfo.Sonar.DiagEmi.origine]              = extract_variable(L2, 'Sonar.DiagEmi.origine',              'double');
[L2, this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens] = extract_variable(L2, 'Sonar.DiagEmi.ConstructTypeCompens', 'double');
[L2, this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens]   = extract_variable(L2, 'Sonar.DiagEmi.IfremerTypeCompens',   'double');
[L2, this.UserInfo.Sonar.DiagRec.etat]                 = extract_variable(L2, 'Sonar.DiagRec.etat',                 'double');
[L2, this.UserInfo.Sonar.DiagRec.origine]              = extract_variable(L2, 'Sonar.DiagRec.origine',              'double');
[L2, this.UserInfo.Sonar.DiagRec.ConstructTypeCompens] = extract_variable(L2, 'Sonar.DiagRec.ConstructTypeCompens', 'double');
[L2, this.UserInfo.Sonar.DiagRec.IfremerTypeCompens]   = extract_variable(L2, 'Sonar.DiagRec.IfremerTypeCompens',   'double');
[L2, this.UserInfo.Sonar.AireInso.etat]                = extract_variable(L2, 'Sonar.AireInso.etat',                'double');
[L2, this.UserInfo.Sonar.AireInso.origine]             = extract_variable(L2, 'Sonar.AireInso.origine',             'double');
[L2, this.UserInfo.Sonar.BS.etat]                      = extract_variable(L2, 'Sonar.BS.etat',                      'double');
[L2, this.UserInfo.Sonar.BS.origine]                   = extract_variable(L2, 'Sonar.BS.origine',                   'double');
[L2, this.UserInfo.Sonar.BS.origineBelleImage]         = extract_variable(L2, 'Sonar.BS.origineBelleImage',         'double');
[L2, this.UserInfo.Sonar.BS.origineFullCompens]        = extract_variable(L2, 'Sonar.BS.origineFullCompens',        'double');
[L2, this.UserInfo.Sonar.BS.IfremerCompensTable]       = extract_variable(L2, 'Sonar.BS.IfremerCompensTable',       'double');


[L2, this.UserInfo.Colormap]               = extract_variable(L2, 'Colormap',               'double');
[L2, this.UserInfo.ColormapCustom]         = extract_variable(L2, 'ColormapCustom',         'double');
[L2, this.UserInfo.StatValues.Moyenne]     = extract_variable(L2, 'StatValues.Moyenne',     'double');
[L2, this.UserInfo.StatValues.Variance]    = extract_variable(L2, 'StatValues.Variance',    'double');
[L2, this.UserInfo.StatValues.Sigma]       = extract_variable(L2, 'StatValues.Sigma',       'double');
[L2, this.UserInfo.StatValues.Mediane]     = extract_variable(L2, 'StatValues.Mediane',     'double');
[L2, this.UserInfo.StatValues.Min]         = extract_variable(L2, 'StatValues.Min',         'double');
[L2, this.UserInfo.StatValues.Max]         = extract_variable(L2, 'StatValues.Max',         'double');
[L2, this.UserInfo.StatValues.Range]       = extract_variable(L2, 'StatValues.Range',       'double');
[L2, this.UserInfo.StatValues.Quant_03_97] = extract_variable(L2, 'StatValues.Quant_03_97', 'double');
[L2, this.UserInfo.StatValues.Quant_01_99] = extract_variable(L2, 'StatValues.Quant_01_99', 'double');
[L2, this.UserInfo.StatValues.Quant_25_75] = extract_variable(L2, 'StatValues.Quant_25_75', 'double');
[L2, this.UserInfo.StatValues.Sampling]    = extract_variable(L2, 'StatValues.Sampling',    'double');
[L2, this.UserInfo.StatValues.PercentNaN]  = extract_variable(L2, 'StatValues.PercentNaN',  'double');
[L2, this.UserInfo.StatValues.Skewness]    = extract_variable(L2, 'StatValues.Skewness',    'double');
[L2, this.UserInfo.StatValues.Kurtosis]    = extract_variable(L2, 'StatValues.Kurtosis',    'double');
[L2, this.UserInfo.StatValues.Entropy]     = extract_variable(L2, 'StatValues.Entropy',      'double');

[L2, this.UserInfo.Sonar.TVG.IfremerAlpha]  = extract_variable(L2, '#Sonar.TVG.IfremerAlpha', 'double');

if ~isempty(L2)
    messageLignesNonDecodees(L2);
    return
end

%% Extraction des lignes de Algorithm

[L1, L2] = extract_struct(L1, 'Algorithm'); %#ok

%% Extraction des lignes de RasterInfo

[L1, L2] = extract_struct(L1, 'RasterInfo');
if ~isempty(L1)
    messageLignesNonDecodees(L1);
    return
end

[L2, this.RasterInfo.CellType]           = extract_variable(L2, 'CellType',          'char');
[L2, this.RasterInfo.NrOfLines]          = extract_variable(L2, 'NrOfLines',         'double');
[L2, this.RasterInfo.NrOfCellsPerLine]   = extract_variable(L2, 'NrOfCellsPerLine',  'double');
[L2, this.RasterInfo.NrOfBands]          = extract_variable(L2, 'NrOfBands',         'double');
[L2, this.RasterInfo.NullCellValue]      = extract_variable(L2, 'NullCellValue',     'double');
if this.RasterInfo.NullCellValue > 3.4e38
    %     this.RasterInfo.NullCellValue = NaN;
end
[L2, this.RasterInfo.RegistrationCellX]  = extract_variable(L2, 'RegistrationCellX', 'double');
[L2, this.RasterInfo.RegistrationCellY]  = extract_variable(L2, 'RegistrationCellY', 'double');

%% Extraction des lignes de CellInfo

[L2, L3] = extract_struct(L2, 'CellInfo');
[L3, this.RasterInfo.CellInfo.Xdimension]    = extract_variable(L3, 'Xdimension',    'double');
[L3, this.RasterInfo.CellInfo.Ydimension]    = extract_variable(L3, 'Ydimension',    'double');
if ~isempty(L3)
    messageLignesNonDecodees(L3);
    return
end

%% Extraction des lignes de RegistrationCoord

[L2, L3] = extract_struct(L2, 'RegistrationCoord');
if strcmp(this.CoordinateSpace.Projection, 'GEODETIC')
    [L3, V] = extract_variable(L3, 'Longitude',  'char');
    if ~isempty(V)
        mots = strsplit(V, ':');
        if length(mots) == 1
            lon = str2double(mots{1});
        else
            deg = str2double(mots{1});
            min = str2double(mots{2});
            sec = str2double(mots{3});
            lon = sign(deg) * (abs(deg) + min/60 + sec/3600);
        end
        this.RasterInfo.RegistrationCoord.Eastings = lon;
    else
        [L3, this.RasterInfo.RegistrationCoord.Eastings] = extract_variable(L3, 'Eastings',  'double');
    end
    
    [L3, V]   = extract_variable(L3, 'Latitude', 'char');
    if ~isempty(V)
        mots = strsplit(V, ':');
        if length(mots) == 1
            lat = str2double(mots{1});
        else
            deg = str2double(mots{1});
            min = str2double(mots{2});
            sec = str2double(mots{3});
            lat = sign(deg) * (abs(deg) + min/60 + sec/3600);
        end
        this.RasterInfo.RegistrationCoord.Northings = lat;
    else
        [L3, this.RasterInfo.RegistrationCoord.Northings] = extract_variable(L3, 'Northings', 'double');
    end
else
    [L3, this.RasterInfo.RegistrationCoord.Eastings]    = extract_variable(L3, 'Eastings',  'double');
    [L3, this.RasterInfo.RegistrationCoord.Northings]   = extract_variable(L3, 'Northings', 'double');
end
[L3, this.RasterInfo.RegistrationCoord.MetersX]     = extract_variable(L3, 'MetersX',   'double');
[L3, this.RasterInfo.RegistrationCoord.MetersY]     = extract_variable(L3, 'MetersY',   'double');
[L3, this.RasterInfo.RegistrationCoord.MetersZ]     = extract_variable(L3, 'MetersZ',   'double');
if ~isempty(L3)
    messageLignesNonDecodees(L3);
    return
end

%% Extraction des lignes de SensorInfo

[L2, L3] = extract_struct(L2, 'SensorInfo'); %#ok

%% Extraction des lignes de WarpControl

[L2, L3] = extract_struct(L2, 'WarpControl'); %#ok

%% Extraction des lignes de BandId

for k1=1:this.RasterInfo.NrOfBands
    [L2, L3] = extract_struct(L2, 'BandId');
    [L3, str]     = extract_variable(L3, 'Value',  'char');
    this.RasterInfo.BandId(k1).Value = str;
    [L3, X]     = extract_variable(L3, 'Width',  'double');
    this.RasterInfo.BandId(k1).Width = X;
    [L3, str]     = extract_variable(L3, 'Units',  'char');
    this.RasterInfo.BandId(k1).Units = str;
    [L3, str]     = extract_variable(L3, 'Comment',  'char');
    this.RasterInfo.BandId(k1).Comment = str;
    
    if ~isempty(L3)
        messageLignesNonDecodees(L3);
        return
    end
end

%% Extraction des lignes de RegionInfo

nbRegionInfo = getNbOccurrences_struct(L2, 'RegionInfo');
for k1=1:nbRegionInfo
    [L2, L3] = extract_struct(L2, 'RegionInfo');
    [L3, str] = extract_variable(L3, 'Type', 'char');
    this.RasterInfo.RegionInfo(k1).Type = str;
    [L3, str] = extract_variable(L3, 'RegionName', 'char');
    this.RasterInfo.RegionInfo(k1).RegionName = str;
    [L3, str] = extract_variable(L3, 'SourceDataset', 'char');
    this.RasterInfo.RegionInfo(k1).SourceDataset = str;
    
    [L3, L4] = extract_struct(L3, 'RGBcolour');
    [L4, X] = extract_variable(L4, 'Red', 'double');
    this.RasterInfo.RegionInfo(k1).RGBcolour.Red = X;
    [L4, X] = extract_variable(L4, 'Green', 'double');
    this.RasterInfo.RegionInfo(k1).RGBcolour.Green = X;
    [L4, X] = extract_variable(L4, 'Blue', 'double');
    this.RasterInfo.RegionInfo(k1).RGBcolour.Blue = X;
    if ~isempty(L4)
        messageLignesNonDecodees(L4);
        return
    end
    
    [L3, str] = extract_variable(L3, 'ClassNumber', 'char');
    this.RasterInfo.RegionInfo(k1).ClassNumber = str;
    
    % ----------------------------------
    % Extraction des lignes de SubRegion
    
    nbSubRegion = getNbOccurrences_variable(L3, 'SubRegion');
    for iSubRegion=1:nbSubRegion
        [L3, X] = extract_variable(L3, 'SubRegion', 'double');
        this.RasterInfo.RegionInfo(k1).SubRegion{iSubRegion} = X;
    end
    
    % ------------------------------
    % Extraction des lignes de Stats
    
    [L3, L4] = extract_struct(L3, 'Stats');
    if ~isempty(L3)
        messageLignesNonDecodees(L3);
        return
    end
    
    [L4, X] = extract_variable(L4, 'SubsampleRate', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.SubsampleRate = X;
    [L4, X] = extract_variable(L4, 'NumberOfBands', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.NumberOfBands = X;
    [L4, X] = extract_variable(L4, 'NumberOfNonNullCells', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.NumberOfNonNullCells = X;
    [L4, X] = extract_variable(L4, 'NumberOfNullCells', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.NumberOfNullCells = X;
    [L4, X] = extract_variable(L4, 'MinimumValue', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.MinimumValue = X;
    [L4, X] = extract_variable(L4, 'MaximumValue', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.MaximumValue = X;
    [L4, X] = extract_variable(L4, 'MeanValue', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.MeanValue = X;
    [L4, X] = extract_variable(L4, 'MedianValue', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.MedianValue = X;
    [L4, X] = extract_variable(L4, 'CovarianceMatrix', 'double');
    this.RasterInfo.RegionInfo(k1).Stats.CovarianceMatrix = X;
    if ~isempty(L4)
        messageLignesNonDecodees(L4);
        return
    end
    
end

if ~isempty(L2)
    messageLignesNonDecodees(L2);
    return
end


%% Interpretation des codes lus

if ~isempty(this.CoordinateSpace.Datum)
    if strcmp(this.CoordinateSpace.Datum, 'RAW')
        this.Ellipsoide = [];
    else
        this.Ellipsoide = decode_ellipsoide(this);
    end
else
    this.Ellipsoide = [];
end

if ~isempty(this.CoordinateSpace.Projection)
    [Type, Parametres] = decode_projection(this);
    this.Projection.Type = Type;
    this.Projection.Parametres = Parametres;
    
    % Heuristique pour definir les coordonnees metriques dans un fichier
    % non g�n�r� par SonarScope
    if isempty(this.UserInfo.GeometryType)
        this.UserInfo.GeometryType = cl_image.indGeometryType('GeoYX');
    end
    
else
    this.Projection.Type = 1;
end

if isequal(this.UserInfo.NuIdentParent, -1)
    this.UserInfo.NuIdentParent = [];
end

try
    save(nomFicSauve, 'this');
catch %#ok<CTCH>
    str = sprintf('File %s couldn''t be saved, do not panic it is a warning.', nomFicSauve);
    my_warndlg(str, 0);
end



%% Recherche du nombre d'occurences de strIdent

function nbOccurrences = getNbOccurrences_struct(linesIn, strIdent)

nbOccurrences = 0;
for k1=1:length(linesIn)
    mots = strsplit(linesIn{k1});
    if length(mots) == 2
        if strcmpi(mots{1}, strIdent) && strcmpi(mots{2}, 'Begin')
            nbOccurrences = nbOccurrences + 1;
        end
    end
end

%% Extraction des lignes comprises entre strIdent Begin et strIdent End

function [linesIn, linesOut] = extract_struct(linesIn, strIdent)

iDeb = 0;
iFin = 0;
for k1=1:length(linesIn)
    mots = strsplit(linesIn{k1});
    
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Verrue a supprimer des que UserInfo est resolu
    if strcmp(mots{1}, '##')
        mots(1) = [];
    end
    
    if length(mots) == 2
        if strcmpi(mots{1}, strIdent) && strcmpi(mots{2}, 'Begin')
            iDeb = k1+1;
        end
        if strcmpi(mots{1}, strIdent) && strcmpi(mots{2}, 'End')
            iFin = k1-1;
            break
        end
    end
end
if (iDeb ~= 0) && (iFin ~= 0)
    subLines = (iDeb-1):(iFin+1);
    linesOut = linesIn(iDeb:iFin);
    linesIn(subLines) = [];
    for k=1:length(linesOut)
        linesOut{k} = strtrim(linesOut{k});
    end
else
    linesOut = [];
end



%% Recherche du nombre d'occurences de strIdent

function nbOccurrences = getNbOccurrences_variable(linesIn, strIdent)

nbOccurrences = 0;
for k1=1:length(linesIn)
    line = linesIn{k1};
    mots = strsplit(line);
    if length(mots) >= 3
        if strcmpi(mots{1}, strIdent) && strcmpi(mots{2}, '=')
            nbOccurrences = nbOccurrences + 1;
        end
    end
end




%% Decodage de la valeur d'une propriete

function [linesOut, Value] = extract_variable(linesIn, strIdent, type)

Value = [];
iLine = 0;
for k1=1:length(linesIn)
    line = linesIn{k1};
    
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Verrue a supprimer des que UserInfo est resolu
    if strcmp(line(1:2), '##')
        line(1:2) = [];
    end
    
    iEgal = strfind(line, '=');
    if ~isempty(iEgal)
        iEgal = iEgal(1);
        x = double(line(iEgal+1));
        if ~((x == 9) || (x == 32))
            line = [line(1:iEgal) ' ' line(iEgal+1:end)];
        end
        x = double(line(iEgal-1));
        if ~((x == 9) || (x == 32))
            line = [line(1:iEgal-1) ' ' line(iEgal:end)];
        end
    end
    mots = strsplit(line);
%     if isempty(mots{1})
%         mots = mots(2:end);
%     end
    if length(mots) >= 3
        if strcmpi(mots{1}, strIdent) && strcmpi(mots{2}, '=')
            if  strcmpi(mots{3}, '{')
                k2 = 0;
                flag = 1;
                while flag
                    k2 = k2 + 1;
                    line = linesIn{k1+k2};
                    
                    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % Verrue a supprimer des que UserInfo est resolu
                    if strcmp(line(1:2), '##')
                        line(1:2) = [];
                    end
                    
                    mots = strsplit(line);
                    flag = ~strcmpi(mots{1}, '}');
                    if flag
                        Value(k2, :) = str2num(line); %#ok
                    end
                end
                iLine = k1:k1+k2;
                break
            else
                iLine = k1;
                Value = line(strfind(line, '=')+2:end);
                switch type
                    case 'char'
                        if strcmpi(Value(1), '"')
                            Value = Value(2:end);
                        end
                        if strcmpi(Value(end), '"')
                            Value = Value(1:end-1);
                        end
                    case 'double'
                        Value = str2num(Value); %#ok
                    otherwise
                end
                break
            end
        end
    end
end

if iLine == 0
    linesOut = linesIn;
    Value = [];
else
    sub = 1:length(linesIn);
    sub(iLine) = [];
    linesOut = linesIn(sub);
end



%%

function messageLignesNonDecodees(L)
str = cell2str([{'cl_ermapper_ers/decode : Lignes non decodees :'} L]);
str = [repmat(' ', size(str,1), 2) str];
my_warndlg(str, 0);
