function Ellipsoide = decode_ellipsoide(this)

nomFic = getNomFicDatabase('datum_sp.dat');

fid = fopen(nomFic);
lines = textscan(fid, '%s %f %f %f %f %f %f %f', 'Delimiter', ',', 'headerlines', 1);
fclose(fid);
name         = lines{1};
specified    = lines{2};
radius       = lines{3};
eccentricity = lines{4};
flattening   = lines{5};
metre_factor = lines{6};
prime_merid  = lines{7};
merid_value  = lines{8};

if strcmpi(this.CoordinateSpace.Datum, 'RAW')
    Ellipsoide = [];
else
    for k=1:length(name)
        name{k} = rmblank(name{k});
    end
    ind = find(strcmpi(this.CoordinateSpace.Datum, name));
    if isempty(ind)
        Ellipsoide = [];
        str = ['Ellipsoide not interpreted : ' this.CoordinateSpace.Datum];
        my_warndlg(['cl_ermapper_ers/decode : ', str], 0, 'Tag', 'EllipsoideNonInterpretee');
    end

    Ellipsoide.name         = name{ind};
    Ellipsoide.specified    = specified(ind);
    Ellipsoide.radius       = radius(ind);
    Ellipsoide.eccentricity = eccentricity(ind);
    Ellipsoide.flattening   = flattening(ind);
    Ellipsoide.metre_factor = metre_factor(ind);
    Ellipsoide.prime_merid  = prime_merid(ind);
    Ellipsoide.merid_value  = merid_value(ind);
end
