function [Type, Parametres] = decode_projection(this)

codeprojection = this.CoordinateSpace.Projection;

if contains(codeprojection, 'RAW')
    Type = 1;
    Parametres = [];
    return
end

if contains(codeprojection, 'GEODETIC')
    Type = 1;
    Parametres = [];
    return
end

Type = 1;

%% On teste si c'est de l'UTM

if contains(codeprojection, 'UTM')
    Type = 3;
    Parametres.UTM.Fuseau     = str2double(codeprojection(5:end));
    Parametres.UTM.Hemisphere = codeprojection(1);
    return
end


%% On teste si c'est de Mercator

if contains(codeprojection, 'MR')
    Trouve = 0;
    Type = 2;
    nomFic = getNomFicDatabase('mercator.dat');
    Lines = getLines(nomFic);
    for iLine = 1:length(Lines)
        mots = strsplit(Lines{iLine}, ',');
        if strcmp(codeprojection, rmblank(mots{1}))
            Parametres.Mercator.false_north    = str2double(mots{2});
            Parametres.Mercator.false_east     = str2double(mots{3});
            Parametres.Mercator.scale_factor   = str2double(mots{4});
            Parametres.Mercator.centre_merid   = str2double(mots{5});
            Parametres.Mercator.lat_ech_cons   = str2double(mots{6});
            Parametres.Mercator.radius         = str2double(mots{7});
            Parametres.Mercator.eccentricity   = str2double(mots{8});
            Parametres.Mercator.flattening     = str2double(mots{9});
            Parametres.Mercator.prime_merid    = str2double(mots{10});
            Trouve = 1;
            break
        end
    end
    if ~Trouve
        % En attendant de savoir a quoi s'en tenir !!!!!!!!!!!!!!!!!!!!!!!
        codeprojection = codeprojection(3:end);
        centre_lat = str2double(codeprojection(1:end-1));
        if strcmp(codeprojection(end), 'S')
            centre_lat = -centre_lat;
        end
        Parametres.Mercator.false_north    = 0;
        Parametres.Mercator.false_east     = 0;
        Parametres.Mercator.scale_factor   = 1;
        Parametres.Mercator.centre_merid   = 0;
        Parametres.Mercator.lat_ech_cons   = centre_lat;
        Parametres.Mercator.radius         = 0;
        Parametres.Mercator.eccentricity   = 0;
        Parametres.Mercator.flattening     = 0;
        Parametres.Mercator.prime_merid    = 0;



        %         Parametres.Mercator.lat_ech_cons = [];
        %         str = ['Parametres.Mercator non interpretee : ' codeprojection];
        %         my_warndlg(['cl_ermapper_ers/decode : ', str], 1);
    end
    return
end


%% On teste si c'est strereographique polaire

if contains(codeprojection, 'PS') || contains(codeprojection, 'OS')
    Trouve = 0;
    Type = 7;
    nomFic = getNomFicDatabase('stereo.dat');

    Lines = getLines(nomFic);
    for iLine = 1:length(Lines)
        mots = strsplit(Lines{iLine}, ',');
        if strcmp(codeprojection, rmblank(mots{1}))
            Parametres.Stereo.false_north    = str2double(mots{2});
            Parametres.Stereo.false_east     = str2double(mots{3});
            Parametres.Stereo.scale_factor   = str2double(mots{4});
            Parametres.Stereo.centre_merid   = str2double(mots{5});
            Parametres.Stereo.origin_lat     = str2double(mots{6});
            Parametres.Stereo.true_lat       = str2double(mots{7});
            Parametres.Stereo.radius         = str2double(mots{8});
            Parametres.Stereo.eccentricity   = str2double(mots{9});
            Parametres.Stereo.flattening     = str2double(mots{10});
            Parametres.Stereo.prime_merid    = str2double(mots{11});
            Trouve = 1;
            break
        end
    end
    if ~Trouve
        Parametres.Stereo = [];
        str = ['Parametres.Stereo non interpretee : ' codeprojection];
        my_warndlg(['cl_ermapper_ers/decode : ' str], 1);
    end
    return

end

%% on teste si c'est du Lambert

if contains(codeprojection, 'LM1')
    Type = 4;
    Trouve = 0;
    nomFic = getNomFicDatabase('lambert1.dat');
    Lines = getLines(nomFic);
    for iLine = 1:length(Lines)
        mots = strsplit(Lines{iLine}, ',');
        if strcmp(codeprojection, rmblank(mots{1}))
            Parametres.Lambert.false_north    = str2double(mots{2});
            Parametres.Lambert.false_east     = str2double(mots{3});
            Parametres.Lambert.scale_factor   = str2double(mots{4});
            Parametres.Lambert.centre_merid   = str2double(mots{5});
            Parametres.Lambert.centre_lat     = str2double(mots{6});
            Parametres.Lambert.radius         = str2double(mots{7});
            Parametres.Lambert.eccentricity   = str2double(mots{8});
            Parametres.Lambert.flattening     = str2double(mots{9});
            Parametres.Lambert.prime_merid    = str2double(mots{10});
            Trouve = 1;
            break
        end
    end
    if ~Trouve
        Parametres.Lambert = [];
        str = ['Parametres.Lambert non interpretee : ' codeprojection];
        my_warndlg(['cl_ermapper_ers/decode : ' str], 1);
    end
    return
end

%%on teste si c'est du lambert2

if contains(codeprojection, 'LM2')
    Type = 5;
    Trouve = 0;
    nomFic = getNomFicDatabase('lambert2.dat');
    Lines = getLines(nomFic);
    for iLine = 1:length(Lines)
        mots = strsplit(Lines{iLine}, ',');
        if strcmp(codeprojection, rmblank(mots{1}))
            Parametres.Lambert.false_north    = str2double(mots{2});
            Parametres.Lambert.false_east     = str2double(mots{3});
            Parametres.Lambert.false_olat     = str2double(mots{4});
            Parametres.Lambert.false_olon     = str2double(mots{5});
            Parametres.Lambert.scale_factor   = str2double(mots{6});
            Parametres.Lambert.centre_merid   = str2double(mots{7});
            Parametres.Lambert.first_paral    = str2double(mots{8});
            Parametres.Lambert.second_paral   = str2double(mots{9});
            Parametres.Lambert.centre_lat     = str2double(mots{10});
            Parametres.Lambert.defined        = str2double(mots{11});
            Parametres.Lambert.grid_north     = str2double(mots{12});
            Parametres.Lambert.false_onorth   = str2double(mots{13});
            Parametres.Lambert.false_oeast    = str2double(mots{14});
            Parametres.Lambert.radius         = str2double(mots{15});
            Parametres.Lambert.eccentricity   = str2double(mots{16});
            Parametres.Lambert.flattening     = str2double(mots{17});
            Parametres.Lambert.prime_merid    = str2double(mots{18});
            Trouve = 1;
            break
        end
    end
    if ~Trouve
        Parametres.Lambert = [];
        str = ['Parametres.Lambert non interpretee : ' codeprojection];
        my_warndlg(['cl_ermapper_ers/decode : ' str], 1);
    end
    return
end

%% on teste si c'est du Lamcon2

if contains(codeprojection, 'L2') || contains(codeprojection, 'LAM')
    Type = 6;
    Trouve = 0;
    nomFic = getNomFicDatabase('lamcon2.dat');
    Lines = getLines(nomFic);
    for iLine = 1:length(Lines)
        mots = strsplit(Lines{iLine}, ',');
        if strcmp(codeprojection, rmblank(mots{1}))
            Parametres.Lamcon2.false_north    = str2double(mots{2});
            Parametres.Lamcon2.false_east     = str2double(mots{3});
            Parametres.Lamcon2.false_olat     = str2double(mots{4});
            Parametres.Lamcon2.false_olon     = str2double(mots{5});
            Parametres.Lamcon2.scale_factor   = str2double(mots{6});
            Parametres.Lamcon2.centre_merid   = str2double(mots{7});
            Parametres.Lamcon2.first_paral    = str2double(mots{8});
            Parametres.Lamcon2.second_paral   = str2double(mots{9});
            Parametres.Lamcon2.centre_lat     = str2double(mots{10});
            Parametres.Lamcon2.defined        = str2double(mots{11});
            Parametres.Lamcon2.grid_north     = str2double(mots{12});
            Parametres.Lamcon2.false_onorth   = str2double(mots{13});
            Parametres.Lamcon2.false_oeast    = str2double(mots{14});
            Parametres.Lamcon2.radius         = str2double(mots{15});
            Parametres.Lamcon2.eccentricity   = str2double(mots{16});
            Parametres.Lamcon2.flattening     = str2double(mots{17});
            Parametres.Lamcon2.prime_merid    = str2double(mots{18});
            Trouve = 1;
            break
        end
    end
    if ~Trouve
        Parametres.Lamcon2 = [];
        str = ['Parametres.Lamcon2 non interpretee : ' codeprojection];
        my_warndlg(['cl_ermapper_ers/decode : ' str], 1);
    end
    return
end


if Type == 1
    str = ['Projection non interpretee : ' codeProjection];
    my_warndlg(['cl_ermapper_ers/decode : ' str], 1);
end
