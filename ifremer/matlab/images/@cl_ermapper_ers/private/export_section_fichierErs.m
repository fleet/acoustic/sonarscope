function export_section_fichierErs(fidAlg, tree, Level)

Marge = repmat(' ', 1, Level*4);
names = fieldnames(tree);
for i=1:length(names)
    var = tree.(names{i});
    if isstruct(var)
        for k=1:length(var)
            fprintf(fidAlg, '%s%s Begin\n', Marge, names{i});
            export_section_fichierErs(fidAlg, var(k), Level+1)
            fprintf(fidAlg, '%s%s End\n', Marge, names{i});
        end
    else
        switch class(var)
            case 'char'
                if strcmp(var(1), '[') && strcmp(var(end), ']')
                    var = sprintf('{\n%s\n}', var(2:end-1));
                end
                fprintf(fidAlg, '%s%s = %s\n', Marge, names{i}, var);
            case {'double'; 'single'}
                if length(var) == 1
                    var = num2strPrecis(var);
                    fprintf(fidAlg, '%s%s = %s\n', Marge, names{i}, var);
                else
                    fprintf(fidAlg, '%s%s = {\n', Marge, names{i});
                    for k=1:size(var,1)
                        fprintf(fidAlg, '%s%s\n', Marge,  num2str(var(k,:)));
                    end
                    fprintf(fidAlg, '%s}\n', Marge);
                end
            otherwise
                str = sprintf('%s not yest processed in export_section_fichierErs', class(var));
                my_warndlg(str, 1);
        end
    end
end
