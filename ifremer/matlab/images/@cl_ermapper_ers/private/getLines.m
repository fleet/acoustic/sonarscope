function Lines = getLines(nomFic)
fid = fopen(nomFic);
entete = fgets(fid); %#ok
Lines = [];
while feof(fid) == 0
    L = fgetl(fid);
    if length(L) >= 1
        Lines{end+1} = L; %#ok<AGROW>
    end
end
fclose(fid);
