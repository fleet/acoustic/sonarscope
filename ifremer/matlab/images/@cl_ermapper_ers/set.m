% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Name-Value Pair Arguments
%   cf. cl_image
%
% Output Arguments
%   this : instance initialisee
%
% Examples
%   a = cl_ermapper_ers
%   set(a, 'FicIn', '/home2/brekh/dataErMapper/DATASET/')
%   a
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

%% Traitement des messages de la classe

[varargin, this.Version] = getPropertyValue(varargin, 'Version', this.Version);
[varargin, this.Name] = getPropertyValue(varargin, 'Name', this.Name);
[varargin, this.Description] = getPropertyValue(varargin, 'Description', this.Description);
[varargin, this.SourceDataset] = getPropertyValue(varargin, 'SourceDataset', this.SourceDataset);
[varargin, this.LastUpdated] = getPropertyValue(varargin, 'LastUpdated', this.LastUpdated);
[varargin, this.SensorName] = getPropertyValue(varargin, 'SensorName', this.SensorName);
[varargin, this.SenseDate] = getPropertyValue(varargin, 'SenseDate', this.SenseDate);
[varargin, this.DataFile] = getPropertyValue(varargin, 'DataFile', this.DataFile);
[varargin, this.DataSetType] = getPropertyValue(varargin, 'DataSetType', this.DataSetType);
[varargin, this.DataType] = getPropertyValue(varargin, 'DataType', this.DataType);
[varargin, this.ByteOrder] = getPropertyValue(varargin, 'ByteOrder', this.ByteOrder);
[varargin, this.HeaderOffset] = getPropertyValue(varargin, 'HeaderOffset', this.HeaderOffset);
[varargin, this.Comments] = getPropertyValue(varargin, 'Comments', this.Comments);

[varargin, this.CoordinateSpace] = getPropertyValue(varargin, 'CoordinateSpace', this.CoordinateSpace);
[varargin, this.CoordinateSpace.Datum] = getPropertyValue(varargin, 'Datum', this.CoordinateSpace.Datum);
[varargin, this.CoordinateSpace.Projection] = getPropertyValue(varargin, 'Projection', this.CoordinateSpace.Projection);
[varargin, this.CoordinateSpace.CoordinateType] = getPropertyValue(varargin, 'CoordinateType', this.CoordinateSpace.CoordinateType);
[varargin, this.CoordinateSpace.Units] = getPropertyValue(varargin, 'Units', this.CoordinateSpace.Units);
[varargin, this.CoordinateSpace.Rotation] = getPropertyValue(varargin, 'Rotation', this.CoordinateSpace.Rotation);

[varargin, this.RasterInfo] = getPropertyValue(varargin, 'RasterInfo', this.RasterInfo);
[varargin, this.RasterInfo.CellType] = getPropertyValue(varargin, 'CellType', this.RasterInfo.CellType);
[varargin, this.RasterInfo.NullCellValue] = getPropertyValue(varargin, 'NullCellValue', this.RasterInfo.NullCellValue);
[varargin, this.RasterInfo.NrOfLines] = getPropertyValue(varargin, 'NrOfLines', this.RasterInfo.NrOfLines);
[varargin, this.RasterInfo.NrOfCellsPerLine] = getPropertyValue(varargin, 'NrOfCellsPerLine', this.RasterInfo.NrOfCellsPerLine);
[varargin, this.RasterInfo.NrOfBands] = getPropertyValue(varargin, 'NrOfBands', this.RasterInfo.NrOfBands);
[varargin, this.RasterInfo.RegistrationCellX] = getPropertyValue(varargin, 'RegistrationCellX', this.RasterInfo.RegistrationCellX);
[varargin, this.RasterInfo.RegistrationCellY] = getPropertyValue(varargin, 'RegistrationCellY', this.RasterInfo.RegistrationCellY);

[varargin, this.RasterInfo.CellInfo.Xdimension] = getPropertyValue(varargin, 'Xdimension', this.RasterInfo.CellInfo.Xdimension);
[varargin, this.RasterInfo.CellInfo.Ydimension] = getPropertyValue(varargin, 'Ydimension', this.RasterInfo.CellInfo.Ydimension);

[varargin, this.RasterInfo.RegistrationCoord.Eastings] = getPropertyValue(varargin, 'Eastings', this.RasterInfo.RegistrationCoord.Eastings);
[varargin, this.RasterInfo.RegistrationCoord.Northings] = getPropertyValue(varargin, 'Northings', this.RasterInfo.RegistrationCoord.Northings);
[varargin, this.RasterInfo.RegistrationCoord.MetersX] = getPropertyValue(varargin, 'MetersX', this.RasterInfo.RegistrationCoord.MetersX);
[varargin, this.RasterInfo.RegistrationCoord.MetersY] = getPropertyValue(varargin, 'MetersY', this.RasterInfo.RegistrationCoord.MetersY);
[varargin, this.RasterInfo.RegistrationCoord.MetersZ] = getPropertyValue(varargin, 'MetersZ', this.RasterInfo.RegistrationCoord.MetersZ);

[varargin, this.UserInfo.ImageType] = getPropertyValue(varargin, 'UserInfo.ImageType', this.UserInfo.ImageType);
[varargin, this.UserInfo.DataType] = getPropertyValue(varargin, 'UserInfo.DataType', this.UserInfo.DataType);
[varargin, this.UserInfo.GeometryType] = getPropertyValue(varargin, 'UserInfo.GeometryType', this.UserInfo.GeometryType);
[varargin, this.UserInfo.XUnit] = getPropertyValue(varargin, 'UserInfo.XUnit', this.UserInfo.XUnit);
[varargin, this.UserInfo.YUnit] = getPropertyValue(varargin, 'UserInfo.YUnit', this.UserInfo.YUnit);
[varargin, this.UserInfo.XLabel] = getPropertyValue(varargin, 'UserInfo.XLabel', this.UserInfo.XLabel);
[varargin, this.UserInfo.YLabel] = getPropertyValue(varargin, 'UserInfo.YLabel', this.UserInfo.YLabel);
[varargin, this.UserInfo.SpectralStatus] = getPropertyValue(varargin, 'UserInfo.SpectralStatus', this.UserInfo.SpectralStatus);
[varargin, this.UserInfo.YDir] = getPropertyValue(varargin, 'UserInfo.YDir', this.UserInfo.YDir);
[varargin, this.UserInfo.XDir] = getPropertyValue(varargin, 'UserInfo.XDir', this.UserInfo.XDir);
[varargin, this.UserInfo.ColormapIndex] = getPropertyValue(varargin, 'UserInfo.ColormapIndex', this.UserInfo.ColormapIndex);
[varargin, this.UserInfo.Colormap] = getPropertyValue(varargin, 'UserInfo.Colormap', this.UserInfo.Colormap);
[varargin, this.UserInfo.ColormapCustom] = getPropertyValue(varargin, 'UserInfo.ColormapCustom', this.UserInfo.ColormapCustom);
[varargin, this.UserInfo.CLim] = getPropertyValue(varargin, 'UserInfo.CLim', this.UserInfo.CLim);
[varargin, this.UserInfo.Video] = getPropertyValue(varargin, 'UserInfo.Video', this.UserInfo.Video);
[varargin, this.UserInfo.VertExagAuto] = getPropertyValue(varargin, 'UserInfo.VertExagAuto', this.UserInfo.VertExagAuto);
[varargin, this.UserInfo.VertExag] = getPropertyValue(varargin, 'UserInfo.VertExag', this.UserInfo.VertExag);
[varargin, this.UserInfo.Azimuth] = getPropertyValue(varargin, 'UserInfo.Azimuth', this.UserInfo.Azimuth);
[varargin, this.UserInfo.ContourValues] = getPropertyValue(varargin, 'UserInfo.ContourValues', this.UserInfo.ContourValues);
[varargin, this.UserInfo.NuIdentParent] = getPropertyValue(varargin, 'UserInfo.NuIdentParent', this.UserInfo.NuIdentParent);
[varargin, this.UserInfo.TagSynchroX] = getPropertyValue(varargin, 'UserInfo.TagSynchroX', this.UserInfo.TagSynchroX);
[varargin, this.UserInfo.TagSynchroY] = getPropertyValue(varargin, 'UserInfo.TagSynchroY', this.UserInfo.TagSynchroY);
[varargin, this.UserInfo.TagSynchroContrast] = getPropertyValue(varargin, 'UserInfo.TagSynchroContrast', this.UserInfo.TagSynchroContrast);
[varargin, this.UserInfo.InitialFileName] = getPropertyValue(varargin, 'UserInfo.InitialFileName', this.UserInfo.InitialFileName);
[varargin, this.UserInfo.HistoCentralClasses] = getPropertyValue(varargin, 'UserInfo.HistoCentralClasses', this.UserInfo.HistoCentralClasses);
[varargin, this.UserInfo.HistoValues] = getPropertyValue(varargin, 'UserInfo.HistoValues', this.UserInfo.HistoValues);

[varargin, this.UserInfo.StatValues] = getPropertyValue(varargin, 'UserInfo.StatValues', this.UserInfo.StatValues);
[varargin, this.UserInfo.StatSummary] = getPropertyValue(varargin, 'UserInfo.StatSummary', this.UserInfo.StatSummary);

[varargin, this.UserInfo.Sonar.Ident] = getPropertyValue(varargin, 'UserInfo.Sonar.Ident', this.UserInfo.Sonar.Ident);
[varargin, this.UserInfo.Sonar.Family] = getPropertyValue(varargin, 'UserInfo.Sonar.Family', this.UserInfo.Sonar.Family);
[varargin, this.UserInfo.Sonar.RawDataResol] = getPropertyValue(varargin, 'UserInfo.Sonar.RawDataResol', this.UserInfo.Sonar.RawDataResol);
[varargin, this.UserInfo.Sonar.ResolutionD] = getPropertyValue(varargin, 'UserInfo.Sonar.ResolutionD', this.UserInfo.Sonar.ResolutionD);
[varargin, this.UserInfo.Sonar.ResolutionX] = getPropertyValue(varargin, 'UserInfo.Sonar.ResolutionX', this.UserInfo.Sonar.ResolutionX);
[varargin, this.UserInfo.Sonar.TVG.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.etat', this.UserInfo.Sonar.TVG.etat);
[varargin, this.UserInfo.Sonar.TVG.origine] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.origine', this.UserInfo.Sonar.TVG.origine);
[varargin, this.UserInfo.Sonar.TVG.ConstructTypeCompens] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.ConstructTypeCompens', this.UserInfo.Sonar.TVG.ConstructTypeCompens);
[varargin, this.UserInfo.Sonar.TVG.ConstructAlpha] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.ConstructAlpha', this.UserInfo.Sonar.TVG.ConstructAlpha);
[varargin, this.UserInfo.Sonar.TVG.ConstructTable] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.ConstructTable', this.UserInfo.Sonar.TVG.ConstructTable);
[varargin, this.UserInfo.Sonar.TVG.IfremerAlpha] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.IfremerAlpha', this.UserInfo.Sonar.TVG.IfremerAlpha);

[varargin, this.UserInfo.Sonar.NE.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.NE.etat', this.UserInfo.Sonar.NE.etat);
[varargin, this.UserInfo.Sonar.GT.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.GT.etat', this.UserInfo.Sonar.GT.etat);
[varargin, this.UserInfo.Sonar.SH.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.SH.etat', this.UserInfo.Sonar.SH.etat);
[varargin, this.UserInfo.Sonar.TVG.ConstructCoefDiverg] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.ConstructCoefDiverg', this.UserInfo.Sonar.TVG.ConstructCoefDiverg);
[varargin, this.UserInfo.Sonar.TVG.ConstructConstante] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.ConstructConstante', this.UserInfo.Sonar.TVG.ConstructConstante);
[varargin, this.UserInfo.Sonar.TVG.IfremerCoefDiverg] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.IfremerCoefDiverg', this.UserInfo.Sonar.TVG.IfremerCoefDiverg);
[varargin, this.UserInfo.Sonar.TVG.IfremerConstante] = getPropertyValue(varargin, 'UserInfo.Sonar.TVG.IfremerConstante', this.UserInfo.Sonar.TVG.IfremerConstante);
[varargin, this.UserInfo.Sonar.DiagEmi.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagEmi.etat', this.UserInfo.Sonar.DiagEmi.etat);
[varargin, this.UserInfo.Sonar.DiagEmi.origine] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagEmi.origine', this.UserInfo.Sonar.DiagEmi.origine);
[varargin, this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagEmi.ConstructTypeCompens', this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens);
[varargin, this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagEmi.IfremerTypeCompens', this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens);
[varargin, this.UserInfo.Sonar.DiagRec.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagRec.etat', this.UserInfo.Sonar.DiagRec.etat);
[varargin, this.UserInfo.Sonar.DiagRec.origine] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagRec.origine', this.UserInfo.Sonar.DiagRec.origine);
[varargin, this.UserInfo.Sonar.DiagRec.ConstructTypeCompens] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagRec.ConstructTypeCompens', this.UserInfo.Sonar.DiagRec.ConstructTypeCompens);
[varargin, this.UserInfo.Sonar.DiagRec.IfremerTypeCompens] = getPropertyValue(varargin, 'UserInfo.Sonar.DiagRec.IfremerTypeCompens', this.UserInfo.Sonar.DiagRec.IfremerTypeCompens);
[varargin, this.UserInfo.Sonar.AireInso.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.AireInso.etat', this.UserInfo.Sonar.AireInso.etat);
[varargin, this.UserInfo.Sonar.AireInso.origine] = getPropertyValue(varargin, 'UserInfo.Sonar.AireInso.origine', this.UserInfo.Sonar.AireInso.origine);
[varargin, this.UserInfo.Sonar.BS.etat] = getPropertyValue(varargin, 'UserInfo.Sonar.BS.etat', this.UserInfo.Sonar.BS.etat);
[varargin, this.UserInfo.Sonar.BS.origine] = getPropertyValue(varargin, 'UserInfo.Sonar.BS.origine', this.UserInfo.Sonar.BS.origine);
[varargin, this.UserInfo.Sonar.BS.origineBelleImage] = getPropertyValue(varargin, 'UserInfo.Sonar.BS.origineBelleImage', this.UserInfo.Sonar.BS.origineBelleImage);
[varargin, this.UserInfo.Sonar.BS.origineFullCompens] = getPropertyValue(varargin, 'UserInfo.Sonar.BS.origineFullCompens', this.UserInfo.Sonar.BS.origineFullCompens);
[varargin, this.UserInfo.Sonar.BS.IfremerCompensTable] = getPropertyValue(varargin, 'UserInfo.Sonar.BS.IfremerCompensTable', this.UserInfo.Sonar.BS.IfremerCompensTable);

[varargin, X] = getPropertyValue(varargin, 'BandId_Value', []);
this = set_BandId_Value(this, X);

[varargin, X] = getPropertyValue(varargin, 'BandId_Units', []);
this = set_BandId_Units(this, X);

[varargin, X] = getPropertyValue(varargin, 'BandId_Width', []);
this = set_BandId_Width(this, X);

[varargin, X] = getPropertyValue(varargin, 'BandId_Comment', []);
this = set_BandId_Comment(this, X);

[varargin, X] = getPropertyValue(varargin, 'RegionInfo', []);
if ~isempty(X)
    this.RasterInfo.RegionInfo = X;
end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Type', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Type = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.RegionName', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.RegionName = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.RGBcolour.Red', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.RGBcolour.Red = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.RGBcolour.Green', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.RGBcolour.Green = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.RGBcolour.Blue', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.RGBcolour.Blue = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.ClassNumber', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.ClassNumber = X;
%     end
%
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.SubRegion', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.SubRegion = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Stats.NumberOfBands', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Stats.NumberOfBands = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Stats.NumberOfNonNullCells', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Stats.NumberOfNonNullCells = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Stats.MinimumValue', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Stats.MinimumValue = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Stats.MaximumValue', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Stats.MaximumValue = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Stats.MeanValue', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Stats.MeanValue = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Stats.MedianValue', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Stats.MedianValue = X;
%     end
% [varargin, X] = getPropertyValue(varargin, 'RasterInfo.RegionInfo.Stats.CovarianceMatrix', []);
%     if ~isempty(X)
%         this.RasterInfo.RegionInfo.Stats.CovarianceMatrix = X;
%     end


[varargin, FicIn] = getPropertyValue(varargin, 'FicIn', []); %#ok<ASGLU>

if ~isempty(FicIn)
    this.FicIn = FicIn;
    this = decode(this);
    
    UserInfo = get(this, 'UserInfo');
    if isempty(UserInfo.DataType)
        that = this;
        [nomDir, nomFic] = fileparts(FicIn);
        that.FicIn = fullfile(nomDir, nomFic, [nomFic '_UserInfo.txt']);
        that = decode(that, 'UserInfo');
        UserInfo = get(that, 'UserInfo');
        this.UserInfo = UserInfo;
    end
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
