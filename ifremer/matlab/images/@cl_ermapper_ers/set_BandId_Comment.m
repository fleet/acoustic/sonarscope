function this = set_BandId_Comment(this, X)
if ~isempty(X)
    if (length(X) == 1) && (this.RasterInfo.NrOfBands == 3)
        X{1} = 'Red';
        X{2} = 'Grenn';
        X{3} = 'Blue';
    end
    for i=1:this.RasterInfo.NrOfBands
        this.RasterInfo.BandId(i).Comment = X{i};
    end
end
