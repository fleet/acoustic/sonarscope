function this = set_BandId_Units(this, X)
if ~isempty(X)
    if (length(X) == 1) && (this.RasterInfo.NrOfBands == 3)
        X{1} = 'Red';
        X{2} = 'Green';
        X{3} = 'Blue';
    end
    for i=1:this.RasterInfo.NrOfBands
        this.RasterInfo.BandId(i).Units = X{i};
    end
end
