function this = set_BandId_Value(this, X)
if ~isempty(X)
    for k=1:this.RasterInfo.NrOfBands
        this.RasterInfo.BandId(k).Value = X{k};
    end
end