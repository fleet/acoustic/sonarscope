function this = set_BandId_Width(this, X)
if ~isempty(X)
    if (length(X) == 1) && (this.RasterInfo.NrOfBands == 3)
        X{2} = X{1};
        X{3} = X{1};
    end
    for i=1:this.RasterInfo.NrOfBands
        this.RasterInfo.BandId(i).Width = X{i};
    end
end
