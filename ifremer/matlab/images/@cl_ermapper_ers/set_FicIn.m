% Acces en ecriture des proprietes d'une instance
% 
% Syntax
%   this = set_FicIn(this, ...)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Name-Value Pair Arguments
%   cf. cl_image
%
% Output Arguments
%   this : instance initialisee
%
% Examples
%   a = cl_ermapper_ers
%   a = set_FicIn(a,'/home2/brekh/dataErMapper/DATASET/')
%   a
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_FicIn(this, FicIn)

this.FicIn = FicIn;
