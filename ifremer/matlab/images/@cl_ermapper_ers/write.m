% Ecriture d'une instance cl_ermapper_ers sur disque
% 
% Syntax
%   status = write(this)
%
% Input Arguments
%   this : instance de cl_ermapper_ers
%
% Output Arguments
%   status : 1 =tout s'est bien passe, 0=ca c'est pas bien passe
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function status = write(this)

status = 0;
[nomDir, nomFic] = fileparts(this.FicIn);
nomFic = [fullfile(nomDir, nomFic) '.ers'];

fid = fopen(nomFic, 'w+');
if fid == -1
    str = sprintf('Erreur d''ecriture fichier %s', nomFic);
    my_warndlg(['cl_ermapper_ers/write : ', str], 1);
    return
end

write_Begin(fid, 0, 'DatasetHeader')
write_val(fid, 1, 'Version        ', this.Version, 'Entecote')
write_val(fid, 1, 'Name           ', this.Name, 'Entecote')
write_val(fid, 1, 'Description    ', this.Description, 'Entecote')
% write_val(fid, 1, 'SourceDataset  ', this.SourceDataset, 'Entecote')
write_val(fid, 1, 'LastUpdated    ', this.LastUpdated); % BUG lors de la lecture dans Ermapper Fri Jul 09 08:17:58 GMT 2004
write_val(fid, 1, 'SensorName     ', this.SensorName, 'Entecote')
%       write_val(fid, 1, 'SenseDate      ', this.SenseDate, 'Entecote');  % pas connu ??????????????????????????
write_val(fid, 1, 'DataFile       ', this.DataFile, 'Entecote')
write_val(fid, 1, 'DataSetType    ', this.DataSetType)
write_val(fid, 1, 'DataType       ', this.DataType)
write_val(fid, 1, 'ByteOrder      ', this.ByteOrder)
write_val(fid, 1, 'HeaderOffset   ', this.HeaderOffset)
write_val(fid, 1, 'Comments       ', this.Comments, 'Entecote')

write_Begin(fid, 1, 'CoordinateSpace')
write_val(fid, 2, 'Datum         ', this.CoordinateSpace.Datum, 'Entecote')
write_val(fid, 2, 'Projection    ', this.CoordinateSpace.Projection, 'Entecote')
write_val(fid, 2, 'CoordinateType', this.CoordinateSpace.CoordinateType) % LL
%             if ~isempty(this.CoordinateSpace.Datum)
if ~isempty(this.CoordinateSpace.Units)
    write_val(fid, 2, 'Units         ', this.CoordinateSpace.Units, 'Entecote')
end

x = this.CoordinateSpace.Rotation;
if isempty(x)
    Degre   = 0;
    Minute  = 0;
    Seconde = 0;
else
    Degre = floor(x);
    x = (x - Degre) * 60;
    Minute = floor(x);
    x = (x - Minute) * 60;
    Seconde = x;
end
strAzimut = sprintf('%d:%02d:%f', Degre, Minute, Seconde);
write_val(fid, 2, 'Rotation      ', strAzimut)
%             end
write_End(fid, 1, 'CoordinateSpace')

write_Begin(fid, 1, 'RasterInfo')
write_val(fid, 2, 'CellType         ', this.RasterInfo.CellType)
if isnan(this.RasterInfo.NullCellValue)
    write_val(fid, 2, 'NullCellValue    ', '340282346638528859811704183484516925440')
else
    write_val(fid, 2, 'NullCellValue    ', this.RasterInfo.NullCellValue)
end
write_val(fid, 2, 'NrOfLines        ', this.RasterInfo.NrOfLines)
write_val(fid, 2, 'NrOfCellsPerLine ', this.RasterInfo.NrOfCellsPerLine)
write_val(fid, 2, 'NrOfBands        ', this.RasterInfo.NrOfBands)

write_Begin(fid, 2, 'CellInfo')
write_val(fid, 3, 'Xdimension         ', this.RasterInfo.CellInfo.Xdimension)
write_val(fid, 3, 'Ydimension         ', this.RasterInfo.CellInfo.Ydimension)
write_End(fid, 2, 'CellInfo')
write_val(fid, 2, 'RegistrationCellX', this.RasterInfo.RegistrationCellX)
write_val(fid, 2, 'RegistrationCellY', this.RasterInfo.RegistrationCellY)

% if (this.RasterInfo.RegistrationCoord.Eastings ~= 0) && ...
%         (this.RasterInfo.RegistrationCoord.Northings ~= 0) && ...
%         ~strcmp(this.CoordinateSpace.Datum, 'RAW')
%     write_Begin(fid, 2, 'RegistrationCoord')
%     write_val(fid, 3, 'Eastings         ', this.RasterInfo.RegistrationCoord.Eastings)
%     write_val(fid, 3, 'Northings        ', this.RasterInfo.RegistrationCoord.Northings)
%     write_End(fid, 2, 'RegistrationCoord')
% end


if (this.RasterInfo.RegistrationCoord.Eastings ~= 0) && ...
        (this.RasterInfo.RegistrationCoord.Northings ~= 0)
    if strcmp(this.CoordinateSpace.Datum, 'RAW')
        write_Begin(fid, 2, 'RegistrationCoord')
        write_val(fid, 3, 'MetersX         ', this.RasterInfo.RegistrationCoord.Eastings)
        write_val(fid, 3, 'MetersY        ', this.RasterInfo.RegistrationCoord.Northings)
        write_End(fid, 2, 'RegistrationCoord')
    else
        write_Begin(fid, 2, 'RegistrationCoord')
        write_val(fid, 3, 'Eastings         ', this.RasterInfo.RegistrationCoord.Eastings)
        write_val(fid, 3, 'Northings        ', this.RasterInfo.RegistrationCoord.Northings)
        write_End(fid, 2, 'RegistrationCoord')
    end
end



for i=1:this.RasterInfo.NrOfBands
    write_Begin(fid, 2, 'BandId')
    write_val(fid, 3, 'Value         ', this.RasterInfo.BandId(i).Value, 'Entecote')
    write_val(fid, 3, 'Units         ', this.RasterInfo.BandId(i).Units, 'Entecote')
    write_val(fid, 3, 'Comment       ', this.RasterInfo.BandId(i).Comment, 'Entecote')
    write_End(fid, 2, 'BandId')
end
write_End(fid, 1, 'RasterInfo')

write_Begin(fid, 1, 'UserInfo', 'Comment')
write_val(fid, 2, 'ImageType      ', this.UserInfo.ImageType, 'Comment')
write_val(fid, 2, 'DataType       ', this.UserInfo.DataType, 'Comment')
write_val(fid, 2, 'GeometryType   ', this.UserInfo.GeometryType, 'Comment')
write_val(fid, 2, 'SpectralStatus ', this.UserInfo.SpectralStatus, 'Comment')
write_val(fid, 2, 'XUnit          ', this.UserInfo.XUnit, 'Comment')
write_val(fid, 2, 'YUnit          ', this.UserInfo.YUnit, 'Comment')
write_val(fid, 2, 'XLabel         ', this.UserInfo.XLabel, 'Comment')
write_val(fid, 2, 'YLabel         ', this.UserInfo.YLabel, 'Comment')
write_val(fid, 2, 'YDir           ', this.UserInfo.YDir, 'Comment')
write_val(fid, 2, 'XDir           ', this.UserInfo.XDir, 'Comment')
write_val(fid, 2, 'ColormapIndex  ', this.UserInfo.ColormapIndex, 'Comment')
write_val(fid, 2, 'CLim           ', this.UserInfo.CLim, 'Comment')
write_val(fid, 2, 'Video          ', this.UserInfo.Video, 'Comment')
write_val(fid, 2, 'VertExagAuto   ', this.UserInfo.VertExagAuto, 'Comment')
write_val(fid, 2, 'VertExag       ', this.UserInfo.VertExag, 'Comment')
write_val(fid, 2, 'Azimuth        ', this.UserInfo.Azimuth, 'Comment')
write_val(fid, 2, 'ContourValues  ', this.UserInfo.ContourValues, 'Comment')
write_val(fid, 2, 'NuIdentParent  ', this.UserInfo.NuIdentParent, 'Comment')
write_val(fid, 2, 'TagSynchroX    ', this.UserInfo.TagSynchroX, 'Comment')
write_val(fid, 2, 'TagSynchroY    ', this.UserInfo.TagSynchroY, 'Comment')
write_val(fid, 2, 'TagSynchroContrast ', this.UserInfo.TagSynchroContrast, 'Comment')
write_val(fid, 2, 'InitialFileName', this.UserInfo.InitialFileName, 'Comment')

write_val(fid, 2, 'HistoCentralClasses  ', this.UserInfo.HistoCentralClasses, 'Comment')
write_val(fid, 2, 'HistoValues          ', this.UserInfo.HistoValues, 'Comment')
if ~isempty(this.UserInfo.StatValues)
    write_val(fid, 2, 'StatValues.Moyenne     ', this.UserInfo.StatValues.Moyenne, 'Comment')
    write_val(fid, 2, 'StatValues.Variance    ', this.UserInfo.StatValues.Variance, 'Comment')
    write_val(fid, 2, 'StatValues.Sigma       ', this.UserInfo.StatValues.Sigma, 'Comment')
    write_val(fid, 2, 'StatValues.Mediane     ', this.UserInfo.StatValues.Mediane, 'Comment')
    write_val(fid, 2, 'StatValues.Min         ', this.UserInfo.StatValues.Min, 'Comment')
    write_val(fid, 2, 'StatValues.Max         ', this.UserInfo.StatValues.Max, 'Comment')
    write_val(fid, 2, 'StatValues.Range       ', this.UserInfo.StatValues.Range, 'Comment')
    write_val(fid, 2, 'StatValues.Quant_03_97 ', this.UserInfo.StatValues.Quant_03_97, 'Comment')
    write_val(fid, 2, 'StatValues.Quant_01_99 ', this.UserInfo.StatValues.Quant_01_99, 'Comment')
    write_val(fid, 2, 'StatValues.Quant_25_75 ', this.UserInfo.StatValues.Quant_25_75, 'Comment')
    write_val(fid, 2, 'StatValues.Sampling    ', this.UserInfo.StatValues.Sampling, 'Comment')
    write_val(fid, 2, 'StatValues.PercentNaN  ', this.UserInfo.StatValues.PercentNaN, 'Comment')
    write_val(fid, 2, 'StatValues.Skewness    ', this.UserInfo.StatValues.Skewness, 'Comment')
    write_val(fid, 2, 'StatValues.Kurtosis    ', this.UserInfo.StatValues.Kurtosis, 'Comment')
    write_val(fid, 2, 'StatValues.Entropy     ', this.UserInfo.StatValues.Entropy, 'Comment')
end

if ~isempty(this.UserInfo.Sonar.Family)
    write_val(fid, 2, 'Sonar.Ident              ', this.UserInfo.Sonar.Ident, 'Comment')
    write_val(fid, 2, 'Sonar.Family             ', this.UserInfo.Sonar.Family, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.RawDataResol)
    write_val(fid, 2, 'Sonar.RawDataResol       ', this.UserInfo.Sonar.RawDataResol, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.ResolutionD)
    write_val(fid, 2, 'Sonar.ResolutionD        ', this.UserInfo.Sonar.ResolutionD, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.ResolutionX)
    write_val(fid, 2, 'Sonar.ResolutionX        ', this.UserInfo.Sonar.ResolutionX, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.etat)
    write_val(fid, 2, 'Sonar.TVG.etat                ', this.UserInfo.Sonar.TVG.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.origine)
    write_val(fid, 2, 'Sonar.TVG.origine             ', this.UserInfo.Sonar.TVG.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructTypeCompens)
    write_val(fid, 2, 'Sonar.TVG.ConstructTypeCompens', this.UserInfo.Sonar.TVG.ConstructTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructAlpha)
    write_val(fid, 2, 'Sonar.TVG.ConstructAlpha      ', this.UserInfo.Sonar.TVG.ConstructAlpha, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructCoefDiverg)
    write_val(fid, 2, 'Sonar.TVG.ConstructCoefDiverg        ', this.UserInfo.Sonar.TVG.ConstructCoefDiverg, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructConstante)
    write_val(fid, 2, 'Sonar.TVG.ConstructConstante      ', this.UserInfo.Sonar.TVG.ConstructConstante, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructTable)
    write_val(fid, 2, 'Sonar.TVG.ConstructTable      ', this.UserInfo.Sonar.TVG.ConstructTable, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.IfremerAlpha)
    write_val(fid, 2, 'Sonar.TVG.IfremerAlpha        ', this.UserInfo.Sonar.TVG.IfremerAlpha, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.IfremerCoefDiverg)
    write_val(fid, 2, 'Sonar.TVG.IfremerCoefDiverg        ', this.UserInfo.Sonar.TVG.IfremerCoefDiverg, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.IfremerConstante)
    write_val(fid, 2, 'Sonar.TVG.IfremerConstante        ', this.UserInfo.Sonar.TVG.IfremerConstante, 'Comment')
end


if ~isempty(this.UserInfo.Sonar.NE.etat)
    write_val(fid, 2, 'Sonar.NE.etat        ', this.UserInfo.Sonar.NE.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.GT.etat)
    write_val(fid, 2, 'Sonar.GT.etat        ', this.UserInfo.Sonar.GT.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.SH.etat)
    write_val(fid, 2, 'Sonar.SH.etat        ', this.UserInfo.Sonar.SH.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.etat)
    write_val(fid, 2, 'Sonar.DiagEmi.etat        ', this.UserInfo.Sonar.DiagEmi.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.origine)
    write_val(fid, 2, 'Sonar.DiagEmi.origine        ', this.UserInfo.Sonar.DiagEmi.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens)
    write_val(fid, 2, 'Sonar.DiagEmi.ConstructTypeCompens        ', this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens)
    write_val(fid, 2, 'Sonar.DiagEmi.IfremerTypeCompens        ', this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.etat)
    write_val(fid, 2, 'Sonar.DiagRec.etat        ', this.UserInfo.Sonar.DiagRec.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.origine)
    write_val(fid, 2, 'Sonar.DiagRec.origine        ', this.UserInfo.Sonar.DiagRec.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.ConstructTypeCompens)
    write_val(fid, 2, 'Sonar.DiagRec.ConstructTypeCompens        ', this.UserInfo.Sonar.DiagRec.ConstructTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.IfremerTypeCompens)
    write_val(fid, 2, 'Sonar.DiagRec.IfremerTypeCompens        ', this.UserInfo.Sonar.DiagRec.IfremerTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.AireInso.etat)
    write_val(fid, 2, 'Sonar.AireInso.etat        ', this.UserInfo.Sonar.AireInso.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.AireInso.origine)
    write_val(fid, 2, 'Sonar.AireInso.origine        ', this.UserInfo.Sonar.AireInso.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.etat)
    write_val(fid, 2, 'Sonar.BS.etat        ', this.UserInfo.Sonar.BS.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.origine)
    write_val(fid, 2, 'Sonar.BS.origine        ', this.UserInfo.Sonar.BS.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.origineBelleImage)
    write_val(fid, 2, 'Sonar.BS.origineBelleImage        ', this.UserInfo.Sonar.BS.origineBelleImage, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.origineFullCompens)
    write_val(fid, 2, 'Sonar.BS.origineFullCompens        ', this.UserInfo.Sonar.BS.origineFullCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.IfremerCompensTable)
    write_val(fid, 2, 'Sonar.BS.IfremerCompensTable        ', this.UserInfo.Sonar.BS.IfremerCompensTable, 'Comment')
end

write_val(fid, 2, 'Colormap       ', this.UserInfo.Colormap, 'Comment')
write_val(fid, 2, 'ColormapCustom ', this.UserInfo.ColormapCustom, 'Comment')


write_End(fid, 1, 'UserInfo', 'Comment')


write_End(fid, 0, 'DatasetHeader')

fclose(fid);

% ---------------------------------------------------------
% Pour éviter de se tapper tout le décodage du fichier .ers
% Voir cl_ermapper_ers/private/decode

[nomDir, nomFicSauve] = fileparts(nomFic);
if isempty(nomDir)
    nomDir = pwd;
end
nomDirImage = fullfile(nomDir, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDir, nomFicSauve);
    if ~flag
        str = ['Directory creation failure for ' fullfile(nomDir, nomFicSauve)];
        my_warndlg(str, 1);
    end
end
nomFicSauve = fullfile(nomDirImage, [nomFicSauve '_ers.mat']);

try
    save(nomFicSauve, 'this');
    status = 1;
catch %#ok<CTCH>
    messageErreurFichier(nomFicSauve, 'WriteFailure');
    status = 0;
end


function write_Begin(fid, nbTab, Tag, varargin)
[varargin, Comment] = getFlag(varargin, 'Comment'); %#ok<ASGLU> 
if Comment
    return
end
format = get_format_Begin(nbTab, Comment);
fprintf(fid, format, Tag);


function write_End(fid, nbTab, Tag, varargin)
[varargin, Comment] = getFlag(varargin, 'Comment'); %#ok<ASGLU> 
if Comment
    return
end
format = get_format_End(nbTab, Comment);
fprintf(fid, format, Tag);


function write_val(fid, nbTab, Tag, Val, varargin)
[varargin, flagEntecote] = getFlag(varargin, 'Entecote');
[varargin, Comment] = getFlag(varargin, 'Comment'); %#ok<ASGLU> 

if Comment
    return
end

if ischar(Val)
    format = get_format_val(nbTab, Comment);
    if flagEntecote
        fprintf(fid, format, Tag, ['"' Val '"']);
    else
        fprintf(fid, format, Tag, Val);
    end
else
    if length(Val) == 1
        format = get_format_val(nbTab, Comment);
        fprintf(fid, format, Tag, num2strPrecis(Val));
    else
        format = get_format_val_deb(nbTab, Comment);
        fprintf(fid, format, Tag);
        format = get_format_val_data(nbTab, Comment);
        for i=1:size(Val, 1)
            fprintf(fid, format, num2strPrecis(Val(i,:)));
        end
        format = get_format_val_fin(nbTab, Comment);
        fprintf(fid, format);
    end
end


function format = get_format_val(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s = %s\n'];


function format = get_format_val_deb(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s = {\n'];

% -------------------------------------------------------------------------
function format = get_format_val_fin(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '}\n'];


function format = get_format_val_data(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab+2
    format = [format '\t']; %#ok
end
format = [format '%s\n'];


function format = get_format_Begin(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s Begin\n'];


function format = get_format_End(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s End\n'];
