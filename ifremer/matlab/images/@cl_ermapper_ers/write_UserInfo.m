% Ecriture d'une instance cl_ermapper_ers sur disque
% 
% Syntax
%   status = write_UserInfo(this)
%
% Input Arguments
%   this : instance de cl_ermapper_ers
%
% Output Arguments
%   status : 1 =tout s'est bien passe, 0=ca c'est pas bien passe
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function status = write_UserInfo(this)

status = 0;
[nomDir, nomFic] = fileparts(this.FicIn);
nomFic = [fullfile(nomDir, nomFic, nomFic) '_UserInfo.txt'];

fid = fopen(nomFic, 'w+');
if fid == -1
    str = sprintf('Erreur d''ecriture fichier %s', nomFic);
    my_warndlg(['cl_ermapper_ers/write_UserInfo : ', str], 1);
    return
end


write_UserInfo_Begin(fid, 1, 'UserInfo', 'Comment')
write_UserInfo_val(fid, 2, 'ImageType      ', this.UserInfo.ImageType, 'Comment')
write_UserInfo_val(fid, 2, 'DataType       ', this.UserInfo.DataType, 'Comment')
write_UserInfo_val(fid, 2, 'GeometryType   ', this.UserInfo.GeometryType, 'Comment')
write_UserInfo_val(fid, 2, 'SpectralStatus ', this.UserInfo.SpectralStatus, 'Comment')
write_UserInfo_val(fid, 2, 'XUnit          ', this.UserInfo.XUnit, 'Comment')
write_UserInfo_val(fid, 2, 'YUnit          ', this.UserInfo.YUnit, 'Comment')
write_UserInfo_val(fid, 2, 'XLabel         ', this.UserInfo.XLabel, 'Comment')
write_UserInfo_val(fid, 2, 'YLabel         ', this.UserInfo.YLabel, 'Comment')
write_UserInfo_val(fid, 2, 'YDir           ', this.UserInfo.YDir, 'Comment')
write_UserInfo_val(fid, 2, 'XDir           ', this.UserInfo.XDir, 'Comment')
write_UserInfo_val(fid, 2, 'ColormapIndex  ', this.UserInfo.ColormapIndex, 'Comment')
write_UserInfo_val(fid, 2, 'CLim           ', this.UserInfo.CLim, 'Comment')
write_UserInfo_val(fid, 2, 'Video          ', this.UserInfo.Video, 'Comment')
write_UserInfo_val(fid, 2, 'VertExagAuto   ', this.UserInfo.VertExagAuto, 'Comment')
write_UserInfo_val(fid, 2, 'VertExag       ', this.UserInfo.VertExag, 'Comment')
write_UserInfo_val(fid, 2, 'Azimuth        ', this.UserInfo.Azimuth, 'Comment')
write_UserInfo_val(fid, 2, 'ContourValues  ', this.UserInfo.ContourValues, 'Comment')
write_UserInfo_val(fid, 2, 'NuIdentParent  ', this.UserInfo.NuIdentParent, 'Comment')
write_UserInfo_val(fid, 2, 'TagSynchroX    ', this.UserInfo.TagSynchroX, 'Comment')
write_UserInfo_val(fid, 2, 'TagSynchroY    ', this.UserInfo.TagSynchroY, 'Comment')
write_UserInfo_val(fid, 2, 'TagSynchroContrast ', this.UserInfo.TagSynchroContrast, 'Comment')
write_UserInfo_val(fid, 2, 'InitialFileName', this.UserInfo.InitialFileName, 'Comment')

write_UserInfo_val(fid, 2, 'HistoCentralClasses  ', this.UserInfo.HistoCentralClasses, 'Comment')
write_UserInfo_val(fid, 2, 'HistoValues          ', this.UserInfo.HistoValues, 'Comment')
if ~isempty(this.UserInfo.StatValues)
    write_UserInfo_val(fid, 2, 'StatValues.Moyenne     ', this.UserInfo.StatValues.Moyenne, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Variance    ', this.UserInfo.StatValues.Variance, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Sigma       ', this.UserInfo.StatValues.Sigma, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Mediane     ', this.UserInfo.StatValues.Mediane, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Min         ', this.UserInfo.StatValues.Min, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Max         ', this.UserInfo.StatValues.Max, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Range       ', this.UserInfo.StatValues.Range, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Quant_03_97 ', this.UserInfo.StatValues.Quant_03_97, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Quant_01_99 ', this.UserInfo.StatValues.Quant_01_99, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Quant_25_75 ', this.UserInfo.StatValues.Quant_25_75, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Sampling    ', this.UserInfo.StatValues.Sampling, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.PercentNaN  ', this.UserInfo.StatValues.PercentNaN, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Skewness    ', this.UserInfo.StatValues.Skewness, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Kurtosis    ', this.UserInfo.StatValues.Kurtosis, 'Comment')
    write_UserInfo_val(fid, 2, 'StatValues.Entropy     ', this.UserInfo.StatValues.Entropy, 'Comment')
end

if ~isempty(this.UserInfo.Sonar.Family)
    write_UserInfo_val(fid, 2, 'Sonar.Ident              ', this.UserInfo.Sonar.Ident, 'Comment')
    write_UserInfo_val(fid, 2, 'Sonar.Family             ', this.UserInfo.Sonar.Family, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.RawDataResol)
    write_UserInfo_val(fid, 2, 'Sonar.RawDataResol       ', this.UserInfo.Sonar.RawDataResol, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.ResolutionD)
    write_UserInfo_val(fid, 2, 'Sonar.ResolutionD        ', this.UserInfo.Sonar.ResolutionD, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.ResolutionX)
    write_UserInfo_val(fid, 2, 'Sonar.ResolutionX        ', this.UserInfo.Sonar.ResolutionX, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.etat)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.etat                ', this.UserInfo.Sonar.TVG.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.origine)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.origine             ', this.UserInfo.Sonar.TVG.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructTypeCompens)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.ConstructTypeCompens', this.UserInfo.Sonar.TVG.ConstructTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructAlpha)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.ConstructAlpha      ', this.UserInfo.Sonar.TVG.ConstructAlpha, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructCoefDiverg)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.ConstructCoefDiverg        ', this.UserInfo.Sonar.TVG.ConstructCoefDiverg, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructConstante)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.ConstructConstante      ', this.UserInfo.Sonar.TVG.ConstructConstante, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.ConstructTable)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.ConstructTable      ', this.UserInfo.Sonar.TVG.ConstructTable, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.IfremerAlpha)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.IfremerAlpha        ', this.UserInfo.Sonar.TVG.IfremerAlpha, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.IfremerCoefDiverg)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.IfremerCoefDiverg        ', this.UserInfo.Sonar.TVG.IfremerCoefDiverg, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.TVG.IfremerConstante)
    write_UserInfo_val(fid, 2, 'Sonar.TVG.IfremerConstante        ', this.UserInfo.Sonar.TVG.IfremerConstante, 'Comment')
end


if ~isempty(this.UserInfo.Sonar.NE.etat)
    write_UserInfo_val(fid, 2, 'Sonar.NE.etat        ', this.UserInfo.Sonar.NE.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.GT.etat)
    write_UserInfo_val(fid, 2, 'Sonar.GT.etat        ', this.UserInfo.Sonar.GT.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.SH.etat)
    write_UserInfo_val(fid, 2, 'Sonar.SH.etat        ', this.UserInfo.Sonar.SH.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.etat)
    write_UserInfo_val(fid, 2, 'Sonar.DiagEmi.etat        ', this.UserInfo.Sonar.DiagEmi.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.origine)
    write_UserInfo_val(fid, 2, 'Sonar.DiagEmi.origine        ', this.UserInfo.Sonar.DiagEmi.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens)
    write_UserInfo_val(fid, 2, 'Sonar.DiagEmi.ConstructTypeCompens        ', this.UserInfo.Sonar.DiagEmi.ConstructTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens)
    write_UserInfo_val(fid, 2, 'Sonar.DiagEmi.IfremerTypeCompens        ', this.UserInfo.Sonar.DiagEmi.IfremerTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.etat)
    write_UserInfo_val(fid, 2, 'Sonar.DiagRec.etat        ', this.UserInfo.Sonar.DiagRec.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.origine)
    write_UserInfo_val(fid, 2, 'Sonar.DiagRec.origine        ', this.UserInfo.Sonar.DiagRec.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.ConstructTypeCompens)
    write_UserInfo_val(fid, 2, 'Sonar.DiagRec.ConstructTypeCompens        ', this.UserInfo.Sonar.DiagRec.ConstructTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.DiagRec.IfremerTypeCompens)
    write_UserInfo_val(fid, 2, 'Sonar.DiagRec.IfremerTypeCompens        ', this.UserInfo.Sonar.DiagRec.IfremerTypeCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.AireInso.etat)
    write_UserInfo_val(fid, 2, 'Sonar.AireInso.etat        ', this.UserInfo.Sonar.AireInso.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.AireInso.origine)
    write_UserInfo_val(fid, 2, 'Sonar.AireInso.origine        ', this.UserInfo.Sonar.AireInso.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.etat)
    write_UserInfo_val(fid, 2, 'Sonar.BS.etat        ', this.UserInfo.Sonar.BS.etat, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.origine)
    write_UserInfo_val(fid, 2, 'Sonar.BS.origine        ', this.UserInfo.Sonar.BS.origine, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.origineBelleImage)
    write_UserInfo_val(fid, 2, 'Sonar.BS.origineBelleImage        ', this.UserInfo.Sonar.BS.origineBelleImage, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.origineFullCompens)
    write_UserInfo_val(fid, 2, 'Sonar.BS.origineFullCompens        ', this.UserInfo.Sonar.BS.origineFullCompens, 'Comment')
end
if ~isempty(this.UserInfo.Sonar.BS.IfremerCompensTable)
    write_UserInfo_val(fid, 2, 'Sonar.BS.IfremerCompensTable        ', this.UserInfo.Sonar.BS.IfremerCompensTable, 'Comment')
end

write_UserInfo_val(fid, 2, 'Colormap       ', this.UserInfo.Colormap, 'Comment')
write_UserInfo_val(fid, 2, 'ColormapCustom ', this.UserInfo.ColormapCustom, 'Comment')


write_UserInfo_End(fid, 1, 'UserInfo', 'Comment')

fclose(fid);

% ---------------------------------------------------------
% Pour eviter de se tapper tout le decodage du fichier .ers
% Voir cl_ermapper_ers/private/decode

[nomDir, nomFicSauve] = fileparts(nomFic);
if isempty(nomDir)
    nomDir = pwd;
end
nomDirImage = fullfile(nomDir, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDir, nomFicSauve);
    if ~flag
        str = ['Directory creation failure for ' fullfile(nomDir, nomFicSauve)];
        my_warndlg(str, 1);
    end
end
nomFicSauve = fullfile(nomDirImage, [nomFicSauve '_ers.mat']);

try
    save(nomFicSauve, 'this');
    status = 1;
catch %#ok<CTCH>
    messageErreurFichier(nomFicSauve, 'WriteFailure');
    status = 0;
end





% -------------------------------------------------------------------------
function write_UserInfo_Begin(fid, nbTab, Tag, varargin)
[varargin, Comment] = getFlag(varargin, 'Comment'); %#ok<ASGLU>
format = get_format_Begin(nbTab, Comment);
fprintf(fid, format, Tag);

% -------------------------------------------------------------------------
function write_UserInfo_End(fid, nbTab, Tag, varargin)
[varargin, Comment] = getFlag(varargin, 'Comment'); %#ok<ASGLU>
format = get_format_End(nbTab, Comment);
fprintf(fid, format, Tag);

% -------------------------------------------------------------------------
function write_UserInfo_val(fid, nbTab, Tag, Val, varargin)
[varargin, flagEntecote] = getFlag(varargin, 'Entecote');
[varargin, Comment] = getFlag(varargin, 'Comment'); %#ok<ASGLU>
if ischar(Val)
    format = get_format_val(nbTab, Comment);
    if flagEntecote
        fprintf(fid, format, Tag, ['"' Val '"']);
    else
        fprintf(fid, format, Tag, Val);
    end
else
    if length(Val) == 1
        format = get_format_val(nbTab, Comment);
        fprintf(fid, format, Tag, num2strPrecis(Val));
    else
        format = get_format_val_deb(nbTab, Comment);
        fprintf(fid, format, Tag);
        format = get_format_val_data(nbTab, Comment);
        for i=1:size(Val, 1)
            fprintf(fid, format, num2strPrecis(Val(i,:)));
        end
        format = get_format_val_fin(nbTab, Comment);
        fprintf(fid, format);
    end
end

% -------------------------------------------------------------------------
function format = get_format_val(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s = %s\n'];

% -------------------------------------------------------------------------
function format = get_format_val_deb(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s = {\n'];

% -------------------------------------------------------------------------
function format = get_format_val_fin(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '}\n'];

% -------------------------------------------------------------------------
function format = get_format_val_data(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab+2
    format = [format '\t']; %#ok
end
format = [format '%s\n'];

% -------------------------------------------------------------------------
function format = get_format_Begin(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s Begin\n'];

% -------------------------------------------------------------------------
function format = get_format_End(nbTab, Comment)
if Comment
    format = '##';
else
    format = [];
end
for i=1:nbTab
    format = [format '\t']; %#ok
end
format = [format '%s End\n'];
