function ALL_SegmentationSamantha(this, indImage, listeFicAll, nomFicCurves, SameReflectivityStatus, InfoCompensationCurve, gridSize, ...
    nomDirOut, w, StopRegul, UseModelBS)

%% Recherche des layers synchronis�s

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

% Ajout JMA le 05/12/2018 pour �viter la question du choix du layer pour chaque fichier .all
subSameDataTypeAsTheCurrentOne = find((DataTypes == this(indImage).DataType) & (indLayers(:) ~= indImage));
DataTypes(subSameDataTypeAsTheCurrentOne)  = [];
indLayers(subSameDataTypeAsTheCurrentOne)  = []; %#ok<NASGU>
nomsLayers(subSameDataTypeAsTheCurrentOne) = [];
% Fin ajout JMA le 05/12/2018

% [~, sub] = unique(DataTypes);
% DataTypes  = DataTypes(sub);
% indLayers  = indLayers(sub);
% nomsLayers = nomsLayers(sub);

%% Lecture des fichiers de compensation si il y en a

Selection = [];
if isempty(InfoCompensationCurve) || isempty(InfoCompensationCurve.FileName)
    bilan         = [];
    ModeDependant = 0;
    UseModel      = [];
    PreserveMeanValueIfModel = [];
else
    FileNameCompensation = InfoCompensationCurve.FileName;
    if ischar(FileNameCompensation)
        FileNameCompensation = {FileNameCompensation};
    end
    ModeDependant = InfoCompensationCurve.ModeDependant;
    
    for k2=1:length(FileNameCompensation)
        CourbeConditionnelle = load_courbesStats(FileNameCompensation{k2});
        if isempty(CourbeConditionnelle)
            return
        end
        bilan{k2} = CourbeConditionnelle.bilan; %#ok<AGROW>
        if isfield(InfoCompensationCurve, 'UseModel') && ~isempty(InfoCompensationCurve.UseModel)
            UseModel(k2) = InfoCompensationCurve.UseModel(k2); %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = InfoCompensationCurve.PreserveMeanValueIfModel(k2); %#ok<AGROW>
        else
            UseModel(k2) = 0; %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = 0; %#ok<AGROW>
        end
    end
end

%% Loop on the files

Carto = [];
N = length(listeFicAll);
str1 = 'Segmentation des .all';
str2 = 'Processing the segmentation of the .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    str1 = 'Traitement du fichier : ';
    str2 = 'Processing : ';
    fprintf('----------------------------------------------------------\n%s %s\n', Lang(str1,str2), listeFicAll{k});
    
    %% Segmentation d'un fichier
    
    % TODO : d�composer cette fonction pour appel � ALL_SegmentationSamantha_unitaire lorsque la transmission du BackscatterStatus
    % sera r�alis� (afin d'�viter la transmission de this qui serait dupliqu� dans le cas de l'appel � la //Tbx
%     ALL_SegmentationSamantha_unitaire(listeFicAll{k}, ...);
    
    
    %% Lecture des layers
    
    aKM = cl_simrad_all('nomFic', listeFicAll{k});
    if isempty(aKM)
        continue
    end
    
    %% get layers processed as the current image
    
    [~, ImageName] = fileparts(listeFicAll{k});
    [flag, c, indLayerAsCurrentOne, Selection, Carto] = get_layersProcessedAsThis(aKM, this, indImage, nomsLayers, DataTypes, ...
        bilan, ModeDependant, Carto, Selection, SameReflectivityStatus, UseModel, 'PreserveMeanValueIfModel', PreserveMeanValueIfModel);
    if ~flag
        nomFic = get(aKM, 'nomFic');
        str1 = sprintf('Le mise en conformit� de la r�flectivit� n''a pas pu �tre faite pour le fichier "%s'.', nomFic);
        str2 = sprintf('The reflectivity  could not be set for "%s'.', nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'get_layersProcessedAsThisNotPossible');
        continue
    end
    c(indLayerAsCurrentOne).Name = ImageName;
    
    %% Import des courbes
    
    CourbesStatistiques = get_CourbesStatistiques(c(indLayerAsCurrentOne));
    nbBefore = length(CourbesStatistiques);
	[flag, c(indLayerAsCurrentOne)] = import_CourbesStats(c(indLayerAsCurrentOne), nomFicCurves, 'flagPlot', 0);
    if ~flag
        my_close(hw, 'MsgEnd');
        return
    end
    CourbesStatistiques = get_CourbesStatistiques(c(indLayerAsCurrentOne));
    nbAfter = length(CourbesStatistiques);
    subCurves = (nbBefore+1):nbAfter;
    
    %% Interpolation des layers
    
    mask = ROI_auto(c(indLayerAsCurrentOne), 1, 'MaskNonNaN', [], [], [-Inf Inf], 1);
    
    strDataTypeAngle = CourbesStatistiques(subCurves(1)).bilan{1}(1).DataTypeConditions{1};
    indLayerAngle = findIndLayerSonar(c, 1, 'strDataType', strDataTypeAngle, 'WithCurrentImage', 1);
    c([indLayerAsCurrentOne indLayerAngle]) = WinFillNaN(c([indLayerAsCurrentOne indLayerAngle]), 'window', w, 'Mute', 1);
    
    %% Segmentation
    
    [flag, b] = segmentation_CourbesStats(c, indLayerAsCurrentOne, subCurves, indLayerAsCurrentOne, indLayerAngle, ...
        [], 1, w, StopRegul, UseModelBS);
    if ~flag
        continue
    end
    
    ImageName = b.Name;
    [b, flag] = masquage(b, 'LayerMask', mask, 'valMask', 1, 'zone', 1);
    if ~flag
        return
    end
    b.Name = ImageName;
    CLim = b.CLim;
    

    %% Export de l'image segment�e
    
    ImageName = b.Name;
    nomFicOut = fullfile(nomDirOut, [ImageName '.xml']);
    flag = export_xml(b, nomFicOut);
    if ~flag
        % TODO : message erreur
        break
    end
    
    %% Mosaique
    
    [flag, d] = sonarMosaique([b c], 1, gridSize, 'CreationMosAngle', 1, 'indLayerAngle', 1+indLayerAngle, ...
        'MethodeRemplissage', 4, 'NoQuestion', 1);
    if ~flag
        % TODO : message erreur
    end
%     d(1).CLim = CLim;
    d(1).ColormapCustom = jet(CLim(2)+1);
    
    Window = [5 5];
    for k2=1:2
        ImageName = d(k2).Name;
        d(k2) = WinFillNaN(d(k2), 'window', Window);
        d(k2).Name = ImageName;
        nomFicOut = fullfile(nomDirOut, [ImageName '.ers']);
        flag = export_ermapper(d(k2), nomFicOut);
        if~flag
            % TODO : message
        end
    end
end
my_close(hw, 'MsgEnd');
