function [flag, this] = BSCorr2Curves(this, BsCorr, BSCorrInfo)

switch BSCorrInfo.identFormat
    case 1 % EM710
        [flag, this] = BSCorr2CurvesSpline(this, BsCorr);
    case 2 % EM122, EM302
        [flag, this] = BSCorr2CurvesPoly(this, BsCorr);
    case 3 % EM304
        [flag, this] = BSCorr2CurvesSpline(this, BsCorr);
    case 4 % EM2040
        [flag, this] = BSCorr2CurvesSpline(this, BsCorr);
    otherwise
        str1 = 'Ce type de BSCorr n''est pas encore pris en charge.';
        str2 = 'This type of BSCOrr is not supported.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
end
% plotCourbesStats(this, 'Stats', 0);% , 'sub', -1);
