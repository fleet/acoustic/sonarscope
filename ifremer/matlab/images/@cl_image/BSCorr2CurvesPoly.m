function [flag, this] = BSCorr2CurvesPoly(this, BsCorr)

flag = 1;

C0 = cl_sounder([]);

SonarDescription = this.Sonar.Desciption;

for k1=1:length(BsCorr)
    pingMode  = BsCorr(k1).pingMode;
    swathMode = BsCorr(k1).swathMode;
    
    if isempty(swathMode)
        continue
    end
    
    [Mode_1, ListeMode_2] = BsCorr_ModesIn(C0, pingMode, swathMode);
    
    for k=1:length(ListeMode_2)
        SonarDescription = set(SonarDescription, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', ListeMode_2(k));
        nbSectors = size(BsCorr(k1).Sector.Node,1);
        subTxDiag = 1:nbSectors;
        if BsCorr(k1).swathMode == 3 % 2 &v&nt !!!
            subTxDiag = subTxDiag + nbSectors;
        end
        for k2=1:nbSectors
            Level  = -BsCorr(k1).Sector.SL(k2);
            Centre =  BsCorr(k1).Sector.Node(k2,1);
            Width  =  BsCorr(k1).Sector.Node(k2,2);
            
            XData = centrage_magnetique((Centre-Width):1:(Centre+Width));
            YData  = AntennePoly(XData, [Level Centre Width]);
            sub = isnan(YData);
            XData(sub) = [];
            YData(sub) = [];
            
            p = [Level Centre Width];
            pMin = p - [10 30 50];
            pMax = p + [10 30 50];
            
            TxDiag = AntennePolyModel('XData', XData, 'YData', YData, 'ValParams', p, 'ValMinParams', pMin, 'ValMaxParams', pMax);
            % set(TxDiag(k2), 'XData', XData, 'YData', YData, 'params', params, 'paramsSup', {xNodes});
            
            n = length(XData);
            sector.x                  = XData(:);
            sector.y                  = YData(:);
            sector.ymedian            = NaN(n,1,'single');
            sector.nomVarCondition    = 'PingBeam_TxBeamIndexSwath';
            sector.sub                = true(n,1);
            sector.nx                 = ones(n,1,'single');
            sector.std                = zeros(n,1,'single');
            sector.titreV             = 'From BSCorr.txt';
            sector.nomX               = 'PingBeam_BeamPointingAngle';
            sector.Biais              = 0;
            sector.Unit               = 'dB';
            sector.commentaire        = 'Curbe coming from a BSCorr.txt';
            sector.Tag                = 'DiagTx';
            sector.DataTypeValue      = 'Reflectivity';
            sector.DataTypeConditions = {'BeamPointingAngle'  'TxBeamIndexSwath'};
            sector.Support            = {'Layer'  'Layer'};
            sector.GeometryType       = 7;
            sector.TypeCourbeStat     = 'Statistics';
            sector.numVarCondition    = subTxDiag(k2);
            sector.model              = TxDiag;
            sector.Mode               = pingMode; % ListeMode_2(k);
           
            if Mode_1 == 1 % Single Swath
                sector.nomZoneEtude     = sprintf('DiagTx : Mode %d : Swath Single : Signal CW', pingMode);
                sector.TagSup           = {'Sonar.Mode_1', 1; 'Sonar.Mode_2', pingMode};
                sector.NY               = [n nbSectors]; % TODO NY: [152 6]
            else % Double Swath
                sector.nomZoneEtude    = sprintf('DiagTx : Mode %d : Swath Double : Signal CW', pingMode);
                sector.TagSup          = {'Sonar.Mode_1', 2; 'Sonar.Mode_2', pingMode};
                sector.NY              = [n 2*nbSectors]; % TODO NY: [152 6]
            end
            sectors(subTxDiag(k2)) = sector; %#ok<AGROW>
        end
%         plot(TxDiag); title(sprintf('Mode_1 : %d  -   Mode_2 : %d', Mode_1,  ListeMode_2(k)))
        
        if (Mode_1 == 1) || ((Mode_1 == 2) && (swathMode == 3))
            if k == 1
                this.CourbesStatistiques(end+1).bilan{1} = sectors;
            else
                sectors = this.CourbesStatistiques(end).bilan{1};
                for k3=1:length(sectors)
                    sectors(k3).Mode = ListeMode_2(k);
                    if Mode_1 == 1 % Single Swath
                        sectors(k3).nomZoneEtude = sprintf('DiagTx : Mode %d : Swath Single : Signal FM', pingMode);
                        sectors(k3).TagSup       = {'Sonar.Mode_1', 1; 'Sonar.Mode_2', pingMode};
                    else % Double Swath
                        sectors(k3).nomZoneEtude = sprintf('DiagTx : Mode %d : Swath Double : Signal FM', pingMode);
                        sectors(k3).TagSup       = {'Sonar.Mode_1', 2; 'Sonar.Mode_2', pingMode};
                    end
                end
                this.CourbesStatistiques(end+1).bilan{1} = sectors;
            end
            clear sectors
        else
%             messageForDebugInspection
        end
    end
end
