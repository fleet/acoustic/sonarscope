function [flag, this] = BSCorr2CurvesSpline(this, BsCorr)

flag = 1;

C0 = cl_sounder([]);

SonarDescription = this.Sonar.Desciption;

for k1=1:length(BsCorr)
    pingMode  = BsCorr(k1).pingMode;
    swathMode = BsCorr(k1).swathMode;
    
    if isempty(swathMode)
        continue
    end
    
    [Mode_1, ListeMode_2] = BsCorr_ModesIn(C0, pingMode, swathMode);
    
    for k=1:length(ListeMode_2)
        SonarDescription = set(SonarDescription, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', ListeMode_2(k));
        nbSectors = length(BsCorr(k1).Sector);
        subTxDiag = 1:nbSectors;
        if BsCorr(k1).swathMode == 3
            subTxDiag = subTxDiag + nbSectors;
        end
        for k2=1:nbSectors
            xNodes = BsCorr(k1).Sector(k2).Node(:,1);
            yNodes = BsCorr(k1).Sector(k2).Node(:,2);
%             figure; plot(xNodes, yNodes, '*k'); 
            
            xstep = ceil((xNodes(end) - xNodes(1)) / 32);
            xNodesNew = xNodes(1) : xstep : xNodes(end);
            xNodesNew = centrage_magnetique(xNodesNew);
            yNodes = interp1(xNodes, yNodes, xNodesNew, 'linear', 'extrap');
            xNodes = xNodesNew;
            
            XData  = min(xNodes):0.5:max(xNodes);
            YData  = spline(xNodes, yNodes, XData);
%             figure; plot(xNodes, yNodes, '*k'); grid on; hold on; plot(XData, YData);
            
            TxDiag = AntenneSplineModel('XData', XData, 'YData', YData, 'xNodes', xNodes, 'yNodes', yNodes);
%             plot(TxDiag)
            % set(TxDiag(k2), 'XData', XData, 'YData', YData, 'params', params, 'paramsSup', {xNodes});
            
            n = length(XData);
            sector.x                  = XData(:);
            sector.y                  = YData(:);
            sector.ymedian            = NaN(n,1,'single');
            sector.nomVarCondition    = 'PingBeam_TxBeamIndexSwath';
            sector.sub                = true(n,1);
            sector.nx                 = ones(n,1,'single');
            sector.std                = zeros(n,1,'single');
            sector.titreV             = 'From BSCorr.txt';
            sector.nomX               = 'PingBeam_BeamPointingAngle';
            sector.Biais              = 0;
            sector.Unit               = 'dB';
            sector.commentaire        = 'Curbe coming from a BSCorr.txt';
            sector.Tag                = 'DiagTx';
            sector.DataTypeValue      = 'Reflectivity';
            sector.DataTypeConditions = {'BeamPointingAngle'  'TxBeamIndexSwath'};
            sector.Support            = {'Layer'  'Layer'};
            sector.GeometryType       = 7;
            sector.TypeCourbeStat     = 'Statistics';
            sector.numVarCondition    = subTxDiag(k2);
            sector.model              = TxDiag;
            sector.Mode               = ListeMode_2(k);
           
            if Mode_1 == 1 % Single Swath
                sector.nomZoneEtude     = sprintf('DiagTx : Mode %d : Swath Single : Signal CW', pingMode);
                sector.TagSup           = {'Sonar.Mode_1', 1; 'Sonar.Mode_2', pingMode};
                sector.NY               = [n nbSectors]; % TODO NY: [152 6]
            else % Double Swath
                sector.nomZoneEtude    = sprintf('DiagTx : Mode %d : Swath Double : Signal CW', pingMode);
                sector.TagSup          = {'Sonar.Mode_1', 2; 'Sonar.Mode_2', pingMode};
                sector.NY              = [n 2*nbSectors]; % TODO NY: [152 6]
            end
            sectors(subTxDiag(k2)) = sector; %#ok<AGROW>
        end
%         plot(TxDiag); title(sprintf('Mode_1 : %d  -   Mode_2 : %d', Mode_1,  ListeMode_2(k)))
        
        if (Mode_1 == 1) || ((Mode_1 == 2) && (swathMode == 3))
        %   SingleSwath  || (DoubleSwath && Swath#2)
            if k == 1
                this.CourbesStatistiques(end+1).bilan{1} = sectors;
            else
                sectors = this.CourbesStatistiques(end).bilan{1};
                for k3=1:length(sectors)
                    sectors(k3).Mode = ListeMode_2(k);
                    if Mode_1 == 1 % Single Swath
                        sectors(k3).nomZoneEtude = sprintf('DiagTx : Mode %d : Swath Single : Signal FM', pingMode);
                        sectors(k3).TagSup       = {'Sonar.Mode_1', 1; 'Sonar.Mode_2', pingMode};
                    else % Double Swath
                        sectors(k3).nomZoneEtude = sprintf('DiagTx : Mode %d : Swath Double : Signal FM', pingMode);
                        sectors(k3).TagSup       = {'Sonar.Mode_1', 2; 'Sonar.Mode_2', pingMode};
                    end
                end
                this.CourbesStatistiques(end+1).bilan{1} = sectors;
            end
            clear sectors
        else
            %messageForDebugInspection : DoubleSwath && Swath#1
        end
    end
end
