% Calcul des layers latitude et longitude
%
% Syntax
%   [flag, b] = ComputeLatLonInPingAcrossDist(a, AlongDistance, ...
%                                       Heading, FishLatitude, FishLongitude, ...)
%
% Input Arguments
%   a : Instance de cl_image
%   AlongDistance  : Instance de cl_image contenant les distances longitudinales
%   Heading        : Cap du navire (deg)
%   FishLatitude   : Latitude du navire (deg)
%   FishLongitude  : Longitude du navire (deg)
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   LonLim : Encadrement impos� en x ([] par d�faut)
%   LatLim : Encadrement impos� en y ([] par d�faut)
%
% Output Arguments
%   b : Instances de cl_image contenant la latitude et la longitude
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = ComputeLatLonInPingAcrossDist(this, AlongDistance, ...
    Heading, FishLatitude, FishLongitude, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LonLim]            = getPropertyValue(varargin, 'LonLim',            []);
[varargin, LatLim]            = getPropertyValue(varargin, 'LatLim',            []);
[varargin, AcrossDistanceLim] = getPropertyValue(varargin, 'AcrossDistanceLim', []);
[varargin, NoWaitbar]         = getPropertyValue(varargin, 'NoWaitbar',         0); %#ok<ASGLU>

a = [];

%% Controles

listGeometryType = [cl_image.indGeometryType('PingSamples')
    cl_image.indGeometryType('PingAcrossDist')
    cl_image.indGeometryType('PingBeam')
    cl_image.indGeometryType('PingRange')
    cl_image.indGeometryType('PingAcrossSample')];
flag = testSignature(this, 'GeometryType', listGeometryType);   % 5 pour une images sonar multifaisceau
if ~flag
    return
end

identLayerAlongDist  = cl_image.indDataType('AlongDist');
flag = testSignature(AlongDistance, 'GeometryType', listGeometryType, 'DataType', identLayerAlongDist);
if ~flag
    return
end

if isempty(Heading)
    my_warndlg ('Heading non renseign�', 1);
    return
end

if isempty(FishLatitude)
    my_warndlg('FishLatitude non renseign�', 1);
    return
end

if isempty(FishLongitude)
    my_warndlg('FishLongitude non renseign�', 1);
    return
end

% if ~isempty(AcrossDistanceLim)
%     sub = (AcrossDistance.Image < AcrossDistanceLim(1)) | (AcrossDistance.Image > AcrossDistanceLim(2));
%     AcrossDistance.Image(sub) = NaN;
% end

if isempty(this.Carto)
    my_warndlg('No cartography defined in this image', 1);
    flag = 0;
    return
end

flagPingAcrossSample = this.GeometryType == cl_image.indGeometryType('PingAcrossSample');

% [xFish, yFish] = latlon2xy(this.Carto, FishLatitude, FishLongitude);
% [convergence, scaleFactor] = get_convergence(this.Carto, FishLatitude, FishLongitude);

Lat = this;
Lon = this;

[subsuby, subyAlong] = intersect3(this.Sonar.PingCounter(suby,1), ...
    AlongDistance.Sonar.PingCounter(:,1), AlongDistance.Sonar.PingCounter(:,1));
suby  = suby(subsuby);

if isempty(suby)
    flag = 0;
    return
end

[~, ordre] = sort(suby);
suby = suby(ordre);
subyAlong  = subyAlong(ordre);

M = length(suby);

[subsubx, subxAlong] = intersect3(this.x(subx), ...
    AlongDistance.x, AlongDistance.x);
subx  = subx(subsubx);

Heading = double(Heading);

Lat = replace_Image(Lat, NaN(M, length(subsubx), 'double'));
Lat.Writable = true;
Lon = replace_Image(Lon, NaN(M, length(subsubx), 'double'));
Lon.Writable = true;
E2 = get(this.Carto, 'Ellipsoide.Excentricite') ^ 2;
A  = get(this.Carto, 'Ellipsoide.DemiGrandAxe');

if A == 0
    str1 = 'Il n''y a pas de carto d�finie pour cette image.';
    str2 = 'There is no Carto defined for this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

message = this.Name;
message(length(message)+1:150) = '.';
hw = create_waitbar(message, 'Entete', 'IFREMER - SonarScope : processing Latitude & longitude', 'N', M, 'NoWaitbar', NoWaitbar);
for k=1:M
    my_waitbar(k, M, hw);
    iy = suby(k);
    
    % % A �t� remplac� par calcul direct en LatLong
    %     c = cosd(Heading(iy) + convergence(iy));
    %     s = sind(Heading(iy) + convergence(iy));
    %     Rotation = [c s; -s c];
    %
    %     x = AcrossDistance.Image(subyAcross(k), subxAcross) * scaleFactor(iy);
    %     y = AlongDistance.Image( subyAlong(k),  subxAlong) * scaleFactor(iy);
    %     xy = double(Rotation * [x;y]);
    %     x = xy(1,:) + xFish(iy);
    %     y = xy(2,:) + yFish(iy);
    %
    %     [lat, lon] = xy2latlon(this.Carto, x, y);
    %     Lat.Image(k,:) = lat;
    %     Lon.Image(k,:) = lon;
    
    % Calcul direct en LatLong
    % Document Cartolob++ / Manuel de r�f�rence avril 2004 Paragraphe 7.1.2
    
    %     sinLN2 = sind(FishLatitude(iy)) .^ 2;
    %     E2MoinssinLN2 = 1 - E2 * sinLN2;
    %     NORM = A ./ sqrt(E2MoinssinLN2);
    %     RAY0 = NORM * (1 - E2) ./ E2MoinssinLN2;
    %     SC = sind(Heading(iy));
    %     CC = cosd(Heading(iy));
    %     DL = double(AlongDistance.Image( subyAlong(k),  subxAlong));
    %     DT = double(AcrossDistance.Image(subyAcross(k), subxAcross));
    %     Lat.Image(k,:) = FishLatitude(iy)  + (DL * CC - DT * SC) .* ((180/pi) ./ RAY0);
    %     Lon.Image(k,:) = FishLongitude(iy) + (DL * SC + DT * CC) .* ((180/pi) ./ NORM ./ cosd(FishLatitude(iy)));
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Lon.Image(k,:), Lat.Image(k,:), '*b'); grid on;
    
    AlongD  = AlongDistance.Image(subyAlong(k),  subxAlong);
    %     subxAcross = interp1(this.x, 1:this.nbColumns, AlongDistance.x(subxAlong))
    %     AcrossD = this.x(subxAlong);
    AcrossD = this.x(subx); % Modifi� le 29/10/2010
    
    if flagPingAcrossSample
        AcrossD = AcrossD * this.Sonar.ResolAcrossSample(suby(k));
    end
    
    [Lat.Image(k,:), Lon.Image(k,:)] = calculLatLonFromAlongAcross(E2, A, FishLatitude(iy), FishLongitude(iy), Heading(iy), AlongD, AcrossD);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Lon.Image(k,:), Lat.Image(k,:), '*r'); grid on;
end
my_close(hw, 'MsgEnd')

%% Gestion de l'encadrement

if ~isempty(LonLim)
    sub = (Lon.Image < LonLim(1)) | (Lon.Image > LonLim(2));
    Lat.Image(sub) = NaN;
    Lon.Image(sub) = NaN;
end
if ~isempty(LatLim)
    sub = (Lat.Image < LatLim(1)) | (Lat.Image > LatLim(2));
    Lat.Image(sub) = NaN;
    Lon.Image(sub) = NaN;
end

%% Mise a jour de coordonnees

Lat = majCoordonnees(Lat, subx, suby);
Lon = majCoordonnees(Lon, subx, suby);
Lat.Unit = 'deg';
Lon.Unit = 'deg';

%% Calcul des statistiques

Lat = compute_stats(Lat);
Lon = compute_stats(Lon);

%% Rehaussement de contraste

Lat.TagSynchroContrast = num2str(rand(1));
CLim = [Lat.StatValues.Min Lat.StatValues.Max];
Lat.CLim = CLim;

Lon.TagSynchroContrast = num2str(rand(1));
CLim = [Lon.StatValues.Min Lon.StatValues.Max];
Lon.CLim = CLim;

Lat.DataType = cl_image.indDataType('Latitude');
Lon.DataType = cl_image.indDataType('Longitude');

%% Par ici la sortie

a = Lat;
a(2) = Lon;

%% Completion du titre

a(1) = update_Name(a(1));
a(2) = update_Name(a(2));
