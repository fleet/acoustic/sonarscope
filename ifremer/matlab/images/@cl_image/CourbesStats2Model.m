% Optimisation des courbes de statistiques conditionnelles
%
% Syntax
%   this = CourbesStats2Model(this, Tag, ...)
%
% Input Arguments
%   this : Instance de cl_image
%   Tag  : 'BS' ou 'DiagTx'
%
% Name-Value Pair Arguments
%   sub : liste des courbes a traiter
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = CourbesStats2Model(this, Tag, varargin)

[varargin, Last] = getFlag(varargin, 'Last');
if Last
    sub = length(this.CourbesStatistiques);
else
    [varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques)); %#ok<ASGLU>
end

for i=1:length(sub)
    switch Tag
        case 'BS'
            this.CourbesStatistiques(sub(i)).bilan = optimBSLurton(this.CourbesStatistiques(sub(i)).bilan);
        case 'DiagTx'
            this = optimDiagTx(this, sub(i));
    end
end
