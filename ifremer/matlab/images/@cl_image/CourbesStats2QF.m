function CourbesStats2QF(this, subBathy, subResidus)

fig = FigUtils.createSScFigure;

strCoul = 'brgmkc';
nbCoul = length(strCoul);
for k=1:nbCoul
    Coul(k,:) = coul2mat(strCoul(k)); %#ok<AGROW>
end

iCoul = [];
bilanBathy = this.CourbesStatistiques(subBathy).bilan{1};
if ~isempty(subResidus)
    bilanResidus = this.CourbesStatistiques(subResidus).bilan{1}; 
end
if strcmp(bilanBathy(1).DataTypeValue, 'Bathymetry')
    for k2=1:length(bilanBathy)
        if isempty(iCoul)
            iCoul = length(findobj(fig, 'Type', 'line'));
        end
        iCoul = 1 + mod(iCoul, nbCoul);
        
        if isfield(bilanBathy(1), 'Color') && ~isempty(bilanBathy(1).Color)
            Color = bilanBathy(1).Color;
        else
            Color = Coul(iCoul,:);
        end
        
        if isempty(subResidus)
            std = bilanBathy(k2).std;
        else
            std = bilanResidus(k2).std;
        end
        
        QF = std .\ abs(bilanBathy(k2).y);
        QF = log10(QF);
        
        x = bilanBathy(k2).x;
        hc = PlotUtils.createSScPlot(x, QF); grid on; hold on;
        set(hc, 'Color', Color)
    end
end
strLegend = bilanBathy(1).nomZoneEtude;

FigUtils.createSScFigure(fig);
legend(strLegend, 'interpreter', 'none')
