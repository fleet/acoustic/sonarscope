function b = EI_write_Images(aAcrossDist, xEIImage, typePolarEchogram, ...
    Tag, Carto, nomFicAll, nomFicRaw, SonarPortMode_1, ...
    EIReflecImage, EINbPointsImage, EIAnglesImage, ...
    EIAlongDistImage, Unit, Where, varargin)

[varargin, EITxBeamIndexSwathImage] = getPropertyValue(varargin, 'TxBeamIndexSwathImage', []); %#ok<ASGLU>

subAcross = 1:size(EIReflecImage,1);
y = 1:length(subAcross);
GeometryType = cl_image.indGeometryType('PingAcrossDist');
ImageName = strrep(nomFicRaw, '_PolarEchograms', '');
TagSynchroX = [ImageName ' - PingAcrossDist'];
TagSynchroY = ImageName;
TagSynchroX = strrep(TagSynchroX, '_Raw', '');
TagSynchroX = strrep(TagSynchroX, '_Comp', '');
TagSynchroY = strrep(TagSynchroY, '_Raw', '');
TagSynchroY = strrep(TagSynchroY, '_Comp', '');

if typePolarEchogram == 1 % Raw
    ImageName = [ImageName '_raw'];
else
    ImageName = [ImageName '_comp'];
end

switch Where
    case 'ASC'  % Over specular
        ImageName = [ImageName '_ASC'];
    otherwise  % Over seafloor
        ImageName = [ImageName '_ASF'];
end

DataType = cl_image.indDataType('Reflectivity');
b(1) = cl_image('Image', EIReflecImage, 'Name', ImageName, 'Unit', Unit, ...
    'x', xEIImage, 'y', y, 'xUnit', 'm', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

DataType = cl_image.indDataType('AveragedPtsNb');
b(2) = cl_image('Image', EINbPointsImage, 'Name', ImageName, ...
    'x', xEIImage, 'y', y, 'xUnit', 'm', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

DataType = cl_image.indDataType('TxAngle');
b(3) = cl_image('Image', EIAnglesImage, 'Name', ImageName, 'Unit', 'deg', ...
    'x', xEIImage, 'y', y, 'xUnit', 'm', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

DataType = cl_image.indDataType('AlongDist');
b(4) = cl_image('Image', EIAlongDistImage, 'Name', ImageName, 'Unit', 'm', ...
    'x', xEIImage, 'y', y, 'xUnit', 'm', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

if ~isempty(EITxBeamIndexSwathImage) % Cas des sondeurs KM
    DataType = cl_image.indDataType('TxBeamIndexSwath');
    b(5) = cl_image('Image', EITxBeamIndexSwathImage, 'Name', ImageName, 'Unit', '#', ...
        'x', xEIImage, 'y', y, 'xUnit', 'm', 'yUnit', 'Ping', ...
        'DataType', DataType, 'GeometryType', GeometryType);
end

SonarRawDataResol       = get(aAcrossDist, 'SonarRawDataResol');
SonarResolutionD        = get(aAcrossDist, 'SonarResolutionD');
SonarTime               = get(aAcrossDist, 'SonarTime');
SonarImmersion          = get(aAcrossDist, 'SonarImmersion');
SonarHeight             = get(aAcrossDist, 'SonarHeight');
SonarHeading            = get(aAcrossDist, 'SonarHeading');
SonarRoll               = get(aAcrossDist, 'SonarRoll');
SonarPitch              = get(aAcrossDist, 'SonarPitch');
SonarSurfaceSoundSpeed  = get(aAcrossDist, 'SonarSurfaceSoundSpeed');
SonarHeave              = get(aAcrossDist, 'SonarHeave');
SonarPingCounter        = get(aAcrossDist, 'SonarPingCounter');
SonarSampleFrequency    = get(aAcrossDist, 'SonarSampleFrequency');
SonarFishLatitude       = get(aAcrossDist, 'SonarFishLatitude');
SonarFishLongitude      = get(aAcrossDist, 'SonarFishLongitude');
SonarInterlacing        = get(aAcrossDist, 'SonarInterlacing');
SonarPresenceWC         = get(aAcrossDist, 'SonarPresenceWC');
SonarPresenceFM         = get(aAcrossDist, 'SonarPresenceFM');
SonarDistPings          = get(aAcrossDist, 'SonarDistPings');
SonarNbSwaths           = get(aAcrossDist, 'SonarNbSwaths');

SonarDescription = get_SonarDescription(aAcrossDist);

SonarTime = SonarTime.timeMat;
SonarTime = cl_time('timeMat', SonarTime(subAcross));

if isempty(Tag)
    Tag = 'EI';
else
    Tag = [Tag '_EI'];
end
for k=1:length(b)
    b(k) = set_Carto(b(k), Carto);
    b(k) = set_SonarDescription(b(k), SonarDescription);
    b(k) = set(b(k), 'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY);
    b(k) = set(b(k), 'SonarRawDataResol',   SonarRawDataResol);
    b(k) = set(b(k), 'SonarResolutionD',    SonarResolutionD);
    b(k) = set(b(k), 'SonarResolutionX',    SonarResolutionD);
    
    b(k).InitialFileName   = nomFicAll;
    b(k).InitialFileFormat = 'SimradAll';
    
    b(k) = set_SonarTime(b(k),              SonarTime);
    b(k) = set_SonarImmersion(b(k),         SonarImmersion(subAcross));
    b(k) = set_SonarHeight(b(k),            SonarHeight(subAcross));
    b(k) = set_SonarHeading(b(k),           SonarHeading(subAcross));
    b(k) = set_SonarRoll(b(k),              SonarRoll(subAcross));
    b(k) = set_SonarPitch(b(k),             SonarPitch(subAcross));
    b(k) = set_SonarSurfaceSoundSpeed(b(k), SonarSurfaceSoundSpeed(subAcross));
    b(k) = set_SonarHeave(b(k),             SonarHeave(subAcross));
    b(k) = set_SonarPingCounter(b(k),       SonarPingCounter(subAcross));
    b(k) = set_SonarSampleFrequency(b(k),   SonarSampleFrequency(subAcross));
    b(k) = set_SonarFishLatitude(b(k),      SonarFishLatitude(subAcross));
    b(k) = set_SonarFishLongitude(b(k),     SonarFishLongitude(subAcross));
    b(k) = set_SonarPortMode_1(b(k),        SonarPortMode_1(subAcross));
    if ~isempty(SonarInterlacing)
        b(k) = set_SonarInterlacing(b(k),   SonarInterlacing(subAcross));
    end
    if ~isempty(SonarPresenceWC)
        b(k) = set_SonarPresenceWC(b(k),    SonarPresenceWC(subAcross));
    end
    if ~isempty(SonarPresenceFM)
        b(k) = set_SonarPresenceFM(b(k),    SonarPresenceFM(subAcross));
    end
    if ~isempty(SonarDistPings)
        b(k) = set_SonarDistPings(b(k),     SonarDistPings(subAcross));
    end
    if ~isempty(SonarNbSwaths)
        b(k) = set_SonarNbSwaths(b(k),      SonarNbSwaths(subAcross));
    end
    
    b(k) = update_Name(b(k), 'Append', Tag);
end