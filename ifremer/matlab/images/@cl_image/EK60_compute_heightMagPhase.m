function [flag, this] = EK60_compute_heightMagPhase(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contrôles

flag = isSubbottom(this(indImage));
if ~flag
    return
end

%% Recherche du layer de phase latérale % TODO : ramener cette recherche dans

identLayerPhase = cl_image.indDataType('SonarAcrossBeamAngle');
[flag, indLayerPhase, nomsLayers] = listeLayersSynchronises(this, indImage, 'TypeDonneUniquement', identLayerPhase);
if ~flag
    return
end

switch length(indLayerPhase)
    case 0
        str1 = 'Il faut un layer de phase';
        str2 = 'A Phase layer is required';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    case 1
    otherwise
        str1 = 'Confirmation image conditionnelle';
        str2 = 'Conditional image agreement';
        [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single', 'InitialValue', length(nomsLayers));
        if ~flag
            return
        end
        indLayerPhase = indLayerPhase(choix);
end

%% Algorithme

if isempty(this(indImage).Sonar.Height)
    this(indImage).Sonar.Height = zeros(1, length(this(indImage).y));
end

x = this(indImage).x(subx);
I = this(indImage).Image(suby, subx);
if ~isa(I, 'double')
    I = single(I);
end

P = this(indLayerPhase).Image(suby, subx);
if ~isa(P, 'double')
    P = single(P);
end

AmpImage   = I';
PhaseImage = P';

%% Extract backscatter of EK60 data

SampleRate = this(indImage).Sonar.SampleFrequency(1);
[RangeDetection, BS, H1Gate, H2Gate] = ExtractBackscatterMagPhaseEK60(AmpImage, PhaseImage, SampleRate);
RangeDetection = RangeDetection + (x(1) - 1);
%{
H1Gate = H1Gate + (x(1) - 1);
H2Gate = H2Gate + (x(1) - 1);

FigUtils.createSScFigure; PlotUtils.createSScPlot(RangeDetection, 'b'); grid on; hold on;
PlotUtils.createSScPlot(H1Gate, 'k');  PlotUtils.createSScPlot(H2Gate, 'k')
FigUtils.createSScFigure; PlotUtils.createSScPlot(BS, 'b'); grid on;
%}


%% Sauvegarde du signal dans le cache : TODO : en attendant de savoir

% TODO : A AMELIORER DE TOUTE URGENCE

[~, ~, ext] = fileparts(this(indImage).InitialFileName);
switch lower(ext)
    case '.raw'
        a = cl_ExRaw('nomFic', this(indImage).InitialFileName);
end

flag = save_Height(a, RangeDetection);
if ~flag
    return
end

%% Sauvegarde du signal dans l'instance

if size(this(indImage).Sonar.Height, 2) == 2
    this(indImage).Sonar.Height(suby,2) = NaN;
end
this(indImage).Sonar.Height(suby,1)      = RangeDetection(:);
this(indLayerPhase).Sonar.Height(suby,1) = RangeDetection(:);
