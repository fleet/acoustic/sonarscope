function flag = ERS_Export23DViewer(I0, listeFic, repExport, Tag)

if ~iscell(listeFic)
    listeFic = {listeFic};
end

N = length(listeFic);
str1 = 'Export vers GLOBE des fichiers ERS';
str2 = 'Export of ERS files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [flag, this] = cl_image.import_ermapper(listeFic{k});
    if ~flag
        continue
    end
    
    flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
    if ~flag
        continue
    end
    
%     if this.DataType == identBathymetry
        % Interpolation
        window = [11 11];
        this = WinFillNaN(this, 'window', window);
        
        % Filtrage gaussien
        sigma = 1;
        this = filterGauss(this, 'sigma', sigma);
%     end
    
    [~, nomFic] = fileparts(listeFic{k});
    if isempty(Tag)
        FileName3DV = nomFic;
    else
        FileName3DV = [nomFic '_' Tag];
    end
    flag = exportGeotiffSonarScope3DViewerMulti(this, 'sizeTiff', 2500, 'repExport', repExport, 'FileName3DV', FileName3DV);
end
my_close(hw, 'MsgEnd')
