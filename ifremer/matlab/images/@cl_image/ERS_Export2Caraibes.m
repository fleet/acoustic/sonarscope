function flag = ERS_Export2Caraibes(I0, listeFic, repExport, Tag)

if ~iscell(listeFic)
    listeFic = {listeFic};
end

N = length(listeFic);
str1 = 'Export vers GLOBE des fichiers ERS';
str2 = 'Export of ERS files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [flag, this] = cl_image.import_ermapper(listeFic{k});
    if ~flag
        continue
    end
    
    [~, nomFic] = fileparts(listeFic{k});
    if isempty(Tag)
        nomFicCar = fullfile(repExport, nomFic);
    else
        nomFicCar = fullfile(repExport, [nomFic '_' Tag]);
    end
%     flag = export_CaraibesOLD(this, nomFicCar);

    flag = export_Caraibes(this, 1, repExport, 'FileName', nomFicCar);
end
my_close(hw, 'MsgEnd')
