function flag = ERS_Export2GoogleEarth(I0, listeFic, repExport, Tag, TypeOmbrage, Azimuth, VertExag, skipExistingFile)

identBathymetry = cl_image.indDataType('Bathymetry');

if ~iscell(listeFic)
    listeFic = {listeFic};
end

N = length(listeFic);
str1 = 'Export des fichiers ers dans Google-Earth';
str2 = 'Export of ers files to Google-Earth';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [nomDir, nomFic] = fileparts(listeFic{k});
    if isempty(Tag)
        nomFicKmz = fullfile(repExport, [nomFic '.kmz']);
    else
        nomFicKmz = fullfile(repExport, [nomFic '_' Tag '.kmz']);
    end
    
    if skipExistingFile && exist(nomFicKmz, 'file')
        continue
    end
    
    nomDir = fullfile(nomDir, nomFic);
    if ~exist(nomDir, 'dir')
        str1 = sprintf('Le r�pertoire "%s" n''existe pas.', nomDir);
        str2 = sprintf('Directory "%s" does not exist.', nomDir);
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    [flag, this] = cl_image.import_ermapper(listeFic{k});
    if ~flag
        continue
    end
    
    flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
    if ~flag
        continue
    end
    
    if this.DataType == identBathymetry
        % Interpolation
        window = [5 5];
        this = WinFillNaN(this, 'window', window);
        
        % Filtrage gaussien
        sigma = 1;
        this = filterGauss(this, 'sigma', sigma);

        switch TypeOmbrage
            case 4 %  % 3 : ombrage N&B avec azimut, 4=ombrage N&B avec greatestSlopes
                this = sunShadingGreatestSlopeGray(this, VertExag); % , 'Silent', Silent);
%                 [this, flag] = sunShadingGreatestSlope(this, VertExag); % TODO : je pense que c'est plut�t cette fonction qu'il faut appeler ici
        end
    end

    
    valMask0 = 0;
    flag = export_GoogleEarth(this, 'nomFic', nomFicKmz, 'valMask0', valMask0);
    
    if flag
        winopen(nomFicKmz)
    end
end
my_close(hw, 'MsgEnd')

flag = 1;

