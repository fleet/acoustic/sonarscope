% Speckle filtering of an image
%
% Syntax
%   b = FilterImageTbx(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   window    : Sliding window size(Default : [7 7])
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = FilterImageTbx(a);
%   imagesc(b);
%
% See also cl_image/filterGauss Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = FilterImageTbx(this, hFunction, varargin)

N = length(this);
hw = create_waitbar(waitbarMsg('FilterImageTbx'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    this(k) = FilterImageTbx_unit(this(k), hFunction, varargin{:});
    if N > 1
        this(k) = optimiseMemory(this(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = FilterImageTbx_unit(this, hFunction, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);
[varargin, window] = getPropertyValue(varargin, 'window', [3 3]); %#ok<ASGLU>

%% Conversion éventuelle en image d'Intensité

if this.ImageType == 2 % RGB
    this = RGB2Intensity(this);
elseif this.ImageType == 3 % Indexed
    this = Indexed2Intensity(this);
end

%% Algorithm

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    hHood = true(window);
    
    switch func2str(hFunction)
        case 'rangefilt'
            subNaN = isnan(I);
            I(subNaN) = min(I(~subNaN));
        case 'entropyfilt'
            I = mat2ZeroOne(I, this.CLim);
    end
    
    I = hFunction(double(I), hHood);
    Image(:,:,k) = I;
end

%% Output image

Parameters = getHistoryParameters(window);
that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', func2str(hFunction), ...
    'Parameters', Parameters, 'ImageType', 1);
