% Transformation d'une image metrique (x,y en m) en image geometrique (lat,lon)
%
% Syntax
%   b = GeoYX2LatLong(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   b      : instance de cl_image
%   status : 1=Operation reussie, 0=operation non reussie
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_125m.mnt');
%   b = cl_car_ima(nomFic);
%   c = view(b, 'Sonar.Ident', 1);
%   a = get(c, 'Images');
%   imagesc(a)
%
%   [b, status] = GeoYX2LatLong(a);
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, status] = GeoYX2LatLong(this, varargin)

[varargin, NoWaitbar] = getPropertyValue(varargin, 'NoWaitbar', 0);

nbImages = length(this);
if ~NoWaitbar && (nbImages > 1)
    NoWaitbar = 1;
end
str1 = 'Traitement "GeoYX2LatLong"';
str2 = 'Processing "GeoYX2LatLong"';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    [this(k), status] = GeoYX2LatLong_unitaire(this(k), NoWaitbar, varargin{:});
    if nbImages > 1
        this(k) = optimiseMemory(this(k), 'SizeMax', 500000);
    end
end
my_close(hw, 'MsgEnd')


function [this, status] = GeoYX2LatLong_unitaire(this, NoWaitbar, varargin)

global SizePhysTotalMemory %#ok<GVMIS> 

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, GridStep]  = getPropertyValue(varargin, 'GridStep',  []); %#ok<ASGLU>

%% Controles de validite

status = 0;
GeometryType = this.GeometryType;

if GeometryType == cl_image.indGeometryType('LatLong')
    str1 = 'L''image est d�j� en g�om�trie "LatLong".';
    str2 = 'This image is already in "LatLong" geometry.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if GeometryType ~= cl_image.indGeometryType('GeoYX')
    str1 = 'La g�om�trie de l''image fournie n''est pas de type "GeoYX".';
    str2 = 'The geometry of this image is not a "GeoYX" one.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

Carto = this.Carto;
if isempty(Carto)
    str1 = 'La cartographie de l''image fournie n''est pas d�finie';
    str2 = 'The cartography of this image is unknown.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
if get(Carto, 'Projection.Type') == 1
    str1 = 'La projection cartographique de l''image fournie n''est pas d�finie.';
    str2 = 'The cartographic projection of this image is not defined.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
if get(Carto, 'Ellipsoide.Type') == 1
    str1 = 'L''ellipsoide de l''image fournie n''est pas d�finie.';
    str2 = 'The ellipsoid of this image is not defined.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Passage des coordonn�es m�triques en coordonn�es g�om�triques

[X1, Y1] = meshgrid(this.x(subx), this.y(suby));

%{
[Y1_Car, X1_Car] = xy2latlon(Carto, X1, Y1);
[Y1_Test, X1_Test] = xy2latlon_IGN(Carto, X1, Y1);
SonarScope(X1_Car-X1_Test)
SonarScope(Y1_Car-Y1_Test)
X1 = X1_Car;
Y1 = Y1_Car;
%}

% str1 = 'Calcul des coordonn�es des pixels';
% str2 = 'Computing the pixel coordinates';
% WorkInProgress(Lang(str1,str2))

[Y1, X1] = xy2latlon(Carto, X1, Y1); % On reprend les m�me variables pour ne pas gaspiller d'espace m�moire,
% X1 contient maintenant des longitudes et Y1 des latitudes
if isempty(X1)
    return
end
LatNord = max(Y1(:));
LatSud  = min(Y1(:));
LonWest = min(X1(:));
LonEst  = max(X1(:));

if (LonWest < -180) || (LonWest> 180) || (LonEst < -180) || (LonEst> LonEst)
    str1 = 'Les coordonn�es g�ographiques obtenues ne semblent pas valides. Etes-vous s�r de la projection ?';
    str2 = 'The geographic coordinates does not seem to be correct, are you sure of the projection ?';
    my_warndlg(Lang(str1,str2), 1);
    %     return
end

% Test pour savoir si on a assez de m�moire
if (numel(X1)*4*8) <  (SizePhysTotalMemory*1000)
    flagRAM = 1;
else
    if size(X1, 1) == size(X1, 2) % bidouille JMA pour �viter transposition matrices carr�es,
        flagRAM = 1;
    else
        flagRAM = 0;
    end
end

% if numel(X1) > 2000^2 % TODO : faire qualque chose de mieuw que �a : test m�moire physique / 20  ?
if ~flagRAM
    X1 = cl_memmapfile('Value', X1);
    Y1 = cl_memmapfile('Value', Y1);
end

%{
strLatNorth = lat2str(LatNord);
strLonSouth = lon2str(LatSud);
strLatWest  = lat2str(LonWest);
strLonEast  = lon2str(LonEst);
disp(sprintf('Latitude Nord   = %f\t: %s', LatNord, strLatNorth));
disp(sprintf('Latitude Sud    = %f\t: %s', LatSud,  strLonSouth));
disp(sprintf('Longitude Ouest = %f\t: %s', LonWest, strLatWest));
disp(sprintf('Longitude Est   = %f\t: %s', LonEst,  strLonEast));
%}

%% Calcul du pas de la grille en coordonn�es g�ographiques

if isempty(GridStep)
    PasMetrique = min(this.XStep, this.YStep);
else
    PasMetrique = GridStep;
end
RayonTerre = get(Carto, 'Ellipsoide.DemiGrandAxe');
deltaY1 = PasMetrique / (RayonTerre * (pi/180));
deltaX1 = deltaY1 / cosd(mean(Y1(:,1)));

%% Calcul des coordonn�es g�ographiques

Lat = LatSud:deltaY1:LatNord;
Lon = LonWest:deltaX1:LonEst;

if isempty(Lat) || isempty(Lon)
    str1 = 'Les coordonn�es g�ographiques obtenues ne sont pas valides. Etes-vous s�r de la projection ?';
    str2 = 'The geographic coordinates are not correct, are you sure of the projection ?';
    my_warndlg(Lang(str1,str2), 1);
    return
end

[Lat, deltaY1, LatSud, LatNord] = centrage_magnetique(Lat); %#ok
[Lon, deltaX1, LonWest, LonEst] = centrage_magnetique(Lon); %#ok

if this.ImageType == 3
    Method = 'nearest';
else
    Method = 'linear';
end

%% Interpolation de l'image

ClassName = class(this.Image(1));
sizeJ = [length(Lat) length(Lon) this.nbSlides];
switch ClassName
    case {'uint8'; 'int8'; 'uint16'; 'int16'; 'uint32'; 'int32'}
        try
            J = zeros(sizeJ, ClassName);
        catch %#ok<CTCH>
            J = cl_memmapfile(0, 'Size', sizeJ, 'Format', ClassName);
        end
        ValNaN = 0;
    otherwise
        try
            J = NaN(sizeJ, ClassName);
        catch %#ok<CTCH>
            J = cl_memmapfile(NaN, 'Size', sizeJ, 'Format', ClassName);
        end
        ValNaN = NaN;
end

for kSlide=1:this.nbSlides
    switch ClassName
        case {'double'; 'single'}
            A =  this.Image(suby,subx,kSlide);
        otherwise
            A =  single(this.Image(suby,subx,kSlide));
            if ~isnan(this.ValNaN)
                A(A == this.ValNaN) = NaN;
            end
    end
    if strcmp(Method, 'linear')
        A = fillNaN(A, [3 3]);
    end
    if ~flagRAM
        A = cl_memmapfile('Value', A);
    end
    
%     WorkInProgress(['Gridding ' this.Name])
    Z = gridding(X1, Y1, A, Lon, Lat, 'Method', Method, 'NoWaitbar', NoWaitbar);
    
    if strcmp(ClassName, 'uint8') %#ok<STISA>
        switch this.ImageType
            case 1
                subNaN = isnan(Z);
                Z = uint8(Z);
                Z(subNaN) = this.ValNaN;
                clear subNaN
            case 2
                Z = uint8(Z);
        end
    end
    
    J(:,:,kSlide) = Z;
    clear Z
end
clear X1 Y1 A

%% Remise � jour des descripteurs de l'image

this.ValNaN = ValNaN;
this = replace_Image(this, J);
this.x            = Lon;
this.y            = Lat;
this.XLim         = compute_XYLim(this.x);
this.YLim         = compute_XYLim(this.y);
this.GeometryType = cl_image.indGeometryType('LatLong');
this.TagSynchroX  = 'Longitude';
this.TagSynchroY  = 'Latitude';
this.XUnit        = 'deg';
this.YUnit        = 'deg';

%% Compl�tion du titre

this = update_Name(this);
Titre = this.Name;

%% Interpolation

if strcmp(Method, 'linear')
    this = WinFillNaN(this, 'window', [3 3]);% TODO : vaudrait mieux essayer un filtre interpolateur gaussien
end
this.Name = Titre;

%% Apparemment tout s'est bien pass�

status = 1;
