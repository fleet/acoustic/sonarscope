function [flag, SeuilNbBeams] = GetParams_SonarMBES_MasqueNbBeams(this)

NbBeams = this.nbColumns;
 
str1 = 'TODO';
str2 = sprintf('Masking MBES data using the number of valid beams (not NaN values)');
[flag, SeuilNbBeams] = inputOneParametre(Lang(str1,str2), 'Number of beams to validate a ping', ...
    'Value', NbBeams, 'Unit', 'm', 'MinValue', 1, 'MaxValue', NbBeams);
if ~flag
    return
end
