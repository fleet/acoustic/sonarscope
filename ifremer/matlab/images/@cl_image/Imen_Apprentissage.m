% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_calcul_newThese(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = Imen_Apprentissage(this, I, Indices, binsImage, LayerAngle, binsLayerAngle, ...
    nomCourbe, commentaire, numTexture, nomTexture, varargin)

[varargin, valMask]      = getPropertyValue(varargin, 'valMask', []);
[varargin, depMaxClique] = getPropertyValue(varargin, 'depMaxClique', []);
[varargin, Coul]         = getPropertyValue(varargin, 'Coul', []); %#ok<ASGLU>

if isempty(depMaxClique)
    depMaxClique  = 10;
end

% -------------------------------------------------------------
% Lecture et Quantification de l'image conditionnelle angulaire

if isempty(LayerAngle)
    binsLayerAngle = 1;
    DataType_LayerAngle = [];
else
    DataType_LayerAngle = LayerAngle.DataType;
end

% ------------------------------------------
% Apprentissage des matrices de cooccurrence

Cliques = zeros((depMaxClique+1)^2,2);
k = 1;
for i=0:depMaxClique
    for j=0:depMaxClique
        Cliques(k,:) = [i j];
        k = k + 1;
    end
end


CoefsGabor = getCoefGabor;
% nbValHist = 20;
nbValHist = 100;
[GaborModeleAngle, GaborMaxValue] = apprentissage_Gabor(I, Indices, binsLayerAngle,  nbValHist, CoefsGabor, binsImage.NbNiveaux);


CmatModeleAngle = apprentissage_coocMat(I, Indices, binsLayerAngle, Cliques, binsImage.NbNiveaux);

this.Texture(numTexture).nomTexture             = nomTexture;
this.Texture(numTexture).binsImage              = binsImage;
this.Texture(numTexture).binsLayerAngle         = binsLayerAngle;
this.Texture(numTexture).DataType_LayerAngle    = DataType_LayerAngle;
this.Texture(numTexture).DataType_Image         = this.DataType;

this.Texture(numTexture).nomCourbe{valMask}   = nomCourbe;
this.Texture(numTexture).commentaire{valMask} = commentaire;
this.Texture(numTexture).Coul(valMask,:)      = Coul;

this.Texture(numTexture).w                          = [];
this.Texture(numTexture).cliques                    = Cliques;
this.Texture(numTexture).CmatModeleAngle{valMask} = CmatModeleAngle;
this.Texture(numTexture).Gabor.Param                = CoefsGabor;
this.Texture(numTexture).Gabor.Hist{valMask}      = GaborModeleAngle;
this.Texture(numTexture).Gabor.MaxValue{valMask}  = GaborMaxValue;


% --------------------------------------------------------------------------------------------
function CmatModeleAngle = apprentissage_coocMat(I, Angle, binsLayerAngle, Cliques, NbNiveaux)

if isequal(binsLayerAngle,1)
    nbBinsAngles = 2;
    nbCliques = size(Cliques,1);
    CmatModeleAngle = cell(nbCliques, nbBinsAngles-1);
else
    nbBinsAngles = length(binsLayerAngle);
    nbCliques = size(Cliques,1);
    CmatModeleAngle = cell(nbCliques, nbBinsAngles-1);
end

hw = create_waitbar(Lang('Apprentissage en cours', 'Learning signatures'), 'N', nbBinsAngles);
for iAngle=1:nbBinsAngles-1
    my_waitbar(iAngle, nbBinsAngles, hw)
    
    masqueAngle = single((Angle == iAngle));
    [Ia, Ja] = find(masqueAngle);
    masqueAngle(~masqueAngle) = NaN;
    
    minIa = min(Ia);
    maxIa = max(Ia);
    
    minJa = min(Ja);
    maxJa = max(Ja);
    
    subIa = minIa:maxIa;
    subJa = minJa:maxJa;
    
    II = I(subIa, subJa) .* masqueAngle(subIa, subJa);
    
    if isempty(II)
        CmatModeleAngle(:,iAngle) = {[]};
    else
        %         if isempty(find(~isnan(II), 1))
        if ~any(~isnan(II))
            CmatModeleAngle(:,iAngle) = {[]};
        else
            for iClique=1:nbCliques
                CmatModeleAngle(iClique,iAngle) = learnSegmMat({II}, Cliques(iClique,1), Cliques(iClique,2), NbNiveaux);
            end
        end
    end
end
my_close(hw, 'MsgEnd')

if nargout == 0
    figure;
    k = 1;
    for iClique=1:nbCliques
        for iAngle=1:nbBinsAngles-1
            subplot(nbCliques, nbBinsAngles-1, k);
            imagesc(CmatModeleAngle{iClique,iAngle}); axis xy; axis off;
            k = k + 1;
        end
    end
end

function V = getCoefGabor

%{
Teta = (0:7) * (180 / 8);
Teta = [0 22.5 45 67.5 90 112.5 135 157.5]
Teta = [0 22.5 45 90 135]

f0 = [4 8 16] * sqrt(2);
Sigma = [4 8 16 32] * sqrt(2)


figure;
for iTeta=1:length(Teta)
subplot(2,1,1)
for if0=1:length(f0)
plot(Teta(iTeta), f0(if0), '+b'); grid on; hold on; xlabel('Teta'), ylabel('f0');
end
subplot(2,1,2)
for iSigma=1:length(Sigma)
plot(Teta(iTeta), Sigma(iSigma), '+b'); grid on; hold on; xlabel('Teta'), ylabel('Sigma');
end
end
%}

NF = 50;
V = zeros(NF,3);
V(1, :) = [0.2500*sqrt(2),   0, 4*sqrt(2)];
V(2, :) = [0.2500*sqrt(2),  25, 4*sqrt(2)];
V(3, :) = [0.2500*sqrt(2),  45, 4*sqrt(2)];
V(4, :) = [0.2500*sqrt(2),  90, 4*sqrt(2)];
V(5, :) = [0.2500*sqrt(2), 135, 4*sqrt(2)];
V(6, :) = [0.1250*sqrt(2),   0, 8*sqrt(2)];
V(7, :) = [0.1250*sqrt(2),  25, 8*sqrt(2)];
V(8, :) = [0.1250*sqrt(2),  45, 8*sqrt(2)];
V(9, :) = [0.1250*sqrt(2),  90, 8*sqrt(2)];
V(10,:) = [0.1250*sqrt(2), 135, 8*sqrt(2)];
V(11,:) = [0.0625*sqrt(2),  0, 16*sqrt(2)];
V(12,:) = [0.0625*sqrt(2), 25, 16*sqrt(2)];
V(13,:) = [0.0625*sqrt(2), 45, 16*sqrt(2)];
V(14,:) = [0.0625*sqrt(2), 90, 16*sqrt(2)];
V(15,:) = [0.0625*sqrt(2), 135, 16*sqrt(2)];
V(16,:) = [0.0313*sqrt(2),   0, 32*sqrt(2)];
V(17,:) = [0.1250*sqrt(2),   0,  2*sqrt(2)];
V(18,:) = [0.1250*sqrt(2),  25,  2*sqrt(2)];
V(19,:) = [0.1250*sqrt(2),  45,  2*sqrt(2)];
V(20,:) = [0.1250*sqrt(2), 135,  2*sqrt(2)];

V(21,:) = [0.125*sqrt(2), 0, 5*sqrt(2)];
V(22,:) = [0.125*sqrt(2), 25, 5*sqrt(2)];
V(23,:) = [0.125*sqrt(2), 45, 5*sqrt(2)];
V(24,:) = [0.125*sqrt(2), 90, 5*sqrt(2)];
V(25,:) = [0.125*sqrt(2), 135, 5*sqrt(2)];
V(26,:) = [0.5, 0, 6*sqrt(2)];
V(27,:) = [0.5, 25, 6*sqrt(2)];
V(28,:) = [0.5, 45, 6*sqrt(2)];
V(29,:) = [0.5, 90, 6*sqrt(2)];
V(30,:) = [0.5, 135, 6*sqrt(2)];

V(31,:) = [0.0025*sqrt(2),   0,  4*sqrt(2)];
V(32,:) = [0.0025*sqrt(2),  25,  4*sqrt(2)];
V(33,:) = [0.0025*sqrt(2),  45,  4*sqrt(2)];
V(34,:) = [0.0025*sqrt(2),  90,  4*sqrt(2)];
V(35,:) = [0.0025*sqrt(2),  135, 4*sqrt(2)];
V(36,:) = [0.00125*sqrt(2),   0, 8*sqrt(2)];
V(37,:) = [0.00125*sqrt(2),  25, 8*sqrt(2)];
V(38,:) = [0.00125*sqrt(2),  45, 8*sqrt(2)];
V(39,:) = [0.00125*sqrt(2),  90, 8*sqrt(2)];
V(40,:) = [0.00125*sqrt(2), 135, 8*sqrt(2)];

V(41,:) = [0.0625*sqrt(2),   0, 20*sqrt(2)];
V(42,:) = [0.0625*sqrt(2),  25, 20*sqrt(2)];
V(43,:) = [0.0625*sqrt(2),  45, 20*sqrt(2)];
V(44,:) = [0.0625*sqrt(2),  90, 20*sqrt(2)];
V(45,:) = [0.0625*sqrt(2), 135, 20*sqrt(2)];
V(46,:) = [0.0313*sqrt(2),   0, 20*sqrt(2)];
V(47,:) = [0.0313*sqrt(2),  45, 20*sqrt(2)];
V(48,:) = [0.0313*sqrt(2),  90, 20*sqrt(2)];
V(49,:) = [0.0313*sqrt(2), 135, 20*sqrt(2)];
V(50,:) = [0.0313*sqrt(2), 135, 20*sqrt(2)];

V(:,2) = V(:,2) * (pi / 180);

% --------------------------------------------------------------------------------------------
function [GaborModeleAngle, GaborMaxValue] = apprentissage_Gabor(I, Angle, binsLayerAngle, nbValHist, CoefsGabor, NbNiveaux)

NF = size(CoefsGabor, 1);
% WGabor  = 32*ones(NF,1);
WGabor  = 32;
WGaborS2 = floor(WGabor/2);

% for j=1:NF
%     g = gfcreatefilter2(CoefsGabor(j,1), CoefsGabor(j,2), CoefsGabor(j,3), CoefsGabor(j,3), WGabor(j));
%     figure(1245); imagesc(abs(g)); colorbar; title(sprintf('%d', j))
%     drawnow
% end

%{
return

Xga = zeros(K,NF,MM,NN,Ng2);
cga = zeros(K,NF,Ng2);
for j=1:NF
g = gfcreatefilter2(V(j,1),V(j,2),V(j,3),V(j,3),WGabor(j));
for kk=1:K
II = double(I(:,:,kk));
II = reshape(II,m,n);
GG1 = filter2(g,II);
GG1 = real(GG1) .^ 2 + imag(GG1) .^ 2;
GG1 = (GG1-nng(j))/mmg(j);
GG1 = GG1 .* Ng2;
for ii=1:MM
for jj=1:NN
dd = GG1((ii-1)*Tx+1:ii*Tx,(jj-1)*Ty+1:jj*Ty);
ccc = hist_imen(dd(:),Ng);
Xga(kk, j, ii, jj, 1:Ng2) = ccc;
end
end
ccc = hist_imen(GG1(:),Ng);
cga(kk,j,1:Ng2) = ccc;
end
end
%}


if isequal(binsLayerAngle,1)
    nbBinsAngles = 2;
else
    nbBinsAngles = length(binsLayerAngle);
end
GaborModeleAngle = cell(NF, nbBinsAngles-1);
GaborMaxValue    = zeros(NF, nbBinsAngles-1);

hw = create_waitbar(Lang('Apprentissage en cours', 'Learning signatures'), 'N', nbBinsAngles-1);
for iAngle=1:nbBinsAngles-1
    if nbBinsAngles > 2
        my_waitbar(iAngle, nbBinsAngles, hw)
    end
    
    masqueAngle = single((Angle == iAngle));
    [Ia, Ja] = find(masqueAngle);
    masqueAngle(~masqueAngle) = NaN;
    
    minIa = min(Ia);
    maxIa = max(Ia);
    
    minJa = min(Ja);
    maxJa = max(Ja);
    
    subIa = minIa:maxIa;
    subJa = minJa:maxJa;
    
    II = I(subIa, subJa) .* masqueAngle(subIa, subJa);
    
    if isempty(II)
        GaborModeleAngle(:,iAngle) = {[]};
    else
        if ~any(~isnan(II(:)))
            GaborModeleAngle(:,iAngle) = {[]};
        else
            K = all(isnan(II), 1);
            K = find(~K);
            subx = max(1,K(1)-WGaborS2):min(K(end)+WGaborS2, size(II,2));
            
            K = all(isnan(II), 2);
            K = find(~K);
            suby = max(1,K(1)-WGaborS2):min(K(end)+WGaborS2, size(II,2));
            
            JJ = II(suby,subx);
            for iFiltre=1:NF
                g = gfcreatefilter2(CoefsGabor(iFiltre,1), CoefsGabor(iFiltre,2), CoefsGabor(iFiltre,3), CoefsGabor(iFiltre,3), WGabor);
                
                GG1 = filter2(g, JJ);
                %                 GG1 = real(GG1) .^ 2 + imag(GG1) .^ 2;
                GG1 = abs(GG1);
                GG1 = log(GG1);
                %                 mmg = max(GG1(:));
                %                 nng = min(GG1(:));
                %                 GG1 = (GG1-nng)/mmg; % Bizare, j'aurais divis� par max-min
                %                 GG1 = GG1 .* nbValHist;
                
                %                 maxGG1 = max(GG1(:));
                maxGG1 = NbNiveaux;
                GaborMaxValue(iFiltre,iAngle) = maxGG1;
                %                 GG1 = GG1 .* (100*nbValHist / maxGG1);
                %                 GG1 =(GG1 + 100) * 5;
                GG1 =(GG1 + 20) * (500/140);
                
                figure(1386); subplot(2,1,1); imagesc(GG1); colorbar; title(sprintf('%d', iFiltre)); drawnow
                
                ccc = Imen_histGabor(GG1, nbValHist);
                figure(1386); subplot(2,1,2); plot(ccc); grid on; title(sprintf('%d', iFiltre));
                
                GaborModeleAngle{iFiltre,iAngle} = ccc;
            end
        end
    end
end
my_close(hw, 'MsgEnd')

if nargout == 0
    figure;
    k = 1;
    for iFiltre=1:nbCliques
        for iAngle=1:nbBinsAngles-1
            subplot(nbCliques, nbBinsAngles-1, k);
            imagesc(GaborModeleAngle{iFiltre,iAngle}); axis xy; axis off;
            k = k + 1;
        end
    end
end
