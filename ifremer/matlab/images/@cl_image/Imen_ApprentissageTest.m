% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_calcul_newThese(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = Imen_ApprentissageTest(this, UseCooc, UseGabor, I, IGabor, flagGabor, ...
    Indices, binsImage, LayerAngle, binsLayerAngle, ...
    nomCourbe, commentaire, numTexture, nomTexture, varargin)

[varargin, valMask]      = getPropertyValue(varargin, 'valMask', []);
[varargin, depMaxClique] = getPropertyValue(varargin, 'depMaxClique', []);
[varargin, Coul]         = getPropertyValue(varargin, 'Coul', []);
[varargin, MinMaxGabor]  = getPropertyValue(varargin, 'MinMaxGabor', []); %#ok<ASGLU>

if isempty(depMaxClique)
    depMaxClique  = 10;
end

% -------------------------------------------------------------
% Lecture et Quantification de l'image conditionnelle angulaire

if isempty(LayerAngle)
    binsLayerAngle = 1;
    DataType_LayerAngle = [];
else
    DataType_LayerAngle = LayerAngle.DataType;
end

% ------------------------------------------
% Apprentissage des matrices de cooccurrence

Cliques = zeros((depMaxClique+1)^2,2);
k = 1;
for i=0:depMaxClique
    for j=0:depMaxClique
        Cliques(k,:) = [i j];
        k = k + 1;
    end
end


if UseGabor
    CoefsGabor = Imen_getCoefGabor4;
    nbValHist = 100;
    GaborModeleAngle = apprentissage_Gabor(I, IGabor, Indices, binsLayerAngle,  nbValHist, ...
        CoefsGabor, flagGabor, MinMaxGabor);
end

if UseCooc
    CmatModeleAngle = apprentissage_coocMat(I, Indices, binsLayerAngle, Cliques, binsImage.NbNiveaux);
end

this.Texture(numTexture).UseCooc  = UseCooc;
this.Texture(numTexture).UseGabor = UseGabor;

this.Texture(numTexture).nomTexture             = nomTexture;
this.Texture(numTexture).binsImage              = binsImage;
this.Texture(numTexture).binsLayerAngle         = binsLayerAngle;
this.Texture(numTexture).DataType_LayerAngle    = DataType_LayerAngle;
this.Texture(numTexture).DataType_Image         = this.DataType;

this.Texture(numTexture).nomCourbe{valMask}   = nomCourbe;
this.Texture(numTexture).commentaire{valMask} = commentaire;
this.Texture(numTexture).Coul(valMask,:)      = Coul;

this.Texture(numTexture).w                          = [];

if UseCooc
    this.Texture(numTexture).cliques                    = Cliques;
    this.Texture(numTexture).CmatModeleAngle{valMask} = CmatModeleAngle;
end

if UseGabor
    this.Texture(numTexture).Gabor.Param                = CoefsGabor;
    this.Texture(numTexture).Gabor.Hist{valMask}      = GaborModeleAngle;
    this.Texture(numTexture).Gabor.MinMaxGabor{valMask} = MinMaxGabor;
end


% --------------------------------------------------------------------------------------------
function CmatModeleAngle = apprentissage_coocMat(I, Angle, binsLayerAngle, Cliques, NbNiveaux)

if isequal(binsLayerAngle,1)
    nbBinsAngles = 2;
    nbCliques = size(Cliques,1);
    CmatModeleAngle = cell(nbCliques, nbBinsAngles-1);
else
    nbBinsAngles = length(binsLayerAngle);
    nbCliques = size(Cliques,1);
    CmatModeleAngle = cell(nbCliques, nbBinsAngles-1);
end

hw = create_waitbar(Lang('Apprentissage en cours', 'Learning signatures'), 'N', nbBinsAngles);
for iAngle=1:nbBinsAngles-1
    my_waitbar(iAngle, nbBinsAngles, hw)
    
    masqueAngle = single((Angle == iAngle));
    [Ia, Ja] = find(masqueAngle);
    masqueAngle(~masqueAngle) = NaN;
    
    minIa = min(Ia);
    maxIa = max(Ia);
    
    minJa = min(Ja);
    maxJa = max(Ja);
    
    subIa = minIa:maxIa;
    subJa = minJa:maxJa;
    
    II = I(subIa, subJa) .* masqueAngle(subIa, subJa);
    
    if isempty(II)
        CmatModeleAngle(:,iAngle) = {[]};
    else
        %         if isempty(find(~isnan(II), 1))
        if ~any(~isnan(II))
            CmatModeleAngle(:,iAngle) = {[]};
        else
            for iClique=1:nbCliques
                CmatModeleAngle(iClique,iAngle) = learnSegmMat({II}, Cliques(iClique,1), Cliques(iClique,2), NbNiveaux);
            end
        end
    end
end
my_close(hw, 'MsgEnd')

if nargout == 0
    figure;
    k = 1;
    for iClique=1:nbCliques
        for iAngle=1:nbBinsAngles-1
            subplot(nbCliques, nbBinsAngles-1, k);
            imagesc(CmatModeleAngle{iClique,iAngle}); axis xy; axis off;
            k = k + 1;
        end
    end
end



function GaborModeleAngle = apprentissage_Gabor(I, IGabor, Angle, binsLayerAngle, nbValHist, CoefsGabor, flagGabor, MinMaxGabor)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

NF = size(CoefsGabor, 1);
% WGabor  = 32*ones(NF,1);
WGabor  = 32; % ATTENTION, d�finition redondante, peur-�tre la mettre dans Imen_getCoefGabor
WGaborS2 = floor(WGabor/2);

if isequal(binsLayerAngle,1)
    nbBinsAngles = 2;
else
    nbBinsAngles = length(binsLayerAngle);
end
GaborModeleAngle = cell(NF, nbBinsAngles-1);

hw = create_waitbar(Lang('Apprentissage en cours', 'Learning signatures'), 'N', nbBinsAngles);
for iAngle=1:nbBinsAngles-1
    my_waitbar(iAngle, nbBinsAngles, hw)
    
    masqueAngle = single((Angle == iAngle));
    [Ia, Ja] = find(masqueAngle);
    masqueAngle(~masqueAngle) = NaN;
    
    minIa = min(Ia);
    maxIa = max(Ia);
    
    minJa = min(Ja);
    maxJa = max(Ja);
    
    subIa = minIa:maxIa;
    subJa = minJa:maxJa;
    
    II = I(subIa, subJa) .* masqueAngle(subIa, subJa);
    
    if isempty(II)
        GaborModeleAngle(:,iAngle) = {[]};
    else
        if ~any(~isnan(II(:)))
            GaborModeleAngle(:,iAngle) = {[]};
        else
            K = all(isnan(II), 1);
            K = find(~K);
            subx = max(1,K(1)-WGaborS2):min(K(end)+WGaborS2, size(II,2));
            
            K = all(isnan(II), 2);
            K = find(~K);
            suby = max(1,K(1)-WGaborS2):min(K(end)+WGaborS2, size(II,1));
            
            %             JJ = II(suby,subx);
            for iFiltre=1:NF
                if flagGabor(iFiltre)
                    GG1 = IGabor{iFiltre}(suby,subx);
                    GG1(isnan(II(suby,subx))) = NaN;
                    %                 g = gfcreatefilter2(CoefsGabor(iFiltre,1), CoefsGabor(iFiltre,2), CoefsGabor(iFiltre,3), CoefsGabor(iFiltre,3), WGabor);
                    %
                    %                 GG1 = filter2(g, JJ);
                    %                 GG1 = real(GG1) .^ 2 + imag(GG1) .^ 2;
                    GG1 =(GG1 - MinMaxGabor(iFiltre,1)) * (nbValHist / (MinMaxGabor(iFiltre,2) - MinMaxGabor(iFiltre,1)));
                    
                    %                 figure(1386); subplot(2,1,1); imagesc(GG1); colorbar; title(sprintf('%d', iFiltre)); drawnow
                    
                    ccc = Imen_histGabor(GG1, nbValHist, NUMBER_OF_PROCESSORS);
                    
                    %                 if iFiltre < 10
                    %                     figure(2000+iFiltre); hold on; plot(ccc); grid on;
                    %                 end
                    %                 figure(1386); subplot(2,1,2); plot(ccc); grid on; title(sprintf('%d', iFiltre));
                    
                    GaborModeleAngle{iFiltre,iAngle} = ccc;
                else
                    GaborModeleAngle{iFiltre,iAngle} = [];
                end
            end
        end
    end
end
my_close(hw, 'MsgEnd')

%{
for k=1:length(IGabor)
figure(75645); imagesc(IGabor{k});
pause(0.5);
end

for k=1:length(IGabor)
figure(75645);
plot(GaborModeleAngle{k}); grid on; title(sprintf('iGabor=%d', k))
pause(0.5);
end
%}

if nargout == 0
    figure;
    k = 1;
    for iFiltre=1:nbCliques
        for iAngle=1:nbBinsAngles-1
            subplot(nbCliques, nbBinsAngles-1, k);
            imagesc(GaborModeleAngle{iFiltre,iAngle}); axis xy; axis off;
            k = k + 1;
        end
    end
end
