function this = Imen_Estimation_w5(this, numTexture, subMasque, I, IGabor, ...
    maxWidthSlidingWindow, pointeurWidthSlidingWindow)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

f = 'my_warndlg(Lang(''Cette fen�tre ne peut �tre ferm�e pour l''''instant.'', ''This window cannot be closed for the moment.''), 0, ''Tag'', ''ImpossibleToCloseThisWindow'')';
Fig = figure('CloseRequestFcn', f);
% Fig = figure('CloseRequestFcn', []);

Texture = this.Texture(numTexture);

if Texture.UseCooc
    nbMesuresSimilariteCooc  = size(Texture.cliques,1);
else
    nbMesuresSimilariteCooc = 0;
end

if Texture.UseGabor
    nbMesuresSimilariteGabor = size(Texture.Gabor.Param, 1);
else
    nbMesuresSimilariteGabor = 0;
end

nbMesuresSimilarite = nbMesuresSimilariteCooc + nbMesuresSimilariteGabor;
subTexture = 1:length(Texture.nomCourbe); % ATTENTION : length(~isnan(Texture.nomCourbe))
nbTexture = length(subTexture);

% ListeMaxWidthSlidingWindow = unique(maxWidthSlidingWindow);
nbMaxWidthSlidingWindow = length(maxWidthSlidingWindow);
subMasqueErode = cell(nbMaxWidthSlidingWindow,nbTexture);
for iTexture=1:nbTexture
    subMasqueErode{1,iTexture} = subMasque{iTexture};
end
for i=2:nbMaxWidthSlidingWindow
    for iTexture=1:nbTexture
        subMasqueErode{i,iTexture} = imerode(subMasque{iTexture}, ones(maxWidthSlidingWindow(i)-maxWidthSlidingWindow(1)));
    end
end

if nbMaxWidthSlidingWindow > 1
    [X, Y] = meshgrid(1:size(I,2), 1:size(I,1));
end

%{
w2 = zeros(1,nbMesuresSimilariteCooc);
w2([2 7 8 43]) = 1;
w2 = w2 / sum(w2);
Texture.w = w2;
this.Texture(numTexture) = Texture;
subCliques = find(w2 ~=0);
plotSegmSignature(this, 'numTexture', numTexture, 'OnlyOneFig', 1, 'subCliques', subCliques);
return
%}

SeuilNbMesuresRetenuesInitial = 4;
maxIter = 10000;
minIter = 100;

subTexture       = [];
nbPixelsTexture  = [];
subPixelsTexture = {};
for iTexture=1:length(Texture.nomCourbe)
    if ~isempty(Texture.nomCourbe{iTexture})
        subTexture(end+1) = iTexture; %#ok<AGROW>
        nbPixelsTexture(end+1)  = sum(subMasque{iTexture}(:)); %#ok<AGROW>
        subPixelsTexture{end+1} = find(subMasque{iTexture}); %#ok<AGROW>
    end
end

w = ones(1, nbMesuresSimilarite);
for t=1:maxIter
    % on augmente de nombre d'une unit� toutes les 100 it�rations pour �viter d'�tre pi�g� par une convergence.
    SeuilNbMesuresRetenues = SeuilNbMesuresRetenuesInitial + max(1,floor((t/100))-5);
    
    irandTexture     = subTexture(1 + floor(rand(1) * nbTexture));
    irandPixel       = 1 + floor(rand(1) * nbPixelsTexture(irandTexture));
    [yPixel, xPixel] = ind2sub(size(subMasque{subTexture(irandTexture)}), subPixelsTexture{subTexture(irandTexture)}(irandPixel));
    nx2 = 16;
    ny2 = 16;
    subx = max(1,xPixel-nx2):min(xPixel+nx2, size(I,2));
    suby = max(1,yPixel-ny2):min(yPixel+ny2, size(I,1));
    
    delta = zeros(1,nbMesuresSimilarite);
    
    if Texture.UseCooc
        for iCooc=1:nbMesuresSimilariteCooc
            clique = Texture.cliques(iCooc,:);
            NbNiveaux = size(Texture.CmatModeleAngle{irandTexture}{iCooc}, 1);
            SignalCooc{iCooc} = coocMatNbNiveaux(I(suby,subx), clique(1), clique(2), NbNiveaux); %#ok<AGROW> % coocMatNbNiveauxFiltre
            SignalCooc{iCooc} = double(SignalCooc{iCooc});  %#ok<AGROW>
        end
        QCooc = Texture.CmatModeleAngle(subTexture);
    else
        SignalCooc = [];
        QCooc      = [];
    end
    
    if Texture.UseGabor
        for iGabor=1:nbMesuresSimilariteGabor
            if isempty(IGabor{iGabor})
                SignalGabor{iGabor} = []; %#ok<AGROW>
            else
                if pointeurWidthSlidingWindow(iGabor) ~= 1
%                     WidthFiltreGabor = maxWidthSlidingWindow(pointeurWidthSlidingWindow(iGabor));
                    M = subMasqueErode{pointeurWidthSlidingWindow(iGabor), irandTexture};
                    if M(yPixel, xPixel)
                        subyM = suby;
                        subxM = subx;
                    else
                        % Recherche du point le plus proche
                        subM = find(M);
                        [tilda, ordre] = min((X(subM) - xPixel).^2 + (Y(subM) - yPixel).^2);
                        clear tilda
                        ordre = ordre(1);
                        xPixelM = X(subM(ordre));
                        yPixelM = Y(subM(ordre));
                        subxM = max(1,xPixelM-nx2):min(xPixelM+nx2, size(I,2));
                        subyM = max(1,yPixelM-ny2):min(yPixelM+ny2, size(I,1));
                    end
                else
                    subyM = suby;
                    subxM = subx;
                end
                nbValHist = length(Texture.Gabor.Hist{1}{1}); % ATTENTION DANGER SI LA PREMIERE LECTURE EST VIDE : IL VAUT MIEUX CREER Texture.Gabor.nbValHist
                GG1 = IGabor{iGabor}(subyM,subxM);
                MinVal = Texture.Gabor.MinMaxGabor{irandTexture}(iGabor,1);
                MaxVal = Texture.Gabor.MinMaxGabor{irandTexture}(iGabor,2);
                GG1 =(GG1 - MinVal) * (nbValHist / (MaxVal - MinVal));
                %   figure(1386); subplot(2,1,1); imagesc(GG1); colorbar; title(sprintf('%d', irandMesureSimilarite)); drawnow
                SignalGabor{iGabor} = Imen_histGabor(GG1, nbValHist, NUMBER_OF_PROCESSORS); %#ok<AGROW>
            end
        end
        QGabor = Texture.Gabor.Hist(subTexture);
    else
        SignalGabor = [];
        QGabor      = [];
    end
    
    [QdTCooc, QdTGabor] = Imen_ppD(QCooc, QGabor, SignalCooc, SignalGabor, w, irandTexture, NUMBER_OF_PROCESSORS);
    
    for f=1:nbMesuresSimilariteCooc
        QsTf = Texture.CmatModeleAngle{irandTexture}{f};
        QsTf = double(QsTf);
%         QsTf = QsTf/sum(QsTf(:));
        
        QdTf = QdTCooc{f};
        QdTf = double(QdTf);
%         QdTf = QdTf / sum(QdTf(:));

        KL1 = kullback(QdTf, SignalCooc{f}, NUMBER_OF_PROCESSORS);
        KL2 = kullback(QsTf, SignalCooc{f}, NUMBER_OF_PROCESSORS);
%         KL1 = min(KL1, 2);
%         KL2 = min(KL2, 2);
%{
figure;
subplot(2,2,1); imagesc(QdTf); axis xy; title(sprintf('QdTf  KL1=%f', KL1));
subplot(2,2,2); imagesc(SignalCooc{f}); axis xy; title('SignalCooc');
subplot(2,2,3); imagesc(QsTf); axis xy; title(sprintf('QsTf  KL2=%f', KL2));
%}
       
        if t <= 10
%             figure(4951); subplot(2,1,1); hold on; grid on; plot(t, KL1, '+r');  plot(t, KL2, '+b'); subplot(2,1,2); plot(t, KL1-KL2, '+k'); grid on; hold on;
        end
        
        delta(f) =  KL1 - KL2;
        %{
            figure(6363);
            subplot(1,3,1); imagesc(SignalCooc{f}); title('SignalCooc')
            subplot(1,3,2); imagesc(QdTf); title('QdTf')
            subplot(1,3,3); imagesc(QsTf); title('QsTf')
            title(sprintf('f=%f  delta(f)=%f', f, delta(f)));
            pause(0.5)
        %}
    end
    
    
    for f=1:nbMesuresSimilariteGabor
        QsTf = Texture.Gabor.Hist{irandTexture}{f}(:);
        %         QsTf = QsTf/sum(QsTf(:));
        
        QdTf = QdTGabor{f}(:);
        QdTf = double(QdTf);
        %         QdTf = QdTf / sum(QdTf(:));
        
        KL1 = kullback(QdTf, SignalGabor{f}, NUMBER_OF_PROCESSORS);
        KL2 = kullback(QsTf, SignalGabor{f}, NUMBER_OF_PROCESSORS);
%         KL1 = min(KL1, 2);
%         KL2 = min(KL2, 2);
        
        if t <= 10
%             figure(4952); subplot(2,1,1); hold on; grid on; plot(t, KL1, '+r');  plot(t, KL2, '+b'); subplot(2,1,2); plot(t, KL1-KL2, '+k'); grid on; hold on;
        end
        delta(f+nbMesuresSimilariteCooc) = KL1 - KL2;
        
%         % Apr�s modif Kullback p1 = double(p1(:)) + 0.00001;
%         delta(f+nbMesuresSimilariteCooc) = delta(f+nbMesuresSimilariteCooc)  / 3;
%         delta(delta > 3) = 3;
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %{
            if rand(1) < 0.001
                figure(6364); hold off;
                Legend = {};
                for kk=1:nbTexture
                    plot(Texture.Gabor.Hist{subTexture(kk)}{f}, 'k'); grid on; hold on;
                    Legend{kk, 1} = sprintf('%d', kk); %#ok<AGROW>
                end
                plot(SignalGabor{f}, 'b');
                hold on; grid on;
                plot(QdTf, 'r');
                plot(QsTf, 'g');
                title(sprintf('f=%d  delta(f)=%f', f, delta(f)));
                legend([Legend; {'SignalGabor',; 'QdTf'; 'QsTf'}])
                pause(0.5)
            end
        %}
    end
    
    if any(isnan(delta))
        pppp = find(isnan(delta));
    end
    if any(delta == 0)
        pppp = find(delta == 0);
    end

    %{
    figure(5598); bar(delta); grid on;
    %}
    w = w .* (1 + 0.1 * delta);
    w = w / sum(w);
    
    w2 = w .* w;
    w2 = w2 / sum(w2(:));
    
    [w2Tri, ordre] = sort(w2);
    w2Cum = cumsum(w2Tri);
    subOut = (w2Cum < 0.1);
    subIn = find(w2Cum >= 0.1);
%     if ((length(subIn) <= SeuilNbMesuresRetenues) && all(w2(ordre(subIn)) > 0.1)) || (length(subIn) == 1)
    if (length(subIn) <= SeuilNbMesuresRetenues) || (length(subIn) == 1)
%         pppp = w2(ordre(subIn)) >= 0.1;
%         subIn = subIn(pppp);
        plot_w2(Fig, w2, t, ordre(subIn), w2Cum, nbMesuresSimilarite, SeuilNbMesuresRetenues, maxIter, minIter)
        break
    end
    if (mod(t,10) == 0) || (t < 50)
        plot_w2(Fig, w2, t, ordre(subIn), w2Cum, nbMesuresSimilarite, SeuilNbMesuresRetenues, maxIter, minIter)
    end
    
end
set(Fig, 'CloseRequestFcn', 'closereq');

% figure; bar(w2); grid on;

w2(ordre(subOut)) = 0;
w2(w2 < 0.1) = 0;
w2 = w2 / sum(w2);

subCliques = find(w2 ~=0);
if Texture.UseCooc
    plotSegmSignature(this, 'numTexture', numTexture, 'OnlyOneFig', 1, 'subCliques', subCliques, ...
        'Weights', w2(subCliques));
end

[~, ordre] = sort(w2, 2, 'descend');
for iClique=1:length(subCliques)
    k = ordre(iClique);
    if k <= nbMesuresSimilariteCooc
        fprintf('k=%d  w=%f  Clique = [%d % d]\n', k, w2(k), Texture.cliques(k,:));
    else
        kGabor = k - nbMesuresSimilariteCooc;
        fprintf('k=%d  w=%f  Clique = [%d %d %d]\n', k, w2(k), Texture.Gabor.Param(kGabor,:));
    end
end

Texture.w = w2;
this.Texture(numTexture) = Texture;


% ----------------------------------------------------------------------------------
function [QdTCooc, QdTGabor] = Imen_ppD(QCooc, QGabor, SignalCooc, SignalGabor, w, irandTexture, NUMBER_OF_PROCESSORS)

if (length(QCooc) == 2) || (length(QGabor) == 2)
    switch irandTexture
        case 1
            iTexturePlusProche = 2;
        case 2
            iTexturePlusProche = 1;
    end
    if isempty(QCooc)
        QdTCooc = [];
    else
        QdTCooc  = QCooc{iTexturePlusProche};
    end
    if isempty(QGabor)
        QdTGabor = [];
    else
        QdTGabor = QGabor{iTexturePlusProche};
    end
    return
end

if isempty(QCooc)
    nbTextures = length(QGabor);
else
    nbTextures = length(QCooc);
end

d = Inf(nbTextures,1);
for iTexture=1:nbTextures
    if iTexture ~= irandTexture
        if isempty(QCooc)
            QTCooc = [];
        else
            QTCooc = QCooc{iTexture};
        end
        if isempty(QGabor)
            QTGabor = [];
        else
            QTGabor = QGabor{iTexture};
        end
        d(iTexture) = Imen_crossentropieW(QTCooc, QTGabor, SignalCooc, SignalGabor, w, NUMBER_OF_PROCESSORS);
    end
end
[a, bb] = min(d(:));
if isempty(QCooc)
    QdTCooc = [];
else
    QdTCooc = QCooc{(bb)};
end
if isempty(QGabor)
    QdTGabor = [];
else
    QdTGabor = QGabor{(bb)};
end


function cross = Imen_crossentropieW(QTCooc, QTGabor, SignalCooc, SignalGabor, w, NUMBER_OF_PROCESSORS)

cross = 0;
nbMesuresSimilariteCooc  = length(QTCooc);
nbMesuresSimilariteGabor = length(QTGabor);
nbMesuresSimilarite = nbMesuresSimilariteCooc + nbMesuresSimilariteGabor;
KL = zeros(1,nbMesuresSimilarite);
for iMesureSimilarite=1:nbMesuresSimilariteCooc
    KL(iMesureSimilarite) =  kullback(QTCooc{iMesureSimilarite}(:), SignalCooc{iMesureSimilarite}(:), NUMBER_OF_PROCESSORS);
end
for iMesureSimilarite=1:nbMesuresSimilariteGabor
    KL(iMesureSimilarite+nbMesuresSimilariteCooc) =  kullback(QTGabor{iMesureSimilarite}(:), SignalGabor{iMesureSimilarite}(:), NUMBER_OF_PROCESSORS);
end
% {
% figure(56489); bar(KL); grid on;
%{
[pppp, ordre] = sort(abs(KL));
figure; plot(Signal, 'k'); grid on; hold on; plot(CmatModeleAngleTextue{ordre(1)}(:), 'r')
figure; plot(Signal, 'k'); grid on; hold on; plot(CmatModeleAngleTextue{ordre(end)}(:), 'r')
%}
for iMesureSimilarite=1:nbMesuresSimilarite
    cross = cross + (w(iMesureSimilarite)^2) * KL(iMesureSimilarite);
end



% ------------------------------------------------------------------------------------------------
function plot_w2(Fig, w2, t, sub, w2Cum, nbMesuresSimilarite, SeuilNbMesuresRetenues, maxIter, minIter)

w3 = zeros(size(w2));
w3(sub) = w2(sub);
nbMesuresConservees = length(sub);

figure(Fig);
h = subplot(3,1,1);

bar(h,w2); grid on; title(sprintf('iTer = %d  minIter=%d  maxIter=%d', t, minIter, maxIter))
hold on; bar(h, w3, 'r'); hold off;
set(h, 'XLim', [1 nbMesuresSimilarite], 'YLim', [0 max(w2)])

h = subplot(3,1,2);
plot(h, fliplr(w2Cum)); grid on; title(sprintf('Nb mesures conserv�es = %d   SeuilNbMesuresRetenues = %d', nbMesuresConservees, SeuilNbMesuresRetenues))
hold on;
plot(h, [1 nbMesuresSimilarite], [0.1 0.1], 'r');
plot(h, [SeuilNbMesuresRetenues SeuilNbMesuresRetenues], [0 1], 'r');
hold off;
set(h, 'XLim', [1 nbMesuresSimilarite], 'YLim', [0 1])

h = subplot(3,1,3);
if t == 1
    hold off;
end
plot(h, t, nbMesuresConservees, '*'); grid on; hold on;
set(h, 'XLim', [0 t], 'YLim', [0 nbMesuresSimilarite])
