% Transformation d'une image indexee en intensite
%
% Syntax
%   [b, flag] = Indexed2Intensity(a, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant l'image indexee
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b    : Instance de cl_image contenant l'image en intensite
%   flag : Booleen indiquant si le calcul a ete realise
%
% Examples
%   RGB = imread(getNomFicDatabase('BouffayNeptuneMadeleine.tif'));
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%   [b, flag] = RGB2Indexed(a);
%   imagesc(b)
%   [c, flag] = Indexed2Intensity(b);
%   imagesc(c)
%
% See also cl_image/RGB2Indexed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = Indexed2Intensity(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [this(k), flag] = unitaire_Indexed2Intensity(this(k), varargin{:});
    if ~flag
        return
    end
end


function [this, flag] = unitaire_Indexed2Intensity(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% [varargin, nbCoul] = getPropertyValue(varargin, 'nbCoul', 256);

if this.ImageType ~= 3
    my_warndlg('Cette image n''est pas indexee (ImageType~=3)', 1);
    flag = 0;
    return
end

if this.ColormapIndex == 1
    map = this.ColormapCustom;
else
    map = this.Colormap;
end
% CLim = double(this.CLim);

try
    % On force � single pour �viter des pb par la suite
    X = this.Image(suby,subx);
    subNaN = (X == this.ValNaN);
    oldValMax = max(X(~subNaN));
    %     oldValMin = min(X(~subNaN));
%     X = single(ind2gray(1+X, double(map))); % Comment� par JMA le 25/03/2019
    X = single(ind2gray(X, double(map)));
    X(subNaN) = NaN;
    newValMax = max(X(~subNaN));
    newValMin = min(X(~subNaN));
    X = 1 + (single(oldValMax)-1) * (X-newValMin) / (newValMax - newValMin);
    %     unique(X(~isnan(X)))
    this = replace_Image(this, X);
catch ME
    ME.getReport
    my_warndlg(ME.message, 1);
    flag = 0;
    return
end

this.ValNaN        = NaN;
this.ImageType     = 1;
this.Colormap      = jet(256);
this.ColormapIndex = 3;

%% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
% CLim = [this.StatValues.Min this.StatValues.Max];
CLim = [0 this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'Intensity');
this.Unit = ' ';

%% Par ici la sortie

flag = 1;
