% Transformation d'une image indexee en intensite
%
% Syntax
%   [b, flag] = Indexed2IntensityDirect(a, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant l'image indexee
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b    : Instance de cl_image contenant l'image en intensite
%   flag : Booleen indiquant si le calcul a ete realise
%
% Examples
%   RGB = imread(getNomFicDatabase('BouffayNeptuneMadeleine.tif'));
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%   [b, flag] = RGB2Indexed(a);
%   imagesc(b)
%   [c, flag] = Indexed2IntensityDirect(b);
%   imagesc(c)
%
% See also cl_image/RGB2Indexed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = Indexed2IntensityDirect(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [this(k), flag] = unitaire_Indexed2IntensityDirect(this(k), varargin{:});
    if ~flag
        return
    end
end


function [this, flag] = unitaire_Indexed2IntensityDirect(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% [varargin, nbCoul] = getPropertyValue(varargin, 'nbCoul', 256);

if this.ImageType ~= 3
    my_warndlg('Cette image n''est pas index�e (ImageType~=3)', 1);
    flag = 0;
    return
end

X = this.Image(suby,subx);
X = singleUnlessDouble(X, this.ValNaN);
this = replace_Image(this, X);

this.ValNaN        = NaN;
this.ImageType     = 1;
this.Colormap      = jet(256);
this.ColormapIndex = 3;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
% CLim = [this.StatValues.Min this.StatValues.Max];
CLim = [0 this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'IntensityDirect');
this.Unit = ' ';

%% Par ici la sortie

flag = 1;
