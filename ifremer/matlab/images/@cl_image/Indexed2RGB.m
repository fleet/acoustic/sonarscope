% Transformation d'une image indexee en RGB
%
% Syntax
%   [b, flag] = Indexed2RGB(a, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant l'image indexee
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b    : Instance de cl_image contenant l'image RGB
%   flag : Booleen indiquant si le calcul a ete realise
%
% Examples
%   RGB = imread(getNomFicDatabase('BouffayNeptuneMadeleine.tif'));
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%   [b, flag] = RGB2Indexed(a);
%   imagesc(b)
%   [c, flag] = Indexed2RGB(b);
%   imagesc(c)
%
% See also cl_image/RGB2Indexed cl_image/Indexed2Intensity  Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = Indexed2RGB(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [this(i), flag] = unitaire_Indexed2RGB(this(i), varargin{:});
    if ~flag
        return
    end
end


function [this, flag] = unitaire_Indexed2RGB(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Appel de la fonction de filtrage

if this.ImageType ~= 3
    my_warndlg('Cette image n''est pas indexee (ImageType~=3)', 1);
    flag = 0;
    return
end

map = this.Colormap;
try
    RGB = zeros([length(suby), length(subx), 3], 'uint8');
catch %#ok<CTCH>
    RGB =  cl_memmapfile('Value', 0, 'Size', [length(suby) length(subx) 3], 'Format', 'uint8');
end

str1 = 'Conversion en RGB en cours';
str2 = 'Conversion in RGB,';
N = length(suby);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    X = this.Image(suby(k),subx);
    RGB(k,:,:) = floor(256 * ind2rgb(uint8(X), map));
end
my_close(hw, 'MsgEnd')

this = replace_Image(this, RGB);

this.ColormapCustom = map;
this.ColormapIndex = 1;
this.ValNaN        = NaN;
this.ImageType     = 2;

%% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'RGB');
this.Unit = ' ';
this.ColormapIndex = 1;

%% Par ici la sortie

flag = 1;
