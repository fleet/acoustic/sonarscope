% Convert Intensity image to Indexed
%
% Syntax
%   [b, flag] = Intensity2Indexed(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%
%   b = RGB2Intensity(a);
%   imagesc(b);
%
%   [c, flag] = Intensity2Indexed(b);
%   imagesc(c);
%
% See also cl_image/RGB2Indexed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = Intensity2Indexed(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('Intensity2Indexed'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [K, flag] = Intensity2Indexed_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [that, flag] = Intensity2Indexed_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []);

[varargin, NoStats] = getFlag(varargin, 'NoStats');

that = [];
flag = 1;

if isempty(CLim)
    CLim = this.CLim;
end

%% Control

if this.ImageType ~= 1
    str1 = 'Cette image n''est pas en "Intensité" (ImageType~=1)';
    str2 = 'This image is not an "Intensity" one (ImageType~=1)';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Algorithm

if this.ColormapIndex == 1
    map = this.ColormapCustom;
else
    map = this.Colormap;
end
[varargin, nbCoul] = getPropertyValue(varargin, 'nbCoul', size(map,1)); %#ok<ASGLU>

CLim = double(CLim);
if this.Video == 1
    ValNaN = 255;
else
    ValNaN = 0;
end

try
    subNaN = findNaN(this, 'subx', subx, 'suby', suby);
    I = this.Image(suby,subx);
    I = mat2ind(I, CLim, nbCoul-2);
    I(I < 1) = 1;
    I(subNaN) = ValNaN;
catch ME
    my_warndlg(ME.message, 1);
    that = [];
    flag = 0;
    return
end

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'Intensity2Indexed', 'ValNaN', ValNaN, ...
    'ImageType', 3, 'Unit', ' ', 'ColormapIndex', 1, 'NoStats', NoStats);
that.Colormap       = map;
that.ColormapCustom = map;
that.CLim = [1 size(map,1)];
