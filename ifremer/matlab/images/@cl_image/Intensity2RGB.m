% Convert Intensity image to RGB
%
% Syntax
%   [b, flag] = Intensity2RGB(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%   CLim : Contraste enhancement limits
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%
%   b = RGB2Intensity(a);
%   imagesc(b);
%
%   [c, flag] = Intensity2RGB(b);
%   imagesc(c);
%
% See also cl_image/RGB2Indexed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = Intensity2RGB(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('Intensity2RGB'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [K, flag] = Intensity2RGB_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [that, flag] = Intensity2RGB_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []); %#ok<ASGLU>

if isempty(CLim)
    CLim = this.CLim;
end

that = [];
flag = 1;

%% Control

if this.ImageType ~= 1
    str1 = 'Cette image n''est pas de type "Intensity" (ImageType~=1)';
    str2 = 'This image is not an "Intensity" one (ImageType~=1)';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Algorithm

if this.ColormapIndex == 1
    map = this.ColormapCustom;
else
    map = this.Colormap;
end

if this.Video == 2
    map = flipud(map);
end

% if (this.YDir == 1) && ((this.y(2)-this.y(1)) > 0)
%     suby = flipud(suby(:));
% end

J = zeros(length(suby), length(subx), 3, 'single');
nB = 500;
for iBlock=1:nB:length(suby)
    subBlock = iBlock:min(iBlock-1+nB, length(suby));
    J(subBlock,:,:) = ind2rgb(gray2ind(mat2gray(double(this.Image(suby(subBlock),subx)), double(CLim)), size(map,1)), map);
end
I = reshape(J, [size(J,1)*size(J,2) 3]);
clear J

subNaN = findNaN(this, 'subx', subx, 'suby', suby);
I(subNaN,1) = 0;
I(subNaN,2) = 0;
I(subNaN,3) = 0;
clear subNaN
I = reshape(I, [length(suby) length(subx) 3]);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'Intensity2RGB', 'ValNaN', 0, ...
    'ImageType', 2, 'Unit', ' ', 'ColormapIndex', 2);
