% Convert intensity image in RGB using a GMT colormap
%
% Syntax
%   [b, flag] = Intensity2RGB_GMT(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx  : subsampling in X
%   suby  : subsampling in Y
%   Inter : 1=Interplation des valeurs, 0, affectation directe des valeurs (0 par defaut)
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%
%   b = RGB2Intensity(a);
%   imagesc(b);
%
%   NomFicCpt = getNomFicDatabase('GMT_Haxby.cpt');
%   [c, flag] = Intensity2RGB_GMT(b, NomFicCpt);
%   imagesc(c);
%
% See also cl_image/RGB2Intensity Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = Intensity2RGB_GMT(this, NomFicCpt, varargin)

that = cl_image.empty;

[flag, map, Deb, Fin, B] = import_cpt(NomFicCpt);
if ~flag
    return
end

N = length(this);
hw = create_waitbar(waitbarMsg('Intensity2RGB_GMT'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [K, flag] = Intensity2RGB_GMT_unit(this(k), map, Deb, Fin, B, varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [that, flag] = Intensity2RGB_GMT_unit(this, map, Deb, Fin, B, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Inter] = getPropertyValue(varargin, 'Inter', 0); %#ok<ASGLU>

that = [];
flag = 1;

%% Control

if this.ImageType ~= 1
    str1 = 'Cette image n''est pas de type "Intensity" (ImageType~=1)';
    str2 = 'This image is not an "Intensity" one (ImageType~=1)';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Algorithm

if size(B) == 1
    B = [B B B];
end

I1 = zeros(length(suby), length(subx), 'single');
I2 = zeros(length(suby), length(subx), 'single');
I3 = zeros(length(suby), length(subx), 'single');

if ~isempty(B)
    I1 = I1 + B(1);
    I2 = I2 + B(2);
    I3 = I3 + B(3);
end

if ~isempty(Deb)
    sub = (this.Image(suby,subx) <= Deb(1));
    I1(sub) = map(1,1);
    I2(sub) = map(1,2);
    I3(sub) = map(1,3);
end

N = length(Deb);
for k=1:N
    if Inter
        %                 Map = this.Colormap;
        %             nbCoul = size(Map, 1);
        %             subc = linspace(1, nbCoul, 256);
        %             Colormap(:, 1) = ';
        %             Colormap(:, 2) = interp1(1:nbCoul, Map(:,2), subc)';
        %             Colormap(:, 3) = interp1(1:nbCoul, Map(:,3), subc)';
        I = this.Image(suby,subx);
        sub = (I >= Deb(k)) & (I <= Fin(k));
        I1(sub) = map(k,1) + ((map(k+1,1) - map(k,1)) / (Fin(k) - Deb(k))) * (I(sub) - Deb(k));
        I2(sub) = map(k,2) + ((map(k+1,2) - map(k,2)) / (Fin(k) - Deb(k))) * (I(sub) - Deb(k));
        I3(sub) = map(k,3) + ((map(k+1,3) - map(k,3)) / (Fin(k) - Deb(k))) * (I(sub) - Deb(k));
    else
        sub = (this.Image(suby,subx) >= Deb(k)) & (this.Image(suby,subx) <= Fin(k));
        I1(sub) = map(k,1);
        I2(sub) = map(k,2);
        I3(sub) = map(k,3);
    end
end

if ~isempty(Fin)
    sub = this.Image(suby,subx) >= Fin(end);
    I1(sub) = map(end,1);
    I2(sub) = map(end,2);
    I3(sub) = map(end,3);
end

try
    J = zeros(length(suby), length(subx), 3, 'single');
catch %#ok<CTCH>
    J = cl_memmapfile('Value', 0, 'Size', [length(suby) length(subx) 3], 'Format', 'single');
end
J(:,:,1) = I1;
J(:,:,2) = I2;
J(:,:,3) = I3;
clear I1 I2 I3

%% Output image

that = inherit(this, J, 'subx', subx, 'suby', suby, 'AppendName', 'Intensity2RGB_GMT', 'ValNaN', 0, ...
    'ImageType', 2, 'Unit', ' ');
