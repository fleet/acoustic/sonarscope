function [flag, this] = KMSpecularCompensation(this, flagRecomputeRn, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Vérification des signatures

identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
flag = testSignature(this, 'DataType', [identRayPathSampleNb identTwoWayTravelTimeInSeconds], 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Test si le signaux BSN, BSO, TGVN et TVGCrossOver sont présents

flag(1) = isfield(this.Sonar, 'BSN')          && ~isempty(this.Sonar.BSN);
flag(2) = isfield(this.Sonar, 'BSO')          && ~isempty(this.Sonar.BSO);
flag(3) = isfield(this.Sonar, 'TVGN')         && ~isempty(this.Sonar.TVGN);
flag(4) = isfield(this.Sonar, 'TVGCrossOver') && ~isempty(this.Sonar.TVGCrossOver);

if ~all(flag)
    str1 = 'Cette image ne provient certainement pas d''un sondeur Kongsberg.';
    str2 = 'This image seems not to come from a Kongsberg sounder.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Algorithm

I = this.Image(suby,subx,:);

Correction = NaN(size(I), 'single');
m = this.nbColumns / 2;
subBab = find(subx <= m);
subTri = find(subx > m);
for k=1:length(suby)
    BSN          = this.Sonar.BSN(suby(k),:);
    BSO          = this.Sonar.BSO(suby(k),:);
    Rn         = this.Sonar.TVGN(suby(k),:);
    TVGCrossOver = this.Sonar.TVGCrossOver(suby(k),:);
    
    if size(this.Sonar.BSN, 2) == 1
        if this.DataType == identRayPathSampleNb
            r = I(k, :);
        else
            r = I(k, :) * this.Sonar.SampleFrequency(suby(k));
        end
        Correction(k,:) = (BSN - BSO) * computeCoefMultiplicateurDeltaBS(r, Rn, TVGCrossOver, flagRecomputeRn);
    else
        if this.DataType == identRayPathSampleNb
            r = I(k, subBab);
        else
            r = I(k, subBab) * this.Sonar.SampleFrequency(suby(k), 1);
        end
        Correction(k,subBab) = (BSN(1) - BSO(1)) * computeCoefMultiplicateurDeltaBS(r, Rn(1), TVGCrossOver(1), flagRecomputeRn);
        
        if this.DataType == identRayPathSampleNb
            r = I(k, subTri);
        else
            r = I(k, subTri) * this.Sonar.SampleFrequency(suby(k), 2);
        end
        Correction(k,subTri) = (BSN(2) - BSO(2)) * computeCoefMultiplicateurDeltaBS(r, Rn(2), TVGCrossOver(2), flagRecomputeRn);
    end
    %{
    FigUtils.createSScFigure;
    hc(1) = subplot(2, 1, 1); PlotUtils.createSScPlot(Correction(k,:)); grid on;
    title(sprintf('Bab, BSN=%s  BSO=%s  BSN-BSO=%s', num2str(BSN(1)), num2str(BSO(1)), num2str(BSN(1)-BSO(1))))
    hc(2) = subplot(2, 1, 2); PlotUtils.createSScPlot(r); grid on
    title(sprintf('TGVN=%s', num2str(Rn(1))));
    hold on; plot(ones(size(r))*Rn(1));
    linkaxes(hc, 'x')
    %}

end

%% Output image

DataType  = cl_image.indDataType('Reflectivity');
this = inherit(this, Correction, 'subx', subx, 'suby', suby, 'AppendName', 'KMSpecularCompensation', ...
    'DataType', DataType, 'Unit', 'dB');

flag = 1;


function X = computeCoefMultiplicateurDeltaBS(r, rn, Tetac0, flagRecomputeRn)

if flagRecomputeRn
    fprintf(1,'%8.2f  %8.2f\n', rn, min(r));
    rn = max(rn, min(r));
end

r(r < rn) = rn;
rc0 = rn / cosd(Tetac0);
X = zeros(size(r));
sub = (r >= rn) & (r < rc0);
X(sub) = 1 - sqrt(((r(sub)) - rn) / (rc0 - rn));
