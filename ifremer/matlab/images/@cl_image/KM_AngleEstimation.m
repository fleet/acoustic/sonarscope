function [flag, this] = KM_AngleEstimation(this, flagRecomputeRn, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Vérification des signatures

identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
flag = testSignature(this, 'DataType', [identRayPathSampleNb identTwoWayTravelTimeInSeconds], 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Test si le signaux BSN, BSO, TGVN et TVGCrossOver sont présents

flag(1) = isfield(this.Sonar, 'BSN')          && ~isempty(this.Sonar.BSN);
flag(2) = isfield(this.Sonar, 'BSO')          && ~isempty(this.Sonar.BSO);
flag(3) = isfield(this.Sonar, 'TVGN')         && ~isempty(this.Sonar.TVGN);
flag(4) = isfield(this.Sonar, 'TVGCrossOver') && ~isempty(this.Sonar.TVGCrossOver);

if ~all(flag)
    str1 = 'Cette image ne provient certainement pas d''un sondeur Kongsberg.';
    str2 = 'This image seems not to come from a Kongsberg sounder.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Algorithm

I = this.Image(suby,subx,:);

Correction = NaN(size(I), 'single');
m = this.nbColumns / 2;
subBab = find(subx <= m);
subTri = find(subx > m);
Rn = this.Sonar.TVGN(:,:);
SampleFrequency = this.Sonar.SampleFrequency(:,:);
for k=1:length(suby)
    pingRn = Rn(suby(k),:);
    
    if size(this.Sonar.BSN, 2) == 1
        if this.DataType == identRayPathSampleNb
            r = I(k, :);
        else
            r = I(k, :) * SampleFrequency(suby(k));
        end
        Correction(k,:) = computeKMAngle(r, pingRn, flagRecomputeRn);
        
    else
        
        if this.DataType == identRayPathSampleNb
            r = I(k, subBab);
        else
            r = I(k, subBab) * SampleFrequency(suby(k), 1);
        end
        Correction(k,subBab) = computeKMAngle(r, pingRn(1), flagRecomputeRn);
        
        if this.DataType == identRayPathSampleNb
            r = I(k, subTri);
        else
            r = I(k, subTri) * SampleFrequency(suby(k), 2);
        end
        Correction(k,subTri) = computeKMAngle(r, pingRn(2), flagRecomputeRn);
    end
    %{
    FigUtils.createSScFigure;
    hc(1) = subplot(2, 1, 1); PlotUtils.createSScPlot(Correction); grid on;
    title(sprintf('Bab, BSN=%s  BSO=%s  BSN-BSO=%s', num2str(BSN(1)), num2str(BSO(1), num2str(BSN(1)-BSO(1))))
    hc(2) = subplot(2, 1, 2); PlotUtils.createSScPlot(r); grid on;
    title(sprintf('TGVN=%s', num2str(Rn(1))));
    hold on; plot(ones(size(r))*Rn(1));
    linkaxes(hc, 'x')
    %}

end

%% Output image

DataType  = cl_image.indDataType('IncidenceAngle');
this = inherit(this, Correction, 'subx', subx, 'suby', suby, 'AppendName', 'KM_Approximation', ...
    'DataType', DataType, 'Unit', 'deg');

flag = 1;


function X = computeKMAngle(r, rn, flagRecomputeRn)

if flagRecomputeRn
    rn = max(rn, min(r));
end

r(r < rn) = rn;
X = acosd(rn ./ r);
