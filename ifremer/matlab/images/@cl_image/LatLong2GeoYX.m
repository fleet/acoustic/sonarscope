% Transformation d'une image de g�om�trie LatLong g�om�trie GeoYX
%
% Syntax
%   b = LatLong2GeoYX(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   b    : instance de cl_image
%   flag : 1=Operation reussie, 0=operation non reussie
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_125m.mnt');
%   a = cl_car_ima(nomFic);
%   b = view(a, 'Sonar.Ident', 1);
%   c = get(b, 'Images');
%   imagesc(c)
%
%   [d, flag] = GeoYX2LatLong(c);
%   imagesc(d)
%
%   [e, flag] = LatLong2GeoYX(d);
%   imagesc(e)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = LatLong2GeoYX(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('LatLong2GeoYX'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    %     try
    [flag, that(k)] = LatLong2GeoYX_unit(this(k), varargin{:});
    if ~flag
        return
    end
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
    %     catch
    %         str1 = 'Il semble que vous n''ayez pas suffisemment de m�moire vive pour r�aliser cette fonction.';
    %         str2 = 'It seems you do not have enough RAM to do this processing.';
    %         my_warndlg(Lang(str1,str2), 1);
    %     end
end
my_close(hw)


function [flag, this] = LatLong2GeoYX_unit(this, varargin)

% global SizePhysTotalMemory % Comment� le 05/02/2017 pour obtenir la vraie valeur

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Step] = getPropertyValue(varargin, 'Step', []); %#ok<ASGLU>

%% Controls

flag = 0;
GeometryType = this.GeometryType;

if GeometryType == cl_image.indGeometryType('GeoYX')
    str1 = 'L''image est d�j� en g�om�trie "GeoYX".';
    str2 = 'This image is already in "GeoYX" geometry.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if GeometryType ~= cl_image.indGeometryType('LatLong')
    str1 = 'La g�om�trie de l''image fournie n''est pas de type "LatLong".';
    str2 = 'The geometry of this image is not a "LatLong" one.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

Carto = get(this, 'Carto');
if isempty(Carto) || (get(Carto, 'Projection.Type') == 1) || (get(Carto, 'Ellipsoide.Type') == 1)
    [Carto, flag] = getDefinitionCarto(this);
    if ~flag
        return
    end
    if isempty(Carto)
        str1 = 'La cartographie de l''image fournie n''est pas d�finie';
        str2 = 'The cartography of this image is unknown.';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    this.Carto = Carto;
end

%% Check sizes of matrices

nbRows = numel(subx);
nbCol = numel(suby);
nbBBytesMos = (nbRows * nbCol * 8 * 2) / 1e9;
[SizePhysTotalMemory, SizePhysFreeMemory] = mexPCGetPhysicalMemory;
SizePhysTotalMemory = SizePhysTotalMemory / 1e6;
SizePhysFreeMemory  = SizePhysFreeMemory / 1e6;
if nbBBytesMos > SizePhysTotalMemory
    str1 = sprintf('SonarScope a d�tect� que vous avez peu de m�moire. N''h�sitez pas � interrompre ce traitement si il est trop long.\nSizePhysTotalMemory = %f (Go)  SizePhysFreeMemory = %f (Go)\nTaille de l''image : nbLignes = %d  nbColonnes = %d  Taille = %f (Go)', ...
        SizePhysTotalMemory, SizePhysFreeMemory, nbRows, nbCol, nbBBytesMos);
    str2 = sprintf('You have too few memory space available for this processing. Do not hesitate to stop this processing if it is too long.\nSizePhysTotalMemory = %f (Gb)   SizePhysFreeMemory = %f (Gb)\nSize of the image  nbLines = %d  nbColumns = %d, size = %f (Gb)', ...
        SizePhysTotalMemory, SizePhysFreeMemory, nbRows, nbCol, nbBBytesMos);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'InsufficientMemory');
end

%% Passage des coordonn�es m�triques en coordonn�es g�om�triques

[Lon, Lat] = meshgrid(this.x(subx), this.y(suby));
% if (numel(Lat) * 8 * 2) > SizePhysTotalMemory
%     str1 = 'SonarScope a d�tect� que vous avez peu de m�moire. Il n''est pas possible de faire ce traitement en conservant les donn�es en m�moire. SSc tente une autre strat�gie (donn�es en cache) mail il n''est pas certain que cela soit concluant. De plus, cette m�thode va prendre beaucoup de temps.';
%     str2 = 'You have too few memory space available for this processing. SSc is going to test a B plan but il risks to be very time consuming.';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'InsufficientMemory');
%     Lon = cl_memmapfile('Value', Lon);
%     Lat = cl_memmapfile('Value', Lat);
% end

[n1, n2] = size(Lat);
X1 = NaN(n1, n2);
Y1 = NaN(n1, n2);

% nBlock = ceil((SizePhysTotalMemory * 1000 / 16) / n1);
nBlock = ceil((SizePhysTotalMemory * 1e6 / 32) / n1); % Modiof JMA le 02/12/2016 pour test bug PC Marc
for k2=1:nBlock:n2
    sub2 = k2:min(k2+nBlock-1, n2);
    [X1(:,sub2), Y1(:,sub2)] = latlon2xy(Carto, Lat(:,sub2), Lon(:,sub2));
end
clear Lon Lat

%% Calcul du pas de la grille en coordonn�es g�ographiques

% deltaY1 = abs(mean(diff(Y1(:,1))));
% deltaX1 = abs(mean(diff(X1(1,:))));

if isempty(Step)
    deltaX1 = sqrt(mean(diff(X1(:,1))) ^2 + mean(diff(X1(1,:))) ^ 2);
    deltaY1 = sqrt(mean(diff(Y1(:,1))) ^2 + mean(diff(Y1(1,:))) ^ 2);
    deltaX1 = (deltaX1 + deltaY1) / 2;
    deltaY1 = deltaX1;
else
    deltaX1 = Step;
    deltaY1 = Step;
end

%% Calcul des coordonn�es g�ographiques

xMin = min(X1(:));
xMax = max(X1(:));
yMin = min(Y1(:));
yMax = max(Y1(:));
x = xMin:deltaX1:xMax;
y = yMin:deltaY1:yMax;
[x, deltaX1, xMin, xMax] = centrage_magnetique(x); %#ok
[y, deltaY1, yMin, yMax] = centrage_magnetique(y); %#ok

%% Interpolation de l'image

ClassName = class(this.Image(1));
sizeJ = [length(y) length(x) this.nbSlides];
switch ClassName
    case {'uint8'; 'int8'; 'uint16'; 'int16'; 'uint32'; 'int32'}
        Method = 'nearest';
        try
            J = zeros(sizeJ, ClassName);
        catch %#ok<CTCH>
            J = cl_memmapfile(0, 'Size', sizeJ, 'Format', ClassName);
        end
        ValNaN = 0;
    otherwise
        Method = 'linear';
        try
            J = NaN(sizeJ, ClassName);
        catch %#ok<CTCH>
            J = cl_memmapfile(NaN, 'Size', sizeJ, 'Format', ClassName);
        end
        ValNaN = NaN;
end

for k=1:this.nbSlides
    switch ClassName
        case {'double'; 'single'}
            A =  this.Image(suby,subx,k);
        otherwise
            A =  single(this.Image(suby,subx,k));
            if ~isnan(this.ValNaN)
                A(A == this.ValNaN) = NaN;
            end
    end
    A = fillNaN(A, [3 3]);
    WorkInProgress('Gridding')
%     whos X1 Y1 A x y Method
    Z =  gridding(X1, Y1, A, x, y, 'Method', Method);
    
    if strcmp(ClassName, 'uint8') %#ok<STISA>
        switch this.ImageType
            case 1
                subNaN = isnan(Z);
                Z = uint8(Z);
                Z(subNaN) = this.ValNaN;
                clear subNaN
            case 2
                Z = uint8(Z);
        end
    end
    
    J(:,:,k) = Z;
    clear Z
end
clear X1 Y1 A

this = replace_Image(this, J);
clear I

%% Remise � jour des descripteurs de l'image

this.ValNaN = ValNaN;
this.x = x;
this.y = y;

this = majCoordonnees(this);

%% Calcul des statistiques

this = compute_stats(this);

this.GeometryType = cl_image.indGeometryType('GeoYX');
% this.TagSynchroX = [this.TagSynchroX ' - GeoYX'];
% this.TagSynchroY = [this.TagSynchroY ' - GeoYX'];
TagSynchroXY = shortDescription(this.Carto);
this.TagSynchroX = TagSynchroXY;
this.TagSynchroY = TagSynchroXY;
this.XUnit = 'm';
this.YUnit = 'm';

%% Compl�tion du titre

this = update_Name(this);
Titre = this.Name;

%% Interpolation

this = WinFillNaN(this, 'window', [3 3]);

this.Name = Titre;

%% Apparemment tout s'est bien pass�

flag = 1;
