function [flag, this] = LatLongChangeEllipsoid(this, NameEllipsoidOut, varargin)

% TODO : c'est une �bauche, c'est fait � la va-vite 

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

Carto = this.Carto;
sourceSpheroid = referenceEllipsoid(Carto.MapTbx.Geoid.Code);
targetSpheroid = referenceEllipsoid(NameEllipsoidOut);

DeltaToWGS84 = Carto.MapTbx.Projection.DeltaToWGS84;


% Test

if (DeltaToWGS84.RX == 0) && (DeltaToWGS84.RY == 0) && (DeltaToWGS84.RZ == 0) && (DeltaToWGS84.M == 0)
    x = this.x;
    y = this.y;
%     detax = DeltaToWGS84.DX / this.XStepMetric;
    detax = DeltaToWGS84.DY / this.YStepMetric;
%     detay = DeltaToWGS84.DY / this.YStepMetric;
%     detay = DeltaToWGS84.DX / this.XStepMetric;
    x = x + detax * this.XStep;
%     y = y + detay * this.YStep;
    x = centrage_magnetique(x);
    y = centrage_magnetique(y);
    
    %% Remise � jour des descripteurs de l'image
    
    this.x = x;
    this.y = y;
    this.XLim = compute_XYLim(x);
    this.YLim = compute_XYLim(y);
%     this = update_Name(this);

    Carto = set(Carto, 'Ellipsoide.Type', 11); % wgs84
    this = set_Carto(this, Carto);
    flag = 1;
    return
end



% M�thode qui �tait programm�e par GLU ?

[lon0, lat0] = meshgrid(this.x(subx), this.y(suby));
[flag, CartoOut, lat1, lon1] = latlon2latlonInSpheroidTarget(Carto, lat0, lon0, sourceSpheroid, targetSpheroid);
if ~flag
    return
end

CartoOut = set(CartoOut, 'Projection.Type',  1); % Geodetic

EllipsoidNames = {'wgs84'; 'grs80'; 'clarke80'; 'wgs72'; 'wgs66'; 'international'; 'clarke66'; 'everest'; 'bessel'; 
    'krasovsky'; 'iau65'; 'airy1830'; 'airy1849'; 'wgs60'; 'iau68'};
ind = find(strcmp(EllipsoidNames, NameEllipsoidOut));
ListNomEllipsoidOldfashon = [11 12 14 2 3 4 5 7 8 9 10 1 1 1 1];

% Ellipsoide.Type  <-> '1=Undefined' | '2=wgs72' | '3=wgs66' | '4=international' | '5=clarke66' | '6=clarke80' | '7=everest' | '8=bessel' | '9=krasovsky' | '10=iau65' | {'11=wgs84'} | '12=grs80' | '13=NTF' | '14=clarke80_IGN' | '15=other'
EllipsoideType = ListNomEllipsoidOldfashon(ind); %#ok<FNDSB>
CartoOut = set(CartoOut, 'Ellipsoide.Type', EllipsoideType);

% referenceEllipsoid('clarke80') referenceEllipsoid(7012)
% referenceEllipsoid('grs80')    referenceEllipsoid(7019)
% referenceEllipsoid('wgs84')    referenceEllipsoid(7030) equivalent � wgs84Ellipsoid


%% Calcul du pas de la grille en coordonn�es g�ographiques

deltalon1 = this.XStep;
deltalat1 = this.YStep;

%% Calcul des coordonn�es g�ographiques

xMin = min(lon1(:));
xMax = max(lon1(:));
yMin = min(lat1(:));
yMax = max(lat1(:));
x = xMin:deltalon1:xMax;
y = yMin:deltalat1:yMax;
[x, deltalon1, xMin, xMax] = centrage_magnetique(x); %#ok
[y, deltalat1, yMin, yMax] = centrage_magnetique(y); %#ok

%% Interpolation de l'image

ClassName = class(this.Image(1));
sizeJ = [length(y) length(x) this.nbSlides];
switch ClassName
    case {'uint8'; 'int8'; 'uint16'; 'int16'; 'uint32'; 'int32'}
        Method = 'nearest';
        try
            J = zeros(sizeJ, ClassName);
        catch %#ok<CTCH>
            J = cl_memmapfile(0, 'Size', sizeJ, 'Format', ClassName);
        end
    otherwise
        Method = 'linear';
        try
            J = NaN(sizeJ, ClassName);
        catch %#ok<CTCH>
            J = cl_memmapfile(NaN, 'Size', sizeJ, 'Format', ClassName);
        end
end

for k=1:this.nbSlides
    switch ClassName
        case {'double'; 'single'}
            A =  this.Image(suby,subx,k);
        otherwise
            A =  single(this.Image(suby,subx,k));
            if ~isnan(this.ValNaN)
                A(A == this.ValNaN) = NaN;
            end
    end
    A = fillNaN(A, [3 3]);
    WorkInProgress('Gridding')
%     whos lon1 lat1 A x y Method
    Z =  gridding(lon1, lat1, A, x, y, 'Method', Method);
    
    if strcmp(ClassName, 'uint8') %#ok<STISA>
        switch this.ImageType
            case 1
                subNaN = isnan(Z);
                Z = uint8(Z);
                Z(subNaN) = this.ValNaN;
                clear subNaN
            case 2
                Z = uint8(Z);
        end
    end
    
    J(:,:,k) = Z;
    clear Z
end
clear lon1 lat1 A

this = replace_Image(this, J);
clear I

%% Remise � jour des descripteurs de l'image

this.x = x;
this.y = y;

this = majCoordonnees(this);

%% Calcul des statistiques

this = compute_stats(this);

%% Compl�tion du titre

this = update_Name(this);
Titre = this.Name;

%% Interpolation

this = WinFillNaN(this, 'window', [3 3]);

this.Name = Titre;
this.Carto = CartoOut;

%% Apparemment tout s'est bien pass�

flag = 1;
