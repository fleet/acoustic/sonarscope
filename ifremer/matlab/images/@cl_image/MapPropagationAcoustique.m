
function [flag, a] = MapPropagationAcoustique(this, Frequence, Month, Position, Hauteur, AngleDepart) %#ok<INUSL>

flag = 0;
a    = cl_image.empty;

%% Nom des fichiers de temp�rature et de Salinit�

nomFicTemp1 = getNomFicDatabase('Levitus1x1MonthTemp.nc');
if isempty(nomFicTemp1)
    return
end
nomFicTemp2 = getNomFicDatabase('Levitus1x1SeasonTemp.nc');
if isempty(nomFicTemp2)
    return
end
nomFicTemp3 = getNomFicDatabase('Levitus1x1AnnualTemp.nc');
if isempty(nomFicTemp3)
    return
end

nomFicPsal1 = getNomFicDatabase('Levitus1x1MonthPsal.nc');
if isempty(nomFicPsal1)
    return
end
nomFicPsal2 = getNomFicDatabase('Levitus1x1SeasonPsal.nc');
if isempty(nomFicPsal2)
    return
end
nomFicPsal3 = getNomFicDatabase('Levitus1x1AnnualPsal.nc');
if isempty(nomFicPsal2)
    return
end

%% Cr�ation des objets netcdf

ncTemp1 = cl_netcdf('filename', nomFicTemp1);
if isempty(get(ncTemp1, 'skeleton'))
    return
end

ncTemp2 = cl_netcdf('filename', nomFicTemp2);
if isempty(get(ncTemp2, 'skeleton'))
    return
end

ncTemp3 = cl_netcdf('filename', nomFicTemp3);
if isempty(get(ncTemp3, 'skeleton'))
    return
end

ncPsal1 = cl_netcdf('filename', nomFicPsal1);
if isempty(get(ncPsal1, 'skeleton'))
    return
end

ncPsal2 = cl_netcdf('filename', nomFicPsal2);
if isempty(get(ncPsal2, 'skeleton'))
    return
end

ncPsal3 = cl_netcdf('filename', nomFicPsal3);
if isempty(get(ncPsal3, 'skeleton'))
    return
end

%% Lecture et tri des profondeurs

Depth1 = get_value(ncTemp1, 'STANDARD_LEVEL');
Depth2 = get_value(ncTemp2, 'STANDARD_LEVEL');
Depth3 = get_value(ncTemp3, 'STANDARD_LEVEL');

Date1 = get_value(ncTemp1, 'DATE');
for k=1:size(Date1,1)
    if contains(Date1(k,:), Month)
        iTime1 = k;
        break
    end
end

%% Lecture de la couche mensuelle

Temp1 = get_value(ncTemp1, 'MEAN_VALUE', 'sub2', iTime1);
Temp1 = permute(Temp1, [3 1 2]);
Psal1 = get_value(ncPsal1, 'MEAN_VALUE', 'sub2', iTime1);
Psal1 = permute(Psal1, [3 1 2]);

%% Lecture des couches de Seasonnal compl�mentaires � Month

iTime2 = 1+floor((iTime1-1)/3);
subDepth2 = find(Depth2 > max(Depth1));
Temp2 = get_value(ncTemp2, 'MEAN_VALUE', 'sub1', subDepth2, 'sub2', iTime2);
Temp2 = permute(Temp2, [3 1 2]);
Psal2 = get_value(ncPsal2, 'MEAN_VALUE', 'sub1', subDepth2, 'sub2', iTime2);
Psal2 = permute(Psal2, [3 1 2]);

%% Synth�se de Month et Seasonnal

Depth = [Depth1 Depth2(subDepth2)];
Temp = Temp1;
sub = size(Temp1,3) + (1:size(Temp2,3));
Temp(:,:,sub) = Temp2;
Psal = Psal1;
sub = size(Psal1,3) + (1:size(Psal2,3));
Psal(:,:,sub) = Psal2;
clear Temp1 Temp2 Psal1 Psal2

%% Lecture des couches Annual  compl�mentaires � Seasonnal

subDepth3 = find(Depth3 > max(Depth2));
if ~isempty(subDepth3) % Non v�rifi�
    Temp3 = get_value(ncTemp2, 'MEAN_VALUE', 'sub1', subDepth3);
    Temp3 = permute(Temp3, [3 1 2]);
    Psal3 = get_value(ncPsal2, 'MEAN_VALUE', 'sub1', subDepth3);
    Psal3 = permute(Psal3, [3 1 2]);
    
    Depth = [Depth Depth3(subDepth3)];
    sub = size(Temp,3) + (1:size(Temp3,3));
    Temp(:,:,sub) = Temp3;
    sub = size(Psal,3) + (1:size(Psal3,3));
    Psal(:,:,sub) = Psal3;
    clear Temp3 Psal3
end

%% Gestion des non valeurs

ValNaN = get_valVarAtt(ncTemp1, 'MEAN_VALUE', '_FillValue');
subNaN = (Temp == ValNaN);
Temp(subNaN) = NaN;
ValNaN = get_valVarAtt(ncPsal1, 'MEAN_VALUE', '_FillValue');
subNaN = (Psal == ValNaN);
Psal(subNaN) = NaN;
clear subNaN

%% Lecture des coordonn�es

Lat1 = get_value(ncTemp1, 'LATITUDE');
Lon1 = get_value(ncTemp1, 'LONGITUDE');

%% Lecture Etopo et interpolation aux noeux de la grille Levitus

nomFicEtopo = getNomFicDatabase('TerreEtopo.mat');
Z    = loadmat(nomFicEtopo, 'nomVar', 'Image');
Info = loadmat(nomFicEtopo, 'nomVar', 'Info');
z = -interp2(Info.x, Info.y', Z, Lon1, Lat1');  

%% Calcul du coefficient d'att�nuation

Absoprtion        = NaN(length(Lat1), length(Lon1), 'single');
DepthLevitus      = NaN(length(Lat1), length(Lon1), 'single');
CeleriteSource    = NaN(length(Lat1), length(Lon1), 'single');
CeleriteSeabottom = NaN(length(Lat1), length(Lon1), 'single');
NLon = size(Temp, 2);
str1 = 'Calcul des param�tres acoustiques';
str2 = 'Computing acoustic parameters';
hw = create_waitbar(Lang(str1,str2), 'N', NLon);
for iLon=1:NLon
    my_waitbar(iLon, NLon, hw)
    for iLat=1:size(Temp, 1)
        if isnan(z(iLat,iLon))
            continue
        end
        T = Temp(iLat,iLon,:);
        sub = find(~isnan(T));
        if length(sub) >= 2 % TODO : regrader pourquoi �a ne fonctionne pas pour 1 seul point
            P = Psal(iLat,iLon,:);
            T = T(sub);
            P = P(sub);
            T = squeeze(T);
            P = squeeze(P);
            C = celeriteChen(-Depth(sub), T', P');
            if Position == 1 % Sonar en surface
                [~, CoeffIntegre] = AttenuationGarrison(Frequence, Depth(sub), T, P);
                CeleriteSource(iLat,iLon) = C(1);
            else % Sonar � une hauteur constante par rapport au fond
                Immersion = max(0, z(iLat,iLon) - Hauteur);
                [~, CoeffIntegre] = AttenuationGarrison(Frequence, Depth(sub), T, P, 'Immersion', -Immersion);
                CeleriteSource(iLat,iLon) = interp1(Depth(sub), C, -Immersion, 'linear', 'extrap');
            end
            Absoprtion(iLat,iLon) = CoeffIntegre(end);
            
            if z(iLat,iLon) > Depth(sub(end))
                Absoprtion(iLat,iLon) = interp1(Depth(sub), CoeffIntegre, z(iLat,iLon), 'linear', 'extrap');
                CeleriteSeabottom(iLat,iLon) = interp1(Depth(sub), C, z(iLat,iLon), 'linear', 'extrap');
            elseif z(iLat,iLon) < Depth(sub(1))
                Absoprtion(iLat,iLon) = interp1(Depth(sub), CoeffIntegre, z(iLat,iLon));
                CeleriteSeabottom(iLat,iLon) = interp1(Depth(sub), C, z(iLat,iLon));
            else
                Absoprtion(iLat,iLon) = interp1(Depth(sub), CoeffIntegre, z(iLat,iLon));
                CeleriteSeabottom(iLat,iLon) = interp1(Depth(sub), C, z(iLat,iLon));
            end
            
%             DepthLevitus(iLat,iLon) = Depth(sub(end));
            DepthLevitus(iLat,iLon) = -z(iLat,iLon);
        end
    end
end
my_close(hw, 'MsgEnd')

%% Cr�ation de l'image

if Position == 1
    Name = sprintf('Freq=%skHz-%s', num2str(Frequence), Month);
else
    Name = sprintf('Freq=%skHz-Hauteur/seabed=%sm-%s', num2str(Frequence), num2str(Hauteur), Month);
end
a = cl_image('Image', Absoprtion, 'Unit', 'dB/km', ...
    'DataType', cl_image.indDataType('AbsorptionCoeff'), 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'x', double(Lon1), 'y', double(Lat1), ...
    'XLabel', 'Lat', 'YLabel', 'Long', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LatitudeAbsorption', 'TagSynchroY', 'LongitudeAbsorption', ...
    'Name', Name, 'InitialFileName', nomFicTemp1, 'InitialFileFormat', 'Netcdf');
a = update_Name(a);

a(2) = cl_image('Image', DepthLevitus, 'Unit', 'm', ...
    'DataType', cl_image.indDataType('Bathymetry'), 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'x', double(Lon1), 'y', double(Lat1), ...
    'XLabel', 'Lat', 'YLabel', 'Long', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LatitudeAbsorption', 'TagSynchroY', 'LongitudeAbsorption', ...
    'Name', Name, 'InitialFileName', nomFicEtopo, 'InitialFileFormat', 'Netcdf');
a(2) = update_Name(a(2));

a(3) = cl_image('Image', CeleriteSource, 'Unit', 'm/s', ...
    'DataType', cl_image.indDataType('SoundVelocity'), 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'x', double(Lon1), 'y', double(Lat1), ...
    'XLabel', 'Lat', 'YLabel', 'Long', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LatitudeAbsorption', 'TagSynchroY', 'LongitudeAbsorption', ...
    'Name', Name, 'InitialFileName', nomFicEtopo, 'InitialFileFormat', 'Netcdf');
a(3) = update_Name(a(3), 'Append', 'Antenna');

a(4) = cl_image('Image', CeleriteSeabottom, 'Unit', 'm/s', ...
    'DataType', cl_image.indDataType('SoundVelocity'), 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'x', double(Lon1), 'y', double(Lat1), ...
    'XLabel', 'Lat', 'YLabel', 'Long', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LatitudeAbsorption', 'TagSynchroY', 'LongitudeAbsorption', ...
    'Name', Name, 'InitialFileName', nomFicEtopo, 'InitialFileFormat', 'Netcdf');
a(4) = update_Name(a(4), 'Append', 'Seabed');

Rapport = CeleriteSeabottom ./ CeleriteSource;
a(5) = cl_image('Image', Rapport, 'Unit', '-', ...
    'DataType', 1, 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'x', double(Lon1), 'y', double(Lat1), ...
    'XLabel', 'Lat', 'YLabel', 'Long', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LatitudeAbsorption', 'TagSynchroY', 'LongitudeAbsorption', ...
    'Name', Name, 'InitialFileName', nomFicEtopo, 'InitialFileFormat', 'Netcdf');
a(5) = update_Name(a(5), 'Append', 'RapportCelerity');

AngleSeabed = sind(AngleDepart) .* Rapport;
AngleSeabed(abs(AngleSeabed) > 1) = NaN;
AngleSeabed = asind(AngleSeabed);
a(6) = cl_image('Image', AngleSeabed, 'Unit', 'deg', ...
    'DataType', cl_image.indDataType('RxBeamAngle'), 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'x', double(Lon1), 'y', double(Lat1), ...
    'XLabel', 'Lat', 'YLabel', 'Long', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LatitudeAbsorption', 'TagSynchroY', 'LongitudeAbsorption', ...
    'Name', Name, 'InitialFileName', nomFicEtopo, 'InitialFileFormat', 'Netcdf');
a(6) = update_Name(a(6), 'Append', 'ArrivalAngle');

flag = 1;
