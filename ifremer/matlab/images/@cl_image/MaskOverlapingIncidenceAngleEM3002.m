function this = MaskOverlapingIncidenceAngleEM3002(this, TxBeamIndex)

if isempty(TxBeamIndex) % pr�vu pour EM2040 dual mais a priori pas utile
    sub = true(1,this.nbColumns);
    ns2 = floor(this.nbColumns / 2);
    sub(ns2+1:end) = false;
    sub = repmat(sub, this.nbRows, 1);
else % Pour EM3002
    sub = TxBeamIndex.Image(:,:) == 1;
end
I = this.Image(:,:);

V = I(sub);
V(V > 0) = NaN;
I(sub) = V;

sub = ~sub;
V = I(sub);
V(V < 0) = NaN;
I(sub) = V;

this.Image(:,:) = I;
