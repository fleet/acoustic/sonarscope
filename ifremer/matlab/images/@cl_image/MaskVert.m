% TODO : masque de l'image au dela d'une valeur donn�e pour chaque colonne
% (num�ro de ligne). Cela sert au masquage des donn�es brutes de colonne
% d'eau

function [flag, that] = MaskVert(this, varargin)

N = length(this);
str1 = 'Masquage X en cours';
str2 = 'Processing MaskX';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = MaskVert_unitaire(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = X; %#ok<AGROW>
end
my_close(hw, 'MsgEnd');


function [flag, this] = MaskVert_unitaire(this, varargin)

[varargin, kY]      = getPropertyValue(varargin, 'kY',  []);
[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

% TODO : pr�voir de pouvoir passer les valeur soit en num�ro de ligne soit
% en y et passer un odre Anant/Apr�s
if isempty(kY)
    flag = 0;
    return
end

%% Calcul du masque 

kY = floor(kY);
for k=1:this.nbColumns
    if ~isinf(kY(k)) && ~isnan(kY(k))
        this.Image(kY(k):end,k) = this.ValNaN;
    end
end

%% Calcul des statistiques

if ~NoStats
    this = compute_stats(this);
end

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', 'MaskVert');

flag = 1;
