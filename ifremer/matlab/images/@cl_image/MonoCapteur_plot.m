function [flag, X, Y, V, repImport] = MonoCapteur_plot(this, repImport, varargin)

[varargin, NbLigEntete]  = getPropertyValue(varargin, 'NbLigEntete',    []);
[varargin, ValNaN]       = getPropertyValue(varargin, 'ValNaN',         []);
[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType',   []);
[varargin, IndexColumns] = getPropertyValue(varargin, 'IndedexColumns', [1 2 3 4 5]);
[varargin, sub]          = getPropertyValue(varargin, 'sub',            []);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',          []); %#ok<ASGLU>

X = [];
Y = [];
V = [];

%% Saisie des noms de fichiers image

[flag, ListeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.xyz'; '.*'}, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select some files');
if ~flag
    return
end

%% Lecture de l'ent�te du premier fichier

fid = fopen(ListeFic{1}, 'r');
if fid == -1
    [nomDir, nomFic] = fileparts(ListeFic{1});
    nomFic = fullfile(nomDir, [nomFic '.xyz']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end

for i=1:20
    Entete{i} = fgetl(fid);%#ok
end
fclose(fid);

%% Decodage guid� par l'op�rateur

str = sprintf(Lang('Debut du fichier %', 'Begin of the file %s'), ListeFic{1});
LigneTemoin = Entete{end};
mots = strsplit(LigneTemoin);

Entete{end+1} = ' ';
Entete{end+1} = Lang('Information sur le contenu des colonnes pour la derni�re ligne', 'Information about columns content for last line');
for i=1:length(mots)
    Entete{end+1} = sprintf('Column %d : %s', i, mots{i}); %#ok
end
my_txtdlg(str, Entete, 'windowstyle', 'normal');


if isempty(NbLigEntete)
    [flag, NbLigEntete] = inputOneParametre('xyz importation', Lang('Nb de lignes d''entete', 'Nb of Header Lines'), ...
        'Value', 0, 'MinValue', 0, 'Format', '%d');
    if ~flag
        return
    end
elseif NbLigEntete < 0
    [flag, NbLigEntete] = inputOneParametre('xyz importation', Lang('Nb de lignes d''entete', 'Nb of Header Lines'), ...
        'Value', -NbLigEntete, 'MinValue', 0, 'Format', '%d');
    if ~flag
        return
    end
end

if isempty(ValNaN)
    str1 = 'Sp�cifier la valeur pour "NO-DATA" si besoin';
    str2 = 'Specific value for "No Data" if any';
    [ValNaN, flag] = my_inputdlg(Lang({str1},{str2}), {''});
    if ~flag
        return
    end
    ValNaN = ValNaN{1};
    if ~isempty(ValNaN)
        ValNaN = str2double(ValNaN);
    end
end


if isempty(GeometryType)
    [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', 3);
    if ~flag
        return
    end
elseif GeometryType < 0
    [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', -GeometryType);
    if ~flag
        return
    end
end
switch GeometryType
    case cl_image.indGeometryType('LatLong')
        [flag, indD, indH, indY, indX, indV] = getColumnOrder('Date', 'Hour', 'Latitude', 'Longitude', 'Data', IndexColumns);
    otherwise
        [flag, indD, indH, indX, indY, indV] = getColumnOrder('Date', 'Hour', 'X', 'Y', 'Data', IndexColumns);
end
if ~flag
    return
end

ZLim = [];
nbColors = 64;
Colors = jet(nbColors);

Fig1 = [];
NbFic = length(this);
hw = create_waitbar('Plot .all Runtime Parameters', 'N', NbFic);
for iFic=1:NbFic
    my_waitbar(i, NbFic, hw);
    
    [Time, X, Y, V] = lectureDirecte(ListeFic{iFic}, NbLigEntete, indD, indH, indX, indY, indV);
    
    if ~isempty(sub)
        X = X(sub);
        Y = Y(sub);
        V = V(sub);
    end
    
    if ~isempty(ValNaN)
        V(V == ValNaN) = NaN;
    end
    
    if isempty(Carto)
        if GeometryType == cl_image.indGeometryType('GeoYX')
            [flag, Carto] = editParams(cl_carto([]), [], [], 'MsgInit');
            if ~flag
                return
            end
            [Y, X] = xy2Latlon(Carto, X, Y);
        end
    end
    
    Fig1 = plot_navigation(ListeFic{iFic}, Fig1, X, Y, Time.timeMat, []);
    
    if isempty(ZLim)
        minV = min(V);
        maxV = max(V);
        
        p    = ClParametre('Name', 'Min Value', ...
            'Value', minV);
        p(2) = ClParametre('Name', 'Max Value', ...
            'Value', maxV);
        a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        ZLim = a.getParamsValue;
    end
    
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((V - ZLim(1)) * coef);
    
    for i=1:nbColors
        sub = find(Value == (i-1));
        if ~isempty(sub)
            h = plot(X(sub), Y(sub), '+');
            set(h, 'Color', Colors(i,:), 'Tag', ListeFic{iFic})
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(X(sub), Y(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8, 'Tag', ListeFic{iFic})
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(X(sub), Y(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', ListeFic{iFic})
    end
end
my_close(hw, 'MsgEnd');

axisGeo;
grid on;
drawnow

my_colorbar('ZLim', ZLim)
flag = 1;



function [flag, ind1, ind2, ind3, ind4, ind5] = getColumnOrder(nom1, nom2, nom3, nom4, nom5, IndexColumns)

nomVar = {nom1, nom2, nom3, nom4, nom5};
for i=1:5
    value{i} = num2str(IndexColumns(i)); %#ok<AGROW>
end
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    ind1 = [];
    ind2 = [];
    ind3 = [];
    ind4 = [];
    ind5 = [];
    return
end
ind1 = str2num(value{1}); %#ok<ST2NM>
ind2 = str2num(value{2}); %#ok<ST2NM>
ind3 = str2num(value{3}); %#ok<ST2NM>
ind4 = str2num(value{4}); %#ok<ST2NM>
ind5 = str2num(value{5}); %#ok<ST2NM>


function [Time, X, Y, V] = lectureDirecte(nomFic, NbLigEntete, indD, indH, indX, indY, indV)

Time = [];
X = [];
Y = [];
V = [];

fid = fopen(nomFic, 'r');
if fid == -1
    return
end

for i=1:NbLigEntete
    Entete{i} = fgetl(fid); %#ok
end

k = 0;
Data = [];
pppp = dir(nomFic);
tailleFic = pppp.bytes;
str = sprintf('Reading data from %s', nomFic);
hw = create_waitbar(str, 'N', tailleFic);
while 1
    tailleLue = ftell(fid);
    my_waitbar(tailleLue, tailleFic, hw);
    Line = fgetl(fid);
    if ~ischar(Line)
        break
    end
    Line = regexprep(Line, {'/',; ':'}, ' ');
    Valeurs = sscanf(Line, '%f');
    k = k + 1;
    if k > size(Data,1)
        Data(k+10000, 1:length(Valeurs)) = 0; %#ok<AGROW>
    end
    Data(k,:) = Valeurs; %#ok<AGROW>
end
my_close(hw, 'MsgEnd');
fclose(fid);
Data = Data(1:k,:);

if indD < indH
    Jour = Data(:,indD);
    Data = Data(:, [1:indD-1 indD+1:end]);
    Mois = Data(:,indD);
    Data = Data(:, [1:indD-1 indD+1:end]);
    Annee = Data(:,indD);
    
    Heure = Data(:,indH);
    Data = Data(:, [1:indH-1 indH+1:end]);
    Minute = Data(:,indH);
    Data = Data(:, [1:indH-1 indH+1:end]);
    Seconde = Data(:,indH);
else
    Heure = Data(:,indH);
    Data = Data(:, [1:indH-1 indH+1:end]);
    Minute = Data(:,indH);
    Data = Data(:, [1:indH-1 indH+1:end]);
    Seconde = Data(:,indH);
    
    Jour = Data(:,indD);
    Data = Data(:, [1:indD-1 indD+1:end]);
    Mois = Data(:,indD);
    Data = Data(:, [1:indD-1 indD+1:end]);
    Annee = Data(:,indD);
end

X = Data(:,indX);
Y = Data(:,indY);
V = Data(:,indV);

d = dayJma2Ifr(Jour, Mois, Annee);
h = hourHms2Ifr(Heure, Minute, Seconde);
Time = cl_time('timeIfr', d, h);
