function [flag, a] = MultiImages_Max(this, ImageName, varargin)

[varargin, XLim] = getPropertyValue(varargin, 'XLim', []);
[varargin, YLim] = getPropertyValue(varargin, 'YLim', []);
[varargin, ComputationAbs] = getPropertyValue(varargin, 'ComputationAbs', 0); %#ok<ASGLU>

if isempty(XLim)
    XLim = [Inf -Inf];
    YLim = [Inf -Inf];
    for k=1:length(this)
        [XL, YL] = getXLimYLimForCrop_image(this(k));
        XLim(1) = min(XLim(1), min(XL));
        XLim(2) = max(XLim(2), max(XL));
        YLim(1) = min(YLim(1), min(YL));
        YLim(2) = max(YLim(2), max(YL));
    end
end

XStep = get(this(1), 'XStep');
YStep = get(this(1), 'YStep');
x = XLim(1):XStep:XLim(2);
y = YLim(1):YStep:YLim(2);
x = centrage_magnetique(x);
y = centrage_magnetique(y);

I = -Inf(length(y), length(x), 1, 'double');
nbImages = length(this);
str1 = 'Min d''un ensemble d''images';
str2 = 'Min of a set of images';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw);
    a = this(k);
    
    subx = find((x >= min(a.x)) & (x <= max(a.x)));
    suby = find((y >= min(a.y)) & (y <= max(a.y)));
    val = get_pixels_xy(a, x(subx), y(suby));
    sub = isnan(val);
    if all(sub(:))
        continue
    end
    subxx = subx;
    subyy = suby;
    
    if ComputationAbs
        val = abs(val);
    end
    
    I(subyy, subxx) = max(I(subyy, subxx), val);
end
my_close(hw, 'MsgEnd')
I(isinf(I)) = NaN;

TagSynchroX   = this(1).TagSynchroX;
TagSynchroY   = this(1).TagSynchroY;
Unit          = this(1).Unit;
DataType      = this(1).DataType;
GeometryType  = this(1).GeometryType;
Carto         = get_Carto(this(1));
XUnit         = this(1).XUnit;
YUnit         = this(1).YUnit;
ColormapIndex = this(1).ColormapIndex;

a = cl_image('Image', I, ...
    'Name',          ImageName, ...
    'Unit',          Unit, ...
    'x',             x, ...
    'y',             y, ...
    'XUnit',         XUnit, ...
    'YUnit',         YUnit, ...
    'ColormapIndex', ColormapIndex, ...
    'DataType',      DataType, ...
    'GeometryType',  GeometryType, ...
    'TagSynchroX',   TagSynchroX, ...
    'TagSynchroY',   TagSynchroY, ...
    'Carto',         Carto);
% 'InitialFileName',      nomFic,
% 'InitialFileFormat', 'ASCII xyz', ...
a = update_Name(a);

if ~isempty(Carto)
    a = set(a, 'Carto', Carto);
end

flag = 1;
