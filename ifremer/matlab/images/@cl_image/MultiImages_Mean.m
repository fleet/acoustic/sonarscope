function [flag, a] = MultiImages_Mean(this, ImageName, varargin)

[varargin, XLim]            = getPropertyValue(varargin, 'XLim',            []);
[varargin, YLim]            = getPropertyValue(varargin, 'YLim',            []);
[varargin, NoWaitbar]       = getPropertyValue(varargin, 'NoWaitbar',       0);
[varargin, ComputationUnit] = getPropertyValue(varargin, 'ComputationUnit', []); %#ok<ASGLU>

if isempty(XLim)
    XLim = [Inf -Inf];
    YLim = [Inf -Inf];
    for k=1:length(this)
        [XL, YL] = getXLimYLimForCrop_image(this(k));
        XLim(1) = min(XLim(1), min(XL));
        XLim(2) = max(XLim(2), max(XL));
        YLim(1) = min(YLim(1), min(YL));
        YLim(2) = max(YLim(2), max(YL));
    end
end

XStep = get(this(1), 'XStep');
YStep = get(this(1), 'YStep');
x = XLim(1):XStep:XLim(2);
y = YLim(1):YStep:YLim(2);
x = centrage_magnetique(x);
y = centrage_magnetique(y);

I = zeros(length(y), length(x), 1, 'double');
N = zeros(length(y), length(x), 1, 'single');
nbImages = length(this);
str1 = 'Moyennage d''un ensemble d''images';
str2 = 'Averaging of a set of images';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages, 'NoWaitbar', NoWaitbar);
for k=1:nbImages
    my_waitbar(k, nbImages, hw);
    a = this(k);
    
    subx = find((x >= min(a.x)) & (x <= max(a.x)));
    suby = find((y >= min(a.y)) & (y <= max(a.y)));
    val = get_pixels_xy(a, x(subx), y(suby));
    sub = isnan(val);
    if all(sub(:))
        continue
    end
    subxx = subx;
    subyy = suby;
    
    V = val;
    if ~isempty(ComputationUnit)
        switch ComputationUnit
            case 'Amp'
                V = reflec_dB2Amp(V);
            case 'Enr'
                V = reflec_dB2Enr(V);
        end
    end
    
    V(sub) = 0;
    
    W = ones(size(V), 'single');
    W(sub) = 0;
    
    I(subyy, subxx) = I(subyy, subxx) + V;
    N(subyy, subxx) = N(subyy, subxx) + W;
    
%     figure(7675); imagesc(I); colormap(jet(256)); axis xy
end
my_close(hw, 'MsgEnd')
I = I ./ N;
I(N == 0) = NaN;

% figure(7675); imagesc(I); colormap(jet(256)); axis xy

if ~isempty(ComputationUnit)
        switch ComputationUnit
            case 'Amp'
                I = reflec_Amp2dB(I);
            case 'Enr'
                I = reflec_Enr2dB(I);
        end
end

TagSynchroX   = this(1).TagSynchroX;
TagSynchroY   = this(1).TagSynchroY;
Unit          = this(1).Unit;
DataType      = this(1).DataType;
GeometryType  = this(1).GeometryType;
Carto         = get_Carto(this(1));
XUnit         = this(1).XUnit;
YUnit         = this(1).YUnit;
ColormapIndex = this(1).ColormapIndex;

a = cl_image('Image', I, ...
    'Name',          ImageName, ...
    'Unit',          Unit, ...
    'x',             x, ...
    'y',             y, ...
    'XUnit',         XUnit, ...
    'YUnit',         YUnit, ...
    'ColormapIndex', ColormapIndex, ...
    'DataType',      DataType, ...
    'GeometryType',  GeometryType, ...
    'TagSynchroX',   TagSynchroX, ...
    'TagSynchroY',   TagSynchroY, ...
    'Carto',         Carto);
a = update_Name(a);

if ~isempty(Carto)
    a = set(a, 'Carto', Carto);
end

DataType = cl_image.indDataType('AveragedPtsNb');
a(2) = cl_image('Image', N, ...
    'Name',          ImageName, ...
    'Unit',          '-', ...
    'x',             x, ...
    'y',             y, ...
    'XUnit',         XUnit, ...
    'YUnit',         YUnit, ...
    'ColormapIndex', 3, ...
    'DataType',      DataType, ...
    'GeometryType',  GeometryType, ...
    'TagSynchroX',   TagSynchroX, ...
    'TagSynchroY',   TagSynchroY, ...
    'Carto',          Carto);
a(2) = update_Name(a(2));

if ~isempty(Carto)
    a(2) = set(a(2), 'Carto', Carto);
end

flag = 1;
