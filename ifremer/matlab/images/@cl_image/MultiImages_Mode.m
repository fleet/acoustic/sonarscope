function [flag, a] = MultiImages_Mode(this, ImageName, varargin)

[varargin, XLim] = getPropertyValue(varargin, 'XLim', []);
[varargin, YLim] = getPropertyValue(varargin, 'YLim', []); %#ok<ASGLU>

if isempty(XLim)
    XLim = [Inf -Inf];
    YLim = [Inf -Inf];
    for k=1:length(this)
        [XL, YL] = getXLimYLimForCrop_image(this(k));
        XLim(1) = min(XLim(1), min(XL));
        XLim(2) = max(XLim(2), max(XL));
        YLim(1) = min(YLim(1), min(YL));
        YLim(2) = max(YLim(2), max(YL));
    end
end

XStep = get(this(1), 'XStep');
YStep = get(this(1), 'YStep');
x = XLim(1):XStep:XLim(2);
y = YLim(1):YStep:YLim(2);
x = centrage_magnetique(x);
y = centrage_magnetique(y);

nbCol    = length(x);
nbRows    = length(y);
nbImages = length(this);

try
    ValMode = NaN([nbRows, nbCol], 'single');
catch
    ValMode = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

ValPingImages = NaN([nbCol, nbImages]);

str1 = 'Calcul de l''image mode';
str2 = 'Computing the mode image';
hw = create_waitbar(Lang(str1,str2), 'N', nbRows);
for k1=1:nbRows
    my_waitbar(k1, nbRows, hw);
    ValPingImages(:) = NaN;
    for k2=1:nbImages
        a = this(k2);
        
        subx = find((x >= min(a.x)) & (x <= max(a.x)));
        
        if (y(k1) >= min(a.y)) && (y(k1) <= max(a.y))
            val = get_pixels_xy(a, x(subx), y(k1));
            val(val == 0) = NaN;
            ValPingImages(subx, k2) = val;
        end
    end
    ValMode(k1,:) = grpstats(ValPingImages', [], 'mode');
end
my_close(hw, 'MsgEnd')

TagSynchroX   = this(1).TagSynchroX;
TagSynchroY   = this(1).TagSynchroY;
Unit          = this(1).Unit;
DataType      = this(1).DataType;
GeometryType  = this(1).GeometryType;
Carto         = get_Carto(this(1));
XUnit         = this(1).XUnit;
YUnit         = this(1).YUnit;
ColormapIndex = this(1).ColormapIndex;

a = cl_image('Image', ValMode, ...
    'Name',           ImageName, ...
    'Unit',           Unit, ...
    'x',              x, ...
    'y',              y, ...
    'XUnit',          XUnit, ...
    'YUnit',          YUnit, ...
    'ColormapIndex',  ColormapIndex, ...
    'DataType',       DataType, ...
    'GeometryType',   GeometryType, ...
    'TagSynchroX',    TagSynchroX, ...
    'TagSynchroY',    TagSynchroY, ...
    'Carto',          Carto);
a = update_Name(a);

if ~isempty(Carto)
    a = set(a, 'Carto', Carto);
end

flag = 1;
