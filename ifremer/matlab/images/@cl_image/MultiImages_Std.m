function [flag, a] = MultiImages_Std(this, XLim, YLim, ImageName)

% TODO : c'est pas top de faire �a (Amp si reflectivity) !!!
% Faut �tre coh�rent

[flag, Mean] = MultiImages_Mean(this, ImageName, 'XLim', XLim, 'YLim', YLim, 'ComputationUnit', 'Amp');
a = [];
if ~flag
    return
end
Mean = Mean(1).Image(:,:);

XStep = get(this(1), 'XStep');
YStep = get(this(1), 'YStep');
x = XLim(1):XStep:XLim(2);
y = YLim(1):YStep:YLim(2);
x = centrage_magnetique(x);
y = centrage_magnetique(y);

I = zeros(length(y), length(x), 1, 'double');
N = zeros(length(y), length(x), 1, 'single');
nbImages = length(this);
str1 = 'Moyennage d''un ensemble d''images';
str2 = 'Averaging of a set of images';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw);
    a = this(k);
    
    subx = find((x >= min(a.x)) & (x <= max(a.x)));
    suby = find((y >= min(a.y)) & (y <= max(a.y)));
    val = get_pixels_xy(a, x(subx), y(suby));
    sub = isnan(val);
    if all(sub(:))
        continue
    end
    subxx = subx;
    subyy = suby;
    
    V = val - Mean(subyy, subxx);
    V(sub) = 0;
    
    W = ones(size(V), 'single');
    W(sub) = 0;
    
    I(subyy, subxx) = I(subyy, subxx) + V.^2;
    N(subyy, subxx) = N(subyy, subxx) + W;
end
my_close(hw, 'MsgEnd')
I = sqrt(I ./ N);
I(N == 0) = NaN;

TagSynchroX   = this(1).TagSynchroX;
TagSynchroY   = this(1).TagSynchroY;
Unit          = this(1).Unit;
DataType      = this(1).DataType;
GeometryType  = this(1).GeometryType;
Carto         = get_Carto(this(1));
XUnit         = this(1).XUnit;
YUnit         = this(1).YUnit;
ColormapIndex = this(1).ColormapIndex;

a = cl_image('Image', I, ...
    'Name',          ImageName, ...
    'Unit',           Unit, ...
    'x',              x, ...
    'y',              y, ...
    'XUnit',          XUnit, ...
    'YUnit',          YUnit, ...
    'ColormapIndex',  ColormapIndex, ...
    'DataType',       DataType, ...
    'GeometryType',   GeometryType, ...
    'TagSynchroX',    TagSynchroX, ...
    'TagSynchroY',    TagSynchroY, ...
    'Carto',          Carto);
% 'InitialFileName',      nomFic, 
% 'InitialFileFormat', 'ASCII xyz', ...
a = update_Name(a);

if ~isempty(Carto)
    a = set(a, 'Carto', Carto);
end

DataType = cl_image.indDataType('AveragedPtsNb');
a(2) = cl_image('Image', N, ...
    'Name',          ImageName, ...
    'Unit',          '-', ...
    'x',             x, ...
    'y',             y, ...
    'XUnit',         XUnit, ...
    'YUnit',         YUnit, ...
    'ColormapIndex', 3, ...
    'DataType',      DataType, ...
    'GeometryType',  GeometryType, ...
    'TagSynchroX',   TagSynchroX, ...
    'TagSynchroY',   TagSynchroY, ...
    'Carto',         Carto);
a(2) = update_Name(a(2));

if ~isempty(Carto)
    a(2) = set(a(2), 'Carto', Carto);
end

flag = 1;
