function [flag, a] = NORBIT_CompositMosaic(~, ListeFicErs, repExport)

flag = 0;
a = cl_image.empty;

I0 = cl_image_I0;

listFiles  = {};
listFreq   = [];
indexFiles = [];
for k=1:length(ListeFicErs)
    [~, filename] = fileparts(ListeFicErs{k});
    filename = strrep(filename, '_Snippets100', '');
    filename = strrep(filename, '_Compensation', '');
    filename = strrep(filename, '_WinFillNaN', '');
    filename = strrep(filename, '_LatLong_Reflectivity', '');
    filename = strrep(filename, '_Reflectivity_LatLong', '');
    mots = strsplit(filename, '-');
	freq = str2double(mots{2}(1:end-3));
    if ismember(mots{1}, listFiles)
        listFreq(  end, 2) = freq;
        indexFiles(end, 2) = k;
    else
        listFiles{ end+1, 1} = mots{1}; %#ok<AGROW>
        listFreq(  end+1, 1) = freq; %#ok<AGROW>
        indexFiles(end+1, 1) = k; %#ok<AGROW>
    end
end

%% Boucle sur les fichiers

N = length(listFiles);
for k=1:N
    Freq = listFreq(k,:);
    Freq(Freq == 0) = [];
    if length(Freq) ~= 2
        continue
    end
    [~, ordre] = sort(Freq);
    Freq = Freq(ordre);
    indexFiles(k,:) = indexFiles(k, ordre); %#ok<AGROW>
    
    %% Lecture de l'image de la premi�re fr�quence
    
    nomFic1 = ListeFicErs{indexFiles(k, 1)};
    [flag, Image1] = cl_image.import_ermapper(nomFic1);
    if ~flag
        continue
    end
    
    %% Lecture de l'image de la deuxi�me fr�quence
    
    nomFic2 = ListeFicErs{indexFiles(k, 2)};
    [flag, Image2] = cl_image.import_ermapper(nomFic2);
    if ~flag
        continue
    end
    
    %% Composition des layers en image RGB
    
    [flag, RGB] = composeColoredImageFrom2Frequencies(Image1, Image2, listFiles{k,1}, 'Freq', Freq);
    if flag
        a(end+1) = RGB; %#ok<AGROW>
    end
    
    %{
    %% Extraction de l'image2 sur le m�me cadre g�ographique que Image1
    
    Image2 = extraction(Image2, 'x', Image1.x, 'y', Image1.y, 'ForceExtraction');
    
    %% Transformation des images d'intensit� en images index�es
        
    [Image1Ind, flag] = Intensity2Indexed(Image1, 'CLim', Image1.StatValues.Quant_25_75);
    if ~flag
        continue
    end
    [Image2Ind, flag] = Intensity2Indexed(Image2, 'CLim', Image2.StatValues.Quant_25_75);
    if ~flag
        continue
    end
    
    %% Cr�ation du troisi�me canal
    
    I3Ind = uint8(0.5 * single(Image1Ind.Image) + 0.5 * single(Image2Ind.Image));
    
    RGB = Image1Ind.Image;
    RGB(:,:,3) = Image2Ind.Image;
    RGB(:,:,2) = I3Ind;
    
    %% Output image
    
    a(end+1) = inherit(Image1, RGB, 'ValNaN', 255, 'ImageType', 2, 'Unit', ' ', 'ColormapIndex', 2); %#ok<AGROW>
    ImageName = code_ImageName(a(end), 'Name', listFiles{k,1});
    a(end).Name = ImageName;
    a(end).Comments = sprintf('Red=%dkHz, Geen=%dkHz, Blue=0.5*Red+0.5*Green', Freq(1), Freq(2));
%     SonarScope(a(k))
%}

    %% Export des mosa�ques
    
    ImageName = a(end).Name;
    nomFic = fullfile(repExport, [ImageName '_Composit.ers']);
    flag = export_ermapper(a(end), nomFic);
    
    if N > 1
        a(end) = optimiseMemory(a(end), 'SizeMax', 0);
    end
end
