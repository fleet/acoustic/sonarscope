function [NomLayer, Unite] = NomLayerCaraibesGrid(this)

switch this.DataType
    case  cl_image.indDataType('Reflectivity')
        NomLayer = 'REFLECTIVITY';
        Unite = 'dB';
    case  cl_image.indDataType('Bathymetry')
        NomLayer = 'DEPTH';
        Unite = 'metres';
    case cl_image.indDataType('Latitude')
        NomLayer = 'LATITUDE';
        Unite = 'deg';
    case cl_image.indDataType('Longitude')
        NomLayer = 'LONGITUDE';
        Unite = 'deg';
    case cl_image.indDataType('IncidenceAngle')
        NomLayer = 'INCIDENCE';
        Unite = 'deg';
    case cl_image.indDataType('TxAngle')
        NomLayer = 'EMISSION';
        Unite = 'deg';
    case cl_image.indDataType('TxBeamIndex')
        NomLayer = 'EMISSION_SECTOR';
        Unite = 'beam';
        
    case cl_image.indDataType('RxBeamAngle') % 'sans doute faux : TxBeamAngle'
        NomLayer = 'BEAM_EMI_ANGLE';
        Unite = 'deg';
    case cl_image.indDataType('AveragedPtsNb')
        NomLayer = 'AVRG_PIX_NB';
        Unite = 'samples';
    otherwise
        NomLayer = cl_image.strDataType{this.DataType};
        Unite = '?';
end
