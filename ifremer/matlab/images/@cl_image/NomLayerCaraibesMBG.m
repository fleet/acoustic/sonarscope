function [NomLayer, Unite] = NomLayerCaraibesMBG(this)

switch this.DataType
    case  cl_image.indDataType('AlongDist')
        NomLayer = 'mbAlongDistance';
        Unite = 'm';
    case  cl_image.indDataType('AcrossDist')
        NomLayer = 'mbAcrossDistance';
        Unite = 'm';
    case  cl_image.indDataType('Bathymetry')
        NomLayer = 'mbDepth';
        Unite = 'm';
    case  cl_image.indDataType('TxAngle') % TxBeamAngle ???
        NomLayer = 'mbAcrossBeamAngle';
        Unite = 'deg';
    case  cl_image.indDataType('BeamAzimuthAngle')
        NomLayer = 'mbAzimutBeamAngle';
        Unite = 'deg';
    % ATLANTIDE : correspondance mystérieuse
    case  cl_image.indDataType('Unknown')
        NomLayer = 'mbSoundingBias';
        Unite = 'm';
    case  cl_image.indDataType('MbesQualityFactor')
        NomLayer = 'mbSQuality';
        Unite = ' ';
    case  cl_image.indDataType('Mask')
        NomLayer = 'mbSFlag';
        Unite = ' ';


    otherwise
        NomLayer = [];
        Unite = [];
end
