% Calcul du mod�le de compensation des images photo OTUS du Rov Victor
%
% Syntax
%   flag = OTUS_CalculCompensation(this, listeFic, nomFicCompens, SeuilStd)
%
% Input Arguments
%   this               : Instance de clc_image
%   listeFic           : Liste des fichiers
%   nomFinomFicCompens : Nom du fichier de compensation
%   SeuilStd           : Seuil sur les �cart-types
%
% Output Arguments
%   flag : 1 = OK, 0=KO
%
% See also clc_image/callback_sonar params_OTUS_CalculCompensation Authors
% Authors    : JMA
%--------------------------------------------------------------------------

function flag = OTUS_CalculCompensation(~, listeFic, nomFicCompens, SeuilStd)
        
nbFic = length(listeFic);

XData = 1:nbFic;
YData = NaN(1,nbFic);

FigUtils.createSScFigure;
h  = plot(XData, YData, '+'); grid on; hold on;
xlabel('Image #'); ylabel('Std'); title('Standard deviation of the images')

nbImages = 0;
str1 = 'Lecture des images';
str2 = 'Reads images';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    nomFic = listeFic{k};
%     fprintf('%d / %d : %s\n', k, nbFic, nomFic)
    
    I = imread(nomFic);
    I = double(I);
    
    x = std(I(:));
    YData(k) = x;
    
    if x < SeuilStd
        nbImages = nbImages + 1;
        if nbImages == 1
            J = I;
        else
            J = J + I;
        end
    end
    set(h, 'YData', YData);    
end
my_close(hw, 'MsgEnd');

J = single(J / nbImages);

a = cl_image('Image', J);
export_matlab_array(a, nomFicCompens)
    
SonarScope(J);

flag = 0;
