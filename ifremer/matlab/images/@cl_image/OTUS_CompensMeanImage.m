function flag = OTUS_CompensMeanImage(~, listeFic, nomFicCompens)
 
flag = 0;

I0 = cl_image_I0;

nbFic = length(listeFic);
if nbFic == 0
    return
end

[a, flag] = import_matlab(I0, nomFicCompens, 'Unknown', 1);
a = single(a.Image);

nbFic = length(listeFic);
str1 = 'Compensation des images';
str2 = 'Images compensation';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    nomFic = listeFic{k};
    
    [nomDir1, nomFic1] = fileparts(nomFic);
    nomFicComp = fullfile(nomDir1, [nomFic1 '_2.tif']);

    I = imread(nomFic);
    I = single(I);
    
    I  = uint8(floor(I - a + 128));
    I(I == 0) = 1;
    I(I == 255) = 254;
    imwrite(I, nomFicComp)
    
    nomFicXML1 = fullfile(nomDir1, [nomFic1 '.xml']);
    nomFicXML2 = fullfile(nomDir1, [nomFic1 '_2.xml']);
    [status, message] = copyfile(nomFicXML1, nomFicXML2);
end
my_close(hw);

str1 = 'La compensation des images OTUS par l''image moyenne est termin�e.';
str2 = 'The OTUS images compensation by the mean valueis over.';
my_warndlg(Lang(str1,str2), 0);
