function flag = OTUS_CreateCompensatedImages(~, listeFic, OrdrePolyCompens, nomDirOut)
 
flag = 0;

nbFic = length(listeFic);
if nbFic == 0
    return
end
    
nbFic = length(listeFic);
str1 = 'Compensation des images';
str2 = 'Images compensation';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    str = sprintf('%s : %d/%d : %s', Lang(str1,str2), k, nbFic, listeFic{k});

    flag = OTUS_ImageReadAndComp(listeFic{k}, 3, OrdrePolyCompens, nomDirOut, []);
    if flag
        fprintf('%s : OK\n', str);
    else
        fprintf('%s : KO\n', str);
        renameBadFile(listeFic{k}, 'Message', str)
    end
end
my_close(hw, 'MsgEnd');
