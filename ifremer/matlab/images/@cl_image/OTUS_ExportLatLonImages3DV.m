function OTUS_ExportLatLonImages3DV(~, ListeFicImage, nomDirOut)

I0 = cl_image_I0;

if ischar(ListeFicImage)
    ListeFicImage = {ListeFicImage};
end

flagExportTexture   = 1;
flagExportElevation = 0;

N = length(ListeFicImage);
str1 = 'Export de mosa�ques';
str2 = 'Exports mosaics';
hw = create_waitbar(Lang(str1,str2), 'N', N); % Premier niveau de waitbar
for k=1:N
    my_waitbar(k, N, hw);
%     [flag, a] = cl_image.import_ermapper(ListeFicImage{k}, 'Memmapfile', 0);
    [flag, a] = import_imagePlusPlus(I0, ListeFicImage{k});
    if flag
        nomFic3D = extract_ImageName(a);
        nomLayer = [nomFic3D '_texture_v2'];
        flag = export_3DViewer_InGeotifTiles(a, nomLayer, nomFic3D, nomDirOut, flagExportTexture, flagExportElevation);
    end
end
my_close(hw, 'MsgEnd');
