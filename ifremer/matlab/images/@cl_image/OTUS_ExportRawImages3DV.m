function flag = OTUS_ExportRawImages3DV(~, listeFic, nomFicXML3DV, Carto, nomFicXMLPosition, varargin)

[varargin, Capteur] = getPropertyValue(varargin, 'Capteur', 'OTUS'); %#ok<ASGLU>

flag = 0;

nbFic = length(listeFic);
if nbFic == 0
    return
end

[nomDirXML3DV, nomFicXML3DVSeul] = fileparts(nomFicXML3DV);

map = gray(256);

%% Lecture du fichier qui contient les informations de positionnement des images

[flag, DataPosition] = XMLBinUtils.readGrpData(nomFicXMLPosition);
if ~flag
    return
end

%% Contr�le si toutes les images sont renseign�es au niveau positionnement

n = length(DataPosition.FileNames);
listeImagesDansFichierXML = cell(n,1);
for k=1:n
    [~, listeImagesDansFichierXML{k}] = fileparts(DataPosition.FileNames{k});
end

n = length(listeFic);
listeImages = cell(n,1);
for k=1:n
    [~, listeImages{k}] = fileparts(listeFic{k});
    listeImages{k}(1:3) = listeImagesDansFichierXML{1}(1:3);
end

[liste_Intersection, ia, ib] = intersect(listeImagesDansFichierXML, listeImages);
flag = (length(liste_Intersection) == length(listeFic));

% On remet dans l'ordre listeFic dans l'ordre mais pas DataPosition car on  le fera via la variable 'ia'
listeFic = listeFic(ib);
if ~flag
    str1 = 'Certaines images s�lectionn�es ne sont pas renseign�es dans le fichier de positionnement. Soulez-vous poursuivre avec les fichiers renseign�s uniquement ?';
    str2 = 'Some selected images have no correspondance in the XML navigation file. Do you want to continue with the images that have navigation ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag || (rep == 2)
        return
    end
    listeFic = liste_Intersection;
end

%% Recherche des limites extr�mes

% TODO : D�but partie mutualisable avec OTUS_ExportRawImages3DV
nbFic = length(listeFic);
Lat     = NaN(nbFic,1);
Lon     = NaN(nbFic,1);
Height  = NaN(nbFic,1);
Heading = NaN(nbFic,1);
Focale  = NaN(nbFic,1);
tMat    = NaT(nbFic,1);

liste = cell(nbFic,1);
N = 0;
str1 = 'Lecture des positions des images.';
str2 = 'Reading the positions of the images.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw);
    
    lat     = DataPosition.Latitude(ia(kFic));
    lon     = DataPosition.Longitude(ia(kFic));
    height  = DataPosition.Height(ia(kFic));
    heading = DataPosition.Heading(ia(kFic));
    focale  = DataPosition.Focale(ia(kFic));
    
    if isnan(height) || (height > 15)
        continue
    end
    N = N+1;
    Lat(N)     = lat;
    Lon(N)     = lon;
    Height(N)  = height;
    Heading(N) = heading;
    Focale(N)  = focale;
    liste{N}   = listeFic{kFic};
    switch Capteur
        case 'OTUS'
            [flag, t] = OTUS_getTimeFromFileName(listeFic{kFic});
        case 'SCAMPI'
            [flag, t] = SCAMPI_getTimeFromFileName(listeFic{kFic});
    end
    if flag
        tMat(N) = t;
    end
end
my_close(hw);

if N == 0
    str1 = 'Aucune image n''a pu �tre g�olocalis�e.';
    str2 = 'No image could be geolocated.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

sub = 1:N;
Lat     = Lat(sub);
Lon     = Lon(sub);
Height  = Height(sub);
Heading = Heading(sub);
Focale  = Focale(sub);
% liste   = liste(sub);
tMat    = tMat(sub);
listeFic = listeFic(sub);
% TODO : Fin partie mutualisable avec OTUS_ExportRawImages3DV

% plot_navigation(nomFic, [], Lon, Lat, t_photos.timeMat, []);

%% Cr�ation d'une mosaique par image

TypeCompensation = 1;
OrdrePolyCompens = [];
nomDirImagesCompensee = [];
Correction = [];

nbFic = length(listeFic);
str1 = 'Export des images vers GLOBE';
str2 = 'Exports the images to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    nomFic = listeFic{k};
    [~ , nomFic1] = fileparts(nomFic);
    str = sprintf('%s : %d/%d : %s', Lang(str1,str2), kFic, nbFic, nomFic1);
    
    [flag, I, Distances] = OTUS_ImageReadAndComp(nomFic, TypeCompensation, OrdrePolyCompens, nomDirImagesCompensee, Correction);
    if flag
        fprintf('%s : OK\n', str);
    else
        fprintf('%s : KO\n', str);
        renameBadFile(listeFic{k}, 'Message', str)
        continue
    end
    
    I = my_fliplr(I); % TODO : c'est myst�rieux que l'on doive faire �a mais c'est comme �a que �a marche !!!
    Distances = fliplr(Distances);
    
    %     I = rot90(I);
    I = single(I);
    [nbColumns, nbRows] = size(I);
    
    if k == 1
        DataWCRaw.from            = 'Unkown';
        DataWCRaw.PingNumber      = 1:nbFic;
        DataWCRaw.Date            = zeros(nbFic, 1); % TimePing.date;
        DataWCRaw.Hour            = zeros(nbFic, 1); % TimePing.heure;
        DataWCRaw.LatitudeTop     = NaN(nbFic,nbRows);
        DataWCRaw.LongitudeTop    = NaN(nbFic,nbRows);
        DataWCRaw.LatitudeBottom  = NaN(nbFic,nbRows);
        DataWCRaw.LongitudeBottom = NaN(nbFic,nbRows);
        DataWCRaw.DepthTop        = NaN(nbFic,nbRows);
        DataWCRaw.DepthBottom     = NaN(nbFic,nbRows);
        DataWCRaw.ImageSegment    = NaN(nbFic,nbRows);
        
        Z0 = zeros(1,nbRows);
        subRows = 1:nbRows;
    end
    
    
    [nbRows, nbCol]  = size(I);
    I = floor(I);
    I(I <= 2) = 2;
    I(I >= 2535) = 253;
    
    if isempty(Distances)
        Distances = NaN(nbRows, nbCol, 'single');
        y = (1:nbRows) - nbRows/2;
        for iLig = 1:nbRows
            x = (1:nbCol) - nbCol/2;
            Distances(iLig,:) = sqrt(y(iLig)^2 + x.^2);
        end
    end
    % figure; imagesc(Distances)
    
    Latitude  = Lat(k);
    Longitude = Lon(k);
    if isnan(Height(k))
        continue
    end
    
    [xy0_mos(1,1), xy0_mos(2,1)] = latlon2xy(Carto, Latitude, Longitude);
    Angle = 180 - Heading(k);
    Rot = [cosd(Angle) sind(Angle); -sind(Angle) cosd(Angle)];
    Largeur = 2* Height(k) * tand(Focale(k)/2);
    Hauteur = 2* Height(k) * tand(Focale(k)/2) * (nbRows/nbCol);
    xy_image = [-Largeur/2 -Largeur/2 Largeur/2 Largeur/2; Hauteur/2 -Hauteur/2 -Hauteur/2 Hauteur/2];
    xy_mos = Rot * xy_image + repmat(xy0_mos, 1, 4);
    [LatImage, LonImage] = xy2latlon(Carto, xy_mos(1,:), xy_mos(2,:));
    
    DataWCRaw.LatitudeTop(k,:)     = interp1([1 nbRows], LatImage(1:2), subRows); % Lat(1:2);
    DataWCRaw.LongitudeTop(k,:)    = interp1([1 nbRows], LonImage(1:2), subRows); % Lon(1:2);
    DataWCRaw.LatitudeBottom(k,:)  = interp1([1 nbRows], LatImage([4 3]), subRows); % Lat(3:4);
    DataWCRaw.LongitudeBottom(k,:) = interp1([1 nbRows], LonImage([4 3]), subRows); % Lon(3:4);
    DataWCRaw.DepthTop(k,:)        = Z0;
    DataWCRaw.DepthBottom(k,:)     = Z0;
    DataWCRaw.ImageSegment(k,:)    = Z0;
    DataWCRaw.EndData(k,:)         = Z0;
    
    kRep = floor(k/100);
    nomDir = fullfile(nomDirXML3DV, nomFicXML3DVSeul);
    if ~exist(nomDir, 'dir')
        flag = mkdir(nomDir);
        if ~flag
            messageErreurFichier(nomDirPng, 'WriteFailure');
            return
        end
    end
    nomDirPngRaw = fullfile(nomDir, num2str(kRep, '%03d'));
    if ~exist(nomDirPngRaw, 'dir')
        flag = mkdir(nomDirPngRaw);
        if ~flag
            messageErreurFichier(nomDirPngRaw, 'WriteFailure');
            return
        end
    end
    
    nomFicPng = [num2str(k, '%05d') '.png'];
    nomFicPng = fullfile(nomDirPngRaw, nomFicPng);
    DataWCRaw.SliceName{k}  = nomFicPng;
    
    imwrite(I, map, nomFicPng, 'Transparency', 0)
    DataWCRaw.SliceName{k} = nomFicPng;
end
% SonarScope(Mosa)

tMat = cl_time('timeMat', tMat);
DataWCRaw.Date = tMat.date;
DataWCRaw.Hour = tMat.heure;

DataWCRaw.DepthTop(:)    = -3450; % Snake Pit
DataWCRaw.DepthBottom(:) = -3450; % Snake Pit

DataWCRaw.DepthTop(:)    = -50;
DataWCRaw.DepthBottom(:) = -50;

DataWCRaw.Tide  = zeros(nbFic,nbRows);
DataWCRaw.Heave = zeros(nbFic,nbRows);
DataWCRaw.name = nomFicXML3DVSeul;
DataWCRaw.from = 'SonarScope';

flag = write_XML_ImagesAlongNavigation(nomFicXML3DV, DataWCRaw, nbColumns, nbRows);

my_close(hw, 'MsgEnd');
