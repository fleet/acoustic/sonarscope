% Cr�ation de mosaiques restreintes des images photo OTUS du Rov Victor
%
% Syntax
%   flag = OTUS_Mosaic(this, listeFic, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nbMaxPixelsWidthTile, nbMaxPixelsHeightTile)
%
% Input Arguments
%   this          : Instance de clc_image
%   ListeFicImage : Liste des fichiers
%   resol         : R�solution des mosaiques individuelles (m)
%   MosaicName    : Mosaic name
%   nomRepOut     : Nom du r�pertoire de stockage des mosaiques
%   nomFicCompens : Nom du fichier de compensation
%   Carto         : Instance de cl_carto
%   TypeCompensation : 1 no compensation, 2=One single model, 3=auto on every image
%   nomDirImagesCompensee : Name of the directory where the compensated images will be saved, [] either
%
% Name-Value Pair Arguments
%   nbMaxPixelsWidthTile  : Maximum number of pixels in the width of a tile
%   nbMaxPixelsHeightTile : Maximum number of pixels in the height of a tile
%
% Output Arguments
%   flag : 1 = OK, 0=KO
%
% See also clc_image/callback_sonar params_OTUS_creationXml Authors
% Authors  : JMA
%--------------------------------------------------------------------------

function flag = OTUS_Mosaic(~, listeFic, nomFicXMLPosition, resol, MosaicName, nomRepOut, nomFicCompens, Carto, ...
    TypeCompensation, nomDirImagesCompensee, Capteur, NomRacine, ext, varargin)

[varargin, nbMaxPixelsWidthTile]  = getPropertyValue(varargin, 'nbMaxPixelsWidthTile',  10000);
[varargin, nbMaxPixelsHeightTile] = getPropertyValue(varargin, 'nbMaxPixelsHeightTile', 10000);
[varargin, HeightMax]             = getPropertyValue(varargin, 'HeightMax',             15); %#ok<ASGLU>

flag = 0;

nbFic = length(listeFic);
if nbFic == 0
    return
end

OrdrePolyCompens = 3;

%% Lecture du fichier qui contient les informations de positionnement des images

[flag, DataPosition] = XMLBinUtils.readGrpData(nomFicXMLPosition);
if ~flag
    return
end

%% Contr�le si toutes les images sont renseign�es au niveau positionnement

n = length(DataPosition.FileNames);
listeImagesDansFichierXML = cell(n,1);
for k=1:n
    [~, listeImagesDansFichierXML{k}] = fileparts(DataPosition.FileNames{k});
end

n = length(listeFic);
listeImages = cell(n,1);
for k=1:n
    [~, listeImages{k}] = fileparts(listeFic{k});
    listeImages{k}(1:3) = listeImagesDansFichierXML{1}(1:3);
end

[liste_Intersection, ia, ib] = intersect(listeImagesDansFichierXML, listeImages);
flag = (length(liste_Intersection) == length(listeFic));

% On remet dans l'ordre listeFic dans l'ordre mais pas DataPosition car on  le fera via la variable 'ia'
listeFic = listeFic(ib);
if ~flag
    str1 = 'Certaines images s�lectionn�es ne sont pas renseign�es dans le fichier de positionnement. Soulez-vous poursuivre avec les fichiers renseign�s uniquement ?';
    str2 = 'Some selected images have no correspondance in the XML navigation file. Do you want to continue with the images that have navigation ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag || (rep == 2)
        return
    end
    listeFic = liste_Intersection;
end

%% Lecture du fichier de compensation

if isempty(nomFicCompens)
    Correction = [];
else
    if iscell(nomFicCompens)
        nomFicCompens = nomFicCompens{1};
    end
    Correction = loadmat(nomFicCompens, 'nomVar', 'Image');
end

%% On cr�e au besoin le r�pertoire de stockage des mosa�ques

if ~exist(nomRepOut, 'dir')
    [status, msg] = mkdir(nomRepOut); %#ok
    if status
        %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le repertoire %s.\n', nomRepOut);
    end
end

%% Recherche des limites extr�mes

% TODO : D�but partie mutualisable avec OTUS_ExportRawImages3DV
nbFic = length(listeFic);
Lat     = NaN(nbFic,1);
Lon     = NaN(nbFic,1);
Height  = NaN(nbFic,1);
Heading = NaN(nbFic,1);
Focale  = NaN(nbFic,1);
tMat    = NaT(nbFic,1);

liste = cell(nbFic,1);
N = 0;
str1 = 'Lecture des positions des images.';
str2 = 'Reading the positions of the images.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw);
    
    nomFic = listeFic{kFic};
    
    lat     = DataPosition.Latitude(ia(kFic));
    lon     = DataPosition.Longitude(ia(kFic));
    height  = DataPosition.Height(ia(kFic));
    heading = DataPosition.Heading(ia(kFic));
    focale  = DataPosition.Focale(ia(kFic));
    
    if isnan(height) || (height > HeightMax)
        continue
    end
    N = N+1;
    Lat(N)     = lat;
    Lon(N)     = lon;
    Height(N)  = height;
    Heading(N) = heading;
    Focale(N)  = focale;
    liste{N}   = listeFic{kFic};
    switch Capteur
        case 'OTUS'
            [flag, t] = OTUS_getTimeFromFileName(listeFic{kFic});
        case 'SCAMPI'
            [flag, t] = SCAMPI_getTimeFromFileName(listeFic{kFic});
    end
    if flag
        tMat(N) = t;
    end
end
my_close(hw);

if N == 0
    str1 = 'Aucune image n''a pu �tre g�olocalis�e.';
    str2 = 'No image could be geolocated.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

sub = 1:N;
Lat     = Lat(sub);
Lon     = Lon(sub);
Height  = Height(sub);
Heading = Heading(sub);
Focale  = Focale(sub);
liste   = liste(sub);
tMat    = tMat(sub);
% TODO : Fin partie mutualisable avec OTUS_ExportRawImages3DV

% plot_navigation(nomFic, [], Lon, Lat, t_photos, []);

%% Calcul des images allant dans les diff�rentes dalles

[x, y] = latlon2xy(Carto, Lat, Lon);
XMax = max(x);
XMin = min(x);
YMax = max(y);
YMin = min(y);
Rangex = XMax - XMin;
Rangey = YMax - YMin;

largeurDalle = resol * nbMaxPixelsWidthTile;
hauteurDalle = resol * nbMaxPixelsHeightTile;
Nx = ceil(Rangex / largeurDalle);
Ny = ceil(Rangey / hauteurDalle);
Nx = max(1, Nx);
Ny = max(1, Ny);
listeFicParDalle = cell(Ny,Nx);
kxDalle      = zeros(Ny,Nx);
kyDalle      = zeros(Ny,Nx);
LatDalle     = cell(Ny,Nx);
LonDalle     = cell(Ny,Nx);
HeightDalle  = cell(Ny,Nx);
HeadingDalle = cell(Ny,Nx);
FocaleDalle  = cell(Ny,Nx);

FigDalles = [];
for kx=1:Nx
    XMinDalle = XMin + (kx-1) * largeurDalle;
    XMaxDalle = XMin + kx * largeurDalle;
    for ky=1:Ny
        YMinDalle = YMin + (ky-1) * hauteurDalle;
        YMaxDalle = YMin + ky * hauteurDalle;
        sub = find((x >= XMinDalle) & (x <= XMaxDalle) & (y >= YMinDalle) & (y <= YMaxDalle));
        if isempty(sub)
            continue
        end
        
        listeFicParDalle{ky, kx} = liste(sub);
        kxDalle(ky,kx) = kx;
        kyDalle(ky,kx) = ky;
        
        LatDalle{ky,kx}     = Lat(sub);
        LonDalle{ky,kx}     = Lon(sub);
        HeightDalle{ky,kx}  = Height(sub);
        HeadingDalle{ky,kx} = Heading(sub);
        FocaleDalle{ky,kx}  = Focale(sub);
        
        t_photos = tMat(sub);
        %         FigDalles = plot_navigation(nomFic, FigDalles, Lon(sub), Lat(sub), t_photos, []);
        FigDalles = plot_navigation_OTUS(nomFic, Lon(sub), Lat(sub), t_photos, Capteur, NomRacine, ext, 'Fig', FigDalles);
    end
end
listeFicParDalle = listeFicParDalle(:);
kxDalle = kxDalle(:);
kyDalle = kyDalle(:);

%% S�lection des dalles � traiter

subDalleNonVide = find(kxDalle ~= 0);
N = length(subDalleNonVide);
for kMos=1:N
    kDalle = subDalleNonVide(kMos);
    nomImage{kMos} =  sprintf('%s_%02d_%02d', MosaicName, kxDalle(kDalle), kyDalle(kDalle)); %#ok<AGROW>
end

str1 = 'S�lectionnez les dalles que vous voulez calculer :';
str2 = 'TODO';
[sub, flag] = my_listdlg(Lang(str1,str2), nomImage, 'InitialValue', 1:N);
if ~flag
    return
end
listeFicParDalle = listeFicParDalle(subDalleNonVide(sub));
kxDalle          = kxDalle(subDalleNonVide(sub));
kyDalle          = kyDalle(subDalleNonVide(sub));
LatDalle         = LatDalle(subDalleNonVide(sub));
LonDalle         = LonDalle(subDalleNonVide(sub));
HeightDalle      = HeightDalle(subDalleNonVide(sub));
HeadingDalle     = HeadingDalle(subDalleNonVide(sub));
FocaleDalle      = FocaleDalle(subDalleNonVide(sub));

%% Cr�ation des mosaiques

N = length(listeFicParDalle);
str1 = 'Cr�ation des dalles';
str2 = 'Creates tiles';
hw = create_waitbar(Lang(str1,str2), 'N', N); % Premier niveau de waitbar
for kMos=1:N
    my_waitbar(kMos, N, hw);
    mosaiqueDalle(listeFicParDalle{kMos}, TypeCompensation, OrdrePolyCompens, Correction, Carto, resol, MosaicName, nomRepOut, kxDalle(kMos), kyDalle(kMos), ...
        LatDalle{kMos}, LonDalle{kMos}, HeightDalle{kMos}, HeadingDalle{kMos}, FocaleDalle{kMos}, nomDirImagesCompensee)
end
my_close(hw, 'MsgEnd');


function mosaiqueDalle(listeFic, TypeCompensation, OrdrePolyCompens, Correction, Carto, resol, MosaicName, nomRepOut, kxDalle, kyDalle, ...
    LatDalle, LonDalle, HeightDalle, HeadingDalle, FocaleDalle, nomDirImagesCompensee)

nomImage =  sprintf('%s_%02d_%02d', MosaicName, kyDalle, kxDalle);
TagSynchroContrast = rand(1);
nbFic = length(listeFic);
if nbFic == 0
    return
end

%% Tri des images de telle sorte que la matrice enti�re soit cr�e im�diatement pour �viter de multiples accroissements de la matrice

[~, kMinLat] = min(LatDalle);
[~, kMaxLat] = max(LatDalle);
[~, kMinLon] = min(LonDalle);
[~, kMaxLon] = max(LonDalle);

subBords = unique([kMinLat kMaxLat kMinLon kMaxLon]);
sub = 1:length(listeFic);
sub = [subBords setdiff(sub, subBords)];

listeFic     = listeFic(sub);
LatDalle     = LatDalle(sub);
LonDalle     = LonDalle(sub);
HeightDalle  = HeightDalle(sub);
HeadingDalle = HeadingDalle(sub);
FocaleDalle  = FocaleDalle(sub);

%% Traitement des images

str1 = sprintf('Cr�ation de la dalle "%s"', nomImage);
str2 = sprintf('Creates tile "%s"', nomImage);
hw = create_waitbar(Lang(str1,str2), 'N', nbFic); % Deuxi�me niveau de waitbar
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw);
    nomFic = listeFic{kFic};
    [~ , nomFic1] = fileparts(nomFic);
    str = sprintf('%s : %d/%d : %s', Lang(str1,str2), kFic, nbFic, nomFic1);
    
    %% Traitement de l'image (lecture et compensation si demand�)
    
    [flag, I, Distances, Unit] = OTUS_ImageReadAndComp(nomFic, TypeCompensation, OrdrePolyCompens, nomDirImagesCompensee, Correction);
    if flag
        fprintf('%s : OK\n', str);
    else
        fprintf('%s : KO\n', str);
        renameBadFile(listeFic{k}, 'Message', str)
        continue
    end
    
    I = my_fliplr(I); % TODO : c'est myst�rieux que l'on doive faire �a mais c'est comme �a que �a marche !!!
    Distances = fliplr(Distances);
    
    %% Projection cartographique
    
    Latitude  = LatDalle(kFic);
    Longitude = LonDalle(kFic);
    Height    = HeightDalle(kFic);
    if Height == 0
        continue
    end
    Heading   = HeadingDalle(kFic);
    Focale    = FocaleDalle(kFic);
    
    [xy0_mos(1,1), xy0_mos(2,1)] = latlon2xy(Carto, Latitude, Longitude);
    Angle = 180 - Heading;
    
    Rot = [cosd(Angle) sind(Angle); -sind(Angle) cosd(Angle)];
    
    [nbRows, nbCol, nbCan]  = size(I); %#ok<ASGLU>
    Largeur = 2* Height * tand(Focale/2);
    Hauteur = 2* Height * tand(Focale/2) * (nbRows/nbCol);
    
    deltax = Largeur / nbCol;
    %     deltay = Hauteur / nbRows;
    
    xy_image = [-Largeur/2 -Largeur/2 Largeur/2 Largeur/2; Hauteur/2 -Hauteur/2 -Hauteur/2 Hauteur/2];
    xy_mos = Rot * xy_image + repmat(xy0_mos, 1, 4);
    
    minx = min(xy_mos(1,:));
    maxx = max(xy_mos(1,:));
    miny = min(xy_mos(2,:));
    maxy = max(xy_mos(2,:));
    
    Rot = Rot * (deltax / resol); % deltay
    Rot(3,3) = 1;
%     tform = maketform('affine', Rot);
    tform = affine2d(Rot);
    
%     J = imtransform(I, tform, 'XYScale', 1);
    J = imwarp(I, tform); %, 'XYScale', 1);
%     Distances = imtransform(Distances, tform, 'nearest', 'XYScale', 1);
    Distances = imwarp(Distances, tform, 'nearest'); %, 'XYScale', 1);
    [nbLig_mos, nbCol_mos, nbCan_mos] = size(J);
    
    subNaN = (sum(J,3) == 0);
    subNaN = imdilate(subNaN, ones(5,5));
    
    for iCan=1:nbCan_mos
        K = J(:,:,iCan);
        K(subNaN) = NaN;
        J(:,:,iCan) = K;
    end
    Distances(subNaN) = NaN;
    
    % imshow(J)
    % drawnow
    
    [LatMinMax(1), LonMinMax(1)] = xy2latlon(Carto, minx, miny);
    [LatMinMax(2), LonMinMax(2)] = xy2latlon(Carto, maxx, miny);
    [LatMinMax(3), LonMinMax(3)] = xy2latlon(Carto, maxx, maxy);
    [LatMinMax(4), LonMinMax(4)] = xy2latlon(Carto, minx, maxy);
    LatMin(kFic) = min(LatMinMax); %#ok<AGROW>
    LatMax(kFic) = max(LatMinMax); %#ok<AGROW>
    LonMin(kFic) = min(LonMinMax); %#ok<AGROW>
    LonMax(kFic) = max(LonMinMax); %#ok<AGROW>
    
    x = centrage_magnetique(linspace(LonMin(kFic), LonMax(kFic), nbCol_mos));
    y = centrage_magnetique(linspace(LatMin(kFic), LatMax(kFic), nbLig_mos));
    a = cl_image('Image', J, 'x', x, 'y', y, 'Unit', Unit, ...
        'Name', ['Image_' nomFic1], ...
        'ColormapIndex', 2, ...
        'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
        'DataType', cl_image.indDataType('Reflectivity'), ...
        'GeometryType', cl_image.indGeometryType('LatLong'), ...
        'TagSynchroContrast', TagSynchroContrast, ...
        'Carto', Carto);
    
    b = cl_image('Image', Distances, 'x', x, 'y', y, ...
        'Name', ['Distance_' nomFic1], ...
        'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
        'DataType', cl_image.indDataType('TxAngle'), ...
        'GeometryType', cl_image.indGeometryType('LatLong'), ...
        'Carto', Carto);
    
    %     a.CLim = CLim;
    
    if kFic == 1
        Mosa(1) = a;
        Mosa(2) = b;
    else
        [c, d] = mosaique([Mosa(1) a], 'Angle', [Mosa(2) b], 'FistImageIsCollector', 1);
        
        clear Mosa
        
        Mosa(1) = c;
        Mosa(2) = d;
        clear c d
    end
    clear a b
    
    %% Interpolation de la mosa�que apr�s traitement de la derni�re image
    
    if kFic == nbFic
        Mosa(1) = WinFillNaN(Mosa(1), 'window', [5 5]);
    end
    
    %% Sauvegarde des mosa�ques dans des fichiers ErMapper
    
    if (mod(kFic,100) == 0) || (kFic == nbFic)
        Mosa(1) = update_Name(Mosa(1), 'Name', nomImage);
        Mosa(2) = update_Name(Mosa(2), 'Name', nomImage);
        
        nomFicMosImage = sprintf('%s.ers', Mosa(1).Name);
        nomFicMosAngle = sprintf('%s.ers', Mosa(2).Name);
        %         nomFicGE       = sprintf('Mosa%02d.kmz', im);
        
        Mosa(1).InitialImageName = nomFicMosImage;
        Mosa(2).InitialImageName = nomFicMosImage;
        
        nomFicErMapperLayer = fullfile(nomRepOut, nomFicMosImage);
        nomFicErMapperAngle = fullfile(nomRepOut, nomFicMosAngle);
        %         nomFicGE            = fullfile(nomRepOut, nomFicGE);
        Mosa(1).InitialFileName = nomFicErMapperLayer;
        Mosa(2).InitialFileName = nomFicErMapperLayer;
        Mosa(2).ColormapIndex   = 3;
        
        export_ermapper(Mosa(1), nomFicErMapperLayer);
        export_ermapper(Mosa(2), nomFicErMapperAngle);
    end
end
my_close(hw, 'MsgEnd');
% SonarScope(Mosa)
