% Cr�ation des mosaiques individuelles des images photo OTUS du Rov Victor
%
% Syntax
%   flag = OTUS_MosaiquesIndividuelles(this, listeFic, resol, nomRepOut, nomFicCompens, Carto, CLim)
%
% Input Arguments
%   this          : Instance de clc_image
%   ListeFicImage : Liste des fichiers
%   resol         : R�solution des mosaiques individuelles (m)
%   nomRepOut     : Nom du r�pertoire de stockage des mosaiques
%   nomFicCompens : Nom du fichier de compensation
%   Carto         : Instance de cl_carto
%   CLim          : Bornes du rehaussement de contraste
%
% Output Arguments
%   flag : 1 = OK, 0=KO
%
% See also clc_image/callback_sonar params_OTUS_creationXml Authors
% Authors    : JMA
%--------------------------------------------------------------------------

function flag = OTUS_MosaiquesIndividuelles(~, listeFic, nomFicXMLPosition, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nomDirImagesCompensee, varargin)

[varargin, HeightMax] = getPropertyValue(varargin, 'HeightMax', 15); %#ok<ASGLU>

flag = 0;

OrdrePolyCompens = 2;

nbFic = length(listeFic);
if nbFic == 0
    return
end

%% Lecture du fichier qui contient les informations de positionnement des images

[flag, DataPosition] = XMLBinUtils.readGrpData(nomFicXMLPosition);
if ~flag
    return
end

%% Contr�le si toutes les images sont renseign�es au niveau positionnement

n = length(DataPosition.FileNames);
listeImagesDansFichierXML = cell(n,1);
for k=1:n
    [~, listeImagesDansFichierXML{k}] = fileparts(DataPosition.FileNames{k});
end

n = length(listeFic);
listeImages = cell(n,1);
for k=1:n
    [~, listeImages{k}] = fileparts(listeFic{k});
    listeImages{k}(1:3) = listeImagesDansFichierXML{1}(1:3);
end

[liste_Intersection, ia, ib] = intersect(listeImagesDansFichierXML, listeImages);
flag = (length(liste_Intersection) == length(listeFic));

% On remet dans l'ordre listeFic dans l'ordre mais pas DataPosition car on  le fera via la variable 'ia'
listeFic = listeFic(ib);
if ~flag
    str1 = 'Certaines images s�lectionn�es ne sont pas renseign�es dans le fichier de positionnement. Soulez-vous poursuivre avec les fichiers renseign�s uniquement ?';
    str2 = 'Some selected images have no correspondance in the XML navigation file. Do you want to continue with the images that have navigation ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag || (rep == 2)
        return
    end
    listeFic = liste_Intersection;
end

%% Lecture du fichier de compensation

if isempty(nomFicCompens)
    Correction = [];
else
    if iscell(nomFicCompens)
        nomFicCompens = nomFicCompens{1};
    end
    Correction = loadmat(nomFicCompens, 'nomVar', 'Image');
end

%% Cr�ation d'une mosaique par image

TagSynchroContrast = rand(1);
nbFic = length(listeFic);
str1 = 'Cr�ation des mosaiques individuelles';
str2 = 'Creation of individual mosaics';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw);
    
    nomFic = listeFic{kFic};
    [~ , nomFic1] = fileparts(nomFic);
    str = sprintf('%s : %d/%d : %s', Lang(str1,str2), kFic, nbFic, nomFic1);
    
    Latitude  = DataPosition.Latitude(ia(kFic));
    Longitude = DataPosition.Longitude(ia(kFic));
    Height    = double(DataPosition.Height(ia(kFic)));
    Heading   = double(DataPosition.Heading(ia(kFic)));
    Focale    = double(DataPosition.Focale(ia(kFic)));
    
    if isnan(Height) || (Height > HeightMax)
        fprintf('%s : Height > %f m\n', str, HeightMax);
        continue
    end
    
    if isnan(Heading)
        fprintf('%s : Heading is NAN\n', str);
        continue
    end
    
    %% Traitement de l'image (lecture et compensation si demand�)
    
    [flag, I, Distances] = OTUS_ImageReadAndComp(nomFic, TypeCompensation, OrdrePolyCompens, nomDirImagesCompensee, Correction);
    if flag
        fprintf('%s : OK\n', str);
    else
        fprintf('%s : KO\n', str);
        renameBadFile(listeFic{k}, 'Message', str)
    end
    
    I = my_fliplr(I); % TODO : c'est myst�rieux que l'on doive faire �a mais c'est comme �a que �a marche !!!
    Distances = fliplr(Distances);
    
    %% Projection cartographique
    
    [xy0_mos(1,1), xy0_mos(2,1)] = latlon2xy(Carto, Latitude, Longitude);
    Angle = 180 - Heading;
    
    Rot = [cosd(Angle) sind(Angle); -sind(Angle) cosd(Angle)];
    
    [nbRows, nbCol, nbCan]  = size(I);
    Largeur = 2* Height * tand(Focale/2);
    Hauteur = 2* Height * tand(Focale/2) * (nbRows/nbCol);
    
    deltax = Largeur / nbCol;
    %     deltay = Hauteur / nbRows;
    
    xy_image = [-Largeur/2 -Largeur/2 Largeur/2 Largeur/2; Hauteur/2 -Hauteur/2 -Hauteur/2 Hauteur/2];
    xy_mos = Rot * xy_image + repmat(xy0_mos, 1, 4);
    
    minx = min(xy_mos(1,:));
    maxx = max(xy_mos(1,:));
    miny = min(xy_mos(2,:));
    maxy = max(xy_mos(2,:));
    
    Rot = Rot * (deltax / resol); % deltay
    Rot(3,3) = 1;
%     tform = maketform('affine', double(Rot));
    tform = affine2d(double(Rot));
    
%     J = imtransform(I, tform, 'XYScale', 1);
    J = imwarp(I, tform); %, 'XYScale', 1);
%     Distances = imtransform(Distances, tform, 'nearest', 'XYScale',1);
    Distances = imwarp(Distances, tform, 'nearest'); %, 'XYScale',1);
    
    [nbLig_mos, nbCol_mos, ~] = size(J); % Attention, il faut absolument conserver le ~
    
    subNaN = (sum(J,3) == 0);
    subNaN = imdilate(subNaN, ones(5,5));
    for k=1:nbCan
        JCan = J(:,:,k);
        JCan(subNaN) = NaN;
        J(:,:,k) = JCan;
    end
    Distances(subNaN) = NaN;
    
    %     imshow(J)
    %     drawnow
    
    [LatMinMax(1), LonMinMax(1)] = xy2latlon(Carto, minx, miny);
    [LatMinMax(2), LonMinMax(2)] = xy2latlon(Carto, maxx, miny);
    [LatMinMax(3), LonMinMax(3)] = xy2latlon(Carto, maxx, maxy);
    [LatMinMax(4), LonMinMax(4)] = xy2latlon(Carto, minx, maxy);
    LatMin(kFic) = min(LatMinMax); %#ok<AGROW>
    LatMax(kFic) = max(LatMinMax); %#ok<AGROW>
    LonMin(kFic) = min(LonMinMax); %#ok<AGROW>
    LonMax(kFic) = max(LonMinMax); %#ok<AGROW>
    
    [~, nomFic1] = fileparts(nomFic);
    x = centrage_magnetique(linspace(LonMin(kFic), LonMax(kFic), nbCol_mos));
    y = centrage_magnetique(linspace(LatMin(kFic), LatMax(kFic), nbLig_mos));
    a = cl_image('Image', J, 'x', x, 'y', y, 'Unit', '#', 'XUnit', 'deg', 'YUnit', 'deg', ...
        'Name', nomFic1, 'ColormapIndex', 2, ...
        'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
        'DataType', cl_image.indDataType('Reflectivity'), ...
        'GeometryType', cl_image.indGeometryType('LatLong'), ...
        'TagSynchroContrast', TagSynchroContrast, ...
        'Carto', Carto, 'InitialFileName', nomFic1, ...
        'Memmapfile', 'No');
    
    b = cl_image('Image', Distances, 'x', x, 'y', y, 'Unit', 'deg', 'XUnit', 'deg', 'YUnit', 'deg', ...
        'Name', nomFic1, ...
        'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
        'DataType', cl_image.indDataType('TxAngle'), ...
        'GeometryType', cl_image.indGeometryType('LatLong'), ...
        'Carto', Carto, 'InitialFileName', nomFic1, ...
        'Memmapfile', 'No');
    
    a.CLim = a.StatValues.Quant_25_75;
    a = update_Name(a);
    b = update_Name(b);
    
    %% On cr�e au besoin le r�pertoire de stockage des mosa�ques
    
    if ~exist(nomRepOut, 'dir')
        [status, msg] = mkdir(nomRepOut); %#ok
        if status
            %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
        else
            fprintf('Impossible de c�er le repertoire %s.\n', nomRepOut);
        end
    end
    
    %% Exportation des mosaiques au format ErMapper
    
    %     nomFicMosImage = a.Name;
    %     nomFicMosAngle = b.Name;
    %     nomFicErMapperLayer = fullfile(nomRepOut, nomFicMosImage);
    %     nomFicErMapperAngle = fullfile(nomRepOut, nomFicMosAngle);
    
    %     export_ermapper(a, nomFicErMapperLayer);
    %     export_ermapper(b, nomFicErMapperAngle);
    
    nomFicXML = fullfile(nomRepOut, [nomFic1 '.xml']);
    flag = export_info_xml(a, nomFicXML);
    if ~flag
        return
    end
    
    nomFicXML = fullfile(nomRepOut, [nomFic1 '_Dist.xml']);
    flag = export_info_xml(b, nomFicXML);
    if ~flag
        return
    end
    
    %% Exportation de l'image en .jpg
    
    nomFicJPG = fullfile(nomRepOut, [nomFic1 '.jpg']);
    flag = export_tif(a, nomFicJPG); %, 'CLim', CLim);
    
    nomFicJPG = fullfile(nomRepOut, [nomFic1 '_Dist.jpg']);
    flag = export_tif(b, nomFicJPG);
    
    %% Exportation de la mosaique au format GoogleEarth
    
    %{
nomFicGE = sprintf('%s.kmz', nomFic1);
nomFicGE = fullfile(nomRepOut, nomFicGE);
export_GoogleEarth(a, 'nomFic', nomFicGE);
    %}
    
    clear a b
end
my_close(hw, 'MsgEnd');
% SonarScope(Mosa)
