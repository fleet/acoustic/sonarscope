% Cr�ation d'un film � partir des images photo OTUS du Rov Victor
%
% Syntax
%   flag = OTUS_Movie(this, listeFic, nomFicAvi, fps, quality, ...)
%
% Input Arguments
%   this      : Instance de clc_image
%   listeFic  : Liste des fichiers
%   nomFicAvi : Nom du fichier de navigation
%   fps       : Nom du fichier des param�tres du Rov
%   quality   : Qualit� de la compression (75 pard�faut)
%   CLim      : Bornes du rehaussement de contraste
%
% Name-Value Pair Arguments
%   subx : Sous-�chantillonnage en x
%   suby : Sous-�chantillonnage en y
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% See also clc_image/callback_sonar params_OTUS_Movie Authors
% Authors  : JMA
%--------------------------------------------------------------------------

function flag = OTUS_Movie(this, listeFic, nomFicAvi, fps, quality, varargin) %#ok<INUSL>

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = 0;

nbFic = length(listeFic);
if nbFic == 0
    return
end

nomFic = listeFic{1};
% [nomDir1, nomFic1] = fileparts(nomFic);
% nomFicImage = fullfile(nomDir1, [nomFic1 '.jpg']);
% I = imread(nomFicImage);
I = imread(nomFic);

try
    delete(nomFicAvi)
catch %#ok<CTCH>
end

% TODO : faire une d�pendance : Windows7, 32 bits, 64 bits ???

%OLD mov = avifile(nomFicAvi, 'Fps', fps, 'colormap', gray(236), 'quality', quality);
mov = VideoWriter(nomFicAvi, 'Uncompressed AVI'); %, 'MPEG-4'); % FORGE 353
% profiles = VideoWriter.getProfiles()
mov.FrameRate = fps;
% mov.Quality   = quality;
% mov.Colormap = gray(236);
%{
struct(mov)
Duration: 0
Filename: 'Images.mp4'
Path: 'D:\Temp\OTUS\Images'
FileFormat: 'mp4'
IsOpen: 0
Profile: [1x1 audiovideo.writer.profile.MPEG4]
AllowedDataTypes: []
InternalFramesWritten: []
Height: []
FrameRate: 2
VideoFormat: 'RGB24'
FrameCount: 0
VideoBitsPerPixel: 24
ColorChannels: 3
Width: []
VideoCompressionMethod: 'H.264'
Quality: 95
%}
% profiles = VideoWriter.getProfiles()
open(mov);

% set(gcf, 'renderer', 'zbuffer');

str1 = 'Cr�ation du film';
str2 = 'Creates movie';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for i=1:nbFic
    my_waitbar(i, nbFic, hw);
    
    nomFic = listeFic{i};
    fprintf('%d / %d : %s\n', i, nbFic, nomFic)
    
    %     [nomDir1, nomFic1] = fileparts(nomFic);
    %     nomFicImage = fullfile(nomDir1, [nomFic1 '.jpg']);
    %     I = imread(nomFicImage);
    I = imread(nomFic);
    try
        I = I(suby,subx,:);
    catch
        str1 = 'Les images ne semblent pas avoir la m�me taille, on arr�te les frais ici.';
        str2 = 'The images do not seem to be the same size, we stop the processing here.';
        my_warndlg(Lang(str1,str2), 1);
        my_close(hw, 'MsgEnd');
        close(mov);
        flag = 0;
        return
    end
    if size(I,3) == 1
        a = cl_image('Image', single(I), 'Memmapfile', 'No');
        
        CLim = a.StatValues.Quant_25_75;
        I = a.Image(:,:,:);
        [ny, nx, nc] = size(I);
        scale = min(1000 / ny, 1900 / nx);
        if nc == 1
            I = mat2gray(I, CLim);
            I = imresize(I, scale);
            I = uint8(I*255);
        else
            % Peut-�tre tester si c'est du int8 et faire soit I =
            % uint8(I); soit I = uint8(I*255); en fonction de la valeur
            % max de I
            I = imresize(I, scale);
        end
    end
    % OLD mov = addframe(mov, I);
    writeVideo(mov, I); % FORGE 353
end
my_close(hw, 'MsgEnd');
close(mov);

flag = 1;

if isdeployed
    winopen(nomFicAvi)
else
    my_implay(nomFicAvi)
end

