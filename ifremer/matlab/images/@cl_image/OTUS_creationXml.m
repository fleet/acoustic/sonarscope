% Plus utilis� mais conserv� au cas o� !

function flag = OTUS_creationXml(this, listeFic, nomFicNav, nomFicXls) %#ok<INUSL>
 
flag = 0;

nbFic = length(listeFic);
if nbFic == 0
    return
end

%% R�cup�ration des heures des photos

nbFic = length(listeFic);
date  = zeros(1, nbFic);
heure = zeros(1, nbFic);
hw = create_waitbar('Lecture des heures des images', 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
   
    nomFic = listeFic{k};
    [~, nomFic1] = fileparts(nomFic);
    nomFic1(strfind(nomFic1, '_')) = '-';
    mots = strsplit(nomFic1, '-');
    Jour    = str2double(mots{2});
    Mois    = str2double(mots{3});
    Annee   = str2double(mots{4});
    Heure   = str2double(mots{5});
    Minute  = str2double(mots{6});
    Seconde = str2double(mots{7});
    Milli   = str2double(mots{8});
    
    date(k)  = dayJma2Ifr(Jour, Mois, Annee);
    heure(k) = (Heure*3600 + Minute*60 + Seconde)*1000 + Milli;
end
my_close(hw);
t_photos = cl_time('timeIfr', date, heure);

%% Lecture de la navigation Caraibes corrig�e par Regina

[flag, LatNvi, LonNvi, tNvi, Heading, Speed] = lecFicNav(nomFicNav);
if ~flag
    messageErreurFichier(nomFicNav, 'ReadFailure');
    return
end

Lon = my_interp1_longitude(tNvi, LonNvi, t_photos);
Lat = my_interp1(tNvi, LatNvi, t_photos);
subNaN    = find(isnan(Heading));
subNonNaN = find(~isnan(Heading));

% TODO : remplacer par my_interp1_headingDeg_longitude
Heading(subNaN) = interp1(subNonNaN, Heading(subNonNaN), subNaN);
Cap = my_interp1_headingDeg(tNvi, Heading, t_photos);

%     H = repmat(8, 1, length(t_photos));

% figure; plot(LonNvi, LatNvi, 'k'); grid on;
% hold on; plot(Lon, Lat, '*b'); grid on; hold off;
% str = sprintf('OTUS navigation\n%s', nomFicNav);
% title(str, 'Interpreter', 'none')

% Fig1 = figure;
% plot_navigation(nomFicNav, Fig1, LonNvi, LatNvi, tNvi, Heading)
% hold on; plot(Lon, Lat, '*b'); grid on; hold off;

plot_navigation(nomFicNav, [], LonNvi, LatNvi, tNvi.timeMat, Heading);
hold on; plot(Lon, Lat, '*k'); grid on; hold off;


%% Lecture de la nav dans le fichier excel

[flag, DataNav] = read_NavXlsRov(nomFicXls);
if ~flag
    my_warndlg(['File "' nomFicXls '" coud not be read.'], 1);
    return
end

H        = my_interp1(DataNav.t, DataNav.ALTITUDE, t_photos);
Tilt     = my_interp1(DataNav.t, DataNav.ANGLE_TILT, t_photos);
Pan      = my_interp1(DataNav.t, DataNav.ANGLE_PAN, t_photos);
GITE     = my_interp1(DataNav.t, DataNav.GITE, t_photos);
ASSIETTE = my_interp1(DataNav.t, DataNav.ASSIETTE, t_photos);
CAP_3CCD = my_interp1_headingDeg(DataNav.t, DataNav.CAP_3CCD, t_photos);

%% Exportation des coordonn�es en xml

Focale = 53;
hw = create_waitbar('Cr�ation des fichiers xml', 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    if isnan(Cap(k))
        continue
    end
    
    Data.Heading = Cap(k);
    Data.Lat     = lat2str(Lat(k), ' ', '09.6f');
    Data.Lon     = lon2str(Lon(k), ' ', '09.6f'); 
    Data.Height  = H(k);
    Data.Focale  = Focale;
    
    Data.Tilt       = Tilt(k);
    Data.Pan        = Pan(k);
    Data.GITE       = GITE(k);
    Data.ASSIETTE   = ASSIETTE(k);
    Data.CAP_3CCD   = CAP_3CCD(k);

    nomFic = listeFic{k};
    [nomDir1, nomFic1] = fileparts(nomFic);
    nomFicXml = fullfile(nomDir1, [nomFic1 '.xml']);
    xml_write(nomFicXml, Data);
end
my_close(hw, 'MsgEnd');
