function flag = OTUS_creationXmlFromNMEA(~, listeFic, nomFicNmea, nomFicNavCorrigee, nomFicOut)
 
flag = 0;

nbFicImages = length(listeFic);
if nbFicImages == 0
    return
end

%% R�cup�ration des heures des photos

nbFicImages = length(listeFic);
t_photos = NaT(nbFicImages,1);
hw = create_waitbar('Lecture des heures des images', 'N', nbFicImages);
for k=1:nbFicImages
    my_waitbar(k, nbFicImages, hw);
    [flag, t] = OTUS_getTimeFromFileName(listeFic{k});
    if flag
        t_photos(k) = t;
    end
end
my_close(hw, 'MsgEnd');

%% Lecture des fichiers NMEA

TNav        = datetime.empty();
TCam        = datetime.empty();
Lat         = [];
Lon         = [];
Dep         = [];
Alt         = [];
Hea         = [];
Pan         = [];
Tilt        = [];
Zoom        = [];
Focus       = [];
Iris        = [];
SciZoom     = [];
SciFocus    = [];
SciIris     = [];

Fig1 = [];

nbFicNMEA = length(nomFicNmea);
hw = create_waitbar('Lecture des fichiers NMEA', 'N', nbFicNMEA);
for k=1:nbFicNMEA
    my_waitbar(k, nbFicNMEA, hw);

    [nomDirSeul, nomFicSeul] = fileparts(nomFicNmea{k});
    nomFicMat = fullfile(nomDirSeul, [nomFicSeul '.mat']);
    
    if exist(nomFicMat, 'file')
        NMEA = loadmat(nomFicMat, 'nomVar', 'NMEA');
    else
        [flag, NMEA] = lectureFicNMEA(nomFicNmea{k});
        if ~flag
            continue
        end
        save(nomFicMat, 'NMEA')
    end
    
    sub = ((NMEA.Longitude > -1e-3) & (NMEA.Longitude < 1e-3) & (NMEA.Latitude > -1e-3) & (NMEA.Latitude < 1e-3));
    subKO = find(sub);
    if ~isempty(subKO)
        subOK = find(~sub);
        NMEA.Longitude(subKO) = interp1(subOK, NMEA.Longitude(subOK), subKO);
        NMEA.Latitude(subKO)  = interp1(subOK, NMEA.Latitude(subOK), subKO);
        NMEA.heading(subKO)   = interp1(subOK, NMEA.heading(subOK), subKO);
    end

%     tNav = cl_time('timeIfr', NMEA.dateNav, NMEA.heureNav);
    tNav = timeIfr2Datetime(NMEA.dateNav, NMEA.heureNav);
    
    Fig1 = plot_navigation(nomFicNmea{k}, Fig1, NMEA.Longitude, NMEA.Latitude, tNav, NMEA.heading);
    
    TNav = [TNav; tNav]; %#ok<AGROW>
    Lat  = [Lat; NMEA.Latitude]; %#ok<AGROW>
    Lon  = [Lon; NMEA.Longitude];  %#ok<AGROW>
    Dep  = [Dep; NMEA.depth]; %#ok<AGROW>
    Alt  = [Alt; NMEA.altitude]; %#ok<AGROW>
    Hea  = [Hea; NMEA.heading]; %#ok<AGROW>
    
    tCam =  timeIfr2Datetime(NMEA.dateCam, NMEA.heureCam);
    TCam     = [TCam;     tCam]; %#ok<AGROW>
    Pan      = [Pan;      NMEA.panCam];   %#ok<AGROW>  
    Tilt     = [Tilt;     NMEA.tiltCam];   %#ok<AGROW>  
    Zoom     = [Zoom;     NMEA.zoomCam];   %#ok<AGROW>  
    Focus    = [Focus;    NMEA.focusCam];    %#ok<AGROW> 
    Iris     = [Iris;     NMEA.irisCam];     %#ok<AGROW>
    SciZoom  = [SciZoom;  NMEA.sciZoomCam];     %#ok<AGROW>
    SciFocus = [SciFocus; NMEA.sciFocusCam];    %#ok<AGROW> 
    SciIris  = [SciIris;  NMEA.sciIrisCam];     %#ok<AGROW>
end
my_close(hw);

[TNav,IX] = sort(TNav);
Lat = Lat(IX);
Lon = Lon(IX);
% Dep = Dep(IX);
Alt = Alt(IX);
Hea = Hea(IX);
FigUtils.createSScFigure; PlotUtils.createSScPlot(Lon, Lat, '*-'); grid on; axisGeo

%% Lecture de la navigation Caraibes corrig�e par Regina

if ~isempty(nomFicNavCorrigee)
    [flag, LatNvi, LonNvi, tNvi, Heading, Speed] = lecFicNav(nomFicNavCorrigee); %#ok<ASGLU>
    if ~flag
        messageErreurFichier(nomFicNav, 'ReadFailure');
        return
    end
    
    TNVI = datetime(tNvi.timeMat, 'ConvertFrom', 'datenum');
    Lon = my_interp1_longitude(TNVI, LonNvi, TNav);
    Lat = my_interp1(          TNVI, LatNvi, TNav);
    
    % figure; plot(LonNvi, LatNvi, 'k'); grid on;
    % hold on; plot(Lon, Lat, '*b'); grid on; hold off;
    % str = sprintf('OTUS navigation\n%s', nomFicNav);
    % title(str, 'Interpreter', 'none')
    
    % Fig1 = figure;
    % plot_navigation(nomFicNav, Fig1, LonNvi, LatNvi, TNVI, Heading)
    % hold on; plot(Lon, Lat, '*b'); grid on; hold off;
    
    plot_navigation(nomFicNavCorrigee, [], LonNvi, LatNvi, TNVI, Heading);
    plot_navigation(nomFicNavCorrigee, [], Lon, Lat, TNav, []);
%     hold on; plot(Lon, Lat, '*k'); grid on; hold off;
end

%% Lecture de la nav dans le fichier excel

PanNav      = my_interp1(TCam, Pan,  t_photos);
TiltNav     = my_interp1(TCam, Tilt, t_photos);
% ZoomNav     = my_interp1(TCam, Zoom,     t_photos);
% IrisNav     = my_interp1(TCam, Focus,    t_photos);
% SciZoomNav  = my_interp1(TCam, SciZoom,  t_photos);
% SciFocusNav = my_interp1(TCam, SciFocus, t_photos);
% SciIrisNav  = my_interp1(TCam, SciIris,  t_photos);
Cap         = my_interp1(TNav, Hea, t_photos);
H           = my_interp1(TNav, Alt, t_photos);
LatPhotos   = my_interp1(TNav, Lat, t_photos);
LonPhotos   = my_interp1_longitude(TNav, Lon, t_photos);

plot_navigation(nomFicNavCorrigee, [], LonPhotos, LatPhotos, t_photos, []);

% GITE     = my_interp1(DataNav.t, DataNav.GITE, t_photos);
% ASSIETTE = my_interp1(DataNav.t, DataNav.ASSIETTE, t_photos);
% CAP_3CCD = my_interp1_headingDeg(DataNav.t, DataNav.CAP_3CCD, t_photos);

%% Exportation des donn�es dans une structure XML-Bin

tMat = NaT(nbFicImages, 1);
str1 = 'Lecture des heures � partir des noms des images';
str2 = 'Reads time from the image names';
hw = create_waitbar(Lang(str1,str2), 'N', nbFicImages);
for k=1:nbFicImages
    my_waitbar(k, nbFicImages, hw);
    [flag, t] = OTUS_getTimeFromFileName(listeFic{k});
    if flag
        tMat(k) = t;
    end
end
my_close(hw);

Focale = 53;
N = length(LatPhotos);
DataAllFiles.Latitude  = LatPhotos;
DataAllFiles.Longitude = LonPhotos;
DataAllFiles.Height    = H;
DataAllFiles.Heading   = Cap;
DataAllFiles.Tilt      = TiltNav;
DataAllFiles.Pan       = PanNav;
DataAllFiles.Focale    = Focale + zeros(1, N, 'single');
DataAllFiles.GITE      = zeros(1, N, 'single'); % GITE(k);
DataAllFiles.ASSIETTE  = zeros(1, N, 'single'); %ASSIETTE(k);
DataAllFiles.CAP_3CCD  = Cap; % CAP_3CCD(k);
DataAllFiles.Time      = datenum(tMat);
DataAllFiles.FileNames = listeFic;

flag = OTUS_write_XML_Bin(nomFicOut, DataAllFiles);


function [flag, NMEA] = lectureFicNMEA(nomFic)

flag = 0;
NMEA = [];

N = getNbLinesAsciiFile(nomFic);
if N == 0
    return
end

fid = fopen(nomFic, 'r');
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

NMEA.dateNav      = NaN(N,1);
NMEA.heureNav     = NaN(N,1);
NMEA.Latitude     = NaN(N,1);
NMEA.Longitude    = NaN(N,1);
NMEA.depth        = NaN(N, 1, 'single');
NMEA.altitude     = NaN(N, 1, 'single');
NMEA.heading      = NaN(N, 1, 'single');
NMEA.dateCam      = NaN(N,1);
NMEA.heureCam     = NaN(N,1);
NMEA.panCam       = NaN(N, 1, 'single');
NMEA.tiltCam      = NaN(N, 1, 'single');
NMEA.zoomCam      = NaN(N, 1, 'single');
NMEA.focusCam     = NaN(N, 1, 'single');
NMEA.irisCam      = NaN(N, 1, 'single');
NMEA.sciZoomCam   = NaN(N, 1, 'single');
NMEA.sciFocusCam  = NaN(N, 1, 'single');
NMEA.sciIrisCam   = NaN(N, 1, 'single');

kNav = 0;
kCam = 0;

hw2 = create_waitbar(['Lecture des enregistrements du fichier ' nomFic], 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw2);
    
    tline = fgetl(fid);
    %         disp(tline)
    
    Mots = strsplit(tline, ',');
    if (length(Mots) >= 4) && strcmpi(Mots{4}, 'navigation')
        kNav = kNav + 1;
        
        % $MSF2,29/09/2011,16:42:07.970,NAVIGATION,victor.nav,controller,latitude=0051.3671313,longitude=-014.7277514,depth=0003.35,altitude=142.87,heading=345.0
        
        NMEA.dateNav(kNav)  = dayStr2Ifr(Mots{2});
        HH = str2double(Mots{3}(1:2));
        MM = str2double(Mots{3}(4:5));
        SS = str2double(Mots{3}(7:end));
        NMEA.heureNav(kNav) = (HH * 3600 + MM * 60 + SS) * 1000;
        
        NMEA.Latitude(kNav)  = str2double(Mots{7}(10:end));
        NMEA.Longitude(kNav) = str2double(Mots{8}(11:end));
        NMEA.depth(kNav)     = str2double(Mots{9}(7:end));
        NMEA.altitude(kNav)  = str2double(Mots{10}(10:end));
        NMEA.heading(kNav)   = str2double(Mots{11}(9:end));
    end
    if (length(Mots) >= 4) && strcmp(Mots{4}, 'HDCAMERA')
        kCam = kCam + 1;
        
        % $MSF2,29/09/2011,16:55:30.970,HDCAMERA,victor.Cameras,controller,pan=-00.50,tilt=-23.83,zoom=01.0,focus=014.9,iris=001.8,sciZoom=01.0,sciFocus=003.8,sciIris=099.9
        % $MSF2,29/09/2011,16:55:30.970,HDCAMERA,victor.Cameras,controller,pan=-00.50,tilt=-23.83,zoom=01.0,focus=014.9,iris=001.8,sciZoom=01.0,sciFocus=003.8,sciIris=099.9
        NMEA.dateCam(kCam)  = dayStr2Ifr(Mots{2});
        HH = str2double(Mots{3}(1:2));
        MM = str2double(Mots{3}(4:5));
        SS = str2double(Mots{3}(7:end));
        NMEA.heureCam(kCam) = (HH * 3600 + MM * 60 + SS) * 1000;
        
        NMEA.panCam(kCam)      = str2double(Mots{7}(5:end));
        NMEA.tiltCam(kCam)     = str2double(Mots{8}(6:end));
        NMEA.zoomCam(kCam)     = str2double(Mots{9}(6:end));
        NMEA.focusCam(kCam)    = str2double(Mots{10}(7:end));
        NMEA.irisCam(kCam)     = str2double(Mots{11}(9:end));
        NMEA.sciZoomCam(kCam)  = str2double(Mots{12}(9:end));
        NMEA.sciFocusCam(kCam) = str2double(Mots{13}(10:end));
        NMEA.sciIrisCam(kCam)  = str2double(Mots{14}(9:end));
    end
end
fclose(fid);
my_close(hw2);

subNav = 1:kNav;
NMEA.dateNav      = NMEA.dateNav(subNav);
NMEA.heureNav     = NMEA.heureNav(subNav);
NMEA.Latitude     = NMEA.Latitude(subNav);
NMEA.Longitude    = NMEA.Longitude(subNav);
NMEA.depth        = NMEA.depth(subNav);
NMEA.altitude     = NMEA.altitude(subNav);
NMEA.heading      = NMEA.heading(subNav);

subCam = 1:kCam;
NMEA.dateCam     = NMEA.dateCam(subCam);
NMEA.heureCam    = NMEA.heureCam(subCam);
NMEA.panCam      = NMEA.panCam(subCam);
NMEA.tiltCam     = NMEA.tiltCam(subCam);
NMEA.zoomCam     = NMEA.zoomCam(subCam);
NMEA.focusCam    = NMEA.focusCam(subCam);
NMEA.irisCam     = NMEA.irisCam(subCam);
NMEA.sciZoomCam  = NMEA.sciZoomCam(subCam);
NMEA.sciFocusCam = NMEA.sciFocusCam(subCam);
NMEA.sciIrisCam  = NMEA.sciIrisCam(subCam);

sub = ((NMEA.Latitude == 0) | (NMEA.Longitude == 0) | isnan(NMEA.Latitude)  | isnan(NMEA.Longitude));
NMEA.dateNav(sub)   = [];
NMEA.heureNav(sub)  = [];
NMEA.Latitude(sub)  = [];
NMEA.Longitude(sub) = [];
NMEA.depth(sub)     = [];
NMEA.altitude(sub)  = [];
NMEA.heading(sub)   = [];

flag = 1;


function flag = OTUS_write_XML_Bin(nomFicXml, Data)

[nomDirRacine, nomFicSeul] = fileparts(nomFicXml);
NomDirADefinir = nomFicSeul;
nbSamples = length(Data.Latitude);

%% G�n�ration du XML SonarScope

Info.Title                  = 'OTUS';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Information relative aux images OTUS';
Info.FormatVersion          = '20140125';

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Latitude';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Latitude.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'Longitude';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Longitude.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSLatitude');
  
Info.Signals(end+1).Name      = 'Height';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Height.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSAccuracy');

Info.Signals(end+1).Name      = 'Tilt';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Tilt.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Pan';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'v.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSHeading');

Info.Signals(end+1).Name      = 'Focale';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Focale.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'GITE';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'GITE.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ASSIETTE';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'ASSIETTE.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'CAP_3CCD';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'CAP_3CCD.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Strings(1).Name          = 'FileNames';
Info.Strings(end).Dimensions  = 'NbSamples, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile(NomDirADefinir,'FileNames.bin');
Info.Strings(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire des donn�es binaires

NomDirBin = fullfile(nomDirRacine, NomDirADefinir);
if ~exist(NomDirBin, 'dir')
    status = mkdir(NomDirBin);
    if ~status
        messageErreur(NomDirBin)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des strings

if isfield(Info, 'Strings')
    for k=1:length(Info.Strings)
        flag = writeString(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
        if ~flag
            return
        end
    end
end

flag = 1;
