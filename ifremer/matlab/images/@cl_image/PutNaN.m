% Put NaN to pixels defined bt subx and suby
%
% Syntax
%  b = PutNaN(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a);
%
%   b = PutNaN(a, 'subx', 20:30, 'suby', 50:90);
%   imagesc(b);
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = PutNaN(this, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_PutNaN(this(i), varargin{:});
end


function this = unitaire_PutNaN(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% --------------------------------
% Appel de la fonction de filtrage

I = this.Image(:,:,:);
I(suby,subx,:) = this.ValNaN;
this = replace_Image(this, I);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this = update_Name(this, 'Append', 'PutNaN');
