% Transformation d'une image RGB en 3 images : Hue, Saturation, Value
%
% Syntax
%   [flag, b] = RGB2HSV(a, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant l'image en RGB
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   flag : Booleen indiquant si le calcul a ete realise
%   b    : Instances de cl_image contenant les 3 composantes
%
% Examples
%   RGB = imread(getNomFicDatabase('Mandrill.jpg'));
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%   [flag, b] = RGB2HSV(a);
%   imagesc(b)
%
% See also cl_image/rgb2indexee Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = RGB2HSV(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = unitaire_RGB2HSV(this(i), varargin{:});
    if ~flag
        return
    end
    this(1:3,i) = that;
end
this = this(:);


function [flag, this] = unitaire_RGB2HSV(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

ImageName = this.Name;

% --------------------------------
% Appel de la fonction de filtrage

if this.ImageType ~= 2
    my_warndlg('Cette image n''est pas en RGB (ImageType~=2)', 1);
    flag = 0;
    return
end

% nl = size(this.Image, 1);
% nc = size(this.Image, 2);

HSV = rgb2hsv(this.Image(suby,subx,:));

this = replace_Image(this, HSV(:,:,1));

this.ValNaN    = NaN;
this.ImageType = 1;

% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

% Calcul des statistiques

this = compute_stats(this);

% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.Unit = ' ';

% Completion du titre

this = update_Name(this, 'Append', 'Hue');
this(1).ColormapIndex = 3;


%% Saturation

this(2) = replace_Image(this(1), HSV(:,:,2));
this(2) = compute_stats(this(2));
this(2).TagSynchroContrast = num2str(rand(1));
this(2).CLim               = [this(2).StatValues.Min this(2).StatValues.Max];
this(2).ColormapIndex      = 2;
this(2).Name               = ImageName;
this(2) = update_Name(this(2), 'Append', 'Saturation');

%% Value

this(3) = replace_Image(this(1), HSV(:,:,3));
this(3) = compute_stats(this(3));
this(3).TagSynchroContrast = num2str(rand(1));
this(3).CLim               = [this(3).StatValues.Min this(3).StatValues.Max];
this(3).ColormapIndex      = 2;
this(3).Name               =  ImageName;
this(3) = update_Name(this(3), 'Append', 'Value');

%% Par ici la sortie

flag = 1;
