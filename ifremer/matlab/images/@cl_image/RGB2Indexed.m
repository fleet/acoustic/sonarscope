% Transformation d'une image RGB en image indexee
%
% Syntax
%   [b, flag] = RGB2Indexed(a, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant l'image en RGB
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b    : Instance de cl_image contenant l'image indexee
%   flag : Booleen indiquant si le calcul a ete realise
%
% Examples
%   RGB = imread(getNomFicDatabase('BouffayNeptuneMadeleine.tif'));
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%   [b, flag] = RGB2Indexed(a);
%   imagesc(b)
%
% See also cl_image/RGB2Indexed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = RGB2Indexed(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [this(k), flag] = unitaire_RGB2Indexed(this(k), varargin{:});
    if ~flag
        return
    end
end


function [this, flag] = unitaire_RGB2Indexed(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nbCoul] = getPropertyValue(varargin, 'nbCoul', 256);
[varargin, map]    = getPropertyValue(varargin, 'map', []); %#ok<ASGLU>

%% Appel de la fonction de filtrage

if this.ImageType ~= 2
    my_warndlg('Cette image n''est pas en RGB (ImageType~=2)', 1);
    flag = 0;
    return
end

I = this.Image(suby,subx,:);

if isnan(this.ValNaN)
    subNaN = find(isnan(I));
else
    subNaN = find((I == this.ValNaN) | isnan(I)); % Pour rattraper un bug importation ErMapper
    %     subNaN = find((I == this.ValNaN)); % rattrapage de bug supprim� car manque de m�moire.
    % Remis en commentaire car bug exportation Golfe du Lion avec Roger
end
I(subNaN) = 0;
subNaN = find(sum(I,3) == 0);

if ~isa(I, 'uint8')
    if (this.StatValues.Max ~= 0) && (this.StatValues.Max <= 1)
        I = this.Image(suby,subx,:) * 255;
    end
    I = uint8(floor(I));
end

if isempty(map)
    [X, map] = rgb2ind(I, nbCoul-1);
    X = X + 1;
    
    % D�but ajout pour Roger : on r�ordonne les couleurs
    mapHSV = rgb2hsv(map);
    HSV = mapHSV(:,3) * 1e4 + mapHSV(:,2) * 1e2 + mapHSV(:,1);
    %     mapHSV = rgb2lab(map);
    %     HSV = mapHSV(:,3);
    [~, ordre] = sort(HSV);
    map = map(ordre,:);
    Y = X;
    for k=1:size(map,1)
        sub = (X == ordre(k));
        Y(sub) = k;
    end
    X = Y;
    %     mapHSV = mapHSV(ordre,:);
    %     figure; plot(mapHSV(:,3), '.-'); grid on
    % Fin ajout pour Roger
else
    X = rgb2ind(I, map);
end
X(subNaN) = 0;
map = [0 0 0 ; map];
% figure;image(X); colormap(map); colorbar
this.Image          = X;
this.Colormap       = map;
this.ColormapCustom = map;
this.ColormapIndex = 1;

if isempty(subNaN)
    ValNaN = 0;
else
    ValNaN = X(subNaN(1));
end

this.ValNaN     = ValNaN;
this.ImageType  = 3;

%% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.Unit = ' ';

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Indexed');

%% Par ici la sortie

flag = 1;
