% Convert RGB image to Intensity
%
% Syntax
%   [b, flag] = RGB2Intensity(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   RGB = imread('peppers.png');
%   a = cl_image('Image', RGB, 'ImageType', 2);
%   imagesc(a); axis equal; axis tight
%
%   [b, flag] = RGB2Intensity(a);
%   imagesc(b); colormap(gray(256))
%
% See also cl_image/rgb2indexee Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = RGB2Intensity(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('RGB2Intensity'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [K, flag] = RGB2Intensity_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [that, flag] = RGB2Intensity_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

that = [];
flag = 1;

%% Control

if this.ImageType ~= 2
    str1 = 'Cette image n''est pas en RGB (ImageType~=2)';
    str2 = 'This image is not a RGB one (ImageType~=2)';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Algorithm

I = this.Image(suby,subx,:);
if max(I(:)) > 1
    I = single(I) / 255;
end
[X, map] = rgb2ind(I, 128);
I = ind2gray(X, map);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'RGB2Intensity', 'ValNaN', 0, ...
    'ImageType', 1, 'Unit', ' ', 'ColormapIndex', 2);
