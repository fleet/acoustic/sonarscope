% Changement de la couleur des NaN d'une image RGB
%
% Syntax
%   [b, flag] = RGBNaNColor(a, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant l'image en RGB
%
% Name-Value Pair Arguments
%   Rayon : Rayon du masque englobant (0 par defaut)
%   subx  : subsampling in X
%   suby  : subsampling in Y
%
% Output Arguments
%   b    : Instance de cl_image contenant l'image en intensitee
%   flag : Booleen indiquant si le calcul a ete realise
%
% Examples
%   TODO
%   [b, flag] = RGBNaNColor(a);
%   imagesc(b)
%
%
% See also cl_image/rgb2indexee Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = RGBNaNColor(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [this(i), flag] = RGBNaNColor_unit(this(i), varargin{:});
    if ~flag
        return
    end
end


function [this, flag] = RGBNaNColor_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Rayon] = getPropertyValue(varargin, 'Rayon', 0); %#ok<ASGLU>

%% Appel de la fonction de filtrage

if this.ImageType ~= 2
    my_warndlg('Cette image n''est pas en RGB (ImageType~=2)', 1);
    flag = 0;
    return
end

Mask = masque_englobant(this, 'Rayon', Rayon, 'subx', subx, 'suby', suby);

ValMax = this.StatValues.Max;
if ValMax <= 1
    ValNoir = 1;
else
    ValNoir = 255;
end

I = this.Image(suby,subx,:);
for i=1:3
    J = I(:,:,i);
    J(~Mask.Image(:,:)) = ValNoir;
    I(:,:,i) = J;
end

this = replace_Image(this, I);

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this = update_Name(this, 'Append', 'RGBNaNColor');

% -----------------
% Par ici la sortie

flag = 1;
