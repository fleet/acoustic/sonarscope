% Create a mask using constant values or an interval from the image values
%
% Syntax
%   b = ROI_auto(a, createLayer, LayerName, constants, valInterval, valMask, ...)
%
% Input Arguments
%   a           : One cl_image instance of the image
%   createLayer : 1=create an image, 0=use an existing image
%   LayerName   : Name of the Mask
%   layerMasque : [] or a cl_image instance of a preexisting mask to be appended
%   constants   : Array of constants that determine the mask on the corresponding values of the image
%   valInterval : Interval on image values defining the mask (used only if "constants" is empty
%   valMask     : Value of the mask
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : A cl_image instance containing the mask
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a);
%   b = ROI_auto(a, 1, 'Toto', [], [], [-2500 0], 1);
%     imagesc(b);
%   b = ROI_auto(a, 0, 'Toto', b, [], [-2800 -2500], 2);
%     imagesc(b);
%   b = ROI_auto(a, 0, 'Toto', b, [], [-5000 -2800], 3);
%     imagesc(b);
%     Fig = ROI_plot(a)
%
% See also ROI_manual ROI_auto ROI_plot ROI_export ROI_import Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = ROI_auto(this, createLayer, LayerName, layerMasque, constants, valInterval, valMask, varargin)

that = cl_image.empty;

N = length(this);
str1 = 'Masquage des images';
str2 = 'Masking images';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    if length(layerMasque) <= 1
        M = layerMasque;
    else
        M = layerMasque(k);
    end
    that(k) = ROI_auto_unitaire(this(k), createLayer, LayerName, M, constants, valInterval, valMask, varargin{:});
end
my_close(hw);


function layerMasque = ROI_auto_unitaire(this, createLayer, LayerName, layerMasque, constants, valInterval, valMask, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

if createLayer
    layerMasque = this;
    M = zeros(size(this.Image), 'single');
    
    layerMasque = replace_Image(layerMasque, M);
    clear M
    layerMasque.ValNaN = NaN;
    
    NuIdentParent = this.NuIdent;
    NuIdent       = floor(rand(1) * 1e8);
else
    NuIdentParent = layerMasque.NuIdentParent;
    NuIdent       = layerMasque.NuIdent;
end

M = layerMasque.Image(suby, subx);

if isempty(constants)
    if isequal(size(valInterval) , [2 1])
        valInterval = valInterval';
    end
    for k=1:size(valInterval,1)
        subk = (this.Image(suby, subx) >= valInterval(k,1)) & ...
            (this.Image(suby, subx) <= valInterval(k,2));
        M(subk) = valMask(k);
    end
else
    constants = unique(constants);
    for k=1:length(constants)
        sub = (this.Image(suby, subx) == constants(k));
        M(sub) = valMask;
    end
end

layerMasque.Writable = true;
layerMasque.Image(suby, subx) = M;
layerMasque.Writable = false;

%% Mise � jour des coordonn�es

layerMasque = majCoordonnees(layerMasque, 1:length(this.x), 1:length(this.y));
layerMasque.DataType = cl_image.indDataType('Mask');
layerMasque.Unit = 'num';

%% Calcul des statistiques

layerMasque = compute_stats(layerMasque);

%% Rehaussement de contraste

layerMasque.TagSynchroContrast = num2str(rand(1));
CLim = [0 layerMasque.StatValues.Max];
layerMasque.CLim = CLim;

%% Compl�tion du titre

if createLayer
    layerMasque = update_Name(layerMasque, 'Append', LayerName);
    layerMasque.ColormapIndex = 3;
else
    this.NuIdentParent = NuIdentParent;
    this.NuIdent       = NuIdent;
end
