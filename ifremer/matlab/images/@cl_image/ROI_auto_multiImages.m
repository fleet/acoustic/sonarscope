function [flag, a] = ROI_auto_multiImages(this, indLayers, valInterval, valMask)
 
flag = 1;
a = cl_image.empty;

str1 = 'Masquage des images';
str2 = 'Masking images';
N = length(indLayers);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    a(k) = ROI_auto(this(indLayers(k)), 1, 'Mask', [], [], valInterval, valMask); 
    a(k) = optimiseMemory(a(k), 'SizeMax', 0);
end
my_close(hw)
