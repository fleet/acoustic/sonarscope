% Delete some Regions Of Interest contained in an image
%
% Syntax
%   ROI_delete(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   a : The updated cl_image instance
%
% Examples
%     nomFic = getNomFicDatabase('TexturesSonarMontage02.tif');
%     [flag, a] = import(cl_image, nomFic);
%     [numFig, AxeImage] = imagesc(a);
%     [flag, b, a] = ROI_manual(a, 1, 'Mon Masque', [], numFig, AxeImage);
%     imagesc(b, 'CLim', [0 2])
%     Fig = ROI_plot(b);
%     b = ROI_rename(b);
%     Fig = ROI_plot(b);
%   b = ROI_delete(b);
%     Fig = ROI_plot(b);
%
% See also cl_image/ROI_manual cl_image/ROI_auto cl_image/ROI_plot cl_image/ROI_export cl_image/ROI_import
%            cl_image/rename Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = ROI_delete(this)

%% Synth�se de tous les contours

[this, str] = ROI_summary(this);

%% Lecture des r�gions d'int�r�t contenues dans l'image courante

RegionOfInterest = get(this, 'RegionOfInterest');
if isempty(RegionOfInterest)
    my_warndlg('No Region Of Interest in this image', 1);
    return
end

%% Affichage de tous les contours dans une nouvelle figure

ROI_plot(this);

%% S�lection des contours � afficher dans SonarScope

str1 = 'R�gion d''int�r�t � supprimer';
str2 = 'ROI to delete';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:length(str), 'Entete', 'IFREMER - SonarScope - ROI export');
if ~flag
    return
end
if isempty(rep)
    return
end

% Suppression des masques s�lectionn�s

rep = setdiff(1:length(RegionOfInterest), rep);
this = set(this, 'RegionOfInterest', RegionOfInterest(rep));

