% Export the Region(s) Of Interest of an image in a file
% This function display a file browser to select the file format then the file name
% file name and then a list of the ROI(s). The user can select one, several or all the ROIs
% The expected extensions are .xml, .kml, .kmz, .mat and .shp
%
% Syntax
%   [flag, repExport] = ROI_export(this, repExport)
%
% Input Arguments
%   a         : One cl_image instance
%   repExport : Default directory to init the file browser
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   repImport : Name of the directory where the file was writen
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%   [flag, repExport] = ROI_export(a, my_tempdir)
%
% See also ROI_export_xml ROI_export_kml ROI_export_mat ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : devrait �tre s�par� en 2 : param�tres et export

function [flag, repExport] = ROI_export(this, repExport)

%% Type de fichier

str1 = 'Type de fichier.';
str2 = 'Type of file.';
str = {'XML files (*.xml)'; ...
    'MatLab files (*.mat)'; ...
    'Shape files (*.shp)';...
    'Google-Earth files (*.kmz)'};
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end
listExt = {'.xml'; '.mat'; '.shp'; '.kmz'};
Ext = listExt{rep};

%% Tag suppl�mentaire dans le mom des images si on fait un export multi-images

nbImages = length(this);
if nbImages == 1
    that = update_Name(this);
    
    filtre = initNomFic(that, repExport, 'ROI', Ext);
    str1 = 'Nom du fichier';
    str2 = 'Give a file name';
    [flag, nomFicMasque] = my_uiputfile(['*' Ext], Lang(str1,str2), filtre);
    if ~flag
        return
    end
    repExport = fileparts(nomFicMasque);
else
    % Nom du r�pertoire
    
    str1 = 'Nom du r�pertoire o� seront sauv�s les fichiers,';
    str2 = 'Name of the directory to save the files. Use the "Create a new directory" button if necessary.';
    [flag, nomRepOut] = my_uigetdir(repExport, Lang(str1,str2));
    if ~flag
        return
    end
    repExport = nomRepOut;
    
    % Pr�fixe ou not Pr�fixe
    
    str1 = 'Voulez-vous rajouter un pr�fixe personnel dans les noms de fichiers qui seront g�n�r�s ?';
    str2 = 'Do you want to add your own prefix into the file names that will be created ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if rep == 1
        str1 = 'Pr�fixe � inclure dans les noms de fichiers.';
        str2 = 'Prefix to include in file names.';
        nomVar = {Lang(str1,str2)};
        value = {''};
        [value, flag] = my_inputdlg(nomVar, value);
        if ~flag
            return
        end
        Prefix = value{1};
    else
        Prefix = [];
    end
end

%% Boucle sur les images

str1 = 'Export des images au format ErMapper';
str2 = 'ErMapper Images Export';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for indImage=1:nbImages
    my_waitbar(indImage, nbImages, hw)
    
    that = update_Name(this(indImage));
    
    if nbImages ~= 1
        nomFicMasque = initNomFic(that, repExport, Prefix, Ext);
    end
    
    %% Synth�se de tous les contours
    
    [that, str] = ROI_summary(that);
    
    %% Lecture des r�gions d'int�r�t contenues dans l'image courante
    
    ROI = get(that, 'RegionOfInterest');
    if isempty(ROI)
        if nbImages == 1
            str1 = 'Il n''y a pas de r�gion d''int�r�t dans cette image.';
            str2 = 'No Region Of Interest in this image.';
            my_warndlg(Lang(str1,str2), 1);
            return
        else
            str1 = sprintf('Il n''y a pas de r�gion d''int�r�t dans "%s".', that.Name);
            str2 = sprintf('No Region Of Interest in "%s".', that.Name);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoROIINThisImage', 'TimeDelay', 60);
            continue
        end
    end
    
    %% Affichage de tous les contours dans une nouvelle figure
    
    %{
    onTrace = false;
    N = length(ROI);
    if N > 100
        [rep, validation] = my_questdlg(Lang(...
            sprintf('Il y a %d r�gions d''int�r�ts.\nLes afficher prendra un certain temps...\nVoulez-vous vraiment les afficher ?', N),...
            sprintf('There is %d related ROI.\nDisplaying them will take a long time...\nIs this what you want ?', N)), 'Init', 1);
        if validation && rep == 1
            onTrace = true;
        end
    else
        onTrace = true;
    end
    
    if onTrace
        ROI_plot(that);
    end
    %}
    
    %% S�lection des contours � afficher dans SonarScope
    
    if nbImages == 1
        str1 = 'R�gions d''int�r�t � exporter';
        str2 = 'Regions of interest to export';
        [listeROI, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:length(str), 'Entete', 'IFREMER - SonarScope - ROI export');
        if ~flag
            return
        end
        if isempty(listeROI)
            return
        end
    else
        listeROI = 1:length(str);
    end
    
    
    %% Extraction des contours s�lectionn�s
    
    for k=1:length(listeROI)
        ind = listeROI(k);
        
        pX    = ROI(ind).pX;
        pY    = ROI(ind).pY;
        Num   = ROI(ind).Num(1);
        Color = ROI(ind).Color(1,:);
        Name  = ROI(ind).Name;
        
        RegionOfInterest(k).pX    = pX; %#ok<AGROW>
        RegionOfInterest(k).pY    = pY; %#ok<AGROW>
        RegionOfInterest(k).Num   = Num; %#ok<AGROW>
        RegionOfInterest(k).Color = Color; %#ok<AGROW>
        RegionOfInterest(k).Name  = Name; %#ok<AGROW>
    end
    
    %% Lecture des contours dans le fichier .mat
    
    switch Ext
        case '.xml'
            flag = ROI_export_xml(RegionOfInterest, nomFicMasque);
        case '.mat'
            flag = ROI_export_mat(RegionOfInterest, nomFicMasque);
        case '.shp'
            flag = ROI_export_shp(RegionOfInterest, nomFicMasque);
        case {'.kml', '.kmz'}
            flag = ROI_export_kml(RegionOfInterest, nomFicMasque);
    end   
end
my_close(hw, 'MsgEnd')


function filtre = initNomFic(that, repExport, Prefix, Ext) % TODO : pourrait �tre mutualis� avec ExportErmapper

Titre = extract_ImageName(that);
Titre = rmblank(Titre);
Titre = strrep(Titre, '-', '_');
Titre = strrep(Titre, '+', '_');
Titre = strrep(Titre, '[', '');
Titre = strrep(Titre, ']', '');

if isempty(Prefix)
    filtre = fullfile(repExport, [Titre Ext]);
else
    filtre = fullfile(repExport, [Prefix '_' Titre Ext]);
end

