% Import the Region(s) Of Interest from various file formats into an image
% This function display a file browser to select the file and a list of the ROI(s)
% that will be read, the user can select one, several or all the ROIs
% The expected extensions are .xml, .kml, .kmz, .mat and .shp
%
% Syntax
%   [a, repImport] = ROI_import(a, repImport)
%
% Input Arguments
%   a         : One cl_image instance
%   repImport : Default directory to init the file browser
%
% Output Arguments
%   a         : The updated instance
%   repImport : Name of the directory where the file was found
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%   [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     ROI_plot(a)
%     ROI_plot(a, 'Fig', fig)
%
% See also ROI_export_xml ROI_export_kml ROI_export_mat ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export
%            ROI_import_xml ROI_import_kml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, repImport] = ROI_import(this, repImport)
 
%% Synth�se de tous les contours

this = ROI_summary(this);

%% Nom du fichier .mat contenant les contours

str1 = 'S�lection d''un fichier de R�gion d''Int�r�t.';
str2 = 'Select a file defining a Region Of Interest (ROI)';
[flag, nomFicMasque] = my_uigetfile({'*.xml;*.mat;*.shp;*.cot;*.kml;*.kmz',  'All Files (*.xml,*.mat,*.shp,*.cot,*.kml,*.kmz)';...
                                      '*.xml',  'XML files (*.xml)'; ...
                                      '*.mat','MatLab files (*.mat)'; ...
                                      '*.shp','Shape files (*.shp)'; ...
                                      '*.cot', 'Coeat lines (*.cot)'; ...
                                      '*.kml', 'Google-Earth (*.kml)'; ...
                                      '*.kmz', 'Google-Earth (*.kmz)'}, ...
                                      Lang(str1,str2), repImport);
if ~flag
    return
end
[repImport, ~, Ext] = fileparts(nomFicMasque);

%% Lecture des contours dans le fichier .mat

switch Ext
    case '.xml'
        RegionOfInterest = xml_read(nomFicMasque);
        if isfield(RegionOfInterest, 'ROI') % New
            RegionOfInterest = RegionOfInterest.ROI;
        else % Old
%             RegionOfInterest = RegionOfInterest;
        end
    case '.mat'
        RegionOfInterest = loadmat(nomFicMasque, 'nomVar', 'RegionOfInterest');
    case '.shp'
        [flag, this] = ROI_import_shp(this, nomFicMasque);
        if flag
            RegionOfInterest = get(this, 'RegionOfInterest');
        end
    case '.cot'
        this = import_coastLinesFile(this, nomFicMasque);
    case {'.kml';'.kmz'}
        [flag, this] = import_ROI_kml(this, nomFicMasque);
        if ~flag
            return
        end
end

%% Test si ancien format

for k=1:length(RegionOfInterest)
    if isfield(RegionOfInterest(k), 'nomCourbe')
        RegionOfInterest(k).Name = RegionOfInterest(k).nomCourbe;
    end
    if isfield(RegionOfInterest(k), 'coul')
        RegionOfInterest(k).Color = RegionOfInterest(k).coul;
    end
    if isfield(RegionOfInterest(k), 'code')
        RegionOfInterest(k).Num = RegionOfInterest(k).code;
    end
end
if isfield(RegionOfInterest, 'nomCourbe')
    RegionOfInterest = rmfield(RegionOfInterest, 'nomCourbe');
end
if isfield(RegionOfInterest, 'coul')
    RegionOfInterest = rmfield(RegionOfInterest, 'coul');
end
if isfield(RegionOfInterest, 'code')
    RegionOfInterest = rmfield(RegionOfInterest, 'code');
end

%% Trac� des contours du fichier .mat dans une nouvelle figure

switch Ext
    case {'.xml','.mat'}
        
        Fig = FigUtils.createSScFigure;
        str = sprintf('Region Of Interest stored in\n%s', nomFicMasque);
        title(str, 'interpreter', 'none')
        str = {};
        for k=1:length(RegionOfInterest)
            if isfield(RegionOfInterest(k), 'xy')
                % Pour tenter de r�cup�rer des anciennes versions
                pX = RegionOfInterest(k).xy(1,:);
                pY = RegionOfInterest(k).xy(2,:);
            else
                pX = RegionOfInterest(k).pX;
                pY = RegionOfInterest(k).pY;
            end
            Num = RegionOfInterest(k).Num;
            Color = RegionOfInterest(k).Color;

            %     if isfield(RegionOfInterest(k), 'strLabel') && ~isempty(RegionOfInterest(k).strLabel)
            %         RegionOfInterest(k).strLabel = this.RegionOfInterest(k).strLabel; %#ok<AGROW>
            %     else
            %         RegionOfInterest(k).strLabel = sprintf('%d - Curve #%d', Num, k); %#ok<AGROW>
            %     end
            if ~isfield(RegionOfInterest(k), 'strLabel') || isempty(RegionOfInterest(k).strLabel)
                RegionOfInterest(k).Name = sprintf('%d - Curve #%d', Num, k);
            end
            if ischar(pX)
                pX = str2num(pX); %#ok<ST2NM> % TODO : ne pas remplacer str2num par str2double
                RegionOfInterest(k).pX = pX;
            end
            if ischar(pY)
                pY = str2num(pY); %#ok<ST2NM> % TODO : ne pas remplacer str2num par str2double
                RegionOfInterest(k).pY = pY;
            end
            
            figure(Fig)
            hold on;
            h = PlotUtils.createSScPlot(pX, pY, 'color', Color);
            set(h,'Tag', 'PlotRegionOfInterest');
            str{k} = sprintf('%d - Curve #%d', Num, k); %#ok<AGROW>
            h = text(mean(pX), mean(pY), str{k});
            set(h, 'Color', Color);
        end
        
        %% Saisie des contours � importer
        
        str1 = 'R�gions d''int�r�t � importer';
        str2 = 'ROI to import';
        [rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:length(str), 'Entete', 'IFREMER - SonarScope - ROI import');
        if ~flag
            return
        end
        if isempty(rep)
            return
        end
        
        %% Importaation des contours s�lectionn�s
        
        ROI = get(this, 'RegionOfInterest');
        N = length(ROI);
        for k=1:length(rep)
            ROI(N+k).pX    = RegionOfInterest(rep(k)).pX;
            ROI(N+k).pY    = RegionOfInterest(rep(k)).pY;
            ROI(N+k).Num   = RegionOfInterest(rep(k)).Num;
            ROI(N+k).Color = RegionOfInterest(rep(k)).Color;
            ROI(N+k).Name  = RegionOfInterest(rep(k)).Name;
        end       
        
        this = set(this, 'RegionOfInterest', ROI);
end
