% Import the Region(s) Of Interest from a .kmz or .kml file into an image
%
% Syntax
%   [flag, a] = ROI_import_kml(a, FileName)
%
% Input Arguments
%   a        : One cl_image instance
%   FileName : Name of the Google-Earth file containing the ROI
%
% Output Arguments
%   a : The updated instance
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%   [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     ROI_plot(a)
%     ROI_plot(a, 'Fig', fig)
%
% See also ROI_export_xml ROI_export_kml ROI_export_mat ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = ROI_import_kml(this, FileName)

flag = exist(FileName, 'file');
if ~flag
    return
end

[~, nomFic, ext] = fileparts(FileName);

switch ext
    case '.kmz'
        nomDirTemp = my_tempname;
        [flag, MESSAGE] = mkdir(nomDirTemp);
        if ~flag
            % TODO
            return
        end
        nomFicTemp = fullfile(nomDirTemp, [nomFic '.zip']);
        [flag, MESSAGE] = copyfile(FileName, nomFicTemp);
        if ~flag
            % TODO
            return
        end
        my_unzip(nomFicTemp);
        listeFics = listeFicOnDir(nomDirTemp);
        for k=1:length(listeFics) % En principe il y en a juste un
            [~, nomFicZip] = fileparts(listeFics{k});
            nomFicTemp = fullfile(nomDirTemp, [nomFic '.kml']);
            
            [~, ~, Ext] = fileparts(listeFics{k});
            if strcmpi(Ext, '.kmz')
                movefile(listeFics{k}, nomFicTemp)
            end
            
            [flag, this] = import_ROI_kml_unitaire(this, nomFicTemp);
            if ~flag
                % TODO
            end
            
            [flag, MESSAGE] = rmdir(nomDirTemp, 's');
            if ~flag
                %TODO
            end
        end
        
    case '.kml'
        [flag, this] = import_ROI_kml_unitaire(this, FileName);
        
    otherwise
        % TODO
end


function [flag, this] = import_ROI_kml_unitaire(this, nomFicKml)

tree = xml_read(nomFicKml);

couleurs = [0 0 1; 1 0 0; 0 1 0; 0 0 0; 1 1 0; 1 0 1; 0 1 1];
ROI = get(this, 'RegionOfInterest');
for k1=1:length(tree.Document.Folder)
    for k2=1:length(tree.Document.Folder(k1).Placemark)
        ROI(end+1).pX = tree.Document.Folder(k1).Placemark(k2).Polygon.outerBoundaryIs.LinearRing.coordinates(:,1); %#ok<AGROW>
        ROI(end).pY   = tree.Document.Folder(k1).Placemark(k2).Polygon.outerBoundaryIs.LinearRing.coordinates(:,2);
        
        ROI(end).Num = k1; %shapes(k).Num;
        Name = tree.Document.Folder(k1).Placemark(k2).name;
        ROI(end).Name = Name;
        
        kmod = 1 + mod(k1-1,size(couleurs,1));
        ROI(end).Color   = couleurs(kmod,:);
        %     figure(8756); plot(x, y); grid on; hold on;
    end
end

this = set(this, 'RegionOfInterest', ROI(:));
flag = 1;