% Import the Region(s) Of Interest from a Matlab file into an image
%
% Syntax
%   [flag, a] = ROI_import_mat(a, FileName)
%
% Input Arguments
%   a        : One cl_image instance
%   FileName : Name of the matlab file containing the ROI
%
% Output Arguments
%   a : The updated instance
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%     nomFicROI_mat = getNomFicDatabase('ROI_EM12D_PRISMED.mat');
%   [flag, a] = ROI_import_mat(a, nomFicROI_mat);
%     ROI_plot(a)
%     ROI_plot(a, 'Fig', fig)
%
% See also ROI_export_xml ROI_export_kml ROI_export_mat ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_kml ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = ROI_import_mat(this, nomFic)

if ~exist(nomFic, 'file')
    flag = 0;
    return
end
RegionOfInterest = loadmat(nomFic, 'nomVar', 'RegionOfInterest');

this.RegionOfInterest = [this.RegionOfInterest(:); RegionOfInterest(:)];
flag = 1;