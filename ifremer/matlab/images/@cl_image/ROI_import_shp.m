% Import the Region(s) Of Interest from a Shape file (ESRI) into an image
%
% Syntax
%   [flag, a] = ROI_import_shp(a, FileName)
%
% Input Arguments
%   a        : One cl_image instance
%   FileName : Name of the shape file containing the ROI
%
% Output Arguments
%   a : The updated instance
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%     nomFicROI_shp = getNomFicDatabase('ROI_EM12D_PRISMED.shp');
%   [flag, a] = ROI_import_shp(a, nomFicROI_shp);
%     ROI_plot(a)
%     ROI_plot(a, 'Fig', fig)
%
% See also ROI_export_xml ROI_export_kml ROI_export_mat ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_kml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = ROI_import_shp(this, nomFic)

flag = 0;

%% Lecture du fichier .shp

info = shapeinfo(nomFic);

if ~strcmp(info.ShapeType, 'Polygon')
    str1 = sprintf('Le fichier contient des "%s"\n\nSeuls les polygones sont pris en compte dans SonarScope', info.ShapeType);
    str2 = sprintf('Found %ss\n\nOnly polygons features can be loaded into SonarScope', info.ShapeType);
    my_warndlg(Lang(str1,str2), 1);
end

%% Si aucune zone commune entre BoundingBox et image courante, on sort

shpBoundsX = [min(info.BoundingBox(:,1)), max(info.BoundingBox(:,1)), max(info.BoundingBox(:,1)), min(info.BoundingBox(:,1)), min(info.BoundingBox(:,1))];
shpBoundsY = [min(info.BoundingBox(:,2)), min(info.BoundingBox(:,2)), max(info.BoundingBox(:,2)), max(info.BoundingBox(:,2)), min(info.BoundingBox(:,2))];
imgBoundsX = [this.XLim(1), this.XLim(2), this.XLim(2), this.XLim(1), this.XLim(1)];
imgBoundsY = [this.YLim(1), this.YLim(1), this.YLim(2), this.YLim(2), this.YLim(1)];

xi = polyxpoly(shpBoundsX, shpBoundsY, imgBoundsX, imgBoundsY); % point de croisement polygone/image
ImInPoly = inpolygon(imgBoundsX, imgBoundsY, shpBoundsX, shpBoundsY); % image dans polygon en partie
PolyInIm = inpolygon(shpBoundsX, shpBoundsY, imgBoundsX, imgBoundsY); % polygon dans image en partie
in = ImInPoly | PolyInIm;

if isempty(xi) && ~any(in)
    FigUtils.createSScFigure; PlotUtils.createSScPlot(shpBoundsX, shpBoundsY); grid on; hold on; PlotUtils.createSScPlot(imgBoundsX, imgBoundsY, 'r');
    legend('Polygon bounding box', 'current image bounding box');
    str1 = sprintf('Il n''y a pas de recouvrement entre les deux fichiers !\n\nImpossible de charger le fichier');
    str2 = sprintf('No overlaping between files !\n\nCannot load file.');
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Lecture des contours dans le fichier .shp : mise en correspondance des champs

k = 1;
fieldNames  = {'code', 'nom', 'couleur RVB'; 'code', 'label', 'RGB color'};
attrib2read = {};
attrib      = {info.Attributes(:).Name};
ind         = 1:length(attrib);
attrib{end+1} = Lang('*** AUCUN ***', '*** NONE ***');

while k <= 3
    str1 = sprintf('Selectionner le champ associ� au %s :',       Lang(fieldNames{1,k}, fieldNames{2,k}));
    str2 = sprintf('Select the field name corresponding to %s :', Lang(fieldNames{1,k}, fieldNames{2,k}));
    [rep, flag] = my_listdlg(Lang(str1,str2), attrib, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    
    if rep < length(attrib)
        attrib2read{end+1} = attrib{rep}; %#ok<AGROW>
        ind           = setdiff(ind, rep);
        attrib        = {info.Attributes(ind).Name};
        attrib{end+1} = Lang('*** AUCUN ***', '*** NONE ***');   %#ok<AGROW>
    else
        attrib2read{end+1} = ''; %#ok<AGROW>
    end
    
    k = k+1;
end

%% contr�le de choix des champs

emptyField = cellfun(@isempty, attrib2read);
if all(emptyField)
    str1 = 'Il faut au moins un attribut pour classer les polygones.';
    str2 = 'Need at least one attribute.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

shapes = shaperead(nomFic, ...
    'BoundingBox', [this.XLim(1), this.YLim(1); this.XLim(2), this.YLim(2) ], ...
    'Attributes', attrib2read);
N = length(shapes);

%% Ajout des champs si n�cessaire

if emptyField(1) % Pas de code
    % TODO
end

if emptyField(2) % Pas de label
    % TODO
end

if emptyField(3) % Pas de Color
    uniVal = unique([shapes(:).(attrib2read{1})]);
    colors = rand( numel(uniVal), 3);
    attrib2read{3} = 'color';
    for k2 = 1:length(uniVal)
        ind = ([shapes(:).(attrib2read{1})] == uniVal(k2));
        [shapes(ind).color] = deal(colors(k2,:));
    end
end
% if ~isfield(shapes(k2), 'strLabel') || isempty(shapes(k2).strLabel)
%     shapes(k2).strLabel = sprintf('%d - Curve #%d', Num, k2);
% end


%% Trac� des contours des polygones dans une nouvelle figure

onTrace = false;
if N > 100
    str1 = sprintf('Ce fichier contient %d polygones.\nLes afficher prendra un certain temps...\nVoulez-vous vraiment les afficher ?', N);
    str2 = sprintf('This file contains %d polygons.\nDisplaying them will take a long time...\nIs this what you want ?', N);
    [rep, validation] = my_questdlg(Lang(str1,str2), 'Init', 1);
    if validation && rep == 1
        onTrace = true;
    end
else
    onTrace = true;
end

if onTrace
    Fig = figure;
    [~, nomFicSeul, Ext] = fileparts(nomFic);
    str1 = sprintf('Polygones stock�es dans  "%s"', [nomFicSeul Ext]);
    str2 = sprintf('Polygons stored in "%s"', [nomFicSeul Ext]);
    title(Lang(str1,str2), 'interpreter', 'none')
    
    for k2=1:length(shapes)
        pX  = shapes(k2).X;
        pY  = shapes(k2).Y;
        %     Num = shapes(k2).Num;
        %     Color = shapes(k2).coul;
        
        %     if ~isfield(shapes(k2), 'strLabel') || isempty(shapes(k2).strLabel)
        %         shapes(k2).strLabel = sprintf('%d - Curve #%d', Num, k2);
        %     end
        
        figure(Fig)
        hold on;
        h = plot(pX, pY); grid on;
        set(h, 'Tag', 'PlotRegionOfInterest');
        %     str{k2} = sprintf('%d - Curve #%d', Num, k2); %#ok<AGROW>
        %     h = text(mean(pX), mean(pY), str{k2});
        %     set(h, 'Color', Color);
    end
end

%% Import des r�gions d'inter�t

ROI = get(this, 'RegionOfInterest');
str1 = 'Importation des r�gions d''int�r�t';
str2 = 'Importation Regions Of Interest';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw);
    if ~any(isnan(shapes(k1).X))
        ROI(end+1).pX = shapes(k1).X; %#ok<AGROW>
        ROI(end).pY   = shapes(k1).Y;
        ROI(end).Num  = shapes(k1).(attrib2read{1});
        %         ROI(end).strLabel   = shapes(k1).(attrib2read{2});
        ROI(end).Name  = shapes(k1).(attrib2read{2});
        ROI(end).Color = shapes(k1).(attrib2read{3}); %cell2mat(textscan(shapes(k1).(attrib2read{3}), '%f %f %f'))/255; %shapes(k1).(attrib2read{3});
        %         ROI(end).Name  = shapes(k1).(attrib2read{4});
        %         ROI(end).Comment= shapes(k1).(attrib2read{5});
    else
        indNaN = find(isnan(shapes(k1).X));
        
        ROI(end+1).pX = shapes(k1).X(1:(indNaN(1)-1)); %#ok<AGROW>
        ROI(end).pY   = shapes(k1).Y(1:(indNaN(1)-1));
        
        X = shapes(k1).(attrib2read{1});
        if ischar(X)
            ROI(end).Name = X;
            ROI(end).Num  = length(ROI);
        else
            ROI(end).Name = sprintf('Curve %d', length(ROI));
            ROI(end).Num  = X;
        end
%         if isempty(attrib2read{2})
%             ROI(end).Name = sprintf('Curve%d', length(ROI));
%         else
%             ROI(end).Name = shapes(k1).(attrib2read{2});
%         end
        ROI(end).Color = shapes(k1).(attrib2read{3});%cell2mat(textscan(shapes(k1).(attrib2read{3}), '%f %f %f'))/255; %shapes(k1).(attrib2read{3});
        
        for k2=2:length(indNaN)
            ROI(end+1).pX = shapes(k1).X(indNaN(k2-1)+1 : indNaN(k2)-1); %#ok<AGROW>
            ROI(end).pY   = shapes(k1).Y(indNaN(k2-1)+1 : indNaN(k2)-1);
%             ROI(end).Num  = shapes(k1).(attrib2read{1});
%             %           ROI(end).strLabel  = shapes(k1).(attrib2read{2});
%             if isempty(attrib2read{2})
%                 ROI(end).Name = sprintf('Curve%d', k2);
%             else
%                 ROI(end).Name = shapes(k1).(attrib2read{2});
%             end

            X = shapes(k2).(attrib2read{1});
            if ischar(X)
                ROI(end).Name = X;
                ROI(end).Num  = length(ROI);
            else
                ROI(end).Name = sprintf('Curve%d', length(ROI));
                ROI(end).Num  = X;
            end
        
            ROI(end).Color = shapes(k1).(attrib2read{3});%cell2mat(textscan(shapes(k1).(attrib2read{3}), '%f %f %f'))/255; %shapes(k1).(attrib2read{3});
        end
    end
end
my_close(hw, 'MsgEnd');

%% Traitement sp�cial pour les shapefiles d�finis par Geoffroy pour la mission QUOI : TODO : voir comment int�grer cela dans le d�codage

if (all([ROI(:).Num] == 0)) && (length(ROI(1).Color) > 2) && contains(ROI(1).Color, '-')
    for k=1:length(ROI)
        mots = strsplit(ROI(k).Color, '-');
        ROI(k).Num  = str2double(mots{1});
        ROI(k).Name = mots{2};
    end
    nbColors =  max([ROI(:).Num]);
    Map = jet(nbColors);
    for k=1:length(ROI)
        ROI(k).Color = Map(ROI(k).Num,:);
    end
end

this = set(this, 'RegionOfInterest', ROI);

flag = 1;
