% Interactive delineation of a mask on an image
%
% Syntax
%   [flag, b, a] = ROI_manual(a, createLayer, LayerName, layerMasque, numFig, AxeImage, ...)
%
% Input Arguments
%   a           : One cl_image instance of the image on which one wants to draw countours of a mask
%   createLayer : 1=create an image, 0=use an existing image
%   LayerName   : Name of the Mask
%   layerMasque : [] or a cl_image instance of a preexisting mask to be append
%   numFig      : Figure identifier of the image
%   AxeImage    : Axis handle of the image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : A cl_image instance containing the mask
%   a    : Updated instance of a containing the contours of the mask
%
% Remarks : See SonarScope help for information on the interactive process
%           file:///C:/SonarScopeDoc/DocHtml/acous_sism/sonarscope/Help/region_of_interest1.htm#interactive_delineation
%
% Examples
%     nomFic = getNomFicDatabase('TexturesSonarMontage02.tif');
%     [flag, a] = import(cl_image, nomFic);
%     [numFig, AxeImage] = imagesc(a);
%   [flag, b, a] = ROI_manual(a, 1, 'Mon Masque', [], numFig, AxeImage);
%     imagesc(b, 'CLim', [0 2])
%     ROI_plot(a)
%
% See also cl_image/ROI_manual cl_image/ROI_auto cl_image/ROI_plot cl_image/ROI_export cl_image/ROI_import Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, layerMasque, this] = ROI_manual(this, createLayer, LayerName, layerMasque, numFig, AxeImage, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

masqueAncien = [];
if createLayer
    if length(this.RegionOfInterest) >= 1
        str1 = 'Voulez-vous r�cup�rer les r�gions pr�c�demment d�finies ?';
        str2 = 'Would you like to reuse previously defined ROIs ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
        
        if rep == 1
            ROI_plot_precedent(this);
            subMasque = 1:length(this.RegionOfInterest);
            masqueAncien = masque_precedent(this, subMasque);
        end
    end
else
    ROI_plot_precedent(this);
end

%% Correctif bug point.png R2008a et R2008b

if isdeployed
    MCR_CACHE_ROOT = getenv('MCR_CACHE_ROOT');
    listeDir = listeDirOnDir(MCR_CACHE_ROOT);
    for kDir=1:length(listeDir)
        nomDirCACHE = fullfile(MCR_CACHE_ROOT, listeDir(kDir).name);
        listeDirSonarScope = listeDirOnDir(nomDirCACHE);
        for k=1:length(listeDirSonarScope)
            nomDirCACHESonarScope = fullfile(nomDirCACHE, listeDirSonarScope(k).name);
            nomDirIcons = fullfile(nomDirCACHESonarScope, 'toolbox', 'images', 'icons');
            if exist(nomDirIcons, 'dir')
                nomFicPointPng = getNomFicDatabase('point.png');
                status = copyfile(nomFicPointPng, nomDirIcons);
                if status
                    fprintf('To correct a Matlab bug I have successfully copied "%s" on "%s"\n', ...
                        nomFicPointPng, nomDirIcons);
                else
                    fprintf('To correct a Matlab bug I have tried to copiy "%s" on "%s but the command failed."\n', ...
                        nomFicPointPng, nomDirIcons);
                end
            end
        end
    end
end

%% Trac� du masque

[flag, masque, xy, Num, Color, Name, Comment] = ROI_manual_pickup(this, numFig, AxeImage);
if ~flag
    return
end
if isempty(masque)
    flag = 0;
    return
end

if ~isempty(masqueAncien)
    M = masqueAncien(suby,subx);
    subNon0 = (masque ~= 0);
    M(subNon0) = masque(subNon0);
    masqueAncien(suby,subx) = M;
    subx = 1:length(this.x);
    suby = 1:length(this.y);
    masque = masqueAncien;
end
x = this.x(subx);
y = this.y(suby);

%% Cr�ation du layer "Masque" si besoin

if createLayer
    layerMasque = this;
    
    sz = size(masque);
    M = zeros(sz(1:2), 'single');
    
    layerMasque = replace_Image(layerMasque, M);
    clear M
    layerMasque.ValNaN = NaN;
    
    nbRows      = length(suby);
    nbColumns  = length(subx);
    x_Masque   = this.x(subx);
    y_Masque   = this.y(suby);
    layerMasque.x = x_Masque;
    layerMasque.y = y_Masque;
    
    NuIdentParent = this.NuIdent;
    NuIdent       = floor(rand(1) * 1e8);
else
    nbRows = layerMasque.nbRows;
    nbColumns = layerMasque.nbColumns;
    x_Masque = layerMasque.x;
    y_Masque = layerMasque.y;
    
    NuIdentParent = layerMasque.NuIdentParent;
    NuIdent       = layerMasque.NuIdent;
end

[subx_Masque, suby_Masque] = xy2ij_extrap_outside(layerMasque, x, y);

sub_outOfImage = find(subx_Masque > nbColumns);
if ~isempty(sub_outOfImage)
    N = max(subx_Masque(sub_outOfImage)) - nbColumns;
    X = zeros(size(layerMasque.Image, 1), N, class(layerMasque.Image(1)));
    
    layerMasque = replace_Image(layerMasque, [layerMasque.Image(:,:) X]);
    nbColumns = nbColumns + N;
    x_Masque = linspace(x_Masque(1), x(sub_outOfImage(end)), nbColumns);
end

sub_outOfImage = find(subx_Masque < 1);
if ~isempty(sub_outOfImage)
    N = min(subx_Masque(sub_outOfImage)) - 1;
    N = -N;
    X = zeros(size(layerMasque.Image, 1), N, class(layerMasque.Image(1)));
    
    layerMasque = replace_Image(layerMasque, [X layerMasque.Image(:,:)]);
    subx_Masque = subx_Masque + N;
    nbColumns = nbColumns + N;
    x_Masque = linspace(x(sub_outOfImage(1)), x_Masque(end), nbColumns);
end

sub_outOfImage = find(suby_Masque > nbRows);
if ~isempty(sub_outOfImage)
    N = max(suby_Masque(sub_outOfImage)) - nbRows;
    X = zeros(N, size(layerMasque.Image, 2), class(layerMasque.Image(1)));
    
    layerMasque = replace_Image(layerMasque, [layerMasque.Image(:,:); X]);
    nbRows = nbRows + N;
    y_Masque = linspace(y_Masque(1), y(sub_outOfImage(end)), nbRows);
    layerMasque = sonar_copie_info(this, layerMasque, 1:length(y_Masque), y_Masque); % Je suis pas tr�s sur de moi ici
end

sub_outOfImage = find(suby_Masque < 1);
if ~isempty(sub_outOfImage)
    N = min(suby_Masque(sub_outOfImage)) - 1;
    N = -N;
    X = zeros(N, size(layerMasque.Image, 2), class(layerMasque.Image(1)));
    %     layerMasque.Image = [X; layerMasque.Image];
    
    if isa(layerMasque.Image, 'cl_memmapfile')
        X = single(X);
    end
    
    layerMasque = replace_Image(layerMasque, [X ; layerMasque.Image(:,:)]);
    suby_Masque = suby_Masque + N;
    nbRows = nbRows + N;
    y_Masque = linspace(y(sub_outOfImage(1)), y_Masque(end), nbRows);
    layerMasque = sonar_copie_info(this, layerMasque, 1:length(y_Masque), y_Masque); % Je suis pas tr�s sur de moi ici
end

%% Sauvegarde du resultat dans l'image elle-m�me

this = ROI_union(this);
kROI = length(this.RegionOfInterest);
for k=1:length(xy)
    kROI = kROI+1;
    kLab = Num(k);
    this.RegionOfInterest(kROI).pX = xy{k}(1,:);
    this.RegionOfInterest(kROI).pY = xy{k}(2,:);
    this.RegionOfInterest(kROI).Num = kLab;
    if kLab == 0
        this.RegionOfInterest(kROI).Color = [1 1 1];
    else
        this.RegionOfInterest(kROI).Color = Color(kLab,:);
    end
    this.RegionOfInterest(kROI).Name = Name{kLab};
    this.RegionOfInterest(kROI).Comment = Comment{kLab};
end

%% Cr�ation du masque

M = layerMasque.Image(suby_Masque, subx_Masque);
sub = find(masque ~= 0);
M(sub) = masque(sub); %valMask;
layerMasque.Image(suby_Masque, subx_Masque) = max(M,0);

%% Sauvegarde du resultat dans le masque

if createLayer
    layerMasque.RegionOfInterest = [];
end

kROI = length(layerMasque.RegionOfInterest);
for k=1:length(xy)
    kROI = kROI+1;
    kLab = Num(k);
    layerMasque.RegionOfInterest(kROI).pX      = xy{k}(1,:);
    layerMasque.RegionOfInterest(kROI).pY      = xy{k}(2,:);
    layerMasque.RegionOfInterest(kROI).Num     = kLab;
    layerMasque.RegionOfInterest(kROI).Color   = Color(kLab,:);
    layerMasque.RegionOfInterest(kROI).Name    = Name{kLab};
    layerMasque.RegionOfInterest(kROI).Comment = Comment{kLab};
end

%% Mise � jour de coordonnees

layerMasque.x = x_Masque;
layerMasque.y = y_Masque;

layerMasque = majCoordonnees(layerMasque, 1:length(x_Masque), 1:length(y_Masque));
layerMasque.DataType = cl_image.indDataType('Mask');

%% Calcul des statistiques

layerMasque = compute_stats(layerMasque);

%% Rehaussement de contraste

% layerMasque.TagSynchroContrast = num2str(rand(1));
% CLim = [0 layerMasque.StatValues.Max];
% layerMasque.CLim        = CLim;
% layerMasque.ImageType  = 1;

layerMasque.TagSynchroContrast = num2str(rand(1));
% CLim = [0 layerMasque.StatValues.Max];
CLim = [-0.5 layerMasque.StatValues.Max+0.5];
layerMasque.CLim = CLim;

% map = jet(layerMasque.StatValues.Max);
% map = [0.9 0.9 0.9; layerMasque.RegionOfInterest(1).Color];
% for kROI=2:length(layerMasque.RegionOfInterest)
%     map = [map; layerMasque.RegionOfInterest(kROI).Color]; %#ok<AGROW>
% end



% Attention : modifi� le 15/02/2012 � Wellington pour test segmentation
% avec Arne
% Avant :
%{
if isempty(subMasque)
map = [0.9 0.9 0.9; layerMasque.RegionOfInterest(kROI).Color];
else
map = [0.9 0.9 0.9];
for i=1:length(subMasque)
C = layerMasque.RegionOfInterest(subMasque(i)).Color;
map = [map; C]; %#ok<AGROW>
end
end
%}


% Apr�s :
N = length(Num);
map = ones(1+max(Num),3) * 0.9;
for k=1:N
    map(1+Num(k),:) = Color(Num(k),:);
end

% layerMasque = set(layerMasque, 'ColormapCustom', [0 0 0; map]);
layerMasque.ColormapCustom = map;
layerMasque.ColormapIndex  = 1;
layerMasque.ImageType      = 1;
layerMasque.Video          = 1;
layerMasque.Unit           = '';

% -------------------------------------------------------------------------
% layerMasque.ColormapIndex = 3;

% ATTENTION DANGER : CETTE INSTRUCTION NE
% REMET PAS A JOUR LA TABLE DE COULEUR, IL FAUT IMPERATIVEMENT PASSER PAR
% LA METHOSE set
% -------------------------------------------------------------------------

% layerMasque.ColormapIndex = 3;

%% Compl�tion du titre

if createLayer
    layerMasque = update_Name(layerMasque, 'Append', LayerName);
else
    this.NuIdentParent = NuIdentParent;
    this.NuIdent       = NuIdent;
end



function [flag, masque, xy, Num, Color, Name, Comment] = ROI_manual_pickup(this, numFig, AxeImage)

masque = [];
xy     = [];
Num    = [];
Color  = [];

flag = 1;
color = ['b' 'r' 'k' 'g' 'c' 'm'];
code = 0;
k = 0;
Name = {};
Comment = {};

flagYes = 1;
while flagYes
    disp('roipoly en cours, utilisez la souris pour contourer des regions')
    
    set(0,      'CurrentFigure', numFig)
    set(numFig, 'CurrentAxes',   AxeImage);
    hImage = findobj(AxeImage, 'type', 'image');
    XData = get(hImage, 'XData');
    YData = get(hImage, 'YData');
    XLim = get(AxeImage, 'XLim');
    YLim = get(AxeImage, 'YLim');
    
    subX = ((XData >= XLim(1)) & (XData <= XLim(2)));
    subY = ((YData >= YLim(1)) & (YData <= YLim(2)));
    subx = find((this.x >= XLim(1)) & (this.x <= XLim(2)));
    suby = find((this.y >= YLim(1)) & (this.y <= YLim(2)));
    
    GCF = get(AxeImage, 'Parent');
    while ~strcmp(get(GCF, 'Type'), 'figure')
        GCF = get(GCF, 'Parent');
    end
    
    %     WindowButtonMotionFcn   = get(GCF, 'WindowButtonMotionFcn');
    KeyPressFcn             = get(GCF, 'KeyPressFcn');
    WindowButtonUpFcn       = get(GCF, 'WindowButtonUpFcn');
    WindowButtonDownFcn     = get(GCF, 'WindowButtonDownFcn');
    
    %     set(GCF, 'WindowButtonMotionFcn', '')
    set(GCF, 'KeyPressFcn', '')
    set(GCF, 'WindowButtonUpFcn', '')
    set(GCF, 'WindowButtonDownFcn', '')
    
    [masq, pX, pY] = roipoly;
    pause(1)
    
    %     set(GCF, 'WindowButtonMotionFcn',   WindowButtonMotionFcn)
    set(GCF, 'KeyPressFcn',             KeyPressFcn)
    set(GCF, 'WindowButtonUpFcn',       WindowButtonUpFcn)
    set(GCF, 'WindowButtonDownFcn',     WindowButtonDownFcn)
    
    if isempty(masq)
        [rep, flag] = my_questdlg(Lang('Oups, que voulez-vous faire ?', 'Oups, what do you want to do ?'), ...
            Lang('Supprimer seulement cette r�gion', 'Supress only this region'), Lang('Sortir de la d�finition de r�gion d''int�r�t', 'Go out of the delimitation of the Region Of Interest'));
        if ~flag || (rep == 2)
            flag = 0;
            return
        else
            continue
        end
    end
    
    masq = uint8(masq);
    
    %% Saisie du numero de la region
    
    [flag, code] = inputOneParametre('Define the number of the mask', Lang('Num�ro','Number'), ...
        'Value', code+1, 'MinValue', 1, 'maxvalue', 255, 'Format', '%d');
    if ~flag
        return
    end
    
    if k == 0
        k = k+1;
        masque = masq * uint8(code);
    else
        sub = ((masque .* masq) ~= 0);
        masque = masque + masq * code;
        masque(sub) = code;
    end
    
    %% Trac� du contour sur l'image
    
    Coul = color(1+mod(code-1, length(color)));
    Coul = coul2mat(Coul);
    X = uisetcolor(Coul);
    if length(X) == 3
        Coul = X;
        %     else
        %         if ~isdeployed
        %             'Message for JMA : bizarre : pb colorui � soulever � Mathworks'
        %         end
        % %         masque = [];
        % %         xy = [];
        % %         Num = [];
        % %         Color = [];
        % %         flag = 0;
        % %         return
    end
    
    %% Nom de la courbe
    
    if length(Name) >= code
        strNomCourbe = Name{code};
        strComment = Comment{code};
    else
        strNomCourbe = sprintf('Facies : %d', code);
        strComment = '';
    end
    
    options.Resize = 'on';
    prompt      = {Lang('Nom du facies','Texture name'), Lang('Commentaire','Comment')};
    dlgTitle    = Lang('Analyse de la texture','Texture analysis');
    cmd         = inputdlg(prompt, dlgTitle, [1;10], {strNomCourbe, strComment}, options);
    if isempty(cmd)
        masque = [];
        xy = [];
        Num = [];
        Color = [];
        flag = 0;
        return
    end
    
    
    hold on;
    h = plot(pX, pY, 'Color', Coul);
    set(h,'Tag', 'PlotRegionOfInterest');
    h = text(mean(pX), mean(pY), num2str(code));
    set(h, 'Color', Coul);
    set(h,'Tag', 'PlotRegionOfInterest');
    
    %% Sauvegarde du contour dans un tableau
    
    xy{k}(1,:) = pX; %#ok
    xy{k}(2,:) = pY; %#ok
    Num(k)  = code; %#ok
    %     Color(k,:) = coul2mat(Coul); %#ok
    
    code = max(code,1);% Rajout� le 25/10/2009 dans le cas o� on trace un contour avec label 0
    if code > 0
        Name{code}   = cmd{1}; %#ok
        Comment{code} = cmd{2}; %#ok
        Color(code,:) = Coul; %#ok
    end
    
    k = k + 1;
    
    %% On continue l'operation ?
    
    [choix, flag] = my_questdlg(Lang('Voulez-vous d�finir une autre r�gion d''int�r�t ?', 'Another Region Of Interest ?'));
    if ~flag
        return
    end
    flagYes = (choix ==  1);
end
masque = masque(subY, subX);
masque = stretchmat(masque, [length(suby), length(subx)], 'nearest');


function masque = masque_precedent(this, subMasque)
this = ROI_union(this);
masque = [];
for kSub=1:length(subMasque) %this.RegionOfInterest)
    pX = this.RegionOfInterest(subMasque(kSub)).pX;
    pY = this.RegionOfInterest(subMasque(kSub)).pY;
    code = this.RegionOfInterest(subMasque(kSub)).Num;
    
    masq = roipoly(this.x, this.y, this.Image(:,:,1), pX, pY);
    masq = uint8(masq);
    
    if kSub==1
        masque = masq * uint8(code);
    else
        sub = ((masque .* masq) ~= 0);
        masque = masque + masq * code;
        masque(sub) = code;
    end
end


function ROI_plot_precedent(this)
this = ROI_union(this);
for kROI=1:length(this.RegionOfInterest)
    pX = this.RegionOfInterest(kROI).pX;
    pY = this.RegionOfInterest(kROI).pY;
    code = this.RegionOfInterest(kROI).Num;
    if code == 0
        Color = [1 1 1];
    else
        Color = this.RegionOfInterest(kROI).Color;
        code = min(code,size(code,1));
        Color = Color(code,:);
    end
    hold on;
    h = plot(pX, pY, 'color', Color);
    set(h,'Tag', 'PlotRegionOfInterest');
    h = text(mean(pX), mean(pY), num2str(code));
    set(h, 'Color', Color);
end
