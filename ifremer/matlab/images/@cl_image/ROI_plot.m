% Plot Region(s) Of Interest limits contained in an image
%
% Syntax
%   Fig = ROI_plot(a, ...)
%
% Input Arguments
%   a : One cl_image instance of the image that contains Region Of Interest
%
% Name-Value Pair Arguments
%   Fig : A specific figure handle for the output window
%
% Output Arguments
%   Fig : Figure handle
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%
%     [flag, a] = import(cl_image, nomFic);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%   ROI_plot(a)
%   ROI_plot(a, 'Fig', fig)
%
% See also cl_image/ROI_manual cl_image/ROI_auto cl_image/ROI_plot cl_image/ROI_export cl_image/ROI_import Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Fig = ROI_plot(this, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

this = ROI_union(this);
nbRegionOfInterest = length(this.RegionOfInterest);
if nbRegionOfInterest == 0
    return
end

if nbRegionOfInterest > 10 % Ajout� par JMA le 07/10/2015 car des milliers de ROI sur masque d�duit de DetectionType
    str1 = sprintf('Il y aurait %d r�gions d''int�r�t � tracer sur le graphique de contr�le. Je ne les affiche pas si il y en a plus de 10. Ne vous inqui�tez pas, cela ne change rien � l''op�ration.', nbRegionOfInterest);
    str2 = sprintf('There would be %d regions of interest to plot on the control display. I do not create the figure if there are more than 10 ROI to plot.', nbRegionOfInterest);
    my_warndlg(Lang(str1,str2), 0);
    return
end

if isempty(Fig)
    Fig = FigUtils.createSScFigure;
else
    figure(Fig)
    hold on
end

str1 = sprintf('R�gion d''int�r�t de\nbRegionOfInterest%s', this.Name);
str2 = sprintf('Region Of Interest stored in\nbRegionOfInterest%s', this.Name);
for k=1:nbRegionOfInterest
    pX = this.RegionOfInterest(k).pX;
    pY = this.RegionOfInterest(k).pY;
    
    Num = this.RegionOfInterest(k).Num;
    if Num == 0
        Color = [1 1 1];
    else
        if isempty(this.RegionOfInterest(k).Color)
            Color = [1 1 1];
        else
            Color = this.RegionOfInterest(k).Color(1,:);
        end
    end
    h = PlotUtils.createSScPlot(pX, pY, 'color', Color); grid on;
    title(this.RegionOfInterest(k).Name);
    set(h,'Tag', 'PlotRegionOfInterest');
    hold on;
    
    str = sprintf('%d : %s', Num, this.RegionOfInterest(k).Name);
    h = text(mean(pX), mean(pY), str);
    set(h,'Tag', 'PlotRegionOfInterest');
    set(h, 'Color', Color);
end
strYDir = {'normal'; 'reverse'};
set(gca, 'YDir', strYDir{this.YDir})
title(Lang(str1,str2), 'interpreter', 'none')
