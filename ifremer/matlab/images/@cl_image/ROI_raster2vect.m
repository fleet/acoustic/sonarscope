% Create the Refion(s) Of Interest from a mask
%
% Syntax
%   a = ROI_raster2vect(a, ...)
%
% Input Arguments
%   a : One cl_image instance of a mask
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   a : The updated cl_image instance
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     ROI_plot(a, 'Fig', fig)
%     [c, this] = ROI_vect2raster(a, 1, 'Hi all', []);
%     fig = imagesc(c);
%     ROI_plot(c, 'Fig', fig);
%     c = ROI_delete(c);
%   d = ROI_raster2vect(c);
%     ROI_plot(d)
%
% See also ROI_raster2vect ROI_export ROI_plot Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = ROI_raster2vect(this, varargin)

Fig = [];

N = length(this);
str1 = 'Extraction des contours � partir des masques';
str2 = 'Extracting contours from masks';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [this(k), Fig] = ROI_raster2vect_unitaire(this(k), Fig, varargin{:});
end
my_close(hw, 'MsgEnd')


function [this, Fig] = ROI_raster2vect_unitaire(this, Fig, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Test si on est sur un masque

identMask          = cl_image.indDataType('Mask');
identSegmentation  = cl_image.indDataType('Segmentation');
flag = testSignature(this, 'DataType', [identMask identSegmentation]);
if  ~flag
    return
end

%% Synth�se de tous les contours

this = ROI_summary(this);

%% D�tection des contours

RegionOfInterest = get_detectionContours(this, 'subx', subx, 'suby', suby);
M = length(RegionOfInterest);

%% Importation des contours s�lectionn�s

ROI = this.RegionOfInterest;
N = length(ROI);
% TODO : y'aurait-y pas du clair � mettre entre strLabel et nomCourbe, etc ?
for k=1:M
    ROI(N+k).pX      = RegionOfInterest(k).pX;
    ROI(N+k).pY      = RegionOfInterest(k).pY;
    ROI(N+k).labels  = RegionOfInterest(k).code;
    ROI(N+k).couleur = RegionOfInterest(k).coul;
    %     ROI(N+k).strLabel    = RegionOfInterest(k).strLabel;
    
    ROI(N+k).nomCourbe   = RegionOfInterest(k).nomCourbe; % Modif
    ROI(N+k).Commentaire = [];
end
this.RegionOfInterest = ROI;

Fig = ROI_plot(this, 'Fig', Fig);


function RegionOfInterest = get_detectionContours(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% color = ['b' 'r' 'k' 'g' 'c' 'm'];

I = this.Image(suby,subx);

I(isnan(I)) = 0;
BW = bwperim(I, 8);
[B,~,N] = bwboundaries(BW);
B = B((N+1):end);

for k=1:length(B)
    %     coul = coul2mat(color(1+mod(k-1, length(color))));
    boundary = B{k}';
    %{
% Avant 04/02/2015
RegionOfInterest(k).xy{1} = boundary; %#ok<AGROW>
RegionOfInterest(k).xy{1}(1,:) = this.x(subx(boundary(2,:))); %#ok<AGROW>
RegionOfInterest(k).xy{1}(2,:) = this.y(suby(boundary(1,:))); %#ok<AGROW>
    %}
    
    % Apr�s 04/02/2015
    RegionOfInterest(k).pX = this.x(subx(boundary(2,:))); %#ok<AGROW>
    RegionOfInterest(k).pY = this.y(suby(boundary(1,:))); %#ok<AGROW>
    RegionOfInterest(k).code = k; %#ok<AGROW>
    
    % Test
    Val = I(boundary(1,1), boundary(2,1));
    coul = get_coul(this, Val);
    
    RegionOfInterest(k).coul = coul; %#ok<AGROW>
    %     RegionOfInterest(k).strLabel =  sprintf('%d - Curve #%d', k, k); %#ok<AGROW>
    RegionOfInterest(k).nomCourbe =  sprintf('%d - Curve #%d', k, k); %#ok<AGROW>
end


