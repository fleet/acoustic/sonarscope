% Rename the Regions Of Interest contained in an image
%
% Syntax
%   ROI_rename(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   a : The updated cl_image instance
%
% Examples
%     nomFic = getNomFicDatabase('TexturesSonarMontage02.tif');
%     [flag, a] = import(cl_image, nomFic);
%     [numFig, AxeImage] = imagesc(a);
%     [flag, b, a] = ROI_manual(a, 1, 'Mon Masque', [], numFig, AxeImage);
%     imagesc(b, 'CLim', [0 2])
%     Fig = ROI_plot(b);
%   b = ROI_rename(b);
%     Fig = ROI_plot(b);
%
% See also cl_image/ROI_manual cl_image/ROI_auto cl_image/ROI_plot cl_image/ROI_export cl_image/ROI_import
%            cl_image/delete Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = ROI_rename(this)


%% Synth�se de tous les contours

[this, str] = ROI_summary(this); %#ok<ASGLU>

%% Lecture des r�gions d'int�r�t contenues dans l'image courante

RegionOfInterest = get(this, 'RegionOfInterest');
if isempty(RegionOfInterest)
    my_warndlg('No Region Of Interest in this image', 1);
    return
end

%% Affichage de tous les contours dans une nouvelle figure

ROI_plot(this);

%% Extraction des contours s�lectionn�s

for k=1:length(RegionOfInterest)
    Num(k)     = RegionOfInterest(k).Num(1); %#ok<AGROW>
    Name{k} = RegionOfInterest(k).Name; %#ok<AGROW>
    
    NomVar{k} = sprintf('Contour %d &  Label', k); %#ok<AGROW>
    Value{k}  = sprintf('%d       "%s"', Num(k), Name{k}); %#ok<AGROW>
end
[rep, flag] = my_inputdlg(NomVar, Value, 'Entete', 'IFREMER - SonarScope : Give new numbers & labels');
if ~flag
    return
end

color = ['b' 'r' 'k' 'g' 'c' 'm'];
for k=1:length(RegionOfInterest)
    str = strtrim(rep{k});
    pppp = strfind(str, ' ');
    if isempty(pppp)
        Num = str2double(str);
        RegionOfInterest(k).Num = Num;
        RegionOfInterest(k).Name = '';
    else
        Num = str2double(str(1:pppp));
        RegionOfInterest(k).Num = Num;
        str = strtrim(str(pppp+1:end));
        str = strrep(str, '"', '');
        RegionOfInterest(k).Name = str;
    end
     RegionOfInterest(k).Color = color(1+mod(Num-1, length(color)));
end

for k1=1:length(RegionOfInterest)
    if isempty(RegionOfInterest(k1).Name)
        for k2=1:length(RegionOfInterest)
            if RegionOfInterest(k1).Num == RegionOfInterest(k2).Num && ...
                    ~isempty(RegionOfInterest(k2).Name)
                RegionOfInterest(k1).Name = RegionOfInterest(k2).Name;
            end
        end
    end
end

this = set(this, 'RegionOfInterest', RegionOfInterest);
