% Give a list of the RegionsOf Interest and converts old fashon ROI to the
% current one
%
% Syntax
%   [a, str] = ROI_summary(a)
%
% Input Arguments
%   a   : One cl_image instance
%   str : List of the ROIs
%
% Output Arguments
%   a  : The updated instance  
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     ROI_plot(a)
%     ROI_plot(a, 'Fig', fig)
%   [a, str] = ROI_summary(a);
%     str
%
% See also ROI_import_xml Authors
%    (class) ROI_manual ROI_auto ROI_plot
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, str] = ROI_summary(this)

this = ROI_union(this);

str = [];
RegionOfInterest = [];
ind = 1;
for k=1:length(this.RegionOfInterest)
    if isfield(this.RegionOfInterest(k), 'xy')
        RegionOfInterest(ind).xy = this.RegionOfInterest(k).xy; %#ok<AGROW>
    else
        RegionOfInterest(ind).pX = this.RegionOfInterest(k).pX; %#ok<AGROW>
        RegionOfInterest(ind).pY = this.RegionOfInterest(k).pY; %#ok<AGROW>
    end
    Num  = this.RegionOfInterest(k).Num;
    RegionOfInterest(ind).Num  = Num; %#ok<AGROW>
    if Num == 0
        RegionOfInterest(ind).Color = [0 0 0]; %#ok<AGROW>
    else
        %             RegionOfInterest(ind).Color = this.RegionOfInterest(k).Color(Num,:); %#ok<AGROW>
        RegionOfInterest(ind).Color = this.RegionOfInterest(k).Color; %#ok<AGROW>
    end
    if isfield( this.RegionOfInterest(k), 'strLabel') && ~isempty(this.RegionOfInterest(k).strLabel)
        RegionOfInterest(ind).Name = this.RegionOfInterest(k).strLabel; %#ok<AGROW>
    else
        RegionOfInterest(ind).Name = sprintf('%d - Curve #%d', Num, ind); %#ok<AGROW>
    end
    
    str{ind,1} = sprintf('%d : %s', Num, RegionOfInterest(ind).Name); %#ok<AGROW>
    ind = ind + 1;
end
this.RegionOfInterest = RegionOfInterest;
