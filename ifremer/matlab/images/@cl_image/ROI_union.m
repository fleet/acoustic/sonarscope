% Theoriquement plus utile

function this = ROI_union(this)

% TODO : il faut absolument supprimer ces doubles fa�ons de cr�er des ROI
k = 0;
ROI = [];

for j=1:length(this.RegionOfInterest)
    
    if isfield(this.RegionOfInterest(j), 'Num') % New
        Num = this.RegionOfInterest(j).Num;
    else % Old
        Num = this.RegionOfInterest(j).labels;
    end
    k = k + 1;
    
    if isfield(this.RegionOfInterest(j), 'pX') % New
        ROI(k).pX    = this.RegionOfInterest(j).pX; %#ok<AGROW>
        ROI(k).pY    = this.RegionOfInterest(j).pY; %#ok<AGROW>
    else % Old
        ROI(k).pX    = this.RegionOfInterest(j).xy(1,:); %#ok<AGROW>
        ROI(k).pY    = this.RegionOfInterest(j).xy(2,:); %#ok<AGROW>
    end
    ROI(k).Num   = Num; %#ok<AGROW>
    
    if isfield(this.RegionOfInterest(j), 'Color') % New
        ROI(k).Color = this.RegionOfInterest(j).Color; %#ok<AGROW>
    else % Old
        ROI(k).Color = this.RegionOfInterest(j).couleur; %#ok<AGROW>
    end
    
    if isfield(this.RegionOfInterest(j), 'Name')
        if iscell(this.RegionOfInterest(j).Name)
            ROI(k).Name    = this.RegionOfInterest(j).Name{Num}; %#ok<AGROW>
            if isfield(this.RegionOfInterest(j), 'Comment')
                ROI(k).Comment = this.RegionOfInterest(j).Comment{Num}; %#ok<AGROW>
            else
                ROI(k).Comment = ''; %#ok<AGROW>
            end
        else
            ROI(k).Name = this.RegionOfInterest(j).Name; %#ok<AGROW>
            if isfield(this.RegionOfInterest(j), 'Comment')
                ROI(k).Comment = this.RegionOfInterest(j).Comment; %#ok<AGROW>
            else
                ROI(k).Comment = ''; %#ok<AGROW>
            end
        end
    else % Old
        if iscell(this.RegionOfInterest(j).nomCourbe)
            ROI(k).Name = this.RegionOfInterest(j).nomCourbe{Num}; %#ok<AGROW>
            if isfield(this.RegionOfInterest(j), 'Comment')
                ROI(k).Comment = this.RegionOfInterest(j).Comment; %#ok<AGROW>
            else
                ROI(k).Comment = ''; %#ok<AGROW>
            end
        else
            ROI(k).Name    = this.RegionOfInterest(j).nomCourbe; %#ok<AGROW>
            if isfield(this.RegionOfInterest(j), 'Comment')
                ROI(k).Comment  = this.RegionOfInterest(j).Comment; %#ok<AGROW>
            else
                ROI(k).Comment  = ''; %#ok<AGROW>
            end
        end
    end
end
this.RegionOfInterest = ROI;