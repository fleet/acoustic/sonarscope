% Create or append a Refion Of Interest
%
% Syntax
%   [b, a] = ROI_vect2raster(a, creationLayer, nomLayer, layerMasque, ...)
%
% Input Arguments
%   a             : One cl_image instance
%   creationLayer : 1=creation a mask layer, 0=use an existing mask layer
%   nomLayer      :
%   layerMasque   :
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : One instance of cl_image containing the mask
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     fig = imagesc(a);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     ROI_plot(a, 'Fig', fig)
%   [c, this] = ROI_vect2raster(a, 1, 'Hi all', []);
%     fig = imagesc(c);
%     ROI_plot(c, 'Fig', fig);
%
% See also ROI_raster2vect ROI_import ROI_plot Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [layerMasque, this] = ROI_vect2raster(this, creationLayer, nomLayer, layerMasque, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

[masque, Labels, Colors] = contours2mask(this);
masque = masque(suby,subx);
x = this.x(subx);
y = this.y(suby);

%% Cr�ation du layer "Masque" si besoin

if creationLayer
    layerMasque = this;
    
    %     sz = size(this.Image);
    sz = size(masque);
    M = zeros(sz(1:2), 'single');
    
    layerMasque = replace_Image(layerMasque, M);
    clear M
    layerMasque.ValNaN = NaN;
    
    nbRows    = length(suby);
    nbColumns = length(subx);
    x_Masque = this.x(subx);
    y_Masque = this.y(suby);
    layerMasque.x = x_Masque;
    layerMasque.y = y_Masque;
    
    NuIdentParent = this.NuIdent;
    NuIdent       = floor(rand(1) * 1e8);
else
    nbRows = layerMasque.nbRows;
    nbColumns = layerMasque.nbColumns;
    x_Masque = layerMasque.x;
    y_Masque = layerMasque.y;
    
    NuIdentParent = layerMasque.NuIdentParent;
    NuIdent       = layerMasque.NuIdent;
end

[subx_Masque, suby_Masque] = xy2ij_extrap_outside(layerMasque, x, y);

sub_outOfImage = find(subx_Masque > nbColumns);
if ~isempty(sub_outOfImage)
    N = max(subx_Masque(sub_outOfImage)) - nbColumns;
    X = zeros(size(layerMasque.Image, 1), N, class(layerMasque.Image(1))); % 'like' Non compatible avec la R2011b
    
    layerMasque = replace_Image(layerMasque, [layerMasque.Image(:,:) X]);
    nbColumns = nbColumns + N;
    x_Masque = linspace(x_Masque(1), x(sub_outOfImage(end)), nbColumns);
end

sub_outOfImage = find(subx_Masque < 1);
if ~isempty(sub_outOfImage)
    N = min(subx_Masque(sub_outOfImage)) - 1;
    N = -N;
    X = zeros(size(layerMasque.Image, 1), N, class(layerMasque.Image(1))); % 'like' Non compatible avec la R2011b
    
    layerMasque = replace_Image(layerMasque, [X layerMasque.Image(:,:)]);
    subx_Masque = subx_Masque + N;
    nbColumns = nbColumns + N;
    x_Masque = linspace(x(sub_outOfImage(1)), x_Masque(end), nbColumns);
end

sub_outOfImage = find(suby_Masque > nbRows);
if ~isempty(sub_outOfImage)
    N = max(suby_Masque(sub_outOfImage)) - nbRows;
    X = zeros(N, size(layerMasque.Image, 2), class(layerMasque.Image(1))); % 'like' Non compatible avec la R2011b
    
    layerMasque = replace_Image(layerMasque, [layerMasque.Image(:,:); X]);
    nbRows = nbRows + N;
    y_Masque = linspace(y_Masque(1), y(sub_outOfImage(end)), nbRows);
    
    layerMasque = sonar_copie_info(this, layerMasque, 1:length(y_Masque), y_Masque);
end

sub_outOfImage = find(suby_Masque < 1);
if ~isempty(sub_outOfImage)
    N = min(suby_Masque(sub_outOfImage)) - 1;
    N = -N;
    X = zeros(N, size(layerMasque.Image, 2), class(layerMasque.Image(1))); % 'like' Non compatible avec la R2011b
    
    if isa(layerMasque.Image, 'cl_memmapfile')
        X = single(X);
    end
    
    layerMasque = replace_Image(layerMasque, [X ; layerMasque.Image(:,:)]);
    suby_Masque = suby_Masque + N;
    nbRows = nbRows + N;
    y_Masque = linspace(y(sub_outOfImage(1)), y_Masque(end), nbRows);
    
    layerMasque = sonar_copie_info(this, layerMasque, 1:length(y_Masque), y_Masque); % Je suis pas tr�s sur de moi ici
end

%% Cr�ation du masque

M = layerMasque.Image(suby_Masque, subx_Masque);
% sub = find(masque ~= 0);
sub = (masque ~= 0);
M(sub) = masque(sub); %valMask;
layerMasque.Image(suby_Masque, subx_Masque) = max(M,0);

%% Mise � jour de coordonn�es

layerMasque.x = x_Masque;
layerMasque.y = y_Masque;

layerMasque = majCoordonnees(layerMasque, 1:length(x_Masque), 1:length(y_Masque));
layerMasque.DataType = cl_image.indDataType('Mask');

%% Calcul des statistiques

layerMasque = compute_stats(layerMasque);

%% Rehaussement de contraste

layerMasque.TagSynchroContrast = num2str(rand(1));
% CLim = [0 layerMasque.StatValues.Max];
CLim = [-0.5 layerMasque.StatValues.Max+0.5];

layerMasque.CLim  = CLim;

N = length(Labels);
map = ones(1+max(Labels),3) * 0.9;
for k=1:N
    map(1+Labels(k),:) = Colors(Labels(k),:);
end
layerMasque.ColormapCustom = map;
layerMasque.ColormapIndex  = 1;
layerMasque.ImageType      = 1;
layerMasque.Video          = 1;

%% Compl�tion du titre

if creationLayer
    layerMasque = update_Name(layerMasque, 'Append', nomLayer);
else
    this.NuIdentParent = NuIdentParent;
    this.NuIdent       = NuIdent;
end


function [masque, Labels, Colors] = contours2mask(this)

offset = 0;
nROI = length(this.RegionOfInterest);
for k=1:nROI
    Num = this.RegionOfInterest(k).Num;
    if Num == 0
        offset = 1;
    end
end

masque = [];
Labels = [];
Colors = [];
str1 = 'Cr�ation du masque...';
str2 = 'Mask creation ...';
hw = create_waitbar(Lang(str1,str2), 'N', nROI);
for k=1:nROI
    my_waitbar(k, nROI, hw);
    pX  = this.RegionOfInterest(k).pX;
    pY  = this.RegionOfInterest(k).pY;
    Num = this.RegionOfInterest(k).Num + offset;
    C = this.RegionOfInterest(k).Color;
    if ischar(C)
        C = coul2mat(C);
        if isempty(C)
            C = coul2mat(k);
        end
    end
    Colors(k,:) = C; %#ok<AGROW>
    
    subNonNaN = ~isnan(pX) & ~isnan(pY);
    masq = roipoly(this.x, this.y, this.Image(:,:,1), pX(subNonNaN), pY(subNonNaN));
    % Correction pour PLACA3D : comblement du gap entre deux plaques car le
    % Roipoly ne prend pas en compte la donn�e int�rieure
    masq = imdilate(masq, [1 1 1; 1 1 1; 1 1 1]); % [0 1 0; 1 1 1; 0 1 0]);
    masq = uint8(masq);
    
    if k == 1
        masque = masq * uint8(Num);
    else
        sub = ((masque .* masq) ~= 0);
        masque = masque + masq * Num;
        masque(sub) = Num;
    end
    Labels(k) = Num; %#ok<AGROW>
end
my_close(hw);
