% Put image in virtual memory
%
% Syntax
%   a = Ram2Virtualmemory(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   LogSilence : 1=message in the console, 0=no message (Default : 0)
%
% Output Arguments
%   a : Instance(s) of cl_image
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%     [str, MemMapFileName] = where(a)
%   a = Ram2Virtualmemory(a);
%     [str, MemMapFileName] = where(a)
%     a = Virtualmemory2Ram(a);
%     [str, MemMapFileName] = where(a)
%
% See also cl_image/where cl_image/Virtualmemory2Ram cl_image/optimiseMemory cl_memmapfile Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function this = Ram2Virtualmemory(this, varargin)

N = length(this);
hw = create_waitbar(waitbarMsg('Ram2Virtualmemory'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    this(k) = Ram2Virtualmemory_unit(this(k), varargin{:});
end
my_close(hw, 'MsgEnd')


function this = Ram2Virtualmemory_unit(this, varargin)

if isa(this.Image, 'cl_memmapfile')
    return
end

[varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 0); %#ok<ASGLU>

nomFicMem = my_tempname('.memmapfile');

if ~LogSilence
    str = sprintf('Downloading file : "%s" in virtual memory on file "%s"', this.Name, nomFicMem);
    fprintf('%s\n', str);
end

if size(this.Image,1) == size(this.Image,2)
    return % bidouille JMA pour �viter transposition matrices carr�es Ex import G:\FSK\Course\XYZ
end

this.Image = cl_memmapfile('Value', this.Image, 'FileName', nomFicMem, 'LogSilence', LogSilence);
