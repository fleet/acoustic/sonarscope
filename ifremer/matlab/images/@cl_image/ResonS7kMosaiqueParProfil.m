function [flag, Mosaiques] = ResonS7kMosaiqueParProfil(this, indImage, listeFicAll, ModeDependant, LayerName, ...
    Window, SameReflectivityStatus, InfoCompensationCurve, InfoMos, varargin)

[varargin, Carto]              = getPropertyValue(varargin, 'Carto',          []);
[varargin, IndNavUnit]         = getPropertyValue(varargin, 'IndNavUnit',     []);
[varargin, MaskLatLong]        = getPropertyValue(varargin, 'MaskLatLong',    []);
[varargin, Mute]               = getPropertyValue(varargin, 'Mute',           0);
[varargin, flagTideCorrection] = getPropertyValue(varargin, 'TideCorrection', 0); %#ok<ASGLU>

flag      = 0;
Mosaiques = [];

useParallel = get_UseParallelTbx;
logFileId   = getLogFileId;

resol = InfoMos.gridSize;

dosNetUse('Init')

if isempty(listeFicAll)
    return
end

if ischar(listeFicAll)
    listeFicAll = {listeFicAll};
end

I0 = cl_image_I0;
Selection = [];

%% Recherche des layers synchronis�s

if SameReflectivityStatus && (this(indImage).DataType == cl_image.indDataType('Reflectivity'))
    [flag, ~, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
    if ~flag
        return
    end
else
    nomsLayers{1} = LayerName;
    DataTypes = ALL.identSonarDataType(LayerName);
    
    DataTypes(2)  = cl_image.indDataType('AcrossDist');
    nomsLayers{2} = 'AcrossDist';
    DataTypes(3)  = cl_image.indDataType('AlongDist');
    nomsLayers{3} = 'AlongDist';
    %     DataTypes(4)  = cl_image.indDataType('TxAngle');
    %     nomsLayers{4} = 'TxAngle';
    DataTypes(4)  = cl_image.indDataType('BeamPointingAngle'); % modif JMA le 22/03/2016 : non valid� si EM3002
    nomsLayers{4} = 'BeamPointingAngle';
    DataTypes(5)  = cl_image.indDataType('RxAngleEarth'); % modif JMA le 22/03/2016 : non valid� si EM3002
    nomsLayers{5} = 'RxAngleEarth';
end

%% Lecture des fichiers de compensation si il y en a

if isempty(InfoCompensationCurve) || isempty(InfoCompensationCurve.FileName) ...
        || (iscell(InfoCompensationCurve.FileName) && isempty(InfoCompensationCurve.FileName{1}))
    bilan = [];
else
    FileNameCompensation = InfoCompensationCurve.FileName;
    if ischar(FileNameCompensation)
        FileNameCompensation = {FileNameCompensation};
    end
    ModeDependant = InfoCompensationCurve.ModeDependant;
    
    for k2=1:length(FileNameCompensation)
        CourbeConditionnelle = load_courbesStats(FileNameCompensation{k2});
        if isempty(CourbeConditionnelle)
            %             flag = 0;
            return
        end
        bilan{k2} = CourbeConditionnelle.bilan; %#ok<AGROW>
        % Selection = % TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    end
end

%% R�cup�ration �ventuelle des fichiers mosa�que si reprise d'une mosa�que existante

if InfoMos.TypeMos == 1
    if InfoMos.Unique.CompleteExistingMosaic
        [flag, Z_Mosa_All] = cl_image.import_ermapper(InfoMos.Unique.nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        [flag, A_Mosa_All]  = cl_image.import_ermapper(InfoMos.Unique.nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Lecture du fichier de d�coupe horaire

if isempty(InfoMos.Import.SlotFilename)
    SonarTime = [];
else
    SonarTime = lecFicDecCar(InfoMos.Import.SlotFilename);
end

%% Traitement des fichiers

LastSavedIFic = 0;
N = length(listeFicAll);
NoWaitbar = N > 1;
if InfoMos.TypeMos == 1
    str1 = 'Calcul de la mosa�que';
    str2 = 'Computing the mosaic';
else
    str1 = 'Calcul des mosa�ques individuelles';
    str2 = 'Computing individual mosaics';
end

if useParallel && (InfoMos.TypeMos == 2) && (N > 3)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    iFic = 1; % premier passage en mode normal pour r�cup�rer certaines variables
    nomFic = listeFicAll{iFic};
    msg = sprintf('%d / %d : %s', iFic, N, nomFic);
    logFileId.info('ResonS7kMosaiqueParProfil', msg);
    
    this(indImage) = Virtualmemory2Ram(this(indImage));
    [~, ~, Selection, Carto, ~, nomDataType] = ResonS7kMosaiqueParProfil_unitaire(this, nomFic, iFic, ...
        InfoMos, Z_Mosa_All, A_Mosa_All, indImage, nomsLayers, DataTypes, SonarTime, resol, Window, ...
        SameReflectivityStatus, InfoCompensationCurve, ModeDependant, bilan, ...
        Selection, Carto, NoWaitbar, LastSavedIFic, flagTideCorrection, IndNavUnit, MaskLatLong, Mute);
    logFileIdHere = logFileId;
    parfor (iFic=2:N, useParallel)
        nomFic = listeFicAll{iFic};
%         fprintf('\n%d / %d : %s', iFic, N,  nomFic);
        msg = sprintf('%d / %d : %s', iFic, N, nomFic);
        logFileId.info('ResonS7kMosaiqueParProfil', msg); %#ok<PFBNS>

        ResonS7kMosaiqueParProfil_unitaire(this, nomFic, iFic, ...
            InfoMos, Z_Mosa_All, A_Mosa_All, indImage, nomsLayers, DataTypes, SonarTime, resol, Window, ...
            SameReflectivityStatus, InfoCompensationCurve, ModeDependant, bilan, ...
            Selection, Carto, NoWaitbar, LastSavedIFic, flagTideCorrection, IndNavUnit, MaskLatLong, Mute, ...
            'nomDataType', nomDataType, 'logFileId', logFileIdHere);
        send(DQ, iFic);
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60);
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for iFic=1:N
        my_waitbar(iFic, N, hw)
        nomFic = listeFicAll{iFic};
%         fprintf('\n%d / %d : %s', iFic, N,  nomFic);
        msg = sprintf('%d / %d : %s', iFic, N, nomFic);
        logFileId.info('ResonS7kMosaiqueParProfil', msg);

        [Z_Mosa_All, A_Mosa_All, Selection, Carto, LastSavedIFic] = ResonS7kMosaiqueParProfil_unitaire(this, nomFic, iFic, ...
            InfoMos, Z_Mosa_All, A_Mosa_All, indImage, nomsLayers, DataTypes, SonarTime, resol, Window, ...
            SameReflectivityStatus, InfoCompensationCurve, ModeDependant, bilan, ...
            Selection, Carto, NoWaitbar, LastSavedIFic, flagTideCorrection, IndNavUnit, MaskLatLong, Mute);
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60);
end
    
%% Sauvegarde de la derni�re mouture

if (InfoMos.TypeMos == 1) && (LastSavedIFic ~= N)
    Z_Mosa_All = majCoordonnees(Z_Mosa_All);
    StatValues = Z_Mosa_All.StatValues;
    CLim = StatValues.Quant_25_75;
    export_ermapper(Z_Mosa_All, InfoMos.Unique.nomFicErMapperLayer, 'CLim', CLim);
    
    A_Mosa_All = majCoordonnees(A_Mosa_All);
    export_ermapper(A_Mosa_All, InfoMos.Unique.nomFicErMapperAngle);
end

%% Affichage de l'image si mosa�que unique

if InfoMos.TypeMos == 1
    Mosaiques = [Z_Mosa_All  A_Mosa_All];
end

dosNetUse('Clear')
flag = 1;



function [Z_Mosa_All, A_Mosa_All, Selection, Carto, LastSavedIFic, nomDataType] = ResonS7kMosaiqueParProfil_unitaire(this, nomFic, iFic, ...
    InfoMos, Z_Mosa_All, A_Mosa_All, ...
    indImage, nomsLayers, DataTypes, SonarTime, resol, Window, ...
    SameReflectivityStatus, InfoCompensationCurve, ModeDependant, bilan, ...
    Selection, Carto, NoWaitbar, LastSavedIFic, flagTideCorrection, IndNavUnit, MaskLatLong, Mute, varargin)

global isUsingParfor %#ok<GVMIS>
global logFileId %#ok<GVMIS>

[varargin, nomDataType]   = getPropertyValue(varargin, 'nomDataType',   []);
[varargin, logFileId]     = getPropertyValue(varargin, 'logFileId',     logFileId);
[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', 0);  %#ok<ASGLU>

[~, nomFicSeul] = fileparts(nomFic);

if ~exist(nomFic, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('"%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierAllNExistePAs');
    return
end

%% Test si mosaique individuelle d�j� r�alis�e

if (InfoMos.TypeMos == 2) && InfoMos.skipExistingMosaic && ~isempty(nomDataType)
    nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_' nomDataType '_LatLong.ers']);
    if exist(nomFicErs, 'file')
        return
    end
end

%% Cr�ation de l'instance sonar

aKM = cl_simrad_all('nomFic', nomFic);
if isempty(aKM)
    return
end

%% get layers processed as the current image

if SameReflectivityStatus
    PreserveMeanValueIfModel = InfoCompensationCurve.PreserveMeanValueIfModel;
    [flag, Layers_PingBeam, indImageLayer, Selection, Carto] = get_layersProcessedAsThis(aKM, this, indImage, nomsLayers, DataTypes, ...
        bilan, ModeDependant, Carto, Selection, SameReflectivityStatus, InfoCompensationCurve.UseModel, ...
        'PreserveMeanValueIfModel', PreserveMeanValueIfModel, 'TideCorrection', flagTideCorrection, ...
        'MasqueActif', InfoMos.Import.MasqueActif, 'IndNavUnit', IndNavUnit);
else
    if isempty(InfoCompensationCurve)
        PreserveMeanValueIfModel = 0;
        UseModel                 = 0;
    else
        PreserveMeanValueIfModel = InfoCompensationCurve.PreserveMeanValueIfModel;
        UseModel                 = InfoCompensationCurve.UseModel;
    end
    [flag, Layers_PingBeam, indImageLayer, Selection, Carto] = get_layers(aKM, nomsLayers, DataTypes(1), DataTypes, ...
        bilan, ModeDependant, Carto, Selection, UseModel, ...
        'PreserveMeanValueIfModel', PreserveMeanValueIfModel, 'TideCorrection', flagTideCorrection, ...
        'MasqueActif', InfoMos.Import.MasqueActif, 'IndNavUnit', IndNavUnit, 'Mute', Mute);
end

if ~flag
    return
end
if isempty(indImageLayer) % Cas o� on n'est pas pass� par une image d'exemple
    indImageLayer = length(Layers_PingBeam);
end
nomDataType = cl_image.strDataType{Layers_PingBeam(indImageLayer).DataType};

%% Test si mosaique individuelle d�j� r�alis�e

if (InfoMos.TypeMos == 2) && InfoMos.skipExistingMosaic % Cas pr�vu pour le premier passage dans la fonction
    nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_' nomDataType '_LatLong.ers']);
    if exist(nomFicErs, 'file')
        return
    end
end

%% Recherche du layer d'angle

for k=1:length(Layers_PingBeam)
    DT(k) = Layers_PingBeam(k).DataType; %#ok<AGROW>
end

%     indImageAngle = find(...
%         (DT == cl_image.indDataType('RxAngleEarth')) | ...
%         (DT == cl_image.indDataType('RxBeamAngle')) | ...
%         (DT == cl_image.indDataType('TxAngle')) | ...
%         (DT == cl_image.indDataType('BeamPointingAngle')));
indImageAngle = find(DT == cl_image.indDataType('RxAngleEarth'));
if isempty(indImageAngle)
    indImageAngle = find(DT == cl_image.indDataType('RxBeamAngle'));
end
if isempty(indImageAngle)
    indImageAngle = find(DT == cl_image.indDataType('TxAngle'));
end
if isempty(indImageAngle)
    indImageAngle = find(DT == cl_image.indDataType('BeamPointingAngle'));
end

%% Recherche des pings dans la plage horaire

if ~isempty(InfoMos.Import.SlotFilename)
    layerMasque = sonar_maskTimeSlot(Layers_PingBeam(indImageAngle), SonarTime);
    if ~isempty(layerMasque)
        Layers_PingBeam(indImageLayer) = masquage(Layers_PingBeam(indImageLayer), 'LayerMask', layerMasque, 'valMask', 1);
    end
end

%% Gestion de la limite angulaire

if ~isempty(InfoMos.LimitAngles)
    layerMasque = ROI_auto(Layers_PingBeam(indImageAngle), 1, 'MaskReflec', Layers_PingBeam(indImageLayer), [], InfoMos.LimitAngles, 1);
    Layers_PingBeam(indImageLayer) = masquage(Layers_PingBeam(indImageLayer), 'LayerMask', layerMasque, 'valMask', 1);
end

%% Gestion du recouvrement entre faisceaux dans le cas d'un sondeur dual

% {
% Comment� par JMA le 21/09/2018 pour donn�es EM2040 Shallow Survey 2018 Area3_1 : le sondeur n'est pas dual et il y a une pente lat�rale tr�s importante
if InfoMos.flagMasquageDual
    indImageAcrossDist = findIndLayerSonar(Layers_PingBeam, indImageLayer, 'strDataType', 'AcrossDist', 'OnlyOneLayer', 'WithCurrentImage', 1);
    [flag, Masque] = sonar_masqueRecoveringBeams(Layers_PingBeam(indImageAcrossDist), 'Angles', Layers_PingBeam(indImageAngle));
    if ~flag
        return
    end
    Layers_PingBeam(indImageLayer) = masquage(Layers_PingBeam(indImageLayer), 'LayerMask', Masque, 'valMask', 1);
end
% }

%% Masquage � partir d'un masque en LatLong

if ~isempty(MaskLatLong)
%     logFileInfo(logFileId, nameSurvey, 'Masking reflectivity layer is starting')
    [flag, MaskPingBeam] = sonarMosaiqueInv(Layers_PingBeam, 1, MaskLatLong);
    if ~flag
%         logFileWarn(logFileId, nameSurvey, 'Masking reflectivity layer failed')
        return
    end
    [Layers_PingBeam(indImageLayer), flag] = masquage(Layers_PingBeam(indImageLayer), 'LayerMask', MaskPingBeam, 'valMask', 1);
    if ~flag
%         logFileWarn(logFileId, nameSurvey, 'Masking reflectivity layer failed')
        return
    end
%     logFileInfo(logFileId, nameSurvey, 'Masking reflectivity layer is over')
end

%% Calcul des coordonn�es g�ographiques

[flag, LatLong_PingBeam] = sonarCalculCoordGeo(Layers_PingBeam, indImageLayer, 'NoWaitbar', NoWaitbar, 'Mute', Mute);
if ~flag
    return
end

if InfoMos.TypeGrid == 2
    SonarDistPings = get(Layers_PingBeam(indImageLayer), 'SonarDistPings');
    resolMedian = median(SonarDistPings, 'omitnan');
%     resolMean   = mean(SonarDistPings, 'omitnan');
%     if (resolMean/resolMedian) > 4
%         str1 = sprintf('Il se passe quelque chose d''�trange sur le fichier "%s", la distance inter pings moyenne est de %f m et la distance m�diane est de�%f m, je passe ce fichier.', nomFic, resolMean, resolMedian);
%         str2 = sprintf('Something strange is happening for file "%s", inter ping mean distance is %f m and median is�%f m. I skip this file.', nomFic, resolMean, resolMedian);
%         my_warndlg(Lang(str1,str2), 0);
%         return
%     end
    resol = nearestExpectedGridSize(resolMedian);
        
    %% Recherche du layer AcrossDist pour contr�ler la proposition de r�solution
    
    indLayerAcrossDist = findIndLayerSonar(Layers_PingBeam, 1, 'strDataType', 'AcrossDist', 'WithCurrentImage', 1);
    if isempty(indLayerAcrossDist)
        resolX = 0;
    else
    	resolX = (Layers_PingBeam(indLayerAcrossDist).StatValues.Max - Layers_PingBeam(indLayerAcrossDist).StatValues.Min) / (Layers_PingBeam(indLayerAcrossDist).nbColumns - 1);
    end
    resol = max(resol, resolX);
    fprintf('  --> Estimation of the grid size : Along distance=%s m, Across distance=%s m\n', num2str(resol,2), num2str(resolX,2));
end
[Nx, Ny, resolLon, resolLat, x, y, LonWest, LonEst, LatNord, LatSud, message, ...
    subx, suby, subxLat, subyLat, subxLon, subyLon, flag] = ...
    sonar_mosaique_size(Layers_PingBeam(indImageLayer), LatLong_PingBeam(1), LatLong_PingBeam(2), resol); %#ok<ASGLU>
if ~flag
    return
end

if isnan(resolLon)
    disp(message)
    str1 = sprintf('Le fichier "%s" semble d�fectueux, vous devriez v�rifier sa taille.', nomFic);
    str2 = sprintf('"%s" seems to be in bad form, please check its size.', nomFic);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Cr�ation de la mosa�que du layer principal

[flag, Layer_LatLong] = sonar_mosaique(Layers_PingBeam(indImageLayer), ...
    LatLong_PingBeam(1), LatLong_PingBeam(2), resol, 'AcrossInterpolation', 2, 'NoWaitbar', NoWaitbar);
if ~flag
    return
end

InitialImageName = strrep(Layer_LatLong.InitialImageName, '_Compensation_Compensation', '_Compensation');
InitialImageName = [InitialImageName ' - ' num2str(rand(1))];
Layer_LatLong = WinFillNaN(Layer_LatLong, 'window', Window);
Layer_LatLong.InitialImageName = InitialImageName;

%% Cr�ation de la mosa�que des angles

[flag, Angle_LatLong] = sonar_mosaique(Layers_PingBeam(indImageAngle), ...
    LatLong_PingBeam(1), LatLong_PingBeam(2), resol, 'AcrossInterpolation', 2, ...
    'NoWaitbar', NoWaitbar);
if ~flag
    return
end

Angle_LatLong = WinFillNaN(Angle_LatLong, 'window', Window);
Angle_LatLong.InitialImageName = [Angle_LatLong.InitialImageName ' - ' num2str(rand(1))];

%% Crop des 2 mosa�ques individuelles (elles peuvent ne pas avoir �t� remplies de la m�me fa�on si c'est par exemple une mosa�que d'angles d'incidence)

M1 = isnan(Layer_LatLong);
M2 = isnan(Angle_LatLong);
M = ~(M1 | M2);
clear M1 M2
subxExtraction = find(sum(M,1));
subyExtraction = find(sum(M,2))';

if isempty(subxExtraction) || isempty(subyExtraction)
    return
end

subxExtraction = subxExtraction(1):subxExtraction(end);
subyExtraction = subyExtraction(1):subyExtraction(end);
clear M
if (length(subxExtraction) <= (Layer_LatLong.nbColumns - 10)) || (length(subyExtraction) <= (Layer_LatLong.nbRows-10))
    Layer_LatLong = extraction(Layer_LatLong, 'suby', subyExtraction, 'subx', subxExtraction);
    Angle_LatLong = extraction(Angle_LatLong, 'suby', subyExtraction, 'subx', subxExtraction);
end

%% Sauvegarde de la mosa�que du layer principal si mosa�que individuelle par fichier demand�e

if InfoMos.TypeMos == 2
    nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_' nomDataType '_LatLong.ers']);
    flag = export_ermapper(Layer_LatLong, nomFicErs);
    if ~flag
        return
    end
end

%% Sauvegarde de la mosa�que des angles si mosa�que individuelle par fichier demand�e

if InfoMos.TypeMos == 2
    nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_RxBeamAngle_LatLong.ers']);
    flag = export_ermapper(Angle_LatLong, nomFicErs);
    if ~flag
        return
    end
end

%% Assemblage de mosa�que si mosa�que unique demand�e

if InfoMos.TypeMos == 1
    
    %% Assemblage de la mosaique de l'image avec la mosaique g�n�rale
    
    [~, TitreDepth] = fileparts(InfoMos.Unique.nomFicErMapperLayer);
    [~, TitreAngle] = fileparts(InfoMos.Unique.nomFicErMapperAngle);
    [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Layer_LatLong], ...
        'Angle', [A_Mosa_All Angle_LatLong], ...
        'FistImageIsCollector', 1, ...
        'CoveringPriority', InfoMos.Covering.Priority, ...
        'TetaLimite', InfoMos.Covering.SpecularLimitAngle);
    Z_Mosa_All.Name = TitreDepth;
    A_Mosa_All.Name = TitreAngle;
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(iFic,InfoMos.Backup) == 0)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        StatValues = Z_Mosa_All.StatValues;
        CLim = StatValues.Quant_25_75;
        export_ermapper(Z_Mosa_All, InfoMos.Unique.nomFicErMapperLayer, 'CLim', CLim);
        
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        export_ermapper(A_Mosa_All, InfoMos.Unique.nomFicErMapperAngle);
        
        LastSavedIFic = iFic;
    end
end
