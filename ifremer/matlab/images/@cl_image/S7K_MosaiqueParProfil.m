function [flag, Mosaiques] = S7K_MosaiqueParProfil(this, indImage, listeFicS7k, ModeDependant, LayerName, ...
    Window, SameReflectivityStatus, InfoCompensationCurve, InfoMos, varargin)

[varargin, flagTideCorrection] = getPropertyValue(varargin, 'TideCorrection', 0); %#ok<ASGLU>

flag = 0;
Mosaiques = [];

resol = InfoMos.gridSize;
Carto = [];

S0 = cl_simrad_all([]);
dosNetUse('Init')

if isempty(listeFicS7k)
    return
end

Selection = [];

%% Recherche des layers synchronis�s

nomsLayers{1} = LayerName;
DataTypes = ALL.identSonarDataType(LayerName);

DataTypes(2)  = cl_image.indDataType('AcrossDist');
nomsLayers{2} = 'AcrossDist';
DataTypes(3)  = cl_image.indDataType('AlongDist');
nomsLayers{3} = 'AlongDist';
DataTypes(4)  = cl_image.indDataType('TxAngle');
nomsLayers{4} = 'BeamDepressionAngle'; %'TxAngle';

%% Lecture des fichiers de compensation si il y en a

if isempty(InfoCompensationCurve) || isempty(InfoCompensationCurve.FileName)
    bilan = [];
else
    FileNameCompensation = InfoCompensationCurve.FileName;
    if ischar(FileNameCompensation)
        FileNameCompensation = {FileNameCompensation};
    end
    ModeDependant = InfoCompensationCurve.ModeDependant;
    
    for k2=1:length(FileNameCompensation)
        CourbeConditionnelle = load_courbesStats(FileNameCompensation{k2});
        if isempty(CourbeConditionnelle)
            %             flag = 0;
            return
        end
        bilan{k2} = CourbeConditionnelle.bilan; %#ok<AGROW>
        % Selection = % TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    end
end

%% R�cup�ration �ventuelle des fichiers mosa�que si reprise d'une mosa�que existante

Z_Mosa_All = [];

if InfoMos.TypeMos == 1
    if InfoMos.Unique.CompleteExistingMosaic
        [flag, Z_Mosa_All] = cl_image.import_ermapper(InfoMos.Unique.nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        [flag, A_Mosa_All]  = cl_image.import_ermapper(InfoMos.Unique.nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
end

%% Lecture du fichier de d�coupe horaire

if ~isempty(InfoMos.Import.SlotFilename)
    SonarTime = lecFicDecCar(InfoMos.Import.SlotFilename);
end

%% Traitement des fichiers

LastSavedIFic = 0;
N = length(listeFicS7k);
if InfoMos.TypeMos == 1
    str1 = 'Calcul de la mosa�que';
    str2 = 'Computing the mosaic';
else
    str1 = 'Calcul des mosa�ques individuelles.';
    str2 = 'Computing individual mosaics.';
end
hw = create_waitbar(Lang(str1,str2), 'N', N);
for iFic=1:N
    nomFic = listeFicS7k{iFic};
    [~, nomFicSeul] = fileparts(nomFic);
    
    my_waitbar(iFic, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), iFic, N, nomFic);
    
    if ~exist(nomFic, 'file')
        str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
        str2 = sprintf('"%s" does not exist.', nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierAllNExistePAs');
        continue
    end
    
    %% Cr�ation de l'instance sonar
    
    s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
    if isempty(s7k)
        continue
    end
    
    %% get layers processed as the current image
    
    PreserveMeanValueIfModel = InfoCompensationCurve.PreserveMeanValueIfModel;
    [flag, Layers_PingBeam, indImageLayer, Selection, Carto] = get_layers(s7k, nomsLayers, DataTypes(1), DataTypes, ...
        bilan, ModeDependant, Carto, Selection, InfoCompensationCurve.UseModel, ...
        'PreserveMeanValueIfModel', PreserveMeanValueIfModel, 'TideCorrection', flagTideCorrection, ...
        'MasqueActif', InfoMos.Import.MasqueActif);
    if ~flag
        continue
    end
    
    if Layers_PingBeam(indImageLayer).DataType ~= DataTypes(1) % Test rajout� par JMA le 07/12/2019
        continue
    end
    
    if isempty(indImageLayer) % Cas o� on n'est pas pass� pas par une image d'exemple
        indImageLayer = length(Layers_PingBeam);
    end
    nomDataType = cl_image.strDataType{Layers_PingBeam(indImageLayer).DataType};
    
    %% Recherche du layer d'angle
    
    for k=1:length(Layers_PingBeam)
        DT(k) = Layers_PingBeam(k).DataType; %#ok<AGROW>
    end
    
    %     indImageAngle = find(...
    %         (DT == cl_image.indDataType('RxAngleEarth')) | ...
    %         (DT == cl_image.indDataType('RxBeamAngle')) | ...
    %         (DT == cl_image.indDataType('TxAngle')) | ...
    %         (DT == cl_image.indDataType('BeamPointingAngle')));
    indImageAngle = find(DT == cl_image.indDataType('RxAngleEarth'));
    if isempty(indImageAngle)
        indImageAngle = find(DT == cl_image.indDataType('RxBeamAngle'));
    end
    if isempty(indImageAngle)
        indImageAngle = find(DT == cl_image.indDataType('TxAngle'));
    end
    if isempty(indImageAngle)
        indImageAngle = find(DT == cl_image.indDataType('BeamPointingAngle'));
    end
    
    %% Recherche des pings dans la plage horaire
    
    if ~isempty(InfoMos.Import.SlotFilename)
        layerMasque = sonar_maskTimeSlot(Layers_PingBeam(indImageAngle), SonarTime);
        if ~isempty(layerMasque)
            Layers_PingBeam(indImageLayer) = masquage(Layers_PingBeam(indImageLayer), 'LayerMask', layerMasque, 'valMask', 1);
        end
    end
    
    %% Gestion de la limite angulaire
    
    if ~isempty(InfoMos.LimitAngles)
        layerMasque = ROI_auto(Layers_PingBeam(indImageAngle), 1, 'MaskReflec', Layers_PingBeam(indImageLayer), [], InfoMos.LimitAngles, 1);
        Layers_PingBeam(indImageLayer) = masquage(Layers_PingBeam(indImageLayer), 'LayerMask', layerMasque, 'valMask', 1);
    end
    
    %% Calcul des coordonn�es g�ographiques
    
    [flag, LatLong_PingBeam] = sonarCalculCoordGeo(Layers_PingBeam, indImageLayer);
    if ~flag
        continue
    end
    
    if InfoMos.TypeGrid == 2
        SonarDistPings = get(Layers_PingBeam(indImageLayer), 'SonarDistPings');
        resolMean   = mean(  SonarDistPings, 'omitnan');
        resolMedian = median(SonarDistPings, 'omitnan');
        if (resolMean/resolMedian) > 4
            str1 = sprintf('Il se passe quelque chose d''�trange sur le fichier "%s", la distance inter pings moyenne est de %f m et la distance m�diane est de�%f m, je passe ce fichier.', nomFic, resolMean, resolMedian);
            str2 = sprintf('Something strange is happening for file "%s", inter ping mean distance is %f m and median is�%f m. I skip this file.', nomFic, resolMean, resolMedian);
            my_warndlg(Lang(str1,str2), 0);
            continue
        end
        resol = nearestExpectedGridSize(resolMedian);
    end
    [Nx, Ny, resolLon, resolLat, x, y, LonWest, LonEst, LatNord, LatSud, message, ...
        subx, suby, subxLat, subyLat, subxLon, subyLon, flag] = ...
        sonar_mosaique_size(Layers_PingBeam(indImageLayer), LatLong_PingBeam(1), LatLong_PingBeam(2), resol); %#ok<ASGLU>
    if ~flag
        continue
    end
    
    if isnan(resolLon)
        disp(message)
        str1 = sprintf('Le fichier "%s" semble d�fectueux, vous devriez v�rifier sa taille.', nomFic);
        str2 = sprintf('"%s" seems to be in bad form, please check its size.', nomFic);
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    %% Cr�ation de la mosa�que du layer principal
    
    [flag, Layer_LatLong] = sonar_mosaique(Layers_PingBeam(indImageLayer), ...
        LatLong_PingBeam(1), LatLong_PingBeam(2), resol, 'AcrossInterpolation', 2);
    if ~flag
        continue
    end
    
    InitialImageName = strrep(Layer_LatLong.InitialImageName, '_Compensation_Compensation', '_Compensation');
    InitialImageName = [InitialImageName ' - ' num2str(rand(1))]; %#ok<AGROW>
    Layer_LatLong = WinFillNaN(Layer_LatLong, 'window', Window);
    Layer_LatLong.InitialImageName = InitialImageName;
    
    %% Cr�ation de la mosa�que des angles
        
    [flag, Angle_LatLong] = sonar_mosaique(Layers_PingBeam(indImageAngle), ...
        LatLong_PingBeam(1), LatLong_PingBeam(2), resol, 'AcrossInterpolation', 2);
    if ~flag
        return
    end
    
    InitialImageName = [Angle_LatLong.InitialImageName ' - ' num2str(rand(1))];
    Angle_LatLong = WinFillNaN(Angle_LatLong, 'window', Window);
    Angle_LatLong.InitialImageName = InitialImageName;
    
    %% Crop des 2 mosa�ques individuelles (elles peuvent ne pas avoir �t� remplies de la m�me fa�on si c'est par exemple une mosa�que d'angles d'incidence)
    
    M1 = isnan(Layer_LatLong);
    M2 = isnan(Angle_LatLong);
    M = ~(M1 | M2);
    clear M1 M2
    subxExtraction = find(sum(M,1));
    subyExtraction = find(sum(M,2))';
    subxExtraction = subxExtraction(1):subxExtraction(end);
    subyExtraction = subyExtraction(1):subyExtraction(end);
    clear M
    if (length(subxExtraction) <= (Layer_LatLong.nbColumns - 10)) || (length(subyExtraction) <= (Layer_LatLong.nbRows-10))
        Layer_LatLong = extraction(Layer_LatLong, 'suby', subyExtraction, 'subx', subxExtraction);
        Angle_LatLong = extraction(Angle_LatLong, 'suby', subyExtraction, 'subx', subxExtraction);
    end
    
    %% Sauvegarde de la mosa�que du layer principal si mosa�que individuelle par fichier demand�e
    
    if InfoMos.TypeMos == 2
        nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_' nomDataType '_LatLong.ers']);
        flag = export_ermapper(Layer_LatLong, nomFicErs);
        if ~flag
            return
        end
    end
    
    %% Sauvegarde de la mosa�que des angles si mosa�que individuelle par fichier demand�e
    
    if InfoMos.TypeMos == 2
        nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_RxBeamAngle_LatLong.ers']);
        flag = export_ermapper(Angle_LatLong, nomFicErs);
        if ~flag
            str1 = sprintf('Probl�me d''�criture de "%s", arr�t du traitement.', nomFicErs);
            str2 = sprintf('Write error for "%s", sopping the processing.', nomFicErs);
            my_warndlg(Lang(str1,str2), 1);
            return
        end
    end
    
    %% Assemblage de mosa�que si mosa�que unique demand�e
    
    if InfoMos.TypeMos == 1
        
        %% Assemblage de la mosaique de l'image avec la mosaique g�n�rale
        
        [~, TitreDepth] = fileparts(InfoMos.Unique.nomFicErMapperLayer);
        [~, TitreAngle] = fileparts(InfoMos.Unique.nomFicErMapperAngle);
        [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Layer_LatLong], ...
            'Angle', [A_Mosa_All Angle_LatLong], ...
            'FistImageIsCollector', 1, ...
            'CoveringPriority', InfoMos.Covering.Priority, ...
            'TetaLimite', InfoMos.Covering.SpecularLimitAngle);
        Z_Mosa_All.Name = TitreDepth;
        A_Mosa_All.Name = TitreAngle;
        
        %% Sauvegarde des mosaiques dans des fichiers ErMapper
        
        if (mod(iFic,InfoMos.Backup) == 0)
            Z_Mosa_All = majCoordonnees(Z_Mosa_All);
            StatValues = Z_Mosa_All.StatValues;
            CLim = StatValues.Quant_25_75;
            export_ermapper(Z_Mosa_All, InfoMos.Unique.nomFicErMapperLayer, 'CLim', CLim);
            
            A_Mosa_All = majCoordonnees(A_Mosa_All);
            export_ermapper(A_Mosa_All, InfoMos.Unique.nomFicErMapperAngle);
            
            LastSavedIFic = iFic;
        end
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60);

%% Sauvegarde de la derni�re mouture

if (InfoMos.TypeMos == 1) && (LastSavedIFic ~= N) && ~isempty(Z_Mosa_All)
    Z_Mosa_All = majCoordonnees(Z_Mosa_All);
    StatValues = Z_Mosa_All.StatValues;
    CLim = StatValues.Quant_25_75;
    export_ermapper(Z_Mosa_All, InfoMos.Unique.nomFicErMapperLayer, 'CLim', CLim);
    
    A_Mosa_All = majCoordonnees(A_Mosa_All);
    export_ermapper(A_Mosa_All, InfoMos.Unique.nomFicErMapperAngle);
end

%% Affichage de l'image si mosa�que unique

if (InfoMos.TypeMos == 1)
    if isempty(Z_Mosa_All)
        flag = 0;
        return
    else
        Mosaiques = [Z_Mosa_All  A_Mosa_All];
    end
end

dosNetUse('Clear')
flag = 1;
