function flag = S7K_StatsBathyMinusDTM(this, liste, CleanCurves, TideFile, flagVsLayers, flagDisplay)

MNT = this(1);
MNT = Virtualmemory2Ram(MNT);

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(MNT, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

if ischar(liste)
    liste = {liste};
end

% useParallel = get_UseParallelTbx;

Carto = [];
Bits  = 'AskIt';
N = length(liste);
str1 = 'Nettoyage de la bathym�trie des fichiers .all par comparaison � un MNT de r�f�rence.';
str2 = 'Bathymetry cleaning of .all files by comparison to a DTM.';
% if useParallel && (N > 1) % Contreproductif pour donn�es mission QUOI
%     [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
%     flag = 0;
%     k = N;
%     while flag == 0
%         [flag, X] = ALL_StatsBathyMinusDTM_unitaire(MNT, liste{k});
%         if flag
%             CurvesA(k) = X; %#ok<AGROW>
%         end
%         N = k-1;
%     end
%     parfor (k=1:N, useParallel)
%         fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, liste{k});
%         [flag, Carto, Bits, X1, X2, X3, X4, X5, X6] = S7K_StatsBathyMinusDTM_unitaire(MNT, liste{k}, flagVsLayers, flagDisplay, Carto, Bits, TideFile);
%         if flag
%             CurvesA(k) = X;
%         end
%         send(DQ, iFic);
%     end
%     my_close(hw, 'MsgEnd')
% else
    CurvesA = [];
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, liste{k});
        
        [flag, Carto, Bits, X1, X2, X3, X4, X5, X6] = S7K_StatsBathyMinusDTM_unitaire(MNT, liste{k}, Carto, Bits, TideFile);
        if flag
            if isempty(CurvesA)
                CurvesA = X1;
                CurvesB = X2;
                CurvesC = X3;
                CurvesD = X4;
                CurvesE = X5;
                CurvesF = X6;
            else
                CurvesA(end+1) = X1; %#ok<AGROW>
                CurvesB(end+1) = X2; %#ok<AGROW>
                CurvesC(end+1) = X3; %#ok<AGROW>
                CurvesD(end+1) = X4; %#ok<AGROW>
                CurvesE(end+1) = X5; %#ok<AGROW>
                CurvesF(end+1) = X6; %#ok<AGROW>
            end
        end
    end
    my_close(hw, 'MsgEnd')
% end
if isempty(CurvesA)
    return
end

% flagDisplay
if any(flagVsLayers == 1)
    cleanAndPlotCurves(CurvesA, CleanCurves, 'Bathy Minus DTM (m)', flagDisplay)
end
if any(flagVsLayers == 2)
    cleanAndPlotCurves(CurvesB, false,       'Bathy Minus DTM (m) vs DetectionType', flagDisplay)
end
if any(flagVsLayers == 3)
    cleanAndPlotCurves(CurvesE, CleanCurves, 'Bathy Minus DTM (m) vs Frequencies', flagDisplay)
end

if any(flagVsLayers == 4)
    cleanAndPlotCurves(CurvesC, CleanCurves, 'Bathy Minus DTM percentage (%)', flagDisplay)
end
if any(flagVsLayers == 5)
    cleanAndPlotCurves(CurvesD, false,       'Bathy Minus DTM percentage (%) vs DetectionType', flagDisplay)
end
if any(flagVsLayers == 6)
    cleanAndPlotCurves(CurvesF, false,       'Bathy Minus DTM percentage (%) vs Frequencies', flagDisplay)
end



function cleanAndPlotCurves(CurvesA, CleanCurves, CurveName, flagDisplay)

L = 1000; % 1200
H =  600; % 700

%% Affichage des courbes

[flag, FigCurvesA] = plot_CourbesStats(CurvesA, 'Stats', 0, 'Position', centrageFig(L, H), 'Titre', CurveName);
if ~flag
    return
end

%% Nettoyage des courbes

if CleanCurves
    Title = 'SignalDialog window : please clean up the curves';
    [flag, CurvesACleaned] = CourbesStatsClean(CurvesA, 'Stats', 0, 'NoPlot', 'Title', Title);
    if ~flag || isempty(CurvesACleaned)
        return
    end
else
    CurvesACleaned = CurvesA;
end

%% Plot des courbes nettoy�es

[flag, FigCurvesACleaned] = plot_CourbesStats(CurvesACleaned, 'Stats', 0, ...
    'Position', centrageFig(L, H), 'Titre', CurveName, 'Coul', [0.5 0.5 0.5]);
if ~flag
    return
end
close(FigCurvesA);

%% Moyennage des courbes

[flag, CurvesAMean] = CourbesStatsUnionWithHoles(CurvesACleaned, 'nbPMin', 10, 'nomCourbe', CurveName);
if ~flag
    return
end

%% Nettoyage de la courbe moyenn�e

if CleanCurves
    Title = 'SignalDialog window : please clean up the curve';
    [flag, CurvesAMeanCleaned] = CourbesStatsClean(CurvesAMean, 'Stats', 0, 'NoPlot', 'Title', Title);
    if ~flag
        return
    end
else
    CurvesAMeanCleaned = CurvesAMean;
end

%% Visualisation de la courbe moyenn�e

if any(flagDisplay == 1)
    [~, FigCurvesACleaned] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 0, 'Fig', FigCurvesACleaned, ...
        'Coul', 'b', 'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', [CurveName ' - Mean over all individual curves']);
else
    close(FigCurvesACleaned)
end

if any(flagDisplay == 2)
    [~, FigCurvesAMeanCleaned] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 0, 'Fig', [], ...
        'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', [CurveName ' - Mean only']);
end

if any(flagDisplay == 3)
    [~, FigCurvesAMeanCleanedMeanPlusStd] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 1, 'Fig', [], ...
        'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', [CurveName ' - Mean � std']);
end

if any(flagDisplay == 4)
    for k=1:length(CurvesAMeanCleaned.bilan{1})
        CurvesAMeanCleaned.bilan{1}(k).nomZoneEtude = strrep(CurvesAMeanCleaned.bilan{1}(k).nomZoneEtude, 'Bias', 'Std');
    end
    [~, FigCurvesAMeanCleanedStd] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 2, 'Fig', [], ...
        'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', [CurveName ' - Standard deviation']);
end


function [flag, Carto, Bits, CurvesA, CurvesB, CurvesC, CurvesD, CurvesE, CurvesF] = S7K_StatsBathyMinusDTM_unitaire(MNT, nomFic, Carto, Bits, TideFile, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', false); %#ok<ASGLU>

flag    = 0;
CurvesA = [];
CurvesB = [];
CurvesC = [];
CurvesD = [];
CurvesE = [];
CurvesF = [];

%% Check if file exists

if ~exist(nomFic, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('"%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Cr�ation de l'instance cl_simrad_all

s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
if isempty(s7k)
    return
end

%% Tri des layers dans l'ordre : Bathy, xxx et yyy

[a, Carto, Bits] = view_depth(s7k, 'Carto', Carto, 'ListeLayers', [2:4 10 12], 'TideFile', TideFile, 'Bits', Bits);
a = a([5 1:4]);

% Version = version_datagram(s7k);
% switch Version
%     case 'V1'
%         a = get_Layer(s7k, {'Bathymetry'; 'AcrossDist'; 'AlongDist'; 'BeamPointingAngle',; 'DetectionType'}, 'CartoAuto', 1, 'TideCorrection', 1);
%         % TODO 
%     case 'V2'
%         a = get_Layer(s7k, {'Bathymetry'; 'AcrossDist'; 'AlongDist'; 'BeamPointingAngle',; 'DetectionType'}, 'CartoAuto', 1, 'TideCorrection', 1);
%         % TODO : Cas de la V2 o� l'ordre des layers ramen�s ne respecte pas celui demand�
%         a = a([3 1 2 5 4]);

%% Check if the tide is present

if isempty(a(1).Sonar.Tide) || all(isnan(a(1).Sonar.Tide))
    str = sprintf('No tide for "%s"\n', nomFic);
    my_warndlg(str, 0, 'Tag', 'NoTideHere', 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
end

%% Aspiration du MNT en g�om�trie PingBeam

[flag, b] = sonarMosaiqueInv(a, 1, MNT);
if ~flag
    return
end

%% Calcul de la diff�rence : bathy - MNT

c = b - a(1);
this = [c a];

%% Calcul de la courbe angulaire de Z-MNT

nbPMin = 10; % TODO
Tag = [];
[nomDir, nomCourbe] = fileparts(nomFic);
Comment{1} = sprintf('Dir name : %s', nomDir);

Selection.Name     = nomCourbe;
Selection.TypeVal  = 'Layer';
Selection.indLayer = 5;
Selection.DataType = 88; % BeamPointingAngle
Selection.Unit     = 'deg';

X = courbesStats(this(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    this, Selection, 'stepBins', 1, ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin, ...
    'SelectMode', [], 'SelectSwath', [], 'SelectFM', []);
if isempty(X)
    flag = 0;
    return
end

CurvesA = X.CourbesStatistiques;
% plot_CourbesStats(CurvesA)

%% Calcul de la courbe angulaire de Z-MNT et DetectionType

Selection(2).Name     = nomCourbe;
Selection(2).TypeVal  = 'Layer';
Selection(2).indLayer = 6; % TODO
Selection(2).DataType = 34; % DetectionType
Selection(2).Unit     = '';

X = courbesStats(this(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    this, Selection, 'stepBins', [1, 1], ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin, ...
    'SelectMode', [], 'SelectSwath', [], 'SelectFM', []);
if isempty(X)
    flag = 0;
    return
end
      
CurvesB = X.CourbesStatistiques;
% plot_CourbesStats(CurvesB)

%% Calcul de la courbe angulaire de Z-MNT et Frequencies

bins{1} = CurvesB.bilan{1}(1).x;
bins{1} = -90:90;
F = get(this(1), 'SonarFrequency');
bins{2} = unique(F);
Selection(2).Name     = 'SonarFrequency';
Selection(2).TypeVal  = 'SignalVert';
Selection(2).indLayer = 0;
Selection(2).DataType = [];
Selection(2).Unit     = 'kHz';

X = courbesStats(this(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    this, Selection, ...
    'bins', bins, 'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin, ...
    'SelectMode', [], 'SelectSwath', [], 'SelectFM', []);
if isempty(X)
    flag = 0;
    return
end
      
CurvesE = X.CourbesStatistiques;
% plot_CourbesStats(CurvesE)

%% Calcul de la diff�rence : bathy - MNT

c =  100 * (b - a(1)) / abs(a(1));
this = [c a];

%% Calcul de la courbe angulaire de (Z-MNT)/MNT

nbPMin = 10; % TODO
Tag = [];
[nomDir, nomCourbe] = fileparts(nomFic);
Comment{1} = sprintf('Dir name : %s', nomDir);

clear Selection
Selection.Name     = nomCourbe;
Selection.TypeVal  = 'Layer';
Selection.indLayer = 5;
Selection.DataType = 88; % BeamPointingAngle
Selection.Unit     = 'deg';
        
X = courbesStats(this(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    this, Selection, 'stepBins', 1, ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin, ...
    'SelectMode', [], 'SelectSwath', [], 'SelectFM', []);
if isempty(X)
    flag = 0;
    return
end

CurvesC = X.CourbesStatistiques;
% plot_CourbesStats(CurvesC)

%% Calcul de la courbe angulaire  de (Z-MNT)/MNT en fonction de DetectionType

Selection(2).Name     = nomCourbe;
Selection(2).TypeVal  = 'Layer';
Selection(2).indLayer = 6; % TODO
Selection(2).DataType = 34; % DetectionType
Selection(2).Unit     = '';

X = courbesStats(this(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    this, Selection, 'stepBins', [1, 1], ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin, ...
    'SelectMode', [], 'SelectSwath', [], 'SelectFM', []);
if isempty(X)
    flag = 0;
    return
end
      
CurvesD = X.CourbesStatistiques;
% plot_CourbesStats(CurvesD)

%% Calcul de la courbe angulaire  de (Z-MNT)/MNT en fonction de Frequencies

Selection(2).Name     = 'SonarFrequency';
Selection(2).TypeVal  = 'SignalVert';
Selection(2).indLayer = 0;
Selection(2).DataType = []; % DetectionType
Selection(2).Unit     = 'kHz';

X = courbesStats(this(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    this, Selection, ...
    'bins', bins, 'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin, ...
    'SelectMode', [], 'SelectSwath', [], 'SelectFM', []);
if isempty(X)
    flag = 0;
    return
end
      
CurvesF = X.CourbesStatistiques;
% plot_CourbesStats(CurvesF)
