function flag = SCAMPI_creationXmlFromNMEA(~, listeFic, nomFicNavCorrigee, nomFicOut, Focale)
 
flag = 0;

nbFicImages = length(listeFic);
if nbFicImages == 0
    return
end

%% R�cup�ration des heures des photos

FistTime = 1;
nbFicImages = length(listeFic);
% tMat        = zeros(1, nbFicImages); %% Avant
tMat        = NaN(1, nbFicImages); %% Modif JMA le 28/03/2013
HeightImage = NaN(1, nbFicImages);
str1 = 'Lecture des heures des noms des images.';
str2 = 'reading times from the image names.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFicImages);
for k=1:nbFicImages
    my_waitbar(k, nbFicImages, hw);
    [flag, t] = SCAMPI_getTimeFromFileName(listeFic{k});
    if ~flag
        continue
    end
    tMat(k) = t;
    info = imfinfo(listeFic{k});
    if isfield(info, 'GPSInfo')
        if isfield(info.GPSInfo, 'GPSAltitude')
            if info.GPSInfo.GPSAltitude == 0
                HeightImage(k) = 2.5;
            else
                HeightImage(k) = info.GPSInfo.GPSAltitude / 10;
            end
        else
             HeightImage(k) = 2.5;
        end
    else
        HeightImage(k) = 2.5;
        if FistTime
            str1 = 'Il n''y a pas de donn�e d''altim�trie dans les images, la valeur 2.5 m est utilis�e';
            str2 = 'TODO';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasDeGPSIfo DansImage');
        end
        FistTime = 0;
    end
end
my_close(hw);
tImage = cl_time('timeMat', tMat);
% figure; plot(HeightImage, '-*'); grid on;

LatImage = [];
LonImage = [];

%% Lecture de la navigation

if ~isempty(nomFicNavCorrigee)
    N = getNbLinesAsciiFile(nomFicNavCorrigee);
    if N == 0
        messageErreurFichier(nomFic, 'ReadFailure');
        flag = 0;
        return
    end
    DateCam   = NaN(N, 1);
    HeureCam  = NaN(N, 1);
    LatCam    = NaN(N, 1);
    LonCam    = NaN(N, 1);
    SpeedCam     = NaN(N, 1, 'single');
    HeadingCam   = NaN(N, 1, 'single');
    ImmersionCam = NaN(N, 1, 'single');
    CableOut     = NaN(N, 1, 'single');
    CableTension = NaN(N, 1, 'single');

    fid = fopen(nomFicNavCorrigee, 'r');
    for k=1:N
        str = fgetl(fid);
        if strcmp(str(1:7), '$SCAMPI')
            [flag, DateCam(k), HeureCam(k), LatCam(k), LonCam(k), HeadingCam(k), SpeedCam(k), ImmersionCam(k), CableOut(k), CableTension(k)] = read_NMEA_SCAMPI(str);
        end
    end
    fclose(fid);
        
%     figure; plot(LonCam, LatCam, '.'); grid on; axisGeo

% TODO : remplacer tout ce qui suit par la cr�ation d'un signal et sig.plot()

    figure('Name', 'Signals from navigation file');
    for k=1:5
        hAxes(k) = subplot(5,1,k); %#ok<AGROW>
    end
    PlotUtils.createSScPlot(hAxes(1), HeadingCam);    grid(hAxes(1), 'on'); title(hAxes(1), 'Heading (deg)')
    PlotUtils.createSScPlot(hAxes(2), -ImmersionCam); grid(hAxes(2), 'on'); title(hAxes(2), 'Immersion  (m)')
    PlotUtils.createSScPlot(hAxes(3), SpeedCam);      grid(hAxes(3), 'on'); title(hAxes(3), 'Speed (?)')
    PlotUtils.createSScPlot(hAxes(4), CableOut);      grid(hAxes(4), 'on'); title(hAxes(4), 'CableOut (m)')
    PlotUtils.createSScPlot(hAxes(5), CableTension);  grid(hAxes(5), 'on'); title(hAxes(5), 'CableTension (?)')
    axis(hAxes, 'tight');
    linkaxes(hAxes, 'x'); 
    
    tCam = cl_time('timeIfr', DateCam, HeureCam);
    if ~isempty(tCam)
        LonImage   = my_interp1_longitude(tCam, LonCam,       tImage);
        LatImage   = my_interp1(          tCam, LatCam,       tImage);
        CapImage   = my_interp1(          tCam, HeadingCam,   tImage);
        DepthImage = my_interp1(          tCam, ImmersionCam, tImage);
    end
    
    if all(isnan(LonImage))
        str1 = 'Il y a visiblement incompatibilit� de date entre les images et la navigation.';
        str2 = 'The dates of the images and the navigation file do not correspond.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    
% TODO : remplacer tout ce qui suit par la cr�ation d'un NavigationDialog et sig.plot()
    figure('Name', 'Signals for images');
    for k=1:3
        hAxes(k) = subplot(3,1,k);
    end
%     PlotUtils.createSScPlot(hAxes(1), tImage, CapImage, '*');    grid(hAxes(1), 'on'); title(hAxes(1), 'Heading (deg)')
%     PlotUtils.createSScPlot(hAxes(2), tImage, DepthImage, '*');  grid(hAxes(2), 'on'); title(hAxes(2), 'Immersion (m)')
%     PlotUtils.createSScPlot(hAxes(3), tImage, HeightImage, '*'); grid(hAxes(3), 'on'); title(hAxes(3), 'Height (m)')
    
    % TODO : en attendant de g�rer le truc directement dans PlotUtils.createSScPlot
    PlotUtils.createSScPlot(tImage, CapImage, '*');    grid(hAxes(1), 'on'); title(hAxes(1), 'Heading (deg)')
    PlotUtils.createSScPlot(tImage, -DepthImage, '*'); grid(hAxes(2), 'on'); title(hAxes(2), 'Immersion (m)')
    PlotUtils.createSScPlot(tImage, HeightImage, '*'); grid(hAxes(3), 'on'); title(hAxes(3), 'Height (m)')

    plot_navigation(nomFicNavCorrigee, [], LonImage, LatImage, tImage.timeMat, CapImage);
end

%% Exportation des donn�es dans une structure XML-Bin

N = length(LatImage);
DataAllFiles.Latitude   = LatImage;
DataAllFiles.Longitude  = LonImage;
DataAllFiles.Height     = HeightImage;
DataAllFiles.Heading    = CapImage;
DataAllFiles.Depth      = DepthImage;
DataAllFiles.Tilt       = zeros(1, N, 'single');
DataAllFiles.Pan        = zeros(1, N, 'single');
DataAllFiles.Focale     = Focale + zeros(1, N, 'single');
DataAllFiles.GITE       = zeros(1, N, 'single'); % GITE(k);
DataAllFiles.ASSIETTE   = zeros(1, N, 'single'); %ASSIETTE(k);
DataAllFiles.CAP_3CCD   = zeros(1, N, 'single'); %ASSIETTE(k);
DataAllFiles.Time       = tImage;
DataAllFiles.FileNames  = listeFic;

flag = SCAMPI_write_XML_Bin(nomFicOut, DataAllFiles);


%% Ecriture fichier XML-Bin contenant toutes les infos

function flag = SCAMPI_write_XML_Bin(nomFicXml, Data)

[nomDirRacine, nomFicSeul] = fileparts(nomFicXml);
NomDirADefinir = nomFicSeul;
nbSamples = length(Data.Latitude);

%% G�n�ration du XML SonarScope

Info.Title                    = 'SCAMPI';
Info.TimeOrigin               = '01/01/-4713';
Info.Comments                 = 'Information relative aux images SCAMPI';
Info.FormatVersion            = '20140125';

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Latitude';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Latitude.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'Longitude';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Longitude.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSLatitude');
  
Info.Signals(end+1).Name      = 'Height';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Height.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSAccuracy');

Info.Signals(end+1).Name      = 'Depth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Depth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Tilt';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Tilt.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Pan';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Pan.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSHeading');

Info.Signals(end+1).Name      = 'Focale';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'Focale.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'GITE';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'GITE.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ASSIETTE';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'ASSIETTE.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'CAP_3CCD';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(NomDirADefinir,'CAP_3CCD.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Strings(1).Name          = 'FileNames';
Info.Strings(end).Dimensions  = 'NbSamples, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile(NomDirADefinir,'FileNames.bin');
Info.Strings(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire des donn�es binaires

NomDirBin = fullfile(nomDirRacine, NomDirADefinir);
if ~exist(NomDirBin, 'dir')
    status = mkdir(NomDirBin);
    if ~status
        messageErreur(NomDirBin)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des strings

if isfield(Info, 'Strings')
    for k=1:length(Info.Strings)
        flag = writeString(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
        if ~flag
            return
        end
    end
end

flag = 1;

