function ShowColormaps(this, varargin)

%# function CNES
%# function CNESjet
%# function CNESgray
%# function NIWA1
%# function NIWA2
%# function JetSym
%# function ColorSea
%# function ColorEarth
%# function Haxby
%# function Becker
%# function Catherine
%# function Seismics
%# function SeismicsSym

[varargin, DirCpt] = getPropertyValue(varargin, 'DirCpt', []); %#ok<ASGLU>

if isempty(DirCpt)
    M = 0;
else
    listeCpt = listeFicOnDir(DirCpt, '.cpt');
    M = length(listeCpt);
end

fig1 = figure(...
    'NumberTitle', 'off', ...
    'name',        'SonarScope Colormaps - Implicit colormaps',...
    'Position',    centrageFig(Inf, Inf, 'colDepMin', 300, 'ligDepMin', 300));

fig2 = figure(...
    'NumberTitle', 'off', ...
    'name',        ['SonarScope Colormaps - GMT colormaps found in "' DirCpt '"'],...
    'Position',    centrageFig(Inf, Inf, 'colDepMin', 300, 'ligDepMin', 300));

strColormapIndex = this.strColormapIndex(2:end);
N = length(strColormapIndex);
NV = ceil(N/3);
k = 0;
for i=1:N
    k = k+1;
    Name = strColormapIndex{i};
    map = evalin('caller', [strColormapIndex{i} '(256)']);
    
    RGB = ind2rgb(1:size(map,1),map);
    figure(fig1)
    subplot(NV, 3, k);
    hi = imagesc(RGB);
    set(hi, 'ButtonDownFcn', @plot_colors, 'UserData', {map, strColormapIndex{i}})
    
    axis off
    title(Name)
end

NV = ceil(M/3);
k = 0;
for i=1:M
    k = k+1;
    [~, Name] = fileparts(listeCpt{i});
    
    [flag, map] = import_cpt(listeCpt{i}, 'NColors', 256, 'AskQuestion', 0);
    if ~flag
        continue
    end
    
    
    RGB = ind2rgb(1:size(map,1),map);
    figure(fig2)
    subplot(NV, 3, k);
    hi = imagesc(RGB);
    set(hi, 'ButtonDownFcn', @plot_colors, 'UserData', {map, listeCpt{i}})
    
    axis off
    title(Name, 'interpreter', 'none')
end

    function plot_colors(varargin)
        UserData = get(gcbo, 'UserData');
        map = UserData{1};
        Titre = UserData{2};
        figure;
        subplot(2,1,1)
        rgbplot(map); grid on; title(['RGB  -  ' Titre]);
        legend({'Red' ; 'Green' ; 'Blue'}, 'Interpreter', 'none')
        subplot(2,1,2)
        rgbplot(rgb2hsv(map)); grid on; title(['HSV  -  ' Titre]);
        legend({'Hue' ; 'Saturation' ; 'Value'}, 'Interpreter', 'none')
    end
end
