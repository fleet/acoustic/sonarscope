
function [flag, this] = Sidescan_ExtractWaterColumn(this, Side, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = Sidescan_ExtractWaterColumn_unitaire(this(i), Side, varargin{:});
    if ~flag
        return
    end
    this(i) = that;
end


function [flag, this] = Sidescan_ExtractWaterColumn_unitaire(this, Side, varargin)

[varargin, suby]  = getPropertyValue(varargin, 'suby',  []);
[varargin, Marge] = getPropertyValue(varargin, 'Marge', 0); %#ok<ASGLU>

if isempty(suby)
    suby = 1:length(this.y);
end

%% Controles

if isempty(this.Sonar.Height)
    str1 = 'Il n''y a pas de hauteur d�tect�e dans cette image.';
    str2 = 'No detected height in this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Cr�ation de l'image

if Side == 1 % Babord
    subx = find(this.x < 0);
    subx = fliplr(subx);
else % Tribord
    subx = find(this.x > 0);
end

this.Sonar.Height = my_interpUsual(this.Sonar.Height(:));
this.Sonar.Height = my_interpUsual(this.Sonar.Height, 'nearest', 'extrap');

switch this.GeometryType
    case cl_image.indGeometryType('PingRange')
        resol = this.Sonar.RawDataResol;
        xInSamples = this.x(subx) / resol;
        HInSamples = floor(abs(this.Sonar.Height(suby) / resol));
    case cl_image.indGeometryType('PingSamples')
        xInSamples = this.x(subx);
        HInSamples = floor(abs(this.Sonar.Height(suby)));
    otherwise
        return
end

HMax = max(HInSamples);
subx = subx(abs(xInSamples) <= HMax);
nbCol = length(subx);
if nbCol == 0
    flag = 0;
    my_warndlg('Height does not seem to be computed.', 1)
    return
end
nbRows = length(suby);
J = NaN(nbRows, nbCol, 'single');
for k=1:nbRows
    subx2 = 1:HInSamples(k);
    %     subx2 = 1:nbCol;
    J(k,subx2) = this.Image(suby(k), subx(subx2));
end

%% Cr�ation de l'instance

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, J);

%% Mise a jour de coordonnees

this = majCoordonnees(this, 1:nbCol, suby);
this.TagSynchroX = strrep(this.TagSynchroX, ' - D', '');
this.TagSynchroX = [this.TagSynchroX ' - S'];
this.GeometryType = cl_image.indGeometryType('PingSamples');
this.x = 1:nbCol;
SonarDescription = cl_sounder('Sonar.Ident', 25);
this = set(this, 'SonarDescription', SonarDescription);
this.Sonar.Height = -HInSamples;
this.XLim = compute_XYLim(this.x);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

if Side == 1
    this = update_Name(this, 'Append', 'WCPort');
else
    this = update_Name(this, 'Append', 'WCStarbord');
end

flag = 1;
