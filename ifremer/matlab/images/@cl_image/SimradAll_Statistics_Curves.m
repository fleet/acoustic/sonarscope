% TODO : a d�composer en 3 : BS, DiagTx et autre

function [flag, this, Fig] = SimradAll_Statistics_Curves(this, indImage, listeFicAll, bins, Selection, CourbeBias, nomCourbeBase, ...
    Comment, Coul, nbPMin, SelectMode, SelectSwath, SelectFM, MeanCompType, LayerMaskIn, valMask, ...
    SameReflectivityStatus, InfoCompensationCurve, varargin)

% [varargin, SonarStatus] = getPropertyValue(varargin, 'SonarStatus', []);
% [varargin, nomFicBS]    = getPropertyValue(varargin, 'nomFicBS', []);
[varargin, flagCleanCurves] = getPropertyValue(varargin, 'CleanCurves',    [1 1]);
[varargin, Mute]            = getPropertyValue(varargin, 'Mute',           0);
[varargin, flagMarcRoche]   = getPropertyValue(varargin, 'flagMarcRoche',  0);
[varargin, nomDirMatFiles]  = getPropertyValue(varargin, 'ExportMatFiles', []); %#ok<ASGLU>

useParallel = get_UseParallelTbx;

Tag                 = [];
CourbesStatistiques = [];
Fig                 = [];

%% Set the bins for stats

stepBins = NaN(size(bins));
for k=1:length(bins)
    if length(bins{k}) == 1 % cas d'un EM2040S o� l'utilisateur a s�l�ctionn� TxBeamIndex comme layer conditionnel
        stepBins(k) = 1;
    else
        stepBins(k) = bins{k}(2) - bins{k}(1);
    end
end

%% Recherche des layers synchronis�s

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

% Ajout JMA le 05/12/2018 pour �viter la question du choix du layer pour chaque fichier .all
subSameDataTypeAsTheCurrentOne = find((DataTypes == this(indImage).DataType) & (indLayers(:) ~= indImage));
DataTypes(subSameDataTypeAsTheCurrentOne)  = [];
indLayers(subSameDataTypeAsTheCurrentOne)  = []; %#ok<NASGU>
nomsLayers(subSameDataTypeAsTheCurrentOne) = [];
% Fin ajout JMA le 05/12/2018

% [~, sub] = unique(DataTypes);
% DataTypes  = DataTypes(sub);
% indLayers  = indLayers(sub);
% nomsLayers = nomsLayers(sub);

%% Lecture des fichiers de compensation si il y en a

if isempty(InfoCompensationCurve) || isempty(InfoCompensationCurve.FileName)
    bilan         = [];
    ModeDependant = 0;
    UseModel      = [];
    PreserveMeanValueIfModel = [];
else
    FileNameCompensation = InfoCompensationCurve.FileName;
    if ischar(FileNameCompensation)
        FileNameCompensation = {FileNameCompensation};
    end
    ModeDependant = InfoCompensationCurve.ModeDependant;
    
    for k2=1:length(FileNameCompensation)
        CourbeConditionnelle = load_courbesStats(FileNameCompensation{k2});
        if isempty(CourbeConditionnelle)
            flag = 0;
            return
        end
        bilan{k2} = CourbeConditionnelle.bilan; %#ok<AGROW>
        if isfield(InfoCompensationCurve, 'UseModel') && ~isempty(InfoCompensationCurve.UseModel)
            UseModel(k2) = InfoCompensationCurve.UseModel(k2); %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = InfoCompensationCurve.PreserveMeanValueIfModel(k2); %#ok<AGROW>
        else
            UseModel(k2) = 0; %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = 0; %#ok<AGROW>
        end
    end
end

%% Loop on files

if isempty(LayerMaskIn)
    nbValMasque = 1;
else
	nbValMasque = length(valMask);
end

this(indImage) = Virtualmemory2Ram(this(indImage));
    
N = length(listeFicAll);
CMasque = cell(1,N);
str1 = 'Calcul des courbes sur les fichiers .all';
str2 = 'Processing the statistics on .all files';
if useParallel && (N > 1)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (k=1:N, useParallel)
        fprintf('\n%d / %d : %s\n', k, N,  listeFicAll{k});
        [flag, C] = SimradAll_Statistics_Curves_unitaire(this, listeFicAll{k}, indImage, nomsLayers, DataTypes, ...
            bilan, ModeDependant, Selection, bins, UseModel, PreserveMeanValueIfModel, CourbeBias, nomCourbeBase, ...
            Comment, Coul, SelectMode, SelectSwath, SelectFM, MeanCompType, LayerMaskIn, valMask, ...
            SameReflectivityStatus, Tag, stepBins, nomDirMatFiles, ...
            'isUsingParfor', 1, 'Mute', Mute, 'flagMarcRoche', flagMarcRoche);
        if flag
            CMasque{k} = C;
        end
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        fprintf('\n%d / %d : %s\n', k, N,  listeFicAll{k});
        [flag, C] = SimradAll_Statistics_Curves_unitaire(this, listeFicAll{k}, indImage, nomsLayers, DataTypes, ...
            bilan, ModeDependant, Selection, bins, UseModel, PreserveMeanValueIfModel, CourbeBias, nomCourbeBase, ...
            Comment, Coul, SelectMode, SelectSwath, SelectFM, MeanCompType, LayerMaskIn, valMask, ...
            SameReflectivityStatus, Tag, stepBins, nomDirMatFiles, 'Mute', Mute, 'flagMarcRoche', flagMarcRoche);
        if flag
            CMasque{k} = C;
        end
    end
    my_close(hw, 'MsgEnd')
end

flag = 0;
for k=1:N
    if ~isempty(CMasque{k})
        flag = 1;
    end
end
if ~flag
    my_warndlg('No curve was computed. Perhaps you can get an error message if you do not use the //Tbx', 1);
    return
end

%% Affichage des courbes

[flag, FigAllCurves] = plot_CourbesStats([CMasque{:,:}], 'Stats', 0);
% my_close(FigAllCurves);

if numel(CMasque) == 0
    flag = 0;
    return
end

%% Nettoyage

Colors = 'brgmkc';
CourbesSectors = [];
CourbesSectorsBiais = [];
FigCleanedCurves = [];

% TODO : nbValMasque limit� � une valeur pour le moment dans params_ALL_Statistics_Curves car ce qui suit ne fonctionne pas si plusieurs valeurs de masque

for k=1:nbValMasque
    if nbValMasque ~= 1
        str1 = sprintf('Nettoyage courbes r�gion %d', k);
        str2 = sprintf('Curves cleaning for region %d', k);
        Title = Lang(str1,str2);
    else
        Title = [];
    end
    
    CourbesStatistiques = [];
    for k2=1:size(CMasque,2)
        if ~isempty(CMasque{k,k2})
            if isempty(CourbesStatistiques)
                CourbesStatistiques = CMasque{k,k2};
            else
                CourbesStatistiques(end+1) = CMasque{k,k2}; %#ok<AGROW>
            end
        end
    end
    
    %% Visualisation des courbes nettoy�es
    
    if flagCleanCurves(1)
        [flag, CourbesStatistiquesOut] = CourbesStatsClean(CourbesStatistiques, 'Stats', 0, 'NoPlot', 'Title', Title);
        if ~flag || isempty(CourbesStatistiquesOut)
            continue
        end
    else
        CourbesStatistiquesOut = CourbesStatistiques;
    end
    
    %% Moyennage des courbes
    
    C = CourbesStatistiquesOut;
    [flag, CourbesStatistiquesOut, CourbesStatistiquesBiaisOut] = CourbesStatsUnionWithHoles(C, ...
        'nbPMin', nbPMin, 'nomCourbe', [nomCourbeBase ' - Mean curve'], 'MeanCompType', MeanCompType);
    if ~flag
        continue
    end
    
    %% Nettoyage de la courbe moyenn�e
    
    if flagCleanCurves(2)
        if nbValMasque ~= 1
            str1 = sprintf('Nettoyage de la courbe moyenn�e de la r�gion %d', k);
            str2 = sprintf('Cleaning averaged curve for region %d', k);
            Title = Lang(str1,str2);
        else
            Title = [];
        end
        [flag, CourbesStatistiquesOut] = CourbesStatsClean(CourbesStatistiquesOut, 'Stats', 0, 'NoPlot', 'Title', Title);
        if ~flag
            continue
        end
    else
        [flag, CourbesStatistiquesOut] = CourbesStatsCleanAuto(CourbesStatistiquesOut, 'Stats', 0, 'NoPlot', 'Title', Title);
        if ~flag
            continue
        end
    end
    
    %% Visualisation de la courbe moyenn�e
    
    [~, FigCleanedCurves] = plot_CourbesStats(CourbesStatistiquesOut, 'Stats', 0, 'Fig', FigCleanedCurves, 'Coul', Colors(k), 'LineWidth', 3);
    
    %% Sauve la courbe dans l'image
    
    if (length(CourbesStatistiquesOut.bilan{1}(1).DataTypeConditions) == 1) && (nbValMasque > 1)
        CourbesStatistiquesOut.bilan{1}.DataTypeConditions{2} = 'Mask';
        CourbesStatistiquesOut.bilan{1}.Support{2} = 'Layer';
        CourbesStatistiquesOut.bilan{1}.numVarCondition = valMask(k);
        CourbesStatistiquesOut.bilan{1}.nomVarCondition = 'Mask';
        CourbesStatistiquesOut.bilan{1}.NY(2) = nbValMasque;
        
        CourbesStatistiquesBiaisOut.bilan{1}.DataTypeConditions{2} = 'Mask';
        CourbesStatistiquesBiaisOut.bilan{1}.Support{2} = 'Layer';
        CourbesStatistiquesBiaisOut.bilan{1}.numVarCondition = valMask(k);
        CourbesStatistiquesBiaisOut.bilan{1}.nomVarCondition = 'Mask';
        CourbesStatistiquesBiaisOut.bilan{1}.NY(2) = nbValMasque;
        if isempty(CourbesSectors)
            clear CourbesSectors CourbesSectorsBiais
            CourbesSectors = CourbesStatistiquesOut;
            CourbesSectorsBiais = CourbesStatistiquesBiaisOut;
        else
            CourbesSectors.bilan{1}(k) = CourbesStatistiquesOut.bilan{1};
            CourbesSectorsBiais.bilan{1}(k) = CourbesStatistiquesBiaisOut.bilan{1};
        end
    else
        if isempty(CourbesSectors)
            clear CourbesSectors CourbesSectorsBiais
            CourbesSectors = CourbesStatistiquesOut;
            CourbesSectorsBiais = CourbesStatistiquesBiaisOut;
        else
            CourbesSectors.bilan{1}(k) = CourbesStatistiquesOut.bilan{1};
            CourbesSectorsBiais.bilan{1}(k) = CourbesStatistiquesBiaisOut.bilan{1};
        end
    end
    
    %     this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiquesOut(:)];
end

%% Sauve la courbe dans l'image

if isempty(CourbesSectorsBiais)
%     this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiques(:); CourbesSectors];
    this(indImage).CourbesStatistiques = CourbesStatsConcatenate(this(indImage).CourbesStatistiques(:), CourbesStatistiques(:));
    this(indImage).CourbesStatistiques = CourbesStatsConcatenate(this(indImage).CourbesStatistiques(:), CourbesSectors);
else
%     this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiques(:); CourbesSectorsBiais; CourbesSectors];
    this(indImage).CourbesStatistiques = CourbesStatsConcatenate(this(indImage).CourbesStatistiques(:), CourbesStatistiques(:));
    this(indImage).CourbesStatistiques = CourbesStatsConcatenate(this(indImage).CourbesStatistiques(:), CourbesSectorsBiais);
    this(indImage).CourbesStatistiques = CourbesStatsConcatenate(this(indImage).CourbesStatistiques(:), CourbesSectors);
end

%{
% TODO : on propage le nettoyage � la courbe de compensation mais on ne
% r�cup�re pas le filtrage
% Lorsque le calcul des courbes sera refondu, on ne s'emb�tera pas avec
% cette courbe suppl�mentaire, la valeur moyenne sera stock�e dans
% l'instance et on aura un getter sp�cifique pour la courbe de
% compensation. Ce qui suit devra �tre supprim�
for k=1:length(CourbesStatistiquesBiaisOut.bilan{1})
xClean = CourbesStatistiquesBiaisOut.bilan{1}(k).x;
x = CourbesStatistiquesBiaisOut.bilan{1}(k).x;
[~, ia] = setdiff(x, xClean);
CourbesStatistiquesBiaisOut.bilan{1}(k).x(ia) = [];
CourbesStatistiquesBiaisOut.bilan{1}(k).y(ia) = [];
CourbesStatistiquesBiaisOut.bilan{1}(k).ymedian(ia) = [];
CourbesStatistiquesBiaisOut.bilan{1}(k).sub = 1:length(this(indImage).CourbesStatistiques(end).bilan{1}(k).x);
CourbesStatistiquesBiaisOut.bilan{1}(k).nx(ia) = [];
CourbesStatistiquesBiaisOut.bilan{1}(k).std(ia) = [];
end
%}

nbCourbes = length(this(indImage).CourbesStatistiques);

if isempty(FigAllCurves) || ~ishandle(FigAllCurves)
    FigAllCurves = FigUtils.createSScFigure;
end
plotCourbesStats(this(indImage), 'sub', nbCourbes, 'Stats', 0, 'LineWidth', 3, 'Fig', FigAllCurves, 'Coul', 'k'); % Superposition sur les autres plots
Fig = plotCourbesStats(this(indImage), 'sub', nbCourbes, 'Stats', 0, 'LineWidth', 3); % Superposition sur les autres plots

Fig = [FigCleanedCurves, FigAllCurves, Fig];

%% On met la courbe de compensation dans la pile des courbes

% this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiquesBiaisOut(:)];
