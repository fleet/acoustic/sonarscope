% TODO : � d�composer en 3 : BS, DiagTx et autre

function [flag, this, indCourbeBS, Fig] = SimradAll_Statistics_CurvesBS(this, indImage, listeFicAll, bins, Selection, CourbeBias, nomCourbeBase, Comment, ...
    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, MeanCompType, LayerMaskIn, valMask, SameReflectivityStatus, InfoCompensationCurve, varargin)

[varargin, Carto]           = getPropertyValue(varargin, 'Carto',         []); 
[varargin, flagCleanCurves] = getPropertyValue(varargin, 'CleanCurves',   [1 1 1]);
[varargin, XLim]            = getPropertyValue(varargin, 'XLim',    	  []);
[varargin, flagMarcRoche]   = getPropertyValue(varargin, 'flagMarcRoche', 0);
[varargin, MaskOverlapingIncidenceAngle] = getPropertyValue(varargin,   'MaskOverlapingIncidenceAngle', 0); %#ok<ASGLU>

Fig         = [];
alpha       = [];
indCourbeBS = [];
% Tag       = [];

useParallel = get_UseParallelTbx;
flagPlot = any(flagCleanCurves);

%% Set the bins for stats

stepBins = NaN(size(bins));
for k=1:length(bins)
    if length(bins{k}) == 1 % cas d'un EM2040S o� l'utilisateur a s�l�ctionn� TxBeamIndex comme layer conditionnel
        stepBins(k) = 1;
    else
        stepBins(k) = bins{k}(2) - bins{k}(1);
    end
end

%% Recherche des layers synchronis�s

if SameReflectivityStatus
    [flag, ~, nomsLayersSynchronized, DataTypesSynchronized] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
    if ~flag
        return
    end
else
    nomsLayersSynchronized = [];
    DataTypesSynchronized  = [];
end

%% Lecture des fichiers de compensation si il y en a

DataTypeConditionsComp = {};
if isempty(InfoCompensationCurve) || isempty(InfoCompensationCurve.FileName)
    bilan         = [];
    ModeDependant = 0;
    UseModel      = [];
    PreserveMeanValueIfModel = [];
else
    FileNameCompensation = InfoCompensationCurve.FileName;
    if ischar(FileNameCompensation)
        FileNameCompensation = {FileNameCompensation};
    end
    ModeDependant = InfoCompensationCurve.ModeDependant;
    
    for k2=1:length(FileNameCompensation)
        CourbeConditionnelle = load_courbesStats(FileNameCompensation{k2});
        if isempty(CourbeConditionnelle)
            flag = 0;
            return
        end
        bilan{k2} = CourbeConditionnelle.bilan; %#ok<AGROW>

        for k3=1:length(bilan{k2})
            for k4=1:length(bilan{k2}{k3}(1).DataTypeConditions)
                DataTypeConditionsComp{end+1} = bilan{k2}{k3}(1).DataTypeConditions{k4}; %#ok<AGROW>
            end
        end
        if isfield(InfoCompensationCurve, 'UseModel') && ~isempty(InfoCompensationCurve.UseModel)
            UseModel(k2) = InfoCompensationCurve.UseModel(k2); %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = InfoCompensationCurve.PreserveMeanValueIfModel(k2); %#ok<AGROW>
        else
            UseModel(k2) = 0; %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = 0; %#ok<AGROW>
        end
    end
end

%% Loop on files

LayerReflectivity = this(indImage);
LayerReflectivity = Virtualmemory2Ram(LayerReflectivity); % D�j� en RAM mais on ne sait jamais, �a peut faire gagner du temps

N = length(listeFicAll);
CourbesStatistiques = cell(N,1);
str1 = 'Calcul des courbes sur les fichiers .all';
str2 = 'Processing the statistics on .all files';
msgWaitbar = Lang(str1,str2);

if ~flagPlot && useParallel && (N > 2)
    % TODO : faire en sorte qu'on puisse faire parfor k=1:N
    [hw, DQ] = create_parforWaitbar(N, msgWaitbar);
    k = 1;
    send(DQ, k);
    [flag, C, Carto, alpha, Fig] = SimradAll_Statistics_CurvesBS_unitaire(LayerReflectivity, listeFicAll{k}, Selection, ...
        DataTypeConditionsComp, Carto, alpha, Comment, Fig, LayerMaskIn, valMask, CourbeBias, Coul, SelectMode, SelectSwath, SelectFM, ...
        bilan, ModeDependant, UseModel, PreserveMeanValueIfModel, nomsLayersSynchronized, DataTypesSynchronized, ...
        MaskOverlapingIncidenceAngle, SameReflectivityStatus, nomCourbeBase, bins, stepBins, MeanCompType, flagPlot, flagMarcRoche);
    if flag
        CourbesStatistiques{k} = C;
    end
    parfor k=2:N
        send(DQ, k);
        [flag, C] = SimradAll_Statistics_CurvesBS_unitaire(LayerReflectivity, listeFicAll{k}, Selection, ...
            DataTypeConditionsComp, Carto, alpha, Comment, Fig, LayerMaskIn, valMask, CourbeBias, Coul, SelectMode, SelectSwath, SelectFM, ...
            bilan, ModeDependant, UseModel, PreserveMeanValueIfModel, nomsLayersSynchronized, DataTypesSynchronized, ...
            MaskOverlapingIncidenceAngle, SameReflectivityStatus, nomCourbeBase, bins, stepBins, MeanCompType, flagPlot, flagMarcRoche, ...
            'isUsingParfor', true);
        if flag
            CourbesStatistiques{k} = C;
        end
    end
else
    hw = create_waitbar(msgWaitbar, 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        [flag, C, Carto, alpha, Fig] = SimradAll_Statistics_CurvesBS_unitaire(LayerReflectivity, listeFicAll{k}, Selection, ...
            DataTypeConditionsComp, Carto, alpha, Comment, Fig, LayerMaskIn, valMask, CourbeBias, Coul, SelectMode, SelectSwath, SelectFM, ...
            bilan, ModeDependant, UseModel, PreserveMeanValueIfModel, nomsLayersSynchronized, DataTypesSynchronized, ...
            MaskOverlapingIncidenceAngle, SameReflectivityStatus, nomCourbeBase, bins, stepBins, MeanCompType, flagPlot, flagMarcRoche);
        if flag
            CourbesStatistiques{k} = C;
        end
    end
end
my_close(hw, 'MsgEnd');
CourbesStatistiques = [CourbesStatistiques{:}];
if isempty(CourbesStatistiques)
    my_warndlg('No curve could be computed. Check if the mask is correct', 0)
    flag = 0;
    return
end

%% Nettoyage des courbes

if flagCleanCurves(1)
    [flag, CourbesStatistiquesOut] = CourbesStatsClean(CourbesStatistiques, 'Stats', 0, 'NoPlot');
    if ~flag
        return
    end
else
    CourbesStatistiquesOut = CourbesStatistiques;
end

%% Sauvegarde de la courbe nettoy�e dans l'image

CourbesStatistiquesOutAlreadyHere = this(indImage).CourbesStatistiques(:);
if isempty(CourbesStatistiquesOutAlreadyHere)
    this(indImage).CourbesStatistiques = CourbesStatistiquesOut(:);
else
    try
        this(indImage).CourbesStatistiques = [CourbesStatistiquesOutAlreadyHere; CourbesStatistiquesOut(:)];
    catch
        for k=1:length(CourbesStatistiquesOut)
             this(indImage).CourbesStatistiques(end+1).bilan = CourbesStatistiquesOut(k).bilan;
        end
    end
end

%% S�lection des courbes � conserver

%{
nbNewCourbes = length(CourbesStatistiques);
this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiques(:)];
nbCourbes = length(this(indImage).CourbesStatistiques);
subC = (nbCourbes-nbNewCourbes+1):nbCourbes; % Bidouille � cause de cette #@{[] de courbe "Compens)
[flag, sub] = selectionCourbesStats(this(indImage), 'Tag', Tag, 'Selection', 'Multiple', 'InitialValue', subC);
if ~flag
    return
end
%}

%% Visualisation des courbes nettoy�es

my_close(Fig);
n = length(this(indImage).CourbesStatistiques);
sub = (n-length(CourbesStatistiquesOut)+1):n;
if flagPlot
    Fig = plotCourbesStats(this(indImage), 'sub', sub, 'Stats', 0);
end

%% Moyennage des courbes

CourbesStatistiques = CourbesStatistiquesOut;
[flag, CourbesStatistiquesOut, CourbesStatistiquesBiaisOut] = CourbesStatsUnionWithHoles(CourbesStatistiques, ...
    'nbPMin', nbPMin, 'nomCourbe', [nomCourbeBase ' - Mean curve'], 'MeanCompType', MeanCompType);
if ~flag
    return
end
try
    this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiquesOut(:)];
catch % Pour tester bug rencontr� par Ridha
    this(indImage).CourbesStatistiques(:)
    CourbesStatistiquesOut(:)
end

%% Nettoyage des courbes

if (N > 1) && flagCleanCurves(2)
    CourbesStatistiques = CourbesStatistiquesOut;
    [flag, CourbesStatistiquesOut] = CourbesStatsClean(CourbesStatistiques, 'Stats', 0, 'NoPlot');
    if ~flag
        return
    end
    
    %% On met la courbe nettoy�e dans la pile des courbes
    
    try
        this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiquesOut(:)];
    catch
        for k=1:length(CourbesStatistiquesOut)
             this(indImage).CourbesStatistiques(end+1).bilan = CourbesStatistiquesOut(k).bilan;
        end
    end
%     indCourbeBS = length(this(indImage).CourbesStatistiques);
    
    %% Affichage de la courbe moyenn�e nettoy�e
    
    nbCourbes = length(this(indImage).CourbesStatistiques);
    % plotCourbesStats(this(indImage), 'sub', nbCourbes, 'Stats', 0, 'LineWidth', 3, 'Fig', Fig, 'Coul', 'k'); % Superposition sur les autres plots
    if flagPlot
        plotCourbesStats(this(indImage), 'sub', nbCourbes, 'Stats', 0, 'LineWidth', 3, 'Fig', Fig, 'Coul', 'k'); % Superposition sur les courbes nettoy�es
    end
end

%% On met la courbe de compensation dans la pile des courbes

indCourbeBS = length(this(indImage).CourbesStatistiques);
CourbesStatistiquesBiais = CourbesStatsInheritDataCleaning(CourbesStatistiquesOut, CourbesStatistiquesBiaisOut);
try
    this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiquesBiais(:)];
catch
    for k=1:length(CourbesStatistiquesBiais)
        this(indImage).CourbesStatistiques(end+1).bilan = CourbesStatistiquesBiais(k).bilan;
    end
end

%% Mod�lisation de la courbe de BS             

if flagCleanCurves(3)
    ComputeType = 3;
else
    ComputeType = 1;
end
this(indImage) = optim_BS(this(indImage), 'sub', indCourbeBS, 'ComputeType', ComputeType, 'XLim', XLim); % 1=GUI only, 2=background only, 3=background + GUI
if flagPlot
    Fig(end+1) = plotCourbesStats(this(indImage), 'sub', indCourbeBS, 'Stats', 0, 'Tag', 'BS', 'subTypeCurve', 1:2);
end

%% On calcule la courbe des r�sidus

%{
CourbesStatistiques = get_CourbesStatistiques(this(indImage), 'sub', indCourbeBS);
[flag, CourbesStatistiquesResiduals] = CourbesStatsCreateCurveFromResiduals(CourbesStatistiques);
if flag
    this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiquesResiduals(:)];
end
%}





function [flag, CourbesStatistiques, Carto, alpha, Fig] = SimradAll_Statistics_CurvesBS_unitaire(this, AllFileName, Selection, ...
    DataTypeConditionsComp, Carto, alpha, Comment, Fig, LayerMaskIn, valMask, CourbeBias, Coul, SelectMode, SelectSwath, SelectFM, ...
    bilan, ModeDependant, UseModel, PreserveMeanValueIfModel, nomsLayersSynchronized, DataTypesSynchronized, ...
    MaskOverlapingIncidenceAngle, SameReflectivityStatus, nomCourbeBase, bins, stepBins, MeanCompType, flagPlot, flagMarcRoche, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

CourbesStatistiques = [];

str1 = 'Traitement du fichier : ';
str2 = 'Processing : ';
fprintf('----------------------------------------------------------\n%s %s\n', Lang(str1,str2), AllFileName);

%% Lecture des layers

aKM = cl_simrad_all('nomFic', AllFileName);
if isempty(aKM)
    flag = 0;
    return
end

listeLayersSupplementaires = [];
DataTypeConditions = cl_image.strDataType(this.DataType);
for k1=1:length(Selection)
    if strcmp(Selection(k1).TypeVal, 'Layer')
        DataTypeConditions{end+1} = cl_image.strDataType{Selection(k1).DataType}; %#ok<AGROW>
    end
end

[listeCor, ~, flag] = getListeLayersSupplementaires([DataTypeConditions DataTypeConditionsComp]); % nomsLayersSynchronized
if ~flag
    return
end

DataTypeSynchronized = {};
for k1=1:length(DataTypesSynchronized)
    DataTypeSynchronized{k1} = cl_image.strDataType{DataTypesSynchronized(k1)}; %#ok<AGROW>
end

[listeSynchronized, ~, flag] = getListeLayersSupplementaires(DataTypeSynchronized, 'Names', nomsLayersSynchronized); % nomsLayersSynchronized
if ~flag
    return
end

listeLayersSupplementaires = [listeLayersSupplementaires listeCor listeSynchronized];
listeLayersSupplementaires = unique(listeLayersSupplementaires);

Version = version_datagram(aKM);
if isempty(Version)
    return
end
switch Version
    case 'V1'
        listeLayersSupplementaires = setdiff(listeLayersSupplementaires, [2 3 4 24 25 26]); % Modif JMA le 02/01/2019
        [c, Carto] = import_PingBeam_All_V1(aKM, 'ListeLayers', [2 3 4 24 25 26 listeLayersSupplementaires], 'Carto', Carto);
        if isempty(c)
            return
        end
    case 'V2'
        listeLayersSupplementaires(listeLayersSupplementaires == 14) = [];
        % TODO : v�rifier le "6" car j'ai fait un copi� coll� de V1
        % pour cette valeur
        ListeLayers = SelectionLayers_V1toV2([2 3 14 6 24 26 listeLayersSupplementaires]);
        [c, Carto] = import_PingBeam_All_V2(aKM, 'ListeLayers', ListeLayers, 'Carto', Carto);
        if isempty(c)
            return
        end
end
indLayer = findLayers(c, 'DataType', this.DataType);
for k1=1:length(Selection)
    Selection(k1).indLayer = findLayers(c, 'DataType', Selection(k1).DataType);
end

if MaskOverlapingIncidenceAngle
    indLayerIncidenceAngle = findIndLayerSonar(c, 1, 'strDataType', 'IncidenceAngle', 'WithCurrentImage', 1, 'Mute', 1);
    if ~isempty(indLayerIncidenceAngle)
        indTxBeamIndex = findIndLayerSonar(c, 1, 'strDataType', 'TxBeamIndex', 'WithCurrentImage', 1, 'Mute', 1);
        if strcmp(get(c(1).Sonar.Desciption, 'Sonar.Name'), 'EM3002D')
            c(indLayerIncidenceAngle) = MaskOverlapingIncidenceAngleEM3002(c(indLayerIncidenceAngle), c(indTxBeamIndex));
        end
    end
end

%% Compensation statistique (Ex : courbes de calibration)

if ~isempty(bilan) && (length(bilan) == 1) && iscell(bilan{1})
    bilan = bilan{1};
end
for k2=1:length(bilan)
    indLayerAsCurrentOne = findIndLayerSonar(c, 1, 'DataType', this.DataType, 'OnlyOneLayer', 'WithCurrentImage', 1);
    
    identLayersConditionCompensation = [];
    for k3=1:length(DataTypeConditionsComp)
        identDataType = cl_image.indDataType(DataTypeConditionsComp{k3});
        x = findIndLayerSonar(c, 1, 'DataType', identDataType, 'WithCurrentImage', 1);
        if isempty(x)
            str1 = sprintf('Le layer de type "%s" n''a pas �t� trouv�.', DataTypeConditionsComp{k3});
            str2 = sprintf('Layer of type "%s" was not found.', DataTypeConditionsComp{k3});
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        else
            identLayersConditionCompensation(k3) = x; %#ok<AGROW>
        end
    end
    identLayersConditionCompensation = unique(identLayersConditionCompensation); % Ajout JMA le 21/09/2022 pour Batch2 SPFE

    try

        my_breakpoint

        [d, flag] = compensationCourbesStats(c(indLayerAsCurrentOne), c(identLayersConditionCompensation), ...
            bilan, 'ModeDependant', ModeDependant, 'UseModel', UseModel(k2), ...
            'PreserveMeanValueIfModel', PreserveMeanValueIfModel(k2), 'flagMarcRoche', flagMarcRoche);
    catch % Beurk : il faut trouver pourquoi c'est tant�t l'un tant�t l'autre qui est la bonne recette 
        % Modif JMA le 06/11/2023

        my_breakpoint

        if iscell(bilan)
            bilan_k2 = bilan{k2};
        else
            bilan_k2 = bilan(k2);
        end
        [d, flag] = compensationCourbesStats(c(indLayerAsCurrentOne), c(identLayersConditionCompensation), ...
            bilan_k2, 'ModeDependant', ModeDependant, 'UseModel', UseModel(k2), ...
            'PreserveMeanValueIfModel', PreserveMeanValueIfModel(k2), 'flagMarcRoche', flagMarcRoche);
    end
    if ~flag
        return
    end
    c(indLayerAsCurrentOne) = d;
end

%% Mise en conformit� de la r�flectivit�

[~, indLayerRange] = findAnyLayerRange(c, indLayer);
[flag, d, alpha] = SonarBestBSConstructeur(c, indLayer, 'Range', c(indLayerRange),  'Absoption_dB/km', alpha, 'Mute', 1, ...
    'ChangeInsonifiedArea', SameReflectivityStatus);
if ~flag
    return
end
c(indLayer) = d;
clear d
Tag = 'BS';

%% Calcul des stats

[~, NomFic] = fileparts(c(indLayer).InitialFileName);
nomCourbe = sprintf('%s on %s', nomCourbeBase, NomFic);

GeometryType = this.GeometryType;
if isempty(LayerMaskIn)
    if strcmp(Tag, 'DiagTx') % TODO : faire sans doute le m�me type de truc pour le BS
        [flag, c] = SonarDiagTxCompute(c, indLayer, [], [], NomFic, 'askQuestionSignatureConfSonar', 0);
        if ~flag
            return
        end
    else
        X = courbesStats(c(indLayer), Tag, ...
            nomCourbe, Comment{1}, [], ...
            c, Selection, 'stepBins', stepBins, ...
            'CourbeBias', CourbeBias, 'EditBias', 1, ...
            'GeometryType', cl_image.strGeometryType{GeometryType}, ...
            'Sampling', 1, 'Coul', Coul, 'nbPMin', 2, ...
            'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, ...
            'MeanCompType', MeanCompType, 'NomFic', NomFic);
        if isempty(X)
            return
        else
            c(indLayer) = X;
        end
    end
    c_CourbesStatistiques = c(indLayer).CourbesStatistiques;
    CMasque = c_CourbesStatistiques(end);
    %         if isempty(CourbesStatistiques)
    %             CourbesStatistiques = CMasque;
    %         else
    %             CourbesStatistiques(end+1) = CMasque; %#ok<AGROW>
    %         end
    %         c(indLayer).CourbesStatistiques = CourbesStatistiques;
    CourbesStatistiques = CMasque;
    
    if flagPlot
        if isempty(Fig) || ~ishandle(Fig)
            Fig = FigUtils.createSScFigure;
            centrageFig(Inf, Inf, 'Fig', Fig)
        end
        %             plotCourbesStats(c(indLayer), 'sub', length(CourbesStatistiques), 'Stats', 0, 'Fig', Fig);
        [~, Fig] = plot_CourbesStats(CourbesStatistiques, 'Stats', 0, 'Fig', Fig); % nemestra
    end
else
    [flag, LayerMask] = sonarMosaiqueInv(c, indLayer, LayerMaskIn);
    if ~flag
        return
    end
    if isnan(LayerMask.StatValues.Min)
        return
    end
    clear CMasque
    nbValMasque = length(valMask);
    flagCalculCourbeOK = 0;
    for kValMasque=1:nbValMasque
        C = courbesStats(c(indLayer), Tag, ...
            nomCourbe, Comment{1}, [], ...
            c, Selection, 'bins', bins, ...
            'LayerMask', LayerMask, 'valMask', valMask(kValMasque), 'GroupMasques', 1, ...
            'CourbeBias', CourbeBias, 'EditBias', 1, ...
            'GeometryType', cl_image.strGeometryType{GeometryType}, ...
            'Sampling', 1, 'Coul', Coul, 'nbPMin', 2, ...
            'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, 'MeanCompType', MeanCompType);
        if isempty(C)
            continue
        end
        c(indLayer) = C;
        c_CourbesStatistiques = c(indLayer).CourbesStatistiques;
        CMasque(kValMasque) = c_CourbesStatistiques(end); %#ok<AGROW>
        flagCalculCourbeOK = 1;
    end
    if ~flagCalculCourbeOK
        return
    end
    if nbValMasque > 1
        CMasque = CourbesStatsUnion(CMasque, 'nbPMin', 0, 'nomCourbe', nomCourbe);
    end
    %         if isempty(CourbesStatistiques)
    %             CourbesStatistiques = CMasque(end);
    %         else
    %             CourbesStatistiques(end+1) = CMasque(end); %#ok<AGROW>
    %         end
    %         c(indLayer).CourbesStatistiques = CourbesStatistiques;
    CourbesStatistiques = CMasque(end);
    
    if flagPlot
        if isempty(Fig) || ~ishandle(Fig)
            Fig = FigUtils.createSScFigure;
            centrageFig(Inf, Inf, 'Fig', Fig)
        end
        %             plotCourbesStats(c(indLayer), 'sub', length(CourbesStatistiques{k}), 'Stats', 0, 'Fig', Fig);
        [~, Fig] = plot_CourbesStats(CourbesStatistiques, 'Stats', 0, 'Fig', Fig); % nemestra
    end
end