function [flag, this] = SimradAll_Statistics_CurvesTxDiag(this, listeFicAll, bins, Selection, CourbeBias, nomCourbeBase, ...
    Comment, Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, ...
    MeanCompType, LayerMaskIn, valMask, SameReflectivityStatus, nomFicBS, varargin)

Carto               = [];
Fig                 = [];
CourbesStatistiques = [];

%% 

N = length(listeFicAll);
str1 = 'Calcul des courbes sur les fichiers .all en cours';
str2 = 'Processing the statistics on .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    str1 = 'Traitement du fichier : ';
    str2 = 'Processing : ';
    fprintf('----------------------------------------------------------\n%s %s\n', Lang(str1,str2), listeFicAll{k});
    
    %% Lecture des layers
    
    aKM = cl_simrad_all('nomFic', listeFicAll{k});
    if isempty(aKM)
        continue
    end
    
    listeLayersSupplementaires = [];
    %     DataTypeLayersCompensation = [];
    DataTypeConditions = cl_image.strDataType(this.DataType);
    for k1=1:length(Selection)
        if strcmp(Selection(k1).TypeVal, 'Layer')
            DataTypeConditions{end+1} = cl_image.strDataType{Selection(k1).DataType}; %#ok<AGROW>
        end
    end
    [listeCor, ~, flag] = getListeLayersSupplementaires(DataTypeConditions);
    if ~flag
        return
    end
    listeLayersSupplementaires = [listeLayersSupplementaires listeCor]; %#ok<AGROW>
    listeLayersSupplementaires = unique(listeLayersSupplementaires);
    
    Version = version_datagram(aKM);
    if isempty(Version)
        continue
    end
    switch Version
        case 'V1'
%             switch SousVersion
%                 case 'F'
%                     listeLayersSupplementaires = setdiff(listeLayersSupplementaires, [2 3 4 6 24 26 36 40 41 43 47]);
%                     [c, Carto] = import_PingBeam_All_V1(aKM, 'ListeLayers', [2 3 4 6 24 26 listeLayersSupplementaires 43 47], 'Carto', Carto); % 43 : Angle d'incidence si jamais la courbe de BS a �t� calcul�e en fonction de cet angle
%                 case 'f'
                    listeLayersObligateoires = [2 3 4 24 25 26 36 40 41 43 47];
                    listeLayersSupplementaires = setdiff(listeLayersSupplementaires, listeLayersObligateoires);
                    [c, Carto] = import_PingBeam_All_V1(aKM, 'ListeLayers', [listeLayersObligateoires listeLayersSupplementaires], 'Carto', Carto); % 43 : Angle d'incidence si jamais la courbe de BS a �t� calcul�e en fonction de cet angle
%             end
            if isempty(c)
                continue
            end
        case 'V2'
            listeLayersSupplementaires(listeLayersSupplementaires == 14) = [];
            % TODO : v�rifier le "6" car j'ai fait un copi� coll� de V1 pour cette valeur
            ListeLayers = SelectionLayers_V1toV2([2 3 14 6 24 26 36 40 41 43 47 listeLayersSupplementaires]);
            [c, Carto] = import_PingBeam_All_V2(aKM, 'ListeLayers', ListeLayers, 'Carto', Carto);
            if isempty(c)
                continue
            end
    end
    indLayer = findLayers(c, 'DataType', this.DataType);
    
    for k1=1:length(Selection)
        if Selection(k1).DataType == 4 % Ajout JMA le 02/01/2019 pour .all V1 pour corriger le flou entre TxAngle et BeamPointingAngle
            indLayerAngle = findLayers(c, 'DataType', Selection(k1).DataType);
            if isempty(indLayerAngle)
                indexDataType = cl_image.indDataType('BeamPointingAngle');
                indLayerAngle = findLayers(c, 'DataType', indexDataType);
            end
            Selection(k1).indLayer = indLayerAngle;
        else
            Selection(k1).indLayer = findLayers(c, 'DataType', Selection(k1).DataType);
            if length(Selection(k1).indLayer) > 1
                Selection(k1).indLayer = Selection(k1).indLayer(1); % pas top mais �vite une erreur si l'utilisateur a plusieurs layers de ce type dans SSc
            end
        end
    end
    
     %% Mise en conformit� de la r�flectivit�
   
    [~, indLayerRange] = findAnyLayerRange(c, indLayer);
    [flag, d] = SonarBestBSConstructeur(c, indLayer, 'Range', c(indLayerRange), 'Mute', 1, ...
        'ChangeInsonifiedArea', SameReflectivityStatus);
    if ~flag
        return
    end
    c(indLayer) = d;
    clear d
    
    %% Mise en conformit� de la r�flectivit�
    
    [flag, c(indLayer)] = importCourbesStats(c(indLayer), nomFicBS, 'Tag', 'BS');
    if ~flag
        return
    end
    
    [flag, d] = SonarEtatDiagTx(c, indLayer, [], []); % TODO : passer tous ces subx et suby en PN/PV
    if ~flag
        continue
    end
    c(indLayer) = d;
    clear d
    Tag = 'DiagTx';
    
    %% Calcul des stats
    
    [~, NomFic] = fileparts(c(indLayer).InitialFileName);
    nomCourbe = sprintf('%s on %s', nomCourbeBase, NomFic);
    
    GeometryType = this.GeometryType;

    if isempty(LayerMaskIn)
        LayerMask = [];
        valMask   = 1;
    else
        [flag, LayerMask] = sonarMosaiqueInv(c, indLayer, LayerMaskIn);
        if ~flag
            continue
        end
        if isnan(LayerMask.StatValues.Min)
            continue
        end
    end

    nbValMasque = length(valMask);
    flagCurve = false;
    for kValMasque=1:nbValMasque
        X = courbesStats(c(indLayer), Tag, ...
            nomCourbe, Comment{1}, [], ...
            c, Selection, 'bins', bins, ...
            'LayerMask', LayerMask, 'valMask', valMask(kValMasque), 'GroupMasques', 1, ...
            'CourbeBias', CourbeBias, 'EditBias', 1, ...
            'GeometryType', cl_image.strGeometryType{GeometryType}, ...
            'Sampling', 1, 'Coul', Coul, 'nbPMin', 2, ...
            'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, ...
            'SelectMode2EM2040', SelectMode2EM2040, 'MeanCompType', MeanCompType);
        if isempty(X)
            continue
        end
        flagCurve = true;
        c(indLayer) = X;
        c_CourbesStatistiques = c(indLayer).CourbesStatistiques;
        CMasque(kValMasque) = c_CourbesStatistiques(end); %#ok<AGROW>
    end
    if ~flagCurve
        continue
    end
    if nbValMasque > 1
        CMasque = CourbesStatsUnion(CMasque, 'nbPMin', 0, 'nomCourbe', nomCourbe);
    end
    if isempty(CourbesStatistiques)
        %             CourbesStatistiques = CMasque(end-1);
        CourbesStatistiques = CMasque(end);
    else
        %             CourbesStatistiques(end+1) = CMasque(end-1); %#ok<AGROW>
        CourbesStatistiques(end+1) = CMasque(end); %#ok<AGROW>
    end
    c(indLayer).CourbesStatistiques = CourbesStatistiques;
    %         plotCourbesStats(c(indLayer), 'sub', 1:length(CourbesStatistiques(k)), 'Stats', 0, 'Fig', Fig);

    if isempty(Fig) || ~ishandle(Fig)
        Fig = FigUtils.createSScFigure;
    end
    plotCourbesStats(c(indLayer), 'sub', length(CourbesStatistiques), 'Stats', 0, 'Fig', Fig);
end
my_close(hw, 'MsgEnd');

%% Test si des courbes ont �t� calcul�es

CourbesStatistiques = CourbesStatistiques(:);
if isempty(CourbesStatistiques)
    str1 = 'Aucune courbe calcul�e sur ce lot de fichiers.';
    str2 = 'No curve could be computed on this set of files.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Nettoyage des courbes

str1 = 'Nettoyage des courbes.';
str2 = 'Data cleaning of the curves.';
[flag, CourbesStatistiquesOut] = CourbesStatsClean(CourbesStatistiques, 'Stats', 0, 'NoPlot', ...
    'Title', Lang(str1,str2));
if ~flag
    return
end

%% Sauvegarde de la courbe nettoy�e dans l'image

this.CourbesStatistiques = CourbesStatsConcatenate(this.CourbesStatistiques(:), CourbesStatistiquesOut(:));

%% Visualisation de la courbe nettoy�e

n = length(this.CourbesStatistiques);
sub = (n-length(CourbesStatistiquesOut)+1):n;
figCleaned = plotCourbesStats(this, 'sub', sub, 'Stats', 0, 'Mute', 1);
my_close(Fig)

nbCourbesAMoyenner = length(CourbesStatistiquesOut);
if nbCourbesAMoyenner > 1
    %% Moyennage des courbes
    
    CourbesStatistiques = CourbesStatistiquesOut;
    [flag, CourbesStatistiquesOut] = CourbesStatsUnionWithHoles(CourbesStatistiques, ...
        'nbPMin', nbPMin, 'nomCourbe', [nomCourbeBase ' - Mean curve'], 'MeanCompType', MeanCompType);
    if ~flag
        return
    end
    
    %% Nettoyage des courbes
    
    CourbesStatistiques = CourbesStatistiquesOut;
    str1 = 'Nettoyage de la courbe de synth�se.';
    str2 = 'Data cleaning of the averaged curve.';
    [flag, CourbesStatistiquesOut] = CourbesStatsClean(CourbesStatistiques, 'Stats', 0, 'NoPlot', ...
        'Title', Lang(str1,str2));
    if ~flag
        return
    end
end

%% On met la courbe de compensation dans la pile des courbes

this.CourbesStatistiques = CourbesStatsConcatenate(this.CourbesStatistiques(:), CourbesStatistiquesOut(:));

%% Affichage de la courbe moyenn�e nettoy�e

nbCourbes = length(this.CourbesStatistiques);
% plotCourbesStats(this, 'sub', nbCourbes, 'Stats', 0, 'LineWidth', 3, 'Fig', Fig, 'Coul', 'k'); % Superposition sur les autres plots
if nbCourbesAMoyenner == 1
    plotCourbesStats(this, 'sub', nbCourbes, 'Stats', 0, 'LineWidth', 3); % Superposition sur les autres plots
    my_close(figCleaned)
else
    plotCourbesStats(this, 'sub', nbCourbes, 'Stats', 0, 'LineWidth', 3, 'Fig', figCleaned, 'Coul', 'k'); % Superposition sur les autres plots
end

%% Mod�lisation des courbes de TxDiag

this = optim_DiagTx(this, 'Last');

%% Calcul de la courbe de compensation

this = create_compensationCurveFromNewBscorrAndAppliedDiagTx(this);
