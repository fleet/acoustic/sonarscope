function [flag, AngleSnellDescartes, IBA] = SnellDescartes(RxAngleEarth, Bathy)

AngleSnellDescartes = [];
IBA                 = [];

DT = cl_image.indDataType('RxAngleEarth');
flag = testSignature(RxAngleEarth, 'DataType', DT, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

DT = cl_image.indDataType('Bathymetry');
flag = testSignature(Bathy, 'DataType', DT, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

if isempty(RxAngleEarth.Sonar.BathyCel)
    flag = -1;
end

%% Rechercher le temps

tPings = RxAngleEarth.Sonar.Time.timeMat;
tSSP   = RxAngleEarth.Sonar.BathyCel.TimeStartOfUse.timeMat;
[~, ordre] = unique(tSSP);
if length(ordre) == 1
    indTime = ones(length(tPings),1);
%     if length(tSSP) ~= 1 % Cas du fichier 0043_20220111_044534.kmall (Mail Herv� du 23/05/2022)
%         ZFusion = RxAngleEarth.Sonar.BathyCel.Z(:);
%         CFusion = RxAngleEarth.Sonar.BathyCel.C(:);
%     end
else
%     indTime = interp1(tSSP', 1:length(tSSP), tPings); % Avant modif JMA le 30/08/2016
    indTime = my_interp1_Extrap_PreviousThenNext(tSSP', 1:length(tSSP), tPings); % Apr�s modif JMA le 30/08/2016
end
subNaN    = find( isnan(indTime));
subNonNaN = find(~isnan(indTime));
indTime(subNaN) = interp1(subNonNaN, indTime(subNonNaN), subNaN, 'next', 'extrap');

%% Extraction du 

Immersion = RxAngleEarth.Sonar.Immersion(:,:);

BathyPings  = Bathy.Image(:,:);
AnglesPings = RxAngleEarth.Image(:,:);
AngleSnellDescartes = RxAngleEarth;
for k=1:RxAngleEarth.nbRows
    Z = RxAngleEarth.Sonar.BathyCel.Z(indTime(k),:);
    C = RxAngleEarth.Sonar.BathyCel.C(indTime(k),:);
    subNaN = (isnan(C) | (C == 0));
    Z(subNaN) = [];
    C(subNaN) = [];

    if length(C) > 1
        % figure; plot(C, Z, '-*'); grid on;

        if max(abs(Z)) < max(abs(BathyPings(k,:)))
            Z = [Z -12000]; %#ok<AGROW> 
            C = [C C(end)]; %#ok<AGROW> 
        end

        cBathy = my_interp1(Z, C, BathyPings(k,:));
%         cImmer = my_interp1(Z, C, Immersion(k));
        cImmer = my_interp1_Extrap_PreviousThenNext(Z, C, Immersion(k)); % Modif JMA le 02/02/2022 pour donn�es DRIX XSF
        AngleSnellDescartes.Image(k,:) = asind(sind(AnglesPings(k,:)) .* (cBathy / cImmer));
    else
        AngleSnellDescartes.Image(k,:) = AnglesPings(k,:);
    end
end
% imagesc(AngleSnellDescartes - RxAngleEarth)

%% IBA

IBA = RxAngleEarth - AngleSnellDescartes;
