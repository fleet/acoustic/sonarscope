% Compensation de l'aire insonif�e
%
% Syntax
%   [flag, a, AI] = SonarAireInsoCompens(this, indImage, SonarAireInso_origine_New, ...)
%
% Input Arguments
%   this                      : Instance de clc_image
%   SonarAireInso_origine_New : 1=Constructeur, 2=Ifremer
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%   Mute : Message avertissant si l'image est d�j� conforme � ce qui est demand� (0 par d�faut)
%
% Output Arguments
%   flag : 1 : �a c'est bien pass�, 0 : �a c'est pas bien pass�
%   a    : Instances de cl_image
%   IA   : Aire Insonifi�e
%
% See Also : clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a, X] = SonarAireInsoCompens(this, indImage, SonarAireInso_origine_New, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

a  = [];
X  = [];

identReflectivity   = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, ...
    'GeometryType', 'PingXxxx');
if ~flag
    return
end

SonarAireInso_etat    = get(this(indImage), 'SonarAireInso_etat');
SonarAireInso_origine = get(this(indImage), 'SonarAireInso_origine');

if (SonarAireInso_etat == 1) && (SonarAireInso_origine == SonarAireInso_origine_New)
    flag = 0;
    if ~Mute
        message_ImageInGoodStatus(this(indImage))
    end
    return
end

%{
[flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerTxBeamIndex] = paramsFonctionSonarResol(this, indImage);
if ~flag
return
end
%}

[flag, indLayerInsonifiedArea, indLayerRange, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, ...
    indLayerTxBeamIndex, indLayerSlopeAcross, indLayerSlopeAlong] = paramsFonctionSonarAireInsonifiee(this, indImage); %, 'Mute', Mute);
if ~flag
    return
end

% if (SonarAireInso_origine_New == 3) && isempty(indLayerSlopeAcross) || isempty(indLayerSlopeAlong)
%     str1 = 'La compensation d''aire insonifi�e souhait�e �tait en fonction de l''angle d''incidence. Malheureusement, les layers SlopeAcross et SlopeAlong n''ont pas �t� trouv�s, la compensation est donc r�duite � l''utilisation de l''angle des faisceaux.';
%     str2 = 'You asked for an insonified area compensation based on incidence angles. Unfortunatelly for you, the SlopeAcross and SlopeAlong layers were not found. The software is thus setting this compensation according to beam pointing angles.';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'InsonifiedAreaCompSetTo2', 'TimeDelay', 60);
%     SonarAireInso_origine_New = 2;
% end

a = extraction(this(indImage), 'suby', suby, 'subx', subx);
if SonarAireInso_etat == 1
    if (SonarAireInso_origine == 1) && isComingFromKonsbergMBES(this(indImage))
        identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle'); % TODO : attention pour V1 : 'TxBeamIndex'
        identBeamTxAngle       = cl_image.indDataType('TxAngle');
        indBeamPointingAngle = findIndLayerSonar(this, indImage, 'DataType', [identBeamPointingAngle, identBeamTxAngle], 'WithCurrentImage', 1, 'OnlyOneLayer');
        [flag, X] = sonar_InsonifiedArea_KM(this(indImage), this(indLayerRange), ...
            this(indBeamPointingAngle), 'TxBeamIndex', this(indLayerTxBeamIndex), ...
            'subx', subx, 'suby', suby);
        if ~flag
            return
        end
    else
        if isempty(indLayerInsonifiedArea)
            [flag, X] = sonar_aire_insonifiee_dB(this(indImage), this(indLayerRange), ...
                CasDepth, H, CasDistance, ...
                this(indLayerBathy), ...
                this(indLayerEmission), ...
                SonarAireInso_origine, ...
                'SlopeAcross', this(indLayerSlopeAcross), ...
                'SlopeAlong',  this(indLayerSlopeAlong), ...
                'subx', subx, 'suby', suby);
            if ~flag
                return
            end
        else
            % TODO : que faire ? Dire � l'utilisateur qu'il a d�j� un
            % layer dans la liste ?
            % Cloner ce layer comme cela est fait
            % ici n'est certainement pas la bonne solution
            % TODO : attention : remplacer subx et suby par XLim et YLim
            X = extraction(this(indLayerInsonifiedArea), 'suby', suby, 'subx', subx);
        end
    end
    a = a + X;
    % else
    %     if isempty(Range)
    %         [~, indLayerRange] = findAnyLayerRange(this, indImage);
    %         Range = this(indLayerRange);
    %     end
end

if (SonarAireInso_origine_New == 1) && isComingFromKonsbergMBES(this(indImage))
    identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle'); % TODO : attention pour V1 : 'TxBeamIndex'
    indBeamPointingAngle = findIndLayerSonar(this, indImage, 'DataType', identBeamPointingAngle, 'WithCurrentImage', 1, 'OnlyOneLayer');
    [flag, X] = sonar_InsonifiedArea_KM(this(indImage), this(indLayerRange), this(indBeamPointingAngle), ...
        'TxBeamIndex', this(indLayerTxBeamIndex), 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
else
    if isempty(indLayerInsonifiedArea)
        [flag, X, SonarAireInso_origine_New] = sonar_aire_insonifiee_dB(this(indImage), this(indLayerRange), ...
            CasDepth, H, CasDistance, this(indLayerBathy), this(indLayerEmission), ...
            SonarAireInso_origine_New , 'subx', subx, 'suby', suby, ...
            'SlopeAcross', this(indLayerSlopeAcross), ...
            'SlopeAlong',  this(indLayerSlopeAlong), ...
            'TxBeamIndex', this(indLayerTxBeamIndex));
        if ~flag
            return
        end
    else
        % TODO : que faire ? Dire � l'utilisateur qu'il a d�j� un
        % layer dans la liste ?
        % Cloner ce layer comme cela est fait
        % ici n'est certainement pas la bonne solution
        % TODO : attention : remplacer subx et suby par XLim et YLim
        X = extraction(this(indLayerInsonifiedArea), 'suby', suby, 'subx', subx);
        
        % Ajout JMA le 19/04/2017 : si le layer InsonifiedArea est trouv�
        % on suppose qu'il provient a �t� calcul� � l'aide des angles
        % d'incidence
        if SonarAireInso_origine_New == 2
            SonarAireInso_origine_New = 3;
        end
    end
end
a = a - X;

a = set(a, 'SonarAireInso_etat', 1, 'SonarAireInso_origine', SonarAireInso_origine_New, 'Sonar_DefinitionENCours');

TitreOld = this(indImage).Name;
if SonarAireInso_origine_New == 1
    a = update_Name(a, 'Name', TitreOld, 'Append', 'AireInsoCompensFactory');
    X = update_Name(X, 'Name', TitreOld, 'Append', 'AireInsoCompensFactory');
else
    a = update_Name(a, 'Name', TitreOld, 'Append', 'AireInsoCompensIfremer');
    X = update_Name(X, 'Name', TitreOld, 'Append', 'AireInsoCompensIfremer');
end
