% Creation d'une image sans compensation d'aire insonifiee
%
% Syntax
%   [flag, a, IA] = SonarAireInsoCompensAucune(this, indImage, ...)
%
% Input Arguments
%   this : Instance de clc_image
%
% Name-Value Pair Arguments
%   subx                      : Echantillonage en x
%   suby                      : Echantillonage en y
%
% Output Arguments
%   flag : 1:ca c'est bien pass�, 0 : ca c'est pas bien pass�
%   a    : Instances de cl_image
%   IA   : Insonified area
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a, X] = SonarAireInsoCompensAucune(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin); %#ok<ASGLU>

a  = [];
X  = [];

identReflectivity = cl_image.indDataType('Reflectivity');
indGeometryTypes = [cl_image.indGeometryType('PingBeam')
    cl_image.indGeometryType('PingSamples')
    cl_image.indGeometryType('PingRange')
    cl_image.indGeometryType('PingAcrossDist')];
flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', indGeometryTypes);
if ~flag
    return
end

SonarAireInso_etat = get(this(indImage), 'SonarAireInso_etat');
%     SonarAireInso_origine = get(this(indImage), 'SonarAireInso_origine');

if SonarAireInso_etat == 2
    flag = 0;
    messageAboutCalibration('Tag', 'IA')
    return
end

if isComingFromKonsbergMBES(this(indImage))
    [flag, indLayerRange] = findAnyLayerRange(this, indImage);
    if isempty(indLayerRange)
        str1 = 'L''image provient d''un sondeur Kongsberg, il faut un layer de "TwoWayTravelTimeInSeconds", ou "Range" ou ... pour d�corriger l''aire insonifi�e constructeur.';
        str2 = 'The image is coming from a Kongsberg sounder, a "TwoWayTravelTimeInSeconds", or "Range" or ..; is requested to remove the insonified area made by Kongsberg.';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');
    identTxBeamIndex       = cl_image.indDataType('TxBeamIndex');
    
    indTxBeamIndex         = findIndLayerSonar(this, indImage, 'DataType', identTxBeamIndex, 'WithCurrentImage', 1, 'OnlyOneLayer');
    indBeamPointingAngle   = findIndLayerSonar(this, indImage, 'DataType', identBeamPointingAngle, 'WithCurrentImage', 1, 'OnlyOneLayer');
    [flag, X] = sonar_InsonifiedArea_KM(this(indImage), this(indLayerRange), this(indBeamPointingAngle), ...
        'TxBeamIndex', this(indTxBeamIndex), ...
        'subx', subx, 'suby', suby);
    if ~flag
        return
    end
else
    [flag, indLayerInsonifiedArea, indLayerRange, CasDepth, H, indLayerBathy, CasDistance, ...
        indLayerEmission] = paramsFonctionSonarAireInsonifiee(this, indImage);
    if ~flag
        return
    end
    if isempty(indLayerInsonifiedArea)
        X = sonar_aire_insonifiee_dB(this(indImage), this.Images(indLayerRange), ...
            CasDepth, H, CasDistance, ...
            this(indLayerBathy), ...
            this(indLayerEmission), ...
            1, 'subx', subx, 'suby', suby);
    else
        % TODO : que faire ? Dire � l'utilisateur qu'il a d�j� un
        % layer dans la liste ? Cloner ce layer comme cela est fait
        % ici n'est certainement pas la bonne solution
        % TODO : attention : remplacer subx et suby par XLim et YLim
        X = extraction(this(indLayerInsonifiedArea), 'suby', suby, 'subx', subx);
    end
end

a = extraction(this(indImage), 'suby', suby, 'subx', subx);
a = a + X;
a = set(a, 'SonarAireInso_etat', 2, 'Sonar_DefinitionENCours');
TitreOld = this(indImage).Name;
a = update_Name(a, 'Name', TitreOld, 'Append', 'AireInsoCompensNo');

TitreOld = this(indImage).Name;
X.Name = [TitreOld ' - AireInso'];
X = update_Name(X, 'Name', TitreOld, 'Append', 'AireInso');
