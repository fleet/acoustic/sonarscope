% Cr�ation d'une image sonar sans compensation des diagrammes de reception
%
% Syntax
%   [flag, a] = SonarBSCompens(this, indImage, SonarBS_origine, SonarBS_origineBelleImage, SonarBS_origineFullCompens, SonarBS_LambertCorrection, ...)
%
% Input Arguments
%   this : Instance de clc_image
%   SonarBS_origine            : '1=Belle Image' | '2=Full compensation'
%   SonarBS_origineBelleImage  : '1=Lambert'     | '2=Lurton'
%   SonarBS_origineFullCompens : '1=Analytique'  | '2=Table'
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%   Mute : Message avertissant si l'image est d�j� conforme � ce qui est demand� (0 par d�faut)
%
% Output Arguments
%   flag : 1 : �a c'est bien pass�, 0 : �a c'est pas bien pass�
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarBSCompens(this, indImage, SonarBS_origineNew, SonarBS_origineBelleImageNew, ...
    SonarBS_origineFullCompensNew, SonarBS_LambertCorrectionNew, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

a = cl_image.empty;

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

SonarBS_etat               = get(this(indImage), 'SonarBS_etat');
SonarBS_origine            = get(this(indImage), 'SonarBS_origine');
SonarBS_origineBelleImage  = get(this(indImage), 'SonarBS_origineBelleImage');
SonarBS_origineFullCompens = get(this(indImage), 'SonarBS_origineFullCompens');
SonarBS_LambertCorrection  = get(this(indImage), 'SonarBS_LambertCorrection');
%     SonarBS_IfremerCompensTable = get(this(indImage),   'SonarBS_IfremerCompensTable');

if isempty(SonarBS_origineBelleImageNew)
    SonarBS_origineBelleImageNew = SonarBS_origineBelleImage;
end
if isempty(SonarBS_origineFullCompensNew)
    SonarBS_origineFullCompensNew = SonarBS_origineFullCompens;
end

if (SonarBS_etat == 1) && ...
        (SonarBS_origine            == SonarBS_origineNew) && ...
        (SonarBS_origineBelleImage  == SonarBS_origineBelleImageNew) && ...
        (SonarBS_origineFullCompens == SonarBS_origineFullCompensNew) && ...
        (SonarBS_LambertCorrection  == SonarBS_LambertCorrectionNew)
    flag = 0;
    if ~Mute
        messageAboutCalibration('Tag', 'BS')
    end
else
    a = extraction(this(indImage), 'suby', suby, 'subx', subx, 'ForceExtraction');
    if SonarBS_etat == 1
        [X, flag] = getSonarBSNiveauxImage(this, indImage, subx, suby);
        if flag
            a = a + X;
        else
            return
        end
    end
    
    [X, flag, SonarBS_LambertCorrectionNew] = getSonarBSNiveauxImage(this, indImage, subx, suby, ...
        'SonarBS_origine',            SonarBS_origineNew, ...
        'SonarBS_origineBelleImage',  SonarBS_origineBelleImageNew, ...
        'SonarBS_origineFullCompens', SonarBS_origineFullCompensNew, ...
        'SonarBS_LambertCorrection',  SonarBS_LambertCorrectionNew, ...
        'Mute', Mute);
    if isnan(flag)
        return
    end
    if ~flag
        return
    end
    a = a - X;
    SonarBS_BSLurtonParameters = get(X, 'SonarBS_BSLurtonParameters');
    clear X
    
    a = set(a,  'Sonar_DefinitionENCours', 'SonarBS_etat', 1, ...
        'SonarBS_origine',            SonarBS_origineNew, ...
        'SonarBS_origineBelleImage',  SonarBS_origineBelleImageNew, ...
        'SonarBS_origineFullCompens', SonarBS_origineFullCompensNew, ...
        'SonarBS_LambertCorrection',  SonarBS_LambertCorrectionNew, ...
        'SonarBS_BSLurtonParameters', SonarBS_BSLurtonParameters);
    TitreOld = this(indImage).Name;
    
    if SonarBS_origineNew == 1  % Belle Image
        str = {'R/Rn'; 'AngEarth'; 'AngInc'};
        if SonarBS_origineBelleImageNew == 1    % Lambert
            TitreNew = ['BSCompensationBelleImageLambert' str{SonarBS_LambertCorrectionNew}];
        else    % Lurton
            TitreNew = ['BSCompensationBelleImageLurton' str{SonarBS_LambertCorrectionNew}];
        end
    else    % Full compensation
        if SonarBS_origineFullCompensNew  == 1  % Analytique
            TitreNew = 'BSFullCompensationAnalytique';
        else    % Table
            TitreNew = 'BSFullCompensationTable';
        end
    end
    a = update_Name(a, 'Name', TitreOld, 'Append', TitreNew);
end
