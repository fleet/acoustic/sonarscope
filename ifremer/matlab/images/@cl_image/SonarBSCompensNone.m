% Creation d'une image sonar sans compensation des diagrammes de reception
% TODO
%
% Syntax
%   [flag, b] = SonarBSCompensNone(a, indImage, ...)
%
% Input Arguments
%   a        : cl_image instances
%   indImage : Index of the reflectivity image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%   Mute : Message avertissant si l'image est d�j� conforme � ce qui est demand� (0 par d�faut)
%
% Output Arguments
%   flag : 1:OK, 0=KO
%   b    : Updated instance of cl_image
%
% Examples
%   [flag, b] = TODO
%     imagesc(a)
%     imagesc(b)
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarBSCompensNone(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);
[varargin, Bathy] = getPropertyValue(varargin, 'Bathy', []); %#ok<ASGLU>

a = cl_image.empty;

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity,  'GeometryType', 'PingXxxx');
if ~flag
    return
end

SonarBS_etat = get(this(indImage), 'SonarBS_etat');
if SonarBS_etat == 2
    a = extraction(this(indImage), 'suby', suby, 'subx', subx, 'ForceExtraction');
    flag = 1;
    if ~Mute
        message_ImageInGoodStatus(this(indImage))
    end
else
    a = extraction(this(indImage), 'suby', suby, 'subx', subx, 'ForceExtraction');
    if SonarBS_etat == 1
        [X, flag] = getSonarBSNiveauxImage(this, indImage, subx, suby, 'Bathy', Bathy);
        if ~flag
            return
        end
        a = a + X;
        clear X
    end
    a = set(a, 'SonarBS_etat', 2);
    TitreOld = this(indImage).Name;
    a = update_Name(a, 'Name', TitreOld, 'Append', 'BSCompensNo');
end
