% Calcul du BS
%
% Syntax
%   [flag, a] = SonarBSCompute(this, indImage, subx, suby)
%
% Input Arguments
%   this : Instance de clc_image
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=ca c'est bien passe, 0=ca c'est pas bien passe
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = SonarBSCompute(this, indImage, subx, suby, nomZoneEtude, varargin)

[varargin, TakeTxAngle] = getPropertyValue(varargin, 'TakeTxAngle', 0);
[varargin, EnteteNomCourbe] = getPropertyValue(varargin, 'EnteteNomCourbe', 'BS');
[varargin, NoPlots] = getFlag(varargin, 'NoPlots'); %#ok<ASGLU>

% Test si Reflectivity & geom�trie PingXxxx
identReflectivity  = cl_image.indDataType('Reflectivity');
% flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    return
end

[flag, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, strConf] = paramsModeIfSeveral(this(indImage), suby);
if ~flag
    return
end

%{
if TakeTxAngle
% Verification si c'est le m�me mode sur tout suby
Mode = get(this(indImage), 'PortMode_1');
SelectMode = unique(Mode(suby));
SelectMode(SelectMode == 0) = [];
if length(SelectMode) ~= 1
str1 = 'Il y a plus d''un mode dans cette image, veuillez s�lectionner une partie de l''image ne pr�sentant qu''un seul mode? Ceci est une �tape de calcul ce diagrammes de directivit� c''est la raison pour laquelle le Mode doit �tre pris en compte.';
str2 = 'There are more than one mode in this image, please choose a part of the image with only one mode. This is a DiagTx preparation, that is why you have to take into account Mode.';
my_warndlg(Lang(str1,str2), 1);
flag = 0;
return
end
end
%}

%% Contr�le si le BS est non compens�

SonarBS_etat = get(this(indImage), 'SonarBS_etat');
if SonarBS_etat ~= 2
    str1 = 'Pour calculer un BS, il faut que l''image soit Not compensatede en BS. Allez dans le menu Calibration pour cela';
    str2 = 'To compute a BS, the image must not be compensated. Go to "Calibration" menu for that.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Contr�le si les diagrammes de directivit� en �mission sont compens�s

SonarDiagEmi_etat = get(this(indImage), 'SonarDiagEmi_etat');
if SonarDiagEmi_etat ~= 1
    str1 = 'Pour calculer un BS, il est vivement recommand� que les diagrammes de directivit� en �mission soient compens�s. Voulez-vous outrepasser cette recommandation ?';
    str2 = 'To compute a BS, it is hightly recommended that TxDiag be compensated, that does not seem to be the case? Do you want to overpass this advice ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (rep == 2)
        return
    end
end

%% Contr�le si la TVG est compens�e

SonarTVG_etat = get(this(indImage), 'SonarTVG_etat');
if SonarTVG_etat ~= 1
    str1 = 'Pour calculer un BS, il est vivement recommand� que la TVG soit compens�e. Voulez-vous outrepasser cette recommandation ?';
    str2 = 'To compute a BS, it is hightly recommended that the TVG be compensated, that does not seem to be the case? Do you want to overpass this advice ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (rep == 2)
        return
    end
end

%% Saisie des param�tres

[flag, this, indLayerAngle, indLayerN, bins, MeanCompType] = paramsFonctionSonarBS(this, indImage, TakeTxAngle);
if ~flag
    %     my_warndlg(Lang('Aucun layer d''angle n''a �t� trouv�','None layer was found'), 1);
    return
end

[flag, indLayerMask, valMask] = paramsMasque(this, indImage);
if ~flag
    return
end

for k=1:length(valMask)
    %     if TakeTxAngle
    %         EnteteNomCourbe = sprintf('%s Mode %d', EnteteNomCourbe, SelectMode);strConf
    %     end
    if ~isempty(strConf)
        EnteteNomCourbe = sprintf('%s : %s', EnteteNomCourbe, strConf);
    end
    
    str1 = 'Nom de courbe de r�trodiffusion';
    str2 = 'Backscatter curve name';
    [flag, nomCourbe, Commentaire] = paramsStatsNameComment(this(indImage), ...
        EnteteNomCourbe, nomZoneEtude, indLayerMask, valMask(k), Lang(str1,str2), ...
        'subx', subx, 'suby', suby);
    if ~flag
        break
    end
    
    couleur = []; % uisetcolor([0 0 1], 'Choisissez une couleur associ�e');
    this(indImage) = sonar_calcul_BS(this(indImage), ...
        this(indLayerN), this(indLayerAngle), ...
        nomCourbe, Commentaire, couleur, ...
        'LayerMask', this(indLayerMask), 'valMask', valMask(k), ...
        'subx', subx, 'suby', suby, 'bins', bins, ...
        'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, ...
        'MeanCompType', MeanCompType);
    if ~NoPlots
        plotCourbesStats(this(indImage), 'last', 'Tag', 'BS');
    end
end

if ~NoPlots && (length(valMask) > 1)
    plotCourbesStats(this(indImage), 'Tag', 'BS');
    plotCourbesStats(this(indImage), 'Tag', 'BS', 'Stats', 0);
end
