% Creation d'une image donnant le meilleur BS fournissable par le constructeur
% TODO
%
% Syntax
%   [flag, b, alpha] = SonarBestBSConstructeur(a, indImage, ...)
%
% Input Arguments
%   a        : cl_image instances
%   indImage : Index of the reflectivity image
%
% Name-Value Pair Arguments
%   subx     : Sub-sampling in abscissa
%   suby     : Sub-sampling in ordinates
%
% Output Arguments
%   flag  : 1=OK, 0=KO
%   b     : Updated instance of cl_image
%   alpha : TODO
%
% Examples
%   TODO
%   [flag, b, alpha] = SonarBestBSConstructeur(a, indImage)
%    imagesc(a)
%    imagesc(b)
%
% See also SonarEtatBS sonar_TVG_CompensationSSc Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : ce nom (SonarBestBSConstructeur) n'est plus tr�s adapt�. Ca devrait �tre SonarBestBS tout simplement

function [flag, a, alpha] = SonarBestBSConstructeur(this, indImage, varargin)

[varargin, subx]                 = getPropertyValue(varargin, 'subx',                 []);
[varargin, suby]                 = getPropertyValue(varargin, 'suby',                 []);
[varargin, Mute]                 = getPropertyValue(varargin, 'Mute',                 0);
[varargin, Range]                = getPropertyValue(varargin, 'Range',                []);
[varargin, alpha]                = getPropertyValue(varargin, 'Absoption_dB/km',      []);
[varargin, ChangeInsonifiedArea] = getPropertyValue(varargin, 'ChangeInsonifiedArea', 1);
[varargin, Bathy]                = getPropertyValue(varargin, 'Bathy',                []); %#ok<ASGLU>

%% Test si l'image est de type "Reflectivity"

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    a = [];
    return
end

%% Suppression des messages si sondeur EM1002

SonarSescription = get_SonarDescription(this(indImage));
SonarName = get(SonarSescription, 'SonarName');
switch SonarName
    case 'EM1002'
        Mute = 1;
end

%% R�cup�ration du nom de l'image d'entr�e

ImageName = this(indImage).Name;

%% Traitement de l'image

if isempty(suby)
    suby = 1:this(indImage).nbRows;
end
[flag, a, alpha] = SonarEtatBS(this, indImage, 'subx', subx, 'suby', suby, 'Range', Range, 'Absoption_dB/km', alpha, 'DiagTx', 'RT', 'Mute', Mute, ...
    'ChangeInsonifiedArea', ChangeInsonifiedArea, 'Bathy', Bathy);
if isempty(a)
    return
end

%% D�finition du nom du fichier de sortie

if flag
    a = update_Name(a, 'Name', ImageName, 'Append', 'Step1', 'regexprep', {'_Step.'; ''});
end

%% Message si backscatter status pas optimum

[flag2, msg] = isBestBSStatus(a);
if flag2
    flag = saveLayerInCache(a, suby);
else
    my_warndlg(msg, 0, 'displayItEvenIfSSc', 1, 'Tag', 'CheckBestBSStatus');
end



function flag = saveLayerInCache(a, suby)

nomFic = a.InitialFileName;
if strcmp(a.InitialFileFormat, 'SimradAll')
    [nomDir, nom] = fileparts(nomFic);
    ncFileName = fullfile(nomDir, 'SonarScope', [nom '.nc']);
    flag = NetcdfUtils.existVar(ncFileName, 'Depth', 'ReflectivityBestBS');
    if ~flag
        flag = NetcdfUtils.copyVarDef(ncFileName, 'Depth', 'ReflectivityFromSnippets', 'ReflectivityBestBS');
    end
    if flag
        
        %% Lecture du fichier XML d�crivant la donn�e
        
        [flag, XML] = SSc_ReadDatagrams(nomFic, 'Ssc_Depth', 'XMLOnly', 1);
        if ~flag
            logFileId.warn('SonarBestBSConstructeur', ['Exit because function "SSc_ReadDatagrams" failed for "' nomFic '"']);
            return
        end
        
        %% Save ReflectivityBestBS in SonarScope cache directory (IncidenceAngle_PingBeam)
        
        X = a.Image(:,:);
        if (size(X,1) ~= XML.Dimensions.nbPings) || (size(X,2) ~= XML.Dimensions.nbBeams)
            Y = NaN(XML.Dimensions.nbPings, XML.Dimensions.nbBeams, class(X));
            try
                Y(suby,:) = X;
            catch % Bidouille infame pour �viter bug fichier SPFE 0012_20100225_031309_Belgica.all
                suby(suby > size(X,1)) = [];
                Y(suby,:) = X(1:length(suby),:);
            end
            X = Y;
        end
        %     LayerName = extract_ImageName(a);
        aKM = cl_simrad_all('nomFic', nomFic);
        flag = save_image(aKM, 'Ssc_Depth', 'ReflectivityBestBS', X);
        if ~flag
            logFileId.warn('SonarBestBSConstructeur', ['Exit because function "save_image" failed when exporting "ReflectivityFromSnippetsBestStep1" into "' nomFic '"']);
            return
        end
    end
end
