% Creation d'une image donnant le meilleur BS fournissable par Ifremer
%
% Syntax
%   [flag, a] = SonarBestBSIfremer(this, indImage, ...)
%
% Input Arguments
%   this : Instance de clc_image
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=ca c'est bien passe, 0=ca c'est pas bien passe
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a, SonarTVG_IfremerAlpha] = SonarBestBSIfremer(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, SonarTVG_IfremerAlpha] = getPropertyValue(varargin, 'SonarTVG_IfremerAlpha', []); %#ok<ASGLU>

flagModif = 0;
SonarName = get(this(indImage).Sonar.Desciption, 'SonarName');

%% Test si l'image est de type "Reflectivity"

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    a = [];
    return
end

%% R�cup�ration du nom de l'image d'entr�e

TitreOld = this(indImage).Name;

%% Traitement de la TVG : Modif JMA le 17/09/2015

% TODO or not TODO ?
% On obtenait une image avec des r�sidus de TVG sur l'image compens�es des
% diagrammes de directivit� par le module calibration et non sur les images
% de comp BS+DiagTx
%{
[flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser, alphaUser] = ...
paramsFonctionSonarTVGCompensationSSc(this, indImage, 'Mute', 1);
if flag
[flag, a] = sonar_TVG_CompensationSSc(this(indImage), 'subx', subx, 'suby', suby, ...
'Range', this(indLayerRange), ...
'AbsorptionCoeffRT',   this(indLayerAbsorptionCoeffRT), ...
'AbsorptionCoeffSSc',  this(indLayerAbsorptionCoeffSSc), ...
'AbsorptionCoeffUser', this(indLayerAbsorptionCoeffUser), 'alphaUser', alphaUser);
if ~flag
messageAboutCalibration('Tag', 'TVG')
end
this(indImage) = a;
end
%}

%% Traitement de l'aire insonifi�e

if strcmp(SonarName, 'GeoSwath') % TODO : restreint au Geoswath pour le moment en attente de validation sur .all
    [flag, a] = SonarAireInsoCompens(this, indImage, 2, 'subx', subx, 'suby', suby, 'Mute', 1);
    if flag
        this(indImage) = a;
        subx = 1:a.nbColumns;
        suby = 1:a.nbRows;
        clear a
        flagModif = 1;
    end
end

%% Traitement du Backscatter

[flag, a] = SonarBSCompensNone(this, indImage, 'subx', subx, 'suby', suby, 'Mute', 1);
% if flag % Avant
%     this(indImage) = a;
%     subx = 1:a.nbColumns;
%     suby = 1:a.nbRows;
%     clear a
%     flagModif = 1;
% end
if ~flag % Maintenant
    return
end
if ~isempty(a)
    this(indImage) = a;
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    clear a
    flagModif = 1;
end

%% Traitement des diagrammes de directivit� en �mission

SonarDiagEmi_etat    = get(this(indImage), 'SonarDiagEmi_etat');
SonarDiagEmi_origine = get(this(indImage), 'SonarDiagEmi_origine');
if (SonarDiagEmi_etat ~= 1) || (SonarDiagEmi_origine ~= 2)
    switch SonarName
        case 'GeoSwath' % On corrige des diagrammes de directivit� mesur�s en usine
            %           [flag, a] = SonarDiagTxCompens(this, indImage, 1, 2, 'subx', subx, 'suby', suby, 'Mute', 1);
            flag = 0;
            
        otherwise % On corrige des diagrammes de directivit� mod�lis�s par SSc
            str1 = 'Quelle mod�les doit-on consid�rer pour la compensation des diagrammes de directivit� ?';
            str2 = 'Select the models that you want to use for the Tx beam diagram compensation ?';
            [flag, identCourbe] = selectionCourbesStats(this(indImage), 'Tag', 'DiagTx', 'Title', Lang(str1,str2)); %, 'NoManualSelection', 1);
            if ~flag || isempty(identCourbe)
                flag = 0;
                a = [];
                return
            end
            [flag, a] = SonarDiagTxCompens(this, indImage, 2, 1, 'subx', subx, 'suby', suby, 'Mute', 1, 'identCourbe', identCourbe);
    end
    if flag
        this(indImage) = a;
        clear a
        flagModif = 1;
    end
end

%% D�finition du nom du fichier de sortie

if flagModif
    flag = 1;
    a.Name = [TitreOld ' - Data BS Ifremer'];
else
    flag = 0;
    message_ImageInGoodStatus(this(indImage))
end
