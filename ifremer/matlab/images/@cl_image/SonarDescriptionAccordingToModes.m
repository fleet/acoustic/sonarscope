% TODO : voir en quoi cette fonction est diff�rente de update_SonarDescription_Ping

function [flag, this, SounderMode] = SonarDescriptionAccordingToModes(this, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>Mute

flag = 0;

%% V�rification si c'est le m�me mode sur tout suby

SonarDescription = get(this, 'SonarDescription');

%     [flag, SonarMode_1, SonarMode_2, SonarMode_3] = getSounderParamsForcl_sounderXML(Model, , 'nbSwaths',  nbSwaths, 'presenceFM', SounderMode, 'SounderMode', SounderMode);

Mode = get(this, 'PortMode_1');
if isempty(Mode)
    SounderMode = get(SonarDescription, 'Sonar.Mode_1');
else
    if isempty(suby)
        SounderMode = unique(Mode(:));
    else
        SounderMode = unique(Mode(suby));
    end
    SounderMode(SounderMode  == 0) = [];
    if length(SounderMode) ~= 1
        str1 = 'Il y a plusieurs modes dans cette image, s�lectionnez une partie de l''image o� il n''y en a qu''un seul, svp.';
        str2 = 'There are several modes in this image, please choose a part of the image with only one mode.';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    % TODO Cette partie doit �tre am�lior�e : r�aliser un tri de toutes
    % les configurations posssibles : Mode, Swath, FM et nommer les
    % courbes en _Mode_Swath_FM et les utiliser dans Survey processings
    
    TypeMode_1 = get(SonarDescription, 'TypeMode_1');
    if isempty(TypeMode_1) % Pour compatibilit� avec l'existant : � supprimer quand tout sera au point
        SonarDescription = set(SonarDescription, 'Sonar.Mode_1', SounderMode);
    else
        if strcmp(TypeMode_1, 'SounderMode')
            SonarDescription = set(SonarDescription, 'Sonar.Mode_1', SounderMode);
        else
            if strcmp(TypeMode_1, 'Swath')
                Model = get(SonarDescription, 'Sonar.Name');
                if isempty(this.Sonar.NbSwaths)
                    nbSwaths = 1;
                else
                    if isempty(suby)
                        nbSwaths = this.Sonar.NbSwaths(:);
                    else
                        nbSwaths = this.Sonar.NbSwaths(suby);
                    end
                    liste = 0:2;
                    n = my_hist(nbSwaths, liste);
                    [~, ind] = max(n);
                    nbSwaths = liste(ind); % TODO : calculer plut�t l'histogramme et donner celui qui a le plus de piings
                end
                
                %% CW/FM
                
                if isempty(this.Sonar.PresenceFM)
                    presenceFM = 0;
                else
                    if isempty(suby)
                        presenceFM = this.Sonar.PresenceFM(:);
                    else
                        presenceFM = this.Sonar.PresenceFM(suby);
                    end
                    liste = 0:1;
                    n = my_hist(presenceFM, liste); 
                    [~, ind] = max(n);
                    presenceFM = liste(ind); % TODO : calculer plut�t l'histogramme et donner celui qui a le plus de piings
                end
                
                %% Mode2 if EM2040
                
                signalMode2 = this.SignalsVert.getSignalByTag('Mode2');
                if isempty(signalMode2)
                    listeMode2 = [];
                else
                    [flag, Mode2, Unit] = signalMode2.getValueMatrix();
                    if ~flag
                        return
                    end
                    [listeMode2, ~, JMode2] = unique(Mode2(suby));
%                     nomMode2 = signalMode2.ySample.strIndexList;
                end
                
                %% CentralFrequency if EM2040
                
                signalCentralFrequency = this.SignalsVert.getSignalByTag('CentralFrequency');
                if isempty(signalCentralFrequency)
                    MainFrequency = [];
                else
                    [flag, CentralFrequency] = signalCentralFrequency.getValueMatrix();
                    if ~flag
                        return
                    end
%                     listeCentralFrequency = unique(CentralFrequency(suby,:));
%                     EM2040Frequencies = [200 300 400];
%                     [~, indFreq] = min(abs(EM2040Frequencies - mean(listeCentralFrequency)));
%                     MainFrequency = EM2040Frequencies(indFreq);
                    IdentSonar = get(SonarDescription, 'IdentSonar');
                    MainFrequency = getMainFrequencyIfEM2040(IdentSonar, CentralFrequency(suby,:));
                end
                
                %% 
                
                if isempty(listeMode2)
                    modeListMode2 = [];
                else
                    modeListMode2 = mode(listeMode2);
                end
                [flag, SonarMode_1, SonarMode_2, SonarMode_3] = getSounderParamsForcl_sounderXML(Model, 'nbSwaths',  nbSwaths, ...
                    'presenceFM', presenceFM, 'SounderMode', SounderMode, 'Mute', Mute, 'SounderMode2EM2040', modeListMode2);
                if ~flag
                    return
                end
                SonarDescription = set(SonarDescription, 'Sonar.Mode_1', SonarMode_1, 'Sonar.Mode_2', SonarMode_2, ...
                    'Sonar.Mode_3', SonarMode_3, 'MainFrequency', MainFrequency);
            else
                str1 = 'Message pour JMA : il n''a pas �t� possible de g�rer le mode du sondeur dans "SonarDiagTxCompute"';
                str2 = 'Message for JMA : it was not possible to set the mode in "SonarDiagTxCompute", please report to JMA';
                my_warndlg(Lang(str1,str2), 1);
            end
        end
    end
end
this = set(this, 'SonarDescription', SonarDescription);

flag = 1;
