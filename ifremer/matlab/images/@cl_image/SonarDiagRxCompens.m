% Creation d'une image sonar compensee en diagramme de reception
%
% Syntax
%   [flag, a] = SonarDiagRxCompens(this, indImage, subx, suby, SonarDiagRec_origine_New, SonarDiagRec_Type_New)
% 
% Input Arguments
%   this                     : Instance de clc_image
%   subx                     : Echantillonage en x
%   suby                     : Echantillonage en y
%   SonarDiagRec_origine_New : 1=Constructeur, 2=Ifremer
%   SonarDiagRec_Type_New    : 1=Analytique, 2=Table
%
% Output Arguments
%   flag : 1:ca c'est bien passe, 0 : ca c'est pas bien passe
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarDiagRxCompens(this, indImage, subx, suby, SonarDiagRec_origine_New, SonarDiagRec_Type_New)

a = [];

% Test si Reflectivity et (SonarD ou SonarX)
identReflectivity   = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, ...
    'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingAcrossDist')]);
if flag
    SonarDiagRec_etat    = get(this(indImage), 'SonarDiagRec_etat');
    SonarDiagRec_origine = get(this(indImage), 'SonarDiagRec_origine');
    if SonarDiagRec_origine == 1    % Constructeur
        SonarDiagRec_Type    = get(this(indImage), 'SonarDiagRec_ConstructTypeCompens');
    else    % Ifremer
        SonarDiagRec_Type    = get(this(indImage), 'SonarDiagRec_IfremerTypeCompens');
    end
    
    if (SonarDiagRec_etat    == 1) && ...
       (SonarDiagRec_origine == SonarDiagRec_origine_New) && ...
       (SonarDiagRec_Type    == SonarDiagRec_Type_New)
        flag = 0;
        messageAboutCalibration('Tag', 'DiagRx')
    else
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagRxConstructeur(this, indImage); %#ok
        if isempty(indLayerReceptionBeam)
            if ~isdeployed
                my_warndlg('ATTENTION : indLayerReceptionBeam vide dans SonarDiagRxCompens', 1);
            end
        end
        if flag
            a = extraction(this(indImage), 'suby', suby, 'subx', subx);
            if SonarDiagRec_etat == 1
                SonarDiagRec_origine = get(this(indImage), 'SonarDiagRec_origine');
                X = sonar_diag_Rx(   this(indImage), ...
                    CasDepth, H, CasDistance, ...
                    this(indLayerBathy), ...
                    this(indLayerEmission), ...
                    this(indLayerReceptionBeam), ...
                    'OrigineModeles', SonarDiagRec_origine, ...
                    'TypeDiagramme',  SonarDiagRec_Type, ...
                    'subx', subx, 'suby', suby, 'noLayerReceptionBeamEstime');
                a = a + X;
            end

            X = sonar_diag_Rx(   this(indImage), ...
                CasDepth, H, CasDistance, ...
                this(indLayerBathy), ...
                this(indLayerEmission), ...
                this(indLayerReceptionBeam), ...
                'OrigineModeles', SonarDiagRec_origine_New, ...
                'TypeDiagramme',  SonarDiagRec_Type_New, ...
                'subx', subx, 'suby', suby, 'noLayerReceptionBeamEstime');
            a = a - X;
            a = set(a, 'SonarDiagRec_etat', 1, 'SonarDiagRec_origine', SonarDiagRec_origine_New, 'Sonar_DefinitionENCours');
            
            TitreOld = this(indImage).Name;
            if SonarDiagRec_origine_New == 1
                if SonarDiagRec_Type_New == 1
                    a = set(a, 'SonarDiagRec_ConstructTypeCompens', 1);
                    a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagRecCompensationConstructeurAnalytique');
                else
                    a = set(a, 'SonarDiagRec_ConstructTypeCompens', 2);
                    a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagRecCompensationConstructeurTable');
               end
            else
                if SonarDiagRec_Type_New == 1
                    a = set(a, 'SonarDiagRec_IfremerTypeCompens', 1);
                    a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagRecCompensationIfremerAnalytique');
                else
                    a = set(a, 'SonarDiagRec_IfremerTypeCompens', 2);
                    a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagRecCompensationIfremerTable');
                end
            end
        end
    end
end
