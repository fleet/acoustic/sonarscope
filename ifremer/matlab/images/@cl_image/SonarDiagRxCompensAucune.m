% Creation d'une image sonar sans compensation des diagrammes de reception
%
% Syntax
%   [flag, a] = SonarDiagRxCompensAucune(this, indImage, subx, suby)
% 
% Input Arguments
%   this : Instance de clc_image
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1:ca c'est bien passe, 0 : ca c'est pas bien passe
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarDiagRxCompensAucune(this, indImage, subx, suby)

a = [];

% Test si Reflectivity et (SonarD ou SonarX)
identReflectivity   = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, ...
    'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingAcrossDist')]);
if flag
    SonarDiagRec_etat    = get(this(indImage), 'SonarDiagRec_etat');
    SonarDiagRec_origine = get(this(indImage), 'SonarDiagRec_origine');
    
    if SonarDiagRec_etat == 2
        flag = 0;
        messageAboutCalibration('Tag', 'DiagRx')
    else
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagRxConstructeur(this, indImage); %#ok
        if flag
            X = sonar_diag_Rx(   this(indImage), ...
                CasDepth, H, CasDistance, ...
                this(indLayerBathy), ...
                this(indLayerEmission), ...
                this(indLayerEmissionBeam), ...
                'OrigineModeles',    SonarDiagRec_origine, ...
                'TypeDiagramme', 1, ...
                'subx', subx, 'suby', suby, 'noLayerReceptionBeamEstime');

            a = extraction(this(indImage), 'suby', suby, 'subx', subx);
            a = a - X;
            a = set(a, 'SonarDiagRec_etat', 2, 'Sonar_DefinitionENCours');
            TitreOld = this(indImage).Name;
            a = update_Name(a, 'Name', TitreOld, 'Append', 'RxCompensNo');
        end
    end
end
