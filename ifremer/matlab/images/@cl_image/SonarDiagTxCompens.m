% Cr�ation d'une image sonar compens�e en diagramme d'�mission
%
% Syntax
%   [flag, b] = SonarDiagTxCompens(a, indImage, SonarDiagEmi_origine_New, SonarDiagEmi_Type_New, ...)
%
% Input Arguments
%   a                        : cl_image instances
%   indImage                 : Index of the reflectivity image
%   SonarDiagEmi_origine_New : 1=Constructeur, 2=Ifremer
%   SonarDiagEmi_Type_New    : 1=Analytique, 2=Table
%
% Name-only Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%   Mute : Pas de message d'erreur (Default 0)
%
% Output Arguments
%   flag : 1:OK, 0=KO
%   b    : Updated instance of cl_image
%
% Examples
%   [flag, b] = TODO
%     imagesc(a)
%     imagesc(b)
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarDiagTxCompens(this, indImage, SonarDiagEmi_origine_New, SonarDiagEmi_Type_New, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, Mute]        = getPropertyValue(varargin, 'Mute',        0);
[varargin, identCourbe] = getPropertyValue(varargin, 'identCourbe', []); %#ok<ASGLU>

a = [];

[flag, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, strConf, listeSuby] = paramsModeIfSeveral(this(indImage), suby, ...
    'askQuestionSignatureConfSonar', ~Mute);
if ~flag
    return
end
% suby = listeSuby{1};

[flag, this(indImage), SounderMode] = SonarDescriptionAccordingToModes(this(indImage), 'suby', listeSuby{1}, 'Mute', 1); %#ok<ASGLU>
if ~flag
    return
end

% ATTENTION ICI : Modif JMA le 18/01/2015 (ajout du test if
% (SonarDiagEmi_origine_New) == 2 && (SonarDiagEmi_Type_New == 1))
% C'est pas s�r que la proc�dure BSCorr fonctionne toujours avec cette
% modif : � v�rifier de toute urgence

if (SonarDiagEmi_origine_New == 2) && (SonarDiagEmi_Type_New == 1)
    %{
% Comment� par JMA le 09/02/2016
if isempty(identCourbe)
[flag, identCourbe] = selectionCourbesStats(this(indImage), 'Tag', 'DiagTx', 'SelectionMode', 'Single'); % , 'NoManualSelection', 1);
if ~flag || isempty(identCourbe)
return
end
end
    %}
    
    % dans SonarDiagTxCompute : extraire une fonction de r�cup des modes et set
    % de desciption puis l'utiliser ici
    
    if length(this(indImage).CourbesStatistiques) >= identCourbe
        bilan = this(indImage).CourbesStatistiques(identCourbe).bilan{1};
        nbModels = length(bilan);
        models = get_EmissionModelsCalibration(this(indImage).Sonar.Desciption);
        for k2=1:nbModels
            %         nuSector = bilan(k2).TagSup{2};
            %
            %         TypeMode_1 =  get(this(indImage).Sonar.Desciption, 'Sonar.TypeMode_1');
            %         TypeMode_2 =  get(this(indImage).Sonar.Desciption, 'Sonar.TypeMode_2');
            %
            %         if strcmp(TypeMode_1, 'SounderMode')
            %             this(indImage).Sonar.Desciption = set(this(indImage).Sonar.Desciption, 'Sonar.Mode_1', nuSector);
            %         elseif strcmp(TypeMode_1, 'Swath') && strcmp(TypeMode_2, 'SounderMode')
            %             Mode_Swath = 1; % TODO : impos� � 1 car dans bilan(k2) on ne connait pas ce param�tre !!!
            %             this(indImage).Sonar.Desciption = set(this(indImage).Sonar.Desciption, 'Sonar.Mode_1', Mode_Swath, 'Sonar.Mode_2', nuSector);
            %         end
            
            % Ca bugue ici lors de Check Calibration, Bs & DiagTx
            if isfield(bilan(k2), 'numVarCondition')
                numVarCondition = bilan(k2).numVarCondition;
            else
                numVarCondition = k2;
            end
            if numVarCondition > length(models)
                continue
            end
            if ~isempty(bilan(k2).model)
                models(numVarCondition) = bilan(k2).model;
            end
        end
        this(indImage).Sonar.Desciption = set_EmissionModelsCalibration(this(indImage).Sonar.Desciption, models);
    end
end

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

SonarDiagEmi_etat    = get(this(indImage), 'SonarDiagEmi_etat');
SonarDiagEmi_origine = get(this(indImage), 'SonarDiagEmi_origine');
if SonarDiagEmi_origine == 1    % Constructeur
    SonarDiagEmi_Type = get(this(indImage), 'SonarDiagEmi_ConstructTypeCompens');
else    % Ifremer
    SonarDiagEmi_Type = get(this(indImage), 'SonarDiagEmi_IfremerTypeCompens');
end

if (SonarDiagEmi_etat == 1) && ...
        (SonarDiagEmi_origine == SonarDiagEmi_origine_New) && ...
        (SonarDiagEmi_Type    == SonarDiagEmi_Type_New)
    flag = 0;
    if ~Mute
        message_ImageInGoodStatus(this(indImage))
    end
else
    [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagTxConstructeur(this, indImage); %#ok
    if ~flag
        return
    end
    a = extraction(this(indImage), 'suby', suby, 'subx', subx);
    if SonarDiagEmi_etat == 1
        [X, flag] = sonar_diag_Tx(this(indImage), ...
            CasDepth, H, CasDistance, ...
            this(indLayerBathy), ...
            this(indLayerEmission), ...
            this(indLayerEmissionBeam), ...
            'OrigineModeles', SonarDiagEmi_origine, ...
            'TypeDiagramme',  SonarDiagEmi_Type, ...
            'subx', subx, 'suby', suby);
        if ~flag
            return
        end
        a = a + X;
    end
    
    [X, flag] = sonar_diag_Tx(this(indImage), ...
        CasDepth, H, CasDistance, ...
        this(indLayerBathy), ...
        this(indLayerEmission), ...
        this(indLayerEmissionBeam), ...
        'OrigineModeles', SonarDiagEmi_origine_New, ...
        'TypeDiagramme',  SonarDiagEmi_Type_New, ...
        'subx', subx, 'suby', suby, 'listeSuby', listeSuby);
    if ~flag
        return
    end
    
    a = a - X;
    a = set(a, 'SonarDiagEmi_etat', 1, 'SonarDiagEmi_origine', SonarDiagEmi_origine_New, 'Sonar_DefinitionENCours');
    TitreOld = this(indImage).Name;
    if SonarDiagEmi_origine_New == 1
        if SonarDiagEmi_Type_New == 1
            a = set(a, 'SonarDiagEmi_ConstructTypeCompens', 1);
            a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagEmiCompensationConstructeurAnalytique');
        else
            a = set(a, 'SonarDiagEmi_ConstructTypeCompens', 2);
            a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagEmiCompensationConstructeurTable');
        end
    else
        if SonarDiagEmi_Type_New == 1
            a = set(a, 'SonarDiagEmi_IfremerTypeCompens', 1);
            a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagEmiCompensationIfremerAnalytique');
        else
            a = set(a, 'SonarDiagEmi_IfremerTypeCompens', 2);
            a = update_Name(a, 'Name', TitreOld, 'Append', 'DiagEmiCompensationIfremerTable');
        end
    end
end
