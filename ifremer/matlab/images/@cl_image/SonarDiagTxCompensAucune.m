% Cr�ation d'une image sonar compens�e en diagramme d'�mission
%
% Syntax
%   [flag, a] = SonarDiagTxCompensAucune(this, indImage, ...)
%
% Input Arguments
%   this : Instance de clc_image
%
% Name-only Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%   Mute : Pas de message d'erreur
%
% Output Arguments
%   flag : 1:�a c'est bien pass�, 0 : �a c'est pas bien pass�
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarDiagTxCompensAucune(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

a = [];

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if flag
    SonarDiagEmi_etat    = get(this(indImage), 'SonarDiagEmi_etat');
    SonarDiagEmi_origine = get(this(indImage), 'SonarDiagEmi_origine');
    TypeDiagramme        = get(this(indImage), 'SonarDiagEmi_ConstructTypeCompens');
    if SonarDiagEmi_etat == 2
        flag = 1;
        if ~Mute
            messageAboutCalibration('Tag', 'DiagTx')
        end
    else
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagTxConstructeur(this, indImage); %#ok
        if flag
            [X, flag] = sonar_diag_Tx(this(indImage), ...
                CasDepth, H, CasDistance, ...
                this(indLayerBathy), ...
                this(indLayerEmission), ...
                this(indLayerEmissionBeam), ...
                'OrigineModeles', SonarDiagEmi_origine, ...
                'TypeDiagramme', TypeDiagramme, ...
                'subx', subx, 'suby', suby);
            if ~flag
                return
            end
            
            a = extraction(this(indImage), 'suby', suby, 'subx', subx);
            a = a + X;
            a = set(a, 'SonarDiagEmi_etat', 2, 'Sonar_DefinitionENCours');
            TitreOld = this(indImage).Name;
            a = update_Name(a, 'Name', TitreOld, 'Append', 'TxCompensNo');
        end
    end
end
