% Calcul du DiagTx
%
% Syntax
%   [flag, a] = SonarDiagTxCompute(this, indImage, subx, suby)
%
% Input Arguments
%   this : Instance de clc_image
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=ca c'est bien passe, 0=ca c'est pas bien passe
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = SonarDiagTxCompute(this, indImage, subx, suby, nomZoneEtude, varargin)

[varargin, NomFic]                        = getPropertyValue(varargin, 'NomFic',                        []);
[varargin, askQuestionSignatureConfSonar] = getPropertyValue(varargin, 'askQuestionSignatureConfSonar', 1);
[varargin, MeanCompType]                  = getPropertyValue(varargin, 'MeanCompType',                  1);
[varargin, Parameters]                    = getPropertyValue(varargin, 'Parameters',                    []);
[varargin, SelectMode2EM2040]             = getPropertyValue(varargin, 'SelectMode2EM2040',             []);

if ~isempty(Parameters)
    bins        = Parameters{1};
    Selection   = Parameters{2};
    LayerMaskIn = Parameters{3}; %#ok<NASGU> % Red�finis plus loin dans paramsMasque !!!
    valMask     = Parameters{4}; %#ok<NASGU>
end

[varargin, NoPlots] = getFlag(varargin, 'NoPlots'); %#ok<ASGLU>

%% Test si Reflectivity

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    return
end

SonarDiagEmi_etat = get(this(indImage), 'SonarDiagEmi_etat');
if SonarDiagEmi_etat ~= 2
    str1 = 'Pour calculer les diagrammes, il faut que l''image soit non compens�e.';
    str2 = 'In order to compute DiagTx the image must be uncompensated.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% S�lection de la configuration du sondeur � traiter

[flag, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, strConf, listeSuby] = paramsModeIfSeveral(this(indImage), suby, 'OnlyOneConfiguration', 1, ...
    'askQuestionSignatureConfSonar', askQuestionSignatureConfSonar); %#ok<ASGLU>
if ~flag
    return
end
suby = listeSuby{1};

%%

% SonarMode_1 = [];
% SonarMode_2 = [];
SonarMode_3 = [];

%%

if isempty(Parameters)
    [flag, this, ~, bins, Selection] = paramsFonctionSonarDiagTx(this, indImage);
    if ~flag
        return
    end
end

[flag, indLayerMask, valMask] = paramsMasque(this, indImage); % Une seule valeur de masque � imposer
if ~flag
    return
end

%% V�rification si c'est le m�me mode sur tout suby

[flag, this(indImage), SounderMode] = SonarDescriptionAccordingToModes(this(indImage), 'suby', suby);
if ~flag
    return
end

%% Initialisation du nom de la courbe

SonarDescription = get_SonarDescription(this(indImage));
SonarMode_1 = get(SonarDescription, 'Sonar.Mode_1');
SonarMode_2 = get(SonarDescription, 'Sonar.Mode_2');
% SonarMode_3 = get(SonarDescription, 'Sonar.Mode_3') % Devra �tre utilis� pour l'EM20140
% strModes_1 = get_strModes_1(SonarDescription);
% strModes_2 = get_strModes_2(SonarDescription);
% strMode1 = strrep(strModes_1{SonarMode_1}, ' ', ''); % TODO : on enl�ve un blanc
% strMode1 = strrep(strMode1, 'swath', 'Swath'); % TODO : �a devrait arriver bien format�
% EnteteNomCourbe = sprintf('DiagTx_%s_Mode%s_%d', strMode1, strModes_2{SonarMode_2}, SounderMode);
EnteteNomCourbe = sprintf('DiagTx : %s', strConf);

%% Calcul des courbes

% EnteteNomCourbe = sprintf('DiagTx Mode %d', SounderMode);
i = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TODO
str1 = 'Nom de courbe de directivit� d''antennes en �mission';
str2 = 'Name of the TxDiag curve';
[flag, nomCourbe, Commentaire] = paramsStatsNameComment(this(indImage), ...
    EnteteNomCourbe, nomZoneEtude, indLayerMask, valMask(i), Lang(str1,str2), 'GUI', 0, ...
    'subx', subx, 'suby', suby);
if ~flag
    return
end

Tag = 'DiagTx';
if isempty(SonarMode_1)
    TagSup = [];
else
    TagSup = {'Sonar.Mode_1', SonarMode_1};
    if ~isempty(SonarMode_2)
        TagSup(2,:) = {'Sonar.Mode_2', SonarMode_2};
        if ~isempty(SonarMode_3)
            TagSup(3,:) = {'Sonar.Mode_3', SonarMode_3};
        end
    end
end

[a, bilan] = courbesStats(this(indImage), Tag, ...
    nomCourbe, Commentaire, [], this, Selection, ...
    'MeanCompType', MeanCompType, ...
    'LayerMask', this(indLayerMask), 'valMask', valMask(1), ...
    'bins', bins, 'subx', subx, 'suby', suby, ...
    'GeometryType', this(indImage).GeometryType, ...
    'TagSup', TagSup, 'SelectMode', SounderMode, 'NomFic', NomFic);
if isempty(a)
    flag = 0;
    return
end
this(indImage) = a;

%% Affichage de la courbe

if ~NoPlots
    numFig = 123456;
    CourbesStatsPlot(bilan, 'fig', numFig, 'Stats', 0);
end
