% TODO
%
% Syntax
%   [flag, b, alpha] = SonarEtatBS(a, indImage, ...)
%
% Input Arguments
%   a        : cl_image instances
%   indImage : Index of the reflectivity image
%
% Name-Value Pair Arguments
%   subx     : Sub-sampling in abscissa
%   suby     : Sub-sampling in ordinates
%   Range : TODO
%   alpha : TODO
%
% Output Arguments
%   flag  : 1=OK, 0=KO
%   b     : Updated instance of cl_image
%   alpha : TODO
%
% Examples
%   TODO
%   [flag, b, alpha] = SonarEtatBS(a, indImage)
%    imagesc(a)
%    imagesc(b)
%
% See also SonarBestBSConstructeur Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a, alpha] = SonarEtatBS(this, indImage, varargin)

[varargin, subx]                 = getPropertyValue(varargin, 'subx', []);
[varargin, suby]                 = getPropertyValue(varargin, 'suby', []);
[varargin, Mute]                 = getPropertyValue(varargin, 'Mute', 0);
% [varargin, Range] = getPropertyValue(varargin, 'Range', []);
[varargin, alpha]                = getPropertyValue(varargin, 'Absoption_dB/km',      []);
[varargin, choixDiagTx]          = getPropertyValue(varargin, 'DiagTx',               'RT'); % TODO : 'RT', 'SSc, 'model .mat'
[varargin, ChangeInsonifiedArea] = getPropertyValue(varargin, 'ChangeInsonifiedArea', 1);
[varargin, Bathy]                = getPropertyValue(varargin, 'Bathy',                []); %#ok<ASGLU>

flagModif = 0;
TitreOld  = this(indImage).Name;

%% Compensation du BS

SonarBS_etat = get(this(indImage), 'SonarBS_etat');
if SonarBS_etat ~= 2
    [flag, a] = SonarBSCompensNone(this, indImage, 'Bathy', Bathy, 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
    this(indImage) = a;
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    clear a
    flagModif = 1;
end

%% Compensation des diagrammes de directivit� en �mission

SonarDiagEmi_etat    = get(this(indImage), 'SonarDiagEmi_etat');
SonarDiagEmi_origine = get(this(indImage), 'SonarDiagEmi_origine');
SonarDiagEmi_Type    = get(this(indImage), 'SonarDiagEmi_ConstructTypeCompens');
if (SonarDiagEmi_etat == 1) && (SonarDiagEmi_origine == 1) && strcmp(choixDiagTx, 'RT')
    %messageForDebugInspection
    % Y'a rien � faire, la compensation de DiagTx est d�j� faite avec les
    % mod�les constructeur
else
    if (SonarDiagEmi_etat ~= 1) || (SonarDiagEmi_origine ~= 2)
        [flag, a] = SonarDiagTxCompens(this, indImage, SonarDiagEmi_origine, SonarDiagEmi_Type, 'subx', subx, 'suby', suby, 'Mute', 1); % 1=Analytique
        if flag
            if isempty(a)
                flag = 0;
                return
            end
            this(indImage) = a;
            subx = 1:a.nbColumns;
            suby = 1:a.nbRows;
            clear a
            flagModif = 1;
        end
    end
end

%% Compensation des diagrammes de directivit� en r�ception

%     SonarDiagRec_etat                   <-> '1=Compensated' | {'2=Not compensated'}
%     SonarDiagRec_origine                <-> {'1=Constructeur'} | '2=Ifremer'
%     SonarDiagRec_ConstructTypeCompens   <-> {'1=Analytique'} | '2=Table'
%     SonarDiagRec_IfremerTypeCompens     <-> {'1=Analytique'} | '2=Table'

%% Compensation de la TVG

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if flag
    [flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser] = params_SonarTVG_Compensation(this, indImage, 'Mute', Mute);
    if flag
        [flag, a] = sonar_TVG_Compensation(this(indImage), 'subx', subx, 'suby', suby, ...
            'Range', this(indLayerRange), ...
            'AbsorptionCoeffRT',   this(indLayerAbsorptionCoeffRT), ...
            'AbsorptionCoeffSSc',  this(indLayerAbsorptionCoeffSSc), ...
            'AbsorptionCoeffUser', this(indLayerAbsorptionCoeffUser));
        if flag
            this(indImage) = a;
        end
    end
end

%% Compensation de l'aire insonifi�e

if ChangeInsonifiedArea
    SonarAireInso_etat    = get(this(indImage), 'SonarAireInso_etat');    % '1=Compensated' | '2=Not compensated'
    SonarAireInso_origine = get(this(indImage), 'SonarAireInso_origine'); % '1=Constructeur' | '2=Ifremer'
    if (SonarAireInso_etat == 2) || ((SonarAireInso_etat == 1) && (SonarAireInso_origine ~= 3))
        [flag, a] = SonarAireInsoCompens(this, indImage, 3, 'subx', subx, 'suby', suby, 'Mute', Mute);
        if flag
            this(indImage) = a;
            clear a
            flagModif = 1;
        end
    end
end

%% Compensation des niveaux d'emission, du gain de traitement et du SH

%     SonarNE_etat                        <-> {'1=Compensated'} | '2=Not compensated'
%     SonarGT_etat                        <-> {'1=Compensated'} | '2=Not compensated'
%     SonarSH_etat                        <-> {'1=Compensated'} | '2=Not compensated'

a = this(indImage);
if flagModif
    flag = 1;
    a.Name = [TitreOld ' - Etat BS'];
else
    flag = 0;
    messageAboutCalibration('Tag', 'BS')
end
