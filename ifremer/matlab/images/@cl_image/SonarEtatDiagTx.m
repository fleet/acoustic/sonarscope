% Creation d'une image donnant les diagrammes d'emission
%
% Syntax
%   [flag, a] = SonarEtatDiagTx(this, indImage, subx, suby)
% 
% Input Arguments
%   this : Instance de clc_image
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarEtatDiagTx(this, indImage, subx, suby)

%% Test si l'image est de type "Reflectivity"

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if ~flag
    a = cl_image.empty;
    return
end

%% R�cup�ration du nom de l'image d'entr�e

TitreOld = this(indImage).Name;

%% Traitement de l'image

SonarBS_LambertCorrection = 1;
[flag, a] = SonarBSCompens(this, indImage, 2, 2, 1, SonarBS_LambertCorrection, 'subx', subx, 'suby', suby, 'Mute', 1);
if flag
    this(indImage) = a;
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    clear a
else
    return
end

[flag, a] = SonarDiagTxCompensAucune(this, indImage, 'subx', subx, 'suby', suby, 'Mute', 1);
if flag && ~isempty(a)
    this(indImage) = a;
    clear a
    flagModif = 1; %#ok<NASGU>
end

%% D�finition du nom du fichier de sortie

a = this(indImage);
if flag
    a.Name = [TitreOld ' - Data DiagTx'];
end
