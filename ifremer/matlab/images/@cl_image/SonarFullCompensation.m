% Creation d'une image donnant le meilleur BS fournissable par Ifremer
%
% Syntax
%   [flag, a] = SonarFullCompensation(this, indImage, ...)
%
% Input Arguments
%   this : Instance de clc_image
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=ca c'est bien passe, 0=ca c'est pas bien passe
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarFullCompensation(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin); %#ok<ASGLU>

%% Test si l'image est de type "Reflectivity"

identReflectivity   = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    a = [];
    return
end

%% R�cup�ration du nom de l'image d'entr�e

TitreOld = this(indImage).Name;

%% Traitement de la TVG

% TODO or not TODO ?
%{
[flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser, alphaUser] = ...
paramsFonctionSonarTVGCompensationSSc(this, indImage, 'Mute', 1);
if flag
[flag, a] = sonar_TVG_CompensationSSc(this(indImage), 'subx', subx, 'suby', suby, ...
'Range', this(indLayerRange), ...
'AbsorptionCoeffRT',   this(indLayerAbsorptionCoeffRT), ...
'AbsorptionCoeffSSc',  this(indLayerAbsorptionCoeffSSc), ...
'AbsorptionCoeffUser', this(indLayerAbsorptionCoeffUser), 'alphaUser', alphaUser);
if ~flag
messageAboutCalibration('Tag', 'TVG')
end
this(indImage) = a;
end
%}

%% Traitement du Backscatter

flagModif = 0;
SonarBS_LambertCorrection = 1;
[flag, a] = SonarBSCompens(this, indImage, 1, 2, 1, SonarBS_LambertCorrection, 'subx', subx, 'suby', suby, 'Mute', 1);
if isnan(flag)
    return
end
if flag
    this(indImage) = a;
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    clear a
    flagModif = 1;
end

%% Traitement des diagrammes de directivit� en �mission

SonarDiagEmi_etat    = get(this(indImage), 'SonarDiagEmi_etat');
SonarDiagEmi_origine = get(this(indImage), 'SonarDiagEmi_origine');
if (SonarDiagEmi_etat ~= 1) || (SonarDiagEmi_origine ~= 2)
    [flag, a] = SonarDiagTxCompens(this, indImage, 2, 1, 'subx', subx, 'suby', suby, 'Mute', 1); % 1=Analytique
    if flag
        this(indImage) = a; %#ok<NASGU> 
        %         subx = 1:a.nbColumns;
        %         suby = 1:a.nbRows;
        clear a
        flagModif = 1;
    end
end

%% D�finition du nom du fichier de sortie

if flagModif
    flag = 1;
    a .Name =  [TitreOld ' - Data Compensation'];
else
    flag = 0;
    messageAboutCalibration
end
