% Lancement de SonarScope � partir d'instances de cl_image
%
% Syntax
%   b = SonarScope(a)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   FigureName : Name of the figure
%   ButtonSave : 0=non modal, 1=modal
%
% Output Arguments
%   b : Instance de cli_image
%
% Remarks : La sortie est de type cli_ (classe d'interface). Si vous voulez
%           recuperer des instances de cl_image, il faut faire soit :
%           c = get(b,'Images'); ou c = get_Image(b, NumeroDeLImage)
%
% Examples
%
% See also cli_image SonarScope Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = SonarScope(this, varargin)

[varargin, FigureName] = getPropertyValue(varargin, 'FigureName', []);
[varargin, ButtonSave] = getPropertyValue(varargin, 'ButtonSave', 0);

a = cli_image('Images', this(:), varargin{:});
if nargout == 0
    SonarScope(a, 'ButtonSave', ButtonSave, 'FigureName', FigureName);
else
    a = editobj(a, 'ButtonSave', ButtonSave, 'FigureName', FigureName);
    varargout{1} = get(a, 'Images');
end
