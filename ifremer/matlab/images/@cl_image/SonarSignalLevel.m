% Creation d'une image donnant le meilleur BS fournissable par Ifremer
%
% Syntax
%   [flag, a] = SonarSignalLevel(this, indImage, ...)
%
% Input Arguments
%   this : Instance de clc_image
%
% Name-only Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=ca c'est bien passe, 0=ca c'est pas bien passe
%   a    : Instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = SonarSignalLevel(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

%% Test si l'image est de type "Reflectivity"

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    a = [];
    return
end

[varargin, indLayerRange] = getPropertyValue(varargin, 'indLayerRange', []); %#ok<ASGLU>

%% R�cuperation du nom de l'image d'entr�e

TitreOld = this(indImage).Name;

%% Compensation du BS

flagModif = 0;
SonarBS_etat = get(this(indImage), 'SonarBS_etat');
if SonarBS_etat ~= 2
    [flag, a] = SonarBSCompensNone(this, indImage, 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
    this(indImage) = a;
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    clear a
    flagModif = 1;
end

%% Compensation des diagrammes de directivit� en emission

[flag, a] = SonarDiagTxCompensAucune(this, indImage, 'subx', subx, 'suby', suby, 'Mute', 1);
if flag
    this(indImage) = a;
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    clear a
    flagModif = 1;
end

%% Compensation des diagrammes de directivit� en reception

%     SonarDiagRec_etat                   <-> '1=Compensated' | {'2=Not compensated'}
%     SonarDiagRec_origine                <-> {'1=Constructeur'} | '2=Ifremer'
%     SonarDiagRec_ConstructTypeCompens   <-> {'1=Analytique'} | '2=Table'
%     SonarDiagRec_IfremerTypeCompens     <-> {'1=Analytique'} | '2=Table'

%% Compensation du TVG

if isempty(indLayerRange)
    Range = [];
else
    Range = this(indLayerRange);
end
[flag, a] = sonar_TVG_NoCompensation(this(indImage), 'Range', Range);
if isempty(a)
    return
end
if flag
    this(indImage) = a;
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    clear a
    flagModif = 1;
end

%% Compensation de l'aire insonifi�e

SonarAireInso_etat = get(this(indImage), 'SonarAireInso_etat');    % '1=Compensated' | '2=Not compensated'
% SonarAireInso_origine = get(this(indImage), 'SonarAireInso_origine'); % '1=Constructeur' | '2=Ifremer'
if SonarAireInso_etat == 1
    [flag, a] = SonarAireInsoCompens(this, indImage, 2, 'subx', subx, 'suby', suby);
    if flag
        this(indImage) = a;
        clear a
        flagModif = 1;
    end
end

%% Compensation des niveaux d'�mission, du gain de traitement et du SH

% EN ATTENDANT DE FAIRE MIEUX : SonarNECompensAucune, ...

%     SonarNE_etat                        <-> {'1=Compensated'} | '2=Not compensated'
%     SonarGT_etat                        <-> {'1=Compensated'} | '2=Not compensated'
%     SonarSH_etat                        <-> {'1=Compensated'} | '2=Not compensated'
SonarNE_etat = get(this(indImage), 'SonarNE_etat');
if SonarNE_etat == 1
    Sonar = get(this(indImage), 'SonarDescription');
    NE = get_NE(Sonar);
    %     GT = get_GT(Sonar)
    this(indImage) = this(indImage) + NE(1);
    
    this(indImage) = set(this(indImage), 'SonarNE_etat', 2);
    flagModif = 1;
end

%% D�finition du nom du fichier de sortie

if flagModif
    flag = 1;
    a.Name = [TitreOld ' - Etat RSB'];
else
    flag = 0;
    messageAboutCalibration('Tag', 'NE')
end
