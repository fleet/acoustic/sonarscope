% Sonar_MoustacheRemoval ente 2 images
%
% Syntax
%   [c, status] = Sonar_MoustacheRemoval(a, b, ...)
%
% Input Arguments
%   a : Instance de cl_image de type "RayPathSampleNb"
%   b : Instance de cl_image de type "RxBeamAngle"
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   c      : Instances de cl_image contenant "Bathymetry", "AcrossDist"
%            et "AlongDist"
%   status : 1=tout c'est bien pass�
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : Refaire enti�rement cette fonction en travaillant sur le layer
% TwoWayTravelTime car ce qui est �crit ici ne peur fonctionner que si le
% fond est parfaitement plat.

function [flag, c] = Sonar_MoustacheRemoval(this, Angles, AngleMoustache, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controles de validite

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'DataType', identBathymetry, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

[XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Angles);
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

c(1) = this;
c(1) = replace_Image(c(1), NaN(length(suby), length(subx), 'single'));

c(2) = this;
c(2) = replace_Image(c(2), NaN(length(suby), length(subx), 'single'));

coef = 0.6 / 100;
M = length(suby);
hw = create_waitbar('Processing Moustache removal', 'N', M);
for k=1:M
    my_waitbar(k, M, hw);
    
    Z = this.Image(suby(k), subx);
    %     Teta = Angles.Image(subyb(k), subxb) - this.Sonar.Roll(subyb(k));
    Teta = Angles.Image(subyb(k), subxb);
    AbsTeta = abs(Teta);
    
    subNaNAvant = isnan(Z) | isnan(AbsTeta);
    subNonNaN = find(~subNaNAvant);
    [~, iMilieu] = min(AbsTeta(subNonNaN));
    iMilieu = subNonNaN(iMilieu);
    if isempty(iMilieu)
        continue
    end
    
    Rayon = Z ./ cosd(Teta);
    ZNadire = Rayon(iMilieu);
    
    sub = (Z > ZNadire) & (Rayon > ZNadire*(1+coef)) & (Rayon < ZNadire*(1-coef)) & (AbsTeta < AngleMoustache);
    if all(~sub)
        c(1).Image(k, :) = Z;
        c(2).Image(k, :) = subNaNAvant;
        continue
    end
    sub(iMilieu) = 0;
    fBab = sub(1:iMilieu-1);
    fTri = sub(iMilieu+1:end);
    if ~any(fBab) || ~any(fTri)
        c(1).Image(k, :) = Z;
        c(2).Image(k, :) = subNaNAvant;
        continue
    end
    
    if mod(k,100) == 1
        figure(4575);
        hAxe = subplot(1,1,1);
        
        x = Z .* tand(Teta);
        hold off;
        plot(hAxe, x, Z, '+b');
        hold on;
        plot(hAxe, x(iMilieu), Z(iMilieu), 'ok');
        plot(hAxe, x(sub), Z(sub), 'xr');
        grid on;
        
        Rayon = ZNadire*(1+coef);
        %         cteta = (-AngleMoustache:0.01:AngleMoustache) + this.Sonar.Roll(subyb(k));
        cteta = (-AngleMoustache:0.01:AngleMoustache);
        cy = cosd(cteta) * Rayon;
        cx = tand(cteta) * Rayon;
        plot(hAxe, cx, cy, 'k');
        
        Rayon = ZNadire*(1-coef);
        cy = cosd(cteta) * Rayon;
        cx = tand(cteta) * Rayon;
        plot(hAxe, cx, cy, 'k');
        
        title(sprintf('k=%d', k))
        
        drawnow
    end
    Z(sub) = NaN;
    
    subNaNApres = isnan(Z);
    
    c(1).Image(k, :) = Z;
    c(2).Image(k, :) = xor(subNaNAvant, subNaNApres);
end
my_close(hw, 'MsgEnd')

%% Mise � jour de coordonn�es

c(1) = majCoordonnees(c(1), subx, suby);
c(2) = majCoordonnees(c(2), subx, suby);

%% Calcul des statistiques

c(1) = compute_stats(c(1));
c(2) = compute_stats(c(2));

%% Rehaussement de contraste

c(1).TagSynchroContrast = num2str(rand(1));
c(2).TagSynchroContrast = num2str(rand(1));
CLim = [c(1).StatValues.Min c(1).StatValues.Max];
c(1).CLim          = CLim;
CLim = [c(2).StatValues.Min c(2).StatValues.Max];
c(2).CLim          = CLim;
c(1).ColormapIndex = 3;
c(2).ColormapIndex = 3;
c(1).DataType      = cl_image.indDataType('Bathymetry');
c(2).DataType      = cl_image.indDataType('Mask');
c(1).Unit          = 'm';
c(2).Unit          = 'm';

%% Compl�tion du titre

c(1).Name = [c(1).Name '-Moustache'];
c(1) = update_Name(c(1));
c(2).Name = [c(2).Name '-Mostache'];
c(2) = update_Name(c(2));

%% Par ici la sortie

flag = 1;
