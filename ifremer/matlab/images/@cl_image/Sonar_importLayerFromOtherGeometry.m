function [flag, a] = Sonar_importLayerFromOtherGeometry(this, indImage, subx, suby)

a    = cl_image.empty;
flag = 0;

TC = this(indImage).GeometryType;
if TC == cl_image.indGeometryType('PingSamples')
    [flag, indReceptionBeam, indLayerRange, indLayers] = paramsMBESImportLayer2PingSamples(this, indImage);
    if ~flag
        return
    end
    
    [XLim, YLim, subx, suby, subxRxBeamIndex, subyRxBeamIndex] = intersectionImages(this(indImage), subx, suby, ...
        this(indReceptionBeam), this(indLayerRange), 'SameSize'); %#ok<ASGLU>
    
    [flag, a] = sonar_bathyFais2SonarD(this(indReceptionBeam), ...
        this(indLayerRange), ...
        this(indLayers), ...
        'subx', subxRxBeamIndex, 'suby', subyRxBeamIndex);
    
elseif TC == cl_image.indGeometryType('PingRange')
    my_warndlg('Pour JMA : Verifier si ce traitement est correct : GJHGJHRLHKJ', 1);
    
    % Copie de PingSamples
    [flag, indReceptionBeam, indLayerRange, indLayers] = paramsMBESImportLayer2PingSamples(this, indImage);
    if ~flag
        return
    end
    [flag, a] = sonar_bathyFais2SonarD(this(indReceptionBeam), ...
        this(indLayerRange), ...
        this(indLayers), ...
        'subx', subx, 'suby', suby);
    
elseif TC == cl_image.indGeometryType('PingAcrossDist')
    [flag, indTxAngle, indAcrossDistance, indRange, resolutionX, indLayers, subEmissionAngle, MasquageDual] ...
        = paramsMBESImportLayer2PingAcrossDist(this, indImage, suby);
    if ~flag
        return
    end
    [flag, a] = sonar_MBES_projectionX(this(indLayers(1)), ...
        this(indTxAngle), ...
        this(indAcrossDistance), ...
        this(indRange), ...
        this(indLayers(2:end)), ...
        'resolutionX', resolutionX, ...
        'suby', subEmissionAngle, 'MasquageDual', MasquageDual);
    
elseif TC == cl_image.indGeometryType('PingAcrossSample')
    [flag, indRxBeamIndexPingAcrossSample, AcrossDistPingBeam, indLayersPingBeam] = paramsMBESImportLayer2PingAcrossSample(this, indImage);
    if ~flag
        return
    end
    [flag, a] = sonar_PingBeam2PingAcrossSample(this(indLayersPingBeam), ...
        this(indRxBeamIndexPingAcrossSample), ...
        this(AcrossDistPingBeam));
end
