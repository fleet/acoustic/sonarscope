function flag = SurveyReport_AddLatLongBathymetry(this, nomFicAdoc, Title, Comment, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', cl_image.indDataType('Bathymetry'));
if ~flag
    return
end

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);

%% Extraction �ventuelle et renommage de l'image

Tag = ''; %num2str(randi(100000));
ImageName = this.Name;
ImageName = cl_image.extract_ImageNameVersion(ImageName);
ImageName = cl_image.extract_ImageNameTime(ImageName);

this = extraction(this, 'subx', subx, 'suby', suby);
this.Name = ImageName;

%% Insertion de la bathymetrie

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, ...
    nomFicPngInverseVideo, nomFicKmzInverseVideo] = exportMosaicsFig2PngAndFigFiles(this, nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
    'Title1', Title, 'Comment', Comment, ...
    'nomFicPngInverseVideo', nomFicPngInverseVideo, 'nomFicKmzInverseVideo', nomFicKmzInverseVideo);

%% Insertion de la bathym�trie ombr�e

b = sunShadingGreatestSlope(this, 1);
[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(b, nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag);

%% Insertion des pentes max

[b, flag] = slopeMax(this);
if flag
    b.CLim = '0.5%';
    b.ColormapIndex = 20;
    
    [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(b, nomDirSummary, SurveyName, Tag);
    
    AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag);
end

%% R�ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);
