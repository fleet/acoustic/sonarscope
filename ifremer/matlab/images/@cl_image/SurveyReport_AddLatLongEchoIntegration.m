function SurveyReport_AddLatLongEchoIntegration(this, nomFicAdoc, Title, Comment, Thresholds, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', cl_image.indDataType('Reflectivity'));
if ~flag
    return
end

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);

%% Extraction �ventuelle et renommage de l'image

Tag = ''; %num2str(randi(100000));
ImageName = this.Name;
ImageName = cl_image.extract_ImageNameVersion(ImageName);
ImageName = cl_image.extract_ImageNameTime(ImageName);

CLim = this.CLim;
this = extraction(this, 'subx', subx, 'suby', suby);

this.Name = ImageName;
this.CLim = CLim;

%% Insertion de l'image telle que pr�par�e par l'utilisateur

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] ...
    = exportMosaicsFig2PngAndFigFiles(this, nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
    'Title1', Title, 'Comment', Comment);

%% Insertion des images masqu�es

Thresholds = floor(Thresholds);
nbThresholds = length(Thresholds);
for k=1:nbThresholds
    valInterval = [Thresholds(k)-eps Inf];
    Mask = ROI_auto(this, 1, 'Mask', [], [], valInterval, 1);
    StatValues = Mask.StatValues;
    if StatValues.Max > 0
        b = masquage(this, 'LayerMask', Mask, 'valMask', 1, 'NoWaitbar', 1);
        MaskedImageName = sprintf('Threshold%d-%s', Thresholds(k), ImageName);
        b.Name = MaskedImageName;
        
        [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, MaskedImageName] ...
            = exportMosaicsFig2PngAndFigFiles(b, nomDirSummary, SurveyName, Tag);
        
        AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, MaskedImageName, Tag, ...
            'Comment', Comment);
    end
end

%% R�ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);
