function SurveyReport_AddLatLongReflectivity(this, nomFicAdoc, Title, Comment, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', cl_image.indDataType('Reflectivity'));
if ~flag
    return
end

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);

%% Extraction �ventuelle et renommage de l'image

Tag = ''; %num2str(randi(100000));
ImageName = cl_image.extract_ImageNameVersion(this.Name);
ImageName = cl_image.extract_ImageNameTime(ImageName);

this = extraction(this, 'subx', subx, 'suby', suby);

this.Name = ImageName;

%% Insertion de l'image et de son iverse vid�o

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, ...
    nomFicPngInverseVideo, nomFicKmzInverseVideo] = exportMosaicsFig2PngAndFigFiles(this, nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
    'Title1', Title, 'Comment', Comment, ...
    'nomFicPngInverseVideo', nomFicPngInverseVideo, 'nomFicKmzInverseVideo', nomFicKmzInverseVideo);

%% R�ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);
