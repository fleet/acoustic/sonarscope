% Symetrisation �ventuelle de la table de couleur

function CLim = SymetrieCLim(this, CLim)
        
if this.SymetrieColormap(this.ColormapIndex,1) == 1
    Max  =  abs(max(CLim));
    Min  = -abs(min(CLim));
    Mean = mean(abs(CLim));
    
    str1 = sprintf('La table de couleur utilis�e pour cette image impose des limites oppos�es. Les limites trouv�es sont : %s  Max = %f  Min = %f  Mean = %f  Quelles limites voulez-vous utiliser ?', ...
        num2strCode(CLim), Max, Min, Mean);
    str2 = sprintf('The selected colormap requires symetric values. The entry limits are %s  Max = %f  Min = %f  Mean = %f  Which limits do you want to apply', ...
        num2strCode(CLim), Max, Min, Mean);
    str{1} = ['[-Max  Max] : ' num2str([-Max Max])];
    str{2} = ['[ Min  -Min] : ' num2str([Min -Min])];
    str{3} = ['Alternative  : ' num2str([-Mean Mean])];
%     str{3} = ['[(-Max+Min)/2  (Max-Min)/2] ' num2str([-Mean Mean])];
    [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');

    if flag
        switch rep
            case 1
                CLim = [-Max Max];
            case 2
                CLim = [Min -Min];
            case 3
                CLim = [-Mean Mean];
        end
    else
%         M = max(abs(CLim));
        CLim = [-Max Max];
    end
end
