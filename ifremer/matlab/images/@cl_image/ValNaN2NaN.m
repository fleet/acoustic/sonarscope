function this = ValNaN2NaN(this)

if ~isnan(this.ValNaN)
    subNan = findNaN(this);

    if ~isa(this.Image(1), 'double')
        if ~isa(this.Image(1), 'single')
            this.Image = single(this.Image(:,:,:));
        end
    end

    this = replace_Image(this, this.Image);
    this.Image(subNan) = NaN;
    this.ValNaN = NaN;
end
