% Visualisation d'une image par l'outil imtool
%
% Syntax
%   ViewImage(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Examples
%   a = cl_image('Image', Lena)
%   ViewImage(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ViewImage(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

if this.XDir == 1 && ((this.x(end)-this.x(1)) > 0)
    if (this.YDir == 1) && ((this.y(end)-this.y(1)) > 0)
        I = this.Image(fliplr(suby),subx,:);
    else
        I = this.Image(suby,subx,:);
    end
else
    if (this.YDir == 1) && ((this.y(end)-this.y(1)) > 0)
        I = this.Image(fliplr(suby),fliplr(subx),:);
    else
        I = this.Image(suby,fliplr(subx),:);
    end
end


switch this.ImageType
    case 1 % Intensite
        switch class(I)
            case {'int32'; 'uint32'}
                I = single(I); % double ???
        end
        h = my_imtool(I, this.CLim);
        map = this.Colormap;
        if this.Video == 2
            if this.SymetrieColormap(this.ColormapIndex,2)
                n = floor(size(map,1) / 2);
                map = [map(n+1:end,:) ; map(1:n, :)];
            else
                map = flipud(map);
            end
        end
        set(h, 'Colormap', map);
        
    case 2 % RGB
        h = my_imtool(I);%#ok
        
    case 3 % Indexee
        h = my_imtool(I, this.CLim);
        set(h, 'Colormap', this.Colormap);
        
    case 4 % Binaire
        h = my_imtool(I, this.CLim);
        set(h, 'Colormap', this.Colormap);
end
