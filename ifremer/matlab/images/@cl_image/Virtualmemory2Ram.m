% Put image in RAM
%
% Syntax
%   a = Virtualmemory2Ram(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   LogSilence : 1=message in the console, 0=no message (Default : 0)
%
% Output Arguments
%   a : Instance(s) of cl_image
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%     [str, MemMapFileName] = where(a)
%     a = Ram2Virtualmemory(a);
%     [str, MemMapFileName] = where(a)
%   a = Virtualmemory2Ram(a);
%     [str, MemMapFileName] = where(a)
%
% See also cl_image/where cl_image/Ram2Virtualmemory cl_image/optimiseMemory cl_memmapfile Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function that = Virtualmemory2Ram(this)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('Virtualmemory2Ram'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = Virtualmemory2Ram_unit(this(k));
end  
my_close(hw)


function this = Virtualmemory2Ram_unit(this)

if isa(this.Image, 'cl_memmapfile')
    this.Image = this.Image(:,:,:);
end

if ~isempty(this.Sonar)
    fields = fieldnames(this.Sonar);
    for k=1:length(fields)
        if isa(this.Sonar.(fields{k}), 'cl_memmapfile')
            this.Sonar.(fields{k}) = this.Sonar.(fields{k})(:,:);
        end
    end
end

if isa(this.HistoValues, 'cl_memmapfile')
    this.HistoValues = this.HistoValues(:,:);
end
if isa(this.HistoValues, 'cl_memmapfile')
    this.HistoCentralClasses = this.HistoCentralClasses(:,:);
end
