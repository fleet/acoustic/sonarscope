function WC_InspectPolarEchogramDepthAcrossDist(this, nomFicXml)

if ~iscell(nomFicXml)
    nomFicXml = {nomFicXml};
end

Fig = [];
for kFic=1:length(nomFicXml)
    [~, ~, Ext] = fileparts(nomFicXml{kFic});
    switch Ext
        case '.xml'
            Fig = plot_navigation_WCDepthAcrossDist(this, nomFicXml{kFic}, 'Fig', Fig);
        case '.nc'
%             Fig = []; % Une figure � la fois pour le moment
            Fig = plot_navigation_WCDepthAcrossDistNetcdf(this, nomFicXml{kFic}, kFic, 'Fig', Fig);
    end
end
