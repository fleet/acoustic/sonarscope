function [flag, a] = WC_MatrixTrackBall(this, nomFicXml, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

if ~iscell(nomFicXml)
    nomFicXml = {nomFicXml};
end
str1 = 'Traitement de la bille sur l''ensemble des fichiers .all';
str2 = 'Processing ball track on .all files.';
N = length(nomFicXml);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, a(:,k)] = WC_MatrixTrackBall_unitaire(this, subx, suby, nomFicXml{k}); %#ok<AGROW>
    if ~flag
        return
    end
end
my_close(hw)
a = a(:);


function [flag, a] = WC_MatrixTrackBall_unitaire(this, subx, suby, nomFicXml)

[flag, DataAlongNav] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1);
if ~flag
    return
end

if isempty(subx)
    x = this.x;
else
    x = this.x(subx);
end
xMin = min(x);
xMax = max(x);

if isempty(suby)
    y = this.y;
else
    y = this.y(suby);
end
yMin = min(y);
yMax = max(y);

iPing          = DataAlongNav.iPing(:);
AcrossDistance = DataAlongNav.AcrossDistance;
Depth          = DataAlongNav.Depth;

nbPings = length(iPing);

[~, Name] = fileparts(nomFicXml);
switch this.GeometryType
    case cl_image.indGeometryType('DepthAcrossDist')
        sub1 = find((Depth >= yMin) & (Depth <= yMax));
        sub2 = find((AcrossDistance >= xMin) & (AcrossDistance <= xMax));
        
        a1 = max(DataAlongNav.Reflectivity(sub1,sub2,:), [], 1);
        a1 = squeeze(a1);
        a2 = max(DataAlongNav.Reflectivity(sub1,sub2,:), [], 2);
        a2 = squeeze(a2);
        a3 = max(DataAlongNav.Reflectivity(sub1,sub2,:), [], 3);
        a3 = squeeze(a3);
        
        [~, indAcrossDist] = max(a1, [], 1);
        %         figure; plot(AcrossDistance(sub2(indAcrossDist))); grid on;
        
        [~, indDepth] = max(a2, [], 1);
        %         figure; plot(Depth(sub1(indDepth))); grid on;
        
        AmpNoise1 = median(a1, 1, 'omitnan');
        %         AmpBille2 = max(a2, [], 1);
        AmpBille1 = max(a1, [], 1);
        
        x1 = double(iPing);
        y1 = double(AcrossDistance(sub2));
        x2 = double(iPing);
        y2 = double(Depth(sub1));
        x3 = double(AcrossDistance(sub2));
        y3 = double(Depth(sub1));
        
        TxAngle = NaN(nbPings, length(y1));
        %         Sigma   = NaN(nbPings, 1, 'single');
        for k=1:nbPings
            z = Depth(sub1(indDepth(k)));
            TxAngle(k,:) = atand(y1/abs(z));
            %            Signal = DataAlongNav.Reflectivity(sub1(indDepth(k)),sub2,k);
            %            subLobe = find((Signal > (AmpBille1(k)-25)) & (abs(TxAngle(k,:)-TxAngle(k,indAcrossDist(k))) < 2));
            
            %            signalLobe = Signal(subLobe);
            %            anglesLobe = TxAngle(k,subLobe);
            %            [Ex, Sigma(k)] = CenterGravity(anglesLobe, signalLobe);
            
            
            %{
params(1) = ClParametre('Name', 'Mean', ...
'MinValue', Ex - 2 * Sigma(k), ...
'MaxValue', Ex + 2 * Sigma(k), ...
'Value',    Ex, ...
'Mute',   1);

params(2) = ClParametre('Name', 'Sigma', ...
'MinValue', 0.5 * Sigma(k), ...
'MaxValue', 2 * Sigma(k), ...
'Value',    Sigma(k), ...
'Mute',   1);

modeleGauss = ClModel('Name', 'my_normpdf', 'Params', params, 'XData', anglesLobe);

optim = ClOptim('XData', anglesLobe, 'YData', signalLobe, ...
'xlabel', 'Angles', ...
'ylabel', 'Reflectivity', ...
'Name', 'Reflectivity', ...
'models', modeleGauss);
optim = set(optim, 'models', modeleGauss);

optim.editProperties();
optim.plot
optim.optimize();
optim.plot
            %}
            
            
            %            if mod(k,100) == 0
            %                figure(34686); PlotUtils.createSScPlot(TxAngle(k,:),Signal, '-*'); grid on; title(num2str(k)); hold on; PlotUtils.createSScPlot(TxAngle(k,subLobe), Signal(subLobe), 'r'); hold off;
            %            end
        end
        
        %% Tac� de la figure
        
        Fig = figure('Name', [' - ' Name]);
        subplot(3,10,1); imagesc(x3, y3, a3); axis xy; ylabel('Depth');
        
        h2 = subplot(3,10,2:9);  imagesc(x2, y2, a2); axis xy; title(['Vertical plan - ' Name], 'Interpreter', 'none'); xlabel('Ping numbers');
        hc = subplot(3,10,10); plot_colorbarOnly(hc, DataAlongNav.CLim)
        
        h3 = subplot(3,10,12:19); imagesc(x1, y1, a1);
        axis xy; title('Horizontal plan'); xlabel('Ping numbers'); ylabel('Across Distance')
        set(h3, 'YDir', 'reverse')
        hc = subplot(3,10,20); plot_colorbarOnly(hc, DataAlongNav.CLim)
        
        h4 = subplot(3,10,22:29); PlotUtils.createSScPlot(iPing, AmpNoise1, 'k');
        hold on; PlotUtils.createSScPlot(iPing, AmpBille1, 'b'); grid on; axis tight;
        YLim = get(h4, 'YLim');
        d = (YLim(2)-YLim(1)) / 10;
        set(h4, 'YLim', [YLim(1)-d YLim(2)+d])
        xlabel('Ping numbers'); ylabel('TS'); title('TS on the ball and Noise level (dB)')
        %         h5 = subplot(4,10,32:40); PlotUtils.createSScPlot(iPing, Sigma, '*'); title('Estimated Beam Width (deg)'); xlabel('Ping numbers'); ylabel('Beam Width')
        linkaxes([h2 h3 h4], 'x')
        %         linkaxes([h1 h2], 'y')
        
        %% Sauvegarde en fichier .tif
        
        [nomDir, nomFic] = fileparts(nomFicXml);
        nomFicTif = fullfile(nomDir, [nomFic '.tif']);
        ScreenSize = get(0, 'ScreenSize');
        w = get(Fig, 'Position');
        set(Fig, 'Position', centrageFig(ScreenSize(3), ScreenSize(4)));
        fig2fic(Fig, nomFicTif)
        set(Fig, 'Position', w);
        
        %% Sauvegarde en fichier .fig
        
        [nomDir, nomFic] = fileparts(nomFicXml);
        nomFicFig = fullfile(nomDir, [nomFic '.fig']);
        saveas(Fig, nomFicFig)
        
        str1 = sprintf('"%s" et "%s" ont �t� cr�es', nomFicTif, nomFicFig);
        str2 = sprintf('"%s" and "%s" have been created', nomFicTif, nomFicFig);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'BallTrackDisplaySuccesfull');
end
DT = cl_image.indDataType('Reflectivity');
Time = cl_time('timeIfr', DataAlongNav.Date(:), DataAlongNav.Hour(:));

N = length(indAcrossDist);
BilleLat = NaN(N,1);
BilleLon = NaN(N,1);
for k=1:N
    BilleLat(k) = DataAlongNav.LatitudeTop( k, sub2(indAcrossDist(k)));
    BilleLon(k) = DataAlongNav.LongitudeTop(k, sub2(indAcrossDist(k)));
end
%{
figure; plot(BilleLon, BilleLat); grid on; hold on;
plot(DataAlongNav.LongitudeTop(:,130), DataAlongNav.LatitudeTop(:,130), 'k')
plot(DataAlongNav.LongitudeTop(:,160), DataAlongNav.LatitudeTop(:,130), 'k')
plot(DataAlongNav.LongitudeTop(:,190), DataAlongNav.LatitudeTop(:,130), 'k')
%}

TagSynchroAcrossDist = [Name '-AcrossDist'];
TagSynchroDepth      = [Name '-Depth'];
TagSynchroYPing      = [Name '-Ping'];

k = 1;
a(k) = cl_image('Image', TxAngle, 'x', y1, 'y', x1, 'Name', Name, ...
    'DataType', cl_image.indDataType('TxAngle'), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'), ...
    'TagSynchroX', TagSynchroAcrossDist, 'TagSynchroY', TagSynchroYPing);
a(k) = update_Name(a(k));
a(k) = set(a(k), 'SonarTime', Time);
a(k) = set(a(k), 'SonarPingCounter', DataAlongNav.iPing(:));

k = k + 1;
a(k) = cl_image('Image', a1', 'x', y1, 'y', x1, 'Name',  Name, ...
    'DataType', DT, 'GeometryType', cl_image.indGeometryType('PingAcrossDist'), ...
    'TagSynchroX', TagSynchroAcrossDist, 'TagSynchroY', TagSynchroYPing);
a(k) = update_Name(a(k));
a(k) = set(a(k), 'SonarTime', Time);
a(k) = set(a(k), 'SonarPingCounter', DataAlongNav.iPing(:));

k = k +1;
a(k) = cl_image('Image', a2', 'x', y2, 'y', x2, 'Name', Name, ...
    'DataType', DT, 'GeometryType', cl_image.indGeometryType('PingDepth'), ...
    'TagSynchroX', TagSynchroDepth, 'TagSynchroY', TagSynchroYPing, 'XDir', 2);
a(k) = update_Name(a(k));
a(k) = set(a(k), 'SonarTime', Time);
a(k) = set(a(k), 'SonarPingCounter', DataAlongNav.iPing(:));
a(k) = set(a(k), 'SonarFishLatitude',  BilleLat);
a(k) = set(a(k), 'SonarFishLongitude', BilleLon);
a(k) = set(a(k), 'SonarImmersion', -abs(DataAlongNav.Immersion(:)));

k = k + 1;
a(k) = cl_image('Image', a3,  'x', x3, 'y', y3, 'Name', Name, ...
    'DataType', DT, 'GeometryType', cl_image.indGeometryType('DepthAcrossDist'), ...
    'TagSynchroX', TagSynchroAcrossDist, 'TagSynchroY', TagSynchroAcrossDist);
a(k) = update_Name(a(k));


a = a(:);
% SonarScope(a)

%{
a(k) = set(a(k), 'SonarFishLatitude',  Images.LatitudeTop(:,i));
a(k) = set(a(k), 'SonarFishLongitude', Images.LongitudeTop(:,i));
a(k).CLim = Data.CLim;
a(k) = Ram2Virtualmemory(a(k));

xBab: [2576x1 cl_memmapfile]
xTri: [2576x1 cl_memmapfile]
Depth: [1x409 single]
Immersion: [2576x1 cl_memmapfile]
lonBab: [2576x1 cl_memmapfile]
latBab: [2576x1 cl_memmapfile]
lonCentre: [2576x1 cl_memmapfile]
latCentre: [2576x1 cl_memmapfile]
lonTri: [2576x1 cl_memmapfile]
latTri: [2576x1 cl_memmapfile]
lonBabDepointe: [2576x1 cl_memmapfile]
latBabDepointe: [2576x1 cl_memmapfile]
lonTriDepointe: [2576x1 cl_memmapfile]
latTriDepointe: [2576x1 cl_memmapfile]
AcrossDistance: [1x263 single]
LatitudeTop: [2576x263 cl_memmapfile]
LongitudeTop: [2576x263 cl_memmapfile]
%}
