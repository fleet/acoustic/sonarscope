% Fill XML structure for WC in DepthAcrossDist geom�try or 3DMatrix

function [flagOK, StructXML] = WC_WriteDepthAcrossDistEchograms(...
    Echograms, StructXML, Folders, CLimEchograms, ...
    TypeWCOutput, TransducerDepth, MaskBeyondDetection, Mask, ...
    kPing, TimePing, kWCPing, LatitudePing, LongitudePing, HeadingPing, nbAcross, ...
    flagTideCorrection, Detection, varargin)

[varargin, identSondeur] = getPropertyValue(varargin, 'identSondeur', []);
[varargin, Heave]        = getPropertyValue(varargin, 'Heave',        []); %#ok<ASGLU>
% TODO : passer en argument quand tous les appels seront valid�s
    
flagOK = 1;
if TypeWCOutput == 1 % Images individuelles
    if MaskBeyondDetection && ~isempty(Mask)
        Echograms.Image(Mask) = NaN;
    end
    [flag, WC_Coord] = WC_writeImagePng(Echograms, CLimEchograms, ...
        kPing, LatitudePing, LongitudePing, HeadingPing, Folders, TimePing, ...
        'Crop', 1, 'identSondeur', identSondeur); % TODO : rajouter subx et suby
    if flag
        MinRangeInMeters = min(Detection.RangeInMeters); % Pourra �tre supprim� quand Detection sera op�rationnel
        StructXML = WC_Coord2WC(WC_Coord, StructXML, kWCPing, TransducerDepth, flagTideCorrection, ...
            'MinRangeInMeters', MinRangeInMeters, 'Detection', Detection); % MinRangeInMeters en PN/PV pour l'instant mais devrait �tre pass�
        % en variable tout le temps
        
    else
        flagOK = 0;
    end
else % Matrices 3D
    if MaskBeyondDetection && ~isempty(Mask)
        Echograms.Image(Mask) = NaN;
    end
    [flag, WC_Coord] = WC_writeImageCube3D(Echograms, Folders, kWCPing, ...
        kPing, LatitudePing, LongitudePing, HeadingPing, TimePing, Mask);
    if flag
        StructXML = WC_Coord2WC(WC_Coord, StructXML, kWCPing, TransducerDepth, flagTideCorrection, ...
            'nbAcross', nbAcross, 'Heave', Heave, 'Detection', Detection);
    else
        flagOK = 0;
    end
end
