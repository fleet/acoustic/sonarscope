function ping = WC_calcul_position(this, iPing, Latitude, Longitude, Heading)

[x, y] = latlon2xy(this.Carto, Latitude, Longitude);
for i=1:length(x)
    % [convergence, scaleFactor] = get_convergence(this.Carto, Latitude, Longitude);
    % c = cosd(90-Heading);
    % s = sind(90-Heading);
    c = cosd(Heading(i)); % -convergence : Non +convergence : a tester, 0 � tester Pas sur du tout sur
    s = sind(Heading(i));
    C = [this.x(1) 0; this.x(end) 0] * [c s; -s c];
    % C = C * scaleFactor; % scaleFactor Pas sur du tout
    X = [x(i)+C(1,1) x(i) x(i)+C(2,1)];
    Y = [y(i)+C(1,2) y(i) y(i)+C(2,2)];
    [lat ,lon] = xy2latlon(this.Carto, X', Y');
    
    % FigUtils.createSScFigure(8786); PlotUtils.createSScPlot(Longitude, Latitude, 'o'); grid on; axisGeo
    % hold on; PlotUtils.createSScPlot(lon, lat, 'x'); grid on;
    
%     if isnan(lon(1))
%         ping.y(i)         = this.y(1);
%         ping.lonBab(i)    = lon(1);
%         ping.latBab(i)    = lat(1);
%         ping.lonCentre(i) = lon(2);
%         ping.latCentre(i) = lat(2);
%         ping.Tri(i)       = lon(3);
%         ping.Tri(i)       = lat(3);
%     else
%         ping.y(i)         = this.y(i);
        ping.lonBab(i)    = lon(1);
        ping.latBab(i)    = lat(1);
        ping.lonCentre(i) = lon(2);
        ping.latCentre(i) = lat(2);
        ping.lonTri(i)    = lon(3);
        ping.latTri(i)    = lat(3);
        ping.iPing(i)     = iPing(i);
%     end
end
