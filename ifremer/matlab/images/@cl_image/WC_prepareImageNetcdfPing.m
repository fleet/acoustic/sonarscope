function [flag, DataPingMat] = WC_prepareImageNetcdfPing(LayerRaw, LayerComp, Mask, sliceNames, TransducerDepth, ...
    kGroup,  Latitude, Longitude, Heading, TimePing, varargin)

[varargin, subx]                  = getPropertyValue(varargin, 'subx',                  []);
[varargin, suby]                  = getPropertyValue(varargin, 'suby',                  []);
[varargin, Crop]                  = getPropertyValue(varargin, 'Crop',                  1);
[varargin, identSondeur]          = getPropertyValue(varargin, 'identSondeur',          []);
[varargin, BypassEmptyNavigation] = getPropertyValue(varargin, 'BypassEmptyNavigation', false);
[varargin, MaskBeyondDetection]   = getPropertyValue(varargin, 'MaskBeyondDetection',   false);
[varargin, Detection]             = getPropertyValue(varargin, 'Detection',             []);
[varargin, Mute]                  = getPropertyValue(varargin, 'Mute',                  0); %#ok<ASGLU>

DataPingMat = [];

Coord   = [];
Heading = double(Heading);

if MaskBeyondDetection && ~isempty(Mask)
    LayerRaw.Image(Mask) = NaN;
end

%% R�duction de l'image o� il y a de la donn�e

if ~isempty(subx) && ~isempty(suby)
    LayerRaw = extraction(LayerRaw, 'suby', suby, 'subx', subx, 'NoStats');
elseif Crop
    I = LayerRaw.Image;
    subNonNaN = ~isnan(I);
    subx = find(sum(subNonNaN, 1) ~= 0);
    suby = find(sum(subNonNaN, 2) ~= 0);
    [LayerRaw, flag] = extraction(LayerRaw, 'suby', suby, 'subx', subx, 'NoStats');
    if ~flag
        return
    end
    
    % Ajout JMA le 04/11/2020
    [LayerComp, flag] = extraction(LayerComp, 'suby', suby, 'subx', subx, 'NoStats');
    if ~flag
        return
    end
    Mask = Mask(suby,subx);
end

% Along distances r�elles
% TODO : ATTENTION : extraction ne remet pas � jour SampleBeamData.AcrossDist(subx)
% et SampleBeamData.AlongDist, attention aux effets de bord !!!!!!!!!!!
% il faut donc extraire avec les subx avant. Il est urgent de corriger cela TODO
across = double(LayerRaw.Sonar.SampleBeamData.AcrossDist);
along  = double(LayerRaw.Sonar.SampleBeamData.AlongDist);
depth  = double(LayerRaw.Sonar.SampleBeamData.Depth);
% figure; PlotUtils.createSScPlot(across, along, '-*'); grid on

%% Calcul des coordonn�es

if isnan(Latitude) && isnan(Longitude) % Pour �viter bug en action R&D sans nav
    Latitude  = 0;
    Longitude = 0;
end

sub = ~isnan(across) & ~isnan(along);
XData = across(sub);
YData = along(sub);
depth = depth(sub);

%%

x0 = LayerRaw.Sonar.Ship.Arrays.Transmit.X;
y0 = LayerRaw.Sonar.Ship.Arrays.Transmit.Y;
if isempty(x0)
    switch identSondeur
        case {13003; 13016}
            x0 = 0;
            y0 = 0;
        otherwise
            if ~Mute
                str1 = 'Les param�tres d''installation ne sont pas d�finis';
                str2 = 'The "Installation parameters" are not defined';
                my_warndlg(Lang(str1,str2), 0, 'NoInstallationParameters');
            end
%             flag = 0;
%             return
            x0 = 0;
            y0 = 0;
    end
end
z0 = 0; % LayerRaw.Sonar.SampleBeamData.TransducerDepth ou LayerRaw.Sonar.Ship.Arrays.Transmit.Z ???;  % TODO : position en l'antenne
ZBottom = LayerRaw.y(1);
XDataBottomImage = x0 + (XData - x0) .* ((ZBottom - z0) ./ (depth - z0));
YDataBottomImage = y0 + (YData - y0) .* ((ZBottom - z0) ./ (depth - z0));

if isempty(XData)
    Coord.lonBabDepointe = NaN;
    Coord.latBabDepointe = NaN;
    Coord.lonTriDepointe = NaN;
    Coord.latTriDepointe = NaN;
    if ~BypassEmptyNavigation
        flag = 0;
        return
    end
end

%%

E2 = get(LayerRaw.Carto, 'Ellipsoide.Excentricite') ^ 2;
A  = get(LayerRaw.Carto, 'Ellipsoide.DemiGrandAxe');
[lat, lon] = calculLatLonFromAlongAcross(E2, A, Latitude, Longitude, Heading, YDataBottomImage, XDataBottomImage);
if ~BypassEmptyNavigation
    if isnan(lon(1))
        flag = 0;
        return
    end
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(lon, lat, '-*r'); grid on;

% {
% Code ancestral adapt� aux images en .png
try
    if isempty(lon) || all(isnan(lon)) % Sondeur Reson 7KCenter (pas PDS)
        LonAxe = NaN(size(LayerRaw.x));
        LatAxe = NaN(size(LayerRaw.x));
        LonCentre = NaN;
        LatCentre = NaN;
    else
        pLon = polyfit(XDataBottomImage, lon, 1);
        pLat = polyfit(XDataBottomImage, lat, 1);
        LonAxe = polyval(pLon, LayerRaw.x);
        LatAxe = polyval(pLat, LayerRaw.x);
        LonCentre = polyval(pLon, 0);
        LatCentre = polyval(pLat, 0);
    end
catch
    LonAxe = NaN(size(LayerRaw.x));
    LatAxe = NaN(size(LayerRaw.x));
    LonCentre = NaN;
    LatCentre = NaN;
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(LonAxe, LatAxe, '-*k'); grid on;
% }

%{
try
    LonAxe = my_interp1(          XDataBottomImage, lon, LayerRaw.x, 'linear', 'extrap');
    LatAxe = my_interp1_longitude(XDataBottomImage, lat, LayerRaw.x, 'linear', 'extrap');
    LonCentre = Longitude;
    LatCentre = Latitude;
catch
    LonAxe = NaN(size(LayerRaw.x));
    LatAxe = NaN(size(LayerRaw.x));
    LonCentre = NaN;
    LatCentre = NaN;
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(LonAxe, LatAxe, '-*k'); grid on;
%}

%% 

Coord.lonBabDepointe = LonAxe(1);
Coord.latBabDepointe = LatAxe(1);
Coord.lonTriDepointe = LonAxe(end);
Coord.latTriDepointe = LatAxe(end);

Coord.lonBab = Coord.lonBabDepointe;
Coord.latBab = Coord.latBabDepointe;
Coord.lonTri = Coord.lonTriDepointe;
Coord.latTri = Coord.latTriDepointe;

%%

Coord.lonBab    = LonAxe(1);
Coord.latBab    = LatAxe(1);
Coord.lonCentre = LonCentre;
Coord.latCentre = LatCentre;
Coord.lonTri    = LonAxe(end);
Coord.latTri    = LatAxe(end);
Coord.xBab      = LayerRaw.x(1);
Coord.xTri      = LayerRaw.x(end);
Coord.Depth     = LayerRaw.y(1);
Coord.iPing     = kGroup;
Coord.Date      = TimePing.date;
Coord.Heure     = TimePing.heure;
if isempty(LayerRaw.Sonar.Tide)
    Coord.Tide  = 0;
else
    Coord.Tide  = LayerRaw.Sonar.Tide(1);
end

%% Write group in netcdf file

TPing  = datetime(TimePing.timeMat, 'ConvertFrom', 'datenum');
LayerRawImage  = LayerRaw.Image(:,:);
LayerCompImage = LayerComp.Image(:,:); % TODO image compens�e

DataPingMat.kGroup           = kGroup;
DataPingMat.TPing            = TPing;
DataPingMat.Latitude         = Latitude;
DataPingMat.Longitude        = Longitude;
DataPingMat.TransducerDepth  = TransducerDepth;
DataPingMat.Coord            = Coord;
DataPingMat.Mask             = Mask;
DataPingMat.sliceNames       = sliceNames;
DataPingMat.LayerRawImage    = LayerRawImage;
DataPingMat.LayerCompImage   = LayerCompImage;

% TPing.Format = 'yyyyMMdd_hhmmssSSS';
% nomFicTemp = fullfile(my_tempdir, [char(TPing) '_' num2str(randi(99999, 1), '%05d') '.mat']);

% NetcdfGlobe3DUtils.WC_writeImageNetcdfPing(ncID, DataPingMat);

if ~isempty(Detection)
    DataPingMat.SoundingsRangeInMeters = Detection.RangeInMeters;
    DataPingMat.SoundingsAcrossDist    = Detection.X;
    DataPingMat.SoundingsDepth         = Detection.Z;
end

flag = 1;
