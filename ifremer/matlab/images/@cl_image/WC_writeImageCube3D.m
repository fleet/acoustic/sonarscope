function [flag, Coord] = WC_writeImageCube3D(this, nomFicXml, kWc, iPing, Latitude, Longitude, Heading, TimePing, Mask)

flag  = 0;
Coord = [];

Heading = double(Heading);

[nomDir1, nomFic] = fileparts(nomFicXml);
nomDir2 = fullfile(nomDir1, nomFic);
if ~exist(nomDir2, 'dir')
    flag = mkdir(nomDir2);
    if ~flag
        messageErreurFichier(nomDir2, 'WriteFailure');
        return
    end
end

% TODO : a commenter en m�me temps que modif fonction "sonar_ResonProcessWC" Code TODO20210219
% Mais �a provoqie un bug dans Globe lors de la lecture
%{

%% Ecriture de la r�flectivit�

nomFicRAW = fullfile(nomDir2, 'Reflectivity.bin');
I = this.Image(:,:);
if kWc == 1
    fid = fopen(nomFicRAW, 'w+');
else
    fid = fopen(nomFicRAW, 'a+');
end
% J = (I - (-CLim(1))) * (255 ./ CLim(2));
% J = uint8(J);
% count = fwrite(fid, J', 'uint8');
count = fwrite(fid, I', 'single');
if count ~= numel(I)
    flag = 0;
    return
end
fclose(fid);

%% Ecriture du masque

nomFicMask = fullfile(nomDir2, 'Mask.bin');
if kWc == 1
    fid = fopen(nomFicMask, 'w+');
else
    fid = fopen(nomFicMask, 'a+');
end
count = fwrite(fid, uint8(Mask(:,:)'), 'uint8');
if count ~= numel(I)
    flag = 0;
    return
end
fclose(fid);
%}

%% Calcul des coordonn�es

across = double(this.Sonar.SampleBeamData.AcrossDist);
along  = double(this.Sonar.SampleBeamData.AlongDist);
% depth  = double(this.Sonar.SampleBeamData.Depth);
% figure; PlotUtils.createSScPlot(across, along, '-*'); grid on

% [x, y] = latlon2xy(this.Carto, Latitude, Longitude);
% c = cosd(-Heading);
% s = sind(-Heading);

DeltaY = mean(along, 'omitnan');
if isnan(DeltaY)
    DeltaY   = 0;
end

% C = [this.x(1) DeltaY; this.x(end) DeltaY] * [c s; -s c];
% X = [x+C(1,1) x x+C(2,1)];
% Y = [y+C(1,2) y y+C(2,2)];
% [lat, lon] = xy2latlon(this.Carto, X', Y');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(lon, lat, '-*k'); grid on;

x0 = this.Sonar.Ship.Arrays.Transmit.X; 
if isempty(x0)
    x0 = 0;
end
XData = [this.x(1) x0      this.x(end)];
YData = [DeltaY    DeltaY  DeltaY];
E2 = get(this.Carto, 'Ellipsoide.Excentricite') ^ 2;
A  = get(this.Carto, 'Ellipsoide.DemiGrandAxe');
[lat, lon] = calculLatLonFromAlongAcross(E2, A, Latitude, Longitude, Heading, YData, XData);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(lon, lat, '-*k'); grid on;

if isnan(lon(1))
    return
end

%% Param�tres de sortie

Coord.lonBab    = lon(1);
Coord.latBab    = lat(1);
Coord.lonCentre = lon(2);
Coord.latCentre = lat(2);
Coord.lonTri    = lon(3);
Coord.latTri    = lat(3);
Coord.xBab      = this.x(1);
Coord.xTri      = this.x(end);
Coord.Depth     = this.y(1);

if isfield(this.Sonar.SampleBeamData, 'TransducerDepth')
    Coord.Immersion = -abs(this.Sonar.SampleBeamData.TransducerDepth);
else
    if isfield(this.Sonar.Ship, 'WaterLineZ')
        Coord.Immersion = -this.Sonar.Ship.WaterLineZ; % Cas des sondeurs Kongsberg
    else
        Coord.Immersion = 0; % Cas des sondeurs Reson : TODO : r�soudre ce pb en amont ! Urgent
    end
end

Coord.iPing = iPing;
Coord.Date  = TimePing.date;
Coord.Heure = TimePing.heure;
Coord.Tide  = this.Sonar.Tide;

% Simplification en attendant de voir si on peut repr�senter l'image sous
% forme de voile gonfl�e

sub = ~isnan(across) & ~isnan(along);
XData = across(sub);
YData = along(sub);

% E2 = get(this.Carto, 'Ellipsoide.Excentricite') ^ 2;
% A  = get(this.Carto, 'Ellipsoide.DemiGrandAxe');
% [LatFootprints, LonFootprints] = calculLatLonFromAlongAcross(E2, A, Latitude, Longitude, Heading, YData, XData);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(LonFootprints, LatFootprints, '-*b'); grid on;

if isempty(XData)
    Coord.lonBabDepointe = NaN;
    Coord.latBabDepointe = NaN;
    Coord.lonTriDepointe = NaN;
    Coord.latTriDepointe = NaN;
    
    % Ajout JMA le 19/01/2021 pour tester robot sur fichiers Mag1Phase
    %{
    Coord.lonBab    = NaN;
    Coord.latBab    = NaN;
    Coord.lonCentre = NaN;
    Coord.latCentre = NaN;
    Coord.lonTri    = NaN;
    Coord.latTri    = NaN;
    Coord.xBab      = NaN;
    Coord.xTri      = NaN;
    Coord.Depth     = NaN;
    Coord.iPing     = NaN;
    %}
    return
end

try
    p = polyfit(double(XData), double(YData), 1);
    YDataFitted = polyval(p, XData);
    % figure; PlotUtils.createSScPlot(XData, YData); hold on; grid on; hold on; PlotUtils.createSScPlot(XData, YDataFitted, 'r');
    Coord.alongBab = YDataFitted(1);
    Coord.alongTri = YDataFitted(end);
catch %#ok<CTCH>
    Coord.alongBab = YData(1);
    Coord.alongTri = YData(end);
end

% Coord.lonBabDepointe = LonFootprints(1); % Modif JMA le 31/10/2018
% Coord.latBabDepointe = LatFootprints(1);
% Coord.lonTriDepointe = LonFootprints(end);
% Coord.latTriDepointe = LatFootprints(end);

Coord.lonBabDepointe = Coord.lonBab; % Modif JMA le 31/10/2018
Coord.latBabDepointe = Coord.latBab;
Coord.lonTriDepointe = Coord.lonTri;
Coord.latTriDepointe = Coord.latTri;

Coord.lonBab = Coord.lonBabDepointe;
Coord.latBab = Coord.latBabDepointe;
Coord.lonTri = Coord.lonTriDepointe;
Coord.latTri = Coord.latTriDepointe;

%% Ecriture de la r�flectivit�

% TODO : a r�introduire en m�me temps que modif fonction "sonar_ResonProcessWC" Code TODO20210219
% Mais �a provoqie un bug dans Globe lors de la lecture
% {
nomFicRAW = fullfile(nomDir2, 'Reflectivity.bin');
I = this.Image(:,:);
if kWc == 1
    fid = fopen(nomFicRAW, 'w+');
else
    fid = fopen(nomFicRAW, 'a+');
end
% J = (I - (-CLim(1))) * (255 ./ CLim(2));
% J = uint8(J);
% count = fwrite(fid, J', 'uint8');
count = fwrite(fid, I', 'single');
if count ~= numel(I)
    return
end
fclose(fid);

%% Ecriture du masque

nomFicMask = fullfile(nomDir2, 'Mask.bin');
if kWc == 1
    fid = fopen(nomFicMask, 'w+');
else
    fid = fopen(nomFicMask, 'a+');
end
count = fwrite(fid, uint8(Mask(:,:)'), 'uint8');
if count ~= numel(I)
    return
end
fclose(fid);
% }

flag = 1;
