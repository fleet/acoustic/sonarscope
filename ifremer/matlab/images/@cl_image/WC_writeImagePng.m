function [flag, Coord] = WC_writeImagePng(this, CLim, iPing, Latitude, Longitude, Heading, nomDir2, TimePing, varargin)

[varargin, subx]         = getPropertyValue(varargin, 'subx',         []);
[varargin, suby]         = getPropertyValue(varargin, 'suby',         []);
[varargin, Crop]         = getPropertyValue(varargin, 'Crop',         1);
[varargin, identSondeur] = getPropertyValue(varargin, 'identSondeur', []); %#ok<ASGLU>

Coord = [];

Heading = double(Heading);

%% R�duction de l'image o� il y a de la donn�e

if ~isempty(subx) && ~isempty(suby)
    this = extraction(this, 'suby', suby, 'subx', subx, 'NoStats');
elseif Crop
    I = this.Image;
    subNonNaN = ~isnan(I);
    subx = find(sum(subNonNaN, 1) ~= 0);
    suby = find(sum(subNonNaN, 2) ~= 0);
    [this, flag] = extraction(this, 'suby', suby, 'subx', subx, 'NoStats');
    if ~flag
        return
    end
end

% Along distances r�elles
% TODO : ATTENTION : extraction ne remet pas � jour SampleBeamData.AcrossDist(subx)
% et SampleBeamData.AlongDist, attention aux effets de bord !!!!!!!!!!!
% il faut donc extraire avec les subx avant. Il est urgent de corriger cela TODO
across = double(this.Sonar.SampleBeamData.AcrossDist);
along  = double(this.Sonar.SampleBeamData.AlongDist);
depth  = double(this.Sonar.SampleBeamData.Depth);
% figure; PlotUtils.createSScPlot(across, along, '-*'); grid on

%% Nom du r�pertoire

if ~exist(nomDir2, 'dir')
    flag = mkdir(nomDir2);
    if ~flag
        messageErreurFichier(nomDir2, 'WriteFailure');
        return
    end
end

nomSousRepertoire = sprintf('%03d', floor(iPing/100));
nomDir3 = fullfile(nomDir2, nomSousRepertoire);
if ~exist(nomDir3, 'dir')
    flag = mkdir(nomDir3);
    if ~flag
        messageErreurFichier(nomDir3, 'WriteFailure');
        return
    end
end

%% Export du .png

nomFic1 = sprintf('%05d.png', iPing);
nomFic2 = fullfile(nomDir3, nomFic1);
[Indexed, flag] = Intensity2Indexed(this, 'CLim', CLim, 'NoStats');
if ~flag
    return
end
flag = export_gif(Indexed, nomFic2);
if ~flag
    return
end

%% Export du .tif 32 bits

nomFic1 = sprintf('%05d.tif', iPing);
nomFic2 = fullfile(nomDir3, nomFic1);
flag = export_geoTiff32Bits(this, nomFic2, 'Check', 0, 'Mute', 1);
if ~flag
    return
end

%% Calcul des coordonn�es

if all(isnan(Latitude)) && all(isnan(Longitude)) % Pour �viter bug en action R&D sans nav
    Latitude  = 0;
    Longitude = 0;
end

sub = ~isnan(across) & ~isnan(along);
XData = across(sub);
YData = along(sub);
depth = depth(sub);

%%

if isempty(identSondeur)
    identSondeur = this.Sonar.SampleBeamData.SystemSerialNumber;
end

x0 = this.Sonar.Ship.Arrays.Transmit.X;
y0 = this.Sonar.Ship.Arrays.Transmit.Y;
if isempty(x0)
    switch identSondeur
        case {13003; 13016}
            x0 = 0;
            y0 = 0;
        otherwise
            str1 = 'Les param�tres d''installation ne sont pas d�finis';
            str2 = 'The "Installation parameters" are not defined';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
    end
end
z0 = 0; % this.Sonar.SampleBeamData.TransducerDepth ou this.Sonar.Ship.Arrays.Transmit.Z ???;  % TODO : position en l'antenne
ZBottom = this.y(1);
XDataBottomImage = x0 + (XData - x0) .* ((ZBottom - z0) ./ (depth - z0));
YDataBottomImage = y0 + (YData - y0) .* ((ZBottom - z0) ./ (depth - z0));

if isempty(XData)
    Coord.lonBabDepointe = NaN;
    Coord.latBabDepointe = NaN;
    Coord.lonTriDepointe = NaN;
    Coord.latTriDepointe = NaN;
    flag = 0;
    return
end

%%

E2 = get(this.Carto, 'Ellipsoide.Excentricite') ^ 2;
A  = get(this.Carto, 'Ellipsoide.DemiGrandAxe');
[lat,lon] = calculLatLonFromAlongAcross(E2, A, Latitude, Longitude, Heading, YDataBottomImage, XDataBottomImage);
if isnan(lon(1))
    flag = 0;
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(lon, lat, '-*r'); grid on;

try
    pLon = polyfit(XDataBottomImage, lon, 1);
    pLat = polyfit(XDataBottomImage, lat, 1);
    LonAxe = polyval(pLon, this.x);
    LatAxe = polyval(pLat, this.x);
    LonCentre = polyval(pLon, 0);
    LatCentre = polyval(pLat, 0);
catch
    LonAxe = NaN(size(this.x));
    LatAxe = NaN(size(this.x));
    LonCentre = NaN;
    LatCentre = NaN;
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(LonAxe, LatAxe, '-*k'); grid on;

%% 

Coord.lonBabDepointe = LonAxe(1);
Coord.latBabDepointe = LatAxe(1);
Coord.lonTriDepointe = LonAxe(end);
Coord.latTriDepointe = LatAxe(end);

Coord.lonBab = Coord.lonBabDepointe;
Coord.latBab = Coord.latBabDepointe;
Coord.lonTri = Coord.lonTriDepointe;
Coord.latTri = Coord.latTriDepointe;

%%

Coord.lonBab    = LonAxe(1);
Coord.latBab    = LatAxe(1);
Coord.lonCentre = LonCentre;
Coord.latCentre = LatCentre;
Coord.lonTri    = LonAxe(end);
Coord.latTri    = LatAxe(end);
Coord.xBab      = this.x(1);
Coord.xTri      = this.x(end);

% Modif JMA le 10/10/2020 mission MAYOBS15 pour export d'extraction d'images
% Coord.Depth     = this.y(1);
Coord.Depth     = this.y(1) - this.y(end);

Coord.iPing     = iPing;
Coord.Date      = TimePing.date;
Coord.Heure     = TimePing.heure;
if isempty(this.Sonar.Tide)
    Coord.Tide  = 0;
else
    Coord.Tide  = this.Sonar.Tide(1);
end

%%

flag = 1;
