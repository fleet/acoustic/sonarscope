% Interpolates NaN values. The values are interpolated if they are
% in the vicinity of one or more valid pixels
%
% Syntax
%   b = WinFillNaN(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   window : Size of the sliding window (Default : [3 3])
%   subx   : Sub-sampling in abscissa
%   suby   : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   s = mod(0:2:1800, 360);
%   I = repmat(s, length(s), 1);
%   I(100:250,100:250) = NaN;
%   N = floor(length(s)^2 / 2);
%   d = randi(length(s), [N 2]);
%   for k=1:size(d,1)
%       I(d(k,2),d(k,1)) = NaN;
%   end
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = WinFillNaN(a);
%   imagesc(b);
%
% See also fillNaN_mean fillNaN cl_image/WinFillNaNHeading Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = WinFillNaN(this, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);

that = cl_image.empty;

N = numel(this);
hw = create_waitbar(waitbarMsg('WinFillNaN'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = WinFillNaN_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
that = reshape(that, size(this));
if Mute
    my_close(hw)
else
    my_close(hw, 'MsgEnd', 'TimeDelay', 60)
end


function that = WinFillNaN_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, method]    = getPropertyValue(varargin, 'method',    'mean');
[varargin, window]    = getPropertyValue(varargin, 'window',    [3 3]);
[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []);

[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

flagInteger = isIntegerVal(this);
if flagInteger
    method = 'median';
else
    switch this.DataType
        case cl_image.indDataType('IncidenceAngle') % I0
            method = 'median';
        case cl_image.indDataType('AbsorptionCoeff') % I0
            method = 'median';
    end
end

%% Extract the mask if defined

if isempty(LayerMask)
    Masque = [];
else
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
end

%% Algorithm

nbRows = length(suby);
nbCol = length(subx);

if this.ImageType == 2 % RGB
    nbSlides = 3;
else
    nbSlides = 1;
end
ClasseImage = class(this.Image(1));
try
    if isnan(this.ValNaN)
        Image = NaN([nbRows nbCol nbSlides], ClasseImage);
    else
        Image = zeros([nbRows nbCol nbSlides], ClasseImage);
        Image(:) = this.ValNaN;
    end
catch ME
    ME.getReport
    %     my_warndlg(ME.message, 0);
    Image = cl_memmapfile('Value', this.ValNaN, 'Size', [nbRows nbCol nbSlides], 'Format', ClasseImage);
end

for kSlide=1:nbSlides
    I = this.Image(suby, subx, kSlide);
    
    if ~isempty(Masque)
        K = zeros(size(Masque), 'uint8');
        for ik=1:length(valMask)
            sub = (Masque == valMask(ik));
            K(sub) = 1;
        end
        subNonMasque = find(K == 0);
        K = I(subNonMasque);
    end
    
    if isnan(this.ValNaN)
        %{
% ATTENTION : ne pas utiliser le blocproc car �a pose un pb sur les
bords o� la valeur 0 est copi�e et l'interpolation prend en compte
cette valeur

I = this.Image(suby,subx,kSlide);
I = singleUnlessDouble(I, this.ValNaN);
switch method
case 'mean'
fun = @(block_struct) fillNaN_mean(block_struct.data, window);
otherwise
fun = @(block_struct) fillNaN(block_struct.data, window);
end
Image = blockproc(I, [1024 1024], fun, 'BorderSize', window);
        %}
        
        I = this.Image(suby, subx, kSlide);
        I = singleUnlessDouble(I, this.ValNaN);
        
        switch method
            case 'mean'
                I = fillNaN_mean(I, window);
            otherwise
                I = fillNaN(I, window, 'method', method);
        end
        
        if ~isempty(Masque)
            I(subNonMasque) = K;
        end
        
        if flagInteger
            Image(:,:,kSlide) = floor(I);
        else
            Image(:,:,kSlide) = I;
        end
        
    else
        
        I = this.Image(:,:,kSlide);
        I = singleUnlessDouble(I, this.ValNaN);
        
        I = fillValNaN(I, window, this.ValNaN, 'method', method);
        
        if ~isempty(Masque)
            I(subNonMasque) = K;
        end
        
        if ~isnan(this.ValNaN)
            I(isnan(I)) = this.ValNaN;
        end
        
        Image(:,:,kSlide) = I;
        
        if strcmp(ClasseImage, 'single') || strcmp(ClasseImage, 'double') %#ok<STISA>
            this.ValNaN = NaN;
        else
            this.ValNaN = this.ValNaN;
        end
    end
    
    if isIntegerVal(this)
        Image(:,:,kSlide) = round(Image(:,:,kSlide));
    end
end

%% Output image

Parameters = getHistoryParameters(window);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'WinFillNaN', 'NoStats', NoStats, 'Parameters', Parameters);
