% Interpolates NaN heading values. The values are interpolated if they are
% in the vicinity of one or more valid pixels
%
% Syntax
%   b = WinFillNaNHeading(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   window : Size of the sliding window (Default : [3 3])
%   subx   : Sub-sampling in abscissa
%   suby   : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   s = mod(0:2:1800, 360);
%   I = repmat(s, length(s), 1);
%   I(100:250,100:250) = NaN;
%   N = floor(length(s)^2 / 2);
%   d = randi(length(s), [N 2]);
%   for k=1:size(d,1)
%       I(d(k,2),d(k,1)) = NaN;
%   end
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = WinFillNaNHeading(a);
%   imagesc(b);
%
% See also fillNaN_heading cl_image/WinFillNaN Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = WinFillNaNHeading(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('WinFillNaNHeading'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = WinFillNaNHeading_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = WinFillNaNHeading_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, window] = getPropertyValue(varargin, 'window', [3 3]);
[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

%% Algorithm

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    I = singleUnlessDouble(I, this.ValNaN);
    I = fillNaN_heading(I, window);
    Image(:,:,k) = I;
    if isIntegerVal(this)
        Image(:,:,k) = round(Image(:,:,k));
    end
end

%% Output image

Parameters = getHistoryParameters(window);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'WinFillNaNHeading', 'NoStats', NoStats, 'Parameters', Parameters);
