% Absolute value and complex magnitude of image
%
% Syntax
%   b = abs(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = abs(a);
%   imagesc(b);
%
% See also cl_image/process_function_type1 cl_image/sign Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = abs(this, varargin)
that = process_function_type1(this, @abs, varargin{:});
 