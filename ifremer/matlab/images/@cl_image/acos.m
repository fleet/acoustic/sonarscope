% Inverse cosine (rd)
%
% Syntax
%   b = acos(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(cos(linspace(-2*pi, 2*pi, 200)), 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a);
%
%   b = acos(a);
%   imagesc(b);
%
% See also cl_image/acosd cl_image/cos Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = acos(this, varargin)
that = process_function_type1(this, @acos, 'Unit', 'rd', 'expectedUnit', '', varargin{:});
