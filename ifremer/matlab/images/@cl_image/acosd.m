% Inverse cosine (deg)
%
% Syntax
%   b = acos(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(cos(linspace(-2*pi, 2*pi, 200)), 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = acosd(a);
%   imagesc(b);
%
% See also cl_image/acos cl_image/cosd Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = acosd(this, varargin)
that = process_function_type1(this, @acosd, 'Unit', 'deg', 'expectedUnit', '', varargin{:});
