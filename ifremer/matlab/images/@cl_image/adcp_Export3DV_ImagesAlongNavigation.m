function flag = adcp_Export3DV_ImagesAlongNavigation(this, NomFicXML, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Mute] = getFlag(varargin, 'Mute'); %#ok<ASGLU>

%% Test de la signature

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'));
if ~flag
    return
end

%% Cr�ation de l'image

I = this.Image(suby,subx);
subNaN = isnan(I);
Colormap = this.Colormap;

if this.Video == 2
    Colormap = flipud(Colormap);
end

I = gray2ind(mat2gray(I, double(this.CLim)), size(Colormap,1));
I(I == 0) = 1;
I(subNaN) = 0;


%% Cr�ation du fichier XML-Bin

Immersion   = this.Sonar.Immersion(suby);
BotDepth    = this.Sonar.Height(suby)+Immersion;

FishLatitude  = this.Sonar.FishLatitude(suby);
FishLongitude = this.Sonar.FishLongitude(suby);

if any(isnan(FishLatitude))
    %         fprintf('FishLatitude NaN\n');
    subNaN = isnan(FishLatitude);
    subNonNaN = find(~subNaN);
    subNaN = find(subNaN);
    FishLatitude(subNaN) = interp1(subNonNaN, FishLatitude(subNonNaN), subNaN, 'linear', 'extrap');
end
if any(isnan(FishLongitude))
    %         fprintf('LongitudeNadir NaN\n');
    subNaN = isnan(FishLongitude);
    subNonNaN = find(~subNaN);
    subNaN = find(subNaN);
    FishLongitude(subNaN) = interp1(subNonNaN, FishLongitude(subNonNaN), subNaN, 'linear', 'extrap');
end

T = this.Sonar.Time;
adcp.name = extract_ImageName(this);

[~, name] = fileparts(NomFicXML);
adcp.name = name;

adcp.from            = 'Unkown';
adcp.Date            = T.date;
adcp.Hour            = T.heure;
% seismic.PingNumber   = this.Sonar.PingNumber(suby);
adcp.PingNumber      = suby';
adcp.Tide            = zeros(size(adcp.PingNumber), 'single');

adcp.LatitudeNadir   = FishLatitude;
adcp.LongitudeNadir  = FishLongitude;
adcp.LatitudeTop     = FishLatitude;
adcp.LongitudeTop    = FishLongitude;
adcp.LatitudeBottom  = FishLatitude;
adcp.LongitudeBottom = FishLongitude;
adcp.DepthTop        = Immersion;
adcp.DepthBottom     = BotDepth;

flag = create_XMLBin_ImagesAlongNavigation(I, NomFicXML, Colormap, adcp);

if ~Mute
    str1 = 'L''export vers GLOBE est termin�.';
    str2 = 'The export to GLOBE is over.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'CalculEchoIntegration terminee');
end

%{
FigUtils.createSScFigure;
subplot(4,1,1); PlotUtils.createSScPlot(T, seismic.LatitudeTop(:)); grid on;
subplot(4,1,2); PlotUtils.createSScPlot(T, seismic.LongitudeTop(:)); grid on;
subplot(4,1,3); PlotUtils.createSScPlot(T, seismic.DepthTop(:)); grid on;
subplot(4,1,4); PlotUtils.createSScPlot(T, seismic.DepthBottom(:)); grid on;
find(isnan(seismic.LatitudeTop(:)))
find(isnan(seismic.LongitudeTop(:)))
find(isnan(seismic.DepthTop(:)))
find(isnan(seismic.DepthBottom(:)))
%}

