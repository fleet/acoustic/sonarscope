function flag = adcp_Export3DV_ImagesAlongNavigation_ByBlocks(this, NomFicXML, TailleMax, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Mute] = getFlag(varargin, 'Mute');

nx = length(subx);
ny = length(suby);
TailleMax = min(TailleMax, nx*ny);
nBBlocks = ceil(nx * ny / TailleMax);
n2 = floor(ny / nBBlocks);
[nomDir, nomFic] = fileparts(NomFicXML);

str1 = 'Export par blocks.';
str2 = 'Export by blocks.';
hw = create_waitbar(Lang(str1,str2), 'N', nBBlocks);
for k=1:nBBlocks
    my_waitbar(k, nBBlocks, hw)
    
    sub = (0:(n2+1)) + (k-1)*n2;
    sub(sub < 1) = [];
    sub(sub > ny) = [];
    if nBBlocks == 1
        NomFicXMLBlock = NomFicXML;
    else
        NomFicXMLBlock = fullfile(nomDir, [nomFic '_' num2str(k, '%02d') '.xml']);
    end
    flag = adcp_Export3DV_ImagesAlongNavigation(this, NomFicXMLBlock, 'subx', subx, 'suby', suby(sub), 'Mute', varargin{:});
end
my_close(hw)

if ~Mute
    str1 = 'L''Export vers GLOBE est termin�.';
    str2 = 'The export to GLOBE is over.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'CalculEchoIntegration terminee');
end
