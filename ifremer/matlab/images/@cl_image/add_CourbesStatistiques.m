function this = add_CourbesStatistiques(this, Courbes)

for k1=1:length(Courbes)
    % Before
%     if strcmp(get_DataTypeName(this), Courbes(k1).bilan{1}(1).DataTypeValue) ...
%         && strcmp(get_GeometryTypeName(this), Courbes(k1).bilan{1}(1).GeometryType)
% Now : JMA le 28/08/2015
    if strcmp(get_DataTypeName(this), Courbes(k1).bilan{1}(1).DataTypeValue) ...
        && strcmp(get_GeometryTypeName(this), Courbes(k1).bilan{1}(1).GeometryType)
        flag = alreadyACurve(Courbes(k1), this.CourbesStatistiques);
        if ~flag
            this.CourbesStatistiques = [this.CourbesStatistiques(:); Courbes(k1)];
        end
    end
end

function flag = alreadyACurve(a, b)

a = a.bilan{1};

n = length(b);
flag = true(1,n);
for k1=1:n
    bilan = b(k1).bilan{1};
    if length(bilan) ~= length(a)
        continue
    end
    for k2=1:length(a)
        if length(a(k2).x) ~= length(bilan(k2).x)
            flag(k1) = false;
            continue
        end
        if length(a(k2).y) ~= length(bilan(k2).y)
            flag(k1) = false;
            continue
        end
        
        if isfield(a(k2), 'numVarCondition') && isfield(bilan(k2), 'numVarCondition')
            if a(k2).numVarCondition ~= bilan(k2).numVarCondition
                flag(k1) = false;
                continue
            end
        else
            flag(k1) = false;
            continue
        end
        
        if isfield(a(k2), 'nomVarCondition') && isfield(bilan(k2), 'nomVarCondition')
            if ~strcmp(a(k2).nomVarCondition, bilan(k2).nomVarCondition)
                flag(k1) = false;
                continue
            end
        else
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).titreV, bilan(k2).titreV)
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).nomX, bilan(k2).nomX)
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).Unit, bilan(k2).Unit)
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).nomZoneEtude, bilan(k2).nomZoneEtude)
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).Tag, bilan(k2).Tag)
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).DataTypeValue, bilan(k2).DataTypeValue)
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).GeometryType, bilan(k2).GeometryType)
            flag(k1) = false;
            continue
        end
        
        if ~strcmp(a(k2).TypeCourbeStat, bilan(k2).TypeCourbeStat)
            flag(k1) = false;
            continue
        end
        
        if length(a(k2).DataTypeConditions) ~= length(bilan(k2).DataTypeConditions)
            flag(k1) = false;
            continue
        end
        
        for k3=1:length(a(k2).DataTypeConditions)
            if ~strcmp(a(k2).DataTypeConditions{k3}, bilan(k2).DataTypeConditions{k3})
                flag(k1) = false;
                break
            end
        end
        
        if length(a(k2).Support) ~= length(bilan(k2).Support)
            flag(k1) = false;
            continue
        end
        
        for k3=1:length(a(k2).Support)
            if ~strcmp(a(k2).Support{k3}, bilan(k2).Support{k3})
                flag(k1) = false;
                break
            end
        end
        
        if isfield(a(k2), 'Biais') && isfield(bilan(k2), 'Biais')
            if a(k2).Biais ~= bilan(k2).Biais
                flag(k1) = false;
                continue
            end
        else
            flag(k1) = false;
            continue
        end
        
        if isfield(a(k2), 'Mode') && isfield(bilan(k2), 'Mode')
            if a(k2).Mode ~= bilan(k2).Mode
                flag(k1) = false;
                continue
            end
        else
            flag(k1) = false;
            continue
        end
        
        if isfield(a(k2), 'NbSwaths') && isfield(bilan(k2), 'NbSwaths')
            if a(k2).NbSwaths ~= bilan(k2).NbSwaths
                flag(k1) = false;
                continue
            end
        else
            flag(k1) = false;
            continue
        end
    end
end

flag = any(flag);
%{
                 Biais: -23.1582
           commentaire: ''
                TagSup: []
%}
