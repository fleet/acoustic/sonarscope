% Operateur  "add_proba" ajout de probabilites
%
% Syntax
%   c = add_proba(a, b)
%
% Input Arguments
%   a : Une instance de cl_image
%   b : une instance de cl_image
%
% Output Arguments
%   c : Une instance de cl_image
%
% Examples
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = add_proba(a, b, varargin)

if isobject(a) && isobject(b)
    this   = a;
    lambda = b;
else
    my_warndlg('Parametres attendus de type cl_image', 1);
end

[varargin, Weight] = getPropertyValue(varargin, 'W', 0.5); %#ok<ASGLU>

if Weight > 1
    Weight = 1;
end


nbImages = length(this);
if nbImages ~= 1
    if length(lambda) == 1
        for i=1:nbImages
            varargout{1}(i) = and(this(i), lambda);
        end
    elseif isequal(size(lambda), size(this))
        for i=1:nbImages
            varargout{1}(i) = and(this(i), lambda(i));
        end
    else
        for i=1:nbImages
            varargout{1}(i) = and(this(i), lambda);
        end
    end
    return
end

%% Traitement des non-valeurs

this = ValNaN2NaN(this);

%% Appel de la fonction de filtrage

[XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, 1:this.nbColumns, 1:this.nbRows, lambda);
if isempty(subx) || isempty(suby)
    str = sprintf('No intersection between images');
    my_warndlg(str,1);
    varargout{1} = [];
    return
end
%     lambda.Image = lambda.Image(subyb, subxb);
lambda = replace_Image(lambda, lambda.Image(sort(subyb), sort(subxb))); % Rajout� "sort" le 08/12/2006 : pas v�rifi�
if ~isequal([this.nbRows this.nbColumns], [length(suby) length(subx)])
    %         this.Image = this.Image(suby, subx);
    this = replace_Image(this, this.Image(suby, subx));
    this = majCoordonnees(this, subx, suby);
end

lambda = ValNaN2NaN(lambda);

TmpThis   = this.Image;
TmpLambda = lambda.Image;
TmpThis(isnan(TmpThis))=0;
TmpLambda(isnan(TmpLambda))=0;
resultat = single(TmpThis*(1-Weight) + Weight.*TmpLambda);
resultat = resultat / max(resultat(:));
this = replace_Image(this, resultat);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'add_proba');

%% Par ici la sortie

varargout{1} = this;
