% Create a movie of the images
%
% Syntax
%   animation(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-only Arguments
%   subx     : Sub-sampling in abscissa
%   suby     : Sub-sampling in ordinates
%   nomFic   : Name of the .avi file
%   fps      : Framas per second
%   CLim     : Contrast values
%   quality  : Quality (default : 75)
%   Rotation : (default 1)
%   pV       : ?
%
% Examples
%   TODO
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function animation(this, varargin)

[varargin, subx]     = getPropertyValue(varargin, 'subx',         []);
[varargin, suby]     = getPropertyValue(varargin, 'suby',         []);
[varargin, nomFic]   = getPropertyValue(varargin, 'filename',     []);
[varargin, fps]      = getPropertyValue(varargin, 'fps',          15);
[varargin, CLim]     = getPropertyValue(varargin, 'CLim',         []);
[varargin, quality]  = getPropertyValue(varargin, 'quality',      75);
[varargin, Rotation] = getPropertyValue(varargin, 'Rotation',     1);
[varargin, pV]       = getPropertyValue(varargin, 'profileVideo', 1); %#ok<ASGLU>

if isempty(nomFic)
    nomFic = my_tempname('.avi');
end

[flag, nomFicMovie] = movie(this, nomFic, 'fps', fps, 'subx', subx, 'suby', suby, 'CLim', CLim, ...
    'quality', quality, 'Rotation', Rotation, 'profileVideo', pV);
if ~flag
    return
end

% if isdeployed
    winopen(nomFicMovie)
% else
%     try
%         my_implay(nomFic)
%     catch %#ok<CTCH>
%         winopen(nomFic)
%     end
% end
