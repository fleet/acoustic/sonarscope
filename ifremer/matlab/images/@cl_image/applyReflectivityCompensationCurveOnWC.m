function [flag, this] = applyReflectivityCompensationCurveOnWC(this, bilan, ModeDependant)

% TODO : g�rer le conditionnement en fonction des modes, Single/Double swath

flag = 0;

%% Check if the compensation curve is made for Frflectivity, BeamPointingAngle and TxBeamIndexSwath

for k=1:length(bilan)
    if (length(bilan{k}(1).DataTypeConditions) == 1) && strcmp(bilan{k}(1).DataTypeConditions{1}, 'X')
        break
    end
    
    if length(bilan{k}(1).DataTypeConditions) ~= 2
        str1 = 'Le nombre de layers conditionnel doit �tre de 2.';
        str2 = 'The number of conditional layers must be 2';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    % TODO : attention, ici on compare directement les strings alors
    % qu'habituellement on compare les index !!!
    if ~strcmp(bilan{k}(1).DataTypeConditions{1}, 'BeamPointingAngle')
        str1 = sprintf('Le premier layer conditionnel (d�crit dans la courbe de compensation) est "%s", ce traitement attend "BeamPointingAngle"', bilan{k}(1).DataTypeConditions{1});
        str2 = sprintf('The first conditional layer (described in the compensation curve) is "%s", this processing needs "BeamPointingAngle"', bilan{k}(1).DataTypeConditions{1});
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    if ~strcmp(bilan{k}(1).DataTypeConditions{2}, 'TxBeamIndexSwath')
        str1 = sprintf('Le deuxi�me layer conditionnel (d�crit dans la courbe de compensation) est "%s", ce traitement attend "BeamPointingAngle"', bilan{k}(1).DataTypeConditions{1});
        str2 = sprintf('The second conditional layer (described in the compensation curve) is "%s", this processing needs "TxBeamIndexSwath"', bilan{k}(1).DataTypeConditions{1});
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    %% On red�finit le type de g�om�trie
    
    bilan{k}(1).GeometryType = 'SampleBeam';
end

% [flag, BeamPointingAngle] = create_RxBeamAngle(this);
% if ~flag
%     return
% end

%% Read BeamPointingAngle and TxBeamIndexSwath layers

[flag, ConditionnalLayers] = sonar_SampleBeamRxBeamAnglesCreation(this);
if ~flag
    return
end

%% On red�finit le type d'angle (29 <--> RxBeamAngle ==> 88 <--> BeamPointingAngle)

ConditionnalLayers(1).DataType = cl_image.indDataType('BeamPointingAngle');

%% On red�finit le type d'angle (6 <--> TxBeamIndex ==> 85 <--> TxBeamIndexSwath)

ConditionnalLayers(3).DataType = cl_image.indDataType('TxBeamIndexSwath');

%% Affectation du mode et du num�ro de swath : TODO : d�finir ces infos � la lecture des layers

ConditionnalLayers(3).Sonar.PortMode_1    = 1;
ConditionnalLayers(3).Sonar.NbSwaths(:,1) = 2;
    
%% Compensation

[this, flag] = compensationCourbesStats(this, ConditionnalLayers([1 3]), bilan, 'ModeDependant', ModeDependant);
if ~flag
    return
end


