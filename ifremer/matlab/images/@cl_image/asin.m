% Inverse sine (deg)
%
% Syntax
%   b = asin(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(sin(linspace(-2*pi, 2*pi, 200)), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'rd');
%   imagesc(a);
%
%   b = sin(a);
%   imagesc(b);
%
% See also cl_image/asind cl_image/sind Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = asin(this, varargin)
that = process_function_type1(this, @asin, 'Unit', 'rd', 'expectedUnit', '', varargin{:});
