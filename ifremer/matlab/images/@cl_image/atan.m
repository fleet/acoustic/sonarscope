% Inverse tangent (rd)
%
% Syntax
%   b = atan(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(linspace(-2*pi, 2*pi, 200), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'rd', 'ColormapIndex', 3);
%   imagesc(a);
%   b = tan(a);
%   imagesc(b);
%
%   c = atan(b);
%   imagesc(c);
%
% See also cl_image/atand cl_image/tan Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = atan(this, varargin)
that = process_function_type1(this, @atan, 'Unit', 'rd', 'expectedUnit', '', varargin{:});
