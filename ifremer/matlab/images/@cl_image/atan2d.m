% Inverse tangent (rd)
%
% Syntax
%   b = atand(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(linspace(-2*pi, 2*pi, 200), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'rd', 'ColormapIndex', 3);
%   imagesc(a);
%   b1 = tan(a);
%   imagesc(b1);
%   b2 = tan(a / 2);
%   imagesc(b2);
%
%   c = atan2d(b1, b2);
%   imagesc(c);
%
% See also cl_image/atan cl_image/tand Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = atan2d(this1, this2, varargin)
that = process_function_type3(this1, this2, @atan2d, 'Unit', 'deg', 'expectedUnit', '', varargin{:});
