% Inverse tangent (rd)
%
% Syntax
%   b = atand(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(linspace(-2*pi, 2*pi, 200), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'rd', 'ColormapIndex', 3);
%   imagesc(a);
%   b = tan(a);
%   imagesc(b);
%
%   c = atand(b);
%   imagesc(c);
%
% See also cl_image/atan cl_image/tand Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = atand(this, varargin)
that = process_function_type1(this, @atand, 'Unit', 'deg', 'expectedUnit', '', varargin{:});
