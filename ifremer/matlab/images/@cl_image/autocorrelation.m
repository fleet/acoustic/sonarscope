% Autocorrelation d'une image par convolution
%
% Syntax
%   b = autocorrelation(a, ...) 
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image d'autocorrelation
%
% Remarks : Pr�f�rez autocorrelationByFFT, �a sera beaucoup plus efficace
% 
% Examples 
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   imagesc(a)
%   b = autocorrelation(a, 'subx', 1:50, 'suby', 1:50);
%   imagesc(b)
%
%   [I, label] = ImageSonar(1);
%   I = I(1:50,1:50);
%   a = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   imagesc(a)
%   b = autocorrelation(a);
%   imagesc(b)
%
% See also cl_image cl_image/autocorrelationByFFT Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = autocorrelation(this, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_autocorrelation(this(k), varargin{:});
end


function this = unitaire_autocorrelation(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx);
[C, x, y, dC, dL]  = my_xcorr2(I, I); %#ok<ASGLU>

%% Output image

this.Sonar.Height = []; % TODO : supprimer tous les signaux verticaux
this = inherit(this, C, 'x', x, 'y', y, ...
    'TagSynchroX', num2str(rand(1)), 'TagSynchroY', num2str(rand(1)), ...
    'GeometryType', cl_image.indGeometryType('OrthoYX'), 'DataType', cl_image.indDataType('Correlation'), ...
    'ColormapIndex', 3, 'ValNaN', NaN);

