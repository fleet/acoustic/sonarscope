% Autocorrelation d'une image
%
% Syntax
%   b = autocorrelationByFFT(a, ...) 
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image d'autocorrelation
%
% Examples 
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   b = autocorrelationByFFT(a);
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = autocorrelationByFFT(this, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_autocorrelationByFFT(this(k), varargin{:});
end


function this = unitaire_autocorrelationByFFT(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx);
[C, x, y, dC, dL]  = my_xcorr2_byFFT(I, I); %#ok<ASGLU>

%% Output image

this.Sonar.Height = []; % TODO : supprimer tous les signaux verticaux
this = inherit(this, C, 'x', x, 'y', y, ...
    'TagSynchroX', num2str(rand(1)), 'TagSynchroY', num2str(rand(1)), ...
    'GeometryType', cl_image.indGeometryType('OrthoYX'), 'DataType', cl_image.indDataType('Correlation'), ...
    'ColormapIndex', 3, 'ValNaN', NaN);

