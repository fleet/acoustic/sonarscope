function [flag, this] = calcul_TextureApprentissageCalcul(this, indImage, subx, suby)

[flag, binsImage, indLayerAngle, binsLayerAngle, numTexture,  nomTexture, cliques] ...
    = params_FonctionTexture(this, indImage, subx, suby);
if ~flag
    return
end

[flag, indLayerMask, valMask, Coul] = paramsMasque(this, indImage, 'InitialValue', 'Segmentation');
if isempty(indLayerMask)
    str1 = 'Aucun mayer de masque n''est associ� � cette image, utilisez "ROI / Masques" pour en d�finir.';
    str2 = 'No Mask layer associated to the image, use "ROI / Masks" to define one.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
if ~flag
    return
end

this(indLayerMask) = ROI_union(this(indLayerMask));
ROI = this(indLayerMask).RegionOfInterest;

% Comment� par JMA le 17/12/2011
% if ~isempty(ROI)
%     ROI = ROI(end);
% end

for i=1:length(valMask)
    if isempty(ROI) || ~isfield(ROI(i), 'nomCourbe') || isempty(ROI(i).nomCourbe) || isempty(ROI(i).labels) 
        strNomCourbe = sprintf('Facies : %d', i);
        strCommentaire = '';
    else
        strNomCourbe   = ROI(i).nomCourbe;
        strCommentaire = ROI(i).Commentaire;
        if isempty(strNomCourbe)
            strNomCourbe = sprintf('Facies : %d', ROI(i).labels);
        end
        if isempty(strCommentaire)
            strCommentaire = '';
        end
    end
    
    nomCourbe{i}   = strNomCourbe; %#ok
    Commentaire{i} = strCommentaire; %#ok
    
    %{
    options.Resize = 'on';
    prompt      = {Lang('Nom du facies', 'Texture name'), Lang('Commentaire', 'Comment')};
    dlgTitle    = Lang('Analyse de la texture', 'Texture analysis');
    cmd         = inputdlg(prompt, dlgTitle, [1;10], {strNomCourbe, strCommentaire}, options);
    if isempty(cmd)
        flag = 0;
        return
    end
    nomCourbe{i}   = cmd{1}; %#ok
    Commentaire{i} = cmd{2}; %#ok
    %}
end
drawnow

%%  Quantification des images

xCurrent = get(this(indImage), 'x');
xMasque  = get(this(indLayerMask),  'x');
subxMasque = interp1(xMasque, 1:length(xMasque), xCurrent(subx), 'nearest');

yCurrent = get(this(indImage), 'y');
yMasque  = get(this(indLayerMask),  'y');
subyMasque = interp1(yMasque, 1:length(yMasque), yCurrent(suby), 'nearest');

% Masque = get_Masque_ij(this(indLayerMask), subx, suby);
Masque = get_Masque_ij(this(indLayerMask), subxMasque, subyMasque);
[I, Indices] = texture_calcul_init(this(indImage), ...
    binsImage, ...
    this(indLayerAngle), binsLayerAngle, ...
    'subx', subx, 'suby', suby);

%% Calcul des signatures texturales pour chaque valeur de masque

N = length(valMask);
str1 = 'Calcul des signatures pour les diff�rents faci�s';
str2 = 'Learning signatures for the various facies';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for i=1:N
    my_waitbar(i,N,hw)
    
    %             this(indImage) = texture_calcul(this(indImage), ...
    %                 binsImage, ...
    %                 this(indLayerAngle), binsLayerAngle, ...
    %                 nomCourbe{i}, Commentaire{i}, ...
    %                 numTexture, nomTexture, ...
    %                 'LayerMask', this(indLayerMask), 'valMask', valMask(i), ...
    %                 'cliques', cliques, 'subx', subx, 'suby', suby);
    
    J = I;
    subNonMasque = (Masque ~= valMask(i));
    J(subNonMasque) = NaN;
    
    this(indImage) = texture_calcul_new(this(indImage), ...
        J, Indices, binsImage, ...
        this(indLayerAngle), binsLayerAngle, ...
        nomCourbe{i}, Commentaire{i}, ...
        numTexture, nomTexture, ...
        'valMask', valMask(i), ...
        'cliques', cliques, ...
        'Coul', Coul(i,:));
end
my_close(hw, 'MsgEnd');

plotSegmSignature(this(indImage), 'numTexture', numTexture, 'OnlyOneFig', 1);
