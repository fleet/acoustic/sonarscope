function [flag, lat0, lon0, LatitudeBottom, LongitudeBottom, DepthBottom, TxBeamIndex] ...
    = calcul_positionRawDataWC(this, varargin)

[varargin, SonarShip]  = getPropertyValue(varargin, 'SonarShip', []);
[varargin, typeAlgoWC] = getPropertyValue(varargin, 'typeAlgoWC', 1); %#ok<ASGLU>

lat0            = [];
lon0            = [];
LatitudeBottom  = [];
LongitudeBottom = [];
DepthBottom     = [];

% TxBeamIndex = this.Sonar.SampleBeamData.TransmitSectorNumber(:,:);
Latitude      = this.Sonar.SampleBeamData.Latitude(:,:);
Longitude     = this.Sonar.SampleBeamData.Longitude(:,:);
Heading       = double(this.Sonar.SampleBeamData.Heading);
R1Samples     = double(this.Sonar.SampleBeamData.R1SamplesFiltre(:,:));
% SoundVelocity = double(this.Sonar.SampleBeamData.SoundVelocity(:,:));
% SampleRate    = double(this.Sonar.SampleBeamData.SampleRate(:,:));
% TxAngle       = double(this.Sonar.SampleBeamData.TxAngle(:,:));
TxAngle       = double(this.Sonar.SampleBeamData.RxAnglesEarth_WC(:,:));
TxBeamIndex   = this.Sonar.SampleBeamData.TransmitSectorNumber(:,:);

if isfield(this.Sonar.SampleBeamData, 'AcrossDist')
    AcrossDist       = double(this.Sonar.SampleBeamData.AcrossDist(:,:));
    AlongDist        = double(this.Sonar.SampleBeamData.AlongDist(:,:));
%   RxAnglesEarth_WC = double(this.Sonar.SampleBeamData.BeamPointingAngle(:,:));
    RxAnglesEarth_WC = double(this.Sonar.SampleBeamData.RxAnglesEarth_WC(:,:));
    depth            = double(this.Sonar.SampleBeamData.Depth(:,:));
else
    % 	RxAnglesEarth_WC = this.Sonar.SampleBeamData.TxAngle(:,:);
    %     AlongDist  = zeros(size(RxAnglesEarth_WC));
    %     fe = this.Sonar.SampleBeamData.SampleRate;
    %     AcrossDist = (1500 / (2 * fe)) * this.Sonar.SampleBeamData.R1SamplesFiltre;
    str1 = 'Cette donn�e ne contient pas les informations de "across distance" et "along distance", l''export ne peut pas �tre r�alis�.';
    str2 = 'This data does not contain any "Across distance" and "along distance". The export cannot be done.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
AcrossDist       = AcrossDist(:)';
AlongDist        = AlongDist(:)';
RxAnglesEarth_WC = RxAnglesEarth_WC(:)';
depth            = depth(:)';
R1Samples        = R1Samples(:)';

R1Samples(R1Samples <= 1) = NaN;
% figure; plot(diff(diff(R1Samples)), '-+'); grid

% TODO : il faut rammener la distance de l'antenne du fichier
if isempty(SonarShip)
    Deltay = mean(AlongDist, 'omitnan');
else
    Deltay = SonarShip.Arrays.Transmit.Y;
end

% TODO : tester avec donn�es non compens�es en roulis (EM7111 ?)
% RxAnglesEarth_WC = RxAnglesEarth_WC - Roll; % Modif JMA le 15/04/2016 pour donn�es EM710 Wreck Baie de Douarnenez

AcrossDist = my_interp1_sort(RxAnglesEarth_WC, AcrossDist, TxAngle);
% figure(54345); subplot(2,1,1); plot(RxAnglesEarth_WC); grid on ; axis([1 400 -80 80]); subplot(2,1,2); plot(TxAngle); grid on; axis([1 256 -80 80])
AlongDist  = my_interp1_sort(RxAnglesEarth_WC, AlongDist,  TxAngle);

nbSamples = this.nbRows;

% figure; plot(AcrossDistEnd, AlongDistEnd, '-*'); grid

% Angles = this.Sonar.SampleBeamData.TxAngle;

% TransducerDepth = this.Sonar.SampleBeamData.TransducerDepth;
% DepthBottom = nbSamples .* ((depth - TransducerDepth) ./ R1Samples); % Autre fa�on de calculer DepthBottom
% DepthBottom = DepthBottom + TransducerDepth;




% if typeAlgoWC == 1 % On s'appuie sur les acrossDist, Depth et instant de d�tection
DepthBottom = nbSamples * (depth ./ R1Samples);
% AcrossDistEnd = nbSamples * (AcrossDist ./ R1Samples);
% else
% end

% AlongDistEnd  = nbSamples * ((AlongDist-Deltay)  ./ R1Samples) + Deltay;

% subBadSoundings = isnan(DepthBottom) | isnan(AcrossDistEnd) | isnan(AlongDistEnd);
subBadSoundings = isnan(DepthBottom) | isnan(AcrossDist) | isnan(AlongDist);
subNaN    = find( subBadSoundings);
subNonNaN = find(~subBadSoundings);
if length(subNonNaN) > 1
    DepthBottom(subNaN)   = interp1(subNonNaN, DepthBottom(subNonNaN),   subNaN);
%     AcrossDistEnd(subNaN) = interp1(subNonNaN, AcrossDistEnd(subNonNaN), subNaN);
    AcrossDist(subNaN) = interp1(subNonNaN, AcrossDist(subNonNaN), subNaN);
%     AlongDistEnd(subNaN)  = interp1(subNonNaN, AlongDistEnd(subNonNaN),  subNaN);
    AlongDist(subNaN)  = interp1(subNonNaN, AlongDist(subNonNaN),  subNaN);
end

%% Calcul des coordonn�es

[x0, y0] = latlon2xy(this.Carto, Latitude, Longitude);
c = cosd(-Heading);
s = sind(-Heading);
C = [0 Deltay] * [c s; -s c];
[lat0 ,lon0] = xy2latlon(this.Carto, x0+C(:,1), y0+C(:,2));

% C = [AcrossDistEnd(:) AlongDistEnd(:)] * [c s; -s c];
C = [AcrossDist(:) AlongDist(:)] * [c s; -s c];
[LatitudeBottom, LongitudeBottom] = xy2latlon(this.Carto, x0+C(:,1), y0+C(:,2));
% figure; plot(C(:,1), C(:,2), '-+'); grid on;
% figure; plot(C(:,1), -DepthBottom, '-+'); grid on;
% figure; plot(LongitudeBottom, LatitudeBottom, '-+'); grid on;

% subNaN    = find( isnan(LatitudeBottom));
% subNonNaN = find(~isnan(LatitudeBottom));
% LatitudeBottom  = my_interp1(subNonNaN, LatitudeBottom(subNonNaN),  subNaN, 'linear', 'extrap');
% LongitudeBottom = my_interp1(subNonNaN, LongitudeBottom(subNonNaN), subNaN, 'linear', 'extrap');

flag = 1;
