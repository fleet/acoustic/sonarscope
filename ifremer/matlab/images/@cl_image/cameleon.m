% Transformation des valeurs d'une image pour la ramener � une moyenne et
% un �cart-type donn�
%
% Syntax
%   b = cameleon(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Mean : Final mean value (D�faut : 0)
%   Std  : Final standard deviation value (D�faut : 1)
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   I = imread(nomFic);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = cameleon(a);
%   imagesc(b);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = cameleon(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('cameleon'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = cameleon_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = cameleon_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, m2] = getPropertyValue(varargin, 'Mean', 0);
[varargin, s2] = getPropertyValue(varargin, 'Std', 1); %#ok<ASGLU>

%% Algorithm

m1 = this.StatValues.Moyenne;
s1 = this.StatValues.Sigma;
for k=this.nbSlides:-1:1
    I = this.Image(suby, subx, k);
    I = singleUnlessDouble(I, this.ValNaN);
    Image(:,:,k) = (I-m1) * (s2/s1) + m2;
end

%% Output image

that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'cameleon', 'ValNaN', NaN);
