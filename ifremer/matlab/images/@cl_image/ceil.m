% Nearest integers towards +Inf of image
%
% Syntax
%   b = ceil(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = repmat(-pi:0.1:pi, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = ceil(a);
%   imagesc(b);
%
% See also cl_image/floor cl_image/round Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = ceil(this, varargin)
that = process_function_type1(this, @ceil, varargin{:});
