% Calcul de la celerite par le modele de Chen & Millero
%
% Syntax
%   b = celeriteChen(T, S)
%
% Input Arguments
%   T : Instance de cl_image contenant la Temperature (deg)
%   S : Instance de cl_image contenant la Salinite (PPM)
%
% Name-Value Pair Arguments
%   subx  : subsampling in X
%   suby  : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant la c�l�rite en m/s
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = celeriteChen(T, S, varargin)

nbImages = length(T);
for i=1:nbImages
    this(i) = unitaire_celeriteChen(T(i), S(i), varargin{:});%#ok
end


function this = unitaire_celeriteChen(T, S, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', 1:length(T.x));
[varargin, suby] = getPropertyValue(varargin, 'suby', 1:length(T.y)); %#ok<ASGLU>

indDeb = strfind(T.Name, 'Z=');
indm = strfind(T.Name(indDeb:end), 'm');
Z = str2num(T.Name(indDeb+2:indDeb+indm(1)-2)); %#ok

for i=1:T.nbRows
    Celerite = celeriteChen(abs(Z), T.Image(i,:), S.Image(i,:));
    if ~isreal(Celerite)
        disp('DEMANDER A XAVIER CE QU''il faut faire ici')
        Celerite(isnan(imag(Celerite))) = NaN;
        Celerite = real(Celerite);
    end
    Image(i,:) = Celerite; %#ok
end
this = replace_Image(T, Image);

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this.Unit = 'm/s';
this.DataType = cl_image.indDataType('SoundVelocity');
this = update_Name(this);
