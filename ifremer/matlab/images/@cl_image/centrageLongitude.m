function this = centrageLongitude(this, CentralLongitude, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = centrageLongitude_unitaire(this(i), CentralLongitude, varargin{:});
    if ~flag
        return
    end
    this(i) = that;
end

function [flag, this] = centrageLongitude_unitaire(this, CentralLongitude, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Recentrage de l'image

XLimEarth = [CentralLongitude-180 CentralLongitude+180];
sub = (this.x(subx) < XLimEarth(1));
this.x(subx(sub)) = this.x(subx(sub)) + 360;
sub = (this.x(subx) > XLimEarth(2));
this.x(subx(sub)) = this.x(subx(sub)) - 360;
[x, ordre] = sort(this.x(subx));
Image = this.Image(suby, subx(ordre));
this = replace_Image(this, Image);
this.x = x;


%{
XLimEarth = [CentralLongitude-180 CentralLongitude+180];
x = this.x(subx);
deltax = median(diff(x));
sub = (x < XLimEarth(1));
x(sub) = x(sub) + 360;
sub = (x > XLimEarth(2));
x(sub) = x(sub) - 360;
k2 = 1 + floor(x / deltax);
sub2 = min(k2):max(k2);
x2 = (sub2-1) * deltax;
Image = NaN(length(suby), length(sub2), class(this.Image(1)));
% sub = 1 + floor((x2-x2(1))/deltax);
sub = k2;
Image(:,sub) = this.Image(suby, subx(1:length(sub)));
this = replace_Image(this, Image);
this.x = x2;
x = x2;
%}

%{
deltax = median(diff(this.x));
% k1 = 1 + floor(this.x(subx) / deltax);
k2 = 1 + floor((this.x(subx)-(CentralLongitude-180)) / deltax);
sub = find(k2 < 1);
k2(sub) = k2(sub) + floor(360/deltax);
sub = find(k2 > floor(360/deltax));
k2(sub) = k2(sub) - floor(360/deltax);
x2 = min(k2):max(k2);
x = (x2 * deltax) + (CentralLongitude-180);
Image = NaN(length(suby), length(x2), class(this.Image(1)));
Image(:,k2) = this.Image(suby, subx);
this = replace_Image(this, Image);
this.x = x;
%}

%{
XLimEarth = [CentralLongitude-180 CentralLongitude+180];
sub = (this.x(subx) < XLimEarth(1));
this.x(subx(sub)) = this.x(subx(sub)) + 360;
sub = (this.x(subx) > XLimEarth(2));
this.x(subx(sub)) = this.x(subx(sub)) - 360;
[x, ordre] = sort(this.x(subx));
Image = this.Image(suby, subx(ordre));
this = replace_Image(this, Image);
this.x = x;
%}

%{
XLimEarth = [CentralLongitude-180 CentralLongitude+180];
sub = find(this.x(subx) < XLimEarth(1));
this.x(subx(sub)) = this.x(subx(sub)) + 360;
sub = find(this.x(subx) > XLimEarth(2));
this.x(subx(sub)) = this.x(subx(sub)) - 360;
[x, ordre] = sort(this.x(subx));
deltax = median(diff(x));
x = x(1):deltax:x(end);
Image = NaN(length(suby), length(x), class(this.Image(1)));

subx2 = 1 + floor((this.x(subx) - XLimEarth(1)) / deltax);
Image(:,subx2) = this.Image(suby, subx(ordre));
this = replace_Image(this, Image);
this.x = x;
%}



%% Mise � jour de coordonn�es

this = majCoordonnees(this, 1:length(x), suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'centrageLongitude');
