% Operation de centrage (moyenne=0) et reduction (sigma = 1) d'une image
%
% Syntax
%   b = centrereduit(a)
% 
% Input Arguments
%   a : instances de cl_image
%
% Output Arguments
%   b : instances de cl_image
%
% Examples 
%   for i=1:8
%       [I, label] = ImageSonar(i);
%       a(i) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   end
%   b = centrereduit(a)
%
% See also cl_image cl_image/mean cl_image/max Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = centrereduit(this)

for i=1:length(this)
    this(i) = (this(i) - (this(i).StatValues.Moyenne)) / this(i).StatValues.Sigma;
end
