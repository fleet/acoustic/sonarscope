% Returns a string that describes the instances
%
% Syntax 
%   str = char(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   InfoSonar  : 1 : Information of the image only
%                2 : Information of the sonar data only
%                3 : Information of both Image And Sonar data : (3 par defaut)
%
% Output Arguments
%   str : Summary of the instance(s)
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%   str = char(a)
%
% See also cl_image/display cl_image/char_instance Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char(this, varargin)

N = numel(this);
str = cell(2*N,1);
for k=1:N
    if N > 1
        str{2*k-1} = sprintf('-----Element (%d/%d) -------', k, N);
    end
    str1 = char_instance(this(k), varargin{:});
    if N > 1
      str1 = [repmat(' ', size(str1,1), 2) str1]; %#ok<AGROW>
    end
    str{2*k} = str1;
end
str = cell2str(str);
