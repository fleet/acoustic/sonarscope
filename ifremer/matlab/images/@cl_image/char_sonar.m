% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char_sonar(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_image
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   a = cl_image('Image', Lena);
%   str = char_sonar(a)
%
% See also cl_image Authors
% Authors : JMA
% VERSION  : $Id: char_sonar.m,v 1.1 2005/09/14 08:23:13 augustin Exp augustin $
% ----------------------------------------------------------------------------

function str = char_sonar(this, varargin)


str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok
            str1 = char_sonar(this(i, j), varargin{:});
            str{end+1} = str1; %#ok
        end
    end
    str = cell2str(str);

else
    if (m*n) == 0
        str = '';
        return
    end

    % ---------------
    % Instance unique

    str = char(this.Sonar.Desciption, varargin{:});
    str = [repmat(' ', size(str,1), 2) str];
end