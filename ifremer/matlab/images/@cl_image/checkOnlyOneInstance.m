% Check if the length of the instance is equal to 1 and displays a message if not
%
% Syntax
%   flag = checkOnlyOneInstance(a, functionName)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%   flag = checkOnlyOneInstance(a, 'NameOfTheFunctionWhichAcceptsOnlyOneInstance')
%   flag = checkOnlyOneInstance([a a], 'NameOfTheFunctionWhichAcceptsOnlyOneInstance')
%
% See also Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

 function flag = checkOnlyOneInstance(this, functionName)

flag = 1;
if numel(this) > 1
    str1 = sprintf('La fonction "%s" ne peut traiter qu''une seule instance.', functionName);
    str2 = sprintf('The function  "%s" can process only one instance at a time.', functionName);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
end
