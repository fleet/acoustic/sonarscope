% Check if the Unit of the image is the same as the expected one
%
% Syntax
%   flag = checkUnit(a, expectedUnit)
%
% Input Arguments
%   a            : One cl_image instance
%   expectedUnit : The expected unit
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Remarks : A message is displayed in case the Units do not correspond
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%  flag = checkUnit(a, 'm')
%  flag = checkUnit(a, 'Kg')
%
% See also cl_image_setUnit Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = checkUnit(this, expectedUnit)

if isempty(expectedUnit)
    if isempty(this.Unit)
        flag = 1;
    else
        str1 = sprintf('Unit doit �tre vide. Sa valeur vaut "%s" ici.', this.Unit);
        str2 = sprintf('Unit must be empty. Its value is "%s" here.', this.Unit);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
    end
else
    if strcmpi(this.Unit, expectedUnit)
        flag = 1;
    else
        if isempty(this.Unit)
            str1 = sprintf('Unis est vide, la valeur attendue est "%s".', expectedUnit);
            str2 = sprintf('Unit is empty here. The expected value is "%s".', expectedUnit);
            my_warndlg(Lang(str1,str2), 1);
        else
            str1 = sprintf('L''unit� doit �tre "%s". Sa valeur vaut "%s" ici.', expectedUnit, this.Unit);
            str2 = sprintf('The Unit must be "%s". Its value is "%s" here.', expectedUnit, this.Unit);
            my_warndlg(Lang(str1,str2), 1);
        end
        flag = 0;
    end
end
