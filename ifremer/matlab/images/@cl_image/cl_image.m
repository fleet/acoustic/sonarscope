classdef cl_image
    %
    % Description
    %   SonarScope Image class
    %
    % Syntax
    %   a = cl_image(...)
    %
    % cl_image properties :
    %   Image		   - Matrix containing the image values
    %   Name		   - Name of the image
    %   ... see Property Summary (doc cl_image)
    %
    % Output Arguments
    %   a : One cl_image instance
    %
    % Examples
    %   a = cl_image('Image', Lena)
    %   imagesc(a)
    %
    % Author : JMA
    % See also Authors    
    
    %% D�finition de la structure et initialisation
    
    properties(Constant)

        % List of allowed data type names
        % Examples
        %   cl_image.strDataType
        % See also DataType indDataType edit_DataType
        strDataType = {'Unknown'
            'Bathymetry'
            'Reflectivity'
            'TxAngle'
            'IncidenceAngle'
            'TxBeamIndex'
            'RxBeamIndex'
            'TVG'
            'AveragedPtsNb'
            'Mask'
            'Texture'
            'SlopeAlong'
            'SlopeAcross'
            'SlopeMax'
            'SlopeAzimuth'
            'SunBW'
            'SunColor'
            'ResolAlong'
            'ResolImageAcross'
            'ResolBathyAcross'
            'Correlation'
            'AlongDist'
            'AcrossDist'
            'FootprintArea'
            'InsonifiedAreadB'
            'TxBeamPattern'
            'RxBeamPattern'
            'RayPathLength'
            'RxBeamAngle'
            'RxTime'
            'Latitude'
            'Longitude'
            'RayPathSampleNb'
            'DetectionType'
            'RayPathTravelTime'
            'Probability'
            'KongsbergQualityFactor'
            'Temperature'
            'Salinity'
            'GeoX'
            'GeoY'
            'SoundVelocity'
            'Segmentation'
            'MbesQualityFactor'
            'BeamAzimuthAngle'
            'ResonDetectionBrightness'
            'Heading'
            'ResonDetectionColinearity'
            'Log10Bathymetry'
            'KongsbergLengthDetection'
            'KongsbergAmplitudeNbSamples'
            'KongsbergPhaseOrdrePoly'
            'KongsbergPhaseVariance'
            'TxTiltAngle'
            'Roll'
            'Pitch'
            'Heave'
            'Interlacing'
            'SonarAlongBeamAngle';
            'SonarAcrossBeamAngle';
            'SonarAlongBeamWidth';
            'SonarAcrossBeamWidth'
            'ResonDetectionNadirFilter'
            'KongsbergIBA'
            'KongsbergDetectionInfo'
            'KongsbergRealTimeCleaningInfo'
            'KongsbergTwoWayTravelTime'
            'EmplacementLibre'
            'KongsbergFocusRange'
            'PulseLength'
            'KongsbergSectorTransmitDelay'
            'Frequency'
            'AbsorptionCoeff'
            'SignalBandwith'
            'KongsbergWaveformIdentifier'
            'Histo2D'
            'MbesPhaseEqm'
            'MbesPhaseSlope'
            'MbesDetectionNbSamples'
            'MbesXLJMA'
            'Kullback'
            'Weight'
            'TwoWayTravelTimeInSeconds'
            'TwoWayTravelTimeInSamples'
            'TxBeamIndexSwath'
            'Gravity'
            'Magnetism'
            'BeamPointingAngle'
            'KongsbergSnippetSortDirection'
            'KongsbergSnippetNbSamples'
            'KongsbergSnippetCentralSample'
            'KongsbergSnippetRxBeamIndex'
            'SampleFrequency'
            'Speed'
            'PresenceFM'
            'IfremerQF'
            'RxAngleEarth'
            'Mode'
            'TxAngleKjell'
            'PortStarboard'
            'UncertaintyPerCent'
            'UncertaintyMeters'
            'KongsbergSnippetDetectionInfo'
            'WCSignalWidth'};
        
        % List of allowed geometry type names
        % Examples
        %   cl_image.strGeometryType
        % See also GeometryType indGeometryType edit_GeometryType
        strGeometryType = {'Unknown'; 'GeoYX'; 'LatLong'; 'PingSamples'; 'PingAcrossDist'; 'PingTravelTime'
            'PingBeam'; 'PingRange'; 'DepthAcrossDist'; 'SampleStave'; 'SampleBeam'
            'TravelTimeStave'; 'TravelTimeBeam'; 'PingSnippets'; 'OrthoYX'; 'PingDepth'; 'PingAcrossSample'};
        
        % List of allowed colormap names
        % Examples
        %   cl_image.strColormapIndex
        % See also ColormapIndex indColormapIndex edit_ColormapIndex Colormap
        strColormapIndex = {'User'; 'gray'; 'jet'; 'cool'; 'hsv'; 'hot'; 'bone'; 'copper'; 'pink'; 'flag'
            'CNES'; 'CNESjet'; 'ColorSea'; 'ColorEarth'; 'NIWA1'; 'NIWA2'; 'JetSym'; 'Haxby'; 'Becker'; 'Catherine'; 'Seismics'; 'SeismicsSym'};

        % Indicates if the corresponding strColormapIndex name must be centered around zero
        %   SymetrieColormap(1,:) indicates if the colormap must be centered around 0
        %   SymetrieColormap(2,:) indicates if the colormap is symetrical
        % Examples
        %   cl_image.strColormapIndex
        %   cl_image.SymetrieColormap
        % See also strColormapIndex Colormap
        SymetrieColormap = [0 0 0 0 0 0 0 0 0 0 1 1 0 0 1 1 1 0 1 0 0 1
                            0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1]';
        
        % List of possible image type names
        % Examples
        %   cl_image.strImageType
        % See also ImageType indImageType edit_ImageType
        strImageType = {'Intensity'; 'RGB'; 'Indexed'; 'Boolean'};

        % List of X direction names
        % Examples
        %   cl_image.strXDir
        % See also strXDir XDir indXDir edit_XDir x
        strXDir = {'Right'; 'Left'};

        % List of Y direction names
        % Examples
        %   cl_image.strYDir
        % See also strYDir YDir indYDir edit_YDir y
        strYDir = {'Up'; 'Down'};

        % List of video representation names
        % Examples
        %   cl_image.strVideo
        % See also Video indVideo edit_Video
        strVideo = {'Normal'; 'Inverse'};

        % List of image coding status
        % Examples
        %   cl_image.strSpectralStatus
        % See also SpectralStatus indSpectralStatus
        strSpectralStatus = {'None'; 'Fourier'; 'Wavelet'};
    end
    
    
    properties(Hidden = true)
        Titre = 'Image'; % A conserver tant que tout le remplacement de Titre par Name n'est pas termin�
    end


    properties(GetAccess = 'public', SetAccess = 'public')
        
        %% D�finition de la structure
        
        % Image name
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.Name
        %   a.Name = 'Hello Prismed';
        %   a.Name
        % See also InitialImageName update_Name extract_ImageName code_ImageName extract_ImageNameTime extract_ImageNameVersion
        Name = 'Image';

        % Value to be used for missing data
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.ValNaN
        %   maskNaN = isnan(a);
        %   figure; imagesc(maskNaN); colorbar;
        %   subNaN    = findNaN(a);
        %   subNonNaN = findNonNaN(a);
        % See also isnan findNaN findNonNaN
        ValNaN = NaN;

        % Unit Unit of the image values
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.Unit
        %   a.Unit = 'meters';
        %   a.Unit
        %   [flag, Unit] = cl_image.edit_DataUnit('DataType', a.DataType, 'Title', a.Name)
        % See also Image
        Unit = '';

        % Index of the data type
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.DataType
        %   cl_image.strDataType{a.DataType}
        %   a.Name
        %   a = a.update_Name;
        %   a.Name
        %   a.DataType = 1;
        %   cl_image.strDataType{a.DataType}
        %   a.Name
        %   a = a.update_Name;
        %   a.Name
        %   [flag, DataType] = cl_image.edit_DataType('DataType', a.DataType, 'Title', a.Name)
        % See also strDataType indDataType edit_DataType
        DataType = 1;

        % Index of the geometry type (see strGeometryType)
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.GeometryType
        %   cl_image.strGeometryType{a.GeometryType}
        %   a.Name
        %   a = a.update_Name;
        %   a.Name
        %   a.GeometryType = 1;
        %   cl_image.strGeometryType{a.GeometryType}
        %   a.Name
        %   a = a.update_Name;
        %   a.Name
        %   [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', a.GeometryType, 'Title', a.Name)
        % See also strGeometryType indGeometryType edit_GeometryType
        GeometryType = 1;

        % Index of the image type
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.ImageType
        %   a.strImageType
        %   a.strImageType{a.ImageType}
        %   [flag, ImageType] = a.edit_ImageType('Title', a.Name)
        %   [flag, ImageType] = edit_ImageType(cl_image, 'ImageType', 3, 'Title', 'No image yet')
        % See also strImageType indImageType edit_ImageType
        ImageType = 1;

        % Index of the spectral status
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.SpectralStatus
        %   a.strSpectralStatus
        %   a.strSpectralStatus{a.SpectralStatus}
        % See also strSpectralStatus indSpectralStatus
        SpectralStatus = 1;
        
        % X X values (1:nbColumns if not set)
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.x
        %   a.x = a.x + 10;
        %   imagesc(sunShading(a));
        % See also XLabel XUnit XLim XRange XRangeMetric XStep XStepMetric nbColumns XDir y
        x = [];

        % Name of the X axis
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.XLabel
        %   a.YLabel
        %   a.XLabel = 'X';
        %   a.YLabel = 'Y';
        %   imagesc(sunShading(a));
        % See also x XUnit
        XLabel = 'X';

        % Unit of the X axis values
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.XUnit
        %   a.YUnit
        %   a.XUnit = 'degres';
        %   a.YUnit = 'degres';
        %   imagesc(sunShading(a));
        % See also x XLabel
        XUnit = '';

        % Index of the X direction (see strXDir)
        % Examples
        %   a = cl_image('Image', Lena, 'YDir', 2, 'ColormapIndex', 2);
        %   a.XDir
        %   a.strXDir
        %   a.strXDir{a.XDir}
        %   imagesc(a);
        %   a.XDir = 2;
        %   imagesc(a);
        %   [flag, XDir] = a.edit_XDir('Title', a.Name)
        %   [flag, XDir] = edit_XDir(cl_image, 'XDir', 2)
        % See also strXDir indXDir edit_XDir x
        XDir = 1;

        % Y Y values (1:nbRows if not set)
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.y
        %   [flag, YDir] = a.edit_YDir('Title', a.Name)
        %   [flag, YDir] = edit_YDir(cl_image, 'YDir', 2)
        %   a.y = a.y + 10;
        %   imagesc(sunShading(a));
        % See also YLabel YUnit YLim YRange YRangeMetric YStep YStepMetric nbrows YDir x
        y = [];

        % Name of the Y axis
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.XLabel
        %   a.YLabel
        %   a.XLabel = 'X';
        %   a.YLabel = 'Y';
        %   imagesc(sunShading(a));
        % See also y YUnit
        YLabel = 'Y';

        % Unit of the Y axis values
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.XUnit
        %   a.YUnit
        %   a.XUnit = 'degres';
        %   a.YUnit = 'degres';
        %   imagesc(sunShading(a));
        % See also y YLabel
        YUnit = '';

        % Index of the Y direction
        % Examples
        %   a = cl_image('Image', Lena, 'ColormapIndex', 2);
        %   a.YDir
        %   a.strYDir
        %   a.strYDir{a.YDir}
        %   imagesc(a);
        %   a.YDir = 2;
        %   imagesc(a);
        % See also strYDir indYDir edit_YDir y
        YDir = 1;
        
        % Index of the colormap
        %   a = cl_image('Image', Lena, 'YDir', 2);
        %   a.ColormapIndex
        %   a.strColormapIndex
        %   a.strColormapIndex{a.ColormapIndex}
        %   imagesc(a);
        %   a.ColormapIndex = 2;
        %   imagesc(a);
        % See also strColormapIndex indColormapIndex edit_ColormapIndex SymetrieColormap CLim Video imagesc contour Colormap
        ColormapIndex = 3;

        % Colormap values in case ColormapIndex is set to 1
        %   a = cl_image('Image', Lena, 'YDir', 2, 'ColormapIndex', 2);
        %   imagesc(a);
        %   a.ColormapIndex = 1;
        %   a.ColormapCustom = gray(8);
        %   imagesc(a);
        % See also strColormapIndex indColormapIndex edit_ColormapIndex SymetrieColormap CLim Video imagesc contour Colormap
        ColormapCustom = gray(256);% repmat(linspace(0,1,128)', 1,3);

        % Values that map to the first and last elements of the colormap (size [1 2])
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.CLim
        %   imagesc(a);
        %   a.CLim = [-3500 -1500];
        %   imagesc(a);
        %     % Or
        %   a.CLim = '0.5%';
        %   a.CLim
        %   imagesc(a);
        %   [flag, CLim] = a.edit_CLim
        % See also StatValues Video imagesc contour
        CLim = [];

        % Index of the colormap direction (see strVideo)
        % Examples
        %   a = cl_image('Image', Lena, 'YDir', 2);
        %   a.Video
        %   a.strVideo
        %   a.strVideo{a.Video}
        %   imagesc(a);
        %   a.Video = 2;
        %   imagesc(a);
        %   [flag, ColormapIndex] = a.edit_ColormapIndex
        % See also ColormapIndex CLim imagesc contour
        Video = 1;
        
        % Contour values to use in case the image is displayed as contour plot
        % Examples
        %   a = cl_image('Image', Lena, 'YDir', 2);
        %   a.ContourValues
        %   imagesc(a); 
        %   contour(a); 
        %   a.ContourValues = 0:75:255;
        %   contour(a);
        % See also contour
        ContourValues = [];
        
        % X synchronisation tag (used to link synchronised images)
        % Examples
        %   a = cl_image('Image', Lena);
        %   a.TagSynchroX
        % See also TagSynchroXScale TagSynchroY
        TagSynchroX = num2str(rand(1));

        % Y synchronisation tag (used to link synchronised images)
        % Examples
        %   a = cl_image('Image', Lena);
        %   a.TagSynchroY
        % See also TagSynchroYScale TagSynchroX  
        TagSynchroY = num2str(rand(1));

        % Special X synchronisation tag for images where x values are not in the same units
        %   a.TagSynchroXScale
        % See also TagSynchroX
        TagSynchroXScale = [];

        % Special Y synchronisation tag for images where y values are not in the same units
        %   a.TagSynchroYScale
        % See also TagSynchroY
        TagSynchroYScale = [];

        % X synchronisation tag (used to link contrast enhancement of images)
        % Examples
        %   a = cl_image('Image', Lena);
        %   a.TagSynchroContrast
        TagSynchroContrast = [];
        
        % File name from whom the data was imported
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.InitialFileName
        % See also InitialFileFormat
        InitialFileName = [];

        % File format from whom the data was imported
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.InitialFileFormat
        % See also InitialFileName
        InitialFileFormat  = 'Unknown';

        % File image name
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.Name
        %   a.InitialImageName
        %   b = sunShading(a);
        %   b.Name
        %   b.InitialImageName
        % See also Name
        InitialImageName = '';

        % Comments Comments to stick to the image
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.Comments
        %   a.Comments = 'Your own comments';
        %   a.Comments
        %     a.editProperties
        % See also editProperties
        Comments = '';

        % Center bins of HistoValues (histogram of the image values)
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(a);
        %   figure; bar(a.HistoCentralClasses, a.HistoValues);
        % See also StatValues StatSummary HistoValues
        HistoCentralClasses = [];
        
        % Should not exist, there is a maintenance action to do here
        % (should be an attribut of cl_image_sonar)
        strMaskBits  = {};
        
        % Backup of the mother image colormap in case the image was converted in RGB
        OriginColorbar = cl_image.getStructOriginColorbar;

        % Summary of statistic values in a char
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(a);
        %   a.StatSummary
        %   a.StatValues
        % See also StatValues HistoCentralClasses HistoValues
        StatSummary = '';

        % History History of the image creation
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.History
        %   a = filterGauss(a);
        %   a = sunShading(a);
        %   a.History
        %   for k=1:length(a.History), a.History(k), end
        History = cl_image.getStructHistory;
        
        % Internal parameter that should not exist
        VertExagAuto = 0; % A supprimer mais demande de la maintenance

        % Internal parameter that should not exist
        VertExag     = 1; % A supprimer mais demande de la maintenance

        % Internal parameter that should not exist
        Azimuth      = 0; % A supprimer mais demande de la maintenance

        %% D�finition de la carto
        
        % cl_carto instance
        Carto = []; % cl_carto.empty; % TODO : � tester
        
        %% R�gions d'int�r�t
        
        % Region Of Interest (see method get_RegionOfInterest)
        RegionOfInterest = [];
        
        %% Statistiques conditionelles
        
        % Statistic curves (see get_CourbesStatistiques)
        CourbesStatistiques = [];
        
        %% Matrices de cooccurrence
        
        % Cooccurence matrices
        Texture = [];
        
        %% Champs sp�ciaux pour imagerie sonar
        
        % Sonar Sonar data
        Sonar = cl_image.getStructSonar;
        
        %% Test nouveaux signaux
        
        % Y signals
        SignalsVert = ClSignal.empty(); % seul set_SignalsVert existe actuellement
        
        %% Navigation
        
        % Full navigation
        Navigation = {};

        Writable = '1'; % A conserver tant que tout le remplacement de Titre par Name n'est pas termin�
    end

    
    properties(GetAccess = 'private', SetAccess = 'private')
        % Internal parameter that should not exist
        SaveParamsIfFFT = [];
    end

    
    properties(GetAccess = 'public', SetAccess = 'private')

        % Image Image matrix
        %   a = cl_image('Image', Lena);
        %   imagesc(a);
        %     % or
        %   a = cl_image;
        %   a = set_Image(a, Lena);
        %   imagesc(a);
        %     % Ex of other parameters
        %   a.YDir = 2;
        %   a.ColormapIndex = 2;
        %   imagesc(a);
        %     % or
        %   a = cl_image('Image', Lena, 'YDir', 2, 'ColormapIndex', 2);
        %   imagesc(a);
        % See also Unit nbRows nbColumns nbSlides CLim imagesc
        Image = [];

        % Display X limits of the image
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.XLim
        % See also x XLabel XUnit XRange XRangeMetric XStep XStepMetric
        XLim = [];

        % Display Y limits of the image
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(sunShading(a));
        %   a.YLim
        % See also y YLabel YUnit YRange YRangeMetric YStep YStepMetric
        YLim = [];

        % Values of the current applyed colormap
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(a);
        %   a.Colormap
        % See also strColormapIndex ColormapIndex indColormapIndex edit_ColormapIndex
        Colormap = [];

        % Statistic values of the image
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(a);
        %   a.StatValues
        % See also StatSummary HistoCentralClasses HistoValues
        StatValues = [];

        % Histogram values if the image values
        % Examples
        %   a = cl_image.import_Prismed;
        %   imagesc(a);
        %   figure; bar(a.HistoCentralClasses, a.HistoValues);
        % See also StatValues StatSummary HistoCentralClasses
        HistoValues = [];

        % Internal ID of the image
        % Examples
        %   a = cl_image.import_Prismed;
        %   a.NuIdent
        % See also NuIdentParent
        NuIdent = []; % Numero d'identit� propre � cette image
        
        % Internal ID of the mother image
        % Examples
        %   a = cl_image.import_Prismed;
        %   IDa = a.NuIdent
        %   IDaParent = a.NuIdentParent
        %   b = sunShading(a);
        %   IDbParent = b.NuIdentParent
        %   IDb = b.NuIdent
        % See also NuIdent
        NuIdentParent = [];
    end

    
    methods(Access = 'public')
        
        function this = cl_image(varargin)
            % cl_image constructor
            %   a = cl_image;
            %   a = set_Image(a, Lena);
            %   imagesc(a);
            %   a.YDir = 2;
            %   a.ColormapIndex = 2;
            %   imagesc(a);
            %     % or
            %   a = cl_image('Image', Lena, 'YDir', 2, 'ColormapIndex', 2);
            %   imagesc(a);
            % See also Unit nbRows nbColumns nbSlides CLim imagesc
            this = set(this, varargin{:});
        end
        
        
        function nbRows = nbRows(this)
            % Number of rows of the image
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.nbRows
            % See also Image nbColumns nbSlides
            arguments
                this (1,1)
            end
            nbRows = size(this.Image,1);
        end
        
        
        function nbColumns = nbColumns(this)
            % Number of columns of the image
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.nbColumns
            % See also Image nbRows nbSlides
            arguments
                this (1,1)
            end
            nbColumns = size(this.Image,2);
        end
        
        
        function nbSlides = nbSlides(this)
            % Number of slides of the image (3 for RGB images, 1 otherwise)
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.nbSlides
            % See also Image nbRows nbColumns
            arguments
                this (1,1)
            end
            xOut1 = size(this.Image,3);
            if this.ImageType == 2 % RGB
                nbSlides = 3;
            else
                nbSlides = 1;
            end
            if nbSlides ~= xOut1
                my_breakpoint
            end
        end

        
        function XRange = XRange(this)
            % X limits of the image (left corner of first pixel to the right corner of the last one)
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.XRange
            % See also x XLabel XUnit XLim XRangeMetric XStep XStepMetric
            arguments
                this (1,1)
            end
            XRange = diff(this.XLim);
        end
        
        
        function YRange = YRange(this)
            % Y limits of the image (bottom corner of first pixel to the upper corner of the last one)
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.YRange
            % See also y YLabel YUnit YLim YRangeMetric YStep YStepMetric
            arguments
                this (1,1)
            end
            YRange = diff(this.YLim);
        end
        
        
        function XStep = XStep(this)
            % Space between X values
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.XStep
            % See also x XLabel XUnit XLim XRange XRangeMetric XStepMetric
            arguments
                this (1,1)
            end
            XStep = abs(mean(diff(this.x)));
        end
        
        
        function YStep = YStep(this)
            % Space between Y values
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.YStep
            % See also y YLabel YUnit YLim YRange YRangeMetric YStepMetric
            YStep = abs(mean(diff(this.y)));
        end
        
        
        function XRangeMetric = XRangeMetric(this)
            % XRange brought in meters (if feasible)
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.XRangeMetric
            % See also x XLabel XUnit XLim XRange XStep XStepMetric
            arguments
                this (1,1)
            end
            XRange = this.XRange;
            switch this.GeometryType
                case cl_image.indGeometryType('LatLong')
                    XRangeMetric = abs(XRange) * 6378137 * (pi/180) * cosd(mean(this.y));
                case cl_image.indGeometryType('GeoYX')
                    XRangeMetric = abs(XRange);
                otherwise
                    XRangeMetric = NaN;
            end
        end
        
        
        function YRangeMetric = YRangeMetric(this)
            % YRange brought in meters (if feasible)
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.YRangeMetric
            % See also y YLabel YUnit YLim YRange YStep YStepMetric
            arguments
                this (1,1)
            end
            YRange = this.YRange;
            switch this.GeometryType
                case cl_image.indGeometryType('LatLong')
                    YRangeMetric = abs(YRange) * 6378137 * (pi/180);
                case cl_image.indGeometryType('GeoYX')
                    YRangeMetric = abs(YRange);
                otherwise
                    YRangeMetric = NaN;
            end
        end
        
        
        function XStepMetric = XStepMetric(this)
            % Space brought in meters (if feasible) between X values
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.XStepMetric
            % See also x XLabel XUnit XLim XRange XRangeMetric XStep
            arguments
                this (1,1)
            end
            switch this.GeometryType
                case cl_image.indGeometryType('LatLong')
                    XStepMetric = abs(mean(diff(this.x))) * 6378137 * (pi/180) * cos(mean(this.y) * (pi/180));
                case cl_image.indGeometryType('GeoYX')
                    XStepMetric = abs(mean(diff(this.x)));
                otherwise
                    XStepMetric = NaN;
            end
        end
        
        
        function YStepMetric = YStepMetric(this)
            % Space brought in meters (if feasible) between Y values
            % Examples
            %   a = cl_image.import_Prismed;
            %   a.YStepMetric
            % See also y YLabel YUnit YLim YRange YRangeMetric YStep
            arguments
                this (1,1)
            end
            switch this.GeometryType
                case cl_image.indGeometryType('LatLong')
                    YStepMetric = abs(mean(diff(this.y))) * 6378137 * (pi/180);
                case cl_image.indGeometryType('GeoYX')
                    YStepMetric = abs(mean(diff(this.y)));
                otherwise
                    YStepMetric = NaN;
            end
        end
    end
    
    methods
        function xout = get.Writable(this)
            arguments
                this (1,1)
            end
            if isa(this.Image, 'cl_memmapfile')
                xout = get(this.Image, 'Writable');
            else
                xout = 1;
            end
        end
        
        
        function this = set.Writable(this, X)
            arguments
                this (1,1)
                X    (1,1)
            end
            if isa(this.Image, 'cl_memmapfile')
                this.Image = set(this.Image, 'Writable', X); % TODO : tester ce que donnaient les appels avec chaines de caract�res 'true' et 'false' (sonar_MBES_projectionX)
            end
        end


        function str = get.StatSummary(this)
            % Statistic values of the image in a string
            str = stats2str(this.StatValues);
        end     


        function this = set.StatSummary(this, X)
            % Statistic values of the image in a string
            arguments
                this (1,1)
                X    (:,:) char
            end
            this.StatSummary = X;
        end     
        

        function this = set.ContourValues(this, X)
            % Set the contour plot values
            arguments
                this (1,1)
                X (1,:) {mustBeNumeric}
            end
            if isempty(this.ContourValues) && ~isempty(this.Image) && ~isempty(this.CLim)
                this.ContourValues = linspace(this.CLim(1), this.CLim(2), 10);
            else
                this.ContourValues = X;
            end
        end
        

        function this = set.ColormapIndex(this, X)
            % Setter of the colormap index (see strColormapIndex)
            arguments
                this (1,1)
                X (1,1) {mustBeInteger(X), mustBeGreaterThanOrEqual(X,1), mustBeLessThanOrEqual(X,22)} % N = numel(cl_image.strColormapIndex)
            end
            this = set_ColormapIndex(this, X);
            this.ColormapIndex = X;
        end
         
        
        function this = set.CLim(this, X)
            % Setter of the colormap limits
            arguments
                this (1,1)
                X (1,:) {checkCLim(this, X)}
            end
            if ischar(X) % Ex 0.5% 1% 3%
                switch X
                    case '0.5%'
                        this.CLim = this.StatValues.Quant_25_75;
                    case '1%'
                        this.CLim = this.StatValues.Quant_01_99;
                    case '3%'
                        this.CLim = this.StatValues.Quant_03_97;
                    otherwise
                        % Message ?
                end
            else
                this.CLim = X;
            end
        end

        function checkCLim(~, X)
            eid = 'CLim:NotExpectedValue';
            msg = 'CLim must be numeric value of size (1,2) or ''0.5%%'', ''1%%'' or ''3%%''.';
            if isnumeric(X)
                if isempty(X)
                    % OK
                elseif ~isequal(size(X), [1 2])
                    throwAsCaller(MException(eid,msg))
                end
            elseif ischar(X) && ~((strcmp(X, '0.5%') || strcmp(X, '1%') || strcmp(X, '3%')))
                throwAsCaller(MException(eid,msg))
            end
        end
        
        function this = set.x(this, X)
            % Setter of the X values
            arguments
                this (1,1)
                X {mustBeReal(X), mustBeNonNan(X), mustBeFinite(X)}
            end
            switch length(X)
                case 0
                    this.x = 1:this.nbColumns;
                case 2
                    this.x = linspace(X(1), X(2), this.nbColumns);
                otherwise
                    if this.nbColumns == 0
                        this.x = X(:)';
                    else
                        % Condition comment�e par JMA le 25/02/2022 car ne
                        % permet pas de d�livrer de nouvelles coordonn�es
                        % pour une image agrandie
%                         if length(X) == this.nbColumns
                            this.x = X(:)';
%                         else
%                             str1 = 'cl_image/set : Incompatibilite de taille entre x et Image. Je vous remplace les x par le numero de colonne';
%                             str2 = 'cl_image/set : the x length and the number of columns of the image are not equal. I am replacing your x values by numbers from 1 to nbCol';
%                             my_warndlg(Lang(str1,str2), 0);
%                             this.x = 1:this.nbColumns;
%                         end
                    end
            end
            
            % D�but : A LAISSER QUELQUES TEMPS ET A SUPPRIMER ENSUITE
            if ~isdeployed
                if ~isa(this.x, 'double')
                    my_breakpoint
                    my_warndlg('A signaler � JMA : this.x is not double cl_image/set', 1, 'Tag', 'xNonDouble');
                    this.x = linspace(double(this.x(1)), double(this.x(end)), length(this.x));
                end
            end
            % Fin : A LAISSER QUELQUES TEMPS ET A SUPPRIMER ENSUITE
            
            this.XLim = compute_XYLim(this.x);
        end
        
        
        function this = set.y(this, X)
            % Setter of the Y values
            arguments
                this (1,1)
                X {mustBeReal(X), mustBeNonNan(X), mustBeFinite(X)}
            end
            switch length(X)
                case 0
                    this.y = 1:this.nbRows;
                case 2
                    this.y = linspace(X(1), X(2), this.nbRows);
                otherwise
                    if this.nbRows == 0
                        this.y = X(:)';
                    else
                        % Condition comment�e par JMA le 25/02/2022 car ne
                        % permet pas de d�livrer de nouvelles coordonn�es
                        % pour une image agrandie
%                         if length(X) == this.nbRows
                            this.y = X(:)';
%                         else
%                             str1 = 'cl_image/set : Incompatibilite de taille entre y et Image. Je vous remplace les y par le numero de lignes';
%                             str2 = 'cl_image/set : the y length and the number of rows of the image are not equal. I am replacing your y values by numbers from 1 to nbRow';
%                             my_warndlg(Lang(str1,str2), 0);
%                             this.y = 1:this.nbRows;
%                         end
                    end
            end
            
            % D�but : A LAISSER QUELQUES TEMPS ET A SUPPRIMER ENSUITE
            if ~isdeployed
                if ~isa(this.y, 'double')
                    my_breakpoint
                    my_warndlg('A signaler � JMA : this.y is not double cl_image/set', 1, 'Tag', 'xNonDouble');
                    this.y = linspace(double(this.y(1)), double(this.y(end)), length(this.y));
                end
            end
            % Fin : A LAISSER QUELQUES TEMPS ET A SUPPRIMER ENSUITE
            
            this.YLim = compute_XYLim(this.y);
        end
    end
    
    
    methods(Static, Access = 'public')
        
        function ColormapIndex = indColormapIndex(str)
            % Index of a colormap name
            % Examples
            %   cl_image.strColormapIndex
            %   ind = cl_image.indColormapIndex('jet')
            %   cl_image.strColormapIndex{ind}
            % See also strColormapIndex ColormapIndex edit_ColormapIndex Colormap
            arguments
                str (1,:) {mustBeText(str)}
            end
            flag = strcmpi(str, cl_image.strColormapIndex);
            ColormapIndex = find(flag);
        end
        
        function YDir = indYDir(str)
            % Index of a Y direction name
            % Examples
            %   cl_image.strYDir
            %   ind = cl_image.indYDir('Down')
            %   cl_image.strYDir{ind}
            arguments
                str (1,:) {mustBeText(str)}
            end
            flag = strcmpi(str, cl_image.strYDir);
            YDir = find(flag);
        end
        
        function XDir = indXDir(str)
            % Index of a X direction name
            % Examples
            %   cl_image.strXDir
            %   ind = cl_image.indXDir('Left')
            %   cl_image.strXDir{ind}
            arguments
                str (1,:) {mustBeText(str)}
            end
            flag = strcmpi(str, cl_image.strXDir);
            XDir = find(flag);
        end
        
        function Video = indVideo(str)
            % Index of colormap video representation name (see strVideo)
            % Examples
            %   cl_image.strVideo
            %   ind = cl_image.indVideo('Inverse')
            %   cl_image.strVideo{ind}
            arguments
                str (1,:) {mustBeText(str)}
            end
            flag = strcmpi(str, cl_image.strVideo);
            Video = find(flag);
        end
        
        function SpectralStatus = indSpectralStatus(str)
            % Index of spectral status name
            % Examples
            %   cl_image.strSpectralStatus
            %   ind = cl_image.indSpectralStatus('Fourier')
            %   cl_image.strSpectralStatus{ind}
            % See also strSpectralStatus SpectralStatus
            arguments
                str (1,:) {mustBeText(str)}
            end
            flag = strcmpi(str, cl_image.strSpectralStatus);
            SpectralStatus = find(flag);
        end

        
        function ImageType = indImageType(str)
            % Index of a data type name
            % Examples
            %   cl_image.strImageType
            %   ind = cl_image.indImageType('RGB')
            %   cl_image.strImageType{ind}
            % See also strImageType ImageType edit_ImageType
            arguments
                str (1,:) {mustBeText(str)}
            end
            flag = strcmpi(str, cl_image.strImageType);
            ImageType = find(flag);
            % GROS BORDEL : je ne sais pas pourquoi mais strImageType se retrouve en
            % fran�ais, je ne comprends pas o� cela est modifi� : sans doute anciens
            % fichiers projet ?
            if isempty(ImageType)
                switch str
                    case {'Intensite'; 'Intensity'}
                        ImageType = 1;
                    case 'RGB'
                        ImageType = 2;
                    case {'Indexee'; 'Indexed'}
                        ImageType = 3;
                    case {'Binaire'; 'Boolean'}
                        ImageType = 4;
                end
            end
        end


        function indDT = indDataType(str)
            % Index of a type name
            % Examples
            %   cl_image.strDataType
            %   ind = cl_image.indDataType('Reflectivity')
            %   cl_image.strDataType{ind}
            % See also strDataType DataType edit_DataType
            arguments
                str (1,:) {mustBeText(str)}
            end

            flag = strcmpi(str, cl_image.strDataType);
            indDT = find(flag);
            if isempty(indDT)
                flag = strcmpi('unknown', cl_image.strDataType);
                indDT = find(flag);
            end
        end
        

        function GeometryType = indGeometryType(str)
            % Index of a geometry type name
            % Examples
            %   cl_image.strGeometryType
            %   ind = cl_image.indGeometryType('PingBeam')
            %   cl_image.strGeometryType{ind}
            % See also strGeometryType GeometryType edit_GeometryType
            arguments
                str (1,:) {mustBeText(str)}
            end
            flag = strcmpi(str, cl_image.strGeometryType);
            GeometryType = find(flag);
            if isempty(GeometryType)
                flag = strcmpi('unknown', cl_image.strGeometryType);
                GeometryType = find(flag);
            end
        end

        %% D�fintion des m�thodes statiques d�finies dans des fichiers .m s�par�s
        
        [flag, b] = import_ermapper(nomFic, varargin)

        [flag, b] = import_xml(nomFic, varargin)

        % TODO : adapter tous les import_Xxx(I0, nomFic) (voir cl_image/import)
        
        a = import_Prismed
        
        [flag, Unit]         = edit_DataUnit(varargin)
        [flag, DataType]     = edit_DataType(varargin)
        [flag, GeometryType] = edit_GeometryType(varargin)
        
        Unit = expectedUnitOfData(DataType)
        
        [ImageName, indGeometryType, indexDataType] = uncode_ImageName(ImageName)
        
        ImageName = extract_ImageNameTime(ImageName)
        
        [ImageName, version] = extract_ImageNameVersion(ImageName)
        
    end
    
    
    methods(Static, Access = 'private')
        
        function OriginColorbar = getStructOriginColorbar
            OriginColorbar.Tick      = [];
            OriginColorbar.TickLabel = [];
            OriginColorbar.YLim      = [];
            OriginColorbar.Image     = [];
        end
        
        
        function History = getStructHistory
            History.Function   = 'cl_image';
            History.Name       = '';
            History.Parameters = {};
        end
        
        function Sonar = getStructSonar
            Sonar.Desciption = cl_sounder([]);
            Sonar.RawDataResol         = [];
            Sonar.ResolutionD          = [];
            Sonar.ResolutionX          = [];
            Sonar.Time                 = [];
            Sonar.PingNumber           = [];
            Sonar.Immersion            = [];   % Immersion de l'antenne par rapport  la surface (valeur negative)
            Sonar.Height               = [];   % Hauteur par rapport au fond (valeur positive)
            Sonar.CableOut             = [];
            Sonar.Speed                = [];
            Sonar.Heading              = [];
            Sonar.Roll                 = [];
            Sonar.RollUsedForEmission  = [];
            Sonar.Pitch                = [];
            Sonar.Yaw                  = [];
            Sonar.SurfaceSoundSpeed    = [];
            Sonar.FishLatitude         = [];
            Sonar.FishLongitude        = [];
            Sonar.ShipLatitude         = [];
            Sonar.ShipLongitude        = [];
            Sonar.PortMode_1           = [];
            Sonar.PortMode_2           = [];
            Sonar.StarMode_1           = [];
            Sonar.StarMode_2           = [];
            Sonar.EM2040Mode2          = [];
            Sonar.EM2040Mode3          = [];
            Sonar.Heave                = [];
            Sonar.Tide                 = [];
            Sonar.PingCounter          = [];
            Sonar.SampleFrequency      = [];
            Sonar.SonarFrequency       = [];
            Sonar.Interlacing          = [];
            Sonar.TxAlongAngle         = [];
            Sonar.TxAcrossAngle        = []; % Equivalent du Athwart Angle pour les EK60.
            
            % Ajout� le 30/10/2010
            Sonar.NbSwaths             = [];
            Sonar.PresenceWC           = [];
            Sonar.PresenceFM           = [];
            Sonar.DistPings            = [];
            
            % Ajout� le 14/10/2012
            Sonar.TrueHeave            = [];
            Sonar.Draught              = [];
            Sonar.BathymetryStatus.TideApplied = 0;
            
            Sonar.BSN                  = [];
            Sonar.BSO                  = [];
            Sonar.TVGN                 = [];
            Sonar.TVGCrossOver         = [];
            Sonar.TxBeamWidth          = [];
            Sonar.ResolAcrossSample    = [];
            
            % Ajout� le 10/07/15
            Sonar.AbsorptionCoeff_RT   = [];
            Sonar.AbsorptionCoeff_SSc  = [];
            
            % RAJOUTER MultiPingSequence pour Reson7150
            
            % TODO : faire plut�t une agr�gation de cl_soundSpeed
            Sonar.BathyCel.Z           = [];
            Sonar.BathyCel.T           = [];
            Sonar.BathyCel.S           = [];
            Sonar.BathyCel.C           = [];
            
            % TODO : faire une agr�gation de cl_SonarStatus
            Sonar.TVG.etat                     = 1;
            Sonar.TVG.strEtat                  = {'Compensated'; 'Not compensated'}; % TODO : Tranformer en variable de classe
            
            Sonar.TVG.origine                  = 1;
            Sonar.TVG.strOrigine               = {'RT'; 'SSc'; 'User'}; % TODO : Tranformer en variable de classe
            
            Sonar.TVG.ConstructTypeCompens     = 1;
            Sonar.TVG.strConstructTypeCompens  = {'Analytique'; 'Table'}; % TODO : Tranformer en variable de classe
            
            
            % TVG(t) = CoefDiverg * log10(R) + Alpha * R + Constante
            Sonar.TVG.ConstructCoefDiverg      = 40;
            Sonar.TVG.ConstructAlpha           = 0;   % Attenuation en dB/km
            Sonar.TVG.ConstructConstante       = 0;   % Constante en dB (cas du DF1000)
            
            % Les corrections sont definies par frequence, donc par secteur
            % d'emission. Si les tableaux ne comportent qu'une ligne alors les
            % valeur sont considerees comme etant valables pour tous les secteurs
            
            % Sonar.TVG.ConstructTable(1,:) contient les distances en m
            % Sonar.TVG.ConstructTable(2,:) contient les gain en dB si pas
            %                                     de distinction de secteurs
            % Sonar.TVG.ConstructTable(n,:) contient les gain en dB du
            %                                    secteur numero n-2
            Sonar.TVG.ConstructTable           = [];
            
            % Sonar.TVG.IfremerAlpha(1)   contient le coefficient d'attenuation en dB/km suppose egal
            %                                  pour toutes les profondeurs
            % Sonar.TVG.IfremerAlpha(1,:) contient les profondeurs en m
            % Sonar.TVG.IfremerAlpha(2,:) contient les coefficient d'attenuation moyennes en dB/km si pas
            %                                  de distinction de secteurs
            % Sonar.TVG.IfremerAlpha(n,:) contient le coefficient d'attenuation moyennes en dB/km du
            %                                  secteur numero n-1
            Sonar.TVG.IfremerCoefDiverg        = 40;
            Sonar.TVG.IfremerAlpha             = 0;
            Sonar.TVG.IfremerConstante         = 0;
            
            Sonar.DiagEmi.etat                     = 1;
            Sonar.DiagEmi.strEtat                  = {'Compensated'; 'Not compensated'}; % TODO : Tranformer en variable de classe
            Sonar.DiagEmi.origine                  = 1;
            Sonar.DiagEmi.strOrigine               = {'RT'; 'SSc'}; % TODO : Tranformer en variable de classe
            Sonar.DiagEmi.ConstructTypeCompens     = 1;
            Sonar.DiagEmi.strConstructTypeCompens  = {'Analytique'; 'Table'}; % TODO : Tranformer en variable de classe
            Sonar.DiagEmi.IfremerTypeCompens       = 1;
            Sonar.DiagEmi.strIfremerTypeCompens    = {'Analytique'; 'Table'}; % TODO : Tranformer en variable de classe
            
            Sonar.DiagRec.etat                     = 1;
            Sonar.DiagRec.strEtat                  = {'Compensated'; 'Not compensated'}; % TODO : Tranformer en variable de classe
            Sonar.DiagRec.origine                  = 1;
            Sonar.DiagRec.strOrigine               = {'RT'; 'SSc'}; % TODO : Tranformer en variable de classe
            Sonar.DiagRec.ConstructTypeCompens     = 1;
            Sonar.DiagRec.strConstructTypeCompens  = {'Analytique'; 'Table'}; % TODO : Tranformer en variable de classe
            Sonar.DiagRec.IfremerTypeCompens       = 1;
            Sonar.DiagRec.strIfremerTypeCompens    = {'Analytique'; 'Table'}; % TODO : Tranformer en variable de classe
            
            Sonar.AireInso.etat                    = 2;
            Sonar.AireInso.strEtat                 = {'Compensated'; 'Not compensated'}; % TODO : Tranformer en variable de classe
            Sonar.AireInso.origine                 = 1;
            Sonar.AireInso.strOrigine              = {'RT'; 'SSc RxBeamAngle'; 'SSc IncidenceAngle'}; % TODO : Tranformer en variable de classe
            
            Sonar.NE.etat                          = 1;
            Sonar.NE.strEtat                       = {'Compensated'; 'Not compensated'}; % TODO : Tranformer en variable de classe
            
            Sonar.SH.etat                          = 1;
            Sonar.SH.strEtat                       = {'Compensated'; 'Not compensated'}; % TODO : Tranformer en variable de classe
            
            Sonar.GT.etat                          = 1;
            Sonar.GT.strEtat                       = {'Compensated'; 'Not compensated'}; % TODO : Tranformer en variable de classe
            
            Sonar.BS.etat                          = 1;
            Sonar.BS.strEtat                       = {'Compensated'; 'Not compensated'};
            
            Sonar.BS.origine                       = 1;
            Sonar.BS.strOrigine                    = {'Belle Image'; 'Full compensation'}; % TODO : Tranformer en variable de classe
            Sonar.BS.origineBelleImage             = 1;
            Sonar.BS.strOrigineBelleImage          = {'Lambert'; 'Lurton'}; % TODO : Tranformer en variable de classe
            Sonar.BS.origineFullCompens            = 1;
            Sonar.BS.strOrigineFullCompens         = {'Analytique'; 'Table'}; % TODO : Tranformer en variable de classe
            Sonar.BS.LambertCorrection             = 1;
            Sonar.BS.strLambertCorrection          = {'R/Rn'; 'RxAngleEarth'; 'IncidenceAngle'}; % TODO : Tranformer en variable de classe
            
            Sonar.BS.IfremerCompensModele          = ClModel.empty;
            Sonar.BS.IfremerCompensTable           = []; % 	CompensTable(1,:) contient les angles d'incidence en deg
            % IfremerCompensTable(2,:) contient les valeurs de correction en deg
            Sonar.BS.IdentAlgoSnippets2OneValuePerBeam = [];
            Sonar.BS.SpecularReestablishment           = [];
            
            
            Sonar.Ship.Arrays.Transmit.X        = [];
            Sonar.Ship.Arrays.Transmit.Y        = [];
            Sonar.Ship.Arrays.Transmit.Z        = [];
            Sonar.Ship.Arrays.Transmit.Roll     = [];
            Sonar.Ship.Arrays.Transmit.Pitch    = [];
            Sonar.Ship.Arrays.Transmit.Heading  = [];
            Sonar.Ship.Arrays.Transmit.GyrocompassHeadingOffset = [];
            Sonar.Ship.Arrays.Receive.X         = [];
            Sonar.Ship.Arrays.Receive.Y         = [];
            Sonar.Ship.Arrays.Receive.Z         = [];
            Sonar.Ship.Arrays.Receive.Roll      = [];
            Sonar.Ship.Arrays.Receive.Pitch     = [];
            Sonar.Ship.Arrays.Receive.Heading   = [];
            Sonar.Ship.MRU.X                    = [];
            Sonar.Ship.MRU.Y                    = [];
            Sonar.Ship.MRU.Z                    = [];
            Sonar.Ship.MRU.RollCalibration      = 0; % [];
            Sonar.Ship.MRU.PitchCalibration     = 0; % [];
            Sonar.Ship.MRU.HeadingCalibration   = [];
            Sonar.Ship.MRU.TimeDelay            = [];
            Sonar.Ship.GPS.X                    = [];
            Sonar.Ship.GPS.Y                    = [];
            Sonar.Ship.GPS.Z                    = [];
            Sonar.Ship.GPS.TimeStampUse         = [];
            Sonar.Ship.WaterLineVerticalOffset  = [];
        end
    end
end

% Les TODO :

% DataType : NE PAS OUBLIER DE METTRE A JOUR LA METHODE isIntegerVal
% SI AJOUT DE NOUVEAUX TYPES DE DONNEES
% TODO : faire le m�nage avec RayPathSampleNb dans la R2013a (remplacer par
% TwoWayTravelTimeInSamples. Faire le m�nage pour tous les "range"
% Remarque pour la refonte de SSc
% Il faudrait �viter d'utiliser "RayPathLength" car ce layer peut engendrer pas mal de probl�mes
% Supprimer certains de ces layers :
% identRayTimeLength             = cl_image.indDataType('RayPathTravelTime');
% identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
% identRayPathTravelTime         = cl_image.indDataType('RayPathTravelTime');
% identRayPathLength             = cl_image.indDataType('RayPathLength'); % One way length in meters
% identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
% identTwoWayTravelTimeInSamples = cl_image.indDataType('TwoWayTravelTimeInSamples');


% CHANGER KongsbergQualityFactor en FactoryQualityFactor

% TODO pour mettre un setter avec contr�le dans le constructeur,
% voir https://fr.mathworks.com/help/matlab/matlab_oop/property-set-methods.html
