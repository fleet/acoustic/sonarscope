% Nettoyage d'une courbe de statistiques conditionnelles
%
% Syntax
%   this = cleanCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, nbCurvesCreated] = cleanCourbesStats(this, varargin)

% TODO : attention, modif JMA le 08/02/2019 : les residuals ne sont plus cr��s, voir si calibration BSCorr est touch�

nbCourbesStatsBegin = length(this.CourbesStatistiques);

[varargin, subCourbes] = getPropertyValue(varargin, 'sub',      1:nbCourbesStatsBegin);
[varargin, NewCurve]   = getPropertyValue(varargin, 'NewCurve', 1);

% [varargin, NoPlot] = getFlag(varargin, 'NoPlot');
[varargin, last] = getFlag(varargin, 'last');

nbCurvesCreated = 0;

if last
    subCourbes = subCourbes(end);
end

if isempty(subCourbes)
    message_NoCurve(this)
    return
end

% [flag, CourbesStatistiquesOut] = CourbesStatsClean(this.CourbesStatistiques(subCourbes), varargin{:}); % TODO : pourquoi pas �a ?
for iCourbe=1:length(subCourbes)
    [flag, CourbesStatistiquesOut] = CourbesStatsClean(this.CourbesStatistiques(subCourbes(iCourbe)), varargin{:});
    if ~flag
        continue
    end
    if NewCurve
        this.CourbesStatistiques(end+1) = CourbesStatistiquesOut;
    else
        this.CourbesStatistiques(subCourbes(iCourbe)) = CourbesStatistiquesOut;
    end
end
%{
bilan = CourbesStatistiquesOut.bilan{1};
    bilan = this.CourbesStatistiques(subCourbes(iCourbe)).bilan{1};
    
    Colors = 'brkgmc';
    
    %  DataTypeConditions: {'TxAngle'  'SonarFrequency'} : tester si il s'agit v�ritablement de sectors (TxBeamIOndex ou TxBeamIndexSwath
    
    sectors = bilan;
    nbSectors = length(sectors);
    clear ySample
    k1 = 0;
    for k=1:nbSectors
        if isempty(bilan(k).y)
            continue
        end
        y{k} = bilan(k).y; %#ok<AGROW>
        Residu{k} = []; %#ok<AGROW>
        
        kmod = 1 + mod(k, length(Colors));
        xSample  = XSample('name', bilan(k).DataTypeConditions{1}, 'data', sectors(k).x);
        
        if isfield(sectors(k), 'Unit')
            Unit = sectors(k).Unit;
        else
            Unit = '';
        end
        ySample(1) = YSample('Name', sectors(k).titreV, 'data', sectors(k).y,  'unit',  Unit, ...
            'Color', Colors(kmod), 'marker', '.');
        ySample(2) = YSample('Name', sectors(k).titreV, 'data', sectors(k).nx, 'unit', 'nb', ...
            'Color', Colors(kmod), 'marker', '.');
        
        k1 = k1 + 1;
        sigCurves(k1) = ClSignal('Name', strcat('Curves ', num2str(k)), 'xSample', xSample, 'ySample', ySample); %#ok<AGROW>
    end
    a = SignalDialog(sigCurves, 'Title', 'sigCurves');
    a.openDialog();
    if ~a.okPressedOut
        return
    end
    
    % 'Affichage du signal nettoy� juste apr�s la fermeture de la fen�tre d''optimisation.'
    % a.signalList.plot
    
    curveCleaned = a.signalList;
    flagResidu = false;

    k1 = 0;
    for k=1:nbSectors
        if isempty(bilan(k).y)
            continue
        end
        k1 = k1 + 1;
        xSampleCleaned = curveCleaned(k1).xSample;
        ySampleCleaned = curveCleaned(k1).ySample;
        sub = find(~isinf(ySampleCleaned(2).data));
        bilan(k).x  = xSampleCleaned.data(sub);
        bilan(k).y  = ySampleCleaned(1).data(sub);
        bilan(k).nx = ySampleCleaned(2).data(sub);
        bilan(k).std = bilan(k).std(sub);
        bilan(k).sub = (1:length(sub))';
        if isfield(bilan, 'ymedian')
            bilan(k).ymedian = bilan(k).ymedian(sub);
        end
        res = bilan(k).y(:) - y{k}(sub);
        if all(res(~isnan(res)) == 0)
            Residu{k} = [];
            Filtre(k).Ordre = 0; %#ok<AGROW>
            Filtre(k).Wc    = 0; %#ok<AGROW>
        else
            Residu{k} = res;
            Filtre(k).Ordre = 1; %#ok<AGROW>
            Filtre(k).Wc = 0.1111111111; %#ok<AGROW>
            flagResidu = true;
        end
    end
    
    for k=1:nbSectors
        if flagResidu
            bilanNew(k) = bilan(k); %#ok<AGROW>
            if isempty(Residu{k})
                bilanNew(k).y = zeros(length(sub),1); %#ok<AGROW>
            else
                bilanNew(k).y = Residu{k}; %#ok<AGROW>
            end
            bilanNew(k).nomZoneEtude = sprintf('%s - Residu (%d,%f)',   bilanNew(k).nomZoneEtude, Filtre(k).Ordre, Filtre(k).Wc); %#ok
            bilan(k).nomZoneEtude    = sprintf('%s - Filtered (%d,%f)', bilan(k).nomZoneEtude,    Filtre(k).Ordre, Filtre(k).Wc);
        end
    end
    
    deb = strfind(bilan(1).nomZoneEtude, ' - Clean');
    if isempty(deb)
        bilan(1).nomZoneEtude = [bilan(1).nomZoneEtude ' - Clean 1'];
    else
        k = str2num(bilan(1).nomZoneEtude(deb+9:end)); %#ok<ST2NM>
        bilan(1).nomZoneEtude = [bilan(1).nomZoneEtude(1:deb-1) ' - Clean ' num2str(k+1)];
    end
    
    if NewCurve
        if flagResidu
            this.CourbesStatistiques(end+1).bilan{1} = bilan;
            this.CourbesStatistiques(end+1).bilan{1} = bilanNew;
            if ~NoPlot
                n = length(this.CourbesStatistiques);
                plotCourbesStats(this, 'sub', n-1, 'Stats', 0);
                plotCourbesStats(this, 'sub', n,   'Stats', 0);
            end
        else
            this.CourbesStatistiques(end+1).bilan{1} = bilan;
            if ~NoPlot
                plotCourbesStats(this, 'sub', length(this.CourbesStatistiques), 'Stats', 0);
            end
        end
    else
        this.CourbesStatistiques(subCourbes(iCourbe)).bilan{1} = bilan;
        if ~NoPlot
            plotCourbesStats(this, 'sub', subCourbes(iCourbe), 'Stats', 0);
        end
    end
end

% 'Affichage du signal nettoy� � la fin de la fonction de nettoyage.'
% plotCourbesStats(this, 'sub', length(this.CourbesStatistiques), 'Stats', 0);

nbCourbesStatsEnd = length(this.CourbesStatistiques);
nbCurvesCreated = nbCourbesStatsEnd - nbCourbesStatsBegin;
%{
new_bilan(k).nomZoneEtude = sprintf('%s - Filtre (%d,%f)', bilan(k).nomZoneEtude, Ordre, Wc); %#ok
residu_bilan(k) = bilan(k);%#ok
residu_bilan(k).y = bilan(k).y - y; %#ok
residu_bilan(k).nomZoneEtude = sprintf('%s - Residu (%d,%f)', bilan(k).nomZoneEtude, Ordre, Wc); %#ok
else
new_bilan(k) = bilan(k);%#ok
residu_bilan(k) = bilan(k);%#ok
residu_bilan(k).y(:) = 0; %#ok
residu_bilan(k).nomZoneEtude = sprintf('%s - Residu (No filter)', bilan(k).nomZoneEtude); %#ok
%}
%}
