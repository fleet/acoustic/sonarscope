function that = clone(this)

if isa(this.Image, 'cl_memmapfile')
    ThisFileName = get(this.Image, 'FileName');
    ThatFileName =  my_tempname('.memmapfile');
    status = copyfile(ThisFileName, ThatFileName);
    if status
        that = this;
        that.Image = set(that.Image, 'ChangeFileName', 'FileName', ThatFileName);
    else
        my_warndlg('cl_image/set_Image : erreur', 0);
        that.Image = this.Image(:,:,:);
    end
else
    that = this;
end
