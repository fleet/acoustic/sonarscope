% Create the full image name of an image (with DataType and GeometryType signatures)
%
% Syntax
%   ImageName = code_ImageName(a, ...)
%
% Input Arguments
%   a : Instance of cl_image
%
% Name-Value Pair Arguments
%   ImageName : Name of the image
%   Append    : Extension of the name
%
% Output Arguments
%   ImageName : Name of the image
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15);
%     a.Name
%   ImageName = code_ImageName(a, 'Name', 'Hellow World')
%   ImageName = code_ImageName(a, 'Name', 'Hellow World', 'Append', 'I am Charlie')
%   ImageName = code_ImageName(a, 'Name', 'Hellow World', 'WithAttributs', 0)
%
% See also cl_image/extract_ImageName cl_image/update_Name Authors
% ----------------------------------------------------------------------------

function ImageName = code_ImageName(this, varargin)

[varargin, ImageName]         = getPropertyValue(varargin, 'Name',          this.Name);
[varargin, Append]            = getPropertyValue(varargin, 'Append',        []);
[varargin, flagWithAttributs] = getPropertyValue(varargin, 'WithAttributs', 1);
[varargin, regexprepVal]      = getPropertyValue(varargin, 'regexprep',     []); %#ok<ASGLU>

if ~isempty(regexprepVal)
    ImageName = regexprep(ImageName, regexprepVal{1}, regexprepVal{2});
end

if ~isempty(Append)
    ImageName = [ImageName '_' Append];
end

%% Extraction du nom de l'image en supprimant les attributs GeometryType et DataType

ImageName = extract_ImageName(this, 'Name', ImageName);

%% On verifie si il y a des "." dans le nom de l'image et on les remplace
% par "_" car les exportations d'image cr�ent un nom compos� � partir de
% l'image et si il a plusieurs points �a fout la m....

% TODO : y'a moyen de faire �a en une seule fois avec les expressions
% r�guli�res, genre tout ce qui n'est pas num�rique et alphanum�rique

ImageName = removeAccentuation(ImageName);
ImageName = strrep(ImageName, '.', '_');
ImageName = strrep(ImageName, '[', '_');
ImageName = strrep(ImageName, ']', '_');
ImageName = strrep(ImageName, '{', '_');
ImageName = strrep(ImageName, '}', '_');
ImageName = strrep(ImageName, '(', '_');
ImageName = strrep(ImageName, ')', '_');
ImageName = strrep(ImageName, '*', '_');
ImageName = strrep(ImageName, '$', '_');
ImageName = strrep(ImageName, '&', '_');
ImageName = strrep(ImageName, '@', '_');
ImageName = strrep(ImageName, '=', '_');
ImageName = strrep(ImageName, '�', '_');
ImageName = strrep(ImageName, '~', '_');
ImageName = strrep(ImageName, '"', '_');
ImageName = strrep(ImageName, '#', '_');
ImageName = strrep(ImageName, '/', '_');
ImageName = strrep(ImageName, '\', '_');
ImageName = strrep(ImageName, '!', '_');
ImageName = strrep(ImageName, '|', '_');
ImageName = strrep(ImageName, '?', '_');
ImageName = strrep(ImageName, '^', '_');
ImageName = strrep(ImageName, '%', '_');
ImageName = strrep(ImageName, '�', '_');
ImageName = strrep(ImageName, '`', '_');
ImageName = strrep(ImageName, '''', '_');

ImageName(ImageName == 32) = [];
if ~isempty(ImageName)
    if strcmp(ImageName(1), '_')
        ImageName = ImageName(2:end);
    end
end

ImageName = strrep(ImageName, '_-_', '_');

%% On complete le nom du layer avec la g�ometrie et le type de donn�e

if flagWithAttributs
    if this.GeometryType ~= 1
        Geometry  = this.strGeometryType{this.GeometryType};
        ImageName = sprintf('%s_%s', ImageName, Geometry);
    end
    if this.DataType ~= 1
        DataType  = cl_image.strDataType{this.DataType};
        ImageName = sprintf('%s_%s', ImageName, DataType);
    end
end

ImageName = strrep(ImageName, '__', '_');
ImageName = strrep(ImageName, '__', '_');
ImageName = strrep(ImageName, '__', '_');
ImageName = strrep(ImageName, '__', '_');
ImageName = strrep(ImageName, '__', '_');
ImageName = strrep(ImageName, '__', '_');
