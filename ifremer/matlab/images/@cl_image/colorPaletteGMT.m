% Ecriture d'une table de couleurs GMT (fhichier geoid.cpt)
%
% Syntax
%   flag = colorPaletteGMT(Zmin, Zmax)
%
% Input Arguments
%   Zmin : altitude Min
%   Zmax : altitude Max
%   Zpas : r�solution hauteur
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%   colorPaletteGMT(-4192, -822);
%
% See also cl_image/plotGMT Authors
% Authors : GLU
%--------------------------------------------------------------------------

function colorPaletteGMT(this, pathname, filename)

switch this.ImageType
    case 1
        Zmin = this.CLim(1);
        Zmax = this.CLim(2);
    case 2
        Zmin = this.OriginColorbar.YLim(1);
        Zmax = this.OriginColorbar.YLim(2);
    case 3
        return
end

str = sprintf('#	cpt file created by: colorPaletteGMT(%f, %f)\n',Zmin, Zmax);
str = sprintf('%s#COLOR_MODEL = RGB\n', str);
str = sprintf('%s#\n', str);

% Cr�ation de la table de couleurs
tableCouleur = this.Colormap*255;
[iPas nDim]= size(tableCouleur);%#ok
Zpas=(Zmax-Zmin)/(iPas-1);
Z = Zmin:Zpas:Zmax;
% Partie d�tach�e pour �viter la saturation des points sur des trac�s dans
% des projections hors MERCATOR (variable INTERPOLANT de GMT sans effet).
iLoop = 1;
EPS=1E-06;
str = sprintf('%s%f %d %d %d %f %d %d %d\n', str, ...	
                    Z(iLoop)*(1+EPS), fix(tableCouleur(iLoop, 1)),...
                    fix(tableCouleur(iLoop, 2)),fix(tableCouleur(iLoop, 3)) ,...
                    Z(iLoop+1), fix(tableCouleur(iLoop+1, 1)),...
                    fix(tableCouleur(iLoop+1, 2)),fix(tableCouleur(iLoop+1, 3)));
% Boucle principale.
for iLoop=2:iPas-2
    str = sprintf('%s%f %d %d %d %f %d %d %d\n', str, ...	
                        Z(iLoop), fix(tableCouleur(iLoop, 1)),...
                        fix(tableCouleur(iLoop, 2)),fix(tableCouleur(iLoop, 3)) ,...
                        Z(iLoop+1), fix(tableCouleur(iLoop+1, 1)),...
                        fix(tableCouleur(iLoop+1, 2)),fix(tableCouleur(iLoop+1, 3)));
end
% Partie d�tach�e pour �viter la saturation des points sur des trac�s dans
% des projections hors MERCATOR (variable INTERPOLANT de GMT sans effet).
iLoop = iLoop+1;
EPS=1E-06;
str = sprintf('%s%f %d %d %d %f %d %d %d\n', str, ...	
                    Z(iLoop), fix(tableCouleur(iLoop, 1)),...
                    fix(tableCouleur(iLoop, 2)),fix(tableCouleur(iLoop, 3)) ,...
                    Z(iLoop+1)*(1+EPS), fix(tableCouleur(iLoop+1, 1)),...
                    fix(tableCouleur(iLoop+1, 2)),fix(tableCouleur(iLoop+1, 3)));

str = sprintf('%sB	225	225 225\n',str);
str = sprintf('%sF	225	225 225\n',str);
% str = sprintf('%sN	128 128 128\n',str);    % Gris
str = sprintf('%sN	255 255 255\n',str);

fname = sprintf('%s\\%s.cpt', pathname, filename);
fid = fopen(fname, 'w');

fprintf(fid,'%s',str);
fclose(fid);
