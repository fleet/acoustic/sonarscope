% Colorisation d'une image par un masque
%
% Syntax
%   b = colorisation(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   LayerMask : Image comtenant un masque contenant des entiers
%   valMask   : Valeur du masque a utiliser
%   subx        : subsampling in X
%   suby        : subsampling in Y
%   zone        : {1='In'} | 2='Out'
%
% Output Arguments
%   b : Instance de cl_image contenant l'image masquee
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   a = cl_car_ima(nomFic);
%   I = get(a, 'REFLECTIVITY');
%   b = cl_image('Image', I, 'Name', 'REFLECTIVITY', 'Unit', 'dB', 'ColormapIndex', 2, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(b)
%
%   I = get(a, 'EMISSION_SECTOR');
%   c = cl_image('Image', I, 'Name', 'EMISSION_SECTOR', 'Unit', 'deg', 'ColormapIndex', 3, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(c)
%
%
%   d = colorisation(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = colorisation(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = colorisation(b, 'LayerMask', c, 'valMask', 2, 'zone', 2);
%   imagesc(d)
%
%   d = colorisation(b, 'LayerMask', c, 'valMask', [0 2 4 6 7 9]);
%   imagesc(d)
%
% See also masquage cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = colorisation(this, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_colorisation(this(k), varargin{:});
end


function this = unitaire_colorisation(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask', []);
[varargin, zone]      = getPropertyValue(varargin, 'zone', 1);
[varargin, alpha]     = getPropertyValue(varargin, 'alpha', 0.9); %#ok<ASGLU>

ImageName = this.Name;

Masque = extraction(LayerMask, 'x', this.x(subx), 'y', this.y(suby));
if zone == 1
    K = NaN(size(Masque.Image), 'single');
    for ik=1:length(valMask)
        %         sub = find(Masque.Image == valMask(ik));
        sub = (Masque.Image == valMask(ik));
        K(sub) = 1;
    end
else
    K = ones(size(Masque.Image), 'single');
    for ik=1:length(valMask)
        %         sub = find(Masque.Image == valMask(ik));
        sub = (Masque.Image == valMask(ik));
        K(sub) = NaN;
    end
end

switch Masque.ImageType
    case 1
        [Masque, flag] = Intensity2RGB(Masque);
        if ~flag
            return
        end
    case 3
        [Masque, flag] = Indexed2RGB(Masque);
        if ~flag
            return
        end
end

switch this.ImageType
    case 1
        [this, flag] = Intensity2RGB(this, 'subx', subx, 'suby', suby);
        if ~flag
            return
        end
    case 3
        [this, flag] = Indexed2RGB(this, 'subx', subx, 'suby', suby);
        if ~flag
            return
        end
end

Image = zeros([length(suby) length(subx) 3], 'uint8');
sub = ~isnan(sum(K,3));
for k=1:3
    I = single(this.Image(:,:,k));
    M = single(Masque.Image(:,:,k));
    
    maxI = max(I(:));
    maxM = max(M(:));
    if maxI > 1
        if maxM <= 1
            M = M * 256;
        end
    else
        if maxM > 1
            M = M / 256;
        end
    end
    
    I(sub) = alpha * I(sub) + (1-alpha) * M(sub);
    I(sub) = I(sub) .* K(sub);
    
    if maxI >1
        Image(:,:,k) = I;
    else
        Image(:,:,k) = I * 255;
    end
end

this = replace_Image(this, Image);
this.ValNaN = 0;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.ImageType = this.ImageType;

%% Compl�tion du titre

this.Name = ImageName;
if zone == 1
    Append = sprintf('colorisationIn%3.1f', alpha);
else
    Append = sprintf('colorisationOut%3.1f', alpha);
end
this = update_Name(this, 'Append', Append);
