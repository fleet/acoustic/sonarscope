function [flag, this] = combineMasks(this, Mask2, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

Name1 = extract_ImageName(this);
Name2 = extract_ImageName(Mask2);
Name1 = strrep(Name1, '_', '');
Name2 = strrep(Name2, '_', '');
[Mask2, flag] = extraction(Mask2, 'x', this.x(subx), 'y', this.y(suby));
if ~flag
    return
end

%% Algorithme

I1 = this.Image(suby,subx);
I2 = Mask2.Image;
I3 = zeros(length(suby), length(subx), 'uint8');
Val1 = this.StatValues.Min:this.StatValues.Max;
nbVal1 = length(Val1);
Val2 = Mask2.StatValues.Min:Mask2.StatValues.Max;
nbVal2 = length(Val2);
for k1=1:nbVal1
    for k2=1:nbVal2
        k3 = k2 + (k1-1) * nbVal1;
        sub = (I1 == Val1(k1)) & (I2 == Val2(k2));
        I3(sub) = k3;
        str{k3} = sprintf('Value %d corresponds to %s_%d & %s_%d', k3, Name1, Val1(k1), Name2, Val2(k2)); %#ok<AGROW>
    end
end

%% Cr�ation de l'instance

this = replace_Image(this, I3);
this.ValNaN = NaN;

%% Mise � jour des coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Comb');

this.Comments = cell2str(str);

flag = 1;
