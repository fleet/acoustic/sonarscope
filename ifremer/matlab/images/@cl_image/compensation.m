% Compensation d'une image
%
% Syntax
%   [flag, b, a] = compensation(a, ...)
%
% Input Arguments
%   flag : 1=OK, 0=KO
%   b : Instances de cl_image
%   a : Instance de cl_image �ventuellement comp�t�e
%
% Name-Value Pair Arguments
%   Type : {'1=Moyennage vertical'} | '2=Moyennage horizontal' | '3=plan incline' | '4=polynome'
%   pX   : Ordre du polynome en X (si Type=4)
%   pY   : Ordre du polynome en Y (si Type=4)
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', single(I), 'Name', label);
%   imagesc(a);
%   nbColumns = a.nbColumns)
%   nbRows     = a.nbRows
%
%   angles = linspace(-75, 75, nbColumns);
%   BS = BSLurton(angles, [-5, 3, -30, 2., -20, 10] );
%
%   % Ajout d'une modulation horizontale
%   aH = a + BS;
%   imagesc(aH);
%
%   [flag, cH] = compensation(aH);
%   imagesc(cH);
%   mean_col(cH)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that, this] = compensation(this, varargin)

nbImages = length(this);
that     = cl_image.empty;

for k=1:nbImages
    [flag, a, this(k)] = unitaire_compensation(this(k), varargin{:});
    if ~flag
        my_warndlg('No compensation could be found, sorry.', 0);
        return
    end
    that = [that a]; %#ok<AGROW>
end


function [flag, that, this] = unitaire_compensation(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Type]           = getPropertyValue(varargin, 'Type',      1);
[varargin, pX]             = getPropertyValue(varargin, 'pX',        2);
[varargin, pY]             = getPropertyValue(varargin, 'pY',        2);
[varargin, CleanData]      = getPropertyValue(varargin, 'CleanData', 1);
[varargin, TypeOperation]  = getPropertyValue(varargin, 'TypeOperation',  '-');
[varargin, NomFicPolynome] = getPropertyValue(varargin, 'NomFicPolynome', []);
[varargin, dispCoeffPoly]  = getPropertyValue(varargin, 'dispCoeffPoly',  1);
[varargin, ValLim]         = getPropertyValue(varargin, 'ValLim',         []);
[varargin, SeuilCompPoly]  = getPropertyValue(varargin, 'SeuilCompPoly',  []); %#ok<ASGLU>
% [varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
% [varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []);

% flag = 0;
that = [];

switch Type
    case 1  % Compensation verticale par moyenne
        ExtensionTitre = 'CompensationVert';
        TypeCurve = 'Mean';
        valOrig = mean_col(this, 'subx', subx, 'suby', suby);
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig', subx, suby, CleanData, TypeOperation, 'Vert');
        
    case 2 % Compensation horizontale par moyenne
        ExtensionTitre = 'CompensationHorz';
        valOrig = mean_lig(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Mean';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig, subx, suby, CleanData, TypeOperation, 'Horz');
        
    case 5 % Compensation verticale par valeur m�diane
        ExtensionTitre = 'CompensationVertMedian';
        TypeCurve = 'Median';
        valOrig = median_col(this, 'subx', subx, 'suby', suby);
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig', subx, suby, CleanData, TypeOperation, 'Vert');
        
    case 6 % Compensation horizontale par valeur m�diane
        ExtensionTitre = 'CompensationHorzMedian';
        valOrig = median_lig(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Median';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig, subx, suby, CleanData, TypeOperation, 'Horz');
        
    case 7 % Compensation verticale par valeur max
        ExtensionTitre = 'CompensationVertMax';
        valOrig = max_col(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Maximum';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig', subx, suby, CleanData, TypeOperation, 'Vert');
        
    case 8 % Compensation verticale par valeur min
        ExtensionTitre = 'CompensationVertMin';
        valOrig = min_col(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Minimum';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig', subx, suby, CleanData, TypeOperation, 'Vert');
        
    case 9 % Compensation horizontale Max
        ExtensionTitre = 'CompensationHorzMax';
        valOrig = max_lig(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Maximum';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig, subx, suby, CleanData, TypeOperation, 'Horz');
        
    case 10 % Compensation horizontale Min
        ExtensionTitre = 'CompensationHorzMin';
        valOrig = min_lig(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Minimum';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig, subx, suby, CleanData, TypeOperation, 'Horz');
        
    case 12 % Compensation Vert Sum
        ExtensionTitre = 'CompensationVertSum';
        valOrig = sum_col(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Summation';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig', subx, suby, CleanData, TypeOperation, 'Vert');
        
    case 13 % Compensation horizontale Sum
        ExtensionTitre = 'CompensationHorzSum';
        valOrig = sum_lig(this, 'subx', subx, 'suby', suby);
        TypeCurve = 'Summation';
        [Image, cor] = correctionCourbe(this, TypeCurve, valOrig, subx, suby, CleanData, TypeOperation, 'Horz');
        
    case 3 % Tilted plan
        ExtensionTitre = 'CompensationTiltedPlan';
        [I, subNaN] = windowEnlarge(this.Image(suby, subx), [0 0], this.ValNaN);
        I(subNaN) = NaN;
        
        % -------------------------------------------------------------
        % polyfitSurf est tr�s gourgmand en temps cpu. On
        % sous-�chantillonne la donn�e si besoin
        
        pasx = ceil(length(subx) / 1000);
        pasy = ceil(length(suby) / 1000);
        subsubx = 1:pasx:length(subx);
        subsuby = 1:pasy:length(suby);
        [flag, C, A, Eqm] = polyfitSurf(this.x(subx(subsubx)), this.y(suby(subsuby)), double(I(subsuby,subsubx)), 'Plan'); %#ok<ASGLU>
        [Azimuth, pente, penteDeg] = poly2penteAzimut(C); %#ok<ASGLU>
        if this.GeometryType == cl_image.indGeometryType('GeoYX') % Coordonn�es m�triques
            str1 = sprintf('La pente trouv�e est de %s degres dans la direction %s deg/Nord', ...
                num2str(penteDeg), num2str(Azimuth));
            str2 = sprintf('Slope is %s degrees in %s deg/Nord direction', ...
                num2str(penteDeg), num2str(Azimuth));
            my_warndlg(Lang(str1,str2), 0);
        elseif this.GeometryType == cl_image.indGeometryType('LatLong')
            XStepMetric = this.XStepMetric;
            YStepMetric = this.XStepMetric;
            XStep = get(this, 'XStep');
            YStep = get(this, 'YStep');
            P = [XStepMetric * sind(Azimuth), YStepMetric * cosd(Azimuth)] / sqrt(XStepMetric^2 + YStepMetric^2);
            Azimuth = atan2(P(1), P(2)) * (180/pi);
            
            penteDeg = atand(tand(penteDeg) * sqrt(XStep^2 + YStep^2) / sqrt(XStepMetric^2 + YStepMetric^2));
            str1 = sprintf('La pente trouv�e est de %s degr�s dans la direction %s deg/Nord', ...
                num2str(penteDeg), num2str(Azimuth));
            str2 = sprintf('Slope is %s degrees in %s deg/Nord direction', ...
                num2str(penteDeg), num2str(Azimuth));
            my_warndlg(Lang(str1,str2), 0);
        end
        messageEquation(C)
        if ~isempty(NomFicPolynome)
            Polynome = C;
            save(NomFicPolynome, 'Polynome')
        end
        cor = polyvalSurf(C, this.x(subx), this.y(suby), '2D');
        if ~isa(I, 'double')
            I = single(I);
        end
        Image = correctionImage(I, cor, TypeOperation);
        
    case 4 % Polynome
        ExtensionTitre = 'CompensationPoly';
        Image = singleUnlessDouble(this.Image(suby, subx), this.ValNaN);
        [flag, cor, C2] = modelPolyImage(Image, this.x(subx), this.y(suby), NaN, pX, pY, ...
            'ValLim', ValLim, 'SeuilCompPoly', SeuilCompPoly);
        if isempty(cor)
            return
        end
        %         if ~flag % Comment� en attendant qu'on fasse le clair sur le retour de flag dans correctionPolynomiale
        %             return
        %         end
        if dispCoeffPoly
            messageEquation(C2)
        end
        if ~isempty(NomFicPolynome)
            Polynome = C2;
            save(NomFicPolynome, 'Polynome')
        end
        Image = correctionImage(Image, cor, TypeOperation);
        
    case 11 % Polynome dans .xml
        C = loadmat(NomFicPolynome, 'nomVar', 'Polynome');
        messageEquation(C)
        ExtensionTitre = 'CompensationPoly';
        [I, subNaN] = windowEnlarge(this.Image(suby, subx), [0 0], this.ValNaN);
        I(subNaN) = NaN;
        if ~isa(I, 'double')
            I = single(I);
        end
        XData = this.x(subx);
        YData = this.y(suby);
        cor = polyvalSurf(C, XData, YData, 'Type', class(I), '2D');
        Image = correctionImage(I, cor, TypeOperation);
    otherwise
        disp('Pas encore fait')
end
if isempty(Image)
    flag = 0;
    return
end

%% Output images

that    = inherit(this, cor,   'subx', subx, 'suby', suby, 'AppendName', '_PolySurf',    'ValNaN', NaN, 'ColormapIndex', 3);
that(2) = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', ExtensionTitre, 'ValNaN', NaN, 'ColormapIndex', 3);

flag = 1;


function messageEquation(C)

for k=1:size(C,2)
    xnames{k} = sprintf('x^%d', k-1); %#ok<AGROW>
end

for k=1:size(C,1)
    ynames{k} = sprintf('y^%d', k-1); %#ok<AGROW>
end

f = figure('Position',[100 100 600 350], 'Name', 'Coefficients for surface fitting');
t = uitable('Data', C, 'ColumnName', xnames, 'RowName', ynames, ...
    'Parent', f, 'Units', 'normalized ', 'Position', [0 0 1 1]); %#ok<NASGU>


function I = correctionImage(I, cor, TypeOperation)

switch TypeOperation
    case '-'
        I = I - cor;
    case '/'
        I = I ./ cor;
    case '+'
        I = I + cor;
    case '*'
        I = I .* cor;
end


function [Image, cor] = correctionCourbe(this, TypeCurve, valOrig, subx, suby, CleanData, TypeOperation, Direction)

flagY = strcmp(Direction, 'Horz');
if flagY
    Axe = 'Y';
    valOrig = valOrig';
else
    Axe = 'X';
end

% % Modif MOZ4
% if ~isdeployed
%     val = valOrig;
% else
if CleanData
    nomCourbe   = [];
    Commentaire = [];

    switch Direction
        case 'Horz'
            x = suby;
        case 'Vert'
            x = subx;
        otherwise
            x = 1:length(valOrig);
    end
    Title = sprintf('Edit compensation curve for %s', this.Name);
    xSample = XSample('name', 'Samples',  'data', x);
    ySample = YSample('Name', TypeCurve, 'data', valOrig, 'Unit', this.Unit);
    signal = ClSignal('Name', Direction, 'xSample', xSample, 'ySample', ySample);
    s = SignalDialog(signal, 'Title', Title, 'cleanInterpolation', 1);
    s.openDialog();
    if s.okPressedOut
        val = ySample.data;
    end
    
    str1 = 'Sauvegarde de la courbe dans SSc ?';
    str2 = 'Save the curve dans SSc ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        Image = [];
        cor = [];
        return
    end
    if flag && (rep == 1)
        this = memoriseCourbe(this, val, Axe, subx, suby, 'Heritage', this, ...
            'TypeCurve', TypeCurve, 'CurveName', nomCourbe, 'Comment', Commentaire);
    end
else
    val = valOrig;
end
% end
% % % Fin modif MOZ4

for k=this.nbSlides:-1:1
    if flagY
        cor = repmat(val(k,:)', 1, length(subx));
    else
        cor = repmat(val(k,:), length(suby), 1);
    end
    
    % Anulation modif Arnaud car �a ne fait plus ce qui �tait pr�vu au d�part
    %{
% Modif MOZ4
if ~isdeployed
fun = @(block_struct) ...
median(block_struct.data(:)) * ones(size(block_struct.data));
corping   = blockproc(cor,[4 size(cor,2)], fun); % mediane tous les 4 pings
cormedian = blockproc(cor,[100 size(cor,2)], fun); % mediane tous les 100 pings
idxWrong  = abs(corping - cormedian) > 3; %-5dB
cor(idxWrong) = corping(idxWrong)-cormedian(idxWrong);
cor(~idxWrong) = zeros(size(cor(~idxWrong)));
end
% Fin modif MOZ4
    %}
    
    [I, subNaN] = windowEnlarge(this.Image(suby, subx, k), [0 0], this.ValNaN);
    I(subNaN) = NaN;
    if ~(isa(I, 'double') || isa(I, 'single'))
        I = single(I);
    end
    Image(:, :,k) = correctionImage(I, cor, TypeOperation);
end
