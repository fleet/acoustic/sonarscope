% Compensation d'une image par des courbes statistiques conditionnelles
%
% Syntax
%   [this, flag] = compensationCourbesStats(this, b, bilan, ...)
%
% Input Arguments
%   this  : Instance de cl_image
%   b     : Instances des layers conditionnels
%   bilan : Courbes statistiques
%
% Name-Value Pair Arguments
%   subx :
%   suby :
%
% Output Arguments
%   this : Instance de cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%     nomFic = getNomFicDatabase('DF1000_ExStr.imo');
%     a = cl_car_sonar('nomFic', nomFic);
%     b = view(a, 'Sonar.Ident', 8, 'NumLayer', 1);
%     b = get_Image(b, 1);
%     imagesc(b)
%     c = sonar_lateral_emission(b);
%     imagesc(c)
%     layerXY.flag = [0 0];
%     [b, bilan] = courbesStats(b, 'Test', 'Zone test', 'Ceci est un commentaire', [], c, layerXY);
%
%     d = compensationCourbesStats(b, c, bilan);
%     imagesc(d)
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% DataTypeConditions = bilan{1}(1).DataTypeConditions;
function [this, flag] = compensationCourbesStats(this, b, bilan, varargin)

nbImages = length(this);
for k=1:nbImages
    [that, flag] = unitaire_compensationCourbesStats(this(k), b, bilan, varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end

function [this, flag] = unitaire_compensationCourbesStats(this, b, bilan, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, TypeOperation]            = getPropertyValue(varargin, 'TypeOperation',            '-');
[varargin, TypeStats]                = getPropertyValue(varargin, 'TypeStats',                'Mean');
[varargin, LayerMask]                = getPropertyValue(varargin, 'LayerMask',                []);
[varargin, valMask]                  = getPropertyValue(varargin, 'valMask',                  []);
[varargin, zone]                     = getPropertyValue(varargin, 'zone',                     1);
[varargin, ModeDependant]            = getPropertyValue(varargin, 'ModeDependant',            0);
[varargin, UseModel]                 = getPropertyValue(varargin, 'UseModel',                 0);
[varargin, flagMarcRoche]            = getPropertyValue(varargin, 'flagMarcRoche',            0);
[varargin, PreserveMeanValueIfModel] = getPropertyValue(varargin, 'PreserveMeanValueIfModel', 0); %#ok<ASGLU>

%% Calcul de l'intersection commune entre toutes les images intervenant dans le calcul

if length(b) >= 1
    for k=1:length(b)
        V{k} = b(k); %#ok<AGROW>
    end
    [~, ~, subx, suby] = intersectionImages(this, subx, suby, V{:}, 'SameSize');
    clear V
    
    if isempty(subx) || isempty(suby)
        str1 = 'Il n''y a pas d''intersection entre les images utilis�es pour cette compensation.';
        str2 = 'There is no intersection between the images used for this compensation.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
end

C = {};
for k1=1:length(bilan{1}(1).Support)
    Support = bilan{1}(1).Support{k1};
    switch Support
        case 'Axe'
            %             DataTypeConditions = bilan{1}(k1).DataTypeConditions(k1);
            DataTypeConditions = bilan{1}(1).DataTypeConditions{k1};
            if strcmp(DataTypeConditions, 'X')
                try
                    XTemp = repmat(this.x(subx), length(suby), 1);
                catch %#ok<CTCH>
                    XTemp = cl_memmapfile('Value', 0, 'Size', [length(suby), length(subx)]);
                    for k2=1:length(suby)
                        XTemp(k2,:) = this.x(subx);
                    end
                end
                C{end+1} = replace_Image(this, XTemp); %#ok
                C{end}.x = this.x(subx);
                C{end}.y = this.y(suby);
                C{end}.XLim = compute_XYLim(this.x(subx));
                C{end}.YLim = compute_XYLim(this.y(suby));
            elseif strcmp(DataTypeConditions, 'Y')
                try
                    XTemp = repmat(this.y(suby)', 1, length(subx));
                catch %#ok<CTCH>
                    XTemp = cl_memmapfile('Value', 0, 'Size', [length(suby), length(subx)]);
                    for k2=1:length(subx)
                        XTemp(:,k2) = this.y(suby);
                    end
                end
                C{end+1} = replace_Image(this, XTemp); %#ok
                C{end}.x         = this.x(subx);
                C{end}.y         = this.y(suby);
                C{end}.XLim      = compute_XYLim(this.x(subx));
                C{end}.YLim      = compute_XYLim(this.y(suby));
            end
            
        case 'Layer'
            DataType = cl_image.indDataType(bilan{1}(1).DataTypeConditions{k1});
            if strcmp(bilan{1}(1).DataTypeConditions{k1}, 'Mask')
                DT = cl_image.indDataType('Segmentation');
                if DT ~= 1
                    DataType = [DT DataType]; %#ok<AGROW>
                end
            end
            [flag, indLayer] = findOneLayerDataType(b, 1, DataType);
            if ~flag
                str1 = 'Il y a un truc qui va pas ici, contactez JMA.';
                str2 = 'There is a bug here, please report to JMA.';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                return
            end
            bb = b(indLayer);
            if ~isequal(this.x(subx), bb.x) || ~isequal(this.y(suby), bb.y)
                C{end+1} = extraction(bb, 'x', this.x(subx), 'y', this.y(suby)); %#ok
            else
                C{end+1} = bb; %#ok
            end
            
        case 'SignalVert'
            %             Val = get(this, bilan{1}(1).nomX);
            Val = get(this, bilan{1}(1).DataTypeConditions{k1}); % Modifi� par JMA le 03/04/2014
            Val = Val(:,:);
            if ~isa(this.Image(1), 'double')
                Val = cast(Val, 'single');
            end
            try
                XTemp = repmat(Val(suby), 1, length(subx));
            catch %#ok<CTCH>
                XTemp = cl_memmapfile('Value', 0, 'Size', [length(suby), length(subx)]);
                for k2=1:length(subx)
                    XTemp(:,k2) = Val(suby);
                end
            end
            C{end+1} = replace_Image(this, XTemp); %#ok
            C{end}.x         = this.x(subx);
            C{end}.y         = this.y(suby);
            C{end}.XLim      = compute_XYLim(this.x(subx));
            C{end}.YLim      = compute_XYLim(this.y(suby));
            C{end}.DataType  = cl_image.indDataType(bilan{1}(1).DataTypeConditions{k1});
            
        otherwise
            str1 = sprintf('Support "%s" pas encore �crit dans compensationCourbesStats.', Support);
            str2 = sprintf('Support "%s" not written yet in compensationCourbesStats.', Support);
            my_warndlg(Lang(str1,str2), 1);
    end
end

binsY = [];
if length(bilan{1}(1).Support) > 1
    for k=1:length(bilan{1})
        if isfield(bilan{1}(k), 'numVarCondition')
            binsY(k) = bilan{1}(k).numVarCondition; %#ok<AGROW>
        end
    end
end
flag = 1;

if isa(this.Image(1), 'double') || isa(this.Image(1), 'single')
    I = this.Image(suby, subx);
else
    I = single(this.Image(suby, subx));
end

if isempty(binsY)
    subNonNan = find(~isnan(bilan{1}(1).y));
    nbLigBlock = max(1, floor(500000/size(I,2)));
    nbLigBlock = max(nbLigBlock, 500);
    % V�rification utile uniquement en phase de developpement
    DataTypeCondition = bilan{1}(1).DataTypeConditions{1};
    strDataType = cl_image.strDataType{C{1}.DataType};
    if ~(strcmp(bilan{1}(1).nomX, 'X') || strcmp(bilan{1}(1).nomX, 'Y')) && ~strcmp(strDataType, DataTypeCondition)
        str = sprintf('The compensation curve has been made with a "%s layer, the layer which is given is a "%s", the process cannot continue', ...
            DataTypeCondition, strDataType);
        my_warndlg(str, 1);
        flag = 0;
        return
    end
    
    for k1=1:nbLigBlock:C{1}.nbRows
        subl = k1:min((k1-1+nbLigBlock), C{1}.nbRows);
        J = C{1}.Image(subl,:);
        if strcmp(TypeStats, 'Mean')
            x = bilan{1}(1).x(subNonNan);
            y = bilan{1}(1).y(subNonNan);
            x = [x(1)-eps(x(1)) x x(end)+eps(x(end))]; %#ok<AGROW>
            y = [y(1); y(:); y(end)];
            y = y';
            
            if UseModel
                try
                    compens = bilan{1}.model.compute('x', J);
                    if PreserveMeanValueIfModel
                        if isfield(bilan{1}, 'BiaisModel') && ~isempty(bilan{1}.BiaisModel)
                            compens = compens - bilan{1}.BiaisModel;
                        end
                    end
                catch % Cas o� il n'y a pas de mod�le : TODO : corriger cela en amont
                    compens = interp1(x, y, double(J), 'linear', 'extrap');
                end
            else
                compens = interp1(x, y, double(J), 'linear', 'extrap');
            end
        else
            x = bilan{1}(1).x(subNonNan);
            y = bilan{1}(1).ymedian(subNonNan);
            x = [x(1)-eps(x(1)) x x(end)+eps(x(end))]; %#ok<AGROW>
            y = [y(1) y y(end)]; %#ok<AGROW>
            compens = interp1(x, y, double(J), 'linear', 'extrap');
        end
        
        if ModeDependant && isfield(bilan{1}(1), 'Mode') && ~isempty(bilan{1}(1).Mode) %% Ceinture et bretelles, le premier suffit en principe
            Mode = C{1}.Sonar.PortMode_1(:,1);
            subMode = find(Mode(subl,1) == bilan{1}(1).Mode);
            switch TypeOperation
                case '-'
                    I(subl(subMode),:) = I(subl(subMode),:)  - compens(subMode,:);
                case '/'
                    I(subl(subMode),:) = I(subl(subMode),:) ./ compens(subMode,:);
                case '+'
                    I(subl(subMode),:) = I(subl(subMode),:)  + compens(subMode,:);
                case '*'
                    I(subl(subMode),:) = I(subl(subMode),:) .* compens(subMode,:);
            end
        else
            switch TypeOperation
                case '-'
                    I(subl,:) = I(subl,:)  - compens;
                case '/'
                    I(subl,:) = I(subl,:) ./ compens;
                case '+'
                    I(subl,:) = I(subl,:)  + compens;
                case '*'
                    I(subl,:) = I(subl,:) .* compens;
            end
        end
    end
else
    NY = length(binsY);
    
    switch bilan{1}(1).DataTypeConditions{2}
        case 'SonarFrequency'
            CentrageMagnetic = 0;
        otherwise
            CentrageMagnetic = 1;
    end
    
    YQ  = quantify(C{2}.Image, 'rangeIn', [binsY(1) binsY(end)], 'Step', mean(diff(binsY)), 'rangeOut', [binsY(1) binsY(end)], ...
        'CentrageMagnetic', CentrageMagnetic);
    YQ_MarcRoche = YQ;
    
    if ModeDependant && isfield(bilan{1}(1), 'Mode') && ~isempty(bilan{1}(1).Mode) %% Ceinture et bretelles, le premier suffit en principe
        if isempty(C{2}.Sonar.PortMode_1)
            DataType = cl_image.indDataType('Mode');
            [flag, indLayer] = findOneLayerDataType(b, 1, DataType);
            if ~flag
                str1 = 'Il y a un truc qui va pas ici, contactez JMA.';
                str2 = 'There is a bug here, please report to JMA.';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                return
            end
            bb = b(indLayer);
            if ~isequal(this.x(subx), bb.x) || ~isequal(this.y(suby), bb.y)
                Mode = extraction(bb, 'x', this.x(subx), 'y', this.y(suby));
            else
                Mode = bb;
            end
            Mode = Mode.Image;
            YQ(Mode ~= bilan{1}(1).Mode) = 0;
        else
            Mode = C{2}.Sonar.PortMode_1(:,1);
            if isfield(bilan{1}(1), 'NbSwaths') && ~isempty(bilan{1}(1).NbSwaths)
                if isfield(bilan{1}(1), 'SelectFM') && ~isempty(bilan{1}(1).SelectFM)
                    NbSwaths   = C{2}.Sonar.NbSwaths(:,1);
                    PresenceFM = C{2}.Sonar.PresenceFM(:,1) + 1;
                    subMode = find((Mode ~= bilan{1}(1).Mode) | (NbSwaths ~= bilan{1}(1).NbSwaths) | (PresenceFM ~= bilan{1}(1).SelectFM));
                    YQ(subMode,:) = 0; %#ok<FNDSB>
                else
                    NbSwaths = C{2}.Sonar.NbSwaths(:,1);
                    subMode = find((Mode ~= bilan{1}(1).Mode) | (NbSwaths ~= bilan{1}(1).NbSwaths));
                    YQ(subMode,:) = 0; %#ok<FNDSB>
                end

                %% Ajout JMA le 15/02/2022 pour donn�es EM2040 Marc Roche

                if isfield(bilan{1}(1), 'Mode2EM2040') && ~isempty(bilan{1}(1).Mode2EM2040)
                    Mode2 = get(bb, 'Mode2');
                    subMode = find(Mode2 ~= bilan{1}(1).Mode2EM2040);
                    YQ(subMode,:) = 0; %#ok<FNDSB>
                end
                if flagMarcRoche
                    if isfield(bilan{1}(1), 'Mode3EM2040') && ~isempty(bilan{1}(1).Mode3EM2040)
                        Mode3 = get(bb, 'Mode3');
                        subMode = find((Mode ~= bilan{1}(1).Mode) |  (Mode2 ~= bilan{1}(1).Mode2EM2040) | (Mode3 ~= bilan{1}(1).Mode3EM2040));

                        if isempty(subMode)
                            fprintf('O : %s  Mode1=%d  Mode2=%d  Mode3=%d  Curve name=%s\n', bb.InitialFileName, Mode(1), Mode2(1), Mode3(1), bilan{1}(1).nomZoneEtude);
                            pi;%JMA
                        else
                            fprintf('X : %s  Mode1=%d  Mode2=%d  Mode3=%d  Curve name=%s\n', bb.InitialFileName, Mode(1), Mode2(1), Mode3(1), bilan{1}(1).nomZoneEtude);
                            pi;%JMA
                        end

                        YQ = YQ_MarcRoche;
                        YQ(subMode,:) = 0;
                    else
                        fprintf('! : Pas de mode 3 dans  Curve name=%s\n',  bilan{1}(1).nomZoneEtude);
                    end
                end
            else
                subMode = find(Mode ~= bilan{1}(1).Mode);
                YQ(subMode,:) = 0; %#ok<FNDSB>
            end
        end
    end
    
    compens = NaN(size(I), 'single');
    C1Image = C{1}.Image(:,:);
    for k1=1:NY
        k = bilan{1}(k1).numVarCondition;
        
        if k == 0
            % ATTENTION : ici bug rencontr� si image de condition ne
            % commence pas � 1 : A INSPECTER
            %             my_warndlg('ATTENTION : ici peut-etre que �a va bugger', 0);
        end
        
        sub = find(YQ == k);
        if ~isempty(sub)
                            
%             if k == 1
%                 fprintf('O V�rification : %s  Mode1=%d  Mode2=%d  Mode3=%d  Curve name=%s\n', bb.InitialFileName, Mode(1), Mode2(1), Mode3(1), bilan{1}(1).nomZoneEtude);
%                 pi;%JMA
%             end 

            % Verification utile uniquement en phase de developpement
            subNonNan = find(~isnan(bilan{1}(k1).y));
            if ~isempty(subNonNan)
                DataTypeCondition = bilan{1}(k1).DataTypeConditions{1};
                strDataType = cl_image.strDataType{C{1}.DataType};
                
                if any(strcmp(strDataType, {'TxAngle'; 'BeamPointingAngle'})) ...
                        && any(strcmp(DataTypeCondition, {'TxAngle'; 'BeamPointingAngle'})) % Pour �viter confusion avec cette version de SSc
                    DataTypeCondition = strDataType;
                end
                
                if ~(strcmp(bilan{1}(1).nomX, 'X') || strcmp(bilan{1}(1).nomX, 'Y')) && ~strcmp(strDataType, DataTypeCondition)
                    str = sprintf('The compensation curve has been made with a "%s layer, the layer which is given is a "%s", the process cannot continue', ...
                        DataTypeCondition, strDataType);
                    my_warndlg(str, 1);
                    flag = 0;
                    return
                end
                if strcmp(TypeStats, 'Mean')
                    if length(subNonNan) == 1
%                         if UseModel
%                             % TODO : � faire
                            compens(sub) = bilan{1}(k1).y(subNonNan);
%                         else
%                             compens(sub) = bilan{1}(k1).y(subNonNan);
%                         end
                    else
                        x = bilan{1}(k1).x(subNonNan);
                        y = bilan{1}(k1).y(subNonNan);
                        x = x(:);
                        y = y(:);
                        x = x';
                        y = y';
                        
                        % Modif JMA le 15/01/2012 pour compensation en
                        % fonction des fr�quences du Reson 7150
                        % TODO : il faudrait certainement g�n�raliser �a �
                        % l'ensemble des interp1 ????
                        x = [x(1)-eps(x(1)) x x(end)+eps(x(end))]; %#ok<AGROW>
                        y = [y(1) y y(end)]; %#ok<AGROW>
                        % Fin modif JMA
                        
                        if UseModel && isfield(bilan{1}(k1), 'model') && ~isempty(bilan{1}(k1).model)
                            % compens(sub) = compute(bilan{1}(k1).model, 'x', C1Image(sub)); % Comment� mais valable quand m�me
                            compens(sub) = bilan{1}(k1).model.compute('x', C1Image(sub));
                        else
                            compens(sub) = interp1(x, y, C1Image(sub), 'linear', 'extrap');
                        end
                    end
                else
                    if length(subNonNan) == 1
                        compens(sub) = bilan{1}(k1).ymedian(subNonNan);
                    else
                        % Modif JMA le 15/01/2012 pour compensation en
                        % fonction des fr�quences du Reson 7150
                        % TODO : il faudrait certainement g�n�raliser �a �
                        % l'ensemble des interp1 ????
                        x = bilan{1}(k1).x(subNonNan);
                        y = bilan{1}(k1).ymedian(subNonNan)';
                        x = [x(1)-eps(x(1)) x x(end)+eps(x(end))]; %#ok<AGROW>
                        y = [y(1) y y(end)]; %#ok<AGROW>
                        
                        if UseModel
                            % TODO : non v�rifi�
                            compens = compute(bilan{1}(k1).model, 'x', C1Image(sub));
                        else
                            compens = interp1(x, y, C1Image(sub), 'linear', 'extrap');
                        end
                        % Fin modif JMA
                        
                        %                         compens(sub) = interp1(bilan{1}(k1).x(subNonNan), bilan{1}(k1).ymedian(subNonNan)', C1Image(sub), 'linear', 'extrap');
                    end
                end
                switch TypeOperation
                    % Modifi� le 08/11/2010
                    case '-'
                        I(sub) = I(sub) - compens(sub);
                    case '/'
                        I(sub) = I(sub) ./ compens(sub);
                    case '+'
                        I(sub) = I(sub) + compens(sub);
                    case '*'
                        I(sub) = I(sub) .* compens(sub);
                end
            end
        else
            %             disp('hjk')
        end
    end
    clear YQ
end
clear C sub compens

%% Cas o� la compensation se fait au travers d'un masque

if isempty(LayerMask)
    J = I;
else
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
    if zone == 1
        subI = false(size(Masque));
        for ik=1:length(valMask)
            sub = (Masque == valMask(ik));
            subI(sub) = true;
        end
    else
        subI = true(size(Masque));
        for ik=1:length(valMask)
            sub = (Masque == valMask(ik));
            subI(sub) = false;
        end
    end
    J = this.Image(suby, subx);
    J(subI) = I(subI);
end
clear I subI sub Masque

%% Cr�ation de l'instance

this = replace_Image(this, J);
clear J
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'Compensation');
this.Name = strrep(this.Name, 'Compensation_Compensation', 'Compensation');
