function this = compensation_WaterColumn(this, TransmitSectorNumber, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

%% Appel de la fonction de filtrage

I = this.Image(suby,subx,:);
I = compensation_WaterColumnSectors(I, TransmitSectorNumber, this.Unit);

%% Transfert de l'image dans l'instance

this = replace_Image(this, I);

%% Mise � jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

if ~NoStats
    this = compute_stats(this);
end

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'Comp');
