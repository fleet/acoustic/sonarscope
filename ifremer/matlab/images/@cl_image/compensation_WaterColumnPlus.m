function [flag, a] = compensation_WaterColumnPlus(this)

a = [];

identReflec = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', identReflec, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

if isfield(this.Sonar.SampleBeamData, 'TransmitSectorNumber') % TxBeamIndexSwath
    TransmitSectorNumber = this.Sonar.SampleBeamData.TransmitSectorNumber;
    a = compensation_WaterColumn(this, TransmitSectorNumber);
else
%     flag = 0;
%     a = [];
	a = compensation_WaterColumn(this, []);
end
