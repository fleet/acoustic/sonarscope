% Compl�te une image A par une image B
%
% Syntax
%  b = abs(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%   b : Image "bouche trous"
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = completeAWithB(this, b, varargin)

nbImages = length(this);
for i=1:nbImages
    if length(b) == 1
        [flag, c] = unitaire_completeAWithB(this(i), b, varargin{:});
    else
        [flag, c] = unitaire_completeAWithB(this(i), b(i), varargin{:});
    end
    if ~flag
        return
    end
    this(i) = c;
end


function [flag, this] = unitaire_completeAWithB(this, b, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = false;

%% Intersection des images

[XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, 1:this.nbColumns, 1:this.nbRows, b, 'SameSize');
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Calcul

nbSlides = this.nbSlides;
a = this.Image(suby,subx,:);
c = zeros([length(suby) length(subx) nbSlides], class(a));
if nbSlides == 1
    for iLig=1:length(suby)
        x = this.Image(suby(iLig), subx);
        y = b.Image(subyb(iLig), subxb);
        if isnan(this.ValNaN)
            sub = ~isnan(x);
        else
            sub = (x ~= this.ValNaN);
        end
        x(sub) = y(sub);
        c(iLig,:) = x;
    end
else
    for iLig=1:length(suby)
        x3 = this.Image(suby(iLig), subx, :);
        if isnan(this.ValNaN)
            sub = isnan(sum(x,3));
        else
            sub = sum(x3 == this.ValNaN,3) == nbSlides;
        end
        
        for iCan = 1:nbSlides
            x = x3(1, :, iCan);
            y = b.Image(subyb(iLig), subxb, iCan);
            x(sub) = y(sub);
            c(iLig,:,iCan) = x;
        end
    end
end

this = replace_Image(this, c);

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Union');

flag = true;
