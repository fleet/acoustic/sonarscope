function [flag, RGB] = composeColoredImageFrom2Frequencies(Image1, Image2, ImageName, varargin)

[varargin, Freq] = getPropertyValue(varargin, 'Freq', []); %#ok<ASGLU>

NameImage1 = extract_ImageName(Image1);
NameImage2 = extract_ImageName(Image2);

%% Extraction de l'image2 sur le m�me cadre g�ographique que Image1

Image2 = extraction(Image2, 'x', Image1.x, 'y', Image1.y, 'ForceExtraction');

%% Transformation des images d'intensit� en images index�es

[Image1Ind, flag] = Intensity2Indexed(Image1, 'CLim', Image1.StatValues.Quant_25_75);
if ~flag
    return
end
[Image2Ind, flag] = Intensity2Indexed(Image2, 'CLim', Image2.StatValues.Quant_25_75);
if ~flag
    return
end

%% Cr�ation du troisi�me canal

I3Ind = uint8(0.5 * single(Image1Ind.Image) + 0.5 * single(Image2Ind.Image));

RGB = Image1Ind.Image;
RGB(:,:,3) = Image2Ind.Image;
RGB(:,:,2) = I3Ind;

%% Output image

RGB = inherit(Image1, RGB, 'ValNaN', 255, 'ImageType', 2, 'Unit', ' ', 'ColormapIndex', 2);
ImageName = code_ImageName(RGB, 'Name', ImageName);
RGB.Name = ImageName;
if isempty(Freq)
    RGB.Comments = sprintf('Red=%s, Geen=%s, Blue=0.5*Red+0.5*Green', NameImage1, NameImage2);
else
    RGB.Comments = sprintf('Red=%dkHz, Geen=%dkHz, Blue=0.5*Red+0.5*Green', Freq(1), Freq(2));
end
%     SonarScope(RGB)