function [flag, this] = computeCrossStatistics(this, indImage, ...
    Selection, nomCourbe, Comment, varargin)

[varargin, subx]         = getPropertyValue(varargin, 'subx',         []);
[varargin, suby]         = getPropertyValue(varargin, 'suby',         []);
[varargin, bins]         = getPropertyValue(varargin, 'bins',         []);
[varargin, CourbeBias]   = getPropertyValue(varargin, 'CourbeBias',   0);
[varargin, Coul]         = getPropertyValue(varargin, 'Coul',         []);
[varargin, nbPMin]       = getPropertyValue(varargin, 'nbPMin',       10);
[varargin, SelectMode]   = getPropertyValue(varargin, 'SelectMode',   []);
[varargin, SelectSwath]  = getPropertyValue(varargin, 'SelectSwath',  []);
[varargin, SelectFM]     = getPropertyValue(varargin, 'SelectFM',     []);
[varargin, MeanCompType] = getPropertyValue(varargin, 'MeanCompType', 1);
[varargin, valMask]      = getPropertyValue(varargin, 'valMask',      1);
[varargin, LayerMask]    = getPropertyValue(varargin, 'LayerMask',    []);
[varargin, fig]          = getPropertyValue(varargin, 'fig',          220994); %#ok<ASGLU>

% [varargin, stepBins] = getPropertyValue(varargin, 'stepBins', []);
% [varargin, Sampling] = getPropertyValue(varargin, 'Sampling', []);
% [varargin, EditBias] = getPropertyValue(varargin, 'EditBias', 0);
% [varargin, TagSup] = getPropertyValue(varargin, 'TagSup', []);

flag = 0;

identCurve = length(this(indImage).CourbesStatistiques);
nbCurvesBefore = length(this(indImage).CourbesStatistiques);
for k=1:length(valMask)
    Tag          = this(indImage).Name;
    GeometryType = this(indImage).GeometryType;
    strGeometryType = cl_image.strGeometryType;
    
    a = courbesStats(this(indImage), Tag, ...
        nomCourbe{k}, Comment{k}, [], ...
        this(:), Selection, ...
        'LayerMask', LayerMask, 'valMask', valMask(k), ...
        'bins', bins, 'subx', subx, 'suby', suby, ...
        'CourbeBias', CourbeBias, 'EditBias', 1, ...
        'GeometryType', strGeometryType{GeometryType}, ...
        'Sampling', 1, 'Coul', Coul, 'nbPMin', nbPMin, ...
        'SelectMode', SelectMode, 'SelectSwath', SelectSwath, ...
        'SelectFM', SelectFM, 'MeanCompType', MeanCompType, ...
        'fig', fig);
    if isempty(a)
        return
    end
    this(indImage) = a;
    
    % Affichage de la courbe brute
    identCurve = identCurve + 1;
    figSector(k) = plotCourbesStats(this(indImage), 'sub', identCurve, 'Stats', 0); %#ok<AGROW>
end

%% Nettoyage des courbes

n = length(this(indImage).CourbesStatistiques);
sub = (nbCurvesBefore+1):n;
[flag, CourbesStatistiquesOut] = CourbesStatsClean(this(indImage).CourbesStatistiques(sub), 'Stats', 0, 'NoPlot');
if ~flag
    return
end

%% Sauvegarde de la courbe nettoy�e dans l'image

this(indImage).CourbesStatistiques = [this(indImage).CourbesStatistiques(:); CourbesStatistiquesOut(:)];

%% Visualisation de la courbe nettoy�e

n = length(this(indImage).CourbesStatistiques);
sub = (n-length(CourbesStatistiquesOut)+1):n;
plotCourbesStats(this(indImage), 'sub', sub, 'Stats', 0);

%% Suppression des figure des courbes brutes

for k=1:length(valMask)
    my_close(figSector(k))
end
