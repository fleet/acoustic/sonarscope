function this = computeFilter2(this, subx, suby, h, window, LayerMask, valMask, AppendName, Parameters)

% TODO : tester ceci
%{
%% Convert in intensity image if necessary

switch this.ImageType
    case 2
        this = RGB2Intensity(this);
    case 3
        this = Indexed2Intensity(this);
end
%}

%% Extract the image if subsampled

Titre = this.Name;
this = extraction(this, 'subx', subx, 'suby', suby, 'NoStats');
subNaN = isnan(this);

%% Extract the mask if defined

if isempty(LayerMask)
    subNonMasque = [];
else
    subNonMasque = get_Unmasked(LayerMask, valMask, this.x, this.y);
end

%% Interpolate the image in order not to propagate NaN
 
this = WinFillNaN(this, 'window', window, 'NoStats');
this.Name = Titre;

%% Algorithm

for k=this.nbSlides:-1:1
    I = singleUnlessDouble(this.Image(:, :, k), this.ValNaN);

    if ~isempty(subNonMasque)
        K = I(subNonMasque);
    end

    I = windowEnlarge(I, window, NaN);

    I = filter2(h, I, 'valid'); % valid rend une matrice r�duite de la taille de la fen�tre donc pas besoin de windowEnlargeOff

    if ~isempty(subNonMasque)
        I(subNonMasque) = K;
    end
    
    I(subNaN) = NaN; % Set the NaN pixels that existed in the input image
    Image(:,:,k) = I;
end

%% Output image

this = inherit(this, Image, 'AppendName', AppendName, 'ValNaN', NaN, 'Parameters', Parameters);
