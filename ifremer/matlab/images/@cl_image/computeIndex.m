function subFile = computeIndex(this, varargin)
        
SZ = this.size;
if (length(SZ) == 3) && (SZ(3) == 1)
    SZ = SZ(1:2);
end
if length(SZ) == 3
    nbSlides = SZ(3);
else
    nbSlides = 1;
end

% ------------------------------------------------------------------
% varargin = process_sub(this, varargin{:});
      
if length(SZ) == 2
    sub = cell(1,2);
else
    sub = cell(1,3);
end

switch nargin
    case 2
        switch length(SZ)
            case {2 3}
                sub = [];
                if ischar(varargin{1}) && isequal(varargin{1}, ':')
                    sub{1} = 1:prod(SZ);
                else
                    sub{1} = varargin{1};
                end
%                 n = [length(sub{1}) 1];
                
            otherwise
                my_warndlg('cl_memmapfile Erreur 3', 1);
        end
        
    case 3
        if ischar(varargin{1}) && isequal(varargin{1}, ':')
            sub{1} = 1:SZ(1);
        else
            sub{1} = varargin{1};
        end

        if ischar(varargin{2}) && isequal(varargin{2}, ':')
            sub{2} = 1:SZ(2);
        else
            sub{2} = varargin{2};
        end
        
        n1 = length(sub{1});
        n2 = length(sub{2});
        sub{1} = repmat(sub{1}, 1, n2);
        sub{2} = repmat(sub{2}, n1, 1);
        
%         n = [n1 n2];
        
    case 4
        if ischar(varargin{1}) && isequal(varargin{1}, ':')
            sub{1} = 1:SZ(1);
        else
            sub{1} = varargin{1};
        end

        if ischar(varargin{2}) && isequal(varargin{2}, ':')
            sub{2} = 1:SZ(2);
        else
            sub{2} = varargin{2};
        end
        
        n1 = length(sub{1});
        n2 = length(sub{2});

        if length(SZ) == 2
            sub{1} = repmat(sub{1}', [n2 1]);
            sub{2} = repmat(sub{2}, [n1 1]);
            sub{1} = reshape(sub{1}, [n1 n2]);
            sub{2} = reshape(sub{2}, [n1 n2]);
%             n = [n1 n2];
            
        elseif length(SZ) == 3
            if ischar(varargin{3}) && isequal(varargin{3}, ':')
                sub{3} = 1:SZ(3);
            else
                sub{3} = varargin{3};
            end
            
            n3 = length(sub{3});
            
            sub{1} = repmat(sub{1}', [n2 n3]);
            sub{2} = repmat(sub{2}, [n1 n3]);
            sub{1} = reshape(sub{1}, [n1 n2 n3]);
            sub{2} = reshape(sub{2}, [n1 n2 n3]);
            sub{3} = repmat(sub{3}, [n1*n2 1]);
            sub{3} = reshape(sub{3}, [n1 n2 n3]);
            
%             n = [n1 n2 n3];
        else
            my_warndlg('cl_memmapfile : Errer 9', 1);
        end

    otherwise
        my_warndlg('cl_memmapfile Erreur 4', 1);
end

% -------------------------------------------------------------------------
% [iBlock, Col] = ind_ErMapper2ind_Matlab(this, varargin{:});
switch length(sub)
    case 2
        switch length(SZ)
            case 2
               I1 = ind2sub(SZ, sub{1});
                Blocks = I1;
                
            case 3
                nbSlides = SZ(3);
                [I1,I2,I3] = ind2sub(SZ, sub{1});
                Blocks = (I1-1)*nbSlides + I3;
                
            otherwise
            my_warndlg('cl_memmapfile Erreur 1', 1);
        end
        
    case 3
        I1 = sub{1};
%         I2 = sub{2};
        Blocks = I1;
       
    case 4
        I1 = sub{1};
%         I2 = sub{2};
        I3 = sub{3};
        nbSlides = SZ(3);
        Blocks = (I1-1)*nbSlides + I3;

    otherwise
        my_warndlg('cl_memmapfile Erreur 2', 1);
end


% -------------------------------------------------------------------------
clear sub


% -------------------------------------------------------------------------
% subFile = get_sub_FileErMapper(this,iBlock, Col);


[nbBlocks, lengthBlock] = getNbBlocks(this);
SZ = [nbBlocks, lengthBlock];
% Col = Blocks;

switch nbSlides
    case 1
        SZ(1:2) = fliplr(SZ(1:2));
        subFile = sub2ind(SZ, Blocks, reshape(iBlock, size(Blocks)));
        
    otherwise
        SZ(1:2) = fliplr(SZ(1:2));
        subFile = sub2ind(SZ, Blocks, reshape(iBlock, size(Blocks)));
end

% -------------------------------------------------------------------------
clear iBlock Blocks
