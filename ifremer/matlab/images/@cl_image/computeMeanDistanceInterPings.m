function [flag, resol] = computeMeanDistanceInterPings(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>

if isempty(sub)
    sub = 1:this.nbRows;
end

FishLatitude  = get(this, 'SonarFishLatitude');
FishLongitude = get(this, 'SonarFishLongitude');
[xFish, yFish, flag] = latlon2xy(this.Carto, FishLatitude(sub), FishLongitude(sub));
if flag
    sub = ~isnan(xFish) & ~isnan(yFish);
    N = sum(sub);
    D = sqrt(diff(xFish(sub)).^2 + diff(yFish(sub)).^2);
    resol = sum(D) / N;
    resol = round(resol, 2, 'significant');
else
    resol = [];
end
