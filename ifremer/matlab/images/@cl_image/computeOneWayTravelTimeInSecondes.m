function [flag, TempsSimple] = computeOneWayTravelTimeInSecondes(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = 1;

[~, SonarName] = get_SonarDescription(this);

switch SonarName % TODO : comment distinguer EM2040S et EM2040S ?
    case {'EM3002S'; 'EM1002'; 'EM710'; 'EM122'; 'EM2040'}
        subBab = subx;
        subTri = [];
    case 'EM3002D'
        subBab = find(subx <= (this.nbColumns/2));
        subTri = find(subx >  (this.nbColumns/2));
    case 'RESON7111'
        subBab = subx;
        subTri = [];
end

SamplingRate = this.Sonar.SampleFrequency(:,:);

identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
identRayPathLength             = cl_image.indDataType('RayPathLength'); % RESON7111
switch this.DataType
    case identTwoWayTravelTimeInSeconds
        TempsSimple = this.Image(suby, :) / 2;
    case identRayPathSampleNb
        if ~isempty(subTri)
            TempsSimple(:,subTri) = bsxfun(@rdivide, this.Image(suby, subTri), 2 * SamplingRate(suby, 2));
        end
        TempsSimple(:,subBab) = bsxfun(@rdivide, this.Image(suby, subBab) , 2 * SamplingRate(suby, 1));
    case identRayPathLength
        TempsSimple = this.Image(suby, :) / 1500;
    otherwise
        my_warndlg('computeOneWayTravelTimeInSecondes :  DataType not recognized', 1);
end

% if this.DataType == identTwoWayTravelTimeInSeconds
%     TempsSimple = this.Image(suby, :) / 2;
% else % RayPathSampleNb
%     if ~isempty(subTri)
%         TempsSimple(:,subTri) = bsxfun(@rdivide, this.Image(suby, subTri), 2 * SamplingRate(suby, 2));
%     end
% 	TempsSimple(:,subBab) = bsxfun(@rdivide, this.Image(suby, subBab) , 2 * SamplingRate(suby, 1));
% end

if all(isnan(TempsSimple))
    flag = 0;
end
