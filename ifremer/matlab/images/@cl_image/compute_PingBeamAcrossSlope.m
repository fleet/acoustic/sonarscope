function [flag, AcrossSlope] = compute_PingBeamAcrossSlope(Depth, AcrossDist, varargin)

[subx, suby, varargin] = getSubxSuby(Depth, varargin); %#ok<ASGLU>

AcrossSlope = [];

%% Controls

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(Depth, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', identBathymetry);
if ~flag
    return
end

identAcrossDist = cl_image.indDataType('AcrossDist');
flag = testSignature(AcrossDist, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', identAcrossDist);
if ~flag
    return
end

%% Algorithm

Pente = atand(diff(Depth.Image(:,:), 1, 2) ./ diff(AcrossDist.Image(:,:), 1, 2));
Pente(:,end+1) = NaN;

%% Output image

DataType = cl_image.indDataType('SlopeAcross');
AcrossSlope = inherit(Depth, Pente, 'subx', subx, 'suby', suby, ...
    'ColormapIndex', 3, 'Unit', 'deg', 'DataType', DataType);
