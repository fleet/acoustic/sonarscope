% TODO : m�thore r�alis�e pour plot de naqv et signal RDF : doit �tre
% consolid� avant utilisation intensive

function [flag, Signal, Unit, Colors, InterpMethod] = compute_signal(this, nomFic, nomSignal, varargin)

[varargin, ConstanteDeTemps] = getPropertyValue(varargin, 'ConstanteDeTemps', 5*60);
[varargin, Type]             = getPropertyValue(varargin, 'Type',             'Normal'); %#ok<ASGLU>

nbColors     = 64;
Colors       = jet(nbColors);
InterpMethod = 'linear';

SonarTime = get(this, 'Time');
if isempty(SonarTime)
    flag   = 0;
    Signal = [];
    Unit   = [];
    Colors = [];
    return
end

Longitude = get(this, 'FishLongitude');
Latitude  = get(this, 'FishLatitude');

switch nomSignal
    case 'Time'
        Signal = SonarTime.timeMat;
        Unit = 'ms';
        
    case 'Height' % ATTENTION : valable uniquement pour Height donn� en nombre de sample avec annomalie / 2 RDF
        Signal = get(this, 'Height');
        Signal = computeHeight(this, Signal);
        Unit = 'm';
        
    case 'Drift'
        Signal = computeDrift(this, Latitude, Longitude);
        Unit = 'deg';
        
    case 'InterPingDistance'
        [~, ~, Signal] = calCapFromLatLon(Latitude(:,1), Longitude(:,1));
        Signal = repmat(Signal, 1, size(Latitude,2));
        Unit = 'm';
        
    case 'Heading'
        Signal = get(this, nomSignal);
        Unit = 'deg';
        
    case 'Pitch'
        Signal = get(this, 'Pitch');
        Unit = 'deg';
        
    case 'Roll'
        Signal = get(this, 'Roll');
        Unit = 'deg';
        
    case 'SurfaceSoundSpeed'
        Signal = get(this, 'SurfaceSoundSpeed');
        Unit = 'm/s';
        
    case 'Heave'
        Signal = get(this, 'Heave');
        Unit = 'm';
        
    case 'PingCounter'
        Signal = get(this, 'PingCounter');
        Unit = 'Ping';
        
    case 'RTK-Type' % TODO : supprimer cela et g�rer le calcul dans la classe cl_rdf
        [flag, DataHeight] = SSc_ReadDatagrams(nomFic, 'Ssc_Height');
        if ~flag
            flag   = 0;
            Signal = [];
            Unit   = [];
            Colors = [];
            return
        end
        Signal = DataHeight.HeightType;
        Unit = '';
        InterpMethod = [];
        
    case 'RTK-Height' % TODO : supprimer cela et g�rer le calcul dans la classe cl_rdf
        [flag, DataHeight] = SSc_ReadDatagrams(nomFic, 'Ssc_Height');
        if ~flag
            flag   = 0;
            Signal = [];
            Unit   = [];
            Colors = [];
            return
        end
        Signal = DataHeight.Height;
        Unit = 'm';
        InterpMethod = [];
        
    case 'RTK-Tide' % TODO : supprimer cela et g�rer le calcul dans la classe cl_rdf
        [flag, DataHeight] = SSc_ReadDatagrams(nomFic, 'Ssc_Height');
        if ~flag
            flag   = 0;
            Signal = [];
            Unit   = [];
            Colors = [];
            return
        end
        Signal = DataHeight.Tide;
        Unit = 'm';
        InterpMethod = [];
 
    otherwise
        Signal = get(this, nomSignal);
        Unit = '?';
end

switch Type
    case 'Mean'
        Signal = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
        
    case 'Std'
        [~, Signal] = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
end

if strcmp(nomSignal, 'Heading') && ~strcmp(Type, 'Std')
    Colors = hsv(nbColors);
end

flag = 1;


%{
'range'    'speedFish'    'speedSound'  'altitude'    'temperature'    'speed'    'shipHeading'
'magneticVariation'    'tvgPage'    'auxPitch'    'auxRoll'    'auxDepth'    'auxAlt'    'cableOut'
'fseconds'    'altimeter'    'sampleFreq'
'shieveXoff'    'shieveYoff'    'shieveZoff'    'GPSheight'    'rawDataConfig'
%}

function Signal = computeDrift(this, Latitude, Longitude)
Signal = get(this, 'Heading');
HeadingNav = calCapFromLatLon(Latitude(:,1), Longitude(:,1));
Signal = Signal - repmat(HeadingNav,1,2);
sub = (Signal > 180);
Signal(sub) = Signal(sub) - 360;
sub = (Signal < - 180);
Signal(sub) = Signal(sub) + 360;


function [Signal, Unit] = computeHeight(this, Signal)
SurfaceSoundSpeed = get(this, 'SurfaceSoundSpeed');
SampleFrequency   = get(this, 'SampleFrequency');
Signal = (SurfaceSoundSpeed .* Signal) ./ (2 * SampleFrequency); % TODO : facteur d'�chelle
Signal = Signal / 2; % TODO : pourquoi suis-je oblig� de faire cette division ?
Unit = 'm';
