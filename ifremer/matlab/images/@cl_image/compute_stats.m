% Compute the statistics of an image
%
% Syntax
%   a = compute_stats(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Sampling : Step over the pixels in order to accelerate the computation.
%              The default value depends upon the size of the image (see
%              stats and histo1D)
%   bins     : Bins used to compute the histogram. The defaut array is composed
%              of 100 values equaly distributed from the min value to the max value
%
% Output Arguments
%   a : The updated instance(s)
%
% Remarks : compute_stats is automatically called when creating an instance
%           of cl_image. It can be used only if o,e wants to change the
%           'Sampling' or give our own 'bins'
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%     StatValues = a.StatValues
%   a = compute_stats(a, 'Sampling', 2);
%     StatValues = a.StatValues
%
% See also stats histo1D Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = compute_stats(this, varargin)

N = length(this);
hw = create_waitbar(waitbarMsg('compute_stats'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, this(k)] = compute_stats_unit(this(k), varargin{:});
    if ~flag
        return
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, this] = compute_stats_unit(this, varargin)

[varargin, bins]     = getPropertyValue(varargin, 'bins',     []);
[varargin, Sampling] = getPropertyValue(varargin, 'Sampling', []); %#ok<ASGLU> % Pas d'echantillonnage impos� de la donn�e

%% Calcul des stats

flagFFT = (this.SpectralStatus == 2);
resultats = stats(this.Image, 'ValNaN', this.ValNaN, 'flagFFT', flagFFT, 'Sampling', Sampling);

this.StatValues = resultats;

if isempty(bins)
    Nx = 100;
    miny = this.StatValues.Min;
    if isempty(miny) || isnan(miny)  % Rajout� le 21/01/2015 pour fichiers ombr�s (export Google-Earth)
        N = 0;
        x = [];
    else
        maxy = this.StatValues.Max;
        binwidth = (maxy - miny) ./ Nx;
        xx = miny + binwidth*(0:Nx);
        xx(length(xx)) = maxy;
        bins = xx(1:length(xx)-1) + binwidth/2;
        [N, x] = histo1D(this.Image, bins, 'ValNaN', this.ValNaN, 'Sampling', Sampling);
    end
else
    [N, x] = histo1D(this.Image, bins, 'ValNaN', this.ValNaN, 'Sampling', Sampling);
end

this.HistoValues         = N;
this.HistoCentralClasses = x;

flag = 1;
