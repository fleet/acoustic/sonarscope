% Display the contour lines of the image 
%
% Syntax
%   contour(a, ...) or a.contour(...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx          : subsampling in X
%   suby          : subsampling in Y
%   ContourValues : Contour lines
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   a = import_CaraibesGrid(cl_image, nomFic);
%   imagesc(a);
%   a.ContourValues
%   contour(a);
%   a.ContourValues = -4500:50:-500;
%   contour(a);
%   contour(a, 'ContourValues', -5000:500:0);
%
% Author : JMA
% See also cl_image Authors

function contour(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, ContourValues] = getPropertyValue(varargin, 'ContourValues', this.ContourValues); %#ok<ASGLU>

I = double(this.Image(suby, subx));
x = this.x(subx);
y = this.y(suby);

FigUtils.createSScFigure;
contour(x, y, I, ContourValues);
colorbar; colormap(jet(256));

switch this.GeometryType
    case 2  % Metrique
        axis equal;
    case 3  % Geographique
        axisGeo;
end

if this.YDir == 1
    set(gca, 'YDir', 'normal');
else
    set(gca, 'YDir', 'reverse');
end

if this.XDir == 1 
    set(gca, 'XDir', 'normal');
else
    set(gca, 'XDir', 'reverse');
end