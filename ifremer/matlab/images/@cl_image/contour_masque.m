% Contourage pixelis� d'un masque
%
% Syntax
%  b = contour_masque(a)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%   a =
%   imagesc(a);
%
%   b = contour_masque(a);
%   imagesc(b);
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = contour_masque(this, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_contour_masque(this(i), varargin{:});
end


function this = unitaire_contour_masque(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

se  = strel('disk', 2);
I   = this.Image(suby,subx,:);
ero = imerode(I, se);
I   = imdilate(I, se);
I = I - ero;
this = replace_Image(this, I);

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this = update_Name(this, 'Append', 'ContourMasque');
