% Rehaussement de contraste en passant soit les valeurs soit les quantiles
%
% Syntax
%   b = contraste(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   CLim : Limite basse et haute de la table de couleur
%   %    : Quantiles bas et haut (1% 99%par defaut)
%
% Output Arguments
%   b : Instance de cl_image contenant l'image rehaussee
%
% Examples
%   imagesc(a)
%   b = contraste(a)
%   imagesc(b)
%   b = contraste(a, '%', [1 97])
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = contraste(this, varargin)

for k=1:length(this)
    this(k) = contraste_unitaire(this(k), varargin{:});
end


function this = contraste_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, CLim]    = getPropertyValue(varargin, 'CLim', []);
[varargin, Percent] = getPropertyValue(varargin, '%',    []); %#ok<ASGLU>

if isempty(CLim)
    if isempty(Percent)
        Percent = [1 99];
    end
    
    if length(Percent) == 1
        Percent(2) = 100 - Percent(1);
    end
    
    %% Calcul des statistiques
    
    StatValues = stats(this.Image(suby, subx), 'Seuil', Percent);
    
    %% Definition de CLim
    
    CLim = StatValues.Quant_x;
else
    if length(CLim) ~= 2
        my_warndlg('CLim must have 2 values please', 0);
        return
    end
end

this.CLim = CLim;
