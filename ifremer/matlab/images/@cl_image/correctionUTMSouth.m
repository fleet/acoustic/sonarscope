function this = correctionUTMSouth(this, Carto)

if strcmp(get_ProjectionName(Carto), 'UTM') && strcmp(get(Carto, 'Projection.UTM.Hemisphere'), 'S')
    Y0 = get(Carto, 'Projection.UTM.Y0');
    y = this.y;
    if (median(y) > 0) && (Y0 == 0)
        y = y - 10000000;
        this.y = centrage_magnetique(y);
    end
end