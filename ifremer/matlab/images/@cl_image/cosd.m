% Cosine of angles in deg
%
% Syntax
%   b = cosd(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(linspace(-180, 180, 200), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'deg');
%   imagesc(a);
%
%   b = cosd(a);
%   imagesc(b);
%
% See also cl_image/acos cl_image/cosd Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = cosd(this, varargin)
that = process_function_type1(this, @cosd, 'Unit', [], 'expectedUnit', 'deg', varargin{:});
