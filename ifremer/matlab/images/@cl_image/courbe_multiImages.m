function [flag, this] = courbe_multiImages(Images, indImage, indLayers, listeprofilsAngle, Bins1, listeprofils2, Bins2, ...
    nomCourbe, varargin)

this = Images(indImage);

[varargin, Mask] = getPropertyValue(varargin, 'Mask', []);

if isempty(Mask) || isempty(Mask{1})
    indLayerMask = [];
    valMask      = [];
    [flag, this] = courbe_multiImages_unitaire(this, Images, indLayers, listeprofilsAngle, Bins1, listeprofils2, Bins2, nomCourbe, ...
        indLayerMask, valMask, varargin{:});
else
    indLayerMask = Mask{1};
    valMask      = Mask{2};
    for k=1:length(valMask)
        Images(indLayerMask).RegionOfInterest = Images(indLayerMask).RegionOfInterest;
        if isempty(Images(indLayerMask).RegionOfInterest)
            %             str1 = sprintf('L''image "%s" n''a pas de r�gion d''int�r�t.', Images(indLayerMask).Name);
            %             str2 = sprintf('No ROI defined for "%s".', Images(indLayerMask).Name);
            %             my_warndlg(Lang(str1,str2), 1);
            %             flag = 0;
            %             return
            %         end
            nomCourbeValMask = [nomCourbe ' : ' num2str(valMask(k))];
        else
            try
                nomCourbeValMask = [nomCourbe ' : ' Images(indLayerMask).RegionOfInterest.nomCourbe{valMask(k)}];
            catch % TODO : retrouver comment on remet les ROI au gout du jour : y'a une fonction qui fait �a.
                if isfield(Images(indLayerMask).RegionOfInterest(valMask(k)), 'nomCourbe')
                    nomCourbeValMask = [nomCourbe ' : ' Images(indLayerMask).RegionOfInterest(valMask(k)).nomCourbe];
                else
                    nomCourbeValMask = [nomCourbe ' : ' Images(indLayerMask).RegionOfInterest(valMask(k)).Name]; % Rajout� par JMA le 02/12/2015
                end
            end
        end
        [flag, this] = courbe_multiImages_unitaire(this, Images, indLayers, listeprofilsAngle, Bins1, listeprofils2, Bins2, nomCourbeValMask, ...
            indLayerMask, valMask(k), varargin{:});
    end
end

function [flag, this] = courbe_multiImages_unitaire(this, Images, indLayers, listeprofilsAngle, Bins1, listeprofils2, Bins2, nomCourbe, ...
    indLayerMask, valMask, varargin)

nbImages = length(indLayers);

GeometryType = Images(indLayers(1)).GeometryType;

% if isempty(indLayerMask)
%     M = [];
%     XLim = [];
%     YLim = [];
% else
% %     [M, flag]  = extraction(Images(indLayerMask), 'XLim', XLim, 'YLim', YLim, 'ForceExtraction');
% %     if ~flag
% %         return
% %     end
%     M = Images(indLayerMask);
%     XLim = Images(indLayerMask).XLim;
%     YLim = Images(indLayerMask).YLim;
% end

Fig = FigUtils.createSScFigure;
str1 = 'Calcul des Boxplots en cours';
str2 = 'Computing BoxPlots';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
strLegend = cell(nbImages,1);
k2 = 0;
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    Titre = extract_ImageName(Images(indLayers(k)));
    
    %     if isempty(XLim)
    %         XLimk = Images(indLayers(k)).XLim;
    %         YLimk = Images(indLayers(k)).YLim;
    %     else
    %         XLimk = XLim;
    %         YLimk = YLim;
    %     end
    
    % 	XLimk = Images(indLayers(k)).XLim;
    % 	YLimk = Images(indLayers(k)).YLim;
    XLimk = [min(Images(indLayers(k)).x) max(Images(indLayers(k)).x)];
    YLimk = [min(Images(indLayers(k)).y) max(Images(indLayers(k)).y)];
    
    if isempty(indLayerMask)
        M = [];
    else
        [M, flag] = extraction(Images(indLayerMask), 'XLim', XLimk, 'YLim', YLimk, 'ForceExtraction');
        if ~flag
            continue
        end
    end
    
    
    Selection(1).Name     = Titre;
    Selection(1).TypeVal  = 'Layer';
    Selection(1).indLayer = 1; %listeprofilsAngle(k);
    
    [I, flag] = extraction(Images(indLayers(k)), 'XLim', XLimk, 'YLim', YLimk, 'ForceExtraction');
    if ~flag
        continue
    end
    [A, flag] = extraction(Images(listeprofilsAngle(k)), 'XLim', XLimk, 'YLim', YLimk, 'ForceExtraction');
    if ~flag
        continue
    end
    
    bins{1} = Bins1;
    if ~isempty(listeprofils2)
        Selection(2).Name     = Titre;
        Selection(2).TypeVal  = 'Layer';
        Selection(2).indLayer = 2; %listeprofilsAngle(k);
        [A(2), flag]  = extraction(Images(listeprofils2(k)), 'XLim', XLimk, 'YLim', YLimk, 'ForceExtraction');
        if ~flag
            continue
        end
        bins{2} = Bins2;
    end
    
    %% Affichage des images pour contr�le
    
    %{
% Comment� en attendant de pouvoir mettre une colormap par axe (attente R2011b fig�e)
figure(1232);
hc(1) = subplot(2,2,1); imagesc(I, 'Axe', hc(1));
hc(2) = subplot(2,2,2); imagesc(A, 'Axe', hc(2));
hc(3) = subplot(2,2,3); imagesc(M, 'Axe', hc(3));
imagesc(M.x, M.y, M.Image(:,:)); axis xy; grid on; colormap(jet(256));
linkaxes(hc)
    %}
    
    figure(1232);
    if isempty(M)
        nbAxesVert = 1;
    else
        nbAxesVert = 2;
    end
    hc(1) = subplot(nbAxesVert,2,1); imagesc(I.x, I.y, I.Image(:,:)); axis xy; grid on;
    if I.DataType == cl_image.indDataType('Reflectivity')
        colormap(gray(256))
    else
        colormap(jet(256))
    end
    if A(1).DataType == cl_image.indDataType('IncidenceAngle')
        hc(2) = subplot(nbAxesVert,2,2); imagesc(A(1).x, A(1).y, abs(A(1).Image(:,:))); axis xy; grid on; colormap(jet(256));
        Tag = ['BS : ' Titre];
    else
        hc(2) = subplot(nbAxesVert,2,2); imagesc(A(1).x, A(1).y, A(1).Image(:,:)); axis xy; grid on; colormap(jet(256));
        Tag = Titre;
    end
    if ~isempty(M)
        hc(3) = subplot(nbAxesVert,2,3); imagesc(M.x, M.y, M.Image(:,:)); axis xy; grid on; colormap(jet(256));
    end
    linkaxes(hc)
    
    %% Calcul de la courbe
    
    [a, bilanOut, flag] = courbesStats(I, Tag, nomCourbe, [], [], A, Selection, ...
        'LayerMask', M, 'valMask', valMask, 'bins', bins, ...
        'GeometryType', cl_image.strGeometryType{GeometryType}, ...
        'Sampling', 1, 'nbPMin', 2); %#ok<ASGLU>
    if ~flag
        continue
    end
    
    %     bilan = a.CourbesStatistiques.bilan;
    k2 = k2 + 1;
    bilan(k2) = a.CourbesStatistiques(end).bilan; %#ok<AGROW> % Modifi� le 01/03/2012 � Wellington % TODO = bilanOut
    
    Legend = CourbesStatsPlot(bilan(k2), 'Stats', 0, 'fig', Fig);
    if ~isempty(Legend)
        if iscell(Legend)
            strLegend{k2} = Legend{1};
        else
            strLegend{k2} = Legend;
        end
    end
    
    %{
FigUtils.createSScFigure(1734);
subplot(2,1,1); PlotUtils.createSScPlot(bilan{1}.x, bilan{1}.y); grid on; hold on;
subplot(2,1,2); PlotUtils.createSScPlot(bilan{1}.x, bilan{1}.nx); grid on; hold on;
    %}
    
    %     for k2=1:length(bilan{1})
    %         subNonNaN = (bilan{1}(k2).nx ~= 0);
    %         SumI(subNonNaN) = SumI(subNonNaN) + bilan{1}(k2).y(subNonNaN) .* bilan{1}(k2).nx(subNonNaN);
    %         NI(subNonNaN)   = NI(subNonNaN) + bilan{1}(k2).nx(subNonNaN);
    %     end
    
    %     FigUtils.createSScFigure(1735);
    %     subplot(2,1,1); PlotUtils.createSScPlot(Bins1, SumI./NI); grid on; hold on;
    %     subplot(2,1,2); PlotUtils.createSScPlot(Bins1, NI); grid on; hold on;
end
my_close(hw, 'MsgEnd')

%% R�cup�ration des courbes

if k2 ~= 0
    strLegend = strLegend(1:k2);
    legend(strLegend, 'Interpreter', 'None')
    
    for k1=1:length(bilan)
        CourbesStatistiques(k1).bilan =  bilan(k1); %#ok<AGROW>
    end
    
    CourbesStatistiques = CourbesStatsUnion(CourbesStatistiques);
    
    if isempty(this.CourbesStatistiques)
        this.CourbesStatistiques = CourbesStatistiques;
    else
        for k1=1:length(CourbesStatistiques)
            this.CourbesStatistiques(end+1).bilan{1} = CourbesStatistiques(k1).bilan{1};
        end
    end
    % a.CourbesStatistiques(1).bilan = bilan;
    % plotCourbesStats(this, 'Stats', 0, 'fig', Fig, 'sub', length(this.CourbesStatistiques));
    plotCourbesStats(this, 'Stats', 0, 'fig', Fig, 'sub', length(this.CourbesStatistiques)-1, 'LineWidth', 3);
    plotCourbesStats(this, 'Stats', 0, 'sub', length(this.CourbesStatistiques)-1, 'LineWidth', 3);
end
flag = 1;
