% Compute sone statistical curves
%
% Syntax
%   [a, bilan] = courbesStats(a, Tag, nomZoneEtude, commentaire, N, b, layerXY, ...)
%
% Input Arguments
%   a            : One cl_image instance
%   Tag          : Tag associated to the curve ("BS", "Depth", ...)
%   nomZoneEtude : Nom de la zone d'etude ou "Image entiere" ou "Image zoomee" % TODO : changer de nom
%   N            : [] or cl_image instance containing the number of averaged points
%   b            : cl_image instances containing the synchronized layers
%   Selection    : TODO (see section Examples)
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   a     : Instance of cl_image containing the statistical curves
%   bilan : Structure containing the statistical curves
%
% Examples
%     nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
%     aKM = cl_simrad_all('nomFic', nomFic);
%     [flag, a] = import_DefaultLayers(aKM);
%     SonarScope(a)
%
%     DTReflec = cl_image.indDataType('Reflectivity');
%     [kReflec, nomLayerReflec] = findIndLayerSonar(a, 1, 'DataType', DTReflec, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTAngles = cl_image.indDataType('TxAngle');
%     [kAngles, nomLayerAngles] = findIndLayerSonar(a, 1, 'DataType', DTAngles, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTSectors = cl_image.indDataType('TxBeamIndex');
%     [kSectors, nomLayerSectors] = findIndLayerSonar(a, 1, 'DataType', DTSectors, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     Selection(1).Name     = nomLayerAngles;
%     Selection(1).TypeVal  = 'Layer'
%     Selection(1).indLayer = kAngles;
%     Selection(1).DataType = DTAngles;
%     Selection(2).Name     = nomLayerSectors;
%     Selection(2).TypeVal  = 'Layer'
%     Selection(2).indLayer = kSectors;
%     Selection(2).DataType = DTSectors;
%     bins{1} = -75:75;
%     bins{2} = 1:3;
%   [a(kReflec), bilan] = courbesStats(a(kReflec), nomLayerReflec, 'Courbe test', '', [], ...
%         a, Selection, 'bins', bins, 'GeometryType', 'PingBeam', 'Sampling' , 1);
%     plotCourbesStats(a(kReflec), 'Stats', 0);
%     CurvesFilename = my_tempname('.mat');
%     exportCourbesStats(a(kReflec), CurvesFilename)
%     a(kReflec) = deleteCourbesStats(a(kReflec));
%
%     [flag, a(kReflec)] = import_xml_courbesStats(a(kReflec), CurvesFilename);
%     plotCourbesStats(a(kReflec), 'Stats', 0);
%
% See also cl_image/export_xml_courbesStats cl_image/import_xml_courbesStats Authors
% Authors : JMA
% ----------------------------------------------------------------------------

%{
Maintenance � faire sur courbesStats sonar_calcul_BS SonarDiagTxCompute :
a = courbesStats(this.Images, this.indImage, Tag, ...
nomCourbe{k}, Commentaire{k}, [], ...
1:length(this.Images), Selection, ...
'LayerMask', this.Images(indLayerMask), 'valMask', valMask(k), ...
'bins', bins, 'subx', subx, 'suby', suby, ...
'CourbeBias', CourbeBias, 'EditBias', 1, ...
'GeometryType', strGeometryType{GeometryType}, ...
'Sampling', 1, ...
'Coul', Coul);

et faire qqch comme �a dans courbesStats

Limites = sort(this(indImage).x([subx(1) subx(end)]));
xmin(1) = Limites(1);
xmax(1) = Limites(2);
Limites = sort(this(indImage).y([suby(1) suby(end)]));
ymin(1) = Limites(1);
ymax(1) = Limites(2);
for k=1:length(choixImages)
if choixImages(k) ~= indImage
XLim = this(choixImages(k)).XLim;
xmin(k+1) = XLim(1);
xmax(k+1) = XLim(2);
YLim = this(choixImages(k)).YLim;
ymin(k+1) = YLim(1);
ymax(k+1) = YLim(2);
end
end
XLim(1) = max(xmin);
XLim(2) = min(xmax);
YLim(1) = max(ymin);
YLim(2) = min(ymax);
%}

function [this, bilan, flag] = courbesStats(this, Tag, nomZoneEtude, commentaire, N, b, Selection, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, bins]         = getPropertyValue(varargin, 'bins',         []);
[varargin, stepBins]     = getPropertyValue(varargin, 'stepBins',     []);
[varargin, fig]          = getPropertyValue(varargin, 'fig',          220994);
[varargin, LayerMask]    = getPropertyValue(varargin, 'LayerMask',    []);
[varargin, valMask]      = getPropertyValue(varargin, 'valMask',      []);
[varargin, Sampling]     = getPropertyValue(varargin, 'Sampling',     []);
[varargin, CourbeBias]   = getPropertyValue(varargin, 'CourbeBias',   0);
[varargin, EditBias]     = getPropertyValue(varargin, 'EditBias',     0);
[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []);
[varargin, TagSup]       = getPropertyValue(varargin, 'TagSup',       []);
[varargin, Coul]         = getPropertyValue(varargin, 'Coul',         []);
[varargin, nbPMin]       = getPropertyValue(varargin, 'nbPMin',       10);
[varargin, SelectMode]   = getPropertyValue(varargin, 'SelectMode',   []);
[varargin, SelectSwath]  = getPropertyValue(varargin, 'SelectSwath',  []);
[varargin, SelectFM]     = getPropertyValue(varargin, 'SelectFM',     []);
[varargin, MeanCompType] = getPropertyValue(varargin, 'MeanCompType', 1);
[varargin, NomFic]       = getPropertyValue(varargin, 'NomFic',       []);
[varargin, SelectMode2EM2040] = getPropertyValue(varargin, 'SelectMode2EM2040', []);
[varargin, SelectMode3EM2040] = getPropertyValue(varargin, 'SelectMode3EM2040', []); % pas encore appel�


nbMaxSamplesStats = 2500000;

if CourbeBias
    TypeCourbeStat = 'For Bias Compensation';
else
    TypeCourbeStat = 'Statistics';
end

flag = testSignature(this, 'ImageType', 1);
if ~flag
    this = []; % TODO : V�rifier dans les appels si c'est utile : cr�er flag
    bilan = [];
    return
end

if isempty(Selection)
    Selection(1).Name = this.Name;
    Selection(1).TypeVal = 'Layer';
    Selection(1).indLayer = 1;
end

if (length(Selection) == 1) && strcmp(Selection(1).TypeVal, 'Axe')
    Selection(1).indLayer = 1;
    b = this;
end

choixLayers = [Selection(:).indLayer];
choixImages = choixLayers(choixLayers ~= 0);

%% Recherche de l'intersection commune entre toutes les images

if ~isempty(choixImages)
    if isempty(N)
        [subsubx, subxN, subxb] = intersect3(this.x(subx), b(choixImages(1)).x, b(choixImages(1)).x); %#ok<ASGLU>
        subx  = subx(subsubx);
        
        if isSonarSignal(this, 'PingCounter') && ~isempty(b(choixImages(1)).Sonar.PingCounter)
            this_y = this.Sonar.PingCounter(suby);
            b_y = b(choixImages(1)).Sonar.PingCounter;
            this_y = unwrapPingCounter(this_y);
            b_y    = unwrapPingCounter(b_y);
        else
            this_y = this.y(suby);
            b_y =  b(choixImages(1)).y;
        end
        [subsuby, subyN, subyb] = intersect3(this_y, b_y, b_y); %#ok<ASGLU>
        if isempty(subsuby)
            % Ajout JMA le 14/11/2019 au NIWA pour images de bathy
            % provenant des geotifs EM302 et EM2040 BayOfPlenty/SeurveyN
            [XLim, YLim, subx, suby] = intersectionImages(this, 1:this.nbColumns, 1:this.nbRows, b(choixImages(1)), 'SameSize');
        else
        	suby = suby(subsuby);
        end
    else
        [subsubx, subxN, subxb] = intersect3(this.x(subx), N.x, b(choixImages(1)).x); %#ok<ASGLU>
        subx  = subx(subsubx);
        
        if isSonarSignal(this, 'PingCounter') && ~isempty(b(choixImages(1)).Sonar.PingCounter)
            this_y = this.Sonar.PingCounter(suby);
            b_y = b(choixImages(1)).Sonar.PingCounter;
            N_y = N(choixImages(1)).Sonar.PingCounter;
            this_y = unwrapPingCounter(this_y);
            N_y    = unwrapPingCounter(N_y);
            b_y    = unwrapPingCounter(b_y);
        else
            this_y = this.y(suby);
            b_y =  b(choixImages(1)).y;
            N_y = N.y;
        end
        [subsuby, subyN, subyb] = intersect3(this_y, N_y, b_y); %#ok<ASGLU>
        suby  = suby(subsuby);
    end
end

if isempty(SelectMode)
    sub1 = true(length(suby),1);
else
    sub1 = (this.Sonar.PortMode_1(suby,1) == SelectMode);
end

if isempty(SelectSwath)
    sub2 = true(length(suby),1);
else
    sub2 = (this.Sonar.NbSwaths(suby,1) == SelectSwath);
end

if isempty(SelectFM)
    sub3 = true(length(suby),1);
else
    sub3 = ((1 + this.Sonar.PresenceFM(suby,1)) == SelectFM);
end

if isempty(SelectMode2EM2040)
    sub4 = true(length(suby),1);
else
    signalMode2 = this.SignalsVert.getSignalByTag('Mode2');
    if ~isempty(signalMode2)
        [flag, Mode2] = signalMode2.getValueMatrix();
        if ~flag
            return
        end
    end
%     sub4 = ((1 + this.Sonar.PresenceFM(suby,1)) == SelectMode2EM2040); 
    sub4 = (Mode2 == SelectMode2EM2040); % Modif JMA le 10/03/2022
end

Mode3 = [];
if isempty(SelectMode3EM2040)
    sub5 = true(length(suby),1);
    signalMode3 = this.SignalsVert.getSignalByTag('Mode3');
    if ~isempty(signalMode3)
        [~, Mode3] = signalMode3.getValueMatrix();
    end
else
    signalMode3 = this.SignalsVert.getSignalByTag('Mode3');
    if ~isempty(signalMode3)
        [flag, Mode3] = signalMode3.getValueMatrix();
        if ~flag
            return
        end
    end
    sub5 = (Mode3 == SelectMode3EM2040); % Modif JMA le 15/09/2022
end

suby = suby(sub1 & sub2 & sub3 & sub4 & sub5);
if isempty(suby)
    this  = []; % TODO : V�rifier dans les appels si c'est utile : cr�er flag
    bilan = [];
    flag  = 0;
    return
end

Limites = sort(this.x([subx(1) subx(end)]));
xmin(1) = Limites(1);
xmax(1) = Limites(2);
Limites = sort(this.y([suby(1) suby(end)]));
ymin(1) = Limites(1);
ymax(1) = Limites(2);

XLim(1) = max(xmin);
XLim(2) = min(xmax);
YLim(1) = max(ymin);
YLim(2) = min(ymax);

sub = ((this.x(subx) >= XLim(1)) & (this.x(subx) <= XLim(2)));
subx = subx(sub);
sub = ((this.y(suby) >= YLim(1)) & (this.y(suby) <= YLim(2)));
suby = suby(sub);

if isempty(Sampling)
    Sampling = (length(subx) * length(suby) * get_nbBytes(this.Image(1))) / nbMaxSamplesStats;
    Sampling = max(Sampling, 1);
    Sampling = sqrt(Sampling);
    subx = subx(floor(1:Sampling:end));
    suby = suby(floor(1:Sampling:end));
end

%% Extraction des info pour appel � courbesStats

I = this.Image(suby,subx);
if isempty(N)
    N = [];
else
    N = N.Image(subyN,subxN);
end

J = [];
J_DataType = [];
J_Support = [];

nBY = length(Selection);
CentrageMagnetic = ones(1,nBY);
for k=1:nBY
    if strcmp(Selection(k).TypeVal, 'Axe')
        if strcmp(Selection(k).Name(1:5), 'Axe X')
            J{end+1} = bins{k}; %#ok<AGROW>
            J{end+1} = repmat(this.x(subx), length(suby), 1); %#ok<AGROW>
            J{end+1} = 'X'; %#ok<AGROW>
            J_DataType{end+1} = 'X'; %#ok<AGROW>
        elseif strcmp(Selection(k).Name(1:5), 'Axe Y')
            J{end+1} = bins{k}; %#ok<AGROW>
            J{end+1} = repmat((this.y(suby))', 1, length(subx)); %#ok<AGROW>
            J{end+1} = 'Y'; %#ok<AGROW>
            J_DataType{end+1} = 'Y'; %#ok<AGROW>
        end
        J_Support{end+1} = 'Axe'; %#ok<AGROW>
        
    elseif strcmp(Selection(k).TypeVal, 'SignalVert')
        J{end+1} = bins{k}; %#ok<AGROW>
        V = get(this, Selection(k).Name);
        V = V(:);
        J{end+1} = repmat((V(suby)), 1, length(subx)); %#ok<AGROW>
        J{end+1} = Selection(k).Name; %#ok<AGROW>
        J_DataType{end+1} = Selection(k).Name; %#ok<AGROW>
        J_Support{end+1} = 'SignalVert'; %#ok<AGROW>
        
        % Bidouille pour �viter bug statistiques multiping Reson7125
        switch Selection(k).Name
            case 'SonarFrequency'
                CentrageMagnetic(k) = 0;
        end
        
    elseif strcmp(Selection(k).TypeVal, 'Layer')
        if isempty(stepBins)
            if isempty(bins)
                binsJ = b(Selection(k).indLayer).HistoCentralClasses;
            else
                %             bins{k}(1)   = bins{k}(1)   - eps;  % Pb rencontre avec Lena qui est une image en valeurs entieres de 1 a 255
                %             bins{k}(end) = bins{k}(end) + eps;
                binsJ = bins{k};
            end
        else
            if isempty(b(Selection(k).indLayer).HistoCentralClasses) % Ajout JMA le 20/03/2017
                this = []; % Pour bb rencontr� par Marc Roche lorsque calcul de courbes de BSQ sur un lot
                % de fichiers .all et que l'un des fichiers �tait en dehors de la zone du MNT lors du calcul de
                % l'angle d'incidence
                bilan = [];
                flag = 0;
                return
            end
            ValMin = b(Selection(k).indLayer).HistoCentralClasses(1);
%             ValMax = b(Selection(k).indLayer).HistoCentralClasses(end); % Faux si HistoCentralClasses est un cl_memapfile
            n = length(b(Selection(k).indLayer).HistoCentralClasses);
            ValMax = b(Selection(k).indLayer).HistoCentralClasses(n);

            if isIntegerVal(b(Selection(k).indLayer))
                ValMin = floor(ValMin);
                ValMax = ceil(ValMax);
            end
            binsJ = ValMin : stepBins(k): ValMax;
            binsJ = centrage_magnetique(binsJ);
        end
        
        J{end+1} = binsJ; %#ok<AGROW>
        J{end+1} = get_val_xy(b(Selection(k).indLayer), this.x(subx), this.y(suby)); %#ok<AGROW>
        J{end+1} = b(Selection(k).indLayer).Name; %#ok<AGROW>
        J_DataType{end+1} = cl_image.strDataType{b(Selection(k).indLayer).DataType}; %#ok<AGROW>
        J_Support{end+1} = 'Layer'; %#ok<AGROW>
    end
end

%% Gestion des masques

if ~isempty(LayerMask)
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
    %     subNonMasque = find(Masque ~= valMask);
    subNonMasque = (Masque ~= valMask);
    I(subNonMasque) = NaN;
end

nBY = length(varargin) / 3;
if isempty(CentrageMagnetic)
    CentrageMagnetic = ones(1,nBY);
end

clear LayerMask Limites XLim YLim b b_y bins binsJ
clear choixImages choixLayers nBY subsubx subsuby subxN subxb subyN subyb
clear this_y valMask xmax xmin ymax ymin

%% Cas o� un layer conditionnel est un masque ou une segmentation : on met les valeurs en dehors des bins � NaN pour �viter que les classes externes soient amalgam�es avec les classes limites

nBY = length(J) / 3;
for k=1:nBY
    DT = J_DataType{k};
    %     if cl_image.indDataType(J_DataType{k})
    if strcmp(DT, 'Mask') || strcmp(DT, 'Segmentation')
        k1 = 1 + 3*(k-1);
        k2 = 2 + 3*(k-1);
        subNaN = (J{k2} < J{k1}(1)) | (J{k2} > J{k1}(end));
        J{k2}(subNaN) = NaN; %#ok<AGROW>
    end
end

%% Suppression des NaN

subNaN = isnan(I);
for k=1:nBY
    k2 = 2 + 3*(k-1);
    if isa(J{k2}, 'cl_time')
        J{k2} = J{k2}.timeMat; %#ok<AGROW>
    end
    subNaN = subNaN | isnan(J{k2});
end
I(subNaN) = [];
if isempty(I)
    this = []; % TODO : V�rifier dans les appels si c'est utile : cr�er flag
    bilan = [];
    flag = 0;
    return
end

for k=1:nBY
    k2 = 2 + 3*(k-1);
    J{k2}(subNaN) = []; %#ok<AGROW>
end

clear subNaN k k2

%% Appel de la fonction de calcul des statistiques conditionnelles

[means, sem, counts, bilan, medians] = courbesStats(I, this.Name, N, J{:}, ...
    'nbPMin', nbPMin, 'CourbeBias', CourbeBias, 'EditBias', EditBias, ...
    'CentrageMagnetic', CentrageMagnetic, 'Unit', this.Unit, ...
    'MeanCompType', MeanCompType, 'NomFic', NomFic);
if isempty(means)
    this = []; % TODO : V�rifier dans les appels si c'est utile : cr�er flag
    bilan = [];
    flag = 0;
    return
end
for k=1:length(bilan{1})
    bilan{1}(k).nomZoneEtude       = nomZoneEtude;
    bilan{1}(k).commentaire        = commentaire;
    bilan{1}(k).Tag                = Tag;
    bilan{1}(k).DataTypeValue      = cl_image.strDataType{this.DataType};
    bilan{1}(k).DataTypeConditions = J_DataType;
    bilan{1}(k).Support            = J_Support;
    
    bilan{1}(k).GeometryType   = GeometryType;
    bilan{1}(k).TypeCourbeStat = TypeCourbeStat;
    
    bilan{1}(k).TagSup = TagSup;
    
    if ~isempty(SelectMode)
        bilan{1}(k).Mode = SelectMode;
    end
    
    if ~isempty(SelectMode2EM2040)
        bilan{1}(k).Mode2EM2040 = SelectMode2EM2040;
    end
    
    if isempty(SelectMode3EM2040) % Attention, la gestion de mode3 n'est pas faite de la m�me mani�re que Mode2 car c'est un bricolage pour permettre de faire des compensations par longueur d'impulsion dans SPFE_Batchxxx
        if ~isempty(Mode3)
            bilan{1}(k).Mode3EM2040 = mode(Mode3);
        end
    else
        bilan{1}(k).Mode3EM2040 = SelectMode3EM2040;
    end
    
    if ~isempty(SelectSwath)
        bilan{1}(k).NbSwaths = SelectSwath;
    end
    if ~isempty(SelectFM)
        bilan{1}(k).SelectFM = SelectFM;
    end
    
    if ~isempty(Coul)
        kCoul = min(1+bilan{1}(k).numVarCondition, size(Coul,1));
        bilan{1}(k).Color = Coul(kCoul,:); % +1 car la premi�re couleur est r�serv�e pour les NaN
        %         bilan{1}(k).Color = Coul(k,:); % +1 car la premi�re couleur est r�serv�e pour les NaN
    end
end

%% Sauvegarde des resultats

k = length(this.CourbesStatistiques) + 1;
this.CourbesStatistiques(k).x       = this.x(subx);
this.CourbesStatistiques(k).y       = this.y(suby);
this.CourbesStatistiques(k).means   = means;
this.CourbesStatistiques(k).medians = medians;
this.CourbesStatistiques(k).sem     = sem;
this.CourbesStatistiques(k).counts  = counts;
this.CourbesStatistiques(k).bilan   = bilan;

%% Par ici la sortie

if nargout == 0
    CourbesStatsPlot(bilan, 'fig', fig);
end
