function CourbeConditionnelle = courbesStatsTransmutationPourImagerie(this, CourbeConditionnelle)

for k1=1:length(CourbeConditionnelle)
%     for k=1:length(CourbeConditionnelle(k1).bilan)
%         bilan = CourbeConditionnelle(k1).bilan{k}(1);
    if isfield(CourbeConditionnelle(k1), 'bilan') && ~isempty(CourbeConditionnelle(k1).bilan)
        for k2=1:length(CourbeConditionnelle(k1).bilan{1})
            bilan = CourbeConditionnelle(k1).bilan{1}(k2);
            
            % Pour corriger un manque dans la cr�ation de la courbe dans Stats vertical
            if ~isfield(bilan, 'GeometryType')
                bilan.GeometryType = 'PingBeam';
            end
            
            if strcmp(bilan.GeometryType, 'PingBeam') && strcmp(bilan.DataTypeValue, 'Reflectivity') ...
                    && strcmp(bilan.nomX, 'X')...
                    && (length(bilan.Support) == 1) && strcmp(bilan.Support{1}, 'Axe') ...
                    && (length(bilan.DataTypeConditions) == 1) && strcmp(bilan.DataTypeConditions, 'X') ...
                    && (this.GeometryType == cl_image.indGeometryType('PingSamples')) && (this.DataType == cl_image.indDataType('Reflectivity'))
                bilan.GeometryType = 'PingSamples';
                bilan.nomX = 'RxBeamIndex';
                bilan.Support{1} = 'Layer';
                bilan.DataTypeConditions{1} = 'RxBeamIndex';
            end
            
            % Pour corriger un manque dans la cr�ation de la courbe dans Stats vertical
            if ~isfield(CourbeConditionnelle(k1).bilan{1}(k2), 'GeometryType')
                CourbeConditionnelle(k1).bilan{1}(k2).GeometryType = 'PingBeam';
            end
            
            CourbeConditionnelle(k1).bilan{1}(k2) = bilan;
        end
    end
end