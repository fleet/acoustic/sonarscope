function [flag, ParentGrid] = createJIDEGridFromImage(this, varargin)

ParentGrid  = java.util.ArrayList();

for k=1:length(this)
    gridJIDE = com.jidesoft.grid.DefaultProperty();
    
    gridJIDE.setName(['cl_image(' num2str(k) ')']);
    gridJIDE.setEditable(false);
    ParentGrid.add(gridJIDE);
    
    
    %% General properties
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('General properties');
    p2.setEditable(false);
    gridJIDE.addChild(p2);
    
    %% Name
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Name');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).Name);
    p3.setCategory('Image properties');
    p3.setDisplayName('Name');
    p3.setDescription('Image name.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% ClassName
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ClassName');
    p3.setType(javaclass('char',1));
    p3.setValue(class(this(k).Image(1)));
    p3.setCategory('Image properties');
    p3.setDisplayName('ClassName');
    p3.setDescription('Storage data type.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Memory
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Memory');
    p3.setType(javaclass('char',1));
    p3.setValue(where(this(k)));
    p3.setCategory('Image properties');
    p3.setDisplayName('Memory');
    p3.setDescription('Support of values : RAM or Virtual memory.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% nbRows
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('nbRows');
    p3.setType(javaclass('double',1));
    p3.setValue(this(k).nbRows);
    p3.setCategory('Image properties');
    p3.setDisplayName('nbRows');
    p3.setDescription('Number of rows.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% nbColumns
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('nbColumns');
    p3.setType(javaclass('double',1));
    p3.setValue(this(k).nbColumns);
    p3.setCategory('Image properties');
    p3.setDisplayName('nbColumns');
    p3.setDescription('Number of columns.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Unit
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Unit');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).Unit);
    p3.setCategory('Image properties');
    p3.setDisplayName('Unit');
    p3.setDescription('Unit of the values.');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% ValNaN
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ValNaN');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).ValNaN));
    p3.setCategory('Image properties');
    p3.setDisplayName('ValNaN');
    p3.setDescription('Value used to signify No Data.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% DataType
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('DataType');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).DataType) ' (' cl_image.strDataType{this(k).DataType} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('DataType');
    p3.setDescription('Type of data : Reflectivity, Bathymetry, TxAngle, ....');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options0 = cl_image.strDataType;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strDataType');
    p3.setType(javaclass('char',1));
    p3.setValue(cl_image.strDataType{this(k).DataType});
    p3.setCategory('Image properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('strDataType');
    p3.setDescription('Liste of types of data : Reflectivity, Bathymetry, TxAngle, ....');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% MaskBits
    %{
for i=1:length(this(k).strMaskBits)
    str{end+1} = sprintf('MaskBits{%d}         <--> %s}', i, this(k).strMaskBits{i}); %#ok
end
    %}
    
    %% GeometryType
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('GeometryType');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).GeometryType) ' (' this(k).strGeometryType{this(k).GeometryType} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('GeometryType');
    p3.setDescription('Type of geometry : LatLong, GeoYX, PingBeam, ....');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options1 = this(k).strGeometryType;
    editor1  = com.jidesoft.grid.ListComboBoxCellEditor(options1);
    context1 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor1, context1);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strGeometryType');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).strGeometryType{this(k).GeometryType});
    p3.setCategory('Image properties');
    p3.setEditorContext(context1);
    p3.setDisplayName('strGeometryType');
    p3.setDescription('Liste of types of geometry : LatLong, GeoYX, PingBeam, ....');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% ImageType
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ImageType');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).ImageType) ' (' this(k).strImageType{this(k).ImageType} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('ImageType');
    p3.setDescription('Type of image : Intensity, RGB, ....');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options2 = this(k).strImageType;
    editor2  = com.jidesoft.grid.ListComboBoxCellEditor(options2);
    context2 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor2, context2);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strImageType');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).strImageType{this(k).ImageType});
    p3.setCategory('Image properties');
    p3.setEditorContext(context2);
    p3.setDisplayName('strImageType');
    p3.setDescription('Liste of types of image : Intensity, RGB, ....');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% SpectralStatus
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SpectralStatus');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).SpectralStatus) ' (' this(k).strSpectralStatus{this(k).SpectralStatus} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('SpectralStatus');
    p3.setDescription('Type of image : Intensity, RGB, ....');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options3 = this(k).strSpectralStatus;
    editor3  = com.jidesoft.grid.ListComboBoxCellEditor(options3);
    context3 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor3, context3);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strSpectralStatus');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).strSpectralStatus{this(k).SpectralStatus});
    p3.setCategory('Image properties');
    p3.setEditorContext(context3);
    p3.setDisplayName('strSpectralStatus');
    p3.setDescription('Liste of types of image : Intensity, RGB, ....');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% X
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('X');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).x));
    p3.setCategory('Image properties');
    p3.setDisplayName('X');
    p3.setDescription('X coordinates.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% XLabel
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('XLabel');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).XLabel);
    p3.setCategory('Image properties');
    p3.setDisplayName('XLabel');
    p3.setDescription('Name of the X axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% XUnit
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('XUnit');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).XUnit);
    p3.setCategory('Image properties');
    p3.setDisplayName('XUnit');
    p3.setDescription('Unit of the X axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% XLim
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('XLim');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).XLim));
    p3.setCategory('Image properties');
    p3.setDisplayName('XLim');
    p3.setDescription('Limits of the X axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% XRange
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('XRange');
    p3.setType(javaclass('double',1));
    p3.setValue(diff(this(k).XLim));
    p3.setCategory('Image properties');
    p3.setDisplayName('XRange');
    p3.setDescription('Range of the X axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% XStep
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('XStep');
    p3.setType(javaclass('double',1));
    p3.setValue(get(this(k), 'XStep'));
    p3.setCategory('Image properties');
    p3.setDisplayName('XStep');
    p3.setDescription('Spacing distance between 2 pixels for the X axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% XStepMetric
    if this(k).GeometryType == cl_image.indGeometryType('LatLong')
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('XStepMetric');
        p3.setType(javaclass('double',1));
        p3.setValue(get(this(k), 'XStepMetric'));
        p3.setCategory('Image properties');
        p3.setDisplayName('XStepMetric');
        p3.setDescription('Corresponding metric spacing distance between 2 pixels for the X axe.');
        p3.setEditable(false);
        p2.addChild(p3);
    end
    
    %% XDir
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('XDir');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).XDir) ' (' this(k).strXDir{this(k).XDir} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('XDir');
    p3.setDescription('Orientation of X axe');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options4 = this(k).strXDir;
    editor4  = com.jidesoft.grid.ListComboBoxCellEditor(options4);
    context4 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor4, context4);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strXDir');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).strXDir{this(k).XDir});
    p3.setCategory('Image properties');
    p3.setEditorContext(context4);
    p3.setDisplayName('strXDir');
    p3.setDescription('Liste of possible orientations of X axe');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Y
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Y');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).y));
    p3.setCategory('Image properties');
    p3.setDisplayName('Y');
    p3.setDescription('Y coordinates.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% YLabel
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('YLabel');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).YLabel);
    p3.setCategory('Image properties');
    p3.setDisplayName('YLabel');
    p3.setDescription('Name of the Y axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %%YUnit
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('YUnit');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).YUnit);
    p3.setCategory('Image properties');
    p3.setDisplayName('YUnit');
    p3.setDescription('Unit of the Y axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% YLim
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('YLim');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).YLim));
    p3.setCategory('Image properties');
    p3.setDisplayName('YLim');
    p3.setDescription('Limits of the Y axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% YRange
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('YRange');
    p3.setType(javaclass('double',1));
    p3.setValue(diff(this(k).YLim));
    p3.setCategory('Image properties');
    p3.setDisplayName('YRange');
    p3.setDescription('Range of the Y axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% YStep
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('YStep');
    p3.setType(javaclass('double',1));
    p3.setValue(get(this(k), 'YStep'));
    p3.setCategory('Image properties');
    p3.setDisplayName('YStep');
    p3.setDescription('Spacing distance between 2 pixels for the Y axe.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% YStepMetric
    if this(k).GeometryType == cl_image.indGeometryType('LatLong')
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('YStepMetric');
        p3.setType(javaclass('double',1));
        p3.setValue(get(this(k), 'YStepMetric'));
        p3.setCategory('Image properties');
        p3.setDisplayName('YStepMetric');
        p3.setDescription('Corresponding metric spacing distance between 2 pixels for the Y axe.');
        p3.setEditable(false);
        p2.addChild(p3);
    end
    
    %% YDir
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('YDir');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).YDir) ' (' this(k).strYDir{this(k).YDir} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('YDir');
    p3.setDescription('Orientation of Y axe');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options5 = this(k).strYDir;
    editor5  = com.jidesoft.grid.ListComboBoxCellEditor(options5);
    context5 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor5, context5);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strYDir');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).strYDir{this(k).YDir});
    p3.setCategory('Image properties');
    p3.setEditorContext(context5);
    p3.setDisplayName('strYDir');
    p3.setDescription('Liste of possible orientations of Y axe');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% ColormapIndex
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('NuColormap');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).ColormapIndex) ' (' this(k).strColormapIndex{this(k).ColormapIndex} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('NuColormap');
    p3.setDescription('Colormap number');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options6 = this(k).strColormapIndex;
    editor6  = com.jidesoft.grid.ListComboBoxCellEditor(options6);
    context6 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor6, context6);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strColormapIndex');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).strColormapIndex{this(k).ColormapIndex});
    p3.setCategory('Image properties');
    p3.setEditorContext(context6);
    p3.setDisplayName('strColormapIndex');
    p3.setDescription('Liste of possible colormaps');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Colormap
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Colormap');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).Colormap));
    p3.setCategory('Image properties');
    p3.setDisplayName('Colormap');
    p3.setDescription('Brief summary of Colormap values.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% ColormapCustom
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ColormapCustom');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).ColormapCustom));
    p3.setCategory('Image properties');
    p3.setDisplayName('ColormapCustom');
    p3.setDescription('Brief summary of Custom Colormap values.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% CLim
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('CLim');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).CLim));
    p3.setCategory('Image properties');
    p3.setDisplayName('CLim');
    p3.setDescription('Limits of contrast enhancement.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Video   
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Video');
    p3.setType(javaclass('char',1));
    p3.setValue([num2str(this(k).Video) ' (' this(k).strVideo{this(k).Video} ')']);
    p3.setCategory('Image properties');
    p3.setDisplayName('Video');
    p3.setDescription('Type of video.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    options7 = this(k).strVideo;
    editor7  = com.jidesoft.grid.ListComboBoxCellEditor(options7);
    context7 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor7, context7);

    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('strVideo');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).strVideo{this(k).Video});
    p3.setCategory('Image properties');
    p3.setEditorContext(context7);
    p3.setDisplayName('strVideo');
    p3.setDescription('Liste of possible choices');
    p3.setEditable(true);
    p2.addChild(p3);
 
    %% VertExagAuto
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('stringProp');
    p3.setType(javaclass('double',1));
    p3.setValue(this(k).VertExagAuto);
    p3.setCategory('Image properties');
    p3.setDisplayName('VertExagAuto');
    p3.setDescription('Do not remember what it is.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% VertExag
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('VertExag');
    p3.setType(javaclass('double',1));
    p3.setValue(this(k).VertExag);
    p3.setCategory('Image properties');
    p3.setDisplayName('VertExag');
    p3.setDescription('Factor of shading.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Azimuth
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Azimuth');
    p3.setType(javaclass('double',1));
    p3.setValue(this(k).Azimuth);
    p3.setCategory('Image properties');
    p3.setDisplayName('Azimuth');
    p3.setDescription('Azimuth of shading.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% ContourValues
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ContourValues');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).ContourValues(:)));
    p3.setCategory('Image properties');
    p3.setDisplayName('ContourValues');
    p3.setDescription('Contour values.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% TagSynchroX
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('TagSynchroX');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).TagSynchroX);
    p3.setCategory('Image properties');
    p3.setDisplayName('TagSynchroX');
    p3.setDescription('Tag for X axe synchronization between images.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% TagSynchroY
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('TagSynchroY');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).TagSynchroY);
    p3.setCategory('Image properties');
    p3.setDisplayName('TagSynchroY');
    p3.setDescription('Tag for Y axe synchronization between images.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% TagSynchroContrast
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('TagSynchroContrast');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).TagSynchroContrast);
    p3.setCategory('Image properties');
    p3.setDisplayName('TagSynchroContrast');
    p3.setDescription('Tag for contraste synchronization between images.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% InitialFileName
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('InitialFileName');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).InitialFileName);
    p3.setCategory('Image properties');
    p3.setDisplayName('InitialFileName');
    p3.setDescription('Name of the file from which this(k) image is coming from.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% InitialFileFormat
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('InitialFileFormat');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).InitialFileFormat);
    p3.setCategory('Image properties');
    p3.setDisplayName('InitialFileFormat');
    p3.setDescription('Type of format of the file from which this(k) image is coming from.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% InitialImageName
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('InitialImageName');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).InitialImageName);
    p3.setCategory('Image properties');
    p3.setDisplayName('InitialImageName');
    p3.setDescription('Name of the upper mother image.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Comments
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Comments');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).Comments);
    p3.setCategory('Image properties');
    p3.setDisplayName('Comments');
    p3.setDescription('Comments.');
    p3.setEditable(false);
    p2.addChild(p3);
    
    %% Statistics
    editProperties_statistics(this(k), gridJIDE);
    
    
    %% Carto
    if ~isempty(this(k).Carto)
        p4 = com.jidesoft.grid.DefaultProperty();
        p4.setName('Carto');
        p4.setEditable(false);
        p4.setExpanded(false);
        % editProperties(this(k)(k).params);
        p4 = editProperties(this(k).Carto, 'Parent', p4);
        gridJIDE.addChild(p4);
        p4.setExpanded(false);
    end
    
    %% Sonar Data
    editProperties_sonarData(this(k), gridJIDE);
    
    
end


flag = 1;

function strTag = getTagListJIDE
% Cr�ation d'un Tag unique � chaque liste de valeurs d'un composant
% JIDE.
strTag = ['comboxeditor' num2str(randi(10000))];

