function [flag, that] = create_LayerFromSignalVert(this, signal, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Index] = getPropertyValue(varargin, 'Index', []);

[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

that = [];

%% Algorithm

strDT = strtok(signal.name, '_'); % Utile pour AbsorptionCoeff_RT
DT = cl_image.indDataType(strDT);
if DT == 1
    Suffix = strDT;
else
    Suffix = signal.name((length(strDT)+1):end);
end

[flag, X, Unit] = signal.getValueMatrix();
if ~flag
    return
end

I = NaN(length(suby), length(subx), 'single');

if signal.isLinkedSignal()
    Ind = Index.Image(suby,subx);
    for k1=1:length(suby)
        indPing = Ind(k1,:);
        try
            for k2=min(indPing):max(indPing)
                I(k1,indPing == k2) = X(suby(k1),k2);
            end
        catch
            if size(X,2) == 1 % Pour cas particulier de l'EM300 o� une seule valeur par ping du coefficient d'absorption est donn�e dans le datagramme d'imagerie
                for k2=min(indPing):max(indPing)
                    I(k1,indPing == k2) = X(suby(k1));
                end
            end
        end
    end
else
    for k1=1:length(suby)
        I(k1,:) = X(suby(k1),1);
    end
end

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'NoStats', NoStats, 'ValNaN', NaN, ...
    'DataType', DT, 'Unit', Unit, 'ColormapIndex', 3, 'AppendName', Suffix);
