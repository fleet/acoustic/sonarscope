function [flag, I] = create_MatrixFromSignalVert(this, indImage, signal, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, FirstOfList] = getPropertyValue(varargin, 'FirstOfList', 1);
[varargin, Mute]        = getPropertyValue(varargin, 'Mute',        0); %#ok<ASGLU>

I = [];

%% Algorithm

if isempty(signal.name)
    flag = 0;
    return
end

[flag, X, ~] = signal.getValueMatrix();
if ~flag
    return
end

I = NaN(length(suby), length(subx), 'single');

if signal.isLinkedSignal()
    DT = cl_image.indDataType(signal.link);
    [IndLayerIndexation, ~, ~] = findIndLayerSonar(this, indImage, 'DataType', DT, 'WithCurrentImage', 1, ...
        'OnlyOneLayer', 'AtLeastOneLayer', 'Mute', 1, 'FirstOfList', FirstOfList); 
    
    if isempty(IndLayerIndexation)
        if ~Mute
            str1 = sprintf('Fonction create_MatrixFromSignalVert :\nLe signal "%s" est index� au layer "%s", or ce layer n''a pas �t� trouv�, il est donc impossible de cr�er le layer correspondant.', signal.name, signal.link);
            str2 = sprintf('Function create_MatrixFromSignalVert :\nSignal "%s" is indexed to layer "%s", this layer is missing, it is impossible to create the corresponding layer.', signal.name, signal.link);
            my_warndlg(Lang(str1,str2), 1);
        end
        flag = 0;
        return
    end
    
    Index = this(IndLayerIndexation);
    Ind = Index.Image(suby,subx);
    for k1=1:length(suby)
        indPing = Ind(k1,:);
        try
            for k2=min(indPing):max(indPing)
                I(k1,indPing == k2) = X(suby(k1),k2);
            end
        catch
            if size(X,2) == 1 % Pour cas particulier de l'EM300 o� une seule valeur par ping du coefficient d'absorption est donn�e dans le datagramme d'imagerie
                for k2=min(indPing):max(indPing)
                    I(k1,indPing == k2) = X(suby(k1));
                end
            end
        end
    end
else
    for k1=1:length(suby)
        I(k1,:) = X(suby(k1),1);
    end
end
