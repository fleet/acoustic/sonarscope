function [flag, this] = create_RxBeamAngle(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = create_RxBeamAngle_unitaire(this(i), varargin{:});
    if ~flag
        return
    end
    this(i) = that;
end

function [flag, this] = create_RxBeamAngle_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithme

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

TxAngle = this.Sonar.SampleBeamData.TxAngle;
I = repmat(TxAngle(subx), length(suby), 1);
this = replace_Image(this, I);
% [flag, pppp] = inpaint_nans(double(this.Image(suby,subx,:)));
% this = replace_Image(this, pppp);
% this = replace_Image(this, wsmooth(this.Image(suby,subx,:)));

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);
this. DataType = cl_image.indDataType('TxAngle');

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.Unit = 'deg';

%% Compl�tion du titre

this = update_Name(this);
