% Creation d'un fichier ".mbg" de Caraibes(R)
%
% Syntax
%   create_caraibesMBG(a, nomFicOut, ...)
%
% Input Arguments
%   a         : Instance de cl_image
%   nomFicOut : nom du fichier de sortie
%
% Name-Value Pair Arguments
%   sk_ref   : squelette de r�f�rence
%   NomLayer : Nom du Layer
%   Depth    : Layer
%   x        : �chantillonage en x
%   y        : �chantillonage en y
%   Carto    : objet Carto associ�.
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex2.mnt');
%   a_ref = cl_netcdf('fileName', nomFic);
%   sk_ref = get(a_ref, 'skeleton');
%   a = cl_car_ima(nomFic);
%   b = view(a);
%   c = get_Image(b, 1);
%   SonarScope(c)
%
%   nomFicOut = [tempname '.dtm']
%   I = get(c, 'Image');
%   x = get(c, 'x');
%   y = get(c, 'y');
%   Carto = get(c, 'Carto');
%   create_caraibesMBG(cl_image, nomFicOut, sk_ref, 'Depth', I, x, y, Carto);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function create_caraibesMBG(this, nomFicOut, sk_ref, Layers, Sounder_type, mbShip, mbSurvey, ...
    mbDate, mbTime, mbAbscissa, mbOrdinate, mbMinDepth, mbMaxDepth, Carto, varargin) %#ok

% [mbDepthScale, mbDepthScale_scale_factor] = cauchemard_int32(Layers{3});
% [mbDistanceScale, mbDistanceScale_scale_factor] = cauchemard_int16(Layers{2});

[mbDepthScale, mbDepthScale_scale_factor]       = scale_factor_Depth(Layers{3});
[mbDistanceScale, mbDistanceScale_scale_factor] = scale_factor_Distance(Layers{2});
% [mbRangeScale, mbRangeScale_scale_factor]       = scale_factor_Range(Layers{});

mbNorthLatitude = max(mbOrdinate);
mbSouthLatitude = min(mbOrdinate);
mbEastLongitude = max(mbAbscissa);
mbWestLongitude = min(mbAbscissa);

mbStartDate     = mbDate(1);
mbStartTime     = mbTime(1);
mbEndDate       = mbDate(end);
mbEndTime       = mbTime(end);

N = size(Layers{1}, 1);
nbBeam = size(Layers{1}, 2);

%% Mise � jour des Dimensions

switch Sounder_type
    case {1; 6} % EM12D, EM3000D
        nbAntennaNbr = 2;
    otherwise
        nbAntennaNbr = 1;
end

% sk_ref.mbCycleNbr = N;

numDim = cdf_find_numDim(sk_ref, 'mbCycleNbr');
sk_ref.dim(numDim).value = N;
numDim = cdf_find_numDim(sk_ref, 'mbBeamNbr');
sk_ref.dim(numDim).value = nbBeam;
clear numDim


numAtt = cdf_find_numAtt(sk_ref, 'mbCycleCounter');
sk_ref.att(numAtt).value = N;
numAtt = cdf_find_numAtt(sk_ref, 'mbSounder');
sk_ref.att(numAtt).value = Sounder_type;
numAtt = cdf_find_numAtt(sk_ref, 'mbShip');
sk_ref.att(numAtt).value = mbShip;
numAtt = cdf_find_numAtt(sk_ref, 'mbSurvey');
sk_ref.att(numAtt).value = mbSurvey;
numAtt = cdf_find_numAtt(sk_ref, 'mbStartDate');
sk_ref.att(numAtt).value = mbStartDate;
numAtt = cdf_find_numAtt(sk_ref, 'mbStartTime');
sk_ref.att(numAtt).value = mbStartTime;
numAtt = cdf_find_numAtt(sk_ref, 'mbEndDate');
sk_ref.att(numAtt).value = mbEndDate;
numAtt = cdf_find_numAtt(sk_ref, 'mbEndTime');
sk_ref.att(numAtt).value = mbEndTime;
numAtt = cdf_find_numAtt(sk_ref, 'mbNorthLatitude');
sk_ref.att(numAtt).value = mbNorthLatitude;
numAtt = cdf_find_numAtt(sk_ref, 'mbSouthLatitude');
sk_ref.att(numAtt).value = mbSouthLatitude;
numAtt = cdf_find_numAtt(sk_ref, 'mbEastLongitude');
sk_ref.att(numAtt).value = mbEastLongitude;
numAtt = cdf_find_numAtt(sk_ref, 'mbWestLongitude');
sk_ref.att(numAtt).value = mbWestLongitude;
%{
Meridian_180 = get_valAtt(a_ref, 'mbMeridian180');
Meridian_180 = double(Meridian_180); %??????????????

mbGeoDictionnary        = get_valAtt(a_ref, 'mbGeoDictionnary');
mbGeoRepresentation     = get_valAtt(a_ref, 'mbGeoRepresentation');
mbGeodesicSystem        = get_valAtt(a_ref, 'mbGeodesicSystem');
mbProjParameterValue    = get_valAtt(a_ref, 'mbProjParameterValue');
mbProjParameterCode     = get_valAtt(a_ref, 'mbProjParameterCode');
%}

DemiGrandAxe = get(Carto, 'Ellipsoide.DemiGrandAxe');
E2 = get(Carto, 'Ellipsoide.E2');
Aplatissement = get(Carto, 'Ellipsoide.Aplatissement');
[Ellipsoid, Ellipsoid_name] = getEllipsoidCaraibes(Carto);
Projection = getProjectionCaraibes(Carto);

numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidA');
sk_ref.att(numAtt).value = DemiGrandAxe;
numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidE2');
sk_ref.att(numAtt).value = E2;
numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidInvF');
sk_ref.att(numAtt).value = 1/Aplatissement;
numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidName');
sk_ref.att(numAtt).value = Ellipsoid_name;
numAtt = cdf_find_numAtt(sk_ref, 'mbProjType');
sk_ref.att(numAtt).value = 0;% Projection-1;
numAtt = cdf_find_numAtt(sk_ref, 'mbMinDepth');
sk_ref.att(numAtt).value = mbMinDepth;
numAtt = cdf_find_numAtt(sk_ref, 'mbMaxDepth');
sk_ref.att(numAtt).value = mbMaxDepth;
% mbReference, ...


%% Cr�ation du fichier r�sultat

b = cl_netcdf('fileName', nomFicOut, 'skeleton', sk_ref);
clear nomFicOut

%% Remplissage des valeurs des variables

% b = set_valAtt(b, 'mbCycleCounter', 	N);
% b = set_valAtt(b, 'mbSounder',          Sounder_type);
% b = set_valAtt(b, 'mbShip',             mbShip);
% b = set_valAtt(b, 'mbSurvey',           mbSurvey);
% b = set_valAtt(b, 'mbStartDate',     	mbStartDate);
% b = set_valAtt(b, 'mbStartTime',    	mbStartTime);
% b = set_valAtt(b, 'mbEndDate',          mbEndDate);
% b = set_valAtt(b, 'mbEndTime',          mbEndTime);
% b = set_valAtt(b, 'mbNorthLatitude',	mbNorthLatitude);
% b = set_valAtt(b, 'mbSouthLatitude',	mbSouthLatitude);
% b = set_valAtt(b, 'mbEastLongitude',	mbEastLongitude);
% b = set_valAtt(b, 'mbWestLongitude',	mbWestLongitude);
% b = set_valAtt(b, 'mbEllipsoidA', DemiGrandAxe);
% b = set_valAtt(b, 'mbEllipsoidE2', E2);
% b = set_valAtt(b, 'mbEllipsoidInvF', 1/Aplatissement);
% b = set_valAtt(b, 'mbEllipsoidName', Ellipsoid_name);
% b = set_valAtt(b, 'mbProjType', Projection); %#ok

[varargin, X] = getPropertyValue(varargin, 'mbCycle', []);
if ~isempty(X)
    b = set_value(b, 'mbCycle', X);
end

b = set_value(b, 'mbDate', mbDate);
b = set_value(b, 'mbTime', mbTime);
b = set_value(b, 'mbOrdinate', mbOrdinate);
b = set_value(b, 'mbAbscissa', mbAbscissa);
b = set_value(b, 'mbBeam', nbBeam);

%{
CharIsByte pour
'mbFrequency' 'mbSounderMode' 'mbDistanceScale' 'mbDepthScale' 'mbCQuality'
'mbCFlag' 'mbInterlacing' 'mbCompensationLayerMode' 'mbSQuality' 'mbSFlag'
'mbAntenna' 'mbBFlag' 'mbAFlag' 'mbVelProfilIdx'
%}

[varargin, X] = getPropertyValue(varargin, 'mbFrequency', []);
if ~isempty(X)
    %     b = set_value(b, 'mbFrequency', X);
    %     b = set_value(b, 'mbSonarFrequency', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbSounderMode', []);
if ~isempty(X)
    switch Sounder_type
        case {1; 2; 4} % EM12D, EM12S, EM100
        otherwise
            X = X - 1;
    end
    b = set_value(b, 'mbSounderMode', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbReferenceDepth', []);
if ~isempty(X)
    b = set_value(b, 'mbReferenceDepth', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbDynamicDraught', []);
if ~isempty(X)
    b = set_value(b, 'mbDynamicDraught', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbTide', []);
if ~isempty(X)
    b = set_value(b, 'mbTide', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbSoundVelocity', []);
if ~isempty(X)
    b = set_value(b, 'mbSoundVelocity', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbHeading', []);
if ~isempty(X)
    b = set_value(b, 'mbHeading', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbRoll', []);
if ~isempty(X)
    b = set_value(b, 'mbRoll', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbPitch', []);
if ~isempty(X)
    b = set_value(b, 'mbPitch', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbTransmissionHeave', []);
if ~isempty(X)
    b = set_value(b, 'mbTransmissionHeave', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbVerticalDepth', []);
if ~isempty(X)
    b = set_value(b, 'mbVerticalDepth', abs(X(:) ./ mbDepthScale(:)));
end

[varargin, X] = getPropertyValue(varargin, 'mbCQuality', []);
if ~isempty(X)
    %     b = set_value(b, 'mbCQuality', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbCFlag', []);
if isempty(X)
    X = ones(nbAntennaNbr, N) * 2;
end
b = set_value(b, 'mbCFlag', X);

[varargin, X] = getPropertyValue(varargin, 'mbBFlag', []);
if isempty(X)
    X = ones(1, nbBeam) * 2;
end
b = set_value(b, 'mbBFlag', X);

[varargin, X] = getPropertyValue(varargin, 'mbAFlag', []);
if isempty(X)
    X = ones(1, nbAntennaNbr) * 2;
end
b = set_value(b, 'mbAFlag', X);

[varargin, X] = getPropertyValue(varargin, 'mbInterlacing', []);
if ~isempty(X)
    %     b = set_value(b, 'mbInterlacing', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbSamplingRate', []);
if ~isempty(X)
    b = set_value(b, 'mbSamplingRate', X);
end

[varargin, X] = getPropertyValue(varargin, 'mbCompensationLayerMode', []);
if ~isempty(X)
    %     b = set_value(b, 'mbCompensationLayerMode', X);
end

%% Ecriture des layers

if (length(Layers) >= 4) && ~isempty(Layers{4})
    b = set_scale_value(b, 'mbSoundingBias', [], Layers{4});
end

if (length(Layers) < 5) || isempty(Layers{5})
    [varargin, DetectionType] = getPropertyValue(varargin, 'DetectionType', []); %#ok<ASGLU>
    if isempty(DetectionType)
        QF = ones(size(Layers{3}), 'uint8');
        b = set_scale_value(b, 'mbSQuality', [], QF);
    else
        QF = zeros(size(Layers{3}), 'uint8');
        QF(DetectionType.Image(:,:) == 1) = 1;
        QF(DetectionType.Image(:,:) == 2) = 255;
        b = set_scale_value(b, 'mbSQuality', [], QF);
    end
elseif length(Layers) >= 5
    b = set_scale_value(b, 'mbSQuality', [], Layers{5});
end

if length(Layers) >= 6
    b = set_scale_value(b, 'mbSFlag', [], Layers{6});
end

if (length(Layers) >= 7) && ~isempty(Layers{7})
    b = set_scale_value(b, 'mbAcrossBeamAngle', [], Layers{7});
end

if (length(Layers) >= 8) && ~isempty(Layers{8})
    b = set_scale_value(b, 'mbAzimutBeamAngle', [], Layers{8});
end

%% Traitement particulier de mbDepth

% b = set_valVarAtt(b, 'mbDepthScale', 'scale_factor', mbDepthScale_scale_factor); % ajout GLT
b = set_scale_value(b, 'mbDepth', -mbDepthScale, Layers{3});
b = set_value(b, 'mbDepthScale', mbDepthScale); % ajout GLT

%% Traitement particulier de mbAcrossDistance et mbAlongDistance

b = set_valVarAtt(b, 'mbDistanceScale', 'scale_factor', mbDistanceScale_scale_factor);

b = set_scale_value(b, 'mbAcrossDistance', mbDistanceScale, Layers{2});

b = set_value(b, 'mbDistanceScale', mbDistanceScale);
b = set_scale_value(b, 'mbAlongDistance', mbDistanceScale, Layers{1}); %#ok<NASGU>


function [mbDepthScale, mbDepthScale_scale_factor] = scale_factor_Depth(z)
mbDepthScale_scale_factor = 0.01;
mbDepthScale = ones(1, size(z,1)) * mbDepthScale_scale_factor;


function [mbDepthScale, mbDepthScale_scale_factor] = scale_factor_Distance(z)
Z_MAX = max(abs(z(:)));

if Z_MAX <= 327
    mbDepthScale_scale_factor = 0.01;
elseif Z_MAX <= 655
    mbDepthScale_scale_factor = 0.02;
elseif Z_MAX <= 1638
    mbDepthScale_scale_factor = 0.05;
elseif Z_MAX <= 3276
    mbDepthScale_scale_factor = 0.1;
elseif Z_MAX <= 6553
    mbDepthScale_scale_factor = 0.2;
elseif Z_MAX <= 16383
    mbDepthScale_scale_factor = 0.5;
elseif Z_MAX <= 32767
    mbDepthScale_scale_factor = 1.;
elseif Z_MAX <= 65534
    mbDepthScale_scale_factor = 2.;
elseif Z_MAX <= 163835
    mbDepthScale_scale_factor = 5.;
else
    mbDepthScale_scale_factor = 10.;
end
mbDepthScale = ones(1, size(z,1)) * mbDepthScale_scale_factor;


%{
function [mbDepthScale, mbDepthScale_scale_factor] = scale_factor_Range(z)
Z_MAX = max(abs(z(:)));
if (Z_MAX <= 0.0655)
mbDepthScale_scale_factor = 0.000000001;
elseif (Z_MAX <= 0.1307)
mbDepthScale_scale_factor = 0.000000002;
elseif (Z_MAX <= 0.3275)
mbDepthScale_scale_factor = 0.000000005;
elseif (Z_MAX <= 0.6553)
mbDepthScale_scale_factor = 0.00000001;
elseif (Z_MAX <= 1.3071)
mbDepthScale_scale_factor = 0.00000002;
elseif (Z_MAX <= 3.2767)
mbDepthScale_scale_factor = 0.00000005;
elseif (Z_MAX <= 6.553)
mbDepthScale_scale_factor = 0.0000001;
elseif (Z_MAX <= 13.107)
mbDepthScale_scale_factor = 0.0000002;
elseif (Z_MAX <= 32.767)
mbDepthScale_scale_factor = 0.0000005;
elseif (Z_MAX <= 65.535)
mbDepthScale_scale_factor = 0.000001;
elseif (Z_MAX <= 131.07)
mbDepthScale_scale_factor = 0.000002;
elseif (Z_MAX <= 327.67)
mbDepthScale_scale_factor = 0.000005;
elseif (Z_MAX <= 655.35)
mbDepthScale_scale_factor = 0.00001;
elseif (Z_MAX <= 1310.7)
mbDepthScale_scale_factor = 0.00002;
elseif (Z_MAX <= 3276.7)
mbDepthScale_scale_factor = 0.00005;
elseif (Z_MAX <= 6553.5)
mbDepthScale_scale_factor = 0.0001;
elseif (Z_MAX <= 13107.)
mbDepthScale_scale_factor = 0.0002;
elseif (Z_MAX <= 32767.)
mbDepthScale_scale_factor = 0.0005;
else
mbDepthScale_scale_factor = 0.001;
end
mbDepthScale = ones(1, size(z,1)) * mbDepthScale_scale_factor;
%}


function b = set_scale_value(b, nomVar, Scale, Image)

if numel(Image) == 0
    return
end

try
    value = Image(:,:);
    if isempty(value)
        return
    end
    if ~isempty(Scale)
        for i=1:length(Scale)
            value(i,:) = round(double(value(i,:)) / double(Scale(i)));
        end
    else
        whos value
    end
    b = set_value(b, nomVar, value);
    
catch %#ok<CTCH>
    nomFicNetcdf = get(b, 'fileName');
    if exist(nomFicNetcdf, 'file')
        idFile = netcdf.open(nomFicNetcdf, 'NC_WRITE');
        if isempty(idFile)
            return
        end
    else
        return
    end
    
    str1 = sprintf('Filling variable "%s"', nomVar);
    str2 = sprintf('Filling variable "%s"', nomVar);
    N = size(Image,1);
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for i=1:N
        my_waitbar(i, size(Image,1), hw)
        if isempty(Scale)
            b = set_value(b, nomVar, Image(i,:), 'sub1', i, 'fid', idFile);
        else
            value = round(double(Image(i,:)) / double(Scale(i)));
            b = set_value(b, nomVar, value, 'sub1', i);
        end
    end
    my_close(hw)
    netcdf.close(idFile);
end
