function this = create_compensationCurveFromNewBscorrAndAppliedDiagTx(this)

%% Renommage de la courbe

for k=1:length(this.CourbesStatistiques(end).bilan{1})
    this.CourbesStatistiques(end).bilan{1}(k).titreV       = 'bscorr';
    strLegend = [this.CourbesStatistiques(end).bilan{1}(k).nomZoneEtude ' - bscorr'];
    this.CourbesStatistiques(end).bilan{1}(k).nomZoneEtude = strLegend;
end

%% Plot de la courbe

plotCourbesStats(this, 'Last', 'Stats', 0, 'Tag', 'DiagTx');

%% Compute Compensation curve

DiagTxInPlace = get_EmissionModelsConstructeur(this.Sonar.Desciption);

sectorsBscorr = this.CourbesStatistiques(end).bilan{1};
nbSectors = length(sectorsBscorr);
clear ySample
for k=nbSectors:-1:1
    xCurve = sectorsBscorr(k).x;
    if isempty(xCurve)
        continue
    end
    
    kSecteur = sectorsBscorr(k).numVarCondition;
    
    %{
    % A l'essai : pas bon car bug calcul "Compensation"
    xMin = min(xCurve(1),   DiagTxInPlace(k).XData(1));
    xMax = max(xCurve(end), DiagTxInPlace(k).XData(end));
    step = xCurve(2) - xCurve(1);
    xCurve = xMin:step:xMax;
    %}
    
    xSample = XSample('name', 'Beam pointing angles', 'data', xCurve);
    
    sectorsInPlace{k}    = DiagTxInPlace(kSecteur).compute('x', xCurve);
    BSCorrModel          = sectorsBscorr(k).model.compute('x', xCurve);
    CompensationModel{k} = BSCorrModel - sectorsInPlace{k};
    Compensation         = sectorsBscorr(k).y(:)' - sectorsInPlace{k};
    
    % inPlace Black Signal
    inPlaceBlackSample(1) = YSample('data', sectorsBscorr(k).y, 'unit', 'dB', 'Color', 'b');
    inPlaceBlackSample(2) = YSample('data', BSCorrModel, 'unit', 'dB', 'Color', 'r');
    inPlaceBlackSample(3) = YSample('data', sectorsInPlace{k}, 'unit', 'dB', 'Color', 'k');
    
    SignaName = sprintf('InPlace (black), Stats(blue), BscorrModel(red),\n%s', strLegend);
    inPlaceBlackSignal = ClSignal('Name', SignaName, 'xSample', xSample, 'ySample', inPlaceBlackSample);
    
    % compModel Red Comp Blue Signal
    compModelRedCompBlueSample(1) = YSample('data', CompensationModel{k}, 'unit', 'dB', 'Color', 'r');
    compModelRedCompBlueSample(2) = YSample('data', Compensation, 'unit', 'dB', 'Color', 'b');
    compModelRedCompBlueSignal = ClSignal('Name', 'CompModel (red),  Comp (blue)', 'xSample', xSample, 'ySample', compModelRedCompBlueSample);
    
    % compModel Comp Signal
    compModelCompSample = YSample('data', CompensationModel{k} - Compensation, 'unit', 'dB');
    compModelCompSignal = ClSignal('Name', 'CompModel - Comp', 'xSample', xSample, 'ySample', compModelCompSample);
    
    % Signal Container
    signalList = [inPlaceBlackSignal, compModelRedCompBlueSignal compModelCompSignal];
    signalContainerName = sprintf('Sector %d', k);
    signalContainer(k) = SignalContainer('signalList', signalList, 'name', signalContainerName);
end

signalContainer.plot;
% a = SignalDialog(signalContainer, 'Title', 'sigCurves');
% a.openDialog();

%% Bscorr DiagTx

CourbesStatistiquesOut = this.CourbesStatistiques(end);
for k=1:nbSectors
    CourbesStatistiquesOut.bilan{1}(k).y = CompensationModel{k};
    CourbesStatistiquesOut.bilan{1}(k).model  = [];
    CourbesStatistiquesOut.bilan{1}(k).titreV = 'Compensation';
    CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude = strrep(CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude, ' - bscorr', '');
    CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude = [CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude ' - comp'];
end
% plot_CourbesStats(CourbesStatistiquesOut, 'Stats', 0);
this.CourbesStatistiques = [this.CourbesStatistiques(:); CourbesStatistiquesOut(:)];

%% InPlace TxDiag

CourbesStatistiquesOut = this.CourbesStatistiques(end);
for k=1:nbSectors
    CourbesStatistiquesOut.bilan{1}(k).y = sectorsInPlace{k};
    CourbesStatistiquesOut.bilan{1}(k).model  = [];
    CourbesStatistiquesOut.bilan{1}(k).titreV = 'In place';
    CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude = strrep(CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude, ' - bscorr', '');
    CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude = strrep(CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude, ' - comp', '');
    CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude = [CourbesStatistiquesOut.bilan{1}(k).nomZoneEtude ' - InPlace'];
end
% plot_CourbesStats(CourbesStatistiquesOut, 'Stats', 0);
this.CourbesStatistiques = [this.CourbesStatistiques(:); CourbesStatistiquesOut(:)];

%% Réordonancement des courbes en inPlace, bscorr, compens

this.CourbesStatistiques(end-2:end) = this.CourbesStatistiques([end end-2 end-1]);
