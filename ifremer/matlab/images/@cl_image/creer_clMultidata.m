function [flag, obj] = creer_clMultidata(this, nomFicXml, indIndexType, indLayers, flagMultiTime)

flag = 0;

[~, nomLayer] = fileparts(nomFicXml);
% Cr�ation d'un objet de Type MultiData pour structurer
% la donn�e.
subDataLayer  = indLayers(indIndexType == 0);
subIndexLayer = indLayers(indIndexType == 1);
objDataLayers = this(subDataLayer);
if iscolumn(objDataLayers)
    objDataLayers = objDataLayers';
end
objIndexLayers = this(subIndexLayer);
% Assemblage des layers par for�age du nom identique � chacun qui est
% le champ de groupage des layers.
% Ainsi : tous les layers de MNT disposent du m�me nom et sont index�s
% en temps (ex: KBMA).
if flagMultiTime
    for k=1:numel(objDataLayers)
        listIImageName{k} = objDataLayers(1,k).InitialFileName; %#ok<AGROW>
        if isempty(listIImageName{k})
            listIImageName{k} = 'Unknown'; %#ok<AGROW>
        end
    end
    
    % TODO : tester si listIImageName contient des []
    
    [u, ~, z] = unique(listIImageName);
    if numel(u) == numel(z)
        % Cas des MNT KBMA : nom des fichiers tous diff�rents �
        % l'origine.
        for k=1:numel(objDataLayers)
            objDataLayers(1,k) = set(objDataLayers(1,k), 'Name', nomLayer);
        end
    else
        % Cas de BOB : plusieurs seuils �tal�es le long de l'�chelle
        % des temps. On ajoute un label customis�.
        for k=1:numel(objDataLayers)
            % On �lude le nom du r�pertoire et Extension pour le cas o� le
            % nom soit celui d'un fichier.
            [~, nomFicSeul, ~] = fileparts(u{z(k)});
            DataTypeName       = get_DataTypeName(objDataLayers(1,k));
            objDataLayers(1,k) = set(objDataLayers(1,k), 'Name', [DataTypeName '_' nomFicSeul]);
        end
    end
end

% Cr�ation de l'objet MultiData
sz = size(objDataLayers);
% Pas de layer Index si l'objet est vide.
if any(size(objDataLayers) == 0)
    DimDataLayer = [];
else
    for k=1:ndims(objDataLayers)
        DimDataLayer(k).Name  = 'Unknown'; %#ok<AGROW>
        DimDataLayer(k).Value = sz(k); %#ok<AGROW>
    end
end
sz = size(objIndexLayers);
% Pas de layer Index si l'objet est vide.
if any(size(objIndexLayers) == 0)
    DimDataIndex = [];
else
    for k=1:ndims(objIndexLayers)
        DimDataIndex(k).Name  = 'Unknown'; %#ok<AGROW>
        DimDataIndex(k).Value = sz(k); %#ok<AGROW>
    end
end
obj = cl_multidata('DataLayer', objDataLayers, ...
    'DataIndex', objIndexLayers,...
    'DimDataLayer', DimDataLayer, ...
    'DimDataIndex', DimDataIndex);

if isempty(obj)
    return
end

flag = 1;
