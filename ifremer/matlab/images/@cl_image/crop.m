function [this, flag, Subx, Suby] = crop(this, varargin)

flag = 0;
Subx = {};
Suby = {};

for k=1:numel(this)
    [XLim, YLim] = getXLimYLimForCrop_image(this(k), varargin{:});
    if isempty(XLim)
        this = [];
        return
    end
    [this(k), flag, subx, suby] = extraction(this(k), 'XLim', XLim, 'YLim', YLim);
    Subx{k} = subx{1}; %#ok<AGROW> 
    Suby{k} = suby{1}; %#ok<AGROW> 
end

