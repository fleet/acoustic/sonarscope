% Crosscorrelation between two images
%
% Syntax
%   b = crossCorrelationByFFT(a, b, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%   b : One instance of cl_image, this is the reference image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance of cl_image containing the crosscorrelation image
%
% Examples
%   I = Lena;
%   suby = 100:200;
%   subx = 100:200;
%   a = cl_image('Image', I(suby,subx), 'Name', 'Image 1', ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   imagesc(a);
%
%   b = cl_image('Image', I(subx+5,suby+3), 'Name', 'Image 1', ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   imagesc(b);
%
%   c = crossCorrelationByFFT(a, b);
%   imagesc(c); grid on;
%   [ValMax, x, y, ix, iy] = searchMaxValue(c)
%
% See also my_xcorr2_byFFT cl_image/crossCorrelation Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = crossCorrelationByFFT(this, I2, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('crossCorrelationByFFT'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = crossCorrelationByFFT_unit(this(k), I2, varargin{:});
end
my_close(hw)


function that = crossCorrelationByFFT_unit(this, I2, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx,:);
I = singleUnlessDouble(I, this.ValNaN);
[C, x, y, dC, dL]  = my_xcorr2_byFFT(I, I2.Image);

%% Output image

ImageName = code_ImageName(this, 'Append', 'crossCorrelationByFFT');
Comments = sprintf('Max value for dx=%d, dy=%f   YStepMetric=%f', dC, dL, this.YStepMetric);
that = cl_image('Image', C, 'Name', ImageName, 'x', x, 'y', y, ...
    'ColormapIndex', 3, 'XUnit', 'pixels', 'YUnit', 'pixels', ...
    'DataType', cl_image.indDataType('Correlation'), ...
    'GeometryType', cl_image.indGeometryType('OrthoYX'), 'Comments', Comments);
