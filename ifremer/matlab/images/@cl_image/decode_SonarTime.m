function [flag, SplitTime] = decode_SonarTime(this, NomFicPoints) %#ok

SplitTime = [];
date = [];
heure = [];
fid = fopen(NomFicPoints, 'r');
if fid == -1
    str = sprintf('File %s\ndoesn''t exist. This file is filled when you use "Point&Click" and click button "Save Point In " near Close button on Pixel information window.', NomFicPoints);
    my_warndlg(str, 1);
    flag = 0;
    return
end

while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    tline = rmblank(tline);
    mots = strsplit(tline);
    if strcmp(mots{1}, 'SonarTime') % Ligne officielle
        date(end+1)  = dayStr2Ifr(mots{3}); %#ok
        heure(end+1) = hourStr2Ifr(mots{4}); %#ok
    elseif strcmp(mots{1}, '>') % Ligne Provenant plus ou moins de Caraibes
        date(end+1)  = dayStr2Ifr(mots{2}); %#ok
        heure(end+1) = hourStr2Ifr(mots{3}); %#ok
    elseif length(mots) == 2 % Aucune entete
        date(end+1)  = dayStr2Ifr(mots{1}); %#ok
        heure(end+1) = hourStr2Ifr(mots{2}); %#ok
    end
end
fclose(fid);

if ~isempty(date)
    SplitTime = cl_time('timeIfr', date, heure);
end
flag = 1;
