function [this, Carto, persistent_Carto] = define_carto(this, Carto, nomDir, memeReponses, persistent_Carto, varargin)

[varargin, CartoAuto] = getPropertyValue(varargin, 'CartoAuto', 0); %#ok<ASGLU>

C0 = cl_carto([]);

if isempty(Carto)
    if ~memeReponses
        [nomDirInitial, nomFicInitial] = fileparts(this.InitialFileName);
        nomFicCartoDefault = fullfile(nomDirInitial, ['Carto_' nomFicInitial '.xml']);
        if exist(nomFicCartoDefault, 'file')
            str1 = sprintf('Le fichier "%s" est interprété directement', nomFicCartoDefault);
            str2 = sprintf('File "%s" is used by default.', nomFicCartoDefault);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
            Carto = import_xml(C0, nomFicCartoDefault);
        else
            liste = listeFicOnDir(nomDir, '.xml', 'Filtre', '_Directory', 'FiltreExclude', 'GeoswathParameters_Folder');
            switch length(liste)
                case 0
                    nomFicCartoDefault = '';
                case 1
                    nomFicCartoDefault = liste{1};
                otherwise
                    str1 = 'Plusieurs fichiers implicites existent, sélectionnez en un.';
                    str2 = 'Select one default file please.';
                    [rep, flag] = my_listdlg(Lang(str1,str2), liste, 'SelectionMode', 'Single');
                    if ~flag
                        return
                    end
                    nomFicCartoDefault = liste{rep};
            end
            
            if exist(nomFicCartoDefault, 'file')
                %                 str1 = sprintf('Le fichier "%s" est interprété directement', nomFicCartoDefault);
                %                 str2 = sprintf('File "%s" is used by default.', nomFicCartoDefault);
                %                 my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
                Carto = import_xml(C0, nomFicCartoDefault);
                if isempty(Carto)
                    Carto = getDefinitionCarto(this, 'NoGeodetic', 0);
                end
            else
                Carto = getDefinitionCarto(this, 'NoGeodetic', 0, 'CartoAuto', CartoAuto);
            end
        end
        persistent_Carto = Carto;
    else
        Carto = persistent_Carto;
    end
end
this = set_Carto(this, Carto);
