% Discrete Laplacian
%
% Syntax
%   b = del2(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a)
%
%   b = del2(a);
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = del2(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('del2'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = del2_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = del2_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    I = singleUnlessDouble(I, this.ValNaN);
    Image(:,:,k) = del2(I);
end

%% Output image

Unit = [this.Unit ' / ' this.XUnit]; % TODO : améliorer cela (same as normeGradient ...)
that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', 'del2', ...
    'ValNaN', NaN, 'Unit', Unit);

