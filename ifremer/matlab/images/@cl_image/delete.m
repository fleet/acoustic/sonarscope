% Destruction des images en memoire virtuelle
%
% Syntax
%   b = delete(a)
%
% Input Arguments
%   a : Instances of cl_image
%
% Output Arguments
%   c : Instances of cl_image without images
%
% Remarks : This function is used only at SonarScope exit in order to free
%           the cl_memmapfile instances. This function do not have to be
%           used except in this particular situation (in cli_image/calback)
%
% See also cli_image/calback Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = delete(this)

for k=1:length(this)
    this(k).Image = 'I am going to die';
end

