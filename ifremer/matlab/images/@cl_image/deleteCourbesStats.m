% Destruction d'une courbe de statistiques
%
% Syntax
%   deleteCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = deleteCourbesStats(this, varargin)

if isempty(this.CourbesStatistiques)
    message_NoCurve(this)
    return
end

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques)); %#ok<ASGLU>

if isempty(sub)
    return
end

this.CourbesStatistiques(sub) = [];
