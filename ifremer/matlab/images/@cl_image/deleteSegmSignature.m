% Destruction d'une courbe de statistiques
%
% Syntax
%   deleteSegmSignature(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = deleteSegmSignature(this, varargin)

if isempty(this.Texture)
    message_NoCurve(this)
    return
end

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.Texture)); %#ok<ASGLU>

if isempty(sub)
    return
end

this.Texture(sub) = [];
