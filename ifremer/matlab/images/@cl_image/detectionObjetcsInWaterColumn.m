function Points = detectionObjetcsInWaterColumn(this, DetectedRangeInSamples, Threshold, Latitude, Longitude, ...
    Heading, Carto, TimePing, iPing, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

Heading = double(Heading);

I = this.Image(suby,subx,:);
for k=1:length(subx)
    %     iFin = find(isnan(I(:,k)),1,'first');
    %     if ~isempty(iFin)
    %         delta = iFin - DetectedRangeInSamples(k);
    %         I(max(1,DetectedRangeInSamples(k)-delta):end,k) = NaN;
    %     end
    I(max(1,DetectedRangeInSamples(k)-20):end,k) = NaN;
end

I(I < Threshold) = NaN;

fe = this.Sonar.SampleBeamData.SampleRate;
Points.Celerite = this.Sonar.SampleBeamData.SoundVelocity;
try
    Angles = this.Sonar.SampleBeamData.TxAngle;
catch
    Angles = this.Sonar.SampleBeamData.RxAnglesEarth_WC;
end

[indY, indX] = find(~isnan(I));
Points.Angle          = Angles(indX);
Points.RangeInSamples = indY';
Points.Energie = (I(sub2ind(size(I), indY, indX)))';
D = Points.Celerite * indY / (2*fe);
Points.AcrossDistance =  D' .* sind(Points.Angle);
Points.Z              = -D' .* cosd(Points.Angle);

E2 = get(Carto, 'Ellipsoide.Excentricite') ^ 2;
A  = get(Carto, 'Ellipsoide.DemiGrandAxe');
% Calcul direct en LatLong
% Document Cartolob++ / Manuel de r�f�rence avril 2004 Paragraphe 7.1.2
sinLN2 = sind(Latitude) .^ 2;
E2MoinssinLN2 = 1 - E2 * sinLN2;
NORM = A ./ sqrt(E2MoinssinLN2);
RAY0 = NORM * (1 - E2) ./ E2MoinssinLN2;
SC = sind(Heading);
CC = cosd(Heading);

% DL = double(AlongDistance.Image( subyAlong(k),  subxAlong));
if isfield(this.Sonar.SampleBeamData, 'AlongDist') && ~isempty(this.Sonar.SampleBeamData.AlongDist)
    DL = double(this.Sonar.SampleBeamData.AlongDist(indX));
else
    DL = 0;
end

DT = double(Points.AcrossDistance);
DL = (DL(:))';
DT = (DT(:))';
Points.Latitude  = Latitude  + (DL * CC - DT * SC) .* ((180/pi) ./ RAY0);
Points.Longitude = Longitude + (DL * SC + DT * CC) .* ((180/pi) ./ NORM ./ cosd(Latitude));

Points.iPing = iPing;
Points.EnergieTotale = sum(Points.Energie);
Points.Date  = TimePing.date;
Points.Heure = TimePing.heure;

% figure(5675); plot(Points.AcrossDistance, Points.Z, '*'); grid on; axis equal;
% figure(56756); plot(Points.Longitude, Points.Latitude, '*'); grid on; axis equal; hold on;

% Attention : il ne faut pas supprimer ce calcul car c'est utilis� dans le
% WC_plotEchoes, �a peut �tre utile en mode d�bugage
[Points.XCoor, Points.YCoor] = latlon2xy(Carto, Points.Latitude, Points.Longitude);
