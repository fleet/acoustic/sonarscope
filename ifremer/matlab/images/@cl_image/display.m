% Prints the summary of the instance(s)
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   InfoSonar  : 1 : Information of the image only
%                2 : Information of the sonar data only
%                3 : Information of both Image And Sonar data : (3 par defaut)
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%   a
%   [a a a]
%
% See also cl_image/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : InfoSonar est apparemment inutile pour cette fonction, � confirmer.

function display(this, varargin) %#ok<DISPLAY>

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this, varargin{:}));
