% Convert image to double precision
%
% Syntax
%   b = double(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a);
%
%   b = double(a);
%   imagesc(b);
%
% See also cl_image/singleUnlessDouble Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = double(this, varargin)
that = process_function_type1(this, @double, varargin{:});
