% Downsampling of an image
%
% Syntax
%   b = downsize(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   pasx : Reduction ratio in x (Default : 2)
%   pasy : Reduction ratio in y (Default : 2)
%   or
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a);
%
%   b = downsize(a);
%   imagesc(b)
%
%   b = downsize(a, 'pasx', 4, 'pasy', 4);
%   imagesc(b)
%
% See also cl_image/resize cl_image/extraction Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = downsize(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('downsize'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    X = downsize_unit(this(k), varargin{:});
    if isempty(X)
        that = [];
        return
    end
    that(k) = X;
end
my_close(hw, 'MsgEnd')


function this = downsize_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, pasx]        = getPropertyValue(varargin, 'pasx', []);
[varargin, pasy]        = getPropertyValue(varargin, 'pasy', []);
[varargin, Method]      = getPropertyValue(varargin, 'Method', []);
[varargin, flagFillNaN] = getPropertyValue(varargin, 'FillNaN', 0); %#ok<ASGLU>

if isempty(pasx)
    pasx = 2;
end

if isempty(pasy)
    pasy = 2;
end

if testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage')
    if pasy ~= 1
        my_warndlg('This reduction can be done only if the step in x is equal to 1.', 1);
        this = [];
        return
    end
end

if isempty(Method)
    Method = 'bilinear'; %'mean';%nearest
end

if ~iscell(Method) && strcmp(Method, 'mean') && ((pasx < 1) || (pasy < 1))
    Method = 'bilinear';
end

%% Appel de la fonction de filtrage

% Filtrage des NaN sur les bandes (1 ou 3 pour RGB).
if isnan(this.ValNaN)
    pppp = isnan(this.Image(:,:,:));
else
    pppp = this.Image(:,:,:) == this.ValNaN;
end
nbSlides = this.nbSlides;
pppp = (sum(pppp, nbSlides) == nbSlides);

for k=nbSlides:-1:1
    %     r = floor(this.nbRows / pasy);
    %     c = floor(this.nbColumns / pasx);
    r = floor(length(suby) / pasy);
    c = floor(length(subx) / pasx);
    
    K = this.Image(suby(1:floor(r*pasy)),subx(1:floor(c*pasx)), k);
    
    if this.ImageType ~= 3
        if isa(K, 'uint8') % Rajout� par JMA le 22/05/2011 : supprime les bords noirs : attention si jamais des effets de
            % bords sont constat�s il faudrait peut-�tre v�rifier si on est en RGB (ne pas faire cette instruction si image ind�d�e ?
            K = single(K);
        else
            if ~isa(K, 'double')
                K = single(K);
            end
        end
        % Traitement des NaN
        if (nbSlides == 1) && (this.ImageType ~= 3)
            K(K == this.ValNaN) = NaN;
        else
            dummy    = pppp(suby(1:(r*pasy)),subx(1:(c*pasx)));
            K(dummy) = NaN;
        end
        if flagFillNaN
            K = fillNaN_mean(K, [pasy+1 pasx+1]);
        end
    end
    
    switch this.DataType
        case cl_image.indDataType('Segmentation')
            I(:,:,k) = imresize_median(K, [r c]);
        case cl_image.indDataType('Mask')
            I(:,:,k) = imresize_median(K, [r c]);
        otherwise
            I(:,:,k) = imresize(K, [r c], Method);
    end
end

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, I);

%% Mise � jour de coordonn�es

deltax  = get(this, 'XStep');
deltaxp = deltax * pasx;
N = size(I,2) + 2;
x = (this.x(subx(1)) - deltax/2 + deltaxp/2):deltaxp:(this.x(subx(1)) + deltaxp/2 + N*deltaxp);
x = x(1:size(I,2));

deltay  = get(this, 'YStep');
deltayp = deltay * pasy;
N = size(I,1) + 2;

if this.y(end) > this.y(1)
    y = (this.y(suby(1)) - deltay/2 + deltayp/2) : deltayp : (this.y(suby(1)) + deltayp/2 + N*deltayp);
else
    deltayp = -deltayp;
    y = (this.y(suby(1)) - deltay/2 + deltayp/2) : deltayp : (this.y(suby(1)) + deltayp/2 + N*deltayp);
end
y = y(1:size(I,1));

this = majCoordonneesXY(this, x, y);

% subx = linspace(subx(1), subx(end), size(this.Image, 2));
% suby = linspace(suby(1), suby(end), size(this.Image, 1));
% this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.ValNaN = NaN;

%% Compl�tion du titre

Append = ['downsize' num2strCode([pasy pasx])];
this = update_Name(this, 'Append', Append);

if testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'), 'noMessage')
%     Apparemment n�cessaire pour donn�es SBP MAYOBS15 mais pas pour EK80 !!!
%     this.Sonar.SampleFrequency = this.Sonar.SampleFrequency / pasx;
end

% function y = boxKernel(x)
% y = (-0.5 <= x) & (x < 0.5);
