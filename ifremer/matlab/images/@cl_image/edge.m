% Edge detection
%
% Syntax
%  b = edge(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Method : {'Sobel'} | 'Prewitt' | 'Roberts' | 'Canny'
%   subx   : Sub-sampling in abscissa
%   suby   : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image('Image', Lena, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = edge(a);
%   imagesc(b);
%
%   b = edge(a, 'Method', 'Prewitt');
%   imagesc(b);
%
%   b = edge(a, 'Method', 'Roberts');
%   imagesc(b);
%
%   b = edge(a, 'Method', 'Canny');
%   imagesc(b);
%
% See also edge fspecial Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = edge(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('edge'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = edge_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = edge_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Method] = getPropertyValue(varargin, 'Method', 'Sobel'); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx,:);
I = uint8(quantify(I, 'rangeIn', this.CLim));
I = edge(I, Method);

%% Output image

Parameters = getHistoryParameters(Method);
that = inherit(this, I,'subx',  subx, 'suby', suby, 'AppendName', 'edge', 'CLim', [0 1], 'ColormapIndex', 2, 'Parameters', Parameters);

