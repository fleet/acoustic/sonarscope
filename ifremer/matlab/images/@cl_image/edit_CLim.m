% Graphical User Interface to edit CLim (contrast enhancement)
%
% Syntax
%   [flag, CLim] = edit_CLim(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   CLim : Initialisation values
%
% Output Arguments
%   CLim : [ValMin ValMax] : A two-element vector that limits the range of data values of the image when displayed by imagesc or SonarScope
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a)
%   [flag, CLim] = edit_CLim(a)
%   a.CLim = CLim;
%   imagesc(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, CLim] = edit_CLim(this, varargin)

[varargin, CLim] = getPropertyValue(varargin, 'CLim', this.CLim); %#ok<ASGLU>

str1 = 'Définission des nouvelles valeur de contraste';
str2 = 'Define new values for contrast';
Title = Lang(str1,str2);

p    = ClParametre('Name', 'Min', 'Value', CLim(1));
p(2) = ClParametre('Name', 'Max', 'Value', CLim(2));
a = StyledSimpleParametreDialog('params', p, 'Title', Title);

a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
CLim = a.getParamsValue;
