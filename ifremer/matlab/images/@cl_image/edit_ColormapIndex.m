% Graphical User Interface to edit ColormapIndex
%
% Syntax
%   [flag, ColormapIndex] = edit_ColormapIndex(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   ColormapIndex : Initialisation value
%
% Output Arguments
%   ColormapIndex : Index of the colormap of the image when displayed :  ('1=User' | '2=gray' | '3=jet' | '4=cool' | '5=hsv' | '6=hot' | '7=bone' | '8=copper' | '9=pink' | '10=flag' | '11=CNES' | '12=CNESjet' | '13=ColorSea' | '14=ColorEarth' | '15=NIWA1' | '16=NIWA2' | '17=JetSym' | '18=Haxby' | '19=Becker' | '20=Catherine' | '21=Seismics' | '22=SeismicsSym')
%
% Examples
%   [flag, ColormapIndex] = edit_ColormapIndex(cl_image)
%
% Author : JMA
% See also Authors strColormapIndex ColormapIndex indColormapIndex

function [flag, ColormapIndex] = edit_ColormapIndex(this, varargin)

[varargin, ColormapIndex] = getPropertyValue(varargin, 'ColormapIndex', this.ColormapIndex); %#ok<ASGLU>

str1 = 'Sélectionnez la table de couleur';
str2 = 'Select the colormap';
[ColormapIndex, flag] = my_listdlg(Lang(str1,str2), cl_image.strColormapIndex, 'InitialValue', ColormapIndex, 'SelectionMode', 'Single');
