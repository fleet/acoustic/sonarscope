% Graphical User Interface to edit DataType
%
% Syntax
%   [flag, DataType] = cl_image.edit_DataType(...)
%
% Name-Value Pair Arguments
%   DataType : Initialisation value
%
% Output Arguments
%   flag     : 1=OK, 0=KO
%   DataType : Data type number (see cl_image.strDataType)
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%   [flag, DataType] = cl_image.edit_DataType('DataType', a.DataType)
%     a.DataType = DataType;
%     a = update_Name(a);
%     imagesc(a)
%
% Author : JMA
% See also Authors strDataType DataType indDataType

function [flag, DataType] = edit_DataType(varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []);
[varargin, Title]    = getPropertyValue(varargin, 'Title',    []); %#ok<ASGLU>

if isempty(DataType)
    DataType = 1;
% elseif DataType < 0
%     DataType = -DataType;
end

if isempty(Title)
    str1 = 'Sélectionnez le DataType';
    str2 = 'Select DataType';
else
    str1 = sprintf('Sélectionnez les variables pour "%s"', Title);
    str2 = sprintf('Select DataType for "%s"', Title);
end
[DataType, flag] = my_listdlg(Lang(str1,str2), cl_image.strDataType, 'InitialValue', DataType, 'SelectionMode', 'Single');
