% Graphical User Interface to edit the image Unit
%
% Syntax
%   [flag, Unit] = edit_DataUnit(...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   DataType : Data type that indicates if it is bathymetry or reflectivity, ...
%
% Output Arguments
%   flag     : 1=OK, 0=KO
%   Unit : Unit of the image values
%
% Examples
%   a = cl_image.import_Prismed;
%   a.Unit
%   a.Unit = 'meters';
%   a.Unit
%   [flag, Unit] = cl_image.edit_DataUnit('DataType', a.DataType, 'Title', a.Name)
%
% Author : JMA
% See also Authors

function [flag, Unit] = edit_DataUnit(varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []);
[varargin, Title]    = getPropertyValue(varargin, 'Title',    []); %#ok<ASGLU>

Unit = cl_image.expectedUnitOfData(DataType);

if isempty(Title)
    str1 = 'Unit� de la mesure.';
    str2 = 'Unit of the data.';
else
    str1 = sprintf('Unit� de la mesure pour "%s"', Title);
    str2 = sprintf('Unit of the data for "%s"', Title);
end

[Unit, flag] = my_inputdlg(Lang({str1},{str2}), {Unit}, 'Interpreter', 'None');
if ~flag
    return
end
Unit = Unit{1};
