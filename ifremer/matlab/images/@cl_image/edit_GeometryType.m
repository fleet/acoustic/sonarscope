% Choix interactif de la géométrie
%
% Syntax
%   [flag, GeometryType] = cl_image.edit_GeometryType(...)
%
% Name-Value Pair Arguments
%   GeometryType : Valeur implicite
%
% Output Arguments
%   flag         : 1=OK , 0=KO
%   GeometryType : Type de geometry (Cf cl_image.strGeometryType)
%
% Examples
%   [flag, GeometryType] = cl_image.edit_GeometryType
%   [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', 3)
%
% Author : JMA
% See also Authors strGeometryType GeometryType indGeometryType

function [flag, GeometryType] = edit_GeometryType(varargin)

[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []);
[varargin, Title]        = getPropertyValue(varargin, 'Title',        []); %#ok<ASGLU>

if isempty(GeometryType)
    GeometryType = 1;
% elseif GeometryType < 0
%     GeometryType = -GeometryType;
end

if isempty(Title)
    str1 = 'Type de géométrie';
    str2 = 'Geometry type';
else
    str1 = sprintf('Type de géométrie pour "%s"', Title);
    str2 = sprintf('Geometry type for "%s"', Title);
end

[GeometryType, flag] = my_listdlg(Lang(str1,str2), cl_image.strGeometryType, 'InitialValue', GeometryType, 'SelectionMode', 'Single');
