% Graphical User Interface to edit ImageType
%
% Syntax
%   [flag, ImageType] = edit_ImageType(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   ImageType : Initialisation value
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   ImageType : Image type number
%
% Remarks : It is not recommanded to change the ImageType by your own.
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%   [flag, ImageType] = edit_ImageType(a)
%     a.ImageType = ImageType;
%     ImageType = a.ImageType
%
% Author : JMA
% See also Authors strImageType ImageType indImageType

function [flag, ImageType] = edit_ImageType(this, varargin)

% ImageType = [];
% 
% str1 = 'Il n''est pas recommand� de changer le type d''image. Etes-vous s�r de vouloir poursuivre ?';
% str2 = 'It is not recommanded to change the Image Type. Are you sure to go on ?';
% [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
% if ~flag
%     return
% end
% if rep == 2
%     flag = 0;
%     return
% end

[varargin, ImageType] = getPropertyValue(varargin, 'ImageType', this.ImageType);
[varargin, Title]     = getPropertyValue(varargin, 'Title',     []); %#ok<ASGLU>

if isempty(Title)
    str1 = 'S�lectionnez le type d''image';
    str2 = 'Select the Image Type';
else
    str1 = sprintf('S�lectionnez le type d''image "%s"', Title);
    str2 = sprintf('Select the Image Type"%s"', Title);
end

[ImageType, flag] = my_listdlg(Lang(str1,str2), this.strImageType, 'InitialValue', ImageType, 'SelectionMode', 'Single');
