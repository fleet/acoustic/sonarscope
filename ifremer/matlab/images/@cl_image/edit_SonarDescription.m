% Identification manuelle du sondeur
%
% Syntax
%   [flag, a] = edit_SonarDescription(a)
%
% Input Arguments 
%   a : instance de cl_image
%
% Examples
%   a = cl_image('Image', Lena)
%   [flag, a] = edit_SonarDescription(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = edit_SonarDescription(this)

SonarDescription = this.Sonar.Desciption;
if isempty(SonarDescription)
    SonarDescription = cl_sounder;
end
[flag, this.Sonar.Desciption] = selection(SonarDescription);
