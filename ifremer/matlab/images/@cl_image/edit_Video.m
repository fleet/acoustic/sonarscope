% Graphical User Interface to edit Video
%
% Syntax
%   [flag, Video] = edit_Video(a)
%
% Input Arguments
%   a : One instance of cl_image
%
% Name-Value Pair Arguments
%   Video : Initialisation value
%
% Output Arguments
%   Video : 1=Normal, 2=Inverse
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%     imagesc(a)
%   [flag, Video] = edit_Video(cl_image)
%     a.Video = Video;
%     imagesc(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Video] = edit_Video(this, varargin)

[varargin, Video] = getPropertyValue(varargin, 'Video', this.Video); %#ok<ASGLU>

str1 = 'Vid�o';
str2 = 'Video type';
[Video, flag] = my_listdlg(Lang(str1,str2), this.strVideo, 'InitialValue', Video, 'SelectionMode', 'Single');
