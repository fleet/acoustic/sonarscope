% Graphical User Interface to edit the X direction
%
% Syntax
%   [flag, XDir] = edit_XDir(this, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   XDir  : X direction
%   Title : Append the title of the GUI
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   XDir : X direction
%
% Remarks : It is not recommanded to change the ImageType by your own.
%
% Examples
%   a = cl_image('Image', Lena, 'YDir', 2, 'ColormapIndex', 2);
%   a.XDir
%   a.strXDir
%   a.strXDir{a.XDir}
%   imagesc(a);
%   a.XDir = 2;
%   imagesc(a);
%   [flag, XDir] = a.edit_XDir('Title', a.Name)
%   [flag, XDir] = edit_XDir(cl_image, 'XDir', 2)
%
% Author : JMA
% See also Authors strXDir XDir indXDir edit_XDir x YDir

function [flag, XDir] = edit_XDir(this, varargin)

[varargin, XDir]  = getPropertyValue(varargin, 'XDir',  this.XDir);
[varargin, Title] = getPropertyValue(varargin, 'Title', []); %#ok<ASGLU>

if isempty(Title)
    Title = 'X Direction';
else
    Title = sprintf('X Direction for "%s"', Title);
end

[XDir, flag] = my_listdlg(Title,  cl_image.strXDir, 'InitialValue', XDir, 'SelectionMode', 'Single');
