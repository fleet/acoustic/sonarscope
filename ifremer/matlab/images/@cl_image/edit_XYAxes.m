% Redifine the axis by hand
%
% Syntax
%   a = edit_XYAxes(a)
%
% Input Arguments 
%   a : instance de cl_image
%
% Examples
%   a = cl_image('Image', Lena)
%   a = edit_XYAxes(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = edit_XYAxes(this)
 
nomVar = {'Column1'; 'Row1'; 'X1'; 'Y1'; 'Column2'; 'Row2'; 'X2'; 'Y2'};
value = {'1'; '1'; num2str(this.x(1)); num2str(this.y(1))
    num2str(length(this.x)); num2str(length(this.y)); num2str(this.x(end)); num2str(this.y(end))};
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
 
% nomVar = {'X begin'; 'X end'; 'Y begin'; 'Y end'};
% value = {num2str(this.x(1)); num2str(this.x(end)); num2str(this.y(1)); num2str(this.y(end))};
% [value, flag] = my_inputdlg(nomVar, value);
% if ~flag
%     return
% end

% Format = get(0, 'Format');
% set(0, 'Format', 'long')
format long
c1 = str2num(value{1}); %#ok<ST2NM>
l1 = str2num(value{2}); %#ok<ST2NM>
x1 = str2num(value{3}); %#ok<ST2NM>
y1 = str2num(value{4}); %#ok<ST2NM>
c2 = str2num(value{5}); %#ok<ST2NM>
l2 = str2num(value{6}); %#ok<ST2NM>
x2 = str2num(value{7}); %#ok<ST2NM>
y2 = str2num(value{8}); %#ok<ST2NM>
% set(0, 'Format', Format)

if isnan(x1) || isnan(x2) || isnan(y1) || isnan(y2)
    str1 = 'Une valeur n''est pas interprétable, veuillez recommencer l''opération.';
    str2 = 'One value cannot be interpreted, please redo the the action.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

x = interp1([c1 c2], [x1 x2], 1:this.nbColumns);
y = interp1([l1 l2], [y1 y2], 1:this.nbRows);
% figure; plot(this.x - x); grid
% figure; plot(this.y - y); grid

this.x = x;
this.y = y;

this = majCoordonnees(this);
this.YDir = 1;
