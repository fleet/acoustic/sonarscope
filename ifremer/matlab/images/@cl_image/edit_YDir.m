% Graphical User Interface to edit the Y direction
%
% Syntax
%   [flag, YDir] = edit_YDir(this, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   YDir  : Y direction
%   Title : Append the title of the GUI
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   YDir : Y direction
%
% Remarks : It is not recommanded to change the ImageType by your own.
%
% Examples
%   a = cl_image('Image', Lena, 'YDir', 2, 'ColormapIndex', 2);
%   a.YDir
%   a.strYDir
%   a.strYDir{a.YDir}
%   imagesc(a);
%   a.YDir = 2;
%   imagesc(a);
%   [flag, YDir] = a.edit_YDir('Title', a.Name)
%   [flag, YDir] = edit_YDir(cl_image, 'YDir', 2)
%
% Author : JMA
% See also Authors strYDir YDir indYDir edit_YDir y XDir

function [flag, YDir] = edit_YDir(this, varargin)

[varargin, YDir]  = getPropertyValue(varargin, 'YDir',  this.YDir);
[varargin, Title] = getPropertyValue(varargin, 'Title', []); %#ok<ASGLU>

if isempty(Title)
    Title = 'Y Direction';
else
    Title = sprintf('Y Direction for "%s"', Title);
end

[YDir, flag] = my_listdlg(Title,  cl_image.strYDir, 'InitialValue', YDir, 'SelectionMode', 'Single');
