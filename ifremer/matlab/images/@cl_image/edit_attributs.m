% Graphical User Interface to edit the image attributs (Unit, XUnit, YUnit, TagSynchroX, TagSynchroY, TagSynchroContrast, ValNaN)
%
% Syntax
%   a = edit_attributs(a)
%
% Input Arguments 
%   a : instance de cl_image
%
% Examples
%   a = cl_image('Image', Lena)
%   a = edit_attributs(a)
%
% Author : JMA
% See also Authors


function this = edit_attributs(this)

if isempty(this.Unit)
    this.Unit = ' ';
end

if isempty(this.XUnit)
    this.XUnit = ' ';
end

if isempty(this.YUnit)
    this.YUnit = ' ';
end

if isempty(this.TagSynchroX)
    this.TagSynchroX = num2str(rand(1));
elseif isnumeric(this.TagSynchroX)
    this.TagSynchroX = num2str(this.TagSynchroX);
end

if isempty(this.TagSynchroY)
    this.TagSynchroY = num2str(rand(1));
elseif isnumeric(this.TagSynchroY)
    this.TagSynchroY = num2str(this.TagSynchroY);
end

if isempty(this.TagSynchroContrast)
    this.TagSynchroContrast = num2str(rand(1));
elseif isnumeric(this.TagSynchroContrast)
    this.TagSynchroContrast = num2str(this.TagSynchroContrast);
end
 
nomVar = {'Unit'; 'XUnit'; 'YUnit'; 'TagSynchroX'; 'TagSynchroY'; 'TagSynchroContrast'; 'ValNaN'};
value = {this.Unit; this.XUnit; this.YUnit; this.TagSynchroX; this.TagSynchroY; this.TagSynchroContrast; this.ValNaN};
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end

this.Unit               = value{1};
this.XUnit              = value{2};
this.YUnit              = value{3};
this.TagSynchroX        = value{4};
this.TagSynchroY        = value{5};
this.TagSynchroContrast = value{6};
this.ValNaN             = value{7};

if isempty(this.Unit)
    this.Unit = ' ';
end

if isempty(this.XUnit)
    this.XUnit = ' ';
end

if isempty(this.YUnit)
    this.YUnit = ' ';
end

if isempty(this.TagSynchroX)
    this.TagSynchroX = num2str(rand(1));
end

if isempty(this.TagSynchroY)
    this.TagSynchroY = num2str(rand(1));
end

if isempty(this.TagSynchroContrast)
    this.TagSynchroContrast = num2str(rand(1));
end
 