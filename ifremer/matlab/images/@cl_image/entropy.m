% Entropie de l'image
%
% Syntax
%   e = entropy(a)
% 
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   e : Entropie de l'image
%
% Examples 
%   for k=1:8
%       [I, label] = ImageSonar(k);
%       a(k) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   end
%   e = entropy(a)
%
% See also cl_image cl_image/mean Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function val = entropy(this)

for k=1:length(this)
    val(k) = this(k).StatValues.Entropy;%#ok
end
