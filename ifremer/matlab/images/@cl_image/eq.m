% Check if two instances are equal. Two instances are considered equal if
% they met all the here-under conditions :
%    - same size (nbRows, nbColumns, ...)
%    - same Unit, DataType, GeometryType, ImageType, SpectralStatus
%    - same XLabel, XUnit, x, YLabel, YUnit, y
%    - same values
%
% Syntax
%   flag = eq(a, b)
%
% Input Arguments
%   a : One cl_image instance
%   b : One cl_image instance
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Remarks : A message is displayed in case the instances are dirretent, it
%           gives the reason why the test is negative
%
% Examples
%     a = cl_image('Image', Lena);
%     b = cl_image('Image', Lena);
%  flag = eq(a, b)
%  flag = eq(a, -b)
%
% See also cl_image/ne Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = eq(this, a)

flag = (this.nbColumns == a.nbColumns);
if ~flag
    messageDifStr('nbColumns', this.Name, a.Name, num2str(this.nbColumns), num2str(a.nbColumns), 1)
    return
end

flag = (this.nbRows == a.nbRows);
if ~flag
    messageDifStr('nbRows', this.Name, a.Name, num2str(this.nbRows), num2str(a.nbRows), 1)
    return
end

flag = (this.nbSlides == a.nbSlides);
if ~flag
    messageDifStr('nbSlides', this.Name, a.Name, num2str(this.nbSlides), num2str(a.nbSlides), 1)
    return
end

flag = (this.Unit == a.Unit);
if ~flag
    messageDifStr('Unit', this.Name, a.Name, this.Unit, a.Unit, 1)
    return
end

flag = (this.DataType == a.DataType);
if ~flag
    messageDifStr('DataType', this.Name, a.Name, cl_image.strDataType{this.DataType}, cl_image.strDataType{a.DataType}, 1)
    return
end

flag = (this.GeometryType == a.GeometryType);
if ~flag
    messageDifStr('GeometryType', this.Name, a.Name, cl_image.strGeometryType{this.GeometryType}, cl_image.strGeometryType{a.GeometryType}, 1)
    return
end

flag = (this.ImageType == a.ImageType);
if ~flag
    messageDifStr('ImageType', this.Name, a.Name, cl_image.strImageType{this.ImageType}, cl_image.strImageType{a.ImageType}, 1)
    return
end

flag = (this.SpectralStatus == a.SpectralStatus);
if ~flag
    messageDifStr('SpectralStatus', this.Name, a.Name, cl_image.strSpectralStatus{this.SpectralStatus}, cl_image.strSpectralStatus{a.SpectralStatus}, 1)
    return
end

flag = strcmp(this.XLabel, a.XLabel);
if ~flag
    messageDifStr('XLabel', this.Name, a.Name, this.XLabel, a.XLabel, 1)
    return
end

flag = strcmp(this.XUnit, a.XUnit);
if ~flag
    messageDifStr('XUnit', this.Name, a.Name, this.XUnit, a.XUnit, 1)
    return
end

flag = isequal(this.x, a.x);
if ~flag
    messageDifStr('x', this.Name, a.Name, num2strCode(this.x), num2strCode(a.x), 1)
    return
end

flag = strcmp(this.YLabel, a.YLabel);
if ~flag
    messageDifStr('YLabel', this.Name, a.Name, this.YLabel, a.YLabel, 1)
    return
end

flag = strcmp(this.YUnit, a.YUnit);
if ~flag
    messageDifStr('YUnit', this.Name, a.Name, this.YUnit, a.YUnit, 1)
    return
end

flag = isequal(this.y, a.y);
if ~flag
    messageDifStr('y', this.Name, a.Name, num2strCode(this.y), num2strCode(a.y), 1)
    return
end

I1 = isnan(this);
I2 = isnan(a);
flag = isequal(I1, I2);
if ~flag
    messageDifStr('NaN Image', this.Name, a.Name, num2strCode(I1), num2strCode(I2),0)
    return
end

I1 = this.Image(:,:,:);
I2 = a.Image(:,:,:);
% flag = isequalwithequalnans(I1, I2);
flag = isequaln(I1, I2);
if ~flag
    messageDifStr('Image', this.Name, a.Name, num2strCode(I1), num2strCode(I2), 0)
    return
end

%{                                                                                                                                                                                                                                                                                        
 TODO
   --- Signals ---                                                                                                                                                                                                                                                                                                      
    SonarRawDataResol      <->  (m)                                                                                                                                                                                                                                                                                      
    SonarResolutionD       <->  (m)                                                                                                                                                                                                                                                                                      
    SonarResolutionX       <->  (m)                                                                                                                                                                                                                                                                                      
    SonarTime              <-> 212 cl_time : Deb : 17/01/2014  14:50:34.796  Fin : 17/01/2014  14:40:07.871                                                                                                                                                                                                              
    SonarImmersion         <-> 212*1 vals from 0 to 0 (m)                                                                                                                                                                                                                                                                
    SonarHeight            <-> 212*1 vals from 0 to 0 (m)                                                                                                                                                                                                                                                                
    SonarHeading           <-> 212*1 vals from 0 to 0 (deg)                                                                                                                                                                                                                                                              
    SonarRoll              <-> 212*1 vals from 0 to 0 (deg)                                                                                                                                                                                                                                                              
    SonarPitch             <-> 212*1 vals from 0 to 0 (deg)                                                                                                                                                                                                                                                              
    SonarSurfaceSoundSpeed <-> 212*1 vals from 1534.83 to 1534.9 (m/s)                                                                                                                                                                                                                                                   
    SonarHeave             <-> 212*1 vals from 0 to 0 (deg)                                                                                                                                                                                                                                                              
    SonarTide              <-> 212*1 vals from 0 to 0 (m)                                                                                                                                                                                                                                                                
    SonarPingCounter       <-> 212*1 vals from 48 to 259                                                                                                                                                                                                                                                                 
    SonarSampleFrequency   <-> 212*1 vals from 376.3926 to 1505.5706 (Hz)                                                                                                                                                                                                                                                
    SonarFrequency         <-> 212*1 vals from 10.5 to 13.5 (Hz)                                                                                                                                                                                                                                                         
    SonarFishLatitude      <-> 212*1 vals from 0 to 0 (deg)                                                                                                                                                                                                                                                              
    SonarFishLongitude     <-> 212*1 vals from 0 to 0 (deg)                                                                                                                                                                                                                                                              
    SonarPortMode_1        <-> 212*1 vals from 1 to 1                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                   
                                                                                                                                                                                                                                                                                                                         
         --- Compensation of the source Level ---                                                                                                                                                                                                                                                                        
    SonarNE_etat           <-> {'1=Compensated'} | '2=Not compensated'                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                         
         --- Compensation du gain analogique a la sortie des antennes de reception ---                                                                                                                                                                                                                                   
    SonarGT_etat           <-> {'1=Compensated'} | '2=Not compensated'                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                         
         --- Compensation of the sensibility of the Tx antenas  ---                                                                                                                                                                                                                                                      
    SonarSH_etat           <-> {'1=Compensated'} | '2=Not compensated'                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                         
         --- TVG ---                                                                                                                                                                                                                                                                                                     
    SonarTVG_etat                   <-> {'1=Compensated'} | '2=Not compensated'                                                                                                                                                                                                                                                
    SonarTVG_origine                <-> {'1=Constructeur'} | '2=Ifremer'                                                                                                                                                                                                                                                 
    SonarTVG_ConstructTypeCompens   <-> {'1=Analytique'} | '2=Table'                                                                                                                                                                                                                                                     
           PT(R) = CoefDiverg*log10(R) + 2*Alpha*R + Constante                                                                                                                                                                                                                                                           
    SonarTVG_ConstructAlpha         <-> 0.000000 (dB/km)                                                                                                                                                                                                                                                                 
    SonarTVG_ConstructCoefDiverg    <-> 40.000000                                                                                                                                                                                                                                                                        
    SonarTVG_ConstructConstante     <-> 0.000000 (dB)                                                                                                                                                                                                                                                                    
    SonarTVG_ConstructTable         <-> []                                                                                                                                                                                                                                                                               
    SonarTVG_IfremerAlpha           <-> 0 (dB/km)                                                                                                                                                                                                                                                                        
    SonarTVG_IfremerCoefDiverg      <-> 40                                                                                                                                                                                                                                                                               
    SonarTVG_IfremerConstante       <-> 0 (dB)                                                                                                                                                                                                                                                                           
                                                                                                                                                                                                                                                                                                                         
         --- Compensation of the Tx beam diagrams ---                                                                                                                                                                                                                                                                    
    SonarDiagEmi_etat                   <-> {'1=Compensated'} | '2=Not compensated'                                                                                                                                                                                                                                            
    SonarDiagEmi_origine                <-> {'1=Constructeur'} | '2=Ifremer'                                                                                                                                                                                                                                             
    SonarDiagEmi_ConstructTypeCompens   <-> {'1=Analytique'} | '2=Table'                                                                                                                                                                                                                                                 
    SonarDiagEmi_IfremerTypeCompens     <-> {'1=Analytique'} | '2=Table'                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                                                                                                                         
         --- Compensation of the Rx beam diagrams ---                                                                                                                                                                                                                                                                    
    SonarDiagRec_etat                   <-> {'1=Compensated'} | '2=Not compensated'                                                                                                                                                                                                                                            
    SonarDiagRec_origine                <-> {'1=Constructeur'} | '2=Ifremer'                                                                                                                                                                                                                                             
    SonarDiagRec_ConstructTypeCompens   <-> {'1=Analytique'} | '2=Table'                                                                                                                                                                                                                                                 
    SonarDiagRec_IfremerTypeCompens     <-> {'1=Analytique'} | '2=Table'                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                                                                                                                         
         --- Compensation of the insonified area ---                                                                                                                                                                                                                                                                     
    SonarAireInso_etat                  <-> '1=Compensated' | {'2=Not compensated'}                                                                                                                                                                                                                                            
    SonarAireInso_origine               <-> {'1=Constructeur'} | '2=Ifremer RxBeamAngle' | '3=Ifremer IncidenceAngle'                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                         
         --- Compensation of the BS ---                                                                                                                                                                                                                                                                                  
    SonarBS_etat                        <-> {'1=Compensated'} | '2=Not compensated'                                                                                                                                                                                                                                            
    SonarBS_origine                     <-> {'1=Belle Image'} | '2=Full compensation'                                                                                                                                                                                                                                    
    SonarBS_origineBelleImage           <-> {'1=Lambert'} | '2=Lurton'                                                                                                                                                                                                                                                   
    SonarBS_origineFullCompens          <-> {'1=Analytique'} | '2=Table'                                                                                                                                                                                                                                                 
    SonarBS_LambertCorrection           <-> {'1=R/Rn'} | '2=BeamPointingAngle' | '3=IncidenceAngle'                                                                                                                                                                                                                      
    SonarBS_IfremerCompensTable         <-> []                                                                                                                                                                                                                                                                           
                                                                                                                                                                                                                                                                                                                         
         --- Temporaire ---                                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Transmit.X        <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Transmit.Y        <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Transmit.Z        <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Transmit.Roll     <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Transmit.Pitch    <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Transmit.Heading  <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Receive.X         <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Receive.Y         <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Receive.Z         <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Receive.Roll      <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Receive.Pitch     <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.Arrays.Receive.Heading   <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.MRU.X                    <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.MRU.Y                    <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.MRU.Z                    <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.MRU.RollCalibration      <-> 0.000000                                                                                                                                                                                                                                                                     
    Sonar.Ship.MRU.PitchCalibration     <-> 0.000000                                                                                                                                                                                                                                                                     
    Sonar.Ship.MRU.HeadingCalibration   <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.MRU.TimeDelay            <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.GPS.X                    <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.GPS.Y                    <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.GPS.Z                    <->                                                                                                                                                                                                                                                                              
    Sonar.Ship.WaterLineVerticalOffset  <->  
%}

%{
function messageDifNum(NameParameter, ImageName1, ImageName2, x1, x2)
str1 = sprintf('Le param�tre "%s" nest pas identique pour les images "%s" et "%s"\nVal1=%f, val2=%f', ...
    NameParameter, ImageName1, ImageName2, x1, x2);
str2 = 'TODO';
my_warndlg(Lang(str1,str2), 1);
%}

function messageDifStr(NameParameter, ImageName1, ImageName2, s1, s2, flagMessage)
str1 = sprintf('Le param�tre "%s" n''est pas identique pour les images "%s" et "%s"\nVal1=%s, val2=%s', ...
    NameParameter, ImageName1, ImageName2, s1, s2);
str2 = sprintf('Parameter "%s" is not equal for "%s" and "%s" images\nVal1=%s, val2=%s', ...
    NameParameter, ImageName1, ImageName2, s1, s2);
my_warndlg(Lang(str1,str2), flagMessage, 'Tag', 'MessageComparisonBDSResults');

