% Erreur quadratique moyenne entre 2 images (ou portions d'image)
%
% Syntax
%   E = eqm(a, b, ...)
%
% Input Arguments
%   a : Instance de cl_image
%   b : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx  : subsampling in X
%   suby  : subsampling in Y
%
% Output Arguments
%   E : Erreur quadratique moyenne entre les 2 images
%
% Examples
%   a = cl_image('Image', ImageSonar(1));
%   b = filterGauss(a)
%   Eqm = eqm(a, b)
%
% See also cl_image cl_image/mean Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function E = eqm(a, b, varargin)

[subx, suby, varargin] = getSubxSuby(a, varargin); %#ok<ASGLU>

X1 = singleUnlessDouble(a.Image(suby, subx), a.ValNaN);
X2 = singleUnlessDouble(b.Image(suby, subx), b.ValNaN);

if isnan(a.ValNaN)
    sub1 = ~isnan(X1);
else
    sub1 = (X1 == a.ValNaN);
end

if isnan(b.ValNaN)
    sub2 = ~isnan(X2);
else
    sub2 = (X2 == a.ValNaN);
end

sub1 = sub1 & sub2;
clear sub2

Dif = X1(sub1) - X2(sub1);
clear X1 X2

E = sqrt(mean(Dif(:) .^ 2));
