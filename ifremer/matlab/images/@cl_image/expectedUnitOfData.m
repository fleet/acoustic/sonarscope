% TODO : � poursuivre

function Unit = expectedUnitOfData(DataType)

if isempty(DataType) || ~isnumeric(DataType)
    Unit = '';
    return
end

switch cl_image.strDataType{DataType}
    case 'Bathymetry'
        Unit = 'm';
    case {'Reflectivity'; 'TVG'}
        Unit = 'dB';
    case {'TxAngle'
            'IncidenceAngle'
            'RxBeamAngle'
            'RxAngleEarth'
            'TxAngleKjell'
            'BeamAzimuthAngle'
            'TxTiltAngle'
            'SonarAlongBeamAngle'
            'SonarAcrossBeamAngle'
            'SonarAlongBeamWidth'
            'BeamPointingAngle'}
        Unit = 'deg';
    case {'Latitude'; 'Longitude'; 'Heading'}
        Unit = 'deg';
    case {'Roll'; 'Pitch';'Heave'}
        Unit = 'deg';
    case 'Gravity'
        Unit = 'mGal';
    case 'Magnetism'
        Unit = 'nT';
    case {'SlopeAlong'; 'SlopeAcross'; 'SlopeMax'; 'SlopeAzimuth'}
        Unit = 'deg';
    case {'ResolAlong'; 'ResolImageAcross'; 'ResolBathyAcross'}
        Unit = 'm';
    case {'AlongDist'; 'AcrossDist'}
        Unit = 'm';
    case 'FootprintArea'
        Unit = 'm2';
    case {'GeoX'; 'GeoY'}
        Unit = 'm';
    case 'AbsorptionCoeff'
        Unit = 'dB/km';
    case 'KongsbergIBA'
        Unit = 'deg';
    case 'InsonifiedAreadB'
        Unit = 'dB';
    otherwise
        Unit = '';
end

%{
% TODO : � poursuivre
    'InsonifiedAreadB'
    'TxBeamPattern'
    'RxBeamPattern'
    'RayPathLength'
    'RxTime'
    'RayPathSampleNb'
    'RayPathTravelTime'
    'Temperature'
    'Salinity'
    'SoundVelocity'
    'Segmentation'
    'MbesQualityFactor'
    'ResonDetectionBrightness'
    'ResonDetectionColinearity'
    'Log10Bathymetry'
    'KongsbergPhaseVariance'
    'SonarAcrossBeamWidth'
    'KongsbergIBA'
    'KongsbergTwoWayTravelTime'
    'EmplacementLibre'
    'KongsbergFocusRange'
    'PulseLength'
    'KongsbergSectorTransmitDelay'
    'Frequency'
    'AbsorptionCoeff'
    'SignalBandwith'
    'Histo2D'
    'MbesPhaseEqm'
    'MbesPhaseSlope'
    'MbesXLJMA'
    'TwoWayTravelTimeInSeconds'
    'TwoWayTravelTimeInSamples'
    'TxBeamIndexSwath'
    'SampleFrequency'
    'Speed'
    'IfremerQF'
    'UncertaintyPerCent'
    'UncertaintyMeters'
    'KongsbergSnippetDetectionInfo'};
%}
