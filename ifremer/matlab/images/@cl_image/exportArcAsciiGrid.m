% Ecriture d'un fichier Arc Ascii Grid
%
% Syntax
%   flag = exportArcAsciiGrid(this, varargin)
%
% Input Arguments
%   this        : Instance de cl_image
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%
% See also cl_image Authors
% Authors : GT
%--------------------------------------------------------------------------

function flag = exportArcAsciiGrid(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, fileType] = getPropertyValue(varargin, 'fileType', 'ascii'); %#ok<ASGLU>

%{
% Message pour Arnaud : cette fonction doit-elle �tre utilis�e iniquement
% pour la g�om�trie GeoYX
I0 = cl_image_I0;
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('GeoYX'));
if ~flag
return
end
%}

flag = 0;

% Pas d'image RGB
if (this.ImageType == 2)
    this = RGB2Intensity(this);
end

xllcorner = min(this.x(subx));
yllcorner = min(this.y(suby));

Ycellsize = this.y(2)-this.y(1);
cellsize  = this.x(2)-this.x(1);

if ~nearlyEqual(Ycellsize, cellsize, 0.001)
    str1 = 'TODO';
    str2 = sprintf('Warning : it is not a square grid (deltax not equal to deltay).\n\nProject your image in GeoYX geometry if it is in LatLong and convert this image.');
    my_warndlg(Lang(str1,str2),1);
    flag = 0;
    return
end

Z = this.Image(suby,subx);
NODATA_value= -99999;
Z(isnan(Z)) = NODATA_value;

% V�rue pour contrer un probl�me de FlipUD li� � la gestion de cl_memapfile
if (this.y(end) > this.y(1)) && ((this.GeometryType == cl_image.indGeometryType('LatLong')) || (this.GeometryType == cl_image.indGeometryType('GeoYX')))
    Z = flipud(Z); % Modif JMA : comment� le 11/06/2018 pour Giuseppe
end

%% Ouverture du fichier

fid = fopen(nomFic, 'wt');

%% Ecriture en-t�te
% fprintf(fid, '%s\t%d\n', 'ncols',           length(subx));
% fprintf(fid, '%s\t%d\n', 'nrows',           length(suby));
% fprintf(fid, '%s\t%f\n', 'xllcorner',       xllcorner);
% fprintf(fid, '%s\t%f\n', 'yllcorner',       yllcorner);
% fprintf(fid, '%s\t%f\n', 'cellsize',        cellsize);
% fprintf(fid, '%s\t%d\n', 'NODATA_value',    NODATA_value);

fprintf(fid, '%s %d\n', 'ncols',        length(subx));
fprintf(fid, '%s %d\n', 'nrows',        length(suby));
fprintf(fid, '%s %s\n', 'xllcenter',    num2strPrecis(xllcorner));
fprintf(fid, '%s %s\n', 'yllcenter',    num2strPrecis(yllcorner));
fprintf(fid, '%s %s\n', 'cellsize',     num2strPrecis(cellsize));
fprintf(fid, '%s %d\n', 'NODATA_value', NODATA_value);
fclose(fid); % on referme pour mieux ouvrir.

%% Ecriture donn�es

switch fileType
    case 'ascii'
        dlmwrite(nomFic, Z, '-append', 'delimiter', ' ');
        flag = 1;
        
    case 'bin'
        [pathname, nomFic] = fileparts(nomFic);
        nomFic = fullfile(pathname, [nomFic, '.FLT']);
        
        fid = fopen(nomFic, 'wb');
        fwrite(fid, Z', 'single');
        fclose(fid); % on referme
        
        flag = 1;
end

str1 = 'Export r�ussi';
str2 = 'Export successful';
helpdlg(Lang(str1,str2), 'Export ArcGrid');

