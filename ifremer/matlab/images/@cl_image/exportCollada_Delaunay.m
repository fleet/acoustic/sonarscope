% Ecriture d'un fichier "Digital Asset Exchange *.dae" au format COLLADA
% Surface verticale le long de la navigation
% ----------------------------------------------------------------------
%
% Syntax
%   flag = exportCollada(this, varargin)
%
% Input Arguments
%   this : Instance de cl_image
%   subx : sous-ensemble de coordonn�es en X.
%   suby : sous-ensemble de coordonn�es en Y.
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%
% See also cl_image Authors
% Authors : GLT
% ----------------------------------------------------------------------

function flag = exportCollada_Delaunay(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, filename] = getPropertyValue(varargin, 'filename', 'toto.dae');
[varargin, image]    = getPropertyValue(varargin, 'image',    0);
[varargin, vertexag] = getPropertyValue(varargin, 'vertexag', 10);
[varargin, bloc]     = getPropertyValue(varargin, 'bloc',     0);
[varargin, blocZ]    = getPropertyValue(varargin, 'blocZ',    1.2); %#ok<ASGLU>

% Pas d'image RGB
for k=1:length(this)
    if this(k).ImageType == 2
        this(k) = RGB2Intensity(this(k));
    end
end

% %% Cr�ation du r�pertoire
[nomDirFic, name] = fileparts(filename);
% nomDirKml   = fullfile(nomDirFic, name);
%
% if exist(nomDirKml, 'dir')
%     rmdir(nomDirKml, 's');
% end
%
% flag = [];
% flag(end+1) = mkdir(nomDirKml);
% flag(end+1) = mkdir([nomDirKml, '\images']);
% flag(end+1) = mkdir([nomDirKml, '\models']);
% if ~any(flag)
%     messageErreurFichier(nomDirKml, 'WriteFailure');
%     return
% end

%% Nom des fichiers
% nomFicKML = fullfile(nomDirKml, [name '.kml']);
nomFicDAE = fullfile(nomDirFic, [name '.dae']);
% nomFicDAE = fullfile([nomDirKml, '\models'], [name '.dae']);
if image
    %     nomFicPNG = fullfile([nomDirKml, '\images'], [name '.png']);
    nomFicPNG = fullfile(nomDirFic, [name '.png']);
end


%% --- Cr�ation de l'image PNG --------------------------------------------
% if image
%     map = this.Colormap;
%     CLim = this.CLim;
%     I = gray2ind(mat2gray(this.Image( suby, subx), double(CLim)), size(map,1));
%     I(I == 0) = 1;
%     I(isnan(I)) = 0;
%     imwrite(I, map, nomFicPNG, 'Transparency', 0)
% end

if image
    %     img = imresize(this.Image( suby, subx), [2^nextpow2(length(subx)) 2^nextpow2(length(suby))], 'bilinear');
    %     map = this.Colormap;
    %     CLim = this.CLim;
    %     I = gray2ind(mat2gray(img, double(CLim)), size(map,1));
    %     I(I == 0) = 1;
    %     I(isnan(I)) = 0;
    %     imwrite(I, map, nomFicPNG, 'Transparency', 0)
    
    exportRaster(this, 'nomFic', nomFicPNG, 'Format', 'png', ...
        'subx', subx, 'suby', suby, 'VisuExterne', 0);
end

%% --- Cr�ation du fichier COLLADA DAE ------------------------------------

fidDAE = fopen(nomFicDAE, 'w+');

sendStr(fidDAE, 0, '<?xml version="1.0" encoding="UTF-8"?>');
sendStr(fidDAE, 0, '<COLLADA xmlns="http://www.collada.org/2005/11/COLLADASchema" version="1.4.1">');

%% <Asset>
sendStr(fidDAE, 1, '<asset>');
sendStr(fidDAE, 2, '<contributor>');
sendStr(fidDAE, 3, '<authoring_tool>SonarScope</authoring_tool>');
sendStr(fidDAE, 2, '</contributor>');
sendStr(fidDAE, 2, sprintf('<created>%s</created>', datestr(now, 'yyyy-mm-ddTHH:MM:SS')));
sendStr(fidDAE, 2, sprintf('<modified>%s</modified>', datestr(now, 'yyyy-mm-ddTHH:MM:SS')));
sendStr(fidDAE, 2, '<unit name="meter" meter="1.0"/>');
% sendStr(fidDAE, 2, '<unit name="feet" meter="0.3048"/>'); % POUR TEST
sendStr(fidDAE, 2, '<up_axis>Z_UP</up_axis>');
sendStr(fidDAE, 1, '</asset>');

%% <library_images>
if image
    [~, name] = fileparts(filename);
    
    sendStr(fidDAE, 1, '<library_images>');
    sendStr(fidDAE, 2, '<image id="imageID" name="SonarScope_image">');
    sendStr(fidDAE, 3, sprintf('<init_from>%s</init_from>', [name '.png']));
    sendStr(fidDAE, 2, '</image>');
    sendStr(fidDAE, 1, '</library_images>');
end

%% <library_materials>
sendStr(fidDAE, 1, '<library_materials>');
sendStr(fidDAE, 2, '<material id="materialID" name="material">');
sendStr(fidDAE, 3, '<instance_effect url="#effectID" />');
sendStr(fidDAE, 2, '</material>');
sendStr(fidDAE, 1, '</library_materials>');

%% <library_effects>
if image
    sendStr(fidDAE, 1, '<library_effects>');
    sendStr(fidDAE, 2, '<effect id="effectID" name="effectID">');
    sendStr(fidDAE, 3, '<profile_COMMON>');
    sendStr(fidDAE, 4, '<newparam sid="image_surface">');
    sendStr(fidDAE, 5, '<surface type="2D">');
    sendStr(fidDAE, 6, '<init_from>imageID</init_from>');
    sendStr(fidDAE, 5, '</surface>');
    sendStr(fidDAE, 4, '</newparam>');
    sendStr(fidDAE, 4, '<newparam sid="image_sampler">');
    sendStr(fidDAE, 5, '<sampler2D>');
    sendStr(fidDAE, 6, '<source>image_surface</source>');
    sendStr(fidDAE, 5, '</sampler2D>');
    sendStr(fidDAE, 4, '</newparam>');
    sendStr(fidDAE, 4, '<technique sid="COMMON">');
    sendStr(fidDAE, 5, '<phong>');
    sendStr(fidDAE, 6, '<emission>');
    sendStr(fidDAE, 7, '<color>0.000000 0.000000 0.000000 1</color>');
    sendStr(fidDAE, 6, '</emission>');
    sendStr(fidDAE, 6, '<ambient>');
    sendStr(fidDAE, 7, '<color>0.000000 0.000000 0.000000 1</color>');
    sendStr(fidDAE, 6, '</ambient>');
    sendStr(fidDAE, 6, '<diffuse>');
    sendStr(fidDAE, 7, '<texture texture="image_sampler" texcoord="UVSET0"/>');
    sendStr(fidDAE, 6, '</diffuse>');
    sendStr(fidDAE, 6, '<specular>');
    sendStr(fidDAE, 7, '<color>0.330000 0.330000 0.330000 1</color>');
    sendStr(fidDAE, 6, '</specular>');
    sendStr(fidDAE, 6, '<shininess>');
    sendStr(fidDAE, 7, '<float>20.000000</float>');
    sendStr(fidDAE, 6, '</shininess>');
    sendStr(fidDAE, 6, '<reflectivity>');
    sendStr(fidDAE, 7, '<float>0.100000</float>');
    sendStr(fidDAE, 6, '</reflectivity>');
    sendStr(fidDAE, 6, '<transparent>');
    sendStr(fidDAE, 7, '<color>1 1 1 1</color>');
    sendStr(fidDAE, 6, '</transparent>');
    sendStr(fidDAE, 6, '<transparency>');
    sendStr(fidDAE, 7, '<float>0.000000</float>');
    sendStr(fidDAE, 6, '</transparency>');
    sendStr(fidDAE, 5, '</phong>');
    sendStr(fidDAE, 4, '</technique>');
    sendStr(fidDAE, 4, '<extra>');
    sendStr(fidDAE, 5, '<technique profile="GOOGLEEARTH">');
    sendStr(fidDAE, 6, '<double_sided>1</double_sided>');
    sendStr(fidDAE, 5, '</technique>');
    sendStr(fidDAE, 4, '</extra>');
    sendStr(fidDAE, 3, '</profile_COMMON>');
    sendStr(fidDAE, 2, '</effect>');
    sendStr(fidDAE, 1, '</library_effects>');
    
else
    sendStr(fidDAE, 1, '<library_effects>');
    sendStr(fidDAE, 2, '<effect id="effectID">');
    sendStr(fidDAE, 3, '<profile_COMMON>');
    sendStr(fidDAE, 4, '<technique sid="COMMON">');
    sendStr(fidDAE, 5, '<lambert>');
    sendStr(fidDAE, 6, '<diffuse>');
    sendStr(fidDAE, 7, '<color>0.8901960784313725 0.8823529411764706 0.8705882352941177 1</color>')
    sendStr(fidDAE, 6, '</diffuse>');
    sendStr(fidDAE, 5, '</lambert>');
    sendStr(fidDAE, 4, '</technique>');
    sendStr(fidDAE, 3, '</profile_COMMON>');
    sendStr(fidDAE, 2, '</effect>');
    sendStr(fidDAE, 1, '</library_effects>');
end

%% <library_geometries>

%____ Tableaux ____

%--- Points : sommets des triangles
x = this(1).x(subx) - min(this(1).x(subx));
y = this(1).y(suby) - min(this(1).y(suby));
z = this(1).Image( suby, subx);
z = z * vertexag;
blocZ = blocZ * min(z(:), [], 'omitnan');
dx = abs(diff(x(1:2)));
dy = abs(diff(y(1:2)));
nx = length(x);
ny = length(y);

vertexPos = [];

if image
    uvCoord = [];
end

v = 0:(ny-1);
for ix = 1:nx
    subNonNaN = ~isnan(z(:,ix));
    lenY = sum(subNonNaN);
    
    % position des points de valeur
    vertexPos(:, end+1 : end+lenY) = [x(ix)*ones(1, lenY); ...
        y(subNonNaN); ...
        z(subNonNaN,ix)' ];
    if image
        uvCoord(:,end+1:end+lenY) = [(ix-1)/(nx-1)*ones(1,lenY);
            v(subNonNaN)/(ny-1)];
    end
end

nVertex   = numel(vertexPos) / 3;

% Triangulation 2D
TRI = delaunay(vertexPos(1:2, :)');
% TRI = Ntri * [p1 p2 p3]
% filtre sur les triangles dont l'un des c�t�s au moins est plus grand que
% la maille
%v2
trep = triangulation(TRI, vertexPos(1:2,:)');
nTriangles = size(trep,1);
[~, Radius] = circumcenter(trep, (1:nTriangles)');
% on supprime tous les triangles dont le cercle circonscrit a un diam�tre > diagonale de la maille
sub = Radius <= sqrt(dx^2+dy^2)/2;
TRI = TRI(sub,:);

% Si bloc
if bloc && (length(this) == 1) % bloc avec base � zero
    % Indice des points correspondants � la fronti�re du MNT
    trep = triangulation(TRI, vertexPos(1:2,:)');
    fb = freeBoundary(trep);
    fb = fb(:,1);
    nfb = length(fb);
    
    % ajout dans vertexPos des points du socle : m�me position XY, mais Z
    % en dessous
    vertexPos(:, end+1 : end+length(fb)) = ...
        [vertexPos(1,fb); ...
        vertexPos(2,fb); ...
        blocZ*ones(1, length(fb))];
    
    % Construction de la triangulation des jupes
    for k=1:nfb
        if mod(k,1000) == 0
            fprintf('%d / %d\n', k, nfb);
        end
        % 2 triangles � chaque fois
        % -> triangle base en bas
        m = max(mod(k+1,nfb),1);
        TRI(end+1,:) = [fb(k), nVertex+k, nVertex+m]; %#ok<AGROW>
        % -> triangle pointe en bas
        TRI(end+1,:) = [fb(k), nVertex+m, fb(m)]; %#ok<AGROW>
    end
    
    nVertex = nVertex + nfb; % M�J du nombre de points
    
elseif bloc && (length(this) > 1) % bloc avec base MNT
    x = this(2).x(subx) - min(this(2).x(subx));
    y = this(2).y(suby) - min(this(2).y(suby));
    z = this(2).Image( suby, subx);
    z = z * vertexag;
%     blocZ = blocZ * min(z(:), [], 'omitnan');
    dx = abs(diff(x(1:2)));
    dy = abs(diff(y(1:2)));
    nx = length(x);
    ny = length(y);
    
    if image
        uvCoord = [];
    end
    
    v = 0:(ny-1);
    for ix=1:nx
        subNonNaN = ~isnan(z(:,ix));
        lenY = sum(subNonNaN);
        
        % ajout des positions de la base
        vertexPos(:,end+1:end+lenY) = [x(ix)*ones(1,lenY); ...
            y(subNonNaN); ...
            z(subNonNaN,ix)'];
        if image
            uvCoord(:, end+1 : end+lenY) = [(ix-1)/(nx-1)*ones(1, lenY);
                v(subNonNaN)/(ny-1)];
        end
    end
    
    nVertex2 = nx*ny;
    
    % Triangulation 2D base
    TRI2 = delaunay(vertexPos(1:2, nVertex+1:end)');
    % TRI = Ntri * [p1 p2 p3]
    % filtre sur les triangles dont l'un des c�t�s au moins est plus grand que
    % la maille
    %v2
    trep = triangulation(TRI2, vertexPos(1:2,nVertex+1:end)');
    nTriangles  = size(trep,1);
    [~, Radius] = circumcenter(trep, (1:nTriangles)');
    % on supprime tous les triangles dont le cercle circonscrit a un diam�tre > diagonale de la maille
    TRI2 = TRI2( Radius <= sqrt(dx^2+dy^2)/2 , : );
    
    % Jupes
    % Indice des points correspondants � la fronti�re du MNT Top
    trep = triangulation(TRI, vertexPos(1:2,1:nVertex)');
    fbTop = freeBoundary(trep);
    fbTop = fbTop(:,1);
    nfbTop = length(fbTop);
    
    % Indice des points correspondants � la fronti�re du MNT Base
    trep = triangulation(TRI2, vertexPos(1:2,nVertex+1:end)');
    fbBase = freeBoundary(trep);
    fbBase = fbBase(:,1);
    fbBase = fbBase + nVertex; % offset de nVertex
    nfbBase = length(fbBase);
    
    TRI = [TRI; TRI2+nVertex]; % Ajout des triangles de la base dans le pool de triangles
    
    % calage � un point supperpos�
    %     figure, plot(vertexPos(1,fbTop), 'r')
    %     hold on
    %     plot( vertexPos(1,fbBase), 'b')
    %     figure, plot(vertexPos(2,fbTop), 'r')
    %     hold on
    %     plot( vertexPos(2,fbBase), 'b')
    [miniT, iminT] = min(sqrt(sum(vertexPos(1:2,fbTop).^2)));
    [miniB, iminB] = min(sqrt(sum(vertexPos(1:2,fbBase).^2)));
    if miniT == miniB
        offset = iminB - iminT; % base d�call�e de par rapport � top
        if offset > 0 % base d�call�e de par rapport � top
            offsetB = offset;
            offsetT = 0;
        else % base d�call�e de par rapport � top
            offsetB = 0;
            offsetT = -offset;
        end
    else
        disp('probl�me calage base/top')
    end
    
    % Construction de la triangulation des jupes
    N = max(nfbTop, nfbBase);
    for k=1:N
        if mod(k,1000) == 0
            fprintf('%d / %d\n', k, N);
        end
        % 2 triangles � chaque fois
        % -> triangle base en bas
        TRI(end+1,:) = [fbTop(max(mod(k+offsetT, nfbTop), 1)), ...
            fbBase(max(mod(k+offsetB, nfbBase), 1)),...
            fbBase(max(mod(k+offsetB+1, nfbBase), 1))]; %#ok<AGROW>
        % -> triangle pointe en bas
        TRI(end+1,:) = [fbTop(max(mod(k+offsetT, nfbTop), 1)), ...
            fbTop(max(mod(k+offsetT+1, nfbTop), 1)), ...
            fbBase(max(mod(k+offsetB+1, nfbBase), 1))]; %#ok<AGROW>
    end
    
    nVertex = nVertex + nVertex2;
end

% Triangulation 2.5D de la surface
%--- Normales au triangles
trep       = triangulation(TRI, vertexPos');
nTriangles = size(trep,1);
u          = faceNormal(trep, (1:nTriangles)');

%--- cr�ation des triangles
if image
    p = [TRI(:,1)-1, (1:nTriangles)'-1, TRI(:,1)-1,...
        TRI(:,2)-1, (1:nTriangles)'-1, TRI(:,2)-1,...
        TRI(:,3)-1, (1:nTriangles)'-1, TRI(:,3)-1];
else
    p = [TRI(:,1)-1, (1:nTriangles)'-1,...
        TRI(:,2)-1, (1:nTriangles)'-1,...
        TRI(:,3)-1, (1:nTriangles)'-1];
end


%____ FIN Tableaux ____

sendStr(fidDAE, 1, '<library_geometries>');
sendStr(fidDAE, 2, '<geometry id="geometryID">');
sendStr(fidDAE, 3, '<mesh>');

%--- Coordonn�es des points
sendStr(fidDAE, 4, '<source id="positionID">');
sendStr(fidDAE, 5, sprintf('<float_array id="positionSrc" count="%d">', numel(vertexPos)), 0);
sendStr(fidDAE, 0, sprintf('%f ', reshape(vertexPos, 1, numel(vertexPos))), 0);
sendStr(fidDAE, 0, '</float_array>');
sendStr(fidDAE, 5, '<technique_common>');
sendStr(fidDAE, 6, sprintf('<accessor source="#positionSrc" count="%d" stride="3">', nVertex));
sendStr(fidDAE, 7, '<param name="X" type="float"/>');
sendStr(fidDAE, 7, '<param name="Y" type="float"/>');
sendStr(fidDAE, 7, '<param name="Z" type="float"/>');
sendStr(fidDAE, 6, '</accessor>');
sendStr(fidDAE, 5, '</technique_common>');
sendStr(fidDAE, 4, '</source>');

%--- Coordonn�es des Normales
sendStr(fidDAE, 4, '<source id="normalID">');
sendStr(fidDAE, 5, sprintf('<float_array id="normalSrc" count="%d">', numel(u) ), 0);
sendStr(fidDAE, 0, sprintf('%f ', reshape(u, 1, numel(u))), 0);
sendStr(fidDAE, 0, '</float_array>');
sendStr(fidDAE, 5, '<technique_common>');
sendStr(fidDAE, 6, sprintf('<accessor source="#normalSrc" count="%d" stride="3">', nTriangles));
sendStr(fidDAE, 7, '<param name="X" type="float"/>');
sendStr(fidDAE, 7, '<param name="Y" type="float"/>');
sendStr(fidDAE, 7, '<param name="Z" type="float"/>');
sendStr(fidDAE, 6, '</accessor>');
sendStr(fidDAE, 5, '</technique_common>');
sendStr(fidDAE, 4, '</source>');

%--- Coordonn�es des Texture
if image
    sendStr(fidDAE, 4, '<source id="uvID">');
    sendStr(fidDAE, 5, sprintf('<float_array id="uvSrc" count="%d">',numel(uvCoord)), 0);
    sendStr(fidDAE, 0, sprintf('%f ', reshape(uvCoord, 1, numel(uvCoord))), 0);
    sendStr(fidDAE, 0, '</float_array>');
    sendStr(fidDAE, 4, '<technique_common>');
    sendStr(fidDAE, 6, sprintf('<accessor source="#uvSrc" count="%d" stride="2">', nVertex));
    sendStr(fidDAE, 7, '<param name="S" type="float"/>');
    sendStr(fidDAE, 7, '<param name="T" type="float"/>');
    sendStr(fidDAE, 6, '</accessor>');
    sendStr(fidDAE, 5, '</technique_common>');
    sendStr(fidDAE, 4, '</source>');
end

%____ Definition du maillage ____

%--- Points du maillage
sendStr(fidDAE, 4, '<vertices id="vertexID">');
sendStr(fidDAE, 5, '<input semantic="POSITION" source="#positionID"/>');
sendStr(fidDAE, 4, '</vertices>');

sendStr(fidDAE, 4, sprintf('<triangles material="Material2" count="%d">', nTriangles));
sendStr(fidDAE, 5, '<input semantic="VERTEX" source="#vertexID" offset="0"/>');
sendStr(fidDAE, 5, '<input semantic="NORMAL" source="#normalID" offset="1"/>');
if image
    sendStr(fidDAE, 5, '<input semantic="TEXCOORD" source="#uvID" offset="2" set="0"/>');
end
sendStr(fidDAE, 6, '<p>', 0);
sendStr(fidDAE, 0, sprintf('%d ', reshape(p', 1, numel(p))), 0);
sendStr(fidDAE, 0, '</p>');
sendStr(fidDAE, 4, '</triangles>');

sendStr(fidDAE, 3, '</mesh>');
sendStr(fidDAE, 2, '</geometry>');
sendStr(fidDAE, 1, '</library_geometries>');

%% <library_visual_scenes>
sendStr(fidDAE, 1, '<library_visual_scenes>');
sendStr(fidDAE, 2, '<visual_scene id="VisualSceneID">');
sendStr(fidDAE, 3, '<node name="SketchUp">');
sendStr(fidDAE, 4, '<instance_geometry url="#geometryID">');
sendStr(fidDAE, 5, '<bind_material>');
sendStr(fidDAE, 6, '<technique_common>');
sendStr(fidDAE, 7, '<instance_material symbol="Material2" target="#materialID">');
if image
    sendStr(fidDAE, 8, '<bind_vertex_input semantic="UVSET0" input_semantic="TEXCOORD" input_set="0" />');
end
sendStr(fidDAE, 7, '</instance_material>');
sendStr(fidDAE, 6, '</technique_common>');
sendStr(fidDAE, 5, '</bind_material>');
sendStr(fidDAE, 4, '</instance_geometry>');
sendStr(fidDAE, 3, '</node>');
sendStr(fidDAE, 2, '</visual_scene>');
sendStr(fidDAE, 1, '</library_visual_scenes>');

%% <Scene>
sendStr(fidDAE, 1, '<scene>');
sendStr(fidDAE, 2, '<instance_visual_scene url="#VisualSceneID"/>');
sendStr(fidDAE, 1, '</scene>');

%% Fin du fichier
sendStr(fidDAE, 0, '</COLLADA>');

fclose(fidDAE);

flag = 1;

helpdlg(Lang('Export COLLADA r�ussi ! ','COLLADA Export successful ! '), 'Export COLLADA');


%% SOUS-FONCTIONS
function sendStr(outFid, indent,str, varargin)
% Ecriture d'une chaine de caract�re avec indentation
tabs = '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'; % plein de tabs
fprintf(outFid, tabs(1:indent*2));
fprintf(outFid, str);
if nargin <= 3
    fprintf(outFid, '\n');
end


%{
function [xb, yb, boundaryEdges] = crust(x,y, visu)
% Algorithme Crust de reconstruction de la fronti�re d'un nuage de point
% Reference: N. Amenta, M. Bern, and D. Eppstein.
% "The crust and the beta-skeleton: combinatorial curve reconstruction"
% Graphical Models and Image Processing, 60:125-135, 1998.

numpts = length(x);
% Triangulation de Delaunay de l'ensemble de point
dt = DelaunayTri(x,y);
tri = dt(:,:);
% Insertion des points du diagramme de Voronoi dans la triangulation
V = dt.voronoiDiagram();
V(1,:) = []; % on enl�ve le point � l'infini
numv = size(V,1);
dt.X(end+(1:numv),:) = V;
% Les bords de la triangulation de Delaunay qui connectent des paires
% de points representent la fronti�re.
delEdges = dt.edges();
validx   = delEdges(:,1) <= numpts;
validy   = delEdges(:,2) <= numpts;
boundaryEdges = delEdges((validx & validy), :)';
xb = x(boundaryEdges);
yb = y(boundaryEdges);

if visu
    figure
    triplot(tri,x,y);
    axis equal;
    hold on;
    plot(x,y,'-og');
    plot(xb,yb, '-*r');
    xlabel('Curve reconstruction from a point cloud', 'fontweight','b');
    hold off;
end
%}

%{
function [xb, yb, boundaryEdges] = crust2(x, y, dx, dy, visu)
% Algorithme Crust de reconstruction de la fronti�re d'un nuage de point
% Reference: N. Amenta, M. Bern, and D. Eppstein.
% "The crust and the beta-skeleton: combinatorial curve reconstruction"
% Graphical Models and Image Processing, 60:125-135, 1998.

% Triangulation de Delaunay de l'ensemble de points
dt = DelaunayTri(x,y);
TRI = dt(:,:);
nTriangles = numel(TRI);
[~, Radius] = circumcenters(dt, (1:nTriangles)');
% on supprime tous les triangles dont le cercle csirconscrit a un diam�tre > diagonale de la maille
TRI = TRI( Radius <= sqrt(dx^2+dy^2)/2 , : );
% Les bords de la triangulation de Delaunay qui connectent des paires
% de points representent la fronti�re.
delEdges = dt.edges();
validx   = delEdges(:,1) <= numpts;
validy   = delEdges(:,2) <= numpts;
boundaryEdges = delEdges((validx & validy), :)';
xb = x(boundaryEdges);
yb = y(boundaryEdges);

if visu
    figure
    triplot(tri,x,y);
    axis equal;
    hold on;
    plot(x,y,'-og');
    plot(xb,yb, '-*r');
    xlabel('Curve reconstruction from a point cloud', 'fontweight','b');
    hold off;
end
%}
