% Exportation de courbes de statistiques conditionnelles
%
% Syntax
%   exportCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% -------------------------------------------------------------------------

function exportCourbesStats(this, nomFicSave, varargin) 

% [varargin, flagStats] = getPropertyValue(varargin, 'Stats', 1);
[varargin, sub]  = getPropertyValue(varargin, 'sub',  1:length(this.CourbesStatistiques));
[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if isempty(sub)
    return
end

this = correctif_structureCourbesStatistiques(this);

if isempty(nomFicSave)
    return
end

CourbeConditionnelle = this.CourbesStatistiques(sub);
flag = exist(nomFicSave, 'file');
if (flag == 2) && (Mute == 0)
    msg1 = sprintf('Le fichier "%s" existe. Que voulez-vous faire ?', nomFicSave);
    msg2 = sprintf('File "%s" exists. What do we do ?', nomFicSave);
    [ButtonName, flag] = my_questdlg(Lang(msg1,msg2), Lang('Ecraser', 'Overwrite'), Lang('Compléter','Append'));
    if ~flag
        return
    end
    if ButtonName == 1
        save(nomFicSave, 'CourbeConditionnelle');
    else
        X = CourbeConditionnelle;
        Y = loadmat(nomFicSave);
        CourbeConditionnelle = [Y X];
        save(nomFicSave, 'CourbeConditionnelle');
    end
else
    save(nomFicSave, 'CourbeConditionnelle');
end

for k=length(CourbeConditionnelle):-1:1
    if isempty(CourbeConditionnelle(k).bilan) || ~isfield(CourbeConditionnelle(k).bilan{1}(1), 'residuals') || isempty(CourbeConditionnelle(k).bilan{1}(1).residuals)
        CourbeConditionnelle(k) = [];
    else
        CourbeConditionnelle(k).bilan{1}(1).nomZoneEtude = [CourbeConditionnelle(k).bilan{1}(1).nomZoneEtude ' Residuals'];
        CourbeConditionnelle(k).bilan{1}(1).y            = CourbeConditionnelle(k).bilan{1}(1).residuals;
        CourbeConditionnelle(k).bilan{1}(1).residuals    = [];
        CourbeConditionnelle(k).bilan{1}(1).model        = [];
        [nomDir, nomFic] = fileparts(nomFicSave);
    end
end
if ~isempty(CourbeConditionnelle)
    nomFicSaveResiduals = fullfile(nomDir, [nomFic '_Residuals.mat']);
    save(nomFicSaveResiduals, 'CourbeConditionnelle');
end
