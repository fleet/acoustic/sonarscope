% Exportation de courbes de statistiques conditionnelles
%
% Syntax
%   exportCourbesStats_ASCII(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% -------------------------------------------------------------------------

function exportCourbesStats_ASCII(this, nomFicSave, varargin)

% [varargin, flagStats] = getPropertyValue(varargin, 'Stats', 1);
[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if isempty(sub)
    return
end

if ~isempty(nomFicSave)
    export_ASCII(nomFicSave, this.CourbesStatistiques(sub))
end


function export_ASCII(nomFicSave, Courbes)

for i=1:length(Courbes)
    Courbes(i).bilan{1}.x       = (Courbes(i).bilan{1}.x(:))';
    Courbes(i).bilan{1}.y       = (Courbes(i).bilan{1}.y(:))';
    Courbes(i).bilan{1}.ymedian = (Courbes(i).bilan{1}.ymedian(:))';
    Courbes(i).bilan{1}.std     = (Courbes(i).bilan{1}.std(:))';
    
    %     sub = isnan(Courbes(i).bilan{1}.y);
    %     Courbes(i).bilan{1}.y(sub)       = 9999;
    %     Courbes(i).bilan{1}.ymedian(sub) = 9999;
    %     Courbes(i).bilan{1}.std(sub)     = 9999;
    
    s(i) = Courbes(i).bilan{1}; %#ok<AGROW>
end
DPref.ItemName  = 'item';
DPref.StructItem = false;
DPref.CellItem   = false;
xml_write(nomFicSave, s, 'Stats', DPref);

% bilan = xml_read(nomFicSave);


% fid = fopen(nomFicSave, 'w+');
% names = fieldnames(s);
% for i=1:length(names)
%     fprintf(fid, '%s : ', names{i});
%
%     val = s.(names{i});
%     switch class(val)
%         case 'char'
%             fprintf(fid, '"%s"\n', val);
%         case {'double'; 'single'}
%             val = val(:);
%             fprintf(fid, '%s\n', num2str(val'));
%         case 'cell'
%             fprintf(fid, '"{%s}"\n', val{1});
%         otherwise
%             str = sprintf('%s pas encore trait� dans cl_image/exportCourbesStats_ASCII', class(val));
%             my_warndlg(str, 1);
%     end
% end
% fclose(fid);
