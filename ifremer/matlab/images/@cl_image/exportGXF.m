% Ecriture d'un fichier Grid eXchange File Rev 3  GXF-3
%
% Syntax
%   flag = exportGXF(this, varargin)
%
% Input Arguments
%   this        : Instance de cl_image
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%
% See also cl_image Authors
% Authors : GLT
%--------------------------------------------------------------------------

function flag = exportGXF(this, varargin)

% global GXFfile

flag = 0;

% Pas d'image RGB
if (this.ImageType == 2)
    this = RGB2Intensity(this);
end

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic]    = getPropertyValue(varargin, 'nomFic',    'GXFgrid.gxf');
[varargin, precision] = getPropertyValue(varargin, 'precision', 0.001);
[varargin, fileType]  = getPropertyValue(varargin, 'fileType',  'uncompressed'); %#ok<ASGLU>

fileType  = validatestring(fileType, {'uncompressed', 'compressed', 'repeatcompressed', 'kingdom'});
ImageName = this.Name;

%% Cr�ation de la structure GXF
Z  = this.Image(suby,subx);
if ~isfloat(Z)
    Z = single(Z);
end

GXF.Title           = sprintf('"%s"', ImageName(1:min(length(ImageName), 78)));
GXF.Points          = length(subx);
GXF.Rows            = length(suby);
GXF.RwSeparation    = abs(this.y(2)-this.y(1)); % apparemen abs() car les sens est contenu dans #SENSE !!!
GXF.PtSeparation    = abs(this.x(2)-this.x(1));

switch fileType
    case 'kingdom' % �a c'est pas banal !!!
        GXF.Sense = 1; %premier point en bas � gauche
    otherwise
        GXF.Sense = -2; %premier point en haut � gauche
end

% coordonn�es du coin en bas � gauche, quelque soit le sens d'ecriture de
% la grille
GXF.Xorigin         = min(this.x(subx));
GXF.Yorigin         = min(this.y(suby));

switch GXF.Sense
    case -2
        %         GXF.Xorigin         = this.x(subx(1));%min(this.x(subx));
        %         GXF.Yorigin         = this.y(suby(1));%min(this.y(suby));
    case 1
        %         GXF.Xorigin         = min(this.x(subx));
        %         GXF.Yorigin         = min(this.y(suby));
        Z = flipud(Z);
end

if isnan(this.ValNaN)
    GXF.Zmaximum = max(Z(:), [], 'omitnan');
    GXF.Zminimum = min(Z(:), [], 'omitnan');
else
    GXF.Zmaximum = max(Z(Z ~= this.ValNaN));
    GXF.Zminimum = min(Z(Z ~= this.ValNaN));
end

switch lower(fileType)
    case {'uncompressed', 'kingdom'}
        if isnan(this.ValNaN)
            GXF.Dummy    = getDummy(GXF.Zminimum, GXF.Zmaximum);
            Z(isnan(Z))  = GXF.Dummy;
        else
            GXF.Dummy    = getDummy(GXF.Zminimum, GXF.Zmaximum);
            Z(Z==this.ValNaN)  = GXF.Dummy;
        end
    otherwise
        GXF.Dummy = [];
end

% V�rue pour contrer un probl�me de FlipUD li� � la gestion de cl_memapfile
if this.y(end) > this.y(1)
    Z = flipud(Z);
end

%% Ouverture du fichier

GXFfile = fopen(nomFic, 'wt');

%% Ecriture en-t�te avant Carto
fieldNames = fieldnames(GXF);
for iField = 1:length(fieldNames)
    fprintf(GXFfile, '#%s\n', upper(fieldNames{iField}));
    fprintf(GXFfile, [getFormat(GXF.(fieldNames{iField})) '\n'], GXF.(fieldNames{iField}));
end

%% PROJECTION

cartoDef    = struct(get(this, 'Carto'));
if ~isempty(cartoDef)
    datum       = cartoDef.Ellipsoide.strType{cartoDef.Ellipsoide.Type};
    projection  = cartoDef.Projection.strType{cartoDef.Projection.Type};
    
    %% Test de coh�rence "pr�f carto"/g�ometrie
    switch projection
        case 'Geodetic'
            if this.GeometryType == cl_image.indGeometryType('GeoYX') % A priori ce cas n'arrive jamais
                str1 = sprintf('Export GXF impossible :\n\nIncoh�rence entre g�om�trie de l''image et pr�f�rence cartographiques :\nG�om�trie : %s\nG�od�sie :%s, %s\n\n>> Veuillez d�projeter l''image avant l''export', 'GeoYX', datum, projection);
                str2 = sprintf('GXF Export error :\n\nImage geometry and cartographic preferences are incoherent :\nGeometry : %s\nGeodesy : %s, %s\n\n>> Unproject image before exporting', 'GeoYX', datum, projection);
                my_warndlg(Lang(str1,str2), 1);
                return
            end
        case {'Mercator', 'UTM', 'Lambert', 'Stereographique polaire'}
            if this.GeometryType == cl_image.indGeometryType('LatLong')
                projection = 'Geodetic'; % Implicitement, l'export se fait en Geodetic
            end
    end
    
    switch projection
        case 'Geodetic'
            projName        = datum;
            prime_meridian  = 0;
            projMethod      = 'Geographic';
            projParameters  = [];
            
        case 'Mercator'
            projName        = sprintf('%s / World Mercator', datum);
            prime_meridian  = 0;
            projMethod      = 'Mercator (2SP)';
            projParameters  = sprintf(',%f,%f,%f,%f',...
                cartoDef.Projection.(projection).lat_ech_cons,...
                cartoDef.Projection.(projection).long_merid_orig,...
                cartoDef.Projection.(projection).X0,...
                cartoDef.Projection.(projection).Y0);
            
        case 'UTM'
            % A VERIFIER
            projName        = sprintf('%s / UTM zone %d%s', datum, cartoDef.Projection.(projection).Fuseau, cartoDef.Projection.(projection).Hemisphere);
            prime_meridian  = 0;
            projMethod      = 'Transverse Mercator';
            projParameters  = sprintf(',%f,%f,%f,%f,%f',...
                0,...                                                   % Latitude of natural origin
                cartoDef.Projection.(projection).Central_meridian+3,... % Longitude of natural origin  +3 car erreur dans SSC
                0.9996,...                                              % ??? Scale factor at natural origin
                cartoDef.Projection.(projection).X0,...                 % False Easting
                cartoDef.Projection.(projection).Y0);                   % False Northing
            
        case 'Lambert'
            % A VERIFIER
            projName        = sprintf('%s / %s', datum, cartoDef.Projection.(projection).strType{cartoDef.Projection.(projection).Type} );
            prime_meridian  = 0;
            projMethod      = 'Lambert Conic Conformal (2SP)';
            projParameters  = sprintf(',%f,%f,%f,%f,%f,%f',...
                cartoDef.Projection.(projection).first_paral,...        % Latitude of 1st standard parallel
                cartoDef.Projection.(projection).second_paral,...       % Latitude of 2nd standard parallel
                0,...    % Latitude of false origin
                cartoDef.Projection.(projection).long_merid_orig,...    % Longitude of false origin
                cartoDef.Projection.(projection).X0,...                 % False Origin Easting
                cartoDef.Projection.(projection).Y0);                   % False Origin Northing
            
        case 'Stereographique polaire'
            if strcmpi(cartoDef.Projection.(projection).hemisphere, 'N')
                sign = 1;
                projName = 'Arctic Polar Stereographic';
            else
                sign = -1;
                projName = 'Antarctic Polar Stereographic';
            end
            projName        = sprintf('%s / %s', datum, projName );
            prime_meridian  = 0;
            projMethod      = 'Polar Stereographic';
            projParameters  = sprintf(',%f,%f,%f,%f,%f',...
                cartoDef.Projection.(projection).lat_ech_cons,...       %PARAMETER["latitude_of_origin",-71],
                cartoDef.Projection.(projection).long_merid_horiz,...   %PARAMETER["central_meridian",0],
                1,...                                                   %PARAMETER["scale_factor",1],
                0,...                                                   %PARAMETER["false_easting",0],
                0);                                                     %PARAMETER["false_northing",0],
            
        otherwise %'other'
            my_warndlg(sprintf('Error GXF export :\n\n%s : %s','Unknown Projection', projection),  1);
            return
    end
    
    fprintf(GXFfile, '#MAP_PROJECTION\n"%s"\n"%s",%f,%f,%f\n"%s"%s\n',...
        projName,...
        datum,...
        cartoDef.Ellipsoide.DemiGrandAxe,...
        cartoDef.Ellipsoide.Excentricite,...
        prime_meridian,...
        projMethod,...
        projParameters);
end

%% Ecriture donn�es
switch lower(fileType)
    
    case {'uncompressed', 'kingdom'}
        
        fprintf(GXFfile, '%s\n', '#GRID');
        hWait = waitbar(0, Lang('Export GXF-3 en cours ...','Exporting GXF-3 ... '));
        for i = 1 : GXF.Rows % boucle sur les lignes
            if rem(i, round(GXF.Rows/20)) == 0
                waitbar(i/GXF.Rows, hWait);
            end
            j = 1;
            while j <= GXF.Points % boucle sur les points de chaque ligne
                txtLine = repmat(' ', 1, 80);
                nChar   = 0;
                word    = sprintf( getFormat(Z(i,j)), Z(i,j) );
                lenWord = numel(word);
                nChar   = nChar + lenWord + 1; % +1 pour ' '
                while nChar <= 80 && j <= GXF.Points % tant qu'on a pas 80 char
                    txtLine( nChar-lenWord : nChar ) = [word ' '];
                    word    = sprintf( getFormat(Z(i,j)), Z(i,j) );
                    lenWord = numel(word);
                    nChar   = nChar + lenWord + 1; % +1 pour ' '
                    j       = j+1;
                end
                fprintf(GXFfile, '%s\n', strtrim(txtLine) ); % on �crit la ligne
            end
        end
        my_close(hWait);
        
        
    case 'compressed'
        [GXF.Gtype, GXF.Transform(1), GXF.Transform(2)] = ...
            base90param(GXF.Zminimum, GXF.Zmaximum, precision);
        I90 = scaleBase90(Z, GXF.Transform);
        str = dec2base90(I90, GXF.Gtype);
        str = reshape(str', 1, GXF.Points*GXF.Gtype*GXF.Rows); % on met tout sur une seule ligne
        
        fprintf(GXFfile, '%s\n%d\n',     '#GTYPE', GXF.Gtype );
        fprintf(GXFfile, '%s\n%f,%f\n',  '#TRANSFORM', GXF.Transform );
        fprintf(GXFfile, '%s\n',       '#GRID');
        
        nLinePerRow    = ceil(GXF.Points*GXF.Gtype / 80);
        nCharPerRow    = floor(80/GXF.Gtype)*GXF.Gtype;
        nCharLastRow   = mod(GXF.Points*GXF.Gtype , 80);
        deb = 0;
        
        hWait = waitbar(0, Lang('Export GXF-3 compress� en cours ...','Exporting COMPRESSED GXF-3 ... '));
        for i = 1 : GXF.Rows
            if rem(i, round(GXF.Rows/20)) == 0
                waitbar(i/GXF.Rows, hWait);
            end
            linePerRow = 1;
            while linePerRow < nLinePerRow
                fprintf(GXFfile, '%s\n', str( deb + (1:nCharPerRow) ) );
                linePerRow  = linePerRow + 1;
                deb = deb + nCharPerRow ;
            end
            fprintf(GXFfile, '%s\n', str( deb + (1:nCharLastRow) ) );
            deb = deb + nCharLastRow ;
        end
        my_close(hWait);
        
        
    case 'repeatcompressed'
        [GXF.Gtype, GXF.Transform(1), GXF.Transform(2)] = ...
            base90param(GXF.Zminimum, GXF.Zmaximum, precision);
        I90 = scaleBase90(Z, GXF.Transform);
        str = dec2base90(I90, GXF.Gtype);
        
        fprintf(GXFfile, '%s\n%d\n',     '#GTYPE', GXF.Gtype );
        fprintf(GXFfile, '%s\n%f,%f\n',  '#TRANSFORM', GXF.Transform );
        fprintf(GXFfile, '%s\n',       '#GRID');
        
        hWait = waitbar(0, Lang('Export GXF-3 super compress� en cours ...','Exporting highly compressed GXF-3 ... '));
        for i = 1 : GXF.Rows
            if rem(i, round(GXF.Rows/20)) == 0
                waitbar(i/GXF.Rows, hWait);
            end
            
            str2printZip = '';
            curVal = 1;
            while curVal <= GXF.Points
                nRepeat = 1;
                while curVal < GXF.Points && ...
                        all( str( i, (1:GXF.Gtype) + (curVal-1)*GXF.Gtype) ==...
                        str( i, (1:GXF.Gtype) + curVal*GXF.Gtype ))
                    curVal  = curVal+1;
                    nRepeat = nRepeat+1;
                end
                if nRepeat > 3 % au moins une r�p�tition > 3 cons�cutive, sinon on ne gagne rien
                    str2printZip(end+1:end+GXF.Gtype) = repmat('"',  1, GXF.Gtype );
                    str2printZip(end+1:end+GXF.Gtype) = dec2base90( nRepeat, GXF.Gtype);
                    str2printZip(end+1:end+GXF.Gtype) = str( i, (1:GXF.Gtype) + (curVal-1)*GXF.Gtype );
                    curVal = curVal + 1;
                else
                    str2printZip(end+1:end+nRepeat*GXF.Gtype) = str( i, (1:nRepeat*GXF.Gtype) + (curVal-nRepeat)*GXF.Gtype );
                    curVal = curVal + 1;
                end
            end
            
            nChar          = numel(str2printZip);
            nCharPerRow    = floor(80/GXF.Gtype)*GXF.Gtype;
            deb = 0;
            while deb < nChar % linePerRow < nLinePerRow
                % est ce que les derniers caract�res contiennent un ",
                % signe d'une r�p�tion de valeur ?
                nCharPerRow    = min(nCharPerRow, nChar - deb);
                if (nChar - deb) >  nCharPerRow
                    quoteMark = regexp( str2printZip( deb + (nCharPerRow-(3*GXF.Gtype)+1:nCharPerRow) ), '["]', 'ONCE');
                else
                    quoteMark = [];
                end
                if isempty(quoteMark) || nCharPerRow == nChar
                    fprintf(GXFfile, '%s\n', str2printZip( deb + (1:nCharPerRow) ) );
                    deb         = deb + nCharPerRow ;
                else % on a trouve un " dans les 3 derni�res valeurs, on l'�crira sur la ligne suivante
                    fprintf(GXFfile, '%s\n', str2printZip( deb + ( 1:nCharPerRow - ( 3*GXF.Gtype - quoteMark + 1 ) )));
                    deb         = deb +  nCharPerRow - ( 3*GXF.Gtype - quoteMark + 1 ) ;
                end
                deb = min(nChar, deb);
            end
            
        end
        my_close(hWait);
        
end
flag = 1;
fclose(GXFfile);
helpdlg(Lang('Export r�ussi','Export successful'),'Export GXF-3');


% ------------------------------------------------------------------------
%                           SOUS-FONCTIONS
% -------------------------------------------------------------------------
function frmt = getFormat(var)
% D�termination automatique du format d'un type de donn�es
if ischar(var)
    frmt = '%s';
elseif isinteger(var)
    frmt = '%d';
elseif isfloat(var)
    if all(round(var) == var)
        frmt = '%d';
    else
        frmt = '%f';
    end
end

function Zb90 = scaleBase90(z, ScaleOffset)
% Transformation lin�aire des donn�es pour obtenir des entiers
Zb90 = floor( ( z - ScaleOffset(2) ) / ScaleOffset(1) );
Zb90(isnan(Zb90)) = 2*max(Zb90(:));

function p = nextPow10(n)
% Retourne la puissance de 10 sup�rieur � n
p = ceil( log10( abs(n) ) );

function p = nextPow(n,a)
% Retourne la puissance de a sup�rieur � n
p = ceil( log(n)./log(a) );

function dummy = getDummy(Zmin, Zmax)
% retourne le dummy appropri� pour le maximum donn�
if Zmin < 0 && Zmax > 0
    dummy = 10^nextPow10(Zmax) - 1;
    if dummy <= Zmax
        dummy = 10^(nextPow10(Zmax)+1) - 1;
    end
else
    dummy = 0;
end

function [p, scale, offset] = base90param(Zmin, Zmax, precision)
% retourne les param�tres pour transformation lin�aires des donn�es avant
% passage en base 90
offset  = Zmin;         % on se ramm�ne � z�ro
range   = Zmax - Zmin;  % on regarde la dynamique
p       = nextPow(range/precision, 90);
scale   = range/(90^p-1);

function ss = dec2base90(d, p)
%DEC2BASE90 Convertion d'un entier en base90 ASCII
% Exemples : dec2base(0) = '%' | dec2base90(728999,3) = '~~~'
% voir aussi DEC2BASE
% Input check
narginchk(1,2);
[r,c] = size(d);
d = d(:);
if ~(isnumeric(d) || ischar(d)) || any(d ~= floor(d)) || any(d < 0) || any(d > 1/eps)
    error('ExportGXF:dec2base90', '1stArg doit �tre un tableau d''entier positif');
end
if nargin == 2
    if ~isscalar(p) || ~(isnumeric(p) || ischar(p)) || p ~= floor(p) || p < 0
        error('ExportGXF:dec2base90:2ndArg');
    end
end

% Et hop on calcule ...
% ASCII char [37:126] =  ['%':'~']
b       = 90;
d       = double(d);
NaNs    = d < 0 | d > b^p-1;
n       = max( 1, round( log2( max(d(~NaNs))+1 ) / log2(b) ) );
while any(b.^n <= d(~NaNs))
    n = n + 1;
end
if nargin == 2
    n = max(n,p);
end
s(:,n) = rem(d,b);
while any(d) && n >1
    n       = n - 1;
    d       = floor(d/b);
    s(:, n) = rem(d,b);
end
s(NaNs, :) = -4*ones(sum(NaNs), p); % char ASCII 33 = "!"
ss = repmat('#', r, c*p);
for i=1:p
    ss(:, i:p:end-p+i) = char( reshape(s(:,i), r, c) + 37);
end
