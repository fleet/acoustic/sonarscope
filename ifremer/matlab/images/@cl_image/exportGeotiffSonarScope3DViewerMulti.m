% Exportation de l'image dans GLOBE
%
% Syntax
%   value = exportGeotiffSonarScope3DViewerMulti(a, ident, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_image
%
% Name-Value Pair Arguments
%   subx : Echantillonnage de la premiere dimension
%   suby : Echantillonnage de la deuxieme dimension
%   ...
%
% Output Arguments
%   value : Valeur du parametre ramene a l'unite
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   nomFic = tempname
%   exportGeotiffSonarScope3DViewerMulti(a, 'nomFic', nomFic)
%
% See also clc_image clc_image Authors
% Authors : JMA, GLU, MVN
% ----------------------------------------------------------------------------

function [flag, repExport] = exportGeotiffSonarScope3DViewerMulti(this, varargin) % subx et suby sont � inhiber

[varargin, repExport]   = getPropertyValue(varargin, 'repExport',   my_tempdir);
[varargin, FileName3DV] = getPropertyValue(varargin, 'FileName3DV', '');

str = 'This export can be avoided now. You can export this image in "Geotif 32 bits" and import it directlty in GLOBE.';
my_warndlg(str, 1);

if isempty(FileName3DV)
    if length(this) == 1
        [flagExportTexture, flagExportElevation, ~, nameFiltre] = getParamsDiversEtVaries(this);
        
        ImageName = code_ImageName(this, 'WithAttributs', 0);
        
        [flag, nomFic3D] = my_uiputfile('.xml', 'Save as', fullfile(repExport, [ImageName nameFiltre]));        
        if ~flag
            return
        end
        repExport = fileparts(nomFic3D);
        
        nomFic3D = strrep(nomFic3D, ',', '');
        nomFic3D = strrep(nomFic3D, ';', '');
        if isempty(nomFic3D)
            % GLU : il faudrait faire comme my_uiputfile si on clique sur OK alors qu'on a pas donn� de nom.
            str1 = 'Aucun nom de fichier n''a �t� donn�, recommencez svp.';
            str2 = 'No file was given, do it again.';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        end
        [~, nomLayer] = fileparts(nomFic3D);
        nomFic3D = strrep(nomLayer, '_elevation_v2', '');
        flag = export_3DViewer_InGeotifTiles(this, nomLayer, nomFic3D, repExport, flagExportTexture, flagExportElevation, varargin{:});
    else
        [flag, repExport] = my_uigetdir(repExport, Lang('S�lectionner un r�pertoire','Please select a folder'));
        if ~flag
            return
        end
        N = length(this);
        str1 = 'Export des images au format GLOBE en cours';
        str2 = 'Export images to GLOBE';
        hw = create_waitbar(Lang(str1,str2), 'N', N);
        for k=1:N
            my_waitbar(k, N, hw)
            [flagExportTexture, flagExportElevation, ~, nameFiltre] = getParamsDiversEtVaries(this(k));
            nomFic3D = code_ImageName(this(k), 'WithAttributs', 0);
            nomLayer = [nomFic3D nameFiltre(1:end-4)];
            flag = export_3DViewer_InGeotifTiles(this(k), nomLayer, nomFic3D, repExport, flagExportTexture, flagExportElevation, varargin{:});
        end
        my_close(hw, 'MsgEnd')
    end
else
    [flagExportTexture, flagExportElevation, ~, nameFiltre] = getParamsDiversEtVaries(this);
    nomFic3D = FileName3DV;
    nomLayer = [nomFic3D nameFiltre(1:end-4)];
    flag = export_3DViewer_InGeotifTiles(this, nomLayer, nomFic3D, repExport, flagExportTexture, flagExportElevation, varargin{:});
end

if flag
    str1 = sprintf('L''export GLOBE de "%s" est termin�.', nomLayer);
    str2 = sprintf('The export to GLOBE of "%s" is over.', nomLayer);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportSScGLOBETermine');
else
    str1 = sprintf('L''export 3DV de "%s" ne s''est pas bien termin�.', nomLayer);
    str2 = sprintf('The export to GLOBE of "%s" failed.', nomLayer);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportSScGLOBETermine');
end


function [flagExportTexture, flagExportElevation, filtre, nameFiltre] = getParamsDiversEtVaries(this)

isBathy = (this.DataType == 2);
if isBathy
    flagExportTexture   = false;
    flagExportElevation = true;
else
    flagExportTexture   = true;
    flagExportElevation = false;
end

% Selon le type de donn�es, on ajoute une extension propre au type de la donn�e.
if flagExportElevation
    filtre      = '*_elevation_v2.xml';
    nameFiltre  = '_elevation_v2.xml';
else
    filtre      = '*_texture_v2.xml';
    nameFiltre  = '_texture_v2.xml';
end
