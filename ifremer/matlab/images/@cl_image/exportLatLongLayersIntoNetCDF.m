% Example : 
%     global IfrTbx %#ok<GVMIS>
%     nomFicNetCDF        = [my_tempname '.nc'];
%     load(fullfile(IfrTbx, '..\SonarScopeTbxTests', 'ExportLatLongToNetCDF', 'listNomFicGeotif.mat'));
%     I0                  = cl_image();
% 
%     bk = pwd;
%     cd(fullfile(IfrTbx, '..\SonarScopeTbxTests', 'ExportLatLongToNetCDF'))
%     nomFicNetCDF        = exportLatLongLayersIntoNetCDF(I0, nomFicNetCDF, listNomFicGeotif)
%     cd(bk)
% ---------------------------------------------------------------------------------------

function nomFicNetCDF = exportLatLongLayersIntoNetCDF(I0, nomFicNetCDF, listNomFicGeotif)

%% Lecture du premier layer

[flag,this] = import_geotiff(I0, listNomFicGeotif{1}, 'flagAuto', 1, 'Mute', 1);
if ~flag
    return
end

%% Create directory

nomDir = fileparts(nomFicNetCDF);
if exist(nomDir, 'dir')
    if exist(nomFicNetCDF, 'file')
        delete(nomFicNetCDF);
        if exist(nomFicNetCDF, 'file')
            [nomDir, nomFic, Ext] = fileparts(nomFicNetCDF);
            nomFicNetCDF = fullfile(nomDir, [nomFic '-New' Ext]);
        end
    end
else
    mkdir(nomDir)
end

%% Check if "LatLong" geometry

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Get dimensions

nbLayers  = length(listNomFicGeotif);
nbRows    = this.nbRows;
nbColumns = this.nbColumns;

Longitude    = this.x;
Latitude     = this.y;
listDataType = cl_image.strDataType;


%% Write Latitude and Longitude

nccreate(nomFicNetCDF, 'lat', ...
        'Dimensions', {'lat', nbRows});
ncwrite(nomFicNetCDF, 'lat', Latitude);

nccreate(nomFicNetCDF, 'lon', ...
        'Dimensions', {'lon', nbColumns});
ncwrite(nomFicNetCDF, 'lon', Longitude);

%% Write global attributs

ncwriteatt(nomFicNetCDF, '/', 'Conventions', 'CF 1.6');
ncwriteatt(nomFicNetCDF, '/', 'ProjectCode', 'Sonarscope');
ncwriteatt(nomFicNetCDF, '/', 'Comment', 'Created by GLU');
ncwriteatt(nomFicNetCDF, '/', 'Conventions', 'CF 1.6');
ncwriteatt(nomFicNetCDF, '/', 'Proj4String', '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs');
ncwriteatt(nomFicNetCDF, '/', 'creator_name', 'JMA-GLU');
ncwriteatt(nomFicNetCDF, '/', 'creator_email', 'augustin@ifremer.fr');
ncwriteatt(nomFicNetCDF, '/', 'history', sprintf('%s : Creation', datestr(now, 'yyyy-mm-dd HH:MM:SS')));

%% Write Coordinte Reference System variable
nccreate(nomFicNetCDF, 'crs', 'Dimensions', {'nbcols', 1, 'nbrows', 1}); 
ncwrite(nomFicNetCDF, 'crs', int32(1));
ncwriteatt(nomFicNetCDF, '/', 'grid_mapping_name', 'latitude_longitude');
ncwriteatt(nomFicNetCDF, '/', 'epsg_code',  'EPSG:4326');
ncwriteatt(nomFicNetCDF, '/', 'longitude_of_prime_meridian', 0.0);
ncwriteatt(nomFicNetCDF, '/', 'semi_major_axis', 6378137.0);
ncwriteatt(nomFicNetCDF, '/', 'inverse_flattening', 298.257223563);

%% Write layers

str1 = 'Ecriture des layers de g�om�trie LatLong dans le fichier NetCDF';
str2 = 'Writing Netcdf file of LatLong geometry';
hw = create_waitbar(Lang(str1,str2), 'N', nbLayers);
for k=1:nbLayers
    my_waitbar(k, nbLayers, hw)
    [flag, this] = import_geotiff(I0, listNomFicGeotif{k}, 'flagAuto', 1, 'Mute', 1);
    if ~flag
        return
    end
    
    if k == 1
        x = this.x;
        y = this.y;
    else % Il arrive parfois que les layers n'ont pas exactement les m�me dimensions
        if ~isequal(this.x, x) || ~isequal(this.y, y)
            [that, flag] = extraction(this, 'x', x, 'y', y, 'ForceExtraction');
            if flag
                this = that;
                clear that
            end
        end
    end
    
    nomLayer = this.Name;
    nccreate(nomFicNetCDF, nomLayer, ...
        'Dimensions', {'lon', nbColumns, 'lat', nbRows}, ...
        'FillValue', this.ValNaN); 
    X = this.Image(:,:);
    ncwrite(nomFicNetCDF, nomLayer, X');
    ncwriteatt(nomFicNetCDF, nomLayer, 'unitVar', this.Unit);
    ncwriteatt(nomFicNetCDF, nomLayer, 'standard_name', listDataType{this.DataType});
    ncwriteatt(nomFicNetCDF, nomLayer, 'long_name', nomLayer);
    ncwriteatt(nomFicNetCDF, nomLayer, 'grid_mapping', 'crs');
    % Attribute de type ancillary variables 
    % ncwriteatt(nomFicNetCDF, nomLayer, 'fill_value', this.ValNaN);
end
my_close(hw, 'MsgEnd');


%{
finfo = ncinfo(nomFicNetCDF)
finfo.Dimensions(1)
finfo.Dimensions(2)
finfo.Dimensions(3)
finfo.Variables(1)
finfo.Variables(2)
finfo.Variables(3)
%}
