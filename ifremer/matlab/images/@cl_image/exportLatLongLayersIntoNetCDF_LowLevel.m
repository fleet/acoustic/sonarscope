% TODO : Fonction jamais appel�e !!!!!!!!!!!!!!!!!! Cr�e par GLU ?
% Exemple : 
%     global IfrTbx %#ok<GVMIS>
%     nomFicNetCDF        = [my_tempname '.nc'];
%     load(fullfile(IfrTbx, '..\SonarScopeTbxTests', 'ExportLatLongToNetCDF', 'listNomFicGeotif.mat'));
%     I0                  = cl_image();
% 
%     bk = pwd;
%     cd(fullfile(IfrTbx, '..\SonarScopeTbxTests', 'ExportLatLongToNetCDF'))
%     nomFicNetCDF        = exportLatLongLayersIntoNetCDF_LowLevel(I0, nomFicNetCDF, listNomFicGeotif)
%     cd(bk)
% ---------------------------------------------------------------------------------------

function nomFicNetCDF = exportLatLongLayersIntoNetCDF_LowLevel(I0, nomFicNetCDF, listNomFicGeotif)

%% Lecture du premier layer

[flag,this] = import_geotiff(I0, listNomFicGeotif{1}, 'flagAuto', 1, 'Mute', 1);
if ~flag
    return
end

%% Create directory

nomDir = fileparts(nomFicNetCDF);
if exist(nomDir, 'dir')
    if exist(nomFicNetCDF, 'file')
        delete(nomFicNetCDF);
        if exist(nomFicNetCDF, 'file')
            [nomDir, nomFic, Ext] = fileparts(nomFicNetCDF);
            nomFicNetCDF = fullfile(nomDir, [nomFic '-New' Ext]);
        end
    end
else
    mkdir(nomDir)
end

%% Check if "LatLong" geometry

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Get dimensions

nbLayers     = length(listNomFicGeotif);
Longitude    = this.x;
Latitude     = this.y;
listDataType = cl_image.strDataType;

%%

% X           = this.Image(:,:);
nomLayer    = this.Name;

S.att(1).name               = 'Title';
S.att(end).val              = nomLayer;
S.att(end+1).name           = 'ProjectCode';
S.att(end).val              = 'Sonarscope';
S.att(end+1).name           = 'Comment';
S.att(end).val              = 'Created by GLU';
S.att(end+1).name           = 'Conventions';
S.att(end).val              = 'CF 1.6';
S.att(end+1).name           = 'Proj4String';
S.att(end).val              = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
S.att(end+1).name           = 'creator_name';
S.att(end).val              = 'JMA-GLU';
S.att(end+1).name           = 'creator_email';
S.att(end).val              = 'augustin@ifremer.fr';
S.att(end+1).name           = 'history';
S.att(end).val              = sprintf('%s : Creation', datestr(now, 'yyyy-mm-dd HH:MM:SS'));

S.dim(1).name               = 'lon';
S.dim(end).val              = numel(Longitude);
S.dim(end+1).name           = 'lat';
S.dim(end).val              = numel(Latitude);

S.var(1).name               = 'lon';
S.var(end).type             = 'double';
S.var(end).dim(1).name      = 'lon';
S.var(end).att(1).name      = 'standard_name';
S.var(end).att(end).val     = 'longitude';
S.var(end).att(end+1).name  = 'long_name';
S.var(end).att(end).val     = 'longitude';
S.var(end).att(end+1).name  = 'units';
S.var(end).att(end).val     = 'degrees_east';
S.var(end).att(end+1).name  = 'axis';
S.var(end).att(end).val     = 'X';
S.var(end).att(end+1).name  = 'valid_min';
S.var(end).att(end).val     = -180;
S.var(end).att(end+1).name  = 'valid_max';
S.var(end).att(end).val     = +180;
S.var(end).val              = Longitude;

% 1�re variable
% -------------
S.var(end+1).name           = 'lat';
S.var(end).type             = 'double';
S.var(end).dim(1).name      = 'lat';
S.var(end).att(1).name      = 'standard_name';
S.var(end).att(end).val     = 'latitude';
S.var(end).att(end+1).name  = 'long_name';
S.var(end).att(end).val     = 'latitude';
S.var(end).att(end+1).name  = 'units';
S.var(end).att(end).val     = 'degrees_north';
S.var(end).att(end+1).name  = 'axis';
S.var(end).att(end).val     = 'Y';
S.var(end).att(end+1).name  = 'valid_min';
S.var(end).att(end).val     = -90;
S.var(end).att(end+1).name  = 'valid_max';
S.var(end).att(end).val     = +90;
S.var(end).val              = Latitude;

% Ajout du champ crs (Coordinate Reference System) comme ancillary variable
S.var(end+1).name           = 'crs';
S.var(end).type             = 'int';
S.var(end).att(1).name      = 'grid_mapping_name';
S.var(end).att(end).val     = 'latitude_longitude';
S.var(end).att(end+1).name  = 'epsg_code';
S.var(end).att(end).val     = 'EPSG:4326';
S.var(end).att(end+1).name  = 'longitude_of_prime_meridian';
S.var(end).att(end).val     = 0.0;
S.var(end).att(end+1).name  = 'semi_major_axis';
S.var(end).att(end).val     = 6378137.0;
S.var(end).att(end+1).name  = 'inverse_flattening';
S.var(end).att(end).val     = 298.257223563;
S.var(end).val              = 1;

str1 = 'Ecriture des layers de g�om�trie LatLong dans le fichier NetCDF';
str2 = 'Writing Netcdf file of LatLong geometry';
hw = create_waitbar(Lang(str1,str2), 'N', nbLayers);
for k=1:nbLayers
    my_waitbar(k, nbLayers, hw)
    [flag, this] = import_geotiff(I0, listNomFicGeotif{k}, 'flagAuto', 1, 'Mute', 1);
    if ~flag
        return
    end
    
    if k == 1
        x = this.x;
        y = this.y;
    else % Il arrive parfois que les layers n'ont pas exactement les m�me dimensions
        if ~isequal(this.x, x) || ~isequal(this.y, y)
            [that, flag] = extraction(this, 'x', x, 'y', y, 'ForceExtraction');
            if flag
                this = that;
                clear that
            end
        end
    end
    
    nomLayer  = this.Name;
    X         = this.Image(:,:);
    unitVar   = this.Unit;
    stdName   = listDataType{this.DataType};
    fillValue = this.ValNaN;
    
    % Variable Data
    % -------------
    S.var(end+1).name           = nomLayer;
    S.var(end).val              = double(X');
    S.var(end).dim(1).name      = 'lon';
    S.var(end).dim(2).name      = 'lat';
    S.var(end).type             = 'double';
    S.var(end).att(end+1).name  = 'grid_mapping';
    S.var(end).att(end).val     = 'crs'; % Attribute de type ancillary variables 
    S.var(end).att(end+1).name  = 'standard_name';
    S.var(end).att(end).val     = stdName; %
    S.var(end).att(end+1).name  = 'long_name';
    S.var(end).att(end).val     = nomLayer; 
    S.var(end).att(end+1).name  = 'unit';
    S.var(end).att(end).val     = unitVar;  
    S.var(end).att(end+1).name  = 'fill_value';
    S.var(end).att(end).val     = fillValue; 
end

idGlobalAtt = -1; % netcdf.getConstant('NC_GLOBAL');

% D�finition des variables
nVar  = numel(S.var);
idVar = zeros(1, nVar);

ncID = netcdf.create(nomFicNetCDF, 'CLOBBER');
% Ecriture des attributs globales.
for k=1:numel(S.att)
    netcdf.putAtt(ncID, idGlobalAtt, S.att(k).name, S.att(k).val);
end

for k=1:numel(S.dim)
    idDefDims = netcdf.defDim(ncID, S.dim(k).name, S.dim(k).val); %#ok<NASGU>
end

for v=1:nVar   
    nDims   = numel(S.var(v).dim); 
    idDims  = [];
    % On conserve idDims = [] pour les champs ancillary (crs)
    if nDims
        for d=1:nDims
            idDims(d)  = netcdf.inqDimID(ncID, S.var(v).dim(d).name); %#ok<AGROW>
        end
    end
    
    idVar(v) = netcdf.defVar(ncID,S.var(v).name,S.var(v).type,idDims);
    % Possibilit� de compression en NetCDF4
    % netcdf.defVarDeflate(ncID,idVar(v),true,true,5);
    % Ajout des attributs de variables
    for a=1:numel(S.var(v).att)
        netcdf.putAtt(ncID, idVar(v), S.var(v).att(a).name, S.var(v).att(a).val);
    end
end
% Sortie du Mode Define pour ajout des donn�es.
netcdf.endDef(ncID);

% Ecriture des donn�es des variables.
for v=1:nVar
    % Si on n'�crit par une donn�e scalaire (type ancillary)
    if ~isempty(S.var(v).val)
        netcdf.putVar(ncID, idVar(v), S.var(v).val);
    end
end

% Fermeture du fichier NetCDF
netcdf.close(ncID);

%{
finfo = ncinfo(nomFicNetCDF)
finfo.Dimensions(1)
finfo.Dimensions(2)
finfo.Dimensions(3)
finfo.Variables(1)
finfo.Variables(2)
finfo.Variables(3)
%}
