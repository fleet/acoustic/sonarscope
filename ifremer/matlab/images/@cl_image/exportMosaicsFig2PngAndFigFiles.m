function [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, ...
    nomFicPngInverseVideo, nomFicKmzInverseVideo] = exportMosaicsFig2PngAndFigFiles(this, nomDirSummary, SurveyName, Tag)

DT = cl_image.indDataType('Reflectivity');
if this.DataType == DT % Image en inverse vid�o
    this.Video = 1;
end

Fig = imagesc(this);
ImageName = this.Name;
if isempty(Tag)
    FileName = ImageName;
else
    FileName = [ImageName '-' Tag];
end

nomFicFig = fullfile(nomDirSummary, SurveyName, 'FIG', [FileName '.fig']);
savefig(Fig, nomFicFig)

nomFicPng = fullfile(nomDirSummary, SurveyName, 'PNG', [FileName '.png']);
fig2fic(Fig, nomFicPng)
close(Fig);

nomFicKmz = fullfile(nomDirSummary, SurveyName, 'KMZ', [FileName '.kmz']);
export_GoogleEarth(this, 'nomFic', nomFicKmz, 'flagFillNaN', 1);
winopen(nomFicKmz)

nomFicGeotif = fullfile(nomDirSummary, SurveyName, 'GEOTIF', [FileName '.tif']);
if this.ImageType == 1 % '1=Intensity' | '2=RGB' | '3=Indexed' | '4=Boolean'  
    TypeGeotif = 'Geotif 32 bits file (ready for GIS)';
    export_geoTiff32Bits(this, nomFicGeotif, 'Mute', 1);
else
    TypeGeotif = 'Classical Geotif file';
    export_tif(this, nomFicGeotif);
%     winopen(nomFicGeotif)
end

%% Cas d'une image de r�flectivit�

if (this.DataType == DT) && (nargout == 8) % Image en inverse vid�o
    this.Video = 2;
    
    Fig = imagesc(this);
    FileName = [FileName, '-InverseVideo'];
        
    nomFicPngInverseVideo = fullfile(nomDirSummary, SurveyName, 'PNG', [FileName '.png']);
    fig2fic(Fig, nomFicPngInverseVideo)
    close(Fig);
    
    nomFicKmzInverseVideo = fullfile(nomDirSummary, SurveyName, 'KMZ', [FileName '.kmz']);
    export_GoogleEarth(this, 'nomFic', nomFicKmzInverseVideo, 'flagFillNaN', 1);
    winopen(nomFicKmzInverseVideo)
else
    nomFicPngInverseVideo = [];
    nomFicKmzInverseVideo = [];
end
