function exportPingBeamLayersIntoASCII(this, nomFicASCII)

%% Comma or not comma ?

sep = getCSVseparator;

%% Create directory

nomDir = fileparts(nomFicASCII);
if ~exist(nomDir, 'dir')
    mkdir(nomDir)
end

nbPings = this.nbRows;
nbBeams = this.nbColumns;

nbLayers = length(this);
Line     = NaN(nbLayers, nbBeams);
Format   = cell(nbLayers,1);

%% Cr�ation du fichier

fid = fopen(nomFicASCII, 'w+');

%% Write header

fprintf(fid, 'Ping%sBeam%sTime%sTime UTC%s', sep, sep, sep, sep);
for k3=1:nbLayers
    nomLayer = this(k3).Name;
    fprintf(fid, '%s;', nomLayer);
    switch this(k3).DataType
        case 2 % Bathymetry
            Format{k3} = '%.2f';
        case 3 % Reflectivity
            Format{k3} = '%.2f';
        case {12; 13} % SlopeAlong, SlopeAcross
            Format{k3} = '%.2f';
        case {5; 88; 97; 4} % IncidenceAngle, BeamPointingAngle, RxAngleEarth, TxAngle
            Format{k3} = '%.3f';
        case 73 % AbsorptionCoeff
            Format{k3} = '%.3f';
        case 8 % TVG
            Format{k3} = '%.3f';
        case 25 % InsonifiedAreadB
            Format{k3} = '%.3f';
        case {31; 32} % Latitude, Longitude
            Format{k3} = '%.14f';
        case {40; 41} % GeoX, GeoY
            Format{k3} = '%.14f';
        otherwise
            my_breakpoint
            this(k3).DataType
            cl_image.strDataType{this(k3).DataType} %#ok<NOPRT>
            Format(k3) = '%.1f';
    end
end

fprintf(fid, '\n');

%% Loop on Pings and Beams

format longG
TimeTx = datetime(this(1).Sonar.Time.timeMat, 'ConvertFrom', 'datenum');
str1 = sprintf('Export fichier %s', nomFicASCII);
str2 = sprintf('Export file %s', nomFicASCII);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
for k1=1:nbPings
    my_waitbar(k1, nbPings, hw)
    
    for k3=1:nbLayers
        Line(k3,:) = this(k3).Image(k1,:);
    end
    
    for k2=1:nbBeams
        fprintf(fid, '%d%s %d%s %s%s %0.3f%s', k1, sep, k2, sep, TimeTx(k1), sep, posixtime(TimeTx(k1)), sep);
        
        for k3=1:nbLayers
        	fprintf(fid, [Format{k3} '; '], Line(k3,k2));
%             switch get(this(k3), 'ClassName')
%                 case 'double'
%                     fprintf(fid, '%s;', num2strPrecis(Line(k3,k2)));
%                 case 'single'
%                     fprintf(fid, '%s;', num2strPrecis(single(Line(k3,k2))));
%                 otherwise
%                     fprintf(fid, '%d;', Line(k3,k2));
%             end
        end
        fprintf(fid, '\n');
    end
end
format long

%% Fermeture du fichier

fclose(fid);
my_close(hw, 'MsgEnd')
