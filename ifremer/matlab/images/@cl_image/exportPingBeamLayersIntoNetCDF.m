%   nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = import_PingBeam_All_V2(aKM);
%   SonarScope(b);
%   nomFicNetCDF = my_tempname('.nc')
%   exportPingBeamLayersIntoNetCDF(b, nomFicNetCDF)

function nomFicNetCDF = exportPingBeamLayersIntoNetCDF(this, nomFicNetCDF)

%% Check if "PingBeam" geometry

flag = testSignature(this(1), 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Create directory

nomDir = fileparts(nomFicNetCDF);
if exist(nomDir, 'dir')
    if exist(nomFicNetCDF, 'file')
        delete(nomFicNetCDF);
        if exist(nomFicNetCDF, 'file')
            [nomDir, nomFic, Ext] = fileparts(nomFicNetCDF);
            nomFicNetCDF = fullfile(nomDir, [nomFic '-New' Ext]);
        end
    end
else
    mkdir(nomDir)
end

%% Get dimensions

nbLayers = length(this);
nbPings  = this(1).nbRows;
nbBeams  = this(1).nbColumns;

% nccreate(nomFicNetCDF, 'nbPings', ...
%     'Dimensions', {'nbPings', nbPings}, ...
%     'FillValue', NaN);
% X = linspace(5555, 6666, nbPings);
% ncwrite(nomFicNetCDF, 'nbPings', X);

%% Prepare TimeTx

format longG
TimeTx = datetime(this(1).Sonar.Time.timeMat, 'ConvertFrom', 'datenum');
strTimeTx = char(TimeTx);
TimeUTC = posixtime(TimeTx);

%% Write Time UTC

nccreate(nomFicNetCDF, 'TimeUTC', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TimeUTC', TimeUTC);

%% Write Time in characters

nccreate(nomFicNetCDF, 'TimeTx', ...
        'Dimensions', {'nbPings', nbPings, 'nbCharTime', size(strTimeTx,2)}, ...
        'Datatype', 'char', ...
        'FillValue', 'disable');
ncwrite(nomFicNetCDF, 'TimeTx', strTimeTx);

%% Write layers

str1 = 'Ecriture des layers de g�om�trie PingBeam dans le fichier NetCDF';
str2 = 'Writing Netcdf file of PingBeam geometry layers';
hw = create_waitbar(Lang(str1,str2), 'N', nbLayers);
for k3=1:nbLayers
    my_waitbar(k3, nbLayers, hw)
    nomLayer = this(k3).Name;
    nccreate(nomFicNetCDF, nomLayer, ...
        'Dimensions', {'nbPings', nbPings, 'nbBeams', nbBeams}, ...
        'FillValue', NaN);
    X = this(k3).Image(:,:);
    ncwrite(nomFicNetCDF, nomLayer, X);
end
my_close(hw, 'MsgEnd');

%% Write signals

X = get(this(1), 'FishLatitude');
nccreate(nomFicNetCDF, 'TxLatitude', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TxLatitude', X);

X = get(this(1), 'FishLongitude');
nccreate(nomFicNetCDF, 'TxLongitude', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TxLongitude', X);

X = get(this(1), 'Heading');
nccreate(nomFicNetCDF, 'TxHeading', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TxHeading', X);

X = get(this(1), 'Roll');
nccreate(nomFicNetCDF, 'TxRoll', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TxRoll', X);

X = get(this(1), 'Pitch');
nccreate(nomFicNetCDF, 'TxPitch', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TxPitch', X);

X = get(this(1), 'Heave');
nccreate(nomFicNetCDF, 'TxHeave', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TxHeave', X(:,1)); % TODO

X = get(this(1), 'SurfaceSoundSpeed');
nccreate(nomFicNetCDF, 'TxSurfaceSoundSpeed', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TxSurfaceSoundSpeed', X);

X = get(this(1), 'PingCounter');
nccreate(nomFicNetCDF, 'PingCounter', ...
        'Dimensions', {'nbPings', nbPings}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'PingCounter', X);

format long