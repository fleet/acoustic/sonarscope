% Exporte l'image en differents formats raster. Si l'image est tros grosse, la
% fonction peut la decouper en blocks.
%
% Syntax
%   export(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   nomFic   : Non du fichier de sortie (Une numerotation est rajoutee si decoupage par blocks)
%   Format   : {'mat'}, 'tif'
%   nbLigMax : Nombre maximum de lignes par block (Inf par defaut)
%   nbColMax : Nombre maximum de colonnes par block (Inf par defaut)
%   CLim     : Limites du rehaussement de contraste si format d'image 8bits.
%              Si cette valeur n'est pas donnee, c'est le CLim de this qui est utilise
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   nomFic = tempname
%   exportRaster(a, 'nomFic', nomFic, 'Format', 'tif')
%
% See also cl_image cl_image/set cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function exportRaster(this, varargin)

[varargin, XLim] = getPropertyValue(varargin, 'XLim', []);
[varargin, YLim] = getPropertyValue(varargin, 'YLim', []);
[varargin, Quality] = getPropertyValue(varargin, 'Quality', 75);
[varargin, ViewerSelectionne] = getPropertyValue(varargin, 'ViewerSelectionne', []);
[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask] = getPropertyValue(varargin, 'valMask', []);
[varargin, zone] = getPropertyValue(varargin, 'zone', []);


if length(this) ~= 1
    str1 = 'Une image a la fois svp.';
    str2 = 'One image at a time please.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if isempty(this.Image)
    str1 = 'L''image est vide.';
    str2 = 'The image is empty.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImageVidePourExport', 'TimeDelay', 10);
    return
end


if isempty(XLim)
    [varargin, subx] = getPropertyValue(varargin, 'subx', 1:this.nbColumns);
else
    subx = find((this.x >= XLim(1)) & (this.x <= XLim(2)));
end

if isempty(YLim)
    [varargin, suby] = getPropertyValue(varargin, 'suby', 1:this.nbRows);
else
    suby = find((this.y >= YLim(1)) & (this.y <= YLim(2)));
end

if isempty(subx) || isempty(suby)
    return
end

[varargin, ext]             = getPropertyValue(varargin, 'Format', 'mat');
[varargin, nbLigMax]        = getPropertyValue(varargin, 'nbLigMax', []);
[varargin, nbColMax]        = getPropertyValue(varargin, 'nbColMax', []);
[varargin, flagVisuExterne] = getPropertyValue(varargin, 'VisuExterne', 1);
[varargin, CLim]            = getPropertyValue(varargin, 'CLim', this.CLim);
[varargin, nomFic]          = getPropertyValue(varargin, 'nomFic', my_tempname); %#ok<ASGLU>

[nomDir, nomFic] = fileparts(nomFic);
nomFic = fullfile(nomDir, nomFic);

if isempty(nbColMax)
    nbColMax = length(subx);
end
nbBlkCol = ceil(length(subx) / nbColMax);

if isempty(nbLigMax)
    nbLigMax = length(suby);
end
nbBlkLig = ceil(length(suby) / nbLigMax);

kBlock = 0;
nbBlocks = nbBlkCol * nbBlkLig;
str1 = 'Export de l''image par tuilage en cours';
str2 = 'Exporting the image';
hw = create_waitbar(Lang(str1,str2), 'N', nbBlocks);
% strFullImage = char(this);
for kCol=1:nbBlkCol
    subxBlock = (kCol-1)* nbColMax + (1:nbColMax);
    subxBlock(subxBlock > length(subx)) = [];
    x = this.x(subx(subxBlock));%#ok
    for kLig=1:nbBlkLig
        my_waitbar(kBlock, nbBlocks, hw)
        kBlock = kBlock + 1;
        
        subyBlock = (kLig-1)* nbLigMax + (1:nbLigMax);
        subyBlock(subyBlock > length(suby)) = [];
        
        y = this.y(suby(subyBlock));%#ok
        %         I = this.Image(suby(subyBlock), subx(subxBlock),1);
        %
        %         nbPixNonNan = sum(sum(~isnan(I)));
        %         if nbPixNonNan ~= 0
        %                 figure; imagesc(x, y, I, this.CLim);
        if (nbBlkCol*nbBlkLig) == 1
            nomFicTile = sprintf('%s.%s', nomFic, ext);
        else
            nomFicTile = sprintf('%s_%02d_%02d.%s', nomFic, kLig, kCol, ext);
        end
        
        switch ext
            case 'jpg'
                flag = export_tif(this, nomFicTile, 'CLim', CLim, 'suby', suby(subyBlock), 'subx', subx(subxBlock), ...
                    'Quality', Quality);
                
            case {'tif'; 'tiff'; 'bmp'; 'png'; 'hdf'; 'xwd'; 'ras'; 'pbm'; 'pgm'; 'ppm'; 'pnm'}
                flag = export_tif(this, nomFicTile, 'CLim', CLim, 'suby', suby(subyBlock), 'subx', subx(subxBlock),...
                    'LayerMask', LayerMask, 'valMask', valMask, 'zone', zone);
                
            case 'gif'
                flag = export_gif(this, nomFicTile, 'CLim', CLim, 'suby', suby(subyBlock), 'subx', subx(subxBlock));
                
            otherwise
                str1 = sprintf('cl_image/exportRaster Type de fichier "%s" demand� mais pas encore pr�vu au programme.', ext);
                str2 = sprintf('cl_image/exportRaster Type of file "%s" not ready in SSc.', ext);
                my_warndlg(Lang(str1,str2), 1);
                return
        end
        
        if flag && flagVisuExterne
            ViewerSelectionne = visuExterne(nomFicTile, 'ViewerSelectionne', ViewerSelectionne);
        end
    end
end
my_close(hw, 'MsgEnd')
