% Exportation des signatures de texture
%
% Syntax
%   exportSegmSignature(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function exportSegmSignature(this, nomFicSave, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.Texture));

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if isempty(sub)
    return
end

if ~isempty(nomFicSave)
    Texture = this.Texture(sub);
    flag = exist(nomFicSave, 'file');
    if flag == 2
        msg1 = sprintf('Le fichier "%s" existe. Que voulez-vous faire ?', nomFicSave);
        msg2 = sprintf('File "%s" exists. What do we do ?', nomFicSave);
        [ButtonName, flag] = my_questdlg(Lang(msg1,msg2), Lang('TODO', 'Overwrite'), Lang('TODO','Append'));
        if ~flag
            return
        end
        if ButtonName == 1
            save(nomFicSave, 'Texture');
        else
            X = Texture;
            Y = loadmat(nomFicSave);
            Texture = [Y X];
            save(nomFicSave, 'Texture');
        end
    else
        save(nomFicSave, 'Texture');
    end
end
