function flag = export_3DVMultiImages(EI, nomFicMultiImageXML, varargin)

[varargin, sizeTiff] = getPropertyValue(varargin, 'sizeTiff', 2500); %#ok<ASGLU>

N = length(EI);
indIndexType = zeros(1, N);
indLayers    = 1:N;
[flag, cm] = creer_clMultidata(EI, nomFicMultiImageXML, indIndexType, indLayers, 1);
if ~flag
    return
end

cm = reorganization(cm, 1);

% Export des layers de donn�es et d'index.
flag = exportGeotiffSonarScope3DViewerMultiV3(cm, nomFicMultiImageXML, 'sizeTiff', sizeTiff);
