function flag = export_3DViewer_InGeotifTiles(this, nomLayer, nomFic3D, repExport, flagExportTexture, flagExportElevation, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, sizeTiff] = getPropertyValue(varargin, 'sizeTiff', 2500); %#ok<ASGLU>

% flagRunSSC3DViewer = 0;

str1 = sprintf('Export GLOBE de "%s" en cours', nomLayer);
str2 = sprintf('GLOBE export of %s"', nomLayer);
WorkInProgress(Lang(str1,str2))

%% Ecriture des images

nx          = length(subx);
ny          = length(suby);
TailleBlocX = min(nx,sizeTiff);
TailleBlocY = min(ny,sizeTiff);
minVal      = NaN;
maxVal      = NaN;
% I = get(this, 'Image');
k = 0;
N1 = max(1, floor(nx/TailleBlocX));
N2 = max(1, floor(nx/TailleBlocY));
str1 = 'Export des fichiers geotif en cours';
str2 = 'Exporting geotif images';
hw = create_waitbar(Lang(str1,str2), 'N', N1*N2);

% TODO : TESTER SI CA RESOUD LES PBS DANS 3DV
% delta   = 50;
% delta   = 64;
delta   = 0;
% Tests results :
% delta =1    FAILED
% delta =2    FAILED
% delta =100  PASSED
% delta =50   PASSED

for kx=1:TailleBlocX:nx
    subsubx = (kx-delta):(kx+delta+TailleBlocX);
    subsubx(subsubx < 1) = [];
    subsubx(subsubx > nx) = [];
    for ky=1:TailleBlocY:ny
        my_waitbar(k, N1*N2, hw)
        
        subsuby = (ky-delta):(ky+delta+TailleBlocY);
        subsuby(subsuby < 1) = [];
        subsuby(subsuby > ny) = [];
        
        %         figure(7675); imagesc(I(suby(subsuby),subx(subsubx))); drawnow; pause
        
        % TODO : Futur
        %         suffixeElevation = '_V2_elevation';
        %         suffixeTexture   = '_V2_texture';
        % TODO : Avant
        %         suffixeElevation = '_elevation_v2';
        %         suffixeTexture   = '_texture_v2';
        nomFic3Dxy = sprintf('%s_%d_%d', nomFic3D, (kx-1)/TailleBlocX, (ky-1)/TailleBlocY);
        if flagExportElevation
            [flag, nomFic, minBloc, maxBloc, XLim, YLim] = export_geoTiff(this, 'subx', ...
                subx(subsubx), 'suby', suby(subsuby), 'repExport', repExport, ...
                'newDir', [nomFic3D '_elevation_v2'], 'nomFic', [nomFic3Dxy '_elevation_v2.tif'], ...
                'nbChanels', 1, 'NoNan', true, 'Type', 'single', 'Texture', false); %#ok<ASGLU>
            if ~flag
                return
            end
            if flagExportTexture
                % En int16 [flag, nomFic, minBloc, maxBloc, XLim, YLim] = export_geoTiff(this, 'subx', subx(subsubx), 'suby', suby(subsuby), 'repExport', repExport, 'newDir', [nomFic3D '_elevation_v2'], 'nomFic', [nomFic3Dxy '_texture_v2.tif'], 'nbChanels', 1, 'NoNan', true, 'Type', 'int16', 'Texture', true);
                [flag, nomFic, minBloc, maxBloc, XLim, YLim] = export_geoTiff(this, 'subx', ...
                    subx(subsubx), 'suby', suby(subsuby), 'repExport', repExport, ...
                    'newDir', [nomFic3D '_texture_v2'], ...
                    'nomFic', [nomFic3Dxy '_texture_v2.tif'], ...
                    'nbChanels', 1, 'NoNan', true, 'Type', 'single', ...
                    'Texture', true); %#ok<ASGLU>
                if ~flag
                    return
                end
            end
        else
            % En int16 [flag, nomFic, minBloc, maxBloc, XLim, YLim] = export_geoTiff(this, 'subx', subx(subsubx), 'suby', suby(subsuby), 'repExport', repExport, 'newDir', [nomFic3D '_texture_v2'], 'nomFic', [nomFic3Dxy '_texture_v2.tif'], 'nbChanels', 1, 'NoNan', true, 'Type', 'int16', 'Texture', true);
            % En Float 32
            %             [flag, nomFic, minBloc, maxBloc, XLim, YLim] = export_geoTiff(this, 'subx', ...
            %                 subx(subsubx), 'suby', suby(subsuby), 'repExport', repExport, ...
            %                 'newDir', [nomFic3D '_texture_v2'], ...
            %                 'nomFic', [nomFic3Dxy '_texture_v2.tif'], ...
            %                 'nbChanels', 1, 'NoNan', true, 'Type', 'single', ...
            %                 'Texture', true);
            
            %% Modifi� le 14/02/2012 : 'nbChanels', 1 supprim� pour image stellite Wellington
            [flag, nomFic, minBloc, maxBloc, XLim, YLim] = export_geoTiff(this, 'subx', ...
                subx(subsubx), 'suby', suby(subsuby), 'repExport', repExport, ...
                'newDir', nomFic3D, ...
                'nomFic', [nomFic3Dxy '_texture_v2.tif'], ...
                'NoNan', true, 'Type', 'single', ...
                'Texture', true); %#ok<ASGLU>
            if ~flag
                return
            end
        end
        minVal = min(minVal, minBloc);
        maxVal = max(maxVal, maxBloc);
        
        k = k +1;
        nomFic3DTile{k} = nomFic3Dxy; %#ok<AGROW>
        XLimTile{k} = XLim; %#ok<AGROW>
        YLimTile{k} = YLim; %#ok<AGROW>
        sizePixels{k} = [size(subx(subsubx), 2), size(suby(subsuby), 2)]; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')

%% Cr�ation du fichier XML du layer

typeLayer = cl_image.strDataType{this.DataType};
% For�age du type d'image pour les images RGB car ce type est reconnu dans GLOBE.
typeImage = cl_image.strImageType{this.ImageType};
if strcmpi(typeImage, 'RGB')
    typeLayer = typeImage;
end

if flagExportTexture
    writeXMLGeotif(repExport, nomLayer, typeLayer, nomFic3DTile, true, minVal, maxVal, XLimTile, YLimTile, sizePixels);
else
    writeXMLGeotif(repExport, nomLayer, typeLayer, nomFic3DTile, false, minVal, maxVal, XLimTile, YLimTile, sizePixels);
end

flag = 1;


function writeXMLGeotif(repExport, layerSetName, typeLayerSetName, nomLayer, isMapped, minVal, maxVal, XLim, YLim, sizePixels)

%% Chemin en relatif pris en compte dans GLOBE

nomFicXml = [layerSetName '.xml'];
nomFicXml = fullfile(repExport, nomFicXml);
DirLayer = layerSetName;
[nomDirXml, racineNomFic] = fileparts(nomFicXml); %#ok<ASGLU>
fid = fopen(nomFicXml, 'w+');
if fid == -1
    str = sprintf(Lang('export_3DViewer_InGeotifTiles : Impossible to cr�er Fichier XML %s', ...
        'export_3DViewer_InGeotifTiles : Impossible to create XML config file %s'), nomFicXml);
    my_warndlg(str, 1);
    return
end

switch typeLayerSetName
    case 'Bathymetry'
        UnitLayer = 'm';
        globalMin = -20000;
        globalMax = +20000;
    case 'Reflectivity'
        UnitLayer = 'dB';
        globalMin = -500;
        globalMax = +500;
    case 'Temperature'
        UnitLayer = 'deg C';
        globalMin = -20;
        globalMax = +100;
    case 'Salinity'
        UnitLayer = 'psu';
        globalMin = 0;
        globalMax = +100;
    case 'SunColor' % Pas test� : 255 ou 1 ???
        UnitLayer = '';
        globalMin = 0;
        globalMax = 255;
    case 'Segmentation'
        UnitLayer = ' ';
        globalMin = 0;
        globalMax = +255;
    case 'AveragedPtsNb'
        UnitLayer = ' ';
        globalMin = 0;
        globalMax = 255;
    case 'Unknown'
        UnitLayer = '?';
        globalMin = 0;
        globalMax = +255;
    case 'Mask'
        UnitLayer = '?';
        globalMin = 0;
        if maxVal > 255
            globalMax = maxVal;
        else
            globalMax = +255;
        end
    case 'RGB'
        UnitLayer = '?';
        globalMin = 0;
        globalMax = +255;
    case 'RxTime'
        UnitLayer = 'time';
        % Arbitraire : choix de l'intervalle de travail de ce type de LAyer.
        globalMin = 723181;   % retour de datenum('01-01-1980 00:00:00')
        globalMax = 741444;   % retour de datenum('01-01-2030 00:00:00')
    otherwise
        UnitLayer = '?';
        globalMin = -Inf;
        globalMax = Inf;
        % TODO : d�crire les autres types de layers et faire un otherwise
end

%{
% % Comment� par JMA le 10/10/2020 pour robot acquisition donn�es EM mission MAYOBS15
% Pr�caution pour certaines donn�es mal identifi�es (pour l'instant comme Magnetisme, Gravi).
if minVal < globalMin ||  maxVal > globalMax
    strFR = 'Le type de donn�e n''ayant pas �t� reconnu, il va falloir que vous d�finissiez par vous-m�me les bornes min et max.';
    strUS = 'The data type was not recognized, you will have to define the min and max values by yourself.';
    my_warndlg(Lang(strFR, strUS), 1);
    if minVal < globalMin
        str1 = 'TODO';
        str2 = 'Geotif 32 bits : min value determination';
        str3 = 'Valeur minimale';
        str4 = 'Min val';
        [flag, globalMin] = inputOneParametre(Lang(str1,str2), Lang(str3,str4), 'Value', minVal, 'Unit', 'pixels');
        if ~flag
            return
        end
    end
    if maxVal > globalMax
        str1 = 'TODO';
        str2 = 'Geotif 32 bits : max value determination';
        str3 = 'Valeur maximale';
        str4 = 'Max value';
        [flag, globalMax] = inputOneParametre(Lang(str1,str2), Lang(str3,str4), 'Value', maxVal, 'Unit', 'pixels');
        if ~flag
            return
        end
    end
end
%}

%% Ecriture du fichier XML

fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid, '<LayerSet Name="%s" ShowOnlyOneLayer="false" ShowAtStartup="false" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="LayerSet.xsd">\n', racineNomFic);
fprintf(fid, '\t<LayerDescription>\n');
fprintf(fid, '\t\t<Type>%s</Type>\n', typeLayerSetName);
fprintf(fid, '\t\t<Unit>%s</Unit>\n', UnitLayer);
fprintf(fid, '\t\t<GlobalMin>%8.2f</GlobalMin>\n', globalMin);
fprintf(fid, '\t\t<GlobalMax>%8.2f</GlobalMax>\n', globalMax);
fprintf(fid, '\t\t<ValMin>%14.6f</ValMin>\n', minVal);
fprintf(fid, '\t\t<ValMax>%14.6f</ValMax>\n', maxVal);
fprintf(fid, '\t</LayerDescription>\n');

for k=1:length(nomLayer)
    fprintf(fid, '\t<item>\n');
    
    fprintf(fid, '\t\t<Name>%s</Name>\n', nomLayer{k});
    if isMapped
        fprintf(fid, '\t\t<TerrainMapped>true</TerrainMapped>\n');
    else
        fprintf(fid, '\t\t<TerrainMapped>false</TerrainMapped>\n');
    end
    fprintf(fid, '\t\t<DistanceAboveSurface>0</DistanceAboveSurface>\n');
    % fprintf(fid, '\t\t<BoundingBox>\n');
    % fprintf(fid, '\t\t</BoundingBox>\n');
    West = XLim{k}(1);
    East = XLim{k}(2);
    South = YLim{k}(1);
    North = YLim{k}(2);
    resolDegLat = abs(North-South)/sizePixels{k}(2);
    resolDegLon = abs(East-West)/sizePixels{k}(1);
    fprintf(fid, '\t\t<North><Value>%19.14f</Value></North>\n', North);
    fprintf(fid, '\t\t<South><Value>%19.14f</Value></South>\n', South);
    fprintf(fid, '\t\t<West><Value>%19.14f</Value></West>\n', East);
    fprintf(fid, '\t\t<East><Value>%19.14f</Value></East>\n', West);
    fprintf(fid, '\t\t<Resolution>\n');
    fprintf(fid, '\t\t\t<Latitude>%10.7f</Latitude>\n', resolDegLat);
    fprintf(fid, '\t\t\t<Longitude>%10.7f</Longitude>\n', resolDegLon);
    fprintf(fid, '\t\t</Resolution>\n');
    fprintf(fid, '\t\t<ImageAccessor>\n');
    fprintf(fid, '\t\t\t<LevelZeroTileSizeDegrees>10.000</LevelZeroTileSizeDegrees>\n');
    fprintf(fid, '\t\t\t<ImageFileExtension>tif</ImageFileExtension>\n');
    fprintf(fid, '\t\t\t<PermanentDirectory>%s</PermanentDirectory>\n', DirLayer);
    % fprintf(fid, '\t\t\t<ImageMin>%d</ImageMin>\n', minVal);
    % fprintf(fid, '\t\t\t<ImageMax>%d</ImageMax>\n', maxVal);
    fprintf(fid, '\t\t</ImageAccessor>\n');
    % fprintf(fid, '\t\t<TransparentMinValue>0</TransparentMinValue>\n');
    % Attention : la valeur de transparence Max doit etre diff�rentes
    % de 0 pour �tre efficace.
    % fprintf(fid, '\t\t<TransparentMaxValue>0</TransparentMaxValue>\n');
    fprintf(fid, '\t</item>\n');
end
fprintf(fid, '</LayerSet>\n');
fclose(fid);
