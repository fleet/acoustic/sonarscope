% Exportation de l'image au format ASCII
%
% Syntax
%   export_AsciiMultiLayer(a, flagX, flagY, ...)
%
% Input Arguments
%   a      : Une instance de cl_image
%   nomFic : Non du fichier de sortie (Une numerotation est rajoutee si decoupage par blocks)
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   nomFic = tempname
%   export_CarisAscii(a, 'nomFic', nomFic)
%
% See also cl_image cl_image/set cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_AsciiMultiLayer(this, these, flagX, flagY, nomFic, typeExport, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []); %#ok<ASGLU>

flag = 0;

fid = fopen(nomFic, 'w+');
if fid == -1
    str1 = sprintf('Impossible de cr�er le fichier "%s".', nomFic);
    str2 = sprintf('It is impossible to create file "%s".', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

nbLayers = length(these);
N = length(suby);
hw = create_waitbar('Multi-Layers ASCII export', 'N', N);
y = this.y(suby);
x = this.x(subx);

if flagX
    fprintf(fid, '%s (%s); ', this.XLabel, this.XUnit);
end
if flagY
    fprintf(fid, '%s (%s); ', this.YLabel, this.YUnit);
end
for k=1:nbLayers
    fprintf(fid, '%s (%s); ', cl_image.strDataType{these(k).DataType}, these(k).Unit);
end
fprintf(fid, '\n');

for k=1:nbLayers
    ImageName = extract_ImageName(these(k));
    fprintf(fid, '%s; ', ImageName);
end
fprintf(fid, '\n');

%% Extract the mask if defined

subNonMasque = get_Unmasked(LayerMask, valMask, x, y);

%% Ecriture des valeurs

nbPixelsExported = 0;
Nx = length(x);
for k1=1:N
    my_waitbar(k1, N, hw)
    
    subNaN = false(1, Nx);
    sub2   = true(1, Nx);
    for k2=1:nbLayers
        val{k2} = get_val_xy(these(k2), x, y(k1)); %#ok<AGROW>
        if isempty(subNonMasque)
            subNaN = subNaN | isnan(val{k2});
        else
            subNaN = subNaN | isnan(val{k2}) | subNonMasque(k1,:);
        end
        sub2 = sub2 & isnan(val{k2});
    end
    if typeExport == 1
        subx = find(~subNaN);
    else
        if all(sub2)
            continue
        end
        subx =  find(~sub2);
    end
    for k3=1:length(subx)
        if flagX
            fprintf(fid, '%s;', num2strPrecis(x(subx(k3))));
        end
        if flagY
            fprintf(fid, '%s;', num2strPrecis(y(k1)));
        end
        for k2=1:nbLayers
            fprintf(fid, '%s;', num2strPrecis(val{k2}(subx(k3))));
        end
        nbPixelsExported = nbPixelsExported + 1;
        fprintf(fid, '\n');
    end
end

if nbPixelsExported == 0
    str1 = 'Aucun pixel n''a �t� export�, il est tr�s probable qu''il n''y ait pas d''intersection commune entre toutes les images.';
    str2 = 'No pixel was exported. It seems there is no common intersection between all the selected images.';
    my_warndlg(Lang(str1,str2), 1);
end

my_close(hw, 'MsgEnd')
fclose(fid);
flag = 1;
