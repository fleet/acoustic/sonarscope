function flag = export_AsciiMultiLayerWithoutColocation(this, these, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask', []); %#ok<ASGLU>

flag = 0;

fid = fopen(nomFic, 'w+');
if fid == -1
    str1 = sprintf('Impossible de cr�er le fichier "%s".', nomFic);
    str2 = sprintf('It is impossible to create file "%s".', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

nbLayers = length(these);
y = this.y(suby);
x = this.x(subx);

%% Extract the mask if defined

subNonMasque = get_Unmasked(LayerMask, valMask, x, y);

%% Ecriture du header

for k=1:nbLayers
    fprintf(fid, '%s (%s); ', cl_image.strDataType{these(k).DataType}, these(k).Unit);
end
fprintf(fid, '\n');

for k=1:nbLayers
    ImageName = extract_ImageName(these(k));
    fprintf(fid, '%s; ', ImageName);
end
fprintf(fid, '\n');

%% R�cup�ration des valeurs

hw = create_waitbar('Multi-Layers ASCII export', 'N', nbLayers);
for k2=1:nbLayers
    my_waitbar(k2, nbLayers, hw)
    X = get_val_xy(these(k2), x, y);
    X(subNonMasque) = NaN;
    X(isnan(X) | isinf(X)) = [];
    tabVal{k2} = X; %#ok<AGROW>
    nbVal(k2) = length(X); %#ok<AGROW>
end

%% Ecriture des valeurs

for k1=1:max(nbVal)
    for k2=1:nbLayers
        if k1 <= nbVal(k2)
            fprintf(fid, '%s;', num2strPrecis(tabVal{k2}(k1)));
        else
            fprintf(fid, ';');
        end
    end
    fprintf(fid, '\n');
end

%% The End

my_close(hw, 'MsgEnd')
fclose(fid);
flag = 1;
