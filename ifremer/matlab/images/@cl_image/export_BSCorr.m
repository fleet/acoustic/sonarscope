% TODO : la fonction en deux s�parer les 2 BSCorr Mod�les / nodes
% See also write_BSCorr_Spline Authors

function export_BSCorr(this, nomFicIn, nomFicOut, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', length(this.CourbesStatistiques)); %#ok<ASGLU>

C0 = cl_sounder([]);

if isempty(sub) || isempty(nomFicIn) || isempty(nomFicOut)
    return
end

SounderName = get(this.Sonar.Desciption, 'Sonar.Name');

this = correctif_structureCourbesStatistiques(this);
CourbeConditionnelle = this.CourbesStatistiques(sub);
bilan = CourbeConditionnelle.bilan{1};



% D�but ajout JMA le 08/11/2015 pour EM710, fichier avec multi configurations
if strcmp(bilan(1).TagSup(1,1), 'Sonar.Mode_1')
    Mode_1 = bilan(1).TagSup{1,2};
else
    messageForDebugInspection
end

if strcmp(bilan(1).TagSup(2,1), 'Sonar.Mode_2')
    Mode_2 = bilan(1).TagSup{2,2};
else
    messageForDebugInspection
end
this.Sonar.Desciption = set(this.Sonar.Desciption, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', Mode_2);
% Fin ajout JMA le 08/11/2015 pour EM710, fichier avec multi configurations




bilan(1).TagSup(2,1)
bilan(1).TagSup(2,2)

% TypeMode_1   = get(this.Sonar.Desciption, 'Sonar.TypeMode_1');  % 'Swath'
swathMode   = get(this.Sonar.Desciption, 'Sonar.Mode_1'); % '1=Single swath' | {'2=Dual swath'}
SonarMode_2 = get(this.Sonar.Desciption, 'Sonar.Mode_2'); % '1=Very shallow' | '2=Shallow' | '3=Medium' | '4=Deep CW' | {'5=Deep FM'} | '6=Very Deep CW' | '7=Very Deep FM' | '8=Extra Deep1 CW' | '9=Extra Deep1 FM' | '10=Extra Deep2 CW' | '11=Extra Deep2 FM'

Model = get(this.Sonar.Desciption, 'Sonar.Name');
[flag, pingMode] = inverse_getSounderParamsForcl_sounderXML(Model, swathMode, SonarMode_2);
if ~flag
end

%% Lecture du BSCorr associ� � l'image

% TODO : trouver le fichier qui va avec l'image !!!!!!!!!!!!!!!!!!!!!
[flag, BSCorr] = read_BSCorr(nomFicIn);
if ~flag
    return
end
% BSCorrSIS = BSCorr; % TODO : en attendant de faire la lecture

%% Mise � jour du BSCorr � partir du mod�le

%{
this.Sonar.Desciption
Sonar.Ident               <-> '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson7150_12kHz' | '13=Reson7150_24kHz' | '14=Reson7111' | '15=Reson7125_200kHz' | '16=GeoSwath' | '17=ME70' | {'18=EM710'} | '19=EM3002D' | '20=EM3002S' | '21=EM2000' | '22=EM302' | '23=EM122' | '24=EM2040' | '25=Subbottom' | '26=Klein3000' | '27=EK60' | '28=HAC_Generic' | '29=ADCP' | '30=EM2040D'
Sonar.Name                <-- EM710

bilan(1).Mode
get(bilan(1).model, 'nom')
bilan(1).model
%}

ConfigurationTrouvee = false;
for k=1:length(BSCorr)
    if isempty(BSCorr(k).swathMode)
        continue
    end
    
    if (BSCorr(k).pingMode == pingMode) && ((BSCorr(k).swathMode + 1) == swathMode) % Ca a l'air de fonctionner pour l'EM302 !!!!
        %     if (BSCorr(k).pingMode == pingMode) && ((BSCorr(k).swathMode) == swathMode) % Modif JMA le 08/11/2015 pour l'EM710: A sannuler
        
        [Mode_1, ListeMode_2] = BsCorr_ModesIn(C0, pingMode, swathMode); %#ok<ASGLU> 
        %         [pingModepppp, swathModepppp, Waveformpppp] = BsCorr_ModesOut(C0, Mode_1, ListeMode_2(1))
        
        switch Mode_1
            case 1 % Single swath
                nbSectors = length(bilan);
                subTxDiag1 = 1:nbSectors;
                subTxDiag2 = [];
            case 2 % Double swath
                nbSectors = length(bilan) / 2;
                subTxDiag1 = 1:nbSectors;
                subTxDiag2 = (1:nbSectors) + nbSectors;
            otherwise
%                 Mode_1
                my_breakpoint;
        end
        
        for k2=1:nbSectors
            if isempty(bilan(subTxDiag1(k2)).model)
                continue
            end
            valParams = get_valParams(bilan(subTxDiag1(k2)).model);
            paramsSup = get(bilan(subTxDiag1(k2)).model, 'paramsSup');
            
            switch SounderName
                case {'EM302'; 'EM122'}
                    BSCorr(k).Sector.SL(k2)     = BSCorr(k).Sector.SL(k2)  - valParams(1);
                    BSCorr(k).Sector.Node(k2,1) = valParams(2);
                    BSCorr(k).Sector.Node(k2,2) = valParams(3);
                case {'EM710'; 'EM2040'}
                    xNodes = paramsSup{1};
                    yNodes = valParams;
                    BSCorr(k).Sector(k2).Node = [xNodes(:) yNodes(:)];
                otherwise
                    str1 = sprintf('Le sondeur "%s" n''est pas encore pr�vu danss la fonction export_BSCorr', SounderName);
                    str2 = sprintf('Function export_BSCorr is not ready for "%s" sounder yet.', SounderName);
                    my_warndlg(Lang(str1,str2), 1);
                    return
            end
            ConfigurationTrouvee = true;
        end
        
        if ~isempty(subTxDiag2)
            for k2=1:nbSectors
                valParams = get_valParams(bilan(subTxDiag2(k2)).model);
                paramsSup = get(bilan(subTxDiag2(k2)).model, 'paramsSup');
                
                switch SounderName
                    case {'EM302'; 'EM122'}
                        BSCorr(k+1).Sector.SL(k2)     = BSCorr(k+1).Sector.SL(k2)  - valParams(1);
                        BSCorr(k+1).Sector.Node(k2,1) = valParams(2);
                        BSCorr(k+1).Sector.Node(k2,2) = valParams(3);
                    case {'EM710'; 'EM2040'}
                        xNodes = paramsSup{1};
                        yNodes = valParams;
                        BSCorr(k+1).Sector(k2).Node(:,1) = xNodes;
                        BSCorr(k+1).Sector(k2).Node(:,2) = yNodes;
                    otherwise
                        str1 = sprintf('Le sondeur "%s" n''est pas encore pr�vu danss la fonction export_BSCorr', SounderName);
                        str2 = sprintf('Function export_BSCorr is not ready for "%s" sounder yet.', SounderName);
                        my_warndlg(Lang(str1,str2), 1);
                        return
                end
                ConfigurationTrouvee = true;
            end
        end
    end
end

%       BsCorr(k1).pingMode = pingMode;
%     BsCorr(k1).swathMode = swathMode;

%{

pppp(1)

ans =

pingMode: 1
swathMode: 0
nbSectors: 3
Sector: [1x3 struct]

pppp(1).Sector

ans =

1x3 struct array with fields:

Label
SL
nbNodes
Node
%}

%% Ecriture du BSCorr

if ConfigurationTrouvee
    switch SounderName
        case {'EM302'; 'EM122'}
            flag = write_BSCorr_Poly(nomFicOut, BSCorr);
        case 'EM710'
            flag = write_BSCorr_Spline(BSCorr, nomFicOut); %, 'BSCorrSIS', BSCorrSIS);
        case 'EM2040' % TODO write_BSCorr_SplineEM2040
            flag = write_BSCorr_SplineEM2040(BSCorr, nomFicOut); %, 'BSCorrSIS', BSCorrSIS);
    end
    if ~flag
        messageErreurFichier(nomFicOut, 'WriteFailure');
    end
else
    str1 = 'La configuration n''a pas �t� trouv�e dans le fichier BSCorr, aucune modification n''a �t� r�alis�e.';
    str2 = 'This configuration was not found in the BSCorr. No modification of the BSCorr was done.';
    my_warndlg(Lang(str1,str2), 1);
end
