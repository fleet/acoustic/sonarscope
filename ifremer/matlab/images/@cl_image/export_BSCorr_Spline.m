function export_BSCorr_Spline(this, nomFicIn, nomFicOut, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', length(this.CourbesStatistiques)); %#ok<ASGLU>

C0 = cl_sounder([]);

if isempty(sub) || isempty(nomFicIn) || isempty(nomFicOut)
    return
end

SounderName = get(this.Sonar.Desciption, 'Sonar.Name');
switch SounderName
    case {'EM302'; 'EM122'}
        str1 = 'Veuillez utiliser la fonction "export_BSCorr_Poly" pour les sondeurs EM122 et EM302.';
        str2 = 'Please use "export_BSCorr_Poly" function to for EM122 and EM3020 sounders.';
        my_warndlg(Lang(str1,str2), 1);
        return
    case {'EM710'; 'EM712'; 'EM2040'; 'EM304'}
        % OK
    otherwise
        str1 = 'Seuls les sondeurs Kongsberg EM122, EM302, EM710, EM712 sont �ligibles pour cette fonctionnalit�.';
        str2 = 'Only EM122, EM302, EM710 and EM712 Kongsberg sounders can produce a BSCorr.txt.';
        my_warndlg(Lang(str1,str2), 1);
        return
end

this = correctif_structureCourbesStatistiques(this);
CourbeConditionnelle = this.CourbesStatistiques(sub);
bilan = CourbeConditionnelle.bilan{1};
this = set_SonarDescriptionFromCurveParameters(this, bilan);

% TypeMode_1   = get(this.Sonar.Desciption, 'Sonar.TypeMode_1');  % 'Swath'
swathMode   = get(this.Sonar.Desciption, 'Sonar.Mode_1'); % '1=Single swath' | {'2=Dual swath'}
SonarMode_2 = get(this.Sonar.Desciption, 'Sonar.Mode_2'); % '1=Very shallow' | '2=Shallow' | '3=Medium' | '4=Deep CW' | {'5=Deep FM'} | '6=Very Deep CW' | '7=Very Deep FM' | '8=Extra Deep1 CW' | '9=Extra Deep1 FM' | '10=Extra Deep2 CW' | '11=Extra Deep2 FM'

Model = get(this.Sonar.Desciption, 'Sonar.Name');
[flag, pingMode] = inverse_getSounderParamsForcl_sounderXML(Model, swathMode, SonarMode_2);
if ~flag
    return
end

%% Lecture du BSCorr associ� � l'image

% TODO : trouver le fichier qui va avec l'image !!!!!!!!!!!!!!!!!!!!!
[flag, BSCorr] = read_BSCorr(nomFicIn);
if ~flag
    return
end
% BSCorrSIS = BSCorr; % TODO : en attendant de faire la lecture

%% Mise � jour du BSCorr � partir du mod�le

%{
this.Sonar.Desciption
Sonar.Ident               <-> '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson7150_12kHz' | '13=Reson7150_24kHz' | '14=Reson7111' | '15=Reson7125_200kHz' | '16=GeoSwath' | '17=ME70' | {'18=EM710'} | '19=EM3002D' | '20=EM3002S' | '21=EM2000' | '22=EM302' | '23=EM122' | '24=EM2040' | '25=Subbottom' | '26=Klein3000' | '27=EK60' | '28=HAC_Generic' | '29=ADCP' | '30=EM2040D'
Sonar.Name                <-- EM710

bilan(1).Mode
get(bilan(1).model, 'nom')
bilan(1).model
%}

ConfigurationTrouvee = false;
for k=1:length(BSCorr)
    if isempty(BSCorr(k).swathMode)
        continue
    end
    
    %     if (BSCorr(k).pingMode == pingMode) && ((BSCorr(k).swathMode + 1) == swathMode) % Ca a l'air de fonctionner pour l'EM302 !!!!
    if (BSCorr(k).pingMode == pingMode) && ((BSCorr(k).swathMode) == swathMode) % Modif JMA le 08/11/2015 pour l'EM710: A sannuler
        
        [Mode_1, ListeMode_2] = BsCorr_ModesIn(C0, pingMode, swathMode); %#ok<ASGLU> 
        %         [pingModepppp, swathModepppp, Waveformpppp] = BsCorr_ModesOut(C0, Mode_1, ListeMode_2(1))
        
        switch Mode_1
            case 1 % Single swath
                nbSectors = length(bilan);
                subTxDiag1 = 1:nbSectors;
                subTxDiag2 = [];
            case 2 % Double swath
                nbSectors = ceil(length(bilan) / 2);
                subTxDiag1 = 1:nbSectors;
                subTxDiag2 = (1:nbSectors) + nbSectors;
            otherwise
                %                 Mode_1
        end
        
        for k2=1:nbSectors
            model = bilan(subTxDiag1(k2)).model;
            if isempty(model)
                continue
            end
            
            valParams = model.getParamsValue;
            paramsSup = model.ParamsSup;
            
            xNodes = paramsSup{1};
            yNodes = valParams;
            subNaN = isnan(yNodes);
            xNodes(subNaN) = [];
            yNodes(subNaN) = [];
            BSCorr(k).Sector(k2).Node = [xNodes(:) yNodes(:)];
            BSCorr(k).Sector(k2).nbNodes = length(xNodes);
            
            ConfigurationTrouvee = true;
        end
        
        if ~isempty(subTxDiag2)
            for k2=1:nbSectors
                if subTxDiag2(k2) > length(bilan) % Cas d'un secteur qui manque
                    break
                end
                model = bilan(subTxDiag2(k2)).model;
                if isempty(model)
                    continue
                end
                valParams = model.getParamsValue;
                paramsSup = model.ParamsSup;
                
                xNodes = paramsSup{1};
                yNodes = valParams;
                subNaN = isnan(yNodes);
                xNodes(subNaN) = [];
                yNodes(subNaN) = [];
                BSCorr(k+1).Sector(k2).Node = [xNodes(:) yNodes(:)];
                BSCorr(k+1).Sector(k2).nbNodes = length(xNodes);
                
                ConfigurationTrouvee = true;
            end
        end
    end
end

%       BsCorr(k1).pingMode = pingMode;
%     BsCorr(k1).swathMode = swathMode;

%{

pppp(1)

ans =

pingMode: 1
swathMode: 0
nbSectors: 3
Sector: [1x3 struct]

pppp(1).Sector

ans =

1x3 struct array with fields:

Label
SL
nbNodes
Node
%}

%% Ecriture du BSCorr

if ConfigurationTrouvee
    flag = write_BSCorr_Spline(BSCorr, nomFicOut, SounderName); %, 'BSCorrSIS', BSCorrSIS);
    if ~flag
        messageErreurFichier(nomFicOut, 'WriteFailure');
    end
else
    str1 = 'La configuration n''a pas �t� trouv�e dans le fichier BSCorr, aucune modification n''a �t� r�alis�e.';
    str2 = 'This configuration was not found in the BSCorr. No modification of the BSCorr was done.';
    my_warndlg(Lang(str1,str2), 1);
end
