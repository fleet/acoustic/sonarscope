function [flag, repExport] = export_Caraibes(this, indImage, repExport, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'FileName', []); %#ok<ASGLU>

GeometryType = this(indImage).GeometryType;
DataType     = this(indImage).DataType;

if GeometryType == cl_image.indGeometryType('PingBeam') % PingBeam -> .mbg
    if isempty(nomFic)
        [flag, nomFic, repExport] = getFileNameLinked2ImageName(this(indImage), '.mbg', repExport, 'FullName');
        if ~flag
            return
        end
    end
    export_CaraibesMbg(this, indImage, nomFic, 'suby', suby);
    
else % Autre G�om�trie ET layers synchronis�s
    if DataType == cl_image.indDataType('Bathymetry') && GeometryType == cl_image.indGeometryType('GeoYX') % MNT
        if isempty(nomFic)
            [flag, nomFic, repExport] = getFileNameLinked2ImageName(this(indImage), '.mnt', repExport, 'FullName');
            if ~flag
                return
            end
        end
        Type        = 'DTM';
        Mosaic_type = 'GEO';
        export_CaraibesGrid_3_6(this(indImage), nomFic, Type, Mosaic_type, 'subx', subx, 'suby', suby);
        
    elseif DataType == cl_image.indDataType('Reflectivity') && GeometryType == cl_image.indGeometryType('GeoYX') % MOS
        if isempty(nomFic)
            [flag, nomFic, repExport] = getFileNameLinked2ImageName(this(indImage), '.mos', repExport, 'FullName');
            if ~flag
                return
            end
        end
        Type        = 'MOS';
        Mosaic_type = 'GEO';
        export_CaraibesGrid_3_6(this(indImage), nomFic, Type, Mosaic_type, 'subx', subx, 'suby', suby);
        
    elseif GeometryType == cl_image.indGeometryType('PingAcrossDist') || ...
            GeometryType == cl_image.indGeometryType('PingSamples') % IMO - profil sonar rectiligne : PingSamples = grRawImage  & PingAcrossDist = grWrkImage
        if isempty(nomFic)
            [flag, nomFic, repExport] = getFileNameLinked2ImageName(this(indImage), '.imo', repExport, 'FullName');
            if ~flag
                return
            end
        end
        Type        = 'MOS';
        Mosaic_type = 'STR';
        export_CaraibesGrid_3_6(this(indImage), nomFic, Type, Mosaic_type, 'subx', subx, 'suby', suby);
        
    else
        str1 = sprintf('Le fichier ne peut �tre export� vers Caraibes car le "GeometryType" ET/OU "DataType" ne sont pas compatibles (devraient �tre Bathymetry ou Reflectivity, ou ...)\n\n1 - V�rifier le type de donn�es\n2 - Pensez � projeter (LatLong -> GeoYX)' );
        str2 = sprintf('This "GeometryType" AND/OR this "DataType" are not available in Caraibes(R)\n\n1- Check the DataType (should be Bathymetry or Reflectivitry, or ...)\n2 - Maybe you should project your data (LatLong -> GeoYX)');
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
end

flag = 1;
