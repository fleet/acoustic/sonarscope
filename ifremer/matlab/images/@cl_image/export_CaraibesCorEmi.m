% Exportation de courbes de BS au format Caraibes
%
% Syntax
%   export_CaraibesCorEmi(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% -------------------------------------------------------------------------

function export_CaraibesCorEmi(this, nomFicSave, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if isempty(sub)
    return
end

if ~isempty(nomFicSave)
    %     [nomDir, nomFic, ext] = fileparts(nomFicSave);
    %     nomFicSave = fullfile(nomDir, [nomFic '.rev']);
    
    CourbeConditionnelle = this.CourbesStatistiques(sub);
    bilan = CourbeConditionnelle.bilan{1};
    
    for i=1:length(bilan)
        if ~isfield(bilan(i), 'model')
            return
        end
        Temp = bilan(i).model;
        if isempty(Temp)
            continue
        end
        models(i) = Temp;%#ok
    end
    
    switch bilan(1).DataTypeConditions{1}
        case 'TxAngle'
            X_DATA = 'EMISSION';
        case 'IncidenceAngle'
            X_DATA = 'INCIDENCE';
        otherwise
            return
    end
    switch bilan(1).DataTypeConditions{2}
        case 'TxBeamIndex'
            origineFaisceaux = 'SECTEUR_EMISSION';
        case 'RxBeamIndex'
            origineFaisceaux = 'NUFAIS';
        otherwise
            return
    end
    
    switch bilan(1).DataTypeValue
        case 'Reflectivity'
            Y_DATA = 'REFLECTIVITY';
        otherwise
            return
    end
    
    courbes.Sondeur.Nom             = get(this.Sonar.Desciption, 'Sonar.Name');
    courbes.ordonnee                = Y_DATA;
    courbes.abscisse                = X_DATA;
    courbes.minx                    = -88;  %min(bilan.x);
    courbes.maxx                    = 88;   %max(bilan.x);
    courbes.pasx                    = 0.5;  %mean(diff(bilan.x));
    
    courbes(1).nb_courbes = length(models);
    
    strMode = get(this.Sonar.Desciption, 'Sonar.strMode_1');
    Mode    = get(this.Sonar.Desciption, 'Sonar.Mode_1');
    courbes(1).Sondeur.Mode = strMode{Mode};
    
    courbes(1).origineFaisceaux = origineFaisceaux;
    courbes(1).nb_faisceaux = length(models);
    
    courbes(1).Attenuation = 0;
    
    for i=1:courbes(1).nb_courbes
        courbes(i).nom = sprintf('Courbe%d', i); %#ok
        courbes(i).Modelisation = get(models(i), 'nom'); %#ok
        
        commande = sprintf('courbes(i).%s = ''%s'';', ...
            courbes(i).Modelisation, courbes(i).Modelisation);
        eval(commande);
        
        courbes(i).ModelisationCoefs = get_valParams(models(i)); %#ok
        courbes(i).trace = 1; %#ok
        
        courbes(i).angle = bilan(1).x; %#ok
    end
end
EcrFicCorEmiCaraibes(courbes, nomFicSave)
