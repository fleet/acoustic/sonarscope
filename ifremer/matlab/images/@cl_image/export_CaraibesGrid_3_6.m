% Exportation d'un MNT au format Caraibes(R)
%
% Syntax
%   export_CaraibesGrid_3_6(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Examples
%   a =
%   nomFic = [tempname '.mnt']
%   Type   = 'DTM';
%   export_CaraibesGrid_3_6(a, nomFic)
%   SonarScope(nomFic)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_CaraibesGrid_3_6(this, nomFic, Type, Mosaic_type, varargin)

[subx, suby, varargin] = getSubxSuby(this(1), varargin); %#ok<ASGLU>

switch Type
    case 'DTM'
        DataType = cl_image.indDataType('Bathymetry');
        flag = testSignature(this(1), 'DataType', DataType, 'GeometryType', [cl_image.indGeometryType('GeoYX') cl_image.indGeometryType('LatLong')]);
        if ~flag
            return
        end
        nomFicRef = getNomFicDatabase('RefCIB36_EM300-Lambert93.mnt');
        
    case 'MOS'
        if strcmp(Mosaic_type, 'GEO') % Cadre droit
            flag = testSignature(this(1), 'GeometryType', [cl_image.indGeometryType('GeoYX') cl_image.indGeometryType('LatLong')]);
            nomFicRef = getNomFicDatabase('RefCIB36.mos');
        elseif strcmp(Mosaic_type, 'STR') % Cadre oblique
            flag = testSignature(this(1), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'));
            %nomFicRef = getNomFicDatabase('BOBECO1_rect.imo');
            nomFicRef = getNomFicDatabase('Ex_Template_ReflecStr.imo', 'NoMessage');
            % nomFicRef = getNomFicDatabase('Ex_Template_ReflecStr_RawOnly.imo', 'NoMessage');
            
            % D�but ajout JMA le 19/04/2013 pour pb export de fichiers .ers
            % vers caraibes
            T = this(1).Sonar.Time.timeMat;
            T = T(suby);
            if T(end) < T(1)
                suby = flipud(suby(:));
            end
            % Fin ajout JMA le 19/04/2013
            
        end
        if ~flag
            return
        end
        
    otherwise %'IMO'
        %         DataType = this(1).DataType;
        flag = testSignature(this(1), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'));
        if ~flag
            return
        end
        nomFicRef = getNomFicDatabase('BOBECO1_rect.imo');
end

flag = testEcritureFichier(nomFic);
if ~flag
    return
end

a_ref  = cl_netcdf('fileName', nomFicRef);
sk_ref = get(a_ref, 'skeleton');

LayersName = {};
Layers = {};
Unites = {};
ValNaN = [];
try
    for k=1:length(this)
        [NomLayer, Unite] = NomLayerCaraibesGrid(this(k));
        if ~isempty(NomLayer)
            LayersName{end+1} = NomLayer;  %#ok<AGROW>
            Unites{end+1} = Unite;  %#ok<AGROW>
            
            X = this(k).Image(suby,subx);
            if ~isa(X, 'double')
                X = single(X);
            end
            Layers{end+1} = X;  %#ok<AGROW>
            ValNaN(end+1) = this(k).ValNaN; %#ok<AGROW>
            clear X
        end
    end
catch %#ok<CTCH>
    for k=1:length(this)
        [NomLayer, Unite] = NomLayerCaraibesGrid(this(k));
        if ~isempty(NomLayer)
            LayersName{end+1} = NomLayer; %#ok<AGROW>
            Unites{end+1} = Unite; %#ok<AGROW>
            
            X = this(k).Image(suby,subx);
            Layers{end+1} = cl_memmapfile('Value', X); %#ok<AGROW>
            Layers{end}.Writable = false;
            ValNaN(end+1) = this(k).ValNaN; %#ok<AGROW>
            clear X
        end
    end
end

Format =  'STR'; % Car on ne peut exporter que des fichiers "droits" depuis SSC
Sounder_type = [];

if strcmp(Mosaic_type, 'STR')
    %% Cadre droit
    Time  = this(1).Sonar.Time;
    Date  = Time.date;
    Heure = Time.heure;
    
    
    % Comment� par JMA le 19/04/2013 pour pb export de fichiers .ers
    % vers caraibes
    %{
% Reclassement des donn�es de chronodatage si l'image est extraire d'un
% fichier ERS (donn�es mal ordonnanc�es au chargement).
if (Heure(1) > Heure(end))
Heure = flipud(Heure);
end
if (Date(1) > Date(end))
Date = flipud(Date);
end
    %}
    
    Latitude  = this(1).Sonar.FishLatitude;
    Longitude = this(1).Sonar.FishLongitude;
    Heading   = this(1).Sonar.Heading;
    
    % 'Le quel des 2 ?'
    Velocity        = this(1).Sonar.Speed;
    SurfaceVelocity = this(1).Sonar.SurfaceSoundSpeed;
    
    SonarDescription = this(1).Sonar.Desciption;
    SonarIdent = get(SonarDescription, 'Sonar.Ident');
    
    this(1).Sonar.PortMode_2(this(1).Sonar.PortMode_2 == 0) = 1;
    PortMode      = modeSim2Car(SonarIdent, this(1).Sonar.PortMode_1, this(1).Sonar.PortMode_2);
    StarboardMode = modeSim2Car(SonarIdent, this(1).Sonar.StarMode_1, this(1).Sonar.StarMode_2);
    
    % 'Verifier �a'
    VerticalDepth = this(1).Sonar.Height;
    SimradResolution = (1500/2) ./ this(1).Sonar.SampleFrequency;
    
    if ~isempty(Date)
        Date = Date(suby);
    end
    if ~isempty(Heure)
        Heure = Heure(suby);
    end
    if ~isempty(Latitude)
        Latitude = Latitude(suby);
    end
    if ~isempty(Longitude)
        Longitude = Longitude(suby);
    end
    if ~isempty(Heading)
        Heading = Heading(suby);
    end
    if ~isempty(Velocity)
        Velocity = Velocity(suby);
    end
    if ~isempty(PortMode)
        PortMode = PortMode(suby);
    end
    if ~isempty(StarboardMode)
        StarboardMode = StarboardMode(suby);
    end
    if ~isempty(SurfaceVelocity)
        SurfaceVelocity = SurfaceVelocity(suby);
    end
    if ~isempty(VerticalDepth)
        VerticalDepth = VerticalDepth(suby);
    end
    if ~isempty(SimradResolution)
        SimradResolution = SimradResolution(suby);
    end
    
    Field.Field_name = get_value(a_ref, 'Field_name');
    
    
    %{
--- Caraibes ---
Interlacing
PortNormalIncidenceObliqueDist
PortTvgBsDifference

StarboardNormalIncidenceObliqueDist
StarboardTvgBsDifference
DistPortPing
DistStarboardPing
DistMiddlePing
Col
Row
Flag
PortAlongResolution
MiddleAlongResolution
StarboardAlongResolution
EmissionAlongAngle


--- SonarScope ---
PingNumber: []
Immersion: [1x976 double]
CableOut: []
PingCounter: []
    %}
    if strcmp(Type, 'DTM')
        create_caraibesGrid_3_6(nomFic, sk_ref, LayersName, Layers, Unites, ...
            this(1).x(subx), this(1).y(suby), Type, Mosaic_type, Format, Sounder_type, this(1).Carto, this(1).GeometryType, ...
            'Field', Field, 'Date', Date, 'Hour', Heure, 'Latitude', Latitude, 'Longitude', Longitude, 'Heading', Heading, ...
            'Velocity', Velocity, 'PortMode', PortMode, 'StarboardMode', StarboardMode, 'SurfaceVelocity', SurfaceVelocity, ...
            'VerticalDepth', VerticalDepth, 'SimradResolution', SimradResolution, 'ValeursDansUnite');
        
    elseif strcmp(Type, 'MOS')
        % Cas des images rectilignes de Sonar Lat�raux.
        %         LayersName = {'grRawImage'};
        %         LayersName = {'grWrkImage'};
        LayersName = {'grRawImage','grWrkImage'};
        
        Sonar = this.Sonar;
        Sonar.PrtPixNb      = sum(this.x < 0)  * ones( this.nbRows, 1, 'int16');
        Sonar.StbPixNb      = sum(this.x > 0)  * ones( this.nbRows, 1, 'int16');
        Sonar.PortAcrossDist= abs(min(this.x)) * ones( this.nbRows, 1, 'single');
        Sonar.StbAcrossDist = max(this.x)      * ones( this.nbRows, 1, 'single');
        
        create_caraibesGrid_MosStr(nomFic, sk_ref, LayersName, Layers, ...
            'suby', suby, 'Field', Field, 'Date', Date, 'Hour', Heure, 'Latitude', Latitude, 'Longitude', Longitude, ...
            'Heading', Heading, 'SurfaceVelocity', SurfaceVelocity, 'Str_pixel_size', get(this, 'XStep'), ...
            'SimradResolution', SimradResolution, 'Sonar', Sonar, 'ValeursDansUnite');
    end
else
    %% Cadre g�or�f�renc�
    %     this(1).strGeometryType{this(1).GeometryType}
    create_caraibesGrid_3_6(nomFic, sk_ref, LayersName, Layers, Unites, ...
        this(1).x(subx), this(1).y(suby), Type, Mosaic_type, Format, Sounder_type, this(1).Carto, ...
        this(1).GeometryType, 'ValeursDansUnite', 'ValNaN', ValNaN);
end

% -----------------------------------------------
% Exportation de la description de l'image en XML

% [nomDir, nomFicXml] = fileparts(nomFic);
% nomFicXml = fullfile(nomDir, [nomFicXml '.xml']);
% flag = export_info_xml(this(1), nomFicXml, 'subx', subx, 'suby', suby);
