% Exportation de donn�es PingBeam au format Caraibes ".mbg"
%
% Syntax
%   export_CaraibesMbg(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby : subsampling in Y
%
% Examples
%   nomFicIn  = getNomFicDatabase('REFCar_EM1000.mbg')
%   nomFicIn  = getNomFicDatabase('REFCar_EM300.mbg')
%   nomFicIn  = getNomFicDatabase('REFCar_EM12D.mbg')
%   nomFicOut = my_tempname('.mbg')
%   SonarScope(nomFicIn)
%   SonarScope(nomFicOut)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_CaraibesMbg(this, indImage, nomFic, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

I0 = cl_image_I0;

nomFicRef = getNomFicDatabase('REFCar_EM300.mbg');

flag = testEcritureFichier(nomFic);
if ~flag
    return
end

a_ref = cl_netcdf('fileName', nomFicRef);
sk_ref = get(a_ref, 'skeleton');
%   cdf_disp_list_att(sk_ref, 'Level', 1)
%   cdf_disp_list_dim(sk_ref, 'Level', 1)
%   cdf_disp_list_var(sk_ref, 'Level', 1)

[flag, indLayerDepth] = findIndLayerMBG(this, indImage, 'Bathymetry');
if ~flag
    return
end

[flag, indQuality] = findIndLayerMBG(this, indImage, 'MbesQualityFactor', 'NoMessage');
if ~flag
    %     return
end

[flag, indRangeAcross] = findIndLayerMBG(this, indImage, 'AcrossDist');
if ~flag
    return
end

[flag, indRangeAlong] = findIndLayerMBG(this, indImage, 'AlongDist');
if ~flag
    return
end

[flag, indFlag] = findIndLayerMBG(this, indImage, 'Mask', 'NoMessage');
if ~flag
    %     return
end

[flag, indTxAngle] = findIndLayerMBG(this, indImage, 'TxAngle');
if ~flag
    %     return
end

[flag, indAzimuthAngle] = findIndLayerMBG(this, indImage, 'BeamAzimuthAngle', 'NoMessage');
if ~flag
    %     return
end



[flag, indSoundingBias] = findIndLayerMBG(this, indImage, '????', 'NoMessage');
% if ~flag
%     return
% end


LayersName = {};
Layers = {};
Unites = {};
for k=1:length(this)
    [NomLayer, Unite] = NomLayerCaraibesMBG(this(k));
    switch k
        case indLayerDepth
            indLayer = 3;
        case indQuality
            indLayer = 5;
        case indRangeAcross
            indLayer = 2;
        case indRangeAlong
            indLayer = 1;
        case indFlag
            indLayer = 6;
        case indSoundingBias
            indLayer = 4;
        case indTxAngle
            indLayer = 7;
        case indAzimuthAngle
            indLayer = 8;
        otherwise
            continue
    end
    
    if isempty(suby)
        suby = 1:this(k).nbRows;
    end
    
    LayersName{indLayer} = NomLayer; %#ok<AGROW>
    Unites{indLayer} = Unite; %#ok<AGROW>
    
    % Provisoire pour RESON
    if (indLayer == 6) && any(get(this(k).Sonar.Desciption, 'Sonar.Ident') == [12 13 14 15])
        Y = masqueExtractionBits(this(k), [1 2 6], 'suby', suby);
        X = zeros(this(k).nbRows, this(k).nbColumns, 'single') + 2;
        
        for i=1:length(Y)
            sub = (Y(i).Image == 0);
            if (i == 1) && all(sub(:))
                continue % Pour �viter pi�ge d�couvert par C�cile Breton
            end
            X(sub) = -2;
        end
        
        X(isnan(this(indImage).Image)) = -2;
    else
        X = this(k).Image(suby,:);
    end
    
    Layers{indLayer} = cl_memmapfile('Value', X); %#ok<AGROW>
    Layers{indLayer}.Writable = false; %#ok<AGROW>
    clear X
    
    if this(k).DataType == cl_image.indDataType('Bathymetry')
        mbMinDepth = -this(k).StatValues.Max;
        mbMaxDepth = -this(k).StatValues.Min;
    end
end

Time  = this(indLayerDepth).Sonar.Time;
Date  = Time.date;
Heure = Time.heure;

% 'Le quel des 2 ?'
Velocity        = this(indLayerDepth).Sonar.Speed;
SurfaceVelocity = this(indLayerDepth).Sonar.SurfaceSoundSpeed;

% Traiter Sonar.Interlacing




SonarDescription = this(indLayerDepth).Sonar.Desciption;
% SonarIdent       = get(SonarDescription, 'Sonar.Ident');

% % ATLANTIDE : temporaire. Pb de d�codage.
subNaN = isnan(this(indLayerDepth).Sonar.PortMode_1(:));
if any(subNaN)
    my_warndlg('Mode ind�termin� : on impose la valeur 1 : DANGER !', 1);
    this(indLayerDepth).Sonar.PortMode_2(subNaN) = 1;
    this(indLayerDepth).Sonar.PortMode_1(subNaN) = 1;
    this(indLayerDepth).Sonar.StarMode_2(subNaN) = 1;
    this(indLayerDepth).Sonar.StarMode_1(subNaN) = 1;
end

% PortMode      = modeSim2Car(SonarIdent, this(indLayerDepth).Sonar.PortMode_1, this(indLayerDepth).Sonar.PortMode_2);
% StarboardMode = modeSim2Car(SonarIdent, this(indLayerDepth).Sonar.StarMode_1, this(indLayerDepth).Sonar.StarMode_2);
PortMode      = this(indLayerDepth).Sonar.PortMode_1;
StarboardMode = this(indLayerDepth).Sonar.StarMode_1;


% 'CARAIBES : Verifier �a'
VerticalDepth = this(indLayerDepth).Sonar.Height;
SimradResolution = (1500/2) ./ this(indLayerDepth).Sonar.SampleFrequency;

mbDate = Date(suby);
mbTime = Heure(suby);

if ~isempty(Velocity)
    Velocity = Velocity(suby);
end
if ~isempty(PortMode)
    PortMode = PortMode(suby);
end
if ~isempty(StarboardMode)
    StarboardMode = StarboardMode(suby);
end
if ~isempty(SurfaceVelocity)
    SurfaceVelocity = SurfaceVelocity(suby);
end
if ~isempty(VerticalDepth)
    VerticalDepth = VerticalDepth(suby);
end
if ~isempty(SimradResolution)
    SimradResolution = SimradResolution(suby);
end

mbShip       = get_ShipName(SonarDescription);
mbSurvey     = 'Unknown';
Sounder_type = get_CodeSounderCaraibes(SonarDescription);
switch Sounder_type
    case {1; 6} % EM12D & EM3000D
        mbAntennaNbr = 2;
    otherwise
        mbAntennaNbr = 1;
end

if mbAntennaNbr == 1
    mbSounderMode = PortMode;
else
    mbSounderMode = [PortMode; StarboardMode];
end
%{
mbSamplingRate : SonarDescription : Signal.Freq Proc.SampFreq
%}
mbInterlacing       = [];
mbCompensationLayerMode = [];
% mbReferenceDepth    = [];
% mbDynamicDraught    = [];
% mbTide              = [];
mbCQuality          = [];
mbCFlag             = [];
mbFrequency         = [];
mbReferenceDepth    = [];
mbDynamicDraught    = [];
mbTide              = [];
mbSoundVelocity     = []; %this(indLayerDepth).Sonar.SurfaceSoundSpeed(suby);
mbSamplingRate      = [];

mbCycle             = this(indLayerDepth).Sonar.PingCounter(suby);
if ~isempty(this(indLayerDepth).Sonar.SampleFrequency)
    mbSamplingRate      = this(indLayerDepth).Sonar.SampleFrequency(suby);
end

mbHeading           = this(indLayerDepth).Sonar.Heading(suby);
mbRoll              = this(indLayerDepth).Sonar.Roll(suby);
mbPitch             = this(indLayerDepth).Sonar.Pitch(suby);
mbTransmissionHeave = this(indLayerDepth).Sonar.Heave(suby);
mbVerticalDepth     = -this(indLayerDepth).Sonar.Height(suby);

identDetectionType = cl_image.indDataType('DetectionType');
[flag, indDetectionType] = listeLayersSynchronises(this, indImage, ...
    'TypeDonneUniquement', identDetectionType, 'IncludeCurrentImage');
if ~flag
    return
end

create_caraibesMBG(I0, nomFic, sk_ref, Layers, Sounder_type,  mbShip, mbSurvey, ...
    mbDate, mbTime, this(indLayerDepth).Sonar.FishLongitude(suby), this(indLayerDepth).Sonar.FishLatitude(suby), ...
    mbMinDepth, mbMaxDepth, this(indLayerDepth).Carto, ...
    'mbCycle', mbCycle, 'mbSamplingRate', mbSamplingRate, 'mbSounderMode', mbSounderMode, ...
    'mbReferenceDepth', mbReferenceDepth, 'mbDynamicDraught', mbDynamicDraught, ...
    'mbTide', mbTide, 'mbSoundVelocity', mbSoundVelocity, 'mbHeading', mbHeading, ...
    'mbRoll', mbRoll, 'mbPitch', mbPitch, 'mbTransmissionHeave', mbTransmissionHeave, ...
    'mbVerticalDepth', mbVerticalDepth, 'mbCQuality', mbCQuality, 'mbCFlag', mbCFlag, ...
    'mbInterlacing', mbInterlacing, ...
    'mbCompensationLayerMode', mbCompensationLayerMode, ...
    'mbFrequency', mbFrequency, ...
    'DetectionType', this(indDetectionType));

% export_info_xml(this(indLayerDepth), nomFic, 'suby', suby)

%{
sk.att(1).name  = 'mbVersion';
sk.att(2).name  = 'mbName';
sk.att(3).name  = 'mbClasse';
sk.att(4).name  = 'mbLevel';
sk.att(5).name  = 'mbNbrHistoryRec';
sk.att(6).name  = 'mbTimeReference';
sk.att(15).name  = 'mbMeridian180';
sk.att(16).name  = 'mbGeoDictionnary';
sk.att(17).name  = 'mbGeoRepresentation';
sk.att(18).name  = 'mbGeodesicSystem';
sk.att(19).name  = 'mbEllipsoidName';
sk.att(23).name  = 'mbProjType';
sk.att(24).name  = 'mbProjParameterValue';
sk.att(25).name  = 'mbProjParameterCode';
sk.att(29).name  = 'mbReference';
sk.att(30).name  = 'mbAntennaOffset';
sk.att(31).name  = 'mbAntennaDelay';
sk.att(32).name  = 'mbSounderOffset';
sk.att(33).name  = 'mbSounderDelay';
sk.att(34).name  = 'mbVRUOffset';
sk.att(35).name  = 'mbVRUDelay';
sk.att(36).name  = 'mbHeadingBias';
sk.att(37).name  = 'mbRollBias';
sk.att(38).name  = 'mbPitchBias';
sk.att(39).name  = 'mbHeaveBias';
sk.att(40).name  = 'mbDraught';
sk.att(41).name  = 'mbNavType';
sk.att(42).name  = 'mbNavRef';
sk.att(43).name  = 'mbTideType';
sk.att(44).name  = 'mbTideRef';
sk.att(45).name  = 'mbMinDepth';
sk.att(46).name  = 'mbMaxDepth';
sk.att(47).name  = 'mbCycleCounter';

sk.dim(1).name  = 'mbHistoryRecNbr';
sk.dim(1).value = 20;

sk.dim(2).name  = 'mbNameLength';
sk.dim(2).value = 20;

sk.dim(3).name  = 'mbCommentLength';
sk.dim(3).value = 256;

sk.dim(4).name  = 'mbAntennaNbr';
sk.dim(4).value = 1;

sk.dim(5).name  = 'mbBeamNbr';
sk.dim(5).value = 135;

sk.dim(7).name  = 'mbVelocityProfilNbr';
sk.dim(7).value = 2;

sk.var(1).name  = 'mbHistDate'; 	% (20 values)
sk.var(2).name  = 'mbHistTime'; 	% (20 values)
sk.var(3).name  = 'mbHistCode'; 	% (20 values)
sk.var(4).name  = 'mbHistAutor'; 	% (400 values)
sk.var(5).name  = 'mbHistModule'; 	% (400 values)
sk.var(6).name  = 'mbHistComment'; 	% (5120 values)
sk.var(7).name  = 'mbCycle'; 	% (94 values)
sk.var(8).name  = 'mbDate'; 	% (94 values)
sk.var(9).name  = 'mbTime'; 	% (94 values)
sk.var(10).name  = 'mbOrdinate'; 	% (94 values)
sk.var(11).name  = 'mbAbscissa'; 	% (94 values)
sk.var(13).name  = 'mbSonarFrequency'; 	% (94 values)

sk.var(39).name  = 'mbAntenna'; 	% (135 values)
sk.var(40).name  = 'mbBFlag'; 	% (135 values)
sk.var(41).name  = 'mbBeam'; 	% (1 values)
sk.var(42).name  = 'mbAFlag'; 	% (1 values)
sk.var(43).name  = 'mbVelProfilRef'; 	% (512 values)
sk.var(44).name  = 'mbVelProfilIdx'; 	% (2 values)
sk.var(45).name  = 'mbVelProfilDate'; 	% (2 values)
sk.var(46).name  = 'mbVelProfilTime'; 	% (2 values)
%}


function [flag, indLayer] = findIndLayerMBG(this, indImage, DataType, varargin)

[varargin, NoMessage] = getFlag(varargin, 'NoMessage'); %#ok<ASGLU>

flag = 0;

identLayer  = cl_image.indDataType(DataType);
indLayer = findIndLayerSonar(this, indImage, 'DataType', identLayer, 'WithCurrentImage', 1, 'OnlyOneLayer');

if isempty(indLayer)
    if ~NoMessage
        str = sprintf('No "%s" layer.',DataType );
        my_warndlg(str, 1);
    end
    return
end

flag = 1;
