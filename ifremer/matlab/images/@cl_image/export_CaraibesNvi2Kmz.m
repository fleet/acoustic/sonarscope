% Exportation de la navigation Caraibes(R).nvi en fichier GoogleEarth .kmz
%
% Syntax
%   b = export_CaraibesNvi2Kmz(a, nomFicNvi, nomFicKmz) 
%
% Input Arguments
%   a         : Instance de cl_image
%   nomFicNvi : Nom du fichier de navigation
%   nomFicKmz  : Nom du fichier GoogleEarth
%
% Output Arguments
%   b : Instance de cl_image 
%
% Examples 
%   c = export_CaraibesNvi2Kmz(a, 'I:\IfremerToolboxDataPublicSonar\EM300_Ex1.nvi')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = export_CaraibesNvi2Kmz(this, nomFicNvi, nomFicKmz)

if length(nomFicNvi) > 1
    my_warndlg('Only first file is plotted, please proceed in several steps.', 0);
end

[flag, Lat, Lon, Time] = lecFicNav(nomFicNvi);
if ~flag
    messageErreurFichier(nomFicNvi, 'ReadFailure');
    return
end

t = Time.timeMat;
tDeb = t2str(cl_time('timeMat', t(1)));
tFin = t2str(cl_time('timeMat', t(end)));
Label = sprintf('%s\n%s\n%s', nomFicNvi{1}, tDeb, tFin);
Name = nomFicNvi{1};

plot_navOnGoogleEarth({Lat}, {Lon}, [], {Name}, {Label}, nomFicKmz)
