% Exportation d'une image sonar lat�ral au format Caraibes(R)
%
% Syntax
%   export_CaraibesSidescan(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_CaraibesSidescan(this, nomFicOut, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', 1:this(1).nbRows); %#ok<ASGLU>

DataType = cl_image.indDataType('Reflectivity');
flag = testSignature(this(1), 'DataType', DataType, 'GeometryType', ...
    [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist')  cl_image.indGeometryType('PingRange')]);
if ~flag
    return
end

nomFicRef = getNomFicDatabase('Ex_DF1000_ExStr.imo', 'NoMessage');
if isempty(nomFicRef)
    nomFicRef = getNomFicDatabase('DF1000_ExStr.imo');
end

flag = testEcritureFichier(nomFicOut);
if ~flag
    return
end

a_ref = cl_netcdf('fileName', nomFicRef);
sk_ref = get(a_ref, 'skeleton');

Time            = this.Sonar.Time;
Date            = Time.date;
Heure           = Time.heure;
Latitude        = this.Sonar.FishLatitude;
Longitude       = this.Sonar.FishLongitude;

% SonarIdent = get(SonarDescription, 'Sonar.Ident');
SimradResolution = (1500/2) ./ (unique(this.Sonar.SampleFrequency)*1e3);
% SonarDescription = this(1).Sonar.Desciption;
% SampFreq = get(SonarDescription, 'Proc.SampFreq');
% SimradResolution = (1500/2) ./ SampFreq;

if ~isempty(Date)
    Date = Date(suby);
end
if ~isempty(Heure)
    Heure = Heure(suby);
end
if isempty(Latitude)
    mbNorthLatitude = [];
    mbSouthLatitude = [];
else
    Latitude = Latitude(suby);
    mbNorthLatitude = max(Latitude);
    mbSouthLatitude = min(Latitude);
end
if isempty(Longitude)
    mbEastLongitude = [];
    mbWestLongitude = [];
else
    Longitude = Longitude(suby);
    mbEastLongitude = max(Longitude);
    mbWestLongitude = min(Longitude);
end

%     if ~isempty(PortMode)
%         PortMode = PortMode(suby);
%     end
%     if ~isempty(StarboardMode)
%         StarboardMode = StarboardMode(suby);
%     end

%% Modification du skeleton

sk = sk_ref;

% Attributs globaux du format SMF g�or�f�renc�
mbStartDate = this.Sonar.Time(suby(1)).date;
mbEndDate   = this.Sonar.Time(suby(end)).date;
mbStartTime = this.Sonar.Time(suby(1)).heure;
mbEndTime   = this.Sonar.Time(suby(end)).heure;

% Variables du format SMF de navigation


% Attributs globaux du format SMF g�or�f�renc�
numAtt = cdf_find_numAtt(sk, 'mbName');
sk.att(numAtt).value  = 'mlkjmklj';

numAtt = cdf_find_numAtt(sk, 'mbStartDate');
sk.att(numAtt).value  = mbStartDate;

numAtt = cdf_find_numAtt(sk, 'mbStartTime');
sk.att(numAtt).value  = mbStartTime;

numAtt = cdf_find_numAtt(sk, 'mbEndDate');
sk.att(numAtt).value  = mbEndDate;

numAtt = cdf_find_numAtt(sk, 'mbEndTime');
sk.att(numAtt).value = mbEndTime;

if ~isempty(mbNorthLatitude)
    numAtt = cdf_find_numAtt(sk, 'mbNorthLatitude');
    sk.att(numAtt).value = mbNorthLatitude;
    
    numAtt = cdf_find_numAtt(sk, 'mbSouthLatitude');
    sk.att(numAtt).value = mbSouthLatitude;
end

if ~isempty(mbEastLongitude)
    numAtt = cdf_find_numAtt(sk, 'mbEastLongitude');
    sk.att(numAtt).value = mbEastLongitude;
    
    numAtt = cdf_find_numAtt(sk, 'mbWestLongitude');
    sk.att(numAtt).value = mbWestLongitude;
end

numAtt = cdf_find_numAtt(sk, 'grStrRawPixelSize');
sk.att(numAtt).value = SimradResolution;

numAtt = cdf_find_numAtt(sk, 'grStrSamplingFrequency');
sk.att(numAtt).value = unique(this.Sonar.SampleFrequency*1e3);

numAtt = cdf_find_numAtt(sk, 'grStrCelerity');
sk.att(numAtt).value = 1500; % TODO : r�ussir � r�cup�rer la valeur d'origine du fichier

numAtt = cdf_find_numAtt(sk, 'grStrCelerity');
sk.att(numAtt).value = 1500; % TODO : r�ussir � r�cup�rer la valeur d'origine du fichier

numAtt = cdf_find_numAtt(sk, 'grStrAltBeg');
sk.att(numAtt).value = 0; % TODO : comprendre l'int�r�t de ces variables !!!!

numAtt = cdf_find_numAtt(sk, 'grStrAltEnd');
sk.att(numAtt).value = 0; % TODO : comprendre l'int�r�t de ces variables !!!!

numDim = cdf_find_numDim(sk, 'grRawImage_NbLin');
sk.dim(numDim).value = length(suby);
numAtt = cdf_find_numAtt(sk, 'grStrChannelPixNb');
sk.att(numAtt).value = length(suby)/2; % TODO : comprendre l'int�r�t de ces variables !!!!

% Param�tres cartographiques
sk = fill_carto(this.Carto, sk_ref);


% Dimensions
numDim = cdf_find_numDim(sk, 'grWrkImage_NbLin');
sk.dim(numDim).value = length(suby);

numDim = cdf_find_numDim(sk, 'grRawImage_NbLin');
sk.dim(numDim).value = length(suby);

numDim = cdf_find_numDim(sk, 'grWrkImage_NbCol');
sk.dim(numDim).value = this.nbColumns;

numDim = cdf_find_numDim(sk, 'grRawImage_NbCol');
sk.dim(numDim).value = this.nbColumns;
nomLayer = {'grRawImage'; 'grWrkImage'};


% % % % % switch this.GeometryType
% % % % %     case cl_image.indGeometryType('PingAcrossDist')
% % % % %         numDim = cdf_find_numDim(sk, 'grWrkImage_NbCol');
% % % % %         sk.dim(numDim).value = this.nbColumns;
% % % % %
% % % % %         numDim = cdf_find_numDim(sk, 'grRawImage_NbCol');
% % % % %         sk.dim(numDim).value = this.nbColumns;
% % % % %         nomLayer = {'grRawImage';'grWrkImage'};
% % % % %
% % % % %     otherwise
% % % % %         numDim = cdf_find_numDim(sk, 'grRawImage_NbCol');
% % % % %         sk.dim(numDim).value = this.nbColumns;
% % % % %         nomLayer = 'grRawImage';
% % % % % end

%% Cr�ation du fichier r�sultat

b = cl_netcdf('fileName', nomFicOut, 'skeleton', sk);

%% Remplissage des valeurs des variables

liste = {'grRawImage'; 'grWrkImage'; 'Height'; 'Heading'; 'Speed'; 'Roll'; 'Pitch'; 'Immersion'; 'Cable Out'; 'Surface Sound Speed'; 'Yaw'};
if isSonarSignal(this, 'FishLatitude')
    liste{end+1} = 'Latitude';
    liste{end+1} = 'Longitude';
end
if strcmp(this.InitialFileName, nomFicOut)
    [IdentVisu, validation] = my_listdlg(Lang('Info � �craser : ', 'Information to be removed : '), liste);
    if ~validation
        return
    end
else
    IdentVisu = 1:length(liste);
end

% Attributs Flag � 2 : TODO, comprendre la signification de ces flags
% (pas d�crit dans le .cfg CARAIBES).
X = ones(numel(suby), 1, 'uint8') * 2;
b = set_value(b, 'grRawImage_grSyncFlag', X(:));
b = set_value(b, 'grRawImage_grSyncJulianDay_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncJulianMs_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPingNumber_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncImmersion_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtRTHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbRTHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtUsrHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbUsrHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncSpeed_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncHeading_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncRoll_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPitch_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncYaw_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtMaxRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbMaxRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtUsrRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbUsrRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncSensorDrift_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtPixNb_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbPixNb_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncLatitude_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncLongitude_Flag', X(:));

% WrkImage
b = set_value(b, 'grWrkImage_grSyncFlag', X(:));
b = set_value(b, 'grWrkImage_grSyncJulianDay_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncJulianMs_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPingNumber_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncImmersion_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtRTHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbRTHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtUsrHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbUsrHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncSpeed_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncHeading_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncRoll_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPitch_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncYaw_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtMaxRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbMaxRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtUsrRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbUsrRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncSensorDrift_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtPixNb_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbPixNb_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncLatitude_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncLongitude_Flag', X(:));

%% Sauvegarde de l'heure

b = set_value(b, 'grRawImage_grSyncJulianDay', Date(:));
b = set_value(b, 'grWrkImage_grSyncJulianMs', Heure(:));

%% Sauvegarde de la hauteur

if any(IdentVisu == 2)
    X = this.Sonar.Height(suby);
    X(isnan(X)) = 0;
    
    liste ={'grRawImage_grSyncPrtRTHeight'
        'grRawImage_grSyncStbRTHeight'
        'grWrkImage_grSyncPrtUsrHeight'
        'grWrkImage_grSyncStbUsrHeight'};
    
    choix = my_listdlg(Lang('S�lectionnez les variables de Hauteur � �crire', 'Select Height variables to store data'), liste);
    for iChoix=1:length(choix)
        b = set_value(b, liste{choix(iChoix)}, X(:));
    end
    
end

%% Sauvegarde du cap

if any(IdentVisu == 3) && isSonarSignal(this, 'Heading')
    X = this.Sonar.Heading(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncHeading', X(:));
end

%% Sauvegarde de la Speed

if any(IdentVisu == 4) && isSonarSignal(this, 'Speed')
    X = this.Sonar.Speed(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncSpeed', X(:));
    b = set_value(b, 'grWrkImage_grSyncSpeed', X(:));
end

%% Sauvegarde de la Roll

if any(IdentVisu == 5) && isSonarSignal(this, 'Roll')
    X = this.Sonar.Roll(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncRoll', X(:));
    b = set_value(b, 'grWrkImage_grSyncRoll', X(:));
end

%% Sauvegarde de la Pitch

if any(IdentVisu == 6) && isSonarSignal(this, 'Pitch')
    X = this.Sonar.Pitch(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncPitch', X(:));
    b = set_value(b, 'grWrkImage_grSyncPitch', X(:));
end

%% Sauvegarde de l'immersion

if any(IdentVisu == 7) && isSonarSignal(this, 'Immersion')
    % On ram�ne l'immersion en Z positif (convention CARAIBES).
    X = -abs(this.Sonar.Immersion(suby));
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncImmersion', X(:));
    b = set_value(b, 'grWrkImage_grSyncImmersion', X(:));
end

%% Sauvegarde de CableOut

if any(IdentVisu == 8) && isSonarSignal(this, 'CableOut')
    X = this.Sonar.CableOut(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncCableOut', X(:));
    b = set_value(b, 'grWrkImage_grSyncCableOut', X(:));
end

%% Sauvegarde de la SurfaceSoundSpeed

if any(IdentVisu == 9) && isSonarSignal(this, 'SurfaceSoundSpeed')
    X = this.Sonar.SurfaceSoundSpeed(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncPitch', X(:));
    b = set_value(b, 'grWrkImage_grSyncPitch', X(:));
end

%% Sauvegarde de Yaw

if any(IdentVisu == 10) && isSonarSignal(this, 'Yaw')
    X = this.Sonar.Yaw(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncYaw', X(:));
    b = set_value(b, 'grWrkImage_grSyncYaw', X(:));
end

%% Sauvegarde de la latitude

if any(IdentVisu == 11) && isSonarSignal(this, 'FishLatitude')
    X = this.Sonar.FishLatitude(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncLatitude', X(:));
    b = set_value(b, 'grWrkImage_grSyncLatitude', X(:));
end

%% Sauvegarde de la longitude

if any(IdentVisu == 12) && isSonarSignal(this, 'FishLongitude')
    X = this.Sonar.FishLongitude(suby);
    X(isnan(X)) = 0;
    b = set_value(b, 'grRawImage_grSyncLongitude', X(:));
    b = set_value(b, 'grWrkImage_grSyncLongitude', X(:));
end

%% Sauvegarde de l'image

if any(IdentVisu == 1)
    %     nbColImage = size(Image, 2);
    %     if nbColImage > nbCol
    %         Image(:,(nbCol+1):nbColImage) = [];
    %     elseif nbColImage < nbCol
    %         p = floor((nbCol - nbColImage) / 2);
    %         Image = [repmat(0, length(suby), p) Image];
    %         Image(:,nbCol) = 0;
    %     end
    %     b = set_value(b, 'nomLayer', this.Image(suby,:));
    write_autoscale(b, nomLayer{1}, this.Image(suby,:));
    write_autoscale(b, nomLayer{2}, this.Image(suby,:));
end

%% Exportation de la description de l'image en XML

[nomDir, nomFicXml] = fileparts(nomFicOut);
nomFicXml = fullfile(nomDir, [nomFicXml '.xml']);
flag = export_info_xml(this, nomFicXml, 'suby', suby);


% ATTENTION : FONCTION EN DOUBLE AVEC cl_image/sonar_export_nvi
function sk_ref = fill_carto(Carto, sk_ref)

[Ellipsoid, Ellipsoid_name] = getEllipsoidCaraibes(Carto);
DemiGrandAxe = get(Carto, 'Ellipsoide.DemiGrandAxe');   % en metres

numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidA');
sk_ref.att(numAtt).value = DemiGrandAxe;

E2 = floor(get(Carto, 'Ellipsoide.E2'));
numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidE2');
sk_ref.att(numAtt).value = E2;

InvF = get(Carto, 'Ellipsoide.Aplatissement');
numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidInvF');
sk_ref.att(numAtt).value = InvF;

numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidName');
sk_ref.att(numAtt).value = Ellipsoid_name;

'Type of projection not finished'
%{
ProjType = getProjectionCaraibes(Carto);
switch ProjType
case 1 % MERCATOR
Meridian_180 = 0;
numAtt = cdf_find_numAtt(sk_ref, 'mbMeridian_180');
sk_ref.att(numAtt).value = Meridian_180;

case 2  % LAMBERT

case 3  % UTM

case 4  % St�r�ographique polaire

otherwise
my_warndlg(['ATTENTION PROJECTION PAS ENCORE TRAITEE DANS 'mfilename], 1);
end
numAtt = cdf_find_numAtt(sk_ref, 'mbProjType');
sk_ref.att(numAtt).value = ProjType;

% ProjParameterValue = [0  0  0  0  0  0  0  0  0  0];
numAtt = cdf_find_numAtt(sk_ref, 'mbProjParameterValue');
sk_ref.att(numAtt).value = ProjType;

ProjParameterCode = '';
numAtt = cdf_find_numAtt(sk_ref, 'mbProjParameterCode');
sk_ref.att(numAtt).value = ProjParameterCode;
%}
