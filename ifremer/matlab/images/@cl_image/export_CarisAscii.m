% Exportation de l'image au format CARIS ASCII
%
% Syntax
%   export_CarisAscii(a, ...)
%
% Input Arguments
%   a      : Une instance de cl_image
%   nomFic : Non du fichier de sortie (Une numerotation est rajoutee si decoupage par blocks)
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   nomFic = tempname
%   export_CarisAscii(a, 'nomFic', nomFic)
%
% See also cl_image cl_image/set cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_CarisAscii(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = 0;

fid = fopen(nomFic, 'w+');
if fid == -1
    str1 = sprintf('Impossible de cr�er le fichier "%s".', nomFic);
    str2 = sprintf('It is impossible to create file "%s".', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

% yDir = ((this.y(end) - this.y(1)) > 0);
% flagOppose = ~xor(yDir , (this.YDir == 1));


x = this.x(subx);
y = this.y(suby);
% if flagOppose
%     y = y; %flipud(y(:)); % Non valid�
% else
%     y = y; % Valid�
% end

if this.GeometryType == cl_image.indGeometryType('LatLong')
    fprintf(fid, '//Lat (DD) Long (DD) ');
else
    fprintf(fid, '//Y (m) X (m) ');
end
if this.DataType == cl_image.indDataType('Bathymetry')
    fprintf(fid, 'Depth\n');
else
    fprintf(fid, '%s\n', cl_image.strDataType{this.DataType});
end

str1 = 'Export de la grille au format CARIS ASCII';
str2 = 'Exports to CARIS ASCII';
N = length(suby);
hw = create_waitbar(Lang(str1,str2), 'N', N);
k = 0;
for k1=1:N
    my_waitbar(k1, N, hw)
    Ligne = this.Image(suby(k1), subx);
    for k2=1:length(subx)
        z = Ligne(k2);
        if ~isnan(z)
        k = k + 1;
%         fprintf(fid, '%d \t %11.8f \t %11.8f \t %f\n', k, y(k1), x(k2), -z);
        fprintf(fid, '%d \t %16.16g \t %16.16g \t %f\n', k, y(k1), x(k2), -z);
        end
    end
end
my_close(hw, 'MsgEnd')
fclose(fid);
flag = 1;
