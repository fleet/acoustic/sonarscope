function export_ExRaw(this, varargin)

% [subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', this.InitialFileName);
[varargin, SignalNames] = getPropertyValue(varargin, 'SignalNames', []); %#ok<ASGLU>

%% Contr�les

ident_Reflectivity = cl_image.indDataType('Reflectivity');

% TODO : � compl�ter pour le subbottom

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'), 'DataType', ident_Reflectivity, ...
    'InitialFileFormat', 'Simrad_ExRaw');
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

%% Type de donn�es � sauver

str{1}     = 'Time';
str{end+1} = 'Immersion';
str{end+1} = 'Height';
str{end+1} = 'Speed';
str{end+1} = 'Heading';
str{end+1} = 'Roll';
str{end+1} = 'Pitch';
str{end+1} = 'SoundVelocity'; % SurfaceSoundSpeed
str{end+1} = 'FishLatitude';
str{end+1} = 'FishLongitude';
%     str{end+1} = 'ShipLatitude';
%     str{end+1} = 'ShipLongitude';
str{end+1} = 'Mode_1';
%     str{end+1} = 'Mode_2';
str{end+1} = 'Heave';
str{end+1} = 'Tide';
%     str{end+1} = 'PingCounter';
str{end+1} = 'SampleFrequency';
%     str{end+1} = 'Temperature';
str{end+1} = 'Image';

if isempty(SignalNames)
    str1 = 'Liste des choses qui peuvent �tre sauv�es.';
    str2 = 'List of things that cab be saved.';
    [rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 3);
    if ~flag
        return
    end
else
    if ~iscell(SignalNames)
        SignalNames = {SignalNames};
    end
    rep = [];
    for k1=1:length(SignalNames)
        pppp = strfind(str, SignalNames{k1});
        for k2=1:length(pppp)
            if ~isempty(pppp{k2})
                rep(end+1) = k2; %#ok<AGROW>
            end
        end
    end
end

%% Sauve Signals

a = cl_ExRaw('nomFic', nomFic);

for k=1:length(rep)
    SignalName = str{rep(k)};
    switch SignalName
        case 'Time'
            SignalValues = this.Sonar.Time;
            flag = save_signal(a, 'Ssc_Sample', SignalName, SignalValues.timeMat);
            if ~flag
                return
            end
        case 'Image'
            SignalValues = this.Image(:,:);
            iDeb = strfind(this.Comments, 'Frequency=');
            str = this.Comments(iDeb+10:end-4);
            FreqkHz = str2double(str);
            nomVar = sprintf('Amplitude_%dkHz', FreqkHz);
            flag = save_image(a, 'Ssc_Sample', nomVar, SignalValues);
            if ~flag
                return
            end
            
            
        otherwise
            SignalValues = this.Sonar.(SignalName);
            flag = save_signal(a, SignalName, SignalValues);
            if ~flag
                return
            end
    end
end
