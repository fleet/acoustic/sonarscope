% Exportation de l'image dans GoogleEarth
%
% Syntax
%   export_GoogleEarth(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   nomFic   : Non du fichier de sortie (Une numerotation est rajoutee si decoupage par blocks)
%   nbLigMax : Nombre maximum de lignes par block (Inf par defaut)
%   nbColMax : Nombre maximum de colonnes par block (Inf par defaut)
%   CLim     : Limites du rehaussement de contraste si format d'image 8bits.
%              Si cette valeur n'est pas donnee, c'est le CLim de this qui est utilise
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   nomFic = tempname
%   export_GoogleEarth(a, 'nomFic', nomFic)
%
% See also cl_image cl_image/set cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_GoogleEarth(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, sizeTilex]   = getPropertyValue(varargin, 'nbLigMax',    256);
[varargin, sizeTiley]   = getPropertyValue(varargin, 'nbColMax',    256);
[varargin, nomFicKmz]   = getPropertyValue(varargin, 'nomFic',      []);
[varargin, flagFillNaN] = getPropertyValue(varargin, 'flagFillNaN', 0); %#ok<ASGLU>
% [varargin, valMask0]  = getPropertyValue(varargin, 'valMask0',    0);

if isempty(nomFicKmz)
    nomFicKmz = [my_tempname '.kmz'];
end

% useParallel = get_UseParallelTbx; % Contreproductif si fait sur boucle des x : essayer sur boucle des y
useParallel = 0; % Modif JMA le 07/10/2020 car bugs sur PC CArla MAYOBS15

% minLodPix  = 256;
minLodPix  = 128;
%sizeTile   = 256;

[~, nomFic] = fileparts(nomFicKmz);
nomFicSimple = nomFic;

DirTemp = my_tempname;
flag = mkdir(DirTemp);
if ~flag
    return
end

DirImages = fullfile(DirTemp, 'images');
flag = mkdir(DirImages);
if ~flag
    return
end
if length(this) ~= 1
    my_warndlg('cl_image:export_GoogleEarth Une image � la fois svp', 0);
    return
end

if isempty(this.Image)
    my_warndlg('cl_image:export_GoogleEarth L''image est vide', 0);
    return
end

% Cr�ation de la Colorbar
% bornes de la colorBar
% minval = min(min(this.Image));
% maxval = max(max(this.Image));

switch this.ImageType
    case 1 % Intensit�
        %         if this.ColormapIndex == 3 % jet uniquement
        minval = this.CLim(1);
        maxval = this.CLim(2);
        if isnan(minval)
            flag = 0;
            return
        end
        nomColorBar = [nomFicSimple 'ColorBar' '.png'];
        %cr�ation de la colorBar
        nomFicColorBar = fullfile(DirTemp, nomColorBar);
        my_colorBarInPng(minval, maxval, nomFicColorBar, this.Colormap);
        %         else
        %             nomColorBar = [];
        %         end
        
    case 2 % RGB
        %         nomColorBar = []; % Avant 06/01/2017
        
        % Modif JMA le 06/01/2017 pour Ridha
        if isempty(this.CLim) || any(isnan(this.CLim))
            nomColorBar = [];
        else
            minval = this.CLim(1);
            maxval = this.CLim(2);
            nomColorBar = [nomFicSimple 'ColorBar' '.png'];
            %cr�ation de la colorBar
            nomFicColorBar = fullfile(DirTemp, nomColorBar);
            my_colorBarInPng(minval, maxval, nomFicColorBar, this.Colormap);
        end
        
    case 3 % Indexee
        minval = this.CLim(1);
        maxval = this.CLim(2);
        nomColorBar = [nomFicSimple 'ColorBar' '.png'];
        %cr�ation de la colorBar
        nomFicColorBar = fullfile(DirTemp, nomColorBar);
        my_colorBarInPng(minval, maxval, nomFicColorBar, this.Colormap);
        
    case 4 % Binaire
        nomColorBar = [];
end

nomFicKml = fullfile(DirTemp, [nomFicSimple, '.kml']);

Ny = length(suby);
Nx = length(subx);

NLevels = max(nextpow2(Ny/sizeTiley), nextpow2(Nx/sizeTilex)) + 1;
nbBlocksY = ceil(Ny / sizeTiley);
nbBlocksX = ceil(Nx / sizeTilex);

CLim  = this.CLim;
Video = this.Video;
J = extraction(this, 'suby', suby, 'subx', subx, 'Mute', 1);
J = set(J, 'CLim', CLim, 'Video', Video);
North = max(J.y);
South = min(J.y);
East  = max(J.x);
West  = min(J.x);
posLatLon = [North South West East];

fid = fopen(nomFicKml, 'w+');
fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid, '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n');
fprintf(fid,'<Document>\n');

[~, NomKmz] = fileparts(nomFicKmz);
fprintf(fid,'<name>%s</name>\n', NomKmz);

%% Ajout de la colorbar dans le fichier KML

if ~isempty(nomColorBar)
    fprintf(fid,'<Folder>\n');
    fprintf(fid,'\t<name>Color Bar</name>\n');
    fprintf(fid,'\t<ScreenOverlay>\n');
    fprintf(fid,'\t\t<visibility>0</visibility>');
    fprintf(fid,'\t\t<Icon>\n');
    fprintf(fid,'\t\t\t<href>%s</href>\n', nomColorBar);
    fprintf(fid,'\t\t</Icon>\n');
    fprintf(fid,'\t\t<overlayXY x="0" y="1" xunits="fraction" yunits="fraction"/>\n');
    fprintf(fid,'\t\t<screenXY x="0.85" y="0.85" xunits="fraction" yunits="fraction"/>\n');
    fprintf(fid,'\t\t<size x="80" y="400" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid,'\t</ScreenOverlay>\n');
    fprintf(fid,'</Folder>\n');
end

%%

x = 1:sizeTilex;
y = 1:sizeTiley;

dify = this.y(end) - this.y(1);
if dify > 0
    y = fliplr(y);
    J.Image = my_flipud(J.Image(:,:,:));
end

%% Boucles sur les niveaux de la pyramide

for k=NLevels-1:-1:0
    DirImages = fullfile(DirTemp, num2str(k));
    flag = mkdir(DirImages);
    if ~flag
        return
    end
    
    flagTile      = false(nbBlocksY,nbBlocksX);
    latlonTab     = cell(nbBlocksY,nbBlocksX);
    nomFicTileTab = cell(nbBlocksY,nbBlocksX);
    
    strTitle = sprintf('Creates tiles : %s                     .', DirImages);
    if useParallel && ((nbBlocksY*nbBlocksX) > 100) % TODO : on ne lance la //Tbx que si �a en vaut vraiment la peine
        try
            [hw, DQ] = create_parforWaitbar(nbBlocksY, strTitle);
            parfor ky=1:nbBlocksY
                flagTileY      = flagTile(ky,:);
                latlonTabY     = latlonTab(ky,:);
                nomFicTileTabY = nomFicTileTab(ky,:);
                
                [flagTileY, latlonTabY, nomFicTileTabY] = processBlocksY(x, y, ky, sizeTilex, sizeTiley, nbBlocksX, nbBlocksY, ...
                    Nx, Ny, DirTemp, k, posLatLon, J, flagTileY, latlonTabY, nomFicTileTabY);
                
                flagTile(ky,:)      = flagTileY;
                latlonTab(ky,:)     = latlonTabY;
                nomFicTileTab(ky,:) = nomFicTileTabY;
                
                send(DQ, ky);
            end
        catch
            my_close(hw)
            hw = create_waitbar(strTitle, 'Entete', 'IFREMER - SonarScope : Pyramid Image Creation', 'N', nbBlocksY);
            for ky=1:nbBlocksY
                [flagTile(ky,:), latlonTab(ky,:), nomFicTileTab(ky,:)] = processBlocksY(x, y, ky, sizeTilex, sizeTiley, nbBlocksX, nbBlocksY, ...
                    Nx, Ny, DirTemp, k, posLatLon, J, flagTile(ky,:), latlonTab(ky,:), nomFicTileTab(ky,:));
                my_waitbar(ky, nbBlocksY, hw);
            end
        end
    else
        hw = create_waitbar(strTitle, 'Entete', 'IFREMER - SonarScope : Pyramid Image Creation', 'N', nbBlocksY);
        for ky=1:nbBlocksY
            [flagTile(ky,:), latlonTab(ky,:), nomFicTileTab(ky,:)] = processBlocksY(x, y, ky, sizeTilex, sizeTiley, nbBlocksX, nbBlocksY, ...
                Nx, Ny, DirTemp, k, posLatLon, J, flagTile(ky,:), latlonTab(ky,:), nomFicTileTab(ky,:));
            my_waitbar(ky, nbBlocksY, hw);
        end
    end
    my_close(hw)
    
    %% Ecriture des infos de la couche de la pyramide dans le fichier KML
    
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '\t\t<name>level %d</name>\n', k);
    for ky=1:nbBlocksY
        for kx=1:nbBlocksX
            if flagTile(ky,kx)
                writeInKml(fid, k, nomFicTileTab{ky,kx}, minLodPix, latlonTab{ky,kx});
            end
        end
    end
    fprintf(fid, '</Folder>\n');
    
    %% R�duction par 2 de l'image en x et en y
    
    J = downsize(J, 'pasx', 2, 'pasy', 2, 'FillNaN', flagFillNaN);
    J.CLim = CLim;
    Ny  = J.nbRows;
    Nx  = J.nbColumns;
    nbBlocksY = ceil(Ny/sizeTiley);
    nbBlocksX = ceil(Nx/sizeTilex);
end
fprintf(fid,'</Document>\n</kml>');
fclose(fid);

%% Cr�ation du fichier KMZ (zip the .KML avec les donn�es)

% TODO : Tester zipKml2kmz
% flag = zipKml2kmz(nomFicKmz, DirTemp)
 
str1 = sprintf('Compression du fichier "%s"', nomFicKmz);
str2 = sprintf('Zipping file "%s"', nomFicKmz);
WorkInProgress(Lang(str1,str2), 0)

zip(nomFicKmz, fullfile(DirTemp, '*'))
flag = copyfile([nomFicKmz, '.zip'], nomFicKmz);
if ~flag
    str = sprintf('Impossible to copy file %s to %s.', [nomFicKmz, '.zip'], nomFicKmz);
    my_warndlg(str, 1);
    return
end
delete([nomFicKmz, '.zip'])

flag = rmdir(DirTemp, 's');
if ~flag
    for k=1:60
        flag = rmdir(DirTemp, 's');
        if flag
            break
        end
        pause(1)
    end
    if ~flag
        str1 = sprintf('Attention, le r�pertoire "%s" n''a pas pu �tre supprim� automatiquement.', DirTemp);
        str2 = sprintf('Warning : folder "%s" could not be removed automaticaly.', DirTemp);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportGoolgleEarthDirNotRemoved');
        flag = 1; % Pour que le .kmz soit affich�
        return
    end
end



function [north, south, west, east] = calcCoord(posLatLon, nbBlocksY, nbBlocksX, coeffx, coeffy, ky, kx)

North = posLatLon(1,1);
South = posLatLon(1,2);
West  = posLatLon(1,3);
East  = posLatLon(1,4);

intrvlx  = (East - West);
intrvly  = (North - South);
deplactx = intrvlx * coeffx;
deplacty = intrvly * coeffy;

if coeffy > 1
    north = North;
    south = South;
else
    north = North - (ky-1)*deplacty;
    if ky < nbBlocksY
        south = North - (ky)*deplacty;
    else
        south = South;
    end
end

if coeffx > 1
    west = West;
    east = East;
else
    west = West + (kx-1) * deplactx;
    if kx < nbBlocksX
        east = West + kx * deplactx;
    else
        east = East;
    end
end

Y = [south north];
X = [west east];

south = Y(1);
north = Y(2);
west  = X(1);
east  = X(2);


function  writeInKml(fid, k, nomFicTile, minLodPix, latlon)

north = latlon(1,1);
south = latlon(1,2);
west  = latlon(1,3);
east  = latlon(1,4);

fprintf(fid, '<Folder>\n');
fprintf(fid, '\t\t<name>%s</name>\n', nomFicTile);
fprintf(fid, '\t\t<Region>\n');
fprintf(fid, '\t\t<LatLonAltBox>\n');
fprintf(fid, '\t\t\t<north>%19.14f</north>\n',north);
fprintf(fid, '\t\t\t<south>%19.14f</south>\n',south);
fprintf(fid, '\t\t\t<west>%19.14f</west>\n',west);
fprintf(fid, '\t\t\t<east>%19.14f</east>\n',east);
fprintf(fid, '\t\t</LatLonAltBox>\n');
fprintf(fid, '\t\t<Lod>\n');
fprintf(fid, '\t\t\t<minLodPixels>%d</minLodPixels>\n', minLodPix);
fprintf(fid, '\t\t\t<maxLodPixels>%d</maxLodPixels>\n', -1);
fprintf(fid, '\t\t\t<minFadeExtent>%d</minFadeExtent>\n', 0);
fprintf(fid, '\t\t\t<maxFadeExtent>%d</maxFadeExtent>\n', 0);
fprintf(fid, '\t\t</Lod>\n');
fprintf(fid, '\t\t</Region>\n');
fprintf(fid, '\t\t<GroundOverlay>\n');
fprintf(fid, '\t\t\t<name>%s</name>\n', nomFicTile);
fprintf(fid,'\t\t\t<drawOrder>%d</drawOrder>\n',k);
fprintf(fid,'\t\t\t<Icon>\n');
fprintf(fid,'\t\t\t\t<href>%s</href>\n',nomFicTile);
fprintf(fid,'\t\t\t</Icon>\n');
fprintf(fid,'\t\t\t<gx:altitudeMode>clampToSeaFloor</gx:altitudeMode>\n');
fprintf(fid, '\t\t\t<LatLonAltBox>\n');
fprintf(fid, '\t\t\t\t<north>%19.14f</north>\n', north);
fprintf(fid, '\t\t\t\t<south>%19.14f</south>\n', south);
fprintf(fid, '\t\t\t\t<west>%19.14f</west>\n',   west);
fprintf(fid, '\t\t\t\t<east>%19.14f</east>\n',   east);
fprintf(fid, '\t\t\t</LatLonAltBox>\n');
fprintf(fid, '\t\t</GroundOverlay>\n');
fprintf(fid, '</Folder>\n');



function [flagTile, latlonTab, nomFicTileTab] = exportTileImage(J1, DirTemp, k, posLatLon, x, Nx, Ny, ...
    nbBlocksY, nbBlocksX, kx, ky, sizeTilex, sizeTiley, varargin)

global isUsingParfor %#ok<GVMIS>
global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[varargin, isUsingParfor]        = getPropertyValue(varargin, 'isUsingParfor',        isUsingParfor);
[varargin, NUMBER_OF_PROCESSORS] = getPropertyValue(varargin, 'NUMBER_OF_PROCESSORS', NUMBER_OF_PROCESSORS); %#ok<ASGLU>

subxBlock = (x + (kx-1)*sizeTilex);
subxBlock(subxBlock > Nx) = [];
if isempty(subxBlock)
    return
end

slash = '\';

nomFicTile = sprintf('%d%crow_%d_column_%d.png', k, slash, ky, kx);
nomFicGif = fullfile(DirTemp, nomFicTile);

% TODO export_gif doit �tre renomm� en export_image car la fonction permet d'exporter toutes
% sortes de formats et ici en l'occurrence le format utilis� est le .png
%             flag = export_gif(J, nomFicGif, 'subx', subxBlock, 'suby', subyBlock, 'ESRIWorlfFile', 0);
flag = export_gif(J1, nomFicGif, 'subx', subxBlock, 'ESRIWorlfFile', 0);

if flag
    coeffx = sizeTilex / Nx;
    coeffy = sizeTiley / Ny;
    
    [north, south, west, east] = calcCoord(posLatLon, nbBlocksY, nbBlocksX, coeffx, coeffy, ky, kx);
    latlon =  [north, south, west, east];
    %                 writeInKml(fid, k, nomFicTile, minLodPix, latlon);
    
    flagTile      = 1;
    latlonTab     = latlon;
    nomFicTileTab = nomFicTile;
else
    flagTile      = 0;
    latlonTab     = [];
    nomFicTileTab = [];
end


function [flagTileX, latlonTabX, nomFicTileTabX] = processBlocksY(x, y, ky, sizeTilex, sizeTiley, nbBlocksX, nbBlocksY, Nx, Ny, DirTemp, k, posLatLon, J, ...
    flagTileX, latlonTabX, nomFicTileTabX)

subyBlock = (y + (ky-1)*sizeTiley);
subyBlock(subyBlock > Ny) = [];
if length(subyBlock) < 2
    return
end
J1 = extraction_subxsuby(J, 'suby', subyBlock);
J1.CLim = J.CLim;
for kx=1:nbBlocksX
    [flag, latlon, nomFicTile] = exportTileImage(J1, DirTemp, k, posLatLon, x, Nx, Ny, ...
        nbBlocksY, nbBlocksX, kx, ky, sizeTilex, sizeTiley);
    if flag
        flagTileX(1,kx)      = flag;
        latlonTabX{1,kx}     = latlon;
        nomFicTileTabX{1,kx} = nomFicTile;
    end
end
