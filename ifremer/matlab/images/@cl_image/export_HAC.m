function export_HAC(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

%% Contr�les

ident_Reflectivity = cl_image.indDataType('Reflectivity');

% TODO : � compl�ter pour le subbottom

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'), 'DataType', ident_Reflectivity, ...
    'InitialFileFormat', 'HAC');
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

%% Type de donn�es � sauver

str{1} = 'Height';
% str{2} = 'Heading';
% str{3} = 'FishLatitude';
% str{4} = 'FishLongitude';
% str{5} = 'Image';
str1 = 'Liste des choses qui peuvent �tre sauv�es.';
str2 = 'List of things that can be saved.';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1);
if ~flag
    return
end

%% Sauve Signals

a = cl_hac('nomFic', nomFic);

Comments = this.Comments;
mots = strsplit(Comments, {' ';'='});
% [mots, SounderName] = getPropertyValue(mots, 'SounderName', []);
[mots, NumSounder] = getPropertyValue(mots, 'NumSounder', []);
[mots, NumChannel] = getPropertyValue(mots, 'NumChannel', []);
% [mots, Frequency] = getPropertyValue(mots, 'Frequency', []);

NumSounder = str2double(NumSounder);
NumChannel = str2double(NumChannel);
% Frequency  = str2double(Frequency)

if any(rep == 1) % Height
    
    %     Data.DetectedBottomRange(:,k) = Height .* SoundVelocity / (2 * SampleFrequency);
    %TODO : remplacer SurfaceSoundSpeed par MeanSurfaceSoundSpeed
    H = this.Sonar.Height .* this.Sonar.SurfaceSoundSpeed ./ (2 * this.Sonar.SampleFrequency);
    
    
    % TODO : GLU ici il me manque un truc pour obtenir le num�ro de cette
    % fr�quence dans la liste des fr�quences disponibles pour ce sondeur
    % du genre NumChannel = identNumChannel(a, NumSounder, 'Freq', this.Sonar.SampleFrequency)
    
    flag = save_signal(a, NumSounder, NumChannel, 'DetectedBottomRange', H);
    if ~flag
        return
    end
end

% TODO
if any(rep == 2) % Heading
    flag = save_signal(a, 'heading', this.Sonar.Heading(:));
    if ~flag
        return
    end
end

% TODO
if any(rep == 3) % FishLatitude
    flag = save_signal(a, 'fishLat',  this.Sonar.FishLatitude(:));
    if ~flag
        return
    end
end

% TODO
if any(rep == 4) % FishLongitude
    flag = save_signal(a, 'fishLon',  this.Sonar.FishLongitude(:));
    if ~flag
        return
    end
end

%% Sauve image

% TODO
if any(rep == 5) % Image
    'TODO'
    %     flag = save_signal(a, 'shipLon',  ShipLongitude);
    %     if ~flag
    % %         return
    %     end
end

% switch % Sonar ou subbottom
% case
if ~isequal(subx, 1:this.nbColumns) || ~isequal(suby, 1:this.nbRows)
    my_warndlg('Only the whole image can be flushed into the .all file', 1);
    return
end

%             nomFicSeabed = SimradSeabedNames(this.InitialFileName);
%             status = export_ermapper(this, nomFicSeabed.ReflectivityErs, 'Open', 'r+');
%             if ~status
%                 return
%             end
% end

str1 = 'TODO';
str2 = sprintf('You think, your data is saved in the .all file. They are not, they are saved on SonarScope cache files.');
my_warndlg(Lang(str1,str2), 1);
