function export_Image3D(this, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []);
[varargin, Fig]  = getPropertyValue(varargin, 'Fig',  []); %#ok<ASGLU>

if isempty(suby)
    suby = 1:this.nbRows;
end

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingDepth'));
if ~flag
    return
end

Lat = this.Sonar.FishLatitude;
Lon = this.Sonar.FishLongitude;

Carto = cl_carto('Projection.Type', 2, ...
    'Projection.Mercator.lat_ech_cons',    floor(mean(Lat, 'omitnan')), ...
    'Projection.Mercator.long_merid_orig', floor(mean(Lon, 'omitnan')));
[x, y] = latlon2xy(Carto, Lat, Lon);
x = x(suby);
y = y(suby);

I = this.Image(suby,:,:);
I = I';
z = -abs(this.x(:));
zI = repmat(z, 1, size(I,2));
xI = repmat(x', size(I,1), 1);
yI = repmat(y', size(I,1), 1);

if isempty(Fig) || ~ishandle(Fig)
    [~, haxe] = FigUtils.createSScFigure;
    axes(haxe(1))
    set(gcf, 'Tag', 'MosaicPlot_GeoYX');
else
    figure(Fig);
    hold on;
end

ImageName = this.Name;
map  = this.Colormap;
CLim = this.CLim;
ImageType =  this.ImageType;
switch ImageType
    case 1
        %         surf(lon_surfER60_db(sub,:,k2), lat_surfER60_db(sub,:,k2), depth_surface_ER60_db(sub,:,k2), idx_ER60(sub,:,k2)));
        surf(xI, yI, zI, double(I));
        shading flat; hold on;
        if this.Video == 1
            colormap(map);
        else
            colormap(flipud(map));
        end
        set(gca, 'CLim', CLim)
    case 2
        surf(yI, xI, my_transpose(I)); axis xy; axis tight;
    case 3
        surf(yI, xI, I'); axis xy; axis tight; colormap(map);
end
title(ImageName, 'Interpreter', 'none')
axis tight
set(gca, 'Tag', 'AxePlotMos') % Temporaire : Pour pouvoir demander la superposition dans la m�me figure ou pas lors de l'export de profil sismique en g�om�trie PingDepth
