function export_ImageSignals(this, indImage, indLayers, liste, index, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, currentPoint] = getPropertyValue(varargin, 'currentPoint', []); %#ok<ASGLU>

flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

kTime = [];
for k=1:length(liste)
    if strcmp(liste{k}, 'Time')
        kTime = k;
        break
    end
end

if isempty(kTime)
    Time = [];
else
    % Time = get_vecteur(this(indImage), 'Time', 'suby', suby);
    [flag, Time] = getValSignalIfAny(this(indImage), 'Time', 'suby', suby);
    if ~flag
        return
    end
end

%% Création des axes

nbImages = length(indLayers);
nbAxes = length(index) + nbImages;
FigUtils.createSScFigure;
for k=1:nbAxes
    haxe(k) = subplot(nbAxes, 1, k); %#ok<AGROW>
end

%% Affichage des images

for k=1:nbImages
    ImageName = this(indLayers(k)).Name';
    map       = this(indLayers(k)).Colormap;
    CLim      = this(indLayers(k)).CLim;
    if this(indLayers(k)).Video == 2
        map = flipud(map);
    end
    ImageType = this(indLayers(k)).ImageType;

    if isequal(this(indLayers(k)).TagSynchroX, this(indImage).TagSynchroX)
        subxHere = subx;
    else
        subxHere = 1:this(indLayers(k)).nbColumns;
    end
    [I, xI, yI] = get_val_ij(this(indLayers(k)), suby, subxHere);
    switch ImageType
        case 1
            imagesc(haxe(k), yI, xI, I', CLim); axis(haxe(k), 'xy'); axis(haxe(k), 'tight'); colormap(haxe(k), map);
        case 2
            image(haxe(k), yI, xI, my_transpose(I)); axis(haxe(k), 'xy'); axis(haxe(k), 'tight');
        case 3
            image(haxe(k), yI, xI, I'); axis(haxe(k), 'xy'); axis(haxe(k), 'tight'); colormap(haxe(k), map);
    end
    title(haxe(k), ImageName, 'Interpreter', 'none')
    colorbar(haxe(k))
end

%% Affichage des signaux

y = get(this(indImage), 'y');
for k=1:length(index)
    switch liste{index(k)}
        case 'Vertical profile'
            x = get_x_inXLim(this(indImage));
            if isempty(x) || isempty(currentPoint)
                return
            end
            [ix, iy] = xy2ij(this(indImage), currentPoint(1), currentPoint(2)); %#ok<ASGLU>
            Signal = get_val_ij(this(indImage), suby, ix);
            Unit   = this(indImage).Unit;
            
        case 'MeanValueOfCurrentExtent'
            Signal = get_val_ij(this(indImage), suby, subx);
            Signal = mean(Signal, 2, 'omitnan');
            Unit   = this(indImage).Unit;
            
        otherwise
            [flag, Signal, Unit] = getValSignalIfAny(this(indImage), liste{index(k)}, 'suby', suby);
            if ~flag
                return
            end
    end
    kAxe = k + nbImages;
    if isempty(Signal)
        title(haxe(kAxe), [liste{index(k)} ' - Empty']);
    else
        if strcmp(liste{index(k)}, 'Time')
            Signal = datetime(Signal, 'ConvertFrom', 'datenum');
        end
        hc = PlotUtils.createSScPlot(haxe(kAxe), y(suby), Signal); grid(haxe(kAxe), 'on');
        set(hc, 'UserData', Time)
        if isempty(Unit)
            Title = liste{index(k)};
        else
            Title = sprintf('%s (%s)', liste{index(k)}, Unit);
        end
        title(haxe(kAxe), Title, 'Interpreter', 'none');
        
        hColorbar = colorbar(haxe(kAxe));
        set(hColorbar, 'Visible', 'Off')
    end
    %                 view(90, 90)
end
linkaxes(haxe, 'x');

pause(0.1)
axis tight
