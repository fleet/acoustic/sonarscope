function export_ImageSignalsAccordingTime(this, cross, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = testSignature(this, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

liste = get_liste_vecteurs(this, 'VerticalOnly');
if ~isempty(liste)
    kTime = [];
    for k=1:length(liste)
        if strcmp(liste{k}, 'Time')
            kTime = k;
            break
        end
    end
    if isempty(kTime)
        Time = [];
    else
%         Time = get_vecteur(this, 'Time', 'suby', suby);
        [flag, Time] = getValSignalIfAny(this, 'Time', 'suby', suby);
        if ~flag
            return
        end
    end
    
    if isempty(Time)
        my_warndlg();
        return
    end
    
    [etat, flag] = my_listdlg(Lang('Courbe � visualiser', 'Curves to display'), ...
        liste, 'InitialValue', 1:length(liste));
    if ~flag
        return
    end
    if ~isempty(etat)
        nbAxes = length(etat) + 1;
        FigUtils.createSScFigure;
        for k=1:nbAxes
            haxe(k) = subplot(nbAxes, 1, k); %#ok<AGROW>
        end
        [I, xI, yI] = get_val_ij(this, suby, subx); %#ok<ASGLU>
%         TempsEnSecondes = (Time - Time(1)) / 1000;
        
        TempsEnSecondes = datetime(Time, 'ConvertFrom', 'datenum');
        
%         T = datetime(Time, 'ConvertFrom', 'datenum');
%         TempsEnSecondes = T;

        ImageName = this.Name;
        map = this.Colormap;
        CLim = this.CLim;
        axes(haxe(1))
        switch this.ImageType
            case 1
                pcolor(TempsEnSecondes, xI, double(I')); axis xy; axis tight; colormap(map); shading flat
                set(gca, 'CLim', CLim)
            case 2
                pcolor(TempsEnSecondes, xI, double(my_transpose(I))); axis xy; axis tight; shading flat
            case 3
                pcolor(TempsEnSecondes, xI, double(I')); axis xy; axis tight; colormap(map); shading flat
        end
        title(ImageName, 'Interpreter', 'none')
        colorbar
        
        for k=1:length(etat)
            switch liste{etat(k)}
                case 'Vertical profile'
                    x = get_x_inXLim(this);
                    if isempty(x)
                        return
                    end
                    [ix, iy] = xy2ij(this, cross(1), cross(2)); %#ok<ASGLU>
                    Signal = get_val_ij(this, suby, ix);
                    Unit   = this.Unit;
                    
                case 'MeanValueOfCurrentExtent'
                    Signal = get_val_ij(this, suby, subx);
                    Signal = mean(Signal, 2, 'omitnan');
                    Unit   = this.Unit;
                    
                otherwise
                    [flag, Signal, Unit] = getValSignalIfAny(this, liste{etat(k)}, 'suby', suby);
                    if ~flag
                        return
                    end
            end
            if isempty(Signal)
                title(haxe(k+1),[liste{etat(k)} ' - Empty']);
            else
                if strcmp(liste{etat(k)}, 'Time')
                    Signal = datetime(Signal, 'ConvertFrom', 'datenum');
                end
%                 PlotUtils.createSScPlot(haxe(k+1), TempsEnSecondes, Signal, 'Time', Time); grid on; xlabel('Nb secondes'); grid(haxe(k+1), 'on');
                PlotUtils.createSScPlot(haxe(k+1), TempsEnSecondes, Signal); grid on; xlabel('Nb secondes'); grid(haxe(k+1), 'on');
                if isempty(Unit)
                    Title = liste{etat(k)};
                else
                    Title = sprintf('%s (%s)', liste{etat(k)}, Unit);
                end
                title(haxe(k+1), Title);
                
                hColorbar = colorbar(haxe(k+1));
                set(hColorbar, 'Visible', 'Off')
            end
            %                 view(90, 90)
        end
        linkaxes(haxe, 'x');
    end
    pause(0.5)
    axis tight
end
