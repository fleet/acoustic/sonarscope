function export_ImageSignalsDialog(this, indImage, listLayers, listSignals, index, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, currentPoint] = getPropertyValue(varargin, 'currentPoint', []); %#ok<ASGLU>

flag = testSignature(this((indImage)), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

kTime = [];
for k=1:length(listSignals)
    if strcmp(listSignals{k}, 'Time')
        kTime = k;
        break
    end
end

x = get(this(indImage), 'x');
y = get(this(indImage), 'y');

xSample = XSample('name', 'Pings', 'data', y(suby));
signalList = ClSignal.empty();


ySample = YSample('data', NaN(size(suby)));
for k=1:length(listLayers)
    that = this(listLayers(k));
    signalList(end+1) = ClSignal('Name', that.Name, 'ySample', ySample, 'xSample', xSample); %#ok<AGROW>
    I = get_val_ij(that, suby, subx);
    
    imageData(k) = ClImageData('cData', flipud(I'), 'xData', y(suby), 'yData', x(subx), 'colormapData', that.Colormap, ...
        'clim', that.CLim, 'name', that.Name); %#ok<AGROW>
end

for k=1:length(index)
    switch listSignals{index(k)}
        case 'Vertical profile'
            x = get_x_inXLim(this(indImage));
            if isempty(x) || isempty(currentPoint)
                continue
            end
            [ix, iy] = xy2ij(this(indImage), currentPoint(1), currentPoint(2)); %#ok<ASGLU>
            Signal = get_val_ij(this(indImage), suby, ix);
            Unit   = this(indImage).Unit;
            
        case 'MeanValueOfCurrentExtent'
            Signal = get_val_ij(this(indImage), suby, subx);
            Signal = mean(Signal, 2, 'omitnan');
            Unit   = this(indImage).Unit;
            
        otherwise
            [flag, Signal, Unit] = getValSignalIfAny(this(indImage), listSignals{index(k)}, 'suby', suby);
            if ~flag
                continue
            end
    end

    ySample = YSample('data', Signal, 'unit', Unit);
    signalList(end+1) = ClSignal('Name', listSignals{index(k)}, 'ySample', ySample, 'xSample', xSample); %#ok<AGROW>
end

style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
a = SignalDialog(signalList, 'Title', 'SignalDialog Image a Signals', 'imageDataList', imageData, 'titleStyle', style1, 'waitAnswer', false);
a.openDialog();
