function export_KleinSDF(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

%% Contr�les

ident_Reflectivity = cl_image.indDataType('Reflectivity');

% TODO : � compl�ter pour le subbottom

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'), 'DataType', ident_Reflectivity, ...
    'InitialFileFormat', 'KleinSDF');
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

%% Type de donn�es � sauver

str{1} = 'Height';
str{2} = 'Heading';
str{3} = 'FishLatitude';
str{4} = 'FishLongitude';
str{5} = 'Image';
str1 = 'Liste des choses qui peuvent �tre sauv�es.';
str2 = 'List of things that cab be saved.';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:4);
if ~flag
    return
end

%% Sauve Signals

a = cl_sdf('nomFic', nomFic);

if any(rep == 1) % Height
    setHeightInSamples(a, this.Sonar.Height(:));
    %     flag = save_signal(a, 'altitude', altitude);
    %     if ~flag
    %         return
    %     end
end

if any(rep == 2) % Heading
    flag = save_signal(a, 'heading', this.Sonar.Heading(:));
    if ~flag
        return
    end
end

if any(rep == 3) % FishLatitude
    flag = save_signal(a, 'fishLat',  this.Sonar.FishLatitude(:));
    if ~flag
        return
    end
end

if any(rep == 4) % FishLongitude
    flag = save_signal(a, 'fishLon',  this.Sonar.FishLongitude(:));
    if ~flag
        return
    end
end

%% Sauve image

if any(rep == 5) % Image
    'A faire'
    %     flag = save_signal(a, 'shipLon',  ShipLongitude);
    %     if ~flag
    % %         return
    %     end
end

% switch % Sonar ou subbottom
% case
if ~isequal(subx, 1:this.nbColumns) || ~isequal(suby, 1:this.nbRows)
    my_warndlg('Only the whole image can be flushed into the .all file', 1);
    return
end

%             nomFicSeabed = SimradSeabedNames(this.InitialFileName);
%             status = export_ermapper(this, nomFicSeabed.ReflectivityErs, 'Open', 'r+');
%             if ~status
%                 return
%             end
% end



% function messageImportant
% str = {};
% str{end+1} = 'Ce module de sauvegarde est quelque peu ambigu. Les donn�es des fichiers .all ';
% str{end+1} = 'sont en fait transf�r�s dans des fichiers matlab beaucoup plus rapides � lire. ';
% str{end+1} = 'Ces fichiers se trouvent dans le r�pertoire matlab au m�me niveau que vos ';
% str{end+1} = 'fichiers .all. L''op�ration de sauvegarge est r�alis�e DANS ces fichiers. Si ';
% str{end+1} = 'vous travaillez uniquement avec ce logiciel, �a ne change rien pour vous, tout ';
% str{end+1} = 'est transparent. PAR CONTRE, si vous voulez exploiter ce travail avec un logiciel ';
% str{end+1} = 'autre comme CaraibesR, Caris ou Neptune, il vous faut effectivement archiver vos ';
% str{end+1} = 'r�sultats dans les fichiers .all. Apr�s avoir sauv� vos fichiers dans le r�pertoire ';
% str{end+1} = 'matlab, le logiciel vous demande explicitement si vous voulez sauver les donn�es ';
% str{end+1} = 'dans le .all. Si vous r�pondez oui, il vous demande en plus si vous voulez utiliser ';
% str{end+1} = 'les masques "MasqueDetection" et "MasqueDepth". Vous pouvez en s�lectionner 0? 1 ou les 2 ';
% str{end+1} = 'Une sortie par cancel ne sauvegardera pas les donn�es dans le .all';
%
% % ---------------------------
% % Concatenation en une chaine
%
% s = '';
% for i=1:length(str)
%     s = [s str{i}];
% end
% my_warndlg(s, 1);
%
