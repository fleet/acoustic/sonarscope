function flag = export_MultiLayersErMapper(this, indImage, repExport)

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

[~, Selection] = unique(DataTypes, 'last');
[choix, flag] = my_listdlg(Lang('Layers � exporter', 'Select layers to export'), ...
    nomsLayers, 'InitialValue', Selection);
if ~flag
    return
end

indLayers = indLayers(choix);
for k=1:length(indLayers)
    options = paramsExportErMapper(this(indLayers(k)));
    ImageName = code_ImageName(this(indLayers(k)));
    nomFic = fullfile(repExport, [ImageName '.ers']);
    flag = export_ermapper(this(indLayers(k)), nomFic, 'options', options);
end
 