function export_MultiLayersErmapperSameFrame(this, indImage, repExport, subx, suby)

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

[~, Selection] = unique(DataTypes, 'last');
[choix, flag] = my_listdlg(Lang('Layers � exporter', 'Select layers to export'), ...
    nomsLayers, 'InitialValue', Selection);
if ~flag
    return
end

indLayers = indLayers(choix);
for k=1:length(indLayers)
    [XLim, YLim, subx1, suby1, subx2, suby2] = intersectionImages(this(indImage), subx, suby, this(indLayers(k))); %#ok<ASGLU>
    
    options = paramsExportErMapper(this(indLayers(k)));
    ImageName = code_ImageName(this(indLayers(k)));
    nomFic = fullfile(repExport, [ImageName '.ers']);
    I1 = extraction(this(indLayers(k)), 'subx', subx2, 'suby', suby2);
    flag = export_ermapper(I1, nomFic, 'options', options);
end
