function repExport = export_MultiLayersGifAnimated(this, indImage, repExport)

that = update_Name(this(indImage));
filtre = fullfile(repExport, [that.Name '.gif']);
[flag, nomFic] = my_uiputfile('*.gif', 'Give a file name', filtre);
if ~flag
    return
end
repExport = fileparts(nomFic);

[flag, indLayers] = listeLayersSynchronises(this, indImage);
if ~flag
    return
end

% S�lection des layers pour g�n�rer le film.
[flag, choix] = summary(this(indLayers), 'ExportResults', 0, ...
    'QueryMode', 'MultiSelect');
if ~flag
    % Bouton Cancel
    return
end
% On continue m�me si choix ou strOut est vide.

if ~flag
    str1 = 'Aucune image synchronis�e � l''image courante.';
    str2 = 'None image is synchronized to the current one.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

str1 = 'Voulez vous intercaler l''image courante entre chaque image ?';
str2 = 'Do you want to intercalate the current image between each image ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end

if rep == 1
    indLayers = [repmat(indImage, 1, length(choix)); indLayers(choix)];
    indLayers = indLayers(:);
else
    indLayers = [indImage indLayers(choix)];
end

x = get_x_inXLim(this(indLayers(1)));
y = get_y_inYLim(this(indLayers(1)));

map = [];
for i=1:length(indLayers)
    a = this(indLayers(i));
    switch a.ImageType
        case 1
             ColormapIndex(i) = a.ColormapIndex; %#ok<AGROW>
             map = a.Colormap;
        case 2
            [b, flag] = RGB2Indexed(a, 'map', map);
            if flag && isempty(map)
                map = b.Colormap;
                %             break
            end
            ColormapIndex(i) = 0; %#ok<AGROW>
    end
end
UniqueColormapIndex = unique(ColormapIndex);
if (length(UniqueColormapIndex) > 1) && any(UniqueColormapIndex == 2)
    map = [map; gray(size(map,1))];
    map = map(1:2:end,:);
end

I = zeros(length(y), length(x), 1, length(indLayers), 'uint8');
N = length(indLayers);
hw = create_waitbar(Lang('Cr�ation de l''image GIG annim�e', 'Creating animated GIF image'), 'N', N);
for i=1:N
    my_waitbar(i, length(indLayers), hw);
    a = this(indLayers(i));
    switch a.ImageType
        case 1 % Intensity
            [RGB, flag] = Intensity2RGB(a);
            if ~flag
                continue
            end
        case 2 % RGB
            RGB = a;
        case 3 % Indexee
            [RGB, flag] = Indexed2RGB(a);
            if ~flag
                continue
            end
        case 4 % Binaire
            % Pas fait'
    end
    
    [b, flag] = RGB2Indexed(RGB, 'map', map);
    if ~flag
        continue
    end
    val = get_pixels_xy(b, x, y);
    val = uint8(val);
    
    % TODO : � finir
    deltay = y(2) - y(1);
    if strcmp(where(b), 'On RAM')
        if deltay > 0
            val = flipud(val); % Valid�
        else
            % ???
        end
    else
        if deltay > 0
            % ???
        else
            % ???
        end
    end
    
    if isempty(map)
        map = b.Colormap;
    end
    %             figure; image(val); colormap(map)
    I(:,:,1,i) = val;
end
my_close(hw, 'MsgEnd')

imwrite(I, map, nomFic, 'gif', 'DelayTime', 1, 'LoopCount', Inf);
% if ~isempty(nomFic)
%     visuExterne(nomFic);
% end
