function flag = export_MultiLayersSurfer7(this, indImage, repExport)

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

[~, Selection] = unique(DataTypes, 'last');
[choix, flag] = my_listdlg(Lang('Layers � exporter', 'Select layers to export'), ...
    nomsLayers, 'InitialValue', Selection);
if ~flag
    return
end

indLayers = indLayers(choix);
for k=1:length(indLayers)
    ImageName = code_ImageName(this(indLayers(k)));
    nomFic = fullfile(repExport, [ImageName '.grd']);
    flag =  export_surfer(this(indLayers(k)), nomFic, 'Version', 7);
end
 