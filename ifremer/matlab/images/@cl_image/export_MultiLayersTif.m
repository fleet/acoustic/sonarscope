function repExport = export_MultiLayersTif(this, indImage, repExport, varargin)

[varargin, XLim] = getPropertyValue(varargin, 'XLim');
[varargin, YLim] = getPropertyValue(varargin, 'YLim'); %#ok<ASGLU>

that = update_Name(this(indImage));
filtre = fullfile(repExport, [that.Name '.tif']);
[flag, nomFic] = my_uiputfile({'*.tif'; '*.jpg'; '*.gif'}, 'Give a file name', filtre);
if ~flag
    return
end

NomGenerique = extract_ImageName(cl_image, 'Name', nomFic);
[nomDir, NomGenerique] = fileparts(NomGenerique);
[~, ~, ext] = fileparts(nomFic);

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage);
if ~flag
    return
end

[choix, flag] = my_listdlg(Lang('Layers suppl�mentaires� exporter', 'Other layers to export'), ...
    nomsLayers, 'InitialValue', 1:length(indLayers));
if ~flag
    return
end
indLayers = [indImage indLayers(choix)];

nomFicLayer = {};
for k=1:length(indLayers)
    that = update_Name(this(indLayers(k)), 'Name', NomGenerique);
    nomFicLayer{k} = fullfile(nomDir, [that.Name ext]); %#ok<AGROW>
end
for k=1:length(indLayers)
    disp(nomFicLayer{k})
end

N = length(indLayers);
hw = create_waitbar('Image exportation', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    exportRaster(this(indLayers(k)), 'nomFic', nomFicLayer{k}, 'Format', ext(2:end), ...
        'XLim', XLim, 'YLim', YLim, 'VisuExterne', 0)
end
my_close(hw, 'MsgEnd');

repExport = nomDir;
if ~isempty(nomFicLayer)
    visuExterne(nomFicLayer);
end
