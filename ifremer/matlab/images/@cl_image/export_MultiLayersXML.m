function repExport = export_MultiLayersXML(this, indImage, repExport, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, XLim] = getPropertyValue(varargin, 'XLim');
[varargin, YLim] = getPropertyValue(varargin, 'YLim'); %#ok<ASGLU>

nbColumns = this(indImage).nbColumns;
nbRows    = this(indImage).nbRows;

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

[choix, flag] = my_listdlg(Lang('Layers suppl�mentaires� exporter', 'Other layers to export'), ...
    nomsLayers, 'InitialValue', find(indLayers == indImage));
if ~flag
    return
end

[flag, nomDir] = my_uigetdir(repExport, Lang('Choisissez ou cr�ez un r�pertoire.', 'Select or create a directory.'));
if ~flag
    return
end

nomFicLayer = {};
for k=1:length(indLayers)
    that = update_Name(this(indLayers(k)));
    Titre = rmblank(that.Name);
    nomFicLayer{k} = fullfile(nomDir, [Titre '.xml']); %#ok<AGROW>
end
for k=1:length(indLayers)
    disp(nomFicLayer{k})
end

if isequal(subx, 1:nbColumns) && isequal(suby, 1:nbRows)
    XLim = [];
    YLim = [];
end
N = length(choix);
hw = create_waitbar('Image XML exportation', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    if isempty(XLim)
        that = this(indLayers(choix(k)));
    else
        that = extraction(this(indLayers(choix(k))), 'XLim', XLim, 'YLim', YLim);
    end
    flag = export_xml(that, nomFicLayer{choix(k)});
    if ~flag
        str1 = sprintf('%s n''a pas pu �tre export�.', nomFicLayer{choix(k)});
        str2 = sprintf('%s could not be exported.', nomFicLayer{choix(k)});
        my_warndlg(Lang(str1,str2), 1);
    end
end
repExport = nomDir;
% if ~isempty(nomFicLayer)
%     visuExterne(nomFicLayer);
% end
my_close(hw, 'MsgEnd')
