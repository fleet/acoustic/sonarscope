% Exportation de l'image dans NasaWorldWind
%
% Syntax
%   export_NasaWorldWind(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   nomFic   : Non du fichier de sortie (Une numerotation est rajoutee si decoupage par blocks)
%   nbLigMax : Nombre maximum de lignes par block (Inf par defaut)
%   nbColMax : Nombre maximum de colonnes par block (Inf par defaut)
%   CLim     : Limites du rehaussement de contraste si format d'image 8bits.
%              Si cette valeur n'est pas donnee, c'est le CLim de this qui est utilise
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   nomFic = tempname
%   export_NasaWorldWind(a, 'nomFic', nomFic)
%
% See also cl_image cl_image/set cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_NasaWorldWind(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nbLigMax]  = getPropertyValue(varargin, 'nbLigMax', []);
[varargin, nbColMax]  = getPropertyValue(varargin, 'nbColMax', []);
[varargin, nomFicXml] = getPropertyValue(varargin, 'nomFic',   [my_tempname '.xml']);
[varargin, deltaX]    = getPropertyValue(varargin, 'deltaX',   0);
[varargin, deltaY]    = getPropertyValue(varargin, 'deltaY',   0); %#ok<ASGLU>

if length(this) ~= 1
    my_warndlg('cl_image:export_NasaWorldWind Une image a la fois svp', 0);
    return
end

if isempty(this.Image)
    my_warndlg('cl_image:export_NasaWorldWind L''image est vide', 0);
    return
end

[nomDir, nomFic] = fileparts(nomFicXml);%#ok

if isempty(nbColMax)
    nbColMax = length(subx);
end
nbBlkCol = ceil(length(subx) / nbColMax);

if isempty(nbLigMax)
    nbLigMax = length(suby);
end
nbBlkLig = ceil(length(suby) / nbLigMax);

% --------------------------------------------------------
% Creation du fr�pertoire C:\Program Files\NASA\World Wind
% 1.3\Data\Earth\SonarScope si il n'existe pas

str = sprintf('Creating file %s\nTake care the NasaWorldWind version must be the one specified in the file name.', nomFicXml);
my_warndlg(str, 1);
flag = createRepData(nomFicXml);
if ~flag
    return
end

% --------------------------------
% Ecritude du d�but du fichier xml

[nomDirXml, nomFicSimple] = fileparts(nomFicXml);
fid = fopen(nomFicXml, 'w+');
if fid == -1
    str = sprintf('Impossible to create file %s', nomFicXml);
    my_warndlg(str, 1);
    flag = 0;
    return
end
[nomDir, Nom, ext] = fileparts(nomFicXml);%#ok
fprintf(fid, '<LayerSet Name="%s" ShowOnlyOneLayer="false" ShowAtStartup="false">\n', nomFicSimple);

nomDirXml = fileparts(nomDirXml);
nomDirXml = fileparts(nomDirXml);


Recouvrement = 1;
% nbLigMax = nbLigMax - Recouvrement;
% nbColMax = nbColMax - Recouvrement;
hw = create_waitbar('Nasa-World-Wind Exportation', 'N', nbBlkLig);
for j=1:nbBlkLig
    subyBlock = (j-1)* nbLigMax + (1-Recouvrement:nbLigMax);
    subyBlock(subyBlock < 1) = [];
    subyBlock(subyBlock > length(suby)) = [];
    subyBlock = suby(subyBlock);
    if subyBlock(end) == suby(end)
        subyBlock = suby(end-nbLigMax+1:end);
    end
    
    for i=1:nbBlkCol
        my_waitbar((i+(j-1)*nbBlkCol), (nbBlkLig*nbBlkCol), hw);
        
        subxBlock = (i-1)* nbColMax + (1-Recouvrement:nbColMax);
        subxBlock(subxBlock > length(subx)) = [];
        subxBlock(subxBlock < 1) = [];
        subxBlock = subx(subxBlock);
        
        if subxBlock(end) == subx(end)
            subxBlock = subx(end-nbColMax+1:end);
        end
        
        if isnan(this.ValNaN)
            I = ~isnan(this.Image(subyBlock,subxBlock,:));
        else
            I = (this.Image(subyBlock,subxBlock,:) ~= this.ValNaN);
        end
        if size(I,3) ~= 1
            I = sum(I,3);
        end
        sumV = find(sum(I,1) ~= 0);
        sumH = find(sum(I,2) ~= 0);
        
        if isempty(sumV) || isempty(sumH)
            continue
        end
        
        if (nbBlkLig == 1) && (nbBlkCol == 1)
            nomFicTile = nomFicSimple;
        else
            nomFicTile = sprintf('%s_%02d_%02d', nomFicSimple, j, i);
        end
        nomFicGifRelatif = fullfile('Data', 'Earth', 'SonarScope', nomFicSimple, [nomFicTile, '.gif']);
        nomFicGif = fullfile(nomDirXml, nomFicGifRelatif);
        
        subxImagette = subxBlock(sumV(1):sumV(end));
        subyImagette = subyBlock(sumH(1):sumH(end));
        flag = export_gif(this, nomFicGif, 'subx', subxImagette, 'suby', subyImagette);
        if ~flag
            str = sprintf('Impossible to create file %s', nomFicGif);
            my_warndlg(str, 1);
            fclose(fid);
            delete(nomFicXml)
            return
        end
        North = max(this.y(subyBlock(sumH)));
        South = min(this.y(subyBlock(sumH)));
        East  = max(this.x(subxBlock(sumV)));
        West  = min(this.x(subxBlock(sumV)));
        
        [North, South, East, West] = decalageEsoteric(deltaX, deltaY, North, South, East, West);
        
        flag = write_XML(fid, nomFicGifRelatif, North, South, East, West);
        if ~flag
            str = sprintf('Impossible to write XML info for file %s', nomFicGif);
            my_warndlg(str, 1);
            fclose(fid);
            delete(nomFicXml)
            return
        end
    end
end
my_close(hw)

% ---------------------------------
% Ecritude de la fin du fichier xml

fprintf(fid, '</LayerSet>\n');
fclose(fid);

% ------------------------------------------
function flag = write_XML(fid, nomFicGif, North, South, East, West)

[nomDir, Nom] = fileparts(nomFicGif);

fprintf(fid, '<ImageLayer ShowAtStartup="true">\n');
fprintf(fid, '<Name>%s</Name>\n', Nom);
fprintf(fid, '<DistanceAboveSurface>0</DistanceAboveSurface>\n');
fprintf(fid, '  <BoundingBox>\n');
fprintf(fid, '    <North><Value>%19.14f</Value></North>\n', North);
fprintf(fid, '    <South><Value>%19.14f</Value></South>\n', South);
fprintf(fid, '    <West><Value>%19.14f</Value></West>\n',   West);
fprintf(fid, '    <East><Value>%19.14f</Value></East>\n',   East);
fprintf(fid, '  </BoundingBox>\n');
fprintf(fid, '  <TexturePath>%s</TexturePath>\n', nomFicGif);
fprintf(fid, '<Opacity>255</Opacity>\n');
fprintf(fid, '<TerrainMapped>false</TerrainMapped>\n');
fprintf(fid, '</ImageLayer>\n');

flag = 1;


function flag = createRepData(nomFicXml)
[nomDirXml, nomFicSimple] = fileparts(nomFicXml);
nomDirXml = fileparts(nomDirXml);
nomDirXml = fileparts(nomDirXml);
nomDir = fullfile(nomDirXml, 'Data', 'Earth', 'SonarScope', nomFicSimple);
if exist(nomDir, 'dir') ~= 7
    nomDir = fullfile(nomDirXml, 'Data', 'Earth', 'SonarScope');
    if ~exist(nomDir, 'dir')
        [flag, msg] = mkdir(nomDir);
        if ~flag
            my_warndlg(msg, 1);
            return
        end
    end
    nomDir = fullfile(nomDirXml, 'Data', 'Earth', 'SonarScope', nomFicSimple);
    [flag, msg] = mkdir(nomDir);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
else
    flag = 1;
end


function [North, South, East, West] = decalageEsoteric(deltaX, deltaY, North, South, East, West)

a = cl_carto('Ellipsoide.Type', 11);
R = get(a, 'Ellipsoide.DemiGrandAxe');

offsetLat = (deltaY / R) * (180/pi);
offsetLon = (deltaX / R) * (180/pi);%  * cosd((North+South)/2);

North = North + offsetLat;
South = South + offsetLat;
East  = East + offsetLon;
West  = West + offsetLon;
