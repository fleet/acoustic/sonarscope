% TODO : ceci est un premier jet. Il y a de multiples am�liorations � faire
% : interpolation des latitudes et longitude, masquage de la donn�e apr�s
% la d�tection de hauteur, d�calage angulaire, etc ...

function flag = export_RawEchogram2SSc3DV(this, nomFicXml, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

TimePing = this.Sonar.SampleBeamData.Time;

if isfield(this.Sonar.SampleBeamData, 'TransducerDepth')
    TransducerDepth = this.Sonar.SampleBeamData.TransducerDepth;
    TransducerDepth = TransducerDepth(1); % TODO
else
    TransducerDepth = 0;
end

[~, name] = fileparts(nomFicXml);
Info.name = name;

Colormap = this.Colormap;
CLim = this.CLim;
I = get_ImageInd(this, subx, suby, Colormap, CLim);

Info.from = 'Unkown';
Info.Date = TimePing.date;
Info.Hour = TimePing.heure;
% Info.PingNumber      = this.Sonar.PingNumber(suby);
[nbSamples, nbBeams] = size(I); %#ok<ASGLU>

% if isfield(this.Sonar.SampleBeamData, 'Roll')
%     Roll = this.Sonar.SampleBeamData.Roll;
% else
%     Roll = 0;
% end
[flag, lat0, lon0, LatitudeBottom, LongitudeBottom, DepthBottom, TxBeamIndex] = calcul_positionRawDataWC(this);
if ~flag
    return
end
% if isnan(LongitudeBottom(1))
%     %     return
% end

if ~isdeployed
    figure; plot(LongitudeBottom, LatitudeBottom, '-*'); grid
end

Info.PingNumber      = 1;
Info.Tide            = 0;

Info.LatitudeTop     = lat0 + zeros(1, nbBeams);
Info.LongitudeTop    = lon0 + zeros(1, nbBeams);

Info.LatitudeBottom  = LatitudeBottom;
Info.LongitudeBottom = LongitudeBottom;
Info.DepthTop        = -TransducerDepth + zeros(1, nbBeams);
Info.DepthBottom     = -TransducerDepth + DepthBottom;
Info.ImageSegment    = TxBeamIndex;

I(I< 2) = 2;
subNaN = isnan(DepthBottom);
I(:,subNaN) = 0;

flag = create_XMLBin_ImagesAlongNavigation(I', nomFicXml, Colormap, Info);

%% C'est fini

message(flag, nomFicXml)



function message(flag, nomFicXml)
if flag
    str1 = sprintf('La cr�ation du fichier "%s" est termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" is over.', nomFicXml);
else
    str1 = sprintf('La cr�ation du fichier "%s" ne s''est pas bien termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" failed.', nomFicXml);
end
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProcessingWCIsOver');



function I = get_ImageInd(this, subx, suby, Colormap, CLim)

I = this.Image(suby,subx);
subNaN = isnan(I);

if this.Video == 2
    Colormap = flipud(Colormap);
end

I = gray2ind(mat2gray(I, double(CLim)), size(Colormap,1));
I(I == 0) = 1;
I(subNaN) = 0;
