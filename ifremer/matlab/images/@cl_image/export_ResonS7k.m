function export_ResonS7k(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

ident(1) = cl_image.indDataType('Bathymetry');
ident(2) = cl_image.indDataType('RayPathSampleNb');
ident(3) = cl_image.indDataType('DetectionType');
ident(4) = cl_image.indDataType('Reflectivity');

if this.GeometryType == cl_image.indGeometryType('PingBeam')
    flag = testSignature(this, 'DataType', ident, 'GeometryType', cl_image.indGeometryType('PingBeam'));
elseif this.GeometryType == cl_image.indGeometryType('PingSamples')
    % Imagerie 3=Reflectivity et 4=SonarD
    flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', cl_image.indGeometryType('PingSamples'));
end
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
identSondeur = selectionSondeur(s7k);
write_signaux(s7k, identSondeur, 'Heading',      this.Sonar.Heading(:))
write_signaux(s7k, identSondeur, 'SoundSpeed',   this.Sonar.SurfaceSoundSpeed(:))
write_signaux(s7k, identSondeur, 'Latitude',     this.Sonar.FishLatitude(:))
write_signaux(s7k, identSondeur, 'Longitude',    this.Sonar.FishLongitude(:))
% write_signaux(s7k, 'Tide',         this.Sonar.Tide) % Commente car Tide =0 dans this

% Comment� par JMA le 06/04/2016 pour donn�es 7150
% write_signaux(s7k, identSondeur, 'VehicleDepth', -this.Sonar.Height(:))

% Data.PingCounter
% Data.TransducerDepth
% Data.NbBeams
% Data.Frequency
% Data.HeightSource
% Data.Roll
% Data.Pitch
% Data.Heave

str{1} = 'Only the deduced mask (Possibility to reset the mask)';
str{2} = 'The layer itself (No possibility of restoration)';
[rep, flag] = my_listdlg('Liste of items', str, 'SelectionMode', 'Single');
if ~flag
    return
end
if rep == 1
    [flag, Data] = read_depth_s7k(s7k, 'identSondeur', identSondeur);
    if ~flag
        return
    end
    X = ~isnan(this.Image(suby, subx));
    X = uint8(X);
    X(X == 1) = 231; % Modif JMA le 25/01/2021
    Data.Mask(suby, subx) = X;
    write_depthOnRawFile(s7k, identSondeur, 'Mask', Data.Mask);
    return
end

switch this.DataType
    case ident(1)   % Bathymetry
        [flag, Data] = read_depth_s7k(s7k, 'identSondeur', identSondeur);
        if ~flag
            return
        end
        Data.Depth(suby, subx) = this.Image(suby, subx);
        write_depthOnRawFile(s7k, identSondeur, 'Depth', Data.Depth)
        
    case ident(2)   % RayPathSampleNb
        [flag, Data] = read_depth_s7k(s7k, 'identSondeur', identSondeur);
        if ~flag
            return
        end
        Data.Range(suby, subx) = this.Image(suby, subx);
        write_depthOnRawFile(s7k, identSondeur, 'Range', Data.Range)
                
    case ident(3) % DetectionType
        [flag, Data] = read_depth_s7k(s7k, 'identSondeur', identSondeur);
        if ~flag
            return
        end
        Mask = ones(length(suby), length(subx), 'single');
        Mask(isnan(this.Image(suby, subx))) = NaN;
        %         Mask(this.Image(suby, subx) == 0) = NaN;
        Data.Depth(suby, subx) = Data.Depth(suby, subx) .* Mask;
        write_depthOnRawFile(s7k, identSondeur, 'Depth', Data.Depth)
        
    case ident(4)  % Reflectivity
        if ~isequal(suby, 1:this.nbRows)
            my_warndlg('Only the whole image can be flushed into the .s7k file', 1);
            return
        end
        [nomDir, nomFic] = fileparts(this.InitialFileName);
        nomFicReflectivity  = fullfile(nomDir, 'SonarScope', [nomFic '_seabedImage_reflectivity']);
        status = export_ermapper(this,nomFicReflectivity);
        
        %         [CodeCodage, CellType, ValNaN] = export_ermapper_init(this);
        %         nomFicReflectivity  = fullfile(nomDir, 'SonarScope', [nomFic '_seabedImage_reflectivity']);
        %         status = export_ermapper_image(this, nomFicReflectivity, CodeCodage, ValNaN);
        if ~status
            return
        end
        
    otherwise
        str = sprintf('Cas pas encore prevu dans export_ResonS7k');
        my_warndlg(str, 1);
        return
end
