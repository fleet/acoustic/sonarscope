function export_SAR(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

%% Contr�les

ident_Reflectivity = cl_image.indDataType('Reflectivity');

% TODO : � compl�ter pour le subbottom

indGeometryTypes = [cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist')];
flag = testSignature(this, 'GeometryType', indGeometryTypes, 'DataType', ident_Reflectivity, ...
    'InitialFileFormat', 'SonarSAR');
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

%% Type de donn�es � sauver

str = [];
str{1}     = 'Time';
str{end+1} = 'Immersion';
str{end+1} = 'Height';
str{end+1} = 'Speed';
str{end+1} = 'Heading';
str{end+1} = 'FishLatitude';
str{end+1} = 'FishLongitude';
% str{end+1} = 'Roll';
% str{end+1} = 'Pitch';

if this.DataType == cl_image.indGeometryType('PingRange')
    str{end+1} = 'Image';
end

str1 = 'Liste des choses qui peuvent �tre sauv�es.';
str2 = 'List of things that cab be saved.';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', [3 5 6 7]);
if ~flag
    return
end

%% Sauve Signals

for k=1:length(rep)
    SignalName = str{rep(k)};
    SignalValues = this.Sonar.(SignalName);
    switch SignalName
        case 'Time'
            Time = SignalValues.timeMat;
            flag = save_signal(nomFic, 'Ssc_Sar', 'Time', Time);
            if ~flag
                return
            end
            
        case 'FishLatitude'
            flag = save_signal(nomFic, 'Ssc_Sar', 'Latitude', SignalValues);
            if ~flag
                return
            end
            
        case 'FishLongitude'
            flag = save_signal(nomFic, 'Ssc_Sar', 'Longitude', SignalValues);
            if ~flag
                return
            end
            
        case 'Heading'
            flag = save_signal(nomFic, 'Ssc_Sar', 'Heading', SignalValues);
            if ~flag
                return
            end
            
        case 'Height'
            flag = save_signal(nomFic, 'Ssc_Sar', 'Height', SignalValues);
            if ~flag
                return
            end
            
        case 'Immersion'
            flag = save_signal(nomFic, 'Ssc_Sar', 'Immersion', SignalValues);
            if ~flag
                return
            end
            
        case 'Speed'
            flag = save_signal(nomFic, 'Ssc_Sar', 'Speed', SignalValues);
            if ~flag
                return
            end
            
            
            %         case 'Image'
            %             'TODO'
            
        otherwise
            str1 = sprintf('Signal "%s" non trouv� dansla fonction export_SAR.', SignalName);
            str2 = sprintf('"%s" not programmed in function export_SAR.', SignalName);
            my_warndlg(Lang(str1,str2), 1);
    end
end

return

% switch % Sonar ou subbottom
% case
if ~isequal(subx, 1:this.nbColumns) || ~isequal(suby, 1:this.nbRows)
    my_warndlg('Only the whole image can be flushed into the .all file', 1);
    return
end

%             nomFicSeabed = SimradSeabedNames(this.InitialFileName);
%             status = export_ermapper(this, nomFicSeabed.ReflectivityErs, 'Open', 'r+');
%             if ~status
%                 return
%             end
% end


% function messageImportant
% str = {};
% str{end+1} = 'Ce module de sauvegarde est quelque peu ambigu. Les donn�es des fichiers .all ';
% str{end+1} = 'sont en fait transf�r�s dans des fichiers matlab beaucoup plus rapides � lire. ';
% str{end+1} = 'Ces fichiers se trouvent dans le r�pertoire matlab au m�me niveau que vos ';
% str{end+1} = 'fichiers .all. L''op�ration de sauvegarge est r�alis�e DANS ces fichiers. Si ';
% str{end+1} = 'vous travaillez uniquement avec ce logiciel, �a ne change rien pour vous, tout ';
% str{end+1} = 'est transparent. PAR CONTRE, si vous voulez exploiter ce travail avec un logiciel ';
% str{end+1} = 'autre comme CaraibesR, Caris ou Neptune, il vous faut effectivement archiver vos ';
% str{end+1} = 'r�sultats dans les fichiers .all. Apr�s avoir sauv� vos fichiers dans le r�pertoire ';
% str{end+1} = 'matlab, le logiciel vous demande explicitement si vous voulez sauver les donn�es ';
% str{end+1} = 'dans le .all. Si vous r�pondez oui, il vous demande en plus si vous voulez utiliser ';
% str{end+1} = 'les masques "MasqueDetection" et "MasqueDepth". Vous pouvez en s�lectionner 0? 1 ou les 2 ';
% str{end+1} = 'Une sortie par cancel ne sauvegardera pas les donn�es dans le .all';
%
% % ---------------------------
% % Concatenation en une chaine
%
% s = '';
% for i=1:length(str)
%     s = [s str{i}];
% end
% my_warndlg(s, 1);
%
