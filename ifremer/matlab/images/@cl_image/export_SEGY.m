function export_SEGY(this, varargin)

% [subx, suby, varargin] = getSubxSuby(this, varargin);
[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

%% Contr�les

ident_Reflectivity = cl_image.indDataType('Reflectivity');

% TODO : � compl�ter pour le subbottom

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'), 'DataType', ident_Reflectivity, ...
    'InitialFileFormat', 'SEGY');
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

%% Type de donn�es � sauver

str{1} = 'Height';
str{2} = 'Latitude & Longitude';
str{3} = 'Time';
% str{2} = 'Heading';
% str{5} = 'Image';
str1 = 'Liste des choses qui peuvent �tre sauv�es.';
str2 = 'List of things that can be saved.';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1);
if ~flag
    return
end

%% Cr�ation d'une instance juste pour s'assurer que �a existe

a = cl_segy('nomFic', nomFic);

%% Sauve Signals

if any(rep == 1) % Height
    X = this.Sonar.Height(:,1);
    flag = save_Height(a, X);
    if ~flag
        return
    end
end

if any(rep == 2) % FishLatitude
    DataPosition.Latitude  = this.Sonar.FishLatitude(:,1);
    DataPosition.Longitude = this.Sonar.FishLongitude(:,1);
    flag = write_position(a, DataPosition);
    if ~flag
        return
    end
end

if any(rep == 3) % Time
    X = this.Sonar.Time;
    %     flag = save_Time(a, X);
    flag = set_time(a, X);
    if ~flag
        return
    end
end

% str1 = 'Seuleument la hauteur est sauv�e. Si vous avez besoin de plus merci de me le faire savoir. JMA';
% str2 = 'Only the "Height" signal is saved. If you need more please send me an email. JMA';
% my_warndlg(Lang(str1,str2), 1);
