function export_SimradAll(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

ident_Bathymetry      = cl_image.indDataType('Bathymetry');
ident_RayPathSampleNb = cl_image.indDataType('RayPathSampleNb');
ident_Mask            = cl_image.indDataType('Mask');
ident_Reflectivity    = cl_image.indDataType('Reflectivity');
ident_DetectionType   = cl_image.indDataType('DetectionType');

if this.GeometryType == cl_image.indGeometryType('PingBeam')
    flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingBeam'));
elseif this.GeometryType == cl_image.indGeometryType('PingSamples')
    % Imagerie 3=Reflectivity et 4=SonarD
    flag = testSignature(this, 'DataType', [cl_image.indDataType('Reflectivity')
        cl_image.indDataType('RxBeamIndex')], ...
        'GeometryType', cl_image.indGeometryType('PingSamples'));
end
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

aKM = cl_simrad_all('nomFic', nomFic);

if this.GeometryType == cl_image.indGeometryType('PingBeam')
    switch this.DataType
        case ident_Bathymetry   % Bathymetry
            [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 0); % ATTENTION : mettre absolument Memmapfile=O ici sinon c'est la m....
            subxData = subx;
            [subyData, subThis] = intersect3(DataDepth.PingCounter(:,1), ...
                this.Sonar.PingCounter(suby,1), this.Sonar.PingCounter(suby,1));
            suby = suby(subThis); % Rajout� le 06/06/2010
            
            Image = this.Image(suby, subxData);
            Depth = DataDepth.Depth(subyData, subxData);
            Mask  = DataDepth.Mask(subyData, subxData);
            
            subNaN = isnan(Image) | isnan(Depth);
            Mask(subNaN) = 1;
            
            TransducerDepth = get(this, 'SonarImmersion');
            Heave           = get(this, 'Heave');
            TideValue       = get(this, 'Tide');
            TransducerDepth = TransducerDepth(suby);
            Heave           = Heave(suby);
            for k=1:length(suby)
                %                 iPing = subyData(k);
                
                %% TODO : Attention, ici, on atteint les limites de la chose. Il faut garder en m�moire dans l'image
                %% si la correction de mar�e a �t� faite, idem pour le Draught et Heave/TrueHeave
                
                if isempty(TideValue)
                    Image(k,:) = Image(k,:) - (TransducerDepth(k) - Heave(k));
                else
                    TideValue = TideValue(suby);
                    Image(k,:) = Image(k,:) - (TransducerDepth(k) - Heave(k) - TideValue(k));
                end
            end
            %                 DataDepth.Depth(subyData, subxData) = Image;
            DataDepth.Mask(subyData, subxData) = Mask;
            
            flag = write_depth_mask(aKM, DataDepth);
%             flag = write_depth_signals(aKM, DataDepth); % En test

            
        case ident_Reflectivity
            [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 0); % ATTENTION : mettre absolument Memmapfile=O ici sinon c'est la m....
            if ~flag
                return
            end
            subxData = subx;
            subyData = intersect3(DataDepth.PingCounter(:,1), ...
                this.Sonar.PingCounter(suby,1), this.Sonar.PingCounter(suby,1));
            
            Image  = this.Image(suby, subxData);
            Reflec = DataDepth.ReflectivityFromSnippets(subyData, subxData);
            Mask   = DataDepth.Mask(subyData, subxData);
            
            subNaN = isnan(Image) | isnan(Reflec);
            
            % Moyen tr�s maladroit pour d�tecter si on est sur Depth ou
            % Depth-Heave, ...
            Mask(subNaN) = 1;
            
            %             DataDepth.ReflectivityFromSnippets(subyData, subxData) = Image;
            DataDepth.Mask(subyData, subxData) = Mask;
            
            flag = write_depth_bin(aKM, DataDepth);
            
            InitialValue   = 2;
            
        case ident_RayPathSampleNb   % RayPathSampleNb
            if getUseNewFormatsAll
                my_warndlg('If you need the exportation for RayPathSampleNb for New SonarScope Internal Formats please ask to sonarscope@ifremer.fr to write it. ', 1);
                return
            else
                [flag, DataDepth] = read_depth_bin(aKM);
                subxData = subx;
                subyData = intersect3(DataDepth.PingCounter(:,1), ...
                    this.Sonar.PingCounter(suby,1), this.Sonar.PingCounter(suby,1));
                
                DataDepth.Range(subyData, subxData) = this.Image(subyData, subxData);
                
                % -----------------------------------------------------------------
                % Pour l'instant on ne sauve pas la donn�e car on ne sait pas si le
                % layer est la hauteur d'eau sous la quille o� si cest H+Tirant
                % d'eau - Heave. Ca fonctionnerait uniquement si on chargeait
                % "Depth" et non pas "Depth - Heave"
                
                %         write_depthOnRawFile(aKM, 'Range', DataDepth.Range)
                % -----------------------------------------------------------------
                
                Masque = read_masque(aKM);
                Masque.MasqueDepth(subyData, subxData) = Masque.MasqueDepth(subyData, subxData) & ...
                    ~isnan(this.Image(subyData, subxData));
                write_masque(aKM, 'MasqueDepth', Masque.MasqueDepth)
            end
            
        case ident_Mask
            Data = read_masque(aKM);
            subxData = subx;
            subyData = intersect3(1:length(Data.MasqueDepth), ...
                this.Sonar.PingCounter(suby,1), this.Sonar.PingCounter(suby,1));
            
            Data.MasqueDetection(subyData, subxData) = this.Image(subyData, subxData);
            write_masque(aKM, 'MasqueDetection', Data.MasqueDetection)
            
        case ident_DetectionType
            [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 0); % ATTENTION : mettre absolument Memmapfile=O ici sinon c'est la m....
            subxData = subx;
            [subyData, subThis] = intersect3(DataDepth.PingCounter(:,1), this.Sonar.PingCounter(suby,1), this.Sonar.PingCounter(suby,1));
            suby = suby(subThis); % Rajout� le 06/06/2010
            
            Image = this.Image(suby, subxData);
            Depth = DataDepth.Depth(subyData, subxData);
            Mask  = DataDepth.Mask(subyData, subxData);
            
            subNaN = isnan(Image) | isnan(Depth);
            Mask(subNaN) = 1;
            DataDepth.Mask(subyData, subxData) = Mask;
            flag = write_depth_mask(aKM, DataDepth);
            
            %{
% Version qui fonctionne mais �a modifie uniquement le masque  particulier � la bathym�trie
[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 0); % ATTENTION : mettre absolument Memmapfile=O ici sinon c'est la m....
MaskDetection = isnan(this.Image(suby, subx));
DataDepth.Mask(suby, subx) = (DataDepth.Mask(suby, subx) | MaskDetection);
flag = write_depth_mask(aKM, DataDepth);
            %}
            %{
Data = read_masque(aKM);
subxData = subx;
%             subyData = intersect3(1:length(Data.MasqueDepth), ...
%                 this.Sonar.PingCounter(suby,1), this.Sonar.PingCounter(suby,1));
subyData = suby; % TODO : g�rer cela correctement SVP

MaskDetection = ~isnan(this.Image(suby, subx));
Data.MasqueDetection(subyData, subxData) = Data.MasqueDetection(subyData, subxData)  & MaskDetection;
write_masque(aKM, 'MasqueDetection', Data.MasqueDetection)

% Rajout� par JMA le 23/09/2015
Data.MasqueDepth(subyData, subxData) = Data.MasqueDepth(subyData, subxData) & (MaskDetection == 1);
write_masque(aKM, 'MasqueDepth', Data.MasqueDepth)
            %}
            
        otherwise % On r�cup�re le masque
            %             str1 = 'Cas pas encore pr�vu dans la fonction export_SimradAll. Envoyez ce message � sonarscope@ifremer.fr.';
            %             str2 = 'Case not written in export_SimradAll function yet. Report to sonarscope@ifremer.fr.';
            %             my_warndlg(Lang(str1,str2), 1);
            %             return
            [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 0); % ATTENTION : mettre absolument Memmapfile=O ici sinon c'est la m....
            if flag
                MaskDetection = isnan(this.Image(suby, subx));
                DataDepth.Mask(suby, subx) = (DataDepth.Mask(suby, subx) | MaskDetection);
                flag = write_depth_mask(aKM, DataDepth);
            end
    end
    
elseif this.GeometryType == cl_image.indGeometryType('PingSamples')
    switch this.DataType
        case ident_Reflectivity
            if ~isequal(suby, 1:this.nbRows)
                my_warndlg('Only the whole image can be flushed into the .all file', 1);
                return
            end
            
            nomFicSeabed = SimradSeabedNames(this.InitialFileName);
            status = export_ermapper(this, nomFicSeabed.ReflectivityErs, 'Open', 'r+');
            if ~status
                return
            end
            
        case cl_image.indDataType('RxBeamIndex')
            if ~isequal(suby, 1:this.nbRows)
                my_warndlg('Only the whole image can be flushed into the .all file', 1);
                return
            end
            
            nomFicSeabed = SimradSeabedNames(this.InitialFileName);
            status = export_ermapper(this, nomFicSeabed.BeamsErs, 'Open', 'r+');
            if ~status
                return
            end
            
        otherwise
            str1 = 'Cas pas encore pr�vu dans la fonction export_SimradAll. Envoyez ce message � sonarscope@ifremer.fr.';
            str2 = 'Case not written in export_SimradAll function yet. Report to sonarscope@ifremer.fr.';
            my_warndlg(Lang(str1,str2), 1);
            return
    end
end

%% Export des donn�es d'attitude

if isfield(this.Sonar, 'Tide') && ~isempty(this.Sonar.Tide)
    flag = save_attitude(aKM, 'Tide', this.Sonar.Time, this.Sonar.Tide);
    if ~flag
        return
    end
end
