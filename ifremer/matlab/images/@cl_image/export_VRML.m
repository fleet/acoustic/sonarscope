% Ecriture d'un fichier VRML selon diff�rentes g�om�trie.
%
% Syntax
%   flag = export_VRML_PointSet(this, varargin)
%
% Input Arguments
%   this     : Instance de cl_image
%   Geometry : G�om�trie YX, LatLong ou PingBeam.
%   subx     : sous-ensemble de coordonn�es en X.
%   suby     : sous-ensemble de coordonn�es en Y.
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%
% See also cl_image Authors
% Authors : GLT, GLU
% -------------------------------------------------------------------------

function [flag, repExport] = export_VRML(this, indImage, GeometryType, subx, suby, repExport)

switch GeometryType
    case cl_image.indGeometryType('GeoYX')
        ImageName = extract_ImageName(this(indImage));

        [flag, nomFic, Az, El, vertExag, modeGrid] = paramVRML(ImageName);
        if ~flag
            return
        end
        
        if strcmp(modeGrid, 'Points')
            [flag, indlayerDrapage] = paramsDrapageFledermaus(this, indImage); % r�utilisation de cette fonction pour drapage d'image associ�e
            if ~flag
                return
            end
            % Ce mode d'export a �t� adapt� pour sortir des nuages de
            % points mais mieux vaut utiliser l'autre proc�dure
            flag = export_VRML_Elevation(this(indImage), ...
                'filename', nomFic, 'subx', subx, 'suby', suby, 'Az', Az, 'El', El, 'vertExag', vertExag, ...
                'textureMapping', this(indlayerDrapage), 'exportGridMode', modeGrid); %#ok<NASGU>
            % TODO : ca ne semble pas fonctionner : GLU : � voir en m�me
            % temps que regroupement des fonctions
        else
            str1 = 'Pas encore fait.';
            str2 = 'Not yet done.';
            my_warndlg(Lang(str1,str2), 1);
            %{
            % Comment� par JMA le 13/06/2013 car la fonction
            % "export_VRML_PointSet" tente de calculer des coordonn�es
            % g�ographiques comme si l'image �tait une image sonar en
            % PingBeam !!!
            flagRepere = 1; % On force pour l'instant;
            flag = export_VRML_PointSet(this, indImage, ...
                GeometryType, 'filename', nomFic, 'subx', subx, 'suby', suby, ...
                'flagRepere', flagRepere, 'Az', Az, 'El', El, 'vertExag', vertExag, ...
                'radius', radius, 'DisplayAxis', DisplayAxis); %#ok<NASGU>
            %}
        end
        
    case cl_image.indGeometryType('PingBeam') % G�om�trie Pour extraction d'�pave.
        filtre = fullfile(repExport, 'filename.wrl');
        [flag, nomFic] = my_uiputfile({'*.wrl'}, Lang('Nom du fichier', 'Give a file name'), filtre);
        if ~flag
            return
        end
        repExport = fileparts(nomFic);
        
        [flag, flagRepere, modeExport] = paramVRML2;
        if strcmp(modeExport, 'PointSet')
            if ~flag
                return
            end
            
            flag = export_VRML_PointSet(this, indImage, ...
                GeometryType, 'filename', nomFic, 'subx', subx, 'suby', suby, ...
                'flagRepere', flagRepere); %#ok<NASGU>
        else % Export en FaceSet
            if ~flag
                return
            end
            
            % Ensemble Points non calcul� encore.
            flag = export_VRML_FaceSet(this, indImage, [], ...
                nomFic, 'subx', subx, 'suby', suby, ...
                'flagRepere', flagRepere); %#ok<NASGU>
            
        end
        % Ouverture par Navigateur + Cortona
        flag = export_VRML_OpenFile(nomFic); %#ok<NASGU>
            
    otherwise
        str1 = 'Seules les g�om�tries "GeoYX" peuvent �tre repr�sent�es en VRML -> Projetez vos donn�es.';
        str2 = 'Only "GeoYX" images can be exported to VRML -> Project your data.';
        my_warndlg(Lang(str1,str2), 1);
end
flag = 1;
