% Ecriture d'un fichier VRML
%
% Syntax
%   flag = export_VRML_PointSet(this, varargin)
%
% Input Arguments
%   this        : Instance de cl_image
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%
% See also cl_image Authors
% Authors : GLT, GLU
%--------------------------------------------------------------------------

function flag = export_VRML_PointSet(Images, indLayer, Geometry, varargin)

[subx, suby, varargin] = getSubxSuby(Images(indLayer), varargin);

[varargin, filename] = getPropertyValue(varargin, 'filename', 'filename.wrl');

flag = 0; %#ok<NASGU>

%% on des coordonnées absolues des points (soit en Ping Beam soit en GeoX

if Geometry == cl_image.indGeometryType('GeoYX')
    [flag, Points] = sonar_exportSoundingsToVRML(Images, indLayer, subx, suby); %#ok<ASGLU>
    flag           = export_VRML_PointSet_GeoYX(Images, indLayer, Points, filename, varargin{:});
else
    Points.XCoor = Images(indLayer).x(subx);
    Points.YCoor = Images(indLayer).y(suby);
    Points.Z     = Images(indLayer).Image( suby, subx)';
    flag         = export_VRML_PointSet_GeoPingBeam(Images, indLayer, Points, filename, varargin{:});
end
