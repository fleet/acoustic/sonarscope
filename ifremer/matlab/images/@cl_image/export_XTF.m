function export_XTF(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

%% Contr�les

I0 = cl_image_I0;
ident_Reflectivity = cl_image.indDataType('Reflectivity');

% TODO : � compl�ter pour le subbottom

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingRange'), 'DataType', ident_Reflectivity, ...
    'InitialFileFormat', 'XTF');
if ~flag
    return
end

% messageImportant

if ~isequal(nomFic, this.InitialFileName)
    copyfile(this.InitialFileName, nomFic)
end

%% Type de donn�es � sauver

str{1}     = 'Time';
str{end+1} = 'Immersion';
str{end+1} = 'Height';
str{end+1} = 'Layback'; % CableOut
str{end+1} = 'Speed';
str{end+1} = 'Heading';
str{end+1} = 'Roll';
str{end+1} = 'Pitch';
str{end+1} = 'SoundVelocity'; % SurfaceSoundSpeed
str{end+1} = 'FishLatitude';
str{end+1} = 'FishLongitude';
str{end+1} = 'ShipLatitude';
str{end+1} = 'ShipLongitude';
str{end+1} = 'Mode_1';
str{end+1} = 'Mode_2';
str{end+1} = 'Heave';
str{end+1} = 'Tide';
str{end+1} = 'PingCounter';
str{end+1} = 'SampleFrequency';
str{end+1} = 'Temperature';
str{end+1} = 'Image';
str1 = 'Liste des choses qui peuvent �tre sauv�es.';
str2 = 'List of things that cab be saved.';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', [3 6 10 11]);
if ~flag
    return
end

%% Sauve Signals

a = cl_xtf('nomFic', nomFic);

[flag, FileHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

if isempty(this.Carto)
    nomDir = fileparts(a.nomFic);
    [Carto, flag] = getDefinitionCarto(I0, 'nomDirSave', nomDir, 'NoGeodetic', 0);
    if ~flag
        return
    end
else
    Carto = this.Carto;
end

for k=1:length(rep)
    SignalName = str{rep(k)};
    SignalValues = this.Sonar.(SignalName);
    switch SignalName
        case 'Time'
            Date = SignalValues.date;
            [Day, Month, Year] = dayIfr2Jma(Date);
            nbPings = length(Year);
            JulianDay = Date - dayJma2Ifr(ones(nbPings,1),ones(nbPings,1),Year);
            
            flag = save_signal(a, 'Ssc_SideScan', 'Year', Year);
            if ~flag
                return
            end
            flag = save_signal(a, 'Ssc_SideScan', 'Month', Month); % Apparemment pas utilis�
            if ~flag
                return
            end
            flag = save_signal(a, 'Ssc_SideScan', 'JulianDay', JulianDay);
            if ~flag
                return
            end
            
            X = SignalValues.heure;
            [Hour, Minute, Second] = hourIfr2Hms(X);
            HSeconds = (Second - floor(Second)) * 100;
            Second = floor(Second);
            
            flag = save_signal(a, 'Ssc_SideScan', 'Hour', Hour);
            if ~flag
                return
            end
            flag = save_signal(a, 'Ssc_SideScan', 'Minute', Minute);
            if ~flag
                return
            end
            flag = save_signal(a, 'Ssc_SideScan', 'Second', Second);
            if ~flag
                return
            end
            flag = save_signal(a, 'Ssc_SideScan', 'HSeconds', HSeconds);
            if ~flag
                return
            end
            
        case 'FishLatitude'
            flag = save_signal_SensorYCoordinate(a, FileHeader.NavUnits, SignalValues, this.Sonar.FishLongitude, Carto);
            if ~flag
                return
            end
            
        case 'FishLongitude'
            flag = save_signal_SensorXCoordinate(a, FileHeader.NavUnits, SignalValues, this.Sonar.FishLatitude, Carto);
            if ~flag
                return
            end
            
        case 'Heading'
            flag = save_signal(a, 'Ssc_SideScan', 'SensorHeading', SignalValues);
            if ~flag
                return
            end
            
        case 'Height'
            flag = save_signal(a, 'Ssc_SideScan', 'SensorPrimaryAltitude', SignalValues);
            if ~flag
                return
            end
            
            
        case 'Image'
            'TODO'
        otherwise
            flag = save_signal(a, 'Ssc_SideScan', SignalName, SignalValues);
            if ~flag
                return
            end
    end
end

%{
Time: [1x1 cl_time]
PingNumber: []
Immersion: [20721x1 single]
Height: [20721x1 single]
CableOut: [20721x1 single]
Speed: [20721x1 single]
Heading: [20721x1 single]
Roll: [20721x1 single]
RollUsedForEmission: []
Pitch: [20721x1 single]
Yaw: []
SurfaceSoundSpeed: [20721x1 single]
FishLatitude: [20721x1 double]
FishLongitude: [20721x1 double]
ShipLatitude: [20721x1 double]
ShipLongitude: [20721x1 double]
PortMode_1: [20721x1 double]
PortMode_2: [20721x1 double]
StarMode_1: []
StarMode_2: []
Heave: [20721x1 single]
Tide: [20721x1 double]
PingCounter: [20721x1 uint32]
SampleFrequency: [20721x1 double]
%}

% switch % Sonar ou subbottom
% case
if ~isequal(subx, 1:this.nbColumns) || ~isequal(suby, 1:this.nbRows)
    my_warndlg('Only the whole image can be flushed into the .all file', 1);
    return
end

%             nomFicSeabed = SimradSeabedNames(this.InitialFileName);
%             status = export_ermapper(this, nomFicSeabed.ReflectivityErs, 'Open', 'r+');
%             if ~status
%                 return
%             end
% end



% function messageImportant
% str = {};
% str{end+1} = 'Ce module de sauvegarde est quelque peu ambigu. Les donn�es des fichiers .all ';
% str{end+1} = 'sont en fait transf�r�s dans des fichiers matlab beaucoup plus rapides � lire. ';
% str{end+1} = 'Ces fichiers se trouvent dans le r�pertoire matlab au m�me niveau que vos ';
% str{end+1} = 'fichiers .all. L''op�ration de sauvegarge est r�alis�e DANS ces fichiers. Si ';
% str{end+1} = 'vous travaillez uniquement avec ce logiciel, �a ne change rien pour vous, tout ';
% str{end+1} = 'est transparent. PAR CONTRE, si vous voulez exploiter ce travail avec un logiciel ';
% str{end+1} = 'autre comme CaraibesR, Caris ou Neptune, il vous faut effectivement archiver vos ';
% str{end+1} = 'r�sultats dans les fichiers .all. Apr�s avoir sauv� vos fichiers dans le r�pertoire ';
% str{end+1} = 'matlab, le logiciel vous demande explicitement si vous voulez sauver les donn�es ';
% str{end+1} = 'dans le .all. Si vous r�pondez oui, il vous demande en plus si vous voulez utiliser ';
% str{end+1} = 'les masques "MasqueDetection" et "MasqueDepth". Vous pouvez en s�lectionner 0? 1 ou les 2 ';
% str{end+1} = 'Une sortie par cancel ne sauvegardera pas les donn�es dans le .all';
%
% % ---------------------------
% % Concatenation en une chaine
%
% s = '';
% for i=1:length(str)
%     s = [s str{i}];
% end
% my_warndlg(s, 1);
%
