% Exportation dans un fichier ermapper
%
% Syntax
%   flag = export_ermapper(aKM, nomFic, ...)
%
% Input Arguments
%   aKM    : Instance de cl_image
%   nomFic : Nom du fichier image ermapper
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   CodeCodage : Type de codage : 1=uint8, 2=uint16, 3=uint32, 4=int8,
%                                 5=int16, 6=int32, 7=single, 8=double
%
% Output Arguments
%   flag  : 1=Reussite, 0=Echec
%
% Examples
%   % Exemple de MNT
%   nomFic = getNomFicDatabase('KBMA0504.ers')
%   [flag, c] = cl_image.import_ermapper(nomFic);
%   SonarScope(c)
%
%   nomFicOut = my_tempname
%   export_ermapper(c, nomFicOut)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
%   % Exemple d'image sonar
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   SonarScope(c) % Or imagesc(c)
%
%   nomFicOut = my_tempname
%   export_ermapper(c, nomFicOut)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
% See also export_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = export_ermapperNew(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this(1), varargin);

[varargin, CodeCodage] = getPropertyValue(varargin, 'CodeCodage', []);
[varargin, ValNaN]     = getPropertyValue(varargin, 'ValNaN', []);
[varargin, Open]       = getPropertyValue(varargin, 'Open', []);
[varargin, options]    = getPropertyValue(varargin, 'options', []); %#ok<ASGLU>

[nomDir, nom, ext] = fileparts(nomFic);
if strcmp(ext, '.ers')
    nomFic = fullfile(nomDir, nom);
end
nomFic = removeAccentuation(nomFic);
if length(nomFic) > 128
    str1 = 'Le nom complet de l''image est peut-�tre trop long, si l''exportation �choue essayer de donner un nom plus cour au fichier (supprimer des r�pertoires, etc ...)';
    str2 = 'The full name of the file is long, if the exportation aborts try to limit the file name (supress some sub-directories or shorten the final name.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NomTropLongPourErMapper');
end


if isequal(subx, 1:this(1).nbColumns) && isequal(suby, 1:this(1).nbRows)
    that = this;
else
    that = extraction(this, 'subx', subx, 'suby', suby);
end
nomFicXML = [nomFic '.xml'];
flag = export_xml(that, nomFicXML, 'ErMapper', 1);
if ~flag
    return
end

% ----------------------------------
% Determination de parametres divers

[CodeCodage, CellType, ImplicitValNaN] = export_ermapper_init(this(1), 'CodeCodage', CodeCodage);
if isempty(ValNaN)
    ValNaN = ImplicitValNaN;
end

% ------------------------
% Creation du fichier .ers

flag = export_ermapper_ersNew(this, nomFic, CellType, ValNaN, 'subx', subx, 'suby', suby, ...
    'options', options);
if ~flag
    return
end



% -------------------------------------
% Creation du fichier des signaux sonar

% flag = export_ermapper_sigV(this(1), nomFic, 'suby', suby);
% if ~flag
%     return
% end

% -------------------------
% Creation du fichier Image

% flag = export_ermapper_image(this, nomFic, CodeCodage, ValNaN, 'subx', subx, 'suby', suby, 'Open', Open);
% if ~flag
%     return
% end

% -----------------------------------------------------------------------
% Exportation des statistiques, R�gios d'int�r�t et Signatures texturales

% BathyCel            = get_SonarBathyCel(this);
% CourbesStatistiques = this.CourbesStatistiques;
% RegionOfInterest    = this.RegionOfInterest;
% Texture             = this.Texture;
% if ~isempty(CourbesStatistiques) || ~isempty(RegionOfInterest) || ~isempty(Texture) || (isfield(BathyCel, 'Depth') && ~isempty(BathyCel.Depth))
%     [nomDir, nomFic] = fileparts(nomFic);
%     nomFicOthers = fullfile(nomDir, nomFic, [nomFic '_Others.mat']);
%     save(nomFicOthers, 'CourbesStatistiques', 'RegionOfInterest', 'Texture', 'BathyCel')
% end
