% Exportation du fichier de description des metadonnees ermapper (.ers)
%
% Syntax
%   status = export_ermapper_ers(aKM, nomFic, CellType, ValNaN, ...)
%
% Input Arguments
%   aKM      : Instance de cl_image
%   nomFic   : Nom du fichier image ermapper
%   CellType : 'Unsigned8BitInteger' | 'Unsigned16BitInteger' | 'Unsigned32BitInteger' |
%              'Signed8BitInteger' | 'Signed16BitInteger' | 'Signed32BitInteger' |
%              'IEEE4ByteReal' | 'IEEE8ByteReal'
%   ValNaN   : Valeur codant les non-valeurs (NaN ou nombre particulier)
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   status  : 1=Reussite, 0=Echec
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   imagesc(c)
%
%   nomFicOut = my_tempname
%   % Exportation qui se suffirait � elle m�me :
%   % export_ermapper(c, nomFicOut)
%
%   % Exportation decomposee :
%   [CodeCodage, CellType, ValNaN] = export_ermapper_init(c)
%
%   % Creation du fichier .ers
%   status = export_ermapper_ers(c, nomFicOut, CellType, ValNaN)
%
%   % Creation du fichier des signaux sonar
%   status = export_ermapper_sigV(c, nomFicOut)
%   % Creation du fichier Image
%   status = export_ermapper_image(c, nomFicOut, CodeCodage, ValNaN)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
% See also export_ermapper export_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function status = export_ermapper_ers(this, nomFic, CellType, ValNaN, varargin)

[varargin, options] = getPropertyValue(varargin, 'options', []);

if isempty(options)
    options.flagImage   = 1;
    options.flagShading = is_shading_possible(this);
end

thisOne = this(1);

[subx, suby, varargin] = getSubxSuby(thisOne, varargin);

[varargin, nomFicData] = getPropertyValue(varargin, 'nomFicData', []); %#ok<ASGLU>

[pathname, nomFicSeul, ext] = fileparts(nomFic);
if ~strcmp(ext, '.ers')
    nomFicSeul = [nomFicSeul, ext];
end
nomFic = fullfile(pathname, nomFicSeul);
[~, nomFicSeul] = fileparts(nomFic);
if isempty(nomFicData)
    nomFicData = [nomFicSeul filesep nomFicSeul];
end

if isempty(thisOne.Sonar.Desciption)
    SensorName = 'Unknown';
else
    SensorName = get(thisOne.Sonar.Desciption, 'Sonar.Name');
end

if isempty(thisOne.Unit) || isequal(thisOne.Unit, ' ')
    thisOne.Unit = 'Unknown';
end
if isempty(thisOne.XUnit) || isequal(thisOne.XUnit, ' ')
    thisOne.XUnit = 'Unknown';
end
if isempty(thisOne.YUnit) || isequal(thisOne.YUnit, ' ')
    thisOne.YUnit = 'Unknown';
end
if isempty(thisOne.XLabel)
    thisOne.XLabel = 'Unknown';
end
if isempty(thisOne.YLabel)
    thisOne.YLabel = 'Unknown';
end
if isempty(thisOne.InitialFileName)
    thisOne.InitialFileName = 'Unknown';
end

b = cl_ermapper_ers;
b = set_FicIn(b, nomFic);
b = set(b, 'Version',       '6.3');
b = set(b, 'Name',          nomFicSeul);
b = set(b, 'Description',   'Coming from SonarScope');
% b = set(b, 'SourceDataset', ????????????????);
% b = set(b, 'LastUpdated',   t2str(cl_time('timeMat', now), 'Format', 2));
b = set(b, 'LastUpdated',   'Fri Dec 26 23:59:59 GMT 1957');
b = set(b, 'SensorName',    SensorName);
% b = set(b, 'SenseDate',
b = set(b, 'DataFile', nomFicData);
b = set(b, 'DataSetType',   'ERStorage');   % ou "Translated" si Fourier ?
b = set(b, 'DataType',      'Raster');
b = set(b, 'ByteOrder',     'LSBFirst');
% b = set(b, 'HeaderOffset',
b = set(b, 'Comments',      'From SonarScope');

if ~isempty(thisOne.Carto)
    %     if this.GeometryType == cl_image.indGeometryType('LatLong')
    %         b = set(b, 'CoordinateType', 'LL'); % East-North ???    : A verifier
    %     else
    b = set(b, 'CoordinateType', 'EN'); % East-North ???    : A verifier
    %     end
    
    GrandAxe = get(thisOne.Carto,     'Ellipsoide.DemiGrandAxe');
    Eccentricity = get(thisOne.Carto, 'Ellipsoide.Excentricite');
    Datum = ident_ellipsoide(b, GrandAxe, Eccentricity);
    b = set(b, 'Datum', Datum);
    
    if this.GeometryType == cl_image.indGeometryType('LatLong')
        Projection = 'GEODETIC';
    else
        Projection = codeProjectionErmapper(thisOne.Carto);
        
        % Cas rencontr� pour l'importation de l'imagerie des .all avec une
        % projection Geodetic qui fout la %@#&" dans erviewer : on met une
        % projection UTM pour �viter des pbs.
        if isempty(Projection)
            Lon = get(thisOne, 'SonarFishLongitude');
            Lat = get(thisOne, 'SonarFishLatitude');
            if ~isempty(Lon) && ~isempty(Lat)
                LonMean = mean(Lon, 'omitnan');
                LatMean = mean(Lat, 'omitnan');
                if ~isnan(LonMean) && ~isnan(LatMean)
                    InitialValue = floor((LonMean+180)/6 + 1);
                    InitialValue = 1 + mod(InitialValue-1+60, 60);
                    if LatMean >= 0
                        Projection = sprintf('NUTM%d', InitialValue);
                    else
                        Projection = sprintf('SUTM%d', InitialValue);
                    end
                else
                    Projection = 'NUTM0'; % C'est pas encore arriv� mais avec la chance que j'ai !
                end
            else
                Projection = 'NUTM0'; % C'est pas encore arriv� mais avec la chance que j'ai !
            end
        end
    end
    b = set(b, 'Projection', Projection);
    
    Azimut = get(thisOne.Carto, 'Referentiel.Azimut');
    b = set(b, 'Rotation', Azimut);
    
    Eastings  = thisOne.x(subx(1));
    Northings = max(thisOne.y(suby));
    
    %     Northings = Northings - 5.0 * abs(mean(diff(thisOne.y)));
    
else
    b = set(b, 'Datum',          'RAW');
    b = set(b, 'Projection',     'RAW');
    b = set(b, 'CoordinateType', 'RAW');
    b = set(b, 'Rotation', 0);
    
    Eastings  = thisOne.x(subx(1));
    Northings = max(thisOne.y(suby));
end

if this.GeometryType == cl_image.indGeometryType('LatLong')
    b = set(b, 'Units', 'degrees');
else
    b = set(b, 'Units', 'm');
end
b = set(b, 'CellType', CellType);
b = set(b, 'NullCellValue', ValNaN);

b = set(b, 'NrOfLines',         length(suby));
b = set(b, 'NrOfCellsPerLine',  length(subx));

%     b = set(b, 'NrOfBands',     size(thisOne.Image,3));
if thisOne.ImageType == 2 % RGB
    b = set(b, 'NrOfBands',     3);
    BandId_Value   = {'Red'; 'Green'; 'Blue'};
    BandId_Units   = {thisOne.Unit};
    BandId_Comment = {thisOne.Name};
else
    b = set(b, 'NrOfBands', length(this));
    for i=1:length(this)
        BandId_Value{i}   = this(i).Name; %#ok
        BandId_Units{i}   = this(i).Unit; %#ok
        BandId_Comment{i} = this(i).Name; %#ok
    end
end

% b = set(b, 'BandId_Value',      {thisOne.Name});
b = set(b, 'BandId_Value',      BandId_Value);
b = set(b, 'BandId_Units',      BandId_Units);
% b = set(b, 'BandId_Width',      {pi});    % Apparemment, c'est pas ce qu'on croit
b = set(b, 'BandId_Comment',    BandId_Comment);
b = set(b, 'RegistrationCellX', 0);   % ??????????????????????????????????????????????
b = set(b, 'RegistrationCellY', 0);   % ??????????????????????????????????????????????
% b = set(b, 'Xdimension',        abs(thisOne.x(2)-thisOne.x(1)));
% b = set(b, 'Ydimension',        abs(thisOne.y(2)-thisOne.y(1)));
b = set(b, 'Xdimension',        abs(mean(diff(thisOne.x))));
b = set(b, 'Ydimension',        abs(mean(diff(thisOne.y))));

b = set(b, 'Eastings',          Eastings);
b = set(b, 'Northings',         Northings);
b = set(b, 'MetersX',           thisOne.x(subx(end))-thisOne.x(subx(1)));   % ??????????????????????????????????????????????
b = set(b, 'MetersY',           thisOne.y(suby(end))-thisOne.y(suby(1)));   % ??????????????????????????????????????????????
if ~isempty(thisOne.StatValues)
    b = set(b, 'MetersZ',           thisOne.StatValues.Range);   % ??????????????????????????????????????????????
end

b = set(b, 'UserInfo.ImageType',                       thisOne.ImageType);
b = set(b, 'UserInfo.DataType',                      thisOne.DataType);
b = set(b, 'UserInfo.GeometryType',                 thisOne.GeometryType);
b = set(b, 'UserInfo.XUnit',                           thisOne.XUnit);
b = set(b, 'UserInfo.YUnit',                           thisOne.YUnit);
b = set(b, 'UserInfo.XLabel',                          thisOne.XLabel);
b = set(b, 'UserInfo.YLabel',                          thisOne.YLabel);
b = set(b, 'UserInfo.SpectralStatus',                  thisOne.SpectralStatus);
b = set(b, 'UserInfo.YDir',                            thisOne.YDir);
b = set(b, 'UserInfo.XDir',                            thisOne.XDir);
b = set(b, 'UserInfo.ColormapIndex',                   thisOne.ColormapIndex);
b = set(b, 'UserInfo.Colormap',                        thisOne.Colormap);
b = set(b, 'UserInfo.ColormapCustom',                  thisOne.ColormapCustom);
b = set(b, 'UserInfo.CLim',                            thisOne.CLim);
b = set(b, 'UserInfo.Video',                           thisOne.Video);
b = set(b, 'UserInfo.VertExagAuto',                    thisOne.VertExagAuto);
b = set(b, 'UserInfo.VertExag',                        thisOne.VertExag);
b = set(b, 'UserInfo.Azimuth',                         thisOne.Azimuth);
b = set(b, 'UserInfo.ContourValues',                   thisOne.ContourValues);
if isempty(thisOne.NuIdentParent)
    thisOne.NuIdentParent = -1;
end
b = set(b, 'UserInfo.NuIdentParent',                   thisOne.NuIdentParent);
b = set(b, 'UserInfo.TagSynchroX',                     thisOne.TagSynchroX);
b = set(b, 'UserInfo.TagSynchroY',                     thisOne.TagSynchroY);
b = set(b, 'UserInfo.TagSynchroContrast',              thisOne.TagSynchroContrast);
b = set(b, 'UserInfo.InitialFileName',                 thisOne.InitialFileName);

b = set(b, 'UserInfo.HistoCentralClasses',             thisOne.HistoCentralClasses);
b = set(b, 'UserInfo.HistoValues',                     thisOne.HistoValues);
b = set(b, 'UserInfo.StatValues',                      thisOne.StatValues);
b = set(b, 'UserInfo.StatSummary',                     thisOne.StatSummary);

if ~isempty(thisOne.Sonar.Desciption)
    b = set(b, 'UserInfo.Sonar.Ident',                 get(thisOne.Sonar.Desciption, 'Sonar.Ident'));
    b = set(b, 'UserInfo.Sonar.Family',                get(thisOne.Sonar.Desciption, 'Sonar.Family'));
end
b = set(b, 'UserInfo.Sonar.RawDataResol',              thisOne.Sonar.RawDataResol);
b = set(b, 'UserInfo.Sonar.ResolutionD',               thisOne.Sonar.ResolutionD);
b = set(b, 'UserInfo.Sonar.ResolutionX',               thisOne.Sonar.ResolutionX);
b = set(b, 'UserInfo.Sonar.TVG.etat',                  thisOne.Sonar.TVG.etat);
b = set(b, 'UserInfo.Sonar.TVG.origine',               thisOne.Sonar.TVG.origine);
b = set(b, 'UserInfo.Sonar.TVG.ConstructTypeCompens',  thisOne.Sonar.TVG.ConstructTypeCompens);
b = set(b, 'UserInfo.Sonar.TVG.ConstructAlpha',        thisOne.Sonar.TVG.ConstructAlpha);
b = set(b, 'UserInfo.Sonar.TVG.ConstructConstante',    thisOne.Sonar.TVG.ConstructConstante);
b = set(b, 'UserInfo.Sonar.TVG.ConstructCoefDiverg',   thisOne.Sonar.TVG.ConstructCoefDiverg);
b = set(b, 'UserInfo.Sonar.TVG.ConstructTable',        thisOne.Sonar.TVG.ConstructTable);
b = set(b, 'UserInfo.Sonar.TVG.IfremerAlpha',          thisOne.Sonar.TVG.IfremerAlpha);
b = set(b, 'UserInfo.Sonar.TVG.IfremerConstante',      thisOne.Sonar.TVG.IfremerConstante);
b = set(b, 'UserInfo.Sonar.TVG.IfremerCoefDiverg',     thisOne.Sonar.TVG.IfremerCoefDiverg);
b = set(b, 'UserInfo.Sonar.NE.etat',                   thisOne.Sonar.NE.etat);
b = set(b, 'UserInfo.Sonar.GT.etat',                   thisOne.Sonar.GT.etat);
b = set(b, 'UserInfo.Sonar.SH.etat',                   thisOne.Sonar.SH.etat);
b = set(b, 'UserInfo.Sonar.TVG.ConstructCoefDiverg',   thisOne.Sonar.TVG.ConstructCoefDiverg);
b = set(b, 'UserInfo.Sonar.TVG.ConstructConstante',    thisOne.Sonar.TVG.ConstructConstante);
b = set(b, 'UserInfo.Sonar.DiagEmi.etat',                  thisOne.Sonar.DiagEmi.etat);
b = set(b, 'UserInfo.Sonar.DiagEmi.origine',                thisOne.Sonar.DiagEmi.origine);
b = set(b, 'UserInfo.Sonar.DiagEmi.ConstructTypeCompens',   thisOne.Sonar.DiagEmi.ConstructTypeCompens);
b = set(b, 'UserInfo.Sonar.DiagEmi.IfremerTypeCompens',     thisOne.Sonar.DiagEmi.IfremerTypeCompens);
b = set(b, 'UserInfo.Sonar.DiagRec.etat',               thisOne.Sonar.DiagRec.etat);
b = set(b, 'UserInfo.Sonar.DiagRec.origine',            thisOne.Sonar.DiagRec.origine);
b = set(b, 'UserInfo.Sonar.DiagRec.ConstructTypeCompens', thisOne.Sonar.DiagRec.ConstructTypeCompens);
b = set(b, 'UserInfo.Sonar.DiagRec.IfremerTypeCompens', thisOne.Sonar.DiagRec.IfremerTypeCompens);
b = set(b, 'UserInfo.Sonar.AireInso.etat',              thisOne.Sonar.AireInso.etat);
b = set(b, 'UserInfo.Sonar.AireInso.origine',           thisOne.Sonar.AireInso.origine);
b = set(b, 'UserInfo.Sonar.BS.etat',                    thisOne.Sonar.BS.etat);
b = set(b, 'UserInfo.Sonar.BS.origine',                 thisOne.Sonar.BS.origine);
b = set(b, 'UserInfo.Sonar.BS.origineBelleImage',       thisOne.Sonar.BS.origineBelleImage);
b = set(b, 'UserInfo.Sonar.BS.origineFullCompens',      thisOne.Sonar.BS.origineFullCompens);
b = set(b, 'UserInfo.Sonar.BS.IfremerCompensTable',     thisOne.Sonar.BS.IfremerCompensTable);

%  ColormapIndex  <-> {'1=User'} | '2=gray' | '3=jet' | '4=cool' | '5=hsv' | '6=hot' | '7=bone' | '8=copper' | '9=pink' | '10=flag' | '11=CNES' |
%  '12=CNESjet' | '13=ColorSea' | '14=ColorEarth' | '15=NIWA1' | '16=NIWA2' | '17=JetSym' | '18=Haxby' | '19=Becker' | '20=Catherine'

switch thisOne.ColormapIndex
    case 2
        LookupTable = 'greyscale';
    otherwise
        LookupTable = 'pseudocolor';
end

write(b);
status = write_UserInfo(b);

if options.flagImage
    nomFicAlg = get_nomFicAlg(cl_ermapper_ers([]), nomFic);
    export_alg(b, nomFicAlg, nomFic, this.CLim, this.HistoCentralClasses, this.HistoValues, ...
        'nomLayer', this.Name, 'uniteLayer', this.Unit, 'SunAzimuth', 200, 'SunAngle', 80, ...
        'LookupTable', LookupTable);
end

if options.flagShading
    nomFicAlg = get_nomFicAlg(cl_ermapper_ers([]), nomFic, 'Shading');
    export_alg(b, nomFicAlg, nomFic, this.CLim, this.HistoCentralClasses, this.HistoValues, ...
        'nomLayer', this.Name, 'uniteLayer', this.Unit, ...
        'LookupTable', LookupTable, 'Shading');
end
