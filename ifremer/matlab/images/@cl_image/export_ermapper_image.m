% Exportation de l'image dans le fichier image ermapper
%
% Syntax
%   status = export_ermapper_image(this, nomFic, CodeCodage, ValNaN, ...)
%
% Input Arguments
%   this       : Instance de cl_image
%   nomFic     : Nom du fichier image ermapper
%   CodeCodage : Type de codage : 1=uint8, 2=uint16, 3=uint32, 4=int8,
%                                 5=int16, 6=int32, 7=single, 8=double
%   ValNaN     : Valeur codant les non-valeurs (NaN ou nombre particulier)
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   status  : 1=Reussite, 0=Echec
%
% Remarks : Cette fonction est appelee � l'interieur de export_ermapper,
% elle peut toute fois etre appelee directement conformement � l'exemple
% afin de de modifier que le fichier image
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   imagesc(c)
%
%   nomFicOut = my_tempname
%   % Exportation qui se suffirait � elle m�me :
%   % export_ermapper(c, nomFicOut)
%
%   % Exportation decomposee :
%   [CodeCodage, CellType, ValNaN] = export_ermapper_init(c)
%   % Creation du fichier .ers
%   status = export_ermapper_ers(c, nomFicOut, CellType, ValNaN)
%   % Creation du fichier des signaux sonar
%   status = export_ermapper_sigV(c, nomFicOut)
%
%   % Creation du fichier Image
%   status = export_ermapper_image(c, nomFicOut, CodeCodage, ValNaN)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
% See also export_ermapper export_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function status = export_ermapper_image(this, nomFic, CodeCodage, ValNaN, varargin)

thisOne = this(1);

[subx, suby, varargin] = getSubxSuby(thisOne, varargin);

[varargin, Open] = getPropertyValue(varargin, 'Open', []); %#ok<ASGLU>

if isempty(Open)
    Open = 'w+';
end

strFwrite ={'uint8'; 'uint16'; 'uint32'; 'int8'; 'int16'; 'int32'; 'float32'; 'double'};
fwritePrecision = strFwrite{CodeCodage};

if thisOne.ImageType == 2 % RGB
    %     Image = thisOne.Image(suby, subx, :);
    Image = zeros(length(suby), length(subx), 3, class(thisOne.Image(1)));
    for i=1:length(suby)
        Image(i,:,:) = thisOne.Image(suby(i), subx, :);
    end
else
    if length(this) == 1
        if isequal(suby, 1:this.nbRows) && isequal(subx, 1:this.nbColumns)
            Image = this.Image;
            
            % DEBUT VERIFICATION POUR TROUVER UN BUG RELATIF AU MASQUE
            if (size(Image,1) ~= this.nbRows) || (size(Image,2) ~= this.nbColumns)
                str = 'There is a mistake in this image, its size does not fit to its description, please save the session in a project and send it to sonarscope@ifremer.fr. Do not forget to explain how this image has been made.';
                my_warndlg(str, 1);
                Image = zeros(length(suby), length(subx), length(this), class(thisOne.Image(1)));
                for i=1:length(this)
                    Image(:,:,i) = this(i).Image(suby, subx);
                end
            end
            % FIN VERIFICATION POUR TROUVER UN BUG RELATIF AU MASQUE
            
        else
            Image = zeros(length(suby), length(subx), length(this), class(thisOne.Image(1)));
            for i=1:length(this)
                Image(:,:,i) = this(i).Image(suby, subx);
            end
        end
    else
        Image = cl_memmapfile('Value', 0, 'Size', [length(suby) length(subx) length(this)], 'Format', class(thisOne.Image));
        for i=1:length(this)
            for iLig=1:length(suby)
                Image(iLig,:,i) = this(i).Image(suby(iLig), subx);
            end
        end
    end
end


switch CodeCodage
    case 1 % 'uint8'
        if ~isa(Image, 'uint8') && ~isa(Image, 'cl_memmapfile')
            maxImage =  getMaxImage(Image, thisOne.ValNaN);
            if maxImage < 1
                Image = Image * 254;
            end
            Image = uint8(Image);
        end
    case 2 % 'uint16'
        if ~isa(Image, 'uint16')
            maxImage =  getMaxImage(Image, thisOne.ValNaN);
            if maxImage < 1
                Image = Image * (intmax('uint16') - 1);
            end
            Image = uint16(Image);
        end
    case 3 % 'uint32'
        if ~isa(Image, 'uint32')
            maxImage =  getMaxImage(Image, thisOne.ValNaN);
            if maxImage < 1
                Image = Image * (intmax('uint32') - 1);
            end
            Image = uint32(Image);
        end
    case 4 % 'int8'
        if ~isa(Image, 'int8')
            Image = int8(Image);
        end
    case 5 % 'int16'
        if ~isa(Image, 'int16')
            Image = int16(Image(:,:,:));
        end
    case 6 % 'int32'
        if ~isa(Image, 'int32')
            Image = int32(Image(:,:,:));
        end
    case 7 % 'single'
        if ~isa(Image, 'single') && ~isa(Image, 'cl_memmapfile')
            Image = single(Image(:,:,:));
        end
    case 8 % 'double'
        if ~isa(Image, 'double') && ~isa(Image, 'cl_memmapfile')
            Image = double(Image(:,:,:));
        end
end

% [NomDir, InitialFileName] = fileparts(this.InitialFileName)
if isequal(nomFic, this.InitialFileName)
    status = 1;
    return
end

[nomDir, nomFic] = fileparts(nomFic);
if isempty(nomDir)
    nomDir = pwd;
end
nomDirImage = fullfile(nomDir, nomFic);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    mkdir(nomDir, nomFic);
end
nomFic = fullfile(nomDirImage, nomFic);

fid_image = fopen(nomFic, Open, 'ieee-le');
if fid_image == -1
    str1 = sprintf('Erreur d''ecriture fichier %s. Ce fchier est vraismblablement associ� � une image.\nLes fichiers .ers, .alg, ... ont �t� correctement sauv�s.', nomFic);
    str2 = sprintf('File writing failure : %s. This file is certainly associated with an image.\nNote that the .ers, .alg, files have been correctly saved.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    status = 0;
    return
end

if (thisOne.YDir == 1) && ((thisOne.y(end)-thisOne.y(1)) > 0)
    flagFlipud = 0;
else
    flagFlipud = 1;
end

if (thisOne.XDir == 2) && ((thisOne.x(end)-thisOne.x(1)) > 0)
    flagFliplr = 1;
else
    flagFliplr = 0;
end

if (this(1).nbSlides == 1) && (flagFlipud) && ~isa(Image, 'cl_memmapfile')
    count = fwrite(fid_image, my_transpose(Image), fwritePrecision);
    if count ~= numel(Image)
        str = sprintf('Erreur d''ecriture fichier "%s"', nomFic);
        my_warndlg(str,0);
        status = 0;
        return
    end
else
    nbRows = size(Image,1);
    
    %     str = this.Name;
    str = [nomFic '.ers'];
    %     str(length(str)+1:150) = '.';
    str(length(str)+1:100) = '.';
    hw = create_waitbar(str, 'Entete', 'IFREMER - SonarScope : ErMapper Exportation');
    if thisOne.ImageType == 2 % RGB
        nbLigBlock = 1;
    else
        nbLigBlock = min(500, floor(2e6 /size(Image,2)));
    end
    for i=1:nbLigBlock:nbRows
        my_waitbar(i, nbRows, hw);
        
        if flagFlipud
            iLig = i:min(i+nbLigBlock-1, nbRows);
        else
            iLig = i:min(i+nbLigBlock-1, nbRows);
            iLig = nbRows+1-iLig;
        end
        
        Ligne = Image(iLig,:,:);
        if isnan(thisOne.ValNaN)
            sub = find(isnan(Ligne));
        else
            sub = find(Ligne == thisOne.ValNaN);
        end
        Ligne(sub) = ValNaN;
        
        if flagFliplr
            Ligne = my_fliplr(Ligne);
        end
        
        count = fwrite(fid_image, my_transpose(Ligne), fwritePrecision);
        if count ~= numel(Ligne)
            str = sprintf('Erreur d''ecriture fichier "%s"', nomFic);
            my_warndlg(str,0);
            status = 0;
            return
        end
    end
    my_close(hw)
end
fclose(fid_image);
status = 1;


function maxImage = getMaxImage(Image, ValNaN)
if isnan(ValNaN)
    subOK = find(~isnan(Image));
else
    subOK = find(Image ~= ValNaN);
end
maxImage = max(Image(subOK));
