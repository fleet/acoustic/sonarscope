% Identification de parametres servant à l'exportation en ermapper
%
% Syntax
%   [CodeCodage, CellType, ValNaN] = export_ermapper_init(aKM, ...)
%
% Input Arguments
%   aKM : Instance de cl_image
%
% Name-Value Pair Arguments
%   CodeCodage : Type de codage si different de celui de la donnee :
%                1=uint8, 2=uint16, 3=uint32, 4=int8, 5=int16, 6=int32, 7=single, 8=double
%
% Output Arguments
%   CodeCodage : Type de codage : 1=uint8, 2=uint16, 3=uint32, 4=int8, 5=int16, 6=int32, 7=single, 8=double
%   CellType   : 'Unsigned8BitInteger' | 'Unsigned16BitInteger' | 'Unsigned32BitInteger' |
%                'Signed8BitInteger' | 'Signed16BitInteger' | 'Signed32BitInteger' |
%                'IEEE4ByteReal' | 'IEEE8ByteReal'
%   ValNaN     : Valeur codant les non-valeurs (NaN ou nombre particulier)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   imagesc(c)
%
%   [CodeCodage, CellType, ValNaN] = export_ermapper_init(c)
%
% See also export_ermapper export_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [CodeCodage, CellType, ValNaN] = export_ermapper_init(this, varargin)

[varargin, CodeCodage] = getPropertyValue(varargin, 'CodeCodage', []); %#ok<ASGLU>

strCodeCodage ={'Unsigned8BitInteger'
    'Unsigned16BitInteger'
    'Unsigned32BitInteger'
    'Signed8BitInteger'
    'Signed16BitInteger'
    'Signed32BitInteger'
    'IEEE4ByteReal'
    'IEEE8ByteReal'};

if isempty(CodeCodage)
    switch this.ImageType
        case 1  % 'Intensite'
            X = this.Image(1);
            if isa(X, 'uint8')
                CodeCodage = 1;
                ValNaN = 255;
            elseif isa(X, 'logical')
                CodeCodage = 1;
                ValNaN = 0;
            elseif isa(X, 'uint16')
                CodeCodage = 2;
                ValNaN = intmax('uint16');
            elseif isa(X, 'uint32')
                CodeCodage = 3;
                ValNaN = intmax('uint32');
            elseif isa(X, 'int8')
                CodeCodage = 4;
                ValNaN = intmax('int8');
            elseif isa(X, 'int16')
                CodeCodage = 5;
                ValNaN = intmax('int16');
            elseif isa(X, 'int32')
                CodeCodage = 6;
                ValNaN = intmax('int32');
            elseif isa(X, 'single')
                CodeCodage = 7;
                ValNaN = realmax('single');
            elseif isa(X, 'double')
                CodeCodage = 8;
                ValNaN = realmax('double');
            end
        case 2  % 'RGB'
            CodeCodage = 1; % 'uint8'
            ValNaN = 0;
        case 3  % 'Indexee'
            CodeCodage = 1; % 'uint8'
            ValNaN = 255;
        case 4  % 'Binaire'
            CodeCodage = 1; % 'uint8'
            ValNaN = 255;
    end
else
    switch CodeCodage
        case 1
            ValNaN = 255;
        case 2
            ValNaN = intmax('uint16');
        case 3
            ValNaN = intmax('uint32');
        case 4
            ValNaN = intmax('int8');
        case 5
            ValNaN = intmax('int16');
        case 6
            ValNaN = intmax('int32');
        case 7
            ValNaN = realmax('single');
        case 8
            ValNaN = realmax('double');
    end
end
CellType = strCodeCodage{CodeCodage};
