% Exportation des signaux sonar dans le fichier _sigV associe au fichier ermapper
%
% Syntax
%   status = export_ermapper_sigV(aKM, nomFic, ...)
%
% Input Arguments
%   aKM    : Instance de cl_image
%   nomFic : Nom du fichier image ermapper
%
% Name-Value Pair Arguments
%   suby : subsampling in Y
%
% Output Arguments
%   status  : 1=Reussite, 0=Echec
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   imagesc(c)
%
%   nomFicOut = my_tempname
%   % Exportation qui se suffirait � elle m�me :
%   % export_ermapper(c, nomFicOut)
%
%   % Exportation decomposee :
%   [CodeCodage, CellType, ValNaN] = export_ermapper_init(c)
%   % Creation du fichier .ers
%   status = export_ermapper_ers(c, nomFicOut, CellType, ValNaN)
%
%   % Creation du fichier des signaux sonar
%   status = export_ermapper_sigV(c, nomFicOut)
%
%   % Creation du fichier Image
%   status = export_ermapper_image(c, nomFicOut, CodeCodage, ValNaN)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
% See also export_ermapper export_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function status = export_ermapper_sigV(this, nomFic, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', 1:this.nbRows); %#ok<ASGLU>

if ~isempty(this.Sonar.Time) && ...
       ((this.GeometryType == cl_image.indGeometryType('PingSamples')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingAcrossDist')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingRange')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingBeam')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingSnippets')))
    
    [nomDir, nomFic] = fileparts(nomFic);
    if isempty(nomDir)
        nomDir = pwd;
    end
    nomDirImage = fullfile(nomDir, nomFic);
    if ~exist(nomDirImage, 'dir')
        if exist(nomDirImage, 'file')
            delete(nomDirImage)
        end
        flag = mkdir(nomDir, nomFic);
        if ~flag
            msg = sprintf('Directory creation failure on %s', fullfile(nomDir, nomFic));
            my_warndlg(msg, 1);
        end
    end
    
    VersionNumber = 20131211;
    nbColumns = 29;
    X = zeros(length(suby), nbColumns, 'double');
    if ~isempty(this.Sonar.Time)
        time = this.Sonar.Time;
        pppp = time.timeMat;
        X(:, 1) = pppp(suby);
    end
    if ~isempty(this.Sonar.PingNumber)
        my_warndlg('Message for JMA : cl_image/export_ermapper_sigV PingNumber : on ne devrait plus passer par l�. ', 1);
        X(:, 3) = this.Sonar.PingNumber(suby);
    end
    if ~isempty(this.Sonar.Immersion)
        X(:, 4) = this.Sonar.Immersion(suby);
    end
    if ~isempty(this.Sonar.Height)
        X(:, 5) = this.Sonar.Height(suby);
    end
    if ~isempty(this.Sonar.CableOut)
        X(:, 6) = this.Sonar.CableOut(suby);
    end
    if ~isempty(this.Sonar.Speed)
        X(:, 7) = this.Sonar.Speed(suby);
    end
    if ~isempty(this.Sonar.Heading)
        X(:, 8) = this.Sonar.Heading(suby);
    end
    if ~isempty(this.Sonar.Roll)
        X(:, 9) = this.Sonar.Roll(suby);
    end
    if ~isempty(this.Sonar.Pitch)
        X(:, 10) = this.Sonar.Pitch(suby);
    end
    if ~isempty(this.Sonar.Yaw)
        X(:, 11) = this.Sonar.Yaw(suby);
    end
    if ~isempty(this.Sonar.SurfaceSoundSpeed)
        X(:, 12) = this.Sonar.SurfaceSoundSpeed(suby);
    end
    if ~isempty(this.Sonar.FishLatitude)
        X(:, 13) = this.Sonar.FishLatitude(suby);
    end
    if ~isempty(this.Sonar.FishLongitude)
        X(:, 14) = this.Sonar.FishLongitude(suby);
    end
    if ~isempty(this.Sonar.ShipLatitude)
        X(:, 15) = this.Sonar.ShipLatitude(suby);
    end
    if ~isempty(this.Sonar.ShipLongitude)
        X(:, 16) = this.Sonar.ShipLongitude(suby);
    end
    if ~isempty(this.Sonar.PortMode_1)
        X(:, 17) = this.Sonar.PortMode_1(suby);
    end
    if ~isempty(this.Sonar.PortMode_2)
        X(:, 18) = this.Sonar.PortMode_2(suby);
    end
    if ~isempty(this.Sonar.StarMode_1)
        X(:, 19) = this.Sonar.StarMode_1(suby);
    end
    if ~isempty(this.Sonar.StarMode_2)
        X(:, 20) = this.Sonar.StarMode_2(suby);
    end
    if ~isempty(this.Sonar.Heave)
        X(:, 21) = this.Sonar.Heave(suby);
    end
    if ~isempty(this.Sonar.Tide)
        X(:, 22) = this.Sonar.Tide(suby);
    end
    if ~isempty(this.Sonar.PingCounter)
        X(:, 23) = this.Sonar.PingCounter(suby);
    end
    if ~isempty(this.Sonar.SampleFrequency)
        X(:, 24) = this.Sonar.SampleFrequency(suby);
    end
    if ~isempty(this.Sonar.SonarFrequency)
        X(:, 25) = this.Sonar.SonarFrequency(suby);
    end
    if ~isempty(this.Sonar.Interlacing)
        X(:, 26) = this.Sonar.Interlacing(suby);
    end
    
    %         Desciption: [1x1 cl_sounder]
    %              BathyCel: [1x1 struct]
    %                   TVG: [1x1 struct]
    
    if (this.YDir == 1) && ((this.y(end)-this.y(1)) > 0)
        if (X(2, 3) > X(1, 3))
        else
            X = flipud(X);
        end
    else
        if (X(2, 3) > X(1, 3))
            X = flipud(X);
        else
        end
    end
    
    %% Ecriture du fichier
    
    nomFic = fullfile(nomDirImage, nomFic);
    nomFic = [nomFic '_sigV'];
    fid_sigV = fopen(nomFic, 'w+', 'ieee-le.l64');
    if fid_sigV == -1
        str = sprintf('Exportation failure file %s', nomFic);
        my_warndlg(str, 1);
        status = 0;
        return
    end
    count = fwrite(fid_sigV, VersionNumber, 'double');
    if count ~= 1
        str = sprintf('Writing failure file %s', nomFic);
        my_warndlg(str, 1);
        status = 0;
        return
    end
    count = fwrite(fid_sigV, X', 'double');
    if count ~= numel(X)
        str = sprintf('Writing failure file %s', nomFic);
        my_warndlg(str, 1);
        status = 0;
        return
    end
    %             X = fread(fid_sigV, [25 nbRows], 'double');
    
    fclose(fid_sigV);
end

status = 1;
