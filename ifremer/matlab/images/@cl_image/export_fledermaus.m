% Export a Fledermaus .sd file
%
% Syntax
%   flag = export_fledermaus(a, filename, ...)
%
% Input Arguments
%   a        : One instance of cl_image containing an elevation
%   filename : Name of the fledermaus .sd file
%
% Name-Value Pair Arguments
%   subx         : Sub-sampling in abscissa
%   suby         : Sub-sampling in ordinates
%   LayerMapping : One cl_image instance that contains a texture to plot
%                  over the elevation
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     filename = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, filename);
%     a(2) = sunShading(a(1));
%     imagesc(a)
%     nomFicOut = my_tempname('.sd')
%   flag = export_fledermaus(a(1), nomFicOut)
%     winopen(nomFicOut)
%   flag = export_fledermaus(a(1), nomFicOut, 'LayerMapping', a(2))
%     winopen(nomFicOut)
%
%     % For fun
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%   flag = export_fledermaus(a, nomFicOut)
%     winopen(nomFicOut)
%
% See also cl_image/import_fledermaus winopen Authors
% Authors : JMA + GLT
%-------------------------------------------------------------------------

function flag = export_fledermaus(this, filename, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMapping] = getPropertyValue(varargin, 'LayerMapping', []); %#ok<ASGLU>

W = warning;
warning('off', 'all');

% Pr�paration des images
[Z, img, img2, limits] = getImg(this, LayerMapping, suby, subx);
% Ouverture du fichier et �criture
fid = fopen(filename,'wb');
write_main(fid, Z, img, img2, limits);
write_eof(fid)
% on sort
flag = 1;

warning(W)


function [Z, img, img2, limits] = getImg(this, LayerMapping, suby, subx)
% R�cup�ration du MNT et des images (drapp�es ou non)
% La r�solution diff�rente est autoris�e, tant que les limites sont
% communes

%% Intersection des images
if isempty(LayerMapping) && (this.ImageType == 2)
    LayerMapping = this;
    this = RGB2Intensity(this);
end


if ~isempty(LayerMapping)
    
    % Intersection des images
    [~, ~, subx, suby, subcLM, sublLM] = intersectionImagesArnaud(this, subx, suby, LayerMapping);
    
    if (this.ImageType == 2) && (LayerMapping.ImageType ~= 2)
        pppp   = subx;
        subx   = subcLM;
        subcLM = pppp;
        
        pppp   = suby;
        suby   = sublLM;
        sublLM = pppp;
        
        pppp   = this;
        this   = LayerMapping;
        LayerMapping = pppp;
    end
else
    subcLM = subx;
    sublLM = suby;
end
subx = sort(subx);
suby = sort(suby);
subcLM = sort(subcLM);
sublLM = sort(sublLM);

%% MNT
Z = this.Image(suby,subx);
Z = my_transpose(Z);
Z = single(Z);

if (this.XDir == 2) && ((this.x(end)-this.x(1)) > 0)
    flagFlipud = 1;
else
    flagFlipud = 0;
end

if xor((this.YDir == 1), (this.y(end) >  this.y(1)))
    flagFliplr = 1;
else
    flagFliplr = 0;
end

if flagFlipud
    Z = flipud(Z);
end
if flagFliplr
    Z = fliplr(Z);
end

limits(1) = min(this.x(subx)); %xlim(1);
limits(2) = max(this.x(subx));
limits(3) = min(this.y(suby));
limits(4) = max(this.y(suby));
sub = isfinite(Z);
limits(5) = min(Z(sub));
limits(6) = max(Z(sub));

%% Image du MNT

img = this.Image(suby,subx);
img = my_transpose(img);

if flagFlipud
    img = flipud(img);
end
if flagFliplr
    img = fliplr(img);
end

J = zeros([size(img) 3], 'uint8');
nB = 500;
for iBlock=1:nB:size(img,1)
    subBlock = iBlock:min(iBlock-1+nB, size(img,1));
    ZZ = ind2rgb(gray2ind(mat2gray(double(img(subBlock,:)), double(this.CLim)), size(this.Colormap,1)), this.Colormap);
    J(subBlock,:,:) = uint8(floor(single(ZZ)*255));
end
img = J;

%% Image drapp�e

if isempty(LayerMapping)
    img2 = [];
else
    if LayerMapping.nbSlides == 1
        b = Intensity2RGB(LayerMapping, 'subx', subcLM, 'suby', sublLM);
        if b.StatValues.Max <= 1
            Z1 = b.Image(:,:,1) * 255;
            Z2 = b.Image(:,:,2) * 255;
            Z3 = b.Image(:,:,3) * 255;
        else
            Z1 = b.Image(:,:,1);
            Z2 = b.Image(:,:,2);
            Z3 = b.Image(:,:,3);
        end
        img2 = zeros(length(subcLM), length(sublLM), 3, 'uint8');
        img2(:,:,1) = Z1';
        img2(:,:,2) = Z2';
        img2(:,:,3) = Z3';
    else
        Z1 = LayerMapping.Image(sublLM,subcLM,1);
        Z2 = LayerMapping.Image(sublLM,subcLM,2);
        Z3 = LayerMapping.Image(sublLM,subcLM,3);
        if (max(Z1(:)) <= 1) && (max(Z2(:)) <= 1) && (max(Z3(:)) <= 1)
            Z1 = Z1 * 255;
            Z2 = Z2 * 255;
            Z3 = Z3 * 255;
        end
        img2 = zeros(length(subcLM), length(sublLM), 3, 'uint8');
        img2(:,:,1) = Z1';
        img2(:,:,2) = Z2';
        img2(:,:,3) = Z3';
    end
    
    if (LayerMapping.XDir == 2) && ((LayerMapping.x(end)-LayerMapping.x(1)) > 0)
        flagFlipud = 1;
    else
        flagFlipud = 0;
    end
    
    if xor((LayerMapping.YDir == 1), (LayerMapping.y(end) >  LayerMapping.y(1)))
        flagFliplr = 1;
    else
        flagFliplr = 0;
    end
    
    if flagFlipud
        img2 = my_flipud(img2);
    end
    if flagFliplr
        img2 = my_fliplr(img2);
    end
end


function fid = write_main(fid, Z, img, img2, limits)
%% Ecriture d'un fichier ".sd"

fprintf(fid,'%s\n%s\n%s\f\n','%% TDR 2.0 Binary','Created by: SonarScope','%%');

if isempty(img2)
    % Une seule donn�e : SD file basique avec sonarDTM
    write_sonardtm_block(fid)
    write_dtm( fid, Z, limits)
    write_shade( fid, img)
    write_geo( fid, limits)
    
else %elseif
    % drappage : textureDTM SD
    write_texturedtm_block(fid)
    write_shade( fid, img2, 'geoimg') % image drap�e
    write_geo( fid, limits)
    write_dtm( fid, Z, limits)
    write_shade( fid, img)
    write_geo( fid, limits)
    %     else
    %         % A SCENE object
    %         write_scene_block(fid)
    %
    %         write_node_block(fid, 'root', 'Root Node', 'Unknown')
    %             write_geo(fid,'add',limits)
    %             write_alignparent_block(fid)
    %             write_geo(fid,'add',limits)
    %
    %         write_node_block(fid, 'dtm', 'Surface', 'test1.sd')
    %             write_geo(fid,'add',limits)
    %             write_sonardtm_block(fid, tipo)
    %             write_dtm(fid,'add',Z,limits)
    %             write_shade(fid, 'add', img2)
    %             write_geo(fid,'add',limits)
    %         write_sonardtm_atb_block(fid)
    %
    %         write_node_block(fid, 'geoimg', 'Drapped', 'test2.sd')
    %             limits(5:6) = [0 1];
    %             write_geo(fid,'add',limits)
    %             write_geoimg(fid, 'add', img)
    %             write_geo(fid,'add',limits)
    %         write_geoimg_atb_block(fid)
end


function write_sonardtm_block(fid)
%% Ecriture d'un SD_SONARDTM block
fwrite(fid,[10005 2],'integer*4');			% Tag ID, Data Length
fwrite(fid,[0 0 1 1 1 1 (1:18)*0 0 0],'uchar');


function write_texturedtm_block(fid)
%% Ecriture d'un SD_TEXTUREDTM block
fwrite(fid,[10050 0],'integer*4');			% Tag ID, Data Length
fwrite(fid,[0 0 1 1 1 1 (1:18)*0],'uchar');


function write_geo(fid, limits)
%% Ecriture d'un GEOREF block, pour le g�oref�rencement des donn�es
fwrite(fid, [15000 100],            'integer*4');     % Tag ID, Data Length
fwrite(fid, [0 0 1 1 1 1 (1:30)*0], 'uchar');
fwrite(fid, limits,                 'real*8');        % Bornes g�ographiques
fwrite(fid, (1:10)*0,               'integer*4');


function write_dtm(fid, Z, limits)
%% Ecriture d'un DTM block
[m,n] = size(Z);
fwrite(fid, [1000 m*n*2+30],        'integer*4');		% Tag ID, Data Length
fwrite(fid, [0 0 1 1 1 2 (1:18)*0], 'uchar');
fwrite(fid, [2 m n 3 16],           'integer*2');		% nDim(?) nCols nRows ?? BitWidth
fwrite(fid, [limits(5) limits(6)],  'real*8');
fwrite(fid, [0 65535],              'uint16');
Z = scale2uint16(Z, limits(5), limits(6));
fwrite(fid, Z, 'uint16');


function write_shade(fid, img, geoimg)
%% Ecriture d'un SHADE bock ou GEOIMAGE block

if nargin == 2 		% Non geoimage
    soma = 34;
    nZeros = 10;
    bW = [7 32];
    nB = 65535;
elseif strcmp(geoimg,'geoimg')
    soma = 28;
    nZeros = 7;
    bW = [12 8];
    nB = 255;		% geoimage object
else
    return
end

m = size(img,1);
n = size(img,2);

fwrite(fid, [1000 m*n*4+soma],      'integer*4');		% Tag ID, Data Length
fwrite(fid, [0 0 1 1 1 2 (1:18)*0], 'uchar');
fwrite(fid, [2 m n bW],             'integer*2');		% nDim(?) nCols nRows ?? BitWidth
fwrite(fid, [(1:nZeros)*0 nB nB],   'integer*2');

img(:,:,4) = uint8(255);                                % Tranparency layer
img = permute(img,[4 3 1 2]);
fwrite(fid, img,                    'uint8');


function write_eof(fid)
%% Ecriture d'un EOF (EndOfFile) block
fwrite(fid,[999999999 0],'integer*4');
fwrite(fid,[0 0 1 1 1 1 (1:18)*0],'uchar');
fclose(fid);


function zout = scale2uint16(z, zmin, zmax)
%% Mise � l'�chelle d'un variable : double -> uint16

new_range = double(intmax('uint16')); %65534;
add_off   = 0; %1

a = (zmax - zmin) / new_range;
b = zmin;

zout = (z - b) / a;
zout(zout < 0) = 0;
zout(zout > new_range) = new_range;

zout = uint16(floor(zout) + add_off);

