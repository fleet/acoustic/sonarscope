% TODO : supprimer cette fonction et appeler export_tif � la place : pr�voir de conditionner l'image en amont.

function [flag, nomFic, mmin, mmax, XLim, YLim] = export_geoTiff(this, varargin)


[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Type]      = getPropertyValue(varargin, 'Type',     'short');
[varargin, NbBand]    = getPropertyValue(varargin, 'NbBand',   1);
[varargin, nomFic]    = getPropertyValue(varargin, 'nomFic',   'myfile.tif');
[varargin, newDir]    = getPropertyValue(varargin, 'newDir',   '');
[varargin, repExport] = getPropertyValue(varargin, 'repExport', my_tempdir);
[varargin, nbChanels] = getPropertyValue(varargin, 'nbChanels', NaN);
[varargin, NoNan]     = getPropertyValue(varargin, 'NoNan',     false);
[varargin, Carto]     = getPropertyValue(varargin, 'Carto',     []); %#ok<ASGLU>

mmin = [];
mmax = [];
XLim = [];
YLim = [];

newDir = fullfile(repExport, newDir);
if ~exist(newDir, 'dir')
    mkdir(newDir);
end
nomFic = fullfile(newDir, nomFic);
flag = testEcritureFichier(nomFic);
if ~flag
    return
end
isRGB = 0;
if isnan(nbChanels)
    if this.ImageType == 2
        isRGB       = 1;
        nbChanels   = 3;
    else
        nbChanels = this.nbSlides;
    end
end

% Le Type d�signe le type de sortie dans l'image GeoTIFF elle-m�me.
switch Type
    case {'short'; 'int16'}
        format = 'int16';
        nonvalue = intmin('int16');
    case 'int32'
        format = 'int32';
        nonvalue = intmin('int32');
    case 'single'
        format = 'single';
        nonvalue = -32768; %realmin('single');
    otherwise
        % On conserve le type des donn�es (y compris Double pour �crire les
        % donn�es Temps par exemple).
        format = class(this.Image(1));
        if isinteger(this.Image(1))
            nonvalue = intmin(format);
        else
            nonvalue = realmin(format);
        end
end

if isRGB
    format   = 'uint8';
    nonvalue = intmin('uint8');
end

try
    I = zeros(length(suby), length(subx), nbChanels, format);
catch %#ok<CTCH>
    I = cl_memmapfile('Value', 0, 'Size', [length(suby), length(subx), nbChanels], 'Format', format);
end
N = ceil(5e8 / (length(subx) * length(suby)));

% Pour le layer RxTime : passage des param�tres subx et suby pour reforcer le calcul des Min et
% Max. StatVlues ne renvoyant que des Single.
mmin = min(this, 'subx', subx, 'suby', suby);
mmax = max(this, 'subx', subx, 'suby', suby);

for k=1:N:length(suby)
    sub = k:min(k+N-1, length(suby));
    X   = this.Image(suby(sub), subx, :);
    I(sub,:,:) = X;
end

switch format
    case {'int16'; 'uint16'; 'int32'; 'uint32'; 'int8'; 'uint8'}
        if isnan(this.ValNaN)
            subNaN = isnan(this.Image(suby, subx, :));
        else
            subNaN = this.Image(suby, subx, :) == this.ValNaN;
        end
        subNaN = (sum(subNaN,this.nbSlides) == this.nbSlides);
        % subNaN = isnan(this.Image(suby, subx, :));
        if nbChanels == 1
            I(I == 0) = 1;
            I(subNaN) = 0;
        else
            % Cas d'une image RGB. Voir avec JMA pour optimiser les lignes
            % suivantes.
            %             IR = I(:,:,1);
            %             IG = I(:,:,2);
            %             IB = I(:,:,3);
            %             subZero = (IR == 0 & IG == 0 & IB == 0);
            %             IR(subZero) = 1;
            %             IG(subZero) = 1;
            %             IB(subZero) = 1;
            %             IR(subNaN) = 0;
            %             IG(subNaN) = 0;
            %             IB(subNaN) = 0;
            %             nonvalue = -32768;
            %             I(:,:,1) = IR;
            %             I(:,:,2) = IG;
            %             I(:,:,3) = IB;
        end
end

%% Retournement de l'image si besoin
% Ca serait plus judicieux de mettre ce traitement dans la boucle de
% constitution de I

xDir = ((this.x(end) - this.x(1)) > 0);
if ~xDir
    I = my_fliplr(I);
end

yDir = ((this.y(end) - this.y(1)) > 0);
if ~xor(yDir , (this.YDir == 1))
    I = my_flipud(I);
end

%% Cr�ation de l'objet GeoTIFF

% !!!!! Pour l'instant, valable pour des donn�es sur une bande (
% SamplesPerPixel = 1, SampleFormat=3 (Float), Photometric = Separated).

t = Tiff(nomFic, 'w');

tagstruct.ImageLength = size(I,1);
tagstruct.ImageWidth  = size(I,2);

if NbBand > 1
    % en RGB - Float 32
    tagstruct.Photometric     = Tiff.Photometric.RGB;
    tagstruct.SampleFormat    = Tiff.SampleFormat.IEEEFP;
    tagstruct.BitsPerSample   = 32;
    tagstruct.SamplesPerPixel = 3;
    pppp        = I;
    pppp(:,:,2) = I;
    pppp(:,:,3) = I;
    I = pppp;
else
    if strcmp(Type, 'short') || strcmp(Type, 'int16')
        tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
        tagstruct.SampleFormat    = Tiff.SampleFormat.Int;
        tagstruct.BitsPerSample   = 16;
        tagstruct.SamplesPerPixel = 1;
        I = int16(I);
    elseif strcmp(Type, 'int32')
        tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
        tagstruct.SampleFormat    = Tiff.SampleFormat.Int;
        tagstruct.BitsPerSample   = 32;
        tagstruct.SamplesPerPixel = 1;
        I = int32(I);
    elseif strcmp(Type, 'char')
        tagstruct.Photometric     = Tiff.Photometric.Separated;
        tagstruct.SampleFormat    = Tiff.SampleFormat.UInt;
        tagstruct.BitsPerSample   = 8;
        tagstruct.SamplesPerPixel = 1;
        I = uint8((I-min(min(I)))/100);% !!!! arbitraire
        subNaN    = isnan(I);
        I(subNaN) = 0;
    else
        tagstruct.Photometric         = Tiff.Photometric.MinIsBlack;
        if strcmp(format, 'uint8')
            tagstruct.SampleFormat    = Tiff.SampleFormat.UInt;
            tagstruct.SamplesPerPixel = size(I,3);
            tagstruct.BitsPerSample   = 8;
        elseif strcmp(format, 'single')
            tagstruct.SampleFormat    = Tiff.SampleFormat.IEEEFP;
            tagstruct.SamplesPerPixel = size(I,3);
            tagstruct.BitsPerSample   = 32;
            I = single(I);
        elseif strcmp(format, 'double')
            tagstruct.SampleFormat    = Tiff.SampleFormat.IEEEFP;
            tagstruct.SamplesPerPixel = size(I,3);
            tagstruct.BitsPerSample   = 64;
            I = double(I);
        end
    end
end

% Incompatible du Float : tagstruct.RowsPerStrip = 16;
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
% tagstruct.Orientation       = Tiff.Orientation.TopLeft;
tagstruct.RowsPerStrip = 100; % 10000000;
t.setTag(tagstruct);
% if ~isdeployed
%     disp(t)
% end

str1 = 'Erreur d''�criture du fichier geotiff. Assurez vous que vous avez les droits d''�criture et qu''il vous reste suffisemment d''espace avant de recommencer l''op�ration.';
str2 = 'Write failure. Be sure that you have the rights to write and that you have sufficient space left on your drave befor retempting the operation.';
if NoNan
    subNaN = isnan(I);
    I2 = I;
    I2(subNaN) = -32768; %missing value de l'elevation nasaworldwind
    try
        t.write(I2);
    catch
        my_warndlg(Lang(str1,str2), 1);
    end
else
    try
        t.write(I);
    catch
        my_warndlg(Lang(str1,str2), 1);
    end
end
t.close;

% R�cup�ration du chemin de l'�x�cutable
GDALtranslate = 'gdal_translate.exe';

% En 64 bits, on installe GDAL depuis un paquet Core msi
% avec le SetUp External.
pathGDAL = getenv('GDAL_HOME');
if isempty(pathGDAL)
    str1 = 'GDAL n''est pas install� ou son r�f�rencement dans le path n''est pas fait. Refaites l''installation de GDAL � partir du setup external et suivez le instructions ad-hoc si un probl�me de path est d�tect�. Un tutorial sur le sujet est disponible sur la forge dans "Docs", "Tutorials", "Installation" : https://forge.ifremer.fr/docman/?group_id=126&view=listfile&dirid=542&feedback=Document+submitted+successfully';
    str2 = 'GDAL is not installed or its referencement in the path is not done. Redo the GDAL installation through the External setup and observe the instructions about a path troubleshoot if ever. A tutorial exists in the forge in "Docs", "Tutorials", "Installation" : https://forge.ifremer.fr/docman/?group_id=126&view=listfile&dirid=542&feedback=Document+submitted+successfully';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

% R�cuparation des param�tres
x = get(this, 'x');
y = get(this, 'y');
XLim = [min(x(subx)) max(x(subx))];
if all(XLim > 180)
    XLim = XLim - 360;
end

YLim = [min(y(suby)) max(y(suby))];
bounds = [XLim(1) YLim(2) XLim(2) YLim(1)]; % [UpperLeftX UpperLeftY LowerRightX LowerRightY]

if isempty(Carto)
    % TODO : provoque un warning
    Carto = get(this, 'Carto');
%     cartoDef = struct(Carto);
    cartoDef = Carto;
else
    cartoDef = Carto;
end

if isempty(cartoDef)
    str1 = 'D�sol�, les param�tres cartographiques ne sont pas d�finis pour cette image. Vous pouvez les red�finir en utilisant le menu "Info"';
    str2 = 'Sorry, the cartographic parameters are not defined for this image. You can do it now using menu "Info", "Set Current Image", "Cartographic parameters"';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
datum      = cartoDef.Ellipsoide.strType{cartoDef.Ellipsoide.Type};
projection = cartoDef.Projection.strType{cartoDef.Projection.Type};

if this.GeometryType == cl_image.indGeometryType('LatLong')
    projection = 'Geodetic';
end


% TODO : Pourquoi a t'on besoin de ces param�tres si on est en "LatLong" ?


% D�finition Proj.4 de la projection
switch projection
    case 'Geodetic'
        carto = sprintf('-a_srs "+datum=%s +proj=latlong"', datum);
        
    case 'Mercator'
        % NOTE : PROJ.4 does not support a latitude of natural origin other than the equator. !!!
        % Donc on passe en WKT, qui lui, supporte ce param�tre, via un fichier externe
        pathWKT = fullfile( fileparts(nomFic), 'proj.wkt' );
        fidProj = fopen(pathWKT, 'wt');
        fprintf(fidProj, 'PROJCS["%s_World_Mercator",GEOGCS["%s",DATUM["%s",SPHEROID["%s",%f,%f]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433]],PROJECTION["Mercator_1SP"],PARAMETER["latitude_of_origin",%f],PARAMETER["central_meridian",%f],PARAMETER["scale_factor",1],PARAMETER["false_easting",%f],PARAMETER["false_northing",%f],UNIT["metre",1]]',...
            datum,...
            datum,...
            datum,...
            datum,...
            cartoDef.Ellipsoide.DemiGrandAxe,...
            1 / ( 1 - sqrt( 1-cartoDef.Ellipsoide.Excentricite^2 ) ),...
            cartoDef.Projection.(projection).lat_ech_cons,...
            cartoDef.Projection.(projection).long_merid_orig,...
            cartoDef.Projection.(projection).X0,...
            cartoDef.Projection.(projection).Y0);
        fclose(fidProj);
        carto = sprintf('-a_srs "%s"', pathWKT);
        
    case 'UTM'
        carto = sprintf(...
            '-a_srs "+datum=%s +proj=tmerc +lat_0=%f +lon_0=%f +k=%f +x_0=%f +y_0=%f"',...
            datum,...
            0,...                                                   % Latitude of natural origin
            cartoDef.Projection.(projection).Central_meridian,...   % Longitude of natural origin
            0.9996,...                                              % ??? Scale factor at natural origin
            cartoDef.Projection.(projection).X0,...                 % False Easting
            cartoDef.Projection.(projection).Y0);                   % False Northing
        
    case 'Lambert'
        carto = sprintf(...
            '-a_srs "+datum=%s +proj=lcc +lat_1=%f +lat_2=%f +lon_0=%f +x_0=%f +y_0=%f"',...
            datum,...
            cartoDef.Projection.(projection).first_paral,...       % Latitude of 1st standard parallel
            cartoDef.Projection.(projection).second_paral,...       % Latitude of 2nd standard parallel
            cartoDef.Projection.(projection).long_merid_orig,...    % Longitude of false origin
            cartoDef.Projection.(projection).X0,...                 % False Origin Easting
            cartoDef.Projection.(projection).Y0);                   % False Origin Northing
        % +lat_0=%f 0,...  % % ??? Latitude of false origin
        
    case 'Stereographique polaire'
        if strcmpi(cartoDef.Projection.(projection).hemisphere, 'N')
            sign = 1;
        else
            sign = -1;
        end
        carto = sprintf(...
            '-a_srs "+datum=%s +proj=stere +lat_ts=%f +lat_0=%f +lon_0=%f +k_0=1 +x_0=0 +y_0=0"',...
            datum,...
            cartoDef.Projection.(projection).lat_ech_cons,...
            sign*90,...
            cartoDef.Projection.(projection).long_merid_horiz);
        
    otherwise %'other'
        my_warndlg(sprintf('Error Gdal_translate :\n\n%s','Unknown Projection'),  1);
        return
end

frmt = 'GTiff';
[pathstr, name, ext] = fileparts(nomFic);
tempFic = fullfile(pathstr, [name '_Tmp' ext]);

try
    copyfile(nomFic, tempFic); % Copie du fichier d'origine, car origine et modifi� doivent �tre diff�rents
catch ME
    if length(tempFic) > 247
        str1 = sprintf('Attention, la longueur du fichier "%s" d�passe les capacit�s des commandes DOS, la commande "copyfile"  a bugu�.', tempFic);
        str2 = sprintf('WARNING, the lenght of file "%s" is too long, the DOS command "copyfile" failed.', tempFic);
        my_warndlg(Lang(str1,str2), 0, 'displayItEvenIfSSc', 1);
    else
        my_warndlg([ME.identifier ' : ' ME.message], 1);
    end
    flag = 0;
    return
end
pppp = pwd;

try
    cd(pathGDAL)
catch %#ok<CTCH>
    str1 = 'GDAL n''est pas install� ou son r�f�rencement dans le path n''est pas fait. Refaites l''installation de GDAL � partir du setup 3xternal et suivez le instructions ad-hoc si un probl�me de path est d�tect�.';
    str2 = 'GDAL is not installed or its referencement in the path is not done. Redo the GDAL installation through the External setup and observe the instructions about a path troubleshoot if ever.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

% pause(1);
cmd = sprintf('%s -of %s -a_nodata %s -a_srs EPSG:4326 -a_ullr %s %s "%s" "%s"',...
    GDALtranslate, frmt, num2strPrecis(nonvalue), num2strPrecis(bounds), carto, tempFic, nomFic);
    
cmd = regexprep(cmd, 'Program Files', '"Program Files"');
[status, result] = system(cmd, '-echo'); %#ok<ASGLU>
pause(1);

if status
    % Rajout� le 14/02/2012 : �a semble porter ses fruits (pb export image
    % satellite de Wellington)
    % [status, result] = dos(cmd);
    [status, result] = system(cmd, '-echo');
    pause(1)
    if status
        str1 = sprintf('Probl�me dans la g�n�ration de la dalle GeoTIFF : %s\n%s', nomFic, result);
        str2 = sprintf('Creating GEOTIFF tile has failed for "%s"\n%s', nomFic, result);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end

% % % % [status, result] = system(cmd); %#ok<ASGLU,NASGU>
% Pause n�cessaire avant effacement du fichier sinon commande GDAL perdue.
% % % % pause(0.5);
delete(tempFic);    % Effacement du fichier d'origine
if exist('pathWKT', 'var') && exist(pathWKT, 'file') % Effacement du fichier de g�od�sie WKT s'il existe
    delete(pathWKT);
end

cd(pppp);
