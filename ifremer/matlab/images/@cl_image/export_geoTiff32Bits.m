% Ecriture d'une image dans un fichier g�otif uniquement en r�el 32bits
%
% Syntax
%   export_geoTiff32Bits(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   CLim : Rehaussement de contraste
%
% Examples
%
% See also cl_image Authors
% Authors : GLT
% ----------------------------------------------------------------------------

function flag = export_geoTiff32Bits(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Check] = getPropertyValue(varargin, 'Check', 1);
[varargin, Mute]  = getPropertyValue(varargin, 'Mute',  0); %#ok<ASGLU>

stat = this.StatValues; % on r�cup�re les stats pour les passer � GDAL

if Check
    flag = testEcritureFichier(nomFic);
    if ~flag
        return
    end
end

%%  Appel de la fonction d'ecriture

if this.ImageType == 2
    nbChanels = 3;
else
    nbChanels = this.nbSlides;
end

try
    I = zeros(length(suby), length(subx), nbChanels, class(this.Image(1)));
catch %#ok<CTCH>
    I = cl_memmapfile('Value', 0, 'Size', [length(suby), length(subx), nbChanels], 'Format', class(this.Image(1)));
end

N = ceil(5e8 / (length(subx) * length(suby)));
ValeurPresente = false;
for k=1:N:length(suby)
    sub = k:min(k+N-1, length(suby));
    X = this.Image(suby(sub), subx, :);
    if ~ValeurPresente
        if isnan(this.ValNaN)
            if any(~isnan(X(:)))
                ValeurPresente = true;
            end
        else
            if any(X(:) ~= this.ValNaN)
                ValeurPresente = true;
            end
        end
    end
    I(sub,:,:) = X;
end
if ~ValeurPresente
    % Modif JMA le 02/07/2011 pour �viter de sauver des images qui ne contiennent pas de donn�es
    % Ca peut peut-�tre perturber certaines applications (Arnaud ???) Sinon, mette un PropertyName/PropertyValue pour bypasser ce test pour ces applications l�.
    str1 = sprintf('Fichier "%s" non cr�� car tous ses pixels sont NaN.', nomFic);
    str2 = sprintf('File "%s" not created because all the pixels are NaN.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImageTifNonSauveeCarVide');
    flag = 0;
    return
end

xDir = ((this.x(end) - this.x(1)) > 0);
yDir = ((this.y(end) - this.y(1)) > 0);

if xor(xDir , (this.XDir == 1))
    I = fliplr(I);
end
if ~xor(yDir , (this.YDir == 1))
    I = flipud(I);
end

% Comment� par JMA le 16/04/2018 pour images 32 bits de WC : possible effet
% de bord pour d'autres images ???
valNaN = this.ValNaN;

MinVal = min(I(:), [], 'omitnan');
MaxVal = max(I(:), [], 'omitnan');
% if isnan(valNaN)
%     valNaN = ceil(MaxVal) + 1;
%     I(isnan(I)) = valNaN;
% end


% %% Colormap -> Impossible, ne marche pas pour des valeurs r�elles,
% dommage !
% Map     = this.Colormap;
% nbCoul  = size(Map, 1);
% subc    = linspace(1, nbCoul, 4294967296);
%
% Colormap(:, 1) = interp1(1:nbCoul, Map(:,1), subc)';
% Colormap(:, 2) = interp1(1:nbCoul, Map(:,2), subc)';
% Colormap(:, 3) = interp1(1:nbCoul, Map(:,3), subc)';

%% Cr�ation de l'objet Tiff

export_ImageTif32bits(I, nomFic, 'MinVal', MinVal, 'MaxVal', MaxVal, 'Mute', Mute);

%% G�or�f�rencement

if (this.GeometryType == cl_image.indGeometryType('GeoYX')) || (this.GeometryType == cl_image.indGeometryType('LatLong'))
    nodata_value = sprintf('-a_nodata "%d"', valNaN);
    [~, ~, Ext] = fileparts(nomFic);
    switch Ext
        % Si Tiff : conversion en GeoTiff avec Gdal_translate.exe dans
        % 'C:\IfremerToolbox_Rxxxx\ifremer\extern\gdal\'
        case '.tif'
            %% R�cup�ration du chemin de l'�x�cutable
            GDALtranslate = 'gdal_translate.exe';
            % En 64 bits, on installe GDAL depuis un paquet Core msi
            % avec le SetUp External.
            pathGDAL      = getenv('GDAL_HOME');
            GDALtranslate = fullfile(pathGDAL, GDALtranslate);
            
            if ~exist(GDALtranslate, 'file')
                str1 = sprintf('Le fichier "%s" n''a pas �t� trouv� sur votre ordinateur, if vous faut r�cup�rer le dernier "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" qui vous installera ce composant.', GDALtranslate);
                str2 = sprintf('File "%s" not found, you should download and install the last "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" that will install this component.', GDALtranslate);
                my_warndlg(Lang(str1,str2), 1);
            end
            
            %             GDALtranslate = '"C:\Program Files\GDAL\gdal_translate.exe"'; % A l'essai
            
            % R�cuparation des param�tres
            Xlim = this.XLim;
            Ylim = this.YLim;
            bounds = [Xlim(1) Ylim(2) Xlim(2) Ylim(1)]; % [UpperLeftX UpperLeftY LowerRightX LowerRightY]
            
            Carto = this.Carto;
            if isempty(Carto)
                str1 = 'Cette image n''a pas de cartographie associ�e.';
                str2 = 'This image has no associated cartography.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            datum      = Carto.Ellipsoide.strType{Carto.Ellipsoide.Type};
            projection = Carto.Projection.strType{Carto.Projection.Type};
            
            %% Test de coh�rence "pr�f carto"/g�ometrie
            
            switch projection
                case 'Geodetic'
                    if this.GeometryType == cl_image.indGeometryType('GeoYX') % A priori ce cas n'arrive jamais
                        str1 = sprintf('Export tiff r�ussi, mais... impossible de convertir Tiff --> GeoTiff\n\nIncoh�rence entre g�om�trie de l''image et pr�f�rence cartographiques :\nG�om�trie : %s\nG�od�sie :%s, %s\n\n>> Veuillez d�projeter l''image avant l''export', 'GeoYX', datum, projection);
                        str2 = sprintf('Tiff export successfull, but... cannot convert Tiff --> GeoTiff\n\nImage geometry and cartographic preferences are incoherent :\nGeometry : %s\nGeodesy : %s, %s\n\n>> Unproject image before exporting', 'GeoYX', datum, projection);
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
                    
                case {'Mercator', 'UTM', 'Lambert', 'Stereographique polaire'}
                    if this.GeometryType == cl_image.indGeometryType('LatLong')
                        projection = 'Geodetic'; % Implicitement, l'export se fait en Geodetic
                    end
            end
            
            %% D�finition Proj.4 de la projection
            
            switch projection
                case 'Geodetic'
                    carto = sprintf('-a_srs "+datum=%s +proj=latlong"', datum);
                    
                case 'Mercator'
                    % NOTE : PROJ.4 does not support a latitude of natural origin other than the equator. !!!
                    % Donc on passe en WKT, qui lui, supporte ce param�tre, via un fichier externe
                    
                    pathWKT = fullfile(fileparts(nomFic), 'proj.wkt' );
                    fidProj = fopen(pathWKT, 'wt');
                    fprintf(fidProj, 'PROJCS["%s_World_Mercator",GEOGCS["%s",DATUM["%s",SPHEROID["%s",%f,%f]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433]],PROJECTION["Mercator_1SP"],PARAMETER["latitude_of_origin",%f],PARAMETER["central_meridian",%f],PARAMETER["scale_factor",1],PARAMETER["false_easting",%f],PARAMETER["false_northing",%f],UNIT["metre",1]]', ...
                        datum, ...
                        datum, ...
                        datum, ...
                        datum, ...
                        Carto.Ellipsoide.DemiGrandAxe, ...
                        1 / ( 1 - sqrt(1 - Carto.Ellipsoide.Excentricite ^ 2)), ...
                        Carto.Projection.Mercator.lat_ech_cons, ...
                        Carto.Projection.Mercator.long_merid_orig, ...
                        Carto.Projection.Mercator.X0, ...
                        Carto.Projection.Mercator.Y0);
                    fclose(fidProj);
                    carto = sprintf('-a_srs "%s"', pathWKT);
                    
                case 'UTM'
                    carto = sprintf(...
                        '-a_srs "+datum=%s +proj=tmerc +lat_0=%f +lon_0=%f +k=%f +x_0=%f +y_0=%f"', ...
                        datum, ...
                        0, ...                                                 % Latitude of natural origin
                        Carto.Projection.UTM.Central_meridian, ... % Longitude of natural origin
                        0.9996, ...                                            % ??? Scale factor at natural origin
                        Carto.Projection.UTM.X0, ...               % False Easting
                        Carto.Projection.UTM.Y0);                  % False Northing
                    
                case 'Lambert'
                    diffParal = Carto.Projection.Lambert.first_paral - Carto.Projection.Lambert.second_paral;
                    if diffParal < eps(diffParal)
                        % Si les parall�les sont identiques, les param�tres
                        % sont en mode s�cants
                        carto = sprintf(...
                            '-a_srs "+datum=%s +proj=lcc +lat_1=%f +lat_0=%f +k_0=%f +lon_0=%f +x_0=%f +y_0=%f"',...
                            datum,...
                            Carto.Projection.Lambert.first_paral,...     % Latitude of 1st standard parallel
                            Carto.Projection.Lambert.second_paral,...    % Latitude of 2nd standard parallel
                            Carto.Projection.Lambert.scale_factor,...    % Latitude of 2nd standard parallel
                            Carto.Projection.Lambert.long_merid_orig,... % Longitude of false origin
                            Carto.Projection.Lambert.X0,...              % False Origin Easting
                            Carto.Projection.Lambert.Y0);                % False Origin Northing
                        
                    else
                        % Si les parall�les sont diff�rents, les param�tres sont en mode tangents
                        carto = sprintf(...
                            '-a_srs "+datum=%s +proj=lcc +lat_1=%f +lat_2=%f +lon_0=%f +x_0=%f +y_0=%f"',...
                            datum,...
                            Carto.Projection.Lambert.first_paral,...     % Latitude of 1st standard parallel
                            Carto.Projection.Lambert.second_paral,...    % Latitude of 2nd standard parallel
                            Carto.Projection.Lambert.long_merid_orig,... % Longitude of false origin
                            Carto.Projection.Lambert.X0,...              % False Origin Easting
                            Carto.Projection.Lambert.Y0);                % False Origin Northing
                    end
                    
                    % +lat_0=%f 0,...  % % ??? Latitude of false origin
                    
                case 'Stereo'
                    if strcmpi(Carto.Projection.Stereo.hemisphere, 'N')
                        sign = 1;
                    else
                        sign = -1;
                    end
                    carto = sprintf(...
                        '-a_srs "+datum=%s +proj=stere +lat_ts=%f +lat_0=%f +lon_0=%f +k_0=1 +x_0=0 +y_0=0"', ...
                        datum, ...
                        Carto.Projection.Stereo.lat_ech_cons, ...
                        sign*90, ...
                        Carto.Projection.Stereo.long_merid_horiz);
                    
                otherwise %'other'
                    my_warndlg(sprintf('Error Gdal_translate :\n\n%s','Unknown Projection'),  1);
                    return
            end
            
            %% Conversion GeoTiff
            
            frmt = 'GTiff';
            %nodata_value = 0;
            
            [pathstr, name, ext] = fileparts(nomFic);
            tempFic = fullfile(pathstr, [name '_Temp' ext]);
            copyfile(nomFic, tempFic, 'f'); % copie du fichier d'origine, car origine et modifi� doivent �tre diff�rents
            
%             cmd = sprintf(...
%                 '%s -q -of %s %s -a_ullr %f %f %f %f %s "%s" "%s" -co "COMPRESS=PackBits" -co "SOFTWARE=SonarScope" -mo "STATISTICS_MINIMUM=%f" -mo "STATISTICS_MAXIMUM=%f" -mo "STATISTICS_MEAN=%f" -mo "STATISTICS_STDDEV=%f"',...
%                 GDALtranslate, frmt, nodata_value, bounds, carto, tempFic, nomFic, stat.Max, stat.Min, stat.Moyenne, sqrt(stat.Variance));
            cmd = sprintf(...
                '%s -q -of %s %s -a_ullr %f %f %f %f %s "%s" "%s" -co "COMPRESS=PackBits" -mo "STATISTICS_MINIMUM=%f" -mo "STATISTICS_MAXIMUM=%f" -mo "STATISTICS_MEAN=%f" -mo "STATISTICS_STDDEV=%f"',...
                GDALtranslate, frmt, nodata_value, bounds, carto, tempFic, nomFic, stat.Max, stat.Min, stat.Moyenne, sqrt(stat.Variance));
            if ~Mute
                fprintf('%s\n', cmd);
            end
            
            cmd = regexprep(cmd, 'Program Files', '"Program Files"');
            [status, result] = system(cmd, '-echo');
            delete(tempFic); % Effacement du fichier d'origine
            if  exist('pathWKT', 'var') && exist(pathWKT, 'file') % Effacement du fichier de g�od�sie WKT s'il existe
                delete(pathWKT);
            end
            
            if status ~= 0
                my_warndlg(sprintf('Error Gdal_translate :\n\n%s', result),  1);
            end
            
        otherwise
            %% Ecriture fichier Worldfile | Modif GLA 2010/05/28
            export_worldfile(this, nomFic, 'subx', subx, 'suby', suby);
    end
end

%% The End

if ~Mute
    str1 = 'L''export du fichier GeoTiff est termin�';
    str2 = 'The export of the GeoTiff image is over.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportGeoTiff_ProcessingIsOver');
end

flag = 1;
