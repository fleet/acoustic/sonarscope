% Ecriture d'une image dans un fichier gif
%
% Syntax
%   export_gif(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   CLim : Rehaussement de contraste
%
% Examples
%   a = cl_image('Image', Lena);
%   nomFic = [tempname '.tif']
%   export_gif(a, nomFic)
%   visuExterne(nomFic)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_gif(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, ESRIWorlfFile] = getPropertyValue(varargin, 'ESRIWorlfFile', 1); %#ok<ASGLU>

% [varargin, CLim] = getPropertyValue(varargin, 'CLim', this.CLim);

flag = testEcritureFichier(nomFic);
if ~flag
    return
end

Map = this.Colormap;
switch this.ImageType
    case 1 % Intensite
        if (this.DataType == cl_image.indDataType('Mask')) || (this.DataType == cl_image.indDataType('Segmentation'))
            I = this.Image(suby,subx);
            if isnan(this.ValNaN)
                I(isnan(I)) = 0;
            else
                I(I == this.ValNaN) = 0;
            end
            I = uint8(I);
            sub = round(linspace(1, size(Map, 1), max(I(:))));
            Map = [1 1 1; Map(sub,:)];
            this.ValNaN = 0;
        else
            [flag, rep] = isEmptyImage(this, 'subx', subx, 'suby', suby);
            if ~flag || rep
                flag = 0;
                return
            end
            [this, flag] = Intensity2Indexed(this, 'subx', subx, 'suby', suby);
            if ~flag
                return
            end
            I = this.Image(:,:);
            subx = 1:length(subx);
            suby = 1:length(suby);
        end
        
    case 2 % RGB
        [flag, rep] = isEmptyImage(this, 'subx', subx, 'suby', suby);
        if ~flag || rep
            flag = 0;
            return
        end
        [this, flag] = RGB2Indexed(this, 'subx', subx, 'suby', suby);
        if ~flag
            return
        end
        I = this.Image(:,:);
        if ~any(I(:))
            flag = 0;
            return
        end
        this.ValNaN = 0;
        Map = this.Colormap;
        subx = 1:length(subx);
        suby = 1:length(suby);
        
    case 3 % Index�e
        [flag, rep] = isEmptyImage(this, 'subx', subx, 'suby', suby);
        if ~flag || rep
            flag = 0;
            return
        end
        I = this.Image(suby,subx);
        I = uint8(I);
        %         I(I == 255) = 254; % Recomment� le 05/03/2017 car bords
        %         echogrammes rouges
        %         Map = Map(2:end,:); % Modifi� par JMA le 21/09/2014 pour images de segmentation
        %         I = I + 1;% Modifi� par JMA le 21/09/2014 pour images
end

% figure(123456); imagesc(I); colormap(gray(256)); colorbar

xDir = ((this.x(end) - this.x(1)) > 0);
yDir = ((this.y(end) - this.y(1)) > 0);

% if ~xor(xDir , (this.XDir == 1))
%     I = fliplr(I);
% end
if ~xDir
    I = fliplr(I);
end

if ~xor(yDir, (this.YDir == 1))
    I = flipud(I);
end
% figure; imagesc(I); colormap(jet(256));


% DEBUT VERUE POUR CONTOURNER UN BUG GoogleEarth
if this.ValNaN == 0
    %     Map((size(Map,1)+1):256,:) = 1; % Comment� le 11/09/2017 pour images segment�es
end
% FIN VERUE POUR CONTOURNER UN BUG GoogleEarth

% figure; image(I(1:4:end,1:4:end)); colormap(this.Colormap); title(nomFic)

% Map(this.ValNaN+1) = 0;

if this.Video == 2
    Map = flipud(Map);
end

if this.ValNaN == 255
    subNaN = (I == 255);
    I = uint16(I) + 1;
    I(subNaN) = 0;
    I = uint8(I);
    this.ValNaN = 0;
    Map = [Map(end,:) ; Map(1:end-1,:)];
elseif (this.ValNaN == 0) && (this.Video == 2)
    Map = [Map(end,:) ; Map(1:end-1,:)];
end

try
    
    % TODO : ATTENTION, rustine plac�e ici pour palier � une saturation dans
    % GLOBE, erreur constat�e par Gay� pour image de panaches du rapport USAN-TOTAL
    % GLU le 10/04/2013 : remise en commentaire pour �viter des pbs �
    % l'export vers Google Earth : coloration parasites de pixels.
    % I(I == 255) = 254;
    
    imwrite(I, Map, nomFic, 'Transparency', this.ValNaN)
catch %#ok<CTCH>
    try
        imwrite(I, double(Map), nomFic)
    catch %#ok<CTCH>
        disp('Echec export_gif')
        whos I
        flag = 0;
    end
end

if ESRIWorlfFile && (this.GeometryType == cl_image.indGeometryType('GeoYX') || this.GeometryType == cl_image.indGeometryType('LatLong'))
    %% Ecriture fichier Worldfile | Modif GLA 2010/05/28
    export_worldfile(this, nomFic, 'subx', subx, 'suby', suby);
end
