% Exportation dans un fichier GMT (*.grd) au format NF
% cf. http://gmt.soest.hawaii.edu/gmt/html/man/grdreformat.html
%
% Syntax
%   flag = export_gmt(a, nomFic)
%
% Input Arguments
%   a      : Instance de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%   nomFicIn = getNomFicDatabase('KBMA0504.sonarmosaic')
%   a = import_poseidon(cl_image, nomFicIn);
%   nomFicIn = getNomFicDatabase('Aslanian.grd')
%   a = import_gmt(cl_image, nomFicIn);
%   imagesc(a)
%
%   nomFic = my_tempname('.grd')
%   flag = export_gmt(a, nomFic);
%
% See also cl_image/import_gmt cl_image/plotGMT Authors
% Authors : JMA
%--------------------------------------------------------------------------

function flag = export_gmt(this, nomFic, varargin)

[varargin, versGMT]    = getPropertyValue(varargin, 'GMTVer', 6); 
[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

[nomDir, nomFic] = fileparts(nomFic);
nomFic = fullfile(nomDir, [nomFic '.grd']);

% '----------------------------------------------'
% Titre = this.Name
% sens_suby = (suby(2) > suby(1))
% sens_y = (this.y(2) > this.y(1))
% YDir = this.YDir
%
% % if sens_y == -1
% %     suby = fliplr(suby);
% % end

CLim = this.CLim;
switch this.ImageType
    case 1  % Intensite
        z = this.Image(suby,subx);
        flag = export_gmt_z(this, nomFic, subx, suby, CLim, z, versGMT);
        
    case 2  % RGB
        ValMax = this.StatValues.Max;
        if ValMax == 0
            ValNoir = 255;
        elseif ValMax <= 1
            ValNoir = 1;
        else
            ValNoir = 255;
        end
        
        z = this.Image(suby,subx,1);
        if ValNoir == 1
            z = z * 255;
        end
        z(isnan(z)) = 0; % Rajout� le 09/01/2008 pour EarthParErViewer.ers
        CLim = [0 255];
        [nomDir, Name, ext] = fileparts(nomFic);
        Name = fullfile(nomDir, [Name '_r' ext]);
        flag = export_gmt_z(this, Name, subx, suby, CLim, z, versGMT);
        if ~flag
            return
        end
        
        z = this.Image(suby,subx,2);
        if ValNoir == 1
            z = z * 255;
        end
        z(isnan(z)) = 0; % Rajout� le 09/01/2008 pour EarthParErViewer.ers
        CLim = [0 255];
        [nomDir, Name, ext] = fileparts(nomFic);
        Name = fullfile(nomDir, [Name '_g' ext]);
        flag = export_gmt_z(this, Name, subx, suby, CLim, z, versGMT);
        if ~flag
            return
        end
        
        z = this.Image(suby,subx,3);
        if ValNoir == 1
            z = z * 255;
        end
        z(isnan(z)) = 0; % Rajout� le 09/01/2008 pour EarthParErViewer.ers
        CLim = [0 255];
        [nomDir, Name, ext] = fileparts(nomFic);
        Name = fullfile(nomDir, [Name '_b' ext]);
        flag = export_gmt_z(this, Name, subx, suby, CLim, z, versGMT);
        
    case 3  % Indexee
        z = this.Image(suby,subx);
        flag = export_gmt_z(this, nomFic, subx, suby, CLim, z, versGMT);
        
    case 4  % Binaire
        z = this.Image(suby,subx);
        flag = export_gmt_z(this, nomFic, subx, suby, CLim, z, versGMT);
end


function flag = export_gmt_z(this, nomFic, subx, suby, CLim, z, versGMT)

if exist(nomFic, 'file')
    try
        W = warning;
        warning('off')
        delete(nomFic)
        warning(W)
    catch ME
        my_warndlg(ME.message, 0);
    end
end

nbRows = length(suby);
nbColumns = length(subx);

sk.att(1).type   = 'NC_CHAR';
sk.att(1).name   = 'title';
sk.att(1).value = '\0';

sk.att(2).type   = 'NC_CHAR';
sk.att(2).name   = 'source';
sk.att(2).value = this.Name;

sk.dim(1).name  = 'x';
sk.dim(1).value = nbColumns;

sk.dim(2).name  = 'y';
sk.dim(2).value = nbRows;

sk.var(1).name  = 'x';
sk.var(1).type  = 'NC_DOUBLE';
sk.var(1).dim(1).name  = 'x'; 	% 2

sk.var(1).att(1).name  = 'asbcisses';
sk.var(1).att(1).type  = 'NC_CHAR';
sk.var(1).att(1).value = 'user_x_unit\0';

sk.var(2).name  = 'y';
sk.var(2).type  = 'NC_DOUBLE';
sk.var(2).dim(1).name  = 'y'; 	% 2

sk.var(2).att(1).name  = 'ordonnes';
sk.var(2).att(1).type  = 'NC_CHAR';
sk.var(2).att(1).value = 'user_y_unit\0';

sk.var(3).name  = 'z';
sk.var(3).type  = 'NC_DOUBLE';
sk.var(3).dim(1).name  = 'x'; 	% 2
sk.var(3).dim(2).name  = 'y'; 	% 2

sk.var(3).att(1).name  = 'scale_factor';
sk.var(3).att(1).type  = 'NC_DOUBLE';
sk.var(3).att(1).value = 1;

sk.var(3).att(2).name  = 'add_offset';
sk.var(3).att(2).type  = 'NC_DOUBLE';
sk.var(3).att(2).value = 0;

sk.var(3).att(3).name  = 'node_offset';
sk.var(3).att(3).type  = 'NC_DOUBLE';
sk.var(3).att(3).value = 0;

a = cl_netcdf('fileName', nomFic, 'skeleton', sk);
a = set_value(a, 'x', double(this.x(subx)));
a = set_value(a, 'y', double(this.y(suby)));
a = set_value(a, 'z', double(z));

if this.y(end) > this.y(1)
    z = flipud(z);
end

z(z <= CLim(1)) = CLim(1);
z(z >= CLim(2)) = CLim(2);

if this.Video == 2
    z = (CLim(2) + CLim(1)) - z;
end
% Flip inutile � partir de la version 6 de GMT
if versGMT < 6
    z = flipud(z);
end
a = set_value(a, 'z', z);%#ok

flag = 1;

