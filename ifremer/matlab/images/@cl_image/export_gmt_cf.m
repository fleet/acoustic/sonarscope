% Exportation dans un fichier GMT (*.grd) au format CF.
% cf. http://gmt.soest.hawaii.edu/gmt/html/man/grdreformat.html
%
% Syntax
%   flag = export_gmt(a, nomFic)
%
% Input Arguments
%   a      : Instance de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%   nomFicIn = getNomFicDatabase('KBMA0504.sonarmosaic')
%   a = import_poseidon(cl_image, nomFicIn);
%   nomFicIn = getNomFicDatabase('Aslanian.grd')
%   a = import_gmt(cl_image, nomFicIn);
%   imagesc(a)
%
%   nomFic = my_tempname('.grd')
%   flag = export_gmt_cf(a, nomFic);
%
% See also cl_image/import_gmt cl_image/plotGMT Authors
% Authors : JMA
%--------------------------------------------------------------------------

function flag = export_gmt_cf(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

[nomDir, nomFic] = fileparts(nomFic);
nomFic = fullfile(nomDir, [nomFic '.grd']);

% '----------------------------------------------'
% Titre = this.Name
% sens_suby = (suby(2) > suby(1))
% sens_y = (this.y(2) > this.y(1))
% YDir = this.YDir
%
% % if sens_y == -1
% %     suby = fliplr(suby);
% % end

CLim = this.CLim;
switch this.ImageType
    case 1  % Intensite
        z = this.Image(suby,subx);
        flag = export_gmt_z(this, nomFic, subx, suby, CLim, z);
        
    case 2  % RGB
        ValMax = this.StatValues.Max;
        if ValMax == 0
            ValNoir = 255;
        elseif ValMax <= 1
            ValNoir = 1;
        else
            ValNoir = 255;
        end
        
        z = this.Image(suby,subx,1);
        if ValNoir == 1
            z = z * 255;
        end
        z(isnan(z)) = 0; % Rajout� le 09/01/2008 pour EarthParErViewer.ers
        CLim = [0 255];
        [nomDir, Name, ext] = fileparts(nomFic);
        Name = fullfile(nomDir, [Name '_r' ext]);
        flag = export_gmt_z(this, Name, subx, suby, CLim, z);
        if ~flag
            return
        end
        
        z = this.Image(suby,subx,2);
        if ValNoir == 1
            z = z * 255;
        end
        z(isnan(z)) = 0; % Rajout� le 09/01/2008 pour EarthParErViewer.ers
        CLim = [0 255];
        [nomDir, Name, ext] = fileparts(nomFic);
        Name = fullfile(nomDir, [Name '_g' ext]);
        flag = export_gmt_z(this, Name, subx, suby, CLim, z);
        if ~flag
            return
        end
        
        z = this.Image(suby,subx,3);
        if ValNoir == 1
            z = z * 255;
        end
        z(isnan(z)) = 0; % Rajout� le 09/01/2008 pour EarthParErViewer.ers
        CLim = [0 255];
        [nomDir, Name, ext] = fileparts(nomFic);
        Name = fullfile(nomDir, [Name '_b' ext]);
        flag = export_gmt_z(this, Name, subx, suby, CLim, z);
        
    case 3  % Indexee
        z = this.Image(suby,subx);
        flag = export_gmt_z(this, nomFic, subx, suby, CLim, z);
        
    case 4  % Binaire
        z = this.Image(suby,subx);
        flag = export_gmt_z(this, nomFic, subx, suby, CLim, z);
end


function flag = export_gmt_z(this, nomFic, subx, suby, CLim, z)

if exist(nomFic, 'file')
    try
        W = warning;
        warning('off')
        delete(nomFic)
        warning(W)
    catch ME
        my_warndlg(ME.message, 0);
    end
end

nbRows = length(suby);
nbColumns = length(subx);


XLim = compute_XYLim(this.x(subx));
YLim = compute_XYLim(this.y(suby));
if diff(YLim) < 0
    YLim = fliplr(YLim);
end

sk.att(1).type   = 'NC_CHAR';
sk.att(1).name   = 'title';
sk.att(1).value = '\0';

sk.att(2).type   = 'NC_CHAR';
sk.att(2).name   = 'source';
sk.att(2).value = this.Name;

sk.dim(1).name  = 'side';
sk.dim(1).value = 2;

sk.dim(2).name  = 'xysize';
sk.dim(2).value = nbRows * nbColumns;

sk.var(1).name  = 'x_range';
% value : lecture et ecriture par appel aux methodes :
% v = get_value(a, 'x_range');   a = set_value(a, 'x_range', v);
% v = get_value(a, 1);       a = set_value(a, 1, v);
% get_value et set_value acceptent le sous echantillonage
% get_value(a, ident, 'sub1', sub1, 'sub2', sub2, ...);
sk.var(1).type  = 'NC_DOUBLE';
sk.var(1).dim(1).name  = 'side'; 	% 2

sk.var(1).att(1).name  = 'units';
sk.var(1).att(1).type  = 'NC_CHAR';
sk.var(1).att(1).value = 'user_x_unit\0';

sk.var(2).name  = 'y_range';
% value : lecture et ecriture par appel aux methodes :
% v = get_value(a, 'y_range');   a = set_value(a, 'y_range', v);
% v = get_value(a, 2);       a = set_value(a, 2, v);
% get_value et set_value acceptent le sous echantillonage
% get_value(a, ident, 'sub1', sub1, 'sub2', sub2, ...);
sk.var(2).type  = 'NC_DOUBLE';
sk.var(2).dim(1).name  = 'side'; 	% 2

sk.var(2).att(1).name  = 'units';
sk.var(2).att(1).type  = 'NC_CHAR';
sk.var(2).att(1).value = 'user_y_unit\0';

sk.var(3).name  = 'z_range';
% value : lecture et ecriture par appel aux methodes :
% v = get_value(a, 'z_range');   a = set_value(a, 'z_range', v);
% v = get_value(a, 3);       a = set_value(a, 3, v);
% get_value et set_value acceptent le sous echantillonage
% get_value(a, ident, 'sub1', sub1, 'sub2', sub2, ...);
sk.var(3).type  = 'NC_DOUBLE';
sk.var(3).dim(1).name  = 'side'; 	% 2

sk.var(3).att(1).name  = 'units';
sk.var(3).att(1).type  = 'NC_CHAR';
sk.var(3).att(1).value = 'user_z_unit\0';

sk.var(4).name  = 'spacing';
% value : lecture et ecriture par appel aux methodes :
% v = get_value(a, 'spacing');   a = set_value(a, 'spacing', v);
% v = get_value(a, 4);       a = set_value(a, 4, v);
% get_value et set_value acceptent le sous echantillonage
% get_value(a, ident, 'sub1', sub1, 'sub2', sub2, ...);
sk.var(4).type  = 'NC_DOUBLE';
sk.var(4).dim(1).name  = 'side'; 	% 2

sk.var(5).name  = 'dimension';
% value : lecture et ecriture par appel aux methodes :
% v = get_value(a, 'dimension');   a = set_value(a, 'dimension', v);
% v = get_value(a, 5);       a = set_value(a, 5, v);
% get_value et set_value acceptent le sous echantillonage
% get_value(a, ident, 'sub1', sub1, 'sub2', sub2, ...);
sk.var(5).type  = 'NC_LONG';
sk.var(5).dim(1).name  = 'side'; 	% 2

sk.var(6).name  = 'z';
% value : lecture et ecriture par appel aux methodes :
% v = get_value(a, 'z');   a = set_value(a, 'z', v);
% v = get_value(a, 6);       a = set_value(a, 6, v);
% get_value et set_value acceptent le sous echantillonage
% get_value(a, ident, 'sub1', sub1, 'sub2', sub2, ...);
sk.var(6).type  = 'NC_FLOAT';
sk.var(6).dim(1).name  = 'xysize'; 	% 12967321

sk.var(6).att(1).name  = 'scale_factor';
sk.var(6).att(1).type  = 'NC_DOUBLE';
sk.var(6).att(1).value = 1;

sk.var(6).att(2).name  = 'add_offset';
sk.var(6).att(2).type  = 'NC_DOUBLE';
sk.var(6).att(2).value = 0;

sk.var(6).att(3).name  = 'node_offset';
sk.var(6).att(3).type  = 'NC_DOUBLE';
sk.var(6).att(3).value = 0;

a = cl_netcdf('fileName', nomFic, 'skeleton', sk);
% a = set(a, 'fileName', nomFic);
% a = set(a, 'skeleton', sk);
a = set_value(a, 'x_range', double(XLim'));
a = set_value(a, 'y_range', double(YLim'));
a = set_value(a, 'z_range', double(CLim'));
spacing(1) = (this.y(suby(end)) - this.y(suby(1))) / (nbRows - 1);
spacing(2) = (this.x(subx(end)) - this.x(subx(1))) / (nbColumns - 1);
a = set_value(a, 'spacing', spacing');
a = set_value(a, 'dimension', [nbColumns nbRows]');

if this.y(end) > this.y(1)
    z = flipud(z);
end

z(z <= CLim(1)) = CLim(1);
z(z >= CLim(2)) = CLim(2);

if this.Video == 2
    z = (CLim(2) + CLim(1)) - z;
end

z = z';
[sz1, sz2, sz3] = size(z);
z = reshape(z, sz2, sz1, sz3);
z = z(:);
a = set_value(a, 'z', z);%#ok

flag = 1;

