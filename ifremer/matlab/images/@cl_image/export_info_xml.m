% Exportation du fichier de description des metadonnees au format XML (.xml)
%
% Syntax
%   flag = export_info_xml(a, nomFicXml, ...)
%
% Input Arguments
%   a         : Instance de cl_image
%   nomFicXml : Nom du fichier image ermapper
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   flag  : 1=OK, 0=KO
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   imagesc(c)
%
%   nomFicXml = my_tempname('.xml')
%   flag = export_info_xml(c, nomFicXml)
%
% See also export_ermapper export_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = export_info_xml(this, nomFicXml, varargin)

Info = struct(this);
fieldNames = fieldnames(Info);
for i=1:length(fieldNames)
    if strcmp(fieldNames{i}, 'Image')
        Info.Image = class(Info.Image(1));
    elseif strcmp(fieldNames{i}, 'x')
        Info.x = num2strCode(Info.x);
    elseif strcmp(fieldNames{i}, 'y')
        Info.y = num2strCode(Info.y);
    elseif strcmp(fieldNames{i}, 'Carto')
        Info.Carto = struct(Info.Carto);
    elseif strcmp(fieldNames{i}, 'Sonar')
        Info.Sonar = struct(Info.Sonar);
        
        if ~isempty(Info.Sonar.BathyCel) && isfield(Info.Sonar.BathyCel, 'Depth') && ~isempty(Info.Sonar.BathyCel.Depth)
            Info.Sonar.BathyCel.Depth = Info.Sonar.BathyCel.Depth(:,:);
            Info.Sonar.BathyCel.SoundSpeed = Info.Sonar.BathyCel.SoundSpeed(:,:);
        end
        if ~isempty(Info.Sonar.BathyCel) && isfield(Info.Sonar.BathyCel, 'Z') && ~isempty(Info.Sonar.BathyCel.Z)
            Info.Sonar.BathyCel.Z = Info.Sonar.BathyCel.Z(:,:);
            Info.Sonar.BathyCel.T = Info.Sonar.BathyCel.T(:,:);
            Info.Sonar.BathyCel.S = Info.Sonar.BathyCel.S(:,:);
            Info.Sonar.BathyCel.C = Info.Sonar.BathyCel.C(:,:);
        end
        
        if ~isempty(Info.Sonar.Desciption)
            Info.Sonar.Desciption = struct(Info.Sonar.Desciption);
            
            Info.Sonar.Desciption.Emission = struct(Info.Sonar.Desciption.Emission);
            
            clear pppp
            for iTx=1:length(Info.Sonar.Desciption.Emission.ModelsConstructeur)
                X = struct(Info.Sonar.Desciption.Emission.ModelsConstructeur{iTx});
                if ~isempty(X)
                    for iX=1:length(X)
                        X(iX).params = struct(X(iX).Params);
                    end
                    pppp(iTx,1:length(X)) = X; %#ok<AGROW>
                end
            end
            if exist('pppp', 'var')
                Info.Sonar.Desciption.Emission.ModelsConstructeur = pppp;
            end
            
            clear pppp
            for iTx=1:length(Info.Sonar.Desciption.Emission.ModelsCalibration)
                X = struct(Info.Sonar.Desciption.Emission.ModelsCalibration{iTx});
                if ~isempty(X)
                    for iX=1:length(X)
                        X(iX).params = struct(X(iX).Params);
                    end
                    pppp(iTx,1:length(X)) = X;
                end
            end
            if exist('pppp', 'var')
                Info.Sonar.Desciption.Emission.ModelsCalibration = pppp;
            end
            
            
            Info.Sonar.Desciption.Reception = struct(Info.Sonar.Desciption.Reception);
            
            clear pppp
            for iTx=1:length(Info.Sonar.Desciption.Reception.ModelsConstructeur)
                if iscell(Info.Sonar.Desciption.Reception.ModelsConstructeur)
                    %                     my_warndlg('Message for JMA : export_info_xml ModelsConstructeur en cellule.En principe on ne devrait pas trouver ce cas pour la r�ception.', 0);
                    X = struct(Info.Sonar.Desciption.Reception.ModelsConstructeur{iTx});
                else
                    X = struct(Info.Sonar.Desciption.Reception.ModelsConstructeur(iTx));
                end
                if ~isempty(X)
                    for iX=1:length(X)
                        X(iX).params = struct(X(iX).params);
                    end
                    pppp(iTx,1:length(X)) = X;
                end
            end
            if exist('pppp', 'var')
                Info.Sonar.Desciption.Reception.ModelsConstructeur = pppp;
            end
            
            clear pppp
            for iTx=1:length(Info.Sonar.Desciption.Reception.ModelsCalibration)
                if iscell(Info.Sonar.Desciption.Reception.ModelsCalibration)
                    %                     my_warndlg('Message for JMA : export_info_xml  ModelsConstructeur en cellule.En principe on ne devrait pas trouver ce cas pour la r�ception.', 0);
                    X = struct(Info.Sonar.Desciption.Reception.ModelsCalibration{iTx});
                else
                    X = struct(Info.Sonar.Desciption.Reception.ModelsCalibration(iTx));
                end
                if ~isempty(X)
                    for iX=1:length(X)
                        X(iX).params = struct(X(iX).params);
                    end
                    pppp(iTx,1:length(X)) = X;
                end
            end
            if exist('pppp', 'var')
                Info.Sonar.Desciption.Reception.ModelsCalibration = pppp;
            end
        end
        
        if ~isempty(Info.Sonar.Time)
            X = char(Info.Sonar.Time);
            Info.Sonar.Time = X(strfind(X, '(Debut'):end);
            
            Info.Sonar.Immersion            = num2strCode(Info.Sonar.Immersion(:,:));
            Info.Sonar.ResolutionX          = num2strCode(Info.Sonar.ResolutionX(:,:));
            Info.Sonar.Height               = num2strCode(Info.Sonar.Height(:,:));
            Info.Sonar.CableOut             = num2strCode(Info.Sonar.CableOut(:,:));
            Info.Sonar.Speed                = num2strCode(Info.Sonar.Speed(:,:));
            Info.Sonar.Heading              = num2strCode(Info.Sonar.Heading(:,:));
            Info.Sonar.Roll                 = num2strCode(Info.Sonar.Roll(:,:));
            Info.Sonar.RollUsedForEmission  = num2strCode(Info.Sonar.RollUsedForEmission(:,:));
            Info.Sonar.Pitch                = num2strCode(Info.Sonar.Pitch(:,:));
            Info.Sonar.Yaw                  = num2strCode(Info.Sonar.Yaw(:,:));
            Info.Sonar.SurfaceSoundSpeed    = num2strCode(Info.Sonar.SurfaceSoundSpeed(:,:));
            Info.Sonar.FishLatitude         = num2strCode(Info.Sonar.FishLatitude(:,:));
            Info.Sonar.FishLongitude        = num2strCode(Info.Sonar.FishLongitude(:,:));
            Info.Sonar.ShipLatitude         = num2strCode(Info.Sonar.ShipLatitude(:,:));
            Info.Sonar.ShipLongitude        = num2strCode(Info.Sonar.ShipLongitude(:,:));
            Info.Sonar.PortMode_1           = num2strCode(Info.Sonar.PortMode_1(:,:));
            Info.Sonar.PortMode_2           = num2strCode(Info.Sonar.PortMode_2(:,:));
            Info.Sonar.StarMode_1           = num2strCode(Info.Sonar.StarMode_1(:,:));
            Info.Sonar.StarMode_2           = num2strCode(Info.Sonar.StarMode_2(:,:));
            Info.Sonar.Heave                = num2strCode(Info.Sonar.Heave(:,:));
            Info.Sonar.Tide                 = num2strCode(Info.Sonar.Tide(:,:));
            Info.Sonar.PingCounter          = num2strCode(Info.Sonar.PingCounter(:,:));
            Info.Sonar.SampleFrequency      = num2strCode(Info.Sonar.SampleFrequency(:,:));
            Info.Sonar.SonarFrequency       = num2strCode(Info.Sonar.SonarFrequency(:,:));
            Info.Sonar.Interlacing          = num2strCode(Info.Sonar.Interlacing(:,:));
            
            Info.Sonar.BSN                  = num2strCode(Info.Sonar.BSN(:,:));
            Info.Sonar.BSO                  = num2strCode(Info.Sonar.BSO(:,:));
            Info.Sonar.TVGN                 = num2strCode(Info.Sonar.TVGN(:,:));
            Info.Sonar.TVGCrossOver         = num2strCode(Info.Sonar.TVGCrossOver(:,:));
            Info.Sonar.TxBeamWidth          = num2strCode(Info.Sonar.TxBeamWidth(:,:));
        end
    end
end

if isempty(Info.SignalsVert)
    Info.SignalsVert = [];
else
    for k=1:length(Info.SignalsVert)
        [flag, val, Unit] = getValueMatrix(Info.SignalsVert(k));
        if flag
            signalName = Info.SignalsVert(k).name;
            switch signalName
                case 'BSN-BSO'
                    signalName = 'BSN_BSO';
            end
            SignalsVert.(signalName) = sprintf('%s (%s)', num2strCode(val), Unit);
        end
    end
    Info.SignalsVert = SignalsVert;
end

Info.Carto = []; % Ajout� par JMA le 18/02/2021 pour mosa�ques individuelles OTUS
Info.Sonar = []; % Ajout� par JMA le 18/02/2021 pour mosa�ques individuelles OTUS
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml);
    return
end
