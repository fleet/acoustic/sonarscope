% Ecriture d'une image dans un fichier matlab (Image, Info dans un .mat)
%
% Syntax
%   export_matlab_array(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Examples
%   a = cl_image('Image', Lena);
%   nomFic = [tempname '.mat']
%   export_matlab_array(a, nomFic)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function export_matlab_array(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Appel de la fonction d'ecriture

Image = this.Image(suby, subx, :);

Info.Name         = this.Name;
Info.Unit         = this.Unit;
Info.CLim         = this.CLim;
Info.Colormap	  = this.Colormap;
Info.ValNaN       = this.ValNaN;
Info.x            = this.x(subx);
Info.XUnit        = this.XUnit;
Info.XLabel       = this.XLabel;
Info.y            = this.y(suby);
Info.YUnit        = this.YUnit;
Info.YLabel       = this.YLabel;
Info.YDir         = this.YDir;
Info.XDir         = this.XDir;
Info.DataType     = this.DataType;
Info.GeometryType = this.GeometryType;

pppp = whos('Image');
if pppp.bytes > 1e9
    strWarn = Lang('Sauvegarde de l''image dans le fichier en v7.3', 'Saving Image in file');
    hWrn = warndlg(strWarn, 'Process in work');
    save(nomFic, 'Image', 'Info', '-v7.3');
    if ishandle(hWrn)
        close(hWrn);
    end
else
    save(nomFic, 'Image', 'Info');
end

