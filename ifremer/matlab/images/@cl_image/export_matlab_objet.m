% Ecriture d'une image dans un fichier matlab (cl_image dans un .mat)
%
% Syntax
%   export_matlab_objet(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Examples
%   a = cl_image('Image', Lena);
%   nomFic = [tempname '.mat']
%   export_matlab_objet(a, nomFic)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function export_matlab_objet(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

str1 = 'L''exportation de cette image dans ce format ne doit pas �tre utilis�e pour une sauvegarde longue dur�e, son contenu pouvant devenir illisible avec une nouvelle version du logiciel. Pr�f�rez l''exportation au format Er-Mapper.';
str2 = 'The export of the image in this format should not be used for a long perid, the contents of the Matlab object is improved day after day. Prefer the XML export or Er-Mapper.';
my_warndlg(Lang(str1,str2), 1);

this = extraction(this, 'subx', subx, 'suby', suby);

save(nomFic, 'this');
