function flag = export_originalFormat(this, subx, suby)

flag = 0;
    
InitialFileName = this.InitialFileName;
if isempty(InitialFileName)
    str1 = ['Le fichier original "' InitialFileName ' " n''existe pas.'];
    str2 = ['Source file name "' InitialFileName ' " does not exist.'];
    my_warndlg(Lang(str1,str2), 1);
    return
end

[~, ~, ext] = fileparts(InitialFileName);
[flag, nomFic] = my_uiputfile(['*' ext], Lang('Nom du fichier', 'Give a file name'), InitialFileName);
if ~flag
    return
end
% [subc, subl] = get_subx_suby(this);

InitialFileFormat = this.InitialFileFormat;
[~, ~, ext] = fileparts(nomFic);
switch InitialFileFormat
    case 'KleinSDF' % TODO : renommer export_KleinSDF en export_SDF
        export_KleinSDF(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    case 'ErMapper'
        export_ermapper(this, nomFic, 'subx', subx, 'suby', suby, 'Open', 'r+');
        
    case 'MATLAB'
        export_matlab(this, nomFic, 'subx', subx, 'suby', suby);
        
    case {'SimradAll'; 'SimradDatagramAll'} % SimradDatagramAll pour anciens fichiers
        export_SimradAll(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    case 'XTF'
        export_XTF(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    case 'SonarSAR'
        export_SAR(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    case 'ResonS7k'
        export_ResonS7k(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    case 'IMAGE'
        exportRaster(this, 'nomFic', nomFic, 'Format', ext(2:end), ...
            'subx', subx, 'suby', suby);
        
    case 'HAC'
        export_HAC(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    case 'SEGY'
        export_SEGY(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    case 'Simrad_ExRaw'
        export_ExRaw(this, 'nomFic', nomFic, 'subx', subx, 'suby', suby);
        
    otherwise
        str = sprintf('clc_image:callback_export InitialFileFormat="%s" not available yet', ...
            InitialFileFormat);
        my_warndlg(str, 1);
end
