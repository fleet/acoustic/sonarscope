function flag = export_polarEchogram2SSc3DV(this, nomFicXml, repExport, varargin)

N = length(this);
hw = create_waitbar(waitbarMsg('export_polarEchogram2SSc3DV'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    flag = export_polarEchogram2SSc3DV_unit(this(k), nomFicXml, repExport, varargin{:});
    if ~flag
        return
    end
end  
my_close(hw)


function flag = export_polarEchogram2SSc3DV_unit(this, nomFicXml, repExport, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('DepthAcrossDist'));
if ~flag
    return
end

if isempty(nomFicXml)
    ImageName = extract_ImageName(this);
    nomFicXml = fullfile(repExport, [ImageName '.xml']);
end

CLim = this.CLim;
% iPing = this.Sonar.SampleBeamData.PingCounter;
iPing = 1;
[nomDir, nomFic] =  fileparts(nomFicXml);
nomDir2 = fullfile(nomDir, nomFic);
TimePing  = this.Sonar.SampleBeamData.Time;
Latitude  = this.Sonar.SampleBeamData.Latitude;
Longitude = this.Sonar.SampleBeamData.Longitude;
Heading   = this.Sonar.SampleBeamData.Heading;
[flag, Coord] = WC_writeImagePng(this, CLim, ...
    iPing, Latitude, Longitude, Heading, nomDir2, TimePing, 'subx', subx, 'suby', suby);
if ~flag
    message(flag, nomFicXml)
    return
end

% TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
N = 1;
for k=1:(N-1)
    flag = WC_writeImagePng(this, CLim, ...
        iPing+k, Latitude, Longitude, Heading, nomDir2, TimePing, 'subx', subx, 'suby', suby);
    if ~flag
        message(flag, nomFicXml)
        return
    end
end

if isfield(this.Sonar.SampleBeamData, 'TransducerDepth')
    TransducerDepth = this.Sonar.SampleBeamData.TransducerDepth;
    TransducerDepth = TransducerDepth(1); % TODO
else
    TransducerDepth = 0;
end

yHaut = max(this.y(suby([1 end])));
% Avant 
% yBas  = min(this.y(suby([1 end])));
% YStep = this.YStep;
% Z0 = TransducerDepth;
% TransducerDepth = Z0 + yHaut + YStep;
% Coord.Depth     = Z0 + yHaut + yBas; % TODO : tr�s bizarre cette affaire : il faut inspecter ce qui est fait dans GLOBE

% TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO

WC.lonBab(1:N)    =  Coord.lonBab;
WC.latBab(1:N)    =  Coord.latBab;
WC.lonCentre(1:N) =  Coord.lonCentre;
WC.latCentre(1:N) =  Coord.latCentre;
WC.lonTri(1:N)    =  Coord.lonTri;
WC.latTri(1:N)    =  Coord.latTri;
WC.iPing(1:N)     =  Coord.iPing;
WC.xBab(1:N)      =  Coord.xBab;
WC.xTri(1:N)      =  Coord.xTri;

% Avant
% WC.Depth(1:N)     =  Coord.Depth;
% WC.Immersion(1:N)  = TransducerDepth;

% En test
% WC.Depth(1:N)     = Coord.Depth - 2*TransducerDepth + yHaut;
WC.Immersion(1:N) = -TransducerDepth + yHaut;
WC.Depth(1:N)     = WC.Immersion(1:N) + Coord.Depth;

WC.Date(1:N)      =  Coord.Date;
WC.Hour(1:N)      =  Coord.Heure;

WC.lonBabDepointe(1:N) =  Coord.lonBabDepointe;
WC.latBabDepointe(1:N) =  Coord.latBabDepointe;
WC.lonTriDepointe(1:N) =  Coord.lonTriDepointe;
WC.latTriDepointe(1:N) =  Coord.latTriDepointe;
WC.nomFicAll = nomFic;

WC.Name = nomFic;
Survey = [];
WC.nbPings = 1;

WC.nbPings = N; % TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO

Unit = this.CLim;
flag = WC_writeXmlPng(nomFicXml, WC, Survey, Unit);

%% C'est fini

message(flag, nomFicXml)



function message(flag, nomFicXml)
if flag
    str1 = sprintf('La cr�ation du fichier "%s" est termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" is over.', nomFicXml);
else
    str1 = sprintf('La cr�ation du fichier "%s" ne s''est pas bien termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" failed.', nomFicXml);
end
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProcessingWCIsOver');
