% Importation d'un fichier SURFER (*.grd)
%
% Syntax
%   flag = import_surfer(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFicIn = getNomFicDatabase('KBMA0504.grd') % Ancien surfer ?
%   nomFicIn = 'D:\Temp\SPFE\grd\Dsaa.grd'; % Surfer ASCII
%   nomFicIn = 'D:\Temp\SPFE\grd\ZONE2_DTM_5X5M_clipped.grd'; % Surfer 6
%   nomFicIn = 'D:\Temp\SPFE\grd\Colorado.grd'; % Surfer 7
%   [flag, a] = import_surfer(cl_image, nomFicIn);
%   imagesc(a)
%
%   nomFicOut = my_tempname('.grd');
%   flag = export_surfer(a, nomFicOut, 'Version', 6);
%   [flag, b] = import_surfer(cl_image, nomFicOut);
%   imagesc(b)
%
%   nomFicOut = my_tempname('.grd');
%   flag = export_surfer(a, nomFicOut, 'Version', 5);
%   [flag, b] = import_surfer(cl_image, nomFicOut);
%   imagesc(b)
%
%   nomFicOut = my_tempname('.grd');
%   flag = export_surfer(a, nomFicOut, 'Version', 7);
%   flag = import_surfer(cl_image, nomFicOut);
%
% See also cl_image/import_surfer Authors
% Authors : JMA
%--------------------------------------------------------------------------

function flag = export_surfer(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Version] = getPropertyValue(varargin, 'Version', 7); %#ok<ASGLU>

switch Version
    case 5
        flag = export_surfer5(this, nomFic, 'subx', subx, 'suby', suby);
    case 6
        flag = export_surfer6(this, nomFic, 'subx', subx, 'suby', suby);
    case 7
        flag = export_surfer7(this, nomFic, 'subx', subx, 'suby', suby);
end

function flag = export_surfer5(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag  = 0;
cols = length(subx);
rows = length(suby);
xlo = min(this.x(subx));
xhi = max(this.x(subx));
ylo = min(this.y(suby));
yhi = max(this.y(suby));
z = this.Image(suby,subx);
zlo =  min(z(:));
zhi =  max(z(:));

NoData = ceil(cast(zhi+(zhi-zlo)/2, class(z)));
z(isnan(z)) = NoData; % Test gestion des NaN

ySize = mean(diff(this.y(suby)));
if ySize < 0
    z = flipud(z);
end

fid = fopen(nomFic, 'w+');
if fid == -1
    return
end
fprintf(fid, 'DSAA\n');
fprintf(fid, '%d %d\n', cols, rows);
fprintf(fid, '%s %s\n', num2strPrecis(xlo), num2strPrecis(xhi));
fprintf(fid, '%s %s\n', num2strPrecis(ylo), num2strPrecis(yhi));
fprintf(fid, '%s %s\n', num2strPrecis(zlo), num2strPrecis(zhi));
str1 = 'Ecriture du fichier Surfer ASCII en cours';
str2 = 'Writing Surfer ASCII file';
hw = create_waitbar(Lang(str1,str2), 'N', rows);
for k1=1:rows
    my_waitbar(k1, rows, hw)
    fprintf(fid, '%s ', num2strPrecis(z(k1,:)));
    %     for k2=1:cols
    %         fprintf(fid, '%s ', num2strPrecis(z(k1,k2)));
    %     end
    fprintf(fid, '\n');
end
my_close(hw, 'MsgEnd');
fclose(fid);
flag = 1;


function flag = export_surfer6(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag  = 0;
cols = length(subx);
rows = length(suby);
xlo = min(this.x(subx));
xhi = max(this.x(subx));
ylo = min(this.y(suby));
yhi = max(this.y(suby));
z = this.Image(suby,subx);
zlo =  min(z(:));
zhi =  max(z(:));

NoData = ceil(cast(zhi+(zhi-zlo)/2, class(z)));
z(isnan(z)) = NoData; % Test gestion des NaN

ySize = mean(diff(this.y(suby)));
if ySize < 0
    z = flipud(z);
end

fid = fopen(nomFic, 'w+');
if fid == -1
    return
end
fwrite(fid, 'DSBB')
fwrite(fid, cols, 'int16');
fwrite(fid, rows, 'int16');
fwrite(fid, xlo, 'double');
fwrite(fid, xhi, 'double');
fwrite(fid, ylo, 'double');
fwrite(fid, yhi, 'double');
fwrite(fid, zlo, 'double');
fwrite(fid, zhi, 'double');
fwrite(fid, z', 'single');
fclose(fid);
flag = 1;


function flag = export_surfer7(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag  = 0;
cols = length(subx);
rows = length(suby);
xlo = min(this.x(subx));
xSize = mean(diff(this.x(subx)));
ylo = min(this.y(suby));
ySize = mean(diff(this.y(suby)));
z = this.Image(suby,subx);
zlo =  min(z(:));
zhi =  max(z(:));

if ySize < 0
    ySize = -ySize;
    z = flipud(z);
end

NoData = ceil(cast(zhi+(zhi-zlo)/2, class(z)));
z(isnan(z)) = NoData; % Test gestion des NaN

fid = fopen(nomFic, 'w+');
if fid == -1
    return
end
fwrite(fid, 'DSRB');
fwrite(fid, 4, 'int32');
fwrite(fid, 1, 'int32');

fwrite(fid, 'GRID');
fwrite(fid, 72,     'int32');
fwrite(fid, rows,   'int32');
fwrite(fid, cols,   'int32');
fwrite(fid, xlo,    'double');
fwrite(fid, ylo,    'double');
fwrite(fid, xSize,  'double');
fwrite(fid, ySize,  'double');
fwrite(fid, zlo,    'double');
fwrite(fid, zhi,    'double');
fwrite(fid, 0,      'double');
fwrite(fid, NoData, 'double');

fwrite(fid, 'DATA');
fwrite(fid, cols*rows*8, 'int32');
fwrite(fid, z', 'double');

fclose(fid);
flag = 1;
