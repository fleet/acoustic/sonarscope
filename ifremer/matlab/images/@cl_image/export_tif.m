% Ecriture d'une image dans un fichier tif, jpeg, ...
%
% Syntax
%   export_tif(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   CLim : Rehaussement de contraste
%
% Examples
%   a = cl_image('Image', Lena);
%   nomFic = [tempname '.tif']
%   export_tif(a, nomFic)
%   visuExterne(nomFic)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_tif(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, CLim]      = getPropertyValue(varargin, 'CLim',      this.CLim);
[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []);
[varargin, zone]      = getPropertyValue(varargin, 'zone',      []);
[varargin, Mute]      = getPropertyValue(varargin, 'Mute',      0);

flag = testEcritureFichier(nomFic);
if ~flag
    return
end

mask = [];

[pathstr, name, ext] = fileparts(nomFic);
tempFic = fullfile(pathstr, [name '_Temp' ext]);

%%  Appel de la fonction d'�criture

if this.ImageType == 2
    nbSlides = 3;
    %mask = mask2OnesOrNaN(LayerMask, valMask, zone, this.x(subx), this.y(suby));
    % masquage
    if ~isempty(LayerMask)
        Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
        if zone == 1
            mask = zeros(size(Masque), 'single');
            for ik=1:length(valMask)
                % sub = find(Masque == valMask(ik));
                sub = (Masque == valMask(ik));
                mask(sub) = 1;
            end
        else
            mask = ones(size(Masque), 'single');
            for ik=1:length(valMask)
                % sub = find(Masque == valMask(ik));
                sub = (Masque == valMask(ik));
                mask(sub) = 0;
            end
        end
        [path, file] = fileparts(nomFic);
        tempFic = fullfile(path, [file , '.png']);
    end
else
    nbSlides = 1;
end

try
    I = zeros(length(suby), length(subx), nbSlides, 'like', this.Image(1));
catch %#ok<CTCH>
    I = cl_memmapfile('Value', 0, 'Size', [length(suby), length(subx), nbSlides], 'Format', class(this.Image(1)));
end

N = ceil(5e8 / (length(subx) * length(suby)));
ValeurPresente = false;
for k=1:N:length(suby)
    sub = k:min(k+N-1, length(suby));
    X = this.Image(suby(sub), subx, :);
    if ~ValeurPresente
        if isnan(this.ValNaN)
            if any(~isnan(X(:)))
                ValeurPresente = true;
            end
        else
            if any(X(:) ~= this.ValNaN)
                ValeurPresente = true;
            end
        end
    end
    I(sub,:,:) = X;
end
if ~ValeurPresente
    % Modif JMA le 02/07/2011 pour �viter de sauver des images qui ne contiennent pas de donn�es
    % Ca peut peut-�tre perturber certaines applications (Arnaud ???) Sinon, mette un PropertyName/PropertyValue pour bypasser ce test pour ces applications l�.
    str1 = sprintf('Fichier "%s" non cr�� car tous ses pixels sont NaN.', nomFic);
    str2 = sprintf('File "%s" not created because all the pixels are NaN.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImageTifNonSauveeCarVide');
    flag = 0;
    return
end

W = warning;
warning('off', 'all');
xDir = ((this.x(end) - this.x(1)) > 0);
yDir = ((this.y(end) - this.y(1)) > 0);

switch this.ImageType
    case 1 % Intensity
        if nbSlides == 1
            % Pourquoi �craser les valeurs et transformer en RGB ?
            I = quantify(I, 'rangeIn', CLim);
            I(isnan(I))     = 0;
            nodata_value = sprintf('-a_nodata "%d"', 0); % Pour export Geo_Tiff
            I = uint8(I);
            
            if xor(xDir , (this.XDir == 1))
                I = fliplr(I);
            end
            if ~xor(yDir , (this.YDir == 1))
                I = flipud(I);
            end
            
            if (this.ColormapIndex == 2) || isequal(this.Colormap, gray(256))
                
                if this.Video ==  2
                    I = 255 - I;
                end
                imwrite(I, tempFic, varargin{:})
                
            else
                Map     = this.Colormap;
                nbCoul  = size(Map, 1);
                subc    = linspace(1, nbCoul, 256);
                
                Colormap(:, 1) = interp1(1:nbCoul, Map(:,1), subc)';
                Colormap(:, 2) = interp1(1:nbCoul, Map(:,2), subc)';
                Colormap(:, 3) = interp1(1:nbCoul, Map(:,3), subc)';
                
                imwrite(I, Colormap, tempFic, varargin{:})
            end
        else
            for k=1:size(I,3)
                if xor(xDir , (this.XDir == 1))
                    I(:,:,k) = fliplr(I(:,:,k));
                end
                
                if ~xor(yDir , (this.YDir == 1))
                    I(:,:,k) = flipud(I(:,:,k));
                end
            end
            if isa(I(1), 'uint8')
                imwrite(I(:,:,:), tempFic, varargin{:})
            else
                J = I(:,:,:);
                if max(J(:)) <= 1
                    J = uint8(J * 255);
                else
                    J = uint8(J);
                end
                imwrite(J, tempFic, varargin{:})
            end
        end
        
    case 2 % RGB
        for k=1:size(I,3)
            if xor(xDir , (this.XDir == 1))
                I(:,:,k) = fliplr(I(:,:,k));
            end
            
            if ~xor(yDir , (this.YDir == 1))
                I(:,:,k) = flipud(I(:,:,k));
            end
        end
        
        if ~isempty(mask)
            if xor(xDir , (this.XDir == 1))
                mask = fliplr(mask);
            end
            
            if ~xor(yDir , (this.YDir == 1))
                mask = flipud(mask);
            end
        end
        
        %         imwrite(I(:,:,:), tempFic)
        if isa(I(1), 'uint8')
            %             nodata_value = sprintf('-a_nodata "%d %d %d"', ones(1,3) * this.ValNaN); % Pour export Geo_Tiff
            %             imwrite(I(:,:,:), tempFic, varargin{:})
            if isempty(mask)
                imwrite(I(:,:,:), tempFic, varargin{:});
            else
                imwrite(I(:,:,:), tempFic, varargin{:}, 'Alpha', uint8(mask));
            end
            nodata_value =  '-b 1 -b 2 -b 3 -mask 4 --config GDAL_TIFF_INTERNAL_MASK YES';
            
        else
            J = I(:,:,:);
            if max(J(:)) <= 1
                nodata_value = sprintf('-a_nodata "%d %d %d"', ones(1,3) * this.ValNaN * 255); % Pour export Geo_Tiff
                J = uint8(J * 255);
            else
                nodata_value = sprintf('-a_nodata "%d %d %d"', ones(1,3) * this.ValNaN); % Pour export Geo_Tiff
                J = uint8(J);
            end
            imwrite(J, tempFic, varargin{:})
        end
        %         nodata_value = '';
        
    case 3 % Indexed
        if xor(xDir , (this.XDir == 1))
            I = fliplr(I);
        end
        if ~xor(yDir , (this.YDir == 1))
            I = flipud(I);
        end
        
        nodata_value = sprintf('-a_nodata "%d"', this.ValNaN); % Pour export Geo_Tiff
        % TODO : que faire si this.ValNaN = NaN ? Peut-�tre faut-il
        % consid�rer qu'il n'y a pas de ValNaN (exemple Mandrill ?)
        
        imwrite(uint8(I), this.Colormap, tempFic, varargin{:})
        
    case 4 % Binary
        % Pas fait
end

warning(W)

%% G�or�f�rencement

if this.GeometryType == cl_image.indGeometryType('GeoYX') || this.GeometryType == cl_image.indGeometryType('LatLong')
    
    [~, ~, Ext] = fileparts(nomFic);
    switch Ext
        % Si Tiff : conversion en GeoTiff avec Gdal_translate.exe dans
        % 'C:\IfremerToolbox_Rxxxx\ifremer\extern\gdal\'
        case '.tif'
            %% R�cup�ration du chemin de l'�x�cutable
            GDALtranslate   = 'gdal_translate.exe';
            pathGDAL      = getenv('GDAL_HOME');
            GDALtranslate = fullfile(pathGDAL, GDALtranslate);
            
            if ~exist(GDALtranslate, 'file')
                str1 = sprintf('Le fichier "%s" n''a pas �t� trouv� sur votre ordinateur, if vous faut r�cup�rer le dernier "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" qui vous installera ce composant.', GDALtranslate);
                str2 = sprintf('File "%s" not found, you should download and install the last "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" that will install this component.', GDALtranslate);
                my_warndlg(Lang(str1,str2), 1);
            end
            
            % R�cuparation des param�tres
            Xlim = this.XLim;
            Ylim = this.YLim;
            bounds = [Xlim(1) Ylim(2) Xlim(2) Ylim(1)]; % [UpperLeftX UpperLeftY LowerRightX LowerRightY]
            
            Carto = get(this, 'Carto');
            Ellipsoide = Carto.Ellipsoide;
            Projection = Carto.Projection;
            
            datum      = get_EllipsoidName(Carto);
            identProjection = get_ProjectionName(Carto);
            % TODO : continuer ce travail ou bien profiter de la nouvelle
            % classe carto pour faire des acc�s de type toto.tata.titi MAIS
            % NE JAMAIS UTILISER de struct(classe) !!!!
            
            %% Test de coh�rence "pr�f carto"/g�ometrie
            
            switch identProjection
                case 'Geodetic'
                    if this.GeometryType == cl_image.indGeometryType('GeoYX') % A priori ce cas n'arrive jamais
                        str1 = sprintf('Export tiff r�ussi, mais... impossible de convertir Tiff --> GeoTiff\n\nIncoh�rence entre g�om�trie de l''image et pr�f�rence cartographiques :\nG�om�trie : %s\nG�od�sie :%s, %s\n\n>> Veuillez d�projeter l''image avant l''export', 'GeoYX', datum, identProjection);
                        str2 = sprintf('Tiff export successfull, but... cannot convert Tiff --> GeoTiff\n\nImage geometry and cartographic preferences are incoherent :\nGeometry : %s\nGeodesy : %s, %s\n\n>> Unproject image before exporting', 'GeoYX', datum, identProjection);
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
                case {'Mercator', 'UTM', 'Lambert', 'Stereographique polaire'}
                    if this.GeometryType == cl_image.indGeometryType('LatLong')
                        identProjection = 'Geodetic'; % Implicitement, l'export se fait en Geodetic
                    end
            end
            
            %% D�finition Proj.4 de la projection
            
            switch identProjection
                case 'Geodetic'
                    carto = sprintf('-a_srs "+datum=%s +proj=latlong"', datum);
                    
                case 'Mercator'
                    % NOTE : PROJ.4 does not support a latitude of natural origin other than the equator. !!!
                    % Donc on passe en WKT, qui lui, supporte ce param�tre, via un fichier externe
                    pathWKT = fullfile( fileparts(nomFic), 'proj.wkt' );
                    fidProj = fopen(pathWKT, 'wt');
                    fprintf(fidProj, 'PROJCS["%s_World_Mercator",GEOGCS["%s",DATUM["%s",SPHEROID["%s",%f,%f]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433]],PROJECTION["Mercator_1SP"],PARAMETER["latitude_of_origin",%f],PARAMETER["central_meridian",%f],PARAMETER["scale_factor",1],PARAMETER["false_easting",%f],PARAMETER["false_northing",%f],UNIT["metre",1]]',...
                        datum,...
                        datum,...
                        datum,...
                        datum,...
                        Ellipsoide.DemiGrandAxe,...
                        1 / ( 1 - sqrt( 1-Ellipsoide.Excentricite^2 ) ),...
                        Projection.(identProjection).lat_ech_cons,...
                        Projection.(identProjection).long_merid_orig,...
                        Projection.(identProjection).X0,...
                        Projection.(identProjection).Y0);
                    fclose(fidProj);
                    carto = sprintf('-a_srs "%s"', pathWKT);
                    
                case 'UTM'
                    carto = sprintf(...
                        '-a_srs "+datum=%s +proj=tmerc +lat_0=%f +lon_0=%f +k=%f +x_0=%f +y_0=%f"',...
                        datum,...
                        0,...                                                   % Latitude of natural origin
                        Projection.(identProjection).Central_meridian,...   % Longitude of natural origin
                        0.9996,...                                              % ??? Scale factor at natural origin
                        Projection.(identProjection).X0,...                 % False Easting
                        Projection.(identProjection).Y0);                   % False Northing
                    
                case 'Lambert'
                    carto = sprintf(...
                        '-a_srs "+datum=%s +proj=lcc +lat_1=%f +lat_2=%f +lon_0=%f +x_0=%f +y_0=%f"',...
                        datum,...
                        Projection.(identProjection).first_paral,...       % Latitude of 1st standard parallel
                        Projection.(identProjection).second_paral,...       % Latitude of 2nd standard parallel
                        Projection.(identProjection).long_merid_orig,...    % Longitude of false origin
                        Projection.(identProjection).X0,...                 % False Origin Easting
                        Projection.(identProjection).Y0);                   % False Origin Northing
                    % +lat_0=%f 0,...  % % ??? Latitude of false origin
                    
                case 'Stereographique polaire'
                    if strcmpi(Projection.(identProjection).hemisphere, 'N')
                        sign = 1;
                    else
                        sign = -1;
                    end
                    carto = sprintf(...
                        '-a_srs "+datum=%s +proj=stere +lat_ts=%f +lat_0=%f +lon_0=%f +k_0=1 +x_0=0 +y_0=0"',...
                        datum,...
                        Projection.(identProjection).lat_ech_cons,...
                        sign*90,...
                        Projection.(identProjection).long_merid_horiz);
                    
                otherwise %'other'
                    my_warndlg(sprintf('Error Gdal_translate :\n\n%s','Unknown Projection'),  1);
                    return
            end
            
            %% Conversion GeoTiff
            
            frmt = 'GTiff';
            %nodata_value = 0;
            if strcmp(tempFic, nomFic)
                copyfile(nomFic, tempFic, 'f');     % copie du fichier d'origine, car origine et modifi� doivent �tre diff�rents
            end
%             cmd = sprintf('%s -q -of %s %s -a_ullr %f %f %f %f %s "%s" "%s" -co "COMPRESS=PackBits" -co "SOFTWARE=SonarScope"',...
%                 GDALtranslate, frmt, nodata_value, bounds, carto, tempFic, nomFic);
            cmd = sprintf('%s -q -of %s %s -a_ullr %f %f %f %f %s "%s" "%s" -co "COMPRESS=PackBits"',...
                GDALtranslate, frmt, nodata_value, bounds, carto, tempFic, nomFic);
            %      cmd = sprintf(...
            %         '%s -of %s -a_ullr %f %f %f %f %s %s %s',...
            %         GDALtranslate, frmt, bounds, carto, tempFic, nomFic);
            
            if ~Mute
                fprintf('%s\n', cmd);
            end
            
            cmd = regexprep(cmd, 'Program Files', '"Program Files"');
            [status, result] = system(cmd);
            delete(tempFic); % Effacement du fichier d'origine
            if exist('pathWKT', 'var') && exist(pathWKT, 'file') % Effacement du fichier de g�od�sie WKT s'il existe
                delete(pathWKT);
            end
            
            if status ~= 0
                my_warndlg(sprintf('Error Gdal_translate :\n\n%s', result),  1);
                export_worldfile(this, nomFic, 'subx', subx, 'suby', suby);
            end
            
        otherwise
            
            % Ajout JMA le 30/09/2015
            if ~strcmp(tempFic, nomFic)
                my_rename(tempFic, nomFic);
            end
            
            %% Ecriture fichier Worldfile | Modif GLA 2010/05/28
            export_worldfile(this, nomFic, 'subx', subx, 'suby', suby);
    end
    
else
    if ~strcmp(tempFic, nomFic)
        my_rename(tempFic, nomFic);
    end
end
