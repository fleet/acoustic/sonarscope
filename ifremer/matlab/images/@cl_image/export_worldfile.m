% Ecriture d'un fichier WorldFile au format ESRI, associ� � une image tif, jpeg, png, bmp, etc.
% (http://en.wikipedia.org/wiki/World_file). Utilise des fonctions mapping
% toolbox
%
% Syntax
%   export_worldfile(a, nomFic, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   CLim : Rehaussement de contraste
%
% Examples
%   a = cl_image('Image', Lena);
%   nomFic = [tempname '.tif']
%   export_tif(a, nomFic)
%   visuExterne(nomFic)
%
% See also cl_image, export_tif, export_gif
% Authors : GLA
% ----------------------------------------------------------------------------

function export_worldfile(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'noMessage');
% if ~flag
%     return
% end

XStep = get(this, 'XStep');
YStep = -get(this, 'YStep');
xLeft = min(this.x(subx));
yTop  = max(this.y(suby));

refmat = makerefmat(xLeft, yTop, XStep, YStep);
worldfilename = getworldfilename(nomFic);

worldfilewrite(refmat, worldfilename);

% str1 = sprintf('Un fichier de g�or�f�rencement "ESRI World File" a �galement �t� g�n�r� : %s', worldfilename);
% str2 = sprintf('An "ESRI World file" has been created for this image : %s', worldfilename);
% fprintf('%s\n', Lang(str1,str2))
