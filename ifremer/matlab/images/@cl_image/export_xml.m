% Exportation dans un fichier ermapper
%
% Syntax
%   flag = export_xml(a, nomFic, ...)
%
% Input Arguments
%   a      : Instance de cl_image
%   nomFic : Nom du fichier image ermapper
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   flag  : 1=Reussite, 0=Echec
%
% Examples
%   % Exemple de MNT
%   nomFic = getNomFicDatabase('KBMA0504.ers')
%   [flag, c] = cl_image.import_ermapper(nomFic);
%   SonarScope(c)
%
%   nomFicOut = my_tempname
%   export_xml(c, nomFicOut)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
%   % Exemple d'image sonar
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   SonarScope(c) % Or imagesc(c)
%
%   nomFicOut = my_tempname
%   export_xml(c, nomFicOut)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
% See also export_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = export_xml(this, nomFic, varargin)

%{
% Avant le 1/1/2013
% ErMapper sert uniquement dans le cas d'une image RGB o� on doit �crire
% chaque ligne enti�re en Red, Green et blue et passer � la ligne suivante
% sinon les matrices 3D sont �crites leas canaux les uns apr�s les autres
[varargin, ErMapper] = getPropertyValue(varargin, 'ErMapper', 0);
%}


% TODO : ATTENTION DANGER, rajout� par JMA le 01/01/2013 pour donn�es MNT
% sauv�es dans une session et restaur�es sans l'attribut ErMapper (MNT
% de r�f�rence  Marc Roche pour EM2040)
[varargin, ErMapper] = getPropertyValue(varargin, 'ErMapper', []);
if isempty(ErMapper)
    if (this.GeometryType == cl_image.indGeometryType('LatLong')) || (this.GeometryType == cl_image.indGeometryType('GeoYX'))
        yDir = ((this.y(end) - this.y(1)) > 0);
        %     ErMapper = ((this.y(end) - this.y(1)) < 0);
        ErMapper = ~xor(yDir , (this.YDir == 1));
    else
        ErMapper = 0;
    end
end
% Fin ajout JMA du 07/07/2013

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

if (~isempty(subx) && ~isequal(subx, 1:this.nbColumns)) ...
        || (~isempty(suby) && ~isequal(suby, 1:this.nbRows))
    this = extraction(this, 'subx', subx, 'suby', suby);
end

that = update_Name(this);
Titre = that.Name;

[nomDirRacine, nomDir] = fileparts(nomFic);
nomFicXml = fullfile(nomDirRacine, [nomDir '.xml']);

nbSounders = max(1, size(this.Sonar.PingCounter, 2));

Carto = this.Carto;
if isempty(Carto)
    nomFicCarto = '';
else
    nomFicCarto = fullfile(nomDir, 'Carto.xml');
end

CourbesStatistiques = this.CourbesStatistiques;
if isempty(CourbesStatistiques)
    nomFicCurves = '';
else
    nomFicCurves = fullfile(nomDir, 'Curves.mat');
end

RegionOfInterest = this.RegionOfInterest;
if isempty(RegionOfInterest)
    nomFicROI = '';
else
    nomFicROI = fullfile(nomDir, 'RegionOfInterest.xml');
end

SonarDescription = this.Sonar.Desciption;
if isempty(SonarDescription)
    nomFicSonarDescription = '';
else
    nomFicSonarDescription = fullfile(nomDir, 'SonarDescription.mat');
end

BathyCel = get_SonarBathyCel(this);
if isfield(BathyCel, 'Z')
    if isempty(BathyCel.Z)
        nomFicBathyCel = '';
    else
        nomFicBathyCel = fullfile(nomDir, 'BathyCel.mat');
    end
elseif isfield(BathyCel, 'Depth')
    if isempty(BathyCel(1).Depth)
        nomFicBathyCel = '';
    else
        nomFicBathyCel = fullfile(nomDir, 'BathyCel.mat');
    end
end

Texture = this.Texture;
if isempty(Texture)
    nomFicTexture = '';
else
    nomFicTexture = fullfile(nomDir, 'Texture.mat');
end

SignalsVert = this.SignalsVert;
if isempty(SignalsVert)
    nomFicSignalsVert = '';
else
    nomFicSignalsVert = fullfile(nomDir, 'SignalsVert.mat');
end

%% G�n�ration du XML SonarScope

Info.Signature1 = 'SonarScope';
Info.Signature2 = 'Image';
Info.Title = Titre;
Info.Unit = this.Unit;
Info.ValNaN = this.ValNaN;
Info.DataType = cl_image.strDataType{this.DataType};
Info.GeometryType = this.strGeometryType{this.GeometryType};
Info.ImageType = this.strImageType{this.ImageType};
Info.XLabel = this.XLabel;
Info.XUnit = this.XUnit;
Info.XLim = this.XLim;
Info.XDir = this.strXDir{this.XDir};
Info.YLabel = this.YLabel;
Info.YUnit = this.YUnit;
Info.YLim = this.YLim;
Info.YDir = this.strYDir{this.YDir};
Info.ColormapIndex = this.strColormapIndex{this.ColormapIndex};
Info.CLim = this.CLim;
Info.Video = this.strVideo{this.Video};
Info.TagSynchroX = this.TagSynchroX;
Info.TagSynchroY = this.TagSynchroY;
Info.TagSynchroXScale = this.TagSynchroXScale;
Info.TagSynchroYScale = this.TagSynchroYScale;
Info.TagSynchroContrast = this.TagSynchroContrast;
Info.InitialFileName = this.InitialFileName;
Info.InitialFileFormat = this.InitialFileFormat;
Info.InitialImageName = this.InitialImageName;
Info.Comments = this.Comments;
Info.History  = this.History;

Info.SpectralStatus = this.strSpectralStatus{this.SpectralStatus};
Info.SaveParamsIfFFT = this.SaveParamsIfFFT;
Info.OriginColorbar = this.OriginColorbar;

if isfield(this.Sonar, 'SampleBeamData')
    Info.SampleBeamData.SampleRate          = this.Sonar.SampleBeamData.SampleRate;
    Info.SampleBeamData.SoundVelocity       = this.Sonar.SampleBeamData.SoundVelocity;
    % Info.SampleBeamData.SystemSerialNumber  = this.Sonar.SampleBeamData.SystemSerialNumber(:,:);
    Info.SampleBeamData.SystemSerialNumber  = this.Sonar.SampleBeamData.SystemSerialNumber(1);
    if isfield(this.Sonar.SampleBeamData, 'Frequency')
        Info.SampleBeamData.Frequency       = this.Sonar.SampleBeamData.Frequency;
    else
        Info.SampleBeamData.Frequency       = [];
    end
    T = this.Sonar.SampleBeamData.Time;
    Info.SampleBeamData.Time                = T.timeMat;
    Info.SampleBeamData.PingCounter         = this.Sonar.SampleBeamData.PingCounter;
    
    if isfield(this.Sonar.SampleBeamData, 'R0')
        Info.SampleBeamData.R0 = this.Sonar.SampleBeamData.R0;
    else
        Info.SampleBeamData.R0 = [];
    end
    
    if isfield(this.Sonar.SampleBeamData, 'MultiPingSequence')
        Info.SampleBeamData.MultiPingSequence  = this.Sonar.SampleBeamData.MultiPingSequence;
    else
        Info.SampleBeamData.MultiPingSequence  = [];
    end
    % Info.SampleBeamData.AmpPingNormHorzMax  = this.Sonar.SampleBeamData.AmpPingNormHorzMax;
    
    if isfield(this.Sonar.SampleBeamData, 'iBeamBeg')
        Info.SampleBeamData.iBeamBeg = this.Sonar.SampleBeamData.iBeamBeg;
    else
        Info.SampleBeamData.iBeamBeg = [];
    end
    
    if isfield(this.Sonar.SampleBeamData, 'iBeamBeg')
        Info.SampleBeamData.iBeamEnd = this.Sonar.SampleBeamData.iBeamBeg;
    else
        Info.SampleBeamData.iBeamEnd = [];
    end
    
    Info.SampleBeamData.TxPulseWidth        = this.Sonar.SampleBeamData.TxPulseWidth;
    
    if isfield(this.Sonar.SampleBeamData, 'ReceiveBeamWidth')
        Info.SampleBeamData.ReceiveBeamWidth = this.Sonar.SampleBeamData.ReceiveBeamWidth;
    else
        Info.SampleBeamData.ReceiveBeamWidth = [];
    end
    
    if isfield(this.Sonar.SampleBeamData, 'iBeamMax0')
        Info.SampleBeamData.iBeamMax0 = this.Sonar.SampleBeamData.iBeamMax0;
    else
        Info.SampleBeamData.iBeamMax0 = [];
    end
    
    % Info.SampleBeamData.TxAngle             = this.Sonar.SampleBeamData.TxAngle;
    % Info.SampleBeamData.RMax1               = this.Sonar.SampleBeamData.RMax1;
    % Info.SampleBeamData.RMax2               = this.Sonar.SampleBeamData.RMax2;
    % Info.SampleBeamData.MaskWidth           = this.Sonar.SampleBeamData.MaskWidth;
    
    if isfield(this.Sonar.SampleBeamData, 'SamplingRateMinMax')
        Info.SampleBeamData.SamplingRateMinMax  = this.Sonar.SampleBeamData.SamplingRateMinMax;
    else
        Info.SampleBeamData.SamplingRateMinMax = [];
    end
    
    if isfield(this.Sonar.SampleBeamData, 'Latitude')
        Info.SampleBeamData.Latitude  = this.Sonar.SampleBeamData.Latitude;
    else
        Info.SampleBeamData.Latitude = [];
    end
    
    if isfield(this.Sonar.SampleBeamData, 'Longitude')
        Info.SampleBeamData.Longitude  = this.Sonar.SampleBeamData.Longitude;
    else
        Info.SampleBeamData.Longitude = [];
    end
    
    if isfield(this.Sonar.SampleBeamData, 'Heading')
        Info.SampleBeamData.Heading  = this.Sonar.SampleBeamData.Heading;
    else
        Info.SampleBeamData.Heading = [];
    end
    % if isfield(this.Sonar.SampleBeamData, 'TransmitSectorNumber')
    %     Info.SampleBeamData.TransmitSectorNumber = this.Sonar.SampleBeamData.TransmitSectorNumber;
    % else
    %     Info.SampleBeamData.TransmitSectorNumber = [];
    % end
end

Info.StatValues             = this.StatValues;
Info.nomFicCarto            = nomFicCarto;
Info.nomFicCurves           = nomFicCurves;
Info.nomFicROI              = nomFicROI;
Info.nomFicSonarDescription = nomFicSonarDescription;
Info.nomFicBathyCel         = nomFicBathyCel;
Info.nomFicTexture          = nomFicTexture;
Info.nomFicSignalsVert      = nomFicSignalsVert;
Info.ErMapper               = ErMapper;

% Info.TimeOrigin   = '01/01/-4713';
Info.Comments = 'Image exported by SonarScope';
Info.Version  = 20091212;

if ~isempty(this.Sonar) && ~isempty(this.Sonar.RawDataResol)
    Info.Sonar.RawDataResol = this.Sonar.RawDataResol;
end
if ~isempty(this.Sonar) && ~isempty(this.Sonar.ResolutionD)
    Info.Sonar.ResolutionD = this.Sonar.ResolutionD;
end
if ~isempty(this.Sonar) && ~isempty(this.Sonar.ResolutionX)
    Info.Sonar.ResolutionX = this.Sonar.ResolutionX;
end

%% ReflectivityStatus

if ~isempty(this.Sonar) && (this.DataType == cl_image.indDataType('Reflectivity'))
    Info.Sonar.TVG      = this.Sonar.TVG;
    Info.Sonar.DiagEmi  = this.Sonar.DiagEmi;
    Info.Sonar.DiagRec  = this.Sonar.DiagRec;
    Info.Sonar.AireInso = this.Sonar.AireInso;
    Info.Sonar.NE       = this.Sonar.NE;
    Info.Sonar.SH       = this.Sonar.SH;
    Info.Sonar.GT       = this.Sonar.GT;
    Info.Sonar.BS       = this.Sonar.BS;
    Info.Sonar.Ship     = this.Sonar.Ship;
end

if isfield(Info, 'Sonar') && isfield(Info.Sonar, 'BS') && isfield(Info.Sonar.BS, 'IfremerCompensModele') && isempty(Info.Sonar.BS.IfremerCompensModele)
    Info.Sonar.BS.IfremerCompensModele = []; % car CmModel [0,0] ou
end

%% Dimensions

Info.Dimensions.nbRows               = this.nbRows;
Info.Dimensions.nbColumns            = this.nbColumns;
Info.Dimensions.nbChannels           = this.nbSlides;
Info.Dimensions.nbSounders           = nbSounders; % Tester sur sondeur dual
Info.Dimensions.lengthColormap       = size(this.Colormap,1);
Info.Dimensions.lengthColormapCustom = size(this.ColormapCustom,1);
Info.Dimensions.lengthContourValues  = length(this.ContourValues);
Info.Dimensions.lengthHistoValues    = length(this.HistoValues);

%% Signaux

switch class(this.HistoCentralClasses(1))
    case 'double'
        Storage = 'double';
    otherwise
        Storage = 'single';
end

Info.Signals(1).Name          = 'x';
Info.Signals(end).Dimensions  = '1, nbColumns';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = Info.XUnit;
Info.Signals(end).FileName    = fullfile(nomDir, 'x.bin');
Info.Signals(end).Tag         = verifKeyWord('X');

Info.Signals(end+1).Name      = 'y';
Info.Signals(end).Dimensions  = 'nbRows, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = Info.YUnit;
Info.Signals(end).FileName    = fullfile(nomDir, 'y.bin');
Info.Signals(end).Tag         = verifKeyWord('Y');

Info.Signals(end+1).Name      = 'Colormap';
Info.Signals(end).Dimensions  = 'lengthColormap, 3';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDir, 'Colormap.bin');
Info.Signals(end).Tag         = verifKeyWord('Colormap');

Info.Signals(end+1).Name      = 'ColormapCustom';
Info.Signals(end).Dimensions  = 'lengthColormapCustom, 3';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDir, 'ColormapCustom.bin');
Info.Signals(end).Tag         = verifKeyWord('ColormapCustom');

Info.Signals(end+1).Name      = 'ContourValues';
Info.Signals(end).Dimensions  = '1, lengthContourValues';
Info.Signals(end).Storage     = Storage;
Info.Signals(end).Unit        = Info.Unit;
Info.Signals(end).FileName    = fullfile(nomDir, 'ContourValues.bin');
Info.Signals(end).Tag         = verifKeyWord('ContourValues');

Info.Signals(end+1).Name      = 'HistoValues';
Info.Signals(end).Dimensions  = '1, lengthHistoValues';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDir, 'HistoValues.bin');
Info.Signals(end).Tag         = verifKeyWord('HistoValues');

Info.Signals(end+1).Name      = 'HistoCentralClasses';
Info.Signals(end).Dimensions  = '1, lengthHistoValues';
Info.Signals(end).Storage     = Storage;
Info.Signals(end).Unit        = Info.Unit;
Info.Signals(end).FileName    = fullfile(nomDir, 'HistoCentralClasses.bin');
Info.Signals(end).Tag         = verifKeyWord('HistoCentralClasses');

nbSignauxNaturels = length(Info.Signals);

if isSonarSignal(this, 'Time')
    Info.Signals(end+1).Name = 'Time';
    T = this.Sonar.Time;
    if isa(T, 'cl_time')
        T = this.Sonar.Time.timeMat;
    else
        T = this.Sonar.Time; % On ne devrait pas passer par l� ! C'est arriv� en faisant un export session de donn�es de WC (PingBeam et DepthAcrossDist)
    end
    if size(T, 2) == 1
        Info.Signals(end).Dimensions = 'nbRows, 1';
    else
        Info.Signals(end).Dimensions = 'nbRows, nbSounders';
    end
    Info.Signals(end).Storage  = 'double';
    Info.Signals(end).Unit     = 'days since JC';
    Info.Signals(end).FileName = fullfile(nomDir, 'Time.bin');
    Info.Signals(end).Tag      = verifKeyWord('SounderTime');
end

if isSonarSignal(this, 'PingCounter')
    Info.Signals(end+1).Name      = 'PingCounter';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'PingCounter.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');
end

if isSonarSignal(this, 'Heading')
    Info.Signals(end+1).Name      = 'Heading';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).FileName    = fullfile(nomDir,'Heading.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderHeading');
end

if isSonarSignal(this, 'Immersion')
    Info.Signals(end+1).Name      = 'Immersion';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile(nomDir,'Immersion.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderImmersion');
end

if isSonarSignal(this, 'Height')
    Info.Signals(end+1).Name      = 'Height';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile(nomDir,'Height.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderHeight');
end

if isSonarSignal(this, 'Roll')
    Info.Signals(end+1).Name      = 'Roll';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).FileName    = fullfile(nomDir,'Roll.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderRoll');
end

if isSonarSignal(this, 'Pitch')
    Info.Signals(end+1).Name      = 'Pitch';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).FileName    = fullfile(nomDir,'Pitch.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderPitch');
end

if isSonarSignal(this, 'SurfaceSoundSpeed')
    Info.Signals(end+1).Name      = 'SurfaceSoundSpeed';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm/s';
    Info.Signals(end).FileName    = fullfile(nomDir,'SurfaceSoundSpeed.bin');
    Info.Signals(end).Tag         = verifKeyWord('SurfaceSoundSpeed');
end

if isSonarSignal(this, 'Heave')
    Info.Signals(end+1).Name      = 'Heave';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).FileName    = fullfile(nomDir,'Heave.bin');
    Info.Signals(end).Tag         = verifKeyWord('Heave');
end

if isSonarSignal(this, 'SampleFrequency')
    Info.Signals(end+1).Name      = 'SampleFrequency';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'Hz';
    Info.Signals(end).FileName    = fullfile(nomDir,'SampleFrequency.bin');
    Info.Signals(end).Tag         = verifKeyWord('SampleFrequency');
end

if isSonarSignal(this, 'Interlacing')
    Info.Signals(end+1).Name      = 'Interlacing';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'Interlacing.bin');
    Info.Signals(end).Tag         = verifKeyWord('Interlacing');
end

if isSonarSignal(this, 'PresenceWC')
    Info.Signals(end+1).Name      = 'PresenceWC';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'PresenceWC.bin');
    Info.Signals(end).Tag         = verifKeyWord('PresenceWC');
end

if isSonarSignal(this, 'PresenceFM')
    Info.Signals(end+1).Name      = 'PresenceFM';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'PresenceFM.bin');
    Info.Signals(end).Tag         = verifKeyWord('PresenceFM');
end

if isSonarSignal(this, 'DistPings')
    Info.Signals(end+1).Name      = 'DistPings';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'DistPings.bin');
    Info.Signals(end).Tag         = verifKeyWord('DistPings');
end

if isSonarSignal(this, 'NbSwaths')
    Info.Signals(end+1).Name      = 'NbSwaths';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'NbSwaths.bin');
    Info.Signals(end).Tag         = verifKeyWord('NbSwaths');
end

if isSonarSignal(this, 'BSN')
    Info.Signals(end+1).Name      = 'BSN';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'BSN.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
end

if isSonarSignal(this, 'BSO')
    Info.Signals(end+1).Name      = 'BSO';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'BSO.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
end

if isSonarSignal(this, 'TVGN')
    Info.Signals(end+1).Name      = 'TVGN';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'TVGN.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
end

if isSonarSignal(this, 'TVGCrossOver')
    Info.Signals(end+1).Name      = 'TVGCrossOver';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'TVGCrossOver.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
end

if isSonarSignal(this, 'TxBeamWidth')
    Info.Signals(end+1).Name      = 'TxBeamWidth';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'TxBeamWidth.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
end

if isSonarSignal(this, 'ResolAcrossSample')
    Info.Signals(end+1).Name      = 'ResolAcrossSample';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'ResolAcrossSample.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
end


if isSonarSignal(this, 'CableOut')
    Info.Signals(end+1).Name      = 'CableOut';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'CableOut.bin');
    Info.Signals(end).Tag         = verifKeyWord('CableOut');
end

if isSonarSignal(this, 'Speed')
    Info.Signals(end+1).Name      = 'Speed';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'Speed.bin');
    Info.Signals(end).Tag         = verifKeyWord('Speed');
end

if isSonarSignal(this, 'RollUsedForEmission')
    Info.Signals(end+1).Name      = 'RollUsedForEmission';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'RollUsedForEmission.bin');
    Info.Signals(end).Tag         = verifKeyWord('RollUsedForEmission');
end

if isSonarSignal(this, 'Yaw')
    Info.Signals(end+1).Name      = 'Yaw';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'Yaw.bin');
    Info.Signals(end).Tag         = verifKeyWord('Yaw');
end

if isSonarSignal(this, 'Tide')
    Info.Signals(end+1).Name      = 'Tide';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'Tide.bin');
    Info.Signals(end).Tag         = verifKeyWord('Tide');
end

% TODO : regarder pourquoi on "SonarFrequency" et non pas "Frequency"
% Supprimer le if dans import_xml (TODO : Pour corriger une mal donne :
% faire le bilan de cette affaire)


if isSonarSignal(this, 'SonarFrequency')
    Info.Signals(end+1).Name      = 'SonarFrequency';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'SonarFrequency.bin');
    Info.Signals(end).Tag         = verifKeyWord('SonarFrequency');
end

if isSonarSignal(this, 'TxAlongAngle')
    Info.Signals(end+1).Name      = 'TxAlongAngle';
    Info.Signals(end).Dimensions  = 'nbRows, nbSounders';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'TxAlongAngle.bin');
    Info.Signals(end).Tag         = verifKeyWord('TxAlongAngle');
end

% Rassembler les 2 modes (port & starboard) en signal 'nbRows, nbSounders'
if isSonarSignal(this, 'PortMode_1')
    Info.Signals(end+1).Name      = 'PortMode_1';
    Info.Signals(end).Dimensions  = 'nbRows, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'PortMode_1.bin');
    Info.Signals(end).Tag         = verifKeyWord('Mode');
end

if isSonarSignal(this, 'PortMode_2')
    Info.Signals(end+1).Name      = 'PortMode_2';
    Info.Signals(end).Dimensions  = 'nbRows, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'PortMode_2.bin');
    Info.Signals(end).Tag         = verifKeyWord('Mode');
end

if isSonarSignal(this, 'StarMode_1')
    Info.Signals(end+1).Name      = 'StarMode_1';
    Info.Signals(end).Dimensions  = 'nbRows, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile(nomDir,'StarMode_1.bin');
    Info.Signals(end).Tag         = verifKeyWord('Mode');
end

if isSonarSignal(this, 'StarMode_2')
    Info.Signals(end+1).Name     = 'StarMode_2';
    Info.Signals(end).Dimensions = 'nbRows, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = '';
    Info.Signals(end).FileName   = fullfile(nomDir,'StarMode_2.bin');
    Info.Signals(end).Tag        = verifKeyWord('Mode');
end

if ~isempty(this.Sonar.EM2040Mode2)
    Info.Signals(end+1).Name     = 'EM2040Mode2';
    Info.Signals(end).Dimensions = 'nbRows, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = '';
    Info.Signals(end).FileName   = fullfile(nomDir,'EM2040Mode2.bin');
    Info.Signals(end).Tag        = verifKeyWord('TODO');
end

if ~isempty(this.Sonar.EM2040Mode2)
    Info.Signals(end+1).Name     = 'EM2040Mode3';
    Info.Signals(end).Dimensions = 'nbRows, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = '';
    Info.Signals(end).FileName   = fullfile(nomDir,'EM2040Mode3.bin');
    Info.Signals(end).Tag        = verifKeyWord('TODO');
end


if isSonarSignal(this, 'FishLatitude')
    Info.Signals(end+1).Name     = 'FishLatitude';
    Info.Signals(end).Dimensions = 'nbRows, nbSounders';
    Info.Signals(end).Storage    = 'double';
    Info.Signals(end).Unit       = 'deg';
    Info.Signals(end).FileName   = fullfile(nomDir,'FishLatitude.bin');
    Info.Signals(end).Tag        = verifKeyWord('FishLatitude');
end

if isSonarSignal(this, 'FishLongitude')
    Info.Signals(end+1).Name     = 'FishLongitude';
    Info.Signals(end).Dimensions = 'nbRows, nbSounders';
    Info.Signals(end).Storage    = 'double';
    Info.Signals(end).Unit       = 'deg';
    Info.Signals(end).FileName   = fullfile(nomDir,'FishLongitude.bin');
    Info.Signals(end).Tag        = verifKeyWord('FishLongitude');
end

if isSonarSignal(this, 'ShipLatitude')
    Info.Signals(end+1).Name     = 'ShipLatitude';
    Info.Signals(end).Dimensions = 'nbRows, nbSounders';
    Info.Signals(end).Storage    = 'double';
    Info.Signals(end).Unit       = 'deg';
    Info.Signals(end).FileName   = fullfile(nomDir,'ShipLatitude.bin');
    Info.Signals(end).Tag        = verifKeyWord('ShipLatitude');
end

if isSonarSignal(this, 'ShipLongitude')
    Info.Signals(end+1).Name     = 'ShipLongitude';
    Info.Signals(end).Dimensions = 'nbRows, nbSounders';
    Info.Signals(end).Storage    = 'double';
    Info.Signals(end).Unit       = 'deg';
    Info.Signals(end).FileName   = fullfile(nomDir,'ShipLongitude.bin');
    Info.Signals(end).Tag        = verifKeyWord('ShipLongitude');
end
nbSignauxVerticaux = length(Info.Signals) - nbSignauxNaturels;

if isfield(this.Sonar, 'SampleBeamData')
    if ~isempty(this.Sonar.SampleBeamData.R1SamplesFiltre)
        Info.Dimensions.nbBeams  = length(this.Sonar.SampleBeamData.R1SamplesFiltre);
        Info.Signals(end+1).Name     = 'R1SamplesFiltre';
        Info.Signals(end).Dimensions = '1, nbBeams';
        Info.Signals(end).Storage    = 'single';
        Info.Signals(end).Unit       = '';
        Info.Signals(end).FileName   = fullfile(nomDir,'R1SamplesFiltre.bin');
        Info.Signals(end).Tag        = verifKeyWord('TODO');
    end
    
    % if ~isempty(this.Sonar.SampleBeamData.SystemSerialNumber)
    %     this.Sonar.SampleBeamData.SystemSerialNumber = this.Sonar.SampleBeamData.SystemSerialNumber(:,:);
    %     Info.Signals(end+1).Name     = 'SystemSerialNumber'; % Danger de collision avec signal vertical
    %     Info.Signals(end).Dimensions = 'nbRows, 1';
    %     Info.Signals(end).Storage    = 'single';
    %     Info.Signals(end).Unit       = '';
    %     Info.Signals(end).FileName   = fullfile(nomDir,'SystemSerialNumber.bin');
    %     Info.Signals(end).Tag        = verifKeyWord('TODO');
    % end
    
    if isfield(this.Sonar.SampleBeamData, 'AmpPingNormHorzMax') && ~isempty(this.Sonar.SampleBeamData.AmpPingNormHorzMax)
        Info.Signals(end+1).Name     = 'AmpPingNormHorzMax';
        Info.Signals(end).Dimensions = '1, nbBeams';
        Info.Signals(end).Storage    = 'single';
        Info.Signals(end).Unit       = '';
        Info.Signals(end).FileName   = fullfile(nomDir,'AmpPingNormHorzMax.bin');
        Info.Signals(end).Tag        = verifKeyWord('TODO');
    end
    
    if isfield(this.Sonar.SampleBeamData, 'RxAnglesEarth_WC')
        this.Sonar.SampleBeamData.TxAngle = this.Sonar.SampleBeamData.RxAnglesEarth_WC;
    end
    
    if ~isempty(this.Sonar.SampleBeamData.TxAngle)
        Info.Signals(end+1).Name     = 'TxAngle'; % Danger de collision avec signal vertical
        Info.Signals(end).Dimensions = '1, nbBeams';
        Info.Signals(end).Storage    = 'single';
        Info.Signals(end).Unit       = '';
        Info.Signals(end).FileName   = fullfile(nomDir,'TxAngle.bin');
        Info.Signals(end).Tag        = verifKeyWord('TODO');
    end
    
    if isfield(this.Sonar.SampleBeamData, 'RMax1') && ~isempty(this.Sonar.SampleBeamData.RMax1)
        Info.Signals(end+1).Name     = 'RMax1';
        Info.Signals(end).Dimensions = '1, nbBeams';
        Info.Signals(end).Storage    = 'single';
        Info.Signals(end).Unit       = '';
        Info.Signals(end).FileName   = fullfile(nomDir,'RMax1.bin');
        Info.Signals(end).Tag        = verifKeyWord('TODO');
    end
    
    if isfield(this.Sonar.SampleBeamData, 'RMax2') && ~isempty(this.Sonar.SampleBeamData.RMax2)
        Info.Signals(end+1).Name     = 'RMax2';
        Info.Signals(end).Dimensions = '1, nbBeams';
        Info.Signals(end).Storage    = 'single';
        Info.Signals(end).Unit       = '';
        Info.Signals(end).FileName   = fullfile(nomDir,'RMax2.bin');
        Info.Signals(end).Tag        = verifKeyWord('TODO');
    end
    
    if isfield(this.Sonar.SampleBeamData, 'MaskWidth') && ~isempty(this.Sonar.SampleBeamData.MaskWidth)
        Info.Signals(end+1).Name     = 'MaskWidth';
        Info.Signals(end).Dimensions = '1, nbBeams';
        Info.Signals(end).Storage    = 'single';
        Info.Signals(end).Unit       = '';
        Info.Signals(end).FileName   = fullfile(nomDir,'MaskWidth.bin');
        Info.Signals(end).Tag        = verifKeyWord('TODO');
    end
    
    if ~isempty(this.Sonar.SampleBeamData.TransmitSectorNumber)
        Info.Signals(end+1).Name     = 'TransmitSectorNumber';
        Info.Signals(end).Dimensions = '1, nbBeams';
        Info.Signals(end).Storage    = 'single';
        Info.Signals(end).Unit       = '';
        Info.Signals(end).FileName   = fullfile(nomDir,'TransmitSectorNumber.bin');
        Info.Signals(end).Tag        = verifKeyWord('TODO');
    end
    
end

nbSignauxHorizontaux = length(Info.Signals) - nbSignauxNaturels - nbSignauxVerticaux;

%% Images

Info.Images(1).Name         = Titre;
Info.Images(end).Dimensions = 'nbRows, nbColumns, nbChannels';
Info.Images(end).Storage    = class(this.Image(1));
Info.Images(end).Unit       = Info.Unit;
Info.Images(end).FileName   = fullfile(nomDir, 'Image.bin');
if ErMapper
    Info.Images(end).Direction = 'FirstValue=FirstPing';
end
% Info.Images(end).Tag          = verifKeyWord('SounderDepth');

%% Cr�ation du fichier XML d�crivant la donn�e

xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire

nomDirBin = fullfile(nomDirRacine, nomDir);
if ~exist(nomDirBin, 'dir')
    status = mkdir(nomDirBin);
    if ~status
        messageErreur(nomDirBin)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k1=1:nbSignauxNaturels
    flag = writeSignal(nomDirRacine, Info.Signals(k1), this.(Info.Signals(k1).Name));
    if ~flag
        %         switch Info.Signals(k1).Name
        %             case {'HistoValues'; 'HistoCentralClasses'}
        %             otherwise
        return
        %         end
    end
end

% for k1=1:(length(Info.Signals)-nbSignauxNaturels)
for k1=1:nbSignauxVerticaux
    k2 = k1+nbSignauxNaturels;
    flag = writeSignal(nomDirRacine, Info.Signals(k2), this.Sonar.(Info.Signals(k2).Name));
    if ~flag
        return
    end
end

for k1=1:nbSignauxHorizontaux
    k2 = k1 + nbSignauxNaturels + nbSignauxVerticaux;
    flag = writeSignal(nomDirRacine, Info.Signals(k2), this.Sonar.SampleBeamData.(Info.Signals(k2).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k1=1:length(Info.Images)
    pppp = this.y;
    pppp = pppp(:);
    AlreadyFlipped = xor((pppp(end) > pppp(1)), (this.YDir == 1));
    flag = writeImage(nomDirRacine, Info.Images(k1), this.Image, 'ErMapper', ErMapper, 'AlreadyFlipped', AlreadyFlipped);
    if ~flag
        return
    end
end

%% Exportation de la cartographie

Carto = this.Carto;
if ~isempty(Carto)
    nomFicCarto = fullfile(nomDirRacine, nomDir, 'Carto.xml');
    flag = export_xml(Carto, nomFicCarto);
    if ~flag
        return
    end
end

%% Exportation des courbes statistiques

CourbesStatistiques = this.CourbesStatistiques;
if ~isempty(CourbesStatistiques)
    %     nomFicCurves = fullfile(nomDirRacine, nomDir, 'Curves.xml');
    %     flag = export_xml_courbesStats(this, nomFicCurves); %#ok<NASGU>
    
    nomFicCurves = fullfile(nomDirRacine, nomDir, 'Curves.mat');
    exportCourbesStats(this, nomFicCurves, 'Mute', 1); %, 'Tag', 'BS');
end

%% Exportation des Regions Of Interest

RegionOfInterest = this.RegionOfInterest;
if ~isempty(RegionOfInterest)
    nomFicROI = fullfile(nomDirRacine, nomDir, 'RegionOfInterest.xml');
    this = ROI_union(this);
    RegionOfInterest = this.RegionOfInterest;
    flag = ROI_export_xml(RegionOfInterest, nomFicROI);
end

flag = 1;

%% TODO : Reste � faire

Sonar = this.Sonar.Desciption;
if ~isempty(Sonar)
    nomFicSonarDescription = fullfile(nomDirRacine, nomDir, 'SonarDescription.mat');
    save(nomFicSonarDescription, 'Sonar')
end

BathyCel = get_SonarBathyCel(this);
if isfield(BathyCel, 'Z')
    if ~isempty(BathyCel.Z)
        nomFicBathyCel = fullfile(nomDirRacine, nomDir, 'BathyCel.mat');
        % Ajout JMA le 30/09/2013 : pas v�rifi�
        try
            BathyCel.Z = BathyCel.Z(:,:);
            BathyCel.T = BathyCel.T(:,:);
            BathyCel.S = BathyCel.S(:,:);
            BathyCel.C = BathyCel.C(:,:);
            save(nomFicBathyCel, 'BathyCel')
        catch %#ok<CTCH>
        end
    end
elseif isfield(BathyCel, 'Depth')
    if ~isempty(BathyCel(1).Depth)
        nomFicBathyCel = fullfile(nomDirRacine, nomDir, 'BathyCel.mat');
        % Ajout JMA le 30/09/2013 : pas v�rifi�
        try
            BathyCel.Depth      = BathyCel.Depth(:,:);
            BathyCel.SoundSpeed = BathyCel.SoundSpeed(:,:);
            save(nomFicBathyCel, 'BathyCel')
        catch %#ok<CTCH>
        end
    end
end

%% Exportation des signatures texturales

Texture = this.Texture;
if ~isempty(Texture)
    nomFicTexture = fullfile(nomDirRacine, nomDir, 'Texture.mat');
    save(nomFicTexture, 'Texture')
end

%% Exportation des signaux verticaux nouvelle g�n�ration

SignalsVert = this.SignalsVert;
if ~isempty(SignalsVert)
    nomFicSignalsVert = fullfile(nomDirRacine, nomDir, 'SignalsVert.mat');
    save(nomFicSignalsVert, 'SignalsVert')
end
