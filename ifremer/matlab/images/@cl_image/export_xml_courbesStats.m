% Export the statistical curves of an image in an .xml file
%
% Syntax
%   flag = export_xml_courbesStats(this, CurvesFilename, ...)
%
% Input Arguments
%   a              : An instance of cl_image
%   CurvesFilename : File name for the statistical curves
%
% Name-Value Pair Arguments
%   sub : Index of the curves to export (default) all the curves)
%
% Name-only Arguments
%   last : Only the las curve will be saved
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
%     aKM = cl_simrad_all('nomFic', nomFic);
%     [flag, a] = import_DefaultLayers(aKM);
%     SonarScope(a)
%
%     DTReflec = cl_image.indDataType('Reflectivity');
%     [kReflec, nomLayerReflec] = findIndLayerSonar(a, 1, 'DataType', DTReflec, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTAngles = cl_image.indDataType('TxAngle');
%     [kAngles, nomLayerAngles] = findIndLayerSonar(a, 1, 'DataType', DTAngles, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTSectors = cl_image.indDataType('TxBeamIndex');
%     [kSectors, nomLayerSectors] = findIndLayerSonar(a, 1, 'DataType', DTSectors, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     Selection(1).Name     = nomLayerAngles;
%     Selection(1).TypeVal  = 'Layer'
%     Selection(1).indLayer = kAngles;
%     Selection(1).DataType = DTAngles;
%     Selection(2).Name     = nomLayerSectors;
%     Selection(2).TypeVal  = 'Layer'
%     Selection(2).indLayer = kSectors;
%     Selection(2).DataType = DTSectors;
%     bins{1} = -75:75;
%     bins{2} = 1:3;
%     [a(kReflec), bilan] = courbesStats(a(kReflec), nomLayerReflec, 'Courbe test', '', [], ...
%         a, Selection, 'bins', bins, 'GeometryType', 'PingBeam', 'Sampling' , 1);
%     CurvesFilename = my_tempname('.mat');
%   flag = export_xml_courbesStats(a(kReflec), CurvesFilename)
%     a(kReflec) = deleteCourbesStats(a(kReflec));
%
%     [flag, a(kReflec)] = import_xml_courbesStats(a(kReflec), CurvesFilename);
%     plotCourbesStats(a(kReflec), 'Stats', 0);
%
% See also cl_image/import_xml_courbesStats Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function flag = export_xml_courbesStats(this, CurvesFilename, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));
[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if isempty(sub)
    flag = 0;
    return
end

if ~isempty(CurvesFilename)
    flag = export_local(CurvesFilename, this.CourbesStatistiques(sub));
end


function flag = export_local(CurvesFilename, Courbes)

[nomDir, nomFic] = fileparts(CurvesFilename);
nomFicXml = fullfile(nomDir, [nomFic '.xml']);
nomDirBin = fullfile(nomDir, nomFic);
nomDir = nomFic;

%% G�n�ration du XML SonarScope

Info.Signature1 = 'SonarScope';
Info.Signature2 = 'Statistic Curves';

%% Dimensions

Info.Dimensions.nbCurves = length(Courbes);

%% Courbes

for k=1:Info.Dimensions.nbCurves
    Info.Curve(k).Dimensions.nbGroups = size(Courbes(k).bilan{1},2);
    
    Info.Curve(k).nomX                  = Courbes(k).bilan{1}(1).nomX;
    Info.Curve(k).titreV                = Courbes(k).bilan{1}(1).titreV;
    Info.Curve(k).nomZoneEtude          = Courbes(k).bilan{1}(1).nomZoneEtude;
    Info.Curve(k).commentaire           = Courbes(k).bilan{1}(1).commentaire;
    Info.Curve(k).Tag                   = Courbes(k).bilan{1}(1).Tag;
    Info.Curve(k).DataTypeValue         = Courbes(k).bilan{1}(1).DataTypeValue;
    Info.Curve(k).GeometryType          = Courbes(k).bilan{1}(1).GeometryType;
    Info.Curve(k).TypeCourbeStat        = Courbes(k).bilan{1}(1).TypeCourbeStat;
    Info.Curve(k).TagSup                = Courbes(k).bilan{1}(1).TagSup;
    Info.Curve(k).DataTypeConditions    = Courbes(k).bilan{1}(1).DataTypeConditions;
    Info.Curve(k).Support               = Courbes(k).bilan{1}(1).Support;
    if isfield(Courbes(k).bilan{1}(1), 'Unit')
        Info.Curve(k).Unit = Courbes(k).bilan{1}(1).Unit;
    else
        Info.Curve(k).Unit = '';
    end
    if isfield(Courbes(k).bilan{1}(1), 'nomVarCondition')
        Info.Curve(k).nomVarCondition = Courbes(k).bilan{1}(1).nomVarCondition;
    end
    Info.Curve(k).NY = Courbes(k).bilan{1}(1).NY;
    
    for iGroup=1:Info.Curve(k).Dimensions.nbGroups
        Info.Curve(k).Group(iGroup).Dimensions.nbSamples = length(Courbes(k).bilan{1}(iGroup).x);
        
        if isfield(Courbes(k).bilan{1}(1), 'numVarCondition')
            Info.Curve(k).Group(iGroup).numVarCondition = Courbes(k).bilan{1}(iGroup).numVarCondition;
        end
        Info.Curve(k).Group(iGroup).Biais           = Courbes(k).bilan{1}(iGroup).Biais;
        
        Info.Curve(k).Group(iGroup).Signals(1).Name        = 'x';
        Info.Curve(k).Group(iGroup).Signals(1).Dimensions  = '1, nbSamples';
        Info.Curve(k).Group(iGroup).Signals(1).Storage     = 'single';
        Info.Curve(k).Group(iGroup).Signals(1).Unit        = '';
        Info.Curve(k).Group(iGroup).Signals(1).FileName    = fullfile(nomDir, ['x_' num2str(k) '_' num2str(iGroup) '.bin']);
        Info.Curve(k).Group(iGroup).Signals(1).Tag         = verifKeyWord('X');
        
        Info.Curve(k).Group(iGroup).Signals(2).Name        = 'y';
        Info.Curve(k).Group(iGroup).Signals(2).Dimensions  = '1, nbSamples';
        Info.Curve(k).Group(iGroup).Signals(2).Storage     = 'single';
        Info.Curve(k).Group(iGroup).Signals(2).Unit        = '';
        Info.Curve(k).Group(iGroup).Signals(2).FileName    = fullfile(nomDir, ['y_' num2str(k) '_' num2str(iGroup) '.bin']);
        Info.Curve(k).Group(iGroup).Signals(2).Tag         = verifKeyWord('Y');
        
        Info.Curve(k).Group(iGroup).Signals(3).Name        = 'ymedian';
        Info.Curve(k).Group(iGroup).Signals(3).Dimensions  = '1, nbSamples';
        Info.Curve(k).Group(iGroup).Signals(3).Storage     = 'single';
        Info.Curve(k).Group(iGroup).Signals(3).Unit        = '';
        Info.Curve(k).Group(iGroup).Signals(3).FileName    = fullfile(nomDir, ['ymedian_' num2str(k) '_' num2str(iGroup) '.bin']);
        Info.Curve(k).Group(iGroup).Signals(3).Tag         = verifKeyWord('Y');
        
        Info.Curve(k).Group(iGroup).Signals(4).Name        = 'sub';
        Info.Curve(k).Group(iGroup).Signals(4).Dimensions  = '1, nbSamples';
        Info.Curve(k).Group(iGroup).Signals(4).Storage     = 'single';
        Info.Curve(k).Group(iGroup).Signals(4).Unit        = '';
        Info.Curve(k).Group(iGroup).Signals(4).FileName    = fullfile(nomDir, ['sub_' num2str(k) '_' num2str(iGroup) '.bin']);
        Info.Curve(k).Group(iGroup).Signals(4).Tag         = verifKeyWord('TODO');
        
        Info.Curve(k).Group(iGroup).Signals(5).Name        = 'nx';
        Info.Curve(k).Group(iGroup).Signals(5).Dimensions  = '1, nbSamples';
        Info.Curve(k).Group(iGroup).Signals(5).Storage     = 'single';
        Info.Curve(k).Group(iGroup).Signals(5).Unit        = '';
        Info.Curve(k).Group(iGroup).Signals(5).FileName    = fullfile(nomDir, ['nx_' num2str(k) '_' num2str(iGroup) '.bin']);
        Info.Curve(k).Group(iGroup).Signals(5).Tag         = verifKeyWord('TODO');
        
        Info.Curve(k).Group(iGroup).Signals(6).Name        = 'std';
        Info.Curve(k).Group(iGroup).Signals(6).Dimensions  = '1, nbSamples';
        Info.Curve(k).Group(iGroup).Signals(6).Storage     = 'single';
        Info.Curve(k).Group(iGroup).Signals(6).Unit        = '';
        Info.Curve(k).Group(iGroup).Signals(6).FileName    = fullfile(nomDir, ['std_' num2str(k) '_' num2str(iGroup) '.bin']);
        Info.Curve(k).Group(iGroup).Signals(6).Tag         = verifKeyWord('TODO');
    end
end


%% Cr�ation du fichier XML d�crivant la donn�e

xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire

if ~exist(nomDirBin, 'dir')
    status = mkdir(nomDirBin);
    if ~status
        messageErreur(nomDirBin)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

nomDirRacine = fileparts(nomDirBin);
for k=1:Info.Dimensions.nbCurves
    for iGroup=1:Info.Curve(k).Dimensions.nbGroups
        X = [];
        NomFicAscii= '';
        for iSignal=1:length(Info.Curve(k).Group(iGroup).Signals)
            flag = writeSignal(nomDirRacine, Info.Curve(k).Group(iGroup).Signals(iSignal),...
                Courbes(k).bilan{1}(iGroup).(Info.Curve(k).Group(iGroup).Signals(iSignal).Name));
            if ~flag
                return
            end
            X = [X Courbes(k).bilan{1}(iGroup).(Info.Curve(k).Group(iGroup).Signals(iSignal).Name)(:)]; %#ok<AGROW>
            NomFicAscii = [NomFicAscii Info.Curve(k).Group(iGroup).Signals(iSignal).Name '_']; %#ok<AGROW>
        end
        NomFicAscii = fullfile(nomDirBin, [NomFicAscii num2str(k) '_' num2str(iGroup) '.txt']);
        % Exportation ASCII
        dlmwrite(NomFicAscii, X)
    end
end

flag = 1;
