% Exportation d'un fichier ASCII xyz (sondes donnees en xyz)
%
% Syntax
%   flag = export_xyz(a, nomFic, ...)
%
% Input Arguments
%   a      : Instance de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%   [X,Y,Z] = peaks(30);
%   a = cl_image('Image', Z)
%   imagesc(a)
%
%   nomFicOut = my_tempname('.xyz')
%   flag = export_xyz(a, nomFicOut)
%
% See also is_GMT Authors
% Authors : JMA
%--------------------------------------------------------------------------
% TODO : demander si on veut un format matrice ou colonnes [N, M] ou [N*M, 1]
% Plus �criture de X ey Y biens�r

function flag = export_xyz(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

fid = fopen(nomFic, 'w+');
if fid == -1
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    return
end

% En-t�te
fprintf(fid, '%s\t%s\t%s\n', ...
    sprintf('%s(%s)', this.YLabel, this.YUnit), ...
    sprintf('%s(%s)', this.XLabel, this.XUnit), ...
    sprintf('Val(%s)', this.Unit));

%% Solution optimis�e

% try
%     %on enl�ve toutes les non-valeurs, pour gagner en place, pas de boucle
%     if isnan(this.ValNaN)
%         subNaN = isnan(this.Image(suby,subx));
%     else
%         subNaN = (this.Image(suby,subx) == this.ValNaN);
%     end
%
%     subNaN = reshape(subNaN, 1, numel(subNaN));
%
%     [xx,yy] = meshgrid(this.x(subx), this.y(suby)); % Peut-�tre une lourdeur en m�moire ici ?!
%
%     im = this.Image(suby,subx);
%
%     fprintf(fid, '%.7f\t%.7f\t%.3f\n', [xx(~subNaN); yy(~subNaN); im(~subNaN)]);
%
% catch ME

%% Solution par d�faut (lente)

NbLig = length(suby);
TypeValNaN = isnan(this.ValNaN);

valX = this.x(subx);
valY = this.y(suby);
str1 = 'Export de la grille en cours';
str2 = 'Export XYZ';
hw = create_waitbar(Lang(str1,str2), 'N', NbLig);
for iLig=1:NbLig
    my_waitbar(iLig, NbLig, hw);
    iy = suby(iLig);
    valYLig = valY(iLig);
    valZ = this.Image(iy,subx);
    if TypeValNaN
        subNonNaN = find(~isnan(valZ));
    else
        subNonNaN = find(valZ ~= this.ValNaN);
    end
    for iCol=1:length(subNonNaN)
        ix = subNonNaN(iCol);
        z = valZ(ix);
        fprintf(fid, '%.10f\t%.10f\t%f\n', valYLig, valX(ix), z);
    end
end
my_close(hw, 'MsgEnd');
fclose(fid);
