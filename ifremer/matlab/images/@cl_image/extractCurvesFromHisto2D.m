function [flag, this] = extractCurvesFromHisto2D(this)

%% Test signature

flag = testSignature(this, 'DataType', cl_image.indDataType('Histo2D'));
if ~flag
    return
end

flag = isequal(this.XUnit, 'deg');
if ~flag
    str1 = 'TODO';
    str2 = sprintf('The XUnit of the image is "%s", the expected one is "deg".', this.XUnit);
    my_warndlg(Lang(str1,str2), 1);
    return
end
flag = isequal(this.YUnit, 'dB');
if ~flag
    str1 = 'TODO';
    str2 = sprintf('The YUnit of the image is "%s", the expected one is "dB".', this.YUnit);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Extraction

angles = this.x;
reflec = this.y;
Histo2D = this.Image(:,:);

nbAngles = length(angles);
Med     = NaN(1,nbAngles);
Moy_dB  = NaN(1,nbAngles);
Moy_Amp = NaN(1,nbAngles);
Moy_Enr = NaN(1,nbAngles);
for k=1:nbAngles
    Histo1D = Histo2D(:,k);
    
    %% Calcul de la m�diane
    
    CumSumHisto1D = cumsum(Histo1D);
    nbPixTotal = CumSumHisto1D(end);
    CumSumHisto1D = CumSumHisto1D / nbPixTotal;
    
    [M, ind] = min(abs(CumSumHisto1D - 0.5));
    if isnan(M)
        continue
    end
%     Med(k) = reflec(ind);
    
    sub = ind-1:ind+1;
    x1 = CumSumHisto1D(sub);
    y1 = reflec(sub);
    [~, ia] = unique(x1);
    Med(k) = interp1(x1(ia), y1(ia), 0.5);
    
    %% Calcul de la moyenne en dB
    
    Moy_dB(k)  = sum(reflec(:) .* (Histo1D(:) / nbPixTotal));
    Moy_Amp(k) = reflec_Amp2dB(sum(reflec_dB2Amp(reflec(:)) .* (Histo1D(:) / nbPixTotal)));
    Moy_Enr(k) = reflec_Enr2dB(sum(reflec_dB2Enr(reflec(:)) .* (Histo1D(:) / nbPixTotal)));
    
    %% Trac� graphique
    
    %{
    figure(1624);
    subplot(2,1,1); plot(reflec, Histo1D); grid on;
    subplot(2,1,2); plot(reflec, CumSumHisto1D); grid on;
    if ~isnan(M)
        hold on;
        plot(Med(k), M+0.5, '*r');
        plot(Moy_dB(k), M+0.5, '*b');
        hold off;
    end
    drawnow
    %}
end

%% Plot des courbes seules

FigUtils.createSScFigure;
PlotUtils.createSScPlot(angles, Med, 'r');
hold on;
PlotUtils.createSScPlot(angles, Moy_dB, 'b');
PlotUtils.createSScPlot(angles, Moy_Amp, 'g');
PlotUtils.createSScPlot(angles, Moy_Enr, 'm');
grid on;
legend({'Median'; 'Moy dB'; 'Moy Amp'; 'Moy Enr'})

%% Plot des courbes par dessus l'image

imagesc(this, 'ColormapIndex', 2)
hold on;
PlotUtils.createSScPlot(angles, Med, 'r');
hold on;
PlotUtils.createSScPlot(angles, Moy_dB, 'b');
PlotUtils.createSScPlot(angles, Moy_Amp, 'g');
PlotUtils.createSScPlot(angles, Moy_Enr, 'm');
grid on;
legend({'Median'; 'Moy dB'; 'Moy Amp'; 'Moy Enr'})

[~, that] = compensation(this, 'Type', 7, 'TypeOperation', '/', 'CleanData', 0);
imagesc(that(2), 'ColormapIndex', 3)
hold on;
PlotUtils.createSScPlot(angles, Med, 'r');
hold on;
PlotUtils.createSScPlot(angles, Moy_dB, 'b');
PlotUtils.createSScPlot(angles, Moy_Amp, 'g');
PlotUtils.createSScPlot(angles, Moy_Enr, 'm');
grid on;
legend({'Median'; 'Moy dB'; 'Moy Amp'; 'Moy Enr'})

% TODO : std, curves
