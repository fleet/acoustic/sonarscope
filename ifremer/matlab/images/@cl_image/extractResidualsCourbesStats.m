function this = extractResidualsCourbesStats(this, varargin)

[varargin, subRoi] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    subRoi = subRoi(end);
end

if isempty(subRoi)
    message_NoCurve(this)
    return
end

for iRoi=1:length(subRoi)
    bilan = this.CourbesStatistiques(subRoi(iRoi)).bilan{1};
    
    if length(bilan) > 1
        for k=1:length(bilan)
            strChoix{k} = sprintf('%s %d ', bilan(k).nomVarCondition, k);%#ok
        end
        str1 = 'Type de visualisation';
        str2 = 'Type of visualization';
        [liste, validation] = my_listdlg(Lang(str1,str2), strChoix);
        if ~validation || isempty(liste)
            return
        end
    else
        liste = 1;
    end
    
    for k=1:length(bilan) %length(liste)
        if any(liste == k)
            nx = bilan(k).nx;
            if isempty(nx)
                continue
            end
            residu_bilan(k)   = bilan(k); %#ok<AGROW>
            residu_bilan(k).y = bilan(k).residuals; %#ok<AGROW>
            residu_bilan(k).nomZoneEtude = sprintf('%s - Residuals', bilan(k).nomZoneEtude); %#ok
            residu_bilan(k).model = []; %#ok<AGROW>
        else
            residu_bilan(k) = bilan(k); %#ok<AGROW>
            residu_bilan(k).y(:) = 0; %#ok<AGROW>
            residu_bilan(k).nomZoneEtude = sprintf('%s - Residuals (No Model?)', bilan(k).nomZoneEtude); %#ok
        end
        residu_bilan(k).Tag = [residu_bilan(k).Tag '-Residuals']; %#ok<AGROW>
    end
    
    this.CourbesStatistiques(end+1) = this.CourbesStatistiques(subRoi(iRoi));
    this.CourbesStatistiques(end).bilan{1} = residu_bilan;
end
