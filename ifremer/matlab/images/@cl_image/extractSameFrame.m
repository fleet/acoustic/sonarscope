function [flag, A, B] = extractSameFrame(this, indLayers, ident, subx, suby, isSynchronized)

% TODO : g�rer les subx et suby
% TODO : tester des images n'ayant pas de zone commune

GT = this(ident).GeometryType;
XStepMetric2 = this(ident).XStepMetric;
YStepMetric2 = this(ident).YStepMetric;
XLim2        = compute_XYLim(this(ident).x);
YLim2        = compute_XYLim(this(ident).y);
for k=1:length(indLayers)
    GT(k+1)         = this(indLayers(k)).GeometryType;
    XStepMetric1(k) = this(indLayers(k)).XStepMetric;      %#ok<AGROW>
    YStepMetric1(k) = this(indLayers(k)).YStepMetric;      %#ok<AGROW>
    XStep(k)        = this(indLayers(k)).XStep;            %#ok<AGROW>
    YStep(k)        = this(indLayers(k)).YStep;            %#ok<AGROW>
    XLim1(k,:)      = compute_XYLim(this(indLayers(k)).x); %#ok<AGROW>
    YLim1(k,:)      = compute_XYLim(this(indLayers(k)).y); %#ok<AGROW>
end

if ~isempty(subx)
    XLim1(1,:) = compute_XYLim(this(indLayers(1)).x(subx));
end
if ~isempty(suby)
    YLim1(1,:) = compute_XYLim(this(indLayers(1)).y(suby));
end

if all(GT == cl_image.indGeometryType('LatLong')) || all(GT == cl_image.indGeometryType('GeoYX')) || isSynchronized
    if any(isnan(XStepMetric1)) || any(isnan(YStepMetric1)) % Rajout� par JMA le 12/04/2019
        XStepMetric1 = XStep;
        YStepMetric1 = YStep;
        XStepMetric2 = this(ident).XStep;
        YStepMetric2 = this(ident).YStep;
    end
    % C'est ceinture et bretelles car beaucoup de pb quand les images n'ont pas la m�me emprise et ne sont pas � la m�me r�solution
    
    pasx = min([XStepMetric1; XStepMetric2]);
    pasy = min([YStepMetric1; YStepMetric2]);
    for k=1:length(indLayers)
        XLim(1) = max(XLim1(k,1), XLim2(1));
        XLim(2) = min(XLim1(k,2), XLim2(2));
        YLim(1) = max(YLim1(k,1), YLim2(1));
        YLim(2) = min(YLim1(k,2), YLim2(2));
        ratiox = XStepMetric1(k) / pasx;
        ratioy = YStepMetric1(k) / pasy;
        [A(k), flag] = extraction(this(indLayers(k)), 'XLim', XLim, 'YLim', YLim); %#ok<AGROW>
        if (ratiox ~= 1) || (ratioy ~= 1)
            A(k) = resize(A(k), ratiox, ratioy); %#ok<AGROW>
        end
        [B(k), flag] = extraction(this(ident), 'XLim', XLim, 'YLim', YLim); %#ok<AGROW>
        ratiox = XStepMetric2(k) / pasx;
        ratioy = YStepMetric2(k) / pasy;
        if (ratiox ~= 1) || (ratioy ~= 1)
            B(k) = resize(B(k), ratiox, ratioy); %#ok<AGROW>
        end
    end
    
    signyA = sign(A(k).y(end) - A(k).y(1));
    signyB = sign(B(k).y(end) - B(k).y(1));
    if signyA ~= signyB
        if signyA == -1
            A(k).Image = flipud(A(k).Image);
            A(k).y     = flipud(A(k).y(:));
        else
            B(k).Image = flipud(B(k).Image);
            B(k).y     = flipud(B(k).y(:));
        end
    end
else % Autres images
    A = this(indLayers);
    B = this(ident);
end
flag = 1;
