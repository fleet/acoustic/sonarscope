% Extract the single image name (without the DataType and GeometryType strings, date and version) 
%
% Syntax
%   ImageName = extract_ImageName(a, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   ImageName : Name of the image
%
% Output Arguments
%   ImageName : Name of the image
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15);
%     imagesc(a)
%     a.Name
%     a = update_Name(a);
%     a.Name
%   ImageName = extract_ImageName(a)
%     a.Name =  'Lena';
%     imagesc(a)
%     a = update_Name(a, 'Name', 'Lena�c');
%     a.Name
%     imagesc(a)
%
% See also cl_image/update_Name Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ImageName = extract_ImageName(this, varargin)

[varargin, ImageName] = getPropertyValue(varargin, 'Name', this.Name); %#ok<ASGLU>

ImageName = cl_image.uncode_ImageName(ImageName);
% On r�p�te cette instruction volontairement car des images comprenaient 2 signatures. Je sais, �a ne devrait pas.
ImageName = cl_image.uncode_ImageName(ImageName);
%{
% TODO : tester dans la R2011a si c'est plus rapide : pb restant � r�soudre : (x)_NomImage
% TODO : ce pb est crucial pour les traitements de multi_images (ex : masquage d'une s�rie d'images avec masques correspondants
% TODO : Autre pb � r�soudre (2)0078_20100519_174837_MaskIn_LatLong_Reflectivity (23:13:53) rend 0078_20100519_174837In
% TODO : c'est pas gagn�
%}
