% Extract the ImageName without the time
%
% Syntax
%   ImageName = cl_image.extract_ImageNameTime(ImageName)
%
% Input Arguments
%   a         : One cl_image instance
%   ImageName : Name of the image
%
% Output Arguments
%   ImageName : Name of the image without the time of creation
%
% Examples
%     ImageName = '(2)EM12D_PRISMED_500m_sunShadingGreatestSlope_LatLong_SunColor (16:23:10)';
%     [ImageName, version] = cl_image.extract_ImageNameVersion(ImageName)
%   ImageName = cl_image.extract_ImageNameTime(ImageName)
%
% See also cl_image.extract_ImageNameVersion cl_image.extract_ImageName cl_image/update_Name Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ImageName = extract_ImageNameTime(ImageName)

subDeb = strfind(ImageName, '(');
subFin = strfind(ImageName, ')');
for k=min(length(subDeb),length(subFin)):-1:1
    i2 = subDeb(k) + 6;
    if (i2 <= length(ImageName))
        i1 = subDeb(k) + 3;
        if strcmp(ImageName(i1), ':') &&  strcmp(ImageName(i2), ':')
            ImageName(subDeb(k):subFin(k)) = [];
        end
    end
end
if strcmp(ImageName(end), ' ')
    ImageName(end) = [];
end
