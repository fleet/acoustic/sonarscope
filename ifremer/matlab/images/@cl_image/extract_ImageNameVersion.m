% Extract the ImageName and the version number of an ImageName
%
% Syntax
%   [ImageName, version] = cl_image.extract_ImageNameVersion(ImageName)
%
% Input Arguments
%   a         : One cl_image instance
%   ImageName : Name of the image
%
% Output Arguments
%   ImageName : Name of the image without the version number
%   version   : Version number
%
% Examples
%     ImageName =
%     '(2)EM12D_PRISMED_500m_sunShadingGreatestSlope_LatLong_SunColor (16:23:10)';
%   [ImageName, version] = cl_image.extract_ImageNameVersion(ImageName)
%     ImageName = cl_image.extract_ImageNameTime(ImageName)
%
% See also cl_image/extract_ImageNameTime cl_image.extract_ImageNameTime cl_image/update_Name Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [ImageName, version] = extract_ImageNameVersion(ImageName)

if isempty(ImageName)
    ImageName = '';
    version = '';
    return
end

version = [];
if strcmp(ImageName(1), '(')
    subFin = strfind(ImageName, ')');
    if ~isempty(subFin)
        version = ImageName(2:subFin(1)-1);
        ImageName(1:subFin(1)) = [];
    end
end
