% Extraction of a part of an image
%
% Syntax
%   b = extraction(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%   x    : Sub sampling x
%   y    : Sub sampling y
%   XLim : Limites of x
%   YLim : Limites of y
%
% Name-only Arguments
%   ForceExtraction : Force the extraction in the given limits
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image('Image', Lena, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = extraction(a, 'suby', 120:180, 'subx', 130:190);
%   imagesc(b);
%
% See also cl_image/downsize cl_image/sampling Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag, Subx, Suby] = extraction(this, varargin)

flag = 0;
that = cl_image.empty;
Subx = {};
Suby = {};

N = length(this);
hw = create_waitbar(waitbarMsg('extraction'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K, subx, suby] = extraction_unit(this(k), varargin{:});
    if ~flag
        return
    end
    %{
if nbImages > 1
K = optimiseMemory(K, 'SizeMax', 0);
end
    %}
    that(k) = K;
    Subx{k} = subx; %#ok<AGROW> 
    Suby{k} = suby; %#ok<AGROW> 
end
my_close(hw)


function [flag, this, subx, suby] = extraction_unit(this, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []);
[varargin, x]    = getPropertyValue(varargin, 'x',    []);
[varargin, y]    = getPropertyValue(varargin, 'y',    []);
[varargin, XLim] = getPropertyValue(varargin, 'XLim', []);
[varargin, YLim] = getPropertyValue(varargin, 'YLim', []);
[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);

[varargin, ForceExtraction] = getFlag(varargin, 'ForceExtraction');
[varargin, NoStats]         = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

flag = 1;

if isempty(XLim) || isempty(YLim)
    if isempty(x)
        if isempty(subx)
            subx = 1:length(this.x);
        end
    else
        [subx, ~] = xy2ij(this, x, this.y, 'Method', 'linear');
    end
    
    if isempty(y)
        if isempty(suby)
            suby = 1:length(this.y);
        end
    else
        [~, suby] = xy2ij(this, this.x, y, 'Method', 'linear');
        suby = sort(suby); % Rajout� le 08/04/2014
    end
else
    subx = find((this.x >= min(XLim)) & (this.x <= max(XLim)));
    suby = find((this.y >= min(YLim)) & (this.y <= max(YLim)));
    if this.y(end) < this.y(1)
        suby = fliplr(suby);
    end
end

if (length(subx) <= 1) || (length(suby) <= 1) % Modifi� par JMA le 07/04/2014
    flag = 0;
    return
end

if isequal(round(subx), 1:this.nbColumns) && isequal(round(suby), 1:this.nbRows) && ~ForceExtraction
    % Ajout� le 21/03/2011 car pb lors de la sauvegarde du projet
    this = replace_Image(this, this.Image(:,:,:));
    
    GeometryType = this.GeometryType;
    if ((GeometryType == cl_image.indGeometryType('LatLong')) || (GeometryType == cl_image.indGeometryType('GeoYX'))) ...
            && (~isequal(this.x, centrage_magnetique(this.x)) || ~isequal(this.y, centrage_magnetique(this.y)))
        if Mute
            this = majCoordonnees(this, subx, suby);
        else
            str1 = sprintf('Les coordonn�es de l''image "%s" ne sont pas centr�es sur une r�gle magn�tique. Ceci est peut-�tre normal mais cela peut entra�ner des dysfonctionnements car toutes les images qui seront d�duites de celle-ci auront leur coordonn�es  qui seront recentr�es automatiquement. Un glissement d''un pixel entre images serait explicable par ce fait. Voulez-vous les recentrer d�s � pr�sent ?', this.Name);
            str2 = sprintf('The coordinates of "%s" are not centered. It can produce some bugs when working with deduced images that will be automatically centered (a shift of 1 pixel between images could be possible). Do you want to center the coordinates ?', this.Name);
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            if rep == 1
                this = majCoordonnees(this, subx, suby);
            end
        end
    end
    return
end

coefx = abs(subx(end) - subx(1)) / (length(subx) - 3);
coefy = abs(suby(end) - suby(1)) / (length(suby) - 3);
if (coefx >= 1) && (coefy >= 1)
    % Sous-�chantillonnage pour �viter une interpolation
    [subx, suby] = checkSubxSuby(subx, suby, this.nbColumns, this.nbRows, ForceExtraction);
    J = this.Image(suby, subx, :);
    
elseif (coefx >= .95) && (coefy >= .95)
    % Encore sous-�chantillonnage pour �viter une interpolation
    [subx, suby] = checkSubxSuby(subx, suby, this.nbColumns, this.nbRows, ForceExtraction);
    J = this.Image(suby, subx, :);
    
elseif (coefx >= 1) && (coefy <= 1)
    % Je ne sais pas pourquoi il faut faire 1/coefx mais c'est comme �a que
    % �a marche !!!!!!!!!!
    %     coefx = abs(subx(end) - subx(1)) / (length(subx) - 1);
    %     coefy = abs(suby(end) - suby(1)) / (length(suby) - 1);
    [subx, suby] = checkSubxSuby(subx, suby, this.nbColumns, this.nbRows, ForceExtraction);
    J = downsize(this.Image(unique(floor(suby)), unique(floor(subx)), :),  'Ny', length(suby), 'Nx', length(subx), 'Method', 'linear'); % modif JMA le 02/06/2016 pour faire comme instruction ligne 182

elseif (coefx <= 1) && (coefy >= 1)
    % Je ne sais pas pourquoi il faut faire 1/coefy mais c'est comme �a que
    % �a marche !!!!!!!!!!
    %     coefx = abs(subx(end) - subx(1)) / (length(subx) - 1);
    %     coefy = abs(suby(end) - suby(1)) / (length(suby) - 1);
    [subx, suby] = checkSubxSuby(subx, suby, this.nbColumns, this.nbRows, ForceExtraction);
    J = downsize(this.Image(unique(floor(suby)), unique(floor(subx)), :),  'Ny', length(suby), 'Nx', length(subx), 'Method', 'linear'); % modif JMA le 02/06/2016

else
    % On agrandit l'image, on passe donc par une interpolation
    [subx, suby] = checkSubxSuby(subx, suby, this.nbColumns, this.nbRows, ForceExtraction);
    J = downsize(this.Image(unique(floor(suby)), unique(floor(subx)), :), 'Ny', length(suby), 'Nx', length(subx), 'Method', 'linear');
end

this = replace_Image(this, J);

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

if ~NoStats
    this = compute_stats(this);
end

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Extraction');


function [subx, suby] = checkSubxSuby(subx, suby, nbColumns, nbRows, ForceExtraction)

subx = round(subx);
suby = round(suby);

if ForceExtraction && ~my_isdeployed
    subxOut = find((subx < 1) | subx > nbColumns, 1);
    subyOut = find((suby < 1) | suby > nbRows, 1);
    if ~isempty(subxOut) || ~isempty(subyOut)
%         my_breakpoint('FctName', 'cl_image/extraction/checkSubxSuby');
    end
end

subx(subx < 1) = 1;
suby(suby < 1) = 1;
subx(subx > nbColumns) = nbColumns;
suby(suby > nbRows) = nbRows;

if ~ForceExtraction
    subx = unique(subx);
    suby = unique(suby);
end
