% Extraction of a part of an image
%
% Syntax
%   b = extraction_subxsuby(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image('Image', Lena, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = extraction_subxsuby(a, 'suby', 120:180, 'subx', 130:190);
%   imagesc(b);
%
% See also cl_image/downsize cl_image/sampling Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = extraction_subxsuby(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('extraction_subxsuby'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K] = extraction_unit(this(k), varargin{:});
    if ~flag
        return
    end
    %{
if nbImages > 1
K = optimiseMemory(K, 'SizeMax', 0);
end
    %}
    that(k) = K;
end
my_close(hw)


function [flag, this] = extraction_unit(this, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []);
% [varargin, Mute]    = getPropertyValue(varargin, 'Mute', 0);

[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

flag = 1;

if isempty(subx)
    subx = 1:length(this.x);
end

if isempty(suby)
    suby = 1:length(this.y);
end

if (length(subx) <= 1) || (length(suby) <= 1) % Modifi� par JMA le 07/04/2014
    flag = 0;
    return
end

this = replace_Image(this, this.Image(suby,subx,:));

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

if ~NoStats
    this = compute_stats(this);
end

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Extraction');
