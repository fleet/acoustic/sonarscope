%  Evaluation d'une instruction operant sur les images (Ex : combinaison lineaire)
%
% Syntax
%   [c, flag] = feval([a b c ...], cmd)
%
% Input Arguments
%   a, b, c : Instances de cl_image ou un scalaire
%   cmd     : Instruction matlab
%
% Output Arguments
%   c    : Une instance de cl_image
%   flag : Booleen qui indique si l'operation est reussie
%
% Examples
%   a = cl_image('Image', ImageSonar(1));
%   b = cl_image('Image', ImageSonar(4));
%   imagesc([a b])
%   c = feval([a, b], [], '0.1 * I1 + 0.9 * I2')
%   imagesc(c)
%   c = feval([a, b], [], '0.9 * I1 + 0.1 * I2')
%   imagesc(c)
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = feval(this, ListeSignauxVert, cmd, varargin)

[subx, suby, varargin] = getSubxSuby(this(1), varargin); %#ok<ASGLU>

for k=2:length(this)
    [~, ~, subx, suby] = intersectionImages(this(1), subx, suby, this(k));
end

%% R�cuperation des images

I{1} = this(1);
[x, y] = ij2xy(this(1), subx, suby);
for k=2:length(this)
    I{k} = extraction(this(k), 'x', x, 'y', y); %#ok
end

%% R�cuperation des signaux

V = cell(0);
for k=1:length(ListeSignauxVert)
    X = this.Sonar.(ListeSignauxVert{k})(suby);
    V{k} = X(:);
end

%% Execution de l'instruction

ind  = regexp(cmd, {'I', 'V'});
inFcn = [];
for k=1:numel(ind)
    for iLoop=1:numel(ind{k})
        if isempty(inFcn)
            inFcn = [cmd(ind{k}(iLoop)) num2str(iLoop)];
        else
            inFcn = [inFcn ',' cmd(ind{k}(iLoop)) num2str(iLoop)]; %#ok<AGROW>
        end
    end
end
% Construction du Handle de fonction par la conversion de chaine
% de caract�re en fonction handle.
fcnHdl = str2func(['@(' inFcn ')' cmd]);

fprintf('Attempting to execute formula : "%s"\n', cmd);

try
    X = [I(:) V(:)];
    % this = feval(g, X{:});
    if isempty(V)
        pppp = fcnHdl(X{:});
        if ismatrix(pppp)
            % Pour traiter les appels � isnan(I1) par exemple.
            this = X{:};
            set(this, 'Image', pppp);
        else
            this = pppp;
        end    
    else
        this= fcnHdl(X{:});
    end
    
catch %#ok<CTCH>
    str1 = 'La commande a �chou�. V�rifiez que tous les op�rateurs sont d�finis pour la classe cl_image';
    str2 = 'The command failed. Ask to JMA to check if the operators you tried to use are defined for the cl_image class.';
    my_warndlg(Lang(str1,str2),1);
    flag = 0;
    return
end

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'Feval');
flag = 1;
