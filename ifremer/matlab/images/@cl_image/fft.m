% Fourier transform of an image
%
% Syntax
%   b = fft(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   I = imread(nomFic);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = fft(a);
%   imagesc(b);
%
%   c = fftFilterDisk(b, [0.05 0.1 0], 1);
%   imagesc(c);
%
%   d = ifft(c);
%   imagesc(d);
%
% See also cl_image/ifft cl_image/fftFilterDisk Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = fft(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('fft'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = fft_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function this = fft_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx);
I = singleUnlessDouble(I, this.ValNaN);
I = double(I);
[I, subNaN] = windowEnlarge(I, [0 0], NaN);
I(subNaN) = this.StatValues.Moyenne;
I = fftshift(fft2(double(I)));
Image(:,:) = I;

%% Output image

% that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'fft');
% % TODO : prendre son temps pour faire cela

this = replace_Image(this, Image);
this.ValNaN = NaN;
this.SpectralStatus = 2;

%% Calcul des statistiques

this = compute_stats(this);

%% Mise � jour de coordonn�es

x = this.x(subx);
y = this.y(suby);

%% Sauvegarde des parametres de l'image pour restitution lors de la transform�e de Fourier inverse

this.SaveParamsIfFFT.x              = x;
this.SaveParamsIfFFT.y              = y;
this.SaveParamsIfFFT.XUnit          = this.XUnit;
this.SaveParamsIfFFT.YUnit          = this.YUnit;
this.SaveParamsIfFFT.ColormapIndex  = this.ColormapIndex;
this.SaveParamsIfFFT.Colormap       = this.Colormap;
this.SaveParamsIfFFT.TagSynchroX    = this.TagSynchroX;
this.SaveParamsIfFFT.TagSynchroY    = this.TagSynchroY;
this.SaveParamsIfFFT.TagSynchroContrast = this.TagSynchroContrast;

%% Calcul des nouveaux axes

N = length(x);
dx = mean(diff(x));
x = ((floor(-N/2):floor(N/2)) / N) / dx;
x(end) = [];
this.x = x;
this.XUnit = ['1 / ' this.XUnit];

N = length(y);
dy = abs(mean(diff(y)));
y = ((floor(-N/2):floor(N/2)) / N) / dy;
y(end) = [];
this.y = y;
this.YUnit = ['1 / ' this.YUnit];

%% Mise � jour des bornes en x et y

this.XLim = compute_XYLim(x);
this.YLim = compute_XYLim(y);

%% Mise � jour de TagSynchroX et TagSynchroY

this.TagSynchroX = [this.TagSynchroX ' - FFT'];
this.TagSynchroY = [this.TagSynchroY ' - FFT'];

%% Rehaussement de contraste

this.TagSynchroContrast = [this.TagSynchroContrast ' - FFT'];
this.CLim               = [this.StatValues.Min this.StatValues.Max];
this.ColormapIndex      = 3;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'fft');
