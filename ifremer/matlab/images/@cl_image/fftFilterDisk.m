% Disk filter in the frequency domain 
%
% Syntax
%   b = fftFilterDisk(a, [Rayon, centrex, centrey], InOut)
%
% Input Arguments
%   a     : Instance(s) of cl_image
%   val   : [radius, centerx, centery]
%   InOut : 1=On garde l'interieur, 2=on garde l'exterieur
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   I = imread(nomFic);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = fft(a);
%   imagesc(b);
%
%   c = fftFilterDisk(b, [0.05 0.1 0], 1);
%   imagesc(c);
%
%   d = ifft(c);
%   imagesc(d);
%
% See also cl_image cl_image/fft cl_image/ifft cl_image/fftFilterGauss Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = fftFilterDisk(this, val, InOut)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('fftFilterDisk'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = fftFilterDisk_unit(this(k), val, InOut);
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = fftFilterDisk_unit(this, val, InOut)

%% Algorithm

dx = mean(diff(this.x));
dy = mean(diff(this.y));
rayon = ceil(val(1)/max(dx,dy));

try
    h = fspecial('disk', rayon);
    h = h / max(h(:));
catch %#ok<CTCH>
    str = sprintf('Pas de license image disponible.');
    my_warndlg(str, 1);
    return
end

Masque = zeros([this.nbRows this.nbColumns], 'single');

[ix, iy] = xy2ij(this, val(2), val(3));
subiy = iy-rayon:iy+rayon;
subix = ix-rayon:ix+rayon;
subsuby = find((subiy >= 1) & (subiy <= this.nbRows));
subsubx = find((subix >= 1) & (subix <= this.nbColumns));

% subOut = find((subsuby < 1) |  (subsuby > size(h, 1)));
subOut = ((subsuby < 1) |  (subsuby > size(h, 1)));
subsuby(subOut) = [];

% subOut = find((subsubx < 1) |  (subsubx > size(h, 2)));
subOut = ((subsubx < 1) |  (subsubx > size(h, 2)));
subsubx(subOut) = [];

% subOut = find((subix < 1) |  (subix > size(Masque, 2)));
subOut = ((subix < 1) |  (subix > size(Masque, 2)));
subix(subOut) = [];

%     subOut = find((subiy(subsuby) < 1) |  (subiy(subsuby) > size(Masque, 1)));
%     subsuby(subOut) = [];

Masque(subiy(subsuby), subix) = h(subsuby, subsubx);

[ix, iy] = xy2ij(this, -val(2), -val(3));
subiy = iy-rayon:iy+rayon;
subix = ix-rayon:ix+rayon;
subsuby = find((subiy >= 1) & (subiy <= this.nbRows));
subsubx = find((subix >= 1) & (subix <= this.nbColumns));


% subOut = find((subsuby < 1) |  (subsuby > size(h, 1)));
subOut = ((subsuby < 1) |  (subsuby > size(h, 1)));
subsuby(subOut) = [];

% subOut = find((subsubx < 1) |  (subsubx > size(h, 2)));
subOut = ((subsubx < 1) |  (subsubx > size(h, 2)));
subsubx(subOut) = [];

% subOut = find((subix < 1) |  (subix > size(Masque, 2)));
subOut = ((subix < 1) |  (subix > size(Masque, 2)));
subix(subOut) = [];

%     subOut = find((subiy(subsuby) < 1) |  (subiy(subsuby) > size(Masque, 1)));
%     subsuby(subOut) = [];


Masque(subiy(subsuby), subix) = h(subsuby, subsubx);

if InOut == 2
    Masque =  1 - Masque;
end
Masque(Masque ~= 1) = 0;
Masque = Masque | rot90(Masque,2);
Masque = double(Masque);
Masque(~Masque) = NaN;

I = this.Image(:,:);
I = double(I);
Image(:,:) = Masque .* I;

%% Output image

Parameters = getHistoryParameters(val);
that = inherit(this, Image, 'AppendName', 'fftFilterDisk', 'ValNaN', NaN, 'Parameters', Parameters);
