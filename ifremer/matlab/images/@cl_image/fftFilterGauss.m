% Gauss filter in the frequency domain 
%
% Syntax
%   b = fftFilterGauss(a, [Rayon, centrex, centrey], InOut)
%
% Input Arguments
%   a     : Instance(s) of cl_image
%   val   : [radius, centerx, centery]
%   InOut : 1=keeps inside, 2=keeps outside
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   I = imread(nomFic);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = fft(a);
%   imagesc(b);
%
%   c = fftFilterGauss(b, [0.05 0.1 0], 1);
%   imagesc(c);
%
%   d = ifft(c);
%   imagesc(d);
%
% See also cl_image cl_image/fft cl_image/ifft  cl_image/fftFilterDisk Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = fftFilterGauss(this, val, InOut)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('fftFilterGauss'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = fftFilterGauss_unit(this(k), val, InOut);
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = fftFilterGauss_unit(this, val, InOut)

%% Algorithm

rayon   = val(1);
centrex = val(2);
centrey = val(3);

x = my_normpdf(this.x, centrex, rayon);
y = my_normpdf(this.y, centrey, rayon);

Filtre = (y' * x) + (fliplr(y)' * fliplr(x));
Filtre = Filtre / max(Filtre(:));
% figure; imagesc(this.x, this.y, Filtre); colorbar
% figure; imagesc(this.x, this.y, reflec_Enr2dB(Filtre)); colorbar

if InOut == 2
    Filtre =  1 - Filtre;
end

I = this.Image(:,:);
I = double(I);
Image(:,:) = Filtre .* I;

%% Output image

Parameters = getHistoryParameters(val);
that = inherit(this, Image, 'AppendName', 'fftFilterGauss', 'ValNaN', NaN, 'Parameters', Parameters);

