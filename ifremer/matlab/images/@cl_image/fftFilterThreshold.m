% Threshold filter in the frequency domain 
%
% Syntax
%   b = fftFilterThreshold(a, threshold) 
%
% Input Arguments
%   a         : Instance(s) of cl_image
%   Threshold : Threshold (data < Thershold are set to 0)
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples 
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   I = imread(nomFic);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = fft(a);
%   imagesc(b);
%
%   c = fftFilterThreshold(b, 70);
%   imagesc(c);
%
%   d = ifft(c);
%   imagesc(d);
%
% See also cl_image cl_image/fft cl_image/ifft Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = fftFilterThreshold(this, threshold)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('fftFilterThreshold'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = fftFilterThreshold_unit(this(k), threshold);
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = fftFilterThreshold_unit(this, threshold)

%% Algorithm

I = this.Image(:,:);
% I = singleUnlessDouble(I, this.ValNaN);
I = double(I);
% I = 20 * log10(abs(I));
Mask = ((20 * log10(abs(I))) < threshold);
I(Mask) = 0;
Image(:,:) = I;

%% Output image

Parameters = getHistoryParameters(threshold);
that = inherit(this, Image, 'AppendName', 'fftFilterThreshold', 'ValNaN', NaN, 'Parameters', Parameters);
