% Filter the bathymetry using the (QF (Quality Factor). Data under a
% threshold are set to NaN
%
% Syntax
%   b = filterBathyQF(a, QF, QFThreshold, ...)
%
% Input Arguments
%   a     : Instance(s) of cl_image containing the bathymetry
%   QF    : Instance(s) of cl_image containing the Quality Factor image for every instance of a
%   QFThreshold : Threshold to apply
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = % TODO
%   imagesc(a);
%
%   b = filterBathyQF(a, QF, 2.8);
%   imagesc(b);
%
% See also cl_image/sign Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterBathyQF(this, QF, QFThreshold, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterBathyQF'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterBathyQF_unit(this(k), QF(k), QFThreshold, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = filterBathyQF_unit(this, QF, QFThreshold, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

X = QF.Image(suby,subx);
subNaN = (X(:,:) < QFThreshold);
clear X

I = single(this.Image(suby,subx));
I(subNaN) = NaN;
clear subNaN

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'filterBathyQF', 'ValNaN', NaN);
