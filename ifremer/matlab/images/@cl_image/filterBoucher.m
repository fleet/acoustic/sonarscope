% JM Boucher filter of an image
%
% Syntax
%   b = filterBoucher(a, ...)
%
% Input Arguments
%   a     : Instance(s) of cl_image
%   Alpha : Alpha
%
% Name-Value Pair Arguments
%   window : Sliding window size (Default : [7 7])
%   subx   : Sub-sampling in abscissa
%   suby   : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterBoucher(a, 1);
%   imagesc(b);
%
% See also cl_image/filterLee Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterBoucher(this, Alpha, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterBoucher'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterBoucher_unit(this(k), Alpha, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = filterBoucher_unit(this, Alpha, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, window] = getPropertyValue(varargin, 'window', [7 7]); %#ok<ASGLU>

%% Conversion �ventuelle en image d'Intensit�

if this.ImageType == 2 % RGB
    this = RGB2Intensity(this);
elseif this.ImageType == 3 % Indexed
    this = Indexed2Intensity(this);
end

%% Algorithm

I = this.Image(suby,subx);
I = singleUnlessDouble(I, this.ValNaN);

fun = @(block_struct) Boucher(block_struct.data, window, Alpha);
I = blockproc(I, [1024 1024], fun, 'BorderSize', window); % 'UseParallel',1

% Remarque : il s'est av�r� contreproductif de vouloir utiliser la
% parall�lisation ici ('UseParallel', UseParallel) car la fonction Lee
% appele la fonction filter2 qui est d�j� parall�lis�e. De plus, cela
% emp�cherait d'impl�menter le ImageAdaptateur sur cl_memmapfile (technique
% � d�velopper pour �viter de faire I = this.Image(suby,subx);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'filterBoucher');


function res = Boucher(I, window, Alpha)

N = prod(window);
meanFilter = ones(window, 'single');

meanFilter = meanFilter / N;

Imoy    = filter2(meanFilter, I);
Ialpha  = filter2(meanFilter, I .^ (Alpha));
Ialpha1 = filter2(meanFilter, I .^ (Alpha+1));
I2alpha = filter2(meanFilter, I .^ (2*Alpha));
w = N \(N + 1);

Ialpha2 = Ialpha.^2;
a = (exp(w * Alpha) .* Ialpha1 - Ialpha2) ./ (I2alpha - Ialpha2);
% res = a;
% return

b = Imoy - a .* Ialpha;
res = a .* (I .^ Alpha) + b;

%{
a = (exp(49\50*alpha).*Ialpha1-Ialpha.^2).\(I2alpha-Ialpha.^2);
%}


%{
function y = NoyauFiltreTLineaire(x, L, Alpha)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);
if isnan(I)
y = NaN;
else
sub = find(~isnan(x));
if isempty(sub)
y = NaN;
else
x = x(sub);
xt = x .^ Alpha;
xt1 = x .^ (Alpha+1);

Imoy = mean(x);
Ialpha = mean(xt);
Ialpha1 = mean(xt1);
vt = var(xt);

if vt == 0
vt = Ialpha;
end

a = (Ialpha1 * L / (L+Alpha) - Ialpha * Imoy) / vt;
if a < 0
b = Imoy - a * Ialpha;
y = a * I ^ Alpha + b;
else
y = Imoy;
end
end
end
%}
