% Circular digital filter of an image (or a part of the image if a mask is given)
%
% Syntax
%   b = filterDisk(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   radius    : Radius of the disk (Defaut : 5) % TODO : renommer en radius
%   LayerMask : Instance(s) of cl_image containing a mask
%   valMask   : Values of the mask
%   subx        : Sub-sampling in abscissa
%   suby        : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterDisk(a);
%   imagesc(b);
%
% See also cl_image/filterRectangle Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterDisk(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterDisk'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterDisk_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function this = filterDisk_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask', []);
[varargin, radius]    = getPropertyValue(varargin, 'radius', 5); %#ok<ASGLU>

%% Compute filter parameters

h = fspecial('disk', radius);
window = size(h);

%% Algorithm

Parameters = getHistoryParameters(radius);
AppendName = 'filterDisk';
this = computeFilter2(this, subx, suby, h, window, LayerMask, valMask, AppendName, Parameters);
