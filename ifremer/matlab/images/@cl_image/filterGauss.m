% Gaussian filter of an image (or a part of the image if a mask is given)
%
% Syntax
%   b = filterGauss(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   sigma     : standard deviation (Default : 2)
%   LayerMask : Instance(s) of cl_image containing a mask
%   valMask   : Values of the mask
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterGauss(a);
%   imagesc(b);
%
% See also cl_image/filterRectangle Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterGauss(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterGauss'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterGauss_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)

% TODO : � partir de filterWiener et filterGauss_unit, cr�er un container
% g�n�raliste qui puisse accueillir tous les filtrages. Am�liorer le cas o�
% on demande une extraction : extraire une image plus grande plut�t que
% l'image demand�e et de lui faire un "enlarge" ensuite.

function this = filterGauss_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []);
[varargin, sigma]     = getPropertyValue(varargin, 'sigma',     2); %#ok<ASGLU>

%% Compute filter parameters

n = floor(2 * sigma);
window = [max(3,2*n+1) max(3,2*n+1)];
h = fspecial('gaussian', window, sigma);
%     figure(20081008); imagesc(-n:n, -n:n, h); axis square; colorbar; title('Gaussian coefficient filter');

Parameters = getHistoryParameters(window, sigma);
AppendName = 'filterGauss';

%% Algorithm

this = computeFilter2(this, subx, suby, h, window, LayerMask, valMask, AppendName, Parameters);
