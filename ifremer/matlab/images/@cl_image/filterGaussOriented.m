% Oriented gaussian filter of an image (or a part of the image if a mask is given)
%
% Syntax
%   b = filterGaussOriented(a, sigma1, sigma2, angle, ...)
%
% Input Arguments
%   a      : Instance(s) of cl_image
%   sigma1 : Width of the gaussian window (std)
%   sigma2 : Width of the gaussian window (std)
%   angle  : Orientation
%
% Name-Value Pair Arguments
%   LayerMask : Instance(s) of cl_image containing a mask
%   valMask   : Values of the mask
%   subx        : Sub-sampling in abscissa
%   suby        : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterGaussOriented(a, 2, 0.5, 45);
%   imagesc(b);
%
% See also cl_image/filterGauss Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterGaussOriented(this, sigma1, sigma2, angle, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterGaussOriented'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterGaussOriented_unit(this(k), sigma1, sigma2, angle, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function this = filterGaussOriented_unit(this, sigma1, sigma2, angle, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []); %#ok<ASGLU>

%% Compute filter parameters

n1 = floor(2 * sigma1);
window = [max(3,2*n1+1) max(3,2*n1+1)];
h = fspecial('gaussian', window, sigma1);

x = -n1:n1;
x2 = linspace(x(1), x(end), n1*sigma2/sigma1);
h = interp2(x, x', h, x2, x');
% figure(20081008); imagesc(h); axis equal; colorbar; title('Gaussian coefficient filter');

h = imrotate(h, -angle, 'bicubic');
[nh1,nh2] = size(h);
if mod(nh1, 2) == 0
    h(end+1,:) = 0;
end
if mod(nh2, 2) == 0
    h(:,end+1) = 0;
end
h = h  / sum(h(:));
% figure(20081008); imagesc(h); axis equal; axis xy; colorbar; title('Gaussian coefficient filter');
window = size(h);

%% Algorithm

Parameters = getHistoryParameters(window, sigma1, sigma2, angle);
AppendName = 'filterGaussOriented';
this = computeFilter2(this, subx, suby, h, window, LayerMask, valMask, AppendName, Parameters);
