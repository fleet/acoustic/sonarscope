% Laplacian filter of an image
%
% Syntax
%   b = filterLaplacian(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   alpha : Shape of the Laplacian from 0 to 1 (Default : 0.2)
%   subx  : Sub-sampling in abscissa
%   suby  : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterLaplacian(a);
%   imagesc(b);
%
% See also cl_image/filterGauss Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterLaplacian(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterLaplacian'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterLaplacian_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = filterLaplacian_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, alpha] = getPropertyValue(varargin, 'alpha', 0.2); %#ok<ASGLU>

%% Algorithm

h = fspecial('laplacian', alpha);
for k=this.nbSlides:-1:1
    [I, subNaN] = windowEnlarge(this.Image(suby,subx,k), [3 3], []);
    I = filter2(h, I, 'valid');
    I(subNaN) = NaN;
    Image(:,:,k) = I;
end

%% Output image

Parameters = getHistoryParameters(alpha);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'filterLaplacian', 'ValNaN', NaN, 'ColormapIndex', 2, 'Parameters', Parameters);
