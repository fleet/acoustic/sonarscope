% Median filter of an image (or a part of the image if a mask is given)
%
% Syntax
%   b = filterMedian(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   window : Sliding window size (Default : [3 3])
%   subx   : Sub-sampling in abscissa
%   suby   : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterMedian(a, 'window', [5 5]);
%   imagesc(b);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterMedian(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterMedian'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterMedian_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = filterMedian_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, window]    = getPropertyValue(varargin, 'window',    [3 3]);
[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []); %#ok<ASGLU>

%% Algorithm

NH = floor(window(2) / 2);
NV = floor(window(1) / 2);

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    I = singleUnlessDouble(I, this.ValNaN);
    
    if ~isempty(LayerMask)
        Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
        K = zeros(size(Masque), 'uint8');
        for ik=1:length(valMask)
            sub = (Masque == valMask(ik));
            K(sub) = 1;
        end
        subNonMasque = find(K == 0);
        K = I(subNonMasque);
    end
    
    I = medfilt2(I, window);
    
    % -----------------------------
    % Get data from the neighbors
    
    I(1:NV,:,k)           = this.Image(suby(1:NV),subx, k);
    I(end-NV+1:end,:,k)   = this.Image(suby((end-NV+1):end),subx, k);
    I(:,1:NH,k)           = this.Image(suby,subx(1:NH), k);
    I(:,(end-NH+1):end,k) = this.Image(suby,subx((end-NH+1):end), k);
    
    if ~isempty(LayerMask)
        I(subNonMasque) = K;
    end
    Image(:,:,k) = I;
end

%% Output image

Parameters = getHistoryParameters(window);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'filterMedian', 'ValNaN', NaN, 'Parameters', Parameters);

