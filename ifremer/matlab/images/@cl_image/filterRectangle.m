% Rectangular digital filter of an image (or a part of the image if a mask is given)
%
% Syntax
%   b = filterRectangle(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   window      : Sliding window size (Default : [3 3])
%   LayerMask : Instance(s) of cl_image containing a mask
%   valMask   : Values of the mask
%   subx        : Sub-sampling in abscissa
%   suby        : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterRectangle(a);
%   imagesc(b);
%
%   b = filterRectangle(a, 'window', [7 7]);
%   imagesc(b)
%
% See also cl_image/filterGauss Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterRectangle(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterRectangle'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterRectangle_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function this = filterRectangle_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []);
[varargin, window]    = getPropertyValue(varargin, 'window',    [3 3]); %#ok<ASGLU>

%% Compute filter parameters

h = ones(window) / prod(window);

%% Algorithm

Parameters = getHistoryParameters(window);
AppendName = 'filterRectangle';
this = computeFilter2(this, subx, suby, h, window, LayerMask, valMask, AppendName, Parameters);
