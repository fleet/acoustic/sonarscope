% Speckle filtering of an image
%
% Syntax
%   b = filterSpeckle(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   indFiltre : 'Lee' | 'Kuan' | {'Frost'} | 'MAP' | 'EAP' | 'TLineaire' | 'MoyQuant25_75'
%   window    : Sliding window size(Default : [7 7])
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterSpeckle(a);
%   imagesc(b);
%
%   b = filterSpeckle(a, 'indFiltre', 1);
%   imagesc(b);
%
% See also cl_image/filterGauss Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterSpeckle(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterSpeckle'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterSpeckle_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = filterSpeckle_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, indFiltre] = getPropertyValue(varargin, 'indFiltre', 3);
[varargin, nbPixMoy]  = getPropertyValue(varargin, 'nbPixMoy',  1);
[varargin, window]    = getPropertyValue(varargin, 'window',    [7 7]);
[varargin, LayerN]    = getPropertyValue(varargin, 'LayerN',    []); %#ok<ASGLU>

strNoyauFiltre = {'Lee'; 'Kuan'; 'Frost'; 'MAP'; 'EAP'; 'TLineaire'; 'MoyQuant25_75'};
NoyauFiltre = ['NoyauFiltre' strNoyauFiltre{indFiltre}];

paramSupTlineaire = 3;

%% Conversion éventuelle en image d'Intensité

if this.ImageType == 2 % RGB
    this = RGB2Intensity(this);
elseif this.ImageType == 3 % Indexed
    this = Indexed2Intensity(this);
end

%% Algorithm

if ~isempty(LayerN)
    N1 = LayerN.Image(suby,subx);
else
    N1 = ones([this.nbRows this.nbColumns], 'single') * nbPixMoy;
end

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    I = singleUnlessDouble(I, this.ValNaN);
    I = nlfilterSpeckle(I, N1, window, NoyauFiltre, paramSupTlineaire);
    Image(:,:,k) = I;
end

%% Output image

Parameters = getHistoryParameters(indFiltre, nbPixMoy, window);
that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', 'speckleFilter', 'Parameters', Parameters);
