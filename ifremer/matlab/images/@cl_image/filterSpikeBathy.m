% Spike filter on tiles of size (20 20] with a shift of 15 between tiles.
% This function estimates for every tile of size [20 20] the
% polynomial surface fitting the data with a minimum error quadratic mean,
% computes the standard deviation and reject points that are upper than
% Seuil*std
%
% Syntax
%   b =  filterSpikeBathy(a, Threshold, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%   Threshold : Multiplier of the standard deviations
%
% Name-Value Pair Arguments
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%   RestoreThreshold : Restoration value (the points that were filtered by the
%                      spike filter that are inferior to this threshold are restored).
%                      This is usefuf when the data is very clean.
%   OtherImages : 1=One wants the polyval and the normalized residuals,
%                 0=No additional layers
%   Margin : Margin around the image to set to NaN (Advise : 0)
%   Fig   : Figure number if you want to display the histogram of normalized residuals
%   Fig2   : Figure number if you want to display the histogram of residuals (in the unit of I)
%
% Output Arguments
%   b : Instance(s) of cl_image.
%       If OtherImages=0, 2 images are created for every instance of a.
%       The former is the filtered image, the later is the mask
%       If OtherImages=1, 4 images are created for every instance of a.
%       The former is the filtered image, the later is the mask, the
%       intermediate are the polysurf and the residuals.
%
% Examples
%   I = Prismed;
%   [N, M] = size(I);
%   Np = floor(N*M / 10);
%   xd = randi(M, [Np 1]);
%   yd = randi(N, [Np 1]);
%   Noise = rand([Np 1]) * 1000;
%   for k=1:Np
%       I(yd(k),xd(k)) = I(yd(k),xd(k)) + Noise(k);
%   end
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = filterSpikeBathy(a, 1.5);
%   imagesc(b);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterSpikeBathy(this, Threshold, varargin)

[varargin, OtherImages] = getPropertyValue(varargin, 'OtherImages', 0);

N = length(this);
if OtherImages
    that = repmat(cl_image,4,N);
else
    that = repmat(cl_image,2,N);
end
hw = create_waitbar(waitbarMsg('filterSpikeBathy'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(:,k) = filterSpikeBathy_unit(this(k), Threshold, 'OtherImages', OtherImages, varargin{:});
    if N > 1
        that(:,k) = optimiseMemory(that(:,k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)
that = that(:);


function that = filterSpikeBathy_unit(this, Threshold, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Margin]           = getPropertyValue(varargin, 'Margin',           []);
[varargin, Fig]              = getPropertyValue(varargin, 'Fig',              []);
[varargin, Fig2]             = getPropertyValue(varargin, 'Fig2',             []);
[varargin, RestoreThreshold] = getPropertyValue(varargin, 'RestoreThreshold', 0);
[varargin, OtherImages]      = getPropertyValue(varargin, 'OtherImages',      1); %#ok<ASGLU>

%% Contols

% Should check that the number of slides is 1

%% Algorithm

I = this.Image(suby,subx);
I = singleUnlessDouble(I, this.ValNaN);
subNaN = isnan(I);

Image = filterSpike(I, Threshold, RestoreThreshold, Margin, Fig, Fig2, 'OtherImages', OtherImages);

%% Output image

that(1) = inherit(this, Image(:,:,1), 'subx', subx, 'suby', suby, 'AppendName', 'filterSpikeBathy', 'ValNaN', NaN);

%% Cr�ation du layer Polyval

if size(Image,3) >= 2
    that(2) = inherit(this, Image(:,:,2), 'subx', subx, 'suby', suby, 'AppendName', 'filterSpikeBathyPolyval', 'ValNaN', NaN);
end

%% Cr�ation du layer ResiduNormalise

if size(Image,3) >= 3
    that(3) = inherit(this, Image(:,:,3), 'subx', subx, 'suby', suby, 'AppendName', 'filterSpikeBathyResiduals', 'ValNaN', NaN, 'DataType', 1);
end

%% Cr�ation du layer Masque

that(end+1) = inherit(this, isnan(Image(:,:,1)) & ~subNaN, 'subx', subx, 'suby', suby, 'AppendName', 'filterSpikeBathyMask', ...
    'ValNaN', NaN, 'CLim', [0 1], 'DataType', cl_image.indDataType('Mask'));

