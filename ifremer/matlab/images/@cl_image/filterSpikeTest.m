% Spike filter
%
% Syntax
%   b =  filterSpikeTest(a, Threshold, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%   Threshold : Multiplier of the standard deviations
%
% Name-Value Pair Arguments
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image. 
%       2 images are created for every instance of a.
%       The former is the filtered image, the later is the mask
%
% Examples
%   I = Prismed;
%   [N, M] = size(I);
%   Np = floor(N*M / 10);
%   xd = randi(M, [Np 1]);
%   yd = randi(N, [Np 1]);
%   Noise = rand([Np 1]) * 100;
%   for k=1:Np
%       I(yd(k),xd(k)) = I(yd(k),xd(k)) + Noise(k);
%   end
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = filterSpikeTest(a, 200);
%   imagesc(b);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterSpikeTest(this, Threshold, varargin)

N = length(this);
that = repmat(cl_image,3,N);
hw = create_waitbar(waitbarMsg('filterSpikeTest'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(:,k) = filterSpikeTest_unit(this(k), Threshold, varargin{:});
    if N > 1
        that(:,k) = optimiseMemory(that(:,k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)
that = that(:);


function that = filterSpikeTest_unit(this, Threshold, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contols

% Should check that the number of slides is 1

%% Algorithm

hFilter = zeros(3)-1;
n = numel(hFilter);
hFilter(ceil(n/2)) = n - 1;
I = this.Image(suby,subx);
I = singleUnlessDouble(I, this.ValNaN);
subNaN = isnan(I);
I(:,:,2) = NaN;

fun = @(block_struct) kernelSpikeFilter(block_struct.data, hFilter, Threshold);
Image = blockproc(I, [100 100], fun, 'BorderSize', [3 3]);

%% Output images

% Filtered image
that(1) = inherit(this, Image(:,:,1), 'subx', subx, 'suby', suby, 'AppendName', 'filterSpikeTest', 'ValNaN', NaN);

% Cr�ation du layer Masque
that(2) = inherit(this, Image(:,:,2), 'subx', subx, 'suby', suby, 'AppendName', 'filterSpikeTestFilter', 'ValNaN', NaN);

% Cr�ation du layer Masque
that(3) = inherit(this, isnan(Image(:,:,1)) & ~subNaN, 'subx', subx, 'suby', suby, 'AppendName', 'filterSpikeTestMask', ...
    'ValNaN', NaN, 'CLim', [0 1], 'DataType', cl_image.indDataType('Mask'));



function X = kernelSpikeFilter(X, hFilter, Threshold)

X(:,:,2) = filter2(hFilter, X(:,:,1));
sub = (X(:,:,2) > Threshold);
Y = X(:,:,1);
Y(sub) = NaN;
X(:,:,1) = Y;
