% Wallis filter of an image
%
% Syntax
%   b = filterWallis(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   window  : Sliding window size (Default : [3 3])
%   moy_ref : Moyenne vers laquelle on veut se repprocher (Default : 128)
%   ect_ref : Ecart-type vers lequel on veut se rapprocher (Default : 50)
%   alpha   : Force de rappel de la moyenne vers  moy_ref [0 1] (Default : 0.8)
%   beta    : Force de rappel de l'ecart-type vers ect_ref [0 1] (Default : 0)
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%    nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%    I = imread(nomFic);
%    a = cl_image('Image', I, 'ColormapIndex', 2);
%    imagesc(a)
%
%    b = filterWallis(a);
%    imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterWallis(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterWallis'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterWallis_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = filterWallis_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, alpha]   = getPropertyValue(varargin, 'alpha',   0.8);
[varargin, beta]    = getPropertyValue(varargin, 'beta',    0);
[varargin, moy_ref] = getPropertyValue(varargin, 'moy_ref', 128);
[varargin, ect_ref] = getPropertyValue(varargin, 'ect_ref', 50);
[varargin, window]  = getPropertyValue(varargin, 'window',  [5 5]); %#ok<ASGLU>

%% Algorithm

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    I = singleUnlessDouble(I, this.ValNaN);
    
    I = nlfilterSpeckle(I, 1, window, 'wallisaugustin', window, alpha, beta, moy_ref, ect_ref);
    
    Image(:,:,k) = I;
end

%% Output image

Parameters = getHistoryParameters(alpha, beta, moy_ref, ect_ref, window);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'filterWallis', 'ValNaN', NaN, 'Parameters', Parameters);
