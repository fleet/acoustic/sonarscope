% Wiener filter of an image
%
% Syntax
%   b = filterWiener(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   window : Sliding window size (Default : [7 7])
%   subx   : Sub-sampling in abscissa
%   suby   : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = filterWiener(a);
%   imagesc(b);
%
% See also cl_image/filterLee Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = filterWiener(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('filterWiener'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = filterWiener_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)

% TODO : � partir de filterWiener et filterGauss_unit, cr�er un container
% g�n�raliste qui puisse accueillir tous les filtrages. Am�liorer le cas o�
% on demande une extraction : extraire une image plus grande plut�t que
% l'image demand�e et de lui faire un "enlarge" ensuite.

function this = filterWiener_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []);
[varargin, window]    = getPropertyValue(varargin, 'window',    [7 7]); %#ok<ASGLU>

%% Compute filter parameters

Parameters = getHistoryParameters(window);
AppendName = 'filterWiener';
hfun = @wiener2;

%% Algorithm

this = computeBlocprocFun(this, subx, suby, hfun, window, LayerMask, valMask, AppendName, Parameters);

% ATTENTION : le fait d'utiliser blocproc peut g�n�rer des diff�rences
% d'aspect sur chaque dalle car le bruit est estim� individuellement sur
% chaque dalle.


%{
function res = Wiener(I, window)

N = prod(window);
meanFilter = ones(window, 'single');
meanFilterM = meanFilter / N;
meanFilterV = meanFilter / (N-1);
m = filter2(meanFilterM, I);
v = filter2(meanFilterV, I .* I);
k = (m .* m) ./ v;
k(k > 1) = 1;
k = 1 - k;
res = m + k .*(I - m);
%}
