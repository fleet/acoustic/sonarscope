function [this, flag] = filtrageSignal(this, nomSignal, OrdreFiltre, Wc)

%% Lecture du signal

val = get(this, nomSignal);
 
%% Filtrage

OK = 0;
while ~OK
    [B,A] = butter(OrdreFiltre, Wc);

    x = 1:length(val);
    subNaN = find(isnan(val));
    subNonNaN = find(~isnan(val));
    if ~isempty(subNonNaN)
        val(subNaN) = interp1(x(subNonNaN), val(subNonNaN), x(subNaN), 'linear', 'extrap');
    end

    if strcmp(nomSignal, 'Heading')
        % Filtrage d'un cap en degres
        valFiltree = val * (pi/180);
        valFiltree = unwrap(valFiltree);
        valFiltree1 = filtfilt(B, A, double(valFiltree));
        valFiltree1 = valFiltree1 * (180/pi);
        valFiltree1 = mod(valFiltree1+360, 360);
    else
        valFiltree1 = filtfilt(B, A, double(val));
    end

    str1 = 'D�truire la figure quand l''inspection est finir';
    str2 = 'Destroy the figure when inspection is finished';
    fig = FigUtils.createSScFigure('Name', Lang(str1,str2));
    plot(x, val, 'k'); grid on; hold on; plot(x, valFiltree1, 'b'); zoom on;
    uiwait(fig)
    
    [rep, flag] = my_questdlg(Lang('La courbe bleue �tait-elle satisfaisante','Was the blue curve correct ?'));
    if ~flag
        return
    end

    switch rep
        case 2  % 'NONE'
            [flag, OrdreFiltre, Wc] = SaisieParamsButterworth( OrdreFiltre, Wc);

        case 1  % 'Blue'
            OK = 1;
            val(:) = valFiltree1;
    end
end

%% Sauvegarde du signal

this = set(this, nomSignal, val);
