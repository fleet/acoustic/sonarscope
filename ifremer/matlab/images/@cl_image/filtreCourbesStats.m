% Nettoyage d'une courbe de statistiques conditionnelles
%
% Syntax
%   this = filtreCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%   Ordre : Ordre du filtre de Butterworth
%   Wc    : Pulsation de coupure normalisée
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = filtreCourbesStats(this, varargin)

[varargin, subRoi] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));
[varargin, Ordre]  = getPropertyValue(varargin, 'Ordre', 2);
[varargin, Wc]     = getPropertyValue(varargin, 'Wc', 0.05);

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    subRoi = subRoi(end);
end

if isempty(subRoi)
    message_NoCurve(this)
    return
end

for iRoi=1:length(subRoi)
    bilan = this.CourbesStatistiques(subRoi(iRoi)).bilan{1};
    
    if length(bilan) > 1
        for i=1:length(bilan)
            strChoix{i} = sprintf('%s %d ', bilan(i).nomVarCondition, i);%#ok
        end
        str1 = 'Type de visualisation';
        str2 = 'Type of visualization';
        [liste, validation] = my_listdlg(Lang(str1,str2), strChoix);
        if ~validation || isempty(liste)
            return
        end
    else
        liste = 1;
    end
    
    
    [B,A] = butter(Ordre, Wc);
    for k=1:length(bilan) %length(liste)
        if any(liste == k)
            nx = bilan(k).nx;
            if isempty(nx)
                continue
            end
            y  = bilan(k).y;
            
            subNonNaN = ~isnan(y);
            if sum(subNonNaN) > 4*length(B)
                y(subNonNaN) = filtfilt(B, A, double(y(subNonNaN)));
            end
            
            new_bilan(k)   = bilan(k);%#ok
            new_bilan(k).y = y; %#ok
            new_bilan(k).nomZoneEtude = sprintf('%s - Filtre (%d,%f)', bilan(k).nomZoneEtude, Ordre, Wc); %#ok
            
            residu_bilan(k)   = bilan(k);%#ok
            residu_bilan(k).y = bilan(k).y - y; %#ok
            residu_bilan(k).nomZoneEtude = sprintf('%s - Residuals (%d,%f)', bilan(k).nomZoneEtude, Ordre, Wc); %#ok
        else
            new_bilan(k)    = bilan(k);%#ok
            residu_bilan(k) = bilan(k);%#ok
            residu_bilan(k).y(:) = 0; %#ok
            residu_bilan(k).nomZoneEtude = sprintf('%s - Residuals (No filter)', bilan(k).nomZoneEtude); %#ok
        end
    end
    %     for k=1:length(subRoi)
    this.CourbesStatistiques(end+1) = this.CourbesStatistiques(subRoi(iRoi));
    this.CourbesStatistiques(end).bilan{1} = residu_bilan;
    
    this.CourbesStatistiques(end+1) = this.CourbesStatistiques(subRoi(iRoi));
    this.CourbesStatistiques(end).bilan{1} = new_bilan;
    %     end
end
