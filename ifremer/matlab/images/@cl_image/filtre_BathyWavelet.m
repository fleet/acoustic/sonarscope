% Filtrage de defauts d'attitude par ondelette sur bathy
%
% Syntax
%   b =  filtre_BathyWavelet(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx  : subsampling in X
%   suby  : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%
% See also cl_image Authors
% Authors : GLT
% ----------------------------------------------------------------------------

function this = filtre_BathyWavelet(this, varargin)

%% Pr�paration de l'image

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

subx = (subx(:))';
suby = suby(:);

subNaN = isnan(this.Image(suby,subx));

% inpainting pour remplacer artificiellement les NaNs.
[flag, that] = inpaint(this, 'subx', subx, 'suby', suby); % m�thode de base;
if ~flag
    return
end

%I = this.Image(suby,subx);
I = that.Image;
%subNaN = isnan(I);

%% Processing

[nb_ping, nb_Beam] = size(I);

% Tentative toute simple
for iBeam=1:nb_Beam
    sigBrut = I(:, iBeam)';
    %zero padding ?!
    sigBrut = [sigBrut, zeros(1, 2^nextpow2(nb_ping) - nb_ping)];
    QMF8        = makeONFilter('Symmlet', 8);
    %scaledmr    = NormNoise(sigBrut, QMF8);
    %y           = scaledmr *1.2;
    [xh, wcoef] = WaveShrink(sigBrut, 'visu', 5, QMF8);
    signalfilt_complet(:, iBeam) = xh(1:nb_ping)';
end

% % Param�tres � d�finir
% %long_filtrage = 2048;
% c = 1;
%
% [nb_ping, nb_Beam] = size(I);
% long_filtrage = 2^( nextpow2(nb_ping) - 1 ); % Puissance de 2 juste inf�rieur
%
% signalfilt_complet  = I;
% prof                = log2(long_filtrage);
% qmf                 = MakeONFilter('Daubechies',12); % cr�ation de l'ondelette cf. WaveLab850
% bord                = floor(0.1 * long_filtrage);
%
% % on elimine les extr�mit�s du faisceau sur 10 pourcents de part et d'autre de la zone traitee
% longbord    = long_filtrage - 2 * bord;
%
% % definition des coefficient a eliminer sous la forme
% %   d = echelle de decomposition
% %   b = indice du bloc a eliminer dans le degre d
% % TODO : Param�trage � d�finir
% bond    = [ 3 2 3 4 5 6 7 16 ];
% dond    = [ 5 4 4 4 4 4 4 5 ];
% nbond   = length(bond);
%
% % definition de la limite HF / BF
% bHF = floor( .40 * long_filtrage ) * ones( nb_Beam , 1 );
%
% % definition de la longueur du filtre median sur le seuil energetique
% medseuil = 3;
%
%
% for iBlock = 0 : longbord : (nb_ping - long_filtrage)
%
%     % Boucle sur block de longueur long_filtrage
%     % TODO remplacer par block processing
%
%     signal = I( (iBlock+1 : iBlock+long_filtrage) , : );
%
%     % initialisation de la taille des objets
%     signalfilt  = signal;
%     wp          = zeros(long_filtrage , prof+1 , nb_Beam);
%     stree       = zeros(1 , 2*long_filtrage-1 , nb_Beam);
%     signalcour  = signal;
%     coeff       = zeros(1 , long_filtrage , nb_Beam);
%     coeffHF     = zeros(1 , long_filtrage , nb_Beam);
%
%     wpfilt  = wp;
%     btree   = zeros(1 , 2*long_filtrage-1 , nb_Beam);
%     vtree   = zeros(1 , 2*long_filtrage-1 , nb_Beam);
%
%     moye    = nanmean(signal);
%
%     for iBeam = 1 : nb_Beam
%         % Analyse en paquet d'ondelette et calcul d'entropie pour chaque faisceau
%
%         % on elimine la valeur moyenne sur chaque faisceau
%         signalcour(:,iBeam) = signal(:,iBeam) - moye(:,iBeam);
%
%         % on decompose en paquets d'ondelettes
%         fprintf ('decomposition bloc %g faisceau %g       \n',iBlock,iBeam);
%         wp (:,:,iBeam) = WPAnalysis( signalcour(:,iBeam) , prof , qmf);         % cf. WaveLab850
%
% %         figure,
% %         [w, btree] = WPTour('P', signalcour(:,iBeam), prof, qmf, 'essai');
%
%         % on calcule l'arbre des entropies pour chaque faisceau
%         stree(:,:,iBeam) = CalcStatTree( wp(:,:,iBeam) , 'Entropy' );
%
%     end
%
%     for l = 1 : nbond
%         % annulation des coefficients lies aux ondulations
%         wp( 1 + bond(l)*long_filtrage / 2^dond(l) : (bond(l)+1)*long_filtrage/2^dond(l) ,...
%             dond(l)+1 , ...
%             : ) = 0;
%     end
%
%     for iBeam = 1 : nb_Beam
%         % dans l'arbre des entropies, les coefs lies aux ondulations sont mis a 0, pour que la base adaptee passe par ces coeff
%         for l =  1 : nbond
%             stree( 1 , node(dond(l),bond(l)) , iBeam ) = 0;                     % cf. WaveLab850
%         end
%     end
%
%     for iBeam = 2 : nb_Beam-1                                                   % ! On zappe le 1ier et dernier faisceau
%         % on calcule la base adaptee a l'arbre des entropies
%         fprintf ('filtrage longitudinal bloc %g faisceau %g       \n', iBlock, iBeam);
%         [bt, vt] = BestBasis( stree(:,:,iBeam) , prof );                        % cf. WaveLab850
%         btree(:,:,iBeam) = bt;
%         vtree(:,:,iBeam) = vt;
%
%         % filtrage transversal par moyennage frequentiel sur 3 faisceaux successifs
%         coeff(:,:,iBeam) = 1/3 * (...
%                 UnpackBasisCoeff( btree(:,:,iBeam), wp(:,:,iBeam-1) ) + ...     % cf. WaveLab850
%                 UnpackBasisCoeff( btree(:,:,iBeam), wp(:,:,iBeam)   ) + ...
%                 UnpackBasisCoeff( btree(:,:,iBeam), wp(:,:,iBeam+1) ) );
%     end
%
%
%     % cas particulier du premier et du dernier faisceau
%
%     % 1ier faisceau
%     [bt, vt] = BestBasis( stree(:,:,1), prof );
%     btree(:,:,1) = bt;
%     vtree(:,:,1) = vt;
%     coeff(:,:,1) = 1/3 * (...
%         UnpackBasisCoeff( btree(:,:,1), wp(:,:,1) )+...
%         UnpackBasisCoeff( btree(:,:,1), wp(:,:,2) )+...
%         UnpackBasisCoeff( btree(:,:,1), wp(:,:,3) ) );
%
%     % dernier faisceau
%     [bt, vt] = BestBasis( stree(:,:,nb_Beam), prof );
%     btree(:,:,nb_Beam) = bt;
%     vtree(:,:,nb_Beam) = vt;
%     coeff(:,:,nb_Beam) = 1/3 * (...
%         UnpackBasisCoeff( btree(:,:,nb_Beam), wp(:,:,nb_Beam-1) )+...
%         UnpackBasisCoeff( btree(:,:,nb_Beam), wp(:,:,nb_Beam)   )+...
%         UnpackBasisCoeff( btree(:,:,nb_Beam), wp(:,:,nb_Beam-2) ));
%
%     %% calcul du seuil energetique
%     fprintf ('seuillage energetique                             \n');
%
%     coeffHF     = coeff;
%     coeffHF( 1 , 1:bHF(iBeam) , :) = 0;
%
%     coeffBFHF   = coeffHF(1,:,:);
%
%     seuil = 1 - ( sum(coeffHF.^2,2) ./ mean(sum(coeffBFHF.^2,2)) );
%     seuil = 1 - ( sum(coeffHF.^2,2) / sum(coeff(1,:,iBeam).^2) );
%
%
%     vectseuil = zeros(nb_Beam,1);
%
%     for iBeam = 1 : nb_Beam
%         lmax = min( nb_Beam , iBeam+medseuil );
%         lmin = max( 1       , iBeam-medseuil );
%         vectseuil(iBeam) = mean( seuil( 1, 1, lmin:lmax ) );
%     end
%
%     s = vectseuil;
%     vectseuil = c * vectseuil / max(vectseuil);
%
%     fprintf ('\n');
%
%     for iBeam = 1 : nb_Beam
%         fprintf ('reconstruction bloc %g faisceau %g seuil %g        \n',iBlock,iBeam,vectseuil(iBeam));
%
%         % on seuille les coefficients
%         coefffilt = ThreshEnergy( coeff(:,:,iBeam), vectseuil(iBeam) );
%         wpfilt(:,:,iBeam) = PackBasisCoeff( btree(:,:,iBeam), wp(:,:,iBeam), coefffilt(:,:) );
%
%         % reconstruction des signaux a partir des coeffs
%         signalfilt(:,iBeam) = WPSynthesis( btree(:,:,iBeam), wpfilt(:,:,iBeam), qmf )' + moye(:,iBeam);
%
%     end
%
%     % fprintf ('\n');
%     % on remplace les nouveaux signaux dans le reste de l'image non filtree.
%     signalfilt_complet( (iBlock+bord+1) : (iBlock+long_filtrage-bord) , : ) = ...
%         signalfilt( (1+bord) : (long_filtrage-bord) , : );
%
% end

%% Cr�ation image de sortie

signalfilt_complet(subNaN) = NaN;
this = replace_Image(this, signalfilt_complet);
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
this.CLim               = [this.StatValues.Min this.StatValues.Max];
this.ColormapIndex      = 3;

%% Compl�tion du titre

ImageName = extract_ImageName(this);
this = update_Name(this, 'Name', [ImageName '_filtWavelet']);


function coefffilt = ThreshEnergy( coeff, seuil )

% algorithme de seuillage energetique
%
% coeff_filtres=ThreshEnergy(coeff_a_filtrer,seuil)
%
% seuil compris entre 0 et 1
%
% Les coeffs sont ranges par ordres croissants
% on calcule l'energie cumulee coeff par coeff dans cet ordre
% quand on a atteint seuil*l'energie totale,
% on met a zeros les coeffs qui n'ont pas ete utilises.

energy  = sum( coeff.^2 );
[coeffs, indexs] = sort( abs(coeff) );

coeffs  = reverse(coeffs);
indexs  = reverse(indexs);

vect    = cumsum( coeffs.^2 );
vectn   = vect/energy;

%[m,n]   = size(vectn);

index       = find( vectn>seuil );
coefffilt   = coeff;

if length(index) > 1
    coefffilt( indexs( index( 2 : length(index) ) ) )=0;
end
