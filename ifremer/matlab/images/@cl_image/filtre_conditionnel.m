function this = filtre_conditionnel(this, LayerConditionnel, Sigma, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask', []); %#ok<ASGLU>

%% Calcul des filtres

Weights = cell(86,1);
for k=1:86
    SigmaAngle = Sigma * tand(k)/tand(60);
    n = ceil(2 * SigmaAngle);
    window = [max(3,2*n+1) max(3,2*n+1)];
    Weights{k} = fspecial('gaussian', window, SigmaAngle);
    %     figure(20081008); imagesc(-n:n, -n:n, Weights{k}); axis square; colorbar; title('Gaussian coefficient filter'); drawnow
end

%% Extraction de l'image

I = this.Image(suby,subx);
[C, flag]  = extraction(LayerConditionnel, 'XLim', this.x(subx([1 end])), 'YLim', this.y(suby([1 end])), 'ForceExtraction');
if ~flag
    return
end
C = C.Image;

%% Gestion du masque

if ~isempty(LayerMask)
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
    K = zeros(size(Masque), 'uint8');
    for ik=1:length(valMask)
        %             sub = find(Masque == valMask(ik));
        sub = (Masque == valMask(ik));
        K(sub) = 1;
    end
    subNonMasque = find(K == 0);
    K = I(subNonMasque);
end

%% Filtrage

% [I, subNaN] = windowEnlarge(I, window, []);
% if ~isa(I, 'double')
%     I = single(I);
% end
% I = filter2(h, I, 'valid');
% I(subNaN) = NaN;

D = NaN(size(I), class(I));
nbCol = length(subx);
nbRows = length(suby);
str1 = 'Filtrage conditionnel';
str2 = 'TODO';
AnglePrecedent = -1;
hw = create_waitbar(Lang(str1,str2), 'N', nbCol);
for iCol=1:nbCol
    my_waitbar(iCol, nbCol, hw)
    for iLig=1:length(suby)
        if ~isnan(I(iLig,iCol))
            Angle = 1 + floor(abs(C(iLig,iCol)));
            if Angle ~= AnglePrecedent
                
                %{
TODO
Index exceeds matrix dimensions.
Error in cl_image/filtre_conditionnel (line 72)
h = Weights{Angle};

                %}
                h = Weights{Angle};
            end
            AnglePrecedent = Angle;
            [m,n] = size(h);
            ms2 = floor(m/2);
            ns2 = floor(n/2);
            ms2p1 = ms2+1;
            ns2p1 = ns2+1;
            X = 0;
            N = 0;
            for kn=-ns2:ns2
                iColPluskn = iCol + kn;
                if (iColPluskn < 1) || (iColPluskn > nbCol)
                    continue
                end
                knpns2p1 = kn+ns2p1;
                for km=-ms2:ms2
                    iLigPluskm = iLig + km;
                    if (iLigPluskm < 1) || (iLigPluskm > nbRows)
                        continue
                    end
                    w = h(km+ms2p1,knpns2p1);
                    X = X + I(iLigPluskm,iColPluskn) * w;
                    N = N + w;
                end
            end
            D(iLig,iCol) = X / N;
        end
    end
end
my_close(hw, 'MsgEnd');

if ~isempty(LayerMask)
    D(subNonMasque) = K;
end

this = replace_Image(this, D);
this.ValNaN = NaN;

%% Mise � jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

Append = ['ConditionalFilter ' num2strCode([window Sigma])];
this = update_Name(this, 'Append', Append);
