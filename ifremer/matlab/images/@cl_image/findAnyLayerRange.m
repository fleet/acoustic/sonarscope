function [flag, indLayerRange] = findAnyLayerRange(this, indImage, varargin)

[varargin, WithCurrentImage] = getPropertyValue(varargin, 'WithCurrentImage', 0);

[varargin, AtLeastOneLayer] = getFlag(varargin, 'AtLeastOneLayer'); %#ok<ASGLU>

indLayerRange = findIndLayerSonar(this, indImage, 'strDataType', 'TwoWayTravelTimeInSeconds', 'OnlyOneLayer', 'WithCurrentImage', WithCurrentImage);
if isempty(indLayerRange)
    indLayerRange = findIndLayerSonar(this, indImage, 'strDataType', 'TwoWayTravelTimeInSamples', 'OnlyOneLayer');
    if isempty(indLayerRange)
        indLayerRange = findIndLayerSonar(this, indImage, 'strDataType', 'RayPathSampleNb', 'OnlyOneLayer');
        if isempty(indLayerRange) && AtLeastOneLayer
            str1 = 'Aucun layer de type "RayPathSampleNb" ou "TwoWayTravelTimeInSeconds" ou ... n''a �t� trouv�.';
            str2 = 'No layer of type "RayPathSampleNb" or "TwoWayTravelTimeInSeconds" or ... was found.';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        end
    end
end

flag = 1;
