% Recherche d'un layer Sonar d'un type particulier synchronise en X et en Y
%
% [choix, nomLayer] = findIndLayerSonar(this, ...)
%
% Input Arguments
%   choix    : Numeros des layers satisfaisant les conditions
%   nomLayer : Noms des layers
%
% Name-Value Pair Arguments
%   DataType : Valeurs definies dans cl_image/cl_image : strTypeDonee
%
% Name-only Arguments
%   WithCurrentImage : Liste comprenant l'image courante
%   OnlyOneLayer     : Un seul layer authorise en sortie
%
% Output Arguments
%   this : instance de cl_image initialis�e
%
% See also findOneLayerDataType listeLayersSynchronises Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : [flag, choix, nomLayer] =

function [choix, nomLayer, DataTypeOut] = findIndLayerSonar(this, indImage, varargin)

[varargin, DataTypeATester]     = getPropertyValue(varargin, 'DataType',         []);
[varargin, strDataTypeATester]  = getPropertyValue(varargin, 'strDataType',      []);
[varargin, Tag]                 = getPropertyValue(varargin, 'Tag',              []);
[varargin, NoTag]               = getPropertyValue(varargin, '~Tag',             []);
[varargin, TagComments]         = getPropertyValue(varargin, 'TagComments',      []);
[varargin, NoTagComments]       = getPropertyValue(varargin, '~TagComments',     []);
[varargin, TitleWindowGUI]      = getPropertyValue(varargin, 'TitleWindowGUI',   []);
[varargin, WithCurrentImage]    = getPropertyValue(varargin, 'WithCurrentImage', 0);
[varargin, FirstOfList]         = getPropertyValue(varargin, 'FirstOfList',      0);
[varargin, Mute]                = getPropertyValue(varargin, 'Mute',             0);

[varargin, OnlyOneLayer]    = getFlag(varargin, 'OnlyOneLayer');
[varargin, AtLeastOneLayer] = getFlag(varargin, 'AtLeastOneLayer'); %#ok<ASGLU>

PS  = cl_image.indGeometryType('PingSamples');
PR  = cl_image.indGeometryType('PingRange');
PAD = cl_image.indGeometryType('PingAcrossDist');

if isempty(DataTypeATester)
    if isempty(strDataTypeATester)
        DataTypeATester = 1;
    else
        if ischar(strDataTypeATester)
            DataTypeATester = cl_image.indDataType(strDataTypeATester);
        else
            for k=1:length(strDataTypeATester)
                DataTypeATester(k) = cl_image.indDataType(strDataTypeATester{k});
            end
        end
    end
end

DataTypeOut = DataTypeATester;

indLayers = 1:length(this);
for k=length(this):-1:1
    thisOne = this(k);
    txtTitre{k}     = thisOne.Name;
    txtComments{k}  = thisOne.Comments;
    txtSynchroX{k}  = thisOne.TagSynchroX;
    txtSynchroY{k}  = thisOne.TagSynchroY;
    GeometryType(k) = thisOne.GeometryType;
    DataType(k)     = thisOne.DataType';
    if GeometryType(k) == PS
        x = get(thisOne, 'SonarResolutionD');
        if isempty(x)
            % On ne devrait pas passer par l�
            resolution(k) = thisOne.x(2) - thisOne.x(1);
        else
            resolution(k) = x(1); % TODO Sondeur Dual
        end
    elseif GeometryType(k) == PR
        x = get(thisOne, 'SonarResolutionD');
        if isempty(x)
            % On ne devrait pas passer par l�
            resolution(k) = thisOne.x(2) - thisOne.x(1);
        else
            resolution(k) = x(1); % TODO Sondeur Dual
        end
    elseif GeometryType(k) == PAD
        resolution(k) = thisOne.x(2) - thisOne.x(1);
    else
        resolution(k) = 0; % ????????????????????
    end
end
if ~WithCurrentImage
    indLayers(indImage) = [];
end
subMemeTagSynchroX = strcmp(txtSynchroX(indLayers), txtSynchroX(indImage));
subMemeTagSynchroY = strcmp(txtSynchroY(indLayers), txtSynchroY(indImage));
subMemeTagSynchroS = (GeometryType(indLayers) == GeometryType(indImage));

subMemeDataType = false(1, length(indLayers));
for k=1:length(indLayers)
    subMemeDataType(k) = subMemeDataType(k) || any(DataType(indLayers(k)) == DataTypeATester); % Modifi� par JMA le 02/07/2011
end

if resolution(indImage) == 0
    indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeDataType)) = [];
else
    subMemeresolution = abs(1 - resolution(indLayers) / resolution(indImage)) < 1e-3;
    indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeDataType & subMemeresolution)) = [];
end

%% Message � l'op�rateur si il n'y a aucun layer trouv� ET qu'on a demand� AU MOINS un layer

if isempty(indLayers)
    choix = [];
    nomLayer = '';
    if AtLeastOneLayer
        if ~Mute
            str1 = sprintf('Aucun layer de type "%s" trouv�.', cl_image.strDataType{DataTypeATester});
            str2 = sprintf('No layer "%s" found in the list.', cl_image.strDataType{DataTypeATester});
            my_warndlg(Lang(str1,str2), 1);
        end
    end
    return
end

choix       = indLayers;
nomLayer    = txtTitre(choix);
txtComments = txtComments(choix);

%% Filtrage des layers si on demande unquement les layers contenant la chaine de caract�re d�finie dans Tag

if ~isempty(Tag)
    sub = false(1,length(nomLayer));
    for k=1:length(nomLayer)
        rep = strfind(nomLayer{k}, Tag);
        sub(k) = ~isempty(rep);
    end
    choix    = choix(sub);
    nomLayer = nomLayer(sub);
    txtComments = txtComments(sub);
end

%% Filtrage des layers si on demande uniquement les layers ne contenant pasla chaine de caract�re d�finie dans Tag

if ~isempty(NoTag)
    sub = false(1,length(nomLayer));
    for k=1:length(nomLayer)
        rep = strfind(nomLayer{k}, NoTag);
        sub(k) = isempty(rep);
    end
    choix    = choix(sub);
    nomLayer = nomLayer(sub);
    txtComments = txtComments(sub);
end

%% Filtrage des layers si on demande unquement les layers contenant la chaine de caract�re d�finie dans TagComments

if ~isempty(TagComments)
    sub = false(1,length(txtComments));
    for k=1:length(txtComments)
        rep = strfind(txtComments{k}, TagComments);
        sub(k) = ~isempty(rep);
    end
    choix    = choix(sub);
    nomLayer = nomLayer(sub);
    txtComments = txtComments(sub);
end

%% Filtrage des layers si on demande uniquement les layers ne contenant pasla chaine de caract�re d�finie dans TagComments

if ~isempty(NoTagComments)
    sub = false(1,length(nomLayer));
    for k=1:length(nomLayer)
        rep = strfind(txtComments{k}, NoTagComments);
        sub(k) = isempty(rep);
    end
    choix    = choix(sub);
    nomLayer = nomLayer(sub);
    txtComments = txtComments(sub); %#ok<NASGU>
end

%% S�lection du layer si on en demande qu'un seul

if OnlyOneLayer
    if length(choix) > 1
        if FirstOfList
            for k=1:length(DataTypeATester)
                DataTypeATester(k);
                sub = find(DataType(choix) == DataTypeATester(k), 1,'first');
                if ~isempty(sub)
                    break
                end
            end
            choix    = choix(sub);
            nomLayer = nomLayer(sub);
        else
            if isempty(TitleWindowGUI)
                if DataTypeATester == 1
                    str1 = 'S�lectionnez un layer parmi ceux-ci';
                    str2 = 'Select one image among these ones';
                else
                    strDT = '';
                    for k=1:length(DataTypeATester)
                        strDT = [strDT '"' cl_image.strDataType{DataTypeATester(k)} '" or ']; %#ok<AGROW>
                    end
                    strDT(end-3:end) = [];
                    str1 = sprintf('Un layer de type %s est n�cessaire dans cette �tape, SSc en trouve plusieurs, choisissez celui qui convient.', strDT);
                    str2 = sprintf('A layer %s is necessary for this processing, SSc found several ones, select the appropriate one.', strDT);
                end
                TitleWindowGUI = Lang(str1,str2);
            end
            
            % TODO : ici, il faudrait s�lectionner le layer qui est le plus
            % haut dans la liste de DataTypeATester
            InitialValue = length(choix);
            
            [rep, flag] = my_listdlg(TitleWindowGUI, ...
                nomLayer, 'SelectionMode', 'Single', 'InitialValue', InitialValue);
            if ~flag
                choix = [];
                nomLayer = '';
                return
            end
            nomLayer = nomLayer(rep);
            choix = choix(rep);
        end
    end
end

%% DataType qui a �t� s�lectionn�

DataTypeOut = DataType(choix);
