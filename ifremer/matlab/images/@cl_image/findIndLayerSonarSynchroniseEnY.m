% Recherche d'un layer Sonar d'un type particulier synchronise en Y seulement
%
% [choix, nomLayer] = findIndLayerSonarSynchroniseEnY(this, ...)
%
% Input Arguments
%   choix    : Numeros des layers satisfaisant les conditions
%   nomLayer : Noms des layers
%
% Name-Value Pair Arguments
%   DataType : Valeurs definies dans cl_image/cl_image : strTypeDonee
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% Remarks : Cette methode devrait s'appeler imagesc tout simplement. Si on
% fait cela, matlab se plante, il faut alors dplacer cette mthode dans le
% repertoire clc_image
%
% See also listeLayersSynchronises clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [choix, nomLayer] = findIndLayerSonarSynchroniseEnY(this, indImage, varargin)

TC = this(indImage).GeometryType;
[varargin, GeometryTypeATester] = getPropertyValue(varargin, 'GeometryType', TC);
[varargin, DataTypeATester]     = getPropertyValue(varargin, 'DataType', []);
[varargin, WithCurrentImage]    = getPropertyValue(varargin, 'WithCurrentImage', 1); %#ok<ASGLU>

indLayers = 1:length(this);
for k=1:length(this)
    txtValeurV{k}   = this(k).Name; %#ok<AGROW>
    txtSynchroY{k}  = this(k).TagSynchroY; %#ok<AGROW>
    GeometryType(k) = this(k).GeometryType; %#ok<AGROW>
    DataType(k)     = this(k).DataType; %#ok<AGROW>
end
if ~WithCurrentImage
    indLayers(indImage) = [];
end
subMemeTagSynchroY = strcmp(txtSynchroY(indLayers), txtSynchroY(indImage));
subMemeGeometryType = (GeometryType(indLayers) == GeometryTypeATester);
if isempty(DataTypeATester)
    subMemeDataType  = ones(size(indLayers));
else
    subMemeDataType  = (DataType(indLayers) == DataTypeATester);
end

indLayers(~(subMemeTagSynchroY & subMemeGeometryType & subMemeDataType)) = [];

if isempty(indLayers)
    choix = [];
    nomLayer = '';
    return
end
choix = indLayers;
nomLayer = txtValeurV(choix);
