function [indLayers, nomsLayers] = findLayers(this, varargin)

[varargin, DataType]     = getPropertyValue(varargin, 'DataType'    , []);
[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []); %#ok<ASGLU>

indLayers  = [];
nomsLayers = {};
for k=1:length(this)
    if ~isempty(DataType)
        DT = this(k).DataType;
        if DT ~= DataType
            continue
        end
    end
    if ~isempty(GeometryType)
        GT = this(k).GeometryType;
        if GT ~= GeometryType
            continue
        end
    end
    indLayers(end+1) = k; %#ok
    nomsLayers{end+1} = this(k).Name; %#ok
end
