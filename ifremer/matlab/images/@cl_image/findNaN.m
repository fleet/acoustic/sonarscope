% Description
%   Indices of the values of the image where the data is missing
%
% Syntax
%   sub = findNaN(this, ...)
%
% Name-Value Pair Arguments
%   subx : 
%   suby : 
%
% Examples
%   a = cl_image.import_Prismed;
%   subNaN = findNaN(a);
%     a.ValNaN
%     maskNaN = isnan(a);
%     figure; imagesc(maskNaN); colorbar;
%     subNonNaN = findNonNaN(a);
%
% Author : JMA
% See also ValNaN isnan findNonNaN

function sub = findNaN(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

if (isempty(subx) && isempty(suby)) || (isequal(subx, 1:this.nbColumns) && isequal(suby, 1:this.nbRows))
    % Pour profiter éventuellement de la gestion mémoire de cl_memmapfile
    if isnan(this.ValNaN)
        sub = isnan(this.Image);
    else
        if this.ValNaN < 3.4e38
            sub = (this.Image == this.ValNaN) | isnan(this.Image);
        else % Cas d'une image provenant de ErMapper
            sub = ((this.Image >= 3.4e38) | isnan(this.Image));
        end
    end
else
    I = this.Image(suby,subx,:);
    if isnan(this.ValNaN)
        sub = (isnan(I));
    else
        if this.ValNaN < 3.4e38
            sub = (I == this.ValNaN);
        else % Cas d'une image provenant de ErMapper
            sub = ((I >= 3.4e38) | isnan(I));
        end
    end
end
sub = find(sub);
