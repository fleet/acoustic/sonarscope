% Description
%   Indices of the values of the image where the data is not missing
%
% Syntax
%   sub = findNaN(this, ...)
%
% Name-Value Pair Arguments
%   subx : 
%   suby : 
%
% Examples
%   a = cl_image.import_Prismed;
%   subNonNaN = findNonNaN(a);
%     a.ValNaN
%     maskNaN = isnan(a);
%     figure; imagesc(maskNaN); colorbar;
%     subNaN = findNaN(a);
%
% Author : JMA
% See also ValNaN isnan findNonNaN

function sub = findNonNaN(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, subxyOnly] = getPropertyValue(varargin, 'subxyOnly', 0); %#ok<ASGLU>

if subxyOnly && (this.nbSlides > 1)
    % Cas o� on ne veut que les adresses en x et y
    I = single(this.Image(suby,subx,:));
    if ~isnan(this.ValNaN)
        if this.ValNaN < 3.4e38
            I(I == this.ValNaN) = NaN;
        else % Cas d'une image provenant de ErMapper
            I(I >= 3.4e38) = NaN;
        end
    end
    I = sum(I,3);
    sub = find(~isnan(I));
    
else
    % Cas g�n�ral ou cas d'une image comprenant plusieurs couches : on rend
    % l'adresse pr�cise de tous les pixels NaN de l'image
    
    if isequal(subx, 1:this.nbColumns) && isequal(suby, 1:this.nbRows)
        if isnan(this.ValNaN)
            sub = find(~isnan(this.Image));
        else
            if this.ValNaN < 3.4e38
                sub = find(this.Image ~= this.ValNaN);
            else % Cas d'une image provenant de ErMapper
                sub = find((this.Image < 3.4e38) & ~isnan(this.Image) );
            end
        end
    else
        I = this.Image(suby,subx,:);
        if isnan(this.ValNaN)
            sub = find(~isnan(I));
        else
            if this.ValNaN < 3.4e38
                sub = find(I ~= this.ValNaN);
            else % Cas d'une image provenant de ErMapper
                sub = find((I < 3.4e38) & ~isnan(I) );
            end
        end
    end
end
