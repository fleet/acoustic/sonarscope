% Descriptif general de la fonction (texte qui apparaitra dans le "Current Directory" de l'environnement matlab)
% Complement de la description (n'apparaitra pas dans le "Current Directory")
%
% Syntax
%   [flag, indLayer, nomLayers]  = findOneLayerDataType(this, indImage, DataType)
%
% Input Arguments
%   this     : Instance de clc_image
%   DataType : ch cl_image
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   indLayer  : Indice de l'image
%   nomLayers : Nom de l'image
%
% Examples
%   DataType = cl_image.indDataType('Bathymetry');
%   [flag, indLayer,  nomLayers]  = findOneLayerDataType(this, 1, DataType)
%
% See also findIndLayerSonar Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, indLayer, nomLayers] = findOneLayerDataType(this, indImage, identLayer)

switch cl_image.strDataType{identLayer}
    case {'TxAngle'; 'BeamPointingAngle'} % Pour �viter confusion avec cette version de SSc
        strDT = {'TxAngle'; 'BeamPointingAngle'};
    otherwise
        strDT = cl_image.strDataType(identLayer);
end
indLayer  = [];
nomLayers = {};
for k=1:length(strDT)
    identDataType = cl_image.indDataType(strDT(k));
    [indLayerOne_loop, nomLayers_loop] = findIndLayerSonar(this, indImage, 'DataType', identDataType, 'WithCurrentImage', 1);
    indLayer  = [indLayerOne_loop indLayer]; %#ok<AGROW>
    nomLayers = [nomLayers_loop nomLayers]; %#ok<AGROW>
end

% [indLayer, nomLayers] = findIndLayerSonar(this, indImage, 'DataType', identLayer, 'WithCurrentImage', 1);
LayerName = cl_image.strDataType{identLayer};

switch length(indLayer)
    case 0
        str1 = sprintf('Aucun layer "%s" trouv�.', LayerName);
        str2 = sprintf('No "%s" layer exists.', LayerName);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    case 1
        flag = 1;
    otherwise
        clear str
        for k=1:length(indLayer)
            str{k} = this(indLayer(k)).Name; %#ok
        end
        msg1 = sprintf('Plusieurs layers "%s" existent. Lequel voulez-vous utiliser ?', LayerName);
        msg2 = sprintf('Many "%s" layers exist. Which one do you want to use ?', LayerName);
        [rep, flag] = my_listdlg(Lang(msg1,msg2), str, 'SelectionMode', 'Single', 'InitialValue', 2);
        if ~flag
            return
        end
        indLayer = indLayer(rep);
end
