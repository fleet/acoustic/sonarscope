function this = flipSignals(this)

suby = this.nbRows:-1:1;
if isSonarSignal(this, 'Time')
    this.Sonar.Time = this.Sonar.Time(suby);
end
if isSonarSignal(this, 'PingNumber')
    this.Sonar.PingNumber       = this.Sonar.PingNumber(suby);
end
if isSonarSignal(this, 'Immersion')
    this.Sonar.Immersion        = this.Sonar.Immersion(suby);
end
if isSonarSignal(this, 'Height')
    this.Sonar.Height           = this.Sonar.Height(suby);
end
if isSonarSignal(this, 'CableOut')
    this.Sonar.CableOut         = this.Sonar.CableOut(suby);
end
if isSonarSignal(this, 'Speed')
    this.Sonar.Speed            = this.Sonar.Speed(suby);
end
if isSonarSignal(this, 'Heading')
    this.Sonar.Heading          = this.Sonar.Heading(suby);
end
if isSonarSignal(this, 'Roll')
    this.Sonar.Roll             = this.Sonar.Roll(suby);
end

if ~isempty(this.Sonar.RollUsedForEmission)
    this.Sonar.RollUsedForEmission = this.Sonar.RollUsedForEmission(suby);
end

if isSonarSignal(this, 'TxAlongAngle')
    this.Sonar.TxAlongAngle = this.Sonar.TxAlongAngle(suby);
end

% Attention this.Sonar.TxAlongAngle;

if isSonarSignal(this, 'Pitch')
    this.Sonar.Pitch            = this.Sonar.Pitch(suby);
end
if isSonarSignal(this, 'Yaw')
    this.Sonar.Yaw              = this.Sonar.Yaw(suby);
end
if isSonarSignal(this, 'SurfaceSoundSpeed')
    this.Sonar.SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed(suby);
end
if isSonarSignal(this, 'FishLatitude')
    this.Sonar.FishLatitude     = this.Sonar.FishLatitude(suby);
end
if isSonarSignal(this, 'FishLongitude')
    this.Sonar.FishLongitude    = this.Sonar.FishLongitude(suby);
end
if isSonarSignal(this, 'ShipLatitude')
    this.Sonar.ShipLatitude     = this.Sonar.ShipLatitude(suby);
end
if isSonarSignal(this, 'ShipLongitude')
    this.Sonar.ShipLongitude    = this.Sonar.ShipLongitude(suby);
end

if isSonarSignal(this, 'Heave')
    this.Sonar.Heave    = this.Sonar.Heave(suby);
end

if isSonarSignal(this, 'Tide')
    this.Sonar.Tide    = this.Sonar.Tide(suby);
end

if isSonarSignal(this, 'PingCounter')
    this.Sonar.PingCounter    = this.Sonar.PingCounter(suby);
end

if isSonarSignal(this, 'PortMode_1')
    this.Sonar.PortMode_1    = this.Sonar.PortMode_1(suby);
end

if isSonarSignal(this, 'PortMode_2')
    this.Sonar.PortMode_2    = this.Sonar.PortMode_2(suby);
end

if isSonarSignal(this, 'StarMode_1')
    this.Sonar.StarMode_1    = this.Sonar.StarMode_1(suby);
end

if isSonarSignal(this, 'StarMode_2')
    this.Sonar.StarMode_2    = this.Sonar.StarMode_2(suby);
end

if isSonarSignal(this, 'Interlacing')
    this.Sonar.Interlacing    = this.Sonar.Interlacing(suby);
end

if isSonarSignal(this, 'PresenceWC')
    this.Sonar.PresenceWC    = this.Sonar.PresenceWC(suby);
end

if isSonarSignal(this, 'PresenceFM')
    this.Sonar.PresenceFM    = this.Sonar.PresenceFM(suby);
end

if isSonarSignal(this, 'DistPings')
    this.Sonar.DistPings    = this.Sonar.DistPings(suby);
end

if isSonarSignal(this, 'NbSwaths')
    this.Sonar.NbSwaths    = this.Sonar.NbSwaths(suby);
end

if isSonarSignal(this, 'SampleFrequency')
    if length(this.Sonar.SampleFrequency) == 1 % Maintenant
        this.Sonar.SampleFrequency = [];
    else
        this.Sonar.SampleFrequency = this.Sonar.SampleFrequency(suby);
    end
end
if isSonarSignal(this, 'SonarFrequency')
    this.Sonar.SonarFrequency    = this.Sonar.SonarFrequency(suby);
end

