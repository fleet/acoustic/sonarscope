% Flip an image left to right
%
% Syntax
%   b = fliplr(a) 
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples 
%   a = cl_image('Image', Lena);
%   imagesc(a)
%
%   b = fliplr(a);
%   imagesc(b)
%
% See also cl_image/flipud Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = fliplr(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('fliplr'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = fliplr_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = fliplr_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

for k=this.nbSlides:-1:1
    I = this.Image(:,:,k);
    Image(:,:,k) = fliplr(I);
end

%% Output image

that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', 'fliplr');
