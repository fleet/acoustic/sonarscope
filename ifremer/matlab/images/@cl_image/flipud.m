% Flip an image up to down
%
% Syntax
%   b = flipud(a) 
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples 
%   a = cl_image('Image', Lena);
%   imagesc(a)
%
%   b = flipud(a);
%   imagesc(b)
%
% See also cl_image/fliplr Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = flipud(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('flipud'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = flipud_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = flipud_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

for k=this.nbSlides:-1:1
    I = this.Image(:,:,k);
    Image(:,:,k) = flipud(I);
end

%% Output image

that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', 'flipud');
