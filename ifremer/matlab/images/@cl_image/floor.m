% Nearest integers towards -Inf of image
%
% Syntax
%   b = floor(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = repmat(-pi:0.1:pi, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = floor(a);
%   imagesc(b);
%
% See also cl_image/round cl_image/ceil Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = floor(this, varargin)
that = process_function_type1(this, @floor, varargin{:});
