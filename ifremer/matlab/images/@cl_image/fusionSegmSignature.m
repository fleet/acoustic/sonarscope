% Fusion de signatures texturales
% Syntax
%   this = fusionSegmSignature(this, ...) 
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    : 
%
% Examples 
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = fusionSegmSignature(this, sub, nomTexture)

str1 = 'Pas de textures � fusionner';
str2 = 'No textures to collapse';
if length(this.Texture) < 2
    my_warndlg(Lang(str1,str2), 1);
    return
end

if length(sub) < 2
    my_warndlg(Lang(str1,str2), 1);
    return
end

TextureOut = this.Texture(sub(1));
for k=2:length(sub)
    TextureIn = this.Texture(sub(k));
    if ~isequal(TextureIn.cliques, TextureOut.cliques)
        str1 = sprintf('Les cliques ne sont pas identiques : %s / %s pour les textures 1 et %d', num2strCode(TextureOut.cliques), num2strCode(TextureIn.cliques), k);
        str2 = sprintf('The cliques are not identical : %s / %s for textures 1 and %d', num2strCode(TextureOut.cliques), num2strCode(TextureIn.cliques), k);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    if TextureOut.binsLayerAngle ~= TextureIn.binsLayerAngle
        str1 = sprintf('Le conditionnement n''est pas identique pour les textures 1 et %d', k);
        str2 = sprintf('The conditionning is not identical for textures 1 and %d', k);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    for i=1:length(TextureIn.CmatModeleAngle)
        if ~isempty(TextureIn.CmatModeleAngle{i})
            alreadyExist = strcmp(TextureIn.nomCourbe{i}, TextureOut.nomCourbe);
            if any(alreadyExist)
                str1 = sprintf('La texture %s / %s est d�j� d�finie.', TextureIn.nomTexture, TextureIn.nomCourbe{i});
                str2 = sprintf('Texture %s / %s is already defined.', TextureIn.nomTexture, TextureIn.nomCourbe{i});
                my_warndlg(Lang(str1,str2), 1);
                continue
            end
            
            TextureOut.nomCourbe{end+1}       = TextureIn.nomCourbe{i};
            TextureOut.commentaire{end+1}     = TextureIn.commentaire{i};
            TextureOut.CmatModeleAngle{end+1} = TextureIn.CmatModeleAngle{i};
            TextureOut.Coul(end+1,:)          = TextureIn.Coul(i,:);
        end
    end
end
TextureOut.nomTexture = nomTexture;
this.Texture = [this.Texture TextureOut];

%{
              binsImage: [1x1 struct]
    DataType_LayerAngle: []
         DataType_Image: 1
%}
