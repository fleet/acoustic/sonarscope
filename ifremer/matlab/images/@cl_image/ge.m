% Logic operator "greater or equal : >="
%
% Syntax
%  b = a >= Val
%  b = ge(a, Val)
%
% Input Arguments
%   a   : Instance(s) of cl_image
%   Val : Threshold
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%     a = cl_image('Image', Lena);
%     imagesc(a);
%   b = ge(a, 100);
%     imagesc(b);
%   b = a >= 100;
%     imagesc(b);
%   b = ge(a, 100, 'subx', 20:200, 'suby', 20:40);
%     imagesc(b);
%
% See also cl_image/gt Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = ge(this, Val, varargin)
that = process_function_type2(this, @ge, Val, varargin{:});
