% Donne un aspect g�ographique de l'image en se basant sur la Mapping Toolbox.
%
% Syntax
%   flag = geoShow(this, varargin)
%
% Examples 
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = import(cl_image, nomFic);
%   flag = geoShow(a);
%
% See also Authors
% Authors : JMA
%------------------------------------------------------------------------------
function flag = geoShow(this, varargin)

flag = 0; %#ok<NASGU>

%% On 'naffiche cette forme d'image que des donn�es G�od�tiques.
% A voir si ont peut projeter afficher toute forme project�e avec la projection ad hoc.
if this.GeometryType == 3
    Z      = get(this, 'Image');
    lat    = get(this, 'Y');
    lon    = get(this, 'X');
    lonLim = this.XLim;
    latLim = this.YLim;
else
    strWrn = Lang('Image non p�vue pour cet affichage', 'This image is not adapted to this display format');
    my_warndlg(strWrn, 'Display Error', 1);
end

%% Affichage bas� sur l'utilisation de la Mapping Toolbox

figure
worldmap(latLim, lonLim)
[X, Y] = meshgrid(lon, lat);
geoshow(Y, X, Z, 'DisplayType','texturemap')
demcmap(Z)


flag = 1;
