function [flag, a] = geoswathComputeBathy(this, InstallationParameters, varargin)

ProfilCelerite = [];
[flag, a] = sonar_range2depth_GeoswathSep2016(this, ProfilCelerite, InstallationParameters, varargin{:});
if ~flag
    return
end
