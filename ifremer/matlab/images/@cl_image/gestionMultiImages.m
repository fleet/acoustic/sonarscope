% Asks to the user if he wants to process synchonized images. The question
% is asked only if there are synchrinized images and if flagImageEntiere is
% set to 1.
%
% Syntax
%   [flag, indLayers, subx, suby] = gestionMultiImages(this, indImage, flagImageEntiere, subx, suby, ...)
%
% Input Arguments
%   this             : List of images
%   indImage         : Index of the current image in the list
%   flagImageEntiere : 1=Processing of whole image is askes, 0=otherwise
%   subx             : Sub-sampling in abscissa
%   suby             : Sub-sampling in ordinates
%
% Name-Value Pair Arguments
%   See listeLayersSynchronises
%
% Name-only Arguments
%   See listeLayersSynchronises
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   indLayers : List of the selected images
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%
% Remarks : subx and suby are set to [] if multi-images is selected
%
% Examples
%   See callback_compute
%
% See also listeLayersSynchronises Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, indLayers, subx, suby] = gestionMultiImages(this, indImage, flagImageEntiere, subx, suby, varargin)

if flagImageEntiere
    [flag, indLayers, nomsLayers, DataTypes, GeometryType] = listeLayersSynchronises(this, indImage, varargin{:});
    if ~flag
        return
    end

    switch length(indLayers)
        case 0
            indLayers = indImage;
            %         case 1
            %             indLayers = indImage;
        otherwise
            % TODO : peut-�tre poser la question avant de pr�senter la
            % fen�tre de s�lection des images ?

            if get_LevelQuestion >= 3
                nomsLayers = [{'ImageName'}; nomsLayers(:)];
                nomsLayers(:,2) = [{'DataType'};     cl_image.strDataType(DataTypes)];
                nomsLayers(:,3) = [{'GeometryType'}; cl_image.strGeometryType(GeometryType)];

                str1 = 'Il existe des images synchronis�es � l''image courante, il vous est possible de r�aliser le m�me traitement sur ces images.OK sans s�lectionner aucune image ou Cancel pour n''appliquer le traitement que � l''image courante.';
                str2 = 'There are some images synchronised with the current one. You can apply the same processing on some of them. Ok without selecting any of them or cancel to process only the current one. ';
                [flag, idxChoix, strOut] = displaySummary(nomsLayers, 'QueryMode', 'Summary', 'Comments', Lang(str1,str2), 'flagSelectLayer', find(indLayers == indImage));
                if ~flag
                    % Bouton Cancel
                    return
                end
                if isempty(strOut) || isempty(idxChoix)
                    % Dans ce cas, on sort avec l'indice courant.
                    flag      = 1;
                    indLayers = indImage;
                    return
                end
                % R�cup�ration des indices de tri (non exploit� pour l'instant).
                str       = strOut(idxChoix);
                [C,ia,ib] = intersect(nomsLayers(:,1),str(:,1),'stable'); %#ok<ASGLU>
                %                 indLayers = [indImage indLayers(ia-1)];
                indLayers = indLayers(ia-1);
                if isempty(indLayers)
                    return
                end
            else
                indLayers = indImage;
            end
    end
    subx = [];
    suby = [];
else
    indLayers = indImage;
end

flag = 1;
