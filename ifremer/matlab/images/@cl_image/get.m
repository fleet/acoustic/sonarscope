% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_image
%
% Name-Value Pair Arguments
%  Cf. cl_image
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passees en argument
%
% Examples
%   a = cl_image('Image', Lena)
%
%   Image = get(a, 'Image');
%   [x, y] = get(a, 'x', 'y');
%
% See also cl_image cl_image/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    nomVar = varargin{k};
    switch nomVar
        case 'Writable'
            my_breakpoint % Remplacer par obj.Writable
            if isa(this.Image, 'cl_memmapfile')
                varargout{end+1} = get(this.Image,  'Writable'); 
            else
                varargout{end+1} = 1; 
            end

        case {'Titre'; 'ImageName'} % Titre et ImageName conserv�s pour l'instant mais en principe plus utiles
            my_breakpoint % Remplacer par obj.Name;
            varargout{end+1} = this.Name;

        case 'Name'
            varargout{end+1} = this.Name;

        case 'CLim'
            varargout{end+1} = this.CLim;
            
        case 'nbRows'
            % TODO : remplacer par instance.nbRows;
            % my_breakpoint
            varargout{end+1} = this.nbRows; 
        case {'x'; 'X'}
            varargout{end+1} = this.x; 
        case {'y'; 'Y'}
            varargout{end+1} = this.y; 
            
        case 'XStep'
            varargout{end+1} = abs(mean(diff(this.x))); 
        case 'YStep'
            varargout{end+1} = abs(mean(diff(this.y))); 
            
        case 'XStepMetric'
            varargout{end+1} = this.XStepMetric;
        case 'YStepMetric'
            varargout{end+1} = this.YStepMetric;
            
        case 'DataType'
            varargout{end+1} = this.DataType; 
        case 'strMaskBits'
            varargout{end+1} = this.strMaskBits; 
        case 'GeometryType'
            varargout{end+1} = this.GeometryType; 
        case 'Unit'
            varargout{end+1} = this.Unit; 
        case 'nbSlides'
            varargout{end+1} = this.nbSlides; 
        case 'YLabel'
            varargout{end+1} = this.YLabel; 
        case 'XLabel'
            varargout{end+1} = this.XLabel; 
        case 'YUnit'
            varargout{end+1} = this.YUnit; 
        case 'XUnit'
            varargout{end+1} = this.XUnit; 
        case 'Azimuth'
            varargout{end+1} = this.Azimuth; 
        case 'VertExag'
            varargout{end+1} = this.VertExag; 
        case 'ImageType'
            varargout{end+1} = this.ImageType; 
        case 'VertExagAuto'
            varargout{end+1} = this.VertExagAuto; 
        case 'strImageType'
            varargout{end+1} = this.strImageType; 
        case 'strGeometryType'
            varargout{end+1} = this.strGeometryType; 
        case 'Memory'
            varargout{end+1} = where(this); 
        case 'ClassName'
            varargout{end+1} = class(this.Image(1)); 
        case 'InitialFileFormat'
            varargout{end+1} = this.InitialFileFormat; 
        case 'Comments'
            varargout{end+1} = this.Comments; 
        case 'Image'
            [subx, suby, varargin] = getSubxSuby(this, varargin);
            varargout{end+1} = this.Image(suby,subx,:);
        case 'Video'
            varargout{end+1} = this.Video; 
        case 'strVideo'
            varargout{end+1} = this.strVideo; 
        case 'YDir'
            varargout{end+1} = this.YDir; 
        case 'XDir'
            varargout{end+1} = this.XDir; 
        case 'strYDir'
            varargout{end+1} = this.strYDir; 
        case 'strXDir'
            varargout{end+1} = this.strXDir; 
        case 'XRange'
            varargout{end+1} = this.XRange;
        case 'XLim'
            varargout{end+1} = this.XLim; 
        case 'YRange'
            varargout{end+1} = this.YRange;
        case 'YLim'
            varargout{end+1} = this.YLim; 
%         case 'CoulIndex'
%             varargout{end+1} = this.CoulIndex; 
        case 'ColormapIndex'
            varargout{end+1} = this.ColormapIndex; 
        case 'strColormapIndex'
            varargout{end+1} = this.strColormapIndex; 
        case 'SpectralStatus'
            varargout{end+1} = this.SpectralStatus; 
        case 'strSpectralStatus'
            varargout{end+1} = this.strSpectralStatus; 
        case 'Colormap'
            varargout{end+1} = this.Colormap; 
        case 'ColormapCustom'
            varargout{end+1} = this.ColormapCustom; 
        case 'HistoCentralClasses'
            varargout{end+1} = this.HistoCentralClasses; 
        case 'HistoValues'
            varargout{end+1} = this.HistoValues; 
        case 'StatValues'
            varargout{end+1} = this.StatValues; %#ok<*AGROW>
        case 'StatSummary'
            varargout{end+1} = stats2str(this.StatValues); 
        case 'StatValues.Sampling'
            varargout{end+1} = this.StatValues.Sampling; 
        case 'TagSynchroX'
            varargout{end+1} = this.TagSynchroX; 
        case 'TagSynchroY'
            varargout{end+1} = this.TagSynchroY; 
        case 'TagSynchroContrast'
            varargout{end+1} = this.TagSynchroContrast; 
        case 'ContourValues'
            varargout{end+1} = this.ContourValues; 
        case 'ValNaN'
            varargout{end+1} = this.ValNaN; 
        case 'strDataType'
            varargout{end+1} = cl_image.strDataType; 
        case 'InitialFileName'
            varargout{end+1} = this.InitialFileName; 
        case 'NuIdent'
            varargout{end+1} = this.NuIdent; 
        case 'NuIdentParent'
            varargout{end+1} = this.NuIdentParent; 
        case 'InitialImageName'
            varargout{end+1} = this.InitialImageName; 
        case 'History'
            varargout{end+1} = this.History; 

        case {'SonarDescription'; 'SonarDesciption'} % Operation de maintenance � faire pour �liminer tous les SonarDesciption
            if isempty(this.Sonar)
                varargout{end+1} = [];
            else
                varargout{end+1} = this.Sonar.Desciption; 
            end

        case 'Sonar'
            varargout{end+1} = this.Sonar; 
        case 'SampleBeamData'
            if isfield(this.Sonar, 'SampleBeamData')
                varargout{end+1} = this.Sonar.SampleBeamData;
            else
                varargout{end+1} = [];
            end
        case 'SonarRawDataResol'
            varargout{end+1} = this.Sonar.RawDataResol; 
        case 'SonarResolutionD'
            varargout{end+1} = this.Sonar.ResolutionD; 
        case 'SonarResolutionX'
            varargout{end+1} = this.Sonar.ResolutionX; 
            
        case 'SonarBathyCel'
            varargout{end+1} = this.Sonar.BathyCel;
        case 'SonarBathyCel_Z'
            varargout{end+1} = this.Sonar.BathyCel.Z; 
        case 'SonarBathyCel_T'
            varargout{end+1} = this.Sonar.BathyCel.T; 
        case 'SonarBathyCel_S'
            varargout{end+1} = this.Sonar.BathyCel.S; 
        case 'SonarBathyCel_C'
            varargout{end+1} = this.Sonar.BathyCel.C; 
        case {'Time'; 'SonarTime'}
            varargout{end+1} = this.Sonar.Time; 
        case {'Datetime'; 'SonarDatetime'}
            varargout{end+1} = datetime(this.Sonar.Time.timeMat, 'ConvertFrom', 'datenum');
        case {'PingNumber';'SonarPingNumber'}
            varargout{end+1} = this.Sonar.PingNumber; 
        case {'Immersion'; 'SonarImmersion'}
            varargout{end+1} = this.Sonar.Immersion; 
        case {'Height'; 'SonarHeight'}
            varargout{end+1} = this.Sonar.Height; 
        case {'CableOut';'SonarCableOut'}
            varargout{end+1} = this.Sonar.CableOut; 
        case {'Speed'; 'SonarSpeed'}
            varargout{end+1} = this.Sonar.Speed; 
        case {'Heading'; 'SonarHeading'}
            varargout{end+1} = this.Sonar.Heading; 
        case {'Roll'; 'SonarRoll'}
            varargout{end+1} = this.Sonar.Roll; 
        case {'Pitch'; 'SonarPitch'}
            varargout{end+1} = this.Sonar.Pitch; 
        case {'TxAlongAngle';'SonarTxAlongAngle'}
            varargout{end+1} = this.Sonar.TxAlongAngle; 
        case {'TxAcrossAngle'; 'SonarTxAcrossAngle'}
            varargout{end+1} = this.Sonar.TxAcrossAngle; 
        case {'Yaw';'SonarYaw'}
            varargout{end+1} = this.Sonar.Yaw; 
        case {'SurfaceSoundSpeed'; 'SonarSurfaceSoundSpeed'}
            varargout{end+1} = this.Sonar.SurfaceSoundSpeed; 
        case {'FishLatitude'; 'SonarFishLatitude'}
            varargout{end+1} = this.Sonar.FishLatitude; 
        case {'FishLongitude'; 'SonarFishLongitude'}
            varargout{end+1} = this.Sonar.FishLongitude; 
        case 'ShipLatitude'
            varargout{end+1} = this.Sonar.ShipLatitude; 
        case 'ShipLongitude'
            varargout{end+1} = this.Sonar.ShipLongitude; 
        case {'PortMode_1'; 'SonarPortMode_1'}
            varargout{end+1} = this.Sonar.PortMode_1; 
            
%         case {'EM2040Mode2'; 'SonarEM2040Mode2'}
%             varargout{end+1} = this.Sonar.EM2040Mode2; 
%         case {'EM2040Mode3'; 'SonarEM2040Mode3'}
%             varargout{end+1} = this.Sonar.EM2040Mode3; 
            
        case 'PortMode_2'
            varargout{end+1} = this.Sonar.PortMode_2; 
        case 'StarMode_1'
            varargout{end+1} = this.Sonar.StarMode_1; 
        case 'StarMode_2'
            varargout{end+1} = this.Sonar.StarMode_2; 
        case {'Heave';'SonarHeave'}
            varargout{end+1} = this.Sonar.Heave; 
        case {'TrueHeave';'SonarTrueHeave'}
            varargout{end+1} = this.Sonar.TrueHeave; 
        case 'Tide'
            varargout{end+1} = this.Sonar.Tide; 
        case 'Draught'
            varargout{end+1} = this.Sonar.Draught; 
        case {'PingCounter'; 'SonarPingCounter'}
            varargout{end+1} = this.Sonar.PingCounter; 
        case {'SampleFrequency'; 'SonarSampleFrequency'}
            varargout{end+1} = this.Sonar.SampleFrequency; 
        case 'SonarFrequency'
            varargout{end+1} = this.Sonar.SonarFrequency; 
        case {'Interlacing'; 'SonarInterlacing'}
            varargout{end+1} = this.Sonar.Interlacing;
            
        case {'PresenceWC'; 'SonarPresenceWC'}
            varargout{end+1} = this.Sonar.PresenceWC; 
        case {'PresenceFM'; 'SonarPresenceFM'}
            varargout{end+1} = this.Sonar.PresenceFM; 
        case {'DistPings'; 'SonarDistPings'}
            varargout{end+1} = this.Sonar.DistPings;
        case {'NbSwaths'; 'SonarNbSwaths'}
            varargout{end+1} = this.Sonar.NbSwaths;
            
%         case 'AbsorptionCoeff_RT'
%             varargout{end+1} = this.Sonar.AbsorptionCoeff_RT;
        case 'AbsorptionCoeff_SSc'
            varargout{end+1} = this.Sonar.AbsorptionCoeff_SSc;
            
        case {'BSN'; 'SonarBSN'}
            varargout{end+1} = this.Sonar.BSN(:,:);
        case {'BSO'; 'SonarBSO'}
            varargout{end+1} = this.Sonar.BSO(:,:);
        case {'BSN-BSO'; 'SonarBSN-BSO'}
            varargout{end+1} = this.Sonar.BSN(:,:) - this.Sonar.BSO(:,:);
        case {'TVGN'; 'SonarTVGN'; 'Rn'; 'SonarRn'}
            varargout{end+1} = this.Sonar.TVGN(:,:);
        case {'TVGCrossOver'; 'SonarTVGCrossOver'}
            varargout{end+1} = this.Sonar.TVGCrossOver(:,:);
        case {'TxBeamWidth'; 'SonarTxBeamWidth'}
            varargout{end+1} = this.Sonar.TxBeamWidth(:,:);
        case {'ResolAcrossSample'; 'SonarResolAcrossSample'}
            varargout{end+1} = this.Sonar.ResolAcrossSample(:,:);

        case 'strSonarTVG_etat'
            varargout{end+1} = this.Sonar.TVG.strEtat; 
        case 'SonarTVG_etat'
            varargout{end+1} = this.Sonar.TVG.etat; 
        case 'strSonarTVG_origine'
            varargout{end+1} = this.Sonar.TVG.strOrigine; 
        case 'SonarTVG_origine'
            varargout{end+1} = this.Sonar.TVG.origine; 
        case 'SonarTVG_ConstructTypeCompens'
            varargout{end+1} = this.Sonar.TVG.ConstructTypeCompens; 
        case 'SonarTVG_ConstructAlpha'
            varargout{end+1} = this.Sonar.TVG.ConstructAlpha; 
        case 'SonarTVG_ConstructConstante'
            varargout{end+1} = this.Sonar.TVG.ConstructConstante; 
        case 'SonarTVG_ConstructCoefDiverg'
            varargout{end+1} = this.Sonar.TVG.ConstructCoefDiverg; 
        case 'SonarTVG_ConstructTable'
            varargout{end+1} = this.Sonar.TVG.ConstructTable; 
        case 'SonarTVG_IfremerAlpha'
            varargout{end+1} = this.Sonar.TVG.IfremerAlpha; 
        case 'SonarTVG_IfremerConstante'
            varargout{end+1} = this.Sonar.TVG.IfremerConstante; 
        case 'SonarTVG_IfremerCoefDiverg'
            varargout{end+1} = this.Sonar.TVG.IfremerCoefDiverg; 

        case 'strSonarDiagEmi_etat'
            varargout{end+1} = this.Sonar.DiagEmi.strEtat; 
        case 'SonarDiagEmi_etat'
            varargout{end+1} = this.Sonar.DiagEmi.etat; 
        case 'strSonarDiagEmi_origine'
            varargout{end+1} = this.Sonar.DiagEmi.strOrigine; 
        case 'SonarDiagEmi_origine'
            varargout{end+1} = this.Sonar.DiagEmi.origine; 
        case 'SonarDiagEmi_ConstructTypeCompens'
            varargout{end+1} = this.Sonar.DiagEmi.ConstructTypeCompens; 
        case 'SonarDiagEmi_IfremerTypeCompens'
            varargout{end+1} = this.Sonar.DiagEmi.IfremerTypeCompens; 


        case 'strSonarDiagRec_etat'
            varargout{end+1} = this.Sonar.DiagRec.strEtat; 
        case 'SonarDiagRec_etat'
            varargout{end+1} = this.Sonar.DiagRec.etat; 
        case 'strSonarDiagRec_origine'
            varargout{end+1} = this.Sonar.DiagRec.strOrigine; 
        case 'SonarDiagRec_origine'
            varargout{end+1} = this.Sonar.DiagRec.origine; 
        case 'SonarDiagRec_ConstructTypeCompens'
            varargout{end+1} = this.Sonar.DiagRec.ConstructTypeCompens; 
        case 'SonarDiagRec_IfremerTypeCompens'
            varargout{end+1} = this.Sonar.DiagRec.IfremerTypeCompens; 


        case 'strSonarAireInso_etat'
            varargout{end+1} = this.Sonar.AireInso.strEtat; 
        case 'SonarAireInso_etat'
            varargout{end+1} = this.Sonar.AireInso.etat; 
        case 'strSonarAireInso_origine'
            varargout{end+1} = this.Sonar.AireInso.strOrigine; 
        case 'SonarAireInso_origine'
            varargout{end+1} = this.Sonar.AireInso.origine; 



        case 'strSonarBS_etat'
            varargout{end+1} = this.Sonar.BS.strEtat; 
        case 'SonarBS_etat'
            varargout{end+1} = this.Sonar.BS.etat; 
        case 'strSonarBS_origine'
            varargout{end+1} = this.Sonar.BS.strOrigine; 
        case 'SonarBS_origine'
            varargout{end+1} = this.Sonar.BS.origine; 
        case 'strSonarBS_origineBelleImage'
            varargout{end+1} = this.Sonar.BS.strOrigineBelleImage; 
        case 'SonarBS_origineBelleImage'
            varargout{end+1} = this.Sonar.BS.origineBelleImage; 
        case 'strSonarBS_origineFullCompens'
            varargout{end+1} = this.Sonar.BS.strOrigineFullCompens; 
        case 'SonarBS_origineFullCompens'
            varargout{end+1} = this.Sonar.BS.origineFullCompens; 
        case 'SonarBS_IfremerCompensModele'
            varargout{end+1} = this.Sonar.BS.IfremerCompensModele; 
        case 'SonarBS_IfremerCompensTable'
            varargout{end+1} = this.Sonar.BS.IfremerCompensTable; 
        case 'SonarBS_LambertCorrection'
            if isfield(this.Sonar.BS, 'LambertCorrection')
                varargout{end+1} = this.Sonar.BS.LambertCorrection;
            else
                varargout{end+1} = [];
            end
        case 'SonarBS_BSLurtonParameters'
            if isfield(this.Sonar.BS, 'BSLurtonParameters')
                varargout{end+1} = this.Sonar.BS.BSLurtonParameters;
            else
                varargout{end+1} = [];
            end

        case 'SonarNE_etat'
            varargout{end+1} = this.Sonar.NE.etat; 


        case 'RollUsedForEmission'
            varargout{end+1} = this.Sonar.RollUsedForEmission; 

        case 'Carto'
            varargout{end+1} = this.Carto; 
            
        case 'CourbesStatistiques'
            varargout{end+1} = this.CourbesStatistiques; 
            
        case 'RegionOfInterest'
            varargout{end+1} = this.RegionOfInterest; 
            
        case 'SymetrieColormap'
            varargout{end+1} = this.SymetrieColormap;
            
        case 'SonarShip'
            varargout{end+1} = this.Sonar.Ship;
            
        case 'MeanValueOfCurrentExtent'
            [subx, suby, varargin] = getSubxSuby(this, varargin);
            X = this.Image(suby,subx,:);
            varargout{end+1} = mean(X, 2, 'omitnan');
            
        otherwise
            signal = this.SignalsVert.getSignalByTag(nomVar);
            [flag, val, ~] = signal.getValueMatrix();
            if flag
                varargout{end+1} = val;
                return
            end
            
            str = sprintf('cl_image/get : %s non pris en compte', nomVar);
            my_warndlg(str, 0);
            varargout{1} = [];
    end
end

