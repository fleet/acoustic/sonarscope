function this = getCorners(this, varargin)

nbImages = length(this);
str1 = 'Traitement Watershed en cours';
str2 = 'Processing Watersheg';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    that(:,k) = getCorners_unitaire(this(k), varargin{:}); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')
this = that(:);


function this = getCorners_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

identMask = cl_image.indDataType('Mask');
flag = testSignature(this, 'DataType', identMask, 'ClassName', 'uint8');
if ~flag
    return
end

%% Extraction des courbes

BW = this.Image(suby,subx);
Gap_size = 2;
EP = 0;
[curve, curve_start, curve_end, curve_mode, curve_num, TJ] = BW2Curves(BW, Gap_size);  % Extract curves
%{
FigUtils.createSScFigure; imshow(~BW); axis on; hold on; axis xy;
PlotUtils.createSScPlot(TJ(:,2), TJ(:,1), 'ob', 'MarkerFaceColor', 'b', 'MarkerSize', 10);
title('T Junctions detected by BW2Curves');
%}

%% D�tection des Corners

[index, S, curveAL, IND, c1] = Curves2Index(curve, curve_mode, curve_num); % Detect corners
[corner_out, c2]             = Index2Corners(curve, curve_mode, curve_num, index, c1, S, curveAL, IND); % localize corners
[corner_final, c3]           = Refine_TJunctions(corner_out, TJ, c2, curve, curve_num, curve_start, curve_end, curve_mode, EP);
% FigUtils.createSScFigure; plot(c3); grid on;

%% Affichage de l'image et des Corners

FigUtils.createSScFigure; imshow(~BW, 'XData', this.x(subx), 'YData', this.y(suby)); axis on; hold on; axis xy;
xc = interp1(1:length(subx), this.x(subx), corner_final(:,2));
yc = interp1(1:length(suby), this.y(suby), corner_final(:,1));
PlotUtils.createSScPlot(xc, yc, 'ob', 'MarkerFaceColor', 'b', 'MarkerSize', 10);
title('Detected corners on the edge image');

% sub = (c3 < 0.75);
% FigUtils.createSScFigure; imshow(~BW); axis on; hold on;
% PlotUtils.createSScPlot(corner_final(sub,2), corner_final(sub,1), 'og', 'MarkerFaceColor', 'g', 'MarkerSize', 10);
% PlotUtils.createSScPlot(corner_final(~sub,2), corner_final(~sub,1), 'ob', 'MarkerFaceColor', 'b', 'MarkerSize', 10);
% title('Detected corners on the edge image');

% TODO : d�velopper une menu SonarScope dans lequel on peut faire un export
% des markers



