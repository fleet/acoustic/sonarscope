% Operation de centrage (moyenne=0) et reduction (sigma = 1) d'une image
%
% Syntax
%   [Carto, flag] = getDefinitionCarto(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   Carto : Instance de cl_carto
%   flag  : 1 (successfull) or 0 (failure)
%
% Examples
%
% See also cl_image cl_carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Carto, flag] = getDefinitionCarto(this, varargin)

C0 = cl_carto([]);

[varargin, NoGeodetic]        = getPropertyValue(varargin, 'NoGeodetic',        0);
[varargin, nomDir]            = getPropertyValue(varargin, 'nomDirSave',        []);
[varargin, BypassCartoFolder] = getPropertyValue(varargin, 'BypassCartoFolder', 0);
[varargin, CartoAuto]         = getPropertyValue(varargin, 'CartoAuto',         1); %#ok<ASGLU>

if isempty(nomDir)
    if exist(this.InitialFileName, 'file')
        nomDir = fileparts(this.InitialFileName);
        [flag, Carto] = test_ExistenceCarto_Folder(nomDir);
        if flag && ~BypassCartoFolder % Modifi� le 27/06/2011
            return
        end
    else
        nomDir = my_tempdir;
    end
else
    if exist(this.InitialFileName, 'file')
        if isempty(nomDir)
            nomDir = fileparts(this.InitialFileName);
        end
        [flag, Carto, nomFicCartoDefault] = test_ExistenceCarto_Folder(nomDir);
        if flag && ~BypassCartoFolder
            str1 = sprintf('Le fichier "%s" est interpr�t� directement', nomFicCartoDefault);
            str2 = sprintf('File "%s" is used by default.', nomFicCartoDefault);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
            return
        end
    elseif ~isempty(nomDir)
        [flag, Carto, nomFicCartoDefault] = test_ExistenceCarto_Folder(nomDir);
        if flag && ~BypassCartoFolder
            str1 = sprintf('Le fichier "%s" est interpr�t� directement', nomFicCartoDefault);
            str2 = sprintf('File "%s" is used by default.', nomFicCartoDefault);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
            return
        end
    end
end

[IsAutoUTMPossible, CartoAutoUTM, codeUTM] = testIfAutoUTMIsPossible(this);

nomFic = fullfile(nomDir, 'Carto_Directory.def');

if IsAutoUTMPossible
    if CartoAuto
        Carto = CartoAutoUTM;
        flag = 1;
        return
    else
        str1 = 'D�finition des param�tres cartographiques';
        str2 = 'Cartographic parametres ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), ...
            {Lang('Manuellement', 'Manual')
            Lang('Importation depuis un fichier', 'Import')
            codeUTM}, 'SelectionMode', 'Single');
        if ~flag
            Carto = [];
            return
        end
        if rep == 3
            Carto = CartoAutoUTM;
            flag = saveCarto(Carto, nomDir, 'Carto_Directory.def');
            
            if ~flag
                str1 = sprintf('Le fichier "%s" n''a pas pu �tre cr��, v�rifiez les droits en �criture dans le r�pertoire.', nomDir);
                str2 = sprintf('"%s" could not be written, check write permissions on the directory please.', nomDir);
                my_warndlg(Lang(str1,str2), 1);
                flag = 1;
                %             Carto = [];
            end
            return
        end
    end
else
    str1 = 'D�finition des param�tres cartographiques';
    str2 = 'Cartographic parametres ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), ...
        Lang('Manuellement', 'Manual'), Lang('Importation depuis un fichier', 'Import'));
    if ~flag
        Carto = [];
        return
    end
end
if rep == 2
    liste = listeFicOnDir(nomDir, '.def');
    if isempty(liste)
        nomDirUp = fileparts(nomDir);
        liste = listeFicOnDir(nomDirUp, '.def');
        if ~isempty(liste)
            nomFic = fullfile(nomDirUp, '*.def');
        end
    end
    if length(liste) == 1
        Filtre = liste{1};
    else
        Filtre = '';
    end
    str1 = 'Nom du fichier de cartographie (un fichier se terminant par "_Folder.def" sera pris en compte automatiquement par la suite pour tous les fichiers de ce dossier) ';
    str2 = 'Give a cartographic file name (a file name ended by "_Folder.def" will be took in account automaticaly for the files of this folder';
    [flag, nomFic] = my_uigetfile(nomFic, Lang(str1,str2), Filtre);
    if flag
        Carto = import_xml(C0, nomFic);
        if isempty(Carto)
            
            % Au cas o� le fichier soit � l'ancienne m�thode
            
            Carto = import(C0, nomFic);
            flag = export_xml(Carto, nomFic);
            if flag
                %                 delete(nomFic)
            end
        end
    else
        rep = 1;
    end
end

if rep == 1
    if this.GeometryType == cl_image.indGeometryType('LatLong')
        LatMean = mean(this.YLim, 'omitnan');
        LonMean = mean(this.XLim, 'omitnan');
    else
        Lon = get(this, 'FishLongitude');
        Lat = get(this, 'FishLatitude');
        if ~isempty(Lon) && ~isempty(Lat)
            LatMean = mean(Lat(:), 'omitnan');
            LonMean = mean(Lon(:), 'omitnan');
        else
            if this.GeometryType == cl_image.indGeometryType('GeoYX')
                Carto = this.Carto;
                ProjectionType = get(Carto, 'Projection.Type');
                if ProjectionType == 2 % Mercator
                    LonMean = get(Carto, 'Projection.Mercator.long_merid_orig');
                    LatMean = get(Carto, 'Projection.Mercator.lat_ech_cons');
                elseif ProjectionType == 3 % UTM
                    LonMean = get(Carto, 'Projection.UTM.Central_meridian');
                    LatMean = 0;
                else
                    LatMean = 0;
                    LonMean = 0;
                    %                     my_warndlg('R�cup�ration de Lat et Lon pas encore fait pour cette projection', 0);
                end
            else
                LatMean = 0;
                LonMean = 0;
            end
        end
    end
    
    Carto = this.Carto;
    if isempty(Carto)
        Carto = C0;
    end
    
    [flag, Carto] = editParams(Carto, round(LatMean), round(LonMean), 'MsgInit', 'NoGeodetic', NoGeodetic);
    if ~flag
        return
    end
    
    %% Sauvergarde des param�tres cartographiques dans un fichier ASCI
    
    flag = saveCarto(Carto, nomDir, 'Carto_Directory.def');
    if ~flag
        return
    end
end

function [IsAutoUTMPossible, Carto, codeUTM] = testIfAutoUTMIsPossible(this)

IsAutoUTMPossible = 0;
Carto             = [];
codeUTM           = [];

Lon = get(this, 'FishLongitude');
Lat = get(this, 'FishLatitude');

if isempty(Lon) || isempty(Lat)
    return
end

sub = find(~isnan(Lon) & ~isnan(Lat));
if isempty(sub)
    return
end

LatMean = mean(Lat(sub));
LonMean = mean(Lon(sub));

Fuseau = floor((LonMean+180)/6 + 1);
if LatMean >= 0
    H = 'N';
else
    H = 'S';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', H);
IsAutoUTMPossible = 1;

codeUTM = [H 'UTM' num2str(Fuseau) ' -> ' num2strCode([Fuseau-1 Fuseau]*6-180)];
