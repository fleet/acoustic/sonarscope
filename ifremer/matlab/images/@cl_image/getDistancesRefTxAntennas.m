function [acrossDistRefTxAntenna, alongDistRefTxAntenna] = getDistancesRefTxAntennas(acrossDistRefShip, alongDistRefShip)

switch acrossDistRefShip.Sonar.Ship.Arrays.Transmit.nb
    case 1
        % TODO : tester avec fichier de l'Atalante
        acrossDistRefTxAntenna = acrossDistRefShip - acrossDistRefShip.Sonar.Ship.Arrays.Transmit.X;
        alongDistRefTxAntenna  = alongDistRefShip  - alongDistRefShip.Sonar.Ship.Arrays.Transmit.Y;
    case 2 % Dual Tx head
        % TODO
        n = acrossDistRefShip.nbColumns;
        ns2 = n / 2;
        subBab = 1:ns2;
        subTri = (ns2+1):n;
        acrossDistRefTxAntenna = acrossDistRefShip;
        alongDistRefTxAntenna  = alongDistRefShip;
        acrossDistRefTxAntenna.Image(:,subBab) = acrossDistRefShip.Image(:,subBab) - acrossDistRefShip.Sonar.Ship.Arrays.Transmit.X(1);
        alongDistRefTxAntenna.Image(:,subBab)  = alongDistRefShip.Image(:,subBab)  - alongDistRefShip.Sonar.Ship.Arrays.Transmit.Y(1);
        acrossDistRefTxAntenna.Image(:,subTri) = acrossDistRefShip.Image(:,subTri) - acrossDistRefShip.Sonar.Ship.Arrays.Transmit.X(2);
        alongDistRefTxAntenna.Image(:,subTri)  = alongDistRefShip.Image(:,subTri)  - alongDistRefShip.Sonar.Ship.Arrays.Transmit.Y(2);
    otherwise
        % TODO : message
end
