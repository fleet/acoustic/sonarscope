function [flag, nomFic, repExport] = getFileNameLinked2ImageName(this, Extension, repExport, varargin)

[varargin, FullName] = getFlag(varargin, 'FullName'); %#ok<ASGLU>

% TODO généraliser pour pouvoir faire :
% [flag, filename] = my_uiputfile({'*.tif'; '*.jpg'; '*.gif'}, Lang('Nom du fichier', 'Give a file name'), filtre);

if FullName
    ImageName = code_ImageName(this);
else
    ImageName = extract_ImageName(this);
end

if iscell(Extension)
    Ext = Extension{1};
else
    Ext = Extension;
end
filtre = fullfile(repExport, [ImageName Ext]);
str1 = 'Nom du fichier';
str2 = 'Give a file name';
[flag, nomFic] = my_uiputfile(Extension, Lang(str1,str2), filtre);
if ~flag
    return
end
repExport = fileparts(nomFic);
flag = 1;
