function [Method_Step1, Method_Step2] = getMethodInterpolation(this)

Method_Step1 = 'linear';
Method_Step2 = [];

switch this.ImageType
    case 3  % Indexee
        Method_Step1 = 'nearest';
    case 4  % Binaire
        Method_Step1 = 'nearest';
end

switch this.DataType
    case cl_image.indDataType('AveragedPtsNb')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('Mask')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('Texture')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('RxTime')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('TxBeamIndex')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('RxBeamIndex')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('RayPathSampleNb')
        if nargout == 1
            Method_Step1 = 'nearest';
        else
            Method_Step2 = @round;
        end
    case cl_image.indDataType('DetectionType')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('RayPathTravelTime')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('KongsbergQualityFactor')
        Method_Step1 = 'nearest';
    case cl_image.indDataType('Frequency')
        Method_Step1 = 'nearest';
end
                       
                        