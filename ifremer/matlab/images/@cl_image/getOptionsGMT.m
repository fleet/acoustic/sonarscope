% Fonctionnalite getOptionsGMT de r�cup�ration des options GMT.
%
% Syntax
%   optGMT = getOptionsGMT(a, Bathy, varargin)
%
% Input Arguments
%   a : instance de clc_image
%
% See also clc_image Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, gmtDlg, repImport] = getOptionsGMT(this, Bathy, repImport, varargin)

gmtDlg = GmtDialog();

% R�cup�ration des options d'impression GMT
Carto = get(this, 'Carto');
if isempty(Carto)
    long_merid_orig = 0;
    YLim = this.YLim;
    lat_ech_cons = round(mean(YLim));
    gmtDlg.longitudeParam.Value = long_merid_orig;
    gmtDlg.latitudeParam.Value  = lat_ech_cons;
else
    gmtDlg.projection = get(Carto, 'Projection.Type') - 1;
    
    switch gmtDlg.projection % ATTENTION +ou-1 ??? CLA_GMTProj
        case 0 % GeoDetic
            Carto = set(Carto, 'Projection.Type', 2);
            long_merid_orig = get(Carto,'Projection.Mercator.long_merid_orig');
            lat_ech_cons    = get(Carto,'Projection.Mercator.lat_ech_cons');
            gmtDlg.longitudeParam.Value = long_merid_orig;
            gmtDlg.latitudeParam.Value  = lat_ech_cons;
            
        case 1  % MERCATOR
            long_merid_orig = get(Carto,'Projection.Mercator.long_merid_orig');
            lat_ech_cons    = get(Carto,'Projection.Mercator.lat_ech_cons');
            gmtDlg.longitudeParam.Value = long_merid_orig;
            gmtDlg.latitudeParam.Value  = lat_ech_cons;
            
        case 2 % UTM
            UTMZone = get(Carto, 'Projection.UTM.Fuseau');
            gmtDlg.utmZone = UTMZone;
            
            UTMHemisphere = get(Carto, 'Projection.UTM.Hemisphere');
            if strcmp(UTMHemisphere, 'N')
                gmtDlg.hemisphere = 1;
            else
                gmtDlg.hemisphere = 2;
            end
            
        case 3 % LAMBERT
            first_paral  = get(Carto,'Projection.Lambert.first_paral');
            second_paral = get(Carto,'Projection.Lambert.first_paral');
            %             long_merid_orig     = get(Carto,'Projection.Lambert.long_merid_orig');
            gmtDlg.firstParallel  = first_paral;
            gmtDlg.secondParallel = second_paral;
            % On doit donner le m�ridien d'origine adapt� � la donn�e (pour
            % le fcontionnement de GMT5).
            long_merid_orig = mean(this.XLim);
            gmtDlg.LongMeridOrig = long_merid_orig;
            
        otherwise % LAMBERT
            
    end
end
gmtDlg.projection   = gmtDlg.projection;
gmtDlg.isPdfDisplay = false;


FlagCountour = false;
% Si l'image ( de type Intensit�) est de type Bathy ou associ� � une bathy.
if ~isempty(Bathy)
    FlagCountour = true;
    StatValues = Bathy.StatValues;
elseif(this.DataType == cl_image.indDataType('Bathymetry') && this.ImageType == 1)
    FlagCountour = true;
    StatValues = this.StatValues;
end

if FlagCountour
    gmtDlg.isContourLines = true;
    % Initialisation des param�tres des trac�s de contours
    Val = compute_graduations([StatValues.Min StatValues.Max]);
    Pas = abs(Val(2) - Val(1));
    gmtDlg.firstIsoBathLevelParam.Value = StatValues.Min;
    gmtDlg.lastIsoBathLevelParam.Value  = StatValues.Max;
    gmtDlg.isoBathStepParam.Value       = Pas;
    gmtDlg.isoBathLabelingParam.Value   = Pas*5;
else
    gmtDlg.isContourLines = false;
end


gmtDlg.repImportNav = repImport;

gmtDlg.openDialog;
if ~gmtDlg.okPressedOut
    flag = 0;
    return
end

rep = gmtDlg.isPlotNav; % logical
if rep == true
    repImport = gmtDlg.repImportNav;
    
    ListeFicNav = gmtDlg.navPathList;
    gmtDlg.navPathList = ListeFicNav;
end

flag = 1;
