function [flag, PatchSampleBeam, PatchDepthAcrossDist] = getPatchEchoIntegrationOverSpecular(A, params_ZoneEI, ALim, BLim)

PatchSampleBeam      = [];
PatchDepthAcrossDist = [];

SeuilBas  = params_ZoneEI.SeuilBas;
SeuilHaut = params_ZoneEI.SeuilHaut;
% DistanceSecutity = params_ZoneEI.DistanceSecutity;

flag = testSignature(A, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

SampleBeamData = get_SonarSampleBeamData(A);
R = SampleBeamData.R1SamplesFiltre;

SampleBeamData.BeamMask(isnan(SampleBeamData.BeamMask)) = 0;
R(SampleBeamData.BeamMask ~= 0) = 0; % Ajout JMA le 17/02/2020 pour prise en compte nettoyage de la bathy

sub0 = find(R <= 1);
sub1 = find(R > 1);

if length(sub1) <= 2 % Si on a moins du 1/20 des sondes en détection alors on passe le ping
% if length(sub1) <= (length(R) / 20) % Si on a moins du 1/20 des sondes en détection alors on passe le ping
    flag = 0;
    return
end

R(sub0) = my_interp1(sub1, R(sub1), sub0, 'nearest', 'extrap');
R0 = min(R); % TODO : utiliser l'estimation BDA ?

if isnan(R0)
    flag = 0;
    return
end

r1 = floor(SeuilBas  * R0);
r2 = max(floor(SeuilHaut * R0), 1);

nbBeams = length(R);
if isempty(ALim)
    subBeams = 1:nbBeams;
else
    AnglesWC = SampleBeamData.RxAnglesEarth_WC;
    subBeams = find((AnglesWC >= ALim(1)) & (AnglesWC <= ALim(2))); % TODO
end
if isempty(subBeams)
    flag = 0;
    return
end
if ~isempty(BLim)
    subBeams(subBeams < BLim(1)) = [];
    subBeams(subBeams > BLim(2)) = [];
end
if isempty(subBeams)
    flag = 0;
    return
end

n = length(subBeams);
XPatch = [subBeams       fliplr(subBeams) subBeams(1)];
YPatch = [repmat(r1,1,n) repmat(r2,1,n)   r1];

PatchSampleBeam.subBeams   = subBeams;
PatchSampleBeam.subSamples = min(r1,r2):max(r1,r2);
PatchSampleBeam.R          = R;
PatchSampleBeam.XPatch     = XPatch;
PatchSampleBeam.YPatch     = YPatch;

if nargout == 1
    PatchDepthAcrossDist = [];
else
    SampleBeamData = get_SonarSampleBeamData(A);
    Teta = SampleBeamData.RxAnglesEarth_WC;
    coef = SampleBeamData.SoundVelocity / (2 * SampleBeamData.SampleRate);
    YPatch = YPatch * coef;
    Angle = Teta(XPatch);
    PatchDepthAcrossDist.XPatch = YPatch .* sind(Angle);
    PatchDepthAcrossDist.YPatch = YPatch .* cosd(Angle);
    PatchDepthAcrossDist.YPatch = YPatch .* cosd(Angle);
end
