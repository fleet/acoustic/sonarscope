% [flag, PatchSampleBeam, PatchDepthAcrossDist] = getSlicesPatchEchoIntegrationOverSpecular(A, params_ZoneEI, ALim, BLim)

function [flag, PatchSampleBeam, PatchDepthAcrossDist] = getSlicesPatchEchoIntegrationOverSpecular(A, params_ZoneEI, ALim, BLim)

flag = 0;

nbSlices = length(params_ZoneEI.Seuils) - 1;
for k=1:nbSlices
    params_ZoneEI.SeuilBas  = params_ZoneEI.Seuils(k);
    params_ZoneEI.SeuilHaut = params_ZoneEI.Seuils(k+1);
    [flag, PatchSampleBeam(k), PatchDepthAcrossDist(k)] = getPatchEchoIntegrationOverSpecular(A, params_ZoneEI, ALim, BLim); %#ok<AGROW> 
end

if flag == 0
    PatchSampleBeam      = struct;
    PatchDepthAcrossDist = struct;
end
