% Cr�ation d'une image sonar correspondant � compensation en cours sur l'image ou nouvelle compensation
% TODO
%
% Syntax
%   [b, flag] = getSonarBSNiveauxImage(a, indImage, subx, suby, ...)
%
% Input Arguments
%   a        : cl_image instances
%   indImage : Index of the reflectivity image
%   subx     : Sub-sampling in abscissa
%   suby     : Sub-sampling in ordinates
%
% Name-Value Pair Arguments
%   SonarBS_origine            : {'1=Belle Image'} | '2=Full compensation'
%   SonarBS_origineBelleImage  : {'1=Lambert'} | '2=Lurton'
%   SonarBS_origineFullCompens : {'1=Analytique'} | '2=Table'
%
% Output Arguments
%   flag : 1:OK, 0=KO
%   b    : Updated instance of cl_image
%
% Examples
%   [flag, b] = TODO
%     imagesc(a)
%     imagesc(b)
%
% See also SonarBSCompensNone Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [a, flag, SonarBS_LambertCorrection] = getSonarBSNiveauxImage(this, indImage, subx, suby, varargin)

a = cl_image.empty;

% SonarBS_etat              = get(this(indImage), 'SonarBS_etat');
SonarBS_origine             = get(this(indImage), 'SonarBS_origine');
SonarBS_origineBelleImage   = get(this(indImage), 'SonarBS_origineBelleImage');
SonarBS_origineFullCompens  = get(this(indImage), 'SonarBS_origineFullCompens');
SonarBS_LambertCorrection   = get(this(indImage), 'SonarBS_LambertCorrection');
SonarBS_BSLurtonParameters  = get(this(indImage), 'SonarBS_BSLurtonParameters');
% SonarBS_IfremerCompensTable = get(this(indImage), 'SonarBS_IfremerCompensTable');

[varargin, SonarBS_origine]             = getPropertyValue(varargin, 'SonarBS_origine',            SonarBS_origine);
[varargin, SonarBS_origineBelleImage]   = getPropertyValue(varargin, 'SonarBS_origineBelleImage',  SonarBS_origineBelleImage);
[varargin, SonarBS_origineFullCompens]  = getPropertyValue(varargin, 'SonarBS_origineFullCompens', SonarBS_origineFullCompens);
[varargin, SonarBS_LambertCorrection]   = getPropertyValue(varargin, 'SonarBS_LambertCorrection',  SonarBS_LambertCorrection);
[varargin, SonarBS_BSLurtonParameters]  = getPropertyValue(varargin, 'SonarBS_BSLurtonParameters', SonarBS_BSLurtonParameters);
[varargin, Bathy]                       = getPropertyValue(varargin, 'Bathy',                      []);
[varargin, Mute]                        = getPropertyValue(varargin, 'Mute',                       0); %#ok<ASGLU>

if (SonarBS_origine == 1) && (SonarBS_origineBelleImage == 1)  % Belle image - Lambert
    if SonarBS_LambertCorrection == 1 % 'R/Rn'
        [flag, indLayerTwoWayTravelTimeInSeconds] = params_LambertCorrectionKM(this, indImage);
        if flag
            [flag, a] = sonar_Lambert_KM(this(indImage), this(indLayerTwoWayTravelTimeInSeconds), ...
                'subx', subx, 'suby', suby);
        end
    else % 'BeamPointingAngle' ou 'IncidenceAngle' % TODO : d�finir angle/vertical !!!
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = params_LambertCorrection(this, indImage, ...
            'LookForBathy', isempty(Bathy));
        if flag
            if isempty(Bathy)
                Bathy = this(indLayerBathy);
            end
            [flag, a] = sonar_BelleImage_Lambert(this(indImage), CasDepth, H, CasDistance, ...
                Bathy, this(indLayerEmission), 'LookForBathy', isempty(Bathy), ...
                'subx', subx, 'suby', suby);
            if ~isempty(indLayerEmission)
                if this(indLayerEmission).DataType == cl_image.indDataType('IncidenceAngle')
                    SonarBS_LambertCorrection = 3;
                end
            end
        end
    end
    
elseif (SonarBS_origine == 1) && (SonarBS_origineBelleImage == 2)  % Belle image - Lurton
    [flag, identCourbe] = selectionCourbesStats(this(indImage), 'Tag', 'BS');
    if ~flag || isempty(identCourbe)
        return
    end
    [flag, indLayerAngleIncidence] = paramsFonctionSonarBS_Image_Lurton(this, indImage, identCourbe);
    if isnan(flag)
        return
    end
    if flag
        [flag, a] = sonar_Image_Lurton(this(indImage),  this(indLayerAngleIncidence), ...
            'subx', subx, 'suby', suby, 'identCourbe', identCourbe);
    end
    
elseif (SonarBS_origine == 2) && (SonarBS_origineFullCompens == 1)   % Full compensation - Analytique
    if isempty(SonarBS_BSLurtonParameters)
        [flag, identCourbe] = selectionCourbesStats(this(indImage), 'Tag', 'BS'); % , 'NoManualSelection', 1);
        if ~flag || isempty(identCourbe)
            return
        end
    else
        identCourbe = [];
    end
    [flag, indLayerAngleIncidence] = paramsFonctionSonarBS_Image_Lurton(this, indImage, identCourbe, 'Mute', Mute);
    if flag
        [flag, a] = sonar_FullCompensationAnalytique(this(indImage), this(indLayerAngleIncidence), ...
            'subx', subx, 'suby', suby, 'identCourbe', identCourbe);
    end
    
elseif (SonarBS_origine == 2) && (SonarBS_origineFullCompens == 2)   % Full compensation - Table
    [flag, identCourbe] = selectionCourbesStats(this(indImage), 'Tag', 'BS');
    if ~flag || isempty(identCourbe)
        return
    end
    [flag, indLayerAngleIncidence] = paramsFonctionSonarBS_Image_Lurton(this, indImage, identCourbe);
    if flag
        a = sonar_FullCompensationTable(this(indImage), this(indLayerAngleIncidence), ...
            'subx', subx, 'suby', suby, 'identCourbe', identCourbe);
    end
    
else
    flag = 0;
    a = cl_image.empty;
    disp('On devrait pas passer par ici en principe')
end
