function [subx, suby, varargin] = getSubxSuby(this, varargin)

try
    [varargin, subx] = getPropertyValue(varargin{:}, 'subx', []);
catch
    [varargin, subx] = getPropertyValue(varargin, 'subx', []);
end
[varargin, suby] = getPropertyValue(varargin,    'suby', []);

if isempty(subx)
    subx = 1:this.nbColumns;
end
if isempty(suby)
    suby = 1:this.nbRows;
end
