function [flag, val, Unit, Legend] = getValSignalIfAny(this, profilYType, varargin)

[varargin, subl] = getPropertyValue(varargin, 'suby', []);
[varargin, val]  = getPropertyValue(varargin, 'val',  []); %#ok<ASGLU>

if isempty(subl)
    subl = 1:this.nbRows; % nbRows
end

Unit   = '';
Legend = [];

%% Profil vertical de l''image

if strcmp(profilYType, 'Image profile') || isequal(profilYType,1)
    flag = 1;
    return
end

%% Signaux verticaux nouvelle génération

signal = this.SignalsVert.getSignalByTag(profilYType);
[flag, val, Unit] = signal.getValueMatrix('subl', subl);
if flag
    try
        Legend = signal.ySample(1).strIndexList;
    catch
        Legend = [];
    end
    return
end



%% Signaux verticaux ancienne génération

switch profilYType
    case {'Mean Value'; 'MeanValueOfCurrentExtent'}
        X = this.Image(subl,:);
        if ~isempty(X)
            val = mean(X, 2, 'omitnan');
            flag = 1;
        end
        
    case 'Time'
        X = get(this, 'Time');
        if ~isempty(X)
            if isa(X, 'cl_time')
                val = X.timeMat;
                val = val(subl);
            else
                val = X(subl);
            end
            flag = 1;
        end
    case 'PingNumber'
        X = get(this, 'PingNumber');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'Immersion'
        X = get(this, 'Immersion');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm';
            flag = 1;
        end
    case 'Height'
        X = get(this, 'Height');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm';
            flag = 1;
        end
    case 'CableOut'
        X = get(this, 'CableOut');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm';
            flag = 1;
        end
    case 'Speed'
        X = get(this, 'Speed');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm/s';
            flag = 1;
        end
    case 'Heading'
        X = get(this, 'Heading');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'Roll'
        X = get(this, 'Roll');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'Pitch'
        X = get(this, 'Pitch');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'Yaw'
        X = get(this, 'Yaw');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'SurfaceSoundSpeed'
        X = get(this, 'SurfaceSoundSpeed');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm/s';
            flag = 1;
        end
    case 'Heave'
        X = get(this, 'Heave');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm';
            flag = 1;
        end
    case 'Tide'
        X = get(this, 'Tide');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm';
            flag = 1;
        end
    case 'Draught'
        X = get(this, 'Draught');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm';
            flag = 1;
        end
    case 'TrueHeave'
        X = get(this, 'TrueHeave');
        if ~isempty(X)
            val = X(subl);
            Unit = 'm';
            flag = 1;
        end
    case 'PingCounter'
        X = get(this, 'PingCounter');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'SampleFrequency'
        X = get(this, 'SampleFrequency');
        if ~isempty(X)
            val = X(subl);
            Unit = 'Hz ou kHz ????';
            flag = 1;
        end
    case 'SonarFrequency'
        X = get(this, 'SonarFrequency');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'FishLatitude'
        X = get(this, 'FishLatitude');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'FishLongitude'
        X = get(this, 'FishLongitude');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'ShipLatitude'
        X = get(this, 'ShipLatitude');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'ShipLongitude'
        X = get(this, 'ShipLongitude');
        if ~isempty(X)
            val = X(subl);
            Unit = 'deg';
            flag = 1;
        end
    case 'PortMode_1'
        X = get(this, 'PortMode_1');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'PortMode_2'
        X = get(this, 'PortMode_2');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'StarMode_1'
        X = get(this, 'StarMode_1');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'StarMode_2'
        X = get(this, 'StarMode_2');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'Interlacing'
        X = get(this, 'SonarInterlacing');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'PresenceWC'
        X = get(this, 'SonarPresenceWC');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'PresenceFM'
        X = get(this, 'PresenceFM');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'DistPings'
        X = get(this, 'DistPings');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'NbSwaths'
        X = get(this, 'NbSwaths');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
        
    case 'BSN'
        X = get(this, 'BSN');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'BSO'
        X = get(this, 'BSO');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'BSN-BSO'
        X = get(this, 'BSN-BSO');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'TVGN'
        X = get(this, 'TVGN');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'TVGCrossOver'
        X = get(this, 'TVGCrossOver');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'TxBeamWidth'
        X = get(this, 'TxBeamWidth');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'ResolAcrossSample'
        X = get(this, 'ResolAcrossSample');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'AbsorptionCoeff_SSc'
        X = get(this, 'AbsorptionCoeff_SSc');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
        
    case 'EM2040Mode2'
        X = get(this, 'EM2040Mode2');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    case 'EM2040Mode3'
        X = get(this, 'EM2040Mode3');
        if ~isempty(X)
            val = X(subl);
            flag = 1;
        end
    otherwise
        flag = 0;
end
