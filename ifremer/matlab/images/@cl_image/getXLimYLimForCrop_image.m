function [XLim, YLim] = getXLimYLimForCrop_image(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, XLimYLim]  = getPropertyValue(varargin, 'XLimYLim',  0);
[varargin, CheckData] = getPropertyValue(varargin, 'CheckData', 1); %#ok<ASGLU>

x = get(this, 'x');
y = get(this, 'y');
if XLimYLim == 1
    XLim = this.XLim;
    subx = find((x >= XLim(1)) & (x <= XLim(2)));
    YLim = this.YLim;
    suby = find((y >= YLim(1)) & (y <= YLim(2)));
end

if CheckData == 1
    val = get_val_ij(this, suby, subx);
    val = sum(val, 3);
    
    if this.DataType == cl_image.indDataType('Mask')
        val(val == 0) = NaN;
    end
    val = ~isnan(val);
    mV = find(sum(val,2));
    mH = find(sum(val,1));
    if (length(mH) < 2) || (length(mV) < 2)
        XLim = [];
        YLim = [];
        return
    end
    XLim = x(subx(mH([1 end])));
    YLim = y(suby(mV([1 end])));
    XLim = [min(XLim) max(XLim)];
    YLim = [min(YLim) max(YLim)];
else
    XLim = x(subx([1 end]));
    YLim = y(suby([1 end]));
    XLim = [min(XLim) max(XLim)];
    YLim = [min(YLim) max(YLim)];
end

