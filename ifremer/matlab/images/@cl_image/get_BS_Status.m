function [flag, str, cstr, Title] = get_BS_Status(this)
    
Title = sprintf('Backscatter status for %s', this.Name);

str  = [];
% cstr = {this.Name};
% cstr{end+1} = ' ';
cstr = {};

identReflectivity = cl_image.indDataType('Reflectivity');

flag = testSignature(this, 'DataType', identReflectivity);
if ~flag
    return
end

%% NE, SH, GT

%{
if this.Sonar.NE.etat == 1
    cstr{end+1} = sprintf('NE : Compensated');
else
    cstr{end+1} = sprintf('NE : Not compensated');
end

if this.Sonar.GT.etat == 1
    cstr{end+1} = sprintf('GT : Compensated');
else
    cstr{end+1} = sprintf('GT : Not compensated');
end

if this.Sonar.SH.etat == 1
    cstr{end+1} = sprintf('SH : Compensated');
else
    cstr{end+1} = sprintf('SH : Not compensated');
end
%}

%% Transmission loss

if this.Sonar.TVG.etat == 1
    switch this.Sonar.TVG.origine
        case 1
            if this.Sonar.TVG.ConstructTypeCompens == 1
                strAlpha = num2str(this.Sonar.TVG.ConstructAlpha, '%.2f');
                cstr{end+1} = sprintf('TL : Compensated, RT, Analytical : PT(R) = %s*log10(R) + 2*Alpha*R + %s', ...
                    num2strCode(this.Sonar.TVG.ConstructCoefDiverg), num2strCode(this.Sonar.TVG.ConstructConstante));
            else
                strAlpha = [];
                cstr{end+1} = sprintf('TL : Compensated, RT, Table : %s', num2strCode(this.Sonar.TVG.ConstructTable));
            end
            
        case 2
            strAlpha = num2str(this.Sonar.TVG.IfremerAlpha, '%.2f');
            cstr{end+1} = sprintf('TL : Compensated, SSc, Analytical : PT(R) = %s*log10(R) + 2*Alpha*R + %s', ...
                num2strCode(this.Sonar.TVG.IfremerCoefDiverg), num2strCode(this.Sonar.TVG.IfremerConstante));
        
        case 3
            strAlpha = num2str(this.Sonar.TVG.IfremerAlpha, '%.2f');
            cstr{end+1} = sprintf('TL : Compensated, User, Analytical : PT(R) = %s*log10(R) + 2*Alpha*R + %s', ...
                num2strCode(this.Sonar.TVG.IfremerCoefDiverg), num2strCode(this.Sonar.TVG.IfremerConstante));
    end
    if ~isempty(strAlpha)
        cstr{end+1} = sprintf('       Alpha is frequency dependent, look at layers in SSc to get the real values. The mean value is %s dB/km', strAlpha);
    end
else
    cstr{end+1} = sprintf('TL : Not compensated');
end

%% Tx diagrams

if this.Sonar.DiagEmi.etat == 1
    if this.Sonar.DiagEmi.origine == 1
        if this.Sonar.DiagEmi.ConstructTypeCompens == 1
            cstr{end+1} = sprintf('TxDiag : Compensated, RT, Analytical');
        else
            cstr{end+1} = sprintf('TxDiag : Compensated, RT, Nodes');
        end
    else
        if this.Sonar.DiagEmi.IfremerTypeCompens == 1
            cstr{end+1} = sprintf('TxDiag : Compensated, SSc, Analytical');
        else
            cstr{end+1} = sprintf('TxDiag : Compensated, SSc, Nodes');
        end
    end
else
    cstr{end+1} = sprintf('TxDiag : Not compensated');
end

%% Rx diagrams

%{
if this.Sonar.DiagRec.etat == 1
    cstr{end+1} = sprintf('RxDiag : Compensated');
    if this.Sonar.DiagRec.origine == 1
        cstr{end+1} = sprintf('RxDiag : Compensated, Factory');
        if this.Sonar.DiagRec.ConstructTypeCompens == 1
            cstr{end+1} = sprintf('RxDiag : Compensated, RT, Analytical');
        else
            cstr{end+1} = sprintf('RxDiag : Compensated, RT, Nodes');
        end
    else
        cstr{end+1} = sprintf('RxDiag : Compensated, Ifremer');
        if this.Sonar.DiagRec.IfremerTypeCompens == 1
            cstr{end+1} = sprintf('RxDiag : Compensated, SSc, Analytical');
        else
            cstr{end+1} = sprintf('RxDiag : Compensated, SSc, Nodes');
        end
    end
else
    cstr{end+1} = sprintf('RxDiag : Not compensated');
end
%}

%% Insonified area

if this.Sonar.AireInso.etat == 1
    cstr{end+1} = sprintf('IA : Compensated, %s', this.Sonar.AireInso.strOrigine{this.Sonar.AireInso.origine});
else
    cstr{end+1} = sprintf('IA : Not compensated');
end

%% BS

if this.Sonar.BS.etat == 1
    if this.Sonar.BS.origine == 1
        if this.Sonar.BS.origineBelleImage == 1
            if isfield(this.Sonar.BS, 'LambertCorrection')
            	cstr{end+1} = sprintf('BS : Compensated, Relative, Lambert (%s)', this.Sonar.BS.strLambertCorrection{this.Sonar.BS.LambertCorrection});
            else
                cstr{end+1} = sprintf('BS : Compensated, Relative, Lambert');
            end
        else
            cstr{end+1} = sprintf('BS : Compensated, Relative, Lurton');
        end
    else
        if this.Sonar.BS.origineFullCompens == 1
            cstr{end+1} = sprintf('BS : Compensated, Full, Analytical');
        else
            cstr{end+1} = sprintf('BS : Compensated, Full, Table, %s', num2strCode(this.Sonar.BS.IfremerCompensTable));
        end
    end
else
    cstr{end+1} = sprintf('BS : Not compensated');
end

if isfield(this.Sonar.BS, 'IdentAlgoSnippets2OneValuePerBeam')
    cstr{end+1} = ' ';
    strAlgo = {'dB'; 'Amp'; 'Enr'; 'Med'};
    cstr{end+1} = sprintf('AlgoSnippets2OneValuePerBeam : %s', strAlgo{this.Sonar.BS.IdentAlgoSnippets2OneValuePerBeam});
end

if isfield(this.Sonar.BS, 'SpecularReestablishment')
    strAlgo = {'No'; 'Yes'};
    cstr{end+1} = sprintf('Specular re-establishment : %s', strAlgo{this.Sonar.BS.SpecularReestablishment + 1});
end

str = cell2str(cstr);
