function [flag, str] = get_Bathy_Status(this)
    
str = {};

identBathymetry = cl_image.indDataType('Bathymetry');

flag = testSignature(this, 'DataType', identBathymetry);
if ~flag
    return
end

if this.Sonar.BathymetryStatus.TideApplied
    str{end+1} = sprintf('TideApplied : Yes');
else
    str{end+1} = sprintf('TideApplied : No');
end

str = cell2str(str);
