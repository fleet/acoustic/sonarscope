function BathymetryStatus = get_BathymetryStatus(this)
BathymetryStatus = this.Sonar.BathymetryStatus;
