% Get Carto (cl_carto aggregation)
%
% Syntax
%  Carto = get_Carto(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   Carto : Instance of cl_carto
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = import(cl_image, nomFic);
%   imagesc(a)
%   Carto = get_Carto(a)
%
% See also cl_carto cl_image/set_Carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Carto = get_Carto(this)
Carto = this.Carto;
