% Get ColormapIndexName
%
% Syntax
%  ColormapIndexName = get_ColormapIndexName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   ColormapIndexName : Data type name
%
% Remarks : No setter for this property
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Lena', 'DataType', 3, 'GeometryType', 15);
%     imagesc(a)
%     strColormapIndex = cl_image.strColormapIndex
%     ColormapIndex = a.ColormapIndex
%   ColormapIndexName = get_ColormapIndexName(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ColormapIndexName = get_ColormapIndexName(this)
ColormapIndexName = this.strColormapIndex{this.ColormapIndex};
