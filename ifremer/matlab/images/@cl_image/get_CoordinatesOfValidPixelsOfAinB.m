% Calcul des coordonn�es des pixels valide de l'image entrante dans la mosa�que

function [subImage, subMos, sub_subImageInMos] = get_CoordinatesOfValidPixelsOfAinB(that, ImageMos)

subImage = findNonNaN(that, 'subxyOnly', 1);
if max(subImage(:)) < intmax('uint32')
    % Dans le cas de pb m�moire, activer la ligne suivante (solution GLU-QO le
    % 16/03/2017). Attention : peut-�tre impactant sur des images tr�s grosses
    % (avec index de pixels tr�s grands) support�es par des PC capables de les traiter.
    subImage = uint32(findNonNaN(that, 'subxyOnly', 1));
end

% ixMos = NaN(size(subImage), 'single'); % V�rifier si probl�me de pr�cision en single
% iyMos = NaN(size(subImage), 'single');
ixMos = NaN(4,length(subImage), 'single');
iyMos = NaN(4,length(subImage), 'single');
w     = NaN(4,length(subImage), 'single');

nB = 1e6;
for k=1:nB:length(subImage)
    subi = k:min(k-1+nB, length(subImage));
    [LVal, CVal] = ind2sub([that.nbRows that.nbColumns], subImage(subi));
    [ixMos(:,subi), iyMos(:,subi), w(:,subi)] = interpCube(ImageMos.x, that.x(CVal), ImageMos.y, that.y(LVal));
end
ixMos = ixMos(:);
iyMos = iyMos(:);
w     = w(:);

subImage = repmat(subImage', 4, 1);
subImage = subImage(:);

subUnsignificant = find(w < (0.25 ^ 2));
clear w that LVal CVal
if ~isempty(subUnsignificant)
    ixMos(subUnsignificant)    = [];
    iyMos(subUnsignificant)    = [];
    subImage(subUnsignificant) = [];
end
clear subUnsignificant
sz = [ImageMos.nbRows ImageMos.nbColumns ImageMos.nbSlides];
sub_subImageOutOfMos = (ixMos > sz(2)) | (iyMos > sz(1)) | (ixMos < 1) | (iyMos < 1);
length_sub_subImageOutOfMos = sum(sub_subImageOutOfMos(:));
clear sub_subImageOutOfMos

if length_sub_subImageOutOfMos == 0
% if length_sub_subImageOutOfMos <= 1
    sub_subImageInMos = 'AllOfThem';
    subMos =  my_sub2ind(sz(1:2), iyMos, ixMos);
    clear ixMos iyMos
else
    sub_subImageInMos = find((ixMos <= sz(2)) & (iyMos <= sz(1)) & (ixMos >= 1) & (iyMos >= 1));
    subMos = uint32(my_sub2ind(sz(1:2), iyMos(sub_subImageInMos), ixMos(sub_subImageInMos)));
    clear ixMos iyMos
end

if ~isequal(sub_subImageInMos, 'AllOfThem')
    subImage = subImage(sub_subImageInMos); % Rajout JMA le 27/06/2017 : images Charline BICOSE
end

isSubMosNaN = isnan(subMos);
subMos(isSubMosNaN)            = [];
subImage(isSubMosNaN)          = [];
sub_subImageInMos(isSubMosNaN) = [];
