% Retourne les regions d'interet definies sur une image
%
% Syntax
%   CourbesStatistiques = get_CourbesStatistiques(a, ...)
%
% Input Arguments
%   a : instance de cl_image
%
% Name-Value Pair Arguments
%   sub : Liste des CourbesStatistiques a retourner
%
% Output Arguments
%   CourbesStatistiques : Structure contenant les Regions D'Interet
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function CourbesStatistiques = get_CourbesStatistiques(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques)); %#ok<ASGLU>

CourbesStatistiques = this.CourbesStatistiques(sub);
