% Get DataTypeName
%
% Syntax
%  DataTypeName = get_DataTypeName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   DataTypeName : Data type name
%
% Remarks : No setter for this property
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%     cl_image.strDataType = cl_image.strDataType
%     DataType   = a.DataType
%   DataTypeName = get_DataTypeName(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function DataTypeName = get_DataTypeName(this)
DataTypeName = cl_image.strDataType{this.DataType};
