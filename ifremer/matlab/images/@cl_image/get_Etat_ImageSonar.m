% Questionnaire pour savoir dans quel etat est l'image sonar.
% On ne peut pas savoir comment ont ete traitees les images provenant de CaraibesR 
%
% Syntax
%   BSStatus = get_Etat_ImageSonar(a, SonarDescription, SonarTVG_IfremerAlpha)
%
% Input Arguments
%   a : Instance de cl_image
%
% Output Arguments
%   SonarDescription       : Instance de cl_sounder
%   SonarTVG_IfremerAlpha : Coefficient d'attenuation en dB/km/m
%
% Output Arguments
%   b : Instance de cl_image contenant l'image d'autocorrelation
%
% Examples 
%   BSStatus = get_Etat_ImageSonar(cl_image, SonarDescription, 0)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [BSStatus, flag] = get_Etat_ImageSonar(this, SonarDescription, SonarTVG_IfremerAlpha)%#ok

BSStatus = init_BSStatus;

% SonarFamily = get(SonarDescription, 'Sonar.Family'); % 1='Sonar' | 2='Multibeam'
% if SonarFamily == 1 % 1='Sonar' a ete traite par chaine sonar lateral
%     disp('A faire')
% 
% else % 2='Multibeam' a ete traite par ereamo ou ereamos

    if isempty(SonarTVG_IfremerAlpha)
        my_warndlg('Absorption coefficient could not be estimated, fixed to 0, take care to input an apropriate value if you want to use the calibration tools.', 1);
        SonarTVG_IfremerAlpha = 0;
    end
    
    [flag, SonarTVG_ConstructeurAlpha] = inputOneParametre('Absorption coefficient used in real-time', 'SonarTVG_ConstructeurAlpha', ...
        'Value', SonarTVG_IfremerAlpha, 'Unit', 'dB/km/m', 'MinValue', 0, 'MaxValue', 200);
    if ~flag
        return
    end
    
    BSStatus.SonarTVG_ConstructAlpha = SonarTVG_ConstructeurAlpha;
    if SonarTVG_IfremerAlpha == 0
        BSStatus.SonarTVG_IfremerAlpha = SonarTVG_ConstructeurAlpha;
    else
        BSStatus.SonarTVG_IfremerAlpha = SonarTVG_IfremerAlpha;
    end

    [rep1, flag] = my_questdlg(Lang('TODO', 'Are Tx diagrams compensated ?'), 'Init', 2);
    if ~flag
        return
    end
    if rep1 == 1
        BSStatus.SonarDiagEmi_etat = 1; % Compense

        [rep2, flag] = my_questdlg(Lang('Origine de la compensation ?', 'Origin of compensation ?'), ...
            Lang('Usine', 'Factory'), 'Ifremer');
        if ~flag
            return
        end
        if rep2 == 1
            BSStatus.SonarDiagEmi_origine = 1; % Constructeur

            [rep3, flag] = my_questdlg(Lang('Type de compensation ?', 'Type of compensation ?'), ...
                Lang('Analytique', 'Analytic'), 'Table');
            if ~flag
                return
            end
            if rep3 == 1
                BSStatus.SonarDiagEmi_ConstructTypeCompens = 1; % Analytique
            else
                BSStatus.SonarDiagEmi_ConstructTypeCompens = 2; % Table
                disp('Lecture des fichiers � placer ici')
            end

        else
            BSStatus.SonarDiagEmi_origine = 2; % Ifremer'

            [rep3, flag] = my_questdlg(Lang('Type de compensation ?', 'Type of compensation ?'), ...
                Lang('Analytic','Analytic'), 'Table');
            if ~flag
                return
            end
            if rep3 == 1
                BSStatus.SonarDiagEmi_IfremerTypeCompens = 1; % Analytique
            else
                BSStatus.SonarDiagEmi_IfremerTypeCompens = 2; % Table
                disp('Lecture du fichier de correction � placer ici')
            end
        end
    else
        BSStatus.SonarDiagEmi_etat = 2; % Not compensated
    end

    str1 = 'Les diagrammes Rx sont-ils compens�s ?';
    str2 = 'Are Rx diagrams compensated ?';
    [rep1, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if rep1 == 1
        BSStatus.SonarDiagRec_etat = 1; % Compense

        str1 = 'Origine de la compensation ?';
        str2 = 'Origin of compensation ?';
        [rep2, flag] = my_questdlg(Lang(str1,str2), Lang('Usine', 'Factory'), 'Ifremer');
        if ~flag
            return
        end
        if rep2 == 1
            BSStatus.SonarDiagRec_origine = 1; % Constructeur
            str1 = 'Type de compensation';
            str2 = 'Type of compensation ?';
            [rep3, flag] = my_questdlg(Lang(str1,str2), ...
                Lang('Analytique', 'Analytic'), 'Table');
            if ~flag
                return
            end
            if rep3 == 1
                BSStatus.SonarDiagRec_ConstructTypeCompens = 1; % Analytique
            else
                BSStatus.SonarDiagRec_ConstructTypeCompens = 2; % Table
                disp('Lecture du fichier de correction � placer ici')
            end

        else
            BSStatus.SonarDiagRec_origine = 2; % Ifremer'

            str1 = 'Type de compensation';
            str2 = 'Type of compensation ?';
            [rep3, flag] = my_questdlg(Lang(str1,str2), ...
                Lang('Analytique', 'Analytic'), 'Table');
            if ~flag
                return
            end
            if rep3 == 1
                BSStatus.SonarDiagRec_IfremerTypeCompens = 1; % Analytique
            else
                BSStatus.SonarDiagRec_IfremerTypeCompens = 2; % Table
                disp('Lecture du fichier de correction � placer ici')
            end

        end
    else
        BSStatus.SonarDiagRec_etat = 2; % Not compensated
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    str1 = 'L''aire insonifi�e a t''elle �t� compens�e ?';
    str2 = 'Is Insonified Area compensated ?';
    [rep1, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep1 == 1
        BSStatus.SonarAireInso_etat = 1; % Compense

        str1 = 'Origine de la compensation ?';
        str2 = 'Origin of compensation ?';
        [rep2, flag] = my_questdlg(Lang(str1,str2), ...
            Lang('Usine', 'Factory'), 'Ifremer');
        if ~flag
            return
        end
        if rep2 == 1
            BSStatus.SonarAireInso_origine = 1; % Constructeur
        else
            BSStatus.SonarAireInso_origine = 2; % Ifremer'
        end
    else
        BSStatus.SonarAireInso_etat = 2; % Not compensated
    end

    str1 = 'Le BS est-il compens� ?';
    str2 = 'Is BS compensated ?';
    [rep1, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep1 == 1
        BSStatus.SonarBS_etat = 1; % Compense

        str1 = 'Origine de la compensation ?';
        str2 = 'Origin of compensation ?';
        [rep2, flag] = my_questdlg(Lang(str1,str2), ...
            'Belle Image', 'Full compensation');
        if ~flag
            return
        end
        if rep2 == 1
            BSStatus.SonarBS_origine = 1; % Belle Image
            str1 = 'Type de compensation ?';
            str2 = 'Type of compensation ?';
            [rep3, flag] = my_questdlg(Lang(str1,str2), 'Lambert', 'Lurton');
            if ~flag
                return
            end
            if rep3 == 1
                BSStatus.SonarBS_origineBelleImage = 1; % Belle Image
            else
                BSStatus.SonarBS_origineBelleImage = 2; % Full compensation
            end
        else
            BSStatus.SonarBS_origine = 2; % Full compensation
            str1 = 'Type de la compensation ?';
            str2 = 'Type of compensation ?';
            [rep3, flag] = my_questdlg(Lang(str1,str2), ...
                Lang('Analytique', 'Analytic'), 'Table');
            if ~flag
                return
            end
            if rep3 == 1
                BSStatus.SonarBS_origineFullCompens = 1; % Analytique
            else
                BSStatus.SonarBS_origineFullCompens = 2; % Table
                disp('Lecture du fichier de corection � placer ici')
            end
        end
    else
        BSStatus.SonarBS_etat = 2; % Not compensated
    end

    BSStatus.SonarBS_IfremerCompensTable         = [];
    drawnow
% end
