% Get GeometryTypeName
%
% Syntax
%  GeometryTypeName = get_GeometryTypeName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   GeometryTypeName : Geometry type name
%
% Remarks : No setter for this property
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = import(cl_image, nomFic);
%   imagesc(a)
%   strGeometryType = cl_image.strGeometryType
%   GeometryType = a.GeometryType
%   GeometryTypeName = get_GeometryTypeName(a)
%
% See also cl_image/get_GeometryTypeName Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function GeometryTypeName = get_GeometryTypeName(this)
GeometryTypeName = this.strGeometryType{this.GeometryType};
