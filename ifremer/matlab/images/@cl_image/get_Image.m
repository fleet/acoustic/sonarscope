% Get Image
%
% Syntax
%  Image = get_Image(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   Image : Matrix containing the image values
%
% Remarks : No setter for this property
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%   Image = get_Image(a);
%     figure; imagesc(Image); colormap(jet(256)); colorbar;
%
% See also cl_image/get_... Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Image = get_Image(this)
% Image = this.Image; % TODO : statuer si il faut rendre Image ou
% Image(:,:,:). C'est diff�rent si Image est un cl_memmapfile
Image = this.Image(:,:,:);
