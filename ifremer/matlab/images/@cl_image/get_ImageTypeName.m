% Get ImageTypeName
%
% Syntax
%  ImageTypeName = get_ImageTypeName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   ImageTypeName : Image type name
%
% Remarks : No setter for this property
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%     strImageType = cl_image.strImageType
%     ImageType = a.ImageType
%   ImageTypeName = get_ImageTypeName(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ImageTypeName = get_ImageTypeName(this)
ImageTypeName = this.strImageType{this.ImageType};
