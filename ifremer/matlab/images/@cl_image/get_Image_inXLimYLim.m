% Retourne l'image comprise dans le rectangle delimite par XLim et YLim
%
% Syntax
%   I = get_Image_inXLimYLim(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   I : Image extraite dans le rectangle delimite par XLim et YLim
%
% Examples
%   a = cl_image('Image', Lena)
%   I = get_Image_inXLimYLim(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [I, subx, suby] = get_Image_inXLimYLim(this, varargin)

[varargin, nbColMax] = getPropertyValue(varargin, 'nbColMax', []);
[varargin, nbLigMax] = getPropertyValue(varargin, 'nbLigMax', []); %#ok<ASGLU>

x = this.x;
y = this.y;

% Correctif de bug temporaire le temps de trouver l'origine du probl�me
% dans l'algo de segmentation
sz = size(this.Image);
if length(x) > sz(2)
    x = x(1: sz(2));
end
if length(y) > sz(1)
    y = y(1: sz(1));
end

XLim(1) = min(this.XLim);
XLim(2) = max(this.XLim);
YLim(1) = min(this.YLim);
YLim(2) = max(this.YLim);
subx = find((x >= XLim(1)) & (x <= XLim(2)));
suby = find((y >= YLim(1)) & (y <= YLim(2)));

if ~isempty(nbColMax)
    pasx = max(1,length(subx) / nbColMax);
    subsubx = round(1:pasx:length(subx));
    subx = subx(subsubx);
end

if ~isempty(nbLigMax)
    pasy = max(1,length(suby) / nbLigMax);
    subsuby = round(1:pasy:length(suby));
    suby = suby(subsuby);
end

% Distinction necessaire au cas ou l'image est de type sparse
if size(this.Image, 3) == 1
    I = this.Image(suby, subx);
else
    I = this.Image(suby, subx, :);
end
