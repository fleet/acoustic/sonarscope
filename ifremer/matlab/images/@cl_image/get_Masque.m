function Masque = get_Masque(LayerMask, x, y)

nbColumns = LayerMask.nbColumns;
nbRows    = LayerMask.nbRows;

[subx_Masque, suby_Masque] = xy2ij_extrap_outside(LayerMask, x, y);

subx = find((subx_Masque >= 1) & (subx_Masque <= nbColumns));
suby = find((suby_Masque >= 1) & (suby_Masque <= nbRows));

Masque = NaN(length(y), length(x), 'single');
Masque(suby,subx) = LayerMask.Image(suby_Masque(suby),subx_Masque(subx));
