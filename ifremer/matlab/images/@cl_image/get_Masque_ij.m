function Masque = get_Masque_ij(LayerMask, subx_Masque, suby_Masque)

nbColumns = LayerMask.nbColumns;
nbRows    = LayerMask.nbRows;

subx = find((subx_Masque >= 1) & (subx_Masque <= nbColumns));
suby = find((suby_Masque >= 1) & (suby_Masque <= nbRows));

Masque = NaN(length(suby_Masque), length(subx_Masque), 'single');
Masque(suby,subx) = LayerMask.Image(suby_Masque(suby),subx_Masque(subx));
