function [TempsSimple, RangeInMeters] = get_RangeInMeters_WC(this, RangeDetectionInSamples, fe, C)

if (length(unique(this.Sonar.SampleBeamData.SamplingRateMinMax)) == 1) || ...
        isempty(this.Sonar.SampleBeamData.SamplingRateMinMax)
    TempsSimple = this.y(:) / (2*fe(1)); % TODO fe(1) pour EM3002
    RangeInMeters = C(1) * RangeDetectionInSamples / (2*fe(1)); % TODO C(1) et fe(1) pour EM3002
else
    feBab = this.Sonar.SampleBeamData.SamplingRateMinMax(1);
    feTri = this.Sonar.SampleBeamData.SamplingRateMinMax(2);
    subTri = (this.Sonar.SampleBeamData.TransmitSectorNumber == 2);
    TempsSimpleBab = this.y(:) / (2*feBab);
    TempsSimpleTri = this.y(:) / (2*feTri);
    TempsSimple(:, ~subTri) = repmat(TempsSimpleBab, 1, sum(~subTri));
    TempsSimple(:,  subTri) = repmat(TempsSimpleTri, 1, sum(subTri));
    if length(C) == 1
        RangeInMeters(~subTri) = C * RangeDetectionInSamples(~subTri) / (2*feBab);
        RangeInMeters( subTri) = C * RangeDetectionInSamples( subTri) / (2*feTri);
    else
        RangeInMeters(~subTri) = C(1) * RangeDetectionInSamples(~subTri) / (2*feBab);
        RangeInMeters( subTri) = C(2) * RangeDetectionInSamples( subTri) / (2*feTri);
    end
end
