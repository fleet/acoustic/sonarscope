function [flag, RxAnglesEarth_WC] = get_RxAnglesEarth_WC(this)

if  isfield(this.Sonar.SampleBeamData, 'BeamPointingAngle') && ~isempty(this.Sonar.SampleBeamData.BeamPointingAngle)
    if length(this.Sonar.SampleBeamData.BeamPointingAngle) == length(this.Sonar.SampleBeamData.R1SamplesFiltre)
        % Sondeurs Reson : TODO : trouver et corriger ce pb � sa source
        RxAnglesEarth_WC = this.Sonar.SampleBeamData.BeamPointingAngle(:,:);
    else
        % Sondeurs Kongsberg : TODO : trouver et corriger ce pb � sa source
        RxAnglesEarth_WC = this.Sonar.SampleBeamData.TxAngle;
    end
elseif isfield(this.Sonar.SampleBeamData, 'RxAnglesEarth_WC') && ~isempty(this.Sonar.SampleBeamData.RxAnglesEarth_WC)
    RxAnglesEarth_WC = this.Sonar.SampleBeamData.RxAnglesEarth_WC;
    RxAnglesEarth_WC = RxAnglesEarth_WC(:)'; % Rajout� par JMA le 23/07/2013
else
    RxAnglesEarth_WC = this.Sonar.SampleBeamData.TxAngle;
end
if all(isnan(RxAnglesEarth_WC))
    flag = 0;
    return
end

flag = 1;
