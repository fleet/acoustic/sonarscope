% Retourne les regions d'interet definies sur une image
%
% Syntax
%   Texture = get_SignatureTexture(a, ...)
%
% Input Arguments
%   a : instance de cl_image
%
% Name-Value Pair Arguments
%   sub : Liste des Texture a retourner
%
% Output Arguments
%   Texture : Structure contenant les Regions D'Interet
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Texture = get_SignatureTexture(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.Texture)); %#ok<ASGLU>

Texture = this.Texture(sub);
