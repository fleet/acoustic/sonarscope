% Retourne les profils de c�l�rit� definis sur une image
%
% Syntax
%   BathyCel = get_SonarBathyCel(a, ...)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   BathyCel : Structure contenant les profils de c�l�rit�
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function BathyCel = get_SonarBathyCel(this)
BathyCel = this.Sonar.BathyCel;
