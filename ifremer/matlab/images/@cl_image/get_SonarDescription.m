function [SonarDescription, SonarName] = get_SonarDescription(this)
SonarDescription = this.Sonar.Desciption;
SonarName = get(SonarDescription, 'Sonar.Name');
