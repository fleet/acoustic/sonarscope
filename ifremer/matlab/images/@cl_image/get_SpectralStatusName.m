% Get SpectralStatusName
%
% Syntax
%  SpectralStatusName = get_SpectralStatusName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   SpectralStatusName : Data type name
%
% Remarks : No setter for this property
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15);
%     imagesc(a)
%     strSpectralStatus = cl_image.strSpectralStatus
%     SpectralStatus = a.SpectralStatus
%   SpectralStatusName = get_SpectralStatusName(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function SpectralStatusName = get_SpectralStatusName(this)
SpectralStatusName = this.strSpectralStatus{this.SpectralStatus};
