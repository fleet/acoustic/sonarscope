% R�cup�ration des Y Ticks de l'�chelle des couleurs
%
% Syntax
%   YTicks = get_YTickLabels(this)
%
% Input Arguments
%   a      : Instance de cl_image
%
% Output Arguments
%   YTicks : tableau de valeurs de Ticks
%
% Examples
%   nomFicIn = getNomFicDatabase('india_geoid.grd')
%   a = import_gmt(cl_image, nomFicIn);
%   T = get_TickColorLabels(a)
%
%
% See also cl_image/plot_gmt Authors
% Authors : GLU
%--------------------------------------------------------------------------

function tickLabels = get_TickColorLabels(this)

% hFig = imagesc(this, 'Visible', 'off');
hFig = figure;
I =  NaN(10);
I(1) = this.CLim(1);
I(2) = this.CLim(2);
imagesc(I); colorbar
hdlAxes = findobj(hFig, 'Tag', 'Colorbar');

tickLabels = get(hdlAxes, 'YTick');

my_close(hFig);
