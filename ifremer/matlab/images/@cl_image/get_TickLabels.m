% R�cup�ration des Y Ticks de l'�chelle des couleurs
%
% Syntax
%   YTicks = get_YTickLabels(this)
%
% Input Arguments
%   a      : Instance de cl_image
%
% Output Arguments
%   YTicks : tableau de valeurs de Ticks
%
% Examples
%   nomFicIn = getNomFicDatabase('india_geoid.grd')
%   a = import_gmt(cl_image, nomFicIn);
%   T = get_TickLabels(a)
%
%
% See also cl_image/plot_gmt Authors
% Authors : GLU
%--------------------------------------------------------------------------

function [XTickLabels, YTickLabels]= get_TickLabels(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

hFig = figure;
imagesc(this.x(subx([1 length(subx)])), this.y(suby([1 length(suby)])), zeros(1000)); axis xy
% hFig = imagesc(this.Image(suby, subx, :), 'Visible', 'off');

hdlAxes = get(hFig, 'CurrentAxes');

XTickLabels = get(hdlAxes, 'XTick');
YTickLabels = get(hdlAxes, 'YTick');

my_close(hFig);
