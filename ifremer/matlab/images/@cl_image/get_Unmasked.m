function subNonMasque = get_Unmasked(LayerMask, valMask, x, y)

if isempty(LayerMask)
    subNonMasque = [];
else
    Masque = get_Masque(LayerMask, x, y);
    K = zeros(size(Masque), 'uint8');
    for ik=1:length(valMask)
        sub = (Masque == valMask(ik));
        K(sub) = 1;
    end
    clear Masque sub
    subNonMasque = (K == 0);
end
