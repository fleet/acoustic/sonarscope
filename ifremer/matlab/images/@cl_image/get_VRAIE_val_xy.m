% Retourne les valeurs de l'image specifiees par les coordonnees i, j
%
% Syntax
%   [val, ix, iy] = get_VRAIE_val_xy(a, x, y)
%
% Input Arguments
%   a : instance de cl_image
%   x : Abscisses
%   y : Ordonnees
%
% Output Arguments
%   val : Valeurs des pixels
%   ix  : Numero de colonne correspondant
%   iy  : Numero de ligne correspondant
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, ix, iy] = get_VRAIE_val_xy(this, valX, valY)

x   = this.x;
y   = this.y;
ix  = interp1(x, 1:length(x), valX, 'nearest', 'extrap');
iy  = interp1(y, 1:length(y), valY, 'nearest', 'extrap');

val = this.Image(iy, ix, :);
