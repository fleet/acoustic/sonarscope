% Get VideoName
%
% Syntax
%  VideoName = get_VideoName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   VideoName : Data type name
%
% Remarks : No setter for this property
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15);
%     imagesc(a)
%     strVideo = cl_image.strVideo
%     Video = a.Video
%   VideoName = get_VideoName(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function VideoName = get_VideoName(this)
VideoName = this.strVideo{this.Video};
