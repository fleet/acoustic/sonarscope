% Get XDirName
%
% Syntax
%  XDirName = get_XDirName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   XDirName : Data type name
%
% Remarks : No setter for this property
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15);
%     imagesc(a)
%     strXDir = cl_image.strXDir
%     XDir = a.XDir
%   XDirName = get_XDirName(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function XDirName = get_XDirName(this)
XDirName = this.strXDir{this.XDir};
