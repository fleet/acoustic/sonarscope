% Get YDirName
%
% Syntax
%  YDirName = get_YDirName(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   YDirName : Data type name
%
% Remarks : No setter for this property
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15);
%     imagesc(a)
%     strYDir = cl_image.strYDir
%     YDir = a.YDir
%   YDirName = get_YDirName(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function YDirName = get_YDirName(this)
YDirName = this.strYDir{this.YDir}; 
