function Coul = get_coul(this, Val)

if  this.ColormapIndex == 1
    Map = this.ColormapCustom;
else
    Map = this.Colormap;
end
CLim = this.CLim;
N = size(Map,1);
%TODO : peut-�tre faudrait-il utiliser la fonction intlut ici ?
k = round(interp1(CLim, [1 N], double(Val), 'linear', 'extrap'));
k = max(k,1);
k = min(k, N);
Coul = Map(k,:);
   