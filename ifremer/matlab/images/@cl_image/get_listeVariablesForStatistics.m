function [strChoix, TypeVal, indLayer, DataType, Unit] = get_listeVariablesForStatistics(this, indImage, indLayers, varargin)

[varargin, noXY] = getPropertyValue(varargin, 'noXY', 0);
[varargin, flagSignauxVert] = getPropertyValue(varargin, 'flagSignauxVert', 1); %#ok<ASGLU>

N = length(indLayers);
strChoix  = cell(N,1);
TypeVal   = cell(N,1);
indLayer  = [];
DataType  = [];
Unit      = {};
for k=1:N
    strChoix{k} = this(indLayers(k)).Name;
    TypeVal{k} = 'Layer';
    indLayer(k) = indLayers(k);               %#ok<AGROW>
    DataType(k) = this(indLayer(k)).DataType; %#ok<AGROW>
    Unit{k}     = this(indLayer(k)).Unit;     %#ok<AGROW>
end
if ~noXY
    % ATTENTION : ce sont les 5 premiers caract�res de cette cha�ne de
    % caract�res qui sera analys�e c.a.c "Axe X"
    strChoix{end+1} = Lang('Axe X de l''image courante', 'Axe X of the current image');
    TypeVal{end+1}  = 'Axe';
    indLayer(end+1) = 0;
    DataType(end+1) = 0;
    Unit{end+1}     = this(indImage).XUnit;
    
    strChoix{end+1} = Lang('Axe Y de l''image courante', 'Axe Y of the current image');
    TypeVal{end+1}  = 'Axe';
    indLayer(end+1) = 0;
    DataType(end+1) = 0;
    Unit{end+1}     = this(indImage).YUnit;
end

if flagSignauxVert
    if isSonarSignal(this(indImage), 'PingCounter')
        strChoix{end+1} = 'PingCounter';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    
    if isSonarSignal(this(indImage), 'Immersion')
        strChoix{end+1} = 'Immersion';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'm';
    end
    
    if isSonarSignal(this(indImage), 'Height')
        strChoix{end+1} = 'Height';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'm';
    end
    
    if isSonarSignal(this(indImage), 'Speed')
        strChoix{end+1} = 'Speed';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'm/s';
    end
    
    if isSonarSignal(this(indImage), 'Heading')
        strChoix{end+1} = 'Heading';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'deg';
    end
    
    if isSonarSignal(this(indImage), 'Roll')
        strChoix{end+1} = 'Roll';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'deg';
    end
    
    if isSonarSignal(this(indImage), 'Pitch')
        strChoix{end+1} = 'Pitch';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'deg';
    end
    
    if isSonarSignal(this(indImage), 'Yaw')
        strChoix{end+1} = 'Yaw';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'deg';
    end
    
    if isSonarSignal(this(indImage), 'SurfaceSoundSpeed')
        strChoix{end+1} = 'SurfaceSoundSpeed';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'm/s';
    end
    
    if isSonarSignal(this(indImage), 'PortMode_1')
        strChoix{end+1} = 'PortMode_1';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    
    if isSonarSignal(this(indImage), 'PortMode_2')
        strChoix{end+1} = 'PortMode_2';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    
    if isSonarSignal(this(indImage), 'Heave')
        strChoix{end+1} = 'Heave';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'm';
    end
    
    if isSonarSignal(this(indImage), 'SampleFrequency')
        strChoix{end+1} = 'SampleFrequency';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'kHz';
    end
    
    if isSonarSignal(this(indImage), 'SonarFrequency')
        strChoix{end+1} = 'SonarFrequency';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'kHz';
    end
    
    if isSonarSignal(this(indImage), 'Interlacing')
        strChoix{end+1} = 'Interlacing';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    
    if isSonarSignal(this(indImage), 'PresenceWC')
        strChoix{end+1} = 'PresenceWC';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    
    if isSonarSignal(this(indImage), 'PresenceFM')
        strChoix{end+1} = 'PresenceFM';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    
    if isSonarSignal(this(indImage), 'DistPings')
        strChoix{end+1} = 'DistPings';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = 'm';
    end
    
    if isSonarSignal(this(indImage), 'NbSwaths')
        strChoix{end+1} = 'NbSwaths';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    
    % Test pour pouvoir faire une statistique en fonction du temps mais �a
    % pose trop de pbs. Ca serait bien de pouvoir faire cela !
    %{
    if isSonarSignal(this(indImage), 'Time')
        strChoix{end+1} = 'Time';
        TypeVal{end+1} = 'SignalVert';
        indLayer(end+1) = 0;
        DataType(end+1) = 0;
        Unit{end+1}     = '#';
    end
    %}
end
