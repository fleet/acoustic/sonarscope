% Liste des vecteurs lies a l'image
%
% Syntax
%   liste = get_liste_vecteurs(a)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Output Arguments
%   liste : Liste des noms des vecteurs lies a l'image
%
% Examples
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function liste = get_liste_vecteurs(this, varargin)

[varargin, VerticalOnly] = getFlag(varargin, 'VerticalOnly'); %#ok<ASGLU>

liste = {'Time'
        'PingNumber'
        'Immersion'
        'Height'
        'CableOut'
        'Speed'
        'Heading'
        'Roll'
        'Pitch'
        'Yaw'
        'SurfaceSoundSpeed'
        'FishLatitude'
        'FishLongitude'
        'ShipLatitude'
        'ShipLongitude'
        'PortMode_1'
        'PortMode_2'
        'StarMode_1'
        'StarMode_2'
        'Heave'
        'TrueHeave'
        'Tide'
        'Draught'
        'PingCounter'
        'SampleFrequency'
        'SonarFrequency'
        'Interlacing'
        'PresenceWC'
        'PresenceFM'
        'DistPings'
        'NbSwaths'
        'BSN'
        'BSO'
        'BSN-BSO'
        'TVGN'
        'TVGCrossOver'
        'TxBeamWidth'
        'ResolAcrossSample'
        'AbsorptionCoeff_RT'
        'AbsorptionCoeff_SSc'
        'EM2040Mode2'
        'EM2040Mode3'};
    
flag = [isSonarSignal(this, 'Time')
        isSonarSignal(this, 'PingNumber')
        isSonarSignal(this, 'Immersion')
        isSonarSignal(this, 'Height')
        isSonarSignal(this, 'CableOut')
        isSonarSignal(this, 'Speed')
        isSonarSignal(this, 'Heading')
        isSonarSignal(this, 'Roll')
        isSonarSignal(this, 'Pitch')
        isSonarSignal(this, 'Yaw')
        isSonarSignal(this, 'SurfaceSoundSpeed')
        isSonarSignal(this, 'FishLatitude')
        isSonarSignal(this, 'FishLongitude')
        isSonarSignal(this, 'ShipLatitude')
        isSonarSignal(this, 'ShipLongitude')
        isSonarSignal(this, 'PortMode_1')
        isSonarSignal(this, 'PortMode_2')
        isSonarSignal(this, 'StarMode_1')
        isSonarSignal(this, 'StarMode_2')
        isSonarSignal(this, 'Heave')
        isSonarSignal(this, 'TrueHeave')
        isSonarSignal(this, 'Tide')
        isSonarSignal(this, 'Draught')
        isSonarSignal(this, 'PingCounter')
        isSonarSignal(this, 'SampleFrequency')
        isSonarSignal(this, 'SonarFrequency')
        isSonarSignal(this, 'Interlacing')
        isSonarSignal(this, 'PresenceWC')
        isSonarSignal(this, 'PresenceFM')
        isSonarSignal(this, 'DistPings')
        isSonarSignal(this, 'NbSwaths')
        isSonarSignal(this, 'BSN')
        isSonarSignal(this, 'BSO')
        isSonarSignal(this, 'BSN')&isSonarSignal(this, 'BSO')
        isSonarSignal(this, 'TVGN')
        isSonarSignal(this, 'TVGCrossOver')
        isSonarSignal(this, 'TxBeamWidth')
        isSonarSignal(this, 'ResolAcrossSample')
        isSonarSignal(this, 'AbsorptionCoeff_RT')
        isSonarSignal(this, 'AbsorptionCoeff_SSc')
        isSonarSignal(this, 'EM2040Mode2')
        isSonarSignal(this, 'EM2040Mode3')
];

tagList = {this.SignalsVert.tag};
liste = [liste; tagList'];
flag = [flag; true(length(tagList),1)];

if VerticalOnly
    liste = [{'Vertical profile'}; 'MeanValueOfCurrentExtent'; liste];
    flag = [1; 1; flag];
else
    liste = [{'Horizontal Profile'}; {'Vertical profile'}; liste];
    flag = [1; 1; flag];
end

liste = liste(flag == 1);
