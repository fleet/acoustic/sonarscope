function SonarMode_2 = get_mode2(this)

nomVar = 'Mode2';
signal = this.SignalsVert.getSignalByTag(nomVar);
[flag, SonarMode_2] = signal.getValueMatrix();
if ~flag
    str = sprintf('cl_image/get : %s non pris en compte', nomVar);
    my_warndlg(str, 0);
end


