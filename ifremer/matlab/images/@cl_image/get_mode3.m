function SonarMode_3 = get_mode3(this)

nomVar = 'Mode3';
signal = this.SignalsVert.getSignalByTag(nomVar);
[flag, SonarMode_3] = signal.getValueMatrix();
if ~flag
    str = sprintf('cl_image/get : %s non pris en compte', nomVar);
    my_warndlg(str, 0);
end


