% Get the number of bytes occupied by the image
%
% Syntax
%   N = get_nbBytes(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   Video : Direction of the x axis when displayed : 1=Normal | 2=Inverse
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%     imagesc(a)
%   N = get_nbBytes(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function N = get_nbBytes(this)

N = this.nbColumns * this.nbRows * this.nbSlides;

switch class(this.Image(1))
    case {'uint8'; 'int8'}
    case {'uint16'; 'int16'}
        N = N * 2;
    case {'uint32'; 'int32'; 'single'}
        N = N * 4;
    case 'double'
        N = N * 8;
    otherwise
        my_warndlg('Strange', 1);
end
