% Retourne les valeurs de l'image specifiees par les coordonnees i, j
%
% Syntax
%   [val, ix, iy] = get_pixels_xy(a, x, y)
%
% Input Arguments
%   a : instance de cl_image
%   x : Abscisses
%   y : Ordonnees
%
% Output Arguments
%   val : Valeurs des pixels
%   ix  : Numero de colonne correspondant
%   iy  : Numero de ligne correspondant
%
% Examples
%  a = cl_image('Image', Lena)
%  imagesc(a)
%
% x = -50:1.3:200;
% y = 100:0.5:300;
% [val, ix, iy] = get_pixels_xy(a, x, y);
% figure; imagesc(x, y, val)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, ix, iy] = get_pixels_xy(this, valX, valY)


x   = this.x;
y   = this.y;
ix  = interp1(x, 1:length(x), valX, 'linear', 'extrap');
iy  = interp1(y, 1:length(y), valY, 'linear', 'extrap');

ix = round(ix);
iy = round(iy);

nx = length(valX);
ny = length(valY);
nc = size(this.Image, 3);
val = NaN([ny, nx, nc], 'single');

suby = find((iy >= 1) & (iy <= length(y)));
subx = find((ix >= 1) & (ix <= length(x)));
val(suby, subx,:) = this.Image(iy(suby), ix(subx), :);


