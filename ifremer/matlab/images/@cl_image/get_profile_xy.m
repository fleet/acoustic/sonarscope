% Retourne les valeurs le long d'une ligne brisee
%
% Syntax
%   [val, d] = get_val_xy(a, X, Y)
%
% Input Arguments
%   a : instance de cl_image
%   X : Abscisses
%   Y : Ordonnees
%
% Output Arguments
%   val      : Valeurs des pixels
%   d        : Distances le long du profil
%   longueur : Longueur du segment
%   azimuth  : Azimuthdu segment
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, d, longueur, azimuth, longueur_m, CadreOut, XPixels, YPixels, nbPix] = get_profile_xy(this, X, Y, NbVoisins, varargin)

[varargin, nbPix] = getPropertyValue(varargin, 'nbPix', []); %#ok<ASGLU>

val        = [];
d          = [];
longueur   = [];
azimuth    = [];
longueur_m = [];

for k=1:length(X)-1
    [val1, d1, l1, a1, lm1, Cadre, XPix, YPix, nbPix] = get_profile_xy_unitaire(this, X([k k+1]), Y([k k+1]), NbVoisins, nbPix);
    longueur(k) = l1; %#ok<AGROW>
    azimuth(k) = a1; %#ok<AGROW>
    if isempty(lm1)
        longueur_m = [];
    else
        longueur_m(k) = lm1; %#ok<AGROW>
    end
    val{1,k} = val1; %#ok
    d{1,k} = d1; %#ok
    XPixels{1,k} = XPix; %#ok<AGROW>
    YPixels{1,k} = YPix; %#ok<AGROW>
    
    CadreOut{k} = Cadre; %#ok<AGROW>
end


function [val, d, longueur, azimuth, longueur_m, Cadre, XPixels, YPixels, nbPix] = get_profile_xy_unitaire(this, X, Y, NbVoisins, nbPix)

Cadre = [];
val = [];

% Utiliser NbVoisins pour faire un moyennage sur un ruban de largeur 2*NbVoisins+1

if this.GeometryType == cl_image.indGeometryType('LatLong')
    %     R = get(this.Carto, 'Ellipsoide.DemiGrandAxe');
    R = 6378137;
    longueur_m = distance(Y(1), X(1), Y(2), X(2)) * R * (pi/180);
    xfac = cosd(mean(Y));
    azimuth = atan2((X(2)-X(1))*xfac , Y(2)-Y(1)) * (180/pi);
else
    longueur_m = [];
    azimuth = atan2(X(2)-X(1) , Y(2)-Y(1)) * (180/pi);
end
longueur = sqrt((X(2) - X(1)) ^ 2 + (Y(2) - Y(1)) ^ 2);


x = this.x;
y = this.y;
% ixDeb = interp1(x, 1:length(x), X(1), 'nearest', 'extrap');
% ixFin = interp1(x, 1:length(x), X(2), 'nearest', 'extrap');
% iyDeb = interp1(y, 1:length(y), Y(1), 'nearest', 'extrap');
% iyFin = interp1(y, 1:length(y), Y(2), 'nearest', 'extrap');
ixDeb = floor(interp1(x, 1:length(x), X(1), 'linear', 'extrap'));
ixFin = floor(interp1(x, 1:length(x), X(2), 'linear', 'extrap'));
iyDeb = floor(interp1(y, 1:length(y), Y(1), 'linear', 'extrap'));
iyFin = floor(interp1(y, 1:length(y), Y(2), 'linear', 'extrap'));

if isempty(nbPix)
    nbPix = ceil(sqrt((ixFin - ixDeb) ^ 2 + (iyFin - iyDeb) ^ 2));
    nbPix = max(nbPix,2);
end

if this.GeometryType == cl_image.indGeometryType('LatLong')
    d = linspace(0, longueur_m, nbPix);
else
    d = linspace(0, longueur, nbPix);
end
XPixels = linspace(X(1), X(2), nbPix);
YPixels = linspace(Y(1), Y(2), nbPix);

c = cosd(-azimuth);
s = sind(-azimuth);
X1Centre =  c * X(1) + s * Y(1);
Y1Centre = -s * X(1) + c * Y(1);
X2Centre =  c * X(2) + s * Y(2);
Y2Centre = -s * X(2) + c * Y(2);
%{
figure;
subplot(1,3,1); plot([X(1) X(2)], [Y(1) Y(2)], '*'); grid on; axis equal;
subplot(1,3,2); plot([X1Centre X2Centre], [Y1Centre Y2Centre], '*'); grid on; axis equal;
%}

DeltaX = sqrt((c * get(this, 'XStep'))^2 + ( s * get(this, 'YStep'))^2);

NbVoisins = NbVoisins * 2;
DeltaX = DeltaX / 2;

for k=(2*NbVoisins+1):-1:1
    dx = (k - NbVoisins - 1) * DeltaX;
    
    XP1(k) = c * (X1Centre + dx) - s * Y1Centre;
    YP1(k) = s * (X1Centre + dx) + c * Y1Centre;
    XP2(k) = c * (X2Centre + dx) - s * Y2Centre;
    YP2(k) = s * (X2Centre + dx) + c * Y2Centre;
    
%     figure(63554); plot([XP1(k) ; XP2(k)], [YP1(k) ; YP2(k)]); grid on; hold on;
    
    V = get_profile_xy_line(this, [XP1(k) ; XP2(k)], [YP1(k) ; YP2(k)], nbPix, x, y);
    if isempty(V)
        return
    end
    if size(V,1) > 1
        V = mean(V, 'omitnan');
    end
    val(k,:) = V;
end
val = mean(val, 1, 'omitnan');
Cadre.X = [XP1(1) XP1(end) XP2(end) XP2(1) XP1(1)];
Cadre.Y = [YP1(1) YP1(end) YP2(end) YP2(1) YP1(1)];
% subplot(1,3,3); plot([XP1 XP2], [YP1 YP2], '*'); grid on; axis equal;
% val = get_profile_xy_line(this, X, Y, nbPix, x, y);

%{
figure;
subplot(1,3,1); plot([X(1) X(2)], [Y(1) Y(2)], '*'); grid on; axis equal;
subplot(1,3,2); plot([X1Centre X2Centre], [Y1Centre Y2Centre], '*'); grid on; axis equal;
subplot(1,3,3); plot([XP1 XP2], [YP1 YP2], '*'); grid on; axis equal;

figure;
plot([XP1 XP2], [YP1 YP2], '-*'); grid on; axis equal; hold on
%}



function val = get_profile_xy_line(this, X, Y, nbPix, x, y)

xi = linspace(X(1), X(2), nbPix);
yi = linspace(Y(1), Y(2), nbPix);

% subx = find((x >= min(xi)) & (x <= max(xi)));

deltax2 = abs(mean(diff(x))/2);
subx = find((x >= (min(xi)-deltax2)) & (x <= (max(xi)+deltax2)));
deltay2 = abs(mean(diff(y))/2);
suby = find((y >= (min(yi)-deltay2)) & (y <= (max(yi)+deltay2)));

%{
figure; plot(xi, yi, '.-'); grid on; hold on;
plot([min(xi)-deltax2 min(xi)+deltax2 max(xi)+deltax2 max(xi)-deltax2 min(xi)-deltax2], ...
     [min(yi)-deltay2 max(yi)-deltay2 max(yi)+deltay2 min(yi)+deltay2 min(yi)-deltay2]);
%}

if isempty(subx) || isempty(suby)
    val = [];
    return
end

subx = (subx(1)-1):(subx(end)+1);
suby = (suby(1)-1):(suby(end)+1);

subx((subx < 1) | (subx > length(x))) = [];
suby((suby < 1) | (suby > length(y))) = [];

% figure(56565656); plot(x(subx), y(subx), '.'); grid on; hold on;

nbCan = size(this.Image, 3);
val = NaN(nbCan,length(xi));
for k=1:nbCan
    if ~(isa(this.Image(1), 'single') || isa(this.Image(1), 'double'))
        I = single(this.Image(suby,subx,k));
    else
        I = this.Image(suby,subx,k);
    end
    
%   val(k,:) = interp2(x(subx), y(suby), I, xi, yi); %#ok

% Modif JMA le 25/04/2019 car l'interpolation ne fonctionnait pas bien sur
% les profils DECCA de Marc Roche mais finalement le interp2 avec m�thode
% "nearest" donne le m�me r�sultat
%{
    x2 = interp1(x(subx), 1:length(subx), xi);
    y2 = interp1(y(suby), 1:length(suby), yi);
    x2 = round(x2);
    y2 = round(y2);
    [n1, n2] = size(I);
    subOK = find((x2 >= 1) & (x2 <= n2) & (y2 >= 1) & (y2 <= n1));
    sub2 = sub2ind(size(I), y2(subOK), x2(subOK));
    val(k,(subOK)) = I(sub2);
    %}
    
    val(k,:) = interp2(x(subx), y(suby), I, xi, yi, 'nearest');
    
%     figure(5646); imagesc(x(subx), y(suby), I); axis xy; colormap(jet(256)); colorbar; hold on; plot(xi, yi, '*'); hold off; drawnow
end
