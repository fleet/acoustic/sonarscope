% Retourne les valeurs de x comprises dans l'intervalle XLim
%
% Syntax
%   x = get_subx_suby(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   x : Valeurs de x comprises dans l'intervalle XLim
%
% Examples
%   a = cl_image('Image', Lena)
%   x = get_subx_suby(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function x = get_subx_suby(this)

x    = this.x;
XLim = this.XLim;
% sub = find((x >= XLim(1)) & (x <= XLim(2)));
sub = ((x >= XLim(1)) & (x <= XLim(2)));
x = x(sub);
