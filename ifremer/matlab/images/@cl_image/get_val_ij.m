% Retourne les valeurs de l'image specifiées par les coordonnées ic, il
%
% Syntax
%   val = get_val_ij(a, il, ic)
% 
% Input Arguments
%   a : instance de cl_image
%   il : numeros des lignes
%   ic : numeros des colonnes
%
% Output Arguments
%   val : valeurs des pixels
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, x, y] = get_val_ij(this, il, ic)

if isempty(il)
    il = 1:this.nbRows;
end
if isempty(ic)
    ic = 1:this.nbColumns;
end
val = this.Image(il, ic, :);
x = this.x(ic);
y = this.y(il);
