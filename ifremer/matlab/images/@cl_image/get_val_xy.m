% Retourne les valeurs de l'image specifiées par les coordonnées (x,y)
%
% Syntax
%   [val, ix, iy] = get_val_xy(a, x, y)
%
% Input Arguments
%   a : instance de cl_image
%   x : Abscisses
%   y : Ordonnees
%
% Output Arguments
%   val : Valeurs des pixels
%   ix  : Numero de colonne correspondant
%   iy  : Numero de ligne correspondant
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, ix, iy] = get_val_xy(this, valX, valY)

x   = this.x;
y   = this.y;

x = linspace(x(1), x(end), length(x));
y = linspace(y(1), y(end), length(y));

ix  = interp1(x, 1:length(x), valX, 'nearest', 'extrap');
iy  = interp1(y, 1:length(y), valY, 'nearest', 'extrap');

if this.nbSlides == 1
    val = this.Image(iy, ix);
else
    sz = size(this.Image);
    I = this.Image(iy, ix, :);
    if ~isa(I, 'double')
        I = single(I);
    end
    if length(sz) == 3
        I = rgb2ntsc(I);
        val = I(:,:,1);
    else
        val = I; 
    end
end
