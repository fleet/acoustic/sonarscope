% Retourne les valeurs de l'image specifi�es par les coordonn�es (x,y)
%
% Syntax
%   [val, ix, iy] = get_val_xy_1D(a, x, y)
%
% Input Arguments
%   a : instance de cl_image
%   x : Abscisses
%   y : Ordonnees
%
% Output Arguments
%   val : Valeurs des pixels
%   ix  : Numero de colonne correspondant
%   iy  : Numero de ligne correspondant
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, ix, iy] = get_val_xy_1D(this, valX, valY, varargin)

sz = size(valX);
if ~isequal(sz, size(valY))
    dbstack
    str1 = 'Message pour JMA : get_val_xy_1D non appel� convenablement. Envoyez le message de la console SVP';
    str2 = 'Message for JMA : get_val_xy_1D not called properly, please send the message delivered in the command tool.';
    my_warndlg(Lang(str1,str2), 1);
    val = [];
    ix  = [];
    iy  = [];
    return
end

x = this.x;
y = this.y;

x = linspace(x(1), x(end), length(x));
y = linspace(y(1), y(end), length(y));

% ix2 = interp1(x, 1:length(x), valX, 'nearest', 'extrap');
% iy2 = interp1(y, 1:length(y), valY, 'nearest', 'extrap');
ix1 = interp1(x, 1:length(x), valX, 'nearest');
iy1 = interp1(y, 1:length(y), valY, 'nearest');

IND = sub2ind([this.nbRows this.nbColumns], iy1, ix1);
val = NaN(size(valX), 'single');
  
subNonNaN = find(~isnan(IND));
val(subNonNaN) = this.Image(IND(subNonNaN));
