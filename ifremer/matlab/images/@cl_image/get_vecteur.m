% Liste des vecteurs lies a l'image
%
% Syntax
%   liste = get_vecteur(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Output Arguments
%   liste : Liste des noms des vecteurs lies a l'image
%
% Examples
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function s = get_vecteur(this, nomVecteur, varargin)

[varargin, suby]    = getPropertyValue(varargin, 'suby',    1:this.nbRows);
[varargin, ijCross] = getPropertyValue(varargin, 'ijCross', [this.nbRows this.nbColumns]/2);

if ~isempty(ijCross)
    [ix, iy] = xy2ij(this, ijCross(1), ijCross(2));
end

switch nomVecteur
    case 'Horizontal Profile'
        [varargin, subx] = getPropertyValue(varargin, 'suby', 1:this.nbColumns); %#ok<ASGLU>
        s = this.Image(iy, subx);
    case 'Vertical profile'
        s = this.Image(suby, ix);
    case 'Time'
        s = this.Sonar.Time(suby);
    case 'Datetime'
        t = this.Sonar.Time.timeMat;
        s = datetime(t(suby), 'ConvertFrom', 'datenum');
    case 'Immersion'
        s = this.Sonar.Immersion(suby);
    case 'Height'
        s = this.Sonar.Height(suby);
    case 'Heading'
        s = this.Sonar.Heading(suby);
    case 'Speed'
        s = this.Sonar.Speed(suby);
    case 'Roll'
        s = this.Sonar.Roll(suby);
    case 'Pitch'
        s = this.Sonar.Pitch(suby);
    case 'Yaw'
        s = this.Sonar.Yaw(suby);
    case 'SurfaceSoundSpeed'
        s = this.Sonar.SurfaceSoundSpeed(suby);
    case 'FishLatitude'
        s = this.Sonar.FishLatitude(suby);
    case 'FishLongitude'
        s = this.Sonar.FishLongitude(suby);
    case 'ShipLatitude'
        s = this.Sonar.ShipLatitude(suby);
    case 'ShipLongitude'
        s = this.Sonar.ShipLongitude(suby);
    case 'PortMode_1'
        s = this.Sonar.PortMode_1(suby);
    case 'Heave'
        s = this.Sonar.Heave(suby);
    case 'Tide'
        s = this.Sonar.Tide(suby);
    case 'PingCounter'
        s = this.Sonar.PingCounter(suby);
    case 'SampleFrequency'
        s = this.Sonar.SampleFrequency(suby);
    case 'SonarFrequency'
        s = this.Sonar.SonarFrequency(suby);
    case {'SoanrCableOut'; 'CableOut'}
        if isempty(this.Sonar.CableOut)
            s = zeros(length(suby), 1, 'single'); % En attendant de trouver pourquoi cette info a disparu
        else
            s = this.Sonar.CableOut(suby);
        end
    case 'Interlacing'
        s = this.Sonar.Interlacing(suby);
    case 'BSN'
        s = this.Sonar.BSN(suby);
    case 'BSO'
        s = this.Sonar.BSO(suby);
    case 'BSN-BSO'
        s = this.Sonar.BSN(suby) - this.Sonar.BSO(suby);
        
    case 'PresenceWC'
        s = this.Sonar.PresenceWC(suby);
    case 'PresenceFM'
        s = this.Sonar.PresenceFM(suby);
        
    case 'DistPings'
        s = this.Sonar.DistPings(suby);
    case 'NbSwaths'
        s = this.Sonar.NbSwaths(suby);
    case 'TxAlongAngle'
        s = this.Sonar.TxAlongAngle(suby);
    case 'TxAcrossAngle'
        s = this.Sonar.TxAcrossAngle(suby);
    case 'TrueHeave'
        s = this.Sonar.TrueHeave(suby);
    case 'Draught'
        s = this.Sonar.Draught(suby);
    case 'TVGN'
        s = this.Sonar.TVGN(suby);
    case 'TVGCrossOver'
        s = this.Sonar.TVGCrossOver(suby);
    case 'TxBeamWidth'
        s = this.Sonar.TxBeamWidth(suby);
    case 'ResolAcrossSample'
        s = this.Sonar.ResolAcrossSample(suby);
        
    otherwise
        disp('cl_image/get_vecteur : On ne devrait pas passer par l�')
        s = [];
end

s = s(:);
if strcmp(nomVecteur, 'Horizontal Profile')
    s = s';
end
