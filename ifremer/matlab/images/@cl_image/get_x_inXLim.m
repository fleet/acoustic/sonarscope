% Retourne les valeurs de x comprises dans l'intervalle XLim
%
% Syntax
%   [x, sub] = get_x_inXLim(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   x   : Valeurs de x comprises dans l'intervalle XLim
%   sub : Indices correspondants
%
% Examples
%   a = cl_image('Image', Lena)
%   x = get_x_inXLim(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [x, sub] = get_x_inXLim(this)

XLim(1) = min(this.XLim);
XLim(2) = max(this.XLim);

x = this.x;
sub = find((x >= XLim(1)) & (x <= XLim(2)));
x = x(sub);
