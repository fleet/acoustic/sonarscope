% Retourne les valeurs de y comprises dans l'intervalle YLim
%
% Syntax
%   [y, sub] = get_y_inYLim(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   y : Valeurs de y comprises dans l'intervalle YLim
%   sub : Indices correspondants
%
% Examples
%   a = cl_image('Image', Lena)
%   y = get_y_inYLim(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [y, sub] = get_y_inYLim(this, varargin)

YLim(1) = min(this.YLim);
YLim(2) = max(this.YLim);

y = this.y;
sub = find((y >= YLim(1)) & (y <= YLim(2)));
y = y(sub);
