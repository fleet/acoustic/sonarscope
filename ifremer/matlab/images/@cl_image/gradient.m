% Gradient of an image according to an azimut
%
% Syntax
%   b = gradient(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Azimuth : Azimut (North = 0, East = 90)
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = cl_image.import_ermapper(nomFic);
%   imagesc(a)
%
%   b = gradient(a);
%   imagesc(b)
%
% See also cl_image/ombrage Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = gradient(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('gradient'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = gradient_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = gradient_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Azimuth] = getPropertyValue(varargin, 'Azimuth', this.Azimuth);
[varargin, window]  = getPropertyValue(varargin, 'window',  [3 3]); %#ok<ASGLU>

%% Algorithm

yDir = ((this.y(end) - this.y(1)) > 0);
if ~xor(yDir , (this.YDir == 1))
    Angle = 180 + Azimuth;
else
    Angle = -Azimuth;
end

XStepMetric = this.XStepMetric;
if isnan(XStepMetric)
    XStepMetric = 1;
end

YStepMetric = this.YStepMetric;
if isnan(YStepMetric)
    YStepMetric = 1;
end

stepx = (length(subx) - 1) / (subx(end) - subx(1));
suby = sort(suby);
stepy = (length(suby) - 1) / (suby(end) - suby(1));

angle = mod(Angle + 360, 360) * (pi/180);
alpha = sin(angle);
beta  = cos(angle);

h = [-1 -2 -1; 0 0 0; 1 2 1];
h = h / sum(abs(h(:)));
hx = h' / (XStepMetric / stepx);
hy = h  / (YStepMetric / stepy);

[I, subNaN] = windowEnlarge(this.Image(suby,subx), [3 3], []);

switch alpha
    case 1
        I = filter2(hx, I, 'valid');
    case -1
        I = -filter2(hx, I, 'valid');
    case 0
        I = beta * filter2(hy, I, 'valid');
    otherwise
        Ix = filter2(hx, I, 'valid');
        Iy = filter2(hy, I, 'valid');
        I = alpha * Ix + beta * Iy;
end

I(subNaN) = NaN;
Image(:,:) = I;

%% Output image

Unit = [this.Unit ' / ' this.XUnit];
Azimuth  = this.Azimuth;
Parameters = getHistoryParameters(Azimuth, window);
that = inherit(this, Image, 'subx',  subx, 'suby', suby, 'AppendName', 'gradient', 'ValNaN', NaN, ...
    'ColormapIndex', 2, 'Unit', Unit, 'Parameters', Parameters);
