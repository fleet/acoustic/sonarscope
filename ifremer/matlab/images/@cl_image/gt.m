% Logic operator "greater than : >"
%
% Syntax
%   b = a > Val;
%   b = gt(a, Val, ...)
%
% Input Arguments
%   a   : Instance(s) of cl_image
%   val : Threshold
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%     a = cl_image('Image', Lena);
%     imagesc(a);
%   b = gt(a, 100);
%     imagesc(b);
%   b = a > 100;
%     imagesc(b);
%   b = gt(a, 100, 'subx', 20:200, 'suby', 20:40);
%     imagesc(b);
%
% See also cl_image/gt Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = gt(this, Val, varargin)
that = process_function_type2(this, @gt, Val, varargin{:});
