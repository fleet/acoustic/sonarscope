% Creation ou enrichissement d'un layer "Masque" interactivement
%
% Syntax
%   b = hand_correction(a, creationLayer, nomLayer, ...)
%
% Input Arguments
%   a                   : Instance de cl_image
%   creationLayer       : 1=creation d'une image, 0=utilisation d'une image existante
%   nomLayer            : 
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image quantifiee
%
% Examples
%   nomFic = getNomFicDatabase('TexturesSonarMontage02.tif');
%   I = imread(nomFic);
%   a = cl_image('Image', I);
%   [numFig, AxeImage] = imagesc(a);
%   
%   b = hand_correction(a, 1, 'Mon Masque', [], numFig, AxeImage);
%   imagesc(b, 'CLim', [0 2])
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, Value] = hand_correction(this, Value, numFig, AxeImage, varargin)

%% Correctif bug point.png R2008a et R2008b

if isdeployed
    MCR_CACHE_ROOT = getenv('MCR_CACHE_ROOT');
    listeDir = listeDirOnDir(MCR_CACHE_ROOT);
    for k1=1:length(listeDir)
        nomDirCACHE = fullfile(MCR_CACHE_ROOT, listeDir(k1).name);
        listeDirSonarScope = listeDirOnDir(nomDirCACHE);
        for k2=1:length(listeDirSonarScope)
            nomDirCACHESonarScope = fullfile(nomDirCACHE, listeDirSonarScope(k2).name);
            nomDirIcons = fullfile(nomDirCACHESonarScope, 'toolbox', 'images', 'icons');
            if exist(nomDirIcons, 'dir')
                nomFicPointPng = getNomFicDatabase('point.png');
                status = copyfile(nomFicPointPng, nomDirIcons);
                if status
                    fprintf('To correct a Matlab bug I have successfully copied "%s" on "%s"\n', ...
                        nomFicPointPng, nomDirIcons);
                else
                    fprintf('To correct a Matlab bug I have tried to copiy "%s" on "%s but the command failed."\n', ...
                        nomFicPointPng, nomDirIcons);
                end
            end
        end
    end
end

set(0,      'CurrentFigure', numFig)
set(numFig, 'CurrentAxes',   AxeImage);
% hImage = findobj(AxeImage, 'type', 'image');
XLim = get(AxeImage, 'XLim');
YLim = get(AxeImage, 'YLim');

subx = find((this.x >= XLim(1)) & (this.x <= XLim(2)));
suby = find((this.y >= YLim(1)) & (this.y <= YLim(2)));

GCF = get(AxeImage, 'Parent');

KeyPressFcn         = get(GCF, 'KeyPressFcn');
WindowButtonUpFcn   = get(GCF, 'WindowButtonUpFcn');
WindowButtonDownFcn = get(GCF, 'WindowButtonDownFcn');

set(GCF, 'KeyPressFcn',         '');
set(GCF, 'WindowButtonUpFcn',   '');
set(GCF, 'WindowButtonDownFcn', '');

[maskScreen, pX, pY] = roipoly;

% TODO GLU : essayer de brancher ces 3 lignes (en commentant le isempty(maskScreen))
% roi = drawpolygon; % Modif JMA le 29/07/2021
% pX = roi.Position(:,1);
% pY = roi.Position(:,2);

pause(1)

set(GCF, 'KeyPressFcn',         KeyPressFcn)
set(GCF, 'WindowButtonUpFcn',   WindowButtonUpFcn)
set(GCF, 'WindowButtonDownFcn', WindowButtonDownFcn)

if isempty(maskScreen)
    [rep, flag] = my_questdlg(Lang('Oups, que voulez-vous faire ?', 'Oups, what do you want to do ?'), ...
        Lang('Seulement cette r�gion', 'Only this region'), Lang('Toute l''op�ration', 'All the operation'));
    if ~flag || (rep == 2)
        return
    end
end

hold on;
plot(pX, pY, 'k');

%% Saisie de la valeur

[flag, Value] = paramsHandCorrection(this, Value);
if ~flag
    return
end

maskImage = roipoly(this.x(subx), this.y(suby), this.Image(suby,subx,1), pX, pY);
maskImage = uint8(maskImage);

%% Correction de l'image

for k=1:size(this.Image, 3)
    Image = this.Image(suby,subx,k);
    Image(maskImage == 1) = Value;
    this.Image(suby,subx,k) = Image;
end
