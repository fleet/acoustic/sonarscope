% Affichage des histogrammes des images
%
% Syntax
%   histo(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   CLim : Bornes du rehaussement de contraste (occulte les valeurs de CLim des instances)
%   Fig  : Numero de figure
%   Axe  : Handle(s) de l'axe
%   Tag  : Tag lie aux images (valeur aleatoire par defaut).
%
% Remarks : Si a est un tableau d'instance, toutes les images contenues
% dans les instances sont visualisees. Si on specifie un numero de figure,
% les images seront visualisees dans differents axes de la meme figure
% sinon, uelles seronts tracees dans des figures differentes.
% Si des handles d'axes sont fournis, le trace des images se fera dans les
% axes definis qui peuvent provenir s'une meme figure ou ade figures
% differentes.
% histo met en place un asservissement des zooms pour les differentes
% instances a l'aide d'un Tag unique, seuls les images issues d'un meme
% histo seront donc asservis. Si on veut pouvoir imposer une
% asservissement avec des images provenant d'un autre histo, il faut a ce
% moment la imposer le meme Tag
%
% Examples
%   a = cl_image('Image', Lena);
%   histo(a)
%
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   histo(a)
%   histo(a, 'Fig', 1000)
%
%   [I, label] = ImageSonar(2);
%   a(2) = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur', ...
%                     'ColormapIndex', 2)
%   a(2).Video = 2
%
%   histo(a)
%   histo(a, 'Fig', 1000)
%   figure; h(1) = subplot(1,2,1); h(2) = subplot(1,2,2);
%   histo(a, 'Axe', h)
%
%   figure; h(1) = subplot(2,2,1); h(2) = subplot(2,2,2); h(3) = subplot(2,2,3); h(4) = subplot(2,2,4);
%   imagesc(a, 'Axe', h(1:2), 'XLim', [20 80], 'YLim', [40 90])
%   histo(a, 'Axe', h(3:4))
%
% See also cl_image cl_image/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function histo(this, varargin)

%% Recup�ration de PrepertyValues locales

[varargin, Fig]       = getPropertyValue(varargin, 'Fig', []);
[varargin, Axe]       = getPropertyValue(varargin, 'Axe', []);
[varargin, CLimLocal] = getPropertyValue(varargin, 'CLim', []);
[varargin, Tag]       = getPropertyValue(varargin, 'Tag', []);

% TODO : faire plut�t le contraire. Rechercher pourquoi j'avais mis en
% place le bouton "Sunchronisation"
[varargin, flagSynchronisation] = getPropertyValue(varargin, 'Synchronisation', 1); %#ok<ASGLU>

%% D�finition d'un Tag unique pour les images de l'instance

if isempty(Tag)
    [~, Tag] = fileparts(my_tempname);
end

%% Boucle sur les images

nbImages = numel(this);
for iImage=1:nbImages
    a = this(iImage);
    
    if isempty(a.Image)
        continue
    end
    
    if isempty(Axe)             % Cas ou des handles d'axes ne sont pas definis
        if isempty(Fig)         % Cas ou il n'y a pas de numero de figure defini
            if isempty(a.Name)
                FigUtils.createSScFigure;
            else
                FigUtils.createSScFigure('NumberTitle', 'off', 'Name', a.Name)
            end
        else
            FigUtils.createSScFigure(Fig);         % Cas o� il y a un num�ro de figure d�fini
            % Creation des axes
            if length(a.x) < length(a.y)
                subplot(1, nbImages, iImage)
            else
                subplot(nbImages, 1, iImage)
            end
        end
    else % Cas ou des handles d'axes sont definis
        if length(Axe) == nbImages
            if ishandle(Axe(iImage))
                if isAGraphicElement(Axe(iImage), 'axes')
                    axes(Axe(iImage));
                else
                    my_warndlg('cl_image:histo Le handle passe n''est pas valable', 0);
                    return
                end
            else
                my_warndlg('cl_image:histo Le handle passe n''est pas valable', 0);
                return
            end
        else
            my_warndlg('cl_image:histo Il n''y a pas autant d''axes que d''images', 0);
            axes(Axe(1));
        end
    end
    
    % ---------------------------------------------------------------
    % Recuperation de rehaussement de contaste defini au niveau de la
    % commande "histo"
    
    if isempty(CLimLocal)
        CLim = a.CLim;
    else
        CLim = CLimLocal;
    end
    
    % ------------------------
    % Visualisation de l'image
    
    switch a.nbSlides
        case 1
            % Cas d'une image en pseudo-couleur
            sub = find(a.HistoCentralClasses >= CLim(1) & a.HistoCentralClasses <= CLim(2));
            plot(a.HistoCentralClasses, a.HistoValues, 'Color', [0.7 0.7 0.7]); hold on; grid on;
            hImage = plot(a.HistoCentralClasses(sub), a.HistoValues(sub)); hold on; grid on;
        case 3
            sub = find(a.HistoCentralClasses >= CLim(1) & a.HistoCentralClasses <= CLim(2));
            plot(a.HistoCentralClasses, a.HistoValues, 'Color', [0.7 0.7 0.7]); hold on; grid on;
            plot(a.HistoCentralClasses(sub), a.HistoValues(sub)); hold on; grid on;
            close
            return
        otherwise
            my_warndlg('cl_image:histo On ne represente ques des images en pseudo-couleur ou en vraies couleurs', 0);
    end
    
    xlabel(['Valeurs (' a.Unit ')'])
    ylabel('Histo')
    title(a.Name, 'Interpreter', 'None')
    
    % ---------------------------------------------
    % Synchonisation des zoom par methode archaique
    % Il serait judicieux d'utiliser cl_zoom
    
    hAxeImage = get(hImage, 'Parent');
    str = Tag;
    set(hAxeImage, 'Tag', str)
    commandeX = sprintf('set(findobj(0, ''Tag'',''%s''), ''XLim'', get(gca, ''XLim''))', str);
    
    % Si il y a plus d'une image, on cree un bouton qui permet de
    % lancer la synchronisation des autres images par rapport a l'image
    % courante
    
    Synchronisation = findobj(gcf, 'Tag', 'Synchronisation');
    if isempty(Synchronisation) && flagSynchronisation
        uicontrol('Parent', gcf, ...
            'Style','pushbutton', ...
            'String', 'Syncho axes', ...
            'Position', [20 10 100 20], ...
            'TooltipString', 'Synchonisation zoom par rapport a l''axe courant', ...
            'Tag', 'Synchronisation', ...
            'CallBack', commandeX);
    end
    
    %% On donne le mom du tag dans la barre de menu
    
    %set(gcf, 'Name', Tag)
    % {
    %     numFig(1) = 0;
    numFig(iImage+1) = gcf; %#ok
    if numFig(iImage+1) ~= numFig(iImage)
        h = findobj(gcf, 'Tag', 'TagDuTag');
        if isempty(h)
            uimenu('Text', ['Tag=' Tag], 'Tag', 'TagDuTag');
        else
            set(h, 'Text', ['Tag=' Tag]);
        end
    end
    % }
end

