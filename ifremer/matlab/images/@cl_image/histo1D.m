function histo1D(this)

TypeHisto = 'Histo';

Name    = this.Name;
ValNaN   = this.ValNaN;
DataType = this.DataType;
I        = this.Image(:,:,:);
bins     = this.HistoCentralClasses;

XLabel = [cl_image.strDataType{DataType} ' (' this.Unit ')'];
Fig = figure;
histo1D(I, bins, TypeHisto, 'ValNaN', ValNaN, 'Titre', Name, 'XLabel', XLabel, 'Fig', Fig);
