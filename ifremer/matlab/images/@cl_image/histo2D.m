% Calcul de l'histogramme entre 2 images
%
% Syntax
%   c = histo2D(a, b, ...)
%
% Input Arguments
%   a : Instance de cl_image
%   b : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   c : Instance de cl_image contenant l'histogramme bidim
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : sortir [flag, this]
function this = histo2D(a, b, varargin)

N = length(a);
str1 = 'Calcul des histogrammes 2D en cours';
str2 = 'Processing 2D histograms';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, that] = histo2D_unitaire(a(k), b(k), varargin{:});
    if flag
        this(k) = that; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')


function [flag, this] = histo2D_unitaire(a, b, varargin)

[subxa, subya, varargin] = getSubxSuby(a, varargin);

[varargin, binsI1]     = getPropertyValue(varargin, 'binsI1',     a.HistoCentralClasses);
[varargin, binsI2]     = getPropertyValue(varargin, 'binsI2',     b.HistoCentralClasses);
[varargin, Mask]       = getPropertyValue(varargin, 'Mask',       []);
[varargin, MaskValues] = getPropertyValue(varargin, 'MaskValues', []); %#ok<ASGLU>

this = [];
flag = 0;

if (length(a) ~= 1) || (length(b) ~= 1)
    return
end

if (a.nbSlides ~= 1) || (b.nbSlides ~= 1)
    str1 = 'Traitement impossible car l''une des images a plusieurs canaux.';
    str2 = 'Unable to process, one of the images has several slides.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Recherche de la partie commune des images

if isempty(Mask)
    [~, ~, subxa, subya, subxb, subyb] = intersectionImages(a, subxa, subya, b, 'SameSize');
else
    [~, ~, subxa, subya, subxb, subyb, subxm, subym] = intersectionImages(a, subxa, subya, b, Mask, 'SameSize');
end

%% Appel de la fonction de calcul de l'histo2D

Imagea = a.Image(subya,subxa);
Imageb = b.Image(subyb,subxb);
if ~isempty(Mask)
    m = Mask.Image(subym, subxm);
    M = true(length(subym), length(subxm));
    for k=1:length(MaskValues)
        M(m == MaskValues(k)) = false;
    end
    Imagea(M) = NaN;
    Imageb(M) = NaN;
end
N = histo2D(Imagea, Imageb, binsI1, binsI2);

this = cl_image('Image', N', 'Name', 'Hiso2D', 'x', binsI2, 'y', binsI1, 'ValNaN', NaN, 'ColormapIndex', 3);

%% Calcul des statistiques

this = compute_stats(this);

%% Complétion du titre

this.GeometryType = 1;
this.TagSynchroX  = get_DataTypeName(b);
this.TagSynchroY  = get_DataTypeName(a);
this.XLabel       = get_DataTypeName(b);
this.YLabel       = get_DataTypeName(a);
this.XUnit        = b.Unit;
this.YUnit        = a.Unit;
this.Unit         = 'Num';
this.DataType     = cl_image.indDataType('Histo2D');

ImageName = extract_ImageName(a);
Append    = get_DataTypeName(b);
this      = update_Name(this, 'Name', ImageName, 'Append', Append);
flag = 1;
