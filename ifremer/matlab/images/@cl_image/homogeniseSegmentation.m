% Homogeneisation des regions de la segmentation
% Smooths the contours of a segmentation
%
% Syntax
%  b = homogeniseSegmentation(a)
%
% Input Arguments
%   a      : Instance(s) of cl_image
%   Radius : Radius of the structurent element
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance(s) of cl_image
%
% Examples
%   a = TODO : prendre une segmentation sur une image sonar
%   imagesc(a);
%
%   b = homogeniseSegmentation(a);
%   imagesc(b);
%
% See also cl_image/smoothContours Authors
% Authors : MM
% ----------------------------------------------------------------------------

function [flag, that] = homogeniseSegmentation(this, Radius, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('homogeniseSegmentation'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K] = homogeniseSegmentation_unit(this(k), Radius, varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = homogeniseSegmentation_unit(this, Radius, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

that = [];

%% Control

ident = cl_image.indDataType('Segmentation');
flag  = testSignature(this, 'DataType', ident);
if ~flag
    return
end

%% Algorithm

se  = strel('square', Radius);
I   = this.Image(suby,subx,:);
I = uint8(I);
I   = imclose(I, se);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'homogeniseSegmentation');
