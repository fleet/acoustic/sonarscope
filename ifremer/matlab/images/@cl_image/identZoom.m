function [nomZoom, iZoneEtude] = identZoom(this, XYLimZoom, Tag)

iZoneEtude = [];
pasx = abs(this.x(2) - this.x(1));
pasy = abs(this.y(2) - this.y(1));

w = axis;
XLimImage = w(1:2);
YLimImage = w(3:4);

% La visualisation ne correspond pas a l'image entiere (Pas de zoom)
%     if isequal(this.visuLim(this.indImage, :), [x(1) x(end) y(1) y(end)])
if isequal(XYLimZoom, [XLimImage YLimImage])
    nomZoom = 'Image entiere';
else
    nomZoom = 'Image zoomee';
end

% TODO : je ne sais plus pourquoi ceci, c'est �trange d'aller chercher des zones d'int�r�t dans les courbes. Ce truc doit �tre revu � t�te repos�e.
for k=1:length(this.CourbesStatistiques)
    if isfield(this.CourbesStatistiques(k), 'x') && isfield(this.CourbesStatistiques(k), 'y') && ...
        ~isempty(this.CourbesStatistiques(k).x) && ~isempty(this.CourbesStatistiques(k).y)
%         pppp = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude;
        XLimZoneEtude(1) = min(this.CourbesStatistiques(k).x);
        XLimZoneEtude(2) = max(this.CourbesStatistiques(k).x);
        YLimZoneEtude(1) = min(this.CourbesStatistiques(k).y);
        YLimZoneEtude(2) = max(this.CourbesStatistiques(k).y);
        flag = compareZoneEtude_Zoom(XLimZoneEtude, YLimZoneEtude, XLimImage, YLimImage, pasx*2, pasy*2);    % *2 car bug
        if flag % L'image est positionnee sur la zone d'etude k
            nomZoom = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude;
            if isempty(Tag)
                iZoneEtude = k;
            else
                if strcmp(this.CourbesStatistiques(k).bilan{1}(1).Tag, Tag)
                    iZoneEtude = k;
                end
            end
        end
    end
end
