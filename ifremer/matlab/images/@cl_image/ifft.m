% Inverse Fourier transform
%
% Syntax
%   b = ifft(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, ...
%                'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   imagesc(a);
%
%   b = fft(a);
%   imagesc(b);
%
%   c = fftFilterDisk(b, [0.05 0.1 0], 1);
%   imagesc(c);
%
%   d = ifft(c);
%   imagesc(d);
%
% See also cl_image/fft cl_image/fftFilterDisk Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = ifft(this)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('ifft'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = ifft_unit(this(k));
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function this = ifft_unit(this)

%% Algorithm

I = this.Image(:,:);
I(isnan(I)) = 0;
I = ifft2(ifftshift(I));
Image(:,:) = single(I);

%% Output image

% that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'ifft');
% % TODO : prendre son temps pour faire cela

this = replace_Image(this, Image);
this.ValNaN = NaN;

%% Calcul des statistiques

this = compute_stats(this);

%% Mise � jour de coordonn�es

this.x                  = this.SaveParamsIfFFT.x;
this.y                  = this.SaveParamsIfFFT.y;
this.XUnit              = this.SaveParamsIfFFT.XUnit;
this.YUnit              = this.SaveParamsIfFFT.YUnit;
this.ColormapIndex      = this.SaveParamsIfFFT.ColormapIndex;
this.Colormap           = this.SaveParamsIfFFT.Colormap;
this.TagSynchroX        = this.SaveParamsIfFFT.TagSynchroX;
this.TagSynchroY        = this.SaveParamsIfFFT.TagSynchroY;
this.TagSynchroContrast = num2str(rand(1));
this.SpectralStatus     = 1;

%% Mise � jour des bornes en x et y

this.XLim = compute_XYLim(this.x);
this.YLim = compute_XYLim(this.y);

%% Rehaussement de contraste

CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'ifft');
