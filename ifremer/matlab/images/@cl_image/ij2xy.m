% Transformation des coordonnees (i,j) en (x, y)
%
% Syntax
%   [x, y] = ij2xy(this, ix, iy)
%
% Input Arguments
%   a : instance de cl_image
%   ix : Numero de colonne correspondant
%   iy : Numero de ligne correspondant
%
% Output Arguments
%   x : Abscisses
%   y : Ordonnees
%
% See also cl_image cl_image/ij2xy Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [x, y] = ij2xy(this, ix, iy)

x  = interp1(1:length(this.x), this.x, ix, 'nearest', 'extrap');
y  = interp1(1:length(this.y), this.y, iy, 'nearest', 'extrap');
