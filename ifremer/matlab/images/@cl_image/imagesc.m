% Affichage des images
%
% Syntax
%   [Fig, Axe] = imagesc(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   CLim : Bornes du rehaussement de contraste (occulte les valeurs de CLim des instances)
%   XLim : Bornes de l'image en X (occulte les valeurs de XLim des instances)
%   YLim : Bornes de l'image en Y (occulte les valeurs de YLim des instances)
%   Fig  : Numero de figure
%   Axe  : Handle(s) de l'axe
%   ColormapIndex : Red�finit la table de couleur
%   Video : Red�finit sens de la table de couleur
%
% Remarks : Si a est un tableau d'instance, toutes les images contenues
% dans les instances sont visualisees. Si on specifie un numero de figure,
% les images seront visualisees dans differents axes de la meme figure
% sinon, uelles seronts tracees dans des figures differentes.
% Si des handles d'axes sont fournis, le trace des images se fera dans les
% axes definis qui peuvent provenir s'une meme figure ou ade figures
% differentes.
% imagesc met en place un asservissement des zooms pour les differentes
% instances a l'aide d'un Tag unique, seuls les images issues d'un meme
% imagesc seront donc asservis. Si on veut pouvoir imposer une
% asservissement avec des images provenant d'un autre imagesc, il faut a ce
% moment la imposer le meme Tag
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   imagesc(a)
%   imagesc(a, 'Fig', 1000)
%
%   [I, label] = ImageSonar(2);
%   a(2) = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur', 'ColormapIndex', 2)
%   a(2) = set(a(2), 'Video', 2)
%
%   imagesc(a)
%   imagesc(a, 'Fig', 1000)
%   figure; h(1) = subplot(1,2,1); h(2) = subplot(1,2,2);
%   imagesc(a, 'Axe', h)
%   imagesc(a, 'Axe', h, 'XLim', [20 80], 'YLim', [40 90])
%
%   figure; h(1) = subplot(2,2,1); h(2) = subplot(2,2,2); h(3) = subplot(2,2,3); h(4) = subplot(2,2,4);
%   imagesc(a, 'Axe', h(1:2), 'XLim', [20 80], 'YLim', [40 90])
%   histo(a, 'Axe', h(3:4))
%
%   b = ind2rgb(a)
%   imagesc(b, 'Fig', 1000)
%
% See also cl_image cl_image/set cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : Calling AXES(h) without an output in a loop can be very slow. Include 'h' in plot function arguments instead.

function [Fig, Axe] = imagesc(this, varargin)

[varargin, Fig]           = getPropertyValue(varargin, 'Fig',           []);
[varargin, Axe]           = getPropertyValue(varargin, 'Axe',           []);
[varargin, CLimLocal]     = getPropertyValue(varargin, 'CLim',          []);
[varargin, Location]      = getPropertyValue(varargin, 'Location',      'EastOutside');
[varargin, Rot90]         = getPropertyValue(varargin, 'Rot90',         0);
[varargin, subx]          = getPropertyValue(varargin, 'subx',          []);
[varargin, suby]          = getPropertyValue(varargin, 'suby',          []);
[varargin, NomY]          = getPropertyValue(varargin, 'NomY',          'y');
[varargin, TypeWindow]    = getPropertyValue(varargin, 'TypeWindow',    []); %  1=imagesc, 2=imagesc_scroll
[varargin, ColormapIndex] = getPropertyValue(varargin, 'ColormapIndex', []);
[varargin, Video]         = getPropertyValue(varargin, 'Video',         []); %#ok<ASGLU>
% [varargin, XLimLocal] = getPropertyValue(varargin, 'XLim', []);
% [varargin, YLimLocal] = getPropertyValue(varargin, 'YLim', []);

flagFig = isempty(Fig);
flagAxe = isempty(Axe);

if isempty(TypeWindow)
    TypeWindow = 1;
end

%% Boucle sur les images

nbImages = numel(this);
for iImage=1:nbImages
    a = this(iImage);
    
    if isempty(subx)
        subxImage = 1:a.nbColumns;
        subyImage = 1:a.nbRows;
    else
        subxImage = subx;
        subyImage = suby;
    end
    
    if isempty(a.Image)
        continue
    end
    
    if ~isempty(ColormapIndex)
        a.ColormapIndex = ColormapIndex;
    end
    if ~isempty(Video)
        a.Video = Video;
    end
    
    if flagAxe             % Cas ou des handles d'axes ne sont pas definis
        if flagFig         % Cas ou il n'y a pas de numero de figure defini
            if isempty(a.Name)
                Fig(iImage) = FigUtils.createSScFigure;
            else
                Fig(iImage) = FigUtils.createSScFigure('NumberTitle', 'off', 'Name', a.Name);
            end
        else
            figure(Fig)         % Cas ou il y a un numero de figure defini
            % Creation des axes
            if length(a.x) < length(a.y)
                subplot(1, nbImages, iImage)
            else
                subplot(nbImages, 1, iImage)
            end
        end
    else % Cas ou des handles d'axes sont definis
        if length(Axe) == nbImages
            if ishandle(Axe(iImage))
                if isAGraphicElement(Axe(iImage), 'axes')
                    axes(Axe(iImage));
                else
                    my_warndlg('cl_image:imagesc : Le handle passe n''est pas valable', 0);
                    return
                end
            else
                my_warndlg('cl_image:imagesc : Le handle passe n''est pas valable', 0);
                return
            end
        else
            %             my_warndlg('Il n''y a pas autant d''axes que d''images', 0);
            axes(Axe(1));
        end
    end
    
    
    %% Visualisation de l'image
    
    switch a.SpectralStatus
        case 1
            I = a.Image(subyImage,subxImage,:);
        case 2
            I = abs(a.Image(subyImage,subxImage,:));
            I(I == 0) = NaN;
            I = 20 * log10(I);
        case 3
            % A faire
    end
    
    %% R�cup�ration de rehaussement de contaste d�fini au niveau de la commande "imagesc"
    
    if isempty(CLimLocal)
        CLim = a.CLim;
        if CLim(1) == CLim(2)
            CLim = [CLim(1)-1 CLim(2)+1];
        end
    else
        if ischar(CLimLocal) % Ex 0.5% 1% 3%
            switch CLimLocal
                case '0.5%'
                    CLim = a.StatValues.Quant_25_75;
                case '1%'
                    CLim = a.StatValues.Quant_01_99;
                case '3%'
                    CLim = a.StatValues.Quant_03_97;
                otherwise
            end
        else
            CLim = CLimLocal;
        end
    end
    
    if a.SymetrieColormap(a.ColormapIndex,1) == 1
        Max = abs(max(CLim));
        %         Min = -abs(min(CLim));
        %         Mean = mean(abs(CLim));
        CLim = [-Max Max];
    end
    
    %% D�fition des coordonn�es
    
    flagY = strcmp(NomY, 'DistanceAlongNavigation');
    if Rot90
        y = a.x(subxImage);
        x = a.y(subyImage);
        if flagY
            % TODO : faire [flag, resol] = getSignal(this, NomY, subyImage);
            [flag, resol] = getDistanceAlongNav(a, subyImage);
            if flag
                flagFliplr = (x(end) < x(1));
                x = (0:(length(x)-1)) * resol;
                a.YLim = [x(1) x(end)];
                if flagFliplr
                    x = fliplr(x);
                end
            end
        end
    else
%         if isempty(a.x)
%             a.x = 1:this.nbColumns;
%         end
        x = a.x(subxImage);
        y = a.y(subyImage);
        if flagY
            [flag, resol] = getDistanceAlongNav(a, subyImage);
            if flag
                flagFliplr = (y(end) < y(1));
                y = (0:(length(y)-1)) * resol;
                a.YLim = [y(1) y(end)];
                if flagFliplr
                    y = fliplr(y);
                end
            end
        end
    end
    
    
    
    % TODO :
    %{
if typeRepresentation == 1
imagesc(x, y, flipud(rot90(I)), CLim);
else
switch typeRepresentationX % TODO : sortir ce test de
la boucle et faire de m�me pour y
case 'x'
case 'DistanceAlongNav'
x =  integration (lat, lon) .....
case 'Time'
x = this.Sonat.Time.timeMat .....
otherwise
n = length(x);
[varargin, x] = getPropertyValue(varargin, 'x', []);
if length(x) ~= n
'beurk'
return
end
pcolor(x, y, double(flipud(rot90(I)))); shading flat; set(gca, 'CLim', CLim);
end
end
    %}
    
    
    %% Affichage de l'image
    
    if ~isreal(I)
        I = abs(I);
    end
    
    if Rot90
        I = my_flipud(my_rot90(I));
    end
    
    switch a.ImageType
        case 1  % Image d'intensit�
            if any(isnan(CLim))
                CLim = [-1 1];
            end
            if CLim(2) <= CLim(1)
                CLim = [CLim(1)-1 CLim(1)+1];
            end
            switch TypeWindow
                case 1
                    imagesc(x, y, I, CLim);
                case 2
                    imagesc_scroll(x, y, I, CLim);
            end
            
            if a.Video == 1
                if a.ColormapIndex == 1
                    cMap = a.ColormapCustom;
                else
                    cMap = a.Colormap;
                end
            else
                cMap = flipud(a.Colormap);
            end
            colormap(gca, cMap)
            h = my_colorbar;
            set(h, 'Location', Location);
            
        case 2  % Image RGB
            MaxI = max(I(:));
            if (isa(I, 'double') || isa(I, 'single'))
                if MaxI > 1
                    I = uint8(I);
                else
                    I = uint8(I * 255);
                end
            end
            %             if a.Video == 2
            %                I = 255 - I;
            %             end
            
            switch TypeWindow
                case 1
                    image(x, y, I);
                case 2
                    imagesc_scroll(x, y, I);
            end
            
        case 3  % Image indexee
            I = uint8(I);
            switch TypeWindow
                case 1
                    image(x, y,  I);
                case 2
                    imagesc_scroll(x, y, I);
            end
            if a.ColormapIndex == 1
                colormap(a.ColormapCustom)
            else
                colormap(a.Colormap)
            end
            colorbar
            %             image(a.x(subxImage), a.y(subyImage), a.Image); colormap(a.ColormapCustom);
            %             imshow(a.x(subxImage), a.y(subyImage), a.Image); colormap(a.Colormap);
            
        case 4  % Image binaire
            switch TypeWindow
                case 1
                    imagesc(x, y, I, CLim);
                case 2
                    imagesc_scroll(x, y, I, CLim);
            end
    end
    
    %% R�cup�ration des limites en X definies  au niveau de la commande "imagesc"
    
    %     if isempty(XLimLocal)
    %         XLim = a.XLim;
    %     else
    %         XLim = XLimLocal;
    %     end
    XLim = compute_XYLim(a.x(subxImage));
    
    %% R�cup�ration des limites en Y definies  au niveau de la commande "imagesc"
    
    %     if isempty(YLimLocal)
    %         YLim = a.YLim;
    %     else
    %         YLim = YLimLocal;
    %     end
    YLim = compute_XYLim(a.y(subyImage));
    
    
    if Rot90
        if isempty(a.XUnit)
            ylabel(a.XLabel, 'Interpreter', 'none')
        else
            ylabel([a.XLabel ' (' a.XUnit ')'], 'Interpreter', 'none')
        end
        if flagY
            xlabel('Distance along navigation (m)')
        else
            if isempty(a.YUnit)
                xlabel(a.YLabel, 'Interpreter', 'none')
            else
                xlabel([a.YLabel ' (' a.YUnit ')'], 'Interpreter', 'none')
            end
        end
        
        if TypeWindow == 1
            axis([YLim XLim])
        end
        
        if a.XDir == 1
            set(gca, 'YDir', 'reverse');
        else
            set(gca, 'YDir', 'normal');
        end
        
        if a.YDir == 1
            set(gca, 'XDir', 'normal');
        else
            set(gca, 'XDir', 'reverse');
        end
    else
        if isempty(a.XUnit)
            xlabel(a.XLabel, 'Interpreter', 'none')
        else
            xlabel([a.XLabel ' (' a.XUnit ')'], 'Interpreter', 'none')
        end
        if flagY
            ylabel('Distance along navigation (m)')
        else
            if isempty(a.YUnit)
                ylabel(a.YLabel, 'Interpreter', 'none')
            else
                ylabel([a.YLabel ' (' a.YUnit ')'], 'Interpreter', 'none')
            end
        end
        axis([XLim YLim])
        if a.XDir == 1
            set(gca, 'XDir', 'normal');
        else
            set(gca, 'XDir', 'reverse');
        end
        
        if a.YDir == 1
            set(gca, 'YDir', 'normal');
        else
            set(gca, 'YDir', 'reverse');
        end
    end
    if isempty(a.Unit)
        title(a.Name, 'Interpreter', 'none');
    else
        title([a.Name ' (' a.Unit ')'], 'Interpreter', 'none');
    end
    
    if a.GeometryType == cl_image.indGeometryType('LatLong')
        axisGeo('Rot90', Rot90)
    end
    
    Axe(iImage) = gca;
    
    dragplot
end


function [flag, resol] = getDistanceAlongNav(this, suby)

resol = [];

Latitude  = this.Sonar.FishLatitude;
Longitude = this.Sonar.FishLongitude;

Carto = getDefinitionCarto(this);
if isempty(Carto)
    str1 = 'La cartographie de l''image fournie n''est pas d�finie';
    str2 = 'The cartography of this image is unknown.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if isempty(Latitude) || isempty(Longitude)
    Latitude  = this.Sonar.ShipLatitude;
    Longitude = this.Sonar.ShipLongitude;
    if isempty(Latitude) || isempty(Longitude)
        str1 = 'Cette image ne contient pas de navigation.';
        str2 = 'This image does not contain any navigation.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
end

[xFish, yFish, flag] = latlon2xy(Carto, Latitude(suby), Longitude(suby));
if flag
    sub = ~isnan(xFish) & ~isnan(yFish);
    N = sum(sub);
    if N == 0
        flag = 0;
        return
    end
    D = sqrt(diff(xFish(sub)).^2 + diff(yFish(sub)).^2);
    resol = sum(D) / (N-1);
    if resol == 0
        flag = 0;
    end
end
