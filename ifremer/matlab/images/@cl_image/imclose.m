% Morphologically close image
%
% Syntax
%  b = imclose(a, se)
%
% Input Arguments
%  a  : Instance(s) of cl_image
%  se : Morphological structuring element 
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   bw = imread('circles.png');
%   a = cl_image('Image', bw, 'ColormapIndex', 2)
%   imagesc(a);
%
%   se = strel('disk',10);
%   b = imclose(a, se);
%   imagesc(b);
%
% See also cl_image/imopen cl_image/imdilate cl_image/imerode cl_image/open imopen imclose imdilate imerode Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = imclose(this, se, varargin)

%{
% TODO : peut-�tre faudra-il passer par une fonction my_imclose pour faire
exactement ce qui �tait fait auparavant dans cl_image/closing
my_imclose(I, se)
if ~isa(I, 'logical')
    I = (I ~= 0);
end
imclose(I,se);
%}

that = process_function_type2(this, @imclose, se, 'ValNaN', 2, 'CLim', [0 1], 'ColormapIndex', 2, varargin{:});
