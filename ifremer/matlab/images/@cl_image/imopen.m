% Morphologically open image
%
% Syntax
%  b = imopen(a, se)
%
% Input Arguments
%  a  : Instance(s) of cl_image
%  se : Morphological structuring element 
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   bw = imread('circles.png');
%   a = cl_image('Image', bw, 'ColormapIndex', 2)
%   imagesc(a);
%
%   se = strel('disk',10);
%   b = imopen(a, se);
%   imagesc(b);
%
% See also cl_image/imclose cl_image/imdilate cl_image/imerode cl_image/open imopen imclose imdilate imerode Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = imopen(this, se, varargin)

%{
% TODO : peut-�tre faudra-il passer par une fonction my_imopen pour faire
exactement ce qui �tait fait auparavant dans cl_image/opening
my_imopen(I, se)
if ~isa(I, 'logical')
    I = (I ~= 0);
end
imopen(I,se);
%}

that = process_function_type2(this, @imopen, se, 'ValNaN', 2, 'CLim', [0 1], 'ColormapIndex', 2, varargin{:});
