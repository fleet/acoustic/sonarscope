% Import data into a cl_image instaance
%
% Syntax
%   [flag, a, Carto, repImport] = import(this, filename, ...)
%
% Input Arguments
%   this     : cl_image or TODO
%   filename :
%
% Name-Value Pair Arguments
%   Carto        : TODO
%   memeReponses : TODO
%   xyCross      : TODO
%   repImport    : TODO
%   SonarIdent   : TODO
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   a         : One or several cl_image instances
%   Carto     : A cl_carto instance if
%   repImport : Directory of the imported file
%
% Remarks : Questions can be asked according to the file format
%
% Examples
%     filename = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a, Carto, repImport] = import(cl_image, filename)
%     SonarScope(a)
%
% See also import_er import_xtf ... Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, a, Carto, repImport, Answers] = import(this, nomFic, varargin)

persistent repLoad persistent_DataTypeGeotiff

[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false);
[varargin, xyCross]      = getPropertyValue(varargin, 'xyCross',      []);
[varargin, repImport]    = getPropertyValue(varargin, 'repImport',    []);
[varargin, SonarIdent]   = getPropertyValue(varargin, 'SonarIdent',   []);
[varargin, Answers]      = getPropertyValue(varargin, 'Answers',      []); %#ok<ASGLU>

% TODO : modifier tous les appels pour que memeReponses = true ou false

a = [];

%% Identification du type de format

[flag, typeFichier, SonarIdent] = identifyFileFormat(nomFic, SonarIdent);
if ~flag
    return
end
courtesy(nomFic)

%% Lecture du fichier

I0 = cl_image_I0;
switch typeFichier
    case 'BAG'
        a = import_bag(I0, nomFic);
        Carto = a.Carto;
        
    case 'BOB'
        % Pour l'instant du moins, on ne filtre pas les layers de Secteurs
        % et de Cellules d'EI.
        [flag, listLayers, listThresholds, listFlagDataType, flagIndexType] = selectionLayers(cl_Bob([]), nomFic);
        M0 = cl_multidata([]);
        if flag
            [flag, m] = import_BOB(M0, nomFic,  'listNumLayers', listLayers, ...
                'listFlagLayerType', listFlagDataType, ...
                'listIndexType', flagIndexType, ...
                'listThresholds', listThresholds);
            dL = get(m, 'DataLayer');
            dL = dL(:);
            iL = get(m, 'DataIndex');
            iL = iL(:);
            a = [dL;iL];
            if isempty(a)
                return
            end
        end
    case 'XML'
        [flag, a] = cl_image.import_xml(nomFic, 'Memmapfile', 1);
        
    case 'XML_ImagesAlongNavigation'
        [flag, a] = import_xml_ImagesAlongNavigation(I0, nomFic);
        
    case 'WaterColumnCube3D'
        [flag, a] = import_xml_WaterColumnCube3D(I0, nomFic, 'Memmapfile', -1);
        
    case 'XML_3DSeismic'
        [flag, a] = import_xml_3DSeismic(I0, nomFic);
        
    case 'ERMAPPER'
        [flag, a] = cl_image.import_ermapper(nomFic, 'memeReponses', memeReponses);
        
    case 'MATLAB'
        [a, flag] = import_matlab(I0, nomFic);
        
    case 'SRTM'
        [GMTMask, flag] = my_questdlg(Lang('Masquage de la mer � partir des donn�es GMT', 'Mask Sea with GMT ?'));
        if ~flag
            a = [];
            return
        end
        [a, flag] = import_SRTM(I0, nomFic, 'GMTMask', GMTMask==1);
        
    case 'GEOTIFF'
        if isempty(persistent_DataTypeGeotiff) || (memeReponses == 0)
            DataTypeGeotiff = [];
        else
            DataTypeGeotiff = persistent_DataTypeGeotiff;
        end
        [flag, a] = import_geotiff(I0, nomFic, 'memeReponses', memeReponses, 'DataType', DataTypeGeotiff);
        Carto = a.Carto;
        DataTypeGeotiff = a.DataType;
        persistent_DataTypeGeotiff = DataTypeGeotiff;
        
    case 'IMAGE'    % {'.tif'; '.tiff'; '.jpg'; '.jpeg'; '.bmp'; '.png'}
        [flag, a] = import_imagePlusPlus(I0, nomFic);
        
    case 'CaraibesMOSImage'
        a = import_CaraibesGrid(I0, nomFic, 'Sonar.Ident', SonarIdent, 'Flipud', 'memeReponses', memeReponses);
        
    case 'CaraibesMOSImage_3_6'
        [flag, a] = import_CaraibesGrid_3_6(I0, nomFic, 'Sonar.Ident', SonarIdent, 'Flipud', 'memeReponses', memeReponses);
        
    case 'CaraibesSTRImage'
        a = import_CaraibesGrid(I0, nomFic, 'Sonar.Ident', SonarIdent, 'memeReponses', memeReponses);
        
    case 'CaraibesSTRImage_3_6'
        [flag, a] = import_CaraibesGrid_3_6(I0, nomFic, 'Sonar.Ident', SonarIdent, 'memeReponses', memeReponses);
        
    case 'CaraibesMNT_3_6'
        [flag, a] = import_CaraibesGrid_3_6(I0, nomFic, 'Sonar.Ident', SonarIdent, 'memeReponses', memeReponses);
        
    case 'CaraibesMNT'
        a = import_CaraibesGrid(I0, nomFic, 'Sonar.Ident', SonarIdent, 'memeReponses', memeReponses);
        
    case 'CaraibesMbb'
        [flag, ListeFlagsInvalides] = paramsFlagCaraibes;
        if ~flag
            return
        end
        a = import_CaraibesMbg(I0, nomFic, 'Sonar.Ident', SonarIdent, ...
            'ListeFlagsInvalides', ListeFlagsInvalides, 'Carto', Carto);
        
    case 'CaraibesSonarLateral'
        %         car = cl_car_sonar('nomFic', nomFic);
        %         a = view(car, 'Sonar.Ident', SonarIdent);
        a = import_CaraibesSideScan(I0, nomFic, 'Sonar.Ident', SonarIdent);
        
    case 'SonarSAR'
        [flag, a, Carto] = import_SAR(I0, nomFic, 'Carto', Carto);
        
    case 'SEGY'
        [flag, listLayers] = selectionLayers(cl_segy([]), nomFic);
        if flag
            [flag, a, Carto] = import_SEGY(I0, nomFic, 'Carto', Carto, 'listLayers', listLayers);
            if isempty(a)
                return
            end
        end
        
    case 'IRAP'
        [a, flag] = import_irap(I0, nomFic);
    case 'SURFER'
        [flag, a] = import_surfer(I0, nomFic, 'memeReponses', memeReponses);
    case 'TRITON'
        [a, flag] = import_triton(I0, nomFic);
    case 'VMAPPERASCII'
        [a, flag] = import_vmascii(I0, nomFic);
    case 'GMT_cf'
        [a, flag] = import_gmt_old(I0, nomFic);
    case 'GMT_nf'
        [a, flag] = import_gmt(I0, nomFic);
    case 'GMT_NIWA'
        [a, flag] = import_gmt_niwa(I0, nomFic);
    case 'GMT_LatLong'
        [a, flag] = import_gmt_LatLong(I0, nomFic);
    case 'GMT_GEBCO'
        [a, flag] = import_gmt_Gebco(I0, nomFic);
    case 'NetCDF_GeoSeas'
        [a, flag] = import_NetCDF_GeoSeas(I0, nomFic);
    case 'ASCII_XYZ'
        [flag, a, Carto] = import_xyz(I0, nomFic, 'memeReponses', memeReponses, 'Carto', Carto);
    case 'GXF'
        [flag, a] = import_gxf(I0, nomFic);
    case 'TomographyOBS'
        [flag, a] = import_tomographyOBS(I0, nomFic);
    case 'EsriGridAscii'
        [flag, a, Carto] = import_EsriGridAscii(I0, nomFic, 'Carto', Carto);
    case 'ASCII_CARIS_LatLon_DM'
        [flag, a, Carto] = import_Caris_LatLon_DM(I0, nomFic, 'Carto', Carto);
    case 'ASCII_CARIS'
        [typeFichier, ScaleFactor, GeometryType, DataType, IndedexColumns] = identifyFormatAscii(nomFic); %#ok<ASGLU>
        [flag, a] = import_xyz(I0, nomFic, ...
            'NbLigEntete', -1, 'nColumns', -4, 'IndedexColumns', IndedexColumns, ...
            'ScaleFactor', ScaleFactor, 'GeometryType', -GeometryType, ...
            'DataType', -DataType); %, 'TypeAlgo', 1);
    case 'HDR-FLT'
        [flag, a] = import_hdr(I0, nomFic);
    case 'LGZ'
        [flag, a] = import_lgz(I0, nomFic);
        
    case 'ESRIshapeFile'
        str1 = sprintf('D�sol�, pas encore cabl�...');
        str2 = sprintf('Sorry, not plugged yet...');
        my_warndlg(Lang(str1,str2), 1);
        %         [rep, validation] = my_questdlg(Lang(...
        %             'Cette fonction est destin� � charger un masque pour l''image courante.\nEst-ce vraiment ce que vous voulez faire ?',...
        %             'This function is aimed at loading a mask for the current displayed image.\nIs this what you want ?'), 'Init', 1);
        %         if validation && rep == 1
        %             Images = get(this, 'Images');
        %             [flag, a] = ROI_import_shp(I0, Images(get(this,'indImage')), nomFic);
        %             clear Images
        %         end
    case 'XTF'
        [flag, a] = import_xtf(I0, nomFic);
        
    case 'ICESHAC'
        [flag, a] = import_hac(I0, nomFic, 'Memmapfile', 1);
        
    case 'GeoSwathRDF'
        b = cl_rdf('nomFic', nomFic);
        if ~isempty(b)
            [flag, a, Carto] = view_Image(b, 'memeReponses', memeReponses, 'Carto', Carto);
        end
        
    case 'KleinSDF'
        b = cl_sdf('nomFic', nomFic);
        flag = ~isempty(b);
        if flag
            [flag, SourceHeading] = SDF.Params.selectSourceHeading;
            if flag
                [flag, a, Carto] = view_Image(b, 'memeReponses', memeReponses, 'Carto', Carto, 'SourceHeading', SourceHeading);
            end
        end
        
    case 'FLEDERMAUS'
        winopen(nomFic)
        [flag, a] = import_fledermaus(I0, nomFic);
        
    case 'TRIAS'
        a = I0;
        a(1)= [];
        if ischar(nomFic)
            nomFic = {nomFic};
        end
        for k=1:length(nomFic)
            X = get_imageTrias(nomFic{k});
            if ~isempty(X)
                a(end+1) = X; %#ok
            end
        end
        if isempty(a)
            flag = 0;
        end
        
    case {'SimradAll'; 'SimradDatagramAll'} % SimradDatagramAll pour anciens fichiers
        [flag, a, Carto] = import_All(this, nomFic, memeReponses, [], Carto, xyCross);
        
    case 'SimradAmpPhase' % TODO : � transf�rer dans case {'SimradAll'; 'SimradDatagramAll'}
        [flag, DepthMin, DepthMax, DisplayLevel] = paramsSimradBeamRawData;
        if ~flag
            a = [];
            return
        end
        
        A = view_SimradRawData(nomFic, 'Carto', Carto, 'DisplayLevel', DisplayLevel, ...
            'DepthMin', DepthMin, 'DepthMax', DepthMax);
        if isempty(A)
            return
        end
        a = [a A];
        
    case 'ResonS7k'
        if ~memeReponses
            str1 = 'Type de g�om�trie';
            str2 = 'Type of Geometry';
            str = {'PingBeam'
                'SideScan Image PDS'
                'SampleBeam - Mag & Phase'
                'SampleBeam - Water Column'
                'PingSamples - Averaged Water Column'};
            [repLoad , flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
            if ~flag
                return
            end
        end
        
        a = I0;
        a(1) = [];
        
        % Bathy Reson
        if any(repLoad == 1)
            s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
            if isempty(s7k)
                return
            end
            [b, Carto] = view_depth(s7k, 'memeReponses', memeReponses, 'Carto', Carto, 'Bits', 'AskIt');
            a = [a b];
        end
        
        % Sidescan Reson
        if any(repLoad == 2)
            s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
            if isempty(s7k)
                return
            end
            [A, Carto, subl_Image, subl_Depth, memeReponses] = view_SidescanImagePDS(s7k, 'memeReponses', memeReponses, 'Carto', Carto); %#ok<ASGLU>
            if isempty(A)
                return
            end
            a = [a A];
        end
        
        % Mag & Phase
        if any(repLoad == 3)
            [flag, subPing, DisplayLevel, nomFicNav, nomFicIP, repImport] = params_ResonBeamRawData(this, nomFic, xyCross, repImport);
            if ~flag
                return
            end
            s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
            A = view_BeamRawData(s7k, 'subPing', subPing, 'Carto', Carto, ...
                'DisplayLevel', DisplayLevel, 'NavFileName', nomFicNav, 'nomFicIP', nomFicIP);
            if isempty(A)
                return
            end
            a = [a A];
        end
        
        % Water Column
        if any(repLoad == 4)
            [flag, subPing, nomFicNav, nomFicIP, repImport] = params_ResonWaterColumnData(this, nomFic, xyCross, repImport);
            if ~flag
                return
            end
            s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
            A = view_WaterColumn(s7k, 'subPing', subPing, 'Carto', Carto', 'NavFileName', nomFicNav, 'nomFicIP', nomFicIP);
            if isempty(A)
                return
            end
            a = [a A];
        end
        
        if any(repLoad == 5) % PingSamples - Averaged Water Column
            s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
            [flag, A] = import_SummedWaterColumn_S7K(s7k);
            if ~flag
                return
            end
            a = [a A];
        end
        
    case 'ODV'
        [flag, a, Carto] = import_ODV(nomFic);
        if isempty(a) % TODO : pourquoi on ne fait pas confiance � flag ici ?
            return
        end
        
    case 'Simrad_ExRaw'
        if ~memeReponses || isempty(Answers) || ~isfield(Answers, 'Simrad_ExRaw')
            [flag, listFrequencies, listDataType] = selectionLayers(cl_ExRaw([]), nomFic);
            if ~flag
                return
            end
        else
            listFrequencies = Answers.Simrad_ExRaw.listFrequencies;
            listDataType    = Answers.Simrad_ExRaw.listDataType;
        end
        [flag, a, Carto] = import_ExRaw(I0, nomFic, 'Carto', Carto, 'listFrequencies', listFrequencies, 'listDataType', listDataType);
        if isempty(a)
            return
        end
        
        if isempty(Answers)
            Answers.Simrad_ExRaw.listFrequencies = listFrequencies;
            Answers.Simrad_ExRaw.listDataType    = listDataType;
        end
        
    case 'Unknown'
        nomFicIn = nomFic;
        [~, nomFic, ext] = fileparts(nomFicIn);
        
        % Test si c'est un fichier .fig
        if strcmp(ext, '.fig')
            %             open(nomFicIn)
            openfig(nomFicIn); % Modif JMA le 07/06/2018
        elseif strcmp(ext, '.avi')
            if isdeployed
                try
                    winopen(nomFicIn)
                catch %#ok<CTCH>
                    str1 = 'Aucun lecteur externe n'' est associ� � ce type de fichier.';
                    str2 = 'No reader assiciated to the extension of this file.';
                    my_warndlg(Lang(str1,str2), 1);
                end
            else
                my_implay(nomFicIn)
            end
        else
            try
                nomFicOut = fullfile(my_tempdir, [nomFic '.tif']);
                
                pgm = getNomExeConvert;
                if isempty(pgm)
                    my_warndlg('ImaheMagick not installed, I cannot try to do a cormat conversion', 1);
                    flag = 0;
                    return
                end
                cmd = sprintf('! "%s" "%s" "%s"', pgm, nomFicIn, nomFicOut);
                
                str1 = sprintf('Le format du fichier n''etant pas reconnu, je tente la commande suivante : %s.\n\nSi ca fonctionne importez ce fichier "%s".', cmd, nomFicOut);
                str2 = sprintf('The format could not be identified, I am tring the command here after : %s.\n\nIf it works, import the output file "%s".', cmd, nomFicOut);
                my_warndlg(Lang(str1,str2), 1);
                eval(cmd)
                %                 visuExterne(nomFicOut)
                % TODO : et apr�s ?
            catch %#ok<CTCH>
                str1 = 'La tentative de conversion avec ImageMagic n''a rien donn�.';
                str2 = 'Th attempt to convert the file with ImageMagic failed.';
                my_warndlg(Lang(str1,str2), 1);
            end
            flag = 0;
        end
end


function pgm = getNomExeConvert

global VisualisateurImageExterne %#ok<GVMIS> 
global IfrTbxResources %#ok<GVMIS>

pgm = [];

if isempty(VisualisateurImageExterne)
    nomFic = fullfile(IfrTbxResources, 'Resources.ini');
    %verifNameLength(nomFic)
    if exist(nomFic, 'file')
        loadVariablesEnv;
    end
end

Viewer = VisualisateurImageExterne;
if ~iscell(Viewer)
    Viewer = {Viewer};
end

for k=1:length(Viewer)
    sub = strfind(Viewer{k}, 'imdisplay.exe');
    if ~isempty(sub)
        iFin = sub(1) - 2;
        nomDir = Viewer{k}(1:iFin);
        pgm = fullfile(nomDir,'convert.exe');
        break
    end
end
