% Exportation de courbes de statistiques conditionnelles
%
% Syntax
%   [flag, this] = importCourbesStats(this, ...)
%
% Input Arguments
%   flag : 1=OK, 0=KO
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = importCourbesStats(this, nomFicCourbe, varargin)

[varargin, Tag] = getPropertyValue(varargin, 'Tag', []); %#ok<ASGLU>

if ~iscell(nomFicCourbe)
    nomFicCourbe = {nomFicCourbe};
end

for k=1:length(nomFicCourbe)
    [flag, this] = importCourbesStats_unitaire(this, nomFicCourbe{k}, Tag);
    if ~flag
        return
    end
end


function [flag, this] = importCourbesStats_unitaire(this, nomFicCourbe, Tag)

[CourbeConditionnelle, flag] = load_courbesStats(nomFicCourbe);
if ~flag
    return
end

CourbeConditionnelle = courbesStatsTransmutationPourImagerie(this, CourbeConditionnelle);
flag = 0;

if ~isempty(Tag)
    subCourbesStatistiques = [];
    for k1=1:length(CourbeConditionnelle)
        if strcmp(CourbeConditionnelle(k1).bilan{1}(1).Tag, Tag)
            subCourbesStatistiques(end+1) = k1; %#ok
        end
    end
    CourbeConditionnelle = CourbeConditionnelle(subCourbesStatistiques);
    if isempty(CourbeConditionnelle)
        return
    end
end

for k1=1:length(CourbeConditionnelle)
    ifin = length(this.CourbesStatistiques);
    try
        if ifin == 0
            this.CourbesStatistiques = CourbeConditionnelle(k1);
        else
            if isfield(this.CourbesStatistiques(ifin), 'x') && isfield(CourbeConditionnelle(k1), 'x')
                pppp = orderfields(CourbeConditionnelle(k1), this.CourbesStatistiques(ifin));
                this.CourbesStatistiques(ifin+1) = pppp;
            else
                this.CourbesStatistiques(ifin+1).bilan = CourbeConditionnelle(k1).bilan;
            end
        end
    catch %#ok<CTCH>
        str1 = 'Cette courbe semble venir d''une version pr�c�dente de SonarScope, elle n''est pas compatible avec les courbes actuellement pr�sentes dans l''image. Vous pouvez soit supprimer les courbes existantes soit lancer une nouvelle instance de SonarScope';
        str2 = 'This curve seems coming from a previous SonarScope version, it is not compatible with the current curves defined for this image. You can remove the current curves or lauch a new SonarScope window.';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    if strcmp(Tag, 'DiagTx')
        nbModels = length(CourbeConditionnelle(k1).bilan{1});
        for k2=1:nbModels
            %             nuSector = CourbeConditionnelle(k1).bilan{1}(k2).TagSup{2};
            Mode = CourbeConditionnelle(k1).bilan{1}(k2).Mode;
            
            TypeMode_1 =  get(this.Sonar.Desciption, 'Sonar.TypeMode_1');
            TypeMode_2 =  get(this.Sonar.Desciption, 'Sonar.TypeMode_2');
            
            if strcmp(TypeMode_1, 'SounderMode')
                this.Sonar.Desciption = set(this.Sonar.Desciption, 'Sonar.Mode_1', Mode);
            elseif strcmp(TypeMode_1, 'Swath') && strcmp(TypeMode_2, 'SounderMode')
                Mode_Swath = 1; % TODO : impos� � 1 car dans CourbeConditionnelle(k1).bilan{1}(k2) on ne connait pas ce param�tre !!!
                this.Sonar.Desciption = set(this.Sonar.Desciption, 'Sonar.Mode_1', Mode_Swath, 'Sonar.Mode_2', Mode);
            end
            
            models = get_EmissionModelsCalibration(this.Sonar.Desciption);
            numVarCondition = CourbeConditionnelle(k1).bilan{1}(k2).numVarCondition;
            if numVarCondition > length(models)
                continue
            end
            if ~isempty(CourbeConditionnelle(k1).bilan{1}(k2).model)
                models(numVarCondition) = CourbeConditionnelle(k1).bilan{1}(k2).model;
            end
        end
        this.Sonar.Desciption = set_EmissionModelsCalibration(this.Sonar.Desciption, models);
    end
end

str1 = sprintf('%s a �t� correctement import�e.', nomFicCourbe);
str2 = sprintf('%s has been correctly loaded.', nomFicCourbe);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImportationCourbeStats', 'TimeDelay', 60);

flag = 1;
