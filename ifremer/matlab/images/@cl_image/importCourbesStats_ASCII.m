% Exportation de courbes de statistiques conditionnelles
%
% Syntax
%   importCourbesStats_ASCII(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% -------------------------------------------------------------------------

function this = importCourbesStats_ASCII(this, nomFic, varargin)

% [varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));
% [varargin, last] = getFlag(varargin, 'last');
% if last
%     sub = sub(end);
% end
%
% if isempty(sub)
%     return
% end

if ~isempty(nomFic)
    %     this.CourbesStatistiques(end+1) = import_ASCII(nomFic, this.CourbesStatistiques(sub));
    bilan = import_ASCII(nomFic);
    for i=1:length(bilan)
        this.CourbesStatistiques(end+1).bilan{1} = bilan(i);
    end
end

% -------------------------------------------------------------------------
% function Courbes = import_ASCII(nomFic, Courbes)
function bilan = import_ASCII(nomFic)

% s = Courbes.bilan{1};
bilan = xml_read(nomFic);

for i=1:length(bilan)
    if ischar( bilan(i).x)
        bilan(i).x = str2num( bilan(i).x); %#ok<ST2NM>
    end
    if ischar( bilan(i).y)
        bilan(i).y = str2num( bilan(i).y); %#ok<ST2NM>
    end
    if ischar( bilan(i).ymedian)
        bilan(i).ymedian = str2num( bilan(i).ymedian); %#ok<ST2NM>
    end
    if ischar( bilan(i).std)
        bilan(i).std = str2num( bilan(i).std); %#ok<ST2NM>
    end
    
    %     sub = (bilan(i).y == 9999);
    %     bilan(i).y(sub)       = NaN;
    %     bilan(i).ymedian(sub) = NaN;
    %     bilan(i).std(sub)     = NaN;
end

% fid = fopen(nomFic, 'w+');
% names = fieldnames(s);
% for i=1:length(names)
%     fprintf(fid, '%s : ', names{i});
%
%     val = s.(names{i});
%     switch class(val)
%         case 'char'
%             fprintf(fid, '"%s"\n', val);
%         case {'double'; 'single'}
%             val = val(:);
%             fprintf(fid, '%s\n', num2str(val'));
%         case 'cell'
%             fprintf(fid, '"{%s}"\n', val{1});
%         otherwise
%             str = sprintf('%s pas encore trait� dans cl_image/importCourbesStats_ASCII', class(val));
%             my_warndlg(str, 1);
%     end
% end
% fclose(fid);
