function [flag, a] = importEGM96(this, LatLim, LonLim)

if isempty(LatLim)
    flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
    if ~flag
        return
    end
    LonLim = this.XLim;
    LatLim = this.YLim;
end


Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
    'Projection.Mercator.long_merid_orig', mean(LonLim), ...
    'Projection.Mercator.lat_ech_cons',    mean(LatLim));

Comment = 'The geoid is a gravitationally equipotential surface that approximates the shape of the Earth. You can visualize the geoid as the surface of the ocean without effects such as weather, waves, and land. The Earth Gravitational Model of 1996 (EGM96) measures geoid height with respect to the ellipsoid specified by the World Geodetic System of 1984 (WGS84).';

try
    [G, refvec] = egm96geoid(1, LatLim, LonLim);
catch
    str1 = 'Cet import n''est disponible que � partir de la version R2019b.';
    str2 = 'This import is available only from Matlab R2019b.';
    my_warndlg(Lang(str1,str2), 1)
    flag = 0;
    a = [];
    return
end
[n1,n2] = size(G);

%{
refvec : 
s � Number of geoid height samples per degree
nlat � Northernmost latitude in degrees, plus 1/(2*s)
wlon � Westernmost longitude in degrees, minus 1/(2*s)
%}

s = refvec(1);
step = 1 / s;
nlat = refvec(2) - step/2;
slat = refvec(2) - n1 * step + step/2;
wlon = refvec(3) + step/2;
elon = refvec(3) + n2 * step - step/2;

Lon = centrage_magnetique(linspace(wlon, elon, n2));
Lat = centrage_magnetique(linspace(slat, nlat, n1));

a = cl_image('Image', G, 'x', Lon, 'y', Lat, 'Name', 'EGM96', ...
    'DataType', cl_image.indDataType('Bathymetry'), ...
    'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);

flag = 1;
