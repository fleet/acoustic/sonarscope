% Exportation de courbes de statistiques conditionnelles
%
% Syntax
%   importSegmSignature(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% VERSION  : $Id: importSegmSignature.m,v 1.1 2005/09/14 08:23:13 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = importSegmSignature(this, nomFicSave)

if ischar(nomFicSave)
    nomFicSave = {nomFicSave};
end

for k=1:length(nomFicSave)
    this = importSegmSignature_unitaire(this, nomFicSave{k});
end



function this = importSegmSignature_unitaire(this, nomFicSave)

if isempty(nomFicSave)
    return
end
Texture = loadmat(nomFicSave, 'nomVar', 'Texture');

for i=1:length(Texture)
    if isempty(this.Texture)
        this.Texture = Texture;
%         this.Texture.nomCourbe = {};
%         this.Texture.commentaire = {};
%         this.Texture.CmatModeleAngle = {};
    else
        this.Texture = [this.Texture Texture];
    end
end


