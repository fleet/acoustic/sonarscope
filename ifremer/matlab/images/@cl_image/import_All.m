function [flag, a, Carto] = import_All(this, nomFic, memeReponses, MasqueActif, Carto, xyCross)

persistent repLoad IndNavUnit IndAttUnit MasqueActifOne

a = cl_image.empty;
aKM = cl_simrad_all('nomFic', nomFic);
if isempty(aKM)
    a = [];
    flag = 0;
    return
end

if ~memeReponses
    % Choix du type de donn�es
    str1 = 'Type de g�om�trie';
    str2 = 'Type of Geometry';
    str = {'PingBeam (from depth and RawRange&BeamAngle datagrams)'
        'PingAcrossSample (From Seabed image)'
        'SampleBeam - Water Column'
        'SampleBeam - Snippets'
        'SampleBeam - Mag & Phase'
        'SampleStave - Stave data'
        'PingSamples - Averaged Water Column'};
    [repLoad , flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
    if ~flag
        return
    end

    % Choix du syst�me de navigation
    [flag, IndNavUnit] = params_ALL_IndexNavigationSystem(aKM);
    if ~flag
        return
    end

    % Choix de la centrale d'attitude
    [flag, IndAttUnit] = params_ALL_IndexAttitudeSystem(aKM);
    if ~flag
        return
    end
end

%% Bathymetry

if any(repLoad == 1)
    if isempty(MasqueActif) || MasqueActif ~= 2
        if IsthereAMask(nomFic)
            if get_LevelQuestion >= 3
                if memeReponses && ~isempty(MasqueActifOne)
                    MasqueActif = MasqueActifOne;
                else
                    str1 = 'Un masque a �t� d�fini pour les .all, voulez-vous l''utiliser';
                    str2 = 'A Mask has been defined for .all file, will you use it ?';
                    [rep, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel', 3);
                    if ~flag
                        a = [];
                        return
                    end
                    MasqueActif = (rep == 1);
                    MasqueActifOne = MasqueActif;
                end
            else
                MasqueActif = 1;
%                 str1 = 'Un masque existe pour ce fichier, il est appliqu� par d�faut en mode Interm�diaire. Vous pouvez supprimer les masque par le menu Survey processings / .All / Data cleaning / Manage masks / Reset masks.';
%                 str2 = 'A mask exists, it is applied by default. You can reset it using menu Survey processings / .All / Data cleaning / Manage masks / Reset masks.';
%                 my_warndlg(Lang(str1,str2), 0, 'Tag', 'MaskAllApplique');
            end
        else
            MasqueActif = 0;
        end
    end
    
    Version = get(aKM, 'Version');
    if isempty(Version)
        str1 = sprintf('Il est impossible de d�terminer la version du format du fichier "%s"', nomFic);
        str2 = sprintf('Impossible to determine file format version on "%s"', nomFic);
        my_warndlg(Lang(str1,str2), 0);
        a = [];
        flag = 0;
        return
    end
    switch Version
        case 'V1'
            [c, Carto] = import_PingBeam_All_V1(aKM, 'memeReponses', memeReponses, 'Carto', Carto, ...
                'MasqueActif', MasqueActif, 'IndNavUnit', IndNavUnit, 'IndAttUnit, IndAttUnit');
            a = [a c];
            
        case 'V2'
            [c, Carto] = import_PingBeam_All_V2(aKM, 'memeReponses', memeReponses, 'Carto', Carto, ...
                'MasqueActif', MasqueActif, 'IndNavUnit', IndNavUnit, 'IndAttUnit', IndAttUnit);
            a = [a c];
    end
end

%% Imagery

if any(repLoad == 2)
    SpecularCompensation = 1;
    [A, Carto, subl_Image, subl_Depth, memeReponses] = import_SeabedImage_All(aKM, 'memeReponses', memeReponses, 'Carto', Carto, ...
        'SpecularCompensation', SpecularCompensation, 'IndNavUnit', IndNavUnit, 'Memmapfile', -1); %#ok<ASGLU>
    a = [a(:)' A(:)'];
end

%% Water Column

if any(repLoad == 3)
    [flag, subPing, ~, ~, ~, CorrectionTVG] = paramsSimradWaterColumn(this, nomFic, xyCross);
    if ~flag
        a = [];
        return
    end
    
    [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', -1);
    if ~flag
        a = [];
        return
    end
    [flag, DataWC] = read_WaterColumn_bin(aKM, 'Memmapfile', -1);
    if ~flag
        a = [];
        return
    end
    [flag, DataRaw] = read_rawRangeBeamAngle(aKM, 'MemmapFile', -1);
    if ~flag
        a = [];
        return
    end
    [~, DataPosition] = read_position_bin(aKM, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
    
    
    [~, DataAttitude] = read_attitude_bin(aKM, 'Memmapfile', 0);
    if ~flag || isempty(DataAttitude) || isempty(DataAttitude.Roll)
        return
    end
    
    [~, DataRuntime] = SSc_ReadDatagrams(nomFic, 'Ssc_Runtime', 'LogSilence', 0);
    
    [~, DataInstalParam, isDual] = read_installationParameters_bin(aKM);
    
    [flag, IdentSonar] = SimradModel2IdentSonar(DataWC.EmModel, DataDepth.EmModel, 'isDual', isDual);
    if ~flag
        a = [];
        return
    end
    SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
        'Sonar.SystemSerialNumber', DataWC.SystemSerialNumber);

    [A, Carto] = import_WaterColumn_All(aKM, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
        DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subPing, 'CorrectionTVG', CorrectionTVG);
    a = [a(:)' A(:)'];
end

%% Snippets

if any(repLoad == 4)
    [subPing, flag] = paramsSimradOneSeabedPingInWaterColumn(this, nomFic, xyCross);
    if ~flag
        a = [];
        return
    end
    
    [A, Carto] = import_SeabedImage_All_InSampleBeamGeometry(aKM, 'Carto', Carto, 'subPing', subPing);
    a = [a(:)' A(:)'];
end

%% Mag & Phase

if any(repLoad == 5)
    [subPing, DepthMin, DepthMax, DisplayLevel, flag] = paramsSimradMagAndPhase(this, nomFic, xyCross);
    if ~flag
        a = [];
        return
    end
    
    [A, Carto] = import_MagAndPhase_All(aKM, 'Carto', Carto, 'subPing', subPing, ...
        'DepthMin', DepthMin, 'DepthMax', DepthMax, 'DisplayLevel', DisplayLevel);
    a = [a(:)' A(:)'];
end

if any(repLoad == 6)
    [flag, subPing] = paramsSimradStaveData(this, nomFic, xyCross);
    if ~flag
        a = [];
        return
    end
        
    [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', -1);
    if ~flag
        a = [];
        return
    end
%     [flag, DataSD] = read_StaveData_bin(aKM, 'Memmapfile', -1);
    [flag, DataSD] = SSc_ReadDatagrams(nomFic, 'Ssc_StaveData', 'Memmapfile', -1); % NON TESTE
    if ~flag
        a = [];
        return
    end
    [flag, DataRaw] = SSc_ReadDatagrams(nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); % Non test� ici
    if ~flag
        a = [];
        return
    end
    [~, DataPosition] = read_position_bin(aKM, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
    
    [flag, IdentSonar] = SimradModel2IdentSonar(DataSD.EmModel, DataDepth.EmModel);
    if ~flag
        a = [];
        return
    end
    SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
        'Sonar.SystemSerialNumber', DataSD.SystemSerialNumber);

    [A, Carto] = import_StaveData_All(aKM, DataDepth, DataSD, DataRaw, DataPosition, SonarDescription, ...
        'Carto', Carto, 'subPing', subPing);
    a = [a(:)' A(:)'];
end

%% PingSamples - Averaged Water Column'

if any(repLoad == 7)
    [flag, AngleLim, CorrectionTVG, TypeMeanUnit] = params_SummedWaterColumn();
    if ~flag
        a = [];
        return
    end
    [flag, A] = import_SummedWaterColumn_All(aKM, 'AngleLim', AngleLim, 'CorrectionTVG', CorrectionTVG, 'TypeMeanUnit', TypeMeanUnit);
    if flag
        a = [a(:)' A];
    end
end

flag = 1;


function flag = IsthereAMask(dataFileName)

flag = existCacheNetcdf(dataFileName);
if flag
    return
end

[nomDir, nomFic] = fileparts(dataFileName);
nomFicMask = fullfile(nomDir, 'SonarScope', nomFic, 'Ssc_Depth', 'Mask.bin');
flag = exist(nomFicMask, 'file');
