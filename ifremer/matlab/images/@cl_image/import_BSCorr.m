function [flag, this] = import_BSCorr(this, nomFicBSCorr, varargin)

% [varargin, subConfigs] = getPropertyValue(varargin, 'sub', []);

%% Test signature

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', identReflectivity, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Lecture du fichier bscorr.txt

[flag, BSCorr, BSCorrInfo] = read_BSCorr(nomFicBSCorr, 'ImportCompensationCurves', 1);
if ~flag
    return
end

%% Cr�ation des courbes

[flag, this] = BSCorr2Curves(this, BSCorr, BSCorrInfo);

%% Envoi des mod�les dans l'instance cl_sounder : DiagTx Calibration Models

SonarDescription = this.Sonar.Desciption;
SonarName = get(SonarDescription, 'Sonar.Name');
SystemSerialNumber = get(SonarDescription, 'Sonar.SystemSerialNumber');
switch SonarName
    case {'EM122'; 'EM302'}
        [flag, SonarDescription] = set_KongsbergBsCorrPoly(SonarDescription, ...
            str2double(SonarName(3:end)), SystemSerialNumber, BSCorr, 'Destination', 2);
    case {'EM710'}
        [flag, SonarDescription] = set_KongsbergBsCorrSpline(SonarDescription, ...
            str2double(SonarName(3:end)), SystemSerialNumber, BSCorr, 'Destination', 2);
    otherwise
        return
end
if ~flag
    return
end
this.Sonar.Desciption = SonarDescription;

%% Visualisation

models = get_EmissionModelsCalibration(SonarDescription);
plot(models)
plot_Tx(SonarDescription, 'Constructeur');
plot_Tx(SonarDescription, 'Calibration');


%% S�lection de la courbe � importer

%{
strSwath = {'Single'; 'Double'};
str = {};
for k =1:length(BSCorr)
if BSCorr(k).swathMode ~= 3
str{end+1} = sprintf('Mode %d - Swath %s', BSCorr(k).pingMode, strSwath{BSCorr(k).swathMode}); %#ok<AGROW>
end
end
[rep, flag] = my_listdlgMultiple('List of words', str, 'InitialValue', 1:length(str));
if ~flag
return
end
subConfigs = ;
%}

