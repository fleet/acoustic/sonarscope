% a = cl_image('Image', Lena);
% [flag, this] = import_BSModelParameters(a);
% plotCourbesStats(a, 'sub', -1, 'Stats', 0)

function [flag, this] = import_BSModelParameters(this)

str1 = 'Saisissez les param�tres de votre mod�le';
str2 = 'Set the parameters of your model.';
model = BSLurtonModel('XData', -85:85);
a = ModelUiDialog(Lang(str1,str2), model);
a.openDialog;
flag = a.paramDialog.okPressedOut;
if ~flag
    return
end
model = a.model;
plot(model); %option
p = model.getParamsValue;

NY = length(model.XData);

bilan.x       = model.XData;
bilan.y       = model.compute;
bilan.ymedian = bilan.y;
bilan.nom     = 'BSLurton model';
bilan.sub     = 1:NY;
bilan.nx      = ones(NY,1);
bilan.std     = zeros(NY,1);
bilan.titreV  = 'BSLurton model defined manually';
bilan.NY      = [NY 1];
bilan.nomX    = 'IncidenceAngle';
bilan.Biais   = 0;
bilan.Unit    = 'dB';
bilan.nomZoneEtude       = 'BS : Manually defined';
bilan.commentaire        = '';
bilan.Tag                = 'BS';
bilan.DataTypeValue      = 'Reflectivity';
bilan.DataTypeConditions = {'IncidenceAngle'};
bilan.Support            = {'Layer'};
bilan.GeometryType       = 'PingBeam';
bilan.TypeCourbeStat     = 'Statistics';
bilan.TagSup             = [];
bilan.residuals          = zeros(1,NY);
bilan.model              = model;
bilan.BiaisModel         = p(3);
            
this.CourbesStatistiques(end+1).bilan{1} = bilan;

flag = 1;


%{
if flag
    [flag, this] = importCourbesStats(this, nomFic, 'Tag', Tag);
    if flag
        nbFic = length(nomFic);
        nbCourbes = get_nbCourbesStatistiques(this);
        for iFic=1:nbFic
            % plotCourbesStats(this, 'sub', nbCourbes-iFic+1, 'Stats', 0, 'Titre', nomFic{iFic});
            plotCourbesStats(this, 'sub', nbCourbes-iFic+1, 'Stats', 0); % Modifi� le 25/07/2013 � bord du Suro�t, TODO : bug d�tect� par C�cile : incoh�rence nom de la curbe et courbe
        end
    end
end
%}