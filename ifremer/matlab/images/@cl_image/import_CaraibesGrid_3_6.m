% Importation de donn�es Caraibes au format MNT selon les nouvelles
% sp�cifications de la version 3.6 de CARAIBES.
%
% Syntax
%   [flag, b] = (a, nomFic, ...)
%
% Input Arguments
%   a      : Instance de type cl_image (cl_image suffisant)
%   nomFic :  Nom du fichier
%
% Name-Value Pair Arguments
%   TagSynchroXY : Tag de synchrimport_CaraibesGridonisation en x et en y
%   Sonar.Ident  : Identification du sondeur
%   ListeLayers  : Liste des layers � importer
%   subx         : subsampling in X
%   suby         : subsampling in Y
%
% Name-only Arguments
%   Flipud : Pour retourner l'image (cas des images mosaiques)
%
% Output Arguments
%   [] : Auto-plot activation
%   a  : Instances de cl_image.
%
% Examples
%   nomFic = getNomFicDatabase('EM300_ExMos.imo');
%   [flag, a] = import_CaraibesGrid_3_6(cl_image, nomFic)
%   SonarScope(a)
%
%   [flag, a] = import_CaraibesGrid_3_6(cl_image, nomFic, 'suby', 1:100);
%   SonarScope(a)
%
% See also cl_image Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, c] = import_CaraibesGrid_3_6(this, nomFic, varargin)

persistent persistent_SonarDescription persistent_EtatSonar

[varargin, sublAsked]    = getPropertyValue(varargin, 'suby',         []);
[varargin, subcAsked]    = getPropertyValue(varargin, 'subx',         []);
[varargin, TagSynchroXY] = getPropertyValue(varargin, 'TagSynchroXY', []);
[varargin, SonarIdent]   = getPropertyValue(varargin, 'Sonar.Ident',  []);
[varargin, ListeLayers]  = getPropertyValue(varargin, 'ListeLayers',  []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false);

[varargin, Flipud] = getFlag(varargin, 'Flipud'); %#ok<ASGLU>

in = cl_netcdf('fileName', nomFic);
% ncEdit(nomFic)

Layer_name  = get_value(in, 'Layer_name');
%Layer_unit = 0;
% numVar      = find_numVar(in, 'DEPTH');
% Layer_unit  = get_valVarAtt(in, numVar, 'units');

str  = [];
Unit = [];
for k=1:size(Layer_name, 1)
    s1 = deblank(Layer_name(k,:));
    if ~isempty(s1)
        numVar      = find_numVar(in, s1);
        Layer_unit  = get_valVarAtt(in, numVar, 'units', 'noMessage'); % NoMessage pour le cas des MNT CARAIBES.
        str{end+1} = s1; %#ok
        %         identLayer(k) = k;
        
        if all(double(Layer_unit) == 0)
            if strcmpi(s1, 'depth')
                Unit{end+1} = 'm'; %#ok
            else
                %                 my_warndlg('Pour JMA : import_CaraibesGrid_3_6 : d�finir l''unit�', 0, 'Tag', 'Pour JMA : import_CaraibesGrid_3_6');
                Unit{end+1} = ''; %#ok
            end
        else
            strUnite = deblank(Layer_unit);
            if strcmpi(strUnite, 'metres')
                strUnite = 'm';
            end
            Unit{end+1} = strUnite; %#ok
            if isempty(strUnite)
                switch  lower(s1)
                    case 'depth'
                        Unit{end+1} = 'm'; %#ok
                    otherwise
                        Unit{end+1} = ''; %#ok
                end
            end
        end
    end
end

if isempty(ListeLayers)
    if length(str) == 1
        ListeLayers = 1;
    else
        ListeLayers = my_listdlg('Layers :', str);
        if isempty(ListeLayers)
            flag = 0;
            c = [];
            return
        end
        drawnow
    end
end

%% Cr�ation de l'instance cli_image

ImmersionAntennes = -2; % Immersion d�finie de mani�re autoritaire. Il vaudrait peut-�tre mieux poser la question

%% Lecture des coordonnees

EtatSonar = [];

nomFicIn = nomFic;
% % % in = cl_netcdf('fileName', nomFicIn);

if isempty(sublAsked)
    suby = 1:get_valAtt(in, 'Number_lines');
else
    suby = sublAsked;
end
if isempty(subcAsked)
    subx = 1:get_valAtt(in, 'Number_columns');
else
    subx = subcAsked;
end

%% Selection interactive de la zone

pasEncoreCalcule = 1;

numAtt = find_numAtt(in, 'Sounder_type');
if ~isempty(numAtt)
    Sounder_type = get_valAtt(in, 'Sounder_type');
    SonarIdent = SonarIdent_caraibes2sonarScope(Sounder_type);
end

%{
[nomDir, nomFicXml] = fileparts(nomFic);
nomFicXml = fullfile(nomDir, [nomFicXml '.xml']);
[flag, SonarDescription, Carto, EtatSonar] = cl_image.import_xml(nomFicXml);
SonarIdent = 1
%}



% SonarDescription = cl_sounder('Sonar.Ident', SonarIdent); % d�plac�
% uniquement pour les images rectilignes

%     SonarDescription = set(SonarDescription, 'Sonar.Mode_2', 2) % POUR IMPOSER LE MODE 140 deg : mission calimero EM1002

Mosaic_type = get_value(in, 'Mosaic_type');
if isempty(Mosaic_type) % Modif JMA  le 21/04/2011 : Nouvelle version de Caraibes
    Mosaic_type = get_valAtt(in, 'Mosaic_type');
end
if strcmp(Mosaic_type, 'STR')  % Fichier rectiligne
    InitialFileFormat = 'CaraibesSTRImage';
    SonarDescription = cl_sounder('Sonar.Ident', SonarIdent);
    
    GeometryType = cl_image.indGeometryType('PingAcrossDist');    % SonarX
    y = (get_valDim(in, 'LINES')-1):-1:0;
    YUnit = 'Ping';
    %         resolution = get(a(k), 'Element_x_size') / 1000;
    Str_Xmin = get_value(in, 'Str_Xmin', 'Convert', 'double');
    Str_Xmax = get_value(in, 'Str_Xmax', 'Convert', 'double');
    if isempty(Str_Xmin) % Modif JMA  le 21/04/2011 : Nouvelle version de Caraibes
        Str_Xmin = double(get_valAtt(in, 'Str_Xmin'));
        Str_Xmax = double(get_valAtt(in, 'Str_Xmax'));
    end
    COLUMNS  = get_valDim(in, 'COLUMNS');
    x = linspace(Str_Xmin, Str_Xmax, COLUMNS);
    
    [x, resolution, Str_Xmin, Str_Xmax] = centrage_magnetique(x); %#ok
    %     resolution = mean(diff(x));
    
    Carto = get_Caraibes_carto(nomFic);
    
else % fichier mosaique
    SonarDescription = cl_sounder('Sonar.Ident', 1); % on force car on n'a aucune utilit� de conserver cette information dans SSC
    if strcmp(Mosaic_type, 'GEO')  % Fichier rectiligne
        InitialFileFormat = 'CaraibesMOSImage';  % ou CaraibesMOSImage
    else
        InitialFileFormat = 'CaraibesMNT';  % ou CaraibesMOSImage
    end
    
    Carto = get_Caraibes_carto_3_6(nomFic);
    if isempty(Carto)
        c = [];
        flag = 0;
        return
    end
    
    % GeometryType = cl_image.indGeometryType('GeoYX');
    if get(Carto, 'Projection.Type') == 6
        % le 13/12/2011 : Verrue pour les fichiers MNT issus de CARAIBES en
        % projection Cylindrique Equidistante
        Latitude_TL  = get_valAtt(in, 'Latitude_TL');
        Latitude_BL  = get_valAtt(in, 'Latitude_BL');
        Longitude_BL = get_valAtt(in, 'Longitude_BL');
        Longitude_BR = get_valAtt(in, 'Longitude_BR');
        
        x = linspace(Longitude_BL, Longitude_BR, get_valDim(in, 'COLUMNS'));
        y = linspace(Latitude_BL, Latitude_TL, get_valDim(in, 'LINES'));
        RayonTerre   = get(Carto, 'Ellipsoide.DemiGrandAxe');
        resolution   = abs(mean(diff(this.y))) / (RayonTerre * (pi/180));
        YUnit        = 'deg';   % M OU DEG ? M OU DEG ? M OU DEG ? M OU DEG ? M OU DEG ? M OU DEG ?
        GeometryType = 3;
        
    else
        [x, y, GeometryType] = get_CaraibesGrid_xy_3_6(nomFic, Carto);
        resolution = get_valAtt(in, 'Element_x_size') / 1000;
        YUnit = 'm';   % M OU DEG ? M OU DEG ? M OU DEG ? M OU DEG ? M OU DEG ? M OU DEG ?
    end
    
    if memeReponses && isa(persistent_SonarDescription, 'cl_sounder')
        SonarDescription = persistent_SonarDescription;
    else
        if isempty(SonarDescription)
            SonarDescription = cl_sounder([]);
        end
        %{
        % Comment� par JMA le 31/10/2018
        str1 = 'Dans certains cas, les fichiers mosaique de Caraibes ne conservent pas la signature du sondeur (Sonars lateraux). ';
        str2 = 'Vous devez dans ce cas definir le nom du sonar.';
        str3 = 'Vous pouvez ensuite definir le mode de fonctionnement.';
        str4 = 'Soyez vigilant lors de l''utilisation des informations concernant les parametres du sondeur (modules sonar).';
        str5 = 'Il est plus que souhaitable de creer dans caraibes un layer ''SOUNDER_MODE'' pour pouvoir utiliser les modules de calcul sonar sans erreur';
        my_warndlg(sprintf('%s\n%s\n%s\n%s\n%s\n', str1,str2,str3,str4,str5), 0, 'Tag', 'Dans certains cas, ...', 'TimeDelay', 60);
        [flag, SonarDescription] = selection(SonarDescription, 'Filemame', nomFic);
        if ~flag
            c = [];
            return
        end
        %}
        persistent_SonarDescription = SonarDescription;
    end
end

switch  GeometryType
    case cl_image.indGeometryType('LatLong')
        TagSynchroX = 'Longitude';
        TagSynchroY = 'Latitude';
    case cl_image.indGeometryType('GeoYX')
        TagSynchroXY = shortDescription(Carto);
        TagSynchroX = TagSynchroXY;
        TagSynchroY = TagSynchroXY;
    otherwise
        if isempty(TagSynchroXY)
            TagSynchroXY = num2str(rand(1));
        end
        TagSynchroX = TagSynchroXY;
        TagSynchroY = TagSynchroXY;
end

nbRows = length(suby);
Interlacing = [];
if GeometryType == cl_image.indGeometryType('PingAcrossDist')    % SonarX
    
    % ???? R�cup�ration des dates et Time ????
    Date  = get_value(in, 'Date', 'sub1', suby);
    Heure = get_value(in, 'Hour', 'sub1', suby);
    t = cl_time('timeIfr', Date, Heure);
    
    subNan = isnan(Date);
    
    % ATTENTION / Il faut prendre en compte :
    %         Field_A_resol                 <-> double [40   1]
    %         Field_B_resol                 <-> double [40   1]
    %         Field_no_data_value           <-> double [40   1]
    %         Field_decod_no_data_value     <-> double [40   1]
    %         Field_min_value               <-> double [40   1]
    %         Field_max_value               <-> double [40   1]
    
    %         SonarStarboardMode = get_value(in, 'StarboardMode', 'sub1', suby);
    SonarPortMode      = get_value(in, 'PortMode',      'sub1', suby);
    if isnan(SonarPortMode(1)) || all(SonarPortMode == 0)  % Bug EM120 Calimero (Mode=NaN) et Donn�es EM120 des allemands (Mode=0)
        SonarPortMode(:) = 1;
        
        Sounder_type = get_value(in, 'Sounder_type');
        if isempty(Sounder_type) % Modif JMA  le 21/04/2011 : Nouvelle version de Caraibes
            Sounder_type = get_valAtt(in, 'Sounder_type');
        end
        switch Sounder_type
            case 0 % Sondeur non d�fini
                SonarIdent = 1;
            case [10 11 12 13 20 21 30 40 41 50 70 80 81 82 83 87 88]
                my_warndlg('Tyoe de sondeur non pr�vu dans SonarScope', 0, 'Tag', 'Sounder_typeNonPrevu');
                SonarIdent = 1;
            case 51 % EM1000
                SonarIdent = 4;
            case 52 % EM12S
                SonarIdent = 2;
            case 53 % EM12D
                SonarIdent = 1;
            case 54 % EM3000S
                SonarIdent = 7;
            case 55 % EM3000D
                SonarIdent = 6;
            case 56 % EM300
                SonarIdent = 3;
            case 57 % EM3002 D ou S
                SonarIdent = 19; % 20 pour single
            case 58 % EM120
                SonarIdent = 11;
            case 59 % EM2000
                SonarIdent = 21;
            case 60 % EM3002 equidistant
                SonarIdent = 19; % ou 20
            case 61 % EM3002 High frequency
                SonarIdent = 19; % ou 20
            case 62 % EM3002 Dual
                SonarIdent = 19; % ou 20
            case 63 % EM710
                SonarIdent = 14;
            case 64 % ME70
                SonarIdent = 17;
            case 65 % EM122
                SonarIdent = 24;
            case 66 % EM302
                SonarIdent = 22;
            case 84 % 7111
                SonarIdent = 14;
            case 85 % 7150
                SonarIdent = 12; % 13 en 24 kHz
            case 86 % 7125
                SonarIdent = 15;
            case 90 % Geoswath
                SonarIdent = 16;
            otherwise
                SonarIdent = 1;
        end
        %         SonarIdent = get(SonarDescription, 'SonarIdent');
        
        SonarIdent = ones(size(SonarPortMode)) * SonarIdent;
        PortMode_1 = ones(size(SonarPortMode));
        PortMode_2 = ones(size(SonarPortMode));
        StarMode_1 = ones(size(SonarPortMode));
        StarMode_2 = ones(size(SonarPortMode));
        %             FactoryMode = ones(size(SonarPortMode)) * 2;
    else
        SonarPortMode(SonarPortMode == 0) = median(SonarPortMode); % Correction bug FOREVER Mode(1)=0
        [SonarIdent, PortMode_1, PortMode_2, FactoryMode, SonarFamily] = modeCar2Sim(SonarPortMode);%#ok
        [SonarIdent, StarMode_1, StarMode_2, FactoryMode, SonarFamily] = modeCar2Sim(SonarPortMode);%#ok
    end
    
    
    % Initialisation avec le premier mode trouve
    SonarDescription = set(SonarDescription,  'Sonar.Ident',  SonarIdent(1), ...
        'Sonar.Mode_1', PortMode_1(1), ...
        'Sonar.Mode_2', PortMode_2(1));
    
    
    SonarHeading       = get_value(in, 'Heading',       'sub1', suby);
    SonarHeight        = get_value(in, 'VerticalDepth', 'sub1', suby);
    SonarFishLatitude  = get_value(in, 'Latitude',      'sub1', suby, 'Convert', 'double');
    SonarFishLongitude = get_value(in, 'Longitude',     'sub1', suby, 'Convert', 'double');
    SonarFishLatitude(subNan)  = NaN;
    SonarFishLongitude(subNan) = NaN;
    
    SonarName = get(SonarDescription, 'Sonar.Name');
    if strcmp(SonarName, 'EM1000')
        Interlacing = get_value(in, 'Interlacing', 'sub1', suby);
    end
    
    SonarHeight = abs(SonarHeight);
    HauteurMoyenne = mean(SonarHeight(~isnan(SonarHeight)));
    
    y = 0:(nbRows-1);
    suby = 1:nbRows;
    YUnit = 'Ping';
    
    %{
RayonTerre = get(Carto, 'Ellipsoide.DemiGrandAxe');
dy = diff(SonarFishLatitude  * pi / 180) * RayonTerre;
dx = diff(SonarFishLongitude * pi / 180) * RayonTerre;
dx = dx * cos(my_nanmean(SonarFishLatitude * pi / 180));

subDxNan = find(isnan(dx));
subDxNonNan = find(~isnan(dx));
dx(subDxNan) = interp1(subDxNonNan, dx(subDxNonNan), subDxNan, 'linear', 'extrap');
dy(subDxNan) = interp1(subDxNonNan, dy(subDxNonNan), subDxNan, 'linear', 'extrap');

longueur = sum(sqrt(dx .^ 2 + dy .^ 2));
if longueur == 0
y = 0:(nbRows-1);
suby = 1:nbRows;
YUnit = 'Ping';
else
figure(6789); hold on; plot(SonarFishLongitude, SonarFishLatitude, '+'); grid on
y = linspace(0, longueur, nbRows);
suby = 1:nbRows;

strWarning = sprintf('ATTENTION : La longueur de cette image rectiligne est evaluee a %f m, ce qui donne un pas moyen de %f m par ping. Cette donnee ne doit etre consideree que comme indicative.', ...
longueur, longueur/nbRows);
disp(strWarning)
my_warndlg(strWarning, 0);
drawnow

YUnit = 'm';
end
    %}
end
Latitude_TL  = get_valAtt(in, 'Latitude_TL');
Latitude_TR  = get_valAtt(in, 'Latitude_TR');
Latitude_BR  = get_valAtt(in, 'Latitude_BR');
Latitude_BL  = get_valAtt(in, 'Latitude_BL');
Longitude_TL = get_valAtt(in, 'Longitude_TL');
Longitude_TR = get_valAtt(in, 'Longitude_TR');
Longitude_BR = get_valAtt(in, 'Longitude_BR');
Longitude_BL = get_valAtt(in, 'Longitude_BL');

LatitudeMoyenne  = mean([Latitude_TL  Latitude_TR  Latitude_BR  Latitude_BL]);
LongitudeMoyenne = mean([Longitude_TL Longitude_TR Longitude_BR Longitude_BL]);

%% Lecture des differents layers de l'image Caraibes et stokage dans l'instance cli_image

for k=1:length(ListeLayers)
    
    %% Lecture de l'image
    
    NomLayer = str{ListeLayers(k)};
    disp(['Lecture du layer : ' NomLayer])
    I = get_CaraibesGrid_layer_3_6(nomFicIn, NomLayer, 'suby', suby, 'subx', subx);
    
    %% On leve quelques bugs inerants aux fonctions de lecture Netcdf
    
    if strcmp(str{ListeLayers(k)}, 'SOUNDER_MODE')
        sub = (I < 0);
        I(sub) = 256 + I(sub);
    end
    
    if strcmp(str{ListeLayers(k)}, 'EMISSION_SECTOR')
        sub = (I < 0);
        I(sub) = NaN;
        I = I + 1; % Pour commencer � 1
    end
    
    if strcmp(str{ListeLayers(k)}, 'BEAM_NB')
        sub = (I < 0);
        I(sub) = 256 + I(sub);
        sub = (I == 255);
        I(sub) = NaN;
        I = I + 1; % Pour commencer � 1
        
        if strcmp(get(SonarDescription, 'Sonar.Name'), 'EM1000')
            for iLig=1:length(Interlacing)
                I(iLig,:) = I(iLig,:) + (2-Interlacing(iLig)) * 60;
            end
        end
    end
    
    %% Cr�ation de l'instance cl_image
    
    if Flipud
        %         I = flipud(I);
        for iLig = 1:floor(nbRows / 2)
            pppp = I(iLig,:);
            I(iLig,:) = I(nbRows + 1 - iLig,:);
            I(nbRows + 1 - iLig,:) = pppp;
        end
        
    end
    
    InitialFileName = get(in,'fileName');
    
    [~, nomFicSeul] = fileparts(InitialFileName);
    [DataType, ColormapIndex] = CAR_identSonarDataType(str{ListeLayers(k)});
    if DataType == 1
        Name = [nomFicSeul '_' str{ListeLayers(k)}];
    else
        Name = nomFicSeul;
    end
    b = cl_image('Image', I, 'Name', Name, 'Unit', Unit{ListeLayers(k)}, ...
        'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', x(subx), 'XUnit', 'm', 'y', y(suby), 'YUnit', YUnit, ...
        'InitialFileName',   InitialFileName, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType',      GeometryType, ...
        'SonarDescription',   SonarDescription);
    
    b = set(b, 'Carto', Carto);
    
    b = set(b, 'SonarResolutionX', resolution);
    
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    if GeometryType == cl_image.indGeometryType('PingAcrossDist')
        %             [DataType, ColormapIndex] = CAR_identSonarDataType(str{ListeLayers(k)});
        %             b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
        %                 'SonarRawDataResol', resolution, 'SonarResolutionD', resolution);
        b = set(b, 'SonarRawDataResol', resolution, 'SonarResolutionD', resolution);
        b = set(b, 'SonarFishLongitude', SonarFishLongitude);
        b = set(b, 'SonarTime', t);
        b = set(b, 'SonarHeading', SonarHeading);
        
        b = set(b, 'SonarPortMode_1', PortMode_1);
        b = set(b, 'SonarPortMode_2', PortMode_2);
        b = set(b, 'SonarStarMode_1', StarMode_1);
        b = set(b, 'SonarStarMode_2', StarMode_2);
        
        %ATTENTION BUG DECODAGE FICHIER CARAIBES Unite = ?
        b = set(b, 'SonarHeight',  -abs(SonarHeight) - abs(ImmersionAntennes));
        
        
        b = set(b, 'SonarFishLatitude', SonarFishLatitude);
        Immersion = repmat(ImmersionAntennes, 1, nbRows);
        b = set(b, 'SonarImmersion', Immersion);
        
        
        if ~isempty(Interlacing)
            b = set(b, 'SonarInterlacing', Interlacing);
        end
        
        if pasEncoreCalcule
            subNonNaN = find(~isnan(Date)); % Modif JMA  le 21/04/2011 : Donn�es Geoswath
            [~, Mois] = dayIfr2Jma(Date(subNonNaN(1))); % Modif JMA  le 21/04/2011 : Donn�es Geoswath
            Freq = get(SonarDescription, 'Signal.Freq');
            z = cl_sound_speed('Incoming', 2, 'LevitusAveragingType', 3, ...
                'Latitude', LatitudeMoyenne, 'Longitude', LongitudeMoyenne, ...
                'Frequency', Freq, 'LevitusMonth', Mois);
            Depth              = get(z, 'Depth');
            AveragedAbsorption = get(z, 'AveragedAbsorption');
            Temperature        = get(z, 'Temperature');
            Salinity           = get(z, 'Salinity');
            SoundSpeed         = get(z, 'SoundSpeed');
            %             Immersion = mean(Immersion); % Immersion du sonar
            
            %             [pppp, iz] = min(abs(abs(Depth - Immersion)));
            [~, iz] = min(abs(abs(Depth + HauteurMoyenne)));
            
            SonarTVG_IfremerAlpha = AveragedAbsorption(iz);
            
            if any(strcmp(str(ListeLayers), 'REFLECTIVITY'))
                if memeReponses && ~isempty(persistent_EtatSonar)
                    EtatSonar = persistent_EtatSonar;
                else
                    [EtatSonar, flag] = get_Etat_ImageSonar(b, SonarDescription, SonarTVG_IfremerAlpha);
                    if ~flag
                        c = [];
                        return
                    end
                    persistent_EtatSonar = EtatSonar;
                end
            end
            
            pasEncoreCalcule = 0;
        end
        b = set(b,  'SonarBathyCel_Z', Depth, ...
            'SonarBathyCel_T', Temperature, ...
            'SonarBathyCel_S', Salinity, ...
            'SonarBathyCel_C', SoundSpeed);
    else
        %             if any(strcmp(str(ListeLayers), 'REFLECTIVITY'))
        if isempty(EtatSonar) && any(strcmp(str(ListeLayers), 'REFLECTIVITY'))
            if memeReponses && ~isempty(persistent_EtatSonar)
                EtatSonar = persistent_EtatSonar;
            else
                [EtatSonar, flag] = get_Etat_ImageSonar(b, SonarDescription, 0);
                if ~flag
                    c = [];
                    return
                end
                persistent_EtatSonar = EtatSonar;
            end
        end
    end
    
    
    if ~isempty(EtatSonar)
        b = set(b,  'Sonar_DefinitionENCours', ...
            'SonarTVG_ConstructTypeCompens',        EtatSonar.SonarTVG_ConstructTypeCompens, ...
            'SonarTVG_ConstructTable',              EtatSonar.SonarTVG_ConstructTable, ...
            'SonarTVG_ConstructAlpha',              EtatSonar.SonarTVG_ConstructAlpha, ...
            'SonarTVG_ConstructConstante',          EtatSonar.SonarTVG_ConstructConstante, ...
            'SonarTVG_ConstructCoefDiverg',         EtatSonar.SonarTVG_ConstructCoefDiverg, ...
            'SonarTVG_IfremerAlpha',                EtatSonar.SonarTVG_IfremerAlpha, ...
            'SonarDiagEmi_etat',                    EtatSonar.SonarDiagEmi_etat, ...
            'SonarDiagEmi_origine',                 EtatSonar.SonarDiagEmi_origine, ...
            'SonarDiagEmi_ConstructTypeCompens',    EtatSonar.SonarDiagEmi_ConstructTypeCompens, ...
            'SonarDiagRec_etat',                    EtatSonar.SonarDiagRec_etat, ...
            'SonarDiagRec_origine',                 EtatSonar.SonarDiagRec_origine, ...
            'SonarDiagRec_ConstructTypeCompens',    EtatSonar.SonarDiagRec_ConstructTypeCompens, ...
            'Sonar_NE_etat',                        EtatSonar.SonarNE_etat, ...
            'Sonar_SH_etat',                        EtatSonar.SonarSH_etat, ...
            'Sonar_GT_etat',                        EtatSonar.SonarGT_etat, ...
            'SonarBS_etat',                         EtatSonar.SonarBS_etat, ...
            'SonarBS_origine',                      EtatSonar.SonarBS_origine, ...
            'SonarBS_origineBelleImage',            EtatSonar.SonarBS_origineBelleImage, ...
            'SonarBS_origineFullCompens',           EtatSonar.SonarBS_origineFullCompens, ...
            'SonarBS_IfremerCompensTable',          EtatSonar.SonarBS_IfremerCompensTable, ...
            'SonarAireInso_etat',                   EtatSonar.SonarAireInso_etat, ...
            'SonarAireInso_origine',                EtatSonar.SonarAireInso_origine);
    end
    b = update_Name(b);
    
    c(k) = b; %#ok
end
flag = 1;


function [DataType, ColormapIndex] = CAR_identSonarDataType(str)

% ColormapIndex : '1=User' | {'2=gray'} | '3=jet' | '4=cool' | '5=hsv' | '6=hot' | '7=bone' | '8=copper' | '9=pink' | '10=flag' | '11=CNES' | '12=CNESjet' | '13=colorSea' | '14=colorEarth'
ColormapIndex      = 3;    % jet par defaut

switch upper(str)
    case 'REFLECTIVITY'
        DataType = cl_image.indDataType('Reflectivity');
        ColormapIndex = 2;    % gray
    case 'REFLECTIVITY_COR'
        DataType = cl_image.indDataType('Reflectivity');
        ColormapIndex = 2;    % gray
    case 'EMISSION'
        DataType = cl_image.indDataType('TxAngle');
    case 'INCIDENCE'
        DataType = cl_image.indDataType('IncidenceAngle');
    case 'EMISSION_SECTOR'
        DataType = cl_image.indDataType('TxBeamIndex');
    case 'BEAM_NB'
        DataType = cl_image.indDataType('RxBeamIndex');
    case 'AVRG_PIX_NB'
        DataType = cl_image.indDataType('AveragedPtsNb');
    case 'DEPTH'
        DataType = cl_image.indDataType('Bathymetry');
    case 'MANUAL_SEG'
        DataType = cl_image.indDataType('RegionOfInterest');
    case 'SEGMENTATION'
        DataType = cl_image.indDataType('Segmentation');
    case 'LSLOPE'
        DataType = cl_image.indDataType('SlopeAlong');
    case 'TSLOPE'
        DataType = cl_image.indDataType('SlopeAcross');
    case 'SLOPE'
        DataType = cl_image.indDataType('Slope');
        
    case 'BEAM_EMI_ANGLE'
        DataType = cl_image.indDataType('RxBeamAngle');
    case 'ANGLE_DIF'
        DataType = cl_image.indDataType('Unknown');
    case 'GROUND_VEL'
        DataType = cl_image.indDataType('Unknown');
    case 'SURF_VEL'
        DataType = cl_image.indDataType('Unknown');
    case 'H_INCIDENCE'
        DataType = cl_image.indDataType('Unknown');
    case 'AVRG_PIX'
        DataType = cl_image.indDataType('Unknown');
    case 'PIX_NB'
        DataType = cl_image.indDataType('Unknown');
    case 'SIMRAD_REF'
        DataType = cl_image.indDataType('Unknown');
    case 'SOUNDER_MODE'
        DataType = cl_image.indDataType('Unknown');
    case 'OBLIQUE_DIST'
        DataType = cl_image.indDataType('Unknown');
    case 'SIMRAD_PIX_NB'
        DataType = cl_image.indDataType('Unknown');
    case 'TRANS_DIST'
        DataType = cl_image.indDataType('Unknown');
    case 'SIMRAD_PING_NB'
        DataType = cl_image.indDataType('Unknown');
    case 'STR_ROW_NB'
        DataType = cl_image.indDataType('Unknown');
    case 'FREQUENCY'
        DataType = cl_image.indDataType('Unknown');
    case 'INTERLACING'
        DataType = cl_image.indDataType('Unknown');
    case 'BS_DELTA'
        DataType = cl_image.indDataType('Unknown');
    case 'LINE_NUMBER'
        DataType = cl_image.indDataType('Unknown');
    case 'COLUMN_NUMBER'
        DataType = cl_image.indDataType('Unknown');
    case 'LATITUDE'
        DataType = cl_image.indDataType('Latitude');
    case 'LONGITUDE'
        DataType = cl_image.indDataType('Longitude');
        
    otherwise
        message = sprintf('Le layer %s ne semble pas �tre un layer de Caraibes. Il est donc assimil� comme "Other"', str);
        my_warndlg(['cl_car_ima/view : ', message], 1);
        DataType = cl_image.indDataType('Unknown');
end
