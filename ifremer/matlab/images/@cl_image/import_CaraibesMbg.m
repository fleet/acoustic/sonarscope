% Affichage de l'image
%
% Input Arguments
%   a : objet de type cl_car_ima
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex1.mbb')
%   a = import_CaraibesMbg(cl_image, nomFic);
%   SonarScope(a)
%   import_CaraibesMbg(cl_image, nomFic, 'suby', 1:100);
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [c, Carto, indLayerImport, SonarIdent, ListeFlagsInvalides] = import_CaraibesMbg(this, nomFic, varargin) %#ok

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor]       = getPropertyValue(varargin, 'isUsingParfor',       isUsingParfor);
[varargin, Carto]               = getPropertyValue(varargin, 'Carto',               []);
[varargin, SonarIdent]          = getPropertyValue(varargin, 'Sonar.Ident',         []);
[varargin, TagSynchroXY]        = getPropertyValue(varargin, 'TagSynchroXY',        num2str(rand(1)));
[varargin, indLayerImport]      = getPropertyValue(varargin, 'indLayerImport',      []);
[varargin, ListeFlagsInvalides] = getPropertyValue(varargin, 'ListeFlagsInvalides', [-4 -2 -1 0 1]);

in = cl_netcdf('fileName', nomFic);

mbCycleNbr   = get_valDim(in, 'mbCycleNbr');
mbBeamNbr    = get_valDim(in, 'mbBeamNbr');
mbAntennaNbr = get_valDim(in, 'mbAntennaNbr');
if mbAntennaNbr == 2
    subBab = 1:(mbBeamNbr/2);
    subTri = (mbBeamNbr/2+1:mbBeamNbr);
end

% mbCycleCounter = get_valAtt(in, 'mbCycleCounter');
% mbCycleCounter = double(mbCycleCounter);
y = 1:mbCycleNbr;

[varargin, suby] = getPropertyValue(varargin, 'suby', 1:mbCycleNbr); %#ok<ASGLU>

if length(suby) < 2
    c = [];
    return
end

InitialFileName   = nomFic;
InitialFileFormat = 'CaraibesMbb'; % ou CaraibesXY

% Il n'y a pas d'identification du sondeur dans le fichier

if isempty(SonarIdent)
    mbSounder  = get_valAtt(in, 'mbSounder');
    SonarIdent = SonarIdent_caraibes2sonarScope(mbSounder);
end

if isempty(SonarIdent)
    SonarName = 'Unknown';
    SonarDescription = [];
else
    SonarDescription = cl_sounder('Sonar.Ident', SonarIdent);
    SonarIdent = get(SonarDescription, 'Sonar.Ident');
    if isempty(SonarIdent) || (SonarIdent == 0)
        [flag, SonarDescription] = selection(SonarDescription);
        if ~flag
            c = [];
            return
        end
        SonarIdent = get(SonarDescription, 'Sonar.Ident');
    end
    if isempty(SonarDescription)
        my_warndlg('A sounder must absolutely be chosen.', 1);
        c = [];
        return
    end
    SonarName = get(SonarDescription, 'Sonar.Name');
end

SonarBS_etat                    = 1;
SonarBS_origine                 = 1;
SonarBS_origineBelleImage       = 1;
SonarBS_origineFullCompens      = 1;
Sonar_NE_etat                   = 1;
Sonar_SH_etat                   = 1;
Sonar_GT_etat                   = 1;
SonarTVG_ConstructTypeCompens   = 1;
SonarTVG_ConstructTable         = [];
SonarBS_IfremerCompensModele    = [];
SonarBS_IfremerCompensTable     = [];

switch SonarName
    case {'EM122'; 'EM710'; 'EM2040'; 'EM712'; 'EM302'; 'EM304'}
        SonarTVG_ConstructAlpha             = pi*10;    % TODO
        SonarTVG_ConstructConstante         = 0;
        SonarTVG_ConstructCoefDiverg        = 40;
        
        SonarDiagEmi_etat                   = 1;
        SonarDiagEmi_origine                = 1;
        SonarDiagEmi_ConstructTypeCompens   = 1;
        
        SonarDiagRec_etat                   = 1;
        SonarDiagRec_origine                = 1;
        SonarDiagRec_ConstructTypeCompens   = 1;
        
    case 'EM1002'
        SonarTVG_ConstructAlpha             = pi*10;    % TODO
        SonarTVG_ConstructConstante         = 0;
        SonarTVG_ConstructCoefDiverg        = 40;
        
        SonarDiagEmi_etat                   = 1;
        SonarDiagEmi_origine                = 1;
        SonarDiagEmi_ConstructTypeCompens   = 1;
        
        SonarDiagRec_etat                   = 1;
        SonarDiagRec_origine                = 1;
        SonarDiagRec_ConstructTypeCompens   = 1;
        
    case {'EM12D'; 'EM12S'}
%         if ~isdeployed
%             disp('cl_image/import_CaraibesMbg : Il faudra ici d�composer toutes ces infos en 2 series de signaux : b�bord et tribord')
%         end
        SonarTVG_ConstructAlpha             = 1.1;
        SonarTVG_ConstructConstante         = 0;
        SonarTVG_ConstructCoefDiverg        = 40;
        
        SonarDiagEmi_etat                   = 1;
        SonarDiagEmi_origine                = 1;
        SonarDiagEmi_ConstructTypeCompens   = 1;
        
        SonarDiagRec_etat                   = 1;
        SonarDiagRec_origine                = 1;
        SonarDiagRec_ConstructTypeCompens   = 1;
        
    case 'ME70'
        SonarTVG_ConstructAlpha             = 1.1;
        SonarTVG_ConstructConstante         = 0;
        SonarTVG_ConstructCoefDiverg        = 40;
        
        SonarDiagEmi_etat                   = 2;
        SonarDiagEmi_origine                = 1;
        SonarDiagEmi_ConstructTypeCompens   = 1;
        
        SonarDiagRec_etat                   = 1;
        SonarDiagRec_origine                = 1;
        SonarDiagRec_ConstructTypeCompens   = 1;
        
    otherwise
        if ~isdeployed
            %             disp('ATTENTION : SonarTVG_ConstructAlpha � definir pour ce sondeur dans cl_image/import_CaraibesMbg')
        end
        SonarTVG_ConstructAlpha             = NaN;
        SonarTVG_ConstructConstante         = 0;
        SonarTVG_ConstructCoefDiverg        = 40;
        
        SonarDiagEmi_etat                   = 1;
        SonarDiagEmi_origine                = 1;
        SonarDiagEmi_ConstructTypeCompens   = 1;
        
        SonarDiagRec_etat                   = 1;
        SonarDiagRec_origine                = 1;
        SonarDiagRec_ConstructTypeCompens   = 1;
end

%% Lecture des layers

[~, nomDim] = get_varDim(in, 'mbDepth');
[~, listeLayers] = find_listVarAssociated2Dim(in, nomDim);

N = length(listeLayers);
DataType = zeros(1,N);
UnitLayers = cell(1,N);
initLoadLayers = zeros(1,N);
for k=1:N
    switch listeLayers{k}
        case 'mbDepth'
            DataType(k)   = cl_image.indDataType('Bathymetry');
            UnitLayers{k} = 'm';
            initLoadLayers(k) = 1;
        case 'mbAlongDistance'
            DataType(k)   = cl_image.indDataType('AlongDist');
            UnitLayers{k} = 'm';
            initLoadLayers(k) = 1;
        case 'mbAcrossDistance'
            DataType(k)   = cl_image.indDataType('AcrossDist');
            UnitLayers{k} = 'm';
            initLoadLayers(k) = 1;
        case 'mbSFlag'
            DataType(k)   = cl_image.indDataType('Mask');
            UnitLayers{k} = ' ';
        case 'mbReflectivity'
            DataType(k)   = cl_image.indDataType('Reflectivity');
            UnitLayers{k} = 'dB';
            initLoadLayers(k) = 1;
        case 'mbAlongSlope'
            DataType(k)   = cl_image.indDataType('SlopeAlong');
            UnitLayers{k} = 'deg';
        case 'mbAcrossSlope'
            DataType(k)   = cl_image.indDataType('SlopeAcross');
            UnitLayers{k} = 'deg';
        case 'mbRange'
            DataType(k)   = cl_image.indDataType('RayPathLength');
            UnitLayers{k} = 'm';
        case 'mbReceptionHeave'
            DataType(k)   = cl_image.indDataType('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            UnitLayers{k} = 'm';
        case 'mbAzimutBeamAngle'
            DataType(k)   = cl_image.indDataType('BeamAzimuthAngle');
            UnitLayers{k} = 'deg';
        case 'mbAcrossBeamAngle'
            DataType(k)   = cl_image.indDataType('TxAngle');
            UnitLayers{k} = 'deg';
            initLoadLayers(k) = 1;
        case 'mbSQuality'
            DataType(k)   = cl_image.indDataType('MbesQualityFactor');
            UnitLayers{k} = ' ';
        case 'mbSoundingBias'
            DataType(k)   = cl_image.indDataType('Unknown');
            UnitLayers{k} = 'm';
        case 'mbSLengthOfDetection'
            DataType(k)   = cl_image.indDataType('KongsbergLengthDetection');
            UnitLayers{k} = '';
        case 'mbQuality'
            DataType(k)   = cl_image.indDataType('IfremerQF');
            UnitLayers{k} = '';
            
        otherwise
            if ~isdeployed
                msg = sprintf('cl_image/import_CaraibesMbg : Layer "%s" pas traite ici', listeLayers{k});
                my_warndlg(msg, 0, 'Tag', 'import_CaraibesMbgLayerNonTraite');
            end
            DataType(k)   = 1;
            UnitLayers{k} = '';
    end
end

%% Traitement sp�cial du layer "Detection" qui est d�duit du layer "mbSQuality"

ismbSQuality = strcmp(listeLayers, 'mbSQuality');
if any(ismbSQuality)
    listeLayers{end+1} = 'Detection';
    DataType(end+1)    = cl_image.indDataType('DetectionType');
    UnitLayers{end+1}  = ' ';
    initLoadLayers(end+1) = 1;
end

if isempty(indLayerImport)
    [indLayerImport, flag] = my_listdlg('Layers :', listeLayers, 'InitialValue', find(initLoadLayers));
    if ~flag
        c = [];
        return
    end
end

if isempty(indLayerImport)
    c = [];
    return
end
drawnow

%% 

[~, nomDim_mbDate] = get_varDim(in, 'mbDate');
if strcmp(nomDim_mbDate{1}, 'mbCycleNbr')
    NomSub = 'sub1'; % si la premi�re dimension est 'mbCycleNbr'
else
    NomSub = 'sub2'; % si la premi�re dimension est ''
end
Date  = get_value(in, 'mbDate', NomSub, suby);
Heure = get_value(in, 'mbTime', NomSub, suby);

subDatesKO = (Date == 2440588); % 01/01/1970  00:00:00.000
Date(subDatesKO)  = [];
Heure(subDatesKO) = [];
suby(subDatesKO)  = [];

t = cl_time('timeIfr', Date, Heure);  % Attention ERREUR SUR ~/TMP/G16, LES HEURES NE SONT PAS LES MEMES

mbSoundVelocity     = get_value(in, 'mbSoundVelocity',     NomSub, suby);
mbHeading           = get_value(in, 'mbHeading',           NomSub, suby);
mbRoll              = get_value(in, 'mbRoll',              NomSub, suby);
mbPitch             = get_value(in, 'mbPitch',             NomSub, suby);
mbTransmissionHeave = get_value(in, 'mbTransmissionHeave', NomSub, suby);

Latitude            = get_value(in, 'mbOrdinate',          NomSub, suby, 'Convert', 'double');
Longitude           = get_value(in, 'mbAbscissa',          NomSub, suby, 'Convert', 'double');
mbSounderMode       = get_value(in, 'mbSounderMode',       NomSub, suby);

% Traitement sp�cial du mode en fonction du sondeur :
% - Le mode commence � 1 pour les ondeurs EM12D, EM12S et EM1000
% - Le mode commence � 0 pour tous les autres sondeurs
switch SonarName
    case {'EM112D'; 'EM12S'; 'EM1000'}
    otherwise
        % mbSounderMode peut etre egal � 0 mais missing_value est aussi
        % �gal � 0
        mbSounderMode(isnan(mbSounderMode)) = 0;
        mbSounderMode = mbSounderMode + 1;
end

mbCycle = get_value(in, 'mbCycle',  NomSub, suby);
% Verue pour corriger bug fichier Delphine "EW9803-Pr01.mbg"
if all(isnan(mbCycle))
    mbCycle = (1:length(mbCycle))';
end

mbSamplingRate = get_value(in, 'mbSamplingRate', NomSub, suby);

%% Signaux

mbCFlag = get_value(in, 'mbCFlag');
mbBFlag = get_value(in, 'mbBFlag');

% mbInterlacing       = get_value(in, 'mbInterlacing',       NomSub, suby);
% mbCompensationLayerMode  = get_value(in, 'mbCompensationLayerMode');
% mbFrequency         = get_value(in, 'mbFrequency',       NomSub, suby);
% mbReferenceDepth    = get_value(in, 'mbReferenceDepth',       NomSub, suby);
% mbDynamicDraught    = get_value(in, 'mbDynamicDraught',       NomSub, suby);
% mbTide              = get_value(in, 'mbTide',              NomSub, suby);
% mbCQuality  = get_value(in, 'mbCQuality');
% mbAFlag     = get_value(in, 'mbAFlag');
% mbAntenna   = get_value(in, 'mbAntenna');

% mbVelProfilRef  = get_value(in, 'mbVelProfilRef');
% mbVelProfilIdx  = get_value(in, 'mbVelProfilIdx');
% mbVelProfilDate = get_value(in, 'mbVelProfilDate');
% mbVelProfilTime = get_value(in, 'mbVelProfilTime');


% unwrap necessaire pour fichier REFCar_EM12D.mbb qui donne 4 fois les m�me
% num�ros de suite
mbCycle = unwrapPingCounter(mbCycle);
if isempty(mbCycle)
    c = [];
    return
end

sub = (Latitude == 0) & (Longitude == 0);
Latitude(sub) = NaN;
Longitude(sub) = NaN;
if (max(Latitude(:)) > 90) || (min(Latitude(:)) < -90) || (max(Longitude(:)) > 180) || (min(Longitude(:)) < -180)
    Latitude  = Latitude  / 1e6;
    Longitude = Longitude / 1e6;
end

%% Lecture du layer Depth

mbDepthScale = get_value(in, 'mbDepthScale', NomSub, suby, 'Convert', 'single');

% GLU :
% le 23/12/2011, correctif pour la lecture des nx formats CARAIBES MBG avec
% attributs scale_factor et add_offset ramen�s au niveau de la variable.
if isempty(mbDepthScale)
    %     numVar          = find_numVar(in, 'mbDepth');
    %     mbDepthScale    = get_valVarAtt(in, numVar, 'scale_factor');
    mbDepthScale = 1; % Correction GLT 23/01/2014
end

Depth = get_value(in, 'mbDepth', NomSub, suby);
subDepthNaN = isnan(Depth);

if mbAntennaNbr == 1
    if isscalar(mbDepthScale)
        Depth = -Depth * mbDepthScale;
    else
        for ienr = 1:size(Depth,1)
            Depth(ienr,:) = -Depth(ienr,:) * mbDepthScale(ienr);
        end
    end
else
    for ienr=1:size(Depth,1)
        if length(mbDepthScale) == 1 % Rajout� par JMA le 08/065/2013 : action GLU
            Depth(ienr,subBab) = -Depth(ienr,subBab) * mbDepthScale;
        elseif size(mbDepthScale,2) == 2
            Depth(ienr,subBab) = -Depth(ienr,subBab) * mbDepthScale(ienr,1);
        else
            Depth(ienr,subBab) = -Depth(ienr,subBab) * mbDepthScale(ienr);
        end
        
        if length(mbDepthScale) == 1 % Rajout� par JMA le 08/065/2013 : action GLU
            Depth(ienr,subTri) = -Depth(ienr,subTri) * mbDepthScale;
        elseif size(mbDepthScale,2) == 2
            Depth(ienr,subTri) = -Depth(ienr,subTri) * mbDepthScale(ienr,2);
        else
            Depth(ienr,subTri) = -Depth(ienr,subTri) * mbDepthScale(ienr);
        end
    end
end

%% Lecture des flags

% mbSFlag = get_value(in, 'mbSFlag', NomSub, suby, 'CharIsInt8'); %'CharIsByte');
mbSFlag = get_value(in, 'mbSFlag', NomSub, suby);
% mbSFlag(subDepthNaN) = NaN;

%% Masquage des sondes

% ListeFlagsInvalides = unique([ListeFlagsInvalides 0]); % TODO GLU :
% comment� par JMA le 02/07/2013 : donn�es export�es � partir d'un .all
ListeFlagsInvalides = unique(ListeFlagsInvalides);
if ~isempty(ListeFlagsInvalides)
    for ienr=1:size(Depth,1)
        X = mbSFlag(ienr,:);
        sub = (X == ListeFlagsInvalides(1));
        for k2=2:length(ListeFlagsInvalides)
            sub = sub | (X == ListeFlagsInvalides(k2)) | (mbBFlag == ListeFlagsInvalides(k2)) | (mbCFlag(ienr) == ListeFlagsInvalides(k2));
        end
        Depth(ienr,sub) = NaN;
    end
end

%% Vertical Depth

iMilieu = floor(size(Depth, 2) / 2);
HeightMilieu = Depth(:,iMilieu);
% mbVerticalDepth = get_value(in, 'mbVerticalDepth', NomSub, suby);
% mbVerticalDepth = -mbVerticalDepth .* mbDepthScale;
% Diff = HeightMilieu - mean(mbVerticalDepth, 2);
SonarHeight = HeightMilieu;
% SonarHeight = mbVerticalDepth;

GeometryType = cl_image.indGeometryType('PingBeam');    % BathyFais

for k=1:length(indLayerImport)
    nomLayer = listeLayers{indLayerImport(k)};
    switch nomLayer
        case 'mbDepth'
            X = Depth;
            
        case 'mbAlongDistance'
            mbAlongDistanceScale = get_value(in, 'mbDistanceScale', NomSub, suby);
            
            X = get_value(in, 'mbAlongDistance', NomSub, suby);
            if mbAntennaNbr == 1
                for ienr = 1:size(X,1)
                    X(ienr,:) = X(ienr,:) * mbAlongDistanceScale(ienr);
                end
            else
                for ienr = 1:size(Depth,1)
                    X(ienr,subBab) = X(ienr,subBab) * mbAlongDistanceScale(ienr, 1);
                    X(ienr,subTri) = X(ienr,subTri) * mbAlongDistanceScale(ienr, 2);
                end
            end
            X(subDepthNaN) = NaN;
            
        case 'mbAcrossDistance'
            mbAcrossDistanceScale = get_value(in, 'mbDistanceScale', NomSub, suby);
            
            X = get_value(in, 'mbAcrossDistance', NomSub, suby);
            if mbAntennaNbr == 1
                for ienr = 1:size(X,1)
                    X(ienr,:) = X(ienr,:) * mbAcrossDistanceScale(ienr);
                end
            else
                for ienr = 1:size(Depth,1)
                    X(ienr,subBab) = X(ienr,subBab) * mbAcrossDistanceScale(ienr, 1);
                    X(ienr,subTri) = X(ienr,subTri) * mbAcrossDistanceScale(ienr, 2);
                end
            end
            X(subDepthNaN) = NaN;
            
        case 'mbRange'
            mbRangeScale = get_value(in, 'mbRangeScale', NomSub, suby);
            
            X = get_value(in, 'mbRange', NomSub, suby);
            if mbAntennaNbr == 1
                for ienr = 1:size(X,1)
                    X(ienr,:) = X(ienr,:) * mbRangeScale(ienr);
                end
            else
                for ienr = 1:size(Depth,1)
                    X(ienr,subBab) = X(ienr,subBab) * mbRangeScale(ienr, 1);
                    X(ienr,subTri) = X(ienr,subTri) * mbRangeScale(ienr, 2);
                end
            end
            X(subDepthNaN) = NaN;
            X = X * 1500;
            
        case 'mbAcrossBeamAngle'
            X = get_value(in, 'mbAcrossBeamAngle', NomSub, suby);
            X(subDepthNaN) = NaN;
            
        case 'mbAzimutBeamAngle'
            X = get_value(in, 'mbAzimutBeamAngle', NomSub, suby);
            X(subDepthNaN) = NaN;
            
        case 'mbSQuality'
            X = get_value(in, 'mbSQuality', NomSub, suby);
            X(subDepthNaN) = NaN;
            
        case 'Detection'
            X = get_value(in, 'mbSQuality', NomSub, suby);
            X = (X > 127);
            X = single(X) + 1;
            X(subDepthNaN) = NaN;
            
        case 'mbSFlag'
            X = mbSFlag;
            
        case 'mbReflectivity'
            X = get_value(in, 'mbReflectivity', NomSub, suby);
            X(subDepthNaN) = NaN;
            
        case 'mbReceptionHeave'
            X = get_value(in, 'mbReceptionHeave', NomSub, suby);
            X(subDepthNaN) = NaN;
            
        otherwise
            X = get_value(in, nomLayer, NomSub, suby);
            %             X = single(X);
            X(subDepthNaN) = NaN;
    end
    
    [~, nomFicSeul] = fileparts(InitialFileName);
    DT = DataType(indLayerImport(k));
    if DT == 1
        Name = [nomFicSeul '_' nomLayer];
    else
        Name = nomFicSeul;
    end
    
    %% Pour corriger import fichier EM3002D Belgica : d�but
    %     SonarScope([X(subyBab,:) X(subyTri,:)])
    %     X = [X(subyBab,:) fliplr(X(subyTri,:))];
    
    % remplacer  y(suby) par y dans appel � cl_image
    
    %% Pour corriger import fichier EM3002D Belgica : fin
    
    if ~strcmp(nomLayer, 'mbSFlag')
        X(isnan(Depth)) = NaN;
    end
    b = cl_image('Image', X, 'Name', Name, 'Unit', UnitLayers{indLayerImport(k)}, ...
        'DataType', DT, 'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroXY, 'TagSynchroY', TagSynchroXY,...
        'XUnit', 'nuBeam', 'y', y(suby), 'YUnit', 'nuPing', ...
        'GeometryType', GeometryType, 'SonarDescription', SonarDescription, ...
        'InitialFileName', InitialFileName, 'InitialFileFormat', InitialFileFormat);
    
    b = set(b, 'SonarTime', t);
    
%     if ~isdeployed
        %         disp('cl_image/import_CaraibesMbg : Attention ici si mbAntennaNbr=2')
%     end
    if mbAntennaNbr == 1
        b = set(b, 'SonarFishLatitude',  Latitude(:));
        b = set(b, 'SonarFishLongitude', Longitude(:));
        b = set(b, 'SonarHeading',       mbHeading(:));
        b = set(b, 'SonarRoll',          mbRoll(:));
        b = set(b, 'SonarPitch',         mbPitch(:));
        b = set(b, 'SonarPingCounter',   mbCycle(:));
    else
        b = set(b, 'SonarFishLatitude',  Latitude(:,1));
        b = set(b, 'SonarFishLongitude', Longitude(:,1));
        b = set(b, 'SonarHeading',       mbHeading(:,1));
        b = set(b, 'SonarRoll',          mbRoll(:,1));
        b = set(b, 'SonarPitch',         mbPitch(:,1));
        b = set(b, 'SonarPingCounter',   mbCycle(:,1));
    end
    b = set(b, 'SonarHeave',             mbTransmissionHeave(:));
    b = set(b, 'SonarSampleFrequency',   mbSamplingRate(:));
    b = set(b, 'SonarSurfaceSoundSpeed', mbSoundVelocity(:));
    
    Immersion = -10; % Immersion definie de maniere autoritaire. Il vaudrait peut-etre mieux poser la question
    Immersion = repmat(Immersion, length(t), 1);
    b = set(b, 'SonarImmersion',  Immersion);
    b = set(b, 'SonarHeight',     -abs(SonarHeight));
    %     Sonar.BathyCel.Z; % A FOURNIR POUR POUVOIR CALCULER PROPREMENT L'ANGLE D'INCIDENCE
    %     Sonar.BathyCel.C; % A FOURNIR POUR POUVOIR CALCULER PROPREMENT L'ANGLE D'INCIDENCE
    
    if ~isempty(mbSounderMode)
        switch SonarName
            case 'EM12D'
                if mbAntennaNbr == 1
                    PortMode_1 = mbSounderMode(:);
                    PortMode_2 = ones(size(PortMode_1));
                    StarMode_1 = mbSounderMode(:);
                    StarMode_2 = ones(size(PortMode_1));
                else
                    PortMode_1 = mbSounderMode(:,1);
                    PortMode_2 = ones(size(PortMode_1));
                    StarMode_1 = mbSounderMode(:,2);
                    StarMode_2 = ones(size(PortMode_1));
                end
                
                b = set(b, 'SonarPortMode_1', PortMode_1);
                b = set(b, 'SonarPortMode_2', PortMode_2);
                b = set(b, 'SonarStarMode_1', StarMode_1);
                b = set(b, 'SonarStarMode_2', StarMode_2);
                
            case {'EM120'; 'EM122'}
                PortMode_1 = mbSounderMode(:);
                PortMode_2 = ones(size(PortMode_1));
                StarMode_1 = PortMode_1(:);
                StarMode_2 = ones(size(PortMode_1));
                
                b = set(b, 'SonarPortMode_1', PortMode_1);
                b = set(b, 'SonarPortMode_2', PortMode_2);
                b = set(b, 'SonarStarMode_1', StarMode_1);
                b = set(b, 'SonarStarMode_2', StarMode_2);
                
            case {'EM300'; 'EM302'}
                PortMode_1 = mbSounderMode(:);
                PortMode_2 = ones(size(PortMode_1));
                StarMode_1 = PortMode_1(:);
                StarMode_2 = ones(size(PortMode_1));
                
                b = set(b, 'SonarPortMode_1', PortMode_1);
                b = set(b, 'SonarPortMode_2', PortMode_2);
                b = set(b, 'SonarStarMode_1', StarMode_1);
                b = set(b, 'SonarStarMode_2', StarMode_2);
                
            case 'EM1002'
                PortMode_1 = mbSounderMode(:);
                PortMode_2 = ones(size(PortMode_1));
                StarMode_1 = mbSounderMode(:);
                StarMode_2 = ones(size(PortMode_1));
                
                b = set(b, 'SonarPortMode_1', PortMode_1);
                b = set(b, 'SonarPortMode_2', PortMode_2);
                b = set(b, 'SonarStarMode_1', StarMode_1);
                b = set(b, 'SonarStarMode_2', StarMode_2);
                
            case 'EM3002D'
                PortMode_1 = mbSounderMode(:);
                PortMode_2 = ones(size(PortMode_1));
                StarMode_1 = mbSounderMode(:);
                StarMode_2 = ones(size(PortMode_1));
                
                b = set(b, 'SonarPortMode_1', PortMode_1);
                b = set(b, 'SonarPortMode_2', PortMode_2);
                b = set(b, 'SonarStarMode_1', StarMode_1);
                b = set(b, 'SonarStarMode_2', StarMode_2);
                
            case 'EM1000'
                %                 TODO
                % Sonar.Interlacing
                
            case 'Reson7150_12kHz'
                % TODO
                
%             otherwise
%                 if ~isdeployed
%                     str1 = sprintf('"%s" pas encore v�rifi� dans "import_CaraibesMbg", envoyez ce fichier � JMA svp.', SonarName);
%                     str2 = sprintf('"%s" not yet checked in "import_CaraibesMbg", send this file to JMA please..', SonarName);
%                     my_warndlg(Lang(str1,str2), 0, 'Tag', 'SondeurNonPrevuDans_import_CaraibesMbg');
%                 end
        end
    end
    
    %     SonarName
    b = set(b,  'Sonar_DefinitionENCours', ...
        'SonarBS_etat',                      SonarBS_etat, ...
        'SonarBS_origine',                   SonarBS_origine, ...
        'SonarBS_origineBelleImage',         SonarBS_origineBelleImage, ...
        'SonarBS_origineFullCompens',        SonarBS_origineFullCompens, ...
        'SonarBS_IfremerCompensModele',      SonarBS_IfremerCompensModele, ...
        'SonarBS_IfremerCompensTable',       SonarBS_IfremerCompensTable, ...
        'SonarTVG_ConstructTypeCompens',     SonarTVG_ConstructTypeCompens, ...
        'SonarTVG_ConstructTable',           SonarTVG_ConstructTable, ...
        'SonarTVG_ConstructAlpha',           SonarTVG_ConstructAlpha, ...
        'SonarTVG_ConstructConstante',       SonarTVG_ConstructConstante, ...
        'SonarTVG_ConstructCoefDiverg',      SonarTVG_ConstructCoefDiverg, ...
        'SonarDiagEmi_etat',                 SonarDiagEmi_etat, ...
        'SonarDiagEmi_origine',              SonarDiagEmi_origine, ...
        'SonarDiagEmi_ConstructTypeCompens', SonarDiagEmi_ConstructTypeCompens, ...
        'SonarDiagRec_etat',                 SonarDiagRec_etat, ...
        'SonarDiagRec_origine',              SonarDiagRec_origine, ...
        'SonarDiagRec_ConstructTypeCompens', SonarDiagRec_ConstructTypeCompens, ...
        'Sonar_NE_etat',                     Sonar_NE_etat, ...
        'Sonar_SH_etat',                     Sonar_SH_etat, ...
        'Sonar_GT_etat',                     Sonar_GT_etat);
    
    %% Definition d'une carto
    
    if isempty(Carto)
        Carto = getDefinitionCarto(b);
    end
    
    b = update_Name(b);
    
    c(k) = set(b, 'Carto', Carto); %#ok<AGROW>
end
