% TODO : remplacer varargout par les variables

function varargout = import_CaraibesSideScan(this, nomFichiers, varargin) %#ok

[varargin, subl]             = getPropertyValue(varargin, 'subl', []);
[varargin, subc]             = getPropertyValue(varargin, 'subc', []);
[varargin, LatitudeMoyenne]  = getPropertyValue(varargin, 'LatitudeMoyenne', NaN);
[varargin, LongitudeMoyenne] = getPropertyValue(varargin, 'LongitudeMoyenne', NaN);
[varargin, Immersion]        = getPropertyValue(varargin, 'Immersion', []);
[varargin, SonarIdent]       = getPropertyValue(varargin, 'Sonar.Ident', []);
[varargin, NumLayer]         = getPropertyValue(varargin, 'NumLayer', []);
[varargin, choixHauteur]     = getPropertyValue(varargin, 'choixHauteur', []);
[varargin, OrigineAlpha]     = getPropertyValue(varargin, 'OrigineAlpha', 4);
[varargin, nomFicCel]        = getPropertyValue(varargin, 'nomFicCel', []);
[varargin, Alpha]            = getPropertyValue(varargin, 'Alpha', []);
[varargin, Temperatue]       = getPropertyValue(varargin, 'Temperatue', []);
[varargin, Salinity]         = getPropertyValue(varargin, 'Salinity', []); %#ok<ASGLU>
% [varargin, SonarTVG_IfremerAlpha] = getPropertyValue(varargin, 'SonarTVG_IfremerAlpha', []);

if ischar(nomFichiers)
    nomFichiers = {nomFichiers};
end

InitialFileFormat = 'CaraibesSonarLateral';

%% Cr�ation de l'instance cli_image

SonarDescription = cl_sounder('Sonar.Ident', SonarIdent);
SonarIdent = get(SonarDescription, 'Sonar.Ident');
Carto = [];

%% Lecture des coordonn�es

ind = 0;
TagSynchroY = {};
for k=1:length(nomFichiers)
    InitialFileName = nomFichiers{k};
    nomFic          = nomFichiers{k}; % En supprimer un ????
    
    in = cl_netcdf('fileName', InitialFileName);
    
    if isempty(SonarIdent) || (SonarIdent == 0)
        [flag, SonarDescription] = selection(SonarDescription, 'Filemame', InitialFileName, 'SelectionObligatoire');
        if ~flag
            varargout{1} = [];
        end
    end
    SonarName = get(SonarDescription, 'Sonar.Name');
    
    nbRows = get_valDim(in, 'grRawImage_NbLin');
    if isempty(subl)
        subl = 1:nbRows;
    end
    
    
    % %     nbImages = get_valAtt(in, 'grLayerNb');
    %     nbCol    = get_valDim(in, 'grRawImage_NbCol');
    
    % ATLANTIDE : grRawImage          grWrkImage          LatitudeImage
    % LongitudeImage      grWrkImage_Emission grWrkImage_dB
    nomLayer = get_valAtt(in, 'grLayerNames');
    % R�paration JMA
    nomLayer = strsplit(nomLayer);
    for i=length(nomLayer):-1:1
        if isempty(nomLayer{i})
            nomLayer(i) =[];
        end
    end
    % ATLANTIDE
    
    [~, nomFic] = fileparts(nomFic);
    
    %% Choix des layers � lire
    
    if isempty(NumLayer)
        if length(nomLayer) == 1
            TagSynchroY = {nomFic};
            s = 1;
        else
            s = my_listdlg('Layers :', nomLayer);
            if isempty(s)
                varargout{1} = [];
                return
            end
            
            for iChoix=1:length(nomLayer)
                TagSynchroY{iChoix} = nomFic; %#ok<AGROW>
            end
        end
    else
        s = NumLayer;
        for is=1:length(s)
            TagSynchroY{is} = nomFi; %#ok<AGROW>
        end
    end
    
    %% S�lection interactive de la zone
    
    pasEncoreCalcule = 1;
    for iLayer=1:length(s)
        i = s(iLayer);
        
        %     nbCol    = get_valDim(in, 'grRawImage_NbCol');
        nbCol(i) = get_valDim(in, [nomLayer{i} '_NbCol']); %#ok<AGROW>
        
        if isempty(subc)
            subc1 = 1:nbCol(i);
            RapportEchelle = 1;
        else
            cDeb = subc(1);
            cFIN = subc(end);
            RapportEchelle = nbCol(i) / nbCol(1);
            cDeb = ceil( cDeb * RapportEchelle);
            cFIN = floor(cFIN * RapportEchelle);
            subc1 = cDeb:cFIN;
        end
        Celerity    = get_valAtt(in, 'grStrCelerity');
        resolRaw    = get_valAtt(in, 'grStrRawPixelSize');
        X           = get_valAtt(in, 'grStrSamplingFrequency');
        resolRaw    = Celerity/(2*X*1e3);
        
        resolD  = get_valVarAtt(in, nomLayer{i}, 'grXPixelSize');
        resolX  = resolD / RapportEchelle;
        
        Milieu = nbCol(i) / 2;
        x = ((1:nbCol(i)) - Milieu) * resolX;
        y = 1:nbRows;
        
        disp(['Lecture du layer : ' nomLayer{i}])
        I = get_value(in,  nomLayer{i}, 'sub1', subc1, 'sub2', subl);
        
        % ------------------------------------------------------------------------------------
        % ATTENTION : Caraibes inverse babord et tribord
        % Ceci a ete valide sur donnees rebent / rapport Axel EHRHOLD Annexe 6, 6ieme fiche signaletique
        % nomDir = '/home3/jackson/Rebent';
        % nomFic = fullfile(nomDir, 'REBENT074.imo');
        % b = view(a, 'Sonar.Ident', 8, 'subl', 4100:4600);
        switch SonarName
            case {'DF1000'; 'SAR'; 'Klein3000'}
                I = my_fliplr(I);
            otherwise
                my_warndlg('cl_imageimport_CaraibesSideScan : Verifier si il faut faire un fliplr comme pour le DF1000', 0, 'Tag', 'cl_imageimport_CaraibesSideScan');
                I = my_fliplr(I);
        end
        
        try
            % Juste pour tester si on aura assez de m�moire, il serait pr�f�rable de lire la m�moire disponible en un seul bloc
            pppp = zeros(size(I), class(I(1))); %#ok<PREALL>
            clear pppp
        catch %#ok<CTCH>
            I = cl_memmapfile('Value', I);
        end
        
        % Verifier sur les autres sonars si il faut faire cela : Sar ??????
        % ------------------------------------------------------------------------------------
        
        NE = get_NE(SonarDescription);
        SH = get_SH(SonarDescription);
        GT = get_GT(SonarDescription);
        
        SonarBS_etat                    = [];
        SonarBS_origine                 = [];
        SonarBS_origineBelleImage       = [];
        SonarBS_origineFullCompens      = [];
        SonarBS_IfremerCompensModele    = [];
        SonarBS_IfremerCompensTable     = [];
        
        switch nomLayer{i}
            case 'grRawImage'
                GeometryType        = cl_image.indGeometryType('PingRange');    % SonarD
                nomVarDate          = 'grRawImage_grSyncJulianDay';
                %                 nomVarHeure         = 'grRawImage_grSyncJulianMs';
                nomVarPingNumber    = 'grRawImage_grSyncPingNumber';
                nomVarImmersion     = 'grRawImage_grSyncImmersion';
                % On ne conserve que la hauteur Real Time, l'optimisation-filtrage Usr
                % n'�tant pas toujours de qualit� ("� la hauteur" !, pfff).
                nomVarPrtRTHeight   = 'grRawImage_grSyncPrtRTHeight';
                %  nomVarStbRTHeight   = 'grRawImage_grSyncStbRTHeight';
                %  nomVarPrtUsrHeight  = 'grRawImage_grSyncPrtUsrHeight';
                %  nomVarStbUsrHeight  = 'grRawImage_grSyncStbUsrHeight';
                nomVarCableOut      = 'grRawImage_grSyncCableOut';
                nomVarSpeed         = 'grRawImage_grSyncSpeed';
                nomVarHeading       = 'grRawImage_grSyncHeading';
                nomVarRoll          = 'grRawImage_grSyncRoll';
                nomVarPitch         = 'grRawImage_grSyncPitch';
                nomVarYaw           = 'grRawImage_grSyncYaw';
                nomVarLatitude      = 'grRawImage_grSyncLatitude';
                nomVarLongitude     = 'grRawImage_grSyncLongitude';
                nomVarSampleFreq    = 'grStrSamplingFrequency';
                resolutionD = resolD;
                resolutionX = resolD;
                TagSynchroX = sprintf('%s - SonarD - %d', nomFic, k);
                Unit = 'dB';
                
            case {'grWrkImage', 'grWrkImage_dB'}
                GeometryType        = cl_image.indGeometryType('PingAcrossDist');    % SonarX
                nomVarDate          = 'grWrkImage_grSyncJulianDay';
                %                 nomVarHeure         = 'grWrkImage_grSyncJulianMs';
                nomVarPingNumber    = 'grWrkImage_grSyncPingNumber';
                nomVarImmersion     = 'grWrkImage_grSyncImmersion';
                nomVarPrtRTHeight   = 'grWrkImage_grSyncPrtRTHeight';
                %                 nomVarStbRTHeight   = 'grWrkImage_grSyncStbRTHeight';
                %                 nomVarPrtUsrHeight  = 'grWrkImage_grSyncPrtUsrHeight';
                %                 nomVarStbUsrHeight  = 'grWrkImage_grSyncStbUsrHeight';
                nomVarCableOut      = 'grWrkImage_grSyncCableOut';
                nomVarSpeed         = 'grWrkImage_grSyncSpeed';
                nomVarHeading       = 'grWrkImage_grSyncHeading';
                nomVarRoll          = 'grWrkImage_grSyncRoll';
                nomVarPitch         = 'grWrkImage_grSyncPitch';
                nomVarYaw           = 'grWrkImage_grSyncYaw';
                nomVarLatitude      = 'grWrkImage_grSyncLatitude';
                nomVarLongitude     = 'grWrkImage_grSyncLongitude';
                resolutionD = resolX;
                resolutionX = resolX;
                TagSynchroX = sprintf('%s - SonarX - %d', nomFic, k);
                Unit = 'dB';
            case {'grWrkImage_Emission', 'grWrkImage_Incidence'}
                GeometryType        = cl_image.indGeometryType('PingRange');    % SonarD
                nomVarDate          = 'grWrkImage_grSyncJulianDay';
                %                 nomVarHeure         = 'grWrkImage_grSyncJulianMs';
                nomVarPingNumber    = 'grWrkImage_grSyncPingNumber';
                nomVarImmersion     = 'grWrkImage_grSyncImmersion';
                nomVarPrtRTHeight   = 'grWrkImage_grSyncPrtRTHeight';
                %                 nomVarStbRTHeight   = 'grWrkImage_grSyncStbRTHeight';
                %                 nomVarPrtUsrHeight  = 'grWrkImage_grSyncPrtUsrHeight';
                %                 nomVarStbUsrHeight  = 'grWrkImage_grSyncStbUsrHeight';
                nomVarCableOut      = 'grWrkImage_grSyncCableOut';
                nomVarSpeed         = 'grWrkImage_grSyncSpeed';
                nomVarHeading       = 'grWrkImage_grSyncHeading';
                nomVarRoll          = 'grWrkImage_grSyncRoll';
                nomVarPitch         = 'grWrkImage_grSyncPitch';
                nomVarYaw           = 'grWrkImage_grSyncYaw';
                nomVarLatitude      = 'grWrkImage_grSyncLatitude';
                nomVarLongitude     = 'grWrkImage_grSyncLongitude';
                resolutionD = resolX;
                resolutionX = resolX;
                TagSynchroX = sprintf('%s - SonarD - %d', nomFic, k);
                Unit = 'degrees';
            otherwise
                GeometryType        = cl_image.indGeometryType('GeoYX');
                nomVarDate          = [];
                %                 nomVarHeure         = [];
                nomVarPingNumber    = [];
                nomVarImmersion     = [];
                nomVarPrtRTHeight   = [];
                %                 nomVarStbRTHeight   = [];
                %                 nomVarPrtUsrHeight  = [];
                %                 nomVarStbUsrHeight  = [];
                nomVarCableOut      = [];
                nomVarSpeed         = [];
                nomVarHeading       = [];
                nomVarRoll          = [];
                nomVarPitch         = [];
                nomVarYaw           = [];
                nomVarLatitude      = [];
                nomVarLongitude     = [];
                TagSynchroX = TagSynchroY;
                Unit = '???';
        end
        
        if ~isempty(nomVarDate)
            Date  = get_value(in, 'grRawImage_grSyncJulianDay', 'sub1', subl);
            Heure = get_value(in, 'grRawImage_grSyncJulianMs', 'sub1', subl);
            t = cl_time('timeIfr', Date, Heure);
        end
        
        switch SonarName
            case 'DF1000'
                % Caraibes fait  50*log10(I)
                % Pour passer de 50*log10(I) a 20*log10(I) on divise par 2.5
                
                subBab = find(x < 0);
                subTri = find(x >= 0);
                
                disp('Message pour JMA : Demander si dB (1) ou Amplitude (3)')
                if length(NE) == 2
                    TypeCompensationSAR = 1;
                else
                    TypeCompensationSAR = 2;
                end
                switch TypeCompensationSAR
                    case 1 % Cas normal
                        I(:,subBab) = (I(:,subBab) / 2.5) - (20*log10((2^15 - 1) / 5) + NE(1) + SH(1) + GT(1));
                        I(:,subTri) = (I(:,subTri) / 2.5) - (20*log10((2^15 - 1) / 5) + NE(2) + SH(2) + GT(2));
                        
                        Sonar_NE_etat = 1;
                        Sonar_SH_etat = 1;
                        Sonar_GT_etat = 1;
                        SonarBS_etat = 2;  % Image Not compensatede
                        
                    case 2 % Pas de compensation mais en dB
                        I(:,subBab) = (I(:,subBab) / 2.5);
                        I(:,subTri) = (I(:,subTri) / 2.5);
                        
                        Sonar_NE_etat = 0;
                        Sonar_SH_etat = 0;
                        Sonar_GT_etat = 0;
                        SonarBS_etat = 2;  % Image Not compensatede
                        
                    case 3 % Pas de compensation mais en niveaux de gris
                        Sonar_NE_etat = 0;
                        Sonar_SH_etat = 0;
                        Sonar_GT_etat = 0;
                        SonarBS_etat = 2;  % Image Not compensatede
                        Unit = 'Amp';
                end
                
            case 'DTS1'
                % Caraibes fait  90*log10(I)
                % Pour passer de 90*log10(I) a 20*log10(I) on divise par 4.5
                
                if GeometryType == cl_image.indGeometryType('PingRange')
                    subBab = find(x < 0);
                    subTri = find(x >= 0);
                    
                    %                     I(:,subBab) = (I(:,subBab) / 100) - (20*log10((2^15 - 1) / 5) + NE(1) + SH(1) + GT(1));
                    %                     I(:,subTri) = (I(:,subTri) / 100) - (20*log10((2^15 - 1) / 5) + NE(2) + SH(2) + GT(2));
                    I(:,subBab) = (I(:,subBab) / 4.5) - (20*log10((2^15 - 1) / 5) + NE(1) + SH(1) + GT(1));
                    I(:,subTri) = (I(:,subTri) / 4.5) - (20*log10((2^15 - 1) / 5) + NE(2) + SH(2) + GT(2));
                end
                Sonar_NE_etat = 1;
                Sonar_SH_etat = 1;
                Sonar_GT_etat = 1;
                
                SonarBS_etat = 2;  % Image Not compensatede
                
            case 'SAR'
                % Caraibes ne transforma pas la valeur
                if GeometryType == cl_image.indGeometryType('PingRange')
                    
                    % Pour passer de 50*log10() a 20*log10()
                    
                    subBab = find(x < 0);
                    nShift = floor(9 * 2.5 / resolX);
                    Milieu = floor(size(I,2) / 2);
                    % I(:,(nShift:Milieu)) = I(:,(1:Milieu-(nShift-1)));
                    I(:,(Milieu+1:end-(nShift-1))) = I(:,(Milieu+nShift):end);
                    subTri = find(x >= 0);
                    
                    %                 I(:,subBab) = (I(:,subBab) / 100) - (20*log10((2^15 - 1) / 5) + NE(1) + SH(1) + GT(1));
                    %                 I(:,subTri) = (I(:,subTri) / 100) - (20*log10((2^15 - 1) / 5) + NE(2) + SH(2) + GT(2));
                    
                    [nuprom, nuLoi, GFb, GFt, SHb, SHt] = getNuLoi(Date(1), Heure(1));
                    [D, a0, DeltaG, LimiteGMax] = getParamEprom(nuprom, nuLoi);
                    TVG_SAR = TvgSar(D, a0, DeltaG, LimiteGMax);
                    
                    GM = 36;
                    GC = 20 * log10(51);
                    %                     NEb = 227;
                    %                     NEt = 225;
                    
                    
                    % Calcul du Niveau acoustique recu
                    % --------------------------------
                    
                    if GeometryType == cl_image.indGeometryType('PingRange')
                        TypeCompensationSAR = 3;
                        switch TypeCompensationSAR
                            case 1 % Cas normal
                                NCb = 2 * reflec_Enr2dB(I(:,subBab));
                                NCt = 2 * reflec_Enr2dB(I(:,subTri));
                                if ~isreal(NCb)
                                    NCb = abs(NCb);
                                end
                                if ~isreal(NCt)
                                    NCt = abs(NCt);
                                end
                                
                                TVG_SAR_b = interp1(linspace(-750, 750, 6000), [fliplr(TVG_SAR) TVG_SAR], x(subBab));
                                TVG_SAR_t = interp1(linspace(-750, 750, 6000), [fliplr(TVG_SAR) TVG_SAR], x(subTri));
                                for iPing=1:size(NCb, 1)
                                    I(iPing,subBab) = NCb(iPing,:) - SHb - TVG_SAR_b - GFb - GM - GC;
                                    I(iPing,subTri) = NCt(iPing,:) - SHt - TVG_SAR_t - GFt - GM - GC;
                                end
                                Unit = 'dB';
                            case 2 % Pas de compensation mais en dB
                                NCb = 2 * reflec_Enr2dB(I(:,subBab));
                                NCt = 2 * reflec_Enr2dB(I(:,subTri));
                                
                                if ~isreal(NCb)
                                    NCb = abs(NCb);
                                end
                                if ~isreal(NCt)
                                    NCt = abs(NCt);
                                end
                                for iPing=1:size(NCb, 1)
                                    I(iPing,subBab) = NCb(iPing,:);
                                    I(iPing,subTri) = NCt(iPing,:);
                                end
                                Unit = 'dB';
                            case 3 % Pas de compensation mais en niveaux de gris
                                Unit = 'Amp';
                        end
                    end
                    
                    % Traitements des antennes
                    % ------------------------
                    
                    %                 sub = find(~isnan(angle_pixels));
                    %                 indicesBab = interp1(AntennesAnglesBab, 1:size(AntennesAnglesBab, 2), angle_pixels(sub), 'nearest');
                    %                 indicesTri = interp1(AntennesAnglesTri, 1:size(AntennesAnglesTri, 2), angle_pixels(sub), 'nearest');
                    %
                    
                    %                     alphaBabMaison = 33.4;
                    %                     alphaTriMaison = 36.5;
                    %                     OffetTribordSurBabord = 8;
                    
                    %                 TVGb = TvgSarMaison(alphaBabMaison, HB(i));
                    %                 TVGt = TvgSarMaison(alphaTriMaison, HB(i));
                    %
                    %                 %BSb(sub) = NRb(sub) - NEb - DVb(indicesBab) + TVGb(sub);
                    %                 %BSt(sub) = NRt(sub) - NEt - DVt(indicesTri) + TVGt(sub) + OffetTribordSurBabord;
                    %                 BSb(sub) = NRb(sub) - NEb + TVGb(sub);
                    %                 BSt(sub) = NRt(sub) - NEt + TVGt(sub) + OffetTribordSurBabord;
                    
                end
                Sonar_NE_etat = 1;
                Sonar_SH_etat = 1;
                Sonar_GT_etat = 1;
                
                SonarBS_etat = 2;  % Image Not compensatede
                
                
                
                
            case {'GeoSwath+'; 'GeoSwath'} % ATTENTION : copi�/coll� du DF1000
                % Caraibes fait  50*log10(I)
                % Pour passer de 50*log10(I) a 20*log10(I) on divise par 2.5
                
                subBab = find(x < 0);
                subTri = find(x >= 0);
                
                disp('Message pour JMA : Demander si dB (1) ou Amplitude (3)')
                TypeCompensationSAR = 1;
                switch TypeCompensationSAR
                    case 1 % Cas normal
                        I(:,subBab) = (I(:,subBab) / 2.5) - (20*log10((2^15 - 1) / 5) + NE(1) + SH(1) + GT(1));
                        I(:,subTri) = (I(:,subTri) / 2.5) - (20*log10((2^15 - 1) / 5) + NE(2) + SH(2) + GT(2));
                        
                        Sonar_NE_etat = 1;
                        Sonar_SH_etat = 1;
                        Sonar_GT_etat = 1;
                        SonarBS_etat = 2;  % Image Not compensatede
                        
                    case 2 % Pas de compensation mais en dB
                        I(:,subBab) = (I(:,subBab) / 2.5);
                        I(:,subTri) = (I(:,subTri) / 2.5);
                        
                        Sonar_NE_etat = 0;
                        Sonar_SH_etat = 0;
                        Sonar_GT_etat = 0;
                        SonarBS_etat = 2;  % Image Not compensatede
                        
                    case 3 % Pas de compensation mais en niveaux de gris
                        Sonar_NE_etat = 0;
                        Sonar_SH_etat = 0;
                        Sonar_GT_etat = 0;
                        SonarBS_etat = 2;  % Image Not compensatede
                        Unit = 'Amp';
                end
                
            case 'Klein3000'
                % my_warndlg('Inspiration du Klein3000 depuis le DST1 !!! - Valider par JMA', 1);
                
                if GeometryType == cl_image.indGeometryType('PingRange')
                    subBab = find(x < 0);
                    subTri = find(x >= 0);
                    
                    I(:,subBab) = (I(:,subBab) / 100) - (20*log10((2^15 - 1) / 5));
                    I(:,subTri) = (I(:,subTri) / 100) - (20*log10((2^15 - 1) / 5));
                end
                Sonar_NE_etat = 1;
                Sonar_SH_etat = 1;
                Sonar_GT_etat = 1;
                
                SonarBS_etat = 2;  % Image Not compensatede
                
                
            otherwise
                my_warndlg('cl_car_sonar/view : Definir le passage a 20*log10() ici', 1);
        end
        if isa(I, 'double')
            I = single(I);
        end
        %         subInf = find(isinf(I));
        %         I(subInf) = NaN;
        
        %         [nomDir, nomFic] = fileparts(nomFic);
        %         TagSynchroY = sprintf('%s - %d', nomFic, k);
        
        ind = ind + 1;
        [~, nomFicSeul] = fileparts(InitialFileName);
        b(ind) = cl_image('Image', I, 'Name', [nomFicSeul ' - ' nomLayer{i}], 'ColormapIndex', 2, ...
            'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY{iLayer}, ...
            'x', x(subc1), 'XUnit', 'm', 'y', y(subl), 'YUnit', 'Ping', ...
            'GeometryType', GeometryType, 'Unit', Unit, ...
            'SonarDescription', SonarDescription, ...
            'InitialFileName', InitialFileName, 'InitialFileFormat', InitialFileFormat); %#ok<AGROW>
        
        
        if (GeometryType == cl_image.indGeometryType('PingRange')) || (GeometryType == cl_image.indGeometryType('PingAcrossDist'))
            b(ind) = set(b(ind),  'DataType',        cl_image.indDataType('Reflectivity'), ...
                'SonarRawDataResol', resolRaw, ...
                'SonarResolutionD',  resolutionD, ...
                'SonarResolutionX',  resolutionX); %#ok<AGROW>
        end
        
        if ~isempty(nomVarDate)
            b(ind) = set(b(ind), 'SonarTime', t); %#ok<AGROW>
        end
        
        if ~isempty(nomVarPingNumber)
            X = get_value(in, nomVarPingNumber, 'sub1', subl);
            if  any(X)
                b(ind) = set(b(ind), 'SonarPingCounter', X); %#ok<AGROW>
                %            b(ind) = set(b(ind), 'y', X, 'YLim', [X(1) X(end)]);
            end
        end
        
        if isempty(Immersion)
            if isempty(nomVarImmersion)
                Immersion = -10; % Immersion definie de maniere autoritaire. Il vaudrait peut-etre mieux poser la question
                Immersion = repmat(Immersion,length(subl), 1);
                b(ind) = set(b(ind), 'SonarImmersion', Immersion); %#ok<AGROW>
            else
                Immersion = get_value(in, nomVarImmersion, 'sub1', subl);
                if any(Immersion)
                    b(ind) = set(b(ind), 'SonarImmersion', -abs(Immersion(:))); %#ok<AGROW>
                else
                    Immersion = -10; % Immersion definie de maniere autoritaire. Il vaudrait peut-etre mieux poser la question
                    Immersion = repmat(Immersion,length(subl),1);
                    b(ind) = set(b(ind), 'SonarImmersion', Immersion); %#ok<AGROW>
                end
            end
        else
            Immersion = -abs(Immersion);
            Immersion = repmat(Immersion,1,length(subl));
            b(ind) = set(b(ind), 'SonarImmersion', Immersion); %#ok<AGROW>
        end
        
        if ~isempty(nomVarPrtRTHeight)
            listeNomsVar = get_ListeVar(in);
            idx = strfind(listeNomsVar, 'Height');
            liste = {};
            for iVar=1:length(idx)
                if ~isempty(idx{iVar})
                    rep = strfind(listeNomsVar{iVar}, 'Flag');
                    if isempty(rep)
                        liste{end+1} = listeNomsVar{iVar}; %#ok<AGROW>
                    end
                end
            end
            
            if isempty(choixHauteur)
                str1 = 'S�lectionnez une variable :';
                str2 = 'Select a variable :';
                choixHauteur = my_listdlg(Lang(str1,str2), liste, 'SelectionMode', 'Single');
            end
            
            X = get_value(in, liste{choixHauteur}, 'sub1', subl);
            %             if  any(X)
            %                 b(ind) = set(b(ind), 'SonarHeight', X);
            %             end
            X(isnan(X)) = 0;
            b(ind) = set(b(ind), 'SonarHeight', X(:)); %#ok<AGROW>
        end
        
        %        if ~isempty(nomVarStbRTHeight)
        %        end
        %
        %        if ~isempty(nomVarPrtUsrHeight)
        %        end
        %
        %        if ~isempty(nomVarStbUsrHeight)
        %        end
        
        if ~isempty(nomVarCableOut)
            X = get_value(in, nomVarCableOut, 'sub1', subl);
            if  any(X)
                b(ind) = set(b(ind), 'SonarCableOut', X(:)); %#ok<AGROW>
            end
        end
        
        if ~isempty(nomVarSpeed)
            X = get_value(in, nomVarSpeed, 'sub1', subl);
            if  any(X)
                b(ind) = set(b(ind), 'SonarSpeed', X(:)); %#ok<AGROW>
            end
        end
        
        if ~isempty(nomVarHeading)
            X = get_value(in, nomVarHeading, 'sub1', subl);
            if  any(X)
                b(ind) = set(b(ind), 'SonarHeading', X(:)); %#ok<AGROW>
            end
        end
        
        if ~isempty(nomVarRoll)
            X = get_value(in, nomVarRoll, 'sub1', subl);
            if  any(X)
                b(ind) = set(b(ind), 'SonarRoll', X(:)); %#ok<AGROW>
            end
        end
        
        if ~isempty(nomVarPitch)
            X = get_value(in, nomVarPitch, 'sub1', subl);
            if  any(X)
                b(ind) = set(b(ind), 'SonarPitch', X(:)); %#ok<AGROW>
            end
        end
        
        if ~isempty(nomVarYaw)
            X = get_value(in, nomVarYaw, 'sub1', subl);
            if  any(X)
                b(ind) = set(b(ind), 'SonarYaw', X(:)); %#ok<AGROW>
            end
        end
        
        if ~isempty(nomVarLatitude)
            X = get_value(in, nomVarLatitude, 'sub1', subl, 'Convert', 'double');
            if  any(X)
                b(ind) = set(b(ind), 'SonarFishLatitude', X(:)); %#ok<AGROW>
                LatitudeMoyenne = my_nanmean(X);
            end
        end
        
        if ~isempty(nomVarLongitude)
            X = get_value(in, nomVarLongitude, 'sub1', subl, 'Convert', 'double');
            if  any(X)
                b(ind) = set(b(ind), 'SonarFishLongitude', X(:)); %#ok<AGROW>
                LongitudeMoyenne = my_nanmean(X);
            end
        end
        
        if ~isempty(nomVarSampleFreq)
            X = get_valAtt(in, nomVarSampleFreq)*1e3; % en Hz
            if ~isempty(X)
                X = repmat(X, numel(subl), 1);
                b(ind) = set(b(ind), 'SampleFrequency', X(:)); %#ok<AGROW>
            end
        end
        
        SonarTVG_ConstructTypeCompens = 1;
        SonarTVG_ConstructTable       = [];
        switch SonarName
            case 'SAR'
                %                 str = 'ATTENTION : Valeurs du TVG a definir dans cl_car_sonar/view';
                %                 warndlg(str)
                
                %                 SonarTVG_etat                 = 1;
                SonarTVG_ConstructTypeCompens = 2;
                SonarTVG_ConstructTable       = []; % Definition a repporter dans cl_sounder
                SonarTVG_ConstructAlpha       = 0;
                SonarTVG_ConstructConstante   = 0;
                SonarTVG_ConstructCoefDiverg  = 0;
                
                SonarDiagEmi_etat                   = 2;
                SonarDiagEmi_origine                = 1;
                SonarDiagEmi_ConstructTypeCompens   = 1;
                
                SonarDiagRec_etat                   = 2;
                SonarDiagRec_origine                = 1;
                SonarDiagRec_ConstructTypeCompens   = 1;
                
                
            case 'DF1000'
                %                 SonarTVG_etat                       = 1;
                SonarTVG_ConstructAlpha             = 0;
                SonarTVG_ConstructConstante         = -53.5; % 61.5 + 40 * log10(2/1500);
                SonarTVG_ConstructCoefDiverg        = 40;
                
                SonarDiagEmi_etat                   = 2;
                SonarDiagEmi_origine                = 1;
                SonarDiagEmi_ConstructTypeCompens   = 1;
                
                SonarDiagRec_etat                   = 2;
                SonarDiagRec_origine                = 1;
                SonarDiagRec_ConstructTypeCompens   = 1;
                
            case 'DTS1'
                %                 SonarTVG_etat                       = 1;
                SonarTVG_ConstructAlpha             = 0;
                SonarTVG_ConstructConstante         = 0;
                SonarTVG_ConstructCoefDiverg        = 0;
                
                SonarDiagEmi_etat                   = 2;
                SonarDiagEmi_origine                = 1;
                SonarDiagEmi_ConstructTypeCompens   = 1;
                
                SonarDiagRec_etat                   = 2;
                SonarDiagRec_origine                = 1;
                SonarDiagRec_ConstructTypeCompens   = 1;
                
            otherwise
                str = 'ATTENTION : SonarTVG_ConstructAlpha a definir pour ce sondeur dans cl_car_sonar/view';
                warndlg(str)
                SonarTVG_ConstructAlpha             = NaN;
                SonarTVG_ConstructConstante         = 0;
                SonarTVG_ConstructCoefDiverg        = 40;
                
                SonarDiagEmi_etat                   = 2;
                SonarDiagEmi_origine                = 1;
                SonarDiagEmi_ConstructTypeCompens   = 1;
                
                SonarDiagRec_etat                   = 2;
                SonarDiagRec_origine                = 1;
                SonarDiagRec_ConstructTypeCompens   = 1;
        end
        
        b(ind) = set(b(ind),  'Sonar_DefinitionENCours', ...
            'SonarTVG_ConstructTypeCompens',        SonarTVG_ConstructTypeCompens, ...
            'SonarTVG_ConstructTable',              SonarTVG_ConstructTable, ...
            'SonarTVG_ConstructAlpha',              SonarTVG_ConstructAlpha, ...
            'SonarTVG_ConstructConstante',          SonarTVG_ConstructConstante, ...
            'SonarTVG_ConstructCoefDiverg',         SonarTVG_ConstructCoefDiverg, ...
            'SonarDiagEmi_etat',                    SonarDiagEmi_etat, ...
            'SonarDiagEmi_origine',                 SonarDiagEmi_origine, ...
            'SonarDiagEmi_ConstructTypeCompens',    SonarDiagEmi_ConstructTypeCompens, ...
            'SonarDiagRec_etat',                    SonarDiagRec_etat, ...
            'SonarDiagRec_origine',                 SonarDiagRec_origine, ...
            'SonarDiagRec_ConstructTypeCompens',    SonarDiagRec_ConstructTypeCompens, ...
            'Sonar_NE_etat',                        Sonar_NE_etat, ...
            'Sonar_SH_etat',                        Sonar_SH_etat, ...
            'Sonar_GT_etat',                        Sonar_GT_etat); %#ok<AGROW>
        
        
        if isempty(SonarBS_etat)
            SonarBS_etat = get(b(ind), 'SonarBS_etat');
        end
        if isempty(SonarBS_origine)
            SonarBS_origine = get(b(ind), 'SonarBS_origine');
        end
        if isempty(SonarBS_origineBelleImage)
            SonarBS_origineBelleImage = get(b(ind), 'SonarBS_origineBelleImage');
        end
        if isempty(SonarBS_origineFullCompens)
            SonarBS_origineFullCompens = get(b(ind), 'SonarBS_origineFullCompens');
        end
        if isempty(SonarBS_IfremerCompensModele)
            SonarBS_IfremerCompensModele  = get(b(ind), 'SonarBS_IfremerCompensModele');
        end
        if isempty(SonarBS_IfremerCompensTable)
            SonarBS_IfremerCompensTable = get(b(ind), 'SonarBS_IfremerCompensTable');
        end
        
        b(ind) = set(b(ind),  'Sonar_DefinitionENCours', ...
            'SonarBS_etat',                         SonarBS_etat, ...
            'SonarBS_origine',                      SonarBS_origine, ...
            'SonarBS_origineBelleImage',            SonarBS_origineBelleImage, ...
            'SonarBS_origineFullCompens',           SonarBS_origineFullCompens, ...
            'SonarBS_IfremerCompensModele',         SonarBS_IfremerCompensModele, ...
            'SonarBS_IfremerCompensTable',          SonarBS_IfremerCompensTable); %#ok<AGROW>
        
        switch OrigineAlpha
            case 1 % Saisie de la valeur
                SonarTVG_IfremerAlpha = Alpha;
                b(ind) = set(b(ind),  'Sonar_DefinitionENCours', ...
                    'SonarTVG_IfremerAlpha',  SonarTVG_IfremerAlpha); %#ok<AGROW>
                
            case 2 % Calcul� � partir d''une valeur moyenne de Temp�rature et salinit�
                SignalFreq = get(SonarDescription, 'Signal.Freq');
                Immersion = mean(Immersion, 'omitnan');
                SonarTVG_IfremerAlpha = AttenuationGarrison(SignalFreq, Immersion, Temperatue, Salinity);
                b(ind) = set(b(ind),  'Sonar_DefinitionENCours', ...
                    'SonarTVG_IfremerAlpha',  SonarTVG_IfremerAlpha); %#ok<AGROW>
                
            case 3 % Calcul� � partir d''un fichier de c�l�rit�
                CelObjet   = cl_car_cel(nomFicCel);
                Date_Cel   = get(CelObjet, 'mbDate');
                Heure_Cel  = get(CelObjet, 'mbTime');
                tCel = cl_time('timeIfr', Date_Cel, Heure_Cel);
                clear Date_Cel Heure_Cel
                
                %                 indCel = find(tCel < t(end));
                [~, indCel] = sort(abs(tCel - t(end)));
                indCel = indCel(1);
                if indCel == 1
                    Warn = sprintf('Verifiez si l''heure de fin de l''image (%s) est compatible avec la l''heure du premier profil de c�l�rite (%s)', ...
                        t2str(t(end)), t2str(tCel(1)));
                    my_warndlg(Warn, 1);
                end
                if indCel == length(tCel)
                    Warn = sprintf('Verifiez si l''heure de debut de l''image (%s) est compatible avec la l''heure du dernier profil de c�l�rite (%s)', ...
                        t2str(t(1)), t2str(tCel(end)));
                    my_warndlg(Warn, 1);
                end
                
                Depth       = get(CelObjet, 'mbDepth',         'subl', indCel);
                SoundSpeed  = get(CelObjet, 'mbSoundVelocity', 'subl', indCel);
                Temperature = get(CelObjet, 'mbTemperature',   'subl', indCel);
                Salinity    = get(CelObjet, 'mbSalinity',      'subl', indCel);
                SignalFreq = get(SonarDescription, 'Signal.Freq');
                Immer = mean(Immersion, 'omitnan');
                [Alpha, SonarTVG_IfremerAlpha] = AttenuationGarrison(SignalFreq, Depth, Temperature, Salinity, ...
                    'Immersion', Immer);
                [~, indCel] = sort(abs(abs(Depth) - abs(Immer)));
                indCel = indCel(1);
                SonarTVG_IfremerAlpha = SonarTVG_IfremerAlpha(indCel);
                b(ind) = set(b(ind),  'Sonar_DefinitionENCours', ...
                    'SonarTVG_IfremerAlpha',  SonarTVG_IfremerAlpha); %#ok<AGROW>
                
            case 4 % Calcul� � partir de la base Levitus
                Depth = [];
                SonarTVG_IfremerAlpha = NaN;
                if ~isnan(LatitudeMoyenne) && ~isnan(LongitudeMoyenne)
                    if pasEncoreCalcule
                        SignalFreq = get(SonarDescription, 'Signal.Freq');
                        [~, Mois] = dayIfr2Jma(Date(1));
                        
                        disp('Recherche de la bathycelerimetrie')
                        z = cl_sound_speed('Incoming', 2, 'LevitusAveragingType', 3, ...
                            'Latitude', LatitudeMoyenne, 'Longitude', LongitudeMoyenne, ...
                            'Frequency', SignalFreq, 'LevitusMonth', Mois);
                        
                        disp('Fin de la recherche de la bathycelerimetrie')
                        
                        Depth = get(z, 'Depth');
                        if isempty(Depth)
                            str = sprintf('Le coefficient d''attenuation n''a pas pu �tre d�duit de Levitus');
                            my_warndlg(str, 1);
                            
                            [flag, SonarTVG_IfremerAlpha] = inputOneParametre('Absorption coefficient is necessary for reflectivity calibration', 'Absorption coef value', ...
                                'Value', 0, 'Unit', 'dB/km/m', 'MinValue', 0, 'MaxValue', 200);
                            if ~flag
                                return
                            end
                        else
                            %                             AveragedAbsorption = get(z, 'AveragedAbsorption');
                            Temperature = get(z, 'Temperature');
                            Salinity    = get(z, 'Salinity');
                            SoundSpeed  = get(z, 'SoundSpeed');
                            Immersion   = mean(Immersion, 'omitnan'); % Immersion du sonar
                            
                            %              Depth = (Depth(:))';
                            %              AveragedAbsorption = (AveragedAbsorption(:))';
                            
                            [~, iz] = min(abs(abs(Depth - Immersion)));
                            
                            % SonarTVG_IfremerAlpha =  AveragedAbsorption(iz); En commentaire car ne prend pas l'immersion en compte
                            SonarTVG_IfremerAlpha = AttenuationGarrison(SignalFreq, Depth, Temperature, Salinity, 'LimBathyLocale', [-Immersion -Immersion+100], 'Immersion', -Immersion);
                            SonarTVG_IfremerAlpha = SonarTVG_IfremerAlpha(iz);
                            %                             TempSonar     = Temperature(iz);
                            %                             SaliniteSonar = Salinity(iz);
                            
                            pasEncoreCalcule = 0;
                            
                            str1 = sprintf('Le coefficient d''attenuation deduit de Levitus est : %f dB/km/m', SonarTVG_IfremerAlpha);
                            str2 = sprintf('Absorption coefficient deduced from Levitus : %f dB/km/m', SonarTVG_IfremerAlpha);
                            my_warndlg(Lang(str1,str2), 0, 'Tag', 'AbsorptionCoefficientDeduced');
                        end
                    end
                end
                
                
                if ~isempty(Depth)
                    b(ind) = set(b(ind),  'SonarBathyCel_Z', Depth, ...
                        'SonarBathyCel_T', Temperature, ...
                        'SonarBathyCel_S', Salinity, ...
                        'SonarBathyCel_C', SoundSpeed); %#ok<AGROW>
                end
                
                DistanseOblique = abs(x);
                warning('off')
                TVG_Ifremer = (40 * log10(DistanseOblique)) + (2 * SonarTVG_IfremerAlpha * DistanseOblique / 1000);
                TVG_Ifremer(isinf(TVG_Ifremer)) = 0;
                warning('on')
                
                switch SonarName
                    case 'DTS1'
                        SonarMode_1 = get(SonarDescription, 'Sonar.Mode_1');
                        switch SonarMode_1
                            case 1  % 75 kHz
                                t = abs((2 * x) / 1500);    % Pour bien faire, il faudrait estimer la celerite
                                % du fond en fonction du profiul Levitus
                                TVG_Constructeur = zeros(size(t));
                                subt = find(t > 0.530);
                                TVG_Constructeur(subt) = 44.6 * (t(subt) - 0.530);
                            case 2  % 120 kHz
                                t = abs((2 * x) / 1500);
                                TVG_Constructeur = zeros(size(t));
                                subt = find(t > 0.290);
                                TVG_Constructeur(subt) = 76.2 * (t(subt) - 0.290);
                            case 3  % 410 hHz
                                t = abs((2 * x) / 1500);
                                TVG_Constructeur = zeros(size(t));
                                subt = find(t > 0.80);
                                TVG_Constructeur(subt) = 145 * (t(subt) - 0.80);
                        end
                        BilanTGV = TVG_Ifremer - TVG_Constructeur;
                        %             else
                        %                 BilanTGV = TVG_Ifremer;
                        %             end
                        
                        %             DistanseOblique = abs(x);
                        %             warning off
                        %             TVG_Ifremer = (40 * log10(DistanseOblique)) + (2 * SonarTVG_IfremerAlpha * DistanseOblique / 1000);
                        %             warning on
                        
                        for iPing=1:size(I,1)
                            I(iPing,:) = I(iPing,:) + BilanTGV;
                        end
                        
                        b(ind) = set(b(ind), 'Image', I); %#ok<AGROW>
                        StatValues = b(ind).StatValues;
                        b(ind).CLim = [StatValues.Min StatValues.Max]; %#ok<AGROW>
                        b(ind) = set(b(ind), 'Sonar_DefinitionENCours', 'SonarTVG_etat', 1, 'SonarTVG_origine', 2); %#ok<AGROW>
                        
                    case 'SAR'
                        % Commente le 12/12/2005
                        %                     for iPing=1:size(I,1)
                        %                         I(iPing,:) = I(iPing,:) + TVG_Ifremer;
                        %                     end
                        %
                        %                     b(ind) = set(b(ind), 'Image', I);
                        %                     StatValues = b(ind).StatValues;
                        %                     b(ind).CLim = [StatValues.Min StatValues.Max];
                        %                     b(ind) = set(b(ind), 'Sonar_DefinitionENCours', 'SonarTVG_etat', 1, 'SonarTVG_origine', 2);
                        
                end
                
                b(ind) = set(b(ind), 'Name', [nomFicSeul ' - ' nomLayer{i}]);   %#ok<AGROW> % ON reimpose le ImageName car il a ete modifie par b(ind) = set(b(ind),  'S...
                
                if ~isempty(SonarTVG_IfremerAlpha)
                    %              b(ind) = set(b(ind), 'SonarTVG_IfremerAlpha', [Depth; AveragedAbsorption]);
                    b(ind) = set(b(ind),  'Sonar_DefinitionENCours', ...
                        'SonarTVG_IfremerAlpha',  SonarTVG_IfremerAlpha); %#ok<AGROW>
                end
        end
        
        % ----------------------
        % Definition d'une carto
        
        if isempty(Carto)
            Carto = getDefinitionCarto(b(ind));
        end
        b(ind) = set(b(ind),  'Carto', Carto); %#ok<AGROW>
        
        % -----------------------------
        % Mise a jour du nom de l'image
        
        b(ind) = update_Name(b(ind)); %#ok<AGROW>
    end
end

% ----------------------------
% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
else
    varargout{1} = b;
end

%{
% -----------------------------------------------------------------------------------------
function resol = get_grXPixelSize(this, nomLayer)

nomFic = get(this, 'nomFic');
a = cl_netcdf('fileName', nomFic);
sk = get(a, 'skeleton');
for i=1:length(sk.var)
if strcmp(sk.var(i).name, nomLayer)
att = sk.var(i).att;
for j=1:length(att)
if strcmp(att(j).name, 'grXPixelSize')
resol = att(j).value;
return
end
end
end
end
%}

function Carto = getDefinitionCarto(b)

disp('On definit une carto par defaut : a ameliorer')
Lon = get(b, 'FishLongitude');
Lat = get(b, 'FishLatitude');
% Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
%     'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
%     'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));

if my_nanmean(Lat) >= 0
    Hemisphere = 'N';
else
    Hemisphere = 'S';
end
Fuseau = 1 + floor((my_nanmean(Lon) + 180) / 6);
if isnan(Lon)
    Fuseau = 30;
    Hemisphere = 'N';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
    'Projection.UTM.Fuseau', Fuseau, ...
    'Projection.UTM.Hemisphere', Hemisphere);

