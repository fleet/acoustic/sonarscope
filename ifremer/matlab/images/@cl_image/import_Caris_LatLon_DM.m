function [flag, this, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, ...
    IndexColumns, DataType, Carto, TypeAlgo, Unit, pasxy] = import_Caris_LatLon_DM(~, nomFic, varargin)

[varargin, sub]            = getPropertyValue(varargin, 'sub',            []);
[varargin, Carto]          = getPropertyValue(varargin, 'Carto',          []);
[varargin, Separator]      = getPropertyValue(varargin, 'Separator',      []);
[varargin, NbLigEntete]    = getPropertyValue(varargin, 'NbLigEntete',    []);
[varargin, DataType]       = getPropertyValue(varargin, 'DataType',       []);
[varargin, GeometryType]   = getPropertyValue(varargin, 'GeometryType',   []);
[varargin, ValNaN]         = getPropertyValue(varargin, 'ValNaN',         []);
[varargin, nColumns]       = getPropertyValue(varargin, 'nColumns',       []);
[varargin, IndexColumns]   = getPropertyValue(varargin, 'IndexColumns',   []);
[varargin, ScaleFactor]    = getPropertyValue(varargin, 'ScaleFactor',    []);
[varargin, ControleManuel] = getPropertyValue(varargin, 'ControleManuel', 1);
[varargin, pasxy]          = getPropertyValue(varargin, 'pasxy',          []);
[varargin, Unit]           = getPropertyValue(varargin, 'Unit',           []);
[varargin, TypeAlgo]       = getPropertyValue(varargin, 'TypeAlgo',       []);
[varargin, SigmaXY]        = getPropertyValue(varargin, 'SigmaXY',        []); %#ok<ASGLU>

this = [];

%% Create the file

fid = fopen(nomFic, 'r');
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.xyz']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end

for k=1:20
    Entete{k} = fgetl(fid);%#ok
end
fclose(fid);

%% Type de donn�es

if isempty(DataType)
    [flag, DataType] = cl_image.edit_DataType;
    if ~flag
        return
    end
elseif DataType < 0
    [flag, DataType] = cl_image.edit_DataType('DataType', -DataType);
    if ~flag
        return
    end
end

%% Unit

if isempty(Unit)
    [flag, Unit] = cl_image.edit_DataUnit('DataType', DataType);
    if ~flag
        return
    end
end

if (DataType == cl_image.indDataType('Bathymetry')) && strcmp(Unit, 'm')
    ScaleFactor = -1;
end
%% Lecture du fichier

[X, Y, V] = lectureIndirecte(nomFic);

%% Extraction si cela est demand�

if ~isempty(sub)
    X = X(sub);
    Y = Y(sub);
    V = V(sub);
end

%% Suppression des NaN

if ~isempty(ValNaN)
    V(V == ValNaN) = NaN;
end
sub = ~isnan(V);
X = X(sub);
Y = Y(sub);
V = V(sub);
clear sub

%% Supression des points dont les coordonn�es sont hors gabarit

if SigmaXY ~= 0
    S = stats(X);
    sub = find(X < (S.Mediane - SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub) = [];
    end
    sub = find(X > (S.Mediane + SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub) = [];
    end
    S = stats(Y);
    sub = find(Y < (S.Mediane - SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub) = [];
    end
    sub = find(Y > (S.Mediane + SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub) = [];
    end
end

%% Test pour voir si c'est une grille maill�e ou un semis de points

unique_x = unique(X);
unique_y = unique(Y);

if isempty(unique_x) || isempty(unique_y)
    my_warndlg('File reading failure.', 1);
    flag = 0;
    return
end

cols = length(unique_x);
rows = length(unique_y);
N = length(V);

% LatMean = [];
% LonMean = [];

GeometryType = cl_image.indGeometryType('LatLong');
unitexy = 'deg';

x = unique(X);
y = unique(Y);

pas = max(1, floor(N/1e5));

xmin = min(X);
xmax = max(X);
ymin = min(Y);
ymax = max(Y);

if ControleManuel
    str1 = 'Une figure va pr�senter la r�partition des donn�es, vous devrez la d�truire apr�s avoir �ventuellement zoom� sur la zone que vous voulez importer. Le traitement se poursuivra apr�s la destruction de cette fen�tre.';
    str2 = 'A figure will give you a preview of the data, you will have to delete it to continue or zoom on the good part of it and then delete it.';
    my_warndlg(Lang(str1,str2), 1);
    fig = figure;
    axes;
    plot(X(1:pas:end), Y(1:pas:end), '+');
    grid on;
    axis([xmin xmax ymin ymax])
    title('You have to kill me to continue')
    
    nomFicSave = fullfile(my_tempdir,'ffff.mat');
    clbk = sprintf('w = axis; save(''%s'', ''w'')', nomFicSave);
    set(fig, 'DeleteFcn', clbk)
    
    % ----------------
    % Boucle d'attente
    
    while ishandle(fig)
        uiwait(fig);
        w = loadmat(nomFicSave, 'nomVar', 'w');
        delete(nomFicSave)
    end
else
    w = [xmin xmax ymin ymax];
end


%% Carto

if isempty(Carto)
    nomDir = fileparts(nomFic);
    [flagExistenceCarto, Carto, nomFicCarto] = test_ExistenceCarto_Folder(nomDir);
    if flagExistenceCarto
        str1 = sprintf('Le fichier "%s" est interpr�t� directement', nomFicCarto);
        str2 = sprintf('File "%s" is used by default.', nomFicCarto);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
    else
        if GeometryType == cl_image.indGeometryType('LatLong')
            [flag, Carto] = editParams(cl_carto([]), mean([ymin ymax]), mean([xmin xmax]), 'MsgInit');
        else
            [flag, Carto] = editParams(cl_carto([]), [], [], 'MsgInit');
        end
        if ~flag
            return
        end
        
        [rep, flag] = my_questdlg(Lang('Voulez-vous sauver les param�tres cartographiques dans un fichier ?','Save the cartographic parameters in a file ?'));
        if ~flag
            return
        end
        if rep == 1
            [flag, nomFicCarto] = my_uiputfile('*.def', Lang('Nom du fichier de d�finition de la carto', 'Give a file name'), ...
                fullfile(nomDir, 'Carto.def'));
            if ~flag
                return
            end
            flag = export(Carto, nomFicCarto);
            if ~flag
                return
            end
        end
    end
end


if ~isequal(w, [xmin xmax ymin ymax])
    xmin = w(1);
    xmax = w(2);
    ymin = w(3);
    ymax = w(4);
    sub = find((X >= xmin) & (X <= xmax) & (Y >= ymin) & (Y <= ymax));
    Y = Y(sub);
    X = X(sub);
    V = V(sub);
    N = length(Y);
    
    xmin = min(X);
    xmax = max(X);
    ymin = min(Y);
    ymax = max(Y);
end

rangex = xmax - xmin;
rangey = ymax - ymin;
surfaceUnitaire = (rangex * rangey) / N;

if GeometryType == cl_image.indGeometryType('PingBeam')
    pasy = 1;
else
    pasy = sqrt(surfaceUnitaire) * 2;
end

%% Type de l'algorithme

if isempty(TypeAlgo)
    str1 = 'Algorithme de maillage.';
    str2 = 'Gridding algorithm';
    str3 = 'Plus proche voisin';
    str4 = 'Nearest';
    str5 = 'Biin�aire';
    str6 = 'Biinear';
    [TypeAlgo, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
    
    if ~flag
        return
    end
end

%% Cr�ation des axes x et y

if isempty(pasxy)
    flag = 0;
    while ~flag
        if GeometryType == cl_image.indGeometryType('LatLong')
            pasx = pasy / cos(mean(y) * (pi/180));
        else
            pasx = pasy;
        end
        
        x = xmin:pasx:xmax;
        y = ymin:pasy:ymax;
        [x, pasx, xmin, xmax] = centrage_magnetique(x);
        [y, pasy, ymin, ymax] = centrage_magnetique(y);
        
        cols = length(x);
        rows = length(y);
        
        if GeometryType == cl_image.indGeometryType('LatLong')
            str = sprintf('Proposition : \n\nPixel size (m): %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasy * (6378137 * (pi/180)), rows, cols);
        else
            str = sprintf('Proposition : \n\nPixel size : %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasx, rows, cols);
        end
        [rep, flag] = my_questdlg(Lang('TODO', str));
        if ~flag
            return
        end
        if rep == 1
            flag = 1;
        else
            if GeometryType == cl_image.indGeometryType('LatLong')
                pasyEdition = pasy * (6378137 * pi/180);
            else
                pasyEdition = pasy;
            end
            
            str1 = 'La donn�e va �tre maill�e, d�finissez la taille de la maille.';
            str2 = 'The import will grid the data. Please select the grid size.';
            [flag, pasy] = inputOneParametre(Lang(str1,str2), 'Grid size', ...
                'Value', pasyEdition, 'Unit', unitexy);
            if ~flag
                return
            end
            
            if GeometryType == cl_image.indGeometryType('LatLong')
                pasy = pasy / (6378137 * pi/180);
                pasx = pasy / cos(mean(y) * (pi/180));
            else
                pasx = pasy;
            end
            flag = 0;
        end
    end
else
    pasx = pasxy(1);
    pasy = pasxy(2);
    x = xmin:pasx:xmax;
    y = ymin:pasy:ymax;
    [x, pasx, xmin] = centrage_magnetique(x);
    [y, pasy, ymin] = centrage_magnetique(y);
    cols = length(x);
    rows = length(y);
end
pasxy(1) = pasx;
pasxy(2) = pasy;


%% Cr�ation de l'image

% [xi,yi] = meshgrid(x, y);
% z2 = griddata(Y, X, V, xi, yi);

z1 = zeros(rows, cols, 'single');
z2 = zeros(rows, cols, 'single');
WorkInProgress(Lang('Maillage en cours', 'Griding data'));
% Pour info : j'ai d� supprimer le my(waitbar car il prenait 99% du temps

switch TypeAlgo
    case 1 % Plus proche voisin
        %         tic
        for i=1:N
            ix = 1 + floor((X(i) - xmin) / pasx);
            iy = 1 + floor((Y(i) - ymin) / pasy);
            
            if (iy >= 1) && (iy <= rows) && (ix >= 1) && (ix <= cols)
                z1(iy,ix) = z1(iy,ix) + 1;
                z2(iy,ix) = z2(iy,ix) + V(i);
            end
        end
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        %         toc
        
    case 2 % Lin�aire
        %         tic
        for i=1:N
            xp = (X(i) - xmin) / pasx;
            yp = (Y(i) - ymin) / pasy;
            ix1 = 1 + floor(xp);
            iy1 = 1 + floor(yp);
            xp = xp +1;
            yp = yp + 1;
            ix2 = ix1 + 1;
            iy2 = iy1 + 1;
            
            if ((iy1 >= 1) && (iy1 <= rows) && (ix1 >= 1) && (ix1 <= cols) && ...
                    (iy2 >= 1) && (iy2 <= rows) && (ix2 >= 1) && (ix2 <= cols))
                xalpha = ix2 - xp;
                yalpha = iy2 - yp;
                
                A1 =     xalpha*yalpha;
                A2 = (1-xalpha)*yalpha;
                A3 = (1-xalpha)*(1-yalpha);
                A4 =     xalpha*(1-yalpha);
                Vali = V(i);
                
                z1(iy1,ix1) = z1(iy1,ix1) + A1;
                z2(iy1,ix1) = z2(iy1,ix1) + A1 * Vali;
                
                z1(iy1,ix2) = z1(iy1,ix2) + A2;
                z2(iy1,ix2) = z2(iy1,ix2) + A2 * Vali;
                
                z1(iy2,ix2) = z1(iy2,ix2) + A3;
                z2(iy2,ix2) = z2(iy2,ix2) + A3 * Vali;
                
                z1(iy2,ix1) = z1(iy2,ix1) + A4;
                z2(iy2,ix1) = z2(iy2,ix1) + A4 * Vali;
            end
        end
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        %         toc
        
    case 3 % Fen�tre de pond�ration gaussi�ne
        % TODO : � poursuivre
        sigma = 0.75;
        n = floor(2 * sigma);
        window = [max(3,2*n+1) max(3,2*n+1)];
        w = window(1);
        %         Y = normpdf(X,mu,sigma)
        h = 100 * fspecial('gaussian', [1 w*100*2], sigma*100);
        h = h(floor(length(h)/2):end);
        %     figure(20081008); plot(h); grid on; title('Gaussian coefficient filter');
        subh = -n:n;
        IX = repmat(subh,  w, 1);
        IY = repmat(subh', 1, w);
        %         tic
        for i=1:N
            Vali = V(i);
            xp = (X(i) - xmin) / pasx;
            yp = (Y(i) - ymin) / pasy;
            ix = 1 + floor(xp);
            iy = 1 + floor(yp);
            
            ix = IX + ix;
            iy = IY + iy;
            
            sub = find((iy >= 1) & (iy <= rows) & (ix >= 1) & (ix <= cols));
            
            for k=1:length(sub)
                kk = sub(k);
                xh = xp+1-ix(kk);
                yh = yp+1-iy(kk);
                %             g = normpdf(sqrt(xh*xh+yh*yh),0,sigma);
                g = h(1+floor(100*sqrt(xh*xh+yh*yh)));
                kx = ix(kk);
                ky = iy(kk);
                z1(ky,kx) = z1(ky,kx) + g;
                z2(ky,kx) = z2(ky,kx) + g * Vali;
            end
        end
        z1(z1 < 0.5) = NaN;
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        %         toc
end

if GeometryType == cl_image.indGeometryType('LatLong')
    TagSynchroX = 'Longitude';
    TagSynchroY = 'Latitude';
else
    TagSynchroX = num2str(rand(1));
    TagSynchroY = num2str(rand(1));
end
[~, Name] = fileparts(nomFic);

if ~isempty(ScaleFactor)
    z2 = z2 * ScaleFactor;
end
this = cl_image('Image',    z2, ...
    'Name',                Name, ...
    'Unit',                 Unit, ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',                unitexy, ...
    'YUnit',                unitexy, ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         GeometryType, ...
    'TagSynchroX',          TagSynchroX, ...
    'TagSynchroY',          TagSynchroY, ...
    'InitialFileName',      nomFic, 'InitialFileFormat', 'ASCII xyz', ...
    'Carto', Carto);
this = update_Name(this);

if ~isempty(Carto)
    this = set(this, 'Carto', Carto);
end

DataType2 = cl_image.indDataType('AveragedPtsNb');
this(2) = cl_image('Image', z1, ...
    'Name',                Name, ...
    'Unit',                 '-', ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',                unitexy, ...
    'YUnit',                unitexy, ...
    'ColormapIndex',        3, ...
    'DataType',             DataType2, ...
    'GeometryType',         GeometryType, ...
    'TagSynchroX',          TagSynchroX, ...
    'TagSynchroY',          TagSynchroY, ...
    'InitialFileName',      nomFic, 'InitialFileFormat', 'ASCII xyz', ...
    'Carto', Carto);
this(2) = update_Name(this(2));

flag = 1;


function [X, Y, V] = lectureIndirecte(nomFic)

fid = fopen(nomFic, 'r');
Entete = fgetl(fid); %#ok

X = {};
Y = {};
V = {};
OK = 1;
Format = '%s %s %f';
indX = 2;
indY = 1;
indV = 3;
while OK
    C = textscan(fid, Format, 200000);
    
    if isempty(C{1})
        OK = 0;
    else
        N = length(C{1});
        X = NaN(N,1);
        Y = NaN(N,1);
        V = NaN(N,1);
        str1 = 'IFREMER - SonarScope : decodage du fichier par paquets de 200000 lignes.';
        str2 = 'IFREMER - SonarScope : decode data by blocks of 200000 lines.';
        hw = create_waitbar(Lang(str1,str2), 'N', N);
        for k=1:N
            my_waitbar(k, N, hw);
            X(k) = strCarisLat2double(C{indX}{k});
            Y(k) = strCarisLon2double(C{indY}{k});
            V(k) = C{indV}(k);
        end
        my_close(hw)
    end
end

function y = strCarisLat2double(s)
D = str2double(s(1:3));
M = str2double(s(5:12));
y = D + M/60;
if strcmpi(s(13), 'w')
    y = -y;
end

function y = strCarisLon2double(s)
D = str2double(s(1:2));
M = str2double(s(4:11));
y = D + M/60;
if strcmpi(s(12), 's')
    y = -y;
end

