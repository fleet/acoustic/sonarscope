function [flag, this] = import_CourbesStats(this, nomFic, varargin)

[varargin, flagPlot] = getPropertyValue(varargin, 'flagPlot', 1);
[varargin, Tag]      = getPropertyValue(varargin, 'Tag',      []); %#ok<ASGLU>

[flag, this] = importCourbesStats(this, nomFic, 'Tag', Tag);
if ~flag 
    return
end

if flagPlot
    %% Trac� des courbes seules
    fig = [];
    nbFic = length(nomFic);
    nbCourbes = get_nbCourbesStatistiques(this);
    for iFic=1:nbFic
        fig = plotCourbesStats(this, 'sub', nbCourbes-iFic+1, 'Stats', 0, 'fig', fig, 'subTypeCurve', 1);
    end
    
    %% Trac� des r�sidus entre courbes et mod�le
    fig = [];
    nbFic = length(nomFic);
    nbCourbes = get_nbCourbesStatistiques(this);
    for iFic=1:nbFic
        fig = plotCourbesStats(this, 'sub', nbCourbes-iFic+1, 'Stats', 0, 'fig', fig, 'subTypeCurve', 2);
    end
    
    %% Trac� des moyennes +- �carts-types
    fig = [];
    nbFic = length(nomFic);
    nbCourbes = get_nbCourbesStatistiques(this);
    for iFic=1:nbFic
        fig = plotCourbesStats(this, 'sub', nbCourbes-iFic+1, 'Stats', 1, 'fig', fig, 'subTypeCurve', 1);
    end
end
