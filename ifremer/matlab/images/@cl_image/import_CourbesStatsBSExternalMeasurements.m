function [flag, this, RepDefaut] = import_CourbesStatsBSExternalMeasurements(this, varargin)

[varargin, RepDefaut] = getPropertyValue(varargin, 'RepDefaut', []); %#ok<ASGLU>

%{
M = csvExample;
for k=size(M,1):-1:1
    str{k} = sprintf('%f, %f', M(k,1), M(k,2));
end
str1 = 'Exemple de fichier d�finissant une coure de BS angulaire';
str2 = 'Example of an angular BS curve file';
edit_infoPixel(str, [], 'PromptString', Lang(str1,str2))
%}

%% Select the ASCII file : TODO : sortir cela de la fonction et l'appeler dans une fonction param_.....

[flag, nomFic] = my_uigetfile({'*.csv*;*.txt','*.csv*,*.txt';
    '*.csv*',  '*.csv*'; ...
    '*.txt',  '*.txt'; ...
    '*.*',  'All Files (*.*)'}, 'Give a file (cancel instead)', RepDefaut);
if ~flag
    return
end
RepDefaut = fileparts(nomFic);

%% Lecture du fichier ASCII

M = csvread(nomFic);
if size(M,2) == 1
	M = dlmread(nomFic);
end
XData = M(:,1);
YData = M(:,2);
[~,sub] = sort(XData);
XData = XData(sub);
YData = YData(sub);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(XData, YData); grid on
XData = XData';
YData = YData';

%% Cr�ation du mod�le

model = BSLurtonModel('XData', XData, 'YData', YData);

%% Lancement de l'outil d'optimisation

optim = ClOptim('XData', XData, 'YData', YData, ...
    'xLabel', 'Incidence angles', ...
    'yLabel', 'Gain', ...
    'Name', 'Gain');
optim.set('models', model);  % optim = set(optim, 'models', model);

a = OptimDialog(optim, 'Title', 'Optim ExClOptimBS', 'windowStyle', 'normal');
a.openDialog();

%{
flag = a.okPressedOut;
if ~flag
return
end
%}

%% R�cup�ration du mod�le fitt�

model = a.optim.models;
plot(model); %option
p = model.getParamsValue;

NY = length(model.XData);

bilan.x       = XData;
bilan.y       = YData;
% bilan.x       = model.XData;
% bilan.y       = model.compute;
bilan.ymedian = bilan.y;
bilan.nom     = 'BSLurton model';
bilan.sub     = 1:NY;
bilan.nx      = ones(NY,1);
bilan.std     = zeros(NY,1);
bilan.titreV  = 'BSLurton model defined manually';
bilan.NY      = [NY 1];
bilan.nomX    = 'IncidenceAngle';
bilan.Biais   = 0;
bilan.Unit    = 'dB';
bilan.nomZoneEtude       = 'BS : Manually defined';
bilan.commentaire        = '';
bilan.Tag                = 'BS';
bilan.DataTypeValue      = 'Reflectivity';
bilan.DataTypeConditions = {'IncidenceAngle'};
bilan.Support            = {'Layer'};
bilan.GeometryType       = 'PingBeam';
bilan.TypeCourbeStat     = 'Statistics';
bilan.TagSup             = [];
bilan.residuals          = zeros(1,NY);
bilan.model              = model;
bilan.BiaisModel         = p(3);

this.CourbesStatistiques(end+1).bilan{1} = bilan;

flag = 1;


%{
function M = csvExample

M = [-66,-21.399
    -65,-21.053
    -64,-20.707
    -63,-20.14
    -62,-20.546
    -61,-20.499
    -60,-20.447
    -59,-20.033
    -58,-20.021
    -57,-19.591
    -56,-19.289
    -55,-19.275
    -54,-18.946
    -53,-18.852
    -52,-18.671
    -51,-18.323
    -50,-18.216
    -49,-18.431
    -48,-18.255
    -47,-18.111
    -46,-18.151
    -45,-18.036
    -44,-17.711
    -43,-18.218
    -42,-17.989
    -41,-17.984
    -40,-18.14
    -39,-17.942
    -38,-17.764
    -37,-17.365
    -36,-17.058
    -35,-16.916
    -34,-16.507
    -33,-16.52
    -32,-16.443
    -31,-16.125
    -30,-15.751
    -29,-15.662
    -28,-15.858
    -27,-15.573
    -26,-15.26
    -25,-15.248
    -24,-15.367
    -23,-14.971
    -22,-14.706
    -21,-14.814
    -20,-14.469
    -19,-14.611
    -18,-14.708
    -17,-14.685
    -16,-14.621
    -15,-13.997
    -14,-13.7
    -13,-13.371
    -12,-13.406
    -11,-13.333
    -10,-13.185
    -9,-13.014
    -8,-12.595
    -7,-12.196
    -6,-11.798
    -5,-11.707
    -4,-11.477
    -3,-11.397
    -2,-10.925
    -1,-11.432
    0,-10.778
    1,-11.345
    2,-11.395
    3,-11.425
    4,-11.73
    5,-12.449
    6,-12.873
    7,-12.779
    8,-12.865
    9,-13.601
    10,-13.627
    11,-13.766
    12,-14.038
    13,-14.63
    14,-14.92
    15,-15.671
    16,-15.466
    17,-16.142
    18,-16.246
    19,-16.19
    20,-15.932
    21,-15.951
    22,-16.042
    23,-15.849
    24,-15.884
    25,-15.814
    26,-16.09
    27,-16.455
    28,-16.316
    29,-16.454
    30,-16.5
    31,-16.506
    32,-16.741
    33,-16.969
    34,-17.113
    35,-17.019
    36,-17.463
    37,-17.75
    38,-18.018
    39,-18.367
    40,-18.43
    41,-18.806
    42,-19.065
    43,-19.411
    44,-19.769
    45,-19.903
    46,-20.018
    47,-19.983
    48,-20.274
    49,-20.668
    50,-20.609
    51,-21.134
    52,-21.26
    53,-21.492
    54,-21.18
    55,-21.212
    56,-21.288
    57,-21.525
    58,-21.932
    59,-22.14
    60,-22.34
    61,-22.602
    62,-22.189
    63,-23.428];
%}
