% Importation d'un fichier ESRI Grid ASCII
%
% Syntax
%   [flag, a] = import_EsriGridAscii(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%   [X,Y,Z] = peaks(30);
%   a = cl_image('Image', Z)
%   imagesc(a)
%
%   nomFicOut = my_tempname
%   flag = export_EsriGridAscii(a, nomFicOut) % Pas encore fait
%
%   [flag, b] = import_EsriGridAscii(cl_image, nomFicOut);
%   imagesc(b)
%
%   nomFic = getNomFicDatabase('*.asc');
%   [flag, b] = import_EsriGridAscii(cl_image, nomFic);
%   imagesc(b)
%
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this, Carto] = import_EsriGridAscii(~, nomFic, varargin)

flag = is_EsriGridAscii(nomFic);
if ~flag
    this = [];
    return
end

%% Test de lecture au format ascii SRTM (produit aussi par ARC-GIS

I0 = cl_image_I0;
[flag, this, Carto] = import_xyz_FormatSRTM(I0, nomFic, varargin{:});
if flag
    return
end

