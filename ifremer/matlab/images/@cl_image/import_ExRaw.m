% Importation d'un fichier RAW (.raw) issu des sondeurs de la gamme Simrad.
% (Ex pour Ek, Er, Es et Ea).
%
% Syntax
%   [flag, a] = import_ExRaw(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('BOURGNEUF-D20120412-T071623_raw.raw') %  GeoLittomer
%   [flag, a] = import_ExRaw(cl_image, nomFic);
%   [flag, a] = import_ExRaw(cl_image, nomFic, 'subImage', 3);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this, Carto, listDataType, channelName, channelFreq] = import_ExRaw(this, nomFic, varargin)

[varargin, listFrequencies] = getPropertyValue(varargin, 'listFrequencies', []);
[varargin, listDataType]    = getPropertyValue(varargin, 'listDataType',    1:5);
[varargin, Carto]           = getPropertyValue(varargin, 'Carto',           []); %#ok<ASGLU>

channelName = {};
channelFreq = [];

cl_ExRaw('nomFic', nomFic);

[flag, DataSample] = SSc_ReadDatagrams(nomFic, 'Ssc_Sample', 'Memmapfile', -1);
if ~flag
    % TODO : message d'erreur SVP
    return
end

[flag, DataPosition] = SSc_ReadDatagrams(nomFic, 'Ssc_Position');
if ~flag
    % TODO : message d'erreur SVP
    return
end

[flag, DataHeight] = SSc_ReadDatagrams(nomFic, 'Ssc_Height');
if flag
    Height = DataHeight.Height;
else
    flag = 1;
    Height = [];
end

%% Lecture des donn�es

NumSounder = 28;
SonarDescription = cl_sounder('Sonar.Ident', NumSounder);
nomSondeur = get(SonarDescription, 'SonarName');

nbChannels = DataSample.Dimensions.nbChannels;
nbSamples  = DataSample.Dimensions.nbSamples;
nbPings    = DataSample.Dimensions.nbPings;

if isempty(listFrequencies)
    listFrequencies = 1:nbChannels;
end

this = cl_image.empty;

nomLayers = {'Amplitude'; 'Sp'; 'Sv'; 'AlongShip'; 'AthwartShip'}; % Attention, on ne peut pas rempacer Amplitude par Poxer ici
for iFreq=1:length(listFrequencies)
    iChannel = listFrequencies(iFreq);
    
    resol             =  1500 / (2 * median( DataSample.SampleFrequency(:), 'omitnan'));
    AcousticFrequency = DataSample.AcousticFrequency(iChannel);
    FreqkHz           = floor(AcousticFrequency/1000);
    Comments = sprintf('SounderName=%s  NumSounder=%d  NumChannel=%d  Frequency=%s kHz', ...
        nomSondeur, NumSounder, iChannel, num2str(FreqkHz));
    
%     fe_Hz = DataSample.SampleFrequency(iChannel);
    x = 1:double(nbSamples);
    y = 1:double(nbPings);
    [~, ImageName] = fileparts(nomFic);
    
    subChannel = [];
    
    if any(listDataType == 1)
        nomVar = sprintf('%s_%dkHz', nomLayers{1}, FreqkHz);
        if isfield(DataSample, nomVar)
            % Si l'image est Nulle.
            if ~all(isnan(DataSample.(nomVar)(:)))
                this(end+1) = cl_image('Image', DataSample.(nomVar), ...
                    'Name',           [ImageName '_Power'], ...
                    'Unit',                'dB', ...
                    'x',                    x, ...
                    'y',                    y, ...
                    'XUnit',               'sample', ...
                    'YUnit',               'Ping', ...
                    'ColormapIndex',        3, ...
                    'DataType',             cl_image.indDataType('Reflectivity'), ...
                    'TagSynchroX',          [ImageName ' - Sample'], ...
                    'TagSynchroY',          [ImageName ' - Ping'], ...
                    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
                    'SonarDescription',      SonarDescription, ...
                    'SonarRawDataResol',    resol, ...
                    'SonarResolutionD',     resol, ...
                    'SonarResolutionX',     resol, ...
                    'InitialFileName',      nomFic, ...
                    'InitialFileFormat',    'Simrad_ExRaw', ...
                    'Comments',             [Comments ' ' nomLayers{1}]); %#ok<AGROW>
                subChannel(end+1) = length(this); %#ok<AGROW>
                channelName{end+1} = 'Power'; %#ok<AGROW>
                channelFreq(end+1) = FreqkHz; %#ok<AGROW>
            else
                listDataType(listDataType == 1) = [];
                strUS = sprintf('Null Image %s not displayed', nomVar);
                strFR = sprintf('Donn�e nulle non affich�e : %s', nomVar);
                my_warndlg(Lang(strFR, strUS), 0, 'Tag', 'ExRaw_DataSample_Null');
            end
        else
            listDataType(listDataType == 1) = [];
        end
    end
    
    if any(listDataType == 2)
        nomVar = sprintf('%s_%dkHz', nomLayers{2}, FreqkHz);
        if isfield(DataSample, nomVar)
            % Si l'image est Nulle.
            if ~all(isnan(DataSample.(nomVar)(:)))
                this(end+1) = cl_image('Image', DataSample.(nomVar), ...
                    'Name',           [ImageName '_Sp'], ...
                    'Unit',                'dB', ...
                    'x',                    x, ...
                    'y',                    y, ...
                    'XUnit',               'sample', ...
                    'YUnit',               'Ping', ...
                    'ColormapIndex',        3, ...
                    'DataType',             cl_image.indDataType('Reflectivity'), ...
                    'TagSynchroX',          [ImageName ' - Sample'], ...
                    'TagSynchroY',          [ImageName ' - Ping'], ...
                    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
                    'SonarDescription',      SonarDescription, ...
                    'SonarRawDataResol',    resol, ...
                    'SonarResolutionD',     resol, ...
                    'SonarResolutionX',     resol, ...
                    'InitialFileName',      nomFic, ...
                    'InitialFileFormat',    'Simrad_ExRaw', ...
                    'Comments',             [Comments ' ' nomLayers{2}]); %#ok<AGROW>
                subChannel(end+1) = length(this); %#ok<AGROW>
                channelName{end+1} = nomLayers{2}; %#ok<AGROW>
                channelFreq(end+1) = FreqkHz; %#ok<AGROW>
            else
                listDataType(listDataType == 2) = [];
                strUS = sprintf('Null Image %s not displayed', nomVar);
                strFR = sprintf('Donn�e nulle non affich�e : %s', nomVar);
                my_warndlg(Lang(strFR, strUS), 0, 'Tag', 'ExRaw_DataSample_Null');
            end
        else
            listDataType(listDataType == 2) = [];
        end
    end
    
    if any(listDataType == 3)
        nomVar = sprintf('%s_%dkHz', nomLayers{3}, FreqkHz);
        if isfield(DataSample, nomVar)
            % Si l'image est Nulle.
            if ~all(isnan(DataSample.(nomVar)(:)))
                this(end+1) = cl_image('Image', DataSample.(nomVar), ...
                    'Name',           [ImageName '_Sv'], ...
                    'Unit',                'dB', ...
                    'x',                    x, ...
                    'y',                    y, ...
                    'XUnit',               'sample', ...
                    'YUnit',               'Ping', ...
                    'ColormapIndex',        3, ...
                    'DataType',             cl_image.indDataType('Reflectivity'), ...
                    'TagSynchroX',          [ImageName ' - Sample'], ...
                    'TagSynchroY',          [ImageName ' - Ping'], ...
                    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
                    'SonarDescription',      SonarDescription, ...
                    'SonarRawDataResol',    resol, ...
                    'SonarResolutionD',     resol, ...
                    'SonarResolutionX',     resol, ...
                    'InitialFileName',      nomFic, ...
                    'InitialFileFormat',    'Simrad_ExRaw', ...
                    'Comments',             [Comments ' ' nomLayers{2}]); %#ok<AGROW>
                subChannel(end+1) = length(this); %#ok<AGROW>
                channelName{end+1} = nomLayers{3}; %#ok<AGROW>
                channelFreq(end+1) = FreqkHz; %#ok<AGROW>
            else
                listDataType(listDataType == 3) = [];
                strUS = sprintf('Null Image %s not displayed', nomVar);
                strFR = sprintf('Donn�e nulle non affich�e : %s', nomVar);
                my_warndlg(Lang(strFR, strUS), 0, 'Tag', 'ExRaw_DataSample_Null');
            end
        else
            listDataType(listDataType == 3) = [];
        end
    end
    
    if any(listDataType == 4)
        nomVar = sprintf('%s_%dkHz', nomLayers{4}, FreqkHz);
        if isfield(DataSample, nomVar)
            % Si l'image est Nulle.
            if ~all(isnan(DataSample.(nomVar)(:)))
                this(end+1) = cl_image('Image', DataSample.(nomVar), ...
                    'Name',           [ImageName '_Along'], ...
                    'Unit',                'deg', ...
                    'x',                    x, ...
                    'y',                    y, ...
                    'XUnit',               'sample', ...
                    'YUnit',               'Ping', ...
                    'ColormapIndex',        3, ...
                    'DataType',             cl_image.indDataType('SonarAlongBeamAngle'), ...
                    'TagSynchroX',          [ImageName ' - Sample'], ...
                    'TagSynchroY',          [ImageName ' - Ping'], ...
                    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
                    'SonarDescription',     SonarDescription, ...
                    'SonarRawDataResol',    resol, ...
                    'SonarResolutionD',     resol, ...
                    'SonarResolutionX',     resol, ...
                    'InitialFileName',      nomFic, ...
                    'InitialFileFormat',    'Simrad_ExRaw', ...
                    'Comments',             [Comments ' ' nomLayers{3}]); %#ok<AGROW>
                subChannel(end+1) = length(this); %#ok<AGROW>
                channelName{end+1} = nomLayers{4}; %#ok<AGROW>
                channelFreq(end+1) = FreqkHz; %#ok<AGROW>
            else
                listDataType(listDataType == 4) = [];
                strUS = sprintf('Null Image %s not displayed', nomVar);
                strFR = sprintf('Donn�e nulle non affich�e : %s', nomVar);
                my_warndlg(Lang(strFR, strUS), 0, 'Tag', 'ExRaw_DataSample_Null');
            end
        else
            listDataType(listDataType == 4) = [];
        end
    end
    
    if any(listDataType == 5)
        nomVar = sprintf('%s_%dkHz', nomLayers{4}, FreqkHz);
        if isfield(DataSample, nomVar)
            % Si l'image est Nulle.
            if ~all(isnan(DataSample.(nomVar)(:)))
                this(end+1) = cl_image('Image', DataSample.(nomVar), ...
                    'Name',         [ImageName '_Athwart'], ...
                    'Unit',              'deg', ...
                    'x',                 x, ...
                    'y',                 y, ...
                    'XUnit',             'sample', ...
                    'YUnit',             'Ping', ...
                    'ColormapIndex',     3, ...
                    'DataType',          cl_image.indDataType('SonarAcrossBeamAngle'), ...
                    'TagSynchroX',       [ImageName ' - Sample'], ...
                    'TagSynchroY',       [ImageName ' - Ping'], ...
                    'GeometryType',      cl_image.indGeometryType('PingSamples'), ...
                    'SonarDescription',  SonarDescription, ...
                    'SonarRawDataResol', resol, ...
                    'SonarResolutionD',  resol, ...
                    'SonarResolutionX',  resol, ...
                    'InitialFileName',   nomFic, ...
                    'InitialFileFormat', 'Simrad_ExRaw', ...
                    'Comments',          [Comments ' ' nomLayers{4}]); %#ok<AGROW>
                subChannel(end+1) = length(this); %#ok<AGROW>
                channelName{end+1} = nomLayers{5}; %#ok<AGROW>
                channelFreq(end+1) = FreqkHz; %#ok<AGROW>
            else
                listDataType(listDataType == 5) = [];
                strUS = sprintf('Null Image %s not displayed', nomVar);
                strFR = sprintf('Donn�e nulle non affich�e : %s', nomVar);
                my_warndlg(Lang(strFR, strUS), 0, 'Tag', 'ExRaw_DataSample_Null');
            end
        else
            listDataType(listDataType == 5) = [];
        end
    end
    
    %{
Mode            = DataSample.Mode(:);
transmitpower: 600
pulselength: 0.001023999997415
bandwidth: 2.859129150390625e+003
absorptioncoefficient: 0.022033587098122
temperature: 7.500000000000000
    %}
    for k=1:length(subChannel)
        k2 = subChannel(k);
        this(k2) = set(this(k2), 'SonarPingCounter',   y(:));
        this(k2) = set(this(k2), 'SonarImmersion',     DataPosition.Immersion(:));
        this(k2) = set(this(k2), 'SonarHeading',       DataPosition.Heading(:));
        this(k2) = set(this(k2), 'SonarSpeed',         DataPosition.Speed(:));
        this(k2) = set(this(k2), 'SonarFishLatitude',  DataPosition.Latitude(:));
        this(k2) = set(this(k2), 'SonarFishLongitude', DataPosition.Longitude(:));
        this(k2) = set(this(k2), 'SonarTime',          DataPosition.Time);
        this(k2) = set(this(k2), 'SonarRoll',          DataPosition.Roll(:));
        this(k2) = set(this(k2), 'SonarPitch',         DataPosition.Pitch(:));
        this(k2) = set(this(k2), 'SonarHeave',         DataPosition.Heave(:));
        
        n = length(DataSample.SampleFrequency);
        if n == nbPings
            this(k2) = set(this(k2), 'SampleFrequency',        DataSample.SampleFrequency(:));
            this(k2) = set(this(k2), 'SonarSurfaceSoundSpeed', DataSample.SoundVelocity(:));
        elseif n == 1
            this(k2) = set(this(k2), 'SampleFrequency',        zeros(nbPings, 1, 'single') + DataSample.SampleFrequency(1));
            this(k2) = set(this(k2), 'SonarSurfaceSoundSpeed', zeros(nbPings, 1, 'single') + DataSample.SoundVelocity(1));
        else
            %             this(k2) = set(this(k2), 'SampleFrequency',        NaN(nbPings, 1, 'single'));
            %             this(k2) = set(this(k2), 'SonarSurfaceSoundSpeed', NaN(nbPings, 1, 'single'));
            
            % Modif JMA le 23/05/2014
            this(k2) = set(this(k2), 'SampleFrequency',        zeros(nbPings, 1, 'single') + DataSample.SampleFrequency(iChannel));
            this(k2) = set(this(k2), 'SonarSurfaceSoundSpeed', zeros(nbPings, 1, 'single') + DataSample.SoundVelocity(1));
        end
        
        if isempty(Height)
            if isfield(DataPosition, 'Height') && ~isempty(DataPosition.Height)
                this(k2) = set(this(k2), 'SonarHeight', DataPosition.Height(:)); % fe_Hz
            end
        else
            this(k2) = set(this(k2), 'SonarHeight', Height(:));
        end
        
        % Ajout� par JMA le 27/01/2013
        if isfield(DataPosition, 'Tide') && ~isempty(DataPosition.Tide)
            this(k2) = set(this(k2), 'SonarTide', DataPosition.Tide(:));
        end
        
        if isempty(Carto)
            Carto = getDefinitionCarto(this(k2));
        end
        this(k2) = set(this(k2),  'Carto', Carto);
        
        this(k2) = update_Name(this(k2), 'Append', [num2str(AcousticFrequency/1000) ' kHz']);
    end
end
this = this(:);
% SonarScope(this)

this = crop(this); % Pour donn�es EK80 qui font 120000 colonnes et dont 95% des valeurs sont NaN !!!

N = numel(this);
sub = true(1,N);
for k=1:N
    if (this(k).nbRows < 2) || (this(k).nbColumns < 2)
        sub(k) = false;
    end
end
this = this(sub);
