% Importation d'un fichier HSX (Reson ASCII) (*.HSX)
%
% Syntax
%   [flag, a] = import_HSX(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFicIn = getNomFicDatabase('RESON_002_1138.HSX')
%   [flag, a] = import_HSX(cl_image, nomFicIn);
%   SonarScope(a)
%
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this] = import_HSX(this, nomFic)

[nb_RMB, nb_RSS, nbSamplesPort, nbSamplesStarboard, nbBeams, flag] = preprocess_HSX(nomFic);
if flag == 0
    return
end

flag = 0;

fid = fopen(nomFic);
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.HSX']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        return
    end
end

s.DEV = [];
s.DV2 = [];
s.OF2 = [];
s.PRI = [];
s.MBI = [];
s.SSI = [];
s.LIN = [];
s.PTS = [];
s.HVF = [];
s.LBP = [];
s.LNN = [];
s.SVC = [];
s.FIX = [];
s.GPS = [];
s.POS = [];
s.GYR = [];
s.RMB = [];
s.RSS = [];
s.HCP = [];

SonarImage      = zeros(nb_RSS, nbSamplesPort+nbSamplesStarboard);
BeamRanges      = zeros(nb_RMB, nbBeams);
BeamIntensity   = BeamRanges;
BeamQualityCode = BeamRanges;
iPing_RSS = 0;
iPing_RMB = 0;

while feof(fid) == 0
    line = fgetl(fid);
    TAG = line(1:3);
    switch TAG
        case 'FTP' % File type (Hypack File Identifier)
            s.FTP = line(5:end);
            
        case 'HSX' % HSX File Identifier
            s.HSX.vn = line(5:end);    % HSX format version number
            
        case 'TND' % Survey Time and Date
            d = line(14:23);
            date = dayStr2Ifr(d([4 5 3 1 2 6 7 8 9 10]));
%             h = line(5:12);
%             heure = hourStr2Ifr(h);
            
        case 'INF' % General Information
            sub = strfind(line, '"');
            mots = strsplit(line(5:sub(end)), '"');
            s.INT.Surveyor = mots{1};
            s.INT.Boat     = mots{2};
            s.INT.Project  = mots{3};
            s.INT.Area     = mots{4};
            X = sscanf(line(sub(end)+1:end), '%f');
            s.INT.tc = X(1);	% Initial Tide correction
            s.INT.dc = X(2);    % Initial draft correction (boat)
            s.INT.sv = X(3);    % Sound velocity

        case 'HSP' % Hysweep Survey Parameters
            X = sscanf(line(5:end), '%f');
            s.HSP.P1 = X(1);    % Minimum depth
            s.HSP.P2 = X(2);    % Maximum depth
            s.HSP.P3 = X(3);    % Port side offset limit
            s.HSP.P4 = X(4);    % Starboard side offset limit
            s.HSP.P5 = X(5);    % Port side beam angle limit
            s.HSP.P6 = X(6);    % Starboard side beam angle limit
            s.HSP.P7 = X(7);    % High beam quality; codes >= this are good
            s.HSP.P8 = X(8);    % Low beam quality; codes < this are bad
            s.HSP.P9 = X(9);    % Sonar Range setting
            s.HSP.P10 = X(10);  % Towfish layback
            s.HSP.P11 = X(11);  % Work units: 0=meters, 1=us foot, 2=int'l foot
            
        case 'DEV'  % Hypack Device Information
            sub = strfind(line, '"');
            s.DEV(end+1).name = line(sub(1)+1:sub(2)-1);
            X = sscanf(line(5:sub(1)-1), '%f');
            s.DEV(end).dn = X(1);
            s.DEV(end).dc = X(2);    % 1,2,3,4=Position, 16=Depth, 32=Heading, 512=MRU, 32768=Extended capabilities
            
        case 'DV2' % Hysweep Device Information
            X = sscanf(line(5:end), '%f');
            s.DV2(end+1).dn = X(1);  % Device number
            s.DV2(end).dc = X(2);    % Hysweep device capabilities (Hexa)
                                     %   0001=Multibeam Sonar
                                     %   0002=Multiple Transducer Sonar
                                     %   0004=GPS (Boat position)
                                     %   0008=Sidescan sonar
                                     %   0010=Single Beam Echosounder
                                     %   0020=Gyro (Boat heading)
                                     %   0200=MRU (Heave, pitch,and roll
                                     %   compensation)
            s.DV2(end).tf = X(3);    % 1 if device is mounted on a tow fish
            s.DV2(end).en = X(4);    % 1 if device is enabled
            
        case 'OF2'  % Hysweep device Offsets
            X = sscanf(line(5:end), '%f');
            s.OF2(end+1).dn = X(1);  % Device number
            s.OF2(end).on = X(2);    % Offset number
                                     %   0=Position antenna offsets
                                     %   1=gyro heading offset
                                     %   2=MRU device offset
                                     %   3=Sonar head 1 / Transducer 1 offsets
                                     %   4=Sonar head 2 / Transducer 2 offsets
                                     %   5=Transducer 3 offsets
                                     %   ...
                                     %   131=5=Transducer 128 offsets
            s.OF2(end).n1 = X(3);    % Starbord/Port mounting offset. Positive starboard
            s.OF2(end).n2 = X(4);    % Forward / Aft mounting offset. Positive forward
            s.OF2(end).n3 = X(5);    % Vertical mounting offset. Positive downward from waterline
            s.OF2(end).n4 = X(6);    % Yaw rotation angle. Positive for clockwise rotation
            s.OF2(end).n5 = X(7);    % Roll rotation angle.Port side up is positive
            s.OF2(end).n6 = X(8);    % Pitch rotation angle. Bow up is positive
            s.OF2(end).n7 = X(9);    % Device latency in seconds
            
        case 'PRI' % Primary Navigation Device
            X = sscanf(line(5:end), '%f');
            s.PRI(end+1).dn = X(1);    % Device number
            
        case 'MBI' % Multibeam / Multiple Transducer Device information
            % Commentaires a completer %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            X = sscanf(line(5:end), '%f');
            s.MBI(end+1).dn = X(1);  % Device number
            s.MBI(end).st = X(2);    % Sonar type code
                                     %   
            s.MBI(end).sf = X(3);    % Sonar flags (Hexa)
                                     % 
            s.MBI(end).bd = X(4);    % Beam data (Hexa)
            %                        % 
            s.MBI(end).n1 = X(5);    % Number of beams, head 1 (multibeam)
                                     % or number of transducers (multitransducers)
            s.MBI(end).n2 = X(6);    % Number of beams, head 2 (multibeam)
            s.MBI(end).fa = X(7);    % First beam angle is for sonar type = fixed angle (degrees, TSS convention)
            s.MBI(end).ai = X(8);    % Angle increment is for sonar type = fixed angle (degrees, TSS convention)

        case 'SSI' % Sidescan device information
            X = sscanf(line(5:end), '%f');
            s.SSI(end+1).dn = X(1);  % Device number
            s.SSI(end).sf = X(2);    % Sonar flags (Hexa)
                                     %   0100=amplitude is bit-shifted into byte storage
            s.SSI(end).np = X(3);    % Number of samples per ping, port transducer
            s.SSI(end).ns = X(4);    % Number of samples per ping, starboard transducer
            
            SonarImage = zeros(0, s.SSI(end).np+s.SSI(end).ns);
            
        case 'LIN' % Planed Line data follows
            X = sscanf(line(5:end), '%f');
            s.LIN(end+1).nw = X(1);  % Number of waypoints
            
        case 'PTS'
            X = sscanf(line(5:end), '%f');
            s.PTS(end+1).x = X(1);  % Waypoint easting in work units
            s.PTS(end).y = X(2);    % Waypoint northing in work units
            
        case 'LBP' % Planed Line Begin Point
            X = sscanf(line(5:end), '%f');
            s.LBP(end+1).x = X(1);  % x grid position
            s.LBP(end).y = X(2);    % y grid position
            
        case 'LNN' % Planed Line name
            s.LNN(end+1) = line(5:end); % Line name or number
            
        case 'SVC' % Sound Velocity Correction
            X = sscanf(line(5:end), '%f');
            s.SVC(end+1).bd = X(1);  % Layer begin depth in work units, referenced to water surface
            s.SVC(end).ed = X(2);  % Layer end depth in work units
            s.SVC(end).sv = X(3);  % Layer sound velocity in meters/second
            
        case 'EOL' % End of planed line
        case 'EOH' % End of Header
            
            
        case 'HVF'  % Hyspeep view filters
            X = sscanf(line(5:end), '%f');
            s.HVF(end+1).dn = X(1);  % Dummy device number, always 99
            s.HVF(end).tt  = X(2);  % Time tag this filter set became active (seconds past midnight)
            s.HVF(end).p1  = X(3);  % Minimum depth in work unit
            s.HVF(end).p2  = X(4);  % Maximum depth in work unit
            s.HVF(end).p3  = X(5);  % Port side offset limit in work units
            s.HVF(end).p4  = X(6);  % Starboard side offset limit in work units
            s.HVF(end).p5  = X(7);  % Minimum beam angle limit, 0 to -90 degrees
            s.HVF(end).p6  = X(8);  % Maximum beam angle limit, 0 to 90 degrees
            
        case 'FIX' % FiX (Event) Mark
            X = sscanf(line(5:end), '%f');
            s.FIX(end+1).dn = X(1);  % Device number or 99
            s.FIX(end).t  = X(2);  % Time tag (seconds past midnight)
            s.FIX(end).n  = X(3);  % Event number
            
        case 'GPS' % GPS measurements
            X = sscanf(line(5:end), '%f');
            s.GPS(end+1).dn  = X(1);  % Device number
            s.GPS(end).t     = X(2);  % Time tag (seconds past midnight)
            s.GPS(end).cog   = X(3);  % Course over ground (degrees)
            s.GPS(end).sog   = X(4);  % Speed over ground (knots)
            s.GPS(end).hdop  = X(5);  % GPS hdop
            s.GPS(end).mode  = X(6);  % GPS mode
                                        % 0=unknown
                                        % 1=stand alone
                                        % 2=differential
                                        % 3=rtk
            s.GPS(end).nsats = X(7);  % Number of satelites
            
        case 'POS' % Position
            X = sscanf(line(5:end), '%f');
            s.POS(end+1).dn  = X(1);  % Device number
            s.POS(end).t     = X(2);  % Time tag (seconds past midnight)
            s.POS(end).x     = X(3);  % Eastings
            s.POS(end).y     = X(4);  % Northings
            
        case 'GYR' % Gyro data (heading)
            X = sscanf(line(5:end), '%f');
            s.GYR(end+1).dn  = X(1);  % Device number
            s.GYR(end).t     = X(2);  % Time tag (seconds past midnight)
            s.GYR(end).h     = X(2);  % Ship heading angle
            
        case 'RMB' % Raw multibeam data
            X = sscanf(line(5:end), '%f');
            s.RMB(end+1).dn     = X(1);  % Device number
            s.RMB(end).t        = X(2);  % Time tag (seconds past midnight)
            s.RMB(end).st       = X(3);  % Sonar type code (see MBI)
            s.RMB(end).sf       = X(4);  % Sonar flags (see MBI)
            s.RMB(end).bd       = X(5);  % Available beam data (see MBI)
            s.RMB(end).n        = X(6);  % Number of beams to follow
            s.RMB(end).sv       = X(7);  % Sound velocity in m/sec
            s.RMB(end).pn       = X(8);  % Ping number (or 0 if not tracked)
            if length(X) == 9
                s.RMB(end).settings = X(9);  % Optional field which provides info on current sonar settings
            end
            
            iPing_RMB = iPing_RMB + 1;

            line = fgetl(fid);
            BeamRanges(iPing_RMB,:) = sscanf(line, '%f')';
            
            line = fgetl(fid);
            BeamIntensity(iPing_RMB,:) = sscanf(line, '%f')';
                        
            line = fgetl(fid);
            BeamQualityCode(iPing_RMB,:) = sscanf(line, '%f')';
            
        case 'RSS' % Raw sidescan data
            X = sscanf(line(5:end), '%f');
            s.RSS(end+1).dn = X(1);  % Device number
            s.RSS(end).t    = X(2);  % Time tag (seconds past midnight)
            s.RSS(end).sf   = X(4);  % Sonar flags (Hexa)
                                         % 0100=amplitude is bit-shifted into byte storage
            s.RSS(end).np   = X(5);  % Number of samples, port transducer (dow-sampled to 2048 max)
            s.RSS(end).ns   = X(6);  % Number of samples, starboard transducer (dow-sampled to 2048 max)
            s.RSS(end).sv   = X();   % Sound velocity in m/sec
            s.RSS(end).pn   = X(7);  % Ping number (or 0 if not tracked)
            s.RSS(end).alt  = X(8);  % Altitude in work units
            s.RSS(end).sr   = X(9);  % Slant rate (samples per second after down-sample)
            s.RSS(end).amin = X(10);  % Amplitude minimum
            s.RSS(end).amax = X(11);  % Amplitude maximum
            s.RSS(end).bs   = X(12);  % Bit shift for byte recording

            line = fgetl(fid);
            PortSamples = sscanf(line, '%f');
%             figure(9994); plot(PortSamples)
            
            line = fgetl(fid);
            StarboardSamples = sscanf(line, '%f');
%             figure(9995); plot(StarboardSamples)

            iPing_RSS = iPing_RSS + 1;
            SonarImage(iPing_RSS, :) = [fliplr(PortSamples') StarboardSamples'];
            
        case 'HCP' % Heave compensation
            X = sscanf(line(5:end), '%f');
            s.HCP(end+1).dn = X(1);  % Device number
            s.HCP(end).t    = X(2);  % Time tag (seconds past midnight)
            s.HCP(end).h    = X(3);  % Heave in meters
            s.HCP(end).r    = X(4);  % Roll in degrees (+ port side up)
            s.HCP(end).p    = X(5);  % Pitch in degrees (+ bow up)
           
        otherwise
            str = sprintf('Enregistrement commancant par %s non encore decode dans import_HSX', TAG);
            my_warndlg(str, 1);
    end
end

%% Fermeture du fichier

fclose(fid);

x = linspace(-s.HSP.P3, s.HSP.P3, s.SSI(end).np+s.SSI(end).ns);
y = 1:size(SonarImage, 1);
resolution = s.HSP.P3 / s.RSS(end).np;

this = cl_image('Image', SonarImage, ...
    'Name',              'Sonar HSX', ...
    'x',                  x, ...
    'y',                  y, ...
    'XUnit',             'm', ...
    'YUnit',             'Ping', ...
    'ColormapIndex',     2, ...
    'DataType',          cl_image.indDataType('Reflectivity'), ...
    'GeometryType',      cl_image.indGeometryType('PingSamples'), ...
    'SonarRawDataResol', resolution, ...
    'SonarResolutionD',  resolution, ...
    'SonarResolutionX',  resolution);

heure = [s.RSS(:).t] * 1000;
t_sonar = cl_time('timeIfr', date, heure);
this = set(this, 'SonarTime', t_sonar);

X =  [s.RSS(:).alt];
this = set(this, 'SonarHeight', X);

X =  [s.RSS(:).pn];
this = set(this, 'SonarPingNumber', X);

heure_GPS = [s.GPS(:).t] * 1000;
t_GPS = cl_time('timeIfr', date, heure_GPS);
X = interp1(t_GPS, [s.GPS(:).sog], t_sonar);
this = set(this, 'SonarSpeed', X * 1852 / 3600);


heure_GYR = [s.GYR(:).t] * 1000;
t_GYR = cl_time('timeIfr', date, heure_GYR);
X = interp1(t_GYR, [s.GYR(:).h], t_sonar);
this = set(this, 'SonarHeading', X);

heure_HCP = [s.HCP(:).t] * 1000;
t_HCP = cl_time('timeIfr', date, heure_HCP);

SonarHeave = interp1(t_HCP, [s.HCP(:).h], t_sonar);
this = set(this, 'SonarHeave', SonarHeave);

SonarRoll = interp1(t_HCP, [s.HCP(:).r], t_sonar);
this = set(this, 'SonarRoll', -SonarRoll);

SonarPitch = interp1(t_HCP, [s.HCP(:).p], t_sonar);
this = set(this, 'SonarPitch', -SonarPitch);


a = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', 19);
[lat, lon] =  xy2latlon(a, [s.POS(:).x], [s.POS(:).y]);

heure_POS = [s.POS(:).t] * 1000;
t_POS = cl_time('timeIfr', date, heure_POS);
X = interp1(t_POS, lon, t_sonar);
Y = interp1(t_POS, lat, t_sonar);
this = set(this, 'SonarFishLatitude',  Y);
this = set(this, 'SonarFishLongitude', X);

this = set(this, 'SonarHeading', [s.RMB(:).sv]);


x = 1:size(BeamRanges, 2);
% XLim = compute_XYLim(x);
this(2) = this;
this(2) = set(this(2), ...
    'Image',                BeamRanges, ...
    'Name',                'BeamRanges HSX', ...
    'x',                    x, ...
    'TagSynchroX',          num2str(rand(1)), ...
    'XUnit',               'beam', ...
    'YUnit',               'Ping', ...
    'ColormapIndex',           3, ...
    'DataType',           cl_image.indDataType('AcrossDist'), ...
    'GeometryType',         cl_image.indGeometryType('PingBeam'));

this(3) = this(2);
this(3) = set(this(3), ...
    'Image',                BeamIntensity, ...
    'Name',                'BeamIntensity HSX', ...
    'DataType',           1);

this(4) = this(2);
this(4) = set(this(4), ...
    'Image',                BeamQualityCode, ...
    'Name',                'BeamQualityCode HSX', ...
    'DataType',           1);

A = -s.MBI.fa:-s.MBI.ai:s.MBI.fa;
EmissionAngle = repmat(A, length(y), 1);

this(5) = this(2);
this(5) = set(this(5), ...
    'Image',                EmissionAngle, ...
    'Name',                'EmissionAngle HSX', ...
    'DataType',           cl_image.indDataType('TxAngle'));

this(6) = this(5) + SonarRoll';
this(6) = set(this(6), ...
    'Image',                EmissionAngle, ...
    'Name',                'EmissionAngle + Roulis', ...
    'DataType',           cl_image.indDataType('TxAngle'));

this(7) = -this(2) * cos(SonarPitch' * (pi/180)) * cos(this(6) * (pi/180));
this(7) = set(this(7), ...
    'Name',                'Bathymetry HSX', ...
    'DataType',           cl_image.indDataType('Bathymetry'));

this(8) = this(2) * sin(this(6) * (pi/180));
this(8) = set(this(8), ...
    'Name',                'AcrossDistance HSX', ...
    'DataType',           cl_image.indDataType('AcrossDist'));

this(9) = this(2) * sin(SonarPitch' * (pi/180)) * cos(this(6) * (pi/180));
this(9) = set(this(9), ...
    'Name',                'AlongDistance HSX', ...
    'DataType',           cl_image.indDataType('AlongDist'));

flag = 1;


% this = set(this, 'SonarImmersion', Immersion);
% this = set(this, 'SonarCableOut', X);
% this = set(this, 'SonarYaw', X);
% this = set(this,  'Sonar_DefinitionENCours', ...
%     'SonarTVG_ConstructTypeCompens',        SonarTVG_ConstructTypeCompens, ...
%     'SonarTVG_ConstructTable',              SonarTVG_ConstructTable, ...
%     'SonarTVG_ConstructAlpha',              SonarTVG_ConstructAlpha, ...
%     'SonarTVG_ConstructConstante',          SonarTVG_ConstructConstante, ...
%     'SonarTVG_ConstructCoefDiverg',         SonarTVG_ConstructCoefDiverg, ...
%     'SonarDiagEmi_etat',                    SonarDiagEmi_etat, ...
%     'SonarDiagEmi_origine',                 SonarDiagEmi_origine, ...
%     'SonarDiagEmi_ConstructTypeCompens',    SonarDiagEmi_ConstructTypeCompens, ...
%     'SonarDiagRec_etat',                    SonarDiagRec_etat, ...
%     'SonarDiagRec_origine',                 SonarDiagRec_origine, ...
%     'SonarDiagRec_ConstructTypeCompens',    SonarDiagRec_ConstructTypeCompens, ...
%     'Sonar_NE_etat',                        Sonar_NE_etat, ...
%     'Sonar_SH_etat',                        Sonar_SH_etat, ...
%     'Sonar_GT_etat',                        Sonar_GT_etat);
% 
% this = set(this,  'Sonar_DefinitionENCours', ...
%     'SonarBS_etat',                         SonarBS_etat, ...
%     'SonarBS_origine',                      SonarBS_origine, ...
%     'SonarBS_origineBelleImage',            SonarBS_origineBelleImage, ...
%     'SonarBS_origineFullCompens',           SonarBS_origineFullCompens, ...
%     'SonarBS_IfremerCompensModele',         SonarBS_IfremerCompensModele, ...
%     'SonarBS_IfremerCompensTable',          SonarBS_IfremerCompensTable);
% this = set(this,  'SonarBathyCel_Z', Depth, ...
%     'SonarBathyCel_T', Temperature, ...
%     'SonarBathyCel_S', Salinity, ...
%     'SonarBathyCel_C', SoundSpeed);
% this = set(this, 'Sonar_DefinitionENCours', 'SonarTVG_etat', 1, 'SonarTVG_origine', 2);



function [nb_RMB, nb_RSS, nbSamplesPort, nbSamplesStarboard, nbBeams, flag] = preprocess_HSX(nomFic)

flag = 0;

fid = fopen(nomFic);
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.HSX']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        return
    end
end

nb_DEV = 0;
nb_DV2 = 0;
nb_OF2 = 0;
nb_PRI = 0;
nb_MBI = 0;
nb_SSI = 0;
nb_LIN = 0;
nb_PTS = 0;
nb_HVF = 0;
nb_LBP = 0;
nb_LNN = 0;
nb_SVC = 0;
nb_FIX = 0;
nb_GPS = 0;
nb_POS = 0;
nb_GYR = 0;
nb_RMB = 0;
nb_RSS = 0;
nb_HCP = 0;

while feof(fid) == 0
    line = fgetl(fid);
    TAG = line(1:3);
    switch TAG
        case 'FTP' % File type (Hypack File Identifier)
            
        case 'HSX' % HSX File Identifier
            
        case 'TND' % Survey Time and Date
            
        case 'INF' % General Information

        case 'HSP' % Hysweep Survey Parameters
            
        case 'DEV'  % Hypack Device Information
            nb_DEV = nb_DEV + 1;
            
        case 'DV2' % Hysweep Device Information
            nb_DV2 = nb_DV2 + 1;
            
        case 'OF2'  % Hysweep device Offsets
            nb_OF2 = nb_OF2 + 1;
            
        case 'PRI' % Primary Navigation Device
            nb_PRI = nb_PRI + 1;
            
        case 'MBI' % Multibeam / Multiple Transducer Device information
            nb_MBI = nb_MBI + 1;

        case 'SSI' % Sidescan device information
            nb_SSI = nb_SSI + 1;
            
        case 'LIN' % Planed Line data follows
            nb_LIN = nb_LIN + 1;
            
        case 'PTS'
            nb_PTS = nb_PTS + 1;
            
        case 'LBP' % Planed Line Begin Point
            nb_LBP = nb_LBP + 1;
            
        case 'LNN' % Planed Line name
            nb_LNN = nb_LNN + 1;
            
        case 'SVC' % Sound Velocity Correction
            nb_SVC = nb_SVC + 1;
            
        case 'EOL' % End of planed line
        case 'EOH' % End of Header
            
            
        case 'HVF'  % Hyspeep view filters
            nb_HVF = nb_HVF + 1;
            
        case 'FIX' % FiX (Event) Mark
            nb_FIX = nb_FIX + 1;
            
        case 'GPS' % GPS measurements
            nb_GPS = nb_GPS + 1;
            
        case 'POS' % Position
            nb_POS = nb_POS + 1;
            
        case 'GYR' % Gyro data (heading)
            nb_GYR = nb_GYR + 1;
            
        case 'RMB' % Raw multibeam data
            nb_RMB = nb_RMB + 1;
            line = fgetl(fid);%#ok
            line = fgetl(fid);%#ok
            line = fgetl(fid);
            
            X = sscanf(line, '%f');
            nbBeams = length(X);

        case 'RSS' % Raw sidescan data
            nb_RSS = nb_RSS + 1;
            line = fgetl(fid);
            X = sscanf(line, '%f');
            nbSamplesPort = length(X);
            
            line = fgetl(fid);
            X = sscanf(line, '%f');
            nbSamplesStarboard = length(X);
            
        case 'HCP' % Heave compensation
            nb_HCP = nb_HCP + 1;
           
        otherwise
            str = sprintf('Enregistrement commancant par %s non encore decode dans import_HSX', TAG);
            my_warndlg(str, 1);
    end
end

% --------------------
% Fermeture du fichier

fclose(fid);

flag = 1;
