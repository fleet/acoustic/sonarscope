% Importation Levitus
%
% Syntax
%   [flag, a] = import_Levitus(cl_image, nomFic, subTime, subDepth);
%
% Input Arguments
%   this     : Instance vide de cl_image
%   nomFic   : Nom du fichier .nc
%   subTime  : 
%   subDepth : 
%   
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('KBMA0504.ers');
%   nomFic = getNomFicDatabase('EM300_NIWA_DEPTH.ers');
%   [flag, b] = import_Levitus(cl_image, nomFic);
%   SonarScope(b)
%
% See also lecFicLevitusMat cl_image cl_ermapper Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = import_Levitus(this, nomFic, subTime, subDepth)

flag = 0;
that = this([]);

if ischar(nomFic)
    nomFic = {nomFic};
end

if (length(subTime) == 1) && (length(subDepth) > 1)
    subTime = repmat(subTime, size(subDepth));
elseif (length(subTime) > 1) && (length(subDepth) == 1)
    subDepth = repmat(subDepth, size(subTime));
elseif length(subTime) ~= length(subDepth)
    return
end

for k=1:length(nomFic)
    [flag, a] = import_Levitus_unitaire(nomFic{k}, subTime(k), subDepth(k));
    if ~flag
        return
    end
    that(end+1) = a; %#ok<AGROW>
end


function [flag, a] = import_Levitus_unitaire(nomFic, iTime, iDepth)

flag = 1;

[~, nomFicSeul] = fileparts(nomFic);
nomFicMat = getNomFicDatabase([nomFicSeul '.mat']);
 
m = matfile(nomFicMat);
[nDepth, nTime, nLat, nLon] = size(m, 'Value'); %#ok<ASGLU>
Lat  = m.Lat;
Lon  = m.Lon;
Date = m.DATE;
Unit = m.Unit;
Z    = m.Z(1,iDepth);
I    = squeeze(m.Value(iDepth, iTime, 1:nLat, 1:nLon));
subNaN = isnan(I);
I(subNaN) = NaN;

Date = strtrim(Date(iTime,:));

[~, Name] = fileparts(nomFic);
Name = sprintf('%s %s Z=%d m', Name, Date, Z);

% Remarque on fliplr car la création de film inverse l'image, c'est
% incompréensible mais on va faire avec
a = cl_image('Image', flipud(I), 'Unit', Unit, ...
    'x', double(Lon), 'y', double(flipud(Lat(:))), ...
    'XLabel', 'Lat', 'YLabel', 'Long', ...
    'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LongitudeLevitus', 'TagSynchroY', 'LatitudeLevitus', ...
    'Name', Name, 'InitialFileName', nomFic, 'InitialFileFormat', 'Netcdf');
