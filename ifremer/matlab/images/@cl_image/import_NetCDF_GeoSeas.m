% Importation d'un fichier NetCDF (*.grd) soit au format GeoSeas (NetCDF-CF,
% ...)
% cf. http://geoseas.ucc.ie/thredds/catalog/geoseas/catalog.html
%
% Syntax
%   [a, flag] = import_NetCDF_geoseas(b, nomFic)
%
% Input Arguments
%   b      : Instance de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('Ifremer_lion-16eme-bathy-cdi2012.mnt')
%   [a, flag] = import_NetCDF_geoseas(cl_image, nomFic);
%   imagesc(a)
%
% See also is_GMT Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_NetCDF_GeoSeas(this, nomFic, varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []); %#ok<ASGLU>

if isempty(DataType)
    [flag, DataType] = cl_image.edit_DataType;
    if ~flag
        return
    end
end

a = cl_netcdf('fileName', nomFic);
sk = get(a, 'skeleton');

%% Test si c'est un fichier GMT

numAtt1 = find_numAtt(a, 'title');
numAtt2 = find_numAtt(a, 'Conventions');
numVar1 = find_numVar(a, 'lat');
numVar2 = find_numVar(a, 'lon');
numVar3 = find_numVar(a, 'DEPTH');
if isempty(numAtt1) ||  isempty(numAtt2) || isempty(numVar1) || isempty(numVar2) || isempty(numVar3)
    flag = 0;
    this = [];
    str = sprintf('Le fichier %s ne semble pas etre un fichier NetCDF', nomFic);
    my_warndlg(str, 1);
    return
end
x = get_value(a, 'lon');
y = get_value(a, 'lat');
z = get_value(a, 'DEPTH');
Name = 'z';

% this = cl_image('Image', flipud(z'), ...
this = cl_image('Image', z, ...
    'Name',              Name, ...
    'x',                 double(x), ...
    'y',                 double(y), ...
    'XUnit',             'deg', ...
    'YUnit',             'deg', ...
    'TagSynchroX',       'Longitude', ...
    'TagSynchroY',       'Latitude', ...
    'ColormapIndex',     3, ...
    'DataType',          DataType, ...
    'GeometryType',      cl_image.indGeometryType('LatLong'), ...
    'InitialFileName',   nomFic, ...
    'InitialFileFormat', 'NetCDF-GeoSEAS');
Carto = cl_carto;
this = set_Carto(this, Carto);

flag = 1;

