% DTM from Prismed survey, résolution 500 m (EM12, Jean-Mascle, Geosciences-Azur)
%
% Syntax
%   a = cl_image.import_Prismed;
%
% INTPUT PARAMETERS :
%   I0 : Instance of cl_image
%
% Output Arguments
%   a : Instance of cl_image
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(sunShading(a));
%
% See also Prismed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function a = import_Prismed

[z, x, y, ~, ~, InitialFileName, InitialFileFormat] = Prismed;
a = cl_image('Image', z, 'Name', 'Prismed', 'ColormapIndex', 20, 'Unit', 'm', ...
    'x', x, 'y', y, 'XLabel', 'Longitude', 'XUnit', 'deg', 'YLabel', 'Latitude', 'YUnit', 'deg', ...
    'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', cl_image.indDataType('Bathymetry'), ...
    'InitialFileName', InitialFileName, 'InitialFileFormat', InitialFileFormat);
