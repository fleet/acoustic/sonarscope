% Importation d'un fichier SAR (*.im)
%
% Syntax
%   [flag, a] = import_SAR(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('SAR_0035.01.im')
%   nomFic = 'L:\APINIL\0034.03.im';
%   [flag, a] = import_SAR(cl_image, nomFic);
%   [flag, a] = import_SAR(cl_image, nomFic, 'subImage', 3);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this, Carto, SonarTVG_IfremerAlpha] = import_SAR(~, nomFic, varargin)

[varargin, Carto]                 = getPropertyValue(varargin, 'Carto', []);
[varargin, subImage]              = getPropertyValue(varargin, 'subImage', []);
[varargin, SonarTVG_IfremerAlpha] = getPropertyValue(varargin, 'SonarTVG_IfremerAlpha', []);
[varargin, MaxSlantRange]         = getPropertyValue(varargin, 'MaxSlantRange', []); %#ok<ASGLU>

this = cl_image.empty;

%% Choix des layers

if isempty(subImage)
    str1 = 'Choix des layers � importer';
    str2 = 'Select the layers you want to import';
    listeLayers{1} = 'Sidescan in Amp';
    listeLayers{2} = 'Sidescan in dB';
    listeLayers{3} = 'Subbottom';
    [subImage, flag] = my_listdlg(Lang(str1,str2), listeLayers, 'InitialValue', 2);
    if ~flag
        return
    end
end

%% Conversion en XML-Bin

flag = Sar2ssc(nomFic);
if ~flag
    return
end

%% Lecture des donn�es

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Sar', 'Memmapfile', -1);
if ~flag
    return
end

if isempty(SonarTVG_IfremerAlpha)
    SonarTVG_IfremerAlpha = SAR_ReadSonarTVG_IfremerAlpha(nomFic);
    flagSonarTVG_IfremerAlpha = isempty(SonarTVG_IfremerAlpha);
else
    flagSonarTVG_IfremerAlpha = 0;
end


%% Cr�ation des images

for k=1:length(subImage)
    switch subImage(k)
        case 1
            [flag, a, Carto, SonarTVG_IfremerAlpha] = import_Sidescan_Amp(nomFic, Data, MaxSlantRange, SonarTVG_IfremerAlpha, Carto);
        case 2
            [flag, a, Carto, SonarTVG_IfremerAlpha] = import_Sidescan_dB(nomFic, Data, MaxSlantRange, SonarTVG_IfremerAlpha, Carto);
        case 3
            [flag, a] = import_Subbottom(nomFic, Data, SonarTVG_IfremerAlpha);
    end
    if ~flag
        return
    end
    this(k) = a;
    
    if flagSonarTVG_IfremerAlpha
        SAR_SaveSonarTVG_IfremerAlpha(nomFic, SonarTVG_IfremerAlpha);
    end
end

function [flag, this, Carto, SonarTVG_IfremerAlpha] = import_Sidescan_Amp(nomFic, Data, MaxSlantRange, SonarTVG_IfremerAlpha, Carto)

y = 1:Data.Dimensions.nbPings;
resol = 0.25;
% x = linspace(resol/2, 750-resol/2, 3000);
x = -3000:3000;
[~, ImageName] = fileparts(nomFic);

if isempty(MaxSlantRange)
    sz = size(Data.Sonar);
    if sz(2) == 4001
        x = -2000:2000;
    else
        x = -3000:3000;
    end
    subx1 = 1:3000;
else
    MaxSlantSample = floor(MaxSlantRange / resol);
    subx = find((x > -MaxSlantSample) & (x < MaxSlantSample));
    x = x(subx);
end

SonarDescription = cl_sounder('Sonar.Ident', 5);

Sidescan = Data.Sonar(:,:) + 1;
this = cl_image('Image', Sidescan, ...
    'Name',           [ImageName ' - Sonar'], ...
    'Unit',                'Amplitude', ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'Sample', ...
    'YUnit',               'Ping', ...
    'ColormapIndex',        2, ...
    'DataType',             cl_image.indDataType('Reflectivity'), ...
    'TagSynchroX',          [ImageName ' - D'], ...
    'TagSynchroY',          [ImageName ' - Y'], ...
    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
    'SonarDescription',      SonarDescription, ...
    'SonarRawDataResol',    resol, ...
    'SonarResolutionD',     resol, ...
    'SonarResolutionX',     resol, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'SonarSAR');

% this = set(this, 'SonarPingNumber',    y);
this = set(this, 'SonarPingCounter',   y(:));
this = set(this, 'SonarImmersion',     Data.Immersion(:));
this = set(this, 'SonarHeight',        Data.Height(:));
this = set(this, 'SonarHeading',       Data.Heading(:));
this = set(this, 'SonarSpeed',         Data.Speed(:));
this = set(this, 'SonarFishLatitude',  Data.Latitude(:));
this = set(this, 'SonarFishLongitude', Data.Longitude(:));
this = set(this, 'SonarPortMode_1',    ones(Data.Dimensions.nbPings,1));
this = set(this, 'SonarPortMode_2',    ones(Data.Dimensions.nbPings,1));
SampFreq = get(SonarDescription, 'Proc.SampFreq');
this = set(this, 'SampleFrequency',    ones(Data.Dimensions.nbPings,1)* SampFreq);
this = set(this, 'SonarTime', Data.Time);
% this = set(this, 'SonarCableOut', X);
% this = set(this, 'SonarRoll', X);
% this = set(this, 'SonarPitch', X);
% this = set(this, 'SonarYaw', X);
SonarTVG_etat                 = 1;
SonarTVG_origine              = 2;
SonarTVG_ConstructTypeCompens = 2;
SonarTVG_ConstructTable       = []; % Definition a repporter dans cl_sounder
SonarTVG_ConstructAlpha       = 0;
SonarTVG_ConstructConstante   = 0;
SonarTVG_ConstructCoefDiverg  = 0;

SonarDiagEmi_etat                   = 2;
SonarDiagEmi_origine                = 1;
SonarDiagEmi_ConstructTypeCompens   = 1;

SonarDiagRec_etat                   = 2;
SonarDiagRec_origine                = 1;
SonarDiagRec_ConstructTypeCompens   = 1;

Sonar_NE_etat = 2;
Sonar_SH_etat = 2;
Sonar_GT_etat = 2;

SonarBS_etat = 2;  % Image Not compensatede

this = set(this,  'Sonar_DefinitionENCours', ...
    'SonarTVG_etat',                        SonarTVG_etat, ...
    'SonarTVG_origine',                     SonarTVG_origine, ...
    'SonarTVG_ConstructTypeCompens',        SonarTVG_ConstructTypeCompens, ...
    'SonarTVG_ConstructTable',              SonarTVG_ConstructTable, ...
    'SonarTVG_ConstructAlpha',              SonarTVG_ConstructAlpha, ...
    'SonarTVG_ConstructConstante',          SonarTVG_ConstructConstante, ...
    'SonarTVG_ConstructCoefDiverg',         SonarTVG_ConstructCoefDiverg, ...
    'SonarDiagEmi_etat',                    SonarDiagEmi_etat, ...
    'SonarDiagEmi_origine',                 SonarDiagEmi_origine, ...
    'SonarDiagEmi_ConstructTypeCompens',    SonarDiagEmi_ConstructTypeCompens, ...
    'SonarDiagRec_etat',                    SonarDiagRec_etat, ...
    'SonarDiagRec_origine',                 SonarDiagRec_origine, ...
    'SonarDiagRec_ConstructTypeCompens',    SonarDiagRec_ConstructTypeCompens, ...
    'Sonar_NE_etat',                        Sonar_NE_etat, ...
    'Sonar_SH_etat',                        Sonar_SH_etat, ...
    'Sonar_GT_etat',                        Sonar_GT_etat, ...
    'SonarBS_etat',                         SonarBS_etat);
% SignalFreq = get(SonarDescription, 'Signal.Freq');

[flag, Depth, Temperature, Salinity, SoundSpeed, SonarTVG_IfremerAlpha] = get_bathyCelerimetry(SonarDescription, Data, SonarTVG_IfremerAlpha);
if ~flag
    return
end

this = set(this,  'SonarBathyCel_Z', Depth, ...
    'SonarBathyCel_T', Temperature, ...
    'SonarBathyCel_S', Salinity, ...
    'SonarBathyCel_C', SoundSpeed);

H = Data.Immersion(:) + Data.Height(:);
CeleriteFond = interp1(Depth, SoundSpeed, H);
this = set(this, 'SonarSurfaceSoundSpeed', CeleriteFond);

Date = Data.Time.date;
[nuprom, nuLoi] = getNuLoi(Date(find(Date(1) ~= 0,1)), 0);
[D, a0, DeltaG, LimiteGMax] = getParamEprom(nuprom, nuLoi);
TVG_SAR = TvgSar(D, a0, DeltaG, LimiteGMax);

TVG_SAR = TVG_SAR(subx1);
SonarTVG_ConstructTable(1,:) = x(subx1);
SonarTVG_ConstructTable(2,:) = TVG_SAR;
this = set(this,  'Sonar_DefinitionENCours', 'SonarTVG_IfremerAlpha',  SonarTVG_IfremerAlpha, ...
    'SonarTVG_ConstructTable', SonarTVG_ConstructTable);

if isempty(Carto)
    Carto = getDefinitionCarto(this);
end
this = set(this,  'Carto', Carto);
this = update_Name(this);


function [flag, this, Carto, SonarTVG_IfremerAlpha] = import_Sidescan_dB(nomFic, Data, MaxSlantRange, SonarTVG_IfremerAlpha, Carto)

y = 1:Data.Dimensions.nbPings;
resol = 0.25;
sz = size(Data.Sonar);
if sz(2) == 4001
    x = -2000:2000;
else
    x = -3000:3000;
end
[~, ImageName] = fileparts(nomFic);

if isempty(MaxSlantRange)
    if sz(2) == 4001
        subx = 1:4001;
        subx1 = 1:2000;
    else
        subx = 1:6001;
        subx1 = 1:3000;
    end
else
    MaxSlantSample = floor(MaxSlantRange / resol);
    subx = find((x > -MaxSlantSample) & (x < MaxSlantSample));
    x = x(subx);
    subx1 =  find((x > -MaxSlantSample) & (x < 0));
end

SonarDescription = cl_sounder('Sonar.Ident', 5);

Sidescan = Data.Sonar(:,subx) + 1;
this = cl_image('Image', Sidescan, ...
    'Name',           [ImageName ' - Sonar'], ...
    'Unit',                'Amplitude', ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'Sample', ...
    'YUnit',               'Ping', ...
    'ColormapIndex',        2, ...
    'DataType',             cl_image.indDataType('Reflectivity'), ...
    'TagSynchroX',          [ImageName ' - D'], ...
    'TagSynchroY',          [ImageName ' - Y'], ...
    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
    'SonarDescription',      SonarDescription, ...
    'SonarRawDataResol',    resol, ...
    'SonarResolutionD',     resol, ...
    'SonarResolutionX',     resol, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'SonarSAR');

% this = set(this, 'SonarPingNumber',    y);
this = set(this, 'SonarPingCounter',   y(:));
this = set(this, 'SonarImmersion',     Data.Immersion(:));
this = set(this, 'SonarHeight',        Data.Height(:));
this = set(this, 'SonarHeading',       Data.Heading(:));
this = set(this, 'SonarSpeed',         Data.Speed(:));
this = set(this, 'SonarFishLatitude',  Data.Latitude(:));
this = set(this, 'SonarFishLongitude', Data.Longitude(:));
this = set(this, 'SonarPortMode_1',    ones(Data.Dimensions.nbPings,1));
this = set(this, 'SonarPortMode_2',    ones(Data.Dimensions.nbPings,1));
SampFreq = get(SonarDescription, 'Proc.SampFreq');
this = set(this, 'SampleFrequency',    ones(Data.Dimensions.nbPings,1)* SampFreq);
this = set(this, 'SonarTime', Data.Time);
% this = set(this, 'SonarCableOut', X);
% this = set(this, 'SonarRoll', X);
% this = set(this, 'SonarPitch', X);
% this = set(this, 'SonarYaw', X);
SonarTVG_etat                 = 1;
SonarTVG_origine              = 2;
SonarTVG_ConstructTypeCompens = 2;
SonarTVG_ConstructTable       = []; % Definition a repporter dans cl_sounder
SonarTVG_ConstructAlpha       = 0;
SonarTVG_ConstructConstante   = 0;
SonarTVG_ConstructCoefDiverg  = 0;

SonarDiagEmi_etat                   = 2;
SonarDiagEmi_origine                = 1;
SonarDiagEmi_ConstructTypeCompens   = 1;

SonarDiagRec_etat                   = 2;
SonarDiagRec_origine                = 1;
SonarDiagRec_ConstructTypeCompens   = 1;

Sonar_NE_etat = 1;
Sonar_SH_etat = 1;
Sonar_GT_etat = 2;
SonarBS_etat = 2;  % Image Not compensatede

this = set(this,  'Sonar_DefinitionENCours', ...
    'SonarTVG_etat',                        SonarTVG_etat, ...
    'SonarTVG_origine',                     SonarTVG_origine, ...
    'SonarTVG_ConstructTypeCompens',        SonarTVG_ConstructTypeCompens, ...
    'SonarTVG_ConstructTable',              SonarTVG_ConstructTable, ...
    'SonarTVG_ConstructAlpha',              SonarTVG_ConstructAlpha, ...
    'SonarTVG_ConstructConstante',          SonarTVG_ConstructConstante, ...
    'SonarTVG_ConstructCoefDiverg',         SonarTVG_ConstructCoefDiverg, ...
    'SonarDiagEmi_etat',                    SonarDiagEmi_etat, ...
    'SonarDiagEmi_origine',                 SonarDiagEmi_origine, ...
    'SonarDiagEmi_ConstructTypeCompens',    SonarDiagEmi_ConstructTypeCompens, ...
    'SonarDiagRec_etat',                    SonarDiagRec_etat, ...
    'SonarDiagRec_origine',                 SonarDiagRec_origine, ...
    'SonarDiagRec_ConstructTypeCompens',    SonarDiagRec_ConstructTypeCompens, ...
    'Sonar_NE_etat',                        Sonar_NE_etat, ...
    'Sonar_SH_etat',                        Sonar_SH_etat, ...
    'Sonar_GT_etat',                        Sonar_GT_etat, ...
    'SonarBS_etat',                         SonarBS_etat);
% SignalFreq = get(SonarDescription, 'Signal.Freq');

[flag, Depth, Temperature, Salinity, SoundSpeed, SonarTVG_IfremerAlpha] = get_bathyCelerimetry(SonarDescription, Data, SonarTVG_IfremerAlpha);
if ~flag
    return
end

this = set(this,  'SonarBathyCel_Z', Depth, ...
    'SonarBathyCel_T', Temperature, ...
    'SonarBathyCel_S', Salinity, ...
    'SonarBathyCel_C', SoundSpeed);

H = Data.Immersion(:) + Data.Height(:);
CeleriteFond = interp1(Depth, SoundSpeed, H);
this = set(this, 'SonarSurfaceSoundSpeed', CeleriteFond);

subBab = find(x > 0);
subTri = fliplr(find(x < 0));

Date = Data.Time.date;
[nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(Date(find(Date(1) ~= 0,1)), 0);
[D, a0, DeltaG, LimiteGMax] = getParamEprom(nuprom, nuLoi);
TVG_SAR = TvgSar(D, a0, DeltaG, LimiteGMax);
GM  = 36;
GC  = 20 * log10(51);
NEb = 227;
NEt = 225;

TVG_SAR = TVG_SAR(subx1);
SonarTVG_ConstructTable(1,:) = x(subx1);
SonarTVG_ConstructTable(2,:) = TVG_SAR;
this = set(this,  'Sonar_DefinitionENCours', 'SonarTVG_IfremerAlpha',  SonarTVG_IfremerAlpha, ...
    'SonarTVG_ConstructTable', SonarTVG_ConstructTable);

alpha = this.Sonar.TVG.IfremerAlpha;
gamma = this.Sonar.TVG.IfremerCoefDiverg;
DistanseObliqueEnKm = abs((x * resol) / 1000);
TVG_Ifremer(subBab) = (gamma * log10(DistanseObliqueEnKm(subBab) * 1000)) + (2 * alpha(1) * DistanseObliqueEnKm(subBab));
TVG_Ifremer(subTri) = (gamma * log10(DistanseObliqueEnKm(subTri) * 1000)) + (2 * alpha(1) * DistanseObliqueEnKm(subTri));

if isempty(Carto)
    Carto = getDefinitionCarto(this);
end
this = set(this,  'Carto', Carto);
this = update_Name(this);

%% Image en dB

% BT_dB = cl_memmapfile('Value', NaN, 'Size', [Data.Dimensions.nbPings, 6000], 'Format', 'single');
BT_dB = NaN(Data.Dimensions.nbPings, 6001, 'single');
for i=1:Data.Dimensions.nbPings
    RdB = 2 * reflec_Enr2dB(Sidescan(i,subBab));
    BT_dB(i,subBab) = RdB - TVG_SAR + TVG_Ifremer(subBab) - (GainFixeBab + GM + GC + NEb + ShBab);
    RdB = 2 * reflec_Enr2dB(Sidescan(i,subTri));
    BT_dB(i,subTri) = RdB - TVG_SAR + TVG_Ifremer(subTri) - (GainFixeTri + GM + GC + NEt + ShTri);
end
clear Sidescan

this.Image = BT_dB;
clear BT_dB
this = set(this, 'Name', [ImageName ' - Sonar dB'], 'Unit', 'dB');

%% Lecture du fichier de directivite des antennes

nomFicBab = getNomFicDatabase('SAR_saev12.ifr');
nomFicTri = getNomFicDatabase('SAR_sarer43.ifr');
[AntennesAnglesBab, DVb] = LecFicAntennes(nomFicBab);
[AntennesAnglesTri, DVt] = LecFicAntennes(nomFicTri);

%% Centrage des diagrammes

offsetBab = -7;
offsetTri = -1.5;
AntennesAnglesBab = AntennesAnglesBab - offsetBab;
AntennesAnglesTri = AntennesAnglesTri - offsetTri;

% Orientation des diagrammes par rapport a l'axe du SAR
% Les diagrammes mesures sont une vue de l'avant

AntennesAnglesTri = -AntennesAnglesTri;

%% Orientation des diagrammes en lateral

depointageBab = (90-25);
depointageTri = (90-25);
AntennesAnglesBab = AntennesAnglesBab + depointageBab;
AntennesAnglesTri = AntennesAnglesTri + depointageTri;

DVb = DVb - max(DVb);
DVt = DVt - max(DVt);

this.Sonar.DiagEmi.IfremerTypeCompens = 2;

PortMode_1 = median(this.Sonar.PortMode_1);
SonarDescription = set(SonarDescription, 'Sonar.Mode_1', PortMode_1);
ModelsCalibration = get_EmissionModelsCalibration(SonarDescription);

ModelsCalibration(1) = set(ModelsCalibration(1), 'XData', -AntennesAnglesBab, 'YData', DVb);
ModelsCalibration(2) = set(ModelsCalibration(2), 'XData', AntennesAnglesTri, 'YData', DVt);
SonarDescription = set_EmissionModelsCalibration(SonarDescription, ModelsCalibration);

this = set(this, 'SonarDescription', SonarDescription);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this = update_Name(this);


function [flag, this] = import_Subbottom(nomFic, Data, SonarTVG_IfremerAlpha)

x = (1:760);
y = 1:Data.Dimensions.nbPings;
resol = 0.25;
SonarDescription = cl_sounder('Sonar.Ident', 25);
[flag, Depth, Temperature, Salinity, SoundSpeed] = get_bathyCelerimetry(SonarDescription, Data, SonarTVG_IfremerAlpha);
if ~flag
    return
end
H = Data.Immersion(:) + Data.Height(:);
CeleriteFond = interp1(Depth, SoundSpeed, H);

[~, ImageName] = fileparts(nomFic);
Subbottom = Data.Sbp(:,:) + 1;
this = cl_image('Image',    Subbottom, ...
    'Name',            [ImageName ' - Sounder'], ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'Sample', ...
    'YUnit',               'Ping', ...
    'Unit',                 'Amp', ...
    'DataType',             cl_image.indDataType('Reflectivity'), ...
    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
    'TagSynchroX',          [ImageName ' - S'], ...
    'TagSynchroY',          [ImageName ' - Y'], ...
    'ColormapIndex',        2, ...
    'Video',                2, ...
    'SonarDescription',      SonarDescription, ...
    'SonarRawDataResol',    resol, ...
    'SonarResolutionD',     resol, ...
    'SonarResolutionX',     resol, ...
    'SonarBathyCel_Z',      Depth, ...
    'SonarBathyCel_T',      Temperature, ...
    'SonarBathyCel_S',      Salinity, ...
    'SonarBathyCel_C',      SoundSpeed, ...
    'SonarSurfaceSoundSpeed', CeleriteFond, ...
    'InitialFileName',      nomFic, ...
    'InitialFileFormat',    'SonarSAR');

this = set(this, 'SonarPingCounter',   y(:));
this = set(this, 'SonarImmersion',     Data.Immersion(:));
this = set(this, 'SonarHeight',        Data.Height(:));
this = set(this, 'SonarHeading',       Data.Heading(:));
this = set(this, 'SonarSpeed',         Data.Speed(:));
this = set(this, 'SonarFishLatitude',  Data.Latitude(:));
this = set(this, 'SonarFishLongitude', Data.Longitude(:));
this = set(this, 'SonarPortMode_1',    ones(Data.Dimensions.nbPings,1));
this = set(this, 'SonarPortMode_2',    ones(Data.Dimensions.nbPings,1));
this = set(this, 'SampleFrequency',    ones(Data.Dimensions.nbPings,1)* 3000);
this = set(this, 'SonarTime',          Data.Time);

flag = 1;


function [flag, Depth, Temperature, Salinity, SoundSpeed, SonarTVG_IfremerAlpha] = get_bathyCelerimetry(SonarDescription, Data, SonarTVG_IfremerAlpha)

SignalFreq = get(SonarDescription, 'BeamForm.Tx.Freq');
Date = Data.Time.date;
[Jour, Mois, Annee] = dayIfr2Jma(find(Date ~= 0,1));%#ok

disp('Recherche de la bathycelerimetrie')
latitude  = Data.Latitude(:);
longitude = Data.Longitude(:);
LatitudeMoyenne  = latitude( find(latitude ~= 0,1));
LongitudeMoyenne = longitude(find(latitude ~= 0,1));
z = cl_sound_speed('Incoming', 2, 'LevitusAveragingType', 3, ...
    'Latitude', LatitudeMoyenne, 'Longitude', LongitudeMoyenne, ...
    'Frequency', SignalFreq, 'LevitusMonth', Mois);
disp('Fin de la recherche de la bathycelerimetrie')

Depth = get(z, 'Depth');
if isempty(Depth)
    if isempty(SonarTVG_IfremerAlpha)
        str1 = 'TODO';
        str2 = sprintf('Le coefficient d''attenuation n''a pas pu etre d�duit de Levitus.\nSp�cifiez le vous-m�me');
        [flag, SonarTVG_IfremerAlpha] = inputOneParametre(Lang(str1,str2), Lang('Coefficient d''attenuation', 'Absorption coef value'), ...
            'Value', 0, 'Unit', 'dB/km/m', 'MinValue', 0, 'MaxValue', 200);
        if ~flag
            return
        end
    end
    % TODO
    str1 = 'Ici �a va planter car cette fonction n''est pas termin�e mais en principe on ne doit pas passer par ici, sauf dans votre cas on dirait. V�rifiez que vous avez install� correctement le r�pertoire de donn�es  SonarScopeData car cette fonction va chercher les donn�es Levitus dans C:\SonarScopeData\Level1\Levitus';
    my_warndlg(str1, 1);
else
    %     AveragedAbsorption = get(z, 'AveragedAbsorption');
    Temperature        = get(z, 'Temperature');
    Salinity           = get(z, 'Salinity');
    SoundSpeed         = get(z, 'SoundSpeed');
    
    if nargout == 5
        flag = 1;
        return
    end
    
    if isempty(SonarTVG_IfremerAlpha)
        Immersion = Data.Immersion(:);
        Immersion = mean(Immersion); % Immersion du sonar
        
        %              Depth = (Depth(:))';
        %              AveragedAbsorption = (AveragedAbsorption(:))';
        
        [~, iz] = min(abs(abs(Depth - Immersion)));
        SonarTVG_IfremerAlpha = AttenuationGarrison(SignalFreq, Depth, Temperature, Salinity, 'LimBathyLocale', [-Immersion -Immersion+100], 'Immersion', -Immersion);
        SonarTVG_IfremerAlpha = SonarTVG_IfremerAlpha(iz,:);
        
        p    = ClParametre('Name', Lang('A babord','On port'), ...
            'Unit', 'dB/km/m', 'Value', SonarTVG_IfremerAlpha(1), 'MinValue', 0, 'MaxValue', 200);
        p(2) = ClParametre('Name', Lang('A tribord','On starbord'), ...
            'Unit', 'dB/km/m', 'Value', SonarTVG_IfremerAlpha(2), 'MinValue', 0, 'MaxValue', 200);
        a = StyledSimpleParametreDialog('params', p, 'Title', 'Absorption coefficient deduced from Levitus');
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        SonarTVG_IfremerAlpha = a.getParamsValue;
    end
end
flag = 1;
