% Import a SEGY file
%
% Syntax
%   [flag, a, Carto] = import_SEGY(I0, Filename, ...)
%
% Input Arguments
%   I0       : Instance of cl_image
%   Filename : Name of the SEGY file
%
% Name-Value Pair Arguments
%   Carto      : Instance of cl_carto
%   listLayers : TODO GLU (Default : 0) ! bizarre !
%   RawData    : TODO GLU (Default : 0)
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Instance of cl_image
%
% Examples
%   TODO : trouver une image d'exemple et la mettre dans SonarScopeData
%   Filename = 'D:\Temp\data_louis2013\lineAB.seg';
%   [flag, a] = import_SEGY(cl_image, Filename);
%   imagesc(a);
%
% See also Authors
% Authors : GLU, JMA
% ----------------------------------------------------------------------------

function [flag, this, Carto] = import_SEGY(~, nomFic, varargin)

[varargin, Carto]      = getPropertyValue(varargin, 'Carto',      []);
[varargin, listLayers] = getPropertyValue(varargin, 'listLayers', 0);
[varargin, RawData]    = getPropertyValue(varargin, 'RawData',    0); %#ok<ASGLU>

this = [];

%% Lecture du fichier

seg = cl_segy('nomFic', nomFic, 'Memmapfile', 0);
if isempty(seg)
    flag = 0;
    return
end

this = cl_image.empty;

for iLayer=1:length(listLayers)
    if listLayers(iLayer) == 2
        flagDataProcess = 1; % On tente de visualiser les donn�es pr�-trait�es
    else
        flagDataProcess = 0;
    end
    
    %% Lecture des ent�tes de traces
    
    [flag, FileHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_Header');
    if ~flag
        return
    end
    
    %% Lecture des ent�tes de traces
    
    [flag, DataTrace] = read_TraceHeaders_bin(seg, 'Memmapfile', 1, 'flagDataProcess', flagDataProcess);
    if ~flag
        return
    end
    
    %% Lecture de l'image
    % Lecture limit�e du nombre de linges sismiques
    
    [nomDir, nomFicSeul, ~] = fileparts(get(seg, 'nomFic'));
    [flag, NbBlocs, NbLinesByBloc] = getNbBlocsData(nomDir, nomFicSeul, DataTrace.Dimensions.NbLines, flagDataProcess);
    if ~flag
        return
    end
    
    % D�termination des indices de Blocs et de Lignes respectifs.
    if NbBlocs > 1
        NbTraces = DataTrace.Dimensions.NbTraceHeaders;
        [flag, idxBloc, idxLine, ModeVisu] = getIdxBlocAndLine(NbBlocs, NbLinesByBloc, NbTraces);
        if ~flag
            return
        end
    else
        idxLine = 1;
        idxBloc = 1;
    end
    [flag, Data] = read_Data_bin(seg, 'Memmapfile', 1, 'flagDataProcess', flagDataProcess, ...
        'subBloc', idxBloc, 'RawData', RawData);
    if ~flag
        return
    end
    
    %% R�cup�ration du temps
    
    [flag, Time] = get_time(seg, 'DataTrace', DataTrace);
    if ~flag
        return
    end
    
    %% R�cup�ration de l'immersion
    
    [flag, Immersion] = get_signal(seg, 'Immersion', 'DataTrace', DataTrace);
    if ~flag
        return
    end
    % figure; plot(Immersion.Value); grid
    
    %% R�cup�ration de la navigation
    
    [flag, DataPosition, Carto] = read_position(seg, 'DataTrace', DataTrace, 'Carto', Carto);
    if ~flag
        return
    end
    
    %% Ecrasement des donn�es de Cache si on d�signe un fichier de Nav diff�rent de celui point� par l'objet
    
    nomFicNavObj = get(seg, 'NavigationFile');
    if ~isempty(DataPosition.NavigationFile)
        if ~strcmp(nomFicNavObj, DataPosition.NavigationFile)
            flagMemmapfileDataTrace     = 0;
            if isa(DataTrace.cdpX, 'cl_memmapfile')
                % Pour unlock des fichiers pour �crasement des donn�es de Cache.
                flagMemmapfileDataTrace     = 1;
                DataTrace.cdpX              = [];
                DataTrace.cdpY              = [];
                DataTrace.SourceX           = [];
                DataTrace.SourceY           = [];
                DataTrace.GroupX            = [];
                DataTrace.GroupY            = [];
                DataTrace.SourceGroupScalar = [];
                DataTrace.CoordinateUnits   = [];
            end
            [flag, DataTrace] = navSegy2ssc(seg, nomFic, DataTrace, DataPosition, flagMemmapfileDataTrace, 'flagDataProcess', flagDataProcess);
            if ~flag
                return
            end
            DataPosition.Longitude = DataTrace.cdpX;
            DataPosition.Latitude  = DataTrace.cdpY;
            seg = set(seg, 'nomFicNav', DataPosition.NavigationFile);
        end
    end
    
    %{
% TODO : sp�cial GinecoMebo SYS

nomFicNav = 'R:\EGINA\utilisateurs\Jean-Marie\SYSIF_GuinecoMebo\SY01.csv';
% nomFicNav = 'R:\EGINA\utilisateurs\Jean-Marie\SYSIF_GuinecoMebo\SY03.csv';
X = textread(nomFicNav);

x = X(:,1);
y = X(:,2);
% figure; plot(x, y, '-*'); grid on

Carto = cl_carto('Projection.Type', 3, 'Projection.UTM.Fuseau', 92);
[lat, lon] = xy2latlon(Carto, x, y);
% figure; PlotUtils.createSScPlot(lon, lat, '-*'); grid on

DataPosition.Latitude  = lat;
DataPosition.Longitude = lon;
    %}
    
    
    %% R�cup�ration de la fr�quence d'�chantillonnage
    
    [flag, SamplingFrequency] = get_signal(seg, 'SamplingFrequency', 'DataTrace', DataTrace);
    if ~flag
        return
    end
    
    %{
% TODO : sp�cial GinecoMebo SYS
SamplingFrequency.Value(:) = 10000
    %}
    
    %% R�cup�ration de la r�solution
    
    [flag, Resolution] = get_signal(seg, 'Resolution', 'DataTrace', DataTrace);
    if ~flag
        return
    end
    
    [resol, m] = unique(Resolution.Value);
    [~, im] = max(m);
    resol = resol(im);
    if length(resol) > 1
        str1 = 'Plusieurs valeurs de r�solution sont pr�sentes dans ce fichier. Je prends la valeur majoritaire : Ceci doit �tre am�lior�.';
        str2 = 'Several resolution are read in this file, I take the most represented, this must be improves..';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlusieursResolsDansFichier');
    end
    
    %% Lecture de la hauteur si elle existe
    
    % TODO : faire appel � get_signal
    Height = [];
    [flag, H] = readHeightSegyInCache(nomFic);
    if flag
        % Height = Data.Height + Immersion.Value;
        Height = H.Height;
    end
    
    % Heave
    [flag, Heave] = get_signal(seg, 'Heave', 'DataTrace', DataTrace);
    if ~flag
        return
    end
    Heave = Heave.Value;
    
    %% R�cup�ration de l'image
    
    % TODO / Gestion Ancienne / nouvelle version Supob par test cdpX == 0 de
    % GroupX : receiver, SourceX : �mission, cdpX : sera d�livr� par Subop
    % version 2.15
    
    % NbTraces            = DataTrace.Dimensions.NbTraceHeaders;
    
    %% Cr�ation de l'image
    
    SonarDescription = cl_sounder('Sonar.Ident', 26);
    
    [~, ImageNameSeul] = fileparts(nomFic);
    
    for k=1:numel(idxLine)
        if numel(idxBloc) == 1
            if iscell(Data.Data)
                Image = Data.Data{:,:};
                Image = Image(:,:);
            else
                Image = Data.Data(:,:);
            end
        else
            % Chargement de l'image selon la g�om�trie choisie.
            if ModeVisu == 1
                Image   = Data.Data{k};
                Image   = Image(:,:,idxLine(k));
            else
                Image = [];
                for kk=1:NbBlocs
                    subImage    = Data.Data{kk};
                    subImage    = subImage(:,idxLine(k),:);
                    subImage    = squeeze(subImage);
                    Image       = [Image subImage]; %#ok<AGROW>
                end
            end
            Image = my_transpose(Image);
        end
        % Donn�es stock�es horizontalement
        if (ndims(Data.Data) > 2) || (FileHeader.TraceSorting == 4) %#ok<ISMAT>
            % Pour les matrices 3D : transposition n�cessaire.
            Image = my_transpose(Image);
        end
        Image(Image == 0) = NaN;
        
        x = 1:size(Image,2); % TODO : attention �a peut varier Addition indices + ms !!!!!!!!!!!!!!!!!!!!!!
        y = 1:size(Image,1);
        if isempty(Height)
            Height = zeros(length(y),1, 'single');
        end
        
        
        if numel(idxBloc) > 1
            ImageName = [ImageNameSeul '_' num2str(k)];
        else
            ImageName = ImageNameSeul;
        end
        if flagDataProcess == 1
            ImageName = [ImageNameSeul '_Demod'];
        end
        this(end+1) = cl_image(...
            'Image',            Image, ...
            'Name',        ImageName, ...
            'Unit',             'Amp', ...
            'x',                x, ...
            'y',                y, ...
            'XUnit',            '"', ...
            'YUnit',            'Ping', ...
            'ColormapIndex',    2, ...
            'DataType',         cl_image.indDataType('Reflectivity'), ...
            'TagSynchroX',      ImageName, ...
            'TagSynchroY',      ImageName, ...
            'GeometryType',     cl_image.indGeometryType('PingSamples'), ...
            'InitialFileName',  nomFic, 'InitialFileFormat', 'SEGY', ...
            'SonarDescription', SonarDescription); %#ok<AGROW>
        
        this(end) = set(this(end), 'SonarRawDataResol', resol, 'SonarResolutionD', resol, 'SonarResolutionX', resol);
        
        %         ImmValue  = -Immersion.Value(:) * resol; % Modif JMA le 11/11/2012
        ImmValue  = -abs(Immersion.Value(:)); % Modif JMA le 11/11/2012
        this(end) = set(this(end), 'SonarPingCounter',        DataTrace.TraceNumber(:));
        this(end) = set(this(end), 'SonarImmersion',          ImmValue);
        this(end) = set(this(end), 'SonarHeight',             Height(:));
        this(end) = set(this(end), 'SonarHeave',              Heave(:));
        % this = set(this, 'SonarHeading',       Cap(:));
        % this = set(this, 'SonarSpeed',         Vitesse(:));
        this(end) = set(this(end), 'SonarFishLatitude',       DataPosition.Latitude(:));
        this(end) = set(this(end), 'SonarFishLongitude',      DataPosition.Longitude(:));
        this(end) = set(this(end), 'SampleFrequency',         SamplingFrequency.Value(:));
        this(end) = set(this(end), 'SonarSurfaceSoundSpeed',  DataTrace.WeatheringVelocity(:)); % SubWeatheringVelocity
        
        this(end) = set(this(end), 'SonarTime', Time);
        
        [flag, Tide] = readTide_mat(seg);
        if flag
            this(end) = set(this(end), 'SonarTide', Tide.Value);
        end
        
        if isempty(Carto)
            Carto = getDefinitionCarto(this(end));
        end
        this(end) = set(this(end),  'Carto', Carto);
        this(end) = update_Name(this(end));
    end
end

this = this(:);
flag = 1;
