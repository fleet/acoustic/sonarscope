% Importation d'un fichier SRTM (*.grd)
%
% Syntax
%   [a, flag] = import_SRTM(a, nomFic, ...)
%
% Input Arguments
%   a      : Instance vide de cl_image
%   nomFic : Nom du fichier
%
% Name-Value Pair Arguments
%   GMTMask : Masquage par un masque calcul� par GMT
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('N48W005.hgt');
%   [a, flag] = import_SRTM(cl_image, nomFic, 'GMTMask', 1);
%   imagesc(a)
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_SRTM(this, nomFic, varargin)

[varargin, GMTMask] = getPropertyValue(varargin, 'GMTMask', 0); %#ok<ASGLU>

% nbOc = sizeFic(nomFic);

fid = fopen(nomFic, 'r', 'b');
if fid == -1
    str = sprintf('%s doesn''t exist or is not readable.', nomFic);
    my_warndlg(str, 1);
    return
end
NbCol = 1201;
NbLig = 1201;
Z = fread(fid, [NbCol NbLig], 'int16');
fclose(fid);

[~, nomFicSeul] = fileparts(nomFic);
if strcmpi(nomFicSeul(4), 'W')
    LonWest = -str2double(nomFicSeul(5:7));
else
    LonWest = str2double(nomFicSeul(5:7));
end
if strcmpi(nomFicSeul(1), 'N')
    LatNord = str2double(nomFicSeul(2:3));
else
    LatNord = -str2double(nomFicSeul(2:3));
end

NbCol = 1201;
NbLig = 1201;
lon = linspace(LonWest, LonWest+1, NbCol);
lat = linspace(LatNord, LatNord+1, NbLig);

Z = single(flipud(Z'));
Z(Z == -32768) = NaN;

Z(Z <= 0) = NaN;

if GMTMask
    [~, ~, M] = getGRDMask([LonWest LonWest+1], [LatNord LatNord+1], NbCol, NbLig, NaN, 1);
    Z = Z .* M;
    se = strel('square', 5) ;
    M = imdilate(M==1, se);
    Z = Z .* M;
end

DataType = cl_image.indDataType('Bathymetry');
this = cl_image('Image',    Z, ...
    'Name',                nomFicSeul, ...
    'x',                    lon, ...
    'y',                    lat, ...
    'XUnit',               'deg', ...
    'YUnit',               'deg', ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         cl_image.indGeometryType('LatLong'), ...
    'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'SRTM');

flag = 1;
