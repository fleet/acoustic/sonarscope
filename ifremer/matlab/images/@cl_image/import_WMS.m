% Importation d'une donnée sur un serveur géographique WMS
%
% Syntax
%   [flag, a] = import_WMS(cl_image, lon_range, lat_range, 'Resol', ...)
%
% Input Arguments
%   wms : Structure contenant
%         server      : Nom du serveur WMS
%         layer       : Nom du layer
%         styles      : 
%         format      : 
%         transparent : 
%         
%   lon_range  : Etendue en longitude (deg)
%   lat_range  : endue en latitude (deg)
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Remarks : Il faut une liaison internet pour utiliser cette fonctionnalité
%
% Examples
%   %wms.server='http://maps.gdal.org/cgi-bin/mapserv_dem?';
%   %wms.layer='srtmplus_raw';
%   %Full SRTM30PLUS info:  http://topex.ucsd.edu/WWW_html/srtm30_plus.html
%   wms.server = 'http://onearth.jpl.nasa.gov/wms.cgi?';
%   wms.layer  = 'srtmplus';
%   wms.styles = 'short_int';
%   wms.format = 'image/geotiff&version=1.1.1';
%   import_WMS(wms, [10 20],[40 46], 'Resol', 30);
%   [flag, a] = import_WMS([10 20],[40 46], 'Resol', 30);
%   SonarScope(a)
%
% See also Authors
% References : Rich Signell (rsignell@usgs.gov)
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [flag, a] = import_WMS(this, wms, lon_range, lat_range, nx, ny) %#ok


% http://svs.gsfc.nasa.gov/
% http://www.demis.nl/home/pages/wms/docs/OpenGISWMS.htm


flag = 0;
a    = [];

lon = linspace(lon_range(1), lon_range(2), nx);
lat = linspace(lat_range(1), lat_range(2), ny);

if strcmp(wms.server(end), '?')
    url = sprintf('%sRequest=GetMap', wms.server);
else
    url = sprintf('%s&Request=GetMap', wms.server);
end

if ~isempty(wms.styles)
    url = sprintf('%s&Styles=%s', url, wms.styles);
end
if ~isempty(wms.layer)
    url = sprintf('%s&Layers=%s', url, wms.layer);
end

url = sprintf('%s&Bbox=%d,%d,%d,%d', url, lon_range(1), lat_range(1), lon_range(2), lat_range(2));

if ~isempty(wms.srs)
    if strcmp(wms.srs(1:4), 'EPSG')
        url = sprintf('%s&SRS=%s', url, wms.srs);
    else
        url = sprintf('%s&CRS=%s', url, wms.srs);
    end
end
if ~isempty(wms.transparent)
    url = sprintf('%s&TRANSPARENT=%s', url, wms.transparent);
end
if ~isempty(nx)
    url = sprintf('%s&Width=%d', url, nx);
end
if ~isempty(ny)
    url = sprintf('%s&Height=%d', url, ny);
end
if ~isempty(wms.format)
    url = sprintf('%s&Format=%s', url, wms.format);
end

str = sprintf('URL composed by SonarScope : %s', url);
my_warndlg(str, 0);

try
    [pix, map] = imread_WMS(url);
catch %#ok<CTCH>
    messageErreurFichier(url, 'ReadFailure')
    return
end

% -------------------
% Autoplot activation

a = cl_image('Image', my_flipud(pix), 'x', lon, 'y', lat', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'XLabel', 'Longitude', 'YLabel', 'Latiitude', 'GeometryType', 3, ...
    'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
    'InitialFileName', url, 'InitialFileFormat', 'WMS');

if isempty(map)
    a.ColormapIndex = 3;
else
    a.ColormapIndex  = 1;
    a.ColormapCustom = map;
end

flag = 1;
