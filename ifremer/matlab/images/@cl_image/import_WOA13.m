% Importation WOA13
%
% Syntax
%   [flag, a] = import_WOA13(cl_image, nomFic, subTime, subDepth);
%
% Input Arguments
%   this     : Instance vide de cl_image
%   nomFic   : Nom du fichier .nc
%   subTime  : 
%   subDepth : 
%   
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Instance de cl_image
%
% Examples
%   TODO
%
% See also lecFicWOA13Mat cl_image cl_ermapper Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = import_WOA13(~, nomFic, subDepth)

flag = 0;
that = cl_image.empty;

if ischar(nomFic)
    nomFic = {nomFic};
end

for k=1:length(nomFic)
    [flag, a] = import_WOA13_unitaire(nomFic{k}, subDepth(k));
    if ~flag
        return
    end
    that(end+1) = a; %#ok<AGROW>
end


function [flag, a] = import_WOA13_unitaire(nomFic, iDepth)

flag = 1;
 
m = matfile(nomFic);
[nLon, nLat, nTime, nDepth] = size(m, 'Value'); %#ok<ASGLU>
Lat  = m.Lat;
Lon  = m.Lon;
% Date = m.DATE;
Unit = m.Unit;
Z    = m.Z(1,iDepth);
I    = squeeze(m.Value(1:nLon, 1:nLat, 1, iDepth));
I    = I';

% Date = strtrim(Date(iTime,:));

[~, Name] = fileparts(nomFic);
Name = sprintf('%s %s Z=%d m', Name, m.TIMESTART, Z);

% Remarque on fliplr car la création de film inverse l'image, c'est
% incompréensible mais on va faire avec
a = cl_image('Image', flipud(I), 'Unit', Unit, ...
    'x', double(Lon), 'y', double(flipud(Lat(:))), ...
    'XLabel', 'Lat', 'YLabel', 'Long', ...
    'XUnit', 'deg', 'YUnit', 'deg', ...
    'TagSynchroX', 'LongitudeWOA13', 'TagSynchroY', 'LatitudeWOA13', ...
    'Name', Name, 'InitialFileName', nomFic, 'InitialFileFormat', 'WOA13');
