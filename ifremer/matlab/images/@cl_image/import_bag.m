% Importation d'un fichier BAG (par conversion en Geotiff)
%
% Syntax
%    a = import_bag(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%
% See also cl_image/import_* Authors
% Authors : GLU (sur la base de import_geotiff)
%--------------------------------------------------------------------------

function a = import_bag(~, nomFic, varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []); %#ok<ASGLU>


%% R�cup�ration info GDAL_NODATA via gdalinfo.exe
% R�cup�ration du chemin de l'�x�cutable
nomExeInfo          = 'gdalinfo.exe';
nomExe              = 'gdal_translate.exe';
[~, rootFicTif, ~]   = fileparts(nomFic);
nomFicTif            = fullfile(my_tempdir, [rootFicTif '.tif']);

% En 64 bits, on installe GDAL depuis un paquet Core msi
% avec le SetUp External.
pathGDAL = getenv('GDAL_HOME');

if ~exist(fullfile(pathGDAL, nomExeInfo), 'file')
    str1 = sprintf('Le fichier "%s" n''a pas �t� trouv� sur votre ordinateur, if vous faut r�cup�rer le dernier "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" qui vous installera ce composant.', nomExeInfo);
    str2 = sprintf('File "%s" not found, you should download and install the last "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" that will install this component.', nomExeInfo);
    my_warndlg(Lang(str1,str2), 1);
end

% Conversion pour r�cup�rer les donn�es Geo en attendant de consolider le tout par bag.

cmd = sprintf('"%s" -co COMPRESS:NONE "%s" "%s"', fullfile(pathGDAL, nomExe), nomFic, nomFicTif);
[status, result] = system(cmd, '-echo');
if status
    str1 = sprintf('Echec de gdal_translate : %s\n%s', nomFic, result);
    str2 = sprintf('gdalInfo has failed for "%s"\n%s', nomFic, result);
    my_warndlg(Lang(str1,str2), 0);
    flag = 0;
    return
end
sInfoTif  = geotiffinfo(nomFicTif);
sInfoTifG = imfinfo(nomFicTif); % Infos graphiques.


if exist(nomFicTif, 'file')
    delete(nomFicTif);
end

% Lecture des donn�es BAG, utilisation de cette fonction en attendant de lire
% les diff�rents layers depuis le GeoTIFF.
sBagRead = read_bag(nomFic, 'read_unc', 'true');
if isempty(sBagRead )
    str1 = sprintf('Echec de read_bag : %s\n%s', nomFic);
    str2 = sprintf('read_bag has failed for "%s"\n%s', nomFic, result);
    my_warndlg(Lang(str1,str2), 0);
end

TagSynchroXY = [];

if strcmp(sInfoTif.ModelType, 'ModelTypeProjected')
    GeometryType = cl_image.indGeometryType('GeoYX');
elseif strcmp(sInfoTif.ModelType, 'ModelTypeGeographic')
    GeometryType = cl_image.indGeometryType('LatLong');
elseif strcmpi(sInfoTif.ModelType, 'unknown')
    GeometryType = 1;
else
    GeometryType = 1;
end

switch lower(sInfoTif.ColorType)
    case 'indexed'
        ImageType = 3;
        ColormapIndex = 1;
    case 'truecolor'
        ImageType = 2;
        ColormapIndex = 1;
    case 'grayscale'
        ImageType = 1;
        ColormapIndex = 3; %jet
    otherwise
        ImageType = 1;
        % 'A COMPLETER'
end

map = jet(256);

x   = sBagRead.x;
y   = sBagRead.y;
I   = sBagRead.z;
Iu  = sBagRead.unc;
[~, ImageName] = fileparts(nomFic);

if isfield(sInfoTifG, 'GDAL_NODATA')
   I(sInfoTifG.GDAL_NODATA) = NaN;
   Iu(sInfoTifG.GDAL_NODATA) = NaN;
end

DataType    = 2; % 'Bathymetry';
Unit        = 'm';
% 1ere Image = Bathymetry
a = cl_image('Image', I, ...
    'Unit',             Unit, ...
    'Name',             ImageName, ...
    'DataType',          DataType, ...
    'GeometryType',      GeometryType, ...
    'x',                 x, ...
    'y',                 y, ...
    'ImageType',         ImageType, ...
    'ColormapIndex',     ColormapIndex, ...
    'InitialFileName',   nomFic, ...
    'InitialFileFormat', 'BAG');
clear I

DataType    = 102; % UncertaintyMeters
Unit        = 'm';

U = cl_image('Image', Iu, ...
    'Unit',              Unit, ...
    'Name',              ImageName, ...
    'DataType',          DataType, ...
    'GeometryType',      GeometryType, ...
    'x',                 x, ...
    'y',                 y, ...
    'ImageType',         ImageType, ...
    'ColormapIndex',     ColormapIndex, ...
    'InitialFileName',   nomFic, ...
    'InitialFileFormat', 'BAG');

if isfield(sInfoTif, 'ImageDescription')
    a = set(a, 'Comments', sInfoTif.ImageDescription);
    U = set(U, 'Comments', sInfoTif.ImageDescription);
end

if ~isempty(map)
    a.ColormapIndex  = 1;
    a.ColormapCustom = map;
    U.ColormapIndex  = 1;
    U.ColormapCustom = map;
end

a = set(a, 'Comments', rootFicTif);
U = set(U, 'Comments', rootFicTif);

%% R�f�rencement de la carto de l'image.

% Conversion des params Geotiff en param�tres SSC pour configuration de la classe Carto.
Carto = convertGeotiffParams2SSCParams(cl_carto([]), sInfoTif);

a = set(a, 'Carto', Carto);
U = set(U, 'Carto', Carto);


switch  GeometryType
    case cl_image.indGeometryType('LatLong')
        TagSynchroX = 'Longitude';
        TagSynchroY = 'Latitude';
        XYunit = 'deg';
    case cl_image.indGeometryType('GeoYX')
        TagSynchroXY    = shortDescription(Carto);
        TagSynchroX     = TagSynchroXY;
        TagSynchroY     = TagSynchroXY;
        XYunit = 'm';
    otherwise
        if isempty(TagSynchroXY)
            TagSynchroXY = num2str(rand(1));
        end
        TagSynchroX = TagSynchroXY;
        TagSynchroY = TagSynchroXY;
        XYunit = [];
end

a = set(a,...
    'TagSynchroX',  TagSynchroX,...
    'TagSynchroY',  TagSynchroY,...
    'XUnit',        XYunit,...
    'Yunit',        XYunit);
U = set(U,...
    'TagSynchroX',  TagSynchroX,...
    'TagSynchroY',  TagSynchroY,...
    'XUnit',        XYunit,...
    'Yunit',        XYunit);

a = [a(:)' U(:)'];

