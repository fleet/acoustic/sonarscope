function this = import_coastLinesFile(this, nomFic)

%% Lecture du fichier .cot (Coast lines given by Daniel Aslanian for geodynamic studies)

[flag, shapes] = read_coastLines(nomFic);
if ~flag
    return
end
%% S�paration des contours ferm�s

shapesOut = shapes;
shapesOut(:) = [];
N = length(shapes);
for k=1:N
    X = shapes(k).X;
    Y = shapes(k).Y;
    
    kSame = find((X == X(1)) & (Y == Y(1)) | isnan(X) | isnan(Y));
    while length(kSame) > 1
        shapesOut(end+1) = shapes(k); %#ok<AGROW>
        shapesOut(end).X = X(1:kSame(2)-1);
        shapesOut(end).Y = Y(1:kSame(2)-1);
        X = X(kSame(2):end);
        Y = Y(kSame(2):end);
        kSame = find((X == X(1)) & (Y == Y(1)));
    end
    shapesOut(end+1) = shapes(k); %#ok<AGROW>
    shapesOut(end).X = X;
    shapesOut(end).Y = Y;
end

%{
figure
color = 'brgkymc';
for k=1:length(shapesOut)
    kmod = 1 + mod(k-1,length(color));
    PlotUtils.createSScPlot(shapesOut(k).X, shapesOut(k).Y, color(kmod)); grid on; hold on;
%     PlotUtils.createSScPlot(shapesOut(k).X, color(kmod)); grid on; hold on;
%     PlotUtils.createSScPlot(shapesOut(k).Y, color(kmod)); grid on; hold on;
end
%}

N = length(shapesOut);
for k=1:N
    X = shapesOut(k).X;
%     X = (unwrap((X+180) * (pi/180)) * (180/pi)) - 180;
    range = max(X)-min(X);
    if range > 350
        sub = find(X < 0);
        X(sub) = X(sub) + 360;
    end
    shapesOut(k).X = X;
end

figure
color = 'brgkymc';
for k=1:length(shapesOut)
    kmod = 1 + mod(k-1,length(color));
    plot(shapesOut(k).X, shapesOut(k).Y, color(kmod)); grid on; hold on;
end
[~, racineNomFic, ~] = fileparts(nomFic);
title(['Plate : ' racineNomFic])
shapes = shapesOut;

%% Import des r�gions d'inter�t

couleurs = [0 0 1; 1 0 0; 0 1 0; 0 0 0; 1 1 0; 1 0 1; 0 1 1];
ROI = get(this, 'RegionOfInterest');
N = length(shapes);
hw = create_waitbar(Lang('Importation ROI...','ROI importation ...'), 'N', N);
for k=1:N
    kmod = 1 + mod(k-1,size(couleurs,1));
    my_waitbar(k, N, hw);
    
    ROI(end+1).xy(1,:) = shapes(k).X; %#ok<AGROW>
    ROI(end).xy(2,:)   = shapes(k).Y;
    ROI(end).labels    = shapes(k).labels;
    ROI(end).nomCourbe = shapes(k).nomCourbe;
    ROI(end).couleur   = couleurs(kmod,:);
end
my_close(hw);
this = set(this, 'RegionOfInterest', ROI);

