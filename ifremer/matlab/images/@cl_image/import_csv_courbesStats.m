% Import the statistical curves of an image in an .csv file
% The image must have an existing curve used as a signatures for all the
% properties of the curves (DataType, name of conditional layers, etc ...).
% A new curve will be created using the same properties but the values will
% be the ones read in the ASCII file.
%
% Syntax
%   flag = import_csv_courbesStats(this, CurvesFilename, ...)
%
% Input Arguments
%   a              : An instance of cl_image
%   CurvesFilename : File name for the statistical curves
%
% Name-Value Pair Arguments
%   sub : Index of the curves to import (default) all the curves)
%
% Name-only Arguments
%   last : Only the las curve will be saved
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
%     aKM = cl_simrad_all('nomFic', nomFic);
%     [flag, a] = import_DefaultLayers(aKM);
%     SonarScope(a)
%
%     DTReflec = cl_image.indDataType('Reflectivity');
%     [kReflec, nomLayerReflec] = findIndLayerSonar(a, 1, 'DataType', DTReflec, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTAngles = cl_image.indDataType('TxAngle');
%     [kAngles, nomLayerAngles] = findIndLayerSonar(a, 1, 'DataType', DTAngles, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTSectors = cl_image.indDataType('TxBeamIndex');
%     [kSectors, nomLayerSectors] = findIndLayerSonar(a, 1, 'DataType', DTSectors, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     Selection(1).Name     = nomLayerAngles;
%     Selection(1).TypeVal  = 'Layer'
%     Selection(1).indLayer = kAngles;
%     Selection(1).DataType = DTAngles;
%     Selection(2).Name     = nomLayerSectors;
%     Selection(2).TypeVal  = 'Layer'
%     Selection(2).indLayer = kSectors;
%     Selection(2).DataType = DTSectors;
%     bins{1} = -75:75;
%     bins{2} = 1:3;
%     [a(kReflec), bilan] = courbesStats(a(kReflec), nomLayerReflec, 'Courbe test', '', [], ...
%         a, Selection, 'bins', bins, 'GeometryType', 'PingBeam', 'Sampling' , 1);
%     CurvesFilename = my_tempname('.mat');
%   flag = import_csv_courbesStats(a(kReflec), CurvesFilename)
%     a(kReflec) = deleteCourbesStats(a(kReflec));
%
%     [flag, a(kReflec)] = import_csv_courbesStats(a(kReflec), CurvesFilename);
%     plotCourbesStats(a(kReflec), 'Stats', 0);
%
% See also cl_image/import_csv_courbesStats Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, this] = import_csv_courbesStats(this, CurvesFilename, indCurve)

flag = 0;

Courbes = this.CourbesStatistiques(indCurve);
nbSubCurves = length(Courbes.bilan{1});

for k=1:nbSubCurves
end

M = csvread(CurvesFilename);
if size(M,2) ~= (2*nbSubCurves)
    str1 = sprintf('D�sol�, le nombre de colonnes du fichir .csv est %d or la courbe s�lectionn�e a %s segments.', size(M,2), nbSubCurves);
    str2 = sprintf('Sorry, the number of column in the .csv file is %d but the selected curve is made of %s segments.', size(M,2), nbSubCurves);
    my_warndlg(Lang(str1,str2), 1);
    return
end

figure; 
for k=1:nbSubCurves
    x = M(:,1+(k-1)*2);
    y = M(:,2+(k-1)*2);
    
    plot(Courbes.bilan{1}(k).x, Courbes.bilan{1}(k).y);
    grid on; hold on;
    plot(x, y, '-k*'); grid on; hold on;
    
    Courbes.bilan{1}(k).y = my_interp1(x, y, Courbes.bilan{1}(k).x);
end
this.CourbesStatistiques(end+1) = Courbes;

flag = 1;
