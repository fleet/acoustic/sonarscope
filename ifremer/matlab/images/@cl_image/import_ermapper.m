% Importation d'un fichier ERMAPPER (*.ers)
%
% Syntax
%   [flag, a] = cl_image.import_ermapper(nomFic);
%
% Input Arguments
%   this   : Instance vide de cl_image
%   nomFic : Nom du fichier .ers
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('KBMA0504.ers');
%   nomFic = getNomFicDatabase('EM300_NIWA_DEPTH.ers');
%   [flag, b] = cl_image.import_ermapper(nomFic);
%   SonarScope(b)
%
% See also cl_image cl_ermapper Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, b] = import_ermapper(nomFic, varargin)

[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false);
[varargin, subz]         = getPropertyValue(varargin, 'subz',         []);

if ischar(nomFic)
    nomFic = {nomFic};
end

nbFiles = length(nomFic);
if nbFiles == 0
    flag = 0;
    b    = cl_image.empty;
    return
end

for k=1:nbFiles
    [flag, a] = import_ermapper_unitaire(nomFic{k}, subz, memeReponses, varargin{:});
    if flag
        b{k} = a; %#ok<AGROW>
    else
        b = cl_image.empty;
        return
    end
end
b = [b{:}];

function [flag, this] = import_ermapper_unitaire(nomFic, subz, memeReponses, varargin)

persistent persistent_DataType

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 1); %#ok<ASGLU>

flag = 0;
this = cl_image.empty;

[nomDir, nomFicSeul, extension] = fileparts(nomFic);
if isempty(extension)
    extension = '.ers';
end
if ~strcmp(extension, '.ers')
    nomFicSeul = [nomFicSeul, extension];
    extension = '.ers';
end

nomFicXML = fullfile(nomDir, [nomFicSeul '.xml']);
if exist(nomFicXML, 'file')
    [flag, this] = cl_image.import_xml(nomFicXML, 'Memmapfile', Memmapfile);
    return
end


% Cas des anciens formats
a = cl_ermapper_ers('FicIn', fullfile(nomDir, [nomFicSeul extension]));
% imagesc(a)

% NullCellValue = get(a, 'NullCellValue');
nbRows = get(a, 'NrOfLines');
if nbRows == 0
    return
end

nbColumns  = get(a, 'NrOfCellsPerLine');
Xdimension = get(a, 'Xdimension');
UserInfo   = get(a, 'UserInfo');

InitialFileFormat = 'ErMapper';

NrOfBands = get(a, 'NrOfBands');
BandId = get(a, 'BandId');
if isempty(BandId)
    Unit = [];
else
    for k=1:NrOfBands
        Unit{k} = BandId(k).Units;%#ok
    end
end

XUnit = UserInfo.XUnit;
YUnit = UserInfo.YUnit;

if isempty(Unit)
    Unit{1} = 'Unknown';
end

if isempty(XUnit)
    XUnit = 'Unknown';
end

if isempty(YUnit)
    YUnit = 'Unknown';
end

CoordinateSpace = get(a,'CoordinateSpace');
if strcmpi(CoordinateSpace.Projection, 'GEODETIC')
    GeometryType = cl_image.indGeometryType('LatLong');
    if isempty(UserInfo.DataType)
        [flag, DataType] = cl_image.edit_DataType;
        if ~flag
            return
        end
    else
        DataType = UserInfo.DataType;
    end
    if DataType == cl_image.indDataType('Bathymetry')
        Unit{1}  = 'm';
    elseif DataType == cl_image.indDataType('Reflectivity')
        Unit{1}  = 'dB';
    else
        Unit{1}  = 'Unknown';
    end
    
    Xdimension = get(a, 'Xdimension');
    Ydimension = get(a, 'Ydimension');
    Eastings   = get(a, 'Eastings');
    Northings  = get(a, 'Northings');
    
    % CORRECTION DE BUG Ifremer_BMO_MNT et PHOTO
    if isempty(Eastings)
        Eastings = 0;
    end
    if isempty(Northings)
        Northings = 0;
    end
    % CORRECTION DE BUG Ifremer_BMO_MNT et PHOTO
    
    x = Eastings:Xdimension:(Eastings   + (nbColumns-1) * Xdimension);
    if isfield(CoordinateSpace, 'CoordinateType') && strcmpi(CoordinateSpace.CoordinateType, 'LL')
        y = Northings:Ydimension:(Northings + (nbRows-1) * Ydimension);
    else
        y = (Northings - (nbRows-1) * Ydimension):Ydimension:(Northings);
        y = fliplr(y);
    end
    
    TagSynchroX = 'Longitude';
    TagSynchroY = 'Latitude';
    XUnit = 'deg';
    YUnit = 'deg';
    
elseif ~isempty(UserInfo.GeometryType)  && (...
           (UserInfo.GeometryType == cl_image.indGeometryType('GeoYX')) ...
        || (UserInfo.GeometryType == cl_image.indGeometryType('LatLong')))
    GeometryType = UserInfo.GeometryType;
    
    if isempty(UserInfo.DataType)
        if memeReponses
            if isempty(persistent_DataType)
                [flag, DataType] = cl_image.edit_DataType;
                persistent_DataType = DataType;
            else
                DataType = persistent_DataType;
                flag = 1;
            end
        else
            [flag, DataType] = cl_image.edit_DataType;
            persistent_DataType = DataType;
        end
        if ~flag
            return
        end
    else
        DataType = UserInfo.DataType;
    end
    if DataType == cl_image.indDataType('Bathymetry')
        Unit{1} = 'm';
    elseif DataType == cl_image.indDataType('Reflectivity')
        Unit{1} = 'dB';
    else
        Unit{1} = 'Unknown';
    end
    
    Xdimension = get(a, 'Xdimension');
    Ydimension = get(a, 'Ydimension');
    Eastings   = get(a, 'Eastings');
    Northings  = get(a, 'Northings');
    
    if isempty(Eastings)
        Eastings = 0;
    end
    if isempty(Northings)
        Northings = 0;
    end
    
    if isempty(Xdimension)
        Xdimension = 1;
    end
    if isempty(Ydimension)
        Ydimension = 1;
    end
    
%     Northings = Northings - 10000000;
    x = Eastings:Xdimension:(Eastings + (nbColumns-1) * Xdimension);
    y = (Northings - (nbRows-1) * Ydimension):Ydimension:(Northings);
    y = fliplr(y);
    
    switch GeometryType
        case cl_image.indGeometryType('GeoYX') % M�trique
            TagSynchroX = num2str(rand(1));
            TagSynchroY = TagSynchroX;
            XUnit = 'm';
            YUnit = 'm';
        case cl_image.indGeometryType('LatLong') % G�ographique
            TagSynchroX = 'Longitude';
            TagSynchroY = 'Latitude';
            XUnit = 'deg';
            YUnit = 'deg';
        otherwise
            % TODO : message SVP
    end
    
elseif ~isempty(UserInfo.GeometryType) && (UserInfo.GeometryType == cl_image.indGeometryType('DepthAcrossDist'))
    GeometryType = UserInfo.GeometryType;
    
    if isempty(UserInfo.DataType)
        [flag, DataType] = cl_image.edit_DataType;
        if ~flag
            return
        end
    else
        DataType = UserInfo.DataType;
    end
    
    if DataType == cl_image.indDataType('Bathymetry')
        Unit{1}  = 'm';
    elseif DataType == cl_image.indDataType('Reflectivity')
        Unit{1}  = 'dB';
    else
        Unit{1}  = 'Unknown';
    end
    
    Xdimension = get(a, 'Xdimension');
    Ydimension = get(a, 'Ydimension');
    Eastings   = get(a, 'MetersX');
    Northings  = get(a, 'MetersY');
    
    x = Eastings:Xdimension:(Eastings   + (nbColumns-1) * Xdimension);
    y = (Northings - (nbRows-1) * Ydimension):Ydimension:(Northings);
    y = fliplr(y);
    
    TagSynchroX = num2str(rand(1));
    TagSynchroY = TagSynchroX;
    
else
    y = nbRows:-1:1;
    if isempty(UserInfo.TagSynchroX)
        TagSynchroX = num2str(rand(1));
    else
        TagSynchroX = UserInfo.TagSynchroX;
    end
    
    if isempty(UserInfo.TagSynchroY)
        TagSynchroY = num2str(rand(1));
    else
        TagSynchroY = UserInfo.TagSynchroY;
    end
    
    flagSigV = existSigV(nomDir, nomFicSeul);
    
    if flagSigV
        Eastings   = get(a, 'Eastings');
        Xdimension = get(a, 'Xdimension');
        x = Eastings:Xdimension:(Eastings   + (nbColumns-1) * Xdimension);
        
        if isempty(UserInfo.GeometryType)
            GeometryType = cl_image.indGeometryType('PingSamples');
        else
            GeometryType = UserInfo.GeometryType;
        end
        
        if isempty(UserInfo.DataType)
            DataType = cl_image.indDataType('Reflectivity');
        else
            DataType = UserInfo.DataType;
        end
    else
        x = (0:(nbColumns-1)) * Xdimension;
        if isempty(UserInfo.GeometryType)
            GeometryType = cl_image.indGeometryType('Unknown');
        else
            GeometryType = UserInfo.GeometryType;
        end
        
        if isempty(UserInfo.DataType)
            DataType = cl_image.indDataType('Reflectivity');
        else
            DataType = UserInfo.DataType;
        end
    end
end

if isfield(UserInfo, 'ImageType') && ~isempty(UserInfo.ImageType)
    ImageType = UserInfo.ImageType;
else
    ImageType = 1;
end

if isempty(UserInfo.ColormapIndex)
    switch DataType
        case cl_image.indDataType('Reflectivity')  % Reflectiviy� en Noir et blanc
            ColormapIndex = 2;
        otherwise % Colomap = jet dans tous les autres cas
            ColormapIndex = 3;
    end
else
    ColormapIndex = UserInfo.ColormapIndex;
end

%% Importation de l'image

[Image, NullCellValue] = import_ermapper_image(a, nomFic);
if length(Image) == 0 %#ok<ISMT>
    flag = 0;
    return
end

UserInfo = get(a, 'UserInfo');
if isempty(UserInfo) || isempty(UserInfo.YDir)
    YDir = 1;
else
    YDir = UserInfo.YDir;
    if isempty(YDir)
        YDir = 1;
    end
end

if (YDir == 1) && ((y(end)-y(1)) > 0)
    Image = my_flipud(Image);
end

BandId = get(a, 'BandId');
if (NrOfBands == 3) && ~isempty(BandId) && ...
        strcmp(BandId(1).Value, 'Red') && ...
        strcmp(BandId(2).Value, 'Green') && ...
        strcmp(BandId(3).Value, 'Blue')
    NrOfBands = 1;
    flagRGB   = 1;
    TitreInit = get(a, 'Name'); % DataFile ?
elseif (NrOfBands == 3) && ~isempty(BandId) && ...
        strcmp(BandId(1).Value, 'Blue') && ...
        strcmp(BandId(2).Value, 'Green') && ...
        strcmp(BandId(3).Value, 'Red')
    NrOfBands = 1;
    flagRGB   = 1;
    TitreInit = get(a, 'Name'); % DataFile ?
    for k=1:size(Image,1)
        pppp = Image(k,:,1);
        Image(k,:,1) = Image(k,:,3);
        Image(k,:,3) = pppp;
    end
    clear pppp
else
    flagRGB = 0;
    TitreInit = get(a, 'Name');
end

if isempty(subz)
    subz = 1:NrOfBands;
end
for k=1:length(subz)
    k2 = subz(k);
    if isempty(TitreInit)
        if ~flagRGB && (NrOfBands ~= 1)
            if isempty(BandId)
                [~, Titre] = fileparts(nomFic);
            else
                Titre = BandId(k2).Comment;
            end
        else
            [~, Titre] = fileparts(nomFic);
        end
    else
        Titre = TitreInit;
    end
    if flagRGB
        this(k) = cl_image;
        this(k) = set_Image(this(k), Image, 'PredefinedStats', {UserInfo.StatValues
            UserInfo.HistoCentralClasses
            UserInfo.HistoValues});
        ImageType = 2;
        
    elseif NrOfBands ==  1
        this(k) = cl_image;
        if isempty(UserInfo.StatValues.Moyenne)
            this(k) = set_Image(this(k), Image);
        else
            this(k) = set_Image(this(k), Image, 'PredefinedStats', {UserInfo.StatValues
                UserInfo.HistoCentralClasses
                UserInfo.HistoValues});
        end
        if isempty(ImageType)
            ImageType = 1;
        end
        
    else
        this(k) = cl_image;
        if isempty(UserInfo.StatValues.Moyenne)
            this(k) = set_Image(this(k), Image(:,:,k2));
        else
            this(k) = set_Image(this(k), Image(:,:,k2), 'PredefinedStats', {UserInfo.StatValues
                UserInfo.HistoCentralClasses
                UserInfo.HistoValues});
        end
        if isempty(ImageType)
            ImageType = 1;
        end
    end
    
    if ~isempty(UserInfo.ColormapCustom)
        this(k).ColormapCustom = UserInfo.ColormapCustom;
    end
    this(k).ColormapIndex      = ColormapIndex;
    this(k).DataType           = DataType;
    this(k).GeometryType       = GeometryType;
    this(k).x                  = x;
    this(k).y                  = y;
    this(k).TagSynchroX        = TagSynchroX;
    this(k).TagSynchroY        = TagSynchroY;
    this(k).Comments           = BandId(1).Comment;
    this(k).InitialFileName    = nomFic;
    this(k).InitialFileFormat  = InitialFileFormat;
    this(k).Name               =  Titre;
    this(k).Unit               = Unit{k2};
    this(k).XUnit              = XUnit;
    this(k).YUnit              = YUnit;
    this(k).YDir               = YDir;
    this(k).TagSynchroContrast = UserInfo.TagSynchroContrast;
    this(k).ImageType          = ImageType;
    this(k).CLim               = UserInfo.CLim;
    this(k).ValNaN             = NullCellValue;
    
    if ~isfield(UserInfo, 'NuIdentParent')
        UserInfo.NuIdentParent = floor(rand(1) * 1e8);
    end
    this(k).NuIdentParent = UserInfo.NuIdentParent;
    
    switch GeometryType
        case {cl_image.indGeometryType('GeoYX') ; cl_image.indGeometryType('LatLong')}
            Carto = ers2carto(a);
            this(k) = set(this(k),  'Carto', Carto);
            
            this(k) = correctionUTMSouth(this(k), Carto);
            
            if ~isempty(UserInfo.Sonar.Ident)
                SonarDescription = cl_sounder('Sonar.Ident', UserInfo.Sonar.Ident);
                this(k) = set(this(k), 'SonarDescription', SonarDescription);
            end
            
            TagSynchroXY = shortDescription(Carto);
            if ~isempty(TagSynchroXY)
                this(k).TagSynchroX = TagSynchroXY;
                this(k).TagSynchroY = TagSynchroXY;
            end
            
        case {cl_image.indGeometryType('PingSamples')
                cl_image.indGeometryType('PingRange')
                cl_image.indGeometryType('PingAcrossDist')
                cl_image.indGeometryType('PingBeam')
                cl_image.indGeometryType('PingSnippets')}
            this(k) = set(this(k), 'SonarRawDataResol', Xdimension, 'SonarResolutionD', Xdimension, 'SonarResolutionX',  Xdimension);
            H = zeros(1,nbRows);
            this(k) = set(this(k), 'SonarHeight', H);
            
            SonarDescription = cl_sounder('Sonar.Ident', UserInfo.Sonar.Ident);
            this(k) = set(this(k), 'SonarDescription', SonarDescription);
            
            
            % ----------------------
            % D�finition d'une carto
            
            Carto = ers2carto(a);
            this(k) = set(this(k),  'Carto', Carto);
    end
    
    % DEBUT RUSTINE POUR DONNEES NIWA REALISEES AVANT MODIF
    if ~isfield(UserInfo.Sonar, 'DiagEmi')
        UserInfo.Sonar.DiagEmi.etat = 1;
        UserInfo.Sonar.DiagEmi.origine = 1;
        UserInfo.Sonar.DiagEmi.ConstructTypeCompens = 1;
        UserInfo.Sonar.DiagEmi.IfremerTypeCompens = 1;
    end
    if ~isfield(UserInfo.Sonar, 'DiagRec')
        UserInfo.Sonar.DiagRec.etat = 2;
        UserInfo.Sonar.DiagRec.origine = 1;
        UserInfo.Sonar.DiagRec.ConstructTypeCompens = 1;
        UserInfo.Sonar.DiagRec.IfremerTypeCompens = 1;
    end
    if ~isfield(UserInfo.Sonar, 'BS')
        UserInfo.Sonar.BS.etat = 1;
        UserInfo.Sonar.BS.origine = 1;
        UserInfo.Sonar.BS.origineBelleImage = 1;
        UserInfo.Sonar.BS.origineFullCompens = 1;
        UserInfo.Sonar.BS.IfremerCompensTable = [];
    end
    % FIN RUSTINE NIWA
    % TODO : faire un this(k) = set_SonarStatus(this(k), UserInfo.Sonar) et g�n�raliser partout
    this(k) = set(this(k), 'Sonar_DefinitionENCours', ...
        'SonarTVG_etat',                    UserInfo.Sonar.TVG.etat, ...
        'SonarTVG_origine',                 UserInfo.Sonar.TVG.origine, ...
        'SonarTVG_ConstructTypeCompens',    UserInfo.Sonar.TVG.ConstructTypeCompens, ...
        'SonarTVG_ConstructAlpha',          UserInfo.Sonar.TVG.ConstructAlpha, ...
        'SonarTVG_ConstructTable',          UserInfo.Sonar.TVG.ConstructTable, ...
        'SonarTVG_ConstructConstante',      UserInfo.Sonar.TVG.ConstructConstante, ...
        'SonarTVG_ConstructCoefDiverg',     UserInfo.Sonar.TVG.ConstructCoefDiverg, ...
        'SonarTVG_IfremerAlpha',            UserInfo.Sonar.TVG.IfremerAlpha, ...
        'SonarTVG_IfremerCoefDiverg',       UserInfo.Sonar.TVG.IfremerCoefDiverg, ...
        'SonarTVG_IfremerConstante',        UserInfo.Sonar.TVG.IfremerConstante, ...
        'SonarDiagEmi_etat',                UserInfo.Sonar.DiagEmi.etat, ...
        'SonarDiagEmi_origine',             UserInfo.Sonar.DiagEmi.origine, ...
        'SonarDiagEmi_ConstructTypeCompens',UserInfo.Sonar.DiagEmi.ConstructTypeCompens, ...
        'SonarDiagEmi_IfremerTypeCompens',  UserInfo.Sonar.DiagEmi.IfremerTypeCompens, ...
        'SonarDiagRec_etat',                UserInfo.Sonar.DiagRec.etat, ...
        'SonarDiagRec_origine',             UserInfo.Sonar.DiagRec.origine, ...
        'SonarDiagRec_ConstructTypeCompens',UserInfo.Sonar.DiagRec.ConstructTypeCompens, ...
        'SonarDiagRec_IfremerTypeCompens',  UserInfo.Sonar.DiagRec.IfremerTypeCompens, ...
        'SonarAireInso_etat',               UserInfo.Sonar.AireInso.etat, ...
        'SonarAireInso_origine',            UserInfo.Sonar.AireInso.origine, ...
        'SonarBS_etat',                     UserInfo.Sonar.BS.etat, ...
        'SonarBS_origine',                  UserInfo.Sonar.BS.origine, ...
        'SonarBS_origineBelleImage',        UserInfo.Sonar.BS.origineBelleImage, ...
        'SonarBS_origineFullCompens',       UserInfo.Sonar.BS.origineFullCompens, ...
        'SonarBS_IfremerCompensTable',      UserInfo.Sonar.BS.IfremerCompensTable, ...
        'Sonar_NE_etat',                    UserInfo.Sonar.NE.etat, ...
        'Sonar_GT_etat',                    UserInfo.Sonar.GT.etat, ...
        'Sonar_SH_etat',                    UserInfo.Sonar.SH.etat);
    
    %% Importation du fichier des signaux sonar
    
    nomDirErs = fullfile(nomDir, nomFicSeul);
    if exist(nomDirErs, 'dir')
        nomFic_sigV = fullfile(nomDir, nomFicSeul, [nomFicSeul '_sigV']);
        if exist(nomFic_sigV, 'file')
            this(k) = import_ermapper_sigV(this(k), fullfile(nomDir, nomFicSeul), GeometryType);
        end
    end
    
    this(k) = update_Name(this(k));
    
    %% Importation des statistiques, R�gios d'int�r�t et Signatures texturales
    
    nomFicOthers = fullfile(nomDir, nomFicSeul, [nomFicSeul '_Others.mat']);
    if exist(nomFicOthers, 'file')
        this(k).CourbesStatistiques = loadmat(nomFicOthers, 'nomVar', 'CourbesStatistiques');
        this(k).RegionOfInterest    = loadmat(nomFicOthers, 'nomVar', 'RegionOfInterest');
        this(k).Texture             = loadmat(nomFicOthers, 'nomVar', 'Texture');
        this(k).Sonar.BathyCel      = loadmat(nomFicOthers, 'nomVar', 'BathyCel');
    end
end

flag = 1;


function flag = existSigV(nomDir, nomFicSeul)

nomFicImage = fullfile(nomDir, nomFicSeul);
if exist(nomFicImage, 'dir')
    nomDir = fullfile(nomDir, nomFicSeul);
end

nomFic_sigV = fullfile(nomDir, [nomFicSeul '_sigV']);
flag = exist(nomFic_sigV, 'file');
