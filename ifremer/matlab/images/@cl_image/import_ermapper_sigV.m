% Exportation dans un fichier ermapper
%
% Syntax
%   a = import_ermapper_sigV(a, nomFic, GeometryType)
%
% Input Arguments
%   a            : Instance de cl_image
%   nomFic       : Nom du fichier image ermapper
%   GeometryType : 
%
% Output Arguments
%   a : Instance de cl_image
%
% Examples
%   % Exemple d'image sonar
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_depth(aKM, 'ListeLayers', 1);
%   c = get_Image(b, 1);
%   imagesc(c)
%   nomFicOut = my_tempname
%   export_ermapper(c, nomFicOut)
%
%   [flag, d] = cl_image.import_ermapper(nomFicOut);
%   d = SonarScope(d);
%   delete(nomFicOut)
%
%
% See also import_ermapper_... Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [this, flag] = import_ermapper_sigV(this, nomFic, GeometryType)

flag = 1;

switch GeometryType
    case {  cl_image.indGeometryType('PingSamples')
            cl_image.indGeometryType('PingRange')
            cl_image.indGeometryType('PingAcrossDist')
            cl_image.indGeometryType('PingBeam')
            cl_image.indGeometryType('PingSnippets')}

        if exist(nomFic, 'dir')
            [nomDir, nomDirEr] = fileparts(nomFic);
            nomFic = fullfile(nomDir, nomDirEr, nomDirEr);
        end

        nomFic = [nomFic '_sigV'];
        fid = fopen(nomFic, 'r', 'ieee-le.l64');
        if fid == -1
            str = sprintf('Importation failure file %s', nomFic);
            my_warndlg(str, 1);
            flag = 0;
            return
        end

        VersionNumber = fread(fid, 1, 'double');
        switch VersionNumber
            case 20131211
                X = fread(fid, [29 Inf], 'double');
            case 20061218
                X = fread(fid, [26 Inf], 'double');
            otherwise
                fseek(fid, 0, 'bof');
                X = fread(fid, [25 Inf], 'double');
        end
        
        fclose(fid);
        X = X';

        if isnan(X(1, 3)) % cas o� le numero de ping est present
            if X(2, 2) > X(1, 2)
                X = flipud(X);
            end
        else % On teste l'heure : Attention danger dans le cas o� changement de date entre le premier ping et le deuxieme
            if X(2, 3) > X(1, 3)
                X = flipud(X);
            end
        end

        % for i=1:20
        %     if all(X(:, i))
        %         X(:, i) = NaN;
        %     end
        % end

        Heure = X(:, 2);
        if any(Heure ~= 0) && ~all(isnan(Heure))
            % Ancienne mouture
            Date  = X(:, 1);
            t = cl_time('timeIfr', Date, Heure);
            this = set_SonarTime(this, t);
            X(:, 1) = t.timeMat;
            X(:, 2) = 0;
        else
            % Nouvelle mouture
            t = cl_time('timeMat', X(:, 1));
        end
        this = set_SonarTime(this, t);

        this = set_SonarPingCounter(this,   X(:, 3));
        this = set_SonarImmersion(this,     X(:, 4));
        this = set_SonarHeight(this,        X(:, 5));

        if any(X(:, 6) ~= 0) % D�comment� le 16/06/2010 (suppression des flag)
            this = set_SonarCableOut(this,  X(:, 6));
        end
        if any(X(:, 7) ~= 0)
            this = set_SonarSpeed(this,     X(:, 7));
        end
        this = set_SonarHeading(this,       X(:, 8));
        this = set_SonarRoll(this,          X(:, 9));
        this = set_SonarPitch(this,         X(:, 10));

        if any(X(:, 11) ~= 0)
            this = set_SonarYaw(this, X(:, 11));
        end
        this = set_SonarSurfaceSoundSpeed(this, X(:, 12));
        this = set_SonarFishLatitude(this,      X(:, 13));
        this = set_SonarFishLongitude(this,     X(:, 14));

        if any(X(:, 15) ~= 0)
            this = set_SonarShipLatitude(this, X(:, 15));
            this = set_SonarShipLongitude(this, X(:, 16));
        end
        this = set_SonarPortMode_1(this,        X(:, 17));
        
        if any(X(:, 18) ~= 0) % Rajout� le 16/06/2010 (suppression des flag)
            this = set_SonarPortMode_2(this,    X(:, 18));
        end
        
        if any(X(:, 19) ~= 0)
            this = set_SonarStarMode_1(this,    X(:, 19));
        end
        if any(X(:, 20) ~= 0)
            this = set_SonarStarMode_2(this,    X(:, 20));
        end
        this = set_SonarHeave(this,             X(:, 21));
        
        if any(X(:, 22) ~= 0)
            this = set_SonarTide(this,          X(:, 22));
        end
        
        this = set_SonarPingCounter(this,       X(:, 23));

        this = set_SonarSampleFrequency(this,   X(:, 24));
        if any(X(:, 25) ~= 0)
            this = set_SonarFrequency(this,     X(:, 25));
        end
        
        if VersionNumber >= 20061218
            if any(X(:, 26) ~= 0)
                this = set_SonarInterlacing(this, X(:, 26));
            end
        end
end

% Pourquoi SonarPingCounter est defini 2 fois ??? (colonnes 3 et 23)
