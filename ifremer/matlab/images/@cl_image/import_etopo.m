% Lecture de donn�es ETOPO
%
% Syntax
%   [flag, a] = import_etopo(a, ...)
%
% Input Arguments
%   flag : 1=OK, 0=KO
%   a : Instance vide de cl_image
%
% Name-Value Pair Arguments
%   LonLim : Limites en longitude
%   LatLim : Limites en latitude
%
% Output Arguments
%   [] : Auto-plot activation
%   a  : Instance cl_image
%
% Remarks : Remarques particulieres concernant la fonction
%
% Examples
%   [flag, a] = import_etopo(cl_image, 'LonLim', [-30 80], 'LatLim', [15 60]);
%   SonarScope(a)
%
% See also Terre Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, this] = import_etopo(~, varargin)

I0 = cl_image_I0;

[varargin, LatLim] = getPropertyValue(varargin, 'LatLim', []);
[varargin, LonLim] = getPropertyValue(varargin, 'LonLim', []);
[varargin, Max]    = getPropertyValue(varargin, 'Max', Inf); %#ok<ASGLU>

nomFic = getNomFicDatabase('Earth_LatLong_Bathymetry.ers');

[flag, this] = cl_image.import_ermapper(nomFic);
if ~flag
    return
end

X1 = min(LatLim);
X2 = max(LatLim);
LatLim = [X1 X2];

X1 = min(LonLim);
X2 = max(LonLim);
LonLim = [X1 X2];

this = extraction(this, 'XLim', LonLim, 'YLim', LatLim);

lonOut = get(this, 'x');
nx = length(lonOut);
if nx > Max
    stepx = nx / Max;
else
    stepx = 1;
end
latOut = get(this, 'y');
ny = length(latOut);
if ny > Max
    stepy = ny / Max;
else
    stepy = 1;
end
if (stepx ~= 1) || (stepy ~= 1)
    this = downsize(this, 'pasx', stepx, 'pasy', stepy);
end
