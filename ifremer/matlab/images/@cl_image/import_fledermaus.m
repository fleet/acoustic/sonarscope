% Importation d'un fichier Fledermaus (.sd)
%
% Syntax
%   [flag, b] = import_fledermaus(a, nomFic)
%
% Input Arguments
%   a      : Instance quelconque de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1=succ�s, 0=Echec
%   b    : Instance2 de cl_image contenant la bathymetrie et le drappage
%
% Remarks : Seul un format particulier est lisible
%
% Examples
%   nomFic = getNomFicDatabase('AXEL2.sd')
%   nomFic = getNomFicDatabase('KWINTEBANK.sd')
%   [flag, a] = import_fledermaus(cl_image, nomFic);
%   SonarScope(a)
%
%   a(2) = sunShading(a(1));
%   imagesc(a(2))
%
%   nomFicOut = my_tempname('.sd')
%   flag = export_fledermaus(a, nomFicOut)
% 
%   [flag, b] = import_fledermaus(cl_image, nomFicOut);
%   imagesc(b)
%
% See also export_fledermaus Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this, positionImages] = import_fledermaus(this, nomFic)

positionImages = [];

%% V�rification si le fichier est bien au format Fledermaus 

flag = is_FLEDERMAUS(nomFic);
if ~flag
    str1 = sprintf('Le fichier "%s" n''est pas au format Fledermaus', nomFic);
    str2 = sprintf('File "%s" in not a Fledermaus file', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

% my_warndlg('La fonction import_fledermaus ne fonctionne actuellement qu''avec le fichier KWINTEBANK.sd', 1);

machineFormat = get_machineFormat(nomFic);
fid = fopen(nomFic, 'r', machineFormat);
Header = read_header(fid);%#ok

% Taille de Kuintebank 203098154

% pos1 = ftell(fid);
TagID      = fread(fid, 1, 'int');%#ok
Length     = fread(fid, 1, 'int');
FilePTR    = fread(fid, 1, 'int');%#ok
DataFormat = fread(fid, 1, 'int');%#ok

X = fread(fid, 5+Length, 'uint16');%#ok

for k=1:min(Length,2)
    positionBlock(k) = ftell(fid);%#ok
    
    fseek(fid, positionBlock(k), 'bof');
    X = fread(fid, 1, 'int32');%#ok
    X = fread(fid, 1, 'int32');%#ok
    nbBytesLayer(k) = fread(fid, 1, 'int32');%#ok
    
    %     n = 11;
    % %     n = 5; % Pour small deuxieme passe
    % if n > 0
    %         X  = fread(fid, n, 'int16');
    %     X'
    % end
    n = 22;
    %     n = 10; % Pour small deuxieme passe
    if n > 0
        X  = fread(fid, n, 'uint8');%#ok
    end
    
    Version = fread(fid, 1, 'uint16'); %#ok % Toujours 0
    nbDim   = fread(fid, 1, 'uint16');   % Toujours 2

    for k2=1:nbDim
        Dim(k2) = fread(fid, 1, 'uint16'); %#ok  %TNombre de lignes et nombre de colonnes
    end
    DataType = fread(fid, 1, 'uint16'); % 3 pour de la bathy, 7 pour le drappage
    BitWidth = fread(fid, 1, 'uint16'); % 16 pour la bathy, 32 pour le drappage (24+8)
    ZMin     = fread(fid, 1, 'double'); % Valeur en metres pour la bathy, 0 pour le drappage
    ZMax     = fread(fid, 1, 'double'); % Valeur en metres pour la bathy, 0 pour le drappage

    if nbDim ~= 2
        return
    end

    nbRows     = Dim(1);
    nbColumns = Dim(2);
    % nbColumns = 1000

    [flag, Fmt] = get_fmt(DataType, BitWidth);
    if ~flag
        return
    end

    positionImages(k) = ftell(fid);%#ok
    
    %{
    for k3=0:256
        k3
        fseek(fid, positionBlock(k), 'bof');
        Z = fread(fid, [nbRows+k3 nbColumns], Fmt);
        figure(1294); imagesc(Z); colormap(jet(256));
        title(num2str(k3))
        drawnow
        pause
    end
    %}
    
    Z = fread(fid, [nbRows nbColumns], Fmt);
%     Z = flipud(Z);
    
    if DataType == 7
%         Z = Z(1:5:end,1:5:end);

        X = zeros(size(Z), 'uint32');
        X(:)= hex2dec('FF');
        X = bitand(Z, X);
        RGB = uint8(X);

        X(:)= hex2dec('FF00');
        X = bitand(Z, X);
        X = bitshift(X, -8);
        RGB(:,:,2) = X;

        X(:)= hex2dec('FF0000');
        X = bitand(Z, X);
        X = bitshift(X, -16);
        RGB(:,:,3) = X;
        
        Z = RGB;
        clear RGB
        
        ImageType = 2;
        
    elseif DataType == 12
        ImageType = 1;    

    else
        Z = single(Z);
        Z(Z == 0) = NaN;
%         Z(Z == 1) = NaN;
        
        a = (ZMax - ZMin) / double(intmax(Fmt(2:end)));
        b = ZMin;
        
        Z = a * Z + b;
        %     figure; imagesc(Z); colorbar
        
        ImageType = 1;   
    end

    % x = 1:nbColumns;
    % y = 1:nbRows;

    x = 1:size(Z,1);
    y = 1:size(Z,2);
    [~, fileName] = fileparts(nomFic);

    switch k
        case 1
            Name = [fileName '-Elevation'];
        case 2
            Name = [fileName '-Mapping'];
        otherwise
            Name = sprintf('%s-Layer %d', fileName, k);
    end
    this(k) = cl_image('Image', my_transpose(Z), ...
        'Name',            Name, ...
        'x',               x, ...
        'y',               y, ...
        'XUnit',           'm', ...
        'YUnit',           'm', ...
        'ColormapIndex',   3, ...
        'ImageType',       ImageType, ...
        'DataType',        cl_image.indDataType('Bathymetry'), ...
        'GeometryType',    cl_image.indGeometryType('LatLong'), ...
        'InitialFileName', nomFic, 'InitialFileFormat', 'FLEDERMAUS');
end
pos4 = ftell(fid);   % 203097982
fseek(fid, pos4, 'bof');

if DataType == 12
    fseek(fid, pos4+34, 'bof');
    XLim(1) = fread(fid, 1, 'double');
    XLim(2) = fread(fid, 1, 'double');
    YLim(1) = fread(fid, 1, 'double');
    YLim(2) = fread(fid, 1, 'double');
    ZLim(1) = fread(fid, 1, 'double');
    ZLim(2) = fread(fid, 1, 'double');%#ok
else
    for k=1:Length
        X = fread(fid, 1, 'uint8'); %#ok% 54
        X = fread(fid, 1, 'uint8'); %#ok% 22
        X = fread(fid, 1, 'uint8'); %#ok% 73
        X = fread(fid, 1, 'uint8'); %#ok% 255
    end
    
    X = fread(fid, 1, 'uint16'); %#ok % 15000
    X = fread(fid, 42, 'uint8'); %#ok % 0 0 100 0 0 0 0 0 1 1 1 1  ET DES Z�ROS
    
    XLim(1) = fread(fid, 1, 'double');
    XLim(2) = fread(fid, 1, 'double');
    YLim(1) = fread(fid, 1, 'double');
    YLim(2) = fread(fid, 1, 'double');
    ZLim(1) = fread(fid, 1, 'double');
    ZLim(2) = fread(fid, 1, 'double');%#ok
    
    pos5 = ftell(fid);
    fseek(fid, pos5, 'bof');
    X = fread(fid, 11, 'uint32');%#ok   % 10 z�ros et 999999999
    X = fread(fid, 28, 'uint8'); %#ok % [000000 1 1 1 et des 0]
    
    % pos6 = ftell(fid);   % 203098154
    fseek(fid, 0, 'eof');
end

fclose(fid);
flag = 1;

x = linspace(XLim(1), XLim(2), length(x));
y = linspace(YLim(1), YLim(2), length(y));
TagSynchro = num2str(rand(1));
for k=1:length(this)
    this(k) = set(this(k), 'x', x, 'y', y, ...
        'TagSynchroX', TagSynchro, 'TagSynchroY', TagSynchro);
end


function [flag, Fmt] = get_fmt(DataType, BitWidth)

flag = 0;
Fmt = [];

if (DataType == 3) && (BitWidth == 16)
    Fmt = '*uint16';
    flag = 1;
    return
end

if (DataType == 7) && (BitWidth == 32)
    Fmt = '*uint32';
    flag = 1;
    return
end

if (DataType == 12) && (BitWidth == 8)
    Fmt = '*uint32';
    flag = 1;
    return
end


function Header = read_header(fid)

flag = 1;
while flag
    line = fgets(fid);
    Header = line;

    % % ----------------------------------
    % Faire un test ici si Binary ou ASCII
    % sur la premiere ligne (%% TDR 2.0 Binary)
    % ------------------------------------

    if strcmp(line(1:2), '%%') && double(line(3)) == 12
        flag = 0;
    end
end


function machineFormat = get_machineFormat(nomFic)

fid = fopen(nomFic, 'r', 'l');
Header = read_header(fid);%#ok
TagID_l      = fread(fid, 1, 'int');%#ok
Length_l     = fread(fid, 1, 'int');%#ok
FilePTR_l    = fread(fid, 1, 'int');%#ok
DataFormat_l = fread(fid, 1, 'int');
fclose(fid);

fid = fopen(nomFic, 'r', 'b');
Header = read_header(fid);%#ok
TagID_b      = fread(fid, 1, 'int');%#ok
Length_b     = fread(fid, 1, 'int');%#ok
FilePTR_b    = fread(fid, 1, 'int');%#ok
DataFormat_b = fread(fid, 1, 'int');%#ok
fclose(fid);

% if Length_l < Length_b
switch DataFormat_l
    case 257
        machineFormat = 'l';
    case 513
        machineFormat = 'l';
    otherwise
        machineFormat = 'b';
end
