% Importation d'un fichier G�oTiff
%
% Syntax
%    [flag, a] = import_geotiff(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%
% See also cl_image/import_* Authors
% Authors : GLT
%--------------------------------------------------------------------------

function [flag, a] = import_geotiff(~, nomFic, varargin)

[varargin, flagAuto] = getPropertyValue(varargin, 'flagAuto', 0);
[varargin, flagMute] = getPropertyValue(varargin, 'Mute',     0);
[varargin, DataType] = getPropertyValue(varargin, 'DataType', []);
[varargin, Unit]     = getPropertyValue(varargin, 'Unit',     []); %#ok<ASGLU>

a = cl_image.empty;

if exist(nomFic, 'file')
    flag = 1;
else
    flag = 0;
    return
end

info  = geotiffinfo(nomFic);
infoG = imfinfo(nomFic); % Infos graphiques.

if ~flagMute
    disp(info)
    disp(infoG)
end

TagSynchroXY = [];

if strcmp(info.ModelType, 'ModelTypeProjected')
    GeometryType = cl_image.indGeometryType('GeoYX');
elseif strcmp(info.ModelType, 'ModelTypeGeographic')
    GeometryType = cl_image.indGeometryType('LatLong');
elseif strcmpi(info.ModelType, 'unknown')
    GeometryType = 1;
else
    GeometryType = 1;
end

[I, map] = imread(nomFic);
% [I, map]= geotiffread(nomFic);
if (ndims(I) == 3) && (size(I,3) > 3)
    I = I(:,:,1:3);
end

%% R�cup�ration info GDAL_NODATA via gdalinfo.exe
% R�cup�ration du chemin de l'�x�cutable

nomExe = 'gdalinfo.exe';
pathGDAL = getenv('GDAL_HOME');
if ~exist(fullfile(pathGDAL, nomExe), 'file')
    str1 = sprintf('Le fichier "%s" n''a pas �t� trouv� sur votre ordinateur, if vous faut r�cup�rer le dernier "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" qui vous installera ce composant.', nomExe);
    str2 = sprintf('File "%s" not found, you should download and install the last "SetUpSonarScope_yyyymmdd_External_Winxx_Rxxxxx.exe" that will install this component.', nomExe);
    my_warndlg(Lang(str1,str2), 1);
end

% pause(1);
cmd = sprintf('"%s" "%s"',...
    fullfile(pathGDAL, nomExe), nomFic);
[status, result] = system(cmd, '-echo');
if status
    str1 = sprintf('Echec de gdalInfo : %s\n%s', nomFic, result);
    str2 = sprintf('gdalInfo has failed for "%s"\n%s', nomFic, result);
    my_warndlg(Lang(str1,str2), 0);
else
    [~, fin] = regexp(result, 'NoData Value=');
    if ~isempty(fin)
%         for k=1:length(fin) % cas RGB : 3 fois NoData Value=
        for k=1:min(length(fin),3) % Modif JMA le 18/06/2024 pour fichiers Ridha de ULIX
            NoData = textscan(result(fin(k)+1:end), '%s[^\n]' );
            NoDataVal(k) = str2double( NoData{1});   %#ok<AGROW>
            if abs(NoDataVal(k)) > 1e38
                IsNoData(:,:,k) =  abs(I(:,:,k)) > 1e38; %#ok<AGROW>
                %NoDataVal(k) = sign(NoDataVal(k))*340282306073709650000000000000000000000; % Pour les tifs export�s depuis ArcGis
            else
                IsNoData(:,:,k) = (I(:,:,k) == NoDataVal(k)); %#ok<AGROW>
            end
        end
        I(IsNoData) = NaN;
        
    elseif strcmp(infoG.PhotometricInterpretation, 'RGB')
        % Le noir est vu comme NaN
        IR = I(:,:,1);
        IG = I(:,:,2);
        IB = I(:,:,3);
        subZero = (IR == 0 & IG == 0 & IB == 0);
        IR(subZero) = NaN;
        IG(subZero) = NaN;
        IB(subZero) = NaN;
        I(:,:,1) = IR;
        I(:,:,2) = IG;
        I(:,:,3) = IB;
    end
end
%%



% if size(map,1) > 256
%     ImageType = 2;
%
%     X = zeros(size(I), 'uint32');
%     X(:)= hex2dec('FF');
%     X = bitand(I, X);
%     RGB = uint8(X);
%
%     X(:)= hex2dec('FF00');
%     X = bitand(Z, X);
%     X = bitshift(X, -8);
%     RGB(:,:,2) = X;
%
%     X(:)= hex2dec('FF0000');
%     X = bitand(Z, X);
%     X = bitshift(X, -16);
%     RGB(:,:,3) = X;
%
% else

switch lower(info.ColorType)
    case 'indexed'
        ImageType = 3;
        ColormapIndex = 1;
    case 'truecolor'
        ImageType = 2;
        ColormapIndex = 1;
    case 'grayscale'
        ImageType = 1;
        ColormapIndex = 3; %jet
    otherwise
        ImageType = 1;
        % 'A COMPLETER'
end

% if size(I,3) == 3
%     ImageType = 2;
% else
%     if isempty(map)
%         ImageType = 1;
%     else
%         ImageType = 3;
%     end
% end
% end

Nx = info.Width;
Ny = info.Height;
if isempty(info.PixelScale)
    GeometryType = 1;
    x = 1:Nx;
    y = 1:Ny;
else
    dx = info.PixelScale(1);
    dy = info.PixelScale(2);
    x = linspace(info.TiePoints.WorldPoints.X, info.TiePoints.WorldPoints.X + (Nx-1) * dx, Nx);
    
    %% Ajout JMA le 26/02/2019 pour fichier geotiff Vanuato SO203_Woodlark-Segment3_surface-static3x3_50m_UTM56S.tif
    if ~isempty(info.ProjParm)
        ind = find(strcmp(info.ProjParmId, 'ProjFalseNorthingGeoKey')); %#ok<EFIND>
        if ~isempty(ind)
            info.TiePoints.WorldPoints.Y = info.TiePoints.WorldPoints.Y - info.ProjParm(7);
        end
    end
    
    y = linspace(info.TiePoints.WorldPoints.Y, info.TiePoints.WorldPoints.Y - (Ny-1) * dy, Ny);
end

if ~isempty(info.CornerCoords)
    %     info.CornerCoords.X
    %     info.CornerCoords.Y
    %     info.CornerCoords.Row
    %     info.CornerCoords.Col
    %     info.CornerCoords.Lat
    %     info.CornerCoords.Lon
    %     input_points = [info.CornerCoords.Col' info.CornerCoords.Row']
    %     input_points = [info.CornerCoords.Col' info.CornerCoords.Row']
    %{
input_points = [info.CornerCoords.X' info.CornerCoords.Y']
base_points  = [info.CornerCoords.Lon' info.CornerCoords.Lat']
tform = cp2tform(input_points, base_points, 'projective');
registered = imtransform(single(I(:,:,1)), tform, 'FillValues', NaN);

N = length(info.CornerCoords.X)
XP = info.CornerCoords.Lon
X  = [ones(1, N) ; info.CornerCoords.Col  ; info.CornerCoords.Row]
Y  = [ones(1, N) ; info.CornerCoords.Col  ; info.CornerCoords.Row]
MX = XP / X
YP = info.CornerCoords.Lat
MY = YP / Y

M = [MX ; MY]
C = M * [1 ; info.CornerCoords.Col(1) ; info.CornerCoords.Row(1)]

[x, y] = meshgrid(0:Nx-1, (0:Ny-1));
xp = MX * [ones(1,Nx*Ny) ; x(:)' ; y(:)'];
yp = MY * [ones(1,Nx*Ny) ; x(:)' ; y(:)'];


u = [ 0   6  -2]';
v = [ 0   3   5]';
x = [-1   0   4]';
y = [-1 -10   4]';
tform = maketform('affine',[u v],[x y]);

Validate the mapping by applying tformfwd. Results should equal [x, y]

[xm, ym] = tformfwd(tform, u, v)
    %}
end

if strcmpi(info.ColorType, 'truecolor')
    nbLayers = size(I,3);
    [~, ImageName] = fileparts(nomFic);
    a = cl_image('Image', I(:,:,:), 'Name', ImageName, 'GeometryType', GeometryType, ...
        'x', x, 'y', y, ...
        'ImageType', ImageType, 'ColormapIndex', ColormapIndex, ...
        'InitialFileName', nomFic, 'InitialFileFormat', 'IMAGE');
    if isfield(info, 'ImageDescription')
        a = set(a, 'Comments', info.ImageDescription);
    end
else
    nbLayers = size(I,3);
    for kImage=nbLayers:-1:1
        [~, ImageName] = fileparts(nomFic);
        a(kImage) = cl_image('Image', I(:,:,kImage), 'Name', ImageName, 'GeometryType', GeometryType, ...
            'x', x, 'y', y, ...
            'ImageType', ImageType, 'ColormapIndex', ColormapIndex, ...
            'InitialFileName', nomFic, 'InitialFileFormat', 'IMAGE');

        if isfield(info, 'ImageDescription')
            a(kImage) = set(a(kImage), 'Comments', info.ImageDescription);
        end

        if ~isempty(map)
            a(kImage).ColormapIndex  = 1;
            a(kImage).ColormapCustom = map;
        end
    end
end


% Conversion des params Geotiff en param�tres SSC pour configuration de la classe Carto.

if isfield(infoG, 'GeoAsciiParamsTag') && contains(infoG.GeoAsciiParamsTag, 'NZGD2000')
%     strProjUTM = 'NZGD2000 / New Zealand Transverse Mercator';
%     [flag, structEPSG, ~] = getParamsProjFromESRIFile('CsLabelName', strProjUTM);
    Carto = cl_carto('CodeEPSG', '2193');
else
    Carto = convertGeotiffParams2SSCParams(cl_carto([]), info);
end
for kImage=1:length(a)
    a(kImage) = set(a(kImage), 'Carto', Carto);
end

[~, indGeometryType, indexDataType] = cl_image.uncode_ImageName(ImageName);
if isempty(GeometryType)
    if indGeometryType == 1
        [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', indGeometryType, 'Title', ImageName);
        if ~flag
            return
        end
    else
        GeometryType = indGeometryType;
    end
end

if isempty(DataType)
    % Modif JMA le 28/10/2019
%     if flagAuto && (indexDataType ~= 1)
%         DataType = indexDataType;
%     else
%         [flag, DataType] = cl_image.edit_DataType('DataType', indexDataType, 'Title', ImageName);
%         if ~flag
%             return
%         end
%     end
    if indexDataType == 1
        [flag, DataType] = cl_image.edit_DataType('DataType', indexDataType, 'Title', ImageName);
        if ~flag
            return
        end
    else
        DataType = indexDataType;
    end
end

if isempty(Unit)
    Unit = cl_image.expectedUnitOfData(DataType);
end
    
switch  GeometryType
    case cl_image.indGeometryType('LatLong')
        TagSynchroX = 'Longitude';
        TagSynchroY = 'Latitude';
        XYunit = 'deg';
    case cl_image.indGeometryType('GeoYX')
        TagSynchroXY = shortDescription(Carto);
        if isempty(TagSynchroXY)
            TagSynchroX = 'GeoX';
            TagSynchroY = 'GeoY';
        else
            TagSynchroX = TagSynchroXY;
            TagSynchroY = TagSynchroXY;
        end
        XYunit = 'm';
    otherwise
        if isempty(TagSynchroXY)
            TagSynchroXY = num2str(rand(1));
        end
        TagSynchroX = TagSynchroXY;
        TagSynchroY = TagSynchroXY;
        XYunit = [];
end

for kImage=1:length(a)
    a(kImage) = set(a(kImage),...
        'Unit',         Unit, ...
        'TagSynchroX',  TagSynchroX,...
        'TagSynchroY',  TagSynchroY,...
        'XUnit',        XYunit,...
        'Yunit',        XYunit, ...
        'DataType',     DataType, ...
        'GeometryType', GeometryType);
end

