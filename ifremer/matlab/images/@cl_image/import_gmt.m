% Importation d'un fichier GMT (*.grd) soit au format CF ou NF.
% cf. http://gmt.soest.hawaii.edu/gmt/html/man/grdreformat.html
%
% Syntax
%   [a, flag] = import_gmt(b, nomFic)
%
% Input Arguments
%   b      : Instance vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('Aslanian.grd')
%   [a, flag] = import_gmt(cl_image, nomFic);
%   imagesc(a)
%
% See also is_GMT Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_gmt(~, nomFic, varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []); %#ok<ASGLU>

this = cl_image.empty;

if isempty(DataType)
    [flag, DataType] = cl_image.edit_DataType;
    if ~flag
        return
    end
end

a = cl_netcdf('fileName', nomFic);
sk = get(a, 'skeleton');

%% Test si c'est un fichier GMT au format (cf. grdreformat).

numVar = find_numVar(a, 'spacing');
if isempty(numVar)
    [this, flag] = nested_import_GMT_NF(DataType, nomFic, a);
    return
end

%% Test si c'est un fichier GMT

numAtt1 = find_numAtt(a, 'title');
numAtt2 = find_numAtt(a, 'source');
numVar1 = find_numVar(a, 'x_range');
numVar2 = find_numVar(a, 'y_range');
numVar3 = find_numVar(a, 'spacing');
numVar4 = find_numVar(a, 'dimension');
if isempty(numAtt1) ||  isempty(numAtt2) || isempty(numVar1) || isempty(numVar2) || isempty(numVar3) || isempty(numVar4)
    flag = 0;
    str = sprintf('Le fichier %s ne semble pas etre un fichier GMT', nomFic);
    my_warndlg(str, 1);
    return
end

x_range = get_value(a, 'x_range');
y_range = get_value(a, 'y_range');
% spacing   = get_value(a, 'spacing');
dimension = get_value(a, 'dimension');
z = get_value(a, numVar4+1);
Name = sk.var(numVar4+1).name;

z = reshape(z, dimension);

x_range = double(x_range);
y_range = double(y_range);
dimension = double(dimension);
x = linspace(x_range(1), x_range(2), dimension(1));
y = linspace(y_range(1), y_range(2), dimension(2));

% figure; imagesc(x, y, flipud(z'));

this = cl_image('Image', flipud(z'), ...
    'Name',                Name, ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'deg', ...
    'YUnit',               'deg', ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         cl_image.indGeometryType('LatLong'), ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'GMT');
Carto = cl_carto;
this = set_Carto(this, Carto);

flag = 1;


function [this, flag] = nested_import_GMT_NF(dataType, nomFic, a)

numAtt1 = find_numAtt(a, 'title');
numAtt2 = find_numAtt(a, 'source');
numVar1 = find_numVar(a, 'x');
numVar2 = find_numVar(a, 'y');
numVar3 = find_numVar(a, 'z');
if isempty(numAtt1) ||  isempty(numAtt2) || isempty(numVar1) || isempty(numVar2) || isempty(numVar3)
    flag = 0;
    this = [];
    str = sprintf('Le fichier %s ne semble pas etre un fichier GMT', nomFic);
    my_warndlg(str, 1);
    return
end

x = get_value(a, numVar1);
x = double(x);
y = get_value(a, numVar2);
y = double(y);
z = get_value(a, numVar3);
Name = 'Layer Z';

% figure; imagesc(x, y, flipud(z'));

this = cl_image('Image', z, ...
    'Name',            Name, ...
    'x',               x, ...
    'y',               y', ...
    'XUnit',           'deg', ...
    'YUnit',           'deg', ...
    'ColormapIndex',   3, ...
    'DataType',        dataType, ...
    'GeometryType',    cl_image.indGeometryType('LatLong'), ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'GMT');

Carto = cl_carto;
this = set_Carto(this, Carto);

flag = 1;

