% Importation d'un fichier GMT (*.grd)
%
% Syntax
%   [a, flag] = import_gmt_Gebco(b, nomFic)
%
% Input Arguments
%   b      : Instance vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('????')
%   [a, flag] = import_gmt_Gebco(cl_image, nomFic);
%   imagesc(a)
%
% See also is_GMT Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag, Carto] = import_gmt_Gebco(~, nomFic, varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []);
[varargin, Carto]    = getPropertyValue(varargin, 'Carto',    []); %#ok<ASGLU>

this = cl_image.empty;

if isempty(DataType)
    [flag, DataType] = cl_image.edit_DataType;
    if ~flag
        return
    end
end

a = cl_netcdf('fileName', nomFic);

%% Test si c'est un fichier GMT

numAtt1 = find_numAtt(a, 'title');
numAtt2 = find_numAtt(a, 'Conventions');
numAtt3 = find_numAtt(a, 'GMT_version');
%     numAtt4 = find_numAtt(a, 'node_offset');
numDim1 = find_numDim(a, 'x');
numDim2 = find_numDim(a, 'y');
numVar1 = find_numVar(a, 'x');
numVar2 = find_numVar(a, 'y');
numVar3 = find_numVar(a, 'z');
if isempty(numAtt1) || isempty(numAtt2) || isempty(numAtt3) || ...
        isempty(numVar1) || isempty(numVar2) || isempty(numVar3) && ...
        ~isempty(numDim1) && ~isempty(numDim2)
    flag = 0;
    str = sprintf(Lang('Le fichier %s ne semble pas etre un fichier GMT', ...
        'This file seems not to be a GMT one.'), nomFic);
    my_warndlg(str, 1);
    return
end

x = get_value(a, 'x');
y = get_value(a, 'y');
z = get_value(a, 'z');

xmin = min(x);
xmax = max(x);
ymin = min(y);
ymax = max(y);
if (xmin > -180) && (xmax < 360) && (ymin > -90) && (ymax < 90)
    TagSynchroX  = 'Longitude';
    TagSynchroY  = 'Latitude';
    GeometryType = cl_image.indGeometryType('LatLong');
else
    TagSynchroX  = num2str(floor(randi(1e6,1)));
    TagSynchroY  = num2str(floor(randi(1e6,1)));
    GeometryType = cl_image.indGeometryType('GeoYX');
end

%% Carto

if isempty(Carto)
    nomDir = fileparts(nomFic);
    
    [flagExistenceCarto, Carto, nomFicCarto] = test_ExistenceCarto_Folder(nomDir);
    if flagExistenceCarto
        str1 = sprintf('Le fichier "%s" est interprété directement', nomFicCarto);
        str2 = sprintf('File "%s" is used by default.', nomFicCarto);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
    else
        if GeometryType == cl_image.indGeometryType('LatLong')
            [flag, Carto] = editParams(cl_carto([]), mean([ymin ymax]), mean([xmin xmax]), 'MsgInit');
        else
            [flag, Carto] = editParams(cl_carto([]), [], [], 'MsgInit');
        end
        if ~flag
            return
        end
        
        [rep, flag] = my_questdlg(Lang('Voulez-vous sauver les paramètres cartographiques dans un fichier ?','Save the cartographic parameters in a file ?'));
        if ~flag
            return
        end
        if rep == 1
            [flag, nomFicCarto] = my_uiputfile('*.def', Lang('Nom du fichier de définition de la carto', 'Give a file name'), ...
                fullfile(nomDir, 'Carto.def'));
            if ~flag
                return
            end
            flag = export(Carto, nomFicCarto);
            if ~flag
                return
            end
        end
    end
end
%{
xp = centrage_magnetique(double(x));
yp = centrage_magnetique(double(y));
% [XI, YI] = meshgrid(xp,yp);
z = interp2(x, y', z, xp, yp', 'linear');
x = xp;
y = yp;
%}

% Préparation travail si le nom de la variable n'est pas "z" : a continuer
%{
ListeVar = get_ListeVar(a);
for k=1:length(ListeVar)
[valDim, nomDim] = get_varDim(a, ListeVar{k});

end
%}

Name = 'z';

% this = cl_image('Image', flipud(z'), ...
this = cl_image('Image', z, ...
    'Name',                Name, ...
    'x',                    double(x), ...
    'y',                    double(y), ...
    'XUnit',               'deg', ...
    'YUnit',               'deg', ...
    'TagSynchroX',          TagSynchroX, ...
    'TagSynchroY',          TagSynchroY, ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         GeometryType, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'GMT', ...
    'Carto', Carto);

flag = 1;
