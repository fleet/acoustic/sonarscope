% Importation d'un fichier GMT (*.grd)
%
% Syntax
%   [a, flag] = import_gmt_LatLong(b, nomFic)
%
% Input Arguments
%   b      : Instance vide de cl_image 
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('Aslanian.grd')
%   [a, flag] = import_gmt_LatLong(cl_image, nomFic);
%   imagesc(a)
%
% See also is_GMT Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_gmt_LatLong(~, nomFic, varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []); %#ok<ASGLU>

this = cl_image.empty;

if isempty(DataType)
    [flag, DataType] = cl_image.edit_DataType;
    if ~flag
        return
    end
end

a = cl_netcdf('fileName', nomFic);

%% Test si c'est un fichier GMT

numAtt1 = find_numAtt(a, 'title');
numAtt2 = find_numAtt(a, 'Conventions');
numAtt3 = find_numAtt(a, 'GMT_version');
numAtt4 = find_numAtt(a, 'history');
numVar1 = find_numVar(a, 'lon');
numVar2 = find_numVar(a, 'lat');
numVar3 = find_numVar(a, 'z');
if isempty(numAtt1) || isempty(numAtt2) || isempty(numAtt3) || isempty(numAtt4) || ...
        isempty(numVar1) || isempty(numVar2) || isempty(numVar3)
    flag = 0;
    str = sprintf(Lang('Le fichier %s ne semble pas etre un fichier GMT', ...
        'This file seems not to be a GMT one.'), nomFic);
    my_warndlg(str, 1);
    return
end

x = get_value(a, 'lon');
y = get_value(a, 'lat');
z = get_value(a, 'z');
Name = 'z';

% this = cl_image('Image', flipud(z'), ...
this = cl_image('Image', z, ...
    'Name',            Name, ...
    'x',               double(x), ...
    'y',               double(y), ...
    'XUnit',           'deg', ...
    'YUnit',           'deg', ...
    'TagSynchroX',     'Longitude', ...
    'TagSynchroY',     'Latitude', ...
    'ColormapIndex',   3, ...
    'DataType',        DataType, ...
    'GeometryType',    cl_image.indGeometryType('LatLong'), ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'GMT');
Carto = cl_carto;
this = set_Carto(this, Carto);

flag = 1;
