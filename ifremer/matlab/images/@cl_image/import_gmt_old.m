% Importation d'un fichier GMT (*.grd)
%
% Syntax
%   [a, flag] = import_gmt(b, nomFic)
%
% Input Arguments
%   b      : Instance vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('Aslanian.grd')
%   [a, flag] = import_gmt(cl_image, nomFic);
%   imagesc(a)
%
% See also is_GMT Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_gmt_old(~, nomFic, varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []); %#ok<ASGLU>

this = cl_image.empty;

if isempty(DataType)
    [flag, DataType] = cl_image.edit_DataType;
    if ~flag
        return
    end
    Unit = 'm';
else
    Unit = '';
end

a = cl_netcdf('fileName', nomFic);
sk = get(a, 'skeleton');

%% Test si c'est un fichier GMT

numAtt1 = find_numAtt(a, 'title');
numAtt2 = find_numAtt(a, 'source');
numVar1 = find_numVar(a, 'x_range');
numVar2 = find_numVar(a, 'y_range');
numVar3 = find_numVar(a, 'spacing');
numVar4 = find_numVar(a, 'dimension');
if isempty(numAtt1) ||  isempty(numAtt2) || isempty(numVar1) || isempty(numVar2) || isempty(numVar3) || isempty(numVar4)
    flag = 0;
    str = sprintf('Le fichier %s ne semble pas etre un fichier GMT', nomFic);
    my_warndlg(str, 1);
    return
end

x_range = get_value(a, 'x_range');
y_range = get_value(a, 'y_range');

if (x_range(1) > 360) && (y_range(1) > 90)
    nomDir = fileparts(nomFic);
    [flag, Carto] = define(cl_carto([]), nomDir, [], []);
    if ~flag
        return
    end
    GeometryType = cl_image.indGeometryType('GeoYX');
else
    GeometryType = cl_image.indGeometryType('LatLong');
    Carto = cl_carto;
end


% spacing   = get_value(a, 'spacing');
dimension = get_value(a, 'dimension');
z = get_value(a, numVar4+1);

z(isinf(z)) = NaN;

Name = sk.var(numVar4+1).name;

z = reshape(z, dimension);

x_range = double(x_range);
y_range = double(y_range);
dimension = double(dimension);
x = linspace(x_range(1), x_range(2), dimension(1));
y = linspace(y_range(1), y_range(2), dimension(2));

% figure; imagesc(x, y, flipud(z'));

this = cl_image('Image', flipud(z'), ...
    'Unit',                 Unit, ...
    'Name',                Name, ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'deg', ...
    'YUnit',               'deg', ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         GeometryType, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'GMT');

this = set_Carto(this, Carto);

flag = 1;
