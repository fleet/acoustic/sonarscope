% Importation d'un fichier GXF-3 (ASCII)
%
% Syntax
%   [flag, a] = import_gxf(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%
% See also cl_image/import_* Authors
% Authors : GLT
%--------------------------------------------------------------------------

function [flag, this] = import_gxf(I0, nomFic)


%% Est ce que le fichier �xiste
fid = fopen(nomFic, 'rt');
if fid < -1
    flag = 0;
    return
end

%% Initialisation structure
mandatoryGXFObject =...
    {'points','rows','grid'};
validGXFObject = ...
    {'dummy','grid','gtype','map_projection','map_datum_transform',...
    'points','ptseparation','rotation','rows','rwseparation','sense','title',...
    'transform','unit_length','xorigin','yorigin','zmaximum','zminimum'};
defaultVal = ...
    {[], [], [], [], [], [], 1, 0, [], 1, 1, '', [1, 0], 'unknown', 0, 0, [], []};

FieldValPair = [validGXFObject; defaultVal];
GXF = struct(FieldValPair{:});

%% Lecture du fichier
while ~feof(fid)
    line = deblank(fgetl(fid));
    if ~isempty(line) && strcmp(line(1), '#') && ~strcmp(line(2), '#') % on ignore les champs perso commen�ant par '##'
        gxfObj = validatestring( lower(line(2:end)), validGXFObject);
        switch gxfObj
            case {'dummy','gtype','points','ptseparation','rotation','rows',...
                    'rwseparation','sense','xorigin','yorigin','zmaximum','zminimum'}
                % 1 param�tre num�rique
                c = textscan(fid, '%f %*[^\n]', 1);
                GXF.(gxfObj) = deal(c{:});
            case 'title'
                GXF.(gxfObj) = fgetl(fid);
            case 'map_projection'
                c = textscan(fid, '%s[^\n]', 1, 'delimiter', ',');
                GXF.(gxfObj).projName = strrep(char(c{1}{:}),'"', '');
                c = textscan(fid, '%s', 1, 'delimiter', ',');
                GXF.(gxfObj).datum = strrep(char(c{1}{:}),'"', '');
                c = textscan(fid, '%f%f%f%*[^\n]', 1, 'delimiter', ',');
                [GXF.(gxfObj).majorAxis, GXF.(gxfObj).eccentricity, GXF.(gxfObj).primeMeridian] = deal(c{:});
                c = textscan(fid, '%s', 1, 'delimiter', ',');
                GXF.(gxfObj).projMethod = strrep(char(c{1}{:}),'"', '');
                c = textscan(fid, '%f', 'delimiter', ',', 'collectOutput', true);
                GXF.(gxfObj).projParam = deal(c{:});
                % TODO le coup du \
            case 'map_datum_transform'
                fprintf('!!! On ignore le champ %s !!!', upper(gxfObj));
            case 'transform'
                c = textscan(fid, '%f,%f,"%s"%*[^\n]', 1, 'collectOutput', true);
                [GXF.(gxfObj), GXF.unitName] = deal(c{:});
            case 'unit_length'
                c = textscan(fid, '%s,%f%*[^\n]', 1, 'collectOutput', true);
                [GXF.(gxfObj), GXF.unit2m] = deal(c{:});
            case 'grid'
                GXF.(gxfObj) = [];
                break % -> dernier objet du fichier lecture de la grille
        end
    end
end

GXFfields = fieldnames(GXF);
for i = 1:numel(mandatoryGXFObject)
    if ~any(strcmp(GXFfields, mandatoryGXFObject{i}))
        str1 = sprintf('Import GXF impossible :\nLe fichier doit contenir les champs :\n%s', char(strcat(mandatoryGXFObject,{', '}))');
        str2 = sprintf('GXF import error :\nfile must contain followinf fields :\n%s', char(strcat(mandatoryGXFObject,{', '}))');
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end

%% lecture de la grille

disp('lecture grille');
if ~isempty(GXF.gtype) && GXF.gtype > 0    
    grid = textscan(fid, '%s', 'collectOutput', true);
    grid = grid{1};
    nLine = size(grid,1);
    nLinePerRow    = ceil(GXF.points*GXF.gtype / 80);
    if nLinePerRow > 1 && size(grid, 1) == nLinePerRow*GXF.rows
        % Lecture base 90 simple     
        grid = char(grid);
        grid = reshape(grid', 80*nLinePerRow, GXF.rows)'; 
        grid = deblank(grid);
        GXF.(gxfObj) = base90ToDec(grid, GXF.gtype);
    else        
        % Lecture base 90, avec r�p�tition        
        GXF.grid = NaN(GXF.rows, GXF.points);        
        points   = NaN(1,GXF.points);
        iRow     = 0;
        iLine    = 0;
        pts      = 0;
        
        while iLine < nLine && pts < GXF.points && iRow < GXF.rows
            iLine      = iLine+1;            
            lineStr    = grid{iLine};
            lenline    = length(lineStr);
            repeatCode = strfind(lineStr, repmat('"', 1, GXF.gtype) );
            
            if isempty(repeatCode) 
                ptsRead = lenline/GXF.gtype;
                points( pts + (1:ptsRead) ) = ...
                    base90ToDec(lineStr, GXF.gtype);
                pts = pts + ptsRead;
            else
                iDx = 1;
                iRepeat = 1;
                while iDx < lenline
                    % on lit les points normaux
                    if iRepeat <= length(repeatCode)
                        ptsRead = ( repeatCode(iRepeat) - iDx ) / GXF.gtype;
                        points( pts + (1:ptsRead) ) = ...
                            base90ToDec( lineStr(iDx:repeatCode(iRepeat)-1), GXF.gtype);
                        pts = pts + ptsRead;                        
                        % on lit les r�p�titions
                        ptsRead = base90ToDec( lineStr(repeatCode(iRepeat)+GXF.gtype-1 + (1:GXF.gtype)), GXF.gtype);
                        val     = base90ToDec( lineStr(repeatCode(iRepeat)+2*GXF.gtype-1 + (1:GXF.gtype)), GXF.gtype);
                        points( pts + (1:ptsRead) ) = val;
                        pts = pts + ptsRead;
                        iDx = repeatCode(iRepeat) + 3*GXF.gtype;
                        iRepeat = iRepeat+1;
                    else % on lit la fin de la ligne
                        ptsRead = ( lenline - iDx +1 ) / GXF.gtype;
                        points( pts + (1:ptsRead) ) = ...
                            base90ToDec( lineStr(iDx:end), GXF.gtype);
                        pts = pts + ptsRead; 
                        iDx = lenline;
                    end
                                      
                end
            end
            
            if pts >= GXF.points                
                iRow            = iRow+1;
                GXF.grid(iRow,:)= points;
                points(:)       = NaN;
                pts             = 0;
            end
        end
        
    end    
    GXF.grid = GXF.grid*GXF.transform(1) + GXF.transform(2);
else
    % lecture toute simple    
    grid = cell2mat(textscan(fid, '%f', GXF.rows*GXF.points, 'collectOutput', true));
    GXF.grid = reshape(grid, GXF.points, GXF.rows)';
    GXF.grid(GXF.grid(:) == GXF.dummy) = NaN;
end
clear grid
disp('fin lecture grille');

%% Carto

if ~isempty(GXF.map_projection)
    % verif pr�sence fichier .def
    nomDir = fileparts(nomFic);
    [flagExistenceCarto, carto, nomFicCarto] = test_ExistenceCarto_Folder(nomDir);
    if flagExistenceCarto
        str1 = sprintf('Le fichier "%s" est interpr�t� directement', nomFicCarto);
        str2 = sprintf('File "%s" is used by default.', nomFicCarto);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
    else
        if strcmpi(GXF.map_projection.projMethod, 'geographic')
            GeometryType = cl_image.indGeometryType('LatLong');
            unitexy = 'deg';
        else
            GeometryType = cl_image.indGeometryType('GeoYX');
            unitexy = 'm';
        end
        
        carto = cl_carto([]);
        if any(strcmpi(GXF.map_projection.datum, get(carto, 'Ellipsoide.strType')))
            set(carto, 'Ellipsoide.type' ,find(strcmpi(GXF.map_projection.datum, get(carto, 'Ellipsoide.strType'))));
            switch GXF.map_projection.projMethod
                case 'Geographic'
                    set(carto,  'Projection.Type', 1 ); %geodetic
                case 'Mercator (2SP)'
                    set(carto, 'Projection.Type',                       2 ); %Mercator
                    set(carto, 'Projection.Mercator.lat_ech_cons',      GXF.map_projection.projParam(1) );
                    set(carto, 'Projection.Mercator.long_merid_orig',   GXF.map_projection.projParam(2) );
                    set(carto, 'Projection.Mercator.X0',                GXF.map_projection.projParam(3) );
                    set(carto, 'Projection.Mercator.Y0',                GXF.map_projection.projParam(4) );
                case 'Transverse Mercator'
                    set(carto,  'Projection.Type', 3 ); %UTM
                    set(carto,  'Projection.UTM.Fuseau',                str2double(GXF.map_projection.projName(end-2:end-1)) );
                    set(carto,  'Projection.UTM.Hemisphere' ,           GXF.map_projection.projName(end) );
                    %             case 'Lambert Conic Conformal (2SP)'
                    %                 disp('TODO')
                    %             case {'Arctic Polar Stereographic', 'Antarctic Polar Stereographic'}
                    %                 disp('TODO')
                otherwise
                    my_warndlg(sprintf('GXF import :\n\n%s : %s','Unknown Projection', GXF.map_projection.projMethod),  1);
                    set(carto,  'Projection.Type', 7); %other
            end
        else
            my_warndlg(sprintf('GXF import :\n\n%s : %s','Unknown datum', GXF.map_projection.datum),  1);
            set(carto, 'Ellipsoide.type' ,1 ); %undefined
        end
    end
else % pas de "map_projection"
    carto = [];
    unitexy = '';
    GeometryType = 1; %unknown
end

%% Type de donn�es

[flag, DataType] = cl_image.edit_DataType;
if ~flag
    return
end

%% Unit

if isfield(GXF, 'unitName')
    Unit = char(GXF.unitName);
else
	[flag, Unit] = cl_image.edit_DataUnit('DataType', DataType);
    if ~flag
        return
    end
end

%% titre
if ~isempty(GXF.title)
    titre = strrep(GXF.title,'"', '');
else
    [~,titre] = fileparts(nomFic);
end

%% X,Y, grid Vs Sense
x = (0:GXF.points-1)*GXF.ptseparation + GXF.xorigin;
y = (0:GXF.rows-1)*GXF.rwseparation + GXF.yorigin;
switch GXF.sense
    case 1
        % rien � faire
    case -2
        GXF.grid = flipud(GXF.grid);        
%     case 3 
%         GXF.grid = fliplr(GXF.grid);
%     case -4
%         GXF.grid = rot90(GXF.grid,2);       
    otherwise
         my_warndlg(sprintf('GXF import :\n\n%s : %s','Unknown SENSE', GXF.sense),  1);
        return    
end

%% cr�ation image

this = cl_image('Image',    GXF.grid, ...
    'Name',                 titre, ...
    'Unit',                 Unit, ...
    'x',                    x, ...
    'y',                    y, ...
    'valNaN',               NaN,...
    'XUnit',                unitexy, ...
    'YUnit',                unitexy, ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         GeometryType,...
    'InitialFileName',      nomFic, ...
    'InitialFileFormat',    'GXF-3', ...
    'Carto', carto);
this = update_Name(this);

if ~isempty(carto)
    this = set(this, 'Carto', carto);
end

flag = 1;



%% ------------------------------------------------------------------------
%                           SOUS-FONCTIONS
% -------------------------------------------------------------------------
function d = base90ToDec(s,p)
% BASE90TODEC Convertion d'une cha�ne de caract�re en base90 ASCII vers un entier
% Exemples : dec2base(0) = '%' | dec2base90(728999,3) = '~~~'
% voir aussi exportGXF.m/dec2base90

b = 90;

s = char(s);
if isempty(s)
    d = []; 
    return
end

[m,n]   = size(s);
s = reshape(s', 1, m*n); % on met tout sur une ligne
if rem(m*n,p) ~= 0
    return
end


if ~isempty(find(s==' ' | s==0,1))
  s = strjust(s);
  s(s==' ' | s==0) = '0';
end

s = reshape(s, p, m*n/p);
s = double(s) - 37; 
s(s == -4) = NaN; % char ASCII 33 = "!" 

for i=1:p
    d(i,:) = s(p-i+1,:) * b^(i-1); %#ok<AGROW>
end

d = sum(d);
d = reshape(d, n/p, m)';
