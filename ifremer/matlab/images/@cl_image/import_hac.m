% Importation d'un fichier HAC (Sondeurs de p�che)
%
% Syntax
%   [flag, a] = import_hac(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.hac')
%   [flag, a] = import_hac(cl_image, nomFic);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this, Carto] = import_hac(~, nomFic, varargin)

% [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 1);
[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);

[varargin, NumSounder] = getPropertyValue(varargin, 'NumSounder', []);
[varargin, listeLayers] = getPropertyValue(varargin, 'listeLayers', []);
[varargin, typeGeometry] = getPropertyValue(varargin, 'typeGeometry', []);

this = cl_image.empty;

if ~exist(nomFic, 'file')
    return
end

a = cl_hac('nomFic', nomFic);
if isempty(a)
    flag = 0;
    return
end


% Info.Sounder(1).Name        = 'EK60';
% Info.Sounder(1).Direction    = 'Vertical';
% Info.Sounder(1).Frequency   = [18 20 22 24 26];
% Info.Sounder(1).TypeSounder = 1;
%
% Info.Sounder(2).Name        = 'ME70';
% Info.Sounder(2).Direction    = 'Multibeam';
% Info.Sounder(2).Frequency   = 1:24;
% Info.Sounder(1).TypeSounder = 2;
%
% Info.Sounder(3).Name        = 'EK60';
% Info.Sounder(3).Direction    = 'Horizontal';
% Info.Sounder(3).Frequency   = 18;
% Info.Sounder(1).TypeSounder = 1;

% [flag, nbSounders] = get_nbSounders(a);
[flag, listSounders] = get_listSounders(a);
if ~flag
    return
end
[flag, listDirection] = get_listDirection(a);
if ~flag
    return
end
[flag, typeSounders] = get_typeSounders(a);
if ~flag
    return
end
[flag, flagIdentLayers] = get_flagIdentLayers(a);
if ~flag
    return
end


if isempty(NumSounder)
    if length(listSounders) > 1
        str1 = 'S�lectionnez le/les sondeurs � importer';
        str2 = 'Please, select sounders to import';
        pppp = {};
        for i=1:numel(listSounders)
            pppp{i} = sprintf('%s | %s', listSounders(i).Name, listDirection{i}); %#ok<AGROW>
        end
        [NumSounder, flag] = my_listdlg(Lang(str1,str2), pppp);
        if ~flag
            return
        end
    else
        NumSounder = 1;
    end
end

if isempty(typeSounders)
    typeGeometry = 1;
    TypeSounder  = 1;
else
    TypeSounder = typeSounders(NumSounder);
    if TypeSounder == 2 % Cas du ME70
        if isempty(typeGeometry)
            str1 = 'Visualisation sous forme de : ';
            str2 = 'TODO';
            [typeGeometry, flag] = my_listdlg(Lang(str1,str2), {'PingSamples'; 'SampleBeam'});
            if ~flag
                return
            end
            
            if typeGeometry == 2
                %                 nbPings = get(a, 'nbPings');
                
                % En attendant de pouvoir acc�der directement � la dimension
                [flag, Data] = read_data(a, 'Memmapfile', 1, 'NumSounder', NumSounder); % , 'NumSounder', []);
                nbPings = Data.Dimensions.nbPings;
                clear Data
                % fin
                
                %                 InitialFileName = this.InitialFileName;
                %                 flagPingXxx = testSignature(this.Images(this.indImage), 'GeometryType', 'PingXxxx', 'noMessage');
                %                 if strcmp(nomFic, InitialFileName) && flagPingXxx
                %                     xyCross  = this.cross(this.indImage, :);
                %                     iy = round(xyCross(2));
                %                     rep = sprintf('1 %d % d', iy, nbPings);
                %                 else
                %                     rep = sprintf('1  %d', nbPings);
                %                 end
                rep = sprintf('1  %d', nbPings);
                [rep, flag] = my_inputdlg(Lang(str1,str2), rep);
                if ~flag
                    return
                end
                PingNumbers = eval(['[' rep{1} ']']);
                %                 PingNumbers = [1 nbPings];
            end
        end
    else
        typeGeometry = 1; % 'PingSamples'
    end
end

%% Liste des layers � importer

if isempty(listeLayers)
    str1 = 'Type de donn�es � importer : ';
    str2 = 'Type of data to import : ';
    pppp = flagIdentLayers(NumSounder, :);
    pppp = find(pppp==1);
    strIdentLayers     = get(a, 'strIdentData');
    str = strIdentLayers(pppp); %#ok<FNDSB>
    [listeLayers, flag] = my_listdlg(Lang(str1,str2), str);
    if ~flag
        return
    end
end

%{
[flag, nbSlices, listeFrequency] = get_nbSlices(a, NumSounder, TypeSounder);
if ~flag
return
end
% TODO : s�lection dans liste listeFrequency
%}

%% Importation

for k=1:length(NumSounder)
    switch typeGeometry
        case 1
            [flag, b, Carto] = import_hac_profil(a, NumSounder(k), Carto, 'TypeSounder', TypeSounder(k), ...
                'listeLayers', listeLayers, varargin{:});
        case 2
            [flag, b, Carto] = import_hac_ping(a, NumSounder(k), Carto, 'TypeSounder', TypeSounder(k), ...
                'listeLayers', listeLayers, 'PingNumbers', PingNumbers, varargin{:});
    end
    if flag
        this(end+1:end+length(b)) = b;
    end
end

