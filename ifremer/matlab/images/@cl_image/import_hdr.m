% Importation d'un fichier ESRI (*.hdr, *.flt)
%
% Syntax
%   [flag, a] = import_hdr(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
% %   nomFic = getNomFicDatabase('Aslanian.hdr')
%   nomFic = 'S:\sig_reunion\DonneesBrutes\Documents\Documents-Oehlert\MNT_Mer.hdr'
%   [flag, a] = import_hdr(cl_image, nomFic);
%   imagesc(a)
%   nomFic = 'S:\sig_reunion\DonneesBrutes\Documents\Documents-Oehlert\MNT_Terre.hdr'
%   [flag, b] = import_hdr(cl_image, nomFic);
%   SonarScope([a b])
%
% See also Authors
% References : http://www.safe.com/reader_writerPDF/esrihdr.pdf
%--------------------------------------------------------------------------

function [flag, this, DataType, GeometryType, Carto] = import_hdr(this, nomFic, varargin)%#ok

[varargin, Carto]        = getPropertyValue(varargin, 'Carto', []);
[varargin, DataType]     = getPropertyValue(varargin, 'DataType', []);
[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []); %#ok<ASGLU>

flag = 0;
this = [];

%% Lecture du fichier .hdr

fid = fopen(nomFic, 'r');
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = 0;
    return
end

nbands    = 1;
layout    = 'bil';
byteorder = 'i';
skipbytes = 0;
ulxmap    = 0;
ulymap    = 0;
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    mots = strsplit(tline);
    switch lower(mots{1})
        case 'nrows'
            nrows = str2double(mots{2});
        case 'ncols'
            ncols = str2double(mots{2});
        case 'nbands'
            nbands = str2double(mots{2});
        case 'nbits'
            nbits = str2double(mots{2});
        case 'byteorder'
            byteorder = lower(mots{2});
        case 'layout'
            layout = lower(mots{2}); % bil : band interleaved by line; bip : band interleaved by pixel; bsq : band sequential
        case 'skipbytes'
            skipbytes = mots{2};
        case 'ulxmap'
            ulxmap = str2double(mots{2});
        case 'ulymap'
            ulymap = str2double(mots{2});
        case 'xdim'
            xdim = str2double(mots{2});
        case 'ydim'
            ydim = str2double(mots{2});
        case 'nodata'
            nodata = str2double(mots{2});
            
        case 'bandrowbytes'
            bandrowbytes = str2double(mots{2});
        case 'totalrowbytes'
            totalrowbytes = str2double(mots{2});
        case 'bandgapbytes'
            bandgapbytes = str2double(mots{2});
            
        otherwise
            str = sprintf('"%s" is not interpreted in file "%s"', mots{1}, nomFic);
            my_warndlg(str, 1);
            fclose(fid);
            return
    end
end
fclose(fid);

%% Lecture du fichier .flt

[nomDir, nomFicSeul] = fileparts(nomFic);
nomFicImage = fullfile(nomDir, [nomFicSeul '.flt']);
if ~exist(nomFicImage, 'file')
    nomFicImage = fullfile(nomDir, [nomFicSeul '.bil']);
    if ~exist(nomFicImage, 'file')
        nomFicImage = fullfile(nomDir, [nomFicSeul '.dem']);
        if ~exist(nomFicImage, 'file')
            str = sprintf('No Image data was found for file "%s"', nomFic);
            my_warndlg(str, 1);
            return
        end
    end
end

% nbBytes = sizeFic(nomFic)
% nbOcPerPixel = nbBytes / (nrows*ncols)

if strcmpi(byteorder(1), 'm') % M (Motorola) | I (Intel)
    machineformat = 'b';
else
    machineformat = 'l';
end

switch nbits
    case 16
        precision = 'int16=>real*4';
    otherwise
        str = sprintf('nbits not interpreted in "%s"', nomFicImage);
        my_warndlg(str, 1);
        return
end

% if (nrows*ncols*nbands*nbits/8) > 6e7
%     X = cl_memmapfile('Value', NaN, 'Size', [nrows ncols nbands], 'Format', 'single');
% else
%     X = NaN([nrows ncols nbands], 'single');
% end

try
    X = NaN([nrows ncols nbands], 'single');
catch %#ok<CTCH>
    X = cl_memmapfile('Value', NaN, 'Size', [nrows ncols nbands], 'Format', 'single');
end

fid = fopen(nomFicImage, 'r', machineformat);
fseek(fid, skipbytes, 'bof');

switch layout
    case 'bil'
        for i=1:nrows
            for k=1:nbands
                Y = fread(fid, ncols, precision);
                Y(Y == nodata) = NaN;
                %                 sub = nearlyEqual(Y, nodata, 0.00001);
                %                 Y(sub) = NaN;
                X(nrows-i+1,:,k) = Y;
            end
        end
    otherwise
        str = sprintf('"%s" pas encore programm� dans import_hdr', layout);
        my_warndlg(str, 1);
        return
end
fclose(fid);

%% Calcul des coordonn�es

x = ulxmap + (0:(ncols-1)) * xdim;
y = ulymap + ((1-nrows):0) * ydim;

% figure; imagesc(x, y, X); colormap(jet(256)); axis xy

%% Demande de renseignement concernant l'image

if isempty(DataType)
    if isempty(DataType)
        [flag, DataType] = cl_image.edit_DataType;
        if ~flag
            return
        end
    end
end

if isempty(GeometryType)
    if isempty(GeometryType)
        [flag, GeometryType] = cl_image.edit_GeometryType;
        if ~flag
            return
        end
    end
end

if isempty(Carto)
    [flag, Carto] = editParams(cl_carto([]), [], [], 'MsgInit');
    if ~flag
        return
    end
end

%% Cr�ation de l'instance image

this = cl_image('Image', X, ...
    'Name',                nomFicSeul, ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'deg', ...
    'YUnit',               'deg', ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         GeometryType, ...
    'Carto',                Carto, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'HDR');

flag = 1;

