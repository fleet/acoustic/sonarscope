% nomFic = 'C:\SonarScopeData\Sonar\EM12D_PRISMED_125m_32bits_LatLong_Bathymetry.tif';
% nomFic = 'C:\SonarScopeData\Sonar\EM12D_PRISMED_125m_GreatestSlope_LatLong_SunColor.tif';
% [flag, a] = import_imagePlusPlus(cl_image, nomFic);
% SonarScope(a)

function [flag, a] = import_imagePlusPlus(I0, nomFic)

flag = 1; % TODO : faut faire quekchose l� !!!

[I, map] = imread(nomFic);
if size(I,3) == 3
    ImageType = 2;
else
    Info = imfinfo(nomFic);
    if strcmp(Info.ColorType, 'indexed')
        ImageType = 3; % Emp�che le rehaussement de contraste si 3 : que faire ? Modifi� le 12/09/2010
    else
        ImageType = 1;
    end
end
[~, ImageName] = fileparts(nomFic);
a = cl_image('Image', I, 'Name', ImageName, 'GeometryType', cl_image.indGeometryType('Unknown'), ...
    'ImageType', ImageType, 'ColormapIndex', 2, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'IMAGE');

clear I

if ~isempty(map)
    a.ColormapIndex  = 1;
    a.ColormapCustom = map;
end

% Lecture WorldFile associ� si pr�sent
a = import_worldfile(a, nomFic);

% On compl�te les informations si on trouve in fichier .xml associ� (cr�� par export_info_xml)
a = import_info_xml(a, nomFic);

[~, nomFicSeul] = fileparts(nomFic);
[~, indGeometryType, indexDataType] = cl_image.uncode_ImageName(nomFicSeul);
if a.DataType == 1
    a.DataType = indexDataType;
end
if a.GeometryType == 1
    a.GeometryType = indGeometryType;
end
