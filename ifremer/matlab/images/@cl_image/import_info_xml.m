function this = import_info_xml(this, nomFic)

[nomDir, nomFic] = fileparts(nomFic);
nomFicXML = fullfile(nomDir, [nomFic '.xml']);

if exist(nomFicXML, 'file') && ~exist(fullfile(nomDir, nomFic), 'dir') %% Condition pour �tre s�r que c'est pas un export XML classique de l'image. Je sais c'est pas tr�s heureux
    Info = xml_read(nomFicXML);
    
    this.Name                   = Info.Name;
    this.Unit                   = Info.Unit;
    this.DataType               = Info.DataType;
    this.GeometryType           = Info.GeometryType;
%     this.ImageType              = Info.ImageType;
    this.SpectralStatus         = Info.SpectralStatus;
    this.SaveParamsIfFFT        = Info.SaveParamsIfFFT;
    this.x                      = linspace(Info.XLim(1), Info.XLim(2), this.nbColumns);
    this.XLabel                 = Info.XLabel;
    this.XUnit                  = Info.XUnit;
    this.XLim                   = Info.XLim;
    this.XDir                   = Info.XDir;
    if Info.YDir == 1
        this.y                  = linspace(Info.YLim(2), Info.YLim(1), this.nbRows);
    else
        this.y                  = linspace(Info.YLim(1), Info.YLim(2), this.nbRows);
    end
    this.YLabel                 = Info.YLabel;
    this.YUnit                  = Info.YUnit;
    this.YLim                   = Info.YLim;
    this.YDir                   = Info.YDir;
    this.CLim                   = Info.CLim;
    this.ColormapCustom         = Info.ColormapCustom;
    this.Video                  = Info.Video;
    this.ContourValues          = Info.ContourValues;
    this.TagSynchroX            = Info.TagSynchroX;
    this.TagSynchroY            = Info.TagSynchroY;
    this.TagSynchroContrast     = Info.TagSynchroContrast;
    this.InitialFileName        = Info.InitialFileName;
    this.InitialFileFormat      = Info.InitialFileFormat;
    this.InitialImageName       = Info.InitialImageName;
    this.Comments               = Info.Comments;
    this.OriginColorbar         = Info.OriginColorbar;
    this.RegionOfInterest       = Info.RegionOfInterest;
    this.CourbesStatistiques	= Info.CourbesStatistiques;
    this.Texture                = Info.Texture;
    if isfield(Info, 'Naviagetion') % Erreur dnas vielles versions
        this.Navigation = Info.Naviagetion;
    else
        this.Navigation = Info.Navigation;
    end
  
    this.ColormapIndex = Info.ColormapIndex;
    
    if ~isempty(Info.Sonar)
        this.Sonar = Info.Sonar;
    end
    if ~isempty(Info.Carto) %% Question GLU : comment faire
        if ~isfield(Info.Carto, 'MapTbx')
            [flag, Carto] = loadNewCartoFromOldCarto(Info.Carto);
            if ~flag
                return
            end
        else
            [flag, Carto] = loadNewCartoFromSomeProperties(Info.Carto);
            if ~flag
                return
            end
                    
        end
        this.Carto = Carto;
    end

%     this.Time = Info.Time;
    this = update_Name(this);
end
