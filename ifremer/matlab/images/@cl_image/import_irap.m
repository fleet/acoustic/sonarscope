% Importation d'un fichier IRAP (*.irap)
%
% Syntax
%   [a, flag] = import_irap(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFicIn = getNomFicDatabase('KBMA0504.irap')
%   a = import_irap(cl_image, nomFicIn);
%   imagesc(a)
%
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_irap(this, nomFic)

fid = fopen(nomFic);
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.irap']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end

% Ligne 1
line = fgets(fid);
x = sscanf(line, '%f');
% rest  = x(1);
rows  = x(2);
% xcell = x(3);
% ycell = x(4);

% Ligne 2
line = fgets(fid);
x = sscanf(line, '%f');
swx  = x(1);
nex  = x(2);
swy  = x(3);
ney  = x(4);

% Ligne 3
line = fgets(fid);
x = sscanf(line, '%f');
cols  = x(1);
% rest1 = x(2);
% rest2 = x(3);
% rest3 = x(4);

% Ligne 4
line = fgets(fid);%#ok
% x = sscanf(line, '%f');

z = fscanf(fid, '%f', [cols, rows]);
z(z == 9999900) = NaN;

fclose(fid);

x = linspace(swx, nex, cols);
y = linspace(swy, ney, rows);

this = cl_image('Image', z', ...
    'Name',                'IRAP DTM', ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'm', ...
    'YUnit',               'm', ...
    'ColormapIndex',        3, ...
    'DataType',             cl_image.indDataType('Bathymetry'), ...
    'GeometryType',         cl_image.indGeometryType('GeoYX'), ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'IRAP');
flag = 1;
