function [flag, this] = import_lgz(~, nomFic, varargin)

persistent persistent_pasxy persistent_TypeAlgo

% [varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, ScaleFactor]    = getPropertyValue(varargin, 'ScaleFactor', []);
[varargin, ControleManuel] = getPropertyValue(varargin, 'ControleManuel', 0);
[varargin, pasxy]          = getPropertyValue(varargin, 'pasxy', []);
[varargin, TypeAlgo]       = getPropertyValue(varargin, 'TypeAlgo', 2);
[varargin, memeReponses]   = getPropertyValue(varargin, 'memeReponses', 0); %#ok<ASGLU>

%% Read the ASCII file

DT = cl_image.indDataType('Bathymetry');
[flag, X, Y, V, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, ...
    DataType, Unit] = read_ASCII(nomFic, 'memeReponses', memeReponses, ...
    'NbLigEntete', 0, 'AskSeparator', 0, 'AskValNaN', 0, 'AskGeometryType', 0, ...
    'GeometryType', cl_image.indGeometryType('LatLong'), 'nColumns', 3, 'IndexColumns', [2 1 3], 'DataType', DT, 'Unit', 'm', ...
    'ScaleFactors', [1 1 -1], 'DisplayEntete', 0); %#ok<ASGLU>
if ~flag
    return
end

%% Test de validit� des donn�es

if (min(Y(:)) < -90) || (max(Y(:)) > 90) || (min(X(:)) < -180) || (max(X(:)) > 360)
    str1 = 'Les coordonn�es g�ographiques ne semblent pas �tre des coordonn�es g�ographiques.';
    str2 = 'The coordinates does not seem to me geographic ones.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if all(V(:) > 0)
    V = -V;
end

%% Test pour voir si c'est une grille maill�e ou un semis de points

unique_x = unique(X);
unique_y = unique(Y);

if isempty(unique_x) || isempty(unique_y)
    str1 = 'Y''a quelque-chose qui cloche l� dedans !';
    str2 = 'There is something wrong here !';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

cols = length(unique_x);
rows = length(unique_y);
N = length(V);

%% Units of axis

if isempty(GeometryType)
    unitexy = 'deg';
else
    if GeometryType == cl_image.indGeometryType('LatLong')
        unitexy = 'deg';
    elseif GeometryType == cl_image.indGeometryType('GeoYX')
        unitexy = 'm';
    else
        unitexy = '';
    end
end

%% Limits of axis

% x = unique_x;
y = unique_y;

pas = max(1, floor(N/1e5));

xmin = min(X);
xmax = max(X);
ymin = min(Y);
ymax = max(Y);

%% Plot of the positions

if ControleManuel
    str1 = 'Une figure va pr�senter la r�partition des donn�es, vous devrez la d�truire apr�s avoir �ventuellement zoom� sur la zone que vous voulez importer. Le traitement se poursuivra apr�s la destruction de cette fen�tre.';
    str2 = 'A figure will give you a preview of the data, you will have to delete it to continue or zoom on the good part of it and then delete it.';
    my_warndlg(Lang(str1,str2), 1);
    fig = figure;
    axes;
    plot(X(1:pas:end), Y(1:pas:end), '+');
    grid on;
    axis([xmin xmax ymin ymax])
    title('You have to kill me to continue')
    
    nomFicSave = fullfile(my_tempdir,'ffff.mat');
    clbk = sprintf('w = axis; save(''%s'', ''w'')', nomFicSave);
    set(fig, 'DeleteFcn', clbk)
    
    % Waiting for the figure to be closed
    while ishandle(fig)
        uiwait(fig);
        w = loadmat(nomFicSave, 'nomVar', 'w');
        delete(nomFicSave)
    end
else
    w = [xmin xmax ymin ymax];
end

%% Carto

latMiddle = (ymin + ymax) / 2;
zone = utmzone(latMiddle, (xmin + xmax) / 2);
if latMiddle >= 0
    hemisphere = 'N';
else
    hemisphere = 'S';
end
strProjUTM = ['WGS 84 / UTM zone ' zone(1:2) hemisphere];
[flag, structEPSG] = getParamsProjFromESRIFile('CsLabelName', strProjUTM);
if ~flag
    return
end
CodeEPSG   = structEPSG.code;
Carto = cl_carto('CodeEPSG', CodeEPSG);

% if GeometryType == cl_image.indGeometryType('LatLong')
%     [flag, Carto] = editParams(cl_carto([]), mean([ymin ymax]), mean([xmin xmax]), 'MsgInit');
% else
%     [flag, Carto] = editParams(cl_carto([]), [], [], 'MsgInit');
% end
% if ~flag
%     return
% end

%% Keeps data in the frave given by the user

if ~isequal(w, [xmin xmax ymin ymax])
    xmin = w(1);
    xmax = w(2);
    ymin = w(3);
    ymax = w(4);
    sub = find((X >= xmin) & (X <= xmax) & (Y >= ymin) & (Y <= ymax));
    Y = Y(sub);
    X = X(sub);
    V = V(sub);
    N = length(Y);
    
    xmin = min(X);
    xmax = max(X);
    ymin = min(Y);
    ymax = max(Y);
end

rangex = xmax - xmin;
rangey = ymax - ymin;
surfaceUnitaire = rangex * rangey / N;

if GeometryType == cl_image.indGeometryType('PingBeam')
    pasy = 1;
else
    pasy = sqrt(surfaceUnitaire);
end

%% Type de l'algorithme

if isempty(TypeAlgo)
    if memeReponses && ~isempty(persistent_TypeAlgo)
        TypeAlgo = persistent_TypeAlgo;
    else
        str1 = 'Algorithme de maillage.';
        str2 = 'Gridding algorithm';
        str3 = 'Plus proche voisin';
        str4 = 'Nearest';
        str5 = 'Biin�aire';
        str6 = 'Biinear';
        [TypeAlgo, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            return
        end
        persistent_TypeAlgo = TypeAlgo;
    end
end

%% Cr�ation des axes x et y

if isempty(pasxy) && ~memeReponses
    flag = 0;
    while ~flag
        if GeometryType == cl_image.indGeometryType('LatLong')
            pasx = pasy / cos(mean(y) * (pi/180));
        else
            pasx = pasy;
        end
        
        x = xmin:pasx:xmax;
        y = ymin:pasy:ymax;
        [x, pasx, xmin, xmax] = centrage_magnetique(x);
        [y, pasy, ymin, ymax] = centrage_magnetique(y);
        
        cols = length(x);
        rows = length(y);
        
        if GeometryType == cl_image.indGeometryType('LatLong')
            str1 = sprintf('Proposition : \n\nTaille de la grille (m): %f\nLignes        : %d\nColonnes    : %d\n\nCela vous convient-il ?', ...
                pasy * (6378137 * (pi/180)), rows, cols);
            str2 = sprintf('Proposition : \n\nPixel size (m): %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasy * (6378137 * (pi/180)), rows, cols);
        else
            str1 = sprintf('Proposition : \n\nTaille de la grille : %f\nLignes        : %d\nColonnes    : %d\n\nCela vous convient-il ?', ...
                pasx, rows, cols);
            str2 = sprintf('Proposition : \n\nPixel size : %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasx, rows, cols);
        end
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
        if rep == 1
            flag = 1;
        else
            if GeometryType == cl_image.indGeometryType('LatLong')
                pasyEdition = pasy * (6378137 * pi/180);
            else
                pasyEdition = pasy;
            end
            str1 = 'La donn�e va �tre maill�e, d�finissez la taille de la maille.';
            str2 = 'The import will grid the data. Please select the grid size.';
            [flag, pasy] = inputOneParametre(Lang(str1,str2), 'Grid size', ...
                'Value', pasyEdition, 'Unit', unitexy);
            if ~flag
                return
            end
            
            if GeometryType == cl_image.indGeometryType('LatLong')
                pasy = pasy / (6378137 * pi/180);
                pasx = pasy / cos(mean(y) * (pi/180));
            else
                pasx = pasy;
            end
            flag = 0;
        end
    end
    pasxy(1) = pasx;
    pasxy(2) = pasy;
    persistent_pasxy = pasxy;
    x = xmin:pasx:xmax;
    y = ymin:pasy:ymax;
    [x, pasx, xmin] = centrage_magnetique(x);
    [y, pasy, ymin] = centrage_magnetique(y);
else
    if isempty(pasxy)
        pasxy = persistent_pasxy;
    end
    pasx = pasxy(1);
    pasy = pasxy(2);
    x = xmin:pasx:xmax;
    y = ymin:pasy:ymax;
    [x, pasx, xmin] = centrage_magnetique(x);
    [y, pasy, ymin] = centrage_magnetique(y);
    cols = length(x);
    rows = length(y);
end
pasxy(1) = pasx;
pasxy(2) = pasy; %#ok<NASGU>

%% Cr�ation de l'image

% [xi,yi] = meshgrid(x, y);
% z2 = griddata(Y, X, V, x, y');

z1 = zeros(rows, cols, 'single');
z2 = zeros(rows, cols, 'single');
WorkInProgress(Lang('Maillage en cours', 'Griding data'));
% Pour info : j'ai d� supprimer le my_waitbar car il prenait 99% du temps

switch TypeAlgo
    case 1 % Plus proche voisin
        %         tic
        for k=1:N
            ix = 1 + floor((X(k) - xmin) / pasx);
            iy = 1 + floor((Y(k) - ymin) / pasy);
            
            if (iy >= 1) && (iy <= rows) && (ix >= 1) && (ix <= cols)
                z1(iy,ix) = z1(iy,ix) + 1;
                z2(iy,ix) = z2(iy,ix) + V(k);
            end
        end
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        %         toc
        
    case 2 % Lin�aire
        %         tic
        for k=1:N
            xp = (X(k) - xmin) / pasx;
            yp = (Y(k) - ymin) / pasy;
            ix1 = 1 + floor(xp);
            iy1 = 1 + floor(yp);
            xp = xp +1;
            yp = yp + 1;
            ix2 = ix1 + 1;
            iy2 = iy1 + 1;
            
            %           if all([iy1 iy2 ix1 ix2]  > [1 1 1 1]) && all([iy1 iy2 ix1 ix2]  < [rows rows cols cols]) % Beaucoup plus long
            if ((iy1 >= 1) && (iy1 <= rows) && (ix1 >= 1) && (ix1 <= cols) && ...
                    (iy2 >= 1) && (iy2 <= rows) && (ix2 >= 1) && (ix2 <= cols))
                xalpha = ix2 - xp;
                yalpha = iy2 - yp;
                
                A1 =     xalpha*yalpha;
                A2 = (1-xalpha)*yalpha;
                A3 = (1-xalpha)*(1-yalpha);
                A4 =     xalpha*(1-yalpha);
                Vali = V(k);
                
                z1(iy1,ix1) = z1(iy1,ix1) + A1;
                z2(iy1,ix1) = z2(iy1,ix1) + A1 * Vali;
                
                z1(iy1,ix2) = z1(iy1,ix2) + A2;
                z2(iy1,ix2) = z2(iy1,ix2) + A2 * Vali;
                
                z1(iy2,ix2) = z1(iy2,ix2) + A3;
                z2(iy2,ix2) = z2(iy2,ix2) + A3 * Vali;
                
                z1(iy2,ix1) = z1(iy2,ix1) + A4;
                z2(iy2,ix1) = z2(iy2,ix1) + A4 * Vali;
            end
        end
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        %         toc
        
    case 3 % Fen�tre de pond�ration gaussienne
        % TODO : � poursuivre
        sigma = 0.75;
        n = floor(2 * sigma);
        window = [max(3,2*n+1) max(3,2*n+1)];
        w = window(1);
        %         Y = normpdf(X,mu,sigma)
        h = 100 * fspecial('gaussian', [1 w*100*2], sigma*100);
        h = h(floor(length(h)/2):end);
        %     figure(20081008); plot(h); grid on; title('Gaussian coefficient filter');
        subh = -n:n;
        IX = repmat(subh,  w, 1);
        IY = repmat(subh', 1, w);
        %         tic
        for k=1:N
            Vali = V(k);
            xp = (X(k) - xmin) / pasx;
            yp = (Y(k) - ymin) / pasy;
            ix = 1 + floor(xp);
            iy = 1 + floor(yp);
            
            ix = IX + ix;
            iy = IY + iy;
            
            sub = find((iy >= 1) & (iy <= rows) & (ix >= 1) & (ix <= cols));
            
            for k2=1:length(sub)
                kk = sub(k2);
                xh = xp+1-ix(kk);
                yh = yp+1-iy(kk);
                %             g = normpdf(sqrt(xh*xh+yh*yh),0,sigma);
                g = h(1+floor(100*sqrt(xh*xh+yh*yh)));
                kx = ix(kk);
                ky = iy(kk);
                z1(ky,kx) = z1(ky,kx) + g;
                z2(ky,kx) = z2(ky,kx) + g * Vali;
            end
        end
        z1(z1 < 0.5) = NaN;
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        %         toc
end

if GeometryType == cl_image.indGeometryType('LatLong')
    TagSynchroX = 'Longitude';
    TagSynchroY = 'Latitude';
else
    TagSynchroX = num2str(rand(1));
    TagSynchroY = num2str(rand(1));
end
[~, Name] = fileparts(nomFic);

if ~isempty(ScaleFactor)
    z2 = z2 * ScaleFactor;
end
this = cl_image('Image', z2, ...
    'Name',             Name, ...
    'Unit',              Unit, ...
    'x',                 x, ...
    'y',                 y, ...
    'XUnit',             unitexy, ...
    'YUnit',             unitexy, ...
    'ColormapIndex',     3, ...
    'DataType',          DataType, ...
    'GeometryType',      GeometryType, ...
    'TagSynchroX',       TagSynchroX, ...
    'TagSynchroY',       TagSynchroY, ...
    'InitialFileName',   nomFic, 'InitialFileFormat', 'ASCII xyz', ...
    'Carto',             Carto);
this = update_Name(this);

if ~isempty(Carto)
    this = set(this, 'Carto', Carto);
end

DataType2 = cl_image.indDataType('AveragedPtsNb');
this(2) = cl_image('Image', z1, ...
    'Name',                Name, ...
    'Unit',                 '-', ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',                unitexy, ...
    'YUnit',                unitexy, ...
    'ColormapIndex',        3, ...
    'DataType',             DataType2, ...
    'GeometryType',         GeometryType, ...
    'TagSynchroX',          TagSynchroX, ...
    'TagSynchroY',          TagSynchroY, ...
    'InitialFileName',      nomFic, 'InitialFileFormat', 'ASCII xyz', ...
    'Carto',                Carto);
this(2) = update_Name(this(2));

flag = 1;
