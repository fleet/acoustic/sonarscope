% Importation d'un fichier MATLAB (.mat)
%
% Syntax
%   a = import_matlab(cl_image, nomFic);
%
% Input Arguments
%   this   : Instance vide de cl_image
%   nomFic : Nom du fichier .ers
%
% Output Arguments
%   a : Instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('SAR_data.mat')
%   b = import_matlab(cl_image, nomFic);
%   imagesc(b)
%   c = cli_image('Images', b);
%   editobj(c)
%
% See also cl_image cl_ermapper Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = import_matlab(~, nomFic, varargin)

[varargin, Unknown]    = getPropertyValue(varargin, 'Unknown', 0);
[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 'Auto'); %#ok<ASGLU>

this = cl_image.empty;

listeDesVariables = load(nomFic);
names = fieldnames(listeDesVariables);
if (length(names) == 2) && any(strcmp(names, 'Image')) && any(strcmp(names, 'Info'))
    Image = loadmat(nomFic, 'nomVar', 'Image');
    Info  = loadmat(nomFic, 'nomVar', 'Info');
    if ~isfield(Info, 'GeometryType')
        flag = 0;
        return
    end
    if isfield(Info, 'Titre')
        Info.Name = Info.Name; % Pour r�cup�rer les fichiers anciennement sauv�s
    end
    
    GeometryType = Info.GeometryType;
    if ~Unknown && GeometryType == 1
        [flag, GeometryType] = cl_image.edit_GeometryType;
        if ~flag
            return
        end
    end
    DataType = Info.DataType;
    if ~Unknown && DataType == 1
        [flag, DataType] = cl_image.edit_DataType;
        if ~flag
            return
        end
    end
    this = cl_image('Image',  Image, ...
        'Name',         Info.Name, ...
        'GeometryType',	GeometryType, ...
        'DataType',     DataType, ...
        'Unit',         Info.Unit, ...
        'CLim',         Info.CLim , ...
        'ValNaN',       Info.ValNaN , ...
        'x',            Info.x , ...
        'XUnit',        Info.XUnit , ...
        'XLabel',       Info.XLabel , ...
        'y',            Info.y , ...
        'YUnit',        Info.YUnit , ...
        'YLabel',       Info.YLabel, ...
        'XDir',         Info.XDir, ...
        'YDir',         Info.YDir, ...
        'Memmapfile',   Memmapfile);
    this.ColormapIndex  = 1;
    this.ColormapCustom = Info.Colormap;
    
    if GeometryType == cl_image.indGeometryType('LatLong')
        this.TagSynchroX = 'Longitude';
        this.TagSynchroY = 'Latitude';
    end
    
    flag = 1;
    return
end

b = loadmat(nomFic);
nomDir = fileparts(nomFic);
if isa(b, 'cli_image')
    this = get(b, 'Images');
    [this, flag] = selectionImages(this);
    recuperationFichierMemoireVirtuelle(this, nomDir)
elseif isa(b, 'cl_image')
    this = b;
    recuperationFichierMemoireVirtuelle(this, nomDir)
    [this, flag] = selectionImages(this);
else
    if isnumeric(b)
        sz = size(b);
        if (sz(1) ~= 1) && (sz(2) ~= 1)
            this = cl_image('Image', b);
            flag = 1;
        else
            str1 = 'Cette donn�e n''est pas une image.';
            str2 = 'This data is not an image.';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
        end
    else
        if isa(b, 'org.apache.xerces.dom.DeferredDocumentImpl') % Cas particulier d'un fichier XML
            flag = 1;
        elseif isfield(b, 'Ellipsoide') % Cas d'un fichier de carto pass� en .mat
            flag = 1;
        elseif iscell(b) && ~isempty(b) && strcmp(b{1}, 'ROOT') % Cas d'un fichier XML
            flag = 1;
        else
            str1 = 'Cette donn�e n''est pas num�rique.';
            str2 = 'This data is not a numeric one.';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
        end
    end
end



function [this, flag] = selectionImages(this)

switch length(this)
    case 0
        flag = 0;
    case 1
        flag = 1;
    otherwise
        titreR = {};
        for k=1:length(this)
            titreR{end+1} = this(k).Name; %#ok
        end
        
        [rep, validation] = my_listdlg(Lang('Choix des images � impoter : ', 'Choix des images � importer :'), titreR);
        if ~validation
            this =[];
            flag = 0;
            return
        end
        this = this(rep);
        flag = 1;
end


function recuperationFichierMemoireVirtuelle(this, nomDir)

for i=1:length(this)
    InMem = get(this(i), 'Memory');
    if strcmp(InMem, 'On RAM')
        this(i).Image = cl_memmapfile('Value', this(i).Image);
    else
        nomFicSauve = InMem(19:end);
        [~, nomFic, ext] = fileparts(nomFicSauve);
        nomFic = fullfile(nomDir, [nomFic ext]);
        if exist(nomFicSauve, 'file') && ~strcmp(nomFic, nomFicSauve)
            delete(nomFicSauve);
        end
        copyfile(nomFic, nomFicSauve, 'f')
    end
end

