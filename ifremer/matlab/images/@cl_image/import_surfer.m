% Importation d'un fichier SURFER (*.grd)
%
% Syntax
%   [flag, a] = import_surfer(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%   nomFicIn = getNomFicDatabase('KBMA0504.grd') % Ancien surfer
%   nomFicIn = 'D:\Temp\SPFE\grd\Dsaa.grd'; % Surfer 5 ASCII
%   nomFicIn = 'D:\Temp\SPFE\grd\ZONE2_DTM_5X5M_clipped.grd'; % Surfer 6
%   nomFicIn = 'D:\Temp\SPFE\grd\Colorado.grd'; % Surfer 7
%   [flag, a] = import_surfer(cl_image, nomFicIn);
%   imagesc(a)
%   nomFicOut = my_tempname('.grd');
%   flag = export_surfer(a, nomFicOut, 'Version', 5);
%   imagesc(a)
%
%   nomFicOut = my_tempname('.grd');
%   flag = export_surfer(a, nomFicOut, 'Version', 6);
%   [flag, b] = import_surfer(cl_image, nomFicOut);
%   imagesc(b)
%
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this] = import_surfer(~, nomFic, varargin)

persistent Carto_persistent DataType_persistent GeometryType_persistent Unit_persistent TagSynchroX_persistent TagSynchroY_persistent

[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, Unit]         = getPropertyValue(varargin, 'Unit',         []);
[varargin, DataType]     = getPropertyValue(varargin, 'DataType',     []);
[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false); %#ok<ASGLU>

this = cl_image.empty;

%% Test de la version

[flag, Version] = getVersion(nomFic);
if ~flag
    return
end

%% D�finition des attributs

if isempty(DataType)
    if memeReponses && ~isempty(DataType_persistent)
        DataType = DataType_persistent;
    else
        [flag, DataType] = cl_image.edit_DataType('DataType', cl_image.indDataType('Bathymetry'));
        if ~flag
            return
        end
        DataType_persistent = DataType;
    end
end

if isempty(GeometryType)
    if memeReponses && ~isempty(GeometryType_persistent)
        GeometryType = GeometryType_persistent;
    else
        [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', 3);
        if ~flag
            return
        end
        GeometryType_persistent = GeometryType;
    end
end

if isempty(Unit)
    if memeReponses && ~isempty(Unit_persistent)
        Unit = Unit_persistent;
    else
        [flag, Unit] = cl_image.edit_DataUnit('DataType', DataType);
        if ~flag
            return
        end
        Unit_persistent = Unit;
    end
end

if isempty(Carto)
    if memeReponses && ~isempty(Carto_persistent)
        Carto = Carto_persistent;
    else
        if GeometryType == cl_image.indGeometryType('GeoYX')
            [flag, Carto] = editParams(cl_carto([]), [], [], 'MsgInit');
            if ~flag
                return
            end
        else
            Carto = [];
        end
        Carto_persistent = Carto;
    end
end

if memeReponses && ~isempty(TagSynchroX_persistent)
    TagSynchroX = TagSynchroX_persistent;
else
    TagSynchroX = num2str(rand(1));
    TagSynchroX_persistent = TagSynchroX;
end

if memeReponses && ~isempty(TagSynchroY_persistent)
    TagSynchroY = TagSynchroY_persistent;
else
    TagSynchroY = num2str(rand(1));
    TagSynchroY_persistent = TagSynchroY;
end


if GeometryType == cl_image.indGeometryType('LatLong')
    TagSynchroX = 'Longitude';
    TagSynchroY = 'Latitude';
end

%% Lecture de la donn�e

switch Version
    case 5 % ASCII
        [flag, this] = import_surfer5(this, nomFic, DataType, GeometryType, Unit, Carto, TagSynchroX, TagSynchroY);
    case 6
        [flag, this] = import_surfer6(this, nomFic, DataType, GeometryType, Unit, Carto, TagSynchroX, TagSynchroY);
    case 7
        [flag, this] = import_surfer7(this, nomFic, DataType, GeometryType, Unit, Carto, TagSynchroX, TagSynchroY);
end


function [flag, Version] = getVersion(nomFic)
flag    = 0;
Version = [];
fid = fopen(nomFic, 'r');
if fid == -1
    return
end
MagicCode = fread(fid, 4, 'char');
fclose(fid);
switch char(MagicCode')
    case 'DSAA'
        Version = 5;
    case 'DSBB'
        Version = 6;
    case 'DSRB'
        Version = 7;
    otherwise
        str1 = sprintf('La version du format du fichier "%s" n''a pas pu �tre reconnu.', nomFic);
        str2 = sprintf('The format version of "%s"could not be identified.', nomFic);
        my_warndlg(Lang(str1,str2), 1);
end
flag = 1;


function [flag, this] = import_surfer5(this, nomFic, DataType, GeometryType, Unit, Carto, TagSynchroX, TagSynchroY)
flag  = 0;
fid = fopen(nomFic, 'r');
if fid == -1
    return
end
Line = fgetl(fid);
if ~strcmp(Line, 'DSAA')
    return
end
Line = fgetl(fid);
X = sscanf(Line, '%d');
cols = X(1);
rows = X(2);
Line = fgetl(fid);
X = sscanf(Line, '%f');
xlo = X(1);
xhi = X(2);
Line = fgetl(fid);
X = sscanf(Line, '%f');
ylo = X(1);
yhi = X(2);
Line = fgetl(fid);
X = sscanf(Line, '%f');
zlo = X(1);
zhi = X(2);
z = NaN(rows, cols, 'single');
for k=1:rows
    Line = fgetl(fid);
    X = sscanf(Line, '%f');
    z(k,:) = X';
end
fclose(fid);
z(z < zlo) = NaN;
z(z > zhi) = NaN;
x = linspace(xlo, xhi, cols);
y = linspace(ylo, yhi, rows);
[~, Name] = fileparts(nomFic);
this = cl_image('Image', z, ...
    'Name',           Name, ...
    'Unit',            Unit, ...
    'x',               x, ...
    'y',               y, ...
    'XUnit',           'm', ...
    'YUnit',           'm', ...
    'TagSynchroX',     TagSynchroX, ...
    'TagSynchroY',     TagSynchroY, ...
    'ColormapIndex',   3, ...
    'DataType',        DataType, ...
    'GeometryType',    GeometryType, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'Surfer', ...
    'Carto',           Carto);
flag = 1;


function [flag, this] = import_surfer6(this, nomFic, DataType, GeometryType, Unit, Carto, TagSynchroX, TagSynchroY)
flag  = 0;
fid = fopen(nomFic, 'r');
if fid == -1
    return
end
MagicCode = fread(fid, 4, 'char');
if ~strcmp(char(MagicCode'), 'DSBB')
    return
end
cols = fread(fid, 1, 'int16');
rows = fread(fid, 1, 'int16');
xlo  = fread(fid, 1, 'double');
xhi  = fread(fid, 1, 'double');
ylo  = fread(fid, 1, 'double');
yhi  = fread(fid, 1, 'double');
zlo  = fread(fid, 1, 'double');
zhi  = fread(fid, 1, 'double');
z = fread(fid, [cols, rows], 'single');
z = z';
fclose(fid);
x = linspace(xlo, xhi, cols);
y = linspace(ylo, yhi, rows);
z(z < zlo) = NaN;
z(z > zhi) = NaN;
[~, Name] = fileparts(nomFic);
this = cl_image('Image', z, ...
    'Name',           Name, ...
    'Unit',            Unit, ...
    'x',               x, ...
    'y',               y, ...
    'XUnit',           'm', ...
    'YUnit',           'm', ...
    'TagSynchroX',     TagSynchroX, ...
    'TagSynchroY',     TagSynchroY, ...
    'ColormapIndex',   3, ...
    'DataType',        DataType, ...
    'GeometryType',    GeometryType, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'Surfer', ...
    'Carto',           Carto);
flag = 1;


function [flag, this] = import_surfer7(this, nomFic, DataType, GeometryType, Unit, Carto, TagSynchroX, TagSynchroY)
flag  = 0;
fid = fopen(nomFic, 'r');
if fid == -1
    return
end
MagicCode = fread(fid, 4, 'char');
if ~strcmp(char(MagicCode'), 'DSRB')
    return
end
Size    = fread(fid, 1, 'int32'); %#ok<NASGU> % 4
Version = fread(fid, 1, 'int32'); %#ok<NASGU> % 1

while 1
    Id = fread(fid, 4, 'char');
    if isempty(Id)
        break
    end
    Size = fread(fid, 1, 'int32');
    switch char(Id')
        case 'GRID' % '44495247'
            rows  = fread(fid, 1, 'int32');
            cols  = fread(fid, 1, 'int32');
            xlo   = fread(fid, 1, 'double');
            ylo   = fread(fid, 1, 'double');
            xSize = fread(fid, 1, 'double');
            ySize = fread(fid, 1, 'double');
            zlo   = fread(fid, 1, 'double');
            zhi   = fread(fid, 1, 'double');
            Rotation = fread(fid, 1, 'double'); %#ok<NASGU> % 0
            BlankValue = fread(fid, 1, 'double');
        case 'DATA' % '41544144'
            z = fread(fid, [cols, rows], 'double');
            z = z';
        otherwise
            str1 = sprintf('Section "%s" non lue dans import_surfer7, envoyez le fichier � JMA S.V.P', Id);
            str2 = sprintf('Section "%s" not read in import_surfer7, send your file to JMA please.', Id);
            my_warndlg(Lang(str1,str2), 1);
            fseek(fid, Size, 'cof');
    end
end
fclose(fid);
x = xlo + (0:(cols-1)) * xSize;
y = ylo + (0:(rows-1)) * ySize;
z(z == BlankValue) = NaN;
z(z < zlo) = NaN;
z(z > zhi) = NaN;
[~, Name] = fileparts(nomFic);
this = cl_image('Image', z, ...
    'Name',            Name, ...
    'Unit',            Unit, ...
    'x',               x, ...
    'y',               y, ...
    'XUnit',           'm', ...
    'YUnit',           'm', ...
    'TagSynchroX',     TagSynchroX, ...
    'TagSynchroY',     TagSynchroY, ...
    'ColormapIndex',   3, ...
    'DataType',        DataType, ...
    'GeometryType',    GeometryType, ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'Surfer', ...
    'Carto',           Carto);
flag = 1;
