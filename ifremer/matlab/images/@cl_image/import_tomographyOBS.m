%{
nomFic = 'I:\Tomographie\mi2.tomo';
[flag, a] = import_tomographyOBS(cl_image, nomFic)
SonarScope(a)
%}

function [flag, this] = import_tomographyOBS(~, nomFic, varargin)

% [varargin, Carto] = getPropertyValue(varargin, 'Carto', []);

this = cl_image.empty;
flag = 0;

%% Lecture du fichier

if ~exist(nomFic, 'file')
    return
end
% [Lon, Lat, Z, Speed] = textread(nomFic);
fid = fopen(nomFic);
if fid == -1
    return
end
C = textscan(fid, '%f %f %f %f %f');
fclose(fid);
Lon   = C{1};
Lat   = C{2};
X     = C{3};
Z     = C{4} * 1000;
Speed = C{5};
% figure; plot(Lon, '-*'); grid on;
% figure; plot(Lat, '-*'); grid on;
% figure; plot(Z, '-*'); grid on;
% figure; plot(Speed, '-*'); grid on;

%% Récupération de l'image

[b, ~, nc] = unique(Z);
nbPoints = length(Z);
nbCol = length(b);

[b, ~, nl] = unique(X);
nbRows = length(b);
Image = NaN(nbRows, nbCol, 'single');
Latitude = NaN(nbRows,1);
Longitude = NaN(nbRows,1);
for k=1:nbPoints
    iCol = nc(k);
    iLig = nl(k);
    Image(iLig,iCol) = Speed(k);
    Latitude(iLig)   = Lat(k);
    Longitude(iLig)  = Lon(k);
end
% Image = fliplr(Image);

% figure; imagesc(Image);

[nbRows, nbCol] = size(Image);
y = 1:nbRows;
x = 1:nbCol; % -Z(m) / 10;
% figure; imagesc(x, y, Image); axis xy;

%% Création de l'image

SonarDescription = cl_sounder('Sonar.Ident', 25);

[~, ImageName] = fileparts(nomFic);
this = cl_image('Image', Image, ...
    'Name',           ImageName, ...
    'Unit',                'Amp', ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               '"', ...
    'YUnit',               'Ping', ...
    'ColormapIndex',        3, ...
    'DataType',             cl_image.indDataType('Reflectivity'), ...
    'TagSynchroX',          ImageName, ...
    'TagSynchroY',          ImageName, ...
    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
    'InitialFileName',      nomFic, 'InitialFileFormat', '???', ...
    'SonarDescription',      SonarDescription);

PingCounter            = 1:nbRows;
Height                 = zeros(nbRows, 1, 'single');
Immersion              = zeros(nbRows, 1, 'single');
SonarSurfaceSoundSpeed = 1500 * ones(nbRows, 1, 'single');
deltaZ = (Z(2)-Z(1));
SamplingFrequency      = SonarSurfaceSoundSpeed / (2*deltaZ); %Z(m) * ones(nbRows, 1, 'single');
Time                   = cl_time('timeIfr', ones(1, nbRows), 1:nbRows);

this = set(this, 'SonarPingCounter',        PingCounter);
this = set(this, 'SonarImmersion',          Immersion);
this = set(this, 'SonarHeight',             Height(:));
% this = set(this, 'SonarHeading',       Cap(:));
% this = set(this, 'SonarSpeed',         Vitesse(:));
this = set(this, 'SonarFishLatitude',       Latitude(:));
this = set(this, 'SonarFishLongitude',      Longitude(:));
this = set(this, 'SampleFrequency',         SamplingFrequency);
this = set(this, 'SonarSurfaceSoundSpeed',  SonarSurfaceSoundSpeed);
this = set(this, 'SonarTime', Time);


% if isempty(Carto)
%     Carto = getDefinitionCarto(this);
% end
% this = set(this,  'Carto', Carto);
this = update_Name(this);

flag = 1;


