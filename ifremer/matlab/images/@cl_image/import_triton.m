% Importation d'un fichier TRITON (*.class)
%
% Syntax
%   [a, flag] = import_triton(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFicIn = getNomFicDatabase('KBMA0504.class')
%   a = import_triton(cl_image, nomFicIn);
%   imagesc(a)
%
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_triton(this, nomFic)%#ok

fid = fopen(nomFic);
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.class']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        this = [];
        flag = 0;
        return
    end
end

% Ligne 1
line = fgets(fid);
sub = strfind(line, '''');
sub = sub(1):sub(2);
line(sub) = 'x';
mots = strsplit(line);

% [mots, VERSION] = getPropertyValue(mots, 'VERSION', []);
% [mots, COMPRESSED] = getPropertyValue(mots, 'COMPRESSED', []);
% [mots, CONTENT] = getPropertyValue(mots, 'CONTENT', []);
% [mots, SCALETYPE] = getPropertyValue(mots, 'SCALETYPE', []);
[mots, RESOLUTION] = getPropertyValue(mots, 'RESOLUTION', []);
[mots, NAN]        = getPropertyValue(mots, 'NAN', []);
% [mots, GRID_VALUE_TYPE] = getPropertyValue(mots, 'GRID_VALUE_TYPE', []);
% [mots, SURVEY_NAME] = getPropertyValue(mots, 'SURVEY_NAME', []);
% [mots, DATE] = getPropertyValue(mots, 'DATE', []);
% [mots, FILE_TYPE] = getPropertyValue(mots, 'FILE_TYPE', []);
% [mots, DATUM] = getPropertyValue(mots, 'DATUM', []);
% [mots, HALF_AX] = getPropertyValue(mots, 'HALF_AX', []);
% [mots, INV_FLAT] = getPropertyValue(mots, 'INV_FLAT', []);
[mots, LAT_LL]  = getPropertyValue(mots, 'LAT_LL', []);
[mots, LONG_LL] = getPropertyValue(mots, 'LONG_LL', []);
% [mots, LAT_UR] = getPropertyValue(mots, 'LAT_UR', []);
% [mots, LONG_UR] = getPropertyValue(mots, 'LONG_UR', []);
[mots, COLS] = getPropertyValue(mots, 'COLS', []);
[mots, ROWS] = getPropertyValue(mots, 'ROWS', []); %#ok<ASGLU>
% [mots, NO_DISC] = getPropertyValue(mots, 'NO_DISC', []);

flag = 1;
while flag
    line = fgets(fid);
    flag = ~isequal(line, newline);
end

cols      = str2double(COLS);
rows      = str2double(ROWS);
xycell    = str2double(RESOLUTION);
nullvalue = str2double(NAN);
swx       = str2double(LONG_LL);
swy       = str2double(LAT_LL);

nex = swx + (cols-1) * xycell;
ney = swy + (rows-1) * xycell;

z = fscanf(fid, '%f', [cols, rows]);
z(z == nullvalue) = NaN;

fclose(fid);

x = linspace(swx, nex, cols);
y = linspace(swy, ney, rows);

this = cl_image('Image', z', ...
    'Name',            'poseidon DTM', ...
    'x',               x, ...
    'y',               y, ...
    'XUnit',           'm', ...
    'YUnit',           'm', ...
    'ColormapIndex',   3, ...
    'DataType',        cl_image.indDataType('Bathymetry'), ...
    'GeometryType',    cl_image.indGeometryType('GeoYX'), ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'poseidon');

flag = 1;

