% Importation d'un fichier MAPINFO (*.vmascii)
%
% Syntax
%   [a, flag] = import_vmascii(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   a    : Instance de cl_image
%   flag : 1 si lecture reussie, 0 sinon
%
% Examples
%   nomFicIn = getNomFicDatabase('KBMA0504.vmascii')
%   a = import_vmascii(cl_image, nomFicIn);
%   imagesc(a)
%
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [this, flag] = import_vmascii(this, nomFic)

fid = fopen(nomFic);
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.vmascii']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end

% Ligne 1
line = fgets(fid);
mots = strsplit(line);
[mots, X] = getPropertyValue(mots, 'ncols', []);
cols = str2double(X);

% Ligne 2
line = fgets(fid);
mots = strsplit(line);
[mots, X] = getPropertyValue(mots, 'nrows', []);
rows = str2double(X);

% Ligne 3
line = fgets(fid);
mots = strsplit(line);
[mots, X] = getPropertyValue(mots, 'xllcenter', []);
swx = str2double(X);

% Ligne 4
line = fgets(fid);
mots = strsplit(line);
[mots, X] = getPropertyValue(mots, 'yllcenter', []);
swy = str2double(X);

% Ligne 5
line = fgets(fid);
mots = strsplit(line);
[mots, X] = getPropertyValue(mots, 'cellsize', []);
xycell = str2double(X);

% Ligne 6
line = fgets(fid);
mots = strsplit(line);
[mots, X] = getPropertyValue(mots, 'NODATA_value', []);
NODATA_value = str2double(X);

z = fscanf(fid, '%f', [cols, rows]);
z(z == NODATA_value) = NaN;

fclose(fid);

nex = swx + (cols-1) * xycell;
ney = swy + (rows-1) * xycell;
x = linspace(swx, nex, cols);
y = linspace(swy, ney, rows);

this = cl_image('Image', flipud(z'), ...
    'Name',             'Depth', ...
    'x',                x, ...
    'y',                y, ...
    'XUnit',           'm', ...
    'YUnit',           'm', ...
    'ColormapIndex',   3, ...
    'DataType',        cl_image.indDataType('Bathymetry'), ...
    'GeometryType',    cl_image.indGeometryType('GeoYX'), ...
    'InitialFileName', nomFic, 'InitialFileFormat', 'VMASCII');

flag = 1;

