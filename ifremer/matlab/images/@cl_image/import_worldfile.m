% Lecture d'un fichier WorldFile au format ESRI, associ� � une image tif,
% jpeg, png, bmp, etc(http://en.wikipedia.org/wiki/World_file), et mise �
% jour des propri�t�s de l'image (cl_image)
% Utilise des fonctions mapping toolbox
%
% Syntax
%   this = import_worldfile(this, nomFic)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   CLim : Rehaussement de contraste
%
% Examples
%   a = cl_image('Image', Lena);
%   nomFic = [tempname '.tif']
%   export_tif(a, nomFic)
%   visuExterne(nomFic)
%
% See also cl_image, export_tif, export_gif
% Authors : GLA
% ----------------------------------------------------------------------------

function this = import_worldfile(this, nomFic)

try
    % V�rification de l'existence de la toolbox.
%     ver('map');
    worldfilename = getworldfilename(nomFic);
    
    if ~exist(worldfilename, 'file')
        str1 = 'Aucun fichier de g�or�f�rencement "ESRI World File" pour cette image';
        str2 = 'No "ESRI World file" for this image';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoESRI World file');
        return
    end
    
    nbRows    = this.nbRows;
    nbColumns = this.nbColumns;
    
%     refMat = worldfileread(worldfilename);
    refMat = worldfileread(worldfilename, 'geographic', [nbRows, nbColumns]); % Modif JMA le 31/05/2021
    
    [lat1, lon1]     = pix2latlon(refMat, 1, 1);
    [lat2, lon2]     = pix2latlon(refMat, 2, 2);
    [latEnd, lonEnd] = pix2latlon(refMat, nbRows, nbColumns);
    
    dlat = lat2-lat1;
    dlon = lon2-lon1; %#ok<NASGU>
    
    % Mise � jour des infos de l'image
    set(this, 'X', linspace(lon1, lonEnd, nbColumns));
    % Forcage (temporaire) de la g�om�trie de ce type d'image en LatLong.
    % Les World File peuvent �tre en XY.
    this.GeometryType = cl_image.indGeometryType('LatLong');
    this.TagSynchroX = 'Longitude';
    this.TagSynchroY = 'Latitude';
    this.XUnit = 'deg';
    this.YUnit = 'deg';
    
    set(this, 'Y', linspace(lat1, latEnd, nbRows));
    % Repositionnement pou
    if dlat < 0
        set(this, 'YDir', 1); %Up
    else
        set(this, 'YDir', 2); %Down
    end
        
    str1 = 'Un fichier de g�or�f�rencement "ESRI World File" a �galement �t� lu';
    str2 = 'An "ESRI World file" has also been read for this image';
    my_warndlg(Lang(str1,str2), 0);
catch
    % Vide
end

