function [flag, this] = import_xml(nomFic, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

this = [];

Data = xml_mat_read(nomFic);
flag = xml_check_signatures(Data, nomFic, 'Image');
if ~flag
    return
end
% gen_object_display(Data)

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(nomFic);
nomDir2 = fullfile(nomDir, nom);
if ~exist(nomDir2, 'dir')
    str1 = sprintf('Le r�pertoire "%s" n''existe pas.\n', nomDir);
    str2 = sprintf('Folder "%s" does not exist.\n', nomDir);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

if isfield(Data, 'ErMapper')
    ErMapper = Data.ErMapper;
    if ischar(ErMapper)
        ErMapper = strcmp(ErMapper, 'true');
    end
else
    ErMapper = 0;
end

if Memmapfile == -1
    MemmapfileSignal = 0;
else
    MemmapfileSignal = abs(Memmapfile);
end
for k=1:length(Data.Signals)
    [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(k), Data.Dimensions, 'Memmapfile', MemmapfileSignal);
    if ~flag
        return
    end
    Signaux.(Data.Signals(k).Name) = Signal;
end

%% Lecture des fichiers binaires des images

MemmapfileImage = abs(Memmapfile);
for k=1:length(Data.Images)
    [flag, Image] = XMLBinUtils.readSignal(nomDir, Data.Images(k), Data.Dimensions, ...
        'Memmapfile', MemmapfileImage, 'ErMapper', ErMapper);
    if ~flag
        return
    end
end

%% Recup�ration de la carto

if isempty(Data.nomFicCarto)
    Carto = [];
else
    nomFicCarto = fullfile(nomDir, Data.nomFicCarto);
    % Pour rattraper une erreur d'extension faite dans l'export
    if strcmp(nomFicCarto(end-3:end), '.txt') && ~exist(nomFicCarto, 'file')
        nomFicCarto(end-3:end) = '.xml';
    end
    C0 = cl_carto([]);
    Carto = import_xml(C0, nomFicCarto);
end

%% Cr�ation de l'objet

DataType       = cl_image.indDataType(      Data.DataType);
GeometryType   = cl_image.indGeometryType(  Data.GeometryType);
XDir           = cl_image.indXDir(          Data.XDir);
YDir           = cl_image.indYDir(          Data.YDir);
Video          = cl_image.indVideo(         Data.Video);
ColormapIndex  = cl_image.indColormapIndex( Data.ColormapIndex);
ImageType      = cl_image.indImageType(     Data.ImageType);
SpectralStatus = cl_image.indSpectralStatus(Data.SpectralStatus);

x = Signaux.x(:);
y = Signaux.y(:);

if ((GeometryType == cl_image.indGeometryType('LatLong')) || (GeometryType == cl_image.indGeometryType('GeoYX'))) ...
        && (~isequal(x, centrage_magnetique(x)) || ~isequal(y, centrage_magnetique(y)))
    str1 = sprintf('Les coordonn�es de l''image "%s" ne sont pas centr�es sur une r�gle magn�tique. Ceci est peut-�tre normal mais cela peut entra�ner des dysfonctionnements car toutes les images qui seront d�duites de celle-ci auront leur coordonn�es  qui seront recentr�es automatiquement. Un glissement d''un pixel entre images serait explicable par ce fait. Je vous conseille de faire une extraction de cette image et de r�pondre "Oui" � la question "Voulez vous recentrer les coordonn�es".', nom);
    str2 = sprintf('The coordinates of "%s" are not centered. It can produce some bugs when working with deduced images that will be automatically centered (a shift of 1 pixel between images could be possible). I recommend that you do an "Extraction" of this image.', nom);
    my_warndlg(Lang(str1,str2), 0);
end

if ErMapper
    AlreadyFlipped = xor((y(end) > y(1)), (YDir == 1));
    if ~AlreadyFlipped
        y = flipud(y);
    end
end
% end

if isnumeric(Data.TagSynchroX)
    Data.TagSynchroX = num2str(Data.TagSynchroX);
end
if isnumeric(Data.TagSynchroY)
    Data.TagSynchroY = num2str(Data.TagSynchroY);
end
if isnumeric(Data.TagSynchroContrast)
    Data.TagSynchroContrast = num2str(Data.TagSynchroContrast);
end

% Rajout� par JMA le 30/01/2014, mission BICOSE.

if isfield(Data, 'StatValues') && ~isempty(Data.StatValues) ...
        && isfield(Signaux, 'HistoCentralClasses') && ~isempty(Signaux.HistoCentralClasses)  && ~isempty(Signaux.HistoValues)
    PredefinedStats{1} = Data.StatValues ;
    PredefinedStats{2} = Signaux.HistoCentralClasses ;
    PredefinedStats{3} = Signaux.HistoValues;
else
    PredefinedStats = [];
end

% Cr�ation de l'instance cl_image

if length(Signaux.ColormapCustom) == 0 %#ok<ISMT>
    ColormapCustom = [];
else
    ColormapCustom = Signaux.ColormapCustom(:,:);
end

this = cl_image('Image',    Image, ...
    'x',                    Signaux.x(:), ...
    'y',                    y, ...
    'DataType',             DataType, ...
    'ImageType',            ImageType, ...
    'GeometryType',         GeometryType, ...
    'XLabel',               Data.XLabel, ...
    'XUnit',                Data.XUnit, ...
    'XDir',                 XDir, ...
    'YLabel',               Data.YLabel, ...
    'YUnit',                Data.YUnit, ...
    'YDir',                 YDir, ...
    'TagSynchroX',          Data.TagSynchroX, ...
    'TagSynchroY',          Data.TagSynchroY, ...
    'TagSynchroContrast',   Data.TagSynchroContrast, ...
    'ValNaN',               Data.ValNaN, ...
    'ColormapIndex',        ColormapIndex, ...
    'CLim',                 Data.CLim, ...
    'Video',                Video, ...
    'Name',                 Data.Title, ...
    'Unit',                 Data.Unit, ...
    'Carto',                Carto, ...
    'InitialFileName',      Data.InitialFileName, ...
    'InitialFileFormat',    Data.InitialFileFormat, ...
    'InitialImageName',     Data.InitialImageName, ...
    'SpectralStatus',       SpectralStatus, ...
    'SaveParamsIfFFT',      Data.SaveParamsIfFFT, ...
    'OriginColorbar',       Data.OriginColorbar, ...
    'ColormapCustom',       ColormapCustom, ...
    'Comments',             Data.Comments, ...
    'PredefinedStats',      PredefinedStats);
if ~isempty(Signaux.ContourValues) && ~isempty(Signaux.ContourValues(:)) % TODO : rustine pour corriger bug import session Alexandre Dano
    this = set_ContourValues(this, Signaux.ContourValues(:,:));
end

if isfield(Data, 'TagSynchroXScale')
    this.TagSynchroXScale = Data.TagSynchroXScale;
end
if isfield(Data, 'TagSynchroYScale')
    this.TagSynchroYScale = Data.TagSynchroYScale;
end

if isfield(Data, 'History')
    for k=length(Data.History):-1:1
        this.History(k).Function   = Data.History(k).Function;
        this.History(k).Parameters = Data.History(k).Parameters;
        if isfield(Data.History(k), 'ImageName') % Anciens fichiers cr��s avant le 28/02/2022
            this.History(k).Name = Data.History(k).ImageName;
        else
            this.History(k).Name = Data.History(k).Name;
        end
    end
end

% imagesc(this)

% Water column

if isfield(Data, 'SampleBeamData')
    this.Sonar.SampleBeamData = Data.SampleBeamData;
    if isfield(this.Sonar.SampleBeamData, 'Time')
        this.Sonar.SampleBeamData.Time = cl_time('timeMat', this.Sonar.SampleBeamData.Time);
    end
end

if isfield(Data, 'Sonar')
    if isfield(Data.Sonar, 'RawDataResol')
        this.Sonar.RawDataResol = Data.Sonar.RawDataResol;
    end
    if isfield(Data.Sonar, 'ResolutionD')
        this.Sonar.ResolutionD = Data.Sonar.ResolutionD;
    end
    if isfield(Data.Sonar, 'ResolutionX')
        this.Sonar.ResolutionX = Data.Sonar.ResolutionX;
    end
    
    if isfield(Data.Sonar, 'TVG')
        this.Sonar.TVG = Data.Sonar.TVG;
    end
    if isfield(Data.Sonar, 'DiagEmi')
        this.Sonar.DiagEmi = Data.Sonar.DiagEmi;
    end
    if isfield(Data.Sonar, 'DiagRec')
        this.Sonar.DiagRec = Data.Sonar.DiagRec;
    end
    if isfield(Data.Sonar, 'AireInso')
        this.Sonar.AireInso = Data.Sonar.AireInso;
    end
    if isfield(Data.Sonar, 'NE')
        this.Sonar.NE = Data.Sonar.NE;
    end
    if isfield(Data.Sonar, 'SH')
        this.Sonar.SH = Data.Sonar.SH;
    end
    if isfield(Data.Sonar, 'GT')
        this.Sonar.GT = Data.Sonar.GT;
    end
    if isfield(Data.Sonar, 'BS')
        this.Sonar.BS = Data.Sonar.BS;
    end
    if isfield(Data.Sonar, 'Ship')
        this.Sonar.Ship = Data.Sonar.Ship;
    end
end

%% Importation des  Signaux

Names = fieldnames(Signaux);
for k=1:length(Names)
    switch Names{k}
        case {'x'; 'y'; 'Colormap'; 'ColormapCustom'; 'ContourValues'; 'HistoValues'; 'HistoCentralClasses'}
        case 'TxAlongAngle' % cas de MNT Reson
        otherwise
            if ErMapper
                S = Signaux.(Names{k});
                if isa(S, 'cl_time')
                    if AlreadyFlipped
                        S = S.timeMat;
                    else
                        S = flipud(S.timeMat);
                    end
                    S = cl_time('timeMat', S);
                else
                    if ~AlreadyFlipped
                        S = flipud(S(:,:));
                    end
                end
                
                Names{k} = strrep(Names{k}, 'SonarFrequency', 'Frequency');
                
                this = set(this, ['Sonar' Names{k}], S);
            else
                % TODO : Pour corriger une mal donne : faire le bilan de
                % cette affaire
                if strfind(Names{k}, 'Sonar')
                    this = set(this, Names{k}, Signaux.(Names{k})); % Maintenant pour SonarFrequency
                else
                    this = set(this, ['Sonar' Names{k}], Signaux.(Names{k})); % Avant
                end
            end
    end
end

%% Importation des r�gions d'int�r�t

if ~isempty(Data.nomFicROI)
    nomFicROI = fullfile(nomDir, Data.nomFicROI);
    if exist(nomFicROI, 'file')
        [flag, this] = ROI_import_xml(this, nomFicROI);
    end
end

%% Importation de la configuration sonar
% A faire TODO

if ~isempty(Data.nomFicSonarDescription)
    nomFicSonarDescription = fullfile(nomDir, Data.nomFicSonarDescription);
    %     warning('off','MATLAB:elementsNowStruc')
    pppp = load(nomFicSonarDescription);
    %     [msgstr, msgid] = lastwarn
    %     warning('on','MATLAB:elementsNowStruc')
    SonarDescription = pppp.Sonar;
    switch class(SonarDescription)
        case 'cl_sounder'
            this.Sonar.Desciption = pppp.Sonar;
%         case 'cl_sounder_new'
%             % TODO : cette classe n'a plus lieu d'�tre car elle est remplac�e par
%             % cl_sounder. Par contre, il faut la conserver pour pouvoir r�cup�rer des
%             % instances stock�es dans des .mat (voir cl_image/import_xml).
%             
%             SonarIdent = get(SonarDescription, 'SonarIdent');
%             if SonarIdent == 0
%                 S = cl_sounder;
%             else
%                 S = cl_sounder('SonarIdent', SonarIdent);
%             end
%             this.Sonar.Desciption = S;
        case 'struct'
            % Surement de tr�s anciennes donn�es
        otherwise
            %             my_breakpoint
    end
    clear pppp
end

%% Importation de la bathy-c�l�rim�trie

if ~isempty(Data.nomFicBathyCel)
    nomFicBathyCel = fullfile(nomDir, Data.nomFicBathyCel);
    this.Sonar.BathyCel = loadmat(nomFicBathyCel, 'nomVar', 'BathyCel');
end

%% Importation des signatures texturales

if ~isempty(Data.nomFicTexture)
    nomFicTexture = fullfile(nomDir, Data.nomFicTexture);
    this.Texture = loadmat(nomFicTexture, 'nomVar', 'Texture');
end

%% Importation des signaux verticaux nouvelle g�n�ration

if isfield(Data, 'nomFicSignalsVert') && ~isempty(Data.nomFicSignalsVert)
    nomFicSignalsVert = fullfile(nomDir, Data.nomFicSignalsVert);
    this.SignalsVert = loadmat(nomFicSignalsVert, 'nomVar', 'SignalsVert');
end

%% Importation des courbes statistiques

if ~isempty(Data.nomFicCurves)
    nomFicCurves = fullfile(nomDir, Data.nomFicCurves);
    [~, ~, Ext] = fileparts(nomFicCurves);
    switch Ext
        case '.mat'
            if exist(nomFicCurves, 'file')
                [flag, this] = importCourbesStats(this, nomFicCurves);
            end
        case '.xml' % Historique : pour r�cup�rer anciens fichiers sauvegard�s
            if exist(nomFicCurves, 'file')
                [flag, this] = import_xml_courbesStats(this, nomFicCurves);
            end
    end
end
