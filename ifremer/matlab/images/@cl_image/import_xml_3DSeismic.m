% % Cette fonction n'a plus l'air d'�tre utile
% nomFicXml = ???; % TODO : retrouver un fichier
% [flag, a] = import_xml_3DSeismic(cl_image, nomFicXml, 'Memmapfile', 1);
 %% Voir import_xml_WaterColumnCube3D

function [flag, a] = import_xml_3DSeismic(~, nomFicXml, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

a = cl_image.empty;

%% Lecture du fichier XML d�crivant la donn�e 

[flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', -1);
if ~flag
    return
end

flag = xml_check_signatures(Data, nomFicXml, '3DSeismic');
if ~flag
    return
end
% gen_object_display(Data)

%% Cr�ation de l'objet

[nSlices, nPings]  = size(Data.Latitude);
% nbZ = size(Data.Reflectivity_xyz, 3);

TagSynchroX = num2str(rand(1));
TagSynchroY = num2str(rand(1));
TagSynchroContrast = num2str(rand(1));

x = 1:nPings; % TODO trouver le pas m�trique
y = 1:nSlices; % TODO trouver le pas m�trique
a = cl_image('Image', Data.Latitude, 'Name', 'Latitude', 'Unit', 'deg', ...
    'x', x, 'y', y, 'XUnit', 'm', 'YUnit', 'm', ...
    'DataType', cl_image.indDataType('Latitude'), ...
    'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY, ...
    'ColormapIndex', 3, 'InitialFileName', nomFicXml, 'InitialFileFormat', '3DSeismic');

a(2) = cl_image('Image', Data.Longitude, 'Name', 'Longitude', 'Unit', 'deg', ...
    'XUnit', 'm', 'YUnit', 'm', ...
    'x', x, 'y', y, ...
    'DataType', cl_image.indDataType('Longitude'), ...
    'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY, ...
    'ColormapIndex', 3, 'InitialFileName', nomFicXml, 'InitialFileFormat', '3DSeismic');

y = Signaux.z;
TagSynchroY = num2str(rand(1));
hw = create_waitbar('Importing Bloc3D_dim2', 'N', nbImages);
for k=1:nSlices
    my_waitbar(k, nSlices, hw)
    Name = sprintf('Reflectivity - Slice %d', k);
    a(k+2) = cl_image('Image', Data.Reflectivity_zxy(:,:,k), 'Name', Name, 'Unit', '?', ...
        'x', x, 'y', y, 'YUnit', '?', 'XUnit', '?', ...
        'DataType', cl_image.indDataType('Reflectivity'), ...
        'TagSynchroY', TagSynchroY, 'TagSynchroX', TagSynchroX, 'TagSynchroContrast', TagSynchroContrast, ...
        'ColormapIndex', 2, 'InitialFileName', nomFicXml, 'InitialFileFormat', 'ImagesAlongNavigation');
end
my_close(hw, 'MsgEnd')
% SonarScope(a)
