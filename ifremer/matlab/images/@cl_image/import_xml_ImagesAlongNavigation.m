% TODO : le d�veloppement de cette fonction n'est pas termin� : manque des
% signaux et la g�n�ralisation des noms de r�pertoires des PNG, cas de
% matrices, etc ...

function [flag, a] = import_xml_ImagesAlongNavigation(~, nomFic, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 1); %#ok<ASGLU>

a = cl_image.empty;

%% Lecture du fichier XML d�crivant la donn�e 

[flag, Data] = XMLBinUtils.readGrpData(nomFic, 'Memmapfile', -1);
if ~flag
    return
end

%% Check the signatures

flag = xml_check_signatures(Data, nomFic, 'ImagesAlongNavigation');
if ~flag
    return
end

%% Cr�ation de l'objet

[nomDir, nom] = fileparts(nomFic);
Time = Data.Time;

% TODO : il faudrait mettre un TAG dans le .xml du genre Support=Bin ou PNG

if isfield(Data, 'Reflectivity')
    [nDepth, nPings, nbImages]  = size(Data.Reflectivity);
else
    nbImages = length(Data.SliceExists);
    nomFicTif = fullfile(nomDir, nom, sprintf('%05d.tif', 1));
    if ~exist(nomFicTif, 'file')
        nomFicTif = fullfile(nomDir, nom, sprintf('%03d', 0), sprintf('%05d.tif', 1));
    end
    I = imread(nomFicTif);
    [nDepth, nPings]  = size(I);
end

TagSynchroX = num2str(rand(1));
TagSynchroY = num2str(rand(1));
TagSynchroContrast = num2str(rand(1));

% Si nbImages=1 c'est une image de subbottom
% Si nbImages>1 c'est une image de Water Column

y = 1:nPings;
% if nbImages ~= 1

% TODO : tout ceci doit �tre mis au carr� : on ne doit pas faire appel � ce
% genre de proc�d�
k2 = 0;
if isfield(Data, 'LatitudeNadir')
    k2 = k2 + 1;
    x = double(Data.AcrossDistance(:,:));
    a(k2) = cl_image('Image', Data.LatitudeTop', 'Name', 'LatitudeTop', 'Unit', 'deg', ...
        'YLabel', 'Ping', 'YUnit', '#', 'XLabel', 'Across Distance', 'XUnit', 'm', ...
        'x', x, 'y', y, ...
        'DataType', cl_image.indDataType('Latitude'), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'), ...
        'TagSynchroY', TagSynchroY, ...
        'ColormapIndex', 3, 'InitialFileName', nomFic, 'InitialFileFormat', 'ImagesAlongNavigation');
    a(k2) = set(a(k2), 'SonarTime', Time);
    a(k2) = set(a(k2), 'SonarPingCounter', Data.PingNumber);
    a(k2) = set(a(k2), 'SonarFishLatitude',  Data.LatitudeNadir');
    a(k2) = set(a(k2), 'SonarFishLongitude', Data.LongitudeNadir');
    
    k2 = k2 + 1;
    a(k2) = cl_image('Image', Data.LongitudeTop', 'Name', 'LongitudeTop', 'Unit', 'deg', ...
        'YLabel', 'Ping', 'YUnit', '#', 'XLabel', 'Across Distance', 'XUnit', 'm', ...
        'x', x, 'y', y, ...
        'DataType', cl_image.indDataType('Longitude'), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'), ...
        'TagSynchroY', TagSynchroY, ...
        'ColormapIndex', 3, 'InitialFileName', nomFic, 'InitialFileFormat', 'ImagesAlongNavigation');
    a(k2) = set(a(k2), 'SonarTime', Time);
    a(k2) = set(a(k2), 'SonarPingCounter', Data.PingNumber);
    a(k2) = set(a(k2), 'SonarFishLatitude',  Data.LatitudeNadir');
    a(k2) = set(a(k2), 'SonarFishLongitude', Data.LongitudeNadir');
end

if nbImages == 1
    ColormapIndex = 2; % Certainement une image de sondeur de s�diment
else
    ColormapIndex = 3; % Certainement une image de WC le long de la navigation
end
for k1=1:nbImages
    k2 = k2 + 1;
    x = linspace(double(max(Data.DepthTop)), double(min(Data.DepthBottom)), nDepth);
    Distance = Data.AcrossDistance(k1);
    Name = sprintf('Reflectivity - Slice %s m', num2str(Distance));
    
    if isfield(Data, 'Reflectivity')
        I = Data.Reflectivity(:,:,k1)';
    else
        nomFicTif = fullfile(nomDir, nom, sprintf('%05d.tif', k1));
        if ~exist(nomFicTif, 'file')
            nomFicTif = fullfile(nomDir, nom, sprintf('%03d', floor(k1/100)), sprintf('%05d.tif', k1));
        end
        I = imread(nomFicTif);
        I = fliplr(I');
    end
    
    I(I == 0) = NaN;
    a(k2) = cl_image('Image', I, 'Name', Name, 'Unit', '?', ...
        'x', x, 'y', y, 'XDir', 2, ...
        'YLabel', 'Ping', 'YUnit', '#', 'XLabel', 'Depth', 'XUnit', 'm', ...
        'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', cl_image.indGeometryType('PingDepth'), ...
        'TagSynchroY', TagSynchroY, 'TagSynchroX', TagSynchroX, 'TagSynchroContrast', TagSynchroContrast, ...
        'ColormapIndex', ColormapIndex, 'InitialFileName', nomFic, 'InitialFileFormat', 'ImagesAlongNavigation');
    a(k2) = set(a(k2), 'SonarTime', Time);
    a(k2) = set(a(k2), 'SonarPingCounter', Data.PingNumber);
    a(k2) = set(a(k2), 'SonarFishLatitude',  Data.LatitudeTop(1,:)');
    a(k2) = set(a(k2), 'SonarFishLongitude', Data.LongitudeTop(1,:)');
end
% SonarScope(a)
