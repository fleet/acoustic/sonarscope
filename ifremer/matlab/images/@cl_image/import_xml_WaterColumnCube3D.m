% nomFic = 'D:\Temp\SPFE\ATraiter\L21_ForTEST\WC-3DMatrix\0021_20180306_210305_SimonStevin_WCCubeRaw_01.xml';
% [flag, a] = import_xml_WaterColumnCube3D(cl_image, nomFic, 'subImage', 100:200:1400);
% SonarScope(a)

function [flag, a] = import_xml_WaterColumnCube3D(~, nomFic, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', -1);
[varargin, subImage]   = getPropertyValue(varargin, 'subImage',   []); %#ok<ASGLU>

a  = cl_image.empty;

%% Lecture du fichier XML

[flag, Data] = XMLBinUtils.readGrpData(nomFic, 'Memmapfile', Memmapfile);
if ~flag
    return
end

%% Check the signature

flag = xml_check_signatures(Data, nomFic, 'WaterColumnCube3D');
if ~flag
    return
end

%% Cr�ation de l'objet

Time = Data.Time;

[nDepth, nbColumns, nPings]  = size(Data.Reflectivity);

TagSynchroX = num2str(rand(1));
TagSynchroY = num2str(rand(1));

y = 1:nPings;
x = double(Data.AcrossDistance);
a = cl_image('Image', Data.LatitudeTop, 'Name', 'LatitudeTop', 'Unit', 'deg', ...
    'YLabel', 'Ping', 'YUnit', '#', 'XLabel', 'Across Distance', 'XUnit', 'm', ...
    'x', x, 'y', y, ...
    'DataType', cl_image.indDataType('Latitude'), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'), ...
    'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY, ...
    'ColormapIndex', 3, 'InitialFileName', nomFic, 'InitialFileFormat', 'ImagesAlongNavigation');
a = set(a, 'SonarTime', Time);
a = set(a, 'SonarPingCounter', Data.iPing);
a = set(a, 'SonarFishLatitude',  Data.latCentre);
a = set(a, 'SonarFishLongitude', Data.lonCentre);

a(2) = cl_image('Image', Data.LongitudeTop, 'Name', 'LongitudeTop', 'Unit', 'deg', ...
    'YLabel', 'Ping', 'YUnit', '#', 'XLabel', 'Across Distance', 'XUnit', 'm', ...
    'x', x, 'y', y, ...
    'DataType', cl_image.indDataType('Longitude'), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'), ...
    'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY, ...
    'ColormapIndex', 3, 'InitialFileName', nomFic, 'InitialFileFormat', 'ImagesAlongNavigation');
a(2) = set(a(2), 'SonarTime', Time);
a(2) = set(a(2), 'SonarPingCounter', Data.iPing);
a(2) = set(a(2), 'SonarFishLatitude',  Data.latCentre);
a(2) = set(a(2), 'SonarFishLongitude', Data.lonCentre);

ColormapIndex = 3; % Certainement une image de WC le long de la navigation
TagSynchroContrast = num2str(rand(1));
x = linspace(double(Data.Depth(1)), 0, nDepth);

%% S�lection des layers

if isempty(subImage)
    str = cell(nbColumns,1);
    for k=1:nbColumns
        str{k} = sprintf('Reflectivity - Slice %s m', num2str(Data.AcrossDistance(k)));
    end
    [subImage, flag] = my_listdlg('Images � d�truire', str, 'InitialValue', 1:nbColumns);
    if ~flag
        return
    end
else
    subImage(subImage > nbColumns) = [];
end
if isempty(subImage)
    flag = 0;
    return
end

%% Cr�ation des images

TagSynchroX = num2str(rand(1));
nbImages = length(subImage);
str1 = 'Lecture des images';
str2 = 'Read images';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    k2 = k + 2;
    kImage = subImage(k);
    Name = sprintf('Reflectivity - Slice %s m', num2str(Data.AcrossDistance(kImage)));
    I = squeeze(Data.Reflectivity(:,kImage,:));
    I = I';
    
    Height = NaN(nPings, 1, 'single');
    for k3=1:nPings
        ind = find(~isnan(I(k3,:)), 1, 'first');
        if ~isnan(ind)
            Height(k3) = x(ind);
        end
    end
    
    a(k2) = cl_image('Image', I, 'Name', Name, 'Unit', '?', ...
        'x', x, 'y', y, 'XDir', 2, ...
        'YLabel', 'Ping', 'YUnit', '#', 'XLabel', 'Depth', 'XUnit', 'm', ...
        'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', cl_image.indGeometryType('PingDepth'), ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY, 'TagSynchroContrast', TagSynchroContrast, ...
        'ColormapIndex', ColormapIndex, 'InitialFileName', nomFic, 'InitialFileFormat', 'ImagesAlongNavigation');
    a(k2) = set(a(k2), 'SonarTime', Time);
    a(k2) = set(a(k2), 'SonarPingCounter',   Data.iPing);
    a(k2) = set(a(k2), 'SonarFishLatitude',  Data.LatitudeTop( :,kImage));
    a(k2) = set(a(k2), 'SonarFishLongitude', Data.LongitudeTop(:,kImage));
    a(k2) = set(a(k2), 'SonarHeight',        Height);
    a(k2).CLim = Data.CLim;
    a(k2) = Ram2Virtualmemory(a(k2), 'LogSilence', 1);
end
my_close(hw, 'MsgEnd')
% SonarScope(a)
