% Import the statistical curves in an image
%
% Syntax
%   [flag, a] = import_xml_courbesStats(a, CurvesFilename, ...)
%
% Input Arguments
%   a              : An instance of cl_image
%   CurvesFilename : File name of the statistical curves
%
% Name-Value Pair Arguments
%   PrioriteLectureASCII : ??? TODO : anglais !
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : The instance of cl_image with the statistical curves
%
% Examples
%     nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
%     allKM = cl_simrad_all('nomFic', nomFic);
%     [flag, a] = import_DefaultLayers(allKM);
%     SonarScope(a)
%
%     DTReflec = cl_image.indDataType('Reflectivity');
%     [kReflec, nomLayerReflec] = findIndLayerSonar(a, 1, 'DataType', DTReflec, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTAngles = cl_image.indDataType('TxAngle');
%     [kAngles, nomLayerAngles] = findIndLayerSonar(a, 1, 'DataType', DTAngles, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     DTSectors = cl_image.indDataType('TxBeamIndex');
%     [kSectors, nomLayerSectors] = findIndLayerSonar(a, 1, 'DataType', DTSectors, 'WithCurrentImage', 1, 'OnlyOneLayer');
%
%     Selection(1).Name     = nomLayerAngles;
%     Selection(1).TypeVal  = 'Layer'
%     Selection(1).indLayer = kAngles;
%     Selection(1).DataType = DTAngles;
%     Selection(2).Name     = nomLayerSectors;
%     Selection(2).TypeVal  = 'Layer'
%     Selection(2).indLayer = kSectors;
%     Selection(2).DataType = DTSectors;
%     bins{1} = -75:75;
%     bins{2} = 1:3;
%     [a(kReflec), bilan] = courbesStats(a(kReflec), nomLayerReflec, 'Courbe test', '', [], ...
%         a, Selection, 'bins', bins, 'GeometryType', 'PingBeam', 'Sampling' , 1);
%     CurvesFilename = my_tempname('.mat');
%     flag = export_xml_courbesStats(a(kReflec), CurvesFilename)
%     a(kReflec) = deleteCourbesStats(a(kReflec));
%
%   [flag, a(kReflec)] = import_xml_courbesStats(a(kReflec), CurvesFilename);
%     plotCourbesStats(a(kReflec), 'Stats', 0);
%
% See also cl_image/export_xml_courbesStats Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, this] = import_xml_courbesStats(this, CurvesFilename, varargin)

[varargin, PrioriteLectureASCII] = getPropertyValue(varargin, 'PrioriteLectureASCII', 0); %#ok<ASGLU>

[nomDir, nomFic] = fileparts(CurvesFilename);

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, [nomFic '.xml']);flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Info = xml_mat_read(nomFicXml);

%% V�rification des signatures

flag = xml_check_signatures(Info, nomFicXml, 'Statistic Curves');
if ~flag
    return
end

%% Lecture du r�pertoire des donn�es binaires

nomDirBin = fullfile(nomDir, nomFic);
if ~exist(nomDirBin, 'dir')
    flag = 0;
    return
end

%% Lecture des courbes

for k=1:Info.Dimensions.nbCurves
    for iGroup=1:Info.Curve(k).Dimensions.nbGroups
        Courbes(k).bilan{1}(iGroup).nomX            = Info.Curve(k).nomX; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).titreV          = Info.Curve(k).titreV; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).nomZoneEtude	= Info.Curve(k).nomZoneEtude; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).commentaire     = Info.Curve(k).commentaire; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).Tag             = Info.Curve(k).Tag; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).DataTypeValue   = Info.Curve(k).DataTypeValue; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).GeometryType    = Info.Curve(k).GeometryType; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).TypeCourbeStat  = Info.Curve(k).TypeCourbeStat; %#ok<AGROW>
        Courbes(k).bilan{1}(iGroup).TagSup          = Info.Curve(k).TagSup; %#ok<AGROW>
        %         Courbes(k).bilan{1}(iGroup).DataTypeConditions{1} = Info.Curve(k).DataTypeConditions; %#ok<AGROW>
        
        % Modif JMA le 21/08/2015 pour r�cup�rer session Jean-Guy
        Courbes(k).bilan{1}(iGroup).DataTypeConditions = Info.Curve(k).DataTypeConditions; %#ok<AGROW>
        
        Courbes(k).bilan{1}(iGroup).Support = Info.Curve(k).Support; %#ok<AGROW>
        
        if isfield(Info.Curve(k), 'Unit')
            Courbes(k).bilan{1}(iGroup).Unit = Info.Curve(k).Unit; %#ok<AGROW>
        else
            Courbes(k).bilan{1}(iGroup).Unit = ''; %#ok<AGROW>
        end
        
        if isfield(Info.Curve(k), 'nomVarCondition')
            Courbes(k).bilan{1}(iGroup).nomVarCondition = Info.Curve(k).nomVarCondition; %#ok<AGROW>
        end
        
        Courbes(k).bilan{1}(iGroup).NY = Info.Curve(k).NY; %#ok<AGROW>
        
        if isfield(Info.Curve(k).Group(iGroup), 'numVarCondition')
            Courbes(k).bilan{1}(iGroup).numVarCondition = Info.Curve(k).Group(iGroup).numVarCondition; %#ok<AGROW>
        end
        
        Courbes(k).bilan{1}(iGroup).Biais = Info.Curve(k).Group(iGroup).Biais; %#ok<AGROW>
        
        % Lecture des fichiers binaires des signaux
        
        for iSignal=1:length(Info.Curve(k).Group(iGroup).Signals)
            [flag, Signal] = XMLBinUtils.readSignal(nomDir, Info.Curve(k).Group(iGroup).Signals(iSignal), ...
                Info.Curve(k).Group(iGroup).Dimensions, 'LogSilence', 0);
            if ~flag
                return
            end
            nomSignal = Info.Curve(k).Group(iGroup).Signals(iSignal).Name;
            Courbes(k).bilan{1}(iGroup).(nomSignal) = Signal;
        end
        NomFicAscii = fullfile(nomDirBin, ['x_y_ymedian_sub_nx_std_' num2str(k) '_' num2str(iGroup) '.txt']);
        if exist(NomFicAscii, 'file') && PrioriteLectureASCII
            X = dlmread(NomFicAscii);
            %             Courbes(k).bilan{1}(iGroup).x = X(:,1);
            Courbes(k).bilan{1}(iGroup).y = X(:,2); %#ok<AGROW>
            Courbes(k).bilan{1}(iGroup).ymedian = X(:,3); %#ok<AGROW>
            %             Courbes(k).bilan{1}(iGroup).sub = X(:,4);
            %             Courbes(k).bilan{1}(iGroup).nx = X(:,5);
            %             Courbes(k).bilan{1}(iGroup).std = X(:,6);
        end
    end
    
    this.CourbesStatistiques(end+1).bilan = Courbes(k).bilan;
end

%{
%% Cr�ation des fichiers binaires des signaux

nomDirRacine = fileparts(nomDirBin);
for k=1:Info.Dimensions.nbCurves
for iGroup=1:Info.Curve(k).Dimensions.nbGroups
X = [];
NomFicAscii= '';
for iSignal=1:length(Info.Curve(k).Group(iGroup).Signals)
flag = writeSignal(nomDirRacine, Info.Curve(k).Group(iGroup).Signals(iSignal),...
Courbes(k).bilan{1}(iGroup).(Info.Curve(k).Group(iGroup).Signals(iSignal).Name));
if ~flag
return
end
X = [X Courbes(k).bilan{1}(iGroup).(Info.Curve(k).Group(iGroup).Signals(iSignal).Name)(:)]; %#ok<AGROW>
NomFicAscii = [NomFicAscii Info.Curve(k).Group(iGroup).Signals(iSignal).Name '_']; %#ok<AGROW>
end
NomFicAscii = fullfile(nomDirBin, [NomFicAscii num2str(k) '_' num2str(iGroup) '.txt']);
% Exportation ASCII
dlmwrite(NomFicAscii, X)
end
end
%}

flag = 1;
