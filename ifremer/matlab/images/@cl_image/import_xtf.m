% Importation d'un fichier Triton (*.xtf)
%
% Syntax
%   [flag, a] = import_xtf(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('demowreck.XTF')
%   [flag, a] = import_xtf(cl_image, nomFic);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this, selection] = import_xtf(this, nomFic, varargin)

% [varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
% [varargin, DataType] = getPropertyValue(varargin, 'DataType', []);
% [varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []);

a = cl_xtf('nomFic', nomFic);
if isempty(a)
    flag = 0;
    selection = [];
    return
end
[flag, this, ~, ~, selection] = view_Image(a, varargin{:});
