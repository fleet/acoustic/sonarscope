% Import of an ASCII file containing {xq, yq, val} data. This function grids the data.
%
% Syntax
%   [flag, a] = import_xyz(a, FileName)
%
% Input Arguments
%   a        : An instance of cl_image
%   FileName : Nom du fichier
%
% Name-Value Pair Arguments
%   Carto           : cl_carto instance
%   ScaleFactor     : Scale factor to apply to the data (Ex : -1 if depths
%                     are positive values in the file)
%   ControleManuel  : O/1 : if 1 a plot of the xq, yq positions is done, the
%                     function is waiting for you to close it. You can zoom
%                     on the part of the data you want to grid.
%   pasxy           : [stepx stepy]
%   TypeAlgo        : 1=Nearest, 2=Biinear
%   DisplayEntete   : 1/0 : 1=Display the first 20 lines in a window
%   NbLigEntete     : Number of heading lines
%   AskSeparator    : 1/0 : 1=aks to the user to define the separator
%   Separator       : Separator if different from usual ones
%   AskValNaN       : 1/0 : 1=aks to the user to define the value for missing data
%   ValNaN          : Missing value
%   nColumns        : Number of columns in the file
%   AskGeometryType : 1/0 : 1=aks to the user to define the GeometryType
%   GeometryType    : See cl_image
%   IndexColumns    : Order of the comumns for x, y and v
%   DataType        : See cl_image
%   Unit            : Unit of the data
%   sub             : Subsampling of the data (Ex : 1:10:1000)
%   SigmaXY         : Coefficient to exclude data After SigmaXY*SandartDeviation
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Two instance of cl_image, first one is the data, the second is
%          the number of points that were averaged
%
% Examples
%   FileName = getNomFicDatabase('LIDAR_1.xyz');
%   [flag, b] = import_xyz(cl_image, FileName);
%   imagesc(b);
%
%   [flag, c] = import_xyz(cl_image, FileName, 'DisplayEntete', 0, 'NbLigEntete', 0, ...
%       'AskSeparator', 0, 'Separator', [], 'AskValNaN', 0, 'ValNaN', [], ...
%       'nColumns', 4, 'AskGeometryType', 0, 'GeometryType', 2, ...
%       'IndexColumns', [1 2 3], 'DataType', 2, 'Unit', 'm');
%   imagesc(c);
%
% See also cl_image/sonar_importBathyFromCaris_ALL process_xyz read_ASCII Authors
% Authors : JMA
%--------------------------------------------------------------------------

% TODO : pr�voir une boucle sur le nombre de fichiers avec conservation des
% param�tres si SameAnswer=1 voir "process_xyz"

% TODO : pr�voir de pouvoir importer plusieurs colonnes de donn�es
% (fichiers ME70 de Laurent par exemple o� on a x?y?Bathy, Reflec,
% AcrossAngle, AlongAngle)

function [flag, this, Carto, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, ...
    IndexColumns, DataType, TypeAlgo, Unit, pasxy] = import_xyz(~, nomFic, varargin)

persistent persistent_pasxy persistent_TypeAlgo

[varargin, Carto]          = getPropertyValue(varargin, 'Carto',          []);
[varargin, ScaleFactor]    = getPropertyValue(varargin, 'ScaleFactor',    []);
[varargin, ControleManuel] = getPropertyValue(varargin, 'ControleManuel', 1);
[varargin, pasxy]          = getPropertyValue(varargin, 'pasxy',          []);
[varargin, TypeAlgo]       = getPropertyValue(varargin, 'TypeAlgo',       []);
[varargin, memeReponses]   = getPropertyValue(varargin, 'memeReponses',   false);

I0 = cl_image_I0;
C0 = cl_carto([]);

%% Test de lecture au format ascii SRTM (produit aussi par ARC-GIS)

[flag, this] = import_xyz_FormatSRTM(I0, nomFic);
if flag
    return
end

%% Read the ASCII file

[flag, x, y, v, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, ...
    DataType, Unit] = read_ASCII(nomFic, 'memeReponses', memeReponses, varargin{:});
if ~flag
    return
end

%% Test pour voir si c'est une grille maill�e ou un semis de points

unique_x = unique(x);
unique_y = unique(y);

if isempty(unique_x) || isempty(unique_y)
    str1 = 'y''a quelque-chose qui cloche l� dedans !';
    str2 = 'There is something wrong here !';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

N = length(v);

%% Units of axis

if isempty(GeometryType)
    unitexy = 'deg';
else
    if GeometryType == cl_image.indGeometryType('LatLong')
        unitexy = 'deg';
    elseif GeometryType == cl_image.indGeometryType('GeoYX')
        unitexy = 'm';
    else
        unitexy = '';
    end
end

%% Limits of axis

xq = unique(x);
yq = unique(y);

pas = max(1, floor(N/1e5));

xmin = min(x);
xmax = max(x);
ymin = min(y);
ymax = max(y);

%% Plot of the positions

if ControleManuel
    str1 = 'Une figure va pr�senter la r�partition des donn�es, vous devrez la d�truire apr�s avoir �ventuellement zoom� sur la zone que vous voulez importer. Le traitement se poursuivra apr�s la destruction de cette fen�tre.';
    str2 = 'A figure will give you a preview of the data, you will have to delete it to continue or zoom on the good part of it and then delete it.';
    my_warndlg(Lang(str1,str2), 1);
    fig = figure;
    axes;
    plot(x(1:pas:end), y(1:pas:end), '+');
    grid on;
    axis([xmin xmax ymin ymax])
    title('You have to kill me to continue')
    
    nomFicSave = fullfile(my_tempdir,'ffff.mat');
    clbk = sprintf('w = axis; save(''%s'', ''w'')', nomFicSave);
    set(fig, 'DeleteFcn', clbk)
    
    % Waiting for the figure to be closed
    while ishandle(fig)
        uiwait(fig);
        w = loadmat(nomFicSave, 'nomVar', 'w');
        delete(nomFicSave)
    end
else
    w = [xmin xmax ymin ymax];
end

%% Carto

if isempty(Carto)
    nomDir = fileparts(nomFic);
    [flagExistenceCarto, Carto, nomFicCarto] = test_ExistenceCarto_Folder(nomDir);
    if flagExistenceCarto
        str1 = sprintf('Le fichier "%s" est interpr�t� directement', nomFicCarto);
        str2 = sprintf('File "%s" is used by default.', nomFicCarto);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
    else
        if GeometryType == cl_image.indGeometryType('LatLong')
            [flag, Carto] = editParams(C0, mean([ymin ymax]), mean([xmin xmax]), 'MsgInit');
        else
            [flag, Carto] = editParams(C0, [], [], 'MsgInit');
        end
        if ~flag
            return
        end
        
        str1 = 'Voulez-vous sauver les param�tres cartographiques dans un fichier ?';
        str2 = 'Save the cartographic parameters in a file ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
        if rep == 1
            str1 = 'Nom du fichier de d�finition de la carto';
            str2 = 'Give a file name';
            [flag, nomFicCarto] = my_uiputfile('*.def', Lang(str1,str2), ...
                fullfile(nomDir, 'Carto.def'));
            if ~flag
                return
            end
            flag = export(Carto, nomFicCarto);
            if ~flag
                return
            end
        end
    end
end

%% Keeps data in the frame given by the user

if ~isequal(w, [xmin xmax ymin ymax])
    xmin = w(1);
    xmax = w(2);
    ymin = w(3);
    ymax = w(4);
    sub = find((x >= xmin) & (x <= xmax) & (y >= ymin) & (y <= ymax));
    y = y(sub);
    x = x(sub);
    v = v(sub);
    N = length(y);
    
    xmin = min(x);
    xmax = max(x);
    ymin = min(y);
    ymax = max(y);
end

rangex = xmax - xmin;
rangey = ymax - ymin;
surfaceUnitaire = rangex * rangey / N;

if GeometryType == cl_image.indGeometryType('PingBeam')
    pasy = 1;
else
    %     pasy = sqrt(surfaceUnitaire) * 2;
    % Question de GLU : pourquoi 2 ? R�ponse : Ceci est un myst�re. Je pense que �a vient d'un essai sur un fichier particulier
    % TODO : r�gler ce pb sur un fichier o� la densit� de point est bien
    % r�partie sur le rectangle correspondant � la grille
    
    pasy = sqrt(surfaceUnitaire);
end


%% Type de l'algorithme

if isempty(TypeAlgo)
    if memeReponses && ~isempty(persistent_TypeAlgo)
        TypeAlgo = persistent_TypeAlgo;
    else
        str1 = 'Algorithme de maillage.';
        str2 = 'Gridding algorithm';
        str3 = 'Plus proche voisin';
        str4 = 'Nearest';
        str5 = 'Biin�aire';
        str6 = 'Biinear';
        [TypeAlgo, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            return
        end
        persistent_TypeAlgo = TypeAlgo;
    end
end

%% Cr�ation des axes xq et yq

if isempty(pasxy) && ~memeReponses
    flag = 0;
    while ~flag
        if GeometryType == cl_image.indGeometryType('LatLong')
            pasx = pasy / cos(mean(yq) * (pi/180));
        else
            pasx = pasy;
        end
        
        xq = xmin:pasx:xmax;
        yq = ymin:pasy:ymax;
        [xq, pasx, xmin, xmax] = centrage_magnetique(xq);
        [yq, pasy, ymin, ymax] = centrage_magnetique(yq);
        
        nbColumns = length(xq);
        nbRows = length(yq);
        
        if GeometryType == cl_image.indGeometryType('LatLong')
            str1 = sprintf('Proposition : \n\nTaille de la grille (m): %f\nLignes        : %d\nColonnes    : %d\n\nCela vous convient-il ?', ...
                pasy * (6378137 * (pi/180)), nbRows, nbColumns);
            str2 = sprintf('Proposition : \n\nPixel size (m): %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasy * (6378137 * (pi/180)), nbRows, nbColumns);
        else
            str1 = sprintf('Proposition : \n\nTaille de la grille : %f\nLignes        : %d\nColonnes    : %d\n\nCela vous convient-il ?', ...
                pasx, nbRows, nbColumns);
            str2 = sprintf('Proposition : \n\nPixel size : %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasx, nbRows, nbColumns);
        end
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
        if rep == 1
            flag = 1;
        else
            if GeometryType == cl_image.indGeometryType('LatLong')
                pasyEdition = pasy * (6378137 * pi/180);
            else
                pasyEdition = pasy;
            end
            str1 = 'La donn�e va �tre maill�e, d�finissez la taille de la maille.';
            str2 = 'The import will grid the data. Please select the grid size.';
            [flag, pasy] = inputOneParametre(Lang(str1,str2), 'Grid size', ...
                'Value', pasyEdition, 'Unit', 'm'); %unitexy);
            if ~flag
                return
            end
            
            if GeometryType == cl_image.indGeometryType('LatLong')
                pasy = pasy / (6378137 * pi/180);
                pasx = pasy / cos(mean(yq) * (pi/180));
            else
                pasx = pasy;
            end
            flag = 0;
        end
    end
    pasxy(1) = pasx;
    pasxy(2) = pasy;
    persistent_pasxy = pasxy;
else
    if isempty(pasxy)
        pasxy = persistent_pasxy;
    end
    pasx = pasxy(1);
    pasy = pasxy(2);
    xq = xmin:pasx:xmax;
    yq = ymin:pasy:ymax;
    [xq, pasx] = centrage_magnetique(xq);
    [yq, pasy] = centrage_magnetique(yq);
end
pasxy(1) = pasx;
pasxy(2) = pasy;

%% Algorithme de gridding

% figure; scatter(x, y, [], v); colormap jet; colorbar, axis('tight'); xlabel('x'); ylabel('y'); title('v'); 

% Z2 = griddata(x, y, v, xq, yq');
% figure; imagesc(xq, yq, Z2); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('vq'); 

[vq, nq] = my_griddata(x, y, v, xq, yq, 'TypeAlgo', TypeAlgo);
% figure; imagesc(xq, yq, vq); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('vq'); 

%% Cr�ation des instances d'images

if GeometryType == cl_image.indGeometryType('LatLong')
    TagSynchroX = 'Longitude';
    TagSynchroY = 'Latitude';
else
    TagSynchroX = num2str(rand(1));
    TagSynchroY = num2str(rand(1));
end
[nomDir, Name] = fileparts(nomFic); %#ok<ASGLU>

if ~isempty(ScaleFactor)
    vq = vq * ScaleFactor;
end
this = cl_image('Image',    vq, ...
    'Name',                Name, ...
    'Unit',                 Unit, ...
    'x',                    xq, ...
    'y',                    yq, ...
    'XUnit',                unitexy, ...
    'YUnit',                unitexy, ...
    'ColormapIndex',        3, ...
    'DataType',             DataType, ...
    'GeometryType',         GeometryType, ...
    'TagSynchroX',          TagSynchroX, ...
    'TagSynchroY',          TagSynchroY, ...
    'InitialFileName',      nomFic, 'InitialFileFormat', 'ASCII xyz', ...
    'Carto', Carto);
this = update_Name(this);

if ~isempty(Carto)
    this = set(this, 'Carto', Carto);
end

DataType2 = cl_image.indDataType('AveragedPtsNb');
this(2) = cl_image('Image', nq, ...
    'Name',                Name, ...
    'Unit',                 '-', ...
    'x',                    xq, ...
    'y',                    yq, ...
    'XUnit',                unitexy, ...
    'YUnit',                unitexy, ...
    'ColormapIndex',        3, ...
    'DataType',             DataType2, ...
    'GeometryType',         GeometryType, ...
    'TagSynchroX',          TagSynchroX, ...
    'TagSynchroY',          TagSynchroY, ...
    'InitialFileName',      nomFic, 'InitialFileFormat', 'ASCII xyz', ...
    'Carto', Carto);
this(2) = update_Name(this(2));

flag = 1;
