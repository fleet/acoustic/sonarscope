% Importation d'un fichier ASCII xyz (sondes donnees en xyz)
%
% Syntax
%   [flag, a] = import_xyz_FormatSRTM(a, nomFic)
%
% Input Arguments
%   a      : Instance meme vide de cl_image
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   a    : Instance de cl_image
%
% Examples
%   [X,Y,Z] = peaks(30);
%   a = cl_image('Image', Z)
%   imagesc(a)
%
%   nomFicOut = my_tempname
%   flag = export_xyz(a, nomFicOut)
%
%   [flag, b] = import_xyz_FormatSRTM(cl_image, nomFicOut);
%   imagesc(b)
%
%   nomFic = getNomFicDatabase('LIDAR_1.xyz');
%   nomFic = getNomFicDatabase('LIDAR_2.xyz');
%   [flag, b] = import_xyz_FormatSRTM(cl_image, nomFic);
%   imagesc(b)
%
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, this, Carto] = import_xyz_FormatSRTM(~, nomFic, varargin)

persistent persistent_DataType persistent_GeometryType

[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false); %#ok<ASGLU>
% [varargin, DataType]     = getPropertyValue(varargin, 'DataType',     []);
% [varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []);

this = cl_image.empty;
Entete = {};

dx = NaN;
dy = NaN;
% str1 = sprintf('Le format du fichier "%s" a �t� reconnu automatiquement, il ne vous reste plus qu''� d�crire le type de donn�es et la g�om�trie.', nomFic);
% str2 = sprintf('The format of "%s" has been recognized automaticaly , you just need to define the data type and the geometry.', nomFic);
% my_warndlg(Lang(str1,str2), 0, 'Tag', 'FormatReconnuAutomatiquement', 'TimeDelay', 60);

%% Read the header of the file

fid = fopen(nomFic, 'r');
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.xyz']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end
isHeader = true;
while ~feof(fid) && isHeader
    Entete{end+1} = fgetl(fid);%#ok
    lin = textscan(Entete{end}, '%f'); % test si on lit un double
    isHeader = isempty(lin{1});
end
Entete(end) = []; % on supprime la derni�re ligne lue puisqu'elle contient des donn�es
nbLigEntete = length(Entete);
fclose(fid);
flag = 1;

if nbLigEntete < 5
    flag = 0;
    return
end

%% Decode Header

mots = strsplit(Entete{1});
if (length(mots) == 2) && strcmpi(mots{1}, 'ncols') && ~isempty(str2double(mots{2}))
    ncols = str2double(mots{2});
else
    flag = 0;
end

if length(Entete) < 2
    flag = 0;
    return
end

mots = strsplit(Entete{2});
if (length(mots) == 2) && strcmpi(mots{1}, 'nrows') && ~isempty(str2double(mots{2}))
    nrows = str2double(mots{2});
else
    flag = 0;
end

mots = strsplit(Entete{3});
if (length(mots) == 2) && strcmpi(mots{1}, 'xllcorner') && ~isempty(str2double(mots{2}))
    xllcorner = str2double(mots{2});
    xllcenter = [];
elseif (length(mots) == 2) && strcmpi(mots{1}, 'xllcenter') && ~isempty(str2double(mots{2}))
    xllcenter = str2double(mots{2});
    xllcorner = [];
else
    flag = 0;
end

mots = strsplit(Entete{4});
if (length(mots) == 2) && strcmpi(mots{1}, 'yllcorner') && ~isempty(str2double(mots{2}))
    yllcorner = str2double(mots{2});
    yllcenter = [];
elseif (length(mots) == 2) && strcmpi(mots{1}, 'yllcenter') && ~isempty(str2double(mots{2}))
    yllcenter = str2double(mots{2});
    yllcorner = [];
else
    flag = 0;
end

mots = strsplit(Entete{5});
if (length(mots) == 2) && strcmpi(mots{1}, 'cellsize') && ~isempty(str2double(mots{2}))
    cellsize = str2double(mots{2});
elseif (length(mots) == 2) && strcmpi(mots{1}, 'dx') && ~isempty(str2double(mots{2}))
    % Exploitation des cellules "non carr�es".
    dx = str2double(mots{2});
else
    flag = 0;
end


idxMot = 6;
if strcmpi(mots{1}, 'dx')
    % Exploitation des cellules "non carr�es" : lecture du dy
    mots   = strsplit(Entete{idxMot});
    idxMot = idxMot + 1;
    if (length(mots) == 2) && strcmpi(mots{1}, 'dy') && ~isempty(str2double(mots{2}))
        dy = str2double(mots{2});
    else
        flag = 0;
    end
end

mots   = strsplit(Entete{min(idxMot, length(Entete))});
idxMot = idxMot + 1;
if (length(mots) == 2) && strcmpi(mots{1}, 'NODATA_value') && ~isempty(str2double(mots{2}))
    NODATA_value = str2double(mots{2});
else
    NODATA_value = [];
end

mots = strsplit(Entete{min(idxMot, length(Entete))});
if (length(mots) == 2) && strcmpi(mots{1}, 'byteorder') && ~isempty(str2double(mots{2}))
    switch mots{2}
        case 'msbfirst'
            byteorder = 'ieee-be';
        case 'lsbfirst'
            byteorder = 'ieee-le';
        otherwise
            flag = 0;
    end
else
    byteorder = 'unknown';
end

if ~flag
    return
end

%% Read the data

fid = fopen(nomFic, 'r');
for k=1:nbLigEntete
    fgetl(fid);
end
C = textscan(fid, '%f');
fclose(fid);

if isempty(C{1}) % ce qui a de grandes chances d'arriver.... si ArcGis
    if strcmp(byteorder, 'unknown')
        [flag, z2le] = readDataSTRM(nomFic, nrows, ncols, NODATA_value, 'ieee-le');
        [flag, z2be] = readDataSTRM(nomFic, nrows, ncols, NODATA_value, 'ieee-be');
        %         figure, imagesc(z2le); colormap(jet(256)); colorbar
        %         figure, imagesc(z2be); colormap(jet(256)); colorbar
        if ~isempty(NODATA_value)
            stdle = std(z2le(z2le ~= NODATA_value), 'omitnan');
            stdbe = std(z2be(z2be ~= NODATA_value), 'omitnan');
        end
        if stdle < stdbe
            z2 = z2le;
        else
            z2 = z2be;
        end
        clear stdle stdbe
    else
        [flag, z2] = readDataSTRM(nomFic, nrows, ncols, byteorder);
    end
    
    figure, imagesc(z2); colormap(jet(256)); colorbar
else
    z2 = reshape(C{:}, ncols, nrows)';
end

%% Process Nodata

if ~isempty(NODATA_value)
    zmax = max(z2(:));
    zmin = min(z2(:));
    if (zmax ~= NODATA_value) || (zmin ~= NODATA_value)
        % Pour cas o� pas assez de pr�cision num�rique NODATA = max
        if abs(zmin - NODATA_value) > abs(zmax - NODATA_value)
            z2(z2 == zmax) = NaN;
        else
            %             z2(z2 == zmin) = NaN; % Comment� le 13/06/2018 pour fichier Giuseppe. Je ne me souviens pas pourquoi j'avais cod� cela
        end
        z2(z2 == NODATA_value) = NaN; % Ajout� par JMA le 12/06/2018 : a d�placer en dehors de la boucle quand �a sera valid�
    else
        z2(z2 == NODATA_value) = NaN;
    end
end
clear C


if memeReponses
    % Datatype
    if isempty(persistent_DataType)
        [flag, DataType] = cl_image.edit_DataType;
        persistent_DataType = DataType;
    else
        DataType = persistent_DataType;
        flag = 1;
    end
    % GeometryType
    if isempty(persistent_GeometryType) %GeometryType)
        [flag, GeometryType] = cl_image.edit_GeometryType;
        if ~flag
            return
        end
        persistent_GeometryType = GeometryType;
    else
        GeometryType = persistent_GeometryType;
    end
    if ~flag
        return
    end
else
    [flag, DataType] = cl_image.edit_DataType;
    if ~flag
        return
    end
    persistent_DataType = DataType;
    [flag, GeometryType] = cl_image.edit_GeometryType;
    if ~flag
        return
    end
    persistent_GeometryType = GeometryType;
end

flagBeginOfFileIsTopOfImage = 1;
switch GeometryType
    case cl_image.indGeometryType('LatLong')
        unitexy = 'deg';
    case cl_image.indGeometryType('GeoYX')
        unitexy = 'm';
    case cl_image.indGeometryType('PingBeam')
        unitexy = '';
        flagBeginOfFileIsTopOfImage = 0;
    case cl_image.indGeometryType('PingAcrossDist')
        unitexy = '';
        flagBeginOfFileIsTopOfImage = 0;
    otherwise
        unitexy = 'm';
end

if ~flag
    return
end

%% Define x and y axis

if isnan(dx)
    dx = cellsize;
end
if isnan(dy)
    dy = cellsize;
end

if isempty(xllcenter)
    xllcenter = xllcorner + dx / 2;
end
if isempty(yllcenter)
    yllcenter = yllcorner + dy / 2;
end

x = linspace(xllcenter, xllcenter+(ncols-1)*dx, ncols);
if flagBeginOfFileIsTopOfImage
    y = linspace(yllcenter+(nrows-1)*dy, yllcenter, nrows);
else
    y = linspace(yllcenter, yllcenter+(nrows-1)*dy, nrows); % Modif JMA le 11/06/2018 pour Giuseppe
end

[~, Name] = fileparts(nomFic);
this = cl_image('Image',           z2, ...
                'Name',           Name, ...
                'x',               x, ...
                'y',               y, ...
                'XUnit',           unitexy, ...
                'YUnit',           unitexy, ...
                'ColormapIndex',   3, ...
                'DataType',        DataType, ...
                'GeometryType',    GeometryType, ...
                'InitialFileName', nomFic, 'InitialFileFormat', 'ASCII xyz');
this = update_Name(this);

if GeometryType == cl_image.indGeometryType('LatLong')
    LatMean = mean(y);
    LonMean = mean(x);
else
    LatMean = [];
    LonMean = [];
end

if isempty(Carto)
    [flag, Carto] = editParams(cl_carto([]), LatMean, LonMean, 'MsgInit');
    if ~flag
        return
    end
end

this = correctionUTMSouth(this, Carto);

this = set(this, 'Carto', Carto);


function [flag, z2] = readDataSTRM(nomFic, nrows, ncols, NODATA_value, byteorder)

[path,filename] =  fileparts(nomFic);
fid = fopen(fullfile(path,  [filename '.flt']), 'rb', byteorder);
z2 = fread(fid, [ncols nrows], 'single')';
fclose(fid);

if ~isempty(NODATA_value)
    zmax = max(z2(:));
    zmin = min(z2(:));
    if zmax ~= NODATA_value || zmin ~= NODATA_value
        % Pour cas ou pas assez de pr�cision num�rique NODATA = max
        if abs(zmin - NODATA_value) > abs(zmax - NODATA_value)
            z2(z2 == zmax) = NaN;
        else
            z2(z2 == zmin) = NaN;
        end
    else
        z2(z2 == NODATA_value) = NaN;
    end
end

flag = 1;
