% Conversion d'images pseudo-couleur en images vraies couleurs
%
% Syntax
%   b = ind2rgb(a)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_SHC.mat');
%   I     = loadmat(nomFic, 'nomVar', 'I');
%   segm5 = loadmat(nomFic, 'nomVar', 'segm5');
%   nbRows = size(I, 1); nbColumns = size(I, 2);
%
%   a(1) = cl_image('Image', I,     'Name', 'Sonar',        'ColormapIndex', 2, 'CLim', [150 256])
%   a(2) = cl_image('Image', segm5, 'Name', 'Texture', 'ColormapIndex', 3, 'CLim', [0 4], ...
%                     'x', [1 nbColumns], 'y', [1 nbRows])
%   imagesc(a, 'Fig', 1000)
%
%   b = ind2rgb(a)
%   imagesc(b, 'Fig', 2000)
%
% See also cl_image cl_image/imagesc cl_image/rgb2ind Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = ind2rgb(this, varargin)
my_warndlg('MESSAGE POUR JMA : cl_image/ind2rgb : cette fonction ne devrait plus etre utilisee', 1);

[subx, suby, varargin] = getSubxSuby(this(1), varargin); %#ok<ASGLU>

for i=1:length(this)
    b = this(i);
    
    if b.nbSlides == 1
        %         if b.CoulIndex == 2
        if b.ImageType == 3
            b = replace_Image(b, ind2rgb(b.Image(suby,subx,:), b.Colormap));
        else
            I   = singleUnlessDouble(b.Image(suby,subx,:), this.ValNaN);
            map = b.Colormap;
            
            I(I < b.CLim(1)) = b.CLim(1);
            I(I > b.CLim(2)) = b.CLim(2);
            
            presenceNan = isnan(sum(I(:)));
            nbCoul = size(map, 1);
            
            if presenceNan
                offset = diff(b.CLim) / nbCoul;
                I = (I - b.CLim(1) + offset) / (diff(b.CLim) + offset);
                n = size(map,1);
                map = [1 1 1 ;
                    interp2(map, 1:3, linspace(1,n,n-1)')];
                I(isnan(I)) = 0;
                %                 I = round(nbCoul * I);
                I = round((nbCoul-1) * I); % Fevrier2006
            else
                I = (I - b.CLim(1)) / (b.CLim(2) - b.CLim(1));
                %                 I = round(nbCoul * I);
                I = round((nbCoul-1) * I); % Fevrier2006
            end
            
            b = replace_Image(b, ind2rgb(I, map));
        end
        this(i) = b;
    else
        my_warndlg('cl_image:ind2rgb L''image d''entree doit etre en pseudo-couleur', 0);
    end
end
