% Conversion d'images pseudo-couleur en images vraies couleurs
%
% Syntax
%   [X, map] = ind2tif(a)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Output Arguments
%   X   : Image
%   map : Table de couleurs
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_SHC.mat');
%   I     = loadmat(nomFic, 'nomVar', 'I');
%   segm5 = loadmat(nomFic, 'nomVar', 'segm5');
%   nbRows = size(I, 1); nbColumns = size(I, 2);
%
%   a(1) = cl_image('Image', I,     'Name', 'Sonar',   'ColormapIndex', 2, 'CLim', [150 256])
%   a(2) = cl_image('Image', segm5, 'Name', 'Texture', 'ColormapIndex', 3, 'CLim', [0 4], ...
%                     'x', [1 nbColumns], 'y', [1 nbRows])
%   imagesc(a, 'Fig', 1000)
%
%   [X, map] = ind2tif(a)
%   image(X, map)
%
% See also cl_image cl_image/RGB2Indexed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [X, map] = ind2tif(this, varargin)
% TODO
my_warndlg('MESSAGE POUR JMA : cl_image/ind2rgb : cette fonction ne devrait plus etre utilisee', 1);

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

if this.nbSlides == 1
    if this.ImageType == 3
        X = ind2tif(this.Image(suby,subx), this.Colormap);
        map = this.Colormap;
    else
        I   = singleUnlessDouble(this.Image(suby,subx), this.ValNaN);
        map = this.Colormap;
        
        I(I < this.CLim(1)) = this.CLim(1);
        I(I > this.CLim(2)) = this.CLim(2);
        
        presenceNan = isnan(sum(I(:)));
        nbCoul = size(map, 1);
        
        if presenceNan
            offset = 1 / nbCoul;
            I = (I - this.CLim(1) + offset) / (this.CLim(2) - this.CLim(1) + offset);
            map = [map; 1 1 1];
            I(isnan(I)) = 0;
        else
            I = (I - this.CLim(1)) / (this.CLim(2) - this.CLim(1));
        end
        X = round(nbCoul * I);
    end
else
    my_warndlg('cl_image:ind2tif L''image d''entree doit etre en pseudo-couleur', 0);
end
