function infoCourbesStats(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques)); %#ok<ASGLU>

if isempty(sub)
    message_NoCurve(this)
    return
end

if sub(1) < 0 % Astuce pour dire les N derni�res courbes
    sub = length(this.CourbesStatistiques) + sub + 1;
end

if isempty(sub)
    message_NoCurve(this)
    return
end

str = {};
for i=1:length(sub)
    str1 = infoCourbesStats(this.CourbesStatistiques(sub(i)).bilan);
    str{end+1} = str1; %#ok<AGROW>
end

str = cell2str(str);

disp(str)
edit_infoPixel(str, [])
