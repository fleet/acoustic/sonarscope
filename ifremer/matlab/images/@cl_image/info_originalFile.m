% Operateur d'opposition de signe
%
% Syntax
%   c = -a
%
% Input Arguments
%   a : Une instance de cl_image ou un scalaire
%
% Output Arguments
%   c : Une instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   a = import_CaraibesGrid(cl_image, nomFic)
%   imagesc(a)
%   info_originalFile(a)
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function info_originalFile(this)

if isempty(this.InitialFileName)
    my_warndlg('InitialFileName is unknown', 1);
    return
end

switch this.InitialFileFormat
    case 'WMS'
        url = this.InitialFileName;
        ind = strfind(url, 'Request');
        if isempty(ind)
            return
        end
        url = [url(1:ind-1) 'Request=GetCapabilities'];
        info_WMS(url);
        
    case 'SRTM'
        str = sprintf('%s file is a SRTM file (1201x1201pixels in int16)', this.InitialFileName);
        my_warndlg(str, 1);

    case 'GMT'
        ncEdit(this.InitialFileName)

    case {'CaraibesSTRImage'; 'CaraibesMNT'; 'CaraibesSonarLateral'; 'CaraibesMbb'; 'CaraibesMOSImage'}
        if exist(this.InitialFileName, 'file')
            ncEdit(this.InitialFileName)
        else
            str = sprintf('%s file doesn''t seem to exist yet', this.InitialFileName);
            my_warndlg(str, 1);
        end
        
    case 'ErMapper'
        [nomDir, nomFicSeul, extension] = fileparts(this.InitialFileName);
        if isempty(extension)
            extension = '.ers';
        end
        nomFic = fullfile(nomDir, [nomFicSeul extension]);
        if exist(nomFic, 'file')
            edit(nomFic)
        else
            str = sprintf('%s file doesn''t seem to exist yet', nomFic);
            my_warndlg(str, 1);
        end
        
    case 'MATLAB'
        if exist(this.InitialFileName, 'file')
            my_warndlg('See result on console', 1);
            str = whos('-file', this.InitialFileName);
            disp(this.InitialFileName)
            disp(str)
            for i=1:length(str)
                fprintf('Variable %d :\n', i);
                str(i)
            end
        else
            str = sprintf('%s file doesn''t seem to exist yet', this.InitialFileName);
            my_warndlg(str, 1);
        end
      
    case 'IMAGE'
        if exist(this.InitialFileName, 'file')
            my_warndlg('See result on console', 1);
            imageinfo(this.InitialFileName)
         else
            str = sprintf('%s file doesn''t seem to exist yet', this.InitialFileName);
            my_warndlg(str, 1);
        end
        
    case 'SimradAll'
        if isdeployed
            pathDirReg = 'SOFTWARE\Wow6432Node\Kongsberg\Sonar Record Viewer\InstallationDir';
            kongsbergPath = winqueryreg('HKEY_LOCAL_MACHINE', pathDirReg, 'path');
        else
            kongsbergPath = 'C:\SScTbx\Installation\Viewers';
        end
        my_winopen('Sonar Record Viewer.exe', this.InitialFileName, 'NomDirExe', kongsbergPath)
        
    case {'ResonS7k', 'RESONs7k'}
        my_winopen('DataView.exe', this.InitialFileName)

    case 'Netcdf'
        ncEdit(this.InitialFileName)
       
    otherwise
        try
            imageinfo(this.InitialFileName)
        catch %#ok<CTCH>
        end
end
