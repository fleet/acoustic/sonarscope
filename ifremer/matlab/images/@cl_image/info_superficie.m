function str = info_superficie(this, varargin)

flag = testSignature(this, 'GeometryType', [cl_image.indGeometryType('LatLong') cl_image.indGeometryType('GeoYX')]);
if ~flag
    str = [];
    return
end

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

S = stats(this.Image(suby,subx,1), 'Complete');

XStepMetric = this.XStepMetric;
YStepMetric = this.YStepMetric;

SuperficieTotale = (this.nbColumns * XStepMetric) * (this.nbRows * YStepMetric);
Superficie = SuperficieTotale * (1-S.PercentNaN/100);

if Superficie < 1e6
    str = ['Surface covered by pixels : ' num2str(Superficie) ' m2'];
else
    str = ['Surface covered by pixels : ' num2str(Superficie/1e6) ' km2'];
end

if S.PercentNaN ~= 0
    str = [str '   (Image filling ' num2str(floor(S.PercentNaN*100)/100) '%)'];
end
