function that = inherit(this, Image, varargin)

[varargin, x]              = getPropertyValue(varargin, 'x',              []);
[varargin, y]              = getPropertyValue(varargin, 'y',              []);
[varargin, subx]           = getPropertyValue(varargin, 'subx',           []);
[varargin, suby]           = getPropertyValue(varargin, 'suby',           []);
[varargin, AppendName]     = getPropertyValue(varargin, 'AppendName',     []);
[varargin, Unit]           = getPropertyValue(varargin, 'Unit',           this.Unit);
[varargin, ColormapIndex]  = getPropertyValue(varargin, 'ColormapIndex',  []);
[varargin, ValNaN]         = getPropertyValue(varargin, 'ValNaN',         []);
[varargin, DataType]       = getPropertyValue(varargin, 'DataType',       []);
[varargin, ImageType]      = getPropertyValue(varargin, 'ImageType',      []);
[varargin, GeometryType]   = getPropertyValue(varargin, 'GeometryType',   []);
[varargin, NoStats]        = getPropertyValue(varargin, 'NoStats',        0);
[varargin, XLabel]         = getPropertyValue(varargin, 'XLabel',         []);
[varargin, YLabel]         = getPropertyValue(varargin, 'YLabel',         []);
[varargin, XUnit]          = getPropertyValue(varargin, 'XUnit',          []);
[varargin, YUnit]          = getPropertyValue(varargin, 'YUnit',          []);
[varargin, TagSynchroX]    = getPropertyValue(varargin, 'TagSynchroX',    []);
[varargin, TagSynchroY]    = getPropertyValue(varargin, 'TagSynchroY',    []);
[varargin, CLim]           = getPropertyValue(varargin, 'CLim',           []);
[varargin, Name]           = getPropertyValue(varargin, 'Name',           []);
[varargin, ColormapCustom] = getPropertyValue(varargin, 'ColormapCustom', []);
[varargin, Video]          = getPropertyValue(varargin, 'Video',          []);
[varargin, Parameters]     = getPropertyValue(varargin, 'Parameters',     []);
[varargin, Sampling]       = getPropertyValue(varargin, 'Sampling',       []);
[varargin, Comments]       = getPropertyValue(varargin, 'Comments',       []);

if ~isempty(varargin)
    str1 = 'Message pour JMA : varargin non vide dans cl_image/inherit';
    str2 = 'Message for JMA : varargin not empty in cl_image/inherit';
    my_warndlg(Lang(str1,str2), 1);
end

%% Replace the image

that = replace_Image(this, Image);

%% Fix the coodinates

if isempty(x) || isempty(y)
    if isempty(subx) && isempty(suby)
        that = majCoordonnees(that);
    else
        that = majCoordonnees(that, subx, suby);
    end
else
    that = majCoordonnees(that, [], [], x, y);
end

%% ValNaN

if ~isempty(ValNaN)
    that.ValNaN = ValNaN;
end

%% Compute the statistics

if ~NoStats
    if isempty(Sampling)
        that = compute_stats(that);
    else
        that = compute_stats(that, 'Sampling', Sampling);
    end
end

%% Contraste enhancement

that.TagSynchroContrast = num2str(rand(1));
if ~NoStats
    CLimStats = [that.StatValues.Min that.StatValues.Max];
    that.CLim = CLimStats;
end

%% CLim

if ~isempty(CLim)
    that.CLim = CLim;
end

%% Colormap

if ~isempty(ColormapIndex)
    that.ColormapIndex = ColormapIndex;
end

%% ColormapCustom

if ~isempty(ColormapCustom)
    that.ColormapCustom = ColormapCustom;
end

%% Video

if isempty(Video)
    this.Video = Video;
end

%% ImageType

if ~isempty(ImageType)
    that.ImageType = ImageType;
end

%% Unit

% if ~isempty(Unit)
    that.Unit = Unit;
% end

%% DataType

if ~isempty(DataType)
    that.DataType = DataType;
end

%% GeometryType

if ~isempty(GeometryType)
    that.GeometryType = GeometryType;
end

%% XLabel

if ~isempty(XLabel)
    that.XLabel = XLabel;
end

%% YLabel

if ~isempty(YLabel)
    that.YLabel = YLabel;
end

%% XUnit

if ~isempty(XUnit)
    that.XUnit = XUnit;
end

%% YUnit

if ~isempty(YUnit)
    that.YUnit = YUnit;
end

%% TagSynchroX

if ~isempty(TagSynchroX)
    that.TagSynchroX = TagSynchroX;
end

%% TagSynchroY

if ~isempty(TagSynchroY)
    that.TagSynchroY = TagSynchroY;
end

%% Name

if ~isempty(Name)
    that.Name = Name;
end

%% Comments

if ~isempty(Comments)
    this.Comments = Comments;
end

%% Set the image name and complete the history

k = length(that.History) + 1;
if isempty(AppendName)
    that = update_Name(that);
    that.History(k).Function = '?';
    that.History(k).Name     = that.Name;
    if ~isempty(Parameters)
        that.History(k).Parameters = Parameters;
    end
else
    that = update_Name(that, 'Append', AppendName);
    that.History(k).Function = AppendName;
    that.History(k).Name     = that.Name;
    if ~isempty(Parameters)
        that.History(k).Parameters = Parameters;
    end
end
