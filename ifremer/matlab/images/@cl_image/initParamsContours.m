function [flag, Contours] = initParamsContours(this)

StatValues = this.StatValues;

Val = compute_graduations([StatValues.Min StatValues.Max]);
Pas = Val(2) - Val(1);

% Pas = StatValues.Range / 10;

p    = ClParametre('Name', 'Last Isobath', ...
    'Unit', 'm', 'Value', StatValues.Max, 'MinValue', StatValues.Min-Pas, 'MaxValue', StatValues.Max+Pas);
p(2) = ClParametre('Name', 'First isobath', ...
    'Unit', 'm', 'Value', StatValues.Min, 'MinValue', StatValues.Min-Pas, 'MaxValue', StatValues.Max+Pas);
p(3) = ClParametre('Name', 'Step', ...
    'Unit', 'm', 'Value', Pas, 'MinValue', Pas/50, 'MaxValue', Pas*50);
p(4) = ClParametre('Name', 'Annotation step', ...
    'Unit', 'm', 'Value', Pas*5, 'MinValue', Pas/50, 'MaxValue', Pas*50);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

Contours.Step           = val(3);
Contours.AnnotationStep = val(4);
Contours.Deb            = val(2);
Contours.Fin            = val(1);

Contours.StepColor = 255 * uisetcolor([0 0 0], 'Define the color for regular contours'); % [255 0 0];
if length(Contours.StepColor) == 1
    Contours.StepColor = [0 0 0];
end
Contours.AnnotationStepColor = 255 * uisetcolor([0 0 0], 'Define the color for annotated contours'); % [0 255 0];
if length(Contours.AnnotationStepColor) == 1
    Contours.AnnotationStepColor = [0 0 0];
end
