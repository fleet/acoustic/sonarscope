function this = init_BathyCel(this)

Time = this.Sonar.Time.timeMat;
dIfr = dayMat2Ifr(Time(1));
[Jour, Mois, Annee] = dayIfr2Jma(dIfr);%#ok

Latitude  = this.Sonar.FishLatitude;
Longitude = this.Sonar.FishLongitude;
sub = find(~isnan(Latitude) & ~isnan(Latitude));
if isempty(sub)
    Depth       = [-12000 0]';
    Temperature = [10 10]';
    Salinity    = [35 35]';
    SoundSpeed  = [1500 1500]';
    str1 = 'Aucune position g�ographique transmise dans "init_BathyCel", la c�l�rit� est fix�e arbitraitement � 1500 m/s.';
    str2 = 'No geographic position sent to "init_BathyCel", the celerity aar arbitraary set to 1500 m/s.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'init_BathyCelNoPositionGeographique');
else
    LatitudeMoyenne  = median(Latitude(sub));
    LongitudeMoyenne = median(Longitude(sub));
    
    Sonar = this.Sonar.Desciption;
    Freq = get(Sonar, 'Signal.Freq');
    
    z = cl_sound_speed('Incoming', 2, 'LevitusAveragingType', 3, ...
        'Latitude', LatitudeMoyenne, 'Longitude', LongitudeMoyenne, ...
        'Frequency', Freq, 'LevitusMonth', Mois);
    
    Depth              = get(z, 'Depth');
    % AveragedAbsorption = get(z, 'AveragedAbsorption');
    Temperature        = get(z, 'Temperature');
    Salinity           = get(z, 'Salinity');
    SoundSpeed         = get(z, 'SoundSpeed');
end

this.Sonar.BathyCel.Z = Depth;
this.Sonar.BathyCel.T = Temperature;
this.Sonar.BathyCel.S = Salinity;
this.Sonar.BathyCel.C = SoundSpeed;
