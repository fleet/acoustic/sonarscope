% In-paints over holes in an image (holes = NaN values)
%
% Syntax
%  b = inpaint(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Method : 1 � 5
%       method == 0 --> (DEFAULT) see method 1, but
%         this method does not build as large of a
%         linear system in the case of only a few
%         NaNs in a large array.
%         Extrapolation behavior is linear.
%
%       method == 1 --> simple approach, applies del^2
%         over the entire array, then drops those parts
%         of the array which do not have any contact with
%         NaNs. Uses a least squares approach, but it
%         does not modify known values.
%         In the case of small arrays, this method is
%         quite fast as it does very little extra work.
%         Extrapolation behavior is linear.
%
%       method == 2 --> uses del^2, but solving a direct
%         linear system of equations for nan elements.
%         This method will be the fastest possible for
%         large systems since it uses the sparsest
%         possible system of equations. Not a least
%         squares approach, so it may be least robust
%         to noise on the boundaries of any holes.
%         This method will also be least able to
%         interpolate accurately for smooth surfaces.
%         Extrapolation behavior is linear.
%
%       method == 3 --+ See method 0, but uses del^4 for
%         the interpolating operator. This may result
%         in more accurate interpolations, at some cost
%         in speed.
%
%       method == 4 --+ Uses a spring metaphor. Assumes
%         springs (with a nominal length of zero)
%         connect each node with every neighbor
%         (horizontally, vertically and diagonally)
%         Since each node tries to be like its neighbors,
%         extrapolation is as a constant function where
%         this is consistent with the neighboring nodes.
%
%       method == 5 --+ See method 2, but use an average
%         of the 8 nearest neighbors to any element.
%         This method is NOT recommended for use.
%
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = Lena;
%   I = double(I);
%   N = 100;
%   d = randi(246, [N 2]);
%   for k=1:size(d,1)
%       subx = d(k,1) + (1:10);
%       suby = d(k,2) + (1:10);
%       I(suby,subx) = NaN;
%   end
%   a = cl_image('Image', I, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = inpaint(a);
%   imagesc(b);
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = inpaint(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('inpaint'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = inpaint_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = X;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = inpaint_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Method]  = getPropertyValue(varargin, 'Method', 0);
[varargin, Mask]    = getPropertyValue(varargin, 'Mask', []);
[varargin, ValMask] = getPropertyValue(varargin, 'ValMask', []); %#ok<ASGLU>

that = [];

%% Algorithm

switch this.ImageType
    case 2 % Image en RGB
        this = RGB2Intensity(this);
    case 3 % Image ind�x�e
        this = Indexed2Intensity(this);
end

I = this.Image(suby,subx,:);
I = singleUnlessDouble(I, this.ValNaN);

% I = inpaint_nans(I, Method);
sz = size(I);
N = max(sz);

%{
[SizePhysTotalMemory, SizePhysFreeMemory] = mexPCGetPhysicalMemory;
(SizePhysFreeMemory * 1000) / (N^2 * 4)
N = min(N, ...);
%}

flag = 0;
while ~flag
    try
        str1 = sprintf('Tentative de traitement "Inpainting" sur des fen�tre [%d %d]. Si cela �choue je retenterai en divisant la hauteur et la largeur par 2.', N, N);
        str2 = sprintf('Attemps to process "Inpainting" with [%d %d] windows. If is fails I will retry using smaller windows.', N, N);
        WorkInProgress(Lang(str1,str2))
        
        W = floor(N/2);
        
        % TODO : tester la m�thode Matlab "J = inpaintCoherent(I,mask);"
        
        fun = @(block_struct) inpaint_nans(block_struct.data, Method);
        I = blockproc(I, [N N], fun, 'BorderSize', [W W]);
        flag = 1;
    catch ME
        switch ME.identifier
            case 'images:blockproc:userfunError' % Cas rencontr� avant modif de inpaint_nans
                str1 = sprintf('La fonction "inpainting" par blocks de [%d %d] a �chou� (%s), je retente l''op�ration en prenant un block plus petit', N, N, ME.message);
                str2 = sprintf('The function "inpainting" by blocks of [%d %d] failed (%s), I try to redo the processing using smaller blocks.', N, N, ME.message);
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'InpaintingFailed');
                N = floor(N/2);
                if N < 128
                    str1 = sprintf('La fonction "inpainting"  a �chou� (%s).', ME.message);
                    str2 = sprintf('The function "inpainting" failed (%s).', ME.message);
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'InpaintingFailed2');
                    return
                end
            otherwise
                str1 = sprintf('La fonction "inpainting" par blocks de [%d %d] a �chou� (%s), je retente l''op�ration en prenant un block plus petit', N, N, ME.message);
                str2 = sprintf('The function "inpainting" by blocks of [%d %d] failed (%s), I try to redo the processing using smaller blocks.', N, N, ME.message);
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'InpaintingFailed');
                N = floor(N/2);
                if N < 128
                    str1 = sprintf('La fonction "inpainting"  a �chou� (%s).', ME.message);
                    str2 = sprintf('The function "inpainting" failed (%s).', ME.message);
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'InpaintingFailed2');
                    return
                end
        end
        
        %{
if (strcmp(ME.identifier,'MATLAB:catenate:dimensionMismatch'))

msg = sprintf('%s', ...
'Dimension mismatch occured: First argument has ', ...
num2str(size(A,2)), ' columns while second has ', ...
num2str(size(B,2)), ' columns.');
error('MATLAB:myCode:dimensions', msg);

% Display any other errors as usual.
else
rethrow(ME);
end
        %}
        
    end
end
% I = single(I);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'inpaint');
that.Comments = 'Take care, this image has been "inpainted"';

%% Maskage

if ~isempty(Mask)
    that = masquage(that, 'LayerMask', Mask, 'valMask', ValMask, 'zone', 1);
end
