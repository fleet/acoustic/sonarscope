% Insertion d'une image dans une autre
%
% Syntax
%    b = insertion(this, b, ...)
%
% Input Arguments
%   this  : Instance de cl_image
%   b     : Instances des layers conditionnels
%
% Name-Value Pair Arguments
%   subx :
%   suby :
%
% Output Arguments
%   b : Instance de cl_image
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a);
%   b = extraction(a, 'suby', 120:140, 'subx', 110:180)
%   imagesc(b)
%
%   c = insertion(2*mean(b)-b, a, []);
%   imagesc(c)
%   imagesc(2*mean(b)-b)
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = insertion(this, b, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_insertion(this(k), b, varargin{:});
end


function this = unitaire_insertion(this, b, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

I = this.Image(suby, subx);
this = set_val_xy(b, this.x(subx), this.y(suby), I);

