function [flag, this] = interactive_histogramCleaning(this, Windows, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = testSignature(this, 'ImageType', 1);
if ~flag
    return
end

ImageName = this.Name;

nbLigBlock = Windows(2);
nbColBlock = Windows(1);

X      = this.Image(suby,subx);
Masque = zeros(size(X), 'single');

fig1 = figure;
nbRows = length(suby);
nbCol = length(subx);
for iCol=1:nbColBlock:nbCol
    subc = iCol:min(nbCol, iCol+nbColBlock-1);
    for iLig=1:nbLigBlock:nbRows
        subl = iLig:min(nbRows, iLig+nbLigBlock-1);
        Y = this.Image(suby(subl),subx(subc));
        M = ~isnan(Y);
        [N, bins] = histo1D(Y);
        if isempty(N)
            continue
        end
        
        figure(fig1)
        imagesc(subx(subc), suby(subl), Y); axis xy; colorbar
        
        sub = getFlaggedBinsOfAnHistogram(bins, N);
        
        pas = bins(2) - bins(1);
        for k1=1:length(sub)
            k = sub(k1);
            I = (Y >= (bins(k)-pas/2) & (Y <= (bins(k)+pas/2)));
            Y(I) = NaN;
            M(I) = 0;
        end
        I = (Y < (bins(1)-pas/2));
        Y(I) = NaN;
        M(I) = 0;
        
        I = (Y > (bins(end)+pas/2));
        Y(I) = NaN;
        M(I) = 0;
        
        X(subl,subc) = Y;
        Masque(subl,subc) = M;
        
        figure(fig1)
        imagesc(subx(subc), suby(subl), Y); axis xy; colorbar
    end
end
my_close(fig1)

%% Cr�ation de l'instance

this = replace_Image(this, X);

%% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'HistoCleaning');

%% Masque

this(2) = replace_Image(this(1), Masque);
this(2).DataType           = cl_image.indDataType('Mask');
this(2) = compute_stats(this(2));
this(2).TagSynchroContrast = num2str(rand(1));
this(2).CLim               = [this(2).StatValues.Min this(2).StatValues.Max];
this(2).ColormapIndex      = 3;
this(2).Name               =  ImageName;
this(2) = update_Name(this(2), 'Append', 'HistoCleaning');

flag = 1;
