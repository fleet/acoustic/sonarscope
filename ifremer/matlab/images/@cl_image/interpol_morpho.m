% TODO : trouver un autre nom
% Interpolation of missing values according to a radius
%
% Syntax
%   b = interpol_morpho(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Rayon : Rayon d'interpolation (1 par defaut)
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   b = interpol_morpho(a);
%   imagesc(b);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = interpol_morpho(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('interpol_morpho'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = interpol_morpho_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = interpol_morpho_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Rayon] = getPropertyValue(varargin, 'Rayon', 1); %#ok<ASGLU>

%% Algorithm

Image = this.Image(suby,subx,:);

B = masque_englobant(this, 'Rayon', Rayon, 'subx', subx, 'suby', suby, 'NoStats');
B = get(B, 'Image');

sz_B = size(B);
nbSlides = this.nbSlides;
if nbSlides == 1
    if isnan(this.ValNaN)
        [I, J] = find(B & isnan(Image));
    else
        [I, J] = find(B & (Image == this.ValNaN));
    end
    X = escargot(Image, I, J, this.ValNaN, 'Rayon', Rayon);
    clear I J
    if isnan(this.ValNaN) % On refait un find car le sub2ind ne passe pas quand les images sont trop grandes (Out of Memory)
        sub = find(B & isnan(Image));
    else
        sub = find(B & (Image == this.ValNaN));
    end
    Image(sub) = X(:);
else
    if isnan(this.ValNaN)
        [I, J] = find(B & isnan(sum(Image,3)));
    else
        [I, J] = find(B & sum((Image == this.ValNaN), 3));
    end
    X = escargot(Image, I, J, this.ValNaN, 'Rayon', Rayon);
    for iCan=1:nbSlides
        Image(sub2ind([sz_B nbSlides], I, J, ones(size(I)) * iCan)) = X(:,iCan);
    end
end
clear B

%% Output image

Parameters = getHistoryParameters(Rayon);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'interpol_morpho', 'Parameters', Parameters);


function Cor = escargot(Image, I, J, ValNaN, varargin)

[varargin, Rayon] = getPropertyValue(varargin, 'Rayon', 1); %#ok<ASGLU>

[nbRows, nbColumns, nbSlides] = size(Image);

sub = -Rayon:Rayon;
[X,Y] = meshgrid(sub, sub);
D = X .^ 2 + Y .^ 2;
[~, sub] = sort(D(:));

switch class(Image)
    case 'double'
        Cor = ones(length(I), nbSlides) * ValNaN;
    otherwise
        Cor = ones(length(I), nbSlides, 'single') * ValNaN;
end

N = length(I);
hw = create_waitbar('Morphological Interpolation', 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw);
    for k2=2:length(sub)
        iLig = I(k1) + X(sub(k2));
        iCol = J(k1) + Y(sub(k2));
        if (iLig >= 1) && (iLig <= nbRows) && (iCol >= 1) && (iCol <= nbColumns)
            Val = Image(iLig, iCol,:);
            
            if isnan(ValNaN)
                if ~isnan(Val)
                    Cor(k1, :) = Val;
                    break
                end
            else
                if Val ~= ValNaN
                    Cor(k1, :) = Val;
                    break
                end
            end
            
        end
    end
end
my_close(hw, 'MsgEnd')
