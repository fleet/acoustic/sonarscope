function [flag, this] = interpolateLayesGeoswath(this, indImage)

SonarDescription = get_SonarDescription(this(indImage));

SonarFamily = get(SonarDescription, 'Sonar.Family');
if SonarFamily == 1 % Sonar Lat�ral
    SonarName = get(SonarDescription, 'Sonar.Name');
    if ~strcmp(SonarName, 'GeoSwath+') && ~strcmp(SonarName, 'GeoSwath')
        flag = 1;
        return
    end
end

indLayerAcrossDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AcrossDist', 'WithCurrentImage', 1);
AcrossDistance = this(indLayerAcrossDistance);

indLayerAlongDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AlongDist', 'WithCurrentImage', 1);
AlongDistance = this(indLayerAlongDistance);

indLayerBathymetry = findIndLayerSonar(this, indImage, 'strDataType', 'Bathymetry', 'WithCurrentImage', 1);
Bathymetry = this(indLayerBathymetry);

indLayerAngles = findIndLayerSonar(this, indImage, 'strDataType', 'BeamPointingAngle', 'WithCurrentImage', 1);
Angles = this(indLayerAngles(end)); % TODO : 

Height = this(indImage).Sonar.Height(:,:);

nbPings  = Bathymetry.nbRows;
% nbColons = Bathymetry.nbColumns;
x = Bathymetry.x;
subBab = (x < 0);
subTri = (x > 0);
xBab = x(subBab);
xTri = x(subTri);
for k1=1:nbPings
    z = Bathymetry.Image(k1,:);
    z = compbeTrous(z, xBab, xTri, subBab, subTri, Height(k1,:));
    Bathymetry.Image(k1,:) = z;
    
    z = AcrossDistance.Image(k1,:);
    z = compbeTrous(z, xBab, xTri, subBab, subTri, Height(k1,:));
    AcrossDistance.Image(k1,:) = z;
    
    z = AlongDistance.Image(k1,:);
    z = compbeTrous(z, xBab, xTri, subBab, subTri, Height(k1,:));
    AlongDistance.Image(k1,:) = z;
    
    z = Angles.Image(k1,:);
    z = compbeTrous(z, xBab, xTri, subBab, subTri, Height(k1,:));
    Angles.Image(k1,:) = z;
end

this(indLayerAcrossDistance) = AcrossDistance;
this(indLayerAlongDistance)  = AlongDistance;
this(indLayerBathymetry)     = Bathymetry;
this(indLayerAngles(end))    = Angles; % TODO : 

flag = 1;


function z = compbeTrous(z, xBab, xTri, subBab, subTri, Height)

zBab = z(subBab);
zTri = z(subTri);

subLessThanH = xBab < Height(1);
subModeThanH = xTri > abs(Height(2));
zBabFloor = zBab(subLessThanH);
zTriFloor = zTri(subModeThanH);
zJoint = [zBabFloor zTriFloor];

subNaN    = find(isnan(zJoint));
subNonNaN = find(~isnan(zJoint));
if isempty(subNonNaN)
    return
end

zJoint(subNaN) = interp1(subNonNaN, zJoint(subNonNaN), subNaN);

n = length(zBabFloor);
zBabFloor = zJoint(1:n);
zTriFloor = zJoint((n+1):end);

zBab(subLessThanH) = zBabFloor;
zTri(subModeThanH) = zTriFloor;

z(subBab) = zBab;
z(subTri) = zTri;
