% Calcul de l'intersection entre differentes images
%
% Syntax
%   [XLim, YLim, subx, suby, ...] = intersectionImagesArnaud(a, subx, suby, ...)
%
% Input Arguments 
%   a    : Instance de cl_image
%   subx : Indices en x de l'image
%   suby : Indices en y de l'image
%
% Name-Value Pair Arguments 
%   b : Instance de cl_image
%   c : Instance de cl_image
%   ...
%
% Name-only Arguments
%   SameSize : Surechantillonnage ou sous-echantillonage des images b, c, ...
%              pour obtenir la meme taille que a
%
% Output Arguments 
%   XLim  : Limites en x
%   YLim  : Limites en y
%   subx  : Indices en x de l'intersection de l'image a
%   suby  : Indices en y de l'intersection de l'image a
%   subxb : Indices en x de l'intersection de l'image b
%   subyb : Indices en y de l'intersection de l'image b
%   subxc : Indices en x de l'intersection de l'image c
%   subyc : Indices en y de l'intersection de l'image c
%   ...
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [XLim, YLim, subx, suby, varargout] = intersectionImagesArnaud(this, subx, suby, varargin)

[varargin, SameSize] = getFlag(varargin, 'SameSize');

XLim = compute_XYLim(this.x(subx));
YLim = compute_XYLim(this.y(suby));

for i=1:length(varargin)
    a = varargin{i};
    XLim_a = [min(a.x) max(a.x)];
    YLim_a = [min(a.y) max(a.y)];
    
    XLim(1) = max(XLim(1), XLim_a(1));
    XLim(2) = min(XLim(2), XLim_a(2));
    YLim(1) = max(YLim(1), YLim_a(1));
    YLim(2) = min(YLim(2), YLim_a(2));
end

subx = find((this.x >= XLim(1)) & (this.x <= XLim(2)));
nx = length(subx);
suby = find((this.y >= YLim(1)) & (this.y <= YLim(2)));
ny = length(suby);
if (nx == 0) || (ny == 0)
    XLim = [];
    YLim = [];
    subx = [];
    suby = [];
    for i=1:(nargout-4)
        varargout{i} = []; %#ok<AGROW>
    end
    return
end

for i=1:length(varargin)
    a = varargin{i};
    subxa = find((a.x >= XLim(1)) & (a.x <= XLim(2)));
    subya = find((a.y >= YLim(1)) & (a.y <= YLim(2)));
    if SameSize
        subxa = subxa(floor(linspace(1, length(subxa), length(subx))));
        subya = subya(floor(linspace(1, length(subya), length(suby))));
        
        DeltaX1 = abs(this.x(2) - this.x(1));
        DeltaX2 = abs(a.x(2) - a.x(1));
        Rapport = DeltaX1 / DeltaX2;
        if Rapport > 1.01
            str1 = sprintf('Les 2 images n''ont pas la m�me taille, la r�solution de "%s" sera utilis�e. L''image "%s" sera sous-�chantillonn�e (facteur %f)', ...
                this.Name, a.Name, DeltaX1/DeltaX2);
            str2 = sprintf('The 2 images have not the same size, the resolution of "%s" is used. The image "%s" will be subsampled (factor %f)', ...
                this.Name, a.Name, DeltaX1/DeltaX2);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'The2ImagesHAveNotTheSameResolution', 'TimeDelay', 300);
        elseif Rapport < 0.99
            str1 = sprintf('Les 2 images n''ont pas la m�me taille, la r�solution de "%s" sera utilis�e. L''image "%s" sera sur-�chantillonn�e (facteur %f)', ...
                this.Name, a.Name, DeltaX1/DeltaX2);
            str2 = sprintf('The 2 images have not the same size, the resolution of "%s" is used. The image "%s" will be oversampled (factor %f)', ...
                this.Name, a.Name, DeltaX1/DeltaX2);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'The2ImagesHAveNotTheSameResolution', 'TimeDelay', 300);
        end
    end
    
    if sign(this.y(2) - this.y(1)) == sign(a.y(2) - a.y(1))
        varargout{2*i-1} = subxa; %#ok<AGROW>
        varargout{2*i}   = subya; %#ok<AGROW>
    else
        varargout{2*i-1} = subxa; %#ok<AGROW>
        varargout{2*i}   = fliplr(subya); %#ok<AGROW>
    end
    nx = min(nx, length(subxa));
    ny = min(ny, length(subya));
end
subx = subx(1:nx);
suby = suby(1:ny);

XLim = compute_XYLim(this.x(subx));
YLim = compute_XYLim(this.y(suby));

% Modifs GLT 31/08/2009 : sinon l'argument sameSize ne sert � rien.
% for i=1:length(varargin)
%     varargout{2*i-1} = varargout{2*i-1}(1:nx);
%     varargout{2*i  } = varargout{2*i  }(1:ny);
% end
