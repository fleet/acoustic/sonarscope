% Inverse (1/X) of image
%
% Syntax
%   b = inv(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Remarks : Values equal to 0 are set to NaN
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = inv(a);
%   imagesc(b);
%
% See also cl_image/mrdivide Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = inv(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('inv'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = inv_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = inv_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx,:);
I = singleUnlessDouble(I, this.ValNaN);
I(I == 0) = NaN;
I = 1 ./ I;

%% Output image

that = inherit(this, I,'subx',  subx, 'suby', suby, 'AppendName', 'inv');
