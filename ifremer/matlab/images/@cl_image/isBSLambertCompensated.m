function [flag, flagLambertComp] = isBSLambertCompensated(this)
    
flagLambertComp = 0;

identReflectivity = cl_image.indDataType('Reflectivity');

flag = testSignature(this, 'DataType', identReflectivity);
if ~flag
    return
end

%% BS

if this.Sonar.BS.etat == 1
    if this.Sonar.BS.origine == 1
        if this.Sonar.BS.origineBelleImage == 1
            flagLambertComp = 1;
        end
    end
end
