function [flag, str] = isBestBSStatus(this)

str = {};

identReflectivity = cl_image.indDataType('Reflectivity');

flag = testSignature(this, 'DataType', identReflectivity);
if ~flag
    return
end

%% Transmission loss

if this.Sonar.TVG.etat == 1
    switch this.Sonar.TVG.origine
        case 1
            flag = 0;
    end
else
    flag = 0;
end

%% Tx diagrams

if this.Sonar.DiagEmi.etat == 1
    if this.Sonar.DiagEmi.origine == 1 % 'TxDiag : Compensated, RT
%         flag = 0; % TODO : ajouter TxDiagCompTolerance en param�tre et faire un if
    end
else
    flag = 0;
end

%% Insonified area

if this.Sonar.AireInso.etat == 1
    if this.Sonar.AireInso.origine ~= 3
    	flag = 0;
    end
else
    flag = 0;
end

%% BS

if this.Sonar.BS.etat == 1
    flag = 0;
end

if ~flag
    str1 = {sprintf('The expected BS Status for "%s" is :', this.Name)};
    str1{end+1} = ' ';
    str1{end+1} = 'TL : Compensated, SSc, Analytical';
    str1{end+1} = 'TxDiag : Compensated, RT, Analytical';
    str1{end+1} = 'RxDiag : Not compensated';
    str1{end+1} = 'IA : Compensated, SSc IncidenceAngle';
    str1{end+1} = 'BS : Not compensated';
    
    [~, ~, cstr2] = get_BS_Status(this);
    str1{end+1} = ' ';
    str1{end+1} = ' ';
    str1{end+1} = 'The BS status of the current image is :';
    str1{end+1} = ' ';
    
    str3 = {' '};
    str3{end+1} = ' ';
    str3{end+1} = 'This can be achieved if you load the layers "AbsorptionCoefficientRT", "AbsorptionCoefficientSSc", "IncidenceAngle" and "InsonifiedAreaSSc"';
    
    str = [str1 cstr2 str3];
    str = cell2str(str);
end
