function rep = isComingFromKonsbergMBES(this)

SonarCompany = get(this.Sonar.Desciption, 'SonarConstructeur');
if strcmp(SonarCompany, 'Kongsberg')
    rep = 1;
else
    SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
    switch SonarName
        case {'EM2040'; 'EM122'; 'EM710'; 'EM302'; 'EM300'; 'EM1002'; 'EM3002'; 'EM3000D'; 'EM300S'}
            rep = 1;
        otherwise
            rep = 0;
    end
end
