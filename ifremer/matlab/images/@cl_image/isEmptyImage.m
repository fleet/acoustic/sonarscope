function [flag, Rep] = isEmptyImage(this, varargin)

nbImages = length(this);
Rep = false(nbImages, 1);
for k=1:nbImages
    Rep(k) = unitaire_isEmptyImage(this(k), varargin{:});
end
flag = 1;


function Rep = unitaire_isEmptyImage(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

Rep = false;

if (this.nbRows == 0) || (this.nbColumns == 0)
    Rep = true;
    return
end

switch this.ImageType
    case 1 % Intensity
        subNaN = findNaN(this, 'subx', subx, 'suby', suby);
        if length(subNaN) == (length(subx)*length(suby))
            Rep = true;
        end
    case 2 % RGB
        subNaN = findNaN(this, 'subx', subx, 'suby', suby);
        if length(subNaN) == (length(subx)*length(suby)*3)
            Rep = true;
        end
    case 3 % Indexed
        subNaN = findNaN(this, 'subx', subx, 'suby', suby);
        if length(subNaN) == (length(subx)*length(suby))
            Rep = true;
        end
    case 4 % Boolean
        my_breakpoint
        'Compléter isEmptyImage pour image Booleen'
        Rep = false;
end
