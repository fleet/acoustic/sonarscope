% Test d'apr�s le type de donnee si le contenu de la donnee est une valeur entiere
%
% Syntax
%   rep = isIntegerVal(a)
% 
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   rep : 1 si c'est une valeur entiere, 0 sinon
%
% Examples 
%
% See also cl_image cl_image/mean Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function val = isIntegerVal(this)

ident{1}     = cl_image.indDataType('Mask');
ident{end+1} = cl_image.indDataType('TxBeamIndex');
ident{end+1} = cl_image.indDataType('TxBeamIndexSwath');
ident{end+1} = cl_image.indDataType('RxBeamIndex');
ident{end+1} = cl_image.indDataType('AveragedPtsNb');
ident{end+1} = cl_image.indDataType('Texture');
ident{end+1} = cl_image.indDataType('DetectionType');
ident{end+1} = cl_image.indDataType('RayPathSampleNb');
ident{end+1} = cl_image.indDataType('Segmentation');
ident{end+1} = cl_image.indDataType('PortStarboard');
% ident{end+1} = cl_image.indDataType('KongsbergQualityFactor'); % Comment� par JMA le 20/10/2018 pour Giacomo

for k=1:length(this)
    switch this(k).DataType
        case ident
            val(k) = 1;%#ok
        otherwise
            val(k) = 0; %#ok
    end
end
