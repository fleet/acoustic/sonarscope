function flag = isSonarSignal(this, nomSignal)

flag = isfield(this.Sonar, nomSignal) && ~isempty(this.Sonar.(nomSignal));