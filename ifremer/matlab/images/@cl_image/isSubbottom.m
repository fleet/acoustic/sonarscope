function flag = isSubbottom(this, varargin)

[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', []); %#ok<ASGLU>

SonarFamily = get(this.Sonar.Desciption, 'Sonar.Family');
if (SonarFamily == 3) || (SonarFamily == 4)
    if isempty(GeometryType)
        GeometryType = [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingDepth')];
    end
    identReflectivity = cl_image.indDataType('Reflectivity');
    flag = testSignature(this, 'DataType', identReflectivity, ...
        'GeometryType', GeometryType);
else
    str1 = 'Cette image n''est pas de type "Subbottom" ou "Fishing".';
    str2 = 'This image is not of "Subbottom" or "Fishing" type.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
