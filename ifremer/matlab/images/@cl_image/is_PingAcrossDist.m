% Test if the geometry of the image is PingAcrossDist
% TODO : this function could be replaced bay a call to testSignature
%
% Syntax
%   flag = is_PingAcrossDist(a) 
%
% Input Arguments
%   a : instance(s) de cl_image
%
% Output Arguments
%   flag : 0 ou 1
%
% Examples
%   a = TODO
%   flag = is_PingAcrossDist(a)
%
% See also is_PingSamples Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = is_PingAcrossDist(this)

for k=1:length(this)
    if this(k).GeometryType == cl_image.indGeometryType('PingAcrossDist')
        flag(k) = 1; %#ok<AGROW>
    else
        flag(k) = 0; %#ok<AGROW>
    end
end
