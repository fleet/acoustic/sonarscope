% Test if the geometry of the image is PingSamples
% TODO : this function could be replaced bay a call to testSignature
%
% Syntax
%   flag = is_PingSamples(a) 
%
% Input Arguments
%   a : instance(s) de cl_image
%
% Output Arguments
%   flag : 0 ou 1
%
% Examples
%   a = TODO
%   flag = is_PingSamples(a)
%
% See also is_PingAcrossDist Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = is_PingSamples(this)

for k=1:length(this)
    if this(k).GeometryType == cl_image.indGeometryType('PingSamples')
        flag(k) = 1; %#ok<AGROW>
    else
        flag(k) = 0; %#ok<AGROW>
    end
end