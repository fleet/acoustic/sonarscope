% Verification si on peut calculer des pentes sur l'image
%
% Syntax
%   flag = is_metrique(a) 
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   flag : 1 si on peut appeler les fonctions de calcul de pente
%
% Examples
%   [Z, lon, lat] = Etopo('LatLim', [43 50], 'LonLim', [-5 8]);
%   a = cl_image('Image', Z, 'Unit', 'm', 'Name', 'France', ...
%                'CLim', [0 500], 'ColormapIndex', 3, ...
%                'x', lon, 'y', lat, ...
%                'XLabel', 'Longitude', 'YLabel', 'Latitude', ...
%                'XUnit', 'deg', 'YUnit', 'deg', 'GeometryType', cl_image.indGeometryType('LatLong'));
%   imagesc(a)
%   [flag, msg] = is_metrique(a)
%   if flag, plusGrandePente(a); end
%
% See also cl_image cl_image/gradient Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, msg] = is_metrique(this)

flag = 1;
msg = [];

if this.GeometryType == cl_image.indGeometryType('Unknown')
    str = this.strGeometryType{this.GeometryType};
    msg{end+1} = sprintf('GeometryType <-> %s : Impossible de calculer les pentes dans ces conditions', str);
    flag = 0;
end

if ~(strcmp(this.Unit, 'm') || strcmp(this.Unit, 'meters') || strcmp(this.Unit, 'metres') || strcmp(this.Unit, 'm�tres'))
    str1 = sprintf('Unit <-> %s : L''unit� de l''image de bathym�trie doit �tre "m" (m�tres), il est impossible de calculer les pentes sans avoir l''assusrance que la donn�e est en m�tres. Vous pouvez changer l''unit� par "Info", ....', this.Unit);
    str2 = sprintf('Unit <-> %s : The unit of the "Bathymetry" image is not "m" (meters), it is impossible to compute the slopes if we have not the assurance that the units are in meters. You can change the "Unit" using "Info", "Set current Image", "Properties"...', this.Unit);
    msg{end+1} = Lang(str1,str2);
    flag = 0;
end

if this.GeometryType == cl_image.indGeometryType('GeoYX')    % Metrique
    if ~strcmp(this.XUnit, 'm')
        msg{end+1} = sprintf('XUnit <-> %s : Impossible de calculer les pentes dans ces conditions. Il faut specifier ''m''', this.XUnit);
        flag = 0;
    end
    if ~strcmp(this.YUnit, 'm')
        msg{end+1} = sprintf('YUnit <-> %s : Impossible de calculer les pentes dans ces conditions. Il faut specifier ''m''', this.YUnit);
        flag = 0;
    end
end

if this.GeometryType == cl_image.indGeometryType('LatLong')    % Geographique
    if ~strcmp(this.XUnit, 'deg')
        msg{end+1} = sprintf('XUnit <-> %s : Impossible de calculer les pentes dans ces conditions. Il faut specifier ''deg''', this.XUnit);
        flag = 0;
    end
    if ~strcmp(this.YUnit, 'deg')
        msg{end+1} = sprintf('YUnit <-> %s : Impossible de calculer les pentes dans ces conditions. Il faut specifier ''deg''', this.YUnit);
        flag = 0;
    end
end

if this.GeometryType == cl_image.indGeometryType('PingSamples')    % SonarD
    msg{end+1} = sprintf('GeometryType <-> %s : Impossible de calculer les pentes pour une image de ce type', this.GeometryType);
    flag = 0;
end

if this.GeometryType == cl_image.indGeometryType('PingAcrossDist')    % SonarX
    if ~strcmp(this.XUnit, 'm')
        msg{end+1} = sprintf('XUnit <-> %s : Impossible de calculer les pentes dans ces conditions. Il faut specifier ''m''', this.XUnit);
        flag = 0;
    end
    if ~strcmp(this.YUnit, 'm')
        msg{end+1} = sprintf('YUnit <-> %s : Impossible de calculer les pentes dans ces conditions. Il faut specifier ''m''', this.YUnit);
        flag = 0;
    end
end

%% Concatenation en une chaine

msg = cell2str(msg);
