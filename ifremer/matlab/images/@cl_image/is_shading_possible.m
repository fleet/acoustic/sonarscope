% Indique si �a vaut le coup d'ombrer l'image
%
% Syntax
%  flagShading = is_shading_possible(a)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Output Arguments
%   flagShading : 1= �a vaut la peine d'ombrer l'image
%                 0= �a ne vaut pas ...
%
% Examples
%   a = cl_image.import_Prismed;
%   flagShading = is_shading_possible(a)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flagShading = is_shading_possible(this)

DT(1)     = cl_image.indDataType('Bathymetry');
DT(end+1) = cl_image.indDataType('Log10Bathymetry');

flagShading = testSignature(this, 'DataType', DT, 'noMessage');
