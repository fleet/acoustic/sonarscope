% Array elements for pixels that are NaN
%
% Syntax
%   Y = isnan(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   Y : Boolean matrix
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a);
%     x = a.x; y = a.y;
%   subNaNa = isnan(a);
%     figure; imagesc(x, y, subNaNa); colorbar; axisGeo;
%
%     b = sunShading(a);
%     imagesc(b);
%   subNaNb = isnan(b);
%     figure; imagesc(x,y,subNaNb); colorbar; axisGeo;
%
% See also cl_image/sign Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Y = isnan(this)

%% Test� sans les memmapfile

if this.nbSlides == 1
    if isnan(this.ValNaN)
        Y = isnan(this.Image);
    else
        Y = (this.Image == this.ValNaN);
    end
else
    if isnan(this.ValNaN)
        Y = isnan(this.Image);
    else
        Y = (this.Image == this.ValNaN);
    end
    Y = sum(uint8(Y), 3, 'native');
    Y = (Y == this.nbSlides);
end
