% Logic operator "less or equal than : <="
%
% Syntax
%   b = a <= Val;
%   b = le(a, Val, ...)
%
% Input Arguments
%   a   : Instance(s) of cl_image
%   val : Threshold
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%     I = repmat(-100:100, 256, 1);
%     a = cl_image('Image', I);
%     imagesc(a);
%   b = le(a, 20);
%     imagesc(b);
%   b = (a <= 20);
%     imagesc(b);
%   b = le(a, 20, 'subx', 20:200, 'suby', 20:40);
%     imagesc(b);
%
% See also cl_image/gt Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = le(this, Val, varargin)
that = process_function_type2(this, @le, Val, varargin{:});
