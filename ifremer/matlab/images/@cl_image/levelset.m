function this = levelset(this, varargin)

nbImages = length(this);
for i=1:nbImages
    that(:,i) = levelset_unitaire(this(i), varargin{:}); %#ok<AGROW>
end
this = that(:);


function that = levelset_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Connectivity] = getPropertyValue(varargin, 'Connectivity', 8);
[varargin, Seuil]        = getPropertyValue(varargin, 'Seuil',        []); %#ok<ASGLU>

WorkInProgress('processing levelset')

I = single(this.Image(:,:));
subNaN = isnan(I);

mask = zeros(size(I));
mask(suby,subx) = true;

% set input parameters
lambda = 0.3;
iterations = 600;

demo_2(I(suby,subx)) % D:\perso-jma\Mes Documents\MATLAB\LevelSet\DRLSE_v0
seg = sfm_chanvese(I, mask, iterations, lambda);

I = single(seg);
I(subNaN) = NaN;
sub1 = (I == 0);
I(sub1) = NaN;

that(1) = replace_Image(this, I);
that(1).ValNaN = NaN;

%% Mise � jour de coordonn�es

that(1) = majCoordonnees(that(1), subx, suby);

%% Calcul des statistiques

that(1) = compute_stats(that(1));

%% Rehaussement de contraste

that(1).TagSynchroContrast = num2str(rand(1));
CLim = [that(1).StatValues.Min that(1).StatValues.Max];
that(1).CLim = CLim;
that(1).ColormapIndex = 3;

%% Compl�tion du titre

that(1).Unit = ' ';
that(1).DataType = cl_image.indDataType('Mask');
that(1) = update_Name(that(1), 'Append', 'Levelset');
