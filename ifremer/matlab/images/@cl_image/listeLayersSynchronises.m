% Liste des layers synchronises en X et en Y
%
% [flag, indLayers, nomLayers, DataTypes, GeometryType] = listeLayersSynchronises(this, indImage, ...)
%
% Input Arguments
%   this : instance de clc_image
%
% Name-only Arguments
%   IncludeCurrentImage : Inclu l'image elle-m�me dans la liste
%   memeDataType      : Meme type de donnee
%
% Output Arguments
%   flag         : 1=OK, 0=KO
%   indLayers    : Indices des image synchronisees en X et Y avec l'image courante
%   nomLayers    : Noms des images ...
%   DataTypes    : Type de donn�e ...
%   GeometryType : Type de g�om�trie
%
% See also findIndLayerSonar clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, indLayers, nomLayers, DataTypes, GeometryType] = listeLayersSynchronises(this, indImage, varargin)

% FONCTION A REPROGRAMMER ENTIEREMENT SOUS FORME
%    liste =
%    liste = get_liste_sameTagSynchroX(liste, ...)
%    liste = get_liste_sameTagSynchroY(liste, ...), etc ...

% TODO : remplacer ces getFlag par getPropertyValue
[varargin, IncludeCurrentImage]  = getFlag(varargin, 'IncludeCurrentImage');
[varargin, memeDataType]         = getFlag(varargin, 'memeDataType');
% [varargin, memeInitialFileName]  = getFlag(varargin, 'memeInitialFileName');
% [varargin, memeInitialImageName] = getFlag(varargin, 'memeInitialImageName');
% [varargin, memeImageName]        = getFlag(varargin, 'memeImageName');

[varargin, flagSynchroX]         = getPropertyValue(varargin, 'SynchroX',             1);
[varargin, flagSynchroY]         = getPropertyValue(varargin, 'SynchroY',             1);
[varargin, flagSynchroS]         = getPropertyValue(varargin, 'SynchroS',             1);

[varargin, memeInitialFileName]  = getPropertyValue(varargin, 'memeInitialFileName',  0);
[varargin, memeInitialImageName] = getPropertyValue(varargin, 'memeInitialImageName', 0);
[varargin, memeImageName]        = getPropertyValue(varargin, 'memeImageName',        0);
[varargin, flagOmitImageNumber]  = getPropertyValue(varargin, 'OmitImageNumber',      0);
[varargin, TypeDonneUniquement]  = getPropertyValue(varargin, 'TypeDonneUniquement',  []);
[varargin, OnlyOneLayer]         = getPropertyValue(varargin, 'OnlyOneLayer',         0);
[varargin, ExcludeDataType]      = getPropertyValue(varargin, 'ExcludeDataType',      []); %#ok<ASGLU>

flag = 1;
indLayers = 1:length(this);
N = length(this);

GeometryType     = NaN(N,1);
DataTypes        = NaN(N,1);
nomLayers        = cell(N,1);
txtSynchroX      = cell(N,1);
txtSynchroY      = cell(N,1);
InitialFileName  = cell(N,1);
InitialImageName = cell(N,1);

for k=1:N
    nomLayers{k}    = this(k).Name;
    txtSynchroX{k}  = this(k).TagSynchroX;
    txtSynchroY{k}  = this(k).TagSynchroY;
    GeometryType(k) = this(k).GeometryType;
    DataTypes(k)    = this(k).DataType;
    
    InitialFileName{k}  = this(k).InitialFileName;
    InitialImageName{k} = this(k).InitialImageName;
    
    if memeImageName % On triche car il faudrait rajouter encore un �tage dans les if et comme de toutes fa�ons il faut repenser compl�tement cette fonction ...
        Name = extract_ImageName(this(k));
        InitialImageName{k} = Name;
        % TODO : il serait souhaitable de limiter dans ce cas la comparaison sur le nombre de caract�res min entre les 2 mais attention, certaines images n'ont
        % pas de InitialImageName
    end
    
    if flagOmitImageNumber
        txtSynchroX{k}  = OmitImageNumber(txtSynchroX{k});
        txtSynchroY{k}  = OmitImageNumber(txtSynchroY{k});
    end
end
if memeImageName
    memeInitialImageName = 1;
end

if ~IncludeCurrentImage
    indLayers(indImage) = [];
end

if flagSynchroX
    subMemeTagSynchroX  = strcmp(txtSynchroX(indLayers), txtSynchroX(indImage));
else
    subMemeTagSynchroX = true(length(indLayers), 1);
end

if flagSynchroY
    subMemeTagSynchroY  = strcmp(txtSynchroY(indLayers), txtSynchroY(indImage));
else
    subMemeTagSynchroY = true(length(indLayers), 1);
end

if flagSynchroS
    subMemeTagSynchroS = (GeometryType(indLayers) == GeometryType(indImage));
else
    subMemeTagSynchroS = true(length(indLayers), 1);
end

subMemeDataTypes        = (DataTypes(indLayers)    == DataTypes(indImage));
subMemeInitialFileName  = strcmp(InitialFileName(indLayers), InitialFileName(indImage));
subMemeInitialImageName = strcmp(InitialImageName(indLayers), InitialImageName(indImage));

if isempty(TypeDonneUniquement)
    if memeDataType
        if memeInitialFileName
            indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeDataTypes & subMemeInitialFileName)) = [];
        else
            if memeInitialImageName
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeDataTypes & subMemeInitialImageName)) = [];
            else
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeDataTypes)) = [];
            end
        end
    else
        if memeInitialFileName
            indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeInitialFileName)) = [];
        else
            if memeInitialImageName
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeInitialImageName)) = [];
            else
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS)) = [];
            end
        end
    end
else
    subTypeDonneUniquement = (DataTypes(indLayers) == TypeDonneUniquement);
    if memeDataType
        if memeInitialFileName
            indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subTypeDonneUniquement & subMemeDataTypes & subMemeInitialFileName)) = [];
        else
            if memeInitialImageName
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subTypeDonneUniquement & subMemeDataTypes & subMemeInitialImageName)) = [];
            else
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subTypeDonneUniquement & subMemeDataTypes)) = [];
            end
        end
    else
        if memeInitialFileName
            indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subTypeDonneUniquement & subMemeInitialFileName)) = [];
            %{
figure;
h(1) = subplot(5,1,1); plot(subMemeTagSynchroX, '*'); grid on; title('subMemeTagSynchroX')
h(2) = subplot(5,1,2); plot(subMemeTagSynchroY, '*'); grid on; title('subMemeTagSynchroY')
h(3) = subplot(5,1,3); plot(subMemeTagSynchroS, '*'); grid on; title('subMemeTagSynchroS')
h(4) = subplot(5,1,4); plot(subTypeDonneUniquement, '*'); grid on; title('subTypeDonneUniquement')
h(5) = subplot(5,1,5); plot(subMemeInitialFileName, '*'); grid on; title('subMemeInitialFileName')
linkaxes(h, 'x')
            %}
        else
            if memeInitialImageName
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subTypeDonneUniquement & subMemeInitialImageName)) = [];
                %{
figure;
h(1) = subplot(5,1,1); plot(subMemeTagSynchroX, '*'); grid on; title('subMemeTagSynchroX')
h(2) = subplot(5,1,2); plot(subMemeTagSynchroY, '*'); grid on; title('subMemeTagSynchroY')
h(3) = subplot(5,1,3); plot(subMemeTagSynchroS, '*'); grid on; title('subMemeTagSynchroS')
h(4) = subplot(5,1,4); plot(subTypeDonneUniquement, '*'); grid on; title('subTypeDonneUniquement')
h(5) = subplot(5,1,5); plot(subMemeInitialImageName, '*'); grid on; title('subMemeInitialImageName')
linkaxes(h, 'x')
                %}
            else
                indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subTypeDonneUniquement)) = [];
            end
        end
    end
end

if ~isempty(ExcludeDataType)
    for k=1:length(ExcludeDataType)
        sub1 = (DataTypes(indLayers) == ExcludeDataType(k));
        indLayers(sub1)  = [];
    end
end

nomLayers    = nomLayers(indLayers);
DataTypes    = DataTypes(indLayers);
GeometryType = GeometryType(indLayers);

%% Cas o� on demande un seul layer

if OnlyOneLayer && (length(indLayers) > 1)
    [flag, indImage, strOut] = summary(this(indLayers), 'ExportResults', 0, ...
        'SelectLayer', length(indLayers), 'QueryMode', 'UniSelect');
    if ~flag
        % Bouton Cancel
        return
    end
    if isempty(indImage) || isempty(strOut)
        return
    end
    
    strIn = nomLayers;
    % Remise en ordre par identification du nom du layer.
    % On cherche juste l'indice dans le cas o� on a filtr� les
    % donn�es.
    strOut = strOut(indImage,1);
    % R�cup�ration des indices de tri.
    [C,ia,ib] = intersect(strIn, strOut, 'stable'); %#ok<ASGLU>
    choix = ia;
    
    nomLayers    = nomLayers(choix);
    DataTypes    = DataTypes(choix);
    indLayers    = indLayers(choix);
    GeometryType = GeometryType(choix);
end


function txtSynchroOut = OmitImageNumber(txtSynchroIn)
mots = strsplit(txtSynchroIn, '_');
n = length(mots);
if n >= 3
    sub = strfind(txtSynchroIn, '_');
    txtSynchroOut = txtSynchroIn(1:(sub(end)-1));
else
    txtSynchroOut = txtSynchroIn;
end
