function [flag, listeprofils, listeprofilsAngle, nomsLayers, nomsLayersAssocies] = listePairesLayersSynchronises(this, indImageCourante, DataTypeAssocie, varargin)

[varargin, listeprofils] = getPropertyValue(varargin, 'listeprofils', []);

[varargin, specialQuestionAssemblageMosaic] = getFlag(varargin, 'QuestionAssemblageMosaic'); %#ok<ASGLU>

if ischar(DataTypeAssocie)
    DataTypeAssocie = {DataTypeAssocie};
end

listeprofilsAngle  = [];
nomsLayersAssocies = [];

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImageCourante, 'memeDataType', 'IncludeCurrentImage');
if ~flag
    return
end

indImageCourante = find(indLayers == indImageCourante);
% if length(indLayers) == 1 % Comment� par JMA le 09/04/2013
%     if isempty(listeprofils)
%         listeprofils = indLayers;
%     end
%     listeprofilsAngle = [];
% else
if isempty(listeprofils)
    if (indImageCourante) > length(nomsLayers) % Rajout� par JMA le 09/04/2013
        str1 = 'Y''a un probl�me de programmation ici, SOS JMA.';
        str2 = 'There is a bug here, report to JMA please.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    if length(nomsLayers) == 1
        listeprofils = indLayers;
    else
        % [flag, indImage, strOut] = summary(this.Images(indLayers), 'ExportResults', 0, ... % GLU GLU GLU GLU GLU GLU
        [flag, indImage, strOut] = summary(this(indLayers), 'ExportResults', 0, ...
            'SelectLayer', indImageCourante, 'QueryMode', 'MultiSelect');
        if ~flag
            % Bouton Cancel
            return
        end
        if isempty(indImage) || isempty(strOut)
            % Filtre total ou bouton Cancel
            return
        end
        strIn = nomsLayers;
        % Remise en ordre par identification du nom du layer.
        strOut  = strOut(indImage,1);
        % R�cup�ration des indices de tri.
        [C,ia,ib] = intersect(strIn, strOut, 'stable');  %#ok<ASGLU>
        choix = ia;
        
        listeprofils = indLayers(choix);
    end
end


indAngle = [];
for k1=1:length(listeprofils)
    for k2=1:length(DataTypeAssocie)
        identAngle = cl_image.indDataType(DataTypeAssocie{k2});
        indAssocie = findIndLayerSonar(this, listeprofils(k1), 'DataType', identAngle);
        if ~isempty(indAssocie)
            indAngle = [indAngle indAssocie]; %#ok<AGROW>
            indAngle = unique(indAngle);
        end
    end
end


if isempty(indAngle)
    listeprofilsAngle = [];
else
    if specialQuestionAssemblageMosaic
        str1 = 'Gestion des recouvrements de profils par les angles d''�mission ?';
        str2 = 'Overlap conditioned by TxAngle ?';
        [choix, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
    else
        choix = 1;
    end
    if choix == 2
        listeprofilsAngle = [];
    else
        for k1=1:length(indAngle)
            InitialFileNameAngle{k1}  = this(indAngle(k1)).InitialFileName;   %#ok<AGROW>
            InitialImageNameAngle{k1} = this(indAngle(k1)).InitialImageName;  %#ok<AGROW>
            nbColumnsAngle(k1)        = this(indAngle(k1)).nbColumns;         %#ok<AGROW>
            nbLigAngle(k1)            = this(indAngle(k1)).nbRows;            %#ok<AGROW>
            NuIdentParent             = this(indAngle(k1)).NuIdentParent;
            if isempty(NuIdentParent)
                NuIdentParentAngle(k1) = floor(rand(1) * 1e8); %#ok<AGROW>
            else
                NuIdentParentAngle(k1) = NuIdentParent;  %#ok<AGROW>
            end
        end
        for k1=1:length(listeprofils)
            InitialFileName  = this(listeprofils(k1)).InitialFileName;
            InitialImageName = this(listeprofils(k1)).InitialImageName;
            NuIdentParent    = this(listeprofils(k1)).NuIdentParent;
            for k2=1:length(indAngle)
                if isempty(InitialFileName) || isempty(InitialFileNameAngle{k2})
                    if strcmp(InitialImageName, InitialImageNameAngle{k2}) || ...
                            (~isempty(NuIdentParent) && (NuIdentParent == NuIdentParentAngle(k2)))
                        listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                        break
                    else
                        nbColumns = this(listeprofils(k1)).nbColumns;
                        nbRows     = this(listeprofils(k1)).nbRows;
                        if (nbColumns == nbColumnsAngle(k2)) && (nbRows == nbLigAngle(k2))
                            listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                            break
                            
                            %                             elseif (abs(nbColumns - nbColumnsAngle(k2)) <= 2) && (abs(nbRows - nbLigAngle(k2)) <= 2)
                        elseif (abs(nbColumns - nbColumnsAngle(k2)) <= 3) && (abs(nbRows - nbLigAngle(k2)) <= 3)
                            % TODO  TODO TODO Bricole rajou�e sur le feu � Wellington pour
                            % palier � 1 pb lors du calcul de l'image
                            % d'incidence
                            listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                            break
                        end
                    end
                    
                elseif strcmp(InitialFileName, InitialFileNameAngle{k2})
                    if strcmp(InitialImageName, InitialImageNameAngle{k2})
                        listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                    else
                        nbColumns = this(listeprofils(k1)).nbColumns;
                        nbRows     = this(listeprofils(k1)).nbRows;
                        if (nbColumns == nbColumnsAngle(k2)) && (nbRows == nbLigAngle(k2))
                            listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                            break
                            
                            %                             elseif (abs(nbColumns - nbColumnsAngle(k2)) <= 2) && (abs(nbRows - nbLigAngle(k2)) <= 2)
                        elseif (abs(nbColumns - nbColumnsAngle(k2)) <= 3) && (abs(nbRows - nbLigAngle(k2)) <= 3)
                            % TODO  TODO TODO Bricole rajou�e sur le feu � Wellington pour
                            % palier � 1 pb lors du calcul de l'image
                            % d'incidence
                            listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                            break
                        else
                            'beurk dans listePairesLayersSynchronises';
                        end
                    end
                    
                elseif ~isempty(strfind(InitialFileName, InitialFileNameAngle{k2}))
                    if strcmp(InitialImageName, InitialImageNameAngle{k2})
                        listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                    else
                        nbColumns = this(listeprofils(k1)).nbColumns;
                        nbRows     = this(listeprofils(k1)).nbRows;
                        if (nbColumns == nbColumnsAngle(k2)) && (nbRows == nbLigAngle(k2))
                            listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                            break
                        end
                    end
                    
                else
                    nbColumns = this(listeprofils(k1)).nbColumns;
                    nbRows     = this(listeprofils(k1)).nbRows;
                    if (nbColumns == nbColumnsAngle(k2)) && (nbRows == nbLigAngle(k2))
                        if length(listeprofilsAngle) ~= k1 % Moyen d�tourn� pour savoir si l'image a �t� trouv�e avant (cas des images marmesonet)
                            listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                            break
                        end
                    end
                end
            end
            
            
            for k2=1:length(indAngle)
                if ~isempty(InitialFileName) && ~isempty(InitialFileNameAngle{k2}) && ...
                        strcmp(InitialFileName, InitialFileNameAngle{k2})
                    if strcmp(InitialImageName, InitialImageNameAngle{k2})
                        listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                    else
                        nbColumns = this(listeprofils(k1)).nbColumns;
                        nbRows     = this(listeprofils(k1)).nbRows;
                        if (nbColumns == nbColumnsAngle(k2)) && (nbRows == nbLigAngle(k2))
                            listeprofilsAngle(k1) = indAngle(k2); %#ok<AGROW>
                            break
                        end
                    end
                end
            end
        end
        
        if length(listeprofils) ~= length(listeprofilsAngle)
            str1 = 'SonarScope n''a pas r�ussi � appairer les images avec les images d''angles, vous allez devoir faire ce travail par vous-m�me';
            str2 = 'SonarScope did not succed to find the correspondances between Images and Angles, you are going to do this job by yourself.';
            my_warndlg(Lang(str1,str2), 1);
            for k1=1:length(indAngle)
                AngleName{k1} = this(indAngle(k1)).Name; %#ok<AGROW>
            end
            for k1=1:length(listeprofils)
                ImageName = this(listeprofils(k1)).Name;
                str1 = sprintf('Image Angle correspondant � %s',  ImageName);
                str2 = sprintf('Angle image corresponding to %s', ImageName);
                [rep, flag] = my_listdlg(Lang(str1,str2), AngleName, 'SelectionMode', 'Single', ...
                    'InitialValue', 1);
                if ~flag
                    return
                end
                listeprofilsAngle(k1) = indAngle(rep); %#ok<AGROW>
            end
        end
    end
end

if ~isempty(listeprofilsAngle) && (length(listeprofilsAngle) ~= length(listeprofils))
    str1 = 'TODO';
    str2 = 'Some images have not their corresponding Angle images. Please do the mosaic in 2 steps : one set with images without angle images and one with them';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if ~isempty(listeprofilsAngle) && (any(listeprofilsAngle == 0))
    sub = find(listeprofilsAngle == 0);
    ImageName = this(sub(1)).Name;
    str1 = 'TODO';
    str2 = sprintf('%s cound not be associated to any image. I suggest you gently zoom on it and perform an extraction on it AND on the associated image then redo the mosaic with these extracted images.', ImageName);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if isempty(listeprofils)
    flag = 0;
    return
end

for k=1:length(listeprofilsAngle)
    nomsLayersAssocies{k} = this(listeprofilsAngle(k)).Name; %#ok<AGROW>
end

flag = 1;
