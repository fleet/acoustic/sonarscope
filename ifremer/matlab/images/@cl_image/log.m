% Log of image
%
% Syntax
%  b = log(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Remarks : Negative values are processed as is : sign(x) .* log(abs(x));
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a);
%
%   b = log(a);
%   imagesc(b);
%
% See also cl_image/log10 cl_image/mpower Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = log(this, varargin)

if isempty(this.Unit)
    Unit = '';
else
    Unit = ['log10(' this.Unit ')'];
end

hFunc = @(x) sign(x) .* log(abs(x));
that = process_function_type1(this, hFunc, 'Unit', Unit, 'FctName', 'log', varargin{:});
