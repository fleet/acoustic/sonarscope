% Log10 of image
%
% Syntax
%  b = log10(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Abs  : Boolean : false -> log10(x), true -> sign(x)*abs(log10(x)) (Default : 0)
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Remarks : If Abs=0 negative values are set to NaN before computing log10
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a);
%
%   b = log10(a);
%   imagesc(b);
%
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = cl_image.import_ermapper(nomFic);
%   imagesc(a)
%
%   b = log10(a, 'Abs', 1);
%   imagesc(b)
%
% See also zlog10 cl_image/log cl_image/mpower Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = log10(this, varargin)

[varargin, Abs] = getPropertyValue(varargin, 'Abs', false);

if isempty(this.Unit)
    Unit = '';
else
    Unit = ['log10(' this.Unit ')'];
end

that = process_function_type2(this, @zlog10, Abs, 'Unit', Unit, varargin{:});
