% Fix coordinates
%
% Syntax
%   this = majCoordonnees(this, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments 
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%   x    : New abscissa (subx must be empty)
%   y    : New ordinates (suby must be empty)
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   b = majCoordonnees(a);
%   b = majCoordonnees(a, subx, suby);
%   b = majCoordonnees(a, [], [], x, y);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = majCoordonnees(this, varargin)

x = [];
y = [];
switch nargin
    case 1
        subx = 1:this.nbColumns;
        suby = 1:this.nbRows;
        
    case 3
        subx = varargin{1};
        suby = varargin{2};
        subx = round(subx);
        suby = round(suby);
        
    case 5
        x = varargin{3};
        y = varargin{4};
        this.x = x; % Rajout� par JMA le 03/03/2019
        this.y = y;
        
    otherwise
        str1 = 'Message pour JMA : mauvais nombre d''arguments dans majCoordonnees.';
        str2 = 'Message for JMA : bad number of parameters in majCoordonnees.';
        my_warndlg(Lang(str1,str2), 1);
end

if isempty(x)
    if isempty(subx)
        x = [];
    else
        x = linspace(this.x(subx(1)), this.x(subx(end)), length(subx));
    end
% else
%     subx = interp1(this.x, 1:this.nbColumns, x);
%     subx = floor(subx);
end

if isempty(y)
    if isempty(suby)
        y = [];
    else
        y = linspace(this.y(min(suby)), this.y(max(suby)), length(suby));
    end
else
    suby = interp1(this.y, 1:this.nbRows, y);
    suby = floor(suby);
end

this.XLim = compute_XYLim(x);
this.YLim = compute_XYLim(y);

this.NuIdentParent = this.NuIdent;
this.NuIdent = floor(rand(1) * 1e8);

if isempty(this.InitialImageName)
    this.InitialImageName = this.Name;
end

if this.GeometryType == cl_image.indGeometryType('PingSamples')
    if ~isdeployed
        % TODO disp('Message pour JMA : Correction a faire ici quand RecolutionD sera correctement d�fini pour RESON')
    end
    %     SampleFrequency = this.Sonar.SampleFrequency;
%     this.Sonar.ResolutionD = (x(end)-x(1)) / (length(x) - 1) * (1500/nanmean(SampleFrequency));
elseif this.GeometryType == cl_image.indGeometryType('PingRange')
    this.Sonar.ResolutionD = (x(end)-x(1)) / (length(x) - 1);
elseif this.GeometryType == cl_image.indGeometryType('PingAcrossDist')
    this.Sonar.ResolutionX = (x(end)-x(1)) / (length(x) - 1);
end

yNew = y;
this = sonar_copie_info(this, this, suby, yNew);

x = centrage_magnetique(x);
y = centrage_magnetique(y);

this.XLim = compute_XYLim(x);
this.YLim = compute_XYLim(y);

this.x = x;
this.y = y;
