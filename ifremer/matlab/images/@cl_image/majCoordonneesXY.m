% Mise a jour des coordonnees
%
% Syntax
%   this = majCoordonnees(this, x, y)
%
% Input Arguments
%   this : Instance de cl_image
%   x : 
%   y : 
%
% Output Arguments
%   this : Instance de cl_image
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = majCoordonneesXY(this, x, y)

% subx = 1:this.nbColumns;
suby = 1:this.nbRows;

this.XLim = compute_XYLim(x);
this.YLim = compute_XYLim(y);

for k=1:length(this.CourbesStatistiques)
    if isfield(this.CourbesStatistiques(k), 'x')
        this.CourbesStatistiques(k).x = intersect(this.CourbesStatistiques(k).x, x);
        this.CourbesStatistiques(k).y = intersect(this.CourbesStatistiques(k).y, y);
    end
end

this.NuIdentParent = this.NuIdent;
this.NuIdent = floor(rand(1) * 1e8);

if isempty(this.InitialImageName)
    this.InitialImageName = this.Name;
end

if this.GeometryType == cl_image.indGeometryType('PingSamples')
    if ~isdeployed
        % TODO disp('Message pour JMA : Correction a faire ici quand RecolutionD sera correctement d�fini pour RESON')
    end
%     SampleFrequency = this.Sonar.SampleFrequency;
%     this.Sonar.ResolutionD = (x(end)-x(1)) / (length(x) - 1) * (1500/nanmean(SampleFrequency));
elseif this.GeometryType == cl_image.indGeometryType('PingRange')
    this.Sonar.ResolutionD = (x(end)-x(1)) / (length(x) - 1);
elseif this.GeometryType == cl_image.indGeometryType('PingAcrossDist')
    this.Sonar.ResolutionX = (x(end)-x(1)) / (length(x) - 1);
end

yNew = y;
this = sonar_copie_info(this, this, suby, yNew);

% x = centrage_magnetique(x);
% y = centrage_magnetique(y);

this.XLim = compute_XYLim(x);
this.YLim = compute_XYLim(y);

this.x = x;
this.y = y;
