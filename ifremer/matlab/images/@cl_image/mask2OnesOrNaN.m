function K = mask2OnesOrNaN(LayerMask, valMask, zone, x, y, varargin)

% [varargin, ValInit] = getPropertyValue(varargin, 'ValInit', NaN); %#ok<ASGLU>

if isempty(LayerMask)
    if zone == 1
        K = NaN(length(suby), length(subx), 'single');
    else
        K = ones(length(suby), length(subx), 'single');
    end
else
    Masque = get_Masque(LayerMask, x, y);
    if zone == 1
        K = NaN(size(Masque), 'single');
%         if ~isnan(ValInit)
%             K(:) = ValInit;
%         end
        for ik=1:length(valMask)
            % sub = find(Masque == valMask(ik));
            sub = (Masque == valMask(ik));
            K(sub) = 1;
        end
    else
        K = ones(size(Masque), 'single');
%         if ~isnan(ValInit)
%             K(:) = ValInit;
%         end
        for ik=1:length(valMask)
            % sub = find(Masque == valMask(ik));
            sub = (Masque == valMask(ik));
            K(sub) = NaN;
        end
    end
end
