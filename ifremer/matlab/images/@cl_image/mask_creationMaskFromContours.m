% TODO : A priori redondant avec ROI_vect2raster : faire le ménage

function [this, LayerMask, a, flag] = mask_creationMaskFromContours(this, creationLayer, nomLayer, LayerMask, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

flag = 1;

%% Récuperation des coordonnées de l'image visualisée

[a, this] = ROI_vect2raster(this, creationLayer, nomLayer, LayerMask, 'subx', subx, 'suby', suby);
if isempty(a)
    flag = 0;
end

if ~creationLayer
    LayerMask = a;
    flag = 0;
end
