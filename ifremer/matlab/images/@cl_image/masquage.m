% Masquage d'une image par un masque
%
% Syntax
%   b = masquage(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   LayerMask : Image comtenant un masque contenant des entiers
%   valMask   : Valeur du masque a utiliser
%   subx      : subsampling in X
%   suby      : subsampling in Y
%   zone      : {1='In'} | 2='Out'
%
% Output Arguments
%   b : Instance de cl_image contenant l'image masquee
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   a = cl_car_ima(nomFic);
%   I = get(a, 'REFLECTIVITY');
%   b = cl_image('Image', I, 'Name', 'REFLECTIVITY', 'Unit', 'dB', 'ColormapIndex', 2, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(b)
%
%   I = get(a, 'EMISSION_SECTOR');
%   c = cl_image('Image', I, 'Name', 'EMISSION_SECTOR', 'Unit', 'deg', 'ColormapIndex', 3, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(c)
%
%
%   d = masquage(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = masquage(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = masquage(b, 'LayerMask', c, 'valMask', 2, 'zone', 2);
%   imagesc(d)
%
%   d = masquage(b, 'LayerMask', c, 'valMask', [0 2 4 6 7 9]);
%   imagesc(d)
%
% See also ROI_auto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = masquage(this, varargin)

[varargin, NoWaitbar] = getPropertyValue(varargin, 'NoWaitbar', 0);
[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);

nbImages = length(this);
str1 = 'Cr�ation des masques';
str2 = 'Processing masks';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages, 'NoWaitbar', NoWaitbar);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    
    switch length(LayerMask)
        case 0
            [flag, that] = unitaire_masquage(this(k), varargin{:});
        case 1
            [flag, that] = unitaire_masquage(this(k), 'LayerMask', LayerMask, varargin{:});
        otherwise
            [flag, that] = unitaire_masquage(this(k), 'LayerMask', LayerMask(k), varargin{:});
    end
    if ~flag
        my_close(hw, 'MsgEnd', 'TimeDelay', 10);
        return
    end
    this(k) = that;
    if nbImages > 1
        this(k) = optimiseMemory(this(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd');
flag = 1;


function [flag, this] = unitaire_masquage(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []);
[varargin, zone]      = getPropertyValue(varargin, 'zone',      1); %#ok<ASGLU>
% [varargin, ValInit]   = getPropertyValue(varargin, 'ValInit', NaN); %#ok<ASGLU>

flag = 0;

if isempty(LayerMask)
    str1 = 'Il n''y a pas de masque associ�.';
    str2 = 'There is no associated mask.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

K = mask2OnesOrNaN(LayerMask, valMask, zone, this.x(subx), this.y(suby)); %, 'ValInit', ValInit);

thisImage = singleUnlessDouble(this.Image(:,:), this.ValNaN);
N = length(suby);
nbSlides = this.nbSlides;
if isa(thisImage(1), 'double')
    Image = NaN(length(suby), length(subx), nbSlides);
    for k1=1:nbSlides
        for k2=1:N
            Image(k2,:,k1) = thisImage(suby(k2), subx,k1) .* double(K(k2,:));
        end
    end
else
    Image = NaN(length(suby), length(subx), nbSlides, 'single');
    for k1=1:nbSlides
        for k2=1:N
            Image(k2,:,k1) = single(thisImage(suby(k2), subx,k1)) .* K(k2,:);
        end
    end
end

if ~isnan(this.ValNaN)
    Image(Image == this.ValNaN) = NaN;
end

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, Image);
this.ValNaN = NaN;

%% Mise � jour des coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

if zone == 1
    this = update_Name(this, 'Append', 'MaskIn');
else
    this = update_Name(this, 'Append', 'MaskOut');
end

flag = 1;
