% Masquage d'une image par un masque
%
% Syntax
%   b = masquageBits(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   LayerMask : Image comtenant un masque contenant des entiers
%   valMask   : Valeur du masque a utiliser
%   subx        : subsampling in X
%   suby        : subsampling in Y
%   zone        : {1='In'} | 2='Out'
%
% Output Arguments
%   b : Instance de cl_image contenant l'image masquee
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   a = cl_car_ima(nomFic);
%   I = get(a, 'REFLECTIVITY');
%   b = cl_image('Image', I, 'Name', 'REFLECTIVITY', 'Unit', 'dB', 'ColormapIndex', 2, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(b)
%
%   I = get(a, 'EMISSION_SECTOR');
%   c = cl_image('Image', I, 'Name', 'EMISSION_SECTOR', 'Unit', 'deg', 'ColormapIndex', 3, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(c)
%
%
%   d = masquageBits(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = masquageBits(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = masquageBits(b, 'LayerMask', c, 'valMask', 2, 'zone', 2);
%   imagesc(d)
%
%   d = masquageBits(b, 'LayerMask', c, 'valMask', [0 2 4 6 7 9]);
%   imagesc(d)
%
% See also colorisation cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = masquageBits(this, LayerMask, Bits, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_masquageBits(this(k), LayerMask, Bits, varargin{:});
end

function this = unitaire_masquageBits(this, LayerMask, Bits, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));

Image = singleUnlessDouble(this.Image(suby, subx, :));
for k1=this.nbSlides:-1:1
    I = Image(:, :, k1);
    for k2=1:length(Bits)
        K = bitand(uint8(Masque), 2^(Bits(k2)-1));
        I(K == 0) = this.ValNaN;
    end
    Image(:,:,k1) = I;
end

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, Image);

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this = update_Name(this, 'Append', 'MaskBits');
