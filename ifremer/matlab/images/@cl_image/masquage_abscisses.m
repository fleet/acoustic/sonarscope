function [flag, this] = masquage_abscisses(this, varargin)

nbImages = length(this);
hw = create_waitbar('Masking abscisses', 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    [flag, that] = unitaire_masquage_abscisses(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that;
    if nbImages > 1
        this(k) = optimiseMemory(this(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd')


function [flag, this] = unitaire_masquage_abscisses(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, subxLimites] = getPropertyValue(varargin, 'subxLimites', []);
[varargin, xLimites]    = getPropertyValue(varargin, 'xLimites', []); %#ok<ASGLU>

if isempty(subxLimites) && isempty(xLimites)
    return
end

[this, flag] = extraction(this, 'subx', subx, 'suby', suby);
if ~flag
    return
end

subxBandes = {};
if ~isempty(subxLimites)
    if iscell(subxLimites)
        subxBandes = subxLimites;
    else
        subxBandes = {subxLimites};
    end
end

Image = this.Image;

nbBandes = length(subxBandes);
N = length(suby);
for k=1:N
    for kBandes=1:nbBandes
        if size(subxBandes{kBandes}, 1) == 1
            subNaN = subxBandes{kBandes}(1):subxBandes{kBandes}(2);
        else
            subNaN = subxBandes{kBandes}(k,1):subxBandes{kBandes}(k,2);
        end
        Image(k,subNaN,:) = NaN;
    end
end

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, Image);
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'MaskAbsicces');
