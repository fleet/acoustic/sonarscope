% Masquage d'une image par un masque
%
% Syntax
%   b = masqueExtractionBits(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   LayerMask : Image comtenant un masque contenant des entiers
%   valMask   : Valeur du masque a utiliser
%   subx        : subsampling in X
%   suby        : subsampling in Y
%   zone        : {1='In'} | 2='Out'
%
% Output Arguments
%   b : Instance de cl_image contenant l'image masquee
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   a = cl_car_ima(nomFic);
%   I = get(a, 'REFLECTIVITY');
%   b = cl_image('Image', I, 'Name', 'REFLECTIVITY', 'Unit', 'dB', 'ColormapIndex', 2, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(b)
%
%   I = get(a, 'EMISSION_SECTOR');
%   c = cl_image('Image', I, 'Name', 'EMISSION_SECTOR', 'Unit', 'deg', 'ColormapIndex', 3, ...
%              'TagSynchroX', 'XY1', 'TagSynchroY', 'XY1');
%   imagesc(c)
%
%
%   d = masqueExtractionBits(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = masqueExtractionBits(b, 'LayerMask', c, 'valMask', 2);
%   imagesc(d)
%
%   d = masqueExtractionBits(b, 'LayerMask', c, 'valMask', 2, 'zone', 2);
%   imagesc(d)
%
%   d = masqueExtractionBits(b, 'LayerMask', c, 'valMask', [0 2 4 6 7 9]);
%   imagesc(d)
%
% See also colorisation cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = masqueExtractionBits(this, Bits, varargin)

nbImages = length(this);
for k=1:nbImages
    that(k,:) = unitaire_masqueExtractionBits(this(k), Bits, varargin{:}); %#ok<AGROW>
    that = that(:);
end


function that = unitaire_masqueExtractionBits(this, Bits, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

Masque = this.Image(suby, subx);
Image  = zeros(size(Masque), 'uint8');

strMaskBits = this.strMaskBits;
for k=1:length(Bits)
    Val = 2^(Bits(k)-1);
    K = bitand(uint8(Masque), Val);
    Image(K == Val) = 1;

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)
    
    that(k) = replace_Image(this, Image); %#ok<AGROW>
    % this.ValNaN = NaN;
    
    % --------------------------
    % Mise a jour de coordonnees
    
    that(k) = majCoordonnees(that(k), subx, suby); %#ok<AGROW>
    
    % -----------------------
    % Calcul des statistiques
    
    that(k) = compute_stats(that(k)); %#ok<AGROW>
    
    % -------------------------
    % Rehaussement de contraste
    
    that(k).TagSynchroContrast = num2str(rand(1)); %#ok<AGROW>
    CLim = [0 1];
    that(k).CLim = CLim; %#ok<AGROW>
    that(k).ValNaN = 255; %#ok<AGROW>
    
    % -------------------
    % Completion du titre
    
    if isempty(strMaskBits)
        that(k) = update_Name(that(k), 'Append', ['Bit' num2str(Bits(k))]); %#ok<AGROW>
    else
        that(k) = update_Name(that(k), 'Append', strMaskBits{Bits(k)}); %#ok<AGROW>
    end
end
