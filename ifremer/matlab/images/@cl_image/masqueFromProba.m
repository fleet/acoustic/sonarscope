% Calcul d'un masque sur les donn�es issues d'une probabilite d'un
% mode de d�tection Amplitude/Phase, Nombre d'echantillons, Variance de
% phase, etc...
%
% Syntax
%   b = masqueFromProba(a, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant la probabilite
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   Seuil      : Seuil de d�tection des zones erron�es (0.5 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%
% Examples
%   A COMPLETER
%   b = masqueFromProba(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, P, flag] = masqueFromProba(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    P = [];
    flag = 0;
    return
end

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Seuil]    = getPropertyValue(varargin, 'Seuil', 0.5);
[varargin, DepthNaN] = getPropertyValue(varargin, 'DepthNaN', []); %#ok<ASGLU>

% ---------
% Controles

% ident1 = cl_image.indDataType('DetectionType');
% ident2 = cl_image.indDataType('KongsbergQualityFactor');
% flag = testSignature(this, 'DataType', [ident1, ident2], 'GeometryType', cl_image.indGeometryType('PingBeam'));
% if ~flag
%     P = [];
%     return
% end

% -------------------------------------
% Calcul du masque

Masque = this.Image;
subMasque = (Masque < Seuil );
Masque(subMasque) = 1;
Masque(~subMasque) = NaN;
Masque(DepthNaN) = 1;

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)
this = replace_Image(this, Masque);
this.ValNaN = NaN;

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, 1:this.nbColumns, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', 'Mask from Proba');

