% TODO : trouver un autre nom
% Mask over the Image, holes are filled according to a radius
%
% Syntax
%   b = masque_englobant(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Rayon : Action radius (Default : 10)
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   b = masque_englobant(a);
%   imagesc(b);
%
% See also cl_image/interpol_morpho Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = masque_englobant(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('masque_englobant'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = masque_englobant_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = masque_englobant_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Rayon] = getPropertyValue(varargin, 'Rayon', 10);

[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

%% Algorithm

if ~isempty(subx) && ~isempty(suby)
    if isequal(subx, 1:length(this.x)) && isequal(suby, 1:length(this.y))
        Image = this.Image; % Pour profiter des images en memoire virtuelle
    else
        if isempty(subx)
            subx = 1:length(this.x);
        end
        if isempty(suby)
            suby = 1:length(this.y);
        end
        Image = this.Image(suby,subx,:);
    end
else
    subx = 1:this.nbColumns;
    suby = 1:this.nbRows;
    Image = this.Image; % Pour profiter des images en memoire virtuelle
end

% ------------------------------------------------------------------
% Test qui s'av�re n�cessaire pour des images RGB qui proviennent de
% fichiers .ers Ceci n'est pas normal car this.ValNaN = 0 : op�ration de
% maintenance � faire
pppp = 0;
for k=1:size(Image, 1)
    pppp = pppp + sum(sum(Image(k,:,:)));
    if isnan(pppp)
        break
    end
end

if isnan(pppp)
    %     this.ValNaN = NaN;
    B = false(size(Image, 1), size(Image, 2));
    for k=1:size(Image,1)
        pppp = isnan(Image(k,:,:));
        for k2=1:size(Image,3)
            B(k,:) = B(k,:) | (pppp(1,:,k2) == 0);
        end
    end
else
    if this.ImageType == 2 % RGB
        if isnan(this.ValNaN)
            B = ~isnan(sum(Image(:,:,:),3));
        else
            B = (Image ~= this.ValNaN);
            B = sum(B,3);
            B = (B ~= 0);
        end
    else
        subNonNaN = findNonNaN(this, 'subx', subx, 'suby', suby);
        B = zeros(size(Image), 'single');
        B(subNonNaN) = 1;
        if this.nbSlides ~= 1
            B = floor(sum(B,3) / this.nbSlides);
        end
    end
end

%% On agrandit l'image sinon les bords de l'image sont consid�r�s comme de la donn�e

N = 2*Rayon + 1;
C = zeros(size(B) + [N N], 'uint8');
C(Rayon+1:Rayon+size(B,1), Rayon+1:Rayon+size(B,2)) = B;

%% On remplit les trous

se = strel('disk', Rayon);
warning('off', 'all');
C = imdilate(C, se);
warning('on', 'all');

%% On erode les tours de l'image

% se = strel('disk', Rayon);
% C = imerode(single(C), se);

%% On reduit l'image

B = C(Rayon+1:Rayon+size(B,1), Rayon+1:Rayon+size(B,2));

%% Output image

DataType = cl_image.indDataType('Mask');
that = inherit(this, B, 'subx', subx, 'suby', suby, 'AppendName', 'masque_englobant', 'ValNaN', 0, ...
    'NoStats', NoStats, 'CLim', [0 1], 'ColormapIndex', 3, 'ImageType', 1, 'DataType', DataType, 'Unit', ' ');
