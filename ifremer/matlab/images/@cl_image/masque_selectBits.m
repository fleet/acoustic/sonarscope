function [flag, Bits] = masque_selectBits(this)

strMaskBits = this.strMaskBits;
if isempty(strMaskBits)
    strMaskBits = {'Bit 1 : unit'
        'Bit 2 : multiple of 2'
        'Bit 3 : multiple of 4'
        'Bit 4 : multiple of 8'
        'Bit 5 : multiple of 16'
        'Bit 6 : multiple of 32'
        'Bit 7 : multiple of 64'
        'Bit 8 : multiple of 128'};    
end

str1 = 'S�lection des bits';
str2 = 'Select bits';
[Bits, flag] = my_listdlgMultiple(Lang(str1,str2), strMaskBits, 'InitialValue', 1:length(strMaskBits));
if ~flag
    return
end
