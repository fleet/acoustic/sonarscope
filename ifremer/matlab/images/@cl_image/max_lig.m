% Get the maximum values along the vertical direction
%
% Syntax
%   [val, N] = max_lig(a, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%   TypeCurve : Type of curve to display : 'Mean' | 'NbPixels' (Default : 'Mean')
%   LayerMask : A cl_image instance of a mask to apply onto the image before processing the statistics
%   valMask   : Value(s) of the mask to use
%
% Output Arguments
%   []  : Auto plot
%   val : Maximum values along the vertical direction
%   N   : Number of averaged pixels
%
% Remarks : In case of Auto plot, a question is asked to the user if
%           he wants to plot the curve in the classical Matlab
%           direction or vertically.
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%   [val, N] = max_lig(a)
%   val = max_lig(a, 'Fig')
%   max_lig(a)
%   max_lig(a, 'TypeCurve', 'NbPixels')
%
% See also cl_image/min_lig cl_image/mean_lig cl_image/median_lig cl_image/mean_lig cl_image/sum_lig cl_image/max_col Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, nbPix] = max_lig(this, varargin)

[varargin, Fig] = getFlag(varargin, 'Fig');
[val, nbPix] = stats_alongOneDirection(this, 'max', 2, Fig | (nargout == 0), varargin{:});
