% Get the mean value of an image
%
% Syntax
%   val = mean(a, ...)
% 
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   val : Mean value(s)
%
% Examples 
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%   val = mean(a)
%
% See also cl_image/min cl_image/max cl_image/median Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function val = mean(this, varargin)

for k=length(this):-1:1
    val(k) = mean_unit(this(k), varargin{:});
end


function val = mean_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

sub = findNonNaN(this, 'subx', subx, 'suby', suby);
I = this.Image(suby,subx,:);
val = mean(I(sub));
