% Get the mean values along the horizontal direction
%
% Syntax
%   [val, N] = mean_col(a, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%   TypeCurve : Type of curve to display : 'Value' | 'NbPixels' (Default : 'Value')
%   LayerMask : A cl_image instance of a mask to apply onto the image before processing the statistics
%   valMask   : Value(s) of the mask to use
%
% Output Arguments
%   []  : Auto plot
%   val : Mean values along the horizontal direction
%   N   : Number of averaged pixels
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%   [val, N] = mean_col(a);
%   val = mean_col(a, 'Fig')
%   mean_col(a);
%   mean_col(a, 'TypeCurve', 'NbPixels');
%
% See also cl_image/min_col cl_image/max_col cl_image/median_col cl_image/sum_col cl_image/mean_lig Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, nbPix] = mean_col(this, varargin)

[varargin, Fig] = getFlag(varargin, 'Fig');
[val, nbPix] = stats_alongOneDirection(this, 'mean', 1, Fig | (nargout == 0), varargin{:});
