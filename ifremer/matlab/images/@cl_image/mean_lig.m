% Get the mean values along the vertical direction
%
% Syntax
%   [val, N] = mean_lig(a, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%   TypeCurve : Type of curve to display : 'Value' | 'NbPixels' (Default : 'Value')
%   LayerMask : A cl_image instance of a mask to apply onto the image before processing the statistics
%   valMask   : Value(s) of the mask to use
%
% Output Arguments
%   []  : Auto plot
%   val : Mean values along the vertical direction
%   N   : Number of averaged pixels
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%   [val, N] = mean_lig(a);
%   val = mean_lig(a, 'Fig')
%   mean_lig(a);
%   mean_lig(a, 'TypeCurve', 'NbPixels');
%
% See also cl_image/min_lig cl_image/max_lig cl_image/median_lig cl_image/sum_lig cl_image/mean_col Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, nbPix] = mean_lig(this, varargin)

[varargin, Fig] = getFlag(varargin, 'Fig');

[val, nbPix] = stats_alongOneDirection(this, 'mean', 2, Fig | (nargout == 0), varargin{:});
