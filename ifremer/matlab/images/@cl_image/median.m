% Get the median value of an image
%
% Syntax
%   val = median(a, ...)
% 
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   val : Median value(s)
%
% Examples 
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%   val = median(a)
%
% See also cl_image/min cl_image/max cl_image/mean cl_image/searchMedianValue Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function val = median(this, varargin)

for k=length(this):-1:1
    val(k) = median_unit(this(k), varargin{:});
end


function val = median_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

sub = findNonNaN(this, 'subx', subx, 'suby', suby);
I = this.Image(suby,subx,:);
val = median(I(sub));
