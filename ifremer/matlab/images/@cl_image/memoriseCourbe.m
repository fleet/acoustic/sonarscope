function this = memoriseCourbe(this, val, Tag, subx, suby, varargin)

[varargin, CurveName] = getPropertyValue(varargin, 'CurveName', []);
[varargin, Comment]   = getPropertyValue(varargin, 'Comment',   []);
[varargin, TypeCurve] = getPropertyValue(varargin, 'TypeCurve', 'Mean');
[varargin, Question]  = getPropertyValue(varargin, 'Question',  0);
[varargin, Heritage]  = getPropertyValue(varargin, 'Heritage',  []); %#ok<ASGLU>

if Question
    str1 = 'Sauvegarde de la courbe ?';
    str2 = 'Save the curve ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (rep == 2)
        return
    end
end

if isempty(CurveName)
    options.Resize = 'on';
    prompt = {Lang('Nom de courbe conditionnelle','Conditionnal curve name'), Lang('Commentaire','Comment')};
    dlgTitle = 'Analyse statistique Inter-Images';
    Comment = sprintf('x from %f to %f\ny from %f to %f', ...
        this.x(subx(1)), this.x(subx(end)),...
        this.y(suby(1)), this.y(suby(end)));
    cmd = inputdlg(prompt, dlgTitle, [1;10], {[Tag ' ' TypeCurve], Comment}, options);
    if isempty(cmd)
        return
    end
    
    CurveName = cmd{1};
    Comment   = cmd{2};
    str = Comment(1,:);
    for k=2:size(Comment, 1)
        str = sprintf('%s\n%s', str, Comment(k,:));
    end
    Comment = str;
end
drawnow

Courbe.x       = this.x(subx);
Courbe.y       = this.y(suby);
Courbe.means   = val';
Courbe.medians = [];
Courbe.sem     = val' * NaN;
Courbe.counts  = val' * NaN;
switch Tag
    case 'X'
        Courbe.bilan{1}.x   = this.x(subx);
        Courbe.bilan{1}.sub = subx;
    case 'Y'
        Courbe.bilan{1}.x   = this.y(suby);
        Courbe.bilan{1}.sub = suby;
end
Courbe.bilan{1}.y                  = val;
Courbe.bilan{1}.nom                = Tag;
Courbe.bilan{1}.nx                 = val * NaN;
Courbe.bilan{1}.std                = val * NaN;
Courbe.bilan{1}.titreV             = this.Name;
Courbe.bilan{1}.NY                 = length(val);
Courbe.bilan{1}.nomX               = Tag;
Courbe.bilan{1}.nomZoneEtude       = CurveName;
Courbe.bilan{1}.commentaire        = Comment;
Courbe.bilan{1}.Tag                = this.Name;
Courbe.bilan{1}.DataTypeValue      = cl_image.strDataType{this.DataType};
Courbe.bilan{1}.DataTypeConditions = {Tag};
Courbe.bilan{1}.Support            = {'Axe'};
Courbe.bilan{1}.Unit               = this.Unit;

if isempty(this.CourbesStatistiques)
    this.CourbesStatistiques = Courbe;
else
    this.CourbesStatistiques(end+1) = Courbe;
end

if ~isempty(Heritage)
    if isempty(Heritage.CourbesStatistiques)
        Heritage.CourbesStatistiques = Courbe;
    else
        Heritage.CourbesStatistiques(end+1) = Courbe;
    end
end
