% Mosaics merging
%
% Syntax
%   [flag, ImageMos, AngleMos] = mergeMosaics(a, ...)
%
% Input Arguments
%   a : Un tableau d'instances de cl_image
%
% Name-Value Pair Arguments
%   Angle : Instances de cl_image donnant l'information angulaire
%   frame : [] = C'est l'image enblobant toutes les mosaiques qui sera produite
%                Dans ce cas, c'est l'image 1 qui devient l'image de reference
%           n  = Numero de l'image qui definit le cadre (devient
%                Dans ce cas, c'est cette image qui devient l'image de reference
%   pasx  : Pas en x (m) (pas de l'image de reference par defaut)
%   pasy  : Pas en y (m) (pas de l'image de reference par defaut)
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   c    : Une instance de cl_image
%
% Examples
%   nomFic = 'C:\TEMP\TestMosaiqueSimple\1_7111_20071210_014039_PP-7111_Interpol[33]_LatLong_Bathymetry.ers';
%   [flag, r1] = cl_image.import_ermapper(nomFic);
%   SonarScope(r1)
%
%   nomFic = 'C:\TEMP\TestMosaiqueSimple\7111_20071210_014039_PP-7111_Interpol[33]_LatLong_TxAngle.ers';
%   [flag, a1] = cl_image.import_ermapper(nomFic);
%   SonarScope(a1)
%
%   nomFic = 'C:\TEMP\TestMosaiqueSimple\1_7111_20071210_021442_PP-7111_Interpol[33]_LatLong_Bathymetry.ers';
%   [flag, r2] = cl_image.import_ermapper(nomFic);
%   SonarScope(r2)
%
%   nomFic = 'C:\TEMP\TestMosaiqueSimple\7111_20071210_021442_PP-7111_Interpol[33]_LatLong_TxAngle.ers';
%   [flag, a2] = cl_image.import_ermapper(nomFic);
%   SonarScope(a2)
%
%   [r3, a3] = mosaique([r1 r2], 'Angle', [a1 a2]);
%   SonarScope([r3 a3 r1 a1 r2 a2])
%
%   [~, r4, a4] = mergeMosaics([r1 r2], 'Angle', [a1 a2]);
%   SonarScope([r4 a4 r1 a1 r2 a2])
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, ImageMos, AngleMos] = mergeMosaics(this, varargin)

global SizePhysTotalMemory %#ok<GVMIS> 

% [varargin, frame]                = getPropertyValue(varargin, 'frame',                []);
[varargin, Angle]                = getPropertyValue(varargin, 'Angle',                []);
[varargin, XLim]                 = getPropertyValue(varargin, 'XLim',                 []);
[varargin, YLim]                 = getPropertyValue(varargin, 'YLim',                 []);
[varargin, AngleLim]             = getPropertyValue(varargin, 'AngleLim',             []);
[varargin, CoveringPriority]     = getPropertyValue(varargin, 'CoveringPriority',     1);
[varargin, bufferZoneSize]       = getPropertyValue(varargin, 'bufferZoneSize',       []);
[varargin, Info]                 = getPropertyValue(varargin, 'Info',                 []);
[varargin, NoWaitbar]            = getPropertyValue(varargin, 'NoWaitbar',            0);
[varargin, FistImageIsCollector] = getPropertyValue(varargin, 'FistImageIsCollector', 0);


% if isempty(frame)
    [varargin, pasx] = getPropertyValue(varargin, 'pasx', []);
    [varargin, pasy] = getPropertyValue(varargin, 'pasy', []); %#ok<ASGLU>
    
    xmin = Inf;
    xmax = -Inf;
    ymin = Inf;
    ymax = -Inf;
    for k=1:length(this)
        x = double(this(k).x);
        y = double(this(k).y);
        xmin = min(xmin, min(x));
        xmax = max(xmax, max(x));
        ymin = min(ymin, min(y));
        ymax = max(ymax, max(y));
    end
    
    if isempty(pasx)
        pasx = mean(diff(this(1).x));
    end
    if isempty(pasy)
        pasy = mean(diff(this(1).y));
    end
    
    a = this(1); % Pour h�riter de tous les descripteurs
    
    xmin = floor(xmin / pasx) * pasx;
    xmax = ceil( xmax / pasx) * pasx;
    ymin = floor(ymin / pasy) * pasy;
    ymax = ceil( ymax / pasy) * pasy;
    x = xmin:abs(pasx):xmax;
    y = ymin:abs(pasy):ymax;
    
    %     [x, pasx, xmin, xmax] = centrage_magnetique(this(1).x);
    %     [y, pasy, ymin, ymax] = centrage_magnetique(this(1).y);
    
    if ~isempty(XLim)
        x = x((x >= min(XLim)) & (x <= max(XLim)));
    end
    if ~isempty(YLim)
        y = y((y >= min(YLim)) & (y <= max(YLim)));
    end
    
    [x, pasx, xmin, xmax] = centrage_magnetique(x); %#ok
    [y, pasy, ymin, ymax] = centrage_magnetique(y); %#ok
    
    if my_isequal(pasx, get(this(1), 'XStep')) && my_isequal(pasy, get(this(1), 'YStep')) ...
            && (length(x) == length(this(1).x)) && (length(y) == length(this(1).y))
        %         'Egal'
%     else
%         a.x = x;
%         a.y = y;
    end
% else
%     a = this(frame);
% end

if FistImageIsCollector && isLargeEnough(this) % TODO : voir si �a peut �tre �tendu (serait utile pour de tr�s grosses mosaiques)
    ImageMos = this(1);
    kDeb = 2;
else
    kDeb = 1;
    
    % Test pour savoir si on a assez de m�moire
    nbSlides = a.nbSlides;
    if (length(y) * length(x) * nbSlides * 4*8) <  (SizePhysTotalMemory*1000)
        flagRAM = 1;
    else
        flagRAM = 0;
    end
    
    if isnan(a.ValNaN) || (a.ValNaN >=3.4e+38) % Cas sp�cial pour mapping sur ErMapper
        try
            if flagRAM
                X = NaN(length(y), length(x), nbSlides, class(a.Image(1)));
            else
                X = cl_memmapfile('Value', NaN, 'Size', [length(y), length(x), nbSlides], 'Format', class(a.Image(1)));
            end
        catch %#ok<CTCH>
            X = cl_memmapfile('Value', NaN, 'Size', [length(y), length(x), nbSlides], 'Format', class(a.Image(1)));
        end
    else
        try
            if flagRAM
                X =  zeros(length(y), length(x), nbSlides, class(a.Image(1)));
            else
                X = cl_memmapfile('Value', a.ValNaN, 'Size', [length(y), length(x), nbSlides], 'Format', class(a.Image(1)));
            end
        catch %#ok<CTCH>
            X = cl_memmapfile('Value', a.ValNaN, 'Size', [length(y), length(x), nbSlides], 'Format', class(a.Image(1)));
        end
    end
    ImageMos = replace_Image(a, X);
    ImageMos.x = x;
    ImageMos.y = y;
    clear X
end

if ~isempty(Angle)
    if FistImageIsCollector && isLargeEnough(this)
        AngleMos = Angle(1).Image;
    else
        % Test pour savoir si on a assez de m�moire
        nbSlides = ImageMos.nbSlides; % TODO : toujours 1 en principe donc � supprimer
        if (ImageMos.nbRows * ImageMos.nbColumns * nbSlides * 4*8) <  (SizePhysTotalMemory*1000)
            flagRAM = 1;
        else
            flagRAM = 0;
        end
        try
            if flagRAM
                AngleMos = NaN(ImageMos.nbRows, ImageMos.nbColumns, 'single');
            else
                AngleMos = cl_memmapfile('Value', NaN, 'Size', [ImageMos.nbRows, ImageMos.nbColumns], 'Format', 'single');
            end
        catch %#ok<CTCH>
            AngleMos = cl_memmapfile('Value', NaN, 'Size', [ImageMos.nbRows, ImageMos.nbColumns], 'Format', 'single');
        end
    end
else
    AngleMos = [];
end

%%

pasx = abs(pasx);
pasy = abs(pasy);
N = length(this);
str1 = sprintf('Assemblage des mosa�ques %s', Info);
str2 = sprintf('Merging mosaics %s', Info);
hw = create_waitbar(Lang(str1,str2), 'N', N, 'NoWaitbar', NoWaitbar);
for k=kDeb:N
    try
        my_waitbar(k, N, hw)
        if isempty(Angle)
            [ImageMos, AngleMos] = processOneImage(this(k), [], ImageMos, [], pasx, pasy, CoveringPriority, [], bufferZoneSize);
        else
            [ImageMos, AngleMos] = processOneImage(this(k), Angle(k), ImageMos, AngleMos, pasx, pasy, CoveringPriority, AngleLim, bufferZoneSize);
        end
    catch ME %#ok<NASGU>
        ImageName = this(k).Name;
        str1 = sprintf('Il y a eu un probl�me d''assemblage pour l''image "%s". Sauvez la session et d�crivez le probl�me � sonarscope@ifremer.fr svp.', ImageName);
        str2 = sprintf('Merging "%s" failed. Save the session and report the problem to sonarscope@ifremer.fr please.', ImageName);
        my_warndlg(Lang(str1,str2), 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 10);

% if ImageMos.StatValues.NbData == 0
%     flag = 0;
%     return
% end

%% Mise � jour de coordonn�es

ImageMos.x    = centrage_magnetique(ImageMos.x);
ImageMos.y    = centrage_magnetique(ImageMos.y);
ImageMos.XLim = compute_XYLim(ImageMos.x);
ImageMos.YLim = compute_XYLim(ImageMos.y);

%% Calcul des statistiques

ImageMos = compute_stats(ImageMos);
if ImageMos.StatValues.NbData == 0
    flag = 0;
    return
else
    flag = 1;
end

%% Rehaussement de contraste

ImageMos.TagSynchroContrast = num2str(rand(1));
if ImageMos.ImageType ~= 2
    CLim = [ImageMos.StatValues.Min ImageMos.StatValues.Max];
    if any(isnan(CLim))
        CLim = [-eps eps];
    end
    if CLim(1) == CLim(2)
        CLim(1) = CLim(1) - eps;
        CLim(2) = CLim(2) + eps;
    end
    ImageMos.CLim = CLim;
end

%% Cas d'un assemblage de masque ou d'images segment�es : on r�cup�re les couleurs

if (ImageMos.ColormapIndex == 1) && ...
        ((ImageMos.DataType == cl_image.indDataType('Mask')) || (ImageMos.DataType == cl_image.indDataType('Segmentation'))) % ColormapIndex     <-> {'1=User'}
    for k=kDeb:N
        MapImage = this(k).ColormapCustom;
        sub = find(sum((MapImage == 0.9), 2) ~= 3);
        map(sub,:) = MapImage(sub,:); %#ok<AGROW>
    end
    ImageMos.ColormapCustom = map;
    ImageMos.CLim           = [-0.5 size(map,1)-0.5];
end

%% Compl�tion du titre

NuIdentParent = floor(rand(1) * 1e8);
% Titre = ['Mosaic' num2str(ImageMos.DataType)];
if isempty(AngleLim)
    Titre = 'Mosaic';
else
    Titre = sprintf('Mosaic_%s', num2str(AngleLim));
    Titre = strrep(Titre, '  ', '_');
    Titre = strrep(Titre, ' ', '_');
end
ImageMos.NuIdentParent = NuIdentParent;
ImageMos.NuIdent = floor(rand(1) * 1e8);
ImageMos.Name = Titre;
ImageMos = update_Name(ImageMos);
ImageMos.InitialFileName  = [];
ImageMos.InitialImageName = ['Mosaic ' num2str(floor(1e6*rand(1)))];

if ~isempty(Angle)
    
    %% Sortie du layer Angle
    
    c = replace_Image(ImageMos, AngleMos);
    clear AngleMos
    
    %% Calcul des statistiques
    
    c = compute_stats(c);
    
    %% Rehaussement de contraste
    
    c.TagSynchroContrast = num2str(rand(1));
    CLim = [c.StatValues.Min c.StatValues.Max];
    if any(isnan(CLim))
        CLim = [-eps eps];
    end
    if CLim(1) == CLim(2)
        CLim(1) = CLim(1) - eps;
        CLim(2) = CLim(2) + eps;
    end
    c.CLim = CLim;
    
    %% Compl�tion du titre
    
    c.Unit             = Angle(1).Unit;
    c.DataType         = Angle(1).DataType;
    c.ColormapIndex    = 17;
    c.NuIdentParent    = NuIdentParent;
    c.NuIdent          = floor(rand(1) * 1e8);
    c.Name             = Titre;
    c = update_Name(c);
    c.InitialFileName  = [];
    c.InitialImageName = ['Mosaic ' num2str(floor(1e6*rand(1)))];
    c.ImageType = 1; % Repr�cis� au cas de traitement de donnees RGB
    
    %% Par ici la sortie
    
    AngleMos = c;
end


function [ImageMos, AngleMos] = processOneImage(this, Angle, ImageMos, AngleMos, pasx, pasy, CoveringPriority, AngleLim, bufferZoneSize)

xmin = floor(min(this.x) / pasx) * pasx;
xmax = ceil( max(this.x) / pasx) * pasx;
ymin = floor(min(this.y) / pasy) * pasy;
ymax = ceil( max(this.y) / pasy) * pasy;
x = xmin:abs(pasx):xmax;
y = ymin:abs(pasy):ymax;
if this.x(end) < this.x(1)
    x = fliplr(x);
end
if this.y(end) < this.y(1)
    y = fliplr(y);
end
x = centrage_magnetique(x);
y = centrage_magnetique(y);
that = extraction(this, 'x', x, 'y', y); % extraction de l'image � la r�solution de la mosa�que
% that = extraction(this, 'x', x, 'y', y, 'ForceExtraction'); % extraction de l'image � la r�solution de la mosa�que
clear x y

%% Calcul des coordonn�es des pixels valide de l'image entrante dans la mosa�que

[subImage, subMos, sub_subImageInMos] = get_CoordinatesOfValidPixelsOfAinB(that, ImageMos);

%% Conditionnement de l'image des angles

if isempty(Angle)
    AngleImage = [];
else
    x = get(that, 'x');
    y = get(that, 'y');
    
    AngleTemp = extraction(Angle, 'x', x, 'y', y);
    AngleImage = AngleTemp.Image;
    if ~isempty(AngleLim)
        AngleImage = AngleImage(:,:);
        AngleImage(AngleImage < AngleLim(1)) = NaN;
        AngleImage(AngleImage > AngleLim(2)) = NaN;
    end
end

%% ajout GLT pour Blending/Mixage simple

switch CoveringPriority
    case 5 % Mixage
        maskTopOrig = isnan(ImageMos.Image); % pour r�cup�rer un tableau logique (avant isletter)
        maskTopOrig(subMos) = true;
        maskBotOrig = ~isnan(ImageMos.Image);
        if ~any(maskBotOrig(:)) % premi�re image : pas de blending
            maskTop = maskTopOrig;
            maskBot = 1 - maskTop;
        else
            % MASK pour Mixage
            maskTop = single(zeros(size(maskTopOrig)));
            maskTop(bwdist(~maskTopOrig) > (.5 * bufferZoneSize/pasx)) = 1; % corridor de mixage
            
            % Mixage avec zone tampon
            blurh   = fspecial('gauss', floor(.5*bufferZoneSize/pasx), 15);
            maskTop = imfilter(maskTop, blurh, 'replicate');
            
            % Zone Tampon uniquement aux intersections
            maskTop(xor(maskTopOrig, maskBotOrig)) = maskTopOrig(xor(maskTopOrig, maskBotOrig));
            maskBot = 1 - maskTop;
        end
        % on ne conserve que les donn�es utiles : i.e. sur la zone de recouvrement
        maskTop = maskTop(subMos);
        maskBot = maskBot(subMos);
        clear maskTopOrig maskBotOrig
        
    otherwise
        maskTop = [];
        maskBot = [];
end

%% Assemblage des deux images

TetaLimite = [];
[ImageMos, AngleMos] = mosaique_GestionRecouvrementProfils(...
    ImageMos, AngleMos, that, AngleImage, subMos, subImage, sub_subImageInMos, CoveringPriority, TetaLimite, maskTop, maskBot);

clear subMos subImage

if ~isempty(AngleLim)
    ImageMos.Image(isnan(AngleMos)) = NaN;
end


function  flag = isLargeEnough(this)

minx1 = min(this(1).x);
maxx1 = max(this(1).x);

if this(1).GeometryType == cl_image.indGeometryType('LatLong')
    if (maxx1 - minx1) > 180 % R�gion � cheval sur le m�ridien 180 deg
        my_warndlg('Chevauchement m�ridien 180 degr�s pas encore g�r�', 1);
    end
end

if length(this) == 2
    minx2 = min(this(2).x);
    maxx2 = max(this(2).x);
    if (maxx2 - minx2) > 180 % R�gion � cheval sur le m�ridien 180 deg
        my_warndlg('Chevauchement m�ridien 180 degr�s pas encore g�r�', 1);
    end
end

if (length(this) == 2) && ...
        (minx1 <= minx2) && ...
        (maxx1 >= maxx2) && ...
        (min(this(1).y) <= min(this(2).y)) && ...
        (max(this(1).y) >= max(this(2).y))
    flag = 1;
else
    flag = 0;
end
