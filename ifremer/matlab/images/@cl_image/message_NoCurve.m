function message_NoCurve(this, varargin)

[varargin, MoreThanOneCurve] = getFlag(varargin, 'MoreThanOneCurve'); %#ok<ASGLU>

if isempty(MoreThanOneCurve)
    str1 = sprintf('Il n''y a pas de courbe dans l''image "%s"', this.Name);
    str2 = sprintf('There is no curve in "%s"', this.Name);
    my_warndlg(Lang(str1,str2), 1);
else
    str1 = sprintf('Il n''y a pas suffisemment de courbes dans l''image "%s" pour r�aliser ce que vous demandez.', this.Name);
    str2 = sprintf('There is not enough curves in "%s" to do what you expect.', this.Name);
    my_warndlg(Lang(str1,str2), 1);
end
