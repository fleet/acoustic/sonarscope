% Get the minimum value of an image
%
% Syntax
%   val = min(a, ...)
% 
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   val : Minimum value(s)
%
% Examples 
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%   val = min(a)
%
% See also cl_image/max cl_image/mean cl_image/median cl_image/searchMinValue Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function val = min(this, varargin)

val = NaN;
for k=length(this):-1:1
    pppp = min_unitaire(this(k), varargin{:});
    % Il se peut dans le cas d'une image de NaN que la valeur soit vide.
    % ce qui provoque un plantage � l'affectation de val(k).
    % https://forge.ifremer.fr/tracker/index.php?func=detail&aid=525&group_id=126&atid=535
    if ~isempty(pppp)
        val(k) = pppp;
    end
end


function val = min_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

sub = findNonNaN(this, 'subx', subx, 'suby', suby);
I = this.Image(suby,subx,:);
val = min(I(sub));
