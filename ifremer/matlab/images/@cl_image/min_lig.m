% Get the minimum values along the vertical direction
%
% Syntax
%   [val, N] = min_lig(a, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   subx      : Sub-sampling in abscissa
%   suby      : Sub-sampling in ordinates
%   TypeCurve : Type of curve to display : 'Mean' | 'NbPixels' (Default : 'Mean')
%   LayerMask : A cl_image instance of a mask to apply onto the image before processing the statistics
%   valMask   : Value(s) of the mask to use
%
% Output Arguments
%   []  : Auto plot
%   val : Minimum values along the vertical direction
%   N   : Number of averaged pixels
%
% Remarks : In case of Auto plot, a question is asked to the user if
%           he wants to plot the curve in the classical Matlab
%           direction or vertically.
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%   [val, N] = min_lig(a)
%   val = min_lig(a, 'Fig')
%   min_lig(a);
%   min_lig(a, 'TypeCurve', 'NbPixels')
%
% See also cl_image/max_lig cl_image/mean_lig cl_image/median_lig cl_image/sum_lig cl_image/min_col Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, nbPix] = min_lig(this, varargin)

[varargin, Fig] = getFlag(varargin, 'Fig');
[val, nbPix] = stats_alongOneDirection(this, 'min', 2, Fig | (nargout == 0), varargin{:});
