function [flag, this] = modifyAttributsCurrentImage(this)

strListeProperties = {
    'DataType'
    'GeometryType'
    'ImageType'
    'ColormapIndex'
    'Video'};
[listeProperties, flag] = my_listdlg(Lang('Propri�t�s � modifier', 'Properties to set'), strListeProperties, 'InitialValue', []);
if ~flag
    return
end
if isempty(listeProperties)
    flag = 0;
    return
end

%% DataType

if any(listeProperties == 1) % DataType
    [flag, DataType] = cl_image.edit_DataType('DataType', this.DataType);
    if ~flag
        return
    end
    this.DataType = DataType;
end

%% GeometryType

if any(listeProperties == 2) % GeometryType
    previousGeometryType = this.GeometryType;
    
    [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', this.GeometryType);
    if ~flag
        return
    end
    this.GeometryType = GeometryType;
    
    if GeometryType == cl_image.indGeometryType('LatLong')
        this.TagSynchroX = 'Longitude';
        this.TagSynchroY = 'Latitude';
    elseif previousGeometryType == cl_image.indGeometryType('LatLong')
        this.TagSynchroX = num2str(rand(1));
        this.TagSynchroY = num2str(rand(1));
    end
end

%% ImageType

if any(listeProperties == 3) % ImageType
    [flag, ImageType] = edit_ImageType(this);
    if ~flag
        return
    end
    this.ImageType = ImageType;
end

%% ColormapIndex

if any(listeProperties == 4) % ColormapIndex
    [flag, ColormapIndex] = edit_ColormapIndex(this);
    if ~flag
        return
    end
    this.ColormapIndex = ColormapIndex;
end

%% Video

if any(listeProperties == 5) % Video
    [flag, Video] = edit_Video(this);
    if ~flag
        return
    end
    this.Video = Video;
end
