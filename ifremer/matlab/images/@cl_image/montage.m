% Creation d'une photo montage des images
%
% Syntax
%   I = montage(a) 
%
% Input Arguments 
%   a : Un tableau d'instances de cl_image
%
% Output Arguments 
%   [] : Auto-plot activation
%   I  : Image assemblee
%
% Examples
%   for i=1:8
%       [I, label] = ImageSonar(i);
%       a(i) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   end
%   montage(a)
%   I = montage(a);
%
% See also cl_image movie cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = montage(this)
  
%% Récuperation de PrepertyValues locales

for i=1:length(this)
    a = ind2rgb(this(i));
    subx = 1:a.nbColumns;
    suby = 1:a.nbRows;
    M(suby,subx,:,i) = get(a, 'Image'); %#ok
end

figure;
h = montage(M);

if nargout ~= 0
    varargout{1} = get(h, 'CData');
    close
end



