% Operateur d'addition
%
% Syntax
%   b = mosaique(a, ...)
%
% Input Arguments
%   a : Un tableau d'instances de cl_image
%
% Name-Value Pair Arguments
%   Angle : Instances de cl_image donnant l'information angulaire
%   frame : [] = C'est l'image enblobant toutes les mosaiques qui sera produite
%                Dans ce cas, c'est l'image 1 qui devient l'image de reference
%           n  = Numero de l'image qui definit le cadre (devient
%                Dans ce cas, c'est cette image qui devient l'image de reference
%   pasx  : Pas en x (m) (pas de l'image de reference par defaut)
%   pasy  : Pas en y (m) (pas de l'image de reference par defaut)
%
% Output Arguments
%   c : Une instance de cl_image
%
% Examples
%   nomFic = getNomFicDatabase('EM300_NIWA_TAN0105_Line125_mos20-IFR-FLT.imo')
%   a(1) = cl_car_ima(nomFic);
%   nomFic = getNomFicDatabase('EM300_NIWA_TAN0105_Line126_mos20-IFR-FLT.imo')
%   a(2) = cl_car_ima(nomFic);
%   nomFic = getNomFicDatabase('EM300_NIWA_TAN0105_Line127_mos20-IFR-FLT.imo')
%   a(3) = cl_car_ima(nomFic);
%   nomFic = getNomFicDatabase('EM300_NIWA_TAN0105_Line128_mos20-IFR-FLT.imo')
%   a(4) = cl_car_ima(nomFic);
%   nomFic = getNomFicDatabase('EM300_NIWA_TAN0105_Line129_mos20-IFR-FLT.imo')
%   a(5) = cl_car_ima(nomFic);
%   nomFic = getNomFicDatabase('EM300_NIWA_TAN0105_Line130_mos20-IFR-FLT.imo')
%   a(6) = cl_car_ima(nomFic);
%
%   b1 = view(a, 'ListeLayers', 1:3, 'Flipud');
%   q = get(b1, 'Images');
%
%   b1 = mosaique(q([1 4 7 10 13 16]), 'Angle', q([3 6 9 12 15 18]));
%   b2 = mosaique(q([1 4 7 10 13 16]));
%   b3 = mosaique(q([2 5 8 11 14 17]), 'Angle', q([3 6 9 12 15 18]));
%   b4 = mosaique(q([2 5 8 11 14 17]));
%
%   SonarScope([q b1 b2 b3 b4])
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [ImageMos, AngleMos] = mosaique(this, varargin)

% [varargin, frame]                = getPropertyValue(varargin, 'frame',                []);
[varargin, Angle]                = getPropertyValue(varargin, 'Angle',                []);
[varargin, XLim]                 = getPropertyValue(varargin, 'XLim',                 []);
[varargin, YLim]                 = getPropertyValue(varargin, 'YLim',                 []);
[varargin, CoveringPriority]     = getPropertyValue(varargin, 'CoveringPriority',     1);
[varargin, TetaLimite]           = getPropertyValue(varargin, 'TetaLimite',           []);
[varargin, FistImageIsCollector] = getPropertyValue(varargin, 'FistImageIsCollector', 0);

% if isempty(frame)
    [varargin, pasx] = getPropertyValue(varargin, 'pasx', []);
    [varargin, pasy] = getPropertyValue(varargin, 'pasy', []); %#ok<ASGLU>
    
    xmin = Inf;
    xmax = -Inf;
    ymin = Inf;
    ymax = -Inf;
    for k=1:length(this)
        x = double(this(k).x);
        y = double(this(k).y);
        xmin = min(xmin, min(x));
        xmax = max(xmax, max(x));
        ymin = min(ymin, min(y));
        ymax = max(ymax, max(y));
    end
    
    if isempty(pasx)
        pasx = mean(diff(this(1).x));
    end
    if isempty(pasy)
        pasy = mean(diff(this(1).y));
    end
    
    a = this(1);    % Pour heriter de tous les descripteurs
    
    xmin = floor(xmin / pasx) * pasx;
    xmax = ceil( xmax / pasx) * pasx;
    ymin = floor(ymin / pasy) * pasy;
    ymax = ceil( ymax / pasy) * pasy;
    x = xmin:abs(pasx):xmax;
    y = ymin:abs(pasy):ymax;
    
    %     [x, pasx, xmin, xmax] = centrage_magnetique(this(1).x);
    %     [y, pasy, ymin, ymax] = centrage_magnetique(this(1).y);
    
    if ~isempty(XLim)
        x = x((x >= min(XLim)) & (x <= max(XLim)));
    end
    if ~isempty(YLim)
        y = y((y >= min(YLim)) & (y <= max(YLim)));
    end
    
    % JMA : A L'essai 04/11/2006
    [x, pasx, xmin, xmax] = centrage_magnetique(x); %#ok
    [y, pasy, ymin, ymax] = centrage_magnetique(y); %#ok
    % JMA : A L'essai 04/11/2006
    
    if my_isequal(pasx, this(1).XStep) && my_isequal(pasy, this(1).YStep) ...
            && (length(x) == length(this(1).x)) && (length(y) == length(this(1).y))
        %         'Egal'
    else
        a.x = x;
        a.y = y;
    end
% else
%     a = this(frame);
% end
nbRows    = length(y);
nbColumns = length(x);

if FistImageIsCollector && isLargeEnough(this)
    ImageMos = this(1);
    kDeb = 2;
else
    kDeb = 1;
    if a.ImageType == 2 % RGB
        nbSlides = 3;
    else
        nbSlides = 1;
    end
    if isnan(a.ValNaN) || (a.ValNaN >= 3.4e+38) % Cas sp�cial pour mapping sur ErMapper
        try
            X = NaN(nbRows, nbColumns, nbSlides, class(a.Image(1)));
        catch %#ok<CTCH>
            X = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbColumns, nbSlides], 'Format', class(a.Image(1)));
        end
        
    else
        try
            X =  zeros(nbRows, nbColumns, nbSlides, class(a.Image(1)));
        catch %#ok<CTCH>
            X = cl_memmapfile('Value', a.ValNaN, 'Size', [nbRows, nbColumns, nbSlides], 'Format', class(a.Image(1)));
        end
    end
    ImageMos = replace_Image(a, X);
    clear X
end

if ~isempty(Angle)
    if FistImageIsCollector && isLargeEnough(this)
        AngleMos = Angle(1).Image;
    else
        try
            AngleMos = NaN(ImageMos.nbRows, ImageMos.nbColumns, 'single');
        catch %#ok<CTCH>
            AngleMos = cl_memmapfile('Value', NaN, 'Size', [ImageMos.nbRows, ImageMos.nbColumns], 'Format', 'single');
        end
        
    end
else
    AngleMos = [];
end

%% Assemblage des images

pasx = abs(pasx);
pasy = abs(pasy);
for k=kDeb:length(this)
    if isempty(Angle)
        [ImageMos, AngleMos] = processOneImage(this(k), [], ImageMos, [], pasx, pasy, CoveringPriority, TetaLimite);
    else
        [ImageMos, AngleMos] = processOneImage(this(k), Angle(k), ImageMos, AngleMos, pasx, pasy, CoveringPriority, TetaLimite);
    end
end

%% Mise � jour des coordonn�es

ImageMos.x = centrage_magnetique(ImageMos.x);
ImageMos.y = centrage_magnetique(ImageMos.y);
ImageMos.XLim = compute_XYLim(ImageMos.x);
ImageMos.YLim = compute_XYLim(ImageMos.y);

%% Calcul des statistiques

ImageMos = compute_stats(ImageMos);

%% Rehaussement de contraste

ImageMos.TagSynchroContrast = num2str(rand(1));
if ImageMos.ImageType ~= 2
    CLim = [ImageMos.StatValues.Min ImageMos.StatValues.Max];
    if any(isnan(CLim))
        CLim = [-eps eps];
    end
    if CLim(1) == CLim(2)
        CLim(1) = CLim(1) - eps;
        CLim(2) = CLim(2) + eps;
    end
    ImageMos.CLim = CLim;
end

%% Compl�tion du titre

NuIdentParent = floor(rand(1) * 1e8);
Titre = ['Mosaic' num2str(ImageMos.DataType)];
ImageMos.NuIdentParent = NuIdentParent;
ImageMos.NuIdent = floor(rand(1) * 1e8);
ImageMos.Name = Titre;
ImageMos = update_Name(ImageMos);
ImageMos.InitialFileName = [];
ImageMos.InitialImageName = ['Mosaic ' num2str(floor(1e6*rand(1)))];


if ~isempty(Angle)
    %     AngleMos(isinf(AngleMos)) = NaN;
    
    
    % % A AMELIORER CAR CA CONSOMME DU CPU : AngleMos(k,subBlock)
    %     for k=1:size(AngleMos,1)
    %         sub = (isinf(AngleMos(k,:))); % Anciennement FIND
    %         AngleMos(k,sub) = NaN;
    %         clear sub
    %     end
    
    %% Sortie du layer Angle
    
    c = replace_Image(ImageMos, AngleMos(:,:));
    clear AngleMos
    
    %% Calcul des statistiques
    
    c = compute_stats(c);
    
    %% Rehaussement de contraste
    
    c.TagSynchroContrast = num2str(rand(1));
    CLim = [c.StatValues.Min c.StatValues.Max];
    if any(isnan(CLim))
        CLim = [-eps eps];
    end
    if CLim(1) == CLim(2)
        CLim(1) = CLim(1) - eps;
        CLim(2) = CLim(2) + eps;
    end
    c.CLim = CLim;
    
    %% Compl�tion du titre
    
    c.Unit             = Angle(1).Unit;
    c.DataType         = Angle(1).DataType;
    c.ColormapIndex    = 17;
    c.NuIdentParent    = NuIdentParent;
    c.NuIdent          = floor(rand(1) * 1e8);
    c.Name             = Titre;
    c = update_Name(c);
    c.InitialFileName  = [];
    c.InitialImageName = ['Mosaic ' num2str(floor(1e6*rand(1)))];
    c.ImageType        = 1; % Repr�cis� au cas de traitement de donnees RGB
    
    %% Par ici la sortie
    
    AngleMos = c;
end


function  flag = isLargeEnough(this)

minx1 = min(this(1).x);
maxx1 = max(this(1).x);

if this(1).GeometryType == cl_image.indGeometryType('LatLong')
    if (maxx1 - minx1) > 180 % R�gion a cheval sur le m�ridien 180 deg
        my_warndlg('Chevauchement m�ridien 180 degr�s pas encore g�r�', 1);
    end
end

if length(this) == 2
    minx2 = min(this(2).x);
    maxx2 = max(this(2).x);
    if (maxx2 - minx2) > 180 % R�gion a cheval sur le m�ridien 180 deg
        my_warndlg('Chevauchement m�ridien 180 degr�s pas encore g�r�', 1);
    end
end

if (length(this) == 2) && ...
        (minx1 <= minx2) && ...
        (maxx1 >= maxx2) && ...
        (min(this(1).y) <= min(this(2).y)) && ...
        (max(this(1).y) >= max(this(2).y))
    flag = 1;
else
    flag = 0;
end


function [ImageMos, AngleMos] = processOneImage(this, Angle, ImageMos, AngleMos, pasx, pasy, CoveringPriority, TetaLimite)

if my_isequal(pasx, get(this, 'XStep')) && my_isequal(pasy, get(this, 'YStep'))
    that = this;
else
    xmin = floor(min(this.x) / pasx) * pasx;
    xmax = ceil( max(this.x) / pasx) * pasx;
    ymin = floor(min(this.y) / pasy) * pasy;
    ymax = ceil( max(this.y) / pasy) * pasy;
    x = xmin:abs(pasx):xmax;
    y = ymin:abs(pasy):ymax;
    if this.x(end) < this.x(1)
        x = fliplr(x);
    end
    if this.y(end) < this.y(1)
        y = fliplr(y);
    end
    x = centrage_magnetique(x);
    y = centrage_magnetique(y);
    that = extraction(this, 'x', x, 'y', y);
end

%% Calcul des coordonn�es des pixels valide de l'image entrante dans la mosa�que

[subImage, subMos, sub_subImageInMos] = get_CoordinatesOfValidPixelsOfAinB(that, ImageMos);

%% Conditionnement de l'image des angles

if isempty(Angle)
    AngleImage = [];
else
    if my_isequal(pasx, get(Angle, 'XStep')) && my_isequal(pasy, get(Angle, 'YStep'))
        AngleImage = Angle.Image;
    else
        x = get(that, 'x');
        y = get(that, 'y');
        % A valider sur mosaique
        
        AngleTemp = extraction(Angle, 'x', x, 'y', y);
        AngleImage = AngleTemp.Image;
    end
end

%% Assemblage des deux images

[ImageMos, AngleMos] = mosaique_GestionRecouvrementProfils(ImageMos, AngleMos, that, AngleImage, subMos, subImage, sub_subImageInMos, ...
    CoveringPriority, TetaLimite);
