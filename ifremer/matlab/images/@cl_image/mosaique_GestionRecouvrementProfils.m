function [ImageMos, AngleMos] = mosaique_GestionRecouvrementProfils(ImageMos, AngleMos, Image, AngleImage, subMos, subImage, sub_subImageInMos, ...
    CoveringPriority, TetaLimite, maskTop, maskBot)

% NbMaxVal = 10000; % Provoque un pb pour donn�es SHOM D:\Temp\SHOM\Etalonnage Reflectivite\EM712_BHO_REFLEC_CarreRenard_20180214 profils 33 et 36 au pas de 0.5 m
% NbMaxVal = 1000000; % Modif JMA le 07/11/2018

if isempty(AngleImage)
    if ImageMos.nbSlides == 1
        switch CoveringPriority
            case 1 % Remplace
                ImageMos.Image(subMos) = Image.Image(subImage);
            case 2 % % Max
                ImageMos.Image(subMos) = max(ImageMos.Image(subMos), Image.Image(subImage), 'omitnan');
            case 3 % % Min
                ImageMos.Image(subMos) = min(ImageMos.Image(subMos), Image.Image(subImage), 'omitnan');
%             case 4 % % Mean
%                 ImageMos.Image(subMos) = mean([ImageMos.Image(subMos), Image.Image(subImage)], 2, 'omitnan');
%             case 5 % % Median
%                 ImageMos.Image(subMos) = median([ImageMos.Image(subMos), Image.Image(subImage)], 2, 'omitnan');
            case 4 % % Somme
                ImageMos.Image(subMos) = sum([ImageMos.Image(subMos), Image.Image(subImage)], 2, 'omitnan');
            case 5 % Mixage                
                ImageMos.Image(subMos) = sum([maskTop.*Image.Image(subImage), maskBot.*ImageMos.Image(subMos)], 2, 'omitnan');                
        end
    else
        valOut = ImageMos.Image(:,:,:);
        valOut = reshape(valOut, [ImageMos.nbRows*ImageMos.nbColumns ImageMos.nbSlides]);
        sz = size(Image.Image);
        X = reshape(Image.Image(:,:,:), [sz(1)*sz(2) sz(3)]);
        for iCan=1:ImageMos.nbSlides
            valOut(subMos,iCan) = X(subImage,iCan);
        end
        valOut = reshape(valOut, [ImageMos.nbRows ImageMos.nbColumns ImageMos.nbSlides]);
        ImageMos.Image = valOut;
     end
else
    
%{
% En fonction jusqu'au 07/11/2018 mais disfonctionnement trouv� pour donn�es SHOM
    X = true(length(subMos), 1);
    if isequal(sub_subImageInMos, 'AllOfThem')
        for k=1:NbMaxVal:length(X)
            subi = k:min(k-1+NbMaxVal, length(X));
            valAngles = AngleMos(subMos(subi));
            subNaN = isnan(valAngles(:));
            if any(subNaN)
                switch CoveringPriority
                    case 1 % Priorit� au centre
                        %                     X(subi) = (abs(valAngles) >= abs(AngleImage(subImage(subi)))) | isnan(valAngles);
                        
                        % D�but bidouille pour palier erreur cl_memmapfile/get_value (matrices carr�es)
                        Teta = AngleImage(subImage(subi));
                        X(subi(:)) = (abs(valAngles(:)) >= abs(Teta(:))) | subNaN;
                        
                    case 2 % Priorit� au bord
                        %                     X(subi) = (abs(valAngles) <= abs(AngleImage(subImage(subi)))) | isnan(valAngles);
                        Teta = AngleImage(subImage(subi));
                        X(subi(:)) = (abs(valAngles(:)) <= abs(Teta(:))) | subNaN;
                        
                    case 3 % Priorit� au centre except� pour le sp�culaire
                        subPixelsNaN = isnan(valAngles);
                        X(subi) = subPixelsNaN;
                        
                        subPixelsNonNaN = find(~subPixelsNaN);
                        clear subPixelsNaN
                        if ~isempty(subPixelsNonNaN)
                            angleImage = abs(AngleImage(subImage(subi(subPixelsNonNaN))));
                            subLimite = (angleImage > TetaLimite);
                            
                            % Priorit� au centre
                            sub2 = subPixelsNonNaN(subLimite);
                            X(subi(sub2)) = (abs(valAngles(sub2)) >= abs(AngleImage(subImage(subi(sub2)))));
                            
                            % Priorit� au bord
                            sub2 = subPixelsNonNaN(~subLimite);
                            X(subi(sub2)) = (abs(valAngles(sub2)) <= abs(AngleImage(subImage(subi(sub2)))));
                        end
                end
            end
        end
        clear sub_subImageInMos subi
        subsub = X;
    else
        for k=1:NbMaxVal:length(X)
            subi = k:min(k-1+NbMaxVal, length(X));
            valAngles = AngleMos(subMos(subi));
            subNaN = isnan(valAngles);
            if any(subNaN)
                sub3 = sub_subImageInMos(subi);
                sub4 = sub3 <= length(subImage);
                sub3 = sub3(sub4);
                subi = subi(sub4);
                subNaN = subNaN(sub4);
                valAngles = valAngles(sub4);
                switch CoveringPriority
                    case 1 % Priorit� au centre
                        X(subi) = (abs(valAngles) >= abs(AngleImage(subImage(sub3)))) | subNaN;
                    case 2 % Priorit� au bord
                        X(subi) = (abs(valAngles) <= abs(AngleImage(subImage(sub3)))) | subNaN;
                    case 3 % Priorit� au centre except� pour le sp�culaire
                        X(subi) = (abs(valAngles) >= abs(AngleImage(subImage(sub3)))) | subNaN;
                end
            end
        end
        clear sub_subImageInMos subi
        subsub = X;
    end
    %}
      
    if isequal(sub_subImageInMos, 'AllOfThem')
        valAngles = AngleMos(subMos);
        subNaN = isnan(valAngles(:));
        if any(subNaN)
            switch CoveringPriority
                case 1 % Priorit� au centre
                    Teta = AngleImage(subImage);
                    subsub = (abs(valAngles(:)) >= abs(Teta(:))) | subNaN;
                    clear Teta
                    
                case 2 % Priorit� au bord
                    Teta = AngleImage(subImage);
                    subsub = (abs(valAngles(:)) <= abs(Teta(:))) | subNaN;
                    clear Teta
                    
                case 3 % Priorit� au centre except� pour le sp�culaire
                    subPixelsNaN = isnan(valAngles);
                    subsub = subPixelsNaN;
                    
                    subPixelsNonNaN = find(~subPixelsNaN);
                    clear subPixelsNaN
                    if ~isempty(subPixelsNonNaN)
                        angleImage = abs(AngleImage(subImage(subPixelsNonNaN)));
                        subLimite = (angleImage > TetaLimite);
                        
                        % TODO : apparemment il manque quelque-chose ici une des images n'est pas correctement g�r�e au niveau recouvrement (la premi�re ?) JMA le 27/05/2021
                        
                        % Priorit� au centre
                        sub2 = subPixelsNonNaN(subLimite);
                        subsub(sub2) = (abs(valAngles(sub2)) >= abs(AngleImage(subImage(sub2))));
                        
                        % Priorit� au bord
                        sub2 = subPixelsNonNaN(~subLimite);
                        subsub(sub2) = (abs(valAngles(sub2)) <= abs(AngleImage(subImage(sub2))));
                    end
            end
            
        else % TODO : non enti�rement v�rifi� (Case 3 en particulier)
            switch CoveringPriority
                case 1 % Priorit� au centre
                    Teta = AngleImage(subImage);
                    subsub = (abs(valAngles(:)) >= abs(Teta(:)));
                    clear Teta
                    
                case 2 % Priorit� au bord
                    Teta = AngleImage(subImage);
                    subsub = (abs(valAngles(:)) <= abs(Teta(:)));
                    clear Teta
                    
                case 3 % Priorit� au centre except� pour le sp�culaire
                    subPixelsNaN = isnan(valAngles);
                    subsub = subPixelsNaN;
                    
                    subPixelsNonNaN = find(~subPixelsNaN);
                    clear subPixelsNaN
                    if ~isempty(subPixelsNonNaN)
                        angleImage = abs(AngleImage(subImage(subPixelsNonNaN)));
                        subLimite = (angleImage > TetaLimite);
                        
                        % Priorit� au centre
                        sub2 = subPixelsNonNaN(subLimite);
                        subsub(sub2) = (abs(valAngles(sub2)) >= abs(AngleImage(subImage(sub2))));
                        
                        % Priorit� au bord
                        sub2 = subPixelsNonNaN(~subLimite);
                        subsub(sub2) = (abs(valAngles(sub2)) <= abs(AngleImage(subImage(sub2))));
                    end
            end
        end
        clear sub_subImageInMos subNaN valAngles

    else
        valAngles = AngleMos(subMos);
        subNaN = isnan(valAngles);
        sub3 = sub_subImageInMos;
        sub4 = sub3 <= length(subImage);
        sub3 = sub3(sub4);
        sub4 = sub4(sub4);

        subNaN = subNaN(sub4);
        valAngles = valAngles(sub4);
        switch CoveringPriority
            case 1 % Priorit� au centre
                subsub(sub4) = (abs(valAngles) >= abs(AngleImage(subImage(sub3)))) | subNaN;
            case 2 % Priorit� au bord
                subsub(sub4) = (abs(valAngles) <= abs(AngleImage(subImage(sub3)))) | subNaN;
            case 3 % Priorit� au centre except� pour le sp�culaire
                subsub(sub4) = (abs(valAngles) >= abs(AngleImage(subImage(sub3)))) | subNaN;
        end

        clear sub_subImageInMos subNaN valAngles
    end
    
    
    length_subsub = sum(subsub);
    if length_subsub == length(subImage)
        AngleImage = AngleImage(subImage);
    else
        AngleImage = AngleImage(subImage(subsub));
    end
    if length_subsub ~= length(subMos)
        subMos = subMos(subsub);
    end
    AngleMos(subMos) = AngleImage;
    clear AngleImage
    if length_subsub ~= length(subImage)
        subImage = subImage(subsub);
    end
    clear subsub

    if ImageMos.nbSlides == 1
        if isa(Image.Image, 'cl_memmapfile') && isa(ImageMos.Image, 'cl_memmapfile') && strcmp(get(Image.Image, 'FileName'), get(ImageMos.Image, 'FileName'))
%             'nemestra'
        else
            I = Image.Image(subImage);
            clear subImage
            %{
            % Avant le 07/11/2018
            for k=1:NbMaxVal:length(subMos)
                subi = k:min(k-1+NbMaxVal, length(subMos));
                valMos = I(subi);
                ImageMos.Image(subMos(subi)) = valMos;
                %                 ImageMos.Image(subMos(subi)) = nanmean([ImageMos.Image(subMos(subi)), valMos], 2); %GLT modif 01/12/2016 pour moyener
            end
            %}
            
            ImageMos.Image(subMos) = I;
            clear I subMos
        end
    else
        valOut = ImageMos.Image(:,:,:);
        valOut = reshape(valOut, [ImageMos.nbRows*ImageMos.nbColumns ImageMos.nbSlides]);
        sz = size(Image.Image);
        I = reshape(Image.Image(:,:,:), [sz(1)*sz(2) sz(3)]);
        for iCan=1:ImageMos.nbSlides
            valOut(subMos,iCan) = I(subImage,iCan);
        end
        clear subImage subMos
        valOut = reshape(valOut, [ImageMos.nbRows ImageMos.nbColumns ImageMos.nbSlides]);
        ImageMos.Image = valOut;
        clear I
   end
end

% ImageMos = WinFillNaN(ImageMos);
% if ~isempty(AngleMos)
%     AngleMos = fillNaN_mean(AngleMos, [3 3]);
% end
