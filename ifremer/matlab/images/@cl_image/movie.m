% Creation d'un film a partir des images
%
% Syntax
%   movie(a)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Examples
%   for k1=1:8
%       [I, label] = ImageSonar(k1);
%       a(k1) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   end
%   nomFic = fullfile(my_tempdir, 'ExampleMovie.avi');
%   film = movie(a, nomFic);
%   movie2avi(film, '/tmp/pppp.avi')
%
% See also cl_image montage cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, nomFicMovie] = movie(this, nomFic, varargin)

[subx, suby, varargin] = getSubxSuby(this(1), varargin);

[varargin, CLim]     = getPropertyValue(varargin, 'CLim', []);
[varargin, fps]      = getPropertyValue(varargin, 'fps', 15);
[varargin, quality]  = getPropertyValue(varargin, 'quality', 75);
[varargin, Rotation] = getPropertyValue(varargin, 'Rotation', 0);
% [varargin, profile]  = getPropertyValue(varargin, 'profileVideo', 'MotionJPEGAVI'); %#ok<ASGLU>MPEG-4
[varargin, profile]  = getPropertyValue(varargin, 'profileVideo', 'MPEG-4'); %#ok<ASGLU>
nbImages = length(this);
for k1=1:nbImages
    nc = this(k1).nbColumns;
    nl = this(k1).nbRows;
    subx(subx > nc) = [];
    suby(suby > nl) = [];
end

try
    warning off 'MATLAB:DELETE:FileNotFound'
    delete(nomFic)
    warning on 'MATLAB:DELETE:FileNotFound'
catch %#ok<CTCH>
end

mov = VideoWriter(nomFic, profile);
mov.FrameRate = fps;
if ~strcmp(profile, 'Uncompressed AVI')
    mov.Quality = quality;
end
open(mov);

hw = create_waitbar('Movie creation', 'N', nbImages);
for k1=1:nbImages
    my_waitbar(k1, nbImages, hw);
    switch this(k1).ImageType
        case 1 % Intensite
            [a, flag] = Intensity2RGB(this(k1), 'subx', subx, 'suby', suby, 'CLim', CLim);
            if ~flag
                return
            end
        case 2  % RGB
            a = this(k1); % Faudrait faire = extraction(this(k1), 'subx', subx, 'suby', suby)
            %             a  = extraction(this(k1), 'subx', subx, 'suby', suby);
        case 3
            %             a = ind2rgb(this(k1));
            [a, flag] = Indexed2RGB(this(k1), 'subx', subx, 'suby', suby);
            if ~flag
                return
            end
    end
    
    I = a.Image(suby,subx,:);
    %     figure(12346); imagesc(a.x, a.y, I); axis xy
    
    if Rotation ~= 0
        I = my_rot90(I, Rotation);
    end
    
    if ((a.XDir == 1) && (diff(a.XLim) < 0)) || ((a.XDir == 2) && (diff(a.XLim) > 0))
        for k2=1:size(I,3)
            I(:,:,k2) = flipud(I(:,:,k2));
        end
    end
    
    yDir = ((a.y(end) - a.y(1)) > 0);
    ErMapper = ~xor(yDir , (a.YDir == 1));
    if ErMapper
        for k2=1:size(I,3)
            I(:,:,k2) = flipud(I(:,:,k2));
        end
    end
    
    switch class(I(1))
        case {'uint8';'uint16';'uint32'}
            I = uint8(I);
        otherwise
            I = uint8(floor(I*255));
    end
    writeVideo(mov, I);
end
my_close(hw, 'MsgEnd')
close(mov);

nomDirMovie = fileparts(nomFic);
nomFicMovie = fullfile(nomDirMovie, mov.Filename);
% winopen(nomFicMovie)
% % my_implay(nomFic)

flag = 1;
