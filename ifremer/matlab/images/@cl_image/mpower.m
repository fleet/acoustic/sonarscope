% Power of image
%
% Syntax
%   b = a ^ p
%
% Input Arguments
%   a : Instance(s) of cl_image
%   p : Exponent
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = repmat(-pi:0.1:pi, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a)
%
%   b = a ^ 2;
%   imagesc(b);
%
%   c = 2 ^ a;
%   imagesc(c);
%
%   d = a ^ b;
%   imagesc(d);
%
% See also cl_image/sqrt cl_image/log10 Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = mpower(this, puissance, varargin)

if isa(this, 'cl_image') && isnumeric(puissance)
    N = length(this);
    cas = 1;
elseif isnumeric(this) && isa(puissance, 'cl_image')
    N = length(puissance);
    cas = 2;
else
    if length(puissance) == 1
        N = length(this);
        cas = 3;
    else
        if length(this) == 1
            N = length(puissance);
            cas = 4;
        else
            N = length(this);
            if N == length(puissance)
                cas = 5;
            else
                str1 = 'Les deux instances doivent �tre de m�me dimension � moins que l''une d''entre elles soit �gale � 1.';
                str2 = 'The two instances must have the same dimensions unless one is 1.';
                my_warndlg(Lang(str1,str2), 1);
                that = cl_image;
                return
            end
        end
    end
end

that = cl_image.empty;
hw = create_waitbar(waitbarMsg('mpower'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    switch cas
        case 1
            that(k) = mpower_unit(this(k), puissance, varargin{:});
        case 2
            that(k) = mpower_unit(this, puissance(k), varargin{:});
        case 3
            that(k) = mpower_unit(this(k), puissance, varargin{:});
        case 4
            that(k) = mpower_unit(this, puissance(k), varargin{:});
        case 5
            that(k) = mpower_unit(this(k), puissance(k), varargin{:});
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = mpower_unit(this, puissance, varargin)


%% Algorithme

if isa(this, 'cl_image') && isnumeric(puissance)
    [subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>
    
    I = this.Image(suby,subx,:);
    I = singleUnlessDouble(I, this.ValNaN);
    I = I .^ puissance;
    
    %% Output image
    
    that = inherit(this, I,'subx',  subx, 'suby', suby, 'AppendName', 'mpower');
    
elseif isnumeric(this) && isa(puissance, 'cl_image')
    [subx, suby, varargin] = getSubxSuby(puissance, varargin{:}); %#ok<ASGLU>
    
    J = puissance.Image(suby,subx,:);
    J = singleUnlessDouble(J, puissance.ValNaN);
    J = this .^ J;
        
    %% Output image
    
    that = inherit(puissance, J,'subx',  subx, 'suby', suby, 'AppendName', 'mpower');
    
else
    [subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>
    
    if isempty(subx) || isempty(suby)
        that = cl_image;
    else
        if (puissance.nbRows == 0) || (puissance.nbColumns == 0)
            that = cl_image;
        else
            I = this.Image(suby,subx,:);
            I = singleUnlessDouble(I, this.ValNaN);
            J = puissance.Image(suby,subx,:);
            J = singleUnlessDouble(J, puissance.ValNaN);
            I = I .^ J;
            
            %% Output image
            
            that = inherit(this, I,'subx',  subx, 'suby', suby, 'AppendName', 'mpower');
        end
    end
end
