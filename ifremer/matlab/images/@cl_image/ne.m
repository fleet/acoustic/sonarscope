% Check if two instances are different. Two instances are considered equal if
% they met all the here-under conditions :
%    - same size (nbRows, nbColumns, ...)
%    - same Unit, DataType, GeometryType, ImageType, SpectralStatus
%    - same XLabel, XUnit, x, YLabel, YUnit, y
%    - same values
%
% Syntax
%   flag = ne(a, b)
%
% Input Arguments
%   a : One cl_image instance
%   b : One cl_image instance
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Remarks : A message is displayed in case the instances are dirretent, it
%           gives the reason why the test is negative
%
% Examples
%     a = cl_image('Image', Lena);
%     b = cl_image('Image', Lena);
%  flag = ne(a, b)
%  flag = ne(a, -b)
%
% See also cl_image/eq Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = ne(this, a)
flag = ~eq(this, a);
