% Calcul de la norme du gradient de l'image : sqrt(px .^ 2 + py .^ 2);
%
% Syntax
%   b = normeGradient(a, ...) 
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx  : Sub-sampling in abscissa
%   suby  : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = Lena;
%   a = cl_image('Image', I);
%   imagesc(a)
%
%   b = normeGradient(a);
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = normeGradient(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('normeGradient'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = normeGradient_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = normeGradient_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx);
I = singleUnlessDouble(I, this.ValNaN);
[px, py] = gradient(I, 1, 1);
I = sqrt(px .^ 2 + py .^ 2);
Image(:,:) = I;

%% Output image

if isempty(this.Unit) || isempty(this.XUnit) || isempty(this.YUnit)
    Unit = [];
elseif strcmp(this.XUnit, this.YUnit)
    Unit = [this.Unit ' / ' this.XUnit];
else
    Unit = [this.Unit ' / ' this.XUnit ' & ' this.YUnit];
end
that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', 'normeGradient', ...
    'ColormapIndex', 2, 'Unit', Unit);
