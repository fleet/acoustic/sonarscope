function this = offset_axis(this, OffsetX, OffsetY, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_offset_axis(this(k), OffsetX, OffsetY, varargin{:});
end

function this = unitaire_offset_axis(this, OffsetX, OffsetY, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Appel de la fonction de filtrage

this.x = this.x - OffsetX;
this.y = this.y - OffsetY;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'offset_axis');
