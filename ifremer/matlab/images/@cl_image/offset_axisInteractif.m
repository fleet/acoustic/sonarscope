% Absolute value and complex magnitude
%
% Syntax
%  b = offset_axisInteractif(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   subx : Sous-echantillonnage en offset_axisInteractifcisses
%   suby : subsampling in Y
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a);
%
%   b = offset_axisInteractif(a);
%   imagesc(b);
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = offset_axisInteractif(this, XCroix, YCroix, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_offset_axisInteractif(this(k), XCroix, YCroix, varargin{:});
end

function this = unitaire_offset_axisInteractif(this, XCroix, YCroix, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

% D�termination des offsets interactivement

[x,y] = ginput(1);
OffsetX = x - XCroix;
OffsetY = y - YCroix;

% D�calage % Todo : on pourrait plut�t appeler offset_axis

this.x = this.x - OffsetX;
this.y = this.y - OffsetY;

%% Mise � jour des coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'offset_axisInteractif');
