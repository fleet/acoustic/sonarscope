function flag = ombrage_movie(this, nomFic, Azimuths, VertExag, varargin)

[varargin, subx]    = getPropertyValue(varargin, 'subx',    []);
[varargin, suby]    = getPropertyValue(varargin, 'suby',    []);
[varargin, CLim]    = getPropertyValue(varargin, 'CLim',    []);
[varargin, fps]     = getPropertyValue(varargin, 'fps',     15);
[varargin, quality] = getPropertyValue(varargin, 'quality', 75); %#ok<ASGLU> 

AzimuthOrigine  = this.Azimuth;
VertExagOrigine = this.VertExag;

a = cl_image.empty;

N = length(Azimuths);
hw = create_waitbar('Processing Sun Illumination Movie', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    this.Azimuth  = Azimuths(k);
    this.VertExag = VertExag;
    [a(k), flag] = sunShading(this, 'CLim', CLim, 'subx', subx, 'suby', suby, 'Silent', 1);
    if ~flag
        break
    end
    if (N * get_nbBytes(a(k))) > 2e7
        a(k) = Ram2Virtualmemory(a(k));
    end
end
my_close(hw, 'MsgEnd')

this.Azimuth  = AzimuthOrigine;
this.VertExag = VertExagOrigine;

[flag, nomFicMovie] = movie(a, nomFic, 'fps', fps, 'quality', quality);
clear a
if ~flag
    return
end

% if isdeployed
    winopen(nomFicMovie)
% else
%     my_implay(nomFicMovie)
% end
