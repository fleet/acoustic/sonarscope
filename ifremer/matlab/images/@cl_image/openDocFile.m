% Ouverture d'un fichier de bureautique
%
% Syntax
%   openDocFile(cl_image, nomFic)
% 
% Input Arguments
%   a      : instance de cl_image
%   NomFic : Nom du fichier de documentation
%
% Examples 
%   openDocFile(cl_image, 'C:\SonarScopeDoc\AsciiDoctor\SScDoc-Head.html')
%
% See also openDocFile Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function openDocFile(~, nomFic)

global SonarScopeDocumentation %#ok<GVMIS>
global IfrTbxResources %#ok<GVMIS>

if exist(nomFic, 'file')
    openDocFile(nomFic);
else
    if isempty(SonarScopeDocumentation)
        nomFic = fullfile(IfrTbxResources, 'Resources.ini');
        %verifNameLength(nomFic)
        if exist(nomFic, 'file')
            loadVariablesEnv;
        end
    end
    [~, nomFic, ext] = fileparts(nomFic);
    nomFic = fullfile(SonarScopeDocumentation, [nomFic ext]);
    openDocFile(nomFic);
end
