function open_ErViewer(this, nomFic, options)

E0 = cl_ermapper_ers([]);
liste{1} = nomFic;
if options.flagImage
    nomFicAlg = get_nomFicAlg(E0, nomFic);
    if exist(nomFicAlg, 'file')
        liste{end+1} = nomFicAlg;
    end
end
if options.flagShading
    nomFicAlg = get_nomFicAlg(E0, nomFic, 'Shading');
    if exist(nomFicAlg, 'file')
        liste{end+1} = nomFicAlg;
    else
        options.flagShading = 0;
    end
end
[rep, flag] = my_listdlg('Possible ErViewer display', liste, 'InitialValue', min(1, length(liste)));
if ~flag
    return
end

for k2=1:length(rep)
    try
        winopen(liste{rep(k2)})
    catch
        str1 = sprintf('"%s" n''a pas pu �tre ouvert par windows. V�rifiez l''association de programme avec les extensions.', liste{rep(k2)});
        str2 = sprintf('"%s" could not be opened by windows. Check the association of file extensions with programs.', liste{rep(k2)});
        my_warndlg(Lang(str1,str2), 0);
    end
end
if options.flagShading && (get_LevelQuestion >= 3)
    CLim = this.CLim;
    if exist(liste{end}, 'file')
        UiUtils.CreateDialog_SunshadingAlg(liste{end}, 'CLim', CLim, 'FileName', liste{end}, 'Unit', this.Unit)
    end
end
