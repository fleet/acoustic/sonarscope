% Optimisation des courbes de statistiques conditionnelles
%
% Syntax
%   this = optimCourbesStatsAllCurvesTogether(this, Tag, ...)
%
% Input Arguments
%   this : Instance de cl_image
%   Tag  : 'BS' ou 'DiagTx'
%
% Name-Value Pair Arguments
%   sub : liste des courbes a traiter
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = optimCourbesStatsAllCurvesTogether(this, Tag, varargin)

[varargin, Last] = getFlag(varargin, 'Last');
if Last
    sub = length(this.CourbesStatistiques);
else
    [varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));
end

this.CourbesStatistiques = CourbesStatsUnion(this.CourbesStatistiques, 'sub', sub, 'CurveForCompensation', 0);

str1 = 'SonarScope a cr�� une courbe moyenne � partir des courbes s�lectionn�es. Voulez-vous la nettoyer avant de lancer la mod�lisation ?';
str2 = 'SonarScope has created a mean curve from the selected curves. Would you like to clean the mean curve prior to launching the Curve Fitting Interface ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    [this, nbCurvesCreated] = cleanCourbesStats(this, 'sub',  length(this.CourbesStatistiques), 'Stats', 0, 'NewCurve', 0); % NewCurve pas cable dans cleanCourbesStats
end

% this.CourbesStatistiques)-1 pour ne pas prendre la courbe compens�e :
% c'est pas top, je sais
switch Tag
    case 'BS'
        this = optim_BS(this, 'sub', length(this.CourbesStatistiques), varargin{:});
    case 'DiagTx'
        this = optim_DiagTx(this, 'sub', length(this.CourbesStatistiques), varargin{:});
    otherwise
        %messageForDebugInspection
end
