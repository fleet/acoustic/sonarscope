function this = optimDiagTx(this, isub, varargin)

[varargin, InitModel]    = getPropertyValue(varargin, 'InitModel',    []);
[varargin, OrigineModel] = getPropertyValue(varargin, 'OrigineModel', 2); % 1=mod�le constructeur, 2=Modele Ifremer
[varargin, NewCurve]     = getPropertyValue(varargin, 'NewCurve',     0); %#ok<ASGLU>

bilan = this.CourbesStatistiques(isub).bilan{1};
this = set_SonarDescriptionFromCurveParameters(this, bilan);

if isempty(InitModel)
    %{
    if isfield(bilan(1), 'model') && ~isempty(bilan(1).model) && isa(bilan(1).model, 'ClModel')
        flagModelPreexistant = 1;
        for k2=1:length(bilan)
            models(k2) = bilan(k2).model; %#ok<AGROW>
        end
    else
        % TODO : Pour l'instant je n'ai pas la signature du type de Swath
        % dans la courbe alors je passe par cette supercherie. C'est pas
        % top !
        %         kPing = find(this.Sonar.PortMode_1(:,1) == bilan(1).Mode);
        %         this = update_SonarDescription_Ping(this, kPing(1));
        
        flagModelPreexistant = 0;
        if OrigineModel == 1 % mod�le constructeur
            models = get_EmissionModelsConstructeur(this.Sonar.Desciption);
        else % Mod�le Ifremer
            models = get_EmissionModelsCalibration(this.Sonar.Desciption);
        end
    end
    %}
    
	flagModelPreexistant = 0;
    if isfield(bilan(1), 'model')
        for k2=1:length(bilan)
            flagModelPreexistant = 1;
            if ~isempty(bilan(k2).model)
                models(k2) = bilan(k2).model; %#ok<AGROW>
            end
        end
    end
    if ~flagModelPreexistant
        % TODO : Pour l'instant je n'ai pas la signature du type de Swath
        % dans la courbe alors je passe par cette supercherie. C'est pas
        % top !
        %         kPing = find(this.Sonar.PortMode_1(:,1) == bilan(1).Mode);
        %         this = update_SonarDescription_Ping(this, kPing(1));
        
        flagModelPreexistant = 0;
        if OrigineModel == 1 % mod�le constructeur
            models = get_EmissionModelsConstructeur(this.Sonar.Desciption);
        else % Mod�le Ifremer
            models = get_EmissionModelsCalibration(this.Sonar.Desciption);
        end
    end
else
    flagModelPreexistant = 1;
    models = InitModel;
end

flagAntenneSpline = 0;
flagModeleTrouve  = 0;
for k2=1:length(bilan)
    XData = bilan(k2).x(:)';
    if isempty(XData)
        continue
    end
    
    YData   = bilan(k2).y(:)';
    Weights = bilan(k2).nx(:)';
    coef = tan(abs(XData(:)') * (pi / 180));
    Weights = Weights ./ (coef + 1);
    
    if isfield(bilan(k2), 'numVarCondition')
        numVarCondition = bilan(k2).numVarCondition;
    else
        numVarCondition = 1;
    end
    
    if numVarCondition > length(models)
        continue
    end
    model = models(numVarCondition);
    
    if flagModelPreexistant
        nbParams = model.get_nbParams;
        if nbParams == 0
            continue
        end
        
        valParams    = model.getParamsValue;
        valminParams = model.getParamsMinValue;
        valmaxParams = model.getParamsMaxValue;
        
        % TODO : � tester
        model = set(model, 'XData', XData, 'YData', YData);
        model.setParamsValue(valParams);
        model.setParamsMinValue(valminParams);
        model.setParamsMaxValue(valmaxParams);
    end
    
    %% Cr�ation de mod�les
    
    model.XData = XData;
    model.YData = YData;
    
    %% Cas de l'antenne par spline : on retravaille les valeurs min et max
    
    nomModel = model.Name;
    if contains(nomModel, 'AntenneSpline')
        flagAntenneSpline = 1;
        
        XDataModel = model.XData;
        XDataMin = min(min(XData), min(XDataModel));
        XDataMax = max(max(XData), max(XDataModel));
        
%         nbNodesMax = 32;
%         Step = round(0.5 + (XDataMax - XDataMin) / (nbNodesMax-2), 1 , 'significant');
        Step = 1; % Modif JMA le 04/09/2018 pour EM304 � bord de Thalassa
        kk = 0;
        for kModel=1:5
            xNodes = centrage_magnetique((XDataMin-Step):Step:(XDataMax+Step));
%             if length(xNodes) > length(XData)
            if length(xNodes) > (length(XData) + 2)
                Step = Step * 2;
                continue
            end
            kk = kk + 1;
            %             yNodes = interp1(XData, YData, xNodes);
            
            %             yNodes = interp1(XData, YData, xNodes, 'linear', 'extrap');
            % Modif JMA le 18/09/2017
            yNodes = interp1(XData, YData, xNodes, 'linear');
            ki = find(~isnan(yNodes), 1, 'first');
            yNodes(1:(ki-1)) = YData(1);
            ki = find(~isnan(yNodes), 1, 'last');
            yNodes((ki+1):end) = YData(end);
            
            nbNodes = length(xNodes);
            % figure; plot(XData, YData, '-*'); grid on; hold on; plot(xNodes, yNodes, 'or')
            
            subOut = find(xNodes < XData(1));
            if ~isempty(subOut)
                XData   = [xNodes(subOut) XData]; %#ok<AGROW>
                YData   = [yNodes(subOut) YData]; %#ok<AGROW>
                Weights = [repmat(Weights(1), 1, length(subOut)) Weights]; %#ok<AGROW>
            end
            
            subOut = find(xNodes > XData(end));
            if ~isempty(subOut)
                XData   = [XData xNodes(subOut)]; %#ok<AGROW>
                YData   = [YData yNodes(subOut)]; %#ok<AGROW>
                Weights = [Weights repmat(Weights(end), 1, length(subOut))]; %#ok<AGROW>
            end
            
            Name = sprintf('AntenneSpline - %s deg', num2str(Step));
            model(kk) = AntenneSplineModel('Name', Name, 'XData', XData, 'YData', YData, 'xNodes', xNodes, 'yNodes', yNodes);
            if kk == 1
                MaxIter = max(100, 25 * nbNodes);
            end
            %             model.Name = [model.Name '-' num2str(Step) '-deg']; % Manque
            %             la possibilit� de d�finir le nom du model et le nom de
            %             l'intitule : TODO Pierre
            
%             Algo = 1;
            
            %--------------------------------------------------------------
            % partie comment�e car inutile. Si besoin de la r�introduire,
            % modifier les valminParams et valmaxParams pour tenir compte
            % des min et max de YDaata
            
            %             valParams = yNodes;
            %             valminParams = min(valParams) + zeros(size(valParams));
            %             valmaxParams = max(valParams) + zeros(size(valParams));
            %
            %                 % TODO : � v�rifier
            %                 model(kModel).setParamsValue(valParams);
            %                 model(kModel).setParamsMinValue(valminParams);
            %                 model(kModel).setParamsMaxValue(valmaxParams);
            
            %--------------------------------------------------------------
            
            %         MaxIter = max(100, 25 * length(xNodes));
            
            Step = Step * 2;
            if (Step > 16) || ((nbNodes / 2) < 3)
                break
            end
        end
    else
%         Algo = 2;
        MaxIter = 100;
    end
    
    %% Create the ClOptim instance
    
    xlabel = bilan(k2).DataTypeConditions{1};
    ylabel = bilan(k2).DataTypeValue;
    optim = ClOptim('XData', XData, 'YData', YData, ...
        'xLabel', xlabel, ...
        'yLabel', ylabel, ...
        'Name', 'Tx Diagram', 'MaxIter', MaxIter);
    
    %% Set the models in the ClOptim instance
    
    % optim.models = [modeleBSLurton, modeleBSJackson, modeleBSHamilton modeleBSJacksonAPL]; % TODO : ne fonctionne pas
    
    for kModel=1:length(model)
        model(kModel).XData       = XData;
        model(kModel).YData       = YData;
        model(kModel).WeightsData = Weights;
    end
    optim.set('models', model);
    
%     if flagAntenneSpline
        % Run optim once
        currentModel = optim.currentModel;
        %         plot(optim)
        for kModel=1:length(optim.models)
            optim.currentModel = kModel;
            optim.optimize();
            %         plot(optim)
        end
        optim.currentModel = currentModel;
%     end
    
    % editProperties(optim);
    % plot(optim);
    
    %% Create and open the OptimDialog
    
    style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
    a = OptimUiDialog(optim, 'Title', 'Tx Beam Diagram model fitting', 'titleStyle', style1);
    
    if flagAntenneSpline 
        QL = get_LevelQuestion;
        if QL >= 3
            a.openDialog;
        else
            my_warndlg('The OptimDialog is disabled here. You can activate it if you set Question Level to 3', 0, ...
                'Tag', 'OptimDialogOptionel', 'TimeDelay', 10, 'displayItEvenIfSSc', 1);
        end
    else
        a.openDialog;
    end
    
    
    % a.optim.plot;
    % ParamsValue = a.optim.getParamsValue;
    model = a.optim.models(a.optim.currentModel);
    % plot(model)
    %     model = model.clone;
    bilan(k2).model = model;
    
    %% Get the fitted model
    
    % optim = a.optim % TODO : question pour Pierre : pourquoi on a un acc�s
    % direct � optim sans ex�cuter cette fonction ?
    
    MyModel(numVarCondition) = model; %#ok<AGROW>
    %     plot(MyModel)
    flagModeleTrouve = 1;
end
if ~flagModeleTrouve
    message_NoCurve(this)
    return
end

try % Modif JMA le 28/04/2020
    bilan = bilan(1:length(MyModel)); % Rajout� le 06/02/2017
catch
end

if NewCurve
    this.CourbesStatistiques(end+1).bilan{1} = bilan;
else
    this.CourbesStatistiques(isub).bilan{1} = bilan;
end
if OrigineModel == 1
    this.Sonar.Desciption = set_EmissionModelsConstructeur(this.Sonar.Desciption, MyModel);
else
    this.Sonar.Desciption = set_EmissionModelsCalibration(this.Sonar.Desciption, MyModel);
end
