% Optimisation des courbes de statistiques conditionnelles
%
% Syntax
%   this = optim_BS(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%   sub : liste des courbes � traiter
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = optim_BS(this, varargin)

[varargin, ComputeType] = getPropertyValue(varargin, 'ComputeType', 2); % 1=GUI only, 2=background only, 3=background + GUI
[varargin, NewCurve]    = getPropertyValue(varargin, 'NewCurve',    0);
[varargin, XLim]        = getPropertyValue(varargin, 'XLim',    	[]);

[varargin, Last] = getFlag(varargin, 'Last');
if Last
    sub = length(this.CourbesStatistiques);
else
    [varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques)); %#ok<ASGLU>
end

for k1=1:length(sub)
    SonarDescription = get(this, 'SonarDescription');
    
    if isempty(SonarDescription)
        SonarDescription = cl_sounder;
        [~, SonarDescription] = selection(SonarDescription);
    end
    if isempty(SonarDescription)
        str1 = 'L''optimisation propose le mod�le de Jackson pour lequel la fr�quence est requise';
        str2 = 'The optimization needs a frequency value for the BSJackson model';
        [flag, Freq] = inputOneParametre(Lang(str1,str2), 'Value', ...
            'Value', 300, 'Unit', 'kHz', 'MinValue', 10, 'MaxValue', 500);
        if ~flag
            return
        end
    else
        Freq = get(SonarDescription, 'Signal.Freq');
    end
    if isempty(Freq)
        Freq = get(SonarDescription, 'BeamForm.Tx.Freq');
    end
    
    if ~isempty(this.CourbesStatistiques(k1).bilan) && isfield(this.CourbesStatistiques(k1).bilan{1}, 'model')
        bilan = optimBSLurton(this.CourbesStatistiques(sub(k1)).bilan, 'Freq', Freq, 'ComputeType', ComputeType);
        if NewCurve
            this.CourbesStatistiques(end+1) = this.CourbesStatistiques(sub(k1));
            this.CourbesStatistiques(end).bilan = bilan;
        else
            this.CourbesStatistiques(sub(k1)).bilan = bilan;
        end
    else
        InitModel = getLastDefinedModel(this.CourbesStatistiques, {'BSLurton'}, 'sub', sub);
        %{
cccc = this.CourbesStatistiques(sub(k1)).bilan;
optimBSLurton([cccc cccc], 'InitModel', InitModel, 'Freq', Freq);
        %}
        bilan = optimBSLurton(this.CourbesStatistiques(sub(k1)).bilan, 'InitModel', InitModel, ...
            'ComputeType', ComputeType, 'XLim', XLim); % , 'Freq', Freq
        if NewCurve
            this.CourbesStatistiques(end+1) = this.CourbesStatistiques(sub(k1));
            this.CourbesStatistiques(end).bilan = bilan;
        else
            this.CourbesStatistiques(sub(k1)).bilan = bilan;
        end
    end
end
