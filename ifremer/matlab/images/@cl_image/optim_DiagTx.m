% Optimisation des courbes de statistiques conditionnelles
%
% Syntax
%   this = optim_DiagTx(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%   sub : liste des courbes a traiter
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = optim_DiagTx(this, varargin)

[varargin, OrigineModel] = getPropertyValue(varargin, 'OrigineModel', 2);
[varargin, NewCurve]     = getPropertyValue(varargin, 'NewCurve',     0);

[varargin, Last] = getFlag(varargin, 'Last');

if Last
    sub = length(this.CourbesStatistiques);
else
    [varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques)); %#ok<ASGLU>
end

for k1=1:length(sub)
    SonarDescription = get(this, 'SonarDescription');
    % TypeMode_1 = get(SonarDescription, 'Sonar.TypeMode_1');
    TypeMode_2 = get(SonarDescription, 'Sonar.TypeMode_2');
    TypeMode_3 = get(SonarDescription, 'Sonar.TypeMode_3');
    Mode1 = get(SonarDescription, 'Sonar.Mode_1');
    Mode2 = get(SonarDescription, 'Sonar.Mode_2');
    Mode3 = get(SonarDescription, 'Sonar.Mode_3');
    if isempty(TypeMode_3)
        if isempty(TypeMode_2)
            TagSup = {'Sonar.Mode_1', Mode1};
        else
            TagSup = {'Sonar.Mode_1', Mode1; 'Sonar.Mode_2', Mode2};
        end
    else
        TagSup = {'Sonar.Mode_1', Mode1; 'Sonar.Mode_2', Mode2; 'Sonar.Mode_3', Mode3};
    end
    
    %     InitModel = getLastDefinedModel(this.CourbesStatistiques(k1), ...
    %         {'AntenneSincDep'; 'AntenneSinc'; 'AntenneCentraleSimrad'}, 'TagSup', TagSup);
    
    % Modif JMA le 18/09/2017 : sub(k1) au lieu de k1
    InitModel = getLastDefinedModel(this.CourbesStatistiques(sub(k1)), ...
        {'AntenneSincDep'; 'AntenneSinc'; 'AntenneCentraleSimrad'}, 'TagSup', TagSup);
    
    this = optimDiagTx(this, sub(k1), 'InitModel', InitModel, 'OrigineModel', OrigineModel, 'NewCurve', NewCurve);
end
