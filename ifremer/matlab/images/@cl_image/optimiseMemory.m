% Put image in virtual memory if necessary (it analyses the total size of
% images and will put the biggest ones in memory untill reaching the
% maximum size allowed)
%
% Syntax
%   a = optimiseMemory(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   SizeMax : Maximum number of bytes of the Images contained in a
%
% Output Arguments
%   a : Instance(s) of cl_image
%
% Remarks : The global variable SizePhysTotalMemory is used as a threshold
%   when comparing the total size of the images. The user can substitute this
%   implicit value using the PN/PV "SizeMax"
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%     [str, MemMapFileName] = where(a)
%   a = optimiseMemory(a);
%     [str, MemMapFileName] = where(a)
%   a = optimiseMemory(a, 'SizeMax', 0);
%     [str, MemMapFileName] = where(a)
%     a = Virtualmemory2Ram(a);
%     [str, MemMapFileName] = where(a)
%
% See also cl_image/where cl_image/Virtualmemory2Ram cl_image/Ram2Virtualmemory cl_memmapfile Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function this = optimiseMemory(this, varargin)

global SizePhysTotalMemory %#ok<GVMIS> 

[varargin, SizeMax] = getPropertyValue(varargin, 'SizeMax', []); %#ok<ASGLU>
if isempty(SizeMax)
    if isempty(SizePhysTotalMemory)
        loadVariablesEnv
    end
    SizeMax = SizePhysTotalMemory;
end

sub = [];
sz  = [];
for k=1:numel(this)
    if strcmp(get(this(k), 'Memory'), 'On RAM') %&& (this(k).nbColumns ~= 0) && (this(k).nbRows ~= 0)
        if isempty(this(k).Image)
            continue
        end
        switch class(this(k).Image(1))
            case 'logical'
                nbBytes = 1;
            case {'uint8'; 'int8'}
                nbBytes = 1;
            case {'uint16'; 'int16'}
                nbBytes = 2;
            case {'uint32'; 'int32'}
                nbBytes = 4;
            case 'single'
                nbBytes = 4;
            case 'double'
                nbBytes = 8;
        end
        if ~isreal(this(k).Image)
            nbBytes = nbBytes * 2;  % Nombre complexe
        end
        
        sub(end+1) = k; %#ok<AGROW>
        sz(end+1) = this(k).nbRows * this(k).nbColumns * this(k).nbColumns * nbBytes; %#ok<AGROW>
    end
end

[~, ordre] = sort(sz, 'descend');
sub = sub(ordre);
sz  = sz(ordre);

LogSilence = isdeployed || (get_LevelQuestion < 3);
nbBytesMax = (SizeMax / 2) * 1024;
while ~isempty(sz) && (sum(sz) > nbBytesMax)
    this(sub(1)) = Ram2Virtualmemory(this(sub(1)), 'LogSilence', LogSilence);
    sz(1)  = [];
    sub(1) = [];
end
