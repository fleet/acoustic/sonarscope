function [flag, indLayerAngle, ModelName, Parameters, listValueMask, Noise] = paramsBS_CurvesSynthesis(this, indImage)

indLayerAngle = [];
ModelName     = [];
Parameters    = [];
Noise         = [];
listValueMask = [];

%% Test si l'image visualis�e est bien un masque

DataType = cl_image.indDataType('Mask');
flag = testSignature(this(indImage), 'DataType', DataType, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% Liste des valeurs contenues dans le masque

listValueMask = unique(this(indImage).Image(:));
nbModels = length(listValueMask);

%% Recherche du layer d'angle d'incidence

identAngle = cl_image.indDataType('IncidenceAngle');
indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identAngle, 'OnlyOneLayer');
if isempty(indLayerAngle)
    identAngle = cl_image.indDataType('RxAngleEarth');
    indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identAngle, 'OnlyOneLayer', 'AtLeastOneLayer');
    if isempty(indLayerAngle)
        flag = 0;
        return
    end
end

%% Nombre de mod�les

% TODO Pierre Mahoudo : avant suppression de ce bloc, r�gler le pb
%{
nbModels = this(indImage).StatValues.Max - this(indImage).StatValues.Min + 1;

%{
param = ClParametre('Name', 'Number of BS curves', ...
    'MinValue', 1, ...
    'MaxValue', nbModels, ...
    'Value', nbModels);
style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
parent = FigUtils.createSScDialog('Position', centrageFig(600, 25));
% param.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a = SpinnerParametreComponent(parent, 'param', param, 'style', style2);
% TODO : faire un SpinnerDialog � la place % TODO Pierre Mahoudo
%}

p = ClParametre('Name', 'Number of models', 'MinValue', 1, 'MaxValue', nbModels, 'Value', nbModels, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', 'Synthesis of BS images');
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 0 0 -1 0 0];
a.openDialog;
% output :
if ~a.okPressedOut
    flag = 0;
    return
end
nbModels = a.getParamsValue();
%}

%% Param�tres des mod�les

ModelName = 'BSLurton';
Axe = [];
model = BSLurtonModel('XData', -80:80);
for k=1:nbModels
    b = ModelUiDialog(['Parameters for model ' num2str(listValueMask(k))], model);
    b.openDialog
    flag = b.paramDialog.okPressedOut;
    if ~flag
        return
    end
    Parameters(k,:) = b.model.getParamsValue; %#ok<AGROW>
    Axe = plot(b.model, 'Axe', Axe); % TODO Pierre Mahoudo : les courbes ne se rajoutent pas
end

%% Noise

p = ClParametre('Name', 'Noise level', 'MinValue', 0, 'MaxValue', 20, 'Value', 5, 'Unit', 'dB');
a = StyledParametreDialog('params', p, 'Title', 'Synthesis of BS images');
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;

% output :
if ~a.okPressedOut
    flag = 0;
    return
end
Noise = a.getParamsValue();

