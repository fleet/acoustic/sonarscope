function [flag, Seuil] = paramsBinarisation(this)

StatValues = this.StatValues;
Seuil      = StatValues.Moyenne;
Unit       = this.Unit;

str1 = 'Paramètres de binarisation.';
str2 = 'Binarization parameters.';
p = ClParametre('Name', Lang('Seuil','Threshold'), 'Unit', Unit,  'MinValue', -Inf, 'MaxValue', Inf, 'Value', Seuil);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    Seuil = [];
    return
end
Seuil = a.getParamsValue;

