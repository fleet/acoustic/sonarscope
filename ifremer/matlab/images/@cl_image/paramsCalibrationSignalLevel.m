function [flag, indLayerRange] = paramsCalibrationSignalLevel(this, indImage)

flag            = 0;
indLayerRange   = [];
nomsLayersRange = [];

%% Recherche des layers shyncronises en Y et de GeometryType=cl_image.indGeometryType('PingBeam')='BathyFais'  

identRayPathSampleNb = cl_image.indDataType('RayPathSampleNb');
identRayPathLength   = cl_image.indDataType('RayPathLength'); % One way length in meters

TagSynchroX = this(indImage).TagSynchroX;
TagSynchroY = this(indImage).TagSynchroY;
for k=1:length(this.Images)
    TagX = this.Images(k).TagSynchroX;
    TagY = this.Images(k).TagSynchroY;
    if strcmp(TagY , TagSynchroY) && strcmp(TagX, TagSynchroX)
        DataType = this.Images(k).DataType;
        if (DataType == identRayPathSampleNb) || (DataType == identRayPathLength) 
            indLayerRange(end+1)   = k; %#ok<AGROW>
            nomsLayersRange{end+1} = this.Images(k).Name; %#ok
        end
    end
end

if isempty(indLayerRange)
    str1 = 'Il faut un layer de type Range pour r�aliser ce traitement.';
    str2 = 'A "Range" layer is necessary for this processing.';
    my_warndlg(Lang(str1,str2), 1);
    return
elseif length(indLayerRange) > 1
    [choix, flag] = my_listdlg(Lang('Layers � utiliser ?','Layers to use ?'), nomsLayersRange);
    if ~flag
        return
    end
    indLayerRange = indLayerRange(choix);
else
    flag = 1;
end
