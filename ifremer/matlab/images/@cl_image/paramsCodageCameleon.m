function [flag, Mean, Std] = paramsCodageCameleon(~)

Mean = 0;
Std  = 1;

p    = ClParametre('Name', 'Mean', 'MinValue', -Inf, 'MaxValue', Inf, 'Value', Mean);
p(2) = ClParametre('Name', 'Std',  'MinValue', -Inf, 'MaxValue', Inf, 'Value', Std);
a = StyledSimpleParametreDialog('params', p, 'Title', 'IFREMER - SonarScope : Change stats');
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-1 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
X = a.getParamsValue;
Mean = X(1);
Std  = X(2);
