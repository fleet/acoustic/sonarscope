function [flag, N, Limits] = paramsCodageQuantification(this)

N = [];
Limits = [];

str1 = 'Type de quantification';
str2 = 'Type of quantization';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang('N iso-classes', 'N iso class'), Lang('Fronti�res sp�cifiques', 'Specific limits'));
if ~flag
    return
end

if rep == 1
    str1 = 'Param�tres de quantification';
    str2 = 'Quantization parameters';
    [flag, N] = inputOneParametre(Lang(str1,str2), Lang('Nombre de niveaux','Number of levels'), ...
        'Value', 255, 'Format', '%d');
    if ~flag
        return
    end
else
%     Limits = -75:10:75;
    Limits = get(this, 'HistoCentralClasses');
    Limits = Limits(:); % Pour �viter cl_memmapfile
    Limits = centrage_magnetique(linspace(Limits(1), Limits(end), 10));
    def    = num2str(Limits);
    answer = inputdlg(Lang('Saisie des fronti�res de classes de l''image conditionnelle', 'Input the  boundary values of the conditional image'), 'IFREMER', 1, {def});
    if isempty(answer)
        return
    end
    Limits = str2num(answer{1}); %#ok
    if isempty(Limits)
        my_warndlg(['Valeurs non interpretables : ' answer{1}], 0);
        flag = false;
        return
    end
end

flag = true;
