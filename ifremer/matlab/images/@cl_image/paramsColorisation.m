function [flag, indLayerMask, valMask, alpha, zone] = paramsColorisation(this, indImage)

alpha = []; 

[flag, indLayerMask, valMask, zone] = paramsMasquage(this, indImage);
if flag
    str1 = 'Colorisation';
    str2 = 'Colorisation';
    p = ClParametre('Name', Lang('Alpha','Alpha'), 'MinValue', 0, 'MaxValue', 1, 'Value', 0.9);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 0 -2 0 -1 0 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    alpha = a.getParamsValue;
end
