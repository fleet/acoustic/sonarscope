function [flag, indLayerMask] = paramsCombineMasks(this, indImage, varargin)

indLayerMask = [];

identMasque       = cl_image.indDataType('Mask');
identSegmentation = cl_image.indDataType('Segmentation');

flag = testSignature(this(indImage), 'DataType', [identMasque identSegmentation]);
if ~flag
    return
end

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage); %, 'IncludeCurrentImage');
if ~flag
    return
end

if isempty(indLayers) % Pas de layer "Masque" trouve
    flag = 0;
    return
end

sub = find((DataTypes == identMasque) | ...
    (DataTypes == identSegmentation)); % Suppression du type "unknown" par JMA le 08/03/2015

if isempty(sub)
    indLayerMask = [];
    str1 = 'Il n''y a pas de masque synchronis� � cette image.';
    str2 = 'There is no mask synchronized to this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

indLayers  = indLayers(sub);
nomsLayers = nomsLayers(sub);

InitialValue = 1;     
str1 = 'Choix d''un masque � appliquer sur l''image';
str2 = 'Mask choice to apply on the image';
[choix, flag] = my_listdlgMultiple(Lang(str1,str2), nomsLayers, ...
	'SelectionMode', 'Single', 'InitialValue', InitialValue);
if ~flag
    indLayerMask = [];
    return
end
indLayerMask = indLayers(choix);
