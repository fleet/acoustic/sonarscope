function [flag, bilan, identLayersCondition, ModeDependant, UseModel] = paramsCompensationCourbesStats(this, indImage, subCourbe)

identLayersCondition = [];
bilan         = [];
ModeDependant = 0;
UseModel      = 0;

this = correctif_structureCourbesStatistiques(this);

CourbesStatistiques = get_CourbesStatistiques(this(indImage), 'sub', subCourbe);
if isempty(CourbesStatistiques)
    flag = 0;
    identLayersCondition = [];
    str1 = 'Pas de courbe definie pour ce type de donnee.';
    str2 = 'No curve defined for this type of data.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

% titreV          = CourbesStatistiques.bilan{1}(1).titreV
% nomX            = CourbesStatistiques.bilan{1}(1).nomX
% nomVarCondition = CourbesStatistiques.bilan{1}(1).nomVarCondition

DataTypeValue      = CourbesStatistiques.bilan{1}(1).DataTypeValue;
indDataTypeValue = cl_image.indDataType(DataTypeValue);

flag = testSignature(this(indImage), 'DataType', indDataTypeValue);
if ~flag
    return
end

DataTypeConditions = CourbesStatistiques.bilan{1}(1).DataTypeConditions;
Support = CourbesStatistiques.bilan{1}(1).Support;

for k=1:length(DataTypeConditions)
    if strcmp(Support{k}, 'Axe')
        flag = strcmp(CourbesStatistiques.bilan{1}(1).DataTypeValue, cl_image.strDataType{indDataTypeValue});
        if ~flag
            str1 = sprintf('Condition non v�rifi�e pour cette compensation :\nDataType attendue : %s\nDataType de l''image :%s', ...
                CourbesStatistiques.bilan{1}(1).DataTypeValue, cl_image.strDataType{this(indImage).DataType});
            str2 = sprintf('Condition not verified for this compensation :\nDataType expected : %s\nDataType of this image :%s', ...
                CourbesStatistiques.bilan{1}(1).DataTypeValue, cl_image.strDataType{this(indImage).DataType});
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        
        if isfield(CourbesStatistiques.bilan{1}(1), 'GeometryType') % Rajout� le 14/04/2013 par JMA pour courbe horizontale d'un WC
            flag = strcmp(CourbesStatistiques.bilan{1}(1).GeometryType, cl_image.strGeometryType{this(indImage).GeometryType});
            if ~flag
                str1 = sprintf('Condition non v�rifi�e pour cette compensation :\nGeometryType attendue : %s\nGeometryType de l''image :%s', ...
                    CourbesStatistiques.bilan{1}(1).GeometryType, cl_image.strGeometryType{this(indImage).GeometryType});
                str2 = sprintf('Condition not verified for this compensation :\nGeometryType expected : %s\nGeometryType of this image :%s', ...
                    CourbesStatistiques.bilan{1}(1).GeometryType, cl_image.strGeometryType{this(indImage).GeometryType});
                my_warndlg(Lang(str1,str2), 1);
                return
            end
        end
%{   
bilan{1}(1).DataTypeValue
Reflectivity

bilan{1}(1).GeometryType
PingAcrossDist
        %}

    elseif strcmp(Support{k}, 'SignalVert')
%         Support{k}
    else
        if strcmp(DataTypeConditions{k}, 'Mask')
            indDataTypeConditions = cl_image.indDataType('Mask');
            [indLayer, nomLayers] = findIndLayerSonar(this, indImage, 'DataType', indDataTypeConditions);
            identDataSegmentation   = cl_image.indDataType('Segmentation');
            [indLayerSegmentation, nomLayersSegmentation] = findIndLayerSonar(this, indImage, 'DataType', identDataSegmentation);
            indLayer  = [indLayer indLayerSegmentation]; %#ok<AGROW>
            nomLayers = [nomLayers nomLayersSegmentation]; %#ok<AGROW>
        else
            indDataTypeConditions = cl_image.indDataType(DataTypeConditions{k});
            [indLayer, nomLayers] = findIndLayerSonar(this, indImage, 'DataType', indDataTypeConditions);
            
            % Ajout JMA le 02/01/2019 pour .all V1 : ajout temporaire mais qui est en principe devenu inutile
            if isempty(indLayer) && strcmp(DataTypeConditions{k}, 'BeamPointingAngle')
                indDataTypeConditions = cl_image.indDataType('TxAngle');
                [indLayer, nomLayers] = findIndLayerSonar(this, indImage, 'DataType', indDataTypeConditions);
            end
        end

        switch length(indLayer)
            case 0
                str1 = 'Pas de layer conditionnel trouv�';
                str2 = 'No conditional layer was found.';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                identLayersCondition = [];
                return
                
            case 1
                
            otherwise
                str1 = 'Image � utiliser :';
                str2 = 'Image to use :';
                [choix, flag] = my_listdlg(Lang(str1,str2), nomLayers, 'SelectionMode', 'Single');
                if ~flag
                    identLayersCondition = [];
                    return
                end
                indLayer = indLayer(choix);
                if isempty(indLayer)
                    flag = 0;
                    identLayersCondition = [];
                    return
                end
        end

        identLayersCondition(end+1) = indLayer; %#ok
    end
end
bilan = CourbesStatistiques.bilan;

if isfield(bilan{1}(1), 'model') && ~isempty(bilan{1}(1).model)
    str1 = 'Que voulez-vous utiliser ?';
    str2 = 'What do you want to use ?';
    str{1} = Lang('Courbe statistique','Statistique curve');
    str{2} = Lang('Mod�le','Model');
    [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    UseModel = (rep == 2);
end

if isfield(bilan{1}(1), 'Mode') && ~isempty(bilan{1}(1).Mode) && (bilan{1}(1).Mode ~= 0)
    SelectMode =  bilan{1}(1).Mode;
    str1 = sprintf('Cette courbe statistique a une signature de mode de fonctionnement du sondeur (Mode %d). Voulez-vous r�aliser ce traitement uniquement sur les pings de m�me mode (les autres pings ne seront pas modifi�s) ? ', SelectMode);
    str2 = sprintf('This statistic curve has a sounder mode signature (Mode %d). Do you want to process only the pings set to that mode (the others will not be modified) ? ', SelectMode);
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    ModeDependant = (rep == 1);
    
    % {
    % Modif GLT : on rajoute une image de mode dans les layer conditionnels si disponible
    if ModeDependant && ~((this(indImage).GeometryType ~= cl_image.indGeometryType('LatLong')) ...
            || (this(indImage).GeometryType ~= cl_image.indGeometryType('GeoYX')))
        indDataTypeMode = cl_image.indDataType('Mode');
        [indLayer, nomLayers] = findIndLayerSonar(this, indImage, 'DataType', indDataTypeMode);
        switch length(indLayer)
            case 0
                str1 = 'Pas de layer Mode trouv�';
                str2 = 'No Mode layer was found.';
                my_warndlg(Lang(str1,str2), 1);
            case 1
                
            otherwise
                str1 = 'Image � utiliser pour le Mode :';
                str2 = 'Image to use for Mode :';
                [choix, flag] = my_listdlg(Lang(str1,str2), nomLayers, 'SelectionMode', 'Single');
                if ~flag
                    indLayer = [];                    
                else
                    indLayer = indLayer(choix);                
                end
        end
        
        identLayersCondition(end+1) = indLayer;
    end
    % }
    % Fin modif  
end
