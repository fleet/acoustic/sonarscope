function [flag, TypeOperation, TypeStats] = paramsCompensationTypeOperation(this, varargin)

[varargin, NoMedian] = getFlag(varargin, 'NoMedian'); %#ok<ASGLU>

TypeOperation = [];

flag = testSignature(this, 'ImageType', 1);
if ~flag
    TypeStats = [];
    return
end

if get_LevelQuestion >= 3
    listeOperateurs = {'-';'/';'+';'*';'-';'/';'+';'*'};
    liste = {'Data - Mean (*** The classical method ***)'
        'Data / Mean (Perhaps useful for bathymetry artefact ?)'
        'Data + Mean (You risk to amplify artefacts)'
        'Data * Mean (You risk to amplify artefacts)'
        'Data - Median'
        'Data / Median'
        'Data + Median'
        'Data * Median'};
    if NoMedian
        liste = liste(1:4);
    end
    str1 = 'Type d''opérateur';
    str2 = 'Type of operator';
    [choix, flag] = my_listdlg(Lang(str1,str2), liste, 'SelectionMode', 'Single', ...
        'InitialValue', 1, 'ColorLevel', 3);
    if flag
        TypeOperation = listeOperateurs{choix};
    else
        TypeOperation = [];
        return
    end
    if choix <= 4
        TypeStats = 'Mean';
    else
        TypeStats = 'Median';
    end
else
    TypeOperation = '-';
    TypeStats = 'Mean';
    flag = 1;
end
