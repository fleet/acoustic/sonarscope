function [flag, indLayer] = paramsCompleteAWithB(this, indImage)

identLayer = this(indImage).DataType;
[flag, indLayer, nomsLayers] = listeLayersSynchronises(this, indImage, 'TypeDonneUniquement', identLayer);
if ~flag
    return
end

switch length(indLayer)
    case 0
        my_warndlg(Lang('Aucune image de m�me nature a �t� trouv�e', 'No similar image has been found.'), 1)
        flag = 0;
        return
    otherwise
        str1 = 'S�lectionnez la seconde image';
        str2 = 'Select second image';
        [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single', 'InitialValue', length(nomsLayers));
        if ~flag
            return
        end
        indLayer = indLayer(choix);
end
