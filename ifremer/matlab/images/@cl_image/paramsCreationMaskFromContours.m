function [flag, creationLayer, nomLayer, indLayerMask] = paramsCreationMaskFromContours(this, indImage)

creationLayer = 0;
nomLayer      = '';
indLayerMask  = [];
flag          = 0;

%% Lecture des r�gions d'int�r�t contenues dans l'image courante

ROI = get(this(indImage), 'RegionOfInterest');
if isempty(ROI)
    str1 = 'Il n''y a pas de r�gion d''int�r�t dans cette image.';
    str2 = 'No Region Of Interest in this image.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Identification du masque

[flag, creationLayer, nomLayer, indLayerMask] = question_Mask(this, indImage);
if ~flag
    return
end
