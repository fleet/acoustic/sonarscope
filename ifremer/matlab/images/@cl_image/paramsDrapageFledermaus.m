function [flag, indlayerDrapage, Colormap] = paramsDrapageFledermaus(this, indImage)

indlayerDrapage = [];

ImageType = this(indImage).ImageType;
[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage);
if ~flag
    return
end

if (ImageType == 2)
    subTI = [];
    for i=1:length(indLayers)
        TI = this(indLayers(i)).ImageType;
        if TI == 1
            subTI(end+1) = i; %#ok<AGROW>
        end
    end
    indLayers = indLayers(subTI);
    nomsLayers = nomsLayers(subTI);
end

if (ImageType == 1) && isempty(indLayers)
    return
    
elseif (ImageType == 1) && ~isempty(indLayers)
    nomsLayers(2:end+1) = nomsLayers;
    nomsLayers{1} = Lang('Aucune', 'None');
    str1 = 'Drapage d''une autre image ?';
    str2 = 'Wrap another image ?';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    if choix ~= 1
        indlayerDrapage = indLayers(choix - 1);
        Colormap= this(indlayerDrapage).Colormap;
    end
    
elseif (ImageType == 2) && ~isempty(indLayers) % Image RGB
    str1 = 'Image d''élévation ?';
    str2 = 'Elevation image ?';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    indlayerDrapage = indLayers(choix);
    Colormap= this(indlayerDrapage).Colormap;
    
elseif (ImageType == 2)
    str1 = 'Cette image est en RBG et il n''y a aucune image d''élévation : Opération impossible.';
    str2 = 'RGB image and no elevation image : Impossible processing.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    
else
    str1 = 'Opération impossible.';
    str2 = 'Impossible processing.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
end
