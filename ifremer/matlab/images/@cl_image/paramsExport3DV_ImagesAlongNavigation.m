function [flag, NomXML, TailleMax, typeOutputFormat, repExport] = paramsExport3DV_ImagesAlongNavigation(this, indImage, repExport)

NomXML           = [];
TailleMax        = 10e6 ; % 2500*1600;
typeOutputFormat = [];
        
%% Test de la signature

flag = testSignature(this(indImage), 'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingDepth')]);
if ~flag
    return
end

%% Taille max des images

if get_LevelQuestion >= 3
    [flag, value] = inputOneParametre('Maximum size (M pixels)', 'Value', ...
        'Value', 4, 'Unit', 'mega pixels', 'MinValue', 0.5);
    if ~flag
        return
    end
else
    value = 10; % 4;
end
TailleMax = value * 1e6;

%% Type de fichier en sortie

str = {'XML-Bin'; 'Netcdf'};
[typeOutputFormat, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
if typeOutputFormat == 1
    Ext = '.xml';
else
    Ext = '.g3d.nc';
end

%% Nom du fichier XML

ImageName = extract_ImageName(this(indImage));
filtreXml = fullfile(repExport, [ImageName Ext]);
[flag, NomXML] = my_uiputfile('*.xml', Lang('Nom du fichier', 'Give a file name'), filtreXml);
if ~flag
    return
end

[~, NomLayer] = fileparts(NomXML);
if length(NomLayer) < 8
    str1 = sprintf('Sorry, "%s" is to short for the GLOBE, you have to give a name containing at least 7 characters.\nThis will be corrected in the future version of GLOBE.', NomLayer);
    my_warndlg(str1, 1);
    flag = 0;
    return
end
repExport = fileparts(NomXML);
