function [flag, nomFic, indLayers, flagX, flagY, typeExport, repExport] = paramsExportAsciiMultiLayer(this, indImage, repExport)

indLayers  = [];
flagX      = false;
flagY      = false;
typeExport = [];

% flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('LatLong'));
% if ~flag
%     return
% end

filtre = fullfile(repExport, 'MultiLayerAscii.csv');
[flag, nomFic] = my_uiputfile('*.csv', Lang('Nom du fichier', 'Give a file name'), filtre);
if ~flag
    return
end
repExport = fileparts(nomFic);
 
[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'OmitImageNumber', 1); %#ok<ASGLU>
if ~flag
    return
end

%% A faire quand y'aura le temps
% nomsLayers{end+1} = 'Axis X of the current image';
% nomsLayers{end+1} = 'Axis Y of the current image';

% Sélection des layers parmis ceux synchronisés.
[flag, ItemsSelect, strOut] = summary(this(indLayers), 'ExportResults', 0, 'QueryMode', 'MultiSelect');
if ~flag
    % Bouton Cancel
    return
end
if isempty(ItemsSelect) || isempty(strOut)
    flag = 0;
    return
end
indLayers = indLayers(ItemsSelect);

%% Type d'export

str{1} = 'List of values with colocation, without missing pixels';
str{2} = 'List of values with colocation, with missing pixels';
str{3} = 'List of values without colocation';
[typeExport, flag] = my_listdlg('Type of export', str, 'SelectionMode', 'Single');
if ~flag
    return
end
if typeExport == 3
    return
end

%% Axes

clear str
str{1} = [this(indImage).XLabel ' (' this(indImage).XUnit ')'];
str{2} = [this(indImage).YLabel ' (' this(indImage).YUnit ')'];
[rep, flag] = my_listdlgMultiple('You can add X and Y as first columns', str ,'InitialValue', 1:2);
if ~flag
    return
end
flagX = any(rep == 1);
flagY = any(rep == 2);

flag  = 1;
