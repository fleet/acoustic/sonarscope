function options = paramsExportErMapper(this)

options.flagShading = is_shading_possible(this);

if this.ImageType == 1
    options.flagImage = 1;
else
    options.flagImage = 0;
end
