function [flag, nomFic, repExport] = paramsExportGeotif(this, indImage, repExport, varargin)

[varargin, exportFmt] = getPropertyValue(varargin, 'ExportFormat', 'Tiff'); %#ok<ASGLU>

nomFic = [];

%% Test si on est bien en LatLong ou GeoYX, et Intensité

flag = testSignature(this(indImage), 'GeometryType', [cl_image.indGeometryType('LatLong') cl_image.indGeometryType('GeoYX')], ...
    'ImageType', 1);
if ~flag
    return
end

%% Nom du fichier

% [flag, nomFic, this] = getFileNameLinked2ImageName(this, '.tif', 'FullName');
% TODO : remplacer ce qui suit et activer getFileNameLinked2ImageName
% Le format BAG est une déclinaison du format Geotiff (module bag_converter)
if strcmpi(exportFmt, 'Tiff')
    NomImage = code_ImageName(this(indImage), 'Append', '_32bits');
    filtre   = fullfile(repExport, [NomImage '.tif']);
    extOut   = '*.tif';
    labelOut = 'GeoTiff';
else
    NomImage = code_ImageName(this(indImage));
    filtre   = fullfile(repExport, [NomImage '.bag']);
    extOut   = '*.bag';
    labelOut = 'BAG';
end

str1 = [labelOut ' : Nom du fichier'];
str2 = [labelOut ' : Give a file name'];
[flag, nomFic] = my_uiputfile({extOut}, Lang(str1,str2), filtre);
if ~flag
    return
end
repExport = fileparts(nomFic);
