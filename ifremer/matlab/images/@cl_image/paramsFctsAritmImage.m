function [flag, ident, isSynchronized, indLayerMask, valMask, zone] = paramsFctsAritmImage(this, indImage, msg, varargin)

[varargin, IncludeCurrentImage] = getFlag(varargin, 'IncludeCurrentImage'); %#ok<ASGLU>

indLayerMask = [];
valMask      = [];
zone         = [];

str = {};
listeIdent = [];
for k=1:length(this)
    if IncludeCurrentImage
        str{end+1} = this(k).Name; %#ok
        listeIdent(end+1) = k; %#ok<AGROW>
    else
        if k ~= indImage
            str{end+1} = this(k).Name; %#ok
            listeIdent(end+1) = k; %#ok<AGROW>
        end
    end
end

[flag, ident] = uiSelectImagesSingle('ExtensionImages', str, 'TitreImages', msg);
if ~flag
    isSynchronized = [];
    return
end
ident = listeIdent(ident);

TagX1 = this(indImage).TagSynchroX;
TagY1 = this(indImage).TagSynchroY;
TagX2 = this(ident).TagSynchroX;
TagY2 = this(ident).TagSynchroY;

if strcmp(TagX1, TagX2) && strcmp(TagY1, TagY2)
    isSynchronized = true;
else
    isSynchronized = false;
end

if nargout == 6
    [flag, indLayerMask, valMask, zone] = paramsMasquage(this, indImage);
end
