function [flag, indLayers, XLim, YLim, ImageName, ComputationUnit] = paramsFctsAritmMultiImages(this, indImage, nameOperator, varargin)

[varargin, flagTypeCadre]    = getPropertyValue(varargin, 'flagTypeCadre',    1);
[varargin, flagImageName]    = getPropertyValue(varargin, 'flagImageName',    1);
[varargin, flagSelectLayers] = getPropertyValue(varargin, 'flagSelectLayers', 1); %#ok<ASGLU>

XLim            = [];
YLim            = [];
ImageName       = [];
ComputationUnit = [];

%% S�lection des images � aditionner

nomImage = this(indImage).Name;
% [flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage'), 'memeDataType');
% Modif JMA le 03/05/2016 car impossible de faire moyenne quadratique de
% SlopeAcross et SlopeAlong. Attendre que l'on ait DataType1 et DataType2
[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage'); %, 'memeDataType');
if ~flag
    return
end

if flagSelectLayers
    indImage = find(strcmp(nomImage, nomsLayers));
    [flag, choix] = uiSelectImages('ExtensionImages', nomsLayers, 'InitialValue', indImage);
    if ~flag
        str1 = 'Aucune image synchronis�e � l''image courante.';
        str2 = 'None image is synchrinized to the current one.';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    % indLayers = [indImage indLayers(choix)];
    indLayers = indLayers(choix);
    DataTypes = DataTypes(choix);
end

sub = [];
N = length(indLayers);
for k=1:N
    if this(indLayers(k)).ImageType ~= 1 % Intensity
        Titre = this(indLayers(k)).Name;
        str1 = sprintf('"%s" n''est pas une image d''intensit�, elle ne peut pas �tre additionn�e � d''autres.', Titre);
        str2 = sprintf('"%s" is not an "Intensity" image, it cannot be added to others.', Titre);
        my_warndlg(Lang(str1,str2), 1);
        sub(end+1) = k; %#ok<AGROW>
    end
end
indLayers(sub) = [];
DataTypes(sub) = [];
if isempty(indLayers)
    str1 = 'Aucune image retenue.';
    str2 = 'No image was kept.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Type de cadrage

if flagTypeCadre
    str{1} = Lang('Cadrage actuel', 'Current extent');
    str{2} = Lang('Plus petite zone commune', 'Smallest crop');
    str{3} = Lang('Plus grande zone commune', 'Largest crop');
    str1 = 'Cadre de l''image r�sultante :';
    str2 = 'Frame of the resulting image :';
    [choixCadre, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 3, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    
    %% Recherche du cadre le plus large
    
    switch choixCadre
        case 1 % Current extent
            XLim = get(this(indImage), 'XLim');
            YLim = get(this(indImage), 'YLim');
            
        case 2 % Smallest crop
            XLim = [-Inf Inf];
            YLim = [-Inf Inf];
            for k=1:N
                [XL, YL] = getXLimYLimForCrop_image(this(indLayers(k)));
                XLim(1) = max(XLim(1), min(XL));
                XLim(2) = min(XLim(2), max(XL));
                YLim(1) = max(YLim(1), min(YL));
                YLim(2) = min(YLim(2), max(YL));
            end
            if (XLim(1) > XLim(2)) || (YLim(1) > YLim(2))
                str1 = 'Pas de zone commune entre toutes ces images.';
                str2 = 'No common area between all these images.';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                my_close(hw)
                return
            end
            
        case 3 % Largest crop
            XLim = [Inf -Inf];
            YLim = [Inf -Inf];
            for k=1:N
                [XL, YL] = getXLimYLimForCrop_image(this(indLayers(k)));
                XLim(1) = min(XLim(1), min(XL));
                XLim(2) = max(XLim(2), max(XL));
                YLim(1) = min(YLim(1), min(YL));
                YLim(2) = max(YLim(2), max(YL));
            end
    end
else
    XLim = [-Inf Inf];
    YLim = [-Inf Inf];
end

%% If reflectivity

switch nameOperator
    case 'Mean'
        Unit = this(indLayers(1)).Unit;
        if (DataTypes(1) == cl_image.indDataType('Reflectivity')) && strcmp(Unit, 'dB')
            str1 = 'Calcul de la valeur en';
            str2 = 'Compute values in';
            listUnits = {'dB', 'Amp', 'Enr'};
            [rep, flag] = my_listdlg(Lang(str1,str2), listUnits, 'InitialValue', 2, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            ComputationUnit = listUnits{rep};
        end
    case {'Min'; 'Max'}
        Unit = this(indLayers(1)).Unit;
        if strcmp(Unit, 'deg')
            str1 = 'Calcul sur la valeur absolue ?';
            str2 = 'Compute on absolute value';
            [ComputationUnit, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
        end
end

%% Nom de l'image de sortie

if flagImageName
    nomVar = {Lang('Nom de l''image.', 'Name of the image.')};
    ImageName = sprintf('%s_%d_Images', nameOperator,  N);
    [ImageName, flag] = my_inputdlg(nomVar, {ImageName});
    if ~flag
        return
    end
    if ~isempty(ImageName)
        ImageName = ImageName{1};
    end
else
    ImageName = sprintf('%s_%d_Images', nameOperator,  N);
end
