function [flag, s, indLayerMask, valMask, zone] = paramsFctsAritmScalaire(this, indImage, msg)
 
flag         = 0;
s            = [];
indLayerMask = [];
valMask      = [];
zone         = [];

answer = inputdlg(msg);
if isempty(answer)
    return
end
s = str2num(answer{1}); %#ok<ST2NM>

[flag, indLayerMask, valMask, zone] = paramsMasquage(this, indImage);
