function [flag, ListeSignauxVert] = paramsFctsAritmSignalVert(this, msg)
 
liste = get_liste_vecteurs(this, 'VerticalOnly');
[ident, flag] = my_listdlg(msg, liste, 'SelectionMode', 'Single');
ListeSignauxVert = liste{ident};
