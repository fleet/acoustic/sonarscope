function [flag, repExport, nomFic, fps, indLayers, CLim, quality, Rotation, profileVideo] = paramsFonctionAnimation(this, indImage, repExport)

nomFic       = [];
fps          = [];
CLim         = [];
quality      = [];
Rotation     = [];
profileVideo = [];

%% Selection des images a include dans le film

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'memeDataType', 'IncludeCurrentImage', ...
    'OmitImageNumber', 1);
if ~flag
    return
end

[rep, flag] = my_listdlg(Lang('S�lection des images', 'Select images'), nomsLayers, 'InitialValue', 1:length(nomsLayers));
if ~flag
    return
end

if isempty(rep)
    flag = 0;
    return
end
indLayers = indLayers(rep);

%% Selection du fichier de sortie

nomFic = fullfile(repExport, 'Film.avi');
[flag, nomFic] = my_uiputfile('*.avi', 'Give a file name', nomFic);
if ~flag
    return
end
repExport = fileparts(nomFic);

str1 = 'TODO';
str2 = 'Movie maker parametres';
p(1) = ClParametre('Name', Lang('Frames par seconde','Frames per second'),...
    'Unit', 'Hz', 'MinValue', 1, 'MaxValue', 25,  'Value', 2);
p(2) = ClParametre('Name', Lang('Qualit� de la compression','Compression Quality'), ...
    'Unit', 'Hz', 'MinValue', 1, 'MaxValue', 100, 'Value', 75);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
X = a.getParamsValue;
fps     = X(1);
quality = X(2);

%% Bornes de rehaussement de contraste

CLim = this(indImage).CLim;
sameCLim = true;
for i=1:length(indLayers)
    if ~isequal(this(indImage).CLim, CLim)
        sameCLim = false;
        break
    end
end
if ~sameCLim
    
end

%% Rotation

str1 = 'Rotation ?';
str2 = 'Rotation ?';
str = {'No rotation'; '90� Clockwise'; '90� Anti Clockwise'; '190�'};
[Rotation, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end
Rotation = Rotation - 1; % 0=0�, 1 = +90�, 2 = 180�, 3=-90�

%% Profile de la Video

str1 = 'Codec Profile ?';
str2 = 'Type de Codec ?';
str = {'Archival:  Motion JPEG 2000 file with lossless compression'; ...
        'Motion JPEG AVI: Compressed AVI file using Motion JPEG codec'; ...
        'Motion JPEG 2000: Compressed Motion JPEG 2000 file'; ...
        'MPEG-4: Compressed MPEG-4 file with H.264 encoding (Windows 7 systems only)'; ...
        'Uncompressed AVI: Uncompressed AVI file with RGB24 video'};
[pppp, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
[profileVideo, ~] = strtok(str{pppp}, ':');
