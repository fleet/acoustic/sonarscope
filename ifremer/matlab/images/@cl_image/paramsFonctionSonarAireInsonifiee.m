function [flag, indLayerInsonifiedArea, indLayerRange, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, ...
    indLayerTxBeamIndex, indLayerSlopeAcross, indLayerSlopeAlong] = paramsFonctionSonarAireInsonifiee(this, indImage, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 1); %#ok<ASGLU>

flag                = 1;
CasDepth            = [];
H                   = [];
indLayerBathy       = [];
CasDistance         = [];
indLayerSlopeAcross = [];
indLayerSlopeAlong  = [];
indLayerEmission    = [];

%% Recherche d'un layer de type  TxBeamIndex ou TxBeamIndexSwath

indLayerTxBeamIndex = findIndLayerSonar(this, indImage, 'strDataType', 'TxBeamIndex', 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerTxBeamIndex)   % Il n'existe pas de layer "TxBeamIndexSwath"
    indLayerTxBeamIndex = findIndLayerSonar(this, indImage, 'strDataType', 'TxBeamIndexSwath', 'WithCurrentImage', 1, 'OnlyOneLayer');
end

%% Recherche de l'information de distance parcourue

[~, indLayerRange] = findAnyLayerRange(this, indImage);
if isempty(indLayerRange)
    OnPeutAbreger = 0;
else
    OnPeutAbreger = 1;
end

%% Recherche si un layer de type "InsonifiedAreadB" existe et que c'est bien un layer calcul� en fonction des pentes

indLayerInsonifiedArea = findIndLayerSonar(this, indImage, 'strDataType', 'InsonifiedAreadB', 'WithCurrentImage', 1);
% SonarScope(this(indLayerInsonifiedArea))
for k=length(indLayerInsonifiedArea):-1:1
    % Test si c'est bien un layer calcul� en fonction des pentes
    if ~strcmp(this(indLayerInsonifiedArea(k)).Comments, '"Insonified Area" computed with slopes')
        indLayerInsonifiedArea(k) = [];
    end
end
if length(indLayerInsonifiedArea) >= 1 % Si par je ne sais quel hazard il se trouvait qu'on ait plusieurs layers du m�me type alors on prend le dernier
    indLayerInsonifiedArea = indLayerInsonifiedArea(1);
    return
end

% TODO : peut-�tre qu'on peut s'arr�ter l� si on a un layer d'aire
% insonifi�e qui convient

%% Recherche de l'information d'angle

% identEmission = cl_image.indDataType('TxAngle');
% indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
% if isempty(indLayerEmission)
%     identEmission = cl_image.indDataType('RxBeamAngle');
%     indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
%     if isempty(indLayerEmission) % TODO : ajout pas tr�s heureux; il faut g�n�rer un layer d'angle d'"incidence" plut�t que �a
%         identEmission = cl_image.indDataType('BeamPointingAngle');
%         indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
%     end
% end

indLayerEmission = findIndLayerSonar(this, indImage, 'strDataType', 'RxAngleEarth', 'WithCurrentImage', 1, ...
    'OnlyOneLayer', 'AtLeastOneLayer', 'Mute', Mute);
if isempty(indLayerEmission)
    flag = 0;
    return
end

%% Recherche si il y a des layers de SlopeAcross et SlopeAlong

indLayerSlopeAcross = findIndLayerSonar(this, indImage, 'strDataType', 'SlopeAcross', 'WithCurrentImage', 1, 'OnlyOneLayer');
indLayerSlopeAlong  = findIndLayerSonar(this, indImage, 'strDataType', 'SlopeAlong',  'WithCurrentImage', 1, 'OnlyOneLayer');

%%

if OnPeutAbreger
    %     return
end

if is_PingSamples(this(indImage))
    CasDistance = 1; % Cas d'une donn�e sonar en distance oblique
    return
else
    if is_PingAcrossDist(this(indImage))
        CasDistance = 2; % Cas d'une donn�e sonar en distance projet�e
    else
        if isempty(indLayerEmission)
            flag = 0;
            CasDistance = 4; % On ne poss�de pas d'information
        else
            CasDistance = 3; % On poss�de l'angle d'emission
        end
    end
end

%% Recherche de l'information de hauteur

[indLayerBathy, nomLayersBathy] = findIndLayerSonar(this, indImage, 'strDataType', 'Bathymetry', ...
    'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerBathy) % Il n'existe pas de layer "Depth"
    if isempty(indLayerEmission)
        if isempty(this(indImage).Sonar.Height)    % Il existe une hauteur sonar
            CasDepth = 2;
        else % On ne peut pas retrouver d'info de hauteur
            str1 = 'Il n''existe pas d''information de hauteur. Voulez-vous continuer le traitement en definissant une hauteur moyenne ?';
            str2 = 'There is no Height data, do you want to continue the processing using a mean height value ?';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            if rep == 2
                flag = 0;
                return
            end
            if rep == 1
                [flag, H] = inputOneParametre('Give the height', Lang('Hauteur','Height'), ...
                    'Value', 1, 'Unit', 'm', 'MinValue', 0);
                if ~flag
                    return
                end
                CasDepth = 4;
            else
                flag = 0;
                CasDepth = 5;
                return
            end
            
        end
    else
        CasDepth = 3; % On poss�de l'angle d'�mission
    end
else
    CasDepth = 1; % On poss�de le layer de hauteur
end

if length(indLayerBathy) > 1
    str1 = 'Layers � utiliser : ';
    str2 = 'Layers to use : ';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomLayersBathy);
    if ~flag
        return
    end
    indLayerBathy = indLayerBathy(choix);
end

