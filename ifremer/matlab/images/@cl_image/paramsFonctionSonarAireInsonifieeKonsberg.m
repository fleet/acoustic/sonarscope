function [flag, indLayerRange, indLayerTxBeamIndex, indLayerBeamPointingAngle] = paramsFonctionSonarAireInsonifieeKonsberg(this, indImage)

indLayerTxBeamIndex = [];
indLayerBeamPointingAngle = [];

[flag, indLayerRange] = findAnyLayerRange(this, indImage, 'WithCurrentImage', 1);
if ~flag
    return
end
if isempty(indLayerRange)
    str1 = 'L''image provient d''un sondeur Kongsberg, il faut un layer de "TwoWayTravelTimeInSeconds", ou "Range" ou ... pour d�corriger l''aire insonifi�e constructeur.';
    str2 = 'The image is coming from a Kongsberg sounder, a "TwoWayTravelTimeInSeconds", or "Range" or ..; is requested to remove the insonified area made by Kongsberg.';
    my_warndlg(Lang(str1,str2), 1);
end

identTxBeamIndex = cl_image.indDataType('TxBeamIndex');
indLayerTxBeamIndex = findIndLayerSonar(this, indImage, 'DataType', identTxBeamIndex, 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerTxBeamIndex)   % Il n'existe pas de layer "TxBeamIndexSwath"
    identTxBeamIndex = cl_image.indDataType('TxBeamIndexSwath');
    indLayerTxBeamIndex = findIndLayerSonar(this, indImage, 'DataType', identTxBeamIndex, 'WithCurrentImage', 1, 'OnlyOneLayer');
end

identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');
identTxAngle           = cl_image.indDataType('TxAngle'); % Rajout� le 15/02/2018 pour donn�es EM1002 de Marc Roche
indLayerBeamPointingAngle = findIndLayerSonar(this, indImage, 'DataType', [identBeamPointingAngle identTxAngle], 'WithCurrentImage', 1, 'OnlyOneLayer');

% TODO : pourquoi n'y a-t'il pas de test : if isempty(indLayerBeamPointingAngle) ???