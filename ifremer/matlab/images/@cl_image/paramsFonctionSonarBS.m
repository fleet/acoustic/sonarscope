function [flag, this, indLayerAngle, indLayerN, bins, MeanCompType] = paramsFonctionSonarBS(this, indImage, TakeTxAngle)

flag = 0;
indLayerAngle = [];
indLayerN     = [];
bins          = [];
MeanCompType  = 1;

identAveragedPtsNb     = cl_image.indDataType('AveragedPtsNb');

identEmissionAngle     = cl_image.indDataType('TxAngle');
identIncidenceAngle    = cl_image.indDataType('IncidenceAngle');
identRxBeamAngle       = cl_image.indDataType('RxBeamAngle');
identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle'); % Modif pour prise en cpte nvelle couche
identRxAngleEarth      = cl_image.indDataType('RxAngleEarth'); % Modif pour prise en cpte nvelle couche

indLayerAngleEmission     = findIndLayerSonar(this, indImage, 'DataType', identEmissionAngle,     'OnlyOneLayer');
indLayerRxBeamAngle       = findIndLayerSonar(this, indImage, 'DataType', identRxBeamAngle,       'OnlyOneLayer');
indLayerBeamPointingAngle = findIndLayerSonar(this, indImage, 'DataType', identBeamPointingAngle, 'OnlyOneLayer');
indLayerRxAngleEarth      = findIndLayerSonar(this, indImage, 'DataType', identRxAngleEarth,      'OnlyOneLayer');

if TakeTxAngle % On impose l'angle d'�mission
    if isempty(indLayerAngleEmission)
        if isempty(indLayerRxBeamAngle) && isempty(indLayerBeamPointingAngle) && isempty(indLayerRxAngleEarth) % Modif pour prise en compte nouvelle couche
            str = 'No "Angle" layer available."';
            my_warndlg(str, 1);
            flag = 0;
            return
        else
            if isempty(indLayerRxAngleEarth)
                if isempty(indLayerBeamPointingAngle)
                    indLayerAngleEmission = indLayerRxBeamAngle; % Modif pour prise en cpte nvelle couche
                else
                    indLayerAngleEmission = indLayerBeamPointingAngle;
                end
            else
                indLayerAngleEmission = indLayerRxAngleEarth;
            end
        end
    end
    if length(indLayerAngleEmission) == 1
        indLayerAngle = indLayerAngleEmission;
        flag = 1;
    end

else % On cherche d'abord l'angle d'incidence

    indLayerAngleIncidence = findIndLayerSonar(this, indImage, 'DataType', identIncidenceAngle, 'OnlyOneLayer');
    if isempty(indLayerAngleIncidence)
        indLayerAngleEmission = findIndLayerSonar(this, indImage, 'DataType', identEmissionAngle, 'OnlyOneLayer');
        if isempty(indLayerRxAngleEarth)
            if isempty(indLayerAngleEmission)
                indLayerBeamPointingAngle = findIndLayerSonar(this, indImage, 'DataType', identBeamPointingAngle, 'OnlyOneLayer');
                if isempty(indLayerBeamPointingAngle)
                    str = 'No "Angle" layer available. Create an "EmissionAngle" at first then eventualy an "IncidenceAngle"';
                    my_warndlg(str, 1);
                else
                    
                    str1 = 'Il n''existe pas de layer IncidenceAngle. Calcul du BS � partir de l''angle BeamPointingAngle et du roulis';
                    str2 = 'No IncidenceAngle found. BS computation using BeamPointingAngle and roll ?';
                    [rep, flag] = my_questdlg(Lang(str1,str2));
                    if ~flag
                        return
                    end
                    if rep == 2
                        flag = 0;
                        return
                    else
                        if length(indLayerBeamPointingAngle) == 1
                            indLayerAngle = indLayerBeamPointingAngle;
                            flag = 1;
                        end
                    end
                    
                    
                end
            else
                str1 = 'Il n''existe pas de layer IncidenceAngle. Calcul du BS � partir de l''angle d''�mission';
                str2 = 'No IncidenceAngle found. BS computation using the transmission angle ?';
                [rep, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
                if rep == 2
                    flag = 0;
                    return
                else
                    % OPERATION DE MAINTENANCE A FAIRE ICI
                    %     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
                    %     if ~flag
                    %         return
                    %     end
                    if length(indLayerAngleEmission) == 1
                        indLayerAngle = indLayerAngleEmission;
                        flag = 1;
                    end
                end
            end
        else
            str1 = 'Il n''existe pas de layer IncidenceAngle. Calcul du BS � partir de l''angle "RxAngleEarth".';
            str2 = 'No IncidenceAngle found. BS computation using "RxAngleEarth" angle.';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            if rep == 2
                flag = 0;
                return
            else
                % OPERATION DE MAINTENANCE A FAIRE ICI
                %     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
                %     if ~flag
                %         return
                %     end
                if length(indLayerRxAngleEarth) == 1
                    indLayerAngle = indLayerRxAngleEarth;
                    flag = 1;
                end
            end
        end
    else
        if length(indLayerAngleIncidence) == 1
            indLayerAngle = indLayerAngleIncidence;
            flag = 1;
        end
    end
end

if flag
    Titre      = this(indLayerAngle).Name;
    Unit       = this(indLayerAngle).Unit;
    StatValues = this(indLayerAngle).StatValues;
    binsAvant  = get(this(indLayerAngle), 'HistoCentralClasses');
    StatValues = StatValues(:); % Pour �viter cl_memmapfile
    binsAvant  = binsAvant(:);  % Pour �viter cl_memmapfile

    [flag, bins] = paramsSaisieBins(Titre, binsAvant, StatValues, Unit, isIntegerVal(this(indLayerAngle)));
    if ~flag
        return
    end
    if ~isequal(bins(:), binsAvant)
        this(indLayerAngle) = compute_stats(this(indLayerAngle), 'bins', bins);
    end

    indLayerN = findIndLayerSonar(this, indImage, 'DataType', identAveragedPtsNb, 'OnlyOneLayer');
    if isempty(indLayerN)
        GT = this(indImage).GeometryType;
        if GT == cl_image.indGeometryType('PingSamples')
        elseif GT == cl_image.indGeometryType('PingRange')
        else
%             str = 'No "AveragedPtsNb" available. Processing done considering 1 sample/pixel.  ';
%             my_warndlg(str, 1);
        end
    else
        disp('Message for JMA : indLayerN = []; paramsFonctionSonarBS indLayerN ompos� � [] pour l''instant car bug de subxN dans cl_image/courbesStats')
        indLayerN = []; % Impos� � [] pour l'instant car bug de subxN dans cl_image/courbesStats
        %{
        str1 = 'Prise en compte du layer AveragedPtsNb';
        str2 = 'Layer AveragedPtsNb considering';
        [rep, flag] = my_listdlg(Lang(str1,str2), ...
            {'Yes'; 'No'}, 'SelectionMode', 'single');
        if ~flag
            return
        end
        if rep == 2
            indLayerN = [];
        end
        %}
    end
end

if get_LevelQuestion >= 3
    [flag, MeanCompType] = question_MeanCompType;
    if ~flag
        return
    end
end
