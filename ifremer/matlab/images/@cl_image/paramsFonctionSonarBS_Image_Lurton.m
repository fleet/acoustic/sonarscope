function [flag, indLayerAngle] = paramsFonctionSonarBS_Image_Lurton(this, indImage, identCourbe, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

indLayerAngle = [];

if isempty(identCourbe) && ~isempty(this(indImage).Sonar.BS.BSLurtonParameters)
    for k=1:length(this(indImage).CourbesStatistiques)
        if strcmp(this(indImage).CourbesStatistiques(k).bilan{1}(1).Tag, 'BS')
            if isfield(this(indImage).CourbesStatistiques(k).bilan{1}(1), 'model')
                model = this(indImage).CourbesStatistiques(k).bilan{1}(1).model;
                valParams = model.getParamsValue;
                if isequal(single(valParams), single(this(indImage).Sonar.BS.BSLurtonParameters))
                    identCourbe = k;
                end
            end
        end
    end
    if isempty(identCourbe)
        flag = 0;
        return
    end
end

CourbesStatistiques = this(indImage).CourbesStatistiques(identCourbe);
if isempty(CourbesStatistiques)
    flag = 0;
    str = sprintf('No BS curve available in this image.');
    my_warndlg(str, 1);
    return
end

bilan = CourbesStatistiques.bilan{1};
if ischar(bilan.DataTypeConditions)
    DataTypeConditions = bilan.DataTypeConditions; % Cas pas normal : rencontr� sur donn�es obtenues � partir d'une restauration de session
else
    DataTypeConditions = bilan.DataTypeConditions{1};
end

identIncidenceAngle = cl_image.indDataType(DataTypeConditions);
indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identIncidenceAngle, 'OnlyOneLayer');

% Ajout JMA le 02/01/2019 pour .all V1 : ajout temporaire mais qui est en principe devenu inutile
if isempty(indLayerAngle) && strcmp(DataTypeConditions, 'TxAngle')
    identIncidenceAngle = cl_image.indDataType('BeamPointingAngle');
    indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identIncidenceAngle, 'OnlyOneLayer');
end

if isempty(indLayerAngle)
    if ~Mute
        str = sprintf('No layer of DataType = "%s" available', DataTypeConditions);
        my_warndlg(str, 1);
    end
    flag = 0;
else
    flag = 1;
end

%{
identIncidenceAngle    = cl_image.indDataType('IncidenceAngle');
indLayerAngleIncidence = findIndLayerSonar(this, indImage, 'DataType', identIncidenceAngle);

if isempty(indLayerAngleIncidence)
    identEmissionAngle  = cl_image.indDataType('TxAngle');
    [indLayerAngleEmission, nomLayersEmission] = findIndLayerSonar(this, indImage, 'DataType', identEmissionAngle);
    if isempty(indLayerAngleEmission)
        str = 'No "Angle" layer available. Create an "EmissionAngle" at first then eventualy an "IncidenceAngle"';
        my_warndlg(str, 1);
        flag = 0;
    else
        str = Lang('Il n''existe pas de layer IncidenceAngle. Calcul du BS a partir de l''angle d''emission',...
            'No IncidenceAngle found. BS computation since the transmission angle ?');
        [reponse, flag] = my_questdlg(str);

        if flag
            if reponse == 1
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this.Images, this.indImage, identBathymetry);
%     if ~flag
%         return
%     end
                if length(indLayerAngleEmission) == 1
                    indLayerAngleIncidence = indLayerAngleEmission;
                    flag = 1;
                else
                    str = 'Many "EmissionAngle" layers exist. Which one do you want to use ?';
                    [reponse, flag] = my_listdlg(str, nomLayersEmission, 'SelectionMode', 'Single');
                    if flag
                        indLayerAngleIncidence = indLayerAngleEmission(reponse);
                    end
                end
            end
        end
    end
else
    flag = 1;
end
indLayerAngle = indLayerAngleIncidence
%}
