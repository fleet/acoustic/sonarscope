function [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagRxConstructeur(this, indImage)

flag                  = 1;
indLayerBathy         = [];
H                     = [];
CasDepth              = [];

%% Recherche du layer : angle d'emission

identEmission = cl_image.indDataType('BeamPointingAngle');
[indLayerEmission, nomLayerEmission] = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerEmission)
    identEmission = cl_image.indDataType('TxAngle');
    [indLayerEmission, nomLayerEmission] = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
end
if isempty(indLayerEmission) && (this(indImage).GeometryType == cl_image.indGeometryType('PingBeam'))
    identEmission = cl_image.indDataType('RxBeamAngle');
    [indLayerEmission, nomLayerEmission] = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
    if isempty(indLayerEmission) % Modif pour prise en compte "BeamPointingAngle" � la place de "RxBeamAngle"
        identEmission = cl_image.indDataType('BeamPointingAngle');
        [indLayerEmission, nomLayerEmission] = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');        
    end
end

identEmissionBeam = cl_image.indDataType('TxBeamIndex');
[indLayerEmissionBeam, nomLayerEmissionBeam]  = findIndLayerSonar(this, indImage, 'DataType', identEmissionBeam , 'WithCurrentImage', 1, 'OnlyOneLayer');

if length(indLayerEmissionBeam) > 1
    str1 = 'Il existe plusieurs images de type "indLayerEmissionBeam", la quelle voulez-vous utiliser ?';
    str2 = 'Many "indLayerEmissionBeam" layers exist, which one do you want to use ?';
    [reponse, flag] = my_listdlg(Lang(str1,str2), nomLayerEmissionBeam, 'SelectionMode', 'Single');
    if flag
        indLayerEmissionBeam = indLayerEmissionBeam(reponse);
    end
end

identReceptionBeam = cl_image.indDataType('RxBeamIndex');
indLayerReceptionBeam  = findIndLayerSonar(this, indImage, 'DataType', identReceptionBeam , 'WithCurrentImage', 1, 'OnlyOneLayer');

%% Recherche de l'information de distance parcourue

if is_PingSamples(this(indImage))
    CasDistance = 1;    % Cas d'une donn�e sonar en distance oblique
    if isempty(indLayerEmission)
        drawnow
        str = ' Aucun layer de type "EmissionAngle" n''a �t� trouv�. Le traitement se fera en calculant l''angle d''�mission en tenant compte de l''angle de roulis. Il est peut-�tre plus prudent de contr�ler tout d''abord les angles de roulis et de cr�er ensuite le layer d''angle d''�mission.';
        ButtonName = questdlg(str, 'IFREMER - SonarScope', 'Continue', 'Cancel', 'Cancel');
        if strcmp(ButtonName, 'Cancel')
            flag = 0;
        end
    end
    return
else
    if is_PingAcrossDist(this(indImage))
        CasDistance = 2;    % Cas d'une donnee sonar en distance projetee
    else
        if isempty(indLayerEmission)
            my_warndlg('Please create a TxAngle image at first.', 1);
            flag = 0;
            CasDistance = 4;    % On ne possede pas d'information
        else
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this.Images, this.indImage, indImage, identBathymetry);
%     if ~flag
%         return
%     end
            if length(indLayerEmission) > 1
                str1 = 'Il existe plusieurs layers Angle, lequel voulez-vous utiliser ?';
                str2 = 'Many angles layers exist, which one do you want to use ?';
                [reponse, flag] = my_listdlg(Lang(str1,str2), nomLayerEmission, 'SelectionMode', 'Single');
                if flag
                    indLayerEmission = indLayerEmission(reponse);
                end
            end
            CasDistance = 3;    % On possede l'angle d'emission
        end
    end
end

%% Recherche de l'information de hauteur

identBathymetry = cl_image.indDataType('Bathymetry');
indLayerBathy = findIndLayerSonar(this, indImage, 'DataType', identBathymetry, 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerBathy)
    % Il n'existe pas de layer "Depth"
    Height = get(this(indImage), 'Height');
    if ~isempty(Height)
        % Il existe une hauteur sonar
        CasDepth = 2;
    else
        % Il n'existe pas de hauteur
        str1 = 'Il n''existe pas d''information de hauteur, voulez-vous continuer le traitement en definissant une hauteur moyenne ?';
        str2 = 'No height information available, do you want to continue using a constant heignt you will define yourself ? ?';
        % TODO : remplacer my_listdlg par my_questdlg
        [reponse, flag] = my_listdlg(Lang(str1,str2), {'Yes'; 'No'}, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        if reponse == 1
            [flag, H] = inputOneParametre('Give the height', Lang('Hauteur','Height'), ...
                'Value', 1, 'Unit', 'm', 'MinValue', 0);
            if ~flag
                return
            end
            CasDepth = 4;
        else
            flag = 0;
            CasDepth = 5;
            return
        end
    end
else
    % Il existe un layer 'Depth'
    CasDepth = 1;
end
