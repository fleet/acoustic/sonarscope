function [flag, indLayerTxAngle, indLayerRxBeamAngle] = paramsFonctionSonarDiagRxLayer(this, indImage)

indLayerRxBeamAngle = [];

%% Recherche des layers

identTxAngle  = cl_image.indDataType('TxAngle');
[flag, indLayerTxAngle] = findOneLayerDataType(this, indImage, identTxAngle);
if ~flag
    return
end

identRxBeamAngle  = cl_image.indDataType('RxBeamAngle');
[flag, indLayerRxBeamAngle] = findOneLayerDataType(this, indImage, identRxBeamAngle);
if ~flag
    return
end
 