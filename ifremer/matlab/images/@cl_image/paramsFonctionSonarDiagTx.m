function [flag, this, choixLayers, bins, Selection] = paramsFonctionSonarDiagTx(this, indImage)

flag = 0;
choixLayers = [];
bins        = [];
Selection   = [];

%% Recherche d'un layer d'angles

identTxAngle           = cl_image.indDataType('TxAngle');
identTxBeamIndex       = cl_image.indDataType('TxBeamIndex');
identTxBeamIndexSwath  = cl_image.indDataType('TxBeamIndexSwath');
identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');

indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identBeamPointingAngle, 'OnlyOneLayer');
if isempty(indLayerAngle)
    indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identTxAngle, 'OnlyOneLayer');
end
if isempty(indLayerAngle)
    str1 = 'Aucun layer d''angle n''a �t� trouv�. Cr�ez en un en premier';
    str2 = 'No "Angle" layer available. Create an "EmissionAngle" at first';
    my_warndlg(Lang(str1,str2), 1);
    return
end
choixLayers(1) = indLayerAngle;

Titre      = this(indLayerAngle).Name;
Unit       = this(indLayerAngle).Unit;
StatValues = this(indLayerAngle).StatValues;
binsAvant  = get(this(indLayerAngle), 'HistoCentralClasses');
StatValues = StatValues(:); % Pour �viter cl_memmapfile
binsAvant  = binsAvant(:); % Pour �viter cl_memmapfile

[flag, Bins] = paramsSaisieBins(Titre, binsAvant, StatValues, Unit, isIntegerVal(this(indLayerAngle)));
if ~flag
    return
end
bins{1} = Bins;
if ~isequal(bins{1}(:), binsAvant)
    this(indLayerAngle) = compute_stats(this(indLayerAngle), 'bins', bins{1});
end
Selection(1).Name     = Titre;
Selection(1).TypeVal  = 'Layer';
Selection(1).indLayer = indLayerAngle;

%% Recherche �ventuelle d'un layer TxBeamIndex

% if this(indImage).GeometryType == cl_image.indGeometryType('PingSamples')
%     Selection(2).Name     = Titre;
%     Selection(2).TypeVal  = 'Axe X';
% else

% TODO : id�alement il faudrait demander identTxBeamIndexSwath pour les
% sondeurs nouveaux et si le mode de fonctionnement de l'image �tudi�e
% permet le dual swath
[flag, indLayerEmissionBeam] = findOneLayerDataType(this, indImage, identTxBeamIndexSwath);
if ~flag
    [flag, indLayerEmissionBeam] = findOneLayerDataType(this, indImage, identTxBeamIndex);
end
if flag
    Titre      = this(indLayerEmissionBeam).Name;
    Unit       = this(indLayerEmissionBeam).Unit;
    StatValues = this(indLayerEmissionBeam).StatValues;
    binsAvant  = get(this(indLayerEmissionBeam), 'HistoCentralClasses');
    StatValues = StatValues(:); % Pour �viter cl_memmapfile
    binsAvant  = binsAvant(:);
    [flag, Bins] = paramsSaisieBins(Titre, binsAvant, StatValues, Unit, isIntegerVal(this(indLayerEmissionBeam)));
    if ~flag
        return
    end
    bins{2} = Bins;
    if ~isequal(bins{2}(:), binsAvant(:))
        this(indLayerEmissionBeam) = compute_stats(this(indLayerEmissionBeam), 'bins', bins{2});
    end
    
    Selection(2).Name     = Titre;
    Selection(2).TypeVal  = 'Layer';
    Selection(2).indLayer = indLayerEmissionBeam;
    choixLayers(2)        = indLayerEmissionBeam;
end
% end
