function [flag, indicesLayers, pasx, pasy] = paramsFonctionSonarDownsize(this, indImage)

indicesLayers = [];
pasx = [];
pasy = [];

p    = ClParametre('Name', Lang('Facteur de reduction en x', 'X reduction factor'), ...
    'Value', 2, 'MinValue', 1);
p(2) = ClParametre('Name',Lang('Facteur de reduction en y', 'Y reduction factor'), ...
    'Value', 1, 'MinValue', 1);
a = StyledSimpleParametreDialog('params', p, 'Title', 'Sonar downsize parametres');
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

pasx = val(1);
pasy = val(2);

[indicesLayers, flag] = selectionLayersSonarsSynchronises(this, indImage, 'Selection', 'Multiple');
