function [flag, alpha, indLayerBathymetry, indLayerTxAngle, alphaAvant] = paramsFonctionSonarTVGCompensationIfremer_LatLongOrGeoYX(this, indImage, varargin)

[varargin, SonarTVG_IfremerAlpha] = getPropertyValue(varargin, 'SonarTVG_IfremerAlpha', []); %#ok<ASGLU>

alpha              = [];
indLayerBathymetry = [];
indLayerTxAngle    = [];
alphaAvant         = [];

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    return
end

if (this(indImage).Sonar.TVG.etat ~= 1) || (this(indImage).Sonar.TVG.origine ~= 2)
    messageAboutCalibration('Tag', 'TVG')
    flag = 0;
    return
end

identBathymetry = cl_image.indDataType('Bathymetry');
indLayerBathymetry = findIndLayerSonar(this, indImage, 'DataType', identBathymetry, 'OnlyOneLayer');
if isempty(indLayerBathymetry)
    str1 = 'Il n''existe aucun layer de type "Bathymetry" synchronis� � cette image.';
    str2 = 'No "Bathymetry" layer synchronized to this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

identTxAngle = cl_image.indDataType('TxAngle');
indLayerTxAngle = findIndLayerSonar(this, indImage, 'DataType', identTxAngle, 'OnlyOneLayer');
if isempty(indLayerTxAngle)
    str1 = 'Il n''existe aucun layer de type "TxAngle" synchronis� � cette image.';
    str2 = 'No "TxAngle" layer synchronized to this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if isempty(SonarTVG_IfremerAlpha)
    alpha = get(this(indImage), 'SonarTVG_IfremerAlpha');
    alpha = mean(alpha);
    str1 = sprintf('Si cette image a �t� r�alis�e avant le 24/09/2007 le "Current Absorption coef" est certainement %f au lieu de %f.\nConservez cette fen�tre le temps de r�pondre � la suivante..', ...
        this(indImage).Sonar.TVG.ConstructAlpha, alpha);
    str2 = sprintf('If this image was made before 2007/09/24 the "Current Absorption coef" is certainly %f instead of %f.\nKeep this window open until next one is answered.', ...
        this(indImage).Sonar.TVG.ConstructAlpha, alpha);
    my_warndlg(Lang(str1,str2), 0);
    
    style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
    style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
    a.titleStyle = style1;
    a.paramStyle = style2;
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [-1 -2 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;
    alphaAvant = val(1);
    alpha      = val(2);
else
    alpha = SonarTVG_IfremerAlpha;
end
