function [flag, indLayerRange, AbsorptionCoeffRT, AbsorptionCoeffSSc, AbsorptionCoeffUser] = paramsFonctionSonarTVGCompensationSSc(this, indImage, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

flag = 0;
indLayerRange       = [];
AbsorptionCoeffRT   = [];
AbsorptionCoeffSSc  = [];
AbsorptionCoeffUser = [];

if (this(indImage).Sonar.TVG.etat == 1) && (this(indImage).Sonar.TVG.origine == 2) % TVG compens�e SSc
    if ~flag
        messageAboutCalibration('Tag', 'TVG', 'Mute', Mute)
    end
    return
end

if this(indImage).Sonar.TVG.etat == 1 % Compens�
    switch this(indImage).Sonar.TVG.origine
        case 1 % RT
            [flag, indLayerRange, AbsorptionCoeffRT] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_RT', 'Mute', Mute);
        case 2 % SSc
            % On ne passe pas par ici
        case 3 % User
            [flag, indLayerRange, AbsorptionCoeffUser] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_User');
    end
    if ~flag
        return
    end
end
[flag, indLayerRange, AbsorptionCoeffSSc] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_SSc', 'Mute', Mute);
