function [flag, cmd, indLayers, ListeSignauxVert] = paramsFormula(this, indImage)
 
indLayers = [];
ListeSignauxVert = [];

str1 = sprintf('\nCette operation permet de realiser une instruction matlab sur les differentes images presentes dans l''interface.');
str1 = sprintf('%s\nOn vous demande dans un premier temps d''ecrire l''instruction sous la forme I1*0.2 + I2/2 - ... ou I1, I2, I3, ... representent les images. ', str1);
str1 = sprintf('%s\nOn vous demandera dans un deuxieme temps de selectionner dans la liste des images celles que vous voulez affecter aux variables I1, I2, ...', str1);
str1 = sprintf('%s\nVous pouvez �galement utiliser des variables V1, V2, ... pour associer � des signaux verticaux Ex : I1-V1+45*cos(V2)', str1);

str2 = sprintf('\nThis functionnality provides user the ability to execute a Matlab statement with the various images loaded.');
str2 = sprintf('%s\nFirst, you must type in the statement, using Matlab formalism : I1*0,2+I2/2-..., where I1,I2,I3,... stands for images 1,2,3. ', str2);
str2 = sprintf('%s\nThen, you need to associate variables to loaded images, by selecting them in a list.', str2);
str2 = sprintf('%s\nOptionaly, you can use V1,V2, ... for vertical signals ( such as pitch, heave, vertical profile, etc...), giving : I1-V1+45*cos(V2)', str2);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'InfoFormula');

str1 = 'Ecrivez votre ligne de commande. Ex : 0.1 * I1 + 1.1 * I2 - 0.2 * I3 - cos(I4)';
str2 = 'Input your commnd line. Ex : 0.1 * I1 + 1.1 * I2 - 0.2 * I3 - cos(I4)';
cmd = inputdlg(Lang(str1,str2));
if isempty(cmd)
    flag = 0;
    return
end
cmd = cmd{1};

for k=1:10
    Var = sprintf('I%d', k);
    if contains(cmd,Var)
        str1 = sprintf('Image � utiliser pour %s ?', Var);
        str2 = sprintf('Image to use for %s ?', Var);
        [flag, Ident] = paramsFctsAritmImage(this, indImage, Lang(str1,str2), 'IncludeCurrentImage');
        if ~flag
            return
        end
        indLayers(k) = Ident; %#ok<AGROW>
    end
end

for k=1:10
    Var = sprintf('V%d', k);
    if contains(cmd,Var)
        str1 = sprintf('Signal vertical � utiliser pour %s ?', Var);
        str2 = sprintf('Vert. Signal to use for %s ?', Var);
        [flag, Ident] = paramsFctsAritmSignalVert(this(indImage), Lang(str1,str2));
        if ~flag
            return
        end
        ListeSignauxVert{k} = Ident; %#ok<AGROW>
    end
end
