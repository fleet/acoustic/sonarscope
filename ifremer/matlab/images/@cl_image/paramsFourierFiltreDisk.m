function [flag, val, InOut] = paramsFourierFiltreDisk(this)

p(1) = ClParametre('Name', Lang('Rayon','Radius'),         'Unit', this.XUnit, 'MinValue', 0, 'MaxValue', Inf, 'Value', 0);
p(2) = ClParametre('Name', Lang('Centre en x','X center'), 'Unit', this.XUnit, 'MinValue', 0, 'MaxValue', Inf, 'Value', 0);
p(3) = ClParametre('Name', Lang('Centre en y','Y center'), 'Unit', this.YUnit, 'MinValue', 0, 'MaxValue', Inf, 'Value', 0);
a = StyledSimpleParametreDialog('params', p, 'Title', 'Disk Fourier filtering parametres');
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    val = [];
    return
end
val = a.getParamsValue;

str1 = 'Partie � conserver ?';
str2 = 'Part to conserve ?';
[InOut, flag] = my_listdlg(Lang(str1,str2), {Lang('Interieur','Interior'); Lang('Exterieur','Exterior')}, 'SelectionMode', 'Single');
