function [flag, seuil] = paramsFourierFiltreSeuil(this)

p = ClParametre('Name', Lang('Seuil', 'Threshold'), 'Unit', this.Unit,  'MinValue', -Inf, 'MaxValue', Inf, 'Value', 0);
a = StyledSimpleParametreDialog('params', p, 'Title', 'Threshold Fourier filtering parametres');
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    seuil = [];
    return
end
seuil = a.getParamsValue;
