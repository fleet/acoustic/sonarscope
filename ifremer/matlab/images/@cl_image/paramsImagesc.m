function [flag, Rot90, NomY, TypeWindow] = paramsImagesc(this)

Rot90      = [];
NomY       = [];
TypeWindow = [];

if testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage') ...
        && (get_LevelQuestion >= 3) ...
        && ~isempty(this.Sonar.FishLatitude) ...
        && ~any(isnan(this.Sonar.FishLatitude(:,1))) % TODO : � am�liorer
    str1 = 'S�lection des abscisses';
    str2 = 'Selection of the abscissa';
    str{1} = 'X';
    str{2} = 'Distance along navigation';
    [repNomY, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    
    switch repNomY
        case 1
            NomY = 'y';
        case 2
            NomY = 'DistanceAlongNavigation';
    end
else
    NomY = 'y';
end

[Rot90, flag] = my_questdlg('Rotation 90�', 'Init', 2);
if ~flag
    return
end

if ~isdeployed && (get_LevelQuestion >= 3) && strcmp(NomY, 'y')
    str1 = 'Type de fen�tre';
    str2 = 'Type of window';
    str{1} = Lang('Classique','Classical');
    str{2} = Lang('Ascenseurs','With sliders');
    [TypeWindow, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'ColorLevel', 3);
    if ~flag
        return
    end
end

%{
Comment� car 
Warning: IMSCROLLPANEL currently requires default XData and YData.
HIM has non-default XData, resetting to [1 5097].
HIM has non-default YData, resetting to [1 1260]. 
> In imscrollpanel>validateXYData at 1408
  In imscrollpanel at 236
  In imagesc_scroll at 22
  In cl_image.imagesc at 260
  In @clc_image\private\callback_export at 346
  In @clc_image\private\callback_KeyPress at 270
  In clc_image.callback at 261
  In cli_image.callback at 63 

Autre pb, �a cr�e une autre fen�tre, on a plus nos menus : c'est nul ce
truc et c'est peut-�tre pas compilable
%}