function [flag, indEmissionAngle, indAcrossDistance, indRange, resolutionX, indLayers, subEmissionAngle, MasquageDual] = ...
    paramsMBESImportLayer2PingAcrossDist(this, indImage, suby)

indAcrossDistance = [];
indRange          = [];
indLayers         = [];
subEmissionAngle  = [];
MasquageDual      = 0;

flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'));
if ~flag
    return
end

resolutionX = get(this(indImage), 'SonarResolutionX');

%% Recherche du layer ReceptionBeam syncrhonise en Y

identEmissionAngle = cl_image.indDataType('TxAngle');
[indEmissionAngle, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
    'DataType', identEmissionAngle, 'GeometryType', cl_image.indGeometryType('PingBeam')); %#ok<ASGLU>
if isempty(indEmissionAngle)
    identRxBeamAngle = cl_image.indDataType('RxBeamAngle');
    [indEmissionAngle, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
        'DataType', identRxBeamAngle, 'GeometryType', cl_image.indGeometryType('PingBeam'));
    if isempty(indEmissionAngle)
        str = sprintf('No layer %d (%s) synchronised with this image.', identEmissionAngle, cl_image.strDataType{identEmissionAngle});
        my_warndlg(str, 1);
        flag = 0;
        return
    elseif length(indEmissionAngle) > 1
        [choix, flag] = my_listdlg(Lang('Quelle image voulez-vous utiliser ?', 'What layer do you want to use ?'), nomsLayers);
        if ~flag
            return
        end
        indEmissionAngle = indEmissionAngle(choix);
    end
end

%% On recherche le layer "AcrossDistance" synchronise en Y

identAcrossDistance = cl_image.indDataType('AcrossDist');
[indAcrossDistance, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
    'DataType', identAcrossDistance, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if isempty(indAcrossDistance)
    str = sprintf('No layer %d (%s) synchronised with this image.', identAcrossDistance, cl_image.strDataType{identAcrossDistance});
    my_warndlg(str, 1);
    flag = 0;
    return
elseif length(indAcrossDistance) > 1
    str1 = 'Quelle image voulez-vous utiliser ?';
    str2 = 'What layer do you want to use ?';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers);
    if ~flag
        return
    end
    indAcrossDistance = indAcrossDistance(choix);
end

%% On recherche le layer "Range" synchronise en Y

% TODO : appeler findAnyLayerRange en compl�tant la fonction pour pouvoir
% passer. S'assurer que �a suit dans la fonction de calcul

if this(indImage).GeometryType == cl_image.indGeometryType('PingSamples')
    identRange = cl_image.indDataType('RayPathSampleNb');
    [indRange, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
        'DataType', identRange, 'GeometryType', cl_image.indGeometryType('PingBeam')); %#ok<ASGLU>
    if isempty(indRange)
        identRange = cl_image.indDataType('TwoWayTravelTimeInSeconds');
        [indRange, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
            'DataType', identRange, 'GeometryType', cl_image.indGeometryType('PingBeam'));
        if isempty(indRange)
            str = sprintf('No layer %d (%s) synchronised with this image.', identRange, cl_image.strDataType{identRange});
            my_warndlg(str, 1);
            flag = 0;
            return
        elseif length(indRange) > 1
            str1 = 'Quelle image voulez-vous utiliser ?';
            str2 = 'What layer do you want to use ?';
            [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers);
            if ~flag
                return
            end
            indRange = indRange(choix);
        end
    end
end

%% Recherche des layers shyncronises en Y et de GeometryType=cl_image.indGeometryType('PingBeam')='BathyFais'  

[indLayers, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if isempty(indLayers)
    str = sprintf('No layer synchronised with this image.');
    my_warndlg(str, 1);
    flag = 0;
    return
elseif length(indLayers) > 1
    [choix, flag] = my_listdlg(Lang('Quelle image voulez-vous utiliser ?', 'What layer do you want to use ?'), nomsLayers);
    if ~flag
        return
    end
    indLayers = indLayers(choix);
end

%%

[flag, resolutionX] = inputOneParametre('Layer importation parametres', 'Resolution', ...
    'Value', resolutionX, 'Unit', 'm', 'MinValue', 0);
if ~flag
    return
end

PingCounter              = get(this(indImage),    'PingCounter');
EmissionAnglePingCounter = get(this(indEmissionAngle), 'PingCounter');
        % TODO : ATTENTION danger, il faut certainement faire
        % PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
[sub, subEmissionAngle] = intersect3(PingCounter(suby), EmissionAnglePingCounter, EmissionAnglePingCounter); %#ok<ASGLU>

%% Masquage des faisceaux en recouvrement

[rep, flag] = my_questdlg(Lang('Masquage de beams redondants si sondeur dual?', ...
    'Mask overlapping beams if dual sounder ?'));
if ~flag
    return
end
MasquageDual = (rep == 1);
