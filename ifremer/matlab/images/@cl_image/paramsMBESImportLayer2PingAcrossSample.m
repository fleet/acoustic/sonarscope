function [flag, indRxBeamIndexPingAcrossSample, AcrossDistPingBeam, indLayersPingBeam] = ...
    paramsMBESImportLayer2PingAcrossSample(this, indImage)

AcrossDistPingBeam              = [];
indLayersPingBeam               = [];
indRxBeamIndexPingAcrossSample  = [];
% MasquageDual                  = 0;

flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('PingAcrossSample'));
if ~flag
    return
end

%% Recherche du layer RxBeamIndex en PingAcrossSample syncrhonise en Y

identRxBeamIndex = cl_image.indDataType('RxBeamIndex');
indRxBeamIndexPingAcrossSample = findIndLayerSonar(this, indImage, 'DataType', identRxBeamIndex, 'WithCurrentImage', 1, 'OnlyOneLayer');

%% Recherche du layer RxBeamIndex en TwoWayTravelTimeInSamples syncrhonise en Y

identAcrossDistPingBeam = cl_image.indDataType('AcrossDist');
[AcrossDistPingBeam, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
    'DataType', identAcrossDistPingBeam, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'WithCurrentImage', 1); %#ok<ASGLU>

%% On recherche le layer "Range" synchronise en Y

% TODO : appeler findAnyLayerRange en compl�tant la fonction pour pouvoir
% passer 'WithCurrentImage', 1
% ATTENTION : indRangePingBeam ne semble pas �tre utilis�

identRange = cl_image.indDataType('TwoWayTravelTimeInSeconds'); % Test� sur format V1
[indRangePingBeam, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
    'DataType', identRange, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'WithCurrentImage', 1); %#ok<ASGLU>
if isempty(indRangePingBeam)
    identRange = cl_image.indDataType('RayPathSampleNb');
    [indRangePingBeam, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, ...
        'DataType', identRange, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'WithCurrentImage', 1); %#ok<ASGLU>
    if isempty(indRangePingBeam)
    %     indRangePingBeam = findIndLayerSonar(this, indRangePingBeam, 'DataType', identRange, 'WithCurrentImage', 1, 'OnlyOneLayer');
        return
    end
end

%% Recherche des layers shyncronises en Y et de GeometryType=cl_image.indGeometryType('PingBeam')='BathyFais'  

[indLayersPingBeam, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if isempty(indLayersPingBeam)
    str1 = sprintf('Aucub layer n''est synchronis� � cette image.');
    str2 = sprintf('No layer synchronised with this image.');
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
elseif length(indLayersPingBeam) > 1
    str1 = 'Quelle image voulez-vous importer ?';
    str2 = 'What layer do you want to import ?';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers);
    if ~flag
        return
    end
    indLayersPingBeam = indLayersPingBeam(choix);
end
