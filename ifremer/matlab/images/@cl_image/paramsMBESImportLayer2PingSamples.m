function [flag, indReceptionBeam, indLayerRange, indLayers] = paramsMBESImportLayer2PingSamples(this, indImage)

flag             = 0;
indLayers        = [];
indLayerRange    = [];
nomsLayers       = [];
nomsLayersRange  = [];

%% On verifie si le layer est bien de type "ReceptionBeam"

identReceptionBeam = cl_image.indDataType('RxBeamIndex');
% flag = testSignature(this(indImage), 'DataType', identReceptionBeam, ...
%             'Message', 'Pour realiser cette operation, il faut etre positionne sur un Layer de DataType="ReceptionBeam"'); 
% if ~flag
%     return
% end

[indReceptionBeam, nomsLayersReceptionBeam] = findIndLayerSonar(this, indImage, 'DataType', identReceptionBeam, 'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer'); %#ok<ASGLU>
if isempty(indReceptionBeam)
    flag = 0;
    return
end

%% Recherche des layers shyncronises en Y et de GeometryType=cl_image.indGeometryType('PingBeam')='BathyFais'  

identRayPathSampleNb = cl_image.indDataType('RayPathSampleNb');
identRayPathLength   = cl_image.indDataType('RayPathLength'); % One way length in meters
identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');

TagSynchroX = this(indImage).TagSynchroX;
TagSynchroY = this(indImage).TagSynchroY;
for k=1:length(this)
    TagX = this(k).TagSynchroX;
    TagY = this(k).TagSynchroY;
    if strcmp(TagY , TagSynchroY) && ~strcmp(TagX, TagSynchroX)
        DataType = this(k).DataType;
        if (DataType == identRayPathSampleNb) || (DataType == identRayPathLength) || (DataType == identTwoWayTravelTimeInSeconds)
            indLayerRange(end+1)   = k; %#ok<AGROW>
            nomsLayersRange{end+1} = this(k).Name; %#ok<AGROW>
        else
            indLayers(end+1)  = k;  %#ok<AGROW>
            nomsLayers{end+1} = this(k).Name; %#ok<AGROW>
        end
    end
end

if isempty(indLayerRange)
    my_warndlg(Lang('Il faut un layer "RayPathSampleNb" ou "RayPathLength"', 'TODO'), 1);
    return
elseif length(indLayerRange) > 1
    [choix, flag] = my_listdlg(Lang('Il existe plusieurs layers de type "RayPathXxxx" lequel dois-je utiliser pour pouvoir faire l''importation ?', ...
        'Several "RayPathXxxx" layers exist, which one will we use ?'), nomsLayersRange);
    if ~flag
        return
    end
    indLayerRange = indLayerRange(choix);
end

if isempty(indLayers)
    return
elseif length(indLayers) == 1
    flag = 1;
else
    [choix, flag] = my_listdlg(Lang('Layers � importer ?', 'Layers to import ?'),  nomsLayers);
    if ~flag
        return
    end
    indLayers = indLayers(choix);
end
