function [flag, indTxAngle, AngleMoustache] = paramsMBESMoustacheRemoval(this, indImage)

indTxAngle     = [];
AngleMoustache = [];

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(this(indImage), 'DataType', identBathymetry, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Recherche du layer TxAngle syncrhonise en X et Y

identTxAngle = [cl_image.indDataType('TxAngle') cl_image.indDataType('BeamPointingAngle')];
indTxAngle = findIndLayerSonar(this, indImage, 'DataType', identTxAngle, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indTxAngle)
    flag = 0;
    return
end

%% Angle moustache

[flag, AngleMoustache] = inputOneParametre(Lang('Angle max', 'Max Angle'), Lang('Angle max', 'Max Angle'), ...
    'Value', 15, 'Unit', 'deg', 'MinValue', 0, 'MaxValue', 25);
if ~flag
    return
end
