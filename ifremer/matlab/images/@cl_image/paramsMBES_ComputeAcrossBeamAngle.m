function [flag, indAcrossDistance] = paramsMBES_ComputeAcrossBeamAngle(this, indImage)

indAcrossDistance = [];

ident1 = cl_image.indDataType('Bathymetry');
flag = testSignature(this(indImage), 'DataType', ident1, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% On recherche le layer "AcrossDistance" synchronise en Y

identAcrossDistance = cl_image.indDataType('AcrossDist');
[indAcrossDistance, nomsLayers] = findIndLayerSonar(this, indImage, 'DataType', identAcrossDistance, ...
    'OnlyOneLayer');

switch length(indAcrossDistance)
    case 0
        indAcrossDistance = [];
        flag = 0;
        return
    case 1
    otherwise
        str1 = 'Confirmation image conditionnelle';
        str2 = 'Conditional image agreement';
        [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indAcrossDistance  = indAcrossDistance(choix);
end
