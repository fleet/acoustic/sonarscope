function [flag, MaxImageSize, DataSSP, subLayers, MaskBeyondDetection, typeAlgoWC] = paramsMBES_SampleBeam2DepthAcrossDist(this)
        
DataSSP             = [];
subLayers           = [];
MaxImageSize        = [];
MaskBeyondDetection = 0;
typeAlgoWC          = [];

%% On v�rifie si le layer est bien de type "RxBeamAngle"

identReflec = cl_image.indDataType('Reflectivity');
identPhase  = cl_image.indDataType('TxAngle');
flag = testSignature(this, 'DataType', [identReflec identPhase], ...
    'GeometryType', cl_image.indGeometryType('SampleBeam'), ...
    'Message', 'Pour realiser cette operation, il faut etre positionne sur un Layer de DataType="SampleBeam"');
if ~flag
    return
end

nomFic = this.InitialFileName;
[~, ~, ext] = fileparts(nomFic);

if strcmpi(ext, '.all')
	[~, DataSSP] = SSc_ReadDatagrams(nomFic, 'Ssc_SoundSpeedProfile'); % Non test� ici
    
elseif strcmpi(ext, '.s7k')
    s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
    identSondeur = selectionSondeur(s7k);
    DataSSP = read_soundSpeedProfile(s7k, identSondeur);
end

%% Autres layers ?

str1 = 'Autres Images ?';
str2 = 'Other Images ?';
strChoix1 = {'Angle / Verticale ..................................(TxAngle)'
    'Distance oblique ................................. (RayPathRange)'
    'Num�ro de faisceau ........................... (RxBeamIndex)'
    'Nombre d''�chantillons .........................(RayPathSampleNb)'
    'Diagramme de directivit� en r�ception (RxBeamPattern)'};
strChoix2 = {'Angle / Nadir .................(TxAngle)'
    'Oblique Distance ......... (RayPathRange)'
    'Beam number .............. (RxBeamIndex)'
    'Number of samples ......(RayPathSampleNb)'
    'Reception diagramme ...(RxBeamPattern)'};
% [subLayers, flag] = my_listdlgMultiple(Lang(str1,str2), Lang(strChoix1,strChoix2), 'InitialValue', []);
[subLayers, flag] = my_listdlgMultiple(Lang(str1,str2), Lang(strChoix1(1:4),strChoix2(1:4)), 'InitialValue', []);
if ~flag
    return
end

%% Nombre max de pixels dans l'image

str1 = 'Nombre maximum de pixels : sqrt(H*L))';
str2 = 'Maximum number of pixels : sqrt(H*W)';
p = ClParametre('Name', Lang('Value', 'Value'), ...
    'Unit', 'pixels',  'MinValue', 500, 'MaxValue', 3000, 'Value', 2000, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
MaxImageSize = a.getParamsValue;

%% Suppression des donn�es en dessous de la d�tection

str1 = 'Masquage de l''image en dessous de la d�tection ?';
str2 = 'Mask image under detection ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
MaskBeyondDetection = (rep == 1);

%% Type d'algorithme

if isempty(DataSSP)
    typeAlgoWC = 1;
    str1 = 'Aucun profil de c�l�rit� n''a �t� trouv�. Il n''est pas possible de choisir la m�thode de ray tracing. L''algorithme bas� sur "Depth and AcrossDist" sera utilis�.';
    str2 = 'No Spound Speed Profile could be found in the file. It is not possible to choice the ray tracing method. The algorithm based on "Depth and AcrossDist" will be used.';
    my_warndlg(Lang(str1,str2), 1)
else
    [flag, typeAlgoWC] = question_WCRaytracingAlgorithm('InitialValue', 2);
    if ~flag
        return
    end
end
