function [flag, Frequence, Month, Position, Hauteur, Angle] = paramsMapPropagationAcoustique(this)

persistent persistent_Frequence persistent_Position persistent_Hauteur persistent_Month persistent_Angle

%% R�cup�ration des valeurs par d�faut

if isempty(this.Sonar.Desciption)
    if isempty(persistent_Frequence)
        Frequence = 0;
    else
        Frequence = persistent_Frequence;
    end
    
    if isempty(persistent_Position)
        Position = 1;
    else
        Position = persistent_Position;
    end
    
else
    Frequence = get(this.Sonar.Desciption, 'Sonar.Freq');
    SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
    switch SonarName
        case {'Sar'; 'DF1000'; 'DTS1'}
            Position = 2; % 'Seabed';
        case {'EM2040'; 'Reson7125'; 'GeoSwath'; 'Subbottom'}
            Position = 1; % 'Indetermine';
        otherwise
            Position = 1; % 'Surface';
    end
end

if isempty(persistent_Hauteur)
    Hauteur = 100;
else
    Hauteur = persistent_Hauteur;
end

if isempty(persistent_Month)
    Month = 1;
else
    Month = persistent_Month;
end

if isempty(persistent_Angle)
    Angle = 70;
else
    Angle = persistent_Angle;
end

%% Fr�quence

str1 = 'Fr�quence du sonar';
str2 = 'Frequency of the sonar';
p = ClParametre('Name', Lang('Fr�quence', 'Frequency'), ...
    'Unit', 'kHz',  'MinValue', 0, 'MaxValue', 500, 'Value', Frequence);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 0 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Frequence = a.getParamsValue;
persistent_Frequence = Frequence;

%% Position

str1 = 'Position du sonar.';
str2 = 'Sonar Position.';
str{1} = Lang('En Surface', 'Sea surface');
str{2} = Lang('Au dessus de fond', 'Over seabed');
[Position, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', Position, 'SelectionMode', 'Single');
if ~flag
    return
end
persistent_Position = Position;

%% Hauteur par rapport au fond

if Position == 2
    str1 = 'Altitude au dessus du fond';
    str2 = 'Height over seabed';
    p = ClParametre('Name', Lang('Hauteur', 'Height'), ...
        'Unit', 'm',  'MinValue', 0, 'MaxValue', 1000, 'Value', Hauteur);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 0 0 0 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    Hauteur = a.getParamsValue;
    persistent_Hauteur = Hauteur;
end

%% Mois

strMonth = {'January'; 'February'; 'March'; 'April'; 'May'; 'Juin'; 'July'; 'August'; 'Sptember'; 'October'; 'November'; 'December'};
[indTemps, flag] = my_listdlg(Lang('Mois', 'Month'), strMonth);
if ~flag
    return
end
Month = strMonth{indTemps};
persistent_Month = Month;

%% Angle de d�part d'un rayon

str1 = 'Angle d''un rayon acoustique';
str2 = 'Angle of one particular beam';
p = ClParametre('Name', Lang('Angle', 'Angle'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 85, 'Value', Angle);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 0 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Angle = a.getParamsValue;
persistent_Angle = Angle;

