function [flag, creationLayer, nomLayer, indLayerMask] = paramsMaskInteractive(this, indImage)

%% Identification du masque

[flag, creationLayer, nomLayer, indLayerMask] = question_Mask(this, indImage);
if ~flag
    return
end

%% Message � l'op�rateur

str1 = 'Pendant le travail de d�limitation ne jouez pas avec le zoom et les boutons de d�calage, SonarScope deviendrait instable, d�sol� !';
str2 = 'During the interactive delineation, DO NOT play with zoom or shift buttons, SonarScope would become unstable, sorry for that.';
my_txtdlg(Lang('ATTENTION', 'WARNING'), Lang(str1,str2), 'Entete', 'SonarScope - IFREMER', 'windowstyle', 'modal');
