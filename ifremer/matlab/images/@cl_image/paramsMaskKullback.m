% TODO : fonction modernis�e sans v�rification. L'ancienne fonction
% paramsMaqueKullback a �t� copi�e dans D:\IfremerToolboxFonctionsObsoletes\ifremer\matlab\images\clc_image\private

function [flag, indLayerMask, valMask] = paramsMaskKullback(this, indImage, varargin)

[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 'Last'); %#ok<ASGLU>

indLayerMask = [];
valMask      = 1;
identMasque  = cl_image.indDataType('Mask');

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'TypeDonneUniquement', identMasque);
if ~flag
    return
end

if isempty(indLayers) % Pas de layer "Masque" trouve
    indLayerMask = [];
    valMask = 1;
    return
end

str1 = 'Choix d''un masque � appliquer sur l''image';
str2 = 'Select a mask to apply on the image';
[choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single');
if ~flag
    return
end
indLayerMask = indLayers(choix);

StatValues = this(indLayerMask).StatValues;

if StatValues.Min == StatValues.Max
    valMask = StatValues.Min;
else
    ValNaN = get(this(indLayerMask), 'ValNaN');
    I = get(this(indLayerMask), 'Image');
    
    if isnan(ValNaN)
        I = I(~isnan(I));
    else
        I = I(I ~= ValNaN);
    end
    
    listeVal = unique(I);
    
    for i=1:length(listeVal)
        str{i} = num2str(listeVal(i)); %#ok
    end
    
    if strcmp(InitialValue, 'All')
        InitialValue = 1:length(str);
    elseif strcmp(InitialValue, 'Segmentation')
        InitialValue = find(listeVal ~= 0);
    else % 'Last
        InitialValue = length(str);
    end
    [choix, flag] = my_listdlg(Lang('Valeurs du masque � utiliser','Mask values to use'), str, ...
        'InitialValue', InitialValue);
    if flag
        valMask = listeVal(choix);
    else
        valMask = 1;
        return
    end
end

