function [flag, indLayerMask, valMask, zone] = paramsMasquage(this, indImage, varargin)

[varargin, DataType]      = getPropertyValue(varargin, 'DataType', []);
[varargin, AtLeastOne]    = getPropertyValue(varargin, 'AtLeastOne', 0);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple'); %#ok<ASGLU>

zone         = 1;
indLayerMask = [];
valMask      = [];

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

if isempty(indLayers) % Pas de layer "Masque" trouve
    flag = 0;
    return
end

if isempty(DataType)
    identMasque       = cl_image.indDataType('Mask');
    identSegmentation = cl_image.indDataType('Segmentation');
    identResonDetectionBrightness  = cl_image.indDataType('ResonDetectionBrightness');
    identResonDetectionColinearity = cl_image.indDataType('ResonDetectionColinearity');
    %     sub = find((DataTypes == identMasque) | ...
    %         (DataTypes == identResonDetectionBrightness) | ...
    %         (DataTypes == identResonDetectionColinearity) | ...
    %         (DataTypes == identSegmentation) | ...
    %         (DataTypes == 1)); % ajout image de type "unknown"
    sub = find((DataTypes == identMasque) | ...
        (DataTypes == identResonDetectionBrightness) | ...
        (DataTypes == identResonDetectionColinearity) | ...
        (DataTypes == identSegmentation)); % Suppression du type "unknown" par JMA le 08/03/2015
    isSegmentation = 0;
else
    identMasque = cl_image.indDataType(DataType);
    sub = find(DataTypes == identMasque);
    isSegmentation = strcmp(DataType, 'Segmentation');
end

if isempty(sub)
    indLayerMask = [];
    valMask = 1;
    if AtLeastOne
        str1 = 'Il n''y a pas de masque synchronis� � cette image.';
        str2 = 'There is no mask synchronized to this image.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
    else
        flag = 1;
    end
    return
end

indLayers  = indLayers(sub);
nomsLayers = nomsLayers(sub);

if strcmpi(SelectionMode, 'multiple')
    InitialValue = [];
else
    InitialValue = 1;
end

str1 = 'Choix d''un masque � appliquer sur l''image';
str2 = 'Mask choice to apply on the image';
[choix, flag] = my_listdlgMultiple(Lang(str1,str2), nomsLayers, ...
    'SelectionMode', SelectionMode, 'InitialValue', InitialValue);
if ~flag
    indLayerMask = [];
    valMask = 1;
    return
end
indLayerMask = indLayers(choix);

if ~isempty(indLayerMask)
    StatValues = this(indLayerMask(1)).StatValues;
    
    if (StatValues.Min == StatValues.Max) && (StatValues.Min == 1)
        StatValues.Min = 0;
    elseif StatValues.Min == StatValues.Max
        valMask = StatValues.Min;
        return
    end
    
    ValNaN = get(this(indLayerMask(1)), 'ValNaN');
    I = get(this(indLayerMask(1)), 'Image');
    
    %     listeVal = unique(I); % Pb Memory
    listeVal = [];
    for k=1:size(I,1)
        L = I(k,:);
        if isnan(ValNaN)
            L = unique(L(~isnan(L)));
        else
            L = unique(L(L ~= ValNaN));
        end
        listeVal = unique([listeVal L]);
    end
    
    %     listeVal(1) = StatValues.Min; % Comment� le 02/03/2012 par JMA �
    %     Wellington pour le CIDCO
    if ValNaN == 0
        listeVal = [0; listeVal(:)];
    end
    
    for k=(StatValues.Min+1):(StatValues.Max-1)
        if any(I == k)
            listeVal(end+1) = k; %#ok<AGROW>
        end
    end
    listeVal(end+1) = StatValues.Max;
    listeVal = unique(listeVal);
    
    for k=1:length(listeVal)
        str{k} = num2str(listeVal(k)); %#ok<AGROW>
    end
    
    if isSegmentation
        valMask = 1:length(str);
        zone = 1;
    else
        str1 = 'Valeurs du masque � utiliser';
        str2 = 'Mask values to use';
        [choix, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', length(str));
        %             'InitialValue', length(str), 'NoSelectAll', 1); % Comment� le
        %             02/03/2012 � Wellington
        if flag
            valMask = listeVal(choix);
        else
            valMask = 1;
            return
        end
        str1 = 'Quelle partie voulez-vous conserver ?';
        str2 = 'What part do you want to keep ?';
        [zone, flag] = my_questdlg(Lang(str1,str2), ...
            Lang('Int�rieur','Inside'), Lang('Ext�rieur','Outside'));
    end
end
