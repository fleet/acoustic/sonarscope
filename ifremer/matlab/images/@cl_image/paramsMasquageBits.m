function [flag, indLayers, Bits] = paramsMasquageBits(this, indImage)
 
Bits = [];
identMasque = cl_image.indDataType('Mask');
[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, ...
    'TypeDonneUniquement', identMasque, 'OnlyOneLayer', 1); %#ok<ASGLU>
if ~flag
    return
end
if isempty(indLayers) % Pas de layer "Masque" trouve
    flag = 0;
    return
end

strMaskBits = this(indLayers).strMaskBits;
if isempty(strMaskBits)
    strMaskBits = {'Bit 1 : unit'
        'Bit 2 : multiple of 2'
        'Bit 3 : multiple of 4'
        'Bit 4 : multiple of 8'
        'Bit 5 : multiple of 16'
        'Bit 6 : multiple of 32'
        'Bit 7 : multiple of 64'
        'Bit 8 : multiple of 128'};    
end

str1 = 'S�lection des bits';
str2 = 'Select bits';
[Bits, flag] = my_listdlgMultiple(Lang(str1,str2), strMaskBits, 'InitialValue', 1:length(strMaskBits));
if ~flag
    return
end
