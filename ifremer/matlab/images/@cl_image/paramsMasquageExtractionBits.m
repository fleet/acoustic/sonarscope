function [flag, Bits] = paramsMasquageExtractionBits(this)
 
Bits = [];

identMask = cl_image.indDataType('Mask');
flag = testSignature(this, 'DataType', identMask);
if  ~flag
    return
end

[flag, Bits] = masque_selectBits(this);
