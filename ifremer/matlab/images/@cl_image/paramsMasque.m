function [flag, indLayerMask, valMask, Coul] = paramsMasque(this, indImage, varargin)

[varargin, InitialValue] = getPropertyValue(varargin, 'InitialValue', 'Last'); %#ok<ASGLU>

indLayerMask = [];
valMask      = 1;
Coul         = [];

identMasque = cl_image.indDataType('Mask');
[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'TypeDonneUniquement', identMasque);
if ~flag
    return
end

identSegmentation = cl_image.indDataType('Segmentation');
[flag, indLayersSegmentation, nomsLayersSegmentation] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'TypeDonneUniquement', identSegmentation);
if ~flag
    return
end

indLayers  = [indLayers  indLayersSegmentation];
nomsLayers = [nomsLayers(:); nomsLayersSegmentation(:)];

if isempty(indLayers) % Pas de layer "Masque" trouve
    return
end

str1 = 'Choix d''un masque � appliquer sur l''image';
str2 = 'Select a mask to apply on the image';
[choix, flag] = my_listdlg(Lang(str1,str2), [{Lang('Aucun masque', 'No mask')}; nomsLayers], 'SelectionMode', 'Single');
if flag
    if choix == 1
        indLayerMask = [];
        valMask = 1;
        return
    else
        indLayerMask = indLayers(choix-1);
    end
else
    indLayerMask = [];
    valMask = 1;
    return
end


% TODO : voir si toute la suite peut �tre remplac�e (en principe, oui) par :
% valMask = selectValMask(this(indLayerMask))
% TODO suite : mais manque la gestion RegionOfInterest


StatValues = this(indLayerMask).StatValues;

if StatValues.Min == StatValues.Max
    valMask = StatValues.Min;
else
    ValNaN = get(this(indLayerMask), 'ValNaN');
    %     I = get(this(indLayerMask), 'Image');
    
    listeVal = [];
    subx = 1:this(indLayerMask).nbColumns;
    for k=1:this(indLayerMask).nbRows
        L = get_val_ij(this(indLayerMask), k, subx);
        
        L = round(L); % Modifi� le 16/09/2010 Bug trouv� sur masque Julien Moreau
        
        if isnan(ValNaN)
            L = L(~isnan(L));
        else
            L = L(L ~= ValNaN);
        end
        if isempty(listeVal)
            listeVal = unique(L(:));
        else
            listeVal = [listeVal; unique(L(:))]; %#ok<AGROW>
        end
    end
    listeVal = unique(listeVal);
    Coul = get_coul(this(indLayerMask), listeVal);
    
    if isempty(this(indLayerMask).RegionOfInterest)
        this(indLayerMask) = ROI_raster2vect(this(indLayerMask));
    end
    
    this(indLayerMask) = ROI_union(this(indLayerMask));
    if isempty(this(indLayerMask).RegionOfInterest)
        str1 = 'Le layer donn� pour le masque ne semble pas �tre un masque.';
        str2 = 'The layer that was given does not seem to be a mask.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        indLayerMask = [];
        valMask = 1;
        return
    end
    
    for k=1:length(listeVal)
        valMask = listeVal(k);
        if valMask == 0
            str{k} = num2str(valMask); %#ok<AGROW>
        else
            if isempty(this(indLayerMask).RegionOfInterest) || (valMask > length(this(indLayerMask).RegionOfInterest))
                str{k} = num2str(valMask); %#ok<AGROW>
            else
                str{k} = [num2str(valMask) ' : ' this(indLayerMask).RegionOfInterest(valMask).Name]; %#ok<AGROW>
            end
        end
    end
    
    if strcmp(InitialValue, 'All')
        InitialValue = 1:length(str);
    elseif strcmp(InitialValue, 'Segmentation')
        InitialValue = find(listeVal ~= 0);
    else % 'Last
        InitialValue = length(str);
    end
    
    str1 = 'Valeurs du masque � utiliser';
    str2 = 'Mask values to use';
    [choix, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', InitialValue);
    if flag
        valMask = listeVal(choix);
        Coul = Coul(choix,:);
    else
        valMask = 1;
        return
    end
    
    %     str1 = 'Partie � conserver ?';
    %     str2 = 'Part to conserve ?';
    %     [InOut, flag] = my_listdlg(Lang(str1,str2), {Lang('Interieur','Interior'); Lang('Exterieur','Exterior')}, 'SelectionMode', 'Single');
    %     if ~flag
    %         indLayerMask = [];
    %         valMask = 1;
    %         return
    %     end
end
