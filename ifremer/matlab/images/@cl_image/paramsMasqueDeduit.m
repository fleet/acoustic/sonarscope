function [flag, creationLayer, nomLayer, indLayerMask, valSpecifiqueDepart, valInterval, valMask] = paramsMasqueDeduit(this, indImage)

valSpecifiqueDepart = [];
valInterval         = [];
valMask             = [];

%% Identification du masque

[flag, creationLayer, nomLayer, indLayerMask] = question_Mask(this, indImage);
if ~flag
    return
end

%% 

% Titre = this.Images(this.indImage).Name;
StatValues = this(indImage).StatValues;

str1 = 'Condition de depart ?';
str2 = 'Initial Condition ?';
[choix, flag] = my_questdlg(Lang(str1,str2), ...
    Lang('Valeurs spécifiques', 'Specific values'), ...
    Lang('Intervalle','Interval'));
if ~flag
    return
end
if choix == 1
    str1 = 'Liste des valeurs spécifiques. Ex [1 3 4] ou [2:5 7]';
    str2 = 'List of specific values. Ex [1 3 4] ou [2:5 7]';
    str3 = 'Valeur du masque.';
    str4 = 'Mask value.';
    nomVar = {Lang(str1,str2) ; Lang(str3,str4)};
    value = {StatValues.Min; 1};
    [value, flag] = my_inputdlg(nomVar, value);
    if ~flag
        return
    end
    valSpecifiqueDepart = value{1};
    valMask = value{2};
else
    str1 = 'TODO';
    str2 = 'Mask processing parametres';
    p    = ClParametre('Name', 'Min value',  'Unit', this(indImage).Unit, 'MinValue', StatValues.Min, 'MaxValue', StatValues.Max, 'Value', StatValues.Min);
    p(2) = ClParametre('Name', 'Max value',  'Unit', this(indImage).Unit, 'MinValue', StatValues.Min, 'MaxValue', StatValues.Max, 'Value', StatValues.Max);
    p(3) = ClParametre('Name', 'Mask value', 'Unit', ' ',                 'MinValue', 1,              'MaxValue', 255,            'Value', 1);
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [-2 -2 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    X = a.getParamsValue;
    valInterval = X(1:2);
    valMask = X(3);
end
