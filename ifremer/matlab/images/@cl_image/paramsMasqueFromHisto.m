function [flag, SeuilProba, nbLigBlock] = paramsMasqueFromHisto(this)
 
SeuilProba = [];
nbLigBlock = [];

ident1 = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'DataType', ident1); %, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

str1 = 'TODO';
str2 = 'Mask from histogram parametres';
p    = ClParametre('Name', Lang('Seuil','Threshold'),                      'MinValue', 0.2, 'MaxValue', 0.8,  'Value', 0.5);
p(2) = ClParametre('Name', Lang('Nb Lig par Blocks', 'Nb Lig per Blocks'), 'MinValue', 50,  'MaxValue', 5000, 'Value', 1000);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-1 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    SeuilProba = [];
    nbLigBlock = [];
    return
end
X = a.getParamsValue;
SeuilProba = X(1);
nbLigBlock = X(2);
