function [flag, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, strConf, listeSuby] = paramsModeIfSeveral(this, suby, varargin)

[varargin, OnlyOneConfiguration]          = getPropertyValue(varargin, 'OnlyOneConfiguration', 0);
[varargin, askQuestionSignatureConfSonar] = getPropertyValue(varargin, 'askQuestionSignatureConfSonar', 1); %#ok<ASGLU>

SelectMode        = [];
SelectSwath       = [];
SelectFM          = [];
SelectMode2EM2040 = [];
strConf           = [];

%% Inventaire des configurations du sondeurs dans l'image

[flag, listeConf, listeConfWithNbPings, N, listeModesConf, listeSwathConf, listeFMConf, listeMode2EM2040, listeSuby] ...
    = sonar_listConfigs(this, suby);
if ~flag
    flag = 1;
    return
end

%% Questions � l'utilisateur

switch length(listeConf)
    case 0
        % Pas de config sonar particuli�re
        messageForDebugInspection
    case 1
        if askQuestionSignatureConfSonar == 1
            str1 = sprintf('Cette image contient une configuration sp�cifique du sondeur (%s). Voulez-vous r�aliser ce traitement en conservant une signature de cette configuration ? ', listeConf{1});
            str2 = sprintf('This image contains a specific configuration of the sounder (%s). Do you want to process the data keeping a tag for this configuration ?', listeConf{1});
            [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
            if ~flag
                return
            end
        else
            rep = 1;
        end
        if rep == 1
            %         SelectMode  = listeModes; % ?????????????????????
            %         SelectSwath = listeSwath;
            %         SelectFM    = listeFM;
            if ~isempty(listeModesConf)
                SelectMode = listeModesConf(1);
            end
            if ~isempty(listeSwathConf)
                SelectSwath = listeSwathConf(1);
            end
            if ~isempty(listeFMConf)
                SelectFM = listeFMConf(1);
            end
            if ~isempty(listeMode2EM2040)
                SelectMode2EM2040 = listeMode2EM2040(1);
            end
            strConf = listeConf{1};
        else
            SelectMode  = [];
            SelectSwath = [];
            SelectFM    = [];
            SelectMode2EM2040 = [];
            return
        end
        
    otherwise
        if abs(askQuestionSignatureConfSonar) == 1
            if ~OnlyOneConfiguration
                str1 = 'Cette image contient des configurations diff�rentes du sondeur, voulez-vous r�aliser le traitement globalement sur toutes les configurations ou en s�lectionner un particulier ? ';
                str2 = 'This image contains various sounder configurations, do you want to process it on all configurations or select a specific one ?';
                [rep, flag] = my_questdlg(Lang(str1,str2), ...
                    Lang('Toutes les configurations', 'All configurations'), ...
                    Lang('Une configuration particuli�re', 'A specific one'));
                if ~flag
                    return
                end
                if rep == 1
                    SelectMode  = [];
                    SelectSwath = [];
                    SelectFM    = [];
                    SelectMode2EM2040 = [];
                    return
                end
            end
        else % Ajout JMA le 10/07/2019
            SelectMode  = [];
            SelectSwath = [];
            SelectFM    = [];
            SelectMode2EM2040 = [];
            return
        end
        
        [~, iX] = sort(N);
        InitialValue = iX(end);
        
        str1 = 'S�lectionnez un mode';
        str2 = 'Select one mode';
        [rep, flag] = my_listdlg(Lang(str1,str2), listeConfWithNbPings, 'InitialValue', InitialValue, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        SelectMode  = listeModesConf(rep);
        if isempty(listeSwathConf)
            SelectSwath = [];
        else
            SelectSwath = listeSwathConf(rep);
        end
        if isempty(listeFMConf)
            SelectFM = [];
        else
            SelectFM = listeFMConf(rep);
        end
        if isempty(listeMode2EM2040)
            SelectMode2EM2040 = [];
        else
            SelectMode2EM2040 = listeMode2EM2040(rep);
        end
        strConf   = listeConf{rep};
        listeSuby = listeSuby(rep);
end
