function [flag, listeprofils, listeprofilsAngle, pasx, pasy, XLim, YLim, CoveringPriority, bufferZoneSize] ...
    = paramsMosaiqueAssemblage(this, indImage, nomZoneEtude, XLimCadre, YLimCadre)

XLim           = [];
YLim           = [];
bufferZoneSize = [];

[flag, listeprofils, listeprofilsAngle, pasx, pasy, CoveringPriority] = params_MosaiqueAssemblage(this, indImage);
if ~flag
    return
end

%% Limites de la mosa�que

xmin =  Inf;
xmax = -Inf;
ymin =  Inf;
ymax = -Inf;
for k=1:length(this(listeprofils))
    x = double(get(this(listeprofils(k)), 'x'));
    y = double(get(this(listeprofils(k)), 'y'));
    xmin = min(xmin, min(x));
    xmax = max(xmax, max(x));
    ymin = min(ymin, min(y));
    ymax = max(ymax, max(y));
end
XLim = [xmin xmax];
YLim = [ymin ymax];

switch nomZoneEtude
    case {'Whole image'; 'Image enti�re'} % TODO : Modifier le code pour ne tester que la version anglaise
        XUnit = this(indImage).XUnit;
        YUnit = this(indImage).YUnit;
        
        p    = ClParametre('Name', 'Xmax', ...
            'Unit', XUnit, 'Value', xmax, 'MinValue', xmin, 'MaxValue', xmax);
        p(2) = ClParametre('Name', 'Xmin', ...
            'Unit', XUnit, 'Value', xmin, 'MinValue', xmin, 'MaxValue', xmax);
        p(3) = ClParametre('Name', 'Ymax', ...
            'Unit', YUnit, 'Value', ymax, 'MinValue', ymin, 'MaxValue', ymax);
        p(4) = ClParametre('Name', 'Ymin', ...
            'Unit', YUnit, 'Value', ymin, 'MinValue', ymin, 'MaxValue', ymax);
        a = StyledSimpleParametreDialog('params', p, 'Title', 'Limits of the mosaic');
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        val = a.getParamsValue;
       
        XLim(1) = val(2);
        XLim(2) = val(1);
        YLim(1) = val(4);
        YLim(2) = val(3);
        
    otherwise
        XLim(1) = max(XLim(1), XLimCadre(1));
        XLim(2) = min(XLim(2), XLimCadre(2));
        YLim(1) = max(YLim(1), YLimCadre(1));
        YLim(2) = min(YLim(2), YLimCadre(2));
end

%% Corridor pour mixage

if CoveringPriority == 5
    XUnit         = this(indImage).XUnit;
    valDefault    = 10 * pasx;
    minValue      =  3 * pasx;
    maxValue      = 0.1 * (min(XLim(2)-XLim(1),YLim(2)-YLim(1)));
    flagUnitIsDeg = false;
    if strcmpi(XUnit, 'deg')
        flagUnitIsDeg = true;
        XUnit         = 'm';
        valDefault    = deg2km(valDefault) * 1000;
        minValue      = deg2km(minValue)   * 1000;
        maxValue      = deg2km(maxValue)   * 1000;
    end

    [flag, val] = inputOneParametre(Lang('Taille de la zone tampon', 'Size of buffer zone'), Lang('Taille de la zone tampon', 'Size of buffer zone'), ...
        'Value', valDefault, 'Unit', XUnit, 'MinValue', minValue, 'MaxValue', maxValue);
    if ~flag
        return
    end

    if flagUnitIsDeg
        val = km2deg(val/1000);
    end
    bufferZoneSize = val(1);
end
