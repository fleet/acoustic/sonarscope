function [flag, Lon, Lat, nomFicEchoes, repExport] = paramsMosaiqueExportSoundings(this, indImage, subx, suby, repExport)

Lon          = [];
Lat          = [];
nomFicEchoes = [];

flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% V�rification d'existance d'un layer de latitude

identLayerLat = cl_image.indDataType('Latitude');
[indLayerLat, nomLayersLatitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLat);
if isempty(indLayerLat)
    str = Lang('Il n''existe pas de layer de type "Latitude", je le cree a la volee.', ...
            'No "Latitude" layer exists, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to createa  permanent "Latitude" layer');
    my_warndlg(str, 0, 'Tag', 'I create a transitory one');
else
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
    if length(indLayerLat) > 1
        str1 = 'Il existe plusieurs images de type "Latitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Latitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLatitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLat = indLayerLat(rep);
    end
end

%% Verification d'existance d'un layer de longitude

identLayerLon = cl_image.indDataType('Longitude');
[indLayerLon, nomLayersLongitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLon);
if isempty(indLayerLon)
    str1 = 'Il n''existe pas de layer de type "Longitude", je le cr�e � la vol�e.';
    str2 = 'No "Longitude" layer exists, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to create a  permanent "Longitude" layer';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'I create a transitory one');
else
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
    if length(indLayerLon) > 1
        str1 = 'Il existe plusieurs images de type "Longitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Longitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLongitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLon = indLayerLon(rep);
    end
end

if isempty(indLayerLat) || isempty(indLayerLon)
    [flag, a] = sonarCalculCoordGeo(this, indImage, 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
    Lat = a(1);
    Lon = a(2);
    clear a
else
    x = get(this(indImage), 'x');
    y = get(this(indImage), 'y');
    x = x(subx);
    y = y(suby);
    Lat = extraction(this(indLayerLat), 'x', x, 'y', y);
    Lon = extraction(this(indLayerLon), 'x', x, 'y', y);
end

Filtre = fullfile(repExport, 'Soundings.xml');
[flag, nomFicEchoes] = my_uiputfile('*.xml', 'Give a file name', Filtre);
if ~flag
    return
end
repExport = fileparts(nomFicEchoes);
