function [flag, resol, indLayerLat, indLayerLon, ...
    AcrossInterpolation, AlongInterpolation, MethodeRemplissage, ...
    CreationMosAngle, indLayerEmissionAngle, LimitAngles, ...
    CreationMosTime, indLayerTime, ...
    CreationMosRange, indLayerRange, flagTideCorrection, SpecularInterpolation] = paramsMosaiqueSonar(this, indImage, varargin)

[~, suby, varargin] = getSubxSuby(this(indImage), varargin); %#ok<ASGLU>

resol                 = NaN;
indLayerLat           = [];
indLayerLon           = [];
indLayerEmissionAngle = [];
indLayerTime          = [];
AcrossInterpolation   = 1;
AlongInterpolation    = 1;
CreationMosAngle      = 0;
CreationMosTime       = 0;
MethodeRemplissage    = 1;
LimitAngles           = [];
CreationMosRange      = 0;
indLayerRange         = [];
flagTideCorrection    = 0;
SpecularInterpolation = 0;

flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% V�rification d'existence d'un layer de latitude

[indLayerLat, nomLayersLatitude] = findIndLayerSonar(this, indImage, 'strDataType', 'Latitude');
if isempty(indLayerLat)
    %     str = Lang('TODO', ...
    %         'The "Latitude" Layer doesn''t exist, I''ll create transitory layers. If you have to do this processing a lot of times on this image it would be judicious to create permanent "Latitude" and "Longitude" layers');
    %     my_warndlg(str, 0, 'Tag', 'I create a transitory one');
else
    if length(indLayerLat) > 1
        str1 = 'Il existe plusieurs images de type "Latitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Latitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLatitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLat = indLayerLat(rep);
    end
end

%% V�rification d'existence d'un layer de longitude

[indLayerLon, nomLayersLongitude] = findIndLayerSonar(this, indImage, 'strDataType', 'Longitude');
if isempty(indLayerLon)
    %     str = Lang('TODO', ...
    %         'The "Longitude" Layer doesn''t exist, I create transitory layers. If you have to do this processing a lot of times on this image it would be judicious to create permanent "Latitude" and "Longitude" layers');
    %     my_warndlg(str, 0, 'Tag', 'I create a transitory one');
else
    if length(indLayerLon) > 1
        str1 = 'Il existe plusieurs images de type "Longitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Longitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLongitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLon = indLayerLon(rep);
    end
end

%% V�rification d'existence d'un layer d'angle d'emission

str1 = 'Voulez-vous r�aliser la mosaique de l''angle d''�mission dans le m�me temps (utile pour la gestion des recouvrements de profils dans le module "Assemblage de mosa�ques") ?';
str2 = 'Do you want to create the "Angle" mosaic ? It is useful to manage overlaps on DTM and sonar mosaics.';
[choix, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if choix == 1
    CreationMosAngle = 1;
    [indLayerEmissionAngle, nomLayersEmissionAngle] = findIndLayerSonar(this, indImage, 'strDataType', 'TxAngle');
    if isempty(indLayerEmissionAngle)
        [indLayerEmissionAngle, nomLayersEmissionAngle] = findIndLayerSonar(this, indImage, 'strDataType', 'RxBeamAngle');
        if isempty(indLayerEmissionAngle)
            [indLayerEmissionAngle, nomLayersEmissionAngle] = findIndLayerSonar(this, indImage, 'strDataType', 'BeamPointingAngle', 'OnlyOneLayer');
            if isempty(indLayerEmissionAngle)
                str1 = 'Il n''existe pas de layer de type "EmissionAngle", je le cr�e � la vol�e.';
                str2 = 'No "EmissionAngle" layer exists, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to createa  permanent "EmissionAngle" layer';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'I create a transitory one', 'TimeDelay', 10);
            end
        end
    else
        if length(indLayerEmissionAngle) > 1
            str1 = 'Il existe plusieurs images de type "TxAngle", la quelle voulez-vous utiliser ?';
            str2 = 'Many "TxAngle" layers exist. Which one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersEmissionAngle, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            indLayerEmissionAngle = indLayerEmissionAngle(rep);
        end
    end
    
    %% Limitation angulaire de la mosaique
    
    [flag, LimitAngles] = question_AngularLimits('QL', 2);
    if ~flag
        return
    end
end

%% V�rification d'existance d'un layer Temps de parcours

if get_LevelQuestion >= 3
    str1 = 'Voulez-vous r�aliser la mosaique de "Range (aller simple en m�tres) ?';
    str2 = 'Do you want to create the Range mosaic (single way in meters) ?';
    [choix, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel', 3);
    if ~flag
        return
    end
else
    choix = 2;
end
if choix == 1
    CreationMosRange = 1;
    [flag, indLayerRange] = findAnyLayerRange(this, indImage, 'AtLeastOneLayer');
    if ~flag
        return
    end
end

%% V�rification d'existance d'un layer Temps

if get_LevelQuestion >= 3
    str1 = 'Voulez-vous r�aliser la mosaique temporelle dans le meme temps (utile pour faire le lien entre image SONAR et mosaique)) ?';
    str2 = 'Do you want to create the "UniversalTime" mosaic ? It is useful to navigate from Sonar images or bathymetric files to mosaics or DTM.';
    [choix, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel', 3);
    if ~flag
        return
    end
else
    choix = 2;
end
if choix == 1
    CreationMosTime = 1;
    indLayerTime = findIndLayerSonar(this, indImage, 'strDataType', 'RxTime');
    if isempty(indLayerTime)
        str = Lang('Il n''existe pas de layer de type "UniversalTime", je le cree a la volee.', ...
            'The "UniversalTime" layer doesn''t exist, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to createa  permanent "UniversalTime" layer');
        my_warndlg(str, 0, 'Tag', 'I create a transitory one');
    else
        if length(indLayerTime) > 1
            str1 = 'Il existe plusieurs images de type "UniversalTime", la quelle voulez-vous utiliser ?';
            str2 = 'Many "UniversalTime" layers exist. Which one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersEmissionAngle, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            indLayerTime = indLayerTime(rep);
        end
    end
end

%% Choix de la r�solution

FishLatitude  = get(this(indImage), 'SonarFishLatitude');
FishLongitude = get(this(indImage), 'SonarFishLongitude');
Carto         = get(this(indImage), 'Carto');

if isempty(Carto)
    str1 = 'La cartographie n''est pas d�finie pour cette image.';
    str2 = 'The carto is not defined for this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if isempty(FishLatitude) || isempty(FishLongitude)
    str1 = 'Cette image ne contient pas de navigation.';
    str2 = 'This image does not contain any navigation.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Recherche du layer AcrossDist pour contr�ler la proposition de r�solution

indLayerAcrossDist = findIndLayerSonar(this, indImage, 'strDataType', 'AcrossDist', 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerAcrossDist)
    resolX = 0;
else
    resolX = (this(indLayerAcrossDist).StatValues.Max - this(indLayerAcrossDist).StatValues.Min) / (this(indLayerAcrossDist).nbColumns - 1);
end

%% Calcul du pas de grille � partir de la longueur moyenne parcourue entre 2 pings

% resol = sonar_meanDistPings(this(indImage), 'suby', suby);

sub = ~isnan(FishLatitude) & ~isnan(FishLongitude);
N = sum(sub);
if N == 0
    % Cas o� on a pas de Latitude et longitude, on prend la r�solution du sonar
    
    resolutionD = get(this(indImage), 'SonarResolutionD');
    resolutionX = get(this(indImage), 'SonarResolutionX');
    if isempty(resolutionX)
        if isempty(resolutionD)
            resol = 1;
        else
            resol = resolutionD;
        end
    else
        resol = resolutionX;
    end
else
    [~, D] = legs(FishLatitude(suby), FishLongitude(suby));
    D = D * (nm2km(1) * 1000);
    resol = sum(D, 'omitnan') / N;
end
resol = max(resol, resolX);

%% Grid size

[flag, resol] = get_gridSize('value', resol);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation('QL', 3);
if ~flag
    return
end

%% Along distance interpolation method

[flag, AlongInterpolation] = question_AlongDistanceInterpolation(this(indImage));
if ~flag
    return
end

%% interpolation du sp�culaire (Geoswath)

[flag, SpecularInterpolation] = question_SpecularInterpolation(this(indImage));
if ~flag
    return
end

%% Filling method

if get_LevelQuestion >= 3
    str1 = 'M�thode de remplissage';
    str2 = 'Filling method';
    [MethodeRemplissage, flag] = my_listdlg(Lang(str1,str2), {Lang('Moyenne','Mean'); 'Max'; 'Min'; Lang('Mediane','Median')}, 'SelectionMode', 'Single', 'ColorLevel', 3);
    if ~flag
        return
    end
end

%% Correction de mar�e

DT = cl_image.indDataType('Bathymetry');
flagTideCorrection = (this(indImage).DataType == DT) && isSonarSignal(this(indImage), 'Tide');
if flagTideCorrection
    if this(indImage).Sonar.BathymetryStatus.TideApplied
        flagTideCorrection = 0;
    else
        [flag, flagTideCorrection] = question_TideCorrection;
        if ~flag
            return
        end
    end
end
