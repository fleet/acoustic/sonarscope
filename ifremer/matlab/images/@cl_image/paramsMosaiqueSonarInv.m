function [flag, indLayerTxAngle, indLayerLat, indLayerLon, indLayerMos, indLayerMosTxAngle] = paramsMosaiqueSonarInv(this, indImage)

indLayerMos = [];
indLayerLat = [];
indLayerLon = [];
indLayerTxAngle = [];
indLayerMosTxAngle = [];

flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% Recherche des images en LatLong

[indLayerMos, nomsLayerMos] = findLayers(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if isempty(indLayerMos)
    my_warndlg('No "LatLong" image to import', 1);
    return
end

str1 = 'Quelle image voulez-vous importer ?';
str2 = 'Which layer do you want to import ?';
[rep, flag] = my_listdlg(Lang(str1,str2), nomsLayerMos, 'SelectionMode', 'Single');
if ~flag
    return
end
indLayerMos = indLayerMos(rep);

%% Verification d'existance d'un layer de latitude

identLayerLat = cl_image.indDataType('Latitude');
[indLayerLat, nomLayersLatitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLat);
if ~isempty(indLayerLat)
    if length(indLayerLat) > 1
        str1 = 'Il existe plusieurs images de type "Latitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Latitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLatitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLat = indLayerLat(rep);
    end
end

%% Verification d'existance d'un layer de longitude

identLayerLon = cl_image.indDataType('Longitude');
[indLayerLon, nomLayersLongitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLon);
if ~isempty(indLayerLon)
    if length(indLayerLon) > 1
        str1 = 'Il existe plusieurs images de type "Longitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Longitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLongitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLon = indLayerLon(rep);
    end
end

%% Recherche du layer TxAngle en g�om�trie LatLong

identLayerTxAngle1 = cl_image.indDataType('TxAngle');
[indLayerMosTxAngle1, nomsLayerMosTxAngle1] = findLayers(this, 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'DataType', identLayerTxAngle1);
identLayerTxAngle2 = cl_image.indDataType('RxBeamAngle');
[indLayerMosTxAngle2, nomsLayerMosTxAngle2] = findLayers(this, 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'DataType', identLayerTxAngle2);
identLayerTxAngle3 = cl_image.indDataType('BeamPointingAngle');
[indLayerMosTxAngle3, nomsLayerMosTxAngle3] = findLayers(this, 'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'DataType', identLayerTxAngle3);
indLayerMosTxAngle = [indLayerMosTxAngle1 indLayerMosTxAngle2 indLayerMosTxAngle3];
nomsLayerMosTxAngle = [nomsLayerMosTxAngle1 nomsLayerMosTxAngle2 nomsLayerMosTxAngle3];
if isempty(indLayerMosTxAngle)
    %     str1 = 'TODO';
    %     str2 = '"TxAngle_LatLong" layer required to run overlaping process. You are not concerned by this message if your mosaic has no overlapping lines.';
    %     my_warndlg(Lang(str1,str2), 1);
    return
else
    str1 = 'Il existe plusieurs images de type "TxAngle", la quelle voulez-vous utiliser ?';
    str2 = 'Many "TxAngle" layers exist, which one do you want to use ?';
    [rep, flag] = my_listdlg(Lang(str1,str2), ...
        [{Lang('Aucune', 'None')}; nomsLayerMosTxAngle(:)], 'SelectionMode', 'Single', 'InitialValue', length(nomsLayerMosTxAngle)+1);
    if ~flag
        return
    end
    if rep == 1
        indLayerMosTxAngle = [];
    else
        indLayerMosTxAngle = indLayerMosTxAngle(rep-1);
    end

%     if length(indLayerMosTxAngle) == 1
%         str1 = 'Il existe plusieurs images de type "TxAngle", la quelle voulez-vous utiliser ?';
%         str2 = 'Many "TxAngle" layers exist. Which one do you want to use ?';
%         [rep, flag] = my_listdlg(Lang(str1,str2), nomsLayerMosTxAngle, 'SelectionMode', 'Single');
%         if ~flag
%             return
%         end
%         indLayerMosTxAngle = indLayerMosTxAngle(rep);
%     end
end

%% Verification d'existance d'un layer de TxAngle

[indLayerTxAngle1, nomLayersTxAngle1] = findIndLayerSonar(this, indImage, 'DataType', identLayerTxAngle1);
[indLayerTxAngle2, nomLayersTxAngle2] = findIndLayerSonar(this, indImage, 'DataType', identLayerTxAngle2);
[indLayerTxAngle3, nomLayersTxAngle3] = findIndLayerSonar(this, indImage, 'DataType', identLayerTxAngle3);
indLayerTxAngle = [indLayerTxAngle1 indLayerTxAngle2 indLayerTxAngle3];
nomLayersTxAngle = [nomLayersTxAngle1 nomLayersTxAngle2 nomLayersTxAngle3];
if isempty(indLayerTxAngle)
    identLayerRxBeamAngle = cl_image.indDataType('RxBeamAngle');
    [indLayerTxAngle, nomLayersTxAngle] = findIndLayerSonar(this, indImage, 'DataType', identLayerRxBeamAngle); %#ok<ASGLU>
    if isempty(indLayerTxAngle)
        str = '"TxAngle" layer required to run overlaping process doesn''t exist.';
        my_warndlg(str, 1);
        flag = 0;
    end
    return
else
    if length(indLayerTxAngle) > 1
        str1 = 'Il existe plusieurs images de type "TxAngle", la quelle voulez-vous utiliser ?';
        str2 = 'Many "TxAngle" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersTxAngle, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerTxAngle = indLayerTxAngle(rep);
    end
end
