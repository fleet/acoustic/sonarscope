function [flag, indLayers, NbMinPoints, indLayerMask, valMask, nomFicCsv, Sampling, flagValNat, repExport] ...
    = paramsMultiImagesBoxPlots(this, indImage, repExport)

indLayerMask = [];
valMask      = [];
NbMinPoints  = [];
nomFicCsv    = [];
Sampling     = [];
flagValNat   = false;
 
[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'memeDataType');
if ~flag
    return
end
if isempty(indLayers)
    return
end

str1 = 'Images � analyser';
str2 = 'Layers to be analysed';
[choix, flag] = my_listdlgMultiple(Lang(str1,str2), nomsLayers);
if ~flag
    return
end
indLayers = indLayers(choix);
[flag, indLayerMask, valMask] = paramsMasque(this, indImage, 'InitialValue', 'Segmentation');
if ~flag
    return
end

str1 = 'Nombre minimum de points pour calculer une valeur';
str2 = 'Minimum number of points for statistic computation';
[flag, NbMinPoints] = inputOneParametre(Lang(str1,str2), 'Value', ...
    'Value', 100, 'MinValue', 10, 'Unit', 'pixels', 'Format', '%d');
if ~flag
    return
end

%% Nom du fichier r�sum�

nomFicCsv = fullfile(repExport, 'StatsSummary.csv');
[flag, nomFicCsv] = my_uiputfile('*.csv', 'Summary File', nomFicCsv);
if ~flag
    return
end
repExport = fileparts(nomFicCsv);

%% Echantillonnage des pixels

[flag, Sampling] = paramsRecomputeStats(this(indImage));
if ~flag
    return
end

%% Calcul en Amplitude ?

if strcmp(this(indImage).Unit, 'dB')
    [rep, flag] = my_listdlg('Compute stats in', {'dB'; 'Amplitude'}, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    flagValNat = (rep == 2);
end
