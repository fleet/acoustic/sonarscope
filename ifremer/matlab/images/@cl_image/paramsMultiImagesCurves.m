function [flag, listeprofils, listeprofils1, Bins1, listeprofils2, Bins2, indLayerMask, valMask, nomCourbe] = paramsMultiImagesCurves(this, indImage)

persistent persistent_ValStep1 persistent_ValMin1 persistent_ValMax1
persistent persistent_ValStep2 persistent_ValMin2 persistent_ValMax2

listeprofils = [];
listeprofils1 = [];
listeprofils2 = [];
indLayerMask = [];
valMask = [];
nomCourbe = [];
Bins1     = [];
Bins2     = [];

%% Selection du type de layer conditionnel

[flag, DataType1, DataType2] = selecCurveDataTypes(this, indImage);
if ~flag
    return
end

%% Recherche des premiers layers conditionnels

[flag, listeprofils, listeprofils1] = listePairesLayersSynchronises(this, indImage, cl_image.strDataType{DataType1});
if ~flag
    return
end
if isempty(listeprofils)
    flag = 0;
    return
end

if isempty(listeprofils1)
    str1 = 'Aucune image n''a pu �tre apair�e.';
    str2 = 'None image could be put in couples.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if length(listeprofils) ~= length(listeprofils1)
    str1 = 'Certaines images n''ont pas pu �tre apair�es.';
    str2 = 'Some images could not be put in couples.';
    my_warndlg(Lang(str1,str2), 1);
end

%% Recherche des seconds layers conditionnels

if ~isempty(DataType2)
    if ~flag
        return
    end
    [flag, listeprofils, listeprofils2] = listePairesLayersSynchronises(this, indImage, cl_image.strDataType{DataType2}, 'listeprofils', listeprofils);
    if ~flag
        return
    end
    if isempty(listeprofils2)
        flag = 0;
        return
    end
    
    if isempty(listeprofils2)
        str1 = 'Aucune image n''a pu �tre apair�e.';
        str2 = 'None image could be put in couples.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    
    if length(listeprofils) ~= length(listeprofils2)
        str1 = 'Certaines images n''ont pas pu �tre apair�es.';
        str2 = 'Some images could not be put in couples.';
        my_warndlg(Lang(str1,str2), 1);
    end
end

%% Recherche d'un masque �ventuel

[flag, indLayerMask, valMask] = paramsMasque(this, indImage, 'InitialValue', 'Segmentation');
if ~flag
    return
end

%% Nom de la courbe

str1 = 'Nom de la courbe :';
str2 = 'Name of the curve :';
nomVar = Lang(str1,str2);
DT = this(indImage).DataType;
% value = sprintf('%s / %s', cl_image.strDataType{DT}, DataTypeAssocie{1});
if isempty(DataType2)
    value = sprintf('%s / %s / %s', cl_image.strDataType{DT}, cl_image.strDataType{DataType1}, cl_image.strDataType{DataType2});
else
    value = sprintf('%s / %s', cl_image.strDataType{DT}, cl_image.strDataType{DataType1});
end
[nomCourbe, flag] = my_inputdlg({nomVar}, {value});
if ~flag
    return
end
nomCourbe =  nomCourbe{1};

%% Saisie de Bins1

if isempty(persistent_ValStep1)
    ValStep1 = 0;
else
    ValStep1 = persistent_ValStep1;
end

if isempty(persistent_ValMin1)
    ValMin1 = 0;
else
    ValMin1 = persistent_ValMin1;
end

if isempty(persistent_ValMax1)
    ValMax1 = 0;
else
    ValMax1 = persistent_ValMax1;
end

unite = this(listeprofils1(1)).Unit;

Titre1 = sprintf('Valeurs extr�mes et intervalle de l''histogramme de %s: ', cl_image.strDataType{DT});
Titre2 = sprintf('Histogram extreme values and step of %s: ', cl_image.strDataType{DT});
p    = ClParametre('Name', 'Min', ...
    'Unit', unite, 'Value', ValMin1);
p(2) = ClParametre('Name', 'Max', ...
    'Unit', unite, 'Value', ValMax1);
p(3) = ClParametre('Name', Lang('Pas', 'Step'), ...
    'Unit', unite, 'Value', ValStep1, 'MinValue', 0);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(Titre1,Titre2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
value = a.getParamsValue;

ValMin1  = value(1);
ValMax1  = value(2);
ValStep1 = value(3);

persistent_ValStep1 = ValStep1;
persistent_ValMin1  = ValMin1;
persistent_ValMax1  = ValMax1;

Bins1 = ValMin1:ValStep1:ValMax1;


%% Saisie de Bins2

if ~isempty(listeprofils2)
    if isempty(persistent_ValStep2)
        ValStep2 = 0;
    else
        ValStep2 = persistent_ValStep2;
    end
    
    if isempty(persistent_ValMin2)
        ValMin2 = 0;
    else
        ValMin2 = persistent_ValMin2;
    end
    
    if isempty(persistent_ValMax2)
        ValMax2 = 0;
    else
        ValMax2 = persistent_ValMax2;
    end
    
    unite = this(listeprofils2(1)).Unit;
    DT = DataType2;
    
    Titre1 = sprintf('Valeurs extr�mes et intervalle de l''histogramme de\n%s', cl_image.strDataType{DT});
    Titre2 = sprintf('Histogram extreme values and step of\n%s', cl_image.strDataType{DT});
    p    = ClParametre('Name', 'Min', ...
        'Unit', unite, 'Value', ValMin2);
    p(2) = ClParametre('Name', 'Max', ...
        'Unit', unite, 'Value', ValMax2);
    p(3) = ClParametre('Name', Lang('Pas', 'Step'), ...
        'Unit', unite, 'Value', ValStep2, 'MinValue', 0);
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(Titre1,Titre2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    value = a.getParamsValue;
    
    ValMin2  = value(1);
    ValMax2  = value(2);
    ValStep2 = value(3);
    
    persistent_ValStep2 = ValStep2;
    persistent_ValMin2 = ValMin2;
    persistent_ValMax2 = ValMax2;
    
    Bins2 = ValMin2:ValStep2:ValMax2;
end
