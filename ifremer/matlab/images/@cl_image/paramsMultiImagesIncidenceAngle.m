function [flag, indLayersTxAngle, indLayersHeading, indLayersBathy] = paramsMultiImagesIncidenceAngle(this, indImage)
 
indLayersTxAngle = [];
indLayersHeading = [];
indLayersBathy   = [];

%% Test si l'image est bien en "LatLong"

ident = cl_image.indDataType('TxAngle');
flag = testSignature(this(indImage), 'DataType', ident, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Recherche des layers de type "Heading" synchronis�s

[flag, indLayersTxAngle, indLayersHeading] = listePairesLayersSynchronises(this, indImage, 'Heading');
if ~flag
    return
end
if isempty(indLayersTxAngle)
    flag = 0;
    return
end

if isempty(indLayersTxAngle)
    str1 = 'Aucune image n''a pu �tre apair�e.';
    str2 = 'None image could be put in couples.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if length(indLayersTxAngle) ~= length(indLayersHeading)
    str1 = 'Certaines images n''ont pas pu �tre apair�es.';
    str2 = 'Some images could not be put in couples.';
    my_warndlg(Lang(str1,str2), 1);
end

%% Recherche du/des MNT

str1 = 'Quel type de MNT doit �tre utilis� ?';
str2 = 'What type of DTM should me use ?';
str3 = 'Global';
str4 = 'Global';
str5 = 'Individuel pour chaque image';
str6 = 'Individual for each image';
[TypeDTM, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
if ~flag
    return
end

identBathymetry = cl_image.indDataType('Bathymetry');
[flag, indLayersBathy] = listeLayersSynchronises(this, indLayersTxAngle(1), 'TypeDonneUniquement', identBathymetry, 'OnlyOneLayer', 1);
if ~flag
    return
end

if TypeDTM == 1 % Global
    if isempty(indLayersBathy)
        str1 = 'Aucun layer de "Bathymetry" was found.';
        str2 = 'No "Bathymetry" was found.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    
else % Individuel pour chaque image
    
    my_warndlg('Not yet verified, may be bugs, ask to sonarscope@ifremer.fr if you need it.', 1);
%     flag = 0;
%     return
    
    [flag, indLayersTxAngle, indLayersBathy] = listePairesLayersSynchronises(this, indImage, 'Bathymetry', 'listeprofils', indLayersTxAngle);
    if ~flag
        return
    end
    if isempty(indLayersTxAngle)
        flag = 0;
        return
    end
    
    if isempty(indLayersTxAngle)
        str1 = 'Aucune image n''a pu �tre apair�e.';
        str2 = 'None image could be put in couples.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    
    if length(indLayersTxAngle) ~= length(indLayersBathy)
        str1 = 'Certaines images n''ont pas pu �tre apair�es.';
        str2 = 'Some images could not be put in couples.';
        my_warndlg(Lang(str1,str2), 1);
    end
end
