function  [flag, threshold] = paramsQuadtree(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

I = this.Image(suby,subx,:);
minVal = min(I(:), [], 'omitnan');
maxVal = max(I(:), [], 'omitnan');
step = (maxVal - minVal) / 10;

str1 = 'Paramétrage du Quadtree';
str2 = 'Quadtree parameter';
p = ClParametre('Name', Lang('Seuil','Threshold'), ...
    'MinValue', 0, 'MaxValue', maxVal - minVal, 'Value', step);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
threshold = a.getParamsValue;
