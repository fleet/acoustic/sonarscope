function [flag, Sampling] = paramsRecomputeStats(~)

Sampling = [];

str1 = 'Mode de calcul des statistiques';
str2 = 'Type of computation';
str3 = 'Sous échantillonnage si besoin';
str4 = 'Sub sampling if needed';
str5 = 'Tous les pixels';
str6 = 'All the pixels';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
if ~flag
    return
end

if rep == 2
    Sampling = 1;
end
