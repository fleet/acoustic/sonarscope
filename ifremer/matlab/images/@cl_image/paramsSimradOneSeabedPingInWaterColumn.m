function [subPing, flag] = paramsSimradOneSeabedPingInWaterColumn(this, nomFic, xyCross)
    
subPing  = [];
   
aKM = cl_simrad_all('nomFic', nomFic);
[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 1);
if ~flag
    return
end

N = length(DataDepth.PingCounter);

str1 = 'Liste des pings � charger : ';
str2 = 'Ping numbers to load : ';

InitialFileName = this.InitialFileName;
flagPingXxx = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if strcmp(nomFic, InitialFileName) && flagPingXxx
    iy = round(xyCross(2));
    rep = sprintf('1 %d % d', iy, N);
else
    rep = sprintf('1  %d', N);
end
[rep, flag] = my_inputdlg(Lang(str1,str2), rep);
if ~flag
    return
end
subPing = eval(['[' rep{1} ']']);
