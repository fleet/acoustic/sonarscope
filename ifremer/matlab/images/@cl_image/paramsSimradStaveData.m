function [flag, subPing] = paramsSimradStaveData(this, nomFic, xyCross)
    
subPing  = [];

aKM = cl_simrad_all('nomFic', nomFic);
if isempty(aKM)
    flag = 0;
    return
end
  
%% Lecture des datagrammes StaveData

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_StaveData', 'Memmapfile', -1); % NON TESTE
if ~flag
    str1 = 'Il n''y a pas de datagrammes de Stave Data dans ce fichier.';
    str2 = 'No Stave Data datagrams in this file.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Lecture des datagrammes Depth

[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 1);
if ~flag
    return
end

%% Choix des num�ros de pings

T1 = DataDepth.PingCounter(:,1);
T2 = Data.PingCounter(:,1);
T1 = T1(~isnan(T1));
T2 = T2(~isnan(T2));
[subDepth, subSD] = intersect3(T1, T2, T2);

N = length(subSD);
if N == 0
    str1 = sprintf('Il n''y a aucun ping commun entre datagrammes Depth et Stave.\nVoulez-vous quand m�me loader les donn�es Stave sans synchronisation avec les autres donn�es ?');
    str2 = sprintf('There is no common pings for Depth and Stave datagrams.\nDo you want to load the Stave data without synchronisation with the other data anyway ?');
    [ind, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if ind == 2
        flag = 0;
        return
    end
    subPing = -T2;
    return
end

InitialFileName = this.InitialFileName;
flagPingXxx = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if strcmp(nomFic, InitialFileName) && flagPingXxx
    iy = round(xyCross(2));
    if (iy > subDepth(1)) && (iy < subDepth(end))
        rep = sprintf('%d %d % d', subDepth(1), iy, subDepth(end));
    else
        rep = sprintf('%d  %d', subDepth(1), subDepth(end));
    end
else
    rep = sprintf('%d  %d', subDepth(1), subDepth(end));
end

str1 = 'Liste des pings � charger : ';
str2 = 'Ping numbers to load : ';
[rep, flag] = my_inputdlg(Lang(str1,str2), rep);
if ~flag
    return
end
subPing = str2num(rep{1});
