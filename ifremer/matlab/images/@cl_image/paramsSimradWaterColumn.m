function [flag, subPing, DepthMin, DepthMax, DisplayLevel, CorrectionTVG] = paramsSimradWaterColumn(this, nomFic, xyCross)
    
subPing       = [];
DepthMin      = [];
DepthMax      = [];
DisplayLevel  = [];
CorrectionTVG = [];
% flag = 0;

aKM = cl_simrad_all('nomFic', nomFic);

%% Test si existence de donn�es colonne d'eau

[flag, DataWC] = read_WaterColumn_bin(aKM, 'Memmapfile', -1);
if ~flag
    str1 = 'Il n''y a pas de datagrammes de Water Column dans ce fichier.';
    str2 = 'No Water Column datagrams in this file.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
  
%% S�lection des pings

[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 1);
if ~flag
    return
end

T1 = DataDepth.PingCounter(:,1);
T2 = DataWC.PingCounter(:,1);
T1 = T1(~isnan(T1));
T2 = T2(~isnan(T2));
[subDepth, subWC] = intersect3(T1, T2, T2);

N = length(subWC); %#ok<NASGU>

str1 = 'Liste des pings � charger : ';
str2 = 'Ping numbers to load : ';

InitialFileName = this.InitialFileName;
flagPingXxx = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if strcmp(nomFic, InitialFileName) && flagPingXxx
    iy = round(xyCross(2));
%     rep = sprintf('1 %d % d', iy, N);
    if (iy > subDepth(1)) && (iy < subDepth(end))
        rep = sprintf('%d %d % d', subDepth(1), iy, subDepth(end));
    else
        rep = sprintf('%d  %d', subDepth(1), subDepth(end));
    end
else
%     rep = sprintf('1  %d', N);
    rep = sprintf('%d  %d', subDepth(1), subDepth(end));
end
[rep, flag] = my_inputdlg(Lang(str1,str2), rep);
if ~flag
    return
end
subPing = eval(['[' rep{1} ']']);

%% Saisie de la distance maximale recherch�e

DepthMax = -12000;
DepthMin = -1;
if get_LevelQuestion >= 3
    str1 = 'TODO';
    str2 = 'Please give min and max Depth values if you can do it, keep initial values if not.';
    p(1) = ClParametre('Name', Lang('Profondeur Min', 'Min Depth'), ...
        'Unit', 'm',  'MinValue', abs(DepthMin), 'MaxValue', abs(DepthMax), 'Value', abs(DepthMin));
    p(2) = ClParametre('Name', Lang('Bloc', 'Bloc'), ...
        'Unit', 'm',  'MinValue', 10, 'MaxValue', abs(DepthMax), 'Value', abs(DepthMax)); % '%d'
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;
    DepthMax = -max(val);
    DepthMin = -min(val);
end

%% Saisie du niveau de visualisation

[flag, DisplayLevel] = question_DetailLevelBDA('Default', 1, 'Passport', 1);
if ~flag
    return
end

%% CorrectionTVG

[flag, CorrectionTVG] = question_CompensationTVG_WC;
if ~flag
    return
end
