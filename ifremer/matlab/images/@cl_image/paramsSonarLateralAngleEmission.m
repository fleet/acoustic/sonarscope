function [flag, SonarFamily, useRoll, indAcrossDistance] = paramsSonarLateralAngleEmission(this, indImage)

useRoll           = 0;
indAcrossDistance = [];
SonarFamily       = [];

GT = this(indImage).GeometryType;
if GT == cl_image.indGeometryType('SampleBeam')
    SonarFamily = -2; % Means Multibeam / Water Column Data
    flag = 1;
    return
else
    flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
    if ~flag
        return
    end
end

SonarFamily = get(get(this(indImage), 'SonarDescription'), 'Sonar.Family');
if SonarFamily == 1 % SonarLateral
    flagRoll = get(this(indImage), 'flagRoll');
    if flagRoll
        [rep, flag] = my_questdlg(Lang('Prise en compte du roulis ?', 'Consider Roll angle ?'), 'Init', 2);
        if flag
            useRoll = (rep == 1);
        end
    end
else % 'SondeurMultiFaisceau'
    identLayer  = cl_image.indDataType('AcrossDist');
    indAcrossDistance = findIndLayerSonar(this, indImage, 'DataType', identLayer, 'OnlyOneLayer');
    if isempty(indAcrossDistance)
        str1 = 'L''image est de type SondeurMultiFaisceau. Il est donc pr�f�rable de cr�er ce layer "EmissionAngle" dans Caraibes. Vous pouvez toutefois cr�er ce layer en faisant comme si il s''agissait d''un sonar lateral. A vos risques et perils.';
        str2 = 'TODO';
        my_warndlg(Lang(str1,str2), 1);

        [rep, flag] = my_questdlg(Lang('Calcul comme si il s''agissait d''un sonar lateral ?', 'Compute like a sidescan sonar ?'));
        if ~flag
            return
        end
        if rep == 1
            SonarFamily = 1;
        end
% flag = 0;
    end
end
