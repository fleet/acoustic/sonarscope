function [flag, FiltreSonar, FiltreProfile, CoteDetection, Penetration] = paramsSonarLateralHauteur(this, subx)

FiltreSonar   = [];
FiltreProfile = [];
CoteDetection = [];
Penetration   = 0;

% val = [1 0.1 2 0 0];
% val = [2 0.05 2 0 0];
val = [2 0.1 2 0 0];
if get_LevelQuestion >= 3
    str1 = 'TODO';
    str2 = sprintf('Height detection parametres (filters are zero-phase\nforward and reverse Butterworth digital filters)');
    p    = ClParametre('Name', Lang('Ordre du filtre donn�es SONAR', 'Filter order of data SONAR'), ...
        'Value', val(1), 'MinValue', 1, 'MaxValue', 8);
    p(2) = ClParametre('Name', Lang('Pulsation de coupure du filtre SONAR', 'Cut-off pulsation of data SONAR'), ...
        'Value', val(2), 'MinValue', 0, 'MaxValue', 0.2);
    p(3) = ClParametre('Name', Lang('Ordre du filtre de la hauteur', 'Filter order of height'), ...
        'Value', val(3), 'MinValue', 1, 'MaxValue', 8);
    p(4) = ClParametre('Name', Lang('Pulsation de coupure du filtre de la hauteur', 'Cut-off pulsation of height filter'), ...
        'Value', val(4), 'MinValue', 0, 'MaxValue', 0.2);
    p(5) = ClParametre('Name', Lang('P�n�tration', 'Penetration'), ...
        'Value', val(5), 'MinValue', 0, 'MaxValue', 1);
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;
else
    msg = 'To get more parameters : Info, Preferences, Questions level = Advanced';
    disp(msg)
end
FiltreSonar   = val(1:2);
FiltreProfile = val(3:4);
Penetration   = val(5);

N = this.nbColumns;
if isequal(subx, 1:N)    % Image entiere
    str1 = 'Calcul de la hauteur sur voie :';
    str2 = 'Set reference side for computation :';
    [CoteDetection, flag] = my_listdlg(Lang(str1,str2), {Lang('Babord', 'Port side'); Lang('Tribord', 'Starboard')}, 'SelectionMode', 'Single');
    if ~flag
        return
    end
else
    CoteDetection = 0;
    flag = 1;
end


