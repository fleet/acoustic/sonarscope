function [flag, resolutionX, createNbp] = paramsSonarLateralProjectionX(this)

resolutionD = get(this, 'SonarResolutionD');
resolutionX = get(this, 'SonarResolutionX');

createNbp = [];

str1 = 'Param�tre de la correctiion d''obliquite';
str2 = 'Slant range correction parametre';
str3 = 'R�solution horizontale';
str4 = 'Horizontal spacing';

[flag, resolutionX] = inputOneParametre(Lang(str1,str2), Lang(str3,str4), ...
    'Value', max(resolutionX, resolutionD), 'Unit', 'm', 'MinValue', resolutionD);
if ~flag
    return
end

% this = set(this, 'SonarResolutionX', resolutionX);

if get_LevelQuestion >= 3
    [rep, flag] = my_questdlg(Lang('Creation du layer Nb Points moyenn�s ?', 'Layer creation of Nb of averaged samples ?'), ...
        'Init', 2, 'ColorLevel', 3);
    if ~flag
        return
    end
    createNbp  = (rep == 1);
else
    createNbp = 0;
end
