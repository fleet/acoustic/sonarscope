function [flag, indLayerAngle, AngleMax, sigma] = paramsSonarSpeculaireReinterpol(this, indImage)

indLayerAngle = [];
AngleMax      = [];
sigma         = [];

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity);
if ~flag
    return
end

%% V�rification d'existance d'un layer de AveragedPtsNb

indDataTypeConditions(1) = cl_image.indDataType('TxAngle');
indDataTypeConditions(2) = cl_image.indDataType('RxBeamAngle');
indDataTypeConditions(3) = cl_image.indDataType('SonarAcrossBeamAngle');
indDataTypeConditions(4) = cl_image.indDataType('BeamPointingAngle');
indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', indDataTypeConditions, 'OnlyOneLayer');
if isempty(indLayerAngle)
    my_warndlg(Lang('Aucun layer "Angle" trouv�.', 'No "Angle" found.'), 1);
    flag = 0;
    return
end

%{
identTxAngle  = cl_image.indDataType('TxAngle');
indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identTxAngle, 'OnlyOneLayer');
if isempty(indLayerAngle)
    identRxBeamAngle  = cl_image.indDataType('RxBeamAngle');
    indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identRxBeamAngle, 'OnlyOneLayer');
    if isempty(indLayerAngle)
        my_warndlg(Lang('Aucun layer "TxAngle" ou "RxBeamAngle" trouv�.', 'No "TxAngle" or "RxBeamAngle" found.'), 1);
    end
end
%}

%% Rayon du filtre gaussien

% [flag, sigma] = paramsTagMenuFiltreGaussien(I0, 'Sigma', 0.5);

str1 = 'IFREMER - SonarScope : Param�tres de filtrage du sp�culaire.';
str2 = 'IFREMER - SonarScope : Specular filter parameters.';
p    = ClParametre('Name', 'Angle Max', 'Unit', 'Deg',   'Value', 25,  'MinValue', 5,   'MaxValue', 45);
p(2) = ClParametre('Name', 'Sigma',      'Unit', 'Pixel', 'Value', 0.5, 'MinValue', 0.1, 'MaxValue', 3);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
AngleMax = val(1);
sigma    = val(2);

% set(a, 'minvalue', [ 5 0.1])
% set(a, 'maxvalue', [45 3])
