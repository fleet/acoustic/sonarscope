function [flag, alpha] = paramsSonarTVGCompensationIfremer(this)

alpha = get(this, 'SonarTVG_IfremerAlpha');

str1 = 'Nouveau coefficient d''att�nuation � appliquer.';
str2 = 'New Absorption coefficient to be applied.';
str3 = 'Coefficient d''att�nuation';
str4 = 'Absorption coefficient';

[flag, alpha] = inputOneParametre(Lang(str1,str2), Lang(str3,str4), ...
    'Value', mean(alpha, 'omitnan'), 'Unit', 'dB/km/m', 'MinValue', 0, 'MaxValue', 100);
if ~flag
    return
end
