function [flag, bins, Selection, CourbeBias, indLayerMask, valMask, nomCourbe, Commentaire, ...
    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType] = ...
    paramsStatisticsCurve(this, indImage, nomZoneEtude, subx, suby, msg, varargin)

[varargin, SonarStatus]                   = getPropertyValue(varargin, 'SonarStatus',                   []);
[varargin, CompleteWithInitialFileName]   = getPropertyValue(varargin, 'CompleteWithInitialFileName',   1);
[varargin, SignatureConfigurationSonar]   = getPropertyValue(varargin, 'SignatureConfigurationSonar',   1);
[varargin, askQuestionSignatureConfSonar] = getPropertyValue(varargin, 'askQuestionSignatureConfSonar', 1);
[varargin, askQuestionMask]               = getPropertyValue(varargin, 'askQuestionMask',               1);
[varargin, SkipTxBeamIndexBins]           = getPropertyValue(varargin, 'SkipTxBeamIndexBins',           0);
[varargin, listeFicAll]                   = getPropertyValue(varargin, 'listeFicAll',                   []);
[varargin, selectMeanCompType]            = getPropertyValue(varargin, 'selectMeanCompType',            0);
[varargin, MeanCompType]                  = getPropertyValue(varargin, 'MeanCompType',                  1);
[varargin, OnlyOneConfiguration]          = getPropertyValue(varargin, 'OnlyOneConfiguration',          []); %#ok<ASGLU>

bins         = [];
Selection    = [];
indLayerMask = [];
valMask      = [];
nomCourbe    = [];
Commentaire  = [];
Coul         = [];
nbPMin       = [];
SelectMode   = [];
SelectSwath  = [];
SelectFM     = [];
SelectMode2EM2040 = [];

switch msg
    case 'STATS_ConditionCalculBias_CI1'
        CourbeBias = 1;
        strAppendCurveName = 'CompensSingle';
    case 'STATS_ConditionCalculBiasMultiple_CI1'
        CourbeBias = 2;
        strAppendCurveName = 'CompensMultiple';
    otherwise
        CourbeBias = 0;
        strAppendCurveName = [];
end

%% S�lection des layers ?

Tag = [];
if isempty(SonarStatus)
    [flag, Selection, Coul] = selectionLayersSynchronises(this, indImage, 'subx', subx, 'suby', suby, 'Max', 2, 'SignauxVert');
    if ~flag
        return
    end
else
    if strcmp(SonarStatus, 'BestBSConstructeur')
        DataTypeIn = [cl_image.indDataType('IncidenceAngle'); cl_image.indDataType('RxAngleEarth'); cl_image.indDataType('TxAngle'); cl_image.indDataType('BeamPointingAngle')];
        [indLayerTxAngle, nomLayer, DataType] = findIndLayerSonar(this, indImage, 'DataType', DataTypeIn, ...
            'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer', 'FirstOfList', 1);
        if isempty(indLayerTxAngle)
            flag = 0;
            return
        end
        Selection.Name     = nomLayer; % extract_ImageName(this(indImage));
        Selection.TypeVal  = 'Layer';
        Selection.indLayer = indLayerTxAngle;
        Selection.DataType = DataType; % TODO :
        
        if DataType ~= DataTypeIn(1)
            str1 = sprintf('ATTENTION : il est pr�f�rable de disposer d''un layer de type "%s" pour r�aliser ce traitement.\n\nLe layer par d�faut trouv� par SSc est de type "%s".\n\nVoulez-vous poursuivre l''op�ration avec ce layer ?', ...
                cl_image.strDataType{DataTypeIn(1)}, cl_image.strDataType{DataType});
            str2 = sprintf('WARNING : it is preferable to use a layer of type "%s" to realize this processing.\n\nThe layer found by default by SSc is of type "%s".\n\nDo you want to continue with this layer ?', ...
                cl_image.strDataType{DataTypeIn(1)}, cl_image.strDataType{DataType});
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            if rep == 2
                flag = 0;
                return
            end
        end
        
        Tag = 'BS';
        
    elseif strcmp(SonarStatus, 'BestDiagTx')
        SignatureConfigurationSonar = 1;
        %         DataType = [cl_image.indDataType('TxAngle'); cl_image.indDataType('BeamPointingAngle'); cl_image.indDataType('IncidenceAngle')];
        DataType = [cl_image.indDataType('TxAngleKjell'); cl_image.indDataType('TxAngle'); cl_image.indDataType('BeamPointingAngle'); cl_image.indDataType('IncidenceAngle')];
        [indLayerTxAngle, nomLayer, DataType] = findIndLayerSonar(this, indImage, 'DataType', DataType, ...
            'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer', 'FirstOfList', 1);
        if isempty(indLayerTxAngle)
            flag = 0;
            return
        end
        Selection(1).Name     = nomLayer; % extract_ImageName(this(indImage));
        Selection(1).TypeVal  = 'Layer';
        Selection(1).indLayer = indLayerTxAngle;
        Selection(1).DataType = DataType; % TODO :
        Tag = 'DiagTx';
        
        DataType = cl_image.indDataType('TxBeamIndexSwath');
        [indLayerTxBeamIndexSwath, nomLayer] = findIndLayerSonar(this, indImage, 'DataType', DataType, ...
            'WithCurrentImage', 1, 'OnlyOneLayer');
        if isempty(indLayerTxBeamIndexSwath)
            DataType = cl_image.indDataType('TxBeamIndex');
            [indLayerTxBeamIndexSwath, nomLayer] = findIndLayerSonar(this, indImage, 'DataType', DataType, ...
                'WithCurrentImage', 1, 'OnlyOneLayer');
            if isempty(indLayerTxBeamIndexSwath)
                flag = 0;
                return
            end
        end
        Selection(2).Name     = nomLayer; % extract_ImageName(this(indImage));
        Selection(2).TypeVal  = 'Layer';
        Selection(2).indLayer = indLayerTxBeamIndexSwath;
        Selection(2).DataType = DataType; % TODO :
    end
end
if isempty(Selection)
    return
end

%% Type de moyennage

DT = this(indImage).DataType;
if DT == cl_image.indDataType('Reflectivity') && ~strcmp(this(indImage).Unit, 'dB')
    MeanCompType = 1;
end

if selectMeanCompType || (get_LevelQuestion >= 3)
    if DT == cl_image.indDataType('Reflectivity')
        Unit = this(indImage).Unit;
        if strcmp(Unit, 'dB')
            [flag, MeanCompType] = question_MeanCompType('InitialValue', MeanCompType);
            if ~flag
                return
            end
        end
        
    elseif DT == cl_image.indDataType('IfremerQF')
        [flag, MeanCompType] = question_MeanCompType('InitialValue', MeanCompType, 'QFIfremer', 1);
        if ~flag
            return
        end
    end
end

%% Signature du mode du sonar

if SignatureConfigurationSonar
    if isempty(listeFicAll)
        that = this(indImage);
    else
        [~, nomFicThis] = fileparts(this(indImage).InitialFileName);
        flag = 0;
        for k=1:length(listeFicAll)
            [~, nomFicThat] = fileparts(listeFicAll{k});
            if strcmp(nomFicThis, nomFicThat)
                flag = 1;
                break
            end
        end
        if flag
            that = this(indImage);
        else
            Carto = this.Carto;
            allV2 = cl_simrad_all('nomFic', listeFicAll{k});
            that = get_Layer(allV2, 'Reflectivity', 'Carto', Carto); % , 'CartoAuto', 1
            %             SonarScope(that)
            suby = [];
        end
    end
    
    [flag, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, strConf, listeSuby] = paramsModeIfSeveral(that, suby, ...
        'askQuestionSignatureConfSonar', askQuestionSignatureConfSonar, 'OnlyOneConfiguration', OnlyOneConfiguration);
    if ~flag
        return
    end
    if ~isempty(listeSuby) && iscell(listeSuby) && ~isempty(listeSuby{1}) % Ceinture et bretelles et plus encore
        if ~isempty(suby)
            suby = listeSuby{1}; % N'est malheureusement pas pris en compte dans le cas d'un layer : TODO TODO TODO !!!!
        end
    end
%     
%     
%     %----------------------------------------------------------------------
%     % Test JMA le 09/02/2022 pour sondeur EM2040D du Belgica pour
%     % mod�lisation des DiagTx
%     if ~isempty(SelectMode2EM2040)
%         SelectMode = SelectMode2EM2040;
%     end
%     %----------------------------------------------------------------------
%     
%     
%     
else
    SelectMode  = [];
    SelectSwath = [];
    SelectFM    = [];
    SelectMode2EM2040 = [];
    strConf     = [];
end

%% Masque

if askQuestionMask
    DT = cl_image.indDataType('Mask');
    if any([Selection(:).DataType] == DT)
        str1 = 'TODO';
        str2 = 'WARNING : next question asks if you want to perform statistics through a mask layer. You have already used a mask layer for the cross statistics, that is OK. I suppose you do not need a second "Mask" layer (except if you are a SSc expert). So answer No to the next question.';
        my_warndlg(Lang(str1,str2), 1);
    end
    [flag, indLayerMask, valMask] = paramsMasque(this, indImage);
    if ~flag
        return
    end
%     if ~isempty(indLayerMask) && ~isempty(valMask) && (MeanCompType ~= 0) && (this(indImage).DataType == cl_image.indDataType('Reflectivity')) && strcmp(this(indImage).Unit, 'dB')
    if ~isempty(indLayerMask) && ~isempty(valMask) && (MeanCompType ~= 1) && (this(indImage).DataType == cl_image.indDataType('Reflectivity')) && strcmp(this(indImage).Unit, 'dB')
        str1 = sprintf('Attention, il semble que vous appr�tiez � calculer des courbes pour r�aliser une segmentation par courbes de BS angulaire. Si c''est bien le cas, je vous recommande de transformer l''image en amplitude avant de vous lancer dans cette op�ration.\n\n Voulez vous poursuivre ?');
        str2 = sprintf('WARNING : it seems you are ready to compute statistic curves to process an angular backscatter segmentation. If it is wright, I recommand that you transform the reflectivity image into amplitude before computing the statistics and then, to process the segmentation on this image.\n\nDo you want to continure ?');
        [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
        if ~flag || (rep == 2)
            flag = 0;
            return
        end
    end
else
    valMask = 1;
end

%% D�finition des bins

bins = [];
for k=1:length(Selection)
    if strcmp(Selection(k).TypeVal, 'Axe')
        if strcmp(Selection(k).Name(1:5), 'Axe X')
            x = get(this(indImage), 'x');
            [flag, Bins] = paramsSaisieBins('X', x(subx), [], this(indImage).XUnit, 0);
            if ~flag
                return
            end
            bins{end+1} = Bins; %#ok<AGROW>
        elseif strcmp(Selection(k).Name(1:5), 'Axe Y')
            y = get(this(indImage), 'y');
            [flag, Bins] = paramsSaisieBins('Y', y(suby), [], this(indImage).YUnit, 0);
            if ~flag
                return
            end
            bins{end+1} = Bins; %#ok<AGROW>
        end
        
    elseif strcmp(Selection(k).TypeVal, 'Layer')
        k2 = Selection(k).indLayer;
        DT = this(k2).DataType;
        if ((DT == cl_image.indDataType('TxBeamIndex')) || (DT == cl_image.indDataType('TxBeamIndexSwath'))) && SkipTxBeamIndexBins
            % Rustine pour contourner dysfonctionnement calcul des TxDiak de Kongsberg par le mode survey processing si
            % on est sur une image qui poss�de moins de secteurs : TODO : trouver une autre solution et supprimer ce if
            bins{end+1} = 1:16; %#ok<AGROW>
        else
            [Bins, ~, flag] = saisieBinsHisto(this(k2));
            if ~flag
                return
            end
            
            Bins = centrage_magnetique(Bins); % Rajout� le 19/01/2009
            
            if ~flag
                return
            end
            bins{end+1} = Bins; %#ok<AGROW>
        end
        
    elseif strcmp(Selection(k).TypeVal, 'SignalVert')
        y = get(this(indImage), Selection(k).Name);
        
        switch Selection(k).Name
            case 'SonarFrequency' % cas sp�cifique Reson 7150
                Bins = unique(y(suby));
                Bins(isnan(Bins)) = [];
            otherwise
                if isa(y, 'cl_time')
                    y = y.timeMat;
                end
                StatValues = stats(y(suby));
                [N, HistoCentralClasses] = histo1D(y(suby));
                HistoCentralClasses(N == 0) = [];
                [flag, Bins] = paramsSaisieBins(Selection(k).Name, HistoCentralClasses, StatValues, Selection(k).Unit, 0);
                if ~flag
                    return
                end
        end
        bins{end+1} = Bins; %#ok<AGROW>
    end
end

%% Boucle sur les valeurs du masque

for k=1:length(valMask)
    %{
if isempty(SelectSwath)
if isempty(SelectMode)
strNomCourbe = sprintf('%s %d', nomZoneEtude, valMask(k));
else
strNomCourbe = sprintf('Mode %d : %s %d', SelectMode, nomZoneEtude, valMask(k));
end
else
nomSwath = {'Single '; 'Double'};
if isempty(SelectMode)
strNomCourbe = sprintf('Swath : %s : %s %d', nomSwath{SelectSwath}, nomZoneEtude, valMask(k));
else
%             strNomCourbe = sprintf('Mode %d : Swath : %s : %s %d', SelectMode, nomSwath{SelectSwath}, nomZoneEtude, valMask(k));
strNomCourbe = sprintf('%s: %s %d', strConf, nomZoneEtude, valMask(k));
end
end
    %}
    %     strNomCourbe = sprintf('%s: %s %d', strConf, nomZoneEtude, valMask(k));
    if isempty(indLayerMask)
        strNomCourbe = strConf; % sprintf('%s', strConf);
    else
        strNomCourbe = sprintf('%s: Mask %d', strConf, valMask(k));
    end
    if (length(strNomCourbe) > 2) && strcmp(strNomCourbe(1:2), ': ')
        strNomCourbe = strNomCourbe(3:end);
    end
    
    % Ici appeler peut-�tre paramsStatsNameComment
    if isempty(strAppendCurveName)
        EnteteNomCourbe = strNomCourbe;
    else
        EnteteNomCourbe = sprintf('%s : %s', strNomCourbe, strAppendCurveName);
    end
    
    str1 = 'Nom de la courbe :';
    str2 = 'Name of the curve :';
    promptNomCourbe = Lang(str1,str2);
    if ~isempty(Tag)
        if isempty(EnteteNomCourbe)
            EnteteNomCourbe = Tag;
        else
            EnteteNomCourbe = [Tag ' : ' EnteteNomCourbe]; %#ok<AGROW>
        end
    end
    Comment = []; % Comment = struct2str(CurveInfo) TODO : transformer info en comment
    [flag, nomCourbek, Commentairek] = paramsStatsNameComment(this(indImage), ...
        EnteteNomCourbe, nomZoneEtude, indLayerMask, valMask(k), promptNomCourbe, ...
        'CompleteWithInitialFileName', CompleteWithInitialFileName, ...
        'Comment', Comment, 'subx', subx, 'suby', suby);
    if ~flag
        break
    end
    nomCourbe{k}   = nomCourbek; %#ok<AGROW>
    Commentaire{k} = Commentairek; %#ok<AGROW>
    drawnow
end

%% Nombre minimum de points

if get_LevelQuestion >= 3
    str1 = 'Nombre minimum de points pour valider une classe';
    str2 = 'Minimum number of points to validate a class';
    [flag, nbPMin] = inputOneParametre(Lang(str1,str2), 'Value', ...
        'Value', 10, 'Unit', 'nb', 'MinValue', 1, 'Format', '%d');
    if ~flag
        return
    end
else
    nbPMin = 10;
end
