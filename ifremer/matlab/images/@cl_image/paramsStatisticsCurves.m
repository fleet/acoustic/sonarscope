function [flag, repExport, bins, Selection, CourbeBias, nomCourbe, Comment, ...
    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType, indLayeMaskLatLong, ...
    valMask, SameReflectivityStatus, InfoCompensationCurve, nomDirMatFiles] = ...
    paramsStatisticsCurves(this, indImage, repImport, repExport, ...
    listFileNames, nomZoneEtude, subx, suby, msg, varargin)

[varargin, SonarStatus]                   = getPropertyValue(varargin, 'SonarStatus',                   []);
[varargin, SignatureConfigurationSonar]   = getPropertyValue(varargin, 'SignatureConfigurationSonar',   0);
[varargin, AskQstSameBSStatus]            = getPropertyValue(varargin, 'AskQstSameBSStatus',            1);
[varargin, AskQstComp]                    = getPropertyValue(varargin, 'AskQstComp',                    0);
[varargin, MeanCompType]                  = getPropertyValue(varargin, 'MeanCompType',                  1);
[varargin, selectMeanCompType]            = getPropertyValue(varargin, 'selectMeanCompType',            0);
[varargin, SkipTxBeamIndexBins]           = getPropertyValue(varargin, 'SkipTxBeamIndexBins',           0);
[varargin, OnlyOneConfiguration]          = getPropertyValue(varargin, 'OnlyOneConfiguration',          0);
[varargin, askQuestionSignatureConfSonar] = getPropertyValue(varargin, 'askQuestionSignatureConfSonar', 0); %#ok<ASGLU>

bins                   = [];
Selection              = [];
CourbeBias             = [];
nomCourbe              = [];
Comment                = [];
Coul                   = [];
nbPMin                 = [];
SelectMode             = [];
SelectSwath            = [];
SelectFM               = [];
indLayeMaskLatLong     = [];
valMask                = [];
SameReflectivityStatus = [];
InfoCompensationCurve  = [];
nomDirMatFiles         = [];
SelectMode2EM2040      = [];

QL = get_LevelQuestion;

%% Test si l'image visualis�e provient bien d'un sondeur Kongsberg

flag = testSignature(this(indImage), 'InitialFileFormat', 'SimradAll', 'noMessage');
if ~flag
    str1 = 'Pour lancer ce traitement il faut �tre plac� sur l''un des layers de r�flectivit� car SSc va chercher avec quels layers d''angle il doit travailler (�a d�pend de la version des datagrammes).';
    str2 = 'To launch this processing, you have to be placed on one of the Reflectivity layer in PingBeam geometry because SSc will look for which type of angles it can use (depends of the datagrams versions).';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% If Reflectivity, ask if the reflectivity has to be set as the current one (same status for BS, DiagTx, etc ...)

if testSignature(this(indImage), 'DataType', cl_image.indDataType('Reflectivity'), 'noMessage')
    if AskQstSameBSStatus
        [flag, SameReflectivityStatus] = question_SameReflectivityStatus;
        if ~flag
            return
        end
    else
        SameReflectivityStatus = 1;
    end
end

%% Courbe de compensation

CurveInfo.listFileNames = listFileNames;
if AskQstComp
    [flag, FileNameCompensation, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport);
    if ~flag
        return
    end
    InfoCompensationCurve.FileName      = FileNameCompensation;
    InfoCompensationCurve.ModeDependant = ModeDependant;
    InfoCompensationCurve.UseModel      = UseModel;
    InfoCompensationCurve.PreserveMeanValueIfModel = PreserveMeanValueIfModel;
    CurveInfo.InfoCompensationCurve = InfoCompensationCurve;
end

%% Recherche d'un masque d�fini en LatLong

% TODO : cette question pourrait �tre mutualis�e avec ALL.Params.SPFE_BatchBSCurvesAndCompMosaics

str1 = 'Voulez-vous calculer les stats au travers d''un masque d�fini en "LatLong" ?';
str2 = 'Do you want to compute the ststistics through a mask given in "LatLong" ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    identLayerMask = cl_image.indDataType('Mask');
    identLayerSegm = cl_image.indDataType('Segmentation');
    [indLayeMaskLatLong, nomsLayerMask] = findLayers(this, 'GeometryType', cl_image.indGeometryType('LatLong'), ...
        'DataType', [identLayerMask identLayerSegm]);
    if isempty(indLayeMaskLatLong)
        str1 = 'Il n''y a pas d''image de type "Mask" en g�om�trie "LatLong".';
        str2 = 'There is no image of "Mask" DataType and "LatLong" geometry".';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    
    str1 = 'S�lectionner le mask ?';
    str2 = 'Select the Mask layer';
    [rep, flag] = my_listdlg(Lang(str1,str2), nomsLayerMask, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    indLayeMaskLatLong = indLayeMaskLatLong(rep);
    
    % TODO : une seule valeur pour le moment car SimradAll_Statistics_Curves pas encore pr�t pour multi masks
    valMask = selectValMask(this(indLayeMaskLatLong), 'SelectionMode', 'Single');
    CurveInfo.MaskLatLon.nomsLayerMask = nomsLayerMask;
    CurveInfo.MaskLatLon.valMask       = valMask;
end

%% Saisie des param�tres de calcul

[flag, bins, Selection, CourbeBias, ~, ~, nomCourbe, Comment, ...
    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType] = ...
    paramsStatisticsCurve(this, indImage, nomZoneEtude, subx, suby, msg, 'SonarStatus', ...
    SonarStatus, 'CompleteWithInitialFileName', 0, 'SignatureConfigurationSonar', SignatureConfigurationSonar, ...
    'askQuestionSignatureConfSonar', askQuestionSignatureConfSonar, 'askQuestionMask', 0, ...
    'SkipTxBeamIndexBins', SkipTxBeamIndexBins, ...
    'listeFicAll', listFileNames, 'MeanCompType', MeanCompType, 'selectMeanCompType', selectMeanCompType, ...
    'CurveInfo', CurveInfo, 'OnlyOneConfiguration', OnlyOneConfiguration);
if ~flag
    return
end
nomCourbe = nomCourbe{1};

%% Nom du r�pertoire de sauvegarde des fichiers MatlabnomDirMatFiles

% if QL >= 3
%     str1 = 'Voulez-vous exporter les donn�es au format Matlab (Pour Ridha)?';
%     str2 = 'Do you want to export the data in Matlab files (For Ridha) ?';
%     [rep, flag] = my_questdlg(Lang(str1,str2));
%     if ~flag
%         return
%     end
%     if rep == 1
%         str1 = 'Nom du r�pertoire o� seront sauv�s les fichiers,';
%         %str2 = 'Name of the directory to save the files. Use the "Create a new directory" button if necessary.';
%         [flag, nomRepOut] = my_uigetdir(repExport, Lang(str1,str2));
%         if ~flag
%             return
%         end
%         repExport = nomRepOut;
%         nomDirMatFiles = nomRepOut;
%     end
% end
