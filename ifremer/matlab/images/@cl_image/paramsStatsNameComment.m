function [flag, nomCourbe, Commentaire] = paramsStatsNameComment(this, ...
    EnteteNomCourbe, nomZoneEtude, indLayerMask, valMask, ...
    promptNomCourbe, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Comment]                     = getPropertyValue(varargin, 'Comment',                     []);
[varargin, CompleteWithInitialFileName] = getPropertyValue(varargin, 'CompleteWithInitialFileName', 1);
[varargin, GUI]                         = getPropertyValue(varargin, 'GUI',                         1); %#ok<ASGLU>

Commentaire = {};
Commentaire{end+1} = sprintf('x : %s to %s', num2str(min(this.x(subx))), num2str(max(this.x(subx))));
Commentaire{end+1} = sprintf('y : %s to %s', num2str(min(this.y(suby))), num2str(max(this.y(suby))));
Commentaire{end+1} = ['InitialFileName : ' this.InitialFileName];
Commentaire = cell2str(Commentaire);

nomCourbe = '';
if ~isempty(EnteteNomCourbe)
    nomCourbe = sprintf('%s: %s', nomCourbe, EnteteNomCourbe);
end
if ~isempty(nomZoneEtude)
    nomCourbe = sprintf('%s : %s', nomCourbe, nomZoneEtude);
end
if ~isempty(indLayerMask)
    nomCourbe = sprintf('%s valMask=%d', nomCourbe, valMask);
end

if CompleteWithInitialFileName && ~isempty(this.InitialFileName)
    [nomDir1, NomFic] = fileparts(this.InitialFileName);
    [~, nomDir2] = fileparts(nomDir1);
    nomCourbe = sprintf('%s on %s', nomCourbe, fullfile(nomDir2, NomFic));
end

if (length(nomCourbe) > 2) && strcmp(nomCourbe(1:2), ': ')
    nomCourbe = nomCourbe(3:end);
end

if (length(nomCourbe) > 3) && strcmp(nomCourbe(1:3), ' : ')
    nomCourbe(1:3) = [];
end

if ~isempty(Comment)
	n = size(Comment,2);
    for k=1:size(Comment,1)
        Commentaire(end+1,1:n) = Comment(k,:); %#ok<AGROW>
    end
end

if GUI == 1
    n = max([length(nomCourbe) size(Commentaire,2) 50]);
    m = max(10, size(Commentaire,1));
    options.Resize = 'on';
    prompt   = {promptNomCourbe, Lang('Commentaire','Comment')};
    dlgTitle = Lang('Analyse statistique Inter-Images','Inter-image statistical analysis');
    cmd      = inputdlg(prompt, dlgTitle, [1 n; m n], {nomCourbe, Commentaire}, options);
    if isempty(cmd)
        flag = 0;
        nomCourbe = [];
        Commentaire = [];
    else
        flag = 1;
        nomCourbe   = cmd{1};
        Commentaire = cmd{2};
    end
    
    drawnow
else
    flag = 1;
end
