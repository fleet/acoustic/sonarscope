function [flag, func] = paramsSubbottomHeightDetection(this, TypeSignal, UnitAlgo)

func = [];
flag = isSubbottom(this);
if ~flag
    return
end

if TypeSignal == 1
    return
end

UnitImage = this.Unit;
if isempty(UnitImage)
    str1 = 'L''unit� de cette image n''est pas d�finie, le calcul de la hauteur sera fait sur la donn�e telle qu''elle est.';
    str2 = 'The Unit of the image is not defined, the height detection will be done as is.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoUnitForHeightDetection');
    return
end
UnitAlgo = lower(UnitAlgo);
if ~isempty(UnitAlgo)
    switch lower(UnitImage)
        case 'amp'
            switch UnitAlgo
                case 'amp'
                    func = [];
                case 'enr'
                    func = @pow2;
                case 'db'
                    func = @reflec_Amp2dB;
            end
            
        case 'enr'
            switch UnitAlgo
                case 'amp'
                    func = @sqrt;
                case 'enr'
                    func = [];
                case 'db'
                    func = @reflec_Enr2dB;
            end
            
        case 'db'
            switch UnitAlgo
                case 'amp'
                    func = @reflec_dB2Amp;
                case 'enr'
                    func = @reflec_dB2Enr;
                case 'db'
                    func = [];
            end
            
        otherwise
            str1 = 'L''unit� de cette image n''est pas "dB", "Amp" ou "Enr", le calcul de la hauteur sera fait sur la donn�e telle qu''elle est.';
            str2 = 'The Unit of the image is not "dB", "Amp" or "Enr", the height detection will be done as is.';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoUnitForHeightDetection');
            return
    end
end
