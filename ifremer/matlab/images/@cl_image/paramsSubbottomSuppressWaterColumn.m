function [flag, Marge] = paramsSubbottomSuppressWaterColumn(this)

Marge = 0;
flag = isSubbottom(this);
if ~flag
    return
end

str1 = 'Marge � laisser au dessus de la d�tection de hauteur.';
str2 = 'Margin over the height detection.';
[flag, Marge] = inputOneParametre(Lang(str1,str2), Lang('Marge', 'Margin'), ...
    'Value', 0, 'Unit', 'samples', 'MinValue', 0);
if ~flag
    return
end
