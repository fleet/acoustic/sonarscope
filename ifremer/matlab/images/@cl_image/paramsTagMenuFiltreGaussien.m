function [flag, Sigma] = paramsTagMenuFiltreGaussien(~, varargin)

[varargin, Sigma] = getPropertyValue(varargin, 'Sigma', 1); %#ok<ASGLU>

str1 = 'Paramétrage du filtre gaussien';
str2 = 'Gaussian filtre parametres';
p = ClParametre('Name', Lang('Ecart-Type','Standard deviation'), 'Unit', 'pixels', 'MinValue', 0.1, 'MaxValue', 10, 'Value', Sigma);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    Sigma = [];
    return
end
Sigma = a.getParamsValue;
