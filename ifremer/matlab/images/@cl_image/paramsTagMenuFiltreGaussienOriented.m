function [flag, sigma1, sigma2, angle] = paramsTagMenuFiltreGaussienOriented(~)

persistent Last_sigma1 Last_sigma2 Last_angle

if isempty(Last_sigma1)
    sigma1 = 1.5;
    sigma2 = 0.75;
    angle = 0;
else
    sigma1 = Last_sigma1;
    sigma2 = Last_sigma2;
    angle  = Last_angle;
end

str1 = 'Paramétrage du filtre gaussien orienté';
str2 = 'Oriented gaussian filtre parametres';
p    = ClParametre('Name', 'Greatest std',                'Unit', ' ',     'MinValue', 0.5, 'MaxValue',  10, 'Value', sigma1);
p(2) = ClParametre('Name', 'Lowest std',                  'Unit', ' ',     'MinValue', 0.5, 'MaxValue',  10, 'Value', sigma2);
p(3) = ClParametre('Name', 'Azimuth of the greatest std', 'Unit', 'deg',  'MinValue', 0,   'MaxValue', 360, 'Value', angle);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    sigma1 = [];
    sigma2 = [];
    angle  = [];
    return
end
X = a.getParamsValue;
sigma1 = X(1);
sigma2 = X(2);
angle  = X(3);

Last_sigma1 = sigma1;
Last_sigma2 = sigma2;
Last_angle  = angle;
