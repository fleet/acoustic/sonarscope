function [flag, alpha] = paramsTagMenuFiltreLaplacien(~)

p = ClParametre('Name', 'Alpha', 'Unit', 'pixels',  'MinValue', 0, 'MaxValue', 1, 'Value', 0.2);
a = StyledSimpleParametreDialog('params', p, 'Title', 'Laplacian filter parametres');
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    alpha = [];
    return
end
alpha = a.getParamsValue;
