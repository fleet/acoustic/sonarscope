function [flag, radius] = paramsTagMenuFiltreMoyenneDisk(~)

str1 = 'Param�tre du filtre Disk';
str2 = 'Disk filter parametres';
p = ClParametre('Name', Lang('Rayon','Radius'), 'Unit', 'pixels', 'MinValue', 1, 'MaxValue', Inf, 'Value', 5);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    radius = [];
    return
end
radius = a.getParamsValue;
