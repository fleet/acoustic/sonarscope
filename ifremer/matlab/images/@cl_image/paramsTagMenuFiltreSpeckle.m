function [flag, window, nbPixMoy, indLayerN] = paramsTagMenuFiltreSpeckle(this, indImage)

window    = [];
nbPixMoy  = 0;
indLayerN = [];

identAveragedPtsNb  = cl_image.indDataType('AveragedPtsNb');
GeometryType = this(indImage).GeometryType;
if (GeometryType == cl_image.indGeometryType('PingSamples')) || ...
   (GeometryType == cl_image.indGeometryType('PingRange'))|| ...
   (GeometryType == cl_image.indGeometryType('PingAcrossDist'))
    indLayerN = findIndLayerSonar(this, indImage, 'DataType', identAveragedPtsNb);
    if isempty(indLayerN)
        str1 = sprintf('Ce traitement s''attend � trouver un layer "AveragedPtsNb"\nCe layer n''a pas �t� trouv�, vous �tes donc pri�s de fournir vous-m�me le nombre de Looks c.a.d le nombre de pixels bruts moyenn�s pour chaque pixel de cette image.');
        str2 = sprintf('This processing is expecting a "AveragedPtsNb" layer.\nThis layer was not found, you have to give by ourself the number of "Looks" (the number of raw pixels averaged by pixel on your image).');
        my_warndlg(Lang(str1,str2), 0);

        SonarRawDataResol = get(this(indImage), 'SonarRawDataResol');
        if GeometryType == cl_image.indGeometryType('PingSamples')
            SonarResolutionD = get(this(indImage), 'SonarResolutionD');
            nbPixMoy = SonarResolutionD / SonarRawDataResol;
        elseif GeometryType == cl_image.indGeometryType('PingRange')
            SonarResolutionD = get(this(indImage), 'SonarResolutionD');
            nbPixMoy = SonarResolutionD / SonarRawDataResol;
        elseif GeometryType == cl_image.indGeometryType('PingAcrossDist')
            SonarResolutionX = get(this(indImage), 'SonarResolutionX');
            nbPixMoy = SonarResolutionX / SonarRawDataResol;
        end
        nbPixMoy = floor(nbPixMoy);
    end
else
    nbPixMoy = 1;
end

if ~isempty(indLayerN)
    [flag, window] = saisie_window([7 7]);
    if ~flag
        return
    end
else
    
    str1 = 'Param�tre du filtre de speckle';
    str2 = 'Speckle filtering parametres';
    p    = ClParametre('Name', Lang('Hauteur', 'Height'), ...
        'Unit', 'pix', 'Value', 7, 'MinValue', 1, 'MaxValue', 11);
    p(2) = ClParametre('Name', Lang('Largeur', 'Width'), ...
        'Unit', 'pix', 'Value', 7, 'MinValue', 1, 'MaxValue', 11);
    p(3) = ClParametre('Name', Lang('Nombre de Looks', 'Number of Looks'), ...
        'Value', nbPixMoy, 'MinValue', 1);
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    value = a.getParamsValue;
    
    window = value(1:2);
    nbPixMoy = value(3);
end
