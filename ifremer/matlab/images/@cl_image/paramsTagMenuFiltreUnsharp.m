function [flag, alpha] = paramsTagMenuFiltreUnsharp(~)

str1 = 'TODO';
str2 = 'Unsharp parametres';
p = ClParametre('Name', 'Alpha', 'Unit', 'pixels',  'MinValue', 0, 'MaxValue', 1, 'Value', 0.2);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    alpha = [];
    return
end
alpha = a.getParamsValue;
