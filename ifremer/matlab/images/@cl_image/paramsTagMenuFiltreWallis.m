function [flag, window, alpha, beta, moy_ref, ect_ref] = paramsTagMenuFiltreWallis(~)

str1 = 'Paramétrage du filtre Wallis';
str2 = 'Wallis parametres';
p    = ClParametre('Name', Lang('Hauteur',     'Height'),    'Unit', 'Pixel', 'MinValue', 3,    'MaxValue',   9, 'Value', 5);
p(2) = ClParametre('Name', Lang('Largeur',     'Width'),     'Unit', 'Pixel', 'MinValue', 3,    'MaxValue',   9, 'Value', 5);
p(3) = ClParametre('Name', Lang('Alpha',       'Alpha'),     'Unit', ' ',     'MinValue', 0,    'MaxValue',   1, 'Value', 0.8);
p(4) = ClParametre('Name', Lang('Beta',        'Beta'),      'Unit', ' ',     'MinValue', 0,    'MaxValue',   1, 'Value', 0);
p(5) = ClParametre('Name', Lang('Moyenne ref', 'Ref mean'),  'Unit', ' ',     'MinValue', -Inf, 'MaxValue', Inf, 'Value', 128);
p(6) = ClParametre('Name', Lang('Sigma Ref',   'Sigma Ref'), 'Unit', ' ',     'MinValue', -Inf, 'MaxValue', Inf, 'Value', 50);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    window  = [];
    alpha   = [];
    beta    = [];
    moy_ref = [];
    ect_ref = [];
    return
end
X = a.getParamsValue;
window  = X(1:2);
alpha   = X(3);
beta    = X(4);
moy_ref = X(5);
ect_ref = X(6);
