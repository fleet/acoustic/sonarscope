function [flag, binsImage, subDep, subDir, subPar] = paramsTexturalParameters(this)

subDep = [];
subDir = [];
subPar = [];

HistoCentralClasses = get(this, 'HistoCentralClasses');
HistoCentralClasses = HistoCentralClasses(:); % Pour �viter cl_memmapfile
binsImage.CLim = [HistoCentralClasses(1) HistoCentralClasses(end)];
binsImage.NbNiveaux = 64;
if (binsImage.CLim(1) > 0) && (binsImage.CLim(2) > 0)
    MinVal = 2 ^ floor(log2(binsImage.CLim(1))/log(64));
    MaxVal = 2 ^ ceil(log2(binsImage.CLim(2)));
elseif (binsImage.CLim(1) < 0) && (binsImage.CLim(2) < 0)
    MaxVal = -2 ^ floor(log2(-binsImage.CLim(2))/log(64));
    MinVal = -2 ^ ceil(log2(-binsImage.CLim(1)));
else
    MinVal = -(2 ^ ceil(log(-binsImage.CLim(1))));
    MaxVal = 2 ^ ceil(log(binsImage.CLim(2)));
end

% if (binsImage.CLim(1) > 0) && (binsImage.CLim(2) > 0)
%     MinVal = 64 * floor(log(binsImage.CLim(1))/log(64));
%     MaxVal = 64 * ceil(log(binsImage.CLim(2))/log(64));
% elseif (binsImage.CLim(1) < 0) && (binsImage.CLim(2) < 0)
%     MaxVal = -(64 * ceil(log(-binsImage.CLim(1))/log(64)));
%     MinVal = -(64 * floor(log(-binsImage.CLim(2))/log(64)));
% else
%     MinVal = -(64 * ceil(log(-binsImage.CLim(1))/log(64)));
%     MaxVal = 64 * ceil(log(binsImage.CLim(2))/log(64));
% end

%%

Unit = this.Unit;

str1 = 'Param�tres de quantification';
str2 = 'Quantization parameters';
p    = ClParametre('Name', Lang('Valeur min','Min value'), ...
    'Unit', Unit,  'MinValue', MinVal, 'MaxValue', MaxVal, 'Value', binsImage.CLim(1));
p(2) = ClParametre('Name', Lang('Valeur max','Max value'), ...
    'Unit', Unit,  'MinValue', MinVal, 'MaxValue', MaxVal, 'Value', binsImage.CLim(2));
p(3) = ClParametre('Name', Lang('Nombre de niveaux','Number of levels'), ...
    'Unit', ' ',  'MinValue', 32, 'MaxValue', 256, 'Value', 64, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
binsImage.CLim      = val(1:2);
binsImage.NbNiveaux = val(3);

%%

nomVar = {'Distances (Ex : 1:10, [1 2 3 4 6], etc, ...)'
    'Directions (Ex: 1, 1:4, 1, [1 3], etc ...)'};
value = {'1:10' ; '1'};
[value, flag] = my_inputdlg(nomVar, value, 'Entete', 'IFREMER - SonarScope : Textural analysis parameters');
if ~flag
    return
end
subDep = str2num(value{1}); %#ok
subDir = str2num(value{2}); %#ok
X = get(cl_cooc([]), 'NomParams');
for i=1:length(X)
    NomParams{i} = func2str(X{i}); %#ok
end
[subPar, flag] = my_listdlg(Lang('Param�tres texturaux', 'Textural parameters'), NomParams);
if ~flag
    return
end
