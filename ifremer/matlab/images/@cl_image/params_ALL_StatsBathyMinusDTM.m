function [flag, CleanCurves] = params_ALL_StatsBathyMinusDTM(this)

CleanCurves   = [];

%% Checking if the current image is a DTM in LatLong geometry

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

%% CleanCurves

str1 = 'Voulez-vous nettoyer les courbes ?';
str2 = 'Do you want to clean up the curves ?';
[ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
CleanCurves = (ind == 1);
