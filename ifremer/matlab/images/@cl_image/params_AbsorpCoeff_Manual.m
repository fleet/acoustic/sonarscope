function [flag, AlphaUser] = params_AbsorpCoeff_Manual(this)

AlphaUser = [];

%% Saisie des coefficients d'att��nuation pour quelques fr�quences

% TODO : on pourrait mieux faire si on avait acc�s directement aux fr�quences des secteurs

SonarDescription = get(this, 'SonarDescription');
AlphaUser.Freq(1) = get(SonarDescription, 'Sonar.Freq');
AlphaUser.Freq(2) = AlphaUser.Freq(1);
AlphaUser.Alpha(1:2) = NaN;

OK = 0;
while ~OK
    str1 = 'TODO';
    str2 = 'dsqsqsdqsdqsd';
    p(1) = ClParametre('Name', Lang('Fr�quence', 'Frequency'), ...
        'Unit', 'kHz',  'MinValue', 1, 'MaxValue', 1000, 'Value', AlphaUser.Freq(1));
    p(2) = ClParametre('Name', Lang('Fr�quence', 'Frequency'), ...
        'Unit', 'kHz',  'MinValue', 1, 'MaxValue', 1000, 'Value', AlphaUser.Freq(2));
    p(3) = ClParametre('Name', Lang('Coeff d''absorbtion', 'Absorption coefficient'), ...
        'Unit', 'dB/km/m',  'MinValue', 0, 'MaxValue', 500, 'Value', AlphaUser.Alpha(1));
    p(4) = ClParametre('Name', Lang('Coeff d''absorbtion', 'Absorption coefficient'), ...
        'Unit', 'dB/km/m',  'MinValue', 0, 'MaxValue', 500, 'Value', AlphaUser.Alpha(2));
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 -1 -2 -1 -1 0 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;
    AlphaUser.Freq  = val(1:2);
    AlphaUser.Alpha = val(3:4);
        
    if val(1) == val(2)
        str1 = 'Les deux fr�quences doivent �tre diff�rentes.';
        str2 = 'The two frequencies have non equal values.';
        my_warndlg(Lang(str1,str2) , 1);
    else
        if isnan(val(3)) || isnan(val(4))
            str1 = 'Les valeurs d''at�nuation ne peuvent pas �tre NaN.';
            str2 = 'The absoption values cannot be NaN.';
            my_warndlg(Lang(str1,str2) , 1);
        else
            OK = 1;
        end
    end
end
