function [flag, nomDirOut, nomFicOut, ColormapIndex, Video, Compress, repExport] ...
    = params_BOB_Export3DViewer(this, listFileNames, repExport)

persistent persistent_ColormapIndex
persistent persistent_Video
persistent persistent_Compress

nomDirOut     = [];
ColormapIndex = [];
Video         = [];
Compress      = [];

%% Select directory

[~, nomFicSeul] = fileparts(listFileNames{1});
str1 = 'Choisissez ou cr�ez le nom du fichier et le r�pertoire o� seront stock�s les donn�es ';
str2 = 'Select or create the file and directory in which the data will be created.';
% [flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
[flag, nomFicOut] = my_uiputfile('*.xml',Lang(str1,str2),fullfile(repExport, nomFicSeul)) ;
if ~flag
    return
end
[nomFicOut, nomDirOut] = fileparts(nomFicOut);
repExport = nomDirOut;

%% S�lection de la table de couleurs

if isempty(persistent_ColormapIndex)
    ColormapIndex = 2;
else
    ColormapIndex = persistent_ColormapIndex;
end
[flag, ColormapIndex] = edit_ColormapIndex(this, 'ColormapIndex', ColormapIndex);
if ~flag
    return
end
persistent_ColormapIndex = ColormapIndex;

%% Choix du mode d'export - compress� ou non.
if isempty(persistent_Compress)
    Compress = 1;
else
    Compress = persistent_Compress;
end
strFR = 'Voulez-vous comprimer en .ifr la donn�e en sortie de cha�ne ?';
strUS = 'Do you want to compress data (.ifr format) ?';

[rep, flag] = my_questdlg(Lang(strFR, strUS), 'Init', Compress);
if ~flag
    return
else
    % Sous forme logique.
    Compress = rep-1;
    Compress = ~Compress;
end
persistent_Compress = Compress+1;
%
%% Video
if isempty(persistent_Video)
    Video = 1;
else
    Video = persistent_Video;
end
[flag, Video] = edit_Video(this, 'Video', Video);
if ~flag
    return
end
persistent_Video = Video;
