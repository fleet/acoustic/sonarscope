function [flag, NameEllipsoidOut] = params_Carto_ChangeEllipsoid(this)

NameEllipsoidOut = [];

%% Test si image en g�om�trie LatLon

flag = testSignature(this, 'GeometryType',  cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Choix de la'ellipsoid de destination

NameEllipsoidOut = 1;
return

EllipsoidNames = {'wgs84'; 'grs80'; 'clarke80'; 'wgs72'; 'wgs66'; 'international'; 'clarke66'; 'everest'; 'bessel'; 
    'krasovsky'; 'iau65'; 'airy1830'; 'airy1849'; 'wgs60'; 'iau68'};
[rep, flag] = my_listdlg('New Ellipsoid', EllipsoidNames, 'SelectionMode', 'Single', 'InitialValue', 1);
if ~flag
    return
end
NameEllipsoidOut = EllipsoidNames{rep};

% Test si l'eelipsoide actuelle est diff�rente de l'ellipsoide de destination

% TODO
% Carto = this.Carto