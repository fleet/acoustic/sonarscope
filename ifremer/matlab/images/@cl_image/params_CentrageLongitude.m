function [flag, CentralLongitude] = params_CentrageLongitude(this)

CentralLongitude = 0;

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

str1 = 'Recentrage de l''image : Offsets on axis';
str2 = 'Center image : Offsets on axis';
[flag, CentralLongitude] = inputOneParametre(Lang(str1,str2), Lang('Longitude centrale', 'Central longitude'), ...
    'Value', 180, 'Unit', 'deg', 'MinValue', -180, 'MaxValue', 360);
if ~flag
    return
end
