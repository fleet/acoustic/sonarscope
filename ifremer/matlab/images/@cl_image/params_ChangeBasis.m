function [flag, geoX_PingBeam, geoY_PingBeam, OriginPolynome, NomFicPolynome, subLayers, gridSize, repImport] ...
    = params_ChangeBasis(this, indImage, subx, suby, repImport)

persistent gridSize_persistent

geoX_PingBeam  = [];
geoY_PingBeam  = [];
OriginPolynome = [];
NomFicPolynome = [];
subLayers      = [];
gridSize       = [];

DT = cl_image.indDataType('Bathymetry');
flag = testSignature(this(indImage), 'DataType', DT, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% V�rification d'existence d'un layer de latitude

identLayerLat = cl_image.indDataType('Latitude');
[indLayerLat, nomLayersLatitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLat);
if length(indLayerLat) > 1
    str1 = 'Il existe plusieurs images de type "Latitude", la quelle voulez-vous utiliser ?';
    str2 = 'Many "Latitude" layers exist. Which one do you want to use ?';
    [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLatitude, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    indLayerLat = indLayerLat(rep);
end

%% V�rification d'existence d'un layer de longitude

identLayerLon = cl_image.indDataType('Longitude');
[indLayerLon, nomLayersLongitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLon);
if length(indLayerLon) > 1
    str1 = 'Il existe plusieurs images de type "Longitude", la quelle voulez-vous utiliser ?';
    str2 = 'Many "Longitude" layers exist. Which one do you want to use ?';
    [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLongitude, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    indLayerLon = indLayerLon(rep);
end


if isempty(indLayerLat) || isempty(indLayerLon)
    [flag, a] = sonarCalculCoordGeo(this, indImage, 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
    geoY_PingBeam = a(1);
    geoX_PingBeam = a(2);
    clear a
else
%     geoY_PingBeam = this(indLayerLat);
%     geoX_PingBeam = this(indLayerLon);

    % ---------------------
    % Extraction des images

    x = get(this(indImage), 'x');
    y = get(this(indImage), 'y');
    x = x(subx);
    y = y(suby);
    geoY_PingBeam = extraction(this(indLayerLat), 'x', x, 'y', y);
    geoX_PingBeam = extraction(this(indLayerLon), 'x', x, 'y', y);
end

[flag, a] = sonar_coordXY(this(indImage), geoY_PingBeam, geoX_PingBeam, 'subx', subx, 'suby', suby);
if flag
    geoY_PingBeam = a(2);
    geoX_PingBeam = a(1);
end

%% Origine du polynome

str1 = 'Origine du polynome ?';
str2 = 'Polynome origin ?';
[OriginPolynome, flag] = my_questdlg(Lang(str1,str2), ...
    Lang('Estim� sur la donn�e', 'Estimated on the data'), ...
    Lang('Donn� dans un fichier', 'Given in a file'));
if ~flag
    return
end

if OriginPolynome == 1
    [flag, NomFicPolynome] = my_uiputfile('*.mat', 'Polynome file', fullfile(repImport, 'coeff_plane.mat'));
    if ~flag
        return
    end
    
else
    [flag, NomFicPolynome, lastDir] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport);
    if ~flag
        return
    end
    repImport = lastDir;
    NomFicPolynome = NomFicPolynome{1};
end

%% Liste des autres layers � projeter

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage);
if ~flag
    return
end
[choix, flag] = my_listdlg(Lang('Layers � exporter', 'Select layers to export'), ...
    nomsLayers);
if ~flag
    return
end
subLayers = indLayers(choix);

%% Pas de la grille

if isempty(gridSize)
    gridSize = 0;
else
    gridSize = gridSize_persistent;
end

str1 = 'Pas de grille';
str2 = 'Grid size';
p    = ClParametre('Name', Lang('Pas de grille', 'Grid size'), ...
    'Unit', 'm', 'Value', gridSize, 'MinValue', 0.01, 'MaxValue', 2);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
gridSize = a.getParamsValue;

gridSize_persistent = gridSize;
