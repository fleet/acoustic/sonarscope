function [flag, OriginPolynome, nomFic, pX, pY, repImport] = params_CompensationPolynome(this, TypePolynome, repImport)

OriginPolynome = [];
nomFic = [];
pX = [];
pY = [];

% Test si Image d'intensit�
flag = testSignature(this, 'ImageType', 1);
if ~flag
    return
end

if TypePolynome == 1 % Plan
    str1 = 'Origine du plan ?';
    str2 = 'Tilted plan origin ?';
else % Polynome
    str1 = 'Origine du polynome ?';
    str2 = 'Polynome origin ?';
end

[OriginPolynome, flag] = my_questdlg(Lang(str1,str2), ...
    Lang('Estim� sur la donn�e', 'Estimated on the data'), ...
    Lang('Donn� dans un fichier', 'Given in a file'));
if ~flag
    return
end

if OriginPolynome == 1
    if TypePolynome == 2
        if get_LevelQuestion >= 3
            ValMax = 8;
        else
            ValMax = 4;
        end
        
        p    = ClParametre('Name', Lang('Ordre du polynome en X', 'Order of the polynome in X'), ...
            'Value', 2, 'MinValue', 0, 'MaxValue', ValMax);
        p(2) = ClParametre('Name', Lang('Ordre du polynome en Y', 'Order of the polynome in Y'), ...
            'Value', 2, 'MinValue', 0, 'MaxValue', ValMax);
        a = StyledSimpleParametreDialog('params', p, 'Title', 'Polynomial compensation parametres');
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        val = a.getParamsValue;
 
        pX = val(1);
        pY = val(2);
        
        [flag, nomFic] = my_uiputfile('*.mat', 'Polynome file', fullfile(repImport, 'coeff_poly.mat'));
        if ~flag
            return
        end
    else
        [flag, nomFic] = my_uiputfile('*.mat', 'Polynome file', fullfile(repImport, 'coeff_plane.mat'));
        if ~flag
            return
        end
    end
else
    [flag, nomFic, lastDir] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport);
    if ~flag
        return
    end
    repImport = lastDir;
    nomFic = nomFic{1};
end
