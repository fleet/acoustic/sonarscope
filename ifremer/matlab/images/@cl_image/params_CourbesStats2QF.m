function [flag, subBathy, subResidus] = params_CourbesStats2QF(this)   

subResidus = [];

%% Courbe de stats sur la Bathy

str1 = 'S�lection de la courbe de stats de la bathym�trie';
str2 = 'Select the curve on the real bathymetry';
[flag, subBathy]  = selectionCourbesStats(this, 'DataTypeValue', 'Bathymetry', 'SelectionMode', 'Single', 'Title', Lang(str1,str2));
if ~flag || isempty(subBathy)
    return
end

%% Courbe de ststs sur les r�sidus

str1 = 'Avez-vous une courbe de stats des r�sidus (Bathy moins Mod�le) ?';
str2 = 'Do you have a curve on the residuals (Bathy minus Model) ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end

if rep == 1
    str1 = 'S�lection de la courbe de stats des r�sidus (Bathy moins Mod�le)';
    str2 = 'Select the curve on the residuals (Bathy minus Model)';
    [flag, subResidus]  = selectionCourbesStats(this, 'DataTypeValue', 'Bathymetry', 'SelectionMode', 'Single', 'Title', Lang(str1,str2));
    if ~flag || isempty(subResidus)
        flag = 0;
        return
    end
    
    % TODO : faudrait v�rifier si ce sont les m�me axes, m�me donn�es
    % conditionnelles, etc ...
    
end
