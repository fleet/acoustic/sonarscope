function  [flag, signal, IndLayerIndexation] = params_CreateLayerFromSignals(this, indImage)

signal = ClSignal.empty();
IndLayerIndexation = [];

%% S�lection du signal

[rep, flag] = my_listdlg('Liste of items', {this(indImage).SignalsVert.name}, 'SelectionMode', 'Single');
if ~flag
    return
end
signal = this(indImage).SignalsVert(rep);
nomVarLayerIndexe = signal.link;

%% Recherche du layer d'indexation

if ~signal.isLinkedSignal()
    return
end

DT = cl_image.indDataType(nomVarLayerIndexe);
[IndLayerIndexation, nomLayer, DataTypeOut] = findIndLayerSonar(this, indImage, 'DataType', DT, 'WithCurrentImage', 1, ...
    'OnlyOneLayer', 'AtLeastOneLayer'); %#ok<ASGLU>
