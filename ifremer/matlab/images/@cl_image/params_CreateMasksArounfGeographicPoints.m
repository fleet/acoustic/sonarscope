function [flag, nomFicIn, nomDirOut, typeFrame, widthFrame, gridSize, repImport, repExport] = params_CreateMasksArounfGeographicPoints(~, repImport, repExport)

persistent persistent_typeFrame persistent_widthFrame persistent_gridSize

nomDirOut  = [];
typeFrame  = [];
widthFrame = [];
gridSize   = [];

%% Message � l'utilisateur

str1 = 'Exemples de fichier attendu.';
str2 = 'Expected file examples.';
str{1} = Lang(str1,str2);
str{end+1} = ' ';
str{end+1} = 'Name;Longitude;Latitude';
str{end+1} = 'G02;2.8909637;51.23638';
str{end+1} = 'G03;2.8836996;51.233941';
str{end+1} = '...';

my_warndlg(cell2str(str), 1); % Apr�s modif 28/04/2014
clear str

%% S�lection des fichiers d'entr�e

[flag, nomFicIn, repImport] = uiSelectFiles('ExtensionFiles', '.txt', 'AllFilters', 1, 'RepDefaut', repImport);
if ~flag
    return
end

%% S�lection des r�pertoire de sortie

[flag, nomDirOut] = my_uigetdir(repExport, 'Enter a directory that contains ErMapper files');
if ~flag
    return
end
repExport = nomDirOut;

%% typeFrame

if isempty(persistent_typeFrame)
    typeFrame = 1;
else
    typeFrame = persistent_typeFrame;
end

str = {'Circle'; 'Square'};
[rep, flag] = my_listdlg('Liste of items', str, 'SelectionMode', 'Single', 'InitialValue', typeFrame);
if ~flag
    return
end
typeFrame = str{rep};
persistent_typeFrame = typeFrame;

%% Largeur de la frame

if isempty(persistent_widthFrame)
    widthFrame = 100;
else
    widthFrame = persistent_widthFrame;
end

if isempty(persistent_gridSize)
    gridSize = 1;
else
    gridSize = persistent_gridSize;
end

str = {'Radius'; 'Width'};
p    = ClParametre('Name', str{rep},    'Unit', 'm', 'MinValue', 1,   'MaxValue', 10000, 'Value', widthFrame);
p(2) = ClParametre('Name', 'grid size', 'Unit', 'm', 'MinValue', 0.1, 'MaxValue', 100,   'Value', gridSize);
a = StyledParametreDialog('params', p, 'Title', 'Mask caracteristics');
a.sizes = [0 -2 0 0 0 -2 -1 0];
a.openDialog;
if ~flag
    return
end
params = a.getParamsValue;
widthFrame = params(1);
gridSize   = params(2);

persistent_widthFrame = widthFrame;
persistent_gridSize   = gridSize;
