function [flag, step] = params_DerivedFunctionsGreatestSlopeDirection(this, subx, suby)

if isempty(subx)
    subx = 1:length(this.x);
end
if isempty(suby)
    suby = 1:length(this.y);
end

stepx = max(1, ceil(length(subx) / 100));
stepy = max(1, ceil(length(suby) / 100));
step = min(stepx, stepy);

p = ClParametre('Name', 'Sampling rate', 'MinValue', 1, 'MaxValue', step * 2, 'Value', step, 'Unit', 'Pixels', 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', 'Subsampling of the quiver image');
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
if ~a.okPressedOut
    flag = 0;
    return
end
step = a.getParamsValue();

flag = 1;
