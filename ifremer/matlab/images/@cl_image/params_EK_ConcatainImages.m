function [flag, listeImages] = params_EK_ConcatainImages(this, indImage)

listeImages = indImage;
nomsLayers  = {extract_ImageName(this(indImage))};

DT  = this(indImage).DataType;
GT  = this(indImage).GeometryType;
IFF = this(indImage).InitialFileFormat;

for k=1:length(this)
    if k ~= indImage
        a = this(k);
        if (a.GeometryType == GT) && (a.DataType == DT) && strcmp(a.InitialFileFormat, IFF)
            listeImages(end+1) = k; %#ok<AGROW>
            nomsLayers{end+1,1}  = extract_ImageName(a); %#ok<AGROW>
        end
    end
end

[flag, sub] = uiSelectImages('ExtensionImages', nomsLayers);
if ~flag
    return
end
listeImages = listeImages(sub);
