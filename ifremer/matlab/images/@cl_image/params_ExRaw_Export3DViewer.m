function [flag, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, ...
    HauteurMin, SuppressionOverHeight, identLayer, listFreq, TailleMax, MeanTide, typeOutputFormat, repExport] ...
    = params_ExRaw_Export3DViewer(this, listFileNames, repExport)

persistent persistent_CLimAmp persistent_CLimPhase persistent_ColormapIndex persistent_HauteurMin persistent_MeanTide

C0 = cl_ExRaw([]);

nomDirOut             = [];
CLim                  = [];
CLimNaN               = [];
Tag                   = [];
ColormapIndex         = [];
HauteurMin            = 0;
SuppressionOverHeight = true;
MeanTide              = 0;
TailleMax             = [];
typeOutputFormat      = [];

%% Type de donn�es � traiter

[flag, listFreq, identLayer, nomLayers] = selectionLayers(C0, listFileNames{1});
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Supression des donn�es au del� de la d�tection de hauteur

str1 = 'Suppression des donn�es au del� de la hauteur d�tect�e ?';
str2 = 'Remove data beyond the detected height ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
SuppressionOverHeight = (rep == 1);


%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% Type de fichier en sortie

str = {'XML-Bin'; 'Netcdf'};
[typeOutputFormat, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
if typeOutputFormat == 2
    return
end

%% Bornes de rehaussement de contraste

for k=1:length(nomLayers)
    switch identLayer(k)
        case 1 % 'Power'
            if isempty(persistent_CLimAmp)
                minVal   = -160;
                maxVal   = -30;
                SeuilInf = -Inf;
                SeuilSup = Inf;
            else
                minVal   = persistent_CLimAmp(1);
                maxVal   = persistent_CLimAmp(2);
                SeuilInf = persistent_CLimAmp(3);
                SeuilSup = persistent_CLimAmp(4);
            end
            Unit = 'dB';
            
        case 2 % 'Sp' % TODO : revoir les valeurs
            if isempty(persistent_CLimAmp)
                minVal   = -160;
                maxVal   = -30;
                SeuilInf = -Inf;
                SeuilSup = Inf;
            else
                minVal   = persistent_CLimAmp(1);
                maxVal   = persistent_CLimAmp(2);
                SeuilInf = persistent_CLimAmp(3);
                SeuilSup = persistent_CLimAmp(4);
            end
            Unit = 'dB';
            
        case 3 % 'Sv'
            if isempty(persistent_CLimAmp)
                minVal   = -100;
                maxVal   = -40;
                SeuilInf = -Inf;
                SeuilSup = Inf;
            else
                minVal   = persistent_CLimAmp(1);
                maxVal   = persistent_CLimAmp(2);
                SeuilInf = persistent_CLimAmp(3);
                SeuilSup = persistent_CLimAmp(4);
            end
            Unit = 'dB';
            
        otherwise
            if isempty(persistent_CLimPhase)
                minVal   = -10;
                maxVal   = 10;
                SeuilInf = -Inf;
                SeuilSup = Inf;
            else
                minVal   = persistent_CLimPhase(1);
                maxVal   = persistent_CLimPhase(2);
                SeuilInf = persistent_CLimPhase(3);
                SeuilSup = persistent_CLimPhase(4);
            end
            Unit = 'deg';
    end
    
    if isempty(persistent_HauteurMin)
        HauteurMin = 0;
    else
        HauteurMin = persistent_HauteurMin;
    end
    
    str1 = 'Valeur Max';
    str2 = 'Max value';
    str3 = 'Valeur Min';
    str4 = 'Min value';
    str5 = 'Transparence au dessus de';
    str6 = 'Set to NaN over';
    str7 = 'Transparence en dessous de ';
    str8 = 'Set to NaN under';
    str9 = 'Suppression des premiers �chos sur';
    str10 = 'Remove first echos on';
    p    = ClParametre('Name', Lang(str1,str2), 'Unit',  Unit, 'Value', maxVal);
    p(2) = ClParametre('Name', Lang(str3,str4), 'Unit',  Unit, 'Value', minVal);
    p(3) = ClParametre('Name', Lang(str7,str8), 'Unit',  Unit, 'Value', SeuilSup);
    p(4) = ClParametre('Name', Lang(str5,str6), 'Unit',  Unit, 'Value', SeuilInf);
    p(5) = ClParametre('Name', Lang(str9,str10), 'Unit', 'm', 'Value', HauteurMin);
    str1 = sprintf('Bornes de rehaussement de contraste : %s', nomLayers{k});
    str2 = sprintf('Limits of the contrast enhancement : %s',  nomLayers{k});
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    Value = a.getParamsValue;
    CLim(k,:)    = Value([2 1]); %#ok<AGROW>
    CLimNaN(k,:) = Value([4 3]); %#ok<AGROW>
    HauteurMin   = Value(5);
    
    switch nomLayers{k}
        case {'Power'; 'Sv'; 'Sp'}
            persistent_CLimAmp   = [CLim(k,:) CLimNaN(k,:)];
        otherwise
            persistent_CLimPhase = [CLim(k,:) CLimNaN(k,:)];
    end
end
persistent_HauteurMin = HauteurMin;

%% S�lection de la table de couleurs

if isempty(persistent_ColormapIndex)
    ColormapIndex = 3;
else
    ColormapIndex = persistent_ColormapIndex;
end
[flag, ColormapIndex] = edit_ColormapIndex(this, 'ColormapIndex', ColormapIndex);
if ~flag
    return
end
persistent_ColormapIndex = ColormapIndex;

%% Saisie d'une mar�e moyenne si elle n'est pas trouv�e dans les fichiers

if isempty(persistent_MeanTide)
    MeanTide = 0;
else
    MeanTide = persistent_MeanTide;
end
str1 = 'Correction unique de mar�e au cas o� cette information ne serait pas trouv�e dans les fichiers.';
str2 = 'Unique tide correction if ever this data was not found in the files.';
[flag, MeanTide] = inputOneParametre(Lang(str1,str2), Lang('Valeur Min', 'Min value'), ...
    'Unit', 'm', 'Value', MeanTide, 'MinValue', 0, 'MaxValue', 16);
if ~flag
    return
end
persistent_MeanTide = MeanTide;

%% Taille max des images support�e par le GLOBE 
% D�pend des caract�ristiques de la carte graphique de la machine

pppp = opengl('data');
str1 = 'Width/Height max size pixels (Cf. Graphic Card)';
str2 = 'Width/Height max size pixels (Cf. Graphic Card)';
[flag, TailleMax] = inputOneParametre(Lang(str1,str2), Lang('Valeur Min', 'Min value'), ...
    'Unit', 'pixels', 'Value', pppp.MaxTextureSize, 'MinValue', 1, 'MaxValue', pppp.MaxTextureSize);
if ~flag
    return
end
