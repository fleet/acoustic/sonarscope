function [flag, indexTypeLayer, indexSelectLayer, nomFic, MultiTime, repExport] ...
    = params_Export3DViewerV3(this, indImage, indLayers, repExport)

indexTypeLayer   = [];
indexSelectLayer = [];
flag             = 0; %#ok<NASGU>
MultiTime        = 0;

% flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('LatLong'));
% if ~flag
%     return
% end

ImageName = extract_ImageName(this(indImage));
filtreXml = fullfile(repExport, [ImageName '.xml']);
[flag, nomFic] = my_uiputfile('*.xml', 'Give a file name', filtreXml);
if ~flag
    return
end

% Identification des types de layers recens�s pour d�signation comme
% layerIndex.
pppp = this(indLayers);
pppp = pppp(:);
clear str
for k=1:length(pppp)
    dataType(k) = pppp(k,1).DataType; %#ok<AGROW>
    % Reconnaissance comme type Index pour l'instant de certains types de donn�es de SSC.
    listLayer(k).type = cl_image.strDataType{dataType(k)}; %#ok<AGROW>
    listLayer(k).name = pppp(k,1).Name; %#ok<AGROW>
    % On suppose comme donn�e Index les Distance Oblique, Tx BeamIndex ou Tx Beam Angle.
    if dataType(k) == 7 || dataType(k) == 28 || dataType(k) == 88
        listLayer(k).Index  = 1; %#ok<AGROW>
    else
        listLayer(k).Index  = 0; %#ok<AGROW>
    end
end
[flag, selectLayer] = uiTableLayer(listLayer);

if isempty(selectLayer)
    return
end
% On retient comme index que ceux not�s comme tel ET S�ctionn�s.
indexTypeLayer   = ([selectLayer{:,4}] == 1) & ([selectLayer{:,3}] == 1);
indexSelectLayer = ([selectLayer{:,4}] == 1);

%% Choix du mode d'agr�gation des layers de donn�es.
% Possibilit� d'extraire les donn�es le long de l'�chelle des temps ou unitairement (pour les mosa�quer avec GLOBE).
% ex : GOBETAS.
% Ceci tant qu'on ne peut Mosa�quer sur le GLOBE des layers de l'�chelle de
% temps.

strFR = 'Voulez-vous consid�rer les layers sur une �chelle de temps ?';
strUS = 'Do you want to consider layers on a time scale ?';
[rep, flag] = my_questdlg(Lang(strFR, strUS));
if ~flag
    return
end
% Sous forme logique.
MultiTime = rep-1;
MultiTime = ~MultiTime;

repExport = fileparts(nomFic);
flag = 1;
