function [flag, sub, nomFicIn, nomFicOut, SounderName, repImport, repExport] = params_ExportBSCorr(this, repImport, repExport)

global IfrTbxResources %#ok<GVMIS>

nomFicIn  = [];
nomFicOut = [];

SonarDescription = get_SonarDescription(this);
SounderName = get(SonarDescription, 'Sonar.Name');

%% S�lection de la courbe � exporter

str1 = 'S�lection de la courbe contenant les mod�les de diagrammes d''antennes � injecter dans le BSCorr.txt';
str2 = 'Select the curve that contains the TxbeamDiagrams you want to export in the BSCorr.txt';
[flag, sub] = selectionCourbesStats(this, 'Tag', 'DiagTx', 'SelectionMode', 'Single', 'Title', Lang(str1,str2));
if ~flag || isempty(sub)
    flag = 0;
    return
end

%% Recherche du fichier BSCorr.txt d'entr�e le plus probable

InitialFileName = this.InitialFileName;
if isempty(InitialFileName)
    str1 = 'L''image courante ne semble pas �tre une image de r�flectivit� issue d''un sondeur Kongsberg.';
    str2 = 'The current image does not seem to be a reflectivity one coming from a Kongsberg MBES.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
    
[nomDir, nomAll] = fileparts(InitialFileName);
if ~exist(nomDir, 'dir')
    nomDir = repExport;
end
nomFicIn = fullfile(nomDir, 'BSCorr_SSc.txt');
str1 = sprintf('Test d''existence du fichier "%s"', nomFicIn);
str2 = sprintf('Test if "%s" exists', nomFicIn);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'RechercheBSCorr', 'TimeDelay', 10);

if exist(nomFicIn, 'file')
    [flag, BSCorr] = read_BSCorr(nomFicIn);
    if ~flag
        nomFicIn = [];
    end
else
    nomDirExtraParameters = fullfile(nomDir, 'SonarScope', nomAll, 'Ssc_ExtraParameters');
    listeFic = findFicInDir(nomDirExtraParameters, 'BSCorr_*.txt');
    if isempty(listeFic)
        nomDirExtraParameters = fileparts(fileparts(nomDirExtraParameters));
        listeFic = findFicInDir(nomDirExtraParameters, 'BSCorr_*.txt');
    end
    if ~isempty(listeFic)
        flag = copyfile(listeFic{1}, nomFicIn);
        if flag
            [flag, BSCorr] = read_BSCorr(nomFicIn);
            if ~flag
                nomFicIn = [];
            end
        else
            nomFicIn = [];
        end
    end
end

if exist(nomFicIn, 'file')
    str1 = sprintf('Le fichier "%s" a �t� trouv�.', nomFicIn);
    str2 = sprintf('"%s" was found.', nomFicIn);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'RechercheBSCorr', 'TimeDelay', 10);
    nomDir = [];
else
    if isempty(InitialFileName)
        flag = 0;
        return
    end
    [nomDir, nomFicIn] = fileparts(InitialFileName);
    nomFicIn = fullfile(nomDir, [nomFicIn '_BSCorr.txt']);
    str1 = sprintf('Test d''existence du fichier "%s"', nomFicIn);
    str2 = sprintf('Test if "%s" exists', nomFicIn);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'RechercheBSCorr', 'TimeDelay', 10);
    if exist(nomFicIn, 'file')
        str1 = sprintf('Le fichier "%s" a �t� trouv�.', nomFicIn);
        str2 = sprintf('"%s" was found.', nomFicIn);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'RechercheBSCorr', 'TimeDelay', 10);
        nomDir = [];
    else
        %% Cas o� aucun fichier BSCorr n'a �t� trouv�, on recherche le BSCorr dans D:\SScTbx\SScTbxResources\KongsbergBSCorr
        
        nomFicIn = sprintf('BScorr_%s.txt', SounderName(3:end));
        nomFicIn = fullfile(IfrTbxResources, 'KongsbergBSCorr', nomFicIn);
        if exist(nomFicIn, 'file')
            [flag, BSCorr] = read_BSCorr(nomFicIn);
            if ~flag
                nomFicIn = [];
            end
        else
            str1 = 'Aucun fichier BSCorr.txt attendu n''a �t� trouv�. Vous devez en trouver un par vous-m�me dans le r�pertoire .\SonarScope\xxxx\Ssc_ExtraParameters.';
            str2 = 'No expected BSCorr.txt was found. You can find one in .\SonarScope\xxxx\Ssc_ExtraParameters directory';
            %         my_warndlg(Lang(str1,str2), 0, 'Tag', 'RechercheBSCorr', 'TimeDelay', 10);
            my_warndlg(Lang(str1,str2), 1);
            nomDir = repImport;
            nomFicIn = '.txt';
        end
    end
end

str1 = 'IFREMER - SonarScope : Nom du fichier BSCorr.txt de r�f�rence';
str2 = 'IFREMER - SonarScope : Name of the reference BSCorr.txt file';
[flag, nomFicIn] = my_uigetfile(nomFicIn, Lang(str1,str2), nomDir);
if ~flag
    return
end
repImport = fileparts(nomFicIn);

%% Nom du fichier BSCorr.txt de sortie

if exist(nomFicIn, 'file')
    nomFic = nomFicIn;
else
    nomFic = fullfile(repExport, 'BSCorr_SSc.txt');
end
str1 = 'IFREMER - SonarScope : Nom du fichier BSCorr.txt modifi�';
str2 = 'IFREMER - SonarScope : Name of the corrected BSCorr.txt file';
[flag, nomFic] = my_uiputfile('*.txt', Lang(str1,str2), nomFic);
if ~flag
    return
end
[repExport, nomFic] = fileparts(nomFic);
nomFicOut = fullfile(repExport, [nomFic '.txt']);
