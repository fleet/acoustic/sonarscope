function [flag, sub, nomFicSave, repExport] = params_ExportDiagTxCurves(this, repExport)

nomFicSave = [];
 
[flag, sub, ~, NomsCourbes] = selectionCourbesStats(this, 'Tag', 'DiagTx');
if ~flag || isempty(sub)
    flag = 0;
    return
end

% nomFic = fullfile(repExport, 'DiagTxCurve.mat');
initNomCourbe = sprintf('%s', NomsCourbes{sub});
initNomCourbe = strrep(initNomCourbe, 'Whole image on', '');
initNomCourbe = strrep(initNomCourbe, '\', '_');
initNomCourbe = strrep(initNomCourbe, ':', '_');
initNomCourbe = strrep(initNomCourbe, ' ', '');
initNomCourbe = strrep(initNomCourbe, '-', '_');
nomFic = fullfile(repExport, [initNomCourbe '.mat']);
str1 = 'IFREMER - SonarScope : Nom du fichier d''export des diagramme de directivité d''émission';
str2 = 'IFREMER - SonarScope : Give a file name for TxBeamDiagram export file';
[flag, nomFic] = my_uiputfile('*.mat', Lang(str1,str2), nomFic);
if ~flag
    return
end
[repExport, nomFic] = fileparts(nomFic);
nomFicSave = fullfile(repExport, [nomFic '.mat']);

flag = 1;
