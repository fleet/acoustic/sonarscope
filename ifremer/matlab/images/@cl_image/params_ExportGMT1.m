function [flag, nomFic, indLayerBathymetry, repExport] = params_ExportGMT1(this, indImage, XLim, YLim, repExport)
 
nomFic             = [];
indLayerBathymetry = [];

%% Test si l'image est bien en "LatLong"

flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Test si la carte ne fait pas plus d'un tour de la terre

% XLim = get(this(indImage), 'XLim');
Xmin = XLim(1);
Xmax = XLim(2);
if abs(Xmax-Xmin) > 360
    str = Lang('L''image s''�tale sur plus de 360�. Veuillez proc�der pr�alablement � une extraction ad�quate.',...
        'The image is spread out over more 360�.  Please proceed an adequate extraction before.');
    my_warndlg(str, 1);
    return
end

%% Test si l'image est une image de bathym�trie

DataType  = this(indImage).DataType;
ImageType = this(indImage).ImageType;
if (DataType == cl_image.indDataType('Bathymetry')) && (ImageType == 1) % 1=Intensite
    indLayerBathymetry = indImage;
else
    identLayerBathymetry  = cl_image.indDataType('Bathymetry');
    [indLayerBathymetry, nomLayersBathymetry] = findIndLayerSonar(this, indImage, 'DataType', identLayerBathymetry);
    
    % On supprime les layers qui ne sont pas en intensite : �a serait mieux
    % de pouvoir faire rirectement findIndLayerSonar(this, indImage, 'DataType',
    % identLayerBathymetry, 'ImageType', 1); mais je n'ai pas le temps de
    % faire �a aujourd'hui
    
    for k=1:length(indLayerBathymetry)
        ImageType = this(indLayerBathymetry(k)).ImageType;
        sub(k) = (ImageType == 1); %#ok % 1=%Intensity
    end
    if ~isempty(nomLayersBathymetry)
        indLayerBathymetry  = indLayerBathymetry(sub);
        nomLayersBathymetry = nomLayersBathymetry(sub);
    end

    % ------------------------------------------------------------------------------------
    % On v�rifie si les layers de bathy ont une intersection commube avec l'image courante
    
    clear sub
    x1  = get(this(indImage), 'x');
    x1 = x1((x1 >= XLim(1)) & (x1 <= XLim(2)));
    y1  = get(this(indImage), 'y');
    y1 = y1((y1 >= YLim(1)) & (y1 <= YLim(2)));
    for k=1:length(indLayerBathymetry)
        x2  = get(this(indLayerBathymetry(k)), 'x');
        y2  = get(this(indLayerBathymetry(k)), 'y');
        subx1 = intersect3(x1, x2, x2);
        suby1 = intersect3(y1, y2, y2);
        sub(k) = ~isempty(subx1) && ~isempty(suby1); % 1=%Intensity
    end
    if ~isempty(nomLayersBathymetry)
        indLayerBathymetry  = indLayerBathymetry(sub);
        nomLayersBathymetry = nomLayersBathymetry(sub);
    end

    if ~isempty(indLayerBathymetry)
        if length(indLayerBathymetry) > 1
            str1 = 'Il existe plusieurs images de "Bathymetry", la quelle voulez-vous utiliser ?';
            str2 = 'There are several "Bathymetry" layers. Witch one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersBathymetry, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            indLayerBathymetry = indLayerBathymetry(rep);
        end
    end
end

%% Saisie du nom du fichier postcript (.ps)

% [flag, nomFic, this] = getFileNameLinked2ImageName(this, '.ps', 'FullName');
% TODO : remplacer ce qui suit et activer getFileNameLinked2ImageName

ImageName = code_ImageName(this(indImage));
filtre = fullfile(repExport, [ImageName '.ps']);
[flag, nomFic] = my_uiputfile('*.ps', 'Give a file name', filtre);
if ~flag
    return
end
repExport = fileparts(nomFic);

% Test si le fichier .ps existe d�j� et peut �tre remplac� car il se passe
% quelque chose de bizarre sur la machine 64 bits, des fichiers .ps restent
% lock�s bien qu'on ne soit plus en train de les utiliser

% if exist(nomFic, 'file')
%     delete(nomFic);
%     if exist(nomFic, 'file')
%         str1 = sprintf('Le fichier "%s" n''a pas pu �tre supprim�, il est sans doute ouvert dans une autre application. Changez de nom de fichier ou fermez toute application de visualisation de ce fichier .ps ou essayez de le supprimer manuellement car nous avons remarqu� que des fichiers postcript pouvaient rester lock�s alors qu''ils ne sont plus utilis�s.', nomFic);
%         str2 =  sprintf('"%s" cannot be deleted. It is perhaps open in a viewer.', nomFic);
%         my_warndlg(Lang(str1,str2), 1);
%         flag = 0;
%         return
%     end
% end
