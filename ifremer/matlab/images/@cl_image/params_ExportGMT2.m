function [flag, nomFicPs, indLayers, rep3D, repMapOn3D, indLayerMapOn3D, repExport] = params_ExportGMT2(this, indImage, XLim, repExport)

indLayers       = [];
indLayerMapOn3D = [];
rep3D           = 0;
repMapOn3D      = 0;
nomFicPs        = [];

%% Test si l'image est bien en "LatLong"

flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Test si la carte ne fait pas plus d'un tour de la terre

% XLim = get(this(indImage), 'XLim');
Xmin = XLim(1);
Xmax = XLim(2);
if abs(Xmax-Xmin) > 360
    str1 = 'L''image s''�tale sur plus de 360�. Veuillez proc�der pr�alablement � une extraction ad�quate.';
    str2 = 'The image is spread out over more 360�.  Please proceed an adequate extraction before.';
    my_warndlg(lang(str1,str2), 1);
    return
end

%% Test si on est sur une image de bathymetrie

identLayerBathymetry  = cl_image.indDataType('Bathymetry');
DataType = this(indImage).DataType;
if DataType == identLayerBathymetry
    str1 = 'Voulez-vous cr�er une repr�sentation 3D ?';
    str2 = 'Do you want a 3D view on top ?';
    [rep3D, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    rep3D = (rep3D == 1);
end

%% Liste des layers suppl�mentaires

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

if rep3D && ~isempty(indLayers)
    str1 = 'Voulez-vous drapper une image sur la vue GLOBE ?';
    str2 = 'Do you want to display a texture image on top GLOBE ?';
    [repMapOn3D, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    repMapOn3D = (repMapOn3D == 1);
    if repMapOn3D
        str1 = 'Layer � drapper sur la vue 3D';
        str2 = 'Layer to map on the 3D view';
        [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'InitialValue', [], 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerMapOn3D = indLayers(choix);
    end
end

%% Liste des layers � afficher en couches minces

if ~isempty(indLayers)
    str1 = 'Layers � afficher comme couches';
    str2 = 'Layers to display as slices';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'InitialValue', []);
    if ~flag
        return
    end
    indLayers = indLayers(choix);
    nomsLayers = nomsLayers(choix);
end

%% Ordre des layers

if length(nomsLayers) > 1
    str1 = 'Ordre des images sur le trac�';
    str2 = 'Order of Images on the plot';
    [flag, ListeOut, Ordre] = uiOrdreItemsPlus(nomsLayers, 'Intitule', Lang(str1,str2)); %#ok<ASGLU>
    indLayers = indLayers(Ordre);
    indLayers  = fliplr(indLayers);
end

%% Saisie du nom du fichier GMT (.grd)

% [flag, nomFic, this] = getFileNameLinked2ImageName(this, '.ps', 'FullName');
% TODO : remplacer ce qui suit et activer getFileNameLinked2ImageName

filtre = fullfile(repExport, 'plotGMTMulti.ps');
[flag, nomFicPs] = my_uiputfile('*.ps', 'Give a file name', filtre);
if ~flag
    return
end
repExport = fileparts(nomFicPs);
