function [flag, liste, index, indLayers] = params_ExportImageSignals(this, indImage, varargin)

[varargin, SelectImages] = getPropertyValue(varargin, 'SelectImages', true); %#ok<ASGLU>

liste     = [];
index     = [];
indLayers = [];

flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

indLayers = indImage;
if SelectImages
    [flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage, ...
        'IncludeCurrentImage', 'SynchroX', 0, 'SynchroS', 0); %#ok<ASGLU>
    if ~flag
        return
    end
    %     [~, Selection] = unique(DataTypes, 'last');
    Selection = find((indLayers == indImage));
    [choix, flag] = my_listdlg(Lang('Layers � exporter', 'Select layers to export'), ...
        nomsLayers, 'InitialValue', Selection);
    if ~flag
        return
    end
    indLayers = indLayers(choix);
end

liste = get_liste_vecteurs(this(indImage), 'VerticalOnly');
if isempty(liste)
    return
end

str1 = 'Courbes � visualiser';
str2 = 'Curves to display';
[index, flag] = my_listdlg(Lang(str1,str2), liste, 'InitialValue', 1);
if ~flag || isempty(index)
    return
end
