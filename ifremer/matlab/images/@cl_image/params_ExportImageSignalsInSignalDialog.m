function [flag, listLayers, listSignals, index] = params_ExportImageSignalsInSignalDialog(this, indImage)

listLayers  = [];
listSignals = [];
index       = [];
    
flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

% [flag, listLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
listLayers = indImage; % Contraint pour l'instant car c'est pas pr�vu d'afficher plusieurs images dans SignalDialog

[flag, listSignals, index] = params_ExportImageSignals(this, indImage, 'SelectImages', false);
if ~flag
    return
end
