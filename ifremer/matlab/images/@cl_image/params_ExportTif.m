function [flag, nomFic, nbLigMax, nbColMax, Quality, repExport] = params_ExportTif(this, Ext, subl, subc, repExport)

nbLigMax = [];
nbColMax = [];
Quality  = [];

ImageName = code_ImageName(this);
filtreTif = fullfile(repExport, [ImageName Ext]);
[flag, nomFic] = my_uiputfile({'*.tif;*.jpg;*.gif;*.bmp;*.png';
    '*.tif'; '*.jpg'; '*.gif'; '*.bmp'; '*.png'}, Lang('Nom du fichier', 'Give a file name'), filtreTif);
if ~flag
    return
end
[pathname, filename, ext2] = fileparts(nomFic);
if isempty(ext2)
    ext = '.tif';
else
    ext = ext2;
end
nomFic = fullfile(pathname, [filename ext]);
[pathname, nomFic, ext] = fileparts(nomFic);
nomFic = fullfile(pathname, [nomFic ext]);

pathname = fileparts(nomFic);
repExport = pathname;

if strcmpi(ext, '.jpg') || strcmpi(ext, '.jpeg')
    str1 = 'TODO';
    str2 = 'Quality (100 = no compression).';
    p = ClParametre('Name', Lang('Qualit�', 'Quality'), ...
        'Unit', '%',  'MinValue', 1, 'MaxValue', 100, 'Value', 75);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    Quality = a.getParamsValue;  
end

nbLigMax = min(length(subl), 32767);
nbColMax = min(length(subc), 32767);
if get_LevelQuestion >= 3
    str1 = 'TODO';
    str2 = 'The image can be written in tiles. You can specify the tile''s height and width';
    p(1) = ClParametre('Name', Lang('Hauteur', 'Height'), ...
        'Unit', 'pixels',  'MinValue', 1, 'MaxValue', length(subl), 'Value', nbLigMax, 'Format', '%d');
    p(2) = ClParametre('Name', Lang('Largeur', 'Width'), ...
        'Unit', 'pixels',  'MinValue', 1, 'MaxValue', length(subc), 'Value', nbColMax, 'Format', '%d');
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;  
    nbLigMax = val(1);
    nbColMax = val(2);
end
