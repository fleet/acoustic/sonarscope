function [flag, s, indLayerMask, valMask, zone] = params_FctsAritmVecteur(this, indImage, cross, suby)

s            = [];
indLayerMask = [];
valMask      = [];
zone         = [];

str1 = 'Vecteur';
str2 = 'Vector';
liste = get_liste_vecteurs(this(indImage));
[choix, flag] = my_listdlg(Lang(str1,str2), liste, 'SelectionMode', 'Single');
if flag
    s = get_vecteur(this(indImage), liste{choix}, 'ijCross', cross, 'suby', suby);
else
    return
end

[flag, indLayerMask, valMask, zone] = paramsMasquage(this, indImage);
