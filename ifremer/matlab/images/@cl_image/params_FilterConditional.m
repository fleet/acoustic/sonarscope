function [flag, indLayerConditionnel, Sigma] = params_FilterConditional(this, indImage) 

% persistent ParamFiltre

indLayerConditionnel = [];

%% S�lection du layer compl�mentaire

indDataTypeConditions(1) = cl_image.indDataType('TxAngle');
indDataTypeConditions(2) = cl_image.indDataType('RxBeamAngle');
indDataTypeConditions(3) = cl_image.indDataType('SonarAcrossBeamAngle');
indDataTypeConditions(4) = cl_image.indDataType('BeamPointingAngle');

%% 

str1 = 'Param�trage du filtre gaussien';
str2 = 'Gaussian filtre parametre';
[flag, Sigma] = inputOneParametre(Lang(str1,str2), Lang('Ecart-Type', 'Standard deviation'), ...
    'Value', 1.2, 'Unit', 'pixels', 'MinValue', 0.5, 'MaxValue', 10);
if ~flag
    return
end

%% S�lection du layer

[indLayers, nomsLayers] = findIndLayerSonar(this, indImage, 'DataType', indDataTypeConditions, 'OnlyOneLayer');

% [flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, ...
%     'TypeDonneUniquement', TypeDonneUniquement);
if ~flag
    return
end

switch length(indLayers)
    case 0
        str1 = 'Aucune image de m�me nature a �t� trouv�e';
        str2 = 'No similar image has been found.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    otherwise
        str1 = 'S�lectionnez la seconde image';
        str2 = 'Select second image';
        [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single', 'InitialValue', length(nomsLayers));
        if ~flag
            return
        end
        indLayerConditionnel = indLayers(choix);
end
