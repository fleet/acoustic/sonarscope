function [flag, nomFic, indlayerDrapage, repExport] = params_Fledermaus(this, indImage, repExport)

[flag, nomFic, repExport] = getFileNameLinked2ImageName(this(indImage), '.sd', repExport, 'FullName');
if ~flag
    indlayerDrapage = [];
    return
end
[flag, indlayerDrapage] = paramsDrapageFledermaus(this, indImage);
if ~flag
    return
end
