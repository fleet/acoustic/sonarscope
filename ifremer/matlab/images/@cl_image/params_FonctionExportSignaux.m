function [flag, nomsFic, ColumnsForProfile, listeSignaux, Separator, TimeFormat, subCourbes, repExport] = params_FonctionExportSignaux(this, indImage, cross, repExport)

nomsFic.Signaux     = '';
nomsFic.AllAttitude = '';
nomsFic.AllClock    = '';
nomsFic.AllHeight   = '';
nomsFic.AllPosition = '';
nomsFic.AllSurfaceSoundSpeed = '';
ColumnsForProfile = [];
subCourbes = [];

listeSignaux  = [];
Separator     = [];
TimeFormat    = [];

str = {};
filtre = fullfile(repExport, 'Signals.csv');
[flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', filtre);
if ~flag
    return
end
repExport = fileparts(nomFic);
nomsFic.Signaux = nomFic;

x = cross(1);
y = cross(2);
[ix, iy] = xy2ij(this(indImage), x, y); %#ok<ASGLU>

[rep, flag] = my_questdlg(Lang('TODO', 'Save image vertical profile ?'));
if ~flag
    return
end
if rep == 1
    nomVar = 'Column numbers (Ex : 69 or [100:200] or [1:10 20:30] for columns average).';
    value = num2str(ix);
    [value, flag] = my_inputdlg(nomVar, value);
    if ~flag
        return
    end
    ColumnsForProfile = str2num(value{1});  %#ok<ST2NM>
    str{end+1} = 'ColumnsAverage';
end

flagTime = 0;
if get(this(indImage), 'flagTime')
    str{end+1} = 'Time';
    flagTime = 1;
end
if get(this(indImage), 'flagPingNumber')
    str{end+1} = 'PingNumber';
end
if get(this(indImage), 'flagImmersion')
    str{end+1} = 'Immersion';
end
if get(this(indImage), 'flagHeight')
    str{end+1} = 'Height';
end
if get(this(indImage), 'flagCableOut')
    str{end+1} = 'CableOut';
end
if get(this(indImage), 'flagSpeed')
    str{end+1} = 'Speed';
end
if get(this(indImage), 'flagHeading')
    str{end+1} = 'Heading';
end
if get(this(indImage), 'flagRoll')
    str{end+1} = 'Roll';
end
if get(this(indImage), 'flagPitch')
    str{end+1} = 'Pitch';
end
if get(this(indImage), 'flagYaw')
    str{end+1} = 'Yaw';
end
if get(this(indImage), 'flagSurfaceSoundSpeed')
    str{end+1} = 'SurfaceSoundSpeed';
end
if get(this(indImage), 'flagFishLatitude')
    str{end+1} = 'FishLatitude';
end
if get(this(indImage), 'flagFishLongitude')
    str{end+1} = 'FishLongitude';
end
if get(this(indImage), 'flagShipLatitude')
    str{end+1} = 'ShipLatitude';
end
if get(this(indImage), 'flagShipLongitude')
    str{end+1} = 'ShipLongitude';
end
if get(this(indImage), 'flagPortMode_1')
    str{end+1} = 'PortMode_1';
end
if get(this(indImage), 'flagPortMode_2')
    str{end+1} = 'PortMode_2';
end
if get(this(indImage), 'flagStarMode_1')
    str{end+1} = 'StarMode_1';
end
if get(this(indImage), 'flagStarMode_2')
    str{end+1} = 'StarMode_2';
end
if get(this(indImage), 'flagHeave')
    str{end+1} = 'Heave';
end
if get(this(indImage), 'flagTide')
    str{end+1} = 'Tide';
end
if get(this(indImage), 'flagPingCounter')
    str{end+1} = 'PingCounter';
end
if get(this(indImage), 'flagSampleFrequency')
    str{end+1} = 'SampleFrequency';
end
if get(this(indImage), 'flagSonarFrequency')
    str{end+1} = 'SonarFrequency';
end
if get(this(indImage), 'flagInterlacing')
    str{end+1} = 'Interlacing';
end

if isempty(str)
    flag = 0;
    return
end

[flag, subCourbes, nbCourbesStatistiques, NomsCourbes] = selectionCourbesStatsVert(this(indImage)); %#ok<ASGLU>
if ~flag || isempty(subCourbes)
    flag = 0;
    return
end

%% Selection des signaux � exporter

[flag, listeItems, ItemsSelect] = uiSelectItems('ItemsToSelect', [str NomsCourbes]); %#ok<ASGLU>
if ~flag
    return
end
listeSignaux = str(ItemsSelect(ItemsSelect <= length(str)));
% listeCourbes = NomsCourbes(ItemsSelect(ItemsSelect > length(str)) - length(str));
subCourbes = subCourbes(ItemsSelect(ItemsSelect > length(str)) - length(str));

%% Choix du s�parateur

Separator = {';'; 'Tab'; 'Space'};
[rep, flag] = my_listdlg('Separator', Separator, 'SelectionMode', 'Single');
if ~flag
    return
end
Separator{2} = sprintf('\t');
Separator{3} = ' ';
Separator = Separator{rep};

%% Choix du format de l'heure

if flagTime
    [TimeFormat, flag] = my_listdlg('Date format', {'JJ/MM/AAAA'; 'YYYY/MM/DD'}, 'SelectionMode', 'Single');
    if ~flag
        return
    end
end

% Test si l'origine le fichier d'origine est un "SimradAll"

InitialFileFormat = this(indImage).InitialFileFormat;
if strcmp(InitialFileFormat, 'SimradAll')
    InitialFileName = this(indImage).InitialFileName;
    if exist(InitialFileName, 'file')
        [rep, flag] = my_listdlg(Lang('Liste des datagrammes � exporter', 'List of other datagrams to export'), ...
            {'Attitude'; 'Clock'; 'Height'; 'Position'; 'Surface Sound Speed'}, 'InitialValue', []);
        if ~flag
            return
        end
        
        % Nom du fichier Attitude
        if any(rep == 1)
            filtre = fullfile(repExport, 'Attitude.csv');
            [flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', filtre);
            if ~flag
                return
            end
            repExport = fileparts(nomFic);
            nomsFic.AllAttitude = nomFic;
        end
        
        % Nom du fichier Clock
        if any(rep == 2)
            filtre = fullfile(repExport, 'Clock.csv');
            [flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', filtre);
            if ~flag
                return
            end
            repExport = fileparts(nomFic);
            nomsFic.AllClock = nomFic;
        end
        
        % Nom du fichier Height
        if any(rep == 3)
            filtre = fullfile(repExport, 'Height.csv');
            [flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', filtre);
            if ~flag
                return
            end
            repExport = fileparts(nomFic);
            nomsFic.AllHeight = nomFic;
        end
        
        % Nom du fichier Position
        if any(rep == 4)
            filtre = fullfile(repExport, 'Position.csv');
            [flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', filtre);
            if ~flag
                return
            end
            repExport = fileparts(nomFic);
            nomsFic.AllPosition = nomFic;
        end
        
        % Nom du fichier SurfaceSoundSpeed
        if any(rep == 5)
            filtre = fullfile(repExport, 'SurfaceSoundSpeed.csv');
            [flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', filtre);
            if ~flag
                return
            end
            repExport = fileparts(nomFic);
            nomsFic.AllSurfaceSoundSpeed = nomFic;
        end
    end
end
