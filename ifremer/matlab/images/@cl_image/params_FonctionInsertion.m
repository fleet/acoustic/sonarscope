function [flag, identParent] = params_FonctionInsertion(this, indImage) 

identParent = [];
ImageName   = {};
 
[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this, indImage); %#ok<ASGLU>
if ~flag
    return
end
 
NuIdentParent = this(indImage).NuIdentParent;
for k=1:length(indLayers)
    NuIdent = this(indLayers(k)).NuIdent;
    if NuIdent == NuIdentParent
        identParent(end+1) = indLayers(k); %#ok<AGROW> 
        ImageName{end+1} = this(indLayers(k)).Name; %#ok<AGROW> 
        flag = 1; % L'image parente est trouv�e
    end
end
if length(identParent) > 1
    str1 = 'Plusieurs images sont cousines, dans laquelle voulez-vous inserer celle-ci ?';
    str2 = 'Several images are cousins, select the one in which you want to insert this one ?';
    [rep, flag] = my_listdlg(Lang(str1,str2), ImageName, 'SelectionMode', 'Single');
    if ~flag
        identParent = [];
        return
    end
    identParent = identParent(rep);
end
if isempty(identParent)
    flag = 0;
    return
end

% Verification de compatibilit� si l'image parente est trouv�e
if flag
    Titre       = this(indImage).Name;
    TitreParent = this(identParent).Name;
    
    DataType       = this(indImage).DataType;
    DataTypeParent = this(identParent).DataType;
    
    if DataType ~= DataTypeParent
        str = sprintf('Operation impossible car \nDataType=%d pour %s et \nDataType=%d pour %s', ...
                        DataType, Titre, DataTypeParent, TitreParent);
        my_warndlg(str, 1);
        flag = 0;
        return
    end
    
    GeometryType       = this(indImage).GeometryType;
    GeometryTypeParent = this(identParent).GeometryType;
    
    if GeometryType ~= GeometryTypeParent
        str = sprintf('Operation impossible car \nGeometryType=%d pour %s et \nGeometryType=%d pour %s', ...
                        GeometryType, Titre, GeometryTypeParent, TitreParent);
        my_warndlg(str, 1);
        flag = 0;
        return
    end
    
    SpectralStatus       = this(indImage).SpectralStatus;
    SpectralStatusParent = this(identParent).SpectralStatus;
    
    if SpectralStatus ~= SpectralStatusParent
        str = sprintf('Operation impossible car \nSpectralStatus=%d pour %s et \nSpectralStatus=%d pour %s', ...
                        SpectralStatus, Titre, SpectralStatusParent, TitreParent);
        my_warndlg(str, 1);
        flag = 0;
        return
    end
end
