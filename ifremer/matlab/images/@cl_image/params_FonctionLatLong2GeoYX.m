function [flag, Step] = params_FonctionLatLong2GeoYX(this)

Step = get(this, 'YStepMetric');

[flag, Step] = inputOneParametre('Grid size for GeoYX image', Lang('Pas de grille', 'Grid size'), ...
    'Value', Step, 'Unit', 'm', 'MinValue', 0);
if ~flag
    return
end
