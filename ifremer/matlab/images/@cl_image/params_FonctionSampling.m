function [pasx, pasy, flag] = params_FonctionSampling(~)

pasx = [];
pasy = [];

str1 = 'TODO';
str2 = sprintf('Sampling parmetres\n(>1 to reduce size, <1 to enlarge image');
p    = ClParametre('Name', Lang('Facteur de reduction en x', 'X reduction factor'), ...
    'Value', 2, 'MinValue', 0);
p(2) = ClParametre('Name', Lang('Facteur de reduction en y', 'Y reduction factor'), ...
    'Value', 2, 'MinValue', 0);
a = StyledSimpleParametreDialog('params', p, 'Title',  Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

pasx = val(1);
pasy = val(2);
