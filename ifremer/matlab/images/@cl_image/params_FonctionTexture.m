function [flag, binsImage, indLayerAngle, binsAngle, numTexture, nomTexture, cliques] = params_FonctionTexture(this, indImage, subx, suby)

indLayerAngle = [];
binsAngle     = [];
nomTexture    = [];
cliques       = [1 0];
binsImage     = [];
numTexture    = [];
% Win         = [32 32];

flag = testSignature(this(indImage), 'ImageType', 1);
if ~flag
    return
end

Texture = get_SignatureTexture(this(indImage));
numTexture = length(Texture) + 1;

HistoCentralClasses = get(this(indImage), 'HistoCentralClasses');
HistoCentralClasses = HistoCentralClasses(:);
binsImage.CLim = [HistoCentralClasses(1) HistoCentralClasses(end)];
binsImage.NbNiveaux = 64;
if (binsImage.CLim(1) > 0) && (binsImage.CLim(2) > 0)
    MinVal = 2 ^ floor(log2(binsImage.CLim(1))/log(64));
    MaxVal = 2 ^ ceil(log2(binsImage.CLim(2)));
    if MinVal < 1
        MinVal = 0;
    end
elseif (binsImage.CLim(1) < 0) && (binsImage.CLim(2) < 0)
    MaxVal = -2 ^ floor(log2(-binsImage.CLim(2))/log(64));
    MinVal = -2 ^ ceil(log2(-binsImage.CLim(1)));
    if MaxVal > -1
        MaxVal = 0;
    end
else
    MinVal = -(2 ^ (ceil(log(-binsImage.CLim(1)))+2));
    MaxVal = 2 ^ (ceil(log(binsImage.CLim(2))+1));
end

% if (binsImage.CLim(1) > 0) && (binsImage.CLim(2) > 0)
%     MinVal = 64 * floor(log(binsImage.CLim(1))/log(64));
%     MaxVal = 64 * ceil(log(binsImage.CLim(2))/log(64));
% elseif (binsImage.CLim(1) < 0) && (binsImage.CLim(2) < 0)
%     MaxVal = -(64 * ceil(log(-binsImage.CLim(1))/log(64)));
%     MinVal = -(64 * floor(log(-binsImage.CLim(2))/log(64)));
% else
%     MinVal = -(64 * ceil(log(-binsImage.CLim(1))/log(64)));
%     MaxVal = 64 * ceil(log(binsImage.CLim(2))/log(64));
% end

Unit = this(indImage).Unit;
str1 = 'Param�tres de quantification';
str2 = 'Quantization parameters';
p    = ClParametre('Name', Lang('Valeur min', 'Min value'), ...
    'Unit', Unit,  'MinValue', MinVal, 'MaxValue', MaxVal, 'Value', binsImage.CLim(1));
p(2) = ClParametre('Name', Lang('Valeur max', 'Max value'), ...
    'Unit', Unit,  'MinValue', MinVal, 'MaxValue', MaxVal, 'Value', binsImage.CLim(2));
p(3) = ClParametre('Name', Lang('Nombre de niveaux', 'Number of levels'), ...
    'Unit', ' ',  'MinValue', 32, 'MaxValue', 256, 'Value', 64, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
binsImage.CLim      = val(1:2);
binsImage.NbNiveaux = val(3);

%%

str1 = 'Voulez-vous r�aliser un apprentissage conditionnel (Angle d''incidence par exemple) ?';
str2 = 'Do you want to process a conditionnal training ?';
[choix, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end

if choix == 1
    [flag, Selection] = selectionLayersSynchronises(this, indImage, 'subx', subx, 'suby', suby, 'Max', 1, 'noXY');
    if ~flag
        return
    end
    indLayers = ([Selection(:).indLayer] ~= 0);
    indLayers = Selection(indLayers).indLayer;
else
    indLayers = [];
end

identRegionOfInterest = cl_image.indDataType('Mask');
for k=1:length(indLayers)
    DataType = this(indLayers(k)).DataType;
    if DataType ~= identRegionOfInterest
        indLayerAngle = indLayers(k);
        binsAvant = get(this(indLayers(k)), 'HistoCentralClasses');
        binsAvant = binsAvant(:); % Pour �viter cl_memmapfile
        binsAvant = -80:10:80; % le temps de la mise au point    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TODO TODO TODO
        def    = num2str(binsAvant);
        str1 = 'Saisie des fronti�res de classes de l''image conditionnelle';
        str2 = 'Input the  boundary values of the conditional image';
        answer = inputdlg(Lang(str1,str2), 'IFREMER', 1, {def});
        if isempty(answer)
            flag = 0;
            return
        end
        binsAngle = str2num(answer{1}); %#ok
        if isempty(binsAngle)
            my_warndlg(['Valeurs non interpretables : ' answer{1}], 0);
            flag = 0;
            return
        end
    end
end

str{1} = sprintf('Signature texturale %d', numTexture);
str{2} = '1 0';
options.Resize = 'on';
str1 = 'Nom de cette signature texturale';
str2 = 'Name of this textural signature';
prompt      = {Lang(str1,str2), Lang('Cliques Ex [10 ; 6 0 ; 0 1]', 'TODO')};
dlgTitle    = 'IFREMER';
cmd         = inputdlg(prompt, dlgTitle, [1;1], str, options);
if isempty(cmd)
    flag = 0;
    return
end
nomTexture = cmd{1};
cliques = str2num(cmd{2}); %#ok

if size(cliques, 2) ~=  2
    str1 = 'Une clique doit obligatoirement �tre d�finie par deux nombres : le d�placement horizontal et le d�placement vertical.';
    str2 = 'A clique has to be discribed by 2 numbers, the horizontal and the vertical shifts';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
