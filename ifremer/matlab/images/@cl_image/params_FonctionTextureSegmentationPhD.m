function [flag, Texture, indLayerAngle, subAngles, Win, SeuilConfusion, SeuilRejet, FilterCoocMat] ...
    = params_FonctionTextureSegmentationPhD(this, indImage)

indLayerAngle   = [];
subAngles       = [];
Win             = [32 32];
SeuilConfusion  = 0;
SeuilRejet      = Inf;
Texture         = [];
FilterCoocMat   = false;

sub  = selectionSegmSignature(this(indImage));
if isempty(sub)
    my_warndlg(Lang('Je n''ai trouv� aucune d�finition de textures', 'No texture signatures found.'), 1);
    flag = 0;
    return
end

Texture = get_SignatureTexture(this(indImage), 'sub', sub);
if isequal(Texture.binsLayerAngle,1)
    nbAngles = 1;
else
    nbAngles = length(Texture.binsLayerAngle)-1;
end
% nbValMasque = length(Texture.nomCourbe);

if nbAngles > 2
    identLayerAngle = Texture.DataType_LayerAngle;
    [flag, indLayerAngle, nomsLayers] = listeLayersSynchronises(this, indImage, ...
        'TypeDonneUniquement', identLayerAngle, 'OnlyOneLayer', 1); %#ok<ASGLU>
    if ~flag
        return
    end
    
    if isempty(indLayerAngle)
        str1 = sprintf('Les signatures texturales font reference a une image conditionnelle de type "%s". Or je ne trouve pas de layer de ce type dans la liste des images.', ...
            cl_image.strDataType{identLayerAngle});
        str2 = sprintf('The textural sigatures are linked to a layer "%s". I cannot find such an image in the list.', ...
                        cl_image.strDataType{identLayerAngle});
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
%     else
%         str1 = 'Confirmation image conditionnelle';
%         str2 = 'Conditional image agreement';
%         [identLayerAngle, flag] = my_listdlg(Lang(str1,str2), ...
%             nomsLayers, 'SelectionMode', 'Single');%#ok
%         if ~flag
%             return
%         end
    end
end


% sumCmatModeleAngle = NaN(nbValMasque, nbAngles, 'single');
% for iMasque=1:nbValMasque
%     if isempty(Texture.CmatModeleAngle{iMasque})
%         continue
%     end
%     for iAngle=1:nbAngles
%         CmatModeleAngle = Texture.CmatModeleAngle{iMasque}(iclique,iAngle);
%         CmatModeleAngle = CmatModeleAngle{1};
%         if isempty(CmatModeleAngle)
%             sumCmatModeleAngle(iMasque, iAngle) = 0;
%         else
%             sumCmatModeleAngle(iMasque, iAngle) = sum(CmatModeleAngle(:));
%         end
%     end
% end
% sumCmatModeleAngle = ~(sumCmatModeleAngle == 0);
% 
% % ------------------------------------
% % Test si aucune matrice n'est definie
% 
% if sum(sumCmatModeleAngle(:)) == 0
%     my_warndlg(Lang('Toutes vos matrices sont vides', 'All the matrices are empty.'), 1);
%     flag = 0;
%     return
% end

%% Saisie de la largeur de la fenetre d'analyse

str1 = 'Hauteur et largeur de la fen�tre d''analyse';
str2 = 'Height and width of the analysis window';
[flag, Win] = saisie_window([32 32], 'maxvalue', [128 128], 'Titre', Lang(str1,str2));
if ~flag
    return
end

if Texture.UseCooc && (get_LevelQuestion >= 3)
    nbMesuresSimilariteCooc  = size(Texture.cliques,1);
    w = Texture.w;
    subCliques = find(w ~= 0);
    if any(subCliques <= nbMesuresSimilariteCooc)
        [rep, flag] = my_questdlg(Lang('FIltrage des matrices de cooccurrence ?', 'Cooccurrence matrices filtering ?'), ...
            'Init', 2, 'ColorLevel', 3);
        if ~flag
            return
        end
        if rep == 1
            FilterCoocMat = true;
        end
    end
end

%% Saisie des seuils de d�cision de non classification

if get_LevelQuestion >= 3
    str1 = 'Param�tres de classification';
    str2 = 'Classification parameters';
    p    = ClParametre('Name', Lang('Seuil Ind�termination', 'Indetermination threshold'), ...
        'Value', SeuilConfusion, 'MinValue', 0, 'MaxValue', 5);
    p(2) = ClParametre('Name', Lang('Seuil Non Class�', 'Unclassified threshold'), ...
        'Value', SeuilRejet, 'MinValue', 1);
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2), 'Format', '%d');
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;
%     a = set(a, 'color', get_ColorLevel(3));

    SeuilConfusion = val(1);
    SeuilRejet     = val(2);
end

% % -----------------------------------------
% % Test si toutes les matrices sont definies
% 
% if sum(sumCmatModeleAngle(:)) == ( nbValMasque* nbAngles)
%     subAngles = 1:nbAngles;
%     flag = 1;
%     return
end

% % --------------------------------------------------------------------------------
% % Recherche des angles pour lesquels toutes les signatures de facies sont definies
% 
% subAngles = find(sum(sumCmatModeleAngle,1) == nbValMasque);
% str1 = sprintf('Seules les signatures conditionnelles %s sont definies pour tous les facies. La segmentation ne se fera par consequent que sur la partie de l''image satisfaisant � ces conditions. Voulez-vous quand-meme proceder � la segmentation ?', ...
%     num2str(Texture.binsLayerAngle(subAngles)));
% str2 = sprintf('Only the "%s" textual signatures are defined for the facies. The segmentation process will operate only on the part of the image where the conditions are satisfied. Do you want to proceed anyway ?', ...
%     num2str(Texture.binsLayerAngle(subAngles)));
% [rep, flag] = my_questdlg(Lang(str1,str2));
% if rep == 2
%     flag = 0;
%     return
% end
