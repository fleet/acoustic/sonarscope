function [flag, binsImage, indLayerAngle, binsAngle, numTexture, nomTexture, ...
                depMaxClique, UseCooc, UseGabor] = params_FonctionTextureThese(this, indImage, subx, suby)

indLayerAngle   = [];
binsAngle       = [];
nomTexture      = [];
depMaxClique    = 0; % 10
UseCooc         = false;
UseGabor        = false;

Texture = get_SignatureTexture(this(indImage));
numTexture = length(Texture) + 1;

HistoCentralClasses = get(this(indImage), 'HistoCentralClasses');
HistoCentralClasses = HistoCentralClasses(:); % Pour "�viter cl_memapfile
binsImage.CLim = [HistoCentralClasses(1) HistoCentralClasses(end)];
binsImage.NbNiveaux = 64;
if (binsImage.CLim(1) > 0) && (binsImage.CLim(2) > 0)
    MinVal = 2 ^ floor(log2(binsImage.CLim(1))/log(64));
    MaxVal = 2 ^ ceil(log2(binsImage.CLim(2)));
    if MinVal < 1
        MinVal = 0;
    end
elseif (binsImage.CLim(1) < 0) && (binsImage.CLim(2) < 0)
    MaxVal = -2 ^ floor(log2(-binsImage.CLim(2))/log(64));
    MinVal = -2 ^ ceil(log2(-binsImage.CLim(1)));
    if MaxVal > -1
        MaxVal = 0;
    end
else
    MinVal = -(2 ^ (ceil(log(-binsImage.CLim(1)))+2));
    MaxVal = 2 ^ (ceil(log(binsImage.CLim(2))+1));
end

% if (binsImage.CLim(1) > 0) && (binsImage.CLim(2) > 0)
%     MinVal = 64 * floor(log(binsImage.CLim(1))/log(64));
%     MaxVal = 64 * ceil(log(binsImage.CLim(2))/log(64));
% elseif (binsImage.CLim(1) < 0) && (binsImage.CLim(2) < 0)
%     MaxVal = -(64 * ceil(log(-binsImage.CLim(1))/log(64)));
%     MinVal = -(64 * floor(log(-binsImage.CLim(2))/log(64)));
% else
%     MinVal = -(64 * ceil(log(-binsImage.CLim(1))/log(64)));
%     MaxVal = 64 * ceil(log(binsImage.CLim(2))/log(64));
% end

%% 

Unit = this(indImage).Unit;
str1 = 'Param�tres de quantification';
str2 = 'Quantization parameters';
p    = ClParametre('Name', Lang('Valeur min', 'Min value'), ...
    'Unit', Unit,  'MinValue', MinVal, 'MaxValue', MaxVal, 'Value', binsImage.CLim(1));
p(2) = ClParametre('Name', Lang('Valeur max', 'Max value'), ...
    'Unit', Unit,  'MinValue', MinVal, 'MaxValue', MaxVal, 'Value', binsImage.CLim(2));
p(3) = ClParametre('Name', Lang('Nombre de niveaux', 'Number of levels'), ...
    'Unit', ' ',  'MinValue', 32, 'MaxValue', 256, 'Value', 64, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
binsImage.CLim      = val(1:2);
binsImage.NbNiveaux = val(3);

%% 

str1 = 'Voulez-vous r�aliser un apprentissage conditionnel (Angle d''incidence par exemple) ?';
str2 = 'Do you want to process a conditionnal training ?';
[choix, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end

if choix == 1
    [flag, Selection] = selectionLayersSynchronises(this, indImage, 'subx', subx, 'suby', suby, 'Max', 1, 'noXY');
    if ~flag
        return
    end
    indLayers = ([Selection(:).indLayer] ~= 0);
    indLayers = Selection(indLayers).indLayer;
else
    indLayers = [];
end

identRegionOfInterest = cl_image.indDataType('Mask');
for i=1:length(indLayers)
    DataType = this(indLayers(i)).DataType;
    if DataType ~= identRegionOfInterest
        indLayerAngle = indLayers(i);
        binsAvant = get(this(indLayers(i)), 'HistoCentralClasses');
        binsAvant = binsAvant(:); % Pour �viter cl_memmapfile
        binsAvant = -80:10:80; % le temps de la mise au point    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        def    = num2str(binsAvant);
        answer = inputdlg(Lang('Saisie des fronti�res de classes de l''image conditionnelle', 'Input the  boundary values of the conditional image'), 'IFREMER', 1, {def});
        if isempty(answer)
            flag = 0;
            return
        end
        binsAngle = str2num(answer{1}); %#ok
        if isempty(binsAngle)
            my_warndlg(['Valeurs non interpretables : ' answer{1}], 0);
            flag = 0;
            return
        end
    end
end

str1 = 'Type de mesure.';
str2 = 'Type of tools.';
str{1} = Lang('Matrices de cooccurrence seulement', 'Only Cooccurrence matrices');
str{2} = Lang('Filtres de Gabor seulement', 'Only Gabor filters');
str{3} = Lang('Matrices de cooccurrence ET filtres de Gabor', 'Both Cooccurrence matrices & Gabor filters');
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 3, 'SelectionMode', 'Single');
if ~flag
    return
end
switch rep
    case 1
        UseCooc  = true;
        UseGabor = false;
    case 2
        UseCooc  = false;
        UseGabor = true;
    case 3
        UseCooc  = true;
        UseGabor = true;
end

if UseCooc
    str1 = 'D�placement maximum / Matrices de cooccurrence';
    str2 = 'Maximum shift / Cooccurrence matrices';
    [flag, depMaxClique] = inputOneParametre(Lang(str1,str2), 'Value', ...
        'Value', 10, 'Unit', 'pixels', 'MinValue', 0, 'MaxValue', 20);
    if ~flag
        return
    end
end

str{1} = sprintf('Signature texturale %d', numTexture);
options.Resize = 'on';
prompt   = {Lang('Nom de cette signature texturale', 'Name of this textural signature')};
dlgTitle = 'IFREMER';
cmd      = inputdlg(prompt, dlgTitle, 1, str, options);
if isempty(cmd)
    flag = 0;
    return
end
nomTexture = cmd{1};
