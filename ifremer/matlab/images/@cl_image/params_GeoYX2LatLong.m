function [flag, GridStep] = params_GeoYX2LatLong(this)

N = length(this);
PasMetrique = NaN(N,1);
for k=1:N
    PasMetrique(k) = min(this(k).XStep, this(k).YStep);
end
Pas = unique(PasMetrique);

if length(Pas) == 1
    str1 = 'Pas de la grille';
    str2 = 'Grid size';
    [flag, GridStep] = inputOneParametre(Lang(str1,str2), 'Value', ...
        'Value', Pas, 'Unit', 'm', 'MinValue', 0);
    if ~flag
        return
    end
else
    for k=1:length(this)
        nomImage = this(k).Name;
        
        str1 = sprintf('Pas de la grille pour\n"%s"', nomImage);
        str2 = sprintf('Grid size for\n"%s"', nomImage);
        [flag, GridStep] = inputOneParametre(Lang(str1,str2), 'Value', ...
            'Value', PasMetrique(k), 'Unit', 'm', 'MinValue', 0);
        if ~flag
            return
        end       
    end
end
