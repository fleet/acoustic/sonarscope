function [flag, typeShape] = params_GeometricFrameFromImageSnippet(~) % TODO : d�placer la fonction

%% Select the type of frame

str{1} = Lang('Un chapeau haut de forme',                 'A top hat');
str{2} = Lang('Une gaussienne',                           'A gaussian');
str{3} = Lang('Un c�ne',                                  'A cone');
str{4} = Lang('Un �cho de fluide dans la colonne d''eau', 'A Water Column fluid echo');
str1 = 'Type de forme � rechercher';
str2 = 'Type of shape to hunt';
[typeShape, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end
