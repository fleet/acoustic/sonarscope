function [flag, indLayerConditional, indLayerMask, valMask, ...
    binsI1, binsI2] = params_Histo2D(this, indImage)
 
indLayerConditional = [];
indLayerMask        = [];
valMask             = [];
binsI1              = [];
binsI2              = [];

%% Recherche des layers conditionnels

[flag, Selection] = selectionLayersSynchronises(this, indImage, 'Max', 1, 'SignauxVert');
if ~flag
    return
end
indLayerConditional = Selection.indLayer;

%% Recherche d'un masque �ventuel

[flag, indLayerMask, valMask] = paramsMasque(this, indImage, 'InitialValue', 'Segmentation');
if ~flag
    return
end

%% Saisie des bornes de l'image courante

[binsI1, ~, flag] = saisieBinsHisto(this(indImage));
if ~flag
    return
end
[binsI2, ~, flag] = saisieBinsHisto(this(indLayerConditional));
if ~flag
    return
end
