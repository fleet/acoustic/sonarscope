function [flag, Windows] = params_HistogramCleaningParBlocks(this, subx, suby) 

Windows = [];

flag = testSignature(this, 'ImageType', 1);
if ~flag
    return
end

% ident1 = cl_image.indDataType('Bathymetry');
% flag = testSignature(this, 'DataType', ident1); %, 'GeometryType', cl_image.indGeometryType('PingBeam'));
% if ~flag
%     return
% end
nbRows    = this.nbRows;
nbColumns = this.nbColumns;

if ~isempty(subx)
    nbRows = min(nbRows, length(suby));
end
if ~isempty(subx)
    nbColumns = min(nbColumns, length(subx));
end

str1 = 'Taille des blocks';
str2 = 'Block size';
p    = ClParametre('Name', Lang('Nb de colonnes', 'Number of columns'), ...
    'Value', nbColumns, 'MinValue', min(nbColumns, 100), 'MaxValue', nbColumns);
p(2) = ClParametre('Name', Lang('Nb de lignes', 'Number of rows'), ...
    'Value', nbRows,  'MinValue', min(nbRows, 100), 'MaxValue', nbRows);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Windows = a.getParamsValue;
