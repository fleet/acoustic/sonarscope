function [flag, listeFic, pathname, GridSize, Step, Tag, repImport, repExport] = params_HorizontalSlices(~, repImport, repExport)

persistent persistent_Step persistent_GridSize

GridSize = [];
Step     = [];
pathname = [];
Tag      = [];

str1 = 'SonarScope browser : s�lectionnez les fichier .xml "WCCube"';
str2 = 'SonarScope browser : Select the "WCCube" .xml files';
[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
    'RepDefaut', repImport, 'ChaineIncluse', '_WCCube', 'Entete', Lang(str1,str2), 'AllFilters', 1);
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers de coupes verticales';
str2 = 'Select or create the directory in which the vertical cutaway files will be created.';
[flag, pathname] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = pathname;

%% Grid size and vertical steps

if isempty(persistent_Step)
    Step = Inf;
else
    Step = persistent_Step;
end

if isempty(persistent_GridSize)
    GridSize = Inf;
else
    GridSize = persistent_GridSize;
end

p    = ClParametre('Name', 'Mosaic grid size',          'Unit', 'm', 'MinValue', 0, 'MaxValue', 200, 'Value', GridSize);
p(2) = ClParametre('Name', 'Vertical slices thickness', 'Unit', 'm', 'MinValue', 0, 'MaxValue', 200, 'Value', Step);
a = StyledParametreDialog('params', p, 'Title', 'Horizontal slices characteristics');
a.sizes = [0 -2    -1    -2    -1    -1    -1    25];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
paramsValue = a.getParamsValue;
GridSize = paramsValue(1);
Step     = paramsValue(2);

persistent_Step     = Step;
persistent_GridSize = GridSize;

%% Saisie du Tag


%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end
