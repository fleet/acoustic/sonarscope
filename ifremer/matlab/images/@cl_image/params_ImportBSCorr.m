function [flag, nomFicBSCorr, subConfigs, repImport] = params_ImportBSCorr(this, indImage, repImport)

nomFicBSCorr = [];
subConfigs   = [];

%% Test signature

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', cl_image.indGeometryType('PingBeam')); 
if ~flag
    return
end

%% Recherche du fichier BSCorr.txt

str1 = 'IFREMER - SonarScope : Nom du fichier BSCorr.txt � importer';
str2 = 'IFREMER - SonarScope : Name of the BSCorr.txt file to be imported';
[flag, nomFicBSCorr] = my_uigetfile('bscorr.txt', Lang(str1,str2), repImport);
if ~flag
    return
end
repImport = fileparts(nomFicBSCorr);

%% Lecture du fichier bscorr.txt

[flag, BSCorr, BSCorrInfo] = read_BSCorr(nomFicBSCorr); %#ok<ASGLU>
if ~flag
    return
end

%% V�rifications

SonarDescription = get_SonarDescription(this(indImage));
SounderName = get(SonarDescription, 'Sonar.Name');
[~, nomSimple, Ext] = fileparts(nomFicBSCorr);
switch BSCorrInfo.identFormat
    case 1 % Cas de l'EM710
        if ~strcmp(SounderName, 'EM710')
            str1 = sprintf('Le fichier %s correspond � un sondeur EM710 mais l''image de r�flectivit� provient d''un %s !', [nomSimple Ext], SounderName);
            str2 = sprintf('%s corresponds to an EM710 but the reflectivity image comes from an %s !', [nomSimple Ext], SounderName);
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        end
    case 2 % Cas des EM122 et EM302
        if ~(strcmp(SounderName, 'EM122') || strcmp(SounderName, 'EM302'))
            str1 = sprintf('Le fichier %s correspond � un sondeur EM710 mais l''image de r�flectivit� provient d''un %s !', [nomSimple Ext], SounderName);
            str2 = sprintf('%s corresponds to an EM710 but the reflectivity image comes from an %s !', [nomSimple Ext], SounderName);
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        end
end

%% S�lection de la courbe � importer

%{
strSwath = {'Single'; 'Double'};
str = {};
for k =1:length(BSCorr)
    if BSCorr(k).swathMode ~= 3
        str{end+1} = sprintf('Mode %d - Swath %s', BSCorr(k).pingMode, strSwath{BSCorr(k).swathMode}); %#ok<AGROW>
    end
end
[rep, flag] = my_listdlgMultiple('List of words', str, 'InitialValue', 1:length(str));
if ~flag
    return
end
subConfigs = ;
%}

