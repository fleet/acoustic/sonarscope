function [flag, LatLim, LonLim] = params_ImportEGM96(this)

LatLim = [];
LonLim = [];

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'noMessage');
if flag
    [ind, flag] = my_questdlg('Same geographical limits as the current image ?');
    if ~flag
        return
    end
    if ind == 1
        LonLim = this.XLim;
        LatLim = this.YLim;
        return
    end
end

p    = ClParametre('Name', 'Longitude West', 'Unit', 'deg', 'Value', -180);
p(2) = ClParametre('Name', 'Longitude East', 'Unit', 'deg', 'Value',  180);
p(3) = ClParametre('Name', 'Latitude South', 'Unit', 'deg', 'Value',  -90);
p(4) = ClParametre('Name', 'Latitude North', 'Unit', 'deg', 'Value',   90);

a = StyledSimpleParametreDialog('params', p, 'Title', 'Geographic limits');
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

LonLim = val(1:2);
LatLim = val(3:4);
