function [flag, window, method] = params_Interpol(this)

method = [];
    
[flag, window] = saisie_window([3 3], 'maxvalue', [51 51]);
if ~flag
    return
end

if get_LevelQuestion >= 3
    str1 = 'Type d''op�rateur';
    str2 = 'Type of operator';
    if isIntegerVal(this)
        listeMethods = {'max'; 'min'; 'median'};
        listeMethods1 = {'Max'; 'Min'; 'M�diane'};
        listeMethods2 = {'Max'; 'Min'; 'Median'};
        [choix, flag] = my_listdlg(Lang(str1,str2), Lang(listeMethods1,listeMethods2), 'SelectionMode', 'Single', 'InitialValue', 3, 'ColorLevel', 3);
    else
        listeMethods = {'mean'; 'max'; 'min'; 'median'};
        listeMethods1 = {'Moyenne'; 'Max'; 'Min'; 'M�diane'};
        listeMethods2 = {'Mean'; 'Max'; 'Min'; 'Median'};
        [choix, flag] = my_listdlg(Lang(str1,str2), Lang(listeMethods1,listeMethods2), 'SelectionMode', 'Single', 'InitialValue', 1, 'ColorLevel', 3);
    end
    if flag
        method = listeMethods{choix};
    else
        method = [];
    end
else
    method = 'mean';
end
