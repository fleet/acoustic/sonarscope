function [flag, indLayerConditionnel, Height, Width] = params_InterpolConditional(this, indImage) 

% persistent ParamFiltre

indLayerConditionnel = [];

%% S�lection du layer compl�mentaire

Height = 1;
Width  = 11;

str1 = 'Fen�tre d''interpolation';
str2 = 'Interpolating window';
p    = ClParametre('Name', 'Greatest std', 'Value', Height, 'MinValue', 1, 'MaxValue', 15);
p(2) = ClParametre('Name', 'Lowest std',   'Value', Width,  'MinValue', 5, 'MaxValue', 20);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
value = a.getParamsValue;

Height = value(1);
Width  = value(2);

%% S�lection du layer

indDataTypeConditions(1) = cl_image.indDataType('Heading');
[indLayers, nomsLayers] = findIndLayerSonar(this, indImage, 'DataType', indDataTypeConditions, 'OnlyOneLayer');

% [flag, indLayers, nomsLayers, ~, flag] = listeLayersSynchronises(this, indImage, ...
%     'TypeDonneUniquement', TypeDonneUniquement);
if ~flag
    return
end

switch length(indLayers)
    case 0
        str1 = 'Aucune image de m�me nature a �t� trouv�e';
        str2 = 'No similar image has been found.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    otherwise
        str1 = 'S�lectionnez la seconde image';
        str2 = 'Select second image';
        [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single', 'InitialValue', length(nomsLayers));
        if ~flag
            return
        end
        indLayerConditionnel = indLayers(choix);
end
