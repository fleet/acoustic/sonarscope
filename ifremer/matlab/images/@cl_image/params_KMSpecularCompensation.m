function [flag, flagRecomputeRn] = params_KMSpecularCompensation(this)

flagRecomputeRn = [];

%% Vérification des signatures

identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
flag = testSignature(this, 'DataType', [identRayPathSampleNb identTwoWayTravelTimeInSeconds], 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Test si le signaux BSN, BSO, TGVN et TVGCrossOver sont présents

flag(1) = isfield(this.Sonar, 'BSN')          && ~isempty(this.Sonar.BSN);
flag(2) = isfield(this.Sonar, 'BSO')          && ~isempty(this.Sonar.BSO);
flag(3) = isfield(this.Sonar, 'TVGN')         && ~isempty(this.Sonar.TVGN);
flag(4) = isfield(this.Sonar, 'TVGCrossOver') && ~isempty(this.Sonar.TVGCrossOver);

if ~all(flag)
    str1 = 'Cette image ne provient certainement pas d''un sondeur Kongsberg.';
    str2 = 'This image seems not to come from a Kongsberg sounder.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Questions

str1 = 'Recalcul de rn';
str2 = 'Recompute rn';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
flagRecomputeRn = (rep == 1);
