function [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = params_LambertCorrection(this, indImage, varargin)

[varargin, LookForBathy] = getPropertyValue(varargin, 'LookForBathy', 1); %#ok<ASGLU>

flag          = 1;
CasDepth      = [];
H             = [];
indLayerBathy = [];

%% Recherche de l'information de distance parcourue

identEmission = cl_image.indDataType('IncidenceAngle');
indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerEmission)
    identEmission = cl_image.indDataType('RxAngleEarth');
    indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
    if isempty(indLayerEmission)
        identEmission = cl_image.indDataType('TxAngle');
        indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
        if isempty(indLayerEmission)
            identEmission = cl_image.indDataType('RxBeamAngle');
            indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
            if isempty(indLayerEmission) % TODO : ajout pas tr�s heureux; il faut g�n�rer un layer d'angle d'"incidence" plut�t que �a
                identEmission = cl_image.indDataType('BeamPointingAngle');
                indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1, 'OnlyOneLayer');
            end
        end
    end
end

if is_PingSamples(this(indImage))
    CasDistance = 1; % Cas d'une donn�e sonar en distance oblique
    return
else
    if is_PingAcrossDist(this(indImage))
        CasDistance = 2; % Cas d'une donn�e sonar en distance projet�e
    else
        if isempty(indLayerEmission)
            flag = 0;
            CasDistance = 4; % On ne poss�de pas d'information
        else
            CasDistance = 3; % On poss�de l'angle d'emission
        end
    end
end

%% Recherche de l'information de hauteur

if LookForBathy
    identBathymetry = cl_image.indDataType('Bathymetry');
    [indLayerBathy, nomLayersBathy] = findIndLayerSonar(this, indImage, 'DataType', identBathymetry, ...
        'WithCurrentImage', 1, 'OnlyOneLayer');
    if isempty(indLayerBathy) % Il n'existe pas de layer "Depth"
        if isempty(indLayerEmission)
            if isempty(this(indImage).Sonar.Height) % Il existe une hauteur sonar
                CasDepth = 2;
            else % On ne peut pas retrouver d'info de hauteur
                str1 = 'Il n''existe pas d''information de hauteur. Voulez-vous continuer le traitement en definissant une hauteur moyenne ?';
                str2 = 'There is no "Height" data. Do you want to cintinue with a constant heigth that you will define yourself ?';
                [rep, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
                if (rep == 2)
                    flag = 0;
                    return
                end
                if rep == 1
                    [flag, H] = inputOneParametre(Lang('Valeur de la hauteur', 'Give the height'), Lang('Hauteur','Height'), ...
                        'Value', 1, 'Unit', 'm', 'MinValue', 0);
                    if ~flag
                        return
                    end
                    CasDepth = 4;
                else
                    flag = 0;
                    CasDepth = 5;
                    return
                end
            end
        else
            CasDepth = 3; % On poss�de l'angle d'emission
        end
    else
        CasDepth = 1; % On poss�de le layer de hauteur
    end
else
    CasDepth = 1; % On poss�de le layer de hauteur
end

if length(indLayerBathy) > 1
    str1 = 'Layers � utiliser :';
    str2 = 'Layers to use :';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomLayersBathy);
    if ~flag
        return
    end
    indLayerBathy = indLayerBathy(choix);
end
