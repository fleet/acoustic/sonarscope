function [flag, indLayerRange] = params_LambertCorrectionKM(this, indImage)

flag = 1;

%% Recherche d'un layer de type TwoWayTravelTimeInSeconds

indLayerRange = findIndLayerSonar(this, indImage, 'strDataType', 'TwoWayTravelTimeInSeconds', ...
    'WithCurrentImage', 1, 'OnlyOneLayer');

% On se rabat sur le layer RayPathSampleNb (sondeurs anciens au format V1)
if isempty(indLayerRange)
    indLayerRange = findIndLayerSonar(this, indImage, 'strDataType', 'RayPathSampleNb', ...
        'WithCurrentImage', 1, 'OnlyOneLayer');
end

if isempty(indLayerRange)
    indLayerRange = findIndLayerSonar(this, indImage, 'strDataType', 'TwoWayTravelTimeInSamples', ...
        'WithCurrentImage', 1, 'OnlyOneLayer');
end

if isempty(indLayerRange)
    str1 = 'Un layer "TwoWayTravelTimeInSeconds" ou "RayPathSampleNb" est nécessaire.';
    str2 = 'A layer of type "TwoWayTravelTimeInSeconds" or "RayPathSampleNb" in necessary.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
end
