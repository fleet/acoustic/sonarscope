function [flag, indLayerAcrossDist] = params_MBESAttitudeAnalysisRoll(this, indImage)

indLayerAcrossDist = [];

%% On v�rifie si le layer est bien de type "RxBeamAngle"

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', identBathymetry);
if ~flag
    return
end

%% Recherche du layer d'angles

DataType = cl_image.indDataType('AcrossDist');
indLayerAcrossDist = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerAcrossDist)
    flag = 0;
    return
end
