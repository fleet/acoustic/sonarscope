function [flag, DataSSP, subLayers] = params_MBES_CreationRxBeamAngleInSampleBeam(this)
        
DataSSP   = [];
subLayers = [];

%% On verifie si le layer est bien de type "RxBeamAngle"

identReflec = cl_image.indDataType('Reflectivity');
identPhase  = cl_image.indDataType('TxAngle');
flag = testSignature(this, 'DataType', [identReflec identPhase], ...
    'GeometryType', cl_image.indGeometryType('SampleBeam'), ...
    'Message', 'Pour realiser cette operation, il faut etre positionne sur un Layer de DataType="SampleBeam"');
if ~flag
    return
end

nomFic = this.InitialFileName;
[~, ~, ext] = fileparts(nomFic);

if strcmpi(ext, '.all')
	[flag, DataSSP] = SSc_ReadDatagrams(nomFic, 'Ssc_SoundSpeedProfile'); % Non test� ici

elseif strcmpi(ext, '.s7k')
    s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
    identSondeur = selectionSondeur(s7k);
    DataSSP = read_soundSpeedProfile(s7k, identSondeur);
end
