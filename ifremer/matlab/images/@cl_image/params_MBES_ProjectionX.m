function [flag, indReceptionBeam, indAcrossDistance, indRange, indLayers, resolutionX, MasquageDual] = params_MBES_ProjectionX(this, indImage, suby)

if nargout >= 6
    flagResolutionX = 1;
else
    flagResolutionX = 0;
end

indReceptionBeam  = [];
indAcrossDistance = [];
indRange          = [];
resolutionX       = [];
indLayers         = [];
MasquageDual      = 0;

TY = this(indImage).TagSynchroY;
TC = this(indImage).GeometryType;

if TC == cl_image.indGeometryType('PingAcrossSample')
    indLayers = getindLayers(this, indImage, cl_image.indGeometryType('PingAcrossSample'));
    resolutionX = get(this(indImage), 'SonarResolutionD');
    [flag, resolutionX] = getResolutionX(this(indImage), suby, resolutionX(1), flagResolutionX);
    if ~flag
        return
    end
    MasquageDual = getMasquageDual(this(indImage));
    return
end
    
flag = testSignature(this(indImage), ...
    'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingBeam')]);
if ~flag
    return
end

%% Recherche du layer ReceptionBeam syncrhonise en X et Y

% TODO : cr�er un Sonar.Family = 4 pour le Geoswath
SonarDescription = get(this(indImage), 'SonarDescription');
% SonarIdent = get(SonarDescription, 'Sonar.Ident')
% Sonar.Family
SonarName = get(SonarDescription, 'SonarName');
if strcmp(SonarName, 'GeoSwath+') || strcmp(SonarName, 'GeoSwath')
    indReceptionBeam = [];
    resolutionX = get(SonarDescription, 'Proc.RangeResol');
%     resolutionX = resolutionX(1);
else
    if TC == cl_image.indGeometryType('PingSamples')
        identReceptionBeam = cl_image.indDataType('RxBeamIndex');
        indReceptionBeam = findIndLayerSonar(this, indImage, 'DataType', identReceptionBeam, 'OnlyOneLayer', 'AtLeastOneLayer');
        if isempty(indReceptionBeam)
            flag = 0;
            return
        end
        resolutionX = get(this(indImage), 'SonarResolutionX');
        resolutionX = resolutionX(1);
    else
        indReceptionBeam = [];
        resolutionX = get(this(indImage), 'SonarResolutionD');
        if isempty(resolutionX)
            resolutionX = 100;
        else
            resolutionX = resolutionX(1);
        end
    end
end

%% On recherche le layer "AcrossDistance" synchronise en Y

identAcrossDistance = cl_image.indDataType('AcrossDist');
nomLayers = {};
for k=1:length(this)
    if k == indImage
        continue
    end
    GeometryType = this(k).GeometryType;
    if strcmp(SonarName, 'GeoSwath+') || strcmp(SonarName, 'GeoSwath')
        if strcmp(TY, this(k).TagSynchroY) && ...
                (this(k).DataType == identAcrossDistance) && ...
                ((GeometryType == cl_image.indGeometryType('PingSamples')) || (GeometryType == cl_image.indGeometryType('PingBeam')))
%                 (GeometryType == cl_image.indGeometryType('PingSamples'))
            indAcrossDistance(end+1) = k; %#ok<AGROW>
            nomLayers{end+1} = this(k).Name; %#ok<AGROW>
        end
    else
        if strcmp(TY, this(k).TagSynchroY) && ...
                (this(k).DataType == identAcrossDistance) && ...
                (GeometryType == cl_image.indGeometryType('PingBeam'))
            indAcrossDistance(end+1) = k; %#ok<AGROW>
            nomLayers{end+1} = this(k).Name;  %#ok<AGROW>
        end
    end
end
if isempty(indAcrossDistance)
    str = sprintf('No layer %d (%s) of geometry PingBeam synchronised with this image.', identAcrossDistance, cl_image.strDataType{identAcrossDistance});
    my_warndlg(str, 1);
    flag = 0;
    return
elseif length(indAcrossDistance) > 1
    str1 = 'Quelle image de type "AcrossDistance" voulez-vous utiliser ?';
    str2 = 'What "AcrossDistance" layer do you want to use ?';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomLayers);
    if ~flag
        return
    end
    indAcrossDistance = indAcrossDistance(choix);
end

%% Cas du Geoswath

if strcmp(SonarName, 'GeoSwath+') || strcmp(SonarName, 'GeoSwath')
    ResolTransversaleImage = resol_trans_image(SonarDescription, 1000, 60);
    str1 = 'R�solution transversale';
    str2 = 'Across resolution';
    
    [flag, resolutionX] = inputOneParametre(Lang(str1,str2), 'Swath step', ...
        'Value', max([ResolTransversaleImage/2 resolutionX]), 'Unit', 'm', 'MinValue', resolutionX, 'MaxValue', 1);
    if ~flag
        return
    end
    
    indLayers = getindLayers(this, indImage, cl_image.indGeometryType('PingSamples'));
    return
end

%% On recherche le layer "Range" synchronise en Y

identRange = cl_image.indDataType('RayPathSampleNb'); % EN
nomLayers = {};
for k=1:length(this)
    if k == indImage
        continue
    end
    if strcmp(TY, this(k).TagSynchroY) && ...
        (this(k).DataType == identRange) && ...
        (this(k).GeometryType == cl_image.indGeometryType('PingBeam')) %BathyFais
        indRange(end+1) = k; %#ok<AGROW>
        nomLayers{end+1} = this(k).Name; %#ok<AGROW>
    end
end
if isempty(indRange)
    identRange = cl_image.indDataType('RayPathLength');
    identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
    nomLayers = {};
    for k=1:length(this)
        if k == indImage
            continue
        end
        if strcmp(TY, this(k).TagSynchroY) && ...
                (any(this(k).DataType == [identRange identTwoWayTravelTimeInSeconds])) && ...
                (this(k).GeometryType == cl_image.indGeometryType('PingBeam')) %BathyFais
            indRange(end+1) = k; %#ok<AGROW>
            nomLayers{end+1} = this(k).Name; %#ok<AGROW>
        end
    end
end
if isempty(indRange)
    str = sprintf('No layer %d (%s) synchronised with this image.', identRange, cl_image.strDataType{identRange});
    my_warndlg(str, 1);
    flag = 0;
    return
elseif length(indRange) > 1
    str1 = 'Quelle image de type "Range" voulez-vous utiliser ?';
    str2 = 'What "Range" layer do you want to use ?';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomLayers);
    if ~flag
        return
    end
    indRange = indRange(choix);
end

indLayers = getindLayers(this, indImage, cl_image.indGeometryType('PingAcrossSample'));
[flag, resolutionX] = getResolutionX(this(indImage), suby, resolutionX, flagResolutionX);
MasquageDual = getMasquageDual(this(indImage));


% Masquage des faisceaux en recouvrement
function MasquageDual = getMasquageDual(this)

Sonar = get_SonarDescription(this);
SonarName = get(Sonar, 'Sonar.Name');
switch SonarName
    %%%%% Mettre � jour cette liste dans la fonction sonar_bathyFais2SonarX
    % Il vaudrait mieux cr�er une m�thode isdual dans la classe cl_sounder
    case {'EM300'; 'EM302'; 'EM120'; 'EM122'; 'EM710'; 'EM1002'; 'EM2040'; 'EM2040S'; 'Reson7125'; 'Reson7111'; 'Reson7150_12kHz'; 'Reson7150_24kHz'}
        MasquageDual = 0;
    case {'EM12D'; 'EM3000D'; 'EM3002D'}
        [rep, flag] = my_questdlg(Lang('Masquage de beams redondants si sondeur dual?', ...
            'Mask overlapping beams if dual sounder ?'));
        if ~flag
            return
        end
        MasquageDual = (rep == 1);
    otherwise
        MasquageDual = 0;
end


% Choix de la resolution
function [flag, resolutionX] = getResolutionX(this, suby, resolutionX, flagResolutionX)

SonarDescription = get(this, 'SonarDescription');
% On estimre la resolution transversale de l'imagerie en consid�rant un
% angle de 60 degr�s et une profondeur de 1000m (la profondeur ne change
% rien) c'est suffisant sinon il faufrait chercher la valeur moyenne de
% l'ensemble des max des angles des pings
ResolTransversaleImage = resol_trans_image(SonarDescription, 1000, 60);

FishLatitude  = get(this, 'SonarFishLatitude');
FishLongitude = get(this, 'SonarFishLongitude');
Carto         = get(this, 'Carto');
if isempty(Carto)
    str1 = 'Aucunz cartographie n''est d�finie pour cette image.';
    str2 = 'No cartography is defined for this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Calcul du pas de grille � partir de la longueur moyenne parcourue entre 2 pings

Lat = FishLatitude(suby);
Lon = FishLongitude(suby);
if all(isnan(Lat)) || all(isnan(Lon))
    [flag, resolutionX] = inputOneParametre('Swath projection parametres', 'Swath step', ...
        'Value', 10, 'Unit', 'm', 'MinValue', 0, 'MaxValue', 100);
    if ~flag
        return
    end
else
    [xFish, yFish] = latlon2xy(Carto, Lat, Lon);
    sub = ~isnan(xFish) & ~isnan(yFish);
    N = sum(sub);
    ResolMax = sum(sqrt(diff(xFish(sub)).^2 + diff(yFish(sub)).^2)) / N;
    if flagResolutionX
        msg = {};
        msg{end+1} = sprintf('Next window will ask you what swath resolution you want.');
        msg{end+1} = sprintf('Here are some information in order to help you to do the good choice :');
        msg{end+1} = ' ';
        msg{end+1} = sprintf('Signal sampling delivers a pixel whose resolution is %f m', resolutionX);
        msg{end+1} = sprintf('Physical resolution estimated for this sounder (at 60 degres) is %f m', ResolTransversaleImage);
        msg{end+1} = sprintf('Longitudinal recurence of pings is %f m', ResolMax);
        msg{end+1} = ' ';
        msg{end+1} = sprintf('If you want to get a precise image use physical resolution.');
        msg{end+1} = sprintf('If you want an image whose geometry is similar to the future mosaic chose longitudinal recurrence.');
        edit_infoPixel(msg, [], 'PromptString', 'Help to choose Swath resolution')
        
        [flag, resolutionX] = inputOneParametre('Swath projection parametres', 'Swath step', ...
            'Value', max([ResolMax/2 ResolTransversaleImage resolutionX]), 'Unit', 'm', 'MinValue', 0, 'MaxValue', 1000);
        if ~flag
            return
        end
    end
end
flag = 1;



function indLayers = getindLayers(this, indImage, TC)

% Recherche des layers synchronises en Y et de GeometryType=cl_image.indGeometryType('PingBeam')='BathyFais'  

TY = this(indImage).TagSynchroY;

[indLayers, nomsLayers] = findIndLayerSonarSynchroniseEnY(this, indImage, 'GeometryType', TC);
if length(indLayers) > 1
    InitialValue     = [];
    identTxAngle     = cl_image.indDataType('TxAngle');
    identRxBeamAngle = cl_image.indDataType('RxBeamAngle');
    identTxBeamIndex = cl_image.indDataType('TxBeamIndex');
    for k=1:length(indLayers)
        if k == indImage
            continue
        end
        if strcmp(TY, this(indLayers(k)).TagSynchroY) && ...
                (this(indLayers(k)).DataType == identTxAngle) && ...
                (this(indLayers(k)).GeometryType == cl_image.indGeometryType('PingBeam'))
            InitialValue(end+1) = k; %#ok<AGROW>
        end
        if strcmp(TY, this(indLayers(k)).TagSynchroY) && ...
                (this(indLayers(k)).DataType == identRxBeamAngle) && ...
                (this(indLayers(k)).GeometryType == cl_image.indGeometryType('PingBeam'))
            InitialValue(end+1) = k; %#ok<AGROW>
        end
        if strcmp(TY, this(indLayers(k)).TagSynchroY) && ...
                (this(indLayers(k)).DataType == identTxBeamIndex) && ...
                (this(indLayers(k)).GeometryType == cl_image.indGeometryType('PingBeam'))
            InitialValue(end+1) = k; %#ok<AGROW>
        end
    end
    if isempty(InitialValue)
        InitialValue = 0;
    end
    str1 = 'Autres layers pouvant �tre trait�s en m�me temps ?';
    str2 = 'Other layers you could process at the same time ?';
    [choix, flag] = my_listdlgMultiple(Lang(str1,str2), nomsLayers, 'InitialValue', InitialValue);
    if ~flag
        return
    end
    indLayers = indLayers(choix);
end
