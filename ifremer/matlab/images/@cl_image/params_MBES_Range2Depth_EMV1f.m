function [flag, indLayerTxAngle, indLayerRollRx, indLayerPitchRx, indLayerTxTiltAngle] = params_MBES_Range2Depth_EMV1f(this, indImage)

indLayerTxAngle     = [];
indLayerRollRx      = [];
indLayerPitchRx     = [];
indLayerTxTiltAngle = [];
 
%% On v�rifie si le layer est bien de type "RxBeamAngle"

identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', identTwoWayTravelTimeInSeconds);
if ~flag
    messageLayers
    return
end

%% Recherche du layer d'angles

% DataType = cl_image.indDataType('TxAngle');
DataType = cl_image.indDataType('RxAngleEarth');
indLayerTxAngle = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerTxAngle)
    messageLayers
    flag = 0;
    return
end

%% Recherche du layer d'angles

DataType = cl_image.indDataType('Roll');
indLayerRollRx = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerRollRx)
    messageLayers
    flag = 0;
    return
end

%% Recherche du layer d'angles

DataType = cl_image.indDataType('Pitch');
indLayerPitchRx = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerPitchRx)
    messageLayers
    flag = 0;
    return
end

%% Recherche du layer TxTiltAngle

DataType = cl_image.indDataType('TxTiltAngle');
indLayerTxTiltAngle = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerTxTiltAngle)
    messageLayers
    flag = 0;
    return
end


function messageLayers

str1 = 'L''image courante doit �tre de type "TwoWayTravelTimeInSeconds", les layeurs "RxAngleEarth", "Roll", "Pitch" et "TxTiltAngle" doivent �tre pr�sents.';
str2 = 'The current image must be of type "TwoWayTravelTimeInSeconds", the layers "RxAngleEarth", "Roll", "Pitch" and "TxTiltAngle" must be loaded.';
my_warndlg(Lang(str1,str2), 1);

