function [flag, indLayerTxAngle, indLayerRollRx, indLayerPitchRx] = params_MBES_Range2Depth_Reson(this, indImage)

indLayerTxAngle = [];
indLayerRollRx  = []; % TODO
indLayerPitchRx = []; % TODO

%% On v�rifie si le layer est bien de type "RxBeamAngle"

% identRayPathRayPathLength = cl_image.indDataType('RayPathLength');
identRayPathRayPathLength = cl_image.indDataType('TwoWayTravelTimeInSeconds');
flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', identRayPathRayPathLength);
if ~flag
    return
end

%% Recherche du layer d'angles

DataType = cl_image.indDataType('TxAngle');
% DataType = cl_image.indDataType('RxAngleEarth');
indLayerTxAngle = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerTxAngle)
    flag = 0;
    return
end

%% Recherche du layer BeamAzimuthAngle

%{
DataType = cl_image.indDataType('BeamAzimuthAngle');
indLayerBeamAzimuthAngle = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerBeamAzimuthAngle)
    flag = 0;
    return
end
%}
