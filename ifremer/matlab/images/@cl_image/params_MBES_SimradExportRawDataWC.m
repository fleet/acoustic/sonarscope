function [flag, Names, MaskAfterDetection, repExport] = params_MBES_SimradExportRawDataWC(this, repExport)
 
Names              = [];
MaskAfterDetection = 0;

flag = testSignature(this, 'GeometryType', [cl_image.indGeometryType('PingBeam'), cl_image.indGeometryType('PingAcrossDist')]);
if ~flag
    return
end

InitialFileFormat = this.InitialFileFormat;
if ~strcmp(InitialFileFormat, 'SimradAll')
    str1 = 'Cette op�ration n''est faisable que sur des donn�es provenant d''un sondeur Kongsberg pour le moment.';
    str2 = 'This process can be done only on images coming from Kongsberg for the moment.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% R�pertoire cible

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers.';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDir1] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDir1;

%% Raw / Compensated Data

nomFicAll = this.InitialFileName;

str1 = 'Type des donn�es de la colonne d''eau';
str2 = 'Data type for the water column';
str = {Lang('Donn�es brutes', 'Raw data'); Lang('Donn�es compens�es', 'Compensated data')};
[TypeDataWC, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 2);
if ~flag
    return
end

[~, nomFic] = fileparts(nomFicAll);

if any(TypeDataWC == 1)
    nomFicXmlRawDefault = fullfile(nomDir1, [nomFic '_Raw_SampleBeam.xml']);
    Names.nomFicXmlRaw = nomFicXmlRawDefault;
    [flag, Names.nomFicXmlRaw] = getNomFicXml(Names.nomFicXmlRaw);
    if ~flag
        return
    end
    nomFicXml = Names.nomFicXmlRaw;
    [Tag, pos] = detectionTag(nomFicXmlRawDefault, Names.nomFicXmlRaw);
else
    Tag = [];
    Names.nomFicXmlRaw = [];
end

if any(TypeDataWC == 2)
    Names.nomFicXmlComp = fullfile(nomDir1, [nomFic '_Comp_SampleBeam.xml']);
    if ~isempty(Tag)
        Names.nomFicXmlComp = [Names.nomFicXmlComp(1:(pos-1)) Tag  Names.nomFicXmlComp(pos:end)];
    end
    [flag, Names.nomFicXmlComp] = getNomFicXml(Names.nomFicXmlComp);
    if ~flag
        return
    end
    nomFicXml = Names.nomFicXmlComp;
else
    Names.nomFicXmlComp = [];
end

[~, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_Raw', '');
nomFic = strrep(nomFic, '_Comp', '');

nomVar = {Lang('Nom de la donn�e dans GLOBE', 'Data name in GLOBE')};
value = {nomFic};
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
Names.AreaName = value{1};

%% Masquage au del� de la d�tection

[flag, MaskAfterDetection] = question_MaskWCBeyondDetection;
if ~flag
    return
end



function [flag, nomFicXml] = getNomFicXml(nomFicXml)

flag = 1;
while 1
    [flag, nomFicXml] = my_uiputfile('*.xml', 'Give a file name', nomFicXml);
    if ~flag
        return
    end    
    
    [nomDirExport, nomDir] = fileparts(nomFicXml);
    nomDirExport = fullfile(nomDirExport, nomDir);
    if exist(nomDirExport, 'dir')
        str1Fr = sprintf('Le r�pertoire "%s" existe d�j�, que voulez-vous faire ?', nomFicXml);
        str1Us = sprintf('"%s" already exists, what do you want to do ?', nomFicXml);
        rep1Fr = 'Le supprimer';
        rep1Us = 'Remove it';
        rep2Fr = 'Renommer le fichier XML';
        rep2Us = 'Rename the XML file';
        [rep, flag] = my_questdlg(Lang(str1Fr, str1Us), Lang(rep1Fr, rep1Us), Lang(rep2Fr, rep2Us), 'Init', 2);
        if ~flag
            return
        end
        
        if rep == 1
            flag = rmdir(nomDirExport, 's');
            if flag
                break
            else
                str1 = 'Il est probable que ces fichiers soient ouverts dans GLOBE.';
                str2 = 'Check if these files are open in GLOBE.';
                messageErreurFichier(nomDirExport, 'WriteFailure', 'Message', Lang(str1,str2));
                return
            end
        end
    else
        break
    end
end
