function [flag, Step, SeuilProba] = params_MBES_maskFromAmplitudeDetection(this)

Step       = [];
SeuilProba = [];

%% Test signature

flag = testSignature(this, 'DataType', cl_image.indDataType('DetectionType'), ...
    'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%%

str1 = sprintf('Param�tre de r�glage du masquage de sondes\n� partir de la d�tection d''amplitude');
str2 = sprintf('Parameters of sounding mask\nfrom amplitude detection');
p    = ClParametre('Name', Lang('Nombre de pings par blocks', 'Nb of pings per blocks'), ...
    'Value', 50, 'MinValue', 10);
p(2) = ClParametre('Name', Lang('Seuil', 'Threshold'), ...
    'Value', 0.5, 'MinValue', 0.1, 'MaxValue', 0.9);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

Step       = val(1);
SeuilProba = val(2);
