function [flag, listeprofils, listeprofilsAngle, pasx, pasy, CoveringPriority] = params_MosaiqueAssemblage(this, indImage, varargin)

[varargin, listeprofils] = getPropertyValue(varargin, 'listeprofils', []); %#ok<ASGLU>

pasx             = [];
pasy             = [];
CoveringPriority = 1;

DataTypeAssocie = {'TxAngle'; 'RxBeamAngle'; 'BeamPointingAngle'; 'IncidenceAngle'; 'RxAngleEarth'};
[flag, listeprofils, listeprofilsAngle] = listePairesLayersSynchronises(this, indImage, DataTypeAssocie, ...
    'listeprofils', listeprofils, 'QuestionAssemblageMosaic');
if ~flag
    return
end

%% Pas de la grille

for k=1:length(listeprofils)
    nomsImages{k} = this(listeprofils(k)).Name;  %#ok<AGROW>
    GT = this(listeprofils(k)).GeometryType;
    XStep(k) = get(this(listeprofils(k)), 'XStep'); %#ok<AGROW>.
    YStep(k) = get(this(listeprofils(k)), 'YStep'); %#ok<AGROW>.
    if GT == cl_image.indGeometryType('LatLong')
        Y(k) = get(this(listeprofils(k)), 'YStepMetric'); %#ok<AGROW>.
        strYStep{k} = sprintf('Grid size = %f : %s', Y(k), nomsImages{k}); %#ok<AGROW>.
    else
        Y(k) = YStep(k); %#ok<AGROW>
        strYStep{k} = sprintf('Grid size = %f : %s', YStep(k), nomsImages{k}); %#ok<AGROW>.
    end
    EcartRelatif = (Y-Y(1)) ./ (Y+Y(1));
end

if any(abs(EcartRelatif)  > 1e-6)
    str1 = 'Les images ont différentes résolutions, la quelle voulez-vous utiliser ?';
    str2 = 'The images have different resolutions. Which one do you want to use ?';
    [rep, flag] = my_listdlg(Lang(str1,str2), strYStep, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    pasy = get(this(listeprofils(1)), 'YStep'); % Al'essai (ligne 5 commentée)
    Coef = pasy / YStep(rep);
    pasy = pasy / Coef;
    
    subXStepEquivalent = find(nearlyEqual(EcartRelatif, EcartRelatif(rep), 0.5));
    % On prend le pas en x le plus grand pour éviter un défaut d'image :
    % les pas en x peuvent varier légèrement d'une image à une autre car
    % ils sont dépendants de la longitude moyenne de l'image. Pour éviter
    % que certaines lignes soient oubliées par la projection (très léger
    % sur-échantillonage)
    [~, OrdreXStep] = sort(XStep(subXStepEquivalent), 'descend');
    pasx = XStep(subXStepEquivalent(OrdreXStep(1)));
    
else
    pasy = get(this(listeprofils(1)), 'YStep');
    
    % On prend le pas en x le plus grand pour éviter un défaut d'image :
    % les pas en x peuvent varier légèrement d'une image à une autre car
    % ils sont dépendants de la longitude moyenne de l'image. Pour éviter
    % que certaines lignes soient oubliées par la projection (très léger
    % sur-échantillonage)
    [~, OrdreXStep] = sort(XStep, 'descend');
    pasx = XStep(OrdreXStep(1));
end

%% Gestion des recouvrements

if isempty(listeprofilsAngle)
    strFR = 'Ordre des images à assembler (la 1ère est l''image de support, etc.)';
    strUS = 'Order of Images to be assembled (first is the bottom layer, etc.)';
    [flag, ListeOut, Ordre] = uiOrdreItemsPlus(nomsImages, ...
        'Intitule', Lang(strFR, strUS)); %#ok<ASGLU>
    listeprofils = listeprofils(Ordre);
    
    str1 = 'Priorité donnée pour le recouvrement de profil : ';
    str2 = 'Priority to be used for recovering profiles : ';
    [CoveringPriority, flag] = my_listdlg(Lang(str1,str2), ...
        {Lang('Remplacement', 'Overwrite'); ...
        Lang('Valeur Max', 'Max');  ...
        Lang('Valeur Min', 'Min');...
        Lang('Somme', 'Sum');...
        Lang('Mixage', 'Blend')});
    if ~flag
        return
    end
else
    str1 = 'Priorité donnée pour le recouvrement de profil : ';
    str2 = 'Priority to be used for recovering profiles : ';
    [CoveringPriority, flag] = my_questdlg(Lang(str1,str2), ...
        Lang('Centre', 'Center'), Lang('Extrémités', 'Borders'));
    if ~flag
        return
    end
end
