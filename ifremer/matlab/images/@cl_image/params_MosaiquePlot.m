function [flag, Lon, Lat, Z, ThreeD, ExaVert, TagFig, hAxe] = params_MosaiquePlot(this, indImage, subx, suby)

persistent ExaVert_persistent

Lon     = [];
Lat     = [];
Z       = [];
ExaVert = 1;
ThreeD  = 0;
hAxe    = [];
TagFig  = [];

flag = testSignature(this(indImage), 'GeometryType', 'PingXxxx');
if ~flag
    return
end

str1 = 'Type de géométrie';
str2 = 'Type of geometry';
strTypeGeometry = {'LatLong', 'GeoYX'};
[typeGeometry, flag] = my_listdlg(Lang(str1,str2), strTypeGeometry, 'InitialValue', 2, 'SelectionMode', 'Single');
if ~flag
    return
end

TagFig = ['MosaicPlot_', strTypeGeometry{typeGeometry}];

if typeGeometry == 2
    
    %% Vérification d'existence d'un layer de GeoX

    identLayerGeoX = cl_image.indDataType('GeoX');
    [indLayerGeoX, nomLayersGeoX] = findIndLayerSonar(this, indImage, 'DataType', identLayerGeoX);
    if isempty(indLayerGeoX)
%         str1 = 'Il n''existe pas de layer de type "GeoX", je le cree a la volee.';
%         str2 = 'No "GeoX" layer exists, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to createa  permanent "GeoX" layer';
%         str = Lang(str1,str2);
%         my_warndlg(str, 0, 'Tag', 'I create a transitory one', 'TimeDelay', 10);
    else
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
        if length(indLayerGeoX) > 1
            str1 = 'Il existe plusieurs images de type "GeoX", la quelle voulez-vous utiliser ?';
            str2 = 'Many "GeoX" layers exist. Which one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersGeoX, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            indLayerGeoX = indLayerGeoX(rep);
        end
    end
    
    %% Vérification d'existance d'un layer de GeoY

    identLayerGeoY = cl_image.indDataType('GeoY');
    [indLayerGeoY, nomLayersGeoY] = findIndLayerSonar(this, indImage, 'DataType', identLayerGeoY);
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
    if isempty(indLayerGeoY)
%         str1 = 'Il n''existe pas de layer de type "GeoY", je le cree a la volee.';
%         str2 = 'No "GeoY" layer exists, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to createa  permanent "GeoY" layer';
%         my_warndlg(Lang(str1,str2), 0, 'Tag', 'I create a transitory one', 'TimeDelay', 10);
    else
        if length(indLayerGeoY) > 1
            str1 = 'Il existe plusieurs images de type "GeoY", la quelle voulez-vous utiliser ?';
            str2 = 'Many "GeoY" layers exist. Which one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersGeoY, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            indLayerGeoY = indLayerGeoY(rep);
        end
    end
    
    if ~isempty(indLayerGeoX) && ~isempty(indLayerGeoY)
        Lat = this(indLayerGeoY);
        Lon = this(indLayerGeoX);
        str1 = 'Représentation 3D ?';
        str2 = '3D representation ?';
        [ThreeD, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
        if ~flag
            return
        end
        if ThreeD == 1
            Z = getElevation(this, indImage);
        end
        [flag, hAxe] = findFig('Tag', TagFig);
        return
    end
end

%% Vérification d'existence d'un layer de latitude

identLayerLat = cl_image.indDataType('Latitude');
[indLayerLat, nomLayersLatitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLat);
if isempty(indLayerLat)
%     str1 = 'Il n''existe pas de layer de type "Latitude", je le cree a la volee.';
%     str2 = 'No "Latitude" layer exists, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to createa  permanent "Latitude" layer';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'I create a transitory one', 'TimeDelay', 10);
else
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
    if length(indLayerLat) > 1
        str1 = 'Il existe plusieurs images de type "Latitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Latitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLatitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLat = indLayerLat(rep);
    end
end

%% Vérification d'existence d'un layer de longitude

identLayerLon = cl_image.indDataType('Longitude');
[indLayerLon, nomLayersLongitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLon);
if isempty(indLayerLon)
%     str1 = 'Il n''existe pas de layer de type "Longitude", je le cree a la volee.';
%     str2 = 'No "Longitude" layer exists, I create a transitory one. If you have to do this processing a lot of times on this image it would be judicious to createa  permanent "Longitude" layer';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'I create a transitory one', 'TimeDelay', 10);
else
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
    if length(indLayerLon) > 1
        str1 = 'Il existe plusieurs images de type "Longitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Longitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLongitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLon = indLayerLon(rep);
    end
end


if isempty(indLayerLat) || isempty(indLayerLon)
    [flag, a] = sonarCalculCoordGeo(this, indImage, 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
    Lat = a(1);
    Lon = a(2);
    clear a
else
%     Lat = this(indLayerLat);
%     Lon = this(indLayerLon);

    % ---------------------
    % Extraction des images

    x = get(this(indImage), 'x');
    y = get(this(indImage), 'y');
    x = x(subx);
    y = y(suby);
    Lat = extraction(this(indLayerLat), 'x', x, 'y', y);
    Lon = extraction(this(indLayerLon), 'x', x, 'y', y);
end

if typeGeometry == 2
    [flag, a] = sonar_coordXY(this(indImage), Lat, Lon, 'subx', subx, 'suby', suby);
    if flag
        Lat = a(2);
        Lon = a(1);
    end

    str1 = 'Représentation 3D ?';
    str2 = '3D representation ?';
    [ThreeD, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if ThreeD == 1
        Z = getElevation(this, indImage);
        
        if ~isempty(ExaVert_persistent)
            ExaVert = ExaVert_persistent;
        end
        [flag, ExaVert] = inputOneParametre('Vertical exageration', 'Vertical exageration', ...
            'Value', ExaVert, 'MinValue', 0.001, 'MaxValue', 1000);
        if ~flag
            return
        end
        ExaVert_persistent = ExaVert;
    end
end
[flag, hAxe] = findFig('Tag', TagFig);


function Z = getElevation(this, indImage)

TD = this(indImage).DataType;
identLayerBathy = cl_image.indDataType('Bathymetry');
if TD == identLayerBathy
    Z = [];
else
    indLayerBathy = findIndLayerSonar(this, indImage, 'DataType', identLayerBathy);
    if length(indLayerBathy) == 1
        Z = this(indLayerBathy);
    else
        identBathymetry  = cl_image.indDataType('Bathymetry');
        indLayerBathymetry = findIndLayerSonar(this, indImage, 'DataType', identBathymetry, 'OnlyOneLayer');
        Z = this(indLayerBathymetry);
    end
end
