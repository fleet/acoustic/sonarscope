function [flag, indLayers, indLayersMasque, zone, valMask] = params_MultiImagesMasquagePaires(this, indImage)

indLayersMasque = [];
zone            = [];
valMask         = 1:255;

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'memeDataType');
if ~flag
    return
end

str1 = 'Liste des masques';
str2 = 'Liste of masks';
[choix, flag] = my_listdlgMultiple(Lang(str1,str2), nomsLayers);
if ~flag
    return
end

indLayers = indLayers(choix);
indLayersMasque = zeros(size(indLayers));
identMask = cl_image.indDataType('Mask');
for k=1:length(indLayers)
    % C'est la �%*&~# : il faur repenser compl�tementtout �a
    [flag, indLayerMaskAssocie] = listeLayersSynchronises(this, indLayers(k), 'TypeDonneUniquement', identMask, 'memeInitialFileName', 1); % comment� par JMA le 19/11/2018 �au NIWA
    if ~flag
        return
    end
    if isempty(indLayerMaskAssocie)
        [flag, indLayerMaskAssocie] = listeLayersSynchronises(this, indLayers(k), 'TypeDonneUniquement', identMask, 'memeInitialImageName', 1);
        if ~flag
            return
        end
        if isempty(indLayerMaskAssocie)
            [flag, indLayerMaskAssocie] = listeLayersSynchronises(this, indLayers(k), 'TypeDonneUniquement', identMask, 'memeImageName', 1);
            if ~flag
                return
            end
            if isempty(indLayerMaskAssocie)
                continue
            end
        end
    end
    if length(indLayerMaskAssocie) > 1
        str1 = sprintf('Plusieurs masques ont �t� trouv�s pour "%s", c''est le dernier qui est pris en compte.\nFaites du m�nage dans la liste des images si cela pose un probl�me.', nomsLayers{k});
        str2 = sprintf('Several masks were found for "%s", the last one is taking into account.\nClean the list of images if necessary.', nomsLayers{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'TropDeMasques_GP_ROI_MultiImages_MaskAppairedImages');
    end
    indLayersMasque(k) = indLayerMaskAssocie(end);
end

sub = find(indLayersMasque == 0);
if ~isempty(sub)
    str1 = 'Certaines images n''ont pas trouv�es leurs masques.';
    str2 = 'Some images are not linked to masks.';
    my_warndlg(Lang(str1,str2), 1);
    indLayers(sub) = [];
    indLayersMasque(sub) = [];
    if isempty(indLayers)
        flag = 0;
        return
    end
end

%% Valeurs du masque

str1 = 'Voulez vous masquer les images avec des valeurs particuli�res des masques ? Sinon, l''ensemble des valeurs seront prises.';
str2 = 'Do you want to use particular values from the masks ? Otherwise, all the values will be used.';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    nomVar = {'Val : Ex 1 2 4:6'};
    value = {'1'};
    [value, flag] = my_inputdlg(nomVar, value);
    if ~flag
        return
    end
    valMask = str2num(value{1}); %#ok<ST2NM>
end

%% Masquage Int�rieur ou Exterieur

[zone, flag] = my_questdlg(Lang('Quelle partie voulez-vous conserver ?', 'What part do you want to keep ?'), ...
    Lang('Int�rieur', 'Inside'), Lang('Ext�rieur', 'Outside'));
