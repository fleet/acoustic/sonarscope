function [flag, ListeFicErs, repImport, repExport] = params_NORBIT_CompositMosaic(~, repImport, repExport)

[flag, ListeFicErs, repImport] = uiSelectFiles('ExtensionFiles', '.ers', 'RepDefaut', repImport, ...
    'ChaineIncluse', 'kHz', 'InitGeometryType', cl_image.indGeometryType('LatLong'), 'InitDataType', cl_image.indDataType('Reflectivity'));
if ~flag
    return
end

str1 = 'Répertoire d''export des images composites';
str2 = 'Output directory of composit images';
nomDir = uigetdir(repExport, Lang(str1,str2));
if isequal(repExport, 0)
    return
end
repExport = nomDir;
