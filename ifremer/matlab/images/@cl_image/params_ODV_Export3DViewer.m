function [flag, listFileNames, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, ...
    IdentPartData, TailleMax, Video, repImport, repExport] = params_ODV_Export3DViewer(this, repImport, repExport)

persistent persistent_CLimSubbottom persistent_CLimWC persistent_CLimBoth persistent_ColormapIndex
persistent persistent_Video

nomDirOut     = [];
OrigineCLim   = [];
CLim          = [];
Tag           = [];
ColormapIndex = [];
IdentPartData = [];
TailleMax     = Inf;
Video         = 2;

%% S�lection des fichiers ODV

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', {'.odv'; '.ODV'}, 'AllFilters', 1, 'RepDefaut', repImport);
if ~flag
    return
end

%% Type de donn�es � traiter

str1 = 'Choix de la partie � exporter.';
str2 = 'Select the part of the data you want to export.';
str{1} = Lang('Direction', 'Direction');
str{2} = Lang('Magnitude', 'Magnitude');
str{3} = Lang('Echo Int�gration ', 'Echo Int�gration');
[IdentPartData, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% Origine du rehaussement de contraste

str1 = 'Origine du rehaussement de contraste :';
str2 = 'Contrast enhancement :';
[OrigineCLim, flag] = my_questdlg(Lang(str1,str2), ...
    Lang('Unique pour tous les fichiers', 'Unique for all files'), ...
    Lang('G�n�r�s pendant le Pr�processing des Signaux', 'Got from Preprocessing Signals'));
if ~flag
    return
end

%% Bornes de rehaussement de contraste

if OrigineCLim == 1
    switch IdentPartData
        case 1
            CLim = this.CLim;
            if isempty(persistent_CLimSubbottom)
                minVal   = CLim(1);
                maxVal   = CLim(2);
            else
                minVal   = persistent_CLimSubbottom(1);
                maxVal   = persistent_CLimSubbottom(2);
            end
            Unit = 'Amp';
        case 2
            if isempty(persistent_CLimWC)
                minVal   = -1;
                maxVal   = 1;
            else
                minVal   = persistent_CLimWC(1);
                maxVal   = persistent_CLimWC(2);
            end
            Unit = 'Amp';
        case 3
            if isempty(persistent_CLimBoth)
                minVal   = -1;
                maxVal   = 1;
            else
                minVal   = persistent_CLimBoth(1);
                maxVal   = persistent_CLimBoth(2);
            end
            Unit = 'Amp';
    end
    
    [flag, CLim] = saisie_CLim(minVal, maxVal, Unit, 'SignalName', 'ODV WC data');
    if ~flag
        return
    end
    
    switch IdentPartData
        case 1
            persistent_CLimSubbottom = CLim;
        case 2
            persistent_CLimWC     = CLim;
        case 3
            persistent_CLimBoth   = CLim;
    end
end

%% S�lection de la table de couleurs

if isempty(persistent_ColormapIndex)
    ColormapIndex = 2;
else
    ColormapIndex = persistent_ColormapIndex;
end
[flag, ColormapIndex] = edit_ColormapIndex(this, 'ColormapIndex', ColormapIndex);
if ~flag
    return
end
persistent_ColormapIndex = ColormapIndex;

%% Taille max des images

[flag, TailleMax] = inputOneParametre('Maximum size (M pixels)', 'Value', ...
    'Value', 4, 'Unit', 'mega pixels', 'MinValue', 0.5);
if ~flag
    return
end
TailleMax = TailleMax * 1e6;

%% Video

if isempty(persistent_Video)
    Video = 1;
else
    Video = persistent_Video;
end
[flag, Video] = edit_Video(this, 'Video', Video);
if ~flag
    return
end
persistent_Video = Video;
