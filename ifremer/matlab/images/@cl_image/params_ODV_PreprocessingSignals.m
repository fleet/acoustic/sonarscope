function [flag, nomFic, repImport] = params_ODV_PreprocessingSignals(this, repImport)

InitialFileFormat = this.InitialFileFormat;
if strcmp(InitialFileFormat, 'ODV')
    [Selection, flag] = my_questdlg(Lang('S�lection des fichiers', 'Files selection ...'), ...
        Lang('Ce fichier de cette image ci seulement', 'This file only'), ...
        Lang('Choix avec un "browser" de fichiers', 'With a browser'));
    if isempty(Selection) || ~flag
        return
    end
else
    Selection = 0;
end

if Selection == 1
    nomFic = this.InitialFileName;
else
    [flag, nomFic, repImport] = uiSelectFiles('ExtensionFiles', {'.odv'; '.ODV'}, 'AllFilters', 1, 'RepDefaut', repImport);
    if ~flag
        return
    end
    repImport = fileparts(nomFic{1});
end
