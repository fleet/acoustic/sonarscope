function [flag, nomFicIn, nomFicOut, Labels2Plot, strColor, identColor, repImport, repExport] = params_PlotGeographicPositionsInGoogleEarth(~, repImport, repExport)

nomFicOut   = [];
Labels2Plot = [];
strColor    = [];
identColor  = [];

%% Message � l'utilisateur

str1 = 'Exemples de fichier attendu.';
str2 = 'Expected file examples.';
str{1} = Lang(str1,str2);
str{end+1} = ' ';
str{end+1} = 'Name Latitude Longitude';
str{end+1} = 'Cc301 49.90542 -7.21258';
str{end+1} = 'Cc302 50.016 -7.37442';
str{end+1} = 'Cc303 50.08319 -8.0829';
str{end+1} = 'Cc304 50.1425 -8.45383';
str{end+1} = 'Cc305 50.18759 -7.04889';
str{end+1} = 'Cc306 50.32292 -8.70817';
str{end+1} = 'Cc307 50.37583 -7.76583';
str{end+1} = ' ';
str{end+1} = 'Or with a label which will allow you to select one or several items';
str{end+1} = ' ';
str{end+1} = 'Name Lat Lon Label';
str{end+1} = 'Cc301 49.90542 -7.21258 2';
str{end+1} = 'Cc302 50.016 -7.37442 1';
str{end+1} = 'Cc303 50.08319 -8.0829 3';
str{end+1} = 'Cc304 50.1425 -8.45383 2';
str{end+1} = 'Cc305 50.18759 -7.04889 2';
str{end+1} = 'Cc306 50.32292 -8.70817 1';
str{end+1} = 'Cc307 50.37583 -7.76583 4';
% edit_infoPixel(str, [], 'PromptString', Lang(str1,str2));
my_warndlg(cell2str(str), 1); % Apr�s modif 28/04/2014
clear str

%% S�lection des fichiers d'entr�e

[flag, nomFicIn, repImport] = uiSelectFiles('ExtensionFiles', '.txt', 'AllFilters', 1, 'RepDefaut', repImport);
if ~flag
    return
end

%% Lecture du fichier

Label = [];
for k=1:length(nomFicIn)
    T = readtable(nomFicIn{k});
    % T(1:5,:)
    
    if size(T,2) > 3
        Label  = [Label; T.(4)]; %#ok<AGROW>
    end
end
if ~isempty(Label)
    Label = unique(Label);
    for k=1:length(Label)
        str{k} = num2str(Label(k));
    end
    str1 = 'S�lectionnez les labels que vous voulez tracer';
    str2 = 'Select the labels to plot';
    [rep, flag] = my_listdlg(Lang(str1,str2), str', 'InitialValue', 1:length(Label));
    if ~flag
        return
    end
    Labels2Plot = Label(rep);
end

%% Color

strColor{1}     = 'ylw';
strColor{end+1} = 'blue';
strColor{end+1} = 'grn';
strColor{end+1} = 'ltblu';
strColor{end+1} = 'pink';
strColor{end+1} = 'purple';
strColor{end+1} = 'red';
strColor{end+1} = 'wht';
str1 = 'S�lectionnez la couleur du pushpin';
str2 = 'Select the pushpin color';
[identColor, flag] = my_listdlg(Lang(str1,str2), strColor, 'SelectionMode', 'Single');
if ~flag
    return
end

%% S�lection des fichiers de sortie

[flag, nomFicOut] = my_uiputfile('*.kml', 'KML file', repExport);
if ~flag
    return
end
repExport = fileparts(nomFicOut);
