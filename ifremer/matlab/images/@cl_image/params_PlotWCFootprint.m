function [flag, FileNameXML, FileNameNav, repImport] = params_PlotWCFootprint(~, repImport)

FileNameNav = [];

[flag, FileNameXML, repImport] = uiSelectFiles('ExtensionFiles', '.xml', 'AllFilters', 1, 'RepDefaut', repImport, 'ChaineIncluse', '_PolarEchograms');
if ~flag
    return
end

[flag, FileNameNav, repImport] = uiSelectFiles('ExtensionFiles', {'.nvi'; '.nav'}, 'AllFilters', 1, 'RepDefaut', repImport);
flag = 1;