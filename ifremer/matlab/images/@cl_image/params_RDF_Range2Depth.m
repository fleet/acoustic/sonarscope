function [flag, ProfilCelerite, InstallationParameters, repImport] = params_RDF_Range2Depth(this, repImport)

InstallationParameters = [];
ProfilCelerite         = [];

%% On v�rifie si le layer est bien de type "RxBeamAngle"

identTxAngle = cl_image.indDataType('BeamPointingAngle');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'), 'DataType', identTxAngle); %, ...
if ~flag
    return
end

%% InstallationParameters

[flag, InstallationParameters, repImport] = RDF_installationParameters('repImport', repImport);
if ~flag
    return
end
