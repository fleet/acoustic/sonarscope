function [flag, indLayers, valInterval, valMask] = params_ROI_auto_multiImages(this, indImage)
 
valInterval = [];
valMask     = [];

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'memeDataType');
if ~flag
    return
end

if isempty(indLayers)
    return
end

str1 = 'Liste des images';
str2 = 'Liste of layers';
[choix, flag] = my_listdlgMultiple(Lang(str1,str2), nomsLayers);
if ~flag
    return
end
if isempty(choix)
    flag = 0;
    return
end
indLayers = indLayers(choix);


StatValues = this(indImage).StatValues;
Unit = this(indImage).Unit;

str1 = sprintf('Nombre de bandes\n(Ex : 2 bandes pour [-65 -15] et [15 60])');
str2 = sprintf('Number of strips\n(Ex : 2 strips for [-65 -15] and [15 60])');
[flag, nbBandes] = inputOneParametre(Lang(str1,str2), 'Nb', ...
    'Value', 1, 'MinValue', 1, 'MaxValue', 10);
if ~flag
    return
end

for k=1:nbBandes
    str1 = sprintf('Limites des valeurs pour la bande %d / %d', k, nbBandes);
    str2 = sprintf('Limits of values for strp %d / %d', k, nbBandes); % Mask processing parametres'
    p    = ClParametre('Name', Lang('Valeur minimale', 'Min value'), ...
        'Unit', Unit, 'Value', StatValues.Min);
    p(2) = ClParametre('Name', Lang('Valeur maximale', 'Max value'), ...
        'Unit', Unit, 'Value', StatValues.Max);
    p(3) = ClParametre('Name', 'Mask value', ...
        'Value', 1, 'MinValue', 1, 'MaxValue', 255);
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;

    valInterval(k,:) = sort(val(1:2)); %#ok<AGROW>
    valMask(k)       = val(3); %#ok<AGROW>
end
