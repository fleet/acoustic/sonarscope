function [flag, indImages, Method] = params_Recalage(this, indImage)

Method = [];

%% Liste des images

str = {};
listeIdent = [];
for k=1:length(this)
    if k ~= indImage
        str{end+1} = this(k).Name; %#ok
        listeIdent(end+1) = k; %#ok<AGROW>
    end
end

msg = 'Select the image to be shifted vs the current image';
[flag, indImages] = uiSelectImagesSingle('ExtensionImages', str, 'TitreImages', msg);
if ~flag
    return
end
indImages = listeIdent(indImages);

%% M�thode d'interpolation

[Method, flag] = my_questdlg('Method', '2x1D', '2D', 'Init', 2);
if ~flag
    return
end
