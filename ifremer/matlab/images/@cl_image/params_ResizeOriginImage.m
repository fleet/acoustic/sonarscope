function [flag, pasx, pasy] = params_ResizeOriginImage(this, indImage, varargin)

pasx = [];
pasy = [];

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage);
if ~flag
    return
end

if isempty(indLayers)
    flag = 0;
    return
end

str1 = 'Choix de l''image d''origine';
str2 = 'Select the original Image';
[choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single');
if ~flag
    return
end
indLayerOrigin = indLayers(choix);

x = get(this(indImage), 'x');
y = get(this(indImage), 'y');

xOrigin = get(this(indLayerOrigin), 'x');
yOrigin = get(this(indLayerOrigin), 'y');

if ~isempty(subx)
    x = x(subx);
end

if ~isempty(suby)
    y = y(suby);
end

subxOrigin = find((xOrigin >= min(x)) & (xOrigin <= max(x)));
subyOrigin = find((yOrigin >= min(y)) & (yOrigin <= max(y)));

pasx = length(subxOrigin) / length(x);
pasy = length(subyOrigin) / length(y);
