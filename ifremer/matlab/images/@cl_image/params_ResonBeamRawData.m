function [flag, subPing, DisplayLevel, nomFicNav, nomFicIP, repImport] = params_ResonBeamRawData(this, nomFic, xyCross, repImport)
    
subPing      = [];
DisplayLevel = [];
nomFicNav    = [];
nomFicIP     = [];

%% Ouverture du fichier .s7k
    
s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);

%% Lecture de "depth"

[flag, DataDepth] = read_depth_s7k(s7k);
if ~flag
    return
end

%% Saisie des pings � charger

N = length(DataDepth.PingCounter);
str1 = 'Liste des pings � charger : ';
str2 = 'Ping numbers to load : ';
InitialFileName = this.InitialFileName;
flagPingXxx = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if strcmp(nomFic, InitialFileName) && flagPingXxx
    iy = round(xyCross(2));
    rep = sprintf('1 %d % d', iy, N);
else
    rep = sprintf('1  %d', N);
end
[rep, flag] = my_inputdlg(Lang(str1,str2), rep);
if ~flag
    return
end
subPing = eval(['[' rep{1} ']']);

%% Saisie du niveau de visualisation

[flag, DisplayLevel] = question_DetailLevelBDA('Default', 1);
if ~flag
    return
end

%% Fichier de navigation

str1 = 'Disposez-vous d''un fichier de navigation permettant de positionner les donn�es ?';
str2 = 'Do you have a navigation file that would allow to locate the data ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    str1 = 'SonarScope - Entrez le nom de fichier de navigation au format Caraibes (.nvi)';
    str2 = 'SonarScope - Please select the navigation file (Caraibes format .nvi)';
    [flag, nomFicNav] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', '.nvi', 'RepDefaut', repImport);
    if ~flag
        return
    end
    repImport = fileparts(nomFicNav{1});
end

%% Fichier de Installation Parameters

str1 = 'Disposez-vous d''un fichier XML de "Installation Parameters" ?';
str2 = 'Do you have an "Installation Parameters" XML file ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    str1 = 'SonarScope - Entrez le nom de fichier "InstallationParameters"';
    str2 = 'SonarScope - Please select the "InstallationParameters" file';
    [flag, nomFicIP] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', '.xml', ...
        'ChaineIncluse', 'InstallationParameters', 'RepDefaut', repImport);
    if ~flag
        return
    end
    repImport = fileparts(nomFicIP{1});
end
