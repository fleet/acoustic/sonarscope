function [flag, listeFic, Names, StepCompute, StepDisplay, nomFicNav, nomFicIP, OrigineWC, ...
    ChoixNatDecibel, CLimRaw, CLimComp, subBeams, subPings, XLim, YLim, TagName, MaskAfterDetection, ...
    repImport, repExport, typeAlgoWC, MaxImageSize, Carto, skipAlreadyProcessedFiles, typeOutputFormat] =  ...
    params_ResonS7k_Echograms(this, TypeWCOutput, listeFic, sameFile, subx, suby, repImport, repExport)

Names              = [];
StepCompute        = 1;
StepDisplay        = 1;
nomFicNav          = [];
nomFicIP           = [];
OrigineWC          = [];
ChoixNatDecibel    = [];
CLimRaw            = [];
CLimComp           = [];
subBeams           = [];
subPings           = [];
XLim               = [];
YLim               = [];
TagName            = '';
MaskAfterDetection = [];
typeAlgoWC         = 1;
MaxImageSize       = [];
Carto              = [];
typeOutputFormat   = 1;
skipAlreadyProcessedFiles = [];

S0 = cl_reson_s7k.empty();

if ischar(listeFic)
    listeFic = {listeFic};
end

%% Fichier InstallationParameters

checkInstallationParameters(S0, listeFic);

%% Carto

if isequal(listeFic{1}, this.InitialFileName)
	Carto = get_Carto(this);
else
    % TODO
end

%% S�lection des pings

x = this.x;
y = this.y;

% Test indirect si l'utilisateur a r�pondu "Current Extent" :
% TODO : il faudrait sortir cette info directement de survey_uiSelectFiles et getSubxSubyContextuel
flagCurrentExtend = ~isequal(subx, 1:length(x)) || ~isequal(suby, length(y));

if sameFile && flagCurrentExtend
    switch this.GeometryType
        case cl_image.indGeometryType('PingBeam')
            % On ne traite que les faisceaux et les ping r�cup�r�s dans le "Current extent"
            subBeams = subx;
            subPings = suby;
            
        case cl_image.indGeometryType('DepthAcrossDist')
            % On limite les images aux bornes "Depth" et "Across Distance" r�cup�r�s dans le "Current extent"
            % On d�duit que l'utilisateur a r�pondu "Current Extent".
            % Comme on est sur une image de type "DepthAcrossDist" alors on rend les limites en x et en y
            if isempty(subx)
                x = [x(1) x(end)];
            else
                x = [x(subx(1)) x(subx(end))];
            end
            if isempty(suby)
                y = [y(1) y(end)];
            else
                y = [y(suby(1)) y(suby(end))];
            end
            XLim = [min(x) max(x)];
            YLim = [min(y) max(y)];
            
            str1 = 'Liste des pings � charger : ';
            str2 = 'Ping numbers to load : ';
            rep = '1:2'; % sprintf('1 %d % d', iy, N);
            [rep, flag] = my_inputdlg(Lang(str1,str2), rep);
            if ~flag
                return
            end
            subPings = eval(['[' rep{1} ']']);
%             subPings = 102:104; % 87:120 % TODO Temporaire

        otherwise
            % Comment� par JMA le 25/02/2021
            %{
            flag = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
            if flag
                subPings = suby;
                str1 = 'Cette g�om�trie ne permet ne permet de r�cup�rer que les num�ros de pings de "Current Extent"';
                str2 = 'Only the Ping numbers from the "Current Extent" will be used for the processing.';
                my_warndlg(Lang(str1,str2), 1);
            else
                str1 = 'Cette g�om�trie ne permet pas d''utiliser les limites de l''image d�finies dans "Current Extent"';
                str2 = 'The limits of the "Current Extent" cannot be used with this geometry.';
                my_warndlg(Lang(str1,str2), 1);
            end
            %}
    end
end

%% Origine des �chogrammes

str1 = 'Source des donn�es (type de datagramme)';
str2 = 'Data incomming (type of datagrams)';
[OrigineWC, flag] = my_questdlg(Lang(str1,str2), 'WC', 'Mag&Phase');
if ~flag
    return
end

%% Masquage au del� de la d�tection

[flag, MaskAfterDetection] = question_MaskWCBeyondDetection;
if ~flag
    return
end

%% Repr�sentation des donn�es en dB / ValNat

str1 = 'Type de repr�sentation';
str2 = 'Type of representation';
[ChoixNatDecibel, flag] = my_questdlg(Lang(str1,str2), 'Amp', 'dB', 'Init', 2);
if ~flag
    return
end
if ChoixNatDecibel == 1
    Unit = 'Amp';
    CLimRaw  = [0 150];
    CLimRawExtreme  = [0 150];
    CLimComp = [0 7];
else
    Unit = 'dB';
    CLimRaw  = [20 60];
    CLimRawExtreme = [-80 100];
    CLimComp = [-5 25];
end

%% CLimRaw et CLimComp

str1 = 'IFREMER - SonarScope : Bornes du rehaussement de contraste';
str2 = 'IFREMER - SonarScope : Color limits (contrast enhancement)';
p(1) = ClParametre('Name', 'Raw Min', ...
    'Unit', Unit,  'MinValue', CLimRawExtreme(1), 'MaxValue', CLimRawExtreme(2), 'Value', CLimRaw(1));
p(2) = ClParametre('Name', 'Raw Max', ...
    'Unit', Unit,  'MinValue', CLimRawExtreme(1), 'MaxValue', CLimRawExtreme(2), 'Value', CLimRaw(2));
p(3) = ClParametre('Name', 'Comp Min', ...
    'Unit', Unit,  'MinValue', CLimComp(1), 'MaxValue', CLimComp(2), 'Value', CLimComp(1));
p(4) = ClParametre('Name', 'Comp Max', ...
    'Unit', Unit,  'MinValue', CLimComp(1), 'MaxValue', CLimComp(2), 'Value', CLimComp(2));
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
CLimRaw  = val(1:2);
CLimComp = val(3:4);

%% Pas d'�chantillonnage des pings

[flag, StepCompute] = question_ComputeEchograms;
if ~flag
    return
end

%% Tag suppl�mentaire dans le mom des images

[flag, TagName] = question_AddTagInOutputFile;
if ~flag
    return
end
TagName2 = TagName;

%% Select directory

str1 = 'Choix du r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, pathname] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = pathname;

%% Prepare images for NWW

if TypeWCOutput == 1 % Images individuelles
    clear Names
    for k=1:length(listeFic)
        [~, nomFic] = fileparts(listeFic{k});
        switch OrigineWC
            case 1
                Name.nomFicNetcdf  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WC_PolarEchograms.g3d.nc']);
                Name.nomFicXmlRaw  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WC_Raw_PolarEchograms.xml']);
                Name.nomFicXmlComp = fullfile(pathname, [nomFic '_dB_' TagName2 '_WC_Comp_PolarEchograms.xml']);
                Name.nomFicEchoes  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WC_Echoes.xml']);
                Name.nomFicKML     = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WC_Envelopes.kml']);
                Name.Unit          = Unit;
                Name.Type          = 'WC';
            case 2
                Name.nomFicNetcdf  = fullfile(pathname, [nomFic TagName2 '_MP_PolarEchograms.g3d.nc']);
                Name.nomFicXmlRaw  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Raw_PolarEchograms.xml']);
                Name.nomFicXmlComp = fullfile(pathname, [nomFic '_dB_' TagName2 '_MP_Comp_PolarEchograms.xml']);
                Name.nomFicEchoes  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Echoes.xml']);
                Name.nomFicKML     = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Envelopes.kml']);
                Name.Unit          = Unit;
                Name.Type          = 'MP';
        end
        Name.AreaName = [];
        Names(k) = Name; %#ok<AGROW>
    end
else % Matrices 3D
    clear Names
    for k=1:length(listeFic)
        [~, nomFic] = fileparts(listeFic{k});
        switch OrigineWC
            case 1
                Name.nomFicNetcdf  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WCCube_PolarEchograms.g3d.nc']);
                Name.nomFicXmlRaw  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WCCube_Raw_PolarEchograms.xml']);
                Name.nomFicXmlComp = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WCCube_Comp_PolarEchograms.xml']);
                Name.nomFicEchoes  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WCCube_Echoes.xml']);
                Name.nomFicKML     = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_WCCube_Envelopes.kml']);
                Name.Unit          = Unit;
                Name.Type          = 'WCCube';
            case 2
                Name.nomFicNetcdf  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Cube_PolarEchograms.g3d.nc']);
                Name.nomFicXmlRaw  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Cube_Raw_PolarEchograms.xml']);
                Name.nomFicXmlComp = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Cube_Comp_PolarEchograms.xml']);
                Name.nomFicEchoes  = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Cube_Echoes.xml']);
                Name.nomFicKML     = fullfile(pathname, [nomFic '_' Unit '_' TagName2 '_MP_Cube_Envelopes.kml']);
                Name.Unit          = Unit;
                Name.Type          = 'MPCube';
        end
        
        Name.AreaName = [];
        Names(k) = Name; %#ok<AGROW>
    end
end

%% Pas de visualisation des pings

[flag, StepDisplay] = WCD.Params.question_VisuEchograms;
if ~flag
    return
end

%% Fichier de navigation et InstallationParameters

if OrigineWC == 2 % Mag&Phase
    Init = 1;
else % WC
    Init = 2;
end
[flag, nomFicNav, nomFicIP, repImport] = S7K_NavigationFileAndInstallationParameters(repImport, 'Init', Init);
if ~flag
    return
end

%% Type d'algorithme

[flag, typeAlgoWC] = question_WCRaytracingAlgorithm('InitialValue', OrigineWC);
if ~flag
    return
end

%% MaxImageSize

[flag, MaxImageSize] = question_WCMaxImageSize;
if ~flag
    return
end

%% skipAlreadyProcessedFiles

N = length(listeFic);
[flag, skipAlreadyProcessedFiles] = question_SkipAlreadyProcessedFiles(N);
if ~flag
    return
end

%% Message //Tbx

if (StepDisplay == 0) && (N > 1)
    flag = messageParallelTbx(1);
    if ~flag
        return
    end
end

%% Type de fichier en sortie

str = {'XML-Bin'; 'Netcdf'};
[typeOutputFormat, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
