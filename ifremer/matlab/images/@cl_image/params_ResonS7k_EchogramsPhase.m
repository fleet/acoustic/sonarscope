function [flag, listFileNames, Names, repImport, repExport, StepCompute, StepDisplay, nomFicNav, nomFicIP, ...
    CLim, subBeams, subPings, XLim, YLim, TagName, MaskAfterDetection, MaxImageSize, ...
    Carto, skipAlreadyProcessedFiles] = ...
    params_ResonS7k_EchogramsPhase(this, TypeWCOutput, listFileNames, sameFile, subx, suby, repImport, repExport)

%{
    TODO : essayer de regrouper
function [flag, listFileNames, Names, StepCompute, StepDisplay, nomFicNav, nomFicIP, OrigineWC, ...
    ChoixNatDecibel, CLimRaw, CLimComp, subBeams, subPings, XLim, YLim, TagName, MaskAfterDetection, ...
    repImport, repExport, typeAlgoWC, MaxImageSize, Carto, skipAlreadyProcessedFiles] =  ...
    params_ResonS7k_Echograms(this, TypeWCOutput, listFileNames, sameFile, subx, suby, repImport, repExport)
%}

Names       = [];
StepCompute = 1;
StepDisplay = 1;
nomFicNav   = [];
nomFicIP    = [];
CLim        = [];
subBeams    = [];
subPings    = [];
XLim        = [];
YLim        = [];
TagName     = '';
Carto       = [];
% typeAlgoWC  =  2;
MaskAfterDetection        = [];
MaxImageSize              = [];
skipAlreadyProcessedFiles = [];

S0 = cl_reson_s7k.empty();

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

%% Fichier InstallationParameters

checkInstallationParameters(S0, listFileNames);

%% Carto

if isequal(listFileNames{1}, this.InitialFileName)
	Carto = get_Carto(this);
else
    % TODO
end


x = this.x;
y = this.y;

% Test indirect si l'utilisateur a r�pondu "Current Extent" :
% TODO : il faudrait sortir cette info directement de survey_uiSelectFiles et getSubxSubyContextuel
flagCurrentExtend = ~isequal(subx, 1:length(x)) || ~isequal(suby, length(y));

if sameFile && flagCurrentExtend
    switch this.GeometryType
        case cl_image.indGeometryType('PingBeam')
            % On ne traite que les faisceaux et les ping r�cup�r�s dans le "Current extent"
            subBeams = subx;
            subPings = suby;
        case cl_image.indGeometryType('DepthAcrossDist')
            % On limite les images aux bornes "Depth" et "Across Distance" r�cup�r�s dans le "Current extent"
            % On d�duit que l'utilisateur a r�pondu "Current Extent".
            % Comme on est sur une image de type "DepthAcrossDist" alors on rend les limites en x et en y
            if isempty(subx)
                x = [x(1) x(end)];
            else
                x = [x(subx(1)) x(subx(end))];
            end
            if isempty(suby)
                y = [y(1) y(end)];
            else
                y = [y(suby(1)) y(suby(end))];
            end
            XLim = [min(x) max(x)];
            YLim = [min(y) max(y)];
            
            str1 = 'Liste des pings � charger : ';
            str2 = 'Ping numbers to load : ';
            rep = '1:2'; % sprintf('1 %d % d', iy, N);
            [rep, flag] = my_inputdlg(Lang(str1,str2), rep);
            if ~flag
                return
            end
            subPings = eval(['[' rep{1} ']']);
%             subPings = 102:104; % 87:120 % TODO Temporaire

        otherwise
            flag = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
            if flag
                subPings = suby;
                str1 = 'Cette g�om�trie ne permet ne permet de r�cup�rer que les num�ros de pings de "Current Extent"';
                str2 = 'Only the Ping numbers from the "Current Extent" will be used for the processing.';
                my_warndlg(Lang(str1,str2), 1);
            else
                str1 = 'Cette g�om�trie ne permet pas d''utiliser les limites de l''image d�finies dans "Current Extent"';
                str2 = 'The limits of the "Current Extent" cannot be used with this geometry.';
                my_warndlg(Lang(str1,str2), 1);
            end
    end
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, repExport] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end

%% Masquage au del� de la d�tection

[flag, MaskAfterDetection] = question_MaskWCBeyondDetection;
if ~flag
    return
end

%% Tag suppl�mentaire dans le mom des images

[flag, TagName] = question_AddTagInOutputFile;
if ~flag
    return
end
if isempty(TagName)
    TagName2 = '';
else
    TagName2 = ['_' TagName];
end

%% Prepare images for NWW

if TypeWCOutput == 1 % Images individuelles
    clear Names
    for k=1:length(listFileNames)
        [~, nomFic] = fileparts(listFileNames{k});
        Name.nomFicXmlPhase  = fullfile(repExport, [nomFic TagName2 '_Phase_PolarEchograms.xml']);
        
        Name.AreaName = [];
        Names(k) = Name; %#ok<AGROW>
    end
else % Matrices 3D
    clear Names
    for k=1:length(listFileNames)
        [~, nomFic] = fileparts(listFileNames{k});
        Name.nomFicXmlPhase = fullfile(repExport, [nomFic TagName2 '_Cube_Phase_PolarEchograms.xml']);
        
        Name.AreaName = [];
        Names(k) = Name; %#ok<AGROW>
    end
end

%% CLim

CLim = [0 180];
p    = ClParametre('Name', 'CLim Phase Max', 'Unit', 'deg', 'Value', CLim(2));
p(2) = ClParametre('Name', 'CLim Phase Min', 'Unit', 'deg', 'Value', CLim(1));
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
CLim = a.getParamsValue;
CLim = sort(CLim);

%% Pas de visualisation des pings

[flag, StepDisplay] = WCD.Params.question_VisuEchograms;
if ~flag
    return
end

%% Fichier de navigation et InstallationParameters

[flag, nomFicNav, nomFicIP, repImport] = S7K_NavigationFileAndInstallationParameters(repImport, 'Init', 1);
if ~flag
    return
end

%% Type d'algorithme

%{
[flag, typeAlgoWC] = question_WCRaytracingAlgorithm('InitialValue', 2);
if ~flag
    return
end
%}

%% MaxImageSize

[flag, MaxImageSize] = question_WCMaxImageSize;
if ~flag
    return
end

%% Skip existing files

N = length(listFileNames);
[flag, skipAlreadyProcessedFiles] = question_SkipAlreadyProcessedFiles(N);
if ~flag
    return
end

%% Message //Tbx
        
if (StepDisplay == 0) && (N > 1)
    flag = messageParallelTbx(1);
    if ~flag
        return
    end
end
