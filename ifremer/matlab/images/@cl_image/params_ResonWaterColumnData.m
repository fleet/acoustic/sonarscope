function [flag, subPing, nomFicNav, nomFicIP, repImport] = params_ResonWaterColumnData(this, nomFic, xyCross, repImport)
    
flag      = 0;
subPing   = [];
nomFicNav = [];
nomFicIP  = [];

%% Ouverture du fichier .s7k

s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
identSondeur = selectionSondeur(s7k);
if isempty(identSondeur)
    return
end

%% Lecture de "depth"

[flag, DataDepth] = read_depth_s7k(s7k, 'identSondeur', identSondeur);
if ~flag
    return
end

%% Saisie des pings � charger

N = length(DataDepth.PingCounter);
str1 = 'Liste des pings � charger : ';
str2 = 'Ping numbers to load : ';
InitialFileName = this.InitialFileName;
flagPingXxx = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if strcmp(nomFic, InitialFileName) && flagPingXxx
    iy = round(xyCross(2));
    rep = sprintf('1 %d % d', iy, N);
else
    rep = sprintf('1  %d', N);
end

[rep, flag] = my_inputdlg(Lang(str1,str2), rep);
if ~flag
    return
end
subPing = eval(['[' rep{1} ']']);

%% Fichier de navigation et InstallationParameters

[flag, nomFicNav, nomFicIP, repImport] = S7K_NavigationFileAndInstallationParameters(repImport);
if ~flag
    return
end

