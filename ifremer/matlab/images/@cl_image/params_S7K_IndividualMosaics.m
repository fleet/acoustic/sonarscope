function [flag, gridSize, CorFile, ModeDependant, selectionData, Window, ...
    skipExistingMosaic, flagTideCorrection, SameReflectivityStatus, InfoCompensationCurve, repImport, repExport] ...
    = params_S7K_IndividualMosaics(this, repImport, repExport)

persistent persistentGridSize

gridSize               = [];
CorFile                = [];
ModeDependant          = [];
selectionData          = [];
Window                 = [3 3];
skipExistingMosaic     = [];
SameReflectivityStatus = 0;
flagTideCorrection     = 0;
InfoCompensationCurve  = [];

%% Type de grille

str1 = 'Type de pas de la grille';
str2 = 'Type of grid size';
[TypeGrid, flag] = my_questdlg(Lang(str1,str2), Lang('Unique', 'Unique'), Lang('Estim� sur chaque image', 'Estimated on each image'));
if ~flag
    return
end

%% Pas de la grille

if TypeGrid == 1
    if isempty(persistentGridSize)
        gridSize = 0;
    else
        gridSize = persistentGridSize;
    end
    
    str1 = 'Donnez un pas de grille valable pour tous les profils';
    str2 = 'Give a consistent grid size for the set of individual lines.';
    [flag, gridSize] = inputOneParametre(Lang(str1,str2), 'Value', ...
        'Value', gridSize, 'Unit', 'm', 'MinValue', 0);
    if ~flag
        return
    end
    
    if gridSize == 0
        str1 = 'La valeur ne peut pas �tre �gale � z�ro.';
        str2 = 'The value cannot be 0';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
end

%% Nom du r�pertoire des mosaiques individuelles

[flag, nomDir] = question_NomDirIndividualMosaics(repExport);
if ~flag
    return
end
repExport = nomDir;

%% Choix du layer

listeData{1} = 'Reflectivity';
listeData{2} = 'Bathymetry';
listeData{3} = 'IncidenceAngle';
str1 = 'Choix du layer :';
str2 = 'Select the layer :';
[selectionData, flag] = my_listdlg(Lang(str1,str2), listeData, 'SelectionMode', 'Single');
if ~flag
    return
end

%% Si Mosaique de Depth, on propose de faire la correction de mar�e

flagTideCorrection = (selectionData == 2); % && isSonarSignal(this, 'Tide');
if flagTideCorrection
    [flag, flagTideCorrection] = question_TideCorrection;
    if ~flag
        return
    end
end

%% If Reflectivity, ask if the reflectivity has to be set as the current one (same status for BS, DiagTx, etc ...)

if testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', 'PingXxxx', 'noMessage')
    [flag, SameReflectivityStatus] = question_SameReflectivityStatus;
    if ~flag
        return
    end
end

%% Compensation statistique

[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport);
if ~flag
    return
end
InfoCompensationCurve.FileName                 = CorFile;
InfoCompensationCurve.ModeDependant            = ModeDependant;
InfoCompensationCurve.UseModel                 = UseModel;
InfoCompensationCurve.PreserveMeanValueIfModel = PreserveMeanValueIfModel;

persistentGridSize = gridSize;

%% skipExistingMosaic

str1 = 'Ne pas refaire les mosaiques existantes ?';
str2 = 'Skip existing mosaics ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
skipExistingMosaic = (rep == 1);
