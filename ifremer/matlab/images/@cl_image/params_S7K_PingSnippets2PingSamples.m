function [flag, indAngle] = params_S7K_PingSnippets2PingSamples(this, indImage)

indAngle = [];
ident = cl_image.indDataType('Reflectivity');
flag = testSignature(this(indImage), 'DataType', ident, 'GeometryType', cl_image.indGeometryType('PingSnippets'));
if ~flag
    return
end

ident = cl_image.indDataType('TxAngle');
indAngle = findIndLayerSonar(this, indImage, 'DataType', ident, 'OnlyOneLayer');
if isempty(indAngle)
    my_warndlg('Sonar/Re-interpolation du speculaire : Operation possible uniquement si il existe une image : synchronisee  de meme resolution et de type DataType = 7; c.a.d AveragedPtsNb', 1);
    flag = 0;
    return
end
