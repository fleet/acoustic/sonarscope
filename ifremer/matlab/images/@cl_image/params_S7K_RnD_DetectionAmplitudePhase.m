function [flag, indLayerAmp, indLayerPhase, DisplayLevel, BeamsToDisplay, IdentAlgo, GateDepth] ...
    = params_S7K_RnD_DetectionAmplitudePhase(this, indImage)

DisplayLevel   = [];
BeamsToDisplay = [];
indLayerAmp    = [];
indLayerPhase  = [];
IdentAlgo      = [];
GateDepth      = [];

flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

identLayerPhase   = cl_image.indDataType('TxAngle');
identReflectivity = cl_image.indDataType('Reflectivity');

TD = this(indImage).DataType;
switch TD
    case identLayerPhase
        indLayerPhase = indImage;
        [flag, indLayerAmp, nomsLayers] = listeLayersSynchronises(this, indLayerPhase, 'TypeDonneUniquement', identReflectivity);
        if ~flag
            return
        end
        switch length(indLayerAmp)
            case 0
                str1 = 'Il faut un layer d''amplitude';
                str2 = 'An Amplitude layer is required';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                return
            case 1
            otherwise
                str1 = 'Confirmation image conditionnelle';
                str2 = 'Conditional image agreement';
                [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single', 'InitialValue', length(nomsLayers));
                if ~flag
                    return
                end
                indLayerAmp = indLayerAmp(choix);
        end
        
    case identReflectivity
        indLayerAmp = indImage;
        [flag, indLayerPhase, nomsLayers] = listeLayersSynchronises(this, indLayerAmp, 'TypeDonneUniquement', identLayerPhase);
        if ~flag
            return
        end
        switch length(indLayerPhase)
            case 0
                str1 = 'Il faut un layer de phase';
                str2 = 'A Phase layer is required';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                return
            case 1
            otherwise
                str1 = 'Confirmation image conditionnelle';
                str2 = 'Conditional image agreement';
                [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single', 'InitialValue', length(nomsLayers));
                if ~flag
                    return
                end
                indLayerPhase = indLayerPhase(choix);
        end
        
    otherwise
        str1 = 'L''image d''entr�e doit �tre une image d''amplitude ou une image de Phase';
        str2 = 'Input image must be Amplitude or phase.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
end

%% Saisie des param�tres de gate en depth

[flag, GateDepth] = parameters_gateDepthBDA;
if ~flag
    return
end

%% Saisie de mode de d�tection (a revalider quand un travail sera fait sur les �paves)

[flag, IdentAlgo] = parameters_BDAgetAlgo;
if ~flag
    return
end

%% TestPlatformCmatlab

flag = parameters_BDAgetPlatform;
if ~flag
    return
end

%% Saisie du niveau de visualisation

[flag, DisplayLevel] = question_DetailLevelBDA('Default', 2, 'Passport', 0);
if ~flag
    return
end

if DisplayLevel == 3
    str1 = 'Liste des faisceaux � visualiser : ';
    str2 = 'Beam numbers to plot in detail : ';
    nbColumns = this(indImage).nbColumns;
    rep = sprintf('1:%d', nbColumns);
    [rep, flag] = my_inputdlg(Lang(str1,str2), rep);
    if ~flag
        return
    end
    BeamsToDisplay = eval(['[' rep{1} ']']);
end

