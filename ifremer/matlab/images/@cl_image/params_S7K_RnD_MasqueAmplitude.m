function [flag, indLayerPhase, GateDepth, IdentAlgo, DisplayLevel] = params_S7K_RnD_MasqueAmplitude(this, indImage)
 
DisplayLevel  = [];
GateDepth     = [];
indLayerPhase = [];
IdentAlgo     = [];

identReflectivity   = cl_image.indDataType('Reflectivity');

flag = testSignature(this(indImage), 'DataType', identReflectivity, ...
    'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

identLayerPhase = cl_image.indDataType('TxAngle');
[flag, indLayerPhase, nomsLayers] = listeLayersSynchronises(this, indImage, 'TypeDonneUniquement', identLayerPhase);
if ~flag
    return
end
switch length(indLayerPhase)
    case 0
        indLayerPhase = [];
        return
    case 1
    otherwise
        str1 = 'Confirmation image conditionnelle';
        str2 = 'Conditional image agreement';
        [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerPhase  = indLayerPhase(choix);
end

%% Saisie des param�tres de gate en depth

[flag, GateDepth] = parameters_gateDepthBDA;
if ~flag
    return
end

%% Saisie de mode de d�tection (a revalider quand un travail sera fait sur les �paves)

[flag, IdentAlgo] = parameters_BDAgetAlgo;
if ~flag
    return
end

%% Param�tres optionnels

%% Saisie du niveau de visualisation

[flag, DisplayLevel] = question_DetailLevelBDA('Default', 1);
if ~flag
    return
end
