function [flag, CleanCurves, TideFile, flagVsLayers, flagDisplay, repImport] = params_S7K_StatsBathyMinusDTM(this, repImport)

CleanCurves  = [];
TideFile     = [];
flagVsLayers = [];
flagDisplay  = [];

%% Checking if the current image is a DTM in LatLong geometry

identBathymetry  = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

%% Fichier de mar�e

[flag, TideFile] = inputTideFile(1, repImport);
if ~flag
    return
end

%% Type de courbes � calculer

str{1} = 'Dif MNT (m) vs Angles';
str{2} = 'Dif MNT (m) vs Angles & DetectionType';
str{3} = 'Dif MNT (m) vs Angles & Frequencies';
str{4} = 'Dif MNT (%) vs Angles';
str{5} = 'Dif MNT (%) vs Angles & DetectionType';
str{6} = 'Dif MNT (%) vs Angles & Frequencies';
[flagVsLayers, flag] = my_listdlg('List of conditional curves', str, 'SelectionMode', 'Multiple');
if ~flag
    return
end
clear str

%% Type de visualisations

str{1} = 'Individual curves';
str{2} = 'Resulting cleaned curve';
str{3} = 'Resulting cleaned curve & std';
str{4} = 'Resulting std';
[flagDisplay, flag] = my_listdlg('Type of plots', str, 'SelectionMode', 'Multiple');
if ~flag
    return
end
clear str

%% CleanCurves

str1 = 'Voulez-vous nettoyer les courbes ?';
str2 = 'Do you want to clean up the curves ?';
[ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
CleanCurves = (ind == 1);
