function [flag, indentMos, listeFic, identLayer, NomFicOut, SourceHeading, repImport, repExport] = params_SDFRectification(this, indImage, repImport, repExport)

indentMos     = [];
listeFic      = [];
identLayer    = [];
NomFicOut     = [];
SourceHeading = [];

%% Check signature of the current image

flag = testSignature(this(indImage), 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Selection des mosaiques � afficher

nomImage = this(indImage).Name;
[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage');
if ~flag
    return
end

indImage = find(strcmp(nomImage, nomsLayers));
[flag, choix] = uiSelectImages('ExtensionImages', nomsLayers, 'InitialValue', indImage);
if ~flag
    str1 = 'Aucune image synchronis�e � l''image courante.';
    str2 = 'None image is synchrinized to the current one.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
indentMos = indLayers(choix);
if isempty(indentMos)
    indentMos = indImage;
end

%% Liste des fichiers

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.sdf', 'RepDefaut', repImport);
if ~flag
    return
end

%% Select the frequency

str1 = 'Choisissez la fr�quence ';
str2 = 'Select the frequency';
[identLayer, flag] = my_listdlg(Lang(str1,str2), {'200kHz'; '400kHz'}, 'SelectionMode', 'Single');
if ~flag
    return
end

%% Origine du cap

[flag, SourceHeading] = SDF.Params.selectSourceHeading;
if ~flag
    return
end

%% Nom du fichier de sortie

str1 = 'IFREMER - SonarScope - Nom du fichier de rectification de la navigation';
str2 = 'IFREMER - SonarScope - Name of the output rectification file';
[flag, NomFicOut] = my_uiputfile('*.txt', Lang(str1,str2), fullfile(repExport, 'SDF_Rectification.txt'));
if ~flag
    return
end
repExport = fileparts(NomFicOut);

if exist(NomFicOut, 'file')
    str1 = 'Le fichier existe d�j�, que voulez-vous faire ?';
    str2 = 'The file already exists, what do you want to do ?';
    str3 = 'Le compl�ter';
    str4 = 'Append';
    str5 = 'Le recommencer';
    str6 = 'Redo';
    [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
    if ~flag
        return
    end
    if rep == 2
        delete(NomFicOut)
    end
end
