function [flag, nomFic, sub, repExport] = params_STATS_CurveExport(this, Ext, repExport)

nomFic = [];

[flag, sub, nbCourbesStatistiques, NomsCourbes] = selectionCourbesStats(this, 'SelectionMode', 'Single'); %#ok<ASGLU>
if ~flag || isempty(sub)
    flag = 0;
    return
end

str = strrep(NomsCourbes{sub}, ':', '_');
str = strrep(str, '/', '_');
str = strrep(str, '\', '_');
str = strrep(str, ' ', '_');
str = strrep(str, '__', '_');
str = strrep(str, '__', '_');
str = strrep(str, '_._', '_');
str = strrep(str, '_-_', '_');
str = strrep(str, '__', '_');

C = get_CourbesStatistiques(this, 'sub', sub);
bilan = C.bilan{1}(1);

str = [str '_' bilan.DataTypeValue];

% SOS panique : c'est la mouise, il est temps de changer tout �a.
if iscell(bilan.DataTypeConditions) && (length(bilan.DataTypeConditions) == 1) && iscell(bilan.DataTypeConditions{1})
    bilan.DataTypeConditions = bilan.DataTypeConditions{1};
end


for k=1:length(bilan.DataTypeConditions)
    str = [str '_' bilan(1).DataTypeConditions{k}]; %#ok<AGROW>
end

nomFic = fullfile(repExport, [str Ext]);
[flag, nomFic] = my_uiputfile('*.mat', 'Give a file name', nomFic);
if ~flag
    return
end
repExport = fileparts(nomFic);
