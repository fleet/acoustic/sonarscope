function [flag, nomFic, sub, repImport] = params_STATS_CurveImportCsv(this, indImage, repImport)

nomFic = [];

% The image must have an existing curve used to host the new one.
% A new curve will be created using the same properties but the values will
% be the ones read in the ASCII file.
str1 = 'TODO';
str2 = 'The image must have an existing curve used to host the new one. A new curve will be created using the same properties but the values will be the ones read in the ASCII file.';
my_warndlg(Lang(str1,str2), 1);

%% Select the curve that will be used to host the one.

[flag, sub, nbCourbesStatistiques, NomsCourbes] = selectionCourbesStats(this(indImage), 'SelectionMode', 'Single'); %#ok<ASGLU>
if ~flag || isempty(sub)
    flag = 0;
    return
end

%% Select the ASCII file

[flag, nomFic] = my_uigetfile('*.csv', 'Give a .csv file then select the curve you want to push the .csv data into.', repImport);
if ~flag
    return
end
repImport = fileparts(nomFic);
