function [flag, subCurves, listeprofils, listeprofils1, indLayerMask, valMask, w, ...
    StopRegul, UseModelBS] = params_SegmentationCourbes(this, indImage)

listeprofils  = [];
listeprofils1 = [];
indLayerMask  = [];
valMask       = [];
w             = [];
StopRegul     = [];
UseModelBS    = [];

%% S�lection des courbes angulaires

n = get_nbCourbesStatistiques(this(indImage));
[flag, subCurves] = selectionCourbesStats(this(indImage), 'InitialValue', n);
if ~flag || isempty(subCurves)
    flag = 0;
    return
end

%% Type de segmentation

%{
str1 = 'Type de segmentation.';
str2 = 'Type of segmentation.';
str{1} = Lang('Classique', 'Classic');
str{2} = Lang('Facies interm�diaires', 'Intermediate facies');
[typeSegmentation, flag] = my_listdlg(Lang(str1,str2), str);
if ~flag
    return
end
%}

%% Courbes statistiques

nbCurves = length(subCurves);
k3 = 0;
for k1=1:nbCurves
    C = get_CourbesStatistiques(this(indImage), 'sub', subCurves(k1));
    for k2=1:length(C(1).bilan{1})
        k3 = k3 + 1;
        bilan{k3} = C(1).bilan{1}(k2); %#ok<AGROW>
    end
end

%% Courbes ou mod�les ?

str1 = 'Que voulez-vous utiliser pour d�crire le BS ?';
str2 = 'What to you want to use for the BS description ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2, 'Models', 'Experimental curves');
if ~flag
    return
end
UseModelBS = (rep == 1);

%% Plot des courbes

% {
x = [];
for k1=1:length(bilan)
    x = [x bilan{k1}(1).x]; %#ok<AGROW>
end
x = [ -90 unique(x) 90];
nx = length(x);
nc = length(bilan);
m = NaN(nc, nx);
s = NaN(nc, nx);
for k1=1:nc
    x1 = bilan{k1}(1).x;
    
    if UseModelBS && isfield(bilan{k1}(1), 'model') && ~isempty( bilan{k1}(1).model)
        y1 = bilan{k1}(1).model.compute('x', x1);
    else
        y1 = bilan{k1}(1).y(:);
    end
    
%     y1 = bilan{k1}(1).y;
    y2 = bilan{k1}(1).std;
    subNonNaN = ~isnan(bilan{k1}(1).y);
    x1 = x1(subNonNaN);
    y1 = y1(subNonNaN);
    y2 = y2(subNonNaN);
    x1 = [-90 x1 90]; %#ok<AGROW>
    y1 = [y1(1); y1(:); y1(end)];
    y2 = [y2(1); y2(:); y2(end)];
    m(k1,:) = my_interp1(x1, y1, x, 'linear', 'extrap');
    s(k1,:) = my_interp1(x1, y2, x, 'linear', 'extrap');
end
FigUtils.createSScFigure;
map = jet(nc);
for k1=1:nc
    h = PlotUtils.createSScPlot(x, m(k1,:)); grid on
    set(h, 'LineWidth', 3, 'Color', map(k1,:)); hold on;
    xlabel(bilan{1}(1).DataTypeConditions{1})
    h = PlotUtils.createSScPlot(x, m(k1,:)-s(k1,:), '--');
    set(h, 'Color', map(k1,:));
    h = PlotUtils.createSScPlot(x, m(k1,:)+s(k1,:), '--'); grid on
    set(h, 'Color', map(k1,:));
end
hold off;
% }

%% Contr�le si on est bien sur le m�me type de donn�es

DTImage = get_DataTypeName(this(indImage));
flag = strcmp(DTImage, bilan{1}(1).DataTypeValue);
if ~flag
    str1 = sprintf('ImageType attendu : %s\nImageType de l''image courante : %s', bilan{1}(1).DataTypeValue, DTImage);
    str2 = sprintf('ImageType expected : %s\nImageType of current image : %s', bilan{1}(1).DataTypeValue, DTImage);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Recherche des premiers layers conditionnels

if iscell(bilan{1}(1).DataTypeConditions)
    DataTypeConditions = bilan{1}(1).DataTypeConditions{1};
else
    DataTypeConditions = bilan{1}(1).DataTypeConditions; % Ca arrive comme cela quand on reimporte des courbes !!!
end
   
% Si on veur faire la segmentation en mode multi-images
% [flag, listeprofils, listeprofils1] = listePairesLayersSynchronises(this, indImage, DataTypeConditions);
% if ~flag
%     return
% end
% if isempty(listeprofils)
%     flag = 0;
%     return
% end

% Segmentation uniquement sur l'image courante

identLayerAngle = cl_image.indDataType(DataTypeConditions);
listeprofils1 = findIndLayerSonar(this, indImage, 'DataType', identLayerAngle, 'OnlyOneLayer');
listeprofils = indImage;

% if isempty(listeprofils1)
%     str1 = 'Aucune image n''a pu �tre apair�e.';
%     str2 = 'None image could be put in couples.';
%     my_warndlg(Lang(str1,str2), 1);
%     flag = 0;
%     return
% end

% if length(listeprofils) ~= length(listeprofils1)
%     str1 = 'Certaines images n''ont pas pu �tre apair�es.';
%     str2 = 'Some images could not be put in couples.';
%     my_warndlg(Lang(str1,str2), 1);
% end

%% Saisie du voisinage

str1 = 'Fen�tre d''observation de la segmentation.';
str2 = 'Segmentation work window.';
[flag, w] = saisie_window([3 3], 'Titre', Lang(str1,str2), 'maxvalue', [50 50]);
if ~flag
    return
end

%% Crit�re d'arr�t de la r�gulatisation

[flag, StopRegul] = saisie_MarkovRegularization;
if ~flag
    return
end

%% Recherche d'un masque �ventuel

[flag, indLayerMask, valMask] = paramsMasque(this, indImage, 'InitialValue', 'Segmentation');
if ~flag
    return
end
