function [flag, nomFicCurves, SameReflectivityStatus, InfoCompensationCurve, gridSize, nomDirOut, ...
    w, StopRegul, UseModelBS, repImport, repExport] = params_SegmentationSamantha(this, repImport, repExport)

nomFicCurves           = [];
SameReflectivityStatus = [];
nomDirOut              = [];
InfoCompensationCurve  = [];
gridSize               = [];
StopRegul              = [];
w                      = [];
UseModelBS             = [];

%% Test si l'image visualis�e est bien un MNT de r�f�rence

DataType = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Nom du fichier des courbes de r�flectivit� angulaire

[flag, nomFicCurves, repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport, ...
    'Entete', 'Select the files that contain the backscatter curves.');
if ~flag
    return
end

%% Courbes ou mod�les ?

str1 = 'Que voulez-vous utiliser pour d�crire le BS ?';
str2 = 'What to you want to use for the BS description ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2, 'Models', 'Experimental curves');
if ~flag
    return
end
UseModelBS = (rep == 1);

%% Ca serait bien d'afficher les courbes ici

flag = import_CourbesStats(this, nomFicCurves, 'flagPlot', 1);
if ~flag
    return
end

%% If Reflectivity, ask if the reflectivity has to be set as the current one (same status for BS, DiagTx, etc ...)

[flag, SameReflectivityStatus] = question_SameReflectivityStatus;
if ~flag
    return
end

%% Saisie du voisinage

str1 = 'Fen�tre d''observation de la segmentation.';
str2 = 'Segmentation work window.';
[flag, w] = saisie_window([3 3], 'Titre', Lang(str1,str2), 'maxvalue', [50 50]);
if ~flag
    return
end

%% Crit�re d'arr�t de la r�gulatisation

[flag, StopRegul] = saisie_MarkovRegularization;
if ~flag
    return
end

%% Compensation statistique

[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport);
if ~flag
    return
end
InfoCompensationCurve.FileName      = CorFile;
InfoCompensationCurve.ModeDependant = ModeDependant;
InfoCompensationCurve.UseModel      = UseModel;
InfoCompensationCurve.PreserveMeanValueIfModel = PreserveMeanValueIfModel;

%% gridSize

[flag, gridSize] = get_gridSize;
if ~flag
    return
end

%% Nom du r�pertoire de sauvegarde des fichiers MatlabnomDirMatFiles

str1 = 'Nom du r�pertoire o� seront sauv�s les images segment�es.';
str2 = 'Name of the directory to save the segmentatios images.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;
