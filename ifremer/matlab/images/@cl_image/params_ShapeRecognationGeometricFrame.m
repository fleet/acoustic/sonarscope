function [flag, typeAlgo, FrameDescription] = params_ShapeRecognationGeometricFrame(this, indImage, varargin)

persistent BaseDiameter TopDiameter Sigma Height Width SignalAmplitude

typeAlgo         = [];
FrameDescription = [];

%% Select a shape or an image snippet

str{1} = Lang('Un chapeau haut de forme',                 'A top hat');
str{2} = Lang('Une gaussienne',                           'A gaussian');
str{3} = Lang('Un c�ne',                                  'A cone');
str{4} = Lang('Un �cho de fluide dans la colonne d''eau', 'A Water Column fluid echo');
str1 = 'Type de forme � rechercher';
str2 = 'Type of shape to hunt';
[typeShape, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end

FrameDescription.TypeShape         = typeShape;
FrameDescription.Info.strTypeShape = str2;

switch this(indImage).GeometryType
    case cl_image.indGeometryType('LatLong')
        Unit = 'm';
        gridSize = this(indImage).YStepMetric;
    case {cl_image.indGeometryType('GeoYX'); cl_image.indGeometryType('OrthoYX')}
        Unit = 'm';
        gridSize = this(indImage).YStepMetric;
    case cl_image.indGeometryType('DepthAcrossDist')
        Unit = 'm';
        gridSize = this(indImage).YStep;
    otherwise
        Unit = 'pixel';
        gridSize = 1;
end
FrameDescription.Info.Unit     = Unit;
FrameDescription.Info.gridSize = gridSize;

%% Select the algorithm

clear str
str{1} = Lang('EQM', 'RMS');
str{2} = Lang('Intercorr�lation', 'Cross-correlation');
str1 = 'Choix de l''algorithme';
str2 = 'Select the algorithm';
[typeAlgo, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end

%% Select the parameters of the shape

switch FrameDescription.TypeShape
    case 1 % A top hat : diam�tre base, diam�tre haut, hauteur
        if isempty(BaseDiameter) || isempty(TopDiameter)|| isempty(SignalAmplitude)
            TopDiameter     =  5 * gridSize;
            BaseDiameter    = 10 * gridSize;
            SignalAmplitude =  1;
        end
        messageHeight(typeAlgo)
        % TODO : donner des valeur plausibles pour MinValue et MaxValue en fonction de yStepMetric
        p    = ClParametre('Name', 'Top diameter',  'Unit', this(indImage).XUnit, ...
            'MinValue', gridSize, 'MaxValue', gridSize*100, 'Value', TopDiameter);
        p(2) = ClParametre('Name', 'Base diameter', 'Unit', this(indImage).XUnit, ...
            'MinValue', gridSize, 'MaxValue', gridSize*200, 'Value', BaseDiameter);
        p(3) = ClParametre('Name', 'Amplitude',        'Unit', this(indImage).Unit, ...
            'MinValue', -5000, 'MaxValue', 5000, 'Value', SignalAmplitude);
        a = StyledParametreDialog('params', p, 'Title', 'Characteristics of the top hat');
        a.sizes = [0 -2 0 -2 0 -1 -1 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        val = a.getParamsValue;
        FrameDescription.TopHat.TopDiameter     = val(1);
        FrameDescription.TopHat.BaseDiameter    = val(2);
        FrameDescription.TopHat.SignalAmplitude = val(3);
        TopDiameter     = FrameDescription.TopHat.TopDiameter;
        BaseDiameter    = FrameDescription.TopHat.BaseDiameter;
        SignalAmplitude = FrameDescription.TopHat.SignalAmplitude;
        
    case 2 % A gaussian : Ecart-type, hauteur
        if isempty(Sigma) || isempty(Height)
            Sigma  = 10 * gridSize;
            Height =  0;
        end
        messageHeight(typeAlgo)
        p    = ClParametre('Name', 'Width (2*std)', 'Unit', Unit, ...
            'MinValue', gridSize*0.5, 'MaxValue', gridSize*200, 'Value', Sigma);
        p(2) = ClParametre('Name', 'Amplitude', 'Unit', this(indImage).Unit, ...
            'MinValue', -5000, 'MaxValue', 5000, 'Value', SignalAmplitude);
        a = StyledParametreDialog('params', p, 'Title', 'Characteristics of the gaussian shape');
        a.sizes = [0 -2 0 -2 0 -1 -1 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            Sigma =  Sigma / gridSize;
            return
        end
        val = a.getParamsValue;
        FrameDescription.Gaussian.Stdx2           = val(1);
        FrameDescription.Gaussian.SignalAmplitude = val(2);
        Sigma           = FrameDescription.Gaussian.Stdx2;
        SignalAmplitude = FrameDescription.Gaussian.SignalAmplitude;
        
    case 3 % A cone : diam�tre base, hauteur
        if isempty(BaseDiameter) || isempty(Height)
            BaseDiameter    = 10 * gridSize;
            SignalAmplitude =  0;
        end
        messageHeight(typeAlgo)
        p    = ClParametre('Name', 'Base diameter', 'Unit', Unit, ...
            'MinValue', gridSize, 'MaxValue', gridSize*200, 'Value', BaseDiameter);
        p(2) = ClParametre('Name', 'Amplitude', Unit', this(indImage).Unit, ...
            'MinValue', -5000, 'MaxValue', 5000, 'Value', Height);
        a = StyledParametreDialog('params', p, 'Title', 'Characteristics of the cone');
        a.sizes = [0 -2 0 -2 0 -1 -1 0];
        a.openDialog;
        if ~flag
            BaseDiameter =  BaseDiameter / gridSize;
            return
        end
        val = a.getParamsValue;
        FrameDescription.Cone.BaseDiameter    = val(1);
        FrameDescription.Cone.SignalAmplitude = val(2);
        BaseDiameter    = FrameDescription.Cone.BaseDiameter;
        SignalAmplitude = FrameDescription.Cone.SignalAmplitude;
        
    case 4 % A Water Column fluid echo : Width, Ecart-type, hauteur
        if isempty(Width) || isempty(Sigma) || isempty(Height)
            Width           = 5 * gridSize;
            Sigma           = 4 * gridSize;
            Height          = Width;
            SignalAmplitude = 1;
        end
        messageHeight(typeAlgo)
        p    = ClParametre('Name', 'Width of the gaz plume (2*std)', 'Unit', Unit, ...
            'MinValue', gridSize*5, 'MaxValue', gridSize*200, 'Value', Sigma);
        
        p(2) = ClParametre('Name', 'Width of the detection window', 'Unit', this(indImage).XUnit, ...
            'MinValue', gridSize*5, 'MaxValue', gridSize*200, 'Value', Width);
        
        p(3) = ClParametre('Name', 'Height of the detection window', 'Unit', this(indImage).YUnit, ...
            'MinValue', -5000, 'MaxValue', 5000, 'Value', Height);
        
        p(4) = ClParametre('Name', 'Amplitude', 'Unit', this(indImage).Unit, ...
            'MinValue', -5000, 'MaxValue', 5000, 'Value', SignalAmplitude);
        
        a = StyledParametreDialog('params', p, 'Title', 'Characteristics of the gaussian shape');
        a.sizes = [0 -2 0 -2 0 -1 -1 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        val = a.getParamsValue;
        FrameDescription.GasPlume.Stdx2           = val(1);
        FrameDescription.GasPlume.Width           = val(2);
        FrameDescription.GasPlume.Height          = val(3);
        FrameDescription.GasPlume.SignalAmplitude = val(4);
        Width           = FrameDescription.GasPlume.Width;
        Sigma           = FrameDescription.GasPlume.Stdx2;
        Height          = FrameDescription.GasPlume.Height;
        SignalAmplitude = FrameDescription.GasPlume.SignalAmplitude;
end


function messageHeight(typeAlgo)
if typeAlgo == 2
    str11 = 'Une hauteur va vous �tre demand�e, seule son signe a une importance pour l''algo Cross-Correlation.';
    str22 = 'A height value for the shape will be required, only its sign will be interpreted for the crosscorrelation algorithm.';
    my_warndlg(Lang(str11,str22), 1);
end
