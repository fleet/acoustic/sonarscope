function [flag, Side] = params_Sidescan_ExtractWaterColumn(this)

identReflectivity = cl_image.indDataType('Reflectivity');

Side = [];
flag = testSignature(this, 'DataType', identReflectivity, ...
    'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange')]);
if ~flag
    return
end

str1 = 'De quel c�t� voulez-vous extraire la donn�e de Colonne d''eau ?';
str2 = 'What side do you want to extract Water Column';
[Side, flag] = my_questdlg(Lang(str1,str2), Lang('B�bord', 'Port'), Lang('Tribord', 'Starboard'));
if ~flag
    return
end
