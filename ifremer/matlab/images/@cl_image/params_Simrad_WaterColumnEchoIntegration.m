function [flag, ListeFic, params_ZoneEI, Tag, Display, repImport] = params_Simrad_WaterColumnEchoIntegration(this, repImport)

ListeFic      = [];
params_ZoneEI = [];
Tag           = '';
Display       = 0;

%% Contr�les

InitialFileFormat = this.InitialFileFormat;
if ~strcmp(InitialFileFormat, 'SimradAll')
    str1 = 'Cette op�ration n''est faisable que sur des donn�es provenant d''un sondeur Kongsberg pour le moment.';
    str2 = 'This process can be done only on images coming from Kongsberg for the moment.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

nomFicAll = this.InitialFileName;
flag = exist(nomFicAll, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', this.InitialFileFormat);
    str2 = sprintf('"%s" does not exist.', this.InitialFileFormat);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Liste des fichiers d'�chogrammes

[~, nomFic] = fileparts(nomFicAll);
str1 = sprintf('IFREMER - SonarScope : S�lectionnez le fichier echogramme correspondant � %s.', nomFic);
str2 = sprintf('IFREMER - SonarScope : Select the echogram corresponding to %s.', nomFic);
[flag, ListeFic, lastDir] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', {'.xml'}, ...
    'RepDefaut', repImport, 'ChaineIncluse', '_Raw_PolarEchograms');
if ~flag
    return
end
repImport = lastDir;
ListeFic  = ListeFic{1};

%% Param�tres de l'�cho-int�gration

[flag, params_ZoneEI, Display] = WCD.Params.EchoIntegration;
if ~flag
    return
end

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end
