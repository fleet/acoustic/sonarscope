function [flag, listeFic, pathname, Step, typeOutputFormat, repImport, repExport] = params_SlicesAlongNavigation(~, repImport, repExport)

persistent LastStep

Step       = [];
pathname   = [];
typeOutputFormat = [];

str1 = 'SonarScope browser : s�lectionnez les fichier .xml "WCCube"';
str2 = 'SonarScope browser : Select the "WCCube" .xml files';
[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
    'RepDefaut', repImport, 'ChaineIncluse', '_WCCube', 'Entete', Lang(str1,str2), 'AllFilters', 1);
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers de coupes verticales';
str2 = 'Select or create the directory in which the vertical cutaway files will be created.';
[flag, pathname] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = pathname;

%% Espacement entre slices

str1 = 'Type d''espacement entre les slices';
str2 = 'Definition of the slice widths';
[ind, flag] = my_questdlg(Lang(str1,str2), 'Manual', 'Auto');
if ~flag
    return
end

if ind == 1
    if isempty(LastStep)
        Step = 25;
    else
        Step = LastStep;
    end
    str1 = 'Espacement des coupes.';
    str2 = 'Define width of across distance cutaways';
    [flag, Step] = inputOneParametre(Lang(str1,str2), Lang('Valeur','Value'), ...
        'Value', Step, 'Unit', 'm', 'MinValue', 0, 'MaxValue', 500);
    if ~flag
        return
    end
    LastStep = Step;
else
    Step = [];
end

%% Type de fichier en sortie

str = {'XML-Bin'; 'Netcdf'};
[typeOutputFormat, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
