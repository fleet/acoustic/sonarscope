function [flag, nomFicSave, sub, repExport] = params_SonarBsExport(this, repExport)

nomFicSave = [];
 
[flag, sub, ~, NomsCourbes] = selectionCourbesStats(this, 'Tag', 'BS');
if ~flag || isempty(sub)
    flag = 0; % Ajout JMA le 01/04/2021
    return
end

initNomCourbe = sprintf('%s', NomsCourbes{sub});
initNomCourbe = strrep(initNomCourbe, 'Whole image on', '');
initNomCourbe = strrep(initNomCourbe, '\', '_');
initNomCourbe = strrep(initNomCourbe, ':', '_');
initNomCourbe = strrep(initNomCourbe, ' ', '');
initNomCourbe = strrep(initNomCourbe, '-', '_');
% nomFic = fullfile(repExport, 'BSLurtonModel.mat');
nomFic = fullfile(repExport, [initNomCourbe '.mat']);
[flag, nomFicSave] = my_uiputfile('*.mat', 'Give a file name', nomFic);
if ~flag
    return
end
repExport = fileparts(nomFicSave);
