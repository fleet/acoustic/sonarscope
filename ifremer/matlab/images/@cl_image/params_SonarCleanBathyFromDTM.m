function [flag, flagTideCorrection] = params_SonarCleanBathyFromDTM(this)
 
flagTideCorrection = 0;

%% Checking if the current image is a DTM in LatLong geometry

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

%% Tide file ?

str1 = 'Voulez-vous prendre en compte la mar�e ?';
str2 = 'Do you want to take the tide into account ?';
[choix, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
flagTideCorrection = (choix == 1);
 