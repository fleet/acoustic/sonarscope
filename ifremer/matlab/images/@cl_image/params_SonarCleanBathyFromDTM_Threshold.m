function [flag, Threshold, MaskIfNoDTM, flagTideCorrection] = params_SonarCleanBathyFromDTM_Threshold(this) 

persistent persistent_Threshold

Threshold          = [];
MaskIfNoDTM        = [];
flagTideCorrection = 0;

% if get_LevelQuestion == 1
%     SonarScopeHelp('TutorialSurvey_Processing_All_DataCleaning_ByComparisonToADtm_UsingAThreshold.pdf');
% end

%% Checking if the current image is a DTM in LatLong geometry

identBathymetry  = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

%% Threshold

if isempty(persistent_Threshold)
    Threshold = Inf;
else
    Threshold = persistent_Threshold;
end

str1 = 'Seuil';
str2 = 'Threshold';
[flag, Threshold] = inputOneParametre('Sonar Clen Bathy From DTM', Lang(str1,str2), ...
    'Value', Threshold, 'Unit', 'm', 'MinValue', 0);
if ~flag
    return
end
persistent_Threshold = Threshold;

%% Masquage en dehord du MNT

str1 = 'Masquage des sondes en dehors du MNT ?';
str2 = 'Mask soundings outside DTM ?';
[MaskIfNoDTM, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end

%% Tide file ?

str1 = 'Voulez-vous prendre en compte la mar�e ?';
str2 = 'Do you want to take the tide into account ?';
[choix, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
flagTideCorrection = (choix == 1);
