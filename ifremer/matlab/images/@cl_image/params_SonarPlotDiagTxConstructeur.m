function [flag, Sonar, Time] = params_SonarPlotDiagTxConstructeur(this)

flag = 0;
Time = [];

Sonar = get(this, 'SonarDescription');
if isempty(Sonar)
    str1 = 'Pas de description de sonar dans cette image.';
    str2 = 'No sonar description in this image image.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if isfield(this.Sonar, 'Time')
    Time = this.Sonar.Time.timeMat;
    Time = Time(1);
end

flag = 1;
