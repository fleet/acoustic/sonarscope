function [flag, indLayerRange, indLayerAbsorptionCoef] = params_SonarTVG(this, indImage, AbsorptionCoeff_Type, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

indLayerRange = [];
indLayerAbsorptionCoef = [];

identReflectivity = cl_image.indDataType('Reflectivity');

flag = testSignature(this(indImage), 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% Recherche d'un layer Range

if this(indImage).GeometryType == cl_image.indGeometryType('PingSamples')
    indLayerRange = [];
else
    [flag, indLayerRange] = findAnyLayerRange(this, indImage, 'AtLeastOneLayer');
    if ~flag
        return
    end
end

%% Recherche d'un layer �ventuel "AbsorptionCoeff"

switch AbsorptionCoeff_Type
    case 'AbsorptionCoeff_RT'
        Tag = '_RT';
        TagFuture = [];
    case 'AbsorptionCoeff_SSc'
        Tag = '_SSc';
        TagFuture = '_RT';
    case 'AbsorptionCoeff_User'
        Tag = '_User';
        TagFuture = '_SSc';
    otherwise
        str1 = sprintf('"%s" n''est pas reconnu dans params_SonarTVG', AbsorptionCoeff_Type);
        str2 = sprintf('"%s" not reckognized in params_SonarTVG', AbsorptionCoeff_Type);
        my_warndlg(Lang(str1,str2), 1);
end

% if isfield(this(indImage).Sonar, AbsorptionCoeff_Type) && ~isempty(this(indImage).Sonar.(AbsorptionCoeff_Type))
%     % Un signal this(indImage).Sonar.(AbsorptionCoeff_Type) a �t� trouv�
% else
% Pas de signal "AbsorptionCoeff_Type" trouv� alors on cherche si un layer "AbsorptionCoeff" est disponible

str1 = sprintf('Plusieurs layers de type "%s" ont �t� trouv�s, choisissez-en un.', AbsorptionCoeff_Type);
str2 = sprintf('Several layers of type "%s" were found, please select a single one.', AbsorptionCoeff_Type);
TitleWindowGUI = Lang(str1,str2);
identLayerAbsorptionCoeff = cl_image.indDataType('AbsorptionCoeff');
[indLayerAbsorptionCoef, nomLayer] = findIndLayerSonar(this, indImage, 'DataType', identLayerAbsorptionCoeff, ...
    'Tag', Tag, 'OnlyOneLayer', 'TitleWindowGUI', TitleWindowGUI); %#ok<ASGLU>
if isempty(indLayerAbsorptionCoef) && ~Mute
    % D�comment� par JMA le 12/07/2023
    % {
    if isempty(TagFuture)
        str1 = sprintf('Le layer d''absorption de type "%s" n''a pas �t� trouv�.', Tag);
        str2 = sprintf('The AbsorptionCoeff of type "%s" was not found.', Tag);
    else
        str1 = sprintf('Le layer d''absorption de type "%s" n''a pas �t� trouv�. On va rechercher "%s"', Tag, TagFuture);
        str2 = sprintf('The AbsorptionCoeff of type "%s" was not found. Will look for "%s".', Tag, TagFuture);
    end
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'AbsorptionCoefficientNotFound', 'TimeDelay', 60);
    % }
    flag = 0;
end
% end
