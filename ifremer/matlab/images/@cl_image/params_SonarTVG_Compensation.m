function [flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser, AlphaUser] = params_SonarTVG_Compensation(this, indImage, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

indLayerRange               = [];
indLayerAbsorptionCoeffRT   = [];
indLayerAbsorptionCoeffSSc  = [];
indLayerAbsorptionCoeffUser = [];
AlphaUser                   = [];

%% Test si l'image est d�j� compens�e en mode User. Dans ce cas on sort de suite car il n'y a rien de mieux � faire

if (this(indImage).Sonar.TVG.etat == 1) && (this(indImage).Sonar.TVG.origine == 3)
    messageAboutCalibration('Tag', 'TVG')
    flag = 1;
    return
end

%% On regarde si il existe un layer AbsorptionCoeffUser

[flag, indLayerRange, indLayerAbsorptionCoeffUser] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_User', 'Mute', Mute);
if ~flag || ~isempty(indLayerAbsorptionCoeffUser)
    %     return
end

%% On regarde si il existe un layer AbsorptionCoeffSSc

[flag, ~, indLayerAbsorptionCoeffSSc] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_SSc', 'Mute', 1);
if ~flag || ~isempty(indLayerAbsorptionCoeffSSc)
    %     return
end

%% On regarde si il existe un layer AbsorptionCoeffRT

[flag, ~, indLayerAbsorptionCoeffRT] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_RT', 'Mute', 1);
if ~flag || ~isempty(indLayerAbsorptionCoeffRT)
    %     return
end
