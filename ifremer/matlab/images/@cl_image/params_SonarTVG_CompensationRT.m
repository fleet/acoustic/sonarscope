function [flag, indLayerRange, AbsorptionCoeffRT, AbsorptionCoeffSSc, AbsorptionCoeffUser] = params_SonarTVG_CompensationRT(this, indImage)

flag = 0;
indLayerRange       = [];
AbsorptionCoeffRT   = [];
AbsorptionCoeffSSc  = [];
AbsorptionCoeffUser = [];

if (this(indImage).Sonar.TVG.etat == 1) && (this(indImage).Sonar.TVG.origine == 1) % TVG compens�e User
    if ~flag
        messageAboutCalibration('Tag', 'TVG')
    end
    return
end

if this(indImage).Sonar.TVG.etat == 1 % Compens�
    switch this(indImage).Sonar.TVG.origine
        case 1 % RT
            % On ne passe pas par ici
        case 2 % SSc
            [flag, indLayerRange, AbsorptionCoeffSSc] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_SSc');
        case 3 % User
            [flag, indLayerRange, AbsorptionCoeffUser] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_User');
    end
    if ~flag
        return
    end
end
[flag, indLayerRange, AbsorptionCoeffRT] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_RT');
