function [flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser] = params_SonarTVG_NoCompensation(this, indImage)

flag = 0;
indLayerRange               = [];
indLayerAbsorptionCoeffRT   = [];
indLayerAbsorptionCoeffSSc  = [];
indLayerAbsorptionCoeffUser = [];

if this(indImage).Sonar.TVG.etat == 2
    if ~flag
        messageAboutCalibration('Tag', 'TVG')
    end
    return
end

switch this(indImage).Sonar.TVG.origine
    case 1 % RT
        [flag, indLayerRange, indLayerAbsorptionCoeffRT] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_RT');
    case 2 % SSc
        [flag, indLayerRange, indLayerAbsorptionCoeffSSc] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_SSc');
    case 3 % User
        [flag, indLayerRange, indLayerAbsorptionCoeffUser] = params_SonarTVG(this, indImage, 'AbsorptionCoeff_User');
end
