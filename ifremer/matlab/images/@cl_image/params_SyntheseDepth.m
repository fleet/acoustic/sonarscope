function [px, py, x, y, Parametres, flag] = params_SyntheseDepth(~)

Titre = 'Gaussian image syntesis general parametres';
p    = ClParametre('Name', Lang('Rang parametre en X', 'Rank of parameter in X'), 'Value', 1, 'MinValue', 1, 'Format', '%d');
p(2) = ClParametre('Name', Lang('Rang parametre en Y', 'Rank of parameter in Y'), 'Value', 2, 'MinValue', 1, 'Format', '%d');
p(3) = ClParametre('Name', Lang('Xmin', 'Xmin'), 'Value', 0);
p(4) = ClParametre('Name', Lang('Xmax', 'Xmax'), 'Value', 1);
p(5) = ClParametre('Name', Lang('Ymin', 'Ymin'), 'Value', 0);
p(6) = ClParametre('Name', Lang('Ymax', 'Ymax'), 'Value', 1);
p(7) = ClParametre('Name', Lang('Nb. Gaussiennes', 'Number of Gaussians'), 'Value', 1, 'MinValue', 1, 'MaxValue', 10);
a = StyledSimpleParametreDialog('params', p, 'Title', Titre);
a.openDialog;
flag = a.okPressedOut;
if ~flag
    px = [];
    py = [];
    x = [];
    y = [];
    Parametres = [];
    return
end
val = a.getParamsValue;

% Récupération des valeurs
px		= sprintf('p%d', val(1));
py		= sprintf('p%d', val(2));
Xmin	= min(val(3:4));
Xmax	= max(val(3:4));
Ymin	= min(val(5:6));
Ymax	= max(val(5:6));
nbGauss = val(7);

% Calcul distributions 1D
x = Xmin:(Xmax-Xmin)/50:Xmax;
y = Ymin:(Ymax-Ymin)/50:Ymax;

Xcenter = (Xmin + Xmax) / 2;
Ycenter = (Ymin + Ymax) / 2;
Sigma1  = (Xmax - Xmin) / 10;
Sigma2  = (Ymax - Ymin) / 10;

for k=1:nbGauss
    Titre = sprintf('Gaussian image syntesis general parametres\nGauss curve Nu %d', k);
    p    = ClParametre('Name', 'Xcenter', 'Value', Xcenter);
    p(2) = ClParametre('Name', 'Ycenter', 'Value', Ycenter);
    p(3) = ClParametre('Name', 'Sigma1',  'Value', Sigma1);
    p(4) = ClParametre('Name', 'Sigma2',  'Value', Sigma2);
    p(5) = ClParametre('Name', 'Angle',   'Value', 0);
    p(6) = ClParametre('Name', 'Weight',  'Value', 1, 'MinValue', 0);
    a = StyledSimpleParametreDialog('params', p, 'Title',Titre);
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        Parametres = [];
        return
    end
    val = a.getParamsValue;

    % Récupération des valeurs
    Parametres(k,:) = val; %#ok
end
