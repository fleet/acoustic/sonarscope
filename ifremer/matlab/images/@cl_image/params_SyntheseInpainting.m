function [flag, Method] = params_SyntheseInpainting(~)

str1 = 'Attention : Cette m�thode comble artificiellement les trous de votre image, cette op�ration est faite par des m�thodes d''interpolation. N''OUBLIEZ pas de pr�ciser cela lors du rendu d''une image ainsi interpol�e ou extrapol�e.';
str2 = 'Warning : this method artificially fills holes within the current image. This is done by interpolating methods. DON''T FORGET to mention it when displaying such an image.';
my_warndlg(Lang(str1,str2), 1);

str1 = 'TODO';
str2 = 'Inpainting Method : Author John D''Errico (woodchips@rochester.rr.com)';
str = {'Default'; 'Method 1'; 'Method 2'; 'Method 3'; 'Method 4'; 'Method 5'};
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
Method = rep - 1;

%      method == 0 --> (DEFAULT) see method 1, but
%         this method does not build as large of a
%         linear system in the case of only a few
%         NaNs in a large array.
%         Extrapolation behavior is linear.
%
%       method == 1 --> simple approach, applies del^2
%         over the entire array, then drops those parts
%         of the array which do not have any contact with
%         NaNs. Uses a least squares approach, but it
%         does not modify known values.
%         In the case of small arrays, this method is
%         quite fast as it does very little extra work.
%         Extrapolation behavior is linear.
%
%       method == 2 --> uses del^2, but solving a direct
%         linear system of equations for nan elements.
%         This method will be the fastest possible for
%         large systems since it uses the sparsest
%         possible system of equations. Not a least
%         squares approach, so it may be least robust
%         to noise on the boundaries of any holes.
%         This method will also be least able to
%         interpolate accurately for smooth surfaces.
%         Extrapolation behavior is linear.
%
%       method == 3 --+ See method 0, but uses del^4 for
%         the interpolating operator. This may result
%         in more accurate interpolations, at some cost
%         in speed.
%
%       method == 4 --+ Uses a spring metaphor. Assumes
%         springs (with a nominal length of zero)
%         connect each node with every neighbor
%         (horizontally, vertically and diagonally)
%         Since each node tries to be like its neighbors,
%         extrapolation is as a constant function where
%         this is consistent with the neighboring nodes.
%
%       method == 5 --+ See method 2, but use an average
%         of the 8 nearest neighbors to any element.
%         This method is NOT recommended for use.