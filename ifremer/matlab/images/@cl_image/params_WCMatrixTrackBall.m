function [flag, listeFic, repImport] =  params_WCMatrixTrackBall(this, repImport)

listeFic = [];

% identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('DepthAcrossDist'));
if ~flag
    return
end

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.xml', ...
    'RepDefaut', repImport, 'ChaineIncluse', '_WCCubeRaw');
if ~flag
    return
end
