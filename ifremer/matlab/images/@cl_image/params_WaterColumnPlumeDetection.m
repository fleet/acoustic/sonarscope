function [flag, listFileNames, nomDirOut, typeAlgo, FrameDescription, Tag, StepDisplay, repImport, repExport] = ...
    params_WaterColumnPlumeDetection(this, indImage, repImport, repExport, varargin)

[varargin, ChaineIncluse] = getPropertyValue(varargin, 'ChaineIncluse', '_Comp_PolarEchograms'); %#ok<ASGLU>

StepDisplay      = 1;
listFileNames    = [];
nomDirOut        = [];
typeAlgo         =  [];
FrameDescription = [];
Tag              = [];

%% Test signature

flag = testSignature(this(indImage), 'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', cl_image.indGeometryType('DepthAcrossDist'));
if ~flag
    return
end

%% Liste des fichiers d'�chogrammes

str1 = 'IFREMER - SonarScope : S�lectionnez les fichiers des �chogrammes';
str2 = 'IFREMER - SonarScope : Select the files of the echograms';
[flag, listFileNames, lastDir] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', {'.xml'}, ...
    'RepDefaut', repImport, 'ChaineIncluse', ChaineIncluse);
if ~flag
    return
end
repImport = lastDir;

%% Param�tre de crossCorrelationMoving

[flag, typeAlgo, FrameDescription] = params_ShapeRecognationGeometricFrame(this, indImage);
if ~flag
    return
end

%% Saisie du Tag

str1 = 'Tag � inclure dans les noms de fichiers.';
str2 = 'Tag to include in file names.';
nomVar = {Lang(str1,str2)};
value = {'-PlumeDetection'};
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
Tag = value{1};

%% Nom du r�pertoire de sauvegarde des nouveaux �chogrammes

str1 = 'Nom du r�pertoire o� seront cr��s les nouveaux �chogrammes';
str2 = 'Name of the destination folder for the new echograms';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Visualisation interm�diaire de l'�chogramme

str1 = 'Visualisation de la reconnaissance de forme';
str2 = 'Shape recognition display (setting 0 will give access to parallel computing)';
[flag, StepDisplay] = WCD.Params.question_VisuEchograms('Title', Lang(str1,str2), 'StepDisplay', 1);
if ~flag
    return
end

%% Message //Tbx

N = length(listFileNames);
if (StepDisplay == 0) && (N > 1)
    flag = messageParallelTbx(1);
    if ~flag
        return
    end
end
