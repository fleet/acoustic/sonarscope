function [flag, indBF, indHF, ImageName] = params_composeColoredImageFrom2Frequencies(this, indImage, varargin)

flag      = 0;
indHF     = [];
ImageName = [];

%% S�lection du layer BF

indBF = findIndLayerSonar(this, indImage, 'DataType', this(indImage).DataType, 'WithCurrentImage', 1, ...
    'OnlyOneLayer', 'AtLeastOneLayer', 'TitleWindowGUI', 'Select the low frequency layer');
if isempty(indBF)
    return
end

%% S�lection du layer HF

indHF = findIndLayerSonar(this, indImage, 'DataType', this(indImage).DataType, 'WithCurrentImage', 1, ...
    'OnlyOneLayer', 'AtLeastOneLayer', 'TitleWindowGUI', 'Select the high frequency layer');
if isempty(indHF)
    return
end

%% Nom de l'image

nomVar = {'Name of the composit image'};
value = {'ImageComposite'};
[ImageName, flag] = my_inputdlg(nomVar, value);
ImageName = ImageName{1};
