function [flag, nomFic, Name, repExport] = params_exportSoundingsToSSc3DV(this, repExport)

Name = [];

ImageName = extract_ImageName(this);
filtre = fullfile(repExport, [ImageName '_soundings.xml']);
[flag, nomFic] = my_uiputfile('*.xml', Lang('Nom du fichier', 'Give a file name'), filtre);
if ~flag
    return
end
repExport = fileparts(nomFic);
Name = extract_ImageName(this); %'Soundings';

str1 = 'Nom de la couche de donn�e dans GLOBE';
str2 = 'TODO';
[Name, flag] = my_inputdlg(Lang(str1,str2), Name);
if ~flag
    return
end
Name = Name{1};
