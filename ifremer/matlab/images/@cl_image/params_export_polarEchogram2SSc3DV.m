function [flag, nomFicXml, repExport] = params_export_polarEchogram2SSc3DV(this, repExport)

%% R�pertoire de sortie

if length(this) == 1
    [flag, nomFicXml, repExport] = getFileNameLinked2ImageName(this, '.xml', repExport);
    if flag
        return
    end
else
    nomFicXml = [];
    
    str1 = 'Nom du r�pertoire o� seront cr��s les fichiers. Utilisez le bouton "Create a new directory" si n�cessaire.';
    str2 = 'Name of the directory to save this individual files. Use the "Create a new directory" button if necessary.';
    [flag, nomRepOut] = my_uigetdir(repExport, Lang(str1,str2));
    if ~flag
        return
    end
    repExport = nomRepOut;
end
