function [flag, InstallationParameters] = params_geoswathComputeBathy(this)

InstallationParameters = [];

DT = cl_image.indDataType('BeamPointingAngle');
flag = testSignature(this, 'DataType', DT, 'GeometryType', cl_image.indGeometryType('PingSamples'));
if ~flag
    return
end

if exist(this.InitialFileName, 'file')
    nomDirRDF = fileparts(this.InitialFileName);
else
    % TODO
end
[flag, InstallationParameters] = RDF_installationParameters('repImport', nomDirRDF);
if ~flag
    return
end
