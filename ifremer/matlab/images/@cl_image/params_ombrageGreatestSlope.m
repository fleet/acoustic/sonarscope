function [flag, VertExag] = params_ombrageGreatestSlope(this)

VertExag = 1;

flag = testSignature(this, 'ImageType', 1);
if ~flag
    return
end

str1 = 'Paramètres de la fonction d''ombrage';
str2 = 'Sun illumination parametres';
p = ClParametre('Name', Lang('Coefficient d''ombrage', 'Shading coefficient'), ...
    'MinValue', 0.1, 'MaxValue', 2, 'Value', VertExag);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 0 -2 0 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
VertExag = a.getParamsValue;
