function [flag, typeAlgo, indLayerShape] = params_shapeRecognationImageSnippet(this, indImage, varargin)

typeAlgo = [];

%% Select the parameters of the shape

str1 = 'Extrait de l''image';
str2 = 'Image containing the shape model';
[flag, indLayerShape] = paramsFctsAritmImage(this, indImage, Lang(str1,str2));
if ~flag
    return
end

%% Select the algorithm

clear str
str{1} = Lang('EQM', 'RMS');
str{2} = Lang('Intercorrélation', 'Cross-correlation');
str1 = 'Choix de l''algorithme';
str2 = 'Select the algorithm';
[typeAlgo, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
