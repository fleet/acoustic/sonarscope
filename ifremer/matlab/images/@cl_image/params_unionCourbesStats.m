function [flag, sub, TypeWeight, TypeCurve, MeanCompType] = params_unionCourbesStats(this)

TypeWeight   = 1;
TypeCurve    = 1;
MeanCompType = 1;

[flag, sub] = selectionCourbesStats(this);
if ~flag || isempty(sub)
    flag = 0;
    return
end

DT = this.CourbesStatistiques(sub(1)).bilan{1}.DataTypeValue;
if strcmp(DT, 'Reflectivity')
    [flag, MeanCompType] = question_MeanCompType('InitialValue', ReflectivityMeanCompTypeDefaultValue);
    if ~flag
        return
    end
end

str1 = 'Voulez-vous pond�rer le moyennage par le nombre de points ?';
str2 = 'Do you want to weight the mean computation, by the number of points ?';
[TypeWeight, flag] = my_questdlg(Lang(str1,str2));
