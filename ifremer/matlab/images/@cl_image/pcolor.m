function flag = pcolor(A, h)

flag = testSignature(A, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

[XPColor, ZPColor, zmaxPColor] = getXYMatrixForPcolor(A);
I = double(get_Image(A)); 
pcolor(h, XPColor, ZPColor, I);
axis ij; axis equal; colormap(jet(256)); shading flat; colorbar;
set(h, 'YLim', [0, zmaxPColor]);


function [X, Z, zmax] = getXYMatrixForPcolor(A)

SampleBeamData = get_SonarSampleBeamData(A);
Teta = SampleBeamData.RxAnglesEarth_WC;
R1 = SampleBeamData.R1SamplesFiltre;

coef = SampleBeamData.SoundVelocity / (2 * SampleBeamData.SampleRate);
R1 = R1 * coef;

z = R1 .* cosd(Teta);
zmax = max(z);

nl = A.nbRows;
z = (1:nl)' * coef;
X = z * sind(Teta);
Z = z * cosd(Teta);
