% Affichage des capteurs associ�s � une image (sonar)
%
% Syntax
%   plot(a)
% 
% Input Arguments
%   a : instance de cl_image
%
% Examples 
%
% See also cl_image cl_image/std Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot(this)

% Tri des images en fonction de TagSynchroX
subTag = process_synchro(this);

for iImage=1:length(subTag)
    a = this(subTag{iImage});

    FigUtils.createSScFigure;

    nbImages = length(a);
    nbAxes   = nbImages + ...
        ~isempty(a(1).Sonar.Time)             + ...
        ~isempty(a(1).Sonar.PingNumber)       + ...
        ~isempty(a(1).Sonar.Immersion)        + ...
        ~isempty(a(1).Sonar.Height)           + ...
        ~isempty(a(1).Sonar.CableOut)         + ...
        ~isempty(a(1).Sonar.Speed)            + ...
        ~isempty(a(1).Sonar.Heading)          + ...
        ~isempty(a(1).Sonar.Roll)             + ...
        ~isempty(a(1).Sonar.Pitch)            + ...
        ~isempty(a(1).Sonar.Yaw)              + ...
        ~isempty(a(1).Sonar.SurfaceSoundSpeed) + ...
        ~isempty(a(1).Sonar.FishLatitude)     + ...
        ~isempty(a(1).Sonar.FishLongitude)    + ...
        ~isempty(a(1).Sonar.ShipLatitude)     + ...
        ~isempty(a(1).Sonar.ShipLongitude)    + ...
        ~isempty(a(1).Sonar.Heave)            + ...
        ~isempty(a(1).Sonar.Tide)             + ...
        ~isempty(a(1).Sonar.PingCounter)      + ...
        ~isempty(a(1).Sonar.SampleFrequency)  + ...
        ~isempty(a(1).Sonar.SonarFrequency);

    for i=1:nbImages
        h(i) = subplot(1, nbAxes, i);%#ok
        imagesc(a(i), 'Axe', h(i))
        axis xy

        if (a(i).GeometryType == cl_image.indGeometryType('PingSamples')) && ~isempty(a(1).Sonar.Height)
            hold on;
            plot(-a(1).Sonar.Height,  a(1).y, 'g')
            plot( a(1).Sonar.Height,  a(1).y, 'r')
        end
    end
    k = nbImages;

    if ~isempty(a(1).Sonar.Height)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Height, a(1).y)
        grid on; axis tight;
        title('Height', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Time)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Time, a(1).y);
        grid on; axis tight;
        title('Time', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.PingNumber)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.PingNumber, a(1).y);
        grid on; axis tight;
        title('PingNumber', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Immersion)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Immersion, a(1).y)
        grid on; axis tight;
        title('Immersion', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.CableOut)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.CableOut, a(1).y)
        grid on; axis tight;
        title('CableOut', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Speed)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Speed, a(1).y)
        grid on; axis tight;
        title('Speed', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Heading)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Heading, a(1).y)
        grid on; axis tight;
        title('Heading', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Roll)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Roll, a(1).y)
        grid on; axis tight;
        title('Roll', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Pitch)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Pitch, a(1).y)
        grid on; axis tight;
        title('Pitch', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Yaw)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Yaw, a(1).y)
        grid on; axis tight;
        title('Yaw', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.SurfaceSoundSpeed)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.SurfaceSoundSpeed, a(1).y)
        grid on; axis tight;
        title('SurfaceSoundSpeed', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Heave)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Heave, a(1).y)
        grid on; axis tight;
        title('Heave', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.Tide)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.Tide, a(1).y)
        grid on; axis tight;
        title('Tide', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.PingCounter)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.PingCounter, a(1).y)
        grid on; axis tight;
        title('PingCounter', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.SampleFrequency)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.SampleFrequency, a(1).y)
        grid on; axis tight;
        title('SampleFrequency', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.SonarFrequency)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.SonarFrequency, a(1).y)
        grid on; axis tight;
        title('SonarFrequency', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.FishLatitude)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.FishLatitude, a(1).y)
        grid on; axis tight;
        title('Latitude', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.FishLongitude)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.FishLongitude, a(1).y)
        grid on; axis tight;
        title('Longitude', 'Interpreter', 'none')
    end

    if ~isempty(a(1).Sonar.ShipLatitude)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.ShipLatitude, a(1).y)
        grid on; axis tight;
        title('Latitude', 'Interpreter', 'none')
    end


    if ~isempty(a(1).Sonar.ShipLongitude)
        k = k + 1;
        h(k) = subplot(1, nbAxes, k);
        plot(a(1).Sonar.ShipLongitude, a(1).y)
        grid on; axis tight;
        title('Longitude', 'Interpreter', 'none')
    end


    if ~isempty(a(1).Sonar.FishLatitude) && ~isempty(a(1).Sonar.FishLongitude)
        f = FigUtils.createSScFigure(48674); hold on; grid on;
        coul = 'brgmyc';
        nbCoul = length(coul);
        iCoul = length(findobj(f, 'Type', 'line'));
        iCoul = 1 + mod(iCoul, nbCoul);
        plot(a(1).Sonar.FishLongitude, a(1).Sonar.FishLatitude, coul(iCoul));
        title('Navigation'); axis equal
    end

    if ~isempty(a(1).Sonar.ShipLatitude) && ~isempty(a(1).Sonar.ShipLongitude)
        FigUtils.createSScFigure(48674); hold on; grid on;
%         coul = 'kbrgmyc';
%         nbCoul = length(coul);
        %             iCoul = length(findobj(f, 'Type', 'line'));
        %             iCoul = 1 + mod(iCoul, nbCoul);
        plot(a(1).Sonar.ShipLongitude, a(1).Sonar.ShipLatitude, 'k');
        title('Navigation'); axis equal
    end
end
linkaxes(h, 'y')

%% On recherche les images ayant le m�me Tag

function sub = process_synchro(this)

for i=1:length(this)
    TagSynchroX{i} = this(i).TagSynchroX; %#ok<AGROW>
end
[T, ~, J] =  unique(TagSynchroX);
for i=1:length(T)
    sub{i} = find(J == i); %#ok<AGROW>
end
