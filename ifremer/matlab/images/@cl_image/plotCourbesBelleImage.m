% Visualisation des courbes servant a fabriquer la "Belle Image"
%
% Syntax
%   plotCourbesBelleImage(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plotCourbesBelleImage(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));

[varargin, fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(fig)
    figure;
end
grid on;
hold off;

x = -85:85;
y = corLambert(x);
plot(x, y, 'k'); grid on; hold on;
str{1} = 'Lambert';

xlabel('Angle (deg)');
ylabel('Compensation (dB)');
title('Compensation de BS "Belle Image"')

coul = 'brgmyc';
nbCoul = length(coul);
for i=1:length(sub)
    bilan = this.CourbesStatistiques(sub(i)).bilan;
    
    titreV = bilan{1}(1).titreV;
    NY     = bilan{1}(1).NY;
    nomX   = bilan{1}(1).nomX;
    
    nbDim = length(NY);
    if nbDim == 1
        iCoul = 1 + mod(i, nbCoul);
        
        if isfield(bilan{1}, 'nomZoneEtude')
            strIdent = [titreV ' / ' nomX ' / ' bilan{1}(1).nomZoneEtude];
        else
            strIdent = [titreV ' / ' nomX];
        end
        
        if isfield(bilan{1}, 'model')
            nomModel = get(bilan{1}.model, 'nom');
            if strcmp(nomModel, 'BSLurton')
                cmenu = uicontextmenu;
                uimenu(cmenu, 'Text', strIdent);
                x = get(bilan{1}.model, 'XData');
                model = bilan{1}.model;
                Params = get_valParams(model);
                y = compute(model) - Params(3);
                
                plot(x, y, coul(iCoul), 'UIContextMenu', cmenu);
                grid on;
                
                str{end+1} = bilan{1}(1).nomZoneEtude; %#ok
            else
                strWarn = sprintf('Seul un model BSLurton peut servir a realiser une belle image. Or votre model est : %s', nomModel);
                my_warndlg(strWarn, 1);
            end
        else
            my_warndlg('No model available', 1);
        end
    end
end
legend(str)
