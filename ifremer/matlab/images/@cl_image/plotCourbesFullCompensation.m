% Visualisation des courbes servant a fabriquer la "Belle Image"
%
% Syntax
%   plotCourbesFullCompensation(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plotCourbesFullCompensation(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));
if isempty(sub)
    my_warndlg(Lang('Pas de courbe definie et encore moins de modele.', 'No curve defined and of course no model.'), 1);
end

[varargin, fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(fig)
    fig = FigUtils.createSScFigure;
end
grid on;
hold off;

str = [];

xlabel('Angle (deg)');
ylabel('Compensation (dB)');
title('Compensation de BS "Belle Image"')

coul = 'brgmyc';
nbCoul = length(coul);
for i=1:length(sub)
    bilan = this.CourbesStatistiques(sub(i)).bilan;
    
    titreV = bilan{1}(1).titreV;
    NY     = bilan{1}(1).NY;
    nomX   = bilan{1}(1).nomX;
    
    nbDim = length(NY);
    if nbDim == 1
        iCoul = 1 + mod(i, nbCoul);
        
        if isfield(bilan{1}, 'nomZoneEtude')
            strIdent = [titreV ' / ' nomX ' / ' bilan{1}(1).nomZoneEtude];
        else
            strIdent = [titreV ' / ' nomX];
        end
        
        if isfield(bilan{1}, 'model')
            %             nomModel = get(bilan{1}.model, 'nom');
            cmenu = uicontextmenu;
            uimenu(cmenu, 'Text', strIdent);
            x = get(bilan{1}.model, 'XData');
            model = bilan{1}.model;
            %             Params = get_valParams(model);
            y = compute(model);
            
            plot(x, y, coul(iCoul), 'UIContextMenu', cmenu);
            hold on; grid on;
            
            str{end+1} = bilan{1}(1).nomZoneEtude; %#ok
        end
    else
        my_warndlg('No model available', 1);
    end
end
if isempty(str)
    delete(fig)
    my_warndlg('No Lurton model available for this curve', 1);
else
    legend(str)
end
