% Calcul de statistiques conditionnelles
%
% Syntax
%   plotCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function fig = plotCourbesStats(this, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);

[flag, fig] = plot_CourbesStats(this.CourbesStatistiques, 'Mute', Mute, varargin{:});
if ~flag && Mute
    message_NoCurve(this)
end
