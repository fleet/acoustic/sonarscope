function plotCourbesStatsAzimuths(this, Azimuths, IncAngleBeg, IncAngleEnd, IncAngleStep, ReflecBeg, ReflecEnd, varargin)

[varargin, Tag]   = getPropertyValue(varargin, 'Tag', []);
[varargin, Titre] = getPropertyValue(varargin, 'Titre', []);
[varargin, sub]   = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));
% [varargin, flagStats]    = getPropertyValue(varargin, 'Stats', 0);
% [varargin, flagMedian]   = getPropertyValue(varargin, 'Median', 0);
% [varargin, Offsets]      = getPropertyValue(varargin, 'Offsets', []);
% [varargin, LineWidth]    = getPropertyValue(varargin, 'LineWidth', 0.5);
% [varargin, subTypeCurve] = getPropertyValue(varargin, 'subTypeCurve', 1:2);

if isempty(sub) || ((length(sub) == 1) && (sub== 0))
    message_NoCurve(this)
    return
end

if sub(1) < 0 % Astuce pour dire les nbAzimuths derni�res courbes
    sub = length(this.CourbesStatistiques) + sub + 1;
end

if ~isempty(Tag)
    for k=1:length(sub)
        if strcmp(this.CourbesStatistiques(sub(k)).bilan{1}(1).Tag, Tag)
            subsub(k) = logical(1); %#ok
        else
            subsub(k) = logical(0); %#ok
        end
    end
    sub = sub(subsub);
end

[varargin, last] = getFlag(varargin, 'last');
if last
    sub = sub(end);
end

if isempty(sub)
    message_NoCurve(this)
    return
end

[varargin, fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(fig)
    fig = FigUtils.createSScFigure;
end

nbAzimuths = length(Azimuths);
if nbAzimuths ~= length(sub)
    str1 = 'Le nombre d''azimuts doit �tre �gal au nombre de courbes.';
    str2 = 'The number of azimuths must be equal to the number of curves.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

xUnique = [];
strLegendAzimuths = cell(nbAzimuths,1);
for k=1:nbAzimuths
    if ~ishandle(fig)
        return
    end
    %     strLegendAzimuths = CourbesStatsPlot(this.CourbesStatistiques(sub(k)).bilan, 'fig', fig, ...
    %         'Stats', flagStats, 'iCoul', k, 'Median', flagMedian, 'Offsets', Offsets, 'LineWidth', LineWidth, ...
    %         'subTypeCurve', subTypeCurve);
    bilan = this.CourbesStatistiques(sub(k)).bilan{1};
    xUnique = unique([xUnique bilan.x]);
    strLegendAzimuths{k} = bilan.titreV;
    %     str = [str strLegendAzimuths]; %#ok
end

nbIncidenceAngles = length(xUnique);
y = NaN(nbAzimuths,nbIncidenceAngles);
for k=1:nbAzimuths
    bilan = this.CourbesStatistiques(sub(k)).bilan{1};
    y(k,:) = interp1(bilan.x, bilan.y, xUnique);
    %     PlotUtils.createSScPlot(xUnique, y(k,:)); grid on; hold on;
end

% TODO : In case we have only one way
% Azimuths = mod([Azimuths(:); Azimuths(:)+180], 360);
% y = [y ;y];

[~,ordre] = sort(Azimuths);
Azimuths = Azimuths(ordre);
y = y(ordre,:);
Azimuths(end+1) = Azimuths(1);
y(end+1,:) = y(1,:);

% Color = 'rgbgmck';
% Color = 'ymcrgb';
% Color = Color';
Color = [0.8 0.5 0
    0 1 1
    1 0 0
    0 1 0
    0 0 1
    0 0 0
    0.5 0 0.9
    0.9 0.75 0
    1 0.4 0
    0.5 0.5 0.5];
nc = size(Color, 1);
LineStyle = {'-'; '-.'};
nl = length(LineStyle);

% {
% xUnique2 = xUnique(1):sepIncidence:xUnique(end);
xUnique2 = IncAngleBeg:IncAngleStep:IncAngleEnd;
xUnique2 = centrage_magnetique(xUnique2);
nbIncidenceAngles = length(xUnique2);
strLegendIncidences = cell(nbIncidenceAngles, 1);
step2 = IncAngleStep / 2;
% {
for k=1:nbIncidenceAngles
    %     kc = 1 + mod(k-1, nc);
    %     kl = 1 + floor((k-1) / nc);
    %     kl = 1 + mod(kl-1, nl);
    %     kc = 1 + mod(k-1, nc);
    kc = 1 + mod(floor(abs(xUnique2(k)) / IncAngleStep), nc);
    %     kl = 1 + floor((k-1) / nc);
    %     kl = 1 + mod(kl-1, nl);
    if xUnique2(k) >= 0
        kl = 1;
    else
        kl = 2;
    end
    ksub = find((xUnique >= (xUnique2(k)-step2)) & (xUnique < (xUnique2(k)+step2)));
    %     subplot(1,2,1);
    hc = PlotUtils.createSScPlot(Azimuths(1:end-1), mean(y(1:end-1,ksub),2)); grid on; hold on; %#ok<FNDSB>
    set(hc, 'Color', Color(kc,:), 'LineStyle', LineStyle{kl})
    strLegendIncidences{k} = sprintf('Inc angle %s (deg)', num2str(xUnique2(k)));
end
set(gca, 'XTick', Azimuths(1:end-1))
legend(strLegendIncidences, 'Interpreter', 'None', 'Location','NorthEastOutside')
title('Backscatter / Azimuths (dB)')
% }

if isempty(ReflecBeg)
    RMin = min(y(:));
    RMax = max(y(:));
    RMin = 5 * floor(RMin / 5);
    RMax = 5 * ceil(RMax / 5);
else
    RMin = ReflecBeg;
    RMax = ReflecEnd;
end
% TTickValue = 0:30:360;
TTickValue = Azimuths;
FontSize = 20;
FigUtils.createSScFigure;
for k=1:nbIncidenceAngles
    %     kc = 1 + mod(k-1, nc);
    ksub = find((xUnique >= (xUnique2(k)-step2)) & (xUnique < (xUnique2(k)+step2)));
    %     subplot(1,2,1);
    %     mmpolar(Azimuths*(pi/180), reflec_dB2Enr(mean(y(:,ksub),2)), Color(kc), ...
    %         'Style', 'compass'); grid on; hold on; %#ok<FNDSB>
    if k == 1
        mmpolar(Azimuths*(pi/180), mean(y(:,ksub),2), ...
            'Style', 'compass', 'RLimit', [RMin RMax], 'TTickValue', TTickValue, 'FontSize', FontSize);
        grid on;
        % TODO : loook why we have to repeat twice this plot.
        hc = mmpolar(Azimuths*(pi/180), mean(y(:,ksub),2), ...
            'Style', 'compass', 'RLimit', [RMin RMax], 'TTickValue', TTickValue, 'FontSize', FontSize);
        grid on;
        h = gca;
        hold on;
    else
        hc = mmpolar(h, Azimuths*(pi/180), mean(y(:,ksub),2), ...
            'Style', 'compass', 'RLimit', [RMin RMax], 'TTickValue', TTickValue, 'FontSize', FontSize);
    end
    strLegendIncidences{k} = sprintf('Inc angle %s (deg)', num2str(xUnique2(k)));
end
for k=1:nbIncidenceAngles
    %     kc = 1 + mod(k-1, nc);
    kc = 1 + mod(floor(abs(xUnique2(k)) / IncAngleStep), nc);
    %     kl = 1 + floor((k-1) / nc);
    %     kl = 1 + mod(kl-1, nl);
    if xUnique2(k) >= 0
        kl = 1;
    else
        kl = 2;
    end
    set(hc(k), 'Color', Color(kc,:), 'LineStyle', LineStyle{kl}, 'LineWidth', 2)
    %     set(gca, 'FontSize', 10)
end
legend(strLegendIncidences, 'Interpreter', 'None', 'Location','NorthEastOutside')
% title('Backscatter / Azimuths (Enr)')





%{
for k=1:nbAzimuths
kc = 1 + mod(k-1, nc);
%     subplot(1,2,2);
polar(Azimuths(:) * (pi/180), reflec_dB2Enr(y(:,k)), Color(kc)); grid on; hold on;
end
legend(strLegendAzimuths, 'Interpreter', 'None', 'Location', 'SouthOutside')
title('Backscatter / Azimuths (Enr)')
%}

%{
FigUtils.createSScFigure;
for k=1:nbIncidenceAngles
kc = 1 + mod(k-1, nc);
%     subplot(1,2,2);
PlotUtils.createSScPlot(Azimuths(1:end-1), reflec_dB2Enr(y(1:end-1,k)), Color(kc)); grid on; hold on;
end
legend(strLegendAzimuths, 'Interpreter', 'None', 'Location', 'SouthOutside')
title('Backscatter / Azimuths (Enr)')
%}

%{
FigUtils.createSScFigure;
for k=1:nbIncidenceAngles
kc = 1 + mod(k-1, nc);
%     subplot(1,2,2);
polar(Azimuths(1:end-1)*(pi/180), reflec_dB2Enr(y(1:end-1,k)), Color(kc)); grid on; hold on;
end
legend(strLegendAzimuths, 'Interpreter', 'None', 'Location', 'SouthOutside')
title('Backscatter / Azimuths (Enr)')
%}

%{
figure
for k=1:nbAzimuths
kc = 1 + mod(k-1, nc);
%     subplot(1,2,2);
polar(xUnique * (pi/180), reflec_dB2Enr(y(k,:)), Color(kc)); grid on; hold on;
end
legend(strLegendAzimuths, 'Interpreter', 'None', 'Location', 'SouthOutside')
title('Backscatter / Azimuths (Enr)')
%}

% if isempty(Titre)
%     Titre = this.CourbesStatistiques(sub(k)).bilan{1}.titreV;
% end
% set(fig, 'Name', ['  Curve coming from ' Titre])
