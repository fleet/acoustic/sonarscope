% Lancement de l'�criture dans un fichier GMT (*.grd)
%
% SYNTAX :
%   flag = plotGMT(this, nomFic)
%
% INPUT PARAMETERS :
%   a      : Instance de cl_image
%   nomFic : Nom du fichier (type.grd)
%
% OUTPUT PARAMETERS :
%   flag : 1 si ecriture reussie, 0 sinon
%
% EXAMPLES :
%   nomFicIn = getNomFicDatabase('india_geoid.grd')
%   a = import_gmt(cl_image, nomFicIn);
%   nomFic = my_tempname('.grd')
%   flag = export_gmt(a, nomFic);
%
%   flag = plotGMT(a, nomFic);
%
% SEE ALSO : cl_image/import_gmt Authors
% AUTHORS  : GLU
%--------------------------------------------------------------------------

function flag = plotGMT6(this, gmtDlg, nomFic, nomFicElevation, varargin)

%# function eps2xxx eps2xxx_memmapfile

global IfrTbx %#ok<GVMIS>

[varargin, PathGMT]     = getPropertyValue(varargin, 'PathGMT', []); 
[subx, suby, varargin]  = getSubxSuby(this, varargin); 

% XLim = get(this,'XLim');
XLim(1) = min(this.x(subx));
XLim(2) = max(this.x(subx));
% YLim = get(this,'YLim');
YLim(1) = min(this.y(suby));
YLim(2) = max(this.y(suby));

if isempty(this.Carto)
    [flag, this.Carto] = editParams(cl_carto([]), mean(YLim), mean(XLim));
    if ~flag
        return
    end
end

WorkInProgress('GMT preparing scriptGMT.bat file');

flag = 1;

dirSave = pwd;

[pathname, filename] = fileparts(nomFic);
nomFic = fullfile(pathname, [filename '.grd']);
% origFileName = filename;
% Recuperation du prefixe du fichier.
titreImage = this.Name;

% Calcul des bornes Min, Max du trac�.
[XTickLabels, YTickLabels] = get_TickLabels(this, 'suby', suby, 'subx', subx);
if ~isempty(XTickLabels)
    XTickInterval = XTickLabels(2) - XTickLabels(1);
else
    XTickInterval = roundToSup((XLim(2) - XLim(1))/5);
end
if ~isempty(YTickLabels)
    YTickInterval = YTickLabels(2) - YTickLabels(1);
else
    YTickInterval = roundToSup((YLim(2) - YLim(1))/5);
end
lon0 = XLim(1);
lon1 = XLim(2);
lat0 = YLim(1);
lat1 = YLim(2);

pathname = strrep(pathname,'\','/');

% Cr�ation de la table de couleurs (fichier <filename>.cpt sous C:\TEMP)
colorPaletteGMT(this, pathname, filename);

try
    % R�cup�ration des options d'impression
    Commentaire     = gmtDlg.comment;
    IndexFmtPapier  = gmtDlg.paperSize;
    PrintMode       = gmtDlg.printingMode;
    TypeScale       = gmtDlg.scaleType;
    FormatTicks     = gmtDlg.tickmarksFormat;
    FlagGrid        = gmtDlg.grid;
    IndexTypeProj   = gmtDlg.projection;
    long_merid_orig = gmtDlg.longitudeParam.Value;
    lat_ech_cons    = gmtDlg.latitudeParam.Value;
    FirstParallel   = gmtDlg.firstParallel.Value;
    SecondParallel  = gmtDlg.secondParallel.Value;
    UTMZone         = gmtDlg.utmZone;
    UTMHemisphere   = gmtDlg.hemisphere;
    FlagTraitDeCote = gmtDlg.isCoastLines;
    FlagContour     = gmtDlg.isContourLines;
    StyleCadre      = gmtDlg.borderStyle;
    FlagEchelleGeo  = ~gmtDlg.isGeometricScale;
    FlagVisuPDF     = gmtDlg.isPdfDisplay;
    ListeFicNav     = gmtDlg.navPathList;
    
    if FlagContour
        Contours.Deb                 = gmtDlg.firstIsoBathLevelParam.Value;
        Contours.Fin                 = gmtDlg.lastIsoBathLevelParam.Value;
        Contours.AnnotationStep      = gmtDlg.isoBathLabelingParam.Value;
        Contours.Step                = gmtDlg.isoBathStepParam.Value;
        Contours.StepColor           = gmtDlg.contourColor * 255;
        Contours.AnnotationStepColor = gmtDlg.ticksColor   * 255;
        
        if length(Contours.StepColor) == 1
            Contours.StepColor = [0 0 0];
        end
        if length(Contours.AnnotationStepColor) == 1
            Contours.AnnotationStepColor = [0 0 0];
        end
    end
    
    % Saisie du type de l'�chelle de trac�.
    if (TypeScale == 1)   % Echelle Geo
        valScale = gmtDlg.denomScaleParam.Value;
    end
    
    % Chargement des formats de trac�s reconnus.
    TabFmtPapier = struct('Label', {}, 'XSize', [], 'YSize', []);
    XA0 = 21   * 4;         % Taille en X du format A0 papier en cm
    YA0 = 29.7 * 4;
    for iLoop=1:11
        TabFmtPapier(iLoop).Label = sprintf('A%d', iLoop-1);
        if (iLoop > 1)
            TabFmtPapier(iLoop).XSize = XA0/(iLoop-1);
            TabFmtPapier(iLoop).YSize = YA0/(iLoop-1);
        else
            TabFmtPapier(iLoop).XSize = XA0;
            TabFmtPapier(iLoop).YSize = YA0;
        end
    end
    
    if isempty(PathGMT)
        [flag, keyGMT, PathGMT, vGMT] = identify_gmt_vers; %#ok<ASGLU>
    end
    
    if ~exist(PathGMT, 'dir')
        Path = getenv('path');
        Path = strsplit(Path, ';');
        PathGMT = [];
        for k=1:length(Path)
            mots = strsplit(Path{k}, filesep);
            if (length(mots) >=2) && strcmpi(mots{end-1}, 'GMT') && strcmpi(mots{end}, 'bin')
                PathGMT = Path{k};
                break
            end
        end
        
        if isempty(PathGMT) % Ceinture et bretelles
            str1 = 'Indiquez le r�pertoire d''installation GMT\bin svp.';
            str2 = 'Please select the GMT\bin folder.';
            [flag, PathGMT] = my_uigetdir('C:\', Lang(str1,str2));
            if ~flag
                return
            end
        end
    end
    cd(PathGMT);
    %end
    
    % Pr�alable n�cessaire � toute op�ration sur GMT.
    cmd = sprintf('!gmtset PROJ_LENGTH_UNIT cm') %#ok<NOPRT>
    eval(cmd)
    
    str = {};
    str{end+1} = sprintf('REM		GMT Script auto');
    str{end+1} = sprintf('REM');
    str{end+1} = sprintf('REM Fichier inspir� des exemples avec GMT, livr�s partiellement avec SonarScope');
    str{end+1} = sprintf('REM Trace du fichier .grd apr�s son traitement puis export');
    str{end+1} = sprintf('REM ===========================================================================');
    str{end+1} = sprintf('echo GMT fichier genere par SonarScope');
    str{end+1} = sprintf('set master=y');
    str{end+1} = sprintf('if exist scriptGMT.bat set master=n');
    str{end+1} = sprintf('if %%master%%==y  cd %s', pathname);
    str{end+1} = sprintf('set longmin=%f', lon0);
    str{end+1} = sprintf('set longmax=%f', lon1);
    str{end+1} = sprintf('set latmin=%f',  lat0);
    str{end+1} = sprintf('set latmax=%f',  lat1);
    str{end+1} = sprintf('set versSonarScope=%s', VersionsMatlab);
    
    str{end+1} = sprintf('gmtset COLOR_BACKGROUND 225/225/225');
    str{end+1} = sprintf('gmtset PROJ_LENGTH_UNIT cm');
    str{end+1} = sprintf('gmtset GMT_INTERPOLANT Linear');
    str{end+1} = sprintf('gmtset FONT_TITLE 12');
    str{end+1} = sprintf('gmtset MAP_TITLE_OFFSET 0c\n');
    str{end+1} = sprintf('gmtset MAP_ANNOT_OFFSET_PRIMARY 0.35c');
    str{end+1} = sprintf('gmtset FONT_ANNOT_PRIMARY 10p'); % ANNOT_FONT_SIZE for GMT4.
    switch StyleCadre
        case 1
            str{end+1} = sprintf('gmtset MAP_FRAME_RGB 255/255/255');
        case 2
            str{end+1} = sprintf('gmtset MAP_FRAME_TYPE plain');
        otherwise
            str{end+1} = sprintf('gmtset MAP_FRAME_TYPE fancy');
    end
    if sign(lon0) == 1 &&  sign(lon1) == 1
        signePlotDegree = '+'; %#ok<NASGU>
    else
        signePlotDegree = '-'; %#ok<NASGU>
    end
    if FormatTicks == 1 % Degres-Minutes-Secondes d�cimales
        % Le signe '-' n'est pas autoris� sous GMT5.
        % str{end+1} = sprintf('gmtset FORMAT_GEO_MAP %sddd:mm:ss', signePlotDegree);
        str{end+1} = sprintf('gmtset FORMAT_GEO_MAP %sddd:mm:ss', '+');
    elseif FormatTicks == 2 % Degr�s d�cimaux
        % Le signe '-' n'est pas autoris� sous GMT5.
        % str{end+1} = sprintf('gmtset FORMAT_GEO_MAP %sD.xxx', signePlotDegree);
        str{end+1} = sprintf('gmtset FORMAT_GEO_MAP %sD.xxx', '+');
    else % Degres-Minutes d�cimales
        % Le signe '-' n'est pas autoris� sous GMT5.
        % str{end+1} = sprintf('gmtset FORMAT_GEO_MAP %sddd:mm:ss', signePlotDegree);
        str{end+1} = sprintf('gmtset FORMAT_GEO_MAP %sddd:mm:ss', '+');
    end
    str{end+1} = sprintf('gmtset FORMAT_GEO_OUT +D');
    str{end+1} = sprintf('gmtset FORMAT_FLOAT_OUT %%%%8.3f');
    str{end+1} = sprintf('gmtset PROJ_ELLIPSOID %s', getCodeGMTEllipsoide(this.Carto));
    str{end+1} = sprintf('gmtset PS_MEDIA %s', TabFmtPapier(IndexFmtPapier).Label);
    
    
    % Choix du type de Projection.
    % Cf . doc DNIS/ESI/DTI/03-022-1.1, librairie CartoLib.
    CLA_Carto = cl_carto('Projection.Type', IndexTypeProj + 1); % Index Proj de Carto = Index Prog de getOptions + 1 (c'est Malin : GLU).
    switch IndexTypeProj
        case 1
            TypeProjection = 'M';
            if (TypeScale == 1)
                TypeProjection = lower(TypeProjection);
            end
            TestProjection = sprintf('-J%c%f/%f/', TypeProjection, long_merid_orig, lat_ech_cons);
            PlotProjection = sprintf('-J%c%f/%f/', TypeProjection, long_merid_orig, lat_ech_cons);
            ProjectionName = sprintf('%s Long Centrale= %.2f, Lat cons.= %.2f', Lang('MERCATOR', 'MERCATOR'),long_merid_orig,lat_ech_cons);
            OffsetScale = 0.5;
        case 2
            % Calcul de la zone
            TypeProjection = 'U';
            if (TypeScale == 1)
                TypeProjection = lower(TypeProjection);
            end
            TestProjection = sprintf('-J%c%d/', TypeProjection, UTMZone);
            PlotProjection = sprintf('-J%c%d/', TypeProjection, UTMZone);
            ProjectionName = sprintf('%s- Zone %d', Lang('UTM', 'UTM'), UTMZone);
            OffsetScale = 2;
        case 3
            % %             G0 = get(CLA_Carto,'Projection.Lambert.long_merid_orig');
            % %             Paral_1er = get(CLA_Carto,'Projection.Lambert.first_paral');
            % %             Paral_2nd = get(CLA_Carto,'Projection.Lambert.second_paral');%#ok
            TypeProjection = 'L';
            if (TypeScale == 1)
                TypeProjection = lower(TypeProjection);
            end
            % A essayer --> TestProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, (lon1-lon0)/2, lat0, Paral_1er, Paral_2nd);
            % A essayer --> PlotProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, G0, lat0, Paral_1er, Paral_2nd);
            %             TestProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, (lon1-lon0)/2, lat0, lat0, lat1);
            %             PlotProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, G0, lat0, Paral_1er, lat1);
            TestProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, (lon1-lon0)/2, lat0, lat0, lat1);
            PlotProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, long_merid_orig, lat0, FirstParallel, lat1);
            ProjectionName = sprintf('%s%d', Lang('LAMBERT', 'LAMBERT'), get(CLA_Carto, 'Projection.Lambert.Type'));
            OffsetScale = 0.5;
            
        case 4
            long_merid_horiz = get(CLA_Carto, 'Projection.Stereo.long_merid_horiz');
            lat_ech_cons = get(CLA_Carto, 'Projection.Stereo.lat_ech_cons');
            TypeProjection = 'S';
            if (TypeScale == 1)
                TypeProjection = lower(TypeProjection);
            end
            TestProjection = sprintf('-J%c%f/%f/', TypeProjection, (lon1-lon0)/2, sign(lat1)*90);
            PlotProjection = sprintf('-J%c%f/%f/', TypeProjection, (lon1-lon0)/2, sign(lat1)*90);
            ProjectionName = sprintf('%s Long Oriz : %s, Lat cons. : %s', Lang('Stereographique', 'Stereographique'),cell2str(long_merid_horiz),cell2str(lat_ech_cons));
            OffsetScale = 0.5;
            
        case 5 % Oblique Mercator
            TypeProjection = 'O';
            if (TypeScale == 1)
                TypeProjection = lower(TypeProjection);
            end
            Azimuth = 30;
            LongPointG = mean(XLim);
            LatPointG  = mean(YLim, 2);
            TestProjection = sprintf('-J%ca%f/%f/%f/', TypeProjection, LongPointG, LatPointG, Azimuth);
            PlotProjection = sprintf('-J%ca%f/%f/%f/', TypeProjection, LongPointG, LatPointG, Azimuth);
            ProjectionName = sprintf('%s Long Centrale= %.2f, Lat cons.= %.2f', Lang('MERCATOR', 'MERCATOR'), ...
                long_merid_orig, lat_ech_cons);
            OffsetScale = 0.5;
    end
    
    % Evaluation de la taille et du mode de trac�.
    if TypeScale == 1
        PlotScale = sprintf('%s%d', '1:', valScale);
        cmd = sprintf('!echo %f %f | mapproject %s%sc -R%f/%f/%f/%f', lon1, lat1, ...
            TestProjection, PlotScale, lon0, lon1, lat0, lat1);
        disp(cmd)
        valScale0 = valScale;
    else
        if PrintMode == 1 % Determination auto. du mode d'impression
            PlotSize = TabFmtPapier(IndexFmtPapier).YSize;
        elseif PrintMode == 2 % Mode Portrait
            PlotSize = TabFmtPapier(IndexFmtPapier).XSize;
        else % Mode Paysage
            PlotSize = TabFmtPapier(IndexFmtPapier).YSize;
        end
        cmd = sprintf('!echo %f %f | mapproject %s%fc -R%f/%f/%f/%f', lon1, lat1, ...
            TestProjection, PlotSize, lon0, lon1, lat0, lat1);
        disp(cmd)
    end
    strOut = evalc(cmd);
    mots = strsplit(strOut, newline);
    XYPlotSize = str2num(mots{1}); %#ok % (1) Taille en X, (2) Taille en Y.
    XPlotSize = XYPlotSize(1);
    YPlotSize = XYPlotSize(2);
    
    if PrintMode == 1 % Determination auto. du mode d'impression
        if YPlotSize > XPlotSize % Mode Portrait, X = Larg, Y = Haut(unit� = cm).
            XMarge = 1;
            YMarge = 2;
            PlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
            PlotMode = '-P';
            LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
            HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
        else % Mode Paysage, X = Larg, Y = Haut (unit� = cm).
            XMarge = 2;
            YMarge = 2;
            PlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
            PlotMode = '';
            LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
            HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
        end
        
    elseif PrintMode == 2 % Mode Portrait
        XMarge = 1;
        YMarge = 2;
        PlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
        PlotMode = '-P';
        LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
        HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
    else % Mode Paysage, X = Larg, Y = Haut (unit� = cm).
        XMarge = 2;
        YMarge = 2;
        PlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
        PlotMode = '';
        LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
        HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
    end
    PlotSize0 = PlotSize;
    
    
    HautSize = YPlotSize;
    LargSize = XPlotSize;
    
    % Ajustement de la taille selon la projection (� 2 cm pr�s de la taille
    % limite)
    iLoop = 0;
    iReducLarg = 0;
    iReducHaut = 0;
    while (LargSize > LargMaxPlotSize || HautSize > HautMaxPlotSize)
        if (LargSize > LargMaxPlotSize)
            iReducLarg = iReducLarg + 1;
        end
        if (HautSize > HautMaxPlotSize)
            iReducHaut = iReducHaut + 1;
        end
        %Facteur croissant ou d�croissant
        if LargSize < LargMaxPlotSize && HautSize < HautMaxPlotSize
            break;
        elseif LargSize > LargMaxPlotSize || HautSize > HautMaxPlotSize
            ScaleFactor = -0.1;  % R�duction des trac�s.
        elseif LargMaxPlotSize > LargSize || HautMaxPlotSize > HautSize
            ScaleFactor = 0.1; % Agrandissement des trac�s.
        end
        iLoop = iLoop+1;
        if TypeScale == 1 % Echelle g�om�trique
            valScale = valScale0*(1 - ScaleFactor*iLoop); % Augmentation d'echelle de 10%
            PlotScale = sprintf('%s%d', '1:', valScale);
            cmd = sprintf('!echo %f %f | mapproject %s%sc -R%f/%f/%f/%f', lon1, lat1, ...
                TestProjection, PlotScale, lon0, lon1, lat0, lat1);
            disp(cmd)
        else                % Echelle en cm
            PlotSize = PlotSize0*(1 + ScaleFactor*iLoop); % Reduction (ou Augmentation) de 10%
            cmd = sprintf('!echo %f %f | mapproject %s%fc -R%f/%f/%f/%f', lon1, lat1, ...
                TestProjection, PlotSize, lon0, lon1, lat0, lat1);
            disp(cmd)
        end
        strOut = evalc(cmd);
        mots = strsplit(strOut, newline);
        XYPlotSize = str2num(mots{1}); %#ok % (1) Taille en X, (2) Taille en Y.
        LargSize = XYPlotSize(1);  % Egale au PlotSize calcul�
        HautSize = XYPlotSize(2);
    end
    if iLoop ~= 0
        str1 = sprintf('%d ajustement(s) de l''echelle de 10 pour cent (dont en largeur: %d et en hauteur: %d\n)', iLoop, iReducLarg, iReducHaut);
        str2 = sprintf('%d readjustment(s) of scale of 10 per cents  (for width:%d and for height:%d)\n', iLoop, iReducLarg, iReducHaut);
        fprintf(Lang(str1,str2));
    end
    
    if TypeScale ~= 1 % Pied de pilote pour l'affichage du libell� du graphique.
        PlotSize = PlotSize - 1;
        LargSize = LargSize -1;
    end
    titreImage = strrep(titreImage,':','-');
    % if (this.DataType == cl_image.indDataType('Bathymetry')) && (get(this.Carto,'Projection.Type') == 3)
    %     ProjectionOption = sprintf('-JU%d/%dc', get(this.Carto,'Projection.UTM.Fuseau'), PlotSize);
    % else
    if (TypeScale == 1) % Echelle g�om�trique
        ProjectionOption = sprintf('%s%s', PlotProjection, PlotScale);
        titrePlot = sprintf('%s - 1/%d %s', titreImage, valScale, ProjectionName);
    else
        ProjectionOption = sprintf('%s%dc', PlotProjection, PlotSize);
        titrePlot = sprintf('%s %s', titreImage, ProjectionName);
    end
    % end
    if this.ImageType ~= 1 % Pas de trac� d'�chelle
        if strcmp(PlotMode, '-P') % Mode Portrait
            YMarge = 0;
        else                      % Mode Paysage
            XMarge = 0; %#ok
        end
        OffsetUnixTime = -1.5;
    else
        if strcmp(PlotMode, '-P') % Mode Portrait
            OffsetUnixTime = -3;
        else                      % Mode Paysage
            OffsetUnixTime = -1.5;
        end
    end
    
    if strcmp(PlotMode, '-P')	% Mode Portrait
        XOffset = (TabFmtPapier(IndexFmtPapier).XSize - LargSize)/2;
        YOffset = (TabFmtPapier(IndexFmtPapier).YSize - HautSize)/2 + YMarge;
        if LargSize > 15 % On fixe 15 cm de largeur par d�faut.
            ScaleOption = sprintf('-D%fc/-1.2c/%dc/0.5ch', LargSize/2, LargSize);
        else % Centrage de l'�chelle au milieu de la page.
            ScaleOption = sprintf('-D%fc/-1.2c/%dc/0.5ch', 1, 15);
        end
        % Positionnement de la valeur par d�faut pour forcer le mode au besoin.
        str{end+1} = sprintf('gmtset PS_PAGE_ORIENTATION portrait');
        % HeaderSize = 8;
    else                        % Mode Paysage
        % Centrage du trac� sur la feuille
        XOffset = (TabFmtPapier(IndexFmtPapier).YSize - LargSize)/2;
        YOffset = (TabFmtPapier(IndexFmtPapier).XSize - HautSize)/2;
        % largeur echelle = taille verticale - titre.
        if LargSize+OffsetScale > 15 % On fixe 15 cm de largeur par d�faut.
            ScaleOption = sprintf('-D%fc/%fc/%dc/0.3c', LargSize+OffsetScale, (HautSize-YMarge)/2, HautSize-YMarge);
        else
            ScaleOption = sprintf('-D%fc/%fc/%dc/0.3c', LargSize+OffsetScale, (15-YMarge)/2, 15);
        end
        str{end+1} = sprintf('gmtset PS_PAGE_ORIENTATION landscape');
        % HeaderSize = 14;
    end
    if FlagGrid == 1
        GridOptionX = '';
        GridOptionY = '';
    else
        NbXTicks  = (lon1-lon0)/XTickInterval;
        NbXTicksMax = max(floor(LargSize),2);  % 1 ticks par cm (min = 2)
        if NbXTicks > NbXTicksMax
            XTickInterval = roundToSup((lon1-lon0) / NbXTicksMax);
        end
        GridOptionX = sprintf('g%d', XTickInterval);
        GridOptionY = sprintf('g%d', YTickInterval);
        if FlagGrid == 3
            str{end+1} = sprintf('gmtset MAP_GRID_CROSS_SIZE_PRIMARY 0.5c');
            str{end+1} = sprintf('gmtset MAP_GRID_PEN_PRIMARY 0.5p');
        end
    end
    
    commentTexte = sprintf('SonarScope - V%%versSonarScope%% - IFREMER - %s', Commentaire);
    NomFicPs = fullfile(pathname, [filename '.ps']);
    if exist(NomFicPs, 'file')
        try
            delete(NomFicPs)
        catch %#ok<CTCH>
        end
    end
    
    if this.ImageType == 2
        Name_r = fullfile(pathname, [filename '_r.grd']);
        Name_g = fullfile(pathname, [filename '_g.grd']);
        Name_b = fullfile(pathname, [filename '_b.grd']);
        str{end+1} = sprintf('grdedit -A "%s"', Name_r);
        str{end+1} = sprintf('grdedit -A "%s"', Name_g);
        str{end+1} = sprintf('grdedit -A "%s"', Name_b);
        str{end+1} = sprintf('grdimage "%s" "%s" "%s" %s -C"%s/%s.cpt" %s -K -X%dc -Y%dc -U/0c/%dc/"%s" > "%s"\n', ...
            Name_r, Name_g, Name_b, ProjectionOption, pathname, filename, PlotMode, XOffset, YOffset, OffsetUnixTime, ...
            commentTexte, NomFicPs);
    else
        str{end+1} = sprintf('grdedit -A "%s"', nomFic);
        str{end+1} = sprintf('grdimage "%s" %s -C"%s/%s.cpt" %s -K -X%dc -Y%dc -U/0c/%dc/"%s" > "%s"\n', ...
            nomFic, ProjectionOption, pathname, filename, PlotMode, XOffset, YOffset, OffsetUnixTime, ...
            commentTexte, NomFicPs);
    end
    if FlagContour
        if isempty(nomFicElevation)
            nomFicElevation = nomFic;
        else
            str{end+1} = sprintf('grdedit -A "%s"', nomFicElevation);
        end
        
        NbChiffresApresLaVirgule = max(0,-floor(log10(Contours.AnnotationStep)));
        str{end+1} = sprintf('gmtset FORMAT_FLOAT_OUT %%%%%d.%df', 5+NbChiffresApresLaVirgule, NbChiffresApresLaVirgule);
        
        CodeCouleurC = sprintf('%d/%d/%d', Contours.AnnotationStepColor(1), Contours.AnnotationStepColor(2), Contours.AnnotationStepColor(3));
        CodeCouleurA = sprintf('%d/%d/%d', Contours.StepColor(1), Contours.StepColor(2), Contours.StepColor(3));
        
%         str{end+1} = sprintf('grdcontour "%s" %s -X0c -Y0c -C%f -A%f+k%s -Gd5c -T -Wc1/%s -Wa3/%s -O -S -K >> "%s"\n', ...
%             nomFicElevation, ProjectionOption, Contours.Step, Contours.AnnotationStep, CodeCouleurC, CodeCouleurC, CodeCouleurA, NomFicPs);

% Modif JMA le 16/04/2020 pour ne plus avoir
% grdcontour(GMT_grdcontour): Warning: 1/0/0/0 not a valid number and may not be decoded properly. 
% grdcontour(GMT_grdcontour): Warning: 3/0/0/0 not a valid number and may not be decoded properly. 
        str{end+1} = sprintf('grdcontour "%s" %s -X0c -Y0c -C%f -A%f+f%s -Gd5c -T -O -S -K >> "%s"\n', ...
            nomFicElevation, ProjectionOption, Contours.Step, Contours.AnnotationStep, CodeCouleurC, NomFicPs);
    end
    
    if FlagTraitDeCote
        % Trac� de c�t� avec des surfaces min de 5 km2.
        str{end+1} = sprintf('pscoast -J -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -A5.0 -W0.5p/0/0/0 -Df -O -K >> "%s"\n', ...
            NomFicPs);
    end
    
    str{end+1} = sprintf('gmtset FORMAT_FLOAT_OUT %%%%8.3f');
    
    if FlagEchelleGeo
        % Choix d'un trac� d'�chelle g�om�trique.
        % Attention au colori de fond de l'�chelle.
        str{end+1} = sprintf('gmtset MAP_SCALE_HEIGHT 0.2c');
        str{end+1} = sprintf('gmtset FONT_LABEL 9p');
        str{end+1} = sprintf('gmtset COLOR_BACKGROUND 0/0/0');
        cmd = sprintf('!project -C%f/%f -E%f/%f -G100000 -Q', lon0, (lat1-lat0)/2, lon1, (lat1-lat0)/2);
        strOut = evalc(cmd);
        mots = strsplit(strOut, newline);
        pppp = str2num(mots{2}); %#ok % 2�me ligne, 3�me �l�ment = Distance
        LengthScale = roundToSup(pppp(3)/10);
        % GeoScale  = sprintf('-Lf%f/%f/%f/%fk:km:b', lat_ech_cons, long_merid_orig, lat_ech_cons, LengthScale);
        % Echelle en km
        if strcmp(PlotMode, '-P') % Mode portrait
            GeoScale = sprintf('-Lfx%f/%f/%f/%fk', HautSize/2, OffsetUnixTime-0.5, lat_ech_cons, LengthScale);
        else
            GeoScale = sprintf('-Lfx%f/%f/%f/%fk', LargSize/2, OffsetUnixTime-0.5, lat_ech_cons, LengthScale);
        end
    else
        GeoScale = '';
    end
    
    % Recup�ration des graduations de la table des couleurs.
    
    if strcmpi(TabFmtPapier(IndexFmtPapier).Label,'A4')
        str{end+1} = sprintf('gmtset FONT_ANNOT_PRIMARY 7p'); % ANNOT_FONT_SIZE for GMT4
    elseif strcmpi(TabFmtPapier(IndexFmtPapier).Label,'A0')
        str{end+1} = sprintf('gmtset FONT_ANNOT_PRIMARY 14p'); % ANNOT_FONT_SIZE for GMT4
    else
        str{end+1} = sprintf('gmtset FONT_ANNOT_PRIMARY 10p'); % ANNOT_FONT_SIZE for GMT4
    end
    if this.ImageType == 1
        str{end+1} = sprintf('psbasemap -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -J %s -Bf%d%sa%d/f%d%sa%dWESN:."%s": -K -O >> "%s"\n', ...
            GeoScale, XTickInterval, GridOptionX, XTickInterval, ...
            YTickInterval, GridOptionY, YTickInterval, titrePlot, NomFicPs);
        
        %{
% Trac� UNH
str{end+1} = sprintf('psbasemap -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -J %s -Bf10.0cg10.0ca10.0c/f10.0cg10.0ca10.0cWESN:."%s": -K -O >> "%s"\n', ...
GeoScale, titrePlot, NomFicPs);
            %}
            
            if iscell(ListeFicNav)
                for k=1:length(ListeFicNav)
                    str{end+1} = sprintf('psxy -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -J "%s" -O -K -G100/100/100 >> "%s"\n', ...
                        ListeFicNav{k}, NomFicPs);  %#ok<AGROW>
                    str{end+1} = sprintf('psxy -R -J "%s" -O -K -Sc0.10c >> "%s"\n', ...
                        ListeFicNav{k}, NomFicPs);  %#ok<AGROW>
                end
            end
            str{end+1} = sprintf('gmtset FONT_ANNOT_PRIMARY 10p'); % ANNOT_FONT_SIZE for GMT4
            CTickLabels = get_TickColorLabels(this);
            CTickScale = CTickLabels(2) - CTickLabels(1);
            CMarkScale = ceil(CTickScale);
            str{end+1} = sprintf('gmtset MAP_ANNOT_OFFSET_PRIMARY 0.1c');
            str{end+1} = sprintf('psscale %s -C"%s/%s.cpt" -B%df%d -O >> "%s"\n', ...
                ScaleOption, pathname, filename, CTickScale, CMarkScale, NomFicPs);
    else
        if iscell(ListeFicNav)
            for k=1:length(ListeFicNav)
                str{end+1} = sprintf('psxy -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -J "%s" -O -K -G100/100/100 >> "%s"\n', ...
                    ListeFicNav{k}, NomFicPs);  %#ok<AGROW>
                str{end+1} = sprintf('psxy -R -J "%s" -O -K -Sc0.10c >> "%s"\n', ...
                    ListeFicNav{k}, NomFicPs);  %#ok<AGROW>
            end
        end
        
        str{end+1} = sprintf('psbasemap -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -J %s -Bf%d%sa%d/f%d%sa%dWESN:."%s": -O >> "%s"\n', ...
            GeoScale, XTickInterval, GridOptionX, XTickInterval, ...
            YTickInterval, GridOptionY, YTickInterval, titrePlot, NomFicPs);
        
        %{Trac� UNH
        str{end+1} = sprintf('psbasemap -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -J %s -Bf10.0cg10.0ca10.0c/f10.0cg10.0ca10.0cWESN:."%s": -O >> "%s"\n', ...
            GeoScale, titrePlot, NomFicPs);
        %}
    end
    
    %Insertion d'un cartouche
    %str{end+1} = sprintf('%secho # 0 0 12 0.0 1 BC 15p %dc j > tmp',HeaderSize);
    %str{end+1} = sprintf('%secho @;255/0/0;SonarScope@;; - V%%versSonarScope%% - @:12:IFREMER-TSI/AS@:: >> tmp');
    %str{end+1} = sprintf('%spstext tmp -R0/1/0/1 -Jx1/1 -O -N -M# -W255po0.5p %s %s  >> %s/%s.ps\n', ...
    %    str, HeaderOption, HeaderOffset, pathname, filename);
    
    % Repositionnement des valeurs par d�faut.
    str{end+1} = sprintf('gmtset COLOR_BACKGROUND 0/0/0');
    str{end+1} = sprintf('gmtset FONT_TITLE 36p');
    str{end+1} = sprintf('gmtset GMT_INTERPOLANT Akima');
    str{end+1} = sprintf('gmtset MAP_ANNOT_OFFSET_PRIMARY 0.2c');
    str{end+1} = sprintf('gmtset FONT_ANNOT_PRIMARY 14p'); % ANNOT_FONT_SIZE for GMT4
    str{end+1} = sprintf('gmtset FORMAT_GEO_MAP +ddd:mm:ss');
    str{end+1} = sprintf('gmtset FORMAT_GEO_OUT +D');
    str{end+1} = sprintf('gmtset FORMAT_FLOAT_OUT %%%%lg');
    str{end+1} = sprintf('gmtset PS_PAGE_ORIENTATION landscape');
    str{end+1} = sprintf('gmtset PROJ_ELLIPSOID WGS-84');
    str{end+1} = sprintf('gmtset PS_MEDIA a4');
    str{end+1} = sprintf('gmtset MAP_SCALE_HEIGHT 0.075i');
    str{end+1} = sprintf('gmtset FONT_LABEL 24p');
    
    % str{end+1} = sprintf('%sdel *.cpt');
    str{end+1} = sprintf('del tmp');
    str{end+1} = sprintf('del .gmt*');
    
    str{end+1} = sprintf('if %%master%%==y cd .. ');
    
    % Effacement des fichiers GMT cr��s temporairement (gmtdefault et
    % gmtcommands) - FDI 08_02 - Maintenace SonarScope
    delete .gmt*;
    
    cd(pathname);
    nomScript = fullfile(my_tempdir, 'scriptGMT.bat');
    fid = fopen(nomScript, 'w+');
    for k=1:length(str)
        fprintf(fid,'%s\n', str{k});
    end
    fclose(fid);
    
    msg = sprintf(Lang('Si vous �tes familier de GMT, vous pouvez visualiser le script "%s" et ajouter vos propres commandes GMT.\n\nAttention, le fichier de sortie <Test.grd> est �cras� � chaque Export GMT depuis SonarScope.', 'If you are familiar with GMT you can edit the file "%s", add your own commands and execute it.\n\nWarning : file <Test.grd> is erased at each GMT exportation.'), nomScript);
    my_warndlg(msg, 0);
    
    cd(PathGMT);
    
    WorkInProgress('GMT processing scriptGMT.bat file');
    cmd = sprintf('! %s', nomScript) %#ok<NOPRT>
    eval(cmd)
    
    WorkInProgress('GMT creating postcript file');
    nomFicPs = fullfile(pathname, [filename '.ps']);
    if isdeployed
        cmd = sprintf('!winopen.exe gsview64 "%s"', nomFicPs);
    else
        cmd = sprintf('!%s\\Installation\\Viewers\\winopen.exe gsview64 "%s"', IfrTbx, nomFicPs);
    end
    if FlagVisuPDF
        nomFicPDF = fullfile(pathname, [filename '.pdf']);
        % Chemin de GhostScript (diff�rentes versions)
        % Attention, pour l'instant, la version est fig�e !!!! (GLU)
        versGS = '8.51';
        pppp = [];
        try
            pppp = winqueryreg('name', 'HKEY_LOCAL_MACHINE', ['Software\AFPL Ghostscript\' versGS]);
        catch %#ok<CTCH>
            try
                pppp = winqueryreg('name', 'HKEY_CURRENT_USER', ['Software\AFPL Ghostscript\' versGS]);
            catch %#ok<CTCH>
            end
        end
        
        if ~isempty(pppp)
            for k=1:size(pppp,1)
                strGS = winqueryreg('HKEY_LOCAL_MACHINE', ['Software\AFPL Ghostscript\' versGS], pppp{k});
                binFound = strfind(strGS,'bin');
                if binFound ~= 0
                    break
                end
            end
            pathGhostScript = strGS(1:binFound-1);
        else
            pppp = dir('C:\Program Files\gs\gs*');
            if isempty(pppp)
                pppp = dir('C:\Program Files (x86)\gs\gs*');
                if ~isempty(pppp)
                    pathGhostScript = fullfile('C:\Program Files (x86)\gs\', pppp(1).name);
                end
            else
                pathGhostScript = fullfile('C:\Program Files\gs\', pppp(1).name);
            end
        end
        if ~isempty(pathGhostScript)
            WorkInProgress('GMT creating PDF file');
            fullGsPath = fullfile(pathGhostScript, 'bin\gswin32c.exe');
            
            try
                % Tester eps2xxx_memmapfile � la place de eps2xxx
                [flag, msg] = eps2xxx_memmapfile(nomFicPs, {'pdf'}, fullGsPath, 0, lower(TabFmtPapier(IndexFmtPapier).Label));
                if flag
                    
                    % D�termination du nom de l'ex�cutable Acrobat Reader.
                    %nomExeAcrobat = readExeAcrobat;
                    %                     nomExeAcrobat = 'C:\Program Files\Adobe\Acrobat 7.0\Reader\AcroRd32.exe'
                    
                    try
                        
                        fprintf('try to open pdf file %s\n',nomFicPDF);
                        winopen(nomFicPDF)
                    catch %#ok<CTCH>
                        disp('Could not open pdf file')
                        msg = sprintf(Lang('V�rifier l''existence et le chemin de Acrobat Reader.',...
                            'Check at existence and path of Acrobat Reader.'));
                        my_warndlg(msg, 1);
                    end
                else
                    my_warndlg(msg, 1);
                end
            catch %#ok<CTCH>
                msg = sprintf(Lang('Probl�me conversion de PS en PDF','Problem in conversion PS to PDF'));
                my_warndlg(msg, 1);
            end
        else
            msg = sprintf(Lang('V�rifier l''existence et le chemin de GhostScript (version 8.51)','Check at existence and path of GhostScript (version 8.51)'));
            my_warndlg(msg, 1);
        end
    end
    cd(PathGMT);
    
    cmd %#ok<NOPRT>
    eval(cmd)
catch %#ok<CTCH>
    pwd
    pathname %#ok<NOPRT>
    cd(pathname);
    pwd
    
    % Suppression des fichiers .gmtdefault et .gmtcommands cr��es et non
    % supprim�s car �chec du script sous le r�pertoire d'enregistrement.
    disp('try catch : catch reached here')
    cmd = '!del tmp' %#ok<NOPRT>
    try
        eval(cmd)
    catch %#ok<CTCH>
        disp('eval(cmd) failed')
    end
    cmd = '!del .gmt*.*' %#ok<NOPRT>
    try
        eval(cmd)
    catch %#ok<CTCH>
        disp('eval(cmd) failed')
    end
end

cd(dirSave);
