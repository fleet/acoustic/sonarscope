% Lancement de l'�criture dans un fichier GMT (*.grd)
%
% Syntax
%   flag = plotGMTMultiLayer(this, indImage, indLayers, gmtDlg,nomFic, ...)
%
% Input Arguments
%   a         : Instance de cl_image
%   indImage  : indice de r�f�rence
%   indLayers : indices des Layers s�lectionn�s
%   gmtDlg    : options GMT
%   nomFic    : liste de noms de fichiers enregistr�s.
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%   nomFicIn{1} = getNomFicDatabase('india_geoid.grd')
%   nomFicIn{1} = getNomFicDatabase('france.grd')
%   a[1] = import_gmt(cl_image, nomFicIn{1});
%   a[2] = import_gmt(cl_image, nomFicIn{2});

%   flag = plotGMTMultiLayer(a, 1, [1 2], options);
%
% See also cl_image/import_gmt Authors
% Authors : GLU
%--------------------------------------------------------------------------

function [flag, nomDirOut] = plotGMTMultiLayer(this, indImage, indLayers, nomFicPs, gmtDlg, OffsetInterLayer, ...
    rep3D, repMapOn3D, indLayerMapOn3D, view_az, view_el, Height3D, varargin)

%# function eps2xxx

global IfrTbx %#ok<GVMIS>

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin); %#ok<ASGLU>

XLim = this(indImage).XLim;
YLim = this(indImage).YLim;

if isempty(this(indImage).Carto)
    [flag, this(indImage).Carto] = editParams(cl_carto([]), mean(YLim), mean(XLim));
    if ~flag
        return
    end
end

% R�cup�ration des options d'impression

Commentaire     = gmtDlg.comment;
IndexFmtPapier  = gmtDlg.paperSize;
PrintMode       = gmtDlg.printingMode;
TypeScale       = gmtDlg.scaleType;
FormatTicks     = gmtDlg.tickmarksFormat;
FlagGrid        = gmtDlg.grid;
IndexTypeProj   = gmtDlg.projection;
long_merid_orig = gmtDlg.longitudeParam.Value;
lat_ech_cons    = gmtDlg.latitudeParam.Value;
    FirstParallel   = gmtDlg.firstParallel.Value;
    SecondParallel  = gmtDlg.secondParallel.Value;
UTMZone         = gmtDlg.utmZone;
    UTMHemisphere   = gmtDlg.hemisphere;
FlagTraitDeCote = gmtDlg.isCoastLines;
FlagContour     = gmtDlg.isContourLines;
StyleCadre      = gmtDlg.borderStyle;
FlagEchelleGeo  = ~gmtDlg.isGeometricScale;
FlagVisuPDF     = gmtDlg.isPdfDisplay;
    ListeFicNav     = gmtDlg.navPathList;

[nomDirOut, nomFicOut] = fileparts(nomFicPs);

%% Extraction des images

x = this(indImage).x(subx);
y = this(indImage).y(suby);
pppp = extraction(this(indLayers), 'x', x, 'y', y);

%% Exportation en fichiers .grd

for k=1:length(pppp)
    nomFicGrd{k} = fullfile(nomDirOut, [nomFicOut num2str(k) '.grd']); %#ok
    [pathname, fileNameGrd{k}, ext] = fileparts(nomFicGrd{k});%#ok
    % Cr�ation de la table de couleurs (fichier <filename>.cpt sous C:\TEMP)
    colorPaletteGMT(pppp(k), pathname, fileNameGrd{k});
    flag = export_gmt(pppp(k), nomFicGrd{k});
    if ~flag
        return
    end
end

if rep3D
    pppp = extraction(this(indImage), 'x', x, 'y', y);
    nomFicElevation = fullfile(nomDirOut, [nomFicOut 'Elevation.grd']);
    [pathname, fileNameElevation, ext] = fileparts(nomFicElevation);
    colorPaletteGMT(pppp, pathname, fileNameElevation);
    flag = export_gmt(pppp, nomFicElevation);
    if ~flag
        return
    end
    
    if repMapOn3D
        pppp = extraction(this(indLayerMapOn3D), 'x', x, 'y', y);
        nomFicMapOn3D = fullfile(nomDirOut, [nomFicOut 'Mapping.grd']);
        [pathname, fileNameMapOn3D, ext] = fileparts(nomFicMapOn3D);
        colorPaletteGMT(pppp, pathname, fileNameMapOn3D);
        flag = export_gmt(pppp, nomFicMapOn3D);
        if ~flag
            return
        end
    else
        nomFicMapOn3D = nomFicElevation;
        indLayerMapOn3D = indImage;
    end
else
    if FlagContour && (this(indImage).DataType == cl_image.indDataType('Bathymetry'))
        pppp = extraction(this(indImage), 'x', x, 'y', y);
        nomFicElevation = fullfile(nomDirOut, [nomFicOut 'Elevation.grd']);
        [pathname, fileNameElevation, ext] = fileparts(nomFicElevation);
        colorPaletteGMT(pppp, pathname, fileNameElevation);
        flag = export_gmt(pppp, nomFicElevation);
        if ~flag
            return
        end
    else
        nomFicElevation = [];
    end
end

dirSave = pwd;

% Calcul des bornes Min, Max du trac�.
[XTickLabels, YTickLabels] = get_TickLabels(this(indImage), 'suby', suby, 'subx', subx);
if ~isempty(XTickLabels)
    XTickInterval = XTickLabels(2) - XTickLabels(1);
else
    XTickInterval = roundToSup((XLim(2) - XLim(1))/5);
end
if ~isempty(YTickLabels)
    YTickInterval = YTickLabels(2) - YTickLabels(1);
else
    YTickInterval = roundToSup((YLim(2) - YLim(1))/5);
end
CTickLabels = get_TickColorLabels(this(indImage));
CTickScale = CTickLabels(end) - CTickLabels(1);
CMarkScale = ceil(CTickScale)/2;
Zmin = CTickLabels(1);
Zmax = CTickLabels(end);

lon0 = XLim(1);
lon1 = XLim(2);
lat0 = YLim(1);
lat1 = YLim(2);

if isempty(nomFicElevation)
    pathname = fileparts(nomFicGrd{1});
else
    pathname = fileparts(nomFicElevation);
end
pathname = strrep(pathname,'\','/');


if FlagContour
    Contours.Deb                 = gmtDlg.firstIsoBathLevelParam.Value;
    Contours.Fin                 = gmtDlg.lastIsoBathLevelParam.Value;
    Contours.AnnotationStep      = gmtDlg.isoBathLabelingParam.Value;
    Contours.Step                = gmtDlg.isoBathStepParam.Value;
    Contours.StepColor           = gmtDlg.contourColor * 255;
    Contours.AnnotationStepColor = gmtDlg.ticksColor   * 255;

    if Contours.Deb > Contours.Fin
        pppp = Contours.Deb;
        Contours.Deb = Contours.Fin;
        Contours.Fin = pppp;
    end
end


% Saisie du type de l'�chelle de trac�.
if (TypeScale == 1)   % Echelle Geo
    valScale = get(optGMT, 'DenoScale');
end

% Chargement des formats de trac�s reconnus.
TabFmtPapier = struct('Label', {}, 'XSize', [], 'YSize', []);
XA0 = 21 * 4;         % Taille en X du format A0 papier en cm
YA0 = 29.7 * 4;
for iLoop=1:11
    TabFmtPapier(iLoop).Label = sprintf('A%d', iLoop-1);
    if (iLoop > 1)
        TabFmtPapier(iLoop).XSize = XA0/(iLoop-1);
        TabFmtPapier(iLoop).YSize = YA0/(iLoop-1);
    else
        TabFmtPapier(iLoop).XSize = XA0;
        TabFmtPapier(iLoop).YSize = YA0;
    end
end

% Pr�alable n�cessaire � toute op�ration sur GMT.
cmd = sprintf('!gmtset MEASURE_UNIT cm');
eval(cmd);


str = {};
str{end+1} = sprintf('REM		GMT Script auto');
str{end+1} = sprintf('REM');
str{end+1} = sprintf('REM Fichier inspir� des exemples avec GMT, livr�s partiellement avec SonarScope');
str{end+1} = sprintf('REM Trace du fichier .grd apr�s son traitement puis export');
str{end+1} = sprintf('REM ===========================================================================');
str{end+1} = sprintf('echo GMT fichier genere par SonarScope');
str{end+1} = sprintf('set master=y');
str{end+1} = sprintf('if exist scriptGMTMulti.bat set master=n');
str{end+1} = sprintf('if %%master%%==y  cd %s', pathname);
str{end+1} = sprintf('gmtset COLOR_BACKGROUND 225/225/225');
str{end+1} = sprintf('gmtset INTERPOLANT Linear');
str{end+1} = sprintf('gmtset HEADER_FONT_SIZE 12');
str{end+1} = sprintf('gmtset HEADER_OFFSET -0.2c');
str{end+1} = sprintf('gmtset ANNOT_OFFSET_PRIMARY 0.35c');
str{end+1} = sprintf('gmtset ANNOT_FONT_SIZE 10p');
switch StyleCadre
    case 1
        str{end+1} = sprintf('gmtset BASEMAP_FRAME_RGB 255/255/255');
    case 2
        str{end+1} = sprintf('gmtset BASEMAP_TYPE plain');
    otherwise
        str{end+1} = sprintf('gmtset BASEMAP_TYPE fancy');
end
if sign(lon0) == 1 &&  sign(lon1) == 1
    signePlotDegree = '+';
else
    signePlotDegree = '-';
end
if FormatTicks == 1 % Degres-Minutes,00
    str{end+1} = sprintf('gmtset PLOT_DEGREE_FORMAT %sddd:mm:ss', signePlotDegree);
elseif FormatTicks == 2 % Decimal
    str{end+1} = sprintf('gmtset PLOT_DEGREE_FORMAT %sD.xxx', signePlotDegree);
else % Degres-Minutes-Sec
    str{end+1} = sprintf('gmtset PLOT_DEGREE_FORMAT %sddd:mm', signePlotDegree);
end
str{end+1} = sprintf('gmtset OUTPUT_DEGREE_FORMAT +D');
str{end+1} = sprintf('gmtset D_FORMAT %%%%8.3f');
str{end+1} = sprintf('gmtset ELLIPSOID %s', getCodeGMTEllipsoide(this(indImage).Carto));
str{end+1} = sprintf('gmtset PAPER_MEDIA %s+', TabFmtPapier(IndexFmtPapier).Label);

% Choix du type de Projection.
% Cf . doc DNIS/ESI/DTI/03-022-1.1, librairie CartoLib.
CLA_Carto = cl_carto('Projection.Type', IndexTypeProj);
switch IndexTypeProj
    case 1
        TypeProjection = 'M';
        if (TypeScale == 1)
            TypeProjection = lower(TypeProjection);
        end
        TestProjection = sprintf('-J%c%f/%f/', TypeProjection, long_merid_orig, lat_ech_cons);
        PlotProjection = sprintf('-J%c%f/%f/', TypeProjection, long_merid_orig, lat_ech_cons);
        ProjectionName = sprintf('%s Long Centrale= %8.3f, Lat cons.= %8.3f', Lang('MERCATOR', 'MERCATOR'),long_merid_orig,lat_ech_cons);
        %         XMarge = 3;
        %         YMarge = 3;
        XMarge = 0.5;
        YMarge = 0.5;
        
    case 2
        % Calcul de la zone
        %             Zone = get(this(indImage).Carto, 'Projection.UTM.Fuseau');
        %             letter = get(this(indImage).Carto, 'Projection.UTM.Hemisphere');%#ok
        TypeProjection = 'U';
        if (TypeScale == 1)
            TypeProjection = lower(TypeProjection);
        end
        TestProjection = sprintf('-J%c%d/', TypeProjection, UTMZone);
        PlotProjection = sprintf('-J%c%d/', TypeProjection, UTMZone);
        ProjectionName = sprintf('%s- Zone %d', Lang('UTM', 'UTM'), UTMZone);
        XMarge = 3;
        YMarge = 4;
        
    case 3
        G0 = get(CLA_Carto,'Projection.Lambert.long_merid_orig');
        Paral_1er = get(CLA_Carto,'Projection.Lambert.first_paral');
        Paral_2nd = get(CLA_Carto,'Projection.Lambert.second_paral');%#ok
        TypeProjection = 'L';
        if (TypeScale == 1)
            TypeProjection = lower(TypeProjection);
        end
        % A essayer --> TestProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, (lon1-lon0)/2, lat0, Paral_1er, Paral_2nd);
        % A essayer --> PlotProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, G0, lat0, Paral_1er, Paral_2nd);
        TestProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, (lon1-lon0)/2, lat0, lat0, lat1);
        PlotProjection = sprintf('-J%c%f/%f/%f/%f/', TypeProjection, G0, lat0, Paral_1er, lat1);
        ProjectionName = sprintf('%s%d', Lang('LAMBERT', 'LAMBERT'), get(CLA_Carto, 'Projection.Lambert.Type'));
        XMarge = 3;
        YMarge = 4;
        
    case 4
        long_merid_horiz = get(CLA_Carto, 'Projection.Stereo.long_merid_horiz');
        lat_ech_cons = get(CLA_Carto, 'Projection.Stereo.lat_ech_cons');
        TypeProjection = 'S';
        if (TypeScale == 1)
            TypeProjection = lower(TypeProjection);
        end
        TestProjection = sprintf('-J%c%f/%f/', TypeProjection, (lon1-lon0)/2, sign(lat1)*90);
        PlotProjection = sprintf('-J%c%f/%f/', TypeProjection, (lon1-lon0)/2, sign(lat1)*90);
        ProjectionName = sprintf('%s Long Oriz : %s, Lat cons. : %s', Lang('Stereographique', 'Stereographique'),cell2str(long_merid_horiz),cell2str(lat_ech_cons));
        XMarge = 3;
        YMarge = 3;
end
RoseProjection = sprintf('-Tx0/0/2c');

% Evaluation de la taille et du mode de trac�.
% PRENDRE EN COMPTE LE NOMBRE LAYERS A AFFICHER POUR LA TAILLE DU TRACE
%GLUGLUGLUGLU
% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if TypeScale == 1
    PlotScale = sprintf('%s%d', '1:', valScale);
    cmd = sprintf('!echo %f %f | mapproject %s%sc -R%f/%f/%f/%f', lon1, lat1, ...
        TestProjection, PlotScale, lon0, lon1, lat0, lat1);
    disp(cmd)
    valScale0 = valScale;
else
    if PrintMode == 1 % Determination auto. du mode d'impression
        PlotSize = TabFmtPapier(IndexFmtPapier).YSize - XMarge*2;
    elseif PrintMode == 2 % Mode Portrait
        PlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
    else % Mode Paysage
        PlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
    end
    cmd = sprintf('!echo %f %f | mapproject %s%fc -R%f/%f/%f/%f', lon1, lat1, ...
        TestProjection, PlotSize, lon0, lon1, lat0, lat1);
    disp(cmd)
end
strOut = evalc(cmd);
mots = strsplit(strOut, newline);
XYPlotSize = str2num(mots{1}); %#ok % (1) Taille en X, (2) Taille en Y.
M(1,:) = [XYPlotSize(1) 0];
M(2,:) = [0 XYPlotSize(2)];
M(3,:) = [XYPlotSize(1) XYPlotSize(2)];
c = cosd(180-view_az);
s = sind(180-view_az);
% abs(M * [c s; -s c]);
MP = M * [c s; -s c];
MP(4,:) = MP(1,:) - MP(2,:);
MP = max(abs(MP));
XPlotSize = MP(1);
YPlotSize = MP(2);
YPlotSize = YPlotSize * cosd(view_el);

YPlotSize = YPlotSize + (length(indLayers) + double(rep3D == 1) - 1) * OffsetInterLayer;
YPlotSize = YPlotSize + double(rep3D == 1) * Height3D;


XYPlotSize(1) = XPlotSize;
XYPlotSize(2) = YPlotSize;
%     XPlotSize = XYPlotSize(1);
%     YPlotSize = XYPlotSize(2);

LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;

if PrintMode == 1 % Determination auto. du mode d'impression
    if (YPlotSize / TabFmtPapier(IndexFmtPapier).YSize) > (XPlotSize / TabFmtPapier(IndexFmtPapier).XSize)
        % Mode portrait
        PlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
        PlotMode = '-P';
        LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
        HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
        PlotSize0 = PlotSize;
    else % Mode Paysage (unit� = cm).
        PlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
        PlotMode = '';
        LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
        HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
        PlotSize0 = PlotSize;
    end
    
elseif PrintMode == 2 % Mode Portrait
    PlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
    PlotMode = '-P';
    LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - XMarge*2;
    HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
    PlotSize0 = PlotSize;
else % Mode Paysage
    PlotSize = TabFmtPapier(IndexFmtPapier).YSize - YMarge*2;
    PlotMode = '';
    LargMaxPlotSize = TabFmtPapier(IndexFmtPapier).XSize - YMarge*2;
    HautMaxPlotSize = TabFmtPapier(IndexFmtPapier).YSize - XMarge*2;
    PlotSize0 = PlotSize;
end


% Ajustement de la taille selon la projection (� 2 cm pr�s de la taille
% limite)
iLoop = 0;
iReducLarg = 0;
iReducHaut = 0;
HautSize = YPlotSize;
LargSize = PlotSize;

while (LargSize > LargMaxPlotSize) || (HautSize > HautMaxPlotSize)
    if (LargSize > LargMaxPlotSize)
        iReducLarg = iReducLarg + 1;
    end
    if (HautSize > HautMaxPlotSize)
        iReducHaut = iReducHaut + 1;
    end
    %Facteur croissant ou d�croissant
    if (LargSize < LargMaxPlotSize) && (HautSize < HautMaxPlotSize)
        break
    elseif (LargSize > LargMaxPlotSize) || (HautSize > HautMaxPlotSize)
        ScaleFactor = -0.1;  % R�duction des trac�s.
    elseif LargMaxPlotSize > LargSize || HautMaxPlotSize > HautSize
        ScaleFactor = 0.1; % Agrandissement des trac�s.
    end
    iLoop = iLoop+1;
    PlotSize = PlotSize0 * (1 - 0.1*iLoop); % Reduction de 10%
    % PRENDRE EN COMPTE LE NOMBRE LAYERS A AFFICHER POUR LA TAILLE DU TRACE
    %GLUGLUGLUGLUL
    % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if TypeScale == 1 % Echelle g�om�trique
        valScale = valScale0*(1 + ScaleFactor*iLoop); % Augmentation d'echelle de 10%
        PlotScale = sprintf('%s%d', '1:', valScale);
        cmd = sprintf('!echo %f %f | mapproject %s%sc -R%f/%f/%f/%f', lon1, lat1, ...
            TestProjection, PlotScale, lon0, lon1, lat0, lat1);
        disp(cmd)
    else                % Echelle en cm
        cmd = sprintf('!echo %f %f | mapproject %s%fc -R%f/%f/%f/%f', lon1, lat1, ...
            TestProjection, PlotSize, lon0, lon1, lat0, lat1);
        disp(cmd)
    end
    strOut = evalc(cmd);
    mots = strsplit(strOut, newline);
    XYPlotSize = str2num(mots{1}); %#ok % (1) Taille en X, (2) Taille en Y.
    LargSize = XYPlotSize(1);  % Egale au PlotSize calcul�
    HautSize = XYPlotSize(2);
end
if iLoop ~= 0
    msg = sprintf(Lang('%d ajustement(s) de l''echelle de 10%% (dont en largeur: %d et en hauteur: %d)', '%d readjustment(s) of scale of 10 %% (for width:%d and for height:%d)'), iLoop, iReducLarg, iReducHaut);
    my_warndlg(msg, 1);
end

if TypeScale ~= 1 % Pied de pilote pour l'affichage du libell� du graphique.
    PlotSize = PlotSize - 1;
    LargSize = LargSize -1;
end
if (TypeScale == 1) % Echelle g�om�trique
    ProjectionOption = sprintf('%s%s', PlotProjection, PlotScale);
else
    ProjectionOption = sprintf('%s%dc', PlotProjection, PlotSize);
end
if this(indImage).ImageType ~= 1 % Pas de trac� d'�chelle
    if strcmp(PlotMode, '-P') % Mode Portrait
        YMarge = 0;
    else                      % Mode Paysage
        XMarge = 0;%#ok
    end
    OffsetUnixTime = -1.5;
else
    if strcmp(PlotMode, '-P') % Mode Portrait
        OffsetUnixTime = -3;
    else                      % Mode Paysage
        OffsetUnixTime = -1.5;
    end
end

if strcmp(PlotMode, '-P')	% Mode Portrait
    XOffset = (TabFmtPapier(IndexFmtPapier).XSize - LargSize)/2;
    YOffset = (TabFmtPapier(IndexFmtPapier).YSize - HautSize)/2 + YMarge;
    % Positionnement de la valeur par d�faut pour forcer le mode au besoin.
    str{end+1} = sprintf('gmtset PAGE_ORIENTATION portrait');
    % HeaderSize = 8;
else                        % Mode Paysage
    % Centrage du trac� sur la feuille
    XOffset = (TabFmtPapier(IndexFmtPapier).YSize - LargSize)/2;
    YOffset = (TabFmtPapier(IndexFmtPapier).XSize - HautSize)/2;
    % largeur echelle = taille verticale - titre.
    str{end+1} = sprintf('gmtset PAGE_ORIENTATION landscape');
    % HeaderSize = 14;
end

if FlagGrid == 1
    GridOptionX = '';
    GridOptionY = '';
else
    GridOptionX = sprintf('g%d', XTickInterval);
    GridOptionY = sprintf('g%d', YTickInterval);
    if FlagGrid == 3
        str{end+1} = sprintf('gmtset GRID_CROSS_SIZE_PRIMARY 0.5c');
        str{end+1} = sprintf('gmtset GRID_PEN_PRIMARY 0.5p');
    end
end

str{end+1} = sprintf('set longmin=%f',lon0);
str{end+1} = sprintf('set longmax=%f',lon1);
str{end+1} = sprintf('set latmin=%f',lat0);
str{end+1} = sprintf('set latmax=%f',lat1);

str{end+1} = sprintf('set view_az=%f', view_az);
str{end+1} = sprintf('set view_el=%f', view_el);

% v = []; %VersionsSonarScope;
% str{end+1} = sprintf('set versSonarScope=%s',v);

commentTexte = sprintf('SonarScope - IFREMER - %s', Commentaire);
try
    delete(nomFicPs)
catch %#ok<CTCH>
end

if rep3D
    str{end+1} = sprintf('grdedit -A "%s"', nomFicElevation);
    %         str{end+1} = sprintf('grdgradient  "%s" -A0 -G"%s_G.grd" -Nt0.75 -M\n', nomFicElevation, fileNameElevation);%#ok
end

if repMapOn3D
    str{end+1} = sprintf('grdedit -A "%s"', nomFicElevation);
end

if FlagEchelleGeo
    % Choix d'un trac� d'�chelle g�om�trique.
    % Attention au colori de fond de l'�chelle.
    str{end+1} = sprintf('gmtset MAP_SCALE_HEIGHT 0.2c');
    str{end+1} = sprintf('gmtset LABEL_FONT_SIZE 9p');
    str{end+1} = sprintf('gmtset COLOR_BACKGROUND 0/0/0');
    cmd = sprintf('!project -C%f/%f -E%f/%f -G100000 -Q', lon0, (lat1-lat0)/2, lon1, (lat1-lat0)/2);
    strOut = evalc(cmd);
    mots = strsplit(strOut, newline);
    pppp = str2num(mots{2}); %#ok % 2�me ligne, 3�me �l�ment = Distance
    LengthScale = roundToSup(pppp(3)/10);
    
    disp('Attention, l''�chelle se positionne parall�lement � la grille : Comment faire ? Qui peut m''aider ?')
    
    if strcmp(PlotMode, '-P') % Mode portrait
        GeoScale  = sprintf('-Lfx%f/%f/%f/%fk:km:b', HautSize/2, OffsetUnixTime, lat_ech_cons, LengthScale);
    else
        GeoScale  = sprintf('-Lfx%f/%f/%f/%fk:km:b', LargSize/2, OffsetUnixTime, lat_ech_cons, LengthScale);
    end
else
    GeoScale = '';
end


YOffset2 = YMarge;
for k=1:length(indLayers)
    if this(indLayers(k)).ImageType == 2
        Name_r = fullfile(pathname, [fileNameGrd{k} '_r' ext]);
        Name_g = fullfile(pathname, [fileNameGrd{k} '_g' ext]);
        Name_b = fullfile(pathname, [fileNameGrd{k} '_b' ext]);
        str{end+1} = sprintf('grdedit -A "%s"', Name_r); %#ok
        str{end+1} = sprintf('grdedit -A "%s"', Name_g); %#ok
        str{end+1} = sprintf('grdedit -A "%s"', Name_b); %#ok
    else
        str{end+1} = sprintf('grdedit -A "%s"', nomFicGrd{k}); %#ok
    end
    
    if k == 1
        if this(indLayers(k)).ImageType == 2
            str{end+1} = sprintf('grdview "%s" -G"%s","%s","%s" %s -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -C"%s/%s.cpt" -Qi %s -X%dc -Y%dc -U/0c/%dc/"%s" -K > "%s"\n', ...
                Name_r, Name_r, Name_g, Name_b, ProjectionOption, view_az, view_el, pathname, fileNameGrd{k}, PlotMode, XOffset, YOffset2, OffsetUnixTime, ...
                commentTexte, nomFicPs);%#ok
        else
            str{end+1} = sprintf('grdview  "%s" %s -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -C"%s/%s.cpt" -Qi %s -X%dc -Y%dc -U/0c/%dc/"%s" -K > "%s"\n', ...
                nomFicGrd{k}, ProjectionOption, view_az, view_el, pathname, fileNameGrd{k}, PlotMode, XOffset, YOffset2, OffsetUnixTime, ...
                commentTexte, nomFicPs);%#ok
        end
        % Choix d'un trac� d'�chelle g�om�trique.
        % Attention au colori de fond de l'�chelle.
        [rep, validation] = my_questdlg(Lang('Voulez-vous une rose des vents ?','Dou yo want a wind rose ?'));
        if validation && (rep == 1)
            str{end+1} = sprintf('psbasemap -R -J -E%f/%f %s2c --COLOR_BACKGROUND=red --TICK_PEN=0.5p,red -O -K >> "%s"\n', ...
                view_az, view_el, RoseProjection, ...
                nomFicPs);%#ok
        end
        str{end+1} = sprintf('psbasemap -R %s -E%f/%f %s -Bf%d%sa%d/f%d%sa%dneSW -O -K >> "%s"\n', ...
            ProjectionOption, view_az, view_el, PlotMode, XTickInterval, GridOptionX, XTickInterval, ...
            YTickInterval, GridOptionY, YTickInterval, ...
            nomFicPs);%#ok
        
    else
        if this(indLayers(k)).ImageType == 2
            str{end+1} = sprintf('grdview "%s" -G"%s","%s","%s" %s -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -C"%s/%s.cpt" -Qi %s  -Y%dc -O -K >> "%s"\n', ...
                Name_r, Name_r, Name_g, Name_b, ProjectionOption, view_az, view_el, pathname, fileNameGrd{k}, PlotMode, YOffset2, ...
                nomFicPs);%#ok
        else
            str{end+1} = sprintf('grdview  "%s" %s -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -C"%s/%s.cpt" -Qi %s -Y%dc -O -K >> "%s"\n', ...
                nomFicGrd{k}, ProjectionOption, view_az, view_el, pathname, fileNameGrd{k}, PlotMode, YOffset2, ...
                nomFicPs);%#ok
        end
        str{end+1} = sprintf('psbasemap -R %s -E%f/%f %s -BneSW -O -K >> "%s"\n', ...
            ProjectionOption, view_az, view_el, PlotMode, nomFicPs);%#ok
    end
    
    
    if FlagContour && ~isempty(nomFicElevation)
        NbChiffresApresLaVirgule = max(0,-floor(log10(Contours.AnnotationStep)));
        str{end+1} = sprintf('gmtset D_FORMAT %%%%%d.%df', 5+NbChiffresApresLaVirgule, NbChiffresApresLaVirgule); %#ok
        
        
        str{end+1} = sprintf('grdedit -A "%s"', nomFicElevation); %#ok
        
        CodeCouleurC = sprintf('%d/%d/%d', Contours.AnnotationStepColor(1), Contours.AnnotationStepColor(2), Contours.AnnotationStepColor(3));
        CodeCouleurA = sprintf('%d/%d/%d', Contours.StepColor(1), Contours.StepColor(2), Contours.StepColor(3));
        str{end+1} = sprintf('grdcontour "%s" -E%f/%f %s -X0c -Y0c -C%f -A%f+k%s -Gd5c -T -Wc1/%s -Wa3/%s -O -S -K >> "%s"\n', ...
            nomFicElevation, view_az, view_el, ProjectionOption, Contours.Step, Contours.AnnotationStep, CodeCouleurC, CodeCouleurC, CodeCouleurA, nomFicPs); %#ok
    end
    
    
    if FlagTraitDeCote
        % Trac� de c�t� avec des surfaces min de 5 km2.
        str{end+1} = sprintf('pscoast -J -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -A5.0 -W0.5p/0/0/0 -Df -O -K >> "%s"\n', ...
            view_az, view_el, nomFicPs); %#ok
    end
    
    YOffset2 = OffsetInterLayer;
end
str{end+1} = sprintf('gmtset D_FORMAT %%%%7.2f');
str{end+1} = sprintf('gmtset ANNOT_FONT_SIZE 8p');

if rep3D
    str{end+1} = sprintf('grdedit -A "%s"', nomFicElevation);
    GridOptionX = '';
    GridOptionY = '';
    
    if ~isempty(indLayerMapOn3D)
        if this(indLayerMapOn3D).ImageType == 2
            [pppp, nomFicMapOn3DSeul] = fileparts(nomFicMapOn3D);
            Name_r = fullfile(pathname, [nomFicMapOn3DSeul '_r' ext]);
            Name_g = fullfile(pathname, [nomFicMapOn3DSeul '_g' ext]);
            Name_b = fullfile(pathname, [nomFicMapOn3DSeul '_b' ext]);
            str{end+1} = sprintf('grdedit -A "%s"', Name_r);
            str{end+1} = sprintf('grdedit -A "%s"', Name_g);
            str{end+1} = sprintf('grdedit -A "%s"', Name_b);
        else
            str{end+1} = sprintf('grdedit -A "%s"', nomFicMapOn3D);
        end
    end
    
    [pppp, nomFicMapOnCpt] = fileparts(nomFicElevation); %#ok
    [pppp, nomFicMapOnCpt] = fileparts(nomFicMapOn3D);
    if isempty(indLayers)
        if this(indLayerMapOn3D).ImageType == 2
            str{end+1} = sprintf('grdview  "%s"  -G"%s","%s","%s" %s -JZ%fc  -C"%s/%s.cpt" -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%%/%f/%f %s -N%f/lightgray -Qi100 -X%dc -Y%dc -U/0c/%dc/"%s" -K > "%s"\n', ...
                nomFicElevation, Name_r, Name_g, Name_b, ProjectionOption, Height3D, pathname, nomFicMapOnCpt, view_az, view_el, Zmin, Zmax, PlotMode, Zmin, XOffset, YOffset2, OffsetUnixTime, ...
                commentTexte, nomFicPs);
        else
            str{end+1} = sprintf('grdview  "%s"  -G"%s" %s -JZ%fc  -C"%s/%s.cpt" -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%%/%f/%f %s -N%f/lightgray -Qi100 -X%dc -Y%dc -U/0c/%dc/"%s" -K > "%s"\n', ...
                nomFicElevation, nomFicMapOn3D, ProjectionOption, Height3D, pathname, nomFicMapOnCpt, view_az, view_el, Zmin, Zmax, PlotMode, Zmin, XOffset, YOffset2, OffsetUnixTime, ...
                commentTexte, nomFicPs);
        end
    else
        if this(indLayerMapOn3D).ImageType == 2
            str{end+1} = sprintf('grdview "%s"  -G"%s","%s","%s" %s -JZ%fc  -C"%s/%s.cpt" -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%%/%f/%f %s -N%f/lightgray -Qi100 -Y%dc -O -K >> "%s"\n', ...
                nomFicElevation, Name_r, Name_g, Name_b, ProjectionOption, Height3D, pathname, nomFicMapOnCpt, view_az, view_el, Zmin, Zmax, PlotMode, Zmin, YOffset2, ...
                nomFicPs);
        else
            str{end+1} = sprintf('grdview "%s"  -G"%s" %s -JZ%fc  -C"%s/%s.cpt" -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%%/%f/%f %s -N%f/lightgray -Qi100 -Y%dc -O -K >> "%s"\n', ...
                nomFicElevation, nomFicMapOn3D, ProjectionOption, Height3D, pathname, nomFicMapOnCpt, view_az, view_el, Zmin, Zmax, PlotMode, Zmin, YOffset2, ...
                nomFicPs);
        end
    end
    
    %         if FlagTraitDeCote
    %             % Trac� de c�t� avec des surfaces min de 5 km2.
    %             str{end+1} = sprintf('pscoast -J -E%f/%f -R%%longmin%%/%%longmax%%/%%latmin%%/%%latmax%% -A5.0 -W0.5p/0/0/0 -Df -O -K >> "%s"\n', ...
    %                 view_az, view_el, nomFicPs); %#ok
    %         end
    
    str{end+1} = sprintf('psbasemap -R -J -JZ4c %s -E%f/%f %s -Z%f/%f -Bf%d%sa%d/f%d%sa%d/%f::wsZ -O >> "%s"\n', ...
        GeoScale, view_az, view_el, PlotMode, Zmin, Zmax, XTickInterval, GridOptionX, XTickInterval, ...
        YTickInterval, GridOptionY, YTickInterval, CMarkScale, ...
        nomFicPs);
end


str{end+1} = sprintf('gmtset D_FORMAT %%%%8.3f');


% Repositionnement des valeurs par d�faut.
str{end+1} = sprintf('gmtset COLOR_BACKGROUND 0/0/0');
str{end+1} = sprintf('gmtset HEADER_FONT_SIZE 36p');
str{end+1} = sprintf('gmtset INTERPOLANT Akima');
str{end+1} = sprintf('gmtset ANNOT_OFFSET_PRIMARY 0.2c');
str{end+1} = sprintf('gmtset ANNOT_FONT_SIZE 14p');
% str{end+1} = sprintf('gmtset PLOT_DEGREE_FORMAT +ddd:mm:ss');
str{end+1} = sprintf('gmtset OUTPUT_DEGREE_FORMAT +D');
str{end+1} = sprintf('gmtset D_FORMAT %%%%lg');
str{end+1} = sprintf('gmtset PAGE_ORIENTATION landscape');
str{end+1} = sprintf('gmtset ELLIPSOID WGS-84');
str{end+1} = sprintf('gmtset PAPER_MEDIA a4+');
str{end+1} = sprintf('gmtset MAP_SCALE_HEIGHT 0.075i');
str{end+1} = sprintf('gmtset LABEL_FONT_SIZE 24p');

% str{end+1} = sprintf('%sdel *.cpt');
str{end+1} = sprintf('del tmp');
str{end+1} = sprintf('del .gmt*');

str{end+1} = sprintf('if %%master%%==y cd .. ');

cd(pathname);
nomScript = fullfile(nomDirOut, [nomFicOut '.bat']);
fid = fopen(nomScript, 'w+');
for k=1:length(str)
    fprintf(fid,'%s\n', str{k});
end
fclose(fid);

msg = sprintf('If you are familiar with GMT you cant edit the file "%s", add your own preferences\nand execute it.', nomScript);
my_warndlg(msg, 1);

cmd = sprintf('! %s', nomScript);
eval(cmd);

if isdeployed
    cmd = sprintf('!winopen.exe gsview32 "%s"', nomFicPs);
else
    cmd = sprintf('!%s\\Installation\\Viewers\\winopen.exe gsview32 "%s"', IfrTbx, nomFicPs);
end
if FlagVisuPDF
    nomFicPDF = fullfile(nomDirOut, [nomFicOut '.pdf']);
    % Chemin de GhostScript (diff�rentes versions)
    pppp = dir('C:\Program Files\gs\gs*');
    if isempty(pppp)
        pppp = dir('C:\Program Files(x86)\gs\gs*');
        if ~isempty(pppp)
            pathGhostScript = fullfile('C:\Program Files(x86)\gs\', pppp(1).name);
        end
    else
        pathGhostScript = fullfile('C:\Program Files\gs\', pppp(1).name);
    end
    if ~isempty(pppp)
        [rep, flag] = my_questdlg(Lang('TODO', 'If Acroread is open please close it !. Is it OK now?'));
        if ~flag
            return
        end
        
        fullGsPath = fullfile(pathGhostScript, 'bin\gswin32c.exe');
        str = {};
        %             str{end+1} = sprintf('eps2xxx(''%s'',%s,''%s'',%d,''%s'')',nomFicPDF,'{''pdf''}',fullGsPath,0, lower(TabFmtPapier(IndexFmtPapier).Label));
        str{end+1} = sprintf('eps2xxx(''%s'',%s,''%s'',%d,''%s'')', nomFicPs,'{''pdf''}', ...
            fullGsPath, 0, lower(TabFmtPapier(IndexFmtPapier).Label));
        cmdPDF = [str{:}];
        eval(cmdPDF);
        try
            eval(cmdPDF);
            % D�termination du nom de l'ex�cutable Acrobat Reader.
            nomExeAcrobat = readExeAcrobat;
            
            try
                if isdeployed
                    cmdPDF = sprintf('!winopen.exe "%s" "%s"', nomExeAcrobat, nomFicPDF);
                else
                    cmdPDF = sprintf('!%s\\Installation\\Viewers\\winopen.exe "%s" "%s"', IfrTbx, nomExeAcrobat, nomFicPDF);
                end
                eval(cmdPDF);
            catch %#ok<CTCH>
                msg = sprintf(Lang('V�rifier l''existence et le chemin de Acrobat Reader (version 7.0). Utiliser directement votre version actuelle','Check at existence and path of Acrobat Reader (version 7.0) or use your own version of Acrobat Reader.'));
                my_warndlg(msg, 1);
            end
        catch %#ok<CTCH>
            msg = sprintf(Lang('Probl�me conversion de PS en PDF','Problem in conversion PS to PDF'));
            my_warndlg(msg, 1);
        end
    else
        msg = sprintf(Lang('V�rifier l''existence et le chemin de GhostScript (version 8.51)','Check at existence and path of GhostScript (version 8.51)'));
        my_warndlg(msg, 1);
    end
end
eval(cmd);

% catch
%     % Suppression des fichiers .gmtdefault et .gmtcommands cr��es et non
%     % supprim�s car �chec du script sous le r�pertoire d'enregistrement.
%     eval('!del tmp');
%     eval('!del .gmt*.*');
% end
%

cd(dirSave);
