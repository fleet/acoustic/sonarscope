% Affichage des signatures de texture
%
% Syntax
%   plotSegmSignature(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plotSegmSignature(this, varargin)

[varargin, OnlyOneFig]      = getPropertyValue(varargin, 'OnlyOneFig', 2);
[varargin, listeValMasque]  = getPropertyValue(varargin, 'valMask', []);
[varargin, listeNumTexture] = getPropertyValue(varargin, 'numTexture', 1:length(this.Texture));
[varargin, subCliques]      = getPropertyValue(varargin, 'subCliques', []);
[varargin, Weights]         = getPropertyValue(varargin, 'Weights', []); %#ok<ASGLU>

N = length(listeNumTexture);
if N == 0
    my_warndlg('Pas de signatures texturales definies.', 1);
    return
end

if N == 1
    Texture = this.Texture(listeNumTexture);
else
    for k=1:N
        plotSegmSignature(this, 'numTexture', listeNumTexture(k), 'subCliques', subCliques, 'Weights', Weights)
    end
    return
end

if isempty(listeValMasque)
    nbFacies = length(Texture.CmatModeleAngle);
    listeValMasque = [];
    for iFacies=1:nbFacies
        CmatFacies = Texture.CmatModeleAngle{iFacies};
        if ~isempty(CmatFacies)
            listeValMasque(end+1) = iFacies; %#ok
        end
    end
    nbFaciesPossibles = length(listeValMasque);
end
if nbFaciesPossibles == 0
    disp('beurk')
end



if isempty(listeValMasque)
    my_warndlg('Pas de signatures texturales definies.', 1);
    return
end

minIa =  Inf;
maxIa = -Inf;
minJa =  Inf;
maxJa = -Inf;
hAxe = preallocateGraphics(0,0);

if isempty(subCliques)
    subCliques = 1:size(Texture.cliques,1);
end
subCliques(subCliques > size(Texture.cliques,1)) = [];
nbCliques = length(subCliques);
if nbCliques == 0
    return
end

for iClique=1:nbCliques
    str = sprintf('%s - Clique %s', Texture.nomTexture, num2strCode(Texture.cliques(subCliques(iClique),:)));
    if ~isempty(Weights)
        str = sprintf('%s    w=%f', str, Weights(iClique));
    end
    fig = figure('name', str);
    
    if isequal(Texture.binsLayerAngle, 1)
        nbAngles = 1;
    else
        nbAngles  = length(Texture.binsLayerAngle)-1;
    end
    nbMasques = length(listeValMasque);
    
    bins = Texture.binsLayerAngle;
    binsTitle = bins;
    
    for iAngle=1:nbAngles
        if (OnlyOneFig == 2) && ~isequal(Texture.binsLayerAngle, 1)
            figA = figure;
        end
        for j=1:nbFaciesPossibles
            valMask = listeValMasque(j);
            CmatModeleAngle = Texture.CmatModeleAngle{valMask};
            
            if isempty(CmatModeleAngle{iAngle})
                continue
            end
            
            iSubplot = iAngle + (j-1)*nbAngles;
            set(0,'CurrentFigure', fig);
            hAxe(end+1) = subplot(nbMasques, nbAngles, iSubplot); %#ok
            
            if length(bins) == 1
                str = sprintf('%s : %s', Lang('Matrice de cooccurence','Co-occurence matrix'), Texture.nomCourbe{valMask});
                title(str);
                axis on; grid on; axis equal;
            else
                str = sprintf('%s', num2strCode(binsTitle(iAngle:iAngle+1)));
                title(str)
            end
            
            if ~isempty(CmatModeleAngle{iAngle})
                I = CmatModeleAngle{subCliques(iClique),iAngle};
                if sum(I(:)) == 0
                    continue
                end
                
                [Ia, Ja] = find(I >= (0.0001*max(I(:))));
                minIa = min(minIa, min(Ia));
                maxIa = max(maxIa, max(Ia));
                minJa = min(minJa, min(Ja));
                maxJa = max(maxJa, max(Ja));
                
                imagesc(I); axis xy; axis off; axis equal;
                
                if length(bins) == 1
                    title(str);
                    axis on; grid on; axis equal;
                else
                    title(str)
                end
                
                if (OnlyOneFig == 2) && ~isequal(Texture.binsLayerAngle, 1)
                    set(0,'CurrentFigure', figA);
                    hAxe(end+1) = subplot(nbMasques, 1, j); %#ok
                    imagesc(I); axis xy; axis on; grid on; axis equal
                    title(str);
                end
            end
        end
    end
    
    commentaire = Texture.commentaire{valMask};
    if ~isempty(commentaire)
        msg = sprintf('Commentaire Signature Texturale (Figure %d) :\n%s', fig, commentaire);
        my_warndlg(msg, 1);
    end
end
if ~isempty(hAxe)
    for k=1:length(hAxe)
        set(hAxe(k), 'YLim', [minIa maxIa]);
        set(hAxe(k), 'XLim', [minJa maxJa]);
    end
    linkaxes(hAxe)
end
