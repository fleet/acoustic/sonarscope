% Affichage des signatures de texture
%
% Syntax
%   plotSegmSignatureDist(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plotSegmSignatureDist(this, varargin)

% identSegmentation = cl_image.indDataType('Segmentation');
% identMask = cl_image.indDataType('Mask');
% flag = testSignature(this, 'DataType', [identSegmentation identMask], 'noMessage');
% if flag
%     my_warndlg(Lang('Posisionnez vous sur l''image ad�quate SVP.',  'Select the correct image please.'), 1);
%     return
% end


[varargin, listeValMasque]  = getPropertyValue(varargin, 'valMask', []);
[varargin, listeNumTexture] = getPropertyValue(varargin, 'numTexture', 1:length(this.Texture)); %#ok<ASGLU>

N = length(listeNumTexture);
if N == 0
    str1 = 'Cette image ne contient pas de signatures texturales.';
    str2 = 'This image does not contain any textural signatures.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if N == 1
    Texture = this.Texture(listeNumTexture);
    
    if isequal(Texture.binsLayerAngle, 1)
        nbAngles = 1;
    else
        nbAngles  = length(Texture.binsLayerAngle)-1;
    end
    
    if nbAngles > 1
        str1 = sprintf('%s (%d angles) : D�sol�, cette fonctionnalit� n''est possible que pour des matrice de cooccurrence sans conditionnement angulaire.', ...
            Texture.nomTexture, nbAngles);
        str2 = sprintf('%s (%d angles) : Sorry, this fonctionnality is not possible for cooccuurrence matrix conditioned by angles.', ...
            Texture.nomTexture, nbAngles);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    
else
    for i=1:N
        plotSegmSignatureDist(this, 'numTexture', listeNumTexture(i))
    end
    return
end

if isempty(listeValMasque)
    nbFacies    = length(Texture.CmatModeleAngle);
    listeValMasque = [];
    for iFacies=1:nbFacies
        CmatFacies = Texture.CmatModeleAngle{iFacies};
        if ~isempty(CmatFacies)
            listeValMasque(end+1) = iFacies; %#ok
        end
    end
    nbFaciesPossibles = length(listeValMasque);
end
if nbFaciesPossibles == 0
    disp('beurk')
end



if isempty(listeValMasque)
    my_warndlg('Pas de signatures texturales definies.', 1);
    return
end

minJy =  Inf;
maxJy = -Inf;
minJx =  Inf;
maxJx = -Inf;
minKy =  Inf;
maxKy = -Inf;
minKx =  Inf;
maxKx = -Inf;
minD  =  Inf;
maxD  = -Inf;
hAxe = preallocateGraphics(0,0);

nbCliques = size(Texture.cliques,1);
for iClique=1:nbCliques
    str = sprintf('%s - Clique %s', Texture.nomTexture, num2strCode(Texture.cliques(iClique,:)));
    fig = figure('name', str);
    
    nbMasques = length(listeValMasque);
    
    for j=1:nbFaciesPossibles
        CmatModeleAngleA = Texture.CmatModeleAngle{listeValMasque(j)};
        if isempty(CmatModeleAngleA{1})
            continue
        end
        J = CmatModeleAngleA{iClique,1};
        if sum(J(:)) == 0
            continue
        end
        
        for k=j+1:nbFaciesPossibles
            CmatModeleAngleB = Texture.CmatModeleAngle{listeValMasque(k)};
            if isempty(CmatModeleAngleB{1})
                continue
            end
            K = CmatModeleAngleB{iClique,1};
            if sum(K(:)) == 0
                continue
            end
            
            iSubplot = (j-1)*(nbFaciesPossibles-1)+(k-1);
            set(0,'CurrentFigure', fig);
            hAxe(end+1) = subplot(nbMasques-1, nbMasques-1, iSubplot); %#ok
            
            
            [Jy, Jx] = find(J >= (0.0001*max(J(:))));
            minJy = min(minJy, min(Jy));
            maxJy = max(maxJy, max(Jy));
            minJx = min(minJx, min(Jx));
            maxJx = max(maxJx, max(Jx));
            
            [Ky, Kx] = find(K >= (0.0001*max(K(:))));
            minKy = min(minKy, min(Ky));
            maxKy = max(maxKy, max(Ky));
            minKx = min(minKx, min(Kx));
            maxKx = max(maxKx, max(Kx));
            
            D = abs(J-K);
            minD = min(minD, min(D(:)));
            maxD = max(maxD, max(D(:)));
            imagesc(D); axis xy; axis off;
            
            S = sum(D(:));
            str = sprintf('D(%d,%d)=%f', j,k,S);
            title(str);
            axis on; grid on;
        end
    end
    %     iSubplot = 1+(nbFaciesPossibles-1)*(nbFaciesPossibles-2);
    %     subplot(nbMasques-1, nbMasques-1, iSubplot);
    %     colorbar
end
if ~isempty(hAxe)
    for i=1:length(hAxe)
        set(hAxe(i), 'YLim', [min(minJy,minKy) max(maxJy,maxKy)]);
        set(hAxe(i), 'XLim', [min(minJx,minKx) max(maxJx,maxKx)]);
        set(hAxe(i), 'CLim', [minD maxD])
    end
    linkaxes(hAxe)
end
