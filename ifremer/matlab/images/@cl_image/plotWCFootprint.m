% Plot of the WC footprint and the WC image combined to a background image
% and a navigation file from another vehicule (Ex Nautile).
% This is a demonstrater before it is implemented in GLOBE
%
% Syntax
%   plotWCFootprint(this, FileNameXML, ...)
%
% Input Arguments
%   a           : One cl_image instance used for the background
%   FileNameXML : Name of the Water Column XML file (geometry DepthAcrossDist)
%
% Name-Value Pair Arguments
%   FileNameNav : File name of a navigation file (.nvi, ...)
%
% Remarks : if a is empty, no background image is displayed
%
% Examples
%   I0 = cl_image_I0;
%   FileNameXML = 'D:\Temp\Herve\Nautile\WC\0042_20131013_082628_AtalanteEM710_Raw_PolarEchograms.xml';
%   FileNameNav = 'D:\Temp\Herve\Nautile\20131013_nautile.nvi';
%   plotWCFootprint(I0, FileNameXML)
%   plotWCFootprint(I0, FileNameXML, 'FileNameNav', FileNameNav)
%
%   FileNameErs = 'D:\Temp\Herve\Nautile\0042_20131013_082628_AtalanteEM710_LatLong_SunColor.ers';
%   [flag, a] = cl_image.import_ermapper(FileNameErs);
%   plotWCFootprint(a, FileNameXML, 'FileNameNav', FileNameNav)
%
% See also lecFicNav XMLBinUtils.readGrpData Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function plotWCFootprint(this, FileNameXML, varargin)

[varargin, FileNameNav] = getPropertyValue(varargin, 'FileNameNav', []); %#ok<ASGLU>

%% Contr�le d'existence des fichiers

if iscell(FileNameXML)
    FileNameXML = FileNameXML{1};
end
if ~exist(FileNameXML, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', FileNameXML);
    str2 = sprintf('"%s" does not exist.', FileNameXML);
    my_warndlg(Lang(str1,str2), 1);
    return
end

if iscell(FileNameNav)
    FileNameNav = FileNameNav{1};
end
if ~isempty(FileNameNav)
    if ~exist(FileNameNav, 'file')
        str1 = sprintf('Le fichier "%s" n''existe pas.', FileNameNav);
        str2 = sprintf('"%s" does not exist.', FileNameNav);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    %% Lecture fichier de nav
    
    [flag, LatNvi, LonNvi, Tnvi] = lecFicNav(FileNameNav);
    if ~flag
        return
    end
    Tnvi = Tnvi.timeMat;
end

%% Lecture fichier WC

[flag, Data] = XMLBinUtils.readGrpData(FileNameXML, 'Memmapfile', 1);
if ~flag
    return
end

if ~isfield(Data, 'DataTag') || ~strcmp(Data.DataTag, 'WaterColumnImages')
    str1 = sprintf('"%s" n''est pas de type "WaterColumnImages".', FileNameXML);
    str2 = sprintf('"%s" is not a "WaterColumnImages" one.', FileNameXML);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Trac� graphique

figure;
% hp1 = uipanel('Position',[.05 .18 .42 .8]);
% hp2 = uipanel('Position',[.53 .18 .42 .8]);
% hp3 = uipanel('Position',[.05 .05 .9 .1]);
hp1 = uipanel('Position',[.01 .12 .49 .87]);
hp2 = uipanel('Position',[.50 .12 .49 .87]);
hp3 = uipanel('Position',[.01 .01 .98 .1]);
uicontrol(hp3, 'Style', 'Slider', 'Min', 1, 'Max', Data.Dimensions.nbPings, 'Value', 1, ...
    'SliderStep', [1/Data.Dimensions.nbPings 10/Data.Dimensions.nbPings], ...
    'Units', 'normalized', 'Position', [0 0 1 1], 'Callback', @slider1_Callback);

XData = [Data.lonBab(1,:) Data.lonTri(1,:)];
YData = [Data.latBab(1,:) Data.latTri(1,:)];
TData = cl_time('timeIfr', Data.Date(:), Data.Hour(:));
TData = TData.timeMat;

Axe = axes('parent', hp1);
if ~isempty(this) && (this.GeometryType == cl_image.indGeometryType('LatLong'))
    imagesc(this, 'Axe', Axe); hold on;
end
if ~isempty(FileNameNav)
    plot(LonNvi, LatNvi); grid on; hold on;
    
    kNav = indexNav(TData(1), Tnvi, Data.iPing(1), Data.iPing(end));
    if isempty(kNav)
        hMarker = plot(LonNvi(1), LatNvi(1), 'or'); % TODO : que faire ?
        set(hMarker, 'Visible', 'Off');
    else
        hMarker = plot(LonNvi(kNav), LatNvi(kNav), 'or');
    end
    set(hMarker, 'MarkerFaceColor', [1 0 0])
end

hWCFootprint = plot(XData, YData, 'k');
Titre = [num2str(Data.iPing(1)) ' : '  t2str(cl_time('timeMat', TData(1)))];
hWCFootprintTitle = title(Titre);

[flag, WC, map] = importWCDepthAcrossDist(FileNameXML, Data.iPing(1));
axes('parent', hp2);
x = [Data.xBab(1) Data.xTri(1)];
y = [Data.Depth(1) Data.Immersion(1)];
hWCImage = image(x, y, flipud(WC)); colormap(map); axis xy; colorbar; axis tight
hWCImageTitle = title(Titre);

%% Callback

    function slider1_Callback(hs, eventdata, handles) %#ok<INUSD>
        k = floor(get(hs, 'Value'));
        XData = [Data.lonBab(k,:) Data.lonTri(k,:)];
        YData = [Data.latBab(k,:) Data.latTri(k,:)];
        set(hWCFootprint, 'XData', XData, 'YData', YData)
        
        if ~isempty(FileNameNav)
            kNav = indexNav(TData(k), Tnvi, Data.iPing(1), Data.iPing(end));
            if isempty(kNav)
                set(hMarker, 'Visible', 'Off');
            else
                XData = LonNvi(kNav);
                YData = LatNvi(kNav);
                set(hMarker, 'XData', XData, 'YData', YData)
                set(hMarker, 'Visible', 'On');
            end
        end
        
        Titre = [num2str(Data.iPing(k)) ' : ' t2str(cl_time('timeMat', TData(k)))];
        set(hWCFootprintTitle, 'String', Titre)
        
        [flag, WC, map] = importWCDepthAcrossDist(FileNameXML, Data.iPing(k));
        x = [Data.xBab(k) Data.xTri(k)];
        y = [Data.Depth(k) Data.Immersion(k)];
        set(hWCImage, 'XData', x, 'YData', y, 'CData', flipud(WC))
        axis tight
        set(hWCImageTitle, 'String', Titre)
    end

end

function kNav = indexNav(TData, Tnvi, iPingDeb, iPingFin)
% figure; plot(abs(Tnvi - TData)); grid on
[~, kNav] = min(abs(Tnvi - TData));
if (kNav == iPingDeb) || (kNav == iPingFin)
    kNav = [];
end
end

% Proceed with callback..
%{
Software: 'SonarScope'
DataTag: 'WaterColumnImages'
Name: 'Raw-0047_20131013_105628_AtalanteEM710'
ExtractedFrom: 'D:\SONARSCOPE\0047_20131013_105628_AtalanteEM710.all'
nomDirExport: 'D:\NAVIRES\ATALANTE\OCT2013'
FormatVersion: 20100201
Survey: 'Unkown'
Vessel: 'Unkown'
Sounder: 'Unkown'
ChiefScientist: 'Unkown'
Dimensions: [1x1 struct]
Signals: [1x17 struct]
Date: [51x1 cl_memmapfile]
Hour: [51x1 cl_memmapfile]
iPing: [51x1 cl_memmapfile]
xBab: [51x1 cl_memmapfile]
xTri: [51x1 cl_memmapfile]
Depth: [51x1 cl_memmapfile]
Immersion: [51x1 cl_memmapfile]
lonBab: [51x1 cl_memmapfile]
latBab: [51x1 cl_memmapfile]
lonCentre: [51x1 cl_memmapfile]
latCentre: [51x1 cl_memmapfile]
lonTri: [51x1 cl_memmapfile]
latTri: [51x1 cl_memmapfile]
lonBabDepointe: [51x1 cl_memmapfile]
latBabDepointe: [51x1 cl_memmapfile]
lonTriDepointe: [51x1 cl_memmapfile]
latTriDepointe: [51x1 cl_memmapfile]
%}
