% Affichage de l'aire insonifiee d'un �chantillon sonar
% 
% Syntax
%   plot_aire_insonifiee_Image(this)
%
% Input Arguments
%   this    : Instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_aire_insonifiee_Image(this, indImage, cross, NbVoisins)

[flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarPlotResol(this, indImage);
if ~flag
    return
end

if (CasDistance == 1) || (CasDepth ~= 4)
    x = cross(1);
    y = cross(2);
    [ix, iy] = xy2ij(this(indImage), x, y);
    NbVoisins = max(1, NbVoisins);
    subx = (ix-NbVoisins):(ix+NbVoisins);
    suby = (iy-NbVoisins):(iy+NbVoisins);
    nbColumns = this(indImage).nbColumns;
    nbRows     = this(indImage).nbRows;
    subx((subx < 1) | (subx > nbColumns)) = [];
    suby((suby < 1) | (suby > nbRows)) = [];
    [subx, suby, AngleEmission, D, H] = sonar_angle_nadir(this(indImage), CasDepth, H, CasDistance, this(indLayerBathy), this(indLayerEmission), ...
        'subx', subx, 'suby', suby); %#ok<ASGLU> % TODO : voir si il faut faire , 'FlagRoll', '0'
    sonar_plot_aire_Image(this(indImage), subx, suby, AngleEmission, D);
end


function [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarPlotResol(this, indImage)

flag = 1;
%     CasDistance   = 4;
indLayerBathy = [];
H             = [];
CasDepth      = [];

%% Recherche du layer : angle d'emission

identEmission    = cl_image.indDataType('TxAngle');
indLayerEmission = findIndLayerSonar(this, indImage, 'DataType', identEmission, 'WithCurrentImage', 1);

%% Recherche de l'information de distance parcourue

if is_PingSamples(this(indImage))
    CasDistance = 1;    % Cas d'une donn�e sonar en distance oblique
    return
else
    if is_PingAcrossDist(this(indImage))
        CasDistance = 2;    % Cas d'une donn�e sonar en distance projet�e
    else
        if isempty(indLayerEmission)
            flag = 0;
            CasDistance = 4; % On ne possede pas d'information
        else
            CasDistance = 3; % On possede l'angle d'emission
        end
    end
end

%% Recherche de l'information de hauteur

identBathymetry = cl_image.indDataType('Bathymetry');
indLayerBathy   = findIndLayerSonar(this, indImage, 'DataType', identBathymetry, 'WithCurrentImage', 1);
if isempty(indLayerBathy)
    % Il n'existe pas de layer "Depth"
    if get(this(indImage), 'flagHeight')
        if isempty(indLayerEmission)
            CasDepth = 2;    %  Il existe une hauteur sonar
        else
            CasDepth = 3;    % On possede l'angle d'emission
        end
    else
        % Il n'existe pas de hauteur
        CasDepth = 4;
    end
else
    % Il existe un layer 'Depth'
    CasDepth = 1;
end
