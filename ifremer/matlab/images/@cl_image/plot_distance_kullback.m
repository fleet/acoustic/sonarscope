%   subx  : subsampling in X
%   suby  : subsampling in Y

function plot_distance_kullback(this, binsImage, ...
    subDep, subDir, LayerMask, valMask, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

NbNiveaux = binsImage.NbNiveaux;

% -------------------------
% Quantification de l'image

I = this.Image(suby, subx);
rangeIn  = binsImage.CLim;
N        = binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(I, 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

% ----------------------------------
% Calcul des matrices de cooccurence

color = 'brkmgybrkmgybrkmgybrkmgybrkmgy';
Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
for i=1:length(valMask)
    subNonMasque = (Masque ~= valMask(i));
    J = I;
    J(subNonMasque) = NaN;
    
    Label1 = sprintf('ValMask=%d', valMask(i));
    Style = cl_style('Color', color(i));
    a(i) = cl_cooc('Image', J, 'Label', Label1, ...
        'Deplacements', subDep, 'Directions', subDir, 'ListeParams', [], ...
        'Style', Style); %#ok
    
    %     Style = cl_style('Color', color(i), 'LineStyle', 'none', 'Marker', '+');
    %     a(i,2) = cl_cooc( 'Image', J, 'Label', Label1, ...
    %         'Deplacements', subDep, 'Directions', subDir, 'ListeParams', [], ...
    %         'WindowAnalyse', W, 'Style', Style);
end

[Tx, Ty, TxEntiers, TyEntiers] = calcul_deplacement_directions(a(1)); %#ok

Deplacements   = get(a(1), 'Deplacements');
Directions     = get(a(1), 'Directions');
nbDirections   = length(Directions);
nbDeplacements = length(Deplacements);
nbFacies = length(a);
iClique = 0;
hAxe = preallocateGraphics(0,0);
CLim = [Inf -Inf];
fig = 9999;
dist = Inf(nbFacies, nbFacies, nbDirections*nbDeplacements);
for iDirection=1:nbDirections
    for iDeplacement=1:nbDeplacements
        %         dist = Inf(nbFacies, nbFacies, nbDirections*nbDeplacements);
        iClique = iClique + 1;
        for iFacies=1:nbFacies
            ImageModel = get(a(iFacies), 'Image');
            iDir(iClique) = Directions(iDirection); %#ok
            iDep(iClique) = Deplacements(iDeplacement); %#ok
            CModele{iFacies} = coocMat(ImageModel, ...
                Tx{iDirection}(iDeplacement), ...
                Ty{iDirection}(iDeplacement), ...
                'NbNiveaux', NbNiveaux, 'Filtre'); %#ok
            %             figure(9999 + iFacies);
            %             imagesc(CModele{iFacies});
            %             colorbar
        end
        
        fig = fig + 1;
        for iFacies1=1:nbFacies
            for iFacies2=iFacies1+1:nbFacies
                dist(iFacies1, iFacies2, iClique) = kullback(CModele{iFacies1}, CModele{iFacies2});
                
                figure(fig);
                iSubplot = (iFacies1-1)*(nbFacies-1)+(iFacies2-1);
                hAxe(end+1) = subplot(nbFacies-1, nbFacies-1, iSubplot); %#ok<AGROW>
                D = abs(CModele{iFacies1} - CModele{iFacies2});
                CLim(1) = min(CLim(1), min(D(:)));
                CLim(2) = max(CLim(2), max(D(:)));
                imagesc(D);
                str = sprintf('Dir=%d, iDep=%d, Diff(%d,%d)', iDirection, iDeplacement, iFacies1, iFacies2);
                title(str)
            end
        end
        X = dist(:, :, iClique);
        X = X(~isnan(X) & ~isinf(X));
        %         distMin(iClique) = min(X(:));
        %         distMin(iClique) = median(X(:)); %#ok
        distMin(iClique) = std(X(:)); %#ok
    end
end
[distMinClique, Index] = max(distMin);
iDirection = iDir(Index);
iDeplacement = iDep(Index);
dx = Tx{iDirection}(iDeplacement);
dy = Ty{iDirection}(iDeplacement);

str = sprintf('Best clique : [%d %d]', dx, dy);
my_warndlg(str, 1);

set(hAxe, 'CLim', CLim)

figure;
for iClique=1:size(dist,3)
    D = dist(:,:,iClique);
    D = D(:);
    x = repmat(iClique, 1, length(D));
    sub = find(~isinf(D));
    if ~isempty(sub)
        plot(x(sub), D(sub), '*'); hold on
    end
end

%{
% ---------------
% Trac� graphique

fig = figure;
Deplacements = get(a(1), 'Deplacements');
Directions   = get(a(1), 'Directions');
nbDirections   = length(Directions);
nbDeplacements = length(Deplacements);
%     [Tx, Ty, TxEntiers, TyEntiers] = calcul_deplacement_directions(a(k,1));

for k=1:size(a,1)
Image = get(a(k,2), 'Image');
[Tx, Ty, TxEntiers, TyEntiers] = calcul_deplacement_directions(a(k,2)); %#ok
CModele  = coocMat(ImageModel, ...
TxEntiers{iDirection}(iDeplacement), ...
TyEntiers{iDirection}(iDeplacement));

ligDeb = get(a(k,2), 'ligDeb');
ligFin = get(a(k,2), 'ligFin');
colDeb = get(a(k,2), 'colDeb');
colFin = get(a(k,2), 'colFin');
j = 0;
for iL=1:length(ligDeb)
for iC=1:length(colDeb)
j = j+1;
Imagette = Image(ligDeb(iL):ligFin(iL), colDeb(iC):colFin(iC));
Cmat  = coocMat(Imagette, ...
TxEntiers{iDirection}(iDeplacement), ...
TyEntiers{iDirection}(iDeplacement));
if sum(sum(Cmat ~= 0)) <= 50
dist(iFacies, k, j) = NaN;
continue
else
dist(iFacies, k, j) = kullback(Cmat, CModele);
end
end
end
end
end

%         figure(9999)
%         imagesc(dist(1, :, :));

figure(fig)
subplot(nbDirections, 1, iDirection)
hold on;

for k=1:size(a,1)
Y = squeeze(dist(:, k, :));
[distKulMin, segmKul] = min(Y, [], 1);
subNonNaN  = ~isnan(distKulMin(:));
distKulMin = distKulMin(subNonNaN);
segmKul    = segmKul(subNonNaN);

Y = distKulMin;

b = a(k,2);
Label = get(b, 'Label');
Style = get(b, 'Style');

cmenu = uicontextmenu;
h = plot(repmat(Deplacements(iDeplacement)+0.1*k, length(Y), 1), Y(:), 'UIContextMenu', cmenu);
uimenu(cmenu, 'Text', Label);

apply(h, Style)

grid on;
end
%             for iSegm = min(segmKul):max(segmKul)
%                 Y = distKulMin(segmKul == iSegm);
%
%                 b = a(iSegm,2);
%                 Label = get(b, 'Label');
%                 Style = get(b, 'Style');
%
%                 cmenu = uicontextmenu;
%                 h = plot(repmat(Deplacements(iDeplacement), length(Y), 1), Y(:), 'UIContextMenu', cmenu);
%                 uimenu(cmenu, 'Label', Label);
%
%                 apply(h, Style)
%
%                 grid on;
%             end
%         end
end
angle = (iDirection -1) * 45;
WindowAnalyse = get(b, 'WindowAnalyse');
title(sprintf('Direction %d (%d deg) - W=%s', ...
Directions(iDirection), angle, num2strCode(WindowAnalyse)))
end
%}
