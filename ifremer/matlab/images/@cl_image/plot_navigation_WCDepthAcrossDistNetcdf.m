function [Fig, hAxeNav, hAxeRaw] = plot_navigation_WCDepthAcrossDistNetcdf(this, nomFic, varargin)

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
[varargin, Depth]     = getPropertyValue(varargin, 'Depth',     []);
[varargin, Height]    = getPropertyValue(varargin, 'Height',    []);
[varargin, Heading]   = getPropertyValue(varargin, 'Heading',   []);
[varargin, FileNames] = getPropertyValue(varargin, 'FileNames', []); %#ok<ASGLU>

%% Lecture du fichier Netcdf 

[flag, DataEchogram] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFic, 'Light', 2);
if ~flag
    return
end

nbPings = length(DataEchogram.Datetime);
if nbPings == 0
    str1 = 'Pas de donn�es disponibles';
    str2 = 'No data available';
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Open netcdf file

ncID = netcdf.open(nomFic);

%% Test d'existence des fichiers image

Datetime  = DataEchogram.Datetime;
Latitude  = DataEchogram.latitude_nadir;
Longitude = DataEchogram.longitude_nadir;

TagLine = nomFic;

%% Trac� de la navigation

if isempty(Fig)
    Fig = FigUtils.createSScFigure('CloseRequestFcn',@my_closereq);
    hAxeRaw  = subplot(2,2,1);
    hAxeComp = subplot(2,2,3);
    hAxeNav  = subplot(2,2,[2 4]);
    if ~isempty(this)
        imagesc(this, 'Axe', hAxeNav); hold on; % TODO : provoque un bug � plot3
        hcNav = colorbar;
        set(hcNav, 'ButtonDownFcn', {@clbkColorbar; hAxeNav}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
    end
    hImageRaw  = [];
    hImageComp = [];
else
    hAxes = findobj(Fig, 'Type', 'axes');
    hAxeRaw    = hAxes(3);
    hAxeComp   = hAxes(2);
    hAxeNav    = hAxes(1);
    hImageRaw  = [];
    hImageComp = [];
end
nomFicPosition = fullfile(my_tempdir, ['Position' num2str(Fig.Number) '.dat']);

h = findobj(Fig, 'Text', 'Import/Export');
if isempty(h)
    uiSSc = uimenu(Fig, 'Text', 'Import/Export');
    uimenu(uiSSc, 'Text', 'Plot nav in Google Earth (.kmz)', 'Callback', @ExportGoogleEarth);
    uimenu(uiSSc, 'Text', 'Plot nav in Globe (.xml)',        'Callback', @ExportNavigation);
    uimenu(uiSSc, 'Text', 'Change directory of images',      'Callback', @ChangeImageDirectory);
end

Ident = rand(1);

h = findobj(Fig, 'Tag', 'hHeure');
if isempty(h)
    uicontrol('Style', 'text', 'Position', [10,10,1000,20], 'Tag', 'hHeure');
    set(Fig, 'ToolBar', 'figure')
end

set(Fig, 'Tag', 'Trace de navigation', 'UserData', Ident)
hc = findobj(hAxeNav, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(hAxeNav, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes, nbCoul) + 1;


% axes(hAxeNav);
hold(hAxeNav, 'on');
h = plot(hAxeNav, Longitude(:), Latitude(:));
setappdata(h, 'Datetime', Datetime(:));
setappdata(h, 'ncID', ncID);
% set(h, 'Marker', '.')
set(h, 'Color', ColorOrder(iCoul,:))
str1 = 'Position des images (tapez "H" pour obtenir la liste des raccourcis clavier)';
str2 = 'Position of images (press "H" to obtain the list of shortcuts)';
title(Lang(str1,str2))

[nomDirImages, nomFicSeul] = fileparts(nomFic);
str = char([Datetime(1), Datetime(end)]);
str = [str repmat(' ', 2, 2)]';
UserData = [nomFicSeul ' - ' str(:)'];
set(h, 'UserData', UserData)
setappdata(h, 'ncFileName', nomFic);

figure(Fig);

LatLim = [min(Latitude)  max(Latitude)];
LonLim = [min(Longitude) max(Longitude)]; % Ligne de changement d'heure pas g�r�e ici
tLim   = [Datetime(1), Datetime(end)];
Color = get(h, 'Color');

UserData = {Color; 'On'; h; nomFic; LatLim; LonLim; tLim; nomFicPosition};

str = sprintf('display(''%s - %s | %s %s | %s %s'')', ...
    nomFic, str(:), ...
    lat2str(Latitude(1)), ...
    lon2str(Longitude(1)), ...
    lat2str(Latitude(end)), ...
    lon2str(Longitude(end)));
set(h, 'ButtonDownFcn', str)

if ~isempty(TagLine)
    set(h, 'Tag', TagLine)
end

axisGeo(hAxeNav);
grid on;
drawnow

%% D�finition des callbacks associ�es aux objets graphiques

set(Fig, 'WindowButtonMotionFcn', @dispHeure, 'BusyAction', 'cancel', 'Interruptible', 'Off')
set(Fig, 'KeyPressFcn', @PressKey);






%% Inner fonction d'affichage du point de la navigation le plus proche de la souris

    function ExportGoogleEarth(varargin)
        plot_navigation_ExportGoogleEarth('SCAMPI', 1)
    end

% Export de la navigation en XML
    function ExportNavigation(varargin)
        plot_navigation_ExportXML
    end

    function ChangeImageDirectory(varargin)
        [flag, nomDir] = my_uigetdir(nomDirImages, 'Directory to put PingAcrossDist images');
        if flag
            nomDirImages = nomDir;
            hImageRaw  = [];
            hImageComp = [];
        end
    end

    function dispHeure(varargin)
        global hLines XData YData ZData ordre Dist DistMin tMin% Dans le debuggueur uniquement
        
        % How Callbacks Are Interrupted :
        % MATLAB checks for queued events that can interrupt a callback function only when it encounters
        % a call to drawnow, figure, getframe, or pause in the executing callback function. When executing
        % one of these functions, MATLAB processes all pending events, including executing all waiting callback
        % functions. The interrupted callback then resumes execution
        drawnow
        
        GCBA = get(gcbf, 'CurrentAxes');
        
        if GCBA ~= hAxeNav
            return
        end
        
        currentPoint = get(GCBA, 'currentPoint');
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        if (currentPoint(1,1) < XLim(1)) || (currentPoint(1,1) > XLim(2)) || (currentPoint(1,2) < YLim(1)) || (currentPoint(1,2) > YLim(2))
            return
        end
        hLines = findobj(gcbf, 'Type', 'line', 'LineStyle', '-');
        if isempty(hLines)
            return
        end
        for k=1:length(hLines)
            pppp = get(hLines(k), 'Tag');
            if isempty(pppp)
                subIsNav(k) = false; %#ok<AGROW>
            else
                if strcmp(pppp, 'PositionPing')
                    subIsNav(k) = false; %#ok<AGROW>
                else
                    subIsNav(k) = true; %#ok<AGROW>
                end
            end
        end
        hLines = hLines(subIsNav);
        DistMin = [];
        tMin    = datetime.empty;
        kPing   = [];
        hLinesConserve = [];
        for k=1:length(hLines)
            ZData = getappdata(hLines(k), 'Datetime');
            if isempty(ZData)
                continue
            end
            UserData = get(hLines(k), 'UserData');
            if isempty(UserData)
                continue
            end
            
            XData = get(hLines(k), 'XData');
            YData = get(hLines(k), 'YData');
            XDataTemp = abs(XData - currentPoint(1,1));
            YDataTemp = abs(YData - currentPoint(1,2));
            Dist = XDataTemp.^2 + YDataTemp.^2;
            [~, ordreDist] = min(Dist);
            DistMin(end+1) = Dist(ordreDist); %#ok
            tMin(end+1)    = ZData(ordreDist); %#ok
            kPing(end+1)   = ordreDist; %#ok
            hLinesConserve(end+1) = hLines(k); %#ok
        end
        [~, ordre] = min(DistMin);
        if isempty(ordre)
            return
        end
        indXMLFile = ordre(1);
        XData = get(hLines(indXMLFile), 'XData');
        YData = get(hLines(indXMLFile), 'YData');
        tMin  = getappdata(hLines(indXMLFile), 'Datetime');
        kPing = kPing(indXMLFile);
        tMin = tMin(kPing);

        LonMin = XData(kPing);
        LatMin = YData(kPing);
        
        hHeure = findobj(gcbf, 'Tag', 'hHeure');
        UserData = get(hLinesConserve(indXMLFile), 'UserData');
        set(hHeure, 'String', ['Point : ' char(tMin) ' Line : ' UserData])
        set(hHeure, 'HorizontalAlignment', 'left')
        ncFileName = getappdata(hLinesConserve(indXMLFile), 'ncFileName');
        ncFileID   = getappdata(hLinesConserve(indXMLFile), 'ncID');
        
        if length(hLines) > 1
            if ~strcmp(get(hLines(indXMLFile), 'Tag'), 'PositionPing')
                set(hLinesConserve, 'LineWidth', 0.5)
                set(hLinesConserve(indXMLFile), 'LineWidth', 3)
            end
        end
        dispImageWC(GCBA, kPing, 2, LatMin, LonMin, ncFileName, ncFileID)
    end


    function PressKey(Fig0, varargin)
        CurrentCharacter = get(Fig0, 'CurrentCharacter');
        if isempty(CurrentCharacter)
            return
        end
        
        switch lower(CurrentCharacter)
            case 'h'
                str = {};
                str{end+1} = Lang('m  : Cr�er un marqueur', 'm  : Create a marker');
                str{end+1} = Lang('o  : Export des marqueurs (.txt)', 'o  : Export markers (.txt)');
                str{end+1} = Lang('i  : Import des marqueurs (.txt)', 'i  : Import markers (.txt)');
                str{end+1} = Lang('s  : Cr�ation des images correspondant aux marqueurs (.txt)', 's  : Create images corresponding to the markers (.txt)');
                str{end+1} = '+  : Zoom * 2';
                str{end+1} = '-  : Zoom / 2';
                str{end+1} = Lang('-> : 1/2 d�calage vers la droite', '-> : 1/2 Shift right');
                str{end+1} = Lang('<- : 1/2 d�calage vers la gauche', '<- : 1/2 Shift left');
                str{end+1} = Lang('   : 1/2 d�calage vers le haut',   '   : 1/2 Shift top');
                str{end+1} = Lang('   : 1/2 d�calage vers le bas',    '   : 1/2 Shift down');
                edit_infoPixel(str, [], 'PromptString', 'SonarScope Shortcuts on local image.')
            case 'm'
                PlotMarker(varargin{:})
            case 'o'
                plot_navigation_ExportMarkers_OTUS(Fig0, hAxeNav)
                
            case 's'
                displayImages
                
            case 'i'
                plot_navigation_ImportMarkers_OTUS(hAxeNav)
                
            case '+'
                zoom(hAxeNav, 2)
            case '-'
                zoom(hAxeNav, 1/2)
                
            otherwise
                if (double(CurrentCharacter) == 28)
                    % D�placement vers la gauche
                    XLim  = get(hAxeNav, 'XLim');
                    Range = diff(XLim);
                    set(hAxeNav, 'XLim', XLim - Range/2)
                elseif (double(CurrentCharacter) == 29)
                    % D�placement vers la droite
                    XLim  = get(hAxeNav, 'XLim');
                    Range = diff(XLim);
                    set(hAxeNav, 'XLim', XLim + Range/2)
                    
                elseif (double(CurrentCharacter) == 30)
                    % D�placement vers le haut
                    YLim  = get(hAxeNav, 'YLim');
                    Range = diff(YLim);
                    set(hAxeNav, 'YLim', YLim + Range/2)
                    
                elseif (double(CurrentCharacter) == 31)
                    % D�placement vers le bas
                    YLim  = get(hAxeNav, 'YLim');
                    Range = diff(YLim);
                    set(hAxeNav, 'YLim', YLim - Range/2)
                end
        end
        axis(hAxeNav);
    end

    function PlotMarker(varargin)
        global hLines XData YData ZData ordre Dist DistMin tMin% Dans le debuggueur uniquement
        
        set(Fig, 'WindowButtonMotionFcn',[])
        
        GCBO = gcbo;
        GCBA = get(gcbf, 'CurrentAxes');
        XLim = get(GCBA, 'XLim');
        YLim = get(GCBA, 'YLim');
        currentPoint = get(GCBA, 'currentPoint');
        hLines = findobj(gcbf, 'Type', 'line');
        for k=1:length(hLines)
            pppp = get(hLines(k), 'Tag');
            if isempty(pppp)
                subIsNav(k) = false; %#ok<AGROW>
            else
                subIsNav(k) = true; %#ok<AGROW>
            end
        end
        hLines = hLines(subIsNav);
        DistMin = [];
        tMin    = [];
        XDataLine = [];
        YDataLine = [];
        hLinesConserve    = [];
        DepthConserve     = [];
        HeightConserve    = [];
        HeadingConserve   = [];
        FileNamesConserve = {};
        
        for k=1:length(hLines)
            ZData = getappdata(hLines(k), 'Datetime');
            if isempty(ZData)
                continue
            end
            UserData = get(hLines(k), 'UserData');
            if isempty(UserData)
                continue
            end
            
            XData = get(hLines(k), 'XData');
            YData = get(hLines(k), 'YData');
            XDataDist = abs(XData-currentPoint(1,1));
            YDataDist = abs(YData-currentPoint(1,2));
            Dist = XDataDist.^2 + YDataDist.^2;
            [~, ordre] = sort(Dist);
            DistMin(end+1) = Dist(indXMLFile); %#ok
            XDataLine(end+1) = XData(indXMLFile); %#ok<AGROW>
            YDataLine(end+1) = YData(indXMLFile); %#ok<AGROW>
            tMin(end+1) = ZData(indXMLFile); %#ok
            hLinesConserve(end+1) = hLines(k); %#ok
            
            if isempty(Depth)
                DepthConserve(end+1) = NaN; %#ok<AGROW>
            else
                DepthConserve(end+1) = Depth(indXMLFile); %#ok<AGROW>
            end
            if isempty(Height)
                HeightConserve(end+1) = NaN; %#ok<AGROW>
            else
                HeightConserve(end+1) = Height(indXMLFile); %#ok<AGROW>
            end
            if isempty(Heading)
                HeadingConserve(end+1) = NaN; %#ok<AGROW>
            else
                HeadingConserve(end+1) = Heading(indXMLFile); %#ok<AGROW>
            end
            if isempty(FileNames)
                FileNamesConserve{end+1} = ' '; %#ok<AGROW>
            else
                FileNamesConserve{end+1} = FileNames(indXMLFile); %#ok<AGROW>
            end
        end
        [~, ordre] = sort(DistMin);
        if isempty(ordre)
            return
        end
        tMin = tMin(indXMLFile);
        LonMin = XDataLine(indXMLFile);
        LatMin = YDataLine(indXMLFile);
        
        UserData = get(GCBO, 'UserData');
        
        % {
        flagHold = ishold;
        if ~flagHold
            hold on
        end
        hPoint = PlotUtils.createSScPlot(XDataLine(indXMLFile), YDataLine(indXMLFile), 'or');
        UserDataMarker = sprintf('%s', char(tMin));
        set(hPoint, 'MarkerSize', 10, 'Tag', 'PointRemarquable', 'MarkerFaceColor', [1 0 0], 'UserData', UserDataMarker)
        if ~flagHold
            hold off
        end
        set(GCBA, 'XLim', XLim);
        set(GCBA, 'YLim', YLim);
        
        if isempty(Depth)
            setappdata(hPoint, 'Depth', NaN);
        else
            setappdata(hPoint, 'Depth', DepthConserve(indXMLFile))
        end
        setappdata(hPoint, 'Height',    HeightConserve(indXMLFile))
        setappdata(hPoint, 'Heading',   HeadingConserve(indXMLFile))
        setappdata(hPoint, 'FileNames', FileNamesConserve{indXMLFile})
        setappdata(hPoint, 'Latitude',  LatMin)
        setappdata(hPoint, 'Longitude', LonMin)
        setappdata(hPoint, 'TimeMat',   tMin)
        
        str1 = 'Commentaire : ';
        str2 = 'Comment : ';
        nomVar = {Lang(str1,str2)};
        value = {''};
        [Comment, flag] = my_inputdlg(nomVar, value);
        if flag
            Comment = Comment{1};
        end
        setappdata(hPoint, 'Comment', Comment)
        ncFileName = getappdata(indXMLFile, 'ncFileName');
        ncFileID   = getappdata(hLinesConserve(indXMLFile), 'ncID');
        
        dispImageWC(GCBA, kPing, 1, LatMin, LonMin, ncFileName, ncFileID)
        set(Fig, 'WindowButtonMotionFcn', @dispHeure, 'BusyAction', 'cancel', 'Interruptible', 'Off')
        axis(hAxeNav);
    end

    function PrintLine(str, varargin) %#ok
        display(str)
    end

    function plotImageLocalization(GCBA, LatMin, LonMin)
        if ~isnan(LatMin)
            plotCross(GCBA, LatMin, LonMin, 2)
        end
    end


    function plotCross(GCBA, Lat, Lon, Type)
        hCross = findobj(GCBA, 'Type', 'line', 'Tag', 'PositionPing');
        if isempty(hCross)
            if Type == 1
                XLim = get(GCBA, 'XLim');
                YLim = get(GCBA, 'YLim');
                hold(GCBA, 'on');
                hc = plot(GCBA, [Lon Lon], YLim, 'r');
                set(hc, 'Tag', 'PositionPing')
                hc = plot(GCBA, XLim, [Lat Lat], 'r');
                set(hc, 'Tag', 'PositionPing')
                hold(GCBA, 'off');
            else
                hold(GCBA, 'on')
                hc = plot(GCBA, Lon, Lat, '.');
                set(hc, 'Color', 'r', 'Tag', 'PositionPing', 'MarkerSize', 20)
                hold(GCBA, 'off')
            end
        else
            if Type == 1
                set(hCross(1), 'YData', [Lat Lat])
                set(hCross(2), 'XData', [Lon Lon])
            else
                set(hCross(1), 'XData', Lon, 'YData', Lat)
            end
        end
    end


    function dispImageWC(GCBA, kPing, Mode, LatMin, LonMin, ncFileName, ncFileID, varargin)
        
        [varargin, Title] = getPropertyValue(varargin, 'Title', []); %#ok<ASGLU>
        
        pppp = zoom(hAxeNav);
        if strcmpi(get(pppp, 'Enable'), 'on')
            return
        end
        
        pppp = pan(Fig);
        if strcmpi(get(pppp, 'Enable'), 'on')
            return
        end
        
        [flag, Data, groupName, sliceNames, layerUnits, MaskSignification] = NetcdfGlobe3DUtils.readOneGroup(ncFileID, kPing);
        if ~flag
            return
        end
                
        IRaw  = Data.raw;
        IComp = Data.comp;
        
        if Mode == 1
            axes(hAxeNav)
            imagesc(IRaw); axis ij; axis square; colormap(gca, 'jet');
            hcNav = colorbar;
            set(hcNav, 'ButtonDownFcn', {@clbkColorbar; hAxeNav}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
            if isempty(Title)
                Title = nomFic;
            else
                Title = [nomFic ' : ' Title];
            end
            title(Title, 'Interpreter', 'none');
        else
            ind = strfind(nomFic, '\');
            strTitle = sprintf('%s  -  Ping %d / %d', nomFic((ind(end)+1):end), kPing, nbPings);
            
            if isempty(hAxeRaw) || ~ishandle(hAxeRaw) || isempty(hImageRaw) || ~ishandle(hImageRaw) || isempty(get(hImageRaw, 'CData'))
                axes(hAxeRaw)
                hImageRaw = imagesc(hAxeRaw, IRaw); axis equal; axis tight; axis xy; colormap(gca, 'jet');
                hcRaw = colorbar; %('SouthOutside');
                set(hcRaw, 'ButtonDownFcn', {@clbkColorbar; hAxeRaw}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
            else
                set(hImageRaw, 'CData', IRaw);
                %                     set(hAxeRaw, 'CLim', CLim)
            end
            title(hAxeRaw, strTitle, 'Interpreter', 'none');
            
            if isempty(hAxeComp) || ~ishandle(hAxeComp) || isempty(hImageComp) || ~ishandle(hImageComp) || isempty(get(hImageComp, 'CData'))
                axes(hAxeComp)
                hImageComp = imagesc(hAxeComp, IComp); axis equal; axis tight; axis xy; colormap(gca, 'jet');
                hcComp = colorbar; %('SouthOutside');
                set(hcComp, 'ButtonDownFcn', {@clbkColorbar; hAxeComp}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
            else
                set(hImageComp, 'CData', IComp);
                %                     set(hAxeComp, 'CLim', CLim)
            end
            title(hAxeComp, strTitle, 'Interpreter', 'none');

            plotImageLocalization(GCBA, LatMin, LonMin)
        end
        %             axes(hAxeRaw)
        axes(hAxeNav);
    end

    function displayImages(varargin)
        h1 = findobj(Fig, 'Tag', 'PointRemarquable');
        
        %% Cadre de recherche
        
        XLim = get(hAxeNav, 'XLim');
        YLim = get(hAxeNav, 'YLim');
        
        str1 = 'Recherche des points dans :';
        str2 = 'Save points from :';
        str3 = 'Graphique entier';
        str4 = 'Whole frame';
        str5 = 'Zoom actuel';
        str6 = 'Current extent';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            return
        end
        if rep == 1
            XLim = [-Inf Inf];
            YLim = [-Inf Inf];
        end
        
        %% Recherche des points
        
        for k=1:length(h1)
            XData = get(h1(k), 'XData');
            YData = get(h1(k), 'YData');
            if any((XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2)))
                UserData = get(h1(k), 'UserData');
                
                DepthImage     = getappdata(h1(k), 'Depth');
                HeightImage    = getappdata(h1(k), 'Height');
                HeadingImage   = getappdata(h1(k), 'Heading');
%               FileNamesImage = getappdata(h1(k), 'FileNames');
                CommentImage   = getappdata(h1(k), 'Comment');
                LatitudeImage  = getappdata(h1(k), 'Latitude');
                LongitudeImage = getappdata(h1(k), 'Longitude');
%               tMatImage      = getappdata(h1(k), 'TimeMat');
                ncFileName     = getappdata(h1(k), 'ncFileName');
                ncFileID       = getappdata(hLinesConserve(indXMLFile), 'ncID');
                
                if iscell(CommentImage)
                    CommentImage = '';
                end
                Title = sprintf('Lon=%s Lat=%s Z=%f H=%f Az=%f %s', num2strPrecis(LongitudeImage), num2strPrecis(LatitudeImage), ...
                    DepthImage, HeightImage, HeadingImage, CommentImage);
                dispImageWC(hAxeNav, kPing, 1, LatitudeImage, LongitudeImage, ncFileName, ncFileID, 'Title', Title)
            end
        end
    end

    function my_closereq(src, callbackdata) %#ok<INUSD>
        
        hLines = findobj(gcbf, 'Type', 'line', 'LineStyle', '-');
        if isempty(hLines)
            return
        end
        for k=1:length(hLines)
            pppp = get(hLines(k), 'Tag');
            if isempty(pppp)
                subIsNav(k) = false; %#ok<AGROW>
            else
                if strcmp(pppp, 'PositionPing')
                    subIsNav(k) = false; %#ok<AGROW>
                else
                    subIsNav(k) = true; %#ok<AGROW>
                end
            end
        end
        hLines = hLines(subIsNav);
        for k=1:length(hLines)
            ncIDLine = getappdata(hLines(k), 'ncID');
            netcdf.close(ncIDLine);
        end
        delete(src)
    end


    function clbkColorbar(~, ~, hAxe, varargin)
        hImcontrast = my_imcontrast(hAxe); % TODO : le Adjust ne fonctionne pas
        waitfor(hImcontrast)
    end
end
