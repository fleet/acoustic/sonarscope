% Affichage d'une navigation Caraibes(R).nvi
%
% Syntax
%   b = plot_nvi(a, nomFicNvi)
%
% Input Arguments
%   a         : Instance de cl_image
%   nomFicNvi : Nom du fichier de navigation
%
% Output Arguments
%   b : Instance de cl_image
%
% Examples
%   c = plot_nvi(a, 'I:\IfremerToolboxDataPublicSonar\EM300_Ex1.nvi')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = plot_nvi(this, nomFicNvi, varargin)

[varargin, nomSignal] = getPropertyValue(varargin, 'Signal', []); %#ok<ASGLU>

%% D�tection du type de fichier

Fig1 = [];

XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
NbFic = length(nomFicNvi);
str1 = 'Trac� de la navigation des .nvi';
str2 = 'Plot navigation of .nvi files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    [flag, Lat, Lon, Time, Heading, Speed, Immesrsion, Altitude, Type] = lecFicNav(nomFicNvi{k});
    if ~flag
        messageErreurFichier(nomFicNvi{k}, 'ReadFailure');
        continue
    end
    
    Fig1 = plot_navigation(nomFicNvi{k}, Fig1, Lon, Lat, Time.timeMat, Heading);
    
    if ~isempty(nomSignal)
        switch nomSignal
            case 'Nav Heading'
                %                 Heading = calCapFromLatLon(Lat, Lon, 'Time', Time);
                [Heading, ~] = legs(Lat, Lon, 'gc'); % Attention : laisser le ~ici car sinon Heading est une matrice
                %                 nomSignal = 'Heading';
                Signal = Heading;
                Unit = 'deg';
            case 'Nav Speed'
                [~, dist] = legs(Lat, Lon, 'gc');
                Deltat = diff(Time.timeMat) * (24*3600);
                dist = distdim(dist,'nm','m');
                Speed = dist ./ Deltat';
                %                 FigUtils.createSScFigure; PlotUtils.createSScPlot(Speed); grid;
                %                 nomSignal = 'Speed';
                Signal = Speed;
                Unit = 'm/s';
            case 'Heading'
                Signal = Heading;
                Unit = 'deg';
            case 'Speed'
                Signal = Speed;
                Unit = 'm/s';
            case 'Immesrsion'
                Signal = Immesrsion;
                Unit = 'm';
            case 'Altitude'
                Signal = Altitude;
                Unit = 'm';
            case 'Type'
                Signal = Type;
                Unit = ' ';
        end
        
        switch nomSignal
            case {'Heading'; 'Nav Heading'}
                ZLim = [0 360];
            case 'Type' % 'PFlag'
                % ZLim = [0 2];
                ZLim = [0 2]; % Quelle valeur pour Type ???
            otherwise
                ZLim(1) = min(ZLim(1), double(min(Signal, [], 'omitnan')));
                ZLim(2) = max(ZLim(2), double(max(Signal, [], 'omitnan')));
        end
        XLim(1) = min(XLim(1), min(Lon, [], 'omitnan'));
        XLim(2) = max(XLim(2), max(Lon, [], 'omitnan'));
        YLim(1) = min(YLim(1), min(Lat, [], 'omitnan'));
        YLim(2) = max(YLim(2), max(Lat, [], 'omitnan'));
    end
    
end
my_close(hw, 'MsgEnd')

if isempty(nomSignal)
    return
end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Unit, 'SignalName', nomSignal);
if ~flag
    return
end

%% Trac� du signal

if isinf(ZLim(1))
    return
end

nbColors = 64;
Colors = jet(nbColors);


figure(Fig1);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

hw = create_waitbar('Plot .nvi files', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    isCaraibes = is_CDF(nomFicNvi{k});
    if isCaraibes
        [flag, Lat, Lon, Time, Heading, Speed, Immesrsion, Altitude, Type] = lecFicNav(nomFicNvi{k});
        if ~flag
            return
        end
    else
        % TODO'
    end
    
    switch nomSignal
        case 'Nav Heading'
            [Heading, ~] = legs(Lat, Lon, 'gc'); % Attention : laisser le ~ici car sinon Heading est une matrice
            nomSignal = 'Heading';
            Signal = [Heading(1); Heading];
            Unit = 'deg';
        case 'Nav Speed'
            [~, dist] = legs(Lat, Lon, 'gc');
            Deltat = diff(Time.timeMat) * (24*3600);
            dist = distdim(dist,'nm','m');
            Speed = dist ./ Deltat';
            %                 FigUtils.createSScFigure; PlotUtils.createSScPlot(Speed); grid;
            nomSignal = 'Speed';
            Signal = [Speed(1); Speed];
            Unit = 'm/s';
        case 'Heading'
            Signal = Heading;
            Unit = 'deg';
        case 'Speed'
            Signal = Speed;
            Unit = 'm/s';
        case 'Immesrsion'
            Signal = Immesrsion;
            Unit = 'm';
        case 'Altitude'
            Signal = Altitude;
            Unit = 'm';
        case 'Type' % 'PFlag'
            %             Signal = PFlag;
            Signal = Type;
            Unit = ' ';
    end
    
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((Signal - ZLim(1)) * coef);
    
    figure(Fig1)
    hold on;
    
    [~, nomFic] = fileparts(nomFicNvi{k});
    for i=1:nbColors
        sub = find(Value == (i-1));
        if ~isempty(sub)
            h = plot(Lon(sub), Lat(sub), '+');
            set(h, 'Color', Colors(i,:), 'Tag', nomFic)
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(Lon(sub), Lat(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(Lon(sub), Lat(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
    end
end
my_close(hw, 'MsgEnd');

if isempty(Unit)
    FigName = sprintf('Navigation - %s', nomSignal);
else
    FigName = sprintf('Navigation - %s (%s)', nomSignal, Unit);
end
title(FigName)

if isempty(Unit)
    FigName = sprintf('Caraibes .nvi plot - %s', nomSignal);
else
    FigName = sprintf('Caraibes .nvi plot - %s (%s)', nomSignal, Unit);
end
figure(Fig1);
set(Fig1, 'name', FigName)
