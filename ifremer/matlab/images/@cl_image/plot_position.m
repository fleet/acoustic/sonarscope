function Fig = plot_position(this, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

NbFic = length(this);
str1 = 'Trac� de la navigation';
str2 = 'Ploting the navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    flag = testSignature(this(k), 'GeometryType', 'PingXxxx');
    if ~flag
        continue
    end
    
    Longitude = this(k).Sonar.FishLongitude;
    Latitude  = this(k).Sonar.FishLatitude;
    subOK = (Latitude ~= 0) & (Longitude ~= 0);
    
    T = this(k).Sonar.Time.timeMat;
    Heading = this(k).Sonar.Heading(subOK);
    nomFic  = this(k).InitialFileName;
    
    plot_navigation(nomFic, Fig, Longitude(subOK), Latitude(subOK), T(subOK), Heading);
end
my_close(hw, 'MsgEnd');
