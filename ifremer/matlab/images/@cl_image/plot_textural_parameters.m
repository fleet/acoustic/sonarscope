%   subx  : subsampling in X
%   suby  : subsampling in Y

function flag = plot_textural_parameters(this, binsImage, ...
    subDep, subDir, subPar, W, LayerMask, valMask, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Quantification de l'image

I = this.Image(suby, subx);
rangeIn  = binsImage.CLim;
N        = binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(I, 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

%% Calcul des matrices de cooccurence

if isempty(LayerMask)
    Label1 = 'Image';
    Style = cl_style('Color', 'b');
    a(1,1) = cl_cooc( 'Image', I, 'Label', Label1, ...
        'Deplacements', subDep, 'Directions', subDir, 'ListeParams', subPar, ...
        'Style', Style);
    
    Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
    a(1,2) = cl_cooc( 'Image', I, 'Label', Label1, ...
        'Deplacements', subDep, 'Directions', subDir, 'ListeParams', subPar, ...
        'WindowAnalyse', W, 'Style', Style);
    
else
    
    %% Gestion des masques
    
    color = 'brkmgybrkmgybrkmgybrkmgybrkmgy';
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
    for i=1:length(valMask)
        subNonMasque = (Masque ~= valMask(i));
        J = I;
        J(subNonMasque) = NaN;
        
        Label1 = sprintf('ValMask=%d', valMask(i));
        Style = cl_style('Color', color(i));
        M = cl_cooc( 'Image', J, 'Label', Label1, ...
            'Deplacements', subDep, 'Directions', subDir, 'ListeParams', subPar, ...
            'Style', Style);
        if isempty(M)
            flag = 0;
            return
        end
        a(i,1) = M; %#ok<AGROW>
        
        Style = cl_style('Color', color(i), 'LineStyle', 'none', 'Marker', '+');
        M = cl_cooc( 'Image', J, 'Label', Label1, ...
            'Deplacements', subDep, 'Directions', subDir, 'ListeParams', subPar, ...
            'WindowAnalyse', W, 'Style', Style);
        if isempty(M)
            flag = 0;
            return
        end
        a(i,2) = M; %#ok<AGROW>
    end
end

%% Trac� graphique

[plotNormpdf, flag] = my_questdlg(Lang('TODO', 'Plot normpdf ?'), 'Init', 2);
if ~flag
    return
end
if plotNormpdf == 2
    for i=1:length(subPar)
        plot(a, 'subPar', i)
    end
else
    for i=1:length(subPar)
        histo(a, 'subPar', i)
    end
end

% imagesc(a(:,1))
str = {'None'; 'Clusters'; 'Min dist'};
[rep, flag] = my_listdlg('Multi-parameters analysis', str, 'InitialValue', 2);
if ~flag
    return
end
if any(rep >= 2)
    NomParams = get(a(1), 'NomParams');
    clear str
    for i=1:length(subPar)
        str{i} = func2str(NomParams{subPar(i)});
    end
    [subParX, flag] = my_listdlg('Parameters', str);
    if ~flag
        return
    end
end

if any(rep == 2)
    plotNuages(a, 'subParX', subParX)
end

if any(rep == 3)
    plotNuages(a, 'visuDistance', 'subParX', subParX)
end
% plotNuages(a, 'ellipsesSeules')
