% Operateur d'addition
%
% Syntax
%   c = a + b
%
% Input Arguments
%   a : Une instance de cl_image ou un scalaire
%   b : Un scalaire ou une instance de cl_image
%
% Output Arguments
%   c : Une instance de cl_image
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label);
%   imagesc(a);
%
%   [I, label] = ImageSonar(2);
%   b = cl_image('Image', I, 'Name', label);
%   imagesc(b);
%
%   c = a + 1000;
%   imagesc(c)
%
%   c = 500 + a;
%   imagesc(c);
%
%   for k=1:8
%       [I, label] = ImageSonar(k);
%       a(k) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   end
%   c = a + (-mean(a));
%   imagesc(c);
%
%   c = a(1) + a(6);
%   imagesc(c);
%
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   rampe = 1:a.nbColumns;
%   c = a + rampe;
%   imagesc(c);
%
%   mc = mean_col(c)
%   c = c + (-mc);
%   imagesc(c);
%
%   rampe = (1:a.nbRows)';
%   c = a + rampe;
%   imagesc(c);
%
%   ml = mean_lig(c)
%   c = c + (-ml);
%   imagesc(c);
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = plus(a, b, varargin)

[varargin, Mask] = getPropertyValue(varargin, 'Mask', []);
[varargin, valMask] = getPropertyValue(varargin, 'valMask', []);
[varargin, zone] = getPropertyValue(varargin, 'zone', 1); % 1 = Inside, 2=Outside

if isobject(a) && isnumeric(b)
    this   = a;
    l = b;
elseif isnumeric(a) && isobject(b)
    this   = b;
    l = a;
elseif isobject(a) && isobject(b)
    this   = a;
    l = b;
end

nbImages = length(this);
for k=1:nbImages
    if isequal(size(l), size(this))
        that = unitaire_plus(this(k), l(k), varargin{:});
    else
        that = unitaire_plus(this(k), l, varargin{:});
    end
    if isempty(that)
        return
    end
    if isempty(Mask) || isempty(valMask)
        this(k) = that;
    else
        this(k) = replaceImageThroughMask(this(k), that, Mask, valMask, zone);
    end
end


function this = unitaire_plus(this, lambda, varargin)

[varargin, NoStats] = getFlag(varargin, 'NoStats'); %#ok<ASGLU>

% Modif le 25/09/2008
% this = replace_Image(this, this.Image);
this = replace_Image(this, this.Image(:,:,:)); % Y'avait un gros bug, pas r�ussi � comprendre pourquoi
this.Writable = true;

%% Traitement des non-valeurs

this = ValNaN2NaN(this);

%% Addition

if isnumeric(lambda)
    if isequal(size(lambda), [1 1])
        if ~(isa(this.Image(1), 'double') || isa(this.Image(1), 'single'))
            this.Image = single(this.Image(:,:,:));
        end
        this = replace_Image(this, this.Image(:,:,:) + lambda);
    elseif isequal(size(lambda), [1 this.nbColumns])
        cor = repmat(lambda, this.nbRows, 1);
        if ~(isa(this.Image(1), 'double') || isa(this.Image(1), 'single'))
            this.Image = single(this.Image(:,:,:));
        end
        for k=1:this.nbSlides
            this.Image(:,:,k) = this.Image(:,:,k) + cor;
        end
    elseif isequal(size(lambda), [this.nbRows 1])
        cor = repmat(lambda, 1, this.nbColumns);
        if ~(isa(this.Image(1), 'double') || isa(this.Image(1), 'single'))
            this.Image = single(this.Image(:,:,:));
        end
        for k=1:this.nbSlides
            this.Image(:,:,k) = this.Image(:,:,k) + cor;
        end
    elseif isequal(size(lambda), [this.nbRows this.nbColumns])
        for k=1:this.nbSlides
            this.Image(:,:,k) = this.Image(:,:,k) + lambda;
        end
    else
        my_errordlg('plus')
    end
else
    [~, ~, subx, suby, subxb, subyb] = intersectionImages(this, 1:this.nbColumns, 1:this.nbRows, lambda, 'SameSize');
    if isempty(subx) || isempty(suby)
        str1 = 'Pas d''intersection commune entre les images.';
        str2 = 'No intersection between the 2 images.';
        my_warndlg(Lang(str1,str2), 1);
        this = [];
        return
    end
    lambda = replace_Image(lambda, lambda.Image(sort(subyb), sort(subxb),:));
    if ~isequal([this.nbRows this.nbColumns], [length(suby) length(subx)])
        this = replace_Image(this, this.Image(suby, subx,:));
        this = majCoordonnees(this, subx, suby);
    end
    
    switch this.SpectralStatus
        case 1
            %{
%             lambda = ValNaN2NaN(lambda);
I1 = this.Image(:,:,:);
if ~(isa(I1, 'double') || isa(I1, 'single'))
I1 = single(I1);
end
I2 = lambda.Image(:,:,:);
if ~(isa(I2, 'double') || isa(I2, 'single'))
I2 = single(I2);
end
this = replace_Image(this, I1 + I2);
            %}
            if ~(isa(this.Image(1), 'double') || isa(this.Image(1), 'single'))
                I1 = single(this.Image(:,:,:));
                this = replace_Image(this, single(I1));
            else
                this = clone(this);
            end
            if strcmp(class(this.Image(1)), class(lambda.Image(1)))
                for k=1:this.nbRows
                    this.Image(k,:,:) = this.Image(k,:,:) + lambda.Image(k,:,:);
                end
            else
                Type = class(this.Image(1));
                for k=1:this.nbRows
                    this.Image(k,:,:) = this.Image(k,:,:) + cast(lambda.Image(k,:,:), Type);
                end
            end
            
        case 2
            this.Image(isnan(this.Image)) = 0;
            lambda.Image(isnan(lambda.Image)) = 0;
            this = replace_Image(this, this.Image(:,:,:) + lambda.Image(:,:,:));
            this.Image(this.Image == 0) = NaN;
        case 3
            disp('Faut pas croire au p�re No�l')
    end
end

%% Calcul des statistiques

if ~NoStats
    this = compute_stats(this);
end

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Plus');
