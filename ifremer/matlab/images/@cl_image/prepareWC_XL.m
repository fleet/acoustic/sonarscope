%% Fonction qui calcule une c�l�rit� �quivalente et de nouveaux angles pour pouvoir calculer les trajets de rayons absolument lin�aires.
% Ceci simplifie �norm�ment le calcul des �chogrammes et donne un aspect
% plus continue que quand on s'appuie sur la bathy (qui pose un pb lorsque
% l'on est en High density)

function [RxAnglesEarth_WCNew, CNew] = prepareWC_XL(this, ProfilCelerite, RxAnglesEarth_WC, Cs, varargin)

[varargin, TransducerDepth] = getPropertyValue(varargin, 'TransducerDepth', []); %#ok<ASGLU>

%{
Bon, voila ce qu'il faut faire.

L'id�e est de faire l'approximation d'un profil isoc�l�re equivalent, qui est tout simplement la valeur moyenne de c(z) sur la hauteur d'eau. Pour le profil que tu m'as envoy�, la valeur est CNew = 1497.4 m/s. Idealement il faut prendre une valeur differente en fonction du profil c(z) local.

Dans les donnees du condeur, les faisceaux sont orientes en fonction d'une celerite qui est la celerite mesuree a la surface, ici Cs = 1532.6 m/s.
Ensuite les echantillons de WCD sont positionn�s en range avec une c�lerit� de r�f�rence qui est sans doute Cref = 1500 m/s.

Tu vas appliquer les op�rations suivantes:

- changer les valeurs des angles des faisceaux. Si les valeurs actuelles sont Angs et les valeurs modifi�es sont Angeq, on a Angeq = arcsin( sin(Angs) / Cs * CNew ).
% C'est avec ces valeurs Angeq que tu vas batir l'image corrigee de WCD.

- changer la r�partition en range des �chantillons. Si les ranges actuels (�chantillons en fonction du temps) sont Rref et les valeurs modifi�es sont Req, on a Req = Rref / Cref * CNew.

Et tu devrais trouver un fond positionn� correctement, et une colonne d'eau interpol�e sans douleur et avec une pr�cision pas trop mal.

Tu me dis quoi,

XL
%}

if isempty(ProfilCelerite)
    % Special donn�es USAN
    SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
    if contains(SonarName, 'Reson7150')
        CNew = 1520; % 1497.4; % 1520 trouv� empiriquement sur trajet retour BICOSE 1497.4 est la valeur qui �tait valide sur USAN
        X = sind(double(RxAnglesEarth_WC)) .* (CNew ./ Cs);
        X(abs(X) > 1) = NaN;
        RxAnglesEarth_WCNew = asind(X);
        
    elseif contains(SonarName, 'Reson7125')
        CNew = Cs;
        X = sind(double(RxAnglesEarth_WC)) .* (CNew ./ Cs);
        X(abs(X) > 1) = NaN;
        RxAnglesEarth_WCNew = asind(X);
    end
else
    %{
            FigUtils.createSScFigure; PlotUtils.createSScPlot(ProfilCelerite.SoundSpeed(indSPP,:), ProfilCelerite.Depth(indSPP,:), '-*'); grid on;
            subDepthSSP = interp1(ProfilCelerite.Depth(indSPP,:), 1:length(ProfilCelerite.Depth(indSPP,:)), this.Sonar.SampleBeamData.Depth, 'nearest');
            subDepthSSP(isnan(subDepthSSP)) = [];
            hold on;  PlotUtils.createSScPlot(ProfilCelerite.SoundSpeed(subDepthSSP), ProfilCelerite.Depth(indSPP,subDepthSSP), '*r');
    %}
    
    if isfield(this.Sonar, 'Ship') ...
            && isfield(this.Sonar.Ship, 'Arrays') ...
            && isfield(this.Sonar.Ship.Arrays, 'Transmit') ...
            && isfield(this.Sonar.Ship.Arrays.Transmit, 'Z') ...
            && ~isempty(this.Sonar.Ship.Arrays.Transmit.Z) ...
            && ~isnan(this.Sonar.Ship.Arrays.Transmit.Z)
        Immersion = -abs(this.Sonar.Ship.Arrays.Transmit.Z);
    else
        Immersion = 0;
    end
    
%     MaxDepth = max(abs(this.Sonar.SampleBeamData.Depth));
    if isempty(TransducerDepth)
        MaxDepth = median(abs(this.Sonar.SampleBeamData.Depth), 'omitnan');
        if isnan(MaxDepth)
            RxAnglesEarth_WCNew = RxAnglesEarth_WC;
            CNew                = Cs;
            return
        end
    else
%         MaxDepth = -abs(TransducerDepth(1));
        if isfield(this.Sonar.SampleBeamData, 'Depth')
            MaxDepth = median(abs(this.Sonar.SampleBeamData.Depth), 'omitnan'); % Modif JMA le 04/05/2020 pour donn�es Mayobs : instruction pr�c�dente tr�s bizarre !
            if isnan(MaxDepth)
                RxAnglesEarth_WCNew = RxAnglesEarth_WC;
                CNew                = Cs;
                return
            end
        else
            RxAnglesEarth_WCNew = RxAnglesEarth_WC;
            CNew                = Cs;
            return
        end
    end
    zSSp = linspace(Immersion, -MaxDepth+Immersion, 1000);
    % TODO : tri des dates
    
    % Test pour AUV
    TransducerDepth = -abs(TransducerDepth);
    if ~isempty(TransducerDepth) && (TransducerDepth(1) < 0) % Heuristique pour savoir si AUV : c'est une horreur !!!
        zSSp = zSSp + TransducerDepth(1);
    end
    
    SSP = [];
    for k1=1:length(ProfilCelerite)
        for k2=1:length(ProfilCelerite(k1).TimeProfileMeasurement)
            PC = ProfilCelerite(k1);
            
            % SSp provenant du monde Kongsberg
            if isa(PC.TimeStartOfUse, 'cl_time')
                T = PC.TimeStartOfUse.timeMat;
            else
                T = PC.TimeStartOfUse;
            end
            PC.TimeStartOfUse = cl_time('timeMat', T(k2));
            
            if isa(PC.TimeProfileMeasurement, 'cl_time')
                T = PC.TimeProfileMeasurement.timeMat;
            else
                T = PC.TimeProfileMeasurement;
            end
            PC.TimeProfileMeasurement = cl_time('timeMat', T(k2));
            
            if isfield(PC, 'PingCounter')
                PC.PingCounter = PC.PingCounter(k2);
            end
            if isfield(PC, 'NbEntries')
                PC.NbEntries = PC.NbEntries(k2);
                NbEntries = PC.NbEntries;
            else
                NbEntries = 1; % Ceinture et bretelles
            end
            if isfield(PC, 'DepthResolution')
                PC.DepthResolution = PC.DepthResolution(k2);
            end
            if isfield(PC, 'Depth')
                if size(PC.Depth,1) == NbEntries
                    PC.Depth = PC.Depth(:,k2);
                else
                    PC.Depth = PC.Depth(k2,:);
                end
            end
            if isfield(PC, 'SoundSpeed')
                if size(PC.SoundSpeed,1) == NbEntries
                    PC.SoundSpeed = PC.SoundSpeed(:,k2);
                else
                    PC.SoundSpeed = PC.SoundSpeed(k2,:);
                end
            end
%             figure; plot(PC.SoundSpeed, PC.Depth); grid on;
            % SSp provenant du monde Reson

            if isfield(PC, 'WaterTemperature')
                if size(PC.WaterTemperature,1) == NbEntries
                    PC.WaterTemperature = PC.WaterTemperature(:,k2);
                else
                    PC.WaterTemperature = PC.WaterTemperature(k2,:);
                end
            end
            if isfield(PC, 'Salinity')
                if size(PC.Salinity,1) == NbEntries
                    PC.Salinity = PC.Salinity(:,k2);
                else
                    PC.Salinity = PC.Salinity(k2,:);
                end
            end
            if isfield(PC, 'Absorption')
                if size(PC.Absorption,1) == NbEntries
                    PC.Absorption = PC.Absorption(:,k2);
                else
                    PC.Absorption = PC.Absorption(k2,:);
                end
            end
            
            SSP = [SSP PC]; %#ok<AGROW>
        end
    end
    
    TSSP = SSP(1).TimeStartOfUse.timeMat;
    if length(TSSP) == 1
        indSPP = 1;
    else
        indSPP = my_interp1_Extrap_PreviousThenNext(TSSP, 1:length(TSSP), this.Sonar.SampleBeamData.Time.timeMat);
        if isnan(indSPP)
            indSPP = 1;
        end
    end
    
    if ~isempty(SSP(indSPP).Depth)
        celerity = SSP(indSPP).SoundSpeed(:);
        if all(celerity < 10) % rencontr� sur fichier Reson 7150_20150902_173404_PP_7150_24kHz
            celerity = celerity * 1000;
        end
%         cSSP1m = my_interp1(SSP(indSPP).Depth(:), celerity, zSSp(:), 'linear');
        cSSP1m = my_interp1(SSP(indSPP).Depth(:), celerity, zSSp(:), 'linear', 'extrap'); % Modif JMA pour donn�es Marc Roche EM2040-0175-Circle-20210419-112832_P.all
%         figure; plot(celerity, SSP(indSPP).Depth(:), '.'); grid on; 
%         figure; plot(cSSP1m, zSSp(:), '.'); grid on; 
        CNew = 1 / mean(1 ./ cSSP1m, 'omitnan');
%         Cs  = celerity(1);
        %{
                    FigUtils.createSScFigure; PlotUtils.createSScPlot(celerity, SSP(indSPP).Depth(:), '-b*');
                    grid on; hold on;
                    plot(cSSP1m, zSSp(:), '.r');
                    plot([CNew CNew ], zSSp([1 end]), 'k');
                    plot([Cs Cs ], zSSp([1 end]), 'g');
                    plot([min(cSSP1m) max(cSSP1m)], [Immersion Immersion], 'k');
        %}
        
        X = sind(double(RxAnglesEarth_WC)) .* (CNew ./ Cs);
        %             figure; plot(X); grid on
        X(abs(X) > 1) = NaN;
        RxAnglesEarth_WCNew = asind(X);
%         figure; plot(RxAnglesEarth_WC, 'b*'); grid on; hold on; plot(RxAnglesEarth_WCNew, 'r*')
    end
end
