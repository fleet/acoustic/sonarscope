
function x = Imen_histGabor(im, nbValHist, varargin)

if nargin == 3
    nbProcessors = varargin{1};
else
    nbProcessors = 0;
end

% IM = im;
if nbProcessors == 0
    if ~islogical(im)
        im = ceil(im);
    end
    
    im = im(:);
    im(isnan(im)) = [];
    
    im(im > nbValHist) = nbValHist;
    im(im <= 0) = 1;
    
    % im(im > nbValHist) = [];
    % im(im <= 0) = [];
    
    x = zeros(1, nbValHist);
    for i=1:length(im)
        k = im(i);
        x(k) = x(k) + 1;
    end
    x = x / sum(x);
%     X = x;
    
%     c = 1 / 3;
%     y = filtfilt([c c c], [1 0 0], x);
%     % figure; plot(x,'k'); grid on; hold on; plot(y, 'r');
%     x = y / sum(y);
    
else
    % Changer le nom hist_Imen_mexmc en Imen_histGabor
     x = hist_Imen_mexmc(single(ceil(im)), int32(nbValHist), double(varargin{:}));
     
%      x(1:end-1) = x(2:end);
%      x(end-1:end) = X(end-1:end);
     
%      if any(x < 0)
%          x
%      end
%      if ~isequal(single(X), single(x))
%          x
%      end
end
