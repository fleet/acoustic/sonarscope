function [flag, CMasque] = SimradAll_Statistics_Curves_unitaire(this, nomFicAll, indImage, nomsLayers, DataTypes, ...
    bilan, ModeDependant, Selection, bins, UseModel, PreserveMeanValueIfModel, CourbeBias, nomCourbeBase, ...
    Comment, Coul, SelectMode, SelectSwath, SelectFM, MeanCompType, LayerMaskIn, valMask, ...
    SameReflectivityStatus, Tag, stepBins, nomDirMatFiles, varargin) %, Fig, Colors)

global isUsingParfor %#ok<GVMIS>

[varargin, Mute]           = getPropertyValue(varargin, 'Mute',           0);
[varargin, flagMarcRoche]  = getPropertyValue(varargin, 'flagMarcRoche',  0);
[varargin, isUsingParfor]  = getPropertyValue(varargin, 'isUsingParfor',  isUsingParfor); %#ok<ASGLU>

%% Lecture des layers

aKM = cl_simrad_all('nomFic', nomFicAll);
if isempty(aKM)
    CMasque = [];
    flag = 0;
    return
end

%% get layers processed as the current image

Carto = [];
[flag, c, indLayerAsCurrentOne, Selection] = get_layersProcessedAsThis(aKM, this, indImage, nomsLayers, DataTypes, ...
    bilan, ModeDependant, Carto, Selection, SameReflectivityStatus, UseModel, ...
    'PreserveMeanValueIfModel', PreserveMeanValueIfModel, 'CartoAuto', 1, 'Mute', Mute, 'flagMarcRoche', flagMarcRoche);
if ~flag
    str1 = sprintf('Le calcul des courbes n''a pas pu �tre men� � terme pour le fichier "%s".', nomFicAll);
    str2 = sprintf('The processing of the .all statistics cannot be done for file "%s".', nomFicAll);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'get_layersProcessedAsThisNotPossible');
    CMasque = [];
    return
end

%% Calcul des stats

[~, NomFic] = fileparts(c(indLayerAsCurrentOne).InitialFileName);
nomCourbe = sprintf('%s on %s', nomCourbeBase, NomFic);

GeometryType = this(indImage).GeometryType;
if isempty(LayerMaskIn)
    LayerMask = cl_image.empty;
    X = courbesStats(c(indLayerAsCurrentOne), Tag, ...
        nomCourbe, Comment{1}, [], ...
        c, Selection, 'bins', bins, 'stepBins', stepBins, ...
        'CourbeBias', CourbeBias, 'EditBias', 1, ...
        'GeometryType', cl_image.strGeometryType{GeometryType}, ...
        'Sampling', 1, 'Coul', Coul, 'nbPMin', 2, ...
        'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, 'MeanCompType', MeanCompType);
    if isempty(X)
        flag = 0;
        CMasque = [];
        return
    else
        c(indLayerAsCurrentOne) = X;
    end
    c_CourbesStatistiques = c(indLayerAsCurrentOne).CourbesStatistiques;
    CMasque = c_CourbesStatistiques(end);
    c(indLayerAsCurrentOne).CourbesStatistiques = CMasque;
    
%     if isempty(Fig) || ~ishandle(Fig)
%         Fig = FigUtils.createSScFigure;
%     end
%     plot_CourbesStats(CMasque, 'Stats', 0, 'Fig', Fig);
else
    [flag, LayerMask] = sonarMosaiqueInv(c, indLayerAsCurrentOne, LayerMaskIn);
    if ~flag
        CMasque = [];
        return
    end
    if isnan(LayerMask.StatValues.Min)
        flag = 0;
        CMasque = [];
        return
    end
    
    nbValMasque = length(valMask);
    for kValMasque=1:nbValMasque
        if (valMask(kValMasque) < LayerMask.StatValues.Min) || (valMask(kValMasque) > LayerMask.StatValues.Max)
            str1 = sprintf('Pas de courbe "%s" cr�� pour la valeur de masque=%d', nomCourbe, valMask(kValMasque));
            str2 = sprintf('No curve "%s" for mask value=%d', nomCourbe, valMask(kValMasque));
            my_warndlg(Lang(str1,str2), 0);
            flag = 0;
            CMasque = [];
            return
        end
        X = courbesStats(c(indLayerAsCurrentOne), Tag, ...
            nomCourbe, Comment{1}, [], ...
            c, Selection, 'bins', bins, ...
            'LayerMask', LayerMask, 'valMask', valMask(kValMasque), 'GroupMasques', 1, ...
            'CourbeBias', CourbeBias, 'EditBias', 1, ...
            'GeometryType', cl_image.strGeometryType{GeometryType}, ...
            'Sampling', 1, 'Coul', Coul, 'nbPMin', 2, ...
            'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, 'MeanCompType', MeanCompType);
        if isempty(X)
            flag = 0;
            CMasque = [];
            return
        end
        c(indLayerAsCurrentOne) = X;
        c_CourbesStatistiques = c(indLayerAsCurrentOne).CourbesStatistiques;
        if isempty(c_CourbesStatistiques)
            str1 = sprintf('Pas de courbe "%s" cr�� pour la valeur de masque=%d', nomCourbe, valMask(kValMasque));
            str2 = sprintf('No curve "%s" for mask value=%d', nomCourbe, valMask(kValMasque));
            my_warndlg(Lang(str1,str2), 0);
            flag = 0;
            CMasque = [];
            return
        end
        CMasque(kValMasque) = c_CourbesStatistiques(end); %#ok<AGROW>
        CMasque(kValMasque).bilan{1}(1).nomZoneEtude = [CMasque(kValMasque).bilan{1}(1).nomZoneEtude ' - Mask ' num2str(valMask(kValMasque))]; %#ok<AGROW>
        
%         if isempty(Fig) || ~ishandle(Fig)
%             Fig = FigUtils.createSScFigure;
%         end
%         kColor = 1 + mod(kValMasque-1, length(Colors));
%         plot_CourbesStats(CMasque{kValMasque,end}, 'Stats', 0, 'Fig', Fig, 'Coul', Colors(kColor));
    end
end
% legend('show');

if ~isempty(nomDirMatFiles)
    exportMatlab([c([indLayerAsCurrentOne Selection.indLayer]) LayerMask], nomDirMatFiles) % LayerMask
end

flag = 1;


function exportMatlab(this, nomDirMatFiles)
for k=1:numel(this)
    nomFic = fullfile(nomDirMatFiles, [this(k).Name '.mat']);
    export_matlab_array(this(k), nomFic)
end
