% Returns a string that describes the instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : One instance of cl_image
%
% Name-Value Pair Arguments
%   InfoSonar  : 1 : Information of the image only
%                2 : Information of the sonar data only
%                3 : Information of both Image And Sonar data : (3 par defaut)
%
% Output Arguments
%   str : Summary of the instance
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%   str = char_instance(a)
%
% See also cl_image/display cl_image/char_instance Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

[varargin, flagInfoSonarData] = getPropertyValue(varargin, 'InfoSonar', 3); %#ok<ASGLU>

str = [];

%% Traitement

if (flagInfoSonarData == 1) || (flagInfoSonarData == 3)
    str{end+1} = char_instance_Image(this);
end

if (flagInfoSonarData == 2) || (flagInfoSonarData == 3)
    str{end+1} = char_instance_sonar_data(this);
end

%% Concatenation en une chaine

str = cell2str(str);


function str = char_instance_Image(this)

str = [];
str{end+1} = sprintf('ImageName         <-> %s', this.Name);
if ~isempty(this.Image)
    str{end+1} = sprintf('ClassName         <-- %s', class(this.Image(1)));
end
str{end+1} = sprintf('Memory            <-- %s', where(this));
str{end+1} = sprintf('nbRows            <-- %d', this.nbRows);
str{end+1} = sprintf('nbColumns         <-- %d', this.nbColumns);
str{end+1} = sprintf('nbSlides          <-- %d', this.nbSlides);
str{end+1} = sprintf('Unit              <-> %s', this.Unit);
str{end+1} = sprintf('ValNaN            <-> %s', num2str(this.ValNaN));
str{end+1} = sprintf('DataType          <-> %d', this.DataType);
for k=1:length(cl_image.strDataType)
    if k == this.DataType
        str{end+1} = sprintf('                     {%d <--> %s}', k, cl_image.strDataType{k}); %#ok
    else
        str{end+1} = sprintf('                      %d <--> %s', k, cl_image.strDataType{k}); %#ok
    end
end
for k=1:length(this.strMaskBits)
    str{end+1} = sprintf('MaskBits{%d}         <--> %s}', k, this.strMaskBits{k}); %#ok
end

str{end+1} = sprintf('GeometryType      <-> %d', this.GeometryType);
for k=1:length(this.strGeometryType)
    if k == this.GeometryType
        str{end+1} = sprintf('                     {%d <--> %s}', k, this.strGeometryType{k}); %#ok
    else
        str{end+1} = sprintf('                      %d <--> %s', k, this.strGeometryType{k}); %#ok
    end
end

str{end+1} = sprintf('ImageType         <-> %s', SelectionDansListe2str(this.ImageType, this.strImageType, 'Num'));
str{end+1} = sprintf('SpectralStatus    <-> %s', SelectionDansListe2str(this.SpectralStatus, this.strSpectralStatus, 'Num'));
str{end+1} = sprintf('X                 <-> %s', num2strCode(this.x));
str{end+1} = sprintf('XLabel            <-> %s', this.XLabel);
str{end+1} = sprintf('XUnit             <-> %s', this.XUnit);
str{end+1} = sprintf('XLim              <-> %s', num2strCode(this.XLim));
str{end+1} = sprintf('XRange            <-- %f', diff(this.XLim));
if isempty(this.x)
    str{end+1} = sprintf('XStep             <--');
else
    str{end+1} = sprintf('XStep             <-- %g', get(this, 'XStep'));
    if (this.GeometryType == cl_image.indGeometryType('LatLong')) || (this.GeometryType == cl_image.indGeometryType('GeoYX'))
        str{end+1} = sprintf('XStepMetric       <-- %g (m)', this.XStepMetric);
        str{end+1} = sprintf('XRangeMetric      <-- %g (m)', this.XRangeMetric);
    end
end
str{end+1} = sprintf('XDir              <-> %s', SelectionDansListe2str(this.XDir, this.strXDir, 'Num'));
str{end+1} = sprintf('Y                 <-> %s', num2strCode(this.y));
str{end+1} = sprintf('YLabel            <-> %s', this.YLabel);
str{end+1} = sprintf('YUnit             <-> %s', this.YUnit);
str{end+1} = sprintf('YLim              <-> %s', num2strCode(this.YLim));
str{end+1} = sprintf('YRange            <-- %f', diff(this.YLim));
if isempty(this.y)
    str{end+1} = sprintf('YStep             <--');
else
    str{end+1} = sprintf('YStep             <-- %g', get(this, 'YStep'));%abs(mean(diff(this.y))));
    if (this.GeometryType == cl_image.indGeometryType('LatLong')) || (this.GeometryType == cl_image.indGeometryType('GeoYX'))
        str{end+1} = sprintf('YStepMetric       <-- %g (m)', this.YStepMetric);
        str{end+1} = sprintf('YRangeMetric      <-- %g (m)', this.YRangeMetric);
    end
end
str{end+1} = sprintf('YDir              <-> %s', SelectionDansListe2str(this.YDir, this.strYDir, 'Num'));
% str{end+1} = sprintf('CoulIndex         <-> %s', SelectionDansListe2str(this.CoulIndex, this.strCoulIndex, 'Num'));
str{end+1} = sprintf('ColormapIndex     <-> %s', SelectionDansListe2str(this.ColormapIndex, this.strColormapIndex, 'Num'));
str{end+1} = sprintf('Colormap          <-- %s', num2strCode(this.Colormap));
str{end+1} = sprintf('ColormapCustom    <-> %s', num2strCode(this.ColormapCustom));
str{end+1} = sprintf('CLim              <-> %s', num2strCode(this.CLim));
str{end+1} = sprintf('Video             <-> %s', SelectionDansListe2str(this.Video, this.strVideo, 'Num'));
str{end+1} = sprintf('VertExagAuto      <-- %f', this.VertExagAuto);
str{end+1} = sprintf('VertExag          <-> %f', this.VertExag);
str{end+1} = sprintf('Azimuth           <-> %f', this.Azimuth);
str{end+1} = sprintf('ContourValues     <-> %s', num2strCode(this.ContourValues(:)));
str{end+1} = sprintf('TagSynchroX       <-> %s', this.TagSynchroX);
str{end+1} = sprintf('TagSynchroY       <-> %s', this.TagSynchroY);
str{end+1} = sprintf('TagSynchroXScale  <-> %s', num2strPrecis(this.TagSynchroXScale));
str{end+1} = sprintf('TagSynchroYScale  <-> %s', num2strPrecis(this.TagSynchroYScale));
str{end+1} = sprintf('TagSynchroContrast <-> %s', this.TagSynchroContrast);
str{end+1} = sprintf('InitialFileName   <-> %s', this.InitialFileName);
str{end+1} = sprintf('InitialFileFormat <-> %s', this.InitialFileFormat);
str{end+1} = sprintf('InitialImageName  <-> %s', this.InitialImageName);
if isempty(this.Comments)
    str{end+1} = sprintf('Comments          <-> []');
else
    str{end+1} = sprintf('Comments          <-> %s', this.Comments(1,:));
end
for k=2:size(this.Comments,1)
    str{end+1} = sprintf('                      %s', this.Comments(k,:)); %#ok<AGROW>
end

%% History

for k=1:length(this.History)
    str{end+1} = sprintf('History(%d)', k); %#ok<AGROW>
    str{end+1} = sprintf('    Function   <-- %s', this.History(k).Function); %#ok<AGROW>
    str{end+1} = sprintf('    ImageName  <-- %s', this.History(k).Name); %#ok<AGROW>
    for k2=1:length(this.History(k).Parameters)
        if isempty(this.History(k).Parameters(k2))
            str{end+1} = sprintf('    Parameters(%d) = []', k2); %#ok<AGROW>
        else
            str{end+1} = sprintf('    Parameters(%d)', k2); %#ok<AGROW>
            str{end+1} = sprintf('        Name  <-- %s', this.History(k).Parameters(k2).Name); %#ok<AGROW>
            Val = this.History(k).Parameters(k2).Value;
            switch class(Val)
                case 'char'
                    str{end+1} = sprintf('        Value <-- %s', Val); %#ok<AGROW>
                case 'double'
                    str{end+1} = sprintf('        Value <-- %s', num2strCode(Val)); %#ok<AGROW>
                case 'strel' % Morphological tructuring element
                    str{end+1} = sprintf('        Value <-- %s', num2strCode(getnhood(Val))); %#ok<AGROW>
                otherwise
                    str{end+1} = sprintf('        Value <-- Message for JMA : this.History(k).Parameters(k2).Value not interpreted'); %#ok<AGROW>
            end
        end
    end
end

%% Carto

if ~isempty(this.Carto)
    str{end+1} = sprintf('--- Aggregation Carto (cl_carto) ---');
    str{end+1} = char(this.Carto);
end

%% Statistics

str{end+1} = '------ Statistics ------';
str{end+1} = sprintf('HistoCentralClasses <-> %s', num2strCode(this.HistoCentralClasses(:)));
str{end+1} = sprintf('HistoValues         <-- %s', num2strCode(this.HistoValues(:)));
str{end+1} = sprintf('StatValues          <-- (structure)');
str{end+1} = sprintf('StatSummary         <--');
StatSummary = this.StatSummary;
str{end+1} = [repmat('  ', size(StatSummary,1), 2) StatSummary];

%% Concatenation en une chaine

str = cell2str(str);



function str = char_instance_sonar_data(this)

str = [];
str{end+1} = '------ Sonar Info ------';
if (this.GeometryType == cl_image.indGeometryType('PingSamples')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingRange')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingAcrossDist')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingBeam')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingSnippets')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingAcrossSample'))
    
    str{end+1} = sprintf(' ');
    str{end+1} = sprintf('  --- Signals ---');
    
    str{end+1} = sprintf('  SonarRawDataResol      <-> %f (m)', this.Sonar.RawDataResol);
    str{end+1} = sprintf('  SonarResolutionD       <-> %f (m)', this.Sonar.ResolutionD);
    str{end+1} = sprintf('  SonarResolutionX       <-> %f (m)', this.Sonar.ResolutionX);
    
    if isSonarSignal(this, 'Time')
        N = length(this.Sonar.Time);
        tdeb = this.Sonar.Time(1);
        tfin = this.Sonar.Time(N);
        str{end+1} = sprintf('  SonarTime              <-> %d cl_time : Deb : %s  Fin : %s', N, t2str(tdeb), t2str(tfin));
    end
    if isSonarSignal(this, 'PingNumber')
        my_warndlg('Message for JMA : cl_image/char_instance PingNumber : on ne devrait plus passer par l�. ', 1);
        str{end+1} = sprintf('  SonarPingNumber        <-> %s', num2strCode(this.Sonar.PingNumber(:,:)));
    end
    if isSonarSignal(this, 'Immersion')
        str{end+1} = sprintf('  SonarImmersion         <-> %s (m)', num2strCode(this.Sonar.Immersion(:,:)));
    end
    if isSonarSignal(this, 'Height')
        str{end+1} = sprintf('  SonarHeight            <-> %s (m)', num2strCode(this.Sonar.Height(:,:)));
    end
    if isSonarSignal(this, 'CableOut')
        str{end+1} = sprintf('  SonarCableOut          <-> %s (m)', num2strCode(this.Sonar.CableOut(:,:)));
    end
    if isSonarSignal(this, 'Speed')
        str{end+1} = sprintf('  SonarSpeed             <-> %s', num2strCode(this.Sonar.Speed(:,:)));
    end
    if isSonarSignal(this, 'Heading')
        str{end+1} = sprintf('  SonarHeading           <-> %s (deg)', num2strCode(this.Sonar.Heading(:,:)));
    end
    if isSonarSignal(this, 'Roll')
        str{end+1} = sprintf('  SonarRoll              <-> %s (deg)', num2strCode(this.Sonar.Roll(:,:)));
    end
    if isSonarSignal(this, 'Pitch')
        str{end+1} = sprintf('  SonarPitch             <-> %s (deg)', num2strCode(this.Sonar.Pitch(:,:)));
    end
    if isSonarSignal(this, 'Yaw')
        str{end+1} = sprintf('  SonarYaw               <-> %s (deg)', num2strCode(this.Sonar.Yaw(:,:)));
    end
    if isSonarSignal(this, 'SurfaceSoundSpeed')
        str{end+1} = sprintf('  SonarSurfaceSoundSpeed <-> %s (m/s)', num2strCode(this.Sonar.SurfaceSoundSpeed(:,:)));
    end
    if isSonarSignal(this, 'Heave')
        str{end+1} = sprintf('  SonarHeave             <-> %s (deg)', num2strCode(this.Sonar.Heave(:,:)));
    end
    if isSonarSignal(this, 'Tide')
        str{end+1} = sprintf('  SonarTide              <-> %s (m)', num2strCode(this.Sonar.Tide(:,:)));
    end
    if isSonarSignal(this, 'PingCounter')
        str{end+1} = sprintf('  SonarPingCounter       <-> %s', num2strCode(this.Sonar.PingCounter(:,:)));
    end
    if isSonarSignal(this, 'SampleFrequency')
        str{end+1} = sprintf('  SonarSampleFrequency   <-> %s (Hz)', num2strCode(this.Sonar.SampleFrequency(:,:)));
    end
    if isSonarSignal(this, 'SonarFrequency')
        str{end+1} = sprintf('  SonarFrequency         <-> %s (Hz)', num2strCode(this.Sonar.SonarFrequency(:,:)));
    end
    if isSonarSignal(this, 'FishLatitude')
        str{end+1} = sprintf('  SonarFishLatitude      <-> %s (deg)', num2strCode(this.Sonar.FishLatitude(:,:)));
    end
    if isSonarSignal(this, 'FishLongitude')
        str{end+1} = sprintf('  SonarFishLongitude     <-> %s (deg)', num2strCode(this.Sonar.FishLongitude(:,:)));
    end
    if isSonarSignal(this, 'ShipLatitude')
        str{end+1} = sprintf('  SonarShipLatitude      <-> %s (deg)', num2strCode(this.Sonar.ShipLatitude(:,:)));
    end
    if isSonarSignal(this, 'ShipLongitude')
        str{end+1} = sprintf('  SonarShipLongitude     <-> %s (deg)', num2strCode(this.Sonar.ShipLongitude(:,:)));
    end
    if isSonarSignal(this, 'PortMode_1')
        str{end+1} = sprintf('  SonarPortMode_1        <-> %s', num2strCode(this.Sonar.PortMode_1(:,:)));
    end
    if isSonarSignal(this, 'PortMode_2')
        str{end+1} = sprintf('  SonarPortMode_2        <-> %s', num2strCode(this.Sonar.PortMode_2(:,:)));
    end
    if isSonarSignal(this, 'StarMode_1')
        str{end+1} = sprintf('  SonarStarMode_1        <-> %s', num2strCode(this.Sonar.StarMode_1(:,:)));
    end
    if isSonarSignal(this, 'StarMode_2')
        str{end+1} = sprintf('  SonarStarMode_2        <-> %s', num2strCode(this.Sonar.StarMode_2(:,:)));
    end
    if isSonarSignal(this, 'Interlacing')
        str{end+1} = sprintf('  SonarInterlacing       <-> %s', num2strCode(this.Sonar.Interlacing(:,:)));
    end
    if isSonarSignal(this, 'TxAlongAngle')
        str{end+1} = sprintf('  SonarTxAlongAngle      <-> %s (deg)', num2strCode(this.Sonar.TxAlongAngle(:,:)));
    end
    if isSonarSignal(this, 'TxAcrossAngle')
        str{end+1} = sprintf('  SonarTxAcrossAngle     <-> %s (deg)', num2strCode(this.Sonar.TxAcrossAngle(:,:)));
    end
    
    if isSonarSignal(this, 'PresenceWC')
        str{end+1} = sprintf('  PresenceWC         <-> %s', num2strCode(this.Sonar.PresenceWC(:,:)));
    end
    if isSonarSignal(this, 'PresenceFM')
        str{end+1} = sprintf('  PresenceFM         <-> %s', num2strCode(this.Sonar.PresenceFM(:,:)));
    end
    if isSonarSignal(this, 'DistPings')
        str{end+1} = sprintf('  SonarDistPings     <-> %s', num2strCode(this.Sonar.DistPings(:,:)));
    end
    if isSonarSignal(this, 'NbSwaths')
        str{end+1} = sprintf('  SonarNbSwaths      <-> %s', num2strCode(this.Sonar.NbSwaths(:,:)));
    end
    
    if isSonarSignal(this, 'TrueHeave')
        str{end+1} = sprintf('  SonarTrueHeave     <-> %s', num2strCode(this.Sonar.TrueHeave(:,:)));
    end
    if isSonarSignal(this, 'Draught')
        str{end+1} = sprintf('  SonarDraught       <-> %s', num2strCode(this.Sonar.Draught(:,:)));
    end
    
    if isSonarSignal(this, 'BSN')
        str{end+1} = sprintf('  SonarBSN       <-> %s', num2strCode(this.Sonar.BSN(:,:)));
    end
    
    if isSonarSignal(this, 'BSO')
        str{end+1} = sprintf('  SonarBSO       <-> %s', num2strCode(this.Sonar.BSO(:,:)));
    end
    
    if isSonarSignal(this, 'TVGN')
        str{end+1} = sprintf('  SonarRn       <-> %s', num2strCode(this.Sonar.TVGN(:,:)));
    end
    
    if isSonarSignal(this, 'TVGCrossOver')
        str{end+1} = sprintf('  SonarTVGCrossOver      <-> %s', num2strCode(this.Sonar.TVGCrossOver(:,:)));
    end
    
    if isSonarSignal(this, 'TxBeamWidth')
        str{end+1} = sprintf('  SonarTxBeamWidth       <-> %s', num2strCode(this.Sonar.TxBeamWidth(:,:)));
    end
    
    if isSonarSignal(this, 'ResolAcrossSample')
        str{end+1} = sprintf('  SonarResolAcrossSample <-> %s', num2strCode(this.Sonar.ResolAcrossSample(:,:)));
    end
    
elseif (this.GeometryType == cl_image.indGeometryType('SampleBeam'))
    % % %     if isSonarSignal(this, 'Time')
    % % %         str{end+1} = sprintf('  Time (ms)          <-> %s', num2strCode(this.Sonar.Time));
    % % %     end
    N = length(this.Sonar.Time);
    if N ~= 0
        tdeb = this.Sonar.Time(1);
        tfin = this.Sonar.Time(N);
        tdeb = cl_time('timeMat', tdeb);
        tfin = cl_time('timeMat', tfin);
        str{end+1} = sprintf('  SonarTime          <-> %d cl_time : Deb : %s  Fin : %s', N, t2str(tdeb), t2str(tfin));
    end
end

if (    (this.GeometryType == cl_image.indGeometryType('PingSamples')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingRange')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingAcrossDist')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingAcrossSample')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingBeam')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingSnippets')) || ...
        (this.GeometryType == cl_image.indGeometryType('LatLong')) || ...
        (this.GeometryType == cl_image.indGeometryType('GeoYX')))
    
    if ~isempty(this.Sonar)
        str{end+1} = sprintf(' ');
        str{end+1} = sprintf('       --- Bathycelerimetry ---');
        if isfield(this.Sonar.BathyCel, 'Z')
            str{end+1} = sprintf('  SonarBathyCel_Z    <-> %s (m)',     num2strCode(this.Sonar.BathyCel.Z));
            str{end+1} = sprintf('  SonarBathyCel_T    <-> %s (deg)',   num2strCode(this.Sonar.BathyCel.T));
            str{end+1} = sprintf('  SonarBathyCel_S    <-> %s (0/000)', num2strCode(this.Sonar.BathyCel.S));
            str{end+1} = sprintf('  SonarBathyCel_C    <-> %s (m/s)',   num2strCode(this.Sonar.BathyCel.C));
        else
            % A faire
        end
        
        str{end+1} = sprintf(' ');
        str{end+1} = sprintf('       --- Compensation of the source Level ---');
        str{end+1} = sprintf('  SonarNE_etat           <-> %s', SelectionDansListe2str(this.Sonar.NE.etat, this.Sonar.NE.strEtat, 'Num'));
        str{end+1} = sprintf(' ');
        
        str{end+1} = sprintf('       --- Compensation du gain analogique a la sortie des antennes de reception ---');
        str{end+1} = sprintf('  SonarGT_etat           <-> %s', SelectionDansListe2str(this.Sonar.GT.etat, this.Sonar.GT.strEtat, 'Num'));
        str{end+1} = sprintf(' ');
        
        str{end+1} = sprintf('       --- Compensation of the sensibility of the Tx antenas  ---');
        str{end+1} = sprintf('  SonarSH_etat           <-> %s', SelectionDansListe2str(this.Sonar.SH.etat, this.Sonar.SH.strEtat, 'Num'));
        str{end+1} = sprintf(' ');
        
        str{end+1} = sprintf('       --- TVG ---');
        str{end+1} = sprintf('  SonarTVG_etat                   <-> %s', SelectionDansListe2str(this.Sonar.TVG.etat, this.Sonar.TVG.strEtat, 'Num'));
        if this.Sonar.TVG.etat == 1 % Compens�
            str{end+1} = sprintf('  SonarTVG_origine                <-> %s', SelectionDansListe2str(this.Sonar.TVG.origine, this.Sonar.TVG.strOrigine, 'Num'));
        else
            str{end+1} = sprintf('  SonarTVG_origine                <-> %s', SelectionDansListe2str(0, this.Sonar.TVG.strOrigine, 'Num'));
        end
        str{end+1} = sprintf('  SonarTVG_ConstructTypeCompens   <-> %s', SelectionDansListe2str(this.Sonar.TVG.ConstructTypeCompens, this.Sonar.TVG.strConstructTypeCompens, 'Num'));
        str{end+1} = sprintf('         PT(R) = CoefDiverg*log10(R) + 2*Alpha*R + Constante');
        str{end+1} = sprintf('  SonarTVG_ConstructAlpha         <-> %f (dB/km)', this.Sonar.TVG.ConstructAlpha);
        str{end+1} = sprintf('  SonarTVG_ConstructCoefDiverg    <-> %f',         this.Sonar.TVG.ConstructCoefDiverg);
        str{end+1} = sprintf('  SonarTVG_ConstructConstante     <-> %f (dB)',    this.Sonar.TVG.ConstructConstante);
        str{end+1} = sprintf('  SonarTVG_ConstructTable         <-> %s', num2strCode(this.Sonar.TVG.ConstructTable));
        str{end+1} = sprintf('  SonarTVG_IfremerAlpha           <-> %s (dB/km)', num2strCode(this.Sonar.TVG.IfremerAlpha));
        str{end+1} = sprintf('  SonarTVG_IfremerCoefDiverg      <-> %s', num2strCode(this.Sonar.TVG.IfremerCoefDiverg));
        str{end+1} = sprintf('  SonarTVG_IfremerConstante       <-> %s (dB)', num2strCode(this.Sonar.TVG.IfremerConstante));
        str{end+1} = sprintf(' ');
        
        str{end+1} = sprintf('       --- Compensation of the Tx beam diagrams ---');
        str{end+1} = sprintf('  SonarDiagEmi_etat                   <-> %s', SelectionDansListe2str(this.Sonar.DiagEmi.etat, this.Sonar.DiagEmi.strEtat, 'Num'));
        str{end+1} = sprintf('  SonarDiagEmi_origine                <-> %s', SelectionDansListe2str(this.Sonar.DiagEmi.origine, this.Sonar.DiagEmi.strOrigine, 'Num'));
        str{end+1} = sprintf('  SonarDiagEmi_ConstructTypeCompens   <-> %s', SelectionDansListe2str(this.Sonar.DiagEmi.ConstructTypeCompens, this.Sonar.DiagEmi.strConstructTypeCompens, 'Num'));
        str{end+1} = sprintf('  SonarDiagEmi_IfremerTypeCompens     <-> %s', SelectionDansListe2str(this.Sonar.DiagEmi.IfremerTypeCompens, this.Sonar.DiagEmi.strIfremerTypeCompens, 'Num'));
        str{end+1} = sprintf(' ');
        
        str{end+1} = sprintf('       --- Compensation of the Rx beam diagrams ---');
        str{end+1} = sprintf('  SonarDiagRec_etat                   <-> %s', SelectionDansListe2str(this.Sonar.DiagRec.etat, this.Sonar.DiagRec.strEtat, 'Num'));
        str{end+1} = sprintf('  SonarDiagRec_origine                <-> %s', SelectionDansListe2str(this.Sonar.DiagRec.origine, this.Sonar.DiagRec.strOrigine, 'Num'));
        str{end+1} = sprintf('  SonarDiagRec_ConstructTypeCompens   <-> %s', SelectionDansListe2str(this.Sonar.DiagRec.ConstructTypeCompens, this.Sonar.DiagRec.strConstructTypeCompens, 'Num'));
        str{end+1} = sprintf('  SonarDiagRec_IfremerTypeCompens     <-> %s', SelectionDansListe2str(this.Sonar.DiagRec.IfremerTypeCompens, this.Sonar.DiagRec.strIfremerTypeCompens, 'Num'));
        str{end+1} = sprintf(' ');
        
        str{end+1} = sprintf('       --- Compensation of the insonified area ---');
        str{end+1} = sprintf('  SonarAireInso_etat                  <-> %s', SelectionDansListe2str(this.Sonar.AireInso.etat, this.Sonar.AireInso.strEtat, 'Num'));
        str{end+1} = sprintf('  SonarAireInso_origine               <-> %s', SelectionDansListe2str(this.Sonar.AireInso.origine, this.Sonar.AireInso.strOrigine, 'Num'));
        str{end+1} = sprintf(' ');
        
        str{end+1} = sprintf('       --- Compensation of the BS ---');
        str{end+1} = sprintf('  SonarBS_etat                        <-> %s', SelectionDansListe2str(this.Sonar.BS.etat, this.Sonar.BS.strEtat, 'Num'));
        str{end+1} = sprintf('  SonarBS_origine                     <-> %s', SelectionDansListe2str(this.Sonar.BS.origine, this.Sonar.BS.strOrigine, 'Num'));
        str{end+1} = sprintf('  SonarBS_origineBelleImage           <-> %s', SelectionDansListe2str(this.Sonar.BS.origineBelleImage,  this.Sonar.BS.strOrigineBelleImage,  'Num'));
        str{end+1} = sprintf('  SonarBS_origineFullCompens          <-> %s', SelectionDansListe2str(this.Sonar.BS.origineFullCompens, this.Sonar.BS.strOrigineFullCompens, 'Num'));
        if isfield(this.Sonar.BS, 'LambertCorrection')
            str{end+1} = sprintf('  SonarBS_LambertCorrection           <-> %s', SelectionDansListe2str(this.Sonar.BS.LambertCorrection, this.Sonar.BS.strLambertCorrection, 'Num'));
        end
        str{end+1} = sprintf('  SonarBS_IfremerCompensTable         <-> %s', num2strCode(this.Sonar.BS.IfremerCompensTable));
        if isfield(this.Sonar.BS, 'BSLurtonParameters')
            str{end+1} = sprintf('  SonarBS_BSLurtonParameters         <-> %s', num2strCode(this.Sonar.BS.BSLurtonParameters));
        end
        %     str{end+1} = sprintf(' ');
        %     str{end+1} = sprintf('  --- Aggregation SonarBS_IfremerCompensModele (ClModel) ---');
        
        if isfield(this.Sonar, 'Ship')
            str{end+1} = sprintf(' ');
            str{end+1} = sprintf('       --- Temporaire ---');
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Transmit.X        <-> %s', num2strCode(this.Sonar.Ship.Arrays.Transmit.X));
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Transmit.Y        <-> %s', num2strCode(this.Sonar.Ship.Arrays.Transmit.Y));
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Transmit.Z        <-> %s', num2strCode(this.Sonar.Ship.Arrays.Transmit.Z));
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Transmit.Roll     <-> %s', num2strCode(this.Sonar.Ship.Arrays.Transmit.Roll));
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Transmit.Pitch    <-> %s', num2strCode(this.Sonar.Ship.Arrays.Transmit.Pitch));
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Transmit.Heading  <-> %s', num2strCode(this.Sonar.Ship.Arrays.Transmit.Heading));
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Receive.X         <-> %f', this.Sonar.Ship.Arrays.Receive.X);
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Receive.Y         <-> %f', this.Sonar.Ship.Arrays.Receive.Y);
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Receive.Z         <-> %f', this.Sonar.Ship.Arrays.Receive.Z);
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Receive.Roll      <-> %f', this.Sonar.Ship.Arrays.Receive.Roll);
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Receive.Pitch     <-> %f', this.Sonar.Ship.Arrays.Receive.Pitch);
            str{end+1} = sprintf('  Sonar.Ship.Arrays.Receive.Heading   <-> %f', this.Sonar.Ship.Arrays.Receive.Heading);
            
            if ~isempty(this.Sonar.Ship.MRU.X)
                str{end+1} = sprintf('  Sonar.Ship.MRU.X                    <-> %f', this.Sonar.Ship.MRU.X);
                str{end+1} = sprintf('  Sonar.Ship.MRU.Y                    <-> %f', this.Sonar.Ship.MRU.Y);
                str{end+1} = sprintf('  Sonar.Ship.MRU.Z                    <-> %f', this.Sonar.Ship.MRU.Z);
                str{end+1} = sprintf('  Sonar.Ship.MRU.RollCalibration      <-> %f', this.Sonar.Ship.MRU.RollCalibration);
                str{end+1} = sprintf('  Sonar.Ship.MRU.PitchCalibration     <-> %f', this.Sonar.Ship.MRU.PitchCalibration);
                str{end+1} = sprintf('  Sonar.Ship.MRU.HeadingCalibration   <-> %f', this.Sonar.Ship.MRU.HeadingCalibration);
                str{end+1} = sprintf('  Sonar.Ship.MRU.TimeDelay            <-> %f', this.Sonar.Ship.MRU.TimeDelay);
            end
            
            if isfield(this.Sonar.Ship.GPS, 'APS')
                str{end+1} = sprintf('  Sonar.Ship.GPS.APS                  <-> %s', num2strCode(this.Sonar.Ship.GPS.APS));
            end
            
            if ~isempty(this.Sonar.Ship.GPS.X)
                str{end+1} = sprintf('  Sonar.Ship.GPS.X                    <-> %s', num2strCode(this.Sonar.Ship.GPS.X));
                str{end+1} = sprintf('  Sonar.Ship.GPS.Y                    <-> %s', num2strCode(this.Sonar.Ship.GPS.Y));
                str{end+1} = sprintf('  Sonar.Ship.GPS.Z                    <-> %s', num2strCode(this.Sonar.Ship.GPS.Z));
                str{end+1} = sprintf('  Sonar.Ship.GPS.TimeStampUse         <-> %s', num2strCode(this.Sonar.Ship.GPS.TimeStampUse));
                str{end+1} = sprintf('  Sonar.Ship.WaterLineVerticalOffset  <-> %f', this.Sonar.Ship.WaterLineVerticalOffset);
            end
        end
    end
    str{end+1} = sprintf(' ');
    
    %     str{end+1} = sprintf(' ');
    %     str{end+1} = sprintf('--- Aggregation SonarDescription (cl_sounder) ---');
    %     str{end+1} = sprintf('  Display par methode char_sonar(x)');
    %         str{end+1} = char(this.Sonar.Desciption);
end

%% BathymetryStatus

if strcmp(get_DataTypeName(this), 'Bathymetry')
    str{end+1} = sprintf('  --- Bathymetry Status ---');
    str{end+1} = sprintf('  Sonar.BathymetryStatus.TideApplied  <-> %d', this.Sonar.BathymetryStatus.TideApplied);
end

%% Signaux verticaux nouvelle g�n�ration

str{end+1} = sprintf(' ');
str{end+1} = sprintf('  --- Signals (New Generation) ---');
for k1=1:length(this.SignalsVert)
    X = [];
    for k2=length(this.SignalsVert(k1).ySample):-1:1
        X(:,k2) = this.SignalsVert(k1).ySample(k2).data;
    end
    str{end+1} = sprintf('  %s         <-> %s (%s)', this.SignalsVert(k1).name, num2strCode(X), this.SignalsVert(k1).ySample(1).unit); %#ok<AGROW>
end

%% SampleBeamData

if isfield(this.Sonar, 'SampleBeamData')
    str{end+1} = sprintf('       --- SampleBeamData ---');
    if isfield(this.Sonar.SampleBeamData, 'SampleRate')
        str{end+1} = sprintf('  SampleRate            <-> %f (Hz)',  this.Sonar.SampleBeamData.SampleRate);
    end
    if isfield(this.Sonar.SampleBeamData, 'SoundVelocity')
        str{end+1} = sprintf('  SoundVelocity         <-> %f (m/s)',  this.Sonar.SampleBeamData.SoundVelocity);
    end
    if isfield(this.Sonar.SampleBeamData, 'SystemSerialNumber')
        str{end+1} = sprintf('  SystemSerialNumber    <-> %s',  num2strCode(this.Sonar.SampleBeamData.SystemSerialNumber));
    end
    if isfield(this.Sonar.SampleBeamData, 'Frequency')
        str{end+1} = sprintf('  Frequency                 <-> %f (Hz)',  this.Sonar.SampleBeamData.Frequency);
    end
    if isfield(this.Sonar.SampleBeamData, 'Time')
        if isa(this.Sonar.SampleBeamData.Time, 'cl_time')
            T = t2str(this.Sonar.SampleBeamData.Time);
        else
            T = this.Sonar.SampleBeamData.Time;
        end
        str{end+1} = sprintf('  Time                        <-> %s',  T);
    end
    if isfield(this.Sonar.SampleBeamData, 'PingCounter')
        str{end+1} = sprintf('  PingCounter                <-> %d',  this.Sonar.SampleBeamData.PingCounter);
    end
    if isfield(this.Sonar.SampleBeamData, 'R0')
        str{end+1} = sprintf('  PingCounter                <-> %d',  this.Sonar.SampleBeamData.R0);
    end
    if isfield(this.Sonar.SampleBeamData, 'MultiPingSequence')
        str{end+1} = sprintf('  MultiPingSequence      <-> %d',  this.Sonar.SampleBeamData.MultiPingSequence);
    end
    if isfield(this.Sonar.SampleBeamData, 'iBeamBeg')
        str{end+1} = sprintf('  iBeamBeg                 <-> %d',  this.Sonar.SampleBeamData.iBeamBeg);
    end
    if isfield(this.Sonar.SampleBeamData, 'iBeamEnd')
        str{end+1} = sprintf('  iBeamEnd                 <-> %d',  this.Sonar.SampleBeamData.iBeamEnd);
    end
    if isfield(this.Sonar.SampleBeamData, 'TxPulseWidth')
        str{end+1} = sprintf('  TxPulseWidth           <-> %f (ms)',  this.Sonar.SampleBeamData.TxPulseWidth);
    end
    if isfield(this.Sonar.SampleBeamData, 'ReceiveBeamWidth')
        str{end+1} = sprintf('  ReceiveBeamWidth       <-> %f (deg)',  this.Sonar.SampleBeamData.ReceiveBeamWidth);
    end
    if isfield(this.Sonar.SampleBeamData, 'iBeamMax0')
        str{end+1} = sprintf('  iBeamMax0              <-> %d',  this.Sonar.SampleBeamData.iBeamMax0);
    end
    if isfield(this.Sonar.SampleBeamData, 'Latitude')
        str{end+1} = sprintf('  Latitude               <-> %s',  num2strPrecis(this.Sonar.SampleBeamData.Latitude));
    end
    if isfield(this.Sonar.SampleBeamData, 'Longitude')
        str{end+1} = sprintf('  Longitude              <-> %s',  num2strPrecis(this.Sonar.SampleBeamData.Longitude));
    end
    if isfield(this.Sonar.SampleBeamData, 'Heading')
        str{end+1} = sprintf('  Heading                <-> %f',  this.Sonar.SampleBeamData.Heading);
    end
    if isfield(this.Sonar.SampleBeamData, 'SamplingRateMinMax')
        str{end+1} = sprintf('  SamplingRateMinMax     <-> %s',  num2strCode(this.Sonar.SampleBeamData.SamplingRateMinMax));
    end
    if isfield(this.Sonar.SampleBeamData, 'R1SamplesFiltre')
        str{end+1} = sprintf('  R1SamplesFiltre          <-> %s',  num2strCode(this.Sonar.SampleBeamData.R1SamplesFiltre));
    end
    if isfield(this.Sonar.SampleBeamData, 'TxAngle')
        str{end+1} = sprintf('  TxAngle                    <-> %s',  num2strCode(this.Sonar.SampleBeamData.TxAngle));
    end
    if isfield(this.Sonar.SampleBeamData, 'TransmitSectorNumber')
        str{end+1} = sprintf('  TransmitSectorNumber    <-> %s',  num2strCode(this.Sonar.SampleBeamData.TransmitSectorNumber));
    end
end

%% Concatenation en une chaine

str = cell2str(str);
