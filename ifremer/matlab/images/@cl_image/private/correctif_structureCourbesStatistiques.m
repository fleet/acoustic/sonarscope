function this = correctif_structureCourbesStatistiques(this)

for kImage=1:length(this)
    for k0=1:numel(this(kImage).CourbesStatistiques)
        for k1=1:numel(this(kImage).CourbesStatistiques(k0).bilan)
            bilan = this(kImage).CourbesStatistiques(k0).bilan{k1};
            for k2=1:numel(bilan)
                if ischar(bilan(k2).DataTypeConditions)
                    bilan(k2).DataTypeConditions = {bilan(k2).DataTypeConditions};
                end
                if ischar(bilan(k2).Support)
                    bilan(k2).Support = {bilan(k2).Support};
                end
            end
            this(kImage).CourbesStatistiques(k0).bilan{k1} = bilan;
        end
    end
end
