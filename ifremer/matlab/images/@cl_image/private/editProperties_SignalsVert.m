function editProperties_SignalsVert(this, p1)

p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('SignalsVert');
p2.setEditable(false);
p1.addChild(p2);

%% Signaux verticaux nouvelle génération

for k1=1:length(this.SignalsVert)
    X = [];
    for k2=length(this.SignalsVert(k1).ySample):-1:1
        X(:,k2) = this.SignalsVert(k1).ySample(k2).data;
    end
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName(this.SignalsVert(k1).name);
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(X));
    p3.setCategory('SonarData'); % TODO : SignalsVert ?
    p3.setDisplayName(this.SignalsVert(k1).name);
    p3.setDescription(this.SignalsVert(k1).name);
    p3.setEditable(false);
    p2.addChild(p3);
end
