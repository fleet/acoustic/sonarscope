function editProperties_sonarData(this, p1)

flag = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if ~flag
    return
end

p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Sonar Data');
p2.setEditable(false);
p1.addChild(p2);

%% RawDataResol
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('RawDataResol');
p3.setType(javaclass('char',1));
p3.setValue(num2strCode(this.Sonar.RawDataResol));
p3.setCategory('SonarData');
p3.setDisplayName('RawDataResol');
p3.setDescription('RawDataResol');
p3.setEditable(false);
p2.addChild(p3);
    
%% ResolutionD
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('ResolutionD');
p3.setType(javaclass('char',1));
p3.setValue(num2strCode(this.Sonar.ResolutionD));
p3.setCategory('SonarData');
p3.setDisplayName('ResolutionD');
p3.setDescription('ResolutionD');
p3.setEditable(false);
p2.addChild(p3);

%% SonarResolutionX
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('SonarResolutionX');
p3.setType(javaclass('char',1));
p3.setValue(num2strCode(this.Sonar.ResolutionX));
p3.setCategory('SonarData');
p3.setDisplayName('SonarResolutionX');
p3.setDescription('SonarResolutionX');
p3.setEditable(false);
p2.addChild(p3);

%% SonarTime
if isSonarSignal(this, 'Time')
    N = length(this.Sonar.Time);
    tdeb = this.Sonar.Time(1);
    tfin = this.Sonar.Time(N);
    str = sprintf('%s - %s', t2str(tdeb), t2str(tfin));
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarTime');
    p3.setType(javaclass('char',1));
    p3.setValue(str);
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarTime');
    p3.setDescription('SonarTime');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% SonarPingCounter
if isSonarSignal(this, 'PingCounter')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarPingCounter');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.PingCounter));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarPingCounter');
    p3.setDescription('SonarPingCounter');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Immersion
if isSonarSignal(this, 'Immersion')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarImmersion');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Immersion));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarImmersion');
    p3.setDescription('SonarImmersion');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Height
if isSonarSignal(this, 'Height')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarHeight');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Height));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarHeight');
    p3.setDescription('SonarHeight');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Height
if isSonarSignal(this, 'CableOut')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarCableOut');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.CableOut));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarCableOut');
    p3.setDescription('SonarCableOut');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Speed
if isSonarSignal(this, 'Speed')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarSpeed');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Speed));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarSpeed');
    p3.setDescription('SonarSpeed');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Heading
if isSonarSignal(this, 'Heading')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarHeading');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Heading));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarHeading');
    p3.setDescription('SonarHeading');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Roll
if isSonarSignal(this, 'Roll')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarRoll');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Roll));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarRoll');
    p3.setDescription('SonarRoll');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Pitch
if isSonarSignal(this, 'Pitch')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarPitch');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Pitch));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarPitch');
    p3.setDescription('SonarPitch');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Yaw
if isSonarSignal(this, 'Yaw')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarYaw');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Yaw));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarYaw');
    p3.setDescription('SonarYaw');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% SurfaceSoundSpeed
if isSonarSignal(this, 'SurfaceSoundSpeed')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarSurfaceSoundSpeed');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.SurfaceSoundSpeed));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarSurfaceSoundSpeed');
    p3.setDescription('SonarSurfaceSoundSpeed');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Heave
if isSonarSignal(this, 'Heave')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarHeave');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Heave));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarHeave');
    p3.setDescription('SonarHeave');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Tide
if isSonarSignal(this, 'Tide')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarTide');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Tide));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarTide');
    p3.setDescription('SonarTide');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% SampleFrequency
if isSonarSignal(this, 'SampleFrequency')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarSampleFrequency');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.SampleFrequency));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarSampleFrequency');
    p3.setDescription('SonarSampleFrequency');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% SonarFrequency
if isSonarSignal(this, 'SonarFrequency')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('SonarFrequency');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.SonarFrequency));
    p3.setCategory('SonarData');
    p3.setDisplayName('SonarFrequency');
    p3.setDescription('SonarFrequency');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% FishLatitude
if isSonarSignal(this, 'FishLatitude')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('FishLatitude');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.FishLatitude));
    p3.setCategory('SonarData');
    p3.setDisplayName('FishLatitude');
    p3.setDescription('FishLatitude');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% FishLongitude
if isSonarSignal(this, 'FishLongitude')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('FishLongitude');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.FishLongitude));
    p3.setCategory('SonarData');
    p3.setDisplayName('FishLongitude');
    p3.setDescription('FishLongitude');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% ShipLatitude
if isSonarSignal(this, 'ShipLatitude')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ShipLatitude');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.ShipLatitude));
    p3.setCategory('SonarData');
    p3.setDisplayName('ShipLatitude');
    p3.setDescription('ShipLatitude');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% ShipLongitude
if isSonarSignal(this, 'ShipLongitude')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ShipLongitude');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.ShipLongitude));
    p3.setCategory('SonarData');
    p3.setDisplayName('ShipLongitude');
    p3.setDescription('ShipLongitude');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% PortMode_1
if isSonarSignal(this, 'PortMode_1')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('PortMode_1');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.PortMode_1));
    p3.setCategory('SonarData');
    p3.setDisplayName('PortMode_1');
    p3.setDescription('PortMode_1');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% PortMode_2
if isSonarSignal(this, 'PortMode_2')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('PortMode_2');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.PortMode_2));
    p3.setCategory('SonarData');
    p3.setDisplayName('PortMode_2');
    p3.setDescription('PortMode_2');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% StarMode_1
if isSonarSignal(this, 'StarMode_1')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('StarMode_1');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.StarMode_1));
    p3.setCategory('SonarData');
    p3.setDisplayName('StarMode_1');
    p3.setDescription('StarMode_1');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% StarMode_2
if isSonarSignal(this, 'StarMode_2')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('StarMode_2');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.StarMode_2));
    p3.setCategory('SonarData');
    p3.setDisplayName('StarMode_2');
    p3.setDescription('StarMode_2');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Interlacing
if isSonarSignal(this, 'Interlacing')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Interlacing');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.Interlacing));
    p3.setCategory('SonarData');
    p3.setDisplayName('Interlacing');
    p3.setDescription('Interlacing');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% PresenceWC
if isSonarSignal(this, 'PresenceWC')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('PresenceWC');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.PresenceWC));
    p3.setCategory('SonarData');
    p3.setDisplayName('PresenceWC');
    p3.setDescription('PresenceWC');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% PresenceFM
if isSonarSignal(this, 'PresenceFM')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('PresenceFM');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.PresenceFM));
    p3.setCategory('SonarData');
    p3.setDisplayName('PresenceFM');
    p3.setDescription('PresenceFM');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% DistPings
if isSonarSignal(this, 'DistPings')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('DistPings');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.DistPings));
    p3.setCategory('SonarData');
    p3.setDisplayName('DistPings');
    p3.setDescription('DistPings');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% NbSwaths
if isSonarSignal(this, 'NbSwaths')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('NbSwaths');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.NbSwaths));
    p3.setCategory('SonarData');
    p3.setDisplayName('NbSwaths');
    p3.setDescription('NbSwaths');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% BSN
if isSonarSignal(this, 'BSN')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BSN');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.BSN));
    p3.setCategory('SonarData');
    p3.setDisplayName('BSN');
    p3.setDescription('BSN');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% BSO
if isSonarSignal(this, 'BSO')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BSO');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.BSO));
    p3.setCategory('SonarData');
    p3.setDisplayName('BSO');
    p3.setDescription('BSO');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Rn
if isSonarSignal(this, 'TVGN')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Rn');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.TVGN));
    p3.setCategory('SonarData');
    p3.setDisplayName('Rn');
    p3.setDescription('Rn');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% TVGCrossOver
if isSonarSignal(this, 'TVGCrossOver')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('TVGCrossOver');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.TVGCrossOver));
    p3.setCategory('SonarData');
    p3.setDisplayName('TVGCrossOver');
    p3.setDescription('TVGCrossOver');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% TxBeamWidth
if isSonarSignal(this, 'TxBeamWidth')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('TxBeamWidth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.TxBeamWidth));
    p3.setCategory('SonarData');
    p3.setDisplayName('TxBeamWidth');
    p3.setDescription('TxBeamWidth');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% ResolAcrossSample
if isSonarSignal(this, 'ResolAcrossSample')
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('ResolAcrossSample');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this.Sonar.ResolAcrossSample));
    p3.setCategory('SonarData');
    p3.setDisplayName('ResolAcrossSample');
    p3.setDescription('ResolAcrossSample');
    p3.setEditable(false);
    p2.addChild(p3);
end

% TODO : A compléter avec EtatSonar
