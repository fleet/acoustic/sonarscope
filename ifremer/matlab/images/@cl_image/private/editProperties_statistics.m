
function editProperties_statistics(this, p1)

p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Statistics');
    p2.setEditable(false);
    p1.addChild(p2);
    
%% HistoCentralClasses
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('HistoCentralClasses');
% p3.setType(javaclass('char',1)); 
p3.setValue(num2strCode(this.HistoCentralClasses(:)));
p3.setCategory('Image statistics');
p3.setDisplayName('HistoCentralClasses');
p3.setDescription('Bins coordinates of the histogram.');
p3.setEditable(false);
p2.addChild(p3);

%% HistoValues
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('HistoValues');
% p3.setType(javaclass('char',1)); 
p3.setValue(num2strCode(this.HistoValues(:)));
p3.setCategory('Image statistics');
p3.setDisplayName('HistoValues');
p3.setDescription('Bins counts of the histogram.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Mean
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Mean');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Moyenne);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Mean');
p3.setDescription('Mean value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Variance
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Variance');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Variance);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Var');
p3.setDescription('Variance value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Sigma
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Sigma');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Sigma);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Std');
p3.setDescription('Standard deviation value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Mediane
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Mediane');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Mediane);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Median');
p3.setDescription('Median value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Min
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Min');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Min);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Min');
p3.setDescription('Min value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Max
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Max');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Max);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Max');
p3.setDescription('Max value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Range
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Range');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Range);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Range');
p3.setDescription('Range of the values (Max - Min).');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Quantile_0.5%
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Quantile_0.5%');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Quant_25_75);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Quantile_0.5%');
p3.setDescription('Values corresponding to 0.5% of saturated values on low and high values.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Quantile_1%
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Quantile_1%');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Quant_01_99);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Quantile_1%');
p3.setDescription('Values corresponding to 1% of saturated values on low and high values.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Quantile_3%
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Quantile_3%');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Quant_03_97);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Quantile_3%');
p3.setDescription('Values corresponding to 3% of saturated values on low and high values.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.PercentNaN
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.PercentNaN');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.PercentNaN);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.PercentNaN');
p3.setDescription('Pourcentage of No Data values.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Sampling
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Sampling');
% p3.setType(javaclass('double',1));
p3.setValue(this.StatValues.Sampling);
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Sampling');
p3.setDescription('Sampling value used to compute statistics.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.NbData
if isfield(this.StatValues, 'NbData') % Pour compatibilité avec données anciennes
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('StatValues.NbData');
    % p3.setType(javaclass('double',1));
    p3.setValue(this.StatValues.NbData);
    p3.setCategory('Image statistics');
    p3.setDisplayName('StatValues.NbData');
    p3.setDescription('NbData value used to compute statistics.');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% StatValues.Skewness
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Skewness');
% p3.setType(javaclass('char',1)); 
p3.setValue('Computed on demand');
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Skewness');
p3.setDescription('Skeness value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Kurtosis
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Kurtosis');
% p3.setType(javaclass('char',1)); 
p3.setValue('Computed on demand');
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Kurtosis');
p3.setDescription('Kurtosis value.');
p3.setEditable(false);
p2.addChild(p3);

%% StatValues.Entropy
p3 = com.jidesoft.grid.DefaultProperty();
p3.setName('StatValues.Entropy');
% p3.setType(javaclass('char',1)); 
p3.setValue('Computed on demand');
p3.setCategory('Image statistics');
p3.setDisplayName('StatValues.Entropy');
p3.setDescription('Entropy value.');
p3.setEditable(false);
p2.addChild(p3);