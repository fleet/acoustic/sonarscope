% Computes a sun shading image from a gradient image
%
% Syntax
%   ombre = gradient2sunShadingGray(this, grad, ...)
%
% Input Arguments
%   this : One cl_image instance
%   grad : Gradient
%
% Output Arguments
%   ombre : Gray level sun shaded image
%
% Examples That is a private function, see example in the "See also" functions
%
% See also cl_image/sunShadingGray Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ombre = gradient2sunShadingGray(this, grad, varargin)

[varargin, VertExag] = getPropertyValue(varargin, 'VertExag', this.VertExag); %#ok<ASGLU>

VertExag = this.VertExagAuto / VertExag;

elevation = 30;
moyenne = my_nanmean(grad(:));
ombre   = (grad - moyenne) / VertExag;
ombre = ((90-elevation) * (pi/180)) - ombre;
ombre = ombre .* ombre;
ombre(ombre < -pi) = -pi;
ombre(ombre >  pi) =  pi;
ombre = cos(ombre);
