function [flag, Value] = paramsHandCorrection(this, Value)
        
str1 = 'Modification interactive de l''image par une valeur particulière';
str2 = 'Set a new value on interactive regions';
[flag, Value] = inputOneParametre(Lang(str1,str2), Lang('Valeur', 'Value'), ...
    'Value', Value, 'Unit', this.Unit);
if ~flag
    return
end
