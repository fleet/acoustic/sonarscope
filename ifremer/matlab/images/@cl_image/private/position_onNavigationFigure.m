function position_onNavigationFigure(Lat, Lon, varargin)

figNav = findobj(0, 'Type', 'Figure', 'Tag', 'Trace de navigation');
for k1=1:length(figNav)
    h = findobj(figNav(k1), 'Tag', 'PositionPing');
    for k2=1:length(h)
        delete(h(k2))
    end

    h = findobj(figNav(k1), 'Type', 'axes');

    subh = [];
    for k2=1:length(h)
        if strcmp(get(h(k2), 'Tag'), 'Colorbar')
            subh(k2) = 0; %#ok<AGROW>
        else
            subh(k2) = 1; %#ok<AGROW>
        end
    end
    h = h(subh == 1);
    for k2=length(h):-1:1
        C = get(h(k2), 'Children');
        for k3=1:length(C)
            if isAGraphicElement(C( k3 ), 'image')
%                 h(k2) = []; % Comment� JMA le 02/09/2014 car plus de
%                 trac� de la croix sur les navigations. Je ne me souviens
%                 plus pourquoi j'avais mis cette exclusion !
            end
        end
    end

    subh = [];
    for k2=1:length(h)
        if isequal(get(h(k2), 'XLim'), [0 1]) && isequal(get(h(k2), 'YLim'), [0 1])
            delete(h(k2))
            subh(k2) = 0; %#ok<AGROW>
        else
            subh(k2) = 1; %#ok<AGROW>
        end
    end
    h = h(subh == 1);


    % Bidouille pour supprimer un axe cr�� lors de l'ouvertire d'un fichier
    % .fig
    if isempty(h)
        return
    end

    %     h(findobj(h, 'Tag', 'Colorbar')) = [];
    h = h(end) ; % Rajout� par JMA le 02/03/2014 pour navigation SCAMPI
    XLim = get(h, 'XLim');
    YLim = get(h, 'YLim');
    if (Lon >= XLim(1)) && (Lon <= XLim(2)) && (Lat >= YLim(1)) && (Lat <= YLim(2))
        hold(h, 'on');
        hc = plot(h, [Lon Lon], YLim, 'r');
        set(hc, 'Tag', 'PositionPing')
        hc = plot(h, XLim, [Lat Lat], 'r');
        set(hc, 'Tag', 'PositionPing')
        hold(h, 'off')
    end
end

if nargin == 3
    Time = varargin{1};
    figCapeurs = findobj(0, 'Type', 'Figure', 'Tag', 'Trace de navigation&Time');
    if ~isempty(figCapeurs)
        h = findobj(figCapeurs, 'Tag', 'PositionPing');
        for k=1:length(h)
            delete(h(k))
        end
        h = findobj(figCapeurs, 'Type', 'axes');
        for k=1:length(h)
            YLim = get(h(k), 'YLim');
            hold(h(k), 'on');
            hc = plot(h(k), [Time Time], YLim, 'r', 'MarkerSize', 1000);
            set(hc, 'Tag', 'PositionPing')
            hold(h(k), 'off');
        end
    end
end