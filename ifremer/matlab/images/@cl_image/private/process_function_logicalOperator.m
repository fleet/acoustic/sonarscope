% Execute logical functions of type y=fct(x) such as or, and, xor, ...
%
% Syntax
%   [c, flag] = process_function_logicalOperator(a, b, hFunc)
%
% Input Arguments
%   a     : Instance(s) of cl_image or matrix
%   b     : Instance(s) of cl_image or matrix
%   hFunc : Handle of the Matlab function
%
% Output Arguments
%   c    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%     I = repmat(-100:100, 256, 1);
%     a = cl_image('Image', I, 'ColormapIndex', 3);
%     imagesc(a)
%     b = a < 10;
%     c = a > 20;
%     imagesc(b)
%     imagesc(c)
%     d = b | c;
%     imagesc(d)
%
%   [e, flag] = process_function_logicalOperator(b, c, @or);
%     imagesc(e);
%
% See also cl_image/and cl_image/or cl_image/xor Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = process_function_logicalOperator(a, b, hFunc)

if isobject(a) && (isnumeric(b) || islogical(b))
    this   = a;
    lambda = b;
elseif (isnumeric(a) || islogical(a) ) && isobject(b)
    this   = b;
    lambda = a;
elseif isobject(a) && isobject(b)
    this   = a;
    lambda = b;
end

nbImages = length(this);
if length(lambda) == 1
    for k=1:nbImages
        [flag, X] = process_function_logicalOperator_unit(this(k), lambda, hFunc);
        if ~flag
            that = [];
            return
        end
        that(k) = X; %#ok<AGROW>
    end
elseif isequal(size(lambda), size(this))
    for k=1:nbImages
        [flag, X] = process_function_logicalOperator_unit(this(k), lambda(k), hFunc);
        if ~flag
            that = [];
            return
        end
        that(k) = X; %#ok<AGROW>
    end
else
    for k=1:nbImages
        [flag, X] = process_function_logicalOperator_unit(this(k), lambda, hFunc);
        if ~flag
            that = [];
            return
        end
        that(k) = X; %#ok<AGROW>
    end
end


function [flag, this] = process_function_logicalOperator_unit(this, lambda, hFunc)

%% Traitement des non-valeurs

% this = Here_ValNaN2NaN(this);

%% Algorithme

if isnumeric(lambda) || islogical(lambda)
    % lamba n'est pas une instance de cl_image
    if this.ImageType == 2 % RGB
        nbSlides = 3;
    else
        nbSlides = 1;
    end
    if isequal(size(lambda), [1 1])
        I = (this.Image(:,:,:) ~= 0);
%         this = replace_Image(this, I & lambda);
        this = replace_Image(this, hFunc(I,lambda));
    elseif isequal(size(lambda), [1 this.nbColumns])
        cor = repmat(lambda, this.nbRows, 1);
        for k=1:nbSlides
            I = (this.Image(:,:,k) ~= 0);
%             this.Image(:,:,k) = I & cor;
            this.Image(:,:,k) = hFunc(I,cor);
        end
    elseif isequal(size(lambda), [this.nbRows 1])
        cor = repmat(lambda, 1, this.nbColumns);
        for k=1:nbSlides
            I = (this.Image(:,:,k) ~= 0);
%             this.Image(:,:,k) = I & cor;
            this.Image(:,:,k) = hFunc(I,cor);
        end
    elseif isequal(size(lambda), [this.nbRows this.nbColumns])
        for k=1:nbSlides
            I = (this.Image(:,:,k) ~= 0);
%             this.Image(:,:,k) = I & lambda;
            this.Image(:,:,k) = hFunc(I,lambda);
        end
    else
        my_errordlg('and')
    end
else
    % lamba est une instance de cl_image
    [~, ~, subx, suby, subxb, subyb] = intersectionImages(this, 1:this.nbColumns, 1:this.nbRows, lambda);
    if isempty(subx) || isempty(suby)
        str1 = 'Pas d''intersection commune entre les images.';
        str2 = 'No intersection between the 2 images.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    lambda = replace_Image(lambda, lambda.Image(sort(subyb), sort(subxb))); % Rajout� "sort" le 08/12/2006 : pas v�rifi�
    if ~isequal([this.nbRows this.nbColumns], [length(suby) length(subx)])
        this = replace_Image(this, this.Image(suby, subx));
        this = majCoordonnees(this, subx, suby);
    end
    
    I = (this.Image(:,:,:) ~= 0);
    J = (lambda.Image(:,:,:) ~= 0);
%     this = replace_Image(this, I & J);
    this = replace_Image(this, hFunc(I,J));
end

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
this.CLim               = [0 1];
this.ValNaN             = 2;
this.ColormapIndex      = 2;

%% Compl�tion du titre

this = update_Name(this, 'Append', func2str(hFunc));

%% Par ici la sortie

flag = 1;

% function this = Here_ValNaN2NaN(this)
% 
% subNan = findNaN(this);
% this = replace_Image(this, this.Image);
% this.Image(subNan) = 0;
% this.ValNaN = NaN;
