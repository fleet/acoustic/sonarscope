% Execute a specific Matlab function of type y=fct(x) such as abs, sin, floor, ...
%
% Syntax
%   [b, flag] = process_function_type1(a, hFunc, ...)
%
% Input Arguments
%   a     : Instance(s) of cl_image
%   hFunc : Handle of the Matlab function
%
% Name-Value Pair Arguments
%   subx       : Sub-sampling in abscissa
%   suby       : Sub-sampling in ordinates
%   FctName    : Function name in case the function handle is an anonymous function (see log and log10)
%   MaskOut    : cl_image instance of a mask to be applied after the algorithm
%   ValMaskOut : Value(s) of the mask to use
%   ZoneMasOut : {1='In'} | 2='Out'
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Remarks : The values are automatically transformed in single unless they
%           are in double. ValNaN is set to NaN
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = process_function_type1(a, @abs);
%   imagesc(b);
%
% See also cl_image/abs cl_image/singleUnlessDouble process_function_type2 process_function_type3 Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = process_function_type1(this, hFunc, varargin)

[varargin, FctName] = getPropertyValue(varargin, 'FctName', func2str(hFunc));

flag = 0;
that = cl_image.empty;

N = length(this);
% FctName = func2str(hFunc);
hw = create_waitbar(waitbarMsg(FctName), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = process_function_type1_unit(this(k), hFunc, FctName, varargin{:});
    if ~flag
        return
    end
    that(k) = X;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = process_function_type1_unit(this, hFunc, FctName, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Unit]        = getPropertyValue(varargin, 'Unit',        this.Unit);
[varargin, ValNaN]      = getPropertyValue(varargin, 'ValNaN',      NaN);
[varargin, Comments]    = getPropertyValue(varargin, 'Comments',    []);
[varargin, MaskOut]     = getPropertyValue(varargin, 'MaskOut',     []);
[varargin, ValMaskOut]  = getPropertyValue(varargin, 'ValMaskOut',  []);
[varargin, ZoneMaskOut] = getPropertyValue(varargin, 'ZoneMaskOut', 1);

if ~isdeployed % Le temps de s'assurer que c'est fait partout
    [varargin, expectedUnit] = getPropertyValue(varargin, 'expectedUnit', []); %#ok<ASGLU>
    if ~isempty(expectedUnit) % One do not test if the expectedUnit is [] as for acos, ... It would be too constraining
        flag = checkUnit(this, expectedUnit);
        if ~flag
            % Throw exception ?
            that = [];
            return
        end
    end
end

%% Algorithm

I = this.Image(suby,subx,:);
I = singleUnlessDouble(I, this.ValNaN);
I = hFunc(I);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', FctName, 'Unit', Unit, ...
    'ValNaN', ValNaN, 'Comments', Comments);

%% Mask after algorithm

if ~isempty(MaskOut)
    that = masquage(that, 'LayerMask', MaskOut, 'valMask', ValMaskOut, 'zone', ZoneMaskOut);
end

flag = 1;
