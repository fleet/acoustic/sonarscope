% Execute a specific Matlab function of type y=fct(x, Var) such as lt, ge, ...
%
% Syntax
%   [b, flag] = abs(a, hFunc, Var, ...)
%
% Input Arguments
%   a      : Instance(s) of cl_image
%   hFunc  : Handle of the Matlab function
%   Var    : Scalar value
%
% Name-Value Pair Arguments
%   subx       : Sub-sampling in abscissa
%   suby       : Sub-sampling in ordinates
%   MaskOut    : cl_image instance of a mask to be applied after the algorithm
%   ValMaskOut : Value(s) of the mask to use
%   ZoneMasOut : {1='In'} | 2='Out'
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Remarks : The values are automatically transformed in single unless they
%           are in double. ValNaN is set to NaN
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = process_function_type2(a, @lt, 128);
%   imagesc(b);
%
%   b = (a < 128);
%   imagesc(b);
%
% See also cl_image/log10 cl_image/singleUnlessDouble process_function_type1 process_function_type3 Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = process_function_type2(this, hFunc, Var, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hName = func2str(hFunc);
hw = create_waitbar(waitbarMsg(hName), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = process_function_type2_unit(this(k), hFunc, hName, Var, varargin{:});
    if ~flag
        return
    end
    that(k) = X;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = process_function_type2_unit(this, hFunc, hName, Var, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Unit]        = getPropertyValue(varargin, 'Unit', this.Unit);
[varargin, ValNaN]      = getPropertyValue(varargin, 'ValNaN', NaN);
[varargin, Comments]    = getPropertyValue(varargin, 'Comments', []);
[varargin, MaskOut]     = getPropertyValue(varargin, 'MaskOut', []);
[varargin, ValMaskOut]  = getPropertyValue(varargin, 'ValMaskOut', []);
[varargin, ZoneMaskOut] = getPropertyValue(varargin, 'ZoneMaskOut', 1);

if ~isdeployed % Le temps de s'assurer que c'est fait partout
    [varargin, expectedUnit] = getPropertyValue(varargin, 'expectedUnit', []); %#ok<ASGLU>
    if ~isempty(expectedUnit) % One do not test if the expectedUnit is [] as for acos, ... It would be too constraining
        flag = checkUnit(this, expectedUnit);
        if ~flag
            % Throw exception ?
            that = [];
            return
        end
    end
end

%% Algorithm

I = this.Image(suby,subx,:);
I = singleUnlessDouble(I, this.ValNaN);
I = hFunc(I, Var);

%% Output image

Parameters = getHistoryParameters(Var);
that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', hName, 'Unit', Unit, ...
    'ValNaN', ValNaN, 'Comments', Comments, 'Parameters', Parameters);

%% Mask after algorithm

if ~isempty(MaskOut)
    that = masquage(that, 'LayerMask', MaskOut, 'valMask', ValMaskOut, 'zone', ZoneMaskOut);
end

flag = 1;
