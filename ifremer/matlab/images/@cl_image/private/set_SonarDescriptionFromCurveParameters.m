function this = set_SonarDescriptionFromCurveParameters(this, bilan)

Mode_2 = [];
Mode_3 = [];

if ~isempty(bilan(1).TagSup) && strcmp(bilan(1).TagSup(1,1), 'Sonar.Mode_1')
    Mode_1 = bilan(1).TagSup{1,2};
else
    if isfield(bilan(1), 'Mode2EM2040') && ~isempty(bilan(1).Mode2EM2040) % Ajout JMA le ???? pour DiagTx EM2040D
        Mode_1 = bilan(1).NbSwaths;
        Mode_2 = bilan(1).Mode2EM2040;
        if isfield(bilan(1), 'Mode3EM2040') && ~isempty(bilan(1).Mode3EM2040) % Ajout JMA le 15/09/2022 pour DiagTx EM2040D
            Mode_3 = bilan(1).Mode2EM2040;
        end
    else
        try % EM710 et nouveau sondeurs
            Mode_1 = bilan(1).NbSwaths; % Modifi� par JMA le 22/03/2017 pour DiagTx EM710 des .all
            Mode_2 = bilan(1).Mode;     % Modifi� par JMA le 22/03/2017 pour DiagTx EM710 des .all
        catch % EM3002 ? TODO : � v�rifier
            Mode_1 = bilan(1).Mode;     % Rajout� par JMA le 06/09/2016 pour DiagTx en surveyProcessing des .all
        end
    end
end

if size(bilan(1).TagSup, 1) >= 2
    if strcmp(bilan(1).TagSup(2,1), 'Sonar.Mode_2')
        Mode_2 = bilan(1).TagSup{2,2};
    else
        messageForDebugInspection
    end
else
%     Mode_2 = [];
end
% this.Sonar.Desciption = set(this.Sonar.Desciption, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', Mode_2);
this.Sonar.Desciption = set(this.Sonar.Desciption, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', Mode_2, 'Sonar.Mode_3', Mode_3);
