% Statistics along horizontal or vertical image directions
%
% Syntax
%   [val, N] = stats_alongOneDirection(a, hFunc, funcName, ...)
%
% Input Arguments
%   a        : One cl_image instance
%   funcName : Generic name of the function
%   Fig      : Plot of the curve (true | false)
%   NumDim   : Direction of the statistic computation (1=Vertical, 2=Horizontal)
%
% Name-Value Pair Arguments
%   subx        : Sub-sampling in abscissa
%   suby        : Sub-sampling in ordinates
%   TypeCurve   : Type of curve to display : 'Value' | 'NbPixels' (Default : 'Value')
%   LayerMask : A cl_image instance of a mask to apply onto the image before processing the statistics
%   valMask   : Value(s) of the mask to use
%
%
% Output Arguments
%   val : Statistics of pixels along the requested direction
%   N   : Number of processed pixels
%
% Remarks : In case of Auto plot, a question is asked to the user if
%           he wants to plot the curve in the classical Matlab
%           direction or vertically.
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%   [val, N] = stats_alongOneDirection(a,'sum', 2, 0);
%   val = stats_alongOneDirection(a, 'sum', 2, 1)
%   stats_alongOneDirection(a, 'sum', 2, 0);
%   stats_alongOneDirection(a, 'sum', 2, 1, 'TypeCurve', 'NbPixels');
%
% See also cl_image/min_lig cl_image/max_lig cl_image/mean_lig cl_image/median_lig cl_image/sum_lig
%            cl_image/min_col cl_image/max_col cl_image/mean_col cl_image/median_col cl_image/sum_col  Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, nbPix] = stats_alongOneDirection(this, fctName, numDim, Fig, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, TypeCurve] = getPropertyValue(varargin, 'TypeCurve', 'Value');
[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, Threshold] = getPropertyValue(varargin, 'Threshold', 0.5);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask',   []); %#ok<ASGLU>

flag = checkOnlyOneInstance(this, [fctName '_lig']);
if ~flag
    % This will provoque an error but it is assumed.
    return
end

if this.ImageType == 2 % RGB
    nbSlides = 3;
else
    nbSlides = 1;
end

switch numDim
    case 1
        nameDirection = 'Vertical';
        val = NaN(nbSlides, length(subx));
        nbPix = zeros(nbSlides, length(subx));
        XLabel = this.XLabel;
        XUnit  = this.XUnit;
        YLabel = this.YLabel;
        YUnit  = this.YUnit;
    case 2
        nameDirection = 'Horizontal';
        val = NaN(nbSlides, length(suby));
        nbPix = zeros(nbSlides, length(suby));
        XLabel = this.YLabel;
        XUnit  = this.YUnit;
        YLabel = this.XLabel;
        YUnit  = this.XUnit;
    otherwise
        str1 = sprintf('numDim=%d non interprété dans la méthode cl_image/private/stats_alongOneDirection', numDim);
        str2 = sprintf('numDim=%d not recognized in method cl_image/private/stats_alongOneDirection', numDim);
        my_warndlg(Lang(str1,str2), 1);
end

if ~isempty(LayerMask)
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
    K = zeros(size(Masque), 'uint8');
    for ik=1:length(valMask)
        %             sub = find(Masque == valMask(ik));
        sub = (Masque == valMask(ik));
        K(sub) = 1;
    end
    subNonMasque = (K == 0);
end

indMinMax = [];
for k=1:nbSlides
    I = (this.Image(suby, subx, k));
    I = singleUnlessDouble(I, this.ValNaN);
    if ~isempty(LayerMask)
        I(subNonMasque) = NaN;
    end
    
    switch fctName
        case 'sum'
            val(k,:) = sum(I, numDim, 'omitnan');
            Familly = 11;
            
        case 'min'
            [val(k,:), indMinMax(:,k)] = min(I, [], numDim); %#ok<AGROW>
            Familly = 10;
            
        case 'max'
            [val(k,:), indMinMax(:,k)] = max(I, [], numDim); %#ok<AGROW>
            Familly = 10;
            
        case 'median'
            val(k,:) = median(I, numDim, 'omitnan');
            Familly = 10;
            
        case 'mean'
            val(k,:) = mean(I, numDim, 'omitnan');
            Familly = 10;
            
        case 'std'
            val(k,:) = std(I, 0, numDim, 'omitnan');
            Familly = 12;
            
        case 'quantile'
            val(k,:) = quantile(I, numDim, Threshold); % TODO : 
            Familly = 10;
            
        otherwise
            str1 = sprintf('"%s" non interprété dans la méthode cl_image/private/stats_alongOneDirection', fctName);
            str2 = sprintf('"%s" not recognized in method cl_image/private/stats_alongOneDirection', fctName);
            my_warndlg(Lang(str1,str2), 1);
    end
    nbPix(k,:) = sum(~isnan(I), numDim);
end
val = val';
nbPix = nbPix';

if Fig
    if numDim == 2
        str1 = 'Inverser les axes ?';
        str2 = 'Reverse axes ?';
        [Sens, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
        abscisses = this.y(suby);
        if ~isempty(indMinMax)
            for k=1:nbSlides
%                 if numDim == 1
                    ordonnees(:,k) = this.x(subx(indMinMax(:,k))); %#ok<AGROW>
%                 else
%                     ordonnees(:,k) = this.x(subx(indMinMax(:,k))); %#ok<AGROW>
%                 end
            end
        end
    else
        abscisses = this.x(subx);
        if isempty(indMinMax)
            Sens = 0;
        else
            Sens = 3;
            for k=1:nbSlides
                if numDim == 1
                    ordonnees(:,k) = this.y(suby(indMinMax(:,k))); %#ok<AGROW>
                else
                    ordonnees(:,k) = this.x(suby(indMinMax(:,k))); %#ok<AGROW>
                end
            end
        end
    end
    
    coul = 'brkgym';
    
    XLim = compute_XYLim(this.x(subx));
    YLim = compute_XYLim(this.y(suby));
    
    % Astuce to create a number that can be shared for similar plot in
    % order to supperpose results
    fig = sum(double([this.Name this.TagSynchroX this.TagSynchroY 'Horz' TypeCurve])) + Sens + Familly;
    
    if ~isempty(indMinMax)
        fig = sum(double([this.Name this.TagSynchroX this.TagSynchroY 'Horz' TypeCurve])) + Sens + Familly + 1000;
        FigUtils.createSScFigure(fig);
        ha(2) = subplot(2,1,2);
        ha(1) = subplot(2,1,1);
    else
        FigUtils.createSScFigure(fig);
    end
    hold on;
    courbes = findobj(fig, 'Type', 'line');
    nbCourbes = length(courbes);
    for k=1:size(val, 2)
        kc = nbCourbes + k;
        kc = 1 + mod(kc-1, length(coul));
        
        str1 = sprintf('Get on X=[%s] et Y=[%s]', num2str(XLim), num2str(YLim));
        str2 = sprintf('display(''%s'');', str1);
        
        if strcmp(TypeCurve, 'Value')
            if Sens == 1
                h = PlotUtils.createSScPlot(val(:,k), abscisses, coul(kc));
                ylabel([XLabel ' (' this.YUnit ')']);
                xlabel(TypeCurve);
                GCA = get(h, 'Parent');
                if this.YDir == 1
                    set(GCA, 'YDir', 'normal');
                else
                    set(GCA, 'YDir', 'reverse');
                end
            else
                h = PlotUtils.createSScPlot(abscisses, val(:,k), coul(kc));
                xlabel([XLabel ' (' XUnit ')']);
                ylabel(TypeCurve);
            end
            cmenu = uicontextmenu;
            uimenu(cmenu, 'Text', str1);
            set(h, 'ButtonDownFcn', str2)
            axis tight
            grid on;
            title([nameDirection ' ' fctName ' : ' this.Name], 'interpreter', 'none');
        else
            if Sens == 1
                h = PlotUtils.createSScPlot(nbPix(:,k), abscisses, coul(kc));
                ylabel([XLabel ' (' XUnit ')']);
                xlabel(TypeCurve);
                if this.YDir == 1
                    set(GCA, 'YDir', 'normal');
                else
                    set(GCA, 'YDir', 'reverse');
                end
            else
                h = PlotUtils.createSScPlot(abscisses, nbPix(:,k), coul(kc));
                xlabel([XLabel ' (' XUnit ')']);
                ylabel(TypeCurve);
            end
            cmenu = uicontextmenu;
            uimenu(cmenu, 'Text', str1);
            set(h, 'ButtonDownFcn', str2)
            axis tight
            grid on;
            title([Lang('Nb Pixels  : ','Nb of pixels : ') this.Name], 'interpreter', 'none');
        end
        
        if ~isempty(indMinMax)
            if Sens == 1
                PlotUtils.createSScPlot(ha(2), ordonnees, abscisses, [coul(kc) '.']);
                xlabel([XLabel ' (' XUnit ')']);
                ylabel([YLabel ' (' YUnit ')']);
                grid(ha(2), 'on'); hold(ha(2), 'on');
                linkaxes(ha, 'y')
                axes(ha(1)) %#ok<LAXES>
            else
                PlotUtils.createSScPlot(ha(2), abscisses, ordonnees, [coul(kc) '.']);
                xlabel(ha(2), [XLabel ' (' XUnit ')']);
                ylabel(ha(2), [YLabel ' (' YUnit ')']);
                grid(ha(2), 'on'); hold(ha(2), 'on');
                linkaxes(ha, 'x')
                axes(ha(1)) %#ok<LAXES>
            end
        end
    end
end



function Y = quantile(I, numDim, Threshold)

useParallel = get_UseParallelTbx;

switch numDim
    case 1
        N = size(I,2);
        Y = NaN(1,N);
        if useParallel
            parfor (k=1:N, useParallel)
                statsValue = stats(I(:,k), 'Seuil', Threshold);
                Y(k) = statsValue.Quant_x;
            end
        else
            for k=1:N
                statsValue = stats(I(:,k), 'Seuil', Threshold);
                Y(k) = statsValue.Quant_x;
            end
        end
    case 2
        N = size(I,1);
        Y = NaN(N,1);
        if useParallel
            parfor (k=1:N, useParallel)
                statsValue = stats(I(k,:), 'Seuil', Threshold);
                Y(k) = statsValue.Quant_x;
            end
        else
            for k=1:N
                statsValue = stats(I(k,:), 'Seuil', Threshold);
                Y(k) = statsValue.Quant_x;
            end
        end
end
