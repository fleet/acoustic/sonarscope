% Agrandissement provisoire de l'image compl�t�e par des NaN
% pour �viter les effets de flou en p�riph�rie d'image
%
% Syntax
%   obj = processLateralEffect_ImageNWW(this, 'subx', ...)
%
% Input Arguments
%   this     : Une instance de la classe cl_image
%
% Name-Value Pair Arguments
%   subx : Echantillonnage de la premiere dimension
%   suby : Echantillonnage de la deuxieme dimension
%   sizeTileNWW : taille des tuiles � ajouter � l'image d'origine.
%
% Output Arguments
%   obj : Objet de type cl_image r�sultant de l'augmentation
%
% Examples
%     [I, label] = ImageSonar(1);
%     a = cl_image('Image', I, 'Name', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur');
%     subx = 1:a.nbColumns;
%     suby = 1:a.nbRows;
%     sizeTileNWW = 150;
%     objDummy = processLateralEffect_ImageNWW(a, 'subx', subx, 'suby', suby, ...
%                'SizeTileNWW', sizeTileNWW);
%
% See also clc_image clc_image Authors
% Authors : JMA,GLU
% ----------------------------------------------------------------------------

function obj = process_LateralEffect_ImageNWW(this, varargin)

[varargin, subx]           = getPropertyValue(varargin, 'subx', []);
[varargin, suby]           = getPropertyValue(varargin, 'suby', []);
[varargin, nbPixelAjouter] = getPropertyValue(varargin, 'SizeTileNWW', 150); %#ok<ASGLU>

obj = extraction(this, 'subx', subx, 'suby', suby);

% Acces aux donn�es
X = get(obj,'X');
Y = get(obj,'Y');
XStep = get(obj,'XStep');
YStep = get(obj,'YStep');

% Ajout des NaN
alpha = get(obj,'Image');

% Recherche de Valeur Nan sur les colonnes et lignes de d�but
% et de fin.
pas=10;
containsValueCol1 = ~all(all(isnan(alpha(1:end,1:pas))));
containsValueColFin = ~all(all(isnan(alpha(1:end,end-pas+1:end))));
containsValueLigne1 = ~all(all(isnan(alpha(1:pas,1:end))));
containsValueLigneFin = ~all(all(isnan(alpha(end-pas+1:end,1:end))));

[lignes,colonnes] = size(alpha);
% Ajout de zones de NaN � gauche de la zone trait�e.
if containsValueCol1
    p1=NaN(lignes,nbPixelAjouter);
    %                 alpha = [p1,alpha];
    try % Pb de Taille m�moire sur des donn�es parfois grandes
        alpha = [p1,alpha(:,:)];
    catch %#ok<CTCH>
        nbC_alpha = size(alpha,2);
        nbC_p1 = size(p1,2);
        pppp = cl_memmapfile('Size', [size(p1,1) nbC_alpha+nbC_p1], ....
            'Format', class(alpha(1)), 'ValNaN', NaN);
        for i=1:nbC_alpha
            pppp(:,i+nbC_p1) = alpha(:,i);
        end
        alpha=pppp;
    end
    
    signeX = sign(X(end)-X(end-1));
    Xdeb(1:nbPixelAjouter) = X(1)-signeX*(nbPixelAjouter:-1:1)*XStep;
    X = [Xdeb,X];
end

% Ajout de zones de NaN � droite de la zone trait�e.
if containsValueColFin
    p2=NaN(lignes,nbPixelAjouter);
    try % Pb de Taille m�moire sur des donn�es parfois grandes
        alpha = [alpha(:,:),p2];
    catch %#ok<CTCH>
        nbC_alpha = size(alpha,2);
        nbC_p2 = size(p2,2);
        pppp = cl_memmapfile('Size', [size(p2,1) nbC_alpha+nbC_p2], ....
            'Format', class(alpha(1)), 'ValNaN', NaN);
        for i=1:nbC_alpha
            pppp(:,i) = alpha(:,i);
        end
        alpha=pppp;
    end
    
    % Concat�nation des longitudes ajout�es avec celles de  l'image.
    signeX = sign(X(end)-X(end-1));
    Xdeb(1:nbPixelAjouter) = X(end)+signeX*(1:nbPixelAjouter)*XStep;
    X = [X,Xdeb];
end

% Recalcul du nb de colonnes de Alpha (modifi�).
[lignes, colonnes] = size(alpha);

% Ajout de zones de NaN en haut de la zone trait�e.
if containsValueLigne1
    p3=NaN(nbPixelAjouter,colonnes);
    try % Pb de Taille m�moire sur des donn�es parfois grandes
        alpha = [p3;alpha(:,:)];
    catch%#ok<CTCH>
        nbL_alpha = size(alpha,1);
        nbL_p3 = size(p3,1);
        pppp = cl_memmapfile('Size', [nbL_alpha+nbL_p3 size(p3,2)], ....
            'Format', class(alpha(1)), 'ValNaN', NaN);
        for i=1:nbL_alpha
            pppp(i+nbL_p3,:) = alpha(i,:);
        end
        alpha=pppp;
    end
    
    % Concat�nation des longitudes ajout�es avec celles de  l'image.
    signeY = sign(Y(end)-Y(end-1));
    Ydeb(1:nbPixelAjouter) = Y(1)-signeY*(nbPixelAjouter:-1:1)*YStep;
    Y = [Ydeb,Y];
end

[lignes,colonnes] = size(alpha);

% Ajout de zones de NaN en bas de la zone trait�e.
if containsValueLigneFin
    p4=NaN(nbPixelAjouter,colonnes);
    try % Pb de Taille m�moire sur des donn�es parfois grandes
        alpha = [alpha(:,:);p4];
    catch %#ok<CTCH>
        nbL_alpha = size(alpha,1);
        nbL_p4 = size(p4,1);
        pppp = cl_memmapfile('Size', [nbL_alpha+nbL_p4 size(p4,2)], ....
            'Format', class(alpha(1)), 'ValNaN', NaN);
        for i=1:nbL_alpha
            pppp(i,:) = alpha(i,:);
        end
        alpha=pppp;
    end
    
    % Concat�nation des latitudes.
    signeY = sign(Y(end)-Y(end-1));
    Ydeb(1:nbPixelAjouter) = Y(end)+signeY*(1:nbPixelAjouter)*YStep;
    Y = [Y,Ydeb];
end

% Recalcul des nouvelles limites de l'image apr�s ajout de
% zones adjacentes de NaN
XLim = [X(1),X(end)];
YLim = [Y(end),Y(1)];

[obj, flag] = set(obj,'Image',alpha,'X',X,'Y',Y,'XLim',XLim,'YLim',YLim); %#ok<ASGLU>
clear alpha;
clear pppp;
clear X;
