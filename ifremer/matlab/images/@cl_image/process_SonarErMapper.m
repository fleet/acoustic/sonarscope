function repImport = process_SonarErMapper(this, Action, repImport)

% TODO : remplacer ce qui suit par survey_uiSelectFiles(this, varargin)
InitialFileFormat = this.InitialFileFormat;
flagPingXxxx = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if strcmp(InitialFileFormat, 'ErMapper') && flagPingXxxx
    [Selection, flag] = my_questdlg(Lang('Sélection des fichiers', 'Files selection ...'), ...
        Lang('Ce fichier de cette image ci seulement', 'This file only'), ...
        Lang('Choix avec un "browser" de fichiers', 'With a browser'));
    if isempty(Selection) || ~flag
        return
    end
else
    Selection = 0;
end

if Selection == 1
    nomFic = this.InitialFileName;
else
    [flag, nomFic, repImport] = uiSelectFiles('ExtensionFiles', {'.ers'; '.xml'}, 'RepDefaut', repImport, ...
        'ChaineExclue', 'Carto');
    if ~flag
        return
    end
end

%% Processing

switch Action
    case 'Visu'
        Option = {'All of them'; 'Index'; 'Position'; 'Runtime'; 'Attitude'; 'Clock'; 'SoundSpeedProfile'; 'SurfaceSoundSpeed'; 'Height'};
        [flag, rep] = question_SelectASignal(Option);
        if ~flag
            return
        end
        preprocessSonarErMapper(nomFic, 'Option', Option{rep})
        
    case 'NavTime'
    	preprocessSonarErMapper(nomFic, 'Option', 'Position')
        
    case 'NavSignal'
%         Option = {'Height'; 'Heading'; 'Time'; 'Bathy'; 'PingNumber'; 'Immersion'; 'CableOut'; 'Roll'; 'Pitch'; 'Yaw'};
%         [flag, rep] = question_SelectASignal(Option);
%         if ~flag
%             return
%         end
        [ZLimAuto, flag] = my_questdlg(Lang('Valeurs limites du signal', 'Limits of the signal values :'), ...
            'Auto', 'Custom');
        if ~flag
            return
        end
        if ZLimAuto == 1
%             ZLim = [];
        else
            p    = ClParametre('Name', 'ZMax', 'Value', 0);
            p(2) = ClParametre('Name', 'ZMin', 'Value', 0);
            a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
%             ZLim = a.getParamsValue;
%             ZLim = sort(ZLim);
        end
        preprocessSonarErMapper(nomFic, 'Option', 'Position');
%         % Pas encore fait
%         plot_nav_data(cl_ermapper_ers([]), nomFic, Option{rep}, 'Fig', Fig, 'ZLim', ZLim)
%         plot_nav_data(cl_ermapper([]), nomFic, Option{rep}, 'Fig', Fig, 'ZLim', ZLim)
        
    case 'Prepa1'
        [PreProcessSeabedimage, flag] = my_questdlg(Lang('Prétraitement des datagrammes "SeabedImage"', 'Preprocessing "SeabedImage" datagrams ?'));
        if ~flag
            return
        end
        [PreProcessRawRangeBeamAngle, flag] = my_questdlg(Lang('Prétraitement des datagrammes "RawRangeBeamAngle"', 'Preprocessing "RawRangeBeamAngle" datagrams ?'));
        if ~flag
            return
        end
        Carto = [];
        Carto = prepare_lectureAll_step1(nomFic, 'Carto', Carto, ...
            'PreProcessSeabedimage',       PreProcessSeabedimage == 1, ...
            'PreProcessRawRangeBeamAngle', PreProcessRawRangeBeamAngle == 1); %#ok<NASGU>
        
    case 'Prepa2'
        prepare_lectureAll_step2(nomFic)
end
