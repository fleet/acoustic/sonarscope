function process_WaterColumnPlumeDetection(~, ListeFicXmlEchogram, nomDirOut, typeAlgo, FrameDescription, Tag, varargin)

global isUsingParfor %#ok<GVMIS>
isUsingParfor = 0;

[varargin, Display] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

useParallel = get_UseParallelTbx;

if ~iscell(ListeFicXmlEchogram)
    ListeFicXmlEchogram = {ListeFicXmlEchogram};
end
if isempty(ListeFicXmlEchogram)
    return
end
nbFic = length(ListeFicXmlEchogram);

%% Question // tbx

if Display == 0
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Action

str1 = 'Reconnaissance de forme sur �chogrammes WC - Fichiers';
str2 = 'Processing shape recognation on WC echograms - Files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw)
    flag = process_WaterColumnPlumeDetection_unitaire(ListeFicXmlEchogram{k}, ...
        nomDirOut, typeAlgo, FrameDescription, Tag, Display, useParallelHere); %#ok<NASGU>
end
my_close(hw, 'MsgEnd')


function flag = process_WaterColumnPlumeDetection_unitaire(nomFicXmlEchogram, ...
    nomDirOut, typeAlgo, FrameDescription, Tag, Display, useParallelHere, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

%% Lecture des fichiers binaires des signaux

[flag, DataEchogram] = XMLBinUtils.readGrpData(nomFicXmlEchogram, 'Memmapfile', -1);
if ~flag
    return
end
[nomDirRaw, nomFicRaw] = fileparts(nomFicXmlEchogram);
nomDirRaw = fullfile(nomDirRaw, nomFicRaw);

if DataEchogram.Dimensions.nbPings == 0
    str1 = 'Pas de donn�es disponibles';
    str2 = 'No data available';
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Affichage de la bathy

flagDisplay = (Display ~= 0);
if flagDisplay
    Fig = FigUtils.createSScFigure('Name', ' - Water Column display - SonarScope - IFREMER');
    %     ScreenSize = get(0,'ScreenSize');
    %     Position = floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4;
    %     set(Fig, 'Position', Position);
    hAxe(1) = subplot(2,1,1);
    hAxe(2) = subplot(2,1,2);
else
    Fig  = [];
    hAxe = [];
end

%% Boucle sur les pings

nbPings = DataEchogram.Dimensions.nbPings;

%% Question // tbx

str1 = 'Reconnaissance de forme sur �chogrammes WC - Pings';
str2 = 'Processing shape recognation on WC echograms - Pings';
if useParallelHere
    [hw, DQ] = create_parforWaitbar(nbPings, Lang(str1,str2));
    parfor (k=1:nbPings, useParallelHere)
        process_WaterColumnPlumeDetection_ping(DataEchogram, nomDirRaw, k, nbPings, ...
            typeAlgo, FrameDescription, flagDisplay, Fig, hAxe, nomFicRaw, nomDirOut, Tag, ...
            'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
    for k=1:nbPings
        my_waitbar(k, nbPings, hw)
        flag = process_WaterColumnPlumeDetection_ping(DataEchogram, nomDirRaw, k, nbPings, ...
            typeAlgo, FrameDescription, flagDisplay, Fig, hAxe, nomFicRaw, nomDirOut, Tag); %#ok<NASGU>
    end
    my_close(hw, 'MsgEnd')
end

%% Ecriture du .xml : plus que temps d'avoir un nouveau format NetCDF !!!

nomFicXMLOut = fullfile(nomDirOut, [nomFicRaw Tag '.xml']);
DataEchogram.Name = [DataEchogram.Name Tag];
for k=1:length(DataEchogram.Signals)
    [nomDirBin, nomFicBin] = fileparts(DataEchogram.Signals(k).FileName);
    DataEchogram.Signals(k).FileName = fullfile([nomDirBin Tag], [nomFicBin '.bin']);
end

DataEchogram.nomFicAll = DataEchogram.ExtractedFrom;
DataEchogram.nbPings = DataEchogram.Dimensions.nbPings;
DataEchogram = rmfield(DataEchogram, 'Software');
DataEchogram = rmfield(DataEchogram, 'DataTag');
DataEchogram = rmfield(DataEchogram, 'ExtractedFrom');
DataEchogram = rmfield(DataEchogram, 'nomDirExport');
DataEchogram = rmfield(DataEchogram, 'FormatVersion');
DataEchogram = rmfield(DataEchogram, 'Survey');
DataEchogram = rmfield(DataEchogram, 'Vessel');
DataEchogram = rmfield(DataEchogram, 'Sounder');
DataEchogram = rmfield(DataEchogram, 'ChiefScientist');
DataEchogram = rmfield(DataEchogram, 'Dimensions');
DataEchogram = rmfield(DataEchogram, 'Signals');

Unit = ''; % TODO
flag = WC_writeXmlPng(nomFicXMLOut, DataEchogram, [], Unit);



function flag = process_WaterColumnPlumeDetection_ping(DataEchogram, nomDirRaw, k, nbPings, ...
    typeAlgo, FrameDescription, flagDisplay, Fig, hAxe, nomFicRaw, nomDirOut, Tag, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor);

[~, nomSimple] = fileparts(nomDirRaw);
fprintf('Processing ping %5d/%5d of %s\n', k, nbPings, nomSimple);

iPing = DataEchogram.iPing(k);
xBab  = DataEchogram.xBab(k);
xtri  = DataEchogram.xTri(k);
Depth = DataEchogram.Depth(k);

%% Lecture de l'�chogramme

if isfield(DataEchogram, 'Immersion')
    Immer = DataEchogram.Immersion(k);
else
    Immer = 0;
end
if isnan(Immer)
    Immer = 0; % Au cas o�
end

nomSousRepertoire = sprintf('%03d', floor(iPing/100));
nomDir3 = fullfile(nomDirRaw, nomSousRepertoire);
nomFic1 = sprintf('%05d.png', iPing);
nomFic2 = fullfile(nomDir3, nomFic1);
if ~exist(nomFic2, 'file')
    flag = 0;
    return
end

[I, map] = imread(nomFic2); %#ok<ASGLU>
xEIImage = linspace(double(xBab), double(xtri), size(I,2));
z = linspace(double(Immer), double(Depth), size(I,1));

DT = cl_image.indDataType('Reflectivity');
subNaN = (I == 0);
I(subNaN) = 1;
WC = cl_image('Image', I, 'x', xEIImage, 'y', z, 'DataType', DT, 'GeometryType', cl_image.indGeometryType('DepthAcrossDist'));

%% Shape recognation

[flag, a] = shapeRecognationGeometricFrame(WC, typeAlgo, FrameDescription, varargin{:}); % TODO : 
if ~flag
    return
end
J = get_Image(a);
J(subNaN) = 0;

%% Affichage du r�sultat

switch typeAlgo
    case 1 % RMS
        CLim = a.CLim;
%         CLim = [CLim(2)-(CLim(2)-CLim(1))/8 CLim(2)];
    case 2 % Crosscorrelation
        CLim = [-8*1000 0];
end


if flagDisplay
    figure(Fig)
    set(Fig, 'CurrentAxes', hAxe(1))
    Title = sprintf('%s - %d/%d - Ping=%d', nomFicRaw, k, nbPings, iPing);
    imagesc(xEIImage, z, I); colormap(jet(256)); colorbar
    axis xy; axis equal; axis tight; title(Title, 'Interpreter', 'none');
    set(Fig, 'CurrentAxes', hAxe(2))
    Title = sprintf('%s - %d/%d - Ping=%d', nomFicRaw, k, nbPings, iPing);
    imagesc(xEIImage, z, J, CLim); colorbar
    axis xy; axis equal; axis tight; title(Title, 'Interpreter', 'none');
end

%% Export du r�sultat

if ~exist(nomDirOut, 'dir')
    status = mkdir(nomDirOut);
    if ~status
        messageErreur(nomDirOut)
        flag = 0;
        return
    end
end

nomDir = fullfile(nomDirOut, [nomFicRaw Tag]);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

nomDir1 = sprintf('%03d', floor(iPing/100));
nomDir = fullfile(nomDir, nomDir1);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

nomFic1 = sprintf('%05d.png', iPing);
nomFic2 = fullfile(nomDir, nomFic1);

[Indexed, flag] = Intensity2Indexed(a, 'CLim', CLim, 'NoStats');
if ~flag
    return
end
Indexed.Image(subNaN) = 255;
flag = export_gif(Indexed, nomFic2);
if ~flag
    return
end

% J = mat2ind(J, CLim, 64);
% imwrite(J, jet, nomFic2) % CLim

%% The end

flag = 1;
