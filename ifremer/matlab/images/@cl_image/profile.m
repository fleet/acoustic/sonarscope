function profile(this, varargin) % ???????????????????????????????????????

[varargin, xy] = getPropertyValue(varargin, 'xy', []);
[varargin, x]  = getPropertyValue(varargin, 'x',  []);
[varargin, y]  = getPropertyValue(varargin, 'y',  []); %#ok<ASGLU>

if ~isempty(y)
    xy = [this.x(1) this.x(end)
        y(1)      y(end)];
end

if ~isempty(x)
    xy = [x(1) x(end)
        this.y(1) this.y(end)];
end

if isempty(xy)
    ym = mean(this.y);
    xy = [this.x(1) this.x(end)
        ym        ym];
end

FigUtils.createSScFigure;
hAxe(1) = subplot(5, 1, 1:4);

imagesc(this, 'Axe', hAxe(1))
hold on;
N = size(xy,2) - 1;
C = get(gca, 'ColorOrder');
for k=1:N
    h = plot(xy(1,k:k+1), xy(2,k:k+1));
    set(h, 'Color', C(1+mod(k, size(C,1)),:), 'LineWidth', 2);
end

colorbar off
colorbar;
colorbar('location', 'NorthOutside')

hAxe(2) = subplot(5, 1, 5); %#ok<NASGU>
grid on;
xlabel('Distance (deg)');
ylabel('Depth');
title('Coupe 1')
D = 0;
for k=1:N
    [val, d, dtotal] = get_profile_xy(this, xy(:,k), xy(:,k+1), 0);
    h = plot(D+d{1}, val{1});
    D = D + dtotal;
    hold on
    set(h, 'Color', C(1+mod(k, size(C,1)),:), 'LineWidth', 2);
end
axis tight
