function that = quadtree(this, threshold, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('quadtree'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = quadtree_unit(this(k), threshold, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = quadtree_unit(this, threshold, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Conversion éventuelle en image d'Intensité

if this.ImageType == 2 % RGB
    this = RGB2Intensity(this);
elseif this.ImageType == 3 % Indexed
    this = Indexed2Intensity(this);
end

%% Algorithm

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    
    sz = size(I);
    szNew = 2 .^ nextpow2(sz);
    I = padarray(I, szNew - sz, 'symmetric', 'post');
    
    S = qtdecomp(I, threshold);
    S = full(S);
    
    blocks = repmat(uint8(0), size(S));
    for dim = [512 256 128 64 32 16 8 4 2 1]
        numblocks = length(find(S == dim));
        if numblocks > 0
            values = repmat(uint8(1), [dim dim numblocks]);
            values(2:dim,2:dim,:) = 0;
            blocks = qtsetblk(blocks, S, dim, values);
        end
    end
    blocks(end,1:end) = 1;
    blocks(1:end,end) = 1;

    Image(:,:,k) = blocks(1:sz(1), 1:sz(2)); 
end

%% Output image

Parameters = getHistoryParameters(threshold);
that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', 'quadtree', 'Parameters', Parameters);
