% Quantification of an image
%
% Syntax
%   b = quantify(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   N      : Number of linspaced values spreded on CLim (Default : 255)
%   CLim   : Limits of data (Default : CLim of the image)
%   Limits : User limits
%   subx   : Sub-sampling in abscissa
%   suby   : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Remarks : Only "N" or "Limits" can be given
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = quantify(a, 'N', 8);
%   imagesc(b);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = quantify(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('quantify'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = quantify_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = quantify_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, N]      = getPropertyValue(varargin, 'N',      255);
[varargin, CLim]   = getPropertyValue(varargin, 'CLim',   this.CLim);
[varargin, Limits] = getPropertyValue(varargin, 'Limits', []); %#ok<ASGLU>

if isempty(Limits)
    Append = sprintf('Quantify%d', N);
    flagIso = true;
else
    Append = 'Quantify';
    flagIso = false;
end

%% Algorithm

rangeOut = [1 N];

for k=this.nbSlides:-1:1
    I = this.Image(suby, subx, k);
    I = singleUnlessDouble(I, this.ValNaN);
    if ~isnan(this.ValNaN)
        I(I == this.ValNaN) = NaN;
    end
    
    if flagIso
        Image(:,:,k) = quantify(I, 'rangeIn', CLim, 'N', N, 'rangeOut', rangeOut);
    else
        Image(:,:,k) = quantify_byBins(I, Limits);
    end
end
this.ValNaN = NaN;

%% Output image

that = inherit(this, Image,'subx',  subx, 'suby', suby, 'AppendName', Append);
