function val = quantiles_lig(this, varargin)

[varargin, Fig] = getFlag(varargin, 'Fig');
val = stats_alongOneDirection(this, 'quantile', 2, Fig | (nargout == 0), varargin{:});
