% TODO : a priori pas utilis�e

function [flag, SameReflectivityStatus] = question_ALLSameReflectivityStatus(this, varargin)

% [varargin, QL] = getPropertyValue(varargin, 'QL', 3);

flag = 1;
SameReflectivityStatus = false;

if testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', 'PingXxxx', 'noMessage')
    [flag, SameReflectivityStatus] = question_SameReflectivityStatus(varargin{:});
end
