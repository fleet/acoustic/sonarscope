%% Along distance interpolation method

function [flag, AlongInterpolation] = question_AlongDistanceInterpolation(this, varargin)

[varargin, QL] = getPropertyValue(varargin, 'QL', 3); %#ok<ASGLU>

flag = 1;
if get_LevelQuestion >= QL
    str1 = 'Interpolation longitudinale';
    str2 = 'Along distance Interpolation';
    [AlongInterpolation, flag] = my_listdlg(Lang(str1,str2), {Lang('Aucune','None'); Lang('Lin�raire','Linear'); Lang('Weighted (pas encore au point)','TODO')}, 'SelectionMode', 'Single', 'InitialValue', 1, 'ColorLevel', 3);
    if ~flag
        return
    end
else
    SonarDescription = get_SonarDescription(this);
    if isempty(SonarDescription)
        AlongInterpolation = 1; % En principe jamais atteint
    else
        SonarFamily = get(SonarDescription, 'Sonar.Family');
        if SonarFamily == 1 % Sonar Lat�ral
            SonarName = get(SonarDescription, 'Sonar.Name');
            if strcmp(SonarName, 'GeoSwath+') || strcmp(SonarName, 'GeoSwath')
                AlongInterpolation = 1;
            else
                AlongInterpolation = 2;
            end
        else % MBES
            AlongInterpolation = 1;
        end
    end
end
