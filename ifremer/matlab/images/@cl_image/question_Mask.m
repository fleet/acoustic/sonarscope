function [flag, creationLayer, nomLayer, indLayerMask] = question_Mask(this, indImage)

creationLayer = 0;
nomLayer      = '';
indLayerMask  = [];
           
identMasque = cl_image.indDataType('Mask');
[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage, 'IncludeCurrentImage', 'TypeDonneUniquement', identMasque);
if ~flag
    return
end

if isempty(indLayers) % Pas de layer "Masque" trouv�
    creationLayer = 1;
else    % au moins 1 layer "Masque" trouv�
    str1 = 'Choix du masque';
    str2 = 'Mask choice';
    [choix, flag] = my_listdlg(Lang(str1,str2), [Lang('Nouveau Masque','New mask');  nomsLayers], 'SelectionMode', 'Single');
    if flag
        if choix == 1
            creationLayer = 1;
        else
            creationLayer = 0;
            indLayerMask = indLayers(choix-1);
        end
    else
        return
    end
end
if creationLayer
    options.Resize = 'on';
    nomLayer = inputdlg(Lang('Nom du masque','Mask name'), 'IFREMER', 1, {Lang('Masque ','Mask')}, options);
    if isempty(nomLayer)
        flag = 0;
        return
    else
        nomLayer = nomLayer{1};
    end
end
