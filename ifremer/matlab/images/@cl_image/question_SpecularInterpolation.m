%% Specular interpolation method

function [flag, SpecularInterpolation] = question_SpecularInterpolation(this) %, varargin)

% [varargin, QL] = getPropertyValue(varargin, 'QL', 3); %#ok<ASGLU>

if isempty(this)
    [flag, SpecularInterpolation] = askQuestion;
    if ~flag
        return
    end
    return
end

SonarDescription = get_SonarDescription(this);
if isempty(SonarDescription)
    SpecularInterpolation = 0; % En principe jamais atteint
else
    SonarFamily = get(SonarDescription, 'Sonar.Family');
    if SonarFamily == 1 % Sonar Lat�ral
        SonarName = get(SonarDescription, 'Sonar.Name');
        if strcmp(SonarName, 'GeoSwath+') || strcmp(SonarName, 'GeoSwath')
            [flag, SpecularInterpolation] = askQuestion;
            if ~flag
                return
            end
        else
            SpecularInterpolation = 0;
        end
    else % MBES
        SpecularInterpolation = 0;
    end
end
flag = 1;


function [flag, SpecularInterpolation] = askQuestion

str1 = 'Interpolation du sp�culaire';
str2 = 'Specular  Interpolation';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    SpecularInterpolation = [];
    return
end
SpecularInterpolation = (rep == 1);
