% TODO : fonction non termin�e
% Recadrage d'une image
%
% Syntax
%   [flag, b] = recadrage(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   XLim : Limites en abscisses
%   YLim : Limites en ordonn�es
%
% Output Arguments
%   [flag, b] : Une instance de cl_image
%
% Examples
%   b = recadrage(a);
%   imagesc(b);
%
% See also cl_image/extraction Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = recadrage(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, a] = unitaire_recadrage(this(i), varargin{:});
    if ~flag
        that = [];
        return
    end
    that(i) = a; %#ok<AGROW>
end


function [flag, this] = unitaire_recadrage(this, varargin)

[varargin, XLim] = getPropertyValue(varargin, 'XLim', this.XLim);
[varargin, YLim] = getPropertyValue(varargin, 'YLim', this.YLim); %#ok<ASGLU>

flag = 0;

if testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage')
    str1 = 'Ce type d''image ne peut pas �tre trait� par la fonction "recadrage".';
    str2 = 'This type of image cannot be processed by the function "recadrage")';
    my_warndlg(Lang(str1,str2), 1);
    return
end

x = this.x;
y = this.y;

subx = find((x >= XLim(1)) & ((x <= XLim(2))));
suby = find((y >= YLim(1)) & ((y <= YLim(2))));

if this.y(end) < this.y(1)
    suby = fliplr(suby);
end

x = XLim(1):this.XStep:XLim(2);
y = YLim(1):this.YStep:YLim(2);

x = centrage_magnetique(x);
y = centrage_magnetique(y);

nx = length(x);
ny = length(y);
nc = this.nbSlides;
ClassName = get(this, 'ClassName');
switch ClassName
    case {'single'; 'double'}
        J = NaN(ny, nx, nc, ClassName);
    otherwise
        J = zeros(ny, nx, nc, ClassName);
        if this.ValNaN ~= 0
            J(:) = this.ValNaN;
        end
end

subx2 = find((x >= min(this.x(subx))) & (x <= max(this.x(subx))));
suby2 = find((y >= min(this.y(suby))) & (y <= max(this.y(suby))));

I = this.Image(suby, subx, :);
sub1 = 1:min(length(subx2),size(I,2));
sub2 = 1:min(length(suby2),size(I,1));
J(suby2(sub2),subx2(sub1),:) = I(sub2,sub1,:);

%%

this.Image = J;
this.x = x;
this.y = y;

this = majCoordonnees(this, 1:length(x), 1:length(y));

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'recadrage');

flag = 1;
