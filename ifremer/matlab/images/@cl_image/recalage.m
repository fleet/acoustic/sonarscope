% Recherche d'un decalage en X et en Y par une methode simple :
% intercorrelation des sommes des lignes et des colonnes
% Les images doivent etre placees l'une et l'autre au centre de l'ecran.
%
% Syntax
%   b = recalage(a, b)
%
% Input Arguments
%   a : Instance de cl_image
%   b : Instances de cl_image
%
% Output Arguments
%   b : Instances de cl_image
%
% Examples
%   a = cl_image('Image', Lena);
%   b = offset_axis(a, -5, +10);
%   c = recalage(a, b);
%
% See also clc_image Recalage2D Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function b = recalage(this, b, varargin)

[varargin, XLim]   = getPropertyValue(varargin, 'XLim',   this.XLim);
[varargin, YLim]   = getPropertyValue(varargin, 'YLim',   this.YLim);
[varargin, Method] = getPropertyValue(varargin, 'Method', 1); %#ok<ASGLU>

this = extraction(this, 'XLim', XLim, 'YLim', YLim);

for k=1:length(b)
    
    %% Extraction de l'image de Reference
    
    I1 = get_val_xy(this, this.x, this.y);
    
    %% Extraction de l'image a decaller
    
    I2 = get_val_xy(b(k), this.x, this.y);
    
    %% Appel a la fonction d'estimation du decalage
    
    [dL, dC] = register(I1, I2, 'Method', Method, 'ValNanI1', this.ValNaN, 'ValNanI2', b(k).ValNaN);
    %     figure(9999)
    %     subplot(2,2,1); imagesc(I1); grid on
    %     subplot(2,2,2); imagesc(I2); grid on
    
    %% Calcul du decalage en x et en y
    
    dxMetric = dC * this.XStepMetric;
    dyMetric = dL * this.YStepMetric;
    
    dx = dC * this.XStep;
    dy = dL * this.YStep;
    
    str1 = sprintf('Le d�calage trouv� est de %s en x et %s en y  -  dxMetric=%s, dyMetric=%s', ...
        num2str(dx), num2str(dy), num2str(dxMetric), num2str(dyMetric));
    str2 = sprintf('Offset found : %s in x and %s in y  -  dxMetric=%s, dyMetric=%s', ...
        num2str(dx), num2str(dy), num2str(dxMetric), num2str(dyMetric));
    fprintf('%s\n', Lang(str1,str2));
    
    %% Remise � jour des cooordonn�es
    
    dx = dC * b(k).XStep;
    dy = dL * b(k).YStep;
    b(k).x = b(k).x - dx;
    b(k).y = b(k).y - dy;
    b(k).XLim = b(k).XLim - dx;
    b(k).YLim = b(k).YLim - dy;
    
    %     imagesc(b(k))
    %     k
    %
end
