function b = recupVersionAnterieure(this, a)

if isa(a, 'cl_image')
    b = a;
    return
end

N = length(a);
hw = create_waitbar(Lang('Maintenance des images', 'Checking images'), 'N', N);
for i=1:N
    my_waitbar(i, N, hw);
    
    b(i) = this; %#ok<AGROW>
    names = fieldnames(a(i));
    for k=1:length(names)
        b(i).(names{k}) = a(i).(names{k});
    end
end
my_close(hw, 'MsgEnd')
