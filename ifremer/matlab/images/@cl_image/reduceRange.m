% Reduce the values of an image below or/and over given values
%
% Syntax
%   [b, flag] = reduceRange(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   algoMin : 1=set NaN under ValMin, 2=set ValMin under ValMin
%   ValMin  : Min value
%   algoMax : 1=set NaN over ValMin, 2=set ValMin over ValMin
%   ValMax  : Ma value
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%   flag : 0=KO, 01=OK
%
% Examples
%   I = rand(256);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = reduceRange(a, 'algoMin', 1, 'ValMin', 0.24, 'algoMax', 2, 'ValMax', 0.6);
%   imagesc(b);
%
% See also cl_image/round cl_image/ceil Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = reduceRange(this, varargin)

[varargin, ValMin]  = getPropertyValue(varargin, 'ValMin', []);
[varargin, ValMax]  = getPropertyValue(varargin, 'ValMax', []);
[varargin, algoMin] = getPropertyValue(varargin, 'algoMin', 1);
[varargin, algoMax] = getPropertyValue(varargin, 'algoMax', 1);

flag = 0;
that = cl_image.empty;

if isempty(ValMin) && isempty(ValMax)
    str1 = 'Il faut donner au moins un ordre : ValMin ou ValMax.';
    str2 = 'You have to define a value for at least ValMin or ValMax.';
    my_warndlg(Lang(str1,str2), 1);
    that = [];
    flag = 0;
    return
end

N = length(this);
hw = create_waitbar(waitbarMsg('reduceRange'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = reduceRange_unit(this(k), algoMin, algoMax, ValMin, ValMax, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = reduceRange_unit(this, algoMin, algoMax, ValMin, ValMax, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx,:);

if ~isempty(ValMin)
    if algoMin == 1
        I(I < ValMin) = NaN;
    else
        I(I < ValMin) = ValMin;
    end
end

if ~isempty(ValMax)
    if algoMax == 1
        I(I > ValMax) = NaN;
    else
        I(I > ValMax) = ValMax;
    end
end

%% Output image

that = inherit(this, I,'subx',  subx, 'suby', suby, 'AppendName', 'reduceRange');
