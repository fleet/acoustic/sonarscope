% Transforms Energy values of an image to dB
%
% Syntax
%   [b, flag] = reflec_Enr2dB(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', single(I).^2, 'Name', label, 'Unit', 'Enr', 'ColormapIndex', 2);
%   imagesc(a);
%
%   [b, flag] = reflec_Enr2dB(a);
%   imagesc(b);
%
% See also reflec_Enr2dB cl_image/reflec_Amp2dB Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = reflec_Enr2dB(this, varargin)
[that, flag] = process_function_type1(this, @reflec_Enr2dB, 'Unit', 'dB', 'expectedUnit', 'Enr', varargin{:});
