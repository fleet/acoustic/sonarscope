% Transforms dB values of an image to Energy
%
% Syntax
%   [b, flag] = reflec_dB2Enr(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label, 'Unit', 'Amp', 'ColormapIndex', 2);
%   imagesc(a);
%
%   [b, flag] = reflec_dB2Enr(a);
%   imagesc(b);
%
%   [c flag] = reflec_dB2Enr(b);
%   imagesc(c);
%   imagesc(sqrt(c));
%
% See also reflec_dB2Amp cl_image/reflec_dB2Amp Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = reflec_dB2Enr(this, varargin)
[that, flag] = process_function_type1(this, @reflec_dB2Enr, 'Unit', 'Enr', 'expectedUnit', 'dB', varargin{:});
