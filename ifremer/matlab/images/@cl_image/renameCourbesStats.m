% Destruction d'une courbe de statistiques
%
% Syntax
%   renameCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = renameCourbesStats(this, varargin)

if isempty(this.CourbesStatistiques)
    message_NoCurve(this)
    return
end

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques)); %#ok<ASGLU>

if isempty(sub)
    return
end

bilan = this.CourbesStatistiques(sub).bilan{1};
nomCourbe   = bilan.nomZoneEtude;
Commentaire = bilan.commentaire;

if iscell(Commentaire)
    str = [];
    for k=1:length(Commentaire)
        str = [str ' ' Commentaire{k}]; %#ok<AGROW>
    end
    Commentaire = str;
end

promptNomCourbe = Lang('Nom de la courbe', 'Curve Name');
options.Resize = 'on';
prompt = {promptNomCourbe, Lang('Commentaire','Comment')};
str1 = 'Nouveau nom de la courbe';
str2 = 'New name for the curve';
dlgTitle = Lang(str1,str2);
if isempty(Commentaire)
    Commentaire = '.';
end
cmd = inputdlg(prompt, dlgTitle, [1;10], {nomCourbe, Commentaire}, options);
if ~isempty(cmd)
    for k=1:length(bilan)
        bilan(k).nomZoneEtude = cmd{1};
        if ~strcmp(cmd{2}, '.')
            bilan(k).commentaire = cmd{2};
        end
    end
    this.CourbesStatistiques(sub).bilan{1} = bilan;
end
