function this = replaceCourbesStats(this, varargin)

[varargin, Tag]     = getPropertyValue(varargin, 'Tag', []);
[varargin, sub]     = getPropertyValue(varargin, 'sub', 1:length(this.CourbesStatistiques));
[varargin, Offsets] = getPropertyValue(varargin, 'Offsets', []);

if isempty(sub)
    message_NoCurve(this)
    return
end

if sub(1) < 0 % Astuce pour dire les N derni�res courbes
    sub = length(this.CourbesStatistiques) + sub + 1;
end

if ~isempty(Tag)
    for k=1:length(sub)
        if strcmp(this.CourbesStatistiques(sub(k)).bilan{1}(1).Tag, Tag)
            subsub(k) = logical(1); %#ok
        else
            subsub(k) = logical(0); %#ok
        end
    end
    sub = sub(subsub);
end

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if isempty(sub)
    message_NoCurve(this)
    return
end

for k=1:length(sub)
    [flag, CurveOffset] = replaceCourbesStats(this.CourbesStatistiques(sub(k)).bilan, Offsets);
    if flag
        this.CourbesStatistiques(end+1) = this.CourbesStatistiques(sub(k));
        this.CourbesStatistiques(end).bilan = CurveOffset;
    end
end
