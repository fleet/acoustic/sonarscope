function that = replaceImageThroughMask(this, New, Mask, valMask, zone, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('replaceImageThroughMask'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    if length(New) == N
        k2 = k;
    else
        k2 = 1;
    end
    if length(Mask) == N
        k3 = k;
    else
        k3 = 1;
    end
    that(k) = replaceImageThroughMask_unit(this(k), New(k2), Mask(k3), valMask, zone, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = replaceImageThroughMask_unit(this, New, LayerMask, valMask, zone, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

K = mask2OnesOrNaN(LayerMask, valMask, zone, this.x(subx), this.y(suby));
for k=this.nbSlides:-1:1
    I = this.Image(suby, subx, k);
    I = singleUnlessDouble(I, this.ValNaN);
    
    J = extraction(New, 'x', this.x(subx), 'y', this.y(suby));
    J = singleUnlessDouble(J.Image(:,:,k), J.ValNaN);
    
    subNonMasque = find(~isnan(K));
    if ~isempty(LayerMask)
        I(subNonMasque) = J(subNonMasque);
    end
    Image(:,:,k) = I;
end

%% Output image

% Parameters = getHistoryParameters(window, sigma);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'ThroughMask', 'ValNaN', NaN); %, 'Parameters', Parameters);
