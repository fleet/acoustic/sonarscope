function this = replace_Image(this, Image)

Seuil = 2e17;

flag1 = isa(this.Image, 'cl_memmapfile');
flag2 = isa(Image, 'cl_memmapfile');

if flag1 && flag2
    this.Image = Image;
%     ThisFileName = get(this.Image, 'FileName');
%     ThatFileName = get(Image, 'FileName');
    
elseif flag1 && ~flag2
    if get_nbBytes(Image) <= Seuil
        this.Image = Image;
    else
        ThisFileName = my_tempname('.memmapfile');
        this.Image   = [];
        this.Image   = cl_memmapfile('FileName', ThisFileName, 'Value', Image);
    end
    
elseif ~flag1 && flag2
    ThatFileName = get(Image, 'FileName');
    ThisFileName =  my_tempname('.memmapfile');
    status = copyfile(ThatFileName, ThisFileName);
    if status == -1
        my_warndlg('cl_image/replace_Image : erreur', 0);
        this.Image = Image(:,:,:);
    else
        Image = set(Image, 'ChangeFileName', 'FileName', ThisFileName);
        this.Image = Image;
    end

elseif ~flag1 && ~flag2
    if get_nbBytes(Image) <= Seuil
        this.Image = Image;
    else
        ThisFileName = my_tempname('.memmapfile');
        this.Image = cl_memmapfile('FileName', ThisFileName, 'Value', Image);
    end
end

this.Writable = false;
