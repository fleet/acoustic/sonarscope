% Resize an image
%
% Syntax
%   b = resize(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Method :  Interpolation method {'nearest', 'bilinear', 'bicubic'}. (Default : 'bilinear')
%   subx   : Sub-sampling in abscissa index
%   suby   : Sub-sampling in ordinates index
%   or
%   x      : Sub-sampling in abscissa values
%   y      : Sub-sampling in ordinates values
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image('Image', Lena);
%   imagesc(a);
%   a.nbRows
%   a.nbColumns
%
%   b = resize(a, 1.3, 0.7);
%   imagesc(b);
%   b.nbRows
%   b.nbColumns
%
% See also cl_image/downsize Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = resize(this, pasx, pasy, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('resize'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = resize_unit(this(k), pasx, pasy, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = resize_unit(this, pasx, pasy, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Method] = getPropertyValue(varargin, 'Method', 'bilinear'); %#ok<ASGLU>

%% Algorithm

nbRows = floor(length(suby) * pasy);
nbCol = floor(length(subx) * pasx);
for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    I = singleUnlessDouble(I, this.ValNaN);
    Image(:,:,k) = imresize(I, [nbRows nbCol], Method);
end

%% Output image

x = linspace(this.x(subx(1)), this.x(subx(end)), nbCol);
y = linspace(this.y(suby(1)), this.y(suby(end)), nbRows);
Parameters = getHistoryParameters(pasx, pasy);
that = inherit(this, Image, 'x', x, 'y', y, 'AppendName', 'resize', 'Parameters', Parameters);
