% Conversion d'images pseudo-couleur en images vraies couleurs
%
% Syntax
%   b = rgb2ind(a)
%
% Input Arguments 
%   a : Une instance de cl_image
%
% Output Arguments 
%   b : Une instance de cl_image
%
% Examples
%   RGB = imread('flowers.tif');
%   RGB = imread('peppers.png');
%
%   a = cl_image('Image', RGB, 'Name', 'flowers')
%   imagesc(a)
%                 
%   b = rgb2ind(a)
%   imagesc(b)
%
% See also cl_image cl_image/imagesc cl_image/ind2rgb Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = rgb2ind(this)

my_warndlg('MESSAGE POUR JMA : cl_image/rgb2ind : cette fonction ne devrait plus etre utilisee', 1);
for i=1:length(this)
    b = this(i);
    if b.nbSlides == 3
        [I, map]        = rgb2ind(b.Image, 256);
        b.Image         = I;
        b.ColormapIndex = 1;
        b.Colormap      = map;
        b.ImageType     = 3;
    end
    this(i) = b;
end
