% Nearest integer of image
%
% Syntax
%  b = round(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = repmat(-pi:0.1:pi, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a);
%
%   b = round(a);
%   imagesc(b);
%
% See also cl_image/floor cl_image/ceil Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = round(this, varargin)
that = process_function_type1(this, @round, varargin{:});
