% a = cl_image('Image', Lena);
% [bins, flagModif, flag] = saisieBinsHisto(a)

function [bins, flagModif, flag] = saisieBinsHisto(this)

ImageName = cl_image.extract_ImageNameVersion(this.Name);
Titre = cl_image.extract_ImageNameTime(ImageName);

[flag, bins] = paramsSaisieBins(Titre, this.HistoCentralClasses, this.StatValues, this.Unit, ...
    isIntegerVal(this), this.HistoValues);
if ~flag
    flagModif = [];
    return
end

if isequal(bins, this.HistoCentralClasses)
    flagModif = 1;
else
    flagModif = 0;
end

%{
dlg = HistoMinMaxStepDialog('Title', Titre, 'histo', this.HistoCentralClasses, 'StatValues', this.StatValues, 'unit', this.Unit, ...
    'isIntegerVal', isIntegerVal(this), 'histValues', this.HistoValues);
dlg.openDialog();
dlg.getParamsValue
%}

