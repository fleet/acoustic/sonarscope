function [flag, Azimuth] = saisie_Azimuth(this, varargin)

[varargin, Titre] = getPropertyValue(varargin, 'Titre', []);
[varargin, Type]  = getPropertyValue(varargin, 'Type',  1); %#ok<ASGLU> % 1=Gradient, 2=Slope

if isempty(Titre)
    if Type == 1
        str1 = 'Orientation du gradient';
        str2 = 'Oriented gradient';
        Titre = Lang(str1,str2);
    else
        str1 = 'Orientation de la pente';
        str2 = 'Oriented slope';
        Titre = Lang(str1,str2);
    end
end
Azimuth = this.Azimuth;

p = ClParametre('Name', Lang('Azimut', 'Azimuth'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 360, 'Value', Azimuth);
a = StyledParametreDialog('params', p, 'Title', Titre);
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    Azimuth  = [];
    return
end
Azimuth = a.getParamsValue;

