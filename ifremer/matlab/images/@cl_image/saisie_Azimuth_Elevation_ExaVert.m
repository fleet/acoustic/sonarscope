function [this, Azimuth, Elevation, OffsetInterLayer, Height3D, flag] = saisie_Azimuth_Elevation_ExaVert(this, indLayers, rep3D)

Azimuth          = 200;
OffsetInterLayer = 2;
Height3D         = 2;
Elevation        = 20;

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% 

str1 = 'TODO';
str2 = '3D parametres';
p(1) = ClParametre('Name', Lang('Azimut','Azimuth'), ...
    'Unit', 'deg',  'MinValue', 1, 'MaxValue', 360, 'Value', Azimuth);
p(2) = ClParametre('Name', Lang('Elevation','Elevation'), ...
    'Unit', 'deg',  'MinValue', 1, 'MaxValue', 90, 'Value', Elevation); % %d
if (length(indLayers) + double(rep3D)) > 1
    p(end+1) = ClParametre('Name', Lang('TODO','Offset between slices'), ...
    'Unit', 'cm',  'MinValue', 0, 'MaxValue', 100, 'Value', OffsetInterLayer);
end
if rep3D
    p(end+1) = ClParametre('Name', Lang('TODO','Height of 3D image'), ...
    'Unit', 'cm',  'MinValue', 0, 'MaxValue', 100, 'Value', Height3D);
end
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
Azimuth   = val(1);
Elevation = val(2);
if (length(indLayers) + double(rep3D)) > 1
    OffsetInterLayer = val(3);
    if rep3D
        Height3D  = val(4);
    end
else
    if rep3D
        Height3D  = val(3);
    end
end
