function [flag, Azimuth, VertExag] = saisie_Azimuth_et_VertExag(this)

Azimuth  = this.Azimuth;
VertExag = this.VertExag;
% indLayers = [];

flag = testSignature(this, 'ImageType', 1);
if ~flag
    return
end

str1 = 'Coefficient d''ombrage';
str2 = 'Shading coefficient';
p(1) = ClParametre('Name', Lang('Azimut', 'Azimuth'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 360, 'Value', Azimuth);
p(2) = ClParametre('Name', Lang('Coefficient d''ombrage', 'Shading coefficient'), ...
    'Unit', ' ', 'MinValue', 0,  'MaxValue', 5, 'Value', VertExag);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    Azimuth  = [];
    VertExag = [];
    return
end
X = a.getParamsValue;
Azimuth  = X(1);
VertExag = X(2);
