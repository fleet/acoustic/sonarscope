function [flag, nomFic, Azimuths, VertExag, fps, quality, repExport] = saisie_Azimuth_et_VertExagMovie(this, repExport)

VertExag = this.VertExag;
Azimuths = [];
fps      = [];
quality  = [];

nomFic = fullfile(repExport, 'Film.mp4');
[flag, nomFic] = my_uiputfile('*.avi', 'Give a file name', nomFic);
if ~flag
    return
end
[pathname, nomFic] = fileparts(nomFic);
nomFic = fullfile(pathname, [nomFic '.avi']);
repExport = fileparts(nomFic);

str1 = 'Param�tres de la fonction d''ombrage';
str2 = 'Sun illumination parametres';
p(1) = ClParametre('Name',  Lang('Azimut d�but', 'Azimuth begin'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 360, 'Value', 0);
p(2) = ClParametre('Name',  Lang('Azimut fin', 'Azimuth end'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 360, 'Value', 359);
p(3) = ClParametre('Name',  Lang('Azimut pas', 'Azimuth step'), ...
    'Unit', 'deg',  'MinValue', 0, 'MaxValue', 360, 'Value', 10);
p(4) = ClParametre('Name',  Lang('Coefficient d''ombrage', 'Shading coefficient'), ...
    'Unit', ' ',  'MinValue', 0, 'MaxValue', 100, 'Value', VertExag);
p(5) = ClParametre('Name',  Lang('Frames par seconde', 'Frames per second'), ...
    'Unit', 'Hz',  'MinValue', 1, 'MaxValue', 25, 'Value', 2, 'Format', '%d');
p(6) = ClParametre('Name',  Lang('Qualit� de la compression', 'Compression Quality'), ...
    'Unit', '%',  'MinValue', 1, 'MaxValue', 100, 'Value', 75);

a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if flag
    val = a.getParamsValue;
    Azimuths = min(val([1 2])): val(3): max(val([1 2]));
    VertExag = val(4);
    fps      = val(5);
    quality  = val(6);
end
