% Subsampling of an image
%
% Syntax
%   b = sampling(a, ...) 
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples 
%   a = cl_image('Image', Lena, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = sampling(a, 'suby', 120:2:180, 'subx', 130:3:190);
%   imagesc(b);
%
% See also cl_image/downsize cl_image/extraction Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = sampling(this, varargin)
%[that, flag] = process_function_type1(this, [], varargin{:});

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('sampling'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = sampling_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = sampling_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controls

% Faudrait tester si l'�chantillonnage est r�gulier. Si c'est pas le cas alors remplacer
% les x et y par les num�ros de lignes et colonnes et mettre le type de g�om�trie � 1 et
% changer les unit�s 

%% Algorithm

I = this.Image(suby,subx,:);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'sampling');
