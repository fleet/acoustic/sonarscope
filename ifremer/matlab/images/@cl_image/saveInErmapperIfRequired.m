% Sauvegarde d'une instance en fichier ErMapper
%
% Syntax
%    saveInErmapperIfRequired(this, NomDirSave)
%
% Input Arguments
%   a          : Une instance de cl_image
%   NomDirSave : 2cases :
%                           [] : pas de sauvegarde
%                           Nom d'un répertoire : sauvegarde de l'image sous ce répertoire
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% VERSION  : $Id: saveInErmapperIfRequired.m,v 1.1 2005/09/14 08:23:13 augustin Exp augustin $
% ----------------------------------------------------------------------------

function saveInErmapperIfRequired(this, NomDirSave)

if ~isempty(NomDirSave)
    for i=1:length(this)
        this(i) = update_Name(this(i));
        nomFicErs = fullfile(NomDirSave, [this(i).Name '.ers']);
        export_ermapper(this(i), nomFicErs);
    end
end
