% Get the max value with coordinates of an image
%
% Syntax
%   [val, x, y, ix, iy, iCan] = searchMaxValue(a, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   val  : Maximum value
%   x    : Abscissa
%   y    : Ordinate
%   iRow : Row number
%   iCol : Column number
%   iCan : Slice number
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%   [val, x, y, ix, iy, iCan] = searchMaxValue(a)
%
% See also cl_image/max cl_image/searchMinValue cl_image/searchMedianValue Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [ValMax, x, y, ix, iy, iCan] = searchMaxValue(this, varargin)

flag = checkOnlyOneInstance(this, 'searchMaxValue');
if ~flag
    % This will provoque an error but it is assumed.
    return
end

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask', []); %#ok<ASGLU>

%% Algorithm

if ~isempty(LayerMask)
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
    K = zeros(size(Masque), 'uint8');
    for ik=1:length(valMask)
        %             sub = find(Masque == valMask(ik));
        sub = (Masque == valMask(ik));
        K(sub) = 1;
    end
    subNonMasque = (K == 0);
end

ix = [];
ValMax = -Inf;
for k=1:this.nbSlides
    for i=1:length(suby)
        Val = this.Image(suby(i),subx,k);
        if ~isa(Val, 'double')
            Val = double(Val);
            Val(Val == this.ValNaN) = NaN;
        end
        
        if ~isempty(LayerMask)
            Val(subNonMasque(i,:)) = NaN;
        end
        
        [V, j] = max(Val);
        if V > ValMax
            ValMax = V;
            iy = suby(i);
            ix = subx(j);
            iCan = k;
        end
    end
end
if isempty(ix)
    ValMax = [];
    x = [];
    y = [];
    iy = [];
    ix = [];
    iCan = [];
    return
end
x = this.x(ix);
y = this.y(iy);
