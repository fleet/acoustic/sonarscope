% Get the median value with coordinates of an image
%
% Syntax
%   [val, x, y, iRow, iCol, iCan] = searchMedianValue(a, ...)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   val  : Median value
%   x    : Abscissa
%   y    : Ordinate
%   iRow : Row number
%   iCol : Column number
%   iCan : Slice number
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%   [val, x, y, iRow, iCol, iCan] = searchMedianValue(a)
%
% See also cl_image/median cl_image/searchMinValue cl_image/searchMaxValue Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, x, y, iRow, iCol, iCan] = searchMedianValue(this, varargin)

flag = checkOnlyOneInstance(this, 'searchMedianValue');
if ~flag
    % This will provoque an error but it is assumed.
    return
end

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

for k=1:this.nbSlides
    I = this.Image(suby,subx,k);
    I = I(:);
    if isnan(this.ValNaN)
        I = I(~isnan(I));
    else
        I = I(I ~= this.ValNaN);
    end
    if isempty(I)
        val = [];
        x = [];
        y = [];
        iRow = [];
        iCol = [];
        iCan = [];
        return
    end
    I = sort(I);
    val= I(floor(length(I)/2));
    [iRow, iCol] = find(this.Image(suby,subx,k) == val, 1, 'first');
    iCan = k;
end
x = this.x(iCol);
y = this.y(iRow);
