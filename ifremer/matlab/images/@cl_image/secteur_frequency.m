function this = secteur_frequency(this, varargin)

N = length(this);
str1 = 'Calcul des images "Frequency"';
str2 = 'Computing images "Frequency"';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    this(k) = unitaire_secteur_frequency(this(k), varargin{:});
end
my_close(hw)


function this = unitaire_secteur_frequency(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contr�les

identTxBeamIndex = cl_image.indDataType('TxBeamIndex');

flag = testSignature(this, 'DataType', identTxBeamIndex);
if ~flag
    this = [];
    return
end

SonarIdent = get(this.Sonar.Desciption, 'Sonar.Ident');
if SonarIdent == 0
    this = [];
    my_warndlg('Pas de sonar defini', 1);
    return
end

%% Traitement

I = NaN(length(suby), length(subx), 'single');

SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
switch SonarName
    case 'EM3002D'
        N = this.nbColumns;
        sub = (subx <= (N/2));
        I(:,sub) = NaN; % TODO : trouver la valeur
        sub = (subx > (N/2));
        I(:,sub) = NaN; % TODO : trouver la valeur
        
    case 'EM3000D'
        N = this.nbColumns;
        sub = (subx <= (N/2));
        I(:,sub) = NaN; % TODO : trouver la valeur
        sub = (subx > (N/2));
        I(:,sub) = NaN; % TODO : trouver la valeur
        
    case 'EM1002'
        TxBeamIndex = this.Image(suby,subx);
        I(TxBeamIndex == 1) = 93.37;
        I(TxBeamIndex == 2) = 98.328;
        I(TxBeamIndex == 3) = 93.37;
        
    otherwise % Th�oriquement on ne devrait pas passer par l� pour les sondeur de deuxi�me et troisi�me g�n�ration
        
        
        
        
        
        
        
        
        
        
        I(:) = NaN; % TODO
end

this.Image  = I;
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

ResolutionD = this.Sonar.ResolutionD;
ResolutionX = this.Sonar.ResolutionX;
this = majCoordonnees(this, subx, suby);
this.Sonar.ResolutionD = ResolutionD;
this.Sonar.ResolutionX = ResolutionX;

%% Calcul des statistiques

[this, flag] = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
if flag
    this.CLim = [this.StatValues.Min this.StatValues.Max];
end
this.Unit          = 'kHz';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Frequency');
this = update_Name(this);
