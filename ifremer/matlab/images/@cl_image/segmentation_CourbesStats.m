function [flag, a] = segmentation_CourbesStats(this, indImage, subCurves, indLayerReflectivity, indLayerAngle, indLayerMask, valMask, w, ...
    StopRegul, UseModelBS, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

% stdMax = Inf; % Test� � 3.5 dB mais �a ne change rien au r�sultat

w = ceil(w/2);

flag = 0;
a = cl_image.empty;

% TODO ; cette fonction est �crite � l'�conomie. Pour bien faire; il faut
% r�cup�rer les types de donn�es conditionnelles dans les fichier de stats.

% TODO : g�rer les subx et suby

% TODO : attention, pour l'instant on ne travaille qu'avec un seul layer
% conditionnel

%% Courbes statistiques

nbCurves = length(subCurves);
if nbCurves == 0
    return
end
for k1=1:nbCurves
    bilan(k1) = this(indImage).CourbesStatistiques(subCurves(k1)).bilan; %#ok<AGROW>
end

%% Contr�le si on est bien sur le m�me type de donn�es

DTImage = get_DataTypeName(this(indImage));
if ~strcmp(DTImage, bilan{1}(1).DataTypeValue)
    str1 = sprintf('ImageType attendu : %s\nImageType de l''image courante : %s', bilan{1}(1).DataTypeValue, DTImage);
    str2 = sprintf('ImageType expected : %s\nImageType of current image : %s', bilan{1}(1).DataTypeValue, DTImage);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Pr�paration des courbes

x = [];
for k1=1:length(bilan)
    for k2=1:length(bilan{k1})
        x = [x bilan{k1}(k2).x]; %#ok<AGROW>
    end
end
x = [ -90 unique(x) 90];

MeanCompType = bilan{1}.MeanCompType;
nx = length(x);
% nc = length(bilan{1});
nc = nbCurves;
m = NaN(nc, nx);
s = NaN(nc, nx);
for k1=1:nc
    for k2=1:length(bilan{k1}) % TODO : en principe toujours de dimension 1 !!!
        x1 = bilan{k1}(k2).x;
        
        if UseModelBS && isfield(bilan{k1}(k2), 'model') && ~isempty(bilan{k1}(k2).model)
            MeanReflec = bilan{k1}(k2).model.compute('x', x1);
        else
            MeanReflec = bilan{k1}(k2).y(:);
        end
                        
        switch MeanCompType
            case 1 % Courbes calcul�es directement en dB
                y1 = bilan{k1}(k2).y;
                y2 = bilan{k1}(k2).std;
            case 2 % Courbes calcul�es directement en Amplitude
                m1 = reflec_dB2Amp(MeanReflec - bilan{k1}(k2).std(:));
                m2 = reflec_dB2Amp(MeanReflec + bilan{k1}(k2).std(:));
                y2 = (m2 - m1) / 2;
                y1 = reflec_dB2Amp(MeanReflec);
%                 figure; plot(y1); grid on; hold on; plot(y1+y2, '--'); plot(y1-y2, '--');
            case 3 % Courbes calcul�es directement en Energie
                m1 = reflec_dB2Enr(MeanReflec - bilan{k1}(k2).std(:));
                m2 = reflec_dB2Enr(MeanReflec + bilan{k1}(k2).std(:));
                y2 = (m2 - m1) / 2;
                y1 = reflec_dB2Enr(MeanReflec);
%                 figure; plot(y1); grid on; hold on; plot(y1+y2, '--'); plot(y1-y2, '--');
            otherwise
                % TODO ?
        end
        
        subNonNaN = ~isnan(bilan{k1}(k2).y);
        x1 = x1(subNonNaN);
        y1 = y1(subNonNaN);
        y2 = y2(subNonNaN);
        x1 = [-90 x1 90]; %#ok<AGROW>
        y1 = [y1(1); y1(:); y1(end)];
        y2 = [y2(1); y2(:); y2(end)];
        
        k3 = max(k1,k2); % Pour prendre en charge soit m courbes de 1 elements ou 1 courbe de N �l�ments
        m(k3,:) = my_interp1(x1, y1, x, 'linear');
        s(k3,:) = my_interp1(x1, y2, x, 'linear');
        m(k3,:) = my_interp1(x, m(k3,:), x, 'nearest', 'extrap');
        s(k3,:) = my_interp1(x, s(k3,:), x, 'nearest', 'extrap');
%         s(k3,:) = min(s(k3,:), stdMax);
    end
end
%{
FigUtils.createSScFigure;
h = PlotUtils.createSScPlot(x, m); grid on
set(h, 'LineWidth', 3); hold on
PlotUtils.createSScPlot(x, m-s, '--');
PlotUtils.createSScPlot(x, m+s, '--'); grid on
%}

%% Traitement des images

str1 = 'Segmentation des images par BS angulaire';
str2 = 'BS angular Segmenting images';
N = length(indLayerReflectivity);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, b] = segmentation_CourbesStats_unitaire(this(indLayerReflectivity(k)), this(indLayerAngle(k)), x, m, s, w, StopRegul, ...
        'Mask', this(indLayerMask), 'valMask', valMask, 'subx',  subx, 'suby', suby, 'MeanCompType', MeanCompType);
    if flag
        a(k) = b;
        % pppp = SonarScope([this(indLayerReflectivity(k)), this(indLayerAngle(k)), b])
        % a(k) = optimiseMemory(a(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd')
