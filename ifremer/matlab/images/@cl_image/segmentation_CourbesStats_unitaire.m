function [flag, this] = segmentation_CourbesStats_unitaire(this, Angle, x, m, s, w, StopRegul, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, MeanCompType] = getPropertyValue(varargin, 'MeanCompType', 1); %#ok<ASGLU>

% [varargin, Mask]    = getPropertyValue(varargin, 'Mask',    []);
% [varargin, valMask] = getPropertyValue(varargin, 'valMask', []);

%% Calcul du terme de rappel aux donn�es

[~, ~, subx1, subx2] = lin_intersect(this.x(subx), Angle.x);
[~, ~, suby1, suby2] = lin_intersect(this.y(suby), Angle.y);
subxI = subx(subx1);
subyI = suby(suby1);
subxA = subx2;
subyA = suby2;

I = this.Image(subyI, subxI);

switch MeanCompType
    case 2 % En Amplitude
        I = reflec_dB2Amp(I);
    case 3 % En Energie
        I = reflec_dB2Enr(I);
end

A = Angle.Image(subyA, subxA);

%% Calcul de la segmentation

IA(:,:,2) = A;
IA(:,:,1) = I;
clear A I

BlockSize = 512;
flagUseParallelTbx = ((numel(IA)) > ((2*BlockSize)^2)); % Si taille A > 4 blocks. Le lancement du "Starting parallel" prend du temps :
if flagUseParallelTbx
    fun = @(block_struct) samantha(block_struct.data, x, m, s, w, StopRegul);
    J = blockproc(IA, [BlockSize BlockSize], fun, 'BorderSize', w+1, 'UseParallel', true);
else
    J = samantha(IA, x, m, s, w, StopRegul);
end
clear IA
% SonarScope(J)

%% Envoi du r�sultat dans l'instance

this = replace_Image(this, J);

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subxI, subyI);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.DataType           = cl_image.indDataType('Segmentation');
this.TagSynchroContrast = num2str(rand(1));
this.ColormapIndex      = 3;
this.CLim               = [-0.5 this.StatValues.Max+0.5];
this.ImageType          = 3; % Image index�e
this.ColormapCustom     = jet(this.StatValues.Max+1);
this.ColormapIndex      = 1;
this.Unit               = '';
this.ValNaN             = 0;

%% Compl�tion du titre

this = update_Name(this);
flag = 1;


function J = samantha(IA, x, m, s, w, StopRegul)

%{
    FigUtils.createSScFigure;
    h = PlotUtils.createSScPlot(x, m); grid on
    set(h, 'LineWidth', 3); hold on
    PlotUtils.createSScPlot(x, m-s, '--');
    PlotUtils.createSScPlot(x, m+s, '--'); grid on

    FigUtils.createSScFigure;
    YLim = [-40 -10];
    for kc=1:nc
        h(kc) = subplot(nc,1,kc); PlotUtils.createSScPlot(x, m(kc,:)); grid on;
    end
    linkaxes(h, 'xy')
%}

I = IA(:,:,1);
A = IA(:,:,2);
clear IA

n1 = size(I,1);
n2 = size(I,2);
nc = size(m,1);

w1 = w(1);
w2 = w(2);

nbPixelsInformes = sum(~isnan(I(:)));
if nbPixelsInformes == 0
    J = zeros(n1,n2);
    % fprintf('Image vide trouv�� ici \n');
    return
end

valCourbe = NaN([size(A) nc]);
stdCourbe = NaN([size(A) nc]);
U1        = NaN([size(A) nc]);
for kc=1:nc % On cr�e des matrices plut�t que reinterpoler en permanence
    valCourbe(:,:,kc) = my_interp1(x, m(kc,:), A);
    stdCourbe(:,:,kc) = my_interp1(x, s(kc,:), A);
end

%{
    figure;
    CLim = [min(stdCourbe(:)) max(stdCourbe(:))];
    for kc=1:nc
        h(kc) = subplot(nc,1,kc); imagesc(stdCourbe(:,:,kc), CLim); colormap(jet(256)); colorbar
    end
    linkaxes(h, 'xy')

    figure;
    CLim = [min(valCourbe(:)) max(valCourbe(:))];
    for kc=1:nc
        h(kc) = subplot(nc,1,kc); imagesc(valCourbe(:,:,kc), CLim); colormap(jet(256)); colorbar
    end
    linkaxes(h, 'xy')

    figure;
    CLim = [-100 100];
    for kc=1:nc
        h(kc) = subplot(nc,1,kc); imagesc((I - valCourbe(:,:,kc)) .^ 2, CLim); colormap(jet(256)); colorbar
    end
    linkaxes(h, 'xy')
%}

%% Pr�calcul du mask o� la segmentation peut �tre faite, �a �vite de refaire ce test � toutes les it�rations

MaskNoProcessing = isnan(I);

%% Terme de rappel aux donn�es

for k1=(w1+1):(n1-w1)
    suby2 = (k1-w1):(k1+w1);
    for k2=(w2+1):(n2-w2)
        if MaskNoProcessing(k1,k2)
            continue
        end
        subx2 = (k2-w2):(k2+w2);
        
        ValueV = I(suby2,subx2);
        AngleV = A(suby2,subx2);
        sub = (~isnan(ValueV) & ~isnan(AngleV));
        ValueV = ValueV(sub);
        
        U1V = NaN(1,nc);
        for kc=1:nc
            valCourbeV = valCourbe(suby2, subx2, kc);
            stdCourbeV = stdCourbe(suby2, subx2, kc);
            valCourbeV = valCourbeV(sub);
            stdCourbeV = stdCourbeV(sub);
            trd = (ValueV - valCourbeV) ./ (sqrt(2) * stdCourbeV);
            u = (trd .* trd) + log(stdCourbeV);
            U1V(kc) = sum(u);
        end
        U1(k1,k2,:) = U1V;
        %{
            figure;
            CLim = [0 mean(U1(:), 'omitnan')];
            for kc=1:nc
                h(kc) = subplot(nc,1,kc); imagesc(U1(:,:,kc), CLim); colormap(jet(256)); colorbar
            end
            linkaxes(h, 'xy')
        %}
    end
end
clear valCourbe stdCourbe

U1(:,1:1+2*w1)     = NaN;
U1(:,end-(0:2*w1)) = NaN;
U1(1:1+2*w1,:)     = NaN;
U1(end-(0:2*w1),:) = NaN;

[~, J] = min(U1, [], 3);
subNaN = isnan(sum(U1,3));
J(subNaN) = 0;
% SonarScope(J)
% profile report

%% R�gularisation

U2 = NaN([size(A) nc]);
U3 = NaN([size(A) nc]);
kIter = 0;
Beta  = 1;
while 1
    kIter = kIter + 1;
    
    for k1=(w1+1):(n1-w1)
        suby2 = (k1-w1):(k1+w1);
        for k2=(w2+1):(n2-w2)
            if MaskNoProcessing(k1,k2)
                continue
            end
            subx2 = (k2-w2):(k2+w2);
            
            U1V = U1(suby2,subx2, 1);
            sub = ~isnan(U1V);
            
            JV = J(suby2,subx2);
            JV = JV(sub);
            for kc=1:nc
                U2V = sum(JV == kc);
                U2(k1,k2,kc) = U2V;
            end
        end
    end
    %{
        figure;
        CLim = [0 max(U2(:), [], 'omitnan')];
        for kc=1:nc
            h(kc) = subplot(nc,1,kc); imagesc(U2(:,:,kc), CLim); colormap(jet(256)); colorbar
        end
        linkaxes(h, 'xy')
    %}
    for kc=1:nc
        U3(:,:,kc) = U1(:,:,kc) - (Beta * U2(:,:,kc));
    end
    %{
        figure;
        CLim = [ min(U3(:), [], 'omitnan') max(U3(:), [], 'omitnan')];
        for kc=1:nc
            h(kc) = subplot(nc,1,kc); imagesc(U3(:,:,kc), CLim); colormap(jet(256)); colorbar
        end
        linkaxes(h, 'xy')
    
        figure;
        k3 = 0
        nanM = max(U3(:), [], 'omitnan') - min(U3(:), [], 'omitnan');
        CLim = [0 nanM/10];
        for kc1=1:nc
            for kc2=1:nc
                if kc2 > kc1
                    k3 = k3 + 1;
                    h12(k3) = subplot(nc-1,nc-1, (kc1-1)*(nc-1) + (kc2-1)); imagesc(abs(U3(:,:,kc1) - U3(:,:,kc2)), CLim); colormap(jet(256)); colorbar
                end
            end
        end
        linkaxes(h12, 'xy')
    %}
    JPrec = J;
    [~, J] = min(U3, [], 3);
    J(subNaN) = 0;
    
    % TODO : r�cup�rer la deuxi�me valeur la plus petite et comparer les
    % valeurs avec la premi�re. Si les deux valeurs sont proches, d�clarer
    % le pixel inclassable.
    
    nbPixelsMutants = (sum(J(:) ~= JPrec(:)));
    PourcentagePixelsMutants = (nbPixelsMutants / nbPixelsInformes) * 100;
    fprintf('Iteration %3d Nb fliping pixels %6d  %7.3f%%  will stop at %f%%\n', ...
        kIter, nbPixelsMutants, PourcentagePixelsMutants, StopRegul)
    if (PourcentagePixelsMutants < StopRegul) %&& (kIter > 10)
        break
    end
    Beta = Beta + 1;
end
J = uint8(J);
