% Transforms a seismic image (oscillating signal) to an envelope representation
%
% Syntax
%   [flag, b] = seismicEnvelope(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance(s) of cl_image
%
% Examples
%   TODO : trouver une image d'exemple et la mettre dans SonarScopeData
%   Filename = 'D:\Temp\data_louis2013\lineAB.seg';
%   [flag, a] = import_SEGY(cl_image, Filename);
%   imagesc(a);
%
%   [flag, b] = seismicEnvelope(a);
%   imagesc(b);
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = seismicEnvelope(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('seismicEnvelope'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [K, flag] = seismicEnvelope_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [that, flag] = seismicEnvelope_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

that = [];

%% Controls

flag = isSubbottom(this);
if ~flag
    return
end

%% Algorithm

I = this.Image(suby, subx);
J = NaN(size(I), 'single');
XData = this.x(subx);
for k=1:length(suby)
    YData = double(I(k,:));
    sub = ~isnan(YData);
    if sum(sub) > 10 % 10 pris au hasard. Quelle valeur faut-il ?
        YData = YData(sub);
        p = polyfit(XData(sub), YData, 1);
        YDataFitted = polyval(p, XData(sub));
        YData = YData - YDataFitted;
        Y = hilbert(YData);
        E = abs(Y);
        J(k,sub) = YDataFitted + E;
    end
end
        
%% Output image

that = inherit(this, J, 'subx', subx, 'suby', suby, 'AppendName', 'seismicEnvelope');
