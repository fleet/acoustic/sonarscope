% Projects a seismic image on a plan (the image mus be "Constant immersion" one)
%
% Syntax
%   [flag, b] = seismicProjectionOnAPlan(a, Lat0, Lon0, Azimuth, ...)
%
% Input Arguments
%   a       : Instance(s) of cl_image
%   Lat0    : Latitude of the reference point
%   Lon0    : Latitude of the reference point
%   Azimuth : Azimuth of the plan
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance(s) of cl_image
%
% Examples
%   % TODO : trouver une image d'exemple et la mettre dans SonarScopeData
%   Filename = 'D:\Temp\data_louis2013\lineAB.seg';
%   Filename = 'D:\Temp\data_louis2013\Pour Thibault\Line2-3Depth-Migr2.seg';
%   [flag, a] = import_SEGY(cl_image, Filename);
%   imagesc(a);
%   [flag, a] = subbottom_suppressWC(a);
%   imagesc(a);
%   [flag, a] = subbottom_ImmersionConstante(a);
%   imagesc(a);
%
%   Lat0    = 40.815;
%   Lon0    = 27.78;
%   Azimuth = 348;
%   resol   = 10;
%   [flag, b] = seismicProjectionOnAPlan(a, Lat0, Lon0, Azimuth, resol);
%   imagesc(b);
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = seismicProjectionOnAPlan(this, Lat0, Lon0, Azimuth, resol, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('seismiccProjectionOnAPlan'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [K, flag] = seismicProjectionOnAPlan_unit(this(k), Lat0, Lon0, Azimuth, resol, varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw)


function [that, flag] = seismicProjectionOnAPlan_unit(this, Lat0, Lon0, Azimuth, resol, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

that = cl_image.empty;

%% Controls

flag = isSubbottom(this, 'GeometryType', cl_image.indGeometryType('PingDepth'));
if ~flag
    return
end

%% Algorithm

c = cosd(Azimuth);
s = sind(Azimuth);

[x, y]   = latlon2xy(this.Carto, this.Sonar.FishLatitude(suby), this.Sonar.FishLongitude(suby));
[x0, y0] = latlon2xy(this.Carto, Lat0, Lon0);
pppp = [c -s; s c] * [(x-x0)'; (y-y0)'];
figure; plot(x, y); grid on
xp = pppp(1,:);
yp = pppp(2,:);
figure; plot(xp, yp); grid on

ypMax = max(yp);
ypMin = min(yp);

yOut = centrage_magnetique(ypMin:resol:ypMax);


pppp = [c s; -s c] * [zeros(size(yOut)); yOut];
figure; plot(zeros(size(yOut)), yOut); grid on
x1 = pppp(1,:) + x0;
y1 = pppp(2,:) + y0;
FigUtils.createSScFigure; PlotUtils.createSScPlot(x1, y1); grid on
[lat1, lon1] = xy2latlon(this.Carto, x1, y1);

FigUtils.createSScFigure; PlotUtils.createSScPlot(this.Sonar.FishLongitude(suby), this.Sonar.FishLatitude(suby)); grid on; hold on;
PlotUtils.createSScPlot(lon1, lat1, 'r')

I = this.Image(suby, subx);
J = NaN(length(yOut), length(subx), 'single');
for k=1:length(suby)
    Val = I(k,:);
    [~, kJ] = min(abs(yOut - yp(k)));
    J(kJ,:) = Val;
end
        
%% Output image

GeometryType = cl_image.indGeometryType('PingDepth');
TagSynchroY = sprintf('Lat0=%f, Lon0=%f, Azimuth=%f', Lat0, Lon0, Azimuth);
that = cl_image('Image', J, 'Unit', this.Unit, ...
    'DataType', this.DataType, 'GeometryType', GeometryType, ...
    'Name', this.Name, ...
    'x', this.x(subx), 'XLabel', this.XLabel, 'XUnit', this.XUnit, ...
    'y', yOut, 'YLabel', 'Dist along proj', 'YUnit', 'm', ...
    'TagSynchroX', this.TagSynchroX, ...
    'TagSynchroY', TagSynchroY, ...
    'ColormapIndex', 2, 'CLim', this.CLim, ...
    'Carto', this.Carto, ...
    'InitialFileName', this.InitialFileName, ...
    'InitialFileFormat', this.InitialFileFormat, ...
    'InitialImageName', this.InitialImageName);
that = WinFillNaN(that, 'window', [5 1]);
that = set(that, 'SonarFishLongitude', lon1);
that = set(that, 'SonarFishLatitude',  lat1);

that = update_Name(that, 'Name', this.InitialImageName, 'Append', 'seismicProjectionOnAPlan');

