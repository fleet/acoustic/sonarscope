function [flag, DataType1, DataType2] = selecCurveDataTypes(this, indImage)

DataType1 = [];
DataType2 = [];

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage); %#ok<ASGLU>
if ~flag
    return
end

if isempty(indLayers)
    str1 = 'Il faut poss�der au moins 2 images syncronis�es en X et en Y pour faire cette op�ration.';
    str2 = 'It is necessary to have 2 synchronized images to do this processing.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Liste des DataType des layers synchronis�s au layer courant

[strChoix, TypeVal, indLayer, DataType, Unit] = get_listeVariablesForStatistics(this, indImage, indLayers, ...
    'noXY', 1, 'flagSignauxVert', 0); %#ok<ASGLU>
DataType = unique(DataType);
DataType(DataType == this(indImage).DataType) = [];
strDataType = cell(length(DataType),1);
for k=1:length(DataType)
    strDataType{k} = cl_image.strDataType{DataType(k)};
end

%% Choix du DataType abscissa

[rep, flag] = my_listdlg('Select the DataType that will be used for the curves abscissa.', strDataType, 'SelectionMode', 'Single');
if ~flag
    return
end
DataType1 = DataType(rep);

%% Statistiques crois�es ?

str1 = 'Statistiques crois�es';
str2 = 'Cross statistics ?';
[choix, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag || (choix == 2)
    return
end

%% Choix du DataType cross-stats

DataType(rep)    = [];
strDataType(rep) = [];
if isempty(strDataType)
    return
end
[rep, flag] = my_listdlg('Select the DataType that will be used for the curves abscissa.', strDataType, 'SelectionMode', 'Single');
if ~flag
    return
end
DataType2 = DataType(rep);

