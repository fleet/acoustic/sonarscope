% TODO : observer la fonction paramsMasque qui fait � peu pr�s la m�me
% chose mais qui vectorise les ROI raster et s�lection multiple ici alors
% que single dans paramsMasque 

function [valMask, Coul] = selectValMask(this, varargin)

[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple'); %#ok<ASGLU>

StatValues = this.StatValues;

Coul = [];

if StatValues.Min == StatValues.Max
    valMask = StatValues.Min;
else
    ValNaN = get(this, 'ValNaN');
%     I = get(this, 'Image');
    
    listeVal = [];
    subx = 1:this.nbColumns;
    for k=1:this.nbRows
        L = get_val_ij(this, k, subx);
        
        L = round(L); % Modifi� le 16/09/2010 Bug trouv� sur masque Julien Moreau
        
        if isnan(ValNaN)
            L = L(~isnan(L));
        else
            L = L(L ~= ValNaN);
        end
        if isempty(listeVal)
            listeVal = unique(L);
        else
            listeVal = [listeVal unique(L)]; %#ok<AGROW>
        end
    end
    listeVal = unique(listeVal);
    Coul = get_coul(this, listeVal);

    for k=1:length(listeVal)
        valMask = listeVal(k);
%         if valMask == 0
            str{k} = num2str(valMask); %#ok
%         else
%             str{k} = num2str(valMask); %#ok
%         end
    end

    InitialValue = find(listeVal ~= 0);
    str1 = 'Valeurs du masque � utiliser';
    str2 = 'Mask values to use';
    [choix, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', InitialValue(1), 'SelectionMode', SelectionMode);
    if flag
        valMask = listeVal(choix);
        Coul = Coul(choix,:);
    end
end
