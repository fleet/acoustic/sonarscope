% Selection de courbes de statistiques conditionnelles
%
% Syntax
%   [flag, sub] = selectionCourbesStats(this)
%
% Input Arguments
%   this : Instance de cl_image
%
% Output Arguments
%   sub : Liste des courbes
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, sub, nbCourbesStatistiques, NomsCourbes, subTypeCurve, Unit] = selectionCourbesStats(this, varargin)

[varargin, SelectionMode]         = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
[varargin, Tag]                   = getPropertyValue(varargin, 'Tag', []);
[varargin, DataTypeValue]         = getPropertyValue(varargin, 'DataTypeValue', []);
[varargin, Title]                 = getPropertyValue(varargin, 'Title', []);
[varargin, NoMessage]             = getPropertyValue(varargin, 'NoMessage', 0);
[varargin, NoManualSelection]     = getPropertyValue(varargin, 'NoManualSelection', 0);
[varargin, SelectionCurveOrModel] = getPropertyValue(varargin, 'SelectionCurveOrModel', 0);
[varargin, LastOne]               = getPropertyValue(varargin, 'LastOne', 0);
[varargin, InitialValue]          = getPropertyValue(varargin, 'InitialValue', []);
[varargin, MayBeNone]             = getPropertyValue(varargin, 'MayBeNone', 0); %#ok<ASGLU>

flag         = 1;
str          = [];
NomsCourbes  = {};
subTypeCurve = 1:2;
Unit         = [];

flagExistenceModel     = [];
subCourbesStatistiques = [];
nbCourbesStatistiques = length(this.CourbesStatistiques);
for k=1:nbCourbesStatistiques
    if isempty(this.CourbesStatistiques(k).bilan)
        continue
    end
    if isempty(Tag)
        if isfield(this.CourbesStatistiques(k).bilan{1}(1), 'Unit')
            Unit = this.CourbesStatistiques(k).bilan{1}(1).Unit;
        else
            Unit = '';
        end
        pppp = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude;
        if isnumeric(pppp)
            pppp = num2str(pppp);
        end
        if isempty(pppp)
            if ~isdeployed
                str1 = 'JMA : Analyser pb dans selectionCourbesStats : il s''agit sans doute d''une courbe dont le filtrage a foir�.';
                str2 = 'TODO';
                my_warndlg(Lang(str1,str2), 1);
            end
            continue
        end
        str{k} = pppp; %#ok<AGROW>
        %         disp('ajouter Tag : nomZoneEtude ici')
        
        if isempty(DataTypeValue)
            subCourbesStatistiques(end+1) = k; %#ok
            flagExistenceModel(end+1) = existModel(this.CourbesStatistiques(k).bilan{1}); %#ok
        else
            if strcmp(this.CourbesStatistiques(k).bilan{1}(1).DataTypeValue, DataTypeValue)
                subCourbesStatistiques(end+1) = k; %#ok
                flagExistenceModel(end+1) = existModel(this.CourbesStatistiques(k).bilan{1}); %#ok
            end
        end
    else
        str{k} = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude; %#ok<AGROW>
        if strcmp(this.CourbesStatistiques(k).bilan{1}(1).Tag, Tag)
            if isempty(DataTypeValue)
                subCourbesStatistiques(end+1) = k; %#ok
                flagExistenceModel(end+1) = existModel(this.CourbesStatistiques(k).bilan{1}); %#ok
            else
                if strcmp(this.CourbesStatistiques(k).bilan{1}(1).DataTypeValue, DataTypeValue)
                    subCourbesStatistiques(end+1) = k; %#ok
                    flagExistenceModel(end+1) = existModel(this.CourbesStatistiques(k).bilan{1}); %#ok
                end
            end
        end
    end
    
    %     if isfield(this.CourbesStatistiques(k).bilan{1}(1), 'model') ...
    %             && ~isempty(this.CourbesStatistiques(k).bilan{1}(1).model) ...
    %             && isa(this.CourbesStatistiques(k).bilan{1}(1).model, 'ClModel') % Ceinture et bretelles mais vaut mieux �tre prudent
    %         flagExistenceModel = 1;
    %     end
end

%% Message si aucune courbe trouv�e

if isempty(subCourbesStatistiques)
    sub = [];
    my_warndlg_NoCurve(this, Tag, 'NoMessage', NoMessage);
    return
end

%% LastOne

if LastOne
    sub = subCourbesStatistiques(end);
    NomsCourbes = str(end);
    return
end

%% Test si on doit sortir sans s�lection manuelle

% if NoManualSelection || (iZoneEtude == 1)
if NoManualSelection % Modif JMA le 13/10/2015
    sub = subCourbesStatistiques;
    NomsCourbes = str;
    return
end

%% S�lection de la ou des courbes

if isempty(Title)
    str1 = 'S�lection des courbes ';
    str2 = 'Curves selection ';
    Title = Lang(str1,str2);
end

if isequal(InitialValue, 'all')
    iZoneEtude = subCourbesStatistiques;
else
    
    if isempty(InitialValue)
        %     iZoneEtude = length(subCourbesStatistiques); % Modifi� � Horten le 25/11/2009
        iZoneEtude = subCourbesStatistiques(end); % Modifi� � Horten le 25/11/2009
    else
        iZoneEtude = InitialValue;
    end
end
if length(subCourbesStatistiques) == 1
    sub = 1;
else
    [~, iZoneEtude] = intersect(subCourbesStatistiques, iZoneEtude);
    if MayBeNone
        [sub, flag] = my_listdlgMultiple(Title, str(subCourbesStatistiques), 'InitialValue', iZoneEtude, 'SelectionMode', SelectionMode);
    else
        [sub, flag] = my_listdlg(Title, str(subCourbesStatistiques), 'InitialValue', iZoneEtude, 'SelectionMode', SelectionMode);
    end
    if ~flag
        return
    end
end
NomsCourbes = str;

%% S�lection de ce que l'on veut tracer

if SelectionCurveOrModel && any(flagExistenceModel(sub))
    str1 = 'Que voulez-vous tracer ?';
    str2 = 'What do you want to plot ?';
    str3 = {'Courbe'; 'Mod�le'; 'R�sidus'};
    str4 = {'Curve'; 'Model'; 'Residuals'};
    [subTypeCurve, flag] = my_listdlg(Lang(str1,str2), Lang(str3,str4), 'InitialValue', 1:2);
    if ~flag
        return
    end
end
sub = subCourbesStatistiques(sub);


function flagExistenceModel = existModel(bilan)

if isfield(bilan(1), 'model') ...
        && ~isempty(bilan(1).model) ...
        && isa(bilan(1).model, 'ClModel') % Ceinture et bretelles mais vaut mieux �tre prudent
    flagExistenceModel = 1;
else
    flagExistenceModel = 0;
end
