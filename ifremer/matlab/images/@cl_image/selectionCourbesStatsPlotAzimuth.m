function [flag, sub, Azimuths, IncAngleBeg, IncAngleEnd, IncAngleStep, ReflecBeg, ReflecEnd] = selectionCourbesStatsPlotAzimuth(this)

Azimuths     = [];
IncAngleBeg  = [];
IncAngleEnd  = [];
IncAngleStep = [];
ReflecBeg    = [];
ReflecEnd    = [];

%% Sélection des courbes

[flag, sub] = selectionCourbesStats(this, 'SelectionCurveOrModel', 1);
if ~flag || isempty(sub)
    flag = 0;
    return
end

N = length(sub);
titreV = cell(N,1);
unit = cell(N,1);
for k=1:N
    bilan = this.CourbesStatistiques(sub(k)).bilan;
    titreV{k} = bilan{1}(1).titreV;
    unit{k} = 'deg';
%     NY     = bilan{1}(1).NY;
%     nomX   = bilan{1}(1).nomX;
%     bilan{1}(k).nomZoneEtude
end

%% Saisie des azimuths

str1 = 'Saisie des azimuths pour les différentes courbes';
str2 = 'Azimuths for the given curves';
for k=1:N
    p(k) = ClParametre('Name', titreV{k}, ...
        'Unit', unit{k}, 'Value', NaN, 'MinValue', 0, 'MaxValue', 360); %#ok<AGROW>
end
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Azimuths = a.getParamsValue;

%% Saisie des angles d'incidence

str1 = 'Echantillonnage des angles d''incidence';
str2 = 'Incidence angles sampling';
p    = ClParametre('Name', 'Min', ...
    'Unit', 'deg', 'Value', -60, 'MinValue', -80, 'MaxValue', 80);
p(2) = ClParametre('Name', 'Max', ...
    'Unit', 'deg', 'Value', 60, 'MinValue', -80, 'MaxValue', 80);
p(3) = ClParametre('Name', Lang('Pas','Step'), ...
    'Unit', 'deg', 'Value', 10, 'MinValue', 5, 'MaxValue', 25);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

IncAngleBeg  = val(1);
IncAngleEnd  = val(2);
IncAngleStep = val(3);

%% Saisie de l'échelle de réflectivité

str1 = 'Echelle de la réflectivité (dB)';
str2 = 'Range of the reflectivity (dB)';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Auto', Lang('Manuel', 'Manual'));
if ~flag
    return
end
if rep == 2
    str1 = 'Echelle de la réflectivité';
    str2 = 'Range of the reflectivity';
    p    = ClParametre('Name', 'Min', ...
        'Unit', 'dB', 'Value', -60, 'MinValue', -60, 'MaxValue', 0);
    p(2) = ClParametre('Name', 'Max', ...
        'Unit', 'dB', 'Value', 0, 'MinValue', -60, 'MaxValue', 0);
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;

    ReflecBeg  = val(1);
    ReflecEnd  = val(2);
end

