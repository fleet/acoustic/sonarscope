function [flag, sub, nbCourbesStatistiques, NomsCourbes, Offsets, repImport] = selectionCourbesStatsPlusOffsets(this, repImport, varargin)

[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
[varargin, Tag]           = getPropertyValue(varargin, 'Tag', []);
[varargin, NoMessage]     = getPropertyValue(varargin, 'NoMessage', 0); %#ok<ASGLU>

str         = [];
NomsCourbes = {};
Offsets     = [];
flag        = 1;

subCourbesStatistiques = [];
nbCourbesStatistiques = length(this.CourbesStatistiques);
for k=1:nbCourbesStatistiques
    if isempty(Tag)
        pppp = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude;
        if isnumeric(pppp)
            pppp = num2str(pppp);
        end
        if isempty(pppp)
            if ~isdeployed
                str1 = 'JMA : Analyser pb dans selectionCourbesStats : il s''agit sans doute d''une courbe dont le filtrage a foir�.';
                str2 = 'TODO';
                my_warndlg(Lang(str1,str2), 1);
            end
            continue
        end
        str{k} = pppp; %#ok<AGROW>
        %         disp('ajouter Tag : nomZoneEtude ici')
        subCourbesStatistiques(end+1) = k; %#ok
    else
        str{k} = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude; %#ok<AGROW>
        if strcmp(this.CourbesStatistiques(k).bilan{1}(1).Tag, Tag)
            subCourbesStatistiques(end+1) = k; %#ok
        end
    end
end

if isempty(subCourbesStatistiques)
    sub = [];
    my_warndlg_NoCurve(this, Tag, 'NoMessage', NoMessage);
    return
end

% if isempty(iZoneEtude)
%     iZoneEtude = length(subCourbesStatistiques);
% else
%     iZoneEtude = find(iZoneEtude == subCourbesStatistiques);
% end
iZoneEtude = length(subCourbesStatistiques); % Modifi� � Horten le 25/11/2009

str1 = 'S�lection des courbes';
str2 = 'Curves selection';
[sub, flag] = my_listdlg(Lang(str1,str2), str(subCourbesStatistiques), 'InitialValue', iZoneEtude, 'SelectionMode', SelectionMode);
if ~flag
    return
end
sub = subCourbesStatistiques(sub);
NomsCourbes = str;

%% Lecture du fichier d'offset

%{
% Exemple format attendu : suite de lignes : num�ro de courbe, offset
1    10
2    -5
3     0
4    20
5     0
6     0
7     0
8     0
9     0
%}

ligne{1} = '1  0';
ligne{end+1} = '1 -3';
ligne{end+1} = '2 -2';
ligne{end+1} = '3  0';
ligne{end+1} = '4  0';
ligne{end+1} = '5 -1';
ligne{end+1} = '6  0';
ligne{end+1} = '7  0';
ligne{end+1} = '8  2';
ligne{end+1} = '9 -1';
str1 = 'Cr�ez un fichier .txt o� vous d�crirez les offsets � appliquer aux diff�rents secteurs comme ceci. Attention : les valeurs seront invers�es.';
str2 = sprintf('Create a .txt file where you describe the offsets for the sector numbers as this. Warning : the values will be set to the opposite sign.');
my_txtdlg(Lang(str1,str2), ligne)

Filtre = fullfile(repImport, '*.txt');
[flag, nomFic] = my_uigetfile('*.txt', 'Give a file name', Filtre);
if ~flag
    return
end
repImport = fileparts(nomFic);

fid = fopen(nomFic);
lines = textscan(fid, '%f %f');
fclose(fid);
X = [lines{1} lines{2}];

for k=1:size(X,1)
    Offsets(X(k,1)) = X(k,2); %#ok<AGROW>
end
