% Selection de courbes de statistiques conditionnelles
%
% Syntax
%   [flag, sub] = selectionCourbesStatsSubtract(this) 
%
% Input Arguments
%   this : Instance de cl_image
%
% Output Arguments
%   sub : Liste des courbes
%
% Name-Value Pair Arguments
%    : 
%
% Examples 
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, subPlus, identMinus] = selectionCourbesStatsSubtract(this, varargin)

subPlus = [];

%% S�lection de la courbe qui sera soustraite aux autres

str1 = 'S�lectionnez la courbe qui devra �tre soustraite aux autres';
str2 = 'Select the curve that will be subtracted to others';
[flag, identMinus, nbTotalCourbes, NomsCourbes] = selectionCourbesStats(this, 'SelectionMode', 'Single', 'Title', Lang(str1,str2), varargin{:});
if ~flag || isempty(identMinus)
    return
end

%% Contr�le si il reste des courbes

if nbTotalCourbes == 1
    str1 = 'Il n''y a pas d''autres courbes pour r�aliser une soustraction';
    str2 = 'There are no other curces to do the subtraction.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% S�lection des autres courbes

subPlus = setdiff(1:nbTotalCourbes, identMinus);
str1 = 'S�lectionnez les autres courbes';
str2 = 'Select the other curves';
[sub, flag] = my_listdlgMultiple(Lang(str1,str2), NomsCourbes(subPlus));
if ~flag
    return
end
subPlus = subPlus(sub);
