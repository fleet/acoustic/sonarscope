% Selection de courbes de statistiques conditionnelles
%
% Syntax
%   [flag, sub, nbCourbesStatistiques, NomsCourbes] = selectionCourbesStats(this)
%
% Input Arguments
%   this : Instance de cl_image
%
% Output Arguments
%   sub : Liste des courbes
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, sub, nbCourbesStatistiques, NomsCourbes] = selectionCourbesStatsVert(this, varargin)

% [varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
% [varargin, XYLimZoom] = getPropertyValue(varargin, 'XYLimZoom', []);
[varargin, Tag]       = getPropertyValue(varargin, 'Tag', []);
[varargin, NoMessage] = getPropertyValue(varargin, 'NoMessage', 0); %#ok<ASGLU>

% [nomZoom, iZoneEtude] = identZoom(this, XYLimZoom, Tag);

str = [];
NomsCourbes = {};

subCourbesStatistiques = [];
nbCourbesStatistiques = length(this.CourbesStatistiques);
for k=1:nbCourbesStatistiques
    if isempty(Tag)
        pppp = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude;
        if isempty(pppp)
            if ~isdeployed
                str1 = 'JMA : Analyser pb dans selectionCourbesStats : il s''agit sans doute d''une courbe dont le filtrage a foir�.';
                str2 = 'TODO';
                my_warndlg(Lang(str1,str2), 1);
            end
            continue
        end
        
        if strcmp(this.CourbesStatistiques(k).bilan{1}(1).nom, 'Y') ...
                && strcmp(this.CourbesStatistiques(k).bilan{1}(1).nomX, 'Y') ...
                && strcmp(this.CourbesStatistiques(k).bilan{1}(1).DataTypeConditions{1}, 'Y')
            str{k} = pppp; %#ok<AGROW>
            subCourbesStatistiques(end+1) = k; %#ok
        end
        
        %{
x: [1x1159 double]
y: [1159x1 double]
nom: 'Y'
sub: [1x1159 double]
nx: [1159x1 double]
std: [1159x1 double]
titreV: [1x76 char]
NY: 1159
nomX: 'Y'
Biais: 0
nomZoneEtude: 'a'
commentaire: ''
Tag: [1x76 char]
DataTypeValue: 'Bathymetry'
DataTypeConditions: {'Y'}
Support: {'Axe'}
GeometryType: 'PingBeam'
TypeCourbeStat: 'For Bias Compensation'
TagSup:
        %}
        
    else
        str{k} = this.CourbesStatistiques(k).bilan{1}(1).nomZoneEtude; %#ok<AGROW>
        if strcmp(this.CourbesStatistiques(k).bilan{1}(1).Tag, Tag)
            % A continuer si besoin
            %            && strcmp(this.CourbesStatistiques(k).bilan{1}(1).nom, 'Y') ...
            %                 && strcmp(this.CourbesStatistiques(k).bilan{1}(1).nomX, 'Y') ...
            %                 && strcmp(this.CourbesStatistiques(k).bilan{1}(1).DataTypeConditions{1}, 'Y')
            subCourbesStatistiques(end+1) = k; %#ok
        end
    end
end

if isempty(subCourbesStatistiques)
    sub = [];
    flag = 0;
    my_warndlg_NoCurve(this, Tag, 'NoMessage', NoMessage);
    return
end

% if isempty(iZoneEtude)
%     iZoneEtude = length(subCourbesStatistiques);
% else
%     iZoneEtude = find(iZoneEtude == subCourbesStatistiques);
% end

sub = subCourbesStatistiques;
% str1 = 'S�lection des courbes';
% str2 = 'Curves selection';
% [sub, flag] = my_listdlg(Lang(str1,str2), str(subCourbesStatistiques), 'InitialValue', iZoneEtude, 'SelectionMode', SelectionMode);
% if ~flag
%     sub = [];
%     return
% end
% sub = subCourbesStatistiques(sub);
NomsCourbes = str;

flag = 1;
