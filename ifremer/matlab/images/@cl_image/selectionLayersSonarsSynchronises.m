% Selection de layers synchronises en X et en Y
%
% this = selectionLayersSonarsSynchronises(this, ...)
%
% Input Arguments
%   this : instance de clc_image
%
% Name-Value Pair Arguments
%   Selection : {'Single'} | 'Multiple'
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [choix, flag] = selectionLayersSonarsSynchronises(this, indImage, varargin)

[varargin, typeSelection] = getPropertyValue(varargin, 'Selection', 'Single'); %#ok<ASGLU>

flag = 1;

indLayers = 1:length(this);
for k=1:length(this)
    txtValeurV{k}   = this(k).Name; %#ok<AGROW>
    txtSynchroX{k}  = this(k).TagSynchroX; %#ok<AGROW>
    txtSynchroY{k}  = this(k).TagSynchroY; %#ok<AGROW>
    GeometryType(k) = this(k).GeometryType;%#ok<AGROW>
    if GeometryType(k) == GeometryType
        SRD = get(this(k), 'SonarResolutionD');
        if isempty(SRD)
            resolution(k) = 0;%#ok<AGROW>
        else
            resolution(k) = SRD;%#ok<AGROW>
        end
    elseif GeometryType(k) == cl_image.indGeometryType('PingAcrossDist')
        SRX = get(this(k), 'SonarResolutionX');
        if isempty(SRX)
            resolution(k) = 0 ;%#ok<AGROW>
        else
            resolution(k) = SRX; %#ok<AGROW>
        end
    else
        resolution(k) = 1; %#ok<AGROW>
    end
end
indLayers(indImage) = [];
subMemeTagSynchroX = strcmp(txtSynchroX(indLayers), txtSynchroX(indImage));
subMemeTagSynchroY = strcmp(txtSynchroY(indLayers), txtSynchroY(indImage));
subMemeTagSynchroS = (GeometryType(indLayers) == GeometryType(indImage));
subMemeresolution = (resolution(indLayers) == resolution(indImage));
indLayers(~(subMemeTagSynchroX & subMemeTagSynchroY & subMemeTagSynchroS & subMemeresolution)) = [];

if isempty(indLayers)
    choix = [];
    return
end

str1 = 'M�me chose sur ?';
str2 = 'Same thing on ?';
strChoix = txtValeurV(indLayers);
[choix, flag] = my_listdlg(Lang(str1,str2), strChoix, 'SelectionMode', typeSelection);
if ~flag
    choix = [];
    return
end
if flag
    choix = indLayers(choix);
else
    choix = [];
end
