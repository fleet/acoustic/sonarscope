% Selection de layers synchronises en X et en Y
%
% this = selectionLayersSynchronises(this, ...)
%
% Input Arguments
%   this : instance de clc_image
%
% Name-Value Pair Arguments
%   Selection : {'Single'} | 'Multiple'
%
% Name-only Arguments
%   noXY : Pas de pseudo-Layers X ey Y
%
% Output Arguments
%   flag      :
%   Selection :
%   Coul      :
%
% See also selecCurveDataTypes listeLayersSynchronises Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Selection, Coul] = selectionLayersSynchronises(this, indImage, varargin)

[varargin, Max]                = getPropertyValue(varargin, 'Max',       Inf);
[varargin, strSecondeQuestion] = getPropertyValue(varargin, 'QuestionY', []);
[varargin, Title]              = getPropertyValue(varargin, 'Title',     []);

[varargin, noXY]            = getFlag(varargin, 'noXY');
[varargin, flagSignauxVert] = getFlag(varargin, 'SignauxVert'); %#ok<ASGLU>

Coul = [];

if isempty(strSecondeQuestion)
    str1 = 'Statistiques crois�es';
    str2 = 'Cross statistics ?';
    strSecondeQuestion = Lang(str1,str2);
end

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this, indImage); %#ok<ASGLU>
if ~flag
    return
end

if noXY
    if isempty(indLayers)
        str1 = 'Il faut poss�der au moins 2 images syncronis�es en X et en Y pour faire cette op�ration.';
        str2 = 'It is necessary to have 2 synchronized images to do this processing.';
        my_warndlg(Lang(str1,str2), 1);
        Selection = [];
        flag = 0;
        return
    end
end

[strChoix, TypeVal, indLayer, DataType, Unit] = get_listeVariablesForStatistics(this, indImage, indLayers, ...
    'noXY', noXY, 'flagSignauxVert', flagSignauxVert);

% A partir de la version R2013a.
% Mise en forme pour s�lection dans l'IHM d�di�.
item = 'Layer';
sub  = strcmp(TypeVal,item);
ListeItems.nomLayers(1,1)       = {strChoix(sub)};
ListeItems.SelectionLayers{1,1} = zeros(1,numel(strChoix(sub)));
titleDataSet{1,1} = 'Layers';

item = 'Axe';
sub  = strcmp(TypeVal,item);
ListeItems.nomLayers(end+1,1)       = {strChoix(sub)};
ListeItems.SelectionLayers{end+1,1} = zeros(1,numel(strChoix(sub)));
titleDataSet{end+1,1} = 'Axe';

item = 'SignalVert';
sub  = strcmp(TypeVal,item);
ListeItems.nomLayers(end+1,1)       = {strChoix(sub)};
ListeItems.SelectionLayers{end+1,1} = zeros(1,numel(strChoix(sub)));
titleDataSet{end+1,1} = Lang('Signal Vertical','Vertical Signal');

if isempty(Title)
    str1 = 'Choix des ordonn�es';
    str2 = 'Selection of X coordinates';
    Title = Lang(str1,str2);
end
[flag, strList] = uiSelectItemsFromTableSheet(ListeItems, ...
    'titleDataSet',  titleDataSet, ...
    'InitialValue2', 2, ...
    'Entete',        Title, ...
    'SelectionMode', 'Single', ...
    'CheckBoxExist', false);
if ~flag
    Selection = [];
    return
end

choix1   = [strList.SelectionLayers{1,:} strList.SelectionLayers{2,:} strList.SelectionLayers{3,:}];
[~,imax] = max( max([ size( strList.nomLayers{1,:}); size(strList.nomLayers{2,:}); size(strList.nomLayers{3,:}) ]));
strChoix = cat(imax, strList.nomLayers{1,:}, strList.nomLayers{2,:}, strList.nomLayers{3,:});
choix1 = find(choix1 == 1);

Selection(1).Name     = strChoix{choix1};
Selection(1).TypeVal  = TypeVal{choix1};
Selection(1).indLayer = indLayer(choix1);
Selection(1).DataType = DataType(choix1);
Selection(1).Unit     = Unit{choix1};

% Suppression des �l�ments tout juste s�lectionn�s pour la suite des s�lections.
for k=1:numel(strList.SelectionLayers)
    if ~isempty(ListeItems.nomLayers{k,:})
        ListeItems.nomLayers{k,:}(logical(strList.SelectionLayers{k,:}))       = [];
        ListeItems.SelectionLayers{k,:}(logical(strList.SelectionLayers{k,:})) = [];
    end
end
% Remise � z�ro pour les cas suivants :
TypeVal(choix1)  = [];
indLayer(choix1) = [];

if ~isempty(strChoix) && (Max > 1)
    flag = 1;
    while flag
        [choix1, flag] = my_questdlg(strSecondeQuestion, 'Init', 2);
        if ~flag
            Selection = [];
            return
        elseif choix1 == 2
            break
        end
        
        str1 = 'Image conditionelle';
        str2 = 'Conditionnal image';
        [flag, strList] = uiSelectItemsFromTableSheet(ListeItems, ...
            'titleDataSet', titleDataSet, ...
            'InitialValue2', 2, ...
            'Entete', Lang(str1,str2), ...
            'SelectionMode', 'Single', ...
            'CheckBoxExist', false);
        
        if flag
            choix1 = [strList.SelectionLayers{1,:} strList.SelectionLayers{2,:} strList.SelectionLayers{3,:}];
            try
                strChoix = [strList.nomLayers{1,:} strList.nomLayers{2,:} strList.nomLayers{3,:}]; % modif JMA le 06/04/2021
            catch
                strChoix = [strList.nomLayers{1,:};strList.nomLayers{2,:};strList.nomLayers{3,:}];
            end
            choix1   = find(choix1 == 1);
            
            Selection(end+1).Name   = strChoix{choix1};  %#ok<AGROW>
            Selection(end).TypeVal  = TypeVal{choix1};
            Selection(end).indLayer = indLayer(choix1);
            Selection(end).Unit     = Unit{choix1};
            if indLayer(choix1) == 0
                Selection(end).DataType = [];
            else
                Selection(end).DataType = this(indLayer(choix1)).DataType;
            end
            
            if indLayer(choix1) == 0
                %                 Coul = jet(128);
            else
                DT = this(indLayer(choix1)).DataType;
                identSegmentation = cl_image.indDataType('Segmentation');
                identMask         = cl_image.indDataType('Mask');
                
                if (DT == identSegmentation) || (DT == identMask)
                    Coul = this(indLayer(choix1)).ColormapCustom;
                end
            end
            
            % Suppression des �l�ments tout juste s�lectionn�s pour la suite des
            % s�lections.
            for k=1:numel(strList.SelectionLayers)
                ListeItems.nomLayers{k,:}(logical(strList.SelectionLayers{k,:}))       = [];
                ListeItems.SelectionLayers{k,:}(logical(strList.SelectionLayers{k,:})) = [];
            end
            
            TypeVal{choix1}  = [];
            indLayer(choix1) = [];
            DataType(choix1) = [];
        else
            break
        end
        
        if length(Selection) >= Max
            break
        end
        
        if isempty(strChoix)
            break
        end
    end
end
