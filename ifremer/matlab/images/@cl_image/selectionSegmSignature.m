% Selection des signatures de texture
%
% Syntax
%   sub = selectionSegmSignature(this)
%
% Input Arguments
%   this : Instance de cl_image
%
% Output Arguments
%   sub : Liste des courbes
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function sub = selectionSegmSignature(this, varargin)

%[varargin, Confirmation] = getFlag(varargin, 'Confirmation');
[varargin, XYLimZoom]     = getPropertyValue(varargin, 'XYLimZoom', []);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Single'); %#ok<ASGLU>

[nomZoom, iZoneEtude]  = identZoom(this, XYLimZoom, []); %#ok<ASGLU>

str = [];
subApprentissageTextures = [];
for numTexture=1:length(this.Texture)
    str{numTexture} = this.Texture(numTexture).nomTexture; %#ok<AGROW>
    %     disp('ajouter Tag : nomZoneEtude ici')
    subApprentissageTextures(end+1) = numTexture; %#ok
end

if isempty(subApprentissageTextures)
    sub = [];
    return
end

if isempty(iZoneEtude)
    iZoneEtude = length(subApprentissageTextures);
else
    iZoneEtude = find(iZoneEtude == subApprentissageTextures);
    if isempty(iZoneEtude)
        iZoneEtude = 1;
    end
end

% if (length(subApprentissageTextures) > 1) || Confirmation
str1 = 'Signatures texturales';
str2 = 'Textural signatures';
[sub, flag] = my_listdlg(Lang(str1,str2), str(subApprentissageTextures), 'InitialValue', iZoneEtude, 'SelectionMode', SelectionMode);
if flag
    sub = subApprentissageTextures(sub);
else
    sub = [];
end
% else
%     sub = subApprentissageTextures(1);
% end
