% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   [a, flag] = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_image
%
% Name-Value Pair Arguments
%  Cf. cl_image
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   a  : Instance de la classe
%   flag : 1=OK, 0=KO
%
% Examples
%   a = cl_image('Image', Lena);
%
%   set(a, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   a
%   a = set(a, 'ColormapIndex', 2)
%
%   set(a, 'ColormapCustom', (gray+jet) /2, 'ColormapIndex', 1)
%   imagesc(a)
%
% See also cl_image cl_image/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = set(this, varargin)

%# function CNES
%# function CNESjet
%# function CNESgray
%# function NIWA1
%# function NIWA2
%# function JetSym
%# function ColorSea
%# function ColorEarth
%# function Haxby
%# function Becker
%# function Catherine
%# function Seismics
%# function SeismicsSym

if isempty(varargin)
    return
end

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 'Auto');

flag = 1;

%% On v�rifie que l'instance est bien de dimension 1

if length(this) ~= 1
    my_warndlg('cl_image/set : L''instance doit �tre de dimension 1', 1);
    flag = 0;
    return
end

for iBidon=1:1 % Pour pouvoir exploiter l'instruction "break"

	[varargin, this.ValNaN] = getPropertyValue(varargin, 'ValNaN',  this.ValNaN);

    % ----------------------------------------------------------------
    % L'image pouvant �tre de taille importante, on pr�f�re �viter des
    % manipulations inutiles

    [varargin, PredefinedStats] = getPropertyValue(varargin, 'PredefinedStats', []);
    [varargin, Image]           = getPropertyValue(varargin, 'Image',           []);
    if ~isempty(Image)
        this = set_Image(this, Image, 'PredefinedStats', PredefinedStats, 'Memmapfile', Memmapfile);
%         if isempty(this.x)
%             this.x = 1:size(Image,2);
%             this.y = 1:size(Image,1);
%         end
    end
    
    [varargin, Name] = getPropertyValue(varargin, 'Name', []);
    if ~isempty(Name)
        this.Name = Name;
    end

    [varargin, this.Unit] = getPropertyValue(varargin, 'Unit', this.Unit);

    % Table de couleurs
    [varargin, this.ColormapIndex]  = getPropertyValueNum(varargin, 'ColormapIndex',  this.ColormapIndex, 1, length(this.strColormapIndex));
    [varargin, this.ColormapCustom] = getPropertyValue(   varargin, 'ColormapCustom', this.ColormapCustom);
    
    this = set_ColormapIndex(this, this.ColormapIndex); % TODO : pourquoi �a ?
    
    % 'XLabel' doit �tre plac� imp�rativement devant 'X' car il y a confusion
    [varargin, this.XLabel] = getPropertyValue(varargin, 'XLabel', this.XLabel);

    % 'YLabel' doit �tre plac� imp�rativement devant 'Y' car il y a confusion
    [varargin, this.YLabel] = getPropertyValue(varargin, 'YLabel', this.YLabel);

    % x
    [varargin, x] = getPropertyValue(varargin, 'X', []);
    if ~isempty(x)
        this.x = x;
    end
    
    % y
	[varargin, y] = getPropertyValue(varargin, 'Y', []);
    if ~isempty(y)
        this.y = y;
    end

%% A partir d'ici on peut tester si varargin est vide et sortir de la boucle

    % XLim
    [varargin, XLim] = getPropertyValue(varargin, 'XLim', []); % Ne doit plus �tre employ� car XLim est �cras� par set_x qui le reinitialise
    if ~isempty(XLim)
        this.XLim = XLim;
        if ~isdeployed
            my_warndlg('set(this, ''XLim'', XLim employed, change to set_XLim(this, XLim)', 1);
        end
    end
    if isempty(varargin)
        break
    end

    % YLim
    [varargin, YLim] = getPropertyValue(varargin, 'YLim', []); % Ne doit plus �tre employ� car YLim est �cras� par set_y qui le reinitialise

    if ~isempty(YLim)
        this.YLim = YLim;
        if ~isdeployed
            my_warndlg('set(this, ''XLim'', XLim employed, change to set_XLim(this, XLim)', 1);
        end
    end
    if isempty(varargin)
        break
    end


    %% Affectation des valeurs ne n�cessitant pas de v�rification

    [varargin, this.DataType] = getPropertyValue(varargin, 'DataType', this.DataType);
    if isempty(varargin)
        break
    end

    [varargin, this.ImageType] = getPropertyValue(varargin, 'ImageType', this.ImageType);
    if isempty(varargin)
        break
    end
%     [varargin, this.Name] = getPropertyValue(varargin, 'Name', this.Name);
%     if isempty(varargin)
%         break
%     end

    [varargin, this.InitialImageName] = getPropertyValue(varargin, 'InitialImageName', this.InitialImageName);
    if isempty(varargin)
        break
    end

    [varargin, this.TagSynchroX] = getPropertyValue(varargin, 'TagSynchroX', this.TagSynchroX);
    if isempty(varargin)
        break
    end

    [varargin, this.TagSynchroY] = getPropertyValue(varargin, 'TagSynchroY', this.TagSynchroY);
    if isempty(varargin)
        break
    end

    [varargin, this.XUnit] = getPropertyValue(varargin, 'XUnit', this.XUnit);
    if isempty(varargin)
        break
    end

    [varargin, this.YUnit] = getPropertyValue(varargin, 'YUnit', this.YUnit);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'InitialFileName', []);
    if ~isempty(X)
        this.InitialFileName = X; % TODO : un setter a.InitialFileName = x devrait �tre d�riv�
    end
    if isempty(varargin)
        break
    end

    [varargin, this.InitialFileFormat] = getPropertyValue(varargin, 'InitialFileFormat', this.InitialFileFormat);
    if isempty(varargin)
        break
    end

    [varargin, this.GeometryType] = getPropertyValue(varargin, 'GeometryType', this.GeometryType);
    if isempty(varargin)
        break
    end

    [varargin, this.Sonar.Desciption] = getPropertyValue(varargin, 'SonarDesciption',  this.Sonar.Desciption);
    if isempty(varargin)
        break
    end
    [varargin, this.Sonar.Desciption] = getPropertyValue(varargin, 'SonarDescription', this.Sonar.Desciption);
    if isempty(varargin)
        break
    end


    [varargin, this.Comments] = getPropertyValue(varargin, 'Comments', this.Comments);
    if isempty(varargin)
        break
    end

    [varargin, this.strMaskBits] = getPropertyValue(varargin, 'strMaskBits', this.strMaskBits);
    if isempty(varargin)
        break
    end



    [varargin, this.VertExagAuto] = getPropertyValue(varargin, 'VertExagAuto', this.VertExagAuto);
    if isempty(varargin)
        break
    end

    [varargin, this.VertExag] = getPropertyValue(varargin, 'VertExag', this.VertExag);
    if isempty(varargin)
        break
    end

    [varargin, this.Azimuth] = getPropertyValue(varargin, 'Azimuth', this.Azimuth);
    if isempty(varargin)
        break
    end

    [varargin, this.TagSynchroContrast] = getPropertyValue(varargin, 'TagSynchroContrast', this.TagSynchroContrast);
    if isempty(varargin)
        this.TagSynchroContrast = num2str(rand(1));
        break
    end

    [varargin, this.SpectralStatus] = getPropertyValue(varargin, 'SpectralStatus', this.SpectralStatus);
    if isempty(varargin)
        break
    end


    [varargin, this.Carto] = getPropertyValue(varargin, 'Carto', this.Carto);
    if isempty(varargin)
        break
    end

    [varargin, this.XDir] = getPropertyValueNum(varargin, 'XDir', this.XDir, 1, 2);
    if isempty(varargin)
        break
    end

    [varargin, this.SaveParamsIfFFT] = getPropertyValue(varargin, 'SaveParamsIfFFT', this.SaveParamsIfFFT);
    if isempty(varargin)
        break
    end
    
    [varargin, this.OriginColorbar] = getPropertyValue(varargin, 'OriginColorbar', this.OriginColorbar);
    if isempty(varargin)
        break
    end

    [varargin, this.Sonar.RawDataResol] = getPropertyValue(varargin, 'SonarRawDataResol', this.Sonar.RawDataResol);
    if isempty(varargin)
        break
    end

    [varargin, this.Sonar.ResolutionD]  = getPropertyValue(varargin, 'SonarResolutionD', this.Sonar.ResolutionD);
    if isempty(varargin)
        break
    end

    [varargin, this.Sonar.ResolutionX]  = getPropertyValue(varargin, 'SonarResolutionX', this.Sonar.ResolutionX);
    if isempty(varargin)
        break
    end


    if isfield(this.Sonar.BathyCel, 'Z')
        [varargin, this.Sonar.BathyCel.Z]   = getPropertyValue(varargin, 'SonarBathyCel_Z', this.Sonar.BathyCel.Z);
        if isempty(varargin)
            break
        end

        [varargin, this.Sonar.BathyCel.T]   = getPropertyValue(varargin, 'SonarBathyCel_T', this.Sonar.BathyCel.T);
        if isempty(varargin)
            break
        end

        [varargin, this.Sonar.BathyCel.S]   = getPropertyValue(varargin, 'SonarBathyCel_S', this.Sonar.BathyCel.S);
        if isempty(varargin)
            break
        end

        [varargin, this.Sonar.BathyCel.C]   = getPropertyValue(varargin, 'SonarBathyCel_C', this.Sonar.BathyCel.C);
        if isempty(varargin)
            break
        end
    else
        [varargin, this.Sonar.BathyCel] = getPropertyValue(varargin, 'SonarBathyCel', this.Sonar.BathyCel);
    end

    [varargin, this.Sonar.TxAlongAngle]  = getPropertyValue(varargin, 'SonarTxAlongAngle',    this.Sonar.TxAlongAngle);
    if isempty(varargin)
        break
    end
    
    [varargin, this.Sonar.TxAcrossAngle]  = getPropertyValue(varargin, 'SonarTxAcrossAngle',    this.Sonar.TxAcrossAngle);
    if isempty(varargin)
        break
    end
    
    [varargin, this.CourbesStatistiques] = getPropertyValue(varargin, 'CourbesStatistiques', this.CourbesStatistiques);
    if isempty(varargin)
        break
    end

    [varargin, this.RegionOfInterest] = getPropertyValue(varargin, 'RegionOfInterest', this.RegionOfInterest);
    if isempty(varargin)
        break
    end

    [varargin, this.ContourValues] = getPropertyValue(varargin, 'ContourValues', this.ContourValues);
    if isempty(varargin)
        break
    end
    
    [varargin, this.Video] = getPropertyValue(varargin, 'Video', this.Video);
    if isempty(varargin)
        break
    end

    %% Sp�cial sonar

    [varargin, this.Sonar.NE.etat] = getPropertyValue(varargin, 'SonarNE_etat', this.Sonar.NE.etat);

    [varargin, X] = getPropertyValue(varargin, 'SonarTime', []);
    this = set_SonarTime(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarImmersion', []);
    this = set_SonarImmersion(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarHeight', []);
    this = set_SonarHeight(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarCableOut', []);
    this = set_SonarCableOut(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarSpeed', []);
    this = set_SonarSpeed(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarHeading', []);
    this = set_SonarHeading(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarRoll', []);
    this = set_SonarRoll(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarPitch', []);
    this = set_SonarPitch(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarYaw', []);
    if ~isempty(X)
        this.Sonar.Yaw = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarSurfaceSoundSpeed', []);
    this = set_SonarSurfaceSoundSpeed(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarHeave', []);
    this = set_SonarHeave(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTide', []);
    this = set_SonarTide(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDraught', []);
    this = set_SonarDraught(this, X);
    if isempty(varargin)
        break
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarTrueHeave', []);
    this = set_SonarTrueHeave(this, X);
    if isempty(varargin)
        break
    end
    

    [varargin, X] = getPropertyValue(varargin, 'SonarPingCounter', []);
    this = set_SonarPingCounter(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarSampleFrequency', []);
    this = set_SonarSampleFrequency(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarFishLatitude', []);
    this = set_SonarFishLatitude(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarFishLongitude', []);
    this = set_SonarFishLongitude(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarShipLatitude', []);
    this = set_SonarShipLatitude(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarShipLongitude', []);
    this = set_SonarShipLongitude(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarPortMode_1', []);
    this = set_SonarPortMode_1(this, X);
    if isempty(varargin)
        break
    end

%     [varargin, X] = getPropertyValue(varargin, 'SonarEM2040Mode2', []);
%     this = set_SonarEM2040Mode2(this, X);
%     if isempty(varargin)
%         break
%     end
% 
%     [varargin, X] = getPropertyValue(varargin, 'SonarEM2040Mode3', []);
%     this = set_SonarEM2040Mode3(this, X);
%     if isempty(varargin)
%         break
%     end

    [varargin, X] = getPropertyValue(varargin, 'SonarPortMode_2', []);
    this = set_SonarPortMode_2(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarStarMode_1', []);
    this = set_SonarStarMode_1(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarStarMode_2', []);
    this = set_SonarStarMode_2(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SampleFrequency', []); % Chercher l'erreur avec "SonarSampleFrequency"
    this = set_SonarSampleFrequency(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarFrequency', []); % Chercher l'erreur avec "SonarFrequency"
    this = set_SonarFrequency(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarInterlacing', []);
    this = set_SonarInterlacing(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarPresenceWC', []);
    this = set_SonarPresenceWC(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarPresenceFM', []);
    this = set_SonarPresenceFM(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDistPings', []);
    this = set_SonarDistPings(this, X);
    if isempty(varargin)
        break
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarNbSwaths', []);
    this = set_SonarNbSwaths(this, X);
    if isempty(varargin)
        break
    end
    
    
    
    
    
    [varargin, X] = getPropertyValue(varargin, 'SonarBSN', []);
    this = set_SonarBSN(this, X);
    if isempty(varargin)
        break
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarBSO', []);
    this = set_SonarBSO(this, X);
    if isempty(varargin)
        break
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarTVGN', []);
    this = set_SonarTVGN(this, X);
    if isempty(varargin)
        break
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarTVGCrossOver', []);
    this = set_SonarTVGCrossOver(this, X);
    if isempty(varargin)
        break
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarTxBeamWidth', []);
    this = set_SonarTxBeamWidth(this, X);
    if isempty(varargin)
        break
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarResolAcrossSample', []);
    this = set_SonarResolAcrossSample(this, X);
    if isempty(varargin)
        break
    end


    
    

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Transmit.X', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Transmit.X = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Transmit.Y', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Transmit.Y = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Transmit.Z', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Transmit.Z = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Transmit.Roll', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Transmit.Roll = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Transmit.Pitch', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Transmit.Pitch = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Transmit.Heading', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Transmit.Heading = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Transmit.GyrocompassHeadingOffset', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Transmit.GyrocompassHeadingOffset = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Receive.X', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Receive.X = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Receive.Y', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Receive.Y = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Receive.Z', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Receive.Z = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Receive.Roll', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Receive.Roll = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Receive.Pitch', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Receive.Pitch = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.Arrays.Receive.Heading', []);
    if ~isempty(X)
        this.Sonar.Ship.Arrays.Receive.Heading = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.MRU.X', []);
    if ~isempty(X)
        this.Sonar.Ship.MRU.X = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.MRU.Y', []);
    if ~isempty(X)
        this.Sonar.Ship.MRU.Y = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.MRU.Z', []);
    if ~isempty(X)
        this.Sonar.Ship.MRU.Z = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.MRU.RollCalibration', []);
    if ~isempty(X)
        this.Sonar.Ship.MRU.RollCalibration = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.MRU.PitchCalibration', []);
    if ~isempty(X)
        this.Sonar.Ship.MRU.PitchCalibration = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.MRU.HeadingCalibration', []);
    if ~isempty(X)
        this.Sonar.Ship.MRU.HeadingCalibration = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.MRU.TimeDelay', []);
    if ~isempty(X)
        this.Sonar.Ship.MRU.TimeDelay = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.GPS.X', []);
    if ~isempty(X)
        this.Sonar.Ship.GPS.X = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.GPS.Y', []);
    if ~isempty(X)
        this.Sonar.Ship.GPS.Y = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.GPS.Z', []);
    if ~isempty(X)
        this.Sonar.Ship.GPS.Z = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.GPS.TimeStampUse', []);
    if ~isempty(X)
        this.Sonar.Ship.GPS.TimeStampUse = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'Sonar.Ship.WaterLineVerticalOffset', []);
    if ~isempty(X)
        this.Sonar.Ship.WaterLineVerticalOffset = X;
    end

    %% Classes de l'histogramme donn�s par l'utilisateur

    [varargin, HistoCentralClasses] = getPropertyValue(varargin, 'HistoCentralClasses', []);
    if ~isempty(HistoCentralClasses)
        this = compute_stats(this, 'bins', HistoCentralClasses);
    end

    %% Affectation des valeurs n�cessitant des v�rifications

    [varargin, this.CLim] = getPropertyValue(varargin, 'CLim', this.CLim);

    [varargin, this.YDir] = getPropertyValueNum(varargin, 'YDir', this.YDir, 1, 2);
    if isempty(varargin)
        break
    end

    %% Attributs de pouvant pas �tre modifi�s

    % ------------------------------------------------------------------
    % Flag pour dire que l'on est en train de d�finir l'etat de l'image.
    % Sans ce flag, on corrige les images (OK pour la TVG, � faire pour les autres corrections)

    [varargin, Sonar_DefinitionENCours] = getFlag(varargin, 'Sonar_DefinitionENCours');

    %% TVG

    flagProcessTVG = 0;

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.etat_Old = this.Sonar.TVG.etat;
        end
        this.Sonar.TVG.etat = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_origine', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.origine_Old  = this.Sonar.TVG.origine;
        end
        this.Sonar.TVG.origine = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_ConstructTypeCompens', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.ConstructTypeCompens_Old = this.Sonar.TVG.ConstructTypeCompens;
        end
        this.Sonar.TVG.ConstructTypeCompens = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_ConstructAlpha', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.ConstructAlpha_Old = this.Sonar.TVG.ConstructAlpha;
        end
        this.Sonar.TVG.ConstructAlpha  = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_ConstructConstante', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.ConstructConstante_Old = this.Sonar.TVG.ConstructConstante;
        end
        this.Sonar.TVG.ConstructConstante  = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_ConstructCoefDiverg', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.ConstructCoefDiverg_Old = this.Sonar.TVG.ConstructCoefDiverg;
        end
        this.Sonar.TVG.ConstructCoefDiverg  = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_ConstructTable', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.ConstructTable_Old = this.Sonar.TVG.ConstructTable;
        end
        this.Sonar.TVG.ConstructTable = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_IfremerAlpha', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.IfremerAlpha_Old = this.Sonar.TVG.IfremerAlpha;
        end
        this.Sonar.TVG.IfremerAlpha = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_IfremerConstante', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.IfremerConstante_Old = this.Sonar.TVG.IfremerConstante;
        end
        this.Sonar.TVG.IfremerConstante = X;
        flagProcessTVG = 1;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarTVG_IfremerCoefDiverg', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.TVG.IfremerCoefDiverg_Old = this.Sonar.TVG.IfremerCoefDiverg;
        end
        this.Sonar.TVG.IfremerCoefDiverg = X;
        flagProcessTVG = 1;
    end

    if ~Sonar_DefinitionENCours && flagProcessTVG
        % C'est pas top du tout de faire ca ==> ACTION DE MAINTENANCE A FAIRE
        [varargin, Range]               = getPropertyValue(varargin, 'Range',               []);
        [varargin, AbsorptionCoeffRT]   = getPropertyValue(varargin, 'AbsorptionCoeffRT',   []);
        [varargin, AbsorptionCoeffSSc]  = getPropertyValue(varargin, 'AbsorptionCoeffSSc',  []);
        [varargin, AbsorptionCoeffUser] = getPropertyValue(varargin, 'AbsorptionCoeffUser', []);
        [TVG, flag] = sonar_TVG_appliquer(this, 'Range', Range, ...
            'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
            'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
            'AbsorptionCoeffUser', AbsorptionCoeffUser);
        if ~flag
            this = []; % A voir si c'est utile de mettre � []
            return
        end
        TVG.Sonar.TVG.etat_Old    = TVG.Sonar.TVG.etat;
        TVG.Sonar.TVG.origine_Old = TVG.Sonar.TVG.origine;
        this = TVG;
    end



    %% DiagEmi

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagEmi_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagEmi.etat_Old = this.Sonar.DiagEmi.etat;
        end
        this.Sonar.DiagEmi.etat = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagEmi_origine', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagEmi.origine_Old = this.Sonar.DiagEmi.origine;
        end
        this.Sonar.DiagEmi.origine = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagEmi_ConstructTypeCompens', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagEmi.ConstructTypeCompens_Old = this.Sonar.DiagEmi.ConstructTypeCompens;
        end
        this.Sonar.DiagEmi.ConstructTypeCompens = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagEmi_IfremerTypeCompens', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagEmi.IfremerTypeCompens_Old  = this.Sonar.DiagEmi.IfremerTypeCompens;
        end
        this.Sonar.DiagEmi.IfremerTypeCompens = X;
    end


    %% DiagRec

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagRec_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagRec.etat_Old = this.Sonar.DiagRec.etat;
        end
        this.Sonar.DiagRec.etat = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagRec_origine', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagRec.origine_Old  = this.Sonar.DiagRec.origine;
        end
        this.Sonar.DiagRec.origine = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagRec_ConstructTypeCompens', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagRec.ConstructTypeCompens_Old  = this.Sonar.DiagRec.ConstructTypeCompens;
        end
        this.Sonar.DiagRec.ConstructTypeCompens = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarDiagRec_IfremerTypeCompens', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.DiagRec.IfremerTypeCompens_Old  = this.Sonar.DiagRec.IfremerTypeCompens;
        end
        this.Sonar.DiagRec.IfremerTypeCompens = X;
    end


    %% Aire Insonifi�e

    [varargin, X] = getPropertyValue(varargin, 'SonarAireInso_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.AireInso.etat_Old = this.Sonar.AireInso.etat;
        end
        this.Sonar.AireInso.etat = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarAireInso_origine', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.AireInso.origine_Old  = this.Sonar.AireInso.origine;
        end
        this.Sonar.AireInso.origine = X;
    end



    %% Niveau d'�mission

    [varargin, X] = getPropertyValue(varargin, 'Sonar_NE_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.NE.etat_Old = this.Sonar.NE.etat;
        end
        this.Sonar.NE.etat = X;
    end


    %% Sensibilit� des antennes

    [varargin, X] = getPropertyValue(varargin, 'Sonar_SH_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.SH.etat_Old = this.Sonar.SH.etat;
        end
        this.Sonar.SH.etat = X;
    end


    %% Gain analogique � la sortie des antennes de r�ception

    [varargin, X] = getPropertyValue(varargin, 'Sonar_GT_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.GT.etat_Old = this.Sonar.GT.etat;
        end
        this.Sonar.GT.etat = X;
    end


    %% Backscatter

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_etat', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.BS.etat_Old = this.Sonar.BS.etat;
        end
        this.Sonar.BS.etat = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_origine', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.BS.origine_Old  = this.Sonar.BS.origine;
        end
        this.Sonar.BS.origine = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_origineBelleImage', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.BS.origineBelleImage  = this.Sonar.BS.origineBelleImage;
        end
        this.Sonar.BS.origineBelleImage = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_LambertCorrection', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.BS.LambertCorrection  = this.Sonar.BS.LambertCorrection;
        end
        this.Sonar.BS.LambertCorrection = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_BSLurtonParameters', []);
    if ~isempty(X)
        this.Sonar.BS.BSLurtonParameters = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_origineFullCompens', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.BS.origineFullCompens  = this.Sonar.BS.origineFullCompens;
        end
        this.Sonar.BS.origineFullCompens = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_IfremerCompensModele', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.BS.IfremerCompensModele_Old  = this.Sonar.BS.IfremerCompensModele;
        end
        this.Sonar.BS.IfremerCompensModele = X;
    end

    [varargin, X] = getPropertyValue(varargin, 'SonarBS_IfremerCompensTable', []);
    if ~isempty(X)
        if ~Sonar_DefinitionENCours
            this.Sonar.BS.IfremerCompensTable_Old  = this.Sonar.BS.IfremerCompensTable;
        end
        this.Sonar.BS.IfremerCompensTable = X;
    end


    [varargin, X] = getPropertyValue(varargin, 'Writable', []);
    if ~isempty(X)
        my_breakpoint % Remplacer par obj.Writable = X;
        if isa(this.Image, 'cl_memmapfile')
            this.Image = set(this.Image, 'Writable', X);
        end
    end    
    
    [varargin, X] = getPropertyValue(varargin, 'SonarR1SamplesFiltre', []);
    if ~isempty(X)
        this.Sonar.SampleBeamData.R1SamplesFiltre = X;
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarAmpPingNormHorzMax', []);
    if ~isempty(X)
        this.Sonar.SampleBeamData.AmpPingNormHorzMax = X;
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarTxAngle', []);
    if ~isempty(X)
        this.Sonar.SampleBeamData.TxAngle = X;
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarRMax1', []);
    if ~isempty(X)
        this.Sonar.SampleBeamData.RMax1 = X;
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarRMax2', []);
    if ~isempty(X)
        this.Sonar.SampleBeamData.RMax2 = X;
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarMaskWidth', []);
    if ~isempty(X)
        this.Sonar.SampleBeamData.MaskWidth = X;
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SonarTransmitSectorNumber', []);
    if ~isempty(X)
        this.Sonar.SampleBeamData.TransmitSectorNumber = X;
    end
    

    %% V�rification si varargin est vide

    if ~isempty(varargin)
        if ischar(varargin{1})
            str = sprintf('%s n''est pas une propriete', varargin{1});
        else
            str = sprintf('Argument not interpretable.');
        end
        %my_breakpoint
        my_warndlg(['cl_image/set : ' str], 1);
    end

end

if isempty(this.CLim) && ~isempty(this.Image)
    if isempty(this.StatValues)
        CLim = [0 1];
    else
        if isempty(this.StatValues.Min)
            CLim(1) = 0;
        else
            CLim(1) = this.StatValues.Min;
        end
        
        if isempty(this.StatValues.Max)
            CLim(2) = 1;
        else
            CLim(2) = this.StatValues.Max;
        end
    end
    this.CLim = CLim;
end

if isempty(this.XLim) && ~isempty(this.x)
    this.XLim = compute_XYLim(this.x);
end

if isempty(this.YLim) && ~isempty(this.y)
    this.YLim = compute_XYLim(this.y);
end

if isempty(this.Name)
    this.Name = 'Image';
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
end
