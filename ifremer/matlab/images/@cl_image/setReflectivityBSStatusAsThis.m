function [flag, c] = setReflectivityBSStatusAsThis(this, c, indLayer, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

SonarBS_etatNew = get(this, 'SonarBS_etat');
if SonarBS_etatNew == 2
    [flag, d] = SonarBSCompensNone(c, indLayer, 'Mute', Mute); % TODO : not tested
else
    SonarBS_origineNew            = get(this, 'SonarBS_origine');
    SonarBS_origineBelleImageNew  = get(this, 'SonarBS_origineBelleImage');
    SonarBS_origineFullCompensNew = get(this, 'SonarBS_origineFullCompens');
    SonarBS_LambertCorrectionNew  = get(this, 'SonarBS_LambertCorrection');
    [flag, d] = SonarBSCompens(c, indLayer, SonarBS_origineNew, SonarBS_origineBelleImageNew, ...
        SonarBS_origineFullCompensNew, SonarBS_LambertCorrectionNew, 'Mute', Mute);
end
if flag
    c(indLayer) = d;
end
flag = 1;
