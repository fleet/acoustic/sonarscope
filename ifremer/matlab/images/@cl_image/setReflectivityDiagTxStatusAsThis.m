function [flag, c] = setReflectivityDiagTxStatusAsThis(this, c, indLayer, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

SonarDiagEmi_etat = get(this, 'SonarDiagEmi_etat');

if SonarDiagEmi_etat == 1 % Image d'origine compens�e
    
    SonarDiagEmi_origine_New = get(this, 'SonarDiagEmi_origine');
    if SonarDiagEmi_origine_New == 1
        SonarDiagEmi_Type_New = get(this, 'SonarDiagEmi_ConstructTypeCompens');
    else
        SonarDiagEmi_Type_New = get(this, 'SonarDiagEmi_IfremerTypeCompens');
    end
    
    % Ajout JMA le 28/07/2016
    if (SonarDiagEmi_origine_New == 2) && (SonarDiagEmi_Type_New == 1)
        [flag, identCourbe] = selectionCourbesStats(this, 'Tag', 'DiagTx', 'NoMessage', Mute);
    else
        identCourbe = [];
    end
    
    c(indLayer).CourbesStatistiques = this.CourbesStatistiques;
    
    [flag, d] = SonarDiagTxCompens(c, indLayer, SonarDiagEmi_origine_New, SonarDiagEmi_Type_New, ...
        'identCourbe', identCourbe, 'Mute', Mute);
    if flag
        c(indLayer) = d;
    end
    
else % Image d'origine non compens�e
    [flag, a] = SonarDiagTxCompensAucune(c, indLayer, 'Mute', 1);
    if ~flag
        return
    end
    if ~isempty(a)
        c(indLayer) = a;
    end
end

flag = 1;
