function [flag, c] = setReflectivityInsonifiedAreaStatusAsThis(this, c, indLayer, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

% TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
SonarAireInso_origine_New = get(this, 'SonarAireInso_origine');
% [flag, indLayerRange] = findAnyLayerRange(c, indLayer, 'AtLeastOneLayer');
% if ~flag
%     return
% end

% indLayerSlopeAcross = findLayers(c, 'DataType', cl_image.indDataType('SlopeAcross'));
% indLayerSlopeAlong  = findLayers(c, 'DataType', cl_image.indDataType('SlopeAlong'));

% [flag, d] = SonarAireInsoCompens(c, indLayer, SonarAireInso_origine_New, ...
%     'Range', c(indLayerRange), 'SlopeAcross', c(indLayerSlopeAcross), 'SlopeAlong', c(indLayerSlopeAlong), 'Mute', Mute);
[flag, d] = SonarAireInsoCompens(c, indLayer, SonarAireInso_origine_New, 'Mute', Mute);
if flag
    c(indLayer) = d;
end
flag = 1;
