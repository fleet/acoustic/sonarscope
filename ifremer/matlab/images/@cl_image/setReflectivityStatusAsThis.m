function [flag, c] = setReflectivityStatusAsThis(this, c, indLayer, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

%% BS processing

[flag, c] = setReflectivityBSStatusAsThis(this, c, indLayer, 'Mute', Mute);
if ~flag
    return
end

%% Insonified area processing

[flag, c] = setReflectivityInsonifiedAreaStatusAsThis(this, c, indLayer, 'Mute', Mute);
if ~flag
    return
end

%% DiagTx processing

[flag, c] = setReflectivityDiagTxStatusAsThis(this, c, indLayer, 'Mute', Mute);
if ~flag
    return
end

%% TVG processing

[flag, c] = setReflectivityTVGStatusAsThis(this, c, indLayer, 'Mute', Mute);
if ~flag
    return
end
