function [flag, c] = setReflectivityTVGStatusAsThis(this, c, indLayer, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

[flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser] ...
    = params_SonarTVG_Compensation(c, indLayer, 'Mute', 1);
if ~flag
    messageForDebugInspection
end

Range               = c(indLayerRange);
AbsorptionCoeffRT   = c(indLayerAbsorptionCoeffRT);
AbsorptionCoeffSSc  = c(indLayerAbsorptionCoeffSSc);
AbsorptionCoeffUser = c(indLayerAbsorptionCoeffUser);

SonarTVG_etat = get(this, 'SonarTVG_etat');
if SonarTVG_etat == 1
    SonarTVG_origine = get(this, 'SonarTVG_origine'); % 1=RT, 2=SSc, 3==User
    switch SonarTVG_origine
        case 3 % USer
            if isempty(indLayerAbsorptionCoeffSSc)
                str1 = 'Il n''y a pas de layer "AbsorptionCoeff" /  "User"';
                str2 = 'No "AbsorptionCoeff" / "User" layer found.';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                return
            end
            [flag, d] = sonar_TVG_CompensationUser(c(indLayer), ...
                'Range', Range, ...
                'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
                'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
                'AbsorptionCoeffUser', AbsorptionCoeffUser);
            if flag
                c(indLayer) = d;
                return
            else
                messageAboutCalibration('Tag', 'TVG', 'Mute', Mute)
            end
            
        case 2 % SSc
            if isempty(indLayerAbsorptionCoeffSSc)
                str1 = 'Il n''y a pas de layer "AbsorptionCoeff" /  "SSc"';
                str2 = 'No "AbsorptionCoeff" / "SSc" layer found.';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                return
            end
            [flag, d] = sonar_TVG_CompensationSSc(c(indLayer), ...
                'Range', Range, ...
                'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
                'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
                'AbsorptionCoeffUser', AbsorptionCoeffUser);
            if flag
                c(indLayer) = d;
                return
            else
                messageAboutCalibration('Tag', 'TVG', 'Mute', Mute)
            end
            
        case 1 % RT
            SonarTVG_origine = get(this, 'SonarTVG_origine'); % 1=RT, 2=SSc, 3==User
            if SonarTVG_origine == 1 % RT
                return
            end
            
            if isempty(indLayerAbsorptionCoeffRT)
                if isfield(c(indLayer).Sonar, 'AbsorptionCoeff_RT') && ~isempty(c(indLayer).Sonar.AbsorptionCoeff_RT)
                    X = repmat(c(indLayer).Sonar.AbsorptionCoeff_RT, 1, Range.nbColumns);
                    DataType = cl_image.indDataType('AbsorptionCoeff');
                    AbsorptionCoeffRT = inherit(c(indLayer), X, 'DataType', DataType, 'Unit', 'dB', 'ColormapIndex', 3);
                else
                    str1 = sprintf('Il n''y a pas de layer "AbsorptionCoeff" / "RT".\nSupprimez le r�pertoire "SonarScope" qui se trouve au m�me niveau que les .all et refaites le tratement. La nouvelle version de SSc recr�era le r�pertoire de cache qui contiendra le layer manquant.');
                    str2 = sprintf('No "AbsorptionCoeff" / "RT" layer found.\nYou should suppress the cache directory named "SonarScope" that was created with a previous SonarScope version and redo the processing.');
                    my_warndlg(Lang(str1,str2), 1);
                    flag = 0;
                    return
                end
            end
            [flag, d] = sonar_TVG_CompensationRT(c(indLayer), ...
                'Range', Range, ...
                'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
                'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
                'AbsorptionCoeffUser', AbsorptionCoeffUser);
            if flag
                c(indLayer) = d;
                return
            else
                messageAboutCalibration('Tag', 'TVG', 'Mute', Mute)
            end
            
        otherwise
            str1 = sprintf('"SonarTVG_etat" = %d dans "setReflectivityTVGStatusAsThis", ceci est non attendu.', SonarTVG_etat);
            str2 = sprintf('Value = %d not expected for "SonarTVG_etat" in "setReflectivityTVGStatusAsThis".', SonarTVG_etat);
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
    end
else
    % TODO : jamais test�
    [flag, d] = sonar_TVG_NoCompensation(c(indLayer), ...
        'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    if flag
        c(indLayer) = d;
    end
end
flag = 1;
