function this = set_BSStatus(this, BSStatus)

% Equivalent � SonarTVG_origine
%     this = set(this,  'Sonar_DefinitionENCours', ...
%         'Sonarxxx', xxxx, ...

this.Sonar.TVG.ConstructTypeCompens     = BSStatus.SonarTVG_ConstructTypeCompens;
this.Sonar.TVG.origine                  = BSStatus.SonarTVG_origine;
this.Sonar.TVG.ConstructTable           = BSStatus.SonarTVG_ConstructTable;
this.Sonar.TVG.ConstructAlpha           = BSStatus.SonarTVG_ConstructAlpha;
this.Sonar.TVG.ConstructConstante       = BSStatus.SonarTVG_ConstructConstante;
this.Sonar.TVG.ConstructCoefDiverg      = BSStatus.SonarTVG_ConstructCoefDiverg;
this.Sonar.TVG.IfremerAlpha             = BSStatus.SonarTVG_IfremerAlpha;
this.Sonar.AireInso.etat                = BSStatus.SonarAireInso_etat;
this.Sonar.AireInso.origine             = BSStatus.SonarAireInso_origine;
this.Sonar.DiagEmi.etat                 = BSStatus.SonarDiagEmi_etat;
this.Sonar.DiagEmi.origine              = BSStatus.SonarDiagEmi_origine;
this.Sonar.DiagEmi.ConstructTypeCompens	= BSStatus.SonarDiagEmi_ConstructTypeCompens;
this.Sonar.DiagRec.etat                 = BSStatus.SonarDiagRec_etat;
this.Sonar.DiagRec.origine              = BSStatus.SonarDiagRec_origine;
this.Sonar.DiagRec.ConstructTypeCompens = BSStatus.SonarDiagRec_ConstructTypeCompens;
this.Sonar.NE.etat                      = BSStatus.SonarNE_etat;
this.Sonar.SH.etat                      = BSStatus.SonarSH_etat;
this.Sonar.GT.etat                      = BSStatus.SonarGT_etat;
this.Sonar.BS.etat                      = BSStatus.SonarBS_etat;
this.Sonar.BS.origine                   = BSStatus.SonarBS_origine;
this.Sonar.BS.origineBelleImage         = BSStatus.SonarBS_origineBelleImage;
this.Sonar.BS.origineFullCompens        = BSStatus.SonarBS_origineFullCompens;
this.Sonar.BS.LambertCorrection         = BSStatus.SonarBS_LambertCorrection;
this.Sonar.BS.IfremerCompensTable       = BSStatus.SonarBS_IfremerCompensTable;
if isfield(BSStatus, 'SonarBS_SpecularReestablishment')
    this.Sonar.BS.SpecularReestablishment = BSStatus.SonarBS_SpecularReestablishment;
end
if isfield(BSStatus, 'SonarBS_IdentAlgoSnippets2OneValuePerBeam')
    this.Sonar.BS.IdentAlgoSnippets2OneValuePerBeam = BSStatus.SonarBS_IdentAlgoSnippets2OneValuePerBeam;
end