% Set Carto (cl_carto aggregation)
%
% Syntax
%  a = set_Carto(a, Carto)
%
% Input Arguments
%   a     : One cl_image instance
%   Carto : One cl_carto instance
%
% Output Arguments
%   a : The updated instance
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = import(cl_image, nomFic);
%   imagesc(a)
%   Carto = get_Carto(a)
%   a = set_Carto(a, Carto)
%
% See also cl_carto cl_image/get_Carto Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_Carto(this, X)
this.Carto = X;