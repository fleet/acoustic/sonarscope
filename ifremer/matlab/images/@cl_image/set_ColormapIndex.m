% Set ColormapIndex
%
% Syntax
%  ColormapIndex = set_ColormapIndex(a)
%
% Input Arguments
%   a             : One cl_image instance
%   ColormapIndex : Direction of the x axis when displayed : '1=Right' | '2=Left'
%
% Output Arguments
%   a : The updated instance
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Lena', 'DataType', 3, 'GeometryType', 15);
%     imagesc(a)
%     ColormapIndex = a.ColormapIndex
%   a = set_ColormapIndex(a, 2);
%     imagesc(a)
%   a = set_ColormapIndex(a, 3);
%     imagesc(a)
%
% See also cl_image/set_ColormapIndex Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_ColormapIndex(this, X, varargin)
% this.ColormapIndex = X; % Commenté par JMA le 24/02/2022 depuis que le
                          % setter est dans la classe (a.ColormapIndex = n)

if X == 1
    % Table de couleur personelle
    map = this.ColormapCustom;
    if this.Video == 2
        map = flipud(map);
    end
    this.Colormap = map;
else
    % Table de couleur matlab
    map = evalin('caller', [this.strColormapIndex{X} '(256)']);
    this.Colormap = map;
end
