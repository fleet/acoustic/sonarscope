% Set ContourValues
%
% Syntax
%  a = set_ContourValues(a, ContourValues)
%
% Input Arguments
%   a             : One cl_image instance
%   ContourValues : Values used when the image is displayed by the countour method
%
% Output Arguments
%   a : The updated instance
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = import(cl_image, nomFic);
%   imagesc(a)
%   contour(a)
%   ContourValues = a.ContourValues
%   a = set_ContourValues(a, ContourValues*2);
%   ContourValues = a.ContourValues
%   contour(a)
%
% See also cl_image/contour Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_ContourValues(this, X)
this.ContourValues = X;

% TODO : voir set(a, 'ContourValues', x)