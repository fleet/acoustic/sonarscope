function this = set_Image(this, Image, varargin)

[varargin, PredefinedStats] = getPropertyValue(varargin, 'PredefinedStats', []);
[varargin, Memmapfile]      = getPropertyValue(varargin, 'Memmapfile',      'Auto');

if get_nbBytes(Image) <= 2e7
    this.Image = Image;
else
    if isa(Image, 'cl_memmapfile')
        this.Image = Image;
    else
        [varargin, NoMemmapfile] = getFlag(varargin, 'NoMemmapfile'); %#ok<ASGLU>
        % TODO : supprimer tous les appels � NoMemmapfile et remplacer par Memmapfile='Auto' / 'No'
        if NoMemmapfile || strcmp(Memmapfile, 'No')
            this.Image = Image;
        else
            if ismatrix(Image) && (size(Image,1) == size(Image,2))
                this.Image = Image; % bidouille JMA pour �viter transposition matrices carr�es Ex import G:\FSK\Course\XYZ
            else
                this.Image = cl_memmapfile('Value', Image);
            end
        end
    end
end

nbSlides  = size(this.Image, 3);
if nbSlides == 3
    this.ImageType = 2;
end
if isa(Image, 'uint8')
    % Avant 07/03/2018
%     if this.ValNaN ~= -1 % Valeur -1 pour signifier qu'il n'y a pas de valeurs manquantes
%         this.ValNaN = 0;
%         sub = (Image == 0);
%         if sum(sub) > 0
%             if ~isdeployed
%                 disp('----------------------------------------------------------------')
%                 disp([this.Name ' : Valeur 0 prise par d�faut pour valeurs manquantes'])
%                 disp('Si les 0 sont des informations, il est pr�f�rable de transformer les 0 en 1')
%                 disp('de la mani�re suivante : I(I == 0) = 1;')
%             end
%         end
%     end
    
% Apr�s 07/03/2018 : plus rien
end

if isempty(PredefinedStats) || ~isfield(PredefinedStats{1}, 'Moyenne') || isempty(PredefinedStats{1}.Moyenne)
    try
        this = compute_stats(this);
    catch %#ok<CTCH>
    end
else
    this.StatValues          = PredefinedStats{1};
    this.StatSummary         = stats2str(PredefinedStats{1});
    this.HistoCentralClasses = PredefinedStats{2};
    this.HistoValues         = PredefinedStats{3};
end

% Numero d'identite propre �a cette image
this.NuIdent = floor(rand(1) * 1e8);

% Comment� par JMA le 24/02/2022
% this = set_x(this, this.x);
% this = set_y(this, this.y);

if isempty(this.x)
    this.x = 1:this.nbColumns;
end
if isempty(this.y)
    this.y = 1:this.nbRows;
end

if isempty(this.CLim)
    CLim = [this.StatValues.Min this.StatValues.Max];
    if any(~isfinite(CLim)) || any(isnan(CLim))
        CLim(1) = min(this.Image(:));
        CLim(2) = max(this.Image(:));
    end
    this.CLim = CLim;
end
this = set_ColormapIndex(this, this.ColormapIndex);

% Very strange : it is necessary to set [] to provoque the computation of ContourValues !!!
this.ContourValues = [];
