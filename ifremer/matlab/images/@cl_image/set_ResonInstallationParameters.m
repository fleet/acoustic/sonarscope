function this = set_ResonInstallationParameters(this, DataSonarInstallationParameters, varargin)

if ~isequal(DataSonarInstallationParameters, 'None') && ~isempty(DataSonarInstallationParameters) && length(DataSonarInstallationParameters.WaterLineVerticalOffset) ~= 0 %#ok<ISMT>
    
    %% Transducers
    
    Tx.nb      = 1;
    Tx.X       = DataSonarInstallationParameters.TransmitArrayX;
    Tx.Y       = DataSonarInstallationParameters.TransmitArrayY;
    Tx.Z       = DataSonarInstallationParameters.TransmitArrayZ;
    Tx.Roll    = DataSonarInstallationParameters.TransmitArrayRoll;
    Tx.Pitch   = DataSonarInstallationParameters.TransmitArrayPitch;
    Tx.Heading = DataSonarInstallationParameters.TransmitArrayHeading;
    
    Tx.nb      = 1;
    Rx.X       = DataSonarInstallationParameters.ReceiveArrayX;
    Rx.Y       = DataSonarInstallationParameters.ReceiveArrayY;
    Rx.Z       = DataSonarInstallationParameters.ReceiveArrayZ;
    Rx.Roll    = DataSonarInstallationParameters.ReceiveArrayRoll;
    Rx.Pitch   = DataSonarInstallationParameters.ReceiveArrayPitch;
    Rx.Heading = DataSonarInstallationParameters.ReceiveArrayHeading;
    
    this = set_SonarShipArraysTransmit(this, Tx);
    this = set_SonarShipArraysReceive( this, Rx);

    %% Motion Sensor
    
    MRU.X                  = DataSonarInstallationParameters.MotionSensorX;
    MRU.Y                  = DataSonarInstallationParameters.MotionSensorY;
    MRU.Z                  = DataSonarInstallationParameters.MotionSensorZ;
    MRU.RollCalibration    = DataSonarInstallationParameters.MotionSensorRollCalibration;
    MRU.PitchCalibration   = DataSonarInstallationParameters.MotionSensorPitchCalibration;
    MRU.HeadingCalibration = DataSonarInstallationParameters.MotionSensorHeadingCalibration;
    MRU.TimeDelay          = DataSonarInstallationParameters.MotionSensorTimeDelay;
    this = set_SonarShipMRU(this, MRU);
    
    %% Position Sensor

    Nav.X         = DataSonarInstallationParameters.PositionSensorX;
    Nav.Y         = DataSonarInstallationParameters.PositionSensorY;
    Nav.Z         = DataSonarInstallationParameters.PositionSensorZ;
    Nav.TimeDelay = DataSonarInstallationParameters.PositionSensorTimeDelay;
    this = set_SonarShipGPS(this, Nav);
    
    %% WaterLineVerticalOffset
    
    WVO.VerticalOffset = DataSonarInstallationParameters.WaterLineVerticalOffset;
    this = set_SonarShipWaterLine(this, WVO);
    
    %% Sauvegarde du fichier InstallationParameters.xml % TODO : c'est ce fichier qui est copi� N fois sur les r�pertoires !!!
    
    if nargin == 3
        nomFic = varargin{1};
        [~, NomImage] = fileparts(nomFic);
        nomDir = my_tempdir;
        nomFicXml = fullfile(nomDir, [NomImage 'InstallationParameters.xml']);
        
        xml_write(nomFicXml, DataSonarInstallationParameters);
        flag = exist(nomFicXml, 'file');
        if ~flag
            messageErreur(nomFicXml)
            return
        end
        
%         str1 = sprintf('Le fichier "%s" a �t� cr��, il vous sera utile si jamais vous voulez visualiser des echogrammes polaires dans GLOBE � partir de fichiers provenant du 7KCenter.', nomFicXml);
%         str2 = sprintf('"%s" was created, it can be useful only if you intend to locate Polar Echograms in GLOBE (from 7KCenter files).', nomFicXml);
%         my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExporXtfInstallationParameters', 'TimeDelay', 60);
    end
end
