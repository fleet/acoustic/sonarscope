function this = set_SampleBeamData(this, X)
if ~isempty(X)
    this.Sonar.SampleBeamData = X;
end

