function this = set_SignalsVert(this, signalContainers)

vertiacalSignalList = ClSignal.empty();

for k=1:length(signalContainers)
    for k2=1:numel(signalContainers(k).signalList)
        vertiacalSignalList(end+1) = signalContainers(k).signalList(k2);  %#ok<AGROW>
    end
end

this.SignalsVert = vertiacalSignalList;
