function this = set_SonarAbsorptionCoeff_RT(this, X)
if ~isempty(X)
    this.Sonar.AbsorptionCoeff_RT = X;
end
