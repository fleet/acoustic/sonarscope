function this = set_SonarBSN(this, X)
if ~isempty(X)
    this.Sonar.BSN = X;
end
