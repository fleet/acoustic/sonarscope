function this = set_SonarBSO(this, X)
if ~isempty(X)
    this.Sonar.BSO = X;
end
