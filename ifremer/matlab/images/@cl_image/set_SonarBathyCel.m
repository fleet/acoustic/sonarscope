function this = set_SonarBathyCel(this, X)

this.Sonar.BathyCel.TimeStartOfUse = [];
if isfield(X, 'Z')
    if ~isempty(X)
        this.Sonar.BathyCel.Z = X.Z(:,:);
        this.Sonar.BathyCel.T = X.T(:,:);
        this.Sonar.BathyCel.S = X.S(:,:);
        this.Sonar.BathyCel.C = X.C(:,:);
    end
elseif isfield(X, 'SoundSpeed')
    [n1,n2] = size(X(1).Depth);
    if n1 > n2
        X(1).Depth      = X(1).Depth';
        X(1).SoundSpeed = X(1).SoundSpeed';
        if isfield(X(1), 'Temperature')
            X(1).Temperature = X(1).Temperature';
            X(1).Salinity    = X(1).Salinity';
        end
    end
    
    this.Sonar.BathyCel.Z = X(1).Depth(:,:);
    this.Sonar.BathyCel.C = X(1).SoundSpeed(:,:);
    if isfield(X(1), 'Temperature')
        this.Sonar.BathyCel.T = X(1).Temperature(:,:);
        this.Sonar.BathyCel.S = X(1).Salinity(:,:);
    else
        this.Sonar.BathyCel.T = [];
        this.Sonar.BathyCel.S = [];
    end
    if isfield( X, 'TimeStartOfUse')
        this.Sonar.BathyCel.TimeStartOfUse = X(1).TimeStartOfUse; % Kongsberg V2
    end
else
    this.Sonar.BathyCel = X(:,:);
end
