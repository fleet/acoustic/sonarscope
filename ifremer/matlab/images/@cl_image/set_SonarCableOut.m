function this = set_SonarCableOut(this, X)
if ~isempty(X)
    this.Sonar.CableOut = X;
end

