function this = set_SonarDistPings(this, X)
if ~isempty(X)
    this.Sonar.DistPings = X;
end
