function this = set_SonarDraught(this, X)
if ~isempty(X)
    this.Sonar.Draught = X(:);
end
