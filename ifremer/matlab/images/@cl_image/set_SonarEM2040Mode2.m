function this = set_SonarEM2040Mode2(this, X)
if ~isempty(X)
    this.Sonar.EM2040Mode2 = X;
end
