function this = set_SonarEM2040Mode3(this, X)
if ~isempty(X)
    this.Sonar.EM2040Mode3 = X;
end
