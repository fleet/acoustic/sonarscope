function this = set_SonarFishLatitude(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.FishLatitude = X(:);
    else
        this.Sonar.FishLatitude = X(:,:);
    end
end
