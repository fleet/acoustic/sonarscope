function this = set_SonarFishLongitude(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.FishLongitude = X(:);
    else
        this.Sonar.FishLongitude = X(:,:);
    end
end
