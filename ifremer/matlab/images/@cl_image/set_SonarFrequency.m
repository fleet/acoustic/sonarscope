function this = set_SonarFrequency(this, X)
if ~isempty(X)
    this.Sonar.SonarFrequency = X;
end
