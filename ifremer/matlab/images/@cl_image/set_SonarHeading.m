function this = set_SonarHeading(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.Heading = X(:);
    else
        this.Sonar.Heading = X(:,:);
    end
end
