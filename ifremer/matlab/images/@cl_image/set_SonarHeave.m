function this = set_SonarHeave(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.Heave = X(:);
    else
        this.Sonar.Heave = X(:,:);
    end
end
