function this = set_SonarHeight(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.Height = -abs(X(:));
    else
        this.Sonar.Height = -abs(X(:,:));
    end
end
