function this = set_SonarImmersion(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.Immersion = X(:);
    else
        this.Sonar.Immersion = X(:,:);
    end
end
