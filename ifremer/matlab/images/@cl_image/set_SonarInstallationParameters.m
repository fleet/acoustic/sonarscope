function this = set_SonarInstallationParameters(this, DataSonarInstallationParameters)

this.Sonar.Ship.WaterLineZ = DataSonarInstallationParameters.WLZ;
% TODO remplacer par
%{
WVO.VerticalOffset = DataSonarInstallationParameters.WLZ;
this = set_SonarShipWaterLine(this, WVO);
%}
    
if ~isfield(DataSonarInstallationParameters, 'GCG')
    DataSonarInstallationParameters.GCG = 0;
end

this.Sonar.Ship.GyrocompassHeadingOffset = DataSonarInstallationParameters.GCG;
if isempty(this.Sonar.Ship.GyrocompassHeadingOffset)
    this.Sonar.Ship.GyrocompassHeadingOffset = 0;
end

%% Transducers 

if isfield(DataSonarInstallationParameters, 'STC')
    SystemTransducerConfiguration = DataSonarInstallationParameters.STC;
    switch SystemTransducerConfiguration
        case 0 % Single Tx + single Rx : Ex EM122, EM302, EM710, EM2040 Single
            nbTx = 1; %#ok<NASGU>
            nbRx = 1; %#ok<NASGU>
        case 1 % Single Head - Ex EM3002 Single Head, EM2040C Single Head
            nbTx = 1; %#ok<NASGU>
            nbRx = 1; %#ok<NASGU>
        case 2 % Dual Head - Ex: EM 3002 Dual Head, EM2040C Dual Head
            nbTx = 2; %#ok<NASGU>
            nbRx = 2; %#ok<NASGU>
        case 3 % Single Tx + Dual Rx - Ex : EM2040 Dual Rx
            nbTx = 1; %#ok<NASGU>
            nbRx = 2; %#ok<NASGU>
        case 4 % Dual Tx + Dual Rx - Ex : EM2040 Dual Tx
            nbTx = 2; %#ok<NASGU>
            nbRx = 2; %#ok<NASGU>
        case 5 % Portable Single Head - Ex : EM2040P Single Head
            nbTx = 1; %#ok<NASGU>
            nbRx = 1; %#ok<NASGU>
        case 6 % Modular - Ex : EM2040M
            nbTx = 1; %#ok<NASGU>
            nbRx = 1; %#ok<NASGU>
        case 7 % Portable Dual Head - Ex : EM2040P Dual Head
            nbTx = 1; %#ok<NASGU>
            nbRx = 1; %#ok<NASGU>
        otherwise
            my_warndlg('SystemTransducerConfiguration not recognized', 1);
            SystemTransducerConfiguration = - 1;
    end
else
    SystemTransducerConfiguration = -1;
end

switch DataSonarInstallationParameters.EmModel
    case {122; 300; 302; 710; 712; 2000; 304; 120} % 304 ajout� sauvagement
        Tx.nb = 1;
        Tx.X       = DataSonarInstallationParameters.S1Y;
        Tx.Y       = DataSonarInstallationParameters.S1X;
        Tx.Z       = DataSonarInstallationParameters.S1Z;
        Tx.Roll    = DataSonarInstallationParameters.S1R;
        Tx.Pitch   = DataSonarInstallationParameters.S1P;
        Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
        
        Rx.nb = 1;
        Rx.X       = DataSonarInstallationParameters.S2Y;
        Rx.Y       = DataSonarInstallationParameters.S2X;
        Rx.Z       = DataSonarInstallationParameters.S2Z;
        Rx.Roll    = DataSonarInstallationParameters.S2R;
        Rx.Pitch   = DataSonarInstallationParameters.S2P;
        Rx.Heading = DataSonarInstallationParameters.S2H; % V�rifier TODO
        
    %{
    case 3002 % et 3020 ?
        switch SystemTransducerConfiguration
            case 1 % Single Head
                Tx.nb = 1;
                Tx.X       = DataSonarInstallationParameters.S1Y;
                Tx.Y       = DataSonarInstallationParameters.S1X;
                Tx.Z       = DataSonarInstallationParameters.S1Z;
                Tx.Roll    = DataSonarInstallationParameters.S1R;
                Tx.Pitch   = DataSonarInstallationParameters.S1P;
                Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
                
                Rx.nb = 1;
                Rx.X       = DataSonarInstallationParameters.S1Y;
                Rx.Y       = DataSonarInstallationParameters.S1X;
                Rx.Z       = DataSonarInstallationParameters.S1Z;
                Rx.Roll    = DataSonarInstallationParameters.S1R;
                Rx.Pitch   = DataSonarInstallationParameters.S1P;
                Rx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
            case 2 % Dual Head
                Tx.nb = 2;
                Tx.X       = [DataSonarInstallationParameters.S1Y DataSonarInstallationParameters.S2Y];
                Tx.Y       = [DataSonarInstallationParameters.S1X DataSonarInstallationParameters.S2X];
                Tx.Z       = [DataSonarInstallationParameters.S1Z DataSonarInstallationParameters.S2Z];
                Tx.Roll    = [DataSonarInstallationParameters.S1R DataSonarInstallationParameters.S2R];
                Tx.Pitch   = [DataSonarInstallationParameters.S1P DataSonarInstallationParameters.S2P];
                Tx.Heading = [DataSonarInstallationParameters.S1H DataSonarInstallationParameters.S2H]; % V�rifier TODO
                
                Rx.nb = 2;
                Rx.X       = [DataSonarInstallationParameters.S2Y DataSonarInstallationParameters.S2Y];
                Rx.Y       = [DataSonarInstallationParameters.S2X DataSonarInstallationParameters.S2X];
                Rx.Z       = [DataSonarInstallationParameters.S2Z DataSonarInstallationParameters.S2Z];
                Rx.Roll    = [DataSonarInstallationParameters.S2R DataSonarInstallationParameters.S2R];
                Rx.Pitch   = [DataSonarInstallationParameters.S2P DataSonarInstallationParameters.S2P];
                Rx.Heading = [DataSonarInstallationParameters.S2H DataSonarInstallationParameters.S2H]; % V�rifier TODO
            otherwise
                my_warndlg('EmModel and SystemTransducerConfiguration not explicit', 1);
        end
        %}
        
    case 3020 % EM3002D apparemment
        if isfield(DataSonarInstallationParameters, 'SystemSerialNumberSecondHead') && (DataSonarInstallationParameters.SystemSerialNumberSecondHead ~= 0)
            Tx.nb = 2;
            Tx.X       = [DataSonarInstallationParameters.S1Y DataSonarInstallationParameters.S2Y];
            Tx.Y       = [DataSonarInstallationParameters.S1X DataSonarInstallationParameters.S2X];
            Tx.Z       = [DataSonarInstallationParameters.S1Z DataSonarInstallationParameters.S2Z];
            Tx.Roll    = [DataSonarInstallationParameters.S1R DataSonarInstallationParameters.S2R];
            Tx.Pitch   = [DataSonarInstallationParameters.S1P DataSonarInstallationParameters.S2P];
            Tx.Heading = [DataSonarInstallationParameters.S1H DataSonarInstallationParameters.S2H]; % V�rifier TODO
            
            Rx = Tx;
        else
            Tx.nb = 1;
            Tx.X       = DataSonarInstallationParameters.S1Y;
            Tx.Y       = DataSonarInstallationParameters.S1X;
            Tx.Z       = DataSonarInstallationParameters.S1Z;
            Tx.Roll    = DataSonarInstallationParameters.S1R;
            Tx.Pitch   = DataSonarInstallationParameters.S1P;
            Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
            
            Rx = Tx;
        end
        
    case 2040
        switch SystemTransducerConfiguration
            case -1 % EM2040 AUV Asterix
                Tx.nb = 1;
                Tx.X       = DataSonarInstallationParameters.S1Y;
                Tx.Y       = DataSonarInstallationParameters.S1X;
                Tx.Z       = DataSonarInstallationParameters.S1Z;
                Tx.Roll    = DataSonarInstallationParameters.S1R;
                Tx.Pitch   = DataSonarInstallationParameters.S1P;
                Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
                
                Rx.nb = 1;
                Rx.X       = DataSonarInstallationParameters.S2Y;
                Rx.Y       = DataSonarInstallationParameters.S2X;
                Rx.Z       = DataSonarInstallationParameters.S2Z;
                Rx.Roll    = DataSonarInstallationParameters.S2R;
                Rx.Pitch   = DataSonarInstallationParameters.S2P;
                Rx.Heading = DataSonarInstallationParameters.S2H; % V�rifier TODO
                
            case 0 % EM2040 Single
                Tx.nb = 1;
                Tx.X       = DataSonarInstallationParameters.S1Y;
                Tx.Y       = DataSonarInstallationParameters.S1X;
                Tx.Z       = DataSonarInstallationParameters.S1Z;
                Tx.Roll    = DataSonarInstallationParameters.S1R;
                Tx.Pitch   = DataSonarInstallationParameters.S1P;
                Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
                
                Rx.nb = 1;
                Rx.X       = DataSonarInstallationParameters.S2Y;
                Rx.Y       = DataSonarInstallationParameters.S2X;
                Rx.Z       = DataSonarInstallationParameters.S2Z;
                Rx.Roll    = DataSonarInstallationParameters.S2R;
                Rx.Pitch   = DataSonarInstallationParameters.S2P;
                Rx.Heading = DataSonarInstallationParameters.S2H; % V�rifier TODO
                
            case 1 % EM2040C % TODO : v�rifier en quoi c'est diff�rent de EM2040
                Tx.nb = 1;
                Tx.X       = DataSonarInstallationParameters.S1Y;
                Tx.Y       = DataSonarInstallationParameters.S1X;
                Tx.Z       = DataSonarInstallationParameters.S1Z;
                Tx.Roll    = DataSonarInstallationParameters.S1R;
                Tx.Pitch   = DataSonarInstallationParameters.S1P;
                Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
                
                Rx.nb = 1;
                Rx.X       = DataSonarInstallationParameters.S2Y;
                Rx.Y       = DataSonarInstallationParameters.S2X;
                Rx.Z       = DataSonarInstallationParameters.S2Z;
                Rx.Roll    = DataSonarInstallationParameters.S2R;
                Rx.Pitch   = DataSonarInstallationParameters.S2P;
                Rx.Heading = DataSonarInstallationParameters.S2H; % V�rifier TODO
                
            case 2 % Dual Head - Ex: EM 3002 Dual Head, EM2040C Dual Head
                my_warndlg('EmModel and SystemTransducerConfiguration configuration not expected', 1);
                
            case 3 % Single Tx + Dual Rx - Ex : EM2040 Dual Rx
                Tx.nb = 1;
                Tx.X       = DataSonarInstallationParameters.S1Y;
                Tx.Y       = DataSonarInstallationParameters.S1X;
                Tx.Z       = DataSonarInstallationParameters.S1Z;
                Tx.Roll    = DataSonarInstallationParameters.S1R;
                Tx.Pitch   = DataSonarInstallationParameters.S1P;
                Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
                
                Rx.nb = 2;
                Rx.X       = [DataSonarInstallationParameters.S2Y DataSonarInstallationParameters.S3Y];
                Rx.Y       = [DataSonarInstallationParameters.S2X DataSonarInstallationParameters.S3X];
                Rx.Z       = [DataSonarInstallationParameters.S2Z DataSonarInstallationParameters.S3Z];
                Rx.Roll    = [DataSonarInstallationParameters.S2R DataSonarInstallationParameters.S3R];
                Rx.Pitch   = [DataSonarInstallationParameters.S2P DataSonarInstallationParameters.S3P];
                Rx.Heading = [DataSonarInstallationParameters.S2H DataSonarInstallationParameters.S3H]; % V�rifier TODO
                
            case 4 % Dual Tx + Dual Rx - Ex : EM2040 Dual Tx
                Tx.nb = 2;
                Tx.X       = [DataSonarInstallationParameters.S0Y DataSonarInstallationParameters.S1Y];
                Tx.Y       = [DataSonarInstallationParameters.S0X DataSonarInstallationParameters.S1X];
                Tx.Z       = [DataSonarInstallationParameters.S0Z DataSonarInstallationParameters.S1Z];
                Tx.Roll    = [DataSonarInstallationParameters.S0R DataSonarInstallationParameters.S1R];
                Tx.Pitch   = [DataSonarInstallationParameters.S0P DataSonarInstallationParameters.S1P];
                Tx.Heading = [DataSonarInstallationParameters.S0H DataSonarInstallationParameters.S1H]; % V�rifier TODO
                
                Rx.nb = 2;
                Rx.X       = [DataSonarInstallationParameters.S2Y DataSonarInstallationParameters.S3Y];
                Rx.Y       = [DataSonarInstallationParameters.S2X DataSonarInstallationParameters.S3X];
                Rx.Z       = [DataSonarInstallationParameters.S2Z DataSonarInstallationParameters.S3Z];
                Rx.Roll    = [DataSonarInstallationParameters.S2R DataSonarInstallationParameters.S3R];
                Rx.Pitch   = [DataSonarInstallationParameters.S2P DataSonarInstallationParameters.S3P];
                Rx.Heading = [DataSonarInstallationParameters.S2H DataSonarInstallationParameters.S3H]; % V�rifier TODO
                
            case 5 % Portable Single Head - Ex : EM2040P Single Head
%                 my_warndlg('EmModel and SystemTransducerConfiguration not pluged yet, please send a .all file to JMA', 1);
                Tx.nb = 1;
                Tx.X       = DataSonarInstallationParameters.S1Y;
                Tx.Y       = DataSonarInstallationParameters.S1X;
                Tx.Z       = DataSonarInstallationParameters.S1Z;
                Tx.Roll    = DataSonarInstallationParameters.S1R;
                Tx.Pitch   = DataSonarInstallationParameters.S1P;
                Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
                
                Rx.nb = 1;
                Rx.X       = DataSonarInstallationParameters.S1Y;
                Rx.Y       = DataSonarInstallationParameters.S1X;
                Rx.Z       = DataSonarInstallationParameters.S1Z;
                Rx.Roll    = DataSonarInstallationParameters.S1R;
                Rx.Pitch   = DataSonarInstallationParameters.S1P;
                Rx.Heading = DataSonarInstallationParameters.S1P;
                
            case 6 % Modular - Ex : EM2040M
                my_warndlg('EmModel and SystemTransducerConfiguration not pluged yet, please send a .all file to JMA', 1);
                
            case 7 % Portable Dual Head - Ex : EM2040P Dual Head
                my_warndlg('EmModel and SystemTransducerConfiguration not pluged yet, please send a .all file to JMA', 1);
                
            otherwise
                my_warndlg('EmModel and SystemTransducerConfiguration not explicit', 1);
        end
        
        %%%%%%
            case 2045 % EM2040C
                if isfield(DataSonarInstallationParameters, 'S1R')
                    Tx.nb = 1;
                    Tx.X       = DataSonarInstallationParameters.S1Y;
                    Tx.Y       = DataSonarInstallationParameters.S1X;
                    Tx.Z       = DataSonarInstallationParameters.S1Z;
                    Tx.Roll    = DataSonarInstallationParameters.S1R;
                    Tx.Pitch   = DataSonarInstallationParameters.S1P;
                    Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO

                    Rx.nb = 1;
                    Rx.X       = Tx.X;
                    Rx.Y       = Tx.Y;
                    Rx.Z       = Tx.Z;
                    Rx.Roll    = Tx.Roll;
                    Rx.Pitch   = Tx.Pitch;
                    Rx.Heading = Tx.Heading; % V�rifier TODO
                elseif isfield(DataSonarInstallationParameters, 'S3R')
                    Tx.nb = 2;
                    Tx.X       = [DataSonarInstallationParameters.S2Y DataSonarInstallationParameters.S3Y];
                    Tx.Y       = [DataSonarInstallationParameters.S2X DataSonarInstallationParameters.S3X];
                    Tx.Z       = [DataSonarInstallationParameters.S2Z DataSonarInstallationParameters.S3Z];
                    Tx.Roll    = [DataSonarInstallationParameters.S2R DataSonarInstallationParameters.S3R];
                    Tx.Pitch   = [DataSonarInstallationParameters.S2P DataSonarInstallationParameters.S3P];
                    Tx.Heading = [DataSonarInstallationParameters.S2H DataSonarInstallationParameters.S3H]; % V�rifier TODO

                    Rx.nb = 2;
                    Rx.X       = [DataSonarInstallationParameters.S2Y DataSonarInstallationParameters.S3Y];
                    Rx.Y       = [DataSonarInstallationParameters.S2X DataSonarInstallationParameters.S3X];
                    Rx.Z       = [DataSonarInstallationParameters.S2Z DataSonarInstallationParameters.S3Z];
                    Rx.Roll    = [DataSonarInstallationParameters.S2R DataSonarInstallationParameters.S3R];
                    Rx.Pitch   = [DataSonarInstallationParameters.S2P DataSonarInstallationParameters.S3P];
                    Rx.Heading = [DataSonarInstallationParameters.S2H DataSonarInstallationParameters.S3H]; % V�rifier TODO
                end
        %%%%%%
        
        
        
    case 1002
        Tx.nb = 1;
        Tx.X       = DataSonarInstallationParameters.S1Y;
        Tx.Y       = DataSonarInstallationParameters.S1X;
        Tx.Z       = DataSonarInstallationParameters.S1Z;
        Tx.Roll    = DataSonarInstallationParameters.S1R;
        Tx.Pitch   = DataSonarInstallationParameters.S1P;
        Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
        
        Rx.nb = 1;
        Rx.X       = DataSonarInstallationParameters.S1Y;
        Rx.Y       = DataSonarInstallationParameters.S1X;
        Rx.Z       = DataSonarInstallationParameters.S1Z;
        Rx.Roll    = DataSonarInstallationParameters.S1R;
        Rx.Pitch   = DataSonarInstallationParameters.S1P;
        Rx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
        
    case 850 % ME70BO S0x en principe mais S1x dans fichier transmis par Na�g
        Tx.nb = 1;
        Tx.X       = DataSonarInstallationParameters.S1Y;
        Tx.Y       = DataSonarInstallationParameters.S1X;
        Tx.Z       = DataSonarInstallationParameters.S1Z;
        Tx.Roll    = DataSonarInstallationParameters.S1R;
        Tx.Pitch   = DataSonarInstallationParameters.S1P;
        Tx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
        
        Rx.nb = 1;
        Rx.X       = DataSonarInstallationParameters.S1Y;
        Rx.Y       = DataSonarInstallationParameters.S1X;
        Rx.Z       = DataSonarInstallationParameters.S1Z;
        Rx.Roll    = DataSonarInstallationParameters.S1R;
        Rx.Pitch   = DataSonarInstallationParameters.S1P;
        Rx.Heading = DataSonarInstallationParameters.S1H; % V�rifier TODO
        
    otherwise
    	my_warndlg('EmModel not recognized', 1);
end            
this = set_SonarShipArraysTransmit(this, Tx);
this = set_SonarShipArraysReceive( this, Rx);

%% Position Sensor

if ~isfield(DataSonarInstallationParameters, 'APS')
    DataSonarInstallationParameters.APS = 1;
end
if ~isfield(DataSonarInstallationParameters, 'P1T')
    DataSonarInstallationParameters.P1T = 0;
end
if ~isfield(DataSonarInstallationParameters, 'P1Y')
    DataSonarInstallationParameters.P1Y = 0;
end
if ~isfield(DataSonarInstallationParameters, 'P1X')
    DataSonarInstallationParameters.P1X = 0;
end
if ~isfield(DataSonarInstallationParameters, 'P1Z')
    DataSonarInstallationParameters.P1Z = 0;
end

Nav.APS  = DataSonarInstallationParameters.APS + 1; % Active position system index
if isequal(DataSonarInstallationParameters.P1T, 'Datagram') % Ajout JMA le 18/06/2021 pour .kmall
    Nav.TimeStampUse(1) = 1;
else
%     Nav.TimeStampUse(1) = DataSonarInstallationParameters.P1T;
    Nav.TimeStampUse = DataSonarInstallationParameters.P1T; % Modif JMA le 03/11/2021
end
Nav.X(1) = DataSonarInstallationParameters.P1Y;
Nav.Y(1) = DataSonarInstallationParameters.P1X;
Nav.Z(1) = DataSonarInstallationParameters.P1Z;
if isfield(DataSonarInstallationParameters, 'P2Y')
    Nav.TimeStampUse(2) = DataSonarInstallationParameters.P2T;
    Nav.X(2) = DataSonarInstallationParameters.P2Y;
    Nav.Y(2) = DataSonarInstallationParameters.P2X;
    Nav.Z(2) = DataSonarInstallationParameters.P2Z;
end
if isfield(DataSonarInstallationParameters, 'P3Y')
    Nav.TimeStampUse(3) = DataSonarInstallationParameters.P3T;
    Nav.X(3) = DataSonarInstallationParameters.P3Y;
    Nav.Y(3) = DataSonarInstallationParameters.P3X;
    Nav.Z(3) = DataSonarInstallationParameters.P3Z;
end
this = set_SonarShipGPS(this, Nav);

%% Motion Sensor

if ~isfield(DataSonarInstallationParameters, 'VSN')
    DataSonarInstallationParameters.VSN = 1; % Cas rencontr� pour donn�es EM1002
end
switch DataSonarInstallationParameters.VSN
    case 0 % Oups ! Pas de centrale d'attitude utilis�e, on fait comme si c'�tait la MRU1
        MRU.X = DataSonarInstallationParameters.MSY;
        MRU.Y = DataSonarInstallationParameters.MSX;
        MRU.Z = DataSonarInstallationParameters.MSZ;
        MRU.RollCalibration    = DataSonarInstallationParameters.MSR;
        MRU.PitchCalibration   = DataSonarInstallationParameters.MSP;
        MRU.HeadingCalibration = DataSonarInstallationParameters.MSG;
        MRU.TimeDelay          = DataSonarInstallationParameters.MSD;
    case 1 % First Motion sensor system
        try
            MRU.X = DataSonarInstallationParameters.MSY;
            MRU.Y = DataSonarInstallationParameters.MSX;
            MRU.Z = DataSonarInstallationParameters.MSZ;
            MRU.RollCalibration    = DataSonarInstallationParameters.MSR;
            MRU.PitchCalibration   = DataSonarInstallationParameters.MSP;
            MRU.HeadingCalibration = DataSonarInstallationParameters.MSG;
            MRU.TimeDelay          = DataSonarInstallationParameters.MSD;
        catch % Cas de l'EM2000 des italiens de Pise
            MRU.X = 0;
            MRU.Y = 0;
            MRU.Z = 0;
            MRU.RollCalibration    = 0;
            MRU.PitchCalibration   = 0;
            MRU.HeadingCalibration = 0;
            MRU.TimeDelay          = 0;
       end
    case 2 % Second Motion sensor system
        MRU.X = DataSonarInstallationParameters.NSY;
        MRU.Y = DataSonarInstallationParameters.NSX;
        MRU.Z = DataSonarInstallationParameters.NSZ;
        MRU.RollCalibration    = DataSonarInstallationParameters.NSR;
        MRU.PitchCalibration   = DataSonarInstallationParameters.NSP;
        MRU.HeadingCalibration = DataSonarInstallationParameters.NSG;
        MRU.TimeDelay          = DataSonarInstallationParameters.NSD;
end
this = set_SonarShipMRU(this, MRU);
