function this = set_SonarInterlacing(this, X)
if ~isempty(X)
    this.Sonar.Interlacing = X;
end
