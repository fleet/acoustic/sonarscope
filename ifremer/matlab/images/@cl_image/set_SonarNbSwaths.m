function this = set_SonarNbSwaths(this, X)
if ~isempty(X)
    this.Sonar.NbSwaths = X;
end
