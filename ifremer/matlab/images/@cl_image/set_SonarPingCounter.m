function this = set_SonarPingCounter(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.PingCounter = X(:);
    else
        this.Sonar.PingCounter = X(:,:);
    end
end
