function this = set_SonarPitch(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.Pitch = X(:);
    else
        this.Sonar.Pitch = X(:,:);
    end
end
