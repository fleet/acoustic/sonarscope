function this = set_SonarPortMode_1(this, X)
if ~isempty(X)
    this.Sonar.PortMode_1 = X;
end
