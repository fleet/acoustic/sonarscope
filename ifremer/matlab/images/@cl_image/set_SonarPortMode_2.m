function this = set_SonarPortMode_2(this, X)
if ~isempty(X)
    this.Sonar.PortMode_2 = X;
end
