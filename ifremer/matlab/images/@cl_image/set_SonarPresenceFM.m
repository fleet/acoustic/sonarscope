function this = set_SonarPresenceFM(this, X)

if ~isempty(X)
    this.Sonar.PresenceFM = X;
end
