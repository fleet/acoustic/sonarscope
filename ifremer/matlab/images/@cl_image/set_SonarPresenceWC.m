function this = set_SonarPresenceWC(this, X)
if ~isempty(X)
    this.Sonar.PresenceWC = X;
end
