function this = set_SonarRTK_FixPosition(this, X)
if ~isempty(X)
    this.Sonar.RTK_FixPosition = X;
end
