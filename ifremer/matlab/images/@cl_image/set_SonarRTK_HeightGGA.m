function this = set_SonarRTK_HeightGGA(this, X)
if ~isempty(X)
    this.Sonar.RTK_HeightGGA = X;
end
