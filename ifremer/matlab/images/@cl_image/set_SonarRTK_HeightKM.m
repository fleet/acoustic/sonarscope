function this = set_SonarRTK_HeightKM(this, X)
if ~isempty(X)
    this.Sonar.RTK_HeightKM = X;
end
