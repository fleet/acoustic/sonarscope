function this = set_SonarResolAcrossSample(this, X)
if ~isempty(X)
    this.Sonar.ResolAcrossSample = X;
end
