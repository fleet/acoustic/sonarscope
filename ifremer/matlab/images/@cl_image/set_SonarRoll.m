function this = set_SonarRoll(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.Roll = X(:);
    else
        this.Sonar.Roll = X(:,:);
    end
end
