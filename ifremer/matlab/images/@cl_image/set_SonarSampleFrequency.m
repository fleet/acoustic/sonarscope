function this = set_SonarSampleFrequency(this, X)
if ~isempty(X)
    if size(X,1) == 1 % Pour raison de compatibilité avec anciennes versions
        this.Sonar.SampleFrequency = X(:); % Modifié le 27/04/2010
    else
        this.Sonar.SampleFrequency = X(:,:);
    end
end

