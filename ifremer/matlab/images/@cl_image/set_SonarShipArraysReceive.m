function this = set_SonarShipArraysReceive(this, X)

if ~isempty(X)
    this.Sonar.Ship.Arrays.Receive.X       = X.X;
    this.Sonar.Ship.Arrays.Receive.Y       = X.Y;
    this.Sonar.Ship.Arrays.Receive.Z       = X.Z;
    this.Sonar.Ship.Arrays.Receive.Roll    = X.Roll;
    this.Sonar.Ship.Arrays.Receive.Pitch   = X.Pitch;
    this.Sonar.Ship.Arrays.Receive.Heading = X.Heading;
end
