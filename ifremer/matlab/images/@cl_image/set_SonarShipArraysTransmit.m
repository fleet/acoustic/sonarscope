function this = set_SonarShipArraysTransmit(this, X)
if ~isempty(X)
    this.Sonar.Ship.Arrays.Transmit.nb      = X.nb;
    this.Sonar.Ship.Arrays.Transmit.X       = X.X;
    this.Sonar.Ship.Arrays.Transmit.Y       = X.Y;
    this.Sonar.Ship.Arrays.Transmit.Z       = X.Z;
    this.Sonar.Ship.Arrays.Transmit.Roll    = X.Roll;
    this.Sonar.Ship.Arrays.Transmit.Pitch   = X.Pitch;
    this.Sonar.Ship.Arrays.Transmit.Heading = X.Heading;
end
