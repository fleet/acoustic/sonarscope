function this = set_SonarShipGPS(this, X)
if ~isempty(X)
    if ~isfield(X, 'APS')
        X.APS = 1;
    end
    this.Sonar.Ship.GPS.APS = X.APS;
    this.Sonar.Ship.GPS.X   = X.X;
    this.Sonar.Ship.GPS.Y   = X.Y;
    this.Sonar.Ship.GPS.Z   = X.Z;
    if isfield(X, 'TimeStampUse')
        this.Sonar.Ship.GPS.TimeStampUse = X.TimeStampUse;
    end
end
