function this = set_SonarShipLatitude(this, X)
if ~isempty(X)
    this.Sonar.ShipLatitude = X;
end
