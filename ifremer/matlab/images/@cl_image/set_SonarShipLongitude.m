function this = set_SonarShipLongitude(this, X)
if ~isempty(X)
    this.Sonar.ShipLongitude = X;
end
