function this = set_SonarShipMRU(this, X)
if ~isempty(X)
    this.Sonar.Ship.MRU.X = X.X;
    this.Sonar.Ship.MRU.Y = X.Y;
    this.Sonar.Ship.MRU.Z = X.Z;
    this.Sonar.Ship.MRU.RollCalibration     = X.RollCalibration;
    this.Sonar.Ship.MRU.PitchCalibration    = X.PitchCalibration;
    this.Sonar.Ship.MRU.HeadingCalibration  = X.HeadingCalibration;
    this.Sonar.Ship.MRU.TimeDelay           = X.TimeDelay;
end
