function this = set_SonarShipWaterLine(this, X)
if ~isempty(X)
    this.Sonar.Ship.WaterLineVerticalOffset = X.VerticalOffset;
end
