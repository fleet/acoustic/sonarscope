function this = set_SonarSpeed(this, X)
if ~isempty(X)
    this.Sonar.Speed = X;
end

