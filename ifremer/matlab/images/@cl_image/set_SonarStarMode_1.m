function this = set_SonarStarMode_1(this, X)
if ~isempty(X)
    this.Sonar.StarMode_1 = X;
end
