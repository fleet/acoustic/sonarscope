function this = set_SonarStarMode_2(this, X)
if ~isempty(X)
    this.Sonar.StarMode_2 = X;
end
