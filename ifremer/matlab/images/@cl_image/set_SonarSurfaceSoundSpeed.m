function this = set_SonarSurfaceSoundSpeed(this, X)
if ~isempty(X)
    this.Sonar.SurfaceSoundSpeed = X;
end

