function this = set_SonarTVGCrossOver(this, X)
if ~isempty(X)
    this.Sonar.TVGCrossOver = X;
end
