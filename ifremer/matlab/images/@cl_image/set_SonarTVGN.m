function this = set_SonarTVGN(this, X)
if ~isempty(X)
    this.Sonar.TVGN = X;
end
