function this = set_SonarTide(this, X)
if ~isempty(X)
    this.Sonar.Tide = X(:);
end
