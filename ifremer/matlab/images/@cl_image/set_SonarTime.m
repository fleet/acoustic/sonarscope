function this = set_SonarTime(this, X)
if ~isempty(X)
    if isa(X, 'datetime')
        X = cl_time('timeMat', datenum(X));
    end
    this.Sonar.Time = X;
end
