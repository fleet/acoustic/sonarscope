function this = set_SonarTrueHeave(this, X)
if ~isempty(X)
    this.Sonar.TrueHeave = X(:);
end
