function this = set_SonarTxAcrossAngle(this, X)
if ~isempty(X)
    this.Sonar.TxAcrossAngle = X;
end
