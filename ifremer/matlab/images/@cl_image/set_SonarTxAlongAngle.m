function this = set_SonarTxAlongAngle(this, X)
if ~isempty(X)
    this.Sonar.TxAlongAngle = X(:);
end
