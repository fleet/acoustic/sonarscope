function this = set_SonarTxBeamWidth(this, X)
if ~isempty(X)
    this.Sonar.TxBeamWidth = X;
end
