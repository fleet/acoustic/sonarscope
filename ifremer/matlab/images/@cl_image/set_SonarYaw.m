function this = set_SonarYaw(this, X)
if ~isempty(X)
    this.Sonar.Yaw = X;
end
