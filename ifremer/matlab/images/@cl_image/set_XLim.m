% Set XLim (limits in x coordinates used for the image display)
%
% Syntax
%  a = set_XLim(a, XLim)
%
% Input Arguments
%   a    : One cl_image instance
%   XLim : Limits of the image in x coordinates
%
% Output Arguments
%   a : The updated instance
%
% Remarks : XLim is set by default when setting x.
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%     XLim = a.XLim
%   a = set_XLim(a, [24 25.5]);
%     imagesc(a) % TODO : l'image s'affiche en entier !
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_XLim(this, X)
this.XLim = X;
