% Set YLim (limits in y coordinates used for the image display)
%
% Syntax
%  a = set_YLim(a, YLim)
%
% Input Arguments
%   a    : One cl_image instance
%   YLim : Limits of the image in y coordinates
%
% Output Arguments
%   a : The updated instance
%
% Remarks : YLim is set by default when setting y.
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(a)
%     YLim = a.YLim
%   a = set_YLim(a, [33 34.5]);
%     imagesc(a)
%
% See also cl_image/set_y Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_YLim(this, X)
this.YLim = X;