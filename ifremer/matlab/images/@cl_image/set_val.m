% Ecriture des valeurs dans l'image aux coordonnees specifiees par les coordonnees i, j
%
% Syntax
%   a = set_val(a, j, i, val)
% 
% Input Arguments
%   a : instance de cl_image
%   j : numeros des lignes
%   i : numeros des colonnes
%   val : valeurs des pixels
%   a : instance de cl_image
%
% Output Arguments
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_val(this, j, i, val)

if isempty(j)
    j = 1:this.nbRows;
end
if isempty(i)
    i = 1:this.nbColumns;
end

this.Image(j, i) = val;
