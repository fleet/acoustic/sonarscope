% Ecrit les valeurs de l'image specifiées par les coordonnées i, j
%
% Syntax
%   a = set_val_xy(a, x, y, val)
% 
% Input Arguments
%   a : instance de cl_image
%   x : Abscisses
%   y : Ordonnées
%   val : valeurs des pixels
%
% Output Arguments
%   a : instance de cl_image
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_val_xy(this, valX, valY, val)

x   = this.x;
y   = this.y;
ix  = interp1(x, 1:length(x), valX, 'nearest', 'extrap');
iy  = interp1(y, 1:length(y), valY, 'nearest', 'extrap');

% this.Image(iy, ix) = val;
for i=1:length(iy)
%     sub = ~isnan(val(i,:));
%     this.Image(iy(i), ix(sub)) = val(i,sub);
    this.Image(iy(i), ix) = val(i,:);
end
