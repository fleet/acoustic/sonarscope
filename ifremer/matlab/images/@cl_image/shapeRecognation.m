function this = shapeRecognation(this, I2, TypeAlgo, flagBosseOuCreux, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

tic

I = this.Image(suby,subx,:);
I = singleUnlessDouble(I, this.ValNaN);

szI2 = size(I2);

n2 = numel(I2);
n2s2 = n2 / 2;

I2 = singleUnlessDouble(I2, this.ValNaN);
I2 = I2 - mean(I2(:), 'omitnan');

% I    = gpuArray(I); % Tentative d'utiliser le GP2 mais c'est hyper lent
% I2   = gpuArray(I2); % Tentative d'utiliser le GP2 mais c'est hyper lent
% szI2 = gpuArray(szI2);

switch TypeAlgo
    case 1 % RMS
        strAlgo = 'RMS';
%         %         threshold = max(I2(:)) - min(I2(:));
%         %         threshold = threshold * 10;
%         threshold = Inf;
        %         pppp = colfilt(I, szI2, 'sliding', @rms)
        
        B2 = nlfilter(I, szI2, @rms);
%         B2 = gather(B2);
        B2 = 10 * log10(1 ./ (1+B2))           * 1000; % !!!!!!!!!!!!!! pour GLOBE
        
    case 2 % Crosscorrelation
        strAlgo = 'CrossCor';
%         threshold = Inf;
        B2 = nlfilter(I, szI2, @mycorr);
%         B2 = gather(B2);
        B2 = 10 * log10(B2+eps)                * 1000; % !!!!!!!!!!!!!! pour GLOBE
end

%% Tentative de d�termination des panaches verticaux  : TODO : Arnaud ?

%% Version vlfeat
%{
% L'ajout du path a �t� comment� dans ifrStartupContributions car il y a
% beaucoup de fonctions qui ont le m�me nom de des fonctions Matlab, �a peut
% engendrer des effets de bords (cummax, det, ...). On pr�f�re d�finir le
% path localement

global IfrTbx %#ok<GVMIS>
demo_path = genpath(fullfile(IfrTbx, 'contributions', 'vlFeatToolbox', 'vlfeat-0.9.21', 'toolbox'));
addpath(demo_path);
% path(path, genpath(fullfile(IfrTbx, 'contributions', 'vlFeatToolbox', 'vlfeat-0.9.21', 'toolbox')))
% vl_setup

f = WC_extractFeatures(B2, ...
'x',              this.x(subx), ...
'y',              this.y(suby), ...
'EllipseAngle',   90, ...
'EllipseRatio',   1, ...
'minClusterDist', 20);

rmpath(demo_path);
%}

%% Version Matlab

%{
bw = (B2 > -2000); %
statsBW = regionprops('table', bw, 'Centroid', 'MajorAxisLength', 'MinorAxisLength', 'Orientation');
figure; imagesc(B2); axis xy, hold on;
plot_ellipse(statsBW.MajorAxisLength, statsBW.MinorAxisLength, statsBW.Centroid(:,1), statsBW.Centroid(:,2), statsBW.Orientation, 'r'); axis equal;
%}

%% Output image

DT = cl_image.indDataType('Weight'); % TODO : pas s�r que ce soit le bon type
Append = cl_image.strDataType{this.DataType};
Append = [Append(1:floor(length(Append) / 2)) ' - ' strAlgo];
this = inherit(this, B2, 'subx', subx, 'suby', suby, 'DataType', DT, 'Unit', '', ...
    'ColormapIndex', 3, 'AppendName', Append);
toc

    function y = rms(I1)
        sub = ~isnan(I1);
        I1 = I1(sub);
        n1 = numel(I1);
        if n1 < n2s2
            y = NaN;
        else
            I1 = I1 - mean(I1);
            y = I1 - I2(sub);
            y = sqrt(sum(y .* y, 'omitnan') / n1);
        end
    end

    function y = mycorr(I1)
        sub = ~isnan(I1);
        I1 = I1(sub);
        n1 = numel(I1);
        if n1 < (n2s2)
            y = NaN;
        else
            I1 = I1 - mean(I1);
            I2s = I2(sub);
            y = dot(I1 / norm(I1), I2s / norm(I2s));
            if y <= 0
                y = NaN;
            end
        end
    end
end
