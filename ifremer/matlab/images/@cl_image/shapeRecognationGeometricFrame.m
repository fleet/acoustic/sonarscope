function [flag, that] = shapeRecognationGeometricFrame(this, typeAlgo, FrameDescription, varargin)

N = length(this);
hw = create_waitbar(waitbarMsg('shapeRecognationGeometricFrame'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = shapeRecognationGeometricFrame_unitaire(this(k), typeAlgo, FrameDescription, varargin{:});
    if ~flag
        that = [];
        return
    end
    that(k) = X; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')


function [flag, this] = shapeRecognationGeometricFrame_unitaire(this, typeAlgo, FrameDescription, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = 0;

%% Algorithm

deltax = this.XStep;
switch FrameDescription.TypeShape
    case 1 % A top hat
        nBase = floor(FrameDescription.TopHat.BaseDiameter / (2*deltax));
        nTop  = floor(FrameDescription.TopHat.TopDiameter  / (2*deltax));
        h = fspecial('disk', nBase);
        h(:) = 0;
        hTop = fspecial('disk', nTop);
        sub1 = (1:(2*nTop+1)) + (nBase-nTop);
        h(sub1,sub1) = hTop;
        I2 = (h / max(h(:))) * FrameDescription.TopHat.SignalAmplitude;
        flagBosseOuCreux = FrameDescription.TopHat.SignalAmplitude > 0;
        
    case 2 % A gaussion : Ecart-type, hauteur
        n = floor(FrameDescription.Gaussian.Stdx2 * 2 / deltax); % TODO remplacer par FrameDescription.Gaussian.Width
        h = fspecial('gaussian', n, FrameDescription.Gaussian.Stdx2);
        I2 = (h / max(h(:))) * FrameDescription.Gaussian.SignalAmplitude;
        flagBosseOuCreux = FrameDescription.Gaussian.SignalAmplitude > 0;
        
    case 3 % A cone : diam�tre base, hauteur
        nBase = floor(FrameDescription.Cone.BaseDiameter / (2*deltax));
        h = fspecial('disk', nBase);
        h(:) = 0;
        for k1=1:(2*nBase+1)
            for k2=1:(2*nBase+1)
                h(k1,k2) = sqrt((nBase+1-k1)^2 + (nBase+1-k2)^2);
            end
        end
        h(h > nBase) = nBase;
        I2 = FrameDescription.Cone.SignalAmplitude - (h / max(h(:))) * FrameDescription.Cone.SignalAmplitude;
        flagBosseOuCreux = FrameDescription.Cone.SignalAmplitude > 0;
        
    case 4 % A Water Column fluid echo : Width, Ecart-type, hauteur
        n2 = floor(FrameDescription.GasPlume.Width  / deltax);
        n1 = floor(FrameDescription.GasPlume.Height / deltax);
        sigma = FrameDescription.GasPlume.Stdx2 / (2*deltax);
        ns2 = floor(n2/2);
        x = -ns2:ns2;
        I2 = gaussCurve(x, [0 sigma 0 FrameDescription.GasPlume.SignalAmplitude]);
        I2 = repmat(I2, n1, 1);
        flagBosseOuCreux = FrameDescription.GasPlume.SignalAmplitude > 0;
        
    case 5 % An image snippet
        I2 = double(FrameDescription.Snippet.Image(:,:));
        if any(isnan(I2))
            my_warndlg('There are NaN values in I2', 1);
            return
        end
        flagBosseOuCreux = 1;
end

this = shapeRecognation(this, I2, typeAlgo, flagBosseOuCreux, varargin{:});
flag = 1;