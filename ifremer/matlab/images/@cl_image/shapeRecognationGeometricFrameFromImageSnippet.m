function shapeRecognationGeometricFrameFromImageSnippet(Snippet, typeShape)

I2 = double(Snippet.Image(:,:));

switch typeShape
    case 1 % A top hat
        my_warndlg('Sorry, not done yet', 1);
        
    case 2 % A gaussian
        my_warndlg('Sorry, not done yet', 1);
        
    case 3 % A cone
        my_warndlg('Sorry, not done yet', 1);
        
    case 4 % A Water Column fluid echo
        if any(isnan(I2))
            my_warndlg('There are NaN values in I2, please interpolate it before.', 1);
            return
        end
        [sigma, width, height, amplitude] = estimateGasPlumeParameters(I2, Snippet.x, Snippet.y);
        
        fprintf('2*sigma   = %s\n', num2str(2*sigma));
        fprintf('width     = %s\n', num2str(width));
        fprintf('height    = %s\n', num2str(height));
        fprintf('amplitude = %s\n', num2str(amplitude));
end


function [sigma, width, height, amplitude] = estimateGasPlumeParameters(I2, x, y)

z = mean(I2);

% minZ = min(z);
% maxZ = max(z);
% [Ex, Sigma] = CenterGravity(x, z);
% plot([Ex-Sigma Ex+Sigma], [minZ minZ], 'k');
% plot([Ex Ex], [minZ maxZ], 'k');
% zPdf = normpdf(x, Ex, Sigma/2);
% plot(x, minZ + zPdf * (maxZ-minZ)/max(zPdf), 'r'); grid on; hold on
% str = sprintf('Sigma=%f', round(Sigma, 3, 'significant'));
% title(str)

[params, str2] = gaussCurveFit(x, z, 'GUI', 0);

zFitted = gaussCurve(x, params);

step = x(2) - x(1);
n = length(x)/2;
x2 = linspace(x(1)-(n*step), x(end)+(n*step), length(x)+2*n);
zFitted2 = gaussCurve(x2, params);

sigma     = params(2);
width     = sigma*4;
height    = width; % max(y) - min(y);
amplitude = params(4) - params(3);

figure;
h(1) = subplot(2,1,1); imagesc(x, y, I2); colormap(jet(256)); colorbar('northoutside');
h(2) = subplot(2,1,2); plot(x, z); grid on; hold on; plot(x2, zFitted2, 'g'); title(str2); plot(x, zFitted, 'r'); title(str2);
linkaxes(h, 'x');
