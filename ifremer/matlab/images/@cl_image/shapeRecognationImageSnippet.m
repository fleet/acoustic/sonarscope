function [flag, that] = shapeRecognationImageSnippet(this, Snippet, typeAlgo, varargin)

N = length(this);
hw = create_waitbar(waitbarMsg('shapeRecognationImageSnippet'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = shapeRecognationImageSnippet_unitaire(this(k), Snippet, typeAlgo, varargin{:});
    if ~flag
        that = [];
        return
    end
    that(k) = X; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')


function [flag, this] = shapeRecognationImageSnippet_unitaire(this, Snippet, typeAlgo, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

flag = 0;

%% Algorithm

I2 = double(Snippet.Image(:,:));
if any(isnan(I2))
    my_warndlg('There are NaN values in I2', 1);
    return
end
flagBosseOuCreux = 1;


this = shapeRecognation(this, I2, typeAlgo, flagBosseOuCreux, varargin{:});
flag = 1;