% Sign of image
%
% Syntax
%  b = sign(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Remarks : sign of values equal to 0 is 0
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = sign(a);
%   imagesc(b);
%
% See also cl_image/abs Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = sign(this, varargin)
that = process_function_type1(this, @sign, 'Unit', [], varargin{:});
