% Sine of angles in deg
%
% Syntax
%   b = sind(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(linspace(-180, 180, 200), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'deg');
%   imagesc(a);
%
%   b = sind(a);
%   imagesc(b);
%
% See also cl_image/acos cl_image/cosd Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = sind(this, varargin)
that = process_function_type1(this, @sind, 'Unit', [], 'expectedUnit', 'deg', varargin{:});
