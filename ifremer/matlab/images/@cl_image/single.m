% Convert image to single precision
%
% Syntax
%  b = single(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a);
%
%   b = single(a);
%   imagesc(b);
%
% See also cl_image/doble Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = single(this, varargin)
that = process_function_type1(this, @single, varargin{:});
