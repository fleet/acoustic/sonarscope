% Slope in a direction given for an azimuth
%
% Syntax
%   [b, flag] = slopeAzimuth(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Azimuth : Azimuth(deg, 0=North, 90=East)
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   [b, flag] = slopeAzimuth(a);
%   imagesc(b);
%
%   [b, flag] = slopeAzimuth(a, 'Azimuth', 90);
%   imagesc(b);
%
% See also cl_image/slopeDirection cl_image/slopeMax Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = slopeAzimuth(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('slopeAzimuth'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K] = slopeAzimuth_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = slopeAzimuth_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Azimuth] = getPropertyValue(varargin, 'Azimuth', this.Azimuth);
[varargin, Unit]    = getPropertyValue(varargin, 'Unit', 'deg'); %#ok<ASGLU> % deg, rd, ratio

that = cl_image.empty;

%% Controls

[flag, msg] = is_metrique(this);
if ~flag
    my_warndlg(msg, 1);
    return
end

%% Algorithm

stepx = (length(subx) - 1) / (subx(end) - subx(1));

suby = sort(suby);
stepy = (length(suby) - 1) / (suby(end) - suby(1));

yDir = ((this.y(end) - this.y(1)) > 0);
if ~xor(yDir , (this.YDir == 1))
    Angle = 180 + Azimuth;
else
    Angle = -Azimuth;
end

XStepMetric = this.XStepMetric;
if isnan(XStepMetric)
    XStepMetric = 1;
end

YStepMetric = this.YStepMetric;
if isnan(YStepMetric)
    YStepMetric = 1;
end

angle = mod(Angle + 360, 360) * (pi/180);
alpha = sin(angle);
beta  = cos(angle);

h = [-1 -2 -1; 0 0 0; 1 2 1];
h = h / sum(abs(h(:)));

hx = h' / (XStepMetric / stepx);
hy = h  / (YStepMetric / stepy);

[I, subNaN] = windowEnlarge(this.Image(suby,subx), [3 3], []);

switch alpha
    case 1
        I = filter2(hx, I, 'valid');
    case -1
        I = -filter2(hx, I, 'valid');
    case 0
        I = beta * filter2(hy, I, 'valid');
    otherwise
        Ix = filter2(hx, I, 'valid');
        Iy = filter2(hy, I, 'valid');
        I = alpha * Ix + beta * Iy;
end

switch Unit
    case 'deg'
        I = atand(I);
    case 'rd'
        I = atan(I);
    case 'ratio'
        %             I = I;
    otherwise
        % Inconnu au bataillon
end

I(subNaN) = NaN;
Image(:,:) = I;

%% Output image

if this.GeometryType == cl_image.indGeometryType('PingAcrossDist') % SonarX
    if Azimuth == 0
        DataType = cl_image.indDataType('SlopeAlong');
    elseif Azimuth == 90
        DataType = cl_image.indDataType('SlopeAcross');
    else
        DataType = cl_image.indDataType('SlopeAzimuth');
    end
else
    DataType = cl_image.indDataType('SlopeAzimuth');
end

Parameters = getHistoryParameters(Azimuth);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', ['az' num2str(Azimuth)], 'ValNaN', NaN, ...
    'ColormapIndex', 3, 'Unit', Unit, 'DataType', DataType, 'Parameters', Parameters);
