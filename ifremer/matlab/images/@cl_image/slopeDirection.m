% Direction of the greatest slope
%
% Syntax
%   [b, flag] = slopeDirection(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   [b, flag] = slopeDirection(a);
%   imagesc(b);
%
% See also cl_image/slopeAzimuth cl_image/slopeMax Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = slopeDirection(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('slopeDirection'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K] = slopeDirection_unit(this(k), (N == 1), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = slopeDirection_unit(this, VisuVecteurs, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, step] = getPropertyValue(varargin, 'Step', []); %#ok<ASGLU>

if isempty(step)
    step = max(1, ceil(length(subx) / 100));
    %  pasy = max(1, ceil(length(suby) / 100));
end

that = cl_image.empty;

%% Controls

[flag, msg] = is_metrique(this);
if ~flag
    my_warndlg(msg, 1);
    return
end

%% Algorithm

XStepMetric = this.XStepMetric;
if isnan(XStepMetric)
    XStepMetric = 1;
end

YStepMetric = this.YStepMetric;
if isnan(YStepMetric)
    YStepMetric = 1;
end

stepx = (length(subx) - 1) / (subx(end) - subx(1));
suby = sort(suby);
stepy = (length(suby) - 1) / (suby(end) - suby(1));

h = [-1 -2 -1; 0 0 0; 1 2 1];
h = h / sum(abs(h(:)));
hx = h' / (XStepMetric / stepx);
hy = h  / (YStepMetric / stepy);

deltay = this.y(end) - this.y(1);
if deltay > 0
    signe = -1; % MNT
    hy = - hy;
else
    signe = 1; % Earth
end

I = this.Image(suby,subx);
I = singleUnlessDouble(I, this.ValNaN);
[I, subNaN] = windowEnlarge(I, [3 3], []);

V = filter2(hy, I, 'valid');
H = filter2(hx, I, 'valid');

%     J = 90 + angle(H + 1i*V) * (180/pi);
J = angle(H + 1i*V) * (180/pi) - 90;
J = mod(360 + J, 360);

J(subNaN) = NaN;

flag = 1;
if flag
    x = this.x(subx);
    y = this.y(suby);

    K = downsize(this.Image(suby,subx), 'pasy', step, 'pasx', step, 'Method', 'mean');
    [px,py] = gradient(K);
    Qx = linspace(x(1), x(end), size(K,2));
    Qy = linspace(y(1), y(end), size(K,1));
    if VisuVecteurs
        FigUtils.createSScFigure; imagesc(x, y, this.Image(suby,subx));
        colormap(jet(256)); hold on; quiver(Qx, Qy, -px, signe*py); axis xy; colorbar;
    end
end
Image(:,:) = J;

%% Output image

that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'slopeDirection', 'ValNaN', NaN, 'ColormapIndex', 5, 'Unit', 'deg', 'DataType', cl_image.indDataType('SlopeAzimuth'));
