% Greatest slope
%
% Syntax
%   [b, flag] = slopeMax(a, ...) 
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples 
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   [b, flag] = slopeMax(a);
%   imagesc(b);
%
% See also cl_image/slopeDirection cl_image/slopeAzimuth Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = slopeMax(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('slopeMax'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K] = slopeMax_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = slopeMax_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

that = cl_image.empty;

%% Controls

[flag, msg] = is_metrique(this);
if ~flag
    my_warndlg(msg, 1);
    return
end

%% Algorithm

XStepMetric = this.XStepMetric;
if isnan(XStepMetric)
    XStepMetric = 1;
end

YStepMetric = this.YStepMetric;
if isnan(YStepMetric)
    YStepMetric = 1;
end

h = [-1 -2 -1; 0 0 0; 1 2 1];
h = h / sum(abs(h(:)));
hx = h' / XStepMetric;
hy = h  / YStepMetric;
[I, subNaN] = windowEnlarge(this.Image(suby,subx), [3 3], []);

H = filter2(hx, I, 'valid');
V = filter2(hy, I, 'valid');

I =  (H .^ 2 + V .^ 2) .^ 0.5;
I = atand(I);
I(subNaN) = NaN;
Image(:,:) = I;

%% Output image

that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'slopeMax', 'ValNaN', NaN, ...
    'ColormapIndex', 3, 'Unit', 'deg', 'DataType', cl_image.indDataType('SlopeMax'));
