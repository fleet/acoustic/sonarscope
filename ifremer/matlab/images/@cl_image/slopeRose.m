% Creates the direction rose of slopes and
%
% Syntax
%   [b, flag] = slopeRose(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%
% Output Arguments
%   b    : Instance(s) of cl_image 3 images are creates for evry instance of a
%          First image is maximum slope
%          Second image is contains directions
%          Third is a segmentation of slopes based on an analysis of cross
%          histogram of maximul slopes and directions
%   flag : 1=OK, 0=KO
%
% Remarks : 2 figures are created : an image of the cross histogram of
% maximul slopes and directions and the figure of the direction rose of
% directions.
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   [b, flag] = slopeRose(a);
%   imagesc(b);
%
% See also cl_image/slopeDirection cl_image/slopeMax Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = slopeRose(this, varargin)

flag = 0;

N = length(this);
that = repmat(cl_image,3,N);
hw = create_waitbar(waitbarMsg('slopeRose'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = slopeRose_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(:,k) = X;
    if N > 1
        that(:,k) = optimiseMemory(that(:,k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)
that = that(:);


function [flag, that] = slopeRose_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Seuil] = getPropertyValue(varargin, 'Seuil', 100); %#ok<ASGLU>

%% Contr�le si une pente_rose peut �tre calcul�e

[flag, msg] = is_metrique(this);
if ~flag
    my_warndlg(msg, 1);
    that = [];
    return
end

ImageName = this.Name;

%% Algorithm

XStepMetric = this.XStepMetric;
if isnan(XStepMetric)
    XStepMetric = 1;
end

YStepMetric = this.YStepMetric;
if isnan(YStepMetric)
    YStepMetric = 1;
end

stepx = (length(subx) - 1) / (subx(end) - subx(1));
suby = sort(suby);
stepy = (length(suby) - 1) / (suby(end) - suby(1));

h = [-1 -2 -1; 0 0 0; 1 2 1];
h  = h / sum(abs(h(:)));

hx = h' / (XStepMetric / stepx);
hy = h  / (YStepMetric / stepy);

[PenteMaxDeg, subNaN] = windowEnlarge(this.Image(suby,subx), [3 3], []);

V = filter2(hy, PenteMaxDeg, 'valid');
H = filter2(hx, PenteMaxDeg, 'valid');

%% Pente

PenteMaxDeg =  (H .^ 2 + V .^ 2) .^ 0.5;
PenteMaxDeg = atand(PenteMaxDeg);
PenteMaxDeg(subNaN) = NaN;

%% Image de la pente max

that(1) = inherit(this, PenteMaxDeg, 'subx', subx, 'suby', suby, 'AppendName', 'SlopeMax', 'ValNaN', NaN, 'ColormapIndex', 3, 'Unit', 'deg');

%% Image des directions

DirectionMaxDeg = 90 + angle(H + 1i*V) * (180/pi);
DirectionMaxDeg = mod(360 - DirectionMaxDeg, 360);

DirectionMaxDeg(subNaN) = NaN;

that(2) = inherit(this, DirectionMaxDeg, 'subx', subx, 'suby', suby, 'AppendName', 'SlopeAzimuth', ...
    'ValNaN', NaN, 'ColormapIndex', 5, 'Unit', 'deg', 'Sampling', 1);

%% R�cup�ration des histogrammes

Slope       = that(1).HistoCentralClasses;
Azimuth     = that(2).HistoCentralClasses;
HistAzimuth = that(2).HistoValues;

%% Trac� de l'histogramme bidimentionnel

nomXY = sprintf('Bidimensional histogram of "%s"', ImageName);
nomX = 'Directions';
nomY = 'Slopes';
histo2D(DirectionMaxDeg, PenteMaxDeg, Azimuth, Slope, 'nomX', nomX, 'nomY', nomY, 'nomXY', nomXY);

% {
[N2, ~, ~, ~, Sub2] = histo2D(DirectionMaxDeg, PenteMaxDeg, Azimuth, Slope, 'nomX', nomX, 'nomY', nomY, 'nomXY', nomXY);
% figure; surf(Azimuth, Slope, N2); shading flat
a = cl_image('Image', N2, 'x', double(Azimuth), 'y', double(Slope), 'Unit', 'Nb', ...
    'Name', 'Histo2D', ...
    'XLabel', 'Directions', 'XUnit', 'deg', ...
    'YLabel', 'Slopes', 'YUnit', 'deg');

Repeat = true;
while Repeat
    b = watershed(a, 'Seuil', Seuil);
    % SonarScope([a b(2)])
    Maxb2 = max(b(2));
    if Maxb2 >= 2
        if Maxb2 > 3
            Seuil = Seuil * 1.1;
        else
            Repeat = false;
        end
    else
        Seuil = Seuil / 1.5;
    end
end

Mask = b(2).Image;
ImageSegmentee = zeros(size(DirectionMaxDeg), 'uint8');
for kSlope=1:length(Slope)
    for kAzimuth=1:length(Azimuth)
        ImageSegmentee(Sub2{kSlope, kAzimuth}) = Mask(kSlope, kAzimuth);
    end
end
% SonarScope(ImageSegmentee)

DataType = cl_image.indDataType('Segmentation');
that(3)  = inherit(this, ImageSegmentee, 'subx', subx, 'suby', suby, 'AppendName', 'facies', 'ValNaN', NaN, 'ColormapIndex', 3, 'Unit', 'deg', 'DataType', DataType);

%% Trac� de la rosace

Titre = sprintf('Rose of "%s"', ImageName);
figure
% h(3) = subplot(1,2,1); polar(DirectionMaxDeg(:) * (pi/180), PenteMaxDeg(:), '.')
% h(4) = subplot(1,2,2);
hAxe = subplot(1,1,1);
polarplot(hAxe, Azimuth * (pi/180), HistAzimuth); title(Titre);
set(hAxe, 'CameraUpVector', [1 0 0], 'YDir', 'reverse')

%{
[coefs,scores,variances,t2] = pca([Azimuth' HistAzimuth']);
subNonNaN = ~isnan(DirectionMaxDeg);
[coefs,scores,variances,t2] = pca([DirectionMaxDeg(subNonNaN) PenteMaxDeg(subNonNaN)]);
% biplot(coefs(:,1:2),'Scores',scores(:,1:2),'VarLabels', {'X1' 'X2'})
figure; plot(Azimuth, HistAzimuth); grid on;
coefs
%}

% subNonNaN = ~isnan(PenteMaxDeg);
% X = cosd(DirectionMaxDeg(subNonNaN)) .* PenteMaxDeg(subNonNaN);
% Y = sind(DirectionMaxDeg(subNonNaN)) .* PenteMaxDeg(subNonNaN);
%
% X = X(1:10:end);
% Y = Y(1:10:end);
%
% figure;
% scatter(X, Y, 10, '.'); grid on; hold on;
% % plot(X,Y,'.'); grid on; hold on;
%
% options = statset('Display','final');
% obj = gmdistribution.fit([X Y], 2, 'Options',options);
% obj.mu
% h = ezcontour(@(x,y)pdf(obj,[x y]), [min(X) max(X)], [min(Y) max(Y)]); grid on;
%
% obj = gmdistribution.fit([Azimuth(:) HistAzimuth(:)], 2)
% obj.mu

nbGaussiennes = 4;
fitHisto1D(Azimuth, HistAzimuth / sum(HistAzimuth), nbGaussiennes, 'Edit');

%% Par ici la sortie

flag = 1;
