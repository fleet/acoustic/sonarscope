% Smooths the contours of a segmentation
%
% Syntax
%  b = smoothContours(a)
%
% Input Arguments
%   a      : Instance(s) of cl_image
%   Radius : Radius of the structurent element
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance(s) of cl_image
%
% Examples
%   a = TODO : prendre une segmentation sur une image sonar
%   imagesc(a);
%
%   b = smoothContours(a);
%   imagesc(b);
%
% See also regions_homogenes Authors
% Authors : MM
% ----------------------------------------------------------------------------

function [flag, that] = smoothContours(this, Radius, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('smoothContours'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K] = smoothContours_unit(this(k), Radius, varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end  
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = smoothContours_unit(this, Radius, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

that = [];

%% Control

ident = cl_image.indDataType('Segmentation');
flag  = testSignature(this, 'DataType', ident);
if ~flag
    return
end

%% Algorithm

se = strel('disk', Radius);
I  = this.Image(suby,subx,:);
I  = imopen(I, se);

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'smoothContours');
