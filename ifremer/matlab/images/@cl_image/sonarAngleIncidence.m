function [flag, a] = sonarAngleIncidence(this, indImage, subx, suby, varargin)

[varargin, CreateLayersPente] = getPropertyValue(varargin, 'CreateLayersPente', 0); %#ok<ASGLU>

GT = this(indImage).GeometryType;
if GT == cl_image.indGeometryType('LatLong') % Cas d'une image de g�om�trie "LatLong"
    [flag, a] = sonar_calculAngleIncidenceinLatLongGeometry(this, indImage, 'CreateLayersPente', CreateLayersPente);
else
    [flag, a] = sonarLateralIncidence(this, indImage, subx, suby);
end
