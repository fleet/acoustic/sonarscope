% Creation de l'image ""EmissionBeam"
%
% Syntax
%   [flag, a] = sonarCalculCoordGeo(this, ...)
%
% Input Arguments
%   this : Instance de clc_image
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1:ca c'est bien passe, 0 sinon
%   a    : Instances de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonarCalculCoordGeo(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin);

[varargin, Mute]      = getPropertyValue(varargin, 'Mute',      0);
[varargin, NoWaitbar] = getPropertyValue(varargin, 'NoWaitbar', 0);

flag = 0;
a    = [];

GeometryType     = get(this(indImage),   'GeometryType');
SonarDescription = get(this(indImage),   'SonarDescription');
SonarFamily      = get(SonarDescription, 'Sonar.Family');

FishLatitude = get(this(indImage), 'FishLatitude');
if isempty(FishLatitude)
    str1 = '"SonarFishLatitude" non renseign� dans cette image';
    str2 = '"SonarFishLatitude" not assigned in this image';
    my_warndlg(Lang(str1,str2), 1);
    return
end

FishLongitude = get(this(indImage), 'FishLongitude');
if isempty(FishLongitude)
    str1 = '"SonarFishLongitude" non renseign� dans cette image';
    str2 = '"SonarFishLongitude" not assigned in this image';
    my_warndlg(Lang(str1,str2), 1);
    return
end

Heading = get(this(indImage), 'Heading');
if isempty(Heading) || all(isnan(Heading(:)))
    if isdeployed
        str1 = '"Heading" non renseign� dans cette image';
        str2 = '"Heading" not assigned in this image';
        my_warndlg(Lang(str1,str2), 1);
        return
    else
        my_warndlg('sonarCalculCoordGeo : Heading non renseign�, on calcule le cap seulement si version de d�v', 0);
        Heading = calCapFromLatLon(FishLatitude, FishLongitude);
    end
end

if GeometryType == cl_image.indGeometryType('PingAcrossSample')
    TagSynchroY = this(indImage).TagSynchroY;
    
    identAlongDistance  = cl_image.indDataType('AlongDist');
    identAcrossDistance = cl_image.indDataType('AcrossDist');
    
    indLayerAcrossDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AcrossDist');
    indLayerAlongDistance  = findIndLayerSonar(this, indImage, 'strDataType', 'AlongDist');
    
    if (GeometryType ~= cl_image.indGeometryType('PingAcrossSample')) && (isempty(indLayerAcrossDistance) || isempty(indLayerAlongDistance))
        for k=1:length(this)
            TY = this(k).TagSynchroY;
            if strcmp(TY, TagSynchroY)
                TC = this(k).GeometryType;
                TD = this(k).DataType;
                if TC == 7 % BathyFais
                    if TD == identAlongDistance
                        indLayerAlongDistance = k;
                    end
                    if TD == identAcrossDistance
                        indLayerAcrossDistance = k;
                    end
                end
            end
        end
    end
    Algo = 5;
    
elseif (GeometryType == cl_image.indGeometryType('PingAcrossDist')) && (SonarFamily == 1) % SonarX et Sonar
    Algo = 1;
    
    % elseif ((GeometryType == cl_image.indGeometryType('PingAcrossDist')) || (GeometryType == cl_image.indGeometryType('PingSamples'))) && (SonarFamily == 2)  % (SonarX et Multibeam)
elseif ((GeometryType == cl_image.indGeometryType('PingSamples')) ...
        || (GeometryType == cl_image.indGeometryType('PingRange'))) && (SonarFamily == 2)  % (SonarX et Multibeam)
    [flag, a] = importLatLonFromBathyFais(this, indImage, varargin{:});
    if flag
        return
    end
    
    indLayerAcrossDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AcrossDist');
    indLayerAlongDistance  = findIndLayerSonar(this, indImage, 'strDataType', 'AlongDist');
    
    if isempty(indLayerAcrossDistance) || isempty(indLayerAlongDistance)
        str1 = sprintf('Cette image provient d''un multifaisceau, or, pour r�aliser CORRECTEMENT une mosaique, il vous faut les layers "AcrossDistance" et "AlongDistance".\n\nJe peux meanmoins realiser cette mosaique comme s''il s''agissait d''un sonar lateral.  Si vous choisissez cette option n''oubliez pas que ca ne sera qu''une approximation.\n\nEst-ce que l''on fait comme si ?');
        str2 = sprintf('This image comes from a multibean echosounder. To create properly a mosaic there must exist the appropriate layers(Latitude & Longitude layers or simply AcrossDistance & AlongDistance. If you want a rough estimation I can process this image as if it was a sidescan sonar, the result should not be used faraway.');
        [choix, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
        if ~flag
            return
        end
        if choix == 1
            Algo = 1;
        else
            flag = 0;
            return
        end
        %         else
        %             [flag, a] = sonar_MBES_projectionX(this(indImage), ...
        %                 this(indReceptionBeam), ...
        %                 this(indAcrossDistance), ...
        %                 this(indRange), ...
        %                 [], ...
        %                 'resolutionX', get(this(indImage), 'SonarResolutionX'));
        %             Algo = 2;
        %         end
        
    else
        Algo = 2;
    end
    
    
elseif (GeometryType == cl_image.indGeometryType('PingAcrossDist')) && (SonarFamily == 2)  % (SonarX et Multibeam)
    
    TagSynchroY = this(indImage).TagSynchroY;
    
    identAlongDistance = cl_image.indDataType('AlongDist');
    
    indLayerAlongDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AlongDist', 'OnlyOneLayer');
    if isempty(indLayerAlongDistance)% MODIF 24/10/2010
        for k=1:length(this)
            TY = this(k).TagSynchroY;
            if strcmp(TY, TagSynchroY)
                TC = this(k).GeometryType;
                TD = this(k).DataType;
                if TC == 7 % BathyFais
                    if TD == identAlongDistance
                        indLayerAlongDistance = k;
                    end
                end
            end
        end
    end
    
    %% On recherche les layers AcrossDistance et AlongDistance sur les donnees BathyFais
    
    if ~isempty(indLayerAlongDistance)
        Algo = 4;
        
    elseif isempty(indLayerAlongDistance)
        str1 = sprintf('Cette image provient d''un multifaisceau, or, pour r�aliser CORRECTEMENT une mosaique, il vous faut les layers "AcrossDistance" et "AlongDistance".\n\nJe peux meanmoins realiser cette mosaique comme s''il s''agissait d''un sonar lateral.  Si vous choisissez cette option n''oubliez pas que ca ne sera qu''une approximation.\n\nEst-ce que l''on fait comme si ?');
        str2 = sprintf('This image comes from a multibean echosounder. To create properly a mosaic there must exist the appropriate layers(Latitude & Longitude layers or simply AcrossDistance & AlongDistance. If you want a rough estimation I can process this image as if it was a sidescan sonar, the result should not be used faraway.');
        [choix, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
        if ~flag
            return
        end
        if choix == 1
            Algo = 1;
        else
            flag = 0;
            return
        end
        
    else
        %         Algo = 3; % Modifi� le 25/10/2010
        disp('Message for JMA : ATTENTION Modif le 25/10/2010, risque d''effet de bord possible (Algo = 2 au lieu de Algo = 3)')
        Algo = 2;
    end
    
elseif GeometryType == cl_image.indGeometryType('PingBeam')
    
    %{
% TODO : remplacer ce qui suit par quelque chose du genre:
[flag, indLayerAcrossDistance] = find_indLayerAcrossDistance(this, indLayerAlongDistance);
    %}
    
    indLayerAcrossDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AcrossDist', 'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer');
    if isempty(indLayerAcrossDistance)
        flag = 0;
        return
    end
    
    indLayerAlongDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AlongDist', 'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer');
    if isempty(indLayerAlongDistance)
        flag = 0;
        return
    end
    Algo = 2;
    
elseif ((GeometryType == cl_image.indGeometryType('PingSamples')) || (GeometryType == cl_image.indGeometryType('PingRange'))) && (SonarFamily == 1) % SonarX et Sonar
    
    % --------------------------------------------------------------------
    % Recherche des layers Latitude et longitude sur les donn�es BathyFais
    
    [flag, a] = importLatLonFromBathyFais(this, indImage, varargin{:});
    if flag
        %         return %  Comment� le 05/12/2010 (Geoswath)
    end
    
    
    % {
    % Rajout� le 29/06/2008 pour GeoSwath
    %{
% TODO : remplacer ce qui suit par quelque chose du genre:
[flag, indLayerAcrossDistance] = find_indLayerAcrossDistance(this, indLayerAlongDistance);
    %}
    
    indLayerAcrossDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AcrossDist', 'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer');
    if isempty(indLayerAcrossDistance)
        flag = 0;
        return
    end
    
    indLayerAlongDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AlongDist', 'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer');
    if isempty(indLayerAlongDistance)
        flag = 0;
        return
    end
    % }
    Algo = 2;
    
else
    str1 = 'Donn�es insuffisantes pour cr�er la mosa�que';
    str2 = 'Something wrong with this sonar, please report to JMA';
    my_warndlg(Lang(str1,str2), 0);
    flag = 0;
    return
end



switch Algo
    case 1
        [flag, a] = sonar_calcul_coordGeo_SonarX(this(indImage), ...
            'Heading', Heading, 'FishLatitude', FishLatitude, 'FishLongitude', FishLongitude, ...
            'NoWaitbar', NoWaitbar, varargin{:});
        
    case 2
        [flag, a] = sonar_calcul_coordGeo_BathyFais(this(indImage), ...
            this(indLayerAcrossDistance(1)), ...
            this(indLayerAlongDistance(1)), ...
            Heading, FishLatitude, FishLongitude, ...
            'subx', subx, 'suby', suby, 'Mute', Mute, varargin{:});
        
    case 3
        PingCounter               = get(this(indImage), 'PingCounter');
        AcrossDistancePingCounter = get(this(indLayerAcrossDistance), 'PingCounter');
        AlongDistancePingCounter  = get(this(indLayerAlongDistance),  'PingCounter');
        
        % TODO : ATTENTION danger, il faut certainement faire
        % PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
        [subsuby, subAcross] = intersect3(PingCounter(suby), ...
            AcrossDistancePingCounter, ...
            AlongDistancePingCounter); %#ok<ASGLU>
        
        Heading = get(this(indLayerAcrossDistance), 'Heading');
        FishLatitude = get(this(indLayerAcrossDistance), 'FishLatitude');
        FishLongitude = get(this(indLayerAcrossDistance), 'FishLongitude');
        [flag, a] = sonar_calcul_coordGeo_BathyFais(this(indLayerAcrossDistance), ...
            this(indLayerAcrossDistance), ...
            this(indLayerAlongDistance), ...
            Heading, FishLatitude, FishLongitude, ...
            'suby', subAcross);
        %             Heading(subAcross), FishLatitude(subAcross), FishLongitude(subAcross), ...
        %             'suby', subAcross);
        if ~flag
            return
        end
        
        resolution = get(this(indImage), 'SonarResolutionX');
        x = this(indImage).x(subx);
        [flag, b] = sonar_bathyFais2SonarX(a, this(indLayerAcrossDistance), resolution, 'XLim', [min(x) max(x)]);
        clear a
        a = b;
        
    case 4
        if (this(indLayerAlongDistance).GeometryType == cl_image.indGeometryType('PingBeam')) ...
                     && (this(indImage).GeometryType == cl_image.indGeometryType('PingAcrossDist'))
            [flag, indLayerAcrossDistance] = find_indLayerAcrossDistance(this, indLayerAlongDistance);
            if ~flag
                return
            end
            
            resolution = get(this(indImage), 'SonarResolutionX');
            x = this(indImage).x(subx);
            [flag, b] = sonar_bathyFais2SonarX(this(indLayerAlongDistance), this(indLayerAcrossDistance), resolution, 'XLim', [min(x) max(x)]);
            if ~flag
                return
            end
        else
            b = this(indLayerAlongDistance(1));
        end
        [flag, a] = ComputeLatLonInPingAcrossDist(this(indImage), b, ...
            Heading, FishLatitude, FishLongitude, 'subx', subx, 'NoWaitbar', NoWaitbar, varargin{:});
    case 5 % indLayerAlongDistance
        [flag, a] = ComputeLatLonInPingAcrossDist(this(indImage), ...
            this(indLayerAlongDistance(1)), ...
            Heading, FishLatitude, FishLongitude, ...
            'subx', subx, 'NoWaitbar', NoWaitbar, varargin{:});
end


function [flag, a] = importLatLonFromBathyFais(this, indImage, varargin)

flag = 0;
a    = [];

TagSynchroX = this(indImage).TagSynchroX;
TagSynchroY = this(indImage).TagSynchroY;

identLayerLatitude  = cl_image.indDataType('Latitude');
identLayerLongitude = cl_image.indDataType('Longitude');
identLayerRange     = cl_image.indDataType('RayPathSampleNb');
identReceptionBeam  = cl_image.indDataType('RxBeamIndex');
identAlongDistance  = cl_image.indDataType('AlongDist');
identAcrossDistance = cl_image.indDataType('AcrossDist');
identBathymetry     = cl_image.indDataType('Bathymetry');

indLayerLatitude  = [];
indLayerLongitude = [];
indLayerRange     = [];
indReceptionBeam  = [];
indAlongDistance  = [];
indAcrossDistance = [];
indBathymetry     = [];

for k=1:length(this)
    TX = this(k).TagSynchroX;
    TY = this(k).TagSynchroY;
    if strcmp(TY, TagSynchroY)
        TC = this(k).GeometryType;
        TD = this(k).DataType;
        if TC == 7 % BathyFais
            if TD == identLayerLatitude
                indLayerLatitude = k;
            end
            if TD == identLayerLongitude
                indLayerLongitude = k;
            end
            if TD == identLayerRange
                indLayerRange = k;
            end
            if TD == identAlongDistance
                indAlongDistance = k;
            end
            if TD == identAcrossDistance
                indAcrossDistance = k;
            end
            if (TD == identBathymetry)
                indBathymetry = k;
            end
        elseif TC == 4 % SonarD
            if (TD == identReceptionBeam) && strcmp(TX, TagSynchroX)
                indReceptionBeam = k;
            end
        end
    end
end
if ~isempty(indLayerLatitude) && ~isempty(indLayerLongitude) && ...
        ~isempty(indLayerRange) && ~isempty(indReceptionBeam)
    
    % Cas o� on a trouv� des layers de latitude et longitude BathyFais et
    % le layer range : on importe les layers de latitude et de longitude en
    % SonarD
    
    [flag, a] = sonar_bathyFais2SonarD(this(indReceptionBeam), ...
        this(indLayerRange), ...
        this([indLayerLatitude indLayerLongitude]), ...
        varargin{:});
    return
end

% -------------------------------------------------------------------------
% On recherche les layers AcrossDistance et AlongDistance sur les donnees BathyFais

if ~isempty(indAcrossDistance) && ~isempty(indAlongDistance) && ...
        ~isempty(indLayerRange) &&  ~isempty(indBathymetry)
    
    Heading       = get(this(indBathymetry), 'Heading');
    FishLatitude  = get(this(indBathymetry), 'FishLatitude');
    FishLongitude = get(this(indBathymetry), 'FishLongitude');
    
    % -----------------------------------------------------
    % Calcul de la latitude et de la longitude en BathyFais
    
    [flag, a] = sonar_calcul_coordGeo_BathyFais(this(indBathymetry), ...
        this(indAcrossDistance), ...
        this(indAlongDistance), ...
        Heading, FishLatitude, FishLongitude, varargin);
    
    % -------------------------------------
    % Importation de ces 2 layers en SonarD
    
    TC = this(indImage).GeometryType;
    if (TC == 7) && ~isempty(indLayerRange) && ~isempty(indReceptionBeam)
        
        % Cas o� on a trouv� des layers de latitude et longitude BathyFais et
        % le layer range : on importe les layers de latitude et de longitude en
        % SonarD
        
        [flag, a] = sonar_bathyFais2SonarD(this(indReceptionBeam), ...
            this(indLayerRange), ...
            a, ...
            varargin{:});
        return
        
    elseif (TC == 4) && ~isempty(indLayerRange) && ~isempty(indReceptionBeam)
        
        % Cas o� on a trouv� des layers de latitude et longitude BathyFais et
        % le layer range : on importe les layers de latitude et de longitude en
        % SonarD
        
        [flag, a] = sonar_bathyFais2SonarD(this(indReceptionBeam), ...
            this(indLayerRange), ...
            a, ...
            varargin{:});
        return
        
    elseif (TC == 5) && ~isempty(indLayerRange) && ~isempty(indReceptionBeam)
        
        % Cas o� on a trouv� des layers de latitude et longitude BathyFais et
        % le layer range : on importe les layers de latitude et de longitude en
        % SonarD
        
        resolution = get(this(indImage), 'SonarResolutionX');
        [flag, b] = sonar_bathyFais2SonarX(a, ...
            this(indAcrossDistance), ...
            resolution);
        a = b;
        return
    end
end


function [flag, indLayerAcrossDistance] = find_indLayerAcrossDistance(this, indImage)

indLayerAcrossDistance = findIndLayerSonar(this, indImage, 'strDataType', 'AcrossDist', 'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer');
if isempty(indLayerAcrossDistance)
    flag = 0;
    return
end
flag = 1;
