% Creation des layers de coordonn�es m�triques
%
% Syntax
%   [flag, a] = sonarCalculCoordXY(this, ...)
% 
% Input Arguments
%   this : Instance de clc_image
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=ca c'est bien passe, 0 sinon
%   a    : Instances de cl_image contenant les abscisses et les ordonn�es
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonarCalculCoordXY(this, indImage, varargin)

a = [];

indLayerLatitude  = findIndLayerSonar(this, indImage, 'strDataType', 'Latitude',  'WithCurrentImage', 1);
indLayerLongitude = findIndLayerSonar(this, indImage, 'strDataType', 'Longitude', 'WithCurrentImage', 1);

if isempty(indLayerLatitude) || isempty(indLayerLongitude)
    [flag, LatLong] = sonarCalculCoordGeo(this, indImage, varargin{:});
    if ~flag
        return
    end
else
    LatLong(1) = this(indLayerLatitude);
    LatLong(2) = this(indLayerLongitude);
end

[flag, a] = sonar_coordXY(this(indImage), LatLong(1), LatLong(2), varargin{:});
