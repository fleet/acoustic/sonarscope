% Creation de l'image "Angle d'incidence"
%
% Syntax
%   [flag, a] = sonarLateralIncidence(this, indImage, subx, suby)
% 
% Input Arguments
%   this : Instance de cl_image
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Instances de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonarLateralIncidence(this, indImage, subx, suby)

a = cl_image.empty;

[flag, indLayerSlopeAlong, indLayerSlopeAcross, indLayerBathymetry, ...
    creationLayerSlopeAlong, creationLayerSlopeAcross] = ...
    paramsSonarLateralAngleIncidence(this, indImage);

if flag
    if creationLayerSlopeAlong || creationLayerSlopeAcross
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
        if length(indLayerBathymetry) > 1
            clear str
            for k=1:length(indLayerBathymetry)
                str{k} = this(indLayerBathymetry(k)).Name; %#ok<AGROW>
            end
            str1 = 'Il existe plusieurs images de type "Bathymetry", la quelle voulez-vous utiliser ?';
            str2 = 'Many "Bathymetry" layers exist. Which one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'InitialValue', 2);
            if ~flag
                return
            end
            indLayerBathymetry = indLayerBathymetry(rep);
        end
    end

    a = cl_image.empty;
    if creationLayerSlopeAlong
        b = this(indLayerBathymetry);
        b.Azimuth = 0;
        [aSlopeAlong, flag] = slopeAzimuth(b);  % , 'subx', subx, 'suby', suby
        a(end+1) = aSlopeAlong;
    else
        if isempty(indLayerSlopeAlong)
            aSlopeAlong = [];
        else
            aSlopeAlong = this(indLayerSlopeAlong);
        end
    end

    if creationLayerSlopeAcross
        b = this(indLayerBathymetry);
        b.Azimuth = 90;
        [aSlopeAcross, flag] = slopeAzimuth(b);   %, 'subx', subx, 'suby', suby
        a(end+1) = aSlopeAcross;
    else
        if isempty(indLayerSlopeAcross)
            aSlopeAcross = [];
        else
            aSlopeAcross = this(indLayerSlopeAcross);
        end
    end

    if flag
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
        if length(aSlopeAlong) > 1
            clear str
            for k=1:length(aSlopeAlong)
                str{k} = aSlopeAlong(k).Name; %#ok<AGROW> 
            end
            str1 = 'Il existe plusieurs images de type "SlopeAlong", la quelle voulez-vous utiliser ?';
            str2 = 'Many "SlopeAlong" layers exist. Which one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'InitialValue', 2);
            if ~flag
                return
            end
            aSlopeAlong = aSlopeAlong(rep);
        end

        if length(aSlopeAcross) > 1
            clear str
            for k=1:length(aSlopeAcross)
                str{k} = aSlopeAcross(k).Name; %#ok<AGROW> 
            end
            str1 = 'Il existe plusieurs images de type "SlopeAcross", la quelle voulez-vous utiliser ?';
            str2 = 'Many "SlopeAcross" layers exist. Which one do you want to use ?';
            [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'InitialValue', 2);
            if ~flag
                return
            end
            aSlopeAcross = aSlopeAcross(rep);
        end

        a(end+1) = sonar_lateral_incidence(this(indImage), ...
            'layersPentes', [aSlopeAlong, aSlopeAcross], ...
            'subx', subx, 'suby', suby);
    end
end


function [flag, indLayerSlopeAlong, indLayerSlopeAcross, indLayerBathymetry, ...
    creationLayerSlopeAlong, creationLayerSlopeAcross] = ...
    paramsSonarLateralAngleIncidence(this, indImage)

creationLayerSlopeAlong  = 0;
creationLayerSlopeAcross = 0;

indLayerSlopeAlong  = [];
indLayerSlopeAcross = [];
indLayerBathymetry  = [];

identEmissionAngle     = cl_image.indDataType('TxAngle');
identRxBeamAngle       = cl_image.indDataType('RxBeamAngle');
identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');
identSlopeAlong        = cl_image.indDataType('SlopeAlong');
identSlopeAcross       = cl_image.indDataType('SlopeAcross');
identBathymetry        = cl_image.indDataType('Bathymetry');

flag = testSignature(this(indImage), 'DataType', [identEmissionAngle identRxBeamAngle identBeamPointingAngle]);
if flag
    SonarType = get(get(this(indImage), 'SonarDescription'), 'Sonar.Family');
    if SonarType == 2    % 'SondeurMultiFaisceau'
        indLayerSlopeAlong  = findIndLayerSonar(this, indImage, 'DataType', identSlopeAlong);
        indLayerSlopeAcross = findIndLayerSonar(this, indImage, 'DataType', identSlopeAcross);
        indLayerBathymetry  = findIndLayerSonar(this, indImage, 'DataType', identBathymetry);

        if isempty(indLayerSlopeAlong) && isempty(indLayerSlopeAcross)
            s{1} = Lang('Il n''existe pas de layers de pente logitudinale et de pente transversale. ', ...
                'There is no across & along slopes layers. ');
            s{2} = Lang('Le calcul de l''angle d''incidence se sera donc a partir de l''angle d''emission et de la formule de Snell-Descartes qui utilise pour cela les informations de bathycelerimetie. ', ...
                'The incidence angle will be created using RxBeamAngle, Snell-Descartes formula and sound speed profile.');
            if isempty(indLayerBathymetry)
                s{3} = Lang('Pour creer ces layers, il vous faudrait avoir un layer de bathymetie ', ...
                    'TODO');
            else
                s{3} = newline;
                s{4} = Lang('Cependant, vous possedez un layer de bathymetrie il est donc possible de ceer les layers manquants. Je peux les creer pour vous si vous le desirez.', ...
                    'Nevertheless you have a Bathymetry layer, it is possible to deduce the missing layers from it, I can do it for you if you want.');
            end
            my_warndlg([s{:}], 1);

            if ~isempty(indLayerBathymetry)
                [reponse, flag] = my_questdlg( Lang('Creation des layers de pente transversale et de pente longitudinale ?', ...
                    'Creation of Across & along slopes layers ?'));
                if ~flag
                    return
                end
                if reponse == 1
                    creationLayerSlopeAlong = 1;
                    creationLayerSlopeAcross  = 1;
                end
            end

        elseif isempty(indLayerSlopeAlong)
            s{1} = Lang('Il n''existe pas de layer de pente longitudinale. ', ...
                'TODO');
            s{2} = Lang('Le calcul de l''angle d''incidence se fera donc a partir de l''angle d''�mission et de la formule de Snell-Descartes qui utilise pour cela les informations de bathycelerimetie. ', ...
                'TODO');
            if isempty(indLayerBathymetry)
                s{3} = Lang('Pour cr�er ce layer, il vous faudrait avoir un layer de bathymetie (DataType=2 pour "Bathymetry") ', ...
                    'TODO');
            else
                s{3} = newline;
                s{4} = Lang('Cependant, vous possedez un layer de bathym�trie il est donc possible de c�er le layer manquant. Je peux le cr�er pour vous si vous le desirez.', ...
                    'TODO');
            end
            my_warndlg([s{:}], 1);

            if ~isempty(indLayerBathymetry)
                [reponse, flag] = my_questdlg( Lang('Cr�ation du layer de pente longitudinale ?', ...
                    'TODO'));
                if ~flag
                    return
                end
                if reponse == 1
                    creationLayerSlopeAlong = 1;
                end
            end

        elseif isempty(indLayerSlopeAcross)
            s{1} = Lang('Il n''existe pas de layer de pente transversale. ', ...
                'TODO');
            s{2} = Lang('Le calcul de l''angle d''incidence se fera donc a partir de l''angle d''�mission et de la formule de Snell-Descartes qui utilise pour cela les informations de bathycelerimetie. ', ...
                'TODO');
            if isempty(indLayerBathymetry)
                s{3} = Lang('Pour creer ce layer, il vous faudrait avoir un layer de bathymetie (DataType=2 pour "Bathymetry") ', ...
                    'TODO');
            else
                s{3} = newline;
                s{4} = Lang('Cependant, vous poss�dez un layer de bathym�trie il est donc possible de c�er le layer manquant. Je peux le creer pour vous si vous le desirez.', ...
                    'TODO');
            end
            my_warndlg([s{:}], 1);

            if ~isempty(indLayerBathymetry)
                [reponse, flag] = my_questdlg(Lang('Cr�ation du layer de pente transversale ?', ...
                    'TODO'));
                if ~flag
                    return
                end
                if reponse == 1
                    creationLayerSlopeAcross  = 1;
                end
            end
        end
    else
        if isempty(indLayerSlopeAlong) || isempty(indLayerSlopeAcross)
            s{1} = Lang('L''image est de type SonarLateral. ', ...
                'TODO');
            s{2} = Lang('Il n''est donc pas possible de cr�er un layer angle d''incidence tenant compte des pentes longitudinales et transversales.', ...
                'TODO');
            my_warndlg([s{:}], 1);
        else
            s{1} = Lang('L''image est de type SonarLateral. ', ...
                'TODO');
            s{2} = Lang('Vous possedez des layers de pentes longitudinales et transversales, je vais donc les utiliser.', ...
                'TODO');
            my_warndlg([s{:}], 1);
        end
    end
end
