% Calcul du layer projete sur l'axe des X
%
% Syntax
%   b = sonarLateral_PingRange2PingAcrossDist(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image projetee
%
% Examples
%   b = sonarLateral_PingRange2PingAcrossDist(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = sonarLateral_PingRange2PingAcrossDist(this, varargin)

that = cl_image.empty;
nbImages = length(this);
for k1=1:nbImages
    a = unitaire_sonarLateral_PingRange2PingAcrossDist(this(k1), varargin{:});
    if isempty(a)
        that = cl_image.empty;
        return
    end
    for k2=1:length(a)
        that(end+1)= a(k2); %#ok
    end
end


function varargout = unitaire_sonarLateral_PingRange2PingAcrossDist(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, resolutionX] = getPropertyValue(varargin, 'resolutionX', []);
[varargin, createNbp]   = getPropertyValue(varargin, 'createNbp',   []);
[varargin, NoWaitbar]   = getPropertyValue(varargin, 'NoWaitbar',   0); %#ok<ASGLU>

if isempty(resolutionX)
    resolutionX = this.Sonar.ResolutionX;
    if isempty(resolutionX)
        resolutionX = this.Sonar.ResolutionD;
    end
end

if isempty(createNbp)
    createNbp = 1;
end

%% Contr�les

flag = testSignature(this, 'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange')]);
if ~flag
    varargout{1} = [];
    return
end

if isempty(this.Sonar.Height)
    str1 = 'La hauteur (Height" n''est pas d�finie. Calculez la pas "Traitements sonar", "Calcul de la hauteur".';
    str2 = '"Height" is not defined. Process height detection please (Sidescan processing menu).';
    my_warndlg(Lang(str1,str2), 1);
    varargout{1} = [];
    return
end

%% Nombre de points d�j� moyenn�s sur l'image de d�part

SampleFrequency = this.Sonar.SampleFrequency;
if isempty(SampleFrequency)
    SampleFrequency = get(this.Sonar.Desciption, 'Proc.SampFreq');
end

DeltaD = 1500 ./ (2 * double(SampleFrequency));
increment = this.Sonar.ResolutionD / this.Sonar.RawDataResol;

D = this.x(subx);

if this.GeometryType == cl_image.indGeometryType('PingSamples')
    D = this.x(subx) .* max(DeltaD(:));
    this.Sonar.Height = this.Sonar.Height .* DeltaD;
    this.Sonar.Height(abs( this.Sonar.Height) >= abs(max(D))) = NaN;
    this.Sonar.Height(abs( this.Sonar.Height) >= abs(min(D))) = NaN;
    flagDConstant = 0;
else
    flagDConstant = 1;
end

hMin = double(min(abs(this.Sonar.Height(suby))));
if abs(D(1)) > hMin
    angleMin = sign(D(1)) * acosd(abs(hMin ./ D(1)));
    if ~isreal(angleMin)
        str1 = 'L''angle calcul� � partir de la hauteur est un nombre complexe, v�rifiez le signal de hauteur SVP.';
        str2 = 'Computed angle is complex, please check the height signal.';
        my_warndlg(Lang(str1,str2), 1);
        varargout{1} = [];
        return
    end
else
    angleMin = 0;
end
XMin =  hMin * tand(angleMin);

if abs(D(end)) > hMin
    angleMax = sign(D(end)) * acosd(abs(hMin ./ D(end)));
    if ~isreal(angleMax)
        str1 = 'L''angle calcul� � partir de la hauteur est un nombre complexe, v�rifiez le signal de hauteur SVP.';
        str2 = 'Computed angle is complex, please check the height signal.';
        my_warndlg(Lang(str1,str2), 1);
        varargout{1} = [];
        return
    end
else
    angleMax = 0;
end
XMax =  hMin * tand(angleMax);

if (XMin == 0) && (XMax == 0)
    str1 = 'Pas de portion correcte selectionn�e';
    str2 = 'No common data';
    my_warndlg(Lang(str1,str2), 1);
    
    varargout{1} = [];
    return
end

if this.GeometryType == cl_image.indGeometryType('PingSamples')
    X = XMin:resolutionX:XMax; % JMA 03/09/07
    this.Sonar.ResolutionX = resolutionX;
elseif this.GeometryType == cl_image.indGeometryType('PingRange')
    X = XMin:resolutionX:XMax;
    this.Sonar.ResolutionX = resolutionX; % Deja defini mais vaut mieux le laisser pour une meilleure compr�hension
else
    'On devrait pas passer par l�'
end
[X, resolutionX, XMin, XMax] = centrage_magnetique(X); %#ok

nbColumns = length(X);

Reflectivite     = NaN(length(suby), nbColumns, 'single');
NbPointsMoyennes = zeros(length(suby), nbColumns, 'single');

% % Pas efficace car appel pixel par pixel
% Reflectivite     = cl_memmapfile('Value', NaN, 'Size', [length(suby), nbColumns], 'Format', 'single');
% NbPointsMoyennes = cl_memmapfile('Value', 0,   'Size', [length(suby), nbColumns], 'Format', 'single');

deltad = D(2) - D(1); % On augmente artificiellement la hauteur d'un pixel pour eviter une image pas tres belle au milieu

M = length(suby);
str1 = sprintf('%s : Projection en distance across (PingSample -> PingAcrossDist)', this.Name);
str2 = sprintf('%s : Slant range correction (PingSample -> PingAcrossDist)', this.Name);
hw = create_waitbar(Lang(str1,str2), 'N', M, 'NoWaitbar', NoWaitbar);
for k1=1:M
    my_waitbar(k1, M, hw);
    
    if ~flagDConstant && (length(DeltaD) > 1)
        D = this.x(subx) .* DeltaD(k1);
    end
    
    iy = suby(k1);
    h = abs(this.Sonar.Height(iy)) + deltad;
    Q = NaN(size(subx), 'single');
    sub = find(abs(D) >= h);
    Q(sub) = acosd(h ./ D(sub));
    x = h * tand(Q);
    
    ping = this.Image(iy,:);
    indx = 1 + round((x - XMin) / resolutionX);
    R = zeros(1, nbColumns);
    %     sub1 = find(~isnan(indx));
    sub1 = find(~isnan(indx) & ~isnan(ping(subx))); % Rajout� pour fichiers SDF par JMA le 18/10/2011
    
    N = NbPointsMoyennes(k1,:);
    for k2=1:length(sub1)
        k3 = sub1(k2);
        ik = indx(k3);
        if (ik >= 1) && (ik <= nbColumns)
            val = ping(k3 + subx(1) - 1);
            R(ik) = R(ik) + val;
            N(ik) = N(ik) + increment;
        end
    end
    NbPointsMoyennes(k1,:) = N;
    
    subNonNan = find((N ~= 0) & ~isnan(R));
    if length(subNonNan) <= 1
        continue
    end
    if ~isempty(subNonNan)
        R(subNonNan) = R(subNonNan) ./ (N(subNonNan) / increment);
    end
    
    subNan = find((N == 0) | isnan(R));
    if ~isempty(subNan) && ~isempty(subNonNan)
        R(subNan) = interp1(subNonNan, R(subNonNan), subNan);
    end
    
    Reflectivite(k1, :) = R;
    NbPointsMoyennes(k1, subNan) = -1;
    
    %     subOutLines = find((X <= -sqrt(D(1) ^ 2 - h^2)) | (X >= sqrt(D(end) ^ 2 - h^2)));
    subOutLines = (X <= -sqrt(D(1) ^ 2 - h^2)) | (X >= sqrt(D(end) ^ 2 - h^2));
    NbPointsMoyennes(k1, subOutLines) = 0;
end
my_close(hw, 'MsgEnd');

this = replace_Image(this, Reflectivite);

this.ValNaN = NaN;

if this.GeometryType == cl_image.indGeometryType('PingSamples')
    %     X = X * DeltaD(1);
    % %     'Instruction d�commant�e dans cl_image/sonarLateral_PingRange2PingAcrossDist pour donn�es Reson PDS sidescan, cette instruction avait �t� commant�e pour un autre sondeur mais je ne me souviens plus le quel. Conclusion, il faut metrre ceci au clair au plus vite'
    %     X = X * DeltaD(1);
end

this.x = X;
this.GeometryType = cl_image.indGeometryType('PingAcrossDist');
this.XUnit = 'm';

%% Mise � jour de coordonn�es

this = majCoordonnees(this, 1:nbColumns, suby);

%% Calcul des statistiques

this = compute_stats(this);

if contains(this.TagSynchroX, 'PingSamples')
    this.TagSynchroX = strrep(this.TagSynchroX, 'PingSamples', 'PingAcrossDist');
else
    str = this.TagSynchroX;
    k = strfind(str, 'SonarD');
    if isempty(k)
        this.TagSynchroX = num2str(rand(1));
    else
        str(k:k+5) = 'SonarX';
        this.TagSynchroX = str;
    end
end

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Mise � jour du nom de l'image

this = update_Name(this);

%% Par ici la sortie

varargout{1} = this;

if createNbp
    this = replace_Image(this, NbPointsMoyennes);
    this = compute_stats(this);
    this.TagSynchroContrast = num2str(rand(1));
    this.CLim               = [this.StatValues.Min this.StatValues.Max];
    this.ColormapIndex      = 3;
    this.DataType           = cl_image.indDataType('AveragedPtsNb');
    this = update_Name(this);
    varargout{1}(2) = this;
end
