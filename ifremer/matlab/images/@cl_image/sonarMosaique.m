function [flag, a] = sonarMosaique(this, indImage, resol, varargin)

[varargin, LimitAngles]           = getPropertyValue(varargin, 'LimitAngles',    []);
[varargin, subx]                  = getPropertyValue(varargin, 'subx',           []);
[varargin, suby]                  = getPropertyValue(varargin, 'suby',           []);
[varargin, NoQuestion]            = getPropertyValue(varargin, 'NoQuestion',     0);
[varargin, NoWaitbar]             = getPropertyValue(varargin, 'NoWaitbar',      0);
[varargin, flagTideCorrection]    = getPropertyValue(varargin, 'TideCorrection', 0);

[varargin, CreationMosAngle]      = getPropertyValue(varargin, 'CreationMosAngle', 0);
[varargin, indLayerEmissionAngle] = getPropertyValue(varargin, 'indLayerAngle',    []);
[varargin, CreationMosRange]      = getPropertyValue(varargin, 'CreationMosRange', 0);
% [varargin, indLayerRange]       = getPropertyValue(varargin, 'indLayerRange',    []);
[varargin, CreationMosTime]       = getPropertyValue(varargin, 'CreationMosTime',  0);
[varargin, indLayerTime]          = getPropertyValue(varargin, 'indLayerTime',     []);

[varargin, indLayerLat]           = getPropertyValue(varargin, 'indLayerLat', []);
[varargin, indLayerLon]           = getPropertyValue(varargin, 'indLayerLon', []);

[varargin, AcrossInterpolation]   = getPropertyValue(varargin, 'AcrossInterpolation',   2);
[varargin, AlongInterpolation]    = getPropertyValue(varargin, 'AlongInterpolation',    1);
[varargin, MethodeRemplissage]    = getPropertyValue(varargin, 'MethodeRemplissage',    1);
[varargin, SpecularInterpolation] = getPropertyValue(varargin, 'SpecularInterpolation', 0); %#ok<ASGLU>

%% Calcul des coordonn�es g�ographique si non fournies

if isempty(indLayerLat) || isempty(indLayerLon)
    [flag, a] = sonarCalculCoordGeo(this, indImage, 'subx', subx, 'suby', suby, 'NoWaitbar', NoWaitbar);
    if ~flag
        return
    end
    if isempty(a)
        my_warndlg('There is no navigation in this file', 0);
        return
    end
    Lat = a(1);
    Lon = a(2);
    clear a
else
    Lat = this(indLayerLat);
    Lon = this(indLayerLon);
end

%% Calcul de la taille de la mosa�que

[Nx, Ny, resolLon, resolLat, x, y, LonWest, LonEst, LatNord, LatSud, message, status] = ...
    sonar_mosaique_size(this(indImage), Lat, Lon, resol, ...
    'subx', subx, 'suby', suby); %#ok<ASGLU>

if isempty(Nx) || isempty(Ny)
    flag = 0;
    a = [];
    return
end

if ~NoQuestion
    disp(message)
    str = sprintf(Lang('%s\n\nCela vous convient-il ?','%s\n\nIs it OK ?'), message);
    [choix, validation] = my_questdlg(str);
    if ~validation || (choix == 2)
        flag = 0;
        a = [];
        return
    end
end

%% R�alisation de la mosa�que

[flag, b] = sonar_mosaique(this(indImage), Lat, Lon, resol, ...
    'AcrossInterpolation', AcrossInterpolation, ...
    'AlongInterpolation',  AlongInterpolation, ...
    'SpecularInterpolation', SpecularInterpolation, ...
    'MethodeRemplissage',  MethodeRemplissage, ...
    'subx', subx, 'suby', suby, ...
    'TideCorrection', flagTideCorrection, 'NoWaitbar', NoWaitbar);% , ...
%     'LayerAngle', this(indLayerEmissionAngle));
InitialImageName = [b.InitialImageName ' - ' num2str(rand(1))];
b.InitialImageName = InitialImageName;
a(1) = b;

%% Cr�ation de la mos�que des angles d'�mission

if CreationMosAngle
    if isempty(indLayerEmissionAngle)
        [X, flag] = sonar_lateral_emission(this(indImage), 'subx', subx, 'suby', suby, 'useRoll', 0);
    else
        X = extraction(this(indLayerEmissionAngle), 'suby', suby);
    end
    
    if ~isempty(X)
        [flag, b] = sonar_mosaique(X, Lat, Lon, resol, ...
            'AcrossInterpolation', AcrossInterpolation, ...
            'AlongInterpolation',  AlongInterpolation, ...
            'SpecularInterpolation', SpecularInterpolation, 'NoWaitbar', NoWaitbar); %, ...
%             'LayerAngle', this(indLayerEmissionAngle));
        b.InitialImageName = InitialImageName;
        b.ColormapIndex    = 17;
        CLim = b.CLim;
        M = max(abs(CLim));
        b.CLim = [-M M];
        if ~isempty(LimitAngles)
            layerMasque = ROI_auto(b, 1, 'MaskReflec', b, [], LimitAngles, 1);
            b = masquage(b, 'LayerMask', layerMasque, 'valMask', 1);
            a = masquage(a, 'LayerMask', layerMasque, 'valMask', 1);
        end
        if CreationMosAngle
            a(end+1) = b;
        end
    end
end

%% Cr�ation de la mosa�que du temps universel

if CreationMosTime
    if isempty(indLayerTime)
        % On pourrait faire mieux dans le cas des
        % multifaisceaux :
        % I = generateTempsTrajet(DataRaw.Range, DataDepth.SamplingRate, subl, subc) / 2;
        
        [flag, X] = sonar_lateral_tempsUniversel(this(indImage), 'subx', subx, 'suby', suby);
        if ~flag
            a = [];
            return
        end
    else
        X = this(indLayerTime);
    end
    
    [flag, b] = sonar_mosaique(X, Lat, Lon, resol, ...
        'AcrossInterpolation', AcrossInterpolation, ...
        'AlongInterpolation',  AlongInterpolation, ...
        'SpecularInterpolation', SpecularInterpolation);
    if ~isempty(LimitAngles)
        b = masquage(b, 'LayerMask', layerMasque, 'valMask', 1);
    end
    b = update_Name(b);
    a(end+1) = b;
end

%% Cr�ation de la mosa�que du range

if CreationMosRange
    [flag, R] = sonar_compute_Range(this, indImage, 'subx', subx, 'suby', suby);
    if ~flag
        a = [];
        return
    end
    
    [flag, b] = sonar_mosaique(R, Lat, Lon, resol, ...
        'AcrossInterpolation', AcrossInterpolation, ...
        'AlongInterpolation',  AlongInterpolation, ...
        'SpecularInterpolation', SpecularInterpolation);
    if ~flag
        a = [];
        return
    end
    
    if ~isempty(LimitAngles)
        b = masquage(b, 'LayerMask', layerMasque, 'valMask', 1);
    end
    b = update_Name(b);
    a(end+1) = b;
end
