function [flag, a] = sonarMosaiqueInv(this, indImage, LayerMos, varargin)

[varargin, indLayerLat]     = getPropertyValue(varargin, 'indLayerLat',     []);
[varargin, indLayerLon]     = getPropertyValue(varargin, 'indLayerLon',     []);
[varargin, indLayerTxAngle] = getPropertyValue(varargin, 'indLayerTxAngle', []);
[varargin, LayerMosTxAngle] = getPropertyValue(varargin, 'LayerMosTxAngle', []);
[varargin, subx]            = getPropertyValue(varargin, 'subx',            []);
[varargin, suby]            = getPropertyValue(varargin, 'suby',            []); %#ok<ASGLU>

if isempty(indLayerLat) || isempty(indLayerLon)
    [flag, a] = sonarCalculCoordGeo(this, indImage, 'subx', subx, 'suby', suby);
    if flag
        Lat = a(1);
        Lon = a(2);
        clear a
    else
        return
    end
else
    Lat = this(indLayerLat);
    Lon = this(indLayerLon);
end

%% R�alisation de la mosa�que

[flag, b] = sonar_mosaiqueInv(this(indImage), this(indLayerTxAngle), ...
    Lat, Lon, LayerMos, LayerMosTxAngle, suby);
if ~flag
    a = [];
    return
end

b.InitialImageName = [b.InitialImageName ' - ' num2str(rand(1))];
a(1) = b;
