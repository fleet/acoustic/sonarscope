% Creation de l'image ""EmissionBeam"
%
% Syntax
%   [flag, a] = sonarSecteurEmission(this, ...)
%
% Input Arguments
%   this : Instance de clc_image
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1:ca c'est bien passe, 0 sinon
%   a    : Instances de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------
 
function [flag, a] = sonarSecteurEmission(this, indImage, varargin)

[subx, suby, varargin] = getSubxSuby(this(indImage), varargin); %#ok<ASGLU>

GeometryType    = this(indImage).GeometryType;
strGeometryType = get_GeometryTypeName(this(indImage));

identEmissionAngle     = cl_image.indDataType('TxAngle');
identBeampointingAngle = cl_image.indDataType('BeamPointingAngle');

indLayerReceptionBeam = [];
flag = 1;
switch GeometryType
    case cl_image.indGeometryType('PingBeam')  % BathyFais
        flag = testSignature(this(indImage), 'DataType', identEmissionAngle);
        indLayerAngle = indImage;
        %     case {cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingAcrossDist')} % Avant 17/01.2013
        
    case {cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingAcrossDist')}
        identRxBeamAngle = cl_image.indDataType('RxBeamAngle');
        DataTypeImage = this(indImage).DataType;
        if DataTypeImage == identRxBeamAngle
            indLayerAngle = indImage;
        else
            identReceptionBeam = cl_image.indDataType('RxBeamIndex');
            indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identEmissionAngle, 'WithCurrentImage', 1, 'OnlyOneLayer');
            if isempty(indLayerAngle)
                indLayerAngle = findIndLayerSonar(this, indImage, 'DataType', identBeampointingAngle, 'WithCurrentImage', 1, 'OnlyOneLayer');
            end
            indLayerReceptionBeam = findIndLayerSonar(this, indImage, 'DataType', identReceptionBeam, 'WithCurrentImage', 1, 'OnlyOneLayer');
            if isempty(indLayerAngle) && isempty(indLayerReceptionBeam) && isempty(indLayerBeamPointingAngle)
                flag = 0;
            end
        end
        
    case cl_image.indGeometryType('SampleBeam')
        [flag, a] = sonar_SampleBeamRxBeamAnglesCreation(this(indImage), 'subx', subx, 'suby', suby);
        a = a(2);
        return
        
    otherwise
        str1 = sprintf('Il n''est pas possible de cr�er un layer "TxBeamIndex" pour une g�om�trie "%s".', strGeometryType);
        str2 = sprintf('It is not possible to create a layer "TxBeamIndex" for the "%s" geometry.', strGeometryType);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
end

if ~flag
    a = [];
    return
end

a = sonar_secteur_emission(this(indLayerAngle), this(indLayerReceptionBeam), 'subx', subx, 'suby', suby);
