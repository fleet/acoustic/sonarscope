function [flag, that] = sonar_BSImageSynthesis(this, Angle, ModelName, Parameters, listValueMask, Noise, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

nbCurves = length(listValueMask);
%{
FigUtils.createSScFigure; % TODO : en attendant que l'on puisse visualiser plusieurs mod�les sur la m�me figure
for k=1:nbCurves
    a(k) = BSLurtonModel('XData', -80:80, 'ValParams', Parameters(k,:)); % TODO Pierre Mahoudo : message bizarre
    a(k) = noise(a(k), 'Coeff', 5);
    PlotUtils.createSScPlot(a(k).XData, a(k).YData); grid on; hold on; % TODO : en attendant que l'on puisse visualiser plusieurs mod�les sur la m�me figure
%     plot(a);
end
%}

%% Algorithm

A = Angle.Image(suby,subx); % TODO : attention, ici on consid�re que les tailles sont les m�mes. Faire un truc du styme [A, B, ...] = xxx(A, B, ..., 'subx', subx, 'suby', suby);
M = this.Image(suby,subx);
I = NaN(size(M), 'single');

for k=1:nbCurves
    sub = (M == listValueMask(k));
    switch ModelName
        case 'BSLurton'
            a = BSLurtonModel('XData', A(sub(:)'), 'ValParams', Parameters(k,:)); % TODO Pierre Mahoudo : message bizarre
        otherwise
            'beurk'
    end
    a = noise(a, 'Coeff', Noise);
    I(sub) = a.YData;
end

%% Output image

DataType = cl_image.indDataType('Reflectivity');
that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'BsSynthetis', ...
    'ColormapIndex', 2, 'DataType', DataType);

%% Sauvegarde des mod�les en tant que courbes

for k=1:nbCurves
    XData = -80:0.5:80;
     switch ModelName
        case 'BSLurton'
            a = BSLurtonModel('XData', XData, 'ValParams', Parameters(k,:)); % TODO Pierre Mahoudo : message bizarre
        otherwise
            'beurk'
    end
    YData = a.compute();
    
    nbPoints = length(XData);
    bilan{1}.x       = XData;
    bilan{1}.y       = YData;
    bilan{1}.ymedian = YData;
    bilan{1}.nom     = Angle.Name;
    bilan{1}.sub     = 1:nbPoints;
    bilan{1}.nx      = nbPoints;
    bilan{1}.std     = zeros(1,nbPoints) + Noise;
    bilan{1}.titreV  = this.Name;
    bilan{1}.NY      = nbPoints;
    bilan{1}.nomX    = Angle.Name;
    bilan{1}.Biais   = 0;
    bilan{1}.Unit    = 'dB';
    
    bilan{1}.nomZoneEtude = ['Mask value ' num2str(listValueMask(k))];
    bilan{1}.commentaire  = 'Synthesis';
    bilan{1}.Tag = '';
    bilan{1}.DataTypeValue      = 'Reflectivity';
    bilan{1}.DataTypeConditions = cl_image.strDataType{Angle.DataType};
    bilan{1}.Support            = 'Layer';

    bilan{1}.GeometryType   = Angle.GeometryType;
    bilan{1}.TypeCourbeStat = 'Statistics';
    
    bilan{1}.TagSup = [];
    
%     if ~isempty(SelectMode)
%         bilan{1}.Mode = SelectMode;
%     end
%     if ~isempty(SelectSwath)
%         bilan{1}.NbSwaths = SelectSwath;
%     end

    that.CourbesStatistiques(end+1).bilan = bilan;
end
% CourbesStatsPlot(that.CourbesStatistiques(end-nbCurves+1:end).bilan, 'Stats', 1, 'subTypeCurve', 1);

flag = 1;