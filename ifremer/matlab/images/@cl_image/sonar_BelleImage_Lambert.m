% Calcul de la compensation de Lambert
%
% Syntax
%   [flag, b] = sonar_BelleImage_Lambert(a, ...) 
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance de cl_image contenant la compensation de Lambert
%
% Examples 
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', [9 1 4], 'Carto', Carto);
%   
%   Reflectivite = get_Image(b, 1);
%   imagesc(Reflectivite)
%   Bathy = get_Image(b, 2);
%   imagesc(Bathy)
%   Emission = get_Image(b, 3);
%   imagesc(Emission)
%   
%   BelleImage = sonar_BelleImage_Lambert(Bathy, 1, [], 3, Bathy, Emission);
%   imagesc(BelleImage)

%   b = sonar_BelleImage_Lambert(BelleImage)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_BelleImage_Lambert(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, that] = unitaire_sonar_BelleImage_Lambert(this(k), CasDepth, H, CasDistance, Bathy, Emission, varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end


function [flag, that] = unitaire_sonar_BelleImage_Lambert(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>
flag = 0;

%% Algorithm

if Emission.DataType == cl_image.indDataType('IncidenceAngle')
    AngleEmission = Emission.Image(suby,subx);
    AngleName = 'AngInc';
elseif Emission.DataType == cl_image.indDataType('RxAngleEarth')
    AngleEmission = Emission.Image(suby,subx);
    AngleName = 'AngEarth';
else
    [subx, suby, AngleEmission] = sonar_angle_nadir(this, CasDepth, H, CasDistance, Bathy, Emission, ...
        'subx', subx, 'suby', suby); % , 'FlagRoll', 0); % Modif JMA le 04/09/2017
    if isempty(AngleEmission)
        return
    end
    AngleName = 'AngComp';
end
I = corLambert(AngleEmission);

%% Output image

DT = cl_image.indDataType('Reflectivity');
that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', AngleName, 'Unit', 'dB', ...
    'ValNaN', NaN, 'ColormapIndex', 3, 'DataType', DT);

flag = 1;
