% Calcul du layer emission perdu par Caraibes
%
% Syntax
%   b = sonar_Caraibes_estimationTxAngle(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%   useRoll : {1='Prise en compte du roulis si il existe'} | 0=Pas de prise
%             en compte du roulis meme si il existe
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_Caraibes_estimationTxAngle(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_Caraibes_estimationTxAngle(this, AcrossDistance, varargin)

nbImages = length(this);
for i=1:nbImages
    if isempty(AcrossDistance)
        [flag, that] = unitaire_sonar_lateral_emission(this(i), [], varargin{:});
    else
        [flag, that] = unitaire_sonar_lateral_emission(this(i), AcrossDistance(i), varargin{:});
    end
    if ~flag
        return
    end
    this(i) = that;
end


function [flag, this] = unitaire_sonar_lateral_emission(this, AcrossDistance, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controles

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', cl_image.indDataType('Bathymetry'));
if ~flag
    this = [];
    return
end


A  = NaN(length(suby), length(subx), 'single');
w = warning;
warning('off', 'all');
for i=1:length(suby)
    Q = AcrossDistance.Image(suby(i),:) ./ this.Image(suby(i),:);
    Q = atan(Q) * (180 / pi);
    A(suby(i),:) = -Q;
end
warning(w)
this = replace_Image(this, A);
this.ValNaN = NaN;

%% Création de l'instance de sortie

this = majCoordonnees(this, subx, suby);
this = compute_stats(this);

this.TagSynchroContrast = num2str(rand(1));
this.CLim               = [this.StatValues.Min this.StatValues.Max];
this.Unit               = 'deg';
this.ColormapIndex      = 3;
this.DataType           = cl_image.indDataType('TxAngle');

this = update_Name(this);
