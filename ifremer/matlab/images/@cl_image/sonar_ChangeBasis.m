function [flag, that] = sonar_ChangeBasis(this, indImage, geoY_PingBeam, geoX_PingBeam, OriginPolynome, NomFicPolynome, gridSize, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, subLayers] = getPropertyValue(varargin, 'subLayers', []); %#ok<ASGLU>

that = cl_image.empty;
Bathy = this(indImage);

identBathymetry = cl_image.indDataType('Bathymetry');
flag = testSignature(Bathy, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', identBathymetry);
if ~flag
    return
end


% TODO : ATTENTION danger, il faut certainement faire
% PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
PC1 = Bathy.Sonar.PingCounter(suby);
PC2 = geoY_PingBeam.Sonar.PingCounter;
PC3 = geoX_PingBeam.Sonar.PingCounter;
[subPC1, ~, subZ] = intersect3(PC1, PC2, PC3); % , PC4); % Il faut generaliser intersect3 en intersectn
% subLon = subLat; % On considere que les images geoY_PingBeam et geoX_PingBeam ont les m�me ordonn�es : DANGER !
clear PC1 PC2 PC3
suby = suby(subPC1);

%% R�cup�ration des 3 matrices, X, Y et Z

kBeams = Bathy.x(subx);
kPings = Bathy.y(suby);
nBeams = length(kBeams);
nPings = length(kPings);

if Bathy.nbSlides ~= 1
    return
end

X = get_val_xy(geoX_PingBeam, kBeams, kPings);
Y = get_val_xy(geoY_PingBeam, kBeams, kPings);
Z = Bathy.Image(suby,subx);

%{
figure;
subplot(2,2,1); imagesc(X); colormap(jet(256)); colorbar; xlabel('Beams'); ylabel('Pings'); title('XGeo');
subplot(2,2,3); imagesc(Y); colormap(jet(256)); colorbar; xlabel('Beams'); ylabel('Pings'); title('YGeo');
subplot(2,2,2); imagesc(Z); colormap(jet(256)); colorbar; xlabel('Beams'); ylabel('Pings'); title('Z');
subplot(2,2,4); surf(X, Y, double(Z)); colormap(jet(256)); colorbar; shading('flat'); xlabel('XGeo'); ylabel('YGeo'); zlabel('Z')
%}

%% Estimation du plan qui minimise les moindres carr�s de Z - ZFit

[ny, nx] = size(Z);
coefReduc = sqrt((nx*ny) / 1e6);
if coefReduc > 1
    flag = -1;
    return
end

sub = find(~isnan(Z));
x = X(sub);
y = Y(sub);
z = double(Z(sub));

if OriginPolynome == 1
    coeff = pca([x y z]);
    PolynomePlan = coeff(:,3);
else
    PolynomePlan = loadmat(NomFicPolynome, 'nomVar', 'Polynome');
end

%% Algo Imen

a = PolynomePlan(1);
b = PolynomePlan(2);
c = PolynomePlan(3);
% Calcul de d : on prend un point de ZFit pour calculer ce plan

x1 = mean(x);
y1 = mean(y);
z1 = mean(z);
UnPoint = [x1 y1 z1];
d = -dot([a b c], UnPoint);

comments = sprintf('Plane coefficients : a = %f  b = %f  c = %f  d = %f', a, b, c, d);
fprintf('%s\n', comments);

%% Projection sur le plan

xp = ((b^2+c^2)*x - a*b*y       - a*c*z       - d*a);
yp = (-a*b*x      + (a^2+c^2)*y - b*c*z       - d*b);
zp = (-a*c*x      - b*c*y       + (a^2+b^2)*z - d*c);
% figure; plot3(xg2, yg2, zg2, '.b'); hold on; plot3(xp, yp, zp, 'r*');

u = [0 0 1];
xplan = u - dot(u, [a b c]) * [a b c];
yplan = cross([a b c], xplan);
x2d = dot([xp(:) yp(:) zp(:)]', repmat(xplan, numel(xp), 1)');
y2d = dot([xp(:) yp(:) zp(:)]', repmat(yplan, numel(xp), 1)');
%{
figure; plot(x2d, y2d); grid on;
figure; scatter(x2d, y2d, [], z(:)); grid on; colormap jet; xlabel('x2d'); ylabel('y2d'); title('Z'); colorbar;
figure; scatter(x2d-min(x2d), y2d-min(y2d), [], z(:)); grid on; colormap jet; xlabel('x2d'); ylabel('y2d'); title('Z');
%}

% TODO : afficher la r�flectivit� au lieu de z

% TODO : interpolation des x2d

dx = x - xp;
dy = y - yp;
dz = z - zp;
ZDif2Plan = sqrt(dx .^2 + dy.^2 + dz.^2);
signe_ZPlane = sign(dot(repmat([a b c], length(x), 1)', [dx dy dz]'));
signe_ZPlane = signe_ZPlane';
ZDif2Plan = ZDif2Plan .* signe_ZPlane;
% figure; scatter(x2d, y2d, [], ZDif2Plan(:)); grid on; colormap jet; xlabel('x2d'); ylabel('y2d'); title('Z'); colorbar;

% TODO : interpoler tous les layers : incidence, BS, tout


Z2d = NaN(nPings, nBeams, 'single');
Z2d(sub) = ZDif2Plan;

X2d = NaN(nPings, nBeams, 'double');
X2d(sub) = x2d;

Y2d = NaN(nPings, nBeams, 'double');
Y2d(sub) = y2d;

% ZFit = NaN(nPings, nBeams, 'single');
% ZFit(sub) = dz;

%% Output images

% R�sultat pour Imen : a, b, c, d, x2dM, y2dM
that    = inherit(Bathy, Z2d, 'subx', subx, 'suby', suby, 'AppendName', '_Z2d', 'ValNaN', NaN, 'ColormapIndex', 3);
that(2) = inherit(Bathy, X2d, 'subx', subx, 'suby', suby, 'AppendName', '_X2d', 'ValNaN', NaN, 'ColormapIndex', 3);
that(3) = inherit(Bathy, Y2d, 'subx', subx, 'suby', suby, 'AppendName', '_Y2d', 'ValNaN', NaN, 'ColormapIndex', 3);
% that(4) = inherit(Bathy, ZFit,      'subx', subx, 'suby', suby, 'AppendName', '_PolySurf', 'ValNaN', NaN, 'ColormapIndex', 3);

%% Cr�ation des images interpol�es

% figure; scatter(x, y, [], z); colormap jet; colorbar; axis('tight'); xlabel('x'); ylabel('y'); title('z'); 

% figure; plot(x2d, y2d, '.'); grid on;
% figure; scatter(x2d, y2d, [], z(:)); grid on; colormap jet; xlabel('x2d'); ylabel('y2d'); title('Z'); colorbar;

%{
layer = this(subLayers(1));
BS = layer.Image(suby,subx);
BS = BS(sub);
figure; scatter3(x, y, z, [], BS(:)); grid on; colormap gray; xlabel('x2d'); ylabel('y2d'); title('BS'); colorbar;
%}

% pasxq = (max(x2d) - min(x2d)) / 1000;
% pasyq = (max(y2d) - min(y2d)) / 1000;
% gridSize = max(pasxq, pasyq);

xq = min(x2d):gridSize:max(x2d);
yq = min(y2d):gridSize:max(y2d);
xq = centrage_magnetique(xq);
yq = centrage_magnetique(yq);

% Equation du plan
[xg2, yg2] = meshgrid(min(x):max(x), min(y):max(y));
zg2 = -(a*xg2 + b*yg2 + d) / c; % Equation du plan
% figure; plot3(x, y, z(:), '.k'); hold on; plot3(xp, yp, zp, '.b');  plot3(xg2, yg2, zg2, '.r')
% figure; plot3(x, y, z(:), '.k'); hold on; plot3(xp, yp, zp, '.b');  plot3(xg2, yg2, zg2, '.r')
% figure;  scatter3(x, y, z, [], BS(:)); colormap gray; hold on; plot3(xp, yp, zp, '.b');  plot3(xg2, yg2, zg2, '.r')


[vq, nq] = my_griddata(x2d, y2d, z(:), xq, yq);
% figure; imagesc(xq, yq, vq); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('vq'); 
% figure; imagesc(xq, yq, nq); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('nq'); 
vq = fillNaN_mean(vq, [5 5]);
% figure; imagesc(xq, yq, vq); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('vqi'); 

ZDif2Planq = my_griddata(x2d, y2d, ZDif2Plan, xq, yq);
ZDif2Planq = fillNaN_mean(ZDif2Planq, [5 5]);


ImageName = extract_ImageName(Bathy);
GeometryType = cl_image.indGeometryType('OrthoYX');
that(4) = cl_image('Image', vq, 'x', xq, 'y', yq, 'XUnit', 'm', 'YUnit', 'm', ...
    'DataType', Bathy.DataType, 'GeometryType', GeometryType, ...
    'XLabel', 'X2D', 'YLabel', 'Y2D', ...
    'Unit', Bathy.Unit, 'ColormapIndex', Bathy.ColormapIndex, 'Name', ImageName, ...
    'comments', comments);
that(4).Name = code_ImageName(that(4));

that(5) = inherit(that(4), nq,  'AppendName', 'nbPoints', 'ColormapIndex', 3);
that(6) = inherit(that(4), ZDif2Planq, 'AppendName', 'DifZ', 'ColormapIndex', 3);

%% Projection des autres layers

for k=1:length(subLayers)
    layer = this(subLayers(k));
    var = layer.Image(suby,subx);
    ClassName = class(var);
    var = singleUnlessDouble(var, 0);
    vq = my_griddata(x2d, y2d, var(:), xq, yq);
    vq = cast(vq, ClassName);
    vq = fillNaN_mean(vq, [5 5]);

    that(end+1) = inherit(that(4), vq, 'ColormapIndex', layer.ColormapIndex, 'Unit', layer.Unit, ...
        'DataType', layer.DataType); %#ok<AGROW>
end

flag = 1;
