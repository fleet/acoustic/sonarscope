% Calcul du cap d'une image a parti des coordonnees geographiques
%
% Syntax
%   b = sonar_ComputeHeading(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   for i=1:2
%       [I, label] = ImageSonar(i);
%       a(i) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   end;
%   b = sonar_ComputeHeading(a);
%   imagesc(b);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_ComputeHeading(this, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_sonar_ComputeHeading(this(i), varargin{:});
end


function this = unitaire_sonar_ComputeHeading(this, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

if isempty(suby)
    suby = 1:length(this.y);
end

%% M�thode

method = 1;
Tw = [];

%% Contr�les

if isempty(this.Sonar.FishLatitude)
    my_warndlg('SonarFishLatitude non renseigne dans cette image', 1);
    return
end

if isempty(this.Sonar.FishLongitude)
    my_warndlg('SonarFishLongitude non renseigne dans cette image', 1);
    return
end

if isempty(this.Sonar.Heading)
    this.Sonar.Heading = calCapFromLatLon(this.Sonar.FishLatitude, this.Sonar.FishLongitude, 'Time', this.Sonar.Time);
end

%% Calcul

this.Sonar.Heading(suby) = calCapFromLatLon(this.Sonar.FishLatitude(suby), this.Sonar.FishLongitude(suby), ...
    'Time', this.Sonar.Time(suby), 'method', method, 'TempsIntegration', Tw);

