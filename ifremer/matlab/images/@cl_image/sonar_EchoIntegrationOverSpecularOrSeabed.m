function [EI, Unit] = sonar_EchoIntegrationOverSpecularOrSeabed(aAcrossDist, aAlongDist, Depth, ...
    typePolarEchogram, DataEchogram, nomDirRaw, nomFicRaw, subEchogram, subDepth, ...
    params_ZoneEI, AntennaAcrossDist, ...
    StepDisplay, varargin)

[varargin, EchogramFileName] = getPropertyValue(varargin, 'EchogramFileName', []); %#ok<ASGLU>

if isempty(EchogramFileName)
    ExtImage = '.xml';
else
    [~, ~, ExtImage] = fileparts(EchogramFileName);
    
    if strcmp(ExtImage, '.nc')
    	ncID = netcdf.open(EchogramFileName);
    end
    map = jet(256);
end

%% D�termination de l'unit�

if isfield(DataEchogram, 'ImageUnit')
    Unit = DataEchogram.ImageUnit;
elseif isfield(DataEchogram, 'ChiefScientist')
    if isempty(DataEchogram.ChiefScientist)
        Unit = [];
    elseif strcmp(DataEchogram.ChiefScientist, 'Amp')
        Unit = 'Amp';
    elseif strcmp(DataEchogram.ChiefScientist, 'dB')
        Unit = 'dB';
    else
        Unit = [];
    end
else
    Unit = [];
end

%% Affichage de la bathy

Z = Depth.Image(subDepth(1):subDepth(end), :);
% nbCols = size(Z,2);
% subBeams = 1:nbCols;
% [Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUI(Z, subBeams, subDepth, StepDisplay);
[Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUInew(Z(:, :), StepDisplay); % Modif JMA le 02/10/2020

%% Initialisations

firstDisplay = 1;
EI = EI_InitStruct(subDepth);

%% Loop on pings

nbPings = length(subEchogram);
str1 = sprintf('Traitement Echo-Int�gration sur fichier "%s"', nomFicRaw);
str2 = sprintf('Echo Integration of "%s"', nomFicRaw);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    flagDisplay = (StepDisplay ~= 0) && (mod(k, StepDisplay) == 0);
    iPing = DataEchogram.iPing(subEchogram(k));
    
    %% D�termination de l'immersion
    
    if isfield(DataEchogram, 'Immersion')
        Immer = DataEchogram.Immersion(subEchogram(k));
    else
        Immer = 0;
    end
    if isnan(Immer)
        Immer = 0; % Au cas o�
    end
    
    if strcmp(ExtImage, '.nc')
        kGrp = k; % iPing
%         [flag, DataGrp, grpName, sliceNames, layerUnits, MaskSignification, flagAtt] = NetcdfGlobe3DUtils.readOneGroup(ncID, kGrp);
        [flag, DataGrp] = NetcdfGlobe3DUtils.readOneGroup(ncID, kGrp);
        if ~flag
            break
        end
        if typePolarEchogram == 1
            I = flipud(DataGrp.raw);
        else
            I = flipud(DataGrp.comp);
        end
        Immer = DataGrp.elevation(1,1);
    else
        
        %% Recherche du nom du fichier contenant l'�chogramme
        
        [flag, nomFic2, ExtImage] = WC_FindImageName(iPing, nomDirRaw);
        if ~flag
            continue
        end
        
        %% Lecture de l'�chogramme
        
        [I, map] = imread(nomFic2);
        Info = imfinfo(nomFic2); % Ajout JMA le 07/10/2019
        I((I < Info.SMinSampleValue) | (I > Info.SMaxSampleValue)) = NaN;
    end
    
%     % Ajout JMA pour corriger un d�faut. Il faut trouver l'explication en aval
%     tmp = I(1:5,1:5);
%     listeVal = unique(tmp(:));
%     if length(listeVal) == 1
%         I((I == listeVal)) = NaN;
%     end
    
    %% Coordonn�es des sondes
    
    AcrossDistPing = aAcrossDist.Image(subDepth(k),:);
    AlongDistPing  = aAlongDist.Image(subDepth(k),:);
    DepthPing      = Depth.Image(subDepth(k),:); % Depth est la donn�e compens�e de l'immersion pour Reson (distance surface-fond)

    %% Calcul du masque vectoriel

    switch params_ZoneEI.Where
        case 'ASC' % OverSpecular
            [flag, xBorneInf, zBorneInf, xBorneSup, zBorneSup, Angle] = ...
                EI_Compute_LimitsOverSpecular(DepthPing, AcrossDistPing, params_ZoneEI, Immer, AntennaAcrossDist);
        otherwise % Over Seafloor
            [flag, xBorneInf, zBorneInf, xBorneSup, zBorneSup, Angle] = ...
                EI_Compute_LimitsOverSeabed(DepthPing, AcrossDistPing, params_ZoneEI, Immer, AntennaAcrossDist);
    end
    if ~flag
        continue
    end

    %% Calcul du masque raster

    xEIImage = linspace(DataEchogram.xBab(subEchogram(k)), DataEchogram.xTri(subEchogram(k)), size(I,2));
    z = linspace(Immer, DataEchogram.Depth(subEchogram(k)), size(I,1));
    BW = roipoly(xEIImage, z, I, ...
        [xBorneInf fliplr(xBorneSup)], [zBorneInf fliplr(zBorneSup)]);
    
    %% Calcul de l'echo-int�gration verticale

    [Moy, N] = WC_ComputeVerticalMean(I, BW, ExtImage, Unit);
    
    %% Remplissage de la structure
    
    EI = EI_FillStruct(EI, k, Moy, N, xEIImage, Angle, AcrossDistPing, AlongDistPing);
    
	%% Visualisation du r�sultat

    if flagDisplay
        [Fig, hAxe, hImage, hPing, hCurve, firstDisplay] = EI_DisplayResults(Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
            I, map, xEIImage, z, BW, ...
            AcrossDistPing, DepthPing, ...
            nomFicRaw, subEchogram, subDepth(k), k, nbPings, iPing, ...
            DataEchogram, Immer, ...
            xBorneInf, zBorneInf, xBorneSup, zBorneSup);
    end
end
my_close(hw)
my_close(Fig)

if strcmp(ExtImage, '.nc')
    netcdf.close(ncID);
end
