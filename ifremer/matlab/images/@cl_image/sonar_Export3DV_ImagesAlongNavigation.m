function flag = sonar_Export3DV_ImagesAlongNavigation(this, NomFicXML, typeOutput, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, CeleriteSediments] = getPropertyValue(varargin, 'CeleriteSediments', []);
[varargin, CLim]              = getPropertyValue(varargin, 'CLim',              this.CLim);
[varargin, Colormap]          = getPropertyValue(varargin, 'Colormap',          this.Colormap);
[varargin, Video]             = getPropertyValue(varargin, 'Video',             this.Video);

[varargin, Mute] = getFlag(varargin, 'Mute'); %#ok<ASGLU>

%% Test de la signature

flag = testSignature(this, 'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingDepth')]);
if ~flag
    return
end

%% Cr�ation de l'image

I = this.Image(suby,subx);
subNaN = isnan(I);

if Video == 2
    Colormap = flipud(Colormap);
end

I = gray2ind(mat2gray(I, double(CLim)), size(Colormap,1));
I(I == 0) = 1;
I(subNaN) = 0;
% I = flipud(I); % A confirmer

% if flagHeight
%     for k=1:length(Height)
%         subWC = 1:(floor(Height(k)));
%         I(subWC,k) = NaN;
%     end
% end

%% Cr�ation du fichier XML-Bin

switch this.GeometryType
    case cl_image.indGeometryType('PingSamples')
        if isempty(this.Sonar.SurfaceSoundSpeed)
            SoundSpeedInWater = 1500 + zeros(length(suby),1, 'single');
        else
            SoundSpeedInWater = this.Sonar.SurfaceSoundSpeed(suby);
        end
        % figure; plot(SoundSpeedInWater, '-*'); grid on; title('SoundSpeedInWater')
        % SoundSpeedInWater(SoundSpeedInWater == 0) = NaN;
        SoundSpeedInWater(SoundSpeedInWater < 1000) = NaN;
        SoundSpeedInWater(isnan(SoundSpeedInWater)) = 1500;
        
        deltax = mean(SoundSpeedInWater ./ (2*this.Sonar.SampleFrequency(suby)), 'omitnan');
        subNaN = find(isnan(deltax));
        if ~isempty(deltax)
            subNonNaN = find(~isnan(deltax));
            deltax(subNaN) = interp1(subNonNaN, deltax(subNonNaN), subNaN);
        end
        if isempty(this.Sonar.Immersion)
            MinHeight_m = -this.x(subx(1))   * deltax;
            MaxHeight_m = -this.x(subx(end)) * deltax;
            %     MinHeight_m = ones(length(suby), 1, 'single') * MinHeight_m;
            %     MaxHeight_m = ones(length(suby), 1, 'single') * MaxHeight_m;
        else
            Immersion_m = -abs(this.Sonar.Immersion(suby));
            Heave = this.Sonar.Heave; %#ok<NASGU>
            
            %{
% Comment� le 30/01/2018 apr�s modif ExRaw2ssc_Position pour traiter immersion = - Heave si Immersion_m==0
if ~isempty(Heave) && ~all(isnan(Heave(suby)))
Immersion_m = Immersion_m - Heave(suby);
end
            %}
            
            Immersion_m = my_interpUsual(Immersion_m, 'linear');
            Immersion_m = my_interpUsual(Immersion_m, 'nearrest', 'extrap');

            % TODO : Ceci est un artifice pour savoir si on a fait une suppression
            % de la colonne d'eau. Il faudrait cr�er une propri�t� "Mute" dans
            % this.Sonar pour savoir dans quel �tat est la donn�e de sondeur de
            % s�diment

            %                 deltax = deltax .* CeleriteSediments ./ SoundSpeedInWater;
            MinHeight_m = Immersion_m - (this.x(subx(1))   .* deltax);
            MaxHeight_m = Immersion_m - (this.x(subx(end)) .* deltax);
        end

    case cl_image.indGeometryType('PingDepth')
        if isempty(this.Sonar.Immersion)
            MinHeight_m = -this.x(subx(1));
            MaxHeight_m = -this.x(subx(end));
        else
            Immersion_m = -abs(this.Sonar.Immersion(suby));
            Heave = this.Sonar.Heave;
            if ~isempty(Heave) && ~all(isnan(Heave(suby)))
                Immersion_m = Immersion_m - Heave(suby);
            end
            Immersion_m = my_interpUsual(Immersion_m, 'linear');
            Immersion_m = my_interpUsual(Immersion_m, 'nearrest', 'extrap');
            
            MinHeight_m = Immersion_m - (this.x(subx(1)));
            MaxHeight_m = Immersion_m - (this.x(subx(end)));
        end
        
    case this.GeometryType == cl_image.indGeometryType('PingDepth')
        Immersion_m = -abs(this.Sonar.Immersion(suby));
        MinHeight_m = repmat(min(this.x), length(suby), 1) + Immersion_m;
        MaxHeight_m = repmat(max(this.x), length(suby), 1) + Immersion_m;
end

% if ~isempty(this.Sonar.Height)
%     MinHeight_m = MinHeight_m + min(this.Sonar.Height(suby));
%     MaxHeight_m = MaxHeight_m + min(this.Sonar.Height(suby));
% end

FishLatitude  = this.Sonar.FishLatitude(suby);
FishLongitude = this.Sonar.FishLongitude(suby);

if any(isnan(FishLatitude))
    %         fprintf('FishLatitude NaN\n');
    subNaN = isnan(FishLatitude);
    subNonNaN = find(~subNaN);
    if length(subNonNaN) < 2
        ImageName = extract_ImageName(this);
        str1 = sprintf('L''export de "%s" au format GLOBE a �chou� car la navigation semble �tre vide.', ImageName);
        str2 = sprintf('Could not export data in GLOBE format for "%s" because the navigation seems empty.', ImageName);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    subNaN = find(subNaN);
    FishLatitude(subNaN) = interp1(subNonNaN, FishLatitude(subNonNaN), subNaN, 'linear', 'extrap');
end
if any(isnan(FishLongitude))
    %         fprintf('LongitudeNadir NaN\n');
    subNaN = isnan(FishLongitude);
    subNonNaN = find(~subNaN);
    subNaN = find(subNaN);
    FishLongitude(subNaN) = interp1(subNonNaN, FishLongitude(subNonNaN), subNaN, 'linear', 'extrap');
end

if ~isempty(this.Sonar.Time)
    T = this.Sonar.Time.timeMat;
    T = T(suby);
    subNaN = isnan(T);
    subNonNaN = find(~subNaN);
    subNaN = find(subNaN);
    T(subNaN) = interp1(subNonNaN, T(subNonNaN), subNaN, 'linear', 'extrap');
    T = cl_time('timeMat', T);
end

seismic.name = extract_ImageName(this);

[~, name] = fileparts(NomFicXML);
seismic.name = name;

seismic.from = 'Unkown';
if isempty(this.Sonar.Time)
    seismic.Date = zeros(length(suby), 1);
    seismic.Hour = zeros(length(suby), 1);
else
    seismic.Date = T.date;
    seismic.Hour = T.heure;
end
% seismic.PingNumber      = this.Sonar.PingNumber(suby);
seismic.PingNumber = suby';

Tide = get(this, 'Tide');
if isempty(Tide) || all(Tide == 0) || all(isnan(Tide))
    seismic.Tide = zeros(size(seismic.PingNumber));
else
    seismic.Tide  = Tide;
end
    
if isfield(this.Sonar, 'TxAcrossAngle') && ~isempty(this.Sonar.TxAcrossAngle)  % Donn�es ME70
    Heading = this.Sonar.Heading(suby);
    subNaN = isnan(Heading);
    subNonNaN = find(~subNaN);
    subNaN = find(subNaN);
    Heading(subNaN) = interp1(subNonNaN, Heading(subNonNaN), subNaN, 'linear', 'extrap');
    
    if isempty(this.Sonar.TxAcrossAngle)
        AngleFaisceaux = 0;
    else
        AngleFaisceaux = this.Sonar.TxAcrossAngle(suby);
    end
    AngleFaisceaux = -AngleFaisceaux; % Vaudrait mieux faire DepthTop  =- MinHeight_m .* cosd(AngleFaisceaux);
    %     HeightInSample    = this.Sonar.Height(suby);
    %     SampleFrequency   = this.Sonar.SampleFrequency(suby);
    %     SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed(suby);
    %     MaxHeight_m = -SurfaceSoundSpeed .* subx(end) ./ (2*SampleFrequency); % TODO / prendre la vitesse int�gr�e !!!
    %     MinHeight_m = -SurfaceSoundSpeed .* subx(1) ./ (2*SampleFrequency); % TODO / prendre la vitesse int�gr�e !!!
    
    [x0, y0] = latlon2xy(this.Carto, FishLatitude, FishLongitude);
    c = cosd(-Heading);
    s = sind(-Heading);
    
    %     C = [0 Deltay] * [c s; -s c];
    %     [lat0 ,lon0] = xy2latlon(this.Carto, x0+C(:,1), y0+C(:,2));
    
    Z0 = zeros(size(suby));
    AcrossDistTop = MinHeight_m .* sind(AngleFaisceaux);
    AlongDistTop  = Z0; % Vaut mieux laisser cela en place au cas o� !
    AcrossDistEnd = MaxHeight_m .* sind(AngleFaisceaux);
    AlongDistEnd  = Z0; % Vaut mieux laisser cela en place au cas o� !
    FishLatitudeBottom  = Z0;
    FishLongitudeBottom = Z0;
    FishLatitudeTop     = Z0;
    FishLongitudeTop    = Z0;
    DepthTop            = MinHeight_m .* cosd(AngleFaisceaux);
    DepthBottom         = MaxHeight_m .* cosd(AngleFaisceaux);
    for k=1:length(c)
        C = [AcrossDistEnd(k) AlongDistEnd(k)] * [c(k) s(k); -s(k) c(k)];
        [FishLatitudeBottom(k), FishLongitudeBottom(k)] = xy2latlon(this.Carto, x0(k)+C(:,1), y0(k)+C(:,2));
        C = [AcrossDistTop(k) AlongDistTop(k)] * [c(k) s(k); -s(k) c(k)];
        [FishLatitudeTop(k), FishLongitudeTop(k)] = xy2latlon(this.Carto, x0(k)+C(:,1), y0(k)+C(:,2));
    end
    
    seismic.LatitudeBottom   = FishLatitudeBottom;
    seismic.LongitudeBottom  = FishLongitudeBottom;
    seismic.LatitudeTop      = FishLatitudeTop;
    seismic.LongitudeTop     = FishLongitudeTop;
    seismic.DepthTop         = DepthTop;
    seismic.DepthBottom      = DepthBottom;
else
    seismic.LatitudeBottom   = FishLatitude;
    seismic.LongitudeBottom  = FishLongitude;
    seismic.LatitudeTop      = FishLatitude;
    seismic.LongitudeTop     = FishLongitude;
    if length(MinHeight_m) == 1
        seismic.DepthTop     = MinHeight_m + zeros(size(FishLatitude));
        seismic.DepthBottom  = MaxHeight_m + zeros(size(FishLatitude));
    else
        seismic.DepthTop     = MinHeight_m;
        seismic.DepthBottom  = MaxHeight_m;
    end
end

if typeOutput == 1 % XML-Bin
    flag = create_XMLBin_ImagesAlongNavigation(I, NomFicXML, Colormap, seismic);
else % Netcdf
    groupNames{1} = 'Seismics';
    sliceNames{1} = 'Seismics';
    sliceUnits{1} = this.Unit;
    ncFileName = strrep(NomFicXML, '.xml', '.g3d.nc');
    seismic.Reflectivity = this.Image(suby,subx);
    seismic.Reflectivity = seismic.Reflectivity';
    seismic.Reflectivity = flipud(seismic.Reflectivity);
    seismic.LatitudeTop         = seismic.LatitudeTop';
    seismic.LongitudeTop        = seismic.LongitudeTop';
    seismic.Dimensions.nbGroups = 1;
    seismic.Dimensions.nbSlices = 1;
    seismic.Dimensions.nbRows   = size(seismic.Reflectivity,1);
    seismic.Dimensions.nbPings  = size(seismic.Reflectivity,2);
    flag = NetcdfGlobe3DUtils.createGlobeFlyTexture(ncFileName, seismic, groupNames, sliceNames, sliceUnits);
end

if ~Mute
    str1 = 'L''export vers GLOBE est termin�.';
    str2 = 'The export to GLOBE is over.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'CalculEchoIntegration terminee');
end

%{
FigUtils.createSScFigure;
subplot(5,1,1); PlotUtils.createSScPlot(T, seismic.LatitudeTop(:)); grid on; title('LatitudeTop')
subplot(5,1,2); PlotUtils.createSScPlot(T, seismic.LongitudeTop(:)); grid on; title('LongitudeTop')
subplot(5,1,3); PlotUtils.createSScPlot(T, seismic.DepthTop(:)); grid on; title('DepthTop')
subplot(5,1,4); PlotUtils.createSScPlot(T, seismic.DepthBottom(:), 'r'); grid on; title('DepthBottom')
subplot(5,1,5); PlotUtils.createSScPlot(T, seismic.DepthTop(:)); grid on;
hold on; PlotUtils.createSScPlot(T, seismic.DepthBottom(:), 'r'); title('DepthTop, DepthBottom')
find(isnan(seismic.LatitudeTop(:)))
find(isnan(seismic.LongitudeTop(:)))
find(isnan(seismic.DepthTop(:)))
find(isnan(seismic.DepthBottom(:)))
%}
