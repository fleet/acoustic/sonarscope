function flag = sonar_Export3DV_ImagesAlongNavigation_ByBlocks(this, NomFicXML, TailleMax, typeOutput, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, CeleriteSediments] = getPropertyValue(varargin, 'CeleriteSediments', []);

[varargin, Mute] = getFlag(varargin, 'Mute');

flag = 0;

% Cas d'impossibilit� de sortie dans le cas du EK60 de type Horizontal.
% Artifice en explorant la cha�ne Comments en attendant davoir un champ
% Orientation dans l'objet Image.
strLayerComments = this.Comments;
if contains(strLayerComments, 'Horizontal')
    str1 = 'La g�om�trie Horizontale du layer ne rend pas possible, pour le moment, l''extraction vers GLOBE';
    str2 = 'Horizontal geometry of the layer make impossible, for the moment, the GLOBE export';
    my_warndlg(Lang(str1,str2), 1);
    return
end

nx = length(subx);
ny = length(suby);
if nx > TailleMax
    strFR = 'L''image est trop haute pour �tre observ�e par GLOBE.';
    strUS = 'The number of columns of the image is too high for GLOBE.';
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'PbWCALongGenerate', 'TimeDelay', 10);
    return
end

% Le calcul de d�coupage ne se fait que le long du profil (pour l'instant).
TailleMax = min(TailleMax, nx*ny);
nBBlocks  = ceil(ny / TailleMax);
n2        = floor(ny / nBBlocks);
[nomDir, nomFic] = fileparts(NomFicXML);

str1 = 'Export par blocks.';
str2 = 'Export by blocks.';
hw = create_waitbar(Lang(str1,str2), 'N', nBBlocks);
for k=1:nBBlocks
    my_waitbar(k, nBBlocks, hw)
    
    sub = (0:(n2+1)) + (k-1)*n2;
    sub(sub < 1) = [];
    sub(sub > ny) = [];
    NomFicXMLBlock = fullfile(nomDir, [nomFic '_' num2str(k, '%02d') '.xml']);
    flag = sonar_Export3DV_ImagesAlongNavigation(this, NomFicXMLBlock, typeOutput, 'subx', subx, 'suby', suby(sub), ...
        'CeleriteSediments', CeleriteSediments, 'Mute', varargin{:});
    if ~flag
        break
    end
end
my_close(hw)

if ~Mute
    str1 = 'L''export vers GLOBE est termin�.';
    str2 = 'The export to GLOBE is over.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportGLOBEAlongNavigationIsOver');
end

flag = 1;
