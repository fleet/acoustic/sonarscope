% Plot d'un nuage de points
%
% Syntax
%   sonar_ExportFly(a, YGeo, XGeo, Z, ...)
%
% Input Arguments
%   a : instance de cl_image
%   YGeo : instance de cl_image de DataType='Latitude'
%   XGeo : instance de cl_image de DataType='Longitude'
%   Z   : [] ou Altitude si ce n'est pas a
%
% Name-Value Pair Arguments
%   subx   : subsampling in X
%   suby   : subsampling in Y
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   b = cl_car_ima(nomFic);
%   c = view(b, 'Sonar.Ident', 3);
%   a = get(c, 'Images');
%   imagesc(a)
%
%   sonar_ExportFly(a(1), a(2), a(3));
%
% See also cl_image Authors
% Authors : JMA + AG
% ----------------------------------------------------------------------------

function flag = sonar_ExportFly(this, XGeo, YGeo, Z, flyFileName, varargin)

% % TODO : d�brancher la s�lection multiple dans ALL.PArams.ExportPLY quand l'export multivariable
% % sera branch� ici
this = this(1);
% this(2) = -this(1);




[subx, suby, varargin] = getSubxSuby(this(1), varargin);

[varargin, xShift] = getPropertyValue(varargin, 'xShift', 0);
[varargin, yShift] = getPropertyValue(varargin, 'yShift', 0); %#ok<ASGLU>

flag = 1;

if xShift ~= 0
    XGeo = XGeo - xShift;
end
if yShift ~= 0
    YGeo = YGeo - yShift;
end

% TODO : ATTENTION danger, il faut certainement faire
% PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
PC1 = this(1).Sonar.PingCounter(suby);
PC2 = YGeo.Sonar.PingCounter;
if isempty(Z)
    PC3 = XGeo.Sonar.PingCounter;
else
    PC3 = Z.Sonar.PingCounter;
end
[subPC1, ~, subZ] = intersect3(PC1, PC2, PC3); % , PC4); % Il faut generaliser intersect3 en intersectn
% subLon = subLat; % On considere que les images YGeo et XGeo ont les m�me ordonn�es : DANGER !
clear PC1 PC2 PC3
suby = suby(subPC1);

%% Si axe designe, traitement du trac� et de l'affichage

x = this(1).x(subx);
y = this(1).y(suby);

for k=1:length(this)
    if this(k).nbSlides ~= 1
        return
    end
    
    V{k} = this(k).Image(suby,subx);
    if this(1).SpectralStatus == 2
        V{k} =  20 * log10(abs(V));
    end
end

Z = Z.Image(subZ,subx);
xLon = get_val_xy(XGeo, x, y);
yLat = get_val_xy(YGeo, x, y);

%% Export

x = xLon(:);
y = yLat(:);
z = Z(:);
subNaN = isnan(z);
x(subNaN) = [];
y(subNaN) = [];
z(subNaN) = [];

for k=1:length(this)
    v{k} = V{k}(:); %#ok<AGROW> % todo BS
    
    v{k}(subNaN) = []; %#ok<AGROW>
    % z = singleUnlessDouble(z, 'NaN');
    % v = singleUnlessDouble(v, this(1).ValNaN);
    v{k} = double(v{k}); %#ok<AGROW>
    if ~isnan(this(1).ValNaN)
        v{k}(v{k} == this(1).ValNaN) = NaN; %#ok<AGROW>
    end
end

str = sprintf('Exporting flyFileName %s', flyFileName);
WorkInProgress(str)
flag = my_plywrite(flyFileName, [], [x y z [v{:}]]);
% [Tri3, Pts3] = plyread(flyFileName);
