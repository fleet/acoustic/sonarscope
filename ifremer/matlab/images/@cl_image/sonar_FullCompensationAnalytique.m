% Calcul de la compensation de Lurton
%
% Syntax
%   [flag, b] = sonar_FullCompensationAnalytique(a, ...)
%
% Input Arguments
%   flag : 0=KO, 1=OK
%   a    : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx        : subsampling in X
%   suby        : subsampling in Y
%   identCourbe : Numero du modele a utiliser
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_FullCompensationAnalytique(this, Incidence, varargin)

flag = 0; % Au cas o� length(this) = 0
N = length(this);
str1 = 'Calcul des images "FullCompensationAnalytique"';
str2 = 'Computing images of "FullCompensationAnalytique"';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, that] = unitaire_sonar_FullCompensationAnalytique(this(k), Incidence, varargin{:});
    if flag
        this(k) = that;
    else
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')


function [flag, this] = unitaire_sonar_FullCompensationAnalytique(this, Incidence, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, identCourbe] = getPropertyValue(varargin, 'identCourbe', []); %#ok<ASGLU>

flag = 0;

if isempty(identCourbe)
    %     identCourbe = 1;
    str1 = 'Quelle mod�les doit-on consid�rer pour la compensation du BS ?';
    str2 = 'Select the models that you want to use for the BS compensation ?';
    [flag, identCourbe] = selectionCourbesStats(this, 'Tag', 'BS', 'Title', Lang(str1,str2)); %, 'NoManualSelection', 1);
    if ~flag || isempty(identCourbe)
        flag = 0;
        return
    end
end

identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');

bilan = this.CourbesStatistiques(identCourbe).bilan;
if ~isfield(bilan{1}, 'model') || isempty(bilan{1}(1).model)
    str1 = 'Il n''y a pas de mod�le analytique dans cette courbe.';
    str2 = 'No analytical model available in this curve';
    my_warndlg(Lang(str1,str2), 1);
    return
end

model = bilan{1}(1).model;
nomModel = model.Name;

[~, ~, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Incidence);
subx = subx(1:length(subxb)); % Rustine pour corriger une erreur
A = Incidence;
if A.DataType == identBeamPointingAngle
    if ~isempty(A.Sonar.Roll)
        A = A - A.Sonar.Roll(:,1); % Rajout� par JMA le 15/01/2013
    end
end
G = NaN(size(A.Image), 'single');
for k=1:length(bilan{1})
    model = bilan{1}(k).model;
    
    % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
    % flipud (voir cl_image/plus
    
    %     this.Image = compute(model, 'x', Incidence.Image(suby, subx));
    
    angles = A.Image(subyb, subxb);
    X = model.compute('x', angles(:));
    
    sub = ~isnan(X);
    G(sub) = X(sub);
end

this = replace_Image(this, G);
this.ValNaN = NaN;

this.Sonar.BS.BSLurtonParameters = model.getParamsValue;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
this.CLim               = [this.StatValues.Min this.StatValues.Max];
this.Unit               = 'dB';
this.ColormapIndex      = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Reflectivity');
Append = ['Niveau ' nomModel];
this = update_Name(this, 'Append', Append);

flag = 1;
