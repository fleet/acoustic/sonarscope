% Calcul de l'image conforme a une courbe de BS mesuree
%
% Syntax
%   b = sonar_FullCompensationTable(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx        : subsampling in X
%   suby        : subsampling in Y
%   identCourbe : Numero du modele a utiliser
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_FullCompensationTable(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_FullCompensationTable(this, Incidence, varargin)

N = length(this);
str1 = 'Calcul des images "FullCompensationAnalytique"';
str2 = 'Computing images of "FullCompensationAnalytique"';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    this(k) = unitaire_sonar_FullCompensationTable(this(k), Incidence, varargin{:});
end
my_close(hw, 'MsgEnd')


function this = unitaire_sonar_FullCompensationTable(this, Incidence, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, identCourbe] = getPropertyValue(varargin, 'identCourbe', []); %#ok<ASGLU>

if isempty(identCourbe)
    identCourbe = 1;
end

% identEmissionAngle     = cl_image.indDataType('TxAngle');
% identIncidenceAngle    = cl_image.indDataType('IncidenceAngle');
% identAveragedPtsNb     = cl_image.indDataType('AveragedPtsNb');
% identRxBeamAngle       = cl_image.indDataType('RxBeamAngle');
identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');

bilan = this.CourbesStatistiques(identCourbe).bilan;
x = bilan{1}.x;
y = bilan{1}.y;
nomZone = this.CourbesStatistiques(identCourbe).bilan{1}(1).nomZoneEtude;

warning('off', 'all'); % Pour eviter le message "NaN found in Y, interpolation at undefined values will result in undefined values"

A = Incidence;
if A.DataType == identBeamPointingAngle
    if ~isempty(A.Sonar.Roll)
        A = A - A.Sonar.Roll(:,1); % Rajout� par JMA le 15/01/2013
    end
end

this = replace_Image(this, interp1(x, y, A.Image(suby, subx)));
warning('on', 'all');

this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'dB';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Unknown');
Append = [' - Niveau Full Compensation Table ' nomZone];
this = update_Name(this, 'Append', Append);
