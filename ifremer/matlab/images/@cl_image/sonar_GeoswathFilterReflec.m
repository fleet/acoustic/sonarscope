function [flag, this] = sonar_GeoswathFilterReflec(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, X] = sonar_GeoswathFilterReflec_unitaire(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = X;
end


function [flag, this] = sonar_GeoswathFilterReflec_unitaire(this, varargin)

% [subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Appel de la fonction de filtrage

H = abs(this.Sonar.Height(:,:));
x = this.x;
% h = fdesign.lowpass('N,F3dB', 12, 0.15);
% d1 = design(h, 'butter');
% n = 2*length(d1.ScaleValues)+1;
% w = gausswin(40);
% w = w / sum(w);

% TODO : ici on peut ajuster la largeur du filtre : remplacer 20 par la
% nouvelle valeur
w =1:20;
w = w / sum(w);
n = 3*length(w)-2;
for k=1:this.nbRows
    Ping = this.Image(k,:);
    sub = find(((x < -H(k,1)) | (x > H(k,2))) & ~isnan(Ping));
    if length(sub) > n
        %         this.Image(k,sub) =  filtfilt(d1.sosMatrix, d1.ScaleValues, double(Ping(sub))); %filtfilt(b,a,X)
        this.Image(k,sub) =  filtfilt(w, 1, double(Ping(sub))); %filtfilt(b,a,X)
    end
end

% this = replace_Image(this, X);

%% Mise � jour de coordonnees

% this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'PingFilter');
flag = 1;
