function [flag, A, LatLon] = sonar_GeoswathInterpAngles(A, LatLon)

x = A.x;
[~, i0] = min(abs(x));

[subA, subLatLon] = intersect3(A.Sonar.PingCounter(:,1), LatLon(1).Sonar.PingCounter(:,1), LatLon(2).Sonar.PingCounter(:,1));

N = length(subA);
for k=1:N
    
    H = A.Sonar.Height(subA(k),:);
    
    % Interpolation
    
    Angle = A.Image(subA(k),:);
    Lat   = LatLon(1).Image(subLatLon(k),:);
    Lon   = LatLon(2).Image(subLatLon(k),:);
    
    if isnan(H(1))
        continue
    end
    ihBab = i0 + floor(H(1));
    if isfinite(ihBab)
        ABab   = Angle(1:ihBab);
        LatBab = Lat(1:ihBab);
        LonBab = Lon(1:ihBab);
        
        ABab(end) = 0;
        sub = isnan(ABab);
        subNonNan = find(~sub);
        if length(subNonNan) > 50
            subNaN = find(sub);
            ABab(subNaN) = interp1(subNonNan, ABab(subNonNan), subNaN);
            Angle(1:ihBab) = ABab;
        end
        
        if ~isfinite(H(2))
            continue
        end
        ihTri = i0 - floor(H(2));
        
        ATri   = Angle(ihTri:end);
        LatTri = Lat(ihTri:end);
        LonTri = Lon(ihTri:end);
        
        ATri(1) = 0;
        sub = isnan(ATri);
        subNonNan = find(~sub);
        if length(subNonNan) > 50
            subNaN = find(sub);
            ATri(subNaN) = interp1(subNonNan, ATri(subNonNan), subNaN);
            Angle(ihTri:end) = ATri;
        end
        
        A.Image(subA(k),:) = Angle;
        %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Angle); grid
        
        subBab = 1:length(LatBab);
        subTri = (1:length(LatTri)) + length(LatBab);
        LatBabTri = [LatBab LatTri];
        LonBabTri = [LonBab LonTri];
        sub = isnan(LatBabTri);
        subNonNan = find(~sub);
        if length(subNonNan) > 50
            subNaN = find(sub);
            LatBabTri(subNaN) = interp1(subNonNan, LatBabTri(subNonNan), subNaN);
            LonBabTri(subNaN) = interp1(subNonNan, LonBabTri(subNonNan), subNaN);
            LatBab = LatBabTri(subBab);
            LatTri = LatBabTri(subTri);
            LonBab = LonBabTri(subBab);
            LonTri = LonBabTri(subTri);
            Lat(1:ihBab)   = LatBab;
            Lat(ihTri:end) = LatTri;
            Lon(1:ihBab)   = LonBab;
            Lon(ihTri:end) = LonTri;
            LatLon(1).Image(subLatLon(k),:) = Lat;
            LatLon(2).Image(subLatLon(k),:) = Lon;
        end
    end
    
    % Extrapolation ???  
end

% SonarScope([A LatLon])
flag = 1;
