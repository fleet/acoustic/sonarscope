function flag = sonar_HACWriteWC_profil(this, nomFicHac, nomDirXml, CLim, CLimNaN, ColormapIndex, ...
    HauteurMinInMeters, SuppressionOverHeight, TailleMax, MeanTide, varargin)

[subx, suby, varargin] = getSubxSuby(this(1), varargin{:});

[varargin, Mute] = getFlag(varargin, 'Mute'); %#ok<ASGLU>

flag = 0;

% Cas d'impossibilit� de sortie dans le cas du EK60 de type Horizontal.
% Artifice en explorant la cha�ne Comments en attendant davoir un champ
% Orientation dans l'objet Image.
strLayerComments = this(1).Comments;
if contains(strLayerComments, 'Horizontal')
    str1 = 'La g�om�trie Horizontale du layer ne rend pas possible, pour le moment, l''extraction vers GLOBE';
    str2 = 'Horizontal geometry of the layer make impossible, for the moment, the GLOBE export';
    my_warndlg(Lang(str1,str2), 1);
    return
end

nx = length(subx);
ny = length(suby);
if nx > TailleMax
    strFR = 'L''image est trop haute pour �tre observ�e par GLOBE.';
    strUS = 'The number of columns of the image is too high for GLOBE.';
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'PbWCALongGenerate', 'TimeDelay', 10);
    return
end

% Le calcul de d�coupage ne se fait que le long du profil (pour l'instant).
TailleMax = min(TailleMax, nx*ny);
nBBlocks  = ceil(ny / TailleMax);
n2        = floor(ny / nBBlocks);

str1 = 'Export par blocks.';
str2 = 'Export by blocks.';
hw = create_waitbar(Lang(str1,str2), 'N', nBBlocks);
for k=1:nBBlocks
    my_waitbar(k, nBBlocks, hw)
    
    sub = (0:(n2+1)) + (k-1)*n2;
    sub(sub < 1) = [];
    sub(sub > ny) = [];
    NomFicXMLBlock = [nomDirXml '_' num2str(k, '%02d') '.xml'];
    flag = sonar_HACWriteWC_profil_Block(this, NomFicXMLBlock, subx, suby(sub), ...
        HauteurMinInMeters, SuppressionOverHeight, MeanTide, CLim, CLimNaN, ColormapIndex, nomFicHac);
    if ~flag
        break
    end
end
my_close(hw)

if ~Mute
    str1 = 'L''export vers GLOBE est termin�.';
    str2 = 'The export to GLOBE is over.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'Export3DVAlongNavigationIsOver');
end

flag = 1;


function flag = sonar_HACWriteWC_profil_Block(this, NomFicXML, subx, suby, ...
    HauteurMinInMeters, SuppressionOverHeight, MeanTide, CLim, CLimNaN, ColormapIndex, nomFicHac)

%% Traitements des diff�rentes fr�quences

[nomDirXml, nomFic] = fileparts(NomFicXML);
nomDirXml = fullfile(nomDirXml, nomFic);

% this = [this(1);  this(:)]; % TODO  : en attendant que le pb soit r�solu dans GLOBE
% profile on
for k=1:length(this)
    SliceName = this(k).Comments;
    
    if (length(subx) == this(k).nbColumns) && (length(suby) == this(k).nbRows)
        X = this(k);
    else
        X = extraction(this(k), 'subx', subx, 'suby', suby);
    end
    
    SurfaceSoundSpeed = get(X, 'SurfaceSoundSpeed');
    SampleFrequency   = get(X, 'SampleFrequency');
    deltay            = mean(SurfaceSoundSpeed ./ (2*SampleFrequency), 'omitnan');
    
    %% Gestion de la supression de la partie haute de l'image
    
    % subxLimites = {[1 100]; [800 890]};
    xLimites = {};
    subxLimites = {};
    if HauteurMinInMeters ~= 0
        HauteurMinInSamples = floor(HauteurMinInMeters / deltay);
        subxLimites{end+1}  = [1 HauteurMinInSamples]; %#ok<AGROW>
    end
    nbBande = length(subxLimites);
    
%     if ~SuppressionOverHeight && isempty(subxLimites) && isempty(xLimites)
% %         X = this(k);
%     else
    if SuppressionOverHeight || ~isempty(subxLimites) || ~isempty(xLimites)
        nbCol = X.nbColumns;
        H = get(X, 'SonarHeight');
        
        if all(H)
            subxLimites{nbBande+1}(:,1) = max(1,floor(abs(H(:)))); %#ok<AGROW>
            subxLimites{nbBande+1}(:,2) = nbCol; %#ok<AGROW>
            
            [flag, X] = masquage_abscisses(X, 'subxLimites', subxLimites);
            if ~flag
                return
            end
        else
            % TODO : Message d'erreur toutes les hauteurs � 0
        end
    end
    
    %% Seuillage �ventuel
    
    if any(~isinf(CLimNaN))
        I = get(X, 'Image');
        if ~isinf(CLimNaN(1))
            I(I < CLimNaN(1)) = NaN;
        end
        if ~isinf(CLimNaN(2))
            I(I > CLimNaN(2)) = NaN;
        end
        X = set_Image(X, I);
    end
    
    %% Recherche des NaN 
    
    subNaN = findNaN(X);
    
    %% Transformation de l'image en indexe
    
    X.ColormapIndex = ColormapIndex;
    [c, flag] = Intensity2Indexed(X, 'CLim', CLim);
    if ~flag
        return
    end
    map = X.Colormap;
    
    %% Cr�ation de l'image
    
    I = get(c, 'Image');
    I(I == 0) = 1;
    I(subNaN) = 0;
        
%     pppp = Lena;
%     I(1:size(pppp,2),1:size(pppp,1)) = pppp';
%     
%     pppp = imread('C:\Temp\this\022028.gif');
%     I(1:size(pppp,2),300+(1:size(pppp,1))) = pppp';
%     
%     pppp = imread('C:\Temp\this\024411.gif');
%     I(1:size(pppp,2),600+(1:size(pppp,1))) = pppp';
    
    
    %% Cr�ation si n�cessaire du r�pertoire d'archivage de l'image
    
    kRep = floor(k/100);
    nomDirPng = fullfile(nomDirXml, num2str(kRep, '%03d'));
    if ~exist(nomDirPng, 'dir')
        flag = mkdir(nomDirPng);
        if ~flag
            messageErreurFichier(nomDirPng, 'WriteFailure');
            return
        end
    end
    
    %% Ecriture de l'image PNG
    
    nbRows = c.nbRows;
    x = get(c, 'x');
    sumX = sum(I == 0);
    subX = find(sumX ~= nbRows);
    if isempty(subX)
        % TODO message d'erreur
        flag = 0;
        return
    end
    x = x(subX);
    nbColumns = length(x);
    
    nomFic1 = [num2str(k, '%05d') '.png'];
    nomFic2 = fullfile(nomDirPng, nomFic1);
    imwrite(I(:,subX)', map, nomFic2, 'Transparency', 0)

    %% Cr�ation du fichier XML-Bin
    
    Immersion         = get(c, 'Immersion');
    FishLatitude      = get(c, 'FishLatitude');
    FishLongitude     = get(c, 'FishLongitude');
    Heave             = get(c, 'SonarHeave');
    Tide              = get(c, 'Tide');
    
    %% TODO pour JMA : � adapter pour la g�om�trie Sample/Beam.
    
%     BathyCel          = get(c, 'SonarBathyCel_C');
%     SampleFrequency   = get(c, 'SampleFrequency');
    Heading           = get(c, 'SonarHeading');
%     Roll              = get(c, 'Roll');
    TxAcrossAngle     = get(c, 'TxAcrossAngle');
    TxAcrossAngle = -TxAcrossAngle;
%     SonarFrequency     = get(c, 'SonarFrequency');
    
%     Tide  = zeros(size(FishLongitude), 'single');
%     Heave = zeros(size(FishLongitude), 'single');

    MinHeight = -x(1)   * deltay;
    MaxHeight = -x(end) * deltay;
    MinHeight = ones(nbRows, 1, 'single') * MinHeight;
    MaxHeight = ones(nbRows, 1, 'single') * MaxHeight;
    %     if ~isempty(Immersion)
    %         MinHeight = MinHeight + Immersion;
    %         MaxHeight = MaxHeight + Immersion;
    %     end
    % if ~isempty(this.Sonar.Height)
    %     MinHeight = MinHeight + min(this.Sonar.Height(suby));
    %     MaxHeight = MaxHeight + min(this.Sonar.Height(suby));
    % end
    
    T = get(c, 'Time');
    Date = T.date;
    Hour = T.heure;
    
    FishLatitude  = replaceNaN(FishLatitude);
    FishLongitude = replaceNaN(FishLongitude);
    Heading       = replaceNaN(Heading);
    Date          = replaceNaN(Date);
    Hour          = replaceNaN(Hour);
    Heave         = replaceNaN(Heave);

%     if isempty(Tag)
%         DataSlices.name = [ImageName '_' nameLayer];
%     else
%         DataSlices.name = [ImageName '_' nameLayer '_' Tag];
%     end
    [~, DataSlices.name] = fileparts(nomDirXml);
    
    DataSlices.from = nomFicHac; %'Unkown';
    DataSlices.Date = Date';
    DataSlices.Hour = Hour';
    DataSlices.PingNumber = (1:nbRows)';
    if isempty(Tide)
        DataSlices.Tide = repmat(MeanTide, size(DataSlices.PingNumber));
    else
        DataSlices.Tide  = Tide;
    end
    
    DataSlices.Heave = Heave;
    
    DataSlices.SliceName{k,1} = SliceName;
    
% Comment� pour test
%     DataSlices.LatitudeNadir  = FishLatitude';
%     DataSlices.LongitudeNadir = FishLongitude';

%     SliceLatitude  = FishLatitude';
%     SliceLongitude = FishLongitude';
    % {
    
    % TODO
    % get(this, 'EchoSounderAcrossAngle')
    
    
    
    [x0, y0] = latlon2xy(this(1).Carto, FishLatitude, FishLongitude);
    c = cosd(-Heading);
    s = sind(-Heading);
    
    %     C = [0 Deltay] * [c s; -s c];
    %     [lat0 ,lon0] = xy2latlon(this.Carto, x0+C(:,1), y0+C(:,2));
    
    Z0 = zeros(size(TxAcrossAngle));
    AcrossDistTop = MinHeight .* sind(TxAcrossAngle);
    AlongDistTop  = Z0; % Vaut mieux laisser cela en place au cas o� !
    AcrossDistEnd = MaxHeight .* sind(TxAcrossAngle);
    AlongDistEnd  = Z0; % Vaut mieux laisser cela en place au cas o� !
    FishLatitudeBottom  = Z0;
    FishLongitudeBottom = Z0;
    FishLatitudeTop     = Z0;
    FishLongitudeTop    = Z0;
    DepthTop            = MinHeight .* cosd(TxAcrossAngle);
    DepthBottom         = MaxHeight .* cosd(TxAcrossAngle);
    if ~isempty(Immersion)
        DepthTop    = DepthTop    + Immersion;
        DepthBottom = DepthBottom + Immersion;
    end
    for k2=1:length(c)
        C = [AcrossDistEnd(k2) AlongDistEnd(k2)] * [c(k2) s(k2); -s(k2) c(k2)];
        [FishLatitudeBottom(k2), FishLongitudeBottom(k2)] = xy2latlon(this(1).Carto, x0(k2)+C(:,1), y0(k2)+C(:,2));
        C = [AcrossDistTop(k2) AlongDistTop(k2)] * [c(k2) s(k2); -s(k2) c(k2)];
        [FishLatitudeTop(k2), FishLongitudeTop(k2)] = xy2latlon(this(1).Carto, x0(k2)+C(:,1), y0(k2)+C(:,2));
    end
        
%     across = (k-2) * 0;
%     along = 0;
%     [SliceLatitude, SliceLongitude] = latlonAcrossAlong2latlon(Carto, FishLatitude', FishLongitude', Heading, across, along);
    % }

    DataSlices.LatitudeTop(k,:)  = FishLatitudeTop';
    DataSlices.LongitudeTop(k,:) = FishLongitudeTop';
    
    % Donn�es optionnelles
    % TODO , corriger les valeurs en fonction du roulis et du tangage
    DataSlices.LatitudeBottom(k,:)  = FishLatitudeBottom';
    DataSlices.LongitudeBottom(k,:) = FishLongitudeBottom';
    
%     HeaveInSamples = Heave / deltay; % TODO POURQUOI 0.25 ? Parce que c'est la valeur qui me donnait le meilleur rendu ! TODO TODO TODO !
%     DataSlices.DepthTop     = MinHeight - 0.25*HeaveInSamples; % TODO  : en attendant que ce soit dfait dans GLOBE
%     DataSlices.DepthBottom  = MaxHeight - 0.25*HeaveInSamples;
    DataSlices.DepthTop(k,:)    = DepthTop;
    DataSlices.DepthBottom(k,:) = DepthBottom;
    
    if any(isnan(DataSlices.PingNumber(:)))
        Artung(nomFicHac, 'PingNumber')
    end
    if any(isnan(DataSlices.Tide(:)))
        Artung(nomFicHac, 'Tide')
    end
    if any(isnan(DataSlices.Heave(:)))
        Artung(nomFicHac, 'Heave')
    end
    if any(isnan(DataSlices.Date(:)))
        Artung(nomFicHac, 'Date')
    end
    if any(isnan(DataSlices.Hour(:)))
        Artung(nomFicHac, 'Hour')
    end
    if any(isnan(DataSlices.LatitudeTop(:)))
        Artung(nomFicHac, 'LatitudeTop')
    end
    if any(isnan(DataSlices.LongitudeTop(:)))
        Artung(nomFicHac, 'LongitudeTop')
    end
    if any(isnan(DataSlices.LatitudeBottom(:)))
        Artung(nomFicHac, 'LatitudeBottom')
    end
    if any(isnan(DataSlices.LongitudeBottom(:)))
        Artung(nomFicHac, 'LongitudeBottom')
    end
    if any(isnan(DataSlices.DepthTop(:)))
        Artung(nomFicHac, 'DepthTop')
    end
    if any(isnan(DataSlices.DepthBottom(:)))
        Artung(nomFicHac, 'DepthBottom')
    end
end
% profile report

flag = write_XML_ImagesAlongNavigation(NomFicXML, DataSlices, nbRows, nbColumns);

% FigUtils.createSScFigure; PlotUtils.createSScPlot(DataSlices.LongitudeTop, DataSlices.LatitudeTop, '+k'); grid; hold on; PlotUtils.createSScPlot(DataSlices.LongitudeBottom, DataSlices.LatitudeBottom, 'o')
% FigUtils.createSScFigure; PlotUtils.createSScPlot(DataSlices.LongitudeBottom, DataSlices.LatitudeBottom, 'o'); grid;


function Artung(nomFicHac, nomVar)
str1 = sprintf('La donn�e "%s" du fichier "%s" a des NaN.', nomVar, nomFicHac);
str2 = sprintf('Data named "%s" of "%s" has some NaN.', nomVar, nomFicHac);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'sonar_HACWriteWC_profilValNaN');


function Val = replaceNaN(Val)
if any(isnan(Val))
    subNaN = isnan(Val);
    subNonNaN = find(~subNaN);
    subNaN = find(subNaN);
    Val(subNaN) = interp1(subNonNaN, Val(subNonNaN), subNaN, 'linear', 'extrap');
end
