% Calcul de la compensation de Lurton
%
% Syntax
%   [flag, b] = sonar_Image_Lurton(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx        : subsampling in X
%   suby        : subsampling in Y
%   identCourbe : Numero du modele a utiliser
%
% Output Arguments
%   b    : Instance de cl_image contenant l'image filtree
%   flag : 1=OK, 0=Erreur
%
% Examples
%   b = sonar_Image_Lurton(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_Image_Lurton(this, Incidence, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, this(i)] = unitaire_Image_Lurton(this(i), Incidence, varargin{:});
    if ~flag
        return
    end
end


function [flag, this] = unitaire_Image_Lurton(this, Incidence, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, identCourbe] = getPropertyValue(varargin, 'identCourbe', []); %#ok<ASGLU>

if isempty(identCourbe)
    identCourbe = 1;
end

% identEmissionAngle     = cl_image.indDataType('TxAngle');
% identIncidenceAngle    = cl_image.indDataType('IncidenceAngle');
% identAveragedPtsNb     = cl_image.indDataType('AveragedPtsNb');
% identRxBeamAngle       = cl_image.indDataType('RxBeamAngle');
identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');

bilan = this.CourbesStatistiques(identCourbe).bilan;
if isfield(bilan{1}, 'model')
    model = bilan{1}.model;
    nomModel = model.Name;
    
    if strcmp(nomModel, 'BSLurton')
        Params = model.getParamsValue;
        
        % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
        % flipud (voir cl_image/plus
        
        [XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Incidence);
        if isempty(subx) || isempty(suby)
            str1 = 'Pas d''intersection commune entre les images.';
            str2 = 'No intersection between the 2 images.';
            my_warndlg(Lang(str1,str2), 1);
            this = [];
            flag = 0;
            return
        end
        
        A = Incidence;
        if A.DataType == identBeamPointingAngle
            if ~isempty(A.Sonar.Roll)
                A = A - A.Sonar.Roll(:,1); % Rajout� par JMA le 15/01/2013
            end
        end
        this = replace_Image(this, compute(model, 'x', A.Image(subyb, subxb)) - Params(3));
    else
        strWarn = sprintf('Seul un model BSLurton peut servir a realiser une belle image. Or votre model est : %s', nomModel);
        my_warndlg(strWarn, 1);
    end
else
    my_warndlg('No model available', 1);
end

this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'dB';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Reflectivity');
this = update_Name(this, 'Append', 'Niveau Lurton "Belle Image"');

%% Par ici la sortie

flag = 1;
