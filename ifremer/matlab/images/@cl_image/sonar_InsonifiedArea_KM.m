function [flag, this, Mask] = sonar_InsonifiedArea_KM(this, range, BeamPointingAngle, varargin)

[varargin, ComputeMask] = getPropertyValue(varargin, 'ComputeMask', 0);

N = length(this);
str1 = 'Calcul des images "KongsbergInsonifiedArea"';
str2 = 'Computing images of "KongsbergInsonifiedArea"';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, that] = sonar_InsonifiedArea_KM_Unitaire(this(k), range(k), BeamPointingAngle(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that(1);
    Mask(k) = that(2); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

if ComputeMask
    this = [this; Mask];
    this = this(:);
end


function [flag, this] = sonar_InsonifiedArea_KM_Unitaire(this, range, BeamPointingAngle, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, TxBeamIndex] = getPropertyValue(varargin, 'TxBeamIndex', []); %#ok<ASGLU>

identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
identTwoWayTravelTimeInSamples = cl_image.indDataType('TwoWayTravelTimeInSamples');
identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
identRayPathLength             = cl_image.indDataType('RayPathLength'); % One way length in meters

if isempty(this.Sonar.TVGN)
    str1 = 'Cette op�ration ne peut �tre r�alis�e sans "TVGN", partez de l''image de r�flectivit� pour cela.';
    str2 = 'This processing cannot be done without "TVGN", start from a "Reflectivity" layer.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

ns2 = floor(this.nbColumns / 2);
if mod(this.nbColumns,2) == 0
    subBab = [true(1,ns2)  false(1,ns2)];
    subTri = [false(1,ns2) true(1,ns2)];
else
    subBab = [true(1,ns2+1)  false(1,ns2)];
    subTri = [false(1,ns2+1) true(1,ns2)];
end
subBab = subBab(subx);
subTri = subTri(subx);

rn = this.Sonar.TVGN(suby,:);

%{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rajout JMA le 06/09/2018 pour SAT EM304
TVGNRecomputed = findYSampleListByTag(this.SignalsVert, 'TVGNRecomputed');
if isempty(TVGNRecomputed)
    rn = this.Sonar.TVGN(suby,:);
else
    rn = TVGNRecomputed.data(suby,:);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}


r = range.Image(suby,subx);
% X = this.Image(suby,subx);
X = NaN(length(suby), length(subx), 'single');
M = zeros(length(suby), length(subx), 'uint8');

SampleFrequency = this.Sonar.SampleFrequency(suby,:);
SoundSpeed_mps  = this.Sonar.SurfaceSoundSpeed(suby,:);
if isempty(this.Sonar.Interlacing)
    Interlacing = [];
else
    Interlacing = this.Sonar.Interlacing(suby,:);
end
if all(isnan(SoundSpeed_mps)) || all(SoundSpeed_mps == 0)
    SoundSpeed_mps(:) = mean(this.Sonar.BathyCel.C(1:end-1), 'omitnan');% TODO : faire mieux que �a
end

SonarName = get(this.Sonar.Desciption, 'SonarName');

RxBeamWidthAlongTrack = get(this.Sonar.Desciption, 'RxBeamWidthAlongTrack');
if isempty(RxBeamWidthAlongTrack) || (RxBeamWidthAlongTrack == 0) % Ceinture et bretelles
    RxBeamWidthAlongTrack = 90;
end

%{
TxBeamWidthAcrossTrack = get(this.Sonar.Desciption, 'TxBeamWidthAcrossTrack');
if isempty(TxBeamWidthAcrossTrack) || (TxBeamWidthAcrossTrack == 0) % Ceinture et bretelles
TxBeamWidthAcrossTrack = 180;
end
%}

%% Cr�ation du layer PulseLengthImage

PulseLength_EffectiveSignal = this.SignalsVert.getSignalByTag('PulseLength_Effective');
[flag, PLEffectiveImage] = create_LayerFromSignalVert(this, PulseLength_EffectiveSignal, 'Index', TxBeamIndex);
if ~flag
    return
end
SignalWaveformIdentifierSignal = this.SignalsVert.getSignalByTag('SignalWaveformIdentifier');
[flag, SignalWaveformIdentifierImage] = create_LayerFromSignalVert(this, SignalWaveformIdentifierSignal, 'Index', TxBeamIndex);
if ~flag
    return
end
subCW = (SignalWaveformIdentifierImage.Image(:,:) == 0);

pulseLength_UsedByKMinIASignal = this.SignalsVert.getSignalByTag('PulseLength_UsedByKMinIA');
[flag, PLImage] = create_LayerFromSignalVert(this, pulseLength_UsedByKMinIASignal, 'Index', TxBeamIndex);
if ~flag
    return
end
Image = PLImage.Image(:,:);
Image(~subCW) = PLEffectiveImage.Image(~subCW);
PLImage = inherit(PLImage, Image);

%% Lecture de certains signaux

rxAcrossBeamWidthSignal = this.SignalsVert.getSignalByTag('RxAcrossBeamWidth');
[status, RxAcrossBeamWidth_deg, Unit] = rxAcrossBeamWidthSignal.getValueMatrix(); %#ok<ASGLU>
if status
    ValRxBeamWidth_deg = RxAcrossBeamWidth_deg;
else
    ValRxBeamWidth_deg = [];
end

txAlongBeamWidthSignal = this.SignalsVert.getSignalByTag('TxAlongBeamWidth');
[status, TxAlongBeamWidth_deg, Unit] = txAlongBeamWidthSignal.getValueMatrix(); %#ok<ASGLU>
if status
    ValTxBeamWidth_deg = AntenneOuvEmiRec(TxAlongBeamWidth_deg, RxBeamWidthAlongTrack);
else % TODO : Th�oriquement plus utile
    ValTxBeamWidth_deg = [];
end

%% Algo

TxBeamIndexImage       = TxBeamIndex.Image(:,:);
BeamPointingAngleImage = BeamPointingAngle.Image(:,:);
this = update_SonarDescription_Ping(this, []);
for k=1:size(r,1)
    Sector = TxBeamIndexImage(suby(k), subx);
    
    switch SonarName % TODO : Th�oriquement plus utile
        case {'EM2040'; 'EM2040S'; 'EM2040D'}
%             SonarMode_1 = get(this, 'Mode1');
%             SonarMode_2 = get(this, 'Mode2');
            SonarMode_2 = get_mode2(this);
%             SonarMode_3 = get(this, 'Mode3');
            SonarMode_3 = get_mode3(this);
%             SonarMode_1 = SonarMode_1(subyRange(k));
            SonarMode_2 = SonarMode_2(suby(k));
            SonarMode_3 = SonarMode_3(suby(k));
            
            SonarMode_1 = this.Sonar.NbSwaths(suby(k));
%             SonarMode_2 = this.Sonar.EM2040Mode2(suby(k));
%             SonarMode_3 = this.Sonar.EM2040Mode3(suby(k));
            this = update_SonarDescription_PingEM2040(this, k == 1, SonarMode_1,  SonarMode_2, SonarMode_3);
        otherwise
            if isempty(Interlacing)
                ScanningInfo = [];
            else
                ScanningInfo = (Interlacing(k) ~= 1); % TODO : faux si premier secteur
            end
            this = update_SonarDescription_Ping(this, suby(k), 'nbSectors', max(Sector), 'ScanningInfo', ScanningInfo);
    end
    
    %% get RxAcrossBeamWidth_deg
    
    if isempty(ValRxBeamWidth_deg)
        RxBeamWidth_deg = get(this.Sonar.Desciption, 'BeamForm.Rx.TransWth');
    else
        RxBeamWidth_deg = ValRxBeamWidth_deg(suby(k));
    end
    
    %% get TxBeamWidthDeg
    
    if isempty(ValTxBeamWidth_deg)
        TxBeamWidth_deg = get(this.Sonar.Desciption, 'LongBeamWth');
    else
        TxBeamWidth_deg = ValTxBeamWidth_deg(suby(k));
    end
    
    %% Algo
    
    BeamPointingAngle_deg = BeamPointingAngleImage(suby(k), subx);
    RxBeamWidth_deg = getRxBeamWidthDepointe(RxBeamWidth_deg, BeamPointingAngle_deg, this.Sonar.Ship.Arrays.Receive.Roll, subBab, subTri);
    
    TxBeamWidth_rad = TxBeamWidth_deg .* (pi/180);
    RxBeamWidth_rad = RxBeamWidth_deg .* (pi/180);
    
    Tp_s = PLImage.Image(suby(k),:) * 1e-3;
    
    r_m = NaN(1, size(r, 2));
    switch range.DataType
        case identTwoWayTravelTimeInSeconds
            r_m = r(k,:) * (SoundSpeed_mps(k,1) / 2);
        case identTwoWayTravelTimeInSamples
            r_m = r(k,:) * (SoundSpeed_mps(k,1) / (2*SampleFrequency(k)));
        case identRayPathSampleNb % TODO : supprimer cet identifiant dans cl_image dans la R2013a
            r_m = r(k,:) * (SoundSpeed_mps(k,1) / (2*SampleFrequency(k)));
        case identRayPathLength % One way length in meters
        otherwise
            str1 = sprintf('Le layer de type "%s" n''est pas encore branch� dans "sonar_InsonifiedArea_KM", contactez JMA', cl_image.strDataType{range.DataType});
            str2 = sprintf('The layer of type "%s" is not plugged yet in "sonar_InsonifiedArea_KM", please email JMA', cl_image.strDataType{range.DataType});
            my_warndlg(Lang(str1,str2), 1);
    end
    rn_metres = rn(k) * (SoundSpeed_mps(k,1) / (2*SampleFrequency(k)));
    
    sub = (r_m < rn_metres);
    r_m(sub)= rn_metres;
    
%     subNonNaN = find(~isnan(RxBeamWidth_rad));
    subNonNaN = ~isnan(RxBeamWidth_rad);
    if ~isempty(subNonNaN)
        
        %{
        % Avant
        rLim = sqrt(rn_metres.^2 + (SoundSpeed_mps(k) * Tp_s(subNonNaN) / (2*RxBeamWidth_rad(subNonNaN))).^2);
        sub_Spec = (r_m < rLim);
        %     FigUtils.createSScFigure; PlotUtils.createSScPlot(r_m, 'r'); grid on; hold on; PlotUtils.createSScPlot([1 length(r_m)], [rn_metres rn_metres], 'k'); PlotUtils.createSScPlot(rLim, 'b')
        
        %     sub_Spec = true(1, length(r_m));
        
        sub0 = find(~sub_Spec);
        if ~isempty(sub0)
            A0 = (SoundSpeed_mps(k) * Tp_s(sub0) * TxBeamWidth_rad .* r_m(sub0)) ./ (2 * sqrt(1 - (rn_metres^2 ./ (r_m(sub0).^2))));
            %     FigUtils.createSScFigure; PlotUtils.createSScPlot(sub0, reflec_Enr2dB(A0)); grid on
            X(k,sub0) = reflec_Enr2dB(A0);
            M(k,sub0) = 2;
        end
        
        subN = find(sub_Spec);
        if ~isempty(subN)
            AN = (RxBeamWidth_rad(subN) .* TxBeamWidth_rad) .* (r_m(subN).^2);
            %     FigUtils.createSScFigure; PlotUtils.createSScPlot(subN, reflec_Enr2dB(AN)); grid on
            X(k,subN) = reflec_Enr2dB(AN);
            M(k,subN) = 1;
        end
        % figure(888557); plot(sub0, reflec_Enr2dB(A0), '-*b'); grid on; hold on; plot(subN, reflec_Enr2dB(AN), '-*r'); hold off; legend({'A0'; 'AN'}); drawnow
        %}

        % Apr�s
            A0 = (SoundSpeed_mps(k) * Tp_s * TxBeamWidth_rad .* r_m) ./ (2 * sqrt(1 - (rn_metres^2 ./ (r_m.^2))));
            %     FigUtils.createSScFigure; PlotUtils.createSScPlot(reflec_Enr2dB(A0)); grid on
%             X(k,sub0) = reflec_Enr2dB(A0);
%             M(k,sub0) = 2;
        
            AN = (RxBeamWidth_rad .* TxBeamWidth_rad) .* (r_m.^2);
            %     FigUtils.createSScFigure; PlotUtils.createSScPlot(reflec_Enr2dB(AN)); grid on
%             X(k,subN) = reflec_Enr2dB(AN);
            [X(k,:), M(k,:)] = min([reflec_Enr2dB(A0); reflec_Enr2dB(AN)]);
%             hold on; PlotUtils.createSScPlot(X(k,:), 'k')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             [X(k,:), M(k,:)] = min([reflec_Amp2dB(A0); reflec_Amp2dB(AN)]); % Test JMA le 12/02/2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
         % figure(883557); plot(reflec_Enr2dB(A0), '-*b'); grid on; hold on; plot(reflec_Enr2dB(AN), '-*r'); hold off; legend({'A0'; 'AN'}); drawnow
        % figure(888557); plot(sub0, reflec_Enr2dB(A0), '-*b'); grid on; hold on; plot(subN, reflec_Enr2dB(AN), '-*r'); hold off; legend({'A0'; 'AN'}); drawnow
    end
end

this = replace_Image(this, X);
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'dB';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('InsonifiedAreadB');
this = update_Name(this, 'Append', 'Factory');

%% Image du mask : 1=AN, 2=A0

this(2) = replace_Image(this, M);
this(2).ValNaN = 0;
this(2) = compute_stats(this(2));
this(2).CLim = [0 3];
this(2).Unit = ' ';
this(2).DataType = cl_image.indDataType('Mask');
this = update_Name(this);

flag = 1;
