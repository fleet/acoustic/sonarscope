% Calcul de la compensation de Lambert
%
% Syntax
%   [flag, b] = sonar_Lambert_KM(a, ...) 
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance de cl_image contenant l'image filtree
%
% Examples 
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', [9 1 4], 'Carto', Carto);
%   
%   Reflectivite = get_Image(b, 1);
%   imagesc(Reflectivite)
%   Bathy = get_Image(b, 2);
%   imagesc(Bathy)
%   Emission = get_Image(b, 3);
%   imagesc(Emission)
%   
%   BelleImage = sonar_Lambert_KM(Bathy, 1, [], 3, Bathy, Emission);
%   imagesc(BelleImage)

%   b = sonar_Lambert_KM(BelleImage)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_Lambert_KM(this, TwoWayTravelTimeInSeconds, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, that] = unitaire_sonar_Lambert_KM(this(k), TwoWayTravelTimeInSeconds, varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end


function [flag, that]  = unitaire_sonar_Lambert_KM(this, TwoWayTravelTimeInSeconds, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>
flag = 0;

logFileId = getLogFileId;

if isempty(suby)
    logFileId.warn('unitaire_sonar_Lambert_KM', 'Exit because "suby" is empty');
    retrun
end

%% Calcul de la correction de Lambert

if isempty(TwoWayTravelTimeInSeconds)
    logFileId.warn('unitaire_sonar_Lambert_KM', 'Exit because "TwoWayTravelTimeInSeconds" is empty');
    return
end

if TwoWayTravelTimeInSeconds.DataType == cl_image.indDataType('TwoWayTravelTimeInSeconds')
    TypeLayerRange = 1;
elseif TwoWayTravelTimeInSeconds.DataType == cl_image.indDataType('RayPathSampleNb')
    TypeLayerRange = 2;
elseif TwoWayTravelTimeInSeconds.DataType == cl_image.indDataType('TwoWayTravelTimeInSamples')
    TypeLayerRange = 2;
else
    that = [];
    logFileId.warn('unitaire_sonar_Lambert_KM', 'Exit because no range layers could be found');
    return
end

% TVGN = this.Sonar.TVGN(:,:);

% {
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rajout JMA le 06/09/2018 pour SAT EM304

TVGNRecomputedSignal = this.SignalsVert.getSignalByTag('TVGNRecomputed');

if isempty(TVGNRecomputedSignal) || all(isnan(TVGNRecomputedSignal.ySample(1).data))
    TVGN = this.Sonar.TVGN(:,:);
else
%     TVGN = TVGNRecomputedSignal.ySample.data;
    for k=1:length(TVGNRecomputedSignal.ySample)
        TVGN(:,k) = TVGNRecomputedSignal.ySample(k).data; %#ok<AGROW>
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% }

I = NaN([length(suby) TwoWayTravelTimeInSeconds.nbColumns], 'single');
TwoWayTravelTimeInSeconds.Image = TwoWayTravelTimeInSeconds.Image(:,:);
for k=1:length(suby)
    fe = TwoWayTravelTimeInSeconds.Sonar.SampleFrequency(suby(k),:);
    rn = TVGN(suby(k), :);
    
    if length(rn) == 2
        m = TwoWayTravelTimeInSeconds.nbColumns / 2;
        subBab = 1:m;
        subTri = (m+1):TwoWayTravelTimeInSeconds.nbColumns;
    end
    if TypeLayerRange == 1 % TwoWayTravelTimeInSeconds
        if length(fe) == 1
            r = TwoWayTravelTimeInSeconds.Image(suby(k),:) * fe;
        else
            r = TwoWayTravelTimeInSeconds.Image(suby(k),:) .* [repmat(fe(1), 1, m) repmat(fe(2), 1, m)];
        end
    else % RayPathSampleNb
        r = TwoWayTravelTimeInSeconds.Image(suby(k),:);
    end
    
    if length(rn) == 1
        r(r < rn) = rn;
        sub = (r >= rn);
        I(k,sub) = reflec_Amp2dB(rn ./ r(sub));
%         I(k,sub) = reflec_Amp2dB(rn ./ r(:));
    else
%         rnMinBab = min(r(subBab));
        sub = (r(subBab) < rn(1));
        r(subBab(sub)) = rn(1);
        I(k,subBab) = reflec_Amp2dB(rn(1) ./ r(subBab));
%         I(k,subBab) = reflec_Amp2dB(rnMinBab ./ r(subBab));
        
%         rnMinTri = min(r(subTri));
        sub = (r(subTri) < rn(2));
        r(subTri(sub)) = rn(2);
        I(k,subTri) = reflec_Amp2dB(rn(2) ./ r(subTri));
%         I(k,subTri) = reflec_Amp2dB(rnMinTri ./ r(subTri));
    end
end

%% Output image

DT = cl_image.indDataType('Reflectivity');
that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'LambertKM', 'Unit', 'dB', ...
    'ValNaN', NaN, 'ColormapIndex', 3, 'DataType', DT);

flag = 1;
