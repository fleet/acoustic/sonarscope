function [flag, this] = sonar_Lambert_SSc(this, Angles, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, that] = unitaire_sonar_Lambert_KM(this(k), Angles, varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end


function [flag, that]  = unitaire_sonar_Lambert_KM(this, Angles, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>
flag = 0;

% TODO : attention : g�rer la mise en correspondance des suby et suby

%% Calcul de la correction de Lambert

if isempty(Angles)
    return
end

I = corLambert(Angles.Image(suby,subx));

%% Output image

DT = cl_image.indDataType('Reflectivity');
that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'LambertSSc', 'Unit', 'dB', ...
    'ValNaN', NaN, 'ColormapIndex', 3, 'DataType', DT);

flag = 1;
