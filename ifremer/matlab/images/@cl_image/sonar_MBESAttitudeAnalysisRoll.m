function flag = sonar_MBESAttitudeAnalysisRoll(Bathy, AcrossDist, VarName, varargin)

[subx, suby, varargin] = getSubxSuby(Bathy, varargin); %#ok<ASGLU>

%% Controls de signatures

ident = cl_image.indDataType('Bathymetry');
flag = testSignature(Bathy, 'DataType', ident, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

ident = cl_image.indDataType('AcrossDist');
flag = testSignature(AcrossDist, 'DataType', ident, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Calcul de l'intersection des deux images

[~, ~, subx, suby, subxb, subyb] = intersectionImages(Bathy, subx, suby, AcrossDist);
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Extraction de la partie commune entre les deux images

Bathy = extraction_subxsuby(Bathy, 'subx', subx, 'suby', suby);
AcrossDist = extraction_subxsuby(AcrossDist, 'subx', subxb, 'suby', subyb);
Time = get(Bathy, 'Datetime');

%% Calcul de la pente transversale

[flag, AcrossSlope] = compute_PingBeamAcrossSlope(Bathy, AcrossDist);
if ~flag
    return
end
Bathy.CLim = '1%';

% imagesc(Z); imagesc(AcrossDist); imagesc(AcrossSlope);
% SonarScope([Z AcrossDist AcrossSlope])

VarValue  = get(Bathy, VarName);
ImageName = Bathy.Name;
nomImage = strtok(ImageName, ' ');
Z = get_Image(Bathy);
I = get_Image(AcrossSlope);
MeanValue = mean(I, 2, 'omitnan');
x = 1:length(MeanValue);
y = 1:size(I,1);
CLim2 = Bathy.CLim;
imageData2 = ClImageData('cData', Z', 'xData', x, 'yData', y, 'colormapData', jet(256), 'clim', CLim2, 'name', ImageName);

ySampleImage = YSample('name', nomImage,    'data', ones(1, length(x), 'single'));
ySample      = YSample('name', 'MeanValue', 'data', MeanValue',         'single', 'marker', '.');
ySample(2)   = YSample('name', VarName,      'data', VarValue',              'single', 'marker', '.');

xSampleRawOne = XSample( 'name', 'Pings', 'data', x);
signalOne     = ClSignal('Name', 'Bathy',               'xSample', xSampleRawOne, 'ySample', ySampleImage);
signalOne(2)  = ClSignal('Name', 'Mean value per ping', 'xSample', xSampleRawOne, 'ySample', ySample(1));
signalOne(3)  = ClSignal('Name', VarName,               'xSample', xSampleRawOne, 'ySample', ySample(2));

Title = 'Bathy data cleaning (define ping flags)';
s = SignalDialog(signalOne, 'Title', Title, 'cleanInterpolation', 0, 'imageDataList', imageData2);%, 'waitAnswer', false);
s.openDialog();

if s.okPressedOut
    MeanValue = ySample(1).data;

    FigPosition = [];
    if isempty(FigPosition) || ~ishandle(FigPosition)
        FigPosition = FigUtils.createSScFigure;
    else
        FigUtils.createSScFigure(FigPosition);
    end
    FigPosition.WindowState = 'maximized';
%     h = findobj(FigPosition, 'Type', 'Line');
%     delete(h);
    hc(1) = subplot(5,1,1); imagesc(Z'); colormap(jet(256)); colorbar; title('Bathy')%, 'CLim', '1%'
    hc(2) = subplot(5,1,2); PlotUtils.createSScPlot(mean(Z,2, 'omitnan')); c = colorbar; c.Visible = 'Off'; axis tight; title('Mean bathy per ping'); grid on;
    hc(3) = subplot(5,1,3); PlotUtils.createSScPlot(MeanValue);    c = colorbar; c.Visible = 'Off'; axis tight; title('Mean slope per ping'); grid on;
    hc(4) = subplot(5,1,4); PlotUtils.createSScPlot(VarValue);     c = colorbar; c.Visible = 'Off'; axis tight; title(VarName); grid on;
    hc(5) = subplot(5,1,5); PlotUtils.createSScPlot(gradient(VarValue)); c = colorbar; c.Visible = 'Off'; axis tight; title(['Gradient of ' VarName]); grid on;
    linkaxes(hc, 'x');
    
    str1 = 'Voulez-vous lancer l''outil d''analyse de l""intercorrelation ?';
    str2 = 'Do you want to launch the crosscorrelation analysis tool ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 1
        XData1 = x;
        YData1 = MeanValue;
        YData2 = VarValue;
        
        M1 = mean(YData1);
        M2 = mean(YData2);
        Y1 = YData1 - M1;
        Y2 = YData2 - M2;
        
        %                 covMx = xcorr(Y1, Y2, 'coeff');
        resol = mean(diff(XData1));
        [covMx, x, Delay, yModele, strModel, XDataFlags] = my_xcorr(Y1, Y2, 'cl_time', 0, 'resol', resol, 'UnitX', 'pings', 'UnitY', 'deg');
        
        % TODO : voir comment transf�rer les infos du model
        
        FigUtils.createSScFigure;
        PlotUtils.createSScPlot(x, covMx); grid on; box on; title('Cross corr');
        
        n = floor(length(x) / 2);
        hold on; hm = PlotUtils.createSScPlot((-n:n), yModele, 'r'); title('Model');
        % title(['Cross corr - ' nomSignal], 'Interpreter', 'none'); % title(strModel, 'Interpreter', 'latex')
        set(hm, 'Tag', 'Model')
        hold on; PlotUtils.createSScPlot([Delay Delay], [min(covMx) max(covMx)], 'k'); %title(['Shift : ' num2str(Delay)])
        Titre = sprintf('%s %s', VarName, strModel);
        title(Titre, 'Interpreter', 'latex')
        %     h = text(-length(x)/5, 0.6, strModel);
        %     set(h, 'Interpreter', 'latex', 'fontsize', 12)
        %     h = text(-length(x)/5, 0.1, ['Shift : ' num2str(Delay)]);
        %     set(h, 'Interpreter', 'latex', 'fontsize', 12)
        
        Deltat = Delay * range(Time) / (length(Time) - 1);
        str1 = sprintf('Le d�calage trouv� est de %5.3f secondes', seconds(Deltat));
        str2 = sprintf('Offset is %5.3f seconds', seconds(Deltat));
        my_warndlg(Lang(str1,str2), 1);
        
        sub = find(XDataFlags == 1);
        if length(sub) >= 2
            set(gca, 'XLim', x(sub([1 end])));
        end
    end
end
