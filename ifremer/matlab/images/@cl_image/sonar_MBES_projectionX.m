% Calcul du layer MBES de geom�trie SonarD en SonarX
%
% Syntax
%   b = sonar_MBES_projectionX(a, across, ...)
%
% Input Arguments
%   a : Instance de cl_image
%   across : Instance de cl_image contenant la distance lat�rale (m)
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%   Others  : Autres layers � projeter en m�me temps
%
% Output Arguments
%   b : Instance(s) de cl_image contenant l'image projetee et le nombre de
%       points moyenn�s
%
% Examples
%   b = sonar_MBES_projectionX(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonar_MBES_projectionX(this, ReceptionBeam, across, Range, varargin)

[varargin, resolutionX] = getPropertyValue(varargin, 'resolutionX', []);
if this.GeometryType == cl_image.indGeometryType('PingSamples')
    SonarDescription = get(this, 'SonarDescription');
    % SonarIdent = get(SonarDescription, 'Sonar.Ident')
    % Sonar.Family
    SonarName = get(SonarDescription, 'SonarName');
    if strcmp(SonarName, 'GeoSwath+') || strcmp(SonarName, 'GeoSwath')
        LayersSup = varargin{1};
        varargin(1) = [];
        [flag, a] = sonar_bathyFais2SonarX(this, across, resolutionX, varargin{:});
        for i=1:length(LayersSup)
            [flag, b] = sonar_bathyFais2SonarX(LayersSup(i), across, resolutionX, varargin{:});
            a(end+1) = b(1); %#ok
        end
    else
        [flag, a] = sonar_MBES_projectionX_fromSonarD(this, ReceptionBeam, across, Range, resolutionX, varargin{:});
    end
    
elseif this.GeometryType == cl_image.indGeometryType('PingAcrossSample')
    [varargin, subx]         = getPropertyValue(varargin, 'subx',         []);
    [varargin, suby]         = getPropertyValue(varargin, 'suby',         []);
    [varargin, flagFillNaN]  = getPropertyValue(varargin, 'FillNaN',      1);
    [varargin, MasquageDual] = getPropertyValue(varargin, 'MasquageDual', 1);
    
    [flag, a] = sonar_PingAcrossSample2PingAcrossDist([this;  varargin{:}], resolutionX, 'subx', subx, 'suby', suby, 'flagFillNaN', flagFillNaN);

else
    LayersSup = varargin{1};
    varargin(1) = [];
    [flag, a] = sonar_bathyFais2SonarX(this, across, resolutionX, varargin{:});
    for i=1:length(LayersSup)
        [flag, b] = sonar_bathyFais2SonarX(LayersSup(i), across, resolutionX, varargin{:});
        a(end+1) = b(1); %#ok
    end
end

function [flag, varargout] = sonar_MBES_projectionX_fromSonarD(this, ReceptionBeam, across, Range, resolutionX, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, createNbp]    = getPropertyValue(varargin, 'createNbp', []);
[varargin, MasquageDual] = getPropertyValue(varargin, 'MasquageDual', 0); %#ok<ASGLU>

suby = sort(suby);

if isempty(resolutionX)
    resolutionX = this.Sonar.ResolutionX;
    if isempty(resolutionX)
        resolutionX = this.Sonar.ResolutionD(1);
    end
end

if isempty(createNbp)
    createNbp = 1;
end

%% Controles

% Test si SonarD
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'));
if ~flag
    return
end

%% Nombre de points deja moyennes sur l'image de depart

increment = this.Sonar.ResolutionD / this.Sonar.RawDataResol;

%% Recherche de la zone commune

suby = suby(~isnan(this.Sonar.PingCounter(suby)));
PC1 = this.Sonar.PingCounter(suby);
PC2 = ReceptionBeam.Sonar.PingCounter;
PC3 = across.Sonar.PingCounter;
PC4 = Range.Sonar.PingCounter;

[~, ~, ilig_Image, ilig_ReceptionBeam, ilig_Across, ilig_Range] = lin_intersect(PC1, PC2, PC3, PC4);
clear PC1 PC2 PC3 PC4
ilig_Image = suby(ilig_Image);

%% Importation du layer range en geometrie SonarD

if MasquageDual
    [flag, Masque] = sonar_masqueRecoveringBeams(across); %#ok<ASGLU>
    across = masquage(across, 'LayerMask', Masque, 'valMask', 1);
    Range  = masquage(Range, 'LayerMask', Masque, 'valMask', 1);
end

[flag, acrossD] = sonar_bathyFais2SonarD(ReceptionBeam, Range, across, 'x', this.x(subx), 'suby', ilig_ReceptionBeam);
if ~flag
    return
end

% MasquageDual = 1;
% flagDual = 1;
% if MasquageDual && flagDual
%     [flag, Masque] = sonar_masqueRecoveringBeams(acrossD);
%     acrossD = masquage(acrossD, 'LayerMask', Masque, 'valMask', 1);
% end

XMin = single(acrossD.StatValues.Min);
XMax = single(acrossD.StatValues.Max);
X = XMin:single(resolutionX(1)):XMax;
[X, resolutionX, XMin, XMax] = centrage_magnetique(X); %#ok
X = double(X);

nbColumns = length(X);
clear XMax

M = length(ilig_Image);
Reflectivite     = NaN(M, nbColumns, 'single');
NuFaisceau       = NaN(M, nbColumns, 'single');
NbPointsMoyennes = zeros(M, nbColumns, 'single');
% SampleFrequency = this.Sonar.SampleFrequency;
% resolutionPing = repmat(resolutionX, M, 1);
% resolutionPing = resolutionPing .* (SampleFrequency(suby,1) ./ (SampleFrequency(suby,1)));

str = this.Name;
str(length(str)+1:150) = '.';
str1 = 'IFREMER - SonarScope : Traitement PingSamples to PingAcrossDist';
str2 = 'IFREMER - SonarScope : Processing PingSamples to PingAcrossDist';
hw = create_waitbar(str, 'Entete', Lang(str1,str2), 'N', M);
clear str
[~, x0] = min(abs(X));
for i=1:M
    my_waitbar(i, M, hw);
    
    iImage = i;
    %     x  = acrossD.Image(ilig_ReceptionBeam(i),:); % Modifi� le 08/03/2010 : pb fichier 112 Anne-Laure
    x  = acrossD.Image(i,:); % Modifi� le 08/03/2010 : pb fichier 112 Anne-Laure
    if sum(~isnan(x)) == 0
        continue
    end
    
    subxReceptionBeam = (ReceptionBeam.x >= min(this.x(subx))) & (ReceptionBeam.x <= max(this.x(subx)));
    
    iFais  = ReceptionBeam.Image(ilig_ReceptionBeam(i),subxReceptionBeam);
    clear subxReceptionBeam
    ping   = this.Image(ilig_Image(i),subx);
    %     subNaN = find(isnan(iFais) | isnan(x));
    subNaN = (isnan(iFais) | isnan(x));
    iFais(subNaN) = [];
    x(subNaN)     = [];
    ping(subNaN)  = [];
    
    %     indx = 1 + round((x - XMin) / resolutionPing(ilig_Image(i)));
    %     indx = x0 + round(x / resolutionPing(ilig_Image(i)));
    indx = x0 + round(x / resolutionX);
    %     indx = 1 + floor((x - XMin) / resolutionX); % Ca centre le BS mais ca
    %     supprime des pings
    R = zeros(1, nbColumns);
    B = zeros(1, nbColumns);
    
    for j=1:length(indx)
        ik = indx(j);
        val = ping(j);
        if (ik >= 1) && (ik <= nbColumns)
            R(ik) = R(ik) + val;
            B(ik) = B(ik) +  iFais(j);
            NbPointsMoyennes(iImage,ik) = NbPointsMoyennes(iImage,ik) + increment;
        end
    end
    N = NbPointsMoyennes(iImage,:);
    
    subNonNan = find((N ~= 0) & ~isnan(R));
    if isempty(subNonNan)
        continue
    else
        R(subNonNan) = R(subNonNan) ./ (N(subNonNan) / increment);
        B(subNonNan) = B(subNonNan) ./ (N(subNonNan) / increment);
    end
    
    subNan    = find(((N(1:end-1) == 0) & (N(2:end) ~= 0)) | ...
        ((N(1:end-1) == 0) & (N(2:end) ~= 0)) );
    if ~isempty(subNan) && (length(subNonNan) > 2)
        R(subNan) = interp1(subNonNan, R(subNonNan), subNan);
        B(subNan) = interp1(subNonNan, B(subNonNan), subNan);
        N(subNan) = -1;
        subNan    = find((N == 0) | isnan(R));
        R(subNan) = NaN;
        B(subNan) = NaN;
    else
        continue
    end
    
    Reflectivite(iImage, :) = R;
    NuFaisceau(iImage, :)   = round(B);
    NbPointsMoyennes(iImage, subNan) = -1;
    
    %{
% Alternative beaucoup plus co�teuse en temps calcul
sub = (indx <= nbColumns);
iFais = iFais(sub);
indx  = indx(sub);
ping  = ping(sub);
B1 = grpstats(iFais, indx, {'mean'});
[R1, N1] = grpstats(ping, indx, {'mean','numel'});
x1 = unique(indx);
Reflectivite(iImage, x1) = R1;
NuFaisceau(iImage, x1)   = round(B1);
NbPointsMoyennes(iImage,x1) = N1;
    %}
end
my_close(hw, 'MsgEnd');

clear acrossD

this = replace_Image(this, Reflectivite);

this.GeometryType = cl_image.indGeometryType('PingAcrossDist');
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

k = strfind(this.TagSynchroX, ' - Ping');
if isempty(k)
    this.TagSynchroX = [this.TagSynchroX ' - PingAcrossDist'];
else
    this.TagSynchroX = [this.TagSynchroX(1:k(end)-1) ' - PingAcrossDist'];
end

this.x = X;
clear X
subx = 1:length(this.x);
this = majCoordonnees(this, subx, ilig_Image);
this.Sonar.ResolutionX = resolutionX;
this.Sonar.Height = Range.Sonar.Height(ilig_Range);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this);

%% Par ici la sortie

this.Writable = false;
varargout{1} = this;

this = replace_Image(this, NuFaisceau);

%% Mise � jour de l'instance

this = compute_stats(this);
this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.ColormapIndex = 3;
this.DataType = cl_image.indDataType('RxBeamIndex');
this = update_Name(this);
this.Writable = false;
varargout{1}(2) = this;

%%

if createNbp
    this = replace_Image(this, NbPointsMoyennes);
    this = compute_stats(this);
    this.TagSynchroContrast = num2str(rand(1));
    CLim = [this.StatValues.Min this.StatValues.Max];
    this.CLim = CLim;
    this.ColormapIndex = 3;
    this.DataType = cl_image.indDataType('AveragedPtsNb');
    this = update_Name(this);
    this.Writable = false;
    varargout{1}(3) = this;
end
flag = 1;
