function [flag, RollBias] = sonar_PatchtestRoll(Z, AcrossDistance, AlongDistance, A, resol)

RollBias = [];

Fig = FigUtils.createSScFigure;
for k=1:2
    Heading{k}    = get(Z(k), 'Heading'); %#ok<AGROW>
    FishLatitude  = get(Z(k), 'FishLatitude');
    FishLongitude = get(Z(k), 'FishLongitude');
        
    % Calcul des coordonn�es g�ographiques
    [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z(k), AcrossDistance(k), AlongDistance(k), ...
        Heading{k}, FishLatitude, FishLongitude);
    if ~flag
        return
    end
    
    % Calcul du MNT
    [flag, Z_Mosa(k)] = sonar_mosaique2(Z(k), A(k), LatLon(1), LatLon(2), resol, 'AlongInterpolation',  1); %#ok<AGROW>
    if ~flag
        return
    end
    
    % Interpolation
    Z_Mosa(k) = WinFillNaN(Z_Mosa(k), 'window', [5 5]); %#ok<AGROW>
    % imagesc(Z_Mosa(k))
    h(k) = subplot(3,2,k); %#ok<AGROW>
    Z_Mosa(k)
    imagesc(Z_Mosa(k), 'Axe', h(k));
    title(extract_ImageName(Z(k)))
end

%% Calcul de l'image r�siduelle

residuals = Z_Mosa(2) - Z_Mosa(1);
if (residuals.nbRows == 0) || (residuals.nbColumns == 0)
    flag = 0;
    return
end
residuals = residuals / 2;
h(3) = subplot(3,2,3);
imagesc(residuals, 'Axe', h(3)); title('Residuals')
    
%% Nettoyage de l'image r�siduelle

str1 = 'Une fen�tre SonarScope va s''ouvrir, elle va vous permettre de nettoyer �ventuellement l''image des r�sidus. Le calcul se poursuivra une faois que vous aurez ferm� cette fen�tre.';
str2 = 'A SonarScope window will open. You can clean up the residual images and close the window to continue the processing.';
my_warndlg(Lang(str1,str2), 1);

residuals = SonarScope(residuals, 'ButtonSave', 1);
residuals = residuals(end);
h(3) = subplot(3,2,3);
imagesc(residuals, 'Axe', h(3)); title('Residuals')

%% Calcul des orientations des deux profils

Heading1 = median(Heading{1}(:));
Heading2 = median(Heading{2}(:));

%% Test si les cap sont bien oppos�s

diff = abs(Heading2 - Heading1);
if abs(diff-180) > 20
    str1 = 'Les orientation de ces deux lignes ne semblent pas �tre oppos�es. Voulez-vous poursuivre malgr� tout ?';
    str2 = 'These two lines do not seem to be in oppposite directions. Do you want to continue anyway ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (rep == 2)
        flag = 0;
        return
    end
end

%% Calcul de la pente

Azimuth = Heading1 + 90;
[p, flag] = slopeAzimuth(residuals, 'Azimuth', Azimuth);
if ~flag
    return
end
p.ColormapIndex = 3;
h(4) = subplot(3,2,4);
imagesc(p, 'Axe', h(4)); title('Slopes')

%% Contr�le de la pente

str1 = 'Une fen�tre SonarScope va s''ouvrir, elle va vous permettre de nettoyer �ventuellement l''image des pentes. Le calcul se poursuivra une faois que vous aurez ferm� cette fen�tre.';
str2 = 'A SonarScope window will open. You can clean up the slopes image and close the window to continue the processing.';
my_warndlg(Lang(str1,str2), 1);
p = SonarScope(p, 'ButtonSave', 1);
p = p(end);
h(4) = subplot(3,2,4);
imagesc(p, 'Axe', h(4)); title('Slopes')

linkaxes(h(1:4))

%% Calcul du biais

h(5) = subplot(3,2,5);
histo(p, 'Axe', h(5), 'Synchronisation', 0); title('Histogram of slopes')

bins = -2:0.01:2;
p = compute_stats(p, 'bins', bins);

histo1D(get_Image(p), bins, 'Histo', 'Titre', 'Histogram of slopes of residuals/2', 'XLabel', 'Slopes');

st = p.StatValues;
RollBias = st.Moyenne;

str1 = sprintf('Le biais de roulis est estim� � %s deg.', num2str(RollBias, '%.3f'));
str2 = sprintf('The roll biais is estimated to %s deg.', num2str(RollBias, '%.3f'));

% TODO : a am�liorer
figure(Fig)
h(6) = subplot(3,2,6); set(h(6), 'Visible', 'Off')
text(0, 1, Lang(str1,str2), 'FontSize', 10);

% my_warndlg(Lang(str1,str2), 1);
edit_infoPixel(Lang(str1,str2), [], 'PromptString', 'Estimated roll biais')

flag = 1;
