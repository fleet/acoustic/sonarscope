function [flag, that] = sonar_PhaseRestauration(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, a] = unitaire_sonar_PhaseRestauration(this(i), varargin{:});
    if ~flag
        that = [];
        return
    end
    that(i,:) = a; %#ok<AGROW>
end


function [flag, this] = unitaire_sonar_PhaseRestauration(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controles

flag = testSignature(this, 'DataType', cl_image.indDataType('TxAngle'), ...
    'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange')]);
if ~flag
    this = [];
    return
end

%% Traitement

m = floor(this.nbColumns / 2);
X = this.Image(suby,:);
Z = NaN(length(suby),length(subx), 'single');
ka = 4; % Caleur pour AC
for k=1:length(suby)
    h = this.Sonar.Height(suby(k),1);
    fe = this.Sonar.SampleFrequency(suby(k),1);
    
    % Tribord
    kh = m + abs(floor(h));
    subTri = (kh+1):this.nbColumns;
    phRaw = X(k,subTri);
    
    flagPlot = (mod(k,10) == 0);
    [pwNew, z] = interferometre_restauration_phase(phRaw, h, fe, ka, k, flagPlot);
    X(k,subTri) = pwNew;
    Z(k,subTri) = z;
end

for k=1:length(suby)
    h = this.Sonar.Height(suby(k),1);
    fe = this.Sonar.SampleFrequency(suby(k),1);
    
    % Babord
    kh = m - abs(floor(h));
    subBab = 1:kh;
    phRaw = fliplr(X(k,subBab));
    
    flagPlot = 0;
    [pwNew, z] = interferometre_restauration_phase(phRaw, h, fe, ka, k, flagPlot);
    X(k,subBab) = -fliplr(pwNew);
    Z(k,subBab) = fliplr(z);
end
this = replace_Image(this, X);

%% Mise � jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

ImageName = this.Name;
this = update_Name(this, 'Append', 'PhRestore');

%% Pseudo bathy

this(2) = replace_Image(this(1), Z);
this(2) = compute_stats(this(2));
this(2).DataType = cl_image.indDataType('Bathymetry');

%% Rehaussement de contraste

this(2).TagSynchroContrast = num2str(rand(1));
CLim = [this(2).StatValues.Min this(2).StatValues.Max];
this(2).CLim = CLim;
this(2) = update_Name(this(2), 'Name', ImageName, 'Append', 'Depth');

