function [flag, this] = sonar_PingAcrossSample2PingAcrossDist(this, resolutionX, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, this(i)] = unitaire_PingAcrossSample2PingAcrossDist(this(i), resolutionX, varargin{:});
end


function [flag, this] = unitaire_PingAcrossSample2PingAcrossDist(this, resolutionX, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, flagFillNaN] = getPropertyValue(varargin, 'FillNaN', 1); %#ok<ASGLU>

I = this.Image(suby,subx);
I = ~isnan(I);

nbPings = length(suby);
ResolAcrossSample = this.Sonar.ResolAcrossSample(suby);
XMin1 = NaN(nbPings,1, 'single');
XMin1Data = NaN(nbPings,1, 'single');
XMax1Data = NaN(nbPings,1, 'single');
ImageName = extract_ImageName(this);
strDataType = cl_image.strDataType{this.DataType};
for k=1:nbPings
    w = find(I(k,:), 1, 'first');
    if isempty(w)
        continue
    else
        ideb = w;
    end
    w = find(I(k,:), 1, 'last');
    ifin = w;
    RAS = ResolAcrossSample(k);
    XMin1Data(k) = this.x(subx(ideb)) * RAS;
    XMax1Data(k) = this.x(subx(ifin)) * RAS;
    XMin1(k)     = this.x(subx(1)) * RAS;
end
clear I
XMin2 = min(XMin1Data);
XMax2 = max(XMax1Data);

deltax2 = resolutionX;
n2 = 1 + ceil((XMax2 - XMin2) / deltax2);
x2 = (1:n2) * deltax2 + XMin2;
x2 = centrage_magnetique(double(x2));
classImage = class(this.Image(1));
K = NaN(nbPings, n2, classImage);
N = NaN(nbPings, n2, 'single');
str1 = sprintf('Traitement AcrossSample -> AcrossDist\n%s\n"%s"', strDataType, ImageName);
str2 = sprintf('Processing AcrossSample -> AcrossDist\n%s\n"%s"', strDataType, ImageName);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
try
    thisImage = this.Image(:,:);
catch %#ok<CTCH>
    thisImage = this.Image;
end
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    if resolutionX > ResolAcrossSample(k) % R�duction
        y1 = thisImage(suby(k), :);
        subx1 = subx;
        deltax1 = ResolAcrossSample(k);
        if NUMBER_OF_PROCESSORS == 0
            [y2, nbPts] = my_reduce(y1, deltax1, deltax2, XMin1(k), XMin2, XMax2, subx1);
        else
            [y2, nbPts] = my_reduce_mex(y1, deltax1, deltax2, XMin1(k), XMin2, XMax2, subx1);
            if length(y2) ~= n2
                [y2, nbPts] = my_reduce(y1, deltax1, deltax2, XMin1(k), XMin2, XMax2, subx1);
            end
        end
        K(k,:) = y2;
        N(k,:) = nbPts;
    else % Agrandissement
        y1 = this.Image(suby(k), :);
        subx1 = subx;
        deltax1 = ResolAcrossSample(k);
        if NUMBER_OF_PROCESSORS == 0
            [y2, nbPts] = my_reduce(y1, deltax1, deltax2, XMin1(k), XMin2, XMax2, subx1);
        else
            [y2, nbPts] = my_reduce_mex(y1, deltax1, deltax2, XMin1(k), XMin2, XMax2, subx1);
        end
        K(k,:) = y2;
        N(k,:) = nbPts;
    end
end
my_close(hw, 'MsgEnd');

if flagFillNaN
    K = fillNaN_mean(K, [1 5]);
end
y = this.y(suby);

this = replace_Image(this, K);
this.GeometryType = cl_image.indGeometryType('PingAcrossDist');
this.TagSynchroX = strrep(this.TagSynchroX, 'PingAcrossSample', 'PingAcrossDist');
this.XUnit = 'm';

%% Mise a jour de coordonnees

this = majCoordonneesXY(this, x2, y);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this);

flag = 1;
