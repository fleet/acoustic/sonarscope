function [flag, a] = sonar_PingBeam2PingAcrossSample(this, RxBeamIndexPingAcrossSample, AcrossDistPingBeam, varargin)

flag = 0;
a    = cl_image.empty;

for k=1:length(this)
    [flag, b] = unitaire_sonar_PingBeam2PingAcrossSample(this(k), RxBeamIndexPingAcrossSample, AcrossDistPingBeam, varargin{:});
    if ~flag
        continue
    end
    a(k) = b;
end


function [flag, a] = unitaire_sonar_PingBeam2PingAcrossSample(this, RxBeamIndexPingAcrossSample, AcrossDistPingBeam, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

suby = sort(suby);
% subx = 1:length(RxBeamIndexPingAcrossSample.x); % Avant

%% Controles

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Recherche de la zone commune

suby = suby(~isnan(this.Sonar.PingCounter(suby)));
PC1 = this.Sonar.PingCounter(suby);
PC3 = RxBeamIndexPingAcrossSample.Sonar.PingCounter;

[~, ~, ilig_Image, ilig_PingAcrossDist] = lin_intersect(PC1, PC3);
clear PC1 PC2 PC3
ilig_Image = suby(ilig_Image);

nbColumns = length(subx);
nbBeamsPingBeam = this.nbColumns;
clear XMax

M = length(ilig_Image);
Reflectivite     = NaN(M, nbColumns, 'single');

% MethodNearest = getMethodInterpolation(this);
flagMethod = isIntegerVal(this);
% if strcmp(MethodNearest, 'nearest')
if flagMethod
    subBeams = 1:nbBeamsPingBeam;
else
    x2 = RxBeamIndexPingAcrossSample.x;
end

str = this.Name;
str(length(str)+1:150) = '.';
hw = create_waitbar(str, 'Entete', 'IFREMER - SonarScope : Processing PingSamples to PingAcrossSample', 'N', M);
clear str
for i=1:M
    my_waitbar(i, M, hw);
    
    PingSignalPingBeam              = this.Image(ilig_Image(i),:);
    PingRxBeamIndexPingAcrossSample = RxBeamIndexPingAcrossSample.Image(ilig_PingAcrossDist(i),:);
    
    R = NaN(1,nbColumns, 'single');
    %     if strcmp(MethodNearest, 'nearest')
    if flagMethod
        subNonNaN = ~isnan(PingRxBeamIndexPingAcrossSample);
        R(subNonNaN) = PingSignalPingBeam(subBeams(PingRxBeamIndexPingAcrossSample(subNonNaN)));
        Reflectivite(i, :) = R;
    else
        subNonNaN = ~isnan(PingRxBeamIndexPingAcrossSample);
        x1 = AcrossDistPingBeam.Image(ilig_Image(i),:);
        subx1NonNaN = ~isnan(x1) & ~isnan(PingSignalPingBeam);
        R(subNonNaN) = my_interp1(x1(subx1NonNaN), PingSignalPingBeam(subx1NonNaN), x2(subNonNaN) * RxBeamIndexPingAcrossSample.Sonar.ResolAcrossSample(ilig_PingAcrossDist(i)));
        Reflectivite(i, subNonNaN)     = R(subNonNaN);
    end
end
my_close(hw, 'MsgEnd');

that = replace_Image(RxBeamIndexPingAcrossSample, Reflectivite);


%% Calcul des statistiques

that = compute_stats(that);

%% Rehaussement de contraste

that.TagSynchroContrast = num2str(rand(1));
CLim = [that.StatValues.Min that.StatValues.Max];
that.CLim     = CLim;
that.Unit     = Unit;
that.DataType = this.DataType;

%% Completion du titre

that = update_Name(that);

%% Par ici la sortie

that = set(that, 'Writable', 'false');
a = that;

flag = 1;
