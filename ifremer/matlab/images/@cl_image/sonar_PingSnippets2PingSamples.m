function [flag, a] = sonar_PingSnippets2PingSamples(this, Angle, varargin)

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, DataSnippets] = getPropertyValue(varargin, 'DataSnippets', []); %#ok<ASGLU>

flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', cl_image.indGeometryType('PingSnippets'));
if ~flag
    a = [];
    return
end

nomFic = this.InitialFileName;

if isempty(DataSnippets)
    nomFicSnippet = ResonSnippetNames(nomFic, str2double(this.TagSynchroX(1:4)));
    DataSnippets = loadmat(nomFicSnippet.Mat, 'nomVar', 'Data');
end
% [sub1, subDataSnippets] = intersect3(this.Sonar.Time(suby), DataSnippets.Time, DataSnippets.Time);

% TODO : ATTENTION danger, il faut certainement faire
% PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
[sub1, subDataSnippets] = intersect3(this.Sonar.PingCounter(suby), DataSnippets.PingCounter, DataSnippets.PingCounter);

DataSnippets.PingCounter                = DataSnippets.PingCounter(subDataSnippets);
DataSnippets.NbBeams                    = DataSnippets.NbBeams(subDataSnippets);
DataSnippets.Reflectivity               = DataSnippets.Reflectivity(subDataSnippets);
% DataSnippets.DataSampleType             = DataSnippets.DataSampleType(subDataSnippets);
DataSnippets.BeamDescriptor             = DataSnippets.BeamDescriptor(subDataSnippets,:);
DataSnippets.BeginSampleDescriptor      = DataSnippets.BeginSampleDescriptor(subDataSnippets,:);
DataSnippets.CenterSampleDistance       = DataSnippets.CenterSampleDistance(subDataSnippets,:);
DataSnippets.BeamAcrossTrackDistance    = DataSnippets.BeamAcrossTrackDistance(subDataSnippets,:);
DataSnippets.EndSampleDescriptor        = DataSnippets.EndSampleDescriptor(subDataSnippets,:);
DataSnippets.SampleRate                 = DataSnippets.SampleRate(subDataSnippets);
T = DataSnippets.Time;
T.timeMat = T.timeMat(subDataSnippets);
DataSnippets.Time = T;
subDataSnippets = 1:length(subDataSnippets);

fe = get(this, 'SampleFrequency');

% NbBeams = DataSnippets.NbBeams(1);
NbBeams = size(DataSnippets.BeamDescriptor, 2);
N = length(subDataSnippets);
RangeCentre = NaN(N, NbBeams, 'single');

Range     = NaN(1, 100*NbBeams, 'single');
% BeamIndex = NaN(1, 100*NbBeams, 'single');

RangeMin = Inf;
RangeMax = -Inf;
Range0 = (-49.5:49.5) - 0.5;

str = sprintf('Processing file %s', nomFic);
hw = create_waitbar(str, 'N', N);
% hTitre = get(get(hw, 'Children'), 'Title');
% set(hTitre, 'Interpreter', 'none', 'String', str);
for i=1:N
    my_waitbar(i, N, hw);
    
    DeltaD = 1500 / (2 * fe(suby(sub1(i))));
    this.Sonar.Height(suby(sub1(i))) = round(this.Sonar.Height(suby(sub1(i))) / DeltaD);
    
    for iFais=1:size(DataSnippets.BeamDescriptor, 2) % DataSnippets.NbBeams(subDataSnippets(i))
        if isempty(DataSnippets.BeamDescriptor(iFais))
            continue
        end
        subc = (1+(iFais-1)*100):(iFais*100);
        if DataSnippets.BeamAcrossTrackDistance(subDataSnippets(i),iFais) < 0
            signe = -1;
        else
            signe = 1;
        end
        RangeCentre(i,iFais)  = DataSnippets.CenterSampleDistance(subDataSnippets(i), iFais) * signe;
        Range(subc) = (Range0 * signe) + RangeCentre(i,iFais);
        
        RangeMin = min(RangeMin, min(Range(subc)));
        RangeMax = max(RangeMax, max(Range(subc)));
    end
    % figure; plot(DataSnippets.CenterSampleDistance(subDataSnippets(i),:), '+'); grid on;
    % figure;
    % plot(DataSnippets.BeamAcrossTrackDistance(subDataSnippets(i),:), '+'); grid on;
    % figure; plot(Range, '+'); grid on
end
my_close(hw)

ColMin = floor(RangeMin)-1;
ColMax =  ceil(RangeMax);
% NbCol = ColMax - ColMin + 2;
NbCol = ColMax - ColMin + 1;

try
    Reflectivity = NaN(N, NbCol, 'single');
    NbPts        = NaN(N, NbCol, 'single');
    Beams        = NaN(N, NbCol, 'single');
    Angles       = NaN(N, NbCol, 'single');
catch
    Reflectivity = cl_memmapfile('Value', NaN, 'Size', [N, NbCol], 'Format', 'single');
    NbPts        = cl_memmapfile('Value', NaN, 'Size', [N, NbCol], 'Format', 'single');
    Beams        = cl_memmapfile('Value', NaN, 'Size', [N, NbCol], 'Format', 'single');
end

hw = create_waitbar('Constitution de l''image sonar', 'N', N);
for i=1:N
    my_waitbar(i, N, hw);
    
    Samples   = this.Image(suby(sub1(i)), :);
    A         = Angle.Image(suby(sub1(i)), :);
    Samples = reflec_dB2Enr(Samples);
    [R, B, Nbp, A] = calcul_Snippet2Sample(NbCol, ColMin, DataSnippets.BeamDescriptor(subDataSnippets(i),:), Samples, A, RangeCentre(i,:));
    iReflec = N - i + 1;
    NbPts(iReflec,:)        = Nbp;
    Beams(iReflec,:)        = B;
    Reflectivity(iReflec,:) = R;
    Angles(iReflec,:)       = A;
    %     figure; plot(Reflectivity(iReflec,:), '+'); grid on; title('subcR')
end
my_close(hw)

%% Reflectivit�

a = replace_Image(this, Reflectivity);

a.x = double(ColMin:ColMax);
a.GeometryType = cl_image.indGeometryType('PingSamples');

%% Mise � jour de coordonn�es

a = majCoordonnees(a, 1:length(a.x), suby);

[~, nomFic] = fileparts(nomFic);
a.TagSynchroX = [this.TagSynchroX(1:4) '_' nomFic ' - PingSamples'];

%% Calcul des statistiques

a = compute_stats(a);

%% Rehaussement de contraste

a.TagSynchroContrast = num2str(rand(1));
CLim = [a.StatValues.Min a.StatValues.Max];
a.CLim = CLim;

%% Mise � jour du nom de l'image

a = update_Name(a);

%% Beams

a(2) = replace_Image(a, Beams);
a(2).Unit = ' ';
a(2).DataType = cl_image.indDataType('RxBeamIndex');
a(2) = compute_stats(a(2));
a(2).ColormapIndex = 3;
a(2).TagSynchroContrast = num2str(rand(1));
CLim = [a(2).StatValues.Min a(2).StatValues.Max];
a(2).CLim = CLim;
a(2) = update_Name(a(2));

%% NbPts

a(3) = replace_Image(a(1), NbPts);
a(3).Unit = 'num';
a(3).DataType = cl_image.indDataType('AveragedPtsNb');
a(3) = compute_stats(a(3));
a(3).ColormapIndex = 3;
a(3).TagSynchroContrast = num2str(rand(1));
CLim = [a(3).StatValues.Min a(3).StatValues.Max];
a(3).CLim = CLim;
a(3) = update_Name(a(3));

%% Angles

a(4) = replace_Image(a(1), Angles);
a(4).Unit = 'Deg';
a(4).DataType = cl_image.indDataType('TxAngle');
a(4) = compute_stats(a(4));
a(4).ColormapIndex = 3;
a(4).TagSynchroContrast = num2str(rand(1));
CLim = [a(4).StatValues.Min a(4).StatValues.Max];
a(4).CLim = CLim;
a(4) = update_Name(a(4));

flag = 1;
