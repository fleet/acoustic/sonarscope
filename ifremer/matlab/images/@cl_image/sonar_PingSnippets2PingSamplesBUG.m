function [flag, a] = sonar_PingSnippets2PingSamplesBUG(this, varargin)

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, DataSnippets] = getPropertyValue(varargin, 'DataSnippets', []); %#ok<ASGLU>

flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', cl_image.indGeometryType('PingSnippets'));
if ~flag
    a = [];
    return
end

nomFic = this.InitialFileName;

if isempty(DataSnippets)
    nomFicSnippet = ResonSnippetNames(nomFic);
    DataSnippets = loadmat(nomFicSnippet.Mat, 'nomVar', 'Data');
end
[sub1, subDataSnippets] = intersect3(this.Sonar.Time(suby), DataSnippets.Time, DataSnippets.Time);
subDataSnippets = fliplr(subDataSnippets);

fe = get(this, 'SampleFrequency');

NbBeams = DataSnippets.NbBeams(1);
N = length(suby);
RangeCentre = NaN(N, NbBeams, 'single');
Range       = NaN(1, 100*NbBeams, 'single');
BeamIndex   = NaN(1, 100*NbBeams, 'single');

RangeMin = Inf;
RangeMax = -Inf;
Range0 = (-49.5:49.5);

str = sprintf('Processing file %s', nomFic);
hw = create_waitbar(str, 'N', N);
% hTitre = get(get(hw, 'Children'), 'Title');
% set(hTitre, 'Interpreter', 'none', 'String', str);
for i=1:N
    my_waitbar(i, N, hw);
    
    DeltaD = 1500 / (2 * fe(suby(i)));
    this.Sonar.Height(suby(i)) = round(this.Sonar.Height(suby(i)) / DeltaD);
    
    for iFais=1:DataSnippets.NbBeams(subDataSnippets(i))
        subc = (1+(iFais-1)*100):(iFais*100);
        if DataSnippets.BeamAcrossTrackDistance(subDataSnippets(i),iFais) < 0
            signe = -1;
        else
            signe = 1;
        end
        RangeCentre(i,iFais)  = DataSnippets.CenterSampleDistance(subDataSnippets(i), iFais) * signe;
        Range(subc) = (Range0 * signe) + RangeCentre(i,iFais);
        
        RangeMin = min(RangeMin, min(Range(subc)));
        RangeMax = max(RangeMax, max(Range(subc)));
    end
end
my_close(hw)

ColMin = floor(RangeMin);
ColMax =  ceil(RangeMax);
NbCol = ColMax - ColMin + 1;

Reflectivity = NaN(N, NbCol, 'single');
NbPts        = NaN(N, NbCol, 'single');
Beams        = NaN(N, NbCol, 'single');
hw = create_waitbar('Constitution de l''image sonar', 'N', N);
% profile on
for i=1:N
    my_waitbar(i, N, hw);
    
    R   = zeros(1, NbCol, 'single');
    Nbp = zeros(1, NbCol, 'single');
    IndexBeamMoyen = zeros(1, NbCol, 'single');
    
    for iFais=1:DataSnippets.NbBeams(subDataSnippets(i))
        subc = (1+(iFais-1)*100):(iFais*100);
        Range(subc) = Range0 * sign(RangeCentre(i,iFais)) + RangeCentre(i,iFais);
        BeamIndex(subc) = iFais;
    end
    %     figure; plot(Range, '+'); grid on; title('Range')
    %     figure; plot(BeamIndex, '+'); grid on; title('BeamIndex')
    
    subcR = round(Range) - ColMin + 1;
    %     figure; plot(subcR, '+'); grid on; title('subcR')
    %     figure; plot(Samples, '+'); grid on; title('Samples')
    
    Samples   = this.Image(suby(i), :);
    subNonNaN =  ~isnan(Samples);
    Samples(subNonNaN) = reflec_dB2Enr(Samples(subNonNaN));
    BeamIndex = BeamIndex(subNonNaN);
    subcR     = subcR(subNonNaN);
    
    for k=1:length(subcR)
        subcR_k = subcR(k);
        R(subcR_k) = R(subcR_k) + Samples(k);
        Nbp(subcR_k) = Nbp(subcR_k) + 1;
        IndexBeamMoyen(subcR_k) = IndexBeamMoyen(subcR_k) + BeamIndex(k);
    end
    for k=1:NbCol
        Nbp_k = Nbp(k);
        if Nbp_k ~= 0
            %             Reflectivity(N-i+1,k) = R(k) / Nbp_k;
            %             NbPts(N-i+1,k) = Nbp_k;
            %             Beams(N-i+1,k) = round(IndexBeamMoyen(k) / Nbp_k);
            Reflectivity(i,k) = R(k) / Nbp_k;
            NbPts(i,k) = Nbp_k;
            Beams(i,k) = round(IndexBeamMoyen(k) / Nbp_k);
        end
    end
    Reflectivity(i,:) = reflec_Enr2dB(Reflectivity(i,:));
    %     figure; plot(Reflectivity(i,:), '+'); grid on; title('subcR')
end
my_close(hw)

% ------------
% Reflectivité

a = replace_Image(this, Reflectivity);

a.x = ColMin:ColMax;
a.GeometryType = cl_image.indGeometryType('PingSamples');

% --------------------------
% Mise a jour de coordonnees

a = majCoordonnees(a, 1:length(a.x), suby);
% a.Sonar.ResolutionX = resolutionX;

[nomDir, nomFic] = fileparts(nomFic);
a.TagSynchroX = [nomFic ' - Samples'];

% -----------------------
% Calcul des statistiques

a = compute_stats(a);

% -------------------------
% Rehaussement de contraste

a.TagSynchroContrast = num2str(rand(1));
CLim = [a.StatValues.Min a.StatValues.Max];
a.CLim = CLim;

% -----------------------------
% Mise a jour du nom de l'image

a = update_Name(a);

% -----
% Beams

a(2) = replace_Image(a, Beams);
a(2).DataType = cl_image.indDataType('RxBeamIndex');
a(2) = compute_stats(a(2));
a(2).TagSynchroContrast = num2str(rand(1));
CLim = [a(2).StatValues.Min a(2).StatValues.Max];
a(2).CLim = CLim;
a(2) = update_Name(a(2));

% -----
% NbPts

a(3) = replace_Image(a(1), NbPts);
a(3).DataType = cl_image.indDataType('AveragedPtsNb');
a(3) = compute_stats(a(3));
a(3).TagSynchroContrast = num2str(rand(1));
CLim = [a(3).StatValues.Min a(3).StatValues.Max];
a(3).CLim = CLim;
a(3) = update_Name(a(3));

flag = 1;
