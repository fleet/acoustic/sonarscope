function [flag, b] = sonar_RDF_FiltreDDP(b, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

flag  = 1;
marge = 1;

RecurrencePlot = 100;

n = floor(2 * marge);
window = [max(3,2*n+1) max(3,2*n+1)];
hFiltre = fspecial('gaussian', window, marge);

N = b(1).nbRows;
% subx = 1: b(1).nbColumns;
if Mute
    hw = [];
else
    str1 = 'Filtrage de la bathy par bins en across distances';
    str2 = 'Bathymetry cleaning based on across bins';
    hw = create_waitbar(Lang(str1,str2), 'N', N);
end
% Z = b(1);
% X = b(2);
for iPing=1:N
     my_waitbar(iPing, N, hw)
     z = b(1).Image(iPing,:);
     x = b(2).Image(iPing,:);
     
%      z = get_val_ij(Z, iPing, subx);
%      x = get_val_ij(X, iPing, subx);
     
     teta = NaN(size(x));
     sub = (abs(x) < abs(z));
     teta(sub) =  abs(atan2(x(sub), -z(sub)) * (180/pi));
%      figure; plot(x, teta, '*k'); grid on; title('Teta')

     if mod(iPing, RecurrencePlot) == 1
        xAvant = x;
        zAvant = z;
     end

     xMin = min(x);
     xMax = max(x);
     xi = linspace(xMin, xMax, 100);
     xi = centrage_magnetique(xi);
     step = xi(2)-xi(1);
     xi(end+1) = xi(end) + step; %#ok<AGROW>
     zi = fliplr(0:-step:min(z));
     zi(zi > 0) = [];
     n1Np = length(zi)+1;
     n2Np = length(xi);
     Np = zeros(n1Np, n2Np, 'single');
     ix = 1 + round((x-xi(1)) / step);
     iz = 1 + round((z-zi(1)) / step);
     for k=1:length(x)
         ix_k = ix(k);
         if (iz(k) >= 1) && (iz(k) <= n1Np)
             if ~isnan(ix_k)
                 Np(iz(k),ix_k) = Np(iz(k),ix_k) + 1;
             end
         end
     end
     
     %      SonarScope(Np)
     Np = filter2(hFiltre, Np, 'same');
     %      SonarScope(Np)
     
     izMax = NaN(1, length(xi), 'single');
     NMax  = NaN(1, length(xi), 'single');
%      for k=1:size(Np,2)
%          Np(:,k) = Np(:,k) / sum(Np(:,k), 'omitnan');
%      end
     for k=1:size(Np,2)
         [NMax(k),izMax(k)] = max(Np(:,k));
     end
%      figure; subplot(2,1,1); plot(xi, izMax); grid on; subplot(2,1,2); plot(xi,NMax); grid
     for k=1:length(x)
         ix_k = ix(k);
         if ~isnan(ix_k)
             if iz(k) < (izMax(ix_k) - marge)
                 x(k) = NaN;
                 z(k) = NaN;
             elseif iz(k) > (izMax(ix_k) + marge)
                 x(k) = NaN;
                 z(k) = NaN;
             elseif NMax(ix_k) < 2
                 if teta(k) > 25
                    x(k) = NaN;
                    z(k) = NaN;
                 end
             end
             % TODO : continuer l'�purage en testant Np : les valeur trop
             % faibles peuvent �tre �limin�es
         end
     end
     
     if mod(iPing, RecurrencePlot) == 1
         Titre = sprintf('Filtre DDP, ping %d / %d', iPing, N);
         figure(1238); plot(xAvant, zAvant, '*r', x, z, '*k'); grid on; title(Titre)
         pause(0.2)
     end
     
     b(1).Image(iPing,:) = z;
     b(2).Image(iPing,:) = x;

%     Z = set_val(Z, iPing, subx, z);
% 	X = set_val(X, iPing, subx, x);
end
% b(1) = Z;
% b(2) = X;
my_close(hw, 'MsgEnd')
my_close(1238); %, 'TimeDelay', 60)
