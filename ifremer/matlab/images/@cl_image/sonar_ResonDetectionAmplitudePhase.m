% Calcul de l'aire insonifiee de la bathymetrie
%
% Syntax
%   [flag, b] = sonar_ResonDetectionAmplitudePhase(a)
%
% Input Arguments
%   a           : Instance de cl_image
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance de cl_image contenant l'image filtree
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that, mov, Phase, Amplitude] = sonar_ResonDetectionAmplitudePhase(Phase, Amplitude, DisplayLevel, BeamsToDisplay, IdentAlgo, GateDepth, varargin)

[varargin, mov] = getPropertyValue(varargin, 'Movie', []);

if (Phase(1).Sonar.SampleBeamData.SystemSerialNumber(1) == 7150) && ...
        (Phase(1).Sonar.SampleBeamData.Frequency < 18000)
    [tetav, DifferentielAngles] = VraisAngles_7150_12kHz;
else
    tetav = [];
end

nbImages = length(Phase);
nbBeams  = Phase(1).nbColumns;

ImageAngles         = NaN(nbImages, nbBeams, 'single');
ImageRange          = NaN(nbImages, nbBeams, 'single');
ImageQualityFactor  = NaN(nbImages, nbBeams, 'single');
ImageTypeDetection  = NaN(nbImages, nbBeams, 'single');

DataDepth = Reson_BottomDetector_initDataDepth(nbImages);
% DataDepth.Time = cl_time;
for iPing=1:nbImages
    [flag, PingRange, PingTD, PingQualityFactor, mov]  = sonar_ResonDetectionAmplitudePhase_unitaire(Phase(iPing), Amplitude(iPing), ...
        DisplayLevel, BeamsToDisplay, iPing, GateDepth, 'Movie', mov, 'IdentAlgo', IdentAlgo, ...
        'EmModel', Phase(1).Sonar.SampleBeamData.EmModel, varargin{:});
    if ~flag
        that = [];
        return
    end
    
    SampleBeamData = Phase(iPing).Sonar.SampleBeamData;
    
    % TODO : pourquoi est-ce que j'avais fait tout �a ???????????????????????????????????????????????????
    % On n'a pas de profil de c�l�rit�, on prend la c�l�rit� de
    % surface. C'est pas top mais on peut pas faire autrement
    coef = SampleBeamData.SoundVelocity / (2 * SampleBeamData.SampleRate);

    
    PingRange = PingRange * coef;
    ImageRange(iPing,:) = PingRange;
    
    
    ImageRange(iPing,:) = PingRange; % R�tabli les 17/03/2016 � la place de tout ce qu'il y a au dessus
    Phase(iPing).Sonar.SampleBeamData.R1SamplesFiltre     = PingRange; % Rajout� le 17/03/2016
    Amplitude(iPing).Sonar.SampleBeamData.R1SamplesFiltre = PingRange; % Rajout� le 17/03/2016
    
    ImageAngles(iPing,:) = SampleBeamData.TxAngle;

    ImageQualityFactor(iPing,:) = PingQualityFactor;
    ImageTypeDetection(iPing,:) = PingTD;
    
    DataDepth.VehicleDepth(iPing)      = 0;
    DataDepth.Time(iPing)              = SampleBeamData.Time;
    DataDepth.Heading(iPing)           = 0;
    DataDepth.TransducerDepth(iPing)   = 0;
    DataDepth.SurfaceSoundSpeed(iPing) = SampleBeamData.SoundVelocity(1); % TODO (1)
    DataDepth.Mode(iPing)              = 1;
    DataDepth.SampleRate(iPing)        = SampleBeamData.SampleRate(1); % TODO (1)
    DataDepth.Frequency(iPing)         = SampleBeamData.Frequency(1)/1000; % TODO (1)
    DataDepth.Roll(iPing)              = 0;
    DataDepth.Pitch(iPing)             = 0;
    DataDepth.Heave(iPing)             = 0;
    DataDepth.Tide(iPing)              = 0;
    DataDepth.PingCounter(iPing)       = SampleBeamData.PingCounter(1);% TODO (1)
    DataDepth.Longitude(iPing)         = 0;
    DataDepth.Latitude(iPing)          = 0;
end
DataDepth.Time = cl_time('timeMat', DataDepth.Time);

if nbImages > 1
    InitialFileName = Phase(1).InitialFileName;
    NomImage = extract_ImageName(Phase(1));
    
    [~, TagSynchro] = fileparts(InitialFileName);
    
    %     TagSynchroX = [NomImage ' - PingBeam'];
    %     TagSynchroY = [NomImage ' - PingBeam'];
    
    SonarDescription =  get(Phase(1), 'SonarDescription');
    identSondeur = get(SonarDescription, 'Sonar.SystemSerialNumber');
    TagSynchroX = [num2str(identSondeur) '_' TagSynchro];
    TagSynchroY = [num2str(identSondeur) '_' TagSynchro];
    
    InitialFileFormat = Phase(1).InitialFileFormat;
    GeometryType = cl_image.indGeometryType('PingBeam');
    DataType = cl_image.indDataType('RayPathSampleNb');
    SonarDescription = get(Phase(1), 'SonarDescription');
    nbCol = Phase(1).nbColumns;
    InitialImageName = Phase(1).InitialImageName;
    
    that = cl_image('Image', ImageRange);
    that.Name              =  NomImage;
    that.InitialImageName  = InitialImageName;
    that.Unit              = 'Sample';
    that.TagSynchroX       = TagSynchroX;
    that.TagSynchroY       = TagSynchroY;
    that.XUnit             = 'Beam #';
    that.YUnit             = 'Ping';
    that.InitialFileName   = InitialFileName;
    that.InitialFileFormat = InitialFileFormat;
    that.GeometryType      = GeometryType;
    that.DataType          = DataType;
    that = set_SonarDescription(that, SonarDescription);
    that.Comments          = 'Range coming from Amplitude & Phase';
    %     that.strMaskBits = strMaskBits;
    that.x                 = 1:nbCol;
    that.y                 = 1:nbImages;
    that.ColormapIndex     = 3;
    
    %% D�finition du nom de l'image
    
    that = update_Name(that);
    
    %% Transfert des signaux
    
    that = set_SonarHeight(that, -abs(DataDepth.VehicleDepth));
    that = set_SonarTime(that, DataDepth.Time);
    that = set_SonarHeading(that, DataDepth.Heading);
    that = set_SonarImmersion(that, DataDepth.TransducerDepth);
    that = set_SonarSurfaceSoundSpeed(that, DataDepth.SurfaceSoundSpeed');
    that = set_SonarPortMode_1(that, DataDepth.Mode);
    
    %     that = set_SonarRawDataResol(that, resolutionMajoritaire);
    %     that = set_SonarResolutionD(that, resolutionMajoritaire);
    
    that = set_SonarSampleFrequency(that, DataDepth.SampleRate);
    that = set_SonarFrequency(that, DataDepth.Frequency);
    
    that = set_SonarRoll(that, DataDepth.Roll);
    that = set_SonarPitch(that, DataDepth.Pitch);
    
    that = set_SonarHeave(that, DataDepth.Heave);
    that = set_SonarTide(that, DataDepth.Tide);
    that = set_SonarPingCounter(that, DataDepth.PingCounter);
    that = set_SonarFishLongitude(that, DataDepth.Longitude);
    that = set_SonarFishLatitude(that, DataDepth.Latitude);
    
    %% Image des angles
    that(2) = replace_Image(that, ImageAngles);
    that(2) = compute_stats(that(2));
    that(2).TagSynchroContrast = num2str(rand(1));
    CLim = [that(2).StatValues.Min that(2).StatValues.Max];
    that(2).CLim = CLim;
    that(2).Unit = 'Deg';
    that(2).DataType = cl_image.indDataType('TxAngle');
    that(2) = update_Name(that(2));
    
    %% Image des QualityFactor
    that(3) = replace_Image(that(1), ImageQualityFactor);
    that(3) = compute_stats(that(3));
    that(3).TagSynchroContrast = num2str(rand(1));
    CLim = [that(3).StatValues.Min that(3).StatValues.Max];
    that(3).CLim = CLim;
    that(3).Unit = '';
    that(3).DataType = cl_image.indDataType('MbesQualityFactor');
    that(3) = update_Name(that(3));
    
    %% Image des TypeDetection
    that(4) = replace_Image(that(1), ImageTypeDetection);
    that(4) = compute_stats(that(4));
    that(4).TagSynchroContrast = num2str(rand(1));
    CLim = [that(4).StatValues.Min that(4).StatValues.Max];
    that(4).CLim = CLim;
    that(4).Unit = '';
    that(4).DataType = cl_image.indDataType('DetectionType');
    that(4) = update_Name(that(4));
else
    that = [];
end
