function [flag, PingRange, TD, QualityFactor, mov] = sonar_ResonDetectionAmplitudePhase_unitaire(Phase, Amplitude, ...
    DisplayLevel, BeamsToDisplay, iPing, GateDepth, varargin)

global TestPlatformCmatlab %#ok<GVMIS>

[varargin, mov]       = getPropertyValue(varargin, 'Movie',     []);
[varargin, EmModel]   = getPropertyValue(varargin, 'EmModel',   []);
[varargin, IdentAlgo] = getPropertyValue(varargin, 'IdentAlgo', 1); %#ok<ASGLU>

Draught = get_DraughtShip;

%% V�rification des signatures

DataType = cl_image.indDataType('TxAngle');
flag = testSignature(Phase, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

%% Param�tres optionels

% [subx, suby, varargin] = getSubxSuby(Phase, varargin); %#ok<ASGLU>%

% TODO JMA : D�composer Amplitude et Phase
% Amplitude.Image(:,:)


%% BDA sur un ping

AmpImage   = Amplitude.Image(:,:);
PhaseImage = Phase.Image(:,:);

DataPing = get(Amplitude, 'SampleBeamData');
Frequency = get(Amplitude.Sonar.Desciption, 'Sonar.Freq');
BeamAngles = DataPing.TxAngle;

if isfield(DataPing, 'BottomDetectDepthFilterFlag')
    BottomDetectDepthFilterFlag       = DataPing.BottomDetectDepthFilterFlag;
    BottomDetectionFilterInfoMinDepth = DataPing.BottomDetectionFilterInfoMinDepth;
    BottomDetectionFilterInfoMaxDepth = DataPing.BottomDetectionFilterInfoMaxDepth;
else
    BottomDetectDepthFilterFlag       = 0;
    BottomDetectionFilterInfoMinDepth = [];
    BottomDetectionFilterInfoMaxDepth = [];
end
TxPulseWidth     = DataPing.TxPulseWidth; % s

% SampleRate       = DataPing.SampleRate; % Hz
SampleRate       = DataPing.SampleRate(1); % Hz

ReceiveBeamWidth = DataPing.ReceiveBeamWidth * (180/pi); % deg
if isfield(DataPing, 'PingSequence')
    PingSequence = DataPing.PingSequence;
else
    PingSequence = [];
end
SystemSerialNumber               = DataPing.SystemSerialNumber;
if isfield(DataPing, 'ProjectionBeam_3dB_WidthVertical')
    ProjectionBeam_3dB_WidthVertical = DataPing.ProjectionBeam_3dB_WidthVertical;
else
    ProjectionBeam_3dB_WidthVertical = [];
end
SoundVelocity = DataPing.SoundVelocity(1);
% MultiPingSequence              = DataPing.MultiPingSequence;

[DepthMinPing, DepthMaxPing] = BDA_gates(GateDepth, BottomDetectDepthFilterFlag, BottomDetectionFilterInfoMinDepth, BottomDetectionFilterInfoMaxDepth, Draught);

if any(TestPlatformCmatlab.sonar_BDA == [1 3 4])
    [PingRange_M, TD_M, QualityFactor_M, AmpPingRange_M, PhasePingRange_M, AmpPingQF_M, PhasePingQF_M, ...
        AmpPingSamplesNb_M, AmpPingQF2_M, PhasePingNbSamples_M, SampleRate_M, BeamAngles_M, iBeamBeg_M, iBeamEnd_M, ...
        SystemSerialNumber_M] = ...
        sonar_BDA(...
        AmpImage, PhaseImage, DepthMinPing, DepthMaxPing, BeamAngles, ...
        Frequency, SampleRate, TxPulseWidth, ReceiveBeamWidth, SystemSerialNumber, ...
        IdentAlgo, DisplayLevel, iPing, PingSequence, BeamsToDisplay, ...
        ProjectionBeam_3dB_WidthVertical, [], 'EmModel', EmModel);
end

if any(TestPlatformCmatlab.sonar_BDA == [2 3 4])
    IdentAlgo1 = 1 + mod(IdentAlgo-1, 3);
    [PingRange_C, TD_C, QualityFactor_C, AmpPingRange_C, PhasePingRange_C, AmpPingQF_C, PhasePingQF_C, ...
        SampleRate_C, BeamAngles_C, iBeamBeg_C, iBeamEnd_C, SystemSerialNumber_C] = ...
        sonar_BDA_reson(...
        single(AmpImage), single(PhaseImage), ...
        double(DepthMinPing), double(DepthMaxPing), double(BeamAngles), ...
        double(Frequency), double(SampleRate), double(TxPulseWidth), double(ProjectionBeam_3dB_WidthVertical), ...
        double(SoundVelocity), double(SystemSerialNumber), double(IdentAlgo1));
end

if any(TestPlatformCmatlab.sonar_BDA == [3 4])
    BDA_compare_Matlab_C('sonar_BDA', 'PingRange',     PingRange_M,     PingRange_C,     iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_PingRange);
    BDA_compare_Matlab_C('sonar_BDA', 'TD',            TD_M,            TD_C,            iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
    BDA_compare_Matlab_C('sonar_BDA', 'QualityFactor', QualityFactor_M, QualityFactor_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_QualityFactor);
    BDA_compare_Matlab_C('sonar_BDA', 'AmpPingRange', AmpPingRange_M, AmpPingRange_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
    BDA_compare_Matlab_C('sonar_BDA', 'PhasePingRange', PhasePingRange_M, PhasePingRange_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
    BDA_compare_Matlab_C('sonar_BDA', 'AmpPingQF', AmpPingQF_M, AmpPingQF_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
    BDA_compare_Matlab_C('sonar_BDA', 'PhasePingQF', PhasePingQF_M, PhasePingQF_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
    if (SampleRate_M ~= SampleRate_C)
        BDA_createMes('sonar_BDA', 'SampleRate', SampleRate_M, SampleRate_C, [], [], 0);
    end
    BDA_compare_Matlab_C('sonar_BDA', 'BeamAngles', BeamAngles_M, BeamAngles_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
    if (iBeamBeg_M ~= iBeamBeg_C)
        BDA_createMes('sonar_BDA', 'iBeamBeg', iBeamBeg_M, iBeamBeg_C, [], [], 0);
    end
    if (iBeamEnd_M ~= iBeamEnd_C)
        BDA_createMes('sonar_BDA', 'iBeamEnd', iBeamEnd_M, iBeamEnd_C, [], [], 0);
    end
    if (SystemSerialNumber_M ~= SystemSerialNumber_C)
        BDA_createMes('sonar_BDA', 'SystemSerialNumber', SystemSerialNumber_M, SystemSerialNumber_C, [], [], 0);
    end
end

switch TestPlatformCmatlab.sonar_BDA
    case {1; 3}
        PingRange             = PingRange_M;
        TD                    = TD_M;
        QualityFactor         = QualityFactor_M;
        AmpPingRange          = AmpPingRange_M;
        PhasePingRange        = PhasePingRange_M;
        AmpPingQF             = AmpPingQF_M;
        PhasePingQF           = PhasePingQF_M;
        AmpPingSamplesNb      = AmpPingSamplesNb_M;
        AmpPingQF2            = AmpPingQF2_M;
        PhasePingNbSamples    = PhasePingNbSamples_M;
        SampleRate            = SampleRate_M;
        BeamAngles            = BeamAngles_M;
        iBeamBeg              = iBeamBeg_M;
        iBeamEnd              = iBeamEnd_M;
        SystemSerialNumber    = SystemSerialNumber_M;
    case {2; 4}
        PingRange             = PingRange_C;
        TD                    = TD_C;
        QualityFactor         = QualityFactor_C;
        AmpPingRange          = AmpPingRange_C;
        PhasePingRange        = PhasePingRange_C;
        AmpPingQF             = AmpPingQF_C;
        PhasePingQF           = PhasePingQF_C;
        AmpPingSamplesNb      = NaN(size(TD_C)); % AmpPingSamplesNb_M;
        AmpPingQF2            = NaN(size(TD_C)); % AmpPingQF2_M;
        PhasePingNbSamples    = NaN(size(TD_C)); % PhasePingNbSamples_M;
        SampleRate            = SampleRate_C;
        BeamAngles            = BeamAngles_C;
        iBeamBeg              = iBeamBeg_C;
        iBeamEnd              = iBeamEnd_C;
        SystemSerialNumber    = SystemSerialNumber_C;
end

if DisplayLevel >= 2
    Coef = 1500 / (2 * SampleRate);
    nbSamples = Amplitude.nbRows;
    subAmp   = find(TD == 1);
    subPh    = find(TD == 2);
    subAmpKO = find((TD == 1) & (AmpPingQF   < 2.5));
    subPhKO  = find((TD == 2) & (PhasePingQF < 2.5));
    
    Fig = FigUtils.createSScFigure;
    
    ImageName = extract_ImageName(Amplitude);
    set(Fig, 'Name', ImageName)
    
    h(1) = subplot(5,2,1);
    hc = PlotUtils.createSScPlot(AmpPingRange, 'k');
    set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
    hold on; grid on;
    hc = PlotUtils.createSScPlot(PhasePingRange, 'r');
    set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
    set(h(1), 'YDir', 'Reverse');
    set(h(1), 'YLim', [0 nbSamples]);
    title('Range');
    hold off;
    %{
MasqueAmp
    %}
    
    h(2) = subplot(5,2,2);
    hc = PlotUtils.createSScPlot(subAmp, AmpPingRange(subAmp), 'k+');
    set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
    hold on; grid on;
    hc = PlotUtils.createSScPlot(subPh,  PhasePingRange(subPh), '+r');
    set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
    set(h(2), 'YDir', 'Reverse');
    set(h(1), 'YLim', [0 nbSamples]);
    title('Range');
    PlotUtils.createSScPlot(subPhKO,  PhasePingRange(subPhKO), 'og');
    PlotUtils.createSScPlot(subAmpKO, AmpPingRange(subAmpKO), 'og');
    hold off;
    
    h(3) = subplot(5,2,3);
    PlotUtils.createSScPlot(-(AmpPingRange * Coef) .* cosd(BeamAngles), 'k');
    hold on; grid on;
    PlotUtils.createSScPlot(-(PhasePingRange * Coef) .* cosd(BeamAngles), 'r');
    title('Z'); hold off;
    
    h(4) = subplot(5,2,4);
    Z = -(AmpPingRange(subAmp) * Coef) .* cosd(BeamAngles(subAmp));
    PlotUtils.createSScPlot(subAmp, Z-Draught, 'k+');
    hold on; grid on;
    Z = -(AmpPingRange(subAmpKO) * Coef) .* cosd(BeamAngles(subAmpKO));
    PlotUtils.createSScPlot(subAmpKO, Z, 'og');
    
    Z = -(PhasePingRange(subPh) * Coef) .* cosd(BeamAngles(subPh));
    PlotUtils.createSScPlot(subPh, Z-Draught, '+r');
    Z = -(PhasePingRange(subPhKO) * Coef) .* cosd(BeamAngles(subPhKO));
    PlotUtils.createSScPlot(subPhKO,  Z, 'og');
    title('Z'); hold off;
    
    h(5) = subplot(5,2,5);
    PlotUtils.createSScPlot(AmpPingQF, 'k'); grid on;
    hold on; grid on;
    PlotUtils.createSScPlot(PhasePingQF, 'r'); grid on;
    hold off; title('PingQF');
    %     set(h(5), 'YLim', [min([AmpPingQF PhasePingQF]) max([4, max(PhasePingQF), max(AmpPingQF)])])
    
    h(6) = subplot(5,2,6);
    PlotUtils.createSScPlot(subAmp, AmpPingQF(subAmp), 'k+'); grid on;
    hold on; grid on;
    PlotUtils.createSScPlot(subAmpKO, AmpPingQF(subAmpKO), 'og');
    PlotUtils.createSScPlot(subPh,  PhasePingQF(subPh), '+r'); grid on;
    PlotUtils.createSScPlot(subPhKO,  PhasePingQF(subPhKO), 'og');
    hold off; title('PingQF');
    set(h(6), 'YLim', [2 max([4, max(PhasePingQF(subPh)), max(AmpPingQF(subAmp))])])
    
    h(7) = subplot(5,2,7);
    PlotUtils.createSScPlot(AmpPingSamplesNb, 'k+'); grid on;
    title('AmpPingSamplesNb');
    
    h(8) = subplot(5,2,8);
    PlotUtils.createSScPlot(AmpPingQF2, 'k+'); grid on;
    hold off; title('AmpPingQF2');
    
    h(9) = subplot(5,2,9);
    PlotUtils.createSScPlot(PhasePingNbSamples, 'k+'); grid on;
    hold off; title('PhasePingNbSamples');
    
    h(10) = subplot(5,2,10);
    PlotUtils.createSScPlot(BeamAngles, 'k+'); grid on;
    hold off; title('BeamAngles');
    
    if ~isempty(iBeamBeg)
        for k=1:length(h)
            hold(h(k), 'on');
            plot(h(k), [iBeamBeg iBeamBeg], get(h(k), 'YLim'), 'g');
            plot(h(k), [iBeamEnd iBeamEnd], get(h(k), 'YLim'), 'g');
            hold(h(k), 'off');
        end
    end
    
    linkaxes(h, 'x')
    
    if SystemSerialNumber == 7111
        Correction = ResonCorrectionAntenna7111(BeamAngles, SampleRate);
        AmpPingRangeOnImage   = AmpPingRange - Correction;
        PhasePingRangeOnImage = PhasePingRange - Correction;
        PingRangeOnImage      = PingRange - Correction;
    else
        AmpPingRangeOnImage   = AmpPingRange;
        PhasePingRangeOnImage = PhasePingRange;
        PingRangeOnImage      = PingRange;
    end
    
    
    %     figure(12317);
    Fig = FigUtils.createSScFigure;
    ImageName = extract_ImageName(Amplitude);
    set(Fig, 'Name', ImageName)
    
    XAmp =  (AmpPingRange * Coef) .* sind(BeamAngles);
    ZAmp = -(AmpPingRange * Coef) .* cosd(BeamAngles);
    hc = PlotUtils.createSScPlot(XAmp(subAmp), ZAmp(subAmp)-Draught, '+k');
    set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
    hold on; grid on;
    PlotUtils.createSScPlot(XAmp(subAmpKO), ZAmp(subAmpKO)-Draught, 'og');
    
    XPh =  (PhasePingRange * Coef) .* sind(BeamAngles);
    ZPh = -(PhasePingRange * Coef) .* cosd(BeamAngles);
    hc = PlotUtils.createSScPlot(XPh(subPh), ZPh(subPh)-Draught, '+r');
    set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
    PlotUtils.createSScPlot(XPh(subPhKO), ZPh(subPhKO)-Draught, 'og');
    
    if ~isempty(BottomDetectionFilterInfoMinDepth)
        W = axis;
        PlotUtils.createSScPlot(W(1:2), [-BottomDetectionFilterInfoMinDepth -BottomDetectionFilterInfoMinDepth], 'k-*');
        PlotUtils.createSScPlot(W(1:2), [-BottomDetectionFilterInfoMaxDepth -BottomDetectionFilterInfoMaxDepth], 'k-*');
    end
    
    if ~isempty(iBeamBeg)
        iBeamBeg = max(iBeamBeg, find(~isnan(XPh) | ~isnan(XAmp), 1, 'first'));
        if isnan(XPh(iBeamBeg))
            h = PlotUtils.createSScPlot([0 XAmp(iBeamBeg)], [0 ZAmp(iBeamBeg)-Draught], 'g');
        else
            h = PlotUtils.createSScPlot([0 XPh(iBeamBeg)], [0 ZPh(iBeamBeg)-Draught], 'g');
        end
        set(h, 'LineWidth', 3)
        set(h, 'Tag', 'BottomDetector')
        
        iBeamEnd = min(iBeamEnd, find(~isnan(XPh) | ~isnan(XAmp), 1, 'last'));
        if isnan(XPh(iBeamEnd))
            h = PlotUtils.createSScPlot([0 XAmp(iBeamEnd)], [0, ZAmp(iBeamEnd)-Draught], 'g');
        else
            h = PlotUtils.createSScPlot([0 XPh(iBeamEnd)], [0, ZPh(iBeamEnd)-Draught], 'g');
        end
        set(h, 'LineWidth', 3)
        set(h, 'Tag', 'BottomDetector')
        title('Bathy');
    end
    
    %% Plot des tol�rances (QF ramen� en �cart-type) autour de la d�tection
    
    dZsurZ = 10.^(-QualityFactor);
    Z1     = NaN(size(QualityFactor), 'single');
    Z2     = NaN(size(QualityFactor), 'single');
    Z1(subAmp) = ZAmp(subAmp) .* (1+2*dZsurZ(subAmp));
    Z2(subAmp) = ZAmp(subAmp) .* (1-2*dZsurZ(subAmp));
    Z1(subPh) = ZPh(subPh) .* (1+2*dZsurZ(subPh));
    Z2(subPh) = ZPh(subPh) .* (1-2*dZsurZ(subPh));
    
    Xp = NaN(size(Z1));
    Z1p = NaN(size(Z1));
    Z2p = NaN(size(Z1));
    Xp(subAmp) = XAmp(subAmp);
    Z1p(subAmp) = Z1(subAmp);
    Z2p(subAmp) = Z2(subAmp);
    PlotUtils.createSScPlot(Xp, Z1p, 'k');
    PlotUtils.createSScPlot(Xp, Z2p, 'k');
    Xp = NaN(size(Z1));
    Z1p = NaN(size(Z1));
    Z2p = NaN(size(Z1));
    Xp(subPh) = XPh(subPh);
    Z1p(subPh) = Z1(subPh);
    Z2p(subPh) = Z2(subPh);
    PlotUtils.createSScPlot(Xp, Z1p, 'r');
    PlotUtils.createSScPlot(Xp, Z2p, 'r');
    hold off;
end

if DisplayLevel == 3
    AmpImage      = Amplitude.Image(:,:);
    PhaseImage    = Phase.Image(:,:);
    ImageName = extract_ImageName(Phase);
    ScreenSize = get(0,'ScreenSize');
    Fig = figure(12349);
    if ~isempty(mov)
        set(Fig, 'Position', floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4);
    end
    
    hImage(1) = subplot(1,2,1);
    Amp_dB = reflec_Enr2dB(AmpImage);
    imagesc(Amp_dB); colormap(jet(256)); colorbar;
    title(sprintf('Bottom detection on %s', ImageName), 'Interpreter', 'none')
    hold on;
    PlotUtils.createSScPlot(abs(PhasePingRangeOnImage), 'b'); PlotUtils.createSScPlot(abs(AmpPingRangeOnImage), 'k');
    PlotUtils.createSScPlot(subAmp, abs(PingRangeOnImage(subAmp)), '+k');
    PlotUtils.createSScPlot(subPh, abs(PingRangeOnImage(subPh)), '+b');
    hold off;
    
    hImage(2) = subplot(1,2,2);
    imagesc(-abs(PhaseImage), [-180 0]); colormap(jet(256)); colorbar;
    title(sprintf('Bottom detection on %s', ImageName), 'Interpreter', 'none')
    hold on;
    PlotUtils.createSScPlot(abs(PhasePingRangeOnImage), 'b'); PlotUtils.createSScPlot(abs(AmpPingRangeOnImage), 'k');
    PlotUtils.createSScPlot(subAmp, abs(PingRangeOnImage(subAmp)), '+k');
    PlotUtils.createSScPlot(subPh, abs(PingRangeOnImage(subPh)), '+b');
    hold off;
    
    linkaxes(hImage, 'x')
    
    if ~isempty(mov)
        I =  getframe(Fig);
        I = I.cdata;
        size(I)
        % OLD mov = addframe(mov, I);
        writeVideo(mov,I); % FORGE 353
    end
end
drawnow
