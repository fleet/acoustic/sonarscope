
function [flag, Range, Eqm, Rampe, mov] = sonar_ResonDetectionPhase_unitaire(Phase, Amplitude, ...
    DisplayLevel, BeamsToDisplay, varargin)

[varargin, mov] = getPropertyValue(varargin, 'Movie', []); %#ok<ASGLU>

%% Vérification des signatures

DataType = cl_image.indDataType('TxAngle');
flag = testSignature(Phase, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

%% Paramètres optionels

% [subx, suby, varargin] = getSubxSuby(Phase, varargin); %#ok<ASGLU>%%

%% Algorithme

BeamAngles          = Amplitude.Sonar.SampleBeamData.TxAngle;
R0                  = Amplitude.Sonar.SampleBeamData.R0;
SoundVelocity       = Amplitude.Sonar.SampleBeamData.SoundVelocity;
SampleRate          = Amplitude.Sonar.SampleBeamData.SampleRate;
Frequency           = Amplitude.Sonar.SampleBeamData.Frequency;
SystemSerialNumber  = Amplitude.Sonar.SampleBeamData.SystemSerialNumber;
PingNumber          = Amplitude.Sonar.SampleBeamData.PingCounter;

Amp = Amplitude.Image(:,:);
Ph  = Phase.Image(:,:);

[Range, NbSamplesPhase, Rampe, Eqm, QualityFactorPhase] = ResonDetectionPhaseImage(Amp, Ph, BeamAngles, R0, SoundVelocity, SampleRate, ...
    Frequency, SystemSerialNumber, PingNumber, DisplayLevel, BeamsToDisplay);

if DisplayLevel >= 2
    ImageName = extract_ImageName(Phase);
    
    if SystemSerialNumber == 7111
        Correction = ResonCorrectionAntenna7111(BeamAngles, SampleRate);
        RangeOnImage = Range - Correction;
    else
        RangeOnImage = Range;
    end
    
    Fig = figure(12349);
    set(Fig, 'Name', 'Phase detection & Quality Factor (Image)')
    if ~isempty(mov)
        ScreenSize = get(0,'ScreenSize');
        set(Fig, 'Position', floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4);
    end
    
    h(1) = subplot(4,1,1:3);
    imagesc(Phase.Image(:,:)); colormap(jet(256)); colorbar;
    title(sprintf('Phase detection on %s', ImageName), 'Interpreter', 'none')
    hold on; PlotUtils.createSScPlot(abs(RangeOnImage), 'ok'); hold off;
    h(2) = subplot(4,1,4);
    PlotUtils.createSScPlot(QualityFactorPhase, 'ok'); grid on; title('Quality Factor');
    hc = colorbar; set(hc, 'Visible', 'Off')
    linkaxes(h, 'x')
    
    if ~isempty(mov)
        I =  getframe(Fig);
        I = I.cdata;
        size(I)
        % OLD mov = addframe(mov, I);
        writeVideo(mov,I); % FORGE 353
    end
    
    Fig = figure(12343);
    set(Fig, 'Name', 'Phase detection & Quality Factor (Signals)')
    h(1) = subplot(4,2,1); PlotUtils.createSScPlot(abs(Range), 'k'); grid on; title('Range')
    set(h(1), 'YDir', 'Reverse')
    h(2) = subplot(4,2,2); PlotUtils.createSScPlot(BeamAngles, 'k'); grid on; title('BeamAngles')
    h(3) = subplot(4,2,3); PlotUtils.createSScPlot(Eqm, 'k'); grid on; title('Eqm')
    h(4) = subplot(4,2,5); PlotUtils.createSScPlot(Rampe, 'k'); grid on; title('Pente')
    h(5) = subplot(4,2,4); PlotUtils.createSScPlot(-2*Range.*cosd(BeamAngles), 'k'); grid on; title('Pseudo-Bathy')
    h(6) = subplot(4,2,6); PlotUtils.createSScPlot(NbSamplesPhase, 'k'); grid on; title('NbSamplesPhase')
    h(7) = subplot(4,2,7); PlotUtils.createSScPlot(QualityFactorPhase, 'k'); grid on; title('QualityFactorPhase')
    linkaxes(h, 'x')
    drawnow
end
