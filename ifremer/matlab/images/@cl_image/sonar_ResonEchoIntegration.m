function [flag, b, Carto] = sonar_ResonEchoIntegration(this, typePolarEchogram, nomFicXmlEchogram, params_ZoneEI, Tag, StepDisplay, varargin)

[varargin, nomFicS7k] = getPropertyValue(varargin, 'nomFicS7k', []);
[varargin, suby]      = getPropertyValue(varargin, 'suby',      []);
[varargin, Carto]     = getPropertyValue(varargin, 'Carto',     []); %#ok<ASGLU>

b = [];

if isempty(nomFicS7k)
    nomFicS7k = this.InitialFileName;
    flag = exist(nomFicS7k, 'file');
    if ~flag
        return
    end
end

%% Lecture des fichiers binaires des signaux

[~, ~, Ext] = fileparts(nomFicXmlEchogram);
if strcmp(Ext, '.nc')
% 	[flag, DataRaw, groupNames, sliceNames, layerUnits, MaskSignification] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXmlEchogram, 'Light', true);
	[flag, DataRaw, groupNames] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXmlEchogram, 'Light', true);
%     nbPings = length(DataRaw.Datetime);
    nbPings = size(DataRaw.elevation, 3);
    subPingEchogram = str2double(groupNames);
    DataEchogram = DataRaw;
    DataEchogram.iPing = subPingEchogram;
    DataEchogram.xBab = DataRaw.across_dist_L;
    DataEchogram.xTri = DataRaw.across_dist_R;
    DataEchogram.Depth = squeeze(DataRaw.elevation(2,1,:));
    DataEchogram.Dimensions.nbPings = nbPings;
else
    [flag, DataEchogram] = XMLBinUtils.readGrpData(nomFicXmlEchogram, 'Memmapfile', -1);
    nbPings = DataEchogram.Dimensions.nbPings;
%     subPingEchogram = DataEchogram.iPing(:);
end
if ~flag
    return
end

if nbPings == 0
    str1 = 'Pas de donn�es disponibles';
    str2 = 'No data available';
    my_warndlg(Lang(str1,str2), 0);
    return
end


[nomDirRaw, nomFicRaw] = fileparts(nomFicXmlEchogram);
nomDirRaw = fullfile(nomDirRaw, nomFicRaw);

if DataEchogram.Dimensions.nbPings == 0
    str1 = 'Pas de donn�es disponibles';
    str2 = 'No data available';
    my_warndlg(Lang(str1,str2), 0);
    return
end

if isempty(nomFicS7k)
    nomFicS7k = this.InitialFileName;
    flag = strcmp(nomFicS7k, 'SIMRAD');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''est pas un fichier SIMRAD.', nomFicS7k);
        str2 = sprintf('"%s" is not a SIMRAD file.', nomFicS7k);
        my_warndlg(Lang(str1,str2), 1);
        b = [];
        return
    end
    flag = exist(nomFicS7k, 'file');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''existe pas.', this.InitialFileFormat);
        str2 = sprintf('"%s" does not exist.', this.InitialFileFormat);
        my_warndlg(Lang(str1,str2), 1);
        b = [];
        return
    end
end

b = [];

s7k = cl_reson_s7k('nomFic', nomFicS7k, 'KeepNcID', true);
if isempty(s7k)
    flag = false;
    return
end

%% Lecture des layers

[c, Carto] = view_depth(s7k, 'ListeLayers', [1 2 3 4 8 30], 'Carto', Carto, 'Bits', [1 2 6]);
if length(c) < 3
    [c, Carto] = view_depth(s7k, 'ListeLayers', [4 6], 'Carto', Carto, 'Bits', [1 2 6]);
    [flag, d] = sonar_range2depth_EM(c(2), c(1), [], [], [], 'nbPlotPerImage', 0);
    if length(d) < 5
        str1 = 'L''�cho-integration ne peut se faire car la bathym�trie n''existe pas dans ce fichier .';
        str2 = 'The echointegration cannot be produced because there is no bathymetry in this data.';
        my_warndlg(Lang(str1,str2), 1);
        flag = false;
        return
    else
        % TODO : ces donn�es n'auront pas de navigation. Il sera possible
        % d'importer la nav manuellement dans les fichiers EI. Il serait
        % pr�f�rable de poser la question "Fichier de vav au cas o� mais �a
        % risque de ne pas servir tr�s souvent. A voir avec Carla
        Depth       = d(5);
        aAcrossDist = d(4);
        aAlongDist  = d(3);
    end
else
    Depth       = c(1);
    aAcrossDist = c(2);
    aAlongDist  = c(3);
end
nbPings = aAcrossDist.nbRows;

if isempty(suby)
    subPingDataDepth = 1:nbPings;
else
    subPingDataDepth = suby;
end

% Avant modif JMA le 05/01/2021
% subPingEchogram = DataEchogram.iPing(:);
% [subEchogram, subDepth] = intersect3(subPingEchogram, subPingDataDepth, subPingDataDepth);

% Apr�s modif JMA le 05/01/2021
T1 = DataEchogram.Datetime;
T2 = datetime(aAcrossDist.Sonar.Time.timeMat, 'ConvertFrom', 'datenum');
[subEchogram, subDepth] = intersect3_datetime(T1, T2, T2);
if isempty(subEchogram)
    flag = false;
    return
end
subDepth = subPingDataDepth(subDepth);
% subAcross = subDepth; % TODO : gestion diff�rente pour Kongsberg : faire l'analyse  

%% Boucle sur les �chogrammes

AntennaAcrossDist = 0;
[EI, Unit] = sonar_EchoIntegrationOverSpecularOrSeabed(aAcrossDist, aAlongDist, Depth, ...
    typePolarEchogram, DataEchogram, nomDirRaw, nomFicRaw, subEchogram, subDepth, ...
    params_ZoneEI, AntennaAcrossDist, ...
    StepDisplay, 'EchogramFileName', nomFicXmlEchogram);

%% Compute PingAcrossDist images

[xEIImage, EIReflecImage, EINbPointsImage, EIAnglesImage, EIAlongDistImage] = ...
    EI_Compute_Images(EI);

%% Write PingAcrossDist images on disk

SonarPortMode_1 = get(aAcrossDist, 'SonarPortMode_1');
b = EI_write_Images(aAcrossDist, xEIImage, typePolarEchogram, ...
    Tag, Carto, nomFicS7k, nomFicRaw, SonarPortMode_1, ...
    EIReflecImage, EINbPointsImage, EIAnglesImage, EIAlongDistImage, Unit, ...
    params_ZoneEI.Where);
% SonarScope(b)

% TODO : message de fin ?