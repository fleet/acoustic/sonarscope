% Calcul de l'aire insonifiee de la bathymetrie
%
% Syntax
%   [flag, b] = sonar_ResonMasqueAmplitude(a)
%
% Input Arguments
%   a : Instance de cl_image
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance de cl_image contenant l'image filtree
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = sonar_ResonMasqueAmplitude(this, Phase, GateDepth, IdentAlgo, DisplayLevel, varargin)

parameters_BDAgetPlatform('Default');

nbImages = length(this);
for k=1:nbImages
    if isempty(Phase)
        [flag, pppp]  = sonar_ResonMasqueAmplitude_unitaire(this(k), [], GateDepth, IdentAlgo, DisplayLevel, varargin{:});
    else
        [flag, pppp]  = sonar_ResonMasqueAmplitude_unitaire(this(k), Phase(k), GateDepth, IdentAlgo, DisplayLevel, varargin{:});
    end
    
    if ~flag
        that = [];
        return
    end
    that(:,k) = pppp; %#ok<AGROW>
end
that = that(:);


function [flag, this] = sonar_ResonMasqueAmplitude_unitaire(this, Phase, GateDepth, IdentAlgo, DisplayLevel, varargin)

%% V�rification des signatures

DataType = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

%% Param�tres optionels

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithme

iPing = 1;
Draught = get_DraughtShip;
% this = sonar_detectionAmplitudeOnReducedMatrix(this, GateDepth, Draught, DisplayLevel, iPing);
DataPing = this.Sonar.SampleBeamData;
AmpImage = this.Image(:,:);
Frequency = get(this.Sonar.Desciption, 'Sonar.Freq');
BeamAngles = DataPing.TxAngle;

BottomDetectDepthFilterFlag         = DataPing.BottomDetectDepthFilterFlag;
BottomDetectionFilterInfoMinDepth   = DataPing.BottomDetectionFilterInfoMinDepth;
BottomDetectionFilterInfoMaxDepth   = DataPing.BottomDetectionFilterInfoMaxDepth;
PulseDuration                       = DataPing.TxPulseWidth; % s
SampleRate                          = DataPing.SampleRate; % Hz
ReceiveBeamWidth                    = DataPing.ReceiveBeamWidth * (180/pi); % deg
PingSequence                        = DataPing.PingSequence;
SystemSerialNumber                  = DataPing.SystemSerialNumber;
ProjectionBeam_3dB_WidthVertical    = DataPing.ProjectionBeam_3dB_WidthVertical;

[DepthMinPing, DepthMaxPing] = BDA_gates(GateDepth, BottomDetectDepthFilterFlag, BottomDetectionFilterInfoMinDepth, BottomDetectionFilterInfoMaxDepth, Draught);

[R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, AmpPingNormHorzMax, MaskWidth, iBeamBeg, iBeamEnd] = ...
    BDA_detectionAmplitudeOnReducedMatrix(AmpImage, BeamAngles, Frequency, ...
    DepthMinPing, DepthMaxPing, ...
    PulseDuration, SampleRate, ReceiveBeamWidth, PingSequence, SystemSerialNumber, ProjectionBeam_3dB_WidthVertical, ...
    DisplayLevel, iPing, IdentAlgo);

DataPing.R0                 = R0;
DataPing.RMax1              = RMax1;
DataPing.RMax2              = RMax2;
DataPing.R1SamplesFiltre    = R1SamplesFiltre;
DataPing.iBeamMax0          = iBeamMax0;
DataPing.AmpPingNormHorzMax = AmpPingNormHorzMax;
DataPing.MaskWidth          = MaskWidth;
DataPing.iBeamBeg           = iBeamBeg;
DataPing.iBeamEnd           = iBeamEnd;
nbBeams = length(R1SamplesFiltre);
DataPing.TransmitSectorNumber = ones(1,nbBeams);
DataPing.TransmitSectorNumber(floor(nbBeams/2):end) = 2;

this.Sonar.SampleBeamData = DataPing;

if isnan(this.Sonar.SampleBeamData.R0)
    str1 = 'Op�ration impossible car le pr�traitement du masque a �chou�.';
    str2 = 'This process is not possible because preprocessing of Mask failed.';
    %     my_warndlg(Lang(str1,str2), 0, 'Tag', 'ResonMasqueAmplitudeR0NaN');
    my_warndlg(Lang(str1,str2), 1);
    return
end

Amp                 = this.Image(:,:);
Teta                = this.Sonar.SampleBeamData.TxAngle;
% ReceiveBeamWidth    = this.Sonar.SampleBeamData.ReceiveBeamWidth * (180/pi);
iBeamMax0           = this.Sonar.SampleBeamData.R0;
% AmpPingNormHorzMax  = this.Sonar.SampleBeamData.AmpPingNormHorzMax;
MaskWidth           = this.Sonar.SampleBeamData.MaskWidth;
% DataTVG         = this.Sonar.SampleBeamData.DataTVG;
iPing = 0;

NbSamplesForPulseLength = nbSamplesForPulseLength(this.Sonar.SampleBeamData.SampleRate, ...
    this.Sonar.SampleBeamData.TxPulseWidth);

% Pr�paration param�tre pour meilleur centrage des fen�tres de rejection du sp�culaire
% Immersion = -5.4 % A r�cup�rer de PDS
% Immersion = -get_DraughtShip;
% NbSamplesForImmersionAntenna = abs(Immersion) * this.Sonar.SampleBeamData.SampleRate / 1500;

RMax1 = this.Sonar.SampleBeamData.RMax1;
RMax2 = this.Sonar.SampleBeamData.RMax2;
R1SamplesFiltre = this.Sonar.SampleBeamData.R1SamplesFiltre;

Masque = ResonMasqueAmplitude(this.Sonar.SampleBeamData.SystemSerialNumber, Amp, Teta, ...
    iBeamMax0, RMax1, RMax2, R1SamplesFiltre, ...
    NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, IdentAlgo);

Amp(~Masque) = NaN;
this = replace_Image(this, Amp);

%% Mise a jour des coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
this.CLim = [0 1];


%% D�finition des attributs de l'image

this.Unit = ' ';
this.ColormapIndex = 3;
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

Append = 'MaskAmp';
this = update_Name(this, 'Append', Append);
% this = update_Name(this);

%% D�termination du masque

this(2) = replace_Image(this(1), Masque);
this(2) = compute_stats(this(2));
this(2).TagSynchroContrast = num2str(rand(1));
this(2).CLim               = [0 1];
this(2).Unit               = ' ';
this(2).ColormapIndex      = 3;
this(2).DataType           = cl_image.indDataType('Mask');
this(2) = update_Name(this(2));

%% Masquage de la phase

if ~isempty(Phase)
    Ph = Phase.Image(:,:);
    Ph(~Masque) = NaN;
    this(3) = replace_Image(Phase, Ph);
    this(3).DataType = cl_image.indDataType('TxAngle');
    this(3).Unit     = 'Deg';
    this(3) = compute_stats(this(3));
    this(3).TagSynchroContrast = num2str(rand(1));
    this(3).CLim = [this(3).StatValues.Min this(3).StatValues.Max];
    Append = 'MaskAmp';
    this(3) = update_Name(this(3), 'Append', Append);
    
    this = this([1 3 2]);
end

flag = 1;


%{
%% Determination of maximum of amplitude

function A_FiltreNorm = SampleBeam_AmplitudeNormalisation(Amp)

nbBeams   = size(Amp,2);
nbSamples = size(Amp,1);

Moitie = floor(nbBeams/2);
subBab = 1:Moitie;
subTri = (Moitie+1):nbBeams;

window = [7 3];
h = ones(window) / prod(window);
A_Filtre = filter2(h, Amp, 'same');

A_FiltreNorm = A_Filtre;
for k=1:nbSamples
y = A_Filtre(k,subBab);
Max = max(y);
y = y / Max;
A_FiltreNorm(k,subBab) = y;

y = A_Filtre(k,subTri);
Max = max(y);
y = y / Max;
A_FiltreNorm(k,subTri) = y;
end

M = mean(A_FiltreNorm, 2);
H = calculer_hauteur_signal1(1-M, [], [], 0);
if ~isnan(H)
A_FiltreNorm(1:(H-1),:) = NaN;
end
%}
