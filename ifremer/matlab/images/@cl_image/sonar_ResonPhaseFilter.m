% Filtrage d'une image de phase au format RangeBeam
%
% Syntax
%  b = sonar_ResonPhaseFilter(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a);
%
%   b = abs(a);
%   imagesc(b);
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_ResonPhaseFilter(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, this(i)] = sonar_ResonPhaseFilter_unitaire(this(i), varargin{:});
    if ~flag
        return
    end
end


function [flag, this] = sonar_ResonPhaseFilter_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% V�rification des signatures

DataType = cl_image.indDataType('TxAngle');
flag = testSignature(this, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

%% Appel de la fonction de filtrage

X = ResonPhaseFilter(this.Image(suby,subx));

this = replace_Image(this, X);

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Filter');

flag = 1;
