function [flag, Carto] = sonar_ResonProcessWCPhase(~, FileName, varargin)

[varargin, subBeams]            = getPropertyValue(varargin, 'subBeams',            []);
[varargin, subPings]            = getPropertyValue(varargin, 'subPings',            []);
[varargin, StepCompute]         = getPropertyValue(varargin, 'StepCompute',         1);
[varargin, TypeWCOutput]        = getPropertyValue(varargin, 'TypeWCOutput',        1);
[varargin, Survey]              = getPropertyValue(varargin, 'Survey',              []);
[varargin, Names]               = getPropertyValue(varargin, 'Names',               []);
[varargin, StepDisplay]         = getPropertyValue(varargin, 'StepDisplay',         1);
[varargin, Carto]               = getPropertyValue(varargin, 'Carto',               1);
[varargin, nomFicNav]           = getPropertyValue(varargin, 'nomFicNav',           []);
[varargin, nomFicIP]            = getPropertyValue(varargin, 'nomFicIP',            []);
[varargin, MaskBeyondDetection] = getPropertyValue(varargin, 'MaskBeyondDetection', 1);
[varargin, flagTideCorrection]  = getPropertyValue(varargin, 'TideCorrection',      0);
[varargin, MaxImageSize]        = getPropertyValue(varargin, 'MaxImageSize',        1000);
[varargin, CLim]                = getPropertyValue(varargin, 'CLim',                [-90 90]);
[varargin, statusDepthAndAcrossDistLimits] = getPropertyValue(varargin, 'statusDepthAndAcrossDistLimits', 1); %#ok<ASGLU>

%% Garbage Collector de Java

java.lang.System.gc

%% Lecture des donn�es

s7k = cl_reson_s7k('nomFic', FileName, 'KeepNcID', true);
if isempty(s7k)
    flag = false;
    return
end
identSondeur = selectionSondeur(s7k);

%% Cr�ation du fichier InstallationParameters

InstallationParameters = read_sonarInstallationParameters(s7k, identSondeur);
if isfield(InstallationParameters, 'WaterLineVerticalOffset')
    write_installationParameters_bin(s7k, identSondeur);
end

%% Lecture des donn�es PingBeam

ProfilCelerite = read_soundSpeedProfile(s7k, identSondeur);

[flag, DataDepth] = read_depth_s7k(s7k, 'identSondeur', identSondeur);
if ~flag
    return
end

if size(DataDepth.TransducerDepth,1) == 1
    DataDepth.TransducerDepth = DataDepth.TransducerDepth(:);
end

%% Lecture de la navigation

Navigation = [];
if isempty(nomFicNav)
    if isempty(DataDepth.Latitude)
        str1 = 'On ne peut pas visualiser les donn�es de colonne d''eau dans GLOBE si il n''y a pas de navigation.';
        str2 = 'It is impossible to display the WC data in GLOBE if there is no navigation.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    else
        TimeNav = DataDepth.Time;
        LatNav  = DataDepth.Latitude;
        LonNav  = DataDepth.Longitude;
        HeadingNav = DataDepth.Heading;
        
        Navigation.LatNav     = LatNav;
        Navigation.LonNav     = LonNav;
        Navigation.TimeNav    = TimeNav;
        Navigation.HeadingNav = HeadingNav;
    end
else
    if iscell(nomFicNav)
        if length(nomFicNav) > 1
            str1 = 'Il n''est pas encore pr�vu de pouvoir utiliser plusieurs fichiers de nav en m�me temps, d�sol�. Si vous avez vraiment besoin de cela faites moi en la demande, sign� JMA.';
            str2 = 'It is not possible to use several navigation files for the moment. If you really need it, please tell me, signed JMA.';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        nomFicNav = nomFicNav{1};
    end
    
    [flag, DataNav] = import_nav(nomFicNav);
    if ~flag
        return
    end
    TimeNav = DataNav.Time;
    LatNav  = DataNav.Nav.Lat;
    LonNav  = DataNav.Nav.Lon;
    HeadingNav = DataNav.Nav.Heading;
    
    Navigation.LatNav     = LatNav;
    Navigation.LonNav     = LonNav;
    Navigation.TimeNav    = TimeNav;
    Navigation.HeadingNav = HeadingNav;
    
    % figure; plot(LonNav, LatNav); grid on
end

%% Lecture du fichier "InstallationParameters"

[flag, InstallationParameters] = get_InstallationParameters(s7k, InstallationParameters, nomFicIP);
if ~flag
    return
end

%% Lecture des donn�es de la colonne d'eau

% [flag, DataWC] = read_WaterColumn_bin(s7k, 'Memmapfile', 1);
% if ~flag
%     str1 = sprintf('Il n''y a pas de datagrammes de Water Column dans le fichier "%s".', FileName);
%     str2 = sprintf('No Water Column datagrams in file "%s".', FileName);
%     my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
%     return
% end
% Mag & Phase

[DataWC, BeamData] = read_BeamRawData(s7k, identSondeur, 'DataDepth', DataDepth, 'subPing', 1, 'DisplayLevel', 0);
if isempty(BeamData)
    flag = 0;
    return
end

%{
[flag, IdentSonar] = DeviceIdentifier2IdentSonar(s7k, DataDepth.SystemSerialNumber);
if ~flag
msg = sprintf('Sondeur %d pas inclus dans cl_reson_s7k/view_depth', DataDepth.SystemSerialNumber);
my_warndlg(msg, 1);
return
end
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
'Sonar.SystemSerialNumber', DataDepth.SystemSerialNumber);
%}

if isempty(subPings)
    subPings = 1:StepCompute:length(DataWC.PingCounter);
else
    subPings = subPings(1):StepCompute:subPings(end);
end
nbBeams = size(DataDepth.Range, 2);

if isempty(subBeams)
    subBeams = 1:nbBeams;
end

%% Recherche des pings communs entre sources de donn�es

% subPingFichier   = DataWC.PingCounter(:);
% subPingDataDepth = DataDepth.PingCounter(subPings);
% [subWC, subDepth] = intersect3(subPingFichier, subPingDataDepth, subPingDataDepth);
% if isempty(subWC)
%     flag = false;
%     return
% end
% nbPings = length(subDepth);

subDepth = DataWC.sub7000;
subWC = DataWC.subWC;
[~, ia] = intersect(subDepth, subPings);
if isempty(ia)
    flag = false;
    return
end
subDepth = subDepth(ia);
subWC    = subWC(ia);
nbPings  = length(subDepth);

%% Recherches des bornes des images si export pour Voxler

% [flag, DataPosition] = read_position_bin(s7k, 'Memmapfile', 1);
% if ~flag
%     return
% end
% DataPosition = read_navigation(s7k, identSondeur);

TimeWC = DataWC.Time;
TimeWC = TimeWC(subWC);

if isa(TimeWC, 'cl_time')
    TimeWC = TimeWC.timeMat;
end

subNonNan = find(~isnan(TimeWC));
subNan    = find(isnan(TimeWC));
TimeWC(subNan) = interp1(subNonNan, TimeWC(subNonNan), subNan, 'linear', 'extrap');
TimeNav = TimeNav.timeMat;

if (TimeNav(1) > TimeWC(end)) || (TimeWC(1) > TimeNav(end))
    %     Latitude  = zeros(nbPings,1);
    %     Longitude = zeros(nbPings,1);
    %     Heading   = zeros(nbPings,1);
    
    str1 = 'Heure des pings / heures du fichier de navigation';
    str2 = 'Tipe of pings / navigation';
    FigTime = figure; plot(cl_time('timeMat', TimeWC), zeros(size(TimeWC)), 'r*');
    hold on; plot(cl_time('timeMat', TimeNav), ones(size(TimeNav)), 'b*'); hold off; grid on;
    title(Lang(str1,str2)); set(gca, 'YLim', [-1 2]);
    legend({FileName; nomFicNav}, 'Interpreter', 'none')
    
    str1 = sprintf('La navigation n''est pas d�finie pour des pings du fichier "%s", on passe ce fichier.\nLa figure %d illustre les temps', FileName, FigTime);
    str2 = sprintf('The navigation is not defined for some pings of file "%s", I skip this file.\nFig %d display the times', FileName, FigTime);
    my_warndlg(Lang(str1,str2), 0);
    
    flag = false;
    return
else
    Latitude  = my_interp1(           TimeNav, LatNav,     TimeWC, 'linear', 'extrap');
    Longitude = my_interp1_longitude( TimeNav, LonNav,     TimeWC, 'linear', 'extrap');
    Heading   = my_interp1_headingDeg(TimeNav, HeadingNav, TimeWC, 'linear', 'extrap');
    % Latitude  = Latitude(subWC);
    % Longitude = Longitude(subWC);
    % Heading   = Heading(subWC);
end

if TypeWCOutput == 1 % Images individuelles
    across = [];
    depth  = [];
else % Matrices 3D
    % On ne peut faire cette partie pour l'instant tant que l'on
    % aura pas fait la liaison entre .s7k SkCenter et .s7k PDS
    % (DataDepth.Depth est vide)
    if isempty(statusDepthAndAcrossDistLimits)
        minDepth = min(min(DataDepth.Depth(subDepth,:)));
        minAcrossDist = min(min(DataDepth.AcrossDist(subDepth,:)));
        maxAcrossDist = max(max(DataDepth.AcrossDist(subDepth,:)));
        minDepth      = double(minDepth);
        minAcrossDist = double(minAcrossDist);
        maxAcrossDist = double(maxAcrossDist);
        
        if isnan(minDepth)
            minDepth      = -1000;
            minAcrossDist = -2200;
            maxAcrossDist =  2200;
        end
        
        [flag, minAcrossDist, maxAcrossDist, minDepth] = question_WCLimits3DMatrix(minAcrossDist, maxAcrossDist, 'minDepth', minDepth);
        if ~flag
        return
        end
    else
        minDepth      = statusDepthAndAcrossDistLimits.minDepth;
        minAcrossDist = statusDepthAndAcrossDistLimits.minAcrossDist;
        maxAcrossDist = statusDepthAndAcrossDistLimits.maxAcrossDist;
    end
    
    Hauteur = abs(minDepth);
    Largeur = maxAcrossDist - minAcrossDist;
    Step = sqrt((Hauteur * Largeur)) / MaxImageSize;
    across = minAcrossDist:Step:maxAcrossDist;
    depth = -Hauteur:Step:0;
    across = centrage_magnetique(across);
    depth  = centrage_magnetique(depth);
end

if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

%% Affichage des �chogrammes polaires

Z = DataDepth.Depth(subDepth(1):subDepth(end), subBeams);
if all(isnan(Z(:)))
    Z = -1000 * (DataDepth.Range(subDepth(1):subDepth(end), subBeams) .* cosd(DataDepth.BeamDepressionAngle(subDepth(1):subDepth(end), subBeams)));
end
% [Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUI(Z, subBeams, subDepth, StepDisplay);
[Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUInew(Z(:, :), StepDisplay); % Modif JMA le 02/10/2020

%% Cr�ation des r�pertoires

[nomDirExportPhase, nomFic] = fileparts(Names.nomFicXmlPhase);
nomDirExportPhase = fullfile(nomDirExportPhase, nomFic);
if exist(nomDirExportPhase, 'dir')
    rmdir(nomDirExportPhase,'s')
end
WcPhaseXmlFilename = Names.nomFicXmlPhase;

%% Initialisation des tableaux

nbAcross = length(across);
StructWcXmlPhase = WC_InitStructXML(nbPings, nbAcross);

%% Boucle sur les pings

firstDisplay = 1;
kWC = 0;
AtLeastOneWCFound = 0;
WcPhaseUnit = 'deg';

str1 = sprintf('Reson Water Column Phase : Traitement des pings de %s', FileName);
str2 = sprintf('Reson Water Column Phase : Processing pings of %s', FileName);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    fprintf('%5d/%5d : %s\n', k, nbPings, FileName);
    
    TimePing = DataDepth.Time(subDepth(k));
    
    [b_Amp, Carto] = view_BeamRawData(s7k, 'subPing', subWC(k), 'Carto', Carto, 'DisplayLevel', 0, 'InstallationParameters', InstallationParameters, 'Navigation', Navigation);
    if isempty(b_Amp)
        continue
    end
    b_phase = b_Amp(2);
    b_Amp   = b_Amp(1);
    AtLeastOneWCFound = 1;

    % TransmitSectorNumber = ones(1, nbBeams, 'single');
    TransmitSectorNumber = ones(1, min(nbBeams,b_Amp.nbColumns), 'single'); % Modif JMA BICOSE
    b_dB = reflec_Amp2dB(b_Amp(1));
    b_dB_Comp  = compensation_WaterColumn(b_dB,  TransmitSectorNumber);
    
    SampleBeamData = get_SonarSampleBeamData(b_Amp);
    DetectedRangeInSamples = DataDepth.Range(subDepth(k),:) * SampleBeamData.SampleRate;
    
    DetectedRangeInSamples = floor(DetectedRangeInSamples);
    if all(isnan(DetectedRangeInSamples))
        continue
    end
    
    [flag, c_Amp, c_phase, Detection, Mask] = sonar_polarEchogram(b_dB_Comp,  ProfilCelerite, 'otherImage', b_phase, ...
        'RangeDetectionInSamples', DetectedRangeInSamples, 'typeAlgoWC', 2, ...
        'across', across, 'depth', depth, 'MaxImageSize', MaxImageSize, 'MaskBeyondDetection', MaskBeyondDetection);
    %         'DataDepthAcrossDist', DataDepth.AcrossDist(subDepth(k),:), 'DataDepthAlongDist', DataDepth.AlongDist(subDepth(k),:));
    if ~flag
        continue
    end
    
    d_Out = c_Amp(2);
    if MaskBeyondDetection
        WcOutPhase = c_phase(2);
    else
        WcOutPhase = c_phase(1);
    end
    WcOutPhase = abs(WcOutPhase);
    
    CLimOutPhase = CLim;
    CLimOutComp  = [0 30];
    
    d_Out.CLim = CLimOutComp;
    
    %% Affichage des echogrammes
    
    if (StepDisplay ~= 0) && (mod(k,StepDisplay) == 0)
        TimePing = DataDepth.Time(subDepth(k));
        [Fig, hAxe, hImage, hPing, hCurve, firstDisplay] = WC_DisplayResults(Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
            TimePing, subDepth(k), d_Out, WcOutPhase, Detection, CLimOutComp, CLimOutPhase);
    end
    
    d_Out.Sonar.SampleBeamData.Latitude       = Latitude(k);
    WcOutPhase.Sonar.SampleBeamData.Latitude  = Latitude(k);
    d_Out.Sonar.SampleBeamData.Longitude      = Longitude(k);
    WcOutPhase.Sonar.SampleBeamData.Longitude = Longitude(k);
    d_Out.Sonar.SampleBeamData.Heading        = Heading(k);
    WcOutPhase.Sonar.SampleBeamData.Heading   = Heading(k);
    d_Out.Sonar.SampleBeamData.Time           = TimePing;
    WcOutPhase.Sonar.SampleBeamData.Time      = TimePing;
    
    if ~isempty(InstallationParameters)
        d_Out      = set_ResonInstallationParameters(d_Out,      InstallationParameters);
        WcOutPhase = set_ResonInstallationParameters(WcOutPhase, InstallationParameters);
        d_Out.Sonar.SampleBeamData.AlongDist(:)      = InstallationParameters.TransmitArrayY;
        WcOutPhase.Sonar.SampleBeamData.AlongDist(:) = InstallationParameters.TransmitArrayY;
    else
        d_Out.Sonar.SampleBeamData.AlongDist(:) = 35.45; % 7150
        WcOutPhase.Sonar.SampleBeamData.AlongDist(:) = 35.45; % 7150
    end
        
    %% Ecriture des echogrammes sur disque
    
    kPing           = subDepth(k);
    kWCPing         = kWC+1;
    TransducerDepth = DataDepth.TransducerDepth(kPing,1);
        
    [flagOK, StructWcXmlPhase] = WC_WriteDepthAcrossDistEchograms(...
        WcOutPhase, StructWcXmlPhase, nomDirExportPhase, CLimOutPhase, ...
        TypeWCOutput, TransducerDepth, MaskBeyondDetection, Mask, ...
        kPing, TimePing, kWCPing, Latitude(k), Longitude(k), Heading(k), nbAcross, ...
        flagTideCorrection, Detection);
    % TODO : rajouter 'Heave, d_Out.Sonar.SampleBeamData.Heave
    
    %% Sauvegarde interm�diaire du fichier XML

    if flagOK
        if mod(kWC+2, 100) == 1 % Ecriture interm�diaire du fichier XML sur disque
            WC_write_PolarEchogramsXML(StructWcXmlPhase, kWC, FileName, TypeWCOutput, WcPhaseXmlFilename, ...
                across, depth, Survey, CLimOutPhase, WcPhaseUnit);
        end
        kWC = kWC + 1;
    end
end
my_close(hw, 'MsgEnd');
my_close(Fig)
if ~AtLeastOneWCFound
    flag = 0;
    return
end

%% Ecriture du fichier XML sur disque

WC_write_PolarEchogramsXML(StructWcXmlPhase, nbPings, FileName, TypeWCOutput, WcPhaseXmlFilename, ...
    across, depth, Survey, CLimOutPhase, WcPhaseUnit);
