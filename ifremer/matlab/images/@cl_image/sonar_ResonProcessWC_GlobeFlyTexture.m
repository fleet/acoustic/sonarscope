function [flag, Carto] = sonar_ResonProcessWC_GlobeFlyTexture(~, FileName, varargin)

[varargin, subBeams]            = getPropertyValue(varargin, 'subBeams',            []);
[varargin, subPings]            = getPropertyValue(varargin, 'subPings',            []);

%% Sp�cifique pour Reson

[varargin, nomFicNav]           = getPropertyValue(varargin, 'nomFicNav',           []);
[varargin, nomFicIP]            = getPropertyValue(varargin, 'nomFicIP',            []);
[varargin, OrigineWC]           = getPropertyValue(varargin, 'OrigineWC',           1);

% Compl�tement diff�rent pour Reson et KM
[varargin, ChoixNatDecibel]     = getPropertyValue(varargin, 'ChoixNatDecibel',     1);

% Idem pour Reson et KM mais pas s�r que ce soit op�rationnel en amont pour Reson

[varargin, flagTideCorrection]  = getPropertyValue(varargin, 'TideCorrection',      0);

% Idem pour Reson et KM
[varargin, CLimRaw]             = getPropertyValue(varargin, 'CLimRaw',             []);
[varargin, CLimComp]            = getPropertyValue(varargin, 'CLimComp',            []);
[varargin, Carto]               = getPropertyValue(varargin, 'Carto',               []);
[varargin, StepCompute]         = getPropertyValue(varargin, 'StepCompute',         1);
[varargin, StepDisplay]         = getPropertyValue(varargin, 'StepDisplay',         1);
[varargin, TypeWCOutput]        = getPropertyValue(varargin, 'TypeWCOutput',        1);
[varargin, Survey]              = getPropertyValue(varargin, 'Survey',              []);
[varargin, Names]               = getPropertyValue(varargin, 'Names',               []);
[varargin, typeAlgoWC]          = getPropertyValue(varargin, 'typeAlgoWC',          1);
[varargin, MaskBeyondDetection] = getPropertyValue(varargin, 'MaskBeyondDetection', 1);
[varargin, MaxImageSize]        = getPropertyValue(varargin, 'MaxImageSize',        1000);
[varargin, statusDepthAndAcrossDistLimits] = getPropertyValue(varargin, 'statusDepthAndAcrossDistLimits', 1);

% Actif pour Reson mais pas pour KM
[varargin, NoInteractivity] = getFlag(varargin, 'NoInteractivity'); %#ok<ASGLU>

%% Garbage Collector de Java

java.lang.System.gc

%% Lecture des donn�es

s7k = cl_reson_s7k('nomFic', FileName, 'KeepNcID', 1); % Test pour �viter des r�ouvertures du fichier netcdf
% mais il faut pouvoir acc�der � la m�thode cl_reson_s7k/delete donc
% transformer la classe classdef cl_reson_s7k < handle
if isempty(s7k)
    flag = false;
    return
end
identSondeur = selectionSondeur(s7k);
[flag, DataIndex] = readFicIndex(s7k, identSondeur);
if ~flag
    return
end

%% Cr�ation du fichier InstallationParameters

InstallationParameters = read_sonarInstallationParameters(s7k, identSondeur, 'DataIndex', DataIndex);
if isfield(InstallationParameters, 'WaterLineVerticalOffset')
    write_installationParameters_bin(s7k, identSondeur);
else
    [flag, InstallationParameters, nomFicIP] = get_InstallationParameters(s7k, InstallationParameters, nomFicIP);
    switch identSondeur
        case {13003; 13016} % Norbit : on fait sans le Instaallation parameter
        otherwise
            if ~flag
                return
            end
            if isempty(InstallationParameters)
                str1 = sprintf('Le fichier "%s" ne contient pas de datagramme 7030 et le r�pertoire ne contient pas le fichier "%s".\n\nJe vous sugg�re de faire un trac� de navigation ou un survey processing de l''ensemble des fichiers. ces traitement cr�eront un fichier "InstallationParameters" si au moins un fichier contient le datagramme 7030.', ...
                    FileName, nomFicIP);
                str2 = sprintf('"%s" does not content any 7030 datagram and its directory does not content "%s".\n\nI suggest you a  navigation plot or a survey processing of all the lines. These processings will create a "InstallationParameters" as soon as a 7030 datagramme is found.', ...
                    FileName, nomFicIP);
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoInstallationParameters');
%                 flag = 0;
%                 return
            end
    end
end

%% Gestion des bornes de contraste

if ChoixNatDecibel == 1 % Amplitude
    CLim_Amp      = CLimRaw;
    CLim_Amp_Comp = CLimComp;
else % dB
    CLim_dB      = CLimRaw;
    CLim_dB_Comp = CLimComp;
end

%% Lecture des donn�es PingBeam

ProfilCelerite = read_soundSpeedProfile(s7k, identSondeur, 'DataIndex', DataIndex);
[flag, DataDepth] = read_depth_s7k(s7k, 'identSondeur', identSondeur, 'DataIndex', DataIndex);
if ~flag
    return
end

if size(DataDepth.TransducerDepth,1) == 1
    DataDepth.TransducerDepth = DataDepth.TransducerDepth(:);
end

%% Lecture de la navigation

BypassEmptyNavigation = false;
Navigation = [];
if isempty(nomFicNav)
    if isempty(DataDepth.Latitude)
        str1 = 'On ne peut pas visualiser les donn�es de colonne d''eau dans GLOBE si il n''y a pas de navigation.';
        str2 = 'It is impossible to display the WC data in GLOBE if there is no navigation.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    else
        TimeNav    = DataDepth.Time;
        LatNav     = DataDepth.Latitude;
        LonNav     = DataDepth.Longitude;
        HeadingNav = DataDepth.Heading;
        
        if all(isnan(LatNav))
            % TODO
            DataPosition = read_navigation(s7k, identSondeur, 'DataIndex', DataIndex);
            if isempty(DataPosition.Latitude)
                Longitude = NaN(length(DataDepth.Datetime), 1);
                Latitude  = NaN(length(DataDepth.Datetime), 1);
                Heading   = NaN(length(DataDepth.Datetime), 1);
                BypassEmptyNavigation = true;
            else
                Longitude = my_interp1_longitude(DataPosition.Datetime, DataPosition.Longitude, DataDepth.Datetime, 'linear', 'extrap');
                Latitude  = my_interp1(DataPosition.Datetime, DataPosition.Latitude,            DataDepth.Datetime, 'linear', 'extrap');
                Heading   = my_interp1_headingDeg(DataPosition.Datetime, DataPosition.Heading,  DataDepth.Datetime, 'linear', 'extrap');
                if all(isnan(Heading))
                    Heading = calCapFromLatLon(Latitude, Longitude);
                end
            end
            LatNav = Latitude;
            LonNav = Longitude;
            HeadingNav = Heading;
        end
        Navigation.LatNav     = LatNav;
        Navigation.LonNav     = LonNav;
        Navigation.TimeNav    = TimeNav;
        Navigation.HeadingNav = HeadingNav;
    end
else
    if iscell(nomFicNav)
        if length(nomFicNav) > 1
            str1 = 'Il n''est pas encore pr�vu de pouvoir utiliser plusieurs fichiers de nav en m�me temps, d�sol�. Si vous avez vraiment besoin de cela faites moi en la demande, sign� JMA.';
            str2 = 'It is not possible to use several navigation files for the moment. If you really need it, please tell me, signed JMA.';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        nomFicNav = nomFicNav{1};
    end
    
    [flag, LatNav, LonNav, TimeNav, HeadingNav, ~, Immersion] = lecFicNav(nomFicNav);
%     [flag, DataNav] = import_nav(nomFicNav);
    if ~flag
        return
    end
%     TimeNav    = DataNav.Time;
%     LatNav     = DataNav.Nav.Lat;
%     LonNav     = DataNav.Nav.Lon;
%     HeadingNav = DataNav.Nav.Heading;
%     if isfield(DataNav.Nav, 'Immersion')
    if ~isempty(Immersion) && ~all(isnan(Immersion))
        ImmersionNav = Immersion;
    else
        ImmersionNav = [];
    end
    
    Navigation.LatNav       = LatNav;
    Navigation.LonNav       = LonNav;
    Navigation.TimeNav      = TimeNav;
    Navigation.HeadingNav   = HeadingNav;
    Navigation.ImmersionNav = ImmersionNav;
    
    % figure; plot(LonNav, LatNav); grid on
    
    if all(DataDepth.TransducerDepth == 0) || all(isnan(DataDepth.TransducerDepth))
        DataDepth.TransducerDepth(:) = interp1(TimeNav, ImmersionNav, DataDepth.Time);
        DataDepth.TransducerDepth = abs(DataDepth.TransducerDepth); % TODO : pas normal
    end
    
end

%% Lecture du fichier "InstallationParameters"

switch identSondeur
    case {13003; 13016} % Norbit : on fait sans le Instaallation parameter
        InstallationParameters = [];
    otherwise
        [flag, InstallationParameters] = get_InstallationParameters(s7k, InstallationParameters, nomFicIP);
%         if ~flag
%             return
%         end
end

%% Lecture des donn�es de la colonne d'eau

sub2 = subPings;
if isempty(sub2)
    sub2 = 1:length(DataDepth.PingCounter);
end
BeamData = [];
for k=1:length(sub2)
    if OrigineWC == 1 % Water Column
        [flag, DataWC, BeamData] = read_WaterColumnData(s7k, identSondeur, 'DataDepth', DataDepth, ...
            'DataIndex', DataIndex, 'subPing', sub2(k), 'Display', 0);
        if ~flag
            break
        end
    else % Mag & Phase
        [DataWC, BeamData] = read_BeamRawData(s7k, identSondeur, 'DataDepth', DataDepth, ...
            'DataIndex', DataIndex, 'subPing', sub2(k), 'DisplayLevel', 0);
    end
    if ~isempty(BeamData)
        break
    end
end
if isempty(BeamData)
    flag = 0;
    return
end

%{
[flag, IdentSonar] = DeviceIdentifier2IdentSonar(s7k, DataDepth.SystemSerialNumber);
if ~flag
msg = sprintf('Sondeur %d pas inclus dans cl_reson_s7k/view_depth', DataDepth.SystemSerialNumber);
my_warndlg(msg, 1);
return
end
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
'Sonar.SystemSerialNumber', DataDepth.SystemSerialNumber);
%}

if isempty(subPings)
    %     subPings = 1:StepCompute:length(DataWC.PingCounter);
    subPings = 1:StepCompute:length(DataDepth.PingCounter); % Modif JMA le 27/09/2016
else
    subPings = subPings(1):StepCompute:subPings(end);
end
nbBeams = size(DataDepth.Range, 2);

if isempty(subBeams)
    subBeams = 1:nbBeams;
end

%% Recherche des pings communs entre sources de donn�es

subDepth = DataWC.sub7000;
subWC = DataWC.subWC;
[~, ia] = intersect(subDepth, subPings);
if isempty(ia)
    flag = false;
    return
end
subDepth = subDepth(ia);
subWC    = subWC(ia);
nbPings  = length(subDepth);

%% Recherches des bornes des images si export pour Voxler

TimeWC = DataWC.Time;
if isa(TimeWC, 'cl_time')
    TimeWC = TimeWC.timeMat;
end
% TimeWC = TimeWC(subWC); % Modif JMA le 17/01/2021

subNonNan = find(~isnan(TimeWC));
subNan    = find(isnan(TimeWC));
TimeWC(subNan) = interp1(subNonNan, TimeWC(subNonNan), subNan, 'linear', 'extrap');
TimeNav = TimeNav.timeMat;

if (TimeNav(1) > TimeWC(end)) || (TimeWC(1) > TimeNav(end))
    str1 = 'Heure des pings / heures du fichier de navigation';
    str2 = 'Type of pings / navigation';
    FigTime = figure;
    plot(cl_time('timeMat', TimeWC), zeros(size(TimeWC)), 'r*');
    hold on; plot(cl_time('timeMat', TimeNav), ones(size(TimeNav)), 'b*'); hold off; grid on;
    title(Lang(str1,str2)); set(gca, 'YLim', [-1 2]);
    if ~isempty(nomFicNav)
        legend({FileName; nomFicNav}, 'Interpreter', 'none')
    end
    
    str1 = sprintf('La navigation n''est pas d�finie pour des pings du fichier "%s", on passe ce fichier.\nLa figure %d illustre les temps', FileName, FigTime.Number);
    str2 = sprintf('The navigation is not defined for some pings of file "%s", I skip this file.\nFig %d displays the times', FileName, FigTime.Number);
    my_warndlg(Lang(str1,str2), 0);
    
    flag = false;
    return
else
    Latitude  = my_interp1(           TimeNav, LatNav,     TimeWC, 'linear', 'extrap');
    Longitude = my_interp1_longitude( TimeNav, LonNav,     TimeWC, 'linear', 'extrap');
    Heading   = my_interp1_headingDeg(TimeNav, HeadingNav, TimeWC, 'linear', 'extrap');
    % Latitude  = Latitude(subWC);
    % Longitude = Longitude(subWC);
    % Heading   = Heading(subWC);
end

if TypeWCOutput == 1 % Images individuelles
    across = [];
    depth  = [];
else % Matrices 3D
    % On ne peut faire cette partie pour l'instant tant que l'on
    % aura pas fait la liaison entre .s7k SkCenter et .s7k PDS
    % (DataDepth.Depth est vide)
    
    if isempty(statusDepthAndAcrossDistLimits)
        minDepth = min(min(DataDepth.Depth(subDepth,:)));
        minAcrossDist = min(min(DataDepth.AcrossDist(subDepth,:)));
        maxAcrossDist = max(max(DataDepth.AcrossDist(subDepth,:)));
        minDepth      = double(minDepth);
        minAcrossDist = double(minAcrossDist);
        maxAcrossDist = double(maxAcrossDist);
        
        if isnan(minDepth)
            minDepth      = -1000;
            minAcrossDist = -2200;
            maxAcrossDist =  2200;
        end
        
        [flag, minAcrossDist, maxAcrossDist, minDepth] = question_WCLimits3DMatrix(minAcrossDist, maxAcrossDist, 'minDepth', minDepth);
        if ~flag
            return
        end
    else
        minDepth      = statusDepthAndAcrossDistLimits.minDepth;
        minAcrossDist = statusDepthAndAcrossDistLimits.minAcrossDist;
        maxAcrossDist = statusDepthAndAcrossDistLimits.maxAcrossDist;
    end
    
    Hauteur = abs(minDepth);
    Largeur = maxAcrossDist - minAcrossDist;
    Step = sqrt((Hauteur * Largeur)) / MaxImageSize;
    Step = round(Step, 2, 'significant');
    across = minAcrossDist:Step:maxAcrossDist;
    depth = -Hauteur:Step:0;
    across = centrage_magnetique(across);
    depth  = centrage_magnetique(depth);
end

if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

%% Affichage des �chogrammes polaires

Z = DataDepth.Depth(subDepth(1):subDepth(end), subBeams);
if all(isnan(Z(:)))
    Z = -1000 * (DataDepth.Range(subDepth(1):subDepth(end), subBeams) .* cosd(DataDepth.BeamDepressionAngle(subDepth(1):subDepth(end), subBeams)));
end
% [Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUI(Z, subBeams, subDepth, StepDisplay);
[Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUInew(Z(:, :), StepDisplay); % Modif JMA le 02/10/2020

%% Cr�ation du fichier Netcdf

sliceNames = {'Raw'; 'Comp'; 'Mask'};
WcNetcdfFilename = Names.nomFicNetcdf;
WcNetcdfFilename = strrep(WcNetcdfFilename, '.nc', '.NC');

% Create empty netcdf file
ncID = netcdf.create(WcNetcdfFilename, 'NETCDF4');

% Create global attributes and slice names
NetcdfGlobe3DUtils.defGlobalAttAndSliceNames(ncID, sliceNames, 'SourceFileName', FileName, ...
    'CLimRaw', CLimRaw, 'CLimComp', CLimComp);

%% Lecture de certains datagrammes pour �viter des relectures � chaque ping

Data7000 = read_sonarSettings(s7k, identSondeur);
if isempty(Data7000)
    flag = false;
    return
end

[nomFicWaterColumnData, flagFilesExist] = ResonWaterColumnDataNames(FileName, identSondeur, []);
[~, ~, Ext] = fileparts(nomFicWaterColumnData.Mat);
UN = 1;
if UN && flagFilesExist && strcmp(Ext, '.mat')
    DataWC = loadmat(nomFicWaterColumnData.Mat, 'nomVar', 'Data');
end
if  UN && flagFilesExist && strcmp(Ext, '.nc') && NetcdfUtils.existGrp(nomFicWaterColumnData.Mat, 'WaterColumnDataIFRData')
    DataWC = NetcdfUtils.Netcdf2Struct(nomFicWaterColumnData.Mat, 'WaterColumnDataIFRData', 'ncID', get_ncID(s7k));
end

% [flag, DataIndex] = readFicIndex(s7k, identSondeur);
% if ~flag
%     return
% end
Data7004     = read_7kBeamGeometry(s7k, identSondeur, 'DataIndex', DataIndex);
DataPosition = read_navigation(s7k, identSondeur, 'DataIndex', DataIndex);

%% Boucle sur les pings

firstDisplay = 1;
SonarDescription = [];

flagPingOK = false(1,nbPings);
AtLeastOneWCFound = 0;
str1 = sprintf('Reson Water Column : Traitement des pings de %s', FileName);
str2 = sprintf('Reson Water Column : Processing pings of %s', FileName);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);

% k -> kGroup

kG3d = 0;
for k=1:nbPings % Stop � k=2263 fichier 'D:\Temp\Norbit\Customer\20190910_054424.s7k'
    my_waitbar(k, nbPings, hw)
    
    str = sprintf('%d / %d : %s', k, nbPings, FileName);
    fprintf('%s\n', str);
    
    TimePing = DataDepth.Time(subDepth(k));
    
    if OrigineWC == 1
        [b_Amp, Carto, ~, SonarDescription] = view_WaterColumn(s7k, 'subPing', subDepth(k), 'Carto', Carto, 'Display', 0, ...
            'InstallationParameters', InstallationParameters, 'Navigation', Navigation, 'SonarDescription', SonarDescription, ...
            'DataDepth', DataDepth, 'Data7000', Data7000, 'DataWC', DataWC, 'Data7004', Data7004, ...
            'DataPosition', DataPosition, 'DataIndex', DataIndex, 'identSondeur', identSondeur, 'Mute', k ~= 1);
        if isempty(b_Amp)
            continue
        end
    else
        [b_Amp, Carto, SonarDescription] = view_BeamRawData(s7k, 'subPing', subDepth(k), 'Carto', Carto, 'DisplayLevel', 0, ...
            'InstallationParameters', InstallationParameters, 'Navigation', Navigation, 'SonarDescription', SonarDescription, ...
            'DataDepth', DataDepth, 'Data7000', Data7000, 'DataWC', DataWC, 'Data7004', Data7004, ...
            'DataPosition', DataPosition, 'DataIndex', DataIndex, 'identSondeur', identSondeur, 'Mute', k ~= 1);
        if isempty(b_Amp)
            continue
        end
        b_Amp = b_Amp(1);
    end
    
    TransmitSectorNumber = ones(1, min(nbBeams,b_Amp.nbColumns), 'single'); % Modif JMA BICOSE
    b_dB = reflec_Amp2dB(b_Amp(1));
    
    b_Amp_Comp = compensation_WaterColumn(b_Amp, TransmitSectorNumber);
    b_dB_Comp  = compensation_WaterColumn(b_dB,  TransmitSectorNumber);
    
    DetectedRangeInSamples = DataDepth.Range(subDepth(k),:) * b_Amp_Comp.Sonar.SampleBeamData.SampleRate;
    
    DetectedRangeInSamples = floor(DetectedRangeInSamples);
    
    subNaN = find(~isnan(DetectedRangeInSamples));
    if length(subNaN) <= 10 %2
        continue
    end
    AtLeastOneWCFound = 1;
        
    [flag, c_DepthAcrossDist, comp_DepthAcrossDist_dB, Detection, Mask] = ...
        sonar_polarEchogram(b_Amp,  ProfilCelerite, 'otherImage', b_dB_Comp, ...
        'RangeDetectionInSamples', DetectedRangeInSamples, ...
        'across', across, 'depth', depth, 'MaxImageSize', MaxImageSize, ...
        'typeAlgoWC', typeAlgoWC, 'MaskBeyondDetection', MaskBeyondDetection);
    %         'DataDepthAcrossDist', DataDepth.AcrossDist(subDepth(k),:), 'DataDepthAlongDist', DataDepth.AlongDist(subDepth(k),:));
    if ~flag
        continue
    end
%     MinRangeInMeters = min(Detection.RangeInMeters, 'omitnan');
    
    flagPingOK(k) = true;
    
    if ChoixNatDecibel == 1 % Amplitude
        WcOutRaw = c_DepthAcrossDist(1);
        WcOutComp = reflec_dB2Amp(comp_DepthAcrossDist_dB(2));
        
        CLimOutRaw  = CLim_Amp;
        CLimOutComp = CLim_Amp_Comp;
    else % dB
        WcOutRaw  = c_DepthAcrossDist(2);
        WcOutComp = comp_DepthAcrossDist_dB(2);
        
        CLimOutRaw  = CLim_dB;
        CLimOutComp = CLim_dB_Comp;
    end
    
    if isempty(CLimOutRaw)
        StatValues = WcOutRaw.StatValues;
        CLimOutRaw = StatValues.Quant_25_75;
    else
        WcOutRaw.CLim = CLimOutRaw;
    end
    WcOutComp.CLim = CLimOutComp;
    
    %% Affichage des echogrammes
    
    if (StepDisplay ~= 0) && (mod(k,StepDisplay) == 0)
        TimePing = DataDepth.Time(subDepth(k));
        [Fig, hAxe, hImage, hPing, hCurve, firstDisplay] = WC_DisplayResults(Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
            TimePing, subDepth(k), WcOutRaw, WcOutComp, Detection, CLimOutRaw, CLimOutComp);
    end
    
    %% Ecriture des echogrammes sur disque
    
    kPing           = subDepth(k);
    TransducerDepth = DataDepth.TransducerDepth(kPing,1);
    
    kG3d = kG3d + 1;
    % Modif JMA le 17/12/2020 pour fichier "G:\Carla\TestRobotS7K\DirOut\20151112_235255_PP_7150_12kHz.s7k"
    % car plantage de Globe si les pings ne commencent pas � 1. Attention,
    % effet de bord important, il n'est plus possible de faire une
    % correspondance entre NUMEROS de pings Depth et Pings WC
%     [flagPing, DataPingMat] = WC_prepareImageNetcdfPing(WcOutRaw, WcOutComp, Mask, sliceNames, TransducerDepth, kG3d, ...
    [flagPing, DataPingMat] = WC_prepareImageNetcdfPing(WcOutRaw, WcOutComp, Mask, sliceNames, TransducerDepth, k, ...
        Latitude(k), Longitude(k), Heading(k), TimePing, ...
        'MaskBeyondDetection', MaskBeyondDetection, ...
        'identSondeur', identSondeur, 'BypassEmptyNavigation', BypassEmptyNavigation, 'Mute', k ~= 1);
    if ~flagPing
        continue
    end
        
    %% Ecriture des echogrammes polaires dans le fichier netcdf
    
    NetcdfGlobe3DUtils.WC_writeImageNetcdfPing(ncID, DataPingMat);
    
end
my_close(hw, 'MsgEnd');
my_close(Fig)
if ~AtLeastOneWCFound
    flag = 0;
    return
end

%% Fermeture du fichier Netcdf

netcdf.close(ncID);

%% Renommage de l'extension .NC en .nc pour indiquer � l'utilisateur que le fichier est termin�

my_rename(WcNetcdfFilename, strrep(WcNetcdfFilename, '.NC', '.nc'));
