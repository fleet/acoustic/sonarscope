function [flag, that] = sonar_ResonSplitFrequencies(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, X] = unitaire_sonar_ResonSplitFrequencies(this(i), varargin{:});
    if ~flag
        that = [];
        return
    end
    that(:,i) = X; %#ok<AGROW>
end
that = that(:);


function [flag, that] = unitaire_sonar_ResonSplitFrequencies(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

indGeometryTypes = [cl_image.indGeometryType('PingBeam') cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist')];
flag = testSignature(this, 'GeometryType', indGeometryTypes);
if ~flag
    that = [];
    return
end

[Freq, k, m] = unique(this.Sonar.SonarFrequency(suby));
if length(Freq) == 1
    my_warndlg(Lang('Cette image n''est pas une image multiping Reson en .','This image is not a Reson Multiping image.'), 1);
    flag = 0;
    that = [];
    return
end

nF = length(Freq);
that(nF) = cl_image_I0;
for i=1:nF
    sub = (m == i);
    
    % --------------------------------
    % Appel de la fonction de filtrage
    
    that(i) = replace_Image(this, this.Image(suby(sub),subx));
    
    % --------------------------
    % Mise � jour de coordonn�es
    
    that(i) = majCoordonnees(that(i), subx, suby(sub));
    
    % -----------------------
    % Calcul des statistiques
    
    that(i) = compute_stats(that(i));
    
    % -------------------------
    % Rehaussement de contraste
    
    that(i).TagSynchroContrast = num2str(rand(1));
    CLim = [that(i).StatValues.Min that(i).StatValues.Max];
    that(i).CLim = CLim;
    
    % -------------------
    % Compl�tion du titre
    
    str = sprintf('Freq=%4.1f', Freq(i));
    that(i) = update_Name(that(i), 'Append', str);
end
flag = 1;

