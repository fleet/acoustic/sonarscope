% Déroulement de la phase 
%
% Syntax
%   [flag, b] = sonar_ResonUnwrappingPhase(a) 
%
% Input Arguments
%   a           : Instance de cl_image
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance de cl_image contenant l'image filtree
%
% Examples 
%   
%   
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = sonar_ResonUnwrappingPhase(this, Amp, DisplayLevel, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, pppp]  = unitaire_sonar_ResonUnwrappingPhase(this(i), Amp(i), DisplayLevel, varargin{:});
    if ~flag
        that = [];
        return
    end
    that(i) = pppp; %#ok<AGROW>
end



function [flag, this] = unitaire_sonar_ResonUnwrappingPhase(this, Amp, DisplayLevel, varargin)

%% Vérification des signatures

DataType = cl_image.indDataType('TxAngle');
flag = testSignature(this, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end
DataType = cl_image.indDataType('Reflectivity');
flag = testSignature(Amp, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

%% Algorithme

subc = 1:min([this.nbColumns Amp.nbColumns ]);
subl = 1:min([this.nbRows    Amp.nbRows]);
r0 = this.Sonar.SampleBeamData.r0;
Teta = this.Sonar.SampleBeamData.TxAngle;

Ph = this.Image(subl,subc);
A  = Amp.Image(subl,subc);

Ph = ResonUnwrappingPhase(Ph, r0, A, Teta, DisplayLevel, extract_ImageName(this));

this = replace_Image(this, Ph);

%% Mise a jour des coordonnees

this = majCoordonnees(this, subc, subl);

%% Calcul des statistiques

this = compute_stats(this);

%% Définition des attributs de l'image

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
Append = '_Unwrapped_';
this = update_Name(this, 'Append', Append);


flag = 1;




%% Determination of a mask on phase

function Phase = ResonUnwrappingPhase(Ph, r0, Amp, Teta, DisplayLevel, NomImage)

nbBeams   = size(Ph,2);
nbSamples = size(Ph,1);

Moitie = floor(nbBeams/2);
% subBab = 1:Moitie;
% subTri = (Moitie+1):nbBeams;

subNaN = isnan(Ph) | isnan(Amp);
Ph(subNaN) = NaN;
Amp(subNaN) = NaN;

AmpNorm = Reson_AmplitudeNormalisation(Amp);
AmpNormFilt = filter2(ones(11,1), AmpNorm, 'same');
r = Reson_AmplitudeDetection(AmpNormFilt, r0);

%{
figure; imagesc(AmpNorm); hold on; plot(r)
figure; imagesc(Ph); hold on; plot(r)
%}


%%%% PROBLEME A TRAITER ICI

% [r2, ip] = func_despike_phasespace(r, 45454);
if DisplayLevel > 1
    Fig = 544575;
else
    Fig = [];
end
Titre = sprintf('Despike FirstEcho on %s', NomImage);
[Z2, subOK, subKO] = func_despike_phasespace(r.*cosd(Teta), Fig, Titre); %#ok<ASGLU>
r2 = r(subOK);

%{
figure; imagesc(AmpNorm); hold on; plot(subOK, r2)
figure; imagesc(Ph); hold on; plot(subOK, r2)
%}

Ordre = 1;
Wc = 0.1;
[Bf,Af] = butter(Ordre,Wc);
rFiltre = filtfilt(Bf, Af, double(r2));

%{
figure; imagesc(AmpNorm); hold on; plot(subOK, rFiltre)
figure; imagesc(Ph); hold on; plot(subOK, rFiltre)
%}

subBab = find(subOK <= Moitie);
subTri = find(subOK > Moitie);
ibeam_rBab = interp1(rFiltre(subBab), subBab, 1:nbSamples);
ibeam_rTri = interp1(rFiltre(subTri), subTri, 1:nbSamples);
%{
figure; imagesc(AmpNorm); hold on; plot(ibeam_rBab, 1:nbSamples); plot(ibeam_rTri, 1:nbSamples)
figure; imagesc(Ph);      hold on; plot(ibeam_rBab, 1:nbSamples); plot(ibeam_rTri, 1:nbSamples)
%}

Phase = NaN(nbSamples, nbBeams, 'single');
for iSample=r0:nbSamples
    subSample = find(~isnan(Ph(iSample,subBab)) & ~isnan(Amp(iSample,subBab)));
    ph = Ph(iSample,subBab(subSample));
    ibeam_r = floor(ibeam_rBab(iSample));
    [val, centre] = min(abs(subBab(subSample)-ibeam_r));
    m = unwrapPh(ph, centre);
%     m = unwrapPh(m, centre);
    Phase(iSample,subBab(subSample)) = m;

    subSample = find(~isnan(Ph(iSample,subTri)) & ~isnan(Amp(iSample,subTri)));
    ph = Ph(iSample,subTri(subSample));
    ibeam_r = floor(ibeam_rTri(iSample));
    [val, centre] = min(abs(subTri(subSample)-ibeam_r));
    m = unwrapPh(ph, centre);
%     m = unwrapPh(m, centre);
    Phase(iSample,subTri(subSample)) = m;
end

function ph = unwrapPh(ph, centre)

n = length(ph);
for i=centre:(n-1)
    if (ph(i+1)-ph(i)) > 180
        ph(i+1) = ph(i+1)-360;
    elseif (ph(i+1)-ph(i)) < -180
        ph(i+1) = ph(i+1)+360;
    end
end
for i=(centre-1):-1:1
    if (ph(i+1)-ph(i)) > 180
        ph(i) = ph(i)+360;
    elseif (ph(i+1)-ph(i)) < -180
        ph(i) = ph(i)-360;
    end
end



%% Normalisation horizontale de l'amplitude

function AmpNorm = Reson_AmplitudeNormalisation(Amp)

nbBeams   = size(Amp,2);
nbSamples = size(Amp,1);

Moitie = floor(nbBeams/2);
subBab = 1:Moitie;
subTri = (Moitie+1):nbBeams;

AmpNorm = Amp;
for iSample=1:nbSamples
    y = Amp(iSample,subBab);
    Max = max(y);
    y = y / Max;
    AmpNorm(iSample,subBab) = y;
    
    y = Amp(iSample,subTri);
    Max = max(y);
    y = y / Max;
    AmpNorm(iSample,subTri) = y;
end

%% Determination of maximum of amplitude

function r = Reson_AmplitudeDetection(AmpNorm, r0)

r0 = floor(r0);
nbBeams = size(AmpNorm,2);
r = NaN(1,nbBeams);
for iBeam=1:nbBeams
    [pppp, H] = max(AmpNorm(r0:end,iBeam));
    r(iBeam) = r0 + H-1;
end



