% Déroulement de la phase 
%
% Syntax
%   [flag, b] = sonar_ResonUnwrappingPhase(a) 
%
% Input Arguments
%   a           : Instance de cl_image
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b    : Instance de cl_image contenant l'image filtree
%
% Examples 
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = sonar_ResonUnwrappingPhase_Horz(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, pppp]  = sonar_ResonUnwrappingPhase_Horz_unitaire(this(i), varargin{:});
    if ~flag
        that = [];
        return
    end
    that(i) = pppp; %#ok<AGROW>
end


function [flag, this] = sonar_ResonUnwrappingPhase_Horz_unitaire(this, varargin)

%% Vérification des signatures

DataType = cl_image.indDataType('TxAngle');
flag = testSignature(this, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    return
end

%% Algorithme

RMax1     = this.Sonar.SampleBeamData.RMax1;
iBeamMax0 = this.Sonar.SampleBeamData.iBeamMax0;

Ph = this.Image(:,:);
Ph = ResonUnwrappingPhase_Horz(Ph, iBeamMax0, RMax1);

this = replace_Image(this, Ph);

%% Calcul des statistiques

this = compute_stats(this);

%% Définition des attributs de l'image

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
Append = '_UnwrappedHorz_';
this = update_Name(this, 'Append', Append);

flag = 1;
