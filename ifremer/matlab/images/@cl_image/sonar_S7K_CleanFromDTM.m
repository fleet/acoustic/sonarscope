function flag = sonar_S7K_CleanFromDTM(this, liste)

identBathymetry   = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

if ischar(liste)
    liste = {liste};
end

Carto = [];
FigPosition = [];

N = length(liste);
str1 = 'Nettoyage de la bathym�trie des fichiers .s7k par comparaison � un MNT de r�f�rence.';
str2 = 'Bathymetry cleaning of .s7k files by comparison to a DTM.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, liste{k});
    
    [flag, this, Carto, FigPosition] = unitaire_sonar_S7K_CleanFromDTM(this, liste{k}, Carto, FigPosition);
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function [flag, this, Carto, FigPosition] = unitaire_sonar_S7K_CleanFromDTM(this, S7kFilename, Carto, FigPosition)

global useCacheNetcdf %#ok<GVMIS>

flag = 0;

%% Check if file exists

if ~exist(S7kFilename, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', S7kFilename);
    str2 = sprintf('"%s" does not exist.', S7kFilename);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Cr�ation de l'instance cl_simrad_s7k

s7k = cl_reson_s7k('nomFic', S7kFilename, 'KeepNcID', true);
if isempty(s7k)
    return
end

[flag, Data, identSondeur] = read_depth_s7k(s7k);
if ~flag
    return
end

%% Lecture des layers

Bits = [1 2 6]; % Brihtness et Colinearity seulement
a = view_depth(s7k, 'ListeLayers', [12 2 3], 'Bits', Bits); % , 'setMaskToAllLayers', 1);
if isempty(a)
    return
end

%% Lecture des layers

DepthOrigine = a(1).Image(:,:);

%% Aspiration du MNT en g�om�trie PingBeam

[flag, b] = sonarMosaiqueInv(a, 1, this(1));
if ~flag
    return
end
DepthFromDTM = b.Image(:,:);

%% Calcul de la diff�rence : bathy - MNT

c = b - a(1);

%% Nettoyage

OK  = 2;
fig = [];

Residuals = c.Image(:,:);
%     subNaN = (Data.Mask ~= 0);
%     Residuals(subNaN)      = NaN;

disp('---------------------------------------------------------------')
while OK == 2
    [fig, FigPosition] = display_images(fig, S7kFilename, DepthFromDTM, DepthOrigine, Residuals, FigPosition);
    
    I = Residuals;
    Mask = Data.Mask(:,:);
    
    MinVal = min(I(:));
    MaxVal = max(I(:));
    
    if isnan(MinVal)
        str1 = sprintf('Le fichier "%s" ne semble pas avoir de zone commune avec le MNT, on passe au suivant.', S7kFilename);
        str2 = sprintf('File "%s" does not seem to have a common part with the DTM, we switch to the next one.', S7kFilename);
        my_warndlg(Lang(str1,str2), 1);
        my_close(fig)
        return
    end
    bins = linspace(MinVal, MaxVal, 200);
    
    [N, bins, ~, SubBins] = histo1D(I, bins, 'Sampling', 1);
	sub = getFlaggedBinsOfAnHistogram(bins, N);
    
    %% On recup�re les modifications
    
    if isempty(sub)
        my_close(fig)
        return
    end
    for k=1:length(sub)
        subPoubelle = SubBins{sub(k)};
        Residuals(subPoubelle) = NaN;
        DepthOrigine(subPoubelle) = NaN;
        Mask(subPoubelle) = 0;
    end
    [fig, FigPosition] = display_images(fig, S7kFilename, DepthFromDTM, DepthOrigine, Residuals, FigPosition);
    
    Data.Mask = Mask;
%     Data.Depth(Mask == 0) = NaN; % TODO : Corriger le masque plut�t que la bathy !!!
    
    %% Ecriture du masque
    
    [nomDir, nomFic] = fileparts(S7kFilename);
    nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
    ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
    catch ME %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
my_close(fig)


function [fig, FigPosition] = display_images(fig, S7kFilename, DepthFromDTM, DepthOrigine, Residuals, FigPosition)

if isempty(fig) || ~ishandle(fig)
    if isempty(FigPosition)
        ScreenSize = get(0, 'ScreenSize');
        fig = figure('Position', centrageFig(950, ScreenSize(4)-150));
    else
        fig = figure('Position', FigPosition);
    end
end

figure(fig)
FigPosition = get(fig, 'Position');

subplot(1,3,1)
imagesc(DepthFromDTM); colorbar; axis xy; colormap(jet(256));
title([S7kFilename 'Depth from DTM'], 'interpreter', 'none')

subplot(1,3,2)
imagesc(DepthOrigine); colorbar; axis xy; colormap(jet(256));
title('Current Depth')

subplot(1,3,3)
imagesc(Residuals); colorbar; axis xy; colormap(jet(256));
title([S7kFilename ' Residuals'], 'interpreter', 'none')
