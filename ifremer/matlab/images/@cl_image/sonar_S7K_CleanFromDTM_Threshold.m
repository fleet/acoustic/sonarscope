function flag = sonar_S7K_CleanFromDTM_Threshold(this, liste, Threshold, MaskIfNoDTM, TideFile)

identBathymetry   = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

if ischar(liste)
    liste = {liste};
end

Carto = [];
FigPosition = [];

N = length(liste);
str1 = 'Nettoyage de la bathym�trie des fichiers .all par comparaison � un MNT de r�f�rence.';
str2 = 'Bathymetry cleaning of .all files by comparison to a DTM.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, liste{k});
    
    [flag, this, Carto, FigPosition] = unitaire_sonar_Simrad_CleanFromDTM(this, liste{k}, Carto, ...
        FigPosition, Threshold, MaskIfNoDTM, TideFile);
end
my_close(hw, 'MsgEnd')


function [flag, this, Carto, FigPosition] = unitaire_sonar_Simrad_CleanFromDTM(this, S7kFilename, Carto, ...
    FigPosition, Threshold, MaskIfNoDTM, TideFile)

global useCacheNetcdf %#ok<GVMIS>

flag = 0;

%% Check if file exists

if ~exist(S7kFilename, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', S7kFilename);
    str2 = sprintf('"%s" does not exist.', S7kFilename);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Cr�ation de l'instance cl_simrad_all

s7k = cl_reson_s7k('nomFic', S7kFilename, 'KeepNcID', true);
if isempty(s7k)
    return
end

[flag, Data, identSondeur] = read_depth_s7k(s7k);
if ~flag
    return
end

%% Lecture des layers

Bits = [1 2 6]; % Brihtness et Colinearity seulement
a = view_depth(s7k, 'ListeLayers', [12 2 3], 'Bits', Bits, 'TideFile', TideFile); %, 'setMaskToAllLayers', 1
if isempty(a)
    return
end

%% Aspiration du MNT en g�om�trie PingBeam

[flag, b] = sonarMosaiqueInv(a, 1, this(1));
if ~flag
    return
end

%% Calcul de la diff�rence : bathy - MNT

c = b - a(1);

%% Nettoyage

Mask = Data.Mask(:,:);
Residuals = c.Image(:,:);

subPoubelle = (abs(Residuals) > Threshold);
Mask(subPoubelle) = 0;

if MaskIfNoDTM
    subPoubelle = isnan(b);
    Mask(subPoubelle) = 0;
end

Data.Mask = Mask;
% Data.Depth(Mask == 0) = NaN; % TODO : Corriger le masque plut�t que la bathy !!!

%% Ecriture du masque

[nomDir, nomFic] = fileparts(S7kFilename);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
try
    if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
        save(nomFicMat, 'Data')
    else
        [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
        NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
    end
catch
    str = sprintf('Erreur ecriture fichier %s', nomFicMat);
    my_warndlg(str, 1);
end
