% sonar_SampleBeamRxBeamAnglesCreation ente 2 images
%
% Syntax
%   [c, status] = sonar_SampleBeamRxBeamAnglesCreation(a, b, ...)
%
% Input Arguments
%   a : Instance de cl_image de type "RayPathSampleNb"
%   b : Instance de cl_image de type "RxBeamAngle"
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   c      : Instances de cl_image contenant "Bathymetry", "AcrossDist"
%            et "AlongDist"
%   status : 1=tout c'est bien^passe
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = sonar_SampleBeamRxBeamAnglesCreation(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that(i,:)] = unitaire_sonar_SampleBeamRxBeamAnglesCreation(this(i), varargin{:}); %#ok<AGROW>
    if ~flag
        return
    end
end
that = that(:);


function [flag, this] = unitaire_sonar_SampleBeamRxBeamAnglesCreation(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

try % TODO : r�gler ce pb !!!!!
    I = repmat(this.Sonar.SampleBeamData.TxAngle(subx), length(suby), 1);
catch
    I = repmat(this.Sonar.SampleBeamData.RxAnglesEarth_WC(subx), length(suby), 1);
end
this = replace_Image(this, I);
this = majCoordonnees(this, subx, suby);
this = compute_stats(this);
this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.DataType = cl_image.indDataType('RxBeamAngle');
this = update_Name(this);

if isfield(this.Sonar.SampleBeamData, 'TransmitSectorNumber') && ~isempty(this.Sonar.SampleBeamData.TransmitSectorNumber)
    I = repmat(this.Sonar.SampleBeamData.TransmitSectorNumber(subx), length(suby), 1);
    this(2) = replace_Image(this, I);
    this(2) = compute_stats(this(2));
    this(2).TagSynchroContrast = num2str(rand(1));
    CLim = [this(2).StatValues.Min this(2).StatValues.Max];
    this(2).CLim = CLim;
    this(2).DataType = cl_image.indDataType('TxBeamIndex');
    this(2) = update_Name(this(2));
end

if isfield(this(1).Sonar.SampleBeamData, 'TxBeamIndexSwath') && ~isempty(this(1).Sonar.SampleBeamData.TxBeamIndexSwath)
    I = repmat(this(1).Sonar.SampleBeamData.TransmitSectorNumber(subx), length(suby), 1);
    this(3) = replace_Image(this(1), I);
    this(3) = compute_stats(this(3));
    this(3).TagSynchroContrast = num2str(rand(1));
    CLim = [this(3).StatValues.Min this(3).StatValues.Max];
    this(3).CLim = CLim;
    this(3).DataType = cl_image.indDataType('TxBeamIndexSwath');
    this(3) = update_Name(this(3));
end

flag = 1;
