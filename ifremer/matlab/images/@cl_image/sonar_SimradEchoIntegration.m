function [flag, b, Carto] = sonar_SimradEchoIntegration(this, typePolarEchogram, nomFicXmlEchogram, params_ZoneEI, Tag, StepDisplay, varargin)

[varargin, nomFicAll] = getPropertyValue(varargin, 'nomFicAll', []);
[varargin, suby]      = getPropertyValue(varargin, 'suby',      []);
[varargin, Carto]     = getPropertyValue(varargin, 'Carto',     []); %#ok<ASGLU>

b = [];

if isempty(nomFicAll)
    nomFicAll = this.InitialFileName;
    flag = exist(nomFicAll, 'file');
    if ~flag
        return
    end
end

%% Lecture des fichiers binaires des signaux

[nomDirRaw, nomFicRaw, Ext] = fileparts(nomFicXmlEchogram);
if strcmp(Ext, '.nc')
    nomFicRaw = strrep(nomFicRaw, '.g3d', '');

% 	[flag, DataRaw, groupNames, sliceNames, layerUnits, MaskSignification] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXmlEchogram, 'Light', true);
	[flag, DataRaw, groupNames] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXmlEchogram, 'Light', true);
    nbPings = length(DataRaw.Datetime);
    subPingEchogram = str2double(groupNames);
    %DataRaw.Datetime;
    DataEchogram = DataRaw;
    DataEchogram.iPing = subPingEchogram;
    DataEchogram.xBab = DataRaw.across_dist_L;
    DataEchogram.xTri = DataRaw.across_dist_R;
    DataEchogram.Depth = squeeze(DataRaw.elevation(2,1,:));
else
    [flag, DataEchogram] = XMLBinUtils.readGrpData(nomFicXmlEchogram, 'Memmapfile', -1);
    nbPings = DataEchogram.Dimensions.nbPings;
    subPingEchogram = DataEchogram.iPing(:);
end
if ~flag
    return
end
nomDirRaw = fullfile(nomDirRaw, nomFicRaw);

if nbPings == 0
    str1 = 'Pas de donn�es disponibles';
    str2 = 'No data available';
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% ssssss

if isempty(nomFicAll)
    nomFicAll = this.InitialFileName;
    flag = strcmp(nomFicAll, 'SIMRAD');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''est pas un fichier SIMRAD.', nomFicAll);
        str2 = sprintf('"%s" is not a SIMRAD file.', nomFicAll);
        my_warndlg(Lang(str1,str2), 1);
        b = [];
        return
    end
    flag = exist(nomFicAll, 'file');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''existe pas.', this.InitialFileFormat);
        str2 = sprintf('"%s" does not exist.', this.InitialFileFormat);
        my_warndlg(Lang(str1,str2), 1);
        b = [];
        return
    end
end

b = [];

aKM = cl_simrad_all('nomFic', nomFicAll);
if isempty(aKM)
    flag = false;
    return
end
[aAcrossDist, Carto] = get_Layer(aKM, 'AcrossDist', 'Carto', Carto);
aAlongDist           = get_Layer(aKM, 'AlongDist',  'Carto', Carto);
Depth                = get_Layer(aKM, 'Depth',      'Carto', Carto);

%{
% Pr�paration masquage �paisseur signal
try % Try car en rodage
    WCSignalWidth = get_Layer(aKM, 'WCSignalWidth', 'Carto', Carto);
catch
    WCSignalWidth = [];
end
%}

[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 1);
if ~flag
    return
end

[flag, DataRawRange] = SSc_ReadDatagrams(nomFicAll, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); % Non test� ici
if ~flag
    return
end

% [flag, DataInstallationParameters, isDual] = read_installationParameters_bin(aKM);

if isempty(suby)
    subPingDataDepth = 1:DataDepth.Dimensions.nbPings;  %TODO : DataDepth.PingCounter(:,1);
else
    subPingDataDepth = suby;
end
% subPingDataRaw = 1:DataRawRange.Dimensions.nbPings;  %TODO : DataDepth.PingCounter(:,1);
% [subEchogram, subDepth, subRaw] = intersect3(subPingEchogram, subPingDataDepth, subPingDataRaw);

% [subEchogram, subDepth, subRaw] = intersect3(DataRaw.Datetime, DataDepth.Datetime(subPingDataDepth), DataRawRange.Datetime); % Modif JMA le 10/02/2021 pour donn�es nettoy�es pae EI/Ping
[subEchogram, subDepth, subRaw] = intersect3(DataEchogram.Datetime, DataDepth.Datetime(subPingDataDepth), DataRawRange.Datetime); % Modif JMA le 10/02/2021 pour donn�es nettoy�es pae EI/Ping
if isempty(subEchogram)
    flag = false;
    return
end
subDepth = subPingDataDepth(subDepth);
DataEchogram.iPing = subPingEchogram;

PingCounterRustine = aAcrossDist.Sonar.PingCounter;
[subAcross, sub2] = intersect3(PingCounterRustine, DataDepth.PingCounter(subDepth,1), DataDepth.PingCounter(subPingDataDepth,1)); %#ok<ASGLU>

%% Boucle sur les �chogrammes

% AntennaAcrossDist = DataInstallationParameters.S1Y;
AntennaAcrossDist = 0;
[EI, Unit] = sonar_EchoIntegrationOverSpecularOrSeabed(aAcrossDist, aAlongDist, Depth, ...
    typePolarEchogram, DataEchogram, nomDirRaw, nomFicRaw, subEchogram, subDepth, ...
    params_ZoneEI, AntennaAcrossDist, ...
    StepDisplay, 'EchogramFileName', nomFicXmlEchogram);
    
%% Gestion du layer TxBeamIndexSwath

SonarPortMode_1 = get(aAcrossDist, 'SonarPortMode_1');
TxBeamIndexSwath = Simrad_DataRaw2TxBeamIndexSwath(DataRawRange, SonarPortMode_1(subAcross), 'subl', subRaw);

%% Compute PingAcrossDist images

[xEIImage, EIReflecImage, EINbPointsImage, EIAnglesImage, EIAlongDistImage, EITxBeamIndexSwathImage] = ...
    EI_Compute_Images(EI, 'TxBeamIndexSwath', TxBeamIndexSwath);

%% Write PingAcrossDist images on disk

b = EI_write_Images(aAcrossDist, xEIImage, typePolarEchogram, ...
    Tag, Carto, nomFicAll, nomFicRaw, SonarPortMode_1, ...
    EIReflecImage, EINbPointsImage, EIAnglesImage, EIAlongDistImage, ...
	Unit, params_ZoneEI.Where, ...
    'TxBeamIndexSwathImage', EITxBeamIndexSwathImage);
% SonarScope(b)
