function [flag, Carto] = sonar_SimradProcessWC(this, FileName, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []);
% TODO : Rajouter subBeams // mettre � niveau que sonar_ResonProcessWC
% [varargin, subBeams] = getPropertyValue(varargin, 'subBeams', []);
% [varargin, subPings] = getPropertyValue(varargin, 'subPings', []);

% Compl�tement diff�rent pour Reson et KM
[varargin, minAcrossDist] = getPropertyValue(varargin, 'minAcrossDist', []);
[varargin, maxAcrossDist] = getPropertyValue(varargin, 'maxAcrossDist', []);
[varargin, CorrectionTVG] = getPropertyValue(varargin, 'CorrectionTVG', 30);
[varargin, CompReflec]    = getPropertyValue(varargin, 'CompReflec',    []);
[varargin, ModeDependant] = getPropertyValue(varargin, 'ModeDependant', []);

% L�g�rement diff�rent pour Reson et KM
[varargin, CLimRawIn]  = getPropertyValue(varargin, 'CLimRaw',  []);
[varargin, CLimCompIn] = getPropertyValue(varargin, 'CLimComp', []);

% Idem pour Reson et KM mais pas s�r que ce soit op�rationnel en amont pour Reson

[varargin, flagTideCorrection] = getPropertyValue(varargin, 'TideCorrection', 0);
[varargin, IndNavUnit]         = getPropertyValue(varargin, 'IndNavUnit',     []);

% Idem pour Reson et KM
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, StepCompute]  = getPropertyValue(varargin, 'StepCompute',  1);
[varargin, StepDisplay]  = getPropertyValue(varargin, 'StepDisplay',  1);
[varargin, TypeWCOutput] = getPropertyValue(varargin, 'TypeWCOutput', 0);
[varargin, Survey]       = getPropertyValue(varargin, 'Survey',       []);
[varargin, Names]        = getPropertyValue(varargin, 'Names',        []);
[varargin, typeAlgoWC]   = getPropertyValue(varargin, 'typeAlgoWC',   1);
[varargin, MaxImageSize] = getPropertyValue(varargin, 'MaxImageSize', 1000);
[varargin, NoWaitbar]    = getPropertyValue(varargin, 'NoWaitbar',    0);

[varargin, MaskBeyondDetection]       = getPropertyValue(varargin, 'MaskBeyondDetection',       1);
[varargin, skipAlreadyProcessedFiles] = getPropertyValue(varargin, 'skipAlreadyProcessedFiles', 0); %#ok<ASGLU>

% Actif pour Reson mais pas pour KM
% [varargin, NoInteractivity] = getFlag(varargin, 'NoInteractivity');

%% Garbage Collector de Java

java.lang.System.gc

%% Lecture des donn�es

if iscell(FileName)
    FileName = FileName{1};
end
aKM = cl_simrad_all('nomFic', FileName);
if isempty(aKM)
    flag = false;
    return
end

[~, DataSSP] = SSc_ReadDatagrams(FileName, 'Ssc_SoundSpeedProfile');

[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 1);
if ~flag
    return
end

[flag, DataWC] = read_WaterColumn_bin(aKM, 'Memmapfile', -1);
if ~flag
    str1 = sprintf('Il n''y a pas de datagrammes de Water Column dans le fichier "%s".', FileName);
    str2 = sprintf('No Water Column datagrams in file "%s".', FileName);
    my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
    return
end

[flag, DataRaw] = read_rawRangeBeamAngle(aKM, 'MemmapFile', 1);
if ~flag
    return
end

[flag, DataInstalParam, isDual] = read_installationParameters_bin(aKM);
if ~flag
    return
end

[flag, IdentSonar] = SimradModel2IdentSonar(DataWC.EmModel, DataDepth.EmModel, 'isDual', isDual);
if ~flag
    return
end

switch IdentSonar
    case {24; 25; 31} % EM2040S
        F = mean(DataWC.CentralFrequency(:), 'omitnan') / 1000;
        listFreq = [200 300 400];
        [~,ind] = min(abs(listFreq - F));
        MainFrequency = listFreq(ind);
        
        % case % TODO
        
    otherwise
        MainFrequency = [];
end

SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', DataWC.SystemSerialNumber, 'MainFrequency', MainFrequency);
% 'paramsFileName', EM2040S_300kHz

% DataDepth.WCImageOverSpec = NaN(DataDepth.Dimensions.nbPings, DataWC.Dimensions.nbRx, 'single');
% DataDepth.WCAcrossDist    = NaN(DataDepth.Dimensions.nbPings, DataWC.Dimensions.nbRx, 'single');
% DataDepth.WCAlongDist     = NaN(DataDepth.Dimensions.nbPings, DataWC.Dimensions.nbRx, 'single');

if isempty(suby)
    suby = 1:StepCompute:this.nbRows;
else
    suby = suby(1):StepCompute:suby(end);
end

if isempty(CLimCompIn)
    CLimCompIn = [-5 35];
end

%{
% Tentative d'automatisation de l'�chantillonnage
MeanDistPings = mean(this.Sonar.DistPings, 'omitnan');
AcrossDist = DataDepth.AcrossDist(suby,:);
resolAcross = (max(AcrossDist(:)) - min(AcrossDist(:))) / (DataDepth.Dimensions.nbBeams - 1);
resol = nearestExpectedGridSize(max(MeanDistPings, resolAcross));
%}

%% Recherche des pings communs entre sources de donn�es

subPingImage = this.Sonar.PingCounter(suby);
subPingWC    = DataWC.PingCounter(:,1);

subPingWCOK = find(DataWC.FlagPings == 0);
subPingWC   = subPingWC(subPingWCOK); % Ajout JMA le 14/01/2020

subPingDepth = DataDepth.PingCounter(:,1);
[subThis, subWC, subDepth] = intersect3(subPingImage, subPingWC, subPingDepth);
subWC = subPingWCOK(subWC);
subThis = suby(subThis);
if isempty(subThis)
    flag = false;
    return
end

%% Lecture de la donn�e "Attitude"

[flag, DataAttitude] = read_attitude_bin(aKM, 'Memmapfile', 1);
if ~flag || isempty(DataAttitude) || isempty(DataAttitude.Roll)
    return
end

[~, DataRuntime] = SSc_ReadDatagrams(FileName, 'Ssc_Runtime', 'LogSilence', 0);

%% Recherche des bornes des images si export pour Voxler

[flag, DataPosition] = read_position_bin(aKM, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
if ~flag
    return
end
Latitude  = my_interp1(DataPosition.Time, DataPosition.Latitude,            DataWC.Time, 'linear', 'extrap');
Longitude = my_interp1_longitude(DataPosition.Time, DataPosition.Longitude, DataWC.Time, 'linear', 'extrap');
Heading   = my_interp1_headingDeg(DataPosition.Time, DataPosition.Heading,  DataWC.Time, 'linear', 'extrap');

Latitude  = Latitude(subWC);
Longitude = Longitude(subWC);
Heading   = Heading(subWC);

if isfield(DataAttitude, 'Tide')
    TideValue = my_interp1(DataAttitude.Time, DataAttitude.Tide, DataDepth.Time);
    TideValue = TideValue(subDepth);
    TideValue(isnan(TideValue)) = 0;
else
    TideValue = zeros(1,length(subDepth));
end

if TypeWCOutput == 1
    across = [];
    depth  = [];
else % Matrices 3D
    MIN = min(min(DataDepth.AcrossDist(subDepth,:)));
    if isempty(minAcrossDist)
        minAcrossDist = MIN;
    else
        minAcrossDist = max(minAcrossDist, MIN);
    end
    MAX = max(max(DataDepth.AcrossDist(subDepth,:)));
    if isempty(maxAcrossDist)
        maxAcrossDist = MAX;
    else
        maxAcrossDist = min(maxAcrossDist, MAX);
    end
    minAcrossDist = double(minAcrossDist);
    maxAcrossDist = double(maxAcrossDist);
    Largeur = maxAcrossDist - minAcrossDist;
    
    Z = DataDepth.Depth(subDepth,:);
    TideValue = TideValue(:); % Pour �viter un bug R2016a
    Z = bsxfun(@plus, Z, TideValue);
    minDepth = min(min(Z));
    maxDepth = max(TideValue);
    minDepth = double(minDepth);
    Hauteur = maxDepth - minDepth;
    
%     hypo = sqrt(Hauteur^2 + (Largeur/2)^2);
%     Step = hypo / MaxImageSize;
    
    Step = sqrt((Hauteur * Largeur)) / MaxImageSize;
    
    Step = max(Step, 0.25 * (Largeur / (DataDepth.Dimensions.nbBeams-1))); % Ajout JMA le 28/09/2020
    Step = round(Step, 2);
    
    across = minAcrossDist:Step:maxAcrossDist;
    depth = -Hauteur:Step:maxDepth;
    while length(depth) > 500
        Step   = Step * 1.25;
        across = minAcrossDist:Step:maxAcrossDist;
        depth  = -Hauteur:Step:0;
    end
    across = centrage_magnetique(across);
    depth  = centrage_magnetique(depth);
end

if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

%% Affichage des �chogrammes polaires

[Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUInew(DataDepth.Depth(subDepth, :), StepDisplay, 'y', subDepth); % Modif JMA le 02/10/2020

%% Cr�ation des r�pertoires

[nomDirExportRaw, nomFic] = fileparts(Names.nomFicXmlRaw);
nomDirExportRaw = fullfile(nomDirExportRaw, nomFic);
if exist(nomDirExportRaw, 'dir')
    rmdir(nomDirExportRaw,'s')
end
WcRawXmlFilename = Names.nomFicXmlRaw;

[nomDirExportComp, nomFic] = fileparts(Names.nomFicXmlComp);
nomDirExportComp = fullfile(nomDirExportComp, nomFic);
if exist(nomDirExportComp, 'dir')
    rmdir(nomDirExportComp,'s')
end
WcCompXmlFilename = Names.nomFicXmlComp;

%% Lecture du fichier de compensation de la r�flectivit�

if isempty(CompReflec)
    bilan = [];
else
    if ischar(CompReflec)
        CompReflec = {CompReflec};
    end
    for k=1:length(CompReflec)
        [CourbeConditionnelle, flag] = load_courbesStats(CompReflec{k});
        if ~flag
            return
        end
        bilan{k} = CourbeConditionnelle.bilan{1}; %#ok<AGROW>
    end
end

%% Initialisation des tableaux

nbPings  = length(subThis);
nbAcross = length(across);

StructWcXmlRaw  = WC_InitStructXML(nbPings, nbAcross); % Echogramme Raw
StructWcXmlComp = WC_InitStructXML(nbPings, nbAcross); % Echogramme Comp

%% Boucle sur les pings

firstDisplay = 1;
Carto = this.Carto;
TimePingThis = this.Sonar.Time;
DataDepthTime = DataDepth.Time;

kWC = false(1,nbPings);
TransducerDepth = DataDepth.TransducerDepth(:,1);

[~, nomFicSeul, Ext] = fileparts(FileName);
str1 = sprintf('Traitement des pings de WC de %s', [nomFicSeul Ext]);
str2 = sprintf('Processing WC pings of %s', [nomFicSeul Ext]);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings, 'NoWaitbar', NoWaitbar);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    
    fprintf('%5d/%5d : %s\n', k, nbPings, FileName);
    kSubThis = subThis(k);
    
    TimePing = TimePingThis(kSubThis);
    subDepthk = subDepth(k);
    TransducerDepthPing = TransducerDepth(subDepthk);
    
    %% Test appel pour pouvoir �ventuellement brancher la //Tbx sur cette boucle
    
    [flag, StructWcXmlRaw, StructWcXmlComp, ...
        WcOutRaw, WcOutComp, Detection, ...
        Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
        CLimRawOut,  WcRawUnit, CLimCompOut, WcCompUnit] ...
        = sonar_SimradProcessWCPing(aKM, StructWcXmlRaw, StructWcXmlComp, ...
        DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, DataRuntime, DataInstalParam, SonarDescription, ...
        Carto, kSubThis, CorrectionTVG, CompReflec, bilan, ModeDependant, ...
        DataSSP, across, depth, MaxImageSize, typeAlgoWC, MaskBeyondDetection, ...
        Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
        k, TimePing, subDepthk, CLimRawIn, CLimCompIn, ...
        nomDirExportRaw, nomDirExportComp, TypeWCOutput, ...
        Latitude(k), Longitude(k), Heading(k), nbAcross, flagTideCorrection, TransducerDepthPing); % Carto en sortie
    if flag
        kWC(k) = true;
        
        %% Visualisation de contr�le
        
        if (StepDisplay ~= 0) && (mod(k,StepDisplay) == 0)
            TimePing = DataDepthTime(subDepthk);
            [Fig, hAxe, hImage, hPing, hCurve, firstDisplay] = WC_DisplayResults(Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
                TimePing, subDepthk, WcOutRaw, WcOutComp, Detection, CLimRawOut, CLimCompOut);
        end
    end
    
    %% Sauvegarde interm�diaire du fichier XML
    
    if flag && (mod(k+2, 100) == 1) % Ecriture interm�diaire du fichier XML sur disque
        WC_write_PolarEchogramsXML(StructWcXmlRaw,  k, FileName, TypeWCOutput, WcRawXmlFilename,  across, depth, Survey, CLimRawOut,  WcRawUnit);
        WC_write_PolarEchogramsXML(StructWcXmlComp, k, FileName, TypeWCOutput, WcCompXmlFilename, across, depth, Survey, CLimCompOut, WcCompUnit);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60);
my_close(Fig)

if all(~kWC)
    flag = 0;
    return
end

StructWcXmlRaw  = WC_MajStructXML(StructWcXmlRaw,  kWC);
StructWcXmlComp = WC_MajStructXML(StructWcXmlComp, kWC);

%% Rename fichier en .bad si pas de WC !!!

% nbPings = kWC;
% if nbPings == 0
%     str1 = sprintf('Il n''y a pas de datagrammes de Water Column valide dans le fichier "%s". Je le renomme en ".bad"', FileName);
%     str2 = sprintf('No valid Water Column datagrams in file "%s". I rename it in ".bad"', FileName);
%     renameBadFile(aKM, 'WC', 'Message', Lang(str1,str2))
% end

%% Ecriture du fichier XML sur disque

nbPings = sum(kWC);
WC_write_PolarEchogramsXML(StructWcXmlRaw,  nbPings, FileName, TypeWCOutput, WcRawXmlFilename,  across, depth, Survey, CLimRawOut,  WcRawUnit);
WC_write_PolarEchogramsXML(StructWcXmlComp, nbPings, FileName, TypeWCOutput, WcCompXmlFilename, across, depth, Survey, CLimCompOut, WcCompUnit);

    
function [flag, StructWcXmlRaw, StructWcXmlComp, ...
    WcOutRaw, WcOutComp, Detection, ...
    Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
    CLimRawOut,  WcRawUnit, CLimCompOut, WcCompUnit, Carto] ...
    = sonar_SimradProcessWCPing(aKM, StructWcXmlRaw, StructWcXmlComp, ...
    DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, DataRuntime, DataInstalParam, SonarDescription, ...
    Carto, kSubThis, CorrectionTVG, CompReflec, bilan, ModeDependant, ...
    DataSSP, across, depth, MaxImageSize, typeAlgoWC, MaskBeyondDetection, ...
    Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
    k, TimePing, subDepthk, CLimRawIn, CLimCompIn, ...
    nomDirExportRaw, nomDirExportComp, TypeWCOutput, Latitude, Longitude, Heading, nbAcross, ...
    flagTideCorrection, TransducerDepthPing)

flag        = 0;
WcOutRaw    = [];
WcOutComp   = [];
Detection   = [];
CLimRawOut  = [];
WcRawUnit   = [];
CLimCompOut = [];
WcCompUnit  = [];

%% Lecture du ping

[b_SampleBeam_dB, Carto, TransmitSectorNumber, DetectedRangeInSamples] = import_WaterColumn_All(aKM, ...
    DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, DataRuntime, DataInstalParam, SonarDescription, ...
    'Carto', Carto, 'subPing', kSubThis, 'Mute', 1, 'CorrectionTVG', CorrectionTVG);
if isempty(b_SampleBeam_dB)
    return
end

%% Compensation de la r�flectivit� (� l'essai)

if ~isempty(CompReflec)
    [flag, b_SampleBeam_dB] = applyReflectivityCompensationCurveOnWC(b_SampleBeam_dB, bilan, ModeDependant);
    if ~flag
        return
    end
end

%% Compensation par la valeur m�diane

comp_SampleBeam_dB = compensation_WaterColumn(b_SampleBeam_dB, TransmitSectorNumber, 'NoStats');

%% Calcul de l'�chogramme polaire

[flag, c_DepthAcrossDist, comp_DepthAcrossDist_dB, Detection, Mask] = sonar_polarEchogram(b_SampleBeam_dB, DataSSP, ...
    'otherImage', comp_SampleBeam_dB, ...
    'RangeDetectionInSamples', DetectedRangeInSamples, ...
    'across', across, 'depth', depth, 'MaxImageSize', MaxImageSize, ...
    'typeAlgoWC', typeAlgoWC, 'MaskBeyondDetection', MaskBeyondDetection);
%         'DataDepthAcrossDist', DataDepth.AcrossDist(subDepthk,:), 'DataDepthAlongDist', DataDepth.AlongDist(subDepthk,:));
if ~flag
    return
end
% c_DepthAcrossDist(1) : image en amplitude, c_DepthAcrossDist(2) : image en dB
% comp_DepthAcrossDist_dB(1) : image non masqu�e, comp_DepthAcrossDist_dB(2) : image masqu�e

%% S�lection du type de repr�sentation de la r�flectivit�

ChoixNatDecibel = 2; % TODO (fait pour Reson)
if ChoixNatDecibel == 1 % Amplitude
    WcOutRaw  = c_DepthAcrossDist(1);
    WcOutComp = reflec_dB2Amp(comp_DepthAcrossDist_dB(2));
    
    CLimCompOut = CLimCompIn; % TODO
    WcRawUnit   = 'Amp';
else % dB
    WcOutRaw  = c_DepthAcrossDist(2);
    WcOutComp = comp_DepthAcrossDist_dB(2);
    
    CLimCompOut = CLimCompIn;
    WcRawUnit   = 'dB';
end
CLimRawOut = CLimRawIn;
WcCompUnit = 'dB';

WcOutRaw.CLim  = CLimRawOut;
WcOutComp.CLim = CLimCompOut;

%% Ecriture des echogrammes sur disque

kPing   = subDepthk;
% kWCPing = kPing;

[flagOK1, StructWcXmlRaw] = WC_WriteDepthAcrossDistEchograms(...
    WcOutRaw, StructWcXmlRaw, nomDirExportRaw, CLimRawOut, ...
    TypeWCOutput, TransducerDepthPing, MaskBeyondDetection, Mask, ...
    kPing, TimePing, k, Latitude, Longitude, Heading, nbAcross, ...
    flagTideCorrection, Detection, 'Heave', WcOutRaw.Sonar.SampleBeamData.Heave); %#ok<ASGLU>

[flagOK2, StructWcXmlComp] = WC_WriteDepthAcrossDistEchograms(...
    WcOutComp, StructWcXmlComp, nomDirExportComp, CLimCompOut, ...
    TypeWCOutput, TransducerDepthPing, MaskBeyondDetection, Mask, ...
    kPing, TimePing, k, Latitude, Longitude, Heading, nbAcross, ...
    flagTideCorrection, Detection, 'Heave', WcOutRaw.Sonar.SampleBeamData.Heave); %#ok<ASGLU>

% TODO : flag = flagOK1 && flagOK2 voir si il peut y avoir des effets de
% bords car �a n'avait jamais �t� branch�
