function [flag, Carto] = sonar_SimradProcessWC_GlobeFlyTexture(this, FileName, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []);
% TODO : Rajouter subBeams // mettre � niveau que sonar_ResonProcessWC
% [varargin, subBeams] = getPropertyValue(varargin, 'subBeams', []);
% [varargin, subPings] = getPropertyValue(varargin, 'subPings', []);

% Compl�tement diff�rent pour Reson et KM
[varargin, minAcrossDist] = getPropertyValue(varargin, 'minAcrossDist', []);
[varargin, maxAcrossDist] = getPropertyValue(varargin, 'maxAcrossDist', []);
[varargin, CorrectionTVG] = getPropertyValue(varargin, 'CorrectionTVG', 30);
[varargin, CompReflec]    = getPropertyValue(varargin, 'CompReflec',    []);
[varargin, ModeDependant] = getPropertyValue(varargin, 'ModeDependant', []);

% L�g�rement diff�rent pour Reson et KM
[varargin, CLimRawIn]  = getPropertyValue(varargin, 'CLimRaw',  []);
[varargin, CLimCompIn] = getPropertyValue(varargin, 'CLimComp', [-5 35]);

% Idem pour Reson et KM mais pas s�r que ce soit op�rationnel en amont pour Reson

[varargin, Mute]       = getPropertyValue(varargin, 'Mute',           0);
% [varargin, flagTideCorrection] = getPropertyValue(varargin, 'TideCorrection', 0);
[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit',     []);

% Idem pour Reson et KM
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, StepCompute]  = getPropertyValue(varargin, 'StepCompute',  1);
[varargin, TypeWCOutput] = getPropertyValue(varargin, 'TypeWCOutput', 0);
[varargin, Survey]       = getPropertyValue(varargin, 'Survey',       []);
[varargin, Names]        = getPropertyValue(varargin, 'Names',        []);
[varargin, typeAlgoWC]   = getPropertyValue(varargin, 'typeAlgoWC',   1);
[varargin, MaxImageSize] = getPropertyValue(varargin, 'MaxImageSize', 1000);
[varargin, StepDisplay]  = getPropertyValue(varargin, 'StepDisplay',  0);
[varargin, MaskBeyondDetection]       = getPropertyValue(varargin, 'MaskBeyondDetection',       1);
[varargin, skipAlreadyProcessedFiles] = getPropertyValue(varargin, 'skipAlreadyProcessedFiles', 0); %#ok<ASGLU>

% Actif pour Reson mais pas pour KM
% [varargin, NoInteractivity] = getFlag(varargin, 'NoInteractivity');

%% Garbage Collector de Java

java.lang.System.gc

%% Lecture des donn�es

if iscell(FileName)
    FileName = FileName{1};
end
aKM = cl_simrad_all('nomFic', FileName);
if isempty(aKM)
    flag = false;
    return
end

[~, DataSSP] = SSc_ReadDatagrams(FileName, 'Ssc_SoundSpeedProfile');

[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', -1); % 1 avant
if ~flag
    return
end
DataDepth.Depth      = DataDepth.Depth(:,:);
DataDepth.AcrossDist = DataDepth.AcrossDist(:,:);

[flag, DataWC] = read_WaterColumn_bin(aKM, 'Memmapfile', -1);
if ~flag
    str1 = sprintf('Il n''y a pas de datagrammes de Water Column dans le fichier "%s".', FileName);
    str2 = sprintf('No Water Column datagrams in file "%s".', FileName);
    my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
    return
end

[flag, DataRaw] = read_rawRangeBeamAngle(aKM, 'Memmapfile', -1); % 1 avant
if ~flag
    return
end
DataWC.TransmitSectorNumber = DataWC.TransmitSectorNumber(:,:);

[flag, DataInstalParam, isDual] = read_installationParameters_bin(aKM);
if ~flag
    return
end

[flag, IdentSonar] = SimradModel2IdentSonar(DataWC.EmModel, DataDepth.EmModel, 'isDual', isDual);
if ~flag
    return
end

switch IdentSonar
    case {24; 25; 31} % EM2040S
        F = mean(DataWC.CentralFrequency(:), 'omitnan') / 1000;
        listFreq = [200 300 400];
        [~,ind] = min(abs(listFreq - F));
        MainFrequency = listFreq(ind);
        
        % case % TODO
        
    otherwise
        MainFrequency = [];
end

SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', DataWC.SystemSerialNumber, 'MainFrequency', MainFrequency);
% 'paramsFileName', EM2040S_300kHz

% DataDepth.WCImageOverSpec = NaN(DataDepth.Dimensions.nbPings, DataWC.Dimensions.nbRx, 'single');
% DataDepth.WCAcrossDist    = NaN(DataDepth.Dimensions.nbPings, DataWC.Dimensions.nbRx, 'single');
% DataDepth.WCAlongDist     = NaN(DataDepth.Dimensions.nbPings, DataWC.Dimensions.nbRx, 'single');

if isempty(suby)
    suby = 1:StepCompute:this.nbRows;
else
    suby = suby(1):StepCompute:suby(end);
end
% x = this.x;

%% Recherche des pings communs entre sources de donn�es

subPingImage = this.Sonar.PingCounter(suby);
% subPingImage = DataDepth.PingCounter(suby,1);
subPingWC    = DataWC.PingCounter(:,1);

% subPingWCOK = find(DataWC.FlagPings == 0);
NbSamples = sum(DataWC.NumberOfSamples(:,:), 2);
subPingWCOK = find((DataWC.FlagPings == 0) & (NbSamples ~= 0));
subPingWC = subPingWC(subPingWCOK); % Ajout JMA le 14/01/2020

subPingDepth = DataDepth.PingCounter(:,1);
[subThis, subWC, subDepth] = intersect3(subPingImage, subPingWC, subPingDepth);
subWC = subPingWCOK(subWC);
subThis = suby(subThis);
if isempty(subThis)
    flag = false;
    return
end

%% Lecture de la donn�e "Attitude"

[flag, DataAttitude] = read_attitude_bin(aKM, 'Memmapfile', 0); % 1 avant
if ~flag || isempty(DataAttitude) || isempty(DataAttitude.Roll)
    return
end

[~, DataRuntime] = SSc_ReadDatagrams(FileName, 'Ssc_Runtime', 'LogSilence', 0);

%% Recherche des bornes des images si export pour Voxler

[flag, DataPosition] = read_position_bin(aKM, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0); % 1 avant
if ~flag
    return
end
Latitude  = my_interp1(DataPosition.Time, DataPosition.Latitude,            DataWC.Time, 'linear', 'extrap');
Longitude = my_interp1_longitude(DataPosition.Time, DataPosition.Longitude, DataWC.Time, 'linear', 'extrap');
Heading   = my_interp1_headingDeg(DataPosition.Time, DataPosition.Heading,  DataWC.Time, 'linear', 'extrap');

Latitude  = Latitude(subWC);
Longitude = Longitude(subWC);
Heading   = Heading(subWC);

if isfield(DataAttitude, 'Tide')
    TideValue = my_interp1(DataAttitude.Time, DataAttitude.Tide, DataDepth.Time);
    TideValue = TideValue(subDepth);
    TideValue(isnan(TideValue)) = 0;
else
    TideValue = zeros(1,length(subDepth));
end

if TypeWCOutput == 1
    across = [];
    depth  = [];
else % Matrices 3D
    MIN = min(min(DataDepth.AcrossDist(subDepth,:)));
    if isempty(minAcrossDist)
        minAcrossDist = MIN;
    else
        minAcrossDist = max(minAcrossDist, MIN);
    end
    MAX = max(max(DataDepth.AcrossDist(subDepth,:)));
    if isempty(maxAcrossDist)
        maxAcrossDist = MAX;
    else
        maxAcrossDist = min(maxAcrossDist, MAX);
    end
    minAcrossDist = double(minAcrossDist);
    maxAcrossDist = double(maxAcrossDist);
    Largeur = maxAcrossDist - minAcrossDist;
    
    Z = DataDepth.Depth(subDepth,:);
    TideValue = TideValue(:); % Pour �viter un bug R2016a
    Z = bsxfun(@plus, Z, TideValue);
    minDepth = min(min(Z));
    maxDepth = max(TideValue);
    minDepth = double(minDepth);
    Hauteur = maxDepth - minDepth;
    Step = sqrt((Hauteur * Largeur)) / MaxImageSize;
    
    across = minAcrossDist:Step:maxAcrossDist;
    depth = -Hauteur:Step:maxDepth;
    while length(depth) > 500
        Step   = Step * 1.25;
        across = minAcrossDist:Step:maxAcrossDist;
        depth  = -Hauteur:Step:0;
    end
    across = centrage_magnetique(across);
    depth  = centrage_magnetique(depth);
end

if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

%% Cr�ation des r�pertoires

[nomDirExportRaw, nomFic] = fileparts(Names.nomFicXmlRaw);
nomDirExportRaw = fullfile(nomDirExportRaw, nomFic);
if exist(nomDirExportRaw, 'dir')
    rmdir(nomDirExportRaw,'s')
end

[nomDirExportComp, nomFic] = fileparts(Names.nomFicXmlComp);
nomDirExportComp = fullfile(nomDirExportComp, nomFic);
if exist(nomDirExportComp, 'dir')
    rmdir(nomDirExportComp,'s')
end

%% Lecture du fichier de compensation de la r�flectivit�

if isempty(CompReflec)
    bilan = [];
else
    if ischar(CompReflec)
        CompReflec = {CompReflec};
    end
    for k=1:length(CompReflec)
        [CourbeConditionnelle, flag] = load_courbesStats(CompReflec{k});
        if ~flag
            return
        end
        bilan{k} = CourbeConditionnelle.bilan{1}; %#ok<AGROW>
    end
end

%% Cr�ation du fichier Netcdf

sliceNames = {'Raw'; 'Comp'; 'Mask'};
WcNetcdfFilename = Names.nomFicNetcdf;
WcNetcdfFilename = strrep(WcNetcdfFilename, '.nc', '.NC');

% Create empty netcdf file
ncID = netcdf.create(WcNetcdfFilename, 'NETCDF4');

% Create global attributes and slice names
NetcdfGlobe3DUtils.defGlobalAttAndSliceNames(ncID, sliceNames, 'SourceFileName', FileName, ...
    'CLimRaw', CLimRawIn, 'CLimComp', CLimCompIn);

%% Affichage des �chogrammes polaires

[Fig, hAxe, hPing, hImage, hCurve] = WC_CreateGUInew(DataDepth.Depth(subDepth, :), StepDisplay); % Modif JMA le 02/10/2020
firstDisplay = 1;

%% Loop on the pings

nbPings           = length(subThis);
SonarTime         = this.Sonar.Time;
Carto             = this.Carto;
flagPings         = true(nbPings,1);
AtLeastOneWCFound = false;
[~, nomFicSeul, Ext] = fileparts(FileName);
str1 = sprintf('Traitement des pings de WC de %s', [nomFicSeul Ext]);
str2 = sprintf('Processing WC pings of %s',        [nomFicSeul Ext]);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings, 'NoWaitbar', Mute);
for kGroup=1:nbPings
    my_waitbar(kGroup, nbPings, hw)
    fprintf('%5d/%5d : %s\n', kGroup, nbPings, FileName);
    
    subPings = subThis(kGroup);
    TimePing = SonarTime(subPings);
    
    %% Lecture du ping
    
    [b_SampleBeam_dB, ~, TransmitSectorNumber, DetectedRangeInSamples] = import_WaterColumn_All(aKM, ...
        DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, DataRuntime, DataInstalParam, SonarDescription, ...
        'Carto', Carto, 'subPing', subPings, 'Mute', 1, 'CorrectionTVG', CorrectionTVG);
    if isempty(b_SampleBeam_dB)
%         return
        continue
    end
    AtLeastOneWCFound = true;
    
    %% Compensation de la r�flectivit� (� l'essai)
    
    if ~isempty(CompReflec)
        [flagPing, b_SampleBeam_dB] = applyReflectivityCompensationCurveOnWC(b_SampleBeam_dB, bilan, ModeDependant);
        if ~flagPing
            return
        end
    end
    
    %% Compensation par la valeur m�diane
    
    comp_SampleBeam_dB = compensation_WaterColumn(b_SampleBeam_dB, TransmitSectorNumber, 'NoStats');
    
    %% Calcul de l'�chogramme polaire
    
    [flagPing, c_DepthAcrossDist, comp_DepthAcrossDist_dB, Detection, Mask] = sonar_polarEchogram(b_SampleBeam_dB, DataSSP, 'otherImage', comp_SampleBeam_dB, ...
        'RangeDetectionInSamples', DetectedRangeInSamples, ...
        'across', across, 'depth', depth, 'MaxImageSize', MaxImageSize, ...
        'typeAlgoWC', typeAlgoWC, 'MaskBeyondDetection', MaskBeyondDetection);
    %         'DataDepthAcrossDist', DataDepth.AcrossDist(subDepth(k),:), 'DataDepthAlongDist', DataDepth.AlongDist(subDepth(k),:));
    if ~flagPing
        continue
    end
    % c_DepthAcrossDist(1) : image en amplitude, c_DepthAcrossDist(2) : image en dB
    % comp_DepthAcrossDist_dB(1) : image non masqu�e, comp_DepthAcrossDist_dB(2) : image masqu�e
    
    %% S�lection du type de repr�sentation de la r�flectivit�
    
    WcOutRaw  = c_DepthAcrossDist(2);
    WcOutComp = comp_DepthAcrossDist_dB(2);
    
    CLimOutComp = CLimCompIn;
    CLimOutRaw  = CLimRawIn;
    
    WcOutComp = extraction(WcOutComp, 'x', WcOutRaw.x, 'y', WcOutRaw.y, 'ForceExtraction', 'NoStats');
    
    WcOutRaw.CLim  = CLimOutRaw;
    WcOutComp.CLim = CLimOutComp;
    
    %% Ecriture des echogrammes sur disque
    
    TransducerDepth = -abs(DataDepth.TransducerDepth(subPings,1));
    if TypeWCOutput == 1
%         TransducerDepth = TransducerDepth + TideValue(subPings);
        TransducerDepth = TransducerDepth + TideValue(kGroup); % modif JMA le 26/03/2021 pour donn�es AUV pings flagu�s � partir du cap
    end
    
    [flagPing, DataPingMat] = WC_prepareImageNetcdfPing(WcOutRaw, WcOutComp, Mask, sliceNames, TransducerDepth, kGroup, ...
        Latitude(kGroup), Longitude(kGroup), Heading(kGroup), TimePing, ...
        'MaskBeyondDetection', MaskBeyondDetection, 'Detection', Detection);
    if ~flagPing
        continue
    end
    
    flagPings(kGroup) = flagPing;
    
    %% Ecriture des echogrammes polaires dans le fichier netcdf
    
    NetcdfGlobe3DUtils.WC_writeImageNetcdfPing(ncID, DataPingMat);

    %% Fermeture et reouverture du fichier afin de lib�rer la m�moire

    if mod(kGroup, 100) == 0
        netcdf.close(ncID);
        ncID = netcdf.open(WcNetcdfFilename, 'WRITE');
    end
    
    %% Display
    
    if mod(kGroup, StepDisplay) == 0
        [Fig, hAxe, hImage, hPing, hCurve, firstDisplay] = WC_DisplayResults(Fig, hAxe, hImage, hPing, hCurve, firstDisplay, ...
            TimePing, kGroup, WcOutRaw, WcOutComp, Detection, CLimOutRaw, CLimOutComp);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60);

% Fermeture
netcdf.close(ncID);

%% Tests si tout s'est bien termin�

if all(~flagPings)
    flag = 0;
    return
end

if ~AtLeastOneWCFound
    flag = 0;
    return
end

%% Renommage de l'extension .NC en .nc pour indiquer � l'utilisateur que le fichier est termin�

my_rename(WcNetcdfFilename, strrep(WcNetcdfFilename, '.NC', '.nc'));

my_close(Fig);
