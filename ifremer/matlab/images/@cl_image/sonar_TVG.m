% Calcul d'une image representant le TVG applique sur l'image
%
% Syntax
%   b = sonar_TVG(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx       : subsampling in X
%   suby       : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_TVG(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = sonar_TVG(this, varargin)

N = length(this);
str1 = 'Calcul des images de la TVG.';
str2 = 'Computing images of TVG';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, that] = sonar_TVG_unitaire(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end
my_close(hw, 'MsgEnd')


function [flag, this] = sonar_TVG_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Range]               = getPropertyValue(varargin, 'Range',               []);
[varargin, AbsorptionCoeffRT]   = getPropertyValue(varargin, 'AbsorptionCoeffRT',   []);
[varargin, AbsorptionCoeffSSc]  = getPropertyValue(varargin, 'AbsorptionCoeffSSc',  []);
[varargin, AbsorptionCoeffUser] = getPropertyValue(varargin, 'AbsorptionCoeffUser', []); %#ok<ASGLU>

%% Contr�les

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

if (this.GeometryType == cl_image.indGeometryType('PingAcrossDist')) && isempty(this.Sonar.Height)
    str1 = sprintf('Pas de hauteur renseign�e. Veuillez la definir ou la calculer.');
    str2 = sprintf('No "Height" signal. Please define or compute it.');
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

if ~isempty(Range)
    identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
    identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
    identTwoWayTravelTimeInSamples = cl_image.indDataType('TwoWayTravelTimeInSamples');
    
    % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
    % flipud (voir cl_image/plus
    
    [~, ~, subx, suby, subxRange, subyRange] = intersectionImages(this, subx, suby, Range);
    % FAUX : il ne faut pas utiliser get_RangeResol car la
    % r�solution ne d�pend plus du mode de la fr�quence
    % d'�chantillonage
    %     resolution      = get_RangeResol(SonarDescription, this.Sonar.PortMode_1);
    
    %     fe = get(this, 'SampleFrequency'); % Pas de chance cette info n'est
    %     disponible que dans seabedImage
    %     resolution = 1500 ./ (2 * fe(suby));
    
    SonarHeight     = get(this, 'SonarHeight');
    SampleFrequency = get(Range, 'SampleFrequency');
    
    x = this.x(subx);
    DistanseObliqueEnMetres = NaN(length(suby), length(subx));
    Range.Image = Range.Image(:,:);
    for k=1:length(subyRange)
        pppp = x;
        pppp(isnan(Range.Image(subyRange(k), subxRange))) = NaN;
        [~, RangeLine] = min(abs(pppp)); % Modif JMA le 19/03/2021 my_nanmin avant
        
        switch length(SampleFrequency)
            case 0
                resolution = abs(SonarHeight(suby(k)) ./ Range.Image(subyRange(k), subxRange(RangeLine)));
            otherwise
                resolution = 1500 / (2 * SampleFrequency(subyRange(k))); % TODO : 1500 !!!
        end
        switch Range.DataType
            case identRayPathSampleNb
                DistanseObliqueEnMetres(k,:) = Range.Image(subyRange(k), subxRange) .* resolution;
            case identTwoWayTravelTimeInSamples
                DistanseObliqueEnMetres(k,:) = Range.Image(subyRange(k), subxRange) .* resolution;
            case identTwoWayTravelTimeInSeconds
                DistanseObliqueEnMetres(k,:) = Range.Image(subyRange(k), subxRange) .* (1500/2);
            otherwise
                my_warndlg('sonar_TVG not complete, please send your project to sonarscope@ifremer.fr', 1);
        end
    end
end

%% Traitement

this.Image = zeros(length(suby), length(subx), 'single');
if isempty(Range)
    switch this.GeometryType
        case cl_image.indGeometryType('PingRange')
            DistanseObliqueEnMetres = repmat(abs(this.x(subx)), length(suby), 1);
            
        case cl_image.indGeometryType('PingSamples')
            DistanseObliqueEnMetres = repmat(abs(this.x(subx)) * this.Sonar.RawDataResol, length(suby), 1);
            
        case cl_image.indGeometryType('PingAcrossDist')
            DistanseObliqueEnMetres = NaN(size(this.Image), 'single');
            x2 = this.x(subx) .^ 2;
            H = this.Sonar.Height;
            subNaN = find(isnan(H));
            subNonNaN = find(~isnan(H));
            H(subNaN) = interp1(subNonNaN, H(subNonNaN), subNaN);
            for k=1:length(suby)
                h2 = H(suby(k)) .^ 2;
                DistanseObliqueEnMetres(k,:) = sqrt(x2 + h2);
            end
            
        case cl_image.indGeometryType('PingAcrossSample')
            DistanseObliqueEnMetres = NaN(size(this.Image), 'single');
            x2 = this.x(subx) .^ 2;
            H = this.Sonar.Height;
            subNaN = find(isnan(H));
            subNonNaN = find(~isnan(H));
            H(subNaN) = interp1(subNonNaN, H(subNonNaN), subNaN);
            for k=1:length(suby)
                h2 = H(suby(k)) .^ 2;
                DistanseObliqueEnMetres(k,:) = sqrt(x2 + h2) * this.Sonar.ResolAcrossSample(suby(k),1);
            end
            
        case cl_image.indGeometryType('PingBeam')
            my_warndlg('In geometry PingBeam I need layer "Range" to correct TVG, please import it before.', 1);
            flag = 0;
            return
    end
end

TVG.etat                 = this.Sonar.TVG.etat;
TVG.origine              = this.Sonar.TVG.origine;
TVG.ConstructTypeCompens = this.Sonar.TVG.ConstructTypeCompens;
TVG.ConstructAlpha       = this.Sonar.TVG.ConstructAlpha;
TVG.ConstructConstante   = this.Sonar.TVG.ConstructConstante;
TVG.ConstructCoefDiverg  = this.Sonar.TVG.ConstructCoefDiverg;
TVG.ConstructTable       = this.Sonar.TVG.ConstructTable;
TVG.IfremerAlpha         = this.Sonar.TVG.IfremerAlpha;
TVG.IfremerConstante     = this.Sonar.TVG.IfremerConstante;
TVG.IfremerCoefDiverg    = this.Sonar.TVG.IfremerCoefDiverg;

% sub = find(DistanseObliqueEnMetres == 0);
sub = (DistanseObliqueEnMetres == 0);
DistanseObliqueEnMetres(sub) = NaN;

if TVG.etat == 1 % Compens�
    switch TVG.origine
        case 1  % Compensation Constructeur
            if TVG.ConstructTypeCompens == 1 % Compensation par Alpha
                if isempty(AbsorptionCoeffRT) % pas de layer d'AbsorptionCoeff fourni
                    if isempty(this.Sonar.AbsorptionCoeff_RT) % Pas de signal "AbsorptionCoeff_RT" trouv�
                        alpha = TVG.ConstructAlpha;
                    else % Par d�pit on prend la valeur globale fournie dans les renseignements g�n�raux
                        alpha = this.Sonar.AbsorptionCoeff_RT(suby);
                        nbBeams = size(DistanseObliqueEnMetres,2);
                        alpha = repmat(alpha,1,nbBeams);
                    end
                else
                    [~, ~, ~, ~, subxAbsorptionCoeff, subyAbsorptionCoeff] = intersectionImages(this, subx, suby, AbsorptionCoeffRT);
                    alpha = AbsorptionCoeffRT.Image(subyAbsorptionCoeff,subxAbsorptionCoeff); % Un layer "AbsorptionCoeff" est fourni
                end
                beta  = TVG.ConstructConstante;
                Gamma = TVG.ConstructCoefDiverg;
                X =  (Gamma * log10(DistanseObliqueEnMetres)) + ((2 * alpha / 1000) .* DistanseObliqueEnMetres) + beta;
                this = replace_Image(this, X);
            else    % Compensation par Table
                if size(TVG.ConstructTable, 1) == 2
                    disp('PAS TESTE')
                    tableD = TVG.ConstructTable(1, :);   % Distance en m
                    tableG = TVG.ConstructTable(2, :);   % TVG en dB
                    
                    Gain = DistanseObliqueEnMetres;
                    N = numel(Gain);
                    for k=1:1e6:N
                        sub = k:k+1e6-1;
                        sub(sub > N) = [];
                        Gain(sub) = interp1(tableD, tableG, DistanseObliqueEnMetres(sub));
                    end
                    this = replace_Image(this, Gain);
                else
                    disp('GESTION EMISSION_SECTOR A FAIRE')
                end
            end
            %             TVG.origine = 1;
            
        case 2 % Compensation Ifremer
            if isempty(AbsorptionCoeffSSc) % pas de layer d'AbsorptionCoeffSSc fourni
                if isempty(this.Sonar.AbsorptionCoeff_SSc) % Pas de signal "AbsorptionCoeff_SSc" trouv�
                    my_breakpoint;
                    % if isempty(alpha)
                        alpha = TVG.IfremerAlpha;
                    % end
                else % Par d�pit on prend la valeur globale fournie dans les renseignements g�n�raux
                    alpha = this.Sonar.AbsorptionCoeff_SSc(suby);
                    nbBeams = size(DistanseObliqueEnMetres,2);
                    alpha = repmat(alpha,1,nbBeams);
                end
            else
                [~, ~, ~, ~, subxAbsorptionCoeff, subyAbsorptionCoeff] = intersectionImages(this, subx, suby, AbsorptionCoeffSSc);
                alpha = AbsorptionCoeffSSc.Image(subyAbsorptionCoeff,subxAbsorptionCoeff); % Un layer "AbsorptionCoeffSSc" est fourni
            end
            beta  = TVG.IfremerConstante;
            Gamma = TVG.IfremerCoefDiverg;
            X =  (Gamma * log10(DistanseObliqueEnMetres)) + ((2 * alpha / 1000) .* DistanseObliqueEnMetres) + beta;
            this = replace_Image(this, X);
            %             TVG.origine = 2;
            
        case 3  % Compensation User
            % TODO : cr�er this.Sonar.AbsorptionCoeff_User ?
            if isempty(AbsorptionCoeffUser) % pas de layer d'AbsorptionCoeffUser fourni
                %                 if isempty(this.Sonar.AbsorptionCoeff_User) % Pas de signal "AbsorptionCoeff_User" trouv�
                %                     if isempty(alpha)
                %                         alpha = TVG.IfremerAlpha;
                %                     end
                %                 else % Par d�pit on prend la valeur globale fournie dans les renseignements g�n�raux
                %                     alpha = this.Sonar.AbsorptionCoeff_User(suby);
                %                     nbBeams = size(DistanseObliqueEnMetres,2);
                %                     alpha = repmat(alpha,1,nbBeams);
                %                 end
            else
                [~, ~, ~, ~, subxAbsorptionCoeff, subyAbsorptionCoeff] = intersectionImages(this, subx, suby, AbsorptionCoeffUser);
                alpha = AbsorptionCoeffUser.Image(subyAbsorptionCoeff,subxAbsorptionCoeff); % Un layer "AbsorptionCoeffUser" est fourni
            end
            beta  = TVG.IfremerConstante;
            Gamma = TVG.IfremerCoefDiverg;
            X = NaN([this.nbRows this.nbColumns], 'single');
            X(subyAbsorptionCoeff,:) = (Gamma * log10(DistanseObliqueEnMetres(subyAbsorptionCoeff,:))) + ((2 * alpha / 1000) .* DistanseObliqueEnMetres(subyAbsorptionCoeff,:)) + beta;
            this = replace_Image(this, X);
            %             TVG.origine = 3;
    end
else  % Not compensated
    this.Image(:) = 0; % TODO faire quelque-chose ici pour �viter de passer par ce qui suit : tester this = 0;
end

% this.Sonar.TVG = TVG;

%     this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'dB';
this.ColormapIndex = 3;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'TVG');

