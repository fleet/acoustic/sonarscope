function [flag, this] = sonar_TVG_Compensation(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Range]               = getPropertyValue(varargin, 'Range', []);
[varargin, AbsorptionCoeffRT]   = getPropertyValue(varargin, 'AbsorptionCoeffRT', []);
[varargin, AbsorptionCoeffSSc]  = getPropertyValue(varargin, 'AbsorptionCoeffSSc', []);
[varargin, AbsorptionCoeffUser] = getPropertyValue(varargin, 'AbsorptionCoeffUser', []); %#ok<ASGLU>

flag = 0;

%% Attempt to compensate the image with User TVG

if ~isempty(AbsorptionCoeffUser)
    [flag, a] = sonar_TVG_CompensationUser(this, 'subx', subx, 'suby', suby, ...
        'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    if flag
        this = a;
        return
    else
        messageAboutCalibration('Tag', 'TVG')
    end
end

%% Attempt to compensate the image with SSc TVG

if ~isempty(AbsorptionCoeffSSc)
    [flag, a] = sonar_TVG_CompensationSSc(this, 'subx', subx, 'suby', suby, ...
        'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    if flag
        a.Sonar.TVG.IfremerAlpha = AbsorptionCoeffSSc.StatValues.Moyenne; % TODO : cette valeur n'est pas récupérée dans get_BS_Status !!! Pourquoi ?
        this = a;
        return
    else
        %         messageAboutCalibration('Tag', 'TVG')
    end
end

%% Attempt to compensate the image with RT TVG

if ~isempty(AbsorptionCoeffSSc)
    [flag, a] = sonar_TVG_CompensationRT(this, 'subx', subx, 'suby', suby, ...
        'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    if flag
        this = a;
        return
    else
        %         messageAboutCalibration('Tag', 'TVG')
    end
end
