% Calcul d'une image representant la TVG personnalisée sur une image géoréférencée
%
% Syntax
%   b = sonar_TVG_CompensationIfremerLatLong(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx       : subsampling in X
%   suby       : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_TVG_CompensationIfremerLatLong(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_TVG_CompensationIfremerLatLong(this, Bathymetry, Angle, alpha, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% Test si Reflectivity et LatLong ou GeoYX
flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), ...
    'GeometryType', [cl_image.indGeometryType('LatLong') cl_image.indGeometryType('GeoYX')]);
if ~flag
    return
end

SonarTVG_IfremerAlphaOld = this.Sonar.TVG.IfremerAlpha;
if (this.Sonar.TVG.etat == 1) && (this.Sonar.TVG.origine == 2) && ...
        (alpha == SonarTVG_IfremerAlphaOld)
    flag = 0;
    return
end

[~, ~, subx, suby, subxBathymetry, subyBathymetry, subxAngle, subyAngle] = ...
    intersectionImages(this, subx, suby, Bathymetry, Angle);

Image = this.Image(suby,subx);

nbRows = length(suby);
str = this.Name;
str(length(str)+1:150) = '.';
str1 = sprintf('IFREMER - SonarScope : Processing Ifremer TVG compensation');
hw = create_waitbar(str, 'Entete', str1, 'N', nbRows);
for k=1:nbRows
    my_waitbar(k, nbRows, hw);
    Z = Bathymetry.Image(subyBathymetry(k),subxBathymetry);
    A = Angle.Image(subyAngle(k),subxAngle);
    R = abs(Z ./ cosd(A));
    Image(k,:) = Image(k,:) + ((alpha - SonarTVG_IfremerAlphaOld) * 2 / 1000) * R;
end
my_close(hw, 'MsgEnd');

this = replace_Image(this, Image);

%% Nouvelle valeur de alpha

this.Sonar.TVG.IfremerAlpha = alpha;

%% Mise a jour de coordonnées

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'TVG Compensation Ifremer');
