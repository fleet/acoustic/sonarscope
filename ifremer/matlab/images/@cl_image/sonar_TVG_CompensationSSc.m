% Calcul d'une image representant le TVG personnalise
% TODO
%
% Syntax
%   [flag, b] = sonar_TVG_CompensationSSc(a, ...)
%
% Input Arguments
%   a : A Reflectivity cl_image instance
%
% Name-Value Pair Arguments
%   subx     : Sub-sampling in abscissa
%   suby     : Sub-sampling in ordinates
%   Range    : TODO
%   AlphaNew : TODO
%
% Output Arguments
%   flag  : 1=OK, 0=KO
%   b     : Updated instance of cl_image
%
% Examples
%   [flag, b] = sonar_TVG_CompensationSSc(a)
%     imagesc(a)
%     imagesc(b)
%
% See also SonarEtatBS SonarBestBSConstructeur Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_TVG_CompensationSSc(this, varargin)

[varargin, Range]               = getPropertyValue(varargin, 'Range', []);
[varargin, AbsorptionCoeffRT]   = getPropertyValue(varargin, 'AbsorptionCoeffRT', []);
[varargin, AbsorptionCoeffSSc]  = getPropertyValue(varargin, 'AbsorptionCoeffSSc', []);
[varargin, AbsorptionCoeffUser] = getPropertyValue(varargin, 'AbsorptionCoeffUser', []); %#ok<ASGLU>

flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), ...
    'GeometryType', [cl_image.indGeometryType('PingBeam') cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist')]);
if flag
    SonarTVG_etat    = this.Sonar.TVG.etat;
    SonarTVG_origine = this.Sonar.TVG.origine;
    if (SonarTVG_etat == 1) && (SonarTVG_origine == 2)
        flag = 0;
        return
    end
    
    ImageName = this.Name;
    this = set(this, 'SonarTVG_etat', 1, 'SonarTVG_origine', 2, 'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    
    this.Name = ImageName;
    this = update_Name(this, 'Append', 'TVG-SSc');
end
