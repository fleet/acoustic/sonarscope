function [flag, this] = sonar_TVG_CompensationUser(this, varargin)

[varargin, Range]               = getPropertyValue(varargin, 'Range', []);
[varargin, AbsorptionCoeffRT]   = getPropertyValue(varargin, 'AbsorptionCoeffRT', []);
[varargin, AbsorptionCoeffSSc]  = getPropertyValue(varargin, 'AbsorptionCoeffSSc', []);
[varargin, AbsorptionCoeffUser] = getPropertyValue(varargin, 'AbsorptionCoeffUser', []); %#ok<ASGLU>

flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), ...
    'GeometryType', [cl_image.indGeometryType('PingBeam') cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist')]);
if flag
    SonarTVG_etat    = this.Sonar.TVG.etat;
    SonarTVG_origine = this.Sonar.TVG.origine;
    if (SonarTVG_etat == 1) && (SonarTVG_origine == 3)
        flag = 0;
        return
    end
    
    ImageName = this.Name;
    this = set(this, 'SonarTVG_etat', 1, 'SonarTVG_origine', 3, 'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    
    this.Name = ImageName;
    this = update_Name(this, 'Append', 'TVG-User');
end
