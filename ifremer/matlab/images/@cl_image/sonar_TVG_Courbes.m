% Plot the applied TVG in real time and the postprocessed ones
%
% Syntax
%   sonar_TVG_Courbes(a, ...) 
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Examples 
%   TODO : a = 
%   sonar_TVG_Courbes(a)
%
% See also sonar_TVG_SSc sonar_TVG_NoCompensation 
%            sonar_TVG_CompensationRT sonar_TVG_CompensationSSc Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function sonar_TVG_Courbes(this, varargin)

for k=1:length(this)
    sonar_TVG_Courbes_unitaire(this(k));
end

function sonar_TVG_Courbes_unitaire(this)

TVG.ConstructAlpha      = this.Sonar.TVG.ConstructAlpha;
TVG.ConstructConstante  = this.Sonar.TVG.ConstructConstante;
TVG.ConstructCoefDiverg = this.Sonar.TVG.ConstructCoefDiverg;
TVG.ConstructTable      = this.Sonar.TVG.ConstructTable;
TVG.IfremerAlpha        = this.Sonar.TVG.IfremerAlpha;
TVG.IfremerConstante    = this.Sonar.TVG.IfremerConstante;
TVG.IfremerCoefDiverg   = this.Sonar.TVG.IfremerCoefDiverg;

FigUtils.createSScFigure; hold on; grid on;
str1 = 'Correction de TVG (Divergence + Absorbtion)';
str2 = 'TVG correction (Divergence + Absorption)';
title(Lang(str1,str2))
str1 = 'Distance oblique (m)';
str2 = 'Range (m)';
xlabel(Lang(str1,str2))
ylabel('TVG (dB)')

%% Table constructeur

str = [];
if size(TVG.ConstructTable, 1) == 2
    xTable = TVG.ConstructTable(1,:);   % Distance en m
    yTable = TVG.ConstructTable(2,:);   % TVG en dB
    PlotUtils.createSScPlot(xTable, yTable, 'k')
    str1 = 'TVG Constructeur Courbe';
    str2 = 'Factory TVG curve';
    str{end+1} = Lang(str1,str2);
    DistMax = xTable(end);
else
    DistMax = Inf;
end

x = 1:10000;    % Distance en m
DistanseObliqueEnKm = x * 1e-3;

%% Courbe constructeur

alpha = TVG.ConstructAlpha;
beta  = TVG.ConstructConstante;
gamma = TVG.ConstructCoefDiverg;
if length(alpha) == 1
    y = (gamma * log10(DistanseObliqueEnKm * 1000)) + (2 * alpha * DistanseObliqueEnKm) + beta;
    sub = find(x <= DistMax);
    PlotUtils.createSScPlot(x(sub), y(sub), 'b');
    str1 = 'TVG Constructeur Analytique';
    str2 = 'Factory TVG';
    str{end+1} = Lang(str1,str2);
else
    str1 = 'Pb rencontr� dans "sonar_TVG_Courbes", sauvegardez votre session et envoyez la � JMA SVP.';
    str2 = 'Pb encontered in "sonar_TVG_Courbes", please save your session and send it to JMA.';
    my_warndlg(Lang(str1,str2), 1);
end

%% Courbe Ifremer

alpha = TVG.IfremerAlpha;
beta  = TVG.IfremerConstante;
gamma = TVG.IfremerCoefDiverg;
if length(alpha) == 1
    y = (gamma * log10(DistanseObliqueEnKm * 1000)) + (2 * alpha * DistanseObliqueEnKm) + beta;
    sub = find(x <= DistMax);
    PlotUtils.createSScPlot(x(sub), y(sub), 'r');
    str1 = 'TVG Ifremer Analytique';
    str2 = 'Personal TVG';
    str{end+1} = Lang(str1,str2);

elseif size(alpha, 1) == 1 % Alpha pour plusieurs antennes
    
    for k=1:size(alpha, 2)
        y = (gamma * log10(DistanseObliqueEnKm * 1000)) + (2 * alpha(1,k) * DistanseObliqueEnKm) + beta;
        sub = find(x <= DistMax);
        PlotUtils.createSScPlot(x(sub), y(sub), 'r');
        str1 = sprintf('TVG Ifremer Analytique, faisceau %d', k);
        str2 = sprintf('Personal analytical TVG, beam %d', k);
        str{end+1} = Lang(str1,str2); %#ok<AGROW>
    end
else  % Table en fonction de la profondeur
    str1 = 'Pb rencontr� dans "sonar_TVG_Courbes", sauvegardez votre session et envoyez la � JMA SVP.';
    str2 = 'Pb encontered in "sonar_TVG_Courbes", please save your session and send it to JMA.';
    my_warndlg(Lang(str1,str2), 1);
end

legend(str)
