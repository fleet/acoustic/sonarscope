% Compute an image where no TVG is applied
%
% Syntax
%   b = sonar_TVG_NoCompensation(a, ...)
%
% Input Arguments
%   a : Instance of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance of cl_image
%
% Examples
%   TODO : a =
%   b = sonar_TVG_NoCompensation(a)
%   imagesc(a)
%   imagesc(b)
%
% See also sonar_TVG_Courbes sonar_TVG_SSc
%            sonar_TVG_CompensationRT sonar_TVG_CompensationSSc Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_TVG_NoCompensation(this, varargin)

% TODO : G�rer le cas de plusieurs instances ou tester si une sule instance

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Range]               = getPropertyValue(varargin, 'Range', []);
[varargin, AbsorptionCoeffRT]   = getPropertyValue(varargin, 'AbsorptionCoeffRT', []);
[varargin, AbsorptionCoeffSSc]  = getPropertyValue(varargin, 'AbsorptionCoeffSSc', []);
[varargin, AbsorptionCoeffUser] = getPropertyValue(varargin, 'AbsorptionCoeffUser', []);
[varargin, alphaUser]           = getPropertyValue(varargin, 'alphaUser', []); %#ok<ASGLU>

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

SonarTVG_etat = this.Sonar.TVG.etat;
if SonarTVG_etat == 2
    flag = 0;
    return
end

if (length(subx) ~= this.nbColumns) || (length(suby) ~= this.nbRows)
    [this, flag] = extraction(this, 'suby', suby, 'subx', subx);
    if ~flag
        return
    end
end

ImageName = this.Name;
[this, flag] = set(this, 'SonarTVG_etat', 2, 'Range', Range, ...
    'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
    'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
    'AbsorptionCoeffUser', AbsorptionCoeffUser, 'alphaUser', alphaUser);
if ~isempty(this) % TODO : pas tr�s heureux cela !!!
    this.Name = ImageName;
    this = update_Name(this, 'Append', 'NoTVG');
end
