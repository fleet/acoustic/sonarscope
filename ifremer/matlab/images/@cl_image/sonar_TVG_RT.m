% Compute an image where the realtime TVG is applied
%
% Syntax
%   b = sonar_TVG_RT(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   TODO : a =
%   b = sonar_TVG_RT(a)
%   imagesc(a)
%   imagesc(b)
%
% See also sonar_TVG_Courbes sonar_TVG_SSc sonar_TVG_NoCompensation
%            sonar_TVG_CompensationRT sonar_TVG_CompensationSSc Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_TVG_RT(this, varargin)

N = length(this);
str1 = 'Calcul des images de la TVG constructeur';
str2 = 'Computing images of Constructor TVG';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, that] = sonar_TVG_RT_unitaire(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end
my_close(hw, 'MsgEnd')


function [flag, this] = sonar_TVG_RT_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

if (length(subx) ~= this.nbColumns) || (length(suby) ~= this.nbRows)
    [this, flag] = extraction(this, 'suby', suby, 'subx', subx);
    if ~flag
        return
    end
end

ImageName = this.Name;
this = set(this, 'Sonar_DefinitionENCours', 'SonarTVG_etat', 1, 'SonarTVG_origine', 1);
this = sonar_TVG(this, varargin{:});

this.DataType = cl_image.indDataType('TVG');
this.Name = ImageName;
this = update_Name(this, 'Append', 'RT');
