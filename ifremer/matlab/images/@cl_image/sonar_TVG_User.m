% Compute an image where a personnal Time Varying Gain is applied
%
% Syntax
%   b = sonar_TVG_User(a, ...) 
%
% Input Arguments
%   a : Instance of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance of cl_image
%
% Examples 
%   TODO : a = 
%   b = sonar_TVG_User(a)
%   imagesc(a)
%   imagesc(b)
%
% See also sonar_TVG_Courbes sonar_TVG_SSc sonar_TVG_NoCompensation 
%            sonar_TVG_CompensationRT Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : G�rer le cas de plusieurs instances ou tester si une sule instance

function [flag, this] = sonar_TVG_User(this, varargin)

identReflectivity = cl_image.indDataType('Reflectivity');
flag = testSignature(this, 'DataType', identReflectivity, 'GeometryType', 'PingXxxx');

if flag
    ImageName = this.Name;
    this = set(this, 'Sonar_DefinitionENCours', 'SonarTVG_etat', 1, 'SonarTVG_origine', 3);
    this = sonar_TVG(this, varargin{:});

    this.DataType = cl_image.indDataType('TVG');
    this.Name = ImageName;
    this = update_Name(this, 'Append', 'User');
end
