% Computes an image corresponding to the applyed TVG
%
% Syntax
%   b = sonar_TVG_appliquer(a, ...)  % TODO : renommer en sonar_TVG_apply
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   TODO
%   b = sonar_TVG_appliquer(a)
%   imagesc(a)
%   imagesc(b)
%
% See also sonar_TVG sonar_TVG_NoCompensation Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = sonar_TVG_appliquer(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [that, flag] = unitaire_sonar_TVG_appliquer(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end


function [this, flag] = unitaire_sonar_TVG_appliquer(this, varargin)

[varargin, Range]               = getPropertyValue(varargin, 'Range', []);
[varargin, AbsorptionCoeffRT]   = getPropertyValue(varargin, 'AbsorptionCoeffRT', []);
[varargin, AbsorptionCoeffSSc]  = getPropertyValue(varargin, 'AbsorptionCoeffSSc', []);
[varargin, AbsorptionCoeffUser] = getPropertyValue(varargin, 'AbsorptionCoeffUser', []);
[varargin, alpha]               = getPropertyValue(varargin, 'alpha', []); %#ok<ASGLU>

if ~isempty(alpha)
    my_warndlg('Maintenance � faire ici : unitaire_sonar_TVG_appliquer alpha', 1);
end

%% Contr�les

flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), ...
    'GeometryType', 'PingXxxx');
if ~flag
    return
end

if (this.GeometryType == cl_image.indGeometryType('PingAcrossDist')) && isempty(this.Sonar.Height)
    str1 = 'Pas de hauteur renseign�e. Veuillez la definir ou la calculer.';
    str2 = 'No height could be found. Please proceed to create one.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

TVG_precedent = this;
if isfield(TVG_precedent.Sonar.TVG, 'etat_Old')
    TVG_precedent.Sonar.TVG.etat = TVG_precedent.Sonar.TVG.etat_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'origine_Old')
    TVG_precedent.Sonar.TVG.origine = TVG_precedent.Sonar.TVG.origine_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'ConstructTypeCompens_Old')
    TVG_precedent.Sonar.TVG.ConstructTypeCompens = TVG_precedent.Sonar.TVG.ConstructTypeCompens_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'ConstructAlpha_Old')
    TVG_precedent.Sonar.TVG.ConstructAlpha = TVG_precedent.Sonar.TVG.ConstructAlpha_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'ConstructConstante_Old')
    TVG_precedent.Sonar.TVG.ConstructConstante = TVG_precedent.Sonar.TVG.ConstructConstante_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'ConstructCoefDiverg_Old')
    TVG_precedent.Sonar.TVG.ConstructCoefDiverg = TVG_precedent.Sonar.TVG.ConstructCoefDiverg_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'ConstructTable_Old')
    TVG_precedent.Sonar.TVG.ConstructTable = TVG_precedent.Sonar.TVG.ConstructTable_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'IfremerAlpha_Old')
    TVG_precedent.Sonar.TVG.IfremerAlpha = TVG_precedent.Sonar.TVG.IfremerAlpha_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'IfremerConstante_Old')
    TVG_precedent.Sonar.TVG.IfremerConstante = TVG_precedent.Sonar.TVG.IfremerConstante_Old;
end
if isfield(TVG_precedent.Sonar.TVG, 'IfremerCoefDiverg_Old')
    TVG_precedent.Sonar.TVG.IfremerCoefDiverg = TVG_precedent.Sonar.TVG.IfremerCoefDiverg_Old;
end

flagTVGDifferent = 0;
if this.Sonar.TVG.etat ~= TVG_precedent.Sonar.TVG.etat
    flagTVGDifferent = 1;
elseif this.Sonar.TVG.origine ~= TVG_precedent.Sonar.TVG.origine
    flagTVGDifferent = 1;
elseif this.Sonar.TVG.ConstructTypeCompens ~= TVG_precedent.Sonar.TVG.ConstructTypeCompens
    flagTVGDifferent = 1;
else
    if this.Sonar.TVG.ConstructTypeCompens == 1
        if (this.Sonar.TVG.ConstructAlpha           ~= TVG_precedent.Sonar.TVG.ConstructAlpha) || ...
                (this.Sonar.TVG.ConstructConstante  ~= TVG_precedent.Sonar.TVG.ConstructConstante) || ...
                (this.Sonar.TVG.ConstructCoefDiverg ~= TVG_precedent.Sonar.TVG.ConstructCoefDiverg) || ...
                (this.Sonar.TVG.IfremerAlpha        ~= TVG_precedent.Sonar.TVG.IfremerAlpha)
            flagTVGDifferent = 1;
        end
    elseif this.Sonar.TVG.ConstructTypeCompens == 2
        if ~isequal(this.Sonar.TVG.ConstructTable, TVG_precedent.Sonar.TVG.ConstructTable)
            flagTVGDifferent = 1;
        end
    end
end

if flagTVGDifferent
    [TVG, flag] = sonar_TVG(TVG_precedent, 'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    if ~flag
        return
    end
    this = this - TVG;
    
    [TVG, flag] = sonar_TVG(this, 'Range', Range, ...
        'AbsorptionCoeffRT',   AbsorptionCoeffRT, ...
        'AbsorptionCoeffSSc',  AbsorptionCoeffSSc, ...
        'AbsorptionCoeffUser', AbsorptionCoeffUser);
    if ~flag
        return
    end
    this = this + TVG;
    
    this.Name = TVG_precedent.Name;
end

%% Mise � jour de coordonn�es

% this = majCoordonnees(this, subx, suby); % SUITE BUG MIKAEL 30/05/06
% Probl�me venant de this = this - TVG

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'TVG applique');
