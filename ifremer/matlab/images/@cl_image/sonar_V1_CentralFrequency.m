function this = sonar_V1_CentralFrequency(this, Fe, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contr�les

identTxBeamIndex = cl_image.indDataType('TxBeamIndex');

flag = testSignature(this, 'DataType', identTxBeamIndex, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    this = [];
    return
end

%% Traitement

I = this.Image(suby,subx);
J = NaN(length(suby), length(subx), 'single');

for k1=1:length(suby)
    IPing = I(suby(k1),:);
    listValues = unique(IPing(~isnan(IPing)));
    for k2=1:length(listValues)
        kSector = listValues(k2);
        sub = (IPing == kSector);
        
        % Modif JMA 21/01/2016 : Modifier plut�t l'import : un seul secteur !!!!!!!!!!!!!!!!!!!!!
        if kSector > size(Fe,2)
            kSector = size(Fe,2);
        end
        % Fin Modif JMA
        
        
        J(k1,sub) = Fe(suby(k1), kSector) / 1000;
    end
end

this.Image  = J;
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

[this, flag] = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
if flag
    CLim = [this.StatValues.Min this.StatValues.Max];
    this.CLim = CLim;
end

this.Unit = 'kHz';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Frequency');
this = update_Name(this);
