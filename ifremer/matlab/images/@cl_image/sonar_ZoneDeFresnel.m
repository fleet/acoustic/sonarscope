% Calcul de la resolution longitudinale
%
% Syntax
%   b = sonar_ZoneDeFresnel(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_ZoneDeFresnel(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_ZoneDeFresnel(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_sonar_ZoneDeFresnel(this(k), CasDepth, H, CasDistance, Bathy, Emission, varargin{:});
end


function this = unitaire_sonar_ZoneDeFresnel(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Angle d'emission

if isempty(Emission)
    AngleEmission = [];
else
    AngleEmission = abs(Emission(1).Image(suby, subx));
end

%% Information de hauteur

if CasDistance == 1 % On<possede un layer de bathymetrie
    H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
else
    switch CasDepth
        case 1  % Il existe un layer 'Depth'
            H = abs(Bathy(1).Image(suby, subx));
        case 2  % Il existe une hauteur sonar
            H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
        case 3  % On possede l'angle d'emission
            H = repmat(abs(this.x(subx)), length(suby), 1) ./ tan(AngleEmission * (pi/180)); % On possede l'angle d'emission
            H(isinf(H)) = NaN;
        case 4  % On fixe une hauteur arbitraire
        otherwise
            my_warndlg('Pas d''information de hauteur', 1);
            this = [];
            return
    end
end

%% Information de distance

switch CasDistance
    case 1  % Cas d'une donnee sonar en distance oblique
        D = repmat(abs(this.x(subx)), length(suby), 1);
    case 2  % Cas d'une donnee sonar en distance projet�e
        D = sqrt(repmat(this.x(subx), length(suby), 1) .^ 2 + H .^ 2);
    case 3  % On poss�de l'angle e r�ception
        D = H ./ cos(abs(Emission(1).Image(suby, subx)) * pi / 180);
    otherwise
        my_warndlg('Pas d''information de distance', 1);
        this = [];
        return
end

subNan = isnan(this.Image(suby, subx));
D(subNan) = NaN;

%% Calcul de la r�solution

Sonar = get(this, 'SonarDescription');
DistanceFresnel = get(Sonar, 'Signal.DistanceFresnel');
ChampLointain   = get(Sonar, 'Signal.ChampLointain');

this = replace_Image(this, D * 0);
% sub = find(D < ChampLointain);
sub = (D < ChampLointain);
this.Image(sub) = 1;
% sub = find(D < DistanceFresnel);
sub = (D < DistanceFresnel);
this.Image(sub) = 2;

this.ValNaN = NaN;

%% Mise � jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'm';
this.ColormapIndex = 3;

this.Comments = '1 : Fresnel zone, 2 : Farfield';

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', 'FresnelZone');
