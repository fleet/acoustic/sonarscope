% Calcul de l'aire insonifiee de la bathymetrie
%
% Syntax
%   b = sonar_aire_bathy(aKM, CasDepth, H, CasDistance, Bathy, Emission, ...)
%
% Input Arguments
%   aKM         : Instance de cl_image
%   CasDepth    : 1=Il existe un layer 'Depth', 2=Il existe une hauteur sonar, 3=On possede l'angle d'emission, 4=On fixe une hauteur arbitraire
%   H           : [] ou Hauteur sonar � donner
%   CasDistance : 1=Cas d'une donnee sonar en distance oblique, 2=Cas d'une donnee sonar en distance projetee, 3=On possede l'angle d'emission
%   Bathy       : [] ou instance de cl_image contenant la bathymetrie
%   Emission    : [] ou instance de cl_image contenant l'angle d'emission
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', [9 1 4], 'Carto', Carto);
%
%   Reflectivite = get_Image(b, 1);
%   imagesc(Reflectivite)
%   Bathy = get_Image(b, 2);
%   imagesc(Bathy)
%   Emission = get_Image(b, 3);
%   imagesc(Emission)
%
%   Aire = sonar_aire_bathy(Bathy, 1, [], 3, Bathy, Emission);
%   imagesc(Aire)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_aire_bathy(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_sonar_aire_bathy(this(i), CasDepth, H, CasDistance, Bathy, Emission, varargin{:});
end


function this = unitaire_sonar_aire_bathy(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Recherche ou calcul de l'angle d'emission, de la hauteur et de la distance oblique

[subx, suby, AngleEmission, D, H] = sonar_angle_nadir(this, CasDepth, H, CasDistance, Bathy, Emission, ...
    'subx', subx, 'suby', suby); % TODO : voir si il faut faire , 'FlagRoll', '0'

%% Calcul de la resolution

Sonar = get(this, 'SonarDescription');
ResolLong  = resol_long(Sonar, D);
ResolTrans = resol_trans_bathy(Sonar, H, AngleEmission);

this = replace_Image(this, ResolLong .* ResolTrans);
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'm^2';
this.ColormapIndex = 3;

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('FootprintArea');
Append = '_Aire insonifiee Bathy_';
this = update_Name(this, 'Append', Append);

