% Calcul de l'aire insonifiee en imagerie
%
% Syntax
%   [flag, b] = sonar_aire_insonifiee_dB(a, CasDepth, H, CasDistance, Bathy, Emission, origine, ...)
%
% Input Arguments
%   a       : Instance de cl_image
%   origine : 1=Constructeur, 2=Ifremer
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', [9 1 4], 'Carto', Carto);
%
%   Reflectivite = get_Image(b, 1);
%   imagesc(Reflectivite)
%   Bathy = get_Image(b, 2);
%   imagesc(Bathy)
%   Emission = get_Image(b, 3);
%   imagesc(Emission)
%
%   [flag, Aire] = sonar_aire_insonifiee_dB(Bathy, 1, [], 3, Bathy, Emission, 1);
%   imagesc(Aire)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : Attention danger, ici, on consid�re que tous les layers ont les
% m�mes x et y, il faut introduire intersectionImages

function [flag, this, SonarAireInso_origine_New] = sonar_aire_insonifiee_dB(this, Range, CasDepth, H, CasDistance, Bathy, Emission, origine, varargin)

[varargin, ComputeMask] = getPropertyValue(varargin, 'ComputeMask', 0);

SonarAireInso_origine_New = [];

N = length(this);
str1 = 'Calcul des images "Insonified Area"';
str2 = 'Computing images of "Insonified Area"';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    if isempty(Range)
        R = [];
    else
        R = Range(k);
    end
    [flag, that, SonarAireInso_origine_New] = sonar_aire_insonifiee_dB_unitaire(this(k), R, CasDepth, H, CasDistance, Bathy, Emission, origine, varargin{:});
    if flag
        this(k) = that(1);
        Mask(k) = that(2); %#ok<AGROW>
    else
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')

if ComputeMask
    this = [this; Mask];
    this = this(:);
end


function [flag, this, SonarAireInso_origine_New] = sonar_aire_insonifiee_dB_unitaire(this, R, CasDepth, H, CasDistance, Bathy, Emission, origine, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, SlopeAcross] = getPropertyValue(varargin, 'SlopeAcross', []);
[varargin, SlopeAlong]  = getPropertyValue(varargin, 'SlopeAlong', []);
[varargin, TxBeamIndex] = getPropertyValue(varargin, 'TxBeamIndex', []); %#ok<ASGLU>

if isempty(SlopeAcross) || isempty(SlopeAlong)
    SlopeAcross = [];
    SlopeAlong  = [];
    SonarAireInso_origine_New = 2;
else
    SonarAireInso_origine_New = 3;
end

I0 = cl_image_I0;

flag = testSignature(Emission, 'DataType', cl_image.indDataType('RxAngleEarth'));
if ~flag
    return
end

ns2 = floor(this.nbColumns / 2);
if mod(this.nbColumns,2) == 0
    subBab = [true(1,ns2)  false(1,ns2)];
    subTri = [false(1,ns2) true(1,ns2)];
else
    subBab = [true(1,ns2+1)  false(1,ns2)];
    subTri = [false(1,ns2+1) true(1,ns2)];
end
subBab = subBab(subx);
subTri = subTri(subx);

if isempty(R)
    
    %% Recherche ou calcul de l'angle d'�mission, de la hauteur et de la distance oblique
    
    [subx, suby, ~, D_m] = sonar_angle_nadir(this, CasDepth, H, CasDistance, Bathy, Emission, ...
        'subx', subx, 'suby', suby); % TODO : voir si il faut faire , 'FlagRoll', '0'
    
else
    
    SoundSpeed_mps = R.Sonar.SurfaceSoundSpeed(suby,:);
    if all(isnan(SoundSpeed_mps)) || all(SoundSpeed_mps == 0)
        SoundSpeed_mps(:) = mean(this.Sonar.BathyCel.C(1:end-1), 'omitnan');% TODO : faire mieux que �a
    end
    
    switch R.DataType
        case cl_image.indDataType('TwoWayTravelTimeInSeconds')
            D_m = bsxfun(@times, R.Image(suby,subx), SoundSpeed_mps/2);
        case {cl_image.indDataType('TwoWayTravelTimeInSamples'); cl_image.indDataType('RayPathSampleNb')}
            F =  2* R.Sonar.SampleFrequency(suby);
            D_m = bsxfun(@times, R.Image(suby,subx), SoundSpeed_mps ./ F(:));
        case cl_image.indDataType('RayPathLength')
            D_m = R.Image(suby,subx);
        otherwise
            str1 = sprintf('Le layer de type "%s" n''est pas encore branch� dans "sonar_aire_insonifiee_dB", contactez JMA', cl_image.strDataType{R.DataType});
            str2 = sprintf('The layer of type "%s" is not plugged yet in "sonar_aire_insonifiee_dB", please email JMA', cl_image.strDataType{R.DataType});
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
    end
end

%% Calcul de la r�solution

% Voir comment c'est fait dans diagramme_Rx
Sonar = get(this, 'SonarDescription');
sonarFamily = get(Sonar, 'Sonar.Family');

InsonifiedArea = NaN(size(D_m), 'single');
M = zeros(size(D_m), 'uint8');

this = update_SonarDescription_Ping(this, []);

RxBeamWidthAlongTrack_deg = get(this.Sonar.Desciption, 'RxBeamWidthAlongTrack');
if isempty(RxBeamWidthAlongTrack_deg) || (RxBeamWidthAlongTrack_deg == 0) % Ceinture et bretelles
    RxBeamWidthAlongTrack_deg = 90;
end

%{
TxBeamWidthAcrossTrack = get(this.Sonar.Desciption, 'TxBeamWidthAcrossTrack');
if isempty(TxBeamWidthAcrossTrack) || (TxBeamWidthAcrossTrack == 0) % Ceinture et bretelles
TxBeamWidthAcrossTrack = 180;
end
%}

%% Cr�ation du layer PulseLengthImage

pulseLength_EffectiveSignal = this.SignalsVert.getSignalByTag('PulseLength_Effective');
[flag, PLImage] = create_LayerFromSignalVert(this, pulseLength_EffectiveSignal, 'Index', TxBeamIndex);
if ~flag
    return
end

%% Algo

N = length(suby);
str1 = sprintf('Calcul de l''aire insonifi�e : %s', I0.Sonar.AireInso.strOrigine{SonarAireInso_origine_New});
str2 = sprintf('Processing Insonified Area : %s',   I0.Sonar.AireInso.strOrigine{SonarAireInso_origine_New});
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    if isempty(TxBeamIndex)
        nbSectors = [];
    else
        Sector = TxBeamIndex.Image(suby(k), subx);
        nbSectors = max(Sector);
    end
    
    % TODO : Th�oriquement plus utile
    this = update_SonarDescription_Ping(this, suby(k), 'nbSectors', nbSectors, 'Mute', 1);
    
    %% get RxAcrossBeamWidth_deg
    rxAcrossBeamWidthSignal = this.SignalsVert.getSignalByTag('RxAcrossBeamWidth');
    [status, RxAcrossBeamWidth_deg, Unit] = rxAcrossBeamWidthSignal.getValueMatrix('subl', suby(k)); %#ok<ASGLU>
    if status
        %         RxBeamWidth_deg = AntenneOuvEmiRec(RxAcrossBeamWidth_deg, TxBeamWidthAcrossTrack);
        RxBeamWidth_deg = RxAcrossBeamWidth_deg;
    else % TODO : Th�oriquement plus utile
        RxBeamWidth_deg = get(this.Sonar.Desciption, 'BeamForm.Rx.TransWth');
    end
    
    %% get TxBeamWidth_deg
    txAlongBeamWidthSignal = this.SignalsVert.getSignalByTag('TxAlongBeamWidth');
    [status, TxAlongBeamWidth_deg, Unit] = txAlongBeamWidthSignal.getValueMatrix('subl', suby(k)); %#ok<ASGLU>
    if status
        TxBeamWidth_deg = AntenneOuvEmiRec(TxAlongBeamWidth_deg, RxBeamWidthAlongTrack_deg);
    else % TODO : Th�oriquement plus utile
        TxBeamWidth_deg = get(this.Sonar.Desciption, 'LongBeamWth');
    end
    
    %% Algo
    
    ResolAlong_m = D_m(k,:) * (2 * tand(TxBeamWidth_deg / 2));
    
    if isempty(SlopeAcross)
        BeamPointingAngle_deg = Emission.Image(suby(k), subx);
    else
        %{
FigUtils.createSScFigure(8765);
hc(1) = subplot(3,1,1); PlotUtils.createSScPlot(Emission.Image(suby(k), subx)); grid on; title('RxBeamAngle')
hc(2) = subplot(3,1,2); PlotUtils.createSScPlot(SlopeAcross.Image(k,subx)); grid on; title('SlopeAcross')
hc(3) = subplot(3,1,3); PlotUtils.createSScPlot(Emission.Image(suby(k), subx) - SlopeAcross.Image(k,:)); grid on; title('RxBeamAngle - SlopeAcross')
linkaxes(hc, 'x')
        %}
        
        % TODO : Dimitrios
        BeamPointingAngle_deg = (Emission.Image(suby(k), subx) - SlopeAcross.Image(suby(k),subx));
        %         BeamPointingAngle_deg = (Emission.Image(suby(k), subx) + SlopeAcross.Image(suby(k),subx));
    end
    w = warning;
    warning('off', 'all')
    
    % TODO : Dimitrios007
    %     Deltax_m =  C_mps ./ (2 * BandWth * 1e3);
    C_mps = 1500;
    
    T_s = PLImage.Image(suby(k),:) * 1e-3;
    
    Deltax_m =  C_mps .* T_s ./ 2;
    
    % TODO : utiliser l'angle / Hull
    RxBeamWidthDegDepointe_deg = getRxBeamWidthDepointe(RxBeamWidth_deg, BeamPointingAngle_deg, this.Sonar.Ship.Arrays.Receive.Roll, subBab, subTri);
    % RxBeamWidthDegDepointe_deg = RxBeamWidth_deg;
    
    %   R1_m = Deltax_m ./ sind(abs(BeamPointingAngle_deg));
    Ra_m = D_m(k,:);
    R1_m = abs(Ra_m .* sind(BeamPointingAngle_deg) .* (sqrt(1 + (C_mps .* T_s ./ (Ra_m .* (sind(BeamPointingAngle_deg)) .^ 2))) - 1));
    
    if ~isempty(SlopeAlong)
        R1_m = R1_m ./ cosd(SlopeAlong.Image(suby(k),subx));
    end
    
    zPing_m = D_m(k,:) .* cosd(abs(BeamPointingAngle_deg)); % TODO : AngleEarth SVP
    R2_m = abs((tand((BeamPointingAngle_deg + RxBeamWidthDegDepointe_deg/2) ) - tand((BeamPointingAngle_deg - RxBeamWidthDegDepointe_deg/2)))) .* zPing_m;
    %     FigUtils.createSScFigure(8578); PlotUtils.createSScPlot(R1_m, 'b'); grid on; hold on; PlotUtils.createSScPlot(R2_m, 'k');  PlotUtils.createSScPlot(min(R1_m,R2_m), 'r'); hold off; legend({'R1_m';'R2_m';'Min'})
    
    warning(w)
    % 	ResolAcross_m = min(R1_m,R2_m);
    [ResolAcross_m, Indices] = min([R1_m;R2_m]);
    M(k,:) = Indices;
    
    if sonarFamily == 1 % Case of sidescan sonar
        R3 = sqrt(D_m(k,:) .* cosd(BeamPointingAngle_deg) * (2*Deltax_m));
        ResolAcross_m = min(R3, ResolAcross_m);
    end
    InsonifiedArea(k,:) = ResolAlong_m .* ResolAcross_m;
end
my_close(hw, 'MsgEnd')

this = replace_Image(this, reflec_Enr2dB(InsonifiedArea));
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'dB';
this.ColormapIndex = 3;

%% Compl�tion du titre

if origine == 1
    strOrigine = 'Factory';
else
    strOrigine = 'Ifremer';
end

this.Comments = '';

this.DataType = cl_image.indDataType('InsonifiedAreadB');
this = update_Name(this, 'Append', strOrigine);

%% Image du mask : 1=AN, 2=A0

M(isnan(InsonifiedArea)) = 0;
this(2) = replace_Image(this, M);
this(2).ValNaN = 0;
this(2) = compute_stats(this(2));
this(2).CLim = [0 3];
this(2).Unit = ' ';
this(2).DataType = cl_image.indDataType('Mask');
this = update_Name(this);

flag = 1;

