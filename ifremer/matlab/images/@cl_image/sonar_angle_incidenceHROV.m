% Calcul de l'angle d'incidence
%
% Syntax
%   [flag, a] = sonar_angle_incidence(Emission, Heading, SlopeN, SlopeW, ...) 
%
% Input Arguments
%   Emission : Instance de cl_image contenant l'angle d'�mission
%   Heading  : Instance de cl_image contenant le cap
%   SlopeN   : Instance de cl_image contenant la slope North-South
%   SlopeW   : Instance de cl_image contenant la slope East-West
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   a : Instance de cl_image contenant l'angle d'incidence
%
% Examples 
%   [flag, a] = sonar_angle_incidence(Emission, Heading, SlopeN, SlopeW)
%
% See also cl_image Authors
% Authors : JMA + ALV
% ----------------------------------------------------------------------------

function [flag, a] = sonar_angle_incidenceHROV(Emission, AlongBeamPoitingAngle, SlopeN, SlopeW, Range, TxBeamIndex, ...
    acrossDistRefTxAntenna, alongDistRefTxAntenna, bathy, varargin)
    
[subx, suby, varargin] = getSubxSuby(Emission, varargin);

[varargin, flagWaitbar] = getPropertyValue(varargin, 'flagWaitbar', 1); %#ok<ASGLU>

a  = [];

flag = testSignature(Emission, 'DataType', [cl_image.indDataType('TxAngle') cl_image.indDataType('BeamPointingAngle') cl_image.indDataType('RxAngleEarth')]);
if ~flag
    return
end

flag = testSignature(SlopeN, 'DataType', cl_image.indDataType('SlopeAzimuth'));
if ~flag
    return
end

flag = testSignature(SlopeW, 'DataType', cl_image.indDataType('SlopeAzimuth'));
if ~flag
    return
end

% TODO testSignature AlongBeamPoitingAngle

%% Calcul de l'intersection des images

% Attention : sort(sublLM) peut �tre utile pour supprimer les tests de flipud (voir cl_image/plus
     
[~, ~, subx, suby, subxP, subyP, subxAlongBeamPoitingAngle, subyAlongBeamPoitingAngle, ...
    subxRange, subyRange, subxTxBeamIndex, subyTxBeamIndex] = ...
    intersectionImages(Emission, subx, suby, SlopeN, AlongBeamPoitingAngle, Range, TxBeamIndex); %#ok<ASGLU>
if isempty(subx) || isempty(suby)
    flag = 0;
    return
end

%% Range

SoundSpeed = Range.Sonar.SurfaceSoundSpeed(:,:);
if size(SoundSpeed,1) == 1
    SoundSpeed = SoundSpeed';
end
if all(isnan(SoundSpeed)) || all(SoundSpeed == 0)
    SoundSpeed(:) = mean(Range.Sonar.BathyCel.C(1:end-1), 'omitnan');% TODO : faire mieux que �a
end

switch Range.DataType
    case cl_image.indDataType('TwoWayTravelTimeInSeconds')
        ImageRangeInMeters  = bsxfun(@times, Range.Image(:,:), SoundSpeed/2);
    case {cl_image.indDataType('TwoWayTravelTimeInSamples'); cl_image.indDataType('RayPathSampleNb')}
        SampleFrequency =  2* Range.Sonar.SampleFrequency(:);
        ImageRangeInMeters  = bsxfun(@times, Range.Image(:,:), SoundSpeed ./ SampleFrequency(:));
    case cl_image.indDataType('RayPathLength')
        ImageRangeInMeters  = Range.Image(:,:);
        my_breakpoint
%         SampleFrequency = 
    otherwise
        str1 = sprintf('Le layer de type "%s" n''est pas encore branch� dans "sonar_angle_incidence", contactez JMA', cl_image.strDataType{Range.DataType});
        str2 = sprintf('The layer of type "%s" is not plugged yet in "sonar_angle_incidence", please email JMA', cl_image.strDataType{Range.DataType});
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
end

%% Calcul de l'angle d'incidence

nbRows = length(suby);
nbCol = length(subx);

try
    J = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    J = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    PAcross_deg = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    PAcross_deg = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    PAlong_deg = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    PAlong_deg = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    InsonifiedArea = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    InsonifiedArea = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    WCSignalWidthInSeconds = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    WCSignalWidthInSeconds = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

piS180 = pi / 180;
ns2 = floor(Emission.nbColumns / 2);
if mod(Emission.nbColumns,2) == 0
    subBab = [true(1,ns2)  false(1,ns2)];
    subTri = [false(1,ns2) true(1,ns2)];
else
    subBab = [true(1,ns2+1)  false(1,ns2)];
    subTri = [false(1,ns2+1) true(1,ns2)];
end
subBab = subBab(subx);
subTri = subTri(subx);

RxBeamWidthAlongTrack = get(Range.Sonar.Desciption, 'RxBeamWidthAlongTrack');
if isempty(RxBeamWidthAlongTrack) || (RxBeamWidthAlongTrack == 0) % Ceinture et bretelles
    RxBeamWidthAlongTrack = 90;
end

%{
TxBeamWidthAcrossTrack = get(Range.Sonar.Desciption, 'TxBeamWidthAcrossTrack');
if isempty(TxBeamWidthAcrossTrack) || (TxBeamWidthAcrossTrack == 0) % Ceinture et bretelles
    TxBeamWidthAcrossTrack = 180;
end
%}

%% Cr�ation du layer PulseLengthImage

pulseLength_EffectiveSignal = Range.SignalsVert.getSignalByTag('PulseLength_Effective');
[flag, PLImage] = create_LayerFromSignalVert(Range, pulseLength_EffectiveSignal, 'Index', TxBeamIndex);
if ~flag
    return
end

%% Lecture de certains signaux

rxAcrossBeamWidthSignal = Range.SignalsVert.getSignalByTag('RxAcrossBeamWidth');
[status, RxAcrossBeamWidth_deg, Unit] = rxAcrossBeamWidthSignal.getValueMatrix(); %#ok<ASGLU>
if status
    ValRxBeamWidth_deg = RxAcrossBeamWidth_deg;
else
    ValRxBeamWidth_deg = [];
end

txAlongBeamWidthSignal = Range.SignalsVert.getSignalByTag('TxAlongBeamWidth');
[status, TxAlongBeamWidth_deg, Unit] = txAlongBeamWidthSignal.getValueMatrix(); %#ok<ASGLU>
if status
    ValTxBeamWidth_deg = AntenneOuvEmiRec(TxAlongBeamWidth_deg, RxBeamWidthAlongTrack);
else % TODO : Th�oriquement plus utile
    ValTxBeamWidth_deg = [];
end

%% Algo

EmissionImage = Emission.Image(:,:);
SonarName = get(Range.Sonar.Desciption, 'SonarName');
if flagWaitbar
    hw = create_waitbar(Emission.Name, 'Entete', 'IFREMER - SonarScope : Processing incidence angle', 'N', nbRows);
else
    hw = [];
end
for k=1:nbRows
    my_waitbar(k, nbRows, hw);
    
    kRange = subyRange(k);
    
    switch SonarName % TODO : Th�oriquement plus utile
        case {'EM2040'; 'EM2040S'; 'EM2040D'}
            SonarMode_1 = Range.Sonar.NbSwaths(kRange);
%             SonarMode_1 = get(Range, 'Mode1');
%             SonarMode_2 = get(Range, 'Mode2');
            SonarMode_2 = get_mode2(Range);
%             SonarMode_3 = get(Range, 'Mode3');
            SonarMode_3 = get_mode3(Range);
%             SonarMode_1 = SonarMode_1(kRange);
            SonarMode_2 = SonarMode_2(kRange);
            SonarMode_3 = SonarMode_3(kRange);
            Range = update_SonarDescription_PingEM2040(Range,  k == 1, SonarMode_1,  SonarMode_2, SonarMode_3);
        otherwise
            Range = update_SonarDescription_Ping(Range, kRange);
    end
            
    %% get TxBeamWidthDeg
    
    %{
    % TODO : TxAlongBeamWidth = NaN pour EM1002 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    txAlongBeamWidthSignal = Range.SignalsVert.getSignalByTag('TxAlongBeamWidth');
    [status, TxAlongBeamWidth, Unit] = txAlongBeamWidthSignal.getValueMatrix('subl', suby(k)); %#ok<ASGLU>
    if status
        BeamWidthAlongDeg = AntenneOuvEmiRec(TxAlongBeamWidth, RxBeamWidthAlongTrack);
    else % TODO : Th�oriquement plus utile
        BeamWidthAlongDeg = get(Range.Sonar.Desciption, 'LongBeamWth');
    end
    BeamWidthAlongRd = BeamWidthAlongDeg  * (pi/180);
    %}
    
    %% get RxAcrossBeamWidth
    
    %{
    rxAcrossBeamWidthSignal = Range.SignalsVert.getSignalByTag('RxAcrossBeamWidth');    
    [status, RxAcrossBeamWidth, Unit] = rxAcrossBeamWidthSignal.getValueMatrix('subl', suby(k)); %#ok<ASGLU>
    if status
%         BeamWidthAcrossDeg = AntenneOuvEmiRec(RxAcrossBeamWidth, TxBeamWidthAcrossTrack);
        BeamWidthAcrossDeg = RxAcrossBeamWidth;
    else % TODO : Th�oriquement plus utile
        BeamWidthAcrossDeg = get(Range.Sonar.Desciption, 'BeamForm.Rx.TransWth');
    end
    %}
    
        %% get TxBeamWidthDeg
    
    % TODO : on pourrait appeler getValueMatrix une seule fois (pour tous
    % les suby) mais l'op�ration ne semble pas prendre plus de temps que
    % �a. A voir quand il y aura un moment de r�pis
    
    % TODO : TxAlongBeamWidth = NaN pour EM1002 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if isempty(ValTxBeamWidth_deg)
        BeamWidthAlongDeg = get(this.Sonar.Desciption, 'LongBeamWth');
    else
        BeamWidthAlongDeg = ValTxBeamWidth_deg(suby(k));
    end
    BeamWidthAlongRd = BeamWidthAlongDeg  * (pi/180);
    
    %% get RxAcrossBeamWidth
    
    if isempty(ValRxBeamWidth_deg)
        BeamWidthAcrossDeg = get(this.Sonar.Desciption, 'BeamForm.Rx.TransWth');
    else
        BeamWidthAcrossDeg = ValRxBeamWidth_deg(suby(k));
    end
    
    %% Elargissement de l'angle d'ouverture des faisceaux en fonction de la longueur apparente

 %  BeamWidthAcross_rd  = BeamWidthAcrossDeg * (pi/180);
    BeamPointingAngleDeg = double(EmissionImage(suby(k),subx));
    RxBeamWidthDegDepointe = getRxBeamWidthDepointe(BeamWidthAcrossDeg, BeamPointingAngleDeg, Emission.Sonar.Ship.Arrays.Receive.Roll, subBab, subTri);
    BeamWidthAcross_rd = RxBeamWidthDegDepointe * (pi/180);
   
    %% Algo
    
    C = SoundSpeed(kRange,1); % TODO : pas top !
    
    TetaBeamAngleEarth_rd = BeamPointingAngleDeg * piS180;
    subNonNaN = find(~isnan(TetaBeamAngleEarth_rd));
    if isempty(subNonNaN)
        continue
    end
    TetaBeamAngleEarth_rd  = TetaBeamAngleEarth_rd(subNonNaN);
    RxBeamWidthDegDepointe = RxBeamWidthDegDepointe(subNonNaN);
    
    TetaHeading = double(Emission.Sonar.Heading(suby(k))) * piS180;

    TetaSlopeNorth = double(SlopeN.Image(subyP(k),subxP(subNonNaN))) * piS180;
    if all(isnan(TetaSlopeNorth))
        continue
    end
    TetaSlopeWest = double(SlopeW.Image(subyP(k),subxP(subNonNaN))) * piS180;
%     figure; plot(TetaSlopeWest / piS180, 'b'); grid on; hold on; plot(TetaSlopeNorth / piS180, 'r'); legend({'TetaSlopeWest'; 'TetaSlopeNorth'})

    TetaAlongBeamPoitingAngle = double(AlongBeamPoitingAngle.Image(subyAlongBeamPoitingAngle(k),subxAlongBeamPoitingAngle(subNonNaN))) * piS180;



% On fait comme si le terrain �tait plat
% TetaSlopeNorth(:) = 0;
% TetaSlopeWest(:)  = 0;

% On fait comme si il n'y avait pas d'angle 45 deg
% TetaAlongBeamPoitingAngle(:) = 0;

    
    %% Compute the two vectors :  UErefEarth : Acoustic wave, NrefEarth : Normal to the seafloor

%{
    %% CALCUL SYMBOLIQUE DE TOUS LES VECTEURS ET MATRICES DE ROTATION : n�cessite la "Symbolic Math Toolbox" 
    
syms TetaBeamAngleEarth_rd TetaAlongBeamPoitingAngle TetaHeading TetaSlopeNorth TetaSlopeWest

RotPlus  = [cos(TetaHeading)  sin(TetaHeading) 0 ; -sin(TetaHeading) cos(TetaHeading) 0 ; 0 0 1]
RotMinus = [cos(TetaHeading) -sin(TetaHeading) 0 ;  sin(TetaHeading) cos(TetaHeading) 0 ; 0 0 1]

% % Vecteur de d�placement de l'onde acoustique
V1 = [-sin(TetaBeamAngleEarth_rd) ; 0                               ; cos(TetaBeamAngleEarth_rd)]
% V2 = [0                           ; -sin(TetaAlongBeamPoitingAngle) ; cos(TetaAlongBeamPoitingAngle)]
    
RotTang = [1 0 0
0 cos(TetaAlongBeamPoitingAngle) -sin(TetaAlongBeamPoitingAngle)
0 sin(TetaAlongBeamPoitingAngle) cos(TetaAlongBeamPoitingAngle)]
    
UErefShip = RotTang * V1
        
% UErefShip =
%   -sin(TetaBeamAngleEarth_rd)
%  -cos(TetaBeamAngleEarth_rd)*sin(TetaAlongBeamPoitingAngle)
%   cos(TetaBeamAngleEarth_rd)*cos(TetaAlongBeamPoitingAngle)

% % Vecteur normal au DTM
V1 = [cos(TetaSlopeWest) ; 0                   ; sin(TetaSlopeWest)]
V2 = [0                  ; cos(TetaSlopeNorth) ; sin(TetaSlopeNorth)]
NrefEarth = cross(V1,V2)
% NrefEarth = cross(V2,V1)
    
% % NrefEarth =
% %  -cos(TetaSlopeNorth)*sin(TetaSlopeWest)
% %  -cos(TetaSlopeWest)*sin(TetaSlopeNorth)
% %   cos(TetaSlopeWest)*cos(TetaSlopeNorth)

NrefShip = RotMinus * NrefEarth
    
%   NrefShip =  cos(TetaSlopeWest)*sin(TetaHeading)*sin(TetaSlopeNorth) - cos(TetaHeading)*cos(TetaSlopeNorth)*sin(TetaSlopeWest)
%              -cos(TetaHeading)*cos(TetaSlopeWest)*sin(TetaSlopeNorth) - cos(TetaSlopeNorth)*sin(TetaHeading)*sin(TetaSlopeWest)
%               cos(TetaSlopeWest)*cos(TetaSlopeNorth)
%}
           
    %% Veteur onde acoustique
    
% %  NrefEarth = cross(V1,V2)
    UErefShip =[-sin(TetaBeamAngleEarth_rd)
        -cos(TetaBeamAngleEarth_rd).*sin(TetaAlongBeamPoitingAngle)
         cos(TetaBeamAngleEarth_rd).*cos(TetaAlongBeamPoitingAngle)];
     
%     figure(55452); quiver3(zeros(1,size(UErefShip,2)), UErefShip(1,:), UErefShip(2,:), UErefShip(3,:), 'ShowArrowHead', 'off'); grid on

%     normeN = sqrt(sum(UErefShip .^ 2));
%     UErefShip = bsxfun(@rdivide, UErefShip, normeN);
    
    %% Vecteur normal au MNT
    
    NrefEarth = [-cos(TetaSlopeNorth) .* sin(TetaSlopeWest)
                 -cos(TetaSlopeWest)  .* sin(TetaSlopeNorth)
                  cos(TetaSlopeWest)  .* cos(TetaSlopeNorth)];
  
% %  NrefEarth = cross(V2,V1)
%    NrefEarth = [-cos(TetaSlopeNorth) .* sin(TetaSlopeWest)
%                  -cos(TetaSlopeWest)  .* sin(TetaSlopeNorth)
%                   cos(TetaSlopeWest)  .* cos(TetaSlopeNorth)];
    
    RotMinus = [cos(TetaHeading), -sin(TetaHeading), 0; ...
                sin(TetaHeading),  cos(TetaHeading), 0; ...
                               0,                 0, 1];
                           
% 	RotPlus = [  cos(TetaHeading), sin(TetaHeading), 0; ...
%                 -sin(TetaHeading), cos(TetaHeading), 0; ...
%                                 0,                0, 1];

    NrefShip = RotMinus * NrefEarth;
 
%     NrefShip = [cos(TetaSlopeWest).*sin(TetaHeading).*sin(TetaSlopeNorth) - cos(TetaHeading).*cos(TetaSlopeNorth).*sin(TetaSlopeWest)
%         - cos(TetaHeading)*cos(TetaSlopeWest).*sin(TetaSlopeNorth) - cos(TetaSlopeNorth).*sin(TetaHeading).*sin(TetaSlopeWest)
%         cos(TetaSlopeWest).*cos(TetaSlopeNorth)];

    normeN = sqrt(sum(NrefShip .^ 2));
    NrefShip = bsxfun(@rdivide, NrefShip, normeN);
                                                                       
    %% Compute SlopeAcross and SlopeAlong
    
    SlopeAcross_deg = atan2d(NrefShip(3,:), NrefShip(1,:)) - 90;
    SlopeAlong_deg  = atan2d(NrefShip(3,:), NrefShip(2,:)) - 90;
    
    %{
    figure(45554);
    subplot(2,2,1); plot(TetaSlopeWest  * 180/pi); grid on; title('TetaSlopeWest');
    subplot(2,2,2); plot(TetaSlopeNorth * 180/pi); grid on; title('TetaSlopeNorth');
    subplot(2,2,3); plot(SlopeAcross_deg); grid on; title('SlopeAcross tourn�');
    subplot(2,2,4); plot(SlopeAlong_deg);  grid on; title('SlopeAlong tourn�');
    %}
    
    PAcross_deg(k,subNonNaN) = SlopeAcross_deg;
    PAlong_deg(k,subNonNaN)  = SlopeAlong_deg;

    %% Compute Incidence Angle

    X2 = dot(UErefShip, NrefShip);
    
    %{
    x = acrossDistRefTxAntenna.Image(k,subxP(subNonNaN));
    y = alongDistRefTxAntenna.Image(k,subxP(subNonNaN));
    z = bathy.Image(k,subxP(subNonNaN)); % zeros(size(x));
    figure(545122); 
    subplot(4,1,1); plot(x, y, '.-'); grid on; xlabel('Across dist (m)'); ylabel('Along dist (m)');
    subplot(4,1,2); plot(x, TetaAlongBeamPoitingAngle * 180/pi, '.-'); grid on; xlabel('Across dist (m)'); ylabel('TetaAlongBeamPoitingAngle (deg)');
    subplot(4,1,3); plot(x, SlopeAcross_deg, '.-'); grid on; xlabel('Across dist (m)'); ylabel('SlopeAcross_deg');
    subplot(4,1,4); plot(x, SlopeAlong_deg, '.-'); grid on; xlabel('Across dist (m)'); ylabel('SlopeAlong_deg');
    %}
    
    %{
    x = acrossDistRefTxAntenna.Image(k,subxP(subNonNaN));
    y = alongDistRefTxAntenna.Image(k,subxP(subNonNaN));
    z = bathy.Image(k,subxP(subNonNaN)); % zeros(size(x));
    z = bathy.Image(k,subxP(subNonNaN)); % zeros(size(x));
    figure(545222);
    ze = zeros(size(x));
    quiver3(x, y, ze, ze, ze, z, 'b'); grid on
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    %}
    
%{
if mod(k,100) == 1
    x = acrossDistRefTxAntenna.Image(k,subxP(subNonNaN));
    y = alongDistRefTxAntenna.Image(k,subxP(subNonNaN));
    z = bathy.Image(k,subxP(subNonNaN)); % zeros(size(x));
    z0 = bathy.Sonar.Immersion(k);
    subQuiv = 1:20:length(x);

    figure(10000+k);
    subplot(2,2,1)
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    view(0, 0); %  legend({'UErefShip'; 'NrefShip'});

    subplot(2,2,2)
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    view(0, 90); %  legend({'UErefShip'; 'NrefShip'});

    subplot(2,2,3)
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    view(90, 0); %  legend({'UErefShip'; 'NrefShip'});

    subplot(2,2,4)
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    
    figure(101);
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    view(0, 0); %  legend({'UErefShip'; 'NrefShip'});
    axis equal

    figure(102);
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    view(0, 90); %  legend({'UErefShip'; 'NrefShip'});
    axis equal

    figure(103);
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    view(90, 0); %  legend({'UErefShip'; 'NrefShip'});
    axis equal
    
    figure(104)
    quiver3(x(subQuiv), y(subQuiv), z(subQuiv), UErefShip(1,subQuiv), UErefShip(2,subQuiv), UErefShip(3,subQuiv), 'b', 'ShowArrowHead', 'off'); grid on
    hold on; quiver3(x(subQuiv), y(subQuiv), z(subQuiv), NrefShip(1,subQuiv), NrefShip(2,subQuiv), NrefShip(3,subQuiv), 'r', 'ShowArrowHead', 'off');
    plot3(0, 0, z0, '*k'); hold off;
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
    axis equal
    
    drawnow
end
%}
    
    angleIncidence_deg = acosd(X2);
%     angleIncidence_deg = angleIncidence_deg -90;
    
    angleIncidence_deg = angleIncidence_deg .* sign(TetaBeamAngleEarth_rd);
%     sub = (angleIncidence_deg > 180);
%     angleIncidence_deg(sub) = angleIncidence_deg(sub) - 360;
%     sub = (angleIncidence_deg < -180);
%     angleIncidence_deg(sub) = angleIncidence_deg(sub) + 360;
    J(k,subNonNaN) = angleIncidence_deg;
    % figure; plot(I, 'b'); grid on; hold on; plot(angleIncidence_deg, 'r*')
    
    %% Compute InsonifiedArea
    
	T_s = PLImage.Image(suby(k),:) * 1e-3;
    
    tiltAngle_deg = atan2d(UErefShip(3,:), UErefShip(2,:)) - 90;

    angleIncidenceAcrossSwath_deg = TetaBeamAngleEarth_rd *(180/pi) - SlopeAcross_deg;

    angleIncidenceAlongSwath_deg  = SlopeAlong_deg - tiltAngle_deg; % - 180
        
    PingRangeInMeters = ImageRangeInMeters(kRange,subxRange(subNonNaN));
    
    %% Extension footprint along valable quelque soit le r�gime
    
    AlongBeamFootPrintInMeters = PingRangeInMeters .* BeamWidthAlongRd ./ cosd(angleIncidenceAlongSwath_deg);
    
    %% Extension footprint across
    
    % R�gime impulsion courte, incidence rasante
    
%    % Ancienne formule de A1
%     AcrossSignalFootPrint = (C .* T_s(subNonNaN) / 2) ./ sind(abs(angleIncidenceAcrossSwath_deg));
%     A1 = (BeamWidthAlongRd * PingRangeInMeters) .* (AcrossSignalFootPrint ./ cosd(angleIncidenceAlongSwath_deg));
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(A1); grid; title('A1 Old')

% T_s=T_s*1.86;

%   % Nouvelle formule de A1
    X = abs(PingRangeInMeters .* sind(angleIncidenceAcrossSwath_deg) .* (sqrt(1 + (C .* T_s(subNonNaN) ./ (PingRangeInMeters .* (sind(angleIncidenceAcrossSwath_deg)) .^ 2))) - 1));
    AcrossSignalFootPrint = X;
    
    A1 = AlongBeamFootPrintInMeters .* AcrossSignalFootPrint;
    
    % R�gime impulsion longue proche du sp�culaire
    
    % A2 = (PingRangeInMeters .^2) .* (BeamWidthAlongRd * BeamWidthAcross_rd(subNonNaN)) ./ X2;
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(A2); grid; title('A2 Old')
    
    AcrossBeamFootPrintInMeters = PingRangeInMeters .* BeamWidthAcross_rd(subNonNaN) ./ cosd(angleIncidenceAcrossSwath_deg);
    A2 = AlongBeamFootPrintInMeters .* AcrossBeamFootPrintInMeters;
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(A2, 'r'); grid on; hold on; PlotUtils.createSScPlot(A1, 'b')
    
    %mA1A2 = min(A1,A2);
    mA1A2 = sqrt(1./(1./A1.^2+1./A2.^2));
    %{
        FigUtils.createSScFigure(88877);
        PlotUtils.createSScPlot(reflec_Enr2dB(A1), '-*b'); grid on; hold on; PlotUtils.createSScPlot(reflec_Enr2dB(A2), '-*k'); PlotUtils.createSScPlot(reflec_Enr2dB(mA1A2), 'or'); hold off; legend({'A1'; 'A2'; 'min'}); drawnow
    %}
    InsonifiedArea(k,subNonNaN) = mA1A2;
    
    %% Tentative de pr�diction de l'�paisseur du signal de la colonne d'eau / TODO : voir avec Ridha et Na�g pour obtenit les �quations parfaites
    
    %{
    % Equations JMA
    
%     SignalWidth = T_s(subNonNaN) * SampleFrequency(kRange) ./ cosd(angleIncidenceAcrossSwath_deg);
    SignalWidth1InSeconds = min(T_s) ./ cosd(angleIncidenceAcrossSwath_deg);
    SignalWidth2InSeconds = AcrossBeamFootPrintInMeters  / C;
    
    %     figure; plot(SignalWidth1InSeconds, 'b'); grid on; hold on; plot(SignalWidth2InSeconds, 'r')
    wheight = sind(abs(angleIncidenceAcrossSwath_deg));
%     figure; plot(wheight); grid
    SignalWidthInSeconds = wheight .* SignalWidth2InSeconds + (1-wheight) .* SignalWidth1InSeconds;
    SignalWidthInSeconds = SignalWidthInSeconds * 5;
    %{
    figure(56584);
    plot(SignalWidth1InSeconds, 'b'); grid on;
    hold on;
    plot(SignalWidth2InSeconds, 'r'); plot(SignalWidthInSeconds, 'k');
    hold off;
    %}
    WCSignalWidthInSeconds(k,subNonNaN) = SignalWidthInSeconds;
    %}
    
    % {
    % Equation Na�g
    
%     SignalWidth1InSeconds = min(T_s) ./ cosd(angleIncidenceAcrossSwath_deg);
%     SignalWidth2InSeconds = AcrossBeamFootPrintInMeters  / C;
    
    PingRangeInSeconds = PingRangeInMeters / (SoundSpeed(kRange)/2);
    absAlpha = 90 - abs(angleIncidenceAcrossSwath_deg);
    SignalWidthInSecondsMin = PingRangeInSeconds .* (1 - sind(absAlpha) ./ sind(absAlpha + RxBeamWidthDegDepointe/2));
    SignalWidthInSecondsMax = PingRangeInSeconds .* (1 - sind(absAlpha) ./ sind(absAlpha - RxBeamWidthDegDepointe/2));
    SignalWidthInSecondsMin = abs(SignalWidthInSecondsMin);
    SignalWidthInSecondsMax = abs(SignalWidthInSecondsMax);
%     figure(56586); plot(SignalWidthInSecondsMin, 'b'); grid on; hold on;  plot(SignalWidthInSecondsMax, 'k'); hold off;
    SignalWidthInSeconds = max(SignalWidthInSecondsMin, SignalWidthInSecondsMax);
    WCSignalWidthInSeconds(k,subNonNaN) = SignalWidthInSeconds;
    % }
end
my_close(hw, 'MsgEnd')

%% Cr�ation des instances de IncidenceAngle, slope Across et Along

a = replace_Image(Emission, J);
a.DataType = cl_image.indDataType('IncidenceAngle');
a = majCoordonnees(a, subx, suby);
a = compute_stats(a);
a.TagSynchroContrast = num2str(rand(1));
CLim = [a.StatValues.Min a.StatValues.Max];
a.CLim = CLim;
a.ColormapIndex = 3;
a.Unit = 'deg';

a(2) = replace_Image(a(1), PAcross_deg);
a(2).DataType = cl_image.indDataType('SlopeAcross');
a(2) = majCoordonnees(a(2));
a(2) = compute_stats(a(2));
a(2).TagSynchroContrast = num2str(rand(1));
CLim = [a(2).StatValues.Min a(2).StatValues.Max];
a(2).CLim = CLim;
a(2).Unit = 'deg';

a(3) = replace_Image(a(1), PAlong_deg);
a(3).DataType = cl_image.indDataType('SlopeAlong');
a(3) = majCoordonnees(a(3));
a(3) = compute_stats(a(3));
a(3).TagSynchroContrast = num2str(rand(1));
CLim = [a(3).StatValues.Min a(3).StatValues.Max];
a(3).CLim = CLim;
a(3).Unit = 'deg';

a(4) = replace_Image(a(1), reflec_Enr2dB(InsonifiedArea));
% a(4) = replace_Image(a(1), reflec_Amp2dB(InsonifiedArea)); % test JMA le 12/02/2018
a(4).DataType = cl_image.indDataType('InsonifiedAreadB');
a(4) = majCoordonnees(a(4));
a(4) = compute_stats(a(4));
a(4).TagSynchroContrast = num2str(rand(1));
CLim = [a(4).StatValues.Min a(4).StatValues.Max];
a(4).CLim = CLim;
a(4).Unit = 'dB';

a(5) = replace_Image(a(1), WCSignalWidthInSeconds);
a(5).DataType = cl_image.indDataType('WCSignalWidth');
a(5) = majCoordonnees(a(5));
a(5) = compute_stats(a(5));
a(5).TagSynchroContrast = num2str(rand(1));
CLim = [a(5).StatValues.Min a(5).StatValues.Max];
a(5).CLim = CLim;
a(5).Unit = 's';

%{
% Ajout JMA pour �viter bug fichier SPFE 0012_20100225_031309_Belgica.all
for k=1:length(a)
    a(k).y = Emission.y(suby);
    a(k).x = Emission.y(suby);
end
%}

%% Compl�tion du titre

InitialFileName = Emission.InitialFileName;
for k=1:length(a)
    if ~isempty(InitialFileName)
        [~, ImageName] = fileparts(InitialFileName);
        a(k) = update_Name(a(k), 'Name', ImageName);
    else
        a(k) = update_Name(a(k));
    end
end
% SonarScope(a(4))
% pi
