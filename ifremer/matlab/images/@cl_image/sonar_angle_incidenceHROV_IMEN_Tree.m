% Calcul de l'angle d'incidence
%
% Syntax
%   [flag, a] = sonar_angle_incidence(Emission, Heading, SlopeN, SlopeW, ...)
%
% Input Arguments
%   Emission : Instance de cl_image contenant l'angle d'�mission
%   Heading  : Instance de cl_image contenant le cap
%   SlopeN   : Instance de cl_image contenant la slope North-South
%   SlopeW   : Instance de cl_image contenant la slope East-West
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   a : Instance de cl_image contenant l'angle d'incidence
%
% Examples
%   [flag, a] = sonar_angle_incidence(Emission, Heading, SlopeN, SlopeW)
%
% See also cl_image Authors
% Authors : IMEN + Ridha + JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonar_angle_incidenceHROV_IMEN_Tree(Emission, AlongBeamPoitingAngle, SlopeN, SlopeW, ...
    Bathymetry_PingBeam, x_PingBeam, y_PingBeam, Range, TxBeamIndex, IBA, varargin)

[subx, suby, varargin] = getSubxSuby(Emission, varargin);

[varargin, TypeAlgoNormales] = getPropertyValue(varargin, 'TypeAlgoNormales', 1);
[varargin, flagWaitbar]      = getPropertyValue(varargin, 'flagWaitbar',      1); %#ok<ASGLU>

a  = [];

flag = testSignature(Emission, 'DataType', [cl_image.indDataType('TxAngle') cl_image.indDataType('BeamPointingAngle') cl_image.indDataType('RxAngleEarth')]);
if ~flag
    return
end

flag = testSignature(SlopeN, 'DataType', cl_image.indDataType('SlopeAzimuth'));
if ~flag
    return
end

flag = testSignature(SlopeW, 'DataType', cl_image.indDataType('SlopeAzimuth'));
if ~flag
    return
end

% TODO testSignature AlongBeamPoitingAngle

%% Calcul de l'intersection des images

% Attention : sort(sublLM) peut �tre utile pour supprimer les tests de flipud (voir cl_image/plus

[~, ~, subx, suby, subxP, subyP, subxAlongBeamPoitingAngle, subyAlongBeamPoitingAngle, ...
    subxRange, subyRange, subxTxBeamIndex, subyTxBeamIndex] = ...
    intersectionImages(Emission, subx, suby, SlopeN, AlongBeamPoitingAngle, Range, TxBeamIndex); %#ok<ASGLU>
if isempty(subx) || isempty(suby)
    flag = 0;
    return
end

%% Range

SoundSpeed = Range.Sonar.SurfaceSoundSpeed(:,:);
if size(SoundSpeed,1) == 1
    SoundSpeed = SoundSpeed';
end
if all(isnan(SoundSpeed)) || all(SoundSpeed == 0)
    SoundSpeed(:) = mean(Range.Sonar.BathyCel.C(1:end-1), 'omitnan');% TODO : faire mieux que �a
end

switch Range.DataType
    case cl_image.indDataType('TwoWayTravelTimeInSeconds')
        ImageRangeInMeters = bsxfun(@times, Range.Image(:,:), SoundSpeed/2);
    case {cl_image.indDataType('TwoWayTravelTimeInSamples'); cl_image.indDataType('RayPathSampleNb')}
        SampleFrequency    =  2 * Range.Sonar.SampleFrequency(:);
        ImageRangeInMeters = bsxfun(@times, Range.Image(:,:), SoundSpeed ./ SampleFrequency(:));
    case cl_image.indDataType('RayPathLength')
        ImageRangeInMeters = Range.Image(:,:);
        my_breakpoint
        %         SampleFrequency =
    otherwise
        str1 = sprintf('Le layer de type "%s" n''est pas encore branch� dans "sonar_angle_incidence", contactez JMA', cl_image.strDataType{Range.DataType});
        str2 = sprintf('The layer of type "%s" is not plugged yet in "sonar_angle_incidence", please email JMA', cl_image.strDataType{Range.DataType});
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
end

%% Calcul de l'angle d'incidence

nbRows = length(suby);
nbCol = length(subx);

try
    J = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    J = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    PAcross_deg = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    PAcross_deg = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    PAlong_deg = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    PAlong_deg = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    InsonifiedArea = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    InsonifiedArea = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

try
    WCSignalWidthInSeconds = NaN(nbRows, nbCol, 'single');
catch %#ok<CTCH>
    WCSignalWidthInSeconds = cl_memmapfile('Value', NaN, 'Size', [nbRows, nbCol], 'Format', 'single');
end

piS180 = pi / 180;
ns2 = floor(Emission.nbColumns / 2);
if mod(Emission.nbColumns,2) == 0
    subBab = [true(1,ns2)  false(1,ns2)];
    subTri = [false(1,ns2) true(1,ns2)];
else
    subBab = [true(1,ns2+1)  false(1,ns2)];
    subTri = [false(1,ns2+1) true(1,ns2)];
end
subBab = subBab(subx);
subTri = subTri(subx);

RxBeamWidthAlongTrack = get(Range.Sonar.Desciption, 'RxBeamWidthAlongTrack');
if isempty(RxBeamWidthAlongTrack) || (RxBeamWidthAlongTrack == 0) % Ceinture et bretelles
    RxBeamWidthAlongTrack = 90;
end

%% Cr�ation du layer PulseLengthImage

pulseLength_EffectiveSignal = Range.SignalsVert.getSignalByTag('PulseLength_Effective');
[flag, PLImage] = create_LayerFromSignalVert(Range, pulseLength_EffectiveSignal, 'Index', TxBeamIndex);
if ~flag
    return
end

%% Lecture de certains signaux

rxAcrossBeamWidthSignal = Range.SignalsVert.getSignalByTag('RxAcrossBeamWidth');
[status, RxAcrossBeamWidth_deg, Unit] = rxAcrossBeamWidthSignal.getValueMatrix(); %#ok<ASGLU>
if status
    ValRxBeamWidth_deg = RxAcrossBeamWidth_deg;
else
    ValRxBeamWidth_deg = [];
end

txAlongBeamWidthSignal = Range.SignalsVert.getSignalByTag('TxAlongBeamWidth');
[status, TxAlongBeamWidth_deg, Unit] = txAlongBeamWidthSignal.getValueMatrix(); %#ok<ASGLU>
if status
    ValTxBeamWidth_deg = AntenneOuvEmiRec(TxAlongBeamWidth_deg, RxBeamWidthAlongTrack);
else % TODO : Th�oriquement plus utile
    ValTxBeamWidth_deg = [];
end

%% Coordonn�es des sondes ref SSc

nx = length(subx);
ny = length(suby);
val_x_PingBeam_refFCS = x_PingBeam.Image(suby,subx);% reference Fix JMA
val_y_PingBeam_refFCS = y_PingBeam.Image(suby,subx);
val_z_PingBeam_refFCS = double(Bathymetry_PingBeam.Image(suby,subx));
subNonNaNPingBeam = find(~isnan(val_z_PingBeam_refFCS));
val_x_PingBeam_refFCS = val_x_PingBeam_refFCS(subNonNaNPingBeam);
val_y_PingBeam_refFCS = val_y_PingBeam_refFCS(subNonNaNPingBeam);
val_z_PingBeam_refFCS = val_z_PingBeam_refFCS(subNonNaNPingBeam);
[kX, kY] = meshgrid(1:length(subx), 1:ny);
kX = kX(subNonNaNPingBeam);
kY = kY(subNonNaNPingBeam);

%% Position du sonar

FishLatitude  = x_PingBeam.Sonar.FishLatitude;
FishLongitude = x_PingBeam.Sonar.FishLongitude;
FishImmersion = x_PingBeam.Sonar.Immersion;
FishHeading   = x_PingBeam.Sonar.Immersion;

AcrossD = x_PingBeam.Sonar.Ship.Arrays.Transmit.Y;
AlongD  = x_PingBeam.Sonar.Ship.Arrays.Transmit.X;

Carto = x_PingBeam.Carto;
[flag, Lat_TxArray, Lon_TxArray, XGeo_refFCS, YGeo_refFCS] = computePtsGeographicPositions(Carto, ...
    FishLatitude, FishLongitude, FishHeading, AcrossD, AlongD);
ZGeo_refFCS = double(FishImmersion);
% {
Fig = figure;
% subplot(1,2,1); plot(XGeo_refFCS, YGeo_refFCS); axis equal; axis tight; grid on;
% subplot(1,2,2);
pas = 1;
scatter3(val_x_PingBeam_refFCS(1:pas:end), val_y_PingBeam_refFCS(1:pas:end), val_z_PingBeam_refFCS(1:pas:end), ...
    [], val_z_PingBeam_refFCS(1:pas:end), '.');
grid on; hold on; colormap(jet(246)); colorbar
hc = plot3(XGeo_refFCS, YGeo_refFCS, ZGeo_refFCS, 'k');
xlabel('x__refFCS'); ylabel('y__refFCS'); zlabel('z__refFCS');
axis equal
%}

%% Algo

EmissionImage = Emission.Image(:,:);
SonarName = get(Range.Sonar.Desciption, 'SonarName');

% direction = [0.085 -0.89 0.43]; % TODO : mettre la position du sonar
% direction = [-0.89 0.085 -0.43]; % TODO : mettre la position du sonar

%     minX =  mean(val_y_PingBeam_refFCS);
%     minY =  max(val_x_PingBeam_refFCS);
%     minZ = -max(val_z_PingBeam_refFCS);

p_FCS = [val_x_PingBeam_refFCS, val_y_PingBeam_refFCS, val_z_PingBeam_refFCS];

msg = 'Compute knnsearch';
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch TypeAlgoNormales
    case 1 % knnsearch
        nomAlgo = 'knnsearch';
        nbVoisins = 25;
        [NeiborIndex, D] = knnsearch(p_FCS, p_FCS, 'Distance', 'euclidean', 'NSMethod','kdtree', 'K', nbVoisins);
        nbPoints = size(NeiborIndex, 1);
        
    case 2 % KDTreeSearcher
        nomAlgo = 'KDTreeSearcher';
        Mdl = KDTreeSearcher(p_FCS);
        nbPoints = size(Mdl.X, 1);
        
    case 3 % delaunayTriangulation
        nomAlgo = 'delaunayTriangulation';
        % Triangulate 2D
        %Commentaire Imen: C'est mieux de faire la triangulation pas sur x
        %y mais x2d y2d: trinagulation 2d dans le plan de la falaise
        DT = delaunayTriangulation(val_x_PingBeam_refFCS, val_y_PingBeam_refFCS); %les coordonn�es x et y
        % Triangulate 3D ("2.5D")
        figure;
        plot(p_FCS(:,1), p_FCS(:,2), '*r'); hold on;
        triplot(DT); grid on;
        
        DT_3D = triangulation(DT.ConnectivityList, val_x_PingBeam_refFCS, val_y_PingBeam_refFCS, val_z_PingBeam_refFCS);
        % Calculate vertex normals
        NormalLocal_refFCS = DT_3D.vertexNormal();
        NormalLocal_refFCS = NormalLocal_refFCS';
        nbPoints = size(NormalLocal_refFCS, 2);
        
        %{
        % Test autre m�thode
        [NormalVx, NormalVy, NormalVz, PosVx, PosVy, PosVz] = computeNormalVectorTriangulation(...
            [val_x_PingBeam_refFCS, val_y_PingBeam_refFCS, val_z_PingBeam_refFCS], DT, 'center-cells');
        NormalLocal_refFCS = [NormalVx'; NormalVy'; NormalVz'];
        nbPoints = size(NormalLocal_refFCS, 2);
        
        figure;
        quiver3(PosVx, PosVy, PosVz, NormalVx, NormalVy, NormalVz), axis equal
        hold on; trimesh(DT, PosVx, PosVy, PosVz)

        
        figure;
        scatter3(p_FCS(:,1), p_FCS(:,2), p_FCS(:,3), [], p_FCS(:,3), '.'); colormap jet
        subp = 1:500:nbPoints;
        hold on;
        quiver3(PosVx(subp), PosVy(subp), PosVz(subp),...
            NormalLocal_refFCS(1, subp)', NormalLocal_refFCS(2, subp)', NormalLocal_refFCS(3,subp)', 2, '.k'); grid on; axis equal;
        title(nomAlgo, 'Interpreter', 'none')
        %}
end

%{
figure(7878765);
% subplot(1,2,1);
% h1 = plot(D(1,:), '.-'); grid on;
% subp = NeiborIndex(1,:);
% subplot(1,2,2);
% h2 = plot3(p_FCS(:,1), p_FCS(:,2), p_FCS(:,3), '.k'); grid on; axis equal;
h2 = scatter3(p_FCS(:,1), p_FCS(:,2), p_FCS(:,3), ...
    [], p_FCS(:,3), '.'); colormap jet

TCC = readtable('X:\traitement_Campagnes\DataPosdocImen\Pour_Imen\PourImen_xyz - Cloud_N_Plane_k10_R2,2.csv');
subp = 1:1000:nbPoints;
hold on;
h3 = quiver3(p_FCS(subp,1), p_FCS(subp,2), p_FCS(subp,3), TCC.Nx(subp), TCC.Ny(subp), TCC.Nz(subp), '.k'); grid on; axis equal;
%}
%{
for k=2:100000:nbPoints
    subp = NeiborIndex(1,:);
    set(h1, 'YData', D(k,:))
    set(h2, 'XData', p_FCS(subp,1), 'YData', p_FCS(subp,2), 'ZData', p_FCS(subp,3))
    drawnow
end
%}

switch TypeAlgoNormales
    case 1 % knnsearch
        NormalLocal_refFCS = NaN(3,nbPoints, 'single');
        % hw = create_waitbar('Processing', 'N', nbPoints);
        [hw, DQ] = create_parforWaitbar(nbPoints, 'rangesearch');
        parfor k2=1:nbPoints
%         for k2=1:nbPoints
            %     my_waitbar(k2, nbPoints, hw);
            send(DQ, k2);
            % {
            n3 = 2;
            Index = [];
            for k3=-n3:n3
                kYPV = kY(k2) - k3;
                ind1 = ((kYPV-1) * nx) + kX(k2);
                if (ind1 >= 1) && (ind1 <= nbPoints)
                    Index = [Index, NeiborIndex(ind1,:)]; %#ok<PFBNS>
                end
            end
            Index = unique(Index);
            if length(Index) < 3
                continue
            end
            voisins_xyz = p_FCS(Index,:);
            coeff = pca(voisins_xyz);
            if size(coeff,2) < 3
                continue
            end
            PolynomePlan = coeff(:,3);
            %     Signe = sign(dot(PolynomePlan(:), direction(:))); %#ok<PFBNS>
            %     PolynomePlan = PolynomePlan * Signe;
            NormalLocal_refFCS(:,k2) = PolynomePlan;
            % }
            %{
            voisins_xyz = p_FCS(NeiborIndex(k2,:),:);
            
            kYP1 = kY(k2) - 1; % Ping pr�c�dent (-1), m�me faisceau
            ind1 = ((kYP1-1) * nx) + kX(k2);
            if (ind1 >= 1) && (ind1 <= nbPoints)
                voisins_xyz_PingP1 = p_FCS(NeiborIndex(ind1,:),:);
            else
                voisins_xyz_PingP1 = [];
            end
            kYS1 = kY(k2) + 1; % Ping suivant (+1), m�me faisceau
            ind1 = ((kYS1-1) * nx) + kX(k2);
            if (ind1 >= 1) && (ind1 <= nbPoints)
                voisins_xyz_PingS1 = p_FCS(NeiborIndex(ind1,:),:);
            else
                voisins_xyz_PingS1 = [];
            end
            
            kYP2 = kY(k2) - 2; % Ping pr�c�dent (-2) m�me faisceau
            ind1 = ((kYP2-1) * nx) + kX(k2);
            if (ind1 >= 1) && (ind1 <= nbPoints)
                voisins_xyz_PingP2 = p_FCS(NeiborIndex(ind1,:),:);
            else
                voisins_xyz_PingP2 = [];
            end
            kYS2 = kY(k2) + 2; % Ping suivant (+2), m�me faisceau
            ind1 = ((kYS2-1) * nx) + kX(k2);
            if (ind1 >= 1) && (ind1 <= nbPoints)
                voisins_xyz_PingS2 = p_FCS(NeiborIndex(ind1,:),:);
            else
                voisins_xyz_PingS2 = [];
            end
            
            coeff = pca([voisins_xyz; voisins_xyz_PingP1; voisins_xyz_PingP2; voisins_xyz_PingS1; voisins_xyz_PingS2]);
            PolynomePlan = coeff(:,3);
            %     Signe = sign(dot(PolynomePlan(:), direction(:))); %#ok<PFBNS>
            %     PolynomePlan = PolynomePlan * Signe;
            NormalLocal_refFCS(:,k2) = PolynomePlan;
            %}
        end
        my_close(hw);
        
    case 2 % KDTreeSearcher
        rayon = 2;
        NormalLocal_refFCS = NaN(3,nbPoints, 'single');
        % hw = create_waitbar('Processing', 'N', nbPoints);
        [hw, DQ] = create_parforWaitbar(nbPoints, 'rangesearch');
        parfor k2=1:nbPoints
            % for k2=1:nbPoints
            %     my_waitbar(k2, nbPoints, hw);
            send(DQ, k2);
            Idx = rangesearch(Mdl,  p_FCS(k2,:), rayon);
            Idx = Idx{1};
            voisins_xyz = p_FCS(Idx,:);
            coeff = pca(voisins_xyz);
            if size(coeff,2) >= 3 % C'est arriv� pour k k2=113591 pour fichier
                PolynomePlan = coeff(:,3);
                NormalLocal_refFCS(:,k2) = PolynomePlan;
            end
        end
        my_close(hw);
        
    case 3 % delaunayTriangulation
        % C'est d�j� fait
end

%{
figure;
plot3(val_x_PingBeam_refFCS, val_y_PingBeam_refFCS, val_z_PingBeam_refFCS, '.b');
axis equal; xlabel('x'), ylabel('y');
hold on;

subDisplay = 1:100:length(val_y_PingBeam_refFCS);
hold on;
quiver3(val_x_PingBeam_refFCS(subDisplay), val_y_PingBeam_refFCS(subDisplay), val_z_PingBeam_refFCS(subDisplay), ...
    NormalLocal_refFCS(1,subDisplay)', NormalLocal_refFCS(2,subDisplay)', NormalLocal_refFCS(3,subDisplay)', 'ShowArrowHead', 'Off');
%}

UOndeAcoustique_refFCS    = NaN(3, nbPoints, 'single');
UOndeAcous_refFCS_FromXYZ = NaN(3, nbPoints, 'single');
XGeoSoner_refFCS          = NaN(1, nbPoints);
YGeoSoner_refFCS          = NaN(1, nbPoints);
ZGeoSoner_refFCS          = NaN(1, nbPoints);

figure(Fig);
hqN = [];
hp  = [];
iDeb = 0;
if flagWaitbar
    hw = create_waitbar(Emission.Name, 'Entete', 'IFREMER - SonarScope : Processing incidence angle', 'N', nbRows);
else
    hw = [];
end
for k=1:nbRows
    my_waitbar(k, nbRows, hw);
    
    kRange = subyRange(k);
    
    switch SonarName % TODO : Th�oriquement plus utile
        case {'EM2040'; 'EM2040S'; 'EM2040D'}
            SonarMode_1 = Range.Sonar.NbSwaths(kRange);
            %             SonarMode_1 = get(Range, 'Mode1');
            %             SonarMode_2 = get(Range, 'Mode2');
            SonarMode_2 = get_mode2(Range);
            %             SonarMode_3 = get(Range, 'Mode3');
            SonarMode_3 = get_mode3(Range);
            %             SonarMode_1 = SonarMode_1(kRange);
            SonarMode_2 = SonarMode_2(kRange);
            SonarMode_3 = SonarMode_3(kRange);
            Range = update_SonarDescription_PingEM2040(Range,  k == 1, SonarMode_1,  SonarMode_2, SonarMode_3);
        otherwise
            Range = update_SonarDescription_Ping(Range, kRange);
    end
    
    %% get TxBeamWidthDeg
    
    % TODO : on pourrait appeler getValueMatrix une seule fois (pour tous
    % les suby) mais l'op�ration ne semble pas prendre plus de temps que
    % �a. A voir quand il y aura un moment de r�pis
    
    % TODO : TxAlongBeamWidth = NaN pour EM1002 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if isempty(ValTxBeamWidth_deg)
        BeamWidthAlongDeg = get(this.Sonar.Desciption, 'LongBeamWth');
    else
        BeamWidthAlongDeg = ValTxBeamWidth_deg(suby(k));
    end
    BeamWidthAlongRd = BeamWidthAlongDeg  * (pi/180);
    
    %% get RxAcrossBeamWidth
    
    if isempty(ValRxBeamWidth_deg)
        BeamWidthAcrossDeg = get(this.Sonar.Desciption, 'BeamForm.Rx.TransWth');
    else
        BeamWidthAcrossDeg = ValRxBeamWidth_deg(suby(k));
    end
    
    %% Elargissement de l'angle d'ouverture des faisceaux en fonction de la longueur apparente
    
    RollTx = Emission.Sonar.Ship.Arrays.Receive.Roll;
    
%     RollTx(:) = 0; % TODO : V�rifier si il faut le faire
    
    BeamPointingAngleDeg = double(EmissionImage(suby(k),subx));
    RxBeamWidthDegDepointe = getRxBeamWidthDepointe(BeamWidthAcrossDeg, BeamPointingAngleDeg, RollTx, subBab, subTri);
    BeamWidthAcross_rd = RxBeamWidthDegDepointe * (pi/180);
    
    %% Algo
    
    C = SoundSpeed(kRange,1); % TODO : pas top !
    
    if length(RollTx) == 1
        RollTxBeams = zeros(1,length(BeamPointingAngleDeg)) + RollTx;
    else
        RollTxBeams = NaN(1,length(BeamPointingAngleDeg));
        RollTxBeams(subBab) = RollTx(1);
        RollTxBeams(subTri) = RollTx(2);
    end
%     RollTxBeams(:) = 0; % TODO : V�rifier si il faut le faire
    TetaBeamAngleEarth_rd = (BeamPointingAngleDeg - RollTxBeams) * piS180; % ajout Ridha: d�compensation du Roll avant Euler
    %     subNonNaNBeams = find(~isnan(TetaBeamAngleEarth_rd));
    subNonNaNBeams = find(~isnan(Bathymetry_PingBeam.Image(suby(k),subx)));
    if isempty(subNonNaNBeams)
        continue
    end
    TetaBeamAngleEarth_rd  = TetaBeamAngleEarth_rd(subNonNaNBeams);
    RxBeamWidthDegDepointe = RxBeamWidthDegDepointe(subNonNaNBeams);
    
    TetaHeading = double(Emission.Sonar.Heading(suby(k))) * piS180;
    TetaRoll = double(Emission.Sonar.Roll(suby(k))) * piS180;
    TetaPitch = double(Emission.Sonar.Pitch(suby(k))) * piS180;
    
    TetaSlopeNorth = double(SlopeN.Image(subyP(k),subxP(subNonNaNBeams))) * piS180;
    if all(isnan(TetaSlopeNorth))
        continue
    end
    %     TetaSlopeWest = double(SlopeW.Image(subyP(k),subxP(subNonNaNBeams))) * piS180;
    %     figure; plot(TetaSlopeWest / piS180, 'b'); grid on; hold on; plot(TetaSlopeNorth / piS180, 'r'); legend({'TetaSlopeWest'; 'TetaSlopeNorth'})
    
    n = length(subNonNaNBeams);
    subPoints = iDeb+(1:n);
    iDeb = iDeb + n;
        
    TetaAlongBeamPoitingAngle = double(AlongBeamPoitingAngle.Image(subyAlongBeamPoitingAngle(k),subxAlongBeamPoitingAngle(subNonNaNBeams))) * piS180;
    
    PitchTx = get(AlongBeamPoitingAngle, 'PitchTx');
    PitchTx = PitchTx(k,1);
    TetaAlongBeamPoitingAngle = TetaAlongBeamPoitingAngle - (PitchTx * piS180);
    
    %% Veteur onde acoustique
    
    val_x_Ping_refFSC = x_PingBeam.Image(suby(k),subx(subNonNaNBeams));
    val_y_Ping_refFSC = y_PingBeam.Image(suby(k),subx(subNonNaNBeams));
    val_z_Ping_refFSC = Bathymetry_PingBeam.Image(suby(k),subx(subNonNaNBeams));
    
    
    dX = (val_x_Ping_refFSC - XGeo_refFCS(suby(k)));
    dY = (val_y_Ping_refFSC - YGeo_refFCS(suby(k)));
    dZ = (val_z_Ping_refFSC - ZGeo_refFCS(suby(k)));
    UOndeAcous_refFCS_FromXYZ(1,subPoints) = dX;
    UOndeAcous_refFCS_FromXYZ(2,subPoints) = dY;
    UOndeAcous_refFCS_FromXYZ(3,subPoints) = dZ;
    
    

    
    
    UOndeAcous_refVCS = [cos(TetaBeamAngleEarth_rd) .* sin(TetaAlongBeamPoitingAngle)
        sin(TetaBeamAngleEarth_rd)
        cos(TetaBeamAngleEarth_rd) .* cos(TetaAlongBeamPoitingAngle)];
    %{
    my_figure;
    subplot(3,1,1); my_plot(UOndeAcous_refVCS(1,:), 'b'); grid on; title('UOndeAcous_refVCS(1,:)')
    subplot(3,1,2); my_plot(UOndeAcous_refVCS(2,:), 'b'); grid on; title('UOndeAcous_refVCS(2,:)')
    subplot(3,1,3); my_plot(UOndeAcous_refVCS(3,:), 'b'); grid on; title('UOndeAcous_refVCS(3,:)')
        %}
        
        Reuler =[cos(TetaPitch)*cos(TetaHeading) cos(TetaHeading)*sin(TetaRoll)*sin(TetaPitch) - cos(TetaRoll)*sin(TetaHeading) sin(TetaRoll)*sin(TetaHeading) + cos(TetaRoll)*cos(TetaHeading)*sin(TetaPitch);
            cos(TetaPitch)*sin(TetaHeading)      cos(TetaRoll)*cos(TetaHeading) + sin(TetaRoll)*sin(TetaPitch)*sin(TetaHeading) cos(TetaRoll)*sin(TetaPitch)*sin(TetaHeading) - cos(TetaHeading)*sin(TetaRoll);
            -sin(TetaPitch)                      cos(TetaPitch)*sin(TetaRoll)                                                   cos(TetaRoll)*cos(TetaPitch)];
        
        UOndeAcous_refFCS_FromAngles =  Reuler * UOndeAcous_refVCS;
        %{
    my_figure;
    subplot(3,1,1); my_plot(UOndeAcous_refFCS_FromAngles(1,:), 'b'); grid on; title('UOndeAcous_refFCS_FromAngles(1,:)')
    subplot(3,1,2); my_plot(UOndeAcous_refFCS_FromAngles(2,:), 'b'); grid on; title('UOndeAcous_refFCS_FromAngles(2,:)')
    subplot(3,1,3); my_plot(UOndeAcous_refFCS_FromAngles(3,:), 'b'); grid on; title('UOndeAcous_refFCS_FromAngles(3,:)')
        %}
        
        % nbBeams = size(UOndeAcous_refFCS_FromAngles,2);
        % VecteurVertical = [zeros(1,nbBeams); zeros(1,nbBeams); ones(1,nbBeams)];
        % Normal = cross(UOndeAcous_refFCS_FromAngles, VecteurVertical);
        %{
    Ux = Normal(1,:);
    Uy = Normal(2,:);
    Uz = Normal(3,:);
    my_figure;
    subplot(3,1,1); my_plot(Ux, 'b'); grid on; title('Ux')
    subplot(3,1,2); my_plot(Uy, 'b'); grid on; title('Uy')
    subplot(3,1,3); my_plot(Uz, 'b'); grid on; title('Uz')
        %}
        
        %{
%     TetaSD = 0; %2;
    TetaSD = double(IBA.Image(suby(k),subx(subNonNaNBeams)));
%     TetaSD(:) = 0;


    n = length(TetaSD);
    UOndeAcous_refFCS_SnellDescartesCorrige = NaN(3,n);
    for k2=1:n
        c = cosd(TetaSD(k2));
        s = sind(TetaSD(k2));
        c1 = 1-c;
        ux = Ux(k2);
        uy = Uy(k2);
        uz = Uz(k2);
        R = [ux.^2.*c1+c       ux.*uy.*c1-uz.*s  ux.*uz.*c1+uy.*s
            ux.*uy.*c1+uz.*s   uy.^2.*c1+c       uy.*uz.*c1-ux.*s
            ux.*uz.*c1-uy.*s   uy.*uz.*c1+ux.*s  uz.^2.*c1+c];
        UOndeAcous_refFCS_SnellDescartesCorrige(:,k2) = R * UOndeAcous_refFCS_FromAngles(:,k2);
    end
        %}
        % On ne tient pas compte de Snell-Descartes (pour l'instant)
        UOndeAcous_refFCS_SnellDescartesCorrige = UOndeAcous_refFCS_FromAngles; % !!!!!!!!!!!!!!!!!!!!!!!!!!!! UOndeAcous_refFCS_FromXYZ
%         UOndeAcous_refFCS_FromXYZ(:,subPoints)  = UOndeAcous_refFCS_FromAngles; % !!!!!!!!!!!!!!!!!!!!!!!!!!!! UOndeAcous_refFCS_FromXYZ
        
         %{
    nq = size(UOndeAcous_refFCS_SnellDescartesCorrige,2);
    hold on; quiver3(repmat(minX, 1,nq), repmat(minY, 1,nq), repmat(minZ, 1, nq), 100*UOndeAcous_refFCS_SnellDescartesCorrige(1,:), 100*UOndeAcous_refFCS_SnellDescartesCorrige(2,:), 100*UOndeAcous_refFCS_SnellDescartesCorrige(3,:), 'm')
        %}
        
        %{
    my_figure(;
    subplot(3,1,1); my_plot(UOndeAcous_refFCS_SnellDescartesCorrige(1,:), 'b'); grid on; title('UOndeAcous_refFCS_SnellDescartesCorrige(1,:)')
    subplot(3,1,2); my_plot(UOndeAcous_refFCS_SnellDescartesCorrige(2,:), 'b'); grid on; title('UOndeAcous_refFCS_SnellDescartesCorrige(2,:)')
    subplot(3,1,3); my_plot(UOndeAcous_refFCS_SnellDescartesCorrige(3,:), 'b'); grid on; title('UOndeAcous_refFCS_SnellDescartesCorrige(3,:)')
        %}
        
%         val_x_Ping_refFSC = x_PingBeam.Image(suby(k),subx(subNonNaNBeams));
%         val_y_Ping_refFSC = y_PingBeam.Image(suby(k),subx(subNonNaNBeams));
%         val_z_Ping_refFSC = Bathymetry_PingBeam.Image(suby(k),subx(subNonNaNBeams));
        

        % TODO : afficher le nuage de point et la normale du ping avec quiver
        
        %{
    my_figure;
    subplot(3,1,1); my_plot(NrefEarth(1,:), 'b'); grid on; hold on; my_plot(NrefEarthTree(1,:), 'r'); my_plot(NormalLocal_refFCS(1,subPoints), 'g');
    subplot(3,1,2); my_plot(NrefEarth(2,:), 'b'); grid on; hold on; my_plot(NrefEarthTree(2,:), 'r'); my_plot(NormalLocal_refFCS(2,subPoints), 'g');
    subplot(3,1,3); my_plot(NrefEarth(3,:), 'b'); grid on; hold on; my_plot(NrefEarthTree(3,:), 'r'); my_plot(NormalLocal_refFCS(3,subPoints), 'g');
        %}
        NormalLocal_Ping_refFCS = NormalLocal_refFCS(:,subPoints);
        UOndeAcoustique_refFCS(:,subPoints) = UOndeAcous_refFCS_FromAngles;
        XGeoSoner_refFCS(subPoints) = XGeo_refFCS(suby(k));
        YGeoSoner_refFCS(subPoints) = YGeo_refFCS(suby(k));
        ZGeoSoner_refFCS(subPoints) = ZGeo_refFCS(suby(k));
        
        % TODO / Sans doute obligatoire
        %     NormalLocal_Ping_refFCS = [NrefEarth(2,:); NrefEarth(1,:); -NrefEarth(3,:)];
        %{
    my_figure;
    subplot(3,1,1); my_plot(NormalLocal_Ping_refFCS(1,:), 'b'); grid on; hold on; my_plot(NormalLocal_Ping_refFCS(1,:), 'r');
    subplot(3,1,2); my_plot(NormalLocal_Ping_refFCS(2,:), 'b'); grid on; hold on; my_plot(NormalLocal_Ping_refFCS(2,:), 'r');
    subplot(3,1,3); my_plot(NormalLocal_Ping_refFCS(3,:), 'b'); grid on; hold on; my_plot(NormalLocal_Ping_refFCS(3,:), 'r');
        %}
        
        %     NormalLocal_Ping_refFCS = [cos(TetaSlopeWest).*sin(TetaHeading).*sin(TetaSlopeNorth) - cos(TetaHeading).*cos(TetaSlopeNorth).*sin(TetaSlopeWest)
        %         - cos(TetaHeading)*cos(TetaSlopeWest).*sin(TetaSlopeNorth) - cos(TetaSlopeNorth).*sin(TetaHeading).*sin(TetaSlopeWest)
        %         cos(TetaSlopeWest).*cos(TetaSlopeNorth)];
        
        normeN   = sqrt(sum(NormalLocal_Ping_refFCS .^ 2));
        NormalLocal_Ping_refFCS = bsxfun(@rdivide, NormalLocal_Ping_refFCS, normeN);
        
        %% Pr�paration export des donn�es pour Imen
        
        %     Pings(k).kPing                   = kRange;          %#ok<AGROW>
        %     Pings(k).kBeams                  = subx(subNonNaNBeams); %#ok<AGROW>
        %     Pings(k).val_x_Ping_refFSC       = val_x_Ping_refFSC;             %#ok<AGROW>
        %     Pings(k).val_y_Ping_refFSC       = val_y_Ping_refFSC;             %#ok<AGROW>
        %     Pings(k).val_z_Ping_refFSC       = val_z_Ping_refFSC;             %#ok<AGROW>
        %     Pings(k).NormalLocal_Ping_refFCS = NormalLocal_Ping_refFCS;       %#ok<AGROW>
        %     Pings(k).NrefEarth               = NormalLocal_Ping_refFCS;       %#ok<AGROW>
        %     Pings(k).UOndeAcous_refFCS_SnellDescartesCorrige = UOndeAcous_refFCS_SnellDescartesCorrige;       %#ok<AGROW>
        
        %% Compute SlopeAcross and SlopeAlong
        
        %% Projection sur le plan
        
        % {
    x = UOndeAcous_refFCS_FromXYZ(1,subPoints);
    y = UOndeAcous_refFCS_FromXYZ(2,subPoints);
    z = UOndeAcous_refFCS_FromXYZ(3,subPoints);
%     a = NormalLocal_Ping_refFCS(1,:);
%     b = NormalLocal_Ping_refFCS(2,:);
%     c = NormalLocal_Ping_refFCS(3,:);
%     d = 0;
%     Uxp = ((b.^2+c.^2).*x - a.*b.*y       - a.*c.*z       - d.*a);
%     Uyp = (-a.*b.*x      + (a.^2+c.^2).*y - b.*c.*z       - d.*b);
%     Uzp = (-a.*c.*x      - b.*c.*y       + (a.^2+b.^2).*z - d.*c);
%     VAlong  = [Uxp; Uyp; Uzp]; % Projection orthogonale de l'onde acoustique sur le plan tangent local d�fini par NormalLocal_Ping_refFCS
%     VAlong  = VAlong  ./ vecnorm(VAlong, 2, 1);
%     VAcross = cross(VAlong, NormalLocal_Ping_refFCS); % Vecteur perpendiculaire � VAlong et � NormalLocal_Ping_refFCSe 
% 
%     SlopeAlong_deg  = acosd(dot(VAlong,  UOndeAcous_refFCS_FromXYZ(:,subPoints)));
%     SlopeAcross_deg = acosd(dot(VAcross, UOndeAcous_refFCS_FromXYZ(:,subPoints)));

    % Calcul de la normale du plan acoustique � partir des points des
    % diff�rents faisceaux
    
    planAcousticCloud = [x' y' z'];
    planAcousticCloud(end+1,:) = 0;
    coeff = pca(planAcousticCloud);
    if size(coeff,2) >= 3 % C'est arriv� pour k k2=113591 pour fichier
        PolynomePlan = coeff(:,3);
        NPlanAcoustique = PolynomePlan;
    end
    [~, inAngle0] = sort(abs(BeamPointingAngleDeg));
    inAngle0 = inAngle0(1);
    VecteurFaisceauCentral = [x(inAngle0); y(inAngle0); z(inAngle0)];
    
    VAcross = cross(NPlanAcoustique, VecteurFaisceauCentral);
    VAcross = VAcross / norm(VAcross);
    VAlong  = cross(VAcross, VecteurFaisceauCentral);
    VAlong = VAlong / norm(VAlong);
    n = size(NormalLocal_Ping_refFCS,2);
    SlopeAlong_deg  = acosd(dot(repmat(VAlong,1,n),  NormalLocal_Ping_refFCS));
    SlopeAcross_deg = acosd(dot(repmat(VAcross,1,n), NormalLocal_Ping_refFCS));
    figure; plot(SlopeAlong_deg);  grid on; title('SlopeAlong_deg', 'interpreter', 'none')
    figure; plot(SlopeAcross_deg); grid on; title('SlopeAcross_deg', 'interpreter', 'none')
    
            
    %{
    my_figure;
    subplot(3,1,1); my_plot(VAcross(1,:), 'b'); grid on; hold on; plot(NormalLocal_Ping_refFCS(1,:), 'r')
    subplot(3,1,2); my_plot(VAcross(2,:), 'b'); grid on; hold on; plot(NormalLocal_Ping_refFCS(2,:), 'r')
    subplot(3,1,3); my_plot(VAcross(3,:), 'b'); grid on; hold on; plot(NormalLocal_Ping_refFCS(3,:), 'r')

    my_figure;
    subplot(3,1,1); my_plot(VAlong(1,:), 'b'); grid on; hold on; plot(NormalLocal_Ping_refFCS(1,:), 'r')
    subplot(3,1,2); my_plot(VAlong(2,:), 'b'); grid on; hold on; plot(NormalLocal_Ping_refFCS(2,:), 'r')
    subplot(3,1,3); my_plot(VAlong(3,:), 'b'); grid on; hold on; plot(NormalLocal_Ping_refFCS(3,:), 'r')
     
    my_figure;
    subplot(2,1,1); my_plot(dot(VAlong,  NormalLocal_Ping_refFCS), 'b'); grid on;
    subplot(2,1,2); my_plot(dot(VAcross, NormalLocal_Ping_refFCS), 'b'); grid on;
   
    figure;
    subplot(2,1,1); plot(SlopeAcross_deg); grid on; title('SlopeAcross tourn�');
    subplot(2,1,2); plot(SlopeAlong_deg);  grid on; title('SlopeAlong tourn�');
    %}
        
    % Ancienne formule
%     SlopeAcross_deg = atan2d(NormalLocal_Ping_refFCS(3,:), NormalLocal_Ping_refFCS(1,:)) - 90;
%     SlopeAlong_deg  = atan2d(NormalLocal_Ping_refFCS(3,:), NormalLocal_Ping_refFCS(2,:)) - 90;
        
    %{
    figure(45554);
    subplot(2,2,1); plot(TetaSlopeWest  * 180/pi); grid on; title('TetaSlopeWest');
    subplot(2,2,2); plot(TetaSlopeNorth * 180/pi); grid on; title('TetaSlopeNorth');
    subplot(2,2,3); plot(SlopeAcross_deg); grid on; title('SlopeAcross tourn�');
    subplot(2,2,4); plot(SlopeAlong_deg);  grid on; title('SlopeAlong tourn�');
        %}
        
        PAcross_deg(k,subNonNaNBeams) = SlopeAcross_deg;
        PAlong_deg(k,subNonNaNBeams)  = SlopeAlong_deg;
        
        %% Compute Incidence Angle
        
        %X2 = dot(UOndeAcous_refVCS, NormalLocal_Ping_refFCS);
        %     X2=dot(UOndeAcous_refFCS_FromAngles, NormalLocal_Ping_refFCS);
        
        %{
    my_figure;
    subplot(3,1,1); my_plot(UOndeAcous_refFCS_SnellDescartesCorrige(1,:), 'b'); grid on;
    subplot(3,1,2); my_plot(UOndeAcous_refFCS_SnellDescartesCorrige(2,:), 'b'); grid on;
    subplot(3,1,3); my_plot(UOndeAcous_refFCS_SnellDescartesCorrige(3,:), 'b'); grid on;
        %}
        
        %     sensorCenter = [mean(val_y_Ping_refFSC), mean(val_x_Ping_refFSC), 0]; % POSITION DU SONAR SVP !!!!!!
        sensorCenter = [XGeo_refFCS(suby(k)), YGeo_refFCS(suby(k)), ZGeo_refFCS(suby(k))];
        for kBeam=1:size(NormalLocal_Ping_refFCS,2)
            p1 = sensorCenter - [val_x_Ping_refFSC(kBeam), val_y_Ping_refFSC(kBeam), val_z_Ping_refFSC(kBeam)];
            p2 = NormalLocal_Ping_refFCS(:,kBeam);
            % Flip the normal vector if it is not pointing towards the sensor.
            angle = atan2(norm(cross(p1,p2')), p1*p2);
            if abs(angle) > (pi/2)
                NormalLocal_Ping_refFCS(:,kBeam) = -NormalLocal_Ping_refFCS(:,kBeam);
            end
        end
        % {
        if isempty(hqN) || (mod(k,400) == 1)
            hold on
            hqN = quiver3(val_x_Ping_refFSC, val_y_Ping_refFSC, val_z_Ping_refFSC, ...
                NormalLocal_Ping_refFCS(1,:), NormalLocal_Ping_refFCS(2,:), NormalLocal_Ping_refFCS(3,:), 2, 'ShowArrowHead', 'Off');
            set(hqN, 'Color', 'k')
        else
            set(hqN, 'XData', val_x_Ping_refFSC, 'YData', val_y_Ping_refFSC, 'ZData', val_z_Ping_refFSC, ...
                'UData', NormalLocal_Ping_refFCS(1,:), 'VData', NormalLocal_Ping_refFCS(2,:), 'WData', NormalLocal_Ping_refFCS(3,:));
        end
        % }
        
        XP_refFCS = [XGeo_refFCS(suby(k)) val_x_Ping_refFSC XGeo_refFCS(suby(k))];
        YP_refFCS = [YGeo_refFCS(suby(k)) val_y_Ping_refFSC YGeo_refFCS(suby(k))];
        ZP_refFCS = [ZGeo_refFCS(suby(k)) val_z_Ping_refFSC ZGeo_refFCS(suby(k))];
        tabAngles = -60:15:60;
        nTabAngles = length(tabAngles);
        Angles = BeamPointingAngleDeg(subNonNaNBeams);
        idAngles  = NaN(1,nTabAngles);
        valAngles = NaN(1,nTabAngles);
        for kAngle=1:nTabAngles
            [~, ida] = sort(abs(Angles - tabAngles(kAngle)));
            idAngles(kAngle)  = ida(1);
            valAngles(kAngle) = Angles(idAngles(kAngle));
        end
        
        %     idAngles = 1:length(subNonNaNBeams);
        
        nBeams = length(idAngles);
        XGeoJMA_refFCS = repmat(XGeo_refFCS(suby(k)), 1, nBeams);
        YGeoJMA_refFCS = repmat(YGeo_refFCS(suby(k)), 1, nBeams);
        ZGeoJMA_refFCS = repmat(ZGeo_refFCS(suby(k)), 1, nBeams);
        
%         dX = (val_x_Ping_refFSC - XGeo_refFCS(suby(k)));
%         dY = (val_y_Ping_refFSC - YGeo_refFCS(suby(k)));
%         dZ = (val_z_Ping_refFSC - ZGeo_refFCS(suby(k)));
%         UOndeAcous_refFCS_FromXYZ(1,subPoints) = dX;
%         UOndeAcous_refFCS_FromXYZ(2,subPoints) = dY;
%         UOndeAcous_refFCS_FromXYZ(3,subPoints) = dZ;
        
        if isempty(hp) || (k == 1)
            hp = patch(XP_refFCS, YP_refFCS, ZP_refFCS, [0.8 0.8 0.8]);
            
            %         hq = quiver3(XGeoJMA_refFCS, YGeoJMA_refFCS, ZGeoJMA_refFCS, ...
            %             UOndeAcous_refFCS_FromAngles(2,idAngles), UOndeAcous_refFCS_FromAngles(1,idAngles), -UOndeAcous_refFCS_FromAngles(3,idAngles), 10, 'y', 'ShowArrowHead', 'off'); grid on;
            
            hq2 = quiver3(XGeoJMA_refFCS, YGeoJMA_refFCS, ZGeoJMA_refFCS, ...
                UOndeAcous_refFCS_FromXYZ(1,subPoints(idAngles)), ...
                UOndeAcous_refFCS_FromXYZ(2,subPoints(idAngles)), ...
                UOndeAcous_refFCS_FromXYZ(3,subPoints(idAngles)), ...
                1, 'g', 'ShowArrowHead', 'off'); grid on;
        else
            set(hp, 'XData', XP_refFCS, 'YData', YP_refFCS, 'ZData', ZP_refFCS);
            
            %         set(hq, 'XData', XGeoJMA_refFCS, 'YData', YGeoJMA_refFCS, 'ZData', ZGeoJMA_refFCS, ...
            %             'UData', UOndeAcous_refFCS_FromAngles(2,idAngles), 'VData', UOndeAcous_refFCS_FromAngles(1,idAngles), 'WData', -UOndeAcous_refFCS_FromAngles(3,idAngles));
            
            set(hq2, 'XData', XGeoJMA_refFCS, 'YData', YGeoJMA_refFCS, 'ZData', ZGeoJMA_refFCS, ...
                'UData',  UOndeAcous_refFCS_FromXYZ(1,subPoints(idAngles)), ...
                'VData',  UOndeAcous_refFCS_FromXYZ(2,subPoints(idAngles)), ...
                'WData',  UOndeAcous_refFCS_FromXYZ(3,subPoints(idAngles)));
        end
        drawnow
        
        X2 = dot(UOndeAcous_refFCS_SnellDescartesCorrige, NormalLocal_Ping_refFCS);
        %{
    x = acrossDistRefTxAntenna.Image(k,subxP(subNonNaNBeams));
    y = alongDistRefTxAntenna.Image(k,subxP(subNonNaNBeams));
    z = bathy.Image(k,subxP(subNonNaNBeams)); % zeros(size(x));
    figure(545122);
    subplot(4,1,1); plot(x, y, '.-'); grid on; xlabel('Across dist (m)'); ylabel('Along dist (m)');
    subplot(4,1,2); plot(x, TetaAlongBeamPoitingAngle * 180/pi, '.-'); grid on; xlabel('Across dist (m)'); ylabel('TetaAlongBeamPoitingAngle (deg)');
    subplot(4,1,3); plot(x, SlopeAcross_deg, '.-'); grid on; xlabel('Across dist (m)'); ylabel('SlopeAcross_deg');
    subplot(4,1,4); plot(x, SlopeAlong_deg, '.-'); grid on; xlabel('Across dist (m)'); ylabel('SlopeAlong_deg');
        %}
        
        %{
    x = acrossDistRefTxAntenna.Image(k,subxP(subNonNaNBeams));
    y = alongDistRefTxAntenna.Image(k,subxP(subNonNaNBeams));
    z = bathy.Image(k,subxP(subNonNaNBeams)); % zeros(size(x));
    z = bathy.Image(k,subxP(subNonNaNBeams)); % zeros(size(x));
    figure(545222);
    ze = zeros(size(x));
    quiver3(x, y, ze, ze, ze, z, 'b'); grid on
    xlabel('Across dist (m)'); ylabel('Along dist (m)'); zlabel('Depth');
        %}
        
        AC = acosd(X2);
        angleIncidence_deg = 180 - AC;
        angleIncidence_deg = min(angleIncidence_deg, AC);
        %     figure; plot(angleIncidence_deg); grid on
        
        angleIncidence_deg = angleIncidence_deg .* sign(TetaBeamAngleEarth_rd);
        
        J(k,subNonNaNBeams) = angleIncidence_deg;
        % figure; plot(I, 'b'); grid on; hold on; plot(angleIncidence_deg, 'r*')
        
        %% Compute InsonifiedArea
        
        T_s = PLImage.Image(suby(k),:) * 1e-3;
                
        tiltAngle_deg = atan2d(UOndeAcous_refVCS(3,:), UOndeAcous_refVCS(2,:)) - 90;
        
        angleIncidenceAcrossSwath_deg = TetaBeamAngleEarth_rd *(180/pi) - SlopeAcross_deg;
        
        angleIncidenceAlongSwath_deg  = SlopeAlong_deg - tiltAngle_deg; % - 180
        
        PingRangeInMeters = ImageRangeInMeters(kRange,subxRange(subNonNaNBeams));
        
        %% Extension footprint along valable quelque soit le r�gime
        
        AlongBeamFootPrintInMeters = PingRangeInMeters .* BeamWidthAlongRd ./ cosd(angleIncidenceAlongSwath_deg);
        
        %% Extension footprint across
        
        % R�gime impulsion courte, incidence rasante
        
        %    % Ancienne formule de A1
        %     AcrossSignalFootPrint = (C .* T_s(subNonNaNBeams) / 2) ./ sind(abs(angleIncidenceAcrossSwath_deg));
        %     A1 = (BeamWidthAlongRd * PingRangeInMeters) .* (AcrossSignalFootPrint ./ cosd(angleIncidenceAlongSwath_deg));
        %     FigUtils.createSScFigure; PlotUtils.createSScPlot(A1); grid; title('A1 Old')
        
        %   % Nouvelle formule de A1
        X = abs(PingRangeInMeters .* sind(angleIncidenceAcrossSwath_deg) .* (sqrt(1 + (C .* T_s(subNonNaNBeams) ./ (PingRangeInMeters .* (sind(angleIncidenceAcrossSwath_deg)) .^ 2))) - 1));
        AcrossSignalFootPrint = X;
        
        A1 = AlongBeamFootPrintInMeters .* AcrossSignalFootPrint;
        
        % R�gime impulsion longue proche du sp�culaire
        
        % A2 = (PingRangeInMeters .^2) .* (BeamWidthAlongRd * BeamWidthAcross_rd(subNonNaNBeams)) ./ X2;
        %     FigUtils.createSScFigure; PlotUtils.createSScPlot(A2); grid; title('A2 Old')
        
        AcrossBeamFootPrintInMeters = PingRangeInMeters .* BeamWidthAcross_rd(subNonNaNBeams) ./ cosd(angleIncidenceAcrossSwath_deg);
        A2 = AlongBeamFootPrintInMeters .* AcrossBeamFootPrintInMeters;
        % FigUtils.createSScFigure; PlotUtils.createSScPlot(A2, 'r'); grid on; hold on; PlotUtils.createSScPlot(A1, 'b')
        
        mA1A2 = min(A1,A2);
        %{
        FigUtils.createSScFigure(88877);
        PlotUtils.createSScPlot(reflec_Enr2dB(A1), '-*b'); grid on; hold on; PlotUtils.createSScPlot(reflec_Enr2dB(A2), '-*k'); PlotUtils.createSScPlot(reflec_Enr2dB(mA1A2), 'or'); hold off; legend({'A1'; 'A2'; 'min'}); drawnow
        %}
        InsonifiedArea(k,subNonNaNBeams) = mA1A2;
        
        %% Tentative de pr�diction de l'�paisseur du signal de la colonne d'eau / TODO : voir avec Ridha et Na�g pour obtenit les �quations parfaites
        
        %{
    % Equations JMA
    
%     SignalWidth = T_s(subNonNaNBeams) * SampleFrequency(kRange) ./ cosd(angleIncidenceAcrossSwath_deg);
    SignalWidth1InSeconds = min(T_s) ./ cosd(angleIncidenceAcrossSwath_deg);
    SignalWidth2InSeconds = AcrossBeamFootPrintInMeters  / C;
    
    %     figure; plot(SignalWidth1InSeconds, 'b'); grid on; hold on; plot(SignalWidth2InSeconds, 'r')
    wheight = sind(abs(angleIncidenceAcrossSwath_deg));
%     figure; plot(wheight); grid
    SignalWidthInSeconds = wheight .* SignalWidth2InSeconds + (1-wheight) .* SignalWidth1InSeconds;
    SignalWidthInSeconds = SignalWidthInSeconds * 5;
        %{
    figure(56584);
    plot(SignalWidth1InSeconds, 'b'); grid on;
    hold on;
    plot(SignalWidth2InSeconds, 'r'); plot(SignalWidthInSeconds, 'k');
    hold off;
        %}
    WCSignalWidthInSeconds(k,subNonNaNBeams) = SignalWidthInSeconds;
        %}
        
        % {
        % Equation Na�g
        
        %     SignalWidth1InSeconds = min(T_s) ./ cosd(angleIncidenceAcrossSwath_deg);
        %     SignalWidth2InSeconds = AcrossBeamFootPrintInMeters  / C;
        
        PingRangeInSeconds = PingRangeInMeters / (SoundSpeed(kRange)/2);
        absAlpha = 90 - abs(angleIncidenceAcrossSwath_deg);
        SignalWidthInSecondsMin = PingRangeInSeconds .* (1 - sind(absAlpha) ./ sind(absAlpha + RxBeamWidthDegDepointe/2));
        SignalWidthInSecondsMax = PingRangeInSeconds .* (1 - sind(absAlpha) ./ sind(absAlpha - RxBeamWidthDegDepointe/2));
        SignalWidthInSecondsMin = abs(SignalWidthInSecondsMin);
        SignalWidthInSecondsMax = abs(SignalWidthInSecondsMax);
        %     figure(56586); plot(SignalWidthInSecondsMin, 'b'); grid on; hold on;  plot(SignalWidthInSecondsMax, 'k'); hold off;
        SignalWidthInSeconds = max(SignalWidthInSecondsMin, SignalWidthInSecondsMax);
        WCSignalWidthInSeconds(k,subNonNaNBeams) = SignalWidthInSeconds;
        % }
end
my_close(hw, 'MsgEnd')

%% Cr�ation des instances de IncidenceAngle, slope Across et Along

a = replace_Image(Emission, J);
a.DataType = cl_image.indDataType('IncidenceAngle');
a = majCoordonnees(a, subx, suby);
a = compute_stats(a);
a.TagSynchroContrast = num2str(rand(1));
CLim = [a.StatValues.Min a.StatValues.Max];
a.CLim = CLim;
a.ColormapIndex = 3;
a.Unit = 'deg';

a(2) = replace_Image(a(1), PAcross_deg);
a(2).DataType = cl_image.indDataType('SlopeAcross');
a(2) = majCoordonnees(a(2));
a(2) = compute_stats(a(2));
a(2).TagSynchroContrast = num2str(rand(1));
CLim = [a(2).StatValues.Min a(2).StatValues.Max];
a(2).CLim = CLim;
a(2).Unit = 'deg';

a(3) = replace_Image(a(1), PAlong_deg);
a(3).DataType = cl_image.indDataType('SlopeAlong');
a(3) = majCoordonnees(a(3));
a(3) = compute_stats(a(3));
a(3).TagSynchroContrast = num2str(rand(1));
CLim = [a(3).StatValues.Min a(3).StatValues.Max];
a(3).CLim = CLim;
a(3).Unit = 'deg';

a(4) = replace_Image(a(1), reflec_Enr2dB(InsonifiedArea));
% a(4) = replace_Image(a(1), reflec_Amp2dB(InsonifiedArea)); % test JMA le 12/02/2018
a(4).DataType = cl_image.indDataType('InsonifiedAreadB');
a(4) = majCoordonnees(a(4));
a(4) = compute_stats(a(4));
a(4).TagSynchroContrast = num2str(rand(1));
CLim = [a(4).StatValues.Min a(4).StatValues.Max];
a(4).CLim = CLim;
a(4).Unit = 'dB';

a(5) = replace_Image(a(1), WCSignalWidthInSeconds);
a(5).DataType = cl_image.indDataType('WCSignalWidth');
a(5) = majCoordonnees(a(5));
a(5) = compute_stats(a(5));
a(5).TagSynchroContrast = num2str(rand(1));
CLim = [a(5).StatValues.Min a(5).StatValues.Max];
a(5).CLim = CLim;
a(5).Unit = 's';

%{
% Ajout JMA pour �viter bug fichier SPFE 0012_20100225_031309_Belgica.all
for k=1:length(a)
    a(k).y = Emission.y(suby);
    a(k).x = Emission.y(suby);
end
%}

%% Compl�tion du titre

InitialFileName = Emission.InitialFileName;
for k=1:length(a)
    if ~isempty(InitialFileName)
        [~, ImageName] = fileparts(InitialFileName);
        a(k) = update_Name(a(k), 'Name', ImageName);
    else
        a(k) = update_Name(a(k));
    end
end
% SonarScope(a(4))
% pi

%% Export du fichier pour Imen

[pathname, ImageName] = fileparts(InitialFileName);
TableFilename = fullfile(pathname, [ImageName '-' nomAlgo '.csv']);

% T = readtable('C:\Temp\PourImen_xyz_from-rangesearch .csv');
T = table();
T.x   = val_x_PingBeam_refFCS;
T.y   = val_y_PingBeam_refFCS;
T.z   = val_z_PingBeam_refFCS;
T.kX  = kX;
T.kY  = kY;
T.NX  = NormalLocal_refFCS(1,:)';
T.NY  = NormalLocal_refFCS(2,:)';
T.NZ  = NormalLocal_refFCS(3,:)';
T.OAX = UOndeAcous_refFCS_FromXYZ(1,:)';
T.OAY = UOndeAcous_refFCS_FromXYZ(2,:)';
T.OAZ = UOndeAcous_refFCS_FromXYZ(3,:)';
T.xSonar = XGeoSoner_refFCS';
T.ySonar = YGeoSoner_refFCS';
T.zSonar = ZGeoSoner_refFCS';

head(T)
writetable(T, TableFilename)

%{
nomFicCC = 'D:\Temp\Imen\pppp\PourImen_xyz - Cloud_N_Plane_k10_R2,2.csv';
% nomFicCC = 'D:\Temp\Imen\pppp\PourImen_xyz - Cloud_N_Quadratic_k10_R2,2.csv';
% nomFicCC = 'D:\Temp\Imen\pppp\PourImen_xyz - Cloud_N_Triangulation_k10.csv';
TCC = readtable(nomFicCC);
head(TCC)

figure;
scatter3(p_FCS(:,1), p_FCS(:,2), p_FCS(:,3), [], p_FCS(:,3), '.'); colormap jet
subp = 1:100:nbPoints;
hold on;
subTCC = subp;
try
    quiver3(p_FCS(subp,1), p_FCS(subp,2), p_FCS(subp,3), TCC.Nx(subTCC), TCC.Ny(subTCC), TCC.Nz(subTCC), 2, '.k'); grid on; axis equal;
catch
    quiver3(p_FCS(subp,1), p_FCS(subp,2), p_FCS(subp,3), TCC.Var1(subTCC), TCC.Var2(subTCC), TCC.Var3(subTCC), 2, '.k'); grid on; axis equal;
end
[~, Title] = fileparts(nomFicCC);
title(Title, 'Interpreter', 'none')
%}

%% Visualisation des normales        
        
figure;
scatter3(p_FCS(:,1), p_FCS(:,2), p_FCS(:,3), [], p_FCS(:,3), '.'); colormap(jet(256))
subp = 1:500:nbPoints;
hold on;
quiver3(p_FCS(subp,1), p_FCS(subp,2), p_FCS(subp,3),...
    NormalLocal_refFCS(1, subp)', NormalLocal_refFCS(2, subp)', NormalLocal_refFCS(3,subp)', 2, '.k'); grid on; axis equal;
title(nomAlgo, 'Interpreter', 'none')
