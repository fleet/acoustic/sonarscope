% Recherche ou calcul de l'angle par rapport a la verticale
%
% Syntax
%   [subx, suby, AngleEmission, D, H] = sonar_angle_nadir(a, CasDepth, H, CasDistance, Bathy, Emission, ...)
%
% Input Arguments
%   a           : Instance de cl_image
%   CasDepth    : 1=Il existe un layer 'Depth', 2=Il existe une hauteur sonar, 3=On possede l'angle d'emission, 4=On fixe une hauteur arbitraire
%   H           : [] ou Hauteur sonar a donner
%   CasDistance : 1=Cas d'une donnee sonar en distance oblique, 2=Cas d'une donnee sonar en distance projetee, 3=On possede l'angle d'emission
%   Bathy       : [] ou instance de cl_image contenant la bathymetrie
%   Emission    : [] ou instance de cl_image contenant l'angle d'emission
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%   AngleEmission : Angle par rapport au nadire du navire
%   D             : Distance oblique
%   H             : Hauteur sous le bateau
%
% Examples
%   nomFic = getNomFicDatabase('DF1000_ExStr.imo')
%   a = cl_car_sonar('nomFic', nomFic);
%
%   b = view(a, 'Sonar.Ident', 8, 'subl', 1:500);
%   editobj(b)
%
%   c = get(b, 'Images');
%   imagesc(c)
%
%   H = get(c, 'SonarHeight');
%
%  [subx, suby, , AngleEmission, D, H] = sonar_angle_nadir(c, 2, H, 1, [], []);
%  figure; imagesc(AngleEmission); colorbar;
%  figure; imagesc(D); colorbar;
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [subx, suby, AngleEmission, D, H] = sonar_angle_nadir(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, FlagRoll] = getPropertyValue(varargin, 'FlagRoll', 1); %#ok<ASGLU>

%% Angle d'emission

if isempty(Emission)
    AngleEmission = [];
else
    
    % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
    % flipud (voir cl_image/plus
    
    [XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Emission); %#ok<ASGLU>
    if isempty(subx) || isempty(suby)
        str = sprintf('No intersection between the images');
        my_warndlg(str, 1);
        AngleEmission = [];
        D = [];
        H = [];
        return
    end
    AngleEmission = Emission.Image(subyb, subxb);
    
    SonarDescription = get(Emission, 'SonarDescription');
    SonarFamily = get(SonarDescription, 'Sonar.Family');
    if SonarFamily == 2 % MBES
        %         D = [];
        H = [];
    end
    
    if Emission.DataType == cl_image.indDataType('BeamPointingAngle')
        if FlagRoll
            AngleEmission = AngleEmission - repmat(this.Sonar.Roll(suby,1), 1, size(AngleEmission, 2));
        end
    else
        % Dans le cas d'un sonar lateral, c'est l'angle par rapport a la verticale qu'il faut prendre
        RollUsedForEmission =  Emission.Sonar.RollUsedForEmission;
        if ~isempty(RollUsedForEmission)
            switch size(RollUsedForEmission,2)
                case 1
                    AngleEmission = AngleEmission - repmat(RollUsedForEmission(suby,1), 1, size(AngleEmission, 2));
                case 2
                    N = length(suby);
                    str1 = 'Calcul des angles / verticale';
                    str2 = 'Computing the angles / nadir';
                    hw = create_waitbar(Lang(str1,str2), 'N', N);
                    for k=1:N
                        my_waitbar(k, N, hw)
                        Teta = AngleEmission(k,:);
                        subBab = Teta < 0;
                        subTri = Teta > 0;
                        Teta(subBab) = Teta(subBab) - RollUsedForEmission(suby(k),1);
                        Teta(subTri) = Teta(subTri) - RollUsedForEmission(suby(k),2);
                        AngleEmission(k,:) = Teta;
                    end
                    my_close(hw, 'MsgEnd')
                otherwise
                    str1 = 'Probl�me dans "sonar_angle_nadir", sauvegardez votre session et envoyez le tout � JMA SVP..';
                    str2 = 'There is a problem in "sonar_angle_nadir", please save you session and send it to JMA.';
                    my_warndlg(Lang(str1,str2), 1);
            end
        end
    end
    AngleEmission = abs(AngleEmission);
end

%% Information de hauteur

if CasDistance == 1
    H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
else
    switch CasDepth
        case 1  % Il existe un layer 'Depth'
            
            % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
            % flipud (voir cl_image/plus
            
            [XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Bathy); %#ok<ASGLU>
            if isempty(subx) || isempty(suby)
                str = sprintf('No intersection between the images');
                my_warndlg(str, 1);
                AngleEmission = [];
                D = [];
                H = [];
                return
            end
            H = abs(Bathy(1).Image(subyb, subxb));
        case 2  % Il existe une hauteur sonar
            H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
        case 3  % On possede l'angle d'emission
            disp('Message de JMA pour JMA : ATTENTION ICI DIVISION PAR ZERO A TRAITER mieux que ce qui existe')
            AngleEmission(AngleEmission == 0) = NaN;
            H = repmat(abs(this.x(subx)), length(suby), 1) ./ tan(AngleEmission * (pi/180)); % On possede l'angle d'emission
        case 4  % On fixe une hauteur arbitraire
        otherwise
            my_warndlg('No height data available',1);
            AngleEmission = [];
            D = [];
            H = [];
            return
    end
end

%% Information de distance

switch CasDistance
    case 1  % Cas d'une donn�e sonar en distance oblique
        D = repmat(abs(this.x(subx)), length(suby), 1);
    case 2  % Cas d'une donn�e sonar en distance projet�e
        D = sqrt(repmat(this.x(subx), length(suby), 1) .^ 2 + H .^ 2);
    case 3  % On poss�de l'angle d'emission
        h = 0; % TODO : h doit �tre la distance de l'antenne - heave, etc ...
        D = (H-h) ./ cosd(AngleEmission);
    otherwise
        my_warndlg('No range information available', 0);
        AngleEmission = [];
        D = [];
        H = [];
        return
end

%% Angle d'�mission

if isempty(Emission)
    D(D == 0) = NaN;
    D(abs(D) < H) = NaN;
    AngleEmission = acosd(H ./ D);
end

subNan = isnan(this.Image(suby, subx));
H(subNan) = NaN;
D(subNan) = NaN;
