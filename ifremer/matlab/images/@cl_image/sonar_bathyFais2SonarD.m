% Passage de coordonnees BathyFais en coordonnees SonarD
%
% Syntax
%   c = sonar_bathyFais2SonarD(a, Range, b, ...)
%
% Input Arguments
%   a     : instance de cl_image
%   Range : instance de cl_image contenant la distance oblique
%   b     : instances de cl_image
%
% Name-Value Pair Arguments
%   subx   : subsampling in X
%   suby   : subsampling in Y
%
% Output Arguments
%   c      : instances de cl_image
%   flag : 1=Operation reussie, 0=operation non reussie
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_bathyFais2SonarD(this, Range, a, varargin)

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, x]    = getPropertyValue(varargin, 'x', this.x); %#ok<ASGLU>

identEmissionBeam       = cl_image.indDataType('TxBeamIndex');
identReceptionBeam      = cl_image.indDataType('RxBeamIndex');
identMasque             = cl_image.indDataType('Mask');
identTexture            = cl_image.indDataType('Texture');
identResolAlong         = cl_image.indDataType('ResolAlong');
identResolImageAcross	= cl_image.indDataType('ResolImageAcross');
identResolBathyAcross   = cl_image.indDataType('ResolBathyAcross');
identRayPathSampleNb    = cl_image.indDataType('RayPathSampleNb');
identRayPathLength      = cl_image.indDataType('RayPathLength'); % One way length in meters
identDetectionType      = cl_image.indDataType('DetectionType');
identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');

%% Controles de validite

flag = testSignature(Range, 'DataType', [identRayPathSampleNb identRayPathLength identTwoWayTravelTimeInSeconds]);
if ~flag
    return
end

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'));   % test si SonarD
if ~flag
    return
end

if length(a) < 1
    str = sprintf('Il faut au moins une image a importer');
    my_warndlg(str, 1);
    flag = 0;
    return
end

for i=1:length(a)
    if ~strcmp(a(i).TagSynchroY , this.TagSynchroY)
        str = sprintf('Le TagSynchroY ne correspondent pas pour l''image Nu %d : %s', i, a(i).Name);
        my_warndlg(str, 1);
        flag = 0;
        return
    end
    flag = testSignature(a(i), 'GeometryType', cl_image.indGeometryType('PingBeam'));   % BathyFais
    if ~flag
        return
    end
end

SampleFrequency = this.Sonar.SampleFrequency;

%% Puisque tout est controle

subx = find((this.x >= min(x)) & (this.x <= max(x)));
warning('off', 'all');
% suby = sort(suby);
for i=1:length(a)
    [vdeb, vfin, subThis, subRange, suba] = lin_intersect(this.Sonar.PingCounter(suby), Range.Sonar.PingCounter, a(i).Sonar.PingCounter);
    %     [subThis, subRange] = intersect3(this.Sonar.PingCounter(suby), Range.Sonar.PingCounter, a(i).Sonar.PingCounter);
    N = length(subThis);
    
    % RangeSampleFrequency = Range.Sonar.SampleFrequency;
    % step = RangeSampleFrequency(subRange) ./ SampleFrequency(subThis);
    
    sz = [N length(subx)];
    %     if prod(sz) <= (2e7 / 8)
    %         I = NaN(sz,  class(a(i).Image(1)));
    %     else
    %         I = cl_memmapfile('Value', NaN, 'Size', sz, 'Format', class(a(i).Image(1)));
    %     end
    %     clear sz
    
    try
        I = NaN(sz,  class(a(i).Image(1)));
    catch %#ok<CTCH>
        I = cl_memmapfile('Value', NaN, 'Size', sz, 'Format', class(a(i).Image(1)));
    end
    
    clear sub3 tdeb tfin
    
    str = a(i).Name;
    str(length(str)+1:150) = '.';
    hw = create_waitbar(str, 'Entete', 'IFREMER - SonarScope : Processing PingBeam to PingSamples', 'N', N);
    clear str
    for k=1:N
        % GROS BINS A CORRIGER GROS BINS A CORRIGER GROS BINS A CORRIGER
        % GROS BINS A CORRIGER GROS BINS A CORRIGER GROS BINS A CORRIGER
        % GROS BINS A CORRIGER GROS BINS A CORRIGER GROS BINS A CORRIGER
        % GROS BINS A CORRIGER GROS BINS A CORRIGER GROS BINS A CORRIGER
        % GROS BINS A CORRIGER GROS BINS A CORRIGER GROS BINS A CORRIGER
        % GROS BINS A CORRIGER GROS BINS A CORRIGER GROS BINS A CORRIGER
        % GROS BINS A CORRIGER GROS BINS A CORRIGER GROS BINS A CORRIGER
        %         if xor(this.YDir == 1, this.y(2) > this.y(1))
        iImage = k;
        iligThis = suby(subThis(k));
        %         else
        %             iImage = N+1-k;
        %             iligThis = suby(subThis(iImage));
        %         end
        
        
        my_waitbar(k, N, hw);
        
        iligRange = subRange(k);
        
        t_Image = this.Sonar.PingCounter(iligThis);
        t_Range = Range.Sonar.PingCounter(iligRange);
        if t_Range ~= t_Image
            [~, iligRange] = find(min(abs(t_Image, t_Range)));
        end
        clear t_Image t_Range
        
        NuFais_SeabedImage = this.Image(iligThis,subx);
        subPixels = find(~isnan(NuFais_SeabedImage));
        
        if isempty(subPixels)
            continue
        end
        
        [NuFais, indNuFais] = unique(NuFais_SeabedImage(subPixels));
        NuFais(NuFais < 1) = 1;
        NuFais(NuFais > Range.nbColumns) = Range.nbColumns;
        signeRange = sign(this.x(subx(subPixels(indNuFais))));
        
        %         figure; plot(this.x(subPixels(indNuFais)),NuFais_SeabedImage(subPixels(indNuFais))); grid on;
        
        %         R = Range.Image(iligRange,NuFais) .* signeRange * (resolution(k)/2);
        %         R = Range.Image(iligRange,NuFais) .* signeRange / 2;
        
        if Range.DataType == identTwoWayTravelTimeInSeconds
            %             R = Range.Image(iligRange,NuFais) .* signeRange * this.Sonar.SampleFrequency(iligThis);
            R = Range.Image(iligRange,NuFais) .* signeRange * this.Sonar.SampleFrequency(iligThis);
        else
            R = Range.Image(iligRange,NuFais) .* signeRange;
        end
        clear signeRange
        if Range.DataType == identRayPathLength
            R = R * (SampleFrequency(iligRange) / (1500/2));
        end
        
        V =  a(i).Image(suba(k),NuFais);
        if sum(~isnan(V)) < 2
            continue
        end
        %         figure; plot(R,V); grid on
        
        switch a(i).DataType
            case {  identEmissionBeam
                    identReceptionBeam
                    identMasque
                    identTexture
                    identResolAlong
                    identResolImageAcross
                    identResolBathyAcross
                    identRayPathSampleNb
                    identDetectionType}
                
                % Interpolation en fonction du numero de faisceau
                V_SeabedImage = interp1(a(i).x(NuFais), V, NuFais_SeabedImage(subPixels), 'nearest');
                
            otherwise
                
                % -------------------------------------------------------
                % On supprime les retours en arriere.  C'est pas tres pro
                % mais il faut faire vite
                
                subREqual = (diff(R) == 0);
                if sum(subREqual) > 0
                    R(subREqual) = [];
                    V(subREqual) = [];
                end
                clear subREqual
                
                % Interpolation en fonction du Range
                subNonNan = (~isnan(R));
                if sum(subNonNan) < 2
                    fprintf('Vide en i=%d, k=%d\n',i,k)
                    continue
                end
                V_SeabedImage = my_interp1(double(R(subNonNan)), V(subNonNan), double(x(subPixels)), 'linear');%, 'extrap');
                %         figure; plot(double(R(subNonNan)), V(subNonNan)); grid on; hold on; plot(x(subPixels), V_SeabedImage, '-r+')
                %         figure; plot(double(R(subNonNan)), V(subNonNan), '-b+'); grid on;
                %         figure; plot(x(subPixels), V_SeabedImage, '-r+'); grid on
                clear subNonNan
        end
        %         figure; plot(this.x(subPixels), V_SeabedImage); grid on
        %         figure; plot(R,V); grid on; hold on; plot(this.x(subPixels), V_SeabedImage, 'r')
        I(iImage, subPixels) = V_SeabedImage;
        clear V_SeabedImage
    end
    my_close(hw, 'MsgEnd');
    
    b(i) = replace_Image(this, I);%#ok
    clear I
    
    b(i) = majCoordonnees(b(i), subx, suby(subThis)); %#ok
    b(i).DataType = a(i).DataType; %#ok
    b(i).Unit     = a(i).Unit; %#ok
    
    % -----------------------
    % Calcul des statistiques
    
    b(i) = compute_stats(b(i)); %#ok
    
    % -------------------------
    % Rehaussement de contraste
    
    b(i).TagSynchroContrast = num2str(rand(1)); %#ok
    CLim = [b(i).StatValues.Min b(i).StatValues.Max];
    b(i).CLim = CLim; %#ok
    
    b(i) = update_Name(b(i)); %#ok
    
end
warning('on', 'all');

% ---------------------------------
% Apparemment tout s'est bien passe

flag = 1;
this = b;
