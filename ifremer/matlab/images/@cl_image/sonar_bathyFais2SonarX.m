function [flag, this] = sonar_bathyFais2SonarX(this, Across, resolution, varargin)

for i=1:length(this)
    [flag, this(i)] = unitaire_sonar_bathyFais2SonarX(this(i), Across, resolution, varargin{:});
    if ~flag
        return
    end
end


function [flag, this] = unitaire_sonar_bathyFais2SonarX(this, Across, resolution, varargin)

warning('off', 'all');

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, XLim]         = getPropertyValue(varargin, 'XLim', []);
[varargin, MasquageDual] = getPropertyValue(varargin, 'MasquageDual', 0);

SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
switch SonarName
    case {'EM302'; 'EM122'; 'EM710'}
    otherwise
        if MasquageDual
            [flag, Masque] = sonar_masqueRecoveringBeams(Across);
            Across = masquage(Across, 'LayerMask', Masque, 'valMask', 1);
            this   = masquage(this, 'LayerMask', Masque, 'valMask', 1);
        end
end


% Pour corriger un bug rencontr� sur donn�es NIWA TAN0105 line001
% SonarMBESProjectionX d'un layer complementaire qui a une ligne de moins
% que le layer de bathymetrie
suby = suby((suby >= 1) & (suby <= this.nbRows));

[varargin, AcrossInterpolation] = getPropertyValue(varargin, 'AcrossInterpolation', 2); %#ok<ASGLU>

%% D�termination du type d'interpolation en fonction du type de l'image

[Method_Step1, Method_Step2] = getMethodInterpolation(this);

%% Projection de l'image sur la mosaique

TypeVar = class(this.Image(1));
if strcmp(TypeVar, 'uint8')
    ConvertInSingle = 1;
    TypeVar = 'single';
else
    ConvertInSingle = 0;
end

PingCounter               = this.Sonar.PingCounter;
AcrossDistancePingCounter = Across.Sonar.PingCounter;

[subsuby, subAcross] = intersect3(PingCounter(suby,1), ...
    AcrossDistancePingCounter(:,1), ...
    AcrossDistancePingCounter(:,1));
M = length(subsuby);
resolution = double(resolution(1));
XMin = double(Across.StatValues.Min) - resolution;
XMax = double(Across.StatValues.Max) + resolution;
x = XMin:resolution:XMax;
if ~isempty(XLim)
    x(x < XLim(1)) = [];
    x(x > XLim(2)) = [];
end
[x, resolution, XMin, XMax] = centrage_magnetique(x); %#ok

Nx = length(x);

nbSlides = this.nbSlides;
try
    I = NaN(M, Nx, nbSlides, TypeVar);
catch %#ok<CTCH>
    I = cl_memmapfile('Value', NaN, 'Size', [M Nx nbSlides], 'Format', TypeVar);
end

% strTC = this.strGeometryType{this.GeometryType};
% str = sprintf('%s : %s : ', this.Name, strTC);
str = this.Name;
str(length(str)+1:150) = '.';

PremiereAlerte = 1;
for iCan=1:nbSlides
    hw = create_waitbar(str, 'Entete', 'IFREMER - SonarScope : Processing PingBeam to PingAcrossDist', 'N', M);
    %     set(get(get(hw, 'children'), 'Title'), 'Interpreter', 'none')
    for i=1:M
        my_waitbar(i, M, hw);
        
        Ligne_Val = this.Image(suby(subsuby(i)), subx, iCan);
        if ConvertInSingle
            Ligne_Val = single(Ligne_Val);
        end
        
        Ligne_Across = Across.Image(subAcross(i), subx);
        %       Ligne_sub = find(~isnan(Ligne_Across) & (Ligne_Across ~= 0));
        %       ATTENTION : modifi� pour fichier MBG du Geoswath par JM le 06/04/2011
        %       TODO:  V�rifier si �a ne g�n�re pas d'effets de bords pour d'autres sonars
        Ligne_sub = find(~isnan(Ligne_Across) & (Ligne_Across ~= 0) & ~isnan(Ligne_Across) & ~isnan(Ligne_Val));
        
        if isempty(Ligne_sub)
            continue
        end
        
        Ligne_Val = Ligne_Val(Ligne_sub);
        if  sum(~isnan(Ligne_Val)) <= 1
            continue
        end
        
        Ligne_x = (Ligne_Across(Ligne_sub) - XMin) / resolution;
        
        % -----------------------------------------
        % Detection si la navigation est quantifi�e
        
        deltax = median(diff(Ligne_x));
        if PremiereAlerte && ((deltax == 0))
            str1 = 'Il semble que "AcrossDistance" soit quantifi�e. Je continue le traitement mais si vous apercevez des choses �tranges rappelez vous que cela peut �tre l''origine.';
            str2 = 'It seems your AcrossDistance is quantified. I continue processing but if you see strange things on your image keep in mind it can be due to this point.';
            my_warndlg(Lang(str1,str2),  0, 'Tag', 'DataQuantified');
            PremiereAlerte = 0;
        end
        %         indCassures = find(diff(Ligne_x) > (10*deltax));
        %         Ligne_x(indCassures) = NaN;
        deltax = mean(diff(Ligne_x));
        
        switch AcrossInterpolation
            case 2  % Interpolation lineaire
                %% ICI : FAIRE UNE FONCTION INTELLIGENTE qui ferait une
                %% interpolation ou une r�duction de donn�e en fonction de
                %% l'ndroit o� on se trouve sur Ligne_x
                %% Tester cette m�thode sur PPPPPPPPPPPPPPPPPPPPPPPP
                % [Ligne_x, Ligne_Val] = my_xxxx(Ligne_x, Ligne_Val);
                
                if deltax > 1 % Interpolation
                    NPoints = length(Ligne_x);
                    %                     NPointsInterpoles = floor(1.5 * deltax * NPoints);
                    NPointsInterpoles = floor(4 * deltax * length(subx));
                    newx = linspace(1,NPoints, NPointsInterpoles);
                    Ligne_x   = my_interp1(1:NPoints, Ligne_x, newx);
                    Ligne_Val = my_interp1(1:NPoints, Ligne_Val, newx, Method_Step1);% , 'spline'); % A TESTER sur une image presentant des dunes
                    
                    % DEBUT : MODIF JMA 07/11/06
                    %               elseif deltax < 0.8
                else
                    % FIN : MODIF JMA 07/11/06
                    MinLigne_x = min(Ligne_x);
                    MaxLigne_x = max(Ligne_x);
                    NPointsInterpoles = (MaxLigne_x - MinLigne_x) / abs(deltax);
                    if NPointsInterpoles < 2
                        continue
                    end
                    
                    % Pour �viter bug rencontr� sur fichier Simrad UNH o�
                    % la latitude est une constante parfaite sur tout le
                    % ping
                    Ligne_x = double(Ligne_x);
                    
                    %                     newx = linspace(Ligne_x(1), Ligne_x(end),
                    %                     NPointsInterpoles); % Modifi� le 06/04/2011
                    newx = linspace(MinLigne_x, MaxLigne_x, NPointsInterpoles);
                    
                    % Pour supprimer eventuellement un message de interp1. A
                    % voir si c'est mieux de faire �a ou si warning('off')
                    % serait pas mieux : A TESTER sur des donn�es fortement
                    % lacunaires
                    
                    %                 subNonNan = find(~isnan(Ligne_x) & ~isnan(Ligne_Val) & ~isnan(Ligne_y));
                    %                 if isempty(subNonNan)
                    %                     continue
                    %                 end
                    %                 Ligne_Val = my_interp1(Ligne_x(subNonNan), Ligne_Val(subNonNan), newx, Method_Step1);% , 'spline'); % A TESTER sur une image presentant des dunes
                    %                 Ligne_y   = my_interp1(Ligne_x(subNonNan), Ligne_y(subNonNan),   newx);
                    
                    Ligne_Val = my_interp1(Ligne_x, Ligne_Val, newx, Method_Step1);% , 'spline'); % A TESTER sur une image presentant des dunes
                    Ligne_x   = newx;
                    %                     deltax = median(diff(Ligne_x));
                    deltax = mean(diff(Ligne_x));
                    if isempty(deltax == 0)
                        continue
                    end
                    
                    
                    FacteurReduction = floor(0.5 / deltax);
                    if FacteurReduction > 1
                        Ligne_x = downsize(Ligne_x, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        if isempty(Ligne_x)
                            continue
                        end
                        Ligne_Val = downsize(Ligne_Val, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                    end
                end
                
            case 3  % Filtrage
                disp('Pas encore au point')
                return
                
            otherwise % Pas d'interpolation
        end
        
        Ligne_x = 1 + floor(Ligne_x);
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Ca ne devrait pas arriver mais c'est arriv� sur un fichier Reson 7125
        subNaN = find((Ligne_x > Nx) | (Ligne_x < 1));
        if ~isempty(subNaN)
            Ligne_x(subNaN) = [];
            Ligne_Val(subNaN) = [];
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        sub = sub2ind([M Nx nbSlides], repmat(i, 1, length(Ligne_x)), double(Ligne_x), iCan*ones(size(Ligne_x)));
        subVide = find(isnan(I(sub)));
        
        if isempty(Method_Step2)
            I(sub(subVide)) = Ligne_Val(subVide);
        else
            I(sub(subVide)) = Method_Step2(Ligne_Val(subVide));
        end
    end
    my_close(hw, 'MsgEnd')
end

%% Interpolation dans le sens longitudinal

if ConvertInSingle
    I(isnan(I)) = this.ValNaN;
    I = feval(class(this.Image(1)), I);
end

this = replace_Image(this, I);
% figure; imagesc(x, y, I); colorbar;

%% Mise � jour de coordonnees

this.x = x;
this.GeometryType = cl_image.indGeometryType('PingAcrossDist');
this = majCoordonnees(this, 1:Nx, suby(subsuby));

%% Remise � jour des descripteurs de l'image

% this.y = this.y(suby(subsuby));
% this.XLim = compute_XYLim(x);
% this.YLim = compute_XYLim(this.y);

this.Sonar.ResolutionX = resolution;
this.XUnit = 'm';

k = strfind(this.TagSynchroX, ' - Ping');

if isempty(k)
    k = length(this.TagSynchroX) + 1; % Correctif pour images synth�se BD Reson : � supprimer plus tard
end

this.TagSynchroX = [this.TagSynchroX(1:k(end)-1) ' - PingAcrossDist'];
% this.TagSynchroX = [this.TagSynchroX(1:end-11) ' - PingAcrossDist'];

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% On comble les trous

ImageName = this.Name;

% disp('sonar_bathyFais2SonarX : ici je faisais WinFillNaN : faut-il le remettre en ervice ?')
% EST-CE VRAIMENT UTILE ????????????????
% this = WinFillNaN(this);
% EST-CE VRAIMENT UTILE ????????????????

this.Name = ImageName;

%% Compl�tion du titre

this = update_Name(this);

this.InitialImageName = [this.InitialImageName sprintf('From %d to %d', suby(1), suby(end))];

%% Apparemment tout s'est bien pass�

flag = 1;

warning('on', 'all');
