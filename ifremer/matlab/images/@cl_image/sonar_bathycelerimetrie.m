% Edition de la bathycelerimetrie Levitus du centre de la zone (ATTENTION : Resultat non exploite dans l''image, ca reste a faire)
%
% Syntax
%   b = sonar_bathycelerimetrie(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', 9, 'Carto', Carto);
%   c = get_Image(b, 1);
%   imagesc(c)
%
%   sonar_bathycelerimetrie(c)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_bathycelerimetrie(this, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_sonar_bathycelerimetrie(this(i), varargin{:});
end


function this = unitaire_sonar_bathycelerimetrie(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

BathyCel = get_SonarBathyCel(this);
% figure; plot(BathyCel.SoundSpeed(:) , BathyCel.Depth(:)); grid
if ~isempty(BathyCel)
    if isfield(BathyCel, 'Depth') && ~isempty(BathyCel.Depth)
        try % pour contourner bug de profil de c�l�rit� pass� par r�f�rence (bug identifi� par Anne-Laure)
            Coul = 'bkrvmc';
            FigUtils.createSScFigure;
            for i=1:length(BathyCel.TimeStartOfUse)
                iCoul = 1 + mod(i-1,length(Coul));
                PlotUtils.createSScPlot(BathyCel.SoundSpeed(i,:), BathyCel.Depth(i,:), Coul(iCoul)); grid on; hold on;
                Legende{i} = ['StartOfUse : ' t2str(BathyCel.TimeStartOfUse(i))]; %#ok<AGROW>
            end
            legend(Legende)
            %         return
        catch %#ok<CTCH>
            str1 = 'Le profil de c�l�rit� attach� � cette image n''est pas lisible.';
            str2 = 'The sound speed profile linked to this image seems corrupted.';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProfilCeleriteBuggue');
        end
    elseif isfield(BathyCel, 'Z') && ~isempty(BathyCel.C)
        try % pour contourner bug de profil de c�l�rit� pass� par r�f�rence (bug identifi� par Anne-Laure)
            Coul = 'bkrvmc';
            FigUtils.createSScFigure;
            for i=1:length(BathyCel.TimeStartOfUse)
                iCoul = 1 + mod(i-1,length(Coul));
                PlotUtils.createSScPlot(BathyCel.C(i,:), BathyCel.Z(i,:), Coul(iCoul)); grid on; hold on;
                Legende{i} = ['StartOfUse : ' t2str(BathyCel.TimeStartOfUse(i))]; %#ok<AGROW>
            end
            legend(Legende)
            %         return
        catch %#ok<CTCH>
            str1 = 'Le profil de c�l�rit� attach� � cette image n''est pas lisible.';
            str2 = 'The sound speed profile linked to this image seems corrupted.';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProfilCeleriteBuggue');
        end
    end
end

%% Contr�les

SonarDepth = [];
switch this.GeometryType
    case {cl_image.indGeometryType('PingBeam'); cl_image.indGeometryType('PingSamples'); cl_image.indGeometryType('PingAcrossDist')}
        LatFish = this.Sonar.FishLatitude(suby);
        LonFish = this.Sonar.FishLongitude(suby);
        LatCentre = mean(LatFish, 'omitnan');
        LonCentre = mean(LonFish, 'omitnan');
        if isempty(this.Sonar.Immersion)
            SonarDepth = 0;
        else
            SonarDepth = this.Sonar.Immersion(suby);
            SonarDepth = mean(SonarDepth, 'omitnan');
        end
        Date = this.Sonar.Time(2).date;
        [Jour, Mois, Annee] = dayIfr2Jma(Date);%#ok
        SonarDescription = get(this, 'SonarDescription');
        Frequency = get(SonarDescription, 'Signal.Freq');
        
    case {cl_image.indGeometryType('LatLong') cl_image.indGeometryType('GeoYX')}
        LatCentre = mean(this.y(suby), 'omitnan');
        LonCentre = mean(this.x(subx), 'omitnan');
        
        if this.GeometryType == cl_image.indGeometryType('GeoYX')
            [LatCentre, LonCentre] = xy2latlon(this.Carto, LonCentre, LatCentre);
        end
        
        SonarDescription = get(this, 'SonarDescription');
        if isempty(SonarDescription)
            Frequency = 0;
            while Frequency == 0
                [flag, Frequency] = inputOneParametre('Sonar Frequency', 'Frequency', ...
                    'Value', 0, 'Unit', 'kHz', 'MinValue', 0, 'MaxValue', 1000);
                if ~flag
                    return
                end
                
                if Frequency == 0
                    my_warndlg('Frequency cannot be equal to 0', 1);
                end
            end
        else
            SonarDescription = get(this, 'SonarDescription');
            Frequency = get(SonarDescription, 'Signal.Freq');
            % SonarDescription = cl_sounder;
            % [flag, SonarDescription] = selection(SonarDescription);
        end
        
        str = {'January'; 'February'; 'March'; 'April'; 'May'; 'Juin'; 'July'; 'August'; 'Sptember'; 'October'; 'November'; 'December'};
        [Mois, flag] = my_listdlg(Lang('Mois : ', 'Month : '), str);
        if ~flag
            return
        end
        
    otherwise
        my_warndlg('Geometry not ready in sonar_bathycelerimetrie', 1);
        return
end

str1 = 'Le profil de c�l�rit� montr� ci-apr�s est une extraction Levitus correspondant au point central de l''image.';
str2 = 'The following SVP is a Levitus extraction corresponding to the central part of the image.';
my_warndlg(Lang(str1,str2), 0);

a = cli_sound_speed('Incoming', 2, 'LevitusAveragingType', 3, ...
    'Latitude', LatCentre, 'Longitude', LonCentre, ...
    'Frequency', Frequency, 'LevitusMonth', Mois, 'SonarDepth', SonarDepth);
depth = get(a, 'Depth');
if isempty(depth)
    return
end

if ~isempty(BathyCel) && isfield(BathyCel, 'Depth') && ~isempty(BathyCel.Depth)
    DepthLevitus       = get(a, 'Depth');
    TemperatureLevitus = get(a, 'Temperature');
    SalinityLevitus    = get(a, 'Salinity');
    
    Depth       = BathyCel.Depth(1,:);
    SoundSpeed  = BathyCel.SoundSpeed(1,:);
    Temperature = interp1(DepthLevitus, TemperatureLevitus, Depth);
    Salinity    = interp1(DepthLevitus, SalinityLevitus, Depth);
    [LocalAbsorption, AveragedAbsorption] = AttenuationGarrison(Frequency, Depth, Temperature, Salinity);
    a = set(a, 'Incoming', 4, 'Depth', Depth(:), 'Temperature', Temperature(:), 'Salinity', Salinity(:), ...
        'SoundSpeed', SoundSpeed(:), 'LocalAbsorption', LocalAbsorption, 'AveragedAbsorption', AveragedAbsorption);
end
pause(1)
editobj(a);
