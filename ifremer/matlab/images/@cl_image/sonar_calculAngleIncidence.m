% Calcul de l'image d'angle d'incidence

function [flag, b] = sonar_calculAngleIncidence(this, Bathy, Heading, varargin)

[varargin, CreateLayersSlope] = getPropertyValue(varargin, 'CreateLayersPente', 0);

b = cl_image.empty;

N = length(this);
str1 = 'Calcul des images d''angles d''incidence.';
str2 = 'Computing Incidence angle images.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    switch length(Bathy)
        case 1
            B = Bathy;
        case N
            B = Bathy(k);
        otherwise
            flag = 0;
            return
    end
    [flag, a] = sonar_calculAngleIncidence_unitaire(this(k), B, Heading(k), varargin{:});
    if ~flag
        return
    end
    if CreateLayersSlope
        b = [b; a(:)]; %#ok<AGROW>
    else
        b = [b; a(3)]; %#ok<AGROW>
    end
end
my_close(hw)


function [flag, a] = sonar_calculAngleIncidence_unitaire(this, Bathy, Heading, varargin)

a = [];

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, SlopeN] = getPropertyValue(varargin, 'PenteN', []);
[varargin, SlopeW] = getPropertyValue(varargin, 'PenteW', []); %#ok<ASGLU>

%% Calcul des parties communes

[XLim, YLim, subx, suby, subxZ, subyZ] = intersectionImages(this, subx, suby, Bathy, 'SameSize'); %#ok<ASGLU>
[Bathy, flag] = extraction(Bathy, 'subx', subxZ, 'suby', subyZ, 'ForceExtraction');
% [Bathy, flag] = extraction(Bathy, 'x', this.x(subx), 'y', this.y(suby), 'SameFrame');
if ~flag
    return
end

%{
%% Extraction dans le m�me cadre que l'image d'origine

[flag, Bathy] = recadrage(Bathy, 'XLim', this.XLim, 'YLim', this.YLim);
if ~flag
return
end
%}

%% Calcul de la pente Nord-Sud

if isempty(SlopeN)
    [SlopeN, flag] = slopeAzimuth(Bathy, 'Azimuth', 0); %, 'subx', subxZ, 'suby', subyZ);
    if ~flag
        return
    end
end

%% Calcul de la pente Est-West

if isempty(SlopeW)
    [SlopeW, flag] = slopeAzimuth(Bathy, 'Azimuth', 90); %, 'subx', subxZ, 'suby', subyZ);
    if ~flag
        return
    end
end

%% Calcul de l'angle d'incidence

% TODO : attention, �a va beuguer, il manque des variables � l'appel de cette fonction
[flag, a] = sonar_angle_incidence(this, Heading, SlopeN, SlopeW);

%% On r�cup�re les images de pentes Nord-Sud et Ouest-Est

InitialFileName = this.InitialFileName;
if ~isempty(InitialFileName)
    [~, ImageName] = fileparts(InitialFileName);
    SlopeN = update_Name(SlopeN, 'Name', ImageName);
    SlopeW = update_Name(SlopeW, 'Name', ImageName);
else
    SlopeN = update_Name(SlopeN);
    SlopeW = update_Name(SlopeW);
end

a = [SlopeN SlopeW a];
