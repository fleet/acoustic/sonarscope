function [flag, a] = sonar_calculAngleIncidenceinLatLongGeometry(this, indImage, varargin)

[varargin, CreateLayersSlope] = getPropertyValue(varargin, 'CreateLayersPente', 0);
[varargin, SlopeN]            = getPropertyValue(varargin, 'PenteN', []);
[varargin, SlopeW]            = getPropertyValue(varargin, 'PenteW', []); %#ok<ASGLU>

a = [];

identEmissionAngle     = cl_image.indDataType('TxAngle');
identRxBeamAngle       = cl_image.indDataType('RxBeamAngle');
identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');
identRxAngleEarth      = cl_image.indDataType('RxAngleEarth');
flag = testSignature(this(indImage), 'DataType', [identEmissionAngle identRxBeamAngle identBeamPointingAngle identRxAngleEarth], ...
    'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

identBathymetry = cl_image.indDataType('Bathymetry');
[flag, indLayerBathymetry] = findOneLayerDataType(this, indImage, identBathymetry);
if ~flag
    return
end

identHeading = cl_image.indDataType('Heading');
[flag, indLayerHeading] = findOneLayerDataType(this, indImage, identHeading);
if ~flag
    return
end

%% Calcul de l'image d'angle d'incidence

[flag, a] = sonar_calculAngleIncidence(this(indImage), this(indLayerBathymetry), this(indLayerHeading), ...
    'CreateLayersPente', CreateLayersSlope, 'PenteN', SlopeN, 'PenteW', SlopeW);
if ~flag
    return
end

