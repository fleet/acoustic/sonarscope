% Calcul de la reflectivite angulaire
%
% Syntax
%   b = sonar_calcul_BS(a, N, A,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   N : Instance de cl_image contenant
%   A : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_calcul_BS(this, N, A, nomZoneEtude, commentaire, couleur, varargin)

nbImages = length(this);
str1 = 'Calcul des courbes de r�flectivit� angulaire';
str2 = 'Compute angular BS curves';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    this(k) = unitaire_sonar_calcul_BS(this(k), N, A, nomZoneEtude, commentaire, couleur, varargin{:});
end
my_close(hw, 'MsgEnd')


function this = unitaire_sonar_calcul_BS(this, N, A, nomZoneEtude, commentaire, couleur, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask]    = getPropertyValue(varargin, 'LayerMask',    []);
[varargin, valMask]      = getPropertyValue(varargin, 'valMask',      []);
[varargin, bins]         = getPropertyValue(varargin, 'bins',         []);
[varargin, SelectMode]   = getPropertyValue(varargin, 'SelectMode',   []);
[varargin, SelectSwath]  = getPropertyValue(varargin, 'SelectSwath',  []);
[varargin, SelectFM]     = getPropertyValue(varargin, 'SelectFM',     []);
[varargin, MeanCompType] = getPropertyValue(varargin, 'MeanCompType', 1); %#ok<ASGLU>

%% Contr�les

flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'));
if ~flag
    this = [];
    return
end

identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');
identRxAngleEarth = cl_image.indDataType('RxAngleEarth');
flag = testSignature(A, 'DataType', [cl_image.indDataType('TxAngle') cl_image.indDataType('IncidenceAngle') cl_image.indDataType('RxBeamAngle') identBeamPointingAngle identRxAngleEarth]);
if ~flag
    this = [];
    return
end

%% Si angle des faisceaux par rapport � l'antenne alors il faut prendre en compte le roulis

if A.DataType == identBeamPointingAngle
    %     Roll = NaN(this.nbRows, 1, 'single');
    
    [~, ~, ~, suby, ~, subyA] = intersectionImages(this, subx, suby, A);
    
    if isempty(subx) || isempty(suby)
        str1 = 'Il n''y a pas d''intersection entre les images utilis�es pour cette compensation.';
        str2 = 'There is no intersection between the images used for this compensation.';
        my_warndlg(Lang(str1,str2), 1);
        %         flag = 0;
        return
    end
    
    if length(subyA) ~= A.nbRows
        A = extraction(A, 'suby', subyA);
    end
    
    Roll = this.Sonar.Roll(:,:);
    A = A - Roll(subyA,1);
end

%% Calcul de la r�flectivit� angulaire

this = courbesStats(this, 'BS', nomZoneEtude, commentaire, N, A, [], ...
    'LayerMask', LayerMask, 'valMask', valMask, ...
    'GeometryType', this.GeometryType, ...
    'couleur', couleur, 'subx', subx, 'suby', suby, 'bins', {bins}, ...
    'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, ...
    'MeanCompType', MeanCompType);
if isempty(this)
    return
end
