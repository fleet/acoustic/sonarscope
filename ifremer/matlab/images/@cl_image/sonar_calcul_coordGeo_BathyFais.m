% Calcul des layers latitude et longitude
%
% Syntax
%   [flag, b] = sonar_calcul_coordGeo_BathyFais(a, AcrossDistance, AlongDistance, ...
%                                       Heading, FishLatitude, FishLongitude, ...)
%
% Input Arguments
%   a : Instance de cl_image
%   AcrossDistance : Instance de cl_image contenant les distances transversales
%   AlongDistance  : Instance de cl_image contenant les distances longitudinales
%   Heading        : Cap du navire (deg)
%   FishLatitude   : Latitude du navire (deg)
%   FishLongitude  : Longitude du navire (deg)
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%   LonLim : Encadrement impos� en x ([] par d�faut)
%   LatLim : Encadrement impos� en y ([] par d�faut)
%
% Output Arguments
%   b : Instances de cl_image contenant la latitude et la longitude
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonar_calcul_coordGeo_BathyFais(this, AcrossDistance, AlongDistance, ...
    Heading, FishLatitude, FishLongitude, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LonLim]            = getPropertyValue(varargin, 'LonLim',            []);
[varargin, LatLim]            = getPropertyValue(varargin, 'LatLim',            []);
[varargin, AcrossDistanceLim] = getPropertyValue(varargin, 'AcrossDistanceLim', []);
[varargin, Mute]              = getPropertyValue(varargin, 'Mute',              0); %#ok<ASGLU>

a  = [];

%% Contr�les

listGeometryType = [cl_image.indGeometryType('PingSamples') ...
    cl_image.indGeometryType('PingAcrossDist') ...
    cl_image.indGeometryType('PingBeam') ...
    cl_image.indGeometryType('PingRange')];
flag = testSignature(this, 'GeometryType', listGeometryType);   % 5 pour une images sonar multifaisceau
if ~flag
    return
end

identLayerAcrossDist = cl_image.indDataType('AcrossDist');
flag = testSignature(AcrossDistance, 'GeometryType', listGeometryType, 'DataType', identLayerAcrossDist);
if ~flag
    return
end

identLayerAlongDist = cl_image.indDataType('AlongDist');
flag = testSignature(AlongDistance, 'GeometryType', listGeometryType, 'DataType', identLayerAlongDist);
if ~flag
    return
end

if isempty(Heading)
    str1 = '"Heading" non renseign�';
    str2 = '"Heading" not set';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if all(isnan(Heading))
    str1 = 'All "Heading" set to NaN';
    str2 = 'All "Heading" set to NaN';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if isempty(FishLatitude)
    str1 = '"FishLatitude" non renseign�';
    str2 = '"FishLatitude" not set';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if isempty(FishLongitude)
    str1 = '"FishLongitude" non renseign�';
    str2 = '"FishLongitude" not set';
    my_warndlg(Lang(str1,str2), 1);
    return
end

AcrossDistanceImage = AcrossDistance.Image(:,:);
AlongDistanceImage  = AlongDistance.Image(:,:);

if ~isempty(AcrossDistanceLim)
    sub = (AcrossDistanceImage < AcrossDistanceLim(1)) | (AcrossDistanceImage > AcrossDistanceLim(2));
    AcrossDistanceImage(sub) = NaN;
end

if isempty(this.Carto)
    str1 = 'Pas de carto d�finie pour cette image';
    str2 = 'No cartography defined in this image';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

Lat = this;
Lon = this;
% Lat = clone(this);
% Lon = clone(this); % Ajout JMA le 22/02/2022 apr�s transformation de la classe cl_image (classdef ...)

[subsuby, subyAcross, subyAlong] = intersect3(this.Sonar.PingCounter(suby, 1), ...
    AcrossDistance.Sonar.PingCounter(:,1), AlongDistance.Sonar.PingCounter(:,1));
suby  = suby(subsuby);

if isempty(suby)
    flag = 0;
    return
end

[~, ordre] = sort(suby);
suby = suby(ordre);
subyAcross = subyAcross(ordre);
subyAlong  = subyAlong(ordre);

M = length(suby);

[subsubx, subxAcross, subxAlong] = intersect3(this.x(subx), ...
    AcrossDistance.x, AlongDistance.x);
subx  = subx(subsubx);

Heading = double(Heading);

Lat = replace_Image(Lat, NaN(M, length(subsubx), 'double'));
Lat.Writable = true;
Lon = replace_Image(Lon, NaN(M, length(subsubx), 'double'));
Lon.Writable = true;
E2  = get(this.Carto, 'Ellipsoide.Excentricite') ^ 2;
A   = get(this.Carto, 'Ellipsoide.DemiGrandAxe');

if A == 0
    str1 = 'L''ellipso�de n''est pas d�finie.';
    str2 = 'The ellipssoid is not defined.';
    my_warndlg(Lang(str1,str2), 1);
end

str1 = 'Calcul les la latitude et de la longitude';
str2 = 'Processing Latitude & longitude';
strLoop = Lang(str1,str2);
for k=1:M
    if ~Mute
        infoLoop(strLoop, k, M, 'pings')
    end
    iy = suby(k);
    
    AlongD  = AlongDistanceImage( subyAlong(k),  subxAlong);
    AcrossD = AcrossDistanceImage(subyAcross(k), subxAcross);
    [LatPing, LonPing] = calculLatLonFromAlongAcross(E2, A, FishLatitude(iy), FishLongitude(iy), Heading(iy), AlongD, AcrossD);
    Lat.Image(k,:) = LatPing;
    Lon.Image(k,:) = LonPing;
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Lon.Image(k,:), Lat.Image(k,:), '*r'); grid on;
end

%% Gestion de l'encadrement

if ~isempty(LonLim)
    sub = (Lon.Image < LonLim(1)) | (Lon.Image > LonLim(2));
    Lat.Image(sub) = NaN;
    Lon.Image(sub) = NaN;
end
if ~isempty(LatLim)
    sub = (Lat.Image < LatLim(1)) | (Lat.Image > LatLim(2));
    Lat.Image(sub) = NaN;
    Lon.Image(sub) = NaN;
end

%% Mise � jour de coordonn�es

Lat = majCoordonnees(Lat, subx, suby);
Lon = majCoordonnees(Lon, subx, suby);
Lat.Unit = 'deg';
Lon.Unit = 'deg';

%% Calcul des statistiques

Lat = compute_stats(Lat);
Lon = compute_stats(Lon);

%% Rehaussement de contraste

Lat.TagSynchroContrast = num2str(rand(1));
CLim = [Lat.StatValues.Min Lat.StatValues.Max];
Lat.CLim = CLim;
Lat.ColormapIndex = 3;

Lon.TagSynchroContrast = num2str(rand(1));
CLim = [Lon.StatValues.Min Lon.StatValues.Max];
Lon.CLim = CLim;
Lon.ColormapIndex = 3;

Lat.DataType = cl_image.indDataType('Latitude');
Lon.DataType = cl_image.indDataType('Longitude');

%% Compl�tion du titre

Lat = update_Name(Lat);
Lon = update_Name(Lon);

%% Par ici la sortie

a = Lat;
a(2) = Lon;
