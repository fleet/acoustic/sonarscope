% Calcul des layers latitude et longitude
%
% Syntax
%   [flag, b] = sonar_calcul_coordGeo_SonarX(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx           : subsampling in X
%   suby           : subsampling in Y
%   Heading        : Cap du navire (deg)
%   FishLatitude   : Latitude du navire (deg)
%   FishLongitude  : Longitude du navire (deg)
%
% Output Arguments
%   b : Instances de cl_image contenant la latitude et la longitude
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonar_calcul_coordGeo_SonarX(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Heading]       = getPropertyValue(varargin, 'Heading', []);
[varargin, FishLatitude]  = getPropertyValue(varargin, 'FishLatitude', []);
[varargin, FishLongitude] = getPropertyValue(varargin, 'FishLongitude', []);
[varargin, NoWaitbar]     = getPropertyValue(varargin, 'NoWaitbar', 0); %#ok<ASGLU>

a = [];

%% Controles

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingAcrossDist'));
if ~flag
    return
end

if isempty(Heading)
    Heading = this.Sonar.Heading;
    if isempty(Heading)
        my_warndlg('Heading non renseigne', 1);
        return
    end
end

if isempty(FishLatitude)
    FishLatitude = this.Sonar.FishLatitude;
    if isempty(FishLatitude)
        my_warndlg('FishLatitude non renseigne', 1);
        return
    end
end

if isempty(FishLongitude)
    FishLongitude = this.Sonar.FishLongitude;
    if isempty(FishLongitude)
        errordlg('FishLongitude non renseigne', 'IFREMER');
        return
    end
end

E = get(this.Carto, 'Ellipsoide.Excentricite');
E2 = E ^ 2;
A = get(this.Carto, 'Ellipsoide.DemiGrandAxe');

Lat = this;
Lon = this;

M = length(suby);

Lat = replace_Image(Lat, NaN(M, length(subx), 'double'));
Lon = replace_Image(Lon, NaN(M, length(subx), 'double'));

Cap = double(this.Sonar.Heading) * (pi / 180);
str = this.Name;
str(length(str)+1:150) = '.';
hw = create_waitbar(str, 'Entete', 'IFREMER - SonarScope : Processing Latitude & Longitude', 'N', M, 'NoWaitbar', NoWaitbar);
% set(get(get(hw, 'children'), 'Title'), 'Interpreter', 'none')
for k=1:length(suby)
    my_waitbar(k, M, hw);
    
    iy = suby(k);
    
    LN = FishLatitude(iy) * (pi/180);
    x = 1 - E2 * sin(LN) .^ 2;
    NORM = A / sqrt(x);
    RAY0 = NORM * (1 - E2) / (x);
    
    c = cos(Cap(iy));
    s = sin(Cap(iy));
    Rotation = [c s; -s c];
    
    x = double(this.x(subx));   % Au cas o�, x soit en single (ce qui serait une grosse erreur)
    subNonNaN = find(~isnan(this.Image(k,subx)));
    x = x(subNonNaN);
    y = zeros(size(x));
    xy = Rotation * [x;y];
    
    lat = FishLatitude(iy)  + (180/pi) * (xy(2,:) / RAY0);
    lon = FishLongitude(iy) + (180/pi) * (xy(1,:) / NORM / cos(LN));
    
    Lat.Image(k,subNonNaN) = lat;
    Lon.Image(k,subNonNaN) = lon;
end
my_close(hw, 'MsgEnd')

%% Mise a jour de coordonnees

Lat = majCoordonnees(Lat, subx, suby);
Lon = majCoordonnees(Lon, subx, suby);

%% Calcul des statistiques

Lat = compute_stats(Lat);
Lon = compute_stats(Lon);

%% Rehaussement de contraste

Lat.TagSynchroContrast = num2str(rand(1));
CLim = [Lat.StatValues.Min Lat.StatValues.Max];
Lat.CLim = CLim;
Lat.ColormapIndex = 3;

Lon.TagSynchroContrast = num2str(rand(1));
CLim = [Lon.StatValues.Min Lon.StatValues.Max];
Lon.CLim = CLim;
Lon.ColormapIndex = 3;

Lat.DataType = cl_image.indDataType('Latitude');
Lon.DataType = cl_image.indDataType('Longitude');

%% Par ici la sortie

a = Lat;
a(2) = Lon;

%% Completion du titre

a(1) = update_Name(a(1));
a(2) = update_Name(a(2));
