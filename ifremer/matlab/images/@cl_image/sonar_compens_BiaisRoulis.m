function [Z, X] = sonar_compens_BiaisRoulis(Z, A, X, BiaisValue)

BiaisValue = double(BiaisValue);
for i=1:Z.nbRows
    z = double(Z.Image(i,:));
    x = double(X.Image(i,:));
    r = sqrt(z .^ 2 + x .^ 2);
    Angles = acosd(-z ./ r) .* sign(x);
    Angles = Angles + BiaisValue;
    Z.Image(i,:) = -r .* cosd(Angles);
    X.Image(i,:) = r .* sind(Angles);
end


