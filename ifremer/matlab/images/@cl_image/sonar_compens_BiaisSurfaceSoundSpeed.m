function [Z, X] = sonar_compens_BiaisSurfaceSoundSpeed(Z, A, X, SurfaceSoundSpeed, BiaisValue, Immersion, Heave)

BiaisValue = double(BiaisValue);
for i=1:Z.nbRows
    z = Z.Image(i,:);
%     z1 = double(z - Immersion(i) - Heave(i));
    z1 = double(z - Heave(i));
    x = double(X.Image(i,:));
    r = sqrt(z1 .^ 2 + x .^ 2);
    
    Rap = -z ./ r;
    sub = find(Rap < 1);
    Angles = acosd(Rap(sub)) .* sign(x(sub));
    
%     Angles = double(A.Image(i,:));
    DeltaTeta = double(tand(Angles) * (BiaisValue / SurfaceSoundSpeed(i))) * (180/pi);
    
%     Z2 = -r(sub) .* cosd(Angles-DeltaTeta) + Immersion(i) + Heave(i);
    Z2 = -r(sub) .* cosd(Angles-DeltaTeta) + Heave(i);
    X2 =  r(sub) .* sind(Angles-DeltaTeta);
    
%     figure(12876); plot(x,z,'b'); grid on; hold on;plot(X2,Z2,'r'); hold off;
    
    Z.Image(i,sub) = Z2;
    X.Image(i,sub) = X2;
end


