% Calcul de la pente lat�rale sur une image de bathy en PingBeam
%
% Syntax
%   [flag, b] = sonar_computeAcrossBeamAngle(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx       : subsampling in X
%   suby       : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image "SonarAcrossBeamAngle"
%
% Examples
%   b = sonar_computeAcrossBeamAngle(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_computeAcrossBeamAngle(this, AcrossDistance, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    flag = 0;
    return
end

ident1 = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'DataType', ident1, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

[XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, AcrossDistance);
if isempty(subx) || isempty(suby)
    flag = 0;
    return
end

%% Calcul de l'image de pente

Roll1  = NaN(length(suby),1);
Roll2  = NaN(length(suby),1);
for i=1:length(suby)
    y = this.Image(suby(i), subx);
    x = AcrossDistance.Image(subyb(i), subxb);
    subNonNaN = ~isnan(x) & ~isnan(y);
    x = x(subNonNaN);
    y = y(subNonNaN);
    
    if isempty(x)
        continue
    end
    
    [a, b, x0, Eqm] = regressionLineairePonderee(x, y, ones(size(x)));
    Roll1(i) = atand(a);
    %     figure(346425); plot(x, y, '*'); hold on; plot(x, a*x+b, 'k'); grid on; hold off;
    
    phase_poly = a * x + b;
    Diff = y - phase_poly;
    Sigma = std(Diff);
    
    subNonOutliers = abs(Diff) <= (1*Sigma);
    x = x(subNonOutliers);
    y = y(subNonOutliers);
    
    [a, b, x0, Eqm] = regressionLineairePonderee(x, y, ones(size(x)));
    Roll2(i) = atand(a);
    %     figure(346425); plot(x, y, '*'); hold on; plot(x, a*x+b, 'k'); grid on; hold off;
    
    Roll2(i) = atand(a);
end
figure; plot(Roll1, 'k'); grid on; hold on; plot(Roll2, 'b'); hold off;

Bathy  = NaN(length(suby), length(subx), 'single');
Across = NaN(length(suby), length(subx), 'single');
for i=1:length(suby)
    y = this.Image(suby(i), subx);
    x = AcrossDistance.Image(subyb(i), subxb);
    r = sqrt(x .^ 2 + y .^2);
    
    Angle = atan2(x, y);
    Angle = Angle + Roll2(i)*(pi/180);
    
    x = r .* sin(Angle);
    y = r .* cos(Angle);
    
    Bathy(i, :)  = y;
    Across(i, :) = x;
end


this(1) = replace_Image(this, Bathy);
this(1) = majCoordonnees(this(1), subx, suby);
this(1) = compute_stats(this(1));
this(1).TagSynchroContrast = num2str(rand(1));
CLim = [this(1).StatValues.Min this(1).StatValues.Max];
this(1).CLim = CLim;
Append = '_AutoRollCompensation_';
this(1) = update_Name(this(1), 'Append', Append);
this(1) = set_SonarRoll(this(1), Roll2);

this(2) = replace_Image(AcrossDistance, Across);
this(2) = majCoordonnees(this(2), subx, suby);
this(2) = compute_stats(this(2));
this(2).TagSynchroContrast = num2str(rand(1));
CLim = [this(2).StatValues.Min this(2).StatValues.Max];
this(2).CLim = CLim;
Append = '_AutoRollCompensation_';
this(2) = update_Name(this(2), 'Append', Append);
this(2) = set_SonarRoll(this(2), Roll2);

flag = 1;
