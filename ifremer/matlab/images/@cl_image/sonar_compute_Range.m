function [flag, R] = sonar_compute_Range(this, indImage, varargin)

[varargin, subx] = getPropertyValue(varargin, 'subx', []);
[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

R = [];

%% V�rification si on a pass� un layer de type Range

[flag, indLayerRange] = findAnyLayerRange(this, indImage, 'AtLeastOneLayer');
if ~flag
    return
end

%% Liste des identifiants de DataType qui peuvent faire l'affaire

identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
identTwoWayTravelTimeInSamples = cl_image.indDataType('TwoWayTravelTimeInSamples');
identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
identRayPathLength             = cl_image.indDataType('RayPathLength'); % One way length in meters

%% Test si on est d�j� en RayPathLength % One way length in meters

if this(indLayerRange).DataType == identRayPathLength
    if isempty(subx) && isempty(suby)
        R = this;
        flag = 1;
        return
    else
        [~, ~, ~, ~, subxRange, subyRange] = intersectionImages(this(indImage), subx, suby, this(indLayerRange));
        R = extraction(this(indLayerRange), 'subx', subxRange, 'suby', subyRange);
        return
    end
end

%% Conversion

if isempty(subx)
    subx = 1:length(this(indImage).x);
end

if isempty(suby)
    suby = 1:length(this(indImage).y);
end

%% Recherche de la zone commune

[~, ~, subx, suby, subxRange, subyRange] = intersectionImages(this(indImage), subx, suby, this(indLayerRange));

%% Calcul du Range

r = this(indLayerRange).Image(subyRange, subxRange);
SampleFrequency = this(indLayerRange).Sonar.SampleFrequency(subyRange,:);
try
    SoundSpeed = this(indLayerRange).Sonar.SurfaceSoundSpeed(subyRange,:);
catch
    SoundSpeed = this(indLayerRange).Sonar.SurfaceSoundSpeed(1,subyRange); % Ajout� le 22/01/2016 pour bug fichier 0007_20100225_195611_Belgica D:\Temp\SPFE\HBRA1005\raw
    SoundSpeed = SoundSpeed';
end

for k=1:length(subyRange)
    switch this(indLayerRange).DataType
        case identTwoWayTravelTimeInSeconds
            r(k,:) = r(k,:) * (SoundSpeed(k,1) / 2);
        case identTwoWayTravelTimeInSamples
            r(k,:) = r(k,:) * (SoundSpeed(k,1) / (2*SampleFrequency(k)));
        case identRayPathSampleNb % TODO : supprimer cet identifiant dans cl_image dans la R2013a
            r(k,:) = r(k,:) * (SoundSpeed(k,1) / (2*SampleFrequency(k)));
    end
end

R = replace_Image(this(indImage), r);
R.ValNaN = NaN;

%% Mise � jour de coordonn�es

R = majCoordonnees(R, subx, suby);

%% Calcul des statistiques

R = compute_stats(R);

%% Rehaussement de contraste

R.TagSynchroContrast = num2str(rand(1));
R.CLim               = [R.StatValues.Min R.StatValues.Max];
R.Unit               = 'm';
R.ColormapIndex      = 3;

%% Compl�tion du titre

R.DataType = identRayPathLength;
R = update_Name(R);

InitialFileName = this(indImage).InitialFileName;
if isempty(InitialFileName)
    R = update_Name(R, 'Append', 'Temps universel');
else
    [~, ImageName] = fileparts(InitialFileName);
    R = update_Name(R, 'Name', ImageName);
end

flag = 1;

