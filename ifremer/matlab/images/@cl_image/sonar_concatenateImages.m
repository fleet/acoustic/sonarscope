function [flag, that] = sonar_concatenateImages(this)

flag = 1;

N = length(this);
for k=1:N
    T = get_SonarDatetime(this(k));
    tDeb(k,1)    = T(1); %#ok<AGROW>
    nbPings(k)   = this(k).nbRows; %#ok<AGROW>
    nbSamples(k) = this(k).nbColumns; %#ok<AGROW>
end

[~,sub] = sort(tDeb);
this      = this(sub);
nbPings   = nbPings(sub);
nbSamples = nbSamples(sub);

nbPingsThat   = sum(nbPings);
nbSamplesThat = max(nbSamples);

that = this(1);
that.Image = NaN(nbPingsThat, nbSamplesThat, class(this(1).Image(1)));

that.Sonar.Height            = NaN(nbPingsThat, 1, 'single');
that.Sonar.Immersion         = NaN(nbPingsThat, 1, 'single');
that.Sonar.Height            = NaN(nbPingsThat, 1, 'single');
that.Sonar.Speed             = NaN(nbPingsThat, 1, 'single');
that.Sonar.Heading           = NaN(nbPingsThat, 1, 'single');
that.Sonar.Roll              = NaN(nbPingsThat, 1, 'single');
that.Sonar.Pitch             = NaN(nbPingsThat, 1, 'single');
that.Sonar.SurfaceSoundSpeed = NaN(nbPingsThat, 1, 'single');
that.Sonar.FishLatitude      = NaN(nbPingsThat, 1);
that.Sonar.FishLongitude     = NaN(nbPingsThat, 1);
that.Sonar.Heave             = NaN(nbPingsThat, 1, 'single');
that.Sonar.Tide              = NaN(nbPingsThat, 1, 'single');
that.Sonar.PingCounter       = NaN(nbPingsThat, 1, 'single');
that.Sonar.SampleFrequency   = NaN(nbPingsThat, 1, 'single');
that.Sonar.SurfaceSoundSpeed = NaN(nbPingsThat, 1, 'single');
TimeThat                     = NaN(nbPingsThat, 1);

offset = [0 cumsum(nbPings)];
for k=1:N
    [that, TimeThat] = appendImage(that, this(k), offset(k), TimeThat);
end

that.Sonar.Time = cl_time('timeMat', TimeThat);

that.x = 1:nbSamplesThat;
that.y = 1:nbPingsThat;

that.XLim = compute_XYLim(that.x);
that.YLim = compute_XYLim(that.y);

that.NuIdentParent = that.NuIdent;
that.NuIdent = floor(rand(1) * 1e8);

% that = compute_stats(that);
% 
% that.TagSynchroContrast = num2str(rand(1));
% CLim = [that.StatValues.Min that.StatValues.Max];
% that.CLim = CLim;

Append = sprintf('Append%dFiles', N);
that = update_Name(that, 'Append', Append);
% that

%{
if testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'), 'noMessage')
    this.Sonar.SampleFrequency = this.Sonar.SampleFrequency / pasx;
end

%}


function [this, TimeThat] = appendImage(this, a, offset, TimeThat)

subx = 1:a.nbColumns;
suby = (1:a.nbRows) + offset;
this.Image(suby,subx) = a.Image(:,:);

this.Sonar.Immersion(suby,:)         = a.Sonar.Immersion(:,:);
this.Sonar.Height(suby,:)            = a.Sonar.Height(:,:);
this.Sonar.Speed(suby,:)             = a.Sonar.Speed(:,:);
this.Sonar.Heading(suby,:)           = a.Sonar.Heading(:,:);
this.Sonar.Roll(suby,:)              = a.Sonar.Roll(:,:);
this.Sonar.Pitch(suby,:)             = a.Sonar.Pitch(:,:);
this.Sonar.SurfaceSoundSpeed(suby,:) = a.Sonar.SurfaceSoundSpeed(:,:);
this.Sonar.FishLatitude(suby,:)      = a.Sonar.FishLatitude(:,:);
this.Sonar.FishLongitude(suby,:)     = a.Sonar.FishLongitude(:,:);
this.Sonar.Heave(suby,:)             = a.Sonar.Heave(:,:);
this.Sonar.Tide(suby,:)              = a.Sonar.Tide(:,:);
this.Sonar.PingCounter(suby,:)       = a.Sonar.PingCounter(:,:);
this.Sonar.SampleFrequency(suby,:)   = a.Sonar.SampleFrequency(:,:);
this.Sonar.SurfaceSoundSpeed(suby,:) = a.Sonar.SurfaceSoundSpeed(:,:);

TimeThat(suby,:)                     = a.Sonar.Time.timeMat;
