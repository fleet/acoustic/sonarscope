% Cr�ation des layers de coordonn�es m�triques
%
% Syntax
%   [flag, a] = sonar_coordXY(a, Lat, Lon, ...)
%
% Input Arguments
%   a : Instance de cl_image
%   a : Instance de cl_image contenant la Latitude
%   a : Instance de cl_image contenant la Longitude
%
% Name-Value Pair Arguments
%   subx : Echantillonage en x
%   suby : Echantillonage en y
%
% Output Arguments
%   flag : 1=ca c'est bien passe, 0 sinon
%   a    : Instances de cl_image contenant les abscisses et les ordonn�es
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, a] = sonar_coordXY(this, Lat, Lon, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% TODO : ATTENTION danger, il faut certainement faire
% PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
PC1 = this.Sonar.PingCounter(suby);
PC2 = Lat.Sonar.PingCounter;
PC3 = Lon.Sonar.PingCounter;
subPC1 = intersect3(PC1, PC2, PC3); % , PC4); % Il faut generaliser intersect3 en intersectn
suby = suby(subPC1);

nbRows = length(suby);
nbColumns = length(subx);

a(1) = replace_Image(this, NaN(nbRows, nbColumns, 'double'));
a(2) = replace_Image(this, NaN(nbRows, nbColumns, 'double'));

pas = 1000;
% hw = create_waitbar('Processing Metric coordinates', 'N', nbRows);
for il=1:pas:nbRows
%     my_waitbar(il, nbRows, hw);
    subl = il:min(il+pas-1, nbRows);
    
    % %     disp('ATTENTION BUG REPERE ICI QUAND ON FAIT UNE VISU DES FAISCEAUX SUR UN ZOOM. OPERATION DE MAINTENANCE A FAIRE IMPERATIVEMENT')
    
    %     [X1, Y1] = latlon2xy(this.Carto, Lat.Image(subLat(subl),subx), Lon.Image(subLon(subl),subx));
    %     a(1).Image(subl,1:nbColumns) = X1;
    %     a(2).Image(subl,1:nbColumns) = Y1;
    
    [X1, Y1] = latlon2xy(this.Carto, Lat.Image(subl,:), Lon.Image(subl,:));
    %     a(1).Image(subl,1:nbColumns) = X1;
    %     a(2).Image(subl,1:nbColumns) = Y1;
    a(1).Image(subl,1:size(X1,2)) = X1;
    a(2).Image(subl,1:size(Y1,2)) = Y1;
end
% my_close(hw, 'MsgEnd')

a(1).DataType = cl_image.indDataType('GeoX');
a(2).DataType = cl_image.indDataType('GeoY');

%% Mise � jour de coordonn�es

a(1) = majCoordonnees(a(1), subx, suby); % ATTENTION : Danger
a(2) = majCoordonnees(a(2), subx, suby);

%% Calcul des statistiques

a(1) = compute_stats(a(1));
a(2) = compute_stats(a(2));

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [a(1).StatValues.Min a(1).StatValues.Max];
a(1).CLim = CLim;
a(1).ColormapIndex = 3;

this.TagSynchroContrast = num2str(rand(1));
CLim = [a(2).StatValues.Min a(2).StatValues.Max];
a(2).CLim = CLim;
a(2).ColormapIndex = 3;

%% Compl�tion du titre

a(1) = update_Name(a(1));
a(2) = update_Name(a(2));

%% The end

flag = 1;
