function a = sonar_copie_info(this, a, suby, yNew)

if (    (this.GeometryType == cl_image.indGeometryType('PingSamples')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingRange')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingAcrossDist')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingTravelTime')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingSnippets')) || ...
        (this.GeometryType == cl_image.indGeometryType('PingBeam'))) ...
        && ...
        ( (a.GeometryType == cl_image.indGeometryType('PingSamples')) || ...
        (a.GeometryType == cl_image.indGeometryType('PingRange')) || ...
        (a.GeometryType == cl_image.indGeometryType('PingAcrossDist')) || ...
        (a.GeometryType == cl_image.indGeometryType('PingTravelTime')) || ...
        (a.GeometryType == cl_image.indGeometryType('PingSnippets')) || ...
        (a.GeometryType == cl_image.indGeometryType('PingBeam')))

    sub = (yNew >= min(this.y(suby))) & (yNew <= max(this.y(suby)));
    suby = suby(sub);

    if isSonarSignal(this, 'Time')
        a.Sonar.Time = this.Sonar.Time(suby);
    end

    if isSonarSignal(this, 'Datetime')
        a.Sonar.Datetime = this.Sonar.Datetime(suby);
    end

    this = maintenance20110126(this);

    if isSonarSignal(this, 'Immersion')
        a.Sonar.Immersion = this.Sonar.Immersion(suby,:);
    end
    if isSonarSignal(this, 'Height')
        a.Sonar.Height = this.Sonar.Height(suby,:);
    end
    if isSonarSignal(this, 'CableOut')
        a.Sonar.CableOut = this.Sonar.CableOut(suby,:);
    end
    if isSonarSignal(this, 'Speed')
        a.Sonar.Speed = this.Sonar.Speed(suby,:);
    end
    if isSonarSignal(this, 'Heading')
        a.Sonar.Heading = this.Sonar.Heading(suby,:);
    end
    if isSonarSignal(this, 'Roll')
        a.Sonar.Roll = this.Sonar.Roll(suby,:);
    end
    if isSonarSignal(this, 'Pitch')
        a.Sonar.Pitch = this.Sonar.Pitch(suby,:);
    end
    if isSonarSignal(this, 'Yaw')
        a.Sonar.Yaw = this.Sonar.Yaw(suby,:);
    end
    if isSonarSignal(this, 'SurfaceSoundSpeed')
        a.Sonar.SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed(suby,:);
    end
    if isSonarSignal(this, 'FishLatitude')
        a.Sonar.FishLatitude = this.Sonar.FishLatitude(suby,:);
    end
    if isSonarSignal(this, 'FishLongitude')
        a.Sonar.FishLongitude = this.Sonar.FishLongitude(suby,:);
    end
    if isSonarSignal(this, 'ShipLatitude')
        a.Sonar.ShipLatitude = this.Sonar.ShipLatitude(suby,:);
    end
    if isSonarSignal(this, 'ShipLongitude')
        a.Sonar.ShipLongitude = this.Sonar.ShipLongitude(suby,:);
    end

    if isSonarSignal(this, 'Heave')
        a.Sonar.Heave = this.Sonar.Heave(suby,:);
    end

    if isSonarSignal(this, 'Tide')
        a.Sonar.Tide = this.Sonar.Tide(suby,:);
    end

    if isSonarSignal(this, 'PingCounter')
        a.Sonar.PingCounter = this.Sonar.PingCounter(suby,:);
    end

    if isSonarSignal(this, 'PortMode_1')
        a.Sonar.PortMode_1 = this.Sonar.PortMode_1(suby,:);
    end

    if isSonarSignal(this, 'PortMode_2')
        a.Sonar.PortMode_2 = this.Sonar.PortMode_2(suby,:);
    end

    if isSonarSignal(this, 'StarMode_1')
        a.Sonar.StarMode_1 = this.Sonar.StarMode_1(suby,:);
    end

    if isSonarSignal(this, 'StarMode_2')
        a.Sonar.StarMode_2 = this.Sonar.StarMode_2(suby,:);
    end

    if isSonarSignal(this, 'Interlacing')
        a.Sonar.Interlacing = this.Sonar.Interlacing(suby,:);
    end

    if isSonarSignal(this, 'PresenceWC')
        a.Sonar.PresenceWC = this.Sonar.PresenceWC(suby,:);
    end

    if isSonarSignal(this, 'PresenceFM')
        a.Sonar.PresenceFM = this.Sonar.PresenceFM(suby,:);
    end

    if isSonarSignal(this, 'DistPings')
        a.Sonar.DistPings = this.Sonar.DistPings(suby,:);
    end

    if isSonarSignal(this, 'NbSwaths')
        a.Sonar.NbSwaths = this.Sonar.NbSwaths(suby,:);
    end

    if isSonarSignal(this, 'TxAcrossAngle')
        a.Sonar.TxAcrossAngle = this.Sonar.TxAcrossAngle(suby,:);
    end

    if isSonarSignal(this, 'ResolAcrossSample') && (this.GeometryType == cl_image.indGeometryType('PingAcrossSample'))
        a.Sonar.ResolAcrossSample = this.Sonar.ResolAcrossSample(suby,:);
    else
        a.Sonar.ResolAcrossSample = [];
    end

    if isSonarSignal(this, 'SampleFrequency')
        if length(this.Sonar.SampleFrequency) == 1 % Maintenant
            this.Sonar.SampleFrequency = [];
        else
            a.Sonar.SampleFrequency = this.Sonar.SampleFrequency(suby,:);
        end
    end
    if isSonarSignal(this, 'SonarFrequency')
        a.Sonar.SonarFrequency = this.Sonar.SonarFrequency(suby,:);
    end

    %% Signaux verticaux

    for kSig=1:length(this.SignalsVert)
%         sigIn  = this.SignalsVert(kSig);
%         sigOut = a.SignalsVert(kSig);
        sigIn  = clone(this.SignalsVert(kSig));
        sigOut = clone(a.SignalsVert(kSig));
        for kx=1:length(sigIn.xSample)
            sigOut.xSample(kx).data = sigIn.xSample(kx).data(suby);
        end
        for ky=1:length(sigIn.ySample)
            sigOut.ySample(ky).data = sigIn.ySample(ky).data(suby);
        end
        a.SignalsVert(kSig) = sigOut;
     end
end

if isfield(a.Sonar, 'SampleBeamData')
    if a.GeometryType == cl_image.indGeometryType('DepthAcrossDist')
        if isfield(a.Sonar.SampleBeamData, 'AcrossDist')
            a.Sonar.SampleBeamData.AcrossDist = a.Sonar.SampleBeamData.AcrossDist;
        end
        if isfield(a.Sonar.SampleBeamData, 'AlongDist')
            a.Sonar.SampleBeamData.AlongDist = a.Sonar.SampleBeamData.AlongDist;
        end
    else
        %         if isfield(a.Sonar.SampleBeamData, 'TxAngle')
        %             a.Sonar.SampleBeamData.TxAngle              = a.Sonar.SampleBeamData.TxAngle(subx);
        %         end
        %         if isfield(a.Sonar.SampleBeamData, 'TransmitSectorNumber') && ~isempty(a.Sonar.SampleBeamData.TransmitSectorNumber)
        %             a.Sonar.SampleBeamData.TransmitSectorNumber = a.Sonar.SampleBeamData.TransmitSectorNumber(subx);
        %         end
        %         if isfield(a.Sonar.SampleBeamData, 'RMax1')
        %             a.Sonar.SampleBeamData.RMax1                = a.Sonar.SampleBeamData.RMax1(subx);
        %         end
        %         if isfield(a.Sonar.SampleBeamData, 'RMax2')
        %             a.Sonar.SampleBeamData.RMax2                = a.Sonar.SampleBeamData.RMax2(subx);
        %         end
        %         if isfield(a.Sonar.SampleBeamData, 'R1SamplesFiltre')
        %             a.Sonar.SampleBeamData.R1SamplesFiltre      = a.Sonar.SampleBeamData.R1SamplesFiltre(subx);
        %         end
        %         if isfield(a.Sonar.SampleBeamData, 'AmpPingNormHorzMax')
        %             a.Sonar.SampleBeamData.AmpPingNormHorzMax   = a.Sonar.SampleBeamData.AmpPingNormHorzMax(subx);
        %         end
        %         if isfield(a.Sonar.SampleBeamData, 'MaskWidth')
        %             a.Sonar.SampleBeamData.MaskWidth            = a.Sonar.SampleBeamData.MaskWidth(subx);
        %         end

        % Test JMA le 30/05/2016 pour fichier Carla 20150520_081511

        if isfield(a.Sonar.SampleBeamData, 'TxAngle')
            a.Sonar.SampleBeamData.TxAngle = a.Sonar.SampleBeamData.TxAngle;
        end
        if isfield(a.Sonar.SampleBeamData, 'TransmitSectorNumber') && ~isempty(a.Sonar.SampleBeamData.TransmitSectorNumber)
            a.Sonar.SampleBeamData.TransmitSectorNumber = a.Sonar.SampleBeamData.TransmitSectorNumber;
        end
        if isfield(a.Sonar.SampleBeamData, 'RMax1')
            a.Sonar.SampleBeamData.RMax1 = a.Sonar.SampleBeamData.RMax1;
        end
        if isfield(a.Sonar.SampleBeamData, 'RMax2')
            a.Sonar.SampleBeamData.RMax2 = a.Sonar.SampleBeamData.RMax2;
        end
        if isfield(a.Sonar.SampleBeamData, 'R1SamplesFiltre')
            a.Sonar.SampleBeamData.R1SamplesFiltre = a.Sonar.SampleBeamData.R1SamplesFiltre;
        end
        if isfield(a.Sonar.SampleBeamData, 'AmpPingNormHorzMax')
            a.Sonar.SampleBeamData.AmpPingNormHorzMax = a.Sonar.SampleBeamData.AmpPingNormHorzMax;
        end
        if isfield(a.Sonar.SampleBeamData, 'MaskWidth')
            a.Sonar.SampleBeamData.MaskWidth = a.Sonar.SampleBeamData.MaskWidth;
        end
    end
end


function this = maintenance20110126(this)

if isSonarSignal(this, 'Immersion')
    this.Sonar.Immersion = miseEnOrdreSignalVertical(this.Sonar.Immersion);
end
if isSonarSignal(this, 'Height')
    this.Sonar.Height = miseEnOrdreSignalVertical(this.Sonar.Height);
end
if isSonarSignal(this, 'CableOut')
    this.Sonar.CableOut = miseEnOrdreSignalVertical(this.Sonar.CableOut);
end
if isSonarSignal(this, 'Speed')
    this.Sonar.Speed = miseEnOrdreSignalVertical(this.Sonar.Speed);
end
if isSonarSignal(this, 'Heading')
    this.Sonar.Heading = miseEnOrdreSignalVertical(this.Sonar.Heading);
end
if isSonarSignal(this, 'Roll')
    this.Sonar.Roll  = miseEnOrdreSignalVertical(this.Sonar.Roll);
end
if isSonarSignal(this, 'Pitch')
    this.Sonar.Pitch = miseEnOrdreSignalVertical(this.Sonar.Pitch);
end
if isSonarSignal(this, 'Yaw')
    this.Sonar.Yaw = miseEnOrdreSignalVertical(this.Sonar.Yaw);
end
if isSonarSignal(this, 'SurfaceSoundSpeed')
    this.Sonar.SurfaceSoundSpeed  = miseEnOrdreSignalVertical(this.Sonar.SurfaceSoundSpeed);
end
if isSonarSignal(this, 'FishLatitude')
    this.Sonar.FishLatitude = miseEnOrdreSignalVertical(this.Sonar.FishLatitude);
end
if isSonarSignal(this, 'FishLongitude')
    this.Sonar.FishLongitude = miseEnOrdreSignalVertical(this.Sonar.FishLongitude);
end
if isSonarSignal(this, 'ShipLatitude')
    this.Sonar.ShipLatitude = miseEnOrdreSignalVertical(this.Sonar.ShipLatitude);
end
if isSonarSignal(this, 'ShipLongitude')
    this.Sonar.ShipLongitude = miseEnOrdreSignalVertical(this.Sonar.ShipLongitude);
end

if isSonarSignal(this, 'Heave')
    this.Sonar.Heave = miseEnOrdreSignalVertical(this.Sonar.Heave);
end

if isSonarSignal(this, 'Tide')
    this.Sonar.Tide = miseEnOrdreSignalVertical(this.Sonar.Tide);
end

if isSonarSignal(this, 'PingCounter')
    this.Sonar.PingCounter = miseEnOrdreSignalVertical(this.Sonar.PingCounter);
end

if isSonarSignal(this, 'PortMode_1')
    this.Sonar.PortMode_1  = miseEnOrdreSignalVertical(this.Sonar.PortMode_1);
end

if isSonarSignal(this, 'PortMode_2')
    this.Sonar.PortMode_2 = miseEnOrdreSignalVertical(this.Sonar.PortMode_2);
end

if isSonarSignal(this, 'StarMode_1')
    this.Sonar.StarMode_1 = miseEnOrdreSignalVertical(this.Sonar.StarMode_1);
end

if isSonarSignal(this, 'StarMode_2')
    this.Sonar.StarMode_2 = miseEnOrdreSignalVertical(this.Sonar.StarMode_2);
end

if isSonarSignal(this, 'Interlacing')
    this.Sonar.Interlacing = miseEnOrdreSignalVertical(this.Sonar.Interlacing);
end

if isSonarSignal(this, 'PresenceWC')
    this.Sonar.PresenceWC = miseEnOrdreSignalVertical(this.Sonar.PresenceWC);
end

if isSonarSignal(this, 'PresenceFM')
    this.Sonar.PresenceFM = miseEnOrdreSignalVertical(this.Sonar.PresenceFM);
end

if isSonarSignal(this, 'DistPings')
    this.Sonar.DistPings = miseEnOrdreSignalVertical(this.Sonar.DistPings);
end

if isSonarSignal(this, 'NbSwaths')
    this.Sonar.NbSwaths = miseEnOrdreSignalVertical(this.Sonar.NbSwaths);
end

if isSonarSignal(this, 'ResolAcrossSample') % && (this.GeometryType == cl_image.indGeometryType('PingAcrossSample'))
    this.Sonar.ResolAcrossSample = miseEnOrdreSignalVertical(this.Sonar.ResolAcrossSample);
end

if isSonarSignal(this, 'SampleFrequency')
    this.Sonar.SampleFrequency = miseEnOrdreSignalVertical(this.Sonar.SampleFrequency);
end
if isSonarSignal(this, 'SonarFrequency')
    this.Sonar.SonarFrequency = miseEnOrdreSignalVertical(this.Sonar.SonarFrequency);
end


function X = miseEnOrdreSignalVertical(X)

if size(X,1) == 1
    X = X(:,:)';
end

if (size(X,1) == 2) && (size(X,2) > 2)
    X = X(:,:)';
end
