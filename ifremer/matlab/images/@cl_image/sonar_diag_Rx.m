% Calcul de di diagramme de directivite en emission
%
% Syntax
%   b = sonar_diag_Rx(a, CasDepth, H, CasDistance, Bathy, Emission, ReceptionBeam, ...)
%
% Input Arguments
%   a             : Instance de cl_image
%   CasDepth      : 1=Il existe un layer 'Depth', 2=Il existe une hauteur sonar, 3=On possede l'angle d'emission, 4=On fixe une hauteur arbitraire
%   H             : [] ou Hauteur sonar � donner
%   CasDistance   : 1=Cas d'une donnee sonar en distance oblique, 2=Cas d'une donnee sonar en distance projetee, 3=On possede l'angle d'emission
%   Bathy         : [] ou instance de cl_image contenant la bathymetrie
%   Emission      : [] ou instance de cl_image contenant l'angle d'emission
%   ReceptionBeam : [] ou instance de cl_image contenant le numero du faisceau
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', [9 1 4], 'Carto', Carto);
%
%   Reflectivite = get_Image(b, 1);
%   imagesc(Reflectivite)
%   Bathy = get_Image(b, 2);
%   imagesc(Bathy)
%   Emission = get_Image(b, 3);
%   imagesc(Emission)
%
%   [DiagRx, ReceptionBeamEstime] = sonar_diag_Rx(Bathy, 1, [], 3, Bathy, Emission, []);
%   imagesc(DiagRx)
%   imagesc(ReceptionBeamEstime)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, R] = sonar_diag_Rx(this, CasDepth, H, CasDistance, Bathy, Emission, ReceptionBeam, varargin)

nbImages = length(this);
for i=1:nbImages
    [this(i), R(i)] = unitaire_sonar_diag_Rx(this(i), CasDepth, H, CasDistance, Bathy, Emission, ReceptionBeam, varargin{:}); %#ok
end


function [this, R] = unitaire_sonar_diag_Rx(this, CasDepth, H, CasDistance, Bathy, Emission, ReceptionBeam, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, OrigineModeles]             = getPropertyValue(varargin, 'OrigineModeles', []);
[varargin, TypeDiagramme]              = getPropertyValue(varargin, 'TypeDiagramme', 1);
[varargin, noLayerReceptionBeamEstime] = getFlag(varargin, 'noLayerReceptionBeam'); %#ok<ASGLU>

if isempty(OrigineModeles)
    OrigineModeles = 1;
end

%% Angle de r�ception

flagRollSonarLateral = 0;
if isempty(Emission)
    
    % ----------------------
    % Information de hauteur
    
    if CasDistance == 1
        H = abs(this.Sonar.Height(suby));
        H = repmat(H(:), 1, length(subx));
        if isSonarSignal(this, 'Roll')
            flagRollSonarLateral = 1;
            Roll = this.Sonar.Roll(suby);
        end
    else
        switch CasDepth
            case 1  % Il existe un layer 'Depth'
                H = abs(Bathy(1).Image(suby, subx));
            case 2  % Il existe une hauteur sonar
                H = this.Sonar.Height(suby);
                H = repmat(abs(H(:)), 1, length(subx));
                if isSonarSignal(this, 'Roll')
                    flagRollSonarLateral = 1;
                    Roll = this.Sonar.Roll(suby);
                end
            case 3  % On possede l'angle d'emission
                disp('Ici ca va planter car AngleEmission n''est pas defini. Ce cas devrait etre commente tout simplement')
                H = repmat(abs(this.x(subx)), length(suby), 1) ./ tan(Emission * (pi/180)); % On possede l'angle d'emission
            case 4  % On fixe une hauteur arbitraire
            otherwise
                my_warndlg( 'Pas d''information de hauteur', 1);
                this = [];
                return
        end
    end
    
    % -----------------------
    % Information de distance
    
    switch CasDistance
        case 1  % Cas d'une donnee sonar en distance oblique
            D = repmat(abs(this.x(subx)), length(suby), 1);
            sub = find(this.x(subx) < 0);
            D(:,sub) = -D(:,sub);
        case 2  % Cas d'une donnee sonar en distance projetee
            D = sqrt(repmat(this.x(subx), length(suby), 1) .^ 2 + H .^ 2);
            sub = find(this.x(subx) < 0);
            D(:,sub) = -D(:,sub);
        case 3  % On possede l'angle de reception
            D = H ./ cos(Emission);
        otherwise
            my_warndlg('Pas d''information de distance', 1);
            this = [];
            return
    end
    
    % ----------------
    % Angle de reception
    
    D(D == 0) = NaN;
    D(abs(D) < H) = NaN;
    AngleEmission = acos(H ./ D) * (180 / pi);
    
    subAngle = find(AngleEmission > 90);
    AngleEmission(subAngle) = AngleEmission(subAngle) - 180;
    
else
    % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
    % flipud (voir cl_image/plus
    
    [XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Emission(1)); %#ok<ASGLU>
    if isempty(subx) || isempty(suby)
        str1 = 'Pas d''intersection commune entre les images.';
        str2 = 'No intersection between the 2 images.';
        my_warndlg(Lang(str1,str2), 1);
        this = [];
        return
    end
    AngleEmission = Emission(1).Image(subyb, subxb);
end
subNan = isnan(this.Image(suby, subx));
AngleEmission(subNan) = NaN;

%% Dans le cas d'un sonar lateral ou prend en compte le roulis

if flagRollSonarLateral
    AngleEmission = AngleEmission + repmat(Roll(:), 1, size(AngleEmission, 2));
end

%% Calcul du diagramme de reception

if isempty(this.Sonar.PortMode_1) % Pour corriger bug EdgeTech
    this.Sonar.PortMode_1 = ones(length(this.Sonar.Height), 1);
end

Sonar = get(this, 'SonarDescription');
[RX, ReceptionBeamEstime] = diagramme_Rx(Sonar, AngleEmission, 'RxBeamIndex', ReceptionBeam, ...
    'OrigineModeles', OrigineModeles, 'TypeDiagramme', TypeDiagramme, ...
    'Mode', this.Sonar.PortMode_1(suby));

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)
this = replace_Image(this, RX);

this.ValNaN = NaN;

%% Mise � jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'm';
this.ColormapIndex = 3;

% -------------------
% Completion du titre

if OrigineModeles == 1
    strOrigine = 'Constructeur';
else
    strOrigine = 'Ifremer';
end

if TypeDiagramme == 1
    strTypeDiagramme = 'Analytique';
else
    strTypeDiagramme = 'Table';
end

this.DataType = cl_image.indDataType('RxBeamPattern');
Append = ['_DiagRec ' strOrigine ' ' strTypeDiagramme '_'];
this = update_Name(this, 'Append', Append);






R = this;
if ~isempty(ReceptionBeamEstime) && ~noLayerReceptionBeamEstime
    R = replace_Image(R, ReceptionBeamEstime);
    
    % -----------------------
    % Calcul des statistiques
    
    R = compute_stats(R);
    
    % -------------------------
    % Rehaussement de contraste
    
    R.TagSynchroContrast = num2str(rand(1));
    CLim = [R.StatValues.Min R.StatValues.Max];
    R.CLim = CLim;
    R.ColormapIndex = 3;
    
    % -------------------
    % Completion du titre
    
    R.DataType = cl_image.indDataType('RxBeamIndex');
    Append = 'ReceptionBeam Estime';
    R = update_Name(R, 'Append', Append);
end
