% Calcul de di diagramme de directivite en emission
%
% Syntax
%   b = sonar_diag_Rx_Layer(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', [9 1 4], 'Carto', Carto);
%
%   Reflectivite = get_Image(b, 1);
%   imagesc(Reflectivite)
%   Bathy = get_Image(b, 2);
%   imagesc(Bathy)
%   Emission = get_Image(b, 3);
%   imagesc(Emission)
%
%   [DiagRx, ReceptionBeamEstime] = sonar_diag_Rx(Bathy, 1, [], 3, Bathy, Emission, []);
%   [DiagRx] = sonar_diag_Rx_Layer(Bathy, ReceptionBeamEstime);
%   imagesc(DiagRx)
%   imagesc(ReceptionBeamEstime)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_diag_Rx_Layer(this, RxBeamAngle, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_sonar_diag_Rx_Layer(this(i), RxBeamAngle, varargin{:});
end


function this = unitaire_sonar_diag_Rx_Layer(this, RxBeamAngle, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

% [varargin, OrigineModeles] = getPropertyValue(varargin, 'OrigineModeles', 1);

%% Calcul du diagramme de reception

Sonar = get(this, 'SonarDescription');

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)
this = replace_Image(this, diagramme_Rx_Layer(Sonar, this.Image(suby, subx), RxBeamAngle.Image(suby, subx)));
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'm';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('RxBeamPattern');
Append = 'DiagRec Layer RxBeamAngle';
this = update_Name(this, 'Append', Append);


