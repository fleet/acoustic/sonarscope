% Calcul de di diagramme de directivite en emission
%
% Syntax
%   b = sonar_diag_Tx(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%   b = view_depth(aKM, 'ListeLayers', [9 1 4], 'Carto', Carto);
%
%   Reflectivite = get_Image(b, 1);
%   imagesc(Reflectivite)
%   Bathy = get_Image(b, 2);
%   imagesc(Bathy)
%   Emission = get_Image(b, 3);
%   imagesc(Emission)
%
%   DiagTx = sonar_diag_Tx(Bathy, 1, [], 3, Bathy, Emission, []);
%   imagesc(DiagTx)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = sonar_diag_Tx(this, CasDepth, H, CasDistance, Bathy, Emission, EmissionBeam, varargin)

N = length(this);
str1 = 'Calcul des images des TxDiag';
str2 = 'Computing images of TxDiag';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [this(k), flag] = unitaire_sonar_diag_Tx(this(k), CasDepth, H, CasDistance, Bathy, Emission(k), EmissionBeam, varargin{:});
    if ~flag
        this = [];
    end
end
my_close(hw, 'MsgEnd')


function [this, flag] = unitaire_sonar_diag_Tx(this, CasDepth, H, CasDistance, Bathy, Emission, EmissionBeam, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, OrigineModeles] = getPropertyValue(varargin, 'OrigineModeles', 1);
[varargin, TypeDiagramme]  = getPropertyValue(varargin, 'TypeDiagramme',  1);
[varargin, listeSuby]      = getPropertyValue(varargin, 'listeSuby',      []); %#ok<ASGLU>

%% Angle d'�mission

flagRollSonarLateral = 0;
if isempty(Emission)
    
    % ----------------------
    % Information de hauteur
    
    if CasDistance == 1
        H = abs(this.Sonar.Height(suby));
        H = repmat(H(:), 1, length(subx));
        
        if isSonarSignal(this, 'Roll')
            flagRollSonarLateral = 1;
            Roll = this.Sonar.Roll(suby);
        end
    else
        switch CasDepth
            case 1  % Il existe un layer 'Depth'
                H = abs(Bathy(1).Image(suby, subx));
            case 2  % Il existe une hauteur sonar
                H = this.Sonar.Height(suby);
                H = repmat(abs(H(:)), 1, length(subx));
                if isSonarSignal(this, 'Roll')
                    flagRollSonarLateral = 1;
                    Roll = this.Sonar.Roll(suby);
                end
            case 3  % On possede l'angle d'emission
                % TODO : en principe on ne devrait jamais passer par ici non ?
                disp('Ici ca va planter car AngleEmission n''est pas defini. Ce cas devrait �tre comment� tout simplement')
                H = repmat(abs(this.x(subx)), length(suby), 1) ./ tand(AngleEmission); % On possede l'angle d'emission
            case 4  % On fixe une hauteur arbitraire
            otherwise
                my_warndlg('Pas d''information de hauteur', 1);
                flag = 0;
                return
        end
    end
    
    %% Information de distance
    
    switch CasDistance
        case 1  % Cas d'une donnee sonar en distance oblique
            D = repmat(abs(this.x(subx)), length(suby), 1);
            sub = find(this.x(subx) < 0);
            D(:,sub) = -D(:,sub);
        case 2  % Cas d'une donnee sonar en distance projetee
            D = sqrt(repmat(this.x(subx), length(suby), 1) .^ 2 + H .^ 2);
            sub = find(this.x(subx) < 0);
            D(:,sub) = -D(:,sub);
        case 3  % On possede l'angle d'emission
            D = H ./ cos(AngleEmission);
        otherwise
            my_warndlg('Pas d''information de distance', 1); % On ne devrait jamais passer par ici en principe
            flag = 0;
            return
    end
    
    %% Angle d'emission
    
    D(D == 0) = NaN;
    D(abs(D) < H) = NaN;
    AngleEmission = acos(H ./ D) * (180 / pi);
    
    subAngle = find(AngleEmission > 90);
    AngleEmission(subAngle) = AngleEmission(subAngle) - 180;
    
else
    if isempty(EmissionBeam)
        % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
        % flipud (voir cl_image/plus
        
        [~, ~, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Emission);
    else
        % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
        % flipud (voir cl_image/plus
        
        [~, ~, subx, suby, subxb, subyb, subxc, subyc] = intersectionImages(this, subx, suby, Emission, EmissionBeam);
    end
    if isempty(subx) || isempty(suby)
        str1 = 'Pas d''intersection commune entre les images.';
        str2 = 'No intersection between the images.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    AngleEmission = Emission.Image(subyb, subxb);
    
    if Emission.DataType == cl_image.indDataType('BeamPointingAngle')
        Roll = 0;
    else
        if isempty(Emission.Sonar.Roll)
            Roll = 0;
        else
            Roll = Emission.Sonar.Roll(subyb);
        end
    end
end
for k=1:length(suby)
    subNan = isnan(this.Image(suby(k), subx));
    AngleEmission(k,subNan) = NaN;
end

%% Dans le cas d'un sonar lateral ou prend en compte le roulis

if flagRollSonarLateral || strcmp(get(this.Sonar.Desciption, 'Sonar.Name'), 'ME70')
    AngleEmission = AngleEmission + repmat(Roll(:), 1, size(AngleEmission, 2));
end

%% Calcul du diagramme d'emission


if ~isempty(EmissionBeam)
    %     disp('Message de JMA pour JMA : cl_sounder/diagramme_Tx : +1 ici ?????')
    EmissionBeam = EmissionBeam.Image(subyc, subxc);% + 1;
    
    % Verrue pour corriger un bug : A traiter serieusement
    subx = subx(1:length(subxc));
end

if isempty(this.Sonar.PortMode_1) % Pour corriger bug EdgeTech
    this.Sonar.PortMode_1 = ones(length(this.Sonar.Height), 1);
end
T =  this.Sonar.Time.timeMat;

%% Inventaire des configurations du sondeurs dans l'image

[flag, listeConf, listeConfWithNbPings, N, listeModesConf, listeSwathConf, listeFMConf, listeMode2EM2040, ...
    listeSubyHere, listeSubyInd] = sonar_listConfigs(this, suby);
if ~flag
    flag = 1;
    return
end

if isempty(listeSuby)
    listeSuby = listeSubyHere;
end

%% Boucle sur toutes les configurations du sondeur

TX = zeros(size(AngleEmission), 'single');
for k1=1:length(listeSubyHere)
    for k2=1:length(listeSuby)
        [subyConf, ia] = intersect(listeSubyHere{k1}, listeSuby{k2});
        if isempty(subyConf)
            continue
        end
        subyConfInd = listeSubyInd{k1}(ia);
        
        [flag, this] = SonarDescriptionAccordingToModes(this, 'suby', subyConf, 'Mute', 1);
        if ~flag
            return
        end
        Sonar = get(this, 'SonarDescription');
        
        %     [X, flag] = diagramme_Tx(Sonar, AngleEmission(subyConf,:), 'TxBeamIndex', EmissionBeam(subyConf,:), ...
        [X, flag] = diagramme_Tx(Sonar, AngleEmission(subyConfInd,:), 'TxBeamIndex', EmissionBeam(subyConfInd,:), ...
            'OrigineModeles', OrigineModeles, 'TypeDiagramme', TypeDiagramme, ...
            'Time', T(subyConf(1)));
        if flag
            TX(subyConfInd,:) = X;
        end
    end
end

%%


% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)
this = replace_Image(this, TX);

this.ValNaN = NaN;

%% Mise � jour des coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'dB';
this.ColormapIndex = 3;

%% Compl�tion du titre

if OrigineModeles == 1
    strOrigine = 'Constructeur';
else
    strOrigine = 'Ifremer';
end

if TypeDiagramme == 1
    strTypeDiagramme = 'Analytique';
else
    strTypeDiagramme = 'Table';
end

this.DataType = cl_image.indDataType('TxBeamPattern');
Append = [strOrigine ' ' strTypeDiagramme];
this = update_Name(this, 'Append', Append);
