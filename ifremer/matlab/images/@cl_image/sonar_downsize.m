% Filtrage wiener d'une image
%
% Syntax
%   b = sonar_downsize(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   pasx : Facteur de reduction en x (2 par defaut)
%   pasy : Facteur de reduction en y (2 par defaut)
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   nomFic = getNomFicDatabase('DF1000_ExStr.imo');
%   a = cl_car_sonar('nomFic', nomFic);
%   b = view(a, 'Sonar.Ident', 8, 'subl', 1:1000);
%   s = get_Image(b, 1);
%   imagesc(s)
%
%   c = sonar_downsize(s, 'pasy', 1, 'pasx', 16);
%   imagesc(c)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, N1, N2] = sonar_downsize(this, varargin)

nbImages = length(this);

for k=1:nbImages
    if this(k).DataType == cl_image.indDataType('AveragedPtsNb')
        [a, b, c] = unitaire_sonar_downsize(this(k), varargin{:});
        this(k) = a;
        N1(k)   = b; %#ok
        N2(k)   = c; %#ok
    else
        this(k) = unitaire_sonar_downsize(this(k), varargin{:});
        a = cl_image_I0;
        N1(k) = a; %#ok
        N2(k) = a; %#ok
    end
end


function [this, N1, N2] = unitaire_sonar_downsize(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, pasx] = getPropertyValue(varargin, 'pasx', 2);
[varargin, pasy] = getPropertyValue(varargin, 'pasy', 1);
[varargin, N1]   = getPropertyValue(varargin, 'N1',   []);
[varargin, N2]   = getPropertyValue(varargin, 'N2',   []); %#ok<ASGLU>

%% Contr�les

if ~((this.GeometryType == cl_image.indGeometryType('PingBeam')) ...
        || (this.GeometryType == cl_image.indGeometryType('PingSamples')) ...
        || (this.GeometryType == cl_image.indGeometryType('PingAcrossDist')))
    str = sprintf('Operation possible uniquement si GeometryType egal a 4 ou 5');
    my_warndlg(str, 1);
    
    this = [];
    N1 = [];
    N2 = [];
    return
end

%% Cas g�n�ral

Method = 'mean';

%% Cas particuliers

if isIntegerVal(this)
    Method = 'nearest';
end

if strcmp(cl_image.strDataType{this.DataType}, 'AveragedPtsNb')
    Method = 'sum';
end

switch cl_image.strDataType{this.DataType}
    case 'AveragedPtsNb'
        Method = 'sum';
    case 'RegionOfInterest'
        Method = 'nearest';
end

%% Appel de la fonction de filtrage

I = this.Image(suby,subx);
if this.DataType == cl_image.indDataType('AveragedPtsNb')
    I(I <= 0) = NaN;
    N1 = I;
else
    if strcmp(Method, 'mean') && ~isempty(N1)
        I = I .* N1;
        Method = 'sum';
    end
end

I = downsize(I, 'pasy', pasy, 'pasx', pasx, 'Method', Method);

if this.DataType == cl_image.indDataType('AveragedPtsNb')
    N2 = I;
else
    if strcmp(Method, 'sum') && ~isempty(N2)
        I = I * (pasy*pasx) ./ N2;
    end
end


% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)
this = replace_Image(this, I);
clear I

%% Mise � jour de coordonn�es

subx = linspace(subx(1), subx(end), size(this.Image, 2));
suby = linspace(suby(1), suby(end), size(this.Image, 1));
this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

Append = ['Downsize' num2strCode([pasy pasx])];
this = update_Name(this, 'Append', Append);
