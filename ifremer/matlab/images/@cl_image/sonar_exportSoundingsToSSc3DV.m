% Exports soundings in a XML file for GLOBE.
%
% Syntax
%   flag = sonar_exportSoundingsToSSc3DV(this, indImage, nomFic, Name, ...)
%
% Input Arguments
%   a        : Layers in PingBeam geometry
%   indImage : Index of the current image
%   nomFic   : File name of the output .xml file
%   Name     : Nom
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%   TODO : prendre exemple avec donn�es de SonarScopeData
%   a = ....
%   flag = sonar_exportSoundingsToSSc3DV(a, indImage, nomFic, Name, 'subx', subx, 'suby', suby);
%
% See also WC_plotEchoes WC_SaveEchoes Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function flag = sonar_exportSoundingsToSSc3DV(this, indImage, nomFic, Name, varargin)

Z = this(indImage);

[subx, suby, varargin] = getSubxSuby(Z, varargin); %#ok<ASGLU>

%% V�rification d'existence de layers de latitude et longitude

identLayerLat = cl_image.indDataType('Latitude');
indLayerLat   = findIndLayerSonar(this, indImage, 'DataType', identLayerLat, 'OnlyOneLayer');

identLayerLon = cl_image.indDataType('Longitude');
indLayerLon   = findIndLayerSonar(this, indImage, 'DataType', identLayerLon, 'OnlyOneLayer');

if isempty(indLayerLat) || isempty(indLayerLon)
    [flag, a] = sonarCalculCoordGeo(this, indImage, 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
    Lat = a(1);
    Lon = a(2);
    clear a
else
    % Extraction des images
    
    x = get(this(indImage), 'x');
    y = get(this(indImage), 'y');
    x = x(subx);
    y = y(suby);
    Lat = extraction(this(indLayerLat), 'x', x, 'y', y);
    Lon = extraction(this(indLayerLon), 'x', x, 'y', y);
end

% Carto = Z.Carto;
% [XCoor, YCoor] = latlon2xy(Carto, Lat.Image, Lon.Image);

% identReflectivity = cl_image.indDataType('Reflectivity');
% indReflectivity   = findIndLayerSonar(this, indImage, 'DataType', identReflectivity, 'WithCurrentImage', 1, 'OnlyOneLayer');

indAcrossDist = cl_image.indDataType('AcrossDist');
indAcrossDist = findIndLayerSonar(this, indImage, 'DataType', indAcrossDist, 'WithCurrentImage', 1, 'OnlyOneLayer');

[~, indRange] = findAnyLayerRange(this, indImage);
% TODO : s'assurer que la fonction de calcul g�re bien tous les types de
% range

identAngle = cl_image.indDataType('BeamPointingAngle');
indAngles  = findIndLayerSonar(this, indImage, 'DataType', identAngle, 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indAngles)
    identAngle = cl_image.indDataType('TxAngle');
    indAngles  = findIndLayerSonar(this, indImage, 'DataType', identAngle, 'WithCurrentImage', 1, 'OnlyOneLayer');
end
if isempty(indAngles)
    identAngle = cl_image.indDataType('RxBeamAngle');
    indAngles  = findIndLayerSonar(this, indImage, 'DataType', identAngle, 'WithCurrentImage', 1, 'OnlyOneLayer');
end

Across = this(indAcrossDist);
Range  = this(indRange);
Angles = this(indAngles);
% Reflectivity = this(indReflectivity);

T = Z.Sonar.Time;
Date = T.date;
Hour = T.heure;
nbCol = Lon.nbColumns;

% Donn�es utiles
% Points.BaseForm       = 'cube';
Points.iPing     = repmat(Z.Sonar.PingCounter(suby), 1, nbCol);
Points.Latitude  = Lat.Image;
Points.Longitude = Lon.Image;
Points.Z         = Z.Image(suby,subx);
Points.Date      = repmat(Date(suby), 1, nbCol);
Points.Hour      = repmat(Hour(suby), 1, nbCol);

% Donn�es inutiles � supprimer quand nouveau format sera d�fini
% Points.Energie          = Reflectivity.Image(suby,subx);
% Points.EnergieTotale    = repmat(nanmean(Points.Energie, 2), 1, nbCol);
% Points.XCoor            = XCoor;
% Points.YCoor            = YCoor;
Points.Angle            = Angles.Image(suby,subx);
Points.RangeInSamples   = Range.Image(suby,subx);
Points.AcrossDistance   = Across.Image(suby,subx);
% Points.Celerite         = [];

Points.iPing            = Points.iPing(:);
Points.Latitude         = Points.Latitude(:);
Points.Longitude        = Points.Longitude(:);
Points.Z                = Points.Z(:);
Points.Date             = Points.Date(:);
Points.Hour             = Points.Hour(:);
% Points.Energie          = Points.Energie(:);
% Points.EnergieTotale    = Points.EnergieTotale(:);
% Points.XCoor            = Points.XCoor(:);
% Points.YCoor            = Points.YCoor(:);
Points.Angle            = Points.Angle(:);
Points.RangeInSamples   = Points.RangeInSamples(:);
Points.AcrossDistance   = Points.AcrossDistance(:);
Survey.Name             = 'Unknown';
Survey.Vessel           = 'Unknown';
Survey.Sounder          = 'Unknown';
Survey.ChiefScientist   = 'Unknown';

%% Affichage des �chos

% WC_plotEchoes(Points, 'Name', nomFic)

%% Cr�ation du fichier des �chos

nomFicAll = this(indImage).InitialFileName;
flag = WC_SaveEchoes(nomFic, Points, 'InitialFileName', nomFicAll, 'LayerName3DV', Name, 'Survey', Survey);

my_warndlg(Lang('L''export est termin�', 'The export is over'), 0);
