% Calcul des coordonn�es de points pour export vers VRML.
%
% Syntax
%   [flag, Points] = sonar_exportSoundingsToVRML(a, indImage, subx, suby)
%
% Input Arguments
%   a           : instances de cl_image
%   indImage    : indice de l'image
%   subx        : vector de coordonn�es en X.
%   suby        : vector de coordonn�es en Y.
%
% See also sonar_exportSoundingsToSSc3DV Authors
% Authors : JMA, GLU
% ----------------------------------------------------------------------------

function [flag, Points] = sonar_exportSoundingsToVRML(this, indImage, subx, suby)

flag = 0; %#ok<NASGU>

%% V�rification d'existence d'un layer de latitude

identLayerLat = cl_image.indDataType('Latitude');
[indLayerLat, nomLayersLatitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLat);
if ~isempty(indLayerLat)
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
    if length(indLayerLat) > 1
        str1 = 'Il existe plusieurs this de type "Latitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Latitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLatitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLat = indLayerLat(rep);
    end
end

%% V�rification d'existence d'un layer de longitude

identLayerLon = cl_image.indDataType('Longitude');
[indLayerLon, nomLayersLongitude] = findIndLayerSonar(this, indImage, 'DataType', identLayerLon);
if ~isempty(indLayerLon)
% OPERATION DE MAINTENANCE A FAIRE ICI 
%     [flag, indLayerBathymetry,  nomLayersBathymetry]  = findOneLayerDataType(this, indImage, identBathymetry);
%     if ~flag
%         return
%     end
    if length(indLayerLon) > 1
        str1 = 'Il existe plusieurs this de type "Longitude", la quelle voulez-vous utiliser ?';
        str2 = 'Many "Longitude" layers exist. Which one do you want to use ?';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomLayersLongitude, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayerLon = indLayerLon(rep);
    end
end

if isempty(indLayerLat) || isempty(indLayerLon)
    [flag, a] = sonarCalculCoordGeo(this, indImage, 'subx', subx, 'suby', suby);
    if ~flag
        return
    end
    Lat = a(1);
    Lon = a(2);
    clear a
else
    x = get(this(indImage), 'x');
    y = get(this(indImage), 'y');
    x = x(subx);
    y = y(suby);
    Lat = extraction(this(indLayerLat), 'x', x, 'y', y);
    Lon = extraction(this(indLayerLon), 'x', x, 'y', y);
end
this   = this(indImage);
Carto = this.Carto;
[XCoor, YCoor] = latlon2xy(Carto, Lat.Image, Lon.Image);

% Donn�es utiles
Points.Latitude  = Lat.Image;
Points.Longitude = Lon.Image;
Points.Z              = this.Image(suby,subx);

% Donn�es inutiles � supprimer quand nouveau format sera d�fini
Points.XCoor     = XCoor;
Points.YCoor     = YCoor;

Points.Latitude  = Points.Latitude(:);
Points.Longitude = Points.Longitude(:);
Points.Z         = Points.Z(:);
Points.XCoor     = XCoor(:);
Points.YCoor     = YCoor(:);

flag = 1;

