% Exportation de la navigation dans un fichier Caribes(R) ".nvi"
%
% Syntax
%   flag = sonar_export_nvi(a, nomFic)
%
% Input Arguments
%   a      : Instance de cl_image
%   nomFic : Nom du fichier .nvi de sortie
%
% Name-Value Pair Arguments
%   mbShip      : Nom du navire
%   mbSurvey    : Nom de la mission
%   mbHistAutor : Nom de l'auteur
%   mbName      :
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%   nomFicAll  = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all');
%   ou
%   nomFicAll  = getNomFicDatabase('EM300_0005_20000609_121123_raw.all');
%   aKM = cl_simrad_all('nomFic', nomFicAll);
%   c = import_PingBeam_All_V1(aKM, 'ListeLayers', 1);
%
%   nomFicNvi = my_tempname('.nvi')
%   sonar_export_nvi(c, nomFicNvi)
%
%   d = cl_car_nav_data(nomFicNvi);
%   plot(d);
%
% See also cl_netcdf Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function sonar_export_nvi(this, nomFicOut, varargin)

% Remarks : Cette fonction aurait pu �tre �crite plus simplement en
% appelant get_value et set_value qui font la conversion des valeurs
% directement avec l'hypoth�se de pr�sence des propri�t�s : scale_factor,
% add_offet et missing_value. Dans ce cas, la fonction cdf_Caraibes_valUnit2valFic n'aurait
% pas �t� n�cessaire

% Format d'un fichier .nvi :
% Attributs globaux du format SMF g�n�rique
% Attributs globaux du format SMF g�or�f�renc�
% Attributs globaux du format SMF de navigation
% Dimensions du format SMF g�n�rique
% Dimensions du format SMF de navigation
% Variables du format SMF g�n�rique
% Variables du format SMF de navigation

% Attributs globaux du format SMF de navigation

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, mbShip]   = getPropertyValue(varargin, 'mbShip', 'BlackPearl');
[varargin, mbSurvey] = getPropertyValue(varargin, 'mbSurvey', 'Caraibes');
% [varargin, mbHistAutor] = getPropertyValue(varargin, 'mbHistAutor', 'Gore');
[varargin, mbName]   = getPropertyValue(varargin, 'mbName', 'Johnny'); %#ok<ASGLU>

%% Lecture du skelette Modele

nomFicRef = getNomFicDatabase('EM300_Ex1.nvi');
a = cl_netcdf('fileName', nomFicRef);
sk_ref = get(a, 'skeleton');

%% Lecture des variables dans l'instance image

Date  = this.Sonar.Time.date;
Heure = this.Sonar.Time.heure;
Date  = Date(suby);
Heure = Heure(suby);

% Attributs globaux du format SMF g�or�f�renc�
mbStartDate = this.Sonar.Time(suby(1)).date;
mbEndDate   = this.Sonar.Time(suby(end)).date;
mbStartTime = this.Sonar.Time(suby(1)).heure;
mbEndTime   = this.Sonar.Time(suby(end)).heure;

% Variables du format SMF de navigation
Latitude = this.Sonar.FishLatitude(suby);
mbNorthLatitude = max(Latitude);
mbSouthLatitude = min(Latitude);

Longitude = this.Sonar.FishLongitude(suby);
mbEastLongitude = max(Longitude);
mbWestLongitude = min(Longitude);

% Dimensions du format SMF de navigation
mbPositionNbr = length(suby);

%% Modification du skelette

sk = sk_ref;

% Attributs globaux du format SMF g�n�rique
numAtt = cdf_find_numAtt(sk, 'mbName');
sk.att(numAtt).value  = mbName;

% Attributs globaux du format SMF g�or�f�renc�
numAtt = cdf_find_numAtt(sk, 'mbStartDate');
sk.att(numAtt).value  = mbStartDate;

numAtt = cdf_find_numAtt(sk, 'mbStartTime');
sk.att(numAtt).value  = mbStartTime;

numAtt = cdf_find_numAtt(sk, 'mbEndDate');
sk.att(numAtt).value  = mbEndDate;

numAtt = cdf_find_numAtt(sk, 'mbEndTime');
sk.att(numAtt).value = mbEndTime;

numAtt = cdf_find_numAtt(sk, 'mbNorthLatitude');
sk.att(numAtt).value = mbNorthLatitude;

numAtt = cdf_find_numAtt(sk, 'mbSouthLatitude');
sk.att(numAtt).value = mbSouthLatitude;

numAtt = cdf_find_numAtt(sk, 'mbEastLongitude');
sk.att(numAtt).value = mbEastLongitude;

numAtt = cdf_find_numAtt(sk, 'mbWestLongitude');
sk.att(numAtt).value = mbWestLongitude;

% Attributs globaux du format SMF de navigation
numAtt = cdf_find_numAtt(sk, 'mbShip');
sk.att(numAtt).value = mbShip;

numAtt = cdf_find_numAtt(sk, 'mbSurvey');
sk.att(numAtt).value = mbSurvey;

numAtt = cdf_find_numAtt(sk, 'mbPointCounter');
sk.att(numAtt).value = mbPositionNbr;

% Dimensions du format SMF de navigation
numDim = cdf_find_numDim(sk, 'mbPositionNbr');
sk.dim(numDim).value = mbPositionNbr;    % 'mbPositionNbr'

% Param�tres cartographiques
sk = fill_carto(this.Carto, sk_ref);

%% Cr�ation du fichier r�sultat

b = cl_netcdf('fileName', nomFicOut, 'skeleton', sk);

%% Remplissage des valeurs des variables

% Variables du format SMF g�n�rique.
v = get_value(a, 'mbHistAutor');
b = set_value(b, 'mbHistAutor', v);

v = get_value(a, 'mbHistModule');
b = set_value(b, 'mbHistModule', v);

% Variables du format SMF de navigation.
b = set_value(b, 'mbDate', Date);
b = set_value(b, 'mbTime', Heure);
b = set_value(b, 'mbOrdinate', Latitude);
b = set_value(b, 'mbAbscissa', Longitude);

if isSonarSignal(this, 'Height')
    b = set_value(b, 'mbAltitude', this.Sonar.Height(suby));
end

if isSonarSignal(this, 'Immersion')
    b = set_value(b, 'mbImmersion', this.Sonar.Immersion(suby));
end

if isSonarSignal(this, 'Heading')
    b = set_value(b, 'mbHeading', this.Sonar.Heading(suby));
end

if isSonarSignal(this, 'Speed')
    b = set_value(b, 'mbSpeed', this.Sonar.Speed(suby)); %#ok
end





% ATTENTION : FONCTION EN DOUBLE AVEC cl_image/export_CaraibesSidescan
function sk_ref = fill_carto(Carto, sk_ref)

[Ellipsoid, Ellipsoid_name] = getEllipsoidCaraibes(Carto);
DemiGrandAxe = get(Carto, 'Ellipsoide.DemiGrandAxe');   % en metres

numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidA');
sk_ref.att(numAtt).value = DemiGrandAxe;

E2 = floor(get(Carto, 'Ellipsoide.E2'));
numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidE2');
sk_ref.att(numAtt).value = E2;

InvF = get(Carto, 'Ellipsoide.Aplatissement');
numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidInvF');
sk_ref.att(numAtt).value = InvF;

numAtt = cdf_find_numAtt(sk_ref, 'mbEllipsoidName');
sk_ref.att(numAtt).value = Ellipsoid_name;

'Type of projection not finished'
%{
ProjType = getProjectionCaraibes(Carto);
switch ProjType
case 1 % MERCATOR
Meridian_180 = 0;
numAtt = cdf_find_numAtt(sk_ref, 'mbMeridian_180');
sk_ref.att(numAtt).value = Meridian_180;

case 2  % LAMBERT

case 3  % UTM

case 4  % St�r�ographique polaire

otherwise
my_warndlg(['ATTENTION PROJECTION PAS ENCORE TRAITEE DANS ' mfilename], 1);
end
numAtt = cdf_find_numAtt(sk_ref, 'mbProjType');
sk_ref.att(numAtt).value = ProjType;

% ProjParameterValue = [0  0  0  0  0  0  0  0  0  0];
numAtt = cdf_find_numAtt(sk_ref, 'mbProjParameterValue');
sk_ref.att(numAtt).value = ProjType;

ProjParameterCode = '';
numAtt = cdf_find_numAtt(sk_ref, 'mbProjParameterCode');
sk_ref.att(numAtt).value = ProjParameterCode;
%}
