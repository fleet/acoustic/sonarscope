% Exportation des signaux verticaux dans un fichier ASCII
%
% Syntax
%   b = sonar_export_signaux(a, nomsFic, ColumnsForProfile, listeSignaux, Separator, TimeFormat) 
%
% Input Arguments
%   a            : Instance de cl_image
%   nomsFic      : Structure contenant les noms de fichiers
%                  Signaux : Nom du fichier de signaux de l'image
%                  AllAttitude           : Fichier "Attitude" d'un .all
%                  AllClock              : Fichier "Clock" d'un .all
%                  AllHeight             : Fichier "Height" d'un .all
%                  AllPosition           : Fichier "Position" d'un .all
%                  AllSurfaceSoundSpeed  : Fichier "SurfaceSoundSpeed" d'un .all
%   ColumnsForProfile :  Colonnes de l'image devant �tre moy�n�es et
%                        pr�sent�es sous forme de profil
%   listeSignaux : Liste des signaux
%   Separator    : S�parateur
%   TimeFormat   : Type de codage de la date si l'un des signaux contient une date
%                  1= JJ/MM/AAAA, 2=YYYY/MM/DD
%
% Output Arguments
%   b : Instance de cl_image 
%
% Examples 
%   c = sonar_export_signaux(a, 'I:\IfremerToolboxDataPublicSonar\EM300_Ex1.nvi')
%
% Remarks : Si l'image provient d'un .all des fichiers ASCII des datagrams "Attitude", 
%           "Height", etc ... peuvent �tre r�alis�s 
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_export_signaux(this, nomsFic, ColumnsForProfile, listeSignaux, Separator, TimeFormat, subCourbes)

if length(this) > 1
    my_warndlg('Une seule image � la fois SVP', 1);
    return
end

Data = this.Sonar;
for i=1:length(listeSignaux)
    if strcmp(listeSignaux{i}, 'ColumnsAverage')
        for k=length(ColumnsForProfile):-1:1
            X(:,k) = this.Image(:,ColumnsForProfile(k));
        end
        Data.ColumnsAverage = mean(X, 2, 'omitnan');
    end
end

if isempty(subCourbes)
    Courbes = [];
else
    for i=1:length(subCourbes)
        Values = this.CourbesStatistiques.bilan{1}(subCourbes(i)).y;
        Nom    = this.CourbesStatistiques.bilan{1}(subCourbes(i)).nom;
        Courbes(i).Nom = Nom; %#ok<AGROW>
        Courbes(i).Values = Values; %#ok<AGROW>
    end
end

flag = export_table_ascii(nomsFic.Signaux, listeSignaux, Data, Separator, TimeFormat, 'Courbes', Courbes); %#ok<NASGU>
% if ~flag
%     return
% end

% % ---------
% % Controles
% 
% flag = testSignature(this, 'GeometryType', 'PingXxxx');
% if ~flag
%     return
% end

%% Exportation des datagrams "Attitude" d'un fichier .all

if ~isempty(nomsFic.AllAttitude) && strcmp(this.InitialFileFormat, 'SimradAll') && exist(this.InitialFileName, 'file')
    aKM = cl_simrad_all('nomFic', this.InitialFileName);
    Data = read_attitude(aKM);
    flag = export_table_ascii(nomsFic.AllAttitude, {'Time'; 'Roll'; 'Pitch'; 'Heave'; 'Heading'}, Data, Separator, TimeFormat); %#ok<NASGU>
%     if ~flag
%         return
%     end
end

%% Exportation des datagrams "Clock" d'un fichier .all

if ~isempty(nomsFic.AllClock) && strcmp(this.InitialFileFormat, 'SimradAll') && exist(this.InitialFileName, 'file')
    aKM = cl_simrad_all('nomFic', this.InitialFileName);
    Data = read_clock(aKM);
    flag = export_table_ascii(nomsFic.AllClock, {'Time'; 'ExternalTime'}, Data, Separator, TimeFormat); %#ok<NASGU>
%     if ~flag
%         return
%     end
end

%% Exportation des datagrams "SurfaceSoundSpeed" d'un fichier .all

if ~isempty(nomsFic.AllSurfaceSoundSpeed) && strcmp(this.InitialFileFormat, 'SimradAll') && exist(this.InitialFileName, 'file')
    aKM = cl_simrad_all('nomFic', this.InitialFileName);
    Data = read_surfaceSoundSpeed(aKM);
    flag = export_table_ascii(nomsFic.AllSurfaceSoundSpeed, {'Time'; 'SurfaceSoundSpeed'}, Data, Separator, TimeFormat); %#ok<NASGU>
%     if ~flag
%         return
%     end
end

%% Exportation des datagrams "Height" d'un fichier .all

if ~isempty(nomsFic.AllHeight) && strcmp(this.InitialFileFormat, 'SimradAll') && exist(this.InitialFileName, 'file')
    aKM = cl_simrad_all('nomFic', this.InitialFileName);
    Data = read_height(aKM);
    flag = export_table_ascii(nomsFic.AllHeight, {'Time'; 'ExternalTime'}, Data, Separator, TimeFormat); %#ok<NASGU>
%     if ~flag
%         return
%     end
end

%% Exportation des datagrams "Position" d'un fichier .all

if ~isempty(nomsFic.AllPosition) && strcmp(this.InitialFileFormat, 'SimradAll') && exist(this.InitialFileName, 'file')
    aKM = cl_simrad_all('nomFic', this.InitialFileName);
    Data = read_position(aKM);
    flag = export_table_ascii(nomsFic.AllPosition, {'Time'; 'Latitude';'Longitude';'Speed'
        'CourseVessel';'Heading';'PingCounter';'FixPosition';'PositionDecriptor';'NumberOfBytes'}, Data, Separator, TimeFormat); %#ok<NASGU>
%     if ~flag
%         return
%     end
end
