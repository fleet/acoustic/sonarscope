% Filtrage de la bathymetrie (EM1000) dans la zone du speculaire
%
% Syntax
%   b = sonar_filtreBathySpeculaire(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   b = sonar_filtreBathySpeculaire(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_filtreBathySpeculaire(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = unitaire_sonar_filtreBathySpeculaire(this(i), varargin{:});
    if ~flag
        return
    end
    this(i) = that;
end


function [flag, that] = unitaire_sonar_filtreBathySpeculaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, window] = getPropertyValue(varargin, 'window', [3 3]); %#ok<ASGLU>

%% Controls

flag = testSignature(this, 'DataType', cl_image.indDataType('Bathymetry'), ...
    'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end


subNaN = findNaN(this, 'subx', subx, 'suby', suby);
a = WinFillNaN(this, 'window', window, 'subx', subx, 'suby', suby);
that = filterMedian(a, 'window', window);
clear a
that.Image(subNaN) = NaN;

%% Calcul des statistiques

that = compute_stats(that);

%% Rehaussement de contraste

that.TagSynchroContrast = num2str(rand(1));
CLim = [that.StatValues.Min that.StatValues.Max];
that.CLim = CLim;

%% Completion du titre

that = update_Name(that, 'Name', this.Name, 'Append', 'SpikeRemoval');
