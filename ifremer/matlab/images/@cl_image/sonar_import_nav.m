% Importation de la navigation
%
% Syntax
%   b = sonar_import_nav(a, nomFicNav) 
%
% Input Arguments
%   a         : Instance de cl_image
%   nomFicNav : Nom du fichier de navigation
%
% Output Arguments
%   b : Instance de cl_image 
%
% Examples 
%   [I, label] = ImageSonar(1);
%   a(1) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('PingSamples'));
%   c = sonar_import_nav(a, 'I:\IfremerToolboxDataPublicSonar\EM300_Ex1.nvi')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_import_nav(this, nomFicNav)

nbImages = length(this);
for i=1:nbImages
    this(i) = sonar_import_nav_unitaire(this(i), nomFicNav);
end


function this = sonar_import_nav_unitaire(this, nomFicNav)

flag = testSignature(this, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% Lecture de la navigation

[flag, LatNav, LonNav, tNav] = lecFicNav(nomFicNav);
if ~flag
    messageErreurFichier(nomFicNav, 'ReadFailure');
    return
end

tSonar = this.Sonar.Time;

Longitude = my_interp1_longitude(tNav, LonNav, tSonar);
Latitude  = my_interp1(tNav, LatNav, tSonar);

this.Sonar.FishLatitude  = Latitude;
this.Sonar.FishLongitude = Longitude;

