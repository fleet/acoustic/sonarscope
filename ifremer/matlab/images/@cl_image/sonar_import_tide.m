% Importation de la mar�e
%
% Syntax
%   b = sonar_import_tide(a, nomFicTide)
%
% Input Arguments
%   a         : Instance de cl_image
%   nomFicTide : Nom du fichier de navigation
%
% Output Arguments
%   b : Instance de cl_image
%
% Examples
%   c = sonar_import_tide(a, 'I:\IfremerToolboxDataPublicSonar\EM300_Ex1.nvi')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_import_tide(this, nomFicTide, varargin)

[varargin, CleanData] = getPropertyValue(varargin, 'CleanData', 1);
[varargin, Fig]       = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

nbImages = length(this);
for i=1:nbImages
    [this(i), Fig] = unitaire_sonar_import_tide(this(i), nomFicTide, CleanData, Fig);
end


function [this, Fig] = unitaire_sonar_import_tide(this, nomFicTide, CleanData, Fig)


%% Controles

flag = testSignature(this, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

%% Lecture du type de fichier

[flag, Tide] = read_Tide(nomFicTide, 'CleanData', CleanData);
if ~flag
    return
end

Fig = FigUtils.createSScFigure;

plot(Tide.Time, Tide.Value); grid on;
[~, Title, ext] = fileparts(nomFicTide);
Title = [Title ext];
title(Title, 'Interpreter', 'none')

vTide = my_interp1(Tide.Time, Tide.Value, this.Sonar.Time);
if all(isnan(vTide))
    str1 = sprintf('Pas de mar�e pour %s.', this.Name);
    str2 = sprintf('No tide for %s.', this.Name);
    my_warndlg(Lang(str1,str2), 0);
else
    FigUtils.createSScFigure(Fig); hold on;
    PlotUtils.createSScPlot(this.Sonar.Time, vTide, 'r*'); grid on
    legend({Title; this.Name}, 'Interpreter', 'none')
end

this.Sonar.Tide = vTide;


