% Improve the Heave compensation for a PingBeam Bathymetry image
%
% Syntax
%   [b, c] = sonar_improveHeaveCorrection(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   SeuilProba : Seuil de d�tection des sones erron�es (0.5 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_improveHeaveCorrection(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_improveHeaveCorrection(this, varargin)

for k=1:length(this)
    [flag, that] = sonar_improveHeaveCorrection_unitaire(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end


function [flag, this] = sonar_improveHeaveCorrection_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controls

ident = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'DataType', ident, 'GeometryType', [cl_image.indGeometryType('PingBeam') cl_image.indGeometryType('PingAcrossDist')]);
if ~flag
    return
end

%% Cr�ation du masque

I = this.Image(suby,:);

%% Calcul de la bathy moyenne par ping

m = mean(I(:,subx), 2, 'omitnan');

%% Nettoyage du signal
  
OldHeave = this.Sonar.Heave(suby,:);
m = m(:)';
Title = ['Edit Mean bathy - ' this.Name];
xSample = XSample('name', 'Pings',  'data', 1:length(m));
ySample = YSample('Name', 'Mean bathy', 'data', m);
signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
s.openDialog();
if s.okPressedOut
    Residu = m - ySample.data;
end
NewHeave = OldHeave(:) - Residu(:);

%% Calcul de la bathy corrig�e

if size(m,2) == 1
    J = bsxfun(@minus, I, Residu(:));
else
    ns2 = this.nbColumns / 2; % TODO : Danger ici car si l'image a d�j� �t� extraite d'une autre �a ne fonctionne plus
    %     subBab = find(subx <= ns2);
    %     subTri = find(subx > ns2);
    subBab = 1:ns2;
    subTri = (ns2+1):this.nbColumns;
    J = I;
    J(:,subBab) = bsxfun(@minus, I(:,subBab), Residu(:,1));
    J(:,subTri) = bsxfun(@minus, I(:,subTri), Residu(:,2));
end
I2 = cl_image('Image', I', 'ColormapIndex', 20);
I2.Azimuth = 270;
I2 = sunShading(I2);

J2 = cl_image('Image', J', 'ColormapIndex', 20);
J2.Azimuth = 270;
J2 = sunShading(J2);

%% Affichage pour validation

fig = FigUtils.createSScFigure;
centrageFig(1800, 1000, 'Fig', fig);
subplot(4,1,1); PlotUtils.createSScPlot(this.y(suby), Residu); grid on; xlabel(this.YLabel); title('Heave correction (m)');
subplot(4,1,2); PlotUtils.createSScPlot(this.y(suby), OldHeave, 'b'); grid on; xlabel(this.YLabel);
title('Old Heave (blue), New Heave (red)');
hold on; PlotUtils.createSScPlot(this.y(suby), NewHeave, 'r');
subplot(4,1,3); imagesc(this.x(subx), this.y(suby), I2.Image); grid on; xlabel(this.YLabel); title('Old Bathymetry (m)');
subplot(4,1,4); imagesc(this.x(subx), this.y(suby), J2.Image); grid on; xlabel(this.YLabel); title('New Bathymetry (m)');

%% Affichage du signal

%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(m); grid on;
xlabel(this.YLabel);
ylabel('Bathy');
title('Create New Heave');
%}

%% Validation

if ~isempty(this.InitialFileName) && exist(this.InitialFileName, 'file')
    str1 = sprintf('Voulez-vous sauver le nouveau signal "Heave" dans le fichier d''origine "%s" ?', ...
        this.InitialFileName);
    str2 = sprintf('Do you want to save the new "Heave" signal in the original file "%s" ?', ...
        this.InitialFileName);
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 1
        switch this.InitialFileFormat
            case 'SimradAll'
                aKM = cl_simrad_all('nomFic', this.InitialFileName);
                [flag, DataAttitude] = read_attitude_bin(aKM, 'Memmapfile', 0);
                if ~flag || isempty(DataAttitude) || isempty(DataAttitude.Roll)
                    str1 = sprintf('Les donn�es "Attitude" du fichier "%s" n''ont pas �t� trouv�es, la sauvegarde n''a pas pu �tre faite.', this.InitialFileName);
                    str2 = sprintf('The "Attitude" data for file "%s" could not be found, the export could not be done.', this.InitialFileName);
                    my_warndlg(Lang(str1,str2), 1);
                else
                    T = this.Sonar.Time.timeMat;
                    
                    % TODO : attention Sondeur DUAL
                    if size(Residu,2) == 2 % Cas pas encore rencontr�
                        AttitudeResiduals(:,1) = interp1(T(suby), Residu(:,1), DataAttitude.Time.timeMat);
                        AttitudeResiduals(:,2) = interp1(T(suby), Residu(:,2), DataAttitude.Time.timeMat);
                    else
                        AttitudeResiduals = interp1(T(suby), Residu, DataAttitude.Time.timeMat);
                    end
                    
                    AttitudeResiduals(isnan(AttitudeResiduals)) = 0;
                    AttitudeResiduals = reshape(AttitudeResiduals, size(DataAttitude.Heave));
                    NewHeave = DataAttitude.Heave(:,:) - AttitudeResiduals;
                    %                     figure; plot(DataAttitude.Heave(:,:) , 'b'); grid on;hold on; plot(NewHeave, 'r');
                    
                    flag = save_signal(aKM, 'Ssc_Attitude', 'Heave', NewHeave);
                    if ~flag
                        str1 = sprintf('L''export de la navigation pour le fichier "%s" n''a pas pu se faire.', this.InitialFileFormat);
                        str2 = sprintf('The import of navigation for "%s" failed.', this.InitialFileFormat);
                        my_warndlg(Lang(str1,str2), 0);
                        return
                    end
                    
                    [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 1);
                    if flag
                        for k=1:size(DataDepth.TransducerDepth,2)
                            if size(Residu,2) == 2
                                DataDepth.TransducerDepth(:,k) = DataDepth.TransducerDepth(:,k) + Residu(:,k);
                            else
                                DataDepth.TransducerDepth(:,k) = DataDepth.TransducerDepth(:,k) + Residu(:);
                            end
                        end
                        flag = save_signal(aKM, 'Ssc_Depth', 'TransducerDepth', DataDepth.TransducerDepth);
                    end
                end
            otherwise
                str1 = sprintf('La sauvegarde du signal "Heave" n''est pas encore branch�e pour le format "%s". Plese send an email to sonarscope@ifremer.fr', ...
                    this.InitialFileFormat);
                str2 = sprintf('The export of the "Heave" signal is not ready yet for format "%s". Plese send an email to sonarscope@ifremer.fr', ...
                    this.InitialFileFormat);
                my_warndlg(Lang(str1,str2), 1);
        end
    end
end

%% Cr�ation de l'instance

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, J);

%% Mise � jour de coordonn�es

% this = majCoordonnees(this, subx, suby);
this = majCoordonnees(this, 1:this.nbColumns, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Remplacement du signal Heave

this.Sonar.Heave = NewHeave;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'NewHeave');

%% Close figure

my_close(fig);

