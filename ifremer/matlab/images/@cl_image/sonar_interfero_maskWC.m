% Mise � NaN de la WC
%
% Syntax
%   b = sonar_interfero_maskWC(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_interfero_maskWC(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = sonar_interfero_maskWC(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [a, flag] = unitaire_sonar_interfero_maskWC(this(i), varargin{:});
    if flag
        that(i,:) = a(:)'; %#ok<AGROW>
    else
        that = [];
        return
    end
end


function [this, flag] = unitaire_sonar_interfero_maskWC(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, OtherLayers] = getPropertyValue(varargin, 'OtherLayers', []); %#ok<ASGLU>

%% Controles

% Test si SonarD ou SonarX
PS = cl_image.indGeometryType('PingSamples');
PR = cl_image.indGeometryType('PingRange');
flag = testSignature(this, 'GeometryType', [PS PR cl_image.indGeometryType('PingAcrossDist')]);
if ~flag
    this = [];
    return
end

if isempty(this.Sonar.Height)
    str = sprintf('Pas de hauteur renseignee. Veuillez la definir ou la calculer.');
    my_warndlg(str, 1);
    this = [];
    return
end

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, this.Image(suby,subx));
this = majCoordonnees(this, subx, suby);

for k=1:length(OtherLayers)
    OtherLayers(k) = replace_Image(OtherLayers(k), OtherLayers(k).Image(suby,subx));
    OtherLayers(k) = majCoordonnees(OtherLayers(k), subx, suby);
end

% Mask = false(size(that.Image));

H = abs(this.Sonar.Height);
N = length(this.y);
hw = create_waitbar('Processing', 'N', N);
for i=1:N
    my_waitbar(i, N, hw)
    %     sub = isnan(this.Image(i,:));
    %     Mask(i,sub) = true;
    
    sub = (abs(this.x) < H(i));
    this.Image(i,sub) = NaN;
    
    for k=1:length(OtherLayers)
        if isnan(OtherLayers(k).ValNaN)
            OtherLayers(k).Image(i,sub) = NaN;
        else
            OtherLayers(k).Image(i,sub) = true;
        end
    end
    
    %     Mask(i,sub) = true;
end
my_close(hw, 'MsgEnd')


this = compute_stats(this);
this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this = update_Name(this, 'Append', 'WCNaN');

for k=1:length(OtherLayers)
    OtherLayers(k) = compute_stats(OtherLayers(k));
    OtherLayers(k).TagSynchroContrast = num2str(rand(1));
    CLim = [OtherLayers(k).StatValues.Min OtherLayers(k).StatValues.Max];
    OtherLayers(k).CLim = CLim;
    OtherLayers(k) = update_Name(OtherLayers(k), 'Append', 'WCNaN');
end

if ~isempty(OtherLayers)
    this = [OtherLayers(:)' this];
end

