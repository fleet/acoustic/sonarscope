% Calcul du layer projete sur l'axe des X
%
% Syntax
%   b = sonar_lateral_LayerDepth(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image projetee
%
% Examples
%   b = sonar_lateral_LayerDepth(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_lateral_LayerDepth(this, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_sonar_lateral_LayerDepth(this(i), varargin{:});
end


function this = unitaire_sonar_lateral_LayerDepth(this, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []); %#ok<ASGLU>

if isempty(suby)
    suby = 1:length(this.y);
end
subx = 1:length(this.x);

%% Controles

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingAcrossDist'));
if ~flag
    this = [];
    return
end

if isempty(this.Sonar.Height)
    str = sprintf('Pas de hauteur renseignee. Veuillez la definir ou la calculer.');
    my_warndlg(str, 1);
    
    this = [];
    return
end

if isempty(this.Sonar.Immersion)
    str = sprintf('Pas d''immersion renseignee.');
    my_warndlg(str, 1);
    
    this = [];
    return
end

%% Traitement

h = this.Sonar.Height(suby);
h = h(:);

I = this.Sonar.Immersion(suby);
I = I(:);


% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)
this = replace_Image(this, repmat(h+I, 1, this.nbColumns));
this.DataType = cl_image.indDataType('Bathymetry');

%% Mise � jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
this.ColormapIndex = 3;

%% Completion du titre

this = update_Name(this);
