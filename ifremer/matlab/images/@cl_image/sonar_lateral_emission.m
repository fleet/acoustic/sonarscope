% Calcul du layer emission
%
% Syntax
%   b = sonar_lateral_emission(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%   useRoll : {1='Prise en compte du roulis si il existe'} | 0=Pas de prise
%             en compte du roulis meme si il existe
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_lateral_emission(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = sonar_lateral_emission(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [that, flag] = unitaire_sonar_lateral_emission(this(k), varargin{:});
    if flag
        this(k) = that;
    else
        this = [];
        return
    end
end


function [this, flag] = unitaire_sonar_lateral_emission(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, useRoll] = getPropertyValue(varargin, 'useRoll', []); %#ok<ASGLU>

if isempty(useRoll)
    useRoll = 1;
end

%% Contr�les

% Test si SonarD ou SonarX
PS = cl_image.indGeometryType('PingSamples');
PR = cl_image.indGeometryType('PingRange');
flag = testSignature(this, 'GeometryType', [PS PR cl_image.indGeometryType('PingAcrossDist')]);
if ~flag
    this = [];
    return
end

if isempty(this.Sonar.Height)
    str1 = 'Pas de hauteur renseign�e. Veuillez la definir ou la calculer.';
    str2 = 'No height available, please compute it.';
    my_warndlg(Lang(str1,str2), 1);
    this = [];
    return
end

% On sauvegarde le roulis utilise pour le calcul de l'angle d'emission pour pouvoir le retirer lors du calcul
% de l'angle d'incidence et de l'aire insonifiee
if isSonarSignal(this, 'Roll') && useRoll
    this.Sonar.RollUsedForEmission = this.Sonar.Roll;
else
    this.Sonar.RollUsedForEmission = zeros(size(this.Sonar.Roll));
end
this.Sonar.RollUsedForEmission(isnan(this.Sonar.RollUsedForEmission)) = 0;

subBab = find(this.x(subx) <= 0);
subTri = find(this.x(subx) > 0);

this.Image = NaN(length(suby), length(subx), 'single');
w = warning;
warning('off', 'all');
H = abs(this.Sonar.Height(suby,:));
for k=1:length(suby)
    if size(H,2) == 2
        if this.GeometryType == PS
            Q(:,subBab) = H(k,1) ./ this.x(subx(subBab)); %#ok<AGROW> % ATTENTION, CORRECTIF A FAIRE : resolution
            Q(:,subTri) = H(k,2) ./ this.x(subx(subTri)); %#ok<AGROW> % ATTENTION, CORRECTIF A FAIRE : resolution
            Q(abs(Q) > 1) = NaN; %#ok<AGROW>
            Q = acos(Q) * (180 / pi);
            sub = find(Q > 90);
            Q(sub) = Q(sub) - 180;
            
        elseif this.GeometryType == PR
            Q(:,subBab) = H(k,1) ./ this.x(subx(subBab)); % ATTENTION, CORRECTIF A FAIRE : resolution
            Q(:,subTri) = H(k,2) ./ this.x(subx(subTri)); %#ok<NASGU> % ATTENTION, CORRECTIF A FAIRE : resolution
            Q = H(k) ./ this.x(subx);
            Q(abs(Q) > 1) = NaN;
            Q = acos(Q) * (180 / pi);
            sub = find(Q > 90);
            Q(sub) = Q(sub) - 180;
            
        else
            Q(:,subBab) = this.x(subx(subBab)) / H(k,1); % ATTENTION, CORRECTIF A FAIRE : resolution
            Q(:,subTri) = this.x(subx(subTri)) / H(k,2); % ATTENTION, CORRECTIF A FAIRE : resolution
            Q = atan(Q) * (180 / pi);
        end
    else
        if this.GeometryType == PS
            Q = H(k) ./ this.x(subx); % ATTENTION, CORRECTIF A FAIRE : resolution
            Q(abs(Q) > 1) = NaN;
            
            Q = acos(Q) * (180 / pi);
            sub = find(Q > 90);
            
            Q(sub) = Q(sub) - 180;
        elseif this.GeometryType == PR
            Q = H(k) ./ this.x(subx);
            Q(abs(Q) > 1) = NaN;
            
            Q = acos(Q) * (180 / pi);
            sub = find(Q > 90);
            
            Q(sub) = Q(sub) - 180;
        else
            Q = this.x(subx) / H(k);
            Q = atan(Q) * (180 / pi);
        end
    end
    
    if isSonarSignal(this, 'Roll') && useRoll
        if size(this.Sonar.RollUsedForEmission, 2) == 2
            Q(:,subBab) = Q(:,subBab) + this.Sonar.RollUsedForEmission(suby(k), 1);
            Q(:,subTri) = Q(:,subTri) + this.Sonar.RollUsedForEmission(suby(k), 2);
        else
            Q = Q + this.Sonar.RollUsedForEmission(suby(k));
        end
    end
    
    this.Image(k,:) = Q;
end
warning(w)

this.ValNaN = NaN;

%% Mise � jour de coordonn�es

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'deg';
this.ColormapIndex = 3;
this.DataType = cl_image.indDataType('TxAngle');

%% Mise � jour du nom de l'image

this = update_Name(this);


