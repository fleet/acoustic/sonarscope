% Calcul de la hauteur sur une image sonar de type 'SonarD'
%
% Syntax
%   b = sonar_lateral_hauteur(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx          : subsampling in X
%   suby          : subsampling in Y
%   FiltreSonar   : Coefficients de filtrage du signal sonar [Ordre du filtre Pulsation de coupure]
%                   [] pour indiquer "pas de filtrage
%   FiltreProfile : Coefficients de filtrage du signal de hauteur [Ordre du filtre Pulsation de coupure]
%                   [] pour indiquer "pas de filtrage
%   Penetration   : Coefficient de penetration  (0 par defaut)
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_lateral_hauteur(a)
%   polot(a)
%   plot(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_lateral_hauteur(this, varargin)

nbImages = length(this);
for k=1:nbImages
    this(k) = unitaire_sonar_lateral_hauteur(this(k), varargin{:});
end


function this = unitaire_sonar_lateral_hauteur(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, FiltreSonar]   = getPropertyValue(varargin, 'FiltreSonar',   []);
[varargin, FiltreProfile] = getPropertyValue(varargin, 'FiltreProfile', []);
[varargin, Penetration]   = getPropertyValue(varargin, 'Penetration',   0);
[varargin, Offset]        = getPropertyValue(varargin, 'Offset',        0);
[varargin, CoteDetection] = getPropertyValue(varargin, 'CoteDetection', 0); %#ok<ASGLU>

if isempty(FiltreSonar)
    FiltreSonarA = [];
    FiltreSonarB = [];
else
    if FiltreSonar(2) == 0
        FiltreSonarA = [];
        FiltreSonarB = [];
    else
        [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
        FiltreSonarA = A;
        FiltreSonarB = B;
    end
end

if isempty(FiltreProfile)
    FiltreProfileA = [];
    FiltreProfileB = [];
else
    if FiltreProfile(2) == 0
        FiltreProfileA = [];
        FiltreProfileB = [];
    else
        [B,A] = butter(FiltreProfile(1), FiltreProfile(2));
        FiltreProfileA = A;
        FiltreProfileB = B;
    end
end

%% Contr�les

% Test si Reflectivity et SonarD
flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), ...
    'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange')]);
if ~flag
    this = [];
    return
end

if isempty(this.Sonar.Height)
    this.Sonar.Height = zeros(length(this.y), 1);
end

y = this.y(suby);
x = this.x(subx);
subxPositif = find(x > 0);
subxNegatif = find(x < 0);

switch CoteDetection
    case 0
        if length(subxPositif) >= length(subxNegatif)
            flagBabord = 0;
        else
            flagBabord = 1;
        end
    case 1
        flagBabord = 1;
    case 2
        flagBabord = 0;
end

if flagBabord
    I = fliplr(this.Image(suby, subx(subxNegatif)));
    x = fliplr(-x(subxNegatif));
else
    I = this.Image(suby, subx(subxPositif));
    x = x(subxPositif);
end
nx = length(x);

%% Interpolation de I

I = fillNaN_mean(I, [5 5]);

%% D�tection

M = length(y);
H = zeros(M, 1, 'single');

str1 = sprintf('%s : Estimation de la hauteur', this.Name);
str2 = sprintf('%s : Height estimation', this.Name);
hw = create_waitbar(Lang(str1,str2), 'N', M);
for k=1:M
    my_waitbar(k, M, hw);
    
    signal = I(k,:);
    subNonNan = find(~isnan(signal));
    if isempty(subNonNan)
        H(k) = 1;
        continue
    end
    signal = signal(subNonNan);
    h = calculer_hauteur_signal1(signal, FiltreSonarA, FiltreSonarB, Penetration);
    h = subNonNan(h);
    h = h + Offset;
    h = min(nx, max(1,h));
    H(k) = -abs(x(round(h)));
end
my_close(hw, 'MsgEnd');

% figure; imagesc(x, 1:M, I); colormap(jet(256)); colorbar; hold on; plot(-H, 1:M, 'k');

% TODO : voir si Hampel peut �tre remplac� par signal_spike_filter dans ClSignal
%{
HOut = signal_spike_filter(H, 'Height');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(H, 'b'); grid on; hold on; PlotUtils.createSScPlot(HOut, 'r');
H = HOut;
HOut = signal_spike_filter(H, 'Height');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(H, 'b'); grid on; hold on; PlotUtils.createSScPlot(HOut, 'r');
H = HOut;
HOut = signal_spike_filter(H, 'Height');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(H, 'b'); grid on; hold on; PlotUtils.createSScPlot(HOut, 'r');
H = HOut;
%}

if ~isempty(FiltreProfileA) && ~isempty(FiltreProfileB)
    H = filtfilt(FiltreProfileB, FiltreProfileA, double(H));
end

this.Sonar.Height(suby,1) = H(:);
