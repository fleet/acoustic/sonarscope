% Calcul du layer incidence
%
% Syntax
%   b = sonar_lateral_incidence(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx         : subsampling in X
%   suby         : subsampling in Y
%   layersPentes : instances de cl_image contenant : SlopeAlong SlopeAcross
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_lateral_incidence(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_lateral_incidence(this, varargin)

N = length(this);
str1 = 'Calcul des images "Incident angle"';
str2 = 'Computing images of "Incident angle"';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    this(k) = unitaire_sonar_lateral_incidence(this(k), varargin{:});
end
my_close(hw, 'MsgEnd')


function this = unitaire_sonar_lateral_incidence(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, layersPentes] = getPropertyValue(varargin, 'layersPentes', []); %#ok<ASGLU>

%% Contr�les

% Test si EmissionAngle
identEmissionAngle      = cl_image.indDataType('TxAngle');
identRxBeamAngle        = cl_image.indDataType('RxBeamAngle');
identBeamPointingAngle  = cl_image.indDataType('BeamPointingAngle');
flag = testSignature(this, 'DataType', [identEmissionAngle identRxBeamAngle identBeamPointingAngle]);
if ~flag
    this = [];
    return
end

%% Traitement

Immersion = this.Sonar.Immersion;
if isempty(Immersion)
    Immersion = 0;
else
    Immersion = Immersion(suby);
end
H = abs(this.Sonar.Height(suby,1));
try
    Z = this.Sonar.BathyCel.Z;
    C = this.Sonar.BathyCel.C;
catch
    Z = this.Sonar.BathyCel.Depth(:);
    C = this.Sonar.BathyCel.SoundSpeed(:);
end

Immersion = Immersion(:);
H = H(:);
Z = Z(:);
C = C(:);

if isempty(Z)
    coeff = ones(size(this.Sonar.Immersion), 'single');
    str1 = 'Il n''y a pas de profil de c�l�rite fourni dans cette image, la cr�ation de l''angle d''incidence se fera donc sans Snell-Descartes, ce qui est tr�s dommageable.';
    str2 = 'There is no sound speed profile associated to this image, the incidence angle will be done without Snell-Descartes, that is a pity !';
    my_warndlg(Lang(str1,str2), 0);
else
    celeriteSonar = interp1(Z, C, Immersion);
    celeriteFond  = interp1(Z, C, Immersion - H);
    coeff = (celeriteFond ./ celeriteSonar);
end

I = NaN(length(suby), length(subx), 'single');
for i=1:length(suby)
    if isempty(this.Sonar.RollUsedForEmission)
        tetaEmi = this.Image(suby(i),subx);
    else
        tetaEmi = this.Image(suby(i),subx) - this.Sonar.RollUsedForEmission(suby(i));
    end
    if isempty(coeff)
        X = sin(tetaEmi * (pi/180));
    else
        X = coeff(i) * sin(tetaEmi * (pi/180));
    end
    subYes  = find(abs(X) <= 1);
    
    if isempty(layersPentes)
        I(i,subYes) = asin(X(subYes)) * (180/pi);
    else
        % subx or not subx ????????
        
        
        % Cf These Samantha Dugelay page 81
        tetai = asin(X(subYes));
        beta  = layersPentes(1).Image(suby(i),subx(subYes)) * (pi/180); % Pente longitudinale
        alpha = layersPentes(2).Image(suby(i),subx(subYes)) * (pi/180); % Pente transversale
        
        % alpha(:)= 0 * pi/180;
        % beta(:) = 10 * pi/180;
        
        sub = find(~isnan(alpha) & ~isnan(beta));
        tetai = tetai(sub);
        alpha = alpha(sub);
        beta  = beta(sub);
        
        ze = zeros(1, length(tetai));
        % Axe (x,y,z) : x represente le sens longitudinal, y le sens
        % trensversal et z la bathymetrie
        Vi = [-sin(tetai); ze; cos(tetai)];
        Vn = [  -sin(alpha) .* cos(beta)
            -cos(alpha) .* sin(beta)
            cos(alpha) .* cos(beta)];
        
        %             NormeVi = sqrt(Vi(1,:).^2              + Vi(3,:).^2);
        %             NormeVn = sqrt(Vn(1,:).^2 + Vn(2,:).^2 + Vn(3,:).^2);
        %             ProduitScalaire = sum((Vi .* Vn)) ./ (NormeVi .* NormeVn);
        
        ProduitScalaire = sum((Vi .* Vn));  % Car NormeVi=1 et NormeVn=1
        tetaf = acos(ProduitScalaire) * (180/pi);
        
        signeTetai = (tetai >= 0);
        tetaf = tetaf .* (2*signeTetai - 1);
        I(i,subYes(sub)) = tetaf;
    end
end

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = replace_Image(this, I);
this.ValNaN = NaN;

%% Mise � jour des coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'deg';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('IncidenceAngle');
this = update_Name(this);
