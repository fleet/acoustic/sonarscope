% Calcul du "Temps universel"
%
% Syntax
%   b = sonar_lateral_tempsUniversel(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_lateral_tempsUniversel(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_lateral_tempsUniversel(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = unitaire_sonar_lateral_tempsUniversel(this(i), varargin{:});
    if flag
        this(i) = that;
    else
        return
    end
end


function [flag, this] = unitaire_sonar_lateral_tempsUniversel(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contr�les

indGeometryTypes = [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist') cl_image.indGeometryType('PingBeam')];
flag = testSignature(this, 'GeometryType', indGeometryTypes);
if ~flag
    return
end

this.Image  = NaN(length(suby), length(subx), 'double');  % double OBLIGATOIREMENT

C = [];
for i=1:length(suby)
    H = abs(this.Sonar.Height(suby));
    if this.GeometryType == cl_image.indGeometryType('PingSamples')
        x = abs(this.x(subx));
    else
        x = sqrt(this.x(subx) .^ 2  + H(i) ^ 2);
    end
    % On devrait en principe utiliser get(this.Sonar.Desciption, 'Proc.SampFreq') mais c'est=0 pour le
    % DF1000 : CORRECTION A FAIRE
    % A defaut, on fait plus complique
    
    if isempty(C) % On suppose que la vitesse du son est la m�me sur toute l'image
        if isfield(this.Sonar.BathyCel, 'Z')
            Z = this.Sonar.BathyCel.Z;
            if isempty(Z)
                disp('Message pour JMA : sonar_lateral_tempsUniversel C=1500 : c''est pas top de faire ca')
                C = 1500;
            else
                Z1 = this.Sonar.Immersion(i);
                Z2 = this.Sonar.Immersion(i) - H(i);
                %             subZ = find((Z >= Z2) & (Z <= Z1));
                subZ = ((Z >= Z2) & (Z <= Z1));
                C = mean(this.Sonar.BathyCel.C(subZ));
            end
        elseif isfield(this.Sonar.BathyCel, 'SoundSpeed')
            Z = this.Sonar.BathyCel.Depth(:);
            if isempty(Z)
                disp('Message pour JMA : sonar_lateral_tempsUniversel C=1500 : c''est pas top de faire ca')
                C = 1500;
            else
                Z1 = this.Sonar.Immersion(i);
                Z2 = this.Sonar.Immersion(i) - H(i);
                %             subZ = find((Z >= Z2) & (Z <= Z1));
                subZ = ((Z >= Z2) & (Z <= Z1));
                C = mean(this.Sonar.BathyCel.SoundSpeed(subZ), 'omitnan');
            end
        end
    end
    
    Q = this.Sonar.Time(i) + x * (2 * 1000 / C);
    this.Image(i,:) = timeIfr2Mat(Q.date, Q.heure);
end
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'time';
this.ColormapIndex = 3;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('RxTime');
InitialFileName = this.InitialFileName;
if ~isempty(InitialFileName)
    [~, ImageName] = fileparts(InitialFileName);
    this = update_Name(this, 'Name', ImageName);
else
    this = update_Name(this, 'Append', 'Temps universel');
end
