function [flag, listeConf, listeConfWithNbPings, N, listeModesConf, listeSwathConf, listeFMConf, listeMode2EM2040, listeSuby, listeSubyInd] = sonar_listConfigs(this, suby)

listeConf            = [];
listeConfWithNbPings = [];
N                    = [];
listeModesConf       = [];
listeSwathConf       = [];
listeFMConf          = [];
listeMode2EM2040     = [];
listeSuby            = [];
listeSubyInd         = [];

if isempty(suby)
    suby = 1:this.nbRows;
end

flag = testSignature(this, 'GeometryType', 'PingXxxx', 'noMessage');
if ~flag
    flag = 1;
    listeSuby = suby;
    listeSubyInd = 1:length(suby);
    return
end

%% Inventaire des Mode1 pr�sent dans l''image

if isempty(this.Sonar.PortMode_1)
    listeSuby = suby;
    return
else
    if size(this.Sonar.PortMode_1,1) == 1 % Temporaire le temps que l'import .s7k soit refait
        this.Sonar.PortMode_1 = this.Sonar.PortMode_1';
    end
    [listeModes, ~, JMode] = unique(this.Sonar.PortMode_1(suby,1));
end

% signalMode1 = this.SignalsVert.getSignalByTag('Mode1');
% if isempty(signalMode1)
%     listeMode1 = [];
% else
%     [flag, Mode1, Unit] = signalMode1.getValueMatrix();
%     if ~flag
%         return
%     end
%     [listeMode1, ~, JMode1] = unique(Mode1(suby));
%     nomMode1 = signalMode1.ySample.strIndexList;
% end
% listeModes = listeMode1;

%% Mode2 if EM2040

signalMode2 = this.SignalsVert.getSignalByTag('Mode2');
if isempty(signalMode2)
    listeMode2 = [];
else
    [flag, Mode2, Unit] = signalMode2.getValueMatrix();
    if ~flag
        return
    end
    [listeMode2, ~, JMode2] = unique(Mode2(suby));
    nomMode2 = signalMode2.ySample.strIndexList;
end

%% Inventaire du nombre de swaths

if ~isfield(this.Sonar, 'NbSwaths') || isempty(this.Sonar.NbSwaths)
    listeSwath = [];
else
    [listeSwath, ~, JSwath] = unique(this.Sonar.NbSwaths(suby,1));
end

%% Inventaire CW/FM

if ~isfield(this.Sonar, 'PresenceFM') || isempty(this.Sonar.PresenceFM)
    listeFM = [];
else
    [listeFM, ~, JFM] = unique(1 + this.Sonar.PresenceFM(suby,1));
end

%% Questions � l'utilisateur

nModes   = length(listeModes);
nSwaths  = length(listeSwath);
nMode2   = length(listeMode2);
nCWFM    = length(listeFM);
nomSwath = {'Single'; 'Double'}; % Multiple si Reson ?
nomCWFM  = {'CW '; 'FM'; '???'}; % Multiple si Reson ?

listeConf            = [];
N                    = [];
listeModesConf       = [];
listeSwathConf       = [];
listeFMConf          = [];
listeMode2EM2040     = [];
listeConfWithNbPings = [];

idebModes  = min(1,nModes);
idebSwaths = min(1,nSwaths);
idebMode2  = min(1,nMode2);
idebnCWFM  = min(1,nCWFM);
for k1=idebModes:nModes
    for k2=idebSwaths:nSwaths
        for k3=idebnCWFM:nCWFM
            for k4=idebMode2:nMode2
                Line = [];
                if k1 == 0
                    sub1 = true(length(suby),1);
                else
                    Line = sprintf('%s : Mode %d', Line, listeModes(k1));
                    sub1 = (this.Sonar.PortMode_1(suby,1) == listeModes(k1));
                end
                if k2 == 0
                    sub2 = true(length(suby),1);
                else
                    Line = sprintf('%s : Swath %s', Line, nomSwath{listeSwath(k2)});
                    sub2 = (this.Sonar.NbSwaths(suby,1) == listeSwath(k2));
                end
                if k3 == 0
                    sub3 = true(length(suby),1);
                else
                    Line = sprintf('%s : Signal %s', Line, nomCWFM{listeFM(k3)});
                    sub3 = (1 + this.Sonar.PresenceFM(suby,1) == listeFM(k3));
                end
                if k4 == 0
                    sub4 = true(length(suby),1);
                else
                    Line = sprintf('%s : Signal %s', Line, nomMode2{listeMode2(k4)});
                    sub4 = (Mode2(suby) == listeMode2(k4));
                end
                
                sub = sub1 & sub2 & sub3 & sub4;
                nbPings = sum(sub);
                if ~isempty(Line) && any(sub)
                    %                 Line = sprintf('%s : %d pings', Line(4:end), nbPings);
                    Line = Line(4:end);
                    listeConf{end+1,1} = Line; %#ok<AGROW>
                    listeConfWithNbPings{end+1,1} = sprintf('%s : %d pings', Line, nbPings); %#ok<AGROW>
                    N(end+1) = nbPings; %#ok<AGROW>
                    listeModesConf(end+1) = listeModes(k1); %#ok<AGROW>
                    if nSwaths > 0
                        listeSwathConf(end+1) = listeSwath(k2); %#ok<AGROW>
                    end
                    if nCWFM > 0
                        listeFMConf(end+1) = listeFM(k3); %#ok<AGROW>
                    end
                    if nMode2 > 0
                        listeMode2EM2040(end+1) = listeMode2(k4); %#ok<AGROW>
                    end
                    listeSuby{end+1} = suby(sub); %#ok<AGROW>
                    listeSubyInd{end+1} = find(sub); %#ok<AGROW>
                end
            end
        end
    end
end
% listeConf
