function layerMasque = sonar_maskTimeSlot(this, SonarTime, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

layerMasque = this;
M = zeros(size(this.Image), 'single');

layerMasque = replace_Image(layerMasque, M);
clear M
layerMasque.ValNaN = NaN;

M = layerMasque.Image(suby, subx);

Ndecoupe = size(SonarTime,1);
SonarTimeALL = get(this, 'SonarTime');
SonarTimeALL = SonarTimeALL.timeMat';
subyOK = false(size(SonarTimeALL,1),1);
for k=1:Ndecoupe % TODO
    subyOK = subyOK | (...
        (SonarTimeALL >= SonarTime(k,1)) & ...
        (SonarTimeALL <= SonarTime(k,2)) );
end
if all(subyOK)
    layerMasque = [];
    return
end

M(subyOK,:) = 1;

layerMasque.Writable = true;
layerMasque.Image(suby, subx) = M;
layerMasque.Writable = false;

%% Mise � jour des coordonn�es

layerMasque = majCoordonnees(layerMasque, 1:length(this.x), 1:length(this.y));
layerMasque.DataType = cl_image.indDataType('Mask');
layerMasque.Unit     = 'num';

%% Calcul des statistiques

layerMasque = compute_stats(layerMasque);

%% Rehaussement de contraste

layerMasque.TagSynchroContrast = num2str(rand(1));
layerMasque.CLim               = [0 layerMasque.StatValues.Max];

%% Compl�tion du titre

layerMasque = update_Name(layerMasque, 'Append', 'TimeSlot');
layerMasque.ColormapIndex = 3;
