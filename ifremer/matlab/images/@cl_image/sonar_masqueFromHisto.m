% Calcul d'un masque sur les donn�es douteuses d�duite d'une analyse de
% l'histogramme de l'image
%
% Syntax
%   [b, c] = sonar_masqueFromHisto(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx       : subsampling in X
%   suby       : subsampling in Y
%   SeuilProba : Seuil de d�tection des sones erron�es (0.5 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_masqueFromHisto(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this, P] = sonar_masqueFromHisto(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    flag = 0;
    return
end

ident1 = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'DataType', ident1); %, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    P = [];
    return
end

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nbLigBlock] = getPropertyValue(varargin, 'nbLigBlock', 1000);
[varargin, SeuilProba] = getPropertyValue(varargin, 'SeuilProba', 0.5); %#ok<ASGLU>

%% Calcul du masque et de la probabilit�

[Masque, Proba] = cleanHisto(this.Image(suby, subx), 'SeuilProba', SeuilProba, 'nbLigBlock', nbLigBlock);
this = replace_Image(this, Masque);
this.ValNaN = NaN;

%% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', '_Masque from Histo_');

%% Image de la probabilite

P = replace_Image(this, Proba);

%% Calcul des statistiques

P = compute_stats(P);

%% Rehaussement de contraste

P.TagSynchroContrast = num2str(rand(1));
CLim = [P.StatValues.Min P.StatValues.Max];
P.CLim = CLim;
P.ColormapIndex = 3;

%% Completion du titre

P.DataType = cl_image.indDataType('Probability');
P = update_Name(P, 'Append', 'Proba from Phase/Amplitude');

flag = 1;
