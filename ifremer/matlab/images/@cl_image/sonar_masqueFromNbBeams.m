% Calcul d'un masque sur les donn�es douteuses d�duites d'une analyse du
% mode de d�tection Amplitude/Phase
%
% Syntax
%   [b, c] = sonar_masqueFromNbBeams(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   SeuilProba : Seuil de d�tection des sones erron�es (0.5 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_masqueFromNbBeams(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_masqueFromNbBeams(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    flag = 0;
    return
end

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, SeuilNbBeams] = getPropertyValue(varargin, 'SeuilNbBeams', Inf); %#ok<ASGLU>


%% Controles

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Calcul du nombre de faisceaux valides par ping

[~, NbPix] = mean_lig(this, 'suby', suby, 'TypeCurve', 'NbPixels');

%% Calcul du masque et de la probabilit�

flagPing = NbPix >= SeuilNbBeams;
Masque = repmat(flagPing, 1, this.nbColumns);
this = replace_Image(this, Masque);
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, 1:this.nbColumns, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', 'MasqueFromNbOfValidBeams');
