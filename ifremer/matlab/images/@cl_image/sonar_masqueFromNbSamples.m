% Calcul d'un masque sur les donn�es douteuses d�duite d'une analyse du
% mode de d�tection en amplitude : Nombre d'�chantillon
%
% Syntax
%   [b, c] = sonar_masqueFromNbSamples(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   SeuilHaut  : Seuil de d�tection par valeurs �lev�s des zones erron�es (0.9 de la valeur max. par d�faut)
%   SeuilBas   : Seuil de d�tection par valeurs basses des zones erron�es (4 �chantillons par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_masqueFromNbSamples(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this, P] = sonar_masqueFromNbSamples(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    P = [];
    flag = 0;
    return
end

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, SeuilHaut] = getPropertyValue(varargin, 'SeuilHaut', 0.9);
[varargin, SeuilBas]  = getPropertyValue(varargin, 'SeuilBas', 4); %#ok<ASGLU>

%% Calcul du masque

ImAmpNbSamples= this.Image;
% subNaN = find(isnan(ImAmpNbSamples));

MasqueHaut = (ImAmpNbSamples > (((max(this)- min(this))*SeuilHaut)+ min(this)));
MasqueBas = (ImAmpNbSamples < SeuilBas);
Masque = MasqueHaut | MasqueBas ;
Masque = single(Masque);

% Masque(find(Masque==0))= NaN;
Masque(Masque == 0)= NaN;

this = replace_Image(this, Masque);
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, 1:this.nbColumns, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', 'Masque from Phase/Variance');

P = this;
flag = 0;
