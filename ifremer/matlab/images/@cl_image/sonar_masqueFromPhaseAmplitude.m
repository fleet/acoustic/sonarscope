% Calcul d'un masque sur les donn�es douteuses d�duite d'une analyse du
% mode de d�tection Amplitude/Phase
%
% Syntax
%   [b, c] = sonar_masqueFromPhaseAmplitude(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   SeuilProba : Seuil de d�tection des sones erron�es (0.5 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_masqueFromPhaseAmplitude(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_masqueFromPhaseAmplitude(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    flag = 0;
    return
end

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, SeuilProba] = getPropertyValue(varargin, 'SeuilProba', 0.5);
[varargin, Step]       = getPropertyValue(varargin, 'Step', 50); %#ok<ASGLU>

%% Contr�les

ident1 = cl_image.indDataType('DetectionType');
ident2 = cl_image.indDataType('KongsbergQualityFactor');
flag = testSignature(this, 'DataType', [ident1, ident2], 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end

%% Calcul du masque et de la probabilit�

Masque = NaN(length(suby), this.nbColumns, 'single');
Proba  = NaN(length(suby), this.nbColumns, 'single');

N = length(suby);
for k=1:Step:N
    sub = k:min(k+Step,N);
    TypeLayer = (this.DataType == ident2);
    [M, P] = cleanDetection(this.Image(suby(sub), :), 'TypeLayer', TypeLayer, 'SeuilProba', SeuilProba);
    
    Masque(sub,:) = M;
    Proba(sub,:)  = P;
end

this = replace_Image(this, Masque);
this.ValNaN = NaN;

%% Mise � jour de coordonnees

this = majCoordonnees(this, 1:this.nbColumns, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', '_MaskProbaAmp');

%% Image de la probabilite

this(2) = replace_Image(this, Proba);

%% Calcul des statistiques

this(2) = compute_stats(this(2));

%% Rehaussement de contraste

this(2).TagSynchroContrast = num2str(rand(1));
CLim = [this(2).StatValues.Min this(2).StatValues.Max];
this(2).CLim = CLim;
this(2).ColormapIndex = 3;

%% Compl�tion du titre

this(2).DataType = cl_image.indDataType('Probability');
this(2) = update_Name(this(2), 'Append', 'ProbaAmplitude');

this = this([2 1]);

