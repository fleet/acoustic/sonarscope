% Calcul d'un masque sur les donn�es douteuses d�duite d'une analyse du
% mode de d�tection Phase sur la variance
%
% Syntax
%   [b, c] = sonar_masqueFromPhaseVariance(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   Seuil      : Seuil de d�tection des sones erron�es (0.9 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_masqueFromPhaseVariance(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this, P] = sonar_masqueFromPhaseVariance(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    P = [];
    flag = 0;
    return
end

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Seuil] = getPropertyValue(varargin, 'Seuil', 0.9); %#ok<ASGLU>

% ---------
% Controles

% ident1 = cl_image.indDataType('DetectionType');
% ident2 = cl_image.indDataType('KongsbergQualityFactor');
% flag = testSignature(this, 'DataType', [ident1, ident2], 'GeometryType', cl_image.indGeometryType('PingBeam'));
% if ~flag
%     P = [];
%     return
% end

% -------------------------------------
% Calcul du masque

ImPhaseVar= this.Image;
% subNaN = find(isnan(ImPhaseVar));

Masque = (ImPhaseVar>( ((max(this)- min(this))*Seuil)+min(this)));
Masque = single(Masque);
% Masque(subNaN) = NaN;
% Masque(find(Masque==0))= NaN;

this = replace_Image(this, Masque);
this.ValNaN = NaN;

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, 1:this.nbColumns, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', 'Masque from Phase/Variance');


P = this;


