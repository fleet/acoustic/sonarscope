% Cr�ation d'un masque sur les faisceaux qui se recouvrent pour un syst�me
% dual
%
% Syntax
%   [b, c] = sonar_masqueRecoveringBeams(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   SeuilProba : Seuil de d�tection des sones erron�es (0.5 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_masqueRecoveringBeams(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this] = sonar_masqueRecoveringBeams(AcrossDist, varargin)

for k=1:length(AcrossDist)
    [flag, this(k)] = sonar_masqueRecoveringBeams_unitaire(AcrossDist(k), varargin{:}); %#ok<AGROW>
end


function [flag, this] = sonar_masqueRecoveringBeams_unitaire(AcrossDist, varargin)

[subx, suby, varargin] = getSubxSuby(AcrossDist, varargin);

[varargin, Angles] = getPropertyValue(varargin, 'Angles', []); %#ok<ASGLU>

%% Contr�les

identAcrossDist = cl_image.indDataType('AcrossDist');
flag = testSignature(AcrossDist, 'DataType', identAcrossDist, ...
    'GeometryType', [cl_image.indGeometryType('PingBeam') cl_image.indGeometryType('PingAcrossDist')]);
if ~flag
    return
end

%% Cr�ation du masque

Masque = true(length(suby), AcrossDist.nbColumns);

M = floor((AcrossDist.nbColumns)/2);
subBab = 1:M;
subTri = (M+1):AcrossDist.nbColumns;
AcrossDistImage = AcrossDist.Image(:,:);
for k=1:length(suby)
    %{
    da = diff(Angles.Image(suby(k), :));
    da(isnan(da)) = [];
%     if any(da < 0)  % Pas suffisant
    if any(da < -2) % TODO : � ajuster sur donn�es EM2002D de Marc
        x = AcrossDistImage(suby(k), :);
        sub1 = (x(subBab) > 0);
        sub2 = (x(subTri) < 0);
        sub1(find(sub1, 1,'first')) = 0;
        sub2(find(sub2, 1,'last')) = 0;
        Masque(k, [sub1 sub2]) = false;
    end
    Masque(k, xBabFirst:xTriFirst) = false;
    %}
    
    % Modif JMA le 09/06/2020
    xBab = AcrossDistImage(suby(k), subBab);
    xBabLast = xBab(find(~isnan(xBab), 1, 'last'));
    
    xTri = AcrossDistImage(suby(k), subTri);
    xTriFirst = xTri(find(~isnan(xTri), 1, 'first'));
    
    xMiddle = (xBabLast + xTriFirst) / 2;
    if ~isempty(xMiddle)
        xBabFirst = subBab(find(xBab > xMiddle, 1, 'first'));
        xTriFirst = subTri(find(xTri < xMiddle, 1, 'last'));
        %     Masque(k, xBabFirst:xTriFirst) = false;
        nbs2 = floor((xTriFirst - xBabFirst + 1) / 2);
        Masque(k, subBab((end-nbs2+1):end)) = false;
        Masque(k, subTri(1:nbs2)) = false;
    end
    
    %{
    xBab = Angles.Image(suby(k), subBab);
    xTri = Angles.Image(suby(k), subTri);
    xBabFirst = subBab(find(xBab > 0, 1, 'first'));
    xTriFirst = subTri(find(xTri < 0, 1, 'last'));
    Masque(k, xBabFirst:xTriFirst) = false;
    %}
end

%% Cas o� on aurait pass� les angles : Test pour am�liorer la fourniture de l'angle d'incidence pour l'EM3002D

%{
if ~isempty(Angles)
for k=1:length(suby)
A = Angles.Image(suby(k), :);
sub1 = (A(subBab) > 0);
sub2 = (A(subTri) < 0);
sub1(find(sub1, 1,'first')) = 0;
sub2(find(sub2, 1,'last')) = 0;
Masque(suby(k), [sub1 sub2]) = false;
end
end
%}

%% Cr�ation de l'instance

this = replace_Image(AcrossDist, Masque(:,subx));
this.ValNaN = NaN;

%% Mise a jour de coordonn�es

this = majCoordonnees(this, 1:this.nbColumns, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
this.CLim = [0 1];

%% Completion du titre

this.DataType = cl_image.indDataType('Mask');
this = update_Name(this, 'Append', 'MasqueFromNbOfValidBeams');
