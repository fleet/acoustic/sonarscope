function resol = sonar_meanDistPings(this, varargin)

[~, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

resol = mean(this.Sonar.DistPings(suby), 'omitnan');
resol = round(resol, 2, 'significant');
