% TODO : cette fonction devrait �tre renom�e en PingXxxx2LatLong
%
% Projection d'un nuage de points en grille geographique
%
% Syntax
%   [status, Mos] = sonar_mosaique(a, Lat, Lon, ...)
%
% Input Arguments
%   a   : instance de cl_image
%   Lat : instance de cl_image de DataType='Latitude'
%   Lon : instance de cl_image de DataType='Longitude'
%   resol : Resolution en m
%
% Name-Value Pair Arguments
%   subx   : subsampling in X
%   suby   : subsampling in Y
%   AcrossInterpolation : 1=Pas d'interpolation, 2=Interpolation lineaire,
%                         3=M�thode � d�velopper dans le futur
%   AlongInterpolation  : 1=Pas d'interpolation, 2=Interpolation lineaire
%
% Output Arguments
%   status : 1=Operation r�ussie, 0=operation non r�ussie
%   Mos    : Mosaique du layer principal
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   b = cl_car_ima(nomFic);
%   c = view(b, 'Sonar.Ident', 3);
%   a = get(c, 'Images');
%   imagesc(a)
%
%   [status, Mos] = sonar_mosaique(a(1), a(2), a(3), 20);
%   if status
%       imagesc(Mos)
%   end
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [status, Mos] = sonar_mosaique(this, Lat, Lon, resol, varargin)

warning('off', 'all');

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, AcrossInterpolation]   = getPropertyValue(varargin, 'AcrossInterpolation',   1);
[varargin, AlongInterpolation]    = getPropertyValue(varargin, 'AlongInterpolation',    1);
[varargin, SpecularInterpolation] = getPropertyValue(varargin, 'SpecularInterpolation', 0);
[varargin, PremiereAlerte]        = getPropertyValue(varargin, 'PremiereAlerte',        1);
[varargin, MethodeRemplissage]    = getPropertyValue(varargin, 'MethodeRemplissage',    1);
[varargin, NoWaitbar]             = getPropertyValue(varargin, 'NoWaitbar',             0);
[varargin, HeadingRx]             = getPropertyValue(varargin, 'HeadingRx',             []);
[varargin, flagTideCorrection]    = getPropertyValue(varargin, 'TideCorrection',        0); 
[varargin, LayerAngle]            = getPropertyValue(varargin, 'LayerAngle',            []); %#ok<ASGLU>

Mos = [];

%% Calcul des limites g�ographiques de la mosa�que

[Nx, Ny, resolLon, resolLat, x, y, LonWest, LonEst, LatNord, LatSud, message, ...
    subx, suby, subxLat, subyLat, subxLon, subyLon, status] = ...
    sonar_mosaique_size(this, Lat, Lon, resol, 'subx', subx, 'suby', suby); %#ok<ASGLU>
if ~status || isempty(Nx) || isempty(Ny) || (Nx == 0) || (Ny == 0)
    status = 0;
    return
end

% Faudrait faire mieux en cherchant le plus proche voisin
NMin = min([length(suby) length(subyLat) length(subyLon)]);
suby = suby(1:NMin);
subyLat = subyLat(1:NMin);
subyLon = subyLon(1:NMin);

% Faudrait faire mieux en cherchant le plus proche voisin
NMin = min([length(subx) length(subxLon) length(subxLon)]);
subx = subx(1:NMin);
subxLat = subxLat(1:NMin);
subxLon = subxLon(1:NMin);

%% D�termination du type d'interpolation en fonction dy type de l'image

switch this.ImageType
    case 3  % Indexee
        flagRoundAlongInterpolation = 1;
    case 4  % Binaire
        flagRoundAlongInterpolation = 1;
    otherwise % 1:Intensite, 2:RGB
        switch this.DataType
            case cl_image.indDataType('AveragedPtsNb')
                flagRoundAlongInterpolation = 1;
            case cl_image.indDataType('Mask')
                flagRoundAlongInterpolation = 1;
            case cl_image.indDataType('Texture')
                flagRoundAlongInterpolation = 1;
            case cl_image.indDataType('RxTime')
                flagRoundAlongInterpolation = 1;
            otherwise
                flagRoundAlongInterpolation = 0;
        end
end

switch this.DataType
    case cl_image.indDataType('IncidenceAngle')
        flagIncidenceAngle = 1; % On interpole la valeur sur la valeur absolue et on resigne la valeur � partir de l'angle de d�part
    otherwise
        flagIncidenceAngle = 0;
end

switch this.DataType
    case cl_image.indDataType('Segmentation')
        methodInterpAcrossDist = 'nearest';
        MethodeRemplissage = 4;
    case cl_image.indDataType('AbsorptionCoeff')
        methodInterpAcrossDist = 'nearest';
        MethodeRemplissage = 4;
    case cl_image.indDataType('DetectionType')
        methodInterpAcrossDist = 'nearest';
        MethodeRemplissage = 4;
    otherwise
        methodInterpAcrossDist = 'linear';
end

%% Projection de l'image sur la mosa�que

thisImage = this.Image(:,:,:);
LatImage = Lat.Image(:,:);
LonImage = Lon.Image(:,:);

TypeVar = class(thisImage(1));
if isa(TypeVar, 'uint8')
    ConvertInSingle = 1;
    TypeVar = 'single';
else
    ConvertInSingle = 0;
end

switch MethodeRemplissage
    case 1 % 'Mean'
        ValInit = 0;
    case 4 % 'Median' Modif GLA
        ValInit = [];
    otherwise % 2:'Max', 3:'Min'
        ValInit = NaN;
end

% TODO : l'interpolation par valeur m�diane n'est pas optimale car Matlab
% rens la valeur moyenne dans certains cas (Ex : median(1:4) = 2.5). Il
% faudrait faire notre propre fonction qu rend la valeur non moyenn�e
% https://books.google.fr/books?id=N_RhYBhzd-cC&pg=PA245&lpg=PA245&dq=matlab+median+middle&source=bl&ots=0efSkq1Ips&sig=eLwFrpdeHXCPqUfi-d64uRJFNTA&hl=fr&sa=X&ved=0ahUKEwiF4d2LrePMAhWB0RoKHferDLQQ6AEIZjAJ#v=onepage&q=matlab%20median%20middle&f=false

nbSlides = this.nbSlides;
try
    if ValInit == 0
        I = zeros(Ny, Nx, nbSlides, TypeVar);
        N = zeros(Ny, Nx, nbSlides, TypeVar);
    elseif isempty(ValInit) % Modif GLA
%         I = cell(Ny, Nx);   % Tableau de cellules, pas de possibilit� de sp�cifier le type
%         I = cellfun(@single, I, 'Uniformoutput', false); % Conversion en single des tableaux vides de chaque cellule

        % Modif JMA le 27/04/2018 car la machinerie cellfun prend �norm�ment de temps
        I = NaN(Ny, Nx, nbSlides, TypeVar);
        N = NaN(Ny, Nx, nbSlides, TypeVar);
    else
        I = NaN(Ny, Nx, nbSlides, TypeVar);
        N = NaN(Ny, Nx, nbSlides, TypeVar);
    end
catch %#ok<CTCH>
    I = cl_memmapfile('Value', ValInit, 'Size', [Ny, Nx, nbSlides], 'Format', TypeVar);
    N = cl_memmapfile('Value', ValInit, 'Size', [Ny, Nx, nbSlides], 'Format', TypeVar);
end

str = this.Name;
str(length(str)+1:150) = '.';

% AllFrequencies = unique(this.Sonar.SonarFrequency);

if this.Sonar.BathymetryStatus.TideApplied
    flagTideCorrection = 0;
    flagTideApplied    = 1;
else
    DT = cl_image.indDataType('Bathymetry');
    flagTideCorrection = flagTideCorrection && (this.DataType == DT) && isfield(this.Sonar, 'Tide') && ~isempty(this.Sonar.Tide);
    flagTideApplied = flagTideCorrection;
end

DT = cl_image.indDataType('Reflectivity');
flagComputeMeanValInValNat = (this.DataType == DT) && (MethodeRemplissage == 1);
% flagComputeMeanValInValNat = 0;

nbPings = length(suby);
for iCan=1:nbSlides
    hw = create_waitbar(str, 'Entete', 'IFREMER - SonarScope : Processing Mosaic', 'NoWaitbar', NoWaitbar, 'N', nbPings);
    %     set(get(get(hw, 'children'), 'Title'), 'Interpreter', 'none')
    for kPing=1:nbPings
        my_waitbar(kPing, nbPings, hw);
        
        Ligne_Val = thisImage(suby(kPing), subx, iCan);
        if ~isempty(LayerAngle)
            Ligne_Angle = LayerAngle.Image(suby(kPing), subx);
        end
        
        if flagTideCorrection
            Ligne_Val = Ligne_Val + double(this.Sonar.Tide(kPing));
        end
        
        if ConvertInSingle
            Ligne_Val = single(Ligne_Val);
        end
        
        Ligne_Lat = LatImage(subyLat(kPing), subxLat);
        Ligne_Lon = LonImage(subyLon(kPing), subxLon);
        Ligne_sub = ~isnan(Ligne_Lon) & ~isnan(Ligne_Lat) & ~isnan(Ligne_Val);
        
        if sum(Ligne_sub(:)) <= 1
            continue
        end
        
        Ligne_Val = Ligne_Val(Ligne_sub);
        if ~isempty(LayerAngle)
            Ligne_Angle = Ligne_Angle(Ligne_sub);
        end
        
        if ~isempty(HeadingRx)
            Ligne_HeadingRx = HeadingRx(suby(kPing), subx);
            Ligne_HeadingRx = Ligne_HeadingRx(Ligne_sub);
        end
        
        Ligne_x = (Ligne_Lon(Ligne_sub) - LonWest) / resolLon;
        Ligne_y = (Ligne_Lat(Ligne_sub) - LatSud)  / resolLat;
        
        % -----------------------------------------
        % Detection si la navigation est quantifi�e
        
        pppp = sort(abs(diff(Ligne_x))); % Rajout� le 18/12/2010 pour le Geoswath
        deltax = pppp(floor(0.75*length(Ligne_x)));
        %         pppp = sort(diff(Ligne_y));
        pppp = sort(abs(diff(Ligne_y))); % Rajout� le 18/12/2010 pour le Geoswath
        deltay = pppp(floor(0.75*length(Ligne_y)));
        
        if PremiereAlerte && ((deltax == 0) || (deltay == 0))
            str1 = 'Il semble que la latitude et la longitude soient quantifi�es. Je continue le traitement mais si vous apercevez des choses �tranges rappelez vous que cela peut venir de la navigation.';
            str2 = 'It seems your latitude and longitude are quantified. I continue processing but if you see strange things on your image keep in mind it can be due to this point.';
            my_warndlg(Lang(str1,str2),  0, 'Tag', 'DataQuantified');
            PremiereAlerte = 0;
        end
        
        deltaxy = sqrt(deltax^2 + deltay^2);    % Si > 1 on doit faire une interpolation
        if PremiereAlerte && (deltaxy == 0)
            str1 = 'Il semble que la latitude et la longitude soient quantifi�es. Je continue le traitement mais si vous apercevez des choses �tranges rappelez vous que cela peut venir de la navigation.';
            str2 = 'It seems your latitude and longitude are quantified. I continue processing but if you see strange things on your image keep in mind it can be due to this point.';
            my_warndlg(Lang(str1,str2),  0, 'Tag', 'DataQuantified');
            PremiereAlerte = 0;
        end
        
        % ---------------------------------------------------------
        % Test si on arrive � d�tecter des �paves sur un crit�re de
        % continuit� des distances transversales
        
        if AcrossInterpolation <= 2
            TestAcrossInterpolation = AcrossInterpolation;
        else
            TestAcrossInterpolation = 2;
            Signe = sign(diff(Ligne_x));
            subSignePos = sum(Signe > 0);
            subSigneNeg = sum(Signe < 0);
            if (subSignePos ~= 0) && (subSigneNeg ~= 0)
                MaxSigne = max(subSignePos, subSigneNeg);
                MinSigne = min(subSignePos, subSigneNeg);
                Rapport = (MaxSigne - MinSigne) / (MaxSigne + MinSigne);
                if Rapport < 0.97
                    t = this.Sonar.Time(suby(kPing));
                    fprintf('Objet detected on %s line %d : %s\n', this.Name, kPing, t2str(t));
                    TestAcrossInterpolation = 1;
                end
            end
        end
        
        switch TestAcrossInterpolation
            case 2 % Interpolation lin�aire
                if deltaxy > 0.5 % Interpolation % Modifi� le 27/02/2011
                    NPoints = length(Ligne_x);
                    %                     NPointsInterpoles = floor(1.5 * deltaxy * NPoints);
                    NPointsInterpoles = floor(4 * deltaxy * length(subx));
                    newx = linspace(1,NPoints, NPointsInterpoles);
                    UnANPoints = 1:NPoints;
                    Ligne_x = my_interp1_NoSort(UnANPoints, Ligne_x, newx);
                    Ligne_y = my_interp1_NoSort(UnANPoints, Ligne_y, newx);
                    if flagIncidenceAngle
                        % On interpole la valeur sur la valeur absolue et on resigne la valeur � partir de l'angle de d�part
                        Sign_Val  = sign(Ligne_Val);
                        Ligne_Val = my_interp1_NoSort(UnANPoints, abs(Ligne_Val), newx);
                        Sign_Val  = my_interp1_NoSort(UnANPoints, Sign_Val, newx, 'nearest');
                        Ligne_Val = Ligne_Val .* Sign_Val;
                        
                        if ~isempty(LayerAngle)
                            Ligne_Angle = my_interp1_NoSort(UnANPoints, Ligne_Angle, newx);
                        end
                    else
                        Ligne_Val = my_interp1_NoSort(UnANPoints, Ligne_Val, newx, methodInterpAcrossDist);% , 'spline'); % A TESTER sur une image presentant des dunes
                        if ~isempty(LayerAngle)
                            Ligne_Angle = my_interp1_NoSort(UnANPoints, Ligne_Angle, newx, methodInterpAcrossDist);% , 'spline'); % A TESTER sur une image presentant des dunes
                        end
                    end
                    if ~isempty(HeadingRx)
                        Ligne_HeadingRx = my_interp1_NoSort(UnANPoints, Ligne_HeadingRx, newx);
                    end
                    clear UnANPoints
                else
                    FacteurReduction = floor(0.5 / deltaxy);
                    if FacteurReduction > 1
                        Ligne_x = downsize_vector(Ligne_x, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        if isempty(Ligne_x)
                            %                             FacteurReduction = 0;
                            continue
                        end
                        Ligne_y   = downsize_vector(Ligne_y,   'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        Ligne_Val = downsize_vector(Ligne_Val, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        if ~isempty(LayerAngle)
                        	Ligne_Angle = downsize_vector(Ligne_Angle, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        end

                        if ~isempty(HeadingRx)
                            Ligne_HeadingRx =  downsize_vector(Ligne_HeadingRx, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        end
                    end
                end
                
            case 3  % Filtrage
                disp('Pas encore au point')
                return
            otherwise % Pas d'interpolation
        end
        if isempty(Ligne_x)
            continue
        end
        
        Ligne_x = 1 + floor(Ligne_x);
        Ligne_y = 1 + floor(Ligne_y);
         
        if SpecularInterpolation
            [Ligne_Val, Ligne_x, Ligne_y] = RDF_PingSpecularInterpolation(Ligne_Val, Ligne_x, Ligne_y);
            Ligne_x = round(Ligne_x);
            Ligne_y = round(Ligne_y);
        end
       
        if nbSlides == 1
            sub = sub2ind([Ny Nx], Ligne_y, Ligne_x);
        else
            sub = sub2ind([Ny Nx nbSlides], Ligne_y, Ligne_x, iCan*ones(size(Ligne_x)));
        end
        
        if flagComputeMeanValInValNat
            Ligne_Val = reflec_dB2Amp(Ligne_Val);
        end

        switch MethodeRemplissage
            case 1 % 'Mean'
                for k=1:length(Ligne_Val)
                    subk = sub(k);
                    I(subk) = I(subk) + Ligne_Val(k);
                    N(subk) = N(subk) + 1;
                end
            case 2 % 'Max'
                I(sub) = max(I(sub), Ligne_Val);
                N(sub) = 1;
            case 3 % 'Min'
                I(sub) = min(I(sub), Ligne_Val);
                N(sub) = 1;
            case 4 % 'Median' % l'essai GLA : op�rateur non lin�aire -> il faut garder toutes les sondes !!!!
%                 Ligne_Val     = num2cell(Ligne_Val); % conversion en tableau de cellules
%                 Ligne_Val_Cat = cellfun(@(a,b) [a, b], I(sub), Ligne_Val, 'UniformOutput', false);  % Concatenation des valeurs dans les cellules
%                 [I{sub}]      = deal(Ligne_Val_Cat{:}); % Mise � jour des cellules dans le tableau g�n�ral

                % Modif JMA le 27/04/2018 car la machinerie cellfun prend �norm�ment de temps
                subNonNaN = ~isnan(Ligne_Val);
                I(sub(subNonNaN)) = Ligne_Val(subNonNaN);
        end
    end
    my_close(hw)
end

switch MethodeRemplissage % Modif GLT 04/02/2011
    case 4 % Mediane
%         I = cellfun(@median, I, 'UniformOutput', false); % En sortie on obtient une matrice
%         I = cellfun(@single, I);
                
        % Modif JMA le 27/04/2018 car la machinerie cellfun prend �norm�ment de temps
%         I = I;
        
    otherwise
        subNonNan = (N ~= 0);
        if ~isa(I, 'logical')
            I(subNonNan) = I(subNonNan) ./ N(subNonNan);
            I(~subNonNan) = NaN;
        end
        clear subNonNan
        clear N
end

%% Interpolation dans le sens longitudinal

% figure; grid on; hold on
switch AlongInterpolation
    case 2  % Interpolation lineaire
        for iCan=1:nbSlides
            hw = create_waitbar(this.Name, 'Entete', 'IFREMER - SonarScope : Processing Interpolation', 'NoWaitbar', NoWaitbar, 'N', nbPings);
            %             set(get(get(hw, 'children'), 'Title'), 'Interpreter', 'none')
            for kPing=2:nbPings
                my_waitbar(kPing,nbPings,hw);
                
                k = kPing-1;
                Ligne_Lat = LatImage(subyLat(k), subxLat);
                Ligne_Lon = LonImage(subyLon(k), subxLon);
                Ligne_sub = find(~isnan(Ligne_Lon));
                if length(Ligne_sub) <= 1
                    continue
                end
                Ligne_x1 = (Ligne_Lon(Ligne_sub([1 end])) - LonWest) / resolLon;
                Ligne_y1 = (Ligne_Lat(Ligne_sub([1 end])) - LatSud)  / resolLat;
                
                Ligne_Val1 = thisImage(suby(k), subx, iCan);
                Ligne_sub  = ~isnan(Ligne_Lon);
                Ligne_Val1 = Ligne_Val1(Ligne_sub);
                
                if ConvertInSingle
                    Ligne_Val1 = single(Ligne_Val1);
                end
                
                %             if all(isnan(Ligne_Val1))
                %                 continue
                %             end
                subNonNaN = ~isnan(Ligne_Val1);
                if sum(subNonNaN(:)) <= 1
                    continue
                end
                clear subNonNaN
                
                k = kPing;
                Ligne_Lat = LatImage(subyLat(k), subxLat);
                Ligne_Lon = LonImage(subyLon(k), subxLon);
                Ligne_sub = find(~isnan(Ligne_Lon));
                if isempty(Ligne_sub)
                    continue
                end
                Ligne_x2 = (Ligne_Lon(Ligne_sub([1 end])) - LonWest) / resolLon;
                Ligne_y2 = (Ligne_Lat(Ligne_sub([1 end])) - LatSud)  / resolLat;
                
                Ligne_Val2 = thisImage(suby(k), subx, iCan);
                Ligne_sub  = ~isnan(Ligne_Lon);
                Ligne_Val2 = Ligne_Val2(Ligne_sub);
                
                if ConvertInSingle
                    Ligne_Val2 = single(Ligne_Val2);
                end
                
                %             if all(isnan(Ligne_Val2))
                %                 continue
                %             end
                subNonNaN = ~isnan(Ligne_Val2);
                if sum(subNonNaN(:)) <= 1
                    continue
                end
                clear subNonNaN
                
                delta_x1x2 = Ligne_x2 - Ligne_x1;
                delta_y1y2 = Ligne_y2 - Ligne_y1;
                dmax = ceil(1.5 * max(sqrt(delta_x1x2 .^ 2 + delta_y1y2 .^ 2)));
                
                %             plot(Ligne_x1, Ligne_y1, 'r*', Ligne_x2, Ligne_y2, '+r'); grid on
                for k=1:dmax-1
                    Ligne_x1x2 = Ligne_x1 + k * delta_x1x2 / dmax;
                    Ligne_y1y2 = Ligne_y1 + k * delta_y1y2 / dmax;
                    %                 plot(Ligne_x1x2, Ligne_y1y2, 'xb');
                    
                    NPointsInterpoles = ceil(1.5 * sqrt(diff(Ligne_x1) .^ 2 + diff(Ligne_y1) .^ 2));
                    Ligne_x1x2 = linspace(Ligne_x1x2(1), Ligne_x1x2(2), NPointsInterpoles);
                    Ligne_y1y2 = linspace(Ligne_y1y2(1), Ligne_y1y2(2), NPointsInterpoles);
                    Ligne_x = 1 + floor(Ligne_x1x2);
                    Ligne_y = 1 + floor(Ligne_y1y2);
                    
                    if flagRoundAlongInterpolation
                        alpha1 = round(k / dmax);
                    else
                        alpha1 = k / dmax;
                    end
                    
                    %                 sub = sub2ind([Ny Nx], Ligne_y, Ligne_x);
                    sub = sub2ind([Ny Nx nbSlides], Ligne_y, Ligne_x, iCan*ones(size(Ligne_x)));
                    
                    NPoints = length(Ligne_Val1);
                    newx = linspace(1, NPoints, length(sub));
                    Ligne_Val1 = my_interp1_NoSort(1:NPoints, Ligne_Val1, newx);
                    
                    NPoints = length(Ligne_Val2);
                    newx = linspace(1, NPoints, length(sub));
                    Ligne_Val2 = my_interp1_NoSort(1:NPoints, Ligne_Val2, newx);
                    
                    subVide = isnan(I(sub));
                    I(sub(subVide)) = Ligne_Val1(subVide) * (1 -alpha1) + Ligne_Val2(subVide) * alpha1;
                    clear subVide
                    
                    subNonNaN = ~isnan(Ligne_Val1);
                    if sum(subNonNaN(:)) <= 1
                        break
                    end
                    subNonNaN = ~isnan(Ligne_Val2);
                    if sum(subNonNaN(:)) <= 1
                        break
                    end
                    clear subNonNaN
                end
            end

            my_close(hw, 'MsgEnd')
            drawnow
        end

    otherwise % Pas d'interpolation
end

if flagComputeMeanValInValNat
    I = reflec_Amp2dB(I);
end

if ConvertInSingle
    I(isnan(I)) = this.ValNaN;
    I = feval(class(thisImage(1)), I);
elseif strcmp(class(thisImage(1)), 'uint8') %#ok<STISA>
    I(isnan(I)) = 0;
    I = uint8(I);
end

ImageName         = this.Name;
Unit              = this.Unit;
DataType          = this.DataType;
ColormapIndex     = this.ColormapIndex;
InitialFileName   = this.InitialFileName;
InitialFileFormat = this.InitialFileFormat;
InitialImageName  = this.InitialImageName;
Comments          = this.Comments;
Carto             = this.Carto;
SonarDescription  = this.Sonar.Desciption;

Mos = cl_image('Image', I, 'Name', ImageName, 'ColormapIndex', ColormapIndex, ...
    'x', x, 'y', y, 'XLabel', 'Longitude', 'YLabel', 'Latitude', ...
    'XUnit', 'deg', 'YUnit', 'deg', 'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
    'GeometryType', cl_image.indGeometryType('LatLong')', 'DataType',  DataType, 'Unit', Unit, ...
    'InitialFileName', InitialFileName, 'InitialFileFormat', InitialFileFormat, ...
    'InitialImageName', InitialImageName, 'Comments', Comments, ...
    'Carto', Carto, 'ValNaN', this.ValNaN', ...
    'SonarDescription', SonarDescription);

Mos.CourbesStatistiques = this.CourbesStatistiques;
Mos.Sonar.TVG      = this.Sonar.TVG;
Mos.Sonar.DiagEmi  = this.Sonar.DiagEmi;
Mos.Sonar.DiagRec  = this.Sonar.DiagRec;
Mos.Sonar.AireInso = this.Sonar.AireInso;
Mos.Sonar.NE       = this.Sonar.NE;
Mos.Sonar.SH       = this.Sonar.SH;
Mos.Sonar.GT       = this.Sonar.GT;
Mos.Sonar.BS       = this.Sonar.BS;
Mos.Sonar.Ship     = this.Sonar.Ship;

if strcmp(get_DataTypeName(Mos), 'Bathymetry')
    Mos.Sonar.BathymetryStatus.TideApplied = flagTideApplied;
end

Mos = update_Name(Mos);

%% Apparemment tout s'est bien pass�

status = 1;
warning('on', 'all');
