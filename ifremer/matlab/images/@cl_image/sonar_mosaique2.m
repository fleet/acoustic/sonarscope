% Projection d'un nuage de points en grille geographique
%
% Syntax
%   [flag, b, c] = sonar_mosaique2(a, A, Lat, Lon, ...)
%
% Input Arguments
%   a : instance de cl_image
%   R : instance de cl_image contenant l'angle d'�mission
%   Lat : instance de cl_image de DataType='Latitude'
%   Lon : instance de cl_image de DataType='Longitude'
%   resol : Resolution en m
%
% Name-Value Pair Arguments
%   subx   : subsampling in X
%   suby   : subsampling in Y
%   AcrossInterpolation : 1=Pas d'interpolation, 2=Interpolation lineaire,
%                         3=M�thode � d�velopper dans le futur
%   AlongInterpolation  : 1=Pas d'interpolation, 2=Interpolation lineaire
%
% Output Arguments
%   b      : instance de cl_image contenant a mosaique
%   c      : instance de cl_image contenant l'angle mosaique
%   status : 1=Operation reussie, 0=operation non reussie
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   b = cl_car_ima(nomFic);
%   c = view(b, 'Sonar.Ident', 3);
%   a = get(c, 'Images');
%   imagesc(a)
%
%   [status, MosR, MosA] = sonar_mosaique2(a(1), a(2), a(3), a(4), 20);
%   [status, MosR, MosA] = sonar_mosaique2(a(1), a(2), a(3), a(4), 20);
%   imagesc(MosR)
%   imagesc(MosA)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [status, this, that] = sonar_mosaique2(this, A, Lat, Lon, resol, varargin)

that = [];
warning('off', 'all');

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, AcrossInterpolation]   = getPropertyValue(varargin, 'AcrossInterpolation',   1);
[varargin, AlongInterpolation]    = getPropertyValue(varargin, 'AlongInterpolation',    1);
[varargin, PremiereAlerte]        = getPropertyValue(varargin, 'PremiereAlerte',        1);
[varargin, SpecularInterpolation] = getPropertyValue(varargin, 'SpecularInterpolation', 0);

[varargin, NoWaitbar] = getFlag(varargin, 'NoWaitbar'); %#ok<ASGLU>

%% Intersection entre l'image a mosaiquer et l'image d'angle

[~, indMilieu] = sort(abs(this.x(subx)));
indMilieu = indMilieu(1);
[~, indMilieuA] = sort(abs(A.x));
indMilieuA = indMilieuA(1);
decalage = indMilieuA - indMilieu;

subxA = subx + decalage;
subOut = ((subxA >= 1) & (subxA <= A.nbColumns));
subxA = subxA(subOut);
subx = subx(subOut);
clear subOut
%figure; plot(this.x(subx), '+r'); grid on; hold on; plot(A.x(subxA), '+b');

%% Calcul des limites geographiques de la mosaique

[Nx, Ny, resolLon, resolLat, x, y, LonWest, LonEst, LatNord, LatSud, message, ...
    subx, suby, subxLat, subyLat, subxLon, subyLon, status] = ...
    sonar_mosaique_size(this, Lat, Lon, resol, 'subx', subx, 'suby', suby); %#ok<ASGLU>
if ~status
    return
end
if isempty(Nx)
    return
end

% TEMPORAIRE POUR CORRIGER BUG CONCARNEAU
if all(isnan(this.Sonar.PingCounter(suby))) || all(this.Sonar.PingCounter(suby) == 0)
    this.Sonar.PingCounter(suby)=1:length(suby);
    A.Sonar.PingCounter=1:length(suby);
    Lat.Sonar.PingCounter=1:length(suby);
end
% TEMPORAIRE POUR CORRIGER BUG CONCARNEAU


[subysuby, subyA, subyLon] = intersect3(this.Sonar.PingCounter(suby,1), ...
    A.Sonar.PingCounter(:,1), Lat.Sonar.PingCounter(:,1));
suby = suby(subysuby);
subyLat = subyLon;

% TEMPORAIRE POUR CORRIGER BUG 2040 Thalia (Y.Ladroit)a revoir
% subyA=1:length(suby);
% subyLon=1:length(suby);
% subyLat = subyLon;
% TEMPORAIRE POUR CORRIGER BUG 2040 Thalia (Y.Ladroit)

% % Faudrait faire mieux en cherchant le plus proche voisin
% NMin = min([length(suby) length(subyLat) length(subyLon)]);
% suby = suby(1:NMin);
% subyLat = subyLat(1:NMin);
% subyLon = subyLon(1:NMin);

% Vaudrait faire mieux en cherchant le plus proche voisin
NMin = min([length(subx) length(subxLon) length(subxLon)]);
subx    = subx(1:NMin);
subxA   = subxA(1:NMin);
subxLat = subxLat(1:NMin);
subxLon = subxLon(1:NMin);

%% Determination du type d'interpolation en fonction dy type de l'image

switch this.ImageType
    case 1  % Intensite
        InterpolationMethod(1) = 1;
    case 2  % RGB
        InterpolationMethod(1) = 1;
    case 3  % Indexee
        InterpolationMethod(1) = 0;
    case 4  % Binaire
        InterpolationMethod(1) = 0;
end
InterpolationMethod(2) = 1;

switch this.DataType
    case cl_image.indDataType('AveragedPtsNb')
        InterpolationMethod(1) = 0;
    case cl_image.indDataType('Mask')
        InterpolationMethod(1) = 0;
    case cl_image.indDataType('Texture')
        InterpolationMethod(1) = 0;
    case cl_image.indDataType('RxTime')
        InterpolationMethod(1) = 0;
end

%% Projection de l'image sur la mosaique

TypeVar = class(this.Image(1));
if strcmp(TypeVar, 'uint8') %#ok<STISA>
    ConvertInSingle = 1;
    TypeVar = 'single';
else
    ConvertInSingle = 0;
end

nbSlides = this.nbSlides;
try
    I = NaN(Ny, Nx, nbSlides, TypeVar);
    J = NaN(Ny, Nx, nbSlides, 'single');
catch %#ok<CTCH>
    I = cl_memmapfile('Value', NaN, 'Size', [Ny, Nx, nbSlides], 'Format', TypeVar);
    J = cl_memmapfile('Value', NaN, 'Size', [Ny, Nx], 'Format', 'single');
end

str = this.Name;
str(length(str)+1:150) = '.'; %#ok<NASGU>

M = length(suby);
for iCan=1:nbSlides
    str1 = 'Calcul de la mosa�que';
    str2 = 'Computing mosaic';
    strLoop = Lang(str1,str2);
    for k=1:M
        if ~NoWaitbar
            infoLoop(strLoop, k, M, 'pings')
        end
        
        Ligne_Val = this.Image(suby(k), subx, iCan);
        Ligne_A   = single(A.Image(subyA(k), subxA));
        %         if ~(this.y(2) > this.y(1)) % ~pour faciliter la recherche de this.y(2) > this.y(1)
        %         if sign(this.y(2)-this.y(1)) ~= sign(Lat.y(2)-Lat.y(1))
        %             Ligne_Val = fliplr(Ligne_Val);
        %             Ligne_A = fliplr(Ligne_A);
        %         end
        
        if ConvertInSingle
            Ligne_Val = single(Ligne_Val);
        end
        
        Ligne_Lat = Lat.Image(subyLat(k), subxLat);
        Ligne_Lon = Lon.Image(subyLon(k), subxLon);
        
        if SpecularInterpolation
            [Ligne_Val, Ligne_A, Ligne_Lat, Ligne_Lon] = RDF_PingSpecularInterpolation(Ligne_Val, Ligne_A, Ligne_Lat, Ligne_Lon);
        end

        
        Ligne_sub = ~isnan(Ligne_Lon) & ~isnan(Ligne_Lat) & ~isnan(Ligne_Val) & ~isnan(Ligne_A);
        
        %         if isempty(Ligne_sub)
        %             continue
        %         end
        
        Ligne_Val = Ligne_Val(Ligne_sub);
        %         if sum(~isnan(Ligne_Val)) <= 1
        %             continue
        %         end
        Ligne_A = Ligne_A(Ligne_sub);
        %         if  sum(~isnan(Ligne_A)) <= 1
        %             continue
        %         end
        
        Ligne_x = (Ligne_Lon(Ligne_sub) - LonWest) / resolLon;
        Ligne_y = (Ligne_Lat(Ligne_sub) - LatSud)  / resolLat;
        
        % -----------------------------------------
        % Detection si la navigation est quantifi�e
        
        deltax = median(diff(Ligne_x));
        deltay = median(diff(Ligne_y));
        if PremiereAlerte && ((deltax == 0) || (deltay == 0))
            str1 = 'Il semble que la latitude et la longitude soient quantifi�es. Je continue le traitement mais si vous apercevez des choses �tranges rappelez vous que cela peut venir de la navigation.';
            str2 = 'It seems your latitude and longitude are quantified. I continue processing but if you see strange things on your image keep in mind it can be due to this point.';
            my_warndlg(Lang(str1,str2),  0, 'Tag', 'DataQuantified');
            PremiereAlerte = 0;
        end
        
        % %         deltax = mean(diff(Ligne_x));
        % %         deltay = mean(diff(Ligne_y));
        %         deltax = median(diff(Ligne_x)); % Fevrier 2006 Image RESON pourrie
        %         deltay = median(diff(Ligne_y));
        deltaxy = sqrt(deltax^2 + deltay^2);    % Si > 1 on doit faire une interpolation
        if PremiereAlerte && (deltaxy == 0)
            str1 = 'Il semble que la latitude et la longitude soient quantifi�es. Je continue le traitement mais si vous apercevez des choses �tranges rappelez vous que cela peut venir de la navigation.';
            str2 = 'It seems your latitude and longitude are quantified. I continue processing but if you see strange things on your image keep in mind it can be due to this point.';
            my_warndlg(Lang(str1,str2),  0, 'Tag', 'DataQuantified');
            PremiereAlerte = 0;
        end
        
        
        % ---------------------------------------------------------
        % Test si on arrive � d�tecter des �paves sur un crit�re de
        % continuit� des distances transversales
        
        if AcrossInterpolation <= 2
            TestAcrossInterpolation = AcrossInterpolation;
        else
            TestAcrossInterpolation = 2;
            Signe = sign(diff(Ligne_x));
            subSignePos = sum(Signe > 0);
            subSigneNeg = sum(Signe < 0);
            if (subSignePos ~= 0) && (subSigneNeg ~= 0)
                MaxSigne = max(subSignePos, subSigneNeg);
                MinSigne = min(subSignePos, subSigneNeg);
                Rapport = (MaxSigne - MinSigne) / (MaxSigne + MinSigne);
                if Rapport < 0.97
                    t = this.Sonar.Time(suby(k));
                    fprintf('Objet detected on %s line %d : %s\n', this.Name, k, t2str(t));
                    TestAcrossInterpolation = 1;
                end
            end
        end
        
        switch TestAcrossInterpolation
            case 2  % Interpolation lineaire
                %               if deltaxy > 1 % Interpolation
                if deltaxy > 0.5 % Interpolation % Modifi� le 27/02/2011
                    NPoints = length(Ligne_x);
                    NPointsInterpoles = floor(1.5 * deltaxy * NPoints);
                    newx = linspace(1,NPoints, NPointsInterpoles);
                    UnaNPoints = 1:NPoints;
                    Ligne_x = my_interp1_NoSort(UnaNPoints, Ligne_x, newx, 'linear');
                    
                    Ligne_y   = my_interp1_NoSort(UnaNPoints, Ligne_y,   newx, 'linear');
                    
                    Ligne_Val = my_interp1_NoSort(UnaNPoints, Ligne_Val, newx, 'linear');% , 'spline'); % A TESTER sur une image presentant des dunes
                    
                    Ligne_A   = my_interp1_NoSort(UnaNPoints, Ligne_A,   newx, 'linear');% , 'spline'); % A TESTER sur une image presentant des dunes
                    clear UnaNPoints
                else
                    FacteurReduction = floor(0.5 / deltaxy);
                    Ligne_xOrigin = Ligne_x;
                    Ligne_yOrigin = Ligne_y;
                    Ligne_ValOrigin = Ligne_Val;
                    Ligne_AOrigin = Ligne_A;
                    while FacteurReduction > 1
                        Ligne_x = downsize(Ligne_xOrigin, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        if isempty(Ligne_x)
                            FacteurReduction = 0;
                            continue
                        end
                        Ligne_y   = downsize(Ligne_yOrigin,   'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        Ligne_Val = downsize(Ligne_ValOrigin, 'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        Ligne_A   = downsize(Ligne_AOrigin,   'pasy', 1, 'pasx', FacteurReduction, 'Method', 'mean');
                        FacteurReduction = floor(FacteurReduction / 2);
                    end
                end
                
            case 3  % Filtrage
                disp('Pas encore au point')
                return
            otherwise % Pas d'interpolation
        end
        if isempty(Ligne_x)
            continue
        end
        
        Ligne_x = 1 + floor(Ligne_x);
        Ligne_y = 1 + floor(Ligne_y);
        
        % V�rification qui s'est av�r�e utile
        subIn = (Ligne_x >= 1) & (Ligne_x <= Nx) & (Ligne_y >= 1) & (Ligne_y <= Ny);
        Ligne_x  = Ligne_x(subIn);
        Ligne_y  = Ligne_y(subIn);
        
        if nbSlides == 1
            sub = sub2ind([Ny Nx], Ligne_y, Ligne_x);
        else
            sub = sub2ind([Ny Nx nbSlides], Ligne_y, Ligne_x, iCan*ones(size(Ligne_x)));
        end
        
        %         subIn = find((Ligne_x > 1) & (Ligne_x <= Nx) & (Ligne_y > 1) & (Ligne_y <= Ny));
        %         if nbSlides == 1
        %             sub = sub2ind([Ny Nx], Ligne_y(subIn), Ligne_x(subIn));
        %         else
        %             sub = sub2ind([Ny Nx nbSlides], Ligne_y(subIn), Ligne_x(subIn), iCan*ones(size(subIn)));
        %         end
        
        subVide = isnan(I(sub));
        sub_subVide = sub(subVide);
        I(sub_subVide) = Ligne_Val(subVide);
        J(sub_subVide) = Ligne_A(subVide);
    end
end

%% Interpolation dans le sens longitudinal

% figure; grid on; hold on
switch AlongInterpolation
    case 2  % Interpolation lineaire
        for iCan=1:nbSlides
            for k1=2:M
                k2 = k1-1;
                Ligne_Lat = Lat.Image(subyLat(k2), subxLat);
                Ligne_Lon = Lon.Image(subyLon(k2), subxLon);
                Ligne_sub = find(~isnan(Ligne_Lon));
                if length(Ligne_sub) <= 1
                    continue
                end
                Ligne_x1 = (Ligne_Lon(Ligne_sub([1 end])) - LonWest) / resolLon;
                Ligne_y1 = (Ligne_Lat(Ligne_sub([1 end])) - LatSud)  / resolLat;
                
                Ligne_Val1 = this.Image(suby(k2), subx, iCan);
                Ligne_A1   = single(A.Image(subyA(k2), subxA));
                Ligne_sub  = ~isnan(Ligne_Lon);
                Ligne_Val1 = Ligne_Val1(Ligne_sub);
                Ligne_A1   = Ligne_A1(Ligne_sub);
                
                if ConvertInSingle
                    Ligne_Val1 = single(Ligne_Val1);
                end
                
                subNonNaN = ~isnan(Ligne_Val1);
                if sum(subNonNaN(:)) <= 1
                    continue
                end
                
                k2 = k1;
                Ligne_Lat = Lat.Image(subyLat(k2), subxLat);
                Ligne_Lon = Lon.Image(subyLon(k2), subxLon);
                Ligne_sub = find(~isnan(Ligne_Lon));
                if length(Ligne_sub) <= 1
                    continue
                end
                Ligne_x2 = (Ligne_Lon(Ligne_sub([1 end])) - LonWest) / resolLon;
                Ligne_y2 = (Ligne_Lat(Ligne_sub([1 end])) - LatSud)  / resolLat;
                
                Ligne_Val2 = this.Image(suby(k2), subx, iCan);
                Ligne_A2   = A.Image(subyA(k2), subxA);
                Ligne_sub  = ~isnan(Ligne_Lon);
                Ligne_Val2 = Ligne_Val2(Ligne_sub);
                Ligne_A2   = Ligne_A2(Ligne_sub);
                
                if ConvertInSingle
                    Ligne_Val2 = single(Ligne_Val2);
                end
                
                subNonNaN = ~isnan(Ligne_Val2);
                if sum(subNonNaN(:)) <= 1
                    continue
                end
                
                delta_x1x2 = Ligne_x2 - Ligne_x1;
                delta_y1y2 = Ligne_y2 - Ligne_y1;
                dmax = ceil(1.5 * max(sqrt(delta_x1x2 .^ 2 + delta_y1y2 .^ 2)));
                
                % plot(Ligne_x1, Ligne_y1, 'r*', Ligne_x2, Ligne_y2, '+r'); grid on
                for k2=1:dmax-1
                    Ligne_x1x2 = Ligne_x1 + k2 * delta_x1x2 / dmax;
                    Ligne_y1y2 = Ligne_y1 + k2 * delta_y1y2 / dmax;
                    % plot(Ligne_x1x2, Ligne_y1y2, 'xb');
                    
                    NPointsInterpoles = ceil(1.5 * sqrt(diff(Ligne_x1) .^ 2 + diff(Ligne_y1) .^ 2));
                    Ligne_x1x2 = linspace(Ligne_x1x2(1), Ligne_x1x2(2), NPointsInterpoles);
                    Ligne_y1y2 = linspace(Ligne_y1y2(1), Ligne_y1y2(2), NPointsInterpoles);
                    Ligne_x = 1 + floor(Ligne_x1x2);
                    Ligne_y = 1 + floor(Ligne_y1y2);
                    
                    if InterpolationMethod
                        alpha1 = k2 / dmax;
                    else
                        alpha1 = round(k2 / dmax);
                    end
                    
                    % sub = sub2ind([Ny Nx], Ligne_y, Ligne_x);
                    sub = sub2ind([Ny Nx nbSlides], Ligne_y, Ligne_x, iCan*ones(size(Ligne_x)));
                    
                    NPoints = length(Ligne_Val1);
                    newx = linspace(1, NPoints, length(sub));
                    UnaNPoints = 1:NPoints;
                    Ligne_Val1 = my_interp1_NoSort(UnaNPoints, Ligne_Val1, newx, 'linear');
                    
                    Ligne_A1   = my_interp1_NoSort(UnaNPoints, Ligne_A1,   newx, 'linear');
                    clear UnaNPoints
                    
                    NPoints = length(Ligne_Val2);
                    newx = linspace(1, NPoints, length(sub));
                    UnaNPoints = 1:NPoints;
                    Ligne_Val2 = my_interp1_NoSort(UnaNPoints, Ligne_Val2, newx, 'linear');
                    
                    Ligne_A2   = my_interp1_NoSort(UnaNPoints, Ligne_A2,   newx, 'linear');
                    clear UnaNPoints
                    
                    subVide = isnan(I(sub));
                    sub_subVide = sub(subVide);
                    I(sub_subVide) = Ligne_Val1(subVide) * (1 -alpha1) + Ligne_Val2(subVide) * alpha1;
                    J(sub_subVide) = Ligne_A1(subVide) * (1 -alpha1) + Ligne_A2(subVide) * alpha1;
                end
            end
        end
        
    otherwise % Pas d'interpolation
end

if ConvertInSingle
    I(isnan(I)) = this.ValNaN;
    I = feval(class(this.Image(1)), I);
end

%% TODO : refaire tout �a par this = cl_image(....)

%{
this = replace_Image(this, I);
clear I

% figure; imagesc(x, y, I); colorbar;

%% Remise � jour des descripteurs de l'image

this.x            = x;
this.y            = y;
this.XLim         = compute_XYLim(x);
this.YLim         = compute_XYLim(y);
this.GeometryType = cl_image.indGeometryType('LatLong');
this.TagSynchroX  = 'Longitude';
this.TagSynchroY  = 'Latitude';
this.XUnit        = 'deg';
this.YUnit        = 'deg';

this.Sonar.Time                 = [];
this.Sonar.PingNumber           = [];
this.Sonar.Immersion            = [];   % Immersion de l'antenne par rapport  la surface (valeur negative)
this.Sonar.Height               = [];   % Hauteur par rapport au fond (valeur positive)
this.Sonar.CableOut             = [];
this.Sonar.Speed                = [];
this.Sonar.Heading              = [];
this.Sonar.Roll                 = [];
this.Sonar.RollUsedForEmission  = [];
this.Sonar.Pitch                = [];
this.Sonar.Yaw                  = [];
this.Sonar.SurfaceSoundSpeed    = [];
this.Sonar.FishLatitude         = [];
this.Sonar.FishLongitude        = [];
this.Sonar.ShipLatitude         = [];
this.Sonar.ShipLongitude        = [];
this.Sonar.PortMode_1           = [];
this.Sonar.PortMode_2           = [];
this.Sonar.StarMode_1           = [];
this.Sonar.StarMode_2           = [];
this.Sonar.Heave                = [];
this.Sonar.Tide                 = [];
this.Sonar.PingCounter          = [];
this.Sonar.SampleFrequency      = [];
this.Sonar.SonarFrequency       = [];
this.Sonar.Interlacing          = [];

this.Sonar.BSN                  = [];
this.Sonar.BSO                  = [];
this.Sonar.TVGN                 = [];
this.Sonar.TVGCrossOver         = [];
this.Sonar.TxBeamWidth          = [];
this.Sonar.DistPings            = [];

this.Sonar.NbSwaths             = [];
this.Sonar.PresenceWC           = [];
this.Sonar.PresenceFM           = [];

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;
%}

%% Modifi� par JMA le 27/02/2014

TVG      = this.Sonar.TVG;
DiagEmi  = this.Sonar.DiagEmi;
DiagRec  = this.Sonar.DiagRec;
AireInso = this.Sonar.AireInso;
NE       = this.Sonar.NE;
SH       = this.Sonar.SH;
GT       = this.Sonar.GT;
BS       = this.Sonar.BS;
Ship     = this.Sonar.Ship;

pppp = cl_image('Image', I, 'Unit', this.Unit, 'Name', this.Name, ...
    'x', x, 'y', y, 'XUnit', 'deg', 'YUnit', 'deg', ...
    'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', this.DataType, ...
    'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', ...
    'ColormapIndex', this.ColormapIndex, ...
    'Carto', this.Carto, 'InitialFileName', this.InitialFileName, ...
    'InitialImageName', this.InitialImageName, 'Comments', this.Comments, ...
    'SonarDescription',  this.Sonar.Desciption);
if this.DataType == cl_image.indDataType('Reflectivity')
    pppp.Sonar.TVG      = TVG;
    pppp.Sonar.DiagEmi  = DiagEmi;
    pppp.Sonar.DiagRec  = DiagRec;
    pppp.Sonar.AireInso = AireInso;
    pppp.Sonar.NE       = NE;
    pppp.Sonar.SH       = SH;
    pppp.Sonar.GT       = GT;
    pppp.Sonar.BS       = BS;
    pppp.Sonar.Ship     = Ship;
end

%   BathyCel: [1x1 struct]
%   'InitialFileFormat', this.InitialFileFormat, ...
%   this.History

clear this
this = pppp;
clear pppp
clear I

%% Compl�tion du titre

this = update_Name(this);

%% Finalisation de la Mosaique d'angle

that = replace_Image(this, J);
clear J
that.DataType           = A.DataType;
that.ImageType          = 1;
that.Unit               = 'deg';
that.ColormapIndex      = 17;
that = compute_stats(that);
that.TagSynchroContrast = num2str(rand(1));
that.CLim = [that.StatValues.Min that.StatValues.Max];
that = update_Name(that);

%% Apparemment tout s'est bien pass�

status = 1;

warning('on', 'all');
