% TODO : correction � faire pour Mosaique RGB
%
% Projection d'un nuage de points en grille geographique
%
% Syntax
%   b = sonar_mosaiqueInv(a, Lat, Lon, ...)
%
% Input Arguments 
%   a : instance de cl_image
%   Lat : instance de cl_image de DataType='Latitude'
%   Lon : instance de cl_image de DataType='Longitude'
%   resol : Resolution en m
%
% Name-Value Pair Arguments
%   subx   : subsampling in X
%   suby   : subsampling in Y
%   AcrossInterpolation : 1=Pas d'interpolation, 2=Interpolation lineaire,
%                         3=M�thode � d�velopper dans le futur
%   AlongInterpolation  : 1=Pas d'interpolation, 2=Interpolation lineaire
%
% Output Arguments 
%   b      : instance de cl_image
%   status : 1=Operation reussie, 0=operation non reussie
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   b = cl_car_ima(nomFic);
%   c = view(b, 'Sonar.Ident', 3);
%   a = get(c, 'Images');
%   imagesc(a)
%
%   [b, status] = sonar_mosaiqueInv(a(1), a(2), a(3), 20);
%   if status
%       imagesc(b)
%   end
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [status, that] = sonar_mosaiqueInv(this, this_TxAngle, Lat, Lon, Mos, MosTxAngle, suby, varargin)

[varargin, flagWaitbar] = getPropertyValue(varargin, 'flagWaitbar', 1); %#ok<ASGLU>

if isempty(suby)
    suby = 1:this.nbRows;
end

% Mos.Image = repmat(1:size(Mos.Image,2), size(Mos.Image,1), 1);

[subx, subxLat, subxLon]   = intersect3(this.x, Lat.x, Lon.x);

% [subsub, subyLat, subyLon] = intersect3(this.y(suby), Lat.y, Lon.y); %
% Modif JMA le 04/03/2017 pour �viter bug sur fichier SPFE 0012_20100225_031309_Belgica.all
[subsub, subyLat, subyLon] = intersect3(round(this.y(suby)), round(Lat.y), round(Lon.y));

suby = suby(subsub);

if isempty(MosTxAngle)
    this_TxAngle = [];
end

if ~isempty(this_TxAngle)
        % TODO : ATTENTION danger, il faut certainement faire
        % PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
    [subsub, subyLat, subyTxAngle] = intersect3(this.Sonar.PingCounter(suby), ...
        Lat.Sonar.PingCounter, this_TxAngle.Sonar.PingCounter);
    suby = suby(subsub);
    subyLon = subyLat;
    [~, subxTxAngle] = intersect3(this.x(subx), this_TxAngle.x, this_TxAngle.x);
end

% warning('off', 'all');

Mos.Image = Mos.Image(:,:,:);

%% Limites geographiques de la mosaique

LonWestMos = min(Mos.x);
LatSudMos  = min(Mos.y);
resolLonMos = get(Mos, 'XStep');
resolLatMos = get(Mos, 'YStep');

if ~isempty(this_TxAngle)
    LonWestMosTxAngle = min(MosTxAngle.x);
    LatSudMosTxAngle  = min(MosTxAngle.y);
    resolLonMosTxAngle = get(MosTxAngle, 'XStep');
    resolLatMosTxAngle = get(MosTxAngle, 'YStep');
end

TypeVar = class(Mos.Image(1));
if strcmp(TypeVar, 'uint8') %#ok<STISA>
    TypeVar = 'single';
end

Nx = length(subx);
Ny = length(suby);

nbSlides = Mos.nbSlides;
try
    I = NaN(Ny, Nx, nbSlides, TypeVar); % TODO Should be Mos
catch %#ok<CTCH>
    I = cl_memmapfile('Value', NaN, 'Size', [Ny, Nx, nbSlides], 'Format', TypeVar); % TODO Should be Mos
end

str = sprintf('%s : %s : Processing Inverse Mosaic', Mos.Name);

M = length(suby);
Yes = 0;

if flagWaitbar
    hw = create_waitbar(str, 'N', M);
else
    hw = [];
end
for k=1:M
    my_waitbar(k, M, hw);
    
    Ligne_Lat = Lat.Image(subyLat(k), subxLat);
    Ligne_Lon = Lon.Image(subyLon(k), subxLon);
    
    Ligne_sub = find(~isnan(Ligne_Lon));
    
    if isempty(Ligne_sub)
        continue
    end
    
    Ligne_xMos = (Ligne_Lon(Ligne_sub) - LonWestMos) / resolLonMos;
    Ligne_yMos = (Ligne_Lat(Ligne_sub) - LatSudMos)  / resolLatMos;
    
    if ~isempty(this_TxAngle)
        Ligne_xMosTxAngle = (Ligne_Lon(Ligne_sub) - LonWestMosTxAngle) / resolLonMosTxAngle;
        Ligne_yMosTxAngle = (Ligne_Lat(Ligne_sub) - LatSudMosTxAngle)  / resolLatMosTxAngle;
    end
    
    if isempty(this_TxAngle)
        subValide  = (Ligne_xMos >= 1) & (Ligne_xMos <= Mos.nbColumns) & ...
            (Ligne_yMos >= 1) & (Ligne_yMos <= Mos.nbRows);
        Ligne_xMos = Ligne_xMos(subValide);
        Ligne_yMos = Ligne_yMos(subValide);
    else
        subValide  = (Ligne_xMos >= 1) & (Ligne_xMos <= Mos.nbColumns) & ...
            (Ligne_yMos >= 1) & (Ligne_yMos <= Mos.nbRows) & ...
            (Ligne_xMosTxAngle >= 1) & (Ligne_xMosTxAngle <= MosTxAngle.nbColumns) & ...
            (Ligne_yMosTxAngle >= 1) & (Ligne_yMosTxAngle <= MosTxAngle.nbRows);
        Ligne_xMos = Ligne_xMos(subValide);
        Ligne_yMos = Ligne_yMos(subValide);
        Ligne_xMosTxAngle = Ligne_xMosTxAngle(subValide);
        Ligne_yMosTxAngle = Ligne_yMosTxAngle(subValide);
        % FigUtils.createSScFigure(12345); PlotUtils.createSScPlot(Ligne_xMos, Ligne_yMos); grid on; axis([1 Mos.nbColumns 1 Mos.nbRows])
    end
    
    Ligne_sub = Ligne_sub(subValide);
    Ligne_xMos = 1 + floor(Ligne_xMos);
    
    if ~isempty(this_TxAngle)
        Ligne_xMosTxAngle = 1 + floor(Ligne_xMosTxAngle);
    end
    
    if Mos.y(2) >  Mos.y(1)
        Ligne_yMos = 1 + floor(Ligne_yMos);
    else
        Ligne_yMos = Mos.nbRows - floor(Ligne_yMos);
    end
    
    if ~isempty(this_TxAngle)
        if MosTxAngle.y(2) >  MosTxAngle.y(1)
            Ligne_yMosTxAngle = 1 + floor(Ligne_yMosTxAngle);
        else
            Ligne_yMosTxAngle = MosTxAngle.nbRows - floor(Ligne_yMosTxAngle);
        end
    end
    
    for iCan=1:nbSlides
        if nbSlides == 1
            subMos = sub2ind([Mos.nbRows Mos.nbColumns], Ligne_yMos, Ligne_xMos);
        else
            subMos = sub2ind([Mos.nbRows Mos.nbColumns nbSlides], Ligne_yMos, Ligne_xMos, iCan*ones(size(Ligne_xMos)));
        end
        
        if ~isempty(this_TxAngle)
            subMosTxAngle = sub2ind([MosTxAngle.nbRows MosTxAngle.nbColumns], Ligne_yMosTxAngle, Ligne_xMosTxAngle);
        end

        if isempty(this_TxAngle)
            subNonVide = find(~isnan(Mos.Image(subMos)));
            if ~isempty(subNonVide)
                % TODO : il faudrait faire une interpolation intelligente ici en fonction du type de donn�es car �a pose probl�me pour le calcul des angles d'incidence
                I(suby(k),Ligne_sub(subNonVide), iCan) = Mos.Image(subMos(subNonVide));
                Yes = 1;
            end
        else
            if isempty(subMosTxAngle)
                TetaMosTxAngle = MosTxAngle.Image(subMosTxAngle);
                TetaThisTxAngle = this_TxAngle.Image(subyTxAngle(k),subxTxAngle(Ligne_sub));
            else
                subNonVide = find(~isnan(Mos.Image(subMos)) & ~isnan(MosTxAngle.Image(subMosTxAngle)));
                TetaMosTxAngle = MosTxAngle.Image(subMosTxAngle(subNonVide));
                TetaThisTxAngle = this_TxAngle.Image(subyTxAngle(k),subxTxAngle(Ligne_sub(subNonVide)));
            end

            % subMemeAngle = abs(TetaMosTxAngle - TetaThisTxAngle) < 0.5;
            if isempty(TetaMosTxAngle)
                continue
            end
            subMemeAngle = find((abs(tand(TetaMosTxAngle(:)) - tand(TetaThisTxAngle(:))) < 0.1) & ...
                ~isnan(TetaMosTxAngle(:)));
            subMemeAngle = subMemeAngle(3:end-2);
            if isempty(subMemeAngle)
                continue
            else
                I(suby(k),Ligne_sub(subNonVide(subMemeAngle)), iCan) = Mos.Image(subMos(subNonVide(subMemeAngle)));
                Yes = 1;
            end
        end
    end
end
my_close(hw)

if ~Yes
    str1 = sprintf('ATTENTION : "%s" n''a pas de recouvrement g�ographique avec "%s".', this.Name, Mos.Name);
    str2 = sprintf('WARNING : "%s" and "%s" have no geographic overlap.', this.Name, Mos.Name);
%     my_warndlg(Lang(str1,str2), 0, 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoGeographicOverlap');
    that = [];
    status = 0;
    return
end

if Mos.ImageType == 2
%     subNaN = isnan(sum(I,3));
    I = uint8(I);
%     I(subNaN) = 0; % TODO 2:3 aussi
    Mos.ValNaN = 0;
end

%% Output image

that = inherit(this, I, 'subx', subx, 'suby', suby, 'AppendName', 'InverseMosaic', ...
    'DataType', Mos.DataType, 'ValNaN', Mos.ValNaN, 'ColormapCustom', Mos.ColormapIndex, ...
    'ImageType', Mos.ImageType);
%     'ImageType', Mos.ImageType, 'x', subx, 'y', suby); % Modif JMA le 04/03/2017 pour �viter bug SPFE 0012_20100225_031309_Belgica.all

status = 1;
% warning('on', 'all');
