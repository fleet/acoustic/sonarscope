% TODO : pas clair entre Z et I !!! et SpectralStatus !!!

function sonar_mosaique_ExportSoundings(this, Lat, Lon, nomFicEchoes, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Algorithm

x = this.x(subx);
y = this.y(suby);

I = this.Image(suby,subx);
if this.SpectralStatus == 2
    I =  20 * log10(abs(I));
end

Z = this.Image(suby,subx);
T = this.Sonar.Time;
T = T(suby);
Date = T.date;
Hour = T.Hour;

xLon = get_val_xy(Lon, x, y);
yLat = get_val_xy(Lat, x, y);

%% Do the structure

% Donn�es utiles
Points.BaseForm         = 'cube';
Points.iPing            = repmat(suby', 1, length(x));
Points.Latitude         = yLat;
Points.Longitude        = xLon;
Points.Z                = I(:);
Points.Date             = repmat(Date, 1, length(x));
Points.Hour             = repmat(Hour, 1, length(x));

% Donn�es inutiles � supprimer quand nouveau format sera d�fini
Points.Energie          = Z;
Points.EnergieTotale    = Z; %NaN(size(Z), 'single');
Points.XCoor            = zeros(size(Z), 'single');
Points.YCoor            = zeros(size(Z), 'single');
Points.Angle            = [];
Points.RangeInSamples   = [];
Points.AcrossDistance   = zeros(size(Z), 'single');
Points.Celerite         = [];
Survey.Name             = 'Unknown';
Survey.Vessel           = 'Unknown';
Survey.Sounder          = 'Unknown';
Survey.ChiefScientist   = 'Unknown';

Points.Longitude        = Points.Longitude(:);
Points.Latitude         = Points.Latitude(:);
Points.XCoor            = Points.XCoor(:);
Points.YCoor            = Points.YCoor(:);
Points.Z                = Points.Z(:);
Points.Energie          = Points.Energie(:);
Points.AcrossDistance   = Points.AcrossDistance(:);
Points.iPing            = Points.iPing(:);
Points.EnergieTotale    = Points.EnergieTotale(:);

%% Export the data in an Echoes SSc3DV format

[~, DataName] = fileparts(nomFicEchoes);
flag = WC_SaveEchoes(nomFicEchoes, Points, 'InitialFileName', this.InitialFileName, 'LayerName3DV', DataName, 'Survey', Survey);
