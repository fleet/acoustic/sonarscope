% Plot d'un nuage de points
%
% Syntax
%   sonar_mosaique_plot(a, Lat, Lon, Z, ...)
%
% Input Arguments
%   a : instance de cl_image
%   Lat : instance de cl_image de DataType='Latitude'
%   Lon : instance de cl_image de DataType='Longitude'
%   Z   : [] ou Altitude si ce n'est pas a
%
% Name-Value Pair Arguments
%   subx   : subsampling in X
%   suby   : subsampling in Y
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   b = cl_car_ima(nomFic);
%   c = view(b, 'Sonar.Ident', 3);
%   a = get(c, 'Images');
%   imagesc(a)
%
%   sonar_mosaique_plot(a(1), a(2), a(3));
%
% See also cl_image Authors
% Authors : JMA + AG
% ----------------------------------------------------------------------------

function hAxe = sonar_mosaique_plot(this, Lat, Lon, Z, TagFig, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, ThreeD]  = getPropertyValue(varargin, 'ThreeD',  2);
[varargin, ExaVert] = getPropertyValue(varargin, 'ExaVert', 1);
[varargin, hAxe]    = getPropertyValue(varargin, 'hAxe',    []); %#ok<ASGLU>

% TODO : ATTENTION danger, il faut certainement faire
% PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
PC1 = this.Sonar.PingCounter(suby);
PC2 = Lat.Sonar.PingCounter;
if isempty(Z)
    PC3 = Lon.Sonar.PingCounter;
else
    PC3 = Z.Sonar.PingCounter;
end
[subPC1, ~, subZ] = intersect3(PC1, PC2, PC3); % , PC4); % Il faut generaliser intersect3 en intersectn
% subLon = subLat; % On considere que les images Lat et Lon ont les m�me ordonn�es : DANGER !
clear PC1 PC2 PC3
suby = suby(subPC1);


%% Si axe designe, traitement du trace et de l'affichage

x = this.x(subx);
y = this.y(suby);

if this.nbSlides ~= 1
    return
end

I = this.Image(suby,subx);
if this.SpectralStatus == 2
    I =  20 * log10(abs(I));
end

if ~isempty(Z)
    if sign(this.y(2)-this.y(1)) == sign(Z.y(2)-Z.y(1))
        Z = Z.Image(subZ,subx);
    else
        Z = Z.Image(fliplr(subZ),subx);
    end
end

xLon = get_val_xy(Lon, x, y);
yLat = get_val_xy(Lat, x, y);

couleurs = this.Colormap;
nbColors = size(couleurs,1);
if size(couleurs, 1) > 64
    couleurs = zeros(64,3);
    for k=1:3
        couleurs(:,k) = interp1(1:nbColors, this.Colormap(:,k), linspace(k, nbColors, 64));
    end
end
nbColors = size(couleurs,1);

milieuClasse = linspace(this.CLim(1), this.CLim(2), nbColors);

if isempty(hAxe)
    hFig = FigUtils.createSScFigure; hold on; grid on;
    set(hFig, 'Tag', TagFig)
    hAxe = findobj(hFig, 'type', 'axes');
    set(hAxe, 'Tag', 'AxePlotMos');
    
    f = uimenu('Text', 'SonarScope');
    uimenu(f, 'Text', 'Link axis with another figure', 'Callback', @AxisLink);
    uimenu(f, 'Text', 'Export values in a ASCII file', 'Callback', @ExportPointsASCII);
    uimenu(f, 'Text', 'Export values in VRML', 'Callback', @ExportPointsVRML);
else
    axes(hAxe); hold on;
end

XLim(1) = min(xLon(:));
XLim(2) = max(xLon(:));
YLim(1) = min(yLat(:));
YLim(2) = max(yLat(:));

x = linspace(XLim(1), (XLim(1) + 100 * nbColors * eps(XLim(1))), nbColors);
y = linspace(YLim(1), YLim(2),                                   nbColors);
imagesc(x, y, milieuClasse); colormap(couleurs); my_colorbar

Image = I;
milieuClasse(end+1) = Inf;
for k=1:nbColors
    sub = find(I < milieuClasse(k));
    plotUneCouleur(xLon, yLat, I, Z, sub, ThreeD, couleurs(k,:))
    I(sub) = NaN;
end
sub = find(I >= milieuClasse(nbColors));
plotUneCouleur(xLon, yLat, I, Z, sub, ThreeD, couleurs(k,:))

hold off;

if (Lat.DataType == cl_image.indDataType('Latitude')) && ...
        (Lon.DataType == cl_image.indDataType('Longitude'))
    if ThreeD == 1
        axisGeo
    else
        axis normal
    end
    xlabel('Longitude');
    ylabel('Latitude');
elseif (Lat.DataType == cl_image.indDataType('GeoY')) && ...
        (Lon.DataType == cl_image.indDataType('GeoX'))
    axis equal
    xlabel('x');
    ylabel('y');
end
hAxe = gca;

% Ca serait bien de mettre �a dans une callback avec un slider
DataAspectRatio(1) = 1;
DataAspectRatio(2) = 1;
DataAspectRatio(3) = 1 / ExaVert;
set(gca, 'DataAspectRatio', DataAspectRatio)
zlim('auto')

    function AxisLink(varargin)
        hFigOther = findobj(0, 'Tag', TagFig);
        for ifig=1:length(hFigOther)
            strFig{ifig} = sprintf('Figure %d', hFigOther(ifig)); %#ok<AGROW>
        end
        [rep, flag] = my_listdlg(Lang('Figures � synchroniser', 'Figures to synchronize'), strFig);
        clear strFig
        if ~flag
            return
        end
        if isempty(rep)
            return
        end
        for ifig=1:length(rep)
            hAxeLink(ifig) = findobj(hFigOther(rep(ifig)), 'Type', 'axes','-not', 'Tag', 'Colorbar'); %#ok<AGROW>
        end
        linkaxes(hAxeLink,'xy');
    end

    function ExportPointsASCII(varargin)
        persistent persistent_nomDirExport
        ImageName = extract_ImageName(this);
        if isempty(persistent_nomDirExport) || ~exist(persistent_nomDirExport, 'dir')
            filtre  = fullfile(my_tempdir, [ImageName '.txt']);
        else
            filtre  = fullfile(persistent_nomDirExport, [ImageName '.txt']);
        end
        str1 = 'Nom du fichier d''export ASCII';
        str2 = 'Give a file name for the ASCII file';
        [flag, nomFic] = my_uiputfile({'*.txt'}, Lang(str1,str2), filtre);
        if ~flag
            return
        end
        persistent_nomDirExport = fileparts(nomFic);
        
        fid = fopen(nomFic, 'w+');
        if fid == -1
            messageErreurFichier(nomFic, 'WriteFailure');
            return
        end
        
        [N1, N2] = size(Image);
        str1 = 'Traitement en cours';
        str2 = 'Processing';
        hw = create_waitbar(Lang(str1,str2), 'N', N1);
        for k1=1:N1
            my_waitbar(k1, N1, hw)
            for k2=1:N2
                fprintf(fid, '%s; %s; %s\n', ...
                    num2strPrecis(xLon(k1,k2)), num2strPrecis(yLat(k1,k2)), num2strPrecis(Image(k1,k2)));
            end
        end
        my_close(hw, 'MsgEnd');
        fclose(fid);
    end

    function ExportPointsVRML(varargin)
        Nom = extract_ImageName(this);
        [flag, nomFic, Az, El, vertExag, modeGrid, radius, DisplayAxis] = paramVRML(Nom);
        if ~flag
            return
        end
        
        flagRepere = 1;
        % Donn�es utiles
        Points.Latitude  = Lat.Image;
        Points.Longitude = Lon.Image;
        Points.Z         = Image;
        Points.XCoor     = xLon;
        Points.YCoor     = yLat;
        
        % Donn�es inutiles � supprimer quand nouveau format sera d�fini
        Points.Latitude  = Points.Latitude(:);
        Points.Longitude = Points.Longitude(:);
        Points.Z         = Points.Z(:);
        Points.XCoor     = Points.XCoor(:);
        Points.YCoor     = Points.YCoor(:);
        
        
        
        
        if strcmp(modeGrid, 'Texture')
            % Cr�ation du fichier VRML.
            flag = export_VRML_FaceSet(this, 1, Points, nomFic, 'flagRepere', flagRepere, 'Az', Az, 'El', El, ...
                'vertExag', vertExag, 'radius', radius, 'DisplayAxis', DisplayAxis);
            if ~flag
                return
            end
        else
            % Cr�ation du fichier VRML.
            flag = export_VRML_PointSet_GeoYX(this, 1, Points, nomFic, 'flagRepere', flagRepere, ...
                'Az', Az, 'El', El, 'vertExag', vertExag);
            if ~flag
                return
            end
            
            strFR = 'Voulez-vous tester le mode d''export en Grille d''Elevation (en cours de travaux) ?';
            strUS = 'Do you to test the Elevation Grid Export (in progress) ?';
            [rep, flag] = my_questdlg(Lang(strFR, strUS), 'Init', 1);
            if ~flag
                return
            end
            if rep == 1
                % Ce mode d'export a �t� adapt� pour sortir des nuages de
                % points selon le mod�le ElevationGrid (�crit par GLT).
                % Cela permet de g�n�rer des nuages de points dont le
                % lissage de couleurs est g�r� par les plugins VRML.
                % Dans notre cas, on ne superpose pas de texture au-dessus
                % des points.
                % NB : les NaN ne sont pas admis dans cette vue, ils sont
                % ramen�s au Minimum de la matrice.7
                [nomDir, nomFic, ~] = fileparts(nomFic);
                nomFic = fullfile(nomDir, [nomFic '_ElevationGrid.wrml']);
                flag = export_VRML_ElevationWithOutTexture(this, ...
                    'filename', nomFic, 'subx', subx, 'suby', suby, 'Az', Az, 'El', El, 'vertExag', vertExag, ...
                    'exportGridMode', modeGrid); %#ok<NASGU>
            end
        end
    end
end


function plotUneCouleur(xLon, yLat, I, Z, sub, ThreeD, couleurs)
if ThreeD == 1
    if isempty(Z)
        h = PlotUtils.createSScPlot3(xLon(sub), yLat(sub), I(sub));
    else
        h = PlotUtils.createSScPlot3(xLon(sub), yLat(sub), Z(sub));
    end
    set(h, 'LineStyle', 'none', 'Marker', '.', 'Color', couleurs);
else
    h = PlotUtils.createSScPlot(xLon(sub), yLat(sub));
    set(h, 'LineStyle', 'none', 'Marker', '+', 'Color', couleurs);
end
end
