% Calcul des limites geographiques d'une mosaique
%
% Syntax
%   [Nx, Ny, resolLon, resolLat, x, y, LonWest, LonEst, LatNord, LatSud, message, status]  = sonar_mosaique_size(a, Lat, Lon, ...)
%
% Input Arguments
%   a : instance de cl_image
%   Lat : instance de cl_image de DataType='Latitude'
%   Lon : instance de cl_image de DataType='Longitude'
%   resol : Resolution de la mosaique (m)
%
% Name-Value Pair Arguments
%   subx   : subsampling in X
%   suby   : subsampling in Y
%
% Output Arguments
%   Nx       : Nombre de colonnes de la mosaique
%   Ny       : Nombre de lignes de la mosaique
%   resolLon : Resolution en longitude (m)
%   resolLat : Resolution en latitude (m)
%   x        : Abscisses de la mosaique (deg)
%   y        : Ordonnees de la mosaique (deg)
%   LonWest  : Longitude ouest (deg)
%   LonEst   : Longitude est (deg)
%   LatNord  : Latitude Nord (deg)
%   LatSud   : Latitude Sud (deg)
%   status   : 1=Operation reussie, 0=operation non reussie
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo');
%   b = cl_car_ima(nomFic);
%   c = view(b, 'Sonar.Ident', 3);
%   a = get(c, 'Images');
%   imagesc(a)
%
%   [b, status] = sonar_mosaique_size(a(1), a(2), a(3));
%   if status
%       imagesc(b)
%   end
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Nx, Ny, resolLon, resolLat, x, y, LonWest, LonEst, LatNord, LatSud, message, ...
    subx, suby, subxLat, subyLat, subxLon, subyLon, status] = ...
    sonar_mosaique_size(this, Lat, Lon, resol, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

status = 0;

if (length(suby) == 1) || (length(subx) == 1)
    Nx       = [];
    Ny       = [];
    resolLon = [];
    resolLat = [];
    x        = [];
    y        = [];
    LonWest  = [];
    LonEst   = [];
    LatNord  = [];
    LatSud   = [];
    message  = [];
    subx     = [];
    suby     = [];
    subxLat  = [];
    subyLat  = [];
    subxLon  = [];
    subyLon  = [];
    return
end

%% Contrôles de validité

if Lat.DataType ~= cl_image.indDataType('Latitude')
    str = sprintf('Le Layer de "Latitude" n''est pas bon');
    my_warndlg(str, 1);
    return
end

if Lon.DataType ~= cl_image.indDataType('Longitude')
    str = sprintf('La Layer de "Longitude" n''est pas bon');
    my_warndlg(str, 1);
    return
end

% Attention : sort(sublLM) peut etre utile pour supprimer les tests de
% flipud (voir cl_image/plus)

[XLim, YLim, subx, suby, subxLat, subyLat, subxLon, subyLon] = intersectionImages(this, subx, suby, Lat, Lon);
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Passage des coordonnées métriques en coordonnées géométriques

LatNord = -Inf;
LatSud  = Inf;
LonWest = Inf;
LonEst  = -Inf;
for k=1:length(subyLon)
    X1 = Lon.Image(subyLon(k), subxLon);
    Y1 = Lat.Image(subyLat(k), subxLat);
    sub = find(~isnan(X1) & ~isnan(Y1) & ~isinf(X1) & ~isinf(Y1));
    if isempty(sub)
        continue
    end
    X1 = X1(sub);
    Y1 = Y1(sub);
    LatNord = max(LatNord, max(Y1(:)));
    LatSud  = min(LatSud,  min(Y1(:)));
    LonWest = min(LonWest, min(X1(:)));
    LonEst  = max(LonEst,  max(X1(:)));
end

if isinf(LatNord) || isinf(LonWest)
    Nx       = [];
    Ny       = [];
    resolLon = [];
    resolLat = [];
    x        = [];
    y        = [];
    LonWest  = [];
    LonEst   = [];
    LatNord  = [];
    LatSud   = [];
    message  = [];
    subx     = [];
    suby     = [];
    subxLat  = [];
    subyLat  = [];
    subxLon  = [];
    subyLon  = [];
    str1 = sprintf('"%s" n''a pas de navigation. Cette image est ignorée.', this.Name);
    str2 = sprintf('"%s" has no navigation. This image is bypassed.', this.Name);
    my_warndlg(Lang(str1,str2), 0);
    status = 1;
    return
end

if (LonEst - LonWest) > 180
    temp = LonEst;
    LonEst = 360 + LonWest;
    LonWest = temp;
end

%% Alignement sur une grille

LatMilieu = round((LatNord + LatSud) / 2);
RayonTerre = get(this.Carto, 'Ellipsoide.DemiGrandAxe');
resolLat = (double(resol) / RayonTerre) * (180/pi);
resolLon = abs(resolLat / cos(LatMilieu * (pi/180)));
LatSud  = floor(LatSud  / resolLat) * resolLat;
LatNord = ceil( LatNord / resolLat) * resolLat;
LonWest = floor(LonWest / resolLon) * resolLon;
LonEst  = ceil( LonEst  / resolLon) * resolLon;

if isinf(LatSud) || isinf(LatNord) || isinf(LonWest) || isinf(LonEst)
    Nx = 0;
    Ny = 0;
    message = [];
    x = [];
    y = [];
    return
end

%% Calcul des coordonnées géographiques

y = LatSud:resolLat:LatNord;
x = LonWest:resolLon:LonEst;
x(end+1) = x(end) + mean(diff(x));
y(end+1) = y(end) + mean(diff(y));

x = centrage_magnetique(x);
y = centrage_magnetique(y);

Nx = length(x);
Ny = length(y);

%% Constitution du message resumant les limites geographiques

strLatNorth = lat2str(LatNord);
% strLonSouth = lon2str(LatSud); A CORRIGER sur GeoYX2LatLong
strLatSouth = lat2str(LatSud);
strLonWest  = lon2str(LonWest);
strLonEast  = lon2str(LonEst);

message = sprintf('With the %f m resolution, the mosaic would be : \n%d columns\n%d rows\n', ...
    resol, Nx, Ny);
message = sprintf('%s\nNorth Latitude = %f\t: %s', message, LatNord, strLatNorth);
message = sprintf('%s\nSouth Latitude = %f\t: %s', message, LatSud,  strLatSouth);
message = sprintf('%s\nWest Longitude = %f\t: %s', message, LonWest, strLonWest);
message = sprintf('%s\nEast Longitude = %f\t: %s', message, LonEst,  strLonEast);

status = 1;
