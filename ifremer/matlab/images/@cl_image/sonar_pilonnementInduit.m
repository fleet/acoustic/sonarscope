function p = sonar_pilonnementInduit(this, iPing, t)

if isempty(this.Sonar.Pitch)
    p = 0;
    return
end

% MAINTENANCE A FAIRE : il faut g�rer le cas des sondeurs "duals"

PitchTx = this.Sonar.Pitch(iPing);
RollTx  = this.Sonar.Roll(iPing);

Cx = this.Sonar.Ship.MRU.X;
if isempty(Cx)
    p = 0;
    return
end

Cy = this.Sonar.Ship.MRU.Y;
Cz = this.Sonar.Ship.MRU.Z;

Sx = this.Sonar.Ship.Arrays.Transmit.X;
Sy = this.Sonar.Ship.Arrays.Transmit.Y;
Sz = this.Sonar.Ship.Arrays.Transmit.Z;
pTx = pilonnement_induit(PitchTx, RollTx, Sx, Sy, Sz, Cx, Cy, Cz);

% -----------------------------------------------------------------------
% A d�faut d'avoir acc�s � la donn�e d'attitude pleine r�solution on fait
% une interpolation lin�aire : A AMELIORER lorque l'on refondra la partie
% "gestion des signaux"

Time = this.Sonar.Time.timeMat;
PitchRx = my_interp1(Time, this.Sonar.Pitch, t);
RollRx  = my_interp1(Time, this.Sonar.Roll, t);
Sx = this.Sonar.Ship.Arrays.Receive.X;
Sy = this.Sonar.Ship.Arrays.Receive.Y;
Sz = this.Sonar.Ship.Arrays.Receive.Z;

pRx = pilonnement_induit(PitchRx, RollRx, Sx, Sy, Sz, Cx, Cy, Cz);

p = (pTx + pRx) / 2 + this.Sonar.Ship.WaterLineVerticalOffset;

% Min = min(p, [], 'omitnan');
% Max = max(p, [], 'omitnan');
% DIFF = Max - Min


% ------------------------------------------------------------------
function p = pilonnement_induit(Pitch, Roll, Sx, Sy, Sz, Cx, Cy, Cz)

% VERIFIER TOUS LES SIGNES

p = sind(Pitch) * (Sy - Cy) - ...
    cos(Pitch) .* sind(Roll) * (Sx - Cx) + ...
    cosd(Roll) .* cosd(Pitch) * (Sz - Cz);



