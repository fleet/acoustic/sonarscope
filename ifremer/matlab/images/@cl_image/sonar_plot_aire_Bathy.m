% Affichage de l'aire insonifiee sur un pixel et ses voisins
%
% Syntax
%   b = sonar_plot_aire_Bathy(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   fig : Numero de la figure
%
% Examples
%   sonar_plot_aire_Bathy(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function  sonar_plot_aire_Bathy(this, subx, suby, AngleEmission, D, H, varargin)

my_warndlg('This function is not terminated, the plot axis are false.', 1);

[varargin, fig] = getPropertyValue(varargin, 'fig', []); %#ok<ASGLU>

if isempty(fig)
    fig = figure;
else
    figure(fig)
end

x = this.x(subx);
y = this.y(suby);
ixMilieu = ceil(length(x) / 2);
iyMilieu = ceil(length(y) / 2);

Sonar = get(this, 'SonarDescription');
SonarMode_1 = get(Sonar, 'Sonar.Mode_1');
SonarMode_2 = get(Sonar, 'Sonar.Mode_2');

%% Red�finition de Sonar si necessaire

if get(this, 'flagPortMode_1')
    ImageMode_1 = get(this, 'PortMode_1');
    if ImageMode_1(suby(iyMilieu)) ~= SonarMode_1
        Sonar = set(Sonar, 'Sonar.Mode_1', ImageMode_1(suby(iyMilieu)));
    end
end
if get(this, 'flagPortMode_2')
    ImageMode_2 = get(this, 'PortMode_2');
    if ImageMode_2(suby(iyMilieu)) ~= SonarMode_2
        Sonar = set(Sonar, 'Sonar.Mode_2', ImageMode_2(suby(iyMilieu)));
    end
end

%% Calcul des resolutions longitudinales et transversales

BeamFormRxNb = get(Sonar, 'BeamForm.Rx.Nb');
% ResolLong       = resol_long(Sonar, D);
% ResolTransBathy = resol_trans_bathy(Sonar, H, AngleEmission);

[a, ReceptionBeamEstime] = diagramme_Rx(Sonar, AngleEmission(iyMilieu,:));
ReceptionBeamEstime = unique(ReceptionBeamEstime);
ReceptionBeamEstime = (min(ReceptionBeamEstime)-1):(max(ReceptionBeamEstime)+1);
ReceptionBeamEstime(ReceptionBeamEstime < 1) = [];
ReceptionBeamEstime(ReceptionBeamEstime > BeamFormRxNb) = [];

%             [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeCar2Sim(listeModes);
%         SonarDescription = cl_sounder('Sonar.Ident', SonarIdent(1), 'Sonar.Mode_1', SonarMode_1(1), 'Sonar.Mode_2', SonarMode_2);

Angles = get(Sonar, 'BeamForm.Rx.Angles');
AngleEmission = Angles(ReceptionBeamEstime) .* sign(x(1));
AngleEmission = repmat(AngleEmission, 3, 1);

H = repmat(H(2,2), 3, length(ReceptionBeamEstime));
D = H ./ cos(AngleEmission * (pi / 180));
x = H(2,:) .* tan(AngleEmission(2,:) * (pi / 180));

ResolLong       = resol_long(Sonar, D);
ResolTransBathy = resol_trans_bathy(Sonar, H, AngleEmission);

%% Trac� du diagramme 2D

[~, iFais] = min(abs(Angles - AngleEmission(2,2)));
imagesc_Rx(Sonar, 'Constructeur', 'nuFais', iFais, 'H', H(2, 2)); axis equal

%% Trac� de la figure

figure(fig)
hold on
for i=1:length(y)
    for j=1:length(x)
        y1 = y(i) - y(2) - ResolLong(i,j) / 2;
        y2 = y(i) - y(2) + ResolLong(i,j) / 2;
        x1 = x(j) - ResolTransBathy(i,j) / 2;
        x2 = x(j) + ResolTransBathy(i,j) / 2;
        
        str = sprintf('Beam footprint ping %d beam %d', y(i), (i));
        cmenu = uicontextmenu;
        
        if (i == iyMilieu) && (j == ixMilieu)
            patch([x1 x2 x2 x1 x1], [y1 y1 y2 y2 y1], 'c', 'UIContextMenu', cmenu)
        elseif i == j
            plot([x1 x2 x2 x1 x1], [y1 y1 y2 y2 y1], 'b', 'UIContextMenu', cmenu)
        end
        uimenu(cmenu, 'Text', str);
    end
end
hold off
axis equal
title(Lang('Aire insonifiee bathy', 'Bathy insonified area'))
xlabel([this.XLabel ' (' this.XUnit ')'])
ylabel([this.YLabel ' (' this.YUnit ')'])
