% Affichage de l'aire insonifiee sur un pixel et ses voisins
%
% Syntax
%   b = sonar_plot_aire_Image(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   fig : Numero de la figure
%
% Examples
%   sonar_plot_aire_Image(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function  sonar_plot_aire_Image(this, subx, suby, AngleEmission, D, varargin)

my_warndlg('This function is not terminated, the plot axis are false.', 1);

[varargin, fig] = getPropertyValue(varargin, 'fig', []); %#ok<ASGLU>

if isempty(fig)
    fig = figure;
else
    figure(fig)
end

x = this.x(subx);
y = this.y(suby);
ixMilieu = ceil(length(x) / 2);
iyMilieu = ceil(length(y) / 2);

Sonar = get(this, 'SonarDescription');
SonarMode_1 = get(Sonar, 'Sonar.Mode_1');
SonarMode_2 = get(Sonar, 'Sonar.Mode_2');

%% Red�finition de Sonar si necessaire

if get(this, 'flagPortMode_1')
    ImageMode_1 = get(this, 'PortMode_1');
    if ImageMode_1(suby(iyMilieu)) ~= SonarMode_1
        Sonar = set(Sonar, 'Sonar.Mode_1', ImageMode_1(suby(iyMilieu)));
    end
end
if get(this, 'flagPortMode_2')
    ImageMode_2 = get(this, 'PortMode_2');
    if ImageMode_2(suby(iyMilieu)) ~= SonarMode_2
        Sonar = set(Sonar, 'Sonar.Mode_2', ImageMode_2(suby(iyMilieu)));
    end
end

%% Calcul des r�solutions longitudinales et transversales

ResolLong       = resol_long(Sonar, D);
ResolTransImage = resol_trans_image(Sonar, D, AngleEmission);

%% Trac� du diagramme 2D

Angles = get(Sonar, 'BeamForm.Rx.Angles');
[~, iFais] = min(abs(Angles - AngleEmission(2,2)));
H = D(2,2) * cos(AngleEmission(2,2) * pi / 180);
imagesc_Rx(Sonar, 'Constructeur', 'nuFais', iFais, 'H', H); axis equal; axis tight;

%% Trac� de la figure

figure(fig)
hold on
for i=1:length(y)
    for j=1:length(x)
        y1 = y(i) - ResolLong(i,j) / 2;
        y2 = y(i) + ResolLong(i,j) / 2;
        x1 = x(j) - ResolTransImage(i,j) / 2;
        x2 = x(j) + ResolTransImage(i,j) / 2;
        
        str = sprintf('Footprint ping %d beam %d', y(i), x(j));
        cmenu = uicontextmenu;
        
        if (i == iyMilieu) && (j == ixMilieu)
            patch([x1 x2 x2 x1 x1], [y1 y1 y2 y2 y1], 'c', 'UIContextMenu', cmenu);
        elseif i == j
            plot([x1 x2 x2 x1 x1], [y1 y1 y2 y2 y1], 'b', 'UIContextMenu', cmenu);
        end
        uimenu(cmenu, 'Text', str);
    end
end


pasx = x(2) - x(1);
xPiquets = x - pasx/2;
xPiquets(end+1) = xPiquets(end) + pasx;

pasy = y(2) - y(1);
yPiquets = y - pasy/2;
yPiquets(end+1) = yPiquets(end) + pasy;

% w = axis;
for i=1:length(y)
    y1 = yPiquets(i);
    y2 = yPiquets(i+1);
    for j=1:length(x)
        x1 = xPiquets(j);
        x2 = xPiquets(j+1);
        
        str = sprintf('Pixel ping %d beam %d', y(i), (i));
        cmenu = uicontextmenu;
        
        if (i == iyMilieu) && (j == ixMilieu)
            patch([x1 x2 x2 x1 x1], [y1 y1 y2 y2 y1], 'y', 'UIContextMenu', cmenu)
        elseif (i == j)
            plot([x1 x2 x2 x1 x1], [y1 y1 y2 y2 y1], 'k', 'UIContextMenu', cmenu)
        end
        uimenu(cmenu, 'Text', str);
    end
end

hold off
axis equal
title(Lang('Aire insonifiee pixel', 'Pixel inonified area'))
xlabel([this.XLabel ' (' this.XUnit ')'])
ylabel([this.YLabel ' (' this.YUnit ')'])
