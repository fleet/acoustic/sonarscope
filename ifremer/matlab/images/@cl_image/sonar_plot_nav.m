% Trace de la navigation
%
% Syntax
%   sonar_plot_nav(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby    : subsampling in Y
%
% Examples
%   Fig = [];
%   Fig = sonar_plot_nav(a, 'Fig', Fig)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Fig = sonar_plot_nav(this, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

nbImages = length(this);
for k=1:nbImages
    Fig = sonar_plot_nav_unitaire(this(k), Fig);
end


function Fig = sonar_plot_nav_unitaire(this, Fig)


%% Controles

% Test si SonarD ou SonarX ou sondeur de sediment
% flag = testSignature(this, 'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist') cl_image.indGeometryType('PingBeam')]);
flag = testSignature(this, 'GeometryType', 'PingXxxx');
if ~flag
    return
end

if isempty(this.Sonar.FishLatitude) || isempty(this.Sonar.FishLongitude)
    my_warndlg('FishLatitude et FishLongitude pas present dans ce fichier', 1);
    return
end

%% Affichage de la nav seule

Fig = plot_navigation(this.InitialFileName, Fig, this.Sonar.FishLongitude, this.Sonar.FishLatitude, this.Sonar.Time.timeMat, this.Sonar.Heading);
