% sonar_polarEchogram ente 2 images
%
% Syntax
%   [c, status] = sonar_polarEchogram(a, b, ...)
%
% Input Arguments
%   a : Instance de cl_image de type "RayPathSampleNb"
%   b : Instance de cl_image de type "RxBeamAngle"
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   c      : Instances de cl_image contenant "Bathymetry", "AcrossDist" et "AlongDist"
%   status : 1=tout c'est bien^passe
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, c, comp, Detection, Mask, WCSlide] = sonar_polarEchogram(this, ProfilCelerite, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, subLayers]               = getPropertyValue(varargin, 'subLayers',               []);
[varargin, otherImage]              = getPropertyValue(varargin, 'otherImage',              []);
[varargin, RangeDetectionInSamples] = getPropertyValue(varargin, 'RangeDetectionInSamples', []);
[varargin, MaxImageSize]            = getPropertyValue(varargin, 'MaxImageSize',            2000);
[varargin, x]                       = getPropertyValue(varargin, 'across',                  []);
[varargin, depth]                   = getPropertyValue(varargin, 'depth',                   []);
[varargin, MaskBeyondDetection]     = getPropertyValue(varargin, 'MaskBeyondDetection',     0);
[varargin, DisplayDetection]        = getPropertyValue(varargin, 'DisplayDetection',        0);
% [varargin, TideValue]             = getPropertyValue(varargin, 'TideValue', 0);
[varargin, typeAlgoWC]              = getPropertyValue(varargin, 'typeAlgoWC',              1); %#ok<ASGLU>
% [varargin, DataDepthAcrossDist]   = getPropertyValue(varargin, 'DataDepthAcrossDist', []);
% [varargin, DataDepthAlongDist]    = getPropertyValue(varargin, 'DataDepthAlongDist',  []);

J = [];
if ~isempty(otherImage)
    UnitOtherImage  = otherImage.Unit;
    TitreOtherImage = otherImage.Name;
    otherImage = otherImage.Image(suby,subx);
    J = otherImage;
end

c         = [];
comp      = [];
Detection = [];
Mask      = [];

%% Contr�les de validit�

identReflec = cl_image.indDataType('Reflectivity');
identPhase  = cl_image.indDataType('TxAngle');
flag = testSignature(this, 'DataType', [identReflec identPhase], 'GeometryType', cl_image.indGeometryType('SampleBeam'));
if ~flag
    WCSlide = [];
    return
end

% figure; plot(ProfilCelerite.SoundSpeed(indSPP,:), ProfilCelerite.Depth(indSPP,:)); grid
% D = C * (Range/2) / fe;

fe = this.Sonar.SampleBeamData.SampleRate;
C  = this.Sonar.SampleBeamData.SoundVelocity;
if isfield(this.Sonar.SampleBeamData, 'TransducerDepth')
    TransducerDepth = this.Sonar.SampleBeamData.TransducerDepth;
else
    TransducerDepth = 0;
end

[flag, RxAnglesEarth_WC] = get_RxAnglesEarth_WC(this);
if ~flag
    WCSlide = [];
    return
end

%% Calcul de la c�l�rit� �quivalente si algo simplifi� de Xavier Lurton

if typeAlgoWC >= 2
    Cs = C;
    [RxAnglesEarth_WC_XL, C_XL] = prepareWC_XL(this, ProfilCelerite, RxAnglesEarth_WC, Cs, 'TransducerDepth', TransducerDepth);
end
if typeAlgoWC == 3
    OPTIONS = optimset('Display', 'off', 'TolFun', 1e-6, 'MaxIter', 100); % , 'UseParallel', true % TODO : demander si // Tbx dans l'IHM
end

%%

if ~isfield(this.Sonar.SampleBeamData, 'SamplingRateMinMax')
    this.Sonar.SampleBeamData.SamplingRateMinMax = [fe fe];
end

if isempty(RangeDetectionInSamples)
    if isfield(this.Sonar.SampleBeamData, 'R1SamplesFiltre')
        RangeDetectionInSamples = this.Sonar.SampleBeamData.R1SamplesFiltre;
    else
        RangeDetectionInSamples = ones(size(this.Sonar.SampleBeamData.TxAngle)) * this.nbRows;
        %         fe = this.Sonar.SampleBeamData.SampleRate;
        %         RangeDetectionInSamples = BeamDataOut.RD.Range * coef;
    end
end
RangeDetectionInSamples = RangeDetectionInSamples(:,:);
RxAnglesEarth_WC = RxAnglesEarth_WC(:,:);

[TempsSimple, Detection.RangeInMeters] = get_RangeInMeters_WC(this, RangeDetectionInSamples, fe, C);

subOK = (RangeDetectionInSamples > 1);

RangeDetectionInSamples(~subOK) = NaN; % Ajout JMA le 27/01/2020
Detection.RangeInMeters(~subOK) = NaN; % Ajout JMA le 27/01/2020

R0 = min(RangeDetectionInSamples(subOK));
if isempty(R0)
    R0 = 2;
end

R0 = min(floor(R0), this.nbRows);

R1 = floor(R0* 0.9);
mc = mean_col(this, 'suby', R1:R0);
WCSlide.D0 = C(1) * ((R0+R1)/2) / (2*fe(1));
WCSlide.X0 = WCSlide.D0 * sind(RxAnglesEarth_WC);
WCSlide.Z0 = WCSlide.D0 * cosd(RxAnglesEarth_WC);
WCSlide.Reflec_dB = mc;

if (typeAlgoWC == 1) || MaskBeyondDetection
    WCSlide.X0(~subOK)        = NaN;
    WCSlide.Z0(~subOK)        = NaN;
    WCSlide.Reflec_dB(~subOK) = NaN;
end

N = length(suby);
if isfield(this.Sonar.SampleBeamData, 'Depth') ...
        && ~isempty(this.Sonar.SampleBeamData.Depth) ...
        && ~all(isnan(this.Sonar.SampleBeamData.Depth))% TODO : faire modif dans lecture Kongsberg
    %     xAntenne = 0; % TODO : r�cup�rer les infos dans datagrammes
    xAntenne = this.Sonar.Ship.Arrays.Transmit.X; % Modif JMA le 29/11/2016 pour donn�es EM2040 Cyrille \\meskl1\dev\GLOBE\02 PROJETS\2016 Projet Carnot\WC\Recette\FichierRecette\JDD2(all_et_wcd)
    zAntenne = -this.Sonar.Ship.Arrays.Transmit.Z; % Modif JMA le 29/11/2016 pour donn�es EM2040 Cyrille \\meskl1\dev\GLOBE\02 PROJETS\2016 Projet Carnot\WC\Recette\FichierRecette\JDD2(all_et_wcd)
    
    if isempty(xAntenne) % TODO : cas des sondeurs Reson
        xAntenne = 0;
    end
    if isempty(zAntenne) % TODO : cas des sondeurs Reson
        zAntenne = 0;
    end
    
    xAntenne = 0; % Pourquoi ?
    zAntenne = 0; % 0 pour EM710, immersion pour EM122 !!!
    
    X = NaN(N, length(subx));
    Z = NaN(N, length(subx));
    
    %{
FigUtils.createSScFigure;
h(1) = subplot(4,1,1);
PlotUtils.createSScPlot(this.Sonar.SampleBeamData.R1SamplesFiltre(subx), '-*'); grid on; title('R1SamplesFiltre')
h(2) = subplot(4,1,2); PlotUtils.createSScPlot(this.Sonar.SampleBeamData.Depth(subx), '-*'); grid on; title('Depth')
h(3) = subplot(4,1,3); PlotUtils.createSScPlot(this.Sonar.SampleBeamData.AcrossDist(subx), '-*'); grid on; title('AcrossDist')
h(4) = subplot(4,1,4); PlotUtils.createSScPlot(this.Sonar.SampleBeamData.AlongDist(subx), '-*'); grid on; title('AlongDist')
linkaxes(h, 'x')
    %}
    
    if DisplayDetection
        FigUtils.createSScFigure;
        subplot(2,1,1); PlotUtils.createSScPlot(this.Sonar.SampleBeamData.R1SamplesFiltre, '-*r'); grid on; set(gca, 'YDir', 'reverse')
        subplot(2,1,2); PlotUtils.createSScPlot(this.Sonar.SampleBeamData.AcrossDist, this.Sonar.SampleBeamData.Depth, '-*r'); grid on;
    end
    
    % Rajout� par JMA le 26/10/2014 pour fichiers PDS
    n1 = length(this.Sonar.SampleBeamData.AcrossDist);
    n2 = length(this.Sonar.SampleBeamData.R1SamplesFiltre);
    if n1 == n2
        subNaN = isnan(this.Sonar.SampleBeamData.AcrossDist(:)) | isnan(this.Sonar.SampleBeamData.Depth(:)) ...
            | isnan(this.Sonar.SampleBeamData.R1SamplesFiltre(:)) | (this.Sonar.SampleBeamData.R1SamplesFiltre(:) == 1);
    else
        % Bug trouv� sur fichier TVISTOUL 20141125_121138_PP
        % TODO : le positionnement n'est pas bon : REVOIR CA !!!
        sub = 1:min(n1,n2);
        subNaN = isnan(this.Sonar.SampleBeamData.AcrossDist(sub)) | isnan(this.Sonar.SampleBeamData.Depth(sub)) ...
            | isnan(this.Sonar.SampleBeamData.R1SamplesFiltre(sub)) | isnan(this.Sonar.SampleBeamData.R1SamplesFiltre(sub) == 1);
    end
    %subNaN = isnan(this.Sonar.SampleBeamData.AcrossDist) | isnan(this.Sonar.SampleBeamData.Depth) | isnan(this.Sonar.SampleBeamData.R1SamplesFiltre);
    subNonNaN = find(~subNaN);
    subNaN    = find(subNaN);
    if ~isempty(subNaN)
        if all(this.Sonar.SampleBeamData.AcrossDist(subNonNaN) == 0)
            flag = 0;
            WCSlide = [];
            return
        end
        
        this.Sonar.SampleBeamData.AcrossDist(subNaN)      = my_interp1(subNonNaN, this.Sonar.SampleBeamData.AcrossDist(subNonNaN),      subNaN);
        this.Sonar.SampleBeamData.AlongDist(subNaN)       = my_interp1(subNonNaN, this.Sonar.SampleBeamData.AlongDist(subNonNaN),       subNaN);
        this.Sonar.SampleBeamData.Depth(subNaN)           = my_interp1(subNonNaN, this.Sonar.SampleBeamData.Depth(subNonNaN),           subNaN);
        this.Sonar.SampleBeamData.R1SamplesFiltre(subNaN) = my_interp1(subNonNaN, this.Sonar.SampleBeamData.R1SamplesFiltre(subNonNaN), subNaN);
    end
    % Fin ajout
    
    switch typeAlgoWC
        case 1 % On s'appuie sur les acrossDist, Depth et instant de d�tection
            rMoins1 = this.Sonar.SampleBeamData.R1SamplesFiltre(subx);
            subyMoins1 = (suby' - 1);
            
            a = (this.Sonar.SampleBeamData.AcrossDist(subx) - xAntenne) ./ rMoins1;
            X = xAntenne + subyMoins1 * a;
            
            DepthUnderAntenna = (-this.Sonar.SampleBeamData.Depth(subx) - zAntenne);
            a = DepthUnderAntenna ./ rMoins1;
            Z = zAntenne + subyMoins1 * a;
%             OffsetForPitch = nanmean(nanmean(DepthUnderAntenna)) * (1 - cosd(this.Sonar.SampleBeamData.Pitch));
            
        case 2 % On ne consid�re plus que les angles, la c�l�rit� et la fr�quence d'�chantillonnage
            rangeMeters = suby * (C_XL / (fe * 2));
            X = xAntenne + rangeMeters' * sind(RxAnglesEarth_WC_XL(subx));
            Z = zAntenne + rangeMeters' * cosd(RxAnglesEarth_WC_XL(subx));
            
            % Ajout JMA pour donn�es EM2040 de l'AUV (pings 120 � 180 de Cassidaigne
            % plong�e 09, fichier 0005_20140818_083230_raw_01.all
            if isfield(this.Sonar.SampleBeamData, 'Pitch')&& ~isempty(this.Sonar.SampleBeamData.Pitch) && ~isnan(this.Sonar.SampleBeamData.Pitch)
%                 rMoins1 = this.Sonar.SampleBeamData.R1SamplesFiltre(subx);
%                 bathy = rMoins1 * (C_XL / (fe * 2));
%                 OffsetForPitch = -nanmean(bathy) * (1 - cosd(this.Sonar.SampleBeamData.Pitch));
%                 Z = Z - OffsetForPitch;
                Z = Z * cosd(this.Sonar.SampleBeamData.Pitch);
            end
            
        case 3 % M�thode 2 mais enrichie par m�thode 1 : En cours de d�veloppement
            %             if isfield(this.Sonar.SampleBeamData, 'SectorTransmitDelay')
            %                 SectorTransmitDelay = this.Sonar.SampleBeamData.SectorTransmitDelay(subx);
            %             else
            %                 SectorTransmitDelay = 0;
            %             end
            R1SamplesFiltre = this.Sonar.SampleBeamData.R1SamplesFiltre(subx);
            DemiTxPulseLength = this.Sonar.SampleBeamData.TxPulseWidth * (C_XL/(2*2*1000));
            
            XData = this.Sonar.SampleBeamData.AcrossDist(subx);
            YData = this.Sonar.SampleBeamData.Depth(subx);
            
            %{
            [XModel, YModel] = projectionLineaire(R1SamplesFiltre, RxAnglesEarth_WC_XL(subx), C_XL, fe, xAntenne, zAntenne, DemiTxPulseLength);
            fig = FigUtils.createSScFigure; PlotUtils.createSScPlot(XData, YData, 'k'); grid on;
            hold on; PlotUtils.createSScPlot(XModel, YModel, 'b');
            %}
            
            [CNew, AnglesNew, alpha, offsetAngles] = fitCelerity(XData, YData, C_XL, Cs, fe, xAntenne, zAntenne, R1SamplesFiltre, DemiTxPulseLength, RxAnglesEarth_WC(subx), OPTIONS); %#ok<ASGLU>
            
            %{
            figure(fig)
            [XModel, YModel] = projectionLineaire(R1SamplesFiltre, AnglesNew, CNew, fe, xAntenne, zAntenne, DemiTxPulseLength);
            PlotUtils.createSScPlot(XModel, YModel, 'r');
            %}
            
            rangeMeters = suby * (CNew / (fe * 2));
            X = xAntenne + rangeMeters' * sind(AnglesNew);
            Z = zAntenne + rangeMeters' * cosd(AnglesNew);
            
            % Ajout JMA pour donn�es EM2040 de l'AUV (pings 120 � 180 de Cassidaigne
            % plong�e 09, fichier 0005_20140818_083230_raw_01.all
            if isfield(this.Sonar.SampleBeamData, 'Pitch')&& ~isempty(this.Sonar.SampleBeamData.Pitch) && ~isnan(this.Sonar.SampleBeamData.Pitch)
%                 rMoins1 = this.Sonar.SampleBeamData.R1SamplesFiltre(subx);
%                 bathy = rMoins1 * (C_XL / (fe * 2));
%                 OffsetForPitch = -nanmean(bathy) * (1 - cosd(this.Sonar.SampleBeamData.Pitch));
%                 Z = Z - OffsetForPitch;
                Z = Z * cosd(this.Sonar.SampleBeamData.Pitch);
            end
    end
else
    if size(TempsSimple,2) == 1
        D = C(1) * TempsSimple(suby,:); % TODO C(1) pour EM3002
        X = D * sind(RxAnglesEarth_WC(subx));
        Z = D * cosd(RxAnglesEarth_WC(subx));
    else
        D = C * TempsSimple(suby,:);
        X = D .* repmat(sind(RxAnglesEarth_WC(subx)), size(D,1), 1);
        Z = D .* repmat(cosd(RxAnglesEarth_WC(subx)), size(D,1), 1);
    end
end

% % Ajout JMA pour donn�es EM2040 de l'AUV (pings 120 � 180 de Cassidaigne
% % plong�e 09, fichier 0005_20140818_083230_raw_01.all
% C'est OK pour Affichage WC et bathy et Echo-int�gration mais c'est pas
% bon pour visu dans GLOBE
% if isfield(this.Sonar.SampleBeamData, 'Pitch')&& ~isempty(this.Sonar.SampleBeamData.Pitch) && ~isnan(this.Sonar.SampleBeamData.Pitch)
%     Z = Z + OffsetForPitch;
% end

I = this.Image(suby,subx);
subNaN = isnan(I);
X(subNaN) = NaN;
Z(subNaN) = NaN;

% if ~isempty(DataDepthAcrossDist)
%     alongSampleBeam = my_interp1(DataDepthAcrossDist, DataDepthAlongDist, this.Sonar.SampleBeamData.AcrossDist);
%     Y0 = nanmean(alongSampleBeam);
%     Y = NaN(size(X));
%     nbSamples = size(Y,1);
%     for k=1:size(Y,2)
%         Y(:,k) = linspace(Y0, alongSampleBeam(k), nbSamples);
%     end
%     figure(7852); surf(double(X),double(Y),double(-Z),double(I)); shading flat; axis equal
% end

% TODO JMA
if isfield(this.Sonar.SampleBeamData, 'Depth') && ~isempty(this.Sonar.SampleBeamData.Depth)
    Detection.X = this.Sonar.SampleBeamData.AcrossDist(subx);
    Detection.Z = this.Sonar.SampleBeamData.Depth(subx);
else
    Detection.X =  Detection.RangeInMeters(subx) .* sind(RxAnglesEarth_WC(subx));
    Detection.Z = -Detection.RangeInMeters(subx) .* cosd(RxAnglesEarth_WC(subx));
end

%{
figure;
pcolor(double(I)); colormap(jet(256)); shading flat; axis ij; colorbar; title('pcolor(double(I))')

[ny,nx] = size(I);
x = log10(1:nx);
y = log10(1:ny);
figure;
pcolor(x, y, double(I)); colormap(jet(256)); shading flat; axis ij; colorbar; title('pcolor(x, y, double(I)')

figure;
subplot(2,2,1); imagesc(X); colormap(jet(256)); colorbar; title('X')
subplot(2,2,2); imagesc(-Z); colormap(jet(256)); colorbar; title('-Z')
subplot(2,2,3:4); pcolor(X, -Z, double(I)); colormap(jet(256)); shading flat; axis equal; colorbar; title('pcolor(X, -Z, double(I))')
axis tight; colorbar; grid on;
hold on; plot(Detection.X, Detection.Z, 'k')
%}

if MaskBeyondDetection
    R1 = RangeDetectionInSamples(subx);
    
    % Rajout� par JMA le 26/10/2014 pour fichiers PDS
    subNaN = isnan(R1);
    subNonNaN = find(~subNaN);
    subNaN = find(subNaN);
    if ~isempty(subNaN)
        R1(subNaN) = my_interp1(subNonNaN, R1(subNonNaN), subNaN);
    end
    % Fin rajout
    
    R2 = R1;
    for k=3:(length(R1)-2)
        R2(k) = max(R1(k-2:k+2));
    end
    for k=1:length(subx)
        r = floor(R2(k));
        if isnan(r)
            Z(:,k) = NaN;
        else
            ideb = r+10;
            Z(ideb:end,k) = NaN;
        end
    end
end

%% ----

if isempty(depth)
    %     Xmax = double(max([max(X(:)) -min(X(:))]));
    Xmax = double(max(X(:)));
    Xmin = double(min(X(:)));
    Zmax = double(max(abs(Z(:))));
    Pas = Zmax / N;
    [x, Pas] = centrage_magnetique(Xmin:Pas:Xmax);
    depth = centrage_magnetique(-Zmax:Pas:0);
    %     depth = centrage_magnetique(-Zmax:Pas:-this.Sonar.SampleBeamData.TransducerDepth)); % Modif JMA le 08/09/11
    
    while (length(x) * length(depth)) > (MaxImageSize^2)
        Pas = Pas*2;
        [x, Pas] = centrage_magnetique(Xmin:Pas:Xmax);
        depth = centrage_magnetique(-Zmax:Pas:0);
    end
    flagReduceSize = 1;
else
    x = double(x);
    depth  = double(depth);
    flagReduceSize = 0;
end

% RxBeamWidth = get(this.Sonar.Desciption, 'Sonar.TransBeamWth');
% TypeArray   = get(this.Sonar.Desciption, 'Array.Tx.Type');

if ~isempty(RangeDetectionInSamples)
    RangeDetectionInSamples(RangeDetectionInSamples <= 1) = NaN;
    sub = isnan(RangeDetectionInSamples);
    subNaN = find(sub);
    if ~isempty(subNaN)
        subNonNaN = find(~sub);
        if length(subNonNaN) > 1
            RangeDetectionInSamples(subNaN) = interp1(subNonNaN, RangeDetectionInSamples(subNonNaN), subNaN);
            sub = isnan(RangeDetectionInSamples);
            RangeDetectionInSamples(sub) = 0;
        end
    end
    %     figure; plot(subNonNaN, RangeDetectionInSamples(subNonNaN), '*'); grid on; hold on; plot(subNaN, RangeDetectionInSamples(subNaN), '*r')
    for iCol=1:length(subx) %this.nbColumns
        sub = (this.y > RangeDetectionInSamples(subx(iCol)));
        if ~isempty(J)
            J(sub,iCol) = NaN; %#ok<AGROW>
        end
    end
end

% profile on
[Layer, LayerMasked, N, Angle, Range, BeamIndex, RangeSamples, Layer2] = griddingPolar(X, -Z, subx, suby, ...
    RxAnglesEarth_WC(subx), I, J, x, depth, Detection, MaskBeyondDetection, 'subLayers', subLayers, 'otherImage', otherImage);
% profile report

%{
% Contr�le de la correction : on ne devrait pas trouver de lignes en zig-zag !!!!!
% utile aussi pour valider l'�cho-int�gration

if isfield(this.Sonar.SampleBeamData, 'HighDensity')
HD = 1;
HD_AcrossDist         = this.Sonar.SampleBeamData.HighDensity.AcrossDist;
HD_AlongDist          = this.Sonar.SampleBeamData.HighDensity.AlongDist;
HD_Depth              = this.Sonar.SampleBeamData.HighDensity.Depth;
HD_RxAnglesHullBathy  = this.Sonar.SampleBeamData.HighDensity.RxAnglesHullBathy;
HD_RxAnglesEarthBathy = this.Sonar.SampleBeamData.HighDensity.RxAnglesEarthBathy;
else
HD = 0;
end

figure;
hi(1) = subplot(2,1,1);
pcolor(double(X), double(-Z), double(I)); colormap(jet(256)); shading flat;
hold on;
PlotUtils.createSScPlot(this.Sonar.SampleBeamData.AcrossDist, this.Sonar.SampleBeamData.Depth, '-ko');
D0 = R0 * C / (fe * 2);
xcercle = xAntenne + D0 * sind(RxAnglesEarth_WC);
zcercle = zAntenne - D0 * cosd(RxAnglesEarth_WC);
PlotUtils.createSScPlot(xcercle, zcercle, 'k')
if HD
PlotUtils.createSScPlot(HD_AcrossDist, HD_Depth, 'b*')
end

DR1 = this.Sonar.SampleBeamData.R1SamplesFiltre * C / (fe * 2);
xR1 = xAntenne + DR1 .* sind(RxAnglesEarth_WC);
zR1 = zAntenne - DR1 .* cosd(RxAnglesEarth_WC);
PlotUtils.createSScPlot(xR1, zR1, 'g*')

if typeAlgoWC == 1
title('pcolor - AcrossDist,Depth')
else
title('pcolor - Angles, Fe, C')
end

hi(2) = subplot(2,1,2);
imagesc(x, depth, Layer); hold on; axis xy; colormap(jet(256));
PlotUtils.createSScPlot(this.Sonar.SampleBeamData.AcrossDist, this.Sonar.SampleBeamData.Depth, '-ko');
PlotUtils.createSScPlot(xcercle, zcercle, 'k')
if HD
PlotUtils.createSScPlot(HD_AcrossDist, HD_Depth, 'b*')
end
if typeAlgoWC == 1
title('SSc - AcrossDist,Depth')
else
title('SSc - Angles, Fe, C')
end
% PlotUtils.createSScPlot(xcercle+0.75, zcercle, 'k')
linkaxes(hi)
%}

if ~isfield(this.Sonar.SampleBeamData, 'AcrossDist') % Cas des donn�es Mag&Phase des Reson
    this.Sonar.SampleBeamData.AcrossDist = Detection.X;
    this.Sonar.SampleBeamData.Depth      = Detection.Z;
    if isempty(this.Sonar.Ship.Arrays.Transmit.Y)
        this.Sonar.Ship.Arrays.Transmit.Y = 0;
    end
    this.Sonar.SampleBeamData.AlongDist = repmat(this.Sonar.Ship.Arrays.Transmit.Y, 1, length(Detection.X));
end

if isfield(this.Sonar.SampleBeamData, 'AlongDist')
    sub = find(~isnan(this.Sonar.SampleBeamData.AlongDist));
    if isempty(sub)
        along = NaN(size(x));
    else
        along = my_interp1(this.Sonar.SampleBeamData.AcrossDist(sub), this.Sonar.SampleBeamData.AlongDist(sub), x, 'linear', 'extrap');
        %  figure; plot(this.Sonar.SampleBeamData.AcrossDist, this.Sonar.SampleBeamData.AlongDist, 'b'); hold on; grid on; plot(x, along, '*r')
    end
else
    if isfield(this.Sonar, 'Ship') && isfield(this.Sonar.Ship, 'Arrays') && ...
            isfield(this.Sonar.Ship.Arrays, 'Transmit') && isfield(this.Sonar.Ship.Arrays.Transmit, 'Y') && ...
            ~isempty(this.Sonar.Ship.Arrays.Transmit.Y)
        along = repmat(this.Sonar.Ship.Arrays.Transmit.Y, 1, length(x));
    else
        along = NaN(size(x));
    end
end

if isempty(along)
    str1 = 'Cet �chogramme ne semble pas pourvoir �tre r�alis�.';
    str2 = 'This echogram cannot be done.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'EchogramPasFaisable');
    flag = 0;
    return
end

if flagReduceSize
    sumN = sum(N, 1);
    sub = find(sumN ~= 0);
    
    if isempty(sub)
        WCSlide = [];
        flag = 0;
        return
    end
    
    x = x(sub);
    %     along  = along(sub);
    Layer  = Layer(:,sub);
    if ~isempty(LayerMasked)
        LayerMasked = LayerMasked(:,sub);
    end
    if ~isempty(Layer2)
        Layer2 = Layer2(:,sub);
    end
    N = N(:,sub);
    if ~isempty(Angle)
        Angle  = Angle(:,sub);
    end
    if ~isempty(Range)
        Range = Range(:,sub);
    end
    if ~isempty(BeamIndex)
        BeamIndex = BeamIndex(:,sub);
    end
    if ~isempty(RangeSamples)
        RangeSamples = RangeSamples(:,sub);
    end
    %     if ~isempty(RxGain)
    %         RxGain = RxGain(:,sub);
    %     end
    
    sumN = sum(N, 2);
    sub = find(sumN ~= 0);
    depth = depth(sub);
    Layer = Layer(sub,:);
    if ~isempty(LayerMasked)
        LayerMasked = LayerMasked(sub,:);
    end
    if ~isempty(Layer2)
        Layer2 = Layer2(sub,:);
    end
    if ~isempty(Angle)
        Angle = Angle(sub,:);
    end
    if ~isempty(Range)
        Range = Range(sub,:);
    end
    if ~isempty(BeamIndex)
        BeamIndex = BeamIndex(sub,:);
    end
    if ~isempty(RangeSamples)
        RangeSamples = RangeSamples(sub,:);
    end
    %     if ~isempty(RxGain)
    %         RxGain = RxGain(sub,:);
    %     end
end

% Lignes comment�es par JMA le 17/09/2020 pour test export Netcdf
% if isempty(LayerMasked)
%     Mask = [];
% else
%     if (typeAlgoWC >= 2) && ~MaskBeyondDetection
%         Mask = [];
%     else
        Mask = isnan(LayerMasked);
%     end
% end

% depth = depth - this.Sonar.SampleBeamData.TransducerDepth; % TODO  : test 08/09/11
depth = centrage_magnetique(depth);

TagSynchroX = [this.TagSynchroX '_DepthAcrossDist'];
TagSynchroY = TagSynchroX;

Name = this.Name;
if (this.DataType == cl_image.indDataType('Reflectivity'))
    if strcmp(this.Unit, 'dB')
        Reflec_dB  = Layer;
        Reflec_Amp = reflec_dB2Amp(Layer);
    else
        Reflec_Amp = Layer;
        Reflec_dB  = reflec_Amp2dB(Layer);
    end
    %  Reflec_dB(Mask)=NaN;
    c = cl_image('Image',    Reflec_dB, ...
        'DataType',          this.DataType, ...
        'Unit',              'dB', ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',             'm', ...
        'YUnit',             'm', ...
        'Name',             Name, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
%     c = update_Name(c, 'Append', 'dB');
    
    c(2) = cl_image('Image', Reflec_Amp, ...
        'DataType',          this.DataType, ...
        'Unit',              'Amp', ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',            'm', ...
        'YUnit',            'm', ...
        'Name',             Name, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
    c(2) = update_Name(c(2), 'Append', 'Amp');
    
else
    c = cl_image('Image',    Layer, ...
        'DataType',          this.DataType, ...
        'Unit',              this.Unit, ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',             'm', ...
        'YUnit',             'm', ...
        'Name',             Name, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
    c = update_Name(c);
end

if ~isempty(otherImage)
    comp = cl_image('Image', Layer2, ...
        'DataType',          this.DataType, ...
        'Unit',              UnitOtherImage, ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',             'm', ...
        'YUnit',             'm', ...
        'Name',             TitreOtherImage, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
    comp = update_Name(comp, 'Append', 'Comp');
    
    if ~isempty(LayerMasked)
        comp(2) = cl_image('Image', LayerMasked, ...
            'DataType',          this.DataType, ...
            'Unit',              UnitOtherImage, ...
            'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
            'TagSynchroX',       TagSynchroX, ...
            'TagSynchroY',       TagSynchroY, ...
            'x',                 x, ...
            'y',                 depth, ...
            'XUnit',             'm', ...
            'YUnit',             'm', ...
            'Name',             TitreOtherImage, ...
            'InitialFileName',   this.InitialFileName, ...
            'InitialFileFormat', this.InitialFileFormat);
        comp(2) = update_Name(comp(2), 'Append', 'CompMask');
    end
end

if ~isempty(Angle)
    c(end+1) = cl_image('Image', Angle, ...
        'DataType',          cl_image.indDataType('TxAngle'), ...
        'Unit',             'deg', ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',             'm', ...
        'YUnit',             'm', ...
        'Name',             Name, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
    c(end) = update_Name(c(end));
end

if ~isempty(Range)
    c(end+1) = cl_image('Image', Range, ...
        'DataType',          cl_image.indDataType('RayPathLength'), ...
        'Unit',              'm', ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',             'm', ...
        'YUnit',             'm', ...
        'Name',             Name, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
    c(end) = update_Name(c(end));
end

if ~isempty(BeamIndex)
    c(end+1) = cl_image('Image', BeamIndex, ...
        'DataType',          cl_image.indDataType('RxBeamIndex'), ...
        'Unit',              '', ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',             'm', ...
        'YUnit',             'm', ...
        'Name',             Name, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
    c(end) = update_Name(c(end));
end

if ~isempty(RangeSamples)
    my_warndlg('WARNING : image RangeSamples is not finalized.', 1);
    c(end+1) = cl_image('Image', RangeSamples, ...
        'DataType',          cl_image.indDataType('RayPathSampleNb'), ...
        'Unit',              'Sample', ...
        'GeometryType',      cl_image.indGeometryType('DepthAcrossDist'), ...
        'TagSynchroX',       TagSynchroX, ...
        'TagSynchroY',       TagSynchroY, ...
        'x',                 x, ...
        'y',                 depth, ...
        'XUnit',             'm', ...
        'YUnit',             'm', ...
        'Name',             Name, ...
        'InitialFileName',   this.InitialFileName, ...
        'InitialFileFormat', this.InitialFileFormat);
    c(end) = update_Name(c(end));
end

% if ~isempty(RxGain)
%     c(end+1) = cl_image('Image', RxGain, ...
%         'DataType', cl_image.indDataType('RxBeamPattern'), 'Unit', 'dB', ...
%         'GeometryType', cl_image.indGeometryType('DepthAcrossDist'), ...
%         'TagSynchroX', TagSynchroX, ...
%         'TagSynchroY', TagSynchroY, ...
%         'x', x, 'y', depth, 'XUnit', 'm', 'YUnit', 'm', ...
%         'Name', Name, 'InitialFileName', this.InitialFileName, ...
%         'InitialFileFormat', this.InitialFileFormat);
%     c(end) = update_Name(c(end));
% end

c = [c(2:end) c(1)];

for k=1:length(c)
    c(k) = set(c(k), 'Carto', this.Carto);
    %     c(k).Sonar.Ship.Arrays.Transmit = this.Sonar.Ship.Arrays.Transmit;
    %     c(k).Sonar.Ship.Arrays.Receive  = this.Sonar.Ship.Arrays.Receive;
    c(k).Sonar.Ship  = this.Sonar.Ship;
end

% Comment� par JMA le 14/03/2018 pour essai
% this.Sonar.SampleBeamData.AcrossDist = x;
% this.Sonar.SampleBeamData.AlongDist  = along;

for k=1:length(comp)
    comp(k) = set(comp(k), 'Carto', this.Carto); %#ok<AGROW>
    comp(k) = set_SampleBeamData(comp(k), this.Sonar.SampleBeamData); %#ok<AGROW>
    comp(k).Sonar.Ship = this.Sonar.Ship; %#ok<AGROW>
    comp(k).Sonar.Tide = this.Sonar.Tide; %#ok<AGROW>
    comp(k).Sonar.Ship.Arrays.Transmit = this.Sonar.Ship.Arrays.Transmit; %#ok<AGROW>
end

for k=1:length(c)
    c(k) = set(c(k), 'Carto', this.Carto);
    c(k) = set_SampleBeamData(c(k), this.Sonar.SampleBeamData);
    c(k).Sonar.Ship = this.Sonar.Ship;
    c(k).Sonar.Tide = this.Sonar.Tide;
    c(k).Sonar.Ship.Arrays.Transmit = this.Sonar.Ship.Arrays.Transmit;
end

%% Par ici la sortie

flag = 1;
