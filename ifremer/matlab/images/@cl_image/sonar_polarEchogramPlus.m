function [flag, that] = sonar_polarEchogramPlus(this, ProfilCelerite, MaskBeyondDetection, varargin)

that = [];

N = length(this);
% that = repmat(cl_image,2,N); % Commenté pour bug incompréhensible sur image de phase, mission BICOSE
hw = create_waitbar(waitbarMsg('sonar_polarEchogramPlus'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, X] = sonar_polarEchogramPlus_unit(this(k), ProfilCelerite, MaskBeyondDetection, varargin{:});
    if ~flag
        return
    end
%     that(1:length(X),k) = X; % Commenté pour bug incompréhensible sur image de phase, mission BICOSE
    if k == 1
        that = X;
    else
        that = [that X]; %#ok<AGROW>
    end
end  
my_close(hw)
that = that(:);


function [flag, a] = sonar_polarEchogramPlus_unit(this, ProfilCelerite, MaskBeyondDetection, varargin)

% RangeDetectionInSamples R1SamplesFiltre
[flag, a, ~, ~, Mask] = sonar_polarEchogram(this, ProfilCelerite, 'otherImage', this, 'MaskBeyondDetection', MaskBeyondDetection, varargin{:});
if ~flag
    return
end

if MaskBeyondDetection
    for k=1:length(a)
      a(k).Image(Mask) = NaN;
    end
end
