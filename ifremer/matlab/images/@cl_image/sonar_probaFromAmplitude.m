% Calcul de la probabilite sur les donn�es issues du
% mode de d�tection Amplitude/Phase
%
% Syntax
%   [b, c] = sonar_probaFromPhase(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   MU         : Valeur de la phase de probabilite maximale (55 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_probaFromPhase(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, moy] = sonar_probaFromAmplitude(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    this = [];
    moy = [];
    return
end

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, MU] = getPropertyValue(varargin, 'MU', 55); %#ok<ASGLU>

% ---------
% Controles
%
% ident1 = cl_image.indDataType('DetectionType');
% ident2 = cl_image.indDataType('KongsbergQualityFactor');
% flag = testSignature(this, 'DataType', [ident1, ident2], 'GeometryType', cl_image.indGeometryType('PingBeam'));
% if ~flag
%     P = [];
%     return
% end

% -------------------------------------
% Calcul du masque de probabilit�

ImAmpl= this.Image;
% subNaN = find(isnan(ImAmpl));

M = my_nanmean(ImAmpl);

M = M / sum(M, 'omitnan');
MS = cumsum(M);

x = 1:length(MS);
% X = repmat(x, size(ImAmpNbSamples, 1), 1);

SIGMA = sqrt(sum((x-MU).^2 .* M, 'omitnan'));

ProbaAmplitude = normpdf(ImAmpl,MU, SIGMA/2);
ProbaAmplitude = 1- ProbaAmplitude / max(ProbaAmplitude(:));
ProbaAmplitude = ProbaAmplitude / max(max(ProbaAmplitude));
moy = my_nanmean(ProbaAmplitude);
this = replace_Image(this, ProbaAmplitude);
this.ValNaN = NaN;

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, 1:this.nbColumns, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('Unknown');
this = update_Name(this, 'Append', 'Proba from Phase');

