% Calcul d'une probabilite sur les donn�es issues du
% mode de d�tection Nombre d'echantillons sur la detection d'amplitude
%
% Syntax
%   [b, c] = sonar_probaFromNbSamples(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby       : subsampling in Y
%   MU         : Valeur du nombre d'echantillon de probabilite maximale (20 par d�faut)
%
% Output Arguments
%   b : Instance de cl_image contenant le masque
%   c : Instance de cl_image contenant la probabilit�
%
% Examples
%   b = sonar_probaFromNbSamples(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, moy] = sonar_probaFromNbSamples(this, varargin)

if length(this) ~= 1
    my_warndlg('Un seule image a la fois S.V.P', 1);
    this = [];
    moy = [];
    return
end

[~, suby, varargin] = getSubxSuby(this, varargin);

[varargin, MU] = getPropertyValue(varargin, 'MU', 20); %#ok<ASGLU>

% ---------
% Controles
%
% ident1 = cl_image.indDataType('DetectionType');
% ident2 = cl_image.indDataType('KongsbergQualityFactor');
% flag = testSignature(this, 'DataType', [ident1, ident2], 'GeometryType', cl_image.indGeometryType('PingBeam'));
% if ~flag
%     P = [];
%     return
% end

% -------------------------------------
% Calcul du masque de probabilit�

ImAmpNbSamples= this.Image;
% subNaN = find(isnan(ImAmpNbSamples));

M = my_nanmean(ImAmpNbSamples);

M = M / sum(M, 'omitnan');
MS = cumsum(M);

x = 1:length(MS);
% X = repmat(x, size(ImAmpNbSamples, 1), 1);

SIGMA = sqrt(sum((x-MU).^2 .* M, 'omitnan'));

ProbaNbSamples = normpdf(ImAmpNbSamples,MU, SIGMA/4) ;
% ProbaNbSamples = ProbaNbSamples / max(ProbaNbSamples(:));
ProbaNbSamples = ProbaNbSamples / max(max(ProbaNbSamples));
moy = my_nanmean(ProbaNbSamples);
this = replace_Image(this, ProbaNbSamples);
this.ValNaN = NaN;

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, 1:this.nbColumns, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('Unknown');
this = update_Name(this, 'Append', 'Proba from NbSamples');

