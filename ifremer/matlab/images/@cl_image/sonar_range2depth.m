% sonar_range2depth7111 ente 2 images
%
% Syntax
%   [c, status] = sonar_range2depth(a, b, ...)
%
% Input Arguments
%   a : Instance de cl_image de type "RayPathSampleNb"
%   b : Instance de cl_image de type "RxBeamAngle"
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   c      : Instances de cl_image contenant "Bathymetry", "AcrossDist"
%            et "AlongDist"
%   status : 1=tout c'est bien^passe
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [c, this, status] = sonar_range2depth(this, b, d, ProfilCelerite, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, RollImage]              = getPropertyValue(varargin, 'RollImage', []);
[varargin, TransmitTiltAngleImage] = getPropertyValue(varargin, 'TiltImage', []);
[varargin, OffsetRoll]             = getPropertyValue(varargin, 'OffsetRoll', 0); %#ok<ASGLU>

% Correctif � faire en amont (rencontr� dans le cas de donn�es Reson non
% pass�es par PDS
this.Sonar.Heave(abs(this.Sonar.Heave) > 100) = 0;

switch get(this.Sonar.Desciption, 'Sonar.Name')
    case {'GeoSwath+'; 'GeoSwath'}
        RollCoef = -1;
    otherwise
        SystemSerialNumber = get(this.Sonar.Desciption, 'SystemSerialNumber');
        switch SystemSerialNumber
            case 7150
                RollCoef = 0;
            case 7111
                RollCoef = 1; % Pour donn�es d'angles provenant du 7kCenter
                RollCoef = 0; % Pour donn�es d'angles provenantde PDS : les angles ont �t� corrig�s par PDS
            case 7125
                RollCoef = 1;
            otherwise
                
                RollCoef = 0;%                 my_warndlg('Roll effect condidered as already set in the angles. If it is not the case for your sounder please contact sonarscope@ifremer.fr to correct this "otherwise" part of the sofrware or to suppress this message for your sounder.', 0, 'Tag', 'MessageSonar_range2depth');
        end
end
% RollCoef = 1;

%% Controles de validite

identRayTimeLength     = cl_image.indDataType('RayPathTravelTime');
identRayPathSampleNb   = cl_image.indDataType('RayPathSampleNb');
identRayPathTravelTime = cl_image.indDataType('RayPathTravelTime');
identRayPathLength     = cl_image.indDataType('RayPathLength'); % One way length in meters
identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
identTwoWayTravelTimeInSamples = cl_image.indDataType('TwoWayTravelTimeInSamples');

if this.GeometryType == cl_image.indGeometryType('PingSamples')
    status = 1;
else
    str1 = 'Pour r�aliser cette operation, il faut �tre positionn� sur un Layer de DataType="RayPathTravelTime"';
    str2 = 'This processing must be performed on a layer which DataType is "RayPathTravelTime".';
    status = testSignature(this, ...
        'DataType', [identRayTimeLength identRayPathSampleNb identRayPathTravelTime identRayPathLength identTwoWayTravelTimeInSeconds identTwoWayTravelTimeInSamples], ...
        'Message', Lang(str1,str2));
    if ~status
        c = [];
        return
    end
end

% if b.DataType ~= cl_image.indDataType('TxAngle');%  RxBeamAngle'); % CORRIGER CECI dans view
%     str = sprintf('La Layer de "RxBeamAngle" n''est pas bon');
%     msgbox(str, 'IFREMER')
%     return
% end

% Attention : sort(sublLM) peut etre utile pour supprimer les tests de
% flipud (voir cl_image/plus

[XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, b);
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

c(1) = this;
c(1) = replace_Image(c(1), NaN(length(suby), length(subx), 'single'));

c(2) = this;
c(2) = replace_Image(c(2), NaN(length(suby), length(subx), 'single'));

c(3) = this;
c(3) = replace_Image(c(3), NaN(length(suby), length(subx), 'single'));

Time = this.Sonar.Time;

if isempty(ProfilCelerite)
    BathyCel = this.Sonar.BathyCel;
    if isfield(BathyCel, 'Depth')
        BathyCel.Z = BathyCel.Depth;
    end
    if isempty(BathyCel.Z)
        this = init_BathyCel(this);
    end
end


TransducerDepth   = this.Sonar.Immersion;
SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
TxAlongAngle      = this.Sonar.TxAlongAngle;
Pitch             = this.Sonar.Pitch;
Heave             = this.Sonar.Heave;
Roll              = this.Sonar.Roll;

Heave = -Heave; % Pour sondeurs KM TODO : corriger ce truc � la source
%{
figure;
h(1) = subplot(3,1,1); plot(TxAlongAngle, '*'); grid on; title('TxAlongAngle');
h(2) = subplot(3,1,2); plot(Pitch, '*'); grid on; title('Pitch');
h(3) = subplot(3,1,3); plot(TxAlongAngle+Pitch, '*'); grid on; title('TxAlongAngle + Pitch');
linkaxes(h, 'x')
%}

TideValue = this.Sonar.Tide;
if isempty(TideValue)
    TideValue = zeros(length(TransducerDepth),1);
end

SamplingRate    = this.Sonar.SampleFrequency;
shipSettings    = this.Sonar.Ship;              % Ajout pour action dans rayTracingGLT
TimeTx          = this.Sonar.Time.timeMat;      % Ajout pour calcul au t_Rx

W = warning;
warning('off')

if ~isempty(ProfilCelerite)
    try
        %    TOUT CA A BESOIN DE BEAUCOUP DE CLARIFICATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        N = max(length(ProfilCelerite), length(ProfilCelerite.TimeStartOfUse));
        TimeStartOfUse = NaN(1,N);
        for k=1:N
            
            % URGENT, il faut normaliser cette affaire
            try
                TimeStartOfUse(k) = ProfilCelerite(k).TimeStartOfUse.timeMat;
                Formatage = 1;
            catch
                T = ProfilCelerite.TimeStartOfUse.timeMat;
                TimeStartOfUse(k) = T(k);
                Formatage = 2;
            end
        end
        TimeStartOfUse = cl_time('timeMat', TimeStartOfUse);
    catch %#ok<CTCH>
        ProfilCelerite = [];
    end
end

% AnglesVerif = NaN(length(suby), 1,'single');
M = length(suby);
hw = create_waitbar('Computing bathymetry', 'N', M);
for i=1:M
    my_waitbar(i, M, hw);
    if mod(i, floor(M/20)) == 1
        OnTrace = 1;
    else
        OnTrace = 0;
    end
    
    if ~isempty(ProfilCelerite)
        %         difTime = ProfilCelerite.TimeStartOfUse - Time(suby(i));
        difTime = TimeStartOfUse - Time(suby(i));
        difTime(difTime > 0) = [];
        k = length(difTime);
        if k == 0
            k = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end
        if Formatage == 1
            ZCelerite = ProfilCelerite(k).Depth;
            Celerite  = ProfilCelerite(k).SoundSpeed;
        else
            ZCelerite = ProfilCelerite.Depth(k,:);
            Celerite  = ProfilCelerite.SoundSpeed(k,:);
        end
    else
        if isfield(this.Sonar.BathyCel, 'Depth')
            ZCelerite = this.Sonar.BathyCel.Depth;
            Celerite  = this.Sonar.BathyCel.SoundSpeed;
        else
            ZCelerite = this.Sonar.BathyCel.Z;
            Celerite  = this.Sonar.BathyCel.C;
        end
    end
    
    if this.GeometryType == cl_image.indGeometryType('PingSamples')
        %         TempsSimple = abs(this.x(subx)) / (2*this.Sonar.SampleFrequency(suby(i)));
        TempsSimple = (abs(this.x(subx)) * this.Sonar.ResolutionD) / 1500 / 2; % Modifi� le 05/12/2010
    else
        switch this.DataType
            case identRayTimeLength
                TempsSimple =  abs(this.Image(suby(i), subx)) / (2*1000);
            case identRayPathSampleNb % SIMRAD
                TempsSimple = this.Image(suby(i), subx) / (2*SamplingRate(suby(i))); % Simrad
                
                
                
                
                % Grosse bidouille
            case identTwoWayTravelTimeInSeconds
                TempsSimple = this.Image(suby(i), subx) / 2;% abs(this.Image(suby(i), subx)) / (2*SamplingRate(suby(i))*1e-3/1500);
                %         ZCelerite = ZCelerite(1:100);
                %         Celerite = Celerite(1:100);
                
                
                
            case identRayPathLength % One way length in meters
                % ATTENTION : Voir si il ne faudrait pas prendre la moyenne de
                % la c�l�rit� sur la tranche d'eau au lieu de la valeur 1500
                % Voir comment est fait
                TempsSimple = abs(this.Image(suby(i), subx)) / 1500;
                
            otherwise
                my_warndlg('Message pour JMA : Ici je sais pas faire', 1);
        end
    end
    
    AngleIncident = b.Image(subyb(i), subxb); % - 8 * 0.12; % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    sub = find(~isnan(AngleIncident) & (TempsSimple > eps));
    if isempty(sub)
        continue
    end
    
    if isempty(TransducerDepth)
        Immersion = -1.5; % Donn�es .rdf Axel
        TransducerDepth = zeros(length(TideValue),1);
    else
        Immersion = TransducerDepth(suby(i));
    end
    
    if isempty(SurfaceSoundSpeed)
        CeleriteAntenne = 0;
    else
        CeleriteAntenne = SurfaceSoundSpeed(suby(i));
    end
    
    if isnan(Immersion)
        Immersion = -1.5;
    end
    Z = ZCelerite;
    C = Celerite;
    
    % CeleriteAntenne = 1493;
    % CeleriteAntenne = CeleriteAntenne + 3; % Baie de DSouarnenez 09/12/2008
    if CeleriteAntenne ~= 0
        C(Z >= (Immersion-0.1)) = CeleriteAntenne;
    end
    %     C(:) = CeleriteAntenne;
    % C = C + 3;
    
    % Test Goleta 11/11/2008
    % C(C > CeleriteAntenne) =  CeleriteAntenne;
    
    deltaT = 0;
    timeRx = double(TempsSimple(sub)) - deltaT;
    t_Rx = TimeTx(suby(i)) + timeRx / (24*3600);
    if all(isnan(Roll))
        RollRx = 0;
    else
        % Temps UTM � l'instant de R�ception
        
        % USE ROLL & PITCH FROM 1012 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        RollRx  = my_interp1(TimeTx, Roll, t_Rx)  + this.Sonar.Ship.MRU.RollCalibration;    % Roulis � l'instant de R�ception
    end
    
    if (RollCoef == 0) || isempty(this.Sonar.Roll)
        Teta = AngleIncident(sub);
    else
        if isempty(RollImage)
            %Teta = AngleIncident(sub) - RollCoef * (Roll(suby(i)) + this.Sonar.Ship.MRU.RollCalibration);
            %             Teta = AngleIncident(sub) + this.Sonar.Ship.MRU.RollCalibration; % Bizarre que �a marche comme �a
            Teta = AngleIncident(sub) + RollCoef * (RollRx + this.Sonar.Ship.MRU.RollCalibration); % Th�orique
            % Take Pitch into account to (projection)
        else
            iPingRoll = suby(i); % A faire proprement !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            RollPing = RollImage.Image(iPingRoll,:);
            Teta = AngleIncident(sub) + RollCoef * (RollPing + this.Sonar.Ship.MRU.RollCalibration);
        end
    end
    
    Teta = Teta + OffsetRoll;
    
    %     AnglesVerif(i,sub) = Teta - RollRx;
    
    if ~isempty(TransmitTiltAngleImage)
        iPingTilt = suby(i); % A faire proprement !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        pppp = TransmitTiltAngleImage.Image(iPingTilt, :);
    end
    
    %     deltaT = 0;
    %     timeRx = double(TempsSimple(sub)) - deltaT;
    
    if all(isnan(Z)) % Pas de profil de c�l�rit� % Modif pour comparaison fichiers Peter sans RayTracing (mais qui utilise la c�l�rit� de surface) !!!!!!!!!!!!!!!!!!!!!!
        valPitch = Pitch(suby(i)) + this.Sonar.Ship.MRU.PitchCalibration; %corrig�e des donn�es de calibration
        if isnan(valPitch)
            Xbathy =  (1500 * timeRx) .* sind(Teta);
            Zbathy = -(1500 * timeRx) .* cosd(Teta);
            Ybathy =  (1500 * timeRx) .* cosd(Teta) * 0;
        else
            Xbathy =  (1500 * timeRx) .* sind(Teta);
            Zbathy = -(1500 * timeRx) .* cosd(Teta) / cosd(valPitch);
            Ybathy =  (1500 * timeRx) .* cosd(Teta) * sind(valPitch);
        end
        
    else % Profil fourni
        
        %t_Rx    = TimeTx(suby(i)) + timeRx / (24*3600);                                     % Temps UTM � l'instant de R�ception
        PitchTx = Pitch(suby(i)) + this.Sonar.Ship.MRU.PitchCalibration;                    % Tangage � l'instant d'�mission corrig�e des donn�es de calibration
        if isnan(PitchTx)
            PitchTx = 0;
        end
        if all(isnan(Pitch))
            PitchRx = 0;
        else
            PitchRx = my_interp1(TimeTx, Pitch, t_Rx) + this.Sonar.Ship.MRU.PitchCalibration ;  % Tangage � l'instant de R�ception
        end
        %RollRx  = my_interp1(TimeTx, Roll, t_Rx)  + this.Sonar.Ship.MRU.RollCalibration ;    % Roulis � l'instant de R�ception
        
        
        if isempty(this.Sonar.TxAlongAngle)
            txAlongAngle = 0;
        else
            SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
            switch SonarName
                case 'Reson7150'
                    AngleLongitudinalAntenne = 2;
                otherwise
                    AngleLongitudinalAntenne = 0;
            end
            
            % figure; plot(this.Sonar.TxAlongAngle + this.Sonar.Pitch,'+'); grid
            txAlongAngle = AngleLongitudinalAntenne + this.Sonar.TxAlongAngle(suby(i)) + this.Sonar.Pitch(suby(i));
            % Bad combination : should compute in 2 steps !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        end
        
        coefPitch = 0; % 0 pour fichier EvalHydro/7111/Rock/20080220_001307.s7k
        coefPitch = -1; % 0 pour fichier EvalHydro/7111/Rock/20080220_001307.s7k
        %         RollCoef = 1;
        
        
        Teta = Teta-RollRx; % Pour les sondeurs KM Comment� le 05/12/2010
        
        
        if OnTrace
            rayTracingGLT(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx*RollCoef, txAlongAngle*coefPitch, shipSettings , 'Immersion', Immersion, 'Fig', 753943);
        end
        [Xbathy, Ybathy, Zbathy] = rayTracingGLT(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx*RollCoef, txAlongAngle*coefPitch, shipSettings , 'Immersion', Immersion);
    end
    
    subComplex = (imag(Zbathy) ~= 0);
    Zbathy(subComplex) = NaN;
    Xbathy(subComplex) = NaN;
    
    % ----------------------------
    % Calcul du pilonnement induit
    
    %{
TimeTx      = this.Sonar.Time.timeMat;
UTMtimeRx   = TimeTx(suby(i)) + timeRx / (24*3600);
p           = sonar_pilonnementInduit(this, suby(i), UTMtimeRx);
Zbathy      = Zbathy + p;
    %}
    
    if isempty(TransducerDepth)
        TransducerDepth = zeros(size(Heave), 'single');
    end
    if isempty(TideValue)
        TideValue = zeros(size(Heave), 'single');
    end
    if isnan(Heave(suby(i)))
        Zbathy = Zbathy + (TransducerDepth(suby(i)) - TideValue(suby(i)));
    else
        Zbathy = Zbathy + (TransducerDepth(suby(i)) - TideValue(suby(i))) + Heave(suby(i)); % (HeaveTx + HeaveRx)/2 !!!!!!!!!!!!!!!!!!!!!!!!!
    end
    
    %     angleAlong = angleAlong * (pi/180);
    %     if all(isnan(Heave))
    c(1).Image(i, sub) = Zbathy;
    %     else
    %         c(1).Image(i, sub) = Zbathy + Heave(suby(i));
    %     end
    %c(1).Image(i, sub) = Zbathy;
    c(2).Image(i, sub) = Xbathy;
    c(3).Image(i, sub) = Ybathy;
end
my_close(hw, 'MsgEnd')
warning(W)

% SonarScope(AnglesVerif);
% c(3).Image(:,:) = AnglesVerif;

%% Mise a jour de coordonnees

c(1) = majCoordonnees(c(1), subx, suby);
c(2) = majCoordonnees(c(2), subx, suby);
c(3) = majCoordonnees(c(3), subx, suby);

%% Calcul des statistiques

c(1) = compute_stats(c(1));
c(2) = compute_stats(c(2));
c(3) = compute_stats(c(3));

%% Rehaussement de contraste

c(1).TagSynchroContrast = num2str(rand(1));
c(2).TagSynchroContrast = num2str(rand(1));
c(3).TagSynchroContrast = num2str(rand(1));
CLim = [c(1).StatValues.Min c(1).StatValues.Max];
c(1).CLim          = CLim;
CLim = [c(2).StatValues.Min c(2).StatValues.Max];
c(2).CLim          = CLim;
CLim = [c(3).StatValues.Min c(3).StatValues.Max];
c(3).CLim          = CLim;
c(1).ColormapIndex    = 3;
c(2).ColormapIndex    = 3;
c(3).ColormapIndex    = 3;
c(1).DataType    = cl_image.indDataType('Bathymetry');
c(2).DataType    = cl_image.indDataType('AcrossDist');
c(3).DataType    = cl_image.indDataType('AlongDist');
c(1).Unit          = 'm';
c(2).Unit          = 'm';
c(3).Unit          = 'm';

%% Completion du titre

c(1).Name = [c(1).Name '_Computed'];
c(1) = update_Name(c(1));
c(2).Name = [c(2).Name '_Computed'];
c(2) = update_Name(c(2));
c(3).Name = [c(3).Name '_Computed'];
c(3) = update_Name(c(3));

%% Par ici la sortie

status = 1;

my_close(753943)
