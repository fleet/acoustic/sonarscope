function [flag, c] = sonar_range2depth_EM(this, RxAngle, RollRxImage, PitchRxImage, TiltTxImage, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, nbPlotPerImage] = getPropertyValue(varargin, 'nbPlotPerImage', 20); %#ok<ASGLU>

[~, SonarName] = get_SonarDescription(this);

% isXYZ = ~isempty(TiltTxImage);


% TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% R�cup�rer l'information "compensation du pitch dans runtime datagram)
%{
Yaw/Pitch Stab:    0x84
No yaw stabilization
Heading filter, medium
Pitch stabilization is on
%}


% Correctif � faire en amont (rencontr� dans le cas de donn�es Reson non
% pass�es par PDS
this.Sonar.Heave(abs(this.Sonar.Heave) > 100) = 0;


%% On v�rifie si le layer est bien de type "TwoWayTravelTimeInSeconds"

identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', identTwoWayTravelTimeInSeconds);
if ~flag
    return
end

%% Calcul des intersections

[~, ~, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, RxAngle);
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Pr�pration des images r�ceptrices

for k=5:-1:1
    c(k) = replace_Image(this, NaN(length(suby), length(subx), 'single'));
end

%% R�cup�ration des profils de s�curit�

Time = this.Sonar.Time;
TPings = Time.timeMat;
[SSP, indSSP] = getSSP(this, TPings(suby));
if isempty(SSP.ZCelerite)
    indSSP = [];
else
    subNaN    = find( isnan(indSSP));
    subNonNaN = find(~isnan(indSSP));
    indSSP(subNaN) = interp1(subNonNaN, indSSP(subNonNaN), subNaN, 'next', 'extrap');
end

%% R�cup�ration des signaux aux instants de transmission

TransducerDepth   = this.Sonar.Immersion;
SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
PitchTx           = this.Sonar.Pitch;
HeaveTx           = this.Sonar.Heave;
RollTx            = this.Sonar.Roll;
% TxAlongAngle    = this.Sonar.TxAlongAngle;

%% V�rification faite pour sondeurs Reson

if isempty(this.Sonar.Ship.Arrays.Transmit.Roll)
    this.Sonar.Ship.Arrays.Transmit.Roll    = 0;
    this.Sonar.Ship.Arrays.Transmit.Pitch   = 0;
    this.Sonar.Ship.Arrays.Transmit.Heading = 0;
    this.Sonar.Ship.Arrays.Transmit.Z       = 0;
    this.Sonar.Ship.Arrays.Receive.Z        = 0;
    this.Sonar.Ship.Arrays.Transmit.X       = 0;
    this.Sonar.Ship.Arrays.Transmit.Y       = 0;
    this.Sonar.Ship.Arrays.Transmit.Z       = 0;
    this.Sonar.Ship.MRU.X                   = 0;
    this.Sonar.Ship.MRU.Y                   = 0;
    this.Sonar.Ship.MRU.Z                   = 0;
    this.Sonar.Ship.GPS.X                   = 0;
    this.Sonar.Ship.GPS.Y                   = 0;
    this.Sonar.Ship.GPS.APS                 = 1;
end
% Fin verrue pour RESON71257 1UV DAURADE

%{
figure;
h(1) = subplot(3,1,1); plot(TxAlongAngle, '*'); grid on; title('TxAlongAngle');
h(2) = subplot(3,1,2); plot(PitchTx, '*'); grid on; title('PitchTx');
h(3) = subplot(3,1,3); plot(TxAlongAngle+PitchTx, '*'); grid on; title('TxAlongAngle + PitchTx');
linkaxes(h, 'x')
%}

% SamplingRate = this.Sonar.SampleFrequency(:,:);
% shipSettings = this.Sonar.Ship;              % Ajout pour action dans rayTracingGLT
% TimeTx       = this.Sonar.Time.timeMat;      % Ajout pour calcul au t_Rx

W = warning;
warning('off')
str1 = 'Calcul de la bathym�trie';
str2 = 'Computing Bathymetry';
strLoop = Lang(str1,str2);
M = length(suby);
for k=1:M
    infoLoop(strLoop, k, M, 'pings')
    
    if mod(k, floor(M/nbPlotPerImage)) == 1
        OnTrace = 1;
    else
        OnTrace = 0;
    end
    
    %% Simplification du profil de c�l�rit�
    
    Immersion = TransducerDepth(suby(k), 1);
    if isempty(indSSP) % TODO : investiguer donn�es Norbit
        Z = [0 12000];
        C = [1500 1500];
    else
        [Z, C] = simplificationSSP(SSP(indSSP(k)), Immersion, SurfaceSoundSpeed(suby(k), 1));
    end
    
    %% Identificatrion des faisceau b�bord et tribord si sondeur dual
    
    switch SonarName % TODO : comment distinguer EM2040S et EM2040S ?
        case {'EM3002S'; 'EM1002'; 'EM710'; 'EM122'; 'EM2040'; 'EM2045'; 'ME70'; 'EM2040S'; 'EM300'; 'EM302'}
            subBab = subx;
            subTri = [];
        case 'EM3002D'
            subBab = find(subx <= (this.nbColumns/2));
            subTri = find(subx >  (this.nbColumns/2));
        case {'Reson7111'; 'Reson7150'; 'Reson7125_200kHz'; 'Reson7150_12kHz'; 'Reson7150_24kHz'}
            subBab = subx;
            subTri = [];
        case 'Norbit'
            subBab = subx;
            subTri = [];
        otherwise
            str1 = sprintf('%s pas dans la liste de case sonar_range2depth_EM, envoyez ce message � sonarscope@ifremer.fr', SonarName);
            str2 = sprintf('%s not in sonar_range2depth_EM yet, please send this message to sonarscope@ifremer.fr', SonarName);
            my_warndlg(Lang(str1,str2), 1);
    end
    
    %% R�cup�ration du temps de parcours aller
    
    TempsSimple = this.Image(suby(k), subx) / 2;
    timeRx = double(TempsSimple);
    
    %% R�cup�ration des angles de pointage des faisceaux
    
    BeamPointingAngleVersusHull = RxAngle.Image(subyb(k), subxb);
    sub = find(~isnan(BeamPointingAngleVersusHull) & (TempsSimple > eps));
    if isempty(sub)
        continue
    end
    subBab = subBab(~isnan(BeamPointingAngleVersusHull(subBab)) & (TempsSimple(subBab) > eps));
    if ~isempty(subTri)
        subTri = subTri(~isnan(BeamPointingAngleVersusHull(subTri)) & (TempsSimple(subTri) > eps));
    end
    BeamPointingAngleVersusHull = BeamPointingAngleVersusHull(sub);
    
    
    if isempty(RollRxImage)
        RollRx_deg   = zeros(1,length(subx));
        PitchRx_deg  = zeros(1,length(subx));
    else
        RollRx_deg  = RollRxImage.Image(suby(k),:);
        PitchRx_deg = PitchRxImage.Image(suby(k),:);
    end
    
    if isempty(TiltTxImage)
        TiltTx_deg  = zeros(size(RollRx_deg));
        %         TiltTx_deg(:) = 0.471; % Test Reson7111
    else
        TiltTx_deg  = TiltTxImage.Image(suby(k),:);
    end
    if all(isnan(RollRx_deg(sub)))
        continue
    end
    
    RollTx_deg  = RollTx(suby(k));
    PitchTx_deg = PitchTx(suby(k));
    
    RollMounting_rd  = NaN(1, length(subx));
    PitchMounting_rd = NaN(1, length(subx));
    YawMounting_rd   = NaN(1, length(subx));
    
    RollMounting_rd( subBab) = -this.Sonar.Ship.Arrays.Transmit.Roll(1) * (pi/180);
    PitchMounting_rd(subBab) = -this.Sonar.Ship.Arrays.Transmit.Pitch(1) * (pi/180); % Tr�s l�g�rement mieux sur analyse AlongDistance
    %     YawMounting_rd(subBab)   = this.Sonar.Ship.Arrays.Transmit.Heading(1) * (pi/180);
    if ~isempty(subTri)
        RollMounting_rd( subTri) = -this.Sonar.Ship.Arrays.Transmit.Roll(2) * (pi/180); % Pour EM3002S
        PitchMounting_rd(subTri) = -this.Sonar.Ship.Arrays.Transmit.Pitch(2) * (pi/180);
        %         YawMounting_rd(subTri)   = this.Sonar.Ship.Arrays.Transmit.Heading(2) * (pi/180);
    end
%     RollMounting_rdTest = RollMounting_rd;
    
    
    % Comment� pour EM3002S
    %     RollMounting_rd(:) = 0; % Already taken into account in Simrad_AnglesAntenna2ShipFloor when importing the raw angles / arrays
    
    
    
    % PitchMounting_rd(:) = 0;
    
    if ~isfield(this.Sonar.Ship, 'GyrocompassHeadingOffset') % Reson
        this.Sonar.Ship.GyrocompassHeadingOffset = 0;
    end
    YawMounting_rd(subBab) = ( this.Sonar.Ship.Arrays.Transmit.Heading(1) - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Avant
    
    %     YawMounting_rd(subBab) = (-this.Sonar.Ship.Arrays.Transmit.Heading(1) + this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = ( this.Sonar.Ship.Arrays.Transmit.Heading(1) + this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (-this.Sonar.Ship.Arrays.Transmit.Heading(1) - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (0                                          - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (0                                          + this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (-this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180); % Nemestra
    %      YawMounting_rd(subBab) = 0; % Nemestra
    %     YawMounting_rd(subBab) = (2*this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180);
    %     YawMounting_rd(subBab) = (-2*this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180);
    
    if ~isempty(subTri)
        YawMounting_rd(subTri) = (this.Sonar.Ship.Arrays.Transmit.Heading(2) - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180);
    end
    
    Xbathy        = NaN(1,length(subx));
    Ybathy        = NaN(1,length(subx));
    Zbathy        = NaN(1,length(subx));
    AngleIntersec = NaN(1,length(subx));
    Azimuth       = NaN(1,length(subx));
    zoffArray     = NaN(1,length(subx));
    
    Teta_rd    = BeamPointingAngleVersusHull * (pi/180);
    RollTx_rd  = RollTx_deg  * (pi/180);
    PitchTx_rd = PitchTx_deg * (pi/180);
    TiltTx_rd  = TiltTx_deg  * (pi/180);
    RollRx_rd  = RollRx_deg  * (pi/180);
    PitchRx_rd = PitchRx_deg * (pi/180);
    
    % Avant
    %{
if isXYZ % EM122, EM710; EM302, EM2040
switch SonarName
case 'ME70'
% Do not modify Teta_rd
otherwise
%                 Teta_rd = Teta_rd - RollRx_rd(sub); % Tr�s bizarre, on fait d�j� -RollRx dans cl_simrad_all/import_PingBeam_All_V2 !!!!!
RollRx_rd(:) = 0;
end
else
switch SonarName
case {'Reson7111'; 'Reson7150'; 'Reson7125_200kHz'} % TODO 12/24
Teta_rd = Teta_rd - RollRx_rd(sub);
%                 RollRx_rd(:) = 0;
PitchTx_rd = -PitchTx_rd; % A conserver

PitchMounting_rd = -PitchMounting_rd; % Am�lioration pas spectaculaire
TiltTx_rd = -PitchRx_rd;
otherwise % EM1002, EM3002
RollRx_rd(:) = 0;
end
end
    %}
    
    %  Maintenant
    switch SonarName
        case 'ME70'
            RollMounting_rd = -RollMounting_rd;
            % Do not touch anything
            
        case {'Reson7111'; 'Reson7150'; 'Reson7125_200kHz'} % TODO 12/24
            Teta_rd = Teta_rd - RollRx_rd(sub);
            %                 RollRx_rd(:) = 0;
            PitchTx_rd = -PitchTx_rd; % A conserver
            
            PitchMounting_rd = -PitchMounting_rd; % Am�lioration pas spectaculaire
            TiltTx_rd = -PitchRx_rd;
            
        case 'EM3002S'
            RollRx_rd(:) = 0;
            
        case 'Norbit'
            HeaveTx(suby(k)) = - HeaveTx(suby(k));
            RollRx_rd = RollRx_rd - RollTx_rd; % Test JMA le 05/12/2019
%             Teta_rd = Teta_rd - RollRx_rd(sub);
            %                 RollRx_rd(:) = 0;
            PitchTx_rd = -PitchTx_rd; % A conserver
            
            PitchMounting_rd = -PitchMounting_rd; % Am�lioration pas spectaculaire
            TiltTx_rd = -PitchRx_rd;
            
        otherwise % EM1002, EM3002, EM122, etc ...
            RollMounting_rd(:) = 0;
            RollRx_rd(:) = 0;
    end
    
    zoffArray(subBab) = (this.Sonar.Ship.Arrays.Transmit.Z(1) + this.Sonar.Ship.Arrays.Receive.Z(1)) / 2;
    if ~isempty(subTri)
        zoffArray(subTri) = (this.Sonar.Ship.Arrays.Transmit.Z(2) + this.Sonar.Ship.Arrays.Receive.Z(2)) / 2;
    end
    zoffMRU   = this.Sonar.Ship.MRU.Z;
    if isempty(zoffMRU)
        zoffMRU = 0; % Cas rencontr� sur fichier NIWA 0399_20150908_045454 o� VSN=0 alors qu'il y a des MRU
    end
    if OnTrace
        Title = sprintf('%s - Ping %d / %d', extract_ImageName(this), k, M);
        
        rayTracingPitchCompensation(Z, C, Teta_rd, timeRx(sub), -TiltTx_rd(sub), ...
            RollTx_rd, PitchTx_rd, RollRx_rd(sub), PitchRx_rd(sub), ...
            RollMounting_rd(sub), PitchMounting_rd(sub), YawMounting_rd(sub), ...
            zoffArray(sub), zoffMRU, ...
            'Immersion', Immersion, 'Fig', 753943, 'Title', Title);
        drawnow
        %{
% Test 20160225
BeamPoitingAnglesVsArrays = Teta_rd - RollMounting_rdTest;
rayTracingPitchCompensation(Z, C, BeamPoitingAnglesVsArrays, timeRx(sub), -TiltTx_rd(sub), ...
RollTx_rd, PitchTx_rd, RollRx_rd(sub), PitchRx_rd(sub), ...
RollMounting_rdTest(sub), PitchMounting_rd(sub), YawMounting_rd(sub), ...
zoffArray, zoffMRU, ...
'Immersion', Immersion, 'Fig', 753943, 'isXYZ', isXYZ, 'Title', Title);
        %}
    end
    [Xbathy(sub), Ybathy(sub), Zbathy(sub), AngleIntersec(sub), Azimuth(sub)] = rayTracingPitchCompensation(...
        Z, C, Teta_rd, timeRx(sub), -TiltTx_rd(sub), ...
        RollTx_rd, PitchTx_rd, RollRx_rd(sub), PitchRx_rd(sub), ...
        RollMounting_rd(sub), PitchMounting_rd(sub), YawMounting_rd(sub), ...
        zoffArray(sub), zoffMRU, ...
        'Immersion', Immersion);
    
    %{
% Test 20160225
% Les across distances et along distances sont apparemment tr�s bien contenus mais avec une pente tr�s importante
% qui est en relation avec l'angle de montage : tr�s visible pour sondeur EM3002D)
BeamPoitingAnglesVsArrays = Teta_rd - RollMounting_rdTest(sub);
[Xbathy(sub), Ybathy(sub), Zbathy(sub), AngleIntersec(sub), Azimuth(sub)] = rayTracingPitchCompensation(Z, C, BeamPoitingAnglesVsArrays, timeRx(sub), -TiltTx_rd(sub), ...
RollTx_rd, PitchTx_rd, RollRx_rd(sub), PitchRx_rd(sub), ...
RollMounting_rdTest(sub), PitchMounting_rd(sub), YawMounting_rd(sub), ...
zoffArray, zoffMRU, ...
'Immersion', Immersion, 'Fig', 753943, 'isXYZ', isXYZ, 'Title', Title);
        %}
        
        subComplex = (imag(Zbathy) ~= 0);
        Zbathy(subComplex) = NaN;
        Xbathy(subComplex) = NaN;
        Ybathy(subComplex) = NaN;
        
        switch SonarName
            case {'Reson7111'; 'Reson7150'; 'Reson7125_200kHz'} % TODO 12/24
%                 X = bsxfun(@times, Zbathy, sin(PitchRx_rd));
                %             X = bsxfun(@times, Zbathy, sin(PitchTx_rd));
                
                %             FigUtils.createSScFigure(87678);
                %             plot(Ybathy, 'r'); grid on; hold on; plot(X, 'b'); plot(Ybathy + X, 'k'); hold off;
                
                %              Ybathy = Ybathy + X; % TODO : en test
        end
        
        %% Calcul du pilonnement induit
        
        LevelArmX = NaN(1,length(subx));
        LevelArmY = NaN(1,length(subx));
        LevelArmZ = NaN(1,length(subx));
        LevelArmX(subBab) = this.Sonar.Ship.Arrays.Transmit.X(1) - this.Sonar.Ship.MRU.X;
        LevelArmY(subBab) = this.Sonar.Ship.Arrays.Transmit.Y(1) - this.Sonar.Ship.MRU.Y;
        LevelArmZ(subBab) = this.Sonar.Ship.Arrays.Transmit.Z(1) - this.Sonar.Ship.MRU.Z;
        if ~isempty(subTri)
            LevelArmX(subTri) = this.Sonar.Ship.Arrays.Transmit.X(2) - this.Sonar.Ship.MRU.X;
            LevelArmY(subTri) = this.Sonar.Ship.Arrays.Transmit.Y(2) - this.Sonar.Ship.MRU.Y;
            LevelArmZ(subTri) = this.Sonar.Ship.Arrays.Transmit.Z(2) - this.Sonar.Ship.MRU.Z;
        end
        
        ZOffset = zoffArray;
        [DxTx, DyTx, DzTx] = XyzTranslation(LevelArmX, LevelArmY, LevelArmZ, RollTx_rd, -PitchTx_rd, 0, ZOffset);
        [DxRx, DyRx, DzRx] = XyzTranslation(LevelArmX, LevelArmY, LevelArmZ, RollRx_rd, -PitchRx_rd, 0, ZOffset);
        
        Zbathy = Zbathy + (DzTx + DzRx) / 2;
        Xbathy = Xbathy + (DxTx + DxRx) / 2;
        Ybathy = Ybathy + (DyTx + DyRx) / 2;
        
        Xbathy = Xbathy - this.Sonar.Ship.GPS.X(this.Sonar.Ship.GPS.APS);
        Ybathy = Ybathy - this.Sonar.Ship.GPS.Y(this.Sonar.Ship.GPS.APS);
        
        Zbathy(subBab) = Zbathy(subBab) - this.Sonar.Ship.Arrays.Transmit.Z(1);
        if ~isempty(subTri)
            Zbathy(subTri) = Zbathy(subTri) - this.Sonar.Ship.Arrays.Transmit.Z(2);
        end
        
        %     Heave = (HeaveTx(suby(k)) + HeaveRx) / 2;
        
        if isempty(TransducerDepth)
            TransducerDepth = zeros(size(Heave), 'single');
        end
        
        if isnan(HeaveTx(suby(k)))
            Zbathy(subBab) = Zbathy(subBab) - TransducerDepth(suby(k),1);
            if ~isempty(subTri)
                Zbathy(subTri) = Zbathy(subTri) - TransducerDepth(suby(k),2);
            end
        else % Correct for file 0000_20150610_221327_Belgica
            Zbathy(subBab) = Zbathy(subBab) - (TransducerDepth(suby(k),1)) - HeaveTx(suby(k)); % (HeaveTx + HeaveRx)/2
            if ~isempty(subTri)
                Zbathy(subTri) = Zbathy(subTri) - (TransducerDepth(suby(k), 2)) - HeaveTx(suby(k)); % (HeaveTx + HeaveRx)/2 !!!!!!!!!!!!!!!!!!!!!!!!!
                Zbathy(subTri) = Zbathy(subTri) + 2 * (TransducerDepth(suby(k), 2) - TransducerDepth(suby(k), 1)); % Bizare mais c'est �a qui fonctionne !!!
            end
        end
        
        AngleIntersec(sub) = AngleIntersec(sub) .* sign(BeamPointingAngleVersusHull);
        
        %     FigUtils.createSScFigure(5128); PlotUtils.createSScPlot(AngleIntersec, '-'); grid on; drawnow
        
        %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Xbathy, Ybathy, '*'); grid on;
        
        c(1).Image(k,:) = Zbathy;
        c(2).Image(k,:) = Xbathy;
        c(3).Image(k,:) = Ybathy;
        c(4).Image(k,:) = AngleIntersec;
        c(5).Image(k,:) = Azimuth;
end
warning(W)

%% Finalisation des images

for k=1:5
    c(k) = majCoordonnees(c(k), subx, suby);
    c(k) = compute_stats(c(k));
    c(k).TagSynchroContrast = num2str(rand(1));
    c(k).CLim = [c(k).StatValues.Min c(k).StatValues.Max];
    c(k).ColormapIndex = 3;
end

c(1).DataType = cl_image.indDataType('Bathymetry');
c(2).DataType = cl_image.indDataType('AcrossDist');
c(3).DataType = cl_image.indDataType('AlongDist');
c(4).DataType = cl_image.indDataType('TxAngle');
c(5).DataType = cl_image.indDataType('Heading');
c(1).Unit = 'm';
c(2).Unit = 'm';
c(3).Unit = 'm';
c(4).Unit = 'deg';
c(5).Unit = 'deg';
c(5).CLim = [0 360];
c(5).ColormapIndex = 5;

%% Compl�tion du titre

for k=1:5
    c(k).Name = [c(k).Name '_Computed'];
    c(k) = update_Name(c(k));
end
c = c(5:-1:1);

%% Par ici la sortie

flag = 1;
my_close(753943)


function [SSP, indSSP] = getSSP(this, Time)

SSP    = [];
indSSP = [];

%% Premi�re bricole infame

BathyCel = this.Sonar.BathyCel;
if isfield(BathyCel, 'Depth')
    BathyCel.Z = BathyCel.Depth;
end
if isempty(BathyCel.Z)
    this = init_BathyCel(this);
end
ProfilCelerite = this.Sonar.BathyCel;
if isempty(ProfilCelerite)
    return
end

%% Deuxi�me chose bizarre

try
    %    TOUT CA A BESOIN DE BEAUCOUP DE CLARIFICATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    N = max(length(ProfilCelerite), length(ProfilCelerite.TimeStartOfUse));
    TimeStartOfUse = NaN(1,N);
    for k2=1:N
        
        % URGENT, il faut normaliser cette affaire
        try
            TimeStartOfUse(k2) = ProfilCelerite(k2).TimeStartOfUse.timeMat;
            Formatage = 1;
        catch
            T = ProfilCelerite.TimeStartOfUse.timeMat;
            TimeStartOfUse(k2) = T(k2);
            Formatage = 2;
        end
    end
    %         TimeStartOfUse = cl_time('timeMat', TimeStartOfUse);
catch %#ok<CTCH>
    TimeStartOfUse = 0;
    Formatage = 1;
end

%% Calcul des index

indSSP = my_interp1_Extrap_PreviousThenNext(TimeStartOfUse, 1:length(TimeStartOfUse), Time);
indSSP(indSSP == 0) = 1;

k2 = 0;
for k=1:max(indSSP)
    if Formatage == 1
        SSPOne.ZCelerite = ProfilCelerite(k).Z;
        SSPOne.Celerite  = ProfilCelerite(k).C;
    else
        try
            SSPOne.ZCelerite = ProfilCelerite.Depth(k,:);
            SSPOne.Celerite  = ProfilCelerite.SoundSpeed(k,:);
        catch
            SSPOne.ZCelerite = ProfilCelerite.Z(k,:);
            SSPOne.Celerite  = ProfilCelerite.C(k,:);
        end
    end
    
    Z = SSPOne.ZCelerite;
    C = SSPOne.Celerite;
    if all(isnan(Z))
        continue
    end
    subNaN = isnan(Z) | isnan(C);
    Z(subNaN) = [];
    C(subNaN) = [];
    
    k2 = k2 + 1;
    SSP(k2).ZCelerite = Z; %#ok<AGROW>
    SSP(k2).Celerite  = C; %#ok<AGROW>
    
    SSP(k2).ZCelerite = SSP(k2).ZCelerite(:); %#ok<AGROW> % force le stockage en colonne
    SSP(k2).Celerite  = SSP(k2).Celerite(:);  %#ok<AGROW> % force le stockage en colonne
    
    if SSP(k2).ZCelerite(end) > SSP(k2).ZCelerite(1)
        SSP(k2).ZCelerite = flipud(SSP(k2).ZCelerite); %#ok<AGROW>
        SSP(k2).Celerite  = flipud(SSP(k2).Celerite); %#ok<AGROW>
    end
end
if isempty(SSP)
    this = init_BathyCel(this);
    ProfilCelerite = this.Sonar.BathyCel;
    SSP.ZCelerite = ProfilCelerite.Z;
    SSP.Celerite  = ProfilCelerite.C;
end


function [Z, C] = simplificationSSP(SSP, Immersion, SurfaceSoundSpeed)

if isempty(SurfaceSoundSpeed)
    CeleriteAntenne = 0;
else
    CeleriteAntenne = SurfaceSoundSpeed;
end

Z = SSP.ZCelerite;
C = SSP.Celerite;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(C, Z, '-*b'); grid on;

if isnan(CeleriteAntenne)
    return
end

if CeleriteAntenne ~= 0
    C(Z >= (Immersion-1)) = CeleriteAntenne;
    %     C(Z >= (Immersion)) = CeleriteAntenne;
end
% hold on; PlotUtils.createSScPlot(C, Z, '-or');
% PlotUtils.createSScPlot([min(C) max(C)], [Immersion Immersion], 'k')
