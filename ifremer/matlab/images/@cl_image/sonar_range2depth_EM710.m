function [flag, c] = sonar_range2depth_EM710(this, TxAngle, RollRxImage, PitchRxImage, TiltTxImage, varargin)

[~, SonarName] = get_SonarDescription(this);

isXYZ = ~isempty(TiltTxImage);

% Correctif � faire en amont (rencontr� dans le cas de donn�es Reson non
% pass�es par PDS
this.Sonar.Heave(abs(this.Sonar.Heave) > 100) = 0;

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% On v�rifie si le layer est bien de type "RxBeamAngle"

identRayPathSampleNb           = cl_image.indDataType('RayPathSampleNb');
identTwoWayTravelTimeInSeconds = cl_image.indDataType('TwoWayTravelTimeInSeconds');
flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingBeam'), 'DataType', [identRayPathSampleNb identTwoWayTravelTimeInSeconds]);
if ~flag
    return
end

%% Calcul des intersections

[~, ~, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, TxAngle);
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Pr�pration des images r�ceptrices

for k=5:-1:1
    c(k) = replace_Image(this, NaN(length(suby), length(subx), 'single'));
end

%% R�cup�ration des profils de s�curit�

Time = this.Sonar.Time;
TPings = Time.timeMat;
[SSP, indSSP] = getSSP(this, TPings(suby));

%% R�cup�ration des signaux aux instants de transmission

TransducerDepth   = this.Sonar.Immersion;
SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
PitchTx           = this.Sonar.Pitch;
HeaveTx           = this.Sonar.Heave;
RollTx            = this.Sonar.Roll;
% TxAlongAngle    = this.Sonar.TxAlongAngle;

%{
figure;
h(1) = subplot(3,1,1); plot(TxAlongAngle, '*'); grid on; title('TxAlongAngle');
h(2) = subplot(3,1,2); plot(PitchTx, '*'); grid on; title('PitchTx');
h(3) = subplot(3,1,3); plot(TxAlongAngle+PitchTx, '*'); grid on; title('TxAlongAngle + PitchTx');
linkaxes(h, 'x')
%}

SamplingRate = this.Sonar.SampleFrequency(:,:);
% shipSettings = this.Sonar.Ship;              % Ajout pour action dans rayTracingGLT
% TimeTx       = this.Sonar.Time.timeMat;      % Ajout pour calcul au t_Rx

W = warning;
warning('off')
M = length(suby);
hw = create_waitbar('Computing bathymetry', 'N', M);

% AnglesVerif = NaN(length(suby), 1,'single');
for k=1:M
    my_waitbar(k, M, hw);
    if mod(k, floor(M/20)) == 1
        OnTrace = 1;
    else
        OnTrace = 0;
    end
    
    %% Simplification du profil de c�l�rit�
    
    Immersion = TransducerDepth(suby(k), 1);
    [Z, C] = simplificationSSP(SSP(indSSP(k)), Immersion, SurfaceSoundSpeed(suby(k), 1));
    
    %% Identificatrion des faisceau b�bord et tribord si sondeur dual
    
    switch SonarName % TODO : comment distinguer EM2040S et EM2040S ?
        case {'EM3002S'; 'EM1002'; 'EM710'; 'EM122'; 'EM2040'}
            subBab = subx;
            subTri = [];
        case 'EM3002D'
            subBab = find(subx <= (this.nbColumns/2));
            subTri = find(subx >  (this.nbColumns/2));
    end
    
    %% R�cup�ration du temps de parcours aller
    
    [flag, TempsSimple] = computeOneWayTravelTimeInSecondes(this, 'suby', suby(k), 'subx', subx);
    if ~flag
        continue
    end
    timeRx = double(TempsSimple);
    
    %% R�cup�ration des angles de pointage des faisceaux
    
    BeamPointingAngleVersusHull = TxAngle.Image(subyb(k), subxb);
    sub = find(~isnan(BeamPointingAngleVersusHull) & (TempsSimple > eps));
    if isempty(sub)
        continue
    end
    subBab = subBab(~isnan(BeamPointingAngleVersusHull(subBab)) & (TempsSimple(subBab) > eps));
    if ~isempty(subTri)
        subTri = subTri(~isnan(BeamPointingAngleVersusHull(subTri)) & (TempsSimple(subTri) > eps));
    end
    BeamPointingAngleVersusHull = BeamPointingAngleVersusHull(sub);
    
    
    RollRx_deg  = RollRxImage.Image(suby(k),:);
    PitchRx_deg = PitchRxImage.Image(suby(k),:);
    if isempty(TiltTxImage)
        TiltTx_deg  = zeros(size(RollRx_deg));
    else
        TiltTx_deg  = TiltTxImage.Image(suby(k),:);
    end
    if all(isnan(RollRx_deg(sub)))
        continue
    end
    
    RollTx_deg  = RollTx(suby(k)) + this.Sonar.Ship.MRU.RollCalibration;      % Tangage � l'instant d'�mission corrig�e des donn�es de calibration
    PitchTx_deg = PitchTx(suby(k)); % + this.Sonar.Ship.MRU.PitchCalibration; % Tangage � l'instant d'�mission corrig�e des donn�es de calibration
    
    RollMounting_rd  = NaN(1, length(subx));
    PitchMounting_rd = NaN(1, length(subx));
    YawMounting_rd   = NaN(1, length(subx));
    
    RollMounting_rd( subBab) = this.Sonar.Ship.Arrays.Transmit.Roll(1) * (pi/180); % Pour EM122
    %     RollMounting_rd( subBab) = -this.Sonar.Ship.Arrays.Transmit.Roll(1) * (pi/180); % Pour EM3002S
    
    %     PitchMounting_rd(subBab) = this.Sonar.Ship.Arrays.Transmit.Pitch(1) * (pi/180); % Avant
    %     PitchMounting_rd(subBab) = 0;
    PitchMounting_rd(subBab) = -this.Sonar.Ship.Arrays.Transmit.Pitch(1) * (pi/180); % Tr�s l�g�rement mieux sur analyse AlongDistance
    
    %     YawMounting_rd(subBab)   = this.Sonar.Ship.Arrays.Transmit.Heading(1) * (pi/180);
    
    if ~isempty(subTri)
        %         PitchRx_deg(subTri)      = PitchRx_deg(subTri) + this.Sonar.Ship.Arrays.Transmit.Pitch(2);
        %         RollMounting_rd( subTri) = this.Sonar.Ship.Arrays.Transmit.Roll(2)    * (pi/180); % Avant
        RollMounting_rd( subTri) = -this.Sonar.Ship.Arrays.Transmit.Roll(2) * (pi/180); % Pour EM3002S
        PitchMounting_rd(subTri) = this.Sonar.Ship.Arrays.Transmit.Pitch(2) * (pi/180);
        %         YawMounting_rd(subTri)   = this.Sonar.Ship.Arrays.Transmit.Heading(2) * (pi/180);
    end
    
    %     RollMounting_rd(:) = 0; % Car d�j� int�gr� � l'import de l'image
    
    
    % TODO : recomment� pour test EM2002S
    %     RollMounting_rd(:) = 0; % Car d�j� int�gr� � l'import de l'image
    
    
    % PitchMounting_rd(:) = 0;
    
    YawMounting_rd(subBab) = ( this.Sonar.Ship.Arrays.Transmit.Heading(1) - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Avant
    %     YawMounting_rd(subBab) = (-this.Sonar.Ship.Arrays.Transmit.Heading(1) + this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = ( this.Sonar.Ship.Arrays.Transmit.Heading(1) + this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (-this.Sonar.Ship.Arrays.Transmit.Heading(1) - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (0                                          - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (0                                          + this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (-this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180); % Nemestra
    %     YawMounting_rd(subBab) = (this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180); % Nemestra
    %      YawMounting_rd(subBab) = 0; % Nemestra
    %     YawMounting_rd(subBab) = (2*this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180);
    %     YawMounting_rd(subBab) = (-2*this.Sonar.Ship.Arrays.Transmit.Heading(1)) * (pi/180);
    
    if ~isempty(subTri)
        YawMounting_rd(subTri) = (this.Sonar.Ship.Arrays.Transmit.Heading(2) - this.Sonar.Ship.GyrocompassHeadingOffset) * (pi/180);
    end
    
    Xbathy        = NaN(1,length(subx));
    Ybathy        = NaN(1,length(subx));
    Zbathy        = NaN(1,length(subx));
    AngleIntersec = NaN(1,length(subx));
    Azimuth       = NaN(1,length(subx));
    
    Teta_rd    = BeamPointingAngleVersusHull  * (pi/180);
    RollTx_rd  = RollTx_deg  * (pi/180);
    PitchTx_rd = PitchTx_deg * (pi/180);
    TiltTx_rd  = TiltTx_deg  * (pi/180);
    RollRx_rd  = RollRx_deg  * (pi/180);
    PitchRx_rd = PitchRx_deg * (pi/180);
    
    if isXYZ % EM122, EM710; EM302, EM2040
        Teta_rd = Teta_rd - RollRx_rd(sub); % Tr�s bizarre, on fait d�j� -RollRx dans cl_simrad_all/cew_depth_V2 !!!!!
    else % EM1002, EM3002
        Teta_rd = Teta_rd - RollRx_rd(sub);
        RollRx_rd(:) = 0;
    end
    
    if OnTrace
        rayTracingPitchCompensation(Z, C, Teta_rd, timeRx(sub), -TiltTx_rd(sub), ...
            RollTx_rd, PitchTx_rd, RollRx_rd(sub), PitchRx_rd(sub), ...
            RollMounting_rd(sub), PitchMounting_rd(sub), YawMounting_rd(sub), ...
            'Immersion', Immersion, 'Fig', 753943, 'isXYZ', isXYZ);
    end
    [Xbathy(sub), Ybathy(sub), Zbathy(sub), AngleIntersec(sub), Azimuth(sub)] = rayTracingPitchCompensation(...
        Z, C, Teta_rd, timeRx(sub), -TiltTx_rd(sub), ...
        RollTx_rd, PitchTx_rd, RollRx_rd(sub), PitchRx_rd(sub), ...
        RollMounting_rd(sub), PitchMounting_rd(sub), YawMounting_rd(sub), ...
        'Immersion', Immersion, 'isXYZ', isXYZ);
    
    subComplex = (imag(Zbathy) ~= 0);
    Zbathy(subComplex) = NaN;
    Xbathy(subComplex) = NaN;
    
    %% Calcul du pilonnement induit
    
    LevelArmX = NaN(1,length(subx));
    LevelArmY = NaN(1,length(subx));
    LevelArmZ = NaN(1,length(subx));
    LevelArmX(subBab) = this.Sonar.Ship.Arrays.Transmit.X(1) - this.Sonar.Ship.MRU.X;
    LevelArmY(subBab) = this.Sonar.Ship.Arrays.Transmit.Y(1) - this.Sonar.Ship.MRU.Y;
    LevelArmZ(subBab) = this.Sonar.Ship.Arrays.Transmit.Z(1) - this.Sonar.Ship.MRU.Z;
    if ~isempty(subTri)
        LevelArmX(subTri) = this.Sonar.Ship.Arrays.Transmit.X(2) - this.Sonar.Ship.MRU.X;
        LevelArmY(subTri) = this.Sonar.Ship.Arrays.Transmit.Y(2) - this.Sonar.Ship.MRU.Y;
        LevelArmZ(subTri) = this.Sonar.Ship.Arrays.Transmit.Z(2) - this.Sonar.Ship.MRU.Z;
    end
    [DxTx, DyTx, DzTx] = XyzTranslation(LevelArmX, LevelArmY, LevelArmZ, RollTx_rd, PitchTx_rd, 0);
    [DxRx, DyRx, DzRx] = XyzTranslation(LevelArmX, LevelArmY, LevelArmZ, RollRx_rd, PitchRx_rd,  0);
    Xbathy = Xbathy + (DxTx + DxRx) / 2;
    Ybathy = Ybathy + (DyTx + DyRx) / 2;
    Zbathy = Zbathy + (DzTx + DzRx) / 2;
    
    Xbathy = Xbathy - this.Sonar.Ship.GPS.X(this.Sonar.Ship.GPS.APS);
    Ybathy = Ybathy - this.Sonar.Ship.GPS.Y(this.Sonar.Ship.GPS.APS);
    
    Zbathy(subBab) = Zbathy(subBab) - this.Sonar.Ship.Arrays.Transmit.Z(1);
    if ~isempty(subTri)
        Zbathy(subTri) = Zbathy(subTri) - this.Sonar.Ship.Arrays.Transmit.Z(2);
    end
    
    %     Heave = (HeaveTx(suby(k)) + HeaveRx) / 2;
    
    if isempty(TransducerDepth)
        TransducerDepth = zeros(size(Heave), 'single');
    end
    
    if isnan(HeaveTx(suby(k)))
        Zbathy(subBab) = Zbathy(subBab) - TransducerDepth(suby(k),1);
        if ~isempty(subTri)
            Zbathy(subTri) = Zbathy(subTri) - TransducerDepth(suby(k),2);
        end
    else % Correct for file 0000_20150610_221327_Belgica
        Zbathy(subBab) = Zbathy(subBab) - (TransducerDepth(suby(k),1)) - HeaveTx(suby(k)); % (HeaveTx + HeaveRx)/2
        if ~isempty(subTri)
            Zbathy(subTri) = Zbathy(subTri) - (TransducerDepth(suby(k), 2)) - HeaveTx(suby(k)); % (HeaveTx + HeaveRx)/2 !!!!!!!!!!!!!!!!!!!!!!!!!
            Zbathy(subTri) = Zbathy(subTri) + 2 * (TransducerDepth(suby(k), 2) - TransducerDepth(suby(k), 1)); % Bizare mais c'est �a qui fonctionne !!!
        end
    end
    
    AngleIntersec(sub) = AngleIntersec(sub) .* sign(BeamPointingAngleVersusHull);
    
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Xbathy, Ybathy, '*'); grid on;
    
    c(1).Image(k,:) = Zbathy;
    c(2).Image(k,:) = Xbathy;
    c(3).Image(k,:) = Ybathy;
    c(4).Image(k,:) = AngleIntersec;
    c(5).Image(k,:) = Azimuth;
end
my_close(hw, 'MsgEnd')
warning(W)

%% Finalisation des images

for k=1:5
    c(k) = majCoordonnees(c(k), subx, suby);
    c(k) = compute_stats(c(k));
    c(k).TagSynchroContrast = num2str(rand(1));
    c(k).CLim               = [c(k).StatValues.Min c(k).StatValues.Max];
    c(k).ColormapIndex      = 3;
end

c(1).DataType = cl_image.indDataType('Bathymetry');
c(2).DataType = cl_image.indDataType('AcrossDist');
c(3).DataType = cl_image.indDataType('AlongDist');
c(4).DataType = cl_image.indDataType('TxAngle');
c(5).DataType = cl_image.indDataType('Heading');
c(1).Unit = 'm';
c(2).Unit = 'm';
c(3).Unit = 'm';
c(4).Unit = 'deg';
c(5).Unit = 'deg';
c(5).CLim = [0 360];
c(5).ColormapIndex = 5;

%% Compl�tion du titre

for k=1:5
    c(k).Name = [c(k).Name '_Computed'];
    c(k) = update_Name(c(k));
end
c = c(5:-1:1);

%% Par ici la sortie

flag = 1;
my_close(753943)


function [SSP, indSSP] = getSSP(this, Time)

SSP    = [];
indSSP = [];

%% Premi�re bricole infame

BathyCel = this.Sonar.BathyCel;
if isfield(BathyCel, 'Depth')
    BathyCel.Z = BathyCel.Depth;
end
if isempty(BathyCel.Z)
    this = init_BathyCel(this);
end
ProfilCelerite = this.Sonar.BathyCel;
if isempty(ProfilCelerite)
    return
end

%% Deuxi�me chose bizarre

try
    %    TOUT CA A BESOIN DE BEAUCOUP DE CLARIFICATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    N = max(length(ProfilCelerite), length(ProfilCelerite.TimeStartOfUse));
    TimeStartOfUse = NaN(1,N);
    for k2=1:N
        
        % URGENT, il faut normaliser cette affaire
        try
            TimeStartOfUse(k2) = ProfilCelerite(k2).TimeStartOfUse.timeMat;
            Formatage = 1;
        catch
            T = ProfilCelerite.TimeStartOfUse.timeMat;
            TimeStartOfUse(k2) = T(k2);
            Formatage = 2;
        end
    end
    %         TimeStartOfUse = cl_time('timeMat', TimeStartOfUse);
catch %#ok<CTCH>
    return
end

%% Calcul des index

indSSP = my_interp1_Extrap_PreviousThenNext(TimeStartOfUse, 1:length(TimeStartOfUse), Time);
indSSP(indSSP == 0) = 1;

for k=1:max(indSSP)
    if Formatage == 1
        SSP(k).ZCelerite = ProfilCelerite(k2).Z; %#ok<AGROW>
        SSP(k).Celerite  = ProfilCelerite(k2).C; %#ok<AGROW>
    else
        try
            SSP(k).ZCelerite = ProfilCelerite.Depth(k2,:); %#ok<AGROW>
            SSP(k).Celerite  = ProfilCelerite.SoundSpeed(k2,:); %#ok<AGROW>
        catch
            SSP(k).ZCelerite = ProfilCelerite.Z(k2,:); %#ok<AGROW>
            SSP(k).Celerite  = ProfilCelerite.C(k2,:); %#ok<AGROW>
        end
    end
    
    Z = SSP(k).ZCelerite;
    C = SSP(k).Celerite;
    subNaN = isnan(Z) | isnan(C);
    SSP(k).ZCelerite(subNaN) = []; %#ok<AGROW>
    SSP(k).Celerite(subNaN) = []; %#ok<AGROW>
    
    SSP(k).ZCelerite = SSP(k).ZCelerite(:); %#ok<AGROW> % force le stockage en colonne
    SSP(k).Celerite  = SSP(k).Celerite(:);  %#ok<AGROW> % force le stockage en colonne
    
    if SSP(k).ZCelerite(end) > SSP(k).ZCelerite(1)
        SSP(k).ZCelerite = SSP(k).flipud(ZCelerite); %#ok<AGROW>
        SSP(k).Celerite  = SSP(k).flipud(Celerite); %#ok<AGROW>
    end
end


function [Z, C] = simplificationSSP(SSP, Immersion, SurfaceSoundSpeed)

if isempty(SurfaceSoundSpeed)
    CeleriteAntenne = 0;
else
    CeleriteAntenne = SurfaceSoundSpeed;
end

Z = SSP.ZCelerite;
C = SSP.Celerite;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(C, Z, '-*b'); grid on;
if CeleriteAntenne ~= 0
    C(Z >= (Immersion-1)) = CeleriteAntenne;
    %     C(Z >= (Immersion)) = CeleriteAntenne;
end
% hold on; PlotUtils.createSScPlot(C, Z, '-or');
% PlotUtils.createSScPlot([min(C) max(C)], [Immersion Immersion], 'k')
