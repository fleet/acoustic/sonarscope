function sonar_range2depth_EM_control(this, indImage, a)

DataType = cl_image.indDataType('Bathymetry');
indLayer = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', '~Tag', 'Computed'); % AtLeastOneLayer
if isempty(indLayer)
    messageNoComparison('Bathymetry')
    return
end
BathyKM = this(indLayer);
indLayer = findIndLayerSonar(a, 1, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
BathySSc = a(indLayer);
BathyResiduals = BathyKM - BathySSc;
BathyRatio     = BathyKM / BathySSc;

DataType = cl_image.indDataType('AcrossDist');
indLayer = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', '~Tag', 'Computed');
if isempty(indLayer)
    messageNoComparison('AcrossDist')
    return
end
AcrossDistKM = this(indLayer);
indLayer = findIndLayerSonar(a, 1, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
AcrossDistSSc = a(indLayer);
AcrossDistResiduals = AcrossDistKM - AcrossDistSSc;
AcrossDistRatio     = AcrossDistKM / AcrossDistSSc;

DataType = cl_image.indDataType('AlongDist');
indLayer = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer', '~Tag', 'Computed');
if isempty(indLayer)
    messageNoComparison('AlongDist')
    return
end
AlongDistKM = this(indLayer);
indLayer = findIndLayerSonar(a, 1, 'DataType', DataType, 'OnlyOneLayer', 'AtLeastOneLayer');
AlongDistSSc = a(indLayer);
AlongDistResiduals = AlongDistKM - AlongDistSSc;
AlongDistRatio     = AlongDistKM / AlongDistSSc;

% nPings = BathySSc.nbRows;
RollTx  = BathyKM.Sonar.Roll(:,:);
PitchTx = BathyKM.Sonar.Pitch(:,:);

figure;
h(1)  = subplot(4,3,1);  imagesc(BathyKM,             'CLim', '0.5%', 'Axe', h(1))
h(2)  = subplot(4,3,2);  imagesc(AcrossDistKM,        'CLim', '0.5%', 'Axe', h(2))
h(3)  = subplot(4,3,3);  imagesc(AlongDistKM,         'CLim', '0.5%', 'Axe', h(3))
h(4)  = subplot(4,3,4);  imagesc(BathySSc,            'CLim', '0.5%', 'Axe', h(4))
h(5)  = subplot(4,3,5);  imagesc(AcrossDistSSc,       'CLim', '0.5%', 'Axe', h(5))
h(6)  = subplot(4,3,6);  imagesc(AlongDistSSc,        'CLim', '0.5%', 'Axe', h(6))
h(7)  = subplot(4,3,7);  imagesc(BathyResiduals,      'CLim', '0.5%', 'Axe', h(7))
h(8)  = subplot(4,3,8);  imagesc(AcrossDistResiduals, 'CLim', '0.5%', 'Axe', h(8))
h(9)  = subplot(4,3,9);  imagesc(AlongDistResiduals,  'CLim', '0.5%', 'Axe', h(9))
h(10) = subplot(4,3,10); imagesc(BathyRatio,          'CLim', '0.5%', 'Axe', h(10))
h(11) = subplot(4,3,11); imagesc(AcrossDistRatio,     'CLim', '0.5%', 'Axe', h(11))
h(12) = subplot(4,3,12); imagesc(AlongDistRatio,      'CLim', '0.5%', 'Axe', h(12))
linkaxes(h)

%% Pr�paration des layers conditionnels pour les statistiques

Tag = BathyResiduals.Name;
GeometryType    = BathyResiduals.GeometryType;
strGeometryType = cl_image.strGeometryType;
Comment = '';

Selection(1).Name     = Tag;
Selection(1).TypeVal  = 'Layer';
Selection(1).indLayer = 1;
Selection(1).DataType = cl_image.indDataType('AcrossDist');
Selection(1).Unit     = 'm';
bins{1} = AcrossDistKM.HistoCentralClasses;

DataType = cl_image.indDataType('TxBeamIndexSwath');
indLayer = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer');
if isempty(indLayer)
    DataType = cl_image.indDataType('TxBeamIndex');
    indLayer = findIndLayerSonar(this, indImage, 'DataType', DataType, 'OnlyOneLayer');
end

if isempty(indLayer)
    LayerConditionnel = [];
else
    LayerConditionnel = this(indLayer);
    Selection(2).Name     = LayerConditionnel.Name;
    Selection(2).TypeVal  = 'Layer';
    Selection(2).indLayer = 2;
    Selection(2).DataType = DataType;
    Selection(2).Unit     = '';
    stats = LayerConditionnel.StatValues;
    bins{2} = stats.Min:stats.Max;
end

%% Calcul des statistiques de ResidualsBathy

nomCourbe = 'ResidualsBathy';
BathyResiduals = courbesStats(BathyResiduals, Tag, ...
    nomCourbe, Comment, [], ...
    [AcrossDistKM LayerConditionnel], Selection, ...
    'bins', bins, ...
    'GeometryType', strGeometryType{GeometryType}, ...
    'Sampling', 1);

if isempty(BathyResiduals)
    return
end

FigUtils.createSScFigure;
bilan = BathyResiduals.CourbesStatistiques.bilan{1};
for k=1:length(bilan)
    hc(k) = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y, '-'); grid on; hold on; %#ok<AGROW>
end
for k=1:length(bilan)
    color = get(hc(k), 'Color');
    hs = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y - bilan(k).std, '--'); grid on; hold on;
    set(hs, 'Color', color);
    hs = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y + bilan(k).std, '--'); grid on; hold on;
    set(hs, 'Color', color);
end
title(bilan(1).nomZoneEtude)

%% Calcul des statistiques de AcrossDistResiduals

nomCourbe = 'AcrossDistResiduals';
AcrossDistResiduals = courbesStats(AcrossDistResiduals, Tag, ...
    nomCourbe, Comment, [], ...
    [AcrossDistKM LayerConditionnel], Selection, ...
    'bins', bins, ...
    'GeometryType', strGeometryType{GeometryType}, ...
    'Sampling', 1);

FigUtils.createSScFigure;
bilan = AcrossDistResiduals.CourbesStatistiques.bilan{1};
for k=1:length(bilan)
    hc(k) = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y, '-'); grid on; hold on;
end
for k=1:length(bilan)
    color = get(hc(k), 'Color');
    hs = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y - bilan(k).std, '--'); grid on; hold on;
    set(hs, 'Color', color);
    hs = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y + bilan(k).std, '--'); grid on; hold on;
    set(hs, 'Color', color);
end
title(bilan(1).nomZoneEtude)

%% Calcul des statistiques de AcrossDistResiduals

nomCourbe = 'AlongDistResiduals';
AlongDistResiduals = courbesStats(AlongDistResiduals, Tag, ...
    nomCourbe, Comment, [], ...
    [AcrossDistKM LayerConditionnel], Selection, ...
    'bins', bins, ...
    'GeometryType', strGeometryType{GeometryType}, ...
    'Sampling', 1);

FigUtils.createSScFigure;
bilan = AlongDistResiduals.CourbesStatistiques.bilan{1};
for k=1:length(bilan)
    hc(k) = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y, '-'); grid on; hold on;
end
for k=1:length(bilan)
    color = get(hc(k), 'Color');
    hs = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y - bilan(k).std, '--'); grid on; hold on;
    set(hs, 'Color', color);
    hs = PlotUtils.createSScPlot(bilan(k).x, bilan(k).y + bilan(k).std, '--'); grid on; hold on;
    set(hs, 'Color', color);
end
title(bilan(1).nomZoneEtude)

%% Trac� des moyennes par pings et mise en correspondance avec le RollTx et RollRx

ns2 = floor(BathyResiduals.nbColumns / 2);
subBab = 1:ns2;
subTri = (ns2+1):BathyResiduals.nbColumns;
meanBathyResiduals         = mean_lig(BathyResiduals);
meanAcrossDistResidualsBab = mean_lig(AcrossDistResiduals, 'subx', subBab);
meanAcrossDistResidualsTri = mean_lig(AcrossDistResiduals, 'subx', subTri);
meanAlongDistResidualsBab  = mean_lig(AlongDistResiduals,  'subx', subBab);
meanAlongDistResidualsTri  = mean_lig(AlongDistResiduals,  'subx', subTri);

meanBathyRatio         = mean_lig(BathyRatio);
meanAcrossDistRatioBab = mean_lig(AcrossDistRatio, 'subx', subBab);
meanAcrossDistRatioTri = mean_lig(AcrossDistRatio, 'subx', subTri);
meanAlongDistRatioBab  = mean_lig(AlongDistRatio,  'subx', subBab);
meanAlongDistRatioTri  = mean_lig(AlongDistRatio,  'subx', subTri);

FigUtils.createSScFigure;
h2(1) = subplot(5,1,1); PlotUtils.createSScPlot(RollTx);                  grid on; title('RollTx');      hold on; plot(-RollTx,  '--')
h2(2) = subplot(5,1,2); PlotUtils.createSScPlot(PitchTx);                 grid on; title('plotPitchTx'); hold on; plot(-PitchTx, '--')
h2(3) = subplot(5,1,3); PlotUtils.createSScPlot(meanBathyResiduals);      grid on; title('meanBathyResiduals')
h2(4) = subplot(5,1,4); PlotUtils.createSScPlot(meanAcrossDistResidualsBab, 'r'); hold on; PlotUtils.createSScPlot(meanAcrossDistResidualsTri, 'g'); grid on; title('meanAcrossDistResiduals')
h2(5) = subplot(5,1,5); PlotUtils.createSScPlot(meanAlongDistResidualsBab,  'r'); hold on; PlotUtils.createSScPlot(meanAlongDistResidualsTri,  'g'); grid on; title('meanAlongDistResiduals')
linkaxes(h2, 'x')

FigUtils.createSScFigure;
h2(1) = subplot(5,1,1); PlotUtils.createSScPlot(RollTx);                  grid on; title('RollTx');      hold on; plot(-RollTx,  '--')
h2(2) = subplot(5,1,2); PlotUtils.createSScPlot(PitchTx);                 grid on; title('plotPitchTx'); hold on; plot(-PitchTx, '--')
h2(3) = subplot(5,1,3); PlotUtils.createSScPlot(meanBathyRatio);      grid on; title('meanBathyRatio')
h2(4) = subplot(5,1,4); PlotUtils.createSScPlot(meanAcrossDistRatioBab, 'r'); hold on; PlotUtils.createSScPlot(meanAcrossDistRatioTri, 'g'); grid on; title('meanAcrossDistRatio')
h2(5) = subplot(5,1,5); PlotUtils.createSScPlot(meanAlongDistRatioBab,  'r'); hold on; PlotUtils.createSScPlot(meanAlongDistRatioTri,  'g'); grid on; title('meanAlongDistRatio')
linkaxes(h2, 'x')

function messageNoComparison(strDataType)

str1 = sprintf('La comparaison du calcul de la bathy ne peut pas se faire car aucun layer de type "%s" n''a �t� trouv�.', strDataType);
str2 = sprintf('The comparison of the bathymetry computation could not be done because no layer of Type "%s" could be found.', strDataType);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'CompareBathyRecomputed');
