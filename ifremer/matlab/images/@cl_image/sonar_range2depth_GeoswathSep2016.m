% sonar_range2depth7111 ente 2 images
%
% Syntax
%   [c, flag] = sonar_range2depth(a, ...)
%
% Input Arguments
%   a : Instance de cl_image de type "BeamPointingAngle"
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   c      : Instances de cl_image contenant "Bathymetry", "AcrossDist"
%            et "AlongDist"
%   flag : 1=tout c'est bien^passe
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, c, this] = sonar_range2depth_GeoswathSep2016(this, ProfilCelerite, InstallationParameters, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

RollCoef  = -1; %OK
coefPitch = -1;
% deltaT      = -InstallationParameters.MRUDelay; % D�j� pris en compte
% lors de la lecture de l'image

Immersion  = InstallationParameters.Draught;% -1.5; % Donn�es .rdf Axel

%% Controles de validite

c(1) = this;
c(1) = replace_Image(c(1), NaN(length(suby), length(subx), 'single'));

c(2) = this;
c(2) = replace_Image(c(2), NaN(length(suby), length(subx), 'single'));

c(3) = this;
c(3) = replace_Image(c(3), NaN(length(suby), length(subx), 'single'));

Time = this.Sonar.Time;

if isempty(ProfilCelerite)
    BathyCel = this.Sonar.BathyCel;
    if isfield(BathyCel, 'Depth')
        BathyCel.Z = BathyCel.Depth;
    end
    if isempty(BathyCel.Z)
        this = init_BathyCel(this);
    end
end

%%
SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
Pitch             = this.Sonar.Pitch;
Heave             = this.Sonar.Heave;
Roll              = this.Sonar.Roll;

Roll  = double(Roll);
Pitch = double(Pitch);
Heave = double(Heave);

TideValue = this.Sonar.Tide;
if isempty(TideValue)
    TideValue = zeros(size(SurfaceSoundSpeed));
end

% SamplingRate = this.Sonar.SampleFrequency;
shipSettings = this.Sonar.Ship;              % Ajout pour action dans rayTracingGLT
TimeTx       = this.Sonar.Time.timeMat;      % Ajout pour calcul au t_Rx

% Bidouille pour cr�er un veteur de temps avec 2 colonnes
nPings           = this.nbRows;
TimeTx(2*nPings) = TimeTx(end);
TimeTx           = reshape(TimeTx(1:2*nPings), nPings, 2); % TODO : a supprimer quand le pb sera r�solu � la source

W = warning;
warning('off')
M = length(suby);
if Mute
    hw = [];
else
    hw = create_waitbar('Computing bathymetry', 'N', M);
end

if ~isempty(ProfilCelerite)
    try
        %    TOUT CA A BESOIN DE BEAUCOUP DE CLARIFICATION
        %    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        N = max(length(ProfilCelerite), length(ProfilCelerite.TimeStartOfUse));
        TimeStartOfUse = NaN(1,N);
        for k2=1:N
            
            % URGENT, il faut normaliser cette affaire
            try
                TimeStartOfUse(k2) = ProfilCelerite(k2).TimeStartOfUse.timeMat;
                Formatage = 1;
            catch
                T = ProfilCelerite.TimeStartOfUse.timeMat;
                TimeStartOfUse(k2) = T(k2);
                Formatage = 2;
            end
        end
        TimeStartOfUse = cl_time('timeMat', TimeStartOfUse);
    catch %#ok<CTCH>
        ProfilCelerite = [];
    end
end

%% Boucle sur les fauch�es

[~, nomFicSeul] = fileparts(this.InitialFileName);

for k1=1:M
    my_waitbar(k1, M, hw);
%     if mod(k1, floor(M/100)) == 1
    if mod(k1,100) == 1
        OnTrace = 1;
    else
        OnTrace = 0;
    end
    
    %% Profil de c�l�rit�
    
    if isempty(ProfilCelerite)
        if isfield(this.Sonar.BathyCel, 'Depth')
            ZCelerite = this.Sonar.BathyCel.Depth;
            Celerite  = this.Sonar.BathyCel.SoundSpeed;
        else
            ZCelerite = this.Sonar.BathyCel.Z;
            Celerite  = this.Sonar.BathyCel.C;
            if isempty(ZCelerite) %Modif Profil par d�faut
                ZCelerite = (0:-1:-500)';
                Celerite = 1492 * ones(size(ZCelerite));
            end
        end
    else
        difTime = TimeStartOfUse - Time(suby(k1));
        difTime(difTime > 0) = [];
        k2 = length(difTime);
        if k2 == 0
            k2 = 1;
        end
        if Formatage == 1
            ZCelerite = ProfilCelerite(k2).Depth;
            Celerite  = ProfilCelerite(k2).SoundSpeed;
        else
            ZCelerite = ProfilCelerite.Depth(k2,:);
            Celerite  = ProfilCelerite.SoundSpeed(k2,:);
        end
    end
    
    %% Angle et temps
    %TempsSimple = abs(this.x(subx)) / (2*this.Sonar.SampleFrequency(suby(k1)));
    %     TempsSimple = (abs(this.x(subx)) * this.Sonar.ResolutionD) / 1500 / 2; % Modifi� le 05/12/2010
    TempsSimple = (abs(this.x(subx)) * this.Sonar.ResolutionD) / 1500; % Remodifi� le 04/07/2013 par JMA apr�s doublement  des fr�quences d'�chantillonnage
    AngleIncident = this.Image(suby(k1), subx);
    
    sub = find(~isnan(AngleIncident) & (TempsSimple > eps));
    subBab = find(sub < this.nbColumns/2 );
    subTri = find(sub >= this.nbColumns/2 );
    
    if isempty(sub)
        continue
    end
    
%     TransducerDepth = zeros(size(TideValue)) + InstallationParameters.Draught;
    
    if isempty(SurfaceSoundSpeed)
        CeleriteAntenne = 0;
    else
        CeleriteAntenne = SurfaceSoundSpeed(suby(k1));
    end
    
    if isnan(Immersion)
        Immersion = -1.5;
    end
    Z = ZCelerite;
    C = Celerite;
    
    if CeleriteAntenne ~= 0
        C(Z >= (Immersion-0.1)) = CeleriteAntenne;
    end
    
    timeRx = double(TempsSimple(sub));
    t_Rx = [ TimeTx(suby(k1),1) * ones(size(subBab)) , TimeTx(suby(k1),2) * ones(size(subTri)) ] + timeRx / (24*3600);
    %t_Rx = TimeTx(suby(k1)) + timeRx / (24*3600);
    
    %% Attitude
    
    if all(isnan(Roll))
        RollRx = 0;
    else
        % Temps UTM � l'instant de R�ception        % USE ROLL & PITCH FROM 1012 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        %RollRx = my_interp1(TimeTx(:,1), Roll(:,1), t_Rx  + this.Sonar.Ship.MRU.RollCalibration;    % Roulis � l'instant de R�ception
        
        %         RollRx = interp1Linear_mex(TimeTx(:,1), Roll(:,1), t_Rx(subBab) )
        RollRx = interp1(TimeTx(:,1), Roll(:,1), t_Rx(subBab));    % Roulis � l'instant de R�ception
        RollRx = [RollRx, my_interp1(TimeTx(:,2), Roll(:,2), t_Rx(subTri) )]; %#ok<AGROW> %  + this.Sonar.Ship.MRU.RollCalibration ;    % Roulis � l'instant de R�ception
    end
    
    
    %PitchTx = Pitch(suby(k1)) + this.Sonar.Ship.MRU.PitchCalibration;                    % Tangage � l'instant d'�mission corrig�e des donn�es de calibration
    PitchTx = [interp1(TimeTx(:,1), Pitch(:, 1), TimeTx(suby(k1),1)) * ones(size(subBab)) ,...  % Tangage � l'�mission
        my_interp1(TimeTx(:,2), Pitch(:, 2), TimeTx(suby(k1),2)) * ones(size(subTri))]; % + this.Sonar.Ship.MRU.PitchCalibration;    % Tangage � l'�mission
    
    if isnan(PitchTx)
        PitchTx = 0;
    end
    if all(isnan(Pitch(:)))
        PitchRx = 0;
    else
        %PitchRx = my_interp1(TimeTx(:,1), Pitch(:,1), t_Rx) + this.Sonar.Ship.MRU.PitchCalibration ;  % Tangage � l'instant de R�ception
        PitchRx = interp1(TimeTx(:,1), Pitch(:,1), t_Rx(subBab));    % Tangage � l'instant de R�ception
        PitchRx = [PitchRx, my_interp1(TimeTx(:,2), Pitch(:,2), t_Rx(subTri))] ;%#ok<AGROW> %  + this.Sonar.Ship.MRU.PitchCalibration ;    % Tangage � l
    end
    
    if all(isnan(Heave(:)))
        Heave = 0;
    else
%         % HeaveRx = my_interp1(TimeTx(:,1), Heave(:,1), t_Rx);
%         HeaveRx = my_interp1(TimeTx(:,1), Heave(:,1), t_Rx(subBab));    % pilonnement � l'instant de R�ception
%         HeaveRx = [HeaveRx, my_interp1(TimeTx(:,2), Heave(:,2), t_Rx(subTri))];    %#ok<AGROW> % pilonnement � l
    end
    
    if isempty(this.Sonar.TxAlongAngle)
        txAlongAngle = 0;
    else
        SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
        switch SonarName
            case 'Reson7150'
                AngleLongitudinalAntenne = 2;
            otherwise
                AngleLongitudinalAntenne = 0;
        end
        
        % figure; plot(this.Sonar.TxAlongAngle + this.Sonar.Pitch,'+'); grid
        txAlongAngle = AngleLongitudinalAntenne + this.Sonar.TxAlongAngle(suby(k1)) + this.Sonar.Pitch(suby(k1));        % Bad combination : should compute in 2 steps !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    end
    
    %% Angle de rec�ption
    if (RollCoef == 0) || isempty(this.Sonar.Roll)
        Teta = AngleIncident(sub);
    else
        %Teta = AngleIncident(sub) - RollCoef * (Roll(suby(k1)) + this.Sonar.Ship.MRU.RollCalibration);
        %Teta = AngleIncident(sub) + this.Sonar.Ship.MRU.RollCalibration; % Bizarre que �a marche comme �a
        Teta = AngleIncident(sub) + RollCoef * (RollRx) ; %+ this.Sonar.Ship.MRU.RollCalibration); % Th�orique
        % Take Pitch into account to (projection)
    end
    
    %% Calcul bathy
    if OnTrace
        %rayTracingGLT(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx*RollCoef, txAlongAngle*coefPitch, shipSettings , 'Immersion', Immersion, 'Fig', 753943);
        %rayTracingGLT_OLD(Z, C, Teta, timeRx, coefPitch*PitchTx, RollRx, txAlongAngle, 'Immersion', Immersion, 'Fig', 753943);
        rayTracingGeoswath(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx, txAlongAngle, shipSettings, ...
            'Immersion', Immersion, 'Fig', 753944, 'Filename', nomFicSeul, 'PingNumber', suby(k1));
        %rayTracing(Z, C, Teta, timeRx, 'Immersion', Immersion, 'Fig', 753943);
    end
    [Xbathy, Ybathy, Zbathy] = rayTracingGeoswath(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx, txAlongAngle, shipSettings , 'Immersion', Immersion);
    %[Xbathy, Ybathy, Zbathy] = rayTracingGLT_OLD(Z, C, Teta, timeRx, coefPitch*PitchTx, RollRx, txAlongAngle, 'Immersion', Immersion);
    %[Xbathy, Zbathy] = rayTracing(Z, C, Teta, timeRx, 'Immersion', Immersion, 'Fig', 753943);
    %Ybathy = zeros(size(Xbathy));
    
    
    subComplex = (imag(Zbathy) ~= 0);
    Zbathy(subComplex) = NaN;
    Xbathy(subComplex) = NaN;
    
    %% Calcul du pilonnement induit
    %p = sonar_pilonnementInduitGeoSwath(coefPitch*PitchTx, RollTx, coefPitch*PitchRx, RollRx, length(subBab), length(subTri));
    
    
    
    %%
%     if isempty(TransducerDepth)
%         TransducerDepth = zeros(size(Heave), 'single');
%     end
    if isempty(TideValue)
        TideValue = zeros(size(Heave), 'single');
    end
    if isnan(Heave(suby(k1)))
        %         Zbathy = Zbathy + (TransducerDepth(suby(k1),1) - TideValue(suby(k1)));
        Zbathy = Zbathy  - TideValue(suby(k1));
    else
        %         Zbathy = Zbathy + (TransducerDepth(suby(k1),1) - TideValue(suby(k1), 1)) - (HeaveRx + Heave(suby(k1),1))/2; % + p;
        %         Zbathy = Zbathy + (TransducerDepth(suby(k1),1) - TideValue(suby(k1), 1)) - Heave(suby(k1),1); % + p;
        % Immersion est d�j� enlev� dans rayTracingGLT
        Zbathy = Zbathy - TideValue(suby(k1), 1) - Heave(suby(k1),1); % + p;
    end
    
    subBab = (Xbathy < 0);
    Zbathy( subBab) = Zbathy( subBab) + InstallationParameters.Port.AntennaOffsetZ;
    Zbathy(~subBab) = Zbathy(~subBab) + InstallationParameters.Starboard.AntennaOffsetZ;
    Xbathy( subBab) = Xbathy( subBab) + InstallationParameters.Port.AntennaOffsetX;
    Xbathy(~subBab) = Xbathy(~subBab) + InstallationParameters.Starboard.AntennaOffsetX;
    Ybathy( subBab) = Ybathy( subBab) + InstallationParameters.Port.AntennaOffsetY;
    Ybathy(~subBab) = Ybathy(~subBab) + InstallationParameters.Starboard.AntennaOffsetY;
    
    %     angleAlong = angleAlong * (pi/180);
    %     if all(isnan(Heave))
    c(1).Image(k1, sub) = Zbathy;
    %     else
    %         c(1).Image(k1, sub) = Zbathy + Heave(suby(k1));
    %     end
    %c(1).Image(k1, sub) = Zbathy;
    c(2).Image(k1, sub) = Xbathy;
    c(3).Image(k1, sub) = Ybathy;
end
my_close(hw, 'MsgEnd')
warning(W)

% SonarScope(AnglesVerif);
% c(3).Image(:,:) = AnglesVerif;

%% Mise � jour de coordonn�es

c(1) = majCoordonnees(c(1), subx, suby);
c(2) = majCoordonnees(c(2), subx, suby);
c(3) = majCoordonnees(c(3), subx, suby);

%% Calcul des statistiques

c(1) = compute_stats(c(1));
c(2) = compute_stats(c(2));
c(3) = compute_stats(c(3));

%% Rehaussement de contraste

c(1).TagSynchroContrast = num2str(rand(1));
c(2).TagSynchroContrast = num2str(rand(1));
c(3).TagSynchroContrast = num2str(rand(1));
CLim = [c(1).StatValues.Min c(1).StatValues.Max];
c(1).CLim          = CLim;
CLim = [c(2).StatValues.Min c(2).StatValues.Max];
c(2).CLim          = CLim;
CLim = [c(3).StatValues.Min c(3).StatValues.Max];
c(3).CLim          = CLim;
c(1).ColormapIndex    = 3;
c(2).ColormapIndex    = 3;
c(3).ColormapIndex    = 3;
c(1).DataType    = cl_image.indDataType('Bathymetry');
c(2).DataType    = cl_image.indDataType('AcrossDist');
c(3).DataType    = cl_image.indDataType('AlongDist');
c(1).Unit          = 'm';
c(2).Unit          = 'm';
c(3).Unit          = 'm';

%% Completion du titre

c(1).Name = [c(1).Name '_Computed'];
c(1) = update_Name(c(1));
c(2).Name = [c(2).Name '_Computed'];
c(2) = update_Name(c(2));
c(3).Name = [c(3).Name '_Computed'];
c(3) = update_Name(c(3));

%% Par ici la sortie

flag = 1;
my_close(753944); %, 'TimeDelay', 60)


function p = sonar_pilonnementInduitGeoSwath(PitchTx, RollTx, PitchRx, RollRx, nBab, nTri) %#ok<DEFNU>
% PitchTx : 2 valeurs pour chaque antenne
% Idem pour chaque valeur angulaire

% [babord tribord]
Sy_Cy = [-0.09  -0.09]; % Longi
Sx_Cx = [-0.11 0.11];   % Trans
Sz_Cz = [0.46 0.46];    % Z
TE    = 0.14;

pTx = pilonnement_induit(PitchTx, RollTx, Sx_Cx, Sy_Cy, Sz_Cz, nBab, nTri);
pRx = pilonnement_induit(PitchRx, RollRx, Sx_Cx, Sy_Cy, Sz_Cz, nBab, nTri);

p = (pTx + pRx) / 2 + TE;



function p = pilonnement_induit(Pitch, Roll, Sx_Cx, Sy_Cy, Sz_Cz, nBab, nTri)

% VERIFIER TOUS LES SIGNES

% p = sind(Pitch) .* (Sy_Cy) - ...
%     cos(Pitch)  .* sind(Roll) .* (Sx_Cx) + ...
%     cosd(Roll)  .* cosd(Pitch) .* (Sz_Cz);

p(1:nBab) = sind(Pitch(1:nBab)) .* (Sy_Cy(1)) - ...
    cos(Pitch(1:nBab))  .* sind(Roll(1:nBab)) .* (Sx_Cx(1)) + ...
    cosd(Roll(1:nBab))  .* cosd(Pitch(1:nBab)) .* (Sz_Cz(1));

p(nBab+1:nBab+nTri) = sind(Pitch(nBab+1:nBab+nTri)) .* (Sy_Cy(2)) - ...
    cos(Pitch(nBab+1:nBab+nTri))  .* sind(Roll(nBab+1:nBab+nTri)) .* (Sx_Cx(2)) + ...
    cosd(Roll(nBab+1:nBab+nTri))  .* cosd(Pitch(nBab+1:nBab+nTri)) .* (Sz_Cz(2));


