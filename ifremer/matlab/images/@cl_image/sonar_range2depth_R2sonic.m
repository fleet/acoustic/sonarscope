

function [c, this, status] = sonar_range2depth_R2sonic(this, b,  ProfilCelerite, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, OffsetRoll] = getPropertyValue(varargin, 'OffsetRoll', 0); %#ok<ASGLU>

RollCoef  = -1;
coefPitch = 1;
deltaT    = 0; %-12.5e-3;
Immersion = 0; % Donn�es .rdf Axel

%% Controles de validite

status = 1;

[~, ~, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, b);
if isempty(subx) || isempty(suby)
    str1 = 'Pas d''intersection commune entre les images.';
    str2 = 'No intersection between the 2 images.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

c(1) = this;
c(1) = replace_Image(c(1), NaN(length(suby), length(subx), 'single'));

c(2) = this;
c(2) = replace_Image(c(2), NaN(length(suby), length(subx), 'single'));

c(3) = this;
c(3) = replace_Image(c(3), NaN(length(suby), length(subx), 'single'));

Time = this.Sonar.Time;

if isempty(ProfilCelerite)
    BathyCel = this.Sonar.BathyCel;
    if isfield(BathyCel, 'Depth')
        BathyCel.Z = BathyCel.Depth;
    end
    if isempty(BathyCel.Z)
        this = init_BathyCel(this);
    end
end

%%
SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
Pitch             = this.Sonar.Pitch;
Heave             = this.Sonar.Heave;
Roll              = this.Sonar.Roll;

% Heave = -Heave;

TideValue = this.Sonar.Tide;
if isempty(TideValue)
    TideValue = zeros(size(SurfaceSoundSpeed));
end

% SamplingRate    = this.Sonar.SampleFrequency;
shipSettings = this.Sonar.Ship;              % Ajout pour action dans rayTracingGLT
TimeTx       = this.Sonar.Time.timeMat;      % Ajout pour calcul au t_Rx

% Bidouille pour cr�er un veteur de temps avec 2 colonnes
nPings           = this.nbRows;
TimeTx(2*nPings) = TimeTx(end);
TimeTx           = reshape(TimeTx(1:2*nPings), nPings, 2); % TODO : a supprimer quand le pb sera r�solu � la source

W = warning;
warning('off')
M = length(suby);
hw = create_waitbar('Computing bathymetry', 'N', M);

if ~isempty(ProfilCelerite)
    try
        %    TOUT CA A BESOIN DE BEAUCOUP DE CLARIFICATION
        %    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        N = max(length(ProfilCelerite), length(ProfilCelerite.TimeStartOfUse));
        TimeStartOfUse = NaN(1,N);
        for k=1:N
            
            % URGENT, il faut normaliser cette affaire
            try
                TimeStartOfUse(k) = ProfilCelerite(k).TimeStartOfUse.timeMat;
                Formatage = 1;
            catch
                T = ProfilCelerite.TimeStartOfUse.timeMat;
                TimeStartOfUse(k) = T(k);
                Formatage = 2;
            end
        end
        TimeStartOfUse = cl_time('timeMat', TimeStartOfUse);
    catch %#ok<CTCH>
        ProfilCelerite = [];
    end
end

%% Boucle sur les fauch�es
for i=1:M
    my_waitbar(i, M, hw);
    if mod(i, floor(M/20)) == 1
        OnTrace = 1;
    else
        OnTrace = 0;
    end
    
    %% Profil de c�l�rit�
    
    if isempty(ProfilCelerite)
        if isfield(this.Sonar.BathyCel, 'Depth')
            ZCelerite = this.Sonar.BathyCel.Depth;
            Celerite  = this.Sonar.BathyCel.SoundSpeed;
        else
            ZCelerite = this.Sonar.BathyCel.Z;
            Celerite  = this.Sonar.BathyCel.C;
            if isempty(ZCelerite) %Modif Profil par d�faut
                ZCelerite = (0:-1:-500)';
                Celerite = 1492 * ones(size(ZCelerite));
            end
        end
    else
        difTime = TimeStartOfUse - Time(suby(i));
        difTime(difTime > 0) = [];
        k = length(difTime);
        if k == 0
            k = 1;
        end
        if Formatage == 1
            ZCelerite = ProfilCelerite(k).Depth;
            Celerite  = ProfilCelerite(k).SoundSpeed;
        else
            ZCelerite = ProfilCelerite.Depth(k,:);
            Celerite  = ProfilCelerite.SoundSpeed(k,:);
        end
    end
    
    %% Angle et temps
    %TempsSimple = abs(this.x(subx)) / (2*this.Sonar.SampleFrequency(suby(i)));
    %     TempsSimple = (abs(this.x(subx)) * this.Sonar.ResolutionD) / 1500 / 2; % Modifi� le 05/12/2010
    TempsSimple   = this.Image(suby(i), subx) / 2;
    AngleIncident = b.Image(subyb(i), subxb);
    
    sub = find(~isnan(AngleIncident) & (TempsSimple > eps));
    %     subBab = find( sub < this.nbColumns/2 );
    %     subTri = find( sub >= this.nbColumns/2 );
    
    if isempty(sub)
        continue
    end
    
    % TODO Port.AntennaOffsetZ Draught
    
    TransducerDepth = zeros(length(TideValue),2);
    
    if isempty(SurfaceSoundSpeed)
        CeleriteAntenne = 0;
    else
        CeleriteAntenne = SurfaceSoundSpeed(suby(i));
    end
    
    if isnan(Immersion)
        Immersion = -1.5;
    end
    Z = ZCelerite;
    C = Celerite;
    
    if CeleriteAntenne ~= 0
        C(Z >= (Immersion-0.1)) = CeleriteAntenne;
    end
    
    
    timeRx = double(TempsSimple(sub));
    t_Rx = TimeTx(suby(i),1) * ones(size(sub)) + (timeRx  - deltaT) / (24*3600);
    %t_Rx = TimeTx(suby(i)) + timeRx / (24*3600);
    
    
    %% Attitude
    
    RollTx = my_interp1(TimeTx(:,1), Roll(:, 1), TimeTx(suby(i),1) - deltaT / (24*3600) ) * ones(size(sub)); % + this.Sonar.Ship.MRU.RollCalibration;    % Tangage � l'�mission
    
    if all(isnan(Roll))
        RollRx = 0;
    else
        % Temps UTM � l'instant de R�ception        % USE ROLL & PITCH FROM 1012 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        %RollRx = my_interp1(TimeTx(:,1), Roll(:,1), t_Rx  + this.Sonar.Ship.MRU.RollCalibration;    % Roulis � l'instant de R�ception
        RollRx = my_interp1(TimeTx(:,1), Roll(:), t_Rx);    % Roulis � l'instant de R�ception
        %         RollRx = [RollRx, my_interp1(TimeTx(:,2), Roll(:,2), t_Rx(subTri) )]; %  + this.Sonar.Ship.MRU.RollCalibration ;    % Roulis � l'instant de R�ception
    end
    
    
    %PitchTx = Pitch(suby(i)) + this.Sonar.Ship.MRU.PitchCalibration;                    % Tangage � l'instant d'�mission corrig�e des donn�es de calibration
    PitchTx = my_interp1(TimeTx(:,1), Pitch(:, 1), TimeTx(suby(i),1) - deltaT / (24*3600) ) * ones(size(sub));  % Tangage � l'�mission
    
    if isnan(PitchTx)
        PitchTx = 0;
    end
    if all(isnan(Pitch(:)))
        PitchRx = 0;
    else
        %PitchRx = my_interp1(TimeTx(:,1), Pitch(:,1), t_Rx) + this.Sonar.Ship.MRU.PitchCalibration ;  % Tangage � l'instant de R�ception
        PitchRx = my_interp1(TimeTx(:,1), Pitch(:,1), t_Rx);    % Tangage � l'instant de R�ception
        %         PitchRx = [PitchRx, my_interp1(TimeTx(:,2), Pitch(:,2), t_Rx(subTri))] ;%  + this.Sonar.Ship.MRU.PitchCalibration ;    % Tangage � l
        
    end
    
    if all(isnan(Heave(:)))
        Heave = 0;
    else
        %HeaveRx = my_interp1(TimeTx(:,1), Heave(:,1), t_Rx);
        HeaveRx = my_interp1(TimeTx(:,1), Heave(:,1), t_Rx);    % pilonnement � l'instant de R�ception
        %         HeaveRx = [HeaveRx, my_interp1(TimeTx(:,2), Heave(:,2), t_Rx(subTri))];    % pilonnement � l
        
    end
    
    
    if isempty(this.Sonar.TxAlongAngle)
        txAlongAngle = 0;
    else
        SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
        switch SonarName
            case 'Reson7150'
                AngleLongitudinalAntenne = 2;
            otherwise
                AngleLongitudinalAntenne = 0;
        end
        
        % figure; plot(this.Sonar.TxAlongAngle + this.Sonar.Pitch,'+'); grid
        txAlongAngle = AngleLongitudinalAntenne + this.Sonar.TxAlongAngle(suby(i)) + this.Sonar.Pitch(suby(i));        % Bad combination : should compute in 2 steps !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    end
    
    %% Angle de rec�ption
    if (RollCoef == 0) || isempty(this.Sonar.Roll)
        Teta = AngleIncident(sub);
    else
        %Teta = AngleIncident(sub) - RollCoef * (Roll(suby(i)) + this.Sonar.Ship.MRU.RollCalibration);
        %Teta = AngleIncident(sub) + this.Sonar.Ship.MRU.RollCalibration; % Bizarre que �a marche comme �a
        Teta = AngleIncident(sub) + RollCoef * (RollRx) ; %+ this.Sonar.Ship.MRU.RollCalibration); % Th�orique
        % Take Pitch into account to (projection)
    end
    
    Teta = Teta + OffsetRoll;
    
    %% Calcul bathy
    if OnTrace
        %rayTracingGLT(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx*RollCoef, txAlongAngle*coefPitch, shipSettings , 'Immersion', Immersion, 'Fig', 753943);
        %rayTracingGLT_OLD(Z, C, Teta, timeRx, coefPitch*PitchTx, RollRx, txAlongAngle, 'Immersion', Immersion, 'Fig', 753943);
        rayTracingGLT(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx, txAlongAngle, shipSettings , 'Immersion', Immersion, 'Fig', 753944);
        %rayTracing(Z, C, Teta, timeRx, 'Immersion', Immersion, 'Fig', 753943);
    end
    [Xbathy, Ybathy, Zbathy] = rayTracingGLT(Z, C, Teta, timeRx, PitchTx*coefPitch, PitchRx*coefPitch, RollRx, txAlongAngle, shipSettings , 'Immersion', Immersion);
    %[Xbathy, Zbathy] = rayTracing(Z, C, Teta, timeRx, 'Immersion', Immersion, 'Fig', 753943);
    %Ybathy = zeros(size(Xbathy));
    
    
    subComplex = (imag(Zbathy) ~= 0);
    Zbathy(subComplex) = NaN;
    Xbathy(subComplex) = NaN;
    
    %% Calcul du pilonnement induit
    
    p = sonar_pilonnementInduitGeoSwath(coefPitch*PitchTx, RollTx, coefPitch*PitchRx, RollRx);
    
    %%
    if isempty(TransducerDepth)
        TransducerDepth = zeros(size(Heave), 'single');
    end
    if isempty(TideValue)
        TideValue = zeros(size(Heave), 'single');
    end
    if isnan(Heave(suby(i)))
        Zbathy = Zbathy + (TransducerDepth(suby(i)) - TideValue(suby(i)));
    else
        %         Zbathy = Zbathy + (TransducerDepth(suby(i)) - TideValue(suby(i))) - (HeaveRx + Heave(suby(i),1))/2 + p;
        Zbathy = Zbathy + (TransducerDepth(suby(i)) - TideValue(suby(i))) + 0*Heave(suby(i),1) - 0*p; % Modif JMA le 22/04/2015
    end
    
    %     angleAlong = angleAlong * (pi/180);
    %     if all(isnan(Heave))
    c(1).Image(i, sub) = Zbathy;
    %     else
    %         c(1).Image(i, sub) = Zbathy + Heave(suby(i));
    %     end
    %c(1).Image(i, sub) = Zbathy;
    c(2).Image(i, sub) = Xbathy;
    c(3).Image(i, sub) = Ybathy;
end
my_close(hw, 'MsgEnd');

my_close(1238, 'TimeDelay', 60)
warning(W)

% SonarScope(AnglesVerif);
% c(3).Image(:,:) = AnglesVerif;

%% Mise a jour de coordonnees

c(1) = majCoordonnees(c(1), subx, suby);
c(2) = majCoordonnees(c(2), subx, suby);
c(3) = majCoordonnees(c(3), subx, suby);

%% Calcul des statistiques

c(1) = compute_stats(c(1));
c(2) = compute_stats(c(2));
c(3) = compute_stats(c(3));

%% Rehaussement de contraste

c(1).TagSynchroContrast = num2str(rand(1));
c(2).TagSynchroContrast = num2str(rand(1));
c(3).TagSynchroContrast = num2str(rand(1));
CLim = [c(1).StatValues.Min c(1).StatValues.Max];
c(1).CLim          = CLim;
CLim = [c(2).StatValues.Min c(2).StatValues.Max];
c(2).CLim          = CLim;
CLim = [c(3).StatValues.Min c(3).StatValues.Max];
c(3).CLim          = CLim;
c(1).ColormapIndex    = 3;
c(2).ColormapIndex    = 3;
c(3).ColormapIndex    = 3;
c(1).DataType    = cl_image.indDataType('Bathymetry');
c(2).DataType    = cl_image.indDataType('AcrossDist');
c(3).DataType    = cl_image.indDataType('AlongDist');
c(1).Unit          = 'm';
c(2).Unit          = 'm';
c(3).Unit          = 'm';

%% Completion du titre

c(1).Name = [c(1).Name '_Computed'];
c(1) = update_Name(c(1));
c(2).Name = [c(2).Name '_Computed'];
c(2) = update_Name(c(2));
c(3).Name = [c(3).Name '_Computed'];
c(3) = update_Name(c(3));

%% Par ici la sortie

status = 1;

my_close(753943, 'TimeDelay', 60)

%%_________________________________________________________________________
function p = sonar_pilonnementInduitGeoSwath(PitchTx, RollTx, PitchRx, RollRx)
% PitchTx : 2 valeurs pour chaque antenne
% Idem pour chaque valeur angulaire

%{
Les coordonn�es (x,y,z) en m�tres, dans le r�f�rentiel du bateau, sont les suivantes :
Centrale inertielle (0 ; 0 ; 0).
Echosondeur  (-1,98 ; 0,6 ; -0,930).
DGPS  (-1,660 ; 1,520 ; 3).
%}

% [babord tribord]
Sy_Cy = -1.98; % Longi
Sx_Cx = 0.6;   % Trans
Sy_Cy = 0.6; % Longi
Sx_Cx = -1.98;   % Trans
Sz_Cz = 0.93;  % Z
TE    = 0;

pTx = pilonnement_induit(PitchTx, RollTx, Sx_Cx, Sy_Cy, Sz_Cz);
pRx = pilonnement_induit(PitchRx, RollRx, Sx_Cx, Sy_Cy, Sz_Cz);

p = (pTx + pRx) / 2 + TE;


function p = pilonnement_induit(Pitch, Roll, Sx_Cx, Sy_Cy, Sz_Cz)

% VERIFIER TOUS LES SIGNES

p = sind(Pitch) .* (Sy_Cy) - ...
    cosd(Pitch)  .* sind(Roll) .* (Sx_Cx) + ...
    cosd(Roll)  .* cosd(Pitch) .* (Sz_Cz);

