% Plot of the WC footprint and the WC image combined to a background image
% and a navigation file from another vehicule (Ex Nautile).
% This is a demonstrater before it is implemented in GLOBE
%
% Syntax
%   sonar_rectification(this, FileNameSDF, ...)
%
% Input Arguments
%   a           : Instance of cl_image used for the background
%   FileNameSDF : Name of the Water Column XML file (geometry DepthAcrossDist)
%
% Name-Value Pair Arguments
%   FileNameNav : File name of a navigation file (.nvi, ...)
%
% Remarks : if a is empty, no background image is displayed
%
% Examples
%   FileNameSDF = 'D:\Temp\Charline\Recalage\SoReco2_063_111025110800.sdf';
%   FileNameErs = 'D:\Temp\Charline\Recalage\EM2040_1m_comp_SW_LatLong_Reflectivity.ers';
%   [flag, a] = cl_image.import_ermapper(FileNameErs);
%   sonar_rectification(a, FileNameSDF)
%
% See also lecFicNav XMLBinUtils.readGrpData Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function sonar_rectification(this, FileNameSDF, SourceHeading, varargin)

%% Contr�le de signature

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Contr�le d'existence des fichiers

if iscell(FileNameSDF)
    FileNameSDF = FileNameSDF{1};
end
if ~exist(FileNameSDF, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', FileNameSDF);
    str2 = sprintf('"%s" does not exist.', FileNameSDF);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Conversion de l'image en RGB

% if this.ImageType == 1
%     [this, flag] = Intensity2RGB(this);
%     if ~flag
%         return
%     end
% end

%% Lecture fichier SDF

sdf = cl_sdf('nomFic', FileNameSDF);
[flag, SDF_PingSample, Carto] = view_Image(sdf, 'ListeLayers', 1, 'SourceHeading', SourceHeading); %#ok<ASGLU>
if ~flag
    return
end
resol = 0.2;
SDF_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(SDF_PingSample, 'resolutionX', resol);

%% Initialisations

[optimizer, metric]= imregconfig('monomodal');


%% Trac� graphique

nbPings = SDF_PingAcrossDist(1).nbRows;
nbLigAffichees = 500;

figure;
hp1 = uipanel('Position',[.01 .12 .49 .87]);
hp2 = uipanel('Position',[.50 .12 .49 .87]);
hp3 = uipanel('Position',[.01 .01 .98 .1]);
uicontrol(hp3, 'Style', 'Slider', 'Min', 1, 'Max', nbPings, 'Value', 1, ...
    'SliderStep', [1/nbPings nbLigAffichees/nbPings], ...
    'Units', 'normalized', 'Position', [0 0 1 1], 'Callback', @slider1_Callback);

Axe1 = axes('parent', hp1);
imagesc(this, 'Axe', Axe1); hold on;

Axe2 = axes('parent', hp2);
imagesc(SDF_PingAcrossDist(1), 'Axe', Axe2); hold on;
yTop = min(nbPings, nbLigAffichees);
set(Axe2, 'YLim', [1 yTop])

displayMosaique(this, SDF_PingAcrossDist(1), resol, 1:yTop)

%% Callback

    function slider1_Callback(hs, eventdata, handles) %#ok<INUSD>
        k = floor(get(hs, 'Value'));
        yTop = min(nbPings, k+nbLigAffichees-1);
        set(Axe2, 'YLim', [k yTop])
        
        displayMosaique(this, SDF_PingAcrossDist(1), resol, k:yTop)
    end
end


function displayMosaique(this, SDF_PingAcrossDist, resol, suby)

[flag, c] = sonarMosaique(SDF_PingAcrossDist, 1, resol, ...
    'AlongInterpolation', 2, 'CreationMosAngle', 1, 'suby', suby, ...
    'NoQuestion', 1, 'NoWaitbar', 1);
if ~flag
    return
end
% [c, flag] = Intensity2RGB(c(1));
% if ~flag
%     return
% end
imagesc(c(1), 'Fig', 1345);

a = extraction(this, 'x', c(1).x, 'y', c(1).y);
imagesc(a, 'Fig', 1346);

% SonarScope([this, pppp, a(1), c(1)]);

I1 = a.Image(:,:);
I2 = c(1).Image(:,:);
% figure; imshowpair(I1,I2);
% J = imfuse(I1,I2);
% figure; imagesc(J);
% 
I1 = I1 - min(I1(:));
I1 = uint8(I1 * (255 / max(I1(:))));
I2 = I2 - min(I2(:));
I2 = uint8(I2 * (255 / max(I2(:))));
% [optimizer, metric]= imregconfig('monomodal');
% moving_reg = imregister(I2, I1, 'affine', optimizer, metric);
% 
% figure; imagesc(moving_reg); grid on

% C = corner(I2);
% figure(1345)
% hold on
% plot(c(1).x(C(:,1)), c(1).y(C(:,2)), 'r*');
% hold off;

if isdeployed
    % Juste pour supprimer un warning lors de la compilation mais �a n'a pas d'effet
else
    H = cpselect(I2, I1);
end

% mytform = fitgeotrans(movingPoints, fixedPoints, 'projective');
% registered = imwarp(I2, mytform);
end