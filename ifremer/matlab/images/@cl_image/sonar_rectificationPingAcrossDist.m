% Mise en correspondance d'une image Klein avec une image de r�f�rence.
%
% Syntax
%   sonar_rectificationPingAcrossDist(a, FileNameSDF, ...)
%
% Input Arguments
%   a           : Instance of cl_image witch is the reference image in LatLon geometry
%   FileNameSDF : Name of Klein .sdf file
%
% Examples
%   FileNameSDF = 'D:\Temp\Charline\Recalage\SoReco2_063_111025120800.sdf';
%   FileNameSDF = 'D:\Temp\Charline\Recalage\SoReco2_063_111025113800.sdf';
%   FileNameSDF = 'D:\Temp\Charline\Recalage\SoReco2_063_111025110800.sdf';
%   FileNameErs = 'D:\Temp\Charline\Recalage\EM2040_1m_comp_SW_LatLong_Reflectivity.ers';
%   NomFicOut   = 'D:\Temp\Charline\Recalage\SDF_Rectification.txt';
%   [flag, a] = cl_image.import_ermapper(FileNameErs);
%   flag = sonar_rectificationPingAcrossDist(a, FileNameSDF)
%
% See also lecFicNav XMLBinUtils.readGrpData Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function flag = sonar_rectificationPingAcrossDist(this, FileNameSDF, identLayer, NomFicOut, SourceHeading)

%% Cr�ation du fichier de sortie

fid = fopen(NomFicOut, 'w+');
if fid == -1
    flag = 0;
    return
end
fclose(fid);

%% Test si la projection de la mosa�que est d�finie

Carto = this(1).Carto;
if get(Carto, 'Projection.Type') == 1
    str1 = 'Veillez d�finir une projection cartographique par d�faut � l''image (pas feodetic).';
    str2 = 'Please define default cartographic projection to the image (no geodetic).';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Affichage de e l'aide et des raccourcis clavier

HelpSDFRectification

%% SDF Processing

N = length(FileNameSDF);
hw = create_waitbar(waitbarMsg('sonar_rectificationPingAcrossDist'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, Carto] = sonar_rectificationPingAcrossDist_unit(this, FileNameSDF{k}, identLayer, NomFicOut, Carto, SourceHeading);
    if ~flag
        return
    end
    %     waitfor(fig)
end
my_close(hw, 'MsgEnd')
end


function [flag, Carto]  = sonar_rectificationPingAcrossDist_unit(this, FileNameSDF, identLayer, NomFicOut, Carto, SourceHeading)

%% Contr�le de signature

for k=1:length(this)
    flag = testSignature(this(k), 'GeometryType', cl_image.indGeometryType('LatLong'));
    if ~flag
        return
    end
end

%% Contr�le d'existence des fichiers

if iscell(FileNameSDF)
    FileNameSDF = FileNameSDF{1};
end
if ~exist(FileNameSDF, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', FileNameSDF);
    str2 = sprintf('"%s" does not exist.', FileNameSDF);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Lecture fichier SDF

sdf = cl_sdf('nomFic', FileNameSDF);
[flag, SDF_PingSample, Carto] = view_Image(sdf, 'ListeLayers', identLayer, 'Carto', Carto, 'SourceHeading', SourceHeading);
if ~flag
    return
end
resol = 0.2;
SDF_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(SDF_PingSample, 'resolutionX', resol);

%% Calcul des coordonn�es g�ographiques

[flag, LatLon] = sonarCalculCoordGeo(SDF_PingAcrossDist, 1);
if ~flag
    return
end
Lat = LatLon(1);
Lon = LatLon(2);

%% Mosaique inverse de la r�f�rence

for k=1:length(this)
    [flag, EM2040_PingAcrossDist(k)] = sonarMosaiqueInv([SDF_PingAcrossDist Lat Lon], 1, this(k), 'indLayerLat', 3, 'indLayerLon', 4); %#ok<AGROW>
    if ~flag
        return
    end
end

% SonarScope([SDF_PingAcrossDist EM2040_PingAcrossDist])

ISDF = SDF_PingAcrossDist(1).Image(:,:);
x    = SDF_PingAcrossDist(1).x;
y    = SDF_PingAcrossDist(1).y;
% figure; imshowpair(I1,I2);
% J = imfuse(I1,I2);
% figure; imagesc(J);
%
ISDF = ISDF - SDF_PingAcrossDist(1).StatValues.Quant_25_75(1);
ISDF = uint8(ISDF * (255 / diff(SDF_PingAcrossDist(1).StatValues.Quant_25_75)));

% H = cpselect(ISDF, IEM2040);
nbAxes = 1 + length(this);
fig = figure;
hAxe(1) = subplot(1,nbAxes,1); imagesc(x, y, ISDF); axis xy;

for k=1:length(EM2040_PingAcrossDist)
    IEM2040 = EM2040_PingAcrossDist(k).Image(:,:,:);
    IEM2040 = IEM2040 - EM2040_PingAcrossDist(k).StatValues.Quant_25_75(1);
    IEM2040 = uint8(IEM2040 * (255 / diff(EM2040_PingAcrossDist(k).StatValues.Quant_25_75)));
    hAxe(1+k) = subplot(1,nbAxes,1+k); imagesc(x, y, IEM2040); axis xy; %#ok<AGROW>
end
colormap(gray(256));
linkaxes(hAxe, 'xy')
% zoom yon

XLim = SDF_PingAcrossDist(1).XLim;
YLim = SDF_PingAcrossDist(1).YLim;

for k2=1:nbAxes
    axes(hAxe(k2)) %#ok<LAXES>
    hold on;
    hHorz(k2) = plot(XLim, [1 1], 'k'); %#ok<AGROW>
    %     set(hHorz(k), 'LineWidth', 10)
    hVert(k2) = plot([0 0], YLim, 'k'); %#ok<AGROW>
    %     set(hVert(k), 'LineWidth', 10)
    hold off;
end
hp = [];

set(fig, 'WindowButtonDownFcn', @Clik);
set(fig, 'KeyPressFcn',         @KeyPress);
set(fig, 'WindowScrollWheelFcn',@ScrollWheel);

PreviousPosition = [];
TabPoints = NaN(0,4);

waitfor(fig)

%% Estimation de la d�rive et du d�placement

flag = processNav(this(1).Carto, SDF_PingAcrossDist, Lat, Lon, TabPoints, NomFicOut, FileNameSDF, hp);

%% Callbacks

    function YLimNew = checkYLim(YLimNew, n)
        YLimNew(2) = min(YLim(2), YLimNew(2));
        YLimNew(1) = YLimNew(2) - n;
        YLimNew(1) = max(1, YLimNew(1));
        YLimNew(2) = YLimNew(1) + n;
        YLimNew(1) = max(YLimNew(1), 1);
        YLimNew(2) = min(YLimNew(2), YLim(2));
        YLimNew    = floor(YLimNew);
    end

    function zoom0
        set(hAxe(1), 'XLim', XLim, 'YLim', YLim)
    end

    function zoom1
        YLimNew = get(hAxe(1), 'YLim');
        YLimNew(2) = YLimNew(1) + (getHeightAxe(hAxe(1))-1);
        set(hAxe(1), 'XLim', XLim, 'YLim', YLimNew)
    end

    function zoomUp(coef)
        YLimNew = floor(get(hAxe(1), 'YLim'));
        n = ((YLimNew(2) - YLimNew(1)));
        YLimNew = YLimNew + (coef*n);
        YLimNew = checkYLim(YLimNew, n);
        set(hAxe(1), 'XLim', XLim, 'YLim', YLimNew)
    end

    function zoomDown(coef)
        YLimNew = floor(get(hAxe(1), 'YLim'));
        n = ((YLimNew(2) - YLimNew(1)));
        YLimNew = YLimNew - (coef*n);
        YLimNew = checkYLim(YLimNew, n);
        set(hAxe(1), 'XLim', XLim, 'YLim', YLimNew)
    end

    function zoomPageUp
        YLimNew = floor(get(hAxe(1), 'YLim'));
        n = ((YLimNew(2) - YLimNew(1)));
        YLimNew = [YLim(2)-n+1, YLim(2)];
        YLimNew = checkYLim(YLimNew, n);
        set(hAxe(1), 'XLim', XLim, 'YLim', YLimNew)
    end

    function zoomPageDown
        YLimNew = floor(get(hAxe(1), 'YLim'));
        n = ((YLimNew(2) - YLimNew(1)));
        YLimNew = [1 n];
        YLimNew = checkYLim(YLimNew, n);
        set(hAxe(1), 'XLim', XLim, 'YLim', YLimNew)
    end

    function zoomPlus
        YLimNew = floor(get(hAxe(1), 'YLim'));
        n = ((YLimNew(2) - YLimNew(1)));
        YLimNew = floor([YLimNew(1)+n/4 YLimNew(2)-n/4]);
        YLimNew = checkYLim(YLimNew, n/2);
        set(hAxe(1), 'XLim', XLim, 'YLim', YLimNew)
    end

    function zoomMoins
        YLimNew = floor(get(hAxe(1), 'YLim'));
        n = ((YLimNew(2) - YLimNew(1)));
        YLimNew = floor([YLimNew(1)-n/4 YLimNew(2)+n/4]);
        YLimNew = checkYLim(YLimNew, n*2);
        set(hAxe(1), 'XLim', XLim, 'YLim', YLimNew)
    end

    function KeyPress(~, eventdata)
        switch eventdata.Character
            case '0'
                zoom0
            case '1'
                zoom1
            case '+'
                zoomPlus
            case '-'
                zoomMoins
            case 'T'
                zoomPageUp
            case 'D'
                zoomPageDown
            case '$'
                flag = processNav(this(1).Carto, SDF_PingAcrossDist, Lat, Lon, TabPoints, NomFicOut, FileNameSDF, hp);
                TabPoints = [];
            otherwise
                switch eventdata.Key
                    case 'numpad0'
                        zoom0
                    case 'numpad1'
                        zoom1
                    case 'downarrow'
                        zoomDown(4/5)
                    case 'uparrow'
                        zoomUp(4/5)
                    case 'pageup'
                        zoomPageUp
                    case 'pagedown'
                        zoomPageDown
                    case 'delete'
                        TabPoints(end,:) = [];
                        sub = ~isnan(hp(end,:));
                        delete(hp(end,sub))
                        hp(end,:) = [];
                    otherwise
                        %                         eventdata.Key
                end
        end
    end

    function ScrollWheel(~, eventdata)
        switch sign(eventdata.VerticalScrollCount)
            case -1
                zoomUp(abs(eventdata.VerticalScrollCount)/10)
            case 1
                zoomDown(abs(eventdata.VerticalScrollCount)/10)
        end
    end

    function Clik(hfig, eventdata, handles) %#ok<INUSD>
        GCA = gca;
        positionCurseurDansFigure = get(GCA, 'currentPoint');
        YLimNew = floor(get(hAxe(1), 'YLim'));
        
        if (positionCurseurDansFigure(1,1) < XLim(1)) || (positionCurseurDansFigure(1,1) > XLim(2)) || ...
                (positionCurseurDansFigure(1,2) < YLimNew(1)) || (positionCurseurDansFigure(1,2) > YLimNew(2))
            return
        end
        
        XData = [positionCurseurDansFigure(2,1) positionCurseurDansFigure(2,1)];
        YData = [positionCurseurDansFigure(1,2) positionCurseurDansFigure(1,2)];
        
        if GCA == hAxe(1)
            PreviousPosition = positionCurseurDansFigure(1, 1:2);
        else
            if ~isempty(PreviousPosition)
                TabPoints(end+1, 1:2) = PreviousPosition;
                TabPoints(end,   3:4) = positionCurseurDansFigure(1, 1:2);
                PreviousPosition = [];
                floor(TabPoints)
                
                hp(end+1,1:3*nbAxes) = NaN;
                for ka=1:nbAxes
                    axes(hAxe(ka)) %#ok<LAXES>
                    hold on;
                    hp(end,1+(ka-1)*3) = plot(TabPoints(end,1), TabPoints(end,2), 'or');
                    hp(end,2+(ka-1)*3) = plot(TabPoints(end,3), TabPoints(end,4), 'og');
                    hp(end,3+(ka-1)*3) = plot(TabPoints(end,[1 3]), TabPoints(end,[2 4]), 'k');
                    hold off
                end
            end
        end
        
        SelectionType = get(hfig, 'SelectionType');
        switch SelectionType
            case 'open' % Double click
                datetime("today")
            case 'extend' % Click molette
                datetime("today")
            case 'alt' % Click droit normal
                % TODO : ici il faut consid�rer que l'on donne la
                % correspondance
                datetime("today")
            case 'normal' % Click normal gauche
                for ka=1:nbAxes
                    set(hHorz(ka), 'YData', YData)
                    set(hVert(ka), 'XData', XData)
                end
            otherwise
                datetime("today")
        end
        
        for ka=1:nbAxes
            set(hHorz(ka), 'YData', YData)
            set(hVert(ka), 'XData', XData)
        end
    end
end


function Height = getHeightAxe(h)
set(h, 'Units', 'pixels')
Position = get(h, 'Position');
Height = floor(Position(4));
set(h, 'Units', 'normalized')
end


function flag = saveFileRectification(NomFicOut, FileNameSDF, Drift, OffsetX, OffsetY, timeIpingCentral)
fid = fopen(NomFicOut, 'a+');
if fid == -1
    flag = 0;
    return
end
t = cl_time('timeMat', timeIpingCentral);
fprintf(fid, '%s %f %f %f %s\n', t2str(t), Drift, OffsetX, OffsetY, FileNameSDF);
fclose(fid);
flag = 1;
end


function HelpSDFRectification

% Callbacks trait�s dans callback_KeyPress

str = {};
% str{end+1} = Lang('m  : Rehaussement de contraste Min Max', 'm  : Contrast Enhancement Min Max');
% str{end+1} = Lang('c  : Rehaussement de contraste 0.5%', 'c  : Contrast Enhancement 0.5%');
% str{end+1} = Lang('M  : Rehaussement de contraste Auto Min Max (On/Off)', 'M  : Contrast Enhancement Auto Min Max (On/Off)');
% str{end+1} = Lang('C  : Rehaussement de contraste Auto 0.5% (On/Off)', 'C  : Contrast Enhancement Auto 0.5% (On/Off)');
str{end+1} = '0  : Quick look';
str{end+1} = '1  : 1 ping for one screen line';
str{end+1} = '+  : Zoom * 2 on pings';
str{end+1} = '-  : Zoom / 2 on pings';
str{end+1} = Lang('Fl�che vers le haut : D�calage vers le haut', 'Up arrow   : Shift top');
str{end+1} = Lang('Fl�che vers le bas  : D�calage vers le bas', 'Down arrow   : Shift down');
str{end+1} = Lang('T  : Positionnement en haut', 'T  : Top of Image');
str{end+1} = Lang('B  : Positionnement en bas',  'B  : Bottom of Image');
str{end+1} = Lang('Roulette de la souris : d�placement',  'Scroll whell : displacement');
str{end+1} = Lang('$  : Calcul de la d�formation',  '$  : Compute the deformation');
str{end+1} = Lang('Suppr : Suppression du dernier couple',  'Suppr : Suppress the last couple');

edit_infoPixel(str, [], 'PromptString', 'Navigation rectification shortcuts.')
end


function flag = processNav(Carto, SDF_PingAcrossDist, Lat, Lon, TabPoints, NomFicOut, FileNameSDF, hp)

flag = 1;

if isempty(TabPoints)
    return
end

n = size(TabPoints,1);
for k=1:n
    LatSDF0(k) = get_val_xy(Lat, 0, TabPoints(k,2)); %#ok<AGROW>
    LonSDF0(k) = get_val_xy(Lon, 0, TabPoints(k,2)); %#ok<AGROW>
    LatSDF1(k) = get_val_xy(Lat, TabPoints(k,1), TabPoints(k,2)); %#ok<AGROW>
    LonSDF1(k) = get_val_xy(Lon, TabPoints(k,1), TabPoints(k,2)); %#ok<AGROW>
    LatSDF2(k) = get_val_xy(Lat, TabPoints(k,3), TabPoints(k,4)); %#ok<AGROW>
    LonSDF2(k) = get_val_xy(Lon, TabPoints(k,3), TabPoints(k,4)); %#ok<AGROW>
end
[xSDF0, ySDF0] = latlon2xy(Carto, LatSDF0, LonSDF0);
[xSDF1, ySDF1] = latlon2xy(Carto, LatSDF1, LonSDF1);
[xSDF2, ySDF2] = latlon2xy(Carto, LatSDF2, LonSDF2);

% D�termination du ping central

timeIpingCentral = getTimePingCentral(SDF_PingAcrossDist, TabPoints(:,1:2));

% figure(6757); plot(TabPoints(:,1), TabPoints(:,2), 'or'); grid on; hold on; plot(TabPoints(:,3), TabPoints(:,4), 'og'); plot(zeros(size(TabPoints,1)), TabPoints(:,2), '*b'); hold off
% figure(6758); plot(LonSDF1, LatSDF1, 'or'); grid on; hold on; plot(LonSDF2, LatSDF2, 'og'); plot(LonSDF0, LatSDF0, '*b'); hold off
% figure(6759); plot(xSDF1, ySDF1, 'or'); grid on; hold on; plot(xSDF2, ySDF2, 'og'); plot(xSDF0, ySDF0, '*b'); hold off

% XData = [xSDF0' ySDF0' xSDF1' ySDF1'];
% sz = size(XData);
% xy2 = driftAndshift(XData(:)', [0, 0, 0], sz);
[Drift, OffsetX, OffsetY] = solveShiftAndDrift(xSDF0, ySDF0, xSDF1, ySDF1, xSDF2, ySDF2);

flag = saveFileRectification(NomFicOut, FileNameSDF, Drift, OffsetX, OffsetY, timeIpingCentral);
if flag
    fillCircle(hp(end-n+1:end,:))
end
end


function [Drift, OffsetX, OffsetY] = solveShiftAndDrift(xSDF0, ySDF0, xSDF1, ySDF1, xSDF2, ySDF2)

XData = [xSDF0' ySDF0' xSDF1' ySDF1'];
% YData = [xSDF1' ySDF1'];
YData = [xSDF2' ySDF2'];
sz = size(XData);
XData = XData(:)';
YData = YData(:)';

%% Cr�ation du mod�le

params    = ClParametre('Name', 'Drift',    'Unit', 'deg', 'MinValue',  -45, 'MaxValue',  45, 'Value', 0);
params(2) = ClParametre('Name', 'X offset', 'Unit', 'm',   'MinValue', -250, 'MaxValue', 250, 'Value', 0);
params(3) = ClParametre('Name', 'Y offset', 'Unit', 'm',   'MinValue', -250, 'MaxValue', 250, 'Value', 0);

model = ClModel('Name', 'driftAndshift', 'Params', params, 'XData', XData, 'YData', YData, 'ParamsSup', {sz});
% model.plot();
% model.editProperties();

%% D�claration d'un instance d'ajustement

optim = ClOptim('XData', XData, 'YData', YData, 'models', model);
optim.optimize();
Params = optim.getParamsValue();
% optim.plot

Drift   = Params(1);
OffsetX = Params(2);
OffsetY = Params(3);

end


function timeIpingCentral = getTimePingCentral(SDF_PingAcrossDist, TabPoints)
if size(TabPoints, 1) == 1
    iPingCentral = floor(TabPoints(1,2));
else
    x = TabPoints(:,1);
    y = TabPoints(:,2);
    [~, sub] = sort(x);
    x = x(sub);
    y = y(sub);
    iPingCentral = floor(interp1(x, y, 0, 'linear', 'extrap'));
end
T = SDF_PingAcrossDist(1).Sonar.Time;
T = T.timeMat;
timeIpingCentral = T(iPingCentral);
end


function fillCircle(hp)
for k=1:numel(hp)
    if strcmp(get(hp(k), 'Marker'), 'o')
        Color = get(hp(k), 'Color');
        set(hp(k), 'MarkerFaceColor', Color)
    end
end
end
