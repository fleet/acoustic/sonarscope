% Calcul de la resolution longitudinale
%
% Syntax
%   b = sonar_resol_trans_bathy(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_resol_trans_bathy(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_resol_trans_bathy(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_sonar_resol_trans_bathy(this(i), CasDepth, H, CasDistance, Bathy, Emission, varargin{:});
end


function this = unitaire_sonar_resol_trans_bathy(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Angle d'�mission

if isempty(Emission)
    AngleEmission = [];
else
    AngleEmission = abs(Emission(1).Image(suby, subx));
end

%% Information de hauteur

if CasDistance == 1
    H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
else
    switch CasDepth
        case 1  % Il existe un layer 'Depth'
            H = abs(Bathy(1).Image(suby, subx));
        case 2  % Il existe une hauteur sonar
            H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
        case 3  % On possede l'angle d'emission
            H = repmat(abs(this.x(subx)), length(suby), 1) ./ tan(AngleEmission * (pi/180)); % On possede l'angle d'emission
        case 4  % On fixe une hauteur arbitraire
        otherwise
            my_warndlg('Pas d''information de hauteur',1);
            this = [];
            return
    end
end

%% Information de distance

switch CasDistance
    case 1  % Cas d'une donnee sonar en distance oblique
        D = repmat(abs(this.x(subx)), length(suby), 1);
    case 2  % Cas d'une donnee sonar en distance projetee
        D = sqrt(repmat(this.x(subx), length(suby), 1) .^ 2 + H .^ 2);
    case 3  % On possede l'angle d'emission
        D = H ./ cos(AngleEmission);
    otherwise
        my_warndlg('Pas d''information de distance',1);
        this = [];
        return
end

%% Angle d'emission

if isempty(Emission)
    D(D == 0) = NaN;
    D(abs(D) < H) = NaN;
    AngleEmission = acos(H ./ D) * 180 / pi;
end
subNan = isnan(this.Image(suby, subx));
AngleEmission(subNan) = NaN;

%% Calcul de la r�solution

Sonar = get(this, 'SonarDescription');
this = replace_Image(this, resol_trans_bathy(Sonar, H, AngleEmission));
this.ValNaN = NaN;

%% Mise � jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'm';
this.ColormapIndex = 3;

this.DataType = cl_image.indDataType('ResolBathyAcross');

% -------------------
% Completion du titre

this = update_Name(this);


