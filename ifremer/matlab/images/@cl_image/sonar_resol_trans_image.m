% Calcul de la resolution transversale de l'imagerie
%
% Syntax
%   b = sonar_resol_trans_image(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_resol_trans_image(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_resol_trans_image(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_sonar_resol_trans_image(this(i), CasDepth, H, CasDistance, Bathy, Emission, varargin{:});
end


function this = unitaire_sonar_resol_trans_image(this, CasDepth, H, CasDistance, Bathy, Emission, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Angle d'�mission

if isempty(Emission)
    AngleEmission = [];
else
    % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
    % flipud (voir cl_image/plus
    
    [XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Emission(1));
    if isempty(subx) || isempty(suby)
        str1 = 'Pas d''intersection commune entre les images.';
        str2 = 'No intersection between the 2 images.';
        my_warndlg(Lang(str1,str2), 1);
        this = [];
        return
    end
    AngleEmission = Emission(1).Image(subyb, subxb);
    
    % Dans le cas d'un sonar lateral, c'est l'angle par rapport a la verticale qu'il faut prendre
    RollUsedForEmission =  Emission(1).Sonar.RollUsedForEmission;
    if ~isempty(RollUsedForEmission)
        AngleEmission = AngleEmission - repmat(RollUsedForEmission(suby), 1, size(AngleEmission, 2));
    end
    
    AngleEmission = abs(AngleEmission);
end

%% Information de hauteur

if CasDistance == 1
    H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
else
    switch CasDepth
        case 1  % Il existe un layer 'Depth'
            % Attention : sort(sublLM) peut etre utile pour supprimer les tests de
            % flipud (voir cl_image/plus
            
            [XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, Bathy);
            if isempty(subx) || isempty(suby)
                str1 = 'Pas d''intersection commune entre les images.';
                str2 = 'No intersection between the 2 images.';
                my_warndlg(Lang(str1,str2), 1);
                this = [];
                return
            end
            H = abs(Bathy(1).Image(subyb, subxb));
        case 2  % Il existe une hauteur sonar
            H = repmat(abs(this.Sonar.Height(suby)), 1, length(subx));
        case 3  % On possede l'angle d'emission
            H = repmat(abs(this.x(subx)), length(suby), 1) ./ tan(AngleEmission * (pi/180)); % On possede l'angle d'emission
        case 4  % On fixe une hauteur arbitraire
        otherwise
            my_warndlg('Pas d''information de hauteur', 1);
            this = [];
            return
    end
end

%% Information de distance

switch CasDistance
    case 1  % Cas d'une donnee sonar en distance oblique
        D = repmat(abs(this.x(subx)), length(suby), 1);
    case 2  % Cas d'une donnee sonar en distance projetee
        D = sqrt(repmat(this.x(subx), length(suby), 1) .^ 2 + H .^ 2);
    case 3  % On possede l'angle d'emission
        D = H ./ cos(AngleEmission * (pi / 180));
    otherwise
        my_warndlg('Pas d''information de distance', 1);
        this = [];
        return
end

%% Angle d'�mission

if isempty(Emission)
    D(D == 0) = NaN;
    D(abs(D) < H) = NaN;
    AngleEmission = acos(H ./ D) * 180 / pi;
end
subNan = isnan(this.Image(suby, subx));
D(subNan) = NaN;

%% Calcul de la resolution

Sonar = get(this, 'SonarDescription');
this = replace_Image(this, resol_trans_image(Sonar, D, AngleEmission));
this.ValNaN = NaN;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'm';
this.ColormapIndex = 3;

this.DataType = cl_image.indDataType('ResolImageAcross');

%% Compl�tion du titre

this = update_Name(this);
