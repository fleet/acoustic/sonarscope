% Calcul du layer "EmissionBeam"
%
% Syntax
%   b = sonar_secteur_emission(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx         : subsampling in X
%   suby         : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_secteur_emission(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [TxBeamIndex, Frequency] = sonar_secteur_emission(this, b, varargin)

TxBeamIndex = cl_image.empty;
Frequency   = cl_image.empty;

N = length(this);
str1 = 'Calcul des images "TxBeamIndex"';
str2 = 'Computing images of "TxBeamIndex"';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    if length(b) == length(this)
        [a, b] = unitaire_sonar_secteur_emission(this(k), b(k), varargin{:});
    else
        [a, b] = unitaire_sonar_secteur_emission(this(k), b, varargin{:});
    end
    if ~isempty(a)
        TxBeamIndex(k) = a;
        Frequency(k)   = b;
    end
end
my_close(hw, 'MsgEnd')


function [TxBeamIndex, Frequency] = unitaire_sonar_secteur_emission(this, b, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contr�les

identEmissionAngle     = cl_image.indDataType('TxAngle');
identEmissionBeam      = cl_image.indDataType('RxBeamAngle');
identBeamPointingAngle = cl_image.indDataType('BeamPointingAngle');

flag = testSignature(this, 'DataType', [identEmissionAngle identEmissionBeam identBeamPointingAngle]);
if ~flag
    TxBeamIndex = [];
    Frequency   = [];
    return
end

SonarIdent = get(this.Sonar.Desciption, 'Sonar.Ident');
if SonarIdent == 0
    TxBeamIndex = [];
    Frequency   = [];
    my_warndlg('Pas de sonar defini', 1);
    return
end

%% Traitement

I = NaN(length(suby), length(subx), 'single');
K = NaN(length(suby), length(subx), 'single');

SonarName = get(this.Sonar.Desciption, 'Sonar.Name');
switch SonarName
    case 'EM3002D'
        N = this.nbColumns;
        sub = (subx <= (N/2));
        I(:,sub) = 1;
        sub = (subx > (N/2));
        I(:,sub) = 2;
        
    case 'EM3000D'
        N = this.nbColumns;
        sub = (subx <= (N/2));
        I(:,sub) = 1;
        sub = (subx > (N/2));
        I(:,sub) = 2;
        
    otherwise
        % TODO : si �a bugue pour 'EM1002' voir ce qui �tait fait dans secteur_frequency.m
        
        Sonar_Mode1_Depart = get(this.Sonar.Desciption, 'Sonar.Mode_1');
        if isempty(this.Sonar.PortMode_1)
            liste_PortMode_1 = Sonar_Mode1_Depart;
            PortMode_1 = zeros(size(this.Sonar.PingCounter),1) + liste_PortMode_1;
        else
            %             PortMode_1 = this.Sonar.PortMode_1(suby);
            try % rrfrhhh !!!!!!!
                PortMode_1 = this.Sonar.PortMode_1(suby,1); % Modifi� par JMA le 17/02/2012
            catch
                PortMode_1 = this.Sonar.PortMode_1(1,suby); % Modifi� par JMA le 17/02/2012
            end
            liste_PortMode_1 = unique(PortMode_1);
        end
        for k1=1:length(liste_PortMode_1)
            modeSonar = liste_PortMode_1(k1);
            this.Sonar.Desciption = set(this.Sonar.Desciption, 'Sonar.Mode_1', modeSonar);
            LimitAng = get(this.Sonar.Desciption, 'BeamForm.Tx.LimitAng');
            Freq     = get(this.Sonar.Desciption, 'BeamForm.Tx.Freq');
            
            if isempty(Freq)
                Freq = get(this.Sonar.Desciption, 'Sonar.Freq');
            end
            
            subMode = find(PortMode_1 == modeSonar);
            J1 = NaN(length(subMode), length(subx), 'single');
            J2 = NaN(length(subMode), length(subx), 'single');
            if isempty(b)
                teta = this.Image(suby(subMode), subx);
                
                % Traitement particulier de l'EM300D
                if SonarIdent == 6 % EM3000D
                    for k2=1:size(teta, 1)
                        subOK = ~isnan(teta(k2,:));
                        Signe = sign(mean(teta(k2,subOK)));
                        if Signe == 1
                            J1(k2,subOK) = 2;
                            J2(k2,subOK) = NaN; % TODO
                        else
                            J1(k2,subOK) = 1;
                            J2(k2,subOK) = NaN; % TODO
                        end
                    end
                else
                    for k2=1:size(LimitAng, 1)
                        sub = (teta >= LimitAng(k2,1)) & (teta <  LimitAng(k2,2));
                        J1(sub) = k2;
                        J2(sub) = Freq(k2);
                    end
                end
            else
                teta = this.Image(suby(subMode), subx);
                N = b.Image(suby(subMode), subx);
                for n = min(N(:), [], 'omitnan'):max(N(:), [], 'omitnan')
                    sub = find(N == n);
                    meanTeta = my_nanmean(teta(sub));
                    for k2=1:size(LimitAng, 1)
                        if (meanTeta >= LimitAng(k2,1)) && (meanTeta <  LimitAng(k2,2))
                            J1(sub) = k2;
                            J2(sub) = NaN; % TODO
                        end
                    end
                end
            end
            I(subMode,:) = J1;
            K(subMode,:) = J2;
        end
        this.Sonar.Desciption = set(this.Sonar.Desciption, 'Sonar.Mode_1', Sonar_Mode1_Depart);
end

%% Mise � jour de coordonn�es

ResolutionD = this.Sonar.ResolutionD;
ResolutionX = this.Sonar.ResolutionX;
this = majCoordonnees(this, subx, suby);
this.Sonar.ResolutionD = ResolutionD;
this.Sonar.ResolutionX = ResolutionX;

%% TxBeamIndex

TxBeamIndex = this;

TxBeamIndex.Image  = I;
TxBeamIndex.ValNaN = NaN;

%% Calcul des statistiques

[TxBeamIndex, flag] = compute_stats(TxBeamIndex);

%% Rehaussement de contraste

TxBeamIndex.TagSynchroContrast = num2str(rand(1));
if flag
    TxBeamIndex.CLim = [TxBeamIndex.StatValues.Min TxBeamIndex.StatValues.Max];
end

TxBeamIndex.Unit          = 'num';
TxBeamIndex.ColormapIndex = 3;

%% Compl�tion du titre

TxBeamIndex.DataType = cl_image.indDataType('TxBeamIndex');
TxBeamIndex = update_Name(TxBeamIndex);


%% Frequency

Frequency = this;

Frequency.Image  = K;
Frequency.ValNaN = NaN;

%% Calcul des statistiques

[Frequency, flag] = compute_stats(Frequency);

%% Rehaussement de contraste

Frequency.TagSynchroContrast = num2str(rand(1));
if flag
    Frequency.CLim = [Frequency.StatValues.Min Frequency.StatValues.Max];
end

Frequency.Unit          = 'kHz';
Frequency.ColormapIndex = 3;

%% Compl�tion du titre

Frequency.DataType = cl_image.indDataType('Frequency');
Frequency = update_Name(Frequency);
