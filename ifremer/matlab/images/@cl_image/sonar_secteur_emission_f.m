% Calcul du layer "EmissionBeam"
%
% Syntax
%   b = sonar_secteur_emission_f(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx         : subsampling in X
%   suby         : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_secteur_emission_f(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = sonar_secteur_emission_f(this, TxBeamIndex, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controles

identEmissionAngle = cl_image.indDataType('TxAngle');
identEmissionBeam  = cl_image.indDataType('RxBeamAngle');

flag = testSignature(this, 'DataType', [identEmissionAngle identEmissionBeam]);
if ~flag
    this = [];
    return
end

SonarIdent = get(this.Sonar.Desciption, 'Sonar.Ident');
if SonarIdent == 0
    this = [];
    my_warndlg('Pas de sonar defini', 1);
    return
end

% ----------
% Traitement

this.Image  = TxBeamIndex;
this.ValNaN = NaN;

% --------------------------
% Mise a jour de coordonnees

ResolutionD = this.Sonar.ResolutionD;
ResolutionX = this.Sonar.ResolutionX;
this = majCoordonnees(this, subx, suby);
this.Sonar.ResolutionD = ResolutionD;
this.Sonar.ResolutionX = ResolutionX;

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

this.Unit = 'deg';
this.ColormapIndex = 3;

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('TxBeamIndex');
this = update_Name(this);
