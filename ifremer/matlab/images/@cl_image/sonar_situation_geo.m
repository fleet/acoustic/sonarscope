% Calcul du layer emission
%
% Syntax
%   b = sonar_situation_geo(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_situation_geo(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, this, LatCentre, LonCentre] = sonar_situation_geo(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, this(i), LatCentre, LonCentre] = unitaire_sonar_situation_geo(this(i), varargin{:});
    if ~flag
        return
    end
end


    function [flag, this, LatCentre, LonCentre] = unitaire_sonar_situation_geo(this, varargin)
        
        [~, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>
        
        LatCentre = [];
        LonCentre = [];
        
        if length(suby) == length(this.y)
            flagZoom = 0;
        else
            flagZoom = 1;
        end
        
        %% Contr�les
        
        flag = testSignature(this, 'GeometryType', 'PingXxxx');
        if ~flag
            return
        end
        
        if isempty(this.Sonar.FishLatitude) || isempty(this.Sonar.FishLongitude)
            my_warndlg('FishLatitude et FishLongitude pas present dans ce fichier', 1);
            flag = 0;
            return
        else
            LatFish = this.Sonar.FishLatitude(suby);
            LonFish = this.Sonar.FishLongitude(suby);
        end
        
        LatMin = min(LatFish);
        LatMax = max(LatFish);
        LonMin = min(LonFish);
        LonMax = max(LonFish);
        
        diffLon = LonMax - LonMin;
        % diffLat = LatMax - LatMin;
        
        LatCentre = (LatMax + LatMin) / 2;
        LonCentre = (LonMax + LonMin) / 2;
        
        diffLonMin = 10;
        if diffLon < diffLonMin
            LonMin = LonCentre - diffLonMin / 2;
            LonMax = LonCentre + diffLonMin / 2;
        end
        
        diffLatMin = 10;
        if diffLon < diffLatMin
            LatMin = LatCentre - diffLatMin / 2;
            LatMin = max(LatMin, -90);
            LatMax = LatCentre + diffLatMin / 2;
            LatMax = min(LatMax, 90);
        end
        
        %% Affichage de la nav seule
        
        Fig1 = 547632765;
        if ~ishandle(Fig1)
            Fig1 = figure(Fig1);
            
            f = uimenu('Text', 'SonarScope');
            uimenu(f, 'Text', 'Save lines list', 'Callback', @SaveLines);
            uimenu(f, 'Text', 'Dimensions', 'Callback',      @Dimensions);
            uimenu(f, 'Text', 'Hide all lines',  'Callback', @HideAllLines);
            uimenu(f, 'Text', 'Awake all lines', 'Callback', @ShowAllLines);
            
            f = uimenu('Text', 'Axis');
            uimenu(f, 'Text', 'Normal', 'Callback', @AxisNormal);
            uimenu(f, 'Text', 'Geo',    'Callback', @AxisGeo);
        end
        
        T = this.Sonar.Time;
        T = T(suby);
        plot_navigation(this.InitialFileName, Fig1, this.Sonar.FishLongitude(suby), this.Sonar.FishLatitude(suby), T.timeMat, [])
        
        % figure(547632765);
        % plot(this.Sonar.FishLongitude, this.Sonar.FishLatitude, '+k'); grid on; hold on;
        % if flagZoom
        %     plot(LonFish, LatFish, '+r');
        % end
        % axisGeo
        
        %% R�cup�ration des param�tres cartographiques d�finis pour l'image d'origine
        
        Carto = this.Carto;
        if isempty(Carto)
            disp('Ici, il faudrait definir une carto par defaut : Mercator ou UTM, A VOIR AVEC Benoit Loubrieu')
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2);
        else
            Carto = set(Carto, 'Ellipsoide.Type', 11); % On impose wgs84 car etopo est defini ainsi
        end
        
        %% Lecture de etopo et creation de l'image
        
        [I, lon, lat, ~, flag] = Etopo([LatMin LatMax], [LonMin LonMax]);
        if ~flag
            return
        end
        
        %% Affichage de la nav sur une figure independante en attendant de pouvoir la
        % tracer sur clc_image
        
        figure; imagesc(lon, lat, I); axisGeo; colormap(jet(256)); colorbar
        hold on
        plot(this.Sonar.FishLongitude, this.Sonar.FishLatitude, '+k');
        if flagZoom
            plot(LonFish, LatFish, '+r');
        end
        
        %% Creation de l'image
        
        this = cl_image('Image', I, 'x', lon, 'y', lat, 'Name', 'Etopo (m)', 'ColormapIndex', 3);
        this.Carto = Carto;
        this.GeometryType = cl_image.indGeometryType('LatLong');
        % this.TagSynchroX = 'Longitude';
        % this.TagSynchroY = 'Latitude';
        
        %% Par ici la sortie
        
        % LatitudeCentre = LatCentre;
        % LongitudeCentre = LonCentre;
        flag = 1;
    end



% -----------------------------------------------------------
% Callback de sauvegarde des lignes affich�es dans un fichier

    function SaveLines(varargin)
        plot_navigation_SaveLines
    end

% ---------------------------------
% Callback de calcul des dimensions

    function Dimensions(varargin)
        plot_navigation_Dimensions
    end

% ------------------------
% Callback Hide All Lines

    function HideAllLines(varargin)
        plot_navigation_HideAllLines
    end

% ------------------------
% Callback Awake All Lines

    function ShowAllLines(varargin)
        plot_navigation_ShowAllLines
    end

% -------------------
% Callback Axe Normal

    function AxisNormal(varargin)
        GCA = findobj(gcf, 'Type', 'axes'); % Un peu rapide comme fa�on de faire
        axis(GCA(1))
        axis normal
    end

% -----------------------
% Callback Axe CartoNorm�

    function AxisGeo(varargin)
        GCA = findobj(gcf, 'Type', 'axes'); % Un peu rapide comme fa�on de faire
        axis(GCA(1))
        axisGeo
    end

end
