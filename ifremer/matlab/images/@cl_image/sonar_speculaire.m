% Cosmétique sur le spéculaire. Les pixels dont N < 1 sont réinterpolés
%
% Syntax
%   b = sonar_speculaire(a, A, ...)
%
% Input Arguments
%   a : Instance de cl_image contenant la réflectivité
%   A : Instance de cl_image contenant les angles
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant l'image filtree
%
% Examples
%   b = sonar_speculaire(a)
%   imagesc(a)
%   imagesc(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, that] = sonar_speculaire(this, A, sigma, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, X] = sonar_speculaire_unitaire(this(k), A(k), sigma, varargin{:});
    if ~flag
        that = [];
        return
    end
    that(k) = X; %#ok<AGROW>
end


function [flag, this] = sonar_speculaire_unitaire(this, A, AngleMax, sigma, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contrôles

flag = testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'));
if ~flag
    this = [];
    return
end

%% Processing

ImageName = extract_ImageName(this);
this = extraction(this, 'subx', subx, 'suby', suby);
XLim = this.XLim;
YLim = this.YLim;
A = extraction(A, 'XLim', XLim, 'YLim', YLim);
Mask = ROI_auto(A, 1, 'Mask', [],  [], [-AngleMax AngleMax], 1);
this = filterGauss(this, 'sigma', sigma, 'LayerMask', Mask, 'valMask', 1);

%% Complétion du titre

this = update_Name(this, 'Name', ImageName, 'Append', 'Speculaire');

