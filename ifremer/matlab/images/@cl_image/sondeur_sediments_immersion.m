% TODO : ce module fait double emploi avec subbottom_ImmersionConstante

function [flag, that] = sondeur_sediments_immersion(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, X] = unitaire_sondeur_sediments_immersion(this(i), varargin{:});
    if ~flag
        that = [];
        return
    end
    that(i) = X; %#ok<AGROW>
end


function [flag, this] = unitaire_sondeur_sediments_immersion(this, varargin)

[~, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controles

flag = isSubbottom(this);
if ~flag
    return
end

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('PingSamples'));
if ~flag
    return
end

if isempty(this.Sonar.Height)
    str1 = sprintf('Pas de hauteur renseignee. Veuillez la definir ou la calculer.');
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2),1);
    
    this = [];
    return
end

if isempty(this.Sonar.Immersion)
    str1 = sprintf('Pas d''immersion renseign�e.');
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2),1);
    
    this = [];
    return
end

%% Traitement

resol = this.Sonar.RawDataResol;
h = double(this.Sonar.Height(suby));
ImmersionInMeters = double(this.Sonar.Immersion(suby));
ImmersionInSample = floor(ImmersionInMeters / resol);
MIN  = min(ImmersionInSample - this.nbColumns);
MAX  = max(ImmersionInSample + floor(h));
N = MAX - MIN + 1;
%     diffImmersion = maxImmersion - minImmersion;

hInSamples = floor(-h);

decalage = ImmersionInSample - hInSamples - MAX;
% figure; plot(decalage); grid
I = NaN(this.nbRows, N, 'single');
for k=1:this.nbRows
    subPing = hInSamples(k):this.nbColumns;
    Ping = this.Image(k,subPing);
    sub = 1:length(subPing);
    I(k, sub - decalage(k) + 1) = Ping;
end

this = replace_Image(this, I);
this.x = 1:size(I,2);
subx = 1:length(this.x);
this.Sonar.Immersion(:) = MAX * resol;
this.Sonar.Height(:)    = decalage;

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
this.TagSynchroX = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'ImmersionCompensated');
