function splitMbgFile(this, NomFicPoints, repImport, repExport)

[flag, NbPings, tCoupure] = get_sonarTimeFromLogFile(NomFicPoints, repImport, repExport); %#ok<ASGLU>
if ~flag
    return
end

in = cl_netcdf('fileName', InitialFileName);
Date  = get_value(in, 'mbDate');
Heure = get_value(in, 'mbTime');
t = cl_time('timeIfr', Date, Heure);  % Attention ERREUR SUR ~/TMP/G16, LES HEURES NE SONT PAS LES MEMES
t = t.timeMat;
tCoupure = tCoupure.timeMat;

sub = (tCoupure <= min(t));
tCoupure(sub) = [];
sub = (tCoupure >= max(t));
tCoupure(sub) = [];

if isempty(tCoupure)
    str = sprintf('No time intersection between %s and cuttting times.', InitialFileName);
    my_warndlg(str, 1);
    return
end

nbCoupures = length(tCoupure);
tCoupure = sort(tCoupure);
index = ones(size(t)) * (nbCoupures+1);

for k2=length(tCoupure):-1:1
    sub = (t <= tCoupure(k2));
    index(sub) = k2;
end

Carto = get(this, 'Carto');
[nomDir, nomFic, ext] = fileparts(InitialFileName);
for k1=1:max(index)
    nomFicOut = fullfile(nomDir, [nomFic '_' num2str(k1) ext]);
    %     fidOut(k1) = fopen(nomFicOut{k1}, 'w+', typeIndian); %#ok
    suby = find(index == k1);

    [a, Carto] = import_CaraibesMbg(cl_image, InitialFileName, 'Sonar.Ident', 2, ...
        'Carto', Carto, 'indLayerImport', 1:9, 'suby', suby);

    %     SonarScope(a)

    flag = export_CaraibesMbg(a, 1, nomFicOut); %#ok<NASGU>

    for k2=1:length(a)
        GeometryType = a(k2).GeometryType;
        strGeometryType = cl_image.strGeometryType;
        DataType = a(k2).DataType;
        nomFicOut = fullfile(nomDir, [nomFic '_' num2str(k1) '_' strGeometryType{GeometryType} '_' cl_image.strDataType{DataType} '.ers']);
        flag = export_ermapper(a(k2), nomFicOut);
    end
end
