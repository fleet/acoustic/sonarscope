% Square root value of image
%
% Syntax
%  b = sqrt(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Abs  : O=sqrt(x), 1=sign(x)*abs(sqrt(x)) (Default : 0)
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Remarks : If Abs=0 negative values are set to NaN before computing sqrt
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I);
%   imagesc(a);
%
%   b = sqrt(a);
%   imagesc(b);
%
%   b = sqrt(a, 'Abs', 1);
%   imagesc(b);
%
% See also cl_image/mpower Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = sqrt(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('sqrt'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = sqrt_unit(this(k), varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = sqrt_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Abs] = getPropertyValue(varargin, 'Abs', false); %#ok<ASGLU>

%% Algorithm

I = this.Image(suby,subx,:);
I = singleUnlessDouble(I, this.ValNaN);
if Abs
    I = sign(I) .* sqrt(abs(I));
else
    I(I <= 0) = NaN;
    I = sqrt(I);
end

%% Output image

that = inherit(this, I,'subx',  subx, 'suby', suby, 'AppendName', 'sqrt');
