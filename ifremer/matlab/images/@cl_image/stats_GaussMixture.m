function this = stats_GaussMixture(this, nbGaussiennes, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

I = this.Image(suby,subx);
I = singleUnlessDouble(I(:), this.ValNaN);
I(isnan(I)) = [];
Options = statset('MaxIter', 2000);
GMModel = fitgmdist(I, nbGaussiennes, 'Options', Options);
mu                  = GMModel.mu;
Sigma               = sqrt(squeeze(GMModel.Sigma));
ComponentProportion = GMModel.ComponentProportion;
[~, ordre] = sort(mu);
mu                  = mu(ordre);
Sigma               = Sigma(ordre);
ComponentProportion = ComponentProportion(ordre);

bins = this.HistoCentralClasses;
[N, bins] = histo1D(I, bins(:), 'proba', 'ValNaN', this.ValNaN);
sub = find(N ~= 0);
fitHisto1D(bins(sub), N(sub), nbGaussiennes, ...
    'm', mu, 's', Sigma, 'w', ComponentProportion, 'Edit')