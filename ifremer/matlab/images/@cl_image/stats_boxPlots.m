function stats_boxPlots(this, indLayers, NbMinPoints, XLim, YLim, nomFicCsv, Sampling, varargin)

[varargin, flagValNat] = getPropertyValue(varargin, 'ValNat', 0);
[varargin, Mask]       = getPropertyValue(varargin, 'Mask',   []); %#ok<ASGLU>

if isempty(Mask) || isempty(Mask{1})
    indLayerMask = [];
    valMask      = [];
else
    indLayerMask = Mask{1};
    valMask      = Mask{2};
end

fig = FigUtils.createSScFigure;
nbImages = length(indLayers);

strCSV{1,  1} = 'Name';
strCSV{1,  2} = 'Mean';
strCSV{1,  3} = 'Std';
strCSV{1,  4} = 'Median';
strCSV{1,  5} = 'Min';
strCSV{1,  6} = 'Max';
strCSV{1,  7} = 'Range';
strCSV{1,  8} = 'Quant 0.5%';
strCSV{1,  9} = 'Quant 99.5%';
strCSV{1, 10} = 'Quant 1%';
strCSV{1, 11} = 'Quant 99%';
strCSV{1, 12} = 'Quant 1.5%';
strCSV{1, 13} = 'Quant 98.5%';
strCSV{1, 14} = 'Quant 3%';
strCSV{1, 15} = 'Quant 97%';
strCSV{1, 16} = 'Quant 25%';
strCSV{1, 17} = 'Quant 75%';
strCSV{1, 18} = 'PercentNaN';
strCSV{1, 19} = 'Sampling';
strCSV{1, 20} = 'NbData';
kCsv = 1;

yMin = Inf;
yMax = -Inf;
str1 = 'Calcul des Boxplots en cours';
str2 = 'Computing BoxPlots';
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    
    [that, flag] = extraction(this(indLayers(k)), 'XLim', XLim, 'YLim', YLim);
    if ~flag
        continue
    end
    
    if ~isempty(indLayerMask)
        [that, flag] = masquage(that, 'LayerMask', this(indLayerMask), 'valMask', valMask);
        if ~flag
            continue
        end
%         imagesc(that)
    end
    
    I = get(that, 'Image');
    if flagValNat
        I = reflec_dB2Amp(I);
    end
    
    Titre = extract_ImageName(this(indLayers(k)));
    if all(isnan(I(:)))
        str1 = sprintf('Pas de pixels valides pour "%s".', Titre);
        str2 = sprintf('No valid pixels for "%s".', Titre);
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    N = sum(~isnan(I(:)));
    if N < NbMinPoints
        str1 = sprintf('Pas assez de pixels valides pour "%s".\n(%d trouv�s, %d requis)', Titre, N, NbMinPoints);
        str2 = sprintf('Not enough valid pixels for "%s".\n(%d found, %d required)', Titre, N, NbMinPoints);
        my_warndlg(Lang(str1,str2), 0);
        continue
    end

    ValStats = stats(I, 'Seuil', [25 75 1.5 98.5], 'Sampling', Sampling);
    if isnan(ValStats.Moyenne)
        continue
    end

    if flagValNat
        mean_minus_std = ValStats.Moyenne - ValStats.Sigma;
        mean_plus_std  = ValStats.Moyenne + ValStats.Sigma;
        SigmaBricole = (reflec_Amp2dB(mean_plus_std) - reflec_Amp2dB(mean_minus_std)) / 2;
        if isnan(SigmaBricole)
            SigmaBricole = reflec_Amp2dB(mean_plus_std) - reflec_Amp2dB(ValStats.Moyenne);
        end
        ValStats.Sigma = SigmaBricole;
        ValStats.Variance    = ValStats.Variance ^ 2;
        ValStats.Moyenne = reflec_Amp2dB(ValStats.Moyenne);
        ValStats.Mediane     = reflec_Amp2dB(ValStats.Mediane);
        ValStats.Min         = reflec_Amp2dB(ValStats.Min);
        ValStats.Max         = reflec_Amp2dB(ValStats.Max);
        ValStats.Range       = ValStats.Max - ValStats.Min;
        ValStats.Quant_25_75 = reflec_Amp2dB(ValStats.Quant_25_75);
        ValStats.Quant_01_99 = reflec_Amp2dB(ValStats.Quant_01_99);
        ValStats.Quant_x     = reflec_Amp2dB(ValStats.Quant_x);
        ValStats.Quant_03_97 = reflec_Amp2dB(ValStats.Quant_03_97);
        ValStats.q1 = reflec_Amp2dB(ValStats.q1);
        ValStats.q3 = reflec_Amp2dB(ValStats.q3);
        ValStats.Skewness = NaN;
        ValStats.Kurtosis = NaN;
        ValStats.Entropy = NaN;
    end

    strStats = stats2str(ValStats);

    nbLigStats = size(strStats, 1);
    cstr = cell(nbLigStats+2,1);
    cstr{1} = sprintf('--------------------------------------------------');
    cstr{2} = sprintf('Image : %s', Titre);
    for kLigStats=1:nbLigStats
        cstr{2+kLigStats} = strStats(kLigStats,:);
    end
    
    kCsv = kCsv + 1;
    strCSV{kCsv,  1} = Titre;
    strCSV{kCsv,  2} = ValStats.Moyenne;
    strCSV{kCsv,  3} = ValStats.Sigma;
    strCSV{kCsv,  4} = ValStats.Mediane;
    strCSV{kCsv,  5} = ValStats.Min;
    strCSV{kCsv,  6} = ValStats.Max;
    strCSV{kCsv,  7} = ValStats.Range;
    strCSV{kCsv,  8} = ValStats.Quant_25_75(1);
    strCSV{kCsv,  9} = ValStats.Quant_25_75(2);
    strCSV{kCsv, 10} = ValStats.Quant_01_99(1);
    strCSV{kCsv, 11} = ValStats.Quant_01_99(2);
    strCSV{kCsv, 12} = ValStats.Quant_x(3);
    strCSV{kCsv, 13} = ValStats.Quant_x(4);
    strCSV{kCsv, 14} = ValStats.Quant_03_97(1);
    strCSV{kCsv, 15} = ValStats.Quant_03_97(2);
    strCSV{kCsv, 16} = ValStats.Quant_x(1);
    strCSV{kCsv, 17} = ValStats.Quant_x(2);
    strCSV{kCsv, 18} = ValStats.PercentNaN;
    strCSV{kCsv, 19} = ValStats.Sampling;
    strCSV{kCsv, 20} = ValStats.NbData;
    
    str = cell2str(cstr);
    disp(str)
    
    Q25  = ValStats.Quant_x(1:2);
    Q1p5 = ValStats.Quant_x(3:4);
    
    figure(fig)
    k1 = k-0.25;
    k2 = k+0.25;
    k3 = k-0.1;
    k4 = k+0.1;
    k5 = k-0.5;
    
    y1 = Q1p5;
    y2 = Q25;
    yMin = min(yMin, y1(1));
    yMax = max(yMax, y1(2));
    h = [];
    h(end+1) = PlotUtils.createSScPlot([k k], [y1(1) y2(1)], 'k--'); hold on %#ok<AGROW>
    
    h(end+1) = PlotUtils.createSScPlot([k k], [y2(2) y1(2)], 'k--'); hold on %#ok<AGROW>
    
    h(end+1) = PlotUtils.createSScPlot([k3 k4], [y1(1) y1(1)], 'k'); %#ok<AGROW>
    h(end+1) = PlotUtils.createSScPlot([k3 k4], [y1(2) y1(2)], 'k'); %#ok<AGROW>
    
    y = Q25;
    %     h(end+1) = my_patch([k1 k1 k2 k2], [y(1) y(2) y(2) y(1)], [0 0 1]); %#ok<AGROW>
    %     set(h(end), 'FaceColor', [1 1 1], 'EdgeColor', [0 0 1])
    h(end+1) = PlotUtils.createSScPlot([k1 k1 k2 k2 k1], [y(1) y(2) y(2) y(1) y(1)]); %#ok<AGROW>
    set(h(end), 'Color', [0 0 1])
    
    h(end+1) = text(k5, double(y1(1)), Titre, 'Rotation', 90, 'Interpreter', 'none'); %#ok<AGROW>
    
    y = ValStats.Mediane;
    h(end+1) = PlotUtils.createSScPlot([k1 k2], [y y], 'r'); %#ok<AGROW>
    
    cmenu = uicontextmenu('Tag', 'contextmenu_stats_boxPlots');
    uimenu(cmenu, 'Text', [Titre ' :'])
    uimenu(cmenu, 'Text', ' ')
    for iStat=1:size(strStats,1)
        uimenu(cmenu, 'Text', strStats(iStat,:))
    end
    set(h, 'UIContextMenu', cmenu);
    
    hg(k) = hggroup; %#ok<AGROW>
    set(h, 'Parent', hg(k)) % parent the barseries to the hggroup
    %}
end
my_close(hw, 'MsgEnd');

GCA = gca;
set(GCA, 'XTick', 1:nbImages)

Delta = (yMax-yMin) / 20;
axis([0 nbImages+1 yMin-Delta yMax+Delta])

%% Cr�ation du fichier de r�sum� des stats

flag = my_csvwrite(nomFicCsv, strCSV);
if flag
    try
        winopen(nomFicCsv)
    catch %#ok<CTCH>
        str1 = sprintf('La tentative d''ouvrir le fichier "%s" directement dans excel a �chou�. Il serait bon que vous associez les ".csv" avec excel (ou un autre tableur). Le fichier a n�anmoins �t� cr�� correctement.', nomFicCsv);
        str2 = sprintf('"%s" could not be open. You should associate the ".csv" extension to excel (or another spreadsheet). Nevertheless the file was created correctly.', nomFicCsv);
        my_warndlg(Lang(str1,str2), 1);
    end
    
    %         my_txtdlg(Lang('Informations g�n�rales sur l''image', 'General information on image'), str, ...
    %             'windowstyle', 'normal');
end
