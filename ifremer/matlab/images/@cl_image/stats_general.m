function stats_general(this, indImage, indLayerMask, valMask, subx, suby)

that = this(indImage);
I = get_val_ij(that, suby, subx);

if ~isempty(indLayerMask)
    J = I;
    sz = size(I);
    x = get(that, 'x');
    y = get(that, 'y');
    Masque = get_Masque(this(indLayerMask), x(subx), y(suby));
    K = zeros(size(Masque), 'uint8');
    for ik=1:length(valMask)
        %             sub = find(Masque == valMask(ik));
        sub = (Masque == valMask(ik));
        K(sub) = 1;
    end
    subNonMasque = (K == 0);
    J(subNonMasque) = NaN;
    that = replace_Image(that, reshape(J, sz));
    that = majCoordonnees(that, subx, suby);
    that = compute_stats(that);
    I = get_val_ij(that, [], []);
end

sStats = allstats(I(:),[],0,[0.5 1 3 25 99.5 99 97 75]);

sStats             = renameField(sStats,   'n',        'NbData');
sStats             = renameField(sStats,   'mean',     'Moyenne');
sStats             = renameField(sStats,   'median',   'Mediane');
sStats             = renameField(sStats,   'min',      'Min');
sStats             = renameField(sStats,   'max',      'Max');
sStats.Quant_03_97 = [sStats.p03 sStats.p07];
sStats.Quant_01_99 = [sStats.p02 sStats.p06];
sStats.Quant_25_75 = [sStats.p01 sStats.p05];
sStats             = renameField(sStats, 'var',    'Variance');
sStats             = renameField(sStats, 'skew',	'Skewness');
sStats             = renameField(sStats, 'kurt',	'Kurtosis');
sStats             = renameField(sStats, 'std',	'Sigma');
sStats.PercentNaN  = sStats.nan/sStats.NbData*100;
sStats.Entropy     = entropy(uint8(quantify(I, 'rangeIn', sStats.Quant_01_99)));

nbMaxSamplesStats = 2500000 * 2; % Max image 1500x1500, Ce chifre pourrait �tre augment� sur machines puissantes
if length(size(I)) < 2 
    sStats.Sampling = max(numel(I) / nbMaxSamplesStats, 1);
elseif length(size(I)) == 2 || length(size(I)) == 3
    Sampling        = numel(I) / nbMaxSamplesStats;
    sStats.Sampling = max(sqrt(Sampling), 1);
elseif length(size(I)) > 3
    Sampling = numel(Y) / nbMaxSamplesStats;
    sStats.Sampling = max(Sampling, 1);
end
sStats       = rmfield(sStats, {'p01', 'p02', 'p03', 'p04', 'p05', 'p06', 'p07', 'p08'});
sStats       = rmfield(sStats, {'q1' 'q3'});
sStats.Range = sStats.Max - sStats.Min;

strStats = stats2str(sStats, 'DataType', that.DataType);

printLog(sStats)

Name = this(indImage).Name;
str{1} = sprintf('Image : %s', Name);
str{2} = sprintf(' ');
for k=1:size(strStats, 1)
    str{end+1} = strStats(k,:); %#ok<AGROW>
end

edit_infoPixel(str, [], 'PromptString', 'SonarScope - statistics.')

str = cell2str(str);
disp(str)
my_warndlg(str, 0);


function printLog(ValStats)

filename = fullfile(my_tempdir, 'StatisticsCollection.txt');
if exist(filename, 'file')
    fid = fopen(filename, 'a+');
    if fid == -1
        % TODO appeler la fonction d'erreur pr�par�e dont je ne me souviens
        % plus du nom
        return
    end
else
    fid = fopen(filename, 'w+');
    fprintf(fid, 'Name\t');
    fprintf(fid, 'Mean\t');
    fprintf(fid, 'Std\t');
    fprintf(fid, 'Median\t');
    fprintf(fid, 'Min\t');
    fprintf(fid, 'Max\t');
    fprintf(fid, 'Range\t');
    fprintf(fid, 'Quant 0.5%%\t');
    fprintf(fid, 'Quant 99.5%%\t');
    fprintf(fid, '\n');
end


fprintf(fid, 'Name\t');
fprintf(fid, '%f\t', ValStats.Moyenne);
fprintf(fid, '%f\t', ValStats.Sigma);
fprintf(fid, '%f\t', ValStats.Mediane);
fprintf(fid, '%f\t', ValStats.Min);
fprintf(fid, '%f\t', ValStats.Max);
fprintf(fid, '%f\t', ValStats.Range);
fprintf(fid, '%f\t', ValStats.Quant_25_75(1));
fprintf(fid, '%f\t', ValStats.Quant_25_75(2));
fprintf(fid, '\n');

fclose(fid);

str1 = sprintf('Le fichier "%s" a �t� compl�t�.', filename);
str2 = sprintf('File "%s" has been appened.', filename);

% Comment� car �a fait dispara�tre la fen�tre des statistiques
my_warndlg(Lang(str1,str2), 0, 'Tag', 'printStatisticsOnLogFile', 'Timer', 0);
% warndlg(Lang(str1,str2)); % En attendant de r�tablir un bon fonctionnement de my_warndlg

%{
     PercentNaN: 99.1280
       Sampling: 1
       Skewness: 'Computed on demand'
       Kurtosis: 'Computed on demand'
        Entropy: 'Computed on demand'

%}
