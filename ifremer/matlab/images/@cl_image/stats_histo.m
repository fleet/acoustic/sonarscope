function that = stats_histo(this, indImage, subx, suby, msg)

[flag, indLayerMask, valMask] = paramsMasque(this, indImage);
if ~flag
    return
end
 
that = this(indImage);
I = get_val_ij(that, suby, subx);

if ~isempty(indLayerMask)
    J = I;
    sz = size(I);
    x = get(that, 'x');
    y = get(that, 'y');
    Masque = get_Masque(this(indLayerMask), x(subx), y(suby));
    K = zeros(size(Masque), 'uint8');
    for ik=1:length(valMask)
        %             sub = find(Masque == valMask(ik));
        sub = (Masque == valMask(ik));
        K(sub) = 1;
    end
    subNonMasque = (K == 0);
    J(subNonMasque) = NaN;
    that = replace_Image(that, reshape(J, sz));
    that = majCoordonnees(that, subx, suby);
    that = compute_stats(that);
    I = get_val_ij(that, [], []);
end

[bins, flagModif, flag] = saisieBinsHisto(that);
if ~flag
    return
end
if flagModif
    that = compute_stats(that, 'bins', bins);
end

switch msg
    case 'STATS_Histogramme_CI1'
        TypeHisto = 'Histo';
    case 'STATS_Probabilite_CI1'
        TypeHisto = 'Proba';
    case 'STATS_DensiteDeProbabilite_CI1'
        TypeHisto = 'Ddp';
    case 'STATS_Hypsometric_CI1'
        TypeHisto = 'Cumul';
end

Titre  = that.Name;
ValNaN = that.ValNaN;

DataType = that.DataType;
XLabel = [cl_image.strDataType{DataType} ' (' that.Unit ')'];
Fig = figure;
histo1D(I, bins, TypeHisto, 'ValNaN', ValNaN, 'Titre', Titre, 'XLabel', XLabel, 'Fig', Fig);
