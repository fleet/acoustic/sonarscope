% Ecart-type de l'image
%
% Syntax
%   m = std(a)
% 
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   val : Moyenne de l'image
%
% Examples 
%   for k=1:8
%       [I, label] = ImageSonar(k);
%       a(k) = cl_image('Image', I, 'Name', label, ...
%           'XUnit', 'm', 'YUnit', 'm', 'GeometryType', cl_image.indGeometryType('GeoYX'));
%   end
%   s = std(a)
%
% See also cl_image cl_image/mean Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function val = std(this)

for k=length(this):-1:1
    val(k) = this(k).StatValues.Sigma;
end
