function [flag, this] = subbottom_ImmersionConstante(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, that] = subbottom_ImmersionConstante_unitaire(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end

function [flag, this] = subbottom_ImmersionConstante_unitaire(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Contr�les

flag = isSubbottom(this);
if ~flag
    return
end

if isempty(this.Sonar.Immersion)
    str1 = 'Il n''y a pas de signal "Immersion" dans cette image.';
    str2 = 'No "Immersion" signal in this image.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Cr�ation de l'image

I = this.Image(suby, subx);

Hx_m = abs(this.Sonar.Immersion(suby));
subNaN = find(isnan(Hx_m));
if ~isempty(subNaN)
    subNonNaN = find(~isnan(Hx_m));
    Hx_m(subNaN) = my_interp1(subNonNaN, Hx_m(subNonNaN), subNaN, 'linear', 'extrap');
end

if isempty(this.Sonar.RawDataResol)
    this.Sonar.RawDataResol = mean(this.Sonar.SurfaceSoundSpeed ./ (2 * this.Sonar.SampleFrequency), 'omitnan');
end
Hx_sample = floor(Hx_m ./ this.Sonar.RawDataResol);
if all(Hx_sample == 0)
    str1 = 'Il semble qu''il n''y ait pas de signal "Immersion" dans cette image. Une immersion = 0 va �tre prise.';
    str2 = 'It seems thet their is no "Immersion" signal in this image. Val 0 will be used.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasDImeersionDansImage');
    %     return
end

HMin_sample = min(Hx_sample);
HMax_sample = max(Hx_sample);
nbCol = HMax_sample - HMin_sample + length(subx);
N = length(suby);
J = NaN(N, nbCol, 'single');
for k=1:N
    X = I(k,:);
    sub = (Hx_sample(k)-HMin_sample) + (1:length(subx));
    J(k,sub) = X;
end

%% Cr�ation de l'instance

this = replace_Image(this, J);

%% Mise � jour de coordonnees

this.x = (double(HMin_sample) + subx(1) - 1) + (1:double(nbCol));
this.x = this.x * this.Sonar.RawDataResol; % JMA le 29/01/2013
this = majCoordonnees(this, 1:nbCol, suby);

[~, nomSeul] = fileparts(this.InitialFileName);
if isempty(nomSeul)
    this.TagSynchroX = [this.TagSynchroX '-WCOff'];
else
    this.TagSynchroX = nomSeul;
    this.TagSynchroY = nomSeul;
end
this.GeometryType = cl_image.indGeometryType('PingDepth');
this.XUnit = 'm';

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Height

% TODO : cr�er une nouvelle g�om�trie du genre PiongSamplesUnderSeabed ou PingSamplesXxxx et
% conditionner l'affichage de la hauteur, supprimer ensuite cette bidouille infame

% SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
% SurfaceSoundSpeed(SurfaceSoundSpeed < 1000) = 1500;
% deltax = SurfaceSoundSpeed ./ (2*this.Sonar.SampleFrequency);

Height_sample = -abs(this.Sonar.Height(:,1));
% FigUtils.createSScFigure; PlotUtils.createSScPlot(Height_sample); grid on; title('Height_sample')

Height_metre = Height_sample * this.Sonar.RawDataResol;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(Height_metre); grid on; title('Height_metre')

newHeight_metre = -abs(this.Sonar.Immersion(:)) + Height_metre;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(this.Sonar.Immersion(:)); grid on; hold on; plot(newHeight_metre, 'r')
% FigUtils.createSScFigure; PlotUtils.createSScPlot(newHeight_metre); grid on; title('newHeight_sample')

% Offset_metre = HMin_sample * this.Sonar.RawDataResol;

newHeight_sample  = newHeight_metre / this.Sonar.RawDataResol;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(newHeight_sample); grid on; title('newHeight_sample + Offset_metre')

% this.Sonar.Height = newHeight_sample; % JMA le 29/01/2013
this.Sonar.Height = newHeight_metre; % JMA le 29/01/2013

% this.Sonar.Immersion(:) = (-HMin_sample - (subx(1)-1)) * this.Sonar.RawDataResol;
this.Sonar.Immersion(:) = -(subx(1)-1) * this.Sonar.RawDataResol;
% this.Sonar.Height(:,2) = this.Sonar.Height(:,1);

%% Compl�tion du titre

this = update_Name(this, 'Append', 'SubbottomCsteImmersion');

flag = 1;
