function [flag, this] = subbottom_compute_height(this, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, that] = unitaire_subbottom_compute_height(this(k), varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end


function [flag, this] = unitaire_subbottom_compute_height(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, TypeSignal]    = getPropertyValue(varargin, 'TypeSignal',     1);
[varargin, flagCleaning]  = getPropertyValue(varargin, 'CleaningHeight', 1);
[varargin, FiltreSonar]   = getPropertyValue(varargin, 'FiltreSonar',    []);
[varargin, FiltreProfile] = getPropertyValue(varargin, 'FiltreProfile',  []);
[varargin, ComputingUnit] = getPropertyValue(varargin, 'ComputingUnit',  []); %#ok<ASGLU>

if isempty(FiltreSonar)
    %     FiltreSonarA = [];
    %     FiltreSonarB = [];
    FiltreSonar = [2 0.1];
    [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
    FiltreSonarA = A;
    FiltreSonarB = B;
else
    if FiltreSonar(2) == 0
        FiltreSonarA = [];
        FiltreSonarB = [];
    else
        [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
        FiltreSonarA = A;
        FiltreSonarB = B;
    end
end

if isempty(FiltreProfile)
    FiltreProfileA = [];
    FiltreProfileB = [];
else
    if FiltreProfile(2) == 0
        FiltreProfileA = [];
        FiltreProfileB = [];
    else
        [B,A] = butter(FiltreProfile(1), FiltreProfile(2));
        FiltreProfileA = A;
        FiltreProfileB = B;
    end
end

%% Contr�les

flag = isSubbottom(this);
if ~flag
    this = [];
    return
end

if isempty(this.Sonar.Height)
    this.Sonar.Height = zeros(1, length(this.y));
end

% y = this.y(suby);
x = this.x(subx);
I = this.Image(suby, subx);
if ~isa(I, 'double')
    I = single(I);
end

[~, func] = paramsSubbottomHeightDetection(this, TypeSignal, ComputingUnit);
if ~isempty(func)
    I = func(I);
end

Titre = this.Name;

%% D�tection

H = subbottom_compute_height_image(I, TypeSignal, FiltreSonarA, FiltreSonarB, 'Titre', Titre);
H = -abs(x(H));

if ~isempty(FiltreProfileA) && ~isempty(FiltreProfileB)
    H = filtfilt(FiltreProfileB, FiltreProfileA, double(H));
end

%% Edition du signal pour �purage

if flagCleaning
    [~, nomFic] = fileparts(this.InitialFileName);
    Title = sprintf('Height of "%s"', nomFic);
    xSample = XSample('name', '#Pings',  'data', 1:length(H));
    ySample = YSample('Name', 'Height', 'data', H);
    signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
    s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
    s.openDialog();
    if s.okPressedOut
        SignalRetour = ySample.data;
    else
        SignalRetour = [];
    end
else
    SignalRetour = H;
end

%% Sauvegarde du signal dans le cache : TODO : en attendant de savoir
% injecter �a dans le .segy (action Anne ?)

% TODO : A AMELIORER DE TOUTE URGENCE

[~, ~, ext] = fileparts(this.InitialFileName);
switch lower(ext)
    case {'.seg'; '.segy'; '.sgy'}
        a = cl_segy('nomFic', this.InitialFileName);
        [flag, HBefore] = get_signal(a, 'Height');
%         [flag, HBefore] = SScCacheXMLBinUtils.getTimeSignalUnit(this.InitialFileName, 'xxxx', 'Height');
        
    case '.raw'
        a = cl_ExRaw('nomFic', this.InitialFileName);
%         [flag, HBefore] = get_signal(a, 'Height');
        [flag, HBefore] = SScCacheXMLBinUtils.getTimeSignalUnit(this.InitialFileName, 'Ssc_Position', 'Height');
        
    case '.hac'
        a = cl_hac('nomFic', this.InitialFileName);
        [flag, HBefore] = get_signal(a, 'Height');
%         [flag, HBefore] = SScCacheXMLBinUtils.getTimeSignalUnit(this.InitialFileName, 'xxxx', 'Height');
end
if flag
    Height = HBefore.Value;
else
    nbPings = get(a, 'nbPings');
    Height = NaN(nbPings, 1, 'single');
end

if ~isempty(SignalRetour)
    Height(suby) = SignalRetour;
    flag = save_Height(a, Height);
    if ~flag
        return
    end
    
    %% Sauvegarde du signal dans l'instance
    
    if size(this.Sonar.Height, 2) == 2
        this.Sonar.Height(suby,2) = NaN;
    end
    this.Sonar.Height(suby,1) = SignalRetour(:);
end
