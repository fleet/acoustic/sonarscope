
function [flag, this] = subbottom_demodulation(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = unitaire_subbottom_demodulation(this(i), varargin{:});
    if ~flag
        return
    end
    this(i) = that;
end


function [flag, this] = unitaire_subbottom_demodulation(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Controles

flag = isSubbottom(this);
if ~flag
    return
end

%% Cr�ation de l'image

I = this.Image(suby, subx);
N = length(suby);
J = NaN(size(I), 'single');
for k=1:N
    y = double(I(k,:));
    subNaN = find(isnan(y));
    if ~isempty(subNaN)
        subNonNaN = find(~isnan(y));
        y(subNaN) = interp1(subNonNaN, y(subNonNaN), subNaN);
    end
    X = demod(y, 1,6.9827, 'qam');
    J(k,:) = X;
end

%% Cr�ation de l'instance

this = replace_Image(this, J);

%% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'Demod');

flag = 1;
