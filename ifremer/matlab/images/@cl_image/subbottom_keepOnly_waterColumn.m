
function [flag, this] = subbottom_keepOnly_waterColumn(this, varargin)

nbImages = length(this);
for i=1:nbImages
    [flag, that] = subbottom_keepOnly_waterColumn_unit(this(i), varargin{:});
    if ~flag
        return
    end
    this(i) = that;
end


function [flag, this] = subbottom_keepOnly_waterColumn_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Marge] = getPropertyValue(varargin, 'Marge', 0); %#ok<ASGLU>

%% Controles

flag = isSubbottom(this);
if ~flag
    return
end

if isempty(this.Sonar.Height)
    str1 = 'Il n''y a pas de hauteur d�tect�e dans cette image.';
    str2 = 'No detected height in this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Cr�ation de l'image

I = this.Image(suby, subx);
H = abs(this.Sonar.Height(suby)) + Marge;
H = H - this.x(subx(1)) + 1;
maxHeight = max(floor(H));
nbCol = min(length(subx), maxHeight);

if nbCol > 10000
    str1 = 'Le signal de hauteur semble probl�matique. Nettoyez le et retentez votre chance.';
    str2 = 'The Height signal seems to be incorrect. Please clean it and try again.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end


N = length(suby);
J = NaN(N, nbCol, 'single');
for k=1:N
    n = min(nbCol, floor(H(k)));
    X = I(k,1:n);
    J(k,1:length(X)) = X;
end

%% Cr�ation de l'instance

this = replace_Image(this, J);

%% Mise a jour de coordonnees

this = majCoordonnees(this, subx(1:nbCol), suby);
% this.TagSynchroX = [this.TagSynchroX '-WCOnly'];

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Completion du titre

this = update_Name(this, 'Append', 'SubbottomOnlyWC');

flag = 1;
