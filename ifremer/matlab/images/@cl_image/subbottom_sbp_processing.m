function [flag, this] = subbottom_sbp_processing(this, paramsProcessing, varargin)

nbImages = length(this);
for k=1:nbImages
    [flag, that] = unitaire_subbottom_sbp_processing(this(k), paramsProcessing, varargin{:});
    if ~flag
        return
    end
    this(k) = that;
end


function [flag, this] = unitaire_subbottom_sbp_processing(this, paramsProcessing, varargin)

subx = 1:length(this.x);
suby = 1:length(this.y);

%% Controles

flag = isSubbottom(this);
if ~flag
    return
end

%% Cr�ation de l'image

pppp = this.InitialFileName;
[nomDir, nomFic, ext] = fileparts(pppp);
nomFic = [nomFic ext];
[~, Data] = sbp_processing(nomDir, nomFic, paramsProcessing);
J = Data.Data;

%% Cr�ation de l'instance

this = replace_Image(this, J);

%% Mise � jour de coordonn�es

this = majCoordonnees(this, subx, suby);

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Demod');

flag = 1;
