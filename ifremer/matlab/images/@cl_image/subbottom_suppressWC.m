function [flag, that] = subbottom_suppressWC(this, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('subbottom_suppressWC'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [K, flag] = subbottom_suppressWC_unit(this(k), varargin{:});
    if ~flag
        return
    end
    that(k) = K;
end
my_close(hw)


function [that, flag] = subbottom_suppressWC_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Marge] = getPropertyValue(varargin, 'Marge', 0); %#ok<ASGLU>

that = [];

%% Controls

flag = isSubbottom(this);
if ~flag
    return
end

if isempty(this.Sonar.Height)
    str1 = 'Il n''y a pas de hauteur d�tect�e dans cette image.';
    str2 = 'No detected height in this image.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Algorithm

I = this.Image(suby, subx);

Hx = abs(this.Sonar.Height(suby));
if all(Hx == 0)
    str1 = 'Il n''y a pas de hauteur d�tect�e dans cette image.';
    str2 = 'No detected height in this image.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

Hp = Hx - this.x(subx(1)) + 1 - Marge;
Hp = max(Hp, 1);
minHp = min(floor(Hp)); % Height en pixels par rapport au bord gauche du zoom actuel
nbCol = length(subx) - minHp + 1;
N = length(suby);
J = NaN(N, nbCol, 'single');
for k=1:N
    X = I(k,floor(Hp(k)):end);
    J(k,1:length(X)) = X;
end

%% Cr�ation de l'instance

this = replace_Image(this, J);

%% Mise a jour de coordonnees

this.x = 1:double(nbCol);
this = majCoordonnees(this, 1:nbCol, suby);
this.TagSynchroX = [this.TagSynchroX '-WCOff'];

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Height

% TODO : cr�er une nouvelle g�om�trie du genre PiongSamplesUnderSeabed ou PingSamplesXxxx et
% conditionner l'affichage de la hauteur, supprimer ensuite cette bidouille infame

% SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
% SurfaceSoundSpeed(SurfaceSoundSpeed < 1000) = 1500;
SurfaceSoundSpeed = 1500;
deltax = SurfaceSoundSpeed ./ (2*this.Sonar.SampleFrequency);
Immersion_m = -abs(this.Sonar.Immersion);
Height_sample = this.Sonar.Height(:,1);
Height_m = (abs(Height_sample) - Marge) .* deltax;
this.Sonar.Immersion = Immersion_m - Height_m;
% this.Sonar.Height(:,2) = this.Sonar.Height(:,1);
this.Sonar.Height(:,2) = 0;
this.Sonar.Height(:,1) = 0;

%% Completion du titre

that = update_Name(this, 'Append', 'SubbottomWithoutWC');

flag = 1;
