% Calcul de statistiques conditionnelles
%
% Syntax
%   plotCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = subtractCourbesStats(this, sub, identMinus, varargin)

[varargin, Tag] = getPropertyValue(varargin, 'Tag', []); %#ok<ASGLU>

if ~isempty(Tag)
    for k=1:length(sub)
        if strcmp(this.CourbesStatistiques(sub).bilan{1}(1).Tag, Tag)
            subsub(k) = logical(1); %#ok
        else
            subsub(k) = logical(0); %#ok
        end
    end
    sub = sub(subsub);
end

if isempty(sub)
    message_NoCurve(this)
    return
end

% CourbesStatsPlot(this.CourbesStatistiques(identMinus).bilan, 'Stats', 0);
% CourbesStatsPlot(this.CourbesStatistiques(sub).bilan, 'Stats', 0);

bilanMinus = this.CourbesStatistiques(identMinus).bilan{1};

nbCurves = length(sub);
for ib=1:nbCurves
    bilan = this.CourbesStatistiques(sub(ib)).bilan{1};
    for kSector=1:length(bilan)
        if isempty(bilan(kSector).x)
            continue
        end
        
        y = interp1(bilanMinus(kSector).x, bilanMinus(kSector).y, bilan(kSector).x);
        residus = bilan(kSector).y(:) - y(:);
        
        bilan(kSector).y = residus;
        %     bilan.nomZoneEtude = ['Subtraction : ' num2strCode(sub(ib))];
        bilan(kSector).nomZoneEtude = [bilan(kSector).nomZoneEtude ' (minus) ' bilanMinus(kSector).Tag];
        this.CourbesStatistiques(end+1).bilan{1} = bilan;
    end
end

CourbesStatsPlot(this.CourbesStatistiques(end-nbCurves+1:end).bilan, 'Stats', 0, 'subTypeCurve', 1);
