% R�sum� des infos d'une s�rie d'images
%
% Syntax
%   str = summary(a, repExport, ...)
%
% Input Arguments
%   a : instance de cl_image
%   repExport : ExportResults
%
% Name-Value Pair Arguments
%
%
% Output Arguments
%   str : une chaine de caract�res representative de l'instance
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, idx, str] = summary(these, varargin)

[varargin, flagExport]     = getPropertyValue(varargin, 'ExportResults', 1);
[varargin, nomDir]         = getPropertyValue(varargin, 'nomDir', my_tempdir);
[varargin, idxSelectLayer] = getPropertyValue(varargin, 'SelectLayer', []);

str = cell(length(these), 33);
% idx = [];
% % filteredStr = [];

n = 0;
n = n +1; str{1,n} = 'Name';
n = n +1; str{1,n} = 'nbRows';
n = n +1; str{1,n} = 'nbColumns';
n = n +1; str{1,n} = 'Unit';
n = n +1; str{1,n} = 'DataType';
n = n +1; str{1,n} = 'GeometryType';
n = n +1; str{1,n} = 'ImageType';
n = n +1; str{1,n} = 'TagSynchroX';
n = n +1; str{1,n} = 'Synchro in X';
n = n +1; str{1,n} = 'TagSynchroY';
n = n +1; str{1,n} = 'Synchro in Y';
n = n +1; str{1,n} = 'Synchro in X&Y';
n = n +1; str{1,n} = 'XLabel';
n = n +1; str{1,n} = 'XStepMetric';
n = n +1; str{1,n} = 'XStep';
n = n +1; str{1,n} = 'XLim';
n = n +1; str{1,n} = 'XRange';
n = n +1; str{1,n} = 'YLabel';
n = n +1; str{1,n} = 'YStepMetric';
n = n +1; str{1,n} = 'YStep';
n = n +1; str{1,n} = 'YLim';
n = n +1; str{1,n} = 'YRange';
n = n +1; str{1,n} = 'Class';
n = n +1; str{1,n} = 'Colormap';
n = n +1; str{1,n} = 'Memory';
n = n +1; str{1,n} = 'ValNaN';
n = n +1; str{1,n} = 'InitialFileFormat';
n = n +1; str{1,n} = 'InitialFileName';
n = n +1; str{1,n} = 'InitialImageName';
n = n +1; str{1,n} = 'Moyenne';
n = n +1; str{1,n} = 'Sigma';
n = n +1; str{1,n} = 'Min';
n = n +1; str{1,n} = 'Max';

%% Traitement

[ordreSynchroX, ordreSynchroY, ordreSynchroXY] = get_ordre(these);
N = length(these);
for k1=1:N
    this = these(k1);
    
    k2 = k1+1;
    n = 0;
    % GLU : le 05/07/2013.
    % Mis en commentaire suite � des tests de JMA sur les layers de C�line avec le
    % nouvel affichage. Cette ligne �tait peut-�tre n�cessaire pour exploiter la recherche de
    % cha�nes. Cela reste � �prouver sur tous les usages de Summary.
    % n = n +1; str{k2,n} = strrep(this.Name, ',', '.');
    n = n +1; str{k2,n} = this.Name;
    n = n +1; str{k2,n} = this.nbRows;
    n = n +1; str{k2,n} = this.nbColumns;
    n = n +1; str{k2,n} = this.Unit;
    n = n +1; str{k2,n} = cl_image.strDataType{this.DataType};
    n = n +1; str{k2,n} = this.strGeometryType{this.GeometryType};
    n = n +1; str{k2,n} = this.strImageType{this.ImageType};
    n = n +1; str{k2,n} = this.TagSynchroX;
    n = n +1; str{k2,n} = ordreSynchroX(k1);
    n = n +1; str{k2,n} = this.TagSynchroY;
    n = n +1; str{k2,n} = ordreSynchroY(k1);
    n = n +1; str{k2,n} = ordreSynchroXY(k1);
    
    n = n +1; str{k2,n} = this.XLabel;
    n = n +1; str{k2,n} = this.XStepMetric;
    n = n +1; str{k2,n} = [num2str(get(this, 'XStep')) ' (' this.XUnit ')'];
    n = n +1; str{k2,n} = num2strCode(this.XLim);
    n = n +1; str{k2,n} = diff(this.XLim);
    
    n = n +1; str{k2,n} = this.YLabel;
    n = n +1; str{k2,n} = this.YStepMetric;
    n = n +1; str{k2,n} = [num2str(get(this, 'YStep')) ' (' this.YUnit ')'];
    n = n +1; str{k2,n} = num2strCode(this.YLim);
    n = n +1; str{k2,n} = diff(this.YLim);
    
    try % Bug avec le projet d'Anne-Laure
        n = n +1; str{k2,n} = class(this.Image(1));
    catch %#ok<CTCH>
        str{k2,n} = 'Bug';
    end
    n = n +1; str{k2,n} = this.strColormapIndex{this.ColormapIndex};
    n = n +1; str{k2,n} = where(this);
    n = n +1; str{k2,n} = num2str(this.ValNaN);
    n = n +1; str{k2,n} = this.InitialFileFormat;
    n = n +1; str{k2,n} = this.InitialFileName;
    n = n +1; str{k2,n} = this.InitialImageName;
    
    if isempty(this.StatValues) % Rajout JMA le 13/04/2017
        this = compute_stats(this);
    end
    
    n = n +1; str{k2,n} = this.StatValues.Moyenne;
    n = n +1; str{k2,n} = this.StatValues.Sigma;
    n = n +1; str{k2,n} = this.StatValues.Min;
    n = n +1; str{k2,n} = this.StatValues.Max;
end

%% Affichage des r�sultats dans une table

%     str = str(:,1); % TODO : Modif JMA le 06/04/2016 au SHOM pour tester si �a limite les bugs de timing etre Java et Matlab
[flag, idx, strOut] = displaySummary(str, 'flagSelectLayer', idxSelectLayer, varargin{:});
% R�cup�ration des indices de tri (non exploit� pour l'instant).
str(1,:) = [];
if ~flag || isempty(strOut) || isempty(str)
    % Filtre total ou bouton Cancel
    str = [];
    return
end
% [C,ia,ib] = intersect(str(:,1),strOut(:,1),'stable'); %#ok<ASGLU>
str  = strOut;

%% Cr�ation d'un fichier ASCII et affichage dans un �diteur

if flagExport
    Filtre = fullfile(nomDir, 'SummaryImages.csv');
    [flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', Filtre);
    if ~flag
        return
    end
    flag = my_csvwrite(nomFic, str);
    if flag
        winopen(nomFic)
    end
end


function [ordreSynchroX, ordreSynchroY, ordreSynchroXY] = get_ordre(these)

for k=1:length(these)
    TagSynchroX{k} = these(k).TagSynchroX; %#ok<AGROW>
    TagSynchroY{k} = these(k).TagSynchroY; %#ok<AGROW>
end

listeTagSynchroX = unique(TagSynchroX);
listeTagSynchroY = unique(TagSynchroY);

nX = length(listeTagSynchroX);
for kX=1:nX
    sub = (strcmp(TagSynchroX, listeTagSynchroX{kX}));
    ordreSynchroX(sub) = kX; %#ok<AGROW>
end

nY = length(listeTagSynchroY);
for kY=1:nY
    sub = (strcmp(TagSynchroY, listeTagSynchroY{kY}));
    ordreSynchroY(sub) = kY; %#ok<AGROW>
end

for kX=1:nX
    for kY=1:nY
        sub = strcmp(TagSynchroX, listeTagSynchroX{kX}) & strcmp(TagSynchroY, listeTagSynchroY{kY});
        ordreSynchroXY(sub) = (kX-1)*nY + kY; %#ok<AGROW>
    end
end

pppp = unique(ordreSynchroXY);
for k=1:length(pppp)
    sub =(ordreSynchroXY == pppp(k));
    qqqq(sub) = k; %#ok<AGROW>
end
ordreSynchroXY = qqqq;
