% Processing sun shading according to an azimut
%
% Syntax
%   b = sunShadingGray(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   Azimuth : Azimut (deg)
%   subx    : Sub-sampling in abscissa
%   suby    : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   b = sunShadingGray(a);
%   imagesc(b);
%
% See also cl_image/sunShading cl_image/sunShadingGreatestSlope Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = sunShadingGray(this, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('sunShadingGray'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = sunShadingGray_unit(this(k), 'Silent', N~=1, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = sunShadingGray_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Azimuth] = getPropertyValue(varargin, 'Azimuth', this.Azimuth);
[varargin, Silent]  = getPropertyValue(varargin, 'Silent', 0); %#ok<ASGLU>

%% Algorithm

yDir = ((this.y(end) - this.y(1)) > 0);
if ~xor(yDir , (this.YDir == 1))
    Angle = 180 + Azimuth;
else
    Angle = -Azimuth;
end

XStepMetric = this.XStepMetric;
if isnan(XStepMetric)
    XStepMetric = 1;
end

YStepMetric = this.YStepMetric;
if isnan(YStepMetric)
    YStepMetric = 1;
end

stepx = (length(subx) - 1) / (subx(end) - subx(1));
suby = sort(suby);
stepy = (length(suby) - 1) / (suby(end) - suby(1));

angle = mod(Angle + 360, 360) * (pi/180);
alpha = sin(angle);
beta  = cos(angle);

h = [-1 -2 -1; 0 0 0; 1 2 1];
h = h / sum(abs(h(:)));

hx = h' / (XStepMetric / stepx);
hy = h  / (YStepMetric / stepy);

for k=this.nbSlides:-1:1
    [I, subNaN] = windowEnlarge(this.Image(suby, subx, k), [3 3], []);

    I = single(I);
    
    switch alpha
        case 1
            I = filter2(hx, I, 'valid');
        case -1
            I = -filter2(hx, I, 'valid');
        case 0
            I = beta * filter2(hy, I, 'valid');
        otherwise
            Ix = filter2(hx, I, 'valid');
            Iy = filter2(hy, I, 'valid');
            I = alpha * Ix + beta * Iy;
    end
    
    % Put the std in the instance
    if this.VertExagAuto == 0
        this.VertExagAuto = std(I(:), 'omitnan') * 2;
    end
    
    I = gradient2sunShadingGray(this, I);
    
    I(subNaN) = NaN;
    Image(:,:,k) = I;
end

%% Output image

Unit = [this.Unit ' / ' this.XUnit];
DataType = cl_image.indDataType('SunBW');
Azimuth  = this.Azimuth;
VertExag = this.VertExag;
Parameters = getHistoryParameters(Azimuth, VertExag);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'sunShadingGray', 'ValNaN', NaN, ...
    'Unit', Unit, 'DataType', DataType, 'ColormapIndex', 2, 'Parameters', Parameters);
