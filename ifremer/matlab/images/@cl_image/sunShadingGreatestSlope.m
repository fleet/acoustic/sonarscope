% Sun shading according to the greatest slope
%
% Syntax
%   [b, flag] = sunShadingGreatestSlope(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%   CLim : Color limits
%
% Output Arguments
%   b    : Instance(s) of cl_image
%   flag : 1=OK, 0=KO
%
% Examples
%   a = cl_image.import_Prismed;
%   imagesc(a);
%
%   [b, flag] = sunShadingGreatestSlope(a, 1);
%   imagesc(b);
%
% See also cl_image/sunShading cl_image/sunShadingGreatestSlopeGray Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [that, flag] = sunShadingGreatestSlope(this, VertExag, varargin)

flag = 0;
that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('sunShadingGreatestSlope'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, K] = sunShadingGreatestSlope_unit(this(k), VertExag, varargin{:});
    if ~flag
        return
    end
    that(k) = K;
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function [flag, that] = sunShadingGreatestSlope_unit(this, VertExag, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, CLim]   = getPropertyValue(varargin, 'CLim', []);
[varargin, Silent] = getPropertyValue(varargin, 'Silent', 0); %#ok<ASGLU>

that = [];

%% Controls

flag = testSignature(this, 'ImageType', 1);
if ~flag
    return
end

if this.nbSlides ~= 1
    str1 = 'Un seul canal svp !';
    str2 = 'Only one channel per image please.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Algorithm

if isempty(CLim)
    CLim = this.CLim;
    if CLim(1) == CLim(2)
        CLim = [CLim(1)-1 CLim(2)+1];
    end
end

if this.ColormapIndex == 2
    Colormap = jet(256);
else
    Colormap = this.Colormap;
end
if this.Video ==  2
    Colormap = flipud(Colormap);
end

ombre = sunShadingGreatestSlopeGray(this, VertExag, 'subx', subx, 'suby', suby, 'Silent', Silent);

I = this.Image(suby,subx);
I = singleUnlessDouble(I, this.ValNaN);
I(:,:,2) = ombre.Image(:,:);

window = [3 3];
fun = @(block_struct) kernelBlendSunshadingColors(block_struct.data, CLim, Colormap);
J = blockproc(I, [100 100], fun, 'BorderSize', window); % 'UseParallel', 1

% Remarque : il s'est av�r� contreproductif de vouloir utiliser la
% parall�lisation ici ('UseParallel', UseParallel) car la fonction Lee
% appele la fonction filter2 qui est d�j� parall�lis�e. De plus, cela
% emp�cherait d'impl�menter le ImageAdaptateur sur cl_memmapfile (technique
% � d�velopper pour �viter de faire I = this.Image(suby,subx);

%% Output image

VertExag = this.VertExag;
Parameters = getHistoryParameters(VertExag);
that = inherit(this, J, 'subx', subx, 'suby', suby, 'AppendName', 'GreatestSlope', 'ValNaN', 0, ...
    'ImageType', 2, 'Unit', ' ', 'DataType', cl_image.indDataType('SunColor'), 'Parameters', Parameters, 'CLim', CLim);


function RGB = kernelBlendSunshadingColors(X, CLim, Colormap)

nbCoul = size(Colormap, 1);
RGB = ind2rgb(gray2ind(mat2gray(double(X(:,:,1)), double(CLim)), nbCoul), Colormap);
RGB = single(RGB);
for k=1:3
    rgb = RGB(:,:,k) .* (X(:,:,2) +1) / 2;
    rgb(rgb < 0) = 0;
    rgb = uint8(1 + floor(254 * rgb));
    RGB(:,:,k) = rgb;
end
RGB = uint8(RGB);
