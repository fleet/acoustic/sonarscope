% Processing sun shading according to the maximum slope
%
% Syntax
%   b = sunShadingGreatestSlopeGray(a, VertExag, ...)
%
% Input Arguments
%   a        : Instance(s) of cl_image
%   VertExag : Vertical exageration % TODO : devrait passer en PN/PV
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = cl_image.import_ermapper(nomFic);
%   imagesc(a);
%
%   b = sunShadingGreatestSlopeGray(a, 1);
%   imagesc(b);
%
% See also cl_image/sunShadingGray Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = sunShadingGreatestSlopeGray(this, VertExag, varargin)

that = cl_image.empty;

N = length(this);
hw = create_waitbar(waitbarMsg('sunShadingGreatestSlopeGray'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(k) = sunShadingGreatestSlopeGray_unit(this(k), VertExag, varargin{:});
    if N > 1
        that(k) = optimiseMemory(that(k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)


function that = sunShadingGreatestSlopeGray_unit(this, VertExag, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Silent] = getPropertyValue(varargin, 'Silent', 0); %#ok<ASGLU>

%% Algorithm

XStepMetric = this.XStepMetric;
if isnan(XStepMetric)
    XStepMetric = 1;
end

YStepMetric = this.YStepMetric;
if isnan(YStepMetric)
    YStepMetric = 1;
end

stepx = (length(subx) - 1) / (subx(end) - subx(1));
suby = sort(suby);
stepy = (length(suby) - 1) / (suby(end) - suby(1));

h = [-1 -2 -1; 0 0 0; 1 2 1];
h = h / sum(abs(h(:)));

hx = h' / (XStepMetric / stepx);
hy = h  / (YStepMetric / stepy);

for k=this.nbSlides:-1:1
    I = this.Image(suby,subx,k);
    I = singleUnlessDouble(I, this.ValNaN);
    
    [J, subNaN] = windowEnlarge(I, [3 3], []);

    I = filter2(hy, single(J), 'valid');
    K = filter2(hx, single(J), 'valid');
    
    % Put the std in the instance
    if this.VertExagAuto == 0
        this.VertExagAuto = std(I(:), 'omitnan') * 2;
    end
    
    I = gradient2sunShadingGray(this, I, 'VertExag', VertExag);
    K = gradient2sunShadingGray(this, K, 'VertExag', VertExag);
    I = sqrt((I+1).^2+(K+1).^2);
    %     I = (I / sqrt(2)) - 1;
    %     I = I * sqrt(2);
    % Ce qui revient � :
    I = I - sqrt(2);
    
    I(subNaN) = NaN;
    Image(:,:,k) = I;
end

%% Output image

Unit = [this.Unit ' / ' this.XUnit]; % TODO : faire �a mieux que �a
DataType = cl_image.indDataType('SunBW');
Parameters = getHistoryParameters(VertExag);
that = inherit(this, Image, 'subx', subx, 'suby', suby, 'AppendName', 'sunShadingGreatestSlopeGray', 'ValNaN', NaN, ...
    'Unit', Unit, 'ColormapIndex', 2, 'DataType', DataType, 'Parameters', Parameters);
