% Representation 3D
%
% Syntax
%   surf(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%   Texture : Instance de cl_image contenant la texture a mapper
%   ZLim    : Limites de l'axe des z (Propriete CLim par defaut)
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   a = import_CaraibesGrid(cl_image, nomFic);
%   imagesc(a)
%   surf(a)
%   b = sunShading(a);
%   surf(a, 'Texture', b)
%   surf(a, 'Azimut', 110, 'Elevation', 30, 'Exageration', 20, 'Light', 4)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function hAxe = surf(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Texture] = getPropertyValue(varargin, 'Texture', []);
[varargin, ZLim]    = getPropertyValue(varargin, 'ZLim', []);

[varargin, IHM] = getFlag(varargin, 'IHM');

z = double(this.Image(suby, subx));
x = this.x(subx);
y = this.y(suby);

if isempty(Texture)
    CLim = [];
    map = this.Colormap;
else
    b = extraction(Texture, 'x', this.x(subx), 'y', this.y(suby));
    if Texture.ImageType == 2
        Texture = double(b.Image);
        map  = [];
        ZLim = [];
        CLim = [];
    else
        [varargin, CLim] = getPropertyValue(varargin, 'CLim', Texture.CLim);
        map = Texture.Colormap;
        
        b = double(b.Image);
        minb = min(b(:));
        b = b - minb;
        CLim = CLim - minb;
        maxb = max(b(:));
        b = b / maxb;
        CLim = CLim / maxb;
        
        %     if max(b(:)) > 1
        %         b = b / 255;
        %         CLim = CLim / 255;
        %     end
        Texture = b;
    end
end
if IHM
    hAxe = my_surf(x, y, z, ...
        'map', map, 'ZLim', ZLim, ...
        'Texture', Texture, 'CLim', CLim, ...
        'GeometryType', this.GeometryType, ...
        varargin{:});
else
    hAxe = surf_ifr(x, y, z, ...
        'map', map, 'ZLim', ZLim, ...
        'Texture', Texture, 'CLim', CLim, ...
        'GeometryType', this.GeometryType, ...
        varargin{:});
    %     hAxe = surf_ifr(x, y, z, ...
    %         'Texture', double(Texture.Image), ...
    %         'GeometryType', this.GeometryType, ...
    %         varargin{:});
end
