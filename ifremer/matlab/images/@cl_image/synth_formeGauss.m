% Cr�ation distribution gaussienne 
% 
% this = synth_formeGauss(this)
%
% Input Arguments
%    this : instance de cl_image
%
% Output Arguments
%    this : instance de cl_image initialisee
%
% See also cl_image
% Authors : VB
% ----------------------------------------------------------------------------

function this = synth_formeGauss(~, px, py, x, y, Parametres)

distrib = zeros(length(x), length(y));
Image	= zeros(length(x), length(y));

for k1=1:size(Parametres, 1)
    xCenter = Parametres(k1,1);
    yCenter = Parametres(k1,2);
    Sigma1  = Parametres(k1,3);
    Sigma2  = Parametres(k1,4);
    Angle   = Parametres(k1,5)*pi/180;
    Weight	= Parametres(k1,6);

    % Xdistrib = normpdf(x, xCenter, Sigma1);
    % Ydistrib = normpdf(y, yCenter, Sigma2);

    cA = cos(Angle);
    sA = sin(Angle);

    % Calcul distribution 2D
    for k2=1:length(x)
        for j=1:length(y)
            x_new = (x(k2)-xCenter)*sA + (y(j)-yCenter)*cA;
            y_new = (x(k2)-xCenter)*cA + (y(j)-yCenter)*(-sA);

            distrib(k1,j) = exp(-0.5*(x_new/Sigma1)^2)*exp(-0.5*(y_new/Sigma2)^2);
        end
    end
    distrib = distrib / max(max(distrib(:)));

    Image = Image + Weight * distrib;
end

Image = Image / sum(sum(Image)); % normalisation

this = cl_image('Image', Image, 'XLabel', px, 'YLabel', py, 'x', x, 'y', y, 'ColormapIndex', 3);
