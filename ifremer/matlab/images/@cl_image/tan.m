% Tangent of angles in rd
%
% Syntax
%   b = tan(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(linspace(-pi/3, pi/3, 200), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'rd');
%   imagesc(a);
%
%   b = tan(a);
%   imagesc(b);
%
% See also cl_image/acos cl_image/cosd Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = tan(this, varargin)
that = process_function_type1(this, @tan, 'Unit', [], 'expectedUnit', 'rd', varargin{:});
