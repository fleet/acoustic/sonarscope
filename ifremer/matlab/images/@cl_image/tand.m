% Tangent of angles in deg
%
% Syntax
%   b = tand(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(linspace(-60, 60, 200), 256, 1);
%   a = cl_image('Image', I, 'Unit', 'deg');
%   imagesc(a);
%
%   b = tand(a);
%   imagesc(b);
%
% See also cl_image/acos cl_image/cosd Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = tand(this, varargin)
that = process_function_type1(this, @tand, 'Unit', [], 'expectedUnit', 'deg', varargin{:});
