%Version de sonar_mosaique_plot avec r�glage auto de l'exag�ration
%verticale
%
% Syntax
%   hAxe = testExaVert()
%
% Input Arguments
%   x : Abscisses OU Limites (XLim) OU []
%   y : Ordonnees OU Limites (YLim) OU []
%   z : Donn�e � repr�senter en 3D
%
% Name-Value Pair Arguments
%   Exageration     : Exageration verticale (1 par defaut)
%
% Output Arguments
%   hAxe  : Handle de l'axe
%
% Examples
%   [X,Y,Z] = peaks(100);
%   testExaVert([], [], Z, 'Azimut', 200, 'Elevation', 45, 'Exageration', 40, 'Light', 4);
%   testExaVert(X(1,:), Y(:,1), Z, 'Azimut', 200, 'Elevation', 45, 'Exageration', 4, 'Light', 4);
%   testExaVert(X, Y, Z, 'Azimut', 200, 'Elevation', 45, 'Exageration', 4, 'Light', 4);
%
% See also testExaVert cl_image/surf Authors
% References : references de l'article d'ou est tire cette fonction
% Authors : JMA
%--------------------------------------------------------------------------

function hAxe = testExaVert(this, Lat, Lon, Z, varargin)

[varargin, Exageration] = getPropertyValue(varargin, 'Exageration', 1);

HautPanelContol = 4;
HautRang1 = 1.0;

% strBox = {'On'; 'Off'};

% repExport = my_tempdir;

%% Use system background color for GUI components
panelColor = get(0,'DefaultUicontrolBackgroundColor');

%% ------------ Callback Functions ---------------


% Figure resize function
    function figResize(varargin)
        if exist('botPanel', 'var') && ishandle(botPanel)
            fpos = get(f, 'Position');
            set(botPanel, 'Position', ...
                [1/20 1/20 fpos(3) HautPanelContol])
            set(AxePanel,'Position', ...
                [1/20 HautPanelContol  fpos(3) fpos(4)-HautPanelContol]);
        end
    end

% Bottom panel resize function
    function botPanelResize(varargin)
    end

%% Callback for plot button
%     function plotCallback(varargin)
%         DataAspectRatio(1) = 1;
%         DataAspectRatio(2) = 1;
%         ExaVert = str2num(get(h_ExaVert,    'String'));
%         DataAspectRatio(3) = 1 / 2^ExaVert;
%         set(gca, 'DataAspectRatio', DataAspectRatio)
%     end
    function plotCallback(varargin)
        DataAspectRatio(1) = 1;
        DataAspectRatio(2) = 1;
        ExaVert = get(h_ExaVert,    'Value');
        DataAspectRatio(3) = 1 / 2^ExaVert;
        set(gca, 'DataAspectRatio', DataAspectRatio)
    end

%% ------------ GUI layout ---------------

% ---------------------
% Creation de la figure

f = figure('Units', 'characters', ...
    'Position',[30 30 120 35], ...
    'ResizeFcn', @figResize, ...
    'Color', panelColor, ...
    'Renderer', 'OpenGL');

% -------------------------------
% Creation du panneau des boutons

botPanel = uipanel('BorderType', 'etchedin', ...
    'Units', 'characters', ...
    'Position',[1/20 1/20 119.9 HautPanelContol], ...
    'Parent', f, ...
    'BackgroundColor', panelColor, ...
    'ResizeFcn', @botPanelResize);

% -----------------------------------
% Creation du panneau contenant l'axe

AxePanel = uipanel('bordertype', 'etchedin', ...
    'Units', 'characters', ...
    'Position', [1/20 HautPanelContol 119.9 27], ...
    'BackgroundColor', panelColor, ...
    'Parent', f);

%% Add an axes to the center panel
hAxe = axes('parent',AxePanel);



% --------------------
% Creation des boutons


% Exageration verticale
uicontrol(f, 'Style', 'text', 'Units', 'characters', ...
    'Position',[5 HautRang1 15 2], ...
    'String', 'Exa vert', ...
    'Parent', botPanel);
h_ExaVert = uicontrol(f, 'Style', 'slider', 'Units', 'characters', ...
    'Position', [25 HautRang1 15 2], ...
    'BackgroundColor', 'white', ...
    'Value', Exageration, ...
    'Min', -10, 'Max', 10, ...
    'Parent', botPanel, ...
    'Callback', @plotCallback);

hAxe = sonar_mosaique_plot(this, Lat, Lon, Z, 'hAxe', hAxe, varargin{:});
end



