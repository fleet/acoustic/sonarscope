% Test de signatures d'une image
%
% Syntax
%   flag = testSignature(a, ...)
%
% Input Arguments
%   a : Instance de cl_image
%
% Name-Value Pair Arguments
%   GeometryType : Valeurs testees pour GeometryType
%                    '1=Unknown' | '2=Metrique' | '3=Geographique' | '4=SonarD' | '5=SonarX'
%   DataType     : Valeurs testees pour DataType 'Reflectivity', 'TxAngle' ...
%
% Name-only Arguments
%   noMessage : Pour ne pas avoir de message de non conformit�
%
% Output Arguments
%   flag : Conformite � la signature (1 ou 0)
%
% Examples
%   DT = cl_image.indDataType('Reflectivity');
%   flag = testSignature(cl_image, 'DataType', DT, 'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingAcrossDist')])
%   flag = testSignature(cl_image, 'DataType', DT, 'GeometryType', 'PingXxxx')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = testSignature(this, varargin)

[varargin, DataType]          = getPropertyValue(varargin, 'DataType',          []);
[varargin, GeometryType]      = getPropertyValue(varargin, 'GeometryType',      []);
[varargin, ImageType]         = getPropertyValue(varargin, 'ImageType',         []);
[varargin, ClassName]         = getPropertyValue(varargin, 'ClassName',         []);
[varargin, Message]           = getPropertyValue(varargin, 'Message',           []);
[varargin, InitialFileFormat] = getPropertyValue(varargin, 'InitialFileFormat', []);

[varargin, noMessage] = getFlag(varargin, 'noMessage'); %#ok<ASGLU>

%% Test InitialFileFormat

if ~isempty(InitialFileFormat)
    flag = strcmp(this.InitialFileFormat, InitialFileFormat);
    if ~flag
        if ~noMessage
            str1 = sprintf('Condition non v�rifi�e pour "InitialFileFormat"\nValeur attendue : "%s"\nValeur de l''image : "%s"', InitialFileFormat, this.InitialFileFormat);
            str2 = sprintf('Condition not reached for "InitialFileFormat"\nExpected value : "%s"\nCurrent image : "%s"', InitialFileFormat, this.InitialFileFormat);
            my_warndlg(Lang(str1,str2), 1);
        end
        return
    end
end

%% Test GeometryType et DataType

if ~isempty(GeometryType) && strcmp(GeometryType, 'PingXxxx')
    GeometryType = [cl_image.indGeometryType('PingBeam') ...
        cl_image.indGeometryType('PingSamples') ...
        cl_image.indGeometryType('PingRange') ...
        cl_image.indGeometryType('PingAcrossDist') ...
        cl_image.indGeometryType('PingDepth') ...
        cl_image.indGeometryType('PingAcrossSample')];
end

flag = ones(size(this));
for k1=1:length(this)
    str{1} = Lang(['Condition non v�rifi�e pour : ' this(k1).Name], ...
        ['Condition not verified for : ' this(k1).Name]);
    str{2} = ' ';
    
    if ~isempty(DataType)
        DataTypeImage = this(k1).DataType;
        test = any(DataTypeImage == DataType);
        if ~test
            flag(k1) = test;
        end
        
        s = [];
        for k2=1:length(DataType)
            s1 = cl_image.strDataType{DataType(k2)};
            s = [s Lang(' ou ', ' or ') s1]; %#ok<AGROW>
        end
        s(1:4) = [];
        str{end+1} = sprintf('%s : %s', Lang('DataType attendu', ...
            'DataType expected'), s); %#ok<AGROW>
        str{end+1} = sprintf('%s : %s', Lang('DataType de l''image courante', ...
            'DataType of current image'), ...
            cl_image.strDataType{DataTypeImage}); %#ok<AGROW>
        
        if test
            str{end+1} = Lang('DataType : Condition respect�e', ...
                'DataType : This condition is respected'); %#ok<AGROW>
            str{end+1} = ''; %#ok<AGROW>
        else
            str{end+1} = Lang('{\color{red}\bf\Rightarrow DataType : Condition non respect�e}', ...
                '{\color{red}\bf\Rightarrow DataType : condition not respected}'); %#ok<AGROW>
            str{end+1} = ''; %#ok<AGROW>
        end
    end
    str{end+1} = ' '; %#ok<AGROW>
    
    if ~isempty(GeometryType)
        GeometryTypeImage = this(k1).GeometryType;
        test = any(GeometryTypeImage == GeometryType);
        if ~test
            flag(k1) = test;
        end
        
        s = [];
        for k2=1:length(GeometryType)
            s1 = this(k1).strGeometryType{GeometryType(k2)};
            s = [s ' or ' s1]; %#ok<AGROW>
        end
        s(1:4) = [];
        str{end+1} = sprintf('%s : %s', Lang('GeometryType attendu', ...
            'GeometryType expected'), s); %#ok<AGROW>
        str{end+1} = sprintf('%s : %s', Lang('GeometryType de l''image courante', ...
            'GeometryType of current image'), ...
            this(k1).strGeometryType{GeometryTypeImage}); %#ok<AGROW>
        
        if test
            str{end+1} = Lang('GeometryType : Condition respect�e', ...
                'GeometryType : This condition is respected'); %#ok<AGROW>
            str{end+1} = ''; %#ok<AGROW>
        else
            str{end+1} = Lang('{\color{red}\bf\Rightarrow GeometryType : Condition non respect�e}', ...
                '{\color{red}\bf\Rightarrow GeometryType : Condition not respected}'); %#ok<AGROW>
            str{end+1} = ''; %#ok<AGROW>
        end
    end
    
    if ~isempty(ImageType)
        ImageTypeImage = this(k1).ImageType;
        test = any(ImageTypeImage == ImageType);
        if ~test
            flag(k1) = test;
        end
        
        s = [];
        for k2=1:length(ImageType)
            s1 = this(k1).strImageType{ImageType(k2)};
            s = [s ' or ' s1]; %#ok<AGROW>
        end
        s(1:4) = [];
        str{end+1} = sprintf('%s : %s', Lang('ImageType attendu', 'ImageType expected'), s); %#ok<AGROW>
        str{end+1} = sprintf('%s : %s', Lang('ImageType de l''image courante', 'ImageType of current image'), ...
            this(k1).strImageType{ImageTypeImage}); %#ok<AGROW>
        
        if test
            str{end+1} = Lang('ImageType : Condition respect�e', 'ImageType : Condition respected'); %#ok<AGROW>
            str{end+1} = ''; %#ok<AGROW>
        else
            str{end+1} = Lang('{\color{red}\bf\Rightarrow ImageType : Condition non respect�e}', ...
                '{\color{red}\bf\Rightarrow ImageType : Condition not respected}'); %#ok<AGROW>
            str{end+1} = ''; %#ok<AGROW>
        end
    end
    
    if ~flag(k1) && ~noMessage
        if ~isempty(Message)
            str{end+1} = Message; %#ok<AGROW>
        end
        
        CreateMode.WindowStyle = 'non-modal';
        CreateMode.Interpreter = 'tex';
        
        if iscell(str)
            msg = cell2str(str);
        else
            msg = str;
        end
        my_warndlg(msg, 1, 'CreateMode', CreateMode);
    end
end

%% Test ClassName

if ~isempty(ClassName)
    ClassNameImage = get(this,'ClassName');
    flag = strcmp(ClassNameImage, ClassName);
    if ~flag
        if ~noMessage
            str1 = sprintf('Condition non v�rifi�e pour "ClassName"\nValeur attendue : "%s"\nValeur de l''image : "%s"', ClassName, ClassNameImage);
            str2 = sprintf('Condition not reached for "ClassName"\nExpected value : "%s"\nCurrent image : "%s"', ClassName, ClassNameImage);
            my_warndlg(Lang(str1,str2), 1);
        end
        return
    end
end
