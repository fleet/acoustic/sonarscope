% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_calcul(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = texture_calcul(this, binsImage, LayerAngle, binsLayerAngle, ...
    nomCourbe, commentaire, numTexture, nomTexture, varargin)

% nbImages = length(this);
% for i=1:nbImages
%     this(i) = unitaire_texture_calcul(this(i), binsImage, LayerAngle, binsLayerAngle, ...
%                 nomCourbe, commentaire, numTexture, nomTexture, varargin{:});
% end
%
% % --------------------------------
% function this = unitaire_texture_calcul(this, binsImage, LayerAngle, binsLayerAngle, ...
%                 nomCourbe, commentaire, numTexture, nomTexture, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, LayerMask] = getPropertyValue(varargin, 'LayerMask', []);
[varargin, valMask]   = getPropertyValue(varargin, 'valMask', []);
[varargin, cliques]   = getPropertyValue(varargin, 'cliques', []); %#ok<ASGLU>

if isempty(cliques)
    cliques  = [1 0];
end

%% Quantification de l'image

rangeIn  = binsImage.CLim;
N        = binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(this.Image(suby,subx), 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

%% Gestion des masques

if ~isempty(LayerMask)
    Masque = get_Masque(LayerMask, this.x(subx), this.y(suby));
    subNonMasque = (Masque ~= valMask);
    I(subNonMasque) = NaN;
end

%% Lecture et Quantification de l'image conditionnelle angulaire

if isempty(LayerAngle)
    Indices = ones(length(suby), length(subx), 'single');
    binsLayerAngle = 1;
    DataType_LayerAngle = [];
else
    val = get_val_xy(LayerAngle, this.x(subx), this.y(suby));
    Indices = quantify_byBins(val, binsLayerAngle);
    DataType_LayerAngle = LayerAngle.DataType;
    %     figure(99999); imagesc(Indices); colorbar; colormap(jet(256)); axis xy; pixval
end

%% Apprentissage des matrices de cooccurrence

CmatModeleAngle = apprentissage_coocMat(I, Indices, length(binsLayerAngle), cliques, binsImage.NbNiveaux);

this.Texture(numTexture).nomTexture             = nomTexture;
this.Texture(numTexture).cliques                = cliques;
this.Texture(numTexture).binsImage              = binsImage;
this.Texture(numTexture).binsLayerAngle         = binsLayerAngle;
this.Texture(numTexture).DataType_LayerAngle  = DataType_LayerAngle;
this.Texture(numTexture).DataType_Image       = this.DataType;

this.Texture(numTexture).nomCourbe{valMask}               = nomCourbe;
this.Texture(numTexture).commentaire{valMask}             = commentaire;
this.Texture(numTexture).CmatModeleAngle{valMask}         = CmatModeleAngle;


function varargout = apprentissage_coocMat(I, Angle, nbBinsAngles, cliques, NbNiveaux)

nbCliques = size(cliques,1);
CmatModeleAngle = cell(nbCliques, nbBinsAngles);

hw = create_waitbar(Lang('Apprentissage en cours', 'Learning signatures'), 'N', nbBinsAngles);
for iAngle=1:nbBinsAngles
    my_waitbar(iAngle, nbBinsAngles, hw)
    
    masqueAngle = single((Angle == iAngle));
    [Ia, Ja] = find(masqueAngle);
    masqueAngle(~masqueAngle) = NaN;
    
    minIa = min(Ia);
    maxIa = max(Ia);
    
    minJa = min(Ja);
    maxJa = max(Ja);
    
    subIa = minIa:maxIa;
    subJa = minJa:maxJa;
    
    II = I(subIa, subJa) .* masqueAngle(subIa, subJa);
    
    if isempty(II)
        CmatModeleAngle(:,iAngle) = {[]};
    else
        %         if isempty(find(~isnan(II), 1))
        if ~any(~isnan(II))
            CmatModeleAngle(:,iAngle) = {[]};
        else
            for iClique=1:nbCliques
                CmatModeleAngle(iClique,iAngle) = learnSegmMat({II}, cliques(iClique,1), cliques(iClique,2), NbNiveaux);
            end
        end
    end
end
my_close(hw, 'MsgEnd')

if nargout == 0
    figure;
    k = 1;
    for iClique=1:nbCliques
        for iAngle=1:nbBinsAngles
            subplot(nbCliques, nbBinsAngles, k);
            imagesc(CmatModeleAngle{iClique,iAngle}); axis xy; axis off;
            k = k + 1;
        end
    end
else
    varargout{1} = CmatModeleAngle;
end
