% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_calcul_init(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [I, Indices] = texture_calcul_init(this, binsImage, LayerAngle, binsLayerAngle, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

%% Quantification de l'image

rangeIn  = binsImage.CLim;
N        = binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(this.Image(suby,subx), 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

%% Lecture et Quantification de l'image conditionnelle angulaire

if isempty(LayerAngle)
    Indices = ones(length(suby), length(subx), 'single');
else
    val = get_val_xy(LayerAngle, this.x(subx), this.y(suby));
    Indices = quantify_byBins(val, binsLayerAngle);
    %     SonarScope(Indices)
    %     figure(99999); imagesc(Indices); colorbar; colormap(jet(256)); axis xy; pixval
end
