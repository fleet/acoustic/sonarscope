% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_calcul(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = texture_calcul_new(this, I, Indices, binsImage, LayerAngle, binsLayerAngle, ...
    nomCourbe, commentaire, numTexture, nomTexture, varargin)

[varargin, valMask] = getPropertyValue(varargin, 'valMask', []);
[varargin, cliques] = getPropertyValue(varargin, 'cliques', []);
[varargin, Coul]    = getPropertyValue(varargin, 'Coul', []); %#ok<ASGLU>

if isempty(cliques)
    cliques  = [1 0];
end

%% Lecture et Quantification de l'image conditionnelle angulaire

if isempty(LayerAngle)
    binsLayerAngle = 1;
    DataType_LayerAngle = [];
else
    DataType_LayerAngle = LayerAngle.DataType;
end

%% Apprentissage des matrices de cooccurrence

CmatModeleAngle = apprentissage_coocMat(I, Indices, binsLayerAngle, cliques, binsImage.NbNiveaux);

this.Texture(numTexture).nomTexture             = nomTexture;
this.Texture(numTexture).cliques                = cliques;
this.Texture(numTexture).binsImage              = binsImage;
this.Texture(numTexture).binsLayerAngle         = binsLayerAngle;
this.Texture(numTexture).DataType_LayerAngle  = DataType_LayerAngle;
this.Texture(numTexture).DataType_Image       = this.DataType;

this.Texture(numTexture).nomCourbe{valMask}       = nomCourbe;
this.Texture(numTexture).commentaire{valMask}     = commentaire;
this.Texture(numTexture).CmatModeleAngle{valMask} = CmatModeleAngle;
this.Texture(numTexture).Coul(valMask,:) = Coul;


function varargout = apprentissage_coocMat(I, Angle, binsLayerAngle, cliques, NbNiveaux)

if isequal(binsLayerAngle,1)
    nbBinsAngles = 2;
    nbCliques = size(cliques,1);
    CmatModeleAngle = cell(nbCliques, nbBinsAngles-1);
else
    nbBinsAngles = length(binsLayerAngle);
    nbCliques = size(cliques,1);
    CmatModeleAngle = cell(nbCliques, nbBinsAngles-1);
end

hw = create_waitbar(Lang('Apprentissage en cours pour les différents angles', 'Learning signatures for the various angles'), 'N', nbBinsAngles);
for iAngle=1:nbBinsAngles-1
    my_waitbar(iAngle, nbBinsAngles, hw)
    
    masqueAngle = single((Angle == iAngle));
    [Ia, Ja] = find(masqueAngle);
    masqueAngle(~masqueAngle) = NaN;
    
    minIa = min(Ia);
    maxIa = max(Ia);
    
    minJa = min(Ja);
    maxJa = max(Ja);
    
    subIa = minIa:maxIa;
    subJa = minJa:maxJa;
    
    II = I(subIa, subJa) .* masqueAngle(subIa, subJa);
    
    if isempty(II)
        CmatModeleAngle(:,iAngle) = {[]};
    else
        %         if isempty(find(~isnan(II), 1))
        if ~any(~isnan(II))
            CmatModeleAngle(:,iAngle) = {[]};
        else
            for iClique=1:nbCliques
                CmatModeleAngle(iClique,iAngle) = learnSegmMat({II}, cliques(iClique,1), cliques(iClique,2), NbNiveaux);
            end
        end
    end
end
my_close(hw, 'MsgEnd')

if nargout == 0
    figure;
    k = 1;
    for iClique=1:nbCliques
        for iAngle=1:nbBinsAngles-1
            subplot(nbCliques, nbBinsAngles-1, k);
            imagesc(CmatModeleAngle{iClique,iAngle}); axis xy; axis off;
            k = k + 1;
        end
    end
else
    varargout{1} = CmatModeleAngle;
end
