% Segments an image using the Imen Master algorithm with adjacent windows
%
% Syntax
%   b = texture_segmenImen1(a, LayerAngle,...)
%
% Input Arguments
%   a          : Instance(s) of cl_image
%   LayerAngle : Instance of cl_image containing the angles if any ([] otherwise)
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image. 3 instances are created :
%       1 : Kullback distance
%       2 : Difference of the 2 smallest Kullback distances
%       3 : Segmentation
%
% Examples
%   a = TODO
%   imagesc(a);
%
%   b = texture_segmenImen1(a)
%   imagesc(b);
%   plot(b)
%
% See also cl_image/texture_segmenImen2 cl_image/texture_segmenImen4 Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = texture_segmenImen1(this, LayerAngle, varargin)

N = length(this);
that = repmat(cl_image,3,N);
hw = create_waitbar(waitbarMsg('texture_segmenImen1'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(:,k) = texture_segmenImen1_unit(this(k), LayerAngle, varargin{:});
end
my_close(hw)
that = that(:);


function that = texture_segmenImen1_unit(this, LayerAngle, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, sub]             = getPropertyValue(varargin, 'sub', []);
[varargin, iClique]         = getPropertyValue(varargin, 'iClique', []);
[varargin, Win]             = getPropertyValue(varargin, 'Win', []);
[varargin, SeuilConfusion]  = getPropertyValue(varargin, 'SeuilConfusion', 0);
[varargin, SeuilRejet]      = getPropertyValue(varargin, 'SeuilRejet', Inf);
[varargin, FilterCoocMat]   = getPropertyValue(varargin, 'FilterCoocMat', false); %#ok<ASGLU>

if isempty(iClique)
    iClique = 1;
end

if isempty(sub)
    sub  = selectionSegmSignature(this);
end

if isempty(Win)
    Win = [32 32];
else
    if length(Win) ~= 2
        Win = [Win(1) Win(1)];
    end
end

%% R�cuperation de la definition des textures

Texture = this.Texture(sub);
% nbFaciesPossibles = length(Texture.nomCourbe);

CmatModeleAngle = Texture.CmatModeleAngle;
nbFacies = length(CmatModeleAngle);
listeFaciesPossibles = [];
for iFacies=1:nbFacies
    CmatFacies = CmatModeleAngle{iFacies};
    if ~isempty(CmatFacies)
        listeFaciesPossibles(end+1) = iFacies; %#ok<AGROW>
    end
end
nbFaciesPossibles = length(listeFaciesPossibles);
if isempty(listeFaciesPossibles)
    disp('beurk')
end
CmatModeleAngle = CmatModeleAngle(listeFaciesPossibles);
Texture.CmatModeleAngle = CmatModeleAngle;
Texture.Coul = Texture.Coul(listeFaciesPossibles,:);

%% Quantification de l'image

rangeIn  = Texture.binsImage.CLim;
N        = Texture.binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(this.Image(suby, subx), 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

%% Lecture et Quantification de l'image conditionnelle angulaire

if isempty(LayerAngle)
    indAngle = ones(length(suby), length(subx), 'single');
else
    val = get_val_xy(LayerAngle, this.x(subx), this.y(suby));
    indAngle = quantify_byBins(val,  Texture.binsLayerAngle);
    %     figure(99999); imagesc(indAngle); colorbar; colormap(jet(256)); axis xy; pixval
end

%% Tuilage de l'image angulaire Simplifier si pas de layer %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

masqueImageValide = ones(size(indAngle), 'uint8');
[indAngleImagette, masqueImagetteValide, nbLI, nbCI]  = indAngImagettes(indAngle, Win, masqueImageValide);

%% Tuilage de l'image

[indAngleImagette, iC, iL] = getImagettes(indAngleImagette, masqueImagetteValide);

%% Segmentation

[segm, dist, dist12] = segmMatAngle(I, Win, indAngleImagette, Texture, nbLI, nbCI, iClique, masqueImagetteValide, ...
    iC, iL, SeuilConfusion, SeuilRejet, FilterCoocMat);

% CmatModeleAngle = Texture.CmatModeleAngle;
% nbFacies = length(CmatModeleAngle);

%% Remise � l'echelle de l'image

Segm = replace_Image(this, segm);

%% Mise � jour de coordonnees

% nbLNew = nbLI * Win(1);
% nbCNew = nbCI * Win(2);
% x =
% Il faudrait faire �a correctement car il y a un pb de multiple : � faire
% avec une image tr�s petite Ex 65x63 pour une fen�tre 32x32 genre
% subx = subx(1)+Win(2)/2 : Win(2): xxxxx;

pasx = Win(2);
deltax  = get(Segm, 'XStep');
deltaxp = get(Segm, 'XStep') * pasx;
N = size(segm,2) + 2;
x = (Segm.x(subx(1)) - deltax/2 + deltaxp/2):deltaxp:(Segm.x(subx(1)) + deltaxp/2 + N*deltaxp);
x = x(1:size(segm,2));

pasy = Win(1);
deltay  = get(Segm, 'YStep');
deltayp = get(Segm, 'YStep') * pasy;
N = size(segm,1) + 2;
if Segm.y(1) < Segm.y(end)
    y = (Segm.y(suby(1)) - deltay/2 + deltayp/2):deltayp:(Segm.y(suby(1)) + deltayp/2 + N*deltayp);
else
    y = (Segm.y(suby(end)) - deltay/2 + deltayp/2):deltayp:(Segm.y(suby(end)) + deltayp/2 + N*deltayp);
    y = fliplr(y);
end
y = y(1:size(segm,1));

Segm = majCoordonneesXY(Segm, x, y);
Segm.ValNaN = 0;

%% Calcul des statistiques

Segm = compute_stats(Segm);

%% Rehaussement de contraste

map = [0 0 0; Texture.Coul; 0.8 0.8 0.8; 0.9 0.9 0.9];
Segm.TagSynchroContrast = num2str(rand(1));
Segm.CLim               = [-0.5 nbFaciesPossibles+2+0.5];
Segm.ColormapCustom     = map;
Segm.ColormapIndex      = 1;
Segm.ImageType          = 3;
Segm.Video              = 1;
Segm.Unit               = 'num';

%% Compl�tion du titre

Segm.DataType = cl_image.indDataType('Segmentation');
Segm = update_Name(Segm);

%%

Dist = replace_Image(Segm, dist);
Dist.ValNaN             = NaN;
Dist = compute_stats(Dist);
Dist.TagSynchroContrast = num2str(rand(1));
Dist.CLim               = [0 Dist.StatValues.Max];
Dist.ColormapIndex      = 3;
Dist.ImageType          = 1;
Dist.Unit               = ' ';
Dist.Video              = 1;
Dist.DataType           = cl_image.indDataType('Kullback');
Dist = update_Name(Dist);

%%

Dist12 = replace_Image(Segm, dist12);
Dist12.ValNaN             = NaN;
Dist12 = compute_stats(Dist12);
Dist12.TagSynchroContrast = num2str(rand(1));
Dist12.CLim               = [0 Dist12.StatValues.Max];
Dist12.ColormapIndex      = 3;
Dist12.ImageType          = 1;
Dist12.Unit               = ' ';
Dist12.Video              = 1;
Dist12.DataType           = cl_image.indDataType('Kullback');
Dist12 = update_Name(Dist12);

that = [Segm, Dist, Dist12];
