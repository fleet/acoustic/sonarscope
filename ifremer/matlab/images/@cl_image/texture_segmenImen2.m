% Segments an image using the Imen Master algorithm with a sliding window
%
% Syntax
%   b = texture_segmenImen2(a, LayerAngle,...)
%
% Input Arguments
%   a          : Instance(s) of cl_image
%   LayerAngle : Instance of cl_image containing the angles if any ([] otherwise)
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image. 3 instances are created :
%       1 : Kullback distance
%       2 : Difference of the 2 smallest Kullback distances
%       3 : Segmentation
%
% Examples
%   a = TODO
%   imagesc(a);
%
%   b = texture_segmenImen2(a)
%   imagesc(b);
%   plot(b)
%
% See also cl_image/texture_segmenImen1 cl_image/texture_segmenImen4 Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = texture_segmenImen2(this, LayerAngle, varargin)

N = length(this);
that = repmat(cl_image,3,N);
hw = create_waitbar(waitbarMsg('texture_segmenImen2'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(:,k) = texture_segmenImen2_unit(this(k), LayerAngle, varargin{:});
    if N > 1
        that(:,k) = optimiseMemory(that(:,k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)
that = that(:);


function that = texture_segmenImen2_unit(this, LayerAngle, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, sub]            = getPropertyValue(varargin, 'sub',            []);
[varargin, iClique]        = getPropertyValue(varargin, 'iClique',        1);
[varargin, Win]            = getPropertyValue(varargin, 'Win',            [32 32]);
[varargin, SeuilConfusion] = getPropertyValue(varargin, 'SeuilConfusion', 0);
[varargin, SeuilRejet]     = getPropertyValue(varargin, 'SeuilRejet',     Inf);
[varargin, FilterCoocMat]  = getPropertyValue(varargin, 'FilterCoocMat',  0); %#ok<ASGLU>

if isempty(sub)
    sub = selectionSegmSignature(this);
end

if length(Win) ~= 2
    Win = [Win(1) Win(1)];
end

%% R�cuperation de la d�finition des textures

Texture = this.Texture(sub);

%% Quantification de l'image

rangeIn  = Texture.binsImage.CLim;
N        = Texture.binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(this.Image(suby, subx), 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

%% Lecture et Quantification de l'image conditionnelle angulaire

flagAngle = ~isempty(LayerAngle);
if flagAngle
    val = get_val_xy(LayerAngle, this.x(subx), this.y(suby));
    indAngle = quantify_byBins(val,  Texture.binsLayerAngle);
    %     figure(99999); imagesc(indAngle); colorbar; colormap(jet(256)); axis xy; pixval
end

CmatModeleAngle = Texture.CmatModeleAngle;
Clique          = Texture.cliques(iClique,:);
NbNiveaux       = Texture.binsImage.NbNiveaux;

dX = Clique(1);
dY = Clique(2);

nbFacies = length(CmatModeleAngle);
listeFaciesPossibles = [];
for iFacies=1:nbFacies
    CmatFacies = CmatModeleAngle{iFacies};
    if ~isempty(CmatFacies)
        listeFaciesPossibles(end+1) = iFacies; %#ok<AGROW>
    end
end
nbFaciesPossibles = length(listeFaciesPossibles);
if isempty(listeFaciesPossibles)
    disp('beurk')
end

for m=1:length(CmatModeleAngle) % rajout� par kevin : les mex-files n'aiment pas les structures contenant des �l�ments = []
    for i=1:length(CmatModeleAngle{1,1})
        if ~isempty(CmatModeleAngle{1,m})
            if isempty(CmatModeleAngle{1,m}{1,i})
                CmatModeleAngle{1,m}{1,i} = 1; % �l�ment d'une dimension
            end
        end
    end
end

CmatModeleAngle = CmatModeleAngle(listeFaciesPossibles);
Texture.Coul    = Texture.Coul(listeFaciesPossibles,:);

% sz = size(Imagettes);

[nbL, nbC] = size(I);
segm = zeros(nbL, nbC, 'uint8');
% dist = NaN(1, nbFacies, 'single');
Dist = zeros(nbL, nbC, 'single');
Dist12 = zeros(nbL, nbC, 'single');

W = floor(Win/2);
str1 = 'Etape 2 de la segmentation';
str2 = 'Segmentation processing step 2';
hw = create_waitbar(Lang(str1,str2), 'N', nbL);
for il=1:nbL
    my_waitbar(il, nbL, hw);
    subl = (il-W(1)+1):(il+W(1));
    subl(subl < 1) = [];
    subl(subl > nbL) = [];
    ImageL = I(subl, :);
    
    %     if flagAngle
    %         indAngleImagetteL = indAngle(il, :);
    %         [segmlocal Distlocal Dist12local] = coocMatNbNiveauxKullbackAnglesSortSegmDist_mexmc(uint8(ImageL), dX, dY, NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), indAngleImagetteL, FilterCoocMat, str2double(getenv('NUMBER_OF_PROCESSORS')));
    %     else
    %         [segmlocal Distlocal Dist12local] = coocMatNbNiveauxKullbackSortSegmDist_mexmc(uint8(ImageL), dX, dY, NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), FilterCoocMat, str2double(getenv('NUMBER_OF_PROCESSORS')));
    %     end
    %
    
    if flagAngle
        indAngleImagetteL = indAngle(il, :);
    else
        indAngleImagetteL = 1;
    end
    
    if NUMBER_OF_PROCESSORS == 0
        for ic=1:nbC
            subc = (ic-W(2)+1):(ic+W(2));
            subc(subc < 1) = [];
            subc(subc > nbC) = [];
            
            Imagette = ImageL(:, subc);
            
            if sum(isnan(Imagette(:))) == numel(Imagette)
                continue
            end
            
            if flagAngle
                %                 iAngle = indAngleImagetteL(W(1), ic);% A VERIFIER
                iAngle = indAngleImagetteL(1, ic);% Modifi� le 17/03/2010
                if isnan(iAngle)
                    continue
                end
            else
                iAngle = 1;
            end
            
            if sum(Imagette(:)) == 0
                continue
            end
            
            if FilterCoocMat
                Cmat = coocMatNbNiveauxFiltre(Imagette, dX, dY, NbNiveaux);
            else
                Cmat = coocMatNbNiveaux(Imagette, dX, dY, NbNiveaux);
            end
            
            dist = Inf(1,nbFaciesPossibles);
            for k=1:nbFaciesPossibles
                %                 iFacies = listeFaciesPossibles(k);
                %                 CmatFacies = CmatModeleAngle{iFacies}{iClique,iAngle};
                CmatFacies = CmatModeleAngle{k}{iClique,iAngle};
                
                %                 if ~isempty(CmatFacies)
                if numel(CmatFacies) > 1 % Remplac� le 21/02/2012
                    dist(k) = kullback(Cmat, CmatFacies, NUMBER_OF_PROCESSORS);
                end
            end
            
            [d, ordre] = sort(dist(:));
            separabilite = abs(d(1)-d(2));
            if d(1) == 0
                continue
            elseif d(1) > SeuilRejet
                segm(il,ic) = nbFaciesPossibles + 2;
            elseif separabilite < SeuilConfusion
                segm(il,ic) = nbFaciesPossibles + 1;
            else
                segm(il,ic) = listeFaciesPossibles(ordre(1));
            end
            Dist(  il,ic) = d(1);
            Dist12(il,ic) = separabilite;
        end
    else
        [segmlocal, Distlocal, Dist12local] = texture_segmenImen2_private_mexmc(uint8(ImageL), dX, dY, NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), indAngleImagetteL, FilterCoocMat, NUMBER_OF_PROCESSORS);
        segm(il,:)   = segmlocal(:);
        Dist(il,:)   = Distlocal(:);
        Dist12(il,:) = Dist12local(:);
    end
end
my_close(hw, 'MsgEnd')

%% Output images

% Image de segmentation
map = [0 0 0; Texture.Coul; 0.8 0.8 0.8; 0.9 0.9 0.9];
that(3) = inherit(this, segm, 'subx', subx, 'suby', suby, 'AppendName', 'ImenMasterSliding', 'ValNaN', 0, 'CLim', [-0.5 nbFaciesPossibles+2+0.5], ...
    'ColormapIndex', 1, 'ColormapCustom', map, 'ImageType', 1, 'Video', 1, 'Unit', 'num', 'DataType', cl_image.indDataType('Segmentation'));

% Distance de Kullback
that(1) = inherit(this, Dist, 'subx', subx, 'suby', suby, 'AppendName', 'Min', 'ValNaN', NaN, ...
    'ColormapIndex', 3, 'ImageType', 1, 'Unit', ' ', 'Video', 1, 'DataType', cl_image.indDataType('Kullback'));
that(1).CLim(1) = 0;

% Diff�rence entre les 2 distances de Kullback les plus petites
that(2) = inherit(this, Dist12, 'subx', subx, 'suby', suby, 'AppendName', 'Diff', 'ValNaN', NaN, ...
    'ColormapIndex', 3, 'ImageType', 1, 'Unit', ' ', 'Video', 1, 'DataType', cl_image.indDataType('Kullback'));
that(2).CLim(1) = 0;
