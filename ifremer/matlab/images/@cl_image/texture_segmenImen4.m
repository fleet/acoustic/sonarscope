% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_segmenImen3(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%   b = texture_segmenImen3(a)
%   imagesc(a)
%   plot(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = texture_segmenImen4(this, LayerAngle, Texture, varargin)

nbImages = length(this);
for i=1:nbImages
    [Segm, Dist, Dist12] = unitaire_texture_segmenImen3(this(i), LayerAngle, Texture, varargin{:});
    that(i,1) = Dist; %#ok<AGROW>
    that(i,2) = Dist12; %#ok<AGROW>
    that(i,3) = Segm; %#ok<AGROW>
end


function [Segm, Dist, Dist12] = unitaire_texture_segmenImen3(this, LayerAngle, Texture, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[subx, suby, varargin] = getSubxSuby(this, varargin);


[varargin, Win]            = getPropertyValue(varargin, 'Win', []);
[varargin, SeuilConfusion] = getPropertyValue(varargin, 'SeuilConfusion', 0);
[varargin, SeuilRejet]     = getPropertyValue(varargin, 'SeuilRejet', Inf);
[varargin, FilterCoocMat]  = getPropertyValue(varargin, 'FilterCoocMat', false); %#ok<ASGLU>

if isempty(Win)
    Win = [32 32];
else
    if length(Win) ~= 2
        Win = [Win(1) Win(1)];
    end
end

% -------------------------
% Quantification de l'image

rangeIn  = Texture.binsImage.CLim;
N        = Texture.binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(this.Image(suby, subx), 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

% -------------------------------------------------------------
% Lecture et Quantification de l'image conditionnelle angulaire

flagAngle = ~isempty(LayerAngle);
if flagAngle
    val = get_val_xy(LayerAngle, this.x(subx), this.y(suby));
    indAngle = quantify_byBins(val,  Texture.binsLayerAngle);
    %     figure(99999); imagesc(indAngle); colorbar; colormap(jet(256)); axis xy; pixval
end

w = Texture.w;
subCliques = find(w ~= 0);

NbNiveaux = Texture.binsImage.NbNiveaux;

% A REVOIR COMPLETEMENT
nbFacies = length(Texture.nomCourbe);

if Texture.UseCooc
    nbMesuresSimilariteCooc  = size(Texture.cliques,1);
    CmatModeleAngle = Texture.CmatModeleAngle;
    listeFaciesPossibles = [];
    for iFacies=1:nbFacies
        CmatFacies = CmatModeleAngle{iFacies};
        if ~isempty(CmatFacies)
            listeFaciesPossibles(end+1) = iFacies; %#ok<AGROW>
        end
    end
else
    nbMesuresSimilariteCooc = 0;
    listeFaciesPossibles = [];
    for iFacies=1:nbFacies
        CmatFacies = Texture.Gabor.Hist{iFacies};
        if ~isempty(CmatFacies)
            listeFaciesPossibles(end+1) = iFacies; %#ok<AGROW>
        end
    end
end

nbFaciesPossibles = length(listeFaciesPossibles);
if isempty(listeFaciesPossibles)
    disp('beurk')
end

nbValHist =0;
for iClique=1:length(subCliques)
    if subCliques(iClique) > nbMesuresSimilariteCooc
        kGabor = subCliques(iClique) - nbMesuresSimilariteCooc;
        %         p1 = Texture.Gabor.Param(kGabor, 1);
        %         p2 = Texture.Gabor.Param(kGabor, 2);
        %         p3 = Texture.Gabor.Param(kGabor, 3);
        nbValHist = length(Texture.Gabor.Hist{1}{1});
        %         g{kGabor} = gfcreatefilter2(p1, p2, p3, p3, WGabor); %#ok<AGROW>
        
        iFreq  = Texture.Gabor.Param(kGabor, 1);
        iTheta = Texture.Gabor.Param(kGabor, 2);
        iSigma = Texture.Gabor.Param(kGabor, 3);
        %         g{kGabor} = GaborWavelet(iTheta, iFreq, iSigma); %#ok<AGROW>
        g{kGabor} = GaborRIF(iFreq, iTheta, iSigma); %#ok<AGROW>
        
        %         I(I == 0) = NaN;
        %         I = fillNaN_mean(I, size(g{kGabor}));
        
        
        II = this.Image(suby,subx);
        if ~isnan(this.ValNaN)
            subNaN = (II == this.ValNaN);
            II = single(II);
            II(subNaN) = NaN;
            clear subNaN
        end
        % }
        
        %         I(I == 0) = NaN;
        II = fillNaN_mean(II, size(g));
        
        
        
        GG1 = filter2(g{kGabor}, II);
        GG1 = real(GG1) .^ 2 + imag(GG1) .^ 2;
        Min = Texture.Gabor.MinMaxGabor{1}(kGabor,1);
        Max = Texture.Gabor.MinMaxGabor{1}(kGabor,2);
        GG1 =(GG1 - Min) * (nbValHist / (Max - Min));
        J{kGabor} = GG1; %#ok<AGROW>
        figure(32760+iClique); imagesc(GG1); colorbar; title(sprintf('iFreq=%d  iTheta=%d  iSigma=%d  w=%f', ...
            iFreq, iTheta, iSigma, w(subCliques(iClique))))
    end
end

% sz = size(Imagettes);

[nbL, nbC] = size(I);
segm = zeros(nbL, nbC, 'uint8');
dist = NaN(1, nbFaciesPossibles, 'single');
Dist = zeros(nbL, nbC, 'single');
Dist12 = zeros(nbL, nbC, 'single');

% for iClique=1:length(subCliques)
%     if subCliques(iClique) > nbMesuresSimilariteCooc
%         kGabor = subCliques(iClique) - nbMesuresSimilariteCooc;
%         figure; imagesc(J{kGabor}); colorbar
%     end
% end

W = floor(Win/2);
hw = create_waitbar(Lang('Etape 2 de la segmentation', 'Segmentation processing step 2'), 'N', nbL);

if NUMBER_OF_PROCESSORS == 0
    for il=1:nbL
        my_waitbar(il, nbL, hw);
        
        sublCooc = (il-W(1)+1):(il+W(1));
        sublCooc(sublCooc < 1) = [];
        sublCooc(sublCooc > nbL) = [];
        ImageLCooc  = I(sublCooc, :);
        
        for iClique=1:length(subCliques)
            if subCliques(iClique) > nbMesuresSimilariteCooc
                kGabor = subCliques(iClique) - nbMesuresSimilariteCooc;
                ImageLGabor{kGabor} = J{kGabor}(sublCooc, :); %#ok<AGROW>
            end
        end
        
        %     sublGabor = (il-W(1)-WGaborS2+1):(il+W(1)+WGaborS2);
        %     sublGabor(sublGabor < 1) = [];
        %     sublGabor(sublGabor > nbL) = [];
        %     ImageLGabor = I(sublGabor, :);
        %
        %     sublG = ismember(sublGabor, sublCooc);
        
        if flagAngle
            indAngleImagetteL = indAngle(sublCooc, :);
        end
        
        for ic=1:nbC
            
            %         if (il == 100) && (ic == 100)
            %             il
            %         end
            %         if (il == 100) && (ic == 300)
            %             il
            %         end
            
            subcCooc = (ic-W(2)+1):(ic+W(2));
            subcCooc(subcCooc < 1) = [];
            subcCooc(subcCooc > nbC) = [];
            ImagetteCooc = ImageLCooc(:, subcCooc);
            
            %         subcGabor = (ic-W(2)-WGaborS2+1):(ic+W(2)+WGaborS2);
            %         subcGabor(subcGabor < 1) = [];
            %         subcGabor(subcGabor > nbC) = [];
            %         ImagetteGabor = ImageLGabor(:, subcGabor);
            %
            %         subcG = ismember(subcGabor, subcCooc);
            
            if sum(isnan(ImagetteCooc(:))) == numel(ImagetteCooc)
                continue
            end
            
            if flagAngle
                iAngle = indAngleImagetteL(W(1), ic);% A VERIFIER
                if isnan(iAngle)
                    continue
                end
            else
                iAngle = 1;
            end
            
            if sum(ImagetteCooc(:)) == 0
                continue
            end
            
            dist(:) = 0;
            for iClique=1:length(subCliques)
                if subCliques(iClique) <= nbMesuresSimilariteCooc
                    Clique = Texture.cliques(subCliques(iClique),:);
                    dX = Clique(1);
                    dY = Clique(2);
                    
                    if FilterCoocMat
                        Cmat = coocMatNbNiveauxFiltre(ImagetteCooc, dX, dY, NbNiveaux);
                    else
                        Cmat = coocMatNbNiveaux(ImagetteCooc, dX, dY, NbNiveaux);
                    end
                    
                    for k=1:nbFaciesPossibles
                        iFacies = listeFaciesPossibles(k);
                        CmatFacies = CmatModeleAngle{iFacies}{subCliques(iClique),iAngle};
                        if isempty(CmatFacies)
                            dist(iFacies) = Inf;
                        else
                            dist(iFacies) = dist(iFacies) + w(subCliques(iClique)) * kullback(Cmat, CmatFacies, NUMBER_OF_PROCESSORS);
                        end
                    end
                else
                    kGabor = subCliques(iClique) - nbMesuresSimilariteCooc;
                    
                    GG1 = ImageLGabor{kGabor};
                    GG1 = GG1(:, subcCooc);
                    
                    %                 figure(1386); subplot(2,1,1); imagesc(abs(GG1)); colorbar; title(sprintf('%d', iFiltre)); drawnow
                    
                    Cmat = Imen_histGabor(GG1, nbValHist, NUMBER_OF_PROCESSORS);
                    
                    for k=1:nbFaciesPossibles
                        iFacies = listeFaciesPossibles(k);
                        CmatFacies = Texture.Gabor.Hist{iFacies}{kGabor,iAngle};
                        KL = kullback(Cmat, CmatFacies, NUMBER_OF_PROCESSORS);
                        
                        if (il == 150) && (ic == 150) ...
                                || (il == 150) && (ic == 400) ...
                                || (il == 442) && (ic == 134) ...
                                || (il == 400) && (ic == 400)
                            kk = k+(iClique-1)*nbFaciesPossibles;
                            figure(54769+il+2*ic); subplot(length(subCliques), nbFaciesPossibles,kk); PlotUtils.createSScPlot(CmatFacies); grid on;
                            hold on; PlotUtils.createSScPlot(Cmat, 'k'); hold off;
                            title(sprintf('il=%d  ic=%d  iClique=%d  iFacies=%d  KL=%f', il, ic, iClique, iFacies, KL)); drawnow;
                        end
                        
                        dist(k) = dist(k) + w(subCliques(iClique)) * KL;
                    end
                end
                
            end
            %         [d, s] = min(dist(:));
            %         segm(il,ic) = s;
            %         Dist(il,ic) = d;
            
            [d, ordre] = sort(dist(:));
            separabilite = abs(d(1)-d(2));
            if d(1) == 0
                continue
            elseif d(1) > SeuilRejet
                segm(il,ic) = nbFacies + 2;
            elseif separabilite < SeuilConfusion
                segm(il,ic) = nbFacies + 1;
            else
                segm(il,ic) = listeFaciesPossibles(ordre(1));
            end
            Dist(il,ic) = d(1);
            Dist12(il,ic) = separabilite;
            %         figure(54769); xlabel(segm(il,ic))
        end
    end
else
    for il=1:nbL
        my_waitbar(il, nbL, hw);
        
        sublCooc = (il-W(1)+1):(il+W(1));
        sublCooc(sublCooc < 1) = [];
        sublCooc(sublCooc > nbL) = [];
        ImageLCooc  = I(sublCooc, :);
        
        nbfg = size(w);
        ImageLGabor = cell(1, nbfg(2) - nbMesuresSimilariteCooc);
        for iClique=1:length(subCliques)
            if subCliques(iClique) > nbMesuresSimilariteCooc
                kGabor = subCliques(iClique) - nbMesuresSimilariteCooc;
                ImageLGabor{kGabor} = J{kGabor}(sublCooc, :);
            end
        end
        
        if all(subCliques > nbMesuresSimilariteCooc)
            CmatModeleAngle = repmat({{}}, 1,nbFacies);
        end
        if all(subCliques <= nbMesuresSimilariteCooc)
            n = 48;
            ImageLGabor = repmat({1}, 1, n);
            X = zeros(1,100);
            X = repmat({X}, 48, 1);
            X = repmat({X}, 1, nbFacies);
            Texture.Gabor.Hist = X;
            Texture.Gabor.MinMaxGabor = repmat({zeros(n,2)}, 1, nbFacies);
            Texture.Gabor.Param = zeros(n,3);
        end
        
        % on complete les elements vide de ImageLGabor par des [1] (les
        % mex-files n'aiment pas les cellules vides)
        for i = 1 : nbfg(2) - nbMesuresSimilariteCooc % pour tous les gabors
            if(isempty(ImageLGabor{i}))
                ImageLGabor{i} = 1;
            end
        end
        
        if flagAngle
            indAngleImagetteL = indAngle(il, :);
        else
            indAngleImagetteL = 1;
        end
        
        %[segmlocal Distlocal Dist12local] = texture_segmenImen3_private_mexmc(uint8(ImageLCooc), w, uint8(subCliques), uint8(Texture.cliques), NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), indAngleImagetteL, FilterCoocMat, str2double(getenv('OMP_NUM_THREADS')));
        
        [segmlocal, Distlocal, Dist12local] = texture_segmenImen4_private_mexmc(uint8(ImageLCooc), w, uint8(subCliques), ...
            uint8(Texture.cliques), NbNiveaux, CmatModeleAngle, SeuilRejet, SeuilConfusion, W(2), nbMesuresSimilariteCooc, ...
            ImageLGabor, nbValHist, Texture.Gabor, 0, indAngleImagetteL, FilterCoocMat, NUMBER_OF_PROCESSORS);
        
        segm(il, :) = segmlocal(:);
        Dist(il, :) = Distlocal(:);
        Dist12(il, :) = Dist12local(:);
    end
end
my_close(hw, 'MsgEnd')

% -----------------------------
% Remise a l'echelle de l'image

Segm = replace_Image(this, segm);

% --------------------------
% Mise a jour de coordonnees

subx = linspace(subx(1), subx(end), size(segm, 2));
suby = linspace(suby(1), suby(end), size(segm, 1));
Segm = majCoordonnees(Segm, subx, suby);

% -----------------------
% Calcul des statistiques

Segm.ValNaN = 0;
Segm = compute_stats(Segm);

% -------------------------
% Rehaussement de contraste

Segm.TagSynchroContrast = num2str(rand(1));
% CLim = [-0.5 Segm.StatValues.Max+0.5];
CLim = [-0.5 nbFaciesPossibles+2+0.5];
Segm.CLim            = CLim;
% map = [1 1 1; Texture.Coul];
map = [0 0 0; Texture.Coul; 0.8 0.8 0.8; 0.9 0.9 0.9];
Segm.ColormapCustom = map;
Segm.ColormapIndex  = 1;
Segm.ImageType      = 3;
Segm.Video          = 1;
Segm.Unit           = 'num';

% -------------------
% Completion du titre

Segm.DataType = cl_image.indDataType('Segmentation');
Segm = update_Name(Segm);

% -----------------------------
% Remise a l'echelle de l'image

Dist = replace_Image(this, Dist);
Dist = majCoordonnees(Dist, subx, suby);
Dist.ValNaN             = NaN;
Dist = compute_stats(Dist);
Dist.TagSynchroContrast = num2str(rand(1));
Dist.CLim               = [0 Dist.StatValues.Max];
Dist.ColormapIndex      = 3;
Dist.ImageType          = 1;
Dist.Unit               = ' ';
Dist.Video              = 1;
Dist.DataType           = cl_image.indDataType('Kullback');
Dist = update_Name(Dist);


% -----------------------------

Dist12 = replace_Image(this, Dist12);
Dist12 = majCoordonnees(Dist12, subx, suby);
Dist12.ValNaN             = NaN;
Dist12 = compute_stats(Dist12);
Dist12.TagSynchroContrast = num2str(rand(1));
Dist12.CLim               = [0 Dist12.StatValues.Max];
Dist12.ColormapIndex      = 3;
Dist12.ImageType          = 1;
Dist12.Unit               = ' ';
Dist12.Video              = 1;
Dist12.DataType           = cl_image.indDataType('Kullback');
Dist12 = update_Name(Dist12, 'Append', 'Diff 2');



%{
% -----------------------------
% Remise a l'echelle de l'image

this = replace_Image(this, segm);

% --------------------------
% Mise a jour de coordonnees

% nbLNew = nbLI * Win(1);
% nbCNew = nbCI * Win(2);
% x =
% Il faudrait faire �a correctement car il y a un pb de multiple : a faire
% avec une image tr�s petite Ex 65x63 pour une fen�tre 32x32 genre
% subx = subx(1)+Win(2)/2 : Win(2): xxxxx;

subx = linspace(subx(1), subx(end), size(segm, 2));
suby = linspace(suby(1), suby(end), size(segm, 1));
this = majCoordonnees(this, subx, suby);

this.ValNaN = 0;

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));

% CLim = [0 this.StatValues.Max];
% this.CLim       = CLim;
% this.ImageType  = 1;    % Image indexee
% this.ColormapIndex = 1;
% map = jet(nbFacies);
% this.ColormapCustom = [0 0 0; map];

CLim = [-0.5 this.StatValues.Max+0.5];
this.CLim       = CLim;
map = [0 0 0; Texture.Coul];
this.ColormapCustom = map;
this.ColormapIndex = 1;
this.ImageType  = 1;

this.Unit = 'num';

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('Segmentation');
this = update_Name(this, 'Append', 'Segmentation');
%}
