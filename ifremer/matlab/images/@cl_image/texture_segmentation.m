% TODO : cette fonction n'est apparemment plus appel�e
%
% Calcul des matrices de cooccurrence d'une zone d'apprentissage
%
% Syntax
%   b = texture_segmentation(a, LayerAngle,...)
%
% Input Arguments
%   a : Instance de cl_image contenant la reflectivite
%   LayerAngle : Instance de cl_image contenant l'angle
%
% Name-Value Pair Arguments
%   subx    : subsampling in X
%   suby    : subsampling in Y
%
% Output Arguments
%   b : Instance de cl_image contenant a et les infos de BS
%
% Examples
%   b = texture_segmentation(a)
%   imagesc(a)
%   plot(b)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = texture_segmentation(this, LayerAngle, varargin)

% TODO : cette fonction n'est apparemment plus appel�e

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_texture_segmentation(this(i), LayerAngle, varargin{:});
end

function this = unitaire_texture_segmentation(this, LayerAngle, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, sub]     = getPropertyValue(varargin, 'sub', []);
[varargin, iClique] = getPropertyValue(varargin, 'iClique', []);
[varargin, Win]     = getPropertyValue(varargin, 'Win', []); %#ok<ASGLU>

if isempty(iClique)
    iClique = 1;
end

if isempty(sub)
    sub = selectionSegmSignature(this);
end

if isempty(Win)
    Win = [32 32];
else
    if length(Win) ~= 2
        Win = [Win(1) Win(1)];
    end
end

%% R�cuperation de la definition des textures

Texture = this.Texture(sub);

%% Quantification de l'image

rangeIn  = Texture.binsImage.CLim;
N        = Texture.binsImage.NbNiveaux;
rangeOut = [1 N];
I = quantify(this.Image(suby, subx), 'rangeIn', rangeIn, 'rangeOut', rangeOut, 'N', N);

%% Lecture et Quantification de l'image conditionnelle angulaire

if isempty(LayerAngle)
    indAngle = ones(length(suby), length(subx), 'single');
else
    val = get_val_xy(LayerAngle, this.x(subx), this.y(suby));
    indAngle = quantify_byBins(val,  Texture.binsLayerAngle);
    %     figure(99999); imagesc(indAngle); colorbar; colormap(jet(256)); axis xy; pixval
end

%% Tuilage de l'image angulaire Simplifier si pas de layer %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

segmentation = zeros(length(suby),length(subx));
masqueImageValide = ones(size(indAngle), 'uint8');

[indAngleImagette, masqueImagetteValide, nbLI, nbCI]  = indAngImagettes(indAngle, Win, masqueImageValide);

% ------------------
% Tuilage de l'image

[indAngleImagette, iC, iL] = getImagettes(indAngleImagette, masqueImagetteValide);

% ------------
% Segmentation

segm = segmMatAngle(I, Win, indAngleImagette, Texture, nbLI, nbCI, iClique, masqueImagetteValide, iC, iL);

CmatModeleAngle = Texture.CmatModeleAngle;
nbFacies = length(CmatModeleAngle);
listeFaciesPossibles = [];
for iFacies=1:nbFacies
    CmatFacies = CmatModeleAngle{iFacies};
    if ~isempty(CmatFacies)
        listeFaciesPossibles(end+1) = iFacies; %#ok
    end
end
nbFaciesPossibles = length(listeFaciesPossibles);
if isempty(listeFaciesPossibles)
    disp('beurk')
end

SE = strel('square',3);
Facies = zeros(size(segm));
for i=1:nbFaciesPossibles
    F = (segm == listeFaciesPossibles(i));
    Facies = Facies + imdilate(F,SE);
end
Frontieres = (Facies >= 2);

% -----------------------------
% Remise a l'echelle de l'image

nbLNew = nbLI * Win(1);
nbCNew = nbCI * Win(2);
segm = stretchmat(segm, [nbLNew,nbCNew], '*nearest');
segm = segm(1:length(suby), 1:length(subx));

Frontieres = stretchmat(Frontieres, [nbLNew,nbCNew], '*nearest');
Frontieres = Frontieres(1:length(suby), 1:length(subx));

subNonFrontieres = find(~Frontieres & (segmentation == 0));
%     segmentation(subNonFrontieres) = segm(subNonFrontieres);
segmentation(subNonFrontieres) = segm(subNonFrontieres);

% subDejaZero = find(masqueImageValide == 0);
% masqueImageValide(subNonFrontieres) = 0;
% masqueImageValide(subDejaZero) = 0;

subNonFrontieres = find(segmentation == 0);
segmentation(subNonFrontieres) = segm(subNonFrontieres);

this.Image  = segmentation;
this.ValNaN = 0;

% --------------------------
% Mise a jour de coordonnees

this = majCoordonnees(this, subx, suby);

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
this.CLim               = [0 this.StatValues.Max];
this.ImageType          = 1;    % Image indexee
this.ColormapIndex      = 1;
map = jet(nbFacies);
this.ColormapCustom     = [0 0 0; map];
this.Unit               = 'num';

% -------------------
% Completion du titre

this.DataType = cl_image.indDataType('Segmentation');
this = update_Name(this, 'Append', 'Segmentation');

