% TODO : registration d'image : utile pour num�riser des cartes SHOM (Herv�
% Bisquay)
%
% Syntax
%   a = tansform(a)
%
% Input Arguments 
%   a : instance de cl_image
%
% Examples
%   a = cl_image('Image', Lena)
%   a = transform(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = transform(this)
 

%% Nombre de points d'appairement

%{
[flag, nbPoints] = inputOneParametre('Image transform', 'Number of points', 'Value', 2, 'MinValue', 2, 'Format', '%d');
if ~flag
    return
end

%% Saisie des coordonn�es

for k=1:nbPoints
    str1 = ['Colonne ' num2str(k)];
    str2 = ['Column ' num2str(k)];
    str3 = ['Ligne ' num2str(k)];
    str4 = ['Row  ' num2str(k)];
    nomVar = {Lang(str1,str2); Lang(str3,str4)};
    
    if k == 1
        col = 1;
        lig = 1;
    elseif k  == nbPoints
        col = length(this.x);
        lig = length(this.y);
    end
        
    value = {num2str(col); num2str(lig)};
    [value, flag] = my_inputdlg(nomVar, value);
    if ~flag
        return
    end
end
%}

nomVar = {'X begin'; 'X end'; 'Y begin'; 'Y end'};
value = {num2str(this.x(1)); num2str(this.x(end)); num2str(this.y(1)); num2str(this.y(end))};
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end

%{
mytform = cp2tform(input_points, base_points, 'projective');
registered = imtransform(unregistered, mytform);
%}

% Format = get(0,'Format');
% set(0, 'Format', 'long')
format long
XDeb = str2num(value{1}); %#ok<ST2NM>
XFin = str2num(value{2}); %#ok<ST2NM>
YDeb = str2num(value{3}); %#ok<ST2NM>
YFin = str2num(value{4}); %#ok<ST2NM>
% set(0, 'Format', Format)

if isnan(XDeb) || isnan(XFin) || isnan(YDeb) || isnan(YFin)
    str1 = 'Une valeur n''est pas interpr�table, veuillez recommencer l''op�ration.';
    str2 = 'One value cannot be interpreted, please redo the the action.';
    my_warndlg(Lang(str1,str2), 1);
    return
end


x = linspace(XDeb, XFin, this.nbColumns);
y = linspace(YDeb, YFin, this.nbRows);

this.x = x;
this.y = y;

this = majCoordonnees(this);
this.YDir = 1;
