% Retourne les valeurs de l'image specifiees par les coordonnees k1, k2
%
% Syntax
%   type_voisins_xy(a, indImage, x, y)
%
% Input Arguments
%   a        : instances de cl_image
%   indImage : Index de l'image courante
%   x        : Abscisses
%   y        : Ordonnees
%
% Name-only Arguments
%   LayerSeul : Affichage du nom et des valeurs de l'image uniquement
%   NbVoisins : Nombre de voisins affiches
%
% Input Arguments
%   strOut : Tableau de cellule contenant les information du pixel
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function strOut = type_voisins_xy(this, indImage, valX, valY, varargin)

[varargin, LayerSeul] = getFlag(varargin, 'LayerSeul');

[varargin, N] = getPropertyValue(varargin, 'NbVoisins', 2); %#ok<ASGLU>

x = this(indImage).x;
y = this(indImage).y;

x = linspace(x(1), x(end), length(x));
y = linspace(y(1), y(end), length(y));

ix  = interp1(x, 1:length(x), valX, 'nearest', 'extrap');
iy  = interp1(y, 1:length(y), valY, 'nearest', 'extrap');
ix = double(ix); % Sinon min() pas content (importation de layer dans l'imagerie multifaisceau
iy = double(iy);

strOut = {};
if ~LayerSeul && ...
       ((this(indImage).GeometryType == cl_image.indGeometryType('PingSamples')) || ...
        (this(indImage).GeometryType == cl_image.indGeometryType('PingAcrossSample')) || ...
        (this(indImage).GeometryType == cl_image.indGeometryType('PingRange')) || ...
        (this(indImage).GeometryType == cl_image.indGeometryType('PingAcrossDist')) || ...
        (this(indImage).GeometryType == cl_image.indGeometryType('PingBeam')) || ...
        (this(indImage).GeometryType == cl_image.indGeometryType('PingSnippets') || ...
        (this(indImage).GeometryType == cl_image.indGeometryType('PingDepth'))))    % SonarD | SonarX
    
    strOut{end+1} = '--------------------------------------------------------';
    if isSonarSignal(this(indImage), 'Time')
        str = sprintf('SonarTime         : %s', t2str(this(indImage).Sonar.Time(iy)));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'PingNumber')
        str = sprintf('PingNumber       : %d', this(indImage).Sonar.PingNumber(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Immersion')
        str = sprintf('Immersion         : %f (m)', this(indImage).Sonar.Immersion(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Height')
        str = sprintf('Height            : %f (m)', this(indImage).Sonar.Height(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'CableOut')
        str = sprintf('CableOut          : %f (m)', this(indImage).Sonar.CableOut(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Speed')
        str = sprintf('Speed             : %f (m/s)', this(indImage).Sonar.Speed(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Heading')
        str = sprintf('Heading           : %f (deg)', this(indImage).Sonar.Heading(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Roll')
        str = sprintf('Roll              : %f (deg)', this(indImage).Sonar.Roll(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'RollUsedForEmission')
        str = sprintf('RollUsedForEmission              : %f (deg)', this(indImage).Sonar.RollUsedForEmission(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Pitch')
        str = sprintf('Pitch             : %f (deg)', this(indImage).Sonar.Pitch(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Yaw')
        str = sprintf('Yaw               : %f (deg)', this(indImage).Sonar.Yaw(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Heave')
        str = sprintf('Heave             : %f (m)', this(indImage).Sonar.Heave(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Tide')
        str = sprintf('Tide              : %f (m)', this(indImage).Sonar.Tide(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'PingCounter')
        str = sprintf('PingCounter       : %d', this(indImage).Sonar.PingCounter(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'SampleFrequency') && (length(this(indImage).Sonar.SampleFrequency) == this(indImage).nbRows)
        str = sprintf('SampleFrequency   : %f (Hz)', this(indImage).Sonar.SampleFrequency(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'SonarFrequency')
        str = sprintf('SonarFrequency    : %f', this(indImage).Sonar.SonarFrequency(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'SurfaceSoundSpeed')
        str = sprintf('SurfaceSoundSpeed : %f (m/s)', this(indImage).Sonar.SurfaceSoundSpeed(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'FishLatitude')
        str = sprintf('FishLatitude      : %f (deg) : %s', this(indImage).Sonar.FishLatitude(iy), lat2str(this(indImage).Sonar.FishLatitude(iy)));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'FishLongitude')
        str = sprintf('FishLongitude     : %f (deg) : %s', this(indImage).Sonar.FishLongitude(iy), lon2str(this(indImage).Sonar.FishLongitude(iy)));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'ShipLatitude')
        str = sprintf('ShipLatitude      : %f (deg) : %s', this(indImage).Sonar.ShipLatitude(iy), lat2str(this(indImage).Sonar.ShipLatitude(iy)));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'ShipLongitude')
        str = sprintf('ShipLongitude     : %f (deg) : %s', this(indImage).Sonar.ShipLongitude(iy), lon2str(this(indImage).Sonar.ShipLongitude(iy)));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'PortMode_1')
        str = sprintf('PortMode_1        : %f ', this(indImage).Sonar.PortMode_1(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'StarMode_1')
        str = sprintf('StarMode_1        : %f ', this(indImage).Sonar.StarMode_1(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'PortMode_2')
        str = sprintf('PortMode_2        : %f ', this(indImage).Sonar.PortMode_2(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'StarMode_2')
        str = sprintf('StarMode_2        : %f ', this(indImage).Sonar.StarMode_2(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'Interlacing')
        str = sprintf('Interlacing        : %f ', this(indImage).Sonar.Interlacing(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'PresenceWC')
        str = sprintf('PresenceWC        : %f ', this(indImage).Sonar.PresenceWC(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'PresenceFM')
        str = sprintf('PresenceFM        : %f ', this(indImage).Sonar.PresenceFM(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'DistPings')
        str = sprintf('DistPings         : %f (m)', this(indImage).Sonar.DistPings(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'NbSwaths')
        str = sprintf('NbSwaths        : %f ', this(indImage).Sonar.NbSwaths(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'ResolAcrossSample')
        str = sprintf('ResolAcrossSample        : %f (m)', this(indImage).Sonar.ResolAcrossSample(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'TxAlongAngle')
        str = sprintf('TxAlongAngle        : %f (deg)', this(indImage).Sonar.TxAlongAngle(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'BSN')
        str = sprintf('BSN               : %f (dB)', this(indImage).Sonar.BSN(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'BSO')
        str = sprintf('BSO               : %f (dB)', this(indImage).Sonar.BSO(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'TVGN')
        str = sprintf('Rn                  : %f (samp)', this(indImage).Sonar.TVGN(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'TVGCrossOver')
        str = sprintf('TVGCrossOver      : %f (deg)', this(indImage).Sonar.TVGCrossOver(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'TxBeamWidth')
        str = sprintf('TxBeamWidth       : %f (deg)', this(indImage).Sonar.TxBeamWidth(iy));
        strOut{end+1} = str;
    end
    
    if isSonarSignal(this(indImage), 'FishLatitude') && isSonarSignal(this(indImage), 'FishLongitude')
        if isSonarSignal(this(indImage), 'Time') && isSonarSignal(this(indImage), 'FishLatitude') && isSonarSignal(this(indImage), 'FishLongitude')
            position_onNavigationFigure(this(indImage).Sonar.FishLatitude(iy), this(indImage).Sonar.FishLongitude(iy), this(indImage).Sonar.Time(iy))
        end
    end
end

if ~LayerSeul
    for k1=1:length(this(indImage).SignalsVert)
        [~, I] = create_MatrixFromSignalVert(this, indImage, ...
            this(indImage).SignalsVert(k1), 'subx', ix, 'suby', iy, 'Mute', 1, 'FirstOfList', 1);
        
        %  Display sumary of signal
        signal = this(indImage).SignalsVert(k1);
        if isnan(I) && signal.isLinkedSignal()
            str = sprintf('%s : Needs layer "%s" to get this value', signal.tag, signal.link);
        else
            if isa(signal.ySample, 'IndexYSample')
                if isempty(signal.ySample(1).strIndexList)
                    str = sprintf('%s : %d', signal.tag, I);
                else
                    str = sprintf('%s : %d (%s)', signal.tag, I, signal.ySample(1).strIndexList{I});
                end
            elseif isa(signal.ySample, 'BooleanYSample')
                strBoolean = {'Off'; 'On'};
                str = sprintf('%s : %s (%s)', signal.tag, num2str(I), strBoolean{I+1});
            elseif isempty(signal.ySample(1).unit)
                str = sprintf('%s : %s', signal.tag, num2str(I));
            else
                str = sprintf('%s : %s (%s)', signal.tag, num2str(I), signal.ySample(1).unit);
            end
        end
        strOut{end+1} = str; %#ok<AGROW>
    end
end

if ~LayerSeul && (this(indImage).GeometryType == cl_image.indGeometryType('LatLong'))    % Geographique
    position_onNavigationFigure(valY, valX)
end

ideb = iy-N;
ideb = max(ideb, 1);
ifin = iy+N;
ifin = min(double(ifin), double(this(indImage).nbRows));
LignesStats = ideb:ifin;

ideb = iy-min(N, 2);
ideb = max(ideb, 1);
ifin = iy+min(N, 2);
ifin = min(double(ifin), double(this(indImage).nbRows));
Lignes = ideb:ifin;

ideb = ix-N;
ideb = max(ideb, 1);
ifin = ix+N;
ifin = min(double(ifin), double(this(indImage).nbColumns));
ColonnesStats = ideb:ifin;

ideb = ix-min(N, 2);
ideb = max(ideb, 1);
ifin = ix+min(N, 2);
ifin = min(double(ifin), double(this(indImage).nbColumns));
Colonnes = ideb:ifin;

Abscisses = x(Colonnes);
Ordonnees = y(Lignes);

switch this(indImage).SpectralStatus
    case 1
%         val = NaN(length(Lignes), length(Colonnes));
%         Lignes(Lignes     < 1) = [];
%         Colonnes(Colonnes < 1) = [];
%         Lignes(Lignes     > this(indImage).nbRows) = [];
%         Colonnes(Colonnes > this(indImage).nbColumns) = [];
        try
            val = double(this(indImage).Image(Lignes, Colonnes));
        catch
            val = NaN(length(Lignes), length(Colonnes));
        end
        try
            valStats = double(this(indImage).Image(LignesStats, ColonnesStats));
        catch
            valStats = NaN(length(LignesStats), length(ColonnesStats));
        end
    case 2
        val = 20 * log10(abs(this(indImage).Image(Lignes, Colonnes)));
        valStats = 20 * log10(abs(this(indImage).Image(LignesStats, ColonnesStats)));
    case 3
        % A faire
end

ImageName = this(indImage).Name;
if isempty(ImageName)
    ImageName = 'Image';
end

strOut{end+1} = '--------------------------------------------------------';

if ~LayerSeul
    strOut{end+1} = ['Raws   : ' num2str(Lignes)];
    strOut{end+1} = [this(indImage).YLabel(:)' ' : ' num2str(Ordonnees)];
    strOut{end+1} = ['Columns : ' num2str(Colonnes)];
    strOut{end+1} = [this(indImage).XLabel(:)' ' : ' num2str(Abscisses)];
    strOut{end+1} = '--------------------------------------------------------';
    
    flagPositionGeographique = 1;
    if ~isempty(this(indImage).Carto) || (this(indImage).GeometryType == cl_image.indGeometryType('LatLong'))
        if get(this(indImage).Carto, 'Projection.Type') == 1
            lat = valY;
            lon = valX;
        elseif this(indImage).GeometryType == cl_image.indGeometryType('GeoYX')
            [lat, lon] = xy2latlon(this(indImage).Carto, valX, valY);
        elseif this(indImage).GeometryType == cl_image.indGeometryType('LatLong')
            lat = valY;
            lon = valX;
        elseif testSignature(this(indImage), 'GeometryType', 'PingXxxx', 'noMessage')
            if isempty(this(indImage).Sonar.FishLatitude) || isnan(this(indImage).Sonar.FishLatitude(iy))
                flagPositionGeographique = 0;
            else
                lat = this(indImage).Sonar.FishLatitude(iy);
                lon = this(indImage).Sonar.FishLongitude(iy);
            end
        else
            flagPositionGeographique = 0;
        end
        
        % ---------------------------------------
        % Affichage des coordonnées géographiques
        
        if flagPositionGeographique
            str = sprintf('Clicked point in decimal degrees                                  : Lat="%s"         Lon="%s"', ...
                num2strPrecis(lat), num2strPrecis(lon));
            strOut{end+1} = str;
            
            strLat = lat2str6(lat);
            strLon = lon2str6(lon);
            str = sprintf('Clicked point in degrees and decimal minutes               : Lat="%s"                Lon="%s"', strLat, strLon);
            strOut{end+1} = str;
            
            strLat = lat2strdms(lat);
            strLon = lon2strdms(lon);
            str = sprintf('Clicked point in degrees, minutes and decimal seconds : Lat="%s"               Lon="%s"', strLat, strLon);
            strOut{end+1} = str;
            
            strOut{end+1} = '--------------------------------------------------------';
        end
    end
end

strOut{end+1} = ['ImageName : ' ImageName ' - (' this(indImage).Unit ')'];
strOut{end+1} = num2str(val);

if isnan(this(indImage).ValNaN)
    subNonNan = ~isnan(valStats);
else
    subNonNan = (valStats ~= this(indImage).ValNaN);
end
sumNaN = sum(subNonNan(:));
if sumNaN ~= 0
    m = mean(valStats(subNonNan(:)));
    s = std(valStats(subNonNan(:)));
    strOut{end+1} = ['Stats on ' num2str(sumNaN) ' points = Mean=' num2str(m) '  Std=', num2str(s)];
end


if this(indImage).DataType == cl_image.indDataType('RxTime')
    strOut{end+1} = '    -> Interpretation';
    for k1=1:size(val,1)
        str = '        ';
        for k2=1:size(val,2)
            t = datetime(val(k1,k2), 'ConvertFrom', 'datenum');
            str = [str char(t) '    ']; %#ok<AGROW>
        end
        strOut{end+1} = str; %#ok<AGROW>
    end
    strOut{end+1} = ' ';
elseif this(indImage).DataType == cl_image.indDataType('Latitude')
    strOut{end+1} = '    -> Interpretation';
    for k1=1:size(val,1)
        str = '        ';
        for k2=1:size(val,2)
            str = [str lat2str(val(k1,k2)) '  ']; %#ok<AGROW>
        end
        strOut{end+1} = str; %#ok<AGROW>
    end
    strOut{end+1} = ' ';
elseif this(indImage).DataType == cl_image.indDataType('Longitude')
    strOut{end+1} = '    -> Interpretation';
    for k1=1:size(val,1)
        str = '        ';
        for k2=1:size(val,2)
            str = [str lon2str(val(k1,k2)) '  ']; %#ok<AGROW>
        end
        strOut{end+1} = str; %#ok<AGROW>
    end
    strOut{end+1} = ' ';
end


if this(indImage).SpectralStatus == 2
    frequence = sqrt(valX^2 + valY^2);
    if frequence == 0
        lambda = Inf;
    else
        lambda = 1/frequence;
    end
    direction = atan2(valY, valX) * 180 / pi;
    Azimuth = 90 - direction;
    str = sprintf('Frequence=%f   Longueur d''onde=%f   Direction=%f   Azimut=%f ', ...
        frequence, lambda, direction, Azimuth);
    strOut{end+1} = str;
    
    msg{1} = sprintf('Image : %s', this(indImage).Name);
    msg{2} = sprintf(' ');
    msg{3} = sprintf('x = \t\t%f (%s)', valX, this(indImage).XUnit);
    msg{4} = sprintf('y = \t\t%f (%s)', valY, this(indImage).YUnit);
    msg{5} = sprintf('Frequence = \t%f', frequence);
    msg{6} = sprintf('Lambda = \t%f', lambda);
    msg{7} = sprintf('Direction = \t%f (deg / repere orthonorme xy)', direction);
    msg{8} = sprintf('Azimut = \t\t%f (deg / Nord geographique)', Azimuth);
    msg = cell2str(msg);
    my_warndlg(msg, 0);
end

if this(indImage).DataType == cl_image.indDataType('Correlation')
    lambda = sqrt(valX^2 + valY^2);
    direction = atan2(valY, valX) * 180 / pi;
    Azimuth = 90 - direction;
    str = sprintf('Longueur d''onde=%f   Direction=%f   Azimut=%f ', lambda, direction, Azimuth);
    strOut{end+1} = str;
    
    msg{1} = sprintf('Image : %s', this(indImage).Name);
    msg{2} = sprintf(' ');
    msg{3} = sprintf('x =  \t\t%f (%s)', valX, this(indImage).XUnit);
    msg{4} = sprintf('y =  \t\t%f (%s)', valY, this(indImage).YUnit);
    msg{6} = sprintf('Lambda = \t%f', lambda);
    msg{7} = sprintf('Direction = \t%f (deg / repere orthonorme xy)', direction);
    msg{8} = sprintf('Azimut = \t\t%f (deg / Nord geographique)', Azimuth);
    my_warndlg(msg, 0);
end
% strOut{end+1} = '--------------------------------------------------------';
