% Conversion de l'image en uint8
%
% Syntax
%  b = uint8(a)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   b : Instance(s) of cl_image
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a);
%
%   b = uint8(a);
%   imagesc(b);
%
% See also cl_image/doble Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = uint8(this, varargin)
that = process_function_type1(this, @uint8, 'ValNaN', 0, varargin{:});
