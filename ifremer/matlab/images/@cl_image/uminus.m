% Operateur d'opposition de signe
%
% Syntax
%   c = -a
%
% Input Arguments
%   a : Une instance de cl_image ou un scalaire
%
% Output Arguments
%   c : Une instance de cl_image
%
% Examples
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Name', label);
%   imagesc(a);
%
%   c = -a
%   imagesc(c);
%
% See also cl_image/minus Authors
% Authors : JMA
% VERSION  : $Id: uminus.m,v 1.1 2005/09/14 08:23:13 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = uminus(this)

nbImages = length(this);
for i=1:nbImages
    this(i) = unitaire_uminus(this(i));
end

% --------------------------------
function this = unitaire_uminus(this)

ImageName = this.Name;

% -------------------------
% For�age du transtypage des data � inverser.

switch class(this.Image(1))
    case 'uint8'
       this.Image = int16(this.Image(:,:,:));
    case 'uint16'
       this.Image = int32(this.Image(:,:,:));
    case 'uint32'
        my_warndlg('Type de donn�e non pris en compte', 1);
end

% --------------------------------
% Appel de la fonction de filtrage

this = replace_Image(this, -this.Image(:,:,:));

% -----------------------
% Calcul des statistiques

this = compute_stats(this);

% -------------------------
% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

% -------------------
% Completion du titre

this.Name = ImageName;
this = update_Name(this, 'Append', 'Opposite');
