% Extract the ImageName (without DataType and GeometryType signatures)
%
% Syntax
%   ImageName = cl_image.uncode_ImageName(ImageName)
%
% Input Arguments
%   a : One cl_image instance
%
% Name-Value Pair Arguments
%   ImageName : Name of the image
%
% Output Arguments
%   ImageName : Name of the image
%
% Examples
%     DataType = 3; GeometryType = 15;
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', GeometryType);
%     a = update_Name(a);
%     ImageName = a.Name
%   [Name, indGeometryType, indexDataType] = cl_image.uncode_ImageName(ImageName)
%
% See also cl_image/extract_ImageName cl_image/update_Name Authors
% ----------------------------------------------------------------------------

function [ImageName, indGeometryType, indexDataType] = uncode_ImageName(ImageName)

indGeometryType = 1;
indexDataType   = 1;

%% On supprime l'heure de cr�ation du layer

ImageName = cl_image.extract_ImageNameVersion(ImageName);
ImageName = cl_image.extract_ImageNameTime(ImageName);

%% GeometryType

%GLU : le 12/09/2011 : mots = strsplit(ImageName, {'_'; ' '; '('; ')'});
pattern = '\s?|_?|\(?|)?';
mots = regexp(ImageName, pattern, 'split');
nbMots = length(mots);
flag = ones(1,nbMots);
for k=nbMots:-1:1
    indGeometryType = find(strcmp(mots{k}, cl_image.strGeometryType), 1);
    if ~isempty(indGeometryType)
        flag(k) = 0;
        break
    end
end
if isempty(indGeometryType)
    indGeometryType = 1;
end

if any(flag == 0)
    mots = mots(find(flag)); %#ok<FNDSB>
    nbMots = length(mots);
    if nbMots ~= 0
        ImageName = mots{1};
        for k=2:nbMots
            ImageName = [ImageName '_' mots{k}]; %#ok<AGROW>
        end
    end
end

%% DataType

strDataType = cl_image.strDataType;

%GLU : le 12/09/2011 : mots = strsplit(ImageName, {'_'; ' '; '('; ')'});
pattern = '\s?|_?|\(?|)?';
mots = regexp(ImageName, pattern, 'split');
nbMots = length(mots);
flag = ones(1,nbMots);

for k=nbMots:-1:1
    indexDataType = find(strcmp(mots{k}, strDataType), 1);
    if ~isempty(indexDataType)
        flag(k) = 0;
        break
    end
end
if isempty(indexDataType)
    indexDataType = 1;
end

if any(flag == 0)
    mots = mots(find(flag)); %#ok<FNDSB>
    nbMots = length(mots);
    if nbMots ~= 0
        ImageName = mots{1};
        for k=2:nbMots
            ImageName = [ImageName '_' mots{k}]; %#ok<AGROW>
        end
    end
end
