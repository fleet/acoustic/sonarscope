% Compl�te une image A par une image B
%
% Syntax
%  b = abs(a, ...)
%
% Input Arguments
%   a : Une instance de cl_image
%   b : Image "bouche trous"
%
% Name-Value Pair Arguments
%   subx : subsampling in X
%   suby : subsampling in Y
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = union(this, b, varargin)

nbImages = length(this);
for i=1:nbImages
    if length(b) == 1
        [flag, c] = union_unitaire(this(i), b, varargin{:});
    else
        [flag, c] = union_unitaire(this(i), b(i), varargin{:});
    end
    if ~flag
        return
    end
    this(i) = c;
end


function [flag, this] = union_unitaire(this, b, varargin)

[varargin, XLim] = getPropertyValue(varargin, 'XLim', []);
[varargin, YLim] = getPropertyValue(varargin, 'YLim', []); %#ok<ASGLU>

%% Intersection des images

% if isempty(XLim)
XLim(1) = min(min(this.x), min(b.x));
XLim(2) = max(max(this.x), max(b.x));
% end
% if isempty(YLim)
YLim(1) = min(min(this.y), min(b.y));
YLim(2) = max(max(this.y), max(b.y));
% end

x = XLim(1):this.XStep:XLim(2);
y = YLim(1):this.YStep:YLim(2);
x = centrage_magnetique(x);
y = centrage_magnetique(y);

if this.ImageType == 1
    [this, flag] = Intensity2RGB(this);
    if ~flag
        return
    end
end
if b.ImageType == 1
    [b, flag] = Intensity2RGB(b);
    if ~flag
        return
    end
end

c = this;
if this.ImageType == 2 % RGB
    nbSlides = 3;
else
    nbSlides = 1;
end
c.Image = NaN([length(y) length(x) nbSlides], class(this.Image(1)));
subxThisInC = find((x >= min(this.x)) & (x <= max(this.x)));
subxThis = find((this.x >= XLim(1)) & (this.x <= XLim(2)));

% subyThisInC    = find((y >= min(this.y)) & (y <= max(this.y)));
subyThisInC    = (y >= min(this.y)) & (y <= max(this.y));

% subyThis = find((this.y >= YLim(1)) & (this.y <= YLim(2)));
subyThis = (this.y >= YLim(1)) & (this.y <= YLim(2));

% Ce qui devait arriver est arriv� : 1 ou 2 pixels de d�calage ! ceci est
% une rustine
if length(subxThisInC) ~= length(subxThis)
    nMin = min(length(subxThisInC),length(subxThis));
    subxThisInC = subxThisInC(1:nMin);
    subxThis    = subxThis(1:nMin);
end
% Fin de la rustine

c.Image(subyThisInC,subxThisInC,:) = this.Image(subyThis,subxThis,:);

subxc = find((x >= min(b.x)) & (x <= max(b.x)));
subyc = find((y >= min(b.y)) & (y <= max(b.y)));

subNonNaN = ~isnan(b);
for iCan=1:b.nbSlides
    B = b.Image(:,:,iCan);
    C = c.Image(subyc,subxc,iCan);
    C(subNonNaN) = B(subNonNaN);
    c.Image(subyc,subxc,iCan) = C;
end

% subxa = find((x >= min(this.x)) & (x <= max(this.x)));
% subya = find((y >= min(this.y)) & (y <= max(this.y)));
% c.Image(suby,subx,:) = b.Image(:,:,:);

%% Calcul

% nbRows = b.nbRows;
% if nbSlides == 1
%     for iLig=1:nbRows
%         x = c.Image(iLig, :);
%         y = b.Image(subyb(iLig), subxb);
%         if isnan(this.ValNaN)
%             sub = ~isnan(x);
%         else
%             sub = (x ~= this.ValNaN);
%         end
%         x(sub) = y(sub);
%         c(iLig,:) = x;
%     end
% else
%     for iLig=1:nbRows
%         x3 = b.Image(subyb(iLig), subxb, :);
%         if isnan(b.ValNaN)
%             sub = isnan(sum(x3,3));
%         else
%             sub = sum(x3 == b.ValNaN,3) == nbSlides;
%         end
%
%         for iCan = 1:nbSlides
%             x = x3(1, :, iCan);
%             y = b.Image(subyb(iLig),subxb(sub), iCan);
%             x(subxa(sub)) = y;
%             c(subya(iLig),:,iCan) = x;
%         end
%     end
% end

this = replace_Image(this, c.Image);

%% Mise � jour de coordonn�es

this.x = x;
if sign(this.y(2)-this.y(1)) == -1
    y = flipud(y(:));
end
this.y = y;
this = majCoordonnees(this, 1:length(x), 1:length(y));

%% Calcul des statistiques

this = compute_stats(this);

%% Rehaussement de contraste

this.TagSynchroContrast = num2str(rand(1));
CLim = [this.StatValues.Min this.StatValues.Max];
this.CLim = CLim;

%% Compl�tion du titre

this = update_Name(this, 'Append', 'Union');

flag = true;
