% PAS FINALISE CAR C'EST SANS DOUTE UNE TRES MAUVAISE IDEE
% car on veur r�unir des courbes qui peuvent avoir �t� calcul�es de fa�on
% tr�s diff�rentes : a jeter apr�s UNH
% Calcul de statistiques conditionnelles
%
% Syntax
%   this = unionCourbesStats(this, ...)
%
% Input Arguments
%   this : Instance de cl_image
%
% Name-Value Pair Arguments
%    :
%
% Examples
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = unionCourbesStats(this, varargin)

[varargin, Tag]          = getPropertyValue(varargin, 'Tag',          []);
[varargin, TypeWeight]   = getPropertyValue(varargin, 'TypeWeight',   1);
[varargin, TypeCurve]    = getPropertyValue(varargin, 'TypeCurve',    1);
[varargin, MeanCompType] = getPropertyValue(varargin, 'MeanCompType', 1);
[varargin, sub]          = getPropertyValue(varargin, 'sub',          1:length(this.CourbesStatistiques));

if isempty(sub)
    message_NoCurve(this)
    return
end

nbCurves = length(sub);
if ~isempty(Tag)
    for k=1:nbCurves
        if strcmp(this.CourbesStatistiques(sub(k)).bilan{1}(1).Tag, Tag)
            subsub(k) = logical(1); %#ok
        else
            subsub(k) = logical(0); %#ok
        end
    end
    sub = sub(subsub);
end

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

if last
    sub = sub(end);
end

if nbCurves < 2
    message_NoCurve(this, 'MoreThanOneCurve')
    return
end

bilan1 = this.CourbesStatistiques(sub(1)).bilan{1};
flag = 1;
for k=2:nbCurves
    bilan2 = this.CourbesStatistiques(sub(k)).bilan{1};
    
    if ~strcmp(bilan1(1).DataTypeValue, bilan2(1).DataTypeValue)
        flag = 0;
        break
    end
    if ~any(strcmp(bilan1(1).DataTypeConditions, bilan2(1).DataTypeConditions))
        flag = 0;
        break
    end
    if ~any(strcmp(bilan1(1).Support, bilan2(1).Support))
        flag = 0;
        break
    end
    if bilan1(1).GeometryType ~= bilan2(1).GeometryType
        flag = 0;
        break
    end
    if ~strcmp(bilan1(1).TypeCourbeStat, bilan2(1).TypeCourbeStat)
        flag = 0;
        break
    end
end
if ~flag
    str1 = 'Les courbes ne sont pas de m�me nature, on ne peut pas les moyenner.';
    str2 = 'The curves were made differently, one cannot compute a mean value.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

n1 = length(this.CourbesStatistiques);
this.CourbesStatistiques = CourbesStatsUnion(this.CourbesStatistiques, 'sub', sub, ...
    'TypeWeight', TypeWeight,  'TypeCurve', TypeCurve, 'MeanCompType', MeanCompType);

n2 = length(this.CourbesStatistiques);
for k=(n1+1):n2
    plotCourbesStats(this, 'sub', k, 'Stats', 0);
end

