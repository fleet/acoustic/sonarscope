function [DataTypes, strDataTypes] = uniqueDataTypes(this)

N = length(this);
DataTypes = zeros(N, 1);
for k=1:N
    DataTypes(k) = this(k).DataType;
end
DataTypes = unique(DataTypes);
strDataTypes = cl_image.strDataType(DataTypes);
