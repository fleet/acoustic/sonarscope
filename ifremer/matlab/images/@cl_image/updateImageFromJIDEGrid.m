function [flag, this] = updateImageFromJIDEGrid(this, ParentGrid, varargin)

flag = 0; %#ok<NASGU>

for k=1:length(ParentGrid)
    child(k) = ParentGrid.get(k-1); %#ok<AGROW> % Les indices de Java commencent � 0.
    
    childTree = get(child(k), 'TreeTableModel');
    propTree = childTree.getProperties;
    for ii=1:propTree.size
        propName    = char(propTree.get(ii-1).getName);
        fieldType   = [];
        switch propName
            case 'strDataType'
                pppp        = char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indDataType(pppp));
                fieldType   = 'DataType';
            case 'strGeometryType'
                pppp        = char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indGeometryType(pppp));
                fieldType   = 'GeometryType';
            case 'strImageType'
                pppp        = char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indImageType(pppp));
                fieldType   = 'ImageType';
            case 'strSpectralStatus'
                pppp        = char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indSpectralStatus(pppp));
                fieldType   = 'SpectralStatus';
            case 'strXDir'
                pppp        = char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indXDir(pppp));
                fieldType   = 'XDir';
            case 'strYDir'
                pppp    	= char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indYDir(pppp));
                fieldType   = 'YDir';
           case 'strVideo'
                 pppp        = char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indVideo(pppp));
                fieldType   = 'Video';
           case 'strColormapIndex'
                pppp        = char(propTree.get(ii-1).getValue);
                valType     = int32(cl_image.indColormapIndex(pppp));
                fieldType   = 'ColormapIndex';
        end
        if ~isempty(fieldType)
            this = set(this, fieldType, valType);
        end
    end
    
end


flag = 1;

