% Complete the image name with DataType and GeometryType strings 
%
% Syntax
%   a = update_Name(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   ImageName : Name of the image
%   Append    : Extension
%
% Output Arguments
%   a : Instance(s) of cl_image
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15);
%     a.Name
%     imagesc(a)
%   a = update_Name(a);
%     a.Name
%     ImageName = extract_ImageName(a)
%     a.Name = 'Lena';
%     imagesc(a)
%   a = update_Name(a, 'Name', 'Lena�c');
%     a.Name
%     imagesc(a)
%   a = update_Name(a, 'Append', 'Hello');
%     a.Name
%     imagesc(a)
%
% See also cl_image/extract_ImageName cl_image/code_ImageName Authors
% ----------------------------------------------------------------------------

function this = update_Name(this, varargin) 

for k=1:numel(this)
    ImageName = code_ImageName(this(k), varargin{:});
    this(k).Name = ImageName;
end
