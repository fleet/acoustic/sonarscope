% TODO : voir en quoi cette fonction est diff�rente de SonarDescriptionAccordingToModes

function this = update_SonarDescription_Ping(this, kPing, varargin)

persistent persistent_ModeSounder persistent_Model persistent_Sonar persistent_nbSwaths persistent_presenceFM

[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         0);
[varargin, nbSectors]    = getPropertyValue(varargin, 'nbSectors',    []);
[varargin, ScanningInfo] = getPropertyValue(varargin, 'ScanningInfo', []); %#ok<ASGLU>

if isempty(kPing) % RAZ des variables persistentes
    persistent_ModeSounder = [];
    persistent_Model       = [];
    persistent_Sonar       = [];
    persistent_nbSwaths    = [];
    persistent_presenceFM  = [];
    return
end

Sonar = this.Sonar.Desciption;
Model = get(Sonar, 'Sonar.Name');

if isempty(this.Sonar.PortMode_1) % rencontr� avec Geoswath : TODO : traiter ce pb
    return
end

% TODO : C'est pas bon pour les EM2040 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ModeSounder = this.Sonar.PortMode_1(kPing,1);

TypeMode_1 = get(Sonar, 'TypeMode_1');
if strcmp(TypeMode_1, 'SounderMode')
    if ~isempty(persistent_Model) && strcmp(persistent_Model, Model) && ~isempty(persistent_ModeSounder) ...
            && (persistent_ModeSounder == ModeSounder) && ~isempty(persistent_Sonar)
        return
    end
    nbSwaths   = [];
    presenceFM = [];
elseif strcmp(TypeMode_1, 'Swath')
    if isempty(this.Sonar.NbSwaths)
        nbSwaths = 1;
    else
        nbSwaths = this.Sonar.NbSwaths(kPing);
    end
    
    if isempty(this.Sonar.PresenceFM)
        presenceFM = 0;
    else
        presenceFM = this.Sonar.PresenceFM(kPing);
    end
    
    if ~isempty(persistent_Model) && strcmp(persistent_Model, Model) ...
            && ~isempty(persistent_ModeSounder) && (persistent_ModeSounder == ModeSounder) ...
            && ~isempty(persistent_nbSwaths) && (persistent_nbSwaths == nbSwaths) ...
            && ~isempty(persistent_presenceFM) && (persistent_presenceFM == presenceFM) ...
            && ~isempty(persistent_Sonar)
        return
    end
else
    nbSwaths   = [];
    presenceFM = [];
end

persistent_Model       = Model;
persistent_ModeSounder = ModeSounder;
persistent_nbSwaths    = nbSwaths;
persistent_presenceFM  = presenceFM;

if isempty(TypeMode_1) % Cas du ME70
    persistent_Sonar = Sonar;
    this.Sonar.Desciption = Sonar;
    return
end

if strcmp(TypeMode_1, 'SounderMode')
    Sonar = set(Sonar, 'Sonar.Mode_1', ModeSounder); % V�rifi� sur EM300
else
    if strcmp(TypeMode_1, 'Swath')
        [flag, SonarMode_1, SonarMode_2, SonarMode_3] = getSounderParamsForcl_sounderXML(Model, 'nbSwaths',  nbSwaths, ...
            'presenceFM', presenceFM, 'SounderMode', ModeSounder, 'nbSectors', nbSectors, 'ScanningInfo', ScanningInfo, ...
            'Mute', Mute);
        if ~flag
            return
        end
        Sonar = set(Sonar, 'Sonar.Mode_1', SonarMode_1, 'Sonar.Mode_2', SonarMode_2, 'Sonar.Mode_3', SonarMode_3);
    else
        str1 = 'Message pour JMA : il n''a pas �t� possible de g�rer le mode du sondeur dans "update_SonarDescription_Ping"';
        str2 = 'Message for JMA : it was not possible to set the mode in "update_SonarDescription_Ping", please report to JMA';
        my_warndlg(Lang(str1,str2), 1);
    end
end

persistent_Sonar = Sonar;
this.Sonar.Desciption = Sonar;
