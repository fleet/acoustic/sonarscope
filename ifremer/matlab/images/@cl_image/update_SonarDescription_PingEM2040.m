% TODO : voir en quoi cette fonction est différente de
% SonarDescriptionAccordingToModes pour l'EM2040

function this = update_SonarDescription_PingEM2040(this, Init, SonarMode_1, SonarMode_2, SonarMode_3)

persistent persistent_Model persistent_SonarMode_1 persistent_SonarMode_2 persistent_SonarMode_3

Sonar = this.Sonar.Desciption;
Model = get(Sonar, 'Sonar.Name');

if ~Init
    if ~isempty(persistent_Model) && strcmp(persistent_Model, Model) ...
            && ~isempty(persistent_SonarMode_1) && (persistent_SonarMode_1 == SonarMode_1) ...
            && ~isempty(persistent_SonarMode_2) && (persistent_SonarMode_2 == SonarMode_2) ...
            && ~isempty(persistent_SonarMode_3) && (persistent_SonarMode_3 == SonarMode_3)
        return
    end
end

persistent_Model = Model;
persistent_SonarMode_1 = SonarMode_1;
persistent_SonarMode_2 = SonarMode_2;
persistent_SonarMode_3 = SonarMode_3;

Sonar = set(Sonar, 'Sonar.Mode_1', SonarMode_1, 'Sonar.Mode_2', SonarMode_2, 'Sonar.Mode_3', SonarMode_3);

this.Sonar.Desciption = Sonar;
