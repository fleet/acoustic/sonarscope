% Computes watershed, Mounts, crests and valleys of a DTM
%
% Syntax
%   b = watershed(a, ...)
%
% Input Arguments
%   a : Instance(s) of cl_image
%
% Name-Value Pair Arguments
%   subx         : Sub-sampling in abscissa
%   suby         : Sub-sampling in ordinates
%   Seuil        : Threshold of histeresis
%   Connectivity : 4 ou 8 (Default : 8)
%
% Output Arguments
%   c : Instance(s) of cl_image. 5 instances are created :
%       1 : Mounts
%       2 : Watersheds
%       3 : Crest and valley lines
%       4 : Crest lines
%       5 : Valley lines
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     imagesc(sunShading(a));
%   b = watershed(a, 'Seuil', 50);
%     imagesc(b);
%
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%     imagesc(a)
%   b = watershed(a, 'Seuil', 10);
%     imagesc(b);
%
% See also watershed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = watershed(this, varargin)

N = length(this);
that = repmat(cl_image,5,N);
hw = create_waitbar(waitbarMsg('watershed'), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    that(:,k) = watershed_unit(this(k), varargin{:});
    if N > 1
        that(:,k) = optimiseMemory(that(:,k), 'SizeMax', 0);
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)
that = that(:);


function that = watershed_unit(this, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin);

[varargin, Connectivity] = getPropertyValue(varargin, 'Connectivity', 8);
[varargin, Seuil]        = getPropertyValue(varargin, 'Seuil', []); %#ok<ASGLU>

%% Algorithm, Initialization

ImageName = this.Name;
this = WinFillNaN(this, 'window', [5 5], 'subx', subx, 'suby', suby, 'NoStats');
subNaN = findNaN(this);

%% Algorithm Watershed image

I = this.Image(:,:);
I = singleUnlessDouble(I, this.ValNaN);

I(subNaN) = 0;
if ~isempty(Seuil)
    I = imhmin(I, Seuil, Connectivity);
end
I = watershed(I, Connectivity);
I = single(I);
I(subNaN) = NaN;
sub1 = find(I == 0);
I(sub1) = NaN;
I = shakeColors(I);

that(1) = inherit(this, I, 'Name', ImageName, 'AppendName', 'Watershed', 'ValNaN', NaN, 'ColormapIndex', 3, 'Unit', ' ', 'DataType', cl_image.indDataType('Mask'));

%% Algorithm Mounts image

I = -singleUnlessDouble(this.Image(:,:), this.ValNaN);
ValMax = max(I(:), [], 'omitnan');
I(subNaN) = ValMax;
if ~isempty(Seuil)
    I = imhmin(I, Seuil, Connectivity);
end
I = watershed(I, Connectivity);
I = single(I);
I(subNaN) = NaN;
sub2 = find(I == 0);
I(sub2) = NaN;
I = shakeColors(I);
that(2) = inherit(this, I, 'Name', ImageName, 'AppendName', 'Mounts', 'ValNaN', NaN, 'ColormapIndex', 3, 'Unit', ' ', 'DataType', cl_image.indDataType('Mask'));

%% Algorithm Crests and Valleys image

I = zeros(size(I), 'uint8');
I(sub1) = 1;
I(sub2) = 2;

that(3) = inherit(this, I, 'Name', ImageName, 'AppendName', 'CrestsValleys', 'ValNaN', 0, 'ColormapIndex', 1, ...
    'Unit', ' ', 'DataType', cl_image.indDataType('Mask'), ...
    'CLim', [0 2], 'ColormapCustom', [1 1 1; 1 0 0; 0 0 1]);

%% Algorithm Valleys image

I = zeros(size(I), 'uint8');
I(sub1) = 1;
clear sub1

that(4) = inherit(this, I, 'Name', ImageName, 'AppendName', 'Crests', 'ValNaN', 0, 'ColormapIndex', 1, ...
    'Unit', ' ', 'DataType', cl_image.indDataType('Mask'), ...
    'CLim', [0 2], 'ColormapCustom', [1 1 1; 1 0 0; 0 0 1]);

%% Algorithm Crests image

I = zeros(size(I), 'uint8');
I(sub2) = 1;
clear sub2

that(5) = inherit(this, I, 'Name', ImageName, 'AppendName', 'Valleys', 'ValNaN', 0, 'ColormapIndex', 1, ...
    'Unit', ' ', 'DataType', cl_image.indDataType('Mask'), ...
    'CLim', [0 2], 'ColormapCustom', [1 1 1; 1 0 0; 0 0 1]);
