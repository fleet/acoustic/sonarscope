% Informs if the image in in RAM or in a memory mapping file
%
% Syntax
%   [str, MemMapFileName] = where(a)
%
% Input Arguments
%   a : One cl_image instance
%
% Output Arguments
%   str            : "On RAM' or 'Virtual memory on FILENAME'
%   MemMapFileName : Name of the memory mapping file
%
% Examples
%     a = cl_image('Image', Lena, 'Name', 'Foo', 'DataType', 3, 'GeometryType', 15, 'ColormapIndex', 2);
%     [str, MemMapFileName] = where(a)
%   a = Ram2Virtualmemory(a);
%     [str, MemMapFileName] = where(a)
%     a = Virtualmemory2Ram(a);
%     [str, MemMapFileName] = where(a)
%
% See also cl_image/Ram2Virtualmemory cl_image/Virtualmemory2Ram cl_image/optimiseMemory cl_memmapfile Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [str, MemMapFileName] = where(this)

if isa(this.Image, 'cl_memmapfile')
    MemMapFileName = get(this.Image, 'FileName');
    str = ['Virtual memory on ' MemMapFileName];

    % ----------------------------------------------------------
    % On verifie si c'est bien un fichier avec l'extension
    % ".memmapfile" car il ne faudrait pas d�truire des fichiers
    % ErMapper

    [~, ~, Ext] = fileparts(MemMapFileName);
    if ~strcmp(Ext, '.memmapfile')
        MemMapFileName = [];
    end
else
    str = 'On RAM';
    MemMapFileName = [];
end