% Logical operator "xor"
%
% Syntax
%   c = xor(a,b)
%
% Input Arguments
%   a : Instance(s) of cl_image or a scalar
%   b : A scalar or an instance of cl_image
%
% Output Arguments
%   c : Instance(s) of cl_image
%
% Examples
%   I = repmat(-100:100, 256, 1);
%   a = cl_image('Image', I, 'ColormapIndex', 3);
%   imagesc(a)
%   b = a < 10;
%   c = a > 20;
%   imagesc(b)
%   imagesc(c)
%   d = xor(b,c);
%   imagesc(d)
%
% See also cl_image/or cl_image/and Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = xor(a, b)
[that, flag] = process_function_logicalOperator(a, b, @xor); %#ok<ASGLU>
