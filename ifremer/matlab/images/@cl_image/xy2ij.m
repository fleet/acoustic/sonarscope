% Transformation des coordonnees (x, y) en (i,j)
%
% Syntax
%   [ix, iy] = xy2ij(a, x, y)
%
% Input Arguments
%   a : instance de cl_image
%   x : Abscisses
%   y : Ordonnees
%
% Output Arguments
%   ix  : Numero de colonne correspondant
%   iy  : Numero de ligne correspondant
%
% See also cl_image cl_image/xy2ij Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [ix, iy] = xy2ij(this, x, y, varargin)

[varargin, method] = getPropertyValue(varargin, 'Method', 'nearest'); %#ok<ASGLU>

X = linspace(this.x(1), this.x(end), length(this.x));
Y = linspace(this.y(1), this.y(end), length(this.y));

ix  = interp1(X, 1:length(X), x, method, 'extrap');
iy  = interp1(Y, 1:length(Y), y, method, 'extrap');
