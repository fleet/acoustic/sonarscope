% Transformation des coordonnees (x, y) en (i,j) avec extrapolation en dehors de l'image
%
% Syntax
%   [ix, iy] = xy2ij_extrap_outside(a, x, y)
%
% Input Arguments
%   a : instance de cl_image
%   x : Abscisses
%   y : Ordonnees
%
% Output Arguments
%   ix  : Numero de colonne correspondant
%   iy  : Numero de ligne correspondant
%
% See also cl_image cl_image/xy2ij_extrap_outside Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [ix, iy] = xy2ij_extrap_outside(this, x, y)

ix = interp1(this.x, 1:length(this.x), x, 'linear', 'extrap');
iy = interp1(this.y, 1:length(this.y), y, 'linear', 'extrap');
ix = round(ix);
iy = round(iy);
