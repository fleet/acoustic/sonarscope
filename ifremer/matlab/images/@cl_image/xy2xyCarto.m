% nomFic = getNomFicDatabase('EM300_ExMos.imo');
% a = cl_car_ima(nomFic)
% c = view(a);
% b = get_Image(c,1)
% x = get(b, 'x')
% y = get(b, 'y')
% [xcarto, ycarto] = xy2xyCarto(b, x(1), y(1))
% [xcarto, ycarto] = xy2xyCarto(b, x, y)

function [xcarto, ycarto] = xy2xyCarto(this, ximage, yimage)

xcarto = Xmin_metric + ximage * cos(orientation);
ycarto = Ymin_metric + yimage * cos(orientation) + (ximage(this.nbColumns-1)-ximage)*sin(orientation);
    
