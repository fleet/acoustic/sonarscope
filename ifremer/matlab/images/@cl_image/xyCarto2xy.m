function [ximage ,yimage ] = xyCarto2xy(this, xcarto, ycarto)

ximage = (xcarto - Xmin_metric) * cos(orientation);
yimage = (ycarto - Ymin_metric - (ximage(this.nbColumns-1)-ximage)*sin(orientation))* cos(orientation);

if (ximage<0) || (yimage<0)
    my_warndlg('Le point d�sign� n appartient pas a l image',1);
    return
end
