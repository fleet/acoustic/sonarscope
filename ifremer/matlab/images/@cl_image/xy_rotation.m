% Cr�ation de coordonn�es dans un rep�re orient�
%
% Syntax
%  b = xy_rotation(a, Azimuth, ...)
%
% Input Arguments
%   a       : Une instance de cl_image
%   Azimuth : Angle de rotation
%
% Name-Value Pair Arguments
%   subx : Sous-echantillonnage en xy_rotationcisses
%   suby : subsampling in Y
%
% Output Arguments
%   b : Une instance de cl_image
%
% Examples
%   a = cl_image('Image', Lena, 'ColormapIndex', 2);
%   imagesc(a);
%
%   b = xy_rotation(a, 30);
%   imagesc(b);
%
% See also cl_image/minus Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function that = xy_rotation(this, Azimuth, varargin)

nbImages = length(this);
for k=1:nbImages
    XY = unitaire_xy_rotation(this(k), Azimuth, varargin{:});
    that(k,1) = XY(1); %#ok<AGROW>
    that(k,2) = XY(2); %#ok<AGROW>
end
that = that(:);


function that = unitaire_xy_rotation(this, Azimuth, varargin)

[subx, suby, varargin] = getSubxSuby(this, varargin); %#ok<ASGLU>

Angle = (90-Azimuth) * (pi/180);
c = cos(Angle);
s = sin(Angle);
x = this.x(subx);
y = this.y(suby);

if this.GeometryType == cl_image.indGeometryType('LatLong')
    x = x * cosd(mean(y));
end

try
    X = zeros(length(suby), length(subx));
    Y = zeros(length(suby), length(subx));
catch %#ok<CTCH>
    X = cl_memmapfile('Value', 0, 'Size', [length(suby) length(subx)]);
    Y = cl_memmapfile('Value', 0, 'Size', [length(suby) length(subx)]);
end

N = length(suby);
hw = create_waitbar('Rotated axis creation', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    X(k,:) =  c * x + s * y(k);
    Y(k,:) = -s * x + c * y(k);
end
my_close(hw, 'MsgEnd')

% TODO / remplacer tout ce qui suit par  that = inherit(this, Image, ...)

X = replace_Image(this, X);
Y = replace_Image(this, Y);

% --------------------------
% Mise a jour de coordonnees

X = majCoordonnees(X, subx, suby);
Y = majCoordonnees(Y, subx, suby);

% -----------------------
% Calcul des statistiques

X = compute_stats(X);
Y = compute_stats(Y);

% -------------------------
% Rehaussement de contraste

X.TagSynchroContrast = num2str(rand(1));
X.CLim               = [X.StatValues.Min X.StatValues.Max];
X.ColormapIndex      = 3;
X.DataType           = 1;
X.ImageType          = 1;

Y.TagSynchroContrast = num2str(rand(1));
Y.CLim               = [Y.StatValues.Min Y.StatValues.Max];
Y.ColormapIndex      = 3;
Y.DataType           = 1;
Y.ImageType          = 1;

% -------------------
% Completion du titre

that(1) = update_Name(X, 'Append', ['X_Azimuth' num2str(Azimuth)]);
that(2) = update_Name(Y, 'Append', ['Y_Azimuth' num2str(Azimuth)]);
