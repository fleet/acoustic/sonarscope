function [xcarto, ycarto] = xyimage2xycarto(this, ximage, yimage)

xcarto = Xmin_metric + ximage * cos(orientation);
ycarto = Ymin_metric + yimage * cos(orientation) + (ximage(this.nbColumns) - ximage) * sin(orientation);
