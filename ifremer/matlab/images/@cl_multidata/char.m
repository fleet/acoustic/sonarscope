% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_multidata
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   nomFic      = getNomFicDatabase('lena.tif');
%   I           = imread(nomFic);
%   a           = cl_image('DataLayer', Lena);
%   timeData    = repmat(now, 256,256);
%   idx         = cl_image('DataIndex', timeData);
%   m           = cl_multidata('DataLayer', a, 'DataIndex', idx)
%   str         = char(a)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char(this, varargin)

str = [];
[m, n] = size(this);

if (m*n) > 1
    for i=1:m
        for j=1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j);
            str1 = char(this(i, j), varargin{:});
            str{end+1} = str1;
        end
    end
    str = cell2str(str);
else
    if (m*n) == 0
        str = '';
        return
    end

    %% Instance unique

    str = char_instance(this, varargin{:});
    str = [repmat(' ', size(str,1), 2) str];
end
