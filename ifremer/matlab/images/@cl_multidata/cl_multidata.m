% Constructeur de cl_multidata
%
% Syntax
%   a = cl_multidata(...)
% 
% Name-Value Pair Arguments
%  
% Output Arguments 
%   a : instance de cl_multidata
%
% Examples
%   nomFic      = getNomFicDatabase('lena.tif');
%   I           = imread(nomFic);
%   a           = cl_image('Image', Lena, 'Name', 'Sacree Lena');
%   load mandrill;
%   a(end+1)    = cl_image('Image', X, 'Name', 'Rien A voir avec Lena');
%   timeData    = repmat(now, 256,256);
%   idx         = cl_image('Image', timeData);
%   m           = cl_multidata('DataLayers', a, 'DataIndex', idx)
%   set(m, 'Name', 'Test de MultiData')
%
% See also cl_image/imagesc Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = cl_multidata(varargin)

%% D�finition de la structure

this.Name         = 'Image';
this.DimDataLayer = [];
this.DataLayer    = []; % cl_image.empty;
this.DimDataIndex = [];
this.DataIndex    = []; % cl_image;


%% Cr�ation de l'instance

this = class(this, 'cl_multidata');

%% On regarde si l'objet a �t� cr�� uniquement pour atteindre une m�thode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
