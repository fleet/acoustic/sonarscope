% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : instance de cl_multidata
%
% Examples
%   nomFic      = getNomFicDatabase('lena.tif');
%   I           = imread(nomFic);
%   a           = cl_image('DataLayer', Lena);
%   timeData    = repmat(now, 256,256);
%   idx         = cl_image('DataIndex', timeData);
%   m           = cl_multidata('DataLayer', a, 'DataIndex', idx)
%   m
%
% See also cl_multidata Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function display(this, varargin)

% --------------------------------------------
% Lecture et affichage des infos de l'instance

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this, varargin{:}));
