% ATTENTION : on va se paser sur la dimension de this pour savoir ce qui
% est des dates et ce qui est des layers
% this(:,k) : Tous les layers pour l'instant k
% this(k,:) : tous les temps pour le layer k

function flag = exportGeotiffSonarScope3DViewerMultiV3(this, nomFicXML, varargin) % subx et suby sont � inhiber

[~, nomLayer] = fileparts(nomFicXML);
nomLayer = {nomLayer}; % TODO

[varargin, subx]     = getPropertyValue(varargin, 'subx', []);
[varargin, suby]     = getPropertyValue(varargin, 'suby', []);
[varargin, sizeTiff] = getPropertyValue(varargin, 'sizeTiff', 2500); %#ok<ASGLU>

[repExport, nomFicSeul] = fileparts(nomFicXML);

%% Organisation pour �criture des images

% Cr�ation de la strcuture pour XML et �criture des images.
LayerSet = MiseEnFormeHeaderLayer(this, nomFicSeul);
if isempty(LayerSet)
    flag = 0;
    return
end

LayerSet = MiseEnFormeDataLayer(this.DataLayer, LayerSet, nomFicSeul, repExport, subx, suby, sizeTiff);
if isempty(LayerSet)
    flag = 0;
    return
end

LayerSet = MiseEnFormeIndexLayer(this.DataIndex, LayerSet, nomFicSeul, repExport, subx, suby, sizeTiff);
if isempty(LayerSet)
    flag = 0;
    return
end

%% Cr�ation du fichier XML du layer

nomFicLayerSet   = [nomLayer{1} '.xml'];
nomFicLayerSet   = fullfile(repExport, nomFicLayerSet);
% DPref.ItemName = 'item';
DPref.StructItem = false;
DPref.CellItem   = false;
xml_write(nomFicLayerSet, LayerSet, 'LayerSet', DPref);

%% The End

str1 = sprintf('L''export GLOBE de "%s" est termin�.', nomLayer{1});
str2 = sprintf('The export to GLOBE of "%s" is over.', nomLayer{1});
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportGLOBETermine', 'TimeDelay', 10);

flag = 1;



function [flag, nomFicXMLTile, minBloc, maxBloc, XLimTile, YLimTile, sizePixels, Carto] = exportGeotiff3DV(this, ...
    repExport, SubLayerDirectory, nomLayer, subx, suby, sizeTiff, kTime, Carto)

flag          = 1;
nomFicXMLTile = [];
XLimTile      = [];
YLimTile      = [];
sizePixels    = [];
minBloc       = 0;
maxBloc       = 0;

if isempty(subx)
    subx = 1:this(1).nbColumns;
end
if isempty(suby)
    suby = 1:this(1).nbRows;
end

delta = 1;

nx          = length(subx);
ny          = length(suby);
TailleBlocX = min(nx,sizeTiff);
TailleBlocY = min(ny,sizeTiff);
minVal      =  Inf;
maxVal      = -Inf;
% I = get(this(1), 'Image');
k = 0;
N1 = max(1, floor(nx/TailleBlocX));
N2 = max(1, floor(nx/TailleBlocY));
str1 = sprintf('Export des fichiers geotif en cours : %s', nomLayer);
str2 = sprintf('Exporting geotif images : %s', nomLayer);
hw = create_waitbar(Lang(str1,str2), 'N', N1*N2);
for kx=1:TailleBlocX:nx
    subsubx = (kx-delta):(kx+delta+TailleBlocX);
    subsubx(subsubx < 1) = [];
    subsubx(subsubx > nx) = [];
    for ky=1:TailleBlocY:ny
        my_waitbar(k, N1*N2, hw)
        
        subsuby = (ky-delta):(ky+delta+TailleBlocY);
        subsuby(subsuby < 1)  = [];
        subsuby(subsuby > ny) = [];
        
        %       figure(7675); imagesc(I(suby(subsuby),subx(subsubx))); drawnow; pause
        
        nomFicXMLxy = sprintf('%s_%d_%d', nomLayer, (kx-1)/TailleBlocX, (ky-1)/TailleBlocY);
        
        nomDirLayer = fullfile(nomLayer, SubLayerDirectory);
        nomDir = fullfile(repExport, nomDirLayer);
        if ~exist(nomDir, 'dir')
            mkdir(nomDir);
        end
        
        % Pas de r�pertoire index� par l'indice de Temps dans le cas des
        % layers de type Index.
        if ~isempty(kTime)
            nomDirTif = fullfile(nomDir, num2str(kTime));
            if ~exist(nomDirTif, 'dir')
                mkdir(nomDirTif);
            end
            nomFicTiff = fullfile(nomDirLayer, num2str(kTime), [nomFicXMLxy '.tif']);
        else
            nomFicTiff = fullfile(nomDirLayer, [nomFicXMLxy '.tif']);
        end
        
        [flag, ~, minBloc, maxBloc, XLim, YLim] = export_geoTiff(this, ...
            'nomFic', nomFicTiff, 'subx', subx(subsubx), 'suby', suby(subsuby), 'repExport', repExport, ...
            'nbChanels', 1, 'NoNan', true, 'Type', 'single', 'Texture', false, 'Carto', Carto);
        
        if ~flag
            my_close(hw)
            return
        end
        
        minVal = min(minVal, minBloc);
        maxVal = max(maxVal, maxBloc);
        
        k = k +1;
        nomFicXMLTile{k} = nomFicXMLxy; %#ok<AGROW>
        XLimTile{k} = XLim; %#ok<AGROW>
        YLimTile{k} = YLim; %#ok<AGROW>
        sizePixels{k} = [size(subx(subsubx), 2), size(suby(subsuby), 2)]; %#ok<AGROW>
    end
end
my_close(hw)


% --- Mise en forme pour la classe MultiData de l'ent�te de layer.
function LayerSet = MiseEnFormeHeaderLayer(this, nomLayer)

LayerSet.LayerDescription.LayerName             = nomLayer;
LayerSet.LayerDescription.PermanentDirectory    = nomLayer;
LayerSet.LayerDescription.North                 = -Inf;
LayerSet.LayerDescription.South                 = Inf;
LayerSet.LayerDescription.West                  = Inf;
LayerSet.LayerDescription.East                  = -Inf;
LayerSet.LayerDescription.Resolution.Latitude   = NaN;
LayerSet.LayerDescription.Resolution.Longitude  = NaN;
LayerSet.LayerDescription.DateInterval.DateMin  = 1;
LayerSet.LayerDescription.DateInterval.DateMax  = size(this.DataLayer,3);
LayerSet.LayerDescription.DateInterval.DateStep = 1;


% --- Mise en forme por la classe MultiData des Layers de donn�es.
function LayerSet = MiseEnFormeDataLayer(this, LayerSet, nomLayer, repExport, subxIn, subyIn, sizeTiff)

Carto = [];
West  = LayerSet.LayerDescription.West;
East  = LayerSet.LayerDescription.East;
South = LayerSet.LayerDescription.South;
North = LayerSet.LayerDescription.North;

% Traitement des layers de type Data.
DateMin  = 1;
[~, nbDataType, DateMax]  = size(this);
nbLayers = size(this,1);
minBlocAllLayers =  Inf(nbLayers,nbDataType);
maxBlocAllLayers = -Inf(nbLayers,nbDataType);
N = (DateMax-DateMin+1) * nbLayers * nbDataType;
str1 = sprintf('Export du Multidata en cours : %s', nomLayer);
str2 = sprintf('Exports Multidata : %s', nomLayer);
hw = create_waitbar(Lang(str1,str2), 'N', N);
khw = 1;
for kTime=DateMin:DateMax
    for kLayer=1:nbLayers
        for kDataType=1:nbDataType
            my_waitbar(khw, N, hw)
            khw = khw+1;
            
            DataTypeName{kLayer} = get_DataTypeName(this(kLayer,kDataType,kTime)); %#ok<AGROW>
            if ~strcmp(DataTypeName{kLayer}, 'Unknown')
                ImageName         = extract_ImageName(this(kLayer,kDataType,1));
                SubLayerDirectory = [DataTypeName{kLayer} '_' ImageName];
                
                % R�ajustement de l'extraction des subx et suby en fonction des param�tres d'entr�e.
                % Les tailles d'images peuvent �tre diff�rentes.
                subx  = 1:this(kLayer,kDataType,kTime).nbColumns;
                if ~isempty(subxIn)
                    subx = subx(subxIn);
                end
                suby  = 1:this(kLayer,kDataType,kTime).nbRows;
                if ~isempty(subyIn)
                    suby = suby(subyIn);
                end
                [flag, nomFicXMLTile, minBloc, maxBloc, XLimTile, YLimTile, pppp, Carto] = exportGeotiff3DV(this(kLayer,kDataType,kTime), ...
                    repExport, SubLayerDirectory, nomLayer, subx, suby, sizeTiff, kTime, Carto); %#ok<ASGLU>
                if ~flag
                    % Il y a vraiment pb ! (dansle chemin de g�n�ration par
                    % exemple).
                    if isempty(nomFicXMLTile)
                        my_close(hw)
                        return
                    end
                    continue
                end
                
                minBlocAllLayers(kLayer,kDataType,DateMin) = min(minBlocAllLayers(kLayer,kDataType,DateMin), minBloc);
                maxBlocAllLayers(kLayer,kDataType,DateMin) = max(maxBlocAllLayers(kLayer,kDataType,DateMin), maxBloc);
                
                for k=1:length(XLimTile)
                    West  = min(West,  XLimTile{k}(1)); % Attention ligne de changement de date
                    East  = max(East,  XLimTile{k}(2));
                    South = min(South, YLimTile{k}(1));
                    North = max(North, YLimTile{k}(2));
                    sizePixels = [numel(subx),numel(suby)];
                end
                LayerSet.SubLayer(kLayer,kDataType,DateMin).SubLayerName      = ImageName;
                LayerSet.SubLayer(kLayer,kDataType,DateMin).Type              = DataTypeName{kLayer};
                LayerSet.SubLayer(kLayer,kDataType,DateMin).Unit              = this(kLayer,kDataType,kTime).Unit;
                LayerSet.SubLayer(kLayer,kDataType,DateMin).IsRGB             = 0;
                LayerSet.SubLayer(kLayer,kDataType,DateMin).ValMin            = minBlocAllLayers(kLayer,kDataType,DateMin);
                LayerSet.SubLayer(kLayer,kDataType,DateMin).ValMax            = maxBlocAllLayers(kLayer,kDataType,DateMin);
                LayerSet.SubLayer(kLayer,kDataType,DateMin).SubLayerDirectory = SubLayerDirectory;
                InitialFileName = this(kLayer,kDataType,kTime).InitialFileName;
                if isempty(InitialFileName)
                    InitialFileName = nomLayer;
                else
                    [~, InitialFileName] = fileparts(InitialFileName);
                end
                % Ecriture des dates sous forme de Balises
                % multiples sous DateIndex.
                LayerSet.SubLayer(kLayer,kDataType,DateMin).DateIndex.Date{kTime} = kTime;
                LayerSet.SubLayer(kLayer,kDataType,DateMin).GroupID = InitialFileName;
            end
        end
    end
end
my_close(hw)

% Assignation des min/max des intervalles de donn�es (coordonn�es,
% valeurs).
LayerSet.LayerDescription.North                = North;
LayerSet.LayerDescription.South                = South;
LayerSet.LayerDescription.West                 = West;
LayerSet.LayerDescription.East                 = East;
LayerSet.LayerDescription.Resolution.Latitude  = abs(North-South)/sizePixels(2);
LayerSet.LayerDescription.Resolution.Longitude = abs(East-West)/sizePixels(1);

for kTime=DateMin:DateMax
    for kLayer=1:nbLayers
        for kDataType=1:nbDataType
            LayerSet.SubLayer(kLayer,kDataType,DateMin).ValMin = min([LayerSet.SubLayer(:,kDataType,:).ValMin]);
            LayerSet.SubLayer(kLayer,kDataType,DateMin).ValMax = max([LayerSet.SubLayer(:,kDataType,:).ValMax]);
        end
    end
end

LayerSet.SubLayer = LayerSet.SubLayer(:);


% Mise en forme pour la classe MultiData des layers d'index si ils existent.
function LayerSet = MiseEnFormeIndexLayer(this, LayerSet, nomLayer, repExport, subxIn, subyIn, sizeTiff)

if isempty(this)
    return
end

Carto = [];

West  = LayerSet.LayerDescription.West;
East  = LayerSet.LayerDescription.East;
South = LayerSet.LayerDescription.South;
North = LayerSet.LayerDescription.North;

% R�cup�ration pour ajout aux DataLayers.
nbDataLayer = numel(LayerSet.SubLayer);

% Traitement des layers de type Data.
[nbDataType, ~]  = size(this);
nbLayers         = 1; % size(this,1);
minBlocAllLayers = Inf(nbLayers,nbDataType);
maxBlocAllLayers = -Inf(nbLayers,nbDataType);
for kDataType=1:nbDataType
    DataTypeName{kDataType} = get_DataTypeName(this(kDataType)); %#ok<AGROW>
    ImageName               = extract_ImageName(this(kDataType));
    % SubLayerDirectory       = [DataTypeName{kDataType} '_' ImageName];
    SubLayerDirectory       = DataTypeName{kDataType};
    
    % TODO : R�ajuster l'extraction des subx et suby en
    % fonction des param�tres d'entr�e. Les tailles d'images peuvent �tre diff�rentes.
    subx  = 1:this(kDataType).nbColumns;
    if ~isempty(subxIn)
        subx = subx(subxIn);
    end
    suby  = 1:this(kDataType).nbRows;
    if ~isempty(subyIn)
        suby = suby(subyIn);
    end
    [flag, nomFicXMLTile, minBloc, maxBloc, XLimTile, YLimTile, sizePixels, Carto] = exportGeotiff3DV(this(kDataType), ...
        repExport, SubLayerDirectory, nomLayer, subx, suby, sizeTiff, [], Carto); %#ok<ASGLU>
    if ~flag
        LayerSet = [];
        return
    end
    
    minBlocAllLayers(kDataType) = min(minBlocAllLayers(kDataType), minBloc);
    maxBlocAllLayers(kDataType) = max(maxBlocAllLayers(kDataType), maxBloc);
    
    for k=1:length(XLimTile)
        West  = min(West,  XLimTile{k}(1)); % Attention ligne de changement de date
        East  = max(East,  XLimTile{k}(2));
        South = min(South, YLimTile{k}(1));
        North = max(North, YLimTile{k}(2));
    end
    LayerSet.SubLayer(kDataType+nbDataLayer).SubLayerName      = ImageName;
    LayerSet.SubLayer(kDataType+nbDataLayer).Type              = DataTypeName{kDataType};
    LayerSet.SubLayer(kDataType+nbDataLayer).Unit              = this(kDataType).Unit;
    LayerSet.SubLayer(kDataType+nbDataLayer).IsRGB             = 0;
    LayerSet.SubLayer(kDataType+nbDataLayer).ValMin            = minBlocAllLayers(kDataType);
    LayerSet.SubLayer(kDataType+nbDataLayer).ValMax            = maxBlocAllLayers(kDataType);
    LayerSet.SubLayer(kDataType+nbDataLayer).SubLayerDirectory = SubLayerDirectory;
    % La balise pour un layer de Type Index = all.
    LayerSet.SubLayer(kDataType+nbDataLayer).GroupID = 'all';
end

LayerSet.LayerDescription.North = North;
LayerSet.LayerDescription.South = South;
LayerSet.LayerDescription.West  = West;
LayerSet.LayerDescription.East  = East;
LayerSet.LayerDescription.Resolution.Latitude  = abs(North-South)/sizePixels{k}(2);
LayerSet.LayerDescription.Resolution.Longitude = abs(East-West)/sizePixels{k}(1);

for kDataType=1+nbDataLayer:nbDataType+nbDataLayer
    LayerSet.SubLayer(kDataType).ValMin = min([LayerSet.SubLayer(kDataType).ValMin]);
    LayerSet.SubLayer(kDataType).ValMax = max([LayerSet.SubLayer(kDataType).ValMax]);
end

LayerSet.SubLayer = LayerSet.SubLayer(:);
