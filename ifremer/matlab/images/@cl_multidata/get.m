% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_multidata
%
% Name-Value Pair Arguments
%  Cf. cl_multidata
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passees en argument
%
% Examples
%   nomFic      = getNomFicDatabase('lena.tif');
%   I           = imread(nomFic);
%   a           = cl_image('DataLayer', Lena);
%   timeData    = repmat(now, 256,256);
%   idx         = cl_image('DataIndex', timeData);
%   m           = cl_multidata('DataLayer', a, 'DataIndex', idx)
%
%   Images = get(a, 'DataLayer');
%   [x, y] = get(Images(1), 'x', 'y');
%
% See also cl_multidata cl_multidata/set Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% On v�rifie que l'instance est bien de demension 1

if length(this) ~= 1
    my_warndlg('cl_multidata/get : L''instance doit etre de dimension 1', 1);
    return
end

%% Boucle de parcours des arguments

varargout = {};
for i=1:length(varargin)

    switch varargin{i}
        case 'Name'
            varargout{end+1} = this.Name; %#ok<AGROW> 
        case 'DataLayer'
            varargout{end+1} = this.DataLayer; %#ok<AGROW> 
        case 'DimDataLayer'
            varargout{end+1} = this.DimDataLayer; %#ok<AGROW> 
        case 'DataIndex'
            varargout{end+1} = this.DataIndex; %#ok<AGROW> 
        case 'DimDataIndex'
            varargout{end+1} = this.DimDataIndex; %#ok<AGROW> 

        otherwise
            str = sprintf('cl_multidata/get %s non pris en compte', varargin{i});
            my_warndlg(['cl_multidata:get ', str], 0);
            varargout{1} = [];
    end
end
