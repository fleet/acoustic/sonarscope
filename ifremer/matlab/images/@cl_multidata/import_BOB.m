% Importation d'une arborescence Bob pour visualisation dans SSC.
%
% Syntax
%   flag = import_BOB(this, varargin)
%
% Input Arguments
%   this        : Instance de cl_image
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%   nomFic          = 'F:\SonarScopeData\BOB-3D\Data\EI_Lay_SvalBard.xml';
%   a               = cl_Bob(nomFicXml);
%   [flag, Data]    = import_BOB(a, nomFic)
%
% See also cl_image Authors
% Authors : GLU
% -------------------------------------------------------------------------

function [flag, this] = import_BOB(~, nomFic, varargin)

global DEBUG %#ok<GVMIS>

if isempty(DEBUG)
    DEBUG = 0;
end

[varargin, Carto]            = getPropertyValue(varargin, 'Carto', []);
[varargin, selectNumLayers]  = getPropertyValue(varargin, 'listNumLayers', []);
[varargin, flagLayerType]    = getPropertyValue(varargin, 'listFlagLayerType', []);
[varargin, selectIndexType]  = getPropertyValue(varargin, 'listIndexType', []);
[varargin, selectThresholds] = getPropertyValue(varargin, 'listThresholds', []);
[varargin, Video]            = getPropertyValue(varargin, 'Video', 1);
[varargin, ColorMapIndex]    = getPropertyValue(varargin, 'ColorMapIndex', 1); %#ok<ASGLU>

%% Lecture du fichier
a = cl_Bob('nomFic', nomFic, 'Memmapfile', 0);
if isempty(a)
    flag = 0;
    return
end

Mat_Data_File = get(a, 'Mat_Data_File');
if ~isempty(Mat_Data_File)
    Mat_EI_Cycles = loadmat(Mat_Data_File);
end

%% Initialisations
NbCells      = get(a, 'NbCells');
NbSectors    = get(a, 'NbSectors');
% BeamWidth    = get(a, 'BeamWidth');
SizeCellEI   = get(a, 'SizeCellEI');
Thresholds   = get(a, 'Th');

this                 = cl_image.empty;

objDataLayers        = cl_image.empty;

objIndexLayers       = cl_image.empty;


% Subdivision des layers de type Data et les layers de tpye Index.
indexData       = [1 2 3];
selectLayerType = indexData(flagLayerType == 1 & selectIndexType == 0);
subIndex        = indexData(flagLayerType == 1 & selectIndexType == 1);
selectIndexType = subIndex - length(selectLayerType);


% D�signation du nom du layer exploit�.
strUS = 'Which Signal do you want to display ?';
strFR = 'Quel est le signal � afficher ?';
str   = {'MVBS: Mean Volume Backscatttering Strength'; 'NASC: Nautical Area Scattering Coefficient'; 'NASS: Nautical Area Scattering Strength=10log(NASC)'};
[rep, flag] = my_listdlg(Lang(strFR, strUS), str, 'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag
    return
end
if rep == 1
    LayerSigLabel = {'MVBS'}; % Mean Volume Backscatttering Strength
    LayerSigUnit  = {'dB re /m'};
    LayerSigName  = {'Sv_bot'};
elseif rep == 2
    LayerSigLabel = {'NASC'}; % Nautical Area Scattering Coefficient
    LayerSigUnit  = {'m2/nmi2'};
    LayerSigName  = {'Sa_bot'};
else
    LayerSigLabel = {'NASS'}; % Nautical Area Scattering Strength, �quivalent du 10*log10(NASC)
    LayerSigUnit  = {'dB re 1(m2/nmi2)'};
    LayerSigName  = {'Sa_bot'};
end
LayerSigType       = {'reflectivity'};

% D�signation des layers de type Index.
LayerIndexSigLabel   = {'NumSector', 'NumCell'};
LayerIndexSigUnit    = {'#', 'm'};
LayerIndexSigType    = {'RxBeamIndex', 'RayPathLength'};

%% Mise en forme du layer d'images.

if DEBUG
    Fig = FigUtils.createSScFigure;
    ha = [];
end
str1            = sprintf('Exploitation des donn�es en cours');
str2            = sprintf('Processing data');
nbIterTotal     = length(selectNumLayers) * length(selectThresholds);
pasWaitBar      = 1;
hw              = create_waitbar(Lang(str1,str2), 'N', nbIterTotal);
LayerSecteur    = [];
LayerCellSignal = [];

% Inhibition du message sur le doublon de valeurs lors du calcul
% d'interpolation.
warning('off', 'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

SonarDescription = cl_sounder('Sonar.Ident', 27); % EK60

for iCycle=1:length(selectNumLayers)
    for iThreshold=1:length(selectThresholds)
        iCurrentTh  	= selectThresholds(iThreshold);
        iCurrentCycle   = selectNumLayers(iCycle);
        iWaitBar        = floor(((iCycle-1)*length(selectThresholds) + iThreshold)/pasWaitBar);
        my_waitbar(iWaitBar, nbIterTotal, hw);
        
        % Formattage des images depuis la structure issue des fichiers BOB.
        if strcmp(LayerSigLabel, 'NASS')
            Image = reflec_Enr2dB(Mat_EI_Cycles(iCurrentCycle, iCurrentTh).(LayerSigName{1}));
        else
            Image = Mat_EI_Cycles(iCurrentCycle, iCurrentTh).(LayerSigName{1});
        end
        
        % Pour contr�ler � l'arriv�e si on abien ce positionnement
        
        Lat = Mat_EI_Cycles(iCurrentCycle, iCurrentTh).lat_bot;
        Lon = Mat_EI_Cycles(iCurrentCycle, iCurrentTh).lon_bot;
        
        % Il y a un secteur qui n'est pas repr�sent� par pcolor: analyser
        % pourquoi. Il s'agit sans doute de la d�finition : piquets ou
        % centres des intervales
        
        % Transformation de l'image en image polaire.
        TxAngle          = Mat_EI_Cycles(iCurrentCycle, iCurrentTh).Angle_secteur_Pan;
        TxAngle = mod(TxAngle,360);
        
        % La donn�e est �tendue � une secteur lat�ral suppl�mentaire : cela
        % doit permettre d'�viter les effets de bord lors du filtrage sous
        % GLOBE.
        TxAngle = interp1(1:NbSectors,TxAngle,0:(NbSectors+1), 'linear', 'extrap');
        
        %{
% Recherche des secteurs qui n'ont pas tourn�
difAng = diff(TxAngle);
subEntrop = find(difAng == 0);
if ~isempty(subEntrop)
TxAngle(subEntrop) = [];
Image(:,subEntrop) = [];
difAng = diff(TxAngle);
end

% Recherche des secteurs qui manquent
subPlus = find(difAng > median(difAng));
for k=1:length(subPlus)
TxAngle = [TxAngle(:,subPlus(k)-1) NaN(NbCells,1) TxAngle(:,subPlus(k):end)];
Image   = [Image(:,subPlus(k)-1) NaN(NbCells,1) Image(:,subPlus(k):end)];
end
        %}
        
        D = (1:NbCells) * SizeCellEI; % 55 * 2 m
        X = D' * sind(TxAngle);
        Y = D' * cosd(TxAngle);
        
        I         = Image;
        I         = I'; % TODO : il faudrait que I soit donn� directement avec la transpos�e (g�om�trie SampleBeam)
        I         = [I(:,1) I I(:,end)]; %#ok<AGROW>
        Xmax      = max(X(:));
        Xmin      = min(X(:));
        Ymax      = max(Y(:));
        Ymin      = min(Y(:));
        
        
        % Cette figure ressemble bien � celle qui est trac�e en coordonn�es
        % g�ographiques
        Pas = Ymax / length(D);
        % Sur�chantillonnage arbitraire des images.
        Pas = Pas / 4;
        
        [x, Pas] = centrage_magnetique(Xmin:Pas:Xmax);
        y        = centrage_magnetique(Ymin:Pas:Ymax);
        
        [XI, YI] = meshgrid(x, y);
        %{
% 3 m�thodes se r�v�lent int�ressantes en remplacement de la m�thode griddingPolar.
Layer = griddata(X, Y, I, XI, YI);
% ou

Layer = griddata(X, Y, I, XI, YI, 'nearest');
LayerMask = griddata(X, Y, I, XI, YI);
Layer(isnan(LayerMask)) = NaN;

%ou
F = scatteredInterpolant(X(:), Y(:), I(:), 'nearest');
Layer = F(XI, YI);

        %}
        
        % Le filtrage des Nan doit �tre en interpolation lin�aire.
        FMask     = scatteredInterpolant(X(:), Y(:), I(:), 'linear');
        LayerMask = FMask(XI, YI);
        
        % }
        % Remplissage du masque par Secteurs.
        if isempty(LayerSecteur)
            pppp = [0 1:NbSectors NbSectors+1];
            % % %             pppp = 1:NbSectors;
            MasqueSecteur           = repmat(pppp, NbCells, 1); % TODO GLUGLUGLU
            FMaskSecteur            = scatteredInterpolant(X(:), Y(:), MasqueSecteur(:),'nearest');
            LayerSecteur            = FMaskSecteur(XI, YI);
            LayerSecteur(isnan(LayerMask)) = NaN;
            %             LayerSecteur = floor(LayerSecteur);
            Mask = (LayerSecteur == 0) | (LayerSecteur == (NbSectors + 1));
            LayerSecteur(Mask)      = NaN;
            % Remplissage du masque par Cellules De signaux.
            RangeMax                = SizeCellEI*NbCells;
            MasqueCellSignal        = repmat(1:SizeCellEI:RangeMax, NbSectors+2, 1)'; % TODO GLUGLUGLU
            % % %             MasqueCellSignal        = repmat(1:SizeCellEI:RangeMax, NbSectors, 1)';
            FMaskCellSignal         = scatteredInterpolant(X(:), Y(:), MasqueCellSignal(:), 'nearest');
            LayerCellSignal         = FMaskCellSignal(XI, YI);
            LayerCellSignal(isnan(LayerMask)) = NaN;
            LayerCellSignal(Mask)   = NaN;
            %             LayerCellSignal = floor(LayerCellSignal);
        end
        
        F = scatteredInterpolant(X(:), Y(:), I(:), 'nearest');
        
        Layer           = F(XI, YI);
        MaskData        = isnan(LayerSecteur) | Mask;
        Layer(MaskData) = NaN;
        
        %% Calcul des coordonn�es g�ographiques du cadre enblobant de l'image
        
        if isempty(Carto)
            LonMean      = mean(Lon(:, 1), 'omitnan'); % Je recherche ici la longitude de Bob, elle est certainement disponible directement quelque part
            InitialValue = floor((LonMean+180)/6 + 1);
            Carto        = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3,  'Projection.UTM.Fuseau', InitialValue);
        end
        
        LatMean = mean(Lat(:, 1), 'omitnan'); % M�me remarque que LonMean
        
        % Calcul des coordonn�es x,y de Bob
        [xBob, yBob] = latlon2xy(Carto, LatMean, LonMean);
        
        % Calcul des coordonn�es des quatres coins de l'image
        xCoins               = [xBob+Xmin, xBob+Xmin, xBob+Xmax, xBob+Xmax];
        yCoins               = [yBob+Ymin, yBob+Ymax, yBob+Ymin, yBob+Ymax];
        [LatCoins, LonCoins] = xy2latlon(Carto, xCoins, yCoins);
        
        LatSud  = min(LatCoins);
        LatNord = max(LatCoins);
        LonWest = min(LonCoins);
        LonEst  = max(LonCoins);
        
        LatEI = linspace(LatSud,  LatNord, size(Layer,1));
        LonEI = linspace(LonWest, LonEst,  size(Layer,2));
        % Centrage magn�tique n�cessaire pour une restauration/affichage
        % ult�rieure du paquet d'images.
        LatEI = centrage_magnetique(LatEI);
        LonEI = centrage_magnetique(LonEI);
        
        TransducerDepth = 0; %#ok<NASGU> % TODO : immersion � r�cup�rer.
        
        % Boucle sur les DataTypes
        InitialFileName = num2str(Thresholds(iCurrentTh));
        LayerData   = {Layer};
        LayerIndex  = {LayerSecteur;LayerCellSignal};
        
        %% Traitements des layers de donn�es.
        for iLayerSigType=1:length(selectLayerType)
            iCurrentLayerSigType  	= selectLayerType(iLayerSigType);
            
            % Pour Carla, ajouter dB si le layer = Reflectivity
            ImageName       = [LayerSigLabel '_Threshold_' num2str(Thresholds(iCurrentTh)) 'dB'];
            
            TypeLayer       = cl_image.indDataType(LayerSigType{iCurrentLayerSigType});
            LayerUnit       = LayerSigUnit{iCurrentLayerSigType};
            % Remplissage du layer de signaux.
            objDataLayers(iThreshold, iLayerSigType, iCycle) = cl_image('Image', LayerData{iLayerSigType}, ...
                'Name',            [ImageName{:}], ...
                'Unit',                 LayerUnit, ...
                'x',                    LonEI, ...
                'y',                    LatEI, ...
                'XUnit',                '"', ...
                'YUnit',                'Ping', ...
                'ColormapIndex',        3, ...
                'DataType',             TypeLayer, ...
                'TagSynchroX',          'BobLongitude', ...
                'TagSynchroY',          'BobLatitude', ...
                'GeometryType',         cl_image.indGeometryType('LatLong'), ...
                'InitialFileName',      InitialFileName, ...
                'InitialFileFormat',    'MAT', ...
                'SonarDescription',      SonarDescription);
            
            if isempty(Carto)
                Carto = getDefinitionCarto(objDataLayers(iThreshold, 1, iCycle));
            end
            
            % Positionnement des attributs de chaque image.
            objDataLayers(iThreshold, iLayerSigType, iCycle) = set(objDataLayers(iThreshold, iLayerSigType, iCycle),  'Carto', Carto);
            objDataLayers(iThreshold, iLayerSigType, iCycle) = update_Name(objDataLayers(iThreshold, iLayerSigType, iCycle));
            objDataLayers(iThreshold, iLayerSigType, iCycle) = set(objDataLayers(iThreshold, iLayerSigType, iCycle), 'ColormapIndex', ColorMapIndex);
            objDataLayers(iThreshold, iLayerSigType, iCycle) = set(objDataLayers(iThreshold, iLayerSigType, iCycle), 'Video', Video);
            
            
            if DEBUG
                subSecteur = (2:NbSectors+1); % On extrait les valeurs utiles (1 � 24 soit 2 � 25).
                figure(Fig); subplot(2,2,1);
                pcolor(Lon, Lat, Image); axisGeo; shading flat;
                hold on; PlotUtils.createSScPlot(Lon', Lat', '*'); axisGeo; grid on; title('Lat, Lon, .mat'); hold off;
                figure(Fig); subplot(2,2,2);
                pcolor(X(:,subSecteur), Y(:,subSecteur), I(:,subSecteur)); axis equal; shading flat; title('X, Y, I')
                hold on; PlotUtils.createSScPlot(X(:,subSecteur), Y(:,subSecteur), '*'); grid on; axis equal; axis tight; hold off;
                figure(Fig); subplot(2,2,3);
                imagesc([Xmin Xmax], [Ymin Ymax], Layer); axis xy; axis equal; title('GridPolarBob')
                hold on; plot(X(:,subSecteur), Y(:,subSecteur), '*'); axis tight; hold off;
                figure(Fig);
                if ~isempty(ha) && ishandle(ha)
                    delete(ha)
                end
                ha = subplot(2,2,4);
                imagesc(objDataLayers(iThreshold, 1, iCycle), 'Axe', ha); title('cl_image')
                hold on; PlotUtils.createSScPlot(Lon', Lat', '*'); grid on; title('Lat, Lon, .mat'); hold off;
                
                % Affichage des donn�es G�om�triques.
                figure(11111); subplot(2,1,1);
                imagesc([Xmin Xmax], [Ymin Ymax], LayerSecteur); colorbar; axis xy; axis equal; title('Secteurs d''�cho int�grations')
                hold on; plot(X(:,subSecteur), Y(:,subSecteur), '*'); axis tight; hold off;
                figure(11111); subplot(2,1,2);
                imagesc([Xmin Xmax], [Ymin Ymax], LayerCellSignal); colorbar; axis xy; axis equal; title('Cellules d''�cho int�grations')
                hold on; plot(X(:,subSecteur), Y(:,subSecteur), '*'); axis tight; hold off;
            end
        end % Fin de la boucle sur les DataType. 
    end
end

%% Traitements des layers d'index
for iIndexType=1:length(selectIndexType)
    iCurrentIndexType = selectIndexType(iIndexType);
    ImageName       = LayerIndexSigLabel(iCurrentIndexType);
    
    TypeLayer       = cl_image.indDataType(LayerIndexSigType{iCurrentIndexType});
    LayerUnit       = LayerIndexSigUnit{iCurrentIndexType};
    % Remplissage du layer de signaux.
    objIndexLayers(iIndexType) = cl_image('Image', LayerIndex{iCurrentIndexType}, ...
        'Name',            ImageName{:}, ...
        'Unit',                 LayerUnit, ...
        'x',                    LonEI, ...
        'y',                    LatEI, ...
        'XUnit',                '"', ...
        'YUnit',                'Ping', ...
        'ColormapIndex',        3, ...
        'DataType',             TypeLayer, ...
        'TagSynchroX',          'BobLongitude', ...
        'TagSynchroY',          'BobLatitude', ...
        'GeometryType',         cl_image.indGeometryType('LatLong'), ...
        'InitialFileName',      InitialFileName, ...
        'InitialFileFormat',    'MAT', ...
        'SonarDescription',      SonarDescription);
    
    if isempty(Carto)
        Carto = getDefinitionCarto(objIndexLayers(iIndexType));
    end
    
    % Positionnement des attributs de chaque image.
    objIndexLayers(iIndexType) = set(objIndexLayers(iIndexType),  'Carto', Carto);
    objIndexLayers(iIndexType) = update_Name(objIndexLayers(iIndexType));
    objIndexLayers(iIndexType) = set(objIndexLayers(iIndexType), 'ColormapIndex', ColorMapIndex);
    objIndexLayers(iIndexType) = set(objIndexLayers(iIndexType), 'Video', Video);
    
    
    if DEBUG
        subSecteur = (2:NbSectors+1); % On extrait les valeurs utiles (1 � 24 soit 2 � 25).
        figure(Fig); subplot(2,2,1);
        pcolor(Lon, Lat, Image); axisGeo; shading flat;
        hold on; PlotUtils.createSScPlot(Lon', Lat', '*'); axisGeo; grid on; title('Lat, Lon, .mat'); hold off;
        figure(Fig); subplot(2,2,2);
        pcolor(X(:,subSecteur), Y(:,subSecteur), I(:,subSecteur)); axis equal; shading flat; title('X, Y, I')
        hold on; PlotUtils.createSScPlot(X(:,subSecteur), Y(:,subSecteur), '*'); grid on; axis equal; axis tight; hold off;
        figure(Fig); subplot(2,2,3);
        imagesc([Xmin Xmax], [Ymin Ymax], Layer); axis xy; axis equal; title('GridPolarBob')
        hold on; plot(X(:,subSecteur), Y(:,subSecteur), '*'); axis tight; hold off;
        figure(Fig);
        if ~isempty(ha) && ishandle(ha)
            delete(ha)
        end
        ha = subplot(2,2,4);
        imagesc(objIndexLayers(iThreshold, 1, iCycle), 'Axe', ha); title('cl_image')
        hold on; PlotUtils.createSScPlot(Lon', Lat', '*'); grid on; title('Lat, Lon, .mat'); hold off;
        
        % Affichage des donn�es G�om�triques.
        figure(11111); subplot(2,1,1);
        imagesc([Xmin Xmax], [Ymin Ymax], LayerSecteur); colorbar; axis xy; axis equal; title('Secteurs d''�cho int�grations')
        hold on; plot(X(:,subSecteur), Y(:,subSecteur), '*'); axis tight; hold off;
        figure(11111); subplot(2,1,2);
        imagesc([Xmin Xmax], [Ymin Ymax], LayerCellSignal); colorbar; axis xy; axis equal; title('Cellules d''�cho int�grations')
        hold on; plot(X(:,subSecteur), Y(:,subSecteur), '*'); axis tight; hold off;
    end
    
    
end % Fin de la boucle sur les Layers de type Index.
my_close(hw, 'MsgEnd');


% R�activation du message sur le doublon de valeurs lors du calcul
% d'interpolation.
warning('on', 'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');


this = cl_multidata('DataLayer', objDataLayers, 'DataIndex', objIndexLayers);
DimDataLayer(1).Name    = 'nbThreshold';
DimDataLayer(1).Value   = length(selectThresholds);
DimDataLayer(2).Name    = 'nbDataType';
DimDataLayer(2).Value   = 1;
DimDataLayer(3).Name    = 'nbCycles';
DimDataLayer(3).Value   = length(selectNumLayers);
set(this, 'DimDataLayer', DimDataLayer);

DimDataIndex(1).Name    = Lang('Neutre', 'Neutral');
DimDataIndex(1).Value   = 1;
DimDataIndex(2).Name    = 'nbIndexType';
DimDataIndex(2).Value   = length(selectIndexType);

set(this, 'DimDataIndex', DimDataIndex);

% objDataLayers   = objDataLayers(:);
% objIndexLayers  = objIndexLayers(:);

flag = 1;


