% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_multidata
%
% Name-Value Pair Arguments
%   DataLayer  : Layers de donn�es de type Image
%   DataIndex  : Index g�ographiques/temporels de donn�es de type Image
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_multidata Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = [];

%% Traitement
str{end+1} = sprintf('Name   <-- %s', this.Name);

str{end+1} = char_instance_Layers(this);

str{end+1} = char_instance_Index(this);

%% Concatenation en une chaine

str = cell2str(str);


function str = char_instance_Layers(this)

str = [];
str{end+1} = sprintf('--- Data Layer --------------------');
nbDims      = ndims(this.DataLayer);
% sz          = size(this.DataLayer);
pppp        = this.DataLayer;
str{end+1}  = sprintf('     Contents :');
str{end+1}  = sprintf('     -----------:');
if ~isempty(pppp)
    for k=1:numel(pppp)
        str{end+1} = sprintf('      ImageName(%02d)   <-- %s', k, pppp(k).Name); %#ok<AGROW> 
    end
    if ~isempty(this.DataLayer)
        str{end+1} = sprintf('      ClasseName      <-- %s', class(this.DataLayer(1)));
    end
end
str{end+1}  = sprintf('     Dimensions :');
str{end+1}  = sprintf('     -----------:');
pppp        = this.DimDataLayer;
if ~isempty(pppp)
    for kDim=1:nbDims
        str{end+1} = sprintf('       Dim(%02d) Name=Value   <-- %s=%d', kDim, pppp(kDim).Name, pppp(kDim).Value); %#ok<AGROW> 
    end
end

%% Concatenation en une chaine
str = cell2str(str);

function str = char_instance_Index(this)

str = [];
str{end+1} = sprintf('--- Data Index --------------------');
nbDims      = ndims(this.DataIndex);
% sz          = size(this.DataIndex);
pppp        = this.DataIndex(:);
str{end+1}  = sprintf('     Contents :');
str{end+1}  = sprintf('     -----------:');
if ~isempty(pppp)
    for k=1:length(pppp)
        str{end+1} = sprintf('     IndexName(%02d)  <-- %s', k, pppp(k).Name); %#ok<AGROW> 
    end
    if ~isempty(this.DataIndex)
        str{end+1} = sprintf('     ClassName      <-- %s', class(this.DataIndex(1)));
    end
end
str{end+1}  = sprintf('     Dimensions :');
str{end+1}  = sprintf('     -----------:');
pppp        = this.DimDataIndex;
if ~isempty(pppp)
    for kDim=1:nbDims
        str{end+1} = sprintf('       Dim(%02d) Name=Value   <-- %s=%d', kDim, pppp(kDim).Name, pppp(kDim).Value); %#ok<AGROW> 
    end
end

%% Concatenation en une chaine
str = cell2str(str);
