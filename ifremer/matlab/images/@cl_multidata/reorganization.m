function M = reorganization(this, flagMultiTime)

M = cl_multidata([]);

% On consid�re les layers comme juxtaposables (sur la m�me �chelle de temps).
if flagMultiTime
    for k=1:numel(this.DataLayer)
        ImageName{k} = extract_ImageName(this.DataLayer(k)); %#ok<AGROW>
        DataType(k)  = this.DataLayer(k).DataType; %#ok<AGROW>
    end
    listDataType  = unique(DataType);
    listImageName = unique(ImageName, 'stable');
        
    nbLayers   = length(listImageName);
    nbDataType = length(listDataType);
    for kLayer=1:nbLayers
        for kDataType=1:nbDataType
            sub = find((strcmp(ImageName, listImageName{kLayer})) & (DataType == listDataType(kDataType)));
            DataLayer(kLayer,kDataType,1:length(sub)) = this.DataLayer(sub); %#ok<AGROW>
        end
    end
    
else
    for k=1:numel(this.DataLayer)
        DataType(k) = this.DataLayer(k).DataType; %#ok<AGROW>
    end
    listDataType = unique(DataType);
    
    nbDataType = length(listDataType);
    nbLayers   = length(this.DataLayer)/nbDataType;
    for kLayer=1:nbLayers
        for kDataType=1:nbDataType
            sub = find(DataType == listDataType(kDataType));
            DataLayer(1:length(sub),kDataType,1) = this.DataLayer(sub); %#ok<AGROW>
        end
    end
end

% Fin des affectations de M apr�s r�organisation.
DimDataLayer(1).Name  = 'nbLayers';
DimDataLayer(1).Value = nbLayers;
DimDataLayer(2).Name  = 'nbDataType';
DimDataLayer(2).Value = nbDataType;
DimDataLayer(3).Name  = 'nbCycles';
DimDataLayer(3).Value = length(sub);
set(M, 'DimDataLayer', DimDataLayer);
set(M, 'DataLayer',    DataLayer);

% R�assignation des layers d'index.
set(M, 'DataIndex',    this.DataIndex(:));
set(M, 'DimDataIndex', this.DimDataIndex);
