% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   [a, flag] = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_multidata
%
% Name-Value Pair Arguments
%  Cf. cl_multidata
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   a  : Instance de la classe
%   flag : 1=OK, 0=KO
%
% Examples
%   nomFic      = getNomFicDatabase('lena.tif');
%   I           = imread(nomFic);
%   a           = cl_image('Image', Lena)
%   timeData    = repmat(now, 256,256);
%   idx         = cl_image('Image', timeData);
%   m           = cl_multidata('DataLayer', a)
%   set(m, 'DataIndex', timeData)
%
% See also cl_multidata cl_multidata/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = set(this, varargin)

flag = 1;

%% On v�rifie que l'instance est bien de demension 1

if length(this) ~= 1
    my_warndlg('cl_multidata/set : L''instance doit etre de dimension 1', 1);
    flag = 0;
    return
end

[varargin, X] = getPropertyValue(varargin, 'Name', []);
if ~isempty(X)
    this.Name = X;
end

[varargin, X] = getPropertyValue(varargin, 'DataLayer', []);
if ~isempty(X)
    this.DataLayer = X;
end

[varargin, X] = getPropertyValue(varargin, 'DimDataLayer', []);
if ~isempty(X)
    this.DimDataLayer = X;
end

[varargin, X] = getPropertyValue(varargin, 'DataIndex', []);
if ~isempty(X)
    this.DataIndex = X;
end

[varargin, X] = getPropertyValue(varargin, 'DimDataIndex', []); %#ok<ASGLU>
if ~isempty(X)
    this.DimDataIndex = X;
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
end
