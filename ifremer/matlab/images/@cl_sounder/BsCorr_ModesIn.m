% See also BsCorr_ModesOut Authors

function [Mode_1, ListeMode_2] = BsCorr_ModesIn(~, pingMode, swathMode)

switch swathMode
    case 1 % Single swath
        Mode_1 = 1;
        % TODO : y'a moyen d'automatiser �a grace
        % Mode2.Num mais j'ai la flemme
        switch pingMode
            case 1
                ListeMode_2 = 1;
            case 2
                ListeMode_2 = 2;
            case 3
                ListeMode_2 = 3;
            case 4
                ListeMode_2 = 4:5;
            case 5
                ListeMode_2 = 6:7;
            case 6
                ListeMode_2 = 8:9;
            case 7
                ListeMode_2 = 10;
            otherwise
                ListeMode_2 = [];
                displayMessage(swathMode, pingMode)
        end
    case {2; 3} % Dual swath
        Mode_1 = 2;
        switch pingMode
            case 1
                ListeMode_2 = 1;
            case 2
                ListeMode_2 = 2;
            case 3
                ListeMode_2 = 3;
            case 4
                ListeMode_2 = 4:5;
            case 5
                ListeMode_2 = 6:7;
            case 6
                ListeMode_2 = 8:9;
            case 7
                ListeMode_2 = 10;
            otherwise
                ListeMode_2 = [];
                displayMessage(swathMode, pingMode)
        end
    otherwise
        Mode_1 = [];
        ListeMode_2 = [];
        displayMessage(swathMode, pingMode)
end

function displayMessage(swathMode, pingMode)
str1 = sprintf('Erreur dans la fonction "BsCorr_ModesIn", swathMode=%d, pingMode=%d. Transmettez ce message � sonarscope@ifremer.fr SVP', swathMode, pingMode);
str2 = sprintf('Error in function "BsCorr_ModesIn", swathMode=%d,  pingMode=%d. Please refer to sonarscope@ifremer.fr', swathMode, pingMode);
my_warndlg(Lang(str1,str2), 1);
