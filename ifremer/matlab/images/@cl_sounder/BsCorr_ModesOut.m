% See also BsCorr_ModesIn Authors

function [pingMode, swathMode, Waveform] = BsCorr_ModesOut(~, Mode_1, Mode_2)

Waveform = 'CW';
switch Mode_1
    case 1
        swathMode = 1;
        switch Mode_2
            case 1
                pingMode = 1; % Very Shallow
            case 2
                pingMode = 2; % Shallow
            case 3
                pingMode = 3; % Medium
            case 4
                pingMode = 4; % Deep CW
            case 5
                pingMode = 4; % Deep FM
            case 6
                pingMode = 5; % Very Deep CW
            case 7
                pingMode = 5; % Very Deep FM
                Waveform = 'FM';
            case 8
                pingMode = 6; % Extra Deep CW
            case 9
                pingMode = 6; % Extra Deep FM
                Waveform = 'FM';
            otherwise
                pingMode = [];
                displayMessage(Mode_1, Mode_2)
        end
    case 2
        swathMode = 2;
        switch Mode_2
            case 1
                pingMode = 1; % Very Shallow
            case 2
                pingMode = 2; % Shallow
            case 3
                pingMode = 3; % Medium
            case 4
                pingMode = 4; % Deep CW
            case 5
                pingMode = 4; % Deep FM
                Waveform = 'FM';
            case 6
                pingMode = 5; % Very Deep CW
            case 7
                pingMode = 5; % Very Deep FM
                Waveform = 'FM';
            case 8
                pingMode = 6; % Extra Deep CW
            case 9
                pingMode = 6; % Extra Deep FM
                Waveform = 'FM';
            otherwise
                pingMode = [];
                displayMessage(Mode_1, Mode_2)
        end
    otherwise
        swathMode = [];
        displayMessage(Mode_1, Mode_2)
end

function displayMessage(Mode_1, Mode_2)
str1 = sprintf('Erreur dans la fonction "BsCorr_ModesOut", Mode_1=%d, Mode_2=%d. Transmettez ce message � sonarscope@ifremer.fr SVP', Mode_1, Mode_2);
str2 = sprintf('Error in function "BsCorr_ModesOut", Mode_1=%d,  Mode_2=%d. Please refer to sonarscope@ifremer.fr', Mode_1, Mode_2);
my_warndlg(Lang(str1,str2), 1);

