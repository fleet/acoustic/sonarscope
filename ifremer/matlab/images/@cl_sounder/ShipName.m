function nom = ShipName(~, SystemSerialNumber)

% -------------------------------------------------------------
% NE PAS OUBLIER DE RENSEIGNER AUSSI cl_simrad_all/read_runtime
% -------------------------------------------------------------

switch SystemSerialNumber
    case 99
        nom = 'Tangaroa';
    case 100
        nom = 'KONGSBERG ??????';
    case 101
        nom = 'KONGSBERG ME70';
    case 102
        nom = 'Beautemps-Beaupr�';
    case 106
        nom = 'Franklin';
    case 131
        nom = 'UNH vessels';
    case 203
        nom = 'Suroit';
    case 271
        nom = 'ALis';
    case 272
        nom = 'Belgica';
    case 277
        nom = 'Beautemps-Beaupr�';
    case 295
        nom = 'Frederick G Creed';
    case 307
        nom = 'Tangaroa';
    case 329
        nom = 'Tangaroa ??????????????';
    case 330
        nom = 'Tangaroa ??????????????';
    case 390
        nom = 'UNH Conference';
    case 507
        nom = 'Belgica'; % ?????????????????????? 271 est le 1002 du Belgica
    case 510
        nom = 'Borda';
    case 593
        nom = 'Belgica';
    case 953
        nom = 'Geosciences-Azur ROV';
    otherwise
        nom = 'Unknown';
end

