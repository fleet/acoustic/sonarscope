function str = char(this, varargin)
str = [];
[m, n] = size(this);

if (m*n) > 1
    for i=1:m
        for j=1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok
            str1 = char(this(i, j), varargin{:});
            str{end+1} = str1; %#ok
        end
    end
    str = cell2str(str);
else
    if (m*n) == 0
        str = '';
        return
    end
    
    %% Instance unique
    
    str = char_instance(this, varargin{:});
    str = [repmat(' ', size(str,1), 2) str];
end

