% Classe repr�sentant un sondeur et ses param�tres
%
% Syntax
%   a = cl_sounder(...)
%
% Name-only Arguments
%    :
%
% Output Arguments
%   a : une instance de cl_sounder
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('paramsFileName', 'EM2000.xml')
%   a = cl_sounder('paramsFileName', 'EM2000.xml', 'Sonar.Mode_1', 2)
%   a = cl_sounder('paramsFileName', 'EM2000.xml', 'Sonar.Mode_1', 1, 'Sonar.Mode_2', 2)
%
% See also Authors
% Authors : JMA

classdef cl_sounder
    
    properties (GetAccess='public', SetAccess='public')
        paramsFileName = '';   % Nom complet du fichier XML de definition des param�tres sondeurs
        IsGood         = false;    % Sert a indiquer si l'initialisation s'est bien passee
        str            = [];       % chaine de caract�re descriptive du sondeur
        Sonar          = [];       % Description g�n�rale du sondeur
        Signal         = [];       % Description du type de signal utilis�
        Array          = [];       % Description des antennes d'emmission-reception
        BeamForm       = [];       % Description du type de formation de voies
        Proc           = [];       % Description du du post-processing
        Survey         = [];       % Description du survey
        Ship           = [];       % description du navire porteur
        Emission       = [];       % description des diagrammes d'emmission
        Reception      = [];       % description des diagrammes de reception
        Display        = 1;
    end
    
    
    methods (Access='public')
        
        function this = cl_sounder(varargin)
            % Constructeur de la classe cl_sounder :
            %   sounder = cl_sounder('paramsFileName', 'EM2000.xml');
            % ----------------------
            
            % Initialisation des propri�t�s par d�faut
            % Pour le moment : m�mes propri�t�s que l'objet cl_sounder
            this.IsGood         = 1;    % Sert a indiquer si l'initialisation s'est bien passee
            
            % -- STR --
            this.str.DirPat     = {'Function'; 'Table'};
            this.str.ArrayType  = {'Rectangular'; 'Linear'; 'Cylinder'};
            this.str.Shad       = {'None'; 'Chebychev'};
            this.str.Display    = {'Light'; 'Medium'; 'Full'};
            this.str.Family     = {'Sonar'; 'Multibeam'; 'Subbottom'; 'Fishing'; 'ADCP'};
            this.str.Version    = [];
            this.str.SonarNames = {  'EM12D'
                'EM12S'
                'EM300'
                'EM1000'
                'SAR'
                'EM3000D'
                'EM3000S'
                'DF1000'
                'DTS1'
                'EM1002'
                'EM120'
                'Reson7150_12kHz'
                'Reson7150_24kHz'
                'Reson7111'
                'Reson7125_200kHz'
                'GeoSwath'
                'ME70'
                'EM710'
                'EM3002D'
                'EM3002S'
                'EM2000'
                'EM302'
                'EM122'
                'EM2040S'
                'EM2045'
                'Subbottom'
                'Klein3000'
                'EK60'
                'HAC_Generic'
                'ADCP'
                'EM2040D'
                'EM304'
                'EM712'
                'Norbit'};
            
            % -- Sonar --
            this.Sonar.Ident              = 0;
            this.Sonar.Constructeur       = '';
            this.Sonar.strVersion         = []; % Uniquement EM300
            this.Sonar.Version            = []; % Uniquement EM300
            this.Sonar.SystemSerialNumber = -1;
            this.Sonar.Family             = 1;
            this.Sonar.strMode_1          = {''};
            this.Sonar.strMode_2          = {''};
            this.Sonar.strMode_3          = {''};  % Ajout GLA
            this.Sonar.TypeMode_1         = [];
            this.Sonar.TypeMode_2         = [];
            this.Sonar.TypeMode_3         = [];
            this.Sonar.Mode_1             = 1;
            this.Sonar.Mode_2             = 1;
            this.Sonar.Mode_3             = 1; % Ajout GLA
            this.Sonar.Freq               = 0;
            this.Sonar.BeamNb             = 0;
            this.Sonar.ApertWth           = 0;
            this.Sonar.TxBeamWth          = 0;
            this.Sonar.LongBeamWth        = 0;
            this.Sonar.TransBeamWth       = 0;
            this.Sonar.MaxDepth           = 0;
            
            % -- Signal --
            this.Signal.strType         = {'CW'; 'Chirp'};
            this.Signal.Type            = 1;
            this.Signal.Freq            = [];   % Frequence en kHz
            this.Signal.Delay           = 0;
            this.Signal.Duration        = [];   % Duree d'impulsion e, ms
            this.Signal.BandWth         = [];   % Largeur de bande en kHz
            this.Signal.Level           = [];   % Niveau d'emission en dB
            this.Signal.GT              = [];   % Gain en sortie des antennes de reception en dB
            this.Signal.SH              = [];   % Sensibilite des antennes en dB
            this.Signal.WaveLength      = [];   % Longueur d'onde en m
            this.Signal.DistanceFresnel = [];   % Distance de Fresnel en m
            this.Signal.ChampLointain   = [];   % Distance de champ lointain en m
            
            % -- Array --
            this.Array.Tx.Type        = 1; %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
            this.Array.Tx.Lth         = 0;
            this.Array.Tx.Wth         = 0;
            this.Array.Tx.TransTilt   = 0;
            this.Array.Tx.TransDirPat = 1;
            this.Array.Rx.Type        = 1; % {'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
            this.Array.Rx.Lth         = 0;
            this.Array.Rx.Wth         = 0;
            this.Array.Rx.TransTilt   = 0;
            this.Array.Rx.TransDirPat = 1;
            
            % -- BeamForm -- Caract�ristiques des faisceaux
            this.BeamForm.Tx.LongWth         = 0;
            this.BeamForm.Tx.TransWth        = 0;
            this.BeamForm.Tx.LongShad        = 1;
            this.BeamForm.Tx.LongShadParams  = [];
            this.BeamForm.Tx.TransShad       = 1;
            this.BeamForm.Tx.TransShadParams = [];
            this.BeamForm.Tx.Nb              = 0;
            this.BeamForm.Tx.CentAng         = [];
            this.BeamForm.Tx.LimitAng        = [];
            this.BeamForm.Tx.Freq            = []; % Frequence du faisceau si different de                     this.Signal.Freq
            this.BeamForm.Tx.Level           = []; % Niveau d'emission par faisceau si different de            this.Signal.Level
            this.BeamForm.Tx.BandWth         = []; % Largeur de bande par faisceau d'emission si different de  this.Signal.BandWth
            this.BeamForm.Tx.Duration        = []; % Duree d'impulsion par faisceau d'emission si different de this.Signal.Duration
            this.BeamForm.Tx.SH              = []; % Sensibilite des antennes si different de                  this.Signal.SH
            this.BeamForm.Tx.TransDirPat     = 1;
            
            this.BeamForm.Rx.LongWth         = 0;
            this.BeamForm.Rx.TransWth        = 0;
            this.BeamForm.Rx.LongShad        = 1;
            this.BeamForm.Rx.LongShadParams  = [];
            this.BeamForm.Rx.TransShad       = 1;
            this.BeamForm.Rx.TransShadParams = [];
            this.BeamForm.Rx.Nb              = 0;
            this.BeamForm.Rx.strRepart       = {'Isodistance'; 'IsoAngle'; 'InBetween'};
            this.BeamForm.Rx.Repart          = 1;
            this.BeamForm.Rx.Angles          = [];
            this.BeamForm.Rx.GT              = [];   % Gain en sortie des antennes de reception en dB si different de this.Signal.GT
            
            % -- Proc --
            % this.Proc.TVGLaw                    = [];
            this.Proc.TVGLaw.ConstructAlpha       = [];
            this.Proc.TVGLaw.ConstructConstante   = [];
            this.Proc.TVGLaw.ConstructCoefDiverg  = [];
            this.Proc.TVGLaw.ConstructTypeCompens = [];
            this.Proc.TVGLaw.ConstructTable       = [];
            
            this.Proc.SampFreq       = 0;
            this.Proc.RangeResol     = 0;
            this.Proc.ShipNoiseLevel = 0;
            this.Proc.RecBandWth     = 0;
            this.Proc.SoundNbPerPing = 0;
            this.Proc.SoundNbPerBeam = 0;
            this.Proc.SampNbPerSound = 0;
            
            % -- Survey --
            this.Survey.Name = '';
            
            % -- Ship --
            this.Ship.Name              = '';
            this.Ship.MRU.Name          = '';
            this.Ship.MRU.HeadAccuracy  = '';
            this.Ship.MRU.PitchAccuracy = '';
            this.Ship.MRU.RollAccuracy  = '';
            this.Ship.MRU.HeaveAccuracy = '';
            this.Ship.MRU.YawAccuracy   = '';
            this.Ship.selfNoise         = 0;
            this.Ship.sonarHeadDepth    = 0;
            
            % -- Emission --
            model = ClModel.empty;
            this.Emission.ModelsConstructeur = {}; % ClModel;
            this.Emission.ModelsCalibration  = {}; % ClModel;
            
            % -- Reception --
            this.Reception.ModelsConstructeur = model;
            this.Reception.ModelsCalibration  = model;
            
            this.Display = 1;
            
            
            if isempty(varargin) || isempty(varargin{1})
                return
            end
            
            % Test si construction sans initialisation -> on sort
            if nargin > 0
                this = set(this, varargin{:});
            else
                this.IsGood = false;
            end
        end % constructor cl_sounder
        
    end %methods definition
   
end %class defition
