function varargout = diagramme_Rx(this, E, varargin)
% Calcul du diagramme d'emission
%
% Syntax
%   R = diagramme_Rx(a, E, ...)
%
% INTPUT PARAMETERS :
%   a : Une instance de cl_sounder
%   E : Angle d'emission (en degres par defaut)
%
% Name-Value Pair Arguments
%   OrigineModeles : {1=Constructeur} | 2=Calibration
%   TypeDiagramme  :
%   ReceptionBeam  : [] ou instance de cl_image contenant le numero de faisceau
%
% Name-only Arguments
%   radian : Les angles d'emission sont en radian
%
% OUTTPUT PARAMETERS :
%   R : Resolution longitudinale (m)
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   R = diagramme_Rx(a, -75:75)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

[varargin, OrigineModeles] = getPropertyValue(varargin, 'OrigineModeles', 1);
[varargin, TypeDiagramme]  = getPropertyValue(varargin, 'TypeDiagramme', 1);
[varargin, ReceptionBeam]  = getPropertyValue(varargin, 'RxBeamIndex', []);
% [varargin, Mode] = getPropertyValue(varargin, 'Mode', []);

[varargin, flag] = getFlag(varargin, 'radian'); %#ok<ASGLU>

if flag
    E = E * (180 / pi);
end
G = NaN(size(E), 'single');

if isempty(ReceptionBeam)
    ReceptionBeamEstime = NaN(size(E), 'single');
else
    ReceptionBeamEstime = [];
    ReceptionBeam = get_Image_inXLimYLim(ReceptionBeam);
end

for iModel=1:length(this.Reception.ModelsConstructeur)
    Modele = this.Reception.ModelsConstructeur(iModel);
    XData = get(Modele, 'XData');
    LimitAng(iModel, 1) = min(XData); %#ok
    LimitAng(iModel, 2) = max(XData); %#ok
end

for iModel=1:(size(LimitAng,1)-1)
    Milieu = (LimitAng(iModel,2) + LimitAng(iModel+1, 1)) / 2;
    LimitAng(iModel,   2) = Milieu;
    LimitAng(iModel+1, 1) = Milieu;
end

for iModel=1:length(this.Reception.ModelsConstructeur)
    if OrigineModeles == 1
        Modele = this.Reception.ModelsConstructeur(iModel); %{this.Sonar.Mode_1, this.Sonar.Mode_2};
    else
        Modele = this.Reception.ModelsCalibration(iModel); %{this.Sonar.Mode_1, this.Sonar.Mode_2};
    end
    
    if isempty(ReceptionBeam)
        sub = find((E >= LimitAng(iModel, 1)) & (E <= LimitAng(iModel, 2)));
        if isempty(sub)
            continue
        end
        
        Angles = E(sub);
        
        if TypeDiagramme == 1
            gain = compute(Modele, 'x', Angles');   % ' car AntenneSincDep est mal foutu
            G(sub) = gain';
        else
            gain = interp(Modele, Angles);
            G(sub) = gain;
        end
        ReceptionBeamEstime(sub) = iModel;
        
    else
        
        ReceptionBeam(ReceptionBeam < 1) = NaN; % Pour palier a un defaut de cl_car_ima/view qui met valNan a NaN alors que c'est -1
        indMin = min(ReceptionBeam(:));
        indMax = max(ReceptionBeam(:));
        for i=indMin:indMax
            sub = find(ReceptionBeam == i);
            
            Angles = E(sub);
            Modele = Modeles(iModel);
            
            if TypeDiagramme == 1
                gain = compute(Modele, 'x', Angles');   % ' car AntenneSincDep est mal foutu
                G(sub) = gain';
            else
                gain = interp(Modele, Angles');
                G(sub) = gain';
            end
            
            G(sub) = gain';
        end
    end
end

% -----------------
% Par ici la sortie

if nargout == 0
    if OrigineModeles == 1
        strOrigine = 'Constructeur';
    else
        strOrigine = 'Calibration';
    end
    
    [n, m] = size(E);
    if (n == 1) && (m ==1)
        varargout{1} = G;   % on force l'affichage
    elseif (n ~= 1) && (m ~=1)
        figure;
        imagesc(G); colorbar
        title(sprintf('Diagramme de reception %s (dB)', strOrigine))
    else
        figure;
        plot(E, G); grid on
        title(['Diagramme de reception ' strOrigine])
        xlabel('Angle d''emission (m)')
        ylabel('Gain (dB)')
    end
else
    varargout{1} = G;
    varargout{2} = ReceptionBeamEstime;
end
