function varargout = diagramme_Rx_Layer(this, EmissionAngle, RxBeamAngle)
% Calcul du diagramme d'emission
%
% Syntax
%   R = diagramme_Rx_Layer(a, EmissionAngle, RxBeamAngle)
%
% INTPUT PARAMETERS :
%   a             : Une instance de cl_sounder
%   EmissionAngle      : Angle d'emission (en degres par defaut)
%   RxBeamAngle : Angle du faisceau (en degres par defaut)
%
% Name-only Arguments
%   radian : Les angles d'emission sont en radian
%
% OUTTPUT PARAMETERS :
%   R : Resolution longitudinale (m)
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   R = diagramme_Rx_Layer(a,
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

Modeles = this.Reception.ModelsConstructeur;

ModeleBab = Modeles(1);
ModeleTri = Modeles(end);

minvalue = get_minvalue(ModeleBab);
minvalue(2) = -90;
minvalue(4) = -90;
ModeleBab = set_minvalue(ModeleBab, minvalue);
ModeleTri = set_minvalue(ModeleTri, minvalue);

maxvalue = get_maxvalue(ModeleBab);
maxvalue(2) = 90;
maxvalue(4) = 90;
ModeleBab = set_maxvalue(ModeleBab, maxvalue);
ModeleTri = set_maxvalue(ModeleTri, maxvalue);

G = NaN(size(EmissionAngle), 'single');
RxBeamAngle = round(RxBeamAngle * 100) / 100;

listeAngles = unique(RxBeamAngle(~isnan(RxBeamAngle)));

figure, plot(listeAngles, '+'); grid on; title('Tous les angles trouves dans l''image')

for i=1:length(listeAngles)
    sub = find(RxBeamAngle == listeAngles(i));
    
    Angles = EmissionAngle(sub);
    
    if listeAngles(i) < 0
        Modele = ModeleBab;  % Le plus a gauche pour recuperer l'inclinaison antenne babord
    else
        Modele = ModeleTri;  % Le plus a droite pour recuperer l'inclinaison antenne tribord
    end
    
    params = get_valParams(Modele);
    
    if this.Array.Rx.Type == 3 % Forme de l'antenne : 1=Rectangular' | {'2=Linear'} | '3=Cylinder'
        params(4) = listeAngles(i);
    end
    
    params(2) = listeAngles(i);
    Modele = set_valParams(Modele, params);
    gain = compute(Modele, 'x', Angles');   % ' car AntenneSincDep est mal foutu
    G(sub) = gain';
    
    %     figure(567); plot(Angles, gain, '+'); grid on;
    %     drawnow
    %     i
end

varargout{1} = G;

