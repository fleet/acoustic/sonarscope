% Calcul du diagramme d'�mission
%
% Syntax
%   R = diagramme_Tx(a, E, ...)
%
% INTPUT PARAMETERS :
%   a : Une instance de cl_sounder
%   E : Angle d'emission (en degres par defaut)
%
% Name-only Arguments
%   radian : Les angles d'emission sont en radian
%
% OUTTPUT PARAMETERS :
%   R : Resolution longitudinale (m)
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   R = diagramme_Tx(a, -75:75)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = diagramme_Tx(this, E, varargin)

% pragma(s) ajout� par JMA le 23/04/2020 car bug en version compil�e pour fichier VLIZ
%#function AntenneSpline AntenneSincDep AntennePoly AntenneCentraleSimrad AntenneEM3002

[varargin, OrigineModeles] = getPropertyValue(varargin, 'OrigineModeles', 1);
[varargin, TypeDiagramme]  = getPropertyValue(varargin, 'TypeDiagramme',  1);
[varargin, EmissionBeam]   = getPropertyValue(varargin, 'TxBeamIndex',    []);
[varargin, Mode]           = getPropertyValue(varargin, 'Mode',           []);

[varargin, flag] = getFlag(varargin, 'radian'); %#ok<ASGLU>

if flag
    E = E * (180 / pi);
end
G = NaN(size(E), 'single');

%% Inventaire des modes pr�sents dans l'image

if isempty(Mode)
    if strcmp(this.Sonar.TypeMode_1, 'SounderMode')
        listeModes = this.Sonar.Mode_1;
    elseif strcmp(this.Sonar.TypeMode_2, 'SounderMode')
        listeModes = this.Sonar.Mode_1;
    else
%         my_warndlg('Gast, c''est embarassant !', 1);
        listeModes = 1; % Modif JMA pour donn�es ME70
    end
else
    listeModes = unique(Mode);
end
listeModes(listeModes == 0) = [];

if length(listeModes) == 1
    ImageMode = [];
else
    ImageMode = repmat(Mode(:), 1, size(E,2));
end

Mode1In = get(this, 'Sonar.Mode_1');
Mode2In = get(this, 'Sonar.Mode_2');
for iMode=1:length(listeModes)
    
    % Comment� par JMA le 28/10/2015
    %{
if strcmp(this.Sonar.TypeMode_1, 'SounderMode')
this = set(this, 'Sonar.Mode_1', listeModes(iMode));
elseif strcmp(this.Sonar.TypeMode_2, 'SounderMode')
this = set(this, 'Sonar.Mode_1', Mode1In, 'Sonar.Mode_2', listeModes(iMode));
else
my_warndlg('Gast, c''est embarassant, y''a pas de type de mode d�fini dans le XML pour ce sondeur !', 0, 'Tag', 'Beurk');
end
    %}
    
    if OrigineModeles == 1
        if isempty(this.Sonar.Mode_2)
            Modeles = this.Emission.ModelsConstructeur{this.Sonar.Mode_1};
        else
            Modeles = this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2};
        end
    else
        if isempty(this.Sonar.Mode_2)
            Modeles = this.Emission.ModelsCalibration{this.Sonar.Mode_1};
        else
            Modeles = this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2};
        end
    end
    
    if isempty(EmissionBeam)
        LimitAng = this.BeamForm.Tx.LimitAng;
        for k= 1:size(LimitAng, 1)
            Modele = Modeles(k);
            
            if isempty(ImageMode)
                sub = find((E >= LimitAng(k,1)) & (E <= LimitAng(k,2)));
            else
                sub = find((E >= LimitAng(k,1)) & (E <= LimitAng(k,2)) & (ImageMode == listeModes(iMode)));
            end
            Angles = E(sub);
            
            if (TypeDiagramme == 1) || ((TypeDiagramme == 2) && strcmp(nomModele, 'AntenneSpline'))
                gain = compute(Modele, 'x', Angles');   % ' car AntenneSincDep est mal foutu
                G(sub) = gain';
            else
                gain = interp(Modele, Angles');
                G(sub) = gain';
            end
        end
    else
        %     'Message de JMA pour JMA : cl_sounder/diagramme_Tx : +1 ici ?????'
        %     EmissionBeam = get(EmissionBeam, 'Image');% + 1;
        EmissionBeam(EmissionBeam < 1) = NaN; % Pour palier a un defaut de cl_car_ima/view qui met valNan a NaN alors que c'est -1
        indMin = min(EmissionBeam(:));
        indMax = max(EmissionBeam(:));
        for k=indMin:indMax
            if k > length(Modeles)
                break
            end
            
            Modele = Modeles(k);
            
            if isempty(ImageMode)
                sub = find((EmissionBeam == k) & ~isnan(E));
            else
                sub = find((EmissionBeam == k) & ~isnan(E) & (ImageMode == listeModes(iMode)));
            end
            
            if isempty(sub) % Cas o� il n'y a pas de donn�es dans le secteur
                continue
            end
            
            Angles = E(sub);
            
            nomModele = Modele.Name;
            
            if TypeDiagramme == 1
                if isempty(nomModele)
                    G(sub) = NaN; % Cas rencontr� par Anne-Laure sur fichier 0180_20070425_151725_raw.all
                    % o� son fichier de compensation ne donnait que des mod�les pour les faisceaux d'�mission 2 � 8
                    % alors que la donn�e comporte des pixels pour lesquels TxBEamIndex est �gal � 1
                else
                    gain = Modele.compute('x', Angles');
                    G(sub) = gain';
                end
            else
                gain = Modele.interp(Angles);
                if isempty(gain)
                    my_warndlg('No Nodes definied for this Model', 1);
                    varargout{1} = [];
                    varargout{2} = 0;
                    return
                end
                G(sub) = gain;
            end
        end
    end
end

this = set(this, 'Sonar.Mode_1', Mode1In, 'Sonar.Mode_2', Mode2In); %#ok<NASGU>

%% Par ici la sortie

if nargout == 0
    if OrigineModeles == 1
        strOrigine = 'Constructeur';
    else
        strOrigine = 'Calibration';
    end
    
    [n, m] = size(E);
    if (n == 1) && (m ==1)
        varargout{1} = G;   % on force l'affichage
        varargout{2} = 1;
    elseif (n ~= 1) && (m ~=1)
        figure;
        imagesc(G); colorbar
        title(sprintf('Tx Beam diagrams %s (dB)', strOrigine))
    else
        FigUtils.createSScFigure;
        PlotUtils.createSScPlot(E, G); grid on
        title(['TxBeam diagrams ' strOrigine])
        xlabel('Angles (deg)')
        ylabel('Gain (dB)')
    end
else
    varargout{1} = G;
    varargout{2} = 1;
end
