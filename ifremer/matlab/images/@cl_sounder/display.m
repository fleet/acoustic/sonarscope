% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : instance de la classe cl_sounder
%
% Examples
%   a = cl_sounder;
%   display(a)
%
% See also cla_param cl_sounder cl_sounder/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

% --------------------------------------------
% Lecture et affichage des infos de l'instance

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char_instance(this));
