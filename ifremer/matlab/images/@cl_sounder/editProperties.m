% a = cl_sounder('paramsFileName', 'EM2000.xml')
% [peer, a] = editProperties(a);

function [Parent, this] = editProperties(this, varargin)

[varargin, Parent]      = getPropertyValue(varargin, 'Parent', []);
[varargin, WindowStyle] = getPropertyValue(varargin, 'WindowStyle', 'normal'); %#ok<ASGLU>

com.mathworks.mwswing.MJUtilities.initJIDE;

if isempty(Parent)
    Parent = java.util.ArrayList();
    Fig = true;
else
    Fig = false;
end

for k=1:length(this)
    p1 = com.jidesoft.grid.DefaultProperty();
    p1.setName(['cl_sounder(' num2str(k) ')']);
    p1.setEditable(false);
    
    if Fig
        Parent.add(p1);
    else
        Parent.addChild(p1);
    end
    
    %% Sous-ensemble Sonar
    
    if this(k).Sonar.Ident ~= 0
        p2 = com.jidesoft.grid.DefaultProperty();
        p2.setName('Sonar');
        p2.setEditable(false);
        p1.addChild(p2);
        
        %% Sonar.Type
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.Ident');
        p3.setType(javaclass('char',1));
        p3.setValue([num2str(this(k).Sonar.Ident) ' (' this(k).str.SonarNames{this(k).Sonar.Ident} ')']);
        p3.setCategory('Sounder properties');
        p3.setDisplayName('Sonar.Ident');
        p3.setDescription('Identifiant type of sounder : EM1000, 7125 ...');
        p3.setEditable(false);
        p2.addChild(p3);
        
        options0 = this(k).str.SonarNames;
        editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
        context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
        com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.Name');
        p3.setType(javaclass('char',1));
        p3.setValue(this(k).str.SonarNames{this(k).Sonar.Ident});
        p3.setCategory('Sonar properties');
        p3.setEditorContext(context0);
        p3.setDisplayName('Sonar.Name');
        p3.setDescription('Type of sounder : EM1000, 7125 ...');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Sonar.SystemSerialNumber
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.SystemSerialNumber');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.SystemSerialNumber));
        p3.setCategory('Sonar.SystemSerialNumber');
        p3.setDisplayName('Sonar System Serial Number');
        p3.setDescription('Sonar System Serial Number SSN');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Sonar.Family
        
        options0 = this(k).str.Family;
        editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
        context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
        com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.Family');
        p3.setType(javaclass('char',1));
        p3.setValue([num2str(this(k).Sonar.Family) ' (' this(k).str.Family{this(k).Sonar.Family} ')']);
        p3.setEditorContext(context0);
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar.Family');
        p3.setDescription('Sonar.Family');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Sonar Mode_1
        
        if ~isempty(this(k).Sonar.Mode_1)
            options0 = this(k).Sonar.strMode_1;
            editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
            context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
            com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
        else
            context0 = [];
        end
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.Mode_1');
        p3.setType(javaclass('char',1));
        p3.setValue([num2str(this(k).Sonar.Mode_1) ' (' this(k).Sonar.strMode_1{this(k).Sonar.Mode_1} ')']);
        p3.setEditorContext(context0);
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar.Mode_1');
        p3.setDescription('Sonar Mode_1');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Sonar.Mode_2
        
        if ~isempty(this(k).Sonar.Mode_2)
            options0 = this(k).Sonar.strMode_2;
            editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
            context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
            com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
        else
            context0 = [];
        end
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.Mode_2');
        p3.setType(javaclass('char',1));
        p3.setValue([num2str(this(k).Sonar.Mode_2) ' (' this(k).Sonar.strMode_2{this(k).Sonar.Mode_2} ')']);
        p3.setEditorContext(context0);
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar.Mode_2');
        p3.setDescription('Sonar Mode_2');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Sonar.Type Mode_1
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.TypeMode_1');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.TypeMode_1));
        p3.setEditorContext(context0);
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar.TypeMode_1');
        p3.setDescription('Sonar TypeMode_1');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Sonar.Type Mode_2
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.TypeMode_2');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.TypeMode_2));
        p3.setEditorContext(context0);
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar.TypeMode_2');
        p3.setDescription('Sonar TypeMode_2');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar.Type Mode_3
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.TypeMode_3');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.TypeMode_3));
        p3.setEditorContext(context0);
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar.TypeMode_3');
        p3.setDescription('Sonar TypeMode_3');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar.Freq
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.Freq');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.Freq));
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar Frequency (kHz)');
        p3.setDescription('Sonar Frequency in Khz');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar.BeamNb
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.BeamNb');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.BeamNb));
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar.BeamNb');
        p3.setDescription('Sonar Beam Number');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar.Aperture Width
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.ApertWth');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.ApertWth));
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar Aperture Width (deg)');
        p3.setDescription('Sonar Aperture Width (deg)');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar Tx Beam Width
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.TxBeamWth');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.TxBeamWth));
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar Tx Beam Width (deg)');
        p3.setDescription('Sonar Tx Beam Width in deg');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar.LongBeamWth
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.LongBeamWth');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.LongBeamWth));
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar Long. Beam Width (deg)');
        p3.setDescription('Sonar Longitudinal Beam Width in Khz');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar.TransBeamWth
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.TransBeamWth');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.TransBeamWth));
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar Trans. Beam Width (deg)');
        p3.setDescription('Sonar Transversal Beam Width in Khz');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sonar.MaxDepth
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Sonar.MaxDepth');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Sonar.MaxDepth));
        p3.setCategory('Sonar Properties');
        p3.setDisplayName('Sonar Max. Depth (m)');
        p3.setDescription('Sonar Maximum Depth in Meters');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Sous-ensemble Signal
        
        p2 = com.jidesoft.grid.DefaultProperty();
        p2.setName('Signal');
        p2.setEditable(false);
        p1.addChild(p2);
        
        %% Signal.Type
        
        options0 = this(k).Signal.strType;
        editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
        context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
        com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.Type');
        p3.setType(javaclass('char',1));
        p3.setValue(this(k).Signal.strType{this(k).Signal.Type});
        p3.setCategory('Signal properties');
        p3.setEditorContext(context0);
        p3.setDisplayName('Signal.Name');
        p3.setDescription('Identifiant name of signal : 1=CW | 2=Chirp');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Signal.Frequency
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.Freq');
        p3.setType(javaclass('char',1));
        if isempty(this(k).Signal.Freq)
            p3.setValue('Defined in BeamForm.Tx.Freq');
        else
            p3.setValue(num2str(this(k).Signal.Freq));
        end
        p3.setCategory('Signal properties');
        p3.setDisplayName('Signal.Freq (kHz)');
        p3.setDescription('Signal Frequency in kHz');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Signal.WaveLength
        
        if isempty(this(k).Signal.WaveLength) % Test si Subbottom pas encore bien int�gr� dans l'objet
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName('Signal.WaveLength');
            p3.setType(javaclass('char',1));
            p3.setValue(num2str(this(k).Signal.WaveLength));
            p3.setCategory('Signal properties');
            p3.setDisplayName('Signal.WaveLength (m)');
            p3.setDescription('Signal WaveLength in m');
            p3.setEditable(true);
            p2.addChild(p3);
            
            % Signal Distance Fresnel
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName('Signal.DistanceFresnel');
            p3.setType(javaclass('char',1));
            p3.setValue(num2str(this(k).Signal.DistanceFresnel));
            p3.setCategory('Signal properties');
            p3.setDisplayName('Signal.DistanceFresnel (m)');
            p3.setDescription('Signal Distance de Fresnel in m');
            p3.setEditable(true);
            p2.addChild(p3);
            
            % Signal Champ Lointain
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName('Signal.ChampLointain');
            p3.setType(javaclass('char',1));
            p3.setValue(num2str(this(k).Signal.ChampLointain));
            p3.setCategory('Signal properties');
            p3.setDisplayName('Signal.ChampLointain (m)');
            p3.setDescription('Signal Champ Lointain in m');
            p3.setEditable(true);
            p2.addChild(p3);
        end
        
        %% Signal.Duration
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.Duration');
        p3.setType(javaclass('char',1));
        if isempty(this(k).Signal.Duration)
            p3.setValue('Defined in BeamForm.Tx.Duration');
        else
            p3.setValue(num2str(this(k).Signal.Duration));
        end
        p3.setCategory('Signal properties');
        p3.setDisplayName('Signal.Duration (ms)');
        p3.setDescription('Signal Duration in ms');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Signal.BandWth
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.BandWth');
        p3.setType(javaclass('char',1));
        if isempty(this(k).Signal.BandWth)
            p3.setValue('Defined in BeamForm.Tx.BandWth');
        else
            p3.setValue(num2str(this(k).Signal.BandWth));
        end
        p3.setCategory('Signal properties');
        p3.setDisplayName('Signal.BandWth (kHz)');
        p3.setDescription('Signal Band Width in kHz');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Signal.Level
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.Level');
        p3.setType(javaclass('char',1));
        if isempty(this(k).Signal.Level)
            p3.setValue('Defined in BeamForm.Tx.Level');
        else
            p3.setValue(num2str(this(k).Signal.Level));
        end
        p3.setCategory('Signal properties');
        p3.setDisplayName('Signal.Level (dB/micro P/1m)');
        p3.setDescription('Signal Level in dB/micro P/1m');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Signal.GT
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.GT');
        p3.setType(javaclass('char',1));
        if isempty(this(k).Signal.GT)
            p3.setValue('Defined in BeamForm.Rx.GT');
        else
            p3.setValue(num2str(this(k).Signal.GT));
        end
        p3.setCategory('Signal properties');
        p3.setDisplayName('Signal.GT (dB)');
        p3.setDescription('Signal GT in dB');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Signal.SH
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.SH');
        p3.setType(javaclass('char',1));
        if isempty(this(k).Signal.SH)
            p3.setValue('Defined in BeamForm.Tx.SH');
        else
            p3.setValue(num2str(this(k).Signal.SH));
        end
        p3.setCategory('Signal properties');
        p3.setDisplayName('Signal.SH (dB)');
        p3.setDescription('Signal SH in dB');
        p3.setEditable(true);
        p2.addChild(p3);
        
        %% Signal.Delay
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Signal.Delay');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).Signal.Delay));
        p3.setCategory('Signal properties');
        p3.setDisplayName('Signal.Delay (ms)');
        p3.setDescription('Signal Delay in ms');
        p3.setEditable(true);
        p2.addChild(p3);
    end
    %% Sous-ensemble Array
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Array');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Array.Tx.Type
    
    options0 = this(k).str.ArrayType;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Tx.Type');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.ArrayType{this(k).Array.Tx.Type});
    p3.setCategory('Array properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('Array.Tx.Name');
    p3.setDescription('Identifiant name of Tx Array : 1=Rectangular | 2=Linear | 3=Cylinder');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.Lth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Lth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Array.Tx.Lth));
    p3.setCategory('Array properties');
    p3.setDisplayName('Array.Tx.Lth (m)');
    p3.setDescription('Array Tx Length in m');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.Wth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Wth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Array.Tx.Wth));
    p3.setCategory('Array properties');
    p3.setDisplayName('Array.Tx.Wth (m)');
    p3.setDescription('Array Tx Width in m');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.TransTilt
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.TransTilt');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Array.Tx.TransTilt));
    p3.setCategory('Array properties');
    p3.setDisplayName('Array.Tx.TransTilt (deg)');
    p3.setDescription('Array Tx TransTilt in deg');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.TxTransDirPat
    
    options0 = this(k).str.DirPat;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Tx.TransDirPat');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.DirPat{this(k).Array.Tx.TransDirPat});
    p3.setCategory('Array properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('Array.Tx.TransDirPat');
    p3.setDescription('Identifiant of Tx Transversal Directivity Pattern  : 1=Function | 2=Table');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.Rx.Type
    
    options0 = this(k).str.ArrayType;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Rx.Type');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.ArrayType{this(k).Array.Rx.Type});
    p3.setCategory('Array properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('Array.Rx.Name');
    p3.setDescription('Identifiant name of Rx Array : 1=Rectangular | 2=Linear | 3=Cylinder');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.Lth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Lth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Array.Rx.Lth));
    p3.setCategory('Array properties');
    p3.setDisplayName('Array.Rx.Lth (m)');
    p3.setDescription('Array Rx Length in m');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.Wth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Wth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Array.Rx.Wth));
    p3.setCategory('Array properties');
    p3.setDisplayName('Array.Rx.Wth (m)');
    p3.setDescription('Array Rx Width in m');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.TransTilt
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.TransTilt');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Array.Rx.TransTilt));
    p3.setCategory('Array properties');
    p3.setDisplayName('Array.Rx.TransTilt (deg)');
    p3.setDescription('Array Rx TransTilt in deg');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Array.Rx.TransDirPat
    
    options0 = this(k).str.DirPat;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Array.Rx.TransDirPat');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.DirPat{this(k).Array.Rx.TransDirPat});
    p3.setCategory('Array properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('Array.Rx.TransDirPat');
    p3.setDescription('Identifiant of Rx Transversal Directivity Pattern  : 1=Function | 2=Table');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Sous-ensemble Beamform
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Beamform');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% BeamForm.Tx.LongWth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.LongWth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).BeamForm.Tx.LongWth));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.LongWth (deg)');
    p3.setDescription('BeamForm Tx Longitudinal Width in deg');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.TransWth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.TransWth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).BeamForm.Tx.TransWth));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.TransWth (deg)');
    p3.setDescription('BeamForm Tx Transversal Width in deg');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.LongShad
    
    options0 = this(k).str.Shad;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.LongShad');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.Shad{this(k).BeamForm.Tx.LongShad});
    p3.setCategory('BeamForm properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('BeamForm.Tx.LongShad');
    p3.setDescription('Identifiant of Longitudinal Shad  : 1=None | 2=Chebychev');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.LongShadParams
    
    if this(k).BeamForm.Tx.LongShad ~= 1
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('BeamForm.Tx.LongShadParams');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).BeamForm.Tx.LongShadParams));
        p3.setCategory('BeamForm properties');
        p3.setDisplayName('BeamForm.Tx.LongShadParams');
        p3.setDescription('BeamForm Tx Longitudinal Shad. Params');
        p3.setEditable(true);
        p2.addChild(p3);
    end
    
    %% BeamForm.Tx.TransShad
    
    options0 = this(k).str.Shad;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.TransShad');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.Shad{this(k).BeamForm.Tx.TransShad});
    p3.setCategory('BeamForm properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('BeamForm.Tx.TransShad');
    p3.setDescription('Identifiant of Transversal Shad  : 1=None | 2=Chebychev');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.TransShadParams
    
    if this(k).BeamForm.Tx.TransShad ~= 1
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('BeamForm.Tx.TransShadParams');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).BeamForm.Tx.TransShadParams));
        p3.setCategory('BeamForm properties');
        p3.setDisplayName('BeamForm.Tx.TransShadParams');
        p3.setDescription('BeamForm Tx Transversal Shad. Params');
        p3.setEditable(true);
        p2.addChild(p3);
    end
    
    %% BeamForm.Tx.Nb
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.Nb');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).BeamForm.Tx.Nb));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.Nb');
    p3.setDescription('BeamForm Tx Nb');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.CentAng
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.CentAng');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Tx.CentAng));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.CentAng');
    p3.setDescription('BeamForm Tx Central Angle');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.LimitAng
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.LimitAng');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Tx.LimitAng));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.LimitAng');
    p3.setDescription('BeamForm Tx Limit Angles');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.Freq
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.Freq');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Tx.Freq));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.Freq');
    p3.setDescription('BeamForm Tx Frequency');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.Level
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.Level');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Tx.Level));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.Level');
    p3.setDescription('BeamForm Tx Level');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.BandWth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.BandWth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Tx.BandWth));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.BandWth');
    p3.setDescription('BeamForm Tx BandWidth');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.Duration
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.Duration');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Tx.Duration));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.Duration');
    p3.setDescription('BeamForm Tx Duration');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Tx.SH
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Tx.SH');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Tx.SH));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Tx.SH');
    p3.setDescription('BeamForm Tx SH');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.LongWth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.LongWth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).BeamForm.Rx.LongWth));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Rx.LongWth (deg)');
    p3.setDescription('BeamForm Rx Longitudinal Width in deg');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.TransWth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.TransWth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).BeamForm.Rx.TransWth));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Rx.TransWth (deg)');
    p3.setDescription('BeamForm Rx Transversal Width in deg');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.LongShad
    
    options0 = this(k).str.Shad;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.LongShad');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.Shad{this(k).BeamForm.Rx.LongShad});
    p3.setCategory('BeamForm properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('BeamForm.Rx.LongShad');
    p3.setDescription('Identifiant of Longitudinal Shad  : 1=None | 2=Chebychev');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.LongShadParams
    
    if this(k).BeamForm.Rx.LongShad ~= 1
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('BeamForm.Rx.LongShadParams');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).BeamForm.Rx.LongShadParams));
        p3.setCategory('BeamForm properties');
        p3.setDisplayName('BeamForm.Rx.LongShadParams');
        p3.setDescription('BeamForm Rx Longitudinal Shad. Params');
        p3.setEditable(true);
        p2.addChild(p3);
    end
    
    %% BeamForm.Rx.TransShad
    
    options0 = this(k).str.Shad;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.TransShad');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.Shad{this(k).BeamForm.Rx.TransShad});
    p3.setCategory('BeamForm properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('BeamForm.Rx.TransShad');
    p3.setDescription('Identifiant of Transversal Shad  : 1=None | 2=Chebychev');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.TransShadParams
    
    if this(k).BeamForm.Rx.TransShad ~= 1
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('BeamForm.Rx.TransShadParams');
        p3.setType(javaclass('char',1));
        p3.setValue(num2str(this(k).BeamForm.Rx.TransShadParams));
        p3.setCategory('BeamForm properties');
        p3.setDisplayName('BeamForm.Rx.TransShadParams');
        p3.setDescription('BeamForm Rx Transversal Shad. Params');
        p3.setEditable(true);
        p2.addChild(p3);
    end
    
    %% BeamForm.Rx.Nb
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.Nb');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).BeamForm.Rx.Nb));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Rx.Nb');
    p3.setDescription('BeamForm Rx Nb');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.Repart
    
    options0 = this(k).BeamForm.Rx.strRepart;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.Repart');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).BeamForm.Rx.strRepart{this(k).BeamForm.Rx.Repart});
    p3.setCategory('BeamForm properties');
    p3.setEditorContext(context0);
    p3.setDisplayName('BeamForm.Rx.Repart');
    p3.setDescription('Identifiant of Rx Repartition : 1=Isodistance | 2=IsoAngle | 3=InBetween');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.Angles
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.Angles');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Rx.Angles));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Rx.Angles');
    p3.setDescription('BeamForm Rx Angles');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% BeamForm.Rx.GT
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('BeamForm.Rx.GT');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).BeamForm.Rx.GT));
    p3.setCategory('BeamForm properties');
    p3.setDisplayName('BeamForm.Rx.GT');
    p3.setDescription('BeamForm RX GT');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Sous-ensemble Proc
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Proc');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Proc.TVGLaw
    
    if isempty(this.Proc.TVGLaw)
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Proc.TVGLaw');
        p3.setType(javaclass('char',1));
        p3.setValue('');
        p3.setCategory('Proc properties');
        p3.setDisplayName('Proc.TVGLaw');
        p3.setDescription('Proc TVG Law');
        p3.setEditable(true);
        p2.addChild(p3);
    else
        %% Sous-ensemble de description de Proc.TVGLaw
        
        p3 = com.jidesoft.grid.DefaultProperty();
        p3.setName('Proc');
        p3.setEditable(false);
        p2.addChild(p3);
        
        %% Proc.TVGLaw.ConstructAlpha
        
        p4 = com.jidesoft.grid.DefaultProperty();
        p4.setName('Proc.TVGLaw.ConstructAlpha');
        p4.setType(javaclass('char',1));
        p4.setValue(num2str(this(k).Proc.TVGLaw.ConstructAlpha));
        p4.setCategory('Proc properties');
        p4.setDisplayName('Proc.TVGLaw.ConstructAlpha');
        p4.setDescription('Proc TVG Law - ConstructAlpha ');
        p4.setEditable(true);
        p3.addChild(p4);
        
        %% Proc.TVGLaw.ConstructConstante
        
        p4 = com.jidesoft.grid.DefaultProperty();
        p4.setName('Proc.TVGLaw.ConstructConstante');
        p4.setType(javaclass('char',1));
        p4.setValue(num2str(this(k).Proc.TVGLaw.ConstructConstante));
        p4.setCategory('Proc properties');
        p4.setDisplayName('Proc.TVGLaw.ConstructConstante');
        p4.setDescription('Proc TVG Law - ConstructConstante');
        p4.setEditable(true);
        p3.addChild(p4);
        
        %% Proc.TVGLaw.ConstructCoefDiverg
        
        p4 = com.jidesoft.grid.DefaultProperty();
        p4.setName('Proc.TVGLaw.ConstructCoefDiverg');
        p4.setType(javaclass('char',1));
        p4.setValue(num2str(this(k).Proc.TVGLaw.ConstructCoefDiverg));
        p4.setCategory('Proc properties');
        p4.setDisplayName('Proc.TVGLaw.ConstructCoefDiverg');
        p4.setDescription('Proc TVG Law - ConstructCoefDiverg');
        p4.setEditable(true);
        p3.addChild(p4);
        
        %% Proc.TVGLaw.ConstructTypeCompens
        
        p4 = com.jidesoft.grid.DefaultProperty();
        p4.setName('Proc.TVGLaw.ConstructTypeCompens');
        p4.setType(javaclass('char',1));
        p4.setValue(num2str(this(k).Proc.TVGLaw.ConstructTypeCompens));
        p4.setCategory('Proc properties');
        p4.setDisplayName('Proc.TVGLaw.ConstructTypeCompens');
        p4.setDescription('Proc TVG Law - ConstructTypeCompens');
        p4.setEditable(true);
        p3.addChild(p4);
        
        %% Proc.TVGLaw.ConstructTable
        
        p4 = com.jidesoft.grid.DefaultProperty();
        p4.setName('Proc.TVGLaw.ConstructTable');
        p4.setType(javaclass('char',1));
        p4.setValue(num2strCode(this(k).Proc.TVGLaw.ConstructTable));
        p4.setCategory('Proc properties');
        p4.setDisplayName('Proc.TVGLaw.ConstructTable');
        p4.setDescription('Proc TVG Law - ConstructTable');
        p4.setEditable(true);
        p3.addChild(p4);
    end % Fin de construction du TVGLaw.
    
    %% Proc.SampFreq
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.SampFreq');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Proc.SampFreq));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.SampFreq (kHz)');
    p3.setDescription('Proc Sample Frequency in kHz');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Proc.RangeResol
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.RangeResol');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Proc.RangeResol));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.RangeResol (m)');
    p3.setDescription('Proc Range Resolution in m');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Proc.ShipNoiseLevel
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.ShipNoiseLevel');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Proc.ShipNoiseLevel));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.ShipNoiseLevel (dB)');
    p3.setDescription('Proc Ship Noie Level in dB');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Proc.RangeResol
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.RangeResol');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Proc.RangeResol));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.RangeResol (m)');
    p3.setDescription('Proc Range Resolution in m');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Proc.RecBandWth
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.RecBandWth');
    p3.setType(javaclass('char',1));
    p3.setValue(num2strCode(this(k).Proc.RecBandWth));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.RecBandWth');
    p3.setDescription('Proc Rec BandWidth in ');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Proc.SoundNbPerPing
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.SoundNbPerPing');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Proc.SoundNbPerPing));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.SoundNbPerPing');
    p3.setDescription('Proc Number Sounds per Ping');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Proc.SoundNbPerBeam
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.SoundNbPerBeam');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Proc.SoundNbPerBeam));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.SoundNbPerBeam (m)');
    p3.setDescription('Proc Number Sounds per Beam');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Proc.SampNbPerSound
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Proc.SampNbPerSound');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(this(k).Proc.SampNbPerSound));
    p3.setCategory('Proc properties');
    p3.setDisplayName('Proc.SampNbPerSound');
    p3.setDescription('Proc Number Samples per Sound');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Sous-ensemble Survey
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Survey');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Survey.Name
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Survey.Name');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).Survey.Name);
    p3.setCategory('Survey properties');
    p3.setDisplayName('Survey.Name');
    p3.setDescription('Survey Name');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Sous-ensemble Ship
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Ship');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Ship.Name
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ship.Name');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).Ship.Name);
    p3.setCategory('Ship properties');
    p3.setDisplayName('Ship.Name');
    p3.setDescription('Ship Name');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Ship.MRU.Name
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Ship.MRU.Name');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).Ship.MRU.Name);
    p3.setCategory('Ship properties');
    p3.setDisplayName('Ship.MRU.Name');
    p3.setDescription('Ship MRU Name');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Sous-ensemble Emission Models
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Emission');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Emission.ModelsConstructeur
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Emission.ModelsConstructeur');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(length(this.Emission.ModelsConstructeur)));
    p3.setCategory('Emission properties');
    p3.setDisplayName('Emission.ModelsConstructeur (ClModel)');
    p3.setDescription('Emission ModelsConstructeur ClModel');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Emission.ModelsCalibration
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Emission.ModelsCalibration');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(length(this.Emission.ModelsCalibration)));
    p3.setCategory('Emission properties');
    p3.setDisplayName('Emission.ModelsCalibration (ClModel)');
    p3.setDescription('Emission ModelsCalibration ClModel');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Sous-ensemble Reception Models
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Reception');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Reception.ModelsConstructeur
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Reception.ModelsConstructeur');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(length(this.Reception.ModelsConstructeur)));
    p3.setCategory('Reception properties');
    p3.setDisplayName('Reception.ModelsConstructeur (ClModel)');
    p3.setDescription('Reception ModelsConstructeur ClModel');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Reception.ModelsCalibration
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Reception.ModelsCalibration');
    p3.setType(javaclass('char',1));
    p3.setValue(num2str(length(this.Reception.ModelsCalibration)));
    p3.setCategory('Reception properties');
    p3.setDisplayName('Reception.ModelsCalibration (ClModel)');
    p3.setDescription('Reception ModelsCalibration ClModel');
    p3.setEditable(true);
    p2.addChild(p3);
    
    %% Sous-ensemble Display
    
    p2 = com.jidesoft.grid.DefaultProperty();
    p2.setName('Display');
    p2.setEditable(false);
    p1.addChild(p2);
    
    %% Display.ModelsConstructeur
    
    options0 = this(k).str.Display;
    editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
    context0 = com.jidesoft.grid.EditorContext(getTagListJIDE);
    com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
    
    p3 = com.jidesoft.grid.DefaultProperty();
    p3.setName('Display.ModelsConstructeur');
    p3.setType(javaclass('char',1));
    p3.setValue(this(k).str.Display{this(k).Display});
    p3.setEditorContext(context0);
    p3.setCategory('Display properties');
    p3.setDisplayName('Display Level');
    p3.setDescription('Display Level : 1=Light | 2=Medium | 3=Full');
    p3.setEditable(false);
    p2.addChild(p3);
end

%% Figure

if Fig
    %% Prepare a properties table containing the Parent
    model = com.jidesoft.grid.PropertyTableModel(Parent);
    % model.expandAll();
    % Pour afficher que les sous-ensembles du composant (Signal, Array, ...)
    model.expandNextLevel();
    model.expandNextLevel();
    
    % Cablage de la Callback de changement des attributs.
    hModel = handle(model, 'CallbackProperties');
    set(hModel, 'PropertyChangeCallback', {@callback_onPropertyChange, this});
    
    
    grid = com.jidesoft.grid.PropertyTable(model);
    hPanelJava = com.jidesoft.grid.PropertyPane(grid);
    
    %% Display the properties hPanelJava onscreen
    hFig = figure;
    set(hFig, 'WindowStyle', WindowStyle);
    set(hFig, 'MenuBar', 'none');
    set(hFig, 'ToolBar', 'none');
    Position = get(hFig, 'Position');
    hPanelMatlab = uipanel(hFig);
    javacomponent(hPanelJava, [5 5 Position(3:4)*0.98], hPanelMatlab);
    
    % Set the resize callback function of uihPanelMatlab container
    set(hPanelMatlab, 'ResizeFcn', {@UihPanelMatlabResizeCallback, hPanelMatlab, hPanelJava});
    
    % Wait for figure window to close & display the prop value
    % uiwait(hFig);
    % disp(prop(1).getValue())
    
end
end

%% Callback function for resize behavior of the uihPanelMatlab container

function UihPanelMatlabResizeCallback(hObject, eventdata, handles, hMainPanel, varargin) %#ok<INUSD,INUSL>
h           = get(handles, 'Children');
hFig        = get(handles, 'Parent');
Position    = get(hFig, 'Position');
set(h, 'Position', [5 5 Position(3:4)*0.98]);
end


function callback_onPropertyChange(model, event, object) %#ok<INUSD>
disp(Lang('R�gler les autres attributs du sondeur', 'Please, adjust other sounder parameters'));
%Identification de la propri�t� en cours de modification.
% newValue   = event.getNewValue();
oldValue    = event.getOldValue();
propName    = event.getPropertyName();
prop        = model.getProperty(propName);
prop.setValue(oldValue);
model.refresh();  % refresh value onscreen
return
end

function strTag = getTagListJIDE
% Cr�ation d'un Tag unique � chaque liste de valeurs d'un composant
% JIDE.
strTag = ['comboxeditor' num2str(randi(10000))];
end
