function Ident = find_IdentSonar(this, Name, varargin)

[varargin, Frequency] = getPropertyValue(varargin, 'Frequency', []); %#ok<ASGLU>

if strcmp(Name, '13003') || strcmp(Name, '13016')
    Name = 'Norbit'; % TODO : fait � l'arrache
end

flagCell = strfind(this.str.SonarNames, Name);
for k=1:length(flagCell)
    flag(k) = ~isempty(flagCell{k}); %#ok<AGROW>
end

sub = find(flag == 1);
switch length(sub)
    case 0
        Ident = 0;
    case 1
        Ident = sub;
    otherwise
        if strcmp(Name, '7150') && (Frequency < 18)
            Ident = sub(1);
        elseif strcmp(Name, '7150') && (Frequency > 18)
            Ident = sub(2);
        else
            disp('Beark')
        end
end
