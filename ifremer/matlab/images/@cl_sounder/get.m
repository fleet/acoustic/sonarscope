function varargout = get(this, varargin)
    varargout = {};
    for k=1:length(varargin)
        switch varargin{k}
            case 'IdentSonar'
                varargout{end+1} = this.Sonar.Ident; %#ok<AGROW>
            case 'paramsFileName'
                varargout{end+1} = this.paramsFileName; %#ok<AGROW>
            case {'str.SonarNames'; 'SonarNames'}
                varargout{end+1} = this.str.SonarNames; %#ok<AGROW>
            case {'Sonar.Ident'; 'SonarIdent'}
                varargout{end+1} = this.Sonar.Ident; %#ok<AGROW>
            case {'Sonar.Name'; 'SonarName'}
                if this.Sonar.Ident == 0
                    varargout{end+1} = this.str.SonarNames{:}; %#ok<AGROW>
                else
                    varargout{end+1} = this.str.SonarNames{this.Sonar.Ident}; %#ok<AGROW>
                end
            case {'Sonar.Constructeur'; 'SonarConstructeur'}
                varargout{end+1} = this.Sonar.Constructeur;   %#ok<AGROW>
            case {'Sonar.Freq', 'SonarFreq'}
                varargout{end+1} = this.Sonar.Freq; %#ok<AGROW>

            case {'Sonar.strMode_1'; 'strModes_1'}
                varargout{end+1} = this.Sonar.strMode_1; %#ok<AGROW>
            case {'Sonar.Mode_1'; 'iMode1'}
                varargout{end+1} = this.Sonar.Mode_1; %#ok<AGROW>
            case {'Sonar.TypeMode_1'; 'TypeMode_1'; 'TypeMode1'}
                varargout{end+1} = this.Sonar.TypeMode_1; %#ok<AGROW>

            case {'Sonar.strMode_2'; 'strModes_2'}
                varargout{end+1} = this.Sonar.strMode_2; %#ok<AGROW>
            case {'Sonar.Mode_2'; 'iMode2'}
                varargout{end+1} = this.Sonar.Mode_2; %#ok<AGROW>
             case {'Sonar.TypeMode_2'; 'TypeMode_2'; 'TypeMode2'}
                varargout{end+1} = this.Sonar.TypeMode_2; %#ok<AGROW>

            case {'Sonar.strMode_3'; 'strModes_3'}
                varargout{end+1} = this.Sonar.strMode_3; %#ok<AGROW>
            case {'Sonar.Mode_3'; 'iMode3'}
                varargout{end+1} = this.Sonar.Mode_3; %#ok<AGROW>
            case {'Sonar.TypeMode_3'; 'TypeMode_3'; 'TypeMode3'}
                varargout{end+1} = this.Sonar.TypeMode_3; %#ok<AGROW>

            case {'Sonar.Family'; 'sonarType'}
                varargout{end+1} = this.Sonar.Family; %#ok<AGROW>
            case 'strFamily'
                varargout{end+1} = this.str.Family; %#ok<AGROW>
            case {'Sonar.SystemSerialNumber'; 'SonarSystemSerialNumber'; 'SystemSerialNumber'}
                varargout{end+1} = this.Sonar.SystemSerialNumber; %#ok<AGROW>
            case {'Sonar.LongBeamWth'; 'LongBeamWth'}
                varargout{end+1} = this.Sonar.LongBeamWth; %#ok<AGROW>
            case {'Sonar.TransBeamWth', 'TransBeamWth'}
                varargout{end+1} = this.Sonar.TransBeamWth; %#ok<AGROW>
            case {'Sonar.MaxDepth'; 'MaxDepth'}
                varargout{end+1} = this.Sonar.MaxDepth; %#ok<AGROW>
            case {'Sonar.ApertWth'; 'ApertWth'}
                varargout{end+1} = this.Sonar.ApertWth; %#ok<AGROW>

            case 'Signal.Freq'
                varargout{end+1} = this.Signal.Freq; %#ok<AGROW>
            case {'Signal.WaveLength'; 'WaveLength'}
                varargout{end+1} = this.Signal.WaveLength; %#ok<AGROW>
            case 'Signal.DistanceFresnel'
                varargout{end+1} = this.Signal.DistanceFresnel; %#ok<AGROW>
            case 'Signal.ChampLointain'
                varargout{end+1} = this.Signal.ChampLointain; %#ok<AGROW>
            case 'Signal.Type'
                varargout{end+1} = this.Signal.Type; %#ok<AGROW>
            case {'Signal.BandWth'; 'BandWth'}
                varargout{end+1} = this.Signal.BandWth; %#ok<AGROW>
            case {'Signal.Level'; 'level'}
                varargout{end+1} = this.Signal.Level; %#ok<AGROW>
            case {'Signal.Duration'; 'duration'}
                varargout{end+1} = this.Signal.Duration; %#ok<AGROW>

            case {'BeamForm.Tx.LongWth';    'TxBeamWidthAlongTrack'}
                varargout{end+1} = this.BeamForm.Tx.LongWth; %#ok<AGROW>
            case {'BeamForm.Tx.TransWth';   'TxBeamWidthAcrossTrack'}
                varargout{end+1} = this.BeamForm.Tx.TransWth; %#ok<AGROW>
            case {'BeamForm.Rx.LongWth';    'RxBeamWidthAlongTrack'}
                varargout{end+1} = this.BeamForm.Rx.LongWth; %#ok<AGROW>
            case {'BeamForm.Rx.TransWth';   'RxBeamWidthAcrossTrack'}
                varargout{end+1} = this.BeamForm.Rx.TransWth; %#ok<AGROW>
            case {'ReceptionMaximumAngle'}
                varargout{end+1} = max(this.BeamForm.Rx.Angles); %#ok<AGROW>

            case {'Array.Rx.Type'; 'RxArrayType'}
                varargout{end+1} = this.Array.Rx.Type; %#ok<AGROW>
            case {'Array.Tx.Type'; 'TxArrayType'}
                varargout{end+1} = this.Array.Tx.Type; %#ok<AGROW>
            case {'Array.Rx.TransTilt'; 'RxArrayTilt'}
                varargout{end+1} = this.Array.Rx.TransTilt; %#ok<AGROW>
            case {'Array.Tx.TransTilt'; 'TxArrayTilt'}
                varargout{end+1} = this.Array.Tx.TransTilt; %#ok<AGROW>

            case {'Proc.RangeResol'; 'RangeResol'}
                varargout{end+1} = this.Proc.RangeResol; %#ok<AGROW>
            case {'Proc.SampFreq'; 'SampFreq'}
                varargout{end+1} = this.Proc.SampFreq; %#ok<AGROW>
            case 'Proc.TVGLaw'
                varargout{end+1} = this.Proc.TVGLaw; %#ok<AGROW>

            case 'ShipName'
                varargout{end+1} = this.Ship.Name; %#ok<AGROW>
            case 'ShipSelfNoise'
                varargout{end+1} = this.Ship.selfNoise; %#ok<AGROW>
            case 'SonarHeadDepth'
                varargout{end+1} = this.Ship.sonarHeadDepth; %#ok<AGROW>
            case 'MRU'
                varargout{end+1} = this.Ship.MRU.Name; %#ok<AGROW>
            case 'HeadAccuracy'
                varargout{end+1} = this.Ship.MRU.HeadAccuracy; %#ok<AGROW>
            case 'PitchAccuracy'
                varargout{end+1} = this.Ship.MRU.PitchAccuracy; %#ok<AGROW>
            case 'RollAccuracy'
                varargout{end+1} = this.Ship.MRU.RollAccuracy; %#ok<AGROW>
            case 'HeaveAccuracy'
                varargout{end+1} = this.Ship.MRU.HeaveAccuracy; %#ok<AGROW>
            case 'YawAccuracy'
                varargout{end+1} = this.Ship.MRU.YawAccuracy; %#ok<AGROW>

            case {'BeamForm.Rx.Nb'; 'RxNbBeams'}
                varargout{end+1} = this.BeamForm.Rx.Nb; %#ok<AGROW>
            case 'BeamForm.Rx.strRepart'
                varargout{end+1} = this.BeamForm.Rx.strRepart; %#ok<AGROW>
            case 'BeamForm.Rx.Repart'
                varargout{end+1} = this.BeamForm.Rx.Repart; %#ok<AGROW>
            case 'BeamForm.Rx.Angles'
                varargout{end+1} = this.BeamForm.Rx.Angles; %#ok<AGROW>
            case 'BeamForm.Tx.LimitAng'
                varargout{end+1} = this.BeamForm.Tx.LimitAng; %#ok<AGROW>

            case 'BeamForm.Tx.Freq'
                varargout{end+1} = this.BeamForm.Tx.Freq; %#ok<AGROW>

            case 'EmissionModelsConstructeur'
                varargout{end+1} = this.Emission.ModelsConstructeur; %#ok<AGROW>

            case 'EmissionModelsCalibration'
                varargout{end+1} = this.Emission.ModelsCalibration; %#ok<AGROW>

            case 'ReceptionModelsConstructeur'
                varargout{end+1} = this.Reception.ModelsConstructeur; %#ok<AGROW>

            case 'ReceptionModelsCalibration'
                varargout{end+1} = this.Reception.ModelsCalibration; %#ok<AGROW>



            case 'Display'
                varargout{end+1} = this.Display; %#ok<AGROW>

            otherwise
                my_warndlg('cl_sounder:get Invalid property name', 0);
        end
    end

