function P = getTxParams(this)

if isempty(this.Emission.ModelsConstructeur)
    P = [];
    return
end

if isempty(this.Sonar.Mode_2)
    M = this.Emission.ModelsConstructeur{this.Sonar.Mode_1};
else
    M = this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2};
end
for k=1:length(M)
    parametres = M(k).getParamsValue;
    P(k,1:length(parametres)) = parametres; %#ok<AGROW>
end
