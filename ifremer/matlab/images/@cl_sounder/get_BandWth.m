% Largeur de bande pour chacune des antennes d'emission
%
% Syntax
%   BandWth = get_BandWth(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   BandWth : Niveau d'emission par antenne d'emission (dB)
%
% Examples
%   a = cl_sounder
%   BandWth = get_BandWth(a)
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function BandWth = get_BandWth(this)

% Comment� par JMA le 27/05/2016

% if isempty(this.Signal.BandWth)
%     BandWth = this.BeamForm.Tx.BandWth;
% else
%     BandWth = repmat(this.Signal.BandWth, 1, this.BeamForm.Tx.Nb);
% end

% Modifi� par JMA le 27/05/2016

if isempty(this.Signal.BandWth)
    BandWth = this.BeamForm.Tx.BandWth;
else
%     n = length(this.Sonar.strMode_2); % Comment� par JMA le 30/05/2016
    n = this.BeamForm.Tx.Nb; % Ajout JMA le 30/05/2016
    if n == 0
        BandWth = this.Signal.BandWth;
    else
        if numel(this.Signal.BandWth) == 1
            for k=n:-1:1
                % TODO conserver les modes, faire des set et remettre
                % le mode apr�s
                %                     PulseLength(k) = this.Signal.Duration; % TODO : a compl�ter Modes1 ou Modes 2 ?
                %             PulseLength = this.Signal.Duration; % TODO : a compl�ter Modes1 ou Modes 2 ? % Comment� par JMA le 27/05/2016
                
                BandWth(k) = this.Signal.BandWth; % Modif JMA le 27/05/2016
            end
        else
            BandWth = this.Signal.BandWth;
        end
    end
    %                 PulseLength = repmat(this.Signal.Duration, 1, this.BeamForm.Tx.Nb);
end

