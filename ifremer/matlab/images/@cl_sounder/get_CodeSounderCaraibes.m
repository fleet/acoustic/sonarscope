
function Sounder_type = get_CodeSounderCaraibes(this)

switch this.Sonar.Ident
    case 1 % EM12D
        Sounder_type = 53;
    case 2 % EM12S
        Sounder_type = 52;
    case 3 % EM300
        Sounder_type = 56;
    case 4 % EM1000
        Sounder_type = 51;
    case 5 % SAR
        Sounder_type = 0;
    case 6 % EM3000D
        Sounder_type = 55;
    case 7 % EM3000S
        Sounder_type = 54;
    case 8 % DF1000
        Sounder_type = 0;
    case 9 % DTS1
        Sounder_type = 0;
    case 10 % EM1002
        Sounder_type = 57;
    case 11 % EM120
        Sounder_type = 58;
        
    case 14 % Reson7111
        Sounder_type = 84;
    case 15% Reson7125
        Sounder_type = 85;
    case 12 % Reson7150 12kHz
        Sounder_type = 86;
    case 13 % Reson7150 24kHz
        Sounder_type = 86;
        
    case 26 % Klein3000
        Sounder_type = 0;
    case 21 % EM2000
        Sounder_type = 0;
    case 20 % EM3002S
        Sounder_type = 0;
    case 19 % EM3002D
        Sounder_type = 0;
    case 22 % EM302
        Sounder_type = 0;
    case 23
        Sounder_type = 0;
        
    otherwise
        Sounder_type = 0;
        my_warndlg('TODO : getParamsReception, d�finir Params(:,4)', 1);
end
