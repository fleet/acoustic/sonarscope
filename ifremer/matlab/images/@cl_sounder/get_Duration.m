% Dur�e d'�mission
%
% Syntax
%   PulseLength = get_Duration(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   PulseLength : Pulse length
%
% Examples
%   a = cl_sounder
%   PulseLength = get_Duration(a)
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function PulseLength = get_Duration(this)

if isempty(this.Signal.Duration)
    PulseLength = this.BeamForm.Tx.Duration;
else
%     n = length(this.Sonar.strMode_2); % Comment� par JMA le 30/05/2016
    n = this.BeamForm.Tx.Nb; % Ajout JMA le 30/05/2016
    if n == 0
        PulseLength = this.Signal.Duration;
    elseif n == length(this.Signal.Duration)
        PulseLength = this.Signal.Duration;
    else
        for k=n:-1:1
            % TODO conserver les modes, faire des set et remettre
            % le mode apr�s
            %                     PulseLength(k) = this.Signal.Duration; % TODO : a compl�ter Modes1 ou Modes 2 ?
%             PulseLength = this.Signal.Duration; % TODO : a compl�ter Modes1 ou Modes 2 ? % Comment� par JMA le 27/05/2016
            
            PulseLength(k) = this.Signal.Duration; % Modif JMA le 27/05/2016
        end
    end
    %                 PulseLength = repmat(this.Signal.Duration, 1, this.BeamForm.Tx.Nb);
end
