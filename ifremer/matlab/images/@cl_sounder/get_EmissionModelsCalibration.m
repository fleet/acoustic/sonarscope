% Lecture du model defini par Ifremer en emission
%
% Syntax
%   model = get_EmissionModelsCalibration(a)
%
% INTPUT PARAMETERS :
%   a : une instance de cl_sounder
%
% INTPUT PARAMETERS :
%   model : instances de ClModel
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   model = get_EmissionModelsCalibration(a)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function  model = get_EmissionModelsCalibration(this)

if isempty(this.Sonar.Mode_2)
    model = this.Emission.ModelsCalibration{this.Sonar.Mode_1};
else
    model = this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2};
end
