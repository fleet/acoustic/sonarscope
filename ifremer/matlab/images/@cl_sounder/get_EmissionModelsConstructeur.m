% Lecture du model defini par le constructeur en emission
%
% Syntax
%   model = get_EmissionModelsConstructeur(a)
%
% INTPUT PARAMETERS :
%   a : une instance de cl_sounder
%
% INTPUT PARAMETERS :
%   model : instances de ClModel
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   model = get_EmissionModelsConstructeur(a)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function  model = get_EmissionModelsConstructeur(this)

if isempty(this.Sonar.Mode_2)
    model = this.Emission.ModelsConstructeur{this.Sonar.Mode_1};
else
    model = this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2};
end
