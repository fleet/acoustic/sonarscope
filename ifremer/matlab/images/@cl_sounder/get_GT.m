function GT = get_GT(this)
    % Gain analogique en sortie des antennes de reception
    %
    % Syntax
    %   GT = get_GT(a)
    %
    % Input Arguments
    %   a : Instance de cl_sounder
    %
    % Output Arguments
    %   GT : Niveau d'emission par antenne d'emission (dB)
    %
    % Examples
    %   a = cl_sounder
    %   GT = get_GT(a)
    %
    % See also cl_sounder Authors
    % Authors : JMA
    % ----------------------------------------------------------------------------

    if isempty(this.Signal.GT)
        GT = this.BeamForm.Rx.GT;
    else
        GT = repmat(this.Signal.GT, 1, this.BeamForm.Tx.Nb);
    end
