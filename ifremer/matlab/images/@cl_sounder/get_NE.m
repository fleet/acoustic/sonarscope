function NE = get_NE(this)
% Niveau d'emission par antenne d'emission
%
% Syntax
%   NE = get_NE(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   NE : Niveau d'emission par antenne d'emission (dB)
%
% Examples
%   a = cl_sounder
%   NE = get_NE(a)
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

if isempty(this.Signal.Level)
    NE = this.BeamForm.Tx.Level;
else
    %                 NE = repmat(this.Signal.Level, 1, this.BeamForm.Tx.Nb);
    NE = this.Signal.Level;
end
