function resolution = get_RangeResol(this, Mode, varargin)
% Resolution de l'imagerie du sondeur
%
% Syntax
%   resolution = get_RangeResol(this, Mode)
%
% Input Arguments
%   this : instance de cl_sounder
%   Mode : modes
%
% Output Arguments
%   resolution : Resolution en distance oblique en m
%
% Examples
%    a = cl_sounder('Sonar.Ident', 1)
%    resolution = get_RangeResol(a, 1:2)
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

switch nargin
    case 2 % Pour compatibilité temporaire avec les appels pas encore différenciés (R2012 / Avant)
        resolution = NaN(length(Mode), 1, 'single');
        ListeModes = unique(Mode);
        for k=1:length(ListeModes)
            this = set(this, 'Sonar.Mode_1', ListeModes(k));
            sub = (Mode == ListeModes(k));
            resolution(sub) = this.Proc.RangeResol;
        end
    otherwise
        Mode1 = Mode;
        Mode2 = varargin{1};
        resolution = NaN(length(Mode1), 1, 'single');
        ListeModes1 = unique(Mode1);
        ListeModes2 = unique(Mode2);
        for k1=1:length(ListeModes1)
            for k2=1:length(ListeModes2)
                this = set(this, 'Sonar.Mode_1', ListeModes1(k1), 'Sonar.Mode_2', ListeModes2(k1));
                sub = (Mode1 == ListeModes1(k1)) & (Mode2 == ListeModes2(k2));
                resolution(sub) = this.Proc.RangeResol;
            end
        end
end
