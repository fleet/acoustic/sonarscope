function SH = get_SH(this)
% Sensibilite des antennes d'emission
%
% Syntax
%   SH = get_SH(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   SH : Niveau d'emission par antenne d'emission (dB)
%
% Examples
%   a = cl_sounder
%   SH = get_SH(a)
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

if isempty(this.Signal.SH)
    SH = this.BeamForm.Tx.SH;
else
    SH = repmat(this.Signal.SH, 1, this.BeamForm.Tx.Nb);
end
