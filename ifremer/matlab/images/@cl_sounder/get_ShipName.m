function ShipName = get_ShipName(this)

switch this.Sonar.SystemSerialNumber
    case -1
        ShipName = 'Unknown';
    case 507
        ShipName = 'Belgica';
    otherwise
        str1 = sprintf('cl_sounder/get_ShipName : SystemSerialNumber = %d : pas encore renseigné ici. Pas de panique, tout va bien.', this.Sonar.SystemSerialNumber);
        str2 = sprintf('cl_sounder/get_ShipName : SystemSerialNumber = %d : not set yet, do not worry about this message.', this.Sonar.SystemSerialNumber);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'SystemSerialNumberNotSetInget_ShipName', 'TimeDelay', 60);
        ShipName = 'Unknown';
end
