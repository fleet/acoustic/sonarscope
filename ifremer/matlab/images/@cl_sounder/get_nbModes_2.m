% Nombre de modes secondaires pour le mode du sondeur
%
% Syntax
%   N = get_nbModes_2(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   N : Nombre de modes du sondeur
%
% Examples
%   a = cl_sounder
%   N = get_nbModes_2(a)
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function N = get_nbModes_2(this)
N = length(this.Sonar.strMode_2);
