% Nombre de sondeurs dans la base de donnees
%
% Syntax
%   N = get_nbSounders(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   N : Nombre de sondeurs dans la base de donnees
%
% Examples
%   a = cl_sounder
%   N = get_nbSounders(a, '')
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function N = get_nbSounders(this)
N = length(this.str.SonarNames);
