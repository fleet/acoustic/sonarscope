% Liste des modes disponible pour le sondeur courant
%
% Syntax
%   strMode_1 = get_strModes_1(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   Sonar.strMode_1 : Nombre de sondeurs dans la base de donnees
%
% Examples
%   a = cl_sounder('Sonar.Ident', 1)
%   strMode_1 = get_strModes_1(a)
%
% See also cl_sounder Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function strMode_1 = get_strModes_1(this)
strMode_1 = this.Sonar.strMode_1;
