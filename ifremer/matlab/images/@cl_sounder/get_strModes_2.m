% Liste des modes disponible pour le sondeur courant
%
% Syntax
%   Sonar.strMode_2 = get_Sonar.strMode_2(a)
%
% Input Arguments
%   a : Instance de cl_sounder
%
% Output Arguments
%   Sonar.strMode_2 : Nombre de sondeurs dans la base de donnees
%
% Examples
%   a = cl_sounder('Sonar.Ident', 1)
%   strMode_2 = get_strModes_2(a)
%
% See also cl_sounder Authors
% Authors : JMA
% -------------------------------------------------------------------------

function strMode_2 = get_strModes_2(this)
strMode_2 = this.Sonar.strMode_2;
