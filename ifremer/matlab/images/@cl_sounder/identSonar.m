function Sonar = identSonar(this, varargin)

[varargin, SonarName]      = getPropertyValue(varargin, 'SonarName', []);
[varargin, SonarStrMode_1] = getPropertyValue(varargin, 'SonarStrMode_1', []); %#ok<ASGLU>

Sonar = [];

if ~isempty(SonarName)
    Sonar.Ident = find_IdentSonar(this, SonarName);
    this = cl_sounder('Sonar.Ident', Sonar.Ident);
end

if ~isempty(SonarStrMode_1)
    listeSonarStrMode_1 = get(this, 'Sonar.strMode_1');
    Sonar.Mode_1 = [];
    for kFreq=1:length(SonarStrMode_1)
        pppp = strfind(listeSonarStrMode_1{kFreq}, SonarStrMode_1);
        if ~isempty(pppp)
            Sonar.Mode_1 = kFreq;
            break
        end
    end
    this = set(this, 'Sonar.Mode_1', Sonar.Mode_1);
end
