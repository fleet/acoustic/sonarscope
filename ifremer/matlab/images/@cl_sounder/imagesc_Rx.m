function imagesc_Rx(this, varargin)
% Affichage des diagrammes d'emission d'un sondeur
%
% Syntax
%   imagesc_Rx(a, ...)
%
% INTPUT PARAMETERS :
%   a : une instance de cl_sounder
%
% Name-only Arguments
%   nuFais : Numeros de faisceaux
%   H      : Hauteur
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   imagesc_Rx(a, 'Constructeur', 'nuFais', 1)
%   imagesc_Rx(a, 'Constructeur', 'nuFais', 1, 'H', 100)
%
% See also cl_sounder cl_sounder/imagesc_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

[varargin, nuFais] = getPropertyValue(varargin, 'nuFais', 1:this.BeamForm.Rx.Nb);
[varargin, H] = getPropertyValue(varargin, 'H', []);

%[varargin, flagConstructeur] = getFlag(varargin, 'Constructeur');
[varargin, flagCalibration] = getFlag(varargin, 'Calibration'); %#ok<ASGLU>

if flagCalibration
    M_Trans = this.Reception.ModelsCalibration(nuFais);
    titre = ['Calibration Reception Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}];
else
    M_Trans = this.Reception.ModelsConstructeur(nuFais);
    titre = ['Factory Reception Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}];
end

angles_Trans_Rx = get(M_Trans, 'XData');
gain_Trans_Rx   = compute(M_Trans);

L = this.Sonar.LongBeamWth;
angles_Long_Rx = linspace(-L, L, 20);
gain_Long_Rx   = AntenneSinc(angles_Long_Rx, [0 0 L]);

if isempty(H)
    x = angles_Trans_Rx;
    y = angles_Long_Rx;
else
    x = H * tan(angles_Trans_Rx * pi / 180);
    y = H * tan(angles_Long_Rx * pi / 180);
end
FigUtils.createSScFigure;
subplot(3,2,1); PlotUtils.createSScPlot(angles_Trans_Rx, gain_Trans_Rx);
title(titre)

grid on; xlabel('Across angle (deg)');  ylabel('Across Gain (dB)');
subplot(3,2,2); plot(x, gain_Trans_Rx);
grid on; xlabel('Across distance (m)'); ylabel('Across Gain (dB)');
subplot(3,2,3); plot(angles_Long_Rx, gain_Long_Rx);
grid on; xlabel('Along Angle (deg)');  ylabel('Along Gain (dB)');
subplot(3,2,4); plot(y, gain_Long_Rx);
grid on; xlabel('Along distance (m)'); ylabel('Along Gain (dB)');
Gain2D = -gain_Long_Rx' * gain_Trans_Rx;
% Gain2D = repmat(-gain_Long_Rx', 1, size(gain_Trans_Rx,2)) .* repmat(gain_Trans_Rx, size(gain_Long_Rx,2), 1);

subplot(3,2,5); imagesc(angles_Trans_Rx, angles_Long_Rx, Gain2D); colorbar
xlabel('Across angle (deg)'); ylabel('Along angle (deg)');
subplot(3,2,6); pcolor(x, y, Gain2D); colorbar
xlabel('Across distance (m)'); ylabel('Along distance (m)');
