function imagesc_Tx(this, varargin)
% Affichage des diagrammes d'emission d'un sondeur
%
% Syntax
%   imagesc_Tx(a, ...)
%
% INTPUT PARAMETERS :
%   a : une instance de cl_sounder
%
% Name-only Arguments
%    :
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   imagesc_Tx(a, 'Constructeur', 'nuFais', 1)
%   imagesc_Tx(a, 'Constructeur', 'nuFais', 1, 'H', 100)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

[varargin, nuFais] = getPropertyValue(varargin, 'nuFais', 1:this.BeamForm.Tx.Nb);
[varargin, H]      = getPropertyValue(varargin, 'H',      []);

% [varargin, flagConstructeur] = getFlag(varargin, 'Constructeur');
[varargin, flagCalibration] = getFlag(varargin, 'Calibration'); %#ok<ASGLU>

if flagCalibration
    if isempty(this.Sonar.Mode_2)
        M_Trans = this.Emission.ModelsCalibration{this.Sonar.Mode_1}(nuFais);
    else
        M_Trans = this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}(nuFais);
    end
    titre = ['Calibration Emission Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}];
else
    if isempty(this.Sonar.Mode_2)
        M_Trans = this.Emission.ModelsConstructeur{this.Sonar.Mode_1}(nuFais);
    else
        M_Trans = this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(nuFais);
    end
    titre = ['Factory Emission Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}];
end

angles_Trans_Rx = get(M_Trans, 'XData');
gain_Trans_Rx   = compute(M_Trans);

L = this.Sonar.LongBeamWth;
angles_Long_Rx = linspace(-L, L, 20);
gain_Long_Rx   = AntenneSinc(angles_Long_Rx, [0 0 L]);

if isempty(H)
    x = angles_Trans_Rx;
    y = angles_Long_Rx;
else
    x = H * tan(angles_Trans_Rx * pi / 180);
    y = H * tan(angles_Long_Rx * pi / 180);
end

figure;
pcolor(x, y, gain_Long_Rx' * gain_Trans_Rx)
colorbar; title(titre)
