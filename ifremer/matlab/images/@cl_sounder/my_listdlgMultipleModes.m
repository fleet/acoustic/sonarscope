function [flag, TabiMode_1, TabiMode_2] = my_listdlgMultipleModes(Sonar, varargin)

[varargin, Time] = getPropertyValue(varargin, 'Time', []); %#ok<ASGLU>

SonarMode_1 = get(Sonar, 'Sonar.Mode_1');
SonarMode_2 = get(Sonar, 'Sonar.Mode_2');
strMode_1   = get(Sonar, 'Sonar.strMode_1');
strModes= {};
TabiMode_1 = [];
TabiMode_2 = [];
TabInit = [];
for iMode_1=1:length(strMode_1)
    Sonar = set(Sonar, 'Sonar.Mode_1', iMode_1, 'Sonar.Mode_2', 1, 'Time', Time);
    strMode_2 = get(Sonar, 'Sonar.strMode_2');
    if isempty(strMode_2)
        SonarName = get(Sonar, 'SonarName');
        strMode1  = get(Sonar, 'Sonar.strMode_1');
        strModes{end+1} = sprintf('Factory Tx Beams : %s : Mode1=%d (%s)', ...
            SonarName, iMode_1, strMode1{iMode_1}); %#ok<AGROW>
        TabiMode_1(end+1) = iMode_1; %#ok<AGROW>
        
        if (iMode_1 == SonarMode_1)
            TabInit(end+1) = 1; %#ok<AGROW>
        else
            TabInit(end+1) = 0; %#ok<AGROW>
        end
    else
        for iMode_2=1:length(strMode_2)
            Sonar = set(Sonar, 'Sonar.Mode_2', iMode_2);
            SonarName = get(Sonar, 'SonarName');
            strMode1  = get(Sonar, 'Sonar.strMode_1');
            strMode2  = get(Sonar, 'Sonar.strMode_2');
            strModes{end+1} = sprintf('Factory Tx Beams : %s : Mode1=%d (%s) : Mode2=%d (%s)', ...
                SonarName, iMode_1, strMode1{iMode_1}, iMode_2, strMode2{iMode_2}); %#ok<AGROW>
            TabiMode_1(end+1) = iMode_1; %#ok<AGROW>
            TabiMode_2(end+1) = iMode_2; %#ok<AGROW>
            
            if (iMode_1 == SonarMode_1) && (iMode_2 == SonarMode_2)
                TabInit(end+1) = 1; %#ok<AGROW>
            else
                TabInit(end+1) = 0; %#ok<AGROW>
            end
        end
    end
end
str1 = 'Liste de tous les modes disponibles pour ce sondeur.';
str2 = 'List of all the modes for this sounder.';
[rep, flag] = my_listdlgMultiple(Lang(str1,str2), strModes, 'InitialValue', find(TabInit));
if ~flag
    return
end
TabiMode_1 = TabiMode_1(rep);
if ~isempty(strMode_2)
    TabiMode_2 = TabiMode_2(rep);
end

