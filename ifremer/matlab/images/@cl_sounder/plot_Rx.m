% Affichage des diagrammes d'emission d'un sondeur
%
% Syntax
%   plot_Rx(a, ...)
%
% INTPUT PARAMETERS :
%   a : une instance de cl_sounder
%
% Name-only Arguments
%    :
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   plot_Rx(a, 'Constructeur', 'Erase', 0)
%   plot_Rx(a, 'Constructeur', 'Calibration', 'nuFais', 10)
%
%   a = cl_sounder('Sonar.Ident', 1, 'Sonar.Mode_1', 2)
%   a = cl_sounder('Sonar.Ident', 1, 'Sonar.Mode_1', 1, 'Sonar.Mode_2', 2)
%
%   for i=1:get_nbSounders(a)
%       a = cl_sounder('Sonar.Ident', i)
%       for j=1:get_nbModes_1(a)
%           a = set(a, 'Sonar.Mode_1', j)
%           for k=1:get_nbModes_2(a)
%               a = set(a, 'Sonar.Mode_1', j, 'Sonar.Mode_2', k)
%           end
%       end
%   end
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_Rx(this, varargin)

[varargin, nuFais] = getPropertyValue(varargin, 'nuFais', 1:this.BeamForm.Rx.Nb);

[varargin, flagConstructeur] = getFlag(varargin, 'Constructeur');
[varargin, flagCalibration]  = getFlag(varargin, 'Calibration'); %#ok<ASGLU>

if isempty(nuFais)
    str1 = 'Veuillez indiquer le num�ro de faisceau svp : "nuFais".';
    str2 = 'Please give the beam number : "nuFais".';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if ~flagConstructeur && ~flagCalibration
    str1 = 'Veuillez indiquer la source svp : ''Constructeur'' ou ''Calibration ou les deux'', svp.';
    str2 = 'Please give the origin : ''Constructeur'' or ''Calibration'' or both.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

FigUtils.createSScFigure;
for k=1:length(nuFais)
    if flagConstructeur && flagCalibration
        Axe = subplot(2,1,1);
        PlotUtils.createSScPlot(this.Reception.ModelsConstructeur(nuFais(k)), 'Axe', Axe, 'Erase', 0);
        title(['Factory Reception Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}])
        
        Axe = subplot(2,1,2);
        PlotUtils.createSScPlot(this.Reception.ModelsCalibration(nuFais(k)), 'Axe', Axe, 'Erase', 0);
        title(['Calibration Reception Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}])
    elseif flagConstructeur
        Axe = gca;
        %                 PlotUtils.createSScPlot(this.Reception.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(nuFais(k)), 'Axe', Axe)
        PlotUtils.createSScPlot(this.Reception.ModelsConstructeur(nuFais(k)), 'Axe', Axe, 'Erase', 0);
        title(['Factory Reception Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}])
    elseif flagCalibration
        Axe = gca;
        PlotUtils.createSScPlot(this.Reception.ModelsCalibration(nuFais(k)), 'Axe', Axe, 'Erase', 0);
        title(['Calibration Reception Beams ' this.str.SonarNames{this.Sonar.Ident} ' ' this.Sonar.strMode_1{this.Sonar.Mode_1} '  ' this.Sonar.strMode_2{this.Sonar.Mode_2}])
    end
    hold on
end
hold off

