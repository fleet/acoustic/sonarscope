% Affichage des diagrammes d'emission d'un sondeur
%
% Syntax
%   plot_Tx(a, ...)
%
% INTPUT PARAMETERS :
%   a : une instance de cl_sounder
%
% Name-only Arguments
%    :
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   plot_Tx(a, 'Constructeur', 'Calibration')
%
%   a = cl_sounder('Sonar.Ident', 1, 'Sonar.Mode_1', 2)
%   a = cl_sounder('Sonar.Ident', 1, 'Sonar.Mode_1', 1, 'Sonar.Mode_2', 2)
%
%   for i=1:get_nbSounders(a)
%       a = cl_sounder('Sonar.Ident', i)
%       for j=1:get_nbModes_1(a)
%           a = set(a, 'Sonar.Mode_1', j)
%           for k=1:get_nbModes_2(a)
%               a = set(a, 'Sonar.Mode_1', j, 'Sonar.Mode_2', k)
%               plot_Tx(a, 'Constructeur', 'Calibration')
%           end
%       end
%   end
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_Tx(this, varargin)

[varargin, Title] = getPropertyValue(varargin, 'Title', []);

[varargin, flagConstructeur] = getFlag(varargin, 'Constructeur');
[varargin, flagCalibration]  = getFlag(varargin, 'Calibration'); %#ok<ASGLU>

if ~flagConstructeur && ~flagCalibration
    str1 = 'Veuillez indiquer la source svp : ''Constructeur'' ou ''Calibration ou les deux'', svp.';
    str2 = 'Please give the origin : ''Constructeur'' or ''Calibration'' or both.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if isempty(this.Sonar.Mode_2)
    this.Sonar.Mode_2 = 1; % TODO : g�rer �a en amont
end

if flagConstructeur && flagCalibration
    FigUtils.createSScFigure;
    Axe = subplot(2,1,1);
    
    % TODO  56556 : r�soudre �a de mani�re plus s�rieuse
    if isempty(this.Sonar.Mode_2)
        this.Sonar.Mode_2 = 1;
        this.Sonar.strMode_2 = {[]};
    end
    
    PlotUtils.createSScPlot(this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}, 'Axe', Axe);
    strMode1 = this.Sonar.strMode_1{this.Sonar.Mode_1};
    if ~ischar(strMode1)
        strMode1 = num2str(strMode1);
    end
    strMode2 = this.Sonar.strMode_2{this.Sonar.Mode_2};
    if ~ischar(strMode2)
        strMode2 = num2str(strMode2);
    end
    str2 = sprintf('Factory Tx Beams : %s : Mode=%d (%s) : Mode2=%d (%s)', ...
        this.str.SonarNames{this.Sonar.Ident}, ...
        this.Sonar.Mode_1, strMode1, ...
        this.Sonar.Mode_2, strMode2);
    title(str2)
    
    Axe = subplot(2,1,2);
    PlotUtils.createSScPlot(this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}, 'Axe', Axe);
    strMode1 = this.Sonar.strMode_1{this.Sonar.Mode_1};
    if ~ischar(strMode1)
        strMode1 = num2str(strMode1);
    end
    strMode2 = this.Sonar.strMode_2{this.Sonar.Mode_2};
    if ~ischar(strMode2)
        strMode2 = num2str(strMode2);
    end
    str2 = sprintf('Calibration Tx Beams : %s : Mode=%d (%s) : Mode2=%d (%s)', ...
        this.str.SonarNames{this.Sonar.Ident}, ...
        this.Sonar.Mode_1, strMode1, ...
        this.Sonar.Mode_2, strMode2);
    
elseif flagConstructeur
    FigUtils.createSScFigure;
    Axe = gca;
    PlotUtils.createSScPlot(this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}, 'Axe', Axe);
    strMode1 = this.Sonar.strMode_1{this.Sonar.Mode_1};
    if ~ischar(strMode1)
        strMode1 = num2str(strMode1);
    end
    str2 = sprintf('Factory Tx Beams : %s : Mode1=%d (%s)', ...
        this.str.SonarNames{this.Sonar.Ident}, ...
        this.Sonar.Mode_1, strMode1);
    if ~isempty(this.Sonar.strMode_2)
        strMode2 = this.Sonar.strMode_2{this.Sonar.Mode_2};
        if ~ischar(strMode2)
            strMode2 = num2str(strMode2);
        end
        str2 = sprintf('%s : Mode2=%d (%s)', str2, this.Sonar.Mode_2, strMode2);
    end
    
elseif flagCalibration
    FigUtils.createSScFigure;
    Axe = gca;
    %     PlotUtils.createSScPlot(this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}, 'Axe', Axe, 'visuComposantes', 0);
    PlotUtils.createSScPlot(this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}, 'Axe', Axe, 'DrawComponents', 0);
    strMode1 = this.Sonar.strMode_1{this.Sonar.Mode_1};
    if ~ischar(strMode1)
        strMode1 = num2str(strMode1);
    end
    str2 = sprintf('Calibration Tx Beams : %s : Mode=%d (%s)', ...
        this.str.SonarNames{this.Sonar.Ident}, ...
        this.Sonar.Mode_1, strMode1);
    if ~isempty(this.Sonar.strMode_2)
        strMode2 = this.Sonar.strMode_2{this.Sonar.Mode_2};
        if ~ischar(strMode2)
            strMode2 = num2str(strMode2);
        end
        str2 = sprintf('%s : Mode2=%d (%s)', str2, this.Sonar.Mode_2, strMode2);
    end
end

if ~isempty(Title)
    str2 = sprintf('%s\n%s', Title, str2);
end
title(str2)
