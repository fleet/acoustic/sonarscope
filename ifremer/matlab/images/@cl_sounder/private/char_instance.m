function str = char_instance(this, varargin)

str = [];

%% Traitement

if ~isempty(this)
    [varargin, flagDisplay] = getPropertyValue(varargin, 'Display', this.Display); %#ok<ASGLU>
    
    str{end+1} = sprintf('Sonar.Ident               <-> %s', SelectionDansListe2str(this.Sonar.Ident, this.str.SonarNames, 'Num'));
    if (this.Sonar.Ident >= 1) && (this.Sonar.Ident <= length(this.str.SonarNames))
        str{end+1} = sprintf('Sonar.Name                <-- %s', this.str.SonarNames{this.Sonar.Ident});
    else
        str{end+1} = sprintf('Sonar.Name            <--');
    end
    if this.Sonar.Ident ~= 0
        str{end+1} = sprintf('Sonar.SystemSerialNumber  <-> %d', this.Sonar.SystemSerialNumber(1));
        str{end+1} = sprintf('Sonar.Family              <-- %s', SelectionDansListe2str(this.Sonar.Family, this.str.Family, 'Num'));
        str{end+1} = sprintf('Sonar.Mode_1              <-> %s', SelectionDansListe2str(this.Sonar.Mode_1, this.Sonar.strMode_1, 'Num'));
        str{end+1} = sprintf('Sonar.Mode_2              <-> %s', SelectionDansListe2str(this.Sonar.Mode_2, this.Sonar.strMode_2, 'Num'));
        
        str{end+1} = sprintf('Sonar.TypeMode_1          <-> %s', this.Sonar.TypeMode_1);
        str{end+1} = sprintf('Sonar.TypeMode_2          <-> %s', this.Sonar.TypeMode_2);
        str{end+1} = sprintf('Sonar.TypeMode_3          <-> %s', this.Sonar.TypeMode_3);
        
        str{end+1} = sprintf('Sonar.Freq                <-- %s (kHz)', num2strCode(this.Sonar.Freq));
        str{end+1} = sprintf('Sonar.BeamNb              <-- %s', num2strCode(this.Sonar.BeamNb));
        str{end+1} = sprintf('Sonar.ApertWth            <-- %f (deg)', this.Sonar.ApertWth);
        str{end+1} = sprintf('Sonar.TxBeamWth           <-- %f (deg)', this.Sonar.TxBeamWth);
        str{end+1} = sprintf('Sonar.LongBeamWth         <-- %f (deg)', this.Sonar.LongBeamWth);
        str{end+1} = sprintf('Sonar.TransBeamWth        <-- %f (deg)', this.Sonar.TransBeamWth);
        str{end+1} = sprintf('Sonar.MaxDepth            <-- %d (m)', this.Sonar.MaxDepth);
        if ~isempty(this.Sonar.Version)
            str{end+1} = sprintf('Sonar.Version             <-> %s', SelectionDansListe2str(this.Sonar.Version, this.str.Version ,'Num'));
        end
        
        str{end+1} = ' ';
        str{end+1} = sprintf('Signal.Type               <-- %s', SelectionDansListe2str(this.Signal.Type,  this.Signal.strType,'Num'));
        if isempty(this.Signal.Freq)
            str{end+1} = sprintf('Signal.Freq               <-- Defined in BeamForm.Tx.Freq');
        else
            str{end+1} = sprintf('Signal.Freq               <-- %f (kHz)', this.Signal.Freq);
        end
        
        if isfield(this.Signal, 'WaveLength') % Test si Subbottom pas encore bien int�gr� dans l'objet
            str{end+1} = sprintf('Signal.WaveLength         <-- %f (m)', this.Signal.WaveLength);
            str{end+1} = sprintf('Signal.DistanceFresnel    <-- %f (m)', this.Signal.DistanceFresnel);
            str{end+1} = sprintf('Signal.ChampLointain      <-- %f (m)', this.Signal.ChampLointain);
        end
        
        if isempty(this.Signal.Duration)
            str{end+1} = sprintf('Signal.Duration           <-- Defined in BeamForm.Tx.Duration');
        else
            str{end+1} = sprintf('Signal.Duration           <-- %f (ms)', this.Signal.Duration);
        end
        if isempty(this.Signal.BandWth)
            str{end+1} = sprintf('Signal.BandWth            <-- Defined in BeamForm.Tx.BandWth');
        else
            str{end+1} = sprintf('Signal.BandWth            <-- %f (kHz)', this.Signal.BandWth);
        end
        if isempty(this.Signal.Level)
            str{end+1} = sprintf('Signal.Level              <-- Defined in BeamForm.Tx.Level');
        else
            str{end+1} = sprintf('Signal.Level              <-- %f (dB/micro P/1m)', this.Signal.Level);
        end
        if isempty(this.Signal.GT)
            str{end+1} = sprintf('Signal.GT                 <-- Defined in BeamForm.Rx.GT');
        else
            str{end+1} = sprintf('Signal.GT                 <-- %f (dB)', this.Signal.GT);
        end
        if isempty(this.Signal.SH)
            str{end+1} = sprintf('Signal.SH                 <-- Defined in BeamForm.Tx.SH');
        else
            str{end+1} = sprintf('Signal.SH                 <-- %f (kHz)', this.Signal.SH);
        end
        str{end+1} = sprintf('Signal.Delay              <-- %f (ms)', this.Signal.Delay);
        
        str{end+1} = ' ';
        str{end+1} = sprintf('Array.Tx.Type             <-- %s', SelectionDansListe2str(this.Array.Tx.Type, this.str.ArrayType, 'Num'));
        str{end+1} = sprintf('Array.Tx.Lth              <-- %f (m)',   this.Array.Tx.Lth);
        str{end+1} = sprintf('Array.Tx.Wth              <-- %f (m)',   this.Array.Tx.Wth);
        str{end+1} = sprintf('Array.Tx.TransTilt        <-- %f (deg)', this.Array.Tx.TransTilt);
        str{end+1} = sprintf('Array.Tx.TransDirPat      <-- %s', SelectionDansListe2str(this.Array.Tx.TransDirPat, this.str.DirPat, 'Num'));
        str{end+1} = sprintf('Array.Rx.Type             <-- %s', SelectionDansListe2str(this.Array.Rx.Type, this.str.ArrayType, 'Num'));
        str{end+1} = sprintf('Array.Rx.Lth              <-- %f (m)',   this.Array.Rx.Lth);
        str{end+1} = sprintf('Array.Rx.Wth              <-- %f (m)',   this.Array.Rx.Wth);
        str{end+1} = sprintf('Array.Rx.TransTilt        <-- %f (deg)', this.Array.Rx.TransTilt);
        str{end+1} = sprintf('Array.Rx.TransDirPat      <-- %s', SelectionDansListe2str(this.Array.Rx.TransDirPat, this.str.DirPat, 'Num'));
        
        str{end+1} = ' ';
        str{end+1} = sprintf('BeamForm.Tx.LongWth       <-- %f (deg)', this.BeamForm.Tx.LongWth);
        str{end+1} = sprintf('BeamForm.Tx.TransWth      <-- %f (deg)', this.BeamForm.Tx.TransWth);
        
        str{end+1} = sprintf('BeamForm.Tx.LongShad      <-- %s', SelectionDansListe2str(this.BeamForm.Tx.LongShad,  this.str.Shad, 'Num'));
        if this.BeamForm.Tx.LongShad ~= 1
            str{end+1} = sprintf('BeamForm.Tx.LongShadParams    <-- [%s]', num2str(this.BeamForm.Tx.LongShadParams));
        end
        str{end+1} = sprintf('BeamForm.Tx.TransShad     <-- %s', SelectionDansListe2str(this.BeamForm.Tx.TransShad, this.str.Shad, 'Num'));
        if this.BeamForm.Tx.TransShad ~= 1
            str{end+1} = sprintf('BeamForm.Tx.TransShadParams <-- [%s]', num2str(this.BeamForm.Tx.TransShadParams));
        end
        str{end+1} = sprintf('BeamForm.Tx.Nb            <-- %d', this.BeamForm.Tx.Nb);
        str{end+1} = sprintf('BeamForm.Tx.CentAng       <-- %s', num2strCode(this.BeamForm.Tx.CentAng));
        str{end+1} = sprintf('BeamForm.Tx.LimitAng      <-- %s', num2strCode(this.BeamForm.Tx.LimitAng));
        str{end+1} = sprintf('BeamForm.Tx.Freq          <-- %s', num2strCode(this.BeamForm.Tx.Freq));
        str{end+1} = sprintf('BeamForm.Tx.Level         <-- %s', num2strCode(this.BeamForm.Tx.Level));
        str{end+1} = sprintf('BeamForm.Tx.BandWth       <-- %s', num2strCode(this.BeamForm.Tx.BandWth));
        str{end+1} = sprintf('BeamForm.Tx.Duration      <-- %s', num2strCode(this.BeamForm.Tx.Duration));
        str{end+1} = sprintf('BeamForm.Tx.SH            <-- %s', num2strCode(this.BeamForm.Tx.SH));
        str{end+1} = sprintf('BeamForm.Tx.TransDirPat   <-- %s', SelectionDansListe2str(this.BeamForm.Tx.TransDirPat, this.str.DirPat, 'Num'));
        
        str{end+1} = ' ';
        str{end+1} = sprintf('BeamForm.Rx.LongShad      <-- %s', SelectionDansListe2str(this.BeamForm.Rx.LongShad,  this.str.Shad, 'Num'));
        if this.BeamForm.Rx.LongShad ~= 1
            str{end+1} = sprintf('BeamForm.Rx.LongShadParams    <-- [%s]', num2str(this.BeamForm.Rx.LongShadParams));
        end
        str{end+1} = sprintf('BeamForm.Rx.TransShad     <-- %s', SelectionDansListe2str(this.BeamForm.Rx.TransShad, this.str.Shad, 'Num'));
        if this.BeamForm.Rx.TransShad ~= 1
            str{end+1} = sprintf('BeamForm.Rx.TransShadParams   <-- [%s]', num2str(this.BeamForm.Rx.TransShadParams));
        end
        str{end+1} = sprintf('BeamForm.Rx.LongWth       <-- %f (deg)', this.BeamForm.Rx.LongWth);
        str{end+1} = sprintf('BeamForm.Rx.TransWth      <-- %f (deg)', this.BeamForm.Rx.TransWth);
        str{end+1} = sprintf('BeamForm.Rx.Nb            <-- %d', this.BeamForm.Rx.Nb);
        str{end+1} = sprintf('BeamForm.Rx.Repart        <-- %s', SelectionDansListe2str(this.BeamForm.Rx.Repart, this.BeamForm.Rx.strRepart ,'Num'));
        str{end+1} = sprintf('BeamForm.Rx.Angles        <-- %s', num2strCode(this.BeamForm.Rx.Angles));
        str{end+1} = sprintf('BeamForm.Rx.GT            <-- %s', num2strCode(this.BeamForm.Rx.GT));
        
        str{end+1} = ' ';
        if isempty(this.Proc.TVGLaw)
            str{end+1} = sprintf('Proc.TVGLaw               <-- []');
        else
            str{end+1} = sprintf('Proc.TVGLaw.ConstructAlpha         <-- %f', this.Proc.TVGLaw.ConstructAlpha);
            str{end+1} = sprintf('Proc.TVGLaw.ConstructConstante     <-- %f', this.Proc.TVGLaw.ConstructConstante);
            str{end+1} = sprintf('Proc.TVGLaw.ConstructCoefDiverg    <-- %f', this.Proc.TVGLaw.ConstructCoefDiverg);
            str{end+1} = sprintf('Proc.TVGLaw.ConstructTypeCompens   <-- %f', this.Proc.TVGLaw.ConstructTypeCompens);
            str{end+1} = sprintf('Proc.TVGLaw.ConstructTable         <-- %s', num2strCode(this.Proc.TVGLaw.ConstructTable));
        end
        
        str{end+1} = sprintf('Proc.SampFreq             <-- %f (kHz)', this.Proc.SampFreq);
        str{end+1} = sprintf('Proc.RangeResol           <-- %f (m)',   this.Proc.RangeResol);
        str{end+1} = sprintf('Proc.ShipNoiseLevel       <-- %f (dB)',  this.Proc.ShipNoiseLevel);
        str{end+1} = sprintf('Proc.RecBandWth           <-- %s', num2strCode(this.Proc.RecBandWth));
        str{end+1} = sprintf('Proc.SoundNbPerPing       <-- %d', this.Proc.SoundNbPerPing);
        str{end+1} = sprintf('Proc.SoundNbPerBeam       <-- %d', this.Proc.SoundNbPerBeam);
        str{end+1} = sprintf('Proc.SampNbPerSound       <-- %d', this.Proc.SampNbPerSound);
        
        str{end+1} = ' ';
        str{end+1} = sprintf('Survey.Name               <-> %s', this.Survey.Name);
        str{end+1} = sprintf('Ship.Name                 <-> %s', this.Ship.Name);
        str{end+1} = sprintf('Ship.MRU.Name             <-> %s', this.Ship.MRU.Name);
        
        switch flagDisplay
            case 1
                str{end+1} = ' ';
                str{end+1} = sprintf('  EmissionModelsConstructeur  <-- %d ClModel', length(this.Emission.ModelsConstructeur));
                str{end+1} = sprintf('  EmissionModelsCalibration   <-- %d ClModel', length(this.Emission.ModelsCalibration));
                str{end+1} = sprintf('  ReceptionModelsConstructeur <-- %d ClModel', length(this.Reception.ModelsConstructeur));
                str{end+1} = sprintf('  ReceptionModelsCalibration  <-- %d ClModel', length(this.Reception.ModelsCalibration));
            case 2
                str{end+1} = ' ';
                str{end+1} = sprintf('--- aggregation Emission.ModelsConstructeur  (ClModel) ---');
                str{end+1} = char_short([this.Emission.ModelsConstructeur{:}]);
                str{end+1} = ' ';
                str{end+1} = sprintf('--- aggregation Emission.ModelsCalibration   (ClModel) ---');
                str{end+1} = char_short([this.Emission.ModelsCalibration{:}]);
                str{end+1} = ' ';
                str{end+1} = sprintf('--- aggregation Reception.ModelsConstructeur (ClModel) ---');
                str{end+1} = char_short(this.Reception.ModelsConstructeur);
                str{end+1} = ' ';
                str{end+1} = sprintf('--- aggregation Reception.ModelsCalibration  (ClModel) ---');
                str{end+1} = char_short(this.Reception.ModelsCalibration);
            case 3
                str{end+1} = sprintf('--- aggregation Emission.ModelsConstructeur  (ClModel) ---');
                str{end+1} = char([this.Emission.ModelsConstructeur{:}]);
            case 4
                str{end+1} = sprintf('--- aggregation Emission.ModelsCalibration   (ClModel) ---');
                str{end+1} = char([this.Emission.ModelsCalibration{:}]);
            case 5
                str{end+1} = sprintf('--- aggregation Reception.ModelsConstructeur (ClModel) ---');
                str{end+1} = char(this.Reception.ModelsConstructeur);
            case 6
                str{end+1} = sprintf('--- aggregation Reception.ModelsCalibration  (ClModel) ---');
                str{end+1} = char(this.Reception.ModelsCalibration);
        end
        str{end+1} = sprintf('Display                   <-> %s', SelectionDansListe2str(this.Display, this.str.Display ,'Num'));
        
    end
end

%% Concatenation en une chaine

str = cell2str(str);
