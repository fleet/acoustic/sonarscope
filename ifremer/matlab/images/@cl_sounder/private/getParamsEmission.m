function this = getParamsEmission(this, X)

ParamsSpecifsSimrad = zeros(this.BeamForm.Tx.Nb, 4);

%             if isfield(X.Sonar, 'Tilt')
%                 Tilt = X.Sonar.Tilt;
%             else
%                 Tilt = 0; % TODO : voir si il est d�fini ailleurs
%             end

AnglesDiag      = {};
DirectivityDiag = {};

Value = searchXML4ValLast(this, X.Sonar, 'Level');
if ~isempty(Value)
    ParamsSpecifsSimrad(:,1) = Value;
end
Value = searchXML4Val(this, X.Sonar, 'CentAng');
if ~isempty(Value)
    ParamsSpecifsSimrad(:,2) = Value;
end
Value = searchXML4Val(this, X.Sonar, 'BeamWidth');
if ~isempty(Value)
    ParamsSpecifsSimrad(:,3) = Value;
end
Value = searchXML4Val(this, X.Sonar, 'Inclination');
if ~isempty(Value)
    ParamsSpecifsSimrad(:,4) = Value;
end
Value = searchXML4Val(this, X.Sonar, 'FileNameDiagTx');
if ~isempty(Value)
    for k1=1:length(Value)
        FileNameDiagTx = getNomFicDatabase(Value{k1});
        if exist(FileNameDiagTx, 'file')
            switch get(this, 'Sonar.Name')
                case 'GeoSwath'
                    [Side, AnglesDiagFic, DirectivityDiagFig] = lecDirectivityGeoswath(this, FileNameDiagTx, 'Tilt', abs(ParamsSpecifsSimrad(k1,4)));
                    if Side == 1 % B�bord
                        AnglesDiag{1}      = AnglesDiagFic;
                        DirectivityDiag{1} = DirectivityDiagFig;
                    elseif Side == 2 % Tribord
                        AnglesDiag{2}      = AnglesDiagFic;
                        DirectivityDiag{2} = DirectivityDiagFig;
                    else
                        % Ce n'est pas une antenne
                        % correspondant � ce mod�le de Geoswath
                        % ou il n'y a pas de correspondance de
                        % dates
                    end
                otherwise
                    [AnglesDiag{k1}, DirectivityDiag{k1}] = lecDirectivity(this, FileNameDiagTx, 'Tilt', ParamsSpecifsSimrad(k1,4)); %#ok<AGROW>
            end
        end
    end
end

ParamsSpecifsCalibration = ParamsSpecifsSimrad;

% ParamsSpecifsSimradMin(:,1) =  ParamsSpecifsSimrad(:,1) - 10;
% ParamsSpecifsSimradMax(:,1) =  ParamsSpecifsSimrad(:,1) + 10;
% ParamsSpecifsSimradMin(:,2) =  ParamsSpecifsSimrad(:,2) - 10;
% ParamsSpecifsSimradMax(:,2) =  ParamsSpecifsSimrad(:,2) + 10;
% ParamsSpecifsSimradMin(:,3) =  ParamsSpecifsSimrad(:,3) - 10;
% ParamsSpecifsSimradMax(:,3) =  ParamsSpecifsSimrad(:,3) + 10;

ParamsSpecifsSimradMin(:,1) =  ParamsSpecifsSimrad(:,1) - 10;
ParamsSpecifsSimradMax(:,1) =  ParamsSpecifsSimrad(:,1) + 10;
ParamsSpecifsSimradMin(:,2) =  ParamsSpecifsSimrad(:,2) - 20;
ParamsSpecifsSimradMax(:,2) =  ParamsSpecifsSimrad(:,2) + 20;
ParamsSpecifsSimradMin(:,3) =  ParamsSpecifsSimrad(:,3) - 20;
ParamsSpecifsSimradMax(:,3) =  ParamsSpecifsSimrad(:,3) + 20;
if size(ParamsSpecifsSimrad,2) == 4 % Cas o� on dit que c'est un SincDep (DF1000), je sais, c'est pas pro
    ParamsSpecifsSimradMin(:,4) = ParamsSpecifsSimrad(:,4);
    ParamsSpecifsSimradMax(:,4) = ParamsSpecifsSimrad(:,4);
end

ParamsSpecifsCalibrationMin = ParamsSpecifsSimradMin;
%             ParamsSpecifsCalibrationMin(1:5, 4) = -40;
%             ParamsSpecifsCalibrationMin(6:10,4) = 0;

ParamsSpecifsCalibrationMax = ParamsSpecifsSimradMax;
%             ParamsSpecifsCalibrationMax(1:5, 4) = 0;
%             ParamsSpecifsCalibrationMax(6:10,4) = 40;

modeleIfremer = AntenneSincDepModel; %('XData', XData, 'YData', YData, 'ValParams', valParams);


% TODO : faire �a autrement (d�finition des mod�les dans les
% XML car bug si modes mixtes (un avec mod�le central
% sp�cifique et l'autre normal)
ParamsBeamCentral    = searchXML4Val(this, X.Sonar, 'ParamsBeamCentral');
TxDiagComp_TypeModel = searchXML4Val(this, X.Sonar, 'TxDiagComp_TypeModel');
if isempty(TxDiagComp_TypeModel)
    TxDiagComp_TypeModel = 'Poly';
end

switch TxDiagComp_TypeModel
    case 'SincDep'
        modeleConstructeur = AntenneSincDepModel;
    case 'Poly'
        modeleConstructeur = AntennePolyModel;
    case 'Spline'
        modeleConstructeur = AntenneSplineModel;
    case 'KjellFct'
        modeleConstructeur = AntenneEM3002Model;
    otherwise
        if ~isdeployed
            str1 = sprintf('Dev only (remplac� par AntennePolyModel) : Mod�le d''antenne "%s" pas encore int�gr� dans cl_sounder.', TxDiagComp_TypeModel);
            str2 = sprintf('Dev only (remplac� par AntennePolyModel) : Antenna model "%s" not yet implemented in cl_sounder.', TxDiagComp_TypeModel);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'TypeModelePasEncoreFait');
        end
        modeleConstructeur = AntennePolyModel;
end

if isempty(this.Sonar.Mode_2)
    % Pas de mode2, mais probl�me lors du calcul du diagramme
    % de directivit�
    noMode2 = true;
    this.Sonar.Mode_2 = 1;
end

switch TxDiagComp_TypeModel
    case {'SincDep'; 'Poly'; 'KjellFct'}
        if ~isempty(ParamsBeamCentral)
            %case {3, 22, 11} %EM300, EM302, EM120
            
            % -------------------------------------------------------------------------
            % Cas particulier du secteur central en mode 'Very Shallow', 'Medium' et 'Shallow'
            if any( strcmpi(this.Sonar.strMode_1(this.Sonar.Mode_1), {'Very Shallow', 'Medium' , 'Shallow'})) || ...
                    any( strcmpi(this.Sonar.strMode_2(this.Sonar.Mode_2), {'Very Shallow', 'Medium' , 'Shallow'}))
                
                k = ceil(size(ParamsSpecifsSimrad, 2) / 2);
                
                XData = this.BeamForm.Tx.LimitAng(k,1) : 0.5 : this.BeamForm.Tx.LimitAng(k,2);
                
                a = AntenneCentraleSimradModel('XData', XData, ...
                    'ValParams',    ParamsBeamCentral, ...
                    'valminParams', [-8       0       -50       -40      -1  -50], ...
                    'valmaxParams', [0        40        0        40      10    0], ...
                    'Enable', [1 1 1 1 0 0], ...
                    'visuComposantes', 2);
                
                this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a;
                if (size(this.Emission.ModelsCalibration, 1) < this.Sonar.Mode_1) || ...
                        (size(this.Emission.ModelsCalibration, 2) < this.Sonar.Mode_2) || ...
                        isempty(this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}) || ...
                        (length(this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}) < k)
                    this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}(k)  = a;
                    this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k)  = a;
                end
            end
        end
        
        for k=1:size(ParamsSpecifsSimrad,1)
            
            %% Mod�le polynomial
            
            a = modeleConstructeur;
            a = a.clone;
            
            if isempty(AnglesDiag)
                XData = this.BeamForm.Tx.LimitAng(k,1) : 0.5 : this.BeamForm.Tx.LimitAng(k,2); % TODO : passe � 0.1 et �valuer incidence sur PAMES
                a.XData = XData;
            else
                XData = AnglesDiag{k};
                YData = DirectivityDiag{k};
                a.XData = XData;
                a.YData = YData;
            end
            a.setParamsValueMinValueMaxValue(ParamsSpecifsSimrad(k,:), ParamsSpecifsSimradMin(k,:), ParamsSpecifsSimradMax(k,:));

            %{
            % Comment� par JMA le 19/04/2017
            %     this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a;
            if (size(this.Emission.ModelsConstructeur, 1) < this.Sonar.Mode_1) || ...
                    (size(this.Emission.ModelsConstructeur, 2) < this.Sonar.Mode_2) || ...
                    isempty(this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}) || ...
                    (length(this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}) < k)
                this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a.clone;
            else
                Dummy = this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k);
                if get_nbParams(Dummy) == 0
                    this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a.clone;
                end
            end
            %}
%             ModelsConstructeur(k) = a.clone; %#ok<AGROW> % R�introduit par JMA le 19/04/2017
            ModelsConstructeur(k) = a; %#ok<AGROW> % R�introduit par JMA le 19/04/2017
            
            %% Mod�le sinus cardinal d�point�
            
            a = modeleIfremer;
            a.XData = XData;
            a.setParamsValueMinValueMaxValue(ParamsSpecifsCalibration(k,:), ParamsSpecifsCalibrationMin(k,:), ParamsSpecifsCalibrationMax(k,:));

            ModelsCalibration(k) = a.clone; %#ok<AGROW> % R�introduit par JMA le 19/04/2017
        end
        try % Fa�on maladroite d'�viter bug pour modes inexistants (EX Very shallow EM122)
            this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2} = ModelsConstructeur;
            this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}  = ModelsCalibration;
        catch
        end
        
    case 'Spline'
        % D�j� d�fini dans set_KongsbergBsCorrSpline
        

        % Rajout� par JMA le 25/07/2016 pour l'EM2040
        for k=1:size(ParamsSpecifsSimrad,1)
            try % Test si d�j� initialis�
                this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k);
            catch % Pas encore initialis�
                XData = this.BeamForm.Tx.LimitAng(k,1) : 0.5 : this.BeamForm.Tx.LimitAng(k,2); % TODO : passe � 0.1 et �valuer incidence sur PAMES
                YData = zeros(size(XData));
                
                [xNodes, yNodes] = model2splineEM2040(ParamsSpecifsSimrad(k,:));
                a = AntenneSplineModel('XData', XData, 'YData', YData, 'xNodes', xNodes, 'yNodes', yNodes);
                
                %     this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a;
                if (size(this.Emission.ModelsConstructeur, 1) < this.Sonar.Mode_1) || ...
                        (size(this.Emission.ModelsConstructeur, 2) < this.Sonar.Mode_2) || ...
                        isempty(this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}) || ...
                        (length(this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}) < k)
                    this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a;
                else
                    Dummy = this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k);
                    if get_nbParams(Dummy) == 0
                        this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a;
                    end
                end
                                
                [xNodes, yNodes] = model2splineEM2040(ParamsSpecifsSimrad(k,:));
                a = AntenneSplineModel('XData', XData, 'YData', YData, 'xNodes', xNodes, 'yNodes', yNodes);
                
                if (size(this.Emission.ModelsCalibration, 1) < this.Sonar.Mode_1) || ...
                        (size(this.Emission.ModelsCalibration, 2) < this.Sonar.Mode_2) || ...
                        isempty(this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}) || ...
                        (length(this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}) < k)
                    this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a;
                else
                    Dummy = this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}(k);
                    if get_nbParams(Dummy) == 0
                        this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2}(k) = a;
                    end
                end
            end
        end
        
    otherwise
        str1 = sprintf('%s non pris en compte dans cl_sounder/provate/getParamsEmission', TxDiagComp_TypeModel);
        str2 = sprintf('%s not set in cl_sounder/provate/getParamsEmission yet', TxDiagComp_TypeModel);
        my_warndlg(Lang(str1,str2), 1);
end

if exist('noMode2', 'var') && noMode2
    % Pas de mode2, mais probl�me lors du calcul du diagramme
    % de directivit� -> on revient � l'�tat standard.
    this.Sonar.Mode_2 = [];
end


function [xNodes, yNodes] = model2splineEM2040(ParamsSpecifsSimrad)

xNodes(1) = ParamsSpecifsSimrad(2) - ParamsSpecifsSimrad(3) / 2;
xNodes(2) = ParamsSpecifsSimrad(2);
xNodes(3) = ParamsSpecifsSimrad(2) + ParamsSpecifsSimrad(3) / 2;
yNodes = zeros(size(xNodes));
