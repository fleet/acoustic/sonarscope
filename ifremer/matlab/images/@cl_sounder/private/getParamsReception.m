function this = getParamsReception(this)
% Calcul des diagramme de directivit� � la r�ception
% D�pend de chaque sondeur pour le moment.
if isempty(this.BeamForm.Rx.Angles)
    Params = zeros(this.Sonar.BeamNb, 4);
    Angles = getAnglesRecSimrad(1, this.Sonar.ApertWth, this.BeamForm.Rx.Repart, this.Sonar.BeamNb);
    Params(1:length(Angles),2) = Angles(:);
else
    Params = zeros(this.BeamForm.Rx.Nb, 4);
    Params(:,2) = this.BeamForm.Rx.Angles';
end
Params(:,3) = this.BeamForm.Rx.TransWth;
Params(:,4) = 0;

% TODO : ramener toutes ces d�finitions dans les XML !!!
switch this.str.SonarNames{this.Sonar.Ident(1)} %Ici sp�cificit� de chaque sondeur TODO : c'est pas top de faire �a !
    case 'EM12D'
        Params(1:81,4)                   = -this.Array.Tx.TransTilt;
        Params(82:this.BeamForm.Rx.Nb,4) =  this.Array.Tx.TransTilt;
    case 'EM12S'
    case {'EM300'; 'EM302'; 'EM120'; 'EM2000'; 'EM122'; 'EM710'; 'EM2040S'; 'EM2040D'; 'EM2045'}
        Params(:,4) = 0;
    case {'EM1000'; 'EM1002'}
        Params(:,4) = this.BeamForm.Rx.Angles';    % SAUF POUR LES PREMIERS ET LES DERNIERS : ANTENNE LINEAIRE
        this.Array.Rx.ArrayTilt = this.BeamForm.Rx.Angles';
    case 'SAR'
        Params(1,4) = -this.Array.Tx.TransTilt;
        Params(2,4) =  this.Array.Tx.TransTilt;
    case {'EM3000D'; 'EM3002D'}
        n = floor(this.BeamForm.Rx.Nb / 2);
        Params(1:n,4)                      = -this.Array.Tx.TransTilt;
        Params((n+1):this.BeamForm.Rx.Nb,4) =  this.Array.Tx.TransTilt;
    case {'EM3000S'; 'EM3002S'}
    case 'DF1000'
        Params(1,4) = -this.Array.Tx.TransTilt;
        Params(2,4) =  this.Array.Tx.TransTilt;
    case 'DTS1'
        Params(1,4) = -this.Array.Tx.TransTilt;
        Params(2,4) =  this.Array.Tx.TransTilt;
    case 'Klein3000'
        Params(1,4) = -this.Array.Tx.TransTilt;
        Params(2,4) =  this.Array.Tx.TransTilt;
    case 'GeoSwath'
        Params(1,4) = -this.Array.Tx.TransTilt;
        Params(2,4) =  this.Array.Tx.TransTilt;
    case {'Reson7150_12kHz'; 'Reson7150_24kHz'; 'Reson7111'; 'Reson7125_200kHz'}
        
    case {'Subbottom'; 'EK60'; 'ADCP'; 'HAC_Generic'}
        
    case {'ME70'; 'EM850'}
        
    otherwise
        str1 = sprintf('"%s" pas encore pr�vu dans cl_sounder/getParamsReception', this.str.SonarNames{this.Sonar.Ident(1)});
        str2 = sprintf('"%s" not set in cl_sounder/getParamsReception', this.str.SonarNames{this.Sonar.Ident(1)});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasEncorePlugged', 'TimeDelay', 60);
end

ParamsMin = Params;
ParamsMax = Params;

ParamsMin(:,1) = Params(:,1) - 10;
ParamsMax(:,1) = Params(:,1) + 10;
ParamsMin(:,2) = Params(:,2) - 10;
ParamsMax(:,2) = Params(:,2) + 10;
ParamsMin(:,3) = Params(:,3) - 10;
ParamsMax(:,3) = Params(:,3) + 10;

for k=1:size(Params,1)
    centre    = Params(k,2);
    ouverture = Params(k,3);
    XData = (centre-ouverture/2):0.1:(centre+ouverture/2);
    
    a = AntenneSincDepModel;
    a.XData = XData;
    a.setParamsValueMinValueMaxValue(Params(k,:), ParamsMin(k,:), ParamsMax(k,:));

    this.Reception.ModelsConstructeur(k) = a;
    this.Reception.ModelsCalibration(k)  = a;
end
