function this = initSounder(this)

persistent persistent_paramsFileName persistent_this

% INITSOUNDER - Initialisation des propri�t�s ind�pendantes du
% mode de fonctionnement du sondeur
% ----------------------

if isempty(persistent_paramsFileName) || isempty(persistent_this) || ~strcmp(this.paramsFileName, persistent_paramsFileName)
    X = xml_mat_read(this.paramsFileName); % Lecture du fichier de param�tres
    persistent_paramsFileName = this.paramsFileName;
else
%     this = persistent_this;
%     return

% TODO : j'ai d� revenir en arri�re car les mod�les sont effac�s dans le
% cas de l'EM710 (les mod�les sont lus dans getSounderParamsForcl_sounderXML
% Revoir cela en profondeur
    X = xml_mat_read(this.paramsFileName); % Lecture du fichier de param�tres
end

%% -- SONAR --
%this.Sonar.Ident                = 0;
this.Sonar.Constructeur          = searchXML4Val(this, X.Sonar, 'Company');
%this.Sonar.strVersion           = []; % Uniquement EM300
%this.Sonar.Version              = []; % Uniquement EM300
%this.Sonar.SystemSerialNumber	= -1;

this.Sonar.TypeMode_1 = searchXML4Val(this, X.Sonar, 'TypeMode1');
this.Sonar.TypeMode_2 = searchXML4Val(this, X.Sonar, 'TypeMode2');
this.Sonar.TypeMode_3 = searchXML4Val(this, X.Sonar, 'TypeMode3');

switch searchXML4Val(this, X.Sonar, 'Type')  % 'MBES', 'Side-Scan' or 'Interfero'
    case 'MBES'
        this.Sonar.Family = 2; % -> '2=Multibeam'
    case 'Side-Scan'
        this.Sonar.Family = 1; % -> '1=Sonar'
    case 'Interfero'
        this.Sonar.Family = 1; % -> '1=Sonar'
    case 'Subbottom'
        this.Sonar.Family = 3; % -> '3=Subbottom'
    case 'Fishing'
        this.Sonar.Family = 4; % -> '4=Fishing'
    case 'ADCP'
        this.Sonar.Family = 5; % -> '5=ADCP'
end

strMode_1 = {X.Sonar.Mode1(:).Name};
this.Sonar.strMode_1 = strMode_1(:);
if ~isempty(this.Sonar.Mode_2) % test si existence de sous-mode
    strMode_2 = {X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(:).Name};
    this.Sonar.strMode_2 = strMode_2(:);
end
%this.Sonar.Mode_1                   = 1;
%this.Sonar.Mode_2                   = 1;
this.Sonar.Freq          = searchXML4Val(this, X.Sonar, 'MainFrequency');
this.Sonar.BeamNb        = searchXML4Val(this, X.Sonar, 'RxNbBeams');
this.Sonar.ApertWth      = searchXML4Val(this, X.Sonar, 'MaxCoverageAngle') / 2; %/2?
%this.Sonar.TxBeamWth    = 0;
this.Sonar.LongBeamWth   = searchXML4Val(this, X.Sonar, 'TxAlongBeamWidth');
this.Sonar.TransBeamWth  = searchXML4Val(this, X.Sonar, 'RxAcrossBeamWidth');
this.Sonar.MaxDepth      = searchXML4Val(this, X.Sonar, 'MaxDepth');

%% -- SIGNAL --
%this.Signal.strType                 = {'CW'; 'Chirp'};    % A FINIR
SignalType = searchXML4Val(this, X.Sonar, 'SignalType');
if ~isempty(SignalType)
    if iscell(SignalType)
        SignalType = SignalType{1}; % TODO : rajout� le 20/02/2012 pour EM710 DualSwath Deep
    end
    switch SignalType
        case 'CW'
            this.Signal.Type = 1;
        case 'FM'
            this.Signal.Type = 2;
    end
end
this.Signal.Freq     = searchXML4Val(this, X.Sonar, 'MainFrequency');   % Frequence en kHz
this.Signal.Delay    = 0;
this.Signal.Duration = searchXML4Val(this, X.Sonar, 'PulseLength');   % Duree d'impulsion e, ms
this.Signal.BandWth  = searchXML4Val(this, X.Sonar, 'BandWth');
if isempty(this.Signal.BandWth) && ~isempty(this.Signal.Duration)
    this.Signal.BandWth  = 1 ./ this.Signal.Duration;   % Largeur de bande en kHz
end
this.Signal.Level = searchXML4Val(this, X.Sonar, 'Level');  % Niveau d'emission en dB
%this.Signal.GT      = [];   % Gain en sortie des antennes de reception en dB
%this.Signal.SH      = [];   % Sensibilite des antennes en dB
if ~isempty(this.Signal.Freq)
    this.Signal.WaveLength = (1500 / this.Signal.Freq) * 1e-3;    % en m
end

L = AntenneTeta2Long(this.Signal.WaveLength, searchXML4Val(this, X.Sonar, 'TxAlongBeamWidth'));   % en m

this.Signal.DistanceFresnel = (L^2) / (this.Signal.WaveLength); % en m
this.Signal.ChampLointain   = this.Signal.DistanceFresnel * 4;    % en m

%% -- ARRAY --
TxArrayType = searchXML4Val(this, X.Sonar, 'TxArrayType');
if ~isempty(TxArrayType)
    switch TxArrayType
        case 'Circular'
            this.Array.Tx.Type = 3; %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
        case 'Rectangular'
            this.Array.Tx.Type = 1; %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
    end
end
%this.Array.Tx.Lth                   = 0;
%this.Array.Tx.Wth                   = 0;
this.Array.Tx.TransTilt             = searchXML4Val(this, X.Sonar, 'Tilt');
%this.Array.Tx.TransDirPat           = 1;

RxArrayType = searchXML4Val(this, X.Sonar, 'RxArrayType');
if ~isempty(RxArrayType)
    switch RxArrayType
        case 'Circular'
            this.Array.Rx.Type = 3; %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
        case 'Rectangular'
            this.Array.Rx.Type = 1; %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
    end
end
%this.Array.Rx.Lth                   = 0;
%this.Array.Rx.Wth                   = 0;
this.Array.Rx.TransTilt             = searchXML4Val(this, X.Sonar, 'Tilt');
%this.Array.Rx.TransDirPat           = 1;

%% -- BEAMFORM --
this.BeamForm.Tx.LongWth             = searchXML4Val(this, X.Sonar, 'TxAlongBeamWidth');
this.BeamForm.Tx.TransWth            = searchXML4Val(this, X.Sonar, 'TxAcrossBeamWidth');
if isempty(this.BeamForm.Tx.TransWth )
    this.BeamForm.Tx.TransWth        = searchXML4Val(this, X.Sonar, 'MaxCoverageAngle');
end
%this.BeamForm.Tx.LongShad           = 1;
%this.BeamForm.Tx.LongShadParams     = [];
%this.BeamForm.Tx.TransShad          = 1;
%this.BeamForm.Tx.TransShadParams    = [];
this.BeamForm.Tx.Nb                  = searchXML4Val(this, X.Sonar, 'TxNbBeams');
this.BeamForm.Tx.CentAng             = searchXML4Val(this, X.Sonar, 'CentAng');
this.BeamForm.Tx.Inclination         = searchXML4Val(this, X.Sonar, 'Inclination');


LimitAng = searchXML4Val(this, X.Sonar, 'LimitAng'); % TODO 05/04/2010 : LimitAng pas r�cup�r� pour tous les secteurs


if ~isempty(LimitAng)
    this.BeamForm.Tx.LimitAng        = reshape(LimitAng, 2, this.BeamForm.Tx.Nb)';
end
this.BeamForm.Tx.Freq                = searchXML4Val(this, X.Sonar, 'Freq'); % Frequence du faisceau si different de                     this.Signal.Freq
%this.BeamForm.Tx.Level              = []; % Niveau d'emission par faisceau si different de            this.Signal.Level
%this.BeamForm.Tx.BandWth            = []; % Largeur de bande par faisceau d'emission si different de  this.Signal.BandWth
%this.BeamForm.Tx.Duration           = []; % Duree d'impulsion par faisceau d'emission si different de this.Signal.Duration
%this.BeamForm.Tx.SH                 = []; % Sensibilite des antennes si different de                  this.Signal.SH
%this.BeamForm.Tx.TransDirPat        = 1;

this.BeamForm.Rx.LongWth             = searchXML4Val(this, X.Sonar, 'RxAlongBeamWidth');
this.BeamForm.Rx.TransWth            = searchXML4Val(this, X.Sonar, 'RxAcrossBeamWidth');
%this.BeamForm.Rx.LongShad           = 1;
%this.BeamForm.Rx.LongShadParams     = [];
%this.BeamForm.Rx.TransShad          = 1;
%this.BeamForm.Rx.TransShadParams    = [];
this.BeamForm.Rx.Nb                 = length(searchXML4Val(this, X.Sonar, 'RxAngles'));
%this.BeamForm.Rx.strRepart          = {'Isodistance'; 'IsoAngle'; 'InBetween'};
RxRepartition = searchXML4Val(this, X.Sonar, 'RxRepartition');
if ~isempty(RxRepartition)
    switch RxRepartition
        case 'IsoDistance'
            this.BeamForm.Rx.Repart = 1;
        case 'IsoAngle'
            this.BeamForm.Rx.Repart = 2;
        otherwise
            this.BeamForm.Rx.Repart = 3;
    end
end
this.BeamForm.Rx.Angles = searchXML4Val(this, X.Sonar, 'RxAngles');
%this.BeamForm.Rx.GT    = [];

%% -- PROC --
%this.Proc.TVGLaw                     = [];
%this.Proc.TVGLaw.ConstructTable      = [];
this.Proc.TVGLaw.ConstructAlpha       = searchXML4Val(this, X.Sonar, 'ConstructAlpha');
this.Proc.TVGLaw.ConstructConstante   = searchXML4Val(this, X.Sonar, 'ConstructConstante');
this.Proc.TVGLaw.ConstructCoefDiverg  = searchXML4Val(this, X.Sonar, 'ConstructCoefDiverg');
this.Proc.TVGLaw.ConstructTypeCompens = searchXML4Val(this, X.Sonar, 'ConstructTypeCompens');

this.Proc.SampFreq = searchXML4Val(this, X.Sonar, 'SamplingFrequency')*1e3;
if ~isempty(this.Proc.SampFreq)
    this.Proc.RangeResol = 1500 ./ (2*this.Proc.SampFreq);
end
%this.Proc.ShipNoiseLevel = 0;
%this.Proc.RecBandWth     = 0;
%this.Proc.SoundNbPerPing = 0;
%this.Proc.SoundNbPerBeam = 0;
%this.Proc.SampNbPerSound = 0;

%% -- Ship --
if isfield(X, 'Vessel')
    this.Ship.Name           = searchXML4Val(this, X.Vessel, 'Name');
    this.Ship.selfNoise      = searchXML4Val(this, X.Vessel, 'selfNoise');
    this.Ship.sonarHeadDepth = searchXML4Val(this, X.Vessel, 'SonarHeadDepth');
    if isfield(X.Vessel, 'MRU')
        this.Ship.MRU.Name          = searchXML4Val(this, X.Vessel.MRU, 'Name');
        this.Ship.MRU.HeadAccuracy  = searchXML4Val(this, X.Vessel.MRU, 'HeadAccuracy');
        this.Ship.MRU.PitchAccuracy = searchXML4Val(this, X.Vessel.MRU, 'PitchAccuracy');
        this.Ship.MRU.RollAccuracy  = searchXML4Val(this, X.Vessel.MRU, 'RollAccuracy');
        this.Ship.MRU.HeaveAccuracy = searchXML4Val(this, X.Vessel.MRU, 'HeaveAccuracy');
        this.Ship.MRU.YawAccuracy   = searchXML4Val(this, X.Vessel.MRU, 'YawAccuracy');
    end
end

%% Emission

this = getParamsEmission(this, X);

%% Reception

if ~isdeployed % Test JMA le 24/05/2017 car beaucoup de temps pris par le clonage des mod�les : action Pierre Mahoudo
%     this = getParamsReception(this);
end

persistent_this = this;
