% Lecture des fichiers de directivite de l'EdgeTech DF1000
% les fichiers sont dans le meme rep et sont donne par la syntaxe FreqSideDir.txt
% ou Freq : Hf  | BF
%    Side : bab | tri
%    Dir  : d1  | d2  ( longitudinal | transversal )
%
% Syntax
%   [z, ...] = lecDirectivity(varargin)
%
% Input Arguments
%
% Name-Value Pair Arguments
%
% Name-Value Pair Arguments
%   'nomFic'      : Nom du fichier de directivite (FreqSideDir.txt)
%
% Output Arguments
%   []            : Auto-plot activation
%   Angles        : Angles.
%   Directivity   : directivite en dB.
%   Bande         : Ouverture a 3dB
%
% Remarks : le maximum est au point (70 degre, O dB)
%
% Examples
%   figure;
%   nomFicBab = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lfbabd1.txt';
%   lecDirectivity('nomFic', nomFicBab);
%
%   nomFicTri = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lftrid1.txt';
%   hold on; lecDirectivity('nomFic', nomFicTri);
%
%   figure;
%   nomFicBab = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lfbabd2.txt';
%   lecDirectivity('nomFic', nomFicBab);
%
%   nomFicTri = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lftrid2.txt';
%   hold on; lecDirectivity('nomFic', nomFicTri);
%
%   [Angles, Directivity, Bande] = lecDirectivity(nomFic);
%
% See also lecDirectivity
% Authors : GLC
%-------------------------------------------------------------------------------

function varargout = lecDirectivity(~, nomFic, varargin)

[varargin, Tilt] = getPropertyValue(varargin, 'Tilt', 0); %#ok<ASGLU>

%% Importation des donnees

Data = importdata(nomFic);
if isstruct(Data)
    Angles      = Data.data(:,1);
    Directivity = Data.data(:,2);
    if length(Data.textdata) == 1
        Mots = strsplit(Data.textdata{1});
        TypeData = Mots{2};
    else
        TypeData = Data.textdata{2};
    end
    switch lower(TypeData)
        case 'amp'
            maxi             = max(Directivity);
            Directivity_norm = reflec_Amp2dB(Directivity ./ maxi);
        case 'enr'
            maxi             = max(Directivity);
            Directivity_norm = reflec_Enr2dB(Directivity ./ maxi);
        case 'db'
            Directivity_norm = Directivity;
        otherwise
            my_warndlg(Lang(str1,str2), 1);
    end
else
    my_warndlg(Lang(str1,str2), 1);
    Angles      = Data(:,1);
    Directivity = Data(:,2);
    maxi             = max(Directivity);
    Directivity_norm = reflec_Amp2dB(Directivity ./ maxi);
end

%% Les angles sont ramenes entre -180 et 180 puis on dcale la courbe (le max a 0 degre)

subsup = find(Angles > 180 & Angles < 360);
subinf = find(Angles <= 180);

if contains(nomFic, 'bab') % Babord
    Angles_COR      = [(Angles(subsup) - 360); Angles(subinf)];
    Directivity_COR = [Directivity_norm(subsup); Directivity_norm(subinf)];
    
    % --------------------------------------------------
    % Max a 0 degre
    
    [~, imaxi] = max(Directivity_COR);
    Angles_COR    = Angles_COR - Angles_COR(imaxi);
    
    % --------------------------------------------------
    % Depointage de 70 degres vers babord
    
    Angles_COR = Angles_COR + Tilt;
    LineStyle = 'b';
else
    Angles_COR = [Angles(subinf) - 180; Angles(subsup) - 180];
    Directivity_COR = [flipud(Directivity_norm(subinf)); flipud(Directivity_norm(subsup))];
    
    % --------------------------------------------------
    % Max a 0 degre
    
    [~, imaxi] = max(Directivity_COR);
    Angles_COR    = Angles_COR - Angles_COR(imaxi);
    
    % --------------------------------------------------
    % Depointage de 70 degres vers tribord
    
    Angles_COR = Angles_COR + Tilt;
    LineStyle = 'r';
end

%% Ouverture a -3 dB

[maxiDirec, imaxiDirec]   = max(Directivity_COR)   ;
subinf = 1 : imaxiDirec;
subsup = imaxiDirec + 1 : length(Directivity_COR);

[~, b] = unique(Directivity_COR(subinf));
b = sort(b);
mini = interp1(Directivity_COR(subinf(b)), Angles_COR(subinf(b)), maxiDirec-3);

[~, b] = unique(Directivity_COR(subsup));
b = sort(b);
maxi = interp1(Directivity_COR(subsup(b)), Angles_COR(subsup(b)), maxiDirec-3);
Bande = maxi - mini;


% ------
% Sortie

if nargout == 0
    subplot(1, 2, 1)
    axes(gca); hold on
    plot(Angles_COR, Directivity_COR, LineStyle); grid on
    xlabel('Degres', 'FontSize', 11);
    ylabel('dB'    , 'FontSize', 11);
    
    subplot(1, 2, 2)
    axes(gca);
    %               Polar_GLC(Angles_COR * pi/180, Directivity_COR, LineStyle); grid on
    polarplot(Angles_COR * (pi/180), reflec_dB2Amp(Directivity_COR), LineStyle); hold on
    
    mots = strsplit(nomFic, '/');
    
    strTitle{1} = sprintf('File : %s', mots{end});
    strTitle{2} = sprintf('Ouverture : %s', num2str(Bande));
    title (strTitle, 'FontSize', 13);
else
    varargout{1} = Angles_COR;
    varargout{2} = Directivity_COR;
    varargout{3} = Bande;
end

