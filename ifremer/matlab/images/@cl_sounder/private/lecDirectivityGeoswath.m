% Lecture des fichiers de directivite de l'EdgeTech DF1000
% les fichiers sont dans le meme rep et sont donne par la syntaxe FreqSideDir.txt
% ou Freq : Hf  | BF
%    Side : bab | tri
%    Dir  : d1  | d2  ( longitudinal | transversal )
%
% Syntax
%   [z, ...] = lecDirectivityGeoswath(varargin)
%
% Input Arguments
%
% Name-Value Pair Arguments
%
% Name-Value Pair Arguments
%   'nomFic' : Nom du fichier de directivite (FreqSideDir.txt)
%
% Output Arguments
%   []          : Auto-plot activation
%   Angles      : Angles.
%   Directivity : directivite en dB.
%   Bande       : Ouverture a 3dB
%
% Remarks : le maximum est au point (70 degre, O dB)
%
% Examples
%   figure;
%   nomFicBab = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lfbabd1.txt';
%   lecDirectivityGeoswath('nomFic', nomFicBab);
%
%   nomFicTri = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lftrid1.txt';
%   hold on; lecDirectivityGeoswath('nomFic', nomFicTri);
%
%   figure;
%   nomFicBab = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lfbabd2.txt';
%   lecDirectlecDirectivityGeoswathivity('nomFic', nomFicBab);
%
%   nomFicTri = '/home1/doppler/augustin/MatlabToolboxIfremer/sonars/@cl_car_sonar/private/Lftrid2.txt';
%   hold on; lecDirectivityGeoswath('nomFic', nomFicTri);
%
%   [Angles, Directivity, Bande] = lecDirectivityGeoswath(nomFic);
%
% See also lecDirectivity
% Authors : GLC
%-------------------------------------------------------------------------------

function [Side, Angles_COR, Directivity_COR, Bande] = lecDirectivityGeoswath(~, nomFic, varargin)

[varargin, Tilt] = getPropertyValue(varargin, 'Tilt', 0); %#ok<ASGLU>

%% Type de Fichier

%{
- Les transducteurs actuellement en place (depuis f�vrier 2010) :
17823 � tribord
19401 � b�bord
%}

if contains(nomFic, '17823')
    Side = 2; % Tribord B�bord
elseif contains(nomFic, '19401')
    Side = 1; % B�bord
else
    str1 = sprintf('Le fichier "%s" n''est pas identifi� dans cl_sounder/lecDirectivityGeoswath', nomFic);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierNonIdentified');
end


%% Importation des donnees

Data = importdata(nomFic);
if isstruct(Data)
    Angles      = Data.data(:,1);
    Directivity = Data.data(:,2);
    if length(Data.textdata) == 1
        Mots = strsplit(Data.textdata{1});
        TypeData = Mots{2};
    else
        TypeData = Data.textdata{2};
    end
    switch lower(TypeData)
        case 'amp'
            maxi             = max(Directivity);
            Directivity_norm = reflec_Amp2dB(Directivity ./ maxi);
        case 'enr'
            maxi             = max(Directivity);
            Directivity_norm = reflec_Enr2dB(Directivity ./ maxi);
        case 'db'
            Directivity_norm = Directivity;
        otherwise
            my_warndlg(Lang(str1,str2), 1);
    end
else
    my_warndlg(Lang(str1,str2), 1);
    Angles      = Data(:,1);
    Directivity = Data(:,2);
    maxi             = max(Directivity);
    Directivity_norm = reflec_Amp2dB(Directivity ./ maxi);
end

%% Attribution b�bord / tribord

if Side == 1 % Babord
    Directivity_COR = Directivity_norm;
    Angles_COR = -Angles - Tilt;
    %                 Directivity_COR = flipud(Directivity_norm);
    %                 Angles_COR = flipud(Angles) - Tilt;
    LineStyle = 'r';
else % Tribord
    Directivity_COR = Directivity_norm;
    Angles_COR = Angles + Tilt;
    %                 Directivity_COR = flipud(Directivity_norm);
    %                 Angles_COR = flipud(Angles) + Tilt;
    LineStyle = 'g';
end

%% Ouverture a -3 dB

[maxiDirec, imaxiDirec]   = max(Directivity_COR)   ;
subinf = 1 : imaxiDirec;
subsup = imaxiDirec + 1 : length(Directivity_COR);

[~, b] = unique(Directivity_COR(subinf));
b = sort(b);
mini = interp1(Directivity_COR(subinf(b)), Angles_COR(subinf(b)), maxiDirec-3);

[~, b] = unique(Directivity_COR(subsup));
b = sort(b);
maxi = interp1(Directivity_COR(subsup(b)), Angles_COR(subsup(b)), maxiDirec-3);
Bande = maxi - mini;

% ------
% Sortie

if nargout == 0
    figure
    subplot(1, 2, 1)
    axes(gca); hold on
    plot(Angles_COR, Directivity_COR, LineStyle); grid on
    xlabel('Degres', 'FontSize', 11);
    ylabel('dB'    , 'FontSize', 11);
    
    subplot(1, 2, 2)
    axes(gca);
    %               Polar_GLC(Angles_COR * pi/180, Directivity_COR, LineStyle); grid on
    polarplot(Angles_COR * (pi/180), reflec_dB2Amp(Directivity_COR), LineStyle); hold on
    
    mots = strsplit(nomFic, '/');
    
    strTitle{1} = sprintf('File : %s', mots{end});
    strTitle{2} = sprintf('Ouverture : %s', num2str(Bande));
    title (strTitle, 'FontSize', 13);
end

