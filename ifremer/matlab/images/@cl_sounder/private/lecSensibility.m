function varargout = lecSensibility(this, nomFic) %#ok

Data        = importdata(nomFic);

Frequence   = Data(:,1);
Sensibility = Data(:,4);

% Normalisation par rapport au max
[maxiSensi, imaxiSensi]   = max(Sensibility);


subinf = 1 : imaxiSensi;
subsup = imaxiSensi+ 1 : length(Sensibility);

mini = interp1(Sensibility(subinf), Frequence(subinf), maxiSensi-3, 'linear', 'extrap');
maxi = interp1(Sensibility(subsup), Frequence(subsup), maxiSensi-3, 'linear', 'extrap');
Bande = maxi - mini;

nomFicSH = nomFic;
sub = strfind(nomFic, 'svc');
nomFicSH(sub :sub+2) = 'shc';

DataSH        = importdata( nomFicSH );

FreqSH = DataSH(:,1);
SH_Val = DataSH(:,4);
SH = interp1(FreqSH, SH_Val, Frequence(imaxiSensi));

Bande     = Bande / 1000;
Frequence = Frequence / 1000;
mini      = mini / 1000;
maxi      = maxi / 1000;

if nargout == 0
    figure;
    
    plot(Frequence, Sensibility, 'b'); grid on
    xlabel('Frequence (kHz)', 'FontSize', 11);
    ylabel('dB'    , 'FontSize', 11);
    
    YLim = get(gca, 'YLim');
    hold on;
    plot( [mini, mini], YLim, 'r')
    plot( [maxi, maxi], YLim, 'r')
    hold off
    
    strTitle{1} = sprintf('File : %s', nomFic);
    strTitle{2} =  sprintf('Resonance : %s kHz, (SV, SH) : (%s, %s) dB, Bande : %s kHz', ...
        num2str((Frequence(imaxiSensi))), num2str((maxiSensi)), num2str((SH)), num2str((Bande)));
    title(strTitle, 'Interpreter', 'none', 'FontSize', 13);
    
else
    
    varargout{1} =  Bande;
    varargout{2} =  Frequence(imaxiSensi);
    varargout{3} =  SH;
    
end
