% R�cup�ration des valeurs sur les trois
% niveaux possibles d'un fichier XML de description de sondeur

function value = searchXML4Val(this, structName, fieldName)

value = [];

if isfield(structName, fieldName) &&...
        ~isempty(structName.(fieldName))
    % A la racine de la structure pass�e en entr�e
    value = structName.(fieldName);
    
elseif ~isempty(this.Sonar.Mode_1) && isfield(structName, 'Mode1') && ...
        isfield( structName.Mode1(this.Sonar.Mode_1), fieldName) &&...
        ~isempty(structName.Mode1(this.Sonar.Mode_1).(fieldName))
    % sinon dans <mode1>
    value = structName.Mode1(this.Sonar.Mode_1).(fieldName);
    
elseif ~isempty(this.Sonar.Mode_1) && isfield(structName, 'Mode1') &&...
        ~isempty(this.Sonar.Mode_2) && isfield(structName.Mode1(this.Sonar.Mode_1), 'Mode2') &&...
        isfield( structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2), fieldName) &&...
        ~isempty(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).(fieldName))
    % ou encore dans <mode2>
    value = structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).(fieldName);
    
elseif ~isempty(this.Sonar.Mode_1) && isfield(structName, 'Mode1') &&...
        ~isempty(this.Sonar.Mode_2) && isfield(structName.Mode1(this.Sonar.Mode_1), 'Mode2') &&...
        ~isempty(this.Sonar.Mode_3) && isfield(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2), 'Mode3') &&...
        ~isempty(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3) && ...
        isfield(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3), fieldName) &&...
        ~isempty(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3).(fieldName))
    % ou encore dans <mode>
    value = structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3).(fieldName);
    
    % TxBeam
elseif isfield(structName, 'TxBeam') &&...
        isfield(structName.TxBeam, fieldName)
    if ischar(structName.TxBeam(1).(fieldName))
        value = {structName.TxBeam.(fieldName)};
    else
        value = [structName.TxBeam.(fieldName)];
    end
    
elseif ~isempty(this.Sonar.Mode_1) && isfield(structName, 'Mode1') &&...
        isfield(structName.Mode1(this.Sonar.Mode_1), 'TxBeam') &&...
        isfield(structName.Mode1(this.Sonar.Mode_1).TxBeam, fieldName)
    if ischar(structName.Mode1(this.Sonar.Mode_1).TxBeam(1).(fieldName))
        value = {structName.Mode1(this.Sonar.Mode_1).TxBeam.(fieldName)};
    else
        value = [structName.Mode1(this.Sonar.Mode_1).TxBeam.(fieldName)];
    end
    
elseif ~isempty(this.Sonar.Mode_1) && isfield(structName, 'Mode1') &&...
        ~isempty(this.Sonar.Mode_2) && isfield(structName.Mode1(this.Sonar.Mode_1), 'Mode2') &&...
        isfield(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2), 'TxBeam') &&...
        isfield(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).TxBeam, fieldName)
    if ischar(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).TxBeam(1).(fieldName))
        value = {structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).TxBeam.(fieldName)};
    else
        value = [structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).TxBeam.(fieldName)];
    end
    
elseif ~isempty(this.Sonar.Mode_1) && isfield(structName, 'Mode1') &&...
        ~isempty(this.Sonar.Mode_2) && isfield(structName.Mode1(this.Sonar.Mode_1), 'Mode2') &&...
        ~isempty(this.Sonar.Mode_3) && isfield(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2), 'Mode3') &&...
        ~isempty(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3) && ...
        isfield( structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3), 'TxBeam') &&...
        isfield( structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3).TxBeam, fieldName)
    if ischar(structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3).TxBeam(1).(fieldName))
        value = {structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3).TxBeam.(fieldName)};
    else
        value = [structName.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(this.Sonar.Mode_3).TxBeam.(fieldName)];
    end
end
