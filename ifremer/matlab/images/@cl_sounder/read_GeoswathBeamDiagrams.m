% nomFic = getNomFicDatabase('Geoswath_20100201.txt')
% [flag, Angles, Gains, Names, Dates, Sides] = read_GeoswathBeamDiagrams(cl_sounder([]), nomFic)
function [flag, Angles, Gains, Names, Dates, Sides] = read_GeoswathBeamDiagrams(~, nomFic)

flag   = 0;
Angles = [];
Gains  = [];
Names  = [];
Dates  = [];
Sides  = [];

if isempty(nomFic)
    str1 = 'Aucun fichier n''est sp�cifi� dans "read_GeoswathBeamDiagrams".';
    str2 = 'No file is specified in "read_GeoswathBeamDiagrams".';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Ouverture du fichier

fid = fopen(nomFic, 'r');
if fid == -1
    return
end

%% Lecture des dates

Ligne = fgetl(fid);
Mots  = strsplit(Ligne);
if ~strcmp(Mots{1}, 'Dates')
    return
end
nbAntennas = length(Mots) - 1;
for k=1:nbAntennas
    if strcmp(Mots{k+1}, 'None')
        Dates(k) = NaN; %#ok<AGROW>
    else
        yyyy = str2double(Mots{k+1}(1:4));
        mm   = str2double(Mots{k+1}(5:6));
        dd   = str2double(Mots{k+1}(7:8));
        dIfr = dayJma2Ifr(dd, mm, yyyy);
        T = cl_time('timeIfr', dIfr, 0);
        Dates(k) = T.timeMat; %#ok<AGROW>
    end
    Names = Mots(2:end);
end

%% Lecture des C�t�s

Ligne = fgetl(fid);
Mots  = strsplit(Ligne);
if ~strcmp(Mots{1}, 'Sides')
    return
end
for k=1:nbAntennas
    switch Mots{k+1}
        case 'None'
            Sides(k) = NaN; %#ok<AGROW>
        case 'Port'
            Sides(k) = -1; %#ok<AGROW>
        case 'Starboard'
            Sides(k) = 1; %#ok<AGROW>
    end
end

%% Lecture des noms d'antennes

Ligne = fgetl(fid);
Mots  = strsplit(Ligne);
if ~strcmp(Mots{1}, 'Angles')
    return
end
Names = Mots(2:end);

%% Lecture des gains de directivit�

nbPoints = 0;
while 1
    Ligne = fgetl(fid);
    if ~ischar(Ligne)
        break
    end
    Mots = strsplit(Ligne);
    nbPoints = nbPoints + 1;
    Angles(nbPoints,1) = str2double(Mots{1}); %#ok<AGROW>
    for k=1:nbAntennas
        Mots{1+k} = strrep(Mots{1+k}, ',', '.');
        Gains(nbPoints,k) = str2double(Mots{1+k}); %#ok<AGROW>
    end
end

%% Fermeture du fichier

fclose(fid);

%% Affichage si pas de variables de sortie

if nargout == 0
    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(Angles, Gains); grid on
    xlabel('Anglers (deg)');
    ylabel('Gains');
    legend(Names)
    
    strTitle{1} = sprintf('File : %s', nomFic);
    title(strTitle, 'Interpreter', 'none');
end
