function varargout = resol_long(this, D)
% Calcul de la resolution longitudinale
%
% Syntax
%   R = resol_long(a, D)
%
% INTPUT PARAMETERS :
%   a : Une instance de cl_sounder
%   D : Distance oblique (m)
%
% OUTTPUT PARAMETERS :
%   R : Resolution longitudinale (m)
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   R = resol_long(a, 0:100)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

%     AngleLongitudinal = get(this, 'Sonar.LongBeamWth');
AngleLongitudinal = this.Sonar.LongBeamWth;

% -----------------------------------------------
% Cr�ation de l'image de r�solution longitudinale

R = D * 2 * tand(AngleLongitudinal/2);

%% Par ici la sortie

if nargout == 0
    [n, m] = size(D);
    if (n == 1) && (m ==1)
        varargout{1} = R;   % on force l'affichage
    elseif (n ~= 1) && (m ~=1)
        figure;
        imagesc(R); colorbar
        title('Longitudinal Resolution (m)')
    else
        figure;
        plot(D, R); grid on
        title('Longitudinal Resolution')
        xlabel('Oblique Distance (m)')
        ylabel('Longitudinal Resolution (m)')
    end
else
    varargout{1} = R;
end

