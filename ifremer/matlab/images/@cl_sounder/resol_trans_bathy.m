function varargout = resol_trans_bathy(this, H, E)
% Calcul de la resolution transversale de la bathymetrie
%
% Syntax
%   R = resol_trans_bathy(a, H, E)
%
% INTPUT PARAMETERS :
%   a : Une instance de cl_sounder
%   H : Bathymetrie (m)
%   E : Angle d'emission (deg)
%
% Name-only Arguments
%    :
%
% OUTTPUT PARAMETERS :
%   R : Resolution longitudinale
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   E = 0:75;
%   H = ones(size(E)) * 100;
%   R = resol_trans_bathy(a, H, E)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

BeamFormTxTransWth = this.BeamForm.Tx.TransWth;
BeamFormRxTransWth = this.BeamForm.Rx.TransWth;
OuvertureTransvEmissionReception = AntenneOuvEmiRec(BeamFormTxTransWth, BeamFormRxTransWth);

% -----------------------
% Calcul de la resolution

w = warning;
warning('off', 'all')
R = (OuvertureTransvEmissionReception * pi / 180) * H ./ (cos(E * pi / 180) .^ 2);
warning(w)

% -----------------
% Par ici la sortie

if nargout == 0
    [n, m] = size(R);
    if (n == 1) && (m ==1)
        varargout{1} = R;   % on force l'affichage
    elseif (n ~= 1) && (m ~=1)
        figure;
        imagesc(R); colorbar
        title('Trans Bathy Resol (m)')
    else
        figure;
        plot(R); grid on
        title('Longitudinal Resolution')
        xlabel('Oblique Distance (m)')
        ylabel('Trans Bathy Resol (m)')
    end
else
    varargout{1} = R;
end

