function varargout = resol_trans_image(this, D, E)
% Calcul de la resolution transversale de l'imagerie
%
% Syntax
%   R = resol_trans_image(a, D, E)
%
% INTPUT PARAMETERS :
%   a : Une instance de cl_sounder
%   D : Distance oblique (m)
%   E : Angle d'emission (deg)
%
% OUTTPUT PARAMETERS :
%   R : Resolution longitudinale (m)
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   E = 0:75;
%   D = ones(size(E)) * 100;
%   R = resol_trans_image(a, D, E)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

AngleTransversal = this.BeamForm.Rx.TransWth;
BandWth = get_BandWth(this);

% On simplifie (de toute facon, 1500 est faux aussi)
BandWth = mean(BandWth);

Deltax =  1500 / (2 * BandWth * 1E3);

%% Calcul de la résolution

E = E * pi / 180;
w = warning;
warning('off', 'all')
R1 = Deltax ./ sin(E);
R2 = 2 * tan(AngleTransversal * (pi /360)) * D ./ cos(E);
warning(w)
R = min(R1,R2);

if this.Sonar.Family == 1   % Cas du sonar lateral
    R3 = sqrt(D .* cos(E) * (2*Deltax));
    R = min(R3,R);
end

%% Par ici la sortie

if nargout == 0
    [n, m] = size(R);
    if (n == 1) && (m ==1)
        varargout{1} = R;   % on force l'affichage
    elseif (n ~= 1) && (m ~=1)
        figure;
        imagesc(R); colorbar
        title('Trans Bathy Resol (m)')
    else
        figure;
        plot(R); grid on
        title('Longitudinal Resolution')
        xlabel('Oblique Distance (m)')
        ylabel('Trans Bathy Resol (m)')
    end
else
    varargout{1} = R;
end
