% Choix d'un sondeur ou d'un mode
%
% Syntax
%   [flag, b] = selection(a)
%
% Input Arguments
%   a : instance de cl_sounder
%
% Name-Value Pair Arguments
%   Masque : Tableau de 3 booleens indiquant si choix : du sondeur, de Mode_1, de Mode_2
%
% Output Arguments
%   b : instance de cl_sounder
%
% Examples
%   [flag, a] = cl_sounder
%   [flag, a] = selection(a)
%   [flag, a] = selection(a, 'Mask', [1 1 0])
%   [flag, a] = selection(a, 'Mask', [1 0 0])
%   [flag, a] = selection(a, 'Mask', [0 1 1])
%   [flag, a] = selection(a, 'Mask', [0 1 0])
%   [flag, a] = selection(a, 'Mask', [0 0 1])
%
% See also cl_sounder cl_sounder/set Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [flag, this] = selection(this, varargin)

[varargin, Masque]   = getPropertyValue(varargin, 'Mask',     [1 1 1]);
[varargin, Filemame] = getPropertyValue(varargin, 'Filemame', []);

[varargin, flagObligatoire] = getFlag(varargin, 'SelectionObligatoire'); %#ok<ASGLU>

flag = 1;

%% Choix du sondeur

N = length(this.str.SonarNames);
if (Masque(1) == 1) || ((this.Sonar.Ident < 1) || (this.Sonar.Ident > N))
    if (this.Sonar.Ident < 1) || (this.Sonar.Ident > N)
        k = 1;
        str2 = this.str.SonarNames;
        
        if ~flagObligatoire
            k = N + 1;
            str2{N + 1} = 'Unknown';
        end
    else
        k = this.Sonar.Ident;
        str2 = this.str.SonarNames;
    end
    
    if isempty(Filemame)
        Title = 'Sounder name ?';
    else
        [~, nomFic] = fileparts(Filemame);
        Title = sprintf('%s : Sounder name ?', nomFic);
    end
    [rep, flag] = my_listdlg(Title, str2, 'InitialValue', k, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    if rep == (N+1)
        this = this([]); % Cas o� on a r�pondu Unknown
        return
    end
    
    if rep ~= this.Sonar.Ident
        this = set(this, 'Sonar.Ident', rep);
    end
end

%% Choix de Mode_1

N1 = length(this.Sonar.strMode_1);
if (Masque(2) == 1) || ((this.Sonar.Mode_1 < 1) || (this.Sonar.Mode_1 > N1))
    if N1 > 1
        Ok = 0;
        while ~Ok
            if (this.Sonar.Mode_1 < 1) || (this.Sonar.Mode_1 > N1)
                k = N1 + 1;
                str2 = this.Sonar.strMode_1;
                str2{N1 + 1} = 'Unknown';
            else
                k = this.Sonar.Mode_1;
                str2 = this.Sonar.strMode_1;
            end
            
            [rep, flag] = my_listdlg('Mode_1 ?', str2, 'InitialValue', k, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            if rep <= N1
                Ok = 1;
            end
        end
        if rep ~= this.Sonar.Mode_1
            this = set(this, 'Sonar.Mode_1', rep);
        end
    end
end

%% Choix de Mode_2

N2 = length(this.Sonar.strMode_2);
if (Masque(3) == 1) || ((this.Sonar.Mode_2 < 1) || (this.Sonar.Mode_2 > N2))
    if N2 > 1
        Ok = 0;
        while ~Ok
            if (this.Sonar.Mode_2 < 1) || (this.Sonar.Mode_2 > N2)
                k = N2 + 1;
                str2 = this.Sonar.strMode_2;
                str2{N2 + 1} = 'Unknown';
            else
                k = this.Sonar.Mode_2;
                str2 = this.Sonar.strMode_2;
            end
            
            [rep, flag] = my_listdlg('Mode_2 ?', str2, 'InitialValue', k, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            if rep <= N2
                Ok = 1;
            end
        end
        if rep ~= this.Sonar.Mode_2
            this = set(this, 'Sonar.Mode_2', rep);
        end
    end
end

if nargout == 0
    plot_Tx(this)
    plot_Rx(this)
end
