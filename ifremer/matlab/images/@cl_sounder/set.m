function this = set(this, varargin)

% -------------------------------------------------------------
% SET - Mise � jour des propri�t�s d'une instance de la classe cl_sounder
% Par soucis de compatibilit�, on garde la possibilit� de
% choisir le mode de fonctionnement du sondeur
% -------------------------------------------------------------

global IfrTbxResources %#ok<GVMIS>
persistent persistent_nomFic persistent_X persistent_nomFicXML persistent_SonarIdent persistent_Info1

[varargin, MainFrequency] = getPropertyValue(varargin, 'MainFrequency',  []);
[varargin, nomFic]        = getPropertyValue(varargin, 'paramsFileName', this.paramsFileName);

if isempty(nomFic)
    [varargin, SonarIdent]                    = getPropertyValue(varargin, 'Sonar.Ident', 1);
    [varargin, this.Sonar.SystemSerialNumber] = getPropertyValue(varargin, 'Sonar.SystemSerialNumber', this.Sonar.SystemSerialNumber);

%     if ~isempty(this.Sonar.SystemSerialNumber) && (unique(this.Sonar.SystemSerialNumber) ~= -1)
    SystemSerialNumber = unique(this.Sonar.SystemSerialNumber(:));
    if ~isempty(SystemSerialNumber) && (SystemSerialNumber(1) ~= -1)
%         str1 = 'Pour info, ce fichier est constitu� du nom du sondeur suivi du num�ro de s�rie du sondeur.';
%         str2 = 'For your information, the name of this file is made of the name of the sounder followed by the system serial number.';
%         explication = Lang(str1,str2);
        
        switch this.str.SonarNames{SonarIdent}
            case {'EM2040'; 'EM2040S'; 'EM2040D'}
                nomFicRecherche = [this.str.SonarNames{SonarIdent} '_' num2str(MainFrequency) 'kHz_' num2str(SystemSerialNumber(1)) '.xml'];
            case 'EM2045'
                nomFicRecherche = [this.str.SonarNames{SonarIdent} '_' num2str(SystemSerialNumber(1)) '.xml'];
            otherwise
                nomFicRecherche = [this.str.SonarNames{SonarIdent} '_' num2str(SystemSerialNumber(1)) '.xml'];
        end
        
        nomFic = getNomFicDatabase(nomFicRecherche, 'NoMessage');
        if ~isempty(nomFic) && exist(nomFic, 'file')
%                         str1 = sprintf('Le fichier "%s" a �t� trouv�, il est utilis� pour d�finir les param�tres du sondeur.\n\n%s', nomFicRecherche, explication);
%                         str2 = sprintf('"%s" was found, it is used fo define the parameters of the sounder.\n\n%s', nomFicRecherche, explication);
%                         my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierXMLSondeurPasTrouve', 'TimeDelay', 60);
        else
            
            switch this.str.SonarNames{SonarIdent}
                case {'EM2040'; 'EM2040S'; 'EM2040D'}
                    nomFicDefaut = [this.str.SonarNames{SonarIdent}  '_' num2str(MainFrequency) 'kHz.xml'];
                case 'EM2045'
                    nomFicDefaut = [this.str.SonarNames{SonarIdent}  '.xml'];
                otherwise
                    nomFicDefaut = [this.str.SonarNames{SonarIdent} '.xml'];
            end
            
            if isempty(persistent_Info1)
%                 str1 = sprintf('Le fichier de param�tres du sondeur (%s) n''a pas �t� trouv�, je recherche le fichier par d�faut (%s).\n%s\n', nomFicRecherche, nomFicDefaut, explication);
%                 str2 = sprintf('The specific file that describes the parameters of this sounder (%s) was not found, I am searching the default one (%s).\n%s\n', nomFicRecherche, nomFicDefaut, explication);
%                 fprintf('%s\n', Lang(str1,str2)); % Comment� par JMA le 30/01/2019
%                 my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierXMLSondeurPasTrouve', 'TimeDelay', 60);
                persistent_Info1 = 1;
            end
            nomFicRecherche = nomFicDefaut;
            nomFic = getNomFicDatabase(nomFicRecherche, 'NoMessage');
        end
    else
        nomFicRecherche = [this.str.SonarNames{SonarIdent} '.xml'];
        nomFic = getNomFicDatabase(nomFicRecherche, 'NoMessage');
    end

    if isempty(nomFic)
        
        % persistent_nomFicXML : partique (emp�che de redemander maintes fois de d�finir le sondeur) mais peut-�tre pi�geux
        
        if isempty(persistent_nomFicXML) || isempty(SonarIdent) || isempty(persistent_SonarIdent) || (SonarIdent ~= persistent_SonarIdent) 
            nomDirXml = fullfile(IfrTbxResources, 'XML_sounder', nomFic);
            % EM300_Suroit.xml
            if isempty(SonarIdent) || isempty(this.str.SonarNames{SonarIdent})
                [flag, nomFic] = uiSelectFiles('ExtensionFiles', '.xml', 'RepDefaut', nomDirXml);
            else
                [flag, nomFic] = uiSelectFiles('ExtensionFiles', '.xml', 'RepDefaut', nomDirXml, ...
                    'ChaineIncluse', this.str.SonarNames{SonarIdent});
            end
            if ~flag
                return
            end
            nomFic = nomFic{1};
            persistent_nomFicXML = nomFic;
            persistent_SonarIdent = SonarIdent;
        else
            nomFic = persistent_nomFicXML;
        end
    end
    if isempty(nomFic)
        str1 = sprintf('%s" n''a pas �t� trouv� dans le r�pertoire de "XML_sounder". Envoyez ce message � sonarscope@ifremer.fr.', nomFicRecherche);
        str2 = sprintf('%s" was not found in  "XML_sounder".Send this message to sonarscope@ifremer.fr please.', nomFicRecherche);
        my_warndlg(Lang(str1,str2), 1);
        return

        %% Cas pour l'EM12S, EM300
    end
end

[nomDir, nomFic] = fileparts(nomFic);
nomFic = fullfile(nomDir, [nomFic '.xml']);
try
    if isempty(persistent_nomFic) || ~strcmp(persistent_nomFic, nomFic)
        X = xml_mat_read(nomFic); % lecture du fichier de param�tres
        persistent_nomFic = nomFic;
        persistent_X = X;
    else
       X =  persistent_X;
       nomFic = persistent_nomFic;
    end
catch ME %#ok<NASGU>
    try
        [~, nomFic, ext] = fileparts(nomFic);
        nomFic = getNomFicDatabase([nomFic ext]);
        X = xml_mat_read(nomFic); % lecture du fichier de param�tres
    catch ME %#ok<NASGU>
        this.IsGood = 0;
        return
    end
end

% R�cup�ration du Nom complet du fichier XML de definition des param�tres sondeurs
this.paramsFileName = nomFic;

% Identification du sondeur par rapport � la liste existantes dans cl-sounder
% TODO : a supprimer dans le futur car plus de liste de sondeur pr�-d�finie
%             SonarIdent = strmatch(X.Sonar.Name, this.str.SonarNames, 'exact');
SonarIdent = find(strcmp(X.Sonar.Name, this.str.SonarNames));

[varargin, SonarIdent] = getPropertyValue(varargin, 'Sonar.Ident', SonarIdent); % Rajout� par JMA le 19/01/2013

if isempty(SonarIdent) % A d�faut on est moins stricte
    %                 SonarIdent = strmatch(X.Sonar.Name, this.str.SonarNames);
    SonarIdent = find(strcmp(X.Sonar.Name, this.str.SonarNames));
    if isempty(SonarIdent)
        this.str.SonarNames = {X.Sonar.Name};
        SonarIdent = 0; % Sondeur ne faisant pas partie de la liste ancestrale de SonarScope
    end
end

[varargin, mode1] = getPropertyValue(varargin, 'Sonar.Mode_1', this.Sonar.Mode_1); % R�cup�ration du N� mode principal
[varargin, mode2] = getPropertyValue(varargin, 'Sonar.Mode_2', this.Sonar.Mode_2); % R�cup�ration du N� mode secondaire
[varargin, mode3] = getPropertyValue(varargin, 'Sonar.Mode_3', this.Sonar.Mode_3); % R�cup�ration du N� mode tertiaire

if isfield(X.Sonar, 'Mode1')
    this.Sonar.Ident = SonarIdent;

    % - MODE1
    if isempty(mode1)
        % Si non pr�cis�, le n� de mode 1 est 1
        this.Sonar.Mode_1 = 1;
    else
        % Autrement, contr�le du nombre de Mode 1 disponible par rapport au fichier
        if mode1 <= length(X.Sonar.Mode1(:))
            this.Sonar.Mode_1 = mode1;
        else
            str1 = sprintf('n� de mode principal invalide (le sondeur poss�de %d defined mode1)', length(X.Sonar.Mode1(:)));
            str2 = sprintf('Invalid n� of main mode (Sounder only has %d defined mode1)',         length(X.Sonar.Mode1(:)));
            disp(Lang(str1,str2));
            this.Sonar.Mode_1 = 1;
        end
    end

    % - MODE2
    if isfield(X.Sonar.Mode1(this.Sonar.Mode_1), 'Mode2') &&...
            ~isempty(X.Sonar.Mode1(this.Sonar.Mode_1).Mode2)

        if isempty(mode2)
            this.Sonar.Mode_2 = 1;
        else
            % Contr�le du nombre de Mode 2 disponible par rapport au fichier
            if mode2 <= length(X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(:))
                this.Sonar.Mode_2 = mode2;
            else
                str1 = sprintf('n� de mode secondaire invalide (le sondeur poss�de %d d�fini pour "mode1" n� %d)',      length(X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(:)), this.Sonar.Mode_1);
                str2 = sprintf('Invalid n� of secondary mode (Sounder only has %d defined "mode2" for "mode1" n� %d )', length(X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(:)), this.Sonar.Mode_1);
                disp(Lang(str1,str2));
                this.Sonar.Mode_2 = 1;
            end
        end

        % - MODE3
        if isfield( X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2), 'Mode3') &&...
                ~isempty( X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3 )
            if isempty(mode3)
                this.Sonar.Mode_3 = 1;
            else
                % Contr�le du nombre de Mode 2 disponible par rapport au fichier
                if mode3 <= length(X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(this.Sonar.Mode_2).Mode3(:))
                    this.Sonar.Mode_3 = mode3;
                else
                    str1 = sprintf('n� de mode3 invalide (le sondeur poss�de %d d�fini pour mode1 n� %d)', length(X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(:)), this.Sonar.Mode_1);
                    str2 = sprintf('Invalid n� of mode3 (Sounder only has %d defined mode2 for mode1 n� %d)', length(X.Sonar.Mode1(this.Sonar.Mode_1).Mode2(:)), this.Sonar.Mode_1);
                    disp(Lang(str1,str2));
                    this.Sonar.Mode_3 = 1;
                end
            end
        else
%                         if ~isdeployed
%                             disp(Lang(...
%                                 sprintf('Aucun mode3 pour mode2 n�%d du mode1 n�%d', this.Sonar.Mode_2, this.Sonar.Mode_1),...
%                                 sprintf('No mode3 for mode2 n�%d of mode1 n�%d',    this.Sonar.Mode_2 , this.Sonar.Mode_1)));
%                             this.Sonar.Mode_3 = [];
%                         end
        end

    else
%         fprintf('No secondary mode for %s Mode_1=%d\n', get(this, 'Sonar.Name'), this.Sonar.Mode_1)
        this.Sonar.Mode_2    = [];
        this.Sonar.strMode_2 = {};
    end

    % Initialisation des propri�t�s du sondeur en fonction du
    % fichier XML de param�tres
    this = initSounder(this);

    %% Calcul des ouvertures angulaires resultantes
    
    this.Sonar.LongBeamWth  = AntenneOuvEmiRec(this.BeamForm.Rx.LongWth,  this.BeamForm.Tx.LongWth);  % 20    1 : 0.998752338877845
    this.Sonar.TransBeamWth = AntenneOuvEmiRec(this.BeamForm.Rx.TransWth, this.BeamForm.Tx.TransWth); %  1  140 : 0.999974490772033

else
    str1 = 'Pas de mode principal pour ce sondeur : le fichier XML est incompatible en l''�tat avec l''application';
    str2 = 'No main mode for this sounder : XML file is unreadable by SonarScope';
    my_warndlg(Lang(str1,str2), 0);
    this.IsGood = 0;
end

if isdeployed
    % Juste pour vider varargin pour compatibilit� avec cl_sounder_old :
    % Supprimer quand �a aura �t� suffisemment �prouv�
    [varargin, Time] = getPropertyValue(varargin, 'Time', []);
end

if ~isempty(varargin)
%     str1 = sprintf('Argument non interpret� : %s', varargin{1});
%     str2 = sprintf('Argument not interpreted : %s', varargin{1});
%     my_warndlg(Lang(str1,str2), 0);
end
