% Ecritude du model defini par Ifremer en emission
%
% Syntax
%   a = set_EmissionModelsCalibration(a, model)
%
% INTPUT PARAMETERS :
%   a     : une instance de cl_sounder
%   model : une instance de ClModel
%
% INTPUT PARAMETERS :
%   a     : une instance de cl_sounder
%
% Examples
%   a = cl_sounder
%   a = cl_sounder('Sonar.Ident', 1)
%   model = get_EmissionModelsCalibration(a)
%   a = set_EmissionModelsCalibration(a, model)
%
% See also cl_sounder cl_sounder/plot_Rx Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function  this = set_EmissionModelsCalibration(this, model)

if isempty(this.Sonar.Mode_2)
    this.Emission.ModelsCalibration{this.Sonar.Mode_1} = model;
else
    this.Emission.ModelsCalibration{this.Sonar.Mode_1, this.Sonar.Mode_2} = model;
end
