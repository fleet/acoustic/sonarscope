function  this = set_EmissionModelsConstructeur(this, model)

if isempty(this.Sonar.Mode_2)
    this.Emission.ModelsConstructeur{this.Sonar.Mode_1} = model;
else
    this.Emission.ModelsConstructeur{this.Sonar.Mode_1, this.Sonar.Mode_2} = model;
end
