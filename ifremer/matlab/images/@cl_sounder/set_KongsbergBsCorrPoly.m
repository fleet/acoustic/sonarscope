function [flag, SonarDescription] = set_KongsbergBsCorrPoly(SonarDescription, EmModel, SystemSerialNumber, BsCorr, varargin)

[varargin, Destination] = getPropertyValue(varargin, 'Destination', 1:2); %#ok<ASGLU> % 1 : copie dans constructeur, 2 : copie dans calibration

SonarName = get(SonarDescription, 'SonarName');
flag = (str2double(SonarName(3:end)) == EmModel);
if ~flag
    str1 = sprintf('Il y a une incompatibilit� de nom de sondeur dans le fichiers BsCorr.txt : EM%d au lieu de %s.', EmModel, SonarName);
    str2 = sprintf('The sonar name inside the BsCorr.txt does not correspond to the data: EM%d instead of %s.', EmModel, SonarName);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NomSondeurDansBsCorrPasBon');
    return
end
SSN = SonarDescription.Sonar.SystemSerialNumber;
flag = (SSN == SystemSerialNumber);
if ~flag
    str1 = sprintf('Il y a une incompatibilit� de num�ro de s�rie du sondeur dans le fichiers BsCorr.txt : %d au lieu de %d.', SystemSerialNumber, SSN);
    str2 = sprintf('The system serial number inside the BsCorr.txt does not correspond to the data: %d instead of %d.', SystemSerialNumber, SSN);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NomSondeurDansBsCorrPasBon');
    %                 return
    flag = 1;
end

%                     MaxPingMode = max([ BsCorr(:).pingMode]);
%                     MaxSwathMode = max([ BsCorr(:).swathMode])+1;
for k1=1:length(BsCorr)
    pingMode  = BsCorr(k1).pingMode;
    %                         swathMode = BsCorr(k1).swathMode;
    swathMode = BsCorr(k1).swathMode + 1; % TODO : rajout� par JMA le 23/04/2014 � bord du Nautilus
    if pingMode == 1 % Mode jamais utilis�
        continue
    end
    
    %                         if swathMode == 0
    if swathMode == 1
        Mode_1 = 1;
        % TODO : y'a moyen d'automatiser �a grace
        % Mode2.Num mais j'ai la flemme
        switch pingMode
            case 1
                ListeMode_2 = 1;
            case 2
                ListeMode_2 = 2;
            case 3
                ListeMode_2 = 3;
            case 4
                ListeMode_2 = 4:5;
            case 5
                ListeMode_2 = 6:7;
            case 6
                ListeMode_2 = 8:9;
            case 7
                ListeMode_2 = 10:11;
        end
    else
        Mode_1 = 2;
        switch pingMode
            case 1
                ListeMode_2 = 1; % Very shallow
            case 2
                ListeMode_2 = 2; % Shallow
            case 3
                ListeMode_2 = 3; % Medium
            case 4
                ListeMode_2 = 4:5; % Deep CW et FM
            case 5
                ListeMode_2 = 6:7; % Very deep CW et FM
            case 6
                ListeMode_2 = 8:9; % Extra deep CW et FM
            case 7
                ListeMode_2 = 10:11; % Extra deep* CW et FM % Valable pour EM302 mais pas EM122
        end
    end
    %Sonar.Mode_2 <-> {'1=Shallow'} | '2=Medium' | '3=Deep CW' | '4=Deep FM' | '5=Very Deep CW' | '6=Very Deep FM' | '7=Extra Deep CW' | '8=Extra Deep FM' | '9=Extra Deep* CW' | '10=Extra Deep* FM'
    
    for k=1:length(ListeMode_2)
        SonarDescription = set(SonarDescription, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', ListeMode_2(k));
        %                 nbSectors = BsCorr(k1).nbSectors; % TODO : pas � jour
        nbSectors = size(BsCorr(k1).Sector.Node,1); % TODO : Faut supprimer Node et remplacer par ParamsPolyKM : reinjecter Level
        subTxDiag = 1:nbSectors;
        if BsCorr(k1).swathMode == 2
            subTxDiag = subTxDiag + nbSectors;
        end
        for k2=1:nbSectors
            Centre = BsCorr(k1).Sector.Node(k2,1);
            Width  = BsCorr(k1).Sector.Node(k2,2);
            XData = (Centre-Width):0.5:(Centre+Width);
            valParams = [0 Centre Width];
            Tolerances = [5 Width/2 Width/2];
            valminParams = valParams - Tolerances;
            valmaxParams = valParams + Tolerances;
            YData = AntennePoly(XData, valParams);
            TxDiag(k2) = AntennePolyModel('XData', XData, 'YData', YData, ...
                'ValParams', valParams, 'ValMinParams', valminParams, 'ValMaxParams', valmaxParams); %#ok<AGROW>
        end
        if any(Destination == 1)
            SonarDescription.Emission.ModelsConstructeur{Mode_1, ListeMode_2(k)}(subTxDiag) = TxDiag(1:nbSectors);
        end
        if any(Destination == 2)
            SonarDescription.Emission.ModelsCalibration{Mode_1, ListeMode_2(k)}(subTxDiag) = TxDiag(1:nbSectors);
        end
        %{
plot(TxDiag);
title(sprintf('Mode_1 : %d  -   Mode_2 : %d', Mode_1,  ListeMode_2(k)))
        %}
    end
end
