% TODO : renommer cette fonction ansi que la version Poly
function [flag, SonarDescription] = set_KongsbergBsCorrSpline(SonarDescription, EmModel, SystemSerialNumber, BsCorr, varargin)
% function [flag, SonarDescription] = BSCorr2TxDiagModels(SonarDescription, EmModel, SystemSerialNumber, BsCorr, varargin)

[varargin, Destination] = getPropertyValue(varargin, 'Destination', 1:2); %#ok<ASGLU> % 1 : copie dans constructeur, 2 : copie dans calibration

C0 = cl_sounder([]);

SonarName = get(SonarDescription, 'SonarName');
if strcmp(SonarName(end), 'D')
    SonarName = SonarName(1:end-1);
end
flag = (str2double(SonarName(3:end)) == EmModel);
if ~flag
    str1 = sprintf('Il y a une incompatibilité de nom de sondeur dans le fichiers BsCorr.txt : EM%d au lieu de %s.', EmModel, SonarName);
    str2 = sprintf('The sonar name inside the BsCorr.txt does not correspond to the data: EM%d instead of %s.', EmModel, SonarName);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NomSondeurDansBsCorrPasBon');
    return
end

SSN = SonarDescription.Sonar.SystemSerialNumber;
flag = (SSN == SystemSerialNumber);
if ~flag
    str1 = sprintf('Il y a une incompatibilité de numéro de série du sondeur dans le fichiers BsCorr.txt : %d au lieu de %d.', SystemSerialNumber, SSN);
    str2 = sprintf('The system serial number inside the BsCorr.txt does not correspond to the data: %d instead of %d.', SystemSerialNumber, SSN);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NomSondeurDansBsCorrPasBon');
    %                 return
    flag = 1;
end


for k1=1:length(BsCorr)
    pingMode  = BsCorr(k1).pingMode;
    swathMode = BsCorr(k1).swathMode;
    
    [Mode_1, ListeMode_2] = BsCorr_ModesIn(C0, pingMode, swathMode);
    
    for k=1:length(ListeMode_2)
        %         SonarDescription = set(SonarDescription, 'Sonar.Mode_1', Mode_1,
        %         'Sonar.Mode_2', ListeMode_2(k)); % Commenté par JMA le 05/07/2016
        nbSectors = length(BsCorr(k1).Sector);
        subTxDiag = 1:nbSectors;
        if BsCorr(k1).swathMode == 2
            subTxDiag = subTxDiag + nbSectors;
        end
        if any(Destination == 1)
            for k2=1:nbSectors
                xNodes = BsCorr(k1).Sector(k2).Node(:,1);
                yNodes = BsCorr(k1).Sector(k2).Node(:,2);
                
                XData = min(xNodes):0.5:max(xNodes);
                subNonNaN = find(~isnan(yNodes));
                YData = spline(xNodes(subNonNaN), yNodes(subNonNaN), XData);
                TxDiag(k2) = AntenneSplineModel('XData', XData, 'YData', YData, 'xNodes', xNodes, 'yNodes', yNodes); %#ok<AGROW>
            end
            SonarDescription.Emission.ModelsConstructeur{Mode_1, ListeMode_2(k)}(subTxDiag) = TxDiag;
        end
        if any(Destination == 2)
            for k2=1:nbSectors
                xNodes = BsCorr(k1).Sector(k2).Node(:,1);
                yNodes = BsCorr(k1).Sector(k2).Node(:,2);
                
                %{
xstep = ceil((xNodes(end) - xNodes(1)) / 32);
xNodesNew = xNodes(1) : xstep : xNodes(end);
xNodesNew = centrage_magnetique(xNodesNew);
yNodes = interp1(xNodes, yNodes, xNodesNew);
xNodes = xNodesNew;
                %}
                
                XData = min(xNodes):0.5:max(xNodes);
                subNonNaN = find(~isnan(yNodes));
                YData = spline(xNodes(subNonNaN), yNodes(subNonNaN), XData);
                TxDiag(k2) = AntenneSplineModel('XData', XData, 'YData', YData, 'xNodes', xNodes, 'yNodes', yNodes);
            end
            SonarDescription.Emission.ModelsCalibration{Mode_1, ListeMode_2(k)}(subTxDiag) = TxDiag;
        end
        %{
plot(TxDiag);
title(sprintf('Mode_1 : %d  -   Mode_2 : %d', Mode_1,  ListeMode_2(k)))
        %}
    end
end
