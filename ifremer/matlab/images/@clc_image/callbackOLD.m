% Methode de gestion des callback du composant
%
% Syntax
%   a = callback(a, msg, ...)
%
% Input Arguments
%   a   : instance de clc_image
%   msg : message identifiant la callback
%
% Name-Value Pair Arguments
%   ... : parametres optionnels de la callback
%
% Output Arguments
%   a : instance de cl_component initialisee
%
% Remarks : doit �tre surcharg�e
%
% See also clc_image
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callbackOLD(this, msg, varargin)

global IfrTbxRepDefInput %#ok<GVMIS>
global IfrTbxRepDefOutput %#ok<GVMIS>

if strcmp(msg, this.msgMoveCurseur)
    this = callback_movePointer(this);
elseif strcmp(msg, this.msgMoveProfils)
    this = callback_moveProfils(this);
else
    this.repImport = define_repImport(this);
    this.repExport = define_repExport(this);

    java.lang.System.gc % Garbage Collector de Java
    
    ContrasteAuto = false;
    
    Tag = ['PleaseWaitWindowStyleModal_' this.internalNumber];
    hFigModal = findobj(0, 'WindowStyle', 'modal', 'Tag', Tag);
    if ~isempty(hFigModal)
        str1 = sprintf('Un traitement est en cours, cela peut arriver � 2 occasions :\n- Vous tentez de lancer un deuxi�me traitement alors qu''il y en a un toujours en cours.\n- Le traitement pr�c�dent s''est termin� sur une erreur qui a produit un crash.\n\nJe vous recommande de valider cette action seulement dans le deuxi�me cas.');
        str2 = sprintf('A processing is in course, this can happen in 2 cases :\n- you are triing to launch a second processing while one is running yet.\n- The previous processing produced an error.\n\nWe recommand you validate this action only in the second case.');
        [rep, flag] = my_questdlg(Lang(str1,str2), ...
            Lang('Abandonner l''action en cours', 'Cancel the current action'), ...
            Lang('Poursuivre l''action en cours', 'Validate the current action'));
        if ~flag || (rep == 1)
            return
        end
        for k=1:length(hFigModal)
            my_close(hFigModal(k))
        end
        
        hFigModal = findobj(0, 'WindowStyle', 'modal');
        for k=1:length(hFigModal)
            my_close(hFigModal(k))
        end
    end
        
    [varargin, Interruptible] = getPropertyValue(varargin, 'Interruptible', 'off'); %#ok
    [varargin, BusyAction]    = getPropertyValue(varargin, 'BusyAction',    'cancel'); %#ok
    
    this.Historic{end+1} = [{msg} varargin];
    
    mots = strsplit(msg, '/');
    msg2 = rmblank(mots{end});
    switch msg2
        case 'msgClick'
            this = callback_click(this);
            
        case 'CoupeHorz'
            this = callback_profileHorz(this);
            
        case 'CoupeVert'
            this = callback_profileVert(this);
            
        case 'Info'
            this = callback_info(this, varargin{:});
            
        case 'Export'
            this = callback_export(this, varargin{:});
            
        case 'ValX'
            this = callback_valeurX(this);
            
        case 'ValY'
            this = callback_valeurY(this);
            
        case 'ValImage'
            ContrasteAuto = true;
            this = callback_valeurImage(this);
            
        case 'lr'
            this = callback_symetrieH(this);
            
        case 'xy'
            this = callback_symetrieV(this);
            
        case 'ViewImage'
            this = callback_ViewImage(this);
            
        case 'Video'
            this = callback_videoInverse(this);
            
        case 'Contraste'
            this = callback_contrast(this, varargin{:});
            
        case 'Grid'
            this = callback_grid(this);
            
        case 'Colorbar'
            this = callback_colorbar(this);
            
        case 'TypeInteractionSouris'
            this = callback_typeInteractionSouris(this, varargin{2:end});
            
        case 'ColormapIndex'
            this = callback_ColormapIndex(this, varargin{:});
            
        case 'IdentImage'
            ContrasteAuto = true;
            this = callback_selectImage(this, varargin{:});
            
        case 'NomImage'
            this = callback_ImageName(this);
            
        case 'NbVoisins'
            this = callback_nbProfilesHV(this);
           
        case 'Delete'
            this = callback_delete(this);
            
        case 'Stats'
            this = callback_STATS(this, varargin{:});
            
        case 'Import'
            this = callback_import(this, varargin{:});
            
        case 'Compute'
            ContrasteAuto = true;
            this = callback_GP(this, varargin{:});
            
        case 'CallbackSonar'
            ContrasteAuto = true;
            this = callback_SP(this, varargin{:});
            
        case 'ZoneEtude'
            this = callback_zoneEtude(this, varargin{:});
            
        case 'ZoomQL'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'QuickLook');
            
        case 'ZoomPrecedent'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'ZoomPrecedent');
            
        case 'ZoomSuivant'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'ZoomSuivant');
            
        case 'AxisEqual'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'AxisEqual');
           
        case 'MoveUp'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'MoveUp');
           
        case 'ZoomInX'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'ZoomInX');
            
        case 'ZoomOutX'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'ZoomOutX');
            
        case 'ZoomInY'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'ZoomInY');
            
        case 'ZoomOutY'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'ZoomOutY');
            
        case 'MoveDown'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'MoveDown');
            
        case 'MoveLeft'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'MoveLeft');
            
        case 'MoveRight'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'MoveRight');
            
        case 'Zoom1'
            ContrasteAuto = true;
            this = callback_zoomDirect(this, 'Zoom1');
            
        case 'VisuImage'
            this = callback_Visu(this, varargin{:});
            
        case 'CloneWindow'
            obj = clone(this);
            editobj(obj);
            
        case 'VisuPlot'
            this = private_plot(this, 'CurrentExtent');
            
        case 'VisuPlotVoisinsX'
            this = private_plot(this, 'VoisinsX');
            
        case 'VisuPlotVoisinsY'
            this = private_plot(this, 'VoisinsY');
            
        case {'IX Edit'; 'IX Plus'; 'IX Moins'}
            this = callback_valeurCol(this, msg);
            
        case {'IY Edit'; 'IY Plus'; 'IY Moins'}
            this = callback_valeurLig(this, msg);
            
        case 'msgKeyPress'
            ContrasteAuto = true;
            this = callback_KeyPress(this, 'KeyPress');
            
        case 'PrintPixel'
            PrintPixel(this);
            
        case 'msgClickCentre'
            ContrasteAuto = true;
            this = callback_zoomCentrage(this);
            
        case 'msgClickCoupeDown'
            this = callback_profile(this);
            
        case 'msgClickHeightDown'
            this = callback_drawHeight(this);
            
        case 'zoom'
            ContrasteAuto = true;
            this = callback_zoom(this, varargin{1}, varargin{2});
            
        case 'HelpSession'
            this = session_help(this);
            
        case 'SaveSession'
            this = session_save(this);
            
        case 'RestoreSession'
            this = session_restore(this);
            
        case 'msgClickOnColorbar'
            this = callback_contrast(this, 'imcontrast');
            
        case 'msgClickSetNaN'
            ContrasteAuto = true;
            this = callback_NaN(this);
            
        case 'msgClickSetVal'
            ContrasteAuto = true;
            this = callback_setVal(this);
            
        case 'GetModifiedImageAxeX'
            ContrasteAuto = true;
            this = callback_GetModifiedImageAxeX(this);
            
        case 'GetModifiedImageAxeY'
            ContrasteAuto = true;
            this = callback_GetModifiedImageAxeY(this);
            
        case 'GetModifiedImageAxeImage'
            ContrasteAuto = true;
            this = callback_GetModifiedImageAxeImage(this);
            
        case 'msgCleanData'
            ContrasteAuto = true;
            this = callback_CleanData(this, 'ContrastAuto', 1);
            
        case 'QuestionLevel'
            this = callback_QuestionLevel(this, varargin{:});
            
        case 'ParallelTbx'
            this = callback_ParallelTbx(this, varargin{:});
            
        otherwise
            SSc_InvalidMessage(msg, which(mfilename))
    end
    PleaseWait(this, 0)

    %% Contraste automatique
    
    if ContrasteAuto && (this.Images(this.indImage).ImageType == 1)
        try
            switch getFlagContrasteAuto
                case 1
                    this = callback_contrast(this, 'ContrasteImageZoomeeMinMax', 'ModeAuto', 1);
                case 2
                    this = callback_contrast(this, 'ContrasteImageZoomeeQuant_0p5pc', 'ModeAuto', 1);
            end
            this = traiter_typeInteractionSouris(this);
        catch
            my_breakpoint
            % Un bug se produit parfois si on a une navigation affich�e,
            % la callback du trac� de nav ne doit pas �tre tr�s bien g�r�e
        end
    end
    
    %% Sauvegarde des noms de r�pertoire d'entr�e et de sortie par d�faut
    
    IfrTbxRepDefInput  = this.repImport;
    IfrTbxRepDefOutput = this.repExport;
    saveVariablesEnv
    
    if ishandle(this.hfig)
        set(this.hfig, 'Pointer', this.Pointer);
    end
end
