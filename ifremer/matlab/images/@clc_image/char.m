% Transformation en chaine de caracteres d'un tableau d'instances
%
% Syntax
%   str = char(a) 
%
% Input Arguments
%   a : instance ou tableau d instance de clc_image
%
% Output Arguments
%    str : une chaine de caracteres representative
%
% Examples
%    char(clc_image)
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok
        end
    end
    str = cell2str(str);

else

    % ---------------
    % Instance unique

    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
