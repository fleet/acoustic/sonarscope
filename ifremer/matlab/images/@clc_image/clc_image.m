% Constructeur de clc_image, classe composant
%
% Syntax
%   a = clc_image(...)
%
% Name-Value Pair Arguments
%   YDir              : {'1=ij'} | '2=xy'
%   fLabelX           : flag affichage des labels selon x, 0 ou 1
%   fLabelY           : flag affichage des labels selon y, 0 ou 1
%   visuGrid          : flag de mise en place de la grille, 0 ou 1
%   visuCoupeHorz     : flag affichage profil selon x, 0 ou 1
%   visuCoupeVert     : flag affichage profil selon y, 0 ou 1
%   visuColorbar      : flag affichage barre de couleurs
%   fFixedColorBar    : flag de reservation de place de la colorbar
%   fToolBar          : flag affichage toolbarre
%   hauteurToolBar    : hauteur (en nb lignes) de la toolbar
%   hauteurProfilX    : hauteur (en nb lignes) du profil en X
%   largeurProfilY    : largeur (en nb colonnes) du profil en Y
%   largeurColorBar   : largeur (en nb colonnes) de la colorbar
%   margeLabelX       : marge (en nb lignes) du label x de l'axe
%   margeLabelY       : marge (en nb colonnes) du label y de l'axe
%   nbTotLignes       : nombre total de lignes
%   nbTotColonnes     : nombre total de colonnes
%   colormap          : colormap appliquee aux axes du composant
%   txtProfilX        : texte de toolbar pour profilX
%   txtProfilY        : texte de toolbar pour profilY
%
%   componentName     : nom associe a ce composant
%   componentVisible  : visibilite du composant    {'on', 'off'}
%   componentEnable   : accessibilite du composant {'on', 'off'}
%   componentUserName : nom de la classe utilisatrice du composant
%   componentUserCb   : nom de la methode de gestion des call back
%                       de la classe utilisatrice du composant
%   componentInsetX   : marge en X du composant (en pixels)
%   componentInsetY   : marge en Y du composant (en pixels)
%
% Output Arguments
%    a : instance de clc_image
%
% Remarks :
%    ne peut etre employe que sous forme d un composant associe a une
%    instance graphique frame matlab.
%    cette version, tres bas niveau, permet d optimiser l utilisation
%    du composant
%
% Examples
%    v = clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

classdef clc_image < handle
    
    %% D�finition de la structure et initialisation
    
    properties(GetAccess = 'public', SetAccess = 'public')
        
        Images       = cl_image.empty;
        indImage     = 1;
        NomFicPoints = my_tempname('NoRandom', '.dat');
        
        Historic = {};
        
        visuGrid              = 0;       % flag de mise en place de la grille
        visuCoupeHorz         = 0;       % flag affichage profil selon x, 0 ou 1
        visuCoupeVert         = 0;       % flag affichage profil selon y, 0 ou 1
        visuColorbar          = 0;       % Component property
        identVisu             = 1;
        strVisu               = {'Image'; 'Contour'; 'surf'; 'plot'};
        strAction             = {'Pixval'; 'Centrage'; 'Coupe'; 'Region'};
        TypeInteractionSouris = 'PointClick';
        repImport             = [];
        repExport             = [];
        
        EreasePixelWarning    = 1; % Pour prevenir la premi�re fois que la touche "espace" efface les pixels
        EreasePixelAction     = 0; % Flag pour autoriser la mise � NaN des pixels par "Space" key press
        
        fLabelX               = 1;       % flag affichage des labels selon x
        fLabelY               = 1;       % flag affichage des labels selon y
        fFixedColorBar        = 0;       % flag de reservation de place de la colorbar
        fToolBar              = 1;       % flag affichage toolbar
        
        visuLim               = [0 0 0 0] * NaN; % Limites de l'images visualisee [xmin xmax ymin ymax]
        visuLimHistory        = cell(0);
        visuLimHistoryPointer = 0;
        
        valBeforeSetNaN       = cell(0);
        
        %         ZoneEtude.XLim         = [-Inf Inf];
        %         ZoneEtude.YLim         = [-Inf Inf];
        %         ZoneEtude.Name         = '';
        ZoneEtude = struct('XLim', [-Inf Inf], 'YLim', [-Inf Inf], 'Name', '');
        %         ZoneEtude(1)           = []; % Taille 0
        %         ZoneEtude           = []; % Taille 0
        ZoneEtudeInd           = 0;
        
        hauteurToolBar       = 10;      % Hauteur (en nb lignes) de la toolbar
        hauteurProfilX       = 8;       % Hauteur (en nb lignes) du profil en X
        largeurProfilY       = 4;       % Largeur (en nb colonnes) du profil en Y
        largeurColorBar      = 2;       % Largeur (en nb colonnes) de la colorbar
        nbTotLignes          = 50;      % Nombre total de lignes
        nbTotColonnes        = 50;      % Nombre total de colonnes
        margeLabelX          = 2;       % Marge (en nb lignes) du label x de l'axe
        margeLabelY          = 2;       % Marge (en nb colonnes) du label y de l'axe
        
        colormap             = [];      % Component property
        cross                = [0 0] * NaN;      % Coordonnees du point definissant les coupes
        %         profilX.x            = [];      % valeurs d'abscisses de l'axe de profil X
        %         profilX.val          = [];      % valeurs de l'axe de profil X
        profilX = struct('x', [], 'val', []);
        
        %         profilY.x            = [];      % valeurs d'abscisses de l'axe de profil Y
        %         profilY.val          = [];      % valeurs de l'axe de profil Y
        %         profilY.Type         = [];
        profilY = struct('x', [], 'val', [], 'Type', []);
        
        %         coupe.xyDeb          = [];      % Coupe dans l'image
        %         coupe.xyFin          = [];      % Coupe dans l'image
        %         coupe.fig            = [];      % Coupe dans l'image
        %         coupe.handles        = [];      % Coupe dans l'image
        %         coupe.handlesPatch   = [];      % Coupe dans l'image
        %         coupe.flagDebut      = [0 0];   % Coupe dans l'image
        coupe = struct('xyDeb', [], 'xyFin', [], 'fig', [], 'handles', [], 'handlesPatch', [], 'flagDebut', []);
        
        msgMoveCurseur       = '';      % Component property
        msgClick             = '';      % Component property
        msgClickCentre       = '';      % Component property
        msgClickCoupeDown    = '';      % Component property
        msgClickHeightDown   = '';      % Component property
        msgClickSetNaN       = '';      % Component property
        msgClickSetVal       = '';      % Component property
        msgKeyPress          = '';      % Component property
        msgMolette           = '';      % Component property
        msgMoveProfils       = '';      % Component property
        msgClickOnColorbar   = '';      % Component property
        
        msgCleanData          = '';      % Component property
        
        globalFrame          = [];      % Internal use only
        hAxePrincipal        = [];      % Internal use only
        hAxeProfilX          = [];      % Internal use only
        hAxeProfilY          = [];      % Internal use only
        hCourbeProfilX       = [];      % Internal use only
        hCourbeProfilY       = [];      % Internal use only
        hHeight              = [];      % Internal use only
        hColorBar            = [];      % Internal use only
        hToolBar             = [];      % Internal use only
        hCroix               = [];      % Internal use only
        hCroixProfilX        = [];      % Internal use only
        hCroixProfilY        = [];      % Internal use only
        hfig                 = [];      % Internal use only
        internalNumber       = [];      % Internal use only
        EreaseWindow         = [1 1];
        EreaseWindowAngle    = 0;
        SetValue             = NaN;
        Pointer              = 'arrow';
        PointerShapeCData    = [];
        PointerShapeHotSpot  = [];
        hGUITypeInteractionSouris = [];
        
        %         ihm.colorbar.cpn     = [];      % Internal use only
        %         ihm.zoom.cpn         = [];      % composant du zoom
        %         ihm.Handles          = {};
        ihm = struct('colorbar', struct('cpn', []), 'zoom', struct('cpn', []), 'Handles', []);
        
        handleAnchor         = [];      % Internal property
        
        NbVoisins            = 0;    % Nombre de profils voisins affiches (coupe Horz et Vert et pixels dans la fenetre de commande
        % indImageReference    = 1;  % Definit une image de reference pour le recalage d'image Comment� par JMA le 18/02/2019
        
        PublicCurves         = [];
        
        %% Super-classe
        
        composant = cl_component([]);
        
        %% Cr�ation de l'objet
        
        % this = class(this, 'clc_image', composant);
        %
        % %% Pre-nitialisation des champs
        %
        % if (nargin == 1) && isempty(varargin{1})
        %     return
        % end
        %
        % %% Initialisation des valeurs transmises lors de l'appel au constructeur
        %
        % this = set(this, varargin{:});
        
    end
    
    methods(Access = 'public')
        function this = clc_image(varargin)
            this = set(this, varargin{:});
        end
%     end
%     
%     methods (Static)
        
        function this = callback(this, msg, varargin)
            
            global IfrTbxRepDefInput %#ok<GVMIS>
            global IfrTbxRepDefOutput %#ok<GVMIS>
            
            if strcmp(msg, this.msgMoveCurseur)
                this = callback_movePointer(this);
            elseif strcmp(msg, this.msgMoveProfils)
                this = callback_moveProfils(this);
            else
                this.repImport = define_repImport(this);
                this.repExport = define_repExport(this);
                
                java.lang.System.gc % Garbage Collector de Java
                
                ContrasteAuto = false;
                
                Tag = ['PleaseWaitWindowStyleModal_' this.internalNumber];
                hFigModal = findobj(0, 'WindowStyle', 'modal', 'Tag', Tag);
                if ~isempty(hFigModal)
                    str1 = sprintf('Un traitement est en cours, cela peut arriver � 2 occasions :\n- Vous tentez de lancer un deuxi�me traitement alors qu''il y en a un toujours en cours.\n- Le traitement pr�c�dent s''est termin� sur une erreur qui a produit un crash.\n\nJe vous recommande de valider cette action seulement dans le deuxi�me cas.');
                    str2 = sprintf('A processing is in course, this can happen in 2 cases :\n- you are triing to launch a second processing while one is running yet.\n- The previous processing produced an error.\n\nWe recommand you validate this action only in the second case.');
                    [rep, flag] = my_questdlg(Lang(str1,str2), ...
                        Lang('Abandonner l''action en cours', 'Cancel the current action'), ...
                        Lang('Poursuivre l''action en cours', 'Validate the current action'));
                    if ~flag || (rep == 1)
                        return
                    end
                    for k=1:length(hFigModal)
                        my_close(hFigModal(k))
                    end
                    
                    hFigModal = findobj(0, 'WindowStyle', 'modal');
                    for k=1:length(hFigModal)
                        my_close(hFigModal(k))
                    end
                end
                
                [varargin, Interruptible] = getPropertyValue(varargin, 'Interruptible', 'off'); %#ok
                [varargin, BusyAction]    = getPropertyValue(varargin, 'BusyAction',    'cancel'); %#ok
                
                this.Historic{end+1} = [{msg} varargin];
                
                mots = strsplit(msg, '/');
                msg2 = rmblank(mots{end});
                switch msg2
                    case 'msgClick'
                        this = callback_click(this);
                        
                    case 'CoupeHorz'
                        this = callback_profileHorz(this);
                        
                    case 'CoupeVert'
                        this = callback_profileVert(this);
                        
                    case 'Info'
                        this = callback_info(this, varargin{:});
                        
                    case 'Export'
                        this = callback_export(this, varargin{:});
                        
                    case 'ValX'
                        this = callback_valeurX(this);
                        
                    case 'ValY'
                        this = callback_valeurY(this);
                        
                    case 'ValImage'
                        ContrasteAuto = true;
                        this = callback_valeurImage(this);
                        
                    case 'lr'
                        this = callback_symetrieH(this);
                        
                    case 'xy'
                        this = callback_symetrieV(this);
                        
                    case 'ViewImage'
                        this = callback_ViewImage(this);
                        
                    case 'Video'
                        this = callback_videoInverse(this);
                        
                    case 'Contraste'
                        this = callback_contrast(this, varargin{:});
                        
                    case 'Grid'
                        this = callback_grid(this);
                        
                    case 'Colorbar'
                        this = callback_colorbar(this);
                        
                    case 'TypeInteractionSouris'
                        this = callback_typeInteractionSouris(this, varargin{2:end});
                        
                    case 'ColormapIndex'
                        this = callback_ColormapIndex(this, varargin{:});
                        
                    case 'IdentImage'
                        ContrasteAuto = true;
                        this = callback_selectImage(this, varargin{:});
                        
                    case 'NomImage'
                        this = callback_ImageName(this);
                        
                    case 'NbVoisins'
                        this = callback_nbProfilesHV(this);
                        
                    case 'Delete'
                        this = callback_delete(this);
                        
                    case 'Stats'
                        this = callback_STATS(this, varargin{:});
                        
                    case 'Import'
                        this = callback_import(this, varargin{:});
                        
                    case 'Compute'
                        ContrasteAuto = true;
                        this = callback_GP(this, varargin{:});
                        
                    case 'CallbackSonar'
                        ContrasteAuto = true;
                        this = callback_SP(this, varargin{:});
                        
                    case 'ZoneEtude'
                        this = callback_zoneEtude(this, varargin{:});
                        
                    case 'ZoomQL'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'QuickLook');
                        
                    case 'ZoomPrecedent'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'ZoomPrecedent');
                        
                    case 'ZoomSuivant'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'ZoomSuivant');
                        
                    case 'AxisEqual'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'AxisEqual');
                        
                    case 'MoveUp'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'MoveUp');
                        
                    case 'ZoomInX'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'ZoomInX');
                        
                    case 'ZoomOutX'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'ZoomOutX');
                        
                    case 'ZoomInY'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'ZoomInY');
                        
                    case 'ZoomOutY'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'ZoomOutY');
                        
                    case 'MoveDown'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'MoveDown');
                        
                    case 'MoveLeft'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'MoveLeft');
                        
                    case 'MoveRight'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'MoveRight');
                        
                    case 'Zoom1'
                        ContrasteAuto = true;
                        this = callback_zoomDirect(this, 'Zoom1');
                        
                    case 'VisuImage'
                        this = callback_Visu(this, varargin{:});
                        
                    case 'CloneWindow'
                        obj = clone(this);
                        editobj(obj);
                        
                    case 'VisuPlot'
                        this = private_plot(this, 'CurrentExtent');
                        
                    case 'VisuPlotVoisinsX'
                        this = private_plot(this, 'VoisinsX');
                        
                    case 'VisuPlotVoisinsY'
                        this = private_plot(this, 'VoisinsY');
                        
                    case {'IX Edit'; 'IX Plus'; 'IX Moins'}
                        this = callback_valeurCol(this, msg);
                        
                    case {'IY Edit'; 'IY Plus'; 'IY Moins'}
                        this = callback_valeurLig(this, msg);
                        
                    case 'msgKeyPress'
                        ContrasteAuto = true;
                        this = callback_KeyPress(this, 'KeyPress');
                        
                    case 'PrintPixel'
                        PrintPixel(this);
                        
                    case 'msgClickCentre'
                        ContrasteAuto = true;
                        this = callback_zoomCentrage(this);
                        
                    case 'msgClickCoupeDown'
                        this = callback_profile(this);
                        
                    case 'msgClickHeightDown'
                        this = callback_drawHeight(this);
                        
                    case 'zoom'
                        ContrasteAuto = true;
                        this = callback_zoom(this, varargin{1}, varargin{2});
                        
                    case 'HelpSession'
                        this = session_help(this);
                        
                    case 'SaveSession'
                        this = session_save(this);
                        
                    case 'RestoreSession'
                        this = session_restore(this);
                        
                    case 'msgClickOnColorbar'
                        this = callback_contrast(this, 'imcontrast');
                        
                    case 'msgClickSetNaN'
                        ContrasteAuto = true;
                        this = callback_NaN(this);
                        
                    case 'msgClickSetVal'
                        ContrasteAuto = true;
                        this = callback_setVal(this);
                        
                    case 'GetModifiedImageAxeX'
                        ContrasteAuto = true;
                        this = callback_GetModifiedImageAxeX(this);
                        
                    case 'GetModifiedImageAxeY'
                        ContrasteAuto = true;
                        this = callback_GetModifiedImageAxeY(this);
                        
                    case 'GetModifiedImageAxeImage'
                        ContrasteAuto = true;
                        this = callback_GetModifiedImageAxeImage(this);
                        
                    case 'msgCleanData'
                        ContrasteAuto = true;
                        this = callback_CleanData(this, 'ContrastAuto', 1);
                        
                    case 'QuestionLevel'
                        this = callback_QuestionLevel(this, varargin{:});
                        
                    case 'ParallelTbx'
                        this = callback_ParallelTbx(this, varargin{:});
                        
                    otherwise
                        SSc_InvalidMessage(msg, which(mfilename))
                end
                PleaseWait(this, 0)
                
                %% Contraste automatique
                
                if ContrasteAuto && (this.Images(this.indImage).ImageType == 1)
                    try
                        switch getFlagContrasteAuto
                            case 1
                                this = callback_contrast(this, 'ContrasteImageZoomeeMinMax', 'ModeAuto', 1);
                            case 2
                                this = callback_contrast(this, 'ContrasteImageZoomeeQuant_0p5pc', 'ModeAuto', 1);
                        end
                        this = traiter_typeInteractionSouris(this);
                    catch
                        my_breakpoint
                        % Un bug se produit parfois si on a une navigation affich�e,
                        % la callback du trac� de nav ne doit pas �tre tr�s bien g�r�e
                    end
                end
                
                %% Sauvegarde des noms de r�pertoire d'entr�e et de sortie par d�faut
                
                IfrTbxRepDefInput  = this.repImport;
                IfrTbxRepDefOutput = this.repExport;
                saveVariablesEnv
                
                if ishandle(this.hfig)
                    set(this.hfig, 'Pointer', this.Pointer);
                end
            end
        end
    end
end
