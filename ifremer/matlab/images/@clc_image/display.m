% Affichage des donnees d'une this de clc_image
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : instance de clc_image
%
% Examples
%   display(clc_image)
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
