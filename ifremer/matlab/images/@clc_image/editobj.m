% Representation de l'instance
%
% Syntax
%   a = editobj(a) 
%
% Input Arguments
%   a : instance de clc_image
%
% OUPUT PARAMETERS :
%   a : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = editobj(this)

%% Cr�ation de l'interface graphique

% this.cl_component = editobj(this.cl_component, 'handleAnchor', this.handleAnchor);
this.composant = editobj(this.composant, 'handleAnchor', this.handleAnchor);
this = construire_ihm(this);
