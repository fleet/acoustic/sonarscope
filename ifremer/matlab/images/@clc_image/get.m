% Lecture des attributs de l'instance
%
% Syntax
%   [...] = get(a, ...)
% 
% Input Arguments
%   a : instance de clc_image
%
% Name-Value Pair Arguments
%    fLabelX           : flag affichage des labels selon x, 0 ou 1
%    fLabelY           : flag affichage des labels selon y, 0 ou 1
%    visuGrid          : flag de mise en place de la grille, 0 ou 1
%    visuCoupeHorz       : flag affichage profil selon x, 0 ou 1
%    visuCoupeVert       : flag affichage profil selon y, 0 ou 1
%    visuColorbar      : flag affichage barre de couleurs
%    hauteurProfilX    : hauteur (en nb lignes) du profil en X
%    largeurProfilY    : largeur (en nb colonnes) du profil en Y
%    largeurColorBar   : largeur (en nb colonnes) de la colorbar
%    margeLabelX       : marge (en nb lignes) du label x de l axe
%    margeLabelY       : marge (en nb colonnes) du label y de l axe
%    nbTotLignes       : nombre total de lignes
%    nbTotColonnes     : nombre total de colonnes
%    lstMsgToolBar     : liste des messages de callback de la toolbar
%    msgMoveCurseur    : msg de call back pour le deplacement du curseur
%    msgClick          : msg de call back pour le click
%    valeurX           : valeur sous le curseur en X
%    valeurY           : valeur sous le curseur en Y
%    valeurV           : valeur V pour valeurX et valeurY
%    txtProfilX        : texte de toolbar pour profilX
%    txtProfilY        : texte de toolbar pour profilY
%    txtValeurX        : texte de toolbar pour X
%    txtValeurY        : texte de toolbar pour Y
%    txtValeurV        : texte de toolbar pour V
%    colormap          : table de couleur employee
%
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant
%    componentEnable   : accessibilite du composant
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%    componentInsetX   : marge en X entre elements de ce composant
%    componentInsetY   : marge en Y entre elements de ce composant
%    componentAnchor   : ancre du composant
%
% Output Arguments
%   liste des valeurs des proprietes indiquees en arguments
% 
% Examples
%    v = clc_image; 
%    set(v, 'componentName', 'Un nom bien choisi');
%    get(v, 'componentName')
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};

for i=1:length(varargin)
    switch varargin{i}
        case 'Images'
            varargout{end+1} = this.Images;   %#ok<AGROW>
        case 'indImage'
            varargout{end+1} = this.indImage;   %#ok<AGROW>

        case 'visuGrid'
            varargout{end+1} = this.visuGrid;   %#ok<AGROW>
        case 'visuCoupeHorz'
            varargout{end+1} = this.visuCoupeHorz;   %#ok<AGROW>
        case 'visuCoupeVert'
            varargout{end+1} = this.visuCoupeVert;   %#ok<AGROW>
        case 'visuColorbar'
            varargout{end+1} = this.visuColorbar;   %#ok<AGROW>
        case 'identVisu'
            varargout{end+1} = this.identVisu;   %#ok<AGROW>
        case 'TypeInteractionSouris'
            varargout{end+1} = this.TypeInteractionSouris;   %#ok<AGROW>
        case 'NbVoisins'
            varargout{end+1} = this.NbVoisins;   %#ok<AGROW>
        case 'repImport'
            varargout{end+1} = this.repImport;   %#ok<AGROW>
        case 'repExport'
            varargout{end+1} = this.repExport;   %#ok<AGROW>

        case 'fLabelX'
            varargout{end+1} = this.fLabelX;   %#ok<AGROW>

        case 'fLabelY'
            varargout{end+1} = this.fLabelY;   %#ok<AGROW>

        case 'hauteurProfilX'
            varargout{end+1} = this.hauteurProfilX;   %#ok<AGROW>

        case 'largeurProfilY'
            varargout{end+1} = this.largeurProfilY;   %#ok<AGROW>

        case 'largeurColorBar'
            varargout{end+1} = this.largeurColorBar;   %#ok<AGROW>

        case 'nbTotLignes'
            varargout{end+1} = this.nbTotLignes;   %#ok<AGROW>

        case 'nbTotColonnes'
            varargout{end+1} = this.nbTotColonnes;   %#ok<AGROW>

        case 'colormap'
            varargout{end+1} = this.colormap;   %#ok<AGROW>

        case 'margeLabelX'
            varargout{end+1} = this.margeLabelX;   %#ok<AGROW>

        case 'margeLabelY'
            varargout{end+1} = this.margeLabelY;   %#ok<AGROW>

        case 'cross'
            varargout{end+1} = this.cross;   %#ok<AGROW>

        case 'visuLim'
            visuLim = this.visuLim;
            visuLim(this.indImage, :) = axis(this.hAxePrincipal);
            varargout{end+1} = visuLim;   %#ok<AGROW>

        case 'visuLimHistory'
            varargout{end+1} = this.visuLimHistory;   %#ok<AGROW>

        case 'ZoneEtude'
            varargout{end+1} = this.ZoneEtude;   %#ok<AGROW>
            
        case 'ZoneEtudeInd'
            varargout{end+1} = this.ZoneEtudeInd;   %#ok<AGROW>

        case 'visuLimHistoryPointer'
            varargout{end+1} = this.visuLimHistoryPointer;   %#ok<AGROW>

        case 'valeurX'
            if is_edit(this)
                varargout{end+1} = get_value(this.ihm.ValX.cpn);   %#ok<AGROW>
            else
                varargout{end+1} = NaN;   %#ok<AGROW>
            end

        case 'valeurY'
            if is_edit(this)
                varargout{end+1} = get_value(this.ihm.ValY.cpn);   %#ok<AGROW>
            else
                varargout{end+1} = NaN;   %#ok<AGROW>
            end

        case 'valeurV'
            if is_edit(this)
                varargout{end+1} = get_value(this.ihm.ValImage.cpn);   %#ok<AGROW>
            else
                varargout{end+1} = NaN;   %#ok<AGROW>
            end

        case 'txtProfilX'
            varargout{end+1} = this.txtProfilX;   %#ok<AGROW>

        case 'txtProfilY'
            varargout{end+1} = this.txtProfilY;   %#ok<AGROW>

        otherwise
            varargout{end+1} = get(this.composant, varargin{i});  %#ok<AGROW>
    end
end
