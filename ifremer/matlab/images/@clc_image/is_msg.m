% Test si message est un message du composant
%
% Syntax
%   flag = is_msg(a, msg)
%
% Input Arguments
%   a   : instance de clc_image
%   msg : Message identifiant l'action a realiser
%
% Output Arguments
%   flag : 1=le message est un message du composant
%          0=le message n'est pas  un message du composant
%
% Examples
%   c = clc_image
%   flag = is_msg(c, 'toto')
%   flag = is_msg(c, 'Msg clc_image xxxxxx')
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = is_msg(this, msg)

switch msg
    case this.msgMoveCurseur
        flag = 1;
    case 'zoom'
        flag = 1;
    case this.msgClick
        flag = 1;
    case this.msgKeyPress
        flag = 1;
    case this.msgMoveProfils
        flag = 1;
        
    case this.msgCleanData
        flag = 1;
        
    case this.msgMolette
        flag = 1;
    case this.msgClickCentre
        flag = 1;
    case this.msgClickCoupeDown
        flag = 1;
    case this.msgClickHeightDown
        flag = 1;
    case this.msgClickSetNaN
        flag = 1;
    case this.msgClickSetVal
        flag = 1;
    case this.ihm.NomImage.msg
        flag = 1;
    case this.ihm.IdentImage.msg
        flag = 1;
    case this.ihm.ValX.msg
        flag = 1;
    case this.ihm.ValY.msg
        flag = 1;
    case this.ihm.ValImage.msg
        flag = 1;
    case this.ihm.CoupeHorz.msg
        flag = 1;
    case this.ihm.CoupeVert.msg
        flag = 1;
    case this.ihm.Stats.msg
        flag = 1;
    case this.ihm.Export.msg
        flag = 1;
    case this.ihm.Import.msg
        flag = 1;
    case this.ihm.XDir.msg
        flag = 1;
    case this.ihm.YDir.msg
        flag = 1;
    case this.ihm.Video.msg
        flag = 1;
    case this.ihm.ViewImage.msg
        flag = 1;
    case this.ihm.Contraste.msg
        flag = 1;
    case this.ihm.Grid.msg
        flag = 1;
    case this.ihm.TypeInteractionSouris.msg
        flag = 1;
    case this.ihm.Colorbar.msg
        flag = 1;
    case this.ihm.ColormapIndex.msg
        flag = 1;
    case this.ihm.NbVoisins.msg
        flag = 1;
    case this.ihm.Delete.msg
        flag = 1;
    case this.ihm.Info.msg
        flag = 1;
    case this.ihm.Compute.msg
        flag = 1;
    case this.ihm.ComputeSonar.msg
        flag = 1;
    case this.ihm.ZoneEtude.msg
        flag = 1;

        
    case this.ihm.VisuImage.msg
        flag = 1;
    case this.ihm.QuestionLevel.msg
        flag = 1;
    case this.ihm.ParallelTbx.msg
        flag = 1;
%     case this.ihm.Visu3D.msg
%         flag = 1;
    case this.ihm.CloneWindow.msg
        flag = 1;
    case this.ihm.VisuPlot.msg
        flag = 1;
    case this.ihm.VisuPlotVoisinsX.msg
        flag = 1;
    case this.ihm.VisuPlotVoisinsY.msg
        flag = 1;


    case this.ihm.QuickLook.msg
        flag = 1;
    case this.ihm.Zoom1.msg
        flag = 1;
    case this.ihm.MoveUp.msg
        flag = 1;
    case this.ihm.MoveDown.msg
        flag = 1;
    case this.ihm.MoveLeft.msg
        flag = 1;
    case this.ihm.MoveRight.msg
        flag = 1;
    case this.ihm.ZoomPrecedent.msg
        flag = 1;
    case this.ihm.ZoomSuivant.msg
        flag = 1;
    case this.ihm.ZoomInX.msg
        flag = 1;
    case this.ihm.ZoomOutX.msg
        flag = 1;
    case this.ihm.ZoomInY.msg
        flag = 1;
    case this.ihm.ZoomOutY.msg
        flag = 1;
%     case this.ihm.AxisEqual.msg
%         flag = 1;

    case this.ihm.IX.msgEdit
        flag = 1;
    case this.ihm.IX.msgPlus
        flag = 1;
    case this.ihm.IX.msgMoins
        flag = 1;

    case this.ihm.IY.msgEdit
        flag = 1;
    case this.ihm.IY.msgPlus
        flag = 1;
    case this.ihm.IY.msgMoins
        flag = 1;
        
    case this.ihm.PrintPixel.msg
        flag = 1;
        
    case this.ihm.HelpSession.msg
        flag = 1;
    case this.ihm.SaveSession.msg
        flag = 1;
    case this.ihm.RestoreSession.msg
        flag = 1;
        
    case this.msgClickOnColorbar
        flag = 1;
        
    otherwise
        flag = 0;
end
