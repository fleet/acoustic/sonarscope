% Test si msg est le message lie au deplacement de la souris
% 
% Syntax
%   flag = is_msgMoveCurseur(a, msg)
%
% Input Arguments
%   a   : instance de clc_image
%   msg : Message identifiant l'action a realiser
%
% Output Arguments
%   flag : 1=le message est celui du deplacement de la souris
%          0=le message n'est pas celui du deplacement de la souris
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = is_msgMoveCurseur(this, msg)

if strcmp(msg, this.msgMoveCurseur)
    flag = 1;
else
    flag = 0;
end
