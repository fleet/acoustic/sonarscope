function flag = is_msgMoveProfils(this, msg)

if strcmp(msg, this.msgMoveProfils)
    flag = 1;
else
    flag = 0;
end
