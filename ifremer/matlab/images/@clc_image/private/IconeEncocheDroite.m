function icone = IconeEncocheDroite(icone)
sz = size(icone);
suby = sz(1)-4:sz(1);
for k=1:length(suby)
    subx = sz(2)-k:sz(2);
    icone(suby(k),subx,1) = 255;
    icone(suby(k),subx,2) = 0;
    icone(suby(k),subx,3) = 0;
end
