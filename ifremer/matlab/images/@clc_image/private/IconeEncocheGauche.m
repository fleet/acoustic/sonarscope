function icone = IconeEncocheGauche(icone)
sz = size(icone);
suby = sz(1)-4:sz(1);
for k=1:length(suby)
    subx = 1:k;
    icone(suby(k),subx,1) = 255;
    icone(suby(k),subx,2) = 0;
    icone(suby(k),subx,3) = 0;
end
