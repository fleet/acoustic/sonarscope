function InfoMapDimensions(this, msg)

[flag, subx, suby, nomZoneEtude] = getSubxSubyContextuel(this, msg); %#ok
x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');
XLim = x(subx([1 end]));
YLim = y(suby([1 end]));

GeometryType = this.Images(this.indImage).GeometryType;
switch GeometryType
    case cl_image.indGeometryType('LatLong')
        YStepMetrique = get(this.Images(this.indImage), 'YStepMetric');
        strSuperficie = info_superficie(this.Images(this.indImage), 'subx', subx, 'suby', suby);
        info_map_dimensions(YLim, XLim, 'YStepMetrique', YStepMetrique, 'Append', strSuperficie)
    case cl_image.indGeometryType('GeoYX')
        YStep = get(this.Images(this.indImage), 'YStep');
        Carto = get(this.Images(this.indImage), 'Carto');
        [lat, lon] = xy2latlon(Carto, [YLim(1) YLim(1) YLim(2) YLim(2) ], [XLim(1) XLim(2) XLim(2) XLim(1)]);
        LatLim = [min(lat) max(lat)];
        LonLim = [min(lon) max(lon)];
        strSuperficie = info_superficie(this.Images(this.indImage), 'subx', subx, 'suby', suby);
        info_map_dimensions(LatLim, LonLim, 'YStepMetrique', YStep, 'Append', strSuperficie)
    otherwise
        flag = testSignature(this.Images(this.indImage), 'GeometryType', 'PingXxxx'); %#ok<NASGU>
        str1 = 'Cette g�om�trie ne permet pas de faire ce calcul.';
        str2 = 'This geometry cannot be used for plot dimensons processing.';
        my_warndlg(Lang(str1,str2), 1);
end