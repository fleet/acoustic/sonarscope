function InfoShortcuts(this) %#ok<INUSD>

% Callbacks trait�s dans callback_KeyPress

str = {};
str{end+1} = Lang('m  : Rehaussement de contraste Min Max', 'm  : Contrast Enhancement Min Max');
str{end+1} = Lang('c  : Rehaussement de contraste 0.5%', 'c  : Contrast Enhancement 0.5%');
str{end+1} = Lang('C  : Rehaussement de contraste 1%', 'C  : Contrast Enhancement 1%');
str{end+1} = Lang('S  : Ombrage couleur', 'S  : Sun Illuminated');
str{end+1} = Lang('i  : Interpolation', 'i  : Interpolation');
str{end+1} = Lang('N  : Mise � NaN des pixels', 'N  : Set pixels to NaN');
str{end+1} = Lang('H  : Histogramme', 'H  : Histogram');
str{end+1} = Lang('v  : Moyennage vertical', 'v  : Vertical average');
str{end+1} = Lang('h  : Moyennage horizontal', 'h  : Horizontal Average');
str{end+1} = Lang('s  : Copie d''�cran', 's  : Screendump');
str{end+1} = Lang('V  : Profil vertical', 'V  : Vertical profile');
str{end+1} = Lang('x  : Exportation au format .xml', 'x  : Export image in .xml');
str{end+1} = Lang('t  : Exportation au format .tif', 't  : Export image in .tif');
str{end+1} = Lang('j  : Exportation au format .jpg', 'j  : Export image in .jpg');
str{end+1} = Lang('b  : Exportation au format .bmp', 'b  : Export image in .bmp');
str{end+1} = Lang('g  : Exportation au format Google-Earth', 'g  : Export image in Google-Earth');
str{end+1} = Lang('G  : Exportation au format GeoTif 32 bits (pour ArcGis)', 'G  : Export image in Geotif 32 bits (for ArcGIS)');
str{end+1} = Lang('e  : Exportation au format ErMapper (.ers)', 'e  : Export image in ErMapper format (.ers)');
str{end+1} = Lang('x  : Exportation au format XML', 'x  : Export image in XML format');
str{end+1} = 'p  : Set Point & Click Mode';
str{end+1} = 'z  : Set Zoom Mode';
str{end+1} = '+  : Zoom * 2';
str{end+1} = '-  : Zoom / 2';
str{end+1} = '*  : Zoom * 1.25';
str{end+1} = '/  : Zoom / 1.25';
str{end+1} = '1  : 1 pixel for 1 pixel';
str{end+1} = '2  : Zoom factor 2, 3 : Zoom factor 3, ...';
str{end+1} = 'q  : Quick-Look';
str{end+1} = Lang('-> : 1/2 d�calage vers la droite', '-> : 1/2 Shift right');
str{end+1} = Lang('<- : 1/2 d�calage vers la gauche', '<- : 1/2 Shift left');
str{end+1} = Lang('   : 1/2 d�calage vers le haut', '   : 1/2 Shift top');
str{end+1} = Lang('   : 1/2 d�calage vers le bas', '   : 1/2 Shift down');
str{end+1} = Lang('T  : Positionnement en haut', 'T  : Top of Image');
str{end+1} = Lang('B  : Positionnement en bas', 'B  : Bottom of Image');
str{end+1} = Lang('L  : Positionnement � Gauche', 'L  : Left of Image');
str{end+1} = Lang('R  : Positionnement � Droite', 'R  : Right of Image');
str{end+1} = Lang('<  : Recherche de la valeur minimale', '<  : Search Min value');
str{end+1} = Lang('>  : Recherche de la valeur maximale', '>  : Search Max value');
str{end+1} = Lang('=  : Recherche de la valeur m�diane', '=  : Search Median value');
str{end+1} = Lang('Suppr : Suppression de l''image', 'Suppr : Remove image');
str{end+1} = Lang('%  : Filtre anti spike sur profil vertical', '%  : Spike filter on vertical signal');
str{end+1} = Lang('f  : Figure Matlab', 'f  : Matlab Figure');
str{end+1} = 'Mouse weel : change layer to previous on next one';

edit_infoPixel(str, [], 'PromptString', 'SonarScope Shortcuts on local image.')

% Libres : 0adefklnoruwxyADEFGIJKMNOPQUVWXYZ.;,?!�<>&^$