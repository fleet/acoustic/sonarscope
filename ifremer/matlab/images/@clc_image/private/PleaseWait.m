function PleaseWait(this, flag)

Tag = ['PleaseWaitWindowStyleModal_' this.internalNumber];
if flag
    str1 = 'Un traitement est en cours (cette fen�tre est utile � SonarScope pour indiquer qu''un traitement est en cours).';
    str2 = 'A processing has begun (this window is useful for SonarScope to know if something is running), please wait ...';
    h = my_warndlg(Lang(str1,str2),  0, 'Tag', Tag, 'Timer', 0, 'Latency', 3);
    set(h, 'WindowStyle', 'modal', 'Visible', 'Off')
else
    str1 = 'En attente d''une action op�rateur.';
    str2 = 'Waiting for User action.';
    WorkInProgress(Lang(str1,str2), 'Gap', 0);
end