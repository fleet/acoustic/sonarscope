function PrintPixel(this, varargin)

[varargin, CreateWindow] = getPropertyValue(varargin, 'CreateWindow', 1); %#ok<ASGLU>

str = private_type_voisins_xy(this);
if ~my_isdeployed
    display(cell2str(str))
end
if CreateWindow
    edit_infoPixel(str, this.NomFicPoints, 'Tag', 'InfoPixelImage');
end
