% Affichage des masques contenus dans l'image courante
%
% Syntax
%   ROI_display(this)
%
% Input Arguments
%   this : Instance de cl_image
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ROI_display(this, varargin)

[varargin, NoQuestion] = getFlag(varargin, 'NoQuestion'); %#ok<ASGLU>

%% Synth�se de tous les contours

[this.Images(this.indImage), str] = ROI_summary(this.Images(this.indImage));

%% Lecture des r�gions d'int�r�t contenues dans l'image courante

RegionOfInterest = get(this.Images(this.indImage), 'RegionOfInterest');
if isempty(RegionOfInterest)
    if ~NoQuestion
        my_warndlg('No Region Of Interest in this image', 1);
    end
    return
end

%% Affichage de tous les contours dans une nouvelle figure

if NoQuestion
    rep = 1:length(str);
else
    ROI_plot(this.Images(this.indImage));
    
    % -------------------------------------------------
    % S�lection des contours � afficher dans SonarScope
    
    [rep, flag] = my_listdlg(Lang('R�gions d''int�r�t � visualiser : ', 'ROI to display : '), str, ...
        'InitialValue', 1:length(str), 'Entete', 'IFREMER - SonarScope - ROI export');
    if ~flag
        return
    end
    if isempty(rep)
        return
    end
end

%% Au cas o�

h = findobj(this.hfig, 'Tag', 'PlotRegionOfInterest');
for i=1:length(h)
    delete(h(i));
end

%% Affichage dans SonarScope

set(0, 'CurrentFigure', this.hfig)
set(this.hfig, 'CurrentAxes', this.hAxePrincipal);
W = axis(this.hAxePrincipal);
for k=1:length(rep)
    ind = rep(k);
    
    pX   = RegionOfInterest(ind).pX;
    pY   = RegionOfInterest(ind).pY;
    coul = RegionOfInterest(ind).Color(1,:);
    
    if all(pX < W(1)) || all(pX > W(2)) || all(pY < W(3)) || all(pY > W(4))
        continue
    end
    
    hold on;
    %     figure; h = PlotUtils.createSScPlot(pX, pY, 'color', coul);
    %     h = my_patch(pX, pY, coul, 'Name', RegionOfInterest(ind).strLabel);
    
    if ischar(coul) && isempty(find(strcmp(coul, {'r'; 'g'; 'b'; 'w'; 'm'; 'y'; 'c'; 'k'}), 1))
        coul = 'b';
    end
    h = my_patch(pX, pY, coul, 'Name', RegionOfInterest(ind).Name);
    set(h,'Tag', 'PlotRegionOfInterest');
    
    %     h = text(mean(pX), mean(pY), RegionOfInterest(ind).strLabel);
    h = text(mean(pX), mean(pY), RegionOfInterest(ind).Name);
    set(h,'Tag', 'PlotRegionOfInterest');
    set(h, 'Color', coul);
end
axis(this.hAxePrincipal, W);
