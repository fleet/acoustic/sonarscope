function [flag, this] = RenameWorkWindow(this)

Name = get(this.hfig, 'Name');

str1 = 'Nouveau nom :';
str2 = 'New name :';
nomVar = {Lang(str1,str2)};
value = {[Name ' : ']};
[Name, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
set(this.hfig, 'Name', Name{1});