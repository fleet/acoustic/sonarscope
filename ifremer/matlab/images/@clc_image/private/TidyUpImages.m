function [flag, this] = TidyUpImages(this)

lastIndImage = this.indImage;
% alternative: jEditbox.setContentType('text/html');
htmlStr = [];
[flag, indImage, strOut] = summary(this.Images, 'ExportResults', 0, 'nomDir', this.repExport, ...
    'SelectLayer', lastIndImage, 'QueryMode', 'TidyUp', ...
    'Comments', htmlStr);
if ~flag
    % Bouton Cancel
    return
end
if isempty(indImage) || isempty(strOut)
    % Filtre total ou bouton Cancel
    return
end
for k=1:length(this.Images)
    strIn{k,1} = this.Images(k).Name; %#ok<AGROW>
end
% Remise en ordre par identification du nom du layer.
strOut = strOut(:,1);

%             [C, ia, ItemsSelect] = intersect(strIn(:,1), strOut(:,1), 'stable');%#ok<ASGLU>
[C, ia, ItemsSelect] = intersect(strOut(:,1), strIn(:,1), 'stable'); %#ok<ASGLU> % Modif JMA le 30/09/2015

this.Images                = this.Images(ItemsSelect);
this.cross                 = this.cross(ItemsSelect, :);
this.identVisu             = this.identVisu(ItemsSelect);
this.visuLim               = this.visuLim(ItemsSelect, :);
this.visuLimHistory        = this.visuLimHistory(ItemsSelect);
this.visuLimHistoryPointer = this.visuLimHistoryPointer(ItemsSelect);
try
    this.valBeforeSetNaN   = this.valBeforeSetNaN(ItemsSelect);
catch %#ok<CTCH>
    disp('Bug � corriger ici dans callback_delete')
end

% Focus sur le layer s�lectionn� en sortie apr�s r�ordonnancement
% des donn�es.
for k=1:length(this.Images)
    strIn{k,1} = this.Images(k).Name;
end
selectLayer = strOut(indImage,1);
[C, ia, ib] = intersect(strIn(:,1), selectLayer, 'stable'); %#ok<ASGLU>
this.indImage = ia;
flag = 1;
