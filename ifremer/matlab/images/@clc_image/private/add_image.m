% Creation d'une nouvelle vue
%
% Syntax
%   this = add_image(thi, a)
% 
% Input Arguments
%   this : Instance de clc_image
%   a    : Nouvelle instance de cl_image
%
% Output Arguments
%   this : Instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = add_image(this, a)

set(0, 'CurrentFigure', this.hfig);

N = length(this.Images);
for k=1:N
    listeImageName{k} = this.Images(k).Name; %#ok<AGROW>
end
for k=1:length(a)
    a(k) = add_CourbesStatistiques(a(k), this.PublicCurves);
    this = add_image_unitaire(this, a(k), listeImageName);
    listeImageName{k+N} = this.Images(k+N).Name;
end


function this = add_image_unitaire(this, a, listeImageName)

N = length(this.Images) + 1;
this.Images(N) = a;

TagSynchroX = this.Images(this.indImage).TagSynchroX;
if strcmp(TagSynchroX, a.TagSynchroX)
    this.cross(N, 1)     = this.cross(   this.indImage, 1);
    this.visuLim(N, 1:2) = this.visuLim( this.indImage, 1:2);
else
    XLim = get(a, 'XLim');
    this.cross(  N, 1)   = mean(XLim);
    this.visuLim(N, 1:2) = XLim;
end

if strcmp(this.Images(this.indImage).TagSynchroY, a.TagSynchroY)
    TagSynchroYScale = this.Images(this.indImage).TagSynchroYScale;
    aScale = a.TagSynchroYScale;
    if isempty(TagSynchroYScale) || isempty(aScale)
        this.cross(  N, 2)   = this.cross(   this.indImage, 2);
        this.visuLim(N, 3:4) = this.visuLim( this.indImage, 3:4);
    else
        ratio = TagSynchroYScale / aScale;
        this.cross(  N, 2)   = this.cross(  this.indImage, 2) * ratio;
        this.visuLim(N, 3:4) = this.visuLim(this.indImage, 3:4) * ratio;
%         this.Images(this.indImage)          = set_YLim(this.Images(k), YLim * ratio);
    end
else
    YLim = get(a, 'YLim');
    this.cross(  N, 2)   = mean(YLim);
    this.visuLim(N, 3:4) = YLim;
    this.profilY.Type = 'Image profile';
end

%% Recalcul des coordonnées du milieu du pixel

if this.Images(N).DataType == cl_image.indDataType('Correlation')
    this.cross(N, 1) = 0;
    this.cross(N, 2) = 0;
else
    [ix, iy] = xy2ij(this.Images(N), this.cross(N, 1), this.cross(N, 2));
    [x, y] = ij2xy(this.Images(N), ix, iy);
    this.cross(N, 1) = x;
    this.cross(N, 2) = y;
end

this.identVisu(N)             = this.identVisu(this.indImage);
this.visuLimHistory{N}        = this.visuLimHistory{this.indImage};
this.visuLimHistoryPointer(N) = this.visuLimHistoryPointer(this.indImage);

if length(this.valBeforeSetNaN) >= this.indImage
    this.valBeforeSetNaN{N} = this.valBeforeSetNaN{this.indImage};
else
    this.valBeforeSetNaN{N} = [];
end

%% Affichage du nom de l'image dans la liste

for k=1:length(this.Images)-1
    x = listeImageName{k};
    
    if strcmp(x(1), '(')
        k1 = strfind(x, ')');
        x = x(k1+1:end);
    end
    if (length(x) > 12) && strcmp(x(end), ')') && strcmp(x(end-9), '(')
        X{k} = x(1:end-11); %#ok<AGROW>
    else
        X{k} = x; %#ok<AGROW>
    end
end
aName = a.Name;
flag = strcmp(X, aName);
NumVer = sum(flag);
if NumVer > 0
    aName= sprintf('(%d)%s', NumVer, aName);
    this.Images(N).Name = aName;
end
CurrentImageName = this.Images(N).Name;
CurrentImageName = [CurrentImageName ' (' datestr(now,13) ')'];

this.Images(N).Name = CurrentImageName;

this.indImage = N;
this.ihm.NomImage.cpn = set_texte(this.ihm.NomImage.cpn, CurrentImageName);

maj_TooltipBoutonSelectionImage(this)
