% TODO : il faudrait sortir cette fonction de clc_image mais il y a taff !

function [flag, this] = calcul_TextureTheseApprentissageCalcul(this, indImage, subx, suby)

[flag, binsImage, indLayerAngle, binsLayerAngle, numTexture, ...
    nomTexture, depMaxClique, UseCooc, UseGabor] = params_FonctionTextureThese(this, indImage, subx, suby);
if ~flag
    return
end

[flag, indLayerMask, valMask, Coul] = paramsMasque(this, indImage, 'InitialValue', 'Segmentation');
if isempty(indLayerMask)
    str1 = 'Aucun mayer de masque n''est associ� � cette image, utilisez "ROI / Masques" pour en d�finir.';
    str2 = 'No Mask layer associated to the image, use "ROI / Masks" to define one.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
if ~flag
    return
end

if isempty(indLayerMask)
    my_warndlg(Lang('Pas de masque associ�.', 'No associated mask.'), 1);
    return
end

ROI = this(indLayerMask).RegionOfInterest;
% if ~isempty(ROI) % ?????? ne sert pas, si ?
%     ROI = ROI(end);
% end

Masque = get_Masque_ij(this(indLayerMask), subx, suby);
    
for iTexture=1:length(valMask)
        
    if isempty(ROI) || ~isfield(ROI, 'nomCourbe') || isempty(ROI(iROI).nomCourbe) || (length(ROI(iROI).nomCourbe) < valMask(iTexture))
        strNomCourbe = sprintf('Facies : %d', iTexture);
        strCommentaire = '';
    else
        iROI = [ROI.labels] == valMask(iTexture);
        iROI = find(iROI, 1, 'first');
%         strNomCourbe   = ROI.nomCourbe{valMask(iTexture)};
%         strCommentaire = ROI.Commentaire{valMask(iTexture)};
        strNomCourbe   = ROI(iROI).nomCourbe;
        strCommentaire = ROI(iROI).Commentaire;
        if isempty(strNomCourbe)
            strNomCourbe = sprintf('Facies : %d', iTexture);
        end
        if isempty(strCommentaire)
            strCommentaire = '';
        end
    end
    
    nomCourbe{iTexture}   = strNomCourbe; %#ok
    Commentaire{iTexture} = strCommentaire; %#ok
    
    MasqueTexture{iTexture} = (Masque == valMask(iTexture));  %#ok<AGROW>
end
drawnow

%%  Quantification des images

[I, Indices] = texture_calcul_init(this(indImage), binsImage,  this(indLayerAngle), binsLayerAngle, ...
    'subx', subx, 'suby', suby);

%% Gabor

maxWidthSlidingWindow = 16 + 1;
if UseGabor
    [CoefsGabor, strCoefsGabor] = Imen_getCoefGabor4; %#ok<ASGLU>
    nbFiltresGabor = size(CoefsGabor, 1);
    MinMaxGabor = zeros(nbFiltresGabor,2);
    
    str1 = 'Filtrage de l''image par les filtre de Gabor.';
    str2 = 'Filtering Image by all Gabor filters.';
    hw = create_waitbar(Lang(str1,str2), 'N', nbFiltresGabor);
    % pppp=5555;
    flagGabor = true(1,nbFiltresGabor);
    maxWidthSlidingWindow = zeros(1,nbFiltresGabor) + maxWidthSlidingWindow;
    for kGabor=1:nbFiltresGabor
        my_waitbar(kGabor, nbFiltresGabor, hw)
        
        %     p1 = CoefsGabor(kGabor, 1);
        %     p2 = CoefsGabor(kGabor, 2);
        %     p3 = CoefsGabor(kGabor, 3);
        % %     g = gfcreatefilter2(p1, p2, p3, p3, WGabor);
        %     g = gfcreatefilter2(p1, p2, p3, p3, WGabor);
        iFreq  = CoefsGabor(kGabor, 1);
        iTheta = CoefsGabor(kGabor, 2);
        iSigma = CoefsGabor(kGabor, 3);
%         g = GaborWavelet(iTheta, iFreq, iSigma);
        g = GaborRIF(iFreq, iTheta, iSigma);
        
        % {
        J = get_val_ij(this(indImage), suby,subx);
        valNaN = this(indImage).ValNaN;
        if ~isnan(valNaN)
            subNaN = (J == valNaN);
            J = single(J);
            J(subNaN) = NaN;
            clear subNaN
        end
        % }
        
        [n1, n2] = size(g);
        maxWidthSlidingWindow(kGabor) = max([maxWidthSlidingWindow(kGabor) n1 n2]);
        for iTexture=1:length(valMask)
            subMasque = imerode(MasqueTexture{iTexture}, ones(maxWidthSlidingWindow(kGabor)));
            nbPixelsInMasque = sum(subMasque(:));
            if nbPixelsInMasque == 0
                str1 = sprintf('Le masque %d, est trop petit', iTexture);
                str2 = 'TODO';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasAssezDePixelsDansMasque');
                flagGabor(kGabor) = false;
            end
        end
        if ~flagGabor(kGabor)
            IGabor{kGabor} = []; %#ok<AGROW>
            continue
        end
        
        J = fillNaN_mean(J, size(g));
        GG1 = filter2(g, J);
        GG1 = real(GG1) .^ 2 + imag(GG1) .^ 2;

        [n1, n2] = size(g);
        n1 = floor(n1/2);
        n2 = floor(n2/2);
        GG1(1:n1,:) = NaN;
        GG1(end-n1+1:end,:) = NaN;
        GG1(:,1:n2) = NaN;
        GG1(:,end-n2+1:end) = NaN;
        Val = stats(GG1);
%         Val = stats(GG1(subMasque));
        
        try
            IGabor{kGabor} = GG1; %#ok<AGROW>
        catch %#ok<CTCH>
            for i=1:kGabor-1
                if ~isa(IGabor{i}, 'cl_memmapfile')
                    IGabor{i} = cl_memmapfile('Value',  IGabor{i});
                end
            end
            IGabor{kGabor} = cl_memmapfile('Value', GG1); %#ok<AGROW>
        end
        
        Milieu = (Val.Quant_25_75(1) + Val.Quant_25_75(2)) / 2;
        D = 0.75 * (Val.Quant_25_75(2) - Val.Quant_25_75(1));
        %     MinMaxGabor(kGabor, :) = Val.Quant_25_75;
        MinMaxGabor(kGabor, :) = [Milieu-D Milieu+D];
        
        %     GG1 =(GG1 - MinMaxGabor(kGabor,1)) * (MinMaxGabor(kGabor,2) - MinMaxGabor(kGabor,1));
        %     GG1; %#ok<AGROW>
        % if (iFreq == 2) && (iTheta == 0)
        %     figure(32760); imagesc(GG1, MinMaxGabor(kGabor,:)); colorbar; title(strCoefsGabor{kGabor})
        % end
        %     figure(32760); imagesc(GG1, MinMaxGabor(kGabor,:)); colorbar; title(strCoefsGabor{kGabor})
        %     figure(pppp); imagesc(GG1, MinMaxGabor(kGabor,:)); colorbar; title(strCoefsGabor{kGabor})
        %     pppp
    end
    my_close(hw)
    % figure; PlotUtils.createSScPlot(MinMaxGabor(:,1), 'b-+'); grid on;
    % hold on; PlotUtils.createSScPlot(MinMaxGabor(:,2), 'r-x'); grid on;
else
    IGabor = [];
    MinMaxGabor = [];
    flagGabor = [];
end

%% Calcul des signatures texturales pour chaque valeur de masque

clear subMasque
N = length(valMask);
hw = create_waitbar(Lang('Calcul des signatures', 'Learning signatures'), 'N', N);
for iTexture=1:N
    while depMaxClique >= 0
        subMasque = imerode(MasqueTexture{iTexture}, ones(depMaxClique));
        nbPixelsInMasque = sum(subMasque(:));
        if nbPixelsInMasque < 16^2  % Cf coocMatNbNiveauxFiltre : if nbtr < min(16^2 / 2, numel(Image)/2)
            depMaxClique = depMaxClique -1;
            str1 = sprintf('Le masque %d, est trop petit', iTexture);
            str2 = 'TODO';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasAssezDePixelsDansMasque');
        else
            break
        end
    end
end

% if UseGabor
    [maxWidthSlidingWindow, ~, pointeurWidthSlidingWindow] = unique(maxWidthSlidingWindow);
% end
for iTexture=1:length(valMask)
    my_waitbar(iTexture, length(valMask), hw)
    
    J = I;
    J(~MasqueTexture{iTexture}) = NaN;
    
    this(indImage) = Imen_ApprentissageTest(this(indImage), ...
        UseCooc, UseGabor, J, IGabor, flagGabor, Indices, binsImage, ...
        this(indLayerAngle), binsLayerAngle, ...
        nomCourbe{iTexture}, Commentaire{iTexture}, ...
        numTexture, nomTexture, ...
        'valMask', valMask(iTexture), ...
        'depMaxClique', depMaxClique, ...
        'MinMaxGabor', MinMaxGabor, ...
        'Coul', Coul(iTexture,:));
    
    MasqueReduit{iTexture} = imerode(MasqueTexture{iTexture}, ones(maxWidthSlidingWindow(1))); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

%{
plotSegmSignature(this(indImage), 'numTexture', numTexture, 'OnlyOneFig', 1);
%}

% -------------------
% Calcul du vecteur w

% this(indImage) = Imen_Estimation_w(this(indImage), numTexture);
% this(indImage) = Imen_Estimation_w3(this(indImage), numTexture, MasqueReduit, I, IGabor);
% this(indImage) = Imen_Estimation_w4(this(indImage), numTexture, MasqueReduit, I, IGabor);
this(indImage) = Imen_Estimation_w5(this(indImage), numTexture, MasqueReduit, I, IGabor, maxWidthSlidingWindow, pointeurWidthSlidingWindow);
% this(indImage) = Imen_Estimation_w2(this(indImage), numTexture);
