% Fonction de tests des appels de CallBack_Sonar.m depuis la fonction de
% test : Test_clc_image_Sonar_Callback.m
% --------------------------------------------------------------------------

function callSonarCallBack(this, typeSonar, menuKeywords)

LogName = fullfile(my_tempdir, ['SonarScopeLog_' userdomain '_' username '.xml']);
if exist(LogName, 'file')
    delete(LogName);
end

for k=1:numel(menuKeywords)
    this = callback_SP(this, menuKeywords{:,k}{:});
end

%% Conservation apr�s effacement des r�sultats pr�alables.

LogNameTest = fullfile(my_tempdir, ['SonarScopeLog_' userdomain '_' username '_' typeSonar '.xml']);
delete(LogNameTest);
copyfile(LogName,LogNameTest)
edit(LogNameTest);
delete(LogName);
