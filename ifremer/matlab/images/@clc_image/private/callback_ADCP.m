function this = callback_ADCP(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'
DCP0 = cl_adcp([]);

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg       
    case 'ODV_Preprocessing_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.odv'; '.odv'; '.odv'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            preprocess_ODV(DCP0, listeFic);
        end
        
    case 'ODV_PreprocessingSignals_CI0'
        [flag, listeFic, this.repImport] = params_ODV_PreprocessingSignals(this.Images(this.indImage), this.repImport);
        if flag
            preprocess_ODVSignals(DCP0, listeFic);
        end
        
    case 'ODV_PlotNavigationAndTime_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.odv'; '.odv'; '.odv'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            plot_nav_adcp(DCP0, nomFic);
        end
        
    case 'ODV_Export3DViewer_CI0'
        [flag, listeFicODV, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, IdentPartData, TailleMax, Video, ...
            this.repImport, this.repExport] = params_ODV_Export3DViewer(this.Images(this.indImage), this.repImport, this.repExport);
        if flag
            if nomDirOut ~= 0
                flag = survey_Export3DViewer(DCP0, listeFicODV, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, ...
                    IdentPartData, TailleMax, Video);
            end
        end
        
    case 'ODV_CompressDirectories_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.odv'; '.odv'; '.odv'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            compressDirectories_SSc(nomFic, 'Tag', 'ODV');
        end
        
    case 'ODV_DeleteCache_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.odv', 'RepDefaut', this.repImport);
        if flag
            eraseSonarScopeCache(nomFic);
        end        

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
