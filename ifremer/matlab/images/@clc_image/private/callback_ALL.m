function this = callback_ALL(this, msg, subx, suby, nomZoneEtude, flagImageEntiere)

% typeFormatFichier = {'All'; 'XSF'}; % Pour s'arr�ter si .all ET .xsf
typeFormatFichier = {'All'};          % Pour s'arr�ter si .all
% typeFormatFichier = {'XSF'};        % Pour s'arr�ter si .xsf
% typeFormatFichier = {};             % Pour ne plus s'arr�ter

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

flag = 1;
if flagImageEntiere
    suby = [];
end
a  = cl_image.empty;

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg 
    case 'ALL_Visu_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, listeOptions] = ALL.Params.plotDatagram;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_plotDatagram(aKM, listeOptions);
                end
            end
        end

    case 'ALL_PlotNavigation_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
            if flag
                Fig = ALL_PlotNav(aKM, 'IndNavUnit', IndNavUnit); %#ok<NASGU>
            end
       end

    case 'ALL_PlotNavigationAndCoverage_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
            if flag
                Fig = ALL_PlotNav(aKM, 'IndNavUnit', IndNavUnit);
                ALL_PlotCoverage(aKM, 'Fig', Fig, 'IndNavUnit', IndNavUnit);
            end
        end

    case 'ALL_PlotNavigationAndSignal_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
       if flag
           [flag, signalName, Mute, modePlotData, step] = params_plotNavSignal(ALL.getKongsbergListSignals);
           if flag
               [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
               if flag
                   ALL_PlotNavSignal(aKM, signalName, 'IndNavUnit', IndNavUnit, 'Step', step, 'Mute', Mute, 'modePlotData', modePlotData);
               end
           end
       end
        
    case 'ALL_CreateFileMarkersRuntime_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicMarker, PlotNav] = ALL.Params.CreateFileMarkers(this.repExport, 'Runtime');
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_CreateFileMarkers(aKM, 'Runtime', nomFicMarker, PlotNav);
                end
            end
        end
        
    case 'ALL_CreateFileMarkersSounSpeedProfiles_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicMarker, PlotNav] = ALL.Params.CreateFileMarkers(this.repExport, 'SoundSpeedProfile');
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_CreateFileMarkers(aKM, 'SoundSpeedProfile', nomFicMarker, PlotNav);
                end
            end
        end
        
    case 'ALL_CreateFileMarkersInstallationParameters_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicMarker, PlotNav] = ALL.Params.CreateFileMarkers(this.repExport, 'InstallationParamaters');
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_CreateFileMarkers(aKM, 'InstallationParamaters', nomFicMarker, PlotNav);
                end
            end
        end

    case 'ALL_ImportNavigation_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav, this.repImport, ImportImmersion] = SSc.Params.ImportNavigation({'.txt'; '.dat'; '.nvi'; '.csv'}, this.repImport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_ImportExternalNav(aKM, nomFicNav, 'ImportImmersion', ImportImmersion);
                end
            end
        end
        
    case 'ALL_NavGPSAccuracy_CI0' % Checked for ALL-XSF
        message_ALL_GPSAccuracy;
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_GPSAccuracy(aKM);
            end
        end
        
    case 'ALL_NavExport_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicXML, this.repExport] = ALL.Params.ExportNav(this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_ExportNavAttitude3DV(aKM, nomFicXML);
                end
            end
        end
        
    case 'ALL_CacheErase_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            eraseSonarScopeCache(listFileNames);
        end        

    case 'ALL_SummaryDatagrams_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, listDatagrams, nomFicSummary, this.repExport] = ALL.Params.SummaryDatagrams('SummaryDatagrams.txt', this.repExport); % , 'MessageParallelTbx', 2
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_Summary_Datagrams(aKM, listDatagrams, nomFicSummary)
                end
            end
        end

    case 'ALL_SummaryLines_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicSummary, listeItems, this.repExport] = ALL.Params.SummaryLines('SummaryLines.csv', this.repExport); % , 'MessageParallelTbx', 2
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_Summary_Lines(aKM, nomFicSummary, 'listeItems', listeItems);
                end
            end
        end
        
        %------------------------------------------------------------------
        
    case 'ALL_SurveyReportCreate_CI0' % Checked
        [flag, adocFileName, WebSiteName, LogFileName, this.repImport] = Adoc.Params.SurveyReport_Create(this.repImport);
        if flag
            AdocUtils.createSurveyReportFramework(adocFileName, 'WebSiteName', WebSiteName, 'LogFileName', LogFileName);
            this.repExport = this.repImport;
        end

    case 'ALL_SurveyReportSummary_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, adocFileName, this.repExport] = Adoc.Params.SurveyReport_Summary(this.repExport);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
                if flag
                    ALL_SurveyReport_Summary(aKM, adocFileName, 'IndNavUnit', IndNavUnit);
                end
            end
        end

    case 'ALL_SurveyReportNavigation_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, adocFileName, ListSignals, this.repExport] = Adoc.Params.SurveyReport_Navigation(this.repExport);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
                if flag
                    ALL_SurveyReport_Navigation(aKM, adocFileName, ListSignals, 'IndNavUnit', IndNavUnit);
                end
            end
        end
        
    case 'ALL_SurveyReportSSP_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, adocFileName, this.repExport] = Adoc.Params.SurveyReport_Summary(this.repExport);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
                if flag
                    ALL_SurveyReport_SoundSpeedProfile(aKM, adocFileName, 'IndNavUnit', IndNavUnit);
                end
            end
        end

    case 'ALL_SurveyReportSummaryNavSSP_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag && ~isempty(listFileNames)
            [flag, adocFileName, ListSignals, this.repExport] = Adoc.Params.SurveyReport_Navigation(this.repExport);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
                if flag
                    ALL_SurveyReport_Summary(aKM, adocFileName, 'IndNavUnit', IndNavUnit, 'OpenAdoc', 0);
                    ALL_SurveyReport_Navigation(aKM, adocFileName, ListSignals, 'IndNavUnit', IndNavUnit, 'OpenAdoc', 0);
                    ALL_SurveyReport_SoundSpeedProfile(aKM, adocFileName, 'IndNavUnit', IndNavUnit);
                end
            end
        end
        
    case 'ALL_SurveyReportGlobalMosaic_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, adocFileName, gridSize, InfoCompensationCurveReflectivity, AreaName, this.repExport] = Adoc.Params.SurveyReport_GlobalMosaic(this.repExport);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1); % TODO
                if flag
                    listFileNames = get_nomFic(aKM);
                     % TODO : aKM plut�t que listFileNames
                    ALL.Process.SurveyReport_GlobalMosaic(adocFileName, listFileNames, gridSize, 'AreaName', AreaName, ...
                        'InfoCompensationCurveReflectivity', InfoCompensationCurveReflectivity, 'IndNavUnit', IndNavUnit)
                end
            end
        end
        
    case 'ALL_SurveyReportAddFigures_CI0' % Checked
        [flag, adocFileName, Title, Comment, hFig, this.repExport] = Adoc.Params.SurveyReport_AddFigures(this.repExport);
        if flag
            AdocUtils.addSomeDisplayedFigures(adocFileName, Title, Comment, hFig);
        end
        
    case 'ALL_SurveyReport_AddLatLongBathymetry_CI1' % Checked
        [flag, adocFileName, Title, Comment, this.repExport] = Adoc.Params.SurveyReport_AddLatLongImage(this.Images(this.indImage), this.repExport);
        if flag
            SurveyReport_AddLatLongBathymetry(this.Images(this.indImage), adocFileName, Title, Comment, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'ALL_SurveyReport_AddLatLongReflectivity_CI1' % Checked
        [flag, adocFileName, Title, Comment, this.repExport] = Adoc.Params.SurveyReport_AddLatLongImage(this.Images(this.indImage), this.repExport);
        if flag
            SurveyReport_AddLatLongReflectivity(this.Images(this.indImage), adocFileName, Title, Comment, ...
                'subx', subx, 'suby', suby)
        end
        
    case 'ALL_SurveyReport_AddLatLongEchoIntegration_CI1'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, adocFileName, Title, Comment, Thresholds, this.repExport] = Adoc.Params.SurveyReport_AddLatLongEchoIntegration(this.Images(this.indImage), this.repExport);
        if flag
            SurveyReport_AddLatLongEchoIntegration(this.Images(this.indImage), adocFileName, Title, Comment, ...
                Thresholds, 'subx', subx, 'suby', suby)
        end
        
    case 'ALL_SurveyReportPlotImageSignals_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, adocFileName, indexConfigs, this.repExport] = Adoc.Params.SurveyReport_PlotImageSignals(this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_SurveyReport_PlotImageSignals(aKM, adocFileName, 'indexConfigs', indexConfigs);
                end
            end
        end
         
    case 'ALL_SurveyReportSonarEquation_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, adocFileName, NomFicMNT, flagCreateCSV, this.repImport, this.repExport] ...
                = Adoc.Params.SurveyReport_SonarEquation(this.repImport, this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_SurveyReport_SonarEquation(aKM, adocFileName, NomFicMNT, flagCreateCSV)
                end
            end
        end
       
    case 'ALL_SurveyReport_Extract_CI0' % Checked
        [flag, adocFileName, nomDirOut, params, flagZip, this.repImport, this.repExport] = Adoc.Params.SurveyReport_Extract(this.repImport, this.repExport);
        if flag
            AdocUtils.extractShortVersion(adocFileName, nomDirOut, params, flagZip);
        end
        
    case 'ALL_SurveyReportCleanDirectories_CI0' % Checked
        [flag, adocFileName, this.repExport] = uiSelectFiles('ExtensionFiles', '.adoc', ...
            'RepDefaut', this.repExport, 'SubTitle', 'Survey Report file names');
        if flag
            AdocUtils.cleanDirectories(adocFileName);
        end
        
    case 'ALL_SurveyReportGroupShapeFiles_CI0' % Checked
        [flag, listShpFileNames, fileNameShpCoverage, fileNameShpNavigation, this.repImport, this.repExport] ...
            = Adoc.Params.GroupShapeFiles(this.repImport, this.repExport);
        if flag
            GroupShapeFiles(listShpFileNames, fileNameShpCoverage, fileNameShpNavigation);
        end
        
        %------------------------------------------------------------------

    case 'ALL_CleanBathyAndRange_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_CleanBathyAndRange(aKM)
            end
        end
     
    case 'ALL_CleanBathy_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_CleanBathy(aKM);
            end
        end
             
    case 'ALL_SOSBeamPointingAngle_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                process_SimradAll_SOSBeamPointingAngle(aKM);
            end
        end
     
    case 'ALL_SOSLevelArm_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            % Valeurs de la BUC % Valeurs th�oriques EM710 Atalante % Appliqu� sur EM710 Geoazur
            Ax = 16.02;         % Ax = 20.15;                       % Ax = 4.15;
            Ay = 0.01;          % Ay = 0.55;                        % Ay = 0.55;
            Az = -5.47;         % Az = -6.33;                       % Az = -0.86;
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                process_SimradAll_SOSLevelArm(aKM, Ax, Ay, Az);
            end
        end     
     
    case 'ALL_EditNav_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, CleanInterpolation] = ALL.Params.EditNav;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_EditNav(aKM, 'CleanInterpolation', CleanInterpolation);
                end
            end
        end
     
    case 'ALL_Spike2D_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, Seuil, SeuilRehabilitation, Marge] = SSc.Params.SpikeRemovalJMA;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_Spike2D(aKM, Seuil, SeuilRehabilitation, Marge);
                end
            end
        end
        
    case 'ALL_Spike1D_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nbRepeats, Threshold, StepDisplay] = ALL.Params.Spike1D;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_Spike1D(aKM, 'nbRepeats', nbRepeats, 'Threshold', Threshold, 'StepDisplay', StepDisplay);
                end
            end
        end
     
    case 'ALL_CleanFromDTM_Interactif_CI0' % Checked for XSF
        OperationMaintenance(typeFormatFichier, {'All'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, flagTideCorrection] = params_SonarCleanBathyFromDTM(this.Images(this.indImage));
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_CleanBathyFromDTM_Histo(aKM, this.Images(this.indImage), flagTideCorrection);
                end
            end
        end
     
    case 'ALL_CleanFromDTM_Threshold_CI0' % Checked for XSF
        OperationMaintenance(typeFormatFichier, {'All'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, Threshold, MaskIfNoDTM, flagTideCorrection] = params_SonarCleanBathyFromDTM_Threshold(this.Images(this.indImage));
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_CleanBathyFromDTM_Threshold(aKM, this.Images(this.indImage), Threshold, MaskIfNoDTM, flagTideCorrection);
                end
            end
        end
     
    case 'ALL_CleanReflectivity_MeanPerPing_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = params_CleanReflectivity;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_pingFlagsReflec(aKM);
                end
            end
        end
     
    case 'ALL_sonar_Simrad_CleanLayerUsingSignals_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, layerName, signalNames] = params_CleanLayerUsingSignals(listFileNames{1});
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_pingFlagsUsingSignals(aKM, layerName, signalNames);
                end
            end
        end
     
    case 'ALL_AUVCleanBathyPerPing_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                flag = ALL_pingFlagsUsingHeading(aKM);
            end
        end
     
    case 'ALL_CleanReflectivity_Histogram_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_CleanReflectivity(aKM);
            end
        end
        
    case 'ALL_CleanReflectivity_HistogramMultifiles_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_CleanReflectivityAtOnce(aKM);
            end
        end
        
    case 'ALL_WCProcessEchograms_CI1' % Checked for ALL-XSF
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repImport, this.repExport, TypeWCOutput, Names, ...
                StepCompute, StepDisplay, CorrectionTVG, ...
                CLimRaw, CompReflec, ModeDependant, flagTideCorrection, typeAlgoWC, ...
                MaskBeyondDetection, skipAlreadyProcessedFiles, MaxImageSize, ~, typeOutput] = ...
                ALL.Params.ComputeEchograms(listFileNames, this.repImport, this.repExport);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
                if flag
                    if typeOutput == 1 % XML-Bin
                        ALL.Process.ComputeEchogramsXMLBin(aKM, TypeWCOutput, Names, ...
                            'suby', suby, 'StepCompute', StepCompute, 'StepDisplay', StepDisplay, ...
                            'CorrectionTVG', CorrectionTVG, 'CLimRaw', CLimRaw, ...
                            'CompReflec', CompReflec, 'ModeDependant', ModeDependant, ...
                            'TideCorrection', flagTideCorrection, 'typeAlgoWC', typeAlgoWC, ...
                            'MaskBeyondDetection', MaskBeyondDetection, ...
                            'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, ...
                            'MaxImageSize', MaxImageSize, 'IndNavUnit', IndNavUnit);
                    else % GLOBE
                        ALL.Process.ComputeEchogramsGlobe(aKM, TypeWCOutput, Names, ...
                            'suby', suby, 'StepCompute', StepCompute, 'StepDisplay', StepDisplay, ...
                            'CorrectionTVG', CorrectionTVG, 'CLimRaw', CLimRaw, ...
                            'CompReflec', CompReflec, 'ModeDependant', ModeDependant, ...
                            'TideCorrection', flagTideCorrection, 'typeAlgoWC', typeAlgoWC, ...
                            'MaskBeyondDetection', MaskBeyondDetection, ...
                            'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, ...
                            'MaxImageSize', MaxImageSize, 'IndNavUnit', IndNavUnit);
                    end
                end
            end
        end
        
    case 'ALL_WC_ExportASCIIDepthAcrossDist_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, listFileNames, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, 'AllFilters', 1, ...
            'RepDefaut', this.repImport, 'ChaineIncluse', '_PolarEchograms', 'SubTitle', 'DepthAcrossDist WCD file selection (.xml or .nc)'); % 'ChaineIncluse', '_Raw_PolarEchograms');
        if flag
            [flag, params_ZoneEI, Display, ALim, configExport] = WCD.Params.ExportPolarEchogramOverSpecularCircleDepthAcrossDist('OverSpecular', 1);
            if flag
                if AnneeVersion >= 2021
                    WC_ExportPolarEchogramOverSpecularCircleDepthAcrossDist(listFileNames, params_ZoneEI, ALim, configExport, Display) % En cours de dev
                else
                    WC_ExportPolarEchogramOverSpecularCircleDepthAcrossDistR2019b(listFileNames, params_ZoneEI, ALim, configExport, Display)
                end
            end
        end
        
    case 'ALL_WC_EchoIntegration_CI1'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, params_ZoneEI, ...
            Tag, Display, this.repImport, this.repExport] ...
            = params_WaterColumnEchoIntegration(1, this.repImport, this.repExport);
        if flag
            ALL.Process.WaterColumnEchoIntegration(ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, ...
                'params_ZoneEI', params_ZoneEI, 'Tag', Tag, 'Display', Display, ...
                'suby', suby);
        end
        
    case 'ALL_WC_EchoIntegrationOverSeabed_CI1'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, params_ZoneEI, ...
            Tag, Display, this.repImport, this.repExport] ...
            = params_WaterColumnEchoIntegration(2, this.repImport, this.repExport);
        if flag
            ALL.Process.WaterColumnEchoIntegration(ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, ...
                'params_ZoneEI', params_ZoneEI, 'Tag', Tag, 'Display', Display, ...
                'suby', suby);
        end
  
    case 'ALL_WC_EI_PingAcrossDist_SignalVsPings_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, ListeFicEI, DataName, TypeUnit, ~, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag, ~, this.repImport] ...
            = params_SimradAll_WC_PlotNav_EI_SSc(this.repImport, 'askMaskOverlapingBeams', 0);
        if flag
            WC_EI_PingAcrossDist_SignalVsPings(ListeFicEI, DataName, TypeUnit, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag);
        end
        
    case 'ALL_WC_RazFlagPings_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            WC_RazFlagPings(listFileNames)
        end
        
    case 'ALL_WC_EI_SampleBeam_SignalVsPings_CI1' % Checked for ALL-XSF
        [flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, CLim, ALim, BLim, params_ZoneEI, ...
            Display, nomDirCSVOut, Tag, maskOverlapingBeams, thresholdValueRaw, thresholdValueComp, ...
            this.repImport] = WCD.Params.PlotNav_EI_SScSampleBeam(this.repImport, 'Ext', {'.all'; '.kmall'}, 'AllFilters', 1, 'MessageParallelTbx', 1);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_WC_EI_SampleBeam_SignalVsPings(aKM, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
                    CLim, ALim, BLim, params_ZoneEI, Display, nomDirCSVOut, Tag, maskOverlapingBeams, ...
                    thresholdValueRaw, thresholdValueComp, 'suby', suby);
            end
        end

    case 'ALL_WC_EI_SampleBeam_SignalSlicesVsPings_CI1'
        [flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ALim, BLim, params_ZoneEI, ...
            flagComp, nomDirCSVOut, Tag, maskOverlapingBeams, CLim, this.repImport] ...
            = WCD.Params.EI_SampleBeam_SignalSlicesVsPings(this.repImport, 'Ext', {'.all'; '.kmall'}, 'AllFilters', 1, 'MessageParallelTbx', 1);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_WC_EI_SampleBeam_SlicesSignalVsPings(aKM, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
                    ALim, BLim, params_ZoneEI, flagComp, CLim, nomDirCSVOut, Tag, maskOverlapingBeams, ...
                    'suby', suby);
            end
        end

    case 'ALL_WC_Mosaic_EI_SSc_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, listFileNames, Resol, Window, Backup, InfoMos, CorFile, ModeDependant, ...
            LimitAngles, listeMode, listeSwath, listeFM, this.repImport, this.repExport] ...
            = params_SimradAll_WC_Mosaic_EI_SSc(this.repImport, this.repExport);
        if flag
                % TODO : [flag, aKM] = ALL.checkFiles(listFileNames);
            [flag, a] = PingAcrossDist2LatLong(listFileNames, Resol, Backup, Window, InfoMos, ...
                'CorFile', CorFile, 'ModeDependant', ModeDependant, 'LimitAngles', LimitAngles, ...
                'listeMode', listeMode, 'listeSwath', listeSwath, 'listeFM', listeFM);
        end
        
    case 'ALL_WaterColumnUncompressFiles_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, listFileNames, DeleteFile, this.repImport] = ALL.Params.WaterColumnUncompressFiles(this.repImport);
        if flag
            process_ALL_WaterColumnUncompressFiles(listFileNames, DeleteFile);
        end
        
    case 'ALL_BackupMask_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_backupMask(aKM);
            end
        end
        
    case 'ALL_ResetMask_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_resetMask(aKM);
            end
        end
        
    case 'ALL_RestoreMask_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_restoreMask(aKM);
            end
        end
        
    case 'ALL_ResetMaskPingsReflec_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_resetPingsFlagsReflec(aKM);
            end
        end
        
    case 'ALL_ImportBathyFromCaris_CI0'
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicTxt, listFileNames] = params_ImportBathyFromCaris(listFileNames);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    sonar_importBathyFromCaris_ALL(aKM, nomFicTxt);
                end
            end
        end
        
    case 'ALL_ImportBathyFromQuimera_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                sonar_importBathyFromQuimera_ALL(aKM);
            end
        end

    case 'ALL_ImportFlagsFromCaraibes_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames, listeFicMBG] = params_sonarImportBathyFromCaraibesMBG(this, {'.all'; '.kmall'});
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                sonar_importBathyFromCaraibesMBG_ALL(aKM, listeFicMBG);
            end
        end
        
    case 'ALL_ImportBathyFromMBSystem_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                sonar_importBathyFromMbSystem_ALL(aKM);
            end
        end

    case 'ALL_ImportBathyFromXSF_CI0'
%         OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames, listeFicXSF] = params_sonarImportBathyFromCaraibesMBG(this, {'.all'; '.kmall'}, 'ExtMBG', '.nc');
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                sonar_importBathyFromXSF_ALL(aKM, listeFicXSF);
            end
        end
        
    case 'ALL_WaterColumnConvertSsc2Hac_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SimradAll_ssc2hac('NomFicAll', listFileNames, 'RepDefaut', lastDir);
        end

    case 'ALL_PingBeam2LatLong_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, ModeDependant, LayerName, Window, flagTideCorrection, SameReflectivityStatus, InfoCompensationCurve, ...
                InfoMos, this.repImport, this.repExport] = params_MBES_IndividualMosaics(this.Images(this.indImage), ...
                listFileNames, this.repImport, this.repExport, 'TypeMos', 1);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
                if flag
                    listFileNames = get_nomFic(aKM);
                % TODO : [flag, aKM] = ALL.checkFiles(listFileNames);
                    [flag, a] = SimradAllMosaiqueParProfil(this.Images, this.indImage, listFileNames, ...
                        ModeDependant, LayerName, Window, ...
                        SameReflectivityStatus, InfoCompensationCurve, InfoMos, ...
                        'TideCorrection', flagTideCorrection, 'IndNavUnit', IndNavUnit);
                end
            end
        end

    case 'ALL_CacheComputeLatLongLayers_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, createLatLong, createGeoYX] = ALL.Params.SimradAllLatLongLayersInCache;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = SimradAllComputeLatLongLayersInCache(aKM, createLatLong, createGeoYX);
                end
            end
        end
        
    case 'ALL_CacheDeleteLatLongLayers_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, createLatLong, createGeoYX] = ALL.Params.SimradAllLatLongLayersInCache;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = SimradAllDeleteLatLongLayersInCache(aKM, createLatLong, createGeoYX);
                end
            end
        end
        
%     case 'ALL_PingSamples2LatLong' % TODO : plus appel�
%         [flag, XLim, YLim] = params_PingBeam2LatLong(this);
%         if flag
%             [flag, a, this.repImport, this.repExport] = Simrad_PingSamples2LatLong(...
%                 'NomDirImport', this.repImport, ...
%                 'NomDirExport', this.repExport, ...
%                 'XLim', XLim, 'YLim', YLim);
%         end
        
    case 'ALL_PingAcrossSample2LatLong_CI1'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repImport, this.repExport] = Simrad_PingAcrossSample2LatLong(...
                'NomDirImport', this.repImport, ...
                'NomDirExport', this.repExport, ...
                'XLim', XLim, 'YLim', YLim);
        end
                
    case 'ALL_PingSamples2PingAcrossDist_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, a, this.repExport] = Simrad_PingSamples2PingAcrossDist('NomDir', this.repExport);
        
    case 'ALL_WC_EI_MosFromMovies_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this.repImport, this.repExport, listFileNames, Resol, window, iSlice, Backup, ...
            nomFicErMapperLayer, nomFicErMapperAngle] = WCD.Params.LaurentEchoIntegration(this.repImport, this.repExport);
        if flag
            [flag, a] = Laurent_PingBeam2LatLong(listFileNames, Resol, Backup, window, iSlice, nomFicErMapperLayer, nomFicErMapperAngle);
        end
        
%     case 'ALL_WC3DMatrix_CI2' % Checked for ALL
    case 'ALL_WC3DMatrix_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repImport, this.repExport, TypeWCOutput, Names, ...
                StepCompute, StepDisplay, CorrectionTVG, ...
                CLimRaw, CompReflec, ModeDependant, flagTideCorrection, typeAlgoWC, ...
                MaskBeyondDetection, skipAlreadyProcessedFiles, ...
                minAcrossDist, maxAcrossDist, nbPingsMax, TypeFileWCOutput] = ALL.Params.ComputeEchograms3DMatrix(listFileNames, this.repImport, this.repExport);
%                 minAcrossDist, maxAcrossDist, nbPingsMax, TypeFileWCOutput] = params_SimradAll_EchogramsCube3D(this.Images(this.indImage), listFileNames, this.repImport, this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL.Process.ComputeEchogramsXMLBin(aKM, TypeWCOutput, Names, ...
                        'suby', suby, 'StepCompute', StepCompute, 'StepDisplay', StepDisplay, ...
                        'CorrectionTVG', CorrectionTVG, 'CLimRaw', CLimRaw, ...
                        'CompReflec', CompReflec, 'ModeDependant', ModeDependant, ...
                        'TideCorrection', flagTideCorrection, 'typeAlgoWC', typeAlgoWC, ...
                        'MaskBeyondDetection', MaskBeyondDetection, 'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, ...
                        'minAcrossDist', minAcrossDist, 'maxAcrossDist', maxAcrossDist, 'nbPingsMax', nbPingsMax);
                    if TypeFileWCOutput == 2 % Netcdf
                        flag = WC_3DMatrix_XML2Netcdf(Names);
                        if flag
                            flag = WC_3DMatrix_RemoveXMLBin(Names);
                        end
                    end
                end
            end
        end
        
    case 'ALL_WC_ExportRawData23DV_CI1' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomDirOut, TypeDataWC, Tag, MaskBeyondDetection, CLimRaw, flagTideCorrection, typeAlgoWC, this.repExport] ...
                = ALL.Params.WC_ExportRawData23DV(this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_WC_ExportRawData23DV(aKM, nomDirOut, TypeDataWC, Tag, MaskBeyondDetection, ...
                        'CLimRaw', CLimRaw, 'suby', suby, 'TideCorrection', flagTideCorrection, 'typeAlgoWC', typeAlgoWC);
                end
            end
        end
        
    case 'ALL_WC_CreatePingBeamEI_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'All'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, typeAlgoWC, resol, Tag, nomDirOut, Display, this.repExport] = ALL.Params.WC_CreatePingBeamEI(this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_WC_CreatePingBeamEI(aKM, resol, nomDirOut, 'Display', Display, 'Tag', Tag, 'typeAlgoWC', typeAlgoWC);
                end
            end
        end
        
    case 'ALL_TrackBall_CI1'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, listFileNames, this.repImport] = params_WCMatrixTrackBall(this.Images(this.indImage), this.repImport);
        if flag
                % TODO : [flag, aKM] = ALL.checkFiles(listFileNames);
            [flag, a] = WC_MatrixTrackBall(this.Images(this.indImage), listFileNames, 'subx', subx, 'suby', suby);
        end
        
    case 'ALL_Mosaiques_Individuelles_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, ModeDependant, LayerName, Window, flagTideCorrection, SameReflectivityStatus, InfoCompensationCurve, ...
                InfoMos, this.repImport, this.repExport] = params_MBES_IndividualMosaics(this.Images(this.indImage), ...
                listFileNames, this.repImport, this.repExport, 'TypeMos', 2);
            if flag
                [flag, aKM, ~, ~, IndNavUnit] = ALL.checkFiles(listFileNames, 'flagIndNavUnit', 1);
                if flag
                    listFileNames = get_nomFic(aKM);
                % TODO : [flag, aKM] = ALL.checkFiles(listFileNames);
                    [flag, a] = SimradAllMosaiqueParProfil(this.Images, this.indImage, listFileNames, ...
                        ModeDependant, LayerName, Window, ...
                        SameReflectivityStatus, InfoCompensationCurve, InfoMos, ...
                        'TideCorrection', flagTideCorrection, 'IndNavUnit', IndNavUnit);
                end
            end
        end
        
    case 'ALL_Mosaiques_ParAngles_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, ListeFicErs, NomGenerique, AngleLim, this.repImport, this.repExport] ...
            = params_MosaicsPerAngularSectors(this.repImport, this.repExport);
        if flag
            SimradAllAssemblageParAngle(this.repExport, ListeFicErs, NomGenerique, AngleLim);
        end
        
    case 'ALL_Mosaiques_MergeIndividuelles_CI0' % Checked
        [flag, a, this.repExport] = ErMapper_mergeMosaics(this.repExport);

    case 'ALL_Statistics_Histo2D_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repExport] = my_uigetdir(this.repExport, Lang('Nom du r�pertoire o� stocker les fichiers :', 'Output directory name :'));
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    [flag, a] = ALL_Histo2DReflecAngles(aKM, this.repExport);
                end
            end
        end
        
    case 'ALL_Statistics_Histo2D_ExtractCurves_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this.Images(this.indImage)] = extractCurvesFromHisto2D(this.Images(this.indImage));
        
    case 'ALL_Statistics_Curves_CI1'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, this.repExport, bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                Coul, nbPMin, SelectMode, SelectSwath, SelectFM, ~, MeanCompType, indLayeMaskLatLong, ...
                valMask, SameReflectivityStatus, InfoCompensationCurve, nomDirMatFiles] = ...
                paramsStatisticsCurves(this.Images, this.indImage, this.repImport, this.repExport, ...
                listFileNames, nomZoneEtude, subx, suby, msg, 'AskQstComp', 1, ...
                'MeanCompType', ReflectivityMeanCompTypeDefaultValue, 'selectMeanCompType', 1);
            if flag
                % TODO : [flag, aKM] = ALL.checkFiles(listFileNames);
                [flag, this.Images] = SimradAll_Statistics_Curves(this.Images, this.indImage, listFileNames, ...
                    bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, MeanCompType, ...
                    this.Images(indLayeMaskLatLong), valMask, SameReflectivityStatus, InfoCompensationCurve, ...
                    'ExportMatFiles', nomDirMatFiles, 'Mute', -1);
            end
        end
        
    case 'ALL_Statistics_Calibration_BSComputeAndModel_CI1' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, this.repExport, bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                Coul, nbPMin, SelectMode, SelectSwath, SelectFM, ~, MeanCompType, indLayeMaskLatLong, ...
                valMask, SameReflectivityStatus, InfoCompensationCurve] = ...
                paramsStatisticsCurves(this.Images, this.indImage, this.repImport, this.repExport, ...
                listFileNames, nomZoneEtude, subx, suby, msg, ...
                'SonarStatus', 'BestBSConstructeur', 'MeanCompType', ReflectivityMeanCompTypeDefaultValue, ...
                'AskQstComp', 1, 'AskQstSameBSStatus', 0);
            if flag
                [flag, this.Images] = SimradAll_Statistics_CurvesBS(this.Images, this.indImage, listFileNames, ...
                    bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, MeanCompType, ...
                    this.Images(indLayeMaskLatLong), valMask, SameReflectivityStatus, InfoCompensationCurve);
            end
        end
        
    case 'ALL_Statistics_Calibration_BSExport2_CI0'
        this = callback_SP_BS_Calib_ModeResult(this, 'SP_BS_Calib_ModeResult_Export2_CI0'); % Modif JMA le 01/04/2021
        
    case 'ALL_Statistics_Calibration_BSRedoModel_CI0'
        this = callback_SP_BS_Calib_ModeResult(this, 'SP_BS_Calib_ModeResult_RedoModel_CI0', [], [], []);
        
    case  'ALL_Statistics_Calibration_DiagTxComputeAndModel_CI1'
%         OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            %         message_MBES_OneConfiguration
            [flag, this.repExport, bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType, indLayeMaskLatLong, valMask, ...
                SameReflectivityStatus] = ...
                paramsStatisticsCurves(this.Images, this.indImage, this.repImport, this.repExport, ...
                listFileNames, nomZoneEtude, subx, suby, msg, ...
                'SonarStatus', 'BestDiagTx', 'SignatureConfigurationSonar', 1, 'SkipTxBeamIndexBins', 1, ...
                'MeanCompType', ReflectivityMeanCompTypeDefaultValue, 'AskQstSameBSStatus', 0, ...
                'OnlyOneConfiguration', 1, 'askQuestionSignatureConfSonar', -1); % TODO
            if flag
                str1 = 'S�lectionner le fichier qui contient la courbe de BS de r�f�rence';
                str2 = 'Select the file that contains the reference BS curve.';
                [flag, nomFicBS] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', this.repImport, ...
                    'ChaineIncluse', 'BS', 'ChaineExclue', '_Residuals', ...
                    'SubTitle', Lang(str1,str2)); % ChaineIncluse
                if flag
                    [flag, this.Images(this.indImage)] = SimradAll_Statistics_CurvesTxDiag(this.Images(this.indImage), listFileNames, ...
                        bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                        Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType, ...
                        this.Images(indLayeMaskLatLong), valMask, SameReflectivityStatus, nomFicBS);
                end
            end
        end

    case 'ALL_Statistics_Calibration_DiagTxComputeAndModel_EM3002_CI1' % Checked for ALL
%         OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, this.repExport, bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType, indLayeMaskLatLong, valMask, ...
                SameReflectivityStatus] = ...
                paramsStatisticsCurves(this.Images, this.indImage, this.repImport, this.repExport, ...
                listFileNames, nomZoneEtude, subx, suby, msg, ...
                'SonarStatus', 'BestDiagTx', 'SignatureConfigurationSonar', 1, 'SkipTxBeamIndexBins', 1, ...
                'MeanCompType', ReflectivityMeanCompTypeDefaultValue, 'AskQstSameBSStatus', 0, ...
                'OnlyOneConfiguration', 1, 'askQuestionSignatureConfSonar', -1); % TODO
            if flag
                str1 = 'S�lectionner le fichier qui contient la courbe de BS de r�f�rence';
                str2 = 'Select the file that contains the reference BS curve.';
                [flag, nomFicBS] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', this.repImport, ...
                    'ChaineIncluse', 'BS', 'ChaineExclue', '_Residuals', ...
                    'SubTitle', Lang(str1,str2)); % ChaineIncluse
                if flag
                    [flag, this.Images(this.indImage)] = SimradAll_Statistics_CurvesTxDiag_EM3002(this.Images(this.indImage), listFileNames, ...
                        bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
                        Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType, ...
                        this.Images(indLayeMaskLatLong), valMask, SameReflectivityStatus, nomFicBS);
                end
            end
        end

        
    case  'ALL_Statistics_Calibration_DiagTxRedoModel_CI0'
        this = callback_SP_BS_Calib_ModeResult(this, 'SP_BS_Calib_ModeResult_DiagTxRedoModel_CI0', [], [], []); 
        
    case  'ALL_Statistics_Calibration_DiagTxExport2_CI0'
        this = callback_SP_BS_Calib_ModeResult(this, 'SP_BS_Calib_ModeResult_DiagTxExport2_CI0', [], [], []);        
       
    case 'ALL_RnD_BottomDetectionSynthese_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, DisplayLevel, nomFicAvi, DepthMin, DepthMax, nomDirErs, BeamsToDisplay, IdentAlgo, this.repExport] ...
                 = ALL.Params.RnD_BottomDetectionSynthese(listFileNames, this.repExport);
            if flag
                % TODO : [flag, aKM] = ALL.checkFiles(listFileNames);
                [flag, a] = sonar_SurveySimradDetectionPhaseAndAmplitude(listFileNames, DisplayLevel, nomFicAvi, ...
                    DepthMin, DepthMax, nomDirErs, BeamsToDisplay, IdentAlgo);
            end
        end
     
    case 'ALL_DatagramsSplit_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                this.repExport = ALL_splitFiles(aKM, this.NomFicPoints, this.repImport, this.repExport);
            end
        end
     
    case 'ALL_DatagramsSplitFrequencies_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'FileExt', '.kmall');
        if flag
            KmAll.splitFrequencies(listFileNames);
        end
     
    case 'ALL_DatagramsWCUnionAllWcd_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                this.repExport = ALL_unionFilesAllWcd(aKM, this.repExport);
            end
        end
        
    case 'ALL_AllMergeFiles_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                this.repExport = ALL_mergeFiles(aKM, this.repExport);
            end
        end
        
    case 'ALL_Robot_CopyAndProcessSounderOutputFiles_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx, this.repImport, this.repExport] ...
            = RobotUtils.InputParams.CopyAndProcessSounderOutputFiles('.all', this.repImport, this.repExport);
        if flag
            RobotUtils.All.launchProcess(paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx);
        end
        
    case 'ALL_Robot_MoveFiles_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'Subtitle2', 'List of files you want to move');
        if flag
            [flag, this.repImport, this.repExport, nomDirOut, DirNames] = RobotUtils.InputParams.MoveFiles(listFileNames, this.repExport);
            if flag
                RobotUtils.Common.moveFiles(listFileNames, nomDirOut, DirNames);
            end
        end
        
    case 'ALL_Robot_PlotKMZ_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'Subtitle2', 'List of files you want to move');
        if flag
            [flag, filtreData, classement, nbSeconds] = RobotUtils.InputParams.PlotKMZ;
            if flag
            	RobotUtils.Common.PlotKMZ(listFileNames, classement, filtreData, nbSeconds);
            end
        end

    case 'ALL_MoveFiles_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repExport] = ALL.Params.MoveFiles(this.repExport);
            if flag
                % Attention : ne pas utiliser [flag, aKM] = ALL.checkFiles(listFileNames) car la fonction 
                % ALL.Process.MoveFiles peut agir sur des fichiers .all sans que le cache ne soit cr��
                ALL.Process.MoveFiles(listFileNames, this.repExport)
            end
        end
        
    case 'ALL_ExportPLY_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, LayerNames, this.repExport] = ALL.Params.ExportPLY(this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_exportPLY(aKM, LayerNames, this.repExport)
                end
            end
        end
        
    case 'ALL_DatagramsWCSeparate_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                this.repExport = splitAllandWcdFiles(aKM, this.repExport);
            end
        end

    case 'ALL_DatagramsSuppress_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                this.repExport = suppressDatagramInAllFile(aKM, this.repExport);
            end
        end
        
    case 'ALL_PlotTide_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_TidePlot(aKM);
            end
        end
        
    case 'ALL_TideImport_CI0' % Checked for ALL-XSF
         [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicTide, this.repImport] = SSc.Params.ImportTide(this.repImport);
            if flag
                if flag
                    [flag, aKM] = ALL.checkFiles(listFileNames);
                    if flag
                        ALL_TideImport(aKM, nomFicTide, 'Tide');
                    end
                end
            end
        end
        
    case 'ALL_AUVImportImmersion_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicImmersion] = uiSelectFiles('ExtensionFiles', '.txt', 'RepDefaut', this.repImport, ...
                'SubTitle', 'Select ascii files that contain the AUV immersion');
            if flag
                if flag
                    [flag, aKM] = ALL.checkFiles(listFileNames);
                    if flag
                        ALL_ImmersionImport(aKM, nomFicImmersion, 'Tide');
                    end
                end
            end
        end
        
    case 'ALL_AUVImportOffsetImmersion_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicImmersion] = uiSelectFiles('ExtensionFiles', '.txt', 'RepDefaut', this.repImport, ...
                'SubT', 'Select ascii files that contain offsets of immersion');
            if flag
                if flag
                    [flag, aKM] = ALL.checkFiles(listFileNames);
                    if flag
                        ALL_OffsetImmersionImport(aKM, nomFicImmersion, 'Tide');
                    end
                end
            end
        end
        
    case 'ALL_DraughtImport_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicDraught, this.repImport] = SSc.Params.ImportTide(this.repImport);
            if flag
                if flag
                    [flag, aKM] = ALL.checkFiles(listFileNames);
                    if flag
                        ALL_TideImport(aKM, nomFicDraught, 'Draught');
                    end
                end
            end
        end
        
    case 'ALL_TrueHeaveImport_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicDraught, this.repImport] = SSc.Params.ImportTide(this.repImport);
            if flag
                if flag
                    [flag, aKM] = ALL.checkFiles(listFileNames);
                    if flag
                        ALL_TideImport(aKM, nomFicDraught, 'TrueHeave');
                    end
                end
            end
        end
        
    case 'ALL_FilterTransducerDepth_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_FilterTransducerDepth(aKM);
            end
        end
        
    case 'ALL_RidhaWaveFilter_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, Periode] = ALL.Params.RidhaWaveFilter;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_RidhaWaveFilter(aKM, Periode);
                end
            end
        end
        
    case 'ALL_RTK_Raz_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_RTK_Raz(aKM);
            end
        end
        
    case 'ALL_RTK_Plot_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_RTK_Plot(aKM);
            end
        end
        
    case 'ALL_RTK_Clean_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_RTK_Clean(aKM);
            end
        end
        
    case 'ALL_RTK_plotGeoid_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, listGeoidFileNames, this.repImport] = uiSelectFiles('ExtensionFiles', '.geoid', 'RepDefaut', this.repImport, ...
            'SubTitle', 'Select GEOID file');
        if flag
            RTK_PlotGeoid(listGeoidFileNames);
        end 
        
    case 'ALL_RTK_ComputeTide_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_RTK_ComputeTide(aKM);
            end
        end    
        
    case 'ALL_RTK_ExportTide_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nonFicTideIn, nomFicTideOut, this.repImport] = params_RTK_CreateTide(this.repImport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_RTK_ExportTide(aKM, nonFicTideIn, nomFicTideOut);
                end
            end
        end
        
    case 'ALL_BackupSignals_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignal] = ALL.Params.BackupSignals(listFileNames);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    BackupRestoreSignals(aKM, NomSignal, 1);
                end
            end
        end
        
    case 'ALL_RestoreSignals_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignal] = ALL.Params.BackupSignals(listFileNames);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    BackupRestoreSignals(aKM, NomSignal, 2);
                end
            end
        end
                
    case 'ALL_PatchTestRoll_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, listFileNames, resol, this.repImport] = params_PatchTestRoll('.all', this.repImport);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_PatchTestRoll(aKM, resol);
            end
        end
                
    case 'ALL_AbsorpCoeffODAS_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicODAS, this.repImport] = params_AbsorpCoeff_ODAS(this.repImport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_AbsorpCoeff_ODAS(aKM, nomFicODAS);
                end
            end
        end
        
    case 'ALL_AbsorpCoeffSSP_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_AbsorpCoeff_SSP_Multi(aKM);
            end
        end
        
    case 'ALL_AbsorpCoeffSIS_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicABS, this.repImport] = params_AbsorpCoeff_SIS(this.repImport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_AbsorpCoeff_SIS(aKM, nomFicABS);
                end
            end
        end
        
    case 'ALL_AbsorpCoeff_Manual_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, alphaUser] = params_AbsorpCoeff_Manual(this.Images(this.indImage));
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_AbsorpCoeff_Manual(aKM, alphaUser);
                end
            end
        end
             
    case 'ALL_IncidenceAngleFromOneDTM_CI0' % Checked for ALL-XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, versionAlgo, TypeAlgoNormales] = params_IncidenceAngleFromOneDTM(this.Images(this.indImage));
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_IncidenceAngleFromOneDTM(aKM, this.Images(this.indImage), ...
                        'versionAlgo', versionAlgo, 'TypeAlgoNormales', TypeAlgoNormales);
                end
            end
        end
        
    case 'ALL_BestBSCompute_CI0' % Checked for XSF
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, this.repExport, InfoCompensationCurve] = ALL.Params.BestBSCompute(this.repImport, this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_BestBSCompute(aKM, InfoCompensationCurve);
                end
            end
        end
             
    case 'ALL_SegmentationSamantha_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicCurves, SameReflectivityStatus, InfoCompensationCurve, gridSize, nomDirOut, w, ...
                StopRegul, UseModelBS, this.repImport, this.repExport] = params_SegmentationSamantha(this.Images(this.indImage), this.repImport, this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_SegmentationSamantha(this.Images, this.indImage, get_nomFic(aKM), nomFicCurves, SameReflectivityStatus, InfoCompensationCurve, ...
                        gridSize, nomDirOut, w, StopRegul, UseModelBS);
                end
            end
        end
        
    case 'ALL_SPFEBatch0_CI0' %  Checked for ALL-XSF
        [flag, surveyDirNames, skipSurveys, this.repImport] = ALL.Params.SPFE_Batch0(this.repImport);
        if flag
            flag = ALL.Process.SPFE_Batch0(surveyDirNames, skipSurveys);
        end
        
    case 'ALL_SPFEBatch1_CI0' %  Checked for ALL-XSF
        [flag, surveyDirNames, ALim, gridSize, skipSurveys, this.repImport] = ALL.Params.SPFE_BatchBSAngularSector(this.repImport);
        if flag
            flag = ALL.Process.SPFE_BatchBSAngularSector(surveyDirNames, ALim, gridSize, skipSurveys, false);
        end
        
    case 'ALL_SPFEBatch2_CI0' %  Checked for ALL-XSF
        [flag, surveyDirNames, ALim, gridSize, skipSurveys, this.repImport] = ALL.Params.SPFE_BatchBSAngularSector(this.repImport);
        if flag
            flag = ALL.Process.SPFE_BatchBSAngularSector(surveyDirNames, ALim, gridSize, skipSurveys, true);
        end
        
    case 'ALL_SPFEBatch3_CI0' %  Checked for ALL-XSF
        [flag, surveyDirNames, this.repImport] = ALL.Params.SPFE_Batch3(this.repImport);
        if flag
            flag = ALL.Process.SPFE_Batch3(surveyDirNames);
        end
        
    case 'ALL_SPFEBatch4_CI0' % Checked for ALL
        OperationMaintenance(typeFormatFichier, {'XSF'})
        [flag, surveyDirNames, CsvOutputFile, this.repImport] = ALL.Params.SPFE_BatchReport(this.repImport);
        if flag
            flag = ALL.Process.SPFE_BatchReport(surveyDirNames, CsvOutputFile);
        end
        
    case 'ALL_SPFEBatch5_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicPoints, nomDirOut, pourcentageDepth, this.repImport, this.repExport] = ALL.Params.SPFE_CreateMasksForMarcRoche(this.repImport, this.repExport);
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_CreateMasksForMarcRoche(aKM, nomFicPoints, nomDirOut, pourcentageDepth);
                end
            end
        end
        




    case 'ALL_ExportAttitude_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
       [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, typeFormat] = ALL.Params.ExportAttitude;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_ExportAttitude(aKM, typeFormat);
                end
            end
        end
        
    case 'ALL_ExportPingNumberAndUnixTime_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, typeFormat] = ALL.Params.ExportAttitude;
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    ALL_ExportPingCounterAndUnixTime(aKM, typeFormat);
                end
            end
        end

    case 'ALL_ReduceCsvFile_CI0'
        [flag, listFileNames, this.repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.csv', 'RepDefaut', this.repImport, ...
            'SubTitle', 'Select the .csv files');
        if flag
            [flag, nomDirOut, timeStep] = ALL.Params.ReduceCsvFile(fileparts(listFileNames{1}));
            if flag
                flag = subsampleCsvFile(listFileNames, nomDirOut, timeStep);
            end
        end

    case 'ALL_Statistics_BathyMNT_CI0' % Checked for XSF
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, CleanCurves] = params_ALL_StatsBathyMinusDTM(this.Images(this.indImage));
            if flag
                [flag, aKM] = ALL.checkFiles(listFileNames);
                if flag
                    flag = ALL_StatsBathyMinusDTM(aKM, this.Images(this.indImage), CleanCurves);
                end
            end
        end
        
    case 'ALL_CacheXML2Netcdf_CI0' % 'Convert XML-Bin to Netcdf'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            ALL_Cache2Netcdf(listFileNames);
        end
        
    case 'ALL_CacheDelXML_CI0' % 'Delete XML-Bin'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SScCacheXMLBinUtils.deleteCachDirectory(listFileNames);
        end
        
    case 'ALL_CacheXML2NetcdfAndDelXML_CI0' % 'Convert XML-Bin to Netcdf & delete XML-Bin')
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [ind, flag] = my_questdlg('Redo Netcdf files if already exist ?', 'Init', 2);
            if flag
                ALL_Cache2Netcdf(listFileNames, 'Redo', ind == 1, 'deleteXMLBin', true);
            end
        end
        
    case 'ALL_DatagramsRepairAllComingFromKmall_CI0'
        OperationMaintenance(typeFormatFichier, {'All'; 'XSF'})
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, aKM] = ALL.checkFiles(listFileNames);
            if flag
                ALL_RepairAllComingFromKmall(aKM);
            end
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end


%% OperationMaintenance
function OperationMaintenance(typeFormatFichier, resteAVerifier)

% Cette fonction place une breakpoint au d�but d'une callback afin que l'on
% puisse mettre un  "% Checked for ..."
% Au d�but de callback_ALL, on choisit les types de fichier pour lesquels
% on veut activer le breakpoint, par exemple {'All'; 'XSF'} si on veut
% faire la v�rification pour des fichiers .all ou .kmall (XSF)
%
% Lorsque cette v�rification est modifie l'appel � cette fonction ou on
% supprime son appel s'il n'y a plus de format � v�rifier

if isempty(typeFormatFichier)
    return
end

if isempty(intersect(typeFormatFichier, resteAVerifier))
    return
end
my_breakpoint('Message', 'V�rifier si cela fonctionne bien pour les donn�es ALL-XSF et ajouter " % Checked for XSF" apr�s le case msg');




%% getListFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.all / .kmall');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', {'.all'; '.kmall'}, 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['Kongsberg ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
