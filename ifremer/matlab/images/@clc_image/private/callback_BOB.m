function this = callback_BOB(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

S0 = cl_Bob([]);
a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'BOB_Export3DViewer'
        [flag, listeFicBob, this.repImport] = survey_uiSelectFiles(this, 'InitialFileFormat', 'BOB', ...
            'ExtensionFiles', {'.bob'; '.xml'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            [flag, nomDirOut, nomFicOut, ColormapIndex, Video, Compress, this.repExport] ...
                = params_BOB_Export3DViewer(this.Images(this.indImage), listeFicBob, this.repExport);
            if flag
                flag = BOB_Export3DViewer(S0, listeFicBob, nomDirOut, nomFicOut,ColormapIndex,Video,Compress);
            end
        end
        
    case 'BOB_ComputeEIFromRawFiles'
        % Appel de la chaine compl�te de traitement.
        [flag, nomFicBob] = BOB_fcnComputeEIFromRAWFiles;
        if ~flag
            return
        end
        % Poursuite syst�matique vers GLOBE.
        str1 = 'Voulez-vous terminer par l''export vers GLOBE?';
        str2 = 'Do you want to export data in GLOBE ?';
        [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 1, ...
            Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
        if flag && (rep == 1)
            [flag, nomDirOut, nomFicOut, ColormapIndex, Video, Compress, this.repExport] ...
                = params_BOB_Export3DViewer(this.Images(this.indImage), {nomFicBob}, this.repExport);
            if flag
                flag = BOB_Export3DViewer(S0, {nomFicBob}, nomDirOut, nomFicOut, ColormapIndex, Video, Compress);
            else
                str1 = 'Probl�me d''export dans GLOBE. SVP, contacter JMA.';
                str2 = 'Problem exporting data in GLOBE. Please contact JMA.';
                my_warndlg(Lang(str1,str2), 1);
            end
        end
        
    case 'BOB_AnalyseDataProcessing'
        % On affiche l'analyse comparative CCU et fichiers RAW.
        flag = BOB_fcnAnalyseCCULogandRAWFiles;
        
    case 'BOB_DispatchRawFiles'
        % Chargement pr�alable d'une analyse comparative de fichiers CCU et RAW.
        [flag, newTabDataCorrect] = BOB_uiDataAnalyser([], 'Entete', 'Synthesis for Survey', 'Resizable', 'on');
        if flag
            [nbSectors, flag] = my_inputdlg(Lang('Saisissez le nombre de secteurs � traiter', 'Please, input the number of sectors'), {0});
            if flag
                flag = ~isempty(nbSectors{:});
                if flag
                    nbSectors = nbSectors{:};
                    % R�partition des fichiers RAW par secteur.
                    [flag, sCCUandCompassExtract, subCCUData] = BOB_uiSelectCCUandCompass('nomFicCCULog', [], ...
                        'Resizable', 'on', ...
                        'NbSectors', nbSectors, ...
                        'DateBegin', [], ...
                        'DateEnd',   []); %#ok<ASGLU>
                    if flag
                        [flag, dirOut] = BOB_fcnDispatchRawFiles(nbSectors, newTabDataCorrect, sCCUandCompassExtract); %#ok<ASGLU>
                    end
                end
            end
        end
        
    case 'BOB_DuplicateConfigXML'
        % Appel de la chaine compl�te de traitement.
        flag = BOB_fcnDuplicateConfig;
        
    case 'BOB_ConvertRAW2HacFiles'
        
        % Appel de la chaine compl�te de traitement.
        flag  = BOB_fcnMultiConvertRaw2Hac;
        
        
    case 'BOB_ComputeEIFromHacFiles'
        [flag, structConfigSurvey] = BOB_fcnInputSurveyParameters('InputPosition', 0);
        if flag
            [rep, flag] = my_questdlg(Lang(   'Avez-vous pens� � mettre � jour la configuration M3D dans les r�pertoires Config ?', ...
                'Have you thought to release M3D configuration XML files ?'), ...
                'Init', 1, Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
            if flag && (rep == 1)
                % Appel de la chaine compl�te de traitement.
                flag = BOB_fcnComputeEIByMovies(structConfigSurvey);
            end
        end
        
    case 'BOB_CreationBOBFilesFor3DV'
        % R�partition des fichiers RAW par secteur.
        [flag, nomFicMatCCU, nomFicBOB] = BOB_fcnCreationBOBFilesFor3DV; %#ok<ASGLU>
        if ~flag
            return
        end
        
    case 'BOB_DisplaySummaryConfig'
        % On affiche sous forme tabul�e et graphique le r�sum� de la configuration nominale.
        [flag, valueCompass, sConfigBOB] = BOB_uiSelectConfig('fileNameConfig', []); %#ok<,ASGLU>
        if ~flag
            return
        end
        
    case 'BOB_DisplayAnalyseDataProcessing'
        % On affiche l'analyse comparative CCU et fichiers RAW.
        [flag, ~] = BOB_uiDataAnalyser([], 'Entete', 'Synthesis for Survey', 'Resizable', 'on');
        
    case 'BOB_DisplayGraphsCCU'
        % On affiche sous forme graphique le r�sum� des angles de CCU
        [flag, sCCUandCompassExtract, subCCUData, subPAROSData] = BOB_uiSelectCCUandCompass( 'nomFicCCULog', [], ...
            'NomDirRawData', [], ...
            'Resizable', 'on', ...
            'NbSectors', [], ...
            'DateBegin', [], ...
            'DateEnd', []); %#ok<ASGLU>
        if ~flag
            return
        end
        
    case 'BOB_ReadOnce'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            BOB_ReadOnce(S0, listeFic);
        end
        
    case 'BOB_ZipDir'
        [flag, nomFicBob, this.repImport] = uiSelectFiles('ExtensionFiles', {'.bob', '.xml'}, 'RepDefaut', this.repImport);
        if flag
            zipSonarScopeCache(nomFicBob);
        end
        
    case 'BOB_DeleteCache'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.bob', 'RepDefaut', this.repImport);
        if flag
            eraseSonarScopeCache(nomFic);
        end
        
    case 'BOB_DeleteCacheOfSurveyProcessing'
        flag  = BOB_fcnEraseDirCacheSSC('NbSectors', [], 'NomDirRawData', []);
        
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
