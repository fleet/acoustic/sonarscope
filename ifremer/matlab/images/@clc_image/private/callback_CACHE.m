function this = callback_CACHE(this, msg)

%% Quelques initialisations

flag = 1;
a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg 
    case 'CACHE_CreateXMLBinOnly' % 'Create XML-Bin only'
        ALL_CacheUseNetcdf(1) % selectLang(this, 'FR')
        maj_menu_CacheDirectory(this)
        
    case 'CACHE_CreateNetcdfAndKeepXMLBin' % 'Create Netcdf and keep XML-Bin'
        ALL_CacheUseNetcdf(2)
        maj_menu_CacheDirectory(this)
        
    case 'CACHE_CreateNetcdfAndDelXMLBin' % 'Create Netcdf and delete XML-Bin'
        ALL_CacheUseNetcdf(3)
        maj_menu_CacheDirectory(this)
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
