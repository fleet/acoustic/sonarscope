function this = callback_CAR(this, msg, subx, suby) %#ok<INUSD>

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

I0 = cl_image_I0;

%% Lancement des traitements
flag = 0;
a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'CAR_PlotNvi_CI0'
        [flag, ListeFicNvi, lastDir] = uiSelectFiles('ExtensionFiles', {'.nvi';'.txt'}, 'AllFilters', 1, ...
            'RepDefaut', this.repImport);
        if flag
            this.repImport = lastDir;
%             plot_nvi(I0, ListeFicNvi);
            plot_navigation_NVI(ListeFicNvi);
        end
        
    case 'CAR_PlotNviSignal_CI0'
        [flag, ListeFicNvi, nomSignal, this.repImport] = params_CAR_PlotNviSignal(this.repImport);
        if flag
            plot_nvi(I0, ListeFicNvi, 'Signal', nomSignal);
        end
        
    case 'CAR_SummaryNvi_CI0'
        [flag, listeFic, this.repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.nvi', 'RepDefaut', this.repImport);
        if flag
            [flag, nomFicSummary, this.repExport] = SSc.Params.getSummaryFilename(this.repExport, '.nvi');
            if flag
                NVI_summary(listeFic, nomFicSummary);
            end
        end
        
    case 'CAR_EditNvi_CI0'
        [flag, ListeFicNvi, lastDir] = uiSelectFiles('ExtensionFiles', {'.nvi'; '.csv'; '.txt'}, ...
            'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            this.repImport = lastDir;
%             plot_nvi(I0, ListeFicNvi);
            edit_navigation_NVI(ListeFicNvi);
        end

    case 'CAR_MBG_PlotNav_CI0'
        [flag, listFileNames, this.repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.mbg', ...
            'RepDefaut', this.repImport, 'Entete', 'IFREMER - SonarScope - Select Caraibes MBG (.mbg)');
        if flag
            plot_navigation_MBG(listFileNames);
        end

    case 'CAR_PingBeam2LatLong_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repImport, this.repExport] = Caraibes_PingBeam2LatLong(...
                'NomDirImport', this.repImport, ...
                'NomDirExport', this.repExport, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'CAR_SMbbSplit_CI0'
        splitMbgFile(this.Images(this.indImage), this.NomFicPoints, this.repImport, this.repExport);

    case 'CAR_SonarLateralImportMbb_CI0'
        [flag, NomFicMbb, NomFicNav, SonarIdent, listeFic, this.repImport] = params_CAR_ImportMbb(this.repImport);
        if flag
            flag = process_CAR_ImportMbb(listeFic, NomFicMbb, NomFicNav, SonarIdent); 
        end
        
    case 'CAR_ProjetArtistiqueUnivLaRochelle_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.mnt', 'RepDefaut', this.repImport);
        if flag
            [flag, a] = ProjetArtistiqueUnivLaRochelle(listeFic);
        end

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
