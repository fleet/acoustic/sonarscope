function this = callback_CINNA(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'CINNA_PlotNavigation'
%         [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.NA', 'RepDefaut', this.repImport);
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.NA'; '.VLIZ'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            plot_nav_CINNA(nomFic);
        end
        
    case 'CINNA_PlotSignalOnNavigation'
        [flag, nomFic, this.repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.NA', 'RepDefaut', this.repImport);
        if flag
            [flag, SignalName] = params_CINNA_PlotSignalOnNavigation();
            if flag
                plot_nav_CINNA(nomFic, 'Signal', SignalName);
            end
        end

    case 'CINNA_RTK_Plot'
%         [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.NA', 'RepDefaut', this.repImport);
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.NA'; '.VLIZ'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            CINNA_RTK_Plot(nomFic);
        end        

    case 'CINNA_RTK_Clean'
%         [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.NA', 'RepDefaut', this.repImport);
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.NA'; '.VLIZ'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            CINNA_RTK_Clean(nomFic);
        end 
        
    case 'CINNA_RTK_CreateTide'
        [flag, nomFic, nomFicTide, this.repImport, this.repExport] = params_CINNA_RTK_CreateTide(this.repImport, this.repExport);
        if flag
            CINNA_RTK_CreateTide(nomFic, nomFicTide);
        end    
      
    case 'CINNA_RTK_Raz'
%         [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.NA', 'RepDefaut', this.repImport);
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.NA'; '.VLIZ'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            CINNA_RTK_Raz(nomFic);
        end        
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
end
