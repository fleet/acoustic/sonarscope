function this = callback_CleanData(this, varargin)

[varargin, flagAxis] = getPropertyValue(varargin, 'flagAxis', [1 1 1]); %#ok<ASGLU>

%% Détermination si le curseur est sur l'un des 3 axes

identAxe = test_curseur_sur_axes(this);
if ~any(identAxe & flagAxis)
    return
end

%[varargin, KeyPress] = getFlag(varargin, 'KeyPress');
% if KeyPress
%     CurrentCharacter = get(gcf, 'CurrentCharacter');
%     if isempty(CurrentCharacter) || double(CurrentCharacter) ~=  32
%         return
%     end
%
%     if ~this.EreasePixelAction
%         return
%     end
% end

%%

SelectionType = get(this.hfig, 'SelectionType');
switch SelectionType
    case 'alt' % Ctrl click ou Clck droit
        msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
        this = traiter_click_new(this);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
    otherwise
        this = traiter_click_CleanData(this, identAxe);
end
