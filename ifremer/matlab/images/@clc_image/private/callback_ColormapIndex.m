% Traitement suite a une action sur le bouton de selection de la table de
% couleur
% 
% Syntax
%   this = callback_ColormapIndex(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_ColormapIndex(this, varargin)

if nargin == 1
    MessageTraitementDroite
    return
end

msg = varargin{1};
Num = str2double(msg);
if isnan(Num)
    WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
    switch msg
        case 'ContrasteChangeNumber'
            map  = get(gcf, 'Colormap');
            NMap = size(map,1);
            
            [flag, N] = inputOneParametre('Number of colors in the new colormap', 'Value', ...
                'Value', NMap, 'Unit', 'nb', 'MinValue', 2, 'MaxValue', 256, 'Format', '%d');
            if ~flag
                return
            end
            
            sub = floor(linspace(1, NMap, N));
            map = map(sub,:);
            this.Images(this.indImage).ColormapIndex  = 1;
            this.Images(this.indImage).ColormapCustom = map;
            this = set_colormap(this, map);
            str1 = 'Le r�sultat de cette op�ration est une table de couleur plac�e dans la table de couleur personnelle';
            str2 = 'This processing creates a colormap put in the "User" entry.';
            my_warndlg(Lang(str1,str2), 1)
            indiquerColorTableUtilisee(this, 1)
            return
            
        case 'ContrasteEditionTable'
            if this.Images(this.indImage).ImageType == 2
                str1 = 'Cette image est en RGB, vous ne pouvez pas modifier ses couleurs';
                str2 = 'This image is a true color one (RGB), it is impossible to change its colors.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            my_colormapeditor
            str1 = 'Lorsque vous aurez modifi� la table de couleur avec l''outil "Colormap Editor" il vous faudra la sauver dans "User" � l''aide du menu "R�cup�ration de la table de couleur modifi�e."';
            str2 = 'When you modify the colormap with the colormap editor you have to save the colormap in the "User" colormap with the menu "Get modified colormap"';
            my_warndlg(Lang(str1,str2), 1);
            return

        case 'ContrasteGetModifiedColormap'
            map  = get(gcf, 'Colormap');
            this.Images(this.indImage).ColormapIndex  = 1;
            this.Images(this.indImage).ColormapCustom = map;

            this = set_colormap(this, map);
            str1 = 'Le r�sultat de cette op�ration est une table de couleur plac�e dans la table de couleur personnelle';
            str2 = 'This processing creates a colormap put in the "User" entry.';
            my_warndlg(Lang(str1,str2), 1)
            indiquerColorTableUtilisee(this, 1)
            return

        case 'ContrasteSaveColormap' % Sauvegarde de la Colormap
            map  = this.Images(this.indImage).Colormap;
            CLim = this.Images(this.indImage).CLim;
            
            % Deux types d'export : GMT ou Caraibes (*cpt ou *tbl)
            str = Lang('Quel format de fichier pour l''export de la palette de couleur ? :', 'Which file format for colormap export ? :');
            [rep, flag] = my_questdlg(str, 'GMT (*.cpt)', 'Caraibes (*.tbl)');
            if ~flag
                return
            end
            if rep == 1
                ext = 'cpt';
            else
                ext = 'tbl';
            end
            
            nomFic = fullfile(this.repExport, ['Example.', ext]);
            [flag, nomFic] = my_uiputfile( ['*.',ext], 'Give a file name', nomFic);
            if ~flag
                return
            end
            this.repExport = fileparts(nomFic);
                        
            switch ext
                case 'cpt'
                    flag = export_cpt(map, nomFic, 'CLim', CLim);
                case 'tbl'
                    flag = export_tbl(map, nomFic, 'CLim', CLim);
            end
            return
                    
        case 'ContrasteLoadColormap'
            nomFic = getNomFicDatabase('GMT_haxby.cpt');
            nomDir = fileparts(nomFic);
            str1 = ['Vous trouverez des fichiers .cpts dans le repertoire "' nomDir '" et aussi sur http://pdfb.wiredworkplace.net/cpt-city/www/stats/hist-cpt.html ou http://soliton.vm.bytemark.co.uk/pub/cpt-city/views/totp-cpt.html'];
            str2 = ['You will find some .cpt files in "' nomDir '" directory and  others on http://pdfb.wiredworkplace.net/cpt-city/www/stats/hist-cpt.html or http://soliton.vm.bytemark.co.uk/pub/cpt-city/views/totp-cpt.html'];
            disp(Lang(str1,str2));
            [flag, nomFic] = uiSelectFiles('ExtensionFiles', {'.cpt'}, ...
                'RepDefaut', this.repImport);
            if ~flag
                return
            end
            
            if length(nomFic) > 1
                str1 = 'Une seule table de couleur SVP.';
                str2 = 'Only one file at a time please.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            ClimOrig = num2str(this.Images(this.indImage).CLim);
%             [flag, map, CLim] = import_cpt(nomFic{1}, 'NColors', 256, 'CLim', ClimOrig);
            [flag, map, CLim] = import_cpt(nomFic{1}, 'CLim', ClimOrig);
            % Conservation des limites de la colormap           
            
            if ~flag
                return
            end
            
            this.Images(this.indImage).CLim           = CLim;
            this.Images(this.indImage).ColormapIndex  = 1;
            this.Images(this.indImage).ColormapCustom = map;

            this = set_colormap(this, map);

            str1 = 'Le r�sultat de cette op�ration est une table de couleur plac�e dans la table de couleur personnelle';
            str2 = 'This processing creates a colormap put in the "User" entry.';
            my_warndlg(Lang(str1,str2), 1)
            indiquerColorTableUtilisee(this, 1)
            
            this.repImport = fileparts(nomFic{1});
            return
            
        case 'ContrasteShowColormaps'
            nomFic = getNomFicDatabase('GMT_haxby.cpt');
            NomDir = fileparts(nomFic);
            ShowColormaps(this.Images(this.indImage), 'DirCpt', NomDir);
            return

        otherwise
            ColormapIndex = [];
    end
else
    indiquerColorTableUtilisee(this, Num)
    ColormapIndex = Num;
end

if ~isempty(ColormapIndex)

    %% R�cup�ration de l'�tat du bouton Vid�o

    lastNuColormap = this.Images(this.indImage).ColormapIndex;
    if ColormapIndex == lastNuColormap
        return
    end

    %% Video inverse

    Video = get_etat(this.ihm.Video.cpn);

    %% Transmission des �tats dans l'instance cl_image

    this.Images(this.indImage).ColormapIndex = ColormapIndex;
    this.Images(this.indImage).Video         = Video + 1;
    
    TagSynchroContrast = this.Images(this.indImage).TagSynchroContrast;
    for i=1:length(this.Images)
        if strcmp(TagSynchroContrast, this.Images(i).TagSynchroContrast)
            this.Images(i).ColormapIndex = ColormapIndex;
        end
    end
    
    map  = this.Images(this.indImage).Colormap;
    this = set_colormap(this, map);
end
