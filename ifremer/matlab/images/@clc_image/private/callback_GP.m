% Callbacks de General processings
%
% Syntax
%   a = callback_GP(a)
%
% Input Arguments
%   a   : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_GP(this, varargin)

if nargin == 1
    MessageTraitementDroite
    return
end

MajColorbar = 1;
I0 = cl_image_I0;

% this = desactiver_frames(this)

%% D�chargement automatique d'images en m�moire virtuelle si besoin

this.Images = optimiseMemory(this.Images, 'SizeMax', 500000);

%% Ecriture dans le fichier Log

msg = varargin{1};

%% Recherche des pixels � traiter

[varargin, status] = getFlag(varargin, 'CurrentExtent');
if status
    [flag, subx, suby, ~, XLim, YLim, flagImageEntiere] = getSubxSubyContextuel(this, 'CurrentExtent');
else
    [flag, subx, suby, ~, XLim, YLim, flagImageEntiere] = getSubxSubyContextuel(this, msg);
end
if ~flag
    return
end

%% Lancement des traitements

flag = 1;

a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'GP_Shift_Compute'
        [flag, indImages, Method] = params_Recalage(this.Images, this.indImage);
        if flag
            XLim = this.Images(this.indImage).XLim;
            YLim = this.Images(this.indImage).YLim;
            a = recalage(this.Images(this.indImage), this.Images(indImages), 'Method', Method, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'GP_Shift_Offsets'
        [flag, this, OffsetX, OffsetY] = saisie_Offsets(this);
        if flag
            a = offset_axis(this.Images(this.indImage), OffsetX, OffsetY, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Shift_Manual_CI1'
        [flag, this, XCroix, YCroix] = params_offsetInteractif(this);
        if flag
            a = offset_axisInteractif(this.Images(this.indImage), XCroix, YCroix, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Shift_Longitude_CI1'
        [flag, CentralLongitude] = params_CentrageLongitude(this.Images(this.indImage));
        if flag
            a = centrageLongitude(this.Images(this.indImage), CentralLongitude, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Geometry_VerticalFlip_CI1'
        a = flipud(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Geometry_HorizontalFlip_CI1'
        a = fliplr(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Geometry_RotXY_CI1'
        [flag, Azimuth] = saisie_Azimuth(this.Images(this.indImage), 'Titre', 'Direction (deg)');
        if flag
            a = xy_rotation(this.Images(this.indImage), Azimuth, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'Autocorrelation' % TODO : supprimer cette fonction car elle n'est plus appel�e
        a = autocorrelation(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_SelfCorrelation_ByFFT_CI1'
        a = autocorrelationByFFT(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'Intercorrelation' % TODO : supprimer cette fonction car elle n'est plus appel�e
        [flag, ident, isSynchronized] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Image � comparer ?', 'Image to compare ?'));
        if flag
        	[flag, A, B] = extractSameFrame(this.Images, this.indImage, ident, subx, suby, isSynchronized);
            if flag
                a = crossCorrelation(A, B);
            end
        end
        
    case 'GP_CrossCorrelation_ByFFT_CI1'
        [flag, ident, isSynchronized] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Image � comparer ?', 'Image to compare ?'));
        if flag
        	[flag, A, B] = extractSameFrame(this.Images, this.indImage, ident, subx, suby, isSynchronized);
            if flag
                a = crossCorrelationByFFT(A, B);
            end
        end
        
    case 'GP_CrossCorrelation_ShapeRecognationGeometricFrame_CI1'
        [flag, typeAlgo, FrameDescription] = params_ShapeRecognationGeometricFrame(this.Images, this.indImage);
        if flag
            [flag, a] = shapeRecognationGeometricFrame(this.Images(this.indImage), typeAlgo, FrameDescription, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_CrossCorrelation_ShapeRecognationImageSnippet_CI1'
        [flag, typeAlgo, indLayerShape] = params_shapeRecognationImageSnippet(this.Images, this.indImage);
        if flag
            [flag, a] = shapeRecognationImageSnippet(this.Images(this.indImage), this.Images(indLayerShape), typeAlgo, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_CrossCorrelation_GeometricFrameFromImageSnippet_CI1'
        [flag, typeShape] = params_GeometricFrameFromImageSnippet(I0);
        if flag
            shapeRecognationGeometricFrameFromImageSnippet(this.Images(this.indImage), typeShape);
        end
        
    case 'GP_Arithmetic_Abs_CI1'
        a = abs(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Sqrt_CI1'
        a = sqrt(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Log10_CI1'
        a = log10(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_10pow_CI1'
        a = mpower(10, this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_SetNaN_CI1'
        a = PutNaN(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_Cos'
        a = cosd(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_AcosRd'
        a = acos(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_AcosDeg'
        a = acosd(this.Images(this.indImage), 'Unity', 'Deg', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_Sin'
        a = sind(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_AsinRd'
        a = asin(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_AsinDeg'
        a = asind(this.Images(this.indImage), 'Unity', 'Deg', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_Tan'
        a = tand(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_AtanRd'
        a = atan(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Trigo_AtanDeg'
        a = atand(this.Images(this.indImage), 'Unity', 'Deg', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Arithmetic_Mean_CI1'
        [flag, listeImages, XLim, YLim, ImageName, ComputationUnit] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'Mean');
        if flag
            [flag, a] = MultiImages_Mean(this.Images(listeImages), ImageName, 'ComputationUnit', ComputationUnit, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'GP_Arithmetic_Std_CI1'
        [flag, listeImages, XLim, YLim, ImageName] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'Std');
        if flag
            [flag, a] = MultiImages_Std(this.Images(listeImages), XLim, YLim, ImageName);
        end
        
    case 'GP_Arithmetic_Med_CI1'
        [flag, listeImages, XLim, YLim, ImageName] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'Median');
        if flag
            [flag, a] = MultiImages_Median(this.Images(listeImages), ImageName, 'XLim', XLim, 'YLim', YLim);
        end
        
    case 'GP_Arithmetic_Mode_CI1'
        [flag, listeImages, XLim, YLim, ImageName] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'Mode');
        if flag
            [flag, a] = MultiImages_Mode(this.Images(listeImages), ImageName, 'XLim', XLim, 'YLim', YLim);
        end
        
    case 'GP_Arithmetic_Min_CI1'
        [flag, listeImages, XLim, YLim, ImageName, ComputationAbs] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'Min');
        if flag
            [flag, a] = MultiImages_Min(this.Images(listeImages), ImageName, 'XLim', XLim, 'YLim', YLim, ...
                'ComputationAbs', ComputationAbs);
        end
        
    case 'GP_Arithmetic_Max_CI1'
        [flag, listeImages, XLim, YLim, ImageName, ComputationAbs] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'Max');
        if flag
            [flag, a] = MultiImages_Max(this.Images(listeImages), ImageName, 'XLim', XLim, 'YLim', YLim, ...
                'ComputationAbs', ComputationAbs);
        end
        
    case 'GP_Arithmetic_Sum_CI1'
        [flag, listeImages, XLim, YLim, ImageName] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'Sum');
        if flag
            [flag, a] = MultiImages_Sum(this.Images(listeImages), ImageName, 'XLim', XLim, 'YLim', YLim);
        end
        
    case 'GP_Arithmetic_QuadMean_CI1'
        [flag, listeImages, XLim, YLim, ImageName] = paramsFctsAritmMultiImages(this.Images, this.indImage, 'QuadMean');
        if flag
            [flag, a] = MultiImages_MeanQuad(this.Images(listeImages), ImageName, 'XLim', XLim, 'YLim', YLim);
        end
        
    case 'GP_Arithmetic_Add_Image'
        [flag, ident, isSynchronized, indLayerMask, valMask, zone] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Image � additionner ?', 'Image to add ?'));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
            if flag
                [flag, A, B] = extractSameFrame(this.Images, indLayers, ident, subx, suby, isSynchronized);
                if flag
                    a = plus(A, B, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
                end
            end
        end
        
    case 'GP_Arithmetic_Subtract_Image'
        [flag, ident, isSynchronized, indLayerMask, valMask, zone] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Image � soustraire ?', 'Image to be subtracted'));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
            if flag
                [flag, A, B] = extractSameFrame(this.Images, indLayers, ident, subx, suby, isSynchronized);
                if flag
                    a = minus(A, B, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
                end
            end
        end
        
    case 'GP_Arithmetic_Multiply_Image'
        [flag, ident, isSynchronized, indLayerMask, valMask, zone] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Image � multiplier ?', 'Image to be multiplied'));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
            if flag
                [flag, A, B] = extractSameFrame(this.Images, indLayers, ident, subx, suby, isSynchronized);
                if flag
                    a = mtimes(A, B, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
                end
            end
        end
        
    case 'GP_Arithmetic_Divide_Image'
        [flag, ident, isSynchronized, indLayerMask, valMask, zone] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Image diviseuse ?', 'Image to be used for the division'));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
            if flag
                [flag, A, B] = extractSameFrame(this.Images, indLayers, ident, subx, suby, isSynchronized);
                if flag
                    a = mrdivide(A, B, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
                end
            end
        end
        
    case 'GP_Arithmetic_Formula_CI0'
        [flag, cmd, indLayers, ListeSignauxVert] = paramsFormula(this.Images, this.indImage);
        if flag
            [a, flag] = feval(this.Images(indLayers), ListeSignauxVert, cmd);
        end
        
    case 'GP_Arithmetic_CompleteAWithB_CI1'
        [flag, ident] = paramsCompleteAWithB(this.Images, this.indImage);
        if flag
            a = completeAWithB(this.Images(this.indImage), this.Images(ident), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Arithmetic_Union_CI1'
        [flag, ident] = paramsCompleteAWithB(this.Images, this.indImage);
        if flag
            a = union(this.Images(this.indImage), this.Images(ident), 'XLim', XLim, 'YLim', YLim); %#ok<GTARG>
        end
        
    case 'GP_Arithmetic_Add_Scalar'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone] = paramsFctsAritmScalaire(this.Images, this.indImage, Lang('Valeur', 'Value'));
        if flag
            % a = I1 + x;
            a = plus(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_Arithmetic_Subtract_Scalar'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone] = paramsFctsAritmScalaire(this.Images, this.indImage, Lang('Valeur', 'Value'));
        if flag
            %             a = I1 - x;
            a = minus(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_Arithmetic_Multiply_Scalar'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone] = paramsFctsAritmScalaire(this.Images, this.indImage, Lang('Valeur', 'Value'));
        if flag
            %             a = I1 * x;
            a = mtimes(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_Arithmetic_Divide_Scalar'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone]  = paramsFctsAritmScalaire(this.Images, this.indImage, Lang('Valeur', 'Value'));
        if flag
            %             a = I1 / x;
            a = mrdivide(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_Arithmetic_Add_Vector'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone] = params_FctsAritmVecteur(this.Images, this.indImage, this.cross(this.indImage,:), suby);
        if flag
            % a = I1 + x;
            a = plus(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_Arithmetic_Subtract_Vector'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone] = params_FctsAritmVecteur(this.Images, this.indImage, this.cross(this.indImage,:), suby);
        if flag
            % a = I1 - x;
            a = minus(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_Arithmetic_Multiply_Vector'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone] = params_FctsAritmVecteur(this.Images, this.indImage, this.cross(this.indImage,:), suby);
        if flag
            % a = I1 * x;
            a = mtimes(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_Arithmetic_Divide_Vector'
        I1 = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        [flag, x, indLayerMask, valMask, zone] = params_FctsAritmVecteur(this.Images, this.indImage, this.cross(this.indImage,:), suby);
        if flag
            % a = I1 / x;
            a = mrdivide(I1, x, 'Mask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_ROI_MorphoMath_And_CI1'
        [flag, ident, isSynchronized] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Deuxi�me image', 'Second image'));
        if flag
        	[flag, A, B] = extractSameFrame(this.Images, this.indImage, ident, subx, suby, isSynchronized);
            if flag
                a = A & B;
            end
        end
        
    case 'GP_ROI_MorphoMath_Or_CI1'
        [flag, ident, isSynchronized] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Deuxi�me image', 'Second image'));
        if flag
        	[flag, A, B] = extractSameFrame(this.Images, this.indImage, ident, subx, suby, isSynchronized);
            if flag
                a = A | B;
            end
        end
        
    case 'GP_ROI_MorphoMath_Xor'
        [flag, ident, isSynchronized] = paramsFctsAritmImage(this.Images, this.indImage, Lang('Deuxi�me image', 'Second image'));
        if flag
        	[flag, A, B] = extractSameFrame(this.Images, this.indImage, ident, subx, suby, isSynchronized);
            if flag
                a = xor(A,B);
            end
        end
        
    case 'GP_Derivate_Gradient_Oriented_CI1'
        [flag, Azimuth]  = saisie_Azimuth(this.Images(this.indImage));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
            if flag
                for k=1:length(indLayers)
                    this.Images(indLayers(k)).Azimuth = Azimuth;
                end
                a = gradient(this.Images(indLayers), ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Derivate_Gradient_Greatest_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            a  = normeGradient(this.Images(indLayers), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Derivate_Laplacian_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            a  = del2(this.Images(indLayers), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Derivate_Slope_Oriented_CI1'
        [flag, Azimuth] = saisie_Azimuth(this.Images(this.indImage), 'Type', 2);
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
            if flag
                for k=1:length(indLayers)
                    this.Images(indLayers(k)).Azimuth = Azimuth;
                end
                [a, flag] = slopeAzimuth(this.Images(indLayers), ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Derivate_Slope_Greatest_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            [a, flag] = slopeMax(this.Images(indLayers), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Derivate_Slope_Direction_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            [flag, Step] = params_DerivedFunctionsGreatestSlopeDirection(this.Images(this.indImage), subx, suby);
            if flag
                [a, flag] = slopeDirection(this.Images(indLayers), ...
                    'subx', subx, 'suby', suby, 'Step', Step);
            end
        end
        
    case 'GP_Derivate_Slope_Rose_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            [a, flag] = slopeRose(this.Images(indLayers), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Derivate_SunShading_AzimuthGrey_CI1'
        [flag, Azimuth, VertExag] = saisie_Azimuth_et_VertExag(this.Images(this.indImage));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
            if flag
                for k=1:length(indLayers)
                    this.Images(indLayers(k)).Azimuth  = Azimuth;
                    this.Images(indLayers(k)).VertExag = VertExag;
                end
                a = sunShadingGray(this.Images(indLayers), ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Derivate_SunShading_Azimuth_CI1'
        [flag, Azimuth, VertExag] = saisie_Azimuth_et_VertExag(this.Images(this.indImage));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
            if flag
                for k=1:length(indLayers)
                    this.Images(indLayers(k)).Azimuth  = Azimuth;
                    this.Images(indLayers(k)).VertExag = VertExag;
                end
                this = setOriginalColorbar(this);
                [a, flag] = sunShading(this.Images(indLayers), ...
                    'subx', subx, 'suby', suby);
                MajColorbar = 0;
            end
        end
        
    case 'GP_Derivate_SunShading_GreatestSlope_CI1'
        [flag, VertExag] = params_ombrageGreatestSlope(this.Images(this.indImage));
        if flag
            this = setOriginalColorbar(this);
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
            if flag
                [a, flag] = sunShadingGreatestSlope(this.Images(indLayers), VertExag, ...
                    'subx', subx, 'suby', suby);
                MajColorbar = 0;
            end
        end
        
    case 'GP_Derivate_SunShading_Movie_CI1'
        [flag, nomFic, Azimuths, VertExag, fps, quality, this.repExport] = saisie_Azimuth_et_VertExagMovie(this.Images(this.indImage), this.repExport);
        if flag
            ombrage_movie(this.Images(this.indImage), nomFic, Azimuths, VertExag, ...
                'fps', fps, 'quality', quality, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_Lee_CI1'
        [flag, window] = saisie_window([7 7]);
        if flag
            a = filterLee(this.Images(this.indImage), 'window', window, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_Wiener_CI1'
        [flag, window] = saisie_window([7 7]);
        if flag
            a = filterWiener(this.Images(this.indImage), 'window', window, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_TLineaire_CI1'
        [flag, window] = saisie_window([7 7]);
        if flag
            Alpha = 1;
            a = filterBoucher(this.Images(this.indImage), Alpha, 'window', window, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_Kuan_CI1'
        [flag, window, nbPixMoy, indLayerN] = paramsTagMenuFiltreSpeckle(this.Images, this.indImage);
        if flag
            a = filterSpeckle(this.Images(this.indImage), ...
                'indFiltre', 2, 'window', window, ...
                'LayerN', this.Images(indLayerN), 'nbPixMoy', nbPixMoy, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_Frost_CI1'
        [flag, window, nbPixMoy, indLayerN] = paramsTagMenuFiltreSpeckle(this.Images, this.indImage);
        if flag
            a = filterSpeckle(this.Images(this.indImage), ...
                'indFiltre', 3, 'window', window, ...
                'LayerN', this.Images(indLayerN), 'nbPixMoy', nbPixMoy, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_MAP_CI1'
        [flag, window, nbPixMoy, indLayerN] = paramsTagMenuFiltreSpeckle(this.Images, this.indImage);
        if flag
            a = filterSpeckle(this.Images(this.indImage), ...
                'indFiltre', 4, 'window', window, ...
                'LayerN', this.Images(indLayerN), 'nbPixMoy', nbPixMoy, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_EAP_CI1'
        [flag, window, nbPixMoy, indLayerN] = paramsTagMenuFiltreSpeckle(this.Images, this.indImage);
        if flag
            a = filterSpeckle(this.Images(this.indImage), ...
                'indFiltre', 5, 'window', window, ...
                'LayerN', this.Images(indLayerN), 'nbPixMoy', nbPixMoy, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Speckle_Quantile_CI1'
        [flag, window, nbPixMoy, indLayerN] = paramsTagMenuFiltreSpeckle(this.Images, this.indImage);
        if flag
            a = filterSpeckle(this.Images(this.indImage), ...
                'indFiltre', 7, 'window', window, ...
                'LayerN', this.Images(indLayerN), 'nbPixMoy', nbPixMoy, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Others_Std_CI1'
        [flag, window] = saisie_window([9 9], 'maxvalue', [51 51]);
        if flag
            a = FilterImageTbx(this.Images(this.indImage), @stdfilt, 'window', window, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Others_Entropy_CI1'
        [flag, window] = saisie_window([9 9], 'maxvalue', [51 51]);
        if flag
            a = FilterImageTbx(this.Images(this.indImage), @entropyfilt, 'window', window, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Others_Range_CI1'
        [flag, window] = saisie_window([3 3], 'maxvalue', [51 51]);
        if flag
            a = FilterImageTbx(this.Images(this.indImage), @rangefilt, 'window', window, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Contours_Sobel_CI1'
        a = edge(this.Images(this.indImage), 'Method', 'Sobel', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Filter_Contours_Prewitt_CI1'
        a = edge(this.Images(this.indImage), 'Method', 'Prewitt', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Filter_Contours_Roberts_CI1'
        a = edge(this.Images(this.indImage), 'Method', 'Roberts', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Filter_Contours_LaplacianOfGaussian_CI1'
        a = edge(this.Images(this.indImage), 'Method', 'Log', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Filter_Contours_ZeroCrossing_CI1'
        a = edge(this.Images(this.indImage), 'Method', 'zerocross', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Filter_Contours_Canny_CI1'
        a = edge(this.Images(this.indImage), 'Method', 'canny', ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Filter_Linear_LowpassRectangle_CI1'
        [flag, window] = saisie_window([3 3], 'maxvalue', [51 51]);
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                a = filterRectangle(this.Images(this.indImage), 'window', window, ...
                    'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Filter_Linear_LowpassDisk_CI1'
        [flag, radius] = paramsTagMenuFiltreMoyenneDisk(this.Images(this.indImage));
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                a = filterDisk(    this.Images(this.indImage), 'radius', radius, ...
                    'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Filter_Linear_LowpassGauss_CI1'
        [flag, sigma] = paramsTagMenuFiltreGaussien(I0);
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                a = filterGauss(this.Images(this.indImage), 'sigma', sigma, ...
                    'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Filter_Linear_LowpassGaussOriented_CI1'
        [flag, sigma1, sigma2, angle] = paramsTagMenuFiltreGaussienOriented(I0);
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                a = filterGaussOriented(this.Images(this.indImage), sigma1, sigma2, angle, ...
                    'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Filter_Others_Laplacian_CI1'
        [flag, alpha] = paramsTagMenuFiltreLaplacien(I0);
        if flag
            a = filterLaplacian(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby, 'alpha', alpha);
        end
        
    case 'GP_Filter_Others_Wallis'
        [flag, window, alpha, beta, moy_ref, ect_ref] = paramsTagMenuFiltreWallis(I0);
        if flag
            a = filterWallis(this.Images(this.indImage), 'window', window, ...
                'alpha', alpha, 'beta', beta, 'moy_ref', moy_ref, 'ect_ref', ect_ref, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Filter_Others_Unsharp'
        [flag, alpha] = paramsTagMenuFiltreUnsharp(I0);
        if flag
            a = filterUnsharp(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby, 'alpha', alpha);
        end
        
    case 'GP_Filter_Others_Median'
        [flag, window] = saisie_window([3 3]);
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                a = filterMedian(this.Images(this.indImage), 'window', window, ...
                    'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Filter_Others_Wiener'
        [flag, window] = saisie_window([3 3]);
        if flag
            a = filterWiener(this.Images(this.indImage), 'window', window, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ExtractInser_Interpolation_Sampling_CI1'
        if testSignature(this.Images(this.indImage), 'GeometryType', 'PingXxxx', 'noMessage')
            [flag, indicesLayers, pasx, pasy] = paramsFonctionSonarDownsize(this.Images, this.indImage);
            if flag
                a = sonar_downsize(this.Images([this.indImage indicesLayers]), ...
                    'subx', subx, 'suby', suby, 'pasx', pasx, 'pasy', pasy);
            end
        else
            [pasx, pasy, flag] = params_FonctionSampling(I0);
            if flag
                a = downsize(this.Images(this.indImage), 'Method', 'nearest', ...
                    'subx', subx, 'suby', suby, 'pasx', pasx, 'pasy', pasy);
            end
        end
        
    case 'GP_ExtractInser_Interpolation_CI1'
        %             [~, ~, ~, flag] = paramsFonctionSonarDownsize(this.Images, this.indImage));
        [pasx, pasy, flag] = params_FonctionSampling(I0);
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
            if flag
                a = downsize(this.Images(indLayers), 'FillNaN', 1, ...
                    'subx', subx, 'suby', suby, 'pasx', pasx, 'pasy', pasy);% , 'Method', 'mean');
            end
        end
        
    case 'GP_ExtractInser_Extraction_CI2'
        a = extraction(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ExtractInser_Crop_CI1'
        a = crop(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Carto_ComputeCoordinates_LatLong2GeoYX_CI0'
        this = conversionCoordonneesGeographiques(this, 'latlon2xy');
        
    case 'GP_Carto_ComputeCoordinates_GeoYX2LatLong_CI0'
        this = conversionCoordonneesGeographiques(this, 'xy2latlon');
        
    case 'GP_Carto_ImportWreckCoordinates_CI0'
        this = setCrossFromWreckPosition(this);
        
    case 'GP_Carto_ImportANavigation_CI0'
        [flag, this, X, Y] = params_overplotNavOnImage(this, 'MoreExtensions', '.nvi');
        if flag
            this = overplotNavOnImage(this, X, Y);
        end
        
    case 'GP_Carto_ExtractProfile_CI0' 
        [flag, this, X, Y] = params_overplotNavOnImage(this);
        if flag
            this = traiter_click_coupe(this, 'X', X, 'Y', Y);
        end
        
    case 'GP_Carto_GeoYX2LatLong_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
        if flag
            [flag, GridStep] = params_GeoYX2LatLong(this.Images(indLayers));
            if flag
                [a, flag] = GeoYX2LatLong(this.Images(indLayers), 'GridStep', GridStep, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Carto_LatLong2GeoYX_CI1'
        % TODO : ici, il faudrait r�cup�rer la r�solution de toutes les
        % images synchronis�es pour pouvoir alerter au cas o�
        % l'utilisateur lance la fonction sur plusieurs images qui n'ont
        % pas la m�me r�solution
        [flag, Step] = params_FonctionLatLong2GeoYX(this.Images(this.indImage));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
            if flag
                [a, flag] = LatLong2GeoYX(this.Images(indLayers), ...
                    'subx', subx, 'suby', suby, 'Step', Step);
            end
        end
        
    case 'GP_Carto_ChangeEllipsoid_CI1'
        [flag, NameEllipsoidOut] = params_Carto_ChangeEllipsoid(this.Images(this.indImage));
        if flag
            [flag, a] = LatLongChangeEllipsoid(this.Images(this.indImage), NameEllipsoidOut, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ExtractInser_Insertion_CI1'
        [flag, identParent] = params_FonctionInsertion(this.Images, this.indImage);
        if flag
            this.Images(identParent) = insertion(this.Images(this.indImage), this.Images(identParent), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Interpolation_CI1'
        [flag, window, method] = params_Interpol(this.Images(this.indImage));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
            if flag
                [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
                if flag
                    a = WinFillNaN(this.Images(indLayers), 'method', method, 'window', window, ...
                        'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                        'subx', subx, 'suby', suby);
                end
            end
        end
        
    case 'GP_Animation_CI1'
        [flag, this.repExport, nomFic, fps, indLayers, CLim, quality, Rotation, pV] ...
            = paramsFonctionAnimation(this.Images, this.indImage, this.repExport);
        if flag
            animation(this.Images(indLayers), 'filename', nomFic, 'fps', fps, ...
                'CLim', CLim, 'quality', quality, 'Rotation', Rotation, 'profileVideo', pV, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_HandCorrection_CI0'
        this = handCorrection(this);
        
    case 'GP_Fourier_FFT_CI1'
        a = fft(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Fourier_Filter_Disk_CI0'
        [flag, val, InOut] = paramsFourierFiltreDisk(this.Images(this.indImage));
        if flag
            a = fftFilterDisk(this.Images(this.indImage), val, InOut);
        end
        
    case 'GP_Fourier_Filter_Gauss_CI0'
        [flag, val, InOut] = paramsFourierFiltreDisk(this);
        if flag
            a = fftFilterGauss(this.Images(this.indImage), val, InOut);
        end
        
    case 'GP_Fourier_Filter_Threshold_CI0'
        [flag, seuil] = paramsFourierFiltreSeuil(this.Images(this.indImage));
        if flag
            a = fftFilterThreshold(this.Images(this.indImage), seuil);
        end
        
    case 'GP_Fourier_IFFT_CI0'
        a = ifft(this.Images(this.indImage));
        a = abs(a); % A cause de fftFilterGauss
        
    case 'GP_Compensation_Vert_Mean_CI1'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 1, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby);
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Horz_Mean_CI1'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 2, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby);
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Vert_Med_CI1'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 5, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby);
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Vert_Max_CI1'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 7, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby);
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Vert_Min'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 8, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby);
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Vert_Sum_CI1'
        TypeOperation = '/';
        [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 12, 'TypeOperation', TypeOperation, ...
            'subx', subx, 'suby', suby);
        if flag
            this.Images(this.indImage) = b;
        end
        
    case 'GP_Compensation_Horz_Med_CI1'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 6, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby);
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Horz_Max_CI1'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 9, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby);
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Horz_Min_CI1'
        [flag, TypeOperation] = paramsCompensationTypeOperation(this.Images(this.indImage), 'NoMedian');
        if flag
            [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 10, 'TypeOperation', TypeOperation, ...
                'subx', subx, 'suby', suby );
            if flag
                this.Images(this.indImage) = b;
            end
        end
        
    case 'GP_Compensation_Horz_Sum_CI1'
        TypeOperation = '/';
        [flag, a, b] = compensation(this.Images(this.indImage), 'Type', 13, 'TypeOperation', TypeOperation, ...
            'subx', subx, 'suby', suby);
        if flag
            this.Images(this.indImage) = b;
        end
        
        
    case 'GP_Compensation_Plan_CI1'
        [flag, OriginPolynome, nomFic, ~, ~, this.repImport] = params_CompensationPolynome(this.Images(this.indImage), 1, this.repImport);
        if flag
            if OriginPolynome == 1
                [flag, a] = compensation(this.Images(this.indImage), ...
                    'Type', 3, 'NomFicPolynome', nomFic, ...
                    'subx', subx, 'suby', suby);
            else
                [flag, a] = compensation(this.Images(this.indImage), ...
                    'Type', 11, 'NomFicPolynome', nomFic, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Compensation_Polynom_CI1'
        [flag, OriginPolynome, nomFic, pX, pY, this.repImport] = params_CompensationPolynome(this.Images(this.indImage), 2, this.repImport);
        if flag
            if OriginPolynome == 1
                if flag
                    [flag, a] = compensation(this.Images(this.indImage), ...
                        'Type', 4, 'NomFicPolynome', nomFic, 'pX', pX, 'pY', pY, ...
                        'subx', subx, 'suby', suby);
                end
            else
                [flag, a] = compensation(this.Images(this.indImage), 'Type', 11, ...
                    'NomFicPolynome', nomFic, 'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Codage_Reflectivity_Enr2dB_CI1'
        [a, flag] = reflec_Enr2dB(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Codage_Reflectivity_dB2Enr_CI1'
        [a, flag] = reflec_dB2Enr(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Codage_Reflectivity_Amp2dB_CI1'
        [a, flag] = reflec_Amp2dB(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Codage_Reflectivity_dB2Amp_CI1'
        [a, flag] = reflec_dB2Amp(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Codage_Log10Abs_CI1'
        a = log10(this.Images(this.indImage), 'Abs', 1, ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Codage_SqrtAbs_CI1'
        a = sqrt(this.Images(this.indImage), 'Abs', 1, ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Codage_ToStats_CI1'
        [flag, Mean, Std] = paramsCodageCameleon(I0);
        if flag
            a = cameleon(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby, ...
                'Mean', Mean, 'Std', Std);
        end
        
    case 'GP_Codage_Quantify_CI1'
        [flag, N, Limits] = paramsCodageQuantification(this.Images(this.indImage));
        if flag
            a = quantify(this.Images(this.indImage), 'N', N, 'Limits', Limits, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_Create_Values_CI1'
        [flag, creationLayer, nomLayer, indLayerMask, valSpecifiqueDepart, valInterval, valMask] = paramsMasqueDeduit(this.Images, this.indImage);
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
            if flag
                for k=1:length(indLayers)
                    b = ROI_auto(this.Images(indLayers(k)), creationLayer, nomLayer, this.Images(indLayerMask), ...
                        valSpecifiqueDepart, valInterval, valMask, ...
                        'subx', subx, 'suby', suby);
                    if creationLayer
                        Name = b.Name;
                        CLim = b.CLim;
                        b = extraction(b, 'subx', subx, 'suby', suby);
                        b.Name = Name;
                        b.CLim = CLim;
                        a(k) = b;
                    else
                        this.Images(indLayerMask) = b;
                    end
                end
            end
        end
        
    case 'GP_ROI_Create_Histo_CI1'
        [flag, Windows] = params_HistogramCleaningParBlocks(this.Images(this.indImage), subx, suby);
        if flag
            [flag, a] = interactive_histogramCleaning(this.Images(this.indImage), Windows, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_Create_Manual_CI0'
        [flag, creationLayer, nomLayer, indLayerMask] = paramsMaskInteractive(this.Images, this.indImage);
        if flag
            [this, a, flag] = maskInteractive(this, creationLayer, nomLayer, indLayerMask);
        end
        
    case 'GP_ROI_Contours_Display'
        ROI_display(this);
        
    case 'GP_ROI_Contours_Rename_CI0'
        this.Images(this.indImage) = ROI_rename(this.Images(this.indImage));
        
    case 'GP_ROI_Contours_Delete_CI0'
        this.Images(this.indImage) = ROI_delete(this.Images(this.indImage));
        this = show_image(this, 'MajColorbar', 0);
        
    case 'GP_ROI_Contours_RemoveCurvesOnCurrentImage_CI0'
        this = mask_cleanSonarScope(this);
        
    case 'GP_ROI_Contours_Mask2Contours_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            this.Images(indLayers) = ROI_raster2vect(this.Images(indLayers), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_Contours_Contours2Mask_CI1'
        [flag, creationLayer, nomLayer, indLayerMask] = paramsCreationMaskFromContours(this.Images, this.indImage);
        if flag
            [subx, suby] = get_subx_suby(this); % TODO : v�rifier si cela est n�cessaire
            % TODO : voir pourquoi on ram�ne 3 instances
            [this.Images(this.indImage), this.Images(indLayerMask), a, flag] = mask_creationMaskFromContours(this.Images(this.indImage), creationLayer, nomLayer, this.Images(indLayerMask), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_MaskImage_CI1'
        [flag, indLayerMask, valMask, zone] = paramsMasquage(this.Images, this.indImage, 'AtLeastOne', 1);
        if flag
            [a, flag] = masquage(this.Images(this.indImage), ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                'subx', subx, 'suby', suby, 'zone', zone);
        end
        
    case 'GP_ROI_CombineMasks_CI1'
        [flag, indLayerMask] = paramsCombineMasks(this.Images, this.indImage);
        if flag
            [flag, a] = combineMasks(this.Images(this.indImage), this.Images(indLayerMask), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_MultiImages_MaskImage_CI0'
        [flag, indLayerMask, valMask, zone] = paramsMasquage(this.Images, this.indImage, 'SelectionMode', 'Single');
        if flag
            [flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage, 'IncludeCurrentImage');
            if flag
                str1 = 'Liste des images';
                str2 = 'Liste of layers';
                [choix, flag] = my_listdlgMultiple(Lang(str1,str2), nomsLayers);
                if flag
                    indLayers = indLayers(choix);
                    if ~isempty(indLayers)
                        a = masquage(this.Images(indLayers), ...
                            'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                            'zone', zone);
                    end
                end
            end
        end
        
    case 'GP_ROI_MultiImages_MaskAppairedImages_CI0'
        [flag, indLayers, indLayerMask, zone, valMask] = params_MultiImagesMasquagePaires(this.Images, this.indImage);
        if flag
            a = masquage(this.Images(indLayers), ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone);
        end
        
    case 'GP_ROI_MultiImages_CreateMask_CI0'
        [flag, indLayers, valInterval, valMask] = params_ROI_auto_multiImages(this.Images, this.indImage);
        if flag
            [flag, a] = ROI_auto_multiImages(this.Images, indLayers, valInterval, valMask);
        end
        
    case 'GP_ROI_BitsSupport_MaskImage_CI1'
        [flag, indLayerMask, Bits] = paramsMasquageBits(this.Images, this.indImage);
        if flag
            a = masquageBits(this.Images(this.indImage), this.Images(indLayerMask), Bits, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_BitsSupport_Extraction_CI1'
        [flag, Bits] = paramsMasquageExtractionBits(this.Images(this.indImage));
        if flag
            a = masqueExtractionBits(this.Images(this.indImage), Bits, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_Contours_Export_CI0'
        [flag, indLayers] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
        if flag
            [flag, this.repExport] = ROI_export(this.Images(indLayers), this.repExport);
        end
        
    case 'GP_ROI_Contours_Import_CI0'
        [this.Images(this.indImage), this.repImport] = ROI_import(this.Images(this.indImage), this.repImport);
        this = show_image(this, 'MajColorbar', 0);
        
    case 'GP_Segmentation_ImenMaster_OriginalSize_CI1'
        [flag, pasx, pasy] = params_ResizeOriginImage(this.Images, this.indImage);
        if flag
            a = resize(this.Images(this.indImage), pasx, pasy, ...
                'subx', subx, 'suby', suby, 'Method', 'nearest');
        end
        
    case {'GP_ROI_ColorizeImage_CI1'; 'GP_Segmentation_ImenMaster_ColorizeImage_CI1'}
        [flag, indLayerMask, valMask, alpha, zone] = paramsColorisation(this.Images, this.indImage);
        if flag
            a = colorisation(this.Images(this.indImage), ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                'subx', subx, 'suby', suby, 'alpha', alpha, 'zone', zone);
        end
        
    case 'GP_ROI_Others_Watershed_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            [flag, Seuil] = paramsWatershed;
            if flag
                a = watershed(this.Images(indLayers), 'Seuil', Seuil, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_ROI_Others_GetCorners_CI1'
        [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'memeDataType');
        if flag
            getCorners(this.Images(indLayers), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_Others_LevelSet_CI1'
        if flag
            a = levelset(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby);
        end
                
    case 'GP_ROI_MorphoMath_Dilatation_CI1'
        [flag, SE] = params_MorphoMath;
        if flag
            a = imdilate(this.Images(this.indImage), SE, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_MorphoMath_Erosion_CI1'
        [flag, SE] = params_MorphoMath;
        if flag
            a = imerode(this.Images(this.indImage), SE, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_MorphoMath_Opening_CI1'
        [flag, SE] = params_MorphoMath;
        if flag
            a = imopen(this.Images(this.indImage), SE, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_MorphoMath_Closing_CI1'
        [flag, SE] = params_MorphoMath;
        if flag
            a = imclose(this.Images(this.indImage), SE, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ROI_MorphoMath_Contour_CI1'
        a = contour_masque(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ROI_MorphoMath_WrappingAroundMAsk_CI1'
        [flag, Radius] = paramsMasqueEnglobant;
        if flag
            a = masque_englobant(this.Images(this.indImage), 'Rayon', Radius, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Codage_Binary_CI1'
        [flag, Seuil] = paramsBinarisation(this.Images(this.indImage));
        if flag
            a = ge(this.Images(this.indImage), Seuil, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Segmentation_ImenMaster_HaralickParameters_CI1'
        % TODO : regrouper ces 3 appels en un seul
        [flag, binsImage, subDep, subDir, subPar] = paramsTexturalParameters(this.Images(this.indImage));
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage, 'InitialValue', 'Segmentation');
            if flag
                [flag, W] = saisie_window([32 32], 'maxvalue', [64 64]);
                if flag
                    flag = plot_textural_parameters(this.Images(this.indImage), binsImage, subDep, subDir, subPar, W, ...
                        this.Images(indLayerMask), valMask, ...
                        'subx', subx, 'suby', suby);
                end
            end
        end
        
    case 'GP_Segmentation_ImenMaster_KullbackDistance_CI1'
        % TODO : regrouper ces 2 appels en un seul
        [flag, binsImage, subDep, subDir] = paramsDistanceKullback(this.Images(this.indImage));
        if flag
            [flag, indLayerMask, valMask] = paramsMaskKullback(this.Images, this.indImage, 'InitialValue', 'Segmentation');
            if flag
                plot_distance_kullback(this.Images(this.indImage), binsImage, subDep, subDir, ...
                    this.Images(indLayerMask), valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Segmentation_Samantha_CI1'
        [flag, subCurves, listeprofils, listeprofils1, indLayerMask, valMask, w, ...
            StopRegul, UseModelBS] = params_SegmentationCourbes(this.Images, this.indImage);
        if flag
            [flag, a] = segmentation_CourbesStats(this.Images, this.indImage, subCurves, listeprofils, listeprofils1, ...
                indLayerMask, valMask, w, StopRegul, UseModelBS, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Segmentation_ImenMaster_Training_Compute_CI1'
        [flag, this.Images] = calcul_TextureApprentissageCalcul(this.Images, this.indImage, subx, suby);
        
    case 'GP_Segmentation_ImenPhd_Training_Compute_CI1'
        [flag, this.Images] = calcul_TextureTheseApprentissageCalcul(this.Images, this.indImage, subx, suby);
        
    case 'GP_Segmentation_ImenMaster_Training_Display_CI0'
        sub  = selectionSegmSignature(this.Images(this.indImage), 'SelectionMode', 'Multiple');
        plotSegmSignature(this.Images(this.indImage), 'numTexture', sub, 'OnlyOneFig', 1);
        flag = 0;
        
    case 'GP_Segmentation_ImenMaster_Training_DisplayKullbackDist_CI0'
        plotSegmSignatureDist(this.Images(this.indImage), 'SelectionMode', 'Multiple');
        
    case 'GP_Segmentation_ImenMaster_Training_Delete_CI0'
        sub  = selectionSegmSignature(this.Images(this.indImage), 'Confirmation', 'SelectionMode', 'Multiple');
        if ~isempty(sub)
            this.Images(this.indImage) = deleteSegmSignature(this.Images(this.indImage), 'sub', sub);
        end
        
    case 'GP_Segmentation_ImenMaster_Training_Export_CI0'
        sub = selectionSegmSignature(this.Images(this.indImage), 'SelectionMode', 'Multiple');
        if isempty(sub)
            message_NoCurve(this.Images(this.indImage))
        else
            nomFic = fullfile(this.repExport, 'Texture.mat');
            [flag, nomFic] = my_uiputfile('*.mat', 'Give a file name', nomFic);
            if ~flag
                return
            end            
            exportSegmSignature(this.Images(this.indImage), nomFic, 'sub', sub);
            
            pathname = fileparts(nomFic);
            this.repExport = pathname;
        end
        
    case 'GP_Segmentation_ImenMaster_Training_Import_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.mat'}, ...
            'RepDefaut', this.repImport);
        if flag
            this.Images(this.indImage) = importSegmSignature(this.Images(this.indImage), nomFic);
        end
        
    case 'GP_Segmentation_ImenMaster_Training_Fusion_CI0'
        sub = selectionSegmSignature(this.Images(this.indImage));
        if isempty(sub)
            message_NoCurve(this.Images(this.indImage))
        else
            nomVar = {'Name of the new textural signature'};
            value = {'Fusion'};
            [value, flag] = my_inputdlg(nomVar, value);
            if flag
                this.Images(this.indImage) = fusionSegmSignature(this.Images(this.indImage), sub, value{1});
            end
        end
        
    case 'GP_Segmentation_ImenMaster_Compute_CI1'
        [flag, sub, indLayerAngle, iclique, ~, Win, typeWindows, SeuilConfusion, SeuilRejet, FilterCoocMat] ...
            = params_FonctionTextureSegmentation(this.Images, this.indImage);
        if flag
            if typeWindows == 1 % Segmentation par fen�tres adjacentes
                a = texture_segmenImen1(this.Images(this.indImage), this.Images(indLayerAngle), ...
                    'Win', Win, 'iClique', iclique, 'sub', sub, ...
                    'SeuilConfusion', SeuilConfusion, 'SeuilRejet', SeuilRejet, 'FilterCoocMat', FilterCoocMat, ...
                    'subx', subx, 'suby', suby);
            else % Segmentation par fen�tres glissantes
                a = texture_segmenImen2(this.Images(this.indImage), this.Images(indLayerAngle), ...
                    'Win', Win, 'iClique', iclique, 'sub', sub, ...
                    'SeuilConfusion', SeuilConfusion, 'SeuilRejet', SeuilRejet, 'FilterCoocMat', FilterCoocMat, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
        
    case 'GP_Segmentation_ImenPhd_Compute_CI1'
        [flag, Texture, indLayerAngle, ~, Win, SeuilConfusion, SeuilRejet, FilterCoocMat] ...
            = params_FonctionTextureSegmentationPhD(this.Images, this.indImage);
        if flag
            a = texture_segmenImen4(this.Images(this.indImage), this.Images(indLayerAngle), ...
                Texture, 'Win', Win, ...
                'SeuilConfusion', SeuilConfusion, 'SeuilRejet', SeuilRejet, 'FilterCoocMat', FilterCoocMat, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Segmentation_Homogenisation_CI1'
        [flag, Radius] = paramsMasqueEnglobant;
        if flag
            [flag, a] = homogeniseSegmentation(this.Images(this.indImage), Radius, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Segmentation_ImenMaster_Smooth_CI1'
        [flag, Radius] = paramsMasqueEnglobant;
        if flag
            [flag, a] = smoothContours(this.Images(this.indImage), Radius, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_Synthesis_InPainting_CI1'
        [flag, Method] = params_SyntheseInpainting(I0);
        if flag
            [flag, indLayerMask, ValMask] = paramsMasque(this.Images, this.indImage);
            if flag
                [flag, a] = inpaint(this.Images(this.indImage), 'Method', Method, ...
                    'Mask', this.Images(indLayerMask), 'ValMask', ValMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_Synthesis_2DGauss_CI1'
        [px, py, x, y, Parametres, flag] = params_SyntheseDepth(I0);
        if flag
            a = synth_formeGauss(this.Images(this.indImage), px, py, x, y, Parametres);
        end
        
    case 'GP_Composit2freq_CI1'
        [flag, indBF, indHF, ImageName] = params_composeColoredImageFrom2Frequencies(this.Images, this.indImage);
        if flag
            [flag, a] = composeColoredImageFrom2Frequencies(this.Images(indBF), this.Images(indHF), ImageName);
        end
        
    case 'GP_Compensation_Curves_CI1'
        [flag, subCourbe] = selectionCourbesStats(this.Images(this.indImage), 'SelectionMode', 'Single');
        if flag && ~isempty(subCourbe)
            [flag, bilan, identLayersCondition, ModeDependant, UseModel] = paramsCompensationCourbesStats(this.Images, this.indImage, subCourbe);
            if flag
                [flag, TypeOperation, TypeStats] = paramsCompensationTypeOperation(this.Images(this.indImage));
                if flag
                    zone = 1;
                    [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
                    if flag
                        [a, flag] = compensationCourbesStats(this.Images(this.indImage), this.Images(identLayersCondition), ...
                            bilan, ...
                            'TypeOperation', TypeOperation, 'TypeStats', TypeStats, ...
                            'LayerMask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone, ...
                            'ModeDependant', ModeDependant, 'UseModel', UseModel, ...
                            'subx', subx, 'suby', suby);
                    end
                end
            end
        end
        
    case 'GP_ImageType_ChangeNaNColor_CI1'
        % Fonction d�plac�e dans IfremerToolboxFonctionsObsoletes
        [Rayon, flag] = paramsMasqueEnglobant;
        if flag
            [a, flag] = RGBNaNColor(this.Images(this.indImage), 'Rayon', Rayon, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_ImageType_RGB2Intensity_CI1'
        [a, flag] = RGB2Intensity(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ImageType_RGB2HSV_CI1'
        [flag, a] = RGB2HSV(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ImageType_RGB2Indexed_CI1'
        [a, flag] = RGB2Indexed(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ImageType_Intensity2RGB_CI1'
        [a, flag] = Intensity2RGB(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ImageType_RGB2IntensityWithGMTColomapLimits_CI1'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.cpt'}, ...
            'RepDefaut', this.repImport);
        if flag
            [rep, flag] = my_questdlg(Lang('M�thode ?', 'Method ?'), Lang('Valeuts GMT', 'GMT values'), Lang('Interpolation lin�aire', 'Linear interpolation'));
            if flag
                [a, flag] = Intensity2RGB_GMT(this.Images(this.indImage), nomFic{1}, 'Inter', rep == 2, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'GP_ImageType_Intensity2Indexed_CI1'
        [a, flag] = Intensity2Indexed(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ImageType_Indexed2RGB_CI1'
        [a, flag] = Indexed2RGB(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ImageType_Indexed2Intensity_CI1'
        [a, flag] = Indexed2Intensity(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ImageType_Indexed2IntensityDirect_CI1'
        [a, flag] = Indexed2IntensityDirect(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Cast_Uint8_CI1'
        a = uint8(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Cast_Single_CI1'
        a = single(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_Cast_Double_CI1'
        a = double(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'GP_ObjectAnalysis_QuadTree_CI1'
        [flag, threshold] = paramsQuadtree(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        if flag
            a = quadtree(this.Images(this.indImage), threshold, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'GP_SignalVert_CreateImage_CI0'
        [flag, signal, IndLayerIndexation] = params_CreateLayerFromSignals(this.Images, this.indImage);
        if flag
            [flag, a] = create_LayerFromSignalVert(this.Images(this.indImage), signal, ...
                'Index', this.Images(IndLayerIndexation));
        end
        
    case 'Help'
        flag = SonarScopeHelp(varargin{2});
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this, 'MajColorbar', MajColorbar);
    this = callback_zoomDirect(this, 'SamePosition'); % Ajout� le 12/08/2009 : devrait r�soudre le pb de zoom qui n'est pas correct lorque l'on traite une image enti�re alors que l'on est positionn� sur une partie de l'image
end
