function this = callback_GetModifiedImageAxeImage(this)

%% TODO : Truc maladroit, juste pour remettre les callbacks en place : faut faire mieux que �a

identAxe = test_curseur_sur_axes(this);
this = traiter_click_CleanData(this, identAxe);

%% Test si on est toujours en mode Rubber

[flag, TypeInteraction] = this.hGUITypeInteractionSouris.getTypeInteractionCleanData();
if ~flag
    return
end
if TypeInteraction ~= 4
    return
end

%% R�cup�ration des modifs

SelectionType = get(this.hfig, 'SelectionType');
if strcmp(SelectionType, 'normal')
    that = this.Images(this.indImage);

    XLim = this.visuLim(this.indImage, 1:2);
    YLim = this.visuLim(this.indImage, 3:4);
    x = that.x;
    y = that.y;
    
    figure(this.hfig)
    GCA = gca;
    GCO = findobj(GCA, 'Type', 'Image');
    
    CData = get(GCO, 'CData');
    masq = isnan(CData);
    
    subx = find((x >= XLim(1)) & (x <= XLim(2)));
    suby = find((y >= YLim(1)) & (y <= YLim(2)));
    val = get_val_ij(this.Images(this.indImage), suby, subx);
    
    if isempty(masq) || isempty(subx) || isempty(suby)
        return
    end
    masq = imresize(masq, [length(suby) length(subx)], 'nearest');
    
    val(masq) = NaN;
    that = set_val(that, suby, subx, val);
    
    this.Images(this.indImage) = that;
end

this = show_image(this);
this = show_profils(this);
