function this = callback_GetModifiedImageAxeX(this)

SelectionType = get(this.hfig, 'SelectionType');
if strcmp(SelectionType, 'normal')
    that = this.Images(this.indImage);
    % ValNaN = get(that, 'ValNaN');
    x = get(that, 'x');
    cross = this.cross(this.indImage,:);
    [~, ~, iy] = get_val_xy(that, cross(1), cross(2));
    
    
    NbVoisins = this.NbVoisins;
    ideb = iy - NbVoisins;
    ideb = max(ideb, 1);
    ifin = iy + NbVoisins;
    ifin = min(ifin, double(that.nbRows));
    iyDisplay = ideb:ifin;
    
    val = get_val_ij(that, iyDisplay, []);
    
    GCA = gca;
    XLim = get(GCA, 'XLim');
    % YLim = get(GCA, 'YLim');
    hc = get(GCA, 'Children');
    hc = setdiff(hc, this.hCroixProfilX);
    % XData = get(hc(end), 'XData');
    
    for k=1:length(hc)
        Type = get(hc(k), 'Type');
        if strcmp(Type, 'line')
            %         XData = get(hc(k), 'XData');
            YData = get(hc(k), 'YData');
            iyhc  = get(hc(k), 'UserData');
            if ~isempty(iyhc)
                LineStyle = get(hc(k), 'LineStyle');
                
                subx = ((x >= XLim(1)) & (x <= XLim(2)));
                %             [c, ia, ib] = intersect(x(subx), XData)
                
                ky = find(iyhc == iyDisplay);
                if ~isempty(ky)
                    
                    %         fprintf('ky = %d\n', ky);
                    
                    switch LineStyle
                        case '-'
                            val(ky,subx) = YData;
                        case '.'
                            %             val(ky,subx) = YData;
                        case 'none'
                            % Kesaco ?
                        otherwise
                            LineStyle %#ok<NOPRT>
                    end
                end
            end
        end
    end
    
    that = set_val(that, iyDisplay, [], val);
    this.Images(this.indImage) = that;
end

this = show_image(this);
this = show_profils(this);
