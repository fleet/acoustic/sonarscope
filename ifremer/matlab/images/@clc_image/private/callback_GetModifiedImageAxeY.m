function this = callback_GetModifiedImageAxeY(this)

if strcmp(this.profilY.Type, 'Vertical profile') || strcmp(this.profilY.Type, 'Image profile')
    this = callback_GetModifiedImageAxeY_Image(this);
else
    this = callback_GetModifiedImageAxeY_Signal(this);
end


function this = callback_GetModifiedImageAxeY_Image(this)

SelectionType = get(this.hfig, 'SelectionType');
if strcmp(SelectionType, 'normal')
    that = this.Images(this.indImage);
    % ValNaN = get(that, 'ValNaN');
    y = get(that, 'y');
    cross = this.cross(this.indImage,:);
    [~, ix, iy] = get_val_xy(that, cross(1), cross(2)); %#ok<ASGLU>
    
    
    NbVoisins = this.NbVoisins;
    ideb = ix - NbVoisins;
    ideb = max(ideb, 1);
    ifin = ix + NbVoisins;
    ifin = min(ifin, double(that.nbColumns));
    ixDisplay = ideb:ifin;
    
    val = get_val_ij(that, [], ixDisplay);
    
    GCA = gca;
    YLim = get(GCA, 'YLim');
    hc = get(GCA, 'Children');
    hc = setdiff(hc, this.hCroixProfilY);
    
    for k=1:length(hc)
        Type = get(hc(k), 'Type');
        if strcmp(Type, 'line')
            XData = get(hc(k), 'XData');
%             YData = get(hc(k), 'YData');
            ixhc  = get(hc(k), 'UserData');
            if ~isempty(ixhc)
                LineStyle = get(hc(k), 'LineStyle');
                
                suby = ((y >= YLim(1)) & (y <= YLim(2)));
                %             [c, ia, ib] = intersect(x(subx), XData)
                
                kx = find(ixhc == ixDisplay);
                if ~isempty(kx)
                    
                    %         fprintf('ky = %d\n', ky);
                    
                    switch LineStyle
                        case '-'
                            val(suby,kx) = XData;
                        case '.'
                            %             val(ky,subx) = YData;
                        case 'none'
                            % Kesaco ?
                        otherwise
                            LineStyle %#ok<NOPRT>
                    end
                end
            end
        end
    end
    
    that = set_val(that, [], ixDisplay, val);
    this.Images(this.indImage) = that;
end

this = show_image(this);
this = show_profils(this);


function this = callback_GetModifiedImageAxeY_Signal(this)

SelectionType = get(this.hfig, 'SelectionType');
if strcmp(SelectionType, 'normal')
    that = this.Images(this.indImage);
    % ValNaN = get(that, 'ValNaN');
    y = get(that, 'y');
    cross = this.cross(this.indImage,:);
    [~, ix] = get_val_xy(that, cross(1), cross(2));
    
    ixDisplay = ix;
    
    val = get_val_ij(that, [], ixDisplay); % TODO : pas utile de lire, faire un NaN(n,1) � la place
    [flag, X] = getValSignalIfAny(that, this.profilY.Type, 'val', val);
    if flag
        val = X;
    else
        this.profilY.Type = 'Image profile'; 
    end

    GCA = gca;
    YLim = get(GCA, 'YLim');
    hc = get(GCA, 'Children');
    hc = setdiff(hc, this.hCroixProfilY);
    
    for k=1:length(hc)
        Type = get(hc(k), 'Type');
        if strcmp(Type, 'line')
            XData = get(hc(k), 'XData');
            %             YData = get(hc(k), 'YData');
            LineStyle = get(hc(k), 'LineStyle');
            
            suby = ((y >= YLim(1)) & (y <= YLim(2)));
            %             [c, ia, ib] = intersect(x(subx), XData)
            
            %         fprintf('ky = %d\n', ky);
            
            switch LineStyle
                case '-'
                    val(suby,1) = XData;
                case '.'
                    %             val(ky,subx) = YData;
                case 'none'
                    % Kesaco ?
                otherwise
                    LineStyle %#ok<NOPRT>
            end
        end
    end
    
    suby = 1:length(y);
    subNaN    = find(isnan(val));
    subNonNaN = find(~isnan(val));
    val(subNaN) = my_interp1(subNonNaN, val(subNonNaN), subNaN);
    [~, this] = setValSignalIfAny(this, val, suby);
    
%     that = set_val(that, [], ixDisplay, val);
%     this.Images(this.indImage) = that;
end

this = show_image(this);
this = show_profils(this);
