function this = callback_HAC(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

H0 = cl_hac([]);
a  = cl_image.empty;

global NetUseWords %#ok<GVMIS> 

NetUseWords = [];

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'HAC_Preprocessing_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.hac', 'RepDefaut', this.repImport);
        if flag
            preprocess_HAC(H0, listeFic);
        end
        
%     case 'HAC_CleanSignals_CI0' % Pas utilis�
%         [flag, nomFic, identFreq, this.repImport] = params_HAC_CleanSignals(this.repImport);
%         if flag
%             HAC_CleanSignals(H0, nomFic, 'identFreq', identFreq);
%         end
        
    case 'HAC_PlotNavigationAndTime_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.hac', 'RepDefaut', this.repImport);
        if flag
            plot_nav_HAC(H0, nomFic);
        end
        
    case 'HAC_PlotNavigationAndSignal_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.hac', 'RepDefaut', this.repImport);
        if flag
            [Fig, Carto] = plot_nav_HAC(H0, listeFic);
            plot_position_data(H0, Fig, listeFic, 'Heading', 'Carto', Carto);
        end
 
    case 'HAC_Export3DViewer_CI0'
        [flag, listeFicHac, this.repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.hac', 'RepDefaut', this.repImport);
        if flag
            [flag, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, HauteurMin, SuppressionOverHeight, identLayer, ...
                TailleMax, MeanTide, this.repExport] = params_HAC_Export3DViewer(this.Images(this.indImage), this.repExport);
            if flag
                flag = survey_Export3DViewer(H0, listeFicHac, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, ...
                    HauteurMin, SuppressionOverHeight, identLayer, TailleMax, MeanTide);
            end
        end
        
    case 'HAC_CompressDirectories_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.hac', 'RepDefaut', this.repImport);
        if flag
            compressDirectories_SSc(nomFic, 'Tag', 'HAC');
        end
        
    case 'HAC_DeleteCache_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.hac', 'RepDefaut', this.repImport);
        if flag
            eraseSonarScopeCache(nomFic);
        end        
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
