% Mise a jour du composant callback_ImageName
%
% Syntax
%   this = callback_ImageName(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_ImageName(this)

%% R�cuperation du nouveau nom

this.ihm.NomImage.cpn = gerer_callback(this.ihm.NomImage.cpn, this.ihm.NomImage.msg);
NewName = get_texte(this.ihm.NomImage.cpn);
NewName = removeAccentuation(NewName);
this.ihm.NomImage.cpn = set_texte(this.ihm.NomImage.cpn, NewName);

%% On met � jour le nom dans l'instance cl_image

this.Images(this.indImage).Name = NewName;

%% R�affichage du nom de l'image

figure(this.hfig);
title(NewName, 'Interpreter', 'none');
