% Traitement des deplacements du curseur
%
% Syntax
%   this = callback_NaN(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_KeyPress(this, varargin)

[varargin, KeyPress] = getFlag(varargin, 'KeyPress'); %#ok<ASGLU>
if ~KeyPress
    return
end

CurrentCharacter = get(gcf, 'CurrentCharacter');
% pppp = double(CurrentCharacter)

if isempty(CurrentCharacter)
    return
end

if strcmp(CurrentCharacter, 'c')
    % Contraste local � 0.5 %
    this = callback_contrast(this, 'ContrasteImageZoomeeQuant_0p5pc');
    
elseif strcmp(CurrentCharacter, 'C')
    % Contraste local � 1 %
    this = callback_contrast(this, 'ContrasteImageZoomeeQuant_1pc');
    
elseif strcmp(CurrentCharacter, 'm')
    % Contraste local � 1 %
    this = callback_contrast(this, 'ContrasteImageZoomeeMinMax');
    
elseif strcmp(CurrentCharacter, 'S')
    % Ombrage couleur
    this = callback_GP(this, 'GP_Derivate_SunShading_Azimuth_CI1', 'CurrentExtent');
    
elseif strcmp(CurrentCharacter, 'i')
    % Interpolation
    this = callback_GP(this, 'GP_Interpolation_CI1', 'CurrentExtent');
    
elseif strcmp(CurrentCharacter, 'N')
    % Interpolation
    this = callback_GP(this, 'GP_Arithmetic_SetNaN_CI1', 'CurrentExtent');
    
elseif strcmp(CurrentCharacter, 'H')
    % Calcul de l'histogramme
    this = callback_STATS(this, 'STATS_Histogramme_CI1', 'CurrentExtent');
    
elseif strcmp(CurrentCharacter, 'v')
    % Moyennage Vertical
    this = callback_STATS(this, 'STATS_MoyenneVert_CI1', 'CurrentExtent');
    
elseif strcmp(CurrentCharacter, 'h')
    % Moyennage Horizontal
    this = callback_STATS(this, 'STATS_MoyenneHorz_CI1', 'CurrentExtent');
    
elseif strcmp(CurrentCharacter, 's')
    % Screendump
    this = callback_export(this, 'EXPORT_ScreenDump_CI0');
    
elseif strcmp(CurrentCharacter, 'e')
    % Exportation Image en .ers
    this = callback_export(this, 'EXPORT_ERS_Image_CI1');
    
elseif strcmp(CurrentCharacter, 't')
    % Exportation Image en .tif
    this = callback_export(this, 'EXPORT_Raster_CI1', '.tif');
    
elseif strcmp(CurrentCharacter, 'j')
    % Exportation Image en .jpg
    this = callback_export(this, 'EXPORT_Raster_CI1', '.jpg');
    
elseif strcmp(CurrentCharacter, 'b')
    % Exportation Image en .bmp
    this = callback_export(this, 'EXPORT_Raster_CI1', '.bmp');
    
elseif strcmp(CurrentCharacter, 'g')
    % Exportation Image en Google-Earth
    this = callback_export(this, 'EXPORT_GoogleEarth_CI1');
    
elseif strcmp(CurrentCharacter, 'G')
    % Exportation Image en GeoTiff r�els sur 32 bits
    this = callback_export(this, 'EXPORT_ImageGeotiff32Bits_CI1');
    
elseif strcmp(CurrentCharacter, 'x')
    % Exportation Image en .xml
    this = callback_export(this, 'EXPORT_XML_Image_CI1');
    
elseif strcmp(CurrentCharacter, 'V')
    % Profil vertical
    this = callback_profileVert(this, 'DirectWay');
    
elseif strcmp(CurrentCharacter, 'p')
    % Set Point & Click Mode
    this = callback_typeInteractionSouris(this, 'PointClick');
    
elseif strcmp(CurrentCharacter, 'z')
    % Set Zoom Mode
    this = callback_typeInteractionSouris(this, 'ZoomInteractif');
    
elseif strcmp(CurrentCharacter, '+')
    % Zoon In
    this = callback_zoomDirect(this, 'ZoomInXY');
    
elseif strcmp(CurrentCharacter, '*')
    % Zoon In
    this = callback_zoomDirect(this, 'ZoomInXY*');
    
elseif strcmp(CurrentCharacter, '-')
    % Zoon Out
    this = callback_zoomDirect(this, 'ZoomOutXY');
    
elseif strcmp(CurrentCharacter, '/')
    % Zoon Out
    this = callback_zoomDirect(this, 'ZoomOutXY/');
    
elseif strcmp(CurrentCharacter, 'q')
    % Zoon Out
    this = callback_zoomDirect(this, 'QuickLook');
    
elseif strcmp(CurrentCharacter, '1')
    % 1pixel pour 1 pixel
    this = callback_zoomDirect(this, 'Zoom1');
    
elseif (strcmp(CurrentCharacter, '2') || ...
        strcmp(CurrentCharacter, '3') || strcmp(CurrentCharacter, '4') || ...
        strcmp(CurrentCharacter, '5') || strcmp(CurrentCharacter, '6') || ...
        strcmp(CurrentCharacter, '7') || strcmp(CurrentCharacter, '8') || ...
        strcmp(CurrentCharacter, '9'))
    % Zoom facteur N
    this = callback_zoomDirect(this, 'ZoomN', 2*str2double(CurrentCharacter));
    
elseif (double(CurrentCharacter) == 28)
    % D�placement vers la gauche
    this = callback_zoomDirect(this, 'MoveLeftDemi');
    
elseif (double(CurrentCharacter) == 29)
    % D�placement vers la droite
    this = callback_zoomDirect(this, 'MoveRightDemi');
    
elseif (double(CurrentCharacter) == 30)
    % D�placement vers le haut
    this = callback_zoomDirect(this, 'MoveUpDemi');
    
elseif (double(CurrentCharacter) == 31)
    % D�placement vers le bas
    this = callback_zoomDirect(this, 'MoveDownDemi');
    
elseif strcmp(CurrentCharacter, 'T')
    % Move Top
    this = callback_zoomDirect(this, 'MoveTop');
    
elseif strcmp(CurrentCharacter, 'B')
    % Move Bottom
    this = callback_zoomDirect(this, 'MoveBottom');
    
elseif strcmp(CurrentCharacter, 'L')
    % Move Left of Image
    this = callback_zoomDirect(this, 'MoveLeftImage');
    
elseif strcmp(CurrentCharacter, 'R')
    % Move Right of Image
    this = callback_zoomDirect(this, 'MoveRightImage');
    
elseif strcmp(CurrentCharacter, '<')
    % Recherche de la valeur minimale
    [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
    if ~flag
        return
    end
    [subx, suby] = get_Image_zoomee(this);
    [~, x, y, ~, ~, iCan] = searchMinValue(this.Images(this.indImage), ...
        'subx', subx, 'suby', suby, ...
        'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
    if isempty(x)
        return
    end
    this.cross(this.indImage, :) = [x y];
    this.visuCoupeHorz = 1;
    this.visuCoupeVert = 1;
    this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
    this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
    this = displayImage(this);
    %     this = show_image(this);
    %     this = show_profils(this);
    %     this = set_cross2synchronizedImages(this);
    
elseif strcmp(CurrentCharacter, '>')
    % Recherche de la valeur maximale
    [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
    if ~flag
        return
    end
    [subx, suby] = get_Image_zoomee(this);
    [~, x, y, ~, ~, iCan] = searchMaxValue(this.Images(this.indImage), ...
        'subx', subx, 'suby', suby, ...
        'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
    if isempty(x)
        return
    end
    this.cross(this.indImage, :) = [x y];
    this.visuCoupeHorz = 1;
    this.visuCoupeVert = 1;
    this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
    this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
    this = displayImage(this);
    %     this = show_image(this);
    %     this = show_profils(this);
    %     this = set_cross2synchronizedImages(this);
    
elseif strcmp(CurrentCharacter, '=')
    % Recherche de la valeur m�diane
    [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
    if ~flag
        return
    end
    [subx, suby] = get_Image_zoomee(this);
    [~, x, y, ~, ~, iCan] = searchMedianValue(this.Images(this.indImage), ...
        'subx', subx, 'suby', suby, ...
        'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
    if isempty(x)
        return
    end
    this.cross(this.indImage, :) = [x y];
    this.visuCoupeHorz = 1;
    this.visuCoupeVert = 1;
    this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
    this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
    this = displayImage(this);
    %     this = show_image(this);
    %     this = show_profils(this);
    %     this = set_cross2synchronizedImages(this);
    
elseif (double(CurrentCharacter) == 32)
    % NaN
    this = callback_NaN(this, 'KeyPress');
    
elseif (double(CurrentCharacter) == 127)
    % Suppr
    this = callback_delete(this);
    
elseif strcmp(CurrentCharacter, '%')
    % Filtre antispike
    [flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this.Images, this.indImage, 'IncludeCurrentImage'); %#ok<ASGLU>
    if ~flag
        return
    end
    indLayerCurrent = (indLayers == this.indImage);
    xyCross  = this.cross(this.indImage, :);
    ix = xy2ij(this.Images(this.indImage), xyCross(1), xyCross(2));
    this = traiterProfilY(this, nomsLayers, indLayers, xyCross, indLayerCurrent, ix, 'Process', 13);
    this = displayImage(this);
    %     this = show_image(this);
    %     this = show_profils(this);
    %     this = set_cross2synchronizedImages(this);
    
elseif strcmp(CurrentCharacter, 'f')
    this = callback_export(this, 'EXPORT_MatlabFigure_CI1');
    
else
    %     disp(sprintf('Key "%s" not yet associated to an action. byte %d', CurrentCharacter, double(CurrentCharacter)))
end

% If faut remettre le zoom en �tat de marche ici : sans doute inutile, faut v�rifier
this = traiter_typeInteractionSouris(this);


function this = displayImage(this)
this = show_image(this);
this = show_profils(this);
this = set_cross2synchronizedImages(this);
