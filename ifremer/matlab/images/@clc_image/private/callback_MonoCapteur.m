function this = callback_MonoCapteur(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements
flag = 0;
a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg       
    case 'MonoCapteur_plot_CI0'
        [flag, X, Y, V, this.repImport] = MonoCapteur_plot(this.Images, this.repImport); %#ok<ASGLU>
        
    case 'MonoCapteur_Prelevements_plot_CI0'
        [~, this.repImport] = Pelevements_plot(this.repImport);
      
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
