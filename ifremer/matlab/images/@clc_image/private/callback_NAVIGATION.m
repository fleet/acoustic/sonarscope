function this = callback_NAVIGATION(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'NAV_PlotNavigation_CI0'
        [flag, ListeFicNvi, this.repImport] = uiSelectFiles('ExtensionFiles', {'.nvi'; '.csv'; '.txt'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
%             plot_nvi(I0, ListeFicNvi);
            plot_navigation_NVI(ListeFicNvi);
        end
        
    case 'NAV_EditNavigation_CI0'
        [flag, ListeFicNvi, this.repImport] = uiSelectFiles('ExtensionFiles', {'.nvi'; '.csv'; '.txt'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
%             plot_nvi(I0, ListeFicNvi);
            edit_navigation_NVI(ListeFicNvi);
        end

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
