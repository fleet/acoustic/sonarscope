function this = callback_NORBIT(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

I0 = cl_image_I0;

%% Lancement des traitements

a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'NORBIT_CompositMosaic_CI0'
        [flag, ListeFicErs, this.repImport, this.repExport] = params_NORBIT_CompositMosaic(I0, this.repImport, this.repExport);
        if flag
            [flag, a] = NORBIT_CompositMosaic(I0, ListeFicErs, this.repExport);
        end

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
