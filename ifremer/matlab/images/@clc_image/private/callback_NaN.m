% Traitement des deplacements du curseur
%
% Syntax
%   this = callback_NaN(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_NaN(this, varargin)

%% Détermination si le curseur est sur l'un des 3 axes

[flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);

if ~any(flag)
    return
end

%%

[varargin, KeyPress] = getFlag(varargin, 'KeyPress'); %#ok<ASGLU>

if KeyPress
    CurrentCharacter = get(gcf, 'CurrentCharacter');
    if isempty(CurrentCharacter) || double(CurrentCharacter) ~=  32
        return
    end
    EraseWindow = [1 1];
    EraseAngle = 0;
    if ~this.EreasePixelAction
        return
    end
else
    EraseWindow = this.EreaseWindow;
    EraseAngle  = this.EreaseWindowAngle;
end

%%

SelectionType = get(this.hfig, 'SelectionType');
switch SelectionType
    case 'open'
        this = traiter_click_NaN(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle);
    case 'extend'
        this = traiter_click_NaN(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle);
    case 'alt' % Clck droit
        this = traiter_click_NaN(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle);
    case 'normal'
        this = traiter_click_NaN(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle); % Click droit
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
end
