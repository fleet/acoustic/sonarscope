function this = callback_OTUS(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

I0 = cl_image_I0;
a  = cl_image.empty;

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'OTUS_Movie'
        [flag, ListeFicImage, nomFicAvi, fps, quality, this.repImport, this.repExport] ...
            = params_OTUS_Movie(this.repImport, this.repExport);
        if flag
            OTUS_Movie(I0, ListeFicImage, nomFicAvi, fps, quality);
        end
        
    case 'OTUS_CreateCompensatedImages'
        [flag, ListeFicImage, OrdrePolyCompens, nomDirOut, this.repImport, this.repExport] ...
            = params_OTUS_CreateCompensatedImages(this.repImport, this.repExport);
        if flag
            flag = OTUS_CreateCompensatedImages(I0, ListeFicImage, OrdrePolyCompens, nomDirOut);
        end

    case 'OTUS_CalculCompensation'
        [flag, ListeFicImage, nomFicCompens, SeuilStd, this.repImport] = params_OTUS_CalculCompensation(this.repImport);
        if flag
            OTUS_CalculCompensation(I0, ListeFicImage, nomFicCompens, SeuilStd);
        end
        
    case 'OTUS_CompensMeanImage'
        [flag, ListeFicImage, nomFicCompens, this.repImport] = params_OTUS_CompensMeanImage(this.repImport);
        if flag
            OTUS_CompensMeanImage(I0, ListeFicImage, nomFicCompens);
        end
       
    case 'OTUS_CreationXmlFromNMEA'
        [flag, ListeFicImage, nomFicNmea, nomFicNavCorrigee, nomFicOut, this.repImport, this.repExport] ...
            = params_OTUS_creationXmlFromNMEA(this.repImport, this.repExport);
        if flag
            OTUS_creationXmlFromNMEA(I0, ListeFicImage, nomFicNmea, nomFicNavCorrigee, nomFicOut);
        end
        
%     case 'OTUS_CreationXml' % Conserv� pour le moment au cas o� !
%         [flag, ListeFicImage, nomFicNav, nomFicExcel, this.repImport] = params_OTUS_creationXml(this.repImport);
%         if flag
%             OTUS_creationXml(I0, ListeFicImage, nomFicNav, nomFicExcel);
%         end
        
    case 'OTUS_PlotNavigation'
        [flag, nomFicNav, this.repImport] = params_OTUS_plotNav(this.repImport);
        if flag
            OTUS_plotNav(nomFicNav);
        end
        
    case 'OTUS_PlotNavigationAndImages'
        [flag, nomFicNav, listeFicImages, decalageHoraire, this.repImport] ...
            = params_OTUS_plotNavAndImages(this.repImport);
        if flag
            OTUS_plotNavAndImages(nomFicNav, listeFicImages, decalageHoraire);
        end
        
    case 'OTUS_PlotNavAndImagesInGoogleEarth'
        [flag, nomFicNav, listeFicImages, decalageHoraire, DisplayFullFile, KmzFilename, ...
            this.repImport] = params_OTUS_PlotNavAndImagesInGoogleEarth(this.repImport);
        if flag
            OTUS_plotNavAndImagesInGoogleEarth(nomFicNav, listeFicImages, decalageHoraire, ...
                KmzFilename, DisplayFullFile);
        end
        
    case 'OTUS_MosaiquesIndividuelles'
        [flag, ListeFicImage, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nomFicXMLPosition, ...
            nomDirImagesCompensee, HeightMax, this.repImport, this.repExport] ...
            = params_OTUS_MosaiquesIndividuelles(this.repImport, this.repExport);
        if flag
            OTUS_MosaiquesIndividuelles(I0, ListeFicImage, nomFicXMLPosition, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nomDirImagesCompensee, ...
                'HeightMax', HeightMax);
        end
       
    case 'OTUS_ExportRawImages3DV'
        [flag, ListeFicImage, nomFicXML, Carto, ~, this.repImport, this.repExport] ...
            = params_OTUS_ExportRawImages3DV(this.repImport, this.repExport);
        if flag
            OTUS_ExportRawImages3DV(I0, ListeFicImage, nomFicXML, Carto);
        end
        
    case 'OTUS_Mosaic'
        [flag, ListeFicImage, resol, MosaicName, nomRepOutDales, nomFicCompens, Carto, TypeCompensation, ...
            nbMaxPixelsWidthTile, nbMaxPixelsHeightTile, nomFicXMLPosition, nomDirImagesCompensee, Capteur, ...
            HeightMax, NomRacine, ext, this.repImport, this.repExport] = params_OTUS_Mosaic(this.repImport, this.repExport, 'Capteur', 'OTUS');
        if flag
            OTUS_Mosaic(I0, ListeFicImage, nomFicXMLPosition, resol, MosaicName, nomRepOutDales, nomFicCompens, Carto, TypeCompensation, ...
                nomDirImagesCompensee, Capteur, NomRacine, ext, 'HeightMax', HeightMax, ...
                'nbMaxPixelsWidthTile', nbMaxPixelsWidthTile, 'nbMaxPixelsHeightTile', nbMaxPixelsHeightTile);
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
