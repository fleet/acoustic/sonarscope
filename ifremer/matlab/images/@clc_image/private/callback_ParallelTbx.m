function this = callback_ParallelTbx(this, msg)

global useParallel %#ok<GVMIS>

if nargin == 1
    return
end

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'ParallelTbxOn'
        [flag, X] = question_NumberOfWorkers;
        if ~flag
            return
        end
        useParallel = X;
        
    case 'ParallelTbxOnMax'
        X = getNumberOfWorkers;
        useParallel = X;
        
    case 'ParallelTbxOff'
        useParallel = 0;
end
this = gerer_menus(this);
