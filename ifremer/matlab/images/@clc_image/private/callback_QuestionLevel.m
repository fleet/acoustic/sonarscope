function this = callback_QuestionLevel(this, msg)

if nargin == 1
    return
end

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case {'QL1'; '1'}
        icone = get_icone(this, 'QL1');
        this.ihm.QuestionLevel.cpn = set_CData(this.ihm.QuestionLevel.cpn, IconeEncocheDroite(icone));
        this = selectLevelQuestions(this, '1');
        this = gerer_menus(this); % TODO : supprimer menu
        
    case {'QL2'; '2'}
        icone = get_icone(this, 'QL2');
        this.ihm.QuestionLevel.cpn = set_CData(this.ihm.QuestionLevel.cpn, IconeEncocheDroite(icone));
        this = selectLevelQuestions(this, '2');
        this = gerer_menus(this);
        
    case {'QL3'; '3'}
        icone = get_icone(this, 'QL3');
        this.ihm.QuestionLevel.cpn = set_CData(this.ihm.QuestionLevel.cpn, IconeEncocheDroite(icone));
        this = selectLevelQuestions(this, '3');
        this = gerer_menus(this);
end
