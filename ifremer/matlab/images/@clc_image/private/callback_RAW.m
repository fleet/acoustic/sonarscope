function this = callback_RAW(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

E0 = cl_ExRaw([]);
a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'RAW_ConvertSsc2Hac_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            this.repImport = lastDir;
            ExRaw_ssc2hac('NomFicRaw', listFileNames, 'RepDefaut', this.repImport);
        end

    case 'RAW_Preprocessing_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            preprocess_ExRaw(E0, listFileNames);
        end
        
    case 'RAW_PlotNavigationAndTime_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            plot_nav_ExRaw(E0, listFileNames);
        end
        
    case 'RAW_PlotNavigationAndSignal_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomSignal] = params_RAW_PlotNavigationAndSignal;
            if flag
                [flag, Fig, Carto] = plot_nav_ExRaw(E0, listFileNames);
                if flag
                    plot_position_data(E0, Fig, listFileNames, nomSignal, 'Carto', Carto);
                end
            end
        end
 
    case 'RAW_ImportNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav, this.repImport] = SSc.Params.ImportNavigation({'.dat'; '.nvi'; '.csv'}, this.repImport);
            if flag
                ExRaw_ImportNavigation(E0, listFileNames, nomFicNav);
            end
        end
        
	case 'RAW_EditNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = RAW_EditNav(E0, listFileNames);
        end
        
    case 'RAW_Export3DViewer_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, HauteurMin, SuppressionOverHeight, ...
                identLayer, listFreq, TailleMax, MeanTide, typeOutput, this.repExport] ...
                = params_ExRaw_Export3DViewer(this.Images(this.indImage), listFileNames, this.repExport);
            if flag
                if typeOutput == 1 % XMLBin
                    flag = survey_Export3DViewer(E0, listFileNames, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, ...
                        HauteurMin, SuppressionOverHeight, identLayer, TailleMax, MeanTide, 'listFreq', listFreq);
                else % Netcdf
                    flag = survey_ExportGlobeFlyTexture(E0, listFileNames, nomDirOut, Tag, ...
                        HauteurMin, SuppressionOverHeight, identLayer, MeanTide, 'listFreq', listFreq);
                end
            end
        end
        
    case 'RAW_CompressDirectories_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            compressDirectories_SSc(listFileNames, 'Tag', 'ExRaw');
        end
        
    case 'RAW_DeleteCache_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            eraseSonarScopeCache(listFileNames);
        end        
        
    case 'RAW_CleanSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, identFreq, identData] = selectionLayers(E0, listFileNames{1}, ...
                'SelectionModeFreq', 'Single', 'SelectionModeDataType', 'Single');
            if flag
                ExRaw_CleanSignals(E0, listFileNames, 'identFreq', identFreq, 'identData', identData);
            end
        end
        
    case 'RAW_TideImport_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicTide, this.repImport] = SSc.Params.ImportTide(this.repImport);
            if flag
                ExRaw_TideImport(E0, listFileNames, nomFicTide);
            end
        end
                
    case 'RAW_ExtractSeabedBS_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicSvp] = survey_uiSelectFiles(this, 'ExtensionFiles', '.asvp', 'RepDefaut', this.repImport);
%           [flag, this, nomFicSvp] = getListFiles(this, 'FileExt', '.asvp'); % Commenté car pas de remise à jour de this.repImport
            if flag
                ExRaw_ExtractSeabedBS(E0, listFileNames, nomFicSvp);
            end
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end




%% getListFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.raw');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.raw', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['SBES ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
