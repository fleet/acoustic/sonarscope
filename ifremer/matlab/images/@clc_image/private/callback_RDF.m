function this = callback_RDF(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

R0 = cl_rdf([]);
a  = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'RDF_Preprocessing_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            readOnce_RDF(R0, listFileNames);
        end
        
    case 'RDF_PreprocessingStep1_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repImport] = preprocess_RDF(R0, listFileNames, 'Step1', 'repImport', this.repImport);
        end

    case 'RDF_PreprocessingStep2_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repImport] = preprocess_RDF(R0, listFileNames, 'Step2', 'repImport', this.repImport);
        end

    case 'RDF_PreprocessingStep3_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repImport] = preprocess_RDF(R0, listFileNames, 'Step3', 'repImport', this.repImport);
        end

    case 'RDF_PreprocessingStep4_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repImport] = preprocess_RDF(R0, listFileNames, 'Step4', 'repImport', this.repImport);
        end
        
    case 'RDF_PlotNavigationAndTime_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            plot_nav_RDF(R0, listFileNames);
        end
        
    case 'RDF_PlotNavigationAndCoverage_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [Fig, Carto] = plot_nav_RDF(R0, listFileNames);
            plot_coverage(R0, listFileNames, 'Fig', Fig, 'Carto', Carto)
        end
        
    case 'RDF_PlotNavigationAndSignal_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignal, InstallationParameters] = params_RDF_NavAndSignal(this.repImport);
            if flag
                [Fig, Carto] = plot_nav_RDF(R0, listFileNames);
                plot_position_data(R0, Fig, listFileNames, NomSignal, 'Carto', Carto, ...
                    'InstallationParameters', InstallationParameters);
            end
        end

    case 'RDF_ImportNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav, InstallationParameters] = params_RDF_ImportNav(this.repImport);
            if flag
                import_external_nav(R0, listFileNames, nomFicNav, InstallationParameters);
            end
        end
        
    case 'RDF_PingSamples2LatLong_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repImport, this.repExport] = RDF_PingSamples2LatLong('nomDirImport', this.repImport, ...
                'nomDirExport', this.repExport, 'XLim', XLim, 'YLim', YLim);
        end
        
    case 'RDF_Mosaiques_Individuelles_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, gridSize, CorFile, ModeDependant, selectionData, Window, MosHeading, skipExistingMosaic, ...
                this.repImport, this.repExport] = params_RDF_IndividualMosaics(this.repImport, this.repExport);
            if flag
                RDF_MosaiqueParProfil(this.repExport, listFileNames, gridSize, CorFile, ModeDependant, ...
                    selectionData, Window, MosHeading, skipExistingMosaic);
            end
        end

    case 'RDF_ResetMask_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            resetMask_RDF(R0, listFileNames);
        end

    case 'RDF_SummaryDatagrams_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicSummary, this.repExport] = params_RDF_SummaryDatagrams(this.repExport);
            if flag
                RDF_SummaryDatagrams(R0, listFileNames, nomFicSummary);
            end
        end

    case 'RDF_SummaryLines_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicSummary, listeItems, this.repExport] = params_RDF_SummaryLines(this.repExport);
            if flag
                RDF_SummaryLines(R0, listFileNames, nomFicSummary, listeItems);
            end
        end
        
    case 'RDF_CompressDirectories_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            compressDirectories_SSc(listFileNames, 'Tag', 'RDF');
        end

    case 'RDF_DeleteCache_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            eraseSonarScopeCache(listFileNames);
        end
        
    case 'RDF_DefineInstallationParameters_CI0'
        [flag, nomFicXml, this.repImport] = params_RDFDefineInstallationParameters(R0, this.repImport);
        if flag
            [flag, Geoswath] = RDF_DefineInstallationParameters([], nomFicXml, 'FileNameAlreadyDefined', 1); %#ok<ASGLU>
        end
        
    case 'RDF_RtkReset_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            RDF_RTK_Raz(R0, listFileNames);
        end        
        
    case 'RDF_RtkPlot_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            RDF_RTK_Plot(R0, listFileNames);
        end        
        
    case 'RDF_RtkClean_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            RDF_RTK_Clean(R0, listFileNames);
        end 
        
    case 'RDF_RtkComputeTide_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            RDF_RTK_ComputeTide(R0, listFileNames);
            RDF_RTK_Plot(R0, listFileNames);
        end
        
    case 'RDF_RtkEditTide_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            RDF_RTK_EditTide(R0, listFileNames);
            RDF_RTK_Plot(R0, listFileNames);
        end
        
    case 'RDF_RtkCreateTide_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nonFicTideIn, nomFicTideOut, this.repImport] = params_RTK_CreateTide(this.repImport);
            if flag
                RDF_RTK_CreateTide(R0, listFileNames, nonFicTideIn, nomFicTideOut);
            end
        end
        
    case 'RDF_MosaiquesSidescan_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repImport, this.repExport] = RDF_PingSamples2LatLong_sidescan('nomDirImport', this.repImport, ...
                'nomDirExport', this.repExport, 'XLim', XLim, 'YLim', YLim);
        end
        
    case 'RDF_BackupSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignal] = params_RDF_BackupSignals;
            if flag
                BackupSignals(R0, listFileNames, NomSignal);
            end
        end
        
    case 'RDF_RestoreSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignal] = params_RDF_BackupSignals;
            if flag
                RestoreSignals(R0, listFileNames, NomSignal);
            end
        end
    
    case 'RDF_Cache1_CI0' % 'Convert XML-Bin to Netcdf'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            RDF_Cache2Netcdf(listFileNames);
        end
        
    case 'RDF_Cache2_CI0' % 'Delete XML-Bin'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SScCacheXMLBinUtils.deleteCachDirectory(listFileNames);
        end
        
    case 'RDF_Cache3_CI0' % 'Convert XML-Bin to Netcdf & delete XML-Bin')
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [ind, flag] = my_questdlg('Redo Netcdf files if already exist ?', 'Init', 2);
            if flag
                RDF_Cache2Netcdf(listFileNames, 'Redo', ind == 1, 'deleteXMLBin', true);
            end
        end
               
%     case 'RDF_Cache5' % Not used
%         [flag, this, listFileNames] = getListFiles(this);
%         if flag
%             RDF_testDeflateLevel(listFileNames);
%         end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end






%% getListAllFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.rdf');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.rdf', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['Geoswath ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
