function this = callback_REMUS(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

flag = 0;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'REMUS_ConvertFileFormat'
        [flag, this, listFileNames] = getListFiles(this, 'FileExt', '.csv');
        if flag
            flag = Remus2Ers(listFileNames);
        end
        
     case 'REMUS_Export3DViever'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomRepOut, Tag, this.repExport] = params_ERS_Export23Dviewer(this.repExport);
            if flag
                flag = ERS_Export23DViewer(I0, listFileNames, nomRepOut, Tag);
            end
        end
        
    case 'REMUS_ExportGoogleEarth'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomRepOut, TypeOmbrage, Azimuth, VertExag, Tag, skipExistingFile, this.repExport] ...
                = params_ERS_Export2GoogleEarth(this.repExport);
            if flag
                flag = ERS_Export2GoogleEarth(I0, listFileNames, nomRepOut, Tag, TypeOmbrage, Azimuth, VertExag, skipExistingFile);
            end
        end

    case 'REMUS_ExportCaraibes'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomRepOut, Tag, this.repExport] = params_ERS_Export23Dviewer(this.repExport);
            if flag
                flag = ERS_Export2Caraibes(I0, listFileNames, nomRepOut, Tag);
            end
        end
          
    case 'REMUS_PlotNavTime'
        this.repImport = process_SonarErMapper(this.Images(this.indImage), 'NavTime', this.repImport);   
      
    case 'REMUS_PlotNavSignal'
        this.repImport = process_SonarErMapper(this.Images(this.indImage), 'NavSignal', this.repImport);   
                
    case 'REMUS_ImportNavigation'
        [flag, NomFicNav, listFileNames, FiltreNav, OrdreButter, Wc, CalculHeading, this.repImport] ...
            = GetParamsErMapperImportNav(this.repImport);
        if flag
            flag = process_ErMapperImportNav(listFileNames, NomFicNav, FiltreNav, OrdreButter, Wc, CalculHeading); 
        end
        
    case 'REMUS_PingRange2PingAcrossDist'
        [~, this.repImport, this.repExport] = ErMapperSL_PingRange2PingAcrossDist(this.repImport, this.repExport);
        
    case 'REMUS_PingAcrossDist2LatLong'
        [flag, a, this.repExport] = ErMapperSL_PingAcrossDist2LatLong('NomDir', this.repExport);

    case 'REMUS_MBES_PingBeam2LatLong'
        [flag, a, this.repExport] = ErMapperMBES_PingBeam2LatLong('NomDir', this.repExport);
        
    case 'REMUS_CleanSignals'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.ers'; '.xml'}, 'RepDefaut', this.repImport, ...
            'ChaineExclue', 'Carto');
        if flag
            flag = Remus_CleanSignals(nomFic);
        end
        
    case 'REMUS_SignalFiltre'
        [~, this.repExport] = ErMapper_SignalFiltre('NomDir', this.repExport);
       
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end






%% getListAllFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.ers');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.ers', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['REMUS ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
