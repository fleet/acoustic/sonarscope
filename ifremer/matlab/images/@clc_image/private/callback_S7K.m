function this = callback_S7K(this, msg, subx, suby, flagImageEntiere)

if isempty(msg)
    MessageTraitementDroite
    return
end

if flagImageEntiere
    suby = [];
end

%% Quelques initialisations

a  = cl_image.empty;
S0 = cl_reson_s7k.empty();

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'S7K_PlotNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, S7k] = checkFiles(S0, listFileNames, 'KeepNcID', true);
            if flag
                plot_nav_S7K(S7k);
            end
        end
                
    case 'S7K_PlotNavigationAndSignal_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, signalNames, Mute, modePlotData] = params_plotNavSignal(getResonListSignals);
            if flag
                [flag, S7k] = checkFiles(S0, listFileNames, 'KeepNcID', true);
                if flag
                    plotNavSignal(S7k, signalNames, 'Mute', Mute, 'modePlotData', modePlotData);
                end
            end
        end
        
    case 'S7K_PlotNavigationAndCoverage_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, S7k] = checkFiles(S0, listFileNames, 'KeepNcID', true);
            if flag
                Fig = plot_nav_S7K(S7k, 'Title', 'Position and coverage');
                plot_coverage(S7k, 'Fig', Fig);
            end
        end
        
    case 'S7K_EditNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = S7K_EditNav(S0, listFileNames);
        end
        
        % Sous-Menu TransGeometry
    case 'S7K_PingBeam2LatLong_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repImport, this.repExport] = Reson_PingBeam2LatLong(this.repImport, this.repExport, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'S7K_PingSamples2LatLong_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repExport] = Reson_PingSamples2LatLong('NomDir', this.repExport, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'S7K_PingSnippets2PingSamples_CI1'
        [flag, indAngle] = params_S7K_PingSnippets2PingSamples(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_PingSnippets2PingSamples(this.Images(this.indImage), ...
                this.Images(indAngle), 'subx', subx, 'suby', suby);
        end
        
        % Sous-Menu Utilities
    case 'S7K_Preprocessing_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            prepare_lectures7k_step1(listFileNames);
        end
        
    case 'S7K_Summary_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicCsv, this.repExport] = params_S7K_Summary('SummaryLines.csv', this.repExport);
            if flag
                [flag, S7k] = checkFiles(S0, listFileNames, 'KeepNcID', true);
                if flag
                    S7K_Summary_Lines(S7k, nomFicCsv);
                end
            end
        end
        
    case 'S7K_CleanSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            S7K_CleanSignals(S0, listFileNames);
        end
        
    case 'S7K_PatchTestRoll_CI0'
        [flag, listFileNames, ~, this.repImport] = params_PatchTestRoll('.s7k', this.repImport);
        if flag
            S7K_PatchTestRoll(S0, listFileNames);
        end
         
    case 'S7K_Statistics_BathyMNT_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, CleanCurves, TideFile, flagVsLayers, flagDisplay, this.repImport] ...
                = params_S7K_StatsBathyMinusDTM(this.Images(this.indImage), this.repImport);
            if flag
                flag = S7K_StatsBathyMinusDTM(this.Images(this.indImage), listFileNames, CleanCurves, TideFile, flagVsLayers, flagDisplay);
            end
        end
       
    case 'S7K_CleanBathyAndRange_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            S7K_CleanBathyAndRange(listFileNames);
        end
        
    case 'S7K_CleanDetectionType_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            S7K_CleanDetectionType(S0, listFileNames);
        end
        
    case 'S7K_CleanBathy_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            S7K_CleanBathy(S0, listFileNames);
        end
        
    case 'S7K_CleanFromDTMInteractif_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
%             [flag, flagTideCorrection] = params_SonarCleanBathyFromDTM(this.Images(this.indImage));
%             if flag
                flag = sonar_S7K_CleanFromDTM(this.Images(this.indImage), listFileNames);
%             end
        end
        
    case 'S7K_CleanFromDTMThreshold_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, Threshold, MaskIfNoDTM, TideFile] = params_SonarCleanBathyFromDTM_Threshold(this.Images(this.indImage));
            if flag
                flag = sonar_S7K_CleanFromDTM_Threshold(this.Images(this.indImage), listFileNames, Threshold, MaskIfNoDTM, TideFile);
            end
        end
        
    case 'S7K_CleanReflectivityMeanPerPing_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = params_CleanReflectivity;
            if flag
                flag = sonar_S7K_CleanReflectivity(S0, listFileNames);
            end
        end

    case 'S7K_CleanReflectivityRAZ_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            resetMask_S7KPingsReflec(S0, listFileNames);
        end
                
    case 'S7K_CheckNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            checkNav(S0, listFileNames);
        end
        
    case 'S7K_SplitFiles_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nbPingMax] = params_split_resonS7k_files(S0);
            if flag
                split_resonS7k_files(S0, listFileNames, nbPingMax)
            end
        end
        
    case 'S7K_SplitFrequencies_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            S7K_SplitFrequencies(S0, listFileNames)
        end
        
    case 'S7K_AllMergeFiles_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            S7K_MergeFiles(S0, listFileNames)
        end
        
    case 'S7K_ExportCaraibesNvi_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [this.repImport, this.repExport] = process_ResonS7k(listFileNames, 'ExportCaraibesNvi', this.repImport, this.repExport); % TODO : ramener traitement ici
        end
        
    case 'S7K_ExportAttitude_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [this.repImport, this.repExport] = process_ResonS7k(listFileNames, 'ExportAttitude', this.repImport, this.repExport); % TODO : ramener traitement ici
        end
        
    case 'S7K_ImportCaraibesNvi_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav] = uiSelectFiles('ExtensionFiles', {'.nvi'; '.txt'}, 'AllFilters', 1, 'RepDefaut', this.repImport);
            if flag
                process_S7K_ImportNavigation(S0, listFileNames, nomFicNav);
            end
        end
        
%     case 'S7K_DeleteCache_CI0' % Pas appel�
%         [flag, this, listFileNames] = getListFiles(this);
%         if flag
%             eraseSonarScopeCache(listFileNames);
%         end
        
        % Sous-Menu OtherDisplay
    case 'S7K_Visu_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            Option = S7K_listSignals(S0);
            [flag, rep] = question_SelectASignal(Option);
            if ~flag
                return
            end
            preprocessS7kFiles(listFileNames, 'Tasks', rep);
        end
        
        % Sous-Menu Water-Column
    case 'S7K_WC_ProcessEchograms_CI1'
        [flag, this, listFileNames, sameFile, subx, suby] = getListFiles(this);
        if flag
            TypeWCOutput = 1;
            [flag, listFileNames, Names, StepCompute, StepDisplay, nomFicNav, nomFicIP, OrigineWC, ...
                ChoixNatDecibel, CLimRaw, CLimComp, subBeams, subPings, XLim, YLim, TagName, ...
                MaskBeyondDetection, this.repImport, this.repExport, typeAlgoWC, MaxImageSize, Carto, ...
                skipAlreadyProcessedFiles, typeOutput] =  ...
                params_ResonS7k_Echograms(this.Images(this.indImage), TypeWCOutput, ...
                listFileNames, sameFile, subx, suby, this.repImport, this.repExport);
            if flag
                if typeOutput == 1 % XML-Bin
                    flag = process_ResonS7k_Echograms(listFileNames, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
                        OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
                        'subBeams', subBeams, 'subPings', subPings, ...
                        'StepCompute', StepCompute, 'StepDisplay', StepDisplay, ...
                        'XLim', XLim, 'YLim', YLim, ...
                        'typeAlgoWC', typeAlgoWC, 'MaxImageSize', MaxImageSize, ...
                        'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, 'Carto', Carto);
                else % GLOBE
                     flag = process_ResonS7k_EchogramsGlobeFlyTexture(listFileNames, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
                        OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
                        'subBeams', subBeams, 'subPings', subPings, ...
                        'StepCompute', StepCompute, 'StepDisplay', StepDisplay, ...
                        'XLim', XLim, 'YLim', YLim, ...
                        'typeAlgoWC', typeAlgoWC, 'MaxImageSize', MaxImageSize, ...
                        'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, 'Carto', Carto);
                end
            end
        end
        
    case 'S7K_WC_ProcessEchogramsPhase_CI1'
        TypeWCOutput = 1;
        [flag, this, listFileNames, sameFile, subx, suby] = getListFiles(this);
        if flag
            [flag, listFileNames, Names, this.repImport, this.repExport, StepCompute, StepDisplay, nomFicNav, nomFicIP, ...
                CLim, subBeams, subPings, XLim, YLim, TagName, ...
                MaskBeyondDetection, MaxImageSize, Carto, skipAlreadyProcessedFiles] = ...
                params_ResonS7k_EchogramsPhase(this.Images(this.indImage), TypeWCOutput, ...
                listFileNames, sameFile, subx, suby, this.repImport, this.repExport);
            if flag
                [flag, statusDepthAndAcrossDistLimits] = params_ResonS7k_MatrixStatusDepthAndAcrossDistLimits();
                if flag
                    % TODO : StepCompute
                    flag = process_ResonS7k_EchogramsPhase(listFileNames, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
                        StepCompute, StepDisplay, CLim, TagName, MaskBeyondDetection, ...
                        'subBeams', subBeams, 'subPings', subPings, 'XLim', XLim, 'YLim', YLim, ...
                        'MaxImageSize', MaxImageSize, ...
                        'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, 'Carto', Carto, ...
                        'statusDepthAndAcrossDistLimits', statusDepthAndAcrossDistLimits);
                end
            end
        end
        
    case 'S7K_WC_ExportWCSidescan3DViewer_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, Side, NomDirXML, TailleMax, Tag, ColormapIndex, Video, CLim, this.repExport] = params_S7K_WC_Export3DViewer(this.repExport);
            if flag
                flag = S7K_WCExport3DViewer(listFileNames, NomDirXML, Tag, CLim, TailleMax, Side, ColormapIndex, Video);
            end
        end
        
    case 'S7K_WC_ProcessEchogramsMatrix_CI1'
        [flag, this, listFileNames, sameFile, subx, suby] = getListFiles(this);
        if flag
            TypeWCOutput = 2; % Matrices3D
            [flag, listFileNames, Names, StepCompute, StepDisplay, nomFicNav, nomFicIP, OrigineWC, ...
                ChoixNatDecibel, CLimRaw, CLimComp, subBeams, subPings, XLim, YLim, TagName, ...
                MaskBeyondDetection, this.repImport, this.repExport, typeAlgoWC, MaxImageSize, Carto, ...
                skipAlreadyProcessedFiles, TypeFileWCOutput] = ...
                params_ResonS7k_Echograms(this.Images(this.indImage), TypeWCOutput, listFileNames, sameFile, subx, suby, this.repImport, this.repExport);
            if flag
                [flag, statusDepthAndAcrossDistLimits] = params_ResonS7k_MatrixStatusDepthAndAcrossDistLimits();
                if flag
                    % TODO : StepCompute
                    flag = process_ResonS7k_Echograms(listFileNames, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
                        OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
                        'subBeams', subBeams, 'subPings', subPings, ...
                        'StepCompute', StepCompute, 'StepDisplay', StepDisplay, ...
                        'XLim', XLim, 'YLim', YLim, ...
                        'typeAlgoWC', typeAlgoWC, 'MaxImageSize', MaxImageSize, ...
                        'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, 'Carto', Carto, ...
                        'statusDepthAndAcrossDistLimits', statusDepthAndAcrossDistLimits);
                    if flag
                        if TypeFileWCOutput == 2
                            flag = WC_3DMatrix_XML2Netcdf(Names);
                            if flag
                                flag = WC_3DMatrix_RemoveXMLBin(Names);
                            end
                        end
                    end
                end
            end
        end
        
        %{
        % Traitement sp�cifique donn�es USAN � bord du PP?
    case 'S7K_ComputePositionGazPlumes_CI0'
        [flag, nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto, this.repImport] = params_ResonCalculCoordPourTotal(this.repImport);
        if flag
            flag = CalculCoordPourTotal(S0, nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto);
        end
        %}
        
        % Sous-Menu R&D
    case 'S7K_RnD_BottomDetectionSynthese_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, DisplayLevel, nomFicAvi, GateDepth, nomDirErs, IdentAlgo, skipAlreadyProcessedFiles, ...
                this.repImport, this.repExport] = params_S7K_DetectionPhaseAndAmplitude(listFileNames, this.repImport, this.repExport);
            if flag
                flag = sonar_SurveyDetectionPhaseAndAmplitude(listFileNames, DisplayLevel, nomFicAvi, GateDepth, nomDirErs, IdentAlgo, skipAlreadyProcessedFiles);
            end
        end
        
    case 'S7K_RnD_CompleteFilesWithPDS_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = sonar_SurveyFusionFichiers(listFileNames);
        end
        
    case 'S7K_CreationSidescan_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, this.repExport] = params_S7K_CreationSidescan(this.repExport);
            if flag
                [flag, a] = sonar_S7KCreationSidescan(listFileNames, this.repExport);
            end
        end
        
    case 'S7K_CompareResultsBDA_CI0'
        [flag, nomDirMatlab, nomDirC, this.repImport, this.repExport] = params_CompareResultsBDA(this.repImport, this.repExport);
        if flag
            flag = sonar_CompareResultsBDA(nomDirMatlab, nomDirC);
        end
        
    case 'S7K_WC_EchoIntegration_CI1'
        [flag, ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, ...
            params_ZoneEI, Tag, Display, this.repImport, this.repExport]...
            = params_WaterColumnEchoIntegration(1, this.repImport, this.repExport, ...
            'ChaineIncluse', '_Raw_PolarEchograms');
        if flag
            process_S7K_WaterColumnEchoIntegration(S0, ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, ...
                params_ZoneEI, 'Tag', Tag, 'Display', Display, ...
                'suby', suby);
        end
        
    case 'S7K_WC_ConvertSsc2Hac_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = S7K_ssc2hac('NomFicAll', listFileNames, 'RepDefaut', this.repImport);
        end
        
    case 'S7K_WC_EchoIntegrationOverSeabed_CI1'
        [flag, ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, ...
            params_ZoneEI, Tag, Display, this.repImport, this.repExport] ...
            = params_WaterColumnEchoIntegration(2, this.repImport, this.repExport, ...
            'ChaineIncluse', '_Comp_PolarEchograms');
        if flag
            process_S7K_WaterColumnEchoIntegration(S0, ListeFicXmlEchogram, listFileNames, nomDirOut, typePolarEchogram, ...
                params_ZoneEI, 'Tag', Tag, 'Display', Display, ...
                'suby', suby);
        end
        
    case 'S7K_WC_EI_PingAcrossDist_SignalVsPings_CI0'
        [flag, ListeFicEI, DataName, TypeUnit, ~, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag, ~, this.repImport] ...
            = params_SimradAll_WC_PlotNav_EI_SSc(this.repImport);
        if flag
            WC_EI_PingAcrossDist_SignalVsPings(ListeFicEI, DataName, TypeUnit, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag);
        end
        
    case 'S7K_WC_Mosaic_EI_SSc_CI0'
        [flag, listFileNames, Resol, Window, Backup, InfoMos, CorFile, ModeDependant, ...
            LimitAngles, listeMode, listeSwath, listeFM, this.repImport, this.repExport] ...
            = params_SimradAll_WC_Mosaic_EI_SSc(this.repImport, this.repExport);
        if flag
            [flag, a] = PingAcrossDist2LatLong(listFileNames, Resol, Backup, Window, InfoMos, ...
                'CorFile', CorFile, 'ModeDependant', ModeDependant, 'LimitAngles', LimitAngles, ...
                'listeMode', listeMode, 'listeSwath', listeSwath, 'listeFM', listeFM);
        end
        
    case 'S7K_WC_EI_MosFromMovies_CI0'
        [flag, this.repImport, this.repExport, listFileNames, Resol, window, iSlice, Backup, nomFicErMapperLayer, ...
            nomFicErMapperAngle] = WCD.Params.LaurentEchoIntegration(this.repImport, this.repExport);
        if flag
            [flag, a] = Laurent_PingBeam2LatLong(listFileNames, Resol, Backup, window, iSlice, nomFicErMapperLayer, nomFicErMapperAngle);
        end
        
    case 'S7K_ImportBathyFromCaraibes_CI0'
        [flag, this, listFileNames, listeFicMBG] = params_sonarImportBathyFromCaraibesMBG(this, '.s7k');
        if flag
            sonar_importBathyFromCaraibesMBG_S7K(S0, listFileNames, listeFicMBG);
        end
        
    case 'S7K_ResetMask_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            resetMask_S7K(S0, listFileNames);
        end
        
    case 'S7K_ImportBathyFromCaris_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicTxt] = params_ImportBathyFromCaris(listFileNames);
            if flag
                sonar_importBathyFromCaris_S7K(S0, listFileNames, nomFicTxt);
            end
        end
        
    case 'S7K_ImportBathyFromMBSystem_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            sonar_importBathyFromMbSystem_S7K(S0, listFileNames);
        end
        
    case 'S7K_MosaiquesIndividuelles_CI0'
        % TODO : Remplacer params_SimradAll_IndividualMosaics par params_MBES_IndividualMosaics
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, gridSize, CorFile, ModeDependant, selectionData, Window, ...
                skipExistingMosaic, flagTideCorrection, ~, ~, this.repImport, this.repExport] ...
                = params_S7K_IndividualMosaics(this.Images(this.indImage), this.repImport, this.repExport);
            if flag
                
                % TODO : g�rer la mar�e comme pour les .all
                if flagTideCorrection
                    ListeExension = {'*.mar;*.tide;*.tid;*.txt', 'MATLAB Files (*.mar,*.tide,*.tid,*.txt)';
                        '*.mar',  'Tide file (*.mar)'; ...
                        '*.tide', 'Tide file (*.tide)'; ...
                        '*.tid', 'Tide file (*.tid)'; ...
                        '*.txt', 'Tide file (*.txt)'; ...
                        '*.*',  'All Files (*.*)'};
                    [flag, TideFile] = my_uigetfile(ListeExension, 'Give a tide file (cancel instead)', repImport);
                    if ~flag
                        TideFile = [];
                    end
                else
                    TideFile = [];
                end
                ResonMosaiqueParProfil(this.repExport, listFileNames, gridSize, CorFile, ModeDependant, selectionData, Window, skipExistingMosaic, ...
                    'TideFile', TideFile);
            end
        end
        
    case 'S7K_MosaiquesComputeBathy_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nbPlotPerImage] = params_S7K_ComputeBathy;
            if flag
                ResonComputeBathy(listFileNames, nbPlotPerImage);
            end
        end
        
    case 'S7K_MosaiquesNewNavFromDrift_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            ResonComputeNewNavFromDrift(listFileNames);
        end
        
    case 'S7K_SurveyReport_Create_CI0'
        [flag, nomFicAdoc, WebSiteName, LogFileName, this.repImport] = Adoc.Params.SurveyReport_Create(this.repImport);
        if flag
            AdocUtils.createSurveyReportFramework(nomFicAdoc, 'WebSiteName', WebSiteName, 'LogFileName', LogFileName);
            this.repExport = this.repImport;
        end
        
    case 'S7K_SurveyReport_Summary_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicAdoc, this.repExport] = params_S7K_SurveyReport_Summary(this.repExport);
            if flag
                [flag, S7k] = checkFiles(S0, listFileNames, 'KeepNcID', true);
                if flag
                    S7K_SurveyReport_Summary(S7k, nomFicAdoc)
                end
            end
        end
   
    case 'S7K_SurveyReport_Navigation_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicAdoc, ListSignals, this.repExport] = params_S7K_SurveyReport_SummaryNavigation(S0, this.repExport);
            if flag
                [flag, S7k] = checkFiles(S0, listFileNames, 'KeepNcID', true);
                if flag
                    S7K_SurveyReport_SummaryNavigation(S7k, nomFicAdoc, ListSignals)
                end
            end
        end
        
    case 'S7K_SurveyReport_SummaryAndNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            [flag, nomFicAdoc, ListSignals, this.repExport] = params_S7K_SurveyReport_SummaryNavigation(S0, this.repExport);
            if flag
                [flag, S7k] = checkFiles(S0, listFileNames, 'KeepNcID', true);
                if flag
                    S7K_SurveyReport_Summary(S7k, nomFicAdoc, 'OpenAdoc', 0)
                    S7K_SurveyReport_SummaryNavigation(S7k, nomFicAdoc, ListSignals)
                end
            end
        end
        
    case 'S7K_SurveyReport_GlobalMosaic_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicAdoc, gridSize, InfoCompensationCurveReflectivity, AreaName, this.repImport] = Adoc.Params.SurveyReport_GlobalMosaic(this.repImport);
            if flag
                S7K_SurveyReport_GlobalMosaic(S0, nomFicAdoc, AreaName, listFileNames, gridSize, InfoCompensationCurveReflectivity)
            end
        end
        
    case 'S7K_SurveyReport_AddFigures_CI0'
        [flag, nomFicAdoc, Title, Comment, hFig, this.repExport] = Adoc.Params.SurveyReport_AddFigures(this.repExport);
        if flag
            AdocUtils.addSomeDisplayedFigures(nomFicAdoc, Title, Comment, hFig);
        end
        
    case 'S7K_SurveyReport_AddLatLongBathymetry_CI1'
        [flag, nomFicAdoc, Title, Comment, this.repExport] = Adoc.Params.SurveyReport_AddLatLongImage(this.Images(this.indImage), this.repExport);
        if flag
            SurveyReport_AddLatLongEchoIntegration(this.Images(this.indImage), nomFicAdoc, Title, Comment, ...
                'subx', subx, 'suby', suby)
        end
        
    case 'S7K_SurveyReport_AddLatLongReflectivity_CI1'
        [flag, nomFicAdoc, Title, Comment, this.repExport] = Adoc.Params.SurveyReport_AddLatLongImage(this.Images(this.indImage), this.repExport);
        if flag
            SurveyReport_AddLatLongReflectivity(this.Images(this.indImage), nomFicAdoc, Title, Comment, ...
                'subx', subx, 'suby', suby)
        end
        
    case 'S7K_SurveyReport_AddLatLongEchoIntegration_CI1'
        [flag, nomFicAdoc, Title, Comment, Thresholds, this.repExport] = Adoc.Params.SurveyReport_AddLatLongEchoIntegration(this.Images(this.indImage), this.repExport);
        if flag
            SurveyReport_AddLatLongEchoIntegration(this.Images(this.indImage), nomFicAdoc, Title, Comment, ...
                Thresholds, 'subx', subx, 'suby', suby)
        end
                
    case 'S7K_SurveyReport_Extract_CI0'
        [flag, nomFicAdoc, nomDirOut, params, flagZip, this.repImport, this.repExport] = params_ALL_SurveyReport_Extract(this.repImport, this.repExport);
        if flag
            AdocUtils.extractShortVersion(nomFicAdoc, nomDirOut, params, flagZip);
        end
        
    case 'S7K_SurveyReport_CleanDirectories_CI0'
        [flag, nomFicAdoc, this.repExport] = Adoc.Params.SurveyReport_Summary(this.repExport);
        if flag
            AdocUtils.cleanDirectories(nomFicAdoc);
        end

    case 'S7K_SurveyReport_PlotImageSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, nomFicAdoc, indexConfigs, this.repExport] = params_S7K_SurveyReport_PlotImageSignals(this.repExport);
            if flag
                S7K_SurveyReport_PlotImageSignals(S0, listFileNames, nomFicAdoc, indexConfigs)
            end
        end
        
    case 'S7K_SurveyReport_PlotSignalsVsTime_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 2);
        if flag
            [flag, nomFicAdoc, indexConfigs, this.repExport] = params_S7K_SurveyReport_PlotSignalsVsTime(this.repExport);
            if flag
                S7K_SurveyReport_PlotSignalsVsTime(S0, listFileNames, nomFicAdoc, indexConfigs)
            end
        end
                
    case 'S7K_Robot_CopyAndProcessSounderOutputFiles_CI0'
        [flag, paramsGeneraux, paramsWC, paramsDTM, paramsMOS, ~, this.repImport, this.repExport] ...
            = RobotUtils.InputParams.CopyAndProcessSounderOutputFiles('.s7k', this.repImport, this.repExport);
        if flag
            RobotUtils.S7k.launchProcess(paramsGeneraux, paramsWC, paramsDTM, paramsMOS);
        end
        
    case 'S7K_Robot_MoveFiles_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'SubTitle2', 'List of files you want to move');
        if flag
            [flag, this.repImport, this.repExport, nomDirOut, DirNames] = RobotUtils.InputParams.MoveFiles(listFileNames, this.repExport);
            if flag
                RobotUtils.Common.moveFiles(listFileNames, nomDirOut, DirNames);
            end
        end
        
    case 'S7K_Robot_PlotKMZ_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'SubTitle2', 'List of files you want to move');
        if flag
            [flag, filtreData, classement, nbSeconds] = RobotUtils.InputParams.PlotKMZ;
            if flag
            	RobotUtils.Common.PlotKMZ(listFileNames, classement, filtreData, nbSeconds);
            end
        end        
    case 'S7K_WC_RazFlagPings_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            WC_RazFlagPings(listFileNames)
        end
      
    %{
    case 'S7K_WC_EI_SampleBeam_SignalVsPings_CI1'
        % TODO : ce n'est qu'un d�but de mise en place. L'int�r�t de ce modile est
        % discutable. Il serait certainement pr�f�rable de le g�n�raliser � partir
        % d'un cache commun entre KM et S7K
        
        [flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, CLim, ALim, BLim, params_ZoneEI, ...
            Display, nomDirCSVOut, Tag, maskOverlapingBeams, thresholdValueRaw, thresholdValueComp, ...
            this.repImport] = WCD.Params.PlotNav_EI_SScSampleBeam(this.repImport, 'Ext', '.s7k', 'MessageParallelTbx', 1);
        if flag
            S7K_WC_EI_SampleBeam_SignalVsPings(S0, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
                CLim, ALim, BLim, params_ZoneEI, Display, nomDirCSVOut, Tag, maskOverlapingBeams, ...
                thresholdValueRaw, thresholdValueComp, 'suby', suby);
        end
    %}
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end




%% getListFiles
function [flag, this, listFileNames, sameFile, subx, suby] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.s7k');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

if nargout == 6
    [flag, listFileNames, repImport, sameFile, subx, suby] = survey_uiSelectFiles(this, 'ExtensionFiles', '.s7k', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
        'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['Reson ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
else
    [flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.s7k', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
        'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['Reson ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
    sameFile = [];
    subx     = [];
    suby     = [];
end
if flag
    this.repImport = repImport;
end
