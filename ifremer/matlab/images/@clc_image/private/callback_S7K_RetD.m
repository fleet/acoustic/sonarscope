function this = callback_S7K_RetD(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

a = cl_image.empty;

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'S7K_RnD_MasqueAmplitude_CI0'
        [flag, indLayerPhase, GateDepth, IdentAlgo, DisplayLevel] = params_S7K_RnD_MasqueAmplitude(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_ResonMasqueAmplitude(this.Images(this.indImage), this.Images(indLayerPhase), GateDepth, IdentAlgo, DisplayLevel);
        end

    case 'S7K_RnD_BottomDetectionAmplitudePhase_CI0'
        [flag, indLayerAmp, indLayerPhase, DisplayLevel, BeamsToDisplay, IdentAlgo, GateDepth] ...
            = params_S7K_RnD_DetectionAmplitudePhase(this.Images, this.indImage);
        if flag
            [flag, a, ~, this.Images(indLayerPhase), this.Images(indLayerAmp)] = ...
                sonar_ResonDetectionAmplitudePhase(this.Images(indLayerPhase), this.Images(indLayerAmp), ...
                DisplayLevel, BeamsToDisplay, IdentAlgo, GateDepth);
        end
        
    case 'S7K_RnD_PhaseUnwrapping_CI0'
        [flag, indLayerAmp, indLayerPhase, DisplayLevel, GateDepth] = params_S7K_RnD_UnwrapPhase(this.Images, this.indImage);
        iPing = 1;
        Draught = get_DraughtShip;
        MatAmplitude = get(this.Images(indLayerAmp), 'Image');
        DataPing = get(this.Images(indLayerAmp), 'SampleBeamData');
        DataPing = sonar_detectionAmplitudeOnReducedMatrix(this.Images(indLayerAmp), MatAmplitude, DataPing, GateDepth, Draught, DisplayLevel, iPing, ...
            'Phase', this.Images(indLayerPhase));
        if flag
            this.Images(indLayerAmp) = set_SampleBeamData(this.Images(indLayerAmp), DataPing);
            if ~isempty(indLayerPhase)
                this.Images(indLayerPhase) = set_SampleBeamData(this.Images(indLayerPhase), DataPing);
            end
            
            [flag, a1] = sonar_ResonUnwrappingPhase_Horz(this.Images(indLayerPhase));
            if flag
                [flag, a2] = sonar_ResonUnwrappingPhase_Vert(this.Images(indLayerPhase));
                if flag
                    a = [a2 a1];
                end
            end
        end

    case 'S7K_RnD_XL_JMA_FiltragePhase_CI1'
        [flag, a] = sonar_ResonPhaseFilter(this.Images(this.indImage));
 
    case 'ResonRnD_SplitFrequencies'
         [flag, a] = sonar_ResonSplitFrequencies(this.Images(this.indImage), 'subx', subx, 'suby', suby);
         
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
