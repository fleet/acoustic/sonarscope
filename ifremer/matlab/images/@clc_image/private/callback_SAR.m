function this = callback_SAR(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SAR_ReadOnce_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = Sar2ssc(listFileNames);
        end
         
    case 'SAR_PlotSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SAR_PlotSignals(listFileNames);
        end
        
    case 'SAR_PlotNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SAR_PlotNavigation(listFileNames);
        end
         
    case 'SAR_PlotNavigationCoverage_CI0'
        [flag, listFileNames, MaxRange, this.repImport] = params_SAR_PlotNavigationCoverage(this.repImport);
        if flag
            SAR_PlotNavigationCoverage(listFileNames, 'MaxRange', MaxRange);
        end
       
    case 'SAR_PlotSignalOnNavigation_CI0'
        [flag, listFileNames, nomSignal, this.repImport] = params_SAR_PlotSignalOnNavigation(this.repImport);
        if flag
            SAR_PlotSignalOnNavigation(listFileNames, nomSignal);
        end
                
    case 'SAR_Summary_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicSummary, this.repExport] = SSc.Params.getSummaryFilename(this.repExport, '.im');
            if flag
                flag = SAR_Summary(listFileNames, nomFicSummary);
            end
        end
        
    case 'SAR_ImportNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav, this.repImport] = SSc.Params.ImportNavigation('.nvi', this.repImport);
            if flag
                SAR_ImportNavigation(listFileNames, nomFicNav);
            end
        end
        
    case 'SAR_CleanSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = SAR_CleanSignals(listFileNames);
        end
        
    case 'SAR_RestoreSignals_CI0'
        [flag, listFileNames, nomSignal, this.repImport] = params_SAR_RestoreSignals(this.repImport);
        if flag
            flag = SAR_RestoreSignals(listFileNames, nomSignal);
        end
        
    case 'SAR_BackupSignals_CI0'
        [flag, listFileNames, nomSignal, this.repImport] = params_SAR_RestoreSignals(this.repImport);
        if flag
            flag = SAR_BackupSignals(listFileNames, nomSignal);
        end
        
    case 'SAR_PingSamples2LatLong_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repExport] = SAR_PingSamples2LatLong('NomDir', this.repImport, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'SAR_CreatePseudoDTM_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repExport] = SAR_CreatePseudoDTM('NomDir', this.repImport, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'SAR_SubbottomExport3DViewer_CI0'
        [flag, listFileNames, nomDirOut, Tag, CLim, NbSamplesMax, TailleMax, CeleriteSediments, this.repImport, this.repExport] ...
            = params_SAR_SubbottomExport3DViewer(this.repImport, this.repExport);
        if flag
            flag = SAR_SubbottomExport3DViewer(listFileNames, nomDirOut, Tag, CLim, NbSamplesMax, TailleMax, CeleriteSediments);
        end
        
    case 'SAR_WC_Export3DViewer_CI0'
        [flag, listFileNames, Side, NomDirXML, TailleMax, Tag, ColormapIndex, Video, CLim, this.repImport, this.repExport] ...
            = params_SAR_WC_Export3DViewer(this.repImport, this.repExport);
        if flag
            flag = S7K_WCExport3DViewer(listFileNames, NomDirXML, Tag, CLim, TailleMax, Side, ColormapIndex, Video);
        end
        
    case 'SAR_Mosaiques_Individuelles_CI0'
        [flag, listeFicAll, gridSize, CorFile, selectionData, this.repImport, this.repExport] = params_SAR_IndividualMosaics(this.repImport, this.repExport);
        if flag
            SAR_Mosaiques_Individuelles(this.repExport, listeFicAll, gridSize, CorFile, selectionData);
        end

    case 'SAR_ZipDirectories_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SAR_ZipDirectories(listFileNames);
        end
        
    case 'SAR_DeleteCache_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            eraseSonarScopeCache(listFileNames);
        end        
             
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end




%% getListFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.im');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.im', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['SAR ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
