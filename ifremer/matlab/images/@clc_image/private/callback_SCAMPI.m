function this = callback_SCAMPI(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

I0 = cl_image_I0;
a  = cl_image.empty;

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SCAMPI_Movie'
        [flag, ListeFicImage, nomFicAvi, fps, quality, this.repImport, this.repExport] ...
            = params_OTUS_Movie(this.repImport, this.repExport);
        if flag
            OTUS_Movie(I0, ListeFicImage, nomFicAvi, fps, quality);
        end
        
    case 'SCAMPI_CreateCompensatedImages'
        [flag, ListeFicImage, OrdrePolyCompens, nomDirOut, this.repImport, this.repExport] ...
            = params_OTUS_CreateCompensatedImages(this.repImport, this.repExport);
        if flag
            flag = OTUS_CreateCompensatedImages(I0, ListeFicImage, OrdrePolyCompens, nomDirOut);
        end

    case 'SCAMPI_CalculCompensation'
        [flag, ListeFicImage, nomFicCompens, SeuilStd, this.repImport] = params_OTUS_CalculCompensation(this.repImport);
        if flag
            SCAMPI_CalculCompensation(I0, ListeFicImage, nomFicCompens, SeuilStd);
        end
        
    case 'SCAMPI_CompensMeanImage'
        [flag, ListeFicImage, nomFicCompens, this.repImport] = params_OTUS_CompensMeanImage(this.repImport);
        if flag
            SCAMPI_CompensMeanImage(I0, ListeFicImage, nomFicCompens);
        end
       
    case 'SCAMPI_CreationXmlFromNMEA'
        [flag, ListeFicImage, nomFicNavCorrigee, nomFicOut, Focale, this.repImport, this.repExport] ...
            = params_SCAMPI_creationXmlFromNMEA(this.repImport, this.repExport);
        if flag
            SCAMPI_creationXmlFromNMEA(I0, ListeFicImage, nomFicNavCorrigee, nomFicOut, Focale);
        end
        
    case 'SCAMPI_MosaiquesIndividuelles'
        [flag, ListeFicImage, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nomFicXMLPosition, ...
            nomDirImagesCompensee, HeightMax, this.repImport, this.repExport] ...
            = params_OTUS_MosaiquesIndividuelles(this.repImport, this.repExport);
        if flag
            OTUS_MosaiquesIndividuelles(I0, ListeFicImage, nomFicXMLPosition, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nomDirImagesCompensee, ...
                'HeightMax', HeightMax);
        end
        
    case 'SCAMPI_ExportLatLonImages3DV'
        [flag, ListeFicImage, nomDirOut, this.repImport, this.repExport] = params_OTUS_ExportLatLongImages3DV(this.repImport, this.repExport);
        if flag
            OTUS_ExportLatLonImages3DV(I0, ListeFicImage, nomDirOut);
        end
        
    case 'SCAMPI_ExportRawImages3DV'
        [flag, ListeFicImage, nomFicXML, Carto, nomFicXMLPosition, this.repImport, this.repExport] ...
            = params_OTUS_ExportRawImages3DV(this.repImport, this.repExport, 'Capteur', 'SCAMPI');
        if flag
            OTUS_ExportRawImages3DV(I0, ListeFicImage, nomFicXML, Carto, nomFicXMLPosition, 'Capteur', 'SCAMPI');
        end
        
    case 'SCAMPI_Mosaic'
        [flag, ListeFicImage, resol, MosaicName, nomRepOutDales, nomFicCompens, Carto, TypeCompensation, ...
            nbMaxPixelsWidthTile, nbMaxPixelsHeightTile, nomFicXMLPosition, nomDirImagesCompensee, ...
            Capteur, HeightMax, NomRacine, ext, this.repImport, this.repExport] ...
            = params_OTUS_Mosaic(this.repImport, this.repExport, 'Capteur', 'SCAMPI');
        if flag
            OTUS_Mosaic(I0, ListeFicImage, nomFicXMLPosition, resol, MosaicName, nomRepOutDales, nomFicCompens, Carto, TypeCompensation, ...
                nomDirImagesCompensee, Capteur, NomRacine, ext, 'HeightMax', HeightMax, ...
                'nbMaxPixelsWidthTile', nbMaxPixelsWidthTile, 'nbMaxPixelsHeightTile', nbMaxPixelsHeightTile);
        end
                
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
