function this = callback_SDF(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SDF_PlotNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, OriginNav] = SDF.Params.plotNav;
            if flag
                SDF.Process.plotNav(listFileNames, 'OriginNav', OriginNav);
            end
        end
        
    case 'SDF_PlotNavigationAndCoverage_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            Fig = SDF.Process.plotNav(listFileNames);
            SDF.Process.plotCoverage(listFileNames, 'Fig', Fig)
        end
        
    case 'SDF_PlotNavigationAndSignal_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignal] = SDF.Params.plotNavAndSignal(listFileNames);
            if flag
                [Fig, Carto] = SDF.Process.plotNav(listFileNames);
                SDF.Process.plotDataOnNav(Fig, listFileNames, NomSignal, 'Carto', Carto);
            end
        end
        
    case 'SDF_ReadOnce_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
        	SDF.Process.readOnce(listFileNames);
        end
        
    case 'SDF_ImportNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav, this.repImport] = SSc.Params.ImportNavigation('.nvi', this.repImport);
            if flag
                SDF.Process.importExternalNav(listFileNames, nomFicNav);
            end
        end
      
    case 'SDF_EditNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, OriginNav] = SDF.Params.plotNav;
            if flag
                [flag, SourceHeading] = SDF.Params.selectSourceHeading;
                if flag
                    SDF.Process.editNav(listFileNames, 'OriginNav', OriginNav, 'SourceHeading', SourceHeading);
                end
            end
        end
        
    case 'SDF_CheckHeights_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = SDF.Process.checkHeights(listFileNames);
        end
        
    case 'SDF_CheckFishNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = SDF.Process.checkFishNav(listFileNames);
        end
        
    case 'SDF_CheckShipNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = SDF.Process.checkShipNav(listFileNames);
        end
        
    case 'SDF_ComputeFishNavFromShipNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = SDF.Process.computeFishNavFromShipNav(listFileNames);
        end
        
    case 'SDF_Rectification_CI0'
        [flag, indentMos, listFileNames, identLayer, NomFicOut, SourceHeading, this.repImport, this.repExport] = params_SDFRectification(this.Images, this.indImage, this.repImport, this.repExport);
        if flag
            flag = sonar_rectificationPingAcrossDist(this.Images(indentMos), listFileNames, identLayer, NomFicOut, SourceHeading);
        end
        
    case 'SDF_Summary_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicSummary, this.repExport] = SSc.Params.getSummaryFilename(this.repExport, '.sdf');
            if flag
                SDF.Process.computeSummary(listFileNames, nomFicSummary); % TODO renommer en SDF_Summary
            end
        end
        
    case 'SDF_PingSamples2LatLong_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, a, this.repExport] = SDF.Process.PingSamples2LatLong(listFileNames, this.repExport);
        end
        
    case 'SDF_PingSamples2PingAcrossDist_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
    	    flag = SDF.Process.PingSamples2PingAcrossDist(listFileNames);
        end
        
    case 'SDF_PlotSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, listeSignals, typeAxeX] = SDF.Params.plotSignals(listFileNames);
            if flag
                SDF.Process.plotSignals(listFileNames, listeSignals, typeAxeX);
            end
        end
        
    case 'SDF_BackupSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignals, backupFileName] = SDF.Params.backupSignals(this.repImport, 'Output');
            if flag
                SDF.Process.backupSignals(listFileNames, NomSignals, backupFileName);
            end
        end
        
    case 'SDF_RestoreSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, NomSignals, backupFileName] = SDF.Params.backupSignals(this.repImport, 'Input');
            if flag
                SDF.Process.restoreSignals(listFileNames, NomSignals, backupFileName);
            end
        end
        
    case 'SDF_DeleteCache_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            eraseSonarScopeCache(listFileNames);
        end
        
    case 'SDF_DatagramsSplit_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [this.repImport, this.repExport] = SDF.Process.splitSdfFile(listFileNames, this.NomFicPoints, this.repImport, this.repExport);
        end
        
    case 'SDF_Mosaiques_Individuelles_CI0'
        [flag, this.repImport, this.repExport] = SDF.Process.computeMosaiqueParProfil('nomDirImport', this.repImport, 'nomDirExport', this.repExport);
        
%     case 'SDF_TideImport_CI0'
%         [flag, this, listFileNames] = getListFiles(this);
%         if flag
%             SDF.Process.tideImport(listFileNames, NomFicTide)
%         end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end




%% getListFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.sdf');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.sdf', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['Geoswath ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
