function this = callback_SEGY(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

S0 = cl_segy([]);
a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SEGY_ReadOnce_CI0'
        [flag, this, listFileNames] = getListFiles(this, 'MessageParallelTbx', 1);
        if flag
            SEGY_ReadOnce(S0, listFileNames);
        end
        
    case 'SEGY_ProcesChirp_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, objParams] = params_SEGY_ProcesChirp(S0);
            if flag
                SEGY_ProcesChirp(S0, listFileNames, objParams);
            end
        end

    case 'SEGY_PlotNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SEGY_PlotNavigation(S0, listFileNames);
        end
        
    case 'SEGY_PlotSignalOnNavigation_CI0'
        [flag, listFileNames, nomSignal, this.repImport] = params_SEGY_PlotNavigationAndSignal(this.repImport);
        if flag
            [Fig, Carto] = SEGY_PlotNavigation(S0, listFileNames);
            SEGY_PlotSignalOnNavigation(S0, Fig, listFileNames, nomSignal, 'Carto', Carto);
        end
         
    case 'SEGY_ImportNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav, this.repImport] = SSc.Params.ImportNavigation( {'.nvi'; '.csv'; '.dat'}, this.repImport);
            if flag
                SEGY_ImportNavigation(S0, listFileNames, nomFicNav);
            end
        end
        
    case 'SEGY_EditNav_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            flag = SEGY_EditNav(S0, listFileNames);
        end
                 
    case 'SEGY_Export3DViewer_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, listLayers, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, NbSamplesMax, ...
                IdentPartData, TailleMax, Marge, Video, typeOutput, this.repExport] = params_SEGY_Export3DViewer(this.Images(this.indImage), listFileNames, this.repExport);
            if flag
                flag = SEGY_Export3DViewer(S0, listFileNames, listLayers, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, ...
                    NbSamplesMax, IdentPartData, TailleMax, Marge, Video, typeOutput);
            end
        end
        
    case 'SEGY_Export3DBlocSismiqueV1_CI0'
        [flag, NomFicSegY, NomDirXML, CEau, this.repImport, this.repExport] = params_SEGY_ExportSismique3DViewerV1('V1', this.repImport, this.repExport);
        if flag
            SEGY_ExportSismique3DViewerV1(NomFicSegY, NomDirXML, CEau);
        end
  
    case 'SEGY_Export3DBlocSismiqueV2_CI0'
        [flag, NomFicSegY, NomFicXML, CEau, this.repImport, this.repExport] = params_SEGY_ExportSismique3DViewerV1('V2', this.repImport, this.repExport);
        if flag
            %{
            flag = SEGY_ExportSismique3DViewerV2Matrix(NomFicSegY, NomFicXML, CEau);
            %}
            flag = SEGY_ExportSismique3DViewerV2(NomFicSegY, NomFicXML, CEau); 
        end
      
    case 'SEGY_DeleteCache_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            eraseSonarScopeCache(listFileNames);
        end
        
    case 'SEGY_CompressDirectories_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            compressDirectories_SSc(listFileNames, 'Tag', 'SEGY');
        end
        
    case 'SEGY_CleanSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, HeightDetection] = params_SEGY_PreprocessingSignals(S0);
            if flag
                SEGY_CleanSignals(S0, listFileNames, HeightDetection);
            end
        end
        
    case 'SEGY_PlotSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            SEGY_PlotSignals(S0, listFileNames);
        end
        
    case 'SEGY_TideImport_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicTide, this.repImport] = SSc.Params.ImportTide(this.repImport);
            if flag
                SEGY_TideImport(S0, listFileNames, nomFicTide);
            end
        end
       
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end




%% getListFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.seg / .sgy / .segy');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', {'.seg'; '.sgy'; '.segy'}, 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['Seismic ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
