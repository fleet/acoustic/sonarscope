% Callbacks de Sonar processings
%
% Syntax
%   a = callback_SP(a)
%
% Input Arguments
%   a   : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_SP(this, varargin)

if isempty(varargin)
    MessageTraitementDroite
    return
end

%% D�chargement automatique d'images en m�moire virtuelle si besoin

this.Images = optimiseMemory(this.Images, 'SizeMax', 500000);

%% Recherche des pixels � traiter

[flag, subx, suby, nomZoneEtude, XLimCadre, YLimCadre, flagImageEntiere] = getSubxSubyContextuel(this, varargin{:});
if ~flag
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

msg = varargin{1};
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_Sidescan'
        this = callback_SP_Sidescan(this, varargin{2:end}, subx, suby);
       
    case 'SP_BS_AddOn'
        this = callback_SP_BS_AddOn(this, varargin{2:end}, subx, suby);
        
    case 'SP_BS_Analysis'
        this = callback_SP_BS_Analysis(this, varargin{2:end}, subx, suby, nomZoneEtude);
        
    case 'SP_SBP'
        this = callback_SP_SBP(this, varargin{2:end}, subx, suby);
        
    case 'SP_Apendices'
        this = callback_SP_Apendices(this, varargin{2:end}, subx, suby);
        
    case 'SP_CreateLayers'
        this = callback_SP_CreateLayers(this, varargin{2:end}, subx, suby);
        
    case 'SP_BS_Calib'
        this = callback_SP_BS_Calib(this, varargin{2:end}, subx, suby);
        
    case 'SP_Resol'
        this = callback_SP_Resol(this, varargin{2:end}, subx, suby);
        
    case 'SP_BS_Calib_ModeResult'
      this = callback_SP_BS_Calib_ModeResult(this, varargin{2:end}, subx, suby, nomZoneEtude);
        
    case 'SP_BS_Calib_CheckCompens'
        this = callback_SP_BS_Calib_CheckCompens(this, varargin{2:end}, subx, suby);
        
    case 'SP_AireInso'
        this = callback_SP_AireInso(this, varargin{2:end}, subx, suby);
        
    case 'SP_DiagTx'
        this = callback_SP_DiagTx(this, varargin{2:end}, subx, suby, nomZoneEtude);
        
    case 'SP_DiagRx'
        this = callback_SP_DiagRx(this, varargin{2:end}, subx, suby);
        
    case 'SP_MBES'
        this = callback_SP_MBES(this, varargin{2:end}, subx, suby);
        
    case 'SP_TVG'
        this = callback_SP_TVG(this, varargin{2:end}, subx, suby);
        
    case 'SP_Mosaique'
        this = callback_SP_Mosaique(this, varargin{2:end}, subx, suby, nomZoneEtude, XLimCadre, YLimCadre);
        
    case 'SP_WCD'
        this = callback_SP_WCD(this, varargin{2:end}, subx, suby, flagImageEntiere);
        
    case 'SP_Speculaire'
        this = callback_SP_Speculaire(this, varargin{2:end}, subx, suby);
        
    case 'MonoCapteur'
        this = callback_MonoCapteur(this, varargin{2:end});

    case 'XYZ'
        this = callback_XYZ(this, varargin{2:end}, subx, suby);

    case 'ADCP_Export3DViewer_CI1'
        [flag, NomFicXML, TailleMax, ~, this.repExport] = paramsExport3DV_ImagesAlongNavigation(this.Images, this.indImage, this.repExport);
        if flag
            flag = adcp_Export3DV_ImagesAlongNavigation_ByBlocks(this.Images(this.indImage), NomFicXML, TailleMax, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'TagExport3DSubBottom' % TODO : A supprimer, supprimer aussi SEGY_ExportSubbottom3DViewerV2
        [flag, this, NomFicSegY, NomFicXML, CEau] = paramsExportSismique3DViewerV1(this, 'V2');
        if flag
            flag = SEGY_ExportSubbottom3DViewerV2(NomFicSegY, NomFicXML, CEau);
        end
        
    % Cas des diff�rents formats de Sondeurs-Sonars-Capteurs.    
    case 'ALL'
        this = callback_ALL(this, varargin{2:end}, subx, suby, nomZoneEtude, flagImageEntiere);
        
    case 'ExRaw' % TODO : rempalcer par 'RAW'
        this = callback_RAW(this, varargin{2:end});
        
    case 'SEGY'
        this = callback_SEGY(this, varargin{2:end});
        
    case 'BOB'
        this = callback_BOB(this, varargin{2:end});
        
    case 'HAC'
        this = callback_HAC(this, varargin{2:end});
        
    case 'ODV'
        this = callback_ADCP(this, varargin{2:end});
        
    case 'SAR'
        this = callback_SAR(this, varargin{2:end});
        
    case 'XTF'
        this = callback_XTF(this, varargin{2:end}, suby);
        
    case 'OTUS'
        this = callback_OTUS(this, varargin{2:end});
        
    case 'SCAMPI'
        this = callback_SCAMPI(this, varargin{2:end});
        
    case 'RDF'
        this = callback_RDF(this, varargin{2:end});
        
    case 'S7K'
        this = callback_S7K(this, varargin{2:end}, subx, suby, flagImageEntiere);
        
    case 'S7K_R&D'
        this = callback_S7K_RetD(this, varargin{2:end}, subx, suby);
        
    case 'SDF'
        this = callback_SDF(this, varargin{2:end});
        
    case 'WCD'
        this = callback_WCD(this, varargin{2:end});
        
    case 'REMUS'
        this = callback_REMUS(this, varargin{2:end});
        
    case 'CINNA'
        this = callback_CINNA(this, varargin{2:end});
        
    case 'NAVIGATION'
        this = callback_NAVIGATION(this, varargin{2:end});
        
    case 'CACHE'
        this = callback_CACHE(this, varargin{2:end});
        
    case 'NORBIT'
        this = callback_NORBIT(this, varargin{2:end});
        
    case 'CAR'
        this = callback_CAR(this, varargin{2:end}, subx, suby);
        
    case 'Help'
        flag = SonarScopeHelp(varargin{2:end});
       
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition'); % Ajout� le 12/08/2009 : devrait r�soudre le pb de zoom qui n'est pas correct lorque l'on traite une image enti�re alors que l'on est positionn� sur une partie de l'image
end
