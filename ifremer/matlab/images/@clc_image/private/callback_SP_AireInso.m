function this = callback_SP_AireInso(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_AireInso_Constructeur_CI1'
        if isComingFromKonsbergMBES(this.Images(this.indImage))
            [flag, indLayerRange, indLayerTxBeamIndex, indLayerBeamPointingAngle] = paramsFonctionSonarAireInsonifieeKonsberg(this.Images, this.indImage);
            if flag
                [flag, a] = sonar_InsonifiedArea_KM(this.Images(this.indImage), this.Images(indLayerRange), this.Images(indLayerBeamPointingAngle), ...
                    'TxBeamIndex', this.Images(indLayerTxBeamIndex), 'ComputeMask', 1, ...
                    'subx', subx, 'suby', suby);
            end
        else
            % TODO : A d�faut de savoir ce qui est fait pour les autres
            % sondeurs autres que Kongsberg, on recalcule la correction
            % d'aire insonifi�e � la fa�on "Ifremer".
            [flag, indLayerInsonifiedArea, indLayerRange, CasDepth, H, indLayerBathy, CasDistance, ...
                indLayerEmission] = paramsFonctionSonarAireInsonifiee(this.Images, this.indImage);
            if flag
                if isempty(indLayerInsonifiedArea)
                    [flag, a] = sonar_aire_insonifiee_dB(this.Images(this.indImage), this.Images(indLayerRange), ...
                        CasDepth, H, CasDistance, ...
                        this.Images(indLayerBathy), this.Images(indLayerEmission), 1, ...
                        'subx', subx, 'suby', suby);
                else
                    % TODO : que faire ? Dire � l'utilisateur qu'il a d�j� un
                    % layer dans la liste ? Cloner ce layer comme cela est fait
                    % ici n'est certainement pas la bonne solution
                    % TODO : attention : remplacer subx et suby par XLim et YLim
                    a = extraction(this.Images(indLayerInsonifiedArea), ...
                        'suby', suby, 'subx', subx);
                end
            end
        end

    case 'SP_AireInso_Ifremer_CI1'
        [flag, indLayerInsonifiedArea, indLayerRange, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, ...
            indLayerTxBeamIndex, indLayerSlopeAcross, indLayerSlopeAlong] = paramsFonctionSonarAireInsonifiee(this.Images, ...
            this.indImage, 'Mute', 0);
        if flag
            if isempty(indLayerInsonifiedArea)
                [flag, a] = sonar_aire_insonifiee_dB(this.Images(this.indImage), this.Images(indLayerRange), ...
                    CasDepth, H, CasDistance, ...
                    this.Images(indLayerBathy), ...
                    this.Images(indLayerEmission), ...
                    2, ...
                    'TxBeamIndex', this.Images(indLayerTxBeamIndex), ...
                    'SlopeAcross', this.Images(indLayerSlopeAcross), ...
                    'SlopeAlong',  this.Images(indLayerSlopeAlong), 'ComputeMask', 1, ...
                    'subx', subx, 'suby', suby); % TODO : add Across slope and AlongSlope if available
            else
                % TODO : que faire ? Dire � l'utilisateur qu'il a d�j� un
                % layer dans la liste ? Cloner ce layer comme cela est fait
                % ici n'est certainement pas la bonne solution
                % TODO : attention : remplacer subx et suby par XLim et YLim
                a = extraction(this.Images(indLayerInsonifiedArea), ...
                    'suby', suby, 'subx', subx);
            end
        end

    case 'SP_AireInso_NoCompens_CI1'
        [flag, a] = SonarAireInsoCompensAucune(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);

    case 'SP_AireInso_CompensConstructeur_CI1'
        [flag, a] = SonarAireInsoCompens(this.Images, this.indImage, 1, ...
            'subx', subx, 'suby', suby);

    case 'SP_AireInso_CompensIfremer_CI1'
        [flag, a] = SonarAireInsoCompens(this.Images, this.indImage, 2, ...
            'subx', subx, 'suby', suby);
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
