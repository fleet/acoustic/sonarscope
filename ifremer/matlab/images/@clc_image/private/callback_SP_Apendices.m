function this = callback_SP_Apendices(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

I0 = cl_image_I0;

%% Lancement des traitements

flag = 1;
a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_Apendices_GeographicSituation_CI1'
        flag = testSignature(this.Images(this.indImage), 'GeometryType', 'PingXxxx');
        if flag
            [flag, a] = sonar_situation_geo(this.Images(this.indImage), ...
                'suby', suby);
        end
        
    case 'SP_Apendices_Bathycelerimetrie_CI0'
        sonar_bathycelerimetrie(this.Images(this.indImage));
        flag = 1;
        
    case 'SP_Apendices_CreateDefaultSalinityFile_CI0'
        flag = createDefaultSalinityFile(this.repImport);

    case 'SP_Apendices_Fresnel_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarResol(this.Images, this.indImage);
        if flag
            a = sonar_ZoneDeFresnel(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_Apendices_ComputeHeading_CI1'
        this.Images(this.indImage) = sonar_ComputeHeading(this.Images(this.indImage), ...
            'suby', suby);
        this = show_profils(this);

    case 'SP_Apendices_ImportNav_CI0'
        [flag, nomFicNav, this.repExport] = paramsFonctionImportNav(this.repExport);
        if flag
            this.Images(this.indImage) = sonar_import_nav(this.Images(this.indImage), nomFicNav);
            this = show_profils(this);
        end

    case 'SP_Apendices_PlotNav_CI0'
        flag = testSignature(this.Images(this.indImage), 'GeometryType', 'PingXxxx');
        if flag
            sonar_plot_nav(this.Images(this.indImage));
        end
 
    case 'SP_Apendices_ImportTide_CI0'
        [flag, nomFicTide, this.repImport] = SSc.Params.ImportTide(this.repImport);
        if flag
            this.Images(this.indImage) = sonar_import_tide(this.Images(this.indImage), nomFicTide);
        end

    case 'SP_Apendices_MapPropagationAcoustique_CI0'
        [flag, Frequence, Month, Position, Hauteur, Angle] = paramsMapPropagationAcoustique(this.Images(this.indImage));
        if flag
            [flag, a] = MapPropagationAcoustique(I0, Frequence, Month, Position, Hauteur, Angle);
        end
        
    case 'SP_Apendices_ExportSignaux_CI0'
        [flag, nomsFic, ColumnsForProfile, listeSignaux, Separator, TimeFormat, subCourbes, this.repExport] ...
            = params_FonctionExportSignaux(this.Images, this.indImage, this.cross(this.indImage,:), this.repExport);
        if flag
            this.Images(this.indImage) = sonar_export_signaux(this.Images(this.indImage), nomsFic, ...
                ColumnsForProfile, listeSignaux, Separator, TimeFormat, subCourbes);
        end
        
    case 'SP_Apendices_PlotWCFootprint_CI0'
        [flag, FileNameXML, FileNameNav, this.repImport] = params_PlotWCFootprint(I0, this.repImport);
        if flag
            plotWCFootprint(this.Images(this.indImage), FileNameXML, 'FileNameNav', FileNameNav)
        end

    case 'SP_Apendices_Pames_CI0'
        PAMES = PamesDialog();
        PAMES.openDialog;
        flag = 1;
        
    case 'SP_Apendices_plotGeographicPositionsInGoogleEarth_CI0'
        [flag, nomFicIn, nomFicOut, Labels2Plot, strColor, identColor, this.repImport, this.repExport] ...
            = params_PlotGeographicPositionsInGoogleEarth(I0, this.repImport, this.repExport);
        if flag
            flag = plotGeographicPositionsInGoogleEarth(nomFicIn, nomFicOut, Labels2Plot, strColor, identColor);
        end
        
    case 'SP_Apendices_CreateMasksArounfGeographicPoints_CI0'
        [flag, nomFicIn, nomDirOut, typeFrame, widthFrame, gridSize, this.repImport, this.repExport] = params_CreateMasksArounfGeographicPoints(I0, this.repImport, this.repExport);
        if flag
            flag = CreateMasksAroundGeographicPoints(nomFicIn, nomDirOut, typeFrame, widthFrame, gridSize);
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
