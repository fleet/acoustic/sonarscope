function this = callback_SP_BS_AddOn(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg   
    case 'SP_BS_KMSpecularCompensation_CI1'
        [flag, flagRecomputeRn] = params_KMSpecularCompensation(this.Images(this.indImage));
        if flag
            [flag, a] = KMSpecularCompensation(this.Images(this.indImage), flagRecomputeRn, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_BS_KMSpecularCompensationAngle_CI1'
        [flag, flagRecomputeRn] = params_KMSpecularCompensation(this.Images(this.indImage));
        if flag
            [flag, a] = KM_AngleEstimation(this.Images(this.indImage), flagRecomputeRn, ...
                'subx', subx, 'suby', suby);
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
