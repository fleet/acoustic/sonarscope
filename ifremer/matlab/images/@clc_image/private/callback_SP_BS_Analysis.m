function this = callback_SP_BS_Analysis(this, msg, subx, suby, nomZoneEtude)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

flag = 1;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_BS_Calcul_CI1'
        [flag, this.Images] = SonarBSCompute(this.Images, this.indImage, subx, suby, nomZoneEtude, ...
            'EnteteNomCourbe', 'BS_Model');

    case 'SP_BS_Plot_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(sub)
            plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'Tag', 'BS');
        end
        
    case 'SP_BS_Nettoyage_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(sub)
            [this.Images(this.indImage), nbCurvesCreated] = cleanCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0); %#ok<ASGLU>
        end
        
    case 'SP_BS_Filter_CI0'
        [flag, sub, nbCourbesStatistiques]  = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
            [flag, Ordre, Wc] = SaisieParamsButterworth(2, 0.02);
            if flag
                this.Images(this.indImage) = filtreCourbesStats(this.Images(this.indImage), ...
                    'sub', sub, 'Stats', 0, 'Ordre', Ordre, 'Wc', Wc);
                subFiltree = nbCourbesStatistiques:(nbCourbesStatistiques-1+length(sub));
                plotCourbesStats(this.Images(this.indImage), 'sub', [sub subFiltree], 'Stats', 0);
            end
        end

    case 'SP_BS_Delete_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(sub)
            this.Images(this.indImage) = deleteCourbesStats(this.Images(this.indImage), 'sub', sub);
        end

    case 'SP_BS_Export_CI0'
        [flag, nomFicSave, sub, this.repExport] = params_SonarBsExport(this.Images(this.indImage), this.repExport);
        if flag
            exportCourbesStats(this, nomFicSave, 'sub', sub, 'Tag', 'BS');
        end

    case 'SP_BS_Import_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', this.repImport, 'ChaineIncluse', 'BS');
        if flag
            [flag, this.Images(this.indImage)] = import_CourbesStats(this.Images(this.indImage), nomFic, 'Tag', 'BS');
        end

%     case 'SP_BS_Adjust_CI0'
%         [flag, sub]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS', 'XYLimZoom', this.visuLim(this.indImage, :));
%         if flag && ~isempty(sub)
%             this.Images(this.indImage) = optim_BS(this.Images(this.indImage), 'sub', sub);
%         end

    case 'SP_BS_ImageSynthesis_CI1'
        [flag, indLayerAngle, ModelName, Parameters, listValueMask, Noise] = paramsBS_CurvesSynthesis(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_BSImageSynthesis(this.Images(this.indImage), this.Images(indLayerAngle), ...
                ModelName, Parameters, listValueMask, Noise, ...
                'subx', subx, 'suby', suby);
        end

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
