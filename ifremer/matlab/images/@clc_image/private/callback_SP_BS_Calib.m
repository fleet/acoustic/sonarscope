function this = callback_SP_BS_Calib(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_BS_CurvesPartial_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(sub)
            plotCourbesBelleImage(this.Images(this.indImage), 'sub', sub);
        end

    case 'SP_BS_CurvesFull_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(sub)
            plotCourbesFullCompensation(this.Images(this.indImage), 'sub', sub);
        end

    case 'SP_BS_Level_LambertKM_CI1'
        [flag, indLayerTwoWayTravelTimeInSeconds] = params_LambertCorrectionKM(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_Lambert_KM(this.Images(this.indImage), this.Images(indLayerTwoWayTravelTimeInSeconds), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_BS_Level_LambertAngles_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = params_LambertCorrection(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_BelleImage_Lambert(this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_BS_Level_Lurton_CI1'
        [flag, identCourbe] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(identCourbe)
            [flag, indLayerAngleIncidence] = paramsFonctionSonarBS_Image_Lurton(this.Images, this.indImage, identCourbe);
            if flag
                [flag, a] = sonar_Image_Lurton(this.Images(this.indImage), ...
                    this.Images(indLayerAngleIncidence), 'identCourbe', identCourbe, ...
                    'subx', subx, 'suby', suby);
            end
        end

    case 'SP_BS_Level_Func_CI1'
        [flag, identCourbe]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(identCourbe)
            [flag, indLayerAngleIncidence] = paramsFonctionSonarBS_Image_Lurton(this.Images, this.indImage, identCourbe);
            if flag
                [flag, a] = sonar_FullCompensationAnalytique(this.Images(this.indImage), ...
                    this.Images(indLayerAngleIncidence), 'identCourbe', identCourbe, ...
                    'subx', subx, 'suby', suby);
            end
        end

    case 'SP_BS_Level_Table_CI1'
        [flag, identCourbe]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(identCourbe)
            [flag, indLayerAngleIncidence] = paramsFonctionSonarBS_Image_Lurton(this.Images, this.indImage, identCourbe);
            if flag
                a = sonar_FullCompensationTable(this.Images(this.indImage), ...
                    this.Images(indLayerAngleIncidence), 'identCourbe', identCourbe, ...
                    'subx', subx, 'suby', suby);
            end
        end

    case 'SP_BS_Compens_None_CI1'
        [flag, a] = SonarBSCompensNone(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);

    case 'SP_BS_Compens_LambertKM_CI1'
        [flag, a] = SonarBSCompens(this.Images, this.indImage, 1, 1, [], 1, ...
            'subx', subx, 'suby', suby);

    case 'SP_BS_Compens_LambertAngles_CI1'
        [flag, a] = SonarBSCompens(this.Images, this.indImage, 1, 1, [], 2, ...
            'subx', subx, 'suby', suby);

    case 'SP_BS_Compens_Lurton_CI1'
        SonarBS_LambertCorrection = 1;
        [flag, a] = SonarBSCompens(this.Images, this.indImage, 1, 2, [], SonarBS_LambertCorrection, ...
            'subx', subx, 'suby', suby);

    case 'SP_BS_FullCompens_Func_CI1'
        SonarBS_LambertCorrection = 1;
        [flag, a] = SonarBSCompens(this.Images, this.indImage, 2, [], 1, SonarBS_LambertCorrection, ...
            'subx', subx, 'suby', suby);

    case 'SP_BS_FullCompens_Table_CI1'
        SonarBS_LambertCorrection = 1;
        [flag, a] = SonarBSCompens(this.Images, this.indImage, 2, [], 2, SonarBS_LambertCorrection, ...
            'subx', subx, 'suby', suby);
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
