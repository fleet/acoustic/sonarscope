function this = callback_SP_BS_Calib_CheckCompens(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_BS_CheckCompensDiagTx_CI1'
        [flag, a] = SonarBestBSIfremer(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);
        
    case 'SP_BS_CheckFullCompens_CI1'
        [flag, a] = SonarFullCompensation(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);
        
    case 'SP_BS_CheckCompens_SignalLevel_CI1'
        [flag, indLayerRange] = paramsCalibrationSignalLevel(this.Images, this.indImage);
        if flag
            [flag, a] = SonarSignalLevel(this.Images, this.indImage, 'indLayerRange', indLayerRange, ...
                'subx', subx, 'suby', suby);
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
