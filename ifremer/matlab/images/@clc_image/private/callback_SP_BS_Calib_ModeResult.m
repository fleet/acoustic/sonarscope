function this = callback_SP_BS_Calib_ModeResult(this, msg, subx, suby, nomZoneEtude)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_BS_Calib_ModeResult_BSConstructeur_CI1'
        [~, indLayerRange] = findAnyLayerRange(this.Images, this.indImage);
        [flag, a] = SonarBestBSConstructeur(this.Images, this.indImage, ...
            'Range', this.Images(indLayerRange), ...
            'subx', subx, 'suby', suby);
        
    case 'SP_BS_Calib_ModeResult_ComputeAndModel_CI1'
         [flag, this.Images] = SonarBSCompute(this.Images, this.indImage, subx, suby, nomZoneEtude, ...
             'TakeTxAngle', 0, 'EnteteNomCourbe', 'BS_Step2', 'NoPlots');
         if flag
             [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS', 'LastOne', 1);
             if flag && ~isempty(sub)
                 [this.Images(this.indImage), nbCurvesCreated] = cleanCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'NoPlot'); %#ok<ASGLU>
                 this.Images(this.indImage) = optim_BS(this.Images(this.indImage), 'Last', 'NewCurve', 0);
                 plotCourbesStats(this.Images(this.indImage), 'sub', -1, 'Stats', 0, 'Tag', 'BS');
%                  [residuals{k}, x{k}] = compute_residuals(model); %#ok<AGROW>
             end
         end
         
    case 'SP_BS_Calib_ModeResult_ImportModelMat_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', this.repImport, 'ChaineIncluse', 'BS');
        if flag
            [flag, this.Images(this.indImage)] = import_CourbesStats(this.Images(this.indImage), nomFic, 'Tag', 'BS');
        end
        
    case 'SP_BS_Calib_ModeResult_ImportModelParameters_CI0'
        [flag, this.Images(this.indImage)] = import_BSModelParameters(this.Images(this.indImage));
        
    case 'SP_BS_Calib_ModeResult_ImportExternalMeasurements_CI0'
        [flag, this.Images(this.indImage), this.repImport] = import_CourbesStatsBSExternalMeasurements(this.Images(this.indImage), 'RepDefaut', this.repImport);
        
    case 'SP_BS_Calib_ModeResult_RedoModel_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(sub)
            this.Images(this.indImage) = optim_BS(this.Images(this.indImage), 'sub', sub, 'NewCurve', 1);
%             plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'Tag', 'BS'); % Modif JMA le 04/10/2015
            plotCourbesStats(this.Images(this.indImage), 'sub', -length(sub), 'Stats', 0, 'Tag', 'BS'); % Modif JMA le 04/10/2015
        end
         
    case 'SP_BS_Calib_ModeResult_RedoModelAllCurvesTogether_CI0'
%         ImageName = this.Images(this.indImage).Name;
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'BS');
        if flag && ~isempty(sub)
%             this.Images(this.indImage) = update_Name(this.Images(this.indImage), 'Name', ImageName, 'Append', 'Step2', 'regexprep', {'_Step.'; ''});
            this.Images(this.indImage) = optimCourbesStatsAllCurvesTogether(this.Images(this.indImage), 'BS', 'sub', sub, 'NewCurve', 0);
            plotCourbesStats(this.Images(this.indImage), 'sub', -2, 'Stats', 0, 'Tag', 'BS');
        end
        
    case 'SP_BS_Calib_ModeResult_Export2_CI0'
        [flag, nomFicSave, sub, this.repExport] = params_SonarBsExport(this.Images(this.indImage), this.repExport);
        if flag
            exportCourbesStats(this.Images(this.indImage), nomFicSave, 'sub', sub, 'Tag', 'BS');
        end
        
    case 'SP_BS_Calib_ModeResult_ExtractResiduals_CI0'
        [flag, sub, nbCourbesStatistiques] = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
            this.Images(this.indImage) = extractResidualsCourbesStats(this.Images(this.indImage), 'sub', sub);
            subResiduals = (nbCourbesStatistiques+1):(nbCourbesStatistiques+length(sub));
            plotCourbesStats(this.Images(this.indImage), 'sub', subResiduals, 'Stats', 0);
        end
        
    case 'SP_BS_Calib_ModeResult_DiagTx_CI1'
        ImageName = this.Images(this.indImage).Name;
        [flag, a] = SonarEtatDiagTx(this.Images, this.indImage, subx, suby);
        if flag
            a = update_Name(a, 'Name', ImageName, 'Append', 'Step3', 'regexprep', {'_Step.'; ''});
        end
        
    case 'SP_BS_Calib_ModeResult_DiagTxComputeAndModel_CI0'
        [flag, this.Images] = SonarDiagTxCompute(this.Images, this.indImage, subx, suby, nomZoneEtude); %, 'NoPlots');
        if flag
            [this.Images(this.indImage), nbCurvesCreated] = cleanCourbesStats(this.Images(this.indImage), 'last', 'Stats', 0, 'NoPlot'); %#ok<ASGLU>
            this.Images(this.indImage) = optim_DiagTx(this.Images(this.indImage), 'Last');
            this.Images(this.indImage) = create_compensationCurveFromNewBscorrAndAppliedDiagTx(this.Images(this.indImage));
        end
        
    case 'SP_BS_Calib_ModeResult_DiagTxImportModel_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', this.repImport, 'ChaineIncluse', 'DiagTx');
        if flag
            [flag, this.Images(this.indImage)] = import_CourbesStats(this.Images(this.indImage), nomFic, 'Tag', 'DiagTx');
        end
        
    case 'SP_BS_Calib_ModeResult_DiagTxRedoModelAllCurvesTogether_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx');
        if flag && ~isempty(sub)
            this.Images(this.indImage) = optimCourbesStatsAllCurvesTogether(this.Images(this.indImage), 'DiagTx', 'sub', sub);
            plotCourbesStats(this.Images(this.indImage), 'last', 'Stats', 0, 'Tag', 'DiagTx');
        end
        
    case 'SP_BS_Calib_ModeResult_DiagTxRedoModel_CI0'
%         ImageName = this.Images(this.indImage).Name;
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx');
        if flag && ~isempty(sub)
            % TODO : attention, il faut peut-�tre s'assurer que le mode du
            % sondeur n'a pas chang� par rapport � la courbe
            this.Images(this.indImage) = optim_DiagTx(this.Images(this.indImage), 'sub', sub, 'NewCurve', 1); % 'Last');
            this.Images(this.indImage) = create_compensationCurveFromNewBscorrAndAppliedDiagTx(this.Images(this.indImage));
        end
        
    case 'SP_BS_Calib_ModeResult_DiagTxComputeAndModelFactory_CI0'
        %         ImageName = this.Images(this.indImage).Name;
        [flag, this.Images] = SonarDiagTxCompute(this.Images, this.indImage, subx, suby, nomZoneEtude, 'NoPlots');
        if flag
            this.Images(this.indImage) = optim_DiagTx(this.Images(this.indImage), 'Last', 'OrigineModel', 1);
%             this.Images(this.indImage) = update_Name(this.Images(this.indImage), 'Name', ImageName, 'Append', 'Step5');
            Sonar = get(this.Images(this.indImage), 'SonarDescription');
            plot_Tx(Sonar, 'Constructeur');
        end

    case 'SP_BS_Calib_ModeResult_DiagTxExport2_CI0'
        [flag, sub, nomFicSave, this.repExport] = params_ExportDiagTxCurves(this.Images(this.indImage), this.repExport);
        if flag
            exportCourbesStats(this.Images(this.indImage), nomFicSave, 'sub', sub, 'Tag', 'DiagTx');
        end
                
    case 'SP_BS_Calib_ModeResult_DiagTxExportBSCorr_CI0'
        [flag, sub, nomFicIn, nomFicOut, SounderName, this.repImport, this.repExport] = params_ExportBSCorr(this.Images(this.indImage), this.repImport, this.repExport);
        if flag
%             export_BSCorr(this.Images(this.indImage), nomFicIn, nomFicOut, 'sub', sub, 'Tag', 'DiagTx');
            switch SounderName
                case {'EM302'; 'EM122'}
                    export_BSCorr_Poly(this.Images(this.indImage), nomFicIn, nomFicOut, 'sub', sub);
                case {'EM710'; 'EM712'; 'EM2040'; 'EM304'}
                    export_BSCorr_Spline(this.Images(this.indImage), nomFicIn, nomFicOut, 'sub', sub);
            end
        end
        
    case 'SP_BS_Calib_ModeResult_DiagTxImportBSCorr_CI0'
        [flag, nomFicBSCorr, subConfigs, this.repImport] = params_ImportBSCorr(this.Images, this.indImage, this.repImport);
        if flag
            [flag, this.Images(this.indImage)] = import_BSCorr(this.Images(this.indImage), nomFicBSCorr, 'sub', subConfigs);
        end
        
    case 'SP_BS_Calib_ModeResult_CreateEmptyEM304Bscorr_CI0'
        flag = create_EM304EmptyBSCorrFile;
        
    case 'SP_BS_Calib_ModeResult_RepairBscorr_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.txt', 'RepDefaut', this.repImport, 'ChaineIncluse', 'BS');
        if flag
            flag = repair_BSCorrFile(nomFic);
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
