function this = callback_SP_CreateLayers(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_CreateLayers_RxBeamAngles_CI1'
        [flag, SonarFamily, useRoll, indAcrossDistance] = paramsSonarLateralAngleEmission(this.Images, this.indImage);
        if flag
            switch SonarFamily
                case 1 % SonarLateral
                    a = sonar_lateral_emission(this.Images(this.indImage), 'useRoll', useRoll, ...
                        'subx', subx, 'suby', suby);
                case -2
                    [flag, a] = create_RxBeamAngle(this.Images(this.indImage));
                otherwise % 'SondeurMultiFaisceau'
                    [flag, a] = sonar_Caraibes_estimationTxAngle(this.Images(this.indImage), this.Images(indAcrossDistance), ...
                        'subx', subx, 'suby', suby);
            end
        end
        
    case 'SP_CreateLayers_Incidence_CI1'
        [flag, a] = sonarAngleIncidence(this.Images, this.indImage, subx, suby, 'CreateLayersPente', 1);
        
    case 'SP_CreateLayers_MultiImagesIncidenceAngle_CI0'
        [flag, indLayersTxAngle, indLayersHeading, indLayerBathy] = paramsMultiImagesIncidenceAngle(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_calculAngleIncidence(this.Images(indLayersTxAngle), ...
                this.Images(indLayerBathy), this.Images(indLayersHeading));
        end

    case 'SP_CreateLayers_SecteurEmission_CI1'
        [flag, a] = sonarSecteurEmission(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);

    case 'SP_CreateLayers_LatLon_CI1'
        [flag, a] = sonarCalculCoordGeo(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);

    case 'SP_CreateLayers_XY_CI1'
        [flag, a] = sonarCalculCoordXY(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);

    case 'SP_CreateLayers_TempsUniversel_CI1'
        [flag, a] = sonar_lateral_tempsUniversel(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
