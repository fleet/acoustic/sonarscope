function this = callback_SP_DiagRx(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

flag = 0;
a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_DiagRx_ConstructeurAnalytique_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagRxConstructeur(this.Images, this.indImage); %#ok<ASGLU>
        if flag
            a = sonar_diag_Rx(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerReceptionBeam), ...
                'OrigineModeles', 1, ...
                'TypeDiagramme',  1, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_DiagRx_ConstructeurTable_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagRxConstructeur(this.Images, this.indImage); %#ok<ASGLU>
        if flag
            a = sonar_diag_Rx(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerReceptionBeam), ...
                'OrigineModeles', 1, ...
                'TypeDiagramme',  2, ...
                'subx', subx, 'suby', suby);
        end
    
    case 'SP_DiagRx_PlotConstructeur_CI0'
        Sonar = get(this.Images(this.indImage), 'SonarDescription');
        plot_Rx(Sonar, 'Constructeur');

    case 'SP_DiagRx_IfremerAnalytiqueTheorique_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagRxConstructeur(this.Images, this.indImage); %#ok<ASGLU>
        if flag
            a = sonar_diag_Rx(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerReceptionBeam), ...
                'OrigineModeles', 2, ...
                'TypeDiagramme',  1, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_DiagRx_IfremerAnalytiqueLayer_CI1'
        [flag, indLayerEmission, indLayerRxBeamAngle] = paramsFonctionSonarDiagRxLayer(this.Images, this.indImage);
        if flag
            a = sonar_diag_Rx_Layer(this.Images(indLayerEmission), this.Images(indLayerRxBeamAngle), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_DiagRx_IfremerTable_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagRxConstructeur(this.Images, this.indImage); %#ok<ASGLU>
        if flag
            a = sonar_diag_Rx(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerReceptionBeam), ...
                'OrigineModeles', 2, ...
                'TypeDiagramme',  2, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_DiagRx_PlotIfremer_CI0'
        Sonar = get(this.Images(this.indImage), 'SonarDescription');
        plot_Rx(Sonar, 'Calibration');

    case 'SP_DiagRx_CompensAucune_CI1'
        [flag, a] = SonarDiagRxCompensAucune(this.Images, this.indImage, subx, suby);

    case 'SP_DiagRx_CompensConstructeurAnalytique_CI1'
        [flag, a] = SonarDiagRxCompens(this.Images, this.indImage, subx, suby, 1, 1);

    case 'SP_DiagRx_CompensConstructeurTable_CI1'
        [flag, a] = SonarDiagRxCompens(this.Images, this.indImage, subx, suby, 1, 2);

    case 'SP_DiagRx_CompensIfremerAnalytique_CI1'
        [flag, a] = SonarDiagRxCompens(this.Images, this.indImage, subx, suby, 2, 1);

    case 'SP_DiagRx_CompensIfremerTable_CI1'
        [flag, a] = SonarDiagRxCompens(this.Images, this.indImage, subx, suby, 2, 2);

               
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
