function this = callback_SP_DiagTx(this, msg, subx, suby, nomZoneEtude)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_DiagTx_ConstructeurAnalytique_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagTxConstructeur(this.Images, this.indImage);%#ok
        if flag
            a = sonar_diag_Tx(this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerEmissionBeam), ...
                'OrigineModeles', 1, ...
                'TypeDiagramme',  1, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_DiagTx_ConstructeurTable_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagTxConstructeur(this.Images, this.indImage);%#ok
        if flag
            [a, flag] = sonar_diag_Tx(this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerEmissionBeam), ...
                'OrigineModeles', 1, ...
                'TypeDiagramme',  2, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_DiagTx_Calcul_CI1'
        [flag, this.Images] = SonarDiagTxCompute(this.Images, this.indImage, subx, suby, nomZoneEtude); 

    case 'SP_DiagTx_Plot_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx');
        if flag && ~isempty(sub)
            plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'Tag', 'DiagTx');
        end
        
    case 'SP_DiagTx_Nettoyage_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx');
        if flag && ~isempty(sub)
            [this.Images(this.indImage), nbCurvesCreated] = cleanCourbesStats(this.Images(this.indImage), ...
                'sub', sub, 'Stats', 0); %#ok<ASGLU>
        end
        
    case 'SP_DiagTx_Filter_CI0'
        [flag, sub, nbCourbesStatistiques]  = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
            [flag, Ordre, Wc] = SaisieParamsButterworth(2, 0.02);
            if flag
                this.Images(this.indImage) = filtreCourbesStats(this.Images(this.indImage), ...
                    'sub', sub, 'Stats', 0, 'Ordre', Ordre, 'Wc', Wc);
                
                subFiltree = nbCourbesStatistiques:(nbCourbesStatistiques-1+length(sub));
                plotCourbesStats(this.Images(this.indImage), 'sub', [sub subFiltree], 'Stats', 0);
            end
        end

    case 'SP_DiagTx_Ajust_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx', 'XYLimZoom', this.visuLim(this.indImage, :));
        if flag && ~isempty(sub)
            this.Images(this.indImage) = optim_DiagTx(this.Images(this.indImage), 'sub', sub);
        end

    case 'SP_DiagTx_2Ifremer_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx', 'XYLimZoom', this.visuLim(this.indImage, :));
        if flag && ~isempty(sub)
            this.Images(this.indImage) = CourbesStats2Model(this.Images(this.indImage), 'DiagTx', 'sub', sub);
        end

    case 'SP_DiagTx_Delete_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx');
        if flag && ~isempty(sub)
            this.Images(this.indImage) = deleteCourbesStats(this.Images(this.indImage), 'sub', sub);
        end

    case 'SP_DiagTx_Export_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'Tag', 'DiagTx');
        if flag && ~isempty(sub)
            nomFic = fullfile(this.repExport, 'DiagTxCurve.mat');
            [flag, nomFicSave] = my_uiputfile('*.mat', 'Give a file name', nomFic);
            if ~flag
                return
            end
            exportCourbesStats(this.Images(this.indImage), nomFicSave, 'sub', sub, 'Tag', 'DiagTx');
            this.repExport = fileparts(nomFicSave);
        end
        
    case 'SP_DiagTx_Import_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', this.repImport, 'ChaineIncluse', 'DiagTx');
        if flag
            [flag, this.Images(this.indImage)] = import_CourbesStats(this.Images(this.indImage), nomFic, 'Tag', 'DiagTx');
        end
        
    case 'SonarDiagTx_PlotConstructeur_CI0'
        [flag, Sonar, Time] = params_SonarPlotDiagTxConstructeur(this.Images(this.indImage));
        if flag
            [flag, TabiMode_1, TabiMode_2] = my_listdlgMultipleModes(Sonar, 'Time', Time);    
            for k=1:length(TabiMode_1)
                if isempty(TabiMode_2)
                    Sonar = set(Sonar, 'Sonar.Mode_1', TabiMode_1(k), 'Time', Time);
                else
                    Sonar = set(Sonar, 'Sonar.Mode_1', TabiMode_1(k), 'Sonar.Mode_2', TabiMode_2(k), 'Time', Time);
                end
                plot_Tx(Sonar, 'Constructeur');
            end
        end

    case 'SonarDiagTx_IfremerAnalytique_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagTxConstructeur(this.Images, this.indImage);%#ok
        if flag
            [a, flag] = sonar_diag_Tx(this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerEmissionBeam), ...
                'OrigineModeles', 2, ...
                'TypeDiagramme',  1, ...
                'subx', subx, 'suby', suby);
        end

    case 'SonarDiagTx_IfremerTable_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission, indLayerEmissionBeam, indLayerReceptionBeam] = paramsFonctionSonarDiagTxConstructeur(this.Images, this.indImage);%#ok
        if flag
            [a, flag] = sonar_diag_Tx(this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                this.Images(indLayerEmissionBeam), ...
                'OrigineModeles', 2, ...
                'TypeDiagramme',  2, ...
                'subx', subx, 'suby', suby);
        end

    case 'SonarDiagTx_CompensAucune_CI1'
        [flag, a] = SonarDiagTxCompensAucune(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);

    case 'SonarDiagTx_CompensConstructeurAnalytique_CI1'
        [flag, a] = SonarDiagTxCompens(this.Images, this.indImage, 1, 1, ...
            'subx', subx, 'suby', suby);

    case 'SonarDiagTx_CompensConstructeurTable_CI1'
        [flag, a] = SonarDiagTxCompens(this.Images, this.indImage, 1, 2, ...
            'subx', subx, 'suby', suby);

    case 'SonarDiagTx_CompensIfremerAnalytique_CI1'
        [flag, a] = SonarDiagTxCompens(this.Images, this.indImage, 2, 1, ...
            'subx', subx, 'suby', suby);

    case 'SonarDiagTx_CompensIfremerTable_CI1'
        [flag, a] = SonarDiagTxCompens(this.Images, this.indImage, 2, 2, ...
            'subx', subx, 'suby', suby);
        
    case 'SonarDiagTx_PlotIfremer_CI0'
        [flag, Sonar, Time] = params_SonarPlotDiagTxConstructeur(this.Images(this.indImage));
        if flag
            [flag, TabiMode_1, TabiMode_2] = my_listdlgMultipleModes(Sonar, 'Time', Time);
            for k=1:length(TabiMode_1)
                if isempty(TabiMode_2)
                    Sonar = set(Sonar, 'Sonar.Mode_1', TabiMode_1(k), 'Time', Time);
                else
                    Sonar = set(Sonar, 'Sonar.Mode_1', TabiMode_1(k), 'Sonar.Mode_2', TabiMode_2(k), 'Time', Time);
                end
                plot_Tx(Sonar, 'Calibration');
            end
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
