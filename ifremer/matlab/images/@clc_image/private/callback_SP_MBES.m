function this = callback_SP_MBES(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

I0 = cl_image_I0;

%% Lancement des traitements

flag = 0;
a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_MBES_WaveletFilter_CI1' % Modif GLT
         flag = testSignature(this.Images(this.indImage), ...
             'DataType', cl_image.indDataType('Bathymetry'),...
             'GeometryType', 'PingXxxx');         
         if flag
             a = filtre_BathyWavelet(this.Images(this.indImage), ...
                 'subx', subx, 'suby', suby);
         end

%     case 'SP_MBES_SpikeRemoval_CI1' % Pas appel�
%         [flag, window] = saisie_window([3 3], 'Titre', 'Window size for median filter');
%         if flag
%             [flag, a] = sonar_filtreBathySpeculaire(this.Images(this.indImage), 'window', window, ...
%                 'subx', subx, 'suby', suby);
%         end
       
    case 'SP_MBES_SpikeRemovalJMA_CI1'
        Unit = this.Images(this.indImage).Unit;
        [flag, Threshold, RestoreThreshold, Margin, OtherImages] = SSc.Params.SpikeRemovalJMA('Unit', Unit);
        if flag
            a = filterSpikeBathy(this.Images(this.indImage), Threshold, ...
                'RestoreThreshold', RestoreThreshold, 'Margin', Margin, 'OtherImages', OtherImages, ...
                'subx', subx, 'suby', suby);
        end
    
    case 'SP_MBES_MoustacheRemoval_CI1'
        [flag, indTxAngle, AngleMoustache] = paramsMBESMoustacheRemoval(this.Images, this.indImage);
        if flag
            [flag, a] = Sonar_MoustacheRemoval(this.Images(this.indImage), this.Images(indTxAngle), AngleMoustache, ...
                'subx', subx, 'suby', suby);
        end
    
    case 'SP_MBES_ProjectionX_CI1'
        [flag, indReceptionBeam, indAcrossDistance, indRange, indLayers, resolutionX, MasquageDual] ...
            = params_MBES_ProjectionX(this.Images, this.indImage, suby);
        if flag
            [flag, a] = sonar_MBES_projectionX(this.Images(this.indImage), ...
                this.Images(indReceptionBeam), ...
                this.Images(indAcrossDistance), ...
                this.Images(indRange), ...
                this.Images(indLayers), ...
                'resolutionX', resolutionX, 'MasquageDual', MasquageDual, ...
                'subx', subx, 'suby', suby);
            
            if (length(a) == 1) && ~isempty(indLayers)
                PingCounter               = get(this.Images(this.indImage), 'PingCounter');
                AcrossDistancePingCounter = get(this.Images(indLayers(1)), 'PingCounter');
                
        % TODO : ATTENTION danger, il faut certainement faire
        % PingCounter(suby,1), AcrossDistancePingCounter(:,2), ...
                [~, subAcrossDistancePingCountere] = intersect3(PingCounter(suby), ...
                    AcrossDistancePingCounter, AcrossDistancePingCounter);
                
                [flag, b] = sonar_MBES_projectionX(this.Images(indLayers(1)), ...
                    this.Images(indLayers(1)), ...
                    this.Images(indAcrossDistance), ...
                    this.Images(indRange), ...
                    this.Images(indLayers(2:end)), ...
                    'resolutionX', resolutionX, 'MasquageDual', MasquageDual, ...
                    'suby', subAcrossDistancePingCountere);
                a = [a b];
            end
        end

        
    case 'SP_MBES_ImportLayer_CI1'
        [flag, a] = Sonar_importLayerFromOtherGeometry(this.Images, this.indImage, subx, suby);

    case 'SP_MBES_Range2Depth_CI1'
        [~, SonarName] = get_SonarDescription(this.Images(this.indImage));
        % {
        % En cours de test
        % Je ne suis pas encore parvenu � obtenir quelque-chose de bon pour l'EM122 contrairement � l'appel de sonar_range2depth_EM122
        switch SonarName
            % TODO : faire une seule fonction
            case {'EM3002S'; 'EM3002D'; 'EM1002'} % Tester si algo EM122 donne le m�me r�sultat
                [flag, indLayerTxAngle, indLayerRollRx, indLayerPitchRx, ~] = params_MBES_Range2Depth_EMV1f(this.Images, this.indImage);
                indLayerTxTiltAngle = [];
                
            case {'EM122'; 'EM710'; 'EM302'; 'EM2040'; 'ME70'; 'EM2045'; 'EM2040D'; 'EM2040S'} % TODO : remove 'EM2040'
                % TODO : dans le debugeur paramsMBES_Range2Depth_EMV2, reimposer le DataType � BeamPointingAngle 
                [flag, indLayerTxAngle, indLayerTxTiltAngle, indLayerRollRx, indLayerPitchRx] = params_MBES_Range2Depth_EMV2(this.Images, this.indImage);
                
            case {'Reson7111'; 'Reson7150'; 'Reson7125_200kHz'; 'Reson7150_12kHz'; 'Reson7150_24kHz'} % TODO 12/24
                [flag, indLayerTxAngle, indLayerRollRx, indLayerPitchRx] = params_MBES_Range2Depth_Reson(this.Images, this.indImage);
                indLayerTxTiltAngle = [];
                
            case {'Norbit'} % TODO 12/24
                [flag, indLayerTxAngle] = params_MBES_Range2Depth_Reson(this.Images, this.indImage);
                indLayerTxTiltAngle = [];
                indLayerRollRx      = [];
                indLayerPitchRx     = [];
                
            otherwise
                str1 = sprintf('%s pas dans la liste de case SonarMBES_Range2Depth, envoyez ce message � sonarscope@ifremer.fr', SonarName);
                str2 = sprintf('%s not in SonarMBES_Range2Depth yet, please send this message to sonarscope@ifremer.fr', SonarName);
                my_warndlg(Lang(str1,str2), 1);
        end
        if flag
            [flag, a] = sonar_range2depth_EM(this.Images(this.indImage), ...
                this.Images(indLayerTxAngle), ...
                this.Images(indLayerRollRx), ...
                this.Images(indLayerPitchRx), ...
                this.Images(indLayerTxTiltAngle), ...
                'subx', subx, 'suby', suby);
            
            % Control of results
            if flag
                switch SonarName
                    case {'Reson7125_200kHz'; 'Norbit'} % Am�liorer tout �a
                    otherwise
                        sonar_range2depth_EM_control(this.Images, this.indImage, a)
                end
            end
        end
        % }
        
        % TODO : int�grer dans une fonction g�n�rale ?
        switch SonarName
            case 'GeoSwath'
                [flag, ProfilCelerite, InstallationParameters, this.repImport] = params_RDF_Range2Depth(this.Images(this.indImage), this.repImport);
                if flag
                    [flag, a] = sonar_range2depth_Geoswath(this.Images(this.indImage), ProfilCelerite, InstallationParameters);
                end
        end
        
    case 'SP_MBES_MasqueNbBeams_CI1'
        flag = testSignature(this.Images(this.indImage), 'GeometryType', cl_image.indGeometryType('PingBeam'));
        if flag
            [flag, SeuilNbBeams] = GetParams_SonarMBES_MasqueNbBeams(this.Images(this.indImage));
            if flag
                [flag, a] = sonar_masqueFromNbBeams(this.Images(this.indImage), 'SeuilNbBeams', SeuilNbBeams, ...
                    'suby', suby);
            end
        end
        
    case 'SP_MBES_MaskDualRecover_CI1'
        ident = cl_image.indDataType('AcrossDist');
        flag = testSignature(this.Images(this.indImage), 'DataType', ident, 'GeometryType', cl_image.indGeometryType('PingBeam'));
        if flag
            [flag, a] = sonar_masqueRecoveringBeams(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_MBES_ImproveHeaveCorrection_CI1'
        ident = cl_image.indDataType('Bathymetry');
        flag = testSignature(this.Images(this.indImage), 'DataType', ident, 'GeometryType', cl_image.indGeometryType('PingBeam'));
        if flag
            [flag, a] = sonar_improveHeaveCorrection(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_MBES_AttitudeAnalysisRoll_CI1'
        [flag, indLayerAcrossDist] = params_MBESAttitudeAnalysisRoll(this.Images, this.indImage);
        if flag
            flag = sonar_MBESAttitudeAnalysisRoll(this.Images(this.indImage), this.Images(indLayerAcrossDist), ...
                'Roll', 'subx', subx, 'suby', suby);
        end
        
    case 'SP_MBES_AttitudeAnalysisPitch_CI1'
        [flag, indLayerAcrossDist] = params_MBESAttitudeAnalysisRoll(this.Images, this.indImage);
        if flag
            flag = sonar_MBESAttitudeAnalysisRoll(this.Images(this.indImage), this.Images(indLayerAcrossDist), ...
                'Pitch', 'subx', subx, 'suby', suby);
        end
        
    case 'SP_MBES_AttitudeAnalysisHeave_CI1'
        [flag, indLayerAcrossDist] = params_MBESAttitudeAnalysisRoll(this.Images, this.indImage);
        if flag
            flag = sonar_MBESAttitudeAnalysisRoll(this.Images(this.indImage), this.Images(indLayerAcrossDist), ...
                'Heave', 'subx', subx, 'suby', suby);
        end
        
    case 'SP_MBES_MasqueDetection_CI1'
        [flag, Step, SeuilProba] = params_MBES_maskFromAmplitudeDetection(this.Images(this.indImage));
        if flag
            [flag, a] = sonar_masqueFromPhaseAmplitude(this.Images(this.indImage), ...
            'SeuilProba', SeuilProba, 'Step', Step, ...
            'suby', suby);
        end
        
    case 'SP_MBES_MasqueAmplitudeNbsamples_CI1'
        ident1 = cl_image.indDataType('DetectionType');
        ident2 = cl_image.indDataType('Unknown');
        flag = testSignature(this.Images(this.indImage), 'DataType', [ident1, ident2], ...
            'GeometryType', cl_image.indGeometryType('PingBeam'));
        if flag
            [flag, a, pppp] = sonar_masqueFromNbSamples(this.Images(this.indImage), ...
                'suby', suby);%#ok.
        end
     
    case 'SP_MBES_ProbaDetection_CI1'
        ident1 = cl_image.indDataType('DetectionType');
        ident2 = cl_image.indDataType('KongsbergQualityFactor');
        flag = testSignature(this.Images(this.indImage), 'DataType', [ident1, ident2], ...
            'GeometryType', cl_image.indGeometryType('PingBeam'));
        if flag
            [flag, ~, a] = sonar_masqueFromPhaseAmplitude(this.Images(this.indImage), ...
                'SeuilProba',  0.5, ...
                'suby', suby);
        end
        
    case 'SP_MBES_MasqueDepth_CI1'
        [flag, ~, nbLigBlock] = paramsMasqueFromHisto(this.Images(this.indImage));
        if flag
            [flag, a, pppp] = sonar_masqueFromHisto(this.Images(this.indImage), ...
                'SeuilProba', 0.5, 'nbLigBlock', nbLigBlock, ...
                'subx', subx, 'suby', suby);
            a(2) = pppp;
        end

    case 'SP_MBES_ProbaDepth_CI1'
        ident1 = cl_image.indDataType('Bathymetry');
        flag = testSignature(this.Images(this.indImage), 'DataType', ident1);%, 'GeometryType', cl_image.indGeometryType('PingBeam'));
        if flag
            [flag, ~, a] = sonar_masqueFromHisto(this.Images(this.indImage), ...
                'SeuilProba',  0.5, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_MBES_ComputeAcrossSlopeInPingBeam_CI1'
        [flag, indAcrossDistance] = paramsMBES_ComputeAcrossBeamAngle(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_computeAcrossBeamAngle(this.Images(this.indImage), this.Images(indAcrossDistance), ...
                'subx', subx, 'suby', suby);
        end
               
    case 'SP_MBES_FilterConditional_CI1'
        [flag, indLayerConditionnel, Sigma] = params_FilterConditional(this.Images, this.indImage);
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                a = filtre_conditionnel(this.Images(this.indImage), this.Images(indLayerConditionnel), Sigma, ...
                    'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'SP_MBES_InterpolConditional_CI1'
        [flag, indLayerConditionnel, Height, Width] = params_InterpolConditional(this.Images, this.indImage);
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                a = interpol_conditionnel(this.Images(this.indImage), this.Images(indLayerConditionnel), Height, Width, ...
                    'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
