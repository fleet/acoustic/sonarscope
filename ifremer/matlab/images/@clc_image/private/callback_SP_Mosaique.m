function this = callback_SP_Mosaique(this, msg, subx, suby, nomZoneEtude, XLimCadre, YLimCadre)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_Mosaique_Assemblage_CI1'
        [flag, listeprofils, listeprofilsAngle, pasx, pasy, XLim, YLim, CoveringPriority, bufferZoneSize] = paramsMosaiqueAssemblage(this.Images, this.indImage, nomZoneEtude, XLimCadre, YLimCadre);
        if flag
            [flag, M, A] = mergeMosaics(this.Images(listeprofils), 'Angle', this.Images(listeprofilsAngle), ...
                'CoveringPriority', CoveringPriority, 'bufferZoneSize', bufferZoneSize, ...
                'pasx', pasx, 'pasy', pasy, 'XLim', XLim, 'YLim', YLim);
%                 'pasx', pasx, 'pasy', pasy);
            if flag
                a = M;
                if ~isempty(A)
                    a(2) = A;
                end
                str1 = 'Attention, le cadre g�ographique de l''image assembl�e reste identique � celui de l''image de d�part, utilisez le bouton "Vue globale (q)" pour voir l''ensemble de l''image.';
                str2 = 'Warning : the frame of the assembled image remains the same as the former one. Use the "Full extent (q)" button to see the entire image.';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'AssemblageCadre', 'TimeDelay', 60);
            end
        end

    case 'SP_Mosaique_Sonar_CI1'
        [flag, resol, indLayerLat, indLayerLon, ...
            AcrossInterpolation, AlongInterpolation, MethodeRemplissage, ...
            CreationMosAngle, indLayerEmissionAngle, LimitAngles, ...
            CreationMosTime, indLayerTime, ...
            CreationMosRange, indLayerRange, ...
            flagTideCorrection, SpecularInterpolation] = paramsMosaiqueSonar(this.Images, this.indImage, ...
            'suby', suby);
        if flag
            [flag, a] = sonarMosaique(this.Images, this.indImage, resol, ...
                'AcrossInterpolation', AcrossInterpolation, ...
                'AlongInterpolation',  AlongInterpolation, ...
                'MethodeRemplissage',  MethodeRemplissage, ...
                'indLayerLat', indLayerLat, 'indLayerLon', indLayerLon, ...
                'CreationMosAngle', CreationMosAngle, 'indLayerAngle', indLayerEmissionAngle, ...
                'CreationMosRange', CreationMosRange, 'indLayerRange', indLayerRange, ...
                'CreationMosTime',  CreationMosTime,  'indLayerTime',  indLayerTime, ...
                'TideCorrection', flagTideCorrection, 'LimitAngles', LimitAngles, ...
                'SpecularInterpolation', SpecularInterpolation, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_Mosaique_Inv_CI1'
        [flag, indLayerTxAngle, indLayerLat, indLayerLon, indLayerMos, indLayerMosTxAngle] = paramsMosaiqueSonarInv(this.Images, this.indImage);
        if flag
            [flag, a] = sonarMosaiqueInv(this.Images, this.indImage, this.Images(indLayerMos), ...
                'indLayerTxAngle', indLayerTxAngle, 'indLayerLat', indLayerLat, 'indLayerLon', indLayerLon, ...
                'LayerMosTxAngle', this.Images(indLayerMosTxAngle), ...
                'subx', subx, 'suby', suby);
       end

    case 'SP_Mosaique_Plot_CI1'
        [flag, Lon, Lat, Z, ThreeD, ExaVert, TagFig, hAxe] = params_MosaiquePlot(this.Images, this.indImage, subx, suby);
        if flag
            sonar_mosaique_plot(this.Images(this.indImage), Lat, Lon, Z, TagFig, ...
                'ThreeD', ThreeD, 'ExaVert', ExaVert, 'hAxe', hAxe, ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_Mosaique_ExportFly_CI1'
        [flag, XGeo, YGeo, Z, flyFileName] = params_ExportFly(this.Images, this.indImage, subx, suby);
        if flag
            flag = sonar_ExportFly(this.Images(this.indImage), XGeo, YGeo, Z, flyFileName, ...
                'subx', subx, 'suby', suby);
            if flag
                winopen(flyFileName);
            end
        end
        
    case 'SP_Mosaique_ChangeBasis_CI1'
        [flag, geoY_PingBeam, geoX_PingBeam, OriginPolynome, NomFicPolynome, subLayers, gridSize, this.repImport] = params_ChangeBasis(this.Images, this.indImage, subx, suby, this.repImport);
        if flag
            [flag, a] = sonar_ChangeBasis(this.Images, this.indImage, geoY_PingBeam, geoX_PingBeam, OriginPolynome, NomFicPolynome, gridSize, ...
                'subLayers', subLayers, 'subx', subx, 'suby', suby);
        end
        
    
    case 'SP_Mosaique_ExportSoundings_CI1'
        [flag, Lon, Lat, nomFicEchoes, this.repExport] = paramsMosaiqueExportSoundings(this.Images, this.indImage, subx, suby, this.repExport);
        if flag
            sonar_mosaique_ExportSoundings(this.Images(this.indImage), Lat, Lon, nomFicEchoes, ...
                'subx', subx, 'suby', suby);
        end
    
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
