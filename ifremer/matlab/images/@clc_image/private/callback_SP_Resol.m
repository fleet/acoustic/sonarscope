function this = callback_SP_Resol(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

flag = 0;
a    = cl_image.empty;

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_Resol_Along_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarResol(this.Images, this.indImage);
        if flag
            a = sonar_resol_long(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_Resol_ImageAcross_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarResol(this.Images, this.indImage);
        if flag
            a = sonar_resol_trans_image(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_Resol_BathyAcross_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarResol(this.Images, this.indImage);
        if flag
            a = sonar_resol_trans_bathy(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_Resol_AireImage_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarResol(this.Images, this.indImage);
        if flag
            a = sonar_aire_image(   this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_Resol_AireBathy_CI1'
        [flag, CasDepth, H, indLayerBathy, CasDistance, indLayerEmission] = paramsFonctionSonarResol(this.Images, this.indImage);
        if flag
            a = sonar_aire_bathy(this.Images(this.indImage), ...
                CasDepth, H, CasDistance, ...
                this.Images(indLayerBathy), ...
                this.Images(indLayerEmission), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_SonarResol_PlotAireImage_CI0'
        plot_aire_insonifiee_Image(this.Images, this.indImage, this.cross(this.indImage,:), this.NbVoisins);

    case 'SP_Resol_PlotAireBathy_CI0'
        plot_aire_insonifiee_Bathy(this.Images, this.indImage, this.cross(this.indImage,:), this.NbVoisins);

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
