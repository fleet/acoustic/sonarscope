function this = callback_SP_SBP(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_EK80_ConcatainImages_CI0'
        [flag, listeImages] = params_EK_ConcatainImages(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_concatenateImages(this.Images(listeImages));
        end
        
    case 'SP_EK60_ComputeHeightMagPhase_CI1'
        [flag, this.Images] = EK60_compute_heightMagPhase(this.Images, this.indImage, ...
            'subx', subx, 'suby', suby);
        if flag
            this = show_profils(this);
        end
        
    case 'SP_SBP_ComputeHeightSignalMontantDB_CI1'
        [flag, this.Images(this.indImage)] = subbottom_compute_height(this.Images(this.indImage), ...
            'TypeSignal', 2, 'ComputingUnit', 'dB', ...
            'subx', subx, 'suby', suby);
        if flag
            this = show_profils(this);
        end
        
    case 'SP_SBP_ComputeHeightSignalMontantAmp_CI1'
        [flag, this.Images(this.indImage)] = subbottom_compute_height(this.Images(this.indImage), ...
            'TypeSignal', 2, 'ComputingUnit', 'amp', ...
            'subx', subx, 'suby', suby);
        if flag
            this = show_profils(this);
        end
        
    case 'SP_SBP_ComputeHeightSignalMontantEnr_CI1'
        [flag, this.Images(this.indImage)] = subbottom_compute_height(this.Images(this.indImage), ...
            'TypeSignal', 2, 'ComputingUnit', 'enr', ...
            'subx', subx, 'suby', suby);
        if flag
            this = show_profils(this);
        end
        
    case 'SP_SBP_ComputeHeightSignalAlternatif_CI1'
        [flag, this.Images(this.indImage)] = subbottom_compute_height(this.Images(this.indImage), ...
            'TypeSignal', 1, ...
            'subx', subx, 'suby', suby);
        if flag
            this = show_profils(this);
        end
                   
    case 'SP_SBP_SuppressWaterColumn_CI1'
        [flag, Marge] = paramsSubbottomSuppressWaterColumn(this.Images(this.indImage));
        if  flag
            [flag, a] = subbottom_suppressWC(this.Images(this.indImage), 'Marge', Marge, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_SBP_Envelope_CI1'
        [flag, a] = seismicEnvelope(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);

%     case 'Seismic_Projection' % Plus utilis�
%         [flag, a] = seismicProjectionOnAPlan(this.Images(this.indImage), ...
%             'subx', subx, 'suby', suby);
        
    case 'SP_SBP_ImmersionConstante_CI1'
        [flag, a] = subbottom_ImmersionConstante(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        
    case 'SP_SBP_KeepOnlyWaterColumn_CI1'
        [flag, Marge] = paramsSubbottomSuppressWaterColumn(this.Images(this.indImage));
        if  flag
            [flag, a] = subbottom_keepOnly_waterColumn(this.Images(this.indImage), 'Marge', Marge, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_SBP_Processing_CI1'
        [flag, objParams] = ProcessingSegyDialog.paramsProcessingSegy();
        if  flag
            [flag, a] = subbottom_sbp_processing(this.Images(this.indImage), objParams, ...
                'subx', subx, 'suby', suby);
        end
                
    case 'SP_SBP_Export3DViewer_CI1'
        [flag, NomFicXML, TailleMax, typeOutput, this.repExport] = paramsExport3DV_ImagesAlongNavigation(this.Images, this.indImage, this.repExport);
        if flag
            flag = sonar_Export3DV_ImagesAlongNavigation_ByBlocks(this.Images(this.indImage), NomFicXML, TailleMax, typeOutput, ...
                'subx', subx, 'suby', suby);
        end
        
%     case 'SP_SBP_Demodulate_CI1'
%         [flag, Marge] = paramsSubbottomSuppressWaterColumn(this.Images(this.indImage));
%         if  flag
%             [flag, a] = subbottom_demodulation(this.Images(this.indImage), 'Marge', Marge, ...
%                  'subx', subx, 'suby', suby);
%         end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
