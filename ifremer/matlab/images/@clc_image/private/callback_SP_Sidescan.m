function this = callback_SP_Sidescan(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

I0 = cl_image_I0;
identTxAngle      = cl_image.indDataType('TxAngle');
identReflectivity = cl_image.indDataType('Reflectivity');

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_Sidescan_InterferoMaskComputeBathy_CI1'
        [flag, InstallationParameters] = params_geoswathComputeBathy(this.Images(this.indImage));
        if flag
            [flag, a] = geoswathComputeBathy(this.Images(this.indImage), InstallationParameters, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_Sidescan_InterferoComputeHeight_CI1'
        flag = testSignature(this.Images(this.indImage), 'DataType', identReflectivity, ...
            'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingRange')]);
        if  flag
            [flag, FiltreSonar, FiltreProfile, CoteDetection, Penetration] = paramsSonarLateralHauteur(this.Images(this.indImage), subx);
            if flag
                this.Images(this.indImage) = sonar_lateral_hauteur(this.Images(this.indImage), ...
                    'FiltreSonar',  FiltreSonar, 'FiltreProfile', FiltreProfile, ...
                    'CoteDetection', CoteDetection, 'Penetration', Penetration, ...
                    'subx', subx, 'suby', suby); %, 'Offset', 5);
                this = show_profils(this);
            end
        end

    case 'SP_Sidescan_InterferoMaskWC_CI1'
        flag = testSignature(this.Images(this.indImage), 'DataType', [identReflectivity, identTxAngle], ...
            'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingRange')]);
        if flag
            [flag, indOtherImages, nomsLayers, DataTypes] = listeLayersSynchronises(this.Images, this.indImage); %#ok<ASGLU>
            if flag
                str1 = 'Autres images � masquer ?';
                str2 = 'Other layers to be masked ?';
                [rep, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'InitialValue', []);
                if flag
                    indOtherImages = indOtherImages(rep);
                    OtherLayers = this.Images(indOtherImages);
                    a = sonar_interfero_maskWC(this.Images(this.indImage), 'OtherLayers', OtherLayers', ...
                        'subx', subx, 'suby', suby);
                end
            end
        end

    case 'SP_Sidescan_InterferoPhaseRestaurationt_CI1'
%         Simulation_interfero('stdPhaseNoise', 40)
       [flag, a] = sonar_PhaseRestauration(this.Images(this.indImage), ...
           'subx', subx, 'suby', suby);

    case 'SP_Sidescan_ComputeHeight_CI1'
        flag = testSignature(this.Images(this.indImage), 'DataType', identReflectivity, ...
            'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange')]);
        if flag
            [flag, FiltreSonar, FiltreProfile, CoteDetection, Penetration] = paramsSonarLateralHauteur(this.Images(this.indImage), subx);
            if flag
                this.Images(this.indImage) = sonar_lateral_hauteur(this.Images(this.indImage), ...
                    'FiltreSonar',  FiltreSonar, 'FiltreProfile', FiltreProfile, ...
                    'CoteDetection', CoteDetection, 'Penetration', Penetration, ...
                    'subx', subx, 'suby', suby);
                this = show_profils(this);
            end
        end
        
    case 'SP_Sidescan_PingRange2PingAcrossDist_CI1'
        flag = testSignature(this.Images(this.indImage), ...
            'GeometryType', [cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingRange')]);
        if flag
            [flag, resolutionX, createNbp] = paramsSonarLateralProjectionX(this.Images(this.indImage));
            if flag
                this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarResolutionX', resolutionX);
                a = sonarLateral_PingRange2PingAcrossDist(this.Images(this.indImage), ...
                    'resolutionX', resolutionX, 'createNbp', createNbp, ...
                    'subx', subx, 'suby', suby);
            end
        end

    case 'SP_Sidescan_LayerDepth_CI1'
        flag = testSignature(this.Images(this.indImage), 'GeometryType', cl_image.indGeometryType('PingAcrossDist'));
        if flag
            a = sonar_lateral_LayerDepth(this.Images(this.indImage), ...
                'suby', suby);
        end

    case 'SP_Sidescan_SedimentImmersion_CI1'
    	[flag, a] = sondeur_sediments_immersion(this.Images(this.indImage), ...
            'suby', suby);

        
    case 'SP_Sidescan_ExtractWCD_CI1'
        [flag, Side] = params_Sidescan_ExtractWaterColumn(this.Images(this.indImage));
        if flag
            [flag, a] = Sidescan_ExtractWaterColumn(this.Images(this.indImage), Side, ...
                'suby', suby);
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
