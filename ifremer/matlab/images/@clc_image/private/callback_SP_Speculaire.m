function this = callback_SP_Speculaire(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

flag = 0; %#ok<NASGU>

%% Lancement des traitements

a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_Speculaire_FiltreGauss_CI0'
        [flag, indLayerAngle, AngleMax, sigma] = paramsSonarSpeculaireReinterpol(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_speculaire(this.Images(this.indImage), this.Images(indLayerAngle), AngleMax, sigma, ...
                'subx', subx, 'suby', suby);
        end

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
