function this = callback_SP_TVG(this, msg, subx, suby)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

flag = 0;
a  = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_TVG_Courbes_CI0'
        sonar_TVG_Courbes(this.Images(this.indImage));

    case 'SP_TVG_RT_CI1'
        [flag, indLayerRange, indLayerAbsorptionCoeffRT] = params_SonarTVG(this.Images, this.indImage, 'AbsorptionCoeff_RT');
        if flag
            [flag, a] = sonar_TVG_RT(this.Images(this.indImage), ...
                'Range', this.Images(indLayerRange), 'AbsorptionCoeffRT', this.Images(indLayerAbsorptionCoeffRT), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_TVG_SSc_CI1'
        [flag, indLayerRange, indLayerAbsorptionCoeffSSc] = params_SonarTVG(this.Images, this.indImage, 'AbsorptionCoeff_SSc');
        if flag
            [flag, a] = sonar_TVG_SSc(this.Images(this.indImage), ...
                'Range', this.Images(indLayerRange), 'AbsorptionCoeffSSc', this.Images(indLayerAbsorptionCoeffSSc), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_TVG_User_CI1'
        [flag, indLayerRange, indLayerAbsorptionCoeffUser] = params_SonarTVG(this.Images, this.indImage, 'AbsorptionCoeff_User');
        if flag
            [flag, a] = sonar_TVG_User(this.Images(this.indImage), ...
                'Range', this.Images(indLayerRange), 'AbsorptionCoeffUser', this.Images(indLayerAbsorptionCoeffUser), ...
                'subx', subx, 'suby', suby);
        end

    case 'SP_TVG_NoCompensation_CI1'
        [flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser] = params_SonarTVG_NoCompensation(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_TVG_NoCompensation(this.Images(this.indImage), ...
                'Range', this.Images(indLayerRange), ...
                'AbsorptionCoeffRT',   this.Images(indLayerAbsorptionCoeffRT), ...
                'AbsorptionCoeffSSc',  this.Images(indLayerAbsorptionCoeffSSc), ...
                'AbsorptionCoeffUser', this.Images(indLayerAbsorptionCoeffUser), ...
                'subx', subx, 'suby', suby);
            if ~flag
                messageAboutCalibration('Tag', 'TVG')
            end
        end

    case 'SP_TVG_CompensationRT_CI1'
        [flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser] = params_SonarTVG_CompensationRT(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_TVG_CompensationRT(this.Images(this.indImage), ...
                'Range', this.Images(indLayerRange), ...
                'AbsorptionCoeffRT',   this.Images(indLayerAbsorptionCoeffRT), ...
                'AbsorptionCoeffSSc',  this.Images(indLayerAbsorptionCoeffSSc), ...
                'AbsorptionCoeffUser', this.Images(indLayerAbsorptionCoeffUser), ...
                'subx', subx, 'suby', suby);
            if ~flag
                messageAboutCalibration('Tag', 'TVG')
            end
        end

    case 'SP_TVG_CompensationSSc_CI1'
        flag = testSignature(this.Images(this.indImage), 'GeometryType', 'PingXxxx', 'noMessage');
        if flag % PingXxxx
            [flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser] = paramsFonctionSonarTVGCompensationSSc(this.Images, this.indImage);
            if flag
                [flag, a] = sonar_TVG_CompensationSSc(this.Images(this.indImage), ...
                    'Range', this.Images(indLayerRange), ...
                    'AbsorptionCoeffRT',   this.Images(indLayerAbsorptionCoeffRT), ...
                    'AbsorptionCoeffSSc',  this.Images(indLayerAbsorptionCoeffSSc), ...
                    'AbsorptionCoeffUser', this.Images(indLayerAbsorptionCoeffUser), ...
                    'subx', subx, 'suby', suby);
                if ~flag
                    messageAboutCalibration('Tag', 'TVG')
                end
            end
        else % LatLong ou GeoYX
            [flag, alpha, indBathymetry, indLayerTxAngle, alphaAvant] = paramsFonctionSonarTVGCompensationIfremer_LatLongOrGeoYX(this.Images, this.indImage);
            if flag
                this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarTVG_IfremerAlpha', alphaAvant, 'Sonar_DefinitionENCours');
                [flag, a] = sonar_TVG_CompensationIfremerLatLong(this.Images(this.indImage), ...
                    this.Images(indBathymetry), this.Images(indLayerTxAngle), alpha, ...
                    'subx', subx, 'suby', suby);
                if ~flag
                    messageAboutCalibration('Tag', 'TVG')
                end
            end
        end
        
    case 'SP_TVG_CompensationUser_CI1' % TODO
        [flag, indLayerRange, indLayerAbsorptionCoeffRT, indLayerAbsorptionCoeffSSc, indLayerAbsorptionCoeffUser] = params_SonarTVG_CompensationUser(this.Images, this.indImage);
        if flag
            [flag, a] = sonar_TVG_CompensationUser(this.Images(this.indImage), ...
                'Range', this.Images(indLayerRange), ...
                'AbsorptionCoeffRT',   this.Images(indLayerAbsorptionCoeffRT), ...
                'AbsorptionCoeffSSc',  this.Images(indLayerAbsorptionCoeffSSc), ...
                'AbsorptionCoeffUser', this.Images(indLayerAbsorptionCoeffUser), ...
                'subx', subx, 'suby', suby);
            if ~flag
                messageAboutCalibration('Tag', 'TVG')
            end
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
