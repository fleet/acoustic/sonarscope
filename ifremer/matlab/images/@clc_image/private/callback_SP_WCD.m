function this = callback_SP_WCD(this, msg, subx, suby, flagImageEntiere)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

flag = 0; %#ok<NASGU>

%% Lancement des traitements

a = cl_image.empty;
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'SP_WCD_WedgeDisplay_CI1'
        [flag, MaxImageSize, ProfilCelerite, subLayers, MaskBeyondDetection, typeAlgoWC] ...
            = paramsMBES_SampleBeam2DepthAcrossDist(this.Images(this.indImage));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
            if flag
                [flag, a] = sonar_polarEchogramPlus(this.Images(indLayers), ProfilCelerite, MaskBeyondDetection, ...
                    'MaxImageSize', MaxImageSize, 'subLayers', subLayers, ...
                    'DisplayDetection', 1, 'typeAlgoWC', typeAlgoWC, ...
                    'subx', subx, 'suby', suby);
            end
        end
        
    case 'SP_WCD_SidelobesCompensation_CI0'
        [flag, a] = compensation_WaterColumnPlus(this.Images(this.indImage));
        
    case 'SP_WCD_CreationRxBeamAngle_CI1'
        flag = params_MBES_CreationRxBeamAngleInSampleBeam(this.Images(this.indImage));
        if flag
            [flag, a] = sonar_SampleBeamRxBeamAnglesCreation(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby);
        end
        
    case 'SP_WCD_SampleBeam2DepthAcrossDist_CI1'
        [flag, ~, this.repImport, this.repExport] = sonar_ProcessWC(this.Images(this.indImage), this.repImport, this.repExport, ...
            'suby', suby); % , 'TideCorrection', 1
        
    case 'SP_WCD_Export2GLOBE_CI1'
        [flag, Names, MaskBeyondDetection, this.repExport] = params_MBES_SimradExportRawDataWC(this.Images(this.indImage), this.repExport);
        if flag
            nomFicAll = this.Images(this.indImage).InitialFileName;
            aKM = cl_simrad_all('nomFic', nomFicAll);
            flag = ALL_SimradProcessRawDataWC(aKM, this.Images(this.indImage), ...
                'MaskBeyondDetection', MaskBeyondDetection, 'Names', Names, ...
                'suby', suby);
        end
        
    case 'SP_WCD_EchoIntegration_CI1'
        [flag, nomFicXmlEchogram, params_ZoneEI, Tag, Display, this.repImport] = params_Simrad_WaterColumnEchoIntegration(this.Images(this.indImage), this.repImport);
        if flag
            [flag, a] = sonar_SimradEchoIntegration(this.Images(this.indImage), nomFicXmlEchogram, params_ZoneEI, Tag, Display, ...
                'suby', suby);
        end
   
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
