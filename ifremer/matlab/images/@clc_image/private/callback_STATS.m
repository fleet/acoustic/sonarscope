% Fonctionnalite callback_STATS specifique au composant
%
% Syntax
%   a = callback_STATS(a)
%
% Input Arguments
%   a   : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_STATS(this, varargin)

if nargin == 1
    MessageTraitementDroite
    return
end

%% Message identifiant la callback

msg = varargin{1};

%% Recherche des pixels a traiter

[flag, subx, suby, nomZoneEtude, XLimCadre, YLimCadre, flagImageEntiere] = getSubxSubyContextuel(this, msg); %#ok<ASGLU>
if ~flag
    return
end
% --------------------------------------------------------------------
% OPERATION MAINTENANCE A FAIRE ICI : tout devrait �tre programm� en
% m�thodes de cl_image comme l'est mean_col : histo1D(this, 'subx', subx,
% 'suby', suby), etc ...
% --------------------------------------------------------------------

%% Traitement des callbacks

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'STATS_General_CI1' % Statistiques elementaires
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            stats_general(this.Images, this.indImage, indLayerMask, valMask, subx, suby);
        end

    case {'STATS_Histogramme_CI1'; 'STATS_Probabilite_CI1'; 'STATS_DensiteDeProbabilite_CI1'; 'STATS_Hypsometric_CI1'}
        this.Images(this.indImage) = stats_histo(this.Images, this.indImage, subx, suby, msg);
        
    case 'STATS_MelangeDeGaussiennes_CI1'  % Saisie du nombre de gaussiennes
        [flag, nbGaussiennes] = params_STATS_MelangeGaussiennes;
        if flag
            this.Images(this.indImage) = stats_GaussMixture(this.Images(this.indImage), nbGaussiennes, 'subx', subx, 'suby', suby);
        end

    case 'STATS_EQM_CI1'  % Erreur Quadratique Moyenne
        [flag, Selection] = selectionLayersSynchronises(this.Images, this.indImage, 'Max', 1);
        if flag && ~isempty(Selection)
            choixLayer = find([Selection(:).indLayer] ~= 0);
            choixLayer = Selection(choixLayer).indLayer; %#ok<FNDSB>
            E = eqm(this.Images(this.indImage), this.Images(choixLayer), 'subx', subx, 'suby', suby);
            Titre1 = this.Images(this.indImage).Name;
            Titre2 = this.Images(choixLayer).Name;
            str = sprintf('Image 1 : %s\nImage 2 : %s\nEQM : %s', Titre1, Titre2, num2str(E));
            fprintf('%s\n', str);
            msgbox(str)
        end
        
    case 'STATS_Histo2D_CI1'  % Calcul et affichage de l'histogramme 2D
        [flag, indLayerConditional, indLayerMask, valMask, binsI1, binsI2] = params_Histo2D(this.Images, this.indImage);
        if flag
            a = histo2D(this.Images(this.indImage), this.Images(indLayerConditional), ...
                'binsI1', binsI1, 'binsI2', binsI2, 'subx', subx, 'suby', suby, ...
                'Mask', this.Images(indLayerMask), 'MaskValues', valMask);
            this = add_image(this, a);
            this = editer_image_selectionnee(this);
        end

    case 'STATS_MoyenneVert_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            val = mean_col(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask);
            this.Images(this.indImage) = memoriseCourbe(this.Images(this.indImage), val, 'X', subx, suby, ...
                'Question', 1);
        end

    case 'STATS_MedianVert_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            val = median_col(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask);
            this.Images(this.indImage) = memoriseCourbe(this.Images(this.indImage), val, 'X', subx, suby, ...
                'TypeCurve', 'Median', 'Question', 1);
        end
        
    case 'STATS_MinVert_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [val, nbPix] = min_col(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
        end
        
    case 'STATS_MaxVert_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [val, nbPix] = max_col(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
        end

    case 'STATS_MoyenneHorz_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            val = mean_lig(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask);
            this.Images(this.indImage) = memoriseCourbe(this.Images(this.indImage), val, 'Y', subx, suby, ...
                'Question', 1);
        end

    case 'STATS_MedianHorz_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            val = median_lig(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask);
            this.Images(this.indImage) = memoriseCourbe(this.Images(this.indImage), val, 'Y', subx, suby, ...
                'TypeCurve', 'Median', 'Question', 1);
        end

    case 'STATS_NbPointsVert_CI1' % sum Vert
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [val, NbPix] = mean_col(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', 'TypeCurve', 'NbPixels', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
            this.Images(this.indImage) = memoriseCourbe(this.Images(this.indImage), NbPix, 'X', subx, suby, ...
                'TypeCurve', 'NbPixels', 'Question', 1);
        end

    case 'STATS_NbPointsHorz_CI1' % sum Horz
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [val, NbPix] = mean_lig(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', 'TypeCurve', 'NbPixels', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
            this.Images(this.indImage) = memoriseCourbe(this.Images(this.indImage), NbPix, 'Y', subx, suby, ...
                'TypeCurve', 'NbPixels', 'Question', 1);
        end
        
    case 'STATS_MinHorz_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [val, nbPix] = min_lig(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
        end
        
    case 'STATS_MaxHorz_CI1'
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [val, nbPix] = max_lig(this.Images(this.indImage), 'subx', subx, 'suby', suby, 'Fig', ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
        end

    case 'STATS_RechercheMin_CI1'  % Recherche de la valeur minimale
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [ValMin, x, y, iLig, iCol, iCan] = searchMinValue(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby, ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask); %#ok<ASGLU>
            this.cross(this.indImage, :) = [x y];
            this.visuCoupeHorz = 1;
            this.visuCoupeVert = 1;
            this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
            this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
            this = show_image(this);
            this = show_profils(this);
            this = set_cross2synchronizedImages(this);
        end

    case 'STATS_RechercheMax_CI1' % Recherche de la valeur maximale
        [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
        if flag
            [~, x, y] = searchMaxValue(this.Images(this.indImage), ...
                'subx', subx, 'suby', suby, ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask);
            this.cross(this.indImage, :) = [x y];
            this.visuCoupeHorz = 1;
            this.visuCoupeVert = 1;
            this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
            this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
            this = show_image(this);
            this = show_profils(this);
            this = set_cross2synchronizedImages(this);
        end

    case 'STATS_RechercheMedian_CI1'  % Recherche de la valeur m�diane
        % Remarque : on ne passe pas de masque parce que ce serait trop
        % chi... � calculer et �a n'a pas beaucoup d'int�r�t
        [~, x, y] = searchMedianValue(this.Images(this.indImage), ...
            'subx', subx, 'suby', suby);
        this.cross(this.indImage, :) = [x y];
        this.visuCoupeHorz = 1;
        this.visuCoupeVert = 1;
        this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
        this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
        this = show_image(this);
        this = show_profils(this);
        this = set_cross2synchronizedImages(this);

    case 'STATS_RecomputeStats_CI0'  % Recalcul des stats
    	[flag, Sampling] = paramsRecomputeStats(this.Images(this.indImage));
        if flag
            this.Images(this.indImage) = compute_stats(this.Images(this.indImage), 'Sampling', Sampling);
        end

    case {'STATS_ConditionCalculFull_CI1'; 'STATS_ConditionCalculBias_CI1'; 'STATS_ConditionCalculBiasMultiple_CI1'} % Statistiques conditionnelles
        [flag, bins, Selection, CourbeBias, indLayerMask, valMask, nomCourbe, Comment, ...
            Coul, nbPMin, SelectMode, SelectSwath, SelectFM, SelectMode2EM2040, MeanCompType] = ...
            paramsStatisticsCurve(this.Images, this.indImage, nomZoneEtude, subx, suby, msg);
        if flag
            [~, this.Images] = computeCrossStatistics(this.Images, this.indImage, ...
                Selection, nomCourbe, Comment, ...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                'bins', bins, 'subx', subx, 'suby', suby, ...
                'CourbeBias', CourbeBias, 'Coul', Coul, 'nbPMin', nbPMin, ...
                'SelectMode', SelectMode, 'SelectSwath', SelectSwath, 'SelectFM', SelectFM, ...
                'MeanCompType', MeanCompType);
        end

    case 'STATS_ConditionPlot_CI0'   
        [flag, sub, ~, ~, subTypeCurve] = selectionCourbesStats(this.Images(this.indImage), 'SelectionCurveOrModel', 1);
        if flag && ~isempty(sub)
        	plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'subTypeCurve', subTypeCurve);
        end

    case 'STATS_ConditionPlotPlusOffsets_CI0'
        [flag, sub, ~, ~, Offsets, repImport] = selectionCourbesStatsPlusOffsets(this.Images(this.indImage), this.repImport);
        if flag && ~isempty(sub)
            this.repImport = repImport;
            plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'Offsets', Offsets);
        end
        
    case 'STATS_PlotAzimuth_CI0'
        [flag, sub, Azimuths, IncAngleBeg, IncAngleEnd, IncAngleStep, ReflecBeg, ReflecEnd] = selectionCourbesStatsPlotAzimuth(this.Images(this.indImage));
        if flag
        	plotCourbesStatsAzimuths(this.Images(this.indImage), Azimuths, IncAngleBeg, IncAngleEnd, IncAngleStep, ReflecBeg, ReflecEnd, 'sub', sub);
        end
        
    case 'STATS_ConditionReplaceCurveByOffsets_CI0'
        [flag, sub, ~, ~, Offsets, repImport] = selectionCourbesStatsPlusOffsets(this.Images(this.indImage), this.repImport);
        if flag && ~isempty(sub)
            this.repImport = repImport;
            this.Images(this.indImage) = replaceCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'Offsets', Offsets);
        end
        
    case 'STATS_BathyStats2QF_CI0'
        [flag, subBathy, subResidus] = params_CourbesStats2QF(this.Images(this.indImage));
        if flag
        	CourbesStats2QF(this.Images(this.indImage), subBathy, subResidus);
        end

    case 'STATS_ConditionPlotMedian_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
        	plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, 'Median', 1);
        end

    case 'STATS_ConditionSoustraction_CI0'
        [flag, subPlus, identMinus] = selectionCourbesStatsSubtract(this.Images(this.indImage), 'SelectionMode', 'Single');
        if flag
            this.Images(this.indImage) = subtractCourbesStats(this.Images(this.indImage), subPlus, identMinus);
        end
        
    case 'STATS_StatsInfo_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
        	infoCourbesStats(this.Images(this.indImage), 'sub', sub);
        end
        
    case 'STATS_PublicSet_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
            this = set_PublicCurves(this, 'sub', sub);
        end

    case 'STATS_PublicDelete_CI0'
        [flag, sub] = selectionPublicCurves(this);
        if flag
            this.PublicCurves(sub) = [];
        end

    case 'STATS_ConditionUnion_CI0' % TODO : renommer ce tag ainsi que la foncrion unionCourbesStats
        [flag, sub, TypeWeight, TypeCurve, MeanCompType] = params_unionCourbesStats(this.Images(this.indImage));
        if flag
            this.Images(this.indImage) = unionCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0, ...
                'TypeWeight', TypeWeight,  'TypeCurve', TypeCurve, 'MeanCompType', MeanCompType);
        end
        
    case 'STATS_ConditionPlotWithSigma_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
            plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 1);
        end
        
    case 'STATS_ConditionPlotWith2Sigma_CI0'
        [flag, sub]  = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
            plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 3);
        end
        
    case 'STATS_ConditionPlotSigma_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
            plotCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 2);
        end
        
    case 'STATS_ConditionClean_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'MayBeNone', 1);
        if flag && ~isempty(sub)
            this.Images(this.indImage) = cleanCourbesStats(this.Images(this.indImage), 'sub', sub, 'Stats', 0);
        end
        
%     case 'STATS_ConditionFilter_CI0' % Plus appel�
%         [flag, sub, nbCourbesStatistiques] = selectionCourbesStats(this.Images(this.indImage));
%         if flag && ~isempty(sub)
%             [flag, Ordre, Wc] = SaisieParamsButterworth(2, 0.02);
%             if flag
%                 this.Images(this.indImage) = filtreCourbesStats(this.Images(this.indImage), ...
%                     'sub', sub, 'Stats', 0, 'Ordre', Ordre, 'Wc', Wc);
%                 subFiltree = nbCourbesStatistiques+2:2:(nbCourbesStatistiques+2*length(sub));
%                 plotCourbesStats(this.Images(this.indImage), 'sub', nbCourbesStatistiques+1, 'Stats', 0);
%                 plotCourbesStats(this.Images(this.indImage), 'sub', [sub subFiltree], 'Stats', 0);
%             end
%         end
        
    case 'STATS_ConditionDelete_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage));
        if flag && ~isempty(sub)
        	this.Images(this.indImage) = deleteCourbesStats(this.Images(this.indImage), 'sub', sub);
        end
        
    case 'STATS_ConditionExport_CI0'
        [flag, nomFic, sub, this.repExport] = params_STATS_CurveExport(this.Images(this.indImage), '.mat', this.repExport);
        if flag
            exportCourbesStats(this.Images(this.indImage), nomFic, 'sub', sub);
        end

    case 'STATS_ConditionExport_ASCII_CI0'
        [flag, nomFic, sub, this.repExport] = params_STATS_CurveExport(this.Images(this.indImage), '.xml', this.repExport);
        if flag
            flag = export_xml_courbesStats(this.Images(this.indImage), nomFic, 'sub', sub); %#ok<NASGU>
            % [flag, this.Images(this.indImage)] = import_xml_courbesStats(this.Images(this.indImage), nomFic);
        end

    case 'STATS_ConditionImport_CSV_CI0'
        [flag, nomFic, indCurve, this.repImport] = params_STATS_CurveImportCsv(this.Images, this.indImage, this.repImport);
        if flag
            [~, this.Images(this.indImage)] = import_csv_courbesStats(this.Images(this.indImage), nomFic, indCurve);
        end
        
    case 'STATS_ConditionImport_ASCII_CI0'
%         [flag, sub] = selectionCourbesStats(this.Images(this.indImage));
%         if flag && ~isempty(sub)
% TODO : � modeerniser
        liste = listeFicOnDir(this.repExport, '.xml');
        if length(liste) == 1
            Filtre = liste{1};
        else
            Filtre = fullfile(this.repExport, '*.xml');
        end
        [flag, nomFic] = my_uigetfile('*.xml', 'Give a file name', Filtre);
        if flag
            str1 = 'Priorit� � :';
            str2 = 'Priority to :';
            [rep, flag] = my_questdlg(Lang(str1,str2), ...
                Lang('Fichiers binaires', 'Binary files'), ...
                Lang('Fichiers ASCII', 'ASCII files'));
            if flag
                PrioriteLectureASCII = (rep == 2);
                %           this.Images(this.indImage) = importCourbesStats_ASCII(this.Images(this.indImage), nomFic);
                [flag, this.Images(this.indImage)] = import_xml_courbesStats(this.Images(this.indImage), nomFic, ...
                    'PrioriteLectureASCII', PrioriteLectureASCII);
                if flag
                    this.repExport = fileparts(nomFic);
                    CourbesStatistiques = this.Images(this.indImage).CourbesStatistiques;
                    plotCourbesStats(this.Images(this.indImage), 'sub', length(CourbesStatistiques), 'Stats', 0);
                end
            end
        end

    case 'STATS_ConditionImport_CI0'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', this.repImport);
        if flag
            [flag, this.Images(this.indImage)] = import_CourbesStats(this.Images(this.indImage), nomFic); %#ok<ASGLU>
        end
        
    case 'STATS_ConditionRename_CI0'
        [flag, sub] = selectionCourbesStats(this.Images(this.indImage), 'SelectionMode', 'Single');
        if flag && ~isempty(sub)
            this.Images(this.indImage) = renameCourbesStats(this.Images(this.indImage), 'sub', sub);
        end
        
    case 'STATS_3MultiImagesBoxPlots_CI0'
        [flag, indLayers, NbMinPoints, indLayerMask, valMask, nomFicCsv, Sampling, flagValNat, this.repExport] ...
            = paramsMultiImagesBoxPlots(this.Images, this.indImage, this.repExport);
        if flag
            stats_boxPlots(this.Images, indLayers, NbMinPoints, XLimCadre, YLimCadre, nomFicCsv, Sampling, 'Mask', {indLayerMask; valMask}, 'ValNat', flagValNat);
        end
        
    case 'STATS_3MultiImagesCurves_CI0'
        [flag, listeprofils, listeprofilsAngle, Bins1, listeprofils2, Bins2, indLayerMask, valMask, nomCourbe] = paramsMultiImagesCurves(this.Images, this.indImage);
        if flag
            [flag, this.Images(this.indImage)] = courbe_multiImages(this.Images, this.indImage, listeprofils,  ...
                listeprofilsAngle, Bins1, listeprofils2, Bins2, ...
                nomCourbe, 'Mask', {indLayerMask; valMask}); %#ok<ASGLU>
        end
        
%     case 'STATS_3MultiImagesPointAndClickCurve_CI0'
%         % TODO
%         my_warndlg('Under work', 1);

    case 'Help'
        flag = SonarScopeHelp(varargin{2}); %#ok<NASGU>
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
end
