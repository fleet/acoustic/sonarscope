% Traitement suite a une action sur la toolbar pour ViewImage
% 
% Syntax
%   this = callback_ViewImage(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_ViewImage(this)

[flag, subx, suby] = getSubxSubyContextuel(this, '');
if ~flag
    return
end

ViewImage(this.Images(this.indImage), 'subx', subx, 'suby', suby);
