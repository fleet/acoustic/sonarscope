function this = callback_Visu(this, msg)

if nargin == 1
    return
end

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'AsAnImage'
        icone = get_icone(this, 'VisuImage');
        this.ihm.VisuImage.cpn = set_CData(this.ihm.VisuImage.cpn, IconeEncocheDroite(icone));
        
        this = callback_VisuImage(this);
        
    case 'AsCountours'
        icone = get_icone(this, 'VisuContours');
        this.ihm.VisuImage.cpn = set_CData(this.ihm.VisuImage.cpn, IconeEncocheDroite(icone));

        [this, flag] = private_imagesc(this);
        if ~flag
            return
        end
        this = callback_VisuContours(this);
end
