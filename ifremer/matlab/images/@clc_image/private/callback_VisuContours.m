% Visualisation de l'image sous forme d'isocontours
%
% Syntax
%   this = callback_VisuContours(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_VisuContours(this)

if this.Images(this.indImage).ImageType == 2
    this.identVisu(this.indImage) = 1;
    str1 = 'Une image RGB ne peut pas �tre repr�sent�e sous forme de contours.';
    str2 = 'A RGB image cannot be plot with contours.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImageRGBCannotBePlotInContours', 'TimeDelay', 60);
    return
end


lastIdentVisu = this.identVisu(this.indImage);
this.identVisu(this.indImage) = 2;

%% Calcul des niveaux si pas donn� par l'utilisateur

CLim = this.Images(this.indImage).CLim;
step = (CLim(2) - CLim(1)) / 20;
step =  nearestExpectedContourStep(step);
ContourValues = CLim(1):step:CLim(2);
ContourValues = centrage_magnetique(ContourValues);
step = ContourValues(2) - ContourValues(1);

this.Images(this.indImage).ContourValues = ContourValues;

% if (CLim(1) < 0) && (CLim(2) > 0)
%     this.Images(this.indImage) = set(this.Images(this.indImage), ...
%         'ContourValues', [get(this.Images(this.indImage), 'ContourValues'), 0]);
%     this.Images(this.indImage) = set(this.Images(this.indImage), ...
%         'ContourValues', sort(get(this.Images(this.indImage), 'ContourValues')));
% end

% def = num2str(get(this.Images(this.indImage), 'ContourValues'));
def = sprintf('%s:%s:%s', num2str(ContourValues(1)), num2str(step), num2str(ContourValues(end)));
answer = inputdlg(Lang('Niveaux', 'Levels'), Lang('Saisie des niveaux', 'Levels input'), 1, {def});
if isempty(answer)
    this.identVisu(this.indImage) = lastIdentVisu;
    return
end
ValContours = str2num(answer{1}); %#ok

if isempty(ValContours)
    my_warndlg([Lang('Valeurs non interpretables : ', 'Insignificant values ') answer{1}], 0);
    return
end

if length(ValContours) == 1
    ValContours = [ValContours ValContours];
end

if isequal(ValContours, get(this.Images(this.indImage), 'ContourValues')) && (lastIdentVisu == 2)
    return
else
    this.Images(this.indImage) = set(this.Images(this.indImage), 'ContourValues', ValContours);
end

%% Mise � jour de la visualisation

this = show_image(this);
this = show_profils(this);

% this = traiter_typeInteractionSouris(this);

