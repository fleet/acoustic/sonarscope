% Visualisation de l'image sous forme d'Image
%
% Syntax
%   this = callback_VisuImage(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_VisuImage(this)

this.identVisu(this.indImage) = 1;

this = show_image(this);
this = show_profils(this);
