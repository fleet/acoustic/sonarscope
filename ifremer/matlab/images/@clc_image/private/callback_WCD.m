function this = callback_WCD(this, msg)

if isempty(msg)
    MessageTraitementDroite
    return
end

a  = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'
I0 = cl_image_I0;

%% Lancement des traitements

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'WCD_PlumeDetection_CI0'
        [flag, ListeFicXmlEchogram, nomDirOut, typeAlgo, FrameDescription, Tag, Display, this.repImport, this.repExport] = ...
            params_WaterColumnPlumeDetection(this.Images, this.indImage, this.repImport, this.repExport);
        if flag
            process_WaterColumnPlumeDetection(this.Images, ListeFicXmlEchogram, nomDirOut, typeAlgo, FrameDescription, Tag, ...
                'Display', Display);
        end
        
    case 'WCD_GroupEchograms_CI0'
        [flag, listeFic, nomFicOut, this.repImport, this.repExport] = params_GroupEchograms(this.repImport, this.repExport);
        if flag
            flag = process_GroupEchograms(listeFic, nomFicOut);
        end
        
    case 'WCD_ZipDirXml_CI0'
        [flag, nomFicXml, this.repImport] = uiSelectFiles('ExtensionFiles', '.xml', 'RepDefaut', this.repImport);
        if flag
            % ici il faudra appeler un autre module de zip : .xml+Dir -> .ifr
            zipAssociatedDirectotyToXmlFile(nomFicXml)
        end
         
    case 'WCD_SlicesAlongNavigation_CI0' % TODO : faire case {'WCD_SlicesAlongNavigation_CI0';'ResonS7k_TagSlicesAlongNavigation'} et supprimer ce case
        [flag, nomFic, nomDirOut, Step, typeOutput, this.repImport, this.repExport] = params_SlicesAlongNavigation(I0, this.repImport, this.repExport);
        if flag
%             [~, ~, Ext] = fileparts(nomFic{1});
%             if (typeOutput == 1) && ~strcmp(Ext, '.nc') % Format de sortie en XML-Bin mais on interdit si format d'entr�e en Netcdf
            if typeOutput == 1 % Format de sortie en XML-Bin mais on interdit si format d'entr�e en Netcdf
                flag = WC_SlicesAlongNavigation(nomFic, nomDirOut, 'Step', Step);
            else % Format de sortie en Netcdf
                flag = WC_SlicesAlongNavFlyTexture(nomFic, nomDirOut, 'Step', Step);
            end
        end
         
    case 'WCD_AlongPath_CI0'
        [flag, listeFic, nomDirOut, listFicCSV, Tag, type3DFileFormat, this.repImport, this.repExport] = params_WC_AlongPath(this.repImport, this.repExport);
        if flag
            [~, ~, Ext] = fileparts(listeFic{1});
            if (type3DFileFormat == 1) && ~strcmp(Ext, '.nc') % Format de sortie en XML-Bin mais on interdit si format d'entr�e en Netcdf
                flag = WC_AlongPath(listeFic, listFicCSV, Tag, nomDirOut);
            else
                flag = WC_AlongPathFlyTexture(listeFic, listFicCSV, Tag, nomDirOut);
            end
        end
        
    case 'WCD_ExportAlongTrackInASCII_CI0'
        [flag, listeFic, nomDirOut, this.repImport, this.repExport] = params_WCExportAlongTrackInASCII(this.repImport, this.repExport);
        if flag
            WC_ExportAlongTrackInASCII(listeFic, nomDirOut)
        end
        
    case 'WCD_HorizontalSlices_CI0'
        [flag, nomFic, nomDirOut, GridSize, Step, Tag, this.repImport, this.repExport] = params_HorizontalSlices(I0, this.repImport, this.repExport);
        if flag
            flag = WC_HorizontalSlices(nomFic, GridSize, Step, Tag, nomDirOut);
        end
        
%     case 'WC_CheckFiles'
%         [flag, nomFicXml, this.repImport] = uiSelectFiles('ExtensionFiles', '.xml', 'RepDefaut', this.repImport);
%         if flag
%             WC_CheckFiles(nomFicXml)
%         end

    case 'WCD_WaterColumnCheckWCDepthAcrossDistPlayer_CI0'
        [flag, listFileNames, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
            'RepDefaut', this.repImport, 'ChaineIncluse', '_PolarEchograms', 'AllFilters', 1);
        if flag
            WC_CheckPolarEchogramDepthAcrossDist(listFileNames)
        end

    case 'WCD_WaterColumnCheckWCDepthAcrossDistNavigation_CI0'
        [flag, listFileNames, backgroundImage, this.repImport] = WCDUtils.InputParams.getDepthAcrossDistCheck2(...
            this.Images(this.indImage), this.repImport, 'PolarEchograms');
        if flag
            WC_InspectPolarEchogramDepthAcrossDist(backgroundImage, listFileNames)
        end
                
    case 'WCD_CheckSlicesAlongNavigation_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
            'RepDefaut', this.repImport, 'ChaineIncluse', '_WCSlice', 'AllFilters', 1);
        if flag
            WC_CheckSlicesAlongNavPlayer(listeFic)
        end

    case 'WCD_CheckMatrix_CI0'
        [flag, listeFic, LayerType, backgroundImage, CLim, muteUnderSeabed, this.repImport] = WCDUtils.InputParams.getMatrixCheck(...
            this.Images(this.indImage), this.repImport);
        if flag
            WCDUtils.Matrix.check(listeFic, CLim, 'backgroundImage', backgroundImage, ...
                'muteUnderSeabed', muteUnderSeabed, 'LayerType', LayerType);
        end
        
    case 'WCD_CheckMatrixMoviePings_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
            'RepDefaut', this.repImport, 'ChaineIncluse', '_WCCube', 'AllFilters', 1);
        if flag
            [flag, nomDirMovie, CLim, fps, quality, profileVideo, subPing, this.repExport] ...
                = WCDUtils.InputParams.matrixMoviePings(listeFic, this.repExport);
            if flag
                WCDUtils.Matrix.movie('Pings', listeFic, nomDirMovie, CLim, 'subPing', subPing, ...
                    'fps', fps, 'quality', quality, 'profileVideo', profileVideo);
            end
        end
        
    case 'WCD_CheckMatrixSPFEStep1_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
            'RepDefaut', this.repImport, 'ChaineIncluse', '_WCCube', 'AllFilters', 1);
        if flag
            [flag, CLim, flagUnderAntenna, subPing, AngleWidth] = WCDUtils.InputParams.matrixDisplayNadir(listeFic);
            if flag
                WCDUtils.Matrix.DisplayNadir(listeFic, CLim, flagUnderAntenna, 'subPing', subPing, 'AngleWidth', AngleWidth);
            end
        end

    case 'WCD_CheckMatrixSPFEStep2_CI0'
        [flag, listFig, showLimits] = WCDUtils.InputParams.matrixSPFEStep2;
        if flag
            WCDUtils.Matrix.SPFEStep2(listFig, showLimits)
        end

        
    case 'WCD_CheckMatrixMovieVertSlices_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
            'RepDefaut', this.repImport, 'ChaineIncluse', '_WCCube', 'AllFilters', 1);
        if flag
            [flag, nomDirMovie, CLim, fps, quality, profileVideo, this.repExport] ...
                = WCDUtils.InputParams.matrixMovie(this.repExport);
            if flag
                 WCDUtils.Matrix.movie('VerticalSlices', listeFic, nomDirMovie, CLim, ...
                     'fps', fps, 'quality', quality, 'profileVideo', profileVideo);
            end
        end
        
    case 'WCD_CheckMatrixMovieHoriSlices_CI0'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
            'RepDefaut', this.repImport, 'ChaineIncluse', '_WCCube', 'AllFilters', 1);
        if flag
            [flag, nomDirMovie, CLim, fps, quality, profileVideo, this.repExport] ...
                = WCDUtils.InputParams.matrixMovie(this.repExport);
            if flag
                 WCDUtils.Matrix.movie('HorizontalSlices', listeFic, nomDirMovie, CLim, ...
                     'fps', fps, 'quality', quality, 'profileVideo', profileVideo);
            end
        end
 
    case 'WCD_CheckSampleBeam_CI0'
        [flag, listeFic, LayerType, backgroundImage, CLim, muteUnderSeabed, this.repImport] = WCDUtils.InputParams.getSampleBeamCheck(...
            this.Images(this.indImage), this.repImport);
        if flag
            WCDUtils.SampleBeam.check(listeFic, CLim, 'backgroundImage', backgroundImage, ...
                'muteUnderSeabed', muteUnderSeabed, 'LayerType', LayerType);
        end
      
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
