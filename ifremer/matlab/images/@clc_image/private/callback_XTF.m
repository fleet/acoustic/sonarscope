function this = callback_XTF(this, msg, ~)

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Quelques initialisations

X0 = cl_xtf([]);

%% Lancement des traitements

a = cl_image.empty; % Modif GLA car sinon bug avec 'MsgMosaiqueAssemblage'

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'XTF_PlotNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            XTF_PlotNavigation(X0, listFileNames);
        end
        
    case 'XTF_PlotNavigationCoverage_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            XTF_PlotNavigationCoverage(X0, listFileNames);
        end
 
    case 'XTF_PlotSignalOnNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomSignal] = params_XTF_PlotNavigationAndSignal(listFileNames);
            if flag
                XTF_PlotNavigationSignals(X0, listFileNames, nomSignal);
            end
        end
        
    case 'XTF_ImportNavigation_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, nomFicNav, this.repImport] = SSc.Params.ImportNavigation('.nvi', this.repImport);
            if flag
                XTF_importNavigation(X0, listFileNames, nomFicNav);
            end
        end
       
    case 'XTF_PlotAppendices_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            Option = {'Index'; 'Sidescan signals'};
            [flag, rep] = question_SelectASignal(Option, 'InitialValue', 2);
            if flag
                preprocessXtfFiles(listFileNames, 'Option', Option(rep))
            end
        end

    case 'XTF_ReadOnce_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            XTF_ReadOnce(X0, listFileNames);
        end
    case 'XTF_ComputeBathy_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            [flag, skipAlreadyProcessedFiles] = params_XTF_process_Bathy(listFileNames);
            if flag
                flag = process_BathyXTF(X0, listFileNames, skipAlreadyProcessedFiles);
            end
        end
        
    case 'XTF_PingSamples2LatLong_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, this, listFileNames] = getListFiles(this);
            if flag
                [flag, resol, Backup, AcrossInterpolation, LimitAngles, window, ...
                    nomFicErMapperLayer, nomFicErMapperAngle, TitreDepth, CompleteExistingMosaic, bilan] = params_XTFPingSamples2LatLong(listFileNames);
                if flag
                    [flag, a, this.repExport] = Xtf_PingSamples2LatLong(listFileNames, resol, Backup, AcrossInterpolation, LimitAngles, window, ...
                        nomFicErMapperLayer, nomFicErMapperAngle, TitreDepth, CompleteExistingMosaic, bilan, ...
                        'XLim', XLim, 'YLim', YLim);
                end
            end
        end
        
    case 'XTF_PingBeam2LatLong_CI1'
        [flag, XLim, YLim] = params_PingBeam2LatLong(this);
        if flag
            [flag, a, this.repImport, this.repExport] = XTF_PingBeam2LatLong(...
                'NomDirImport', this.repImport, ...
                'NomDirExport', this.repExport, ...
                'XLim', XLim, 'YLim', YLim);
        end
        
    case 'XTF_PingRange2PingAcrossDist_CI0'
        flag = XTF_PingSamples2PingAcrossDist('NomDir', this.repImport);

        
    case 'XTF_CleanSignals_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            preprocess_XTFSignals(X0, listFileNames);
            % TODO : Renommer preprocess_XTFSignals en SEGY_CleanSignals
        end
               
    case 'XTF_ZipDir_CI0'
        [flag, listFileNames, this.repImport] = uiSelectFiles('ExtensionFiles', '.xtf', 'AllFilters', 1, 'RepDefaut', this.repImport);
        if flag
            XTF_ZipDirectories(X0, listFileNames);
        end
        
    case 'XTF_DeleteCache_CI0'
        [flag, this, listFileNames] = getListFiles(this);
        if flag
            eraseSonarScopeCache(listFileNames);
        end        
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end




%% getListAllFiles
function [flag, this, listFileNames] = getListFiles(this, varargin)

[varargin, FileExt]            = getPropertyValue(varargin, 'FileExt',            '.xtf');
[varargin, SubTitle2]          = getPropertyValue(varargin, 'SubTitle2',          []);
[varargin, MessageParallelTbx] = getPropertyValue(varargin, 'MessageParallelTbx', 0); %#ok<ASGLU> 

[flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', '.xtf', 'AllFilters', 1, 'RepDefaut', this.repImport, ...
    'MessageParallelTbx', MessageParallelTbx, 'SubTitle', ['XTF file format : ' FileExt ' file selection'], 'SubTitle2', SubTitle2);
if flag
    this.repImport = repImport;
end
