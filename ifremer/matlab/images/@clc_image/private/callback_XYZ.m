function this = callback_XYZ(this, msg, subx, suby) %#ok<INUSD>

if isempty(msg)
    MessageTraitementDroite
    return
end

%% Lancement des traitements

a = cl_image.empty;

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'XYZ_ImportExport_CI0'
        [flag, this.repImport, this.repExort] = process_xyz(this.repImport, this.repExort);

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if ~isnan(flag) && flag && ~isempty(a)
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
    this = callback_zoomDirect(this, 'SamePosition');
end
