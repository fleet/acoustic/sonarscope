% Traitement des deplacements du curseur
%
% Syntax
%   this = callback_click(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_click(this)

flag = test_curseur_sur_axes(this);
if ~any(flag)
    return
end

SelectionType = get(this.hfig, 'SelectionType');
switch SelectionType
    case 'open' % Double click
        this = traiter_click_edit(this);
    case 'extend' % Click molette
        this = traiter_click_edit(this);
    case 'alt' % Click droit normal
    	this = traiter_click_edit(this);
    case 'normal' % Click normal gauche
        this = traiter_click_new(this);
    otherwise
        SSc_InvalidMessage(SelectionType, which(mfilename))
end
