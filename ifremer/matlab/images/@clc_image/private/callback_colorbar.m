% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_colorbar(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_colorbar(this, varargin)

[varargin, MajColorbar] = getPropertyValue(varargin, 'MajColorbar', 1); %#ok<ASGLU>

if ~isfield(this.ihm, 'Colorbar')
    return
end

if ~MajColorbar && isempty(this.Images(this.indImage).OriginColorbar.Tick)
    this = setOriginalColorbar(this);
end

ImageType = this.Images(this.indImage).ImageType;
if (ImageType == 2) && ~isempty(this.Images(this.indImage).OriginColorbar.Tick)
    h = findobj(this.hfig, 'Type', 'axes');
    GCO = setdiff(h, [this.hAxePrincipal this.hAxeProfilX this.hAxeProfilY]);
    AxeCourant = get(this.hfig, 'CurrentAxes');
    set(this.hfig, 'CurrentAxes', GCO);
    RGB = this.Images(this.indImage).OriginColorbar.Image;
    %     RGB = ind2rgb(0:(size(J,1)-1),J);
    hold off
    imagesc([-1 2], this.Images(this.indImage).OriginColorbar.YLim, RGB); axis xy

    set(GCO, 'YTickMode', 'manual')
    set(GCO, 'YTickLabelMode', 'manual')
    set(GCO, 'YTick',      this.Images(this.indImage).OriginColorbar.Tick)
    set(GCO, 'YTickLabel', this.Images(this.indImage).OriginColorbar.TickLabel)
    set(GCO, 'YAxisLocation', 'right')
    set(GCO, 'XTick',      [])
    set(GCO, 'XTickLabel', [])
    set(this.hfig, 'CurrentAxes', AxeCourant);
    return
end

%% Application de la table de couleur

ColormapIndex = this.Images(this.indImage).ColormapIndex;
if ColormapIndex == 1
    map = this.Images(this.indImage).ColormapCustom;
    map(map < 0) = 0;
    map(map > 1) = 1;
    this = set_colormap(this, map);
end

%% R�cup�ration de l'�tat du bouton

this.ihm.Colorbar.cpn = gerer_callback(this.ihm.Colorbar.cpn, this.ihm.Colorbar.msg);
this.visuColorbar     = get_etat(this.ihm.Colorbar.cpn);

%% Mise � jour de la visualisation

if gcf ~= this.hfig
    figure(this.hfig)
end
try
    if ImageType == 3
        hColorBar = colorbar_imageIndexee('vert');
    else
        % TODO : cette fonction prend �norm�ment de temps
        hColorBar = colorbar('vert');
        %         hColorBar = colorbar(this.hAxePrincipal, 'vert'); % Pas plus rapide
    end
    this.ihm.colorbar.cpn = copy(this.ihm.colorbar.cpn, hColorBar, 'ImageIndexee', ImageType == 3);
    if ishandle(hColorBar)
        delete(hColorBar);
    end
catch %#ok<CTCH>
    disp('Ici il y a un bug qui concerne la coloebar que SonarScope �vite de mani�re un peu artisanale')
end

this = positionner(this);


% {
% Test d'interactivit� sur la table de couleur. L'id�e est de faire comme
% si on faisait un zoom ou unzoom, c.a.d, quand on clique sur une couleur on
% centre cette valeur et on divise le pas par 2 et inversement pour un
% click droite

h = findobj(this.hfig, 'Type', 'axes');
GCO = setdiff(h, [this.hAxePrincipal this.hAxeProfilX this.hAxeProfilY]);
pppp = get(GCO(1), 'Children');
set_HitTest_Off(pppp)

set(GCO, 'ButtonDownFcn', 'date')
% Ici, il faut passer une callback �quivalente � ce qu'on fait dans les
% autres callbacks

% Remarque : je ne sais pas o� il faut mettre cette d�finition de callback
% mais je me demande si il ne faut pas � recr�er � chaque fois que la
% colorbar et retrac�e

% msgCallBack = built_cb(this, this.msgClickOnColorbar);
msgCallBack = built_cb(this.composant, this.msgClickOnColorbar);
set(GCO, 'ButtonDownFcn', msgCallBack);

% Attention : cette callback n'est g�n�r�e que si on passe une fois ici
% }
