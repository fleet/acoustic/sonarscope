% Fonctionnalite callback_contrast specifique au composant
%
% Syntax
%   a = callback_contrast(a)
% 
% Input Arguments
%   a : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_contrast(this, varargin)

[varargin, flagModeAuto] = getPropertyValue(varargin, 'ModeAuto', 0);

if nargin == 1
    MessageTraitementDroite
    return
end

%% Control

ImageType = this.Images(this.indImage).ImageType;
Titre = this.Images(this.indImage).Name;
strImageType = get(this.Images(this.indImage), 'strImageType');
if ImageType ~= 1
    str1 = sprintf('L''image "%s" est de type "%s". Seules les images de type "Intensity" peuvent �tre rehauss�es. Vous pouvez la convertir en intensit� : "Traitements g�n�raux", "Couleurs", "index�e -> intensit�".', ...
        Titre, strImageType{ImageType});
    str2 = sprintf('"%s" is of type "%s". Only images of type "Intensity" can be enhanced. You can convert this image using : "General processings", "Colors", "indexed -> intensity".', ...
        Titre, strImageType{ImageType});
    if getFlagContrasteAuto == 1
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ContrasteAutoPasPossible');
    else % 0
        my_warndlg(Lang(str1,str2), 1);
    end
    return
end

%% Lancement des traitements

msg = varargin{1};

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'ContrasteTitre'
        nomFicHelp = getNomFicDatabase('index.htm');
        my_web(['file://' nomFicHelp])
        return

    case 'ContrasteImageSaisieCLim' % Saisie de CLim
        def = num2str(this.Images(this.indImage).CLim);
        answer = inputdlg({'CLim'}, Lang('Saisie des param�tres', 'Parameters input'), 1, {def});
        if isempty(answer)
            return
        end
        CLim = str2num(answer{1}); %#ok
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageEntiereMinMax'  % Min,Max image entiere
        StatValues = this.Images(this.indImage).StatValues;
        CLim = [StatValues.Min StatValues.Max];
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageEntiereQuant_1pc' % Quant 1% image entiere
        StatValues = this.Images(this.indImage).StatValues;
        CLim = StatValues.Quant_01_99;
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageEntiereQuant_0p5pc' % Quant 0.5% image entiere
        StatValues = this.Images(this.indImage).StatValues;
        CLim = StatValues.Quant_25_75;
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageEntiereQuant_3pc' % Quant 3% image entiere
        StatValues = this.Images(this.indImage).StatValues;
        CLim = StatValues.Quant_03_97;
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageZoomeeMinMax' % Min,Max curent extent
        [~, ~, ~, ~, I] = get_Image_zoomee(this, 'UnderSampling',  1);
        flagFFT = (this.Images(this.indImage).SpectralStatus == 2);
        StatValues = stats(I, 'flagFFT', flagFFT);
        CLim = [StatValues.Min StatValues.Max];
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageZoomeeQuant_1pc' % Quant 1% curent extent
        [~, ~, ~, ~, I] = get_Image_zoomee(this, 'UnderSampling',  1);
        flagFFT = (this.Images(this.indImage).SpectralStatus == 2);
        StatValues = stats(I, 'flagFFT', flagFFT);
        CLim = StatValues.Quant_01_99;
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageZoomeeQuant_0p5pc' % Quant 0.5% curent extent
        [subx, suby, x, y, I] = get_Image_zoomee(this, 'UnderSampling',  1); %#ok<ASGLU>
        flagFFT = (this.Images(this.indImage).SpectralStatus == 2);
        StatValues = stats(I, 'flagFFT', flagFFT);
        CLim = StatValues.Quant_25_75;
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    case 'ContrasteImageZoomeeQuant_3pc' % Quant 3% curent extent
        [subx, suby, x, y, I] = get_Image_zoomee(this, 'UnderSampling',  1); %#ok<ASGLU>
        flagFFT = (this.Images(this.indImage).SpectralStatus == 2);
        StatValues = stats(I, 'flagFFT', flagFFT);
        CLim = StatValues.Quant_03_97;
        CLim = SymetrieCLim(this.Images(this.indImage), CLim);
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end
        
    case 'ContrasteImageZoomeeMinMaxAuto'
        this = callback_contrast(this, 'ContrasteImageZoomeeMinMax');
        this = changeStatusContrasteAuto(this, 1);
        return
        
    case 'ContrasteImageZoomee0p5PCAuto'
        this = callback_contrast(this, 'ContrasteImageZoomeeQuant_0p5pc');
        this = changeStatusContrasteAuto(this, 2);
        return
        
    case 'ClickOnColorbar'
        Coordonnees = get(gca, 'currentPoint');
        SelectionType = get(this.hfig, 'SelectionType');
        CLim = this.Images(this.indImage).CLim;
        Y = Coordonnees(1,2);
        switch SelectionType
            case 'normal' % On r�duit la table de couleur
                R = diff(CLim) / 1.5;
            case 'alt' % On dilate la table de couleur
                R = diff(CLim) * 1.5;
            case 'extend' % On centre la table de couleur : Shift - click left mouse button or click both left and right mouse buttons
                R = diff(CLim);
            case 'open' % Double-click any mouse button
                R = diff(CLim); % Pour l'instant
                % TODO : ouvrir un graphique de l'histogramme et activer 2
                % barres verticales plac�es sur CLIM, permettre de bouger
                % ces 2 barres et bouton OK pour rendre la main : ferme la
                % figure et met � jour le CLim de SonarScope. Essayer de
                % rendre les couleurs modifiables en temps r�el sue
                % SonarScope comme cela est fait lorque l'on �dite la
                % colorbar
            otherwise % Ceinture et bretelles
                str1 = sprintf('SelectionType="%s" dans "callback_contrast" non trait�.\nEnvoyez ce message � sonarscope@ifremer.fr SVP.', SelectionType);
                str2 = sprintf('SelectionType="%s" in "callback_contrast" not processed.\nPlease send a copy of this message to sonarscope@ifremer.fr SVP.', SelectionType);
                my_warndlg(Lang(str1,str2), 1);
                R = diff(CLim);
        end
        CLim = [Y-R/2 Y+R/2];
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end
        
    case 'imcontrast'        
        GCA = gca;
        positionCurseurDansFigure = get(GCA, 'currentPoint');
        XLim = get(GCA, 'XLim');
        YLim = get(GCA, 'YLim');
        if (positionCurseurDansFigure(1,1) < XLim(1)) || (positionCurseurDansFigure(1,1) > XLim(2)) || ...
                (positionCurseurDansFigure(1,2) < YLim(1)) || (positionCurseurDansFigure(1,2) > YLim(2))
            return
        end
        
        SelectionType = get(this.hfig, 'SelectionType');
        switch SelectionType
            case 'normal' % Clic gauche. On se sert de imcontrast pour r�gler CLim
                TypeAction = 1;
            case 'alt' % Clic droite, On se sert de imcontrast pour nettoyer la donn�e
                TypeAction = 2;
            case 'extend' % Clic molette. On se sert de imcontrast pour r�gler CLim
                TypeAction = 1;
            case 'open' % Double-click any mouse button. On se sert de imcontrast pour r�gler CLim
                TypeAction = 1;
            otherwise % Ceinture et bretelles
                str1 = sprintf('SelectionType="%s" dans "callback_contrast" non trait�.\nEnvoyez ce message � sonarscope@ifremer.fr SVP.', SelectionType);
                str2 = sprintf('SelectionType="%s" in "callback_contrast" not processed.\nPlease send a copy of this message to sonarscope@ifremer.fr SVP.', SelectionType);
                my_warndlg(Lang(str1,str2), 1);
        end
        
        if TypeAction == 1
            hImcontrast = my_imcontrast(this.hAxePrincipal); % TODO : le Adjust ne fonctionne pas
            waitfor(hImcontrast)
            pause(0.5)
            CLim = get(this.hAxePrincipal, 'CLim');
            setappdata(this.hfig, 'ExitForbidenForTheMoment', []);
        else
%             str1 = sprintf('Nettoyage de l''image bas�e sur l''outil de rehaussement de contraste.\nFonctionnalit� en cours de test, dites moi si c''est opportun  de proposer cela ici.');
%             str2 = sprintf('Data cleaning using image contrast.\nThis processing is for evaluation, I am not sure that it is a good idea.');
%             my_warndlg(Lang(str1,str2), 1); %, 'Tag', 'CleanCurrentImageUsingImcontrast', 'TimeDelay', 10);
            
            % Recherche des pixels � traiter
            
            setappdata(this.hfig, 'ExitForbidenForTheMoment', 1);
            [flag, subx, suby] = getSubxSubyContextuel(this, varargin{end});
            if ~flag
                return
            end
            
            try
                % Warning : keep vSSC variable to provoque an error if the function imcontrast has not been adapted
                [hImcontrast, vSSC] = my_imcontrast(this.hAxePrincipal);
            catch ME
                fprintf('%s\n', getReport(ME));
                str1 = 'Il est pr�f�rable d''utiliser la version corrig�e de imcontrast. Pour cela, voir sous https://forge.ifremer.fr/frs/?group_id=126';
                str2 = 'It is more useful to use imcontrast version corrected for adjust data. See https://forge.ifremer.fr/frs/?group_id=126';
                str1 = sprintf('%s\n\n%s', ME.message, str1);
                str2 = sprintf('%s\n\n%s', ME.message, str2);
                my_warndlg(Lang(str1,str2),1);
                hImcontrast = my_imcontrast(this.hAxePrincipal); % Appel sans vSSC
            end
            waitfor(hImcontrast)
            CLim = get(this.hAxePrincipal, 'CLim');

            Mask = ROI_auto(this.Images(this.indImage), 1, 'Mask', [], [], CLim, 1, 'subx', subx, 'suby', suby);
            Im = extraction(this.Images(this.indImage), 'subx', subx, 'suby', suby);
            b = masquage(Im, 'LayerMask', Mask, 'valMask', 1, 'zone', 1);
            
            str1 = 'Cr�ation d''une nouvelle image (sinon modification de l''image courante).';
            str2 = 'Create a new image (otherwise modify the current image).';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if flag
                if rep == 1
                    this = add_image(this, b);
                    this = editer_image_selectionnee(this);
                    this = callback_zoomDirect(this, 'SamePosition');
                else
                    this.Images(this.indImage) = insertion(b, this.Images(this.indImage)); %, 'subx', subx, 'suby', suby);
                end
            end
            setappdata(this.hfig, 'ExitForbidenForTheMoment', []);
        end
        if ~flagModeAuto
            this = changeStatusContrasteAuto(this, 0);
        end

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        return
end

if length(CLim) ~= 2
    str1 = sprintf('Valeurs non interpretables : %s, recommencez l''op�ration.', answer{1});
    str2 = sprintf('Bad values : %s,    please retry.', answer{1});
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Si l'image ne contient que des NaN, on ne modifie rien

if any(isnan(CLim))
    str1 = 'TODO';
    str2 = 'Statistics are not defined, I try to reprocess them, if it is successfull redo your contrast-enhancement again.';
    my_warndlg(Lang(str1,str2), 1);
    this.Images(this.indImage) = compute_stats(this.Images(this.indImage));
    str = get(this.Images(this.indImage), 'StatSummary');
    my_warndlg(str,0);
    return
end
CLim = sort(CLim);

%% Transmission des etats dans l'instance cl_image

this.Images(this.indImage).CLim = CLim;
map  = this.Images(this.indImage).Colormap;
this = set_colormap(this, map);

%% Traitement des synchronisations

this = process_synchroV(this);

%% Mise � jour de la visualisation

this = show_image(this);
this = show_profils(this);
