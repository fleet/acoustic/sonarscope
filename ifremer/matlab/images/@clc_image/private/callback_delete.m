% Fonctionnalite callback_delete specifique au composant
%
% Syntax
%   a = callback_delete(a)
%
% Input Arguments
%   a   : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_delete(this)

N = length(this.Images);
if N == 1
    str1 = 'Il ne reste qu''une image. C''est pas possible de la d�truire.';
    str2 = 'Only one image is remaining, you cannot delete it.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

str1 = 'Image courante';
str2 = 'Current image';
ListeChoix{1} = Lang(str1,str2);
str1 = 'Toutes les images';
str2 = 'All images';
ListeChoix{2} = Lang(str1,str2);
str1 = 'Toutes les images sauf l''image courante';
str2 = 'All images except the current one';
ListeChoix{3} = Lang(str1,str2);
str1 = 'Toutes les images synchronis�es � l''image courante';
str2 = 'All images synchronized to the current one';
ListeChoix{4} = Lang(str1,str2);
str1 = 'Certaines images';
str2 = 'Some images';
ListeChoix{5} = Lang(str1,str2);
str1 = 'Suppression d''images (Suppr)';
str2 = 'Suppress images (Suppr)';
[reponse, flag] = my_listdlg(Lang(str1,str2), ListeChoix, 'SelectionMode', 'Single');
if ~flag
    return
end
nbImages = length(this.Images);
switch reponse 
    case 1 % Image courante
        liste = this.indImage;
    case 2 % Toutes les images
        liste = 1:nbImages;
    case 3 % Toutes les images sauf l''image courante
        liste = [1:(this.indImage-1) (this.indImage+1):nbImages];
    case 4 % Toutes les images synchronis�es en X et Y � l''image courante
        [flag, liste] = listeLayersSynchronises(this.Images, this.indImage, 'IncludeCurrentImage');
        if ~flag
            return
        end
    case 5 % Certaines images
        [flag, indImage, strOut] = summary(this.Images, 'ExportResults', 0, ...
            'SelectLayer', this.indImage, 'QueryMode', 'Delete');
        if ~flag
            % Bouton Cancel
            return
        end
        if isempty(indImage) || isempty(strOut)
            % Filtre total ou bouton Cancel
            return
        end
        for k=1:length(this.Images)
            strIn{k,1} = this.Images(k).Name; %#ok<AGROW>
        end
        % Remise en ordre par identification du nom du layer.
        strOut      = strOut(indImage,1);
        % R�cup�ration des indices de tri.
        [C,ia,ib] = intersect(strIn,strOut,'stable');  %#ok<ASGLU>
        liste = ia;
end

if length(liste) == nbImages
    liste = liste(2:end);
    str1 = 'La premi�re image est conserv�e car SonarScope doit toujours avoir une image.';
    str2 = 'First image remaining because SonarScope cannot be empty.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FirstImageRemaining', 'TimeDelay', 30);
end

%% Suppression des infos internes concernant cette image

this.Images(liste)                = [];
this.cross(liste, :)              = [];
this.identVisu(liste)             = [];
this.visuLim(liste, :)            = [];
this.visuLimHistory(liste)        = [];
this.visuLimHistoryPointer(liste) = [];
try
    this.valBeforeSetNaN(liste) = [];
catch %#ok<CTCH>
    disp('Bug � corriger ici dans callback_delete')
end

%% Nouvel indice de l'image courante

N = length(this.Images);
this.indImage = max(this.indImage, 1);
this.indImage = min(this.indImage, N);

%% Edition de l'image

this = editer_image_selectionnee(this);
