% Traitement de la demande de tracé de la hauteur dans l'image
% 
% Syntax
%   this = callback_drawHeight(this, mouvement)
%
% Input Arguments
%   this      : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_drawHeight(this)

%% Détermination si le curseur est sur l'un des 3 axes

flag = test_curseur_sur_axes(this);
if ~flag(1)
    return
end

this = traiter_click_Height(this);
