% Fonctionnalite callback_export specifique au composant
%
% Syntax
%   a = callback_export(a)
%
% Input Arguments
%   a : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_export(this, varargin)

%% Message identifiant la callback

if nargin == 1
    msg = 'EXPORT_ERS_Image_CI1';
else
    msg = varargin{1};
end

%% Recuperation de la partie de l'image visible sur l'ecran

[flag, subx, suby, ~, XLim, YLim, flagImageEntiere] = getSubxSubyContextuel(this, msg);
if ~flag
    return
end

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'EXPORT_Ram2VM_CI0'
        this.Images = optimiseMemory(this.Images, 'SizeMax', 0);
        
    case 'EXPORT_VM2Ram_CI0'
        this.Images(this.indImage) = Virtualmemory2Ram(this.Images(this.indImage));
        
    case 'EXPORT_CompressDataFiles_CI0'
        zip_exampleData
        
    case 'EXPORT_UncompressDataFiles_CI0'
        unzip_exampleData
        
    case 'EXPORT_CompressRepertoire_CI0'
        [flag, nomDir] = my_uigetdir(this.repExport, Lang('Repertoire � compresser', 'Directory to be compress'));
        if flag
            zip_exampleData('nomDir', nomDir)
        end
        
    case 'EXPORT_UncompressRepertoire_CI0'
        [flag, nomDir] = my_uigetdir(this.repExport, Lang('Repertoire � d�compresser', 'Directory to be uncompress'));
        if flag
            unzip_exampleData('nomDir', nomDir)
        end
        
    case 'EXPORT_CompressFiles_CI0'
        [flag, nomDir] = my_uigetdir(this.repExport, Lang('Repertoire contenant les fichiers a compresser', 'Directory where the files to be compressed are'));
        if flag
            zip_exampleData('nomDir', nomDir, 'FileSelection', 1)
        end
        
    case 'EXPORT_UncompressFiles_CI0'
        [flag, nomDir] = my_uigetdir(this.repExport, Lang('Repertoire contenant les fichiers a d�compresser', 'Directory where the files to be uncompressed are'));
        if flag
            unzip_exampleData('nomDir', nomDir, 'FileSelection', 1)
        end
        
    case 'EXPORT_GMT1_CI1'
        % V�rification de la validit� d'un trac� bathym�trique
        [flag, nomFic, indLayerBathymetry, this.repExport] = params_ExportGMT1(this.Images, this.indImage, XLim, XLim, this.repExport); % XLim � la place de this.visuLim(this.indImage,1:2) avant
        if ~flag || isempty(nomFic)
            return
        end
        
        [flag, kGMT, pGMT, vGMT] = identify_gmt_vers; %#ok<ASGLU>
        
        flag = export_gmt(this.Images(this.indImage), nomFic, 'GMTVEr', vGMT, ...
            'subx', subx, 'suby', suby);
        if ~flag
            return
        end

        if isempty(indLayerBathymetry)
            subx2 = [];
            suby2 = [];
        else
            [XLim, YLim, subx, suby, subx2, suby2] = intersectionImages(this.Images(this.indImage), subx, suby, this.Images(indLayerBathymetry)); %#ok<ASGLU>
            if isempty(subx) || isempty(suby)
                str = sprintf('Pas d''intersection commune entre les images');
                my_warndlg(str,1);
                return
            end
        end
        
        nomDir = fileparts(nomFic);
        this.repExport = nomDir;
  
        [flag, gmtDlg, this.repImport] = getOptionsGMT(this.Images(this.indImage), this.Images(indLayerBathymetry), this.repImport);
        if ~flag % Action Cancel
            return
        end
        
        if gmtDlg.isContourLines
            nomFicElevation = my_tempname('.grd');           
            flag = export_gmt(this.Images(indLayerBathymetry), nomFicElevation, 'GMTVEr', vGMT, ...
                'suby', suby2, 'subx', subx2);
            if ~flag
                return
            end
        else
            nomFicElevation = [];
        end
        
        if vGMT< 6
            plotGMT(this.Images(this.indImage), gmtDlg, nomFic, nomFicElevation, 'PathGMT', pGMT, ...
                'subx', subx, 'suby', suby);
        else
            plotGMT6(this.Images(this.indImage), gmtDlg, nomFic, nomFicElevation, 'PathGMT', pGMT, ...
                'subx', subx, 'suby', suby);
        end    
        
    case 'EXPORT_GMT2_CI1'
        [flag, nomFicPs, indLayers, rep3D, repMapOn3D, indLayerMapOn3D, this.repExport] ...
            = params_ExportGMT2(this.Images, this.indImage, XLim, thie.repExport); % XLim � la place de this.visuLim(this.indImage,1:2) avant
        if ~flag
            return
        end
          
        [flag, gmtDlg, this.repImport] = getOptionsGMT(this.Images(this.indImage), this.Images([]), this.repImport);
        if ~flag % Action Cancel
            return
        end
        
        [this.Images(this.indImage), Azimut, Elevation, OffsetInterLayer, Height3D, flag] = ...
            saisie_Azimuth_Elevation_ExaVert(this.Images(this.indImage), indLayers, rep3D);
        if ~flag
            return
        end
        
        [flag, nomDirOut] = plotGMTMultiLayer(this.Images, this.indImage, indLayers, nomFicPs, gmtDlg, ...
             OffsetInterLayer, rep3D, repMapOn3D, indLayerMapOn3D,  Azimut, Elevation, Height3D, ...
             'subx', subx, 'suby', suby);
         if ~flag
             return
         end
        this.repExport = nomDirOut;

    case 'EXPORT_Fledermaus_CI1'
        [flag, nomFic, indlayerDrapage, this.repExport] = params_Fledermaus(this.Images, this.indImage, this.repExport);
        if flag
            flag = export_fledermaus(this.Images(this.indImage), nomFic, ...
                'LayerMapping', this.Images(indlayerDrapage), ...
                'subx', subx, 'suby', suby);
            if flag
                open_IVew3D(nomFic) % Affichage dans iView3D
            end
        end
         
    case 'EXPORT_VRML_CI1' 
        GeometryType = this.Images(this.indImage).GeometryType;
        [flag, this.repExport] = export_VRML(this.Images, this.indImage, GeometryType, subx, suby, this.repExport);
        if ~flag
             return
        end
        
    case 'EXPORT_ColladaDae_CI1'
        % [flag, nomFic, this] = getFileNameLinked2ImageName(this, '.dae');
        % TODO : remplacer ce qui suit et activer getFileNameLinked2ImageName
        
        ImageName = deblank(strtok(this.Images(this.indImage).Name, '('));
        filtre = fullfile(this.repExport, [ImageName, '.dae']);
        [flag, nomFic] = my_uiputfile({'*.dae'}, Lang('Nom du fichier', 'Give a file name'), filtre);
        if ~flag
            return
        end
        [flag, vertExag, bloc] = paramCOLLADA;
        if ~flag
            return
        end
        if bloc
            [flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage);
            if ~flag
                return
            end
            if ~isempty(indLayers)
                [flag, indImage, strOut] = summary(this.Images(indLayers), 'ExportResults', 0, ...
                    'SelectLayer', [], 'QueryMode', 'MultiSelect');
                if ~flag
                    % Filtre total ou bouton Cancel
                    return
                end
                % Si OK et filtres remplis.
                if ~isempty(indImage) && ~isempty(strOut)
                    strIn = nomsLayers(:);
                    
                    % Remise en ordre par identification du nom du layer.
                    strOut      = strOut(indImage,1);
                    % R�cup�ration des indices de tri.
                    [C,ia,ib] = intersect(strIn,strOut,'stable');  %#ok<ASGLU>
                    indLayers = indLayers(ia);
                else
                    indLayers = [];
                end
            end
        else
            indLayers = [];
        end
        this.repExport = fileparts(nomFic);
        %  flag = exportCollada(this.Images(this.indImage), 'filename', nomFic, 'subx', subx, 'suby', suby);
        if isempty(indLayers)
            flag = exportCollada_Delaunay(this.Images(this.indImage), 'filename', nomFic, ...
                'vertExag', vertExag, 'bloc', bloc, ...
                'subx', subx, 'suby', suby);
        else
            flag = exportCollada_Delaunay(this.Images([this.indImage, indLayers(end)]), 'filename', nomFic, ...
                'vertExag', vertExag, 'bloc', bloc, ...
                'subx', subx, 'suby', suby);
        end
        if ~flag
            return
        end
        
    case 'EXPORT_ColladaObj_CI1'% [flag, nomFic, this] = getFileNameLinked2ImageName(this, '.dae');
        % TODO : remplacer ce qui suit et activer getFileNameLinked2ImageName
        
        ImageName = deblank(strtok(this.Images(this.indImage).Name, '('));
        filtre = fullfile(this.repExport, [ImageName, '.obj']);
        [flag, nomFic] = my_uiputfile({'*.obj'}, Lang('Nom du fichier', 'Give a file name'), filtre);
        if ~flag
            return
        end
        [flag, vertExag, bloc] = paramCOLLADA;
        if ~flag
            return
        end
        if bloc
            [flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage);
            if ~flag
                return
            end
            if ~isempty(indLayers)
                [flag, indImage, strOut] = summary(this.Images(indLayers), 'ExportResults', 0, ...
                    'SelectLayer', [], 'QueryMode', 'MultiSelect');
                if ~flag
                    % Filtre total ou bouton Cancel
                    return
                end
                % Si OK et filtres remplis.
                if ~isempty(indImage) && ~isempty(strOut)
                    strIn = nomsLayers(:);
                    
                    % Remise en ordre par identification du nom du layer.
                    strOut      = strOut(indImage,1);
                    % R�cup�ration des indices de tri.
                    [C,ia,ib] = intersect(strIn,strOut,'stable');  %#ok<ASGLU>
                    indLayers = indLayers(ia);
                else
                    indLayers = [];
                end
            end
        else
            indLayers = [];
        end
        this.repExport = fileparts(nomFic);
        %             flag = exportCollada(this.Images(this.indImage), 'filename', nomFic, 'subx', subx, 'suby', suby);
        if isempty(indLayers)
            flag = exportOBJ_Delaunay(this.Images(this.indImage), ...
                'filename', nomFic, 'vertExag', vertExag, 'bloc', bloc, ...
                'subx', subx, 'suby', suby);
        else
            flag = exportOBJ_Delaunay(this.Images([this.indImage, indLayers(end)]), ...
                'filename', nomFic, 'vertExag', vertExag, 'bloc', bloc, ...
                'subx', subx, 'suby', suby);
        end
        if ~flag
            return
        end
        
    case {'EXPORT_GXF_3_uncompressed_CI1', 'EXPORT_GXF_3_compressed_CI1', 'EXPORT_GXF_3_repeatcompressed_CI1', 'EXPORT_GXF_3_kingdom_CI1'}
        % [flag, nomFic, this] = getFileNameLinked2ImageName(this, '.gxf', 'FullName');
        % TODO : remplacer ce qui suit et activer getFileNameLinked2ImageName
        
        token = strsplit('EXPORT_GXF_3_kingdom_CI1', '_');
        filtre = fullfile(this.repExport, 'myGrid.gxf');
        [flag, nomFic] = my_uiputfile({'*.gxf'}, Lang('Nom du fichier', 'Give a file name'), filtre);
        if flag
            this.repExport = fileparts(nomFic);
            flag = exportGXF(this.Images(this.indImage), ...
                'nomFic', nomFic, 'fileType',  token{4}, ...
                'subx', subx, 'suby', suby); %#ok<NASGU>
        end
        
    case 'EXPORT_ArcGisAscii_CI1'          
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.asc', this.repExport, 'FullName');
        if flag
            flag = exportArcAsciiGrid(this.Images(this.indImage), nomFic, 'fileType', 'ascii', ...
                'subx', subx, 'suby', suby); %#ok<NASGU>
        end
        
    case 'EXPORT_ArcGISBin_CI1'        
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.hdr', this.repExport, 'FullName');
        if flag
            flag = exportArcAsciiGrid(this.Images(this.indImage), nomFic, 'fileType', 'bin', ...
                'subx', subx, 'suby', suby); %#ok<NASGU>
        end
        
    case 'EXPORT_ScreenDump_CI0'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), {'.tif'; '.jpg'; '.gif'}, this.repExport, 'FullName');
        if flag
            fig2fic(gcf, nomFic)
            visuExterne(nomFic); % Visualisation par un visualisateur externe
        end

    case 'EXPORT_MatlabArray_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.mat', this.repExport, 'FullName');
        if flag
            export_matlab_array(this.Images(this.indImage), nomFic, ...
                'subx', subx, 'suby', suby);
        end

    case 'EXPORT_MatlabObjet_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.mat', this.repExport, 'FullName');
        if flag
            export_matlab_objet(this.Images(this.indImage), nomFic, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'EXPORT_Raster_CI1'
        if nargin == 2
            Ext = '.tif';
        else
            Ext = varargin{2};
        end
        [~, indLayerMask, valMask, zone] = paramsMasquage(this.Images, this.indImage);    
        [flag, nomFic, nbLigMax, nbColMax, Quality, this.repExport] = params_ExportTif(this.Images(this.indImage), ...
            Ext, suby, subx, this.repExport);
        if flag
            [nomDir, ~, Ext] = fileparts(nomFic);
            exportRaster(this.Images(this.indImage), 'nomFic', nomFic, 'Format', Ext(2:end), ...
                'nbLigMax', nbLigMax, 'nbColMax', nbColMax, 'Quality', Quality,...
                'LayerMask', this.Images(indLayerMask), 'valMask', valMask, 'zone', zone, ...
                'subx', subx, 'suby', suby)
            this.repExport = nomDir;
        end

    case {'EXPORT_ImageGeotiff32Bits_CI1'; 'EXPORT_ImageGeotiff32BitsBis_CI1'}
       [flag, nomFic, this.repExport] = paramsExportGeotif(this.Images, this.indImage, this.repExport);
        if flag
            export_geoTiff32Bits(this.Images(this.indImage), nomFic, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'EXPORT_BAG_CI1'
        % Param�tres d'export identiques � GeoTif + appel du module
        % bag_converter.exe
        [flag, nomFic] = paramsExportGeotif(this.Images, this.indImage, this.repExport, 'ExportFormat', 'BAG');
        if flag
            export_bag(this.Images(this.indImage), nomFic, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'EXPORT_XML_Image_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.xml', this.repExport);
        if flag
            export_xml(this.Images(this.indImage), nomFic, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'EXPORT_XML_ImagesSynchro_CI1'
        % XLim = this.visuLim(this.indImage, 1:2); % Comment� le 15/03/3021
        % YLim = this.visuLim(this.indImage, 3:4);
        this.repExport = export_MultiLayersXML(this.Images, this.indImage, this.repExport, ...
            'XLim', XLim, 'YLim', YLim, ...
            'subx', subx, 'suby', suby);
        
    case 'EXPORT_TifImagesSynchro_CI1'
        % XLim = this.visuLim(this.indImage, 1:2); % Comment� le 15/03/3021
        % YLim = this.visuLim(this.indImage, 3:4);
        this.repExport = export_MultiLayersTif(this.Images, this.indImage, this.repExport, ...
            'XLim', XLim, 'YLim', YLim);
        
    case 'EXPORT_ImageGifAnimated_CI1'
        this.repExport = export_MultiLayersGifAnimated(this.Images, this.indImage, this.repExport);

    case 'EXPORT_MatlabImageSignalsY_CI1'
        currentPoint = this.cross(this.indImage, :);
        [flag, liste, index, indLayers] = params_ExportImageSignals(this.Images, this.indImage);
        if flag
            export_ImageSignals(this.Images, this.indImage, indLayers, liste, index, 'currentPoint', currentPoint, ...
                'subx', subx, 'suby', suby)
        end
        
    case 'EXPORT_MatlabImageSignalsT_CI1'
        export_ImageSignalsAccordingTime(this.Images(this.indImage), this.cross(this.indImage, :), ...
            'subx', subx, 'suby', suby)

    case 'EXPORT_MatlabImageSignalsYSignalDialog_CI1'
        currentPoint = this.cross(this.indImage, :);
        [flag, listLayers, listSignals, index] = params_ExportImageSignalsInSignalDialog(this.Images, this.indImage);
        if flag
            export_ImageSignalsDialog(this.Images, this.indImage, listLayers, listSignals, index, 'currentPoint', currentPoint, ...
                'subx', subx, 'suby', suby)
        end
 
    case 'EXPORT_MatlabFigure3D_CI1'
        [flag, Fig] = params_exportImage3D;
        if flag
            export_Image3D(this.Images(this.indImage), 'Fig', Fig, ...
                'suby', suby)
        end
       
    case 'EXPORT_MatlabFigure_CI1'
        [flag, Rot90, NomY, TypeWindow] = paramsImagesc(this.Images(this.indImage));
        if flag
            [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby, 'IncludeCurrentImage', 1);
            if flag
                imagesc(this.Images(indLayers), 'Rot90', (Rot90 == 1), 'NomY', NomY, 'TypeWindow', TypeWindow, ...
                    'subx', subx, 'suby', suby);
            end
        end

    case 'EXPORT_GoogleEarth_CI1'
        [flag, this, nomFic, nbLigMax, nbColMax, valMask0, flagFillNaN] = paramsExportGoogleEarth(this);
        if flag
            % profile on
            flag = export_GoogleEarth(this.Images(this.indImage), 'nomFic', nomFic, ...
                'nbLigMax', nbLigMax, 'nbColMax', nbColMax, 'valMask0', valMask0, 'flagFillNaN', flagFillNaN, ...
                'subx', subx, 'suby', suby);
            % profile report
            if flag
                winopen(nomFic)
            end
        end
        
    case 'EXPORT_GLOBEMultiData_CI1'
        [flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage, 'IncludeCurrentImage'); %#ok<ASGLU>
        if ~flag
            return
        end
        if isempty(indLayers)
            return
        end
        [flag, indIndexType, indSelectLayer, nomFicXML, MultiTime, this.repExport] ...
            = params_Export3DViewerV3(this.Images, this.indImage, indLayers, this.repExport);
        if flag
            GeometryType = this.Images(indLayers(1)).GeometryType;
            switch GeometryType
                case cl_image.indGeometryType('LatLong')
                    % Elimination des layers non-retenus.
                    indLayers(indSelectLayer == 0) = [];
                    indIndexType(indSelectLayer == 0) = [];
                    [flag, b] = creer_clMultidata(this.Images, nomFicXML, indIndexType, indLayers, MultiTime);
                    if ~flag
                        return
                    end
                    
                    % R�organisation par type et nom des layers.
                    b = reorganization(b, MultiTime);
                    
                    % Export des layers de donn�es et d'index.
                    flag = exportGeotiffSonarScope3DViewerMultiV3(b, nomFicXML, ...
                        'sizeTiff', 2500); %#ok<NASGU>

                otherwise
                    str1 = 'Faut passer par l''autre version pour cette g�om�trie';
                    str2 = 'Use the previous export for this geometry';
                    my_warndlg(Lang(str1,str2), 1);
            end
        end

    case 'EXPORT_GLOBE_CI1'
        GeometryType = this.Images(this.indImage).GeometryType;
        switch GeometryType
            case cl_image.indGeometryType('LatLong')
%                 sizeTileNWW = 150; % Ancien tuilage manuel (les fonctions se trouvent dans IfremerToolboxFonctionsObsoletes)
%                 [flag, this.repExport] = exportTilingNasaWorldWindBil(this.Images(this.indImage), 'subx', subx, 'suby', suby, ...
%                     'repExport', this.repExport, 'SizeTileNWW', sizeTileNWW); %#ok<ASGLU>

                [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
                if flag
                    sizeTiff = 2500;
                    [flag, this.repExport] = exportGeotiffSonarScope3DViewerMulti(this.Images(indLayers), ...
                        'sizeTiff', sizeTiff, 'repExport', this.repExport, ...
                        'subx', subx, 'suby', suby); %#ok<ASGLU>
                end

            case cl_image.indGeometryType('PingBeam')
                [flag, nomFic, Name, this.repExport] = params_exportSoundingsToSSc3DV(this.Images(this.indImage), this.repExport);
                if flag
                    flag = sonar_exportSoundingsToSSc3DV(this.Images, this.indImage, nomFic, Name, ...
                        'subx', subx, 'suby', suby); %#ok<NASGU>
                end
                
            case cl_image.indGeometryType('DepthAcrossDist')
                [flag, indLayers, subx, suby] = gestionMultiImages(this.Images, this.indImage, flagImageEntiere, subx, suby);
                if flag
                    [flag, nomFicXml, this.repExport] = params_export_polarEchogram2SSc3DV(this.Images(indLayers), this.repExport);
                    if flag
                        flag = export_polarEchogram2SSc3DV(this.Images(indLayers), nomFicXml, this.repExport, ...
                            'subx', subx, 'suby', suby); %#ok<NASGU>
                    end
                end
                
            case cl_image.indGeometryType('SampleBeam')
                [flag, nomFicXml, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '_SampleBeam.xml', this.repExport);
                if flag
                    flag = export_RawEchogram2SSc3DV(this.Images(this.indImage), nomFicXml, ...
                        'subx', subx, 'suby', suby); %#ok<NASGU>
                end
                
            case {cl_image.indGeometryType('PingSamples') cl_image.indGeometryType('PingDepth')}
                [flag, NomFicXML, TailleMax, typeOutput, this.repExport] = paramsExport3DV_ImagesAlongNavigation(this.Images, this.indImage, this.repExport);
                if flag
                    flag = sonar_Export3DV_ImagesAlongNavigation_ByBlocks(this.Images(this.indImage), NomFicXML, TailleMax, typeOutput, ...
                        'subx', subx, 'suby', suby); %#ok<NASGU>
                end
                
            otherwise
                str1 = 'Seules les g�om�tries "LatLong", "PingBeam" et "DepthAcrossDist" peuvent �tre export�es dans GLOBE.';
                str2 = 'Only "LatLong", "PingBeam"and "DepthAcrossDist" geometries can be plot into GLOBE.';
                my_warndlg(Lang(str1,str2), 1);
        end

    case 'EXPORT_ERS_Image_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.ers', this.repExport, 'FullName');
        if flag
            options = paramsExportErMapper(this.Images(this.indImage));
            flag = export_ermapper(this.Images(this.indImage), nomFic, 'options', options, ...
                'subx', subx, 'suby', suby);
            if flag
                open_ErViewer(this.Images(this.indImage), nomFic, options) % Affichage dans iView3D
            end
        end
        
    case 'EXPORT_ERS_ImagesSynchro_CI1'
        [flag, this.repExport] = my_uigetdir(this.repExport, 'Name of the directory where the Surfer 7 files will be created');
        if flag
            export_MultiLayersErmapperSameFrame(this.Images, this.indImage, this.repExport, subx, suby)
        end
        
    case 'EXPORT_ERS_MultiImages_CI0'
        [flag, this.repExport] = my_uigetdir(this.repExport, 'Name of the directory where the .ers files will be created');
        if flag
            flag = export_MultiLayersErMapper(this.Images, this.indImage, this.repExport); %#ok<NASGU>
        end
        
    case 'EXPORT_CarisAscii_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.txt', this.repExport);
        if flag
            export_CarisAscii(this.Images(this.indImage), nomFic, ...
                'subx', subx, 'suby', suby);
        end

    case 'EXPORT_AsciiMultiLayer_CI1'
        [flag, nomFic, indLayers, flagX, flagY, typeExport, this.repExport] = paramsExportAsciiMultiLayer(this.Images, this.indImage, this.repExport);
        if flag
            [flag, indLayerMask, valMask] = paramsMasque(this.Images, this.indImage);
            if flag
                if typeExport == 3
                    export_AsciiMultiLayerWithoutColocation(this.Images(this.indImage), this.Images(indLayers), nomFic, ...
                        'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                        'subx', subx, 'suby', suby);
                else
                    export_AsciiMultiLayer(this.Images(this.indImage), this.Images(indLayers), flagX, flagY, nomFic, typeExport, ...
                        'LayerMask', this.Images(indLayerMask), 'valMask', valMask, ...
                        'subx', subx, 'suby', suby);
                end
            end
        end
        
    case 'EXPORT_XYZ_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.xyz', this.repExport);
        if flag
            export_xyz(this.Images(this.indImage), nomFic, ...
                'subx', subx, 'suby', suby);
        end

    case 'EXPORT_Caraibes_CI1'
        [flag, this.repExport] = export_Caraibes(this.Images, this.indImage, this.repExport, ...
            'subx', subx, 'suby', suby);
        if ~flag
            return
        end

    case 'EXPORT_FicOrigine_CI1'
        flag = export_originalFormat(this.Images(this.indImage), subx, suby); %#ok<NASGU>
        
    case 'EXPORT_SurferASCII_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.grd', this.repExport, 'FullName');
        if flag
            export_surfer(this.Images(this.indImage), nomFic, 'Version', 5, ...
                'subx', subx, 'suby', suby);
        end

    case 'EXPORT_Surfer7Current_CI1'
        [flag, nomFic, this.repExport] = getFileNameLinked2ImageName(this.Images(this.indImage), '.grd', this.repExport, 'FullName');
        if flag
            export_surfer(this.Images(this.indImage), nomFic, 'Version', 7, ...
                'subx', subx, 'suby', suby);
        end
        
    case 'EXPORT_Surfer7Multi_CI0'
        [flag, repExport] = my_uigetdir(this.repExport, 'Name of the directory where the Surfer 7 files will be created');
        if flag
            flag = export_MultiLayersSurfer7(this.Images, this.indImage, repExport); %#ok<NASGU>
        end

    case 'Help'
        flag = SonarScopeHelp(varargin{2}); %#ok<NASGU>
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        return
end
