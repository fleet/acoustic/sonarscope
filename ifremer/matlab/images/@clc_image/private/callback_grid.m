% Traitement suite a une action sur la toolbar pour le profil x
% 
% Syntax
%   this = callback_grid(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_grid(this)

%% Récuperation de l'etat du bouton

this.ihm.Grid.cpn  = gerer_callback(this.ihm.Grid.cpn, this.ihm.Grid.msg);
this.visuGrid      = get_etat(this.ihm.Grid.cpn);

%% Mise a jour de la visualisation

if this.visuGrid
    set(this.hAxePrincipal, 'XGrid', 'on', 'YGrid', 'on');
    set(this.hAxeProfilX,   'XGrid', 'on', 'YGrid', 'on');
    set(this.hAxeProfilY,   'XGrid', 'on', 'YGrid', 'on');
else
    set(this.hAxePrincipal, 'XGrid', 'off', 'YGrid', 'off');
    set(this.hAxeProfilX,   'XGrid', 'off', 'YGrid', 'off');
    set(this.hAxeProfilY,   'XGrid', 'off', 'YGrid', 'off');
end
