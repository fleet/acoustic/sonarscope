% Fonctionnalite callback_import specifique au composant
%
% Syntax
%   a = callback_import(a)
%
% Input Arguments
%   a   : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_import(this, varargin)

% TODO
% DEBUT ABSOLUMENT NECESSAIRE / C'EST LE SEUL MOYEN QUE J'AI TROUVE POUR
% EVITER UN BUG LORSQUE L'ON CHARGE UN PROJET QUI CONTIENT UN OBJET cl_memmapfile
m = cl_memmapfile('Value', [1 2 3 4; 5 6 7 8], 'LogSilence', 1); %#ok<NASGU>
clear m
% fIN ABSOLUMENT NECESSAIRE

memeReponses = false;

if nargin == 1
    msg = 'ImportationFiles';
else
    msg = varargin{1};
end

%% Lancement des traitements

Carto = [];

I0 = cl_image_I0;
a  = cl_image.empty;
Answers  = [];

flag       = 1;
SonarIdent = [];
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'ImportationFiles'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', ...
            {'.all'; '.kmall'; '.asc'; '.bag'; 'bmp'; '.bob' ;'.csv';'.dtm'; '.ers'; '.grd'; '.gxf'; '.hac'; '.hdr'; '.hgt'; '.im'; '.imo'
            '.jpg'; '.jpeg'; '.jp2'; '.gif'; '.lgz'; '.mat'; '.mbb'; '.mbg';
            '.mnt'; '.mos'; '.gri'; '.nc'; '.png'; '.raw'; '.rdf'; '.sd'; '.sdf'; '.s7k'; '.seg'; '.sgy'; '.segy'; '.shp';
            '.tif'; '.tiff'; '.tomo'; '.txt'; '.xml'; '.xtf'; '.xyz'; '.fig'; '.avi'; '.odv'}, 'AllFilters', 1, ...
            'RepDefaut', this.repImport);
        
    case 'ImportationLayerComplementaire'
        nomFic = this.Images(this.indImage).InitialFileName;
        if isempty(nomFic)
            my_warndlg('No InitialFileName defined for this image', 1);
            return
        end
        [~, ~, ext] = fileparts(nomFic);
        if strcmp(ext, '.ers')
            str = sprintf('%s file does not contain other layers.', nomFic);
            my_warndlg(str, 1);
            flag = 0;
        end
        if strcmp(ext, '.memmapfile')
            str = sprintf('%s comes from a Project Import, link with original file is broken.', nomFic);
            my_warndlg(str, 1);
            flag = 0;
        end
        Carto = get(this.Images(this.indImage), 'Carto');
        
        % Verrue pour Geoswath : il faut absolument redemander la carto qui
        % doit �tre d�finie en Lambert 93 pour Haliotis et UTM pour UNH
        SonarDescription = get(this.Images(this.indImage), 'SonarDescription');
        if ~isempty(SonarDescription)
            SonarName = get(SonarDescription, 'SonarName');
            if ~isempty(SonarName) && (strcmp(SonarName, 'GeoSwath+') || strcmp(SonarName, 'GeoSwath'))
                GT = this.Images(this.indImage).GeometryType;
                if GT ~= cl_image.indGeometryType('PingSamples')
                    Carto = [];
                end
            end
        end
        
    case 'ImportationExamplesEM12D'
        [nomFic, flag] = selectionFichierFiltre('EM12D*');
        SonarIdent = 1; % Valeur definie dans cl_sounder
    case 'ImportationExamplesEM12S'
        [nomFic, flag] = selectionFichierFiltre('EM12S*');
        SonarIdent = 2;
    case 'ImportationExamplesEM300'
        [nomFic, flag] = selectionFichierFiltre('EM300*');
        SonarIdent = 3;
    case 'ImportationExamplesEM1000'
        [nomFic, flag] = selectionFichierFiltre('EM1000*');
        SonarIdent = 4;
    case 'ImportationExamplesEM1002'
        [nomFic, flag] = selectionFichierFiltre('EM1002*');
        SonarIdent = 10;
    case 'ImportationExamplesEM120'
        [nomFic, flag] = selectionFichierFiltre('EM120*');
        SonarIdent = 11;
    case 'ImportationExamplesEM3000D'
        [nomFic, flag] = selectionFichierFiltre('EM3000D*');
        SonarIdent = 6;
    case 'ImportationExamplesEM3000S'
        [nomFic, flag] = selectionFichierFiltre('EM3000S*');
        SonarIdent = 7;
    case 'ImportationExamplesDF1000'
        [nomFic, flag] = selectionFichierFiltre('DF1000*', 'Exclude', '.ifr');
        SonarIdent = 8;
    case 'ImportationExamplesSAR'
        [nomFic, flag] = selectionFichierFiltre('SAR*');
        SonarIdent = 5;
    case 'ImportationExamplesSARIM'
        [nomFic, flag] = selectionFichierFiltre('*.im');
        SonarIdent = 5;
    case 'ImportationExamplesDTS1'
        [nomFic, flag] = selectionFichierFiltre('DTS1*');
        SonarIdent = 9;
    case 'ImportationExamplesCOSMOS'
        [nomFic, flag] = selectionFichierFiltre('COSMOS*');
    case 'ImportationExamplesRESON'
        [nomFic, flag] = selectionFichierFiltre('RESON*');
        SonarIdent = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 'ImportationExamplesGeoacoustics'
        [nomFic, flag] = selectionFichierFiltre('*.rdf');
        SonarIdent = 16; 
        

    case {'ImportationExamplesERMAPPER1'; 'ImportationExamplesERMAPPER2'}
        [nomFic, flag] = selectionFichierFiltre('*.ers');
    case 'ImportationExamplesIMAGE'
        [nomFic, flag] = selectionFichierFiltre({'*.tif*'; '*.tiff*'; '*.jpg'; '*.jpeg'; '*.bmp*'; '*.png*'; '*.pgm*'; '*.gif*'; '*.ppm*'});
    case 'ImportationExamplesMATLAB'
        [nomFic, flag] = selectionFichierFiltre('*.mat');
    case 'ImportationExamplesHDR'
        [nomFic, flag] = selectionFichierFiltre('*.hdr');
    case 'ImportationExamplesSonarImage'
        [nomFic, flag] = selectionFichierFiltre({'*.imo*'; '*.mos*'});
    case 'ImportationExamplesSonarBathy'
        [nomFic, flag] = selectionFichierFiltre({'*.mbb*'; '*.xy*'});
    case 'ImportationExamplesSonarDTM'
        [nomFic, flag] = selectionFichierFiltre('*.mnt*');
    case 'ImportationExamplesASCII_XYZ'
        [nomFic, flag] = selectionFichierFiltre({'*.txt'; '*.xyz'}, 'Exclude', 'readme.txt');
    case 'ImportationExamplesFledermaus'
        [nomFic, flag] = selectionFichierFiltre('*.sd');
    case 'ImportationExamplesSimradAll'
        [nomFic, flag] = selectionFichierFiltre({'*.all'; '.kmall'});
    case 'ImportationExamplesSimradBag'
        [nomFic, flag] = selectionFichierFiltre('*.bag');
    case 'ImportationExamplesResonS7k'
        [nomFic, flag] = selectionFichierFiltre('*.s7k');
    case 'ImportationExamplesLGZ'
        [nomFic, flag] = selectionFichierFiltre('*.lgz');

    case 'ImportationExamplesCARAIBES'
        [nomFic, flag] = selectionFichierFiltre({'*.imo*'; '*.mos*'; '*.mbb*'; '*.xy'; '*.mnt*'});
    case 'ImportationExamplesIRAP'
        [nomFic, flag] = selectionFichierFiltre('*.irap*');
    case 'ImportationExamplesSURFER'
        [nomFic, flag] = selectionFichierFiltre('*.grd*');
    case 'ImportationExamplesTRITON'
        [nomFic, flag] = selectionFichierFiltre({'*.sonarmosaic*'; '*.class'});
    case 'ImportationExamplesVMAPPERASCII'
        [nomFic, flag] = selectionFichierFiltre('*.vmascii');
    case 'ImportationExamplesGMT'
        [nomFic, flag] = selectionFichierFiltre('*.grd*');
    case 'ImportationExamplesFLEDERMAUS'
        [nomFic, flag] = selectionFichierFiltre('*.sd*');
    case 'ImportationExamplesSRTM'
        [nomFic, flag] = selectionFichierFiltre('*.hgt*');
    case 'ImportationExamplesRDF'
        [nomFic, flag] = selectionFichierFiltre('*.rdf');
    case 'ImportationExamplesXTF'
        [nomFic, flag] = selectionFichierFiltre('*.xtf');
    case 'ImportationExamplesSEGY'
        [nomFic, flag] = selectionFichierFiltre({'*.seg'; '*.sgy'; '*.segy'});
    case 'ImportationExamplesBOB'
        [nomFic, flag] = selectionFichierFiltre({'*.bob'; '*.xml'});
    case 'ImportationExamplesADCPODV'
        [nomFic, flag] = selectionFichierFiltre({'*.odv'; '*.dat'; '*.xyz'});
        
    case 'ImportationWMSInfo'
        paramsImportWMS_Info;
        
    case 'ImportationWMS'
        [flag, wms, lon_range, lat_range, nx, ny] = paramsImportWMS(this);
        if ~flag
            return
        end
        [flag, a] = import_WMS(I0, wms, lon_range, lat_range, nx, ny);
        
    case 'ImportationExamplesLevitusAnnual'
        [flag, nomFicNc, subTime, subDepth] = paramsExtractionLevitus('Annual');
        if flag
            [flag, a] = import_Levitus(I0, nomFicNc, subTime, subDepth);
        end
        
    case 'ImportationExamplesLevitusSeason'
        [flag, nomFicNc, subTime, subDepth] = paramsExtractionLevitus('Season');
        if flag
            [flag, a] = import_Levitus(I0, nomFicNc, subTime, subDepth);
        end
        
    case 'ImportationExamplesLevitusMonth'
        [flag, nomFicNc, subTime, subDepth] = paramsExtractionLevitus('Month');
        if flag
            [flag, a] = import_Levitus(I0, nomFicNc, subTime, subDepth);
        end
         
    case 'ImportationExamplesWOAAnnual'
        [flag, nomFicNc, subDepth] = paramsExtractionWOA13('Annual');
        if flag
            [flag, a] = import_WOA13(I0, nomFicNc, subDepth);
        end
        
    case 'ImportationExamplesWOASeason'
        [flag, nomFicNc, subDepth] = paramsExtractionWOA13('Season');
        if flag
            [flag, a] = import_WOA13(I0, nomFicNc, subDepth);
        end
        
    case 'ImportationExamplesWOAMonth'
        [flag, nomFicNc, subDepth] = paramsExtractionWOA13('Month');
        if flag
            [flag, a] = import_WOA13(I0, nomFicNc, subDepth);
        end
        
    case 'ImportationExamplesETOPO_Earth'
        [Z, Lon, Lat, Comment, flag] = Etopo([-90 90], [-180 270]);
        if flag
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            a = cl_image('Image', Z, 'x', Lon, 'y', Lat, 'Name', Lang('Terre', 'Earth'), ...
                'DataType', cl_image.indDataType('Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);
        end
    case 'ImportationExamplesETOPO_France'
        [Z, Lon, Lat, Comment, flag] = Etopo([42 52], [-5 10]);
        if flag
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            a = cl_image('Image', Z, 'x', Lon, 'y', Lat, 'CLim', [0 500], 'Name', 'France (m)', ...
                'DataType', cl_image.indDataType('Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);
        end
    case 'ImportationExamplesETOPO_MediterraneanOccidental'
        [Z, Lon, Lat, Comment, flag] = Etopo([30 46], [-7 16]);
        if flag
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            a = cl_image('Image', Z, 'x', Lon, 'y', Lat, 'Name', Lang('Mediterranee Occidentale', 'Western Mediterranean Sea'), ...
                'DataType', cl_image.indDataType('Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);
        end
    case 'ImportationExamplesETOPO_MediterraneanOriental'
        [Z, Lon, Lat, Comment, flag] = Etopo([30 42], [15 36]);
        if flag
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            a = cl_image('Image', Z, 'x', Lon, 'y', Lat, 'Name', Lang('Mediterranee Orientale', 'Eastern Mediteranean Sea'), ...
                'DataType', cl_image.indDataType('Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);
        end
    case 'ImportationExamplesETOPO_AtlanticNorth'
        [Z, Lon, Lat, Comment, flag] = Etopo([5 55], [-50 -18]);
        if flag
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            a = cl_image('Image', Z, 'x', Lon, 'y', Lat, 'Name', Lang('Atlantique Nord', 'North Atlantic'), ...
                 'DataType', cl_image.indDataType('Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);
        end
    case 'ImportationExamplesETOPO_AtlanticSouth'
        [Z, Lon, Lat, Comment, flag] = Etopo([-65 5], [-45 11]);
        if flag
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            a = cl_image('Image', Z, 'x', Lon, 'y', Lat, 'Name', Lang('Atlantique Sud', 'South Atlantic'), ...
                'DataType', cl_image.indDataType('Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);
        end
    case 'ImportationExamplesETOPO_IndianOcean'
        [Z, Lon, Lat, Comment, flag] = Etopo([-60 30], [20 120]);
        if flag
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            a = cl_image('Image', Z, 'x', Lon, 'y', Lat, 'Name', Lang('Oc�an Indien', 'Indian Ocean / Arabic Sea'), ...
                'DataType', cl_image.indDataType('Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Comments', Comment, 'Carto', Carto);
        end
        
    case 'ImportationEGM96'
        [flag, LatLim, LonLim] = params_ImportEGM96(this.Images(this.indImage));
        if flag
            [flag, a] = importEGM96(this.Images(this.indImage), LatLim, LonLim);
        end

    case 'Help'
        flag = SonarScopeHelp(varargin{2});
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        return
end

if ~flag
    return
end

%% Suppression des doublons

if exist('nomFic', 'var')
    nomFic = supressionDoublonsFichiersErsXml(nomFic);
end

%% Lecture des images

if isempty(a) && exist('nomFic', 'var')
    if ~iscell(nomFic)
        nomFic = {nomFic};
    end

    QuestionJustifiee = 1;
    for k=1:length(nomFic)
        [nomDir, Name, ext] = fileparts(nomFic{k});
        if strcmp(ext, '.gz')
            disp(['Uncompress : ' nomFic{k} ' Please wait'])
            gunzip(nomFic{k}, nomDir);
            delete(nomFic{k})
            nomFic{k} = fullfile(nomDir, Name);
        end
        if strcmp(ext, '.zip')
            disp(['Uncompress : ' nomFic{k} ' Please wait'])
            unzip(nomFic{k}, nomDir);
            delete(nomFic{k})
            nomFic{k} = fullfile(nomDir, Name);
        end
        if strcmp(ext, '.ers')
            nomFicImage = fullfile(nomDir, Name);
            nomFicImagegz = [nomFicImage '.gz'];
            if ~exist(nomFicImage, 'file') && exist(nomFicImagegz, 'file')
                disp(['Uncompress : ' nomFicImagegz ' Please wait'])
                gunzip(nomFicImagegz, nomDir);
                delete(nomFicImagegz)
            end
        end

        [~, ~, ext] = fileparts(nomFic{k});
        if strcmp(ext, '.all') || strcmp(ext, '.kmall')  || strcmp(ext, '.s7k') || strcmp(ext, '.ers') || strcmp(ext, '.grd')
            QuestionJustifiee = 1;
            sameTagSynchroX = 0;
            sameTagSynchroY = 0;
            sameTagSynchroContrast = 0;
        end
    end
    
    if (length(nomFic) > 1) && QuestionJustifiee
        if testSimradFile(nomFic)
            sameTagSynchroX = 0;
            sameTagSynchroY = 0;
            sameTagSynchroContrast = 0;
        else
            str1 = 'M�me TagSynchroX ?';
            str2 = 'Same TagSynchroX ?';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            sameTagSynchroX = (rep == 1);
            
            str1 = 'M�me TagSynchroY ?';
            str2 = 'Same TagSynchroY ?';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            sameTagSynchroY = (rep == 1);
            
            str1 = 'M�me TagSynchroContrast ? (utile uniquement pour avoir le m�me rehaussement de contraste sur toutes les images)';
            str2 = 'Same TagSynchroContrast ? (useful only if you want the same contrast enhancement on all the imported images)';
            [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
            if ~flag
                return
            end
            sameTagSynchroContrast = (rep == 1);
        end
    end

    %% R�cup�ration de la carto (si elle existe) dans l'image courante

    nbFics = length(nomFic);
    str1 = 'Importation de fichier';
    str2 = 'Importing files';
    hw = create_waitbar(Lang(str1,str2), 'N', nbFics);
    for k=1:nbFics
        my_waitbar(k, nbFics, hw)
        
        %% Importation des images

        if isempty(nomFic{k})
            continue
        end

        xyCross = this.cross(this.indImage, :);
        [flag, b, Carto, this.repImport, Answers] = import(this.Images(this.indImage), nomFic{k}, 'SonarIdent', SonarIdent, ...
            'repImport', this.repImport, 'memeReponses', memeReponses, 'Carto', Carto, 'xyCross', xyCross, ...
            'Answers', Answers);
        if ~flag
            continue
%             break
        end
        this.repImport = fileparts(nomFic{k});
        
        if strcmp(msg, 'ImportationLayerComplementaire')
            GeometryType = this.Images(this.indImage).GeometryType;
            if GeometryType ~= cl_image.indGeometryType('SampleBeam')
                TagSynchroX  = this.Images(this.indImage).TagSynchroX;
                TagSynchroY  = this.Images(this.indImage).TagSynchroY;
                for k2=1:length(b)
                    TC = b(k2).GeometryType;
                    if TC == GeometryType
                        SonarDescription1 = get_SonarDescription(this.Images(this.indImage));
                        SonarDescription2 = get_SonarDescription(b(k2));
                        if ~isempty(SonarDescription1) && ~isempty(SonarDescription2) && ...
                                (get(SonarDescription1, 'Sonar.Family') == get(SonarDescription2, 'Sonar.Family'))
                            b(k2) = set(b(k2), 'TagSynchroX', TagSynchroX);
                            b(k2) = set(b(k2), 'TagSynchroY', TagSynchroY);
                        end
                    elseif (TC == cl_image.indGeometryType('PingSamples'))     || ...
                            (TC == cl_image.indGeometryType('PingRange'))      || ...
                            (TC == cl_image.indGeometryType('PingAcrossDist')) || ...
                            (TC == cl_image.indGeometryType('PingTravelTime')) || ...
                            (TC == cl_image.indGeometryType('PingBeam'))
                        b(k2) = set(b(k2), 'TagSynchroY', TagSynchroY);
                    end
                end
            end
        else
            if (k > 1) && sameTagSynchroX
                TagSynchroX = a(end).TagSynchroX;
                for k2=1:length(b)
                    b(k2).TagSynchroX = TagSynchroX;
                end
            end

            if (k == 1) && (length(nomFic) > 1) && QuestionJustifiee
                str1 = 'Les r�ponses sont-elles les m�me pour les autres fichiers ?';
                str2 = 'Are the answers concerning the sonar the same for all the files ?';
                [memeReponses, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
            end

            if (k > 1) && sameTagSynchroY
                TagSynchroY = a(end).TagSynchroY;
                for k2=1:length(b)
                    b(k2) = set(b(k2), 'TagSynchroY', TagSynchroY);
                end
            end

            if (k > 1) && sameTagSynchroContrast
                TagSynchroContrast = a(end).TagSynchroContrast;
                for k2=1:length(b)
                    DataType = b(k2).DataType;
                    b(k2) = set(b(k2), 'TagSynchroContrast', [TagSynchroContrast '_' cl_image.strDataType{DataType}]);
                end
            end
        end

        for k2=1:length(b)
            a(end+1) = b(k2); %#ok
        end
        clear b

        %% Dechargement automatique d'images en m�moire virtuelle si besoin

        b = optimiseMemory([this.Images(:); a(:)], 'SizeMax', 500000);
        ni = length(this.Images);
        this.Images = b(1:ni);
        a = b(ni+1:end);
    end
    my_close(hw, 'MsgEnd')
end

%% Ajout de l'image dans la liste et visualisation

if flag
    this = add_image(this, a);
    this = editer_image_selectionnee(this);
end

%% Nettoyage des fichier cr��s par cl_memmapfile

% garbage_collector
        

function flag = testSimradFile(listeFic)

flag = true;
[~, ~, ext1] = fileparts(listeFic{1});
for k=1:length(listeFic)
    [~, ~, ext2] = fileparts(listeFic{k});
    if ~strcmp(ext1, ext2)
        flag = false;
        return
    end
end
