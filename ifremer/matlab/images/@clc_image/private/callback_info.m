% Fonctionnalit�s callback_info specifiques au composant
%
% Syntax
%   a = callback_info(a)
%
% Input Arguments
%   a : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_info(this, varargin)

if nargin == 1
    msg = 'Info_ImageTable';
else
    msg = varargin{1};
end

flag = 0;

%% Message identifiant la callback

WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg % CallbackInfoRenameFiles
    case 'Info_TidyUpImages'
        [flag, this] = TidyUpImages(this);
        
    case 'Info_SummuryOfLoadedImages'
        lastIndImage = this.indImage;
        flag = summary(this.Images, 'ExportResults', 0, 'nomDir', this.repExport, ...
            'SelectLayer', lastIndImage, 'OKCancel', 0, ...
            'QueryMode', 'Summary', 'WindowStyle', 'normal');

    case 'Info_ImageClassic'
        strOut = char(this.Images(this.indImage), 'InfoSonar', 1);
        if ~isdeployed
            disp(strOut);
        end
        my_txtdlg(Lang('Informations g�n�rales sur l''image', 'General information on image'), str2cell(strOut), ...
            'windowstyle', 'normal');
        
    %{
    case 'Info_ImageXML' % Comment� pour le moment car plantage de l'export xml sur structure Carto - Ellipso�de
        nomFic = fullfile(this.repExport, 'ImageInfo.xml');
        [flag, nomFic] = my_uiputfile('*.xml', 'Give a file name', nomFic);
        if ~flag
            return
        end
        flag = export_info_xml(this.Images(this.indImage), nomFic);
        if flag
            this.repExport = pathname;
            cmd = sprintf('winopen(''%s'')', nomFic);
            eval(cmd);
        end
            %}
        
    case 'Info_ImageTable'
        editProperties(this.Images(this.indImage));
        
    case 'Info_SonarData'
        strOut = char(this.Images(this.indImage), 'InfoSonar', 2);
        if ~isdeployed
            disp(strOut)
        end
        my_txtdlg(Lang('Information sur les donn�es sonar', 'Information on sonar data'), str2cell(strOut), ...
            'windowstyle', 'normal');
        
    case 'Info_SonarDataEquation'
        [flag, strOut, cStrOut, Title] = get_BS_Status(this.Images(this.indImage));
        if flag
            if ~isdeployed
                disp(strOut)
            end
            my_txtdlg(Title, cStrOut, 'windowstyle', 'normal');
        end
        
    case 'TagInfoSonarBathymetryStatus'
        [flag, strOut] = get_Bathy_Status(this.Images(this.indImage));
        if flag
            if ~isdeployed
                disp(strOut)
            end
            my_txtdlg(Lang('Bathymetry status', 'Bathymetry status'), str2cell(strOut), ...
                'windowstyle', 'normal');
        end
        
    case 'TagInfoSonarCharacteristicsLight'
        strOut = char_sonar(this.Images(this.indImage), 'Display', 1);
        if ~isdeployed
            disp(strOut)
        end
        my_txtdlg(Lang('Informations compactes sur le sonar', 'Compact information on the sonar system'), ...
            str2cell(strOut), 'windowstyle', 'normal');
        
    case 'TagInfoSonarCharacteristicsMedium'
        strOut = char_sonar(this.Images(this.indImage), 'Display', 2);
        if ~isdeployed
            disp(strOut)
        end
        my_txtdlg(Lang('Informations "medium" sur le sonar', 'Intermediate information on the sonar system'), ...
            str2cell(strOut), 'windowstyle', 'normal');
        
    case 'TagInfoSonarEmissionModelsConstructeur'
        strOut = char_sonar(this.Images(this.indImage), 'Display', 3);
        if ~isdeployed
            disp(strOut)
        end
        my_txtdlg(Lang('Informations compl�tes sur le sonar', 'Full information on the sonar system'), ...
            str2cell(strOut), 'windowstyle', 'normal');
        
    case 'TagInfoSonarEmissionModelsCalibration'
        strOut = char_sonar(this.Images(this.indImage), 'Display', 4);
        if ~isdeployed
            disp(strOut)
        end
        my_txtdlg(Lang('Informations sur les diagrammes d''�mission calibr�s', 'Information on calibrated transmission diagrams'), ...
            str2cell(strOut), 'windowstyle', 'normal');
        
    case 'TagInfoSonarReceptionModelsConstructeur'
        strOut = char_sonar(this.Images(this.indImage), 'Display', 5);
        if ~isdeployed
            disp(strOut)
        end
        my_txtdlg(Lang('Informations sur les diagrammes d''�mission usine', 'Information on factory transmission diagrams'), ...
            str2cell(strOut), 'windowstyle', 'normal');
        
    case 'TagInfoSonarReceptionModelsCalibration'
        strOut = char_sonar(this.Images(this.indImage), 'Display', 6);
        if ~isdeployed
            disp(strOut)
        end
        my_txtdlg(Lang('Informations sur les diagrammes de r�ception calibr�s', 'Information on calibrated reception diagrams'), ...
            str2cell(strOut), 'windowstyle', 'normal');
        
    case 'TagInfoFile'
        info_originalFile(this.Images(this.indImage))
        
    case 'TagEditSonarDescription'
        [flag, this.Images(this.indImage)] = edit_SonarDescription(this.Images(this.indImage));
        
    case 'TagEditParametres'
        this.Images(this.indImage) = edit_attributs(this.Images(this.indImage));
        flag = 1;
        this = process_synchroX(this);
        this = process_synchroY(this);
        
    case 'TagRedefineXYAxes'
        this.Images(this.indImage) = edit_XYAxes(this.Images(this.indImage));
        flag = 1;
        this = process_synchroX(this);
        this = process_synchroY(this);
        
    case 'SetPropertiesAllImages'
        [flag, this] = modifyAttributsImages(this);
        
    case 'TagEditParametresLists'
        [flag, this.Images(this.indImage)] = modifyAttributsCurrentImage(this.Images(this.indImage));
        
    case 'TagEditCarto'
        Carto = getDefinitionCarto(this.Images(this.indImage), 'BypassCartoFolder', 1, 'CartoAuto', 0, 'nomDirSave', this.repImport);
        this.Images(this.indImage) = set(this.Images(this.indImage), 'Carto', Carto);
        flag = 1;
        
    case 'TagTipOfTheDay'
        TipOfTheDay
        
    case 'TagEditBenchmark'
        bench
        
    case 'TagCreateBlanckFigure'
        CreateBlanckFigure
        
    case 'TagResetSizeSScWindow'
        ResetSizeWorkWindow(this)
        
    case 'TagModifySizePhysTotalMemory'
        ModifySizePhysTotalMemory
        
    case 'TagPreferencesLangFrench'
        selectLang(this, 'FR')
        
    case 'TagPreferencesLangEnglish'
        selectLang(this, 'US')
        
    case 'TagPreferencesGUISelectImageClassical'
        selectGUISelectImage(this, 'On')
        
    case 'TagPreferencesGUISelectImageSimplified'
        selectGUISelectImage(this, 'Off')
        
    case 'TagPreferencesClickRightForFlagYes'
        selectUseClickRightForFlag(this, 'On')
        
    case 'TagPreferencesClickRightForFlagNo'
        selectUseClickRightForFlag(this, 'Off')
        
    case 'TagPreferencesNewFormatsRdfYes'
        selectUseNewFormatsRdf(this, 'On')
    case 'TagPreferencesNewFormatsRdfNo'
        selectUseNewFormatsRdf(this, 'Off')
        
    case 'TagPreferencesUseLogFileYes'
        selectUseLogFile(this, 'On')
        
    case 'TagPreferencesUseLogFileNo'
        selectUseLogFile(this, 'Off')
        
    case 'TagPreferencesTempDir'
        selectTempDir(this)
        
        %     case 'TagPreferencesMultithreadingOne'
        %         selectMultithreading(this, 1)
        %
        %     case 'TagPreferencesMultithreadingAuto'
        %         selectMultithreading(this, 'automatic')
        
    case 'TagPreferencesMexFilesYes'
        selectMexFiles(this, 1)
        
    case 'TagPreferencesMexFilesNo'
        selectMexFiles(this, 2)
        
    case 'TagPreferencesLevelQuestions1'
        this = callback_QuestionLevel(this, 'QL1');
        
    case 'TagPreferencesLevelQuestions2'
        this = callback_QuestionLevel(this, 'QL2');
        
    case 'TagPreferencesLevelQuestions3'
        this = callback_QuestionLevel(this, 'QL3');
        
    case 'TagPreferencesGraphicsOnTop1'
        this = selectGraphicsOnTop(this, '1');
        this = gerer_menus(this);
        
    case 'TagPreferencesGraphicsOnTop2'
        this = selectGraphicsOnTop(this, '2');
        this = gerer_menus(this);
        
    case 'TagIdentAlgoSnippets2OneValuePerBeam_dB'
        this = selectIdentAlgoSnippets2OneValuePerBeam(this, 1);
        this = gerer_menus(this);
        
    case 'TagIdentAlgoSnippets2OneValuePerBeam_Amp'
        this = selectIdentAlgoSnippets2OneValuePerBeam(this, 2);
        this = gerer_menus(this);
        
    case 'TagIdentAlgoSnippets2OneValuePerBeam_Enr'
        this = selectIdentAlgoSnippets2OneValuePerBeam(this, 3);
        this = gerer_menus(this);
        
    case 'TagIdentAlgoSnippets2OneValuePerBeam_Med'
        this = selectIdentAlgoSnippets2OneValuePerBeam(this, 4);
        this = gerer_menus(this);
        
    case 'TagInfoShortcuts'
        InfoShortcuts(this)
        
    case 'TagInfoMapDimensions'
        InfoMapDimensions(this, msg)
        
    case 'TagInfoCloseFigures'
        close_message_windows
        
    case 'CallbackInfoRenameFiles'
        [flag, listeFic, this.repImport] = uiSelectFiles('ExtensionFiles', '.*', 'RepDefaut', this.repImport);
        if flag
            [flag, motif1, motif2] = params_FonctionRenameFiles;
            if flag
                renameFiles(listeFic, motif1, motif2);
            end
        end
        
    case 'TagFindReplace'
        [flag, nomFic, this.repImport] = uiSelectFiles('ExtensionFiles', {'.txt'; '.csv'; '.xml'}, 'AllFilters', 1, ...
            'RepDefaut', this.repImport, 'Entete', 'IFREMER - SonarScoprpe : Select a text file');
        if flag
            [flag, string1, string2, string3, string4, suffix] = params_FindAndReplace;
            if flag
                findAndReplace(nomFic, string1, string2, string3, string4, suffix);
            end
        end
        
    case 'TagRenameWorkWindow'
        [flag, this] = RenameWorkWindow(this);
        
    case 'TagTestMail'
        SendEmailSupport(sprintf('Hi sonarscope@ifremer.fr,\nThis email was sent from the "Info / Test email to SSc support" of SonarScope.'));
        
    case 'ExOptimDialogBackscatter'
        ExOptimDialogBackscatter
        
    case 'ExSignalDialogMultiSectors'
        ExSignalDialogMultiSectors
        flag = SonarScopeHelp('ExSignalDialogMultiSectors_mlx.html');
        
	case 'ExSignalDialogMultiCurves'
        ExSignalDialogMultiCurves
        flag = SonarScopeHelp('ExSignalDialogMultiCurves_mlx.html');
        
	case 'ExSignalDialogMultiCurvesMultiSectors'
        ExSignalDialogMultiCurvesMultiSectors % TODO (pas atteignable pour le moment car comment� dans la cr�ation de SSc
        flag = SonarScopeHelp('ExSignalDialogMultiCurvesMultiSectors_mlx.html');

    case 'ExNavigationDialogAUV'
        ExNavigationDialogAUV
        flag = SonarScopeHelp('ExNavigationDialogAUV_mlx.html');
        
    case 'ExNavigationDialogDaurade'
        flag = ExNavigationDialogDaurade;
        if flag
            flag = SonarScopeHelp('ExNavigationDialogDaurade_mlx.html');
        end
        
    case 'ExSignalDialogAttitude'
        ExSignalDialogAttitude % TODO, faire un autre exemple avec plusieurs fichiers
        flag = SonarScopeHelp('ExSignalDialogAttitude_mlx.html');
        
    case 'ExSignalDialogSoundSpeedProfile'
        ExSignalDialogSoundSpeedProfile % TODO : attendre que les pbs de rotation soient r�gle�s pour faire le .mlx
        flag = SonarScopeHelp('ExSignalDialogSoundSpeedProfile.html');
        
    case 'ExNewPAMES'
        ExNewPAMES
        
    case 'Help'
        flag = SonarScopeHelp(varargin{2});

    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        flag = 0;
end

%% Ajout de l'image dans la liste et visualisation

if flag
    this  = editer_image_selectionnee(this);
end
