% Traitement des deplacements du curseur
%
% Syntax
%   this = callback_movePointer(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_movePointer(this)

this = nettoyage_coupe(this);

%% D�termination si le curseur est sur l'un des 3 axes

[flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);

if length(flag) < 3
    return
end

if any(flag)
    limites = axis(this.hAxePrincipal);
end

%% Position en X

if flag(1) || flag(2)
    if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3))
        if this.Images(this.indImage).XDir == 1
            valX = limites(1) + (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        else
            valX = limites(2) - (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        end
    else
        valX = this.cross(this.indImage, 1);
    end
else
    valX = this.cross(this.indImage, 1);
end

%% Position en Y

if flag(1) || flag(3)
    if (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))
        if this.Images(this.indImage).YDir == 1
            valY = limites(3) + (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        else
            valY = limites(4) - (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        end
    else
        valY = this.cross(this.indImage, 2);
    end
else
    valY = this.cross(this.indImage, 2);
end

if any(flag)
    set(this.hfig, 'Pointer', this.Pointer)
else
    set(this.hfig, 'Pointer', 'arrow')
end

%% Valeur du point

[valV, ix, iy] = get_val_xy(this.Images(this.indImage), valX, valY);
ValNaN  = this.Images(this.indImage).ValNaN;
if ~isnan(ValNaN)
    if valV == ValNaN
        valV = NaN;
    end
end
valV = double(valV);

if this.Images(this.indImage).DataType == cl_image.indDataType('RxTime')
    valV = datetime(valV, 'ConvertFrom', 'datenum', 'Format', 'yyyy-MM-dd HH:mm.ss.SSS');
end

switch this.Images(this.indImage).SpectralStatus
    case 1
    case 2
        valV = 20 * log10(abs(valV));
    case 3
        % A faire
end

%% Valeur du signal vertical

if flag(3)
    val = getValprofilY(this, iy);
    if ~isempty(val)
        set(get(this.hAxeProfilY, 'Title'), 'String', val);
    end
else
    Titre = this.profilY.Type;
    if strcmp(Titre, 'Image profile') || strcmp(Titre, 'Vertical profile')
        set(get(this.hAxeProfilY, 'Title'), 'String', Titre, 'Interpreter', 'none');
    else
        pppp = this.Images(this.indImage).Name;
        if isempty(pppp)
            set(get(this.hAxeProfilY, 'Title'), 'String', 'Image profile');
        else
            set(get(this.hAxeProfilY, 'Title'), 'String', Titre, 'Interpreter', 'none');
        end
    end
end

%% Mise � jour des valeurs indiqu�es par les composants de la toolbar

this.ihm.ValX.cpn     = set_value(this.ihm.ValX.cpn,     valX);
this.ihm.ValY.cpn     = set_value(this.ihm.ValY.cpn,     valY);
this.ihm.ValImage.cpn = set_value(this.ihm.ValImage.cpn, valV);
this.ihm.IX.cpn       = set_value(this.ihm.IX.cpn,       ix);
this.ihm.IY.cpn       = set_value(this.ihm.IY.cpn,       iy);

%% Trac� de la fen�tre de mise � NaN ou SetValue si on est dans l'un de ces 2 modes

if flag(1) && (strcmp(this.TypeInteractionSouris, 'SetNaN') || strcmp(this.TypeInteractionSouris, 'SetVal'))
    w = floor((this.EreaseWindow+1) / 2);
    H = w(1);
    L = w(2);
    
    c = cosd(-this.EreaseWindowAngle);
    s = sind(-this.EreaseWindowAngle);
    Rot = [c s; -s c];
    if H == 0
        H = 0.5;
    end
    if L == 0
        L = 0.5;
    end
    L = L - 0.5;
    H = H - 0.5;
    C = Rot * [L L -L -L L;-H H H -H -H] + repmat([ix;iy], 1, 5);
    C = floor(C);
    x = C(1,:);
    y = C(2,:);
    x(5) = x(1);
    y(5) = y(1);
    [x, y] = ij2xy(this.Images(this.indImage), x, y);
    XStep = this.Images(this.indImage).XStep;
    YStep = this.Images(this.indImage).YStep;
    x = x + XStep / 2;
    y = y - YStep / 2;
%     [x(5), y(5)] = ij2xy(this.Images(this.indImage), ix-w(2), iy-w(1));
%     [x(4), y(4)] = ij2xy(this.Images(this.indImage), ix-w(2), iy+w(1));
%     [x(3), y(3)] = ij2xy(this.Images(this.indImage), ix+w(2), iy+w(1));
%     [x(2), y(2)] = ij2xy(this.Images(this.indImage), ix+w(2), iy-w(1));
%     x(1) = x(5);
%     y(1) = y(5);
    hEraseWindox = findobj(this.hAxePrincipal, 'Tag', 'hEraseWindox');
    if isempty(hEraseWindox)
        IsHold = ishold;
        if ~IsHold
            hold on
        end
        hEraseWindox = plot(x, y);
        set(hEraseWindox, 'Tag', 'hEraseWindox');
        if ~IsHold
            hold off
        end
    else
        set(hEraseWindox, 'XData', x, 'YData', y);
    end
end


function val = getValprofilY(this, iy)

switch this.profilY.Type
    case {'Vertical profile'; 'Image profile'}
        val = [];
    case 'Time'
        X = get(this.Images(this.indImage), 'Time');
        if isempty(X)
            val = [];
        else
            if isa(X, 'cl_time')
                val = t2str(X(iy));
            else
                val = num2str(X(iy));
            end
        end
    otherwise
        X = get(this.Images(this.indImage), this.profilY.Type);
        if isempty(X)
            val = [];
        else
%             val = num2str(X(iy)); % TODO : g�rer la multidimensionalit� des signaux verticaux
            val = num2str(X(iy,:)); % TODO : g�rer la multidimensionalit� des signaux verticaux
            val = rmblank(val);
        end
end
