
function this = callback_moveProfils(this, varargin)

persistent flagPlotCurrentPoint % Ajout JMA le 30/05/2017 , � l'essai

drawnow % Obligatoire sinon lenteurs extr�mes si beaucoup d'images charg�es. Sans doute pour que la caalback puisse �tre interrompue.

% [varargin, CreateWindow] = getPropertyValue(varargin, 'CreateWindow', 0);

%% Determination si le curseur est sur l'un des 3 axes

[flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);

if ~any(flag)
    % On retrace les profils au point courant d�s que l'on sort d'un axe
    
    if isempty(flagPlotCurrentPoint) || flagPlotCurrentPoint % Ajout JMA le 30/05/2017 , � l'essai
        
        % TODO : devrait trouver un moyen pour que �a ne retace pas � chaque
        % fois que l'on bouge la souris en dehors des axes : comparer la
        % position ?
        
        %% Affichage des profils et des valeurs
        
        this = show_profils(this);
        
        %% Traitement des synchronisations
        
        this = process_synchroX(this);
        this = process_synchroY(this);
        
        flagPlotCurrentPoint = false; % Ajout JMA le 30/05/2017 , � l'essai
    end
    return
end
flagPlotCurrentPoint = true; % Ajout JMA le 30/05/2017 , � l'essai

set(this.hfig, 'Pointer', 'arrow');

%% Position en X

if flag(1) || flag(2)
    if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3))
        % D�termination des limites de l'axe principal en x et y
        
        limites = axis(this.hAxePrincipal);
        
        % D�termination de l'abscisse
        if this.Images(this.indImage).XDir == 1
            this.cross(this.indImage, 1) = limites(1) + (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        else
            this.cross(this.indImage, 1) = limites(2) - (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        end
    end
end

%% Position en Y

if flag(1) || flag(3)
    if (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))
        % D�termination des limites de l'axe principal en x et y
        limites = axis(this.hAxePrincipal);
        
        % D�termination de l'ordonn�e
        if this.Images(this.indImage).YDir == 1
            this.cross(this.indImage, 2) = limites(3) + (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        else
            this.cross(this.indImage, 2) = limites(4) - (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        end
    end
end

%% Affichage des profils et des valeurs

this = show_profils(this);

%% Traitement des synchronisations

this = process_synchroX(this);
this = process_synchroY(this);

