% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_nbProfilesHV(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_nbProfilesHV(this)

%% R�cuperation de l'�tat du bouton

this.ihm.NbVoisins.cpn  = gerer_callback(this.ihm.NbVoisins.cpn, this.ihm.NbVoisins.msg);
this.NbVoisins          = get_value(this.ihm.NbVoisins.cpn);

%% Affichage des profils et des valeurs

this = show_profils(this);

%% Affichage des valeurs du voisinage de la croix

private_type_voisins_xy(this);
