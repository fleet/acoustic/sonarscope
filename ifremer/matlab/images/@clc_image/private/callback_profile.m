% Traitement de la demande de coupe interactive dans l'image
% 
% Syntax
%   this = callback_profile(this, mouvement)
%
% Input Arguments
%   this      : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_profile(this)

%% Determination si le curseur est sur l'un des 3 axes

flag = test_curseur_sur_axes(this);
if ~flag(1)
    return
end

this = traiter_click_coupe(this);
