% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_profileHorz(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_profileHorz(this)

%% R�cuperation de l'�tat du bouton

this.ihm.CoupeHorz.cpn = gerer_callback(this.ihm.CoupeHorz.cpn, this.ihm.CoupeHorz.msg);
this.visuCoupeHorz     = get_etat(this.ihm.CoupeHorz.cpn);

%% Mise � jour de la visualisation

this = positionner(this);

%% Traitement de la visibilit� de la croix

traiter_visibilite_croix(this);
