% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_profileVert(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_profileVert(this, varargin)

[varargin, NoSelectionSignaVert] = getPropertyValue(varargin, 'NoSelectionSignaVert', 0);

[varargin, DirectWay] = getFlag(varargin, 'DirectWay'); %#ok<ASGLU>

if DirectWay
    this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
end

%% R�cup�ration de l'�tat du bouton

this.ihm.CoupeVert.cpn = gerer_callback(this.ihm.CoupeVert.cpn, this.ihm.CoupeVert.msg);
this.visuCoupeVert     = get_etat(this.ihm.CoupeVert.cpn);

if this.visuCoupeVert && ~NoSelectionSignaVert
    liste = get_liste_vecteurs(this.Images(this.indImage), 'VerticalOnly');
    if length(liste) > 1
        str1 = 'Courbe � visualiser (V)';
        str2 = 'Curve to display (V)';
        [etat, flag] = my_listdlg(Lang(str1,str2), liste, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        this.profilY.Type = liste{etat};
        this = show_profils(this);
    end
end

%% Mise � jour de la visualisation

this = positionner(this);

%% traitement de la visibilit� de la croix

traiter_visibilite_croix(this);
