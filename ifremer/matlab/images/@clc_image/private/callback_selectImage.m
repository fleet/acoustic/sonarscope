% Mise a jour du composant callback_selectImage
% 
% Syntax
%   this = callback_selectImage(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_selectImage(this, varargin)

global IfrTbxGUISelectImage %#ok<GVMIS>

lastIndImage = this.indImage;

strChoix = listeLayers(this);
if length(strChoix) == 1
    str1 = 'Il n''y a qu''une seule image dans la pile.';
    str2 = 'There is only one image in the pile.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

if isempty(varargin)
    if isempty(IfrTbxGUISelectImage)
        IfrTbxGUISelectImage = 1;
    end
    
    if IfrTbxGUISelectImage == 1
        [flag, indImage, strOut] = summary(this.Images, 'ExportResults', 0, 'nomDir', this.repExport, ...
            'SelectLayer', lastIndImage, 'QueryMode', 'UniSelect');
        if ~flag
            % Bouton Cancel
            return
        end
        if isempty(indImage) || isempty(strOut)
            return
        end
        for k=1:length(this.Images)
            strIn{k,1} = this.Images(k).Name; %#ok<AGROW>
        end
        % Recherche du layer concern� par les choix.
        strOut = strOut(indImage,1);
        % Inibition du message apparaissant en mode D�ploy� (???).
        % http://www.mathworks.com/matlabcentral/answers/74169-unique-rows-cell-array-ignored
        warning('off', 'MATLAB:INTERSECT:RowsFlagIgnored');
        % R�cup�ration des indices de tri.
        [C,ia,ib] = intersect(strIn,strOut,'stable');  %#ok<ASGLU>
        warning('on', 'MATLAB:INTERSECT:RowsFlagIgnored');
        indImage = ia;
    else % N�cessaire pour �cran 4K d'Arne
        for k=1:length(this.Images)
            strIn{k,1} = this.Images(k).Name; %#ok<AGROW>
        end
        str1 = 'S�lectionner une image';
        str2 = 'Select one image';
        [rep, flag] = my_listdlg(Lang(str1,str2), strIn, 'SelectionMode', 'Single', ...
            'InitialValue', lastIndImage);
        if ~flag
            return
        end
        indImage = rep;
    end

else
    if strcmp(varargin{1}, 'NextImage')
        indImage = max(1, min(this.indImage + 1, length(this.Images)));
    elseif strcmp(varargin{1}, 'PreviousImage')
        indImage = max(1, min(this.indImage - 1, length(this.Images)));
    end
end

if indImage == lastIndImage
    return
else
    this.indImage = indImage;
end

%% Edition de l'image

this = editer_image_selectionnee(this);
this = traiter_typeInteractionSouris(this);

idle(this.hfig)
