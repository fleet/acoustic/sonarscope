% Traitement des deplacements du curseur
%
% Syntax
%   this = callback_setVal(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_setVal(this, varargin)

%% Détermination si le curseur est sur l'un des 3 axes

[flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);

if ~any(flag)
    return
end

[varargin, KeyPress] = getFlag(varargin, 'KeyPress'); %#ok<ASGLU>

if KeyPress
    CurrentCharacter = get(gcf, 'CurrentCharacter');
    if isempty(CurrentCharacter) || double(CurrentCharacter) ~=  32
        return
    end
    EraseWindow = [1 1];
    EraseAngle = 0;
    if ~this.EreasePixelAction
        return
    end
else
    EraseWindow = this.EreaseWindow;
    EraseAngle  = this.EreaseWindowAngle;
end

Valeur = this.SetValue;

SelectionType = get(this.hfig, 'SelectionType');
switch SelectionType
    case 'open'
        this = traiter_click_SetVal(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle, Valeur);
    case 'extend'
        this = traiter_click_SetVal(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle, Valeur);
    case 'alt' % Clck droit
        this = traiter_click_SetVal(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle, Valeur);
    case 'normal'
        this = traiter_click_SetVal(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle, Valeur); % Click droit
    otherwise
        str = sprintf('Mouse action "%s3 not interpreted', SelectionType);
        my_warndlg(str, 0);
end
