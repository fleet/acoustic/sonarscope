% Traitement suite a une action sur la toolbar pour le profil x
% 
% Syntax
%   this = callback_symetrieH(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_symetrieH(this)

%% R�cuperation de l'�tat du bouton

this.ihm.XDir.cpn = gerer_callback(this.ihm.XDir.cpn, this.ihm.XDir.msg);
choixXDir         = get_etat(this.ihm.XDir.cpn);

h = get_handle(this.ihm.XDir.cpn);
icone = get(h, 'CData');
icone = my_fliplr(icone);
set(h, 'CData', icone);
    
%% Transmission de l'�tat dans l'instance cl_image

this.Images(this.indImage) = set(this.Images(this.indImage), 'XDir', choixXDir+1);

%% Mise � jour de la visualisation

if this.Images(this.indImage).XDir == 1
    set(this.hAxePrincipal, 'XDir', 'normal');
    set(this.hAxeProfilX,   'XDir', 'normal');
else
    set(this.hAxePrincipal, 'XDir', 'reverse');
    set(this.hAxeProfilX,   'XDir', 'reverse');
end
