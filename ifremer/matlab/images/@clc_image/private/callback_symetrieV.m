% Traitement suite a une action sur la toolbar pour le profil x
% 
% Syntax
%   this = callback_symetrieV(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_symetrieV(this)

%% R�cuperation de l'�tat du bouton

this.ihm.YDir.cpn = gerer_callback(this.ihm.YDir.cpn, this.ihm.YDir.msg);
choixYDir         = get_etat(this.ihm.YDir.cpn);

h = get_handle(this.ihm.YDir.cpn);
icone = get(h, 'CData');
icone = my_flipud(icone);
set(h, 'CData', icone);

%% Transmission de l'�tat dans l'instance cl_image

this.Images(this.indImage) = set(this.Images(this.indImage), 'YDir', choixYDir+1);

%% Mise � jour de la visualisation

if this.Images(this.indImage).YDir == 1
    set(this.hAxePrincipal, 'YDir', 'normal');
    set(this.hAxeProfilY,   'YDir', 'normal');
else
    set(this.hAxePrincipal, 'YDir', 'reverse');
    set(this.hAxeProfilY,   'YDir', 'reverse');
end
