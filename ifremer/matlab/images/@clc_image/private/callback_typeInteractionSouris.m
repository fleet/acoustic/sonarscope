% Traitement suite a une action sur le bouton Zoom
% 
% Syntax
%   this = callback_typeInteractionSouris(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_typeInteractionSouris(this, varargin)

if nargin == 1
    MessageTraitementDroite
    return 
end
msg = varargin{1};

%% IHM pour CleanData

switch msg
    case 'CleanData'
        try
            display_messageCleanData
            this.hGUITypeInteractionSouris = CleanSSCLayerDialog();
            this.hGUITypeInteractionSouris.openDialog();
            
        catch % Truc incompr�hensible lors de la premi�re utilisation, � bas le GUI !
            this.hGUITypeInteractionSouris = CleanSSCLayerDialog();
            this.hGUITypeInteractionSouris.openDialog();
        end
    otherwise
        if ~isempty(this.hGUITypeInteractionSouris) && ishandle(this.hGUITypeInteractionSouris)
            delete(this.hGUITypeInteractionSouris)
        end
end

%% Lancement des traitements

switch msg
    case 'PointClick'
        this.TypeInteractionSouris = 'PointClick';
        this = traiter_typeInteractionSouris(this);
        
    case 'ZoomInteractif'
        this.TypeInteractionSouris = 'ZoomInteractif';
        this = traiter_typeInteractionSouris(this);
        
    case 'Centrage'
%         zoom off
        this.TypeInteractionSouris = 'Centrage';
        msgCallBack = built_cb(this.composant, this.msgClickCentre);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         this = traiter_typeInteractionSouris(this); % Apparemment inutile
        
    case 'Coupe'
        str1 = 'Des clicks normaux ajouteront des sommets au profil. Un shift-, right-, ou double-click ajoutera un point final au profil. Return ou Enter terminera le profil sans rajouter de point final. Backspace ou Delete supprimera le dernier point du profil.';
        str2 = 'Use normal button clicks to add points to the polyline. A shift-, right-, or double-click adds a final point and ends the polyline selection. Pressing Return or Enter ends the polyline selection without adding a final point. Pressing Backspace or Delete removes the previously selected point from the polyline.';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'HelpPolyline');
        this.TypeInteractionSouris = 'Coupe';
        this = traiter_typeInteractionSouris(this);
        
    case 'SetNaN'
        this.TypeInteractionSouris = 'SetNaN';
        
        [flag, Ans, Azimuth] = saisie_windowSetNaN(this);
        if ~flag
            this.TypeInteractionSouris = 1;
            return
        end
        this.EreaseWindow = Ans;
        this.EreaseWindowAngle = Azimuth;
        
        str1 = sprintf('Cliquer sur un pixel supprimera une fen�tre de largeur %s. La touche "espace" permet de supprimer un seul pixel.  Il est recommand� de remettre � jour les statistiques de l''image lorsque vous aurez termin� l''op�ration de nettoyage de l''image (ou m�me en cours de nettoyage), ce traitement est disponible dans le menu statistiques.' , ...
            num2strCode(this.EreaseWindow));
        str2 = sprintf('A click on a pixel will erase a window of size %s. The "space" key can erase the pixel where the pointer is.  (message for JMA : TODO)' , ...
            num2strCode(this.EreaseWindow));
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'Mode Pixel NaN');

        [choix, flag] = my_questdlg(Lang('La touche "Espace" peut effacer le pixel. Voulez-vous activer cette fonctionnalit� ?', ...
            'Pressing "Space" key can erase the pixel. Do you want to activate this functionanity ?'), ...
            'Init', 2);
        if ~flag
            return
        end
        if choix == 1
            this.EreasePixelAction = 1;
            str1 = 'ATTENTION : Cette fonctionnalit� restera active m�me si vous revenez en mode "Point & Click" ou "zoom". Pour supprimer cette fonctionnalit� il vous faudra rappeler le mode NaN et r�pondre "Non" � la question.';
            str2 = 'This fonctionality will be always active even in "Point & Click" and "Zoom" modes. To remove it you will have to recall "NaN" mode and answer "No" to this question.';
            my_warndlg(Lang(str1,str2), 1);
        else
            this.EreasePixelAction = 0;
        end

        this = traiter_typeInteractionSouris(this);
        
    case 'SetVal'
        this.TypeInteractionSouris = 'SetVal';
        
        [flag, Value, Ans, Azimuth] = saisie_windowSetValue(this);
        if ~flag
            this.TypeInteractionSouris = 1;
            return
        end
        this.EreaseWindow = Ans;
        this.SetValue = Value;
        this.EreaseWindowAngle = Azimuth;
        
        str1 = sprintf('Cliquer sur un pixel supprimera une fen�tre de largeur %s. La touche "espace" permet de supprimer un seul pixel.  Il est recommand� de remettre � jour les statistiques de l''image lorsque vous aurez termin� l''op�ration de nettoyage de l''image (ou m�me en cours de nettoyage), ce traitement est disponible dans le menu statistiques.' , ...
            num2strCode(this.EreaseWindow));
        str2 = sprintf('A click on a pixel will erase a window of size %s. The "space" key can erase the pixel where the pointer is.  (message for JMA : TODO)' , ...
            num2strCode(this.EreaseWindow));
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'Mode Pixel NaN');

        [choix, flag] = my_questdlg(Lang('La touche "Espace" peut effacer le pixel. Voulez-vous activer cette fonctionnalit� ?', ...
            'Pressing "Space" key can erase the pixel. Do you want to activate this functionanity ?'), ...
            'Init', 2);
        if ~flag
            return
        end
        if choix == 1
            this.EreasePixelAction = 1;
            str1 = 'ATTENTION : Cette fonctionnalit� restera active m�me si vous revenez en mode "Point & Click" ou "zoom". Pour supprimer cette fonctionnalit� il vous faudra rappeler le mode NaN et r�pondre "Non" � la question.';
            str2 = 'This fonctionality will be always active even in "Point & Click" and "Zoom" modes. To remove it you will have to recall "NaN" mode and answer "No" to this question.';
            my_warndlg(Lang(str1,str2), 1);
        else
            this.EreasePixelAction = 0;
        end

        this = traiter_typeInteractionSouris(this);
       
    case 'CleanData'
         % [flag, typeInteraction, Value] = testGUI(this.hGUITypeInteractionSouris)
        this.TypeInteractionSouris = 'CleanData';
        this = traiter_typeInteractionSouris(this, 'AskWidthsIfBrush');
        % TODO : placer un addlistener sur les boutons (s'inspirer de
        % http://undocumentedmatlab.com/blog/setting-axes-tick-labels-format/)
        
    case 'MoveProfils'
        this.TypeInteractionSouris = 'MoveProfils';
        this = traiter_typeInteractionSouris(this);
        
    case 'DrawHeight'
        GeometryType = [cl_image.indGeometryType('PingRange') cl_image.indGeometryType('PingSamples')];
        flag = testSignature(this.Images(this.indImage), 'GeometryType', GeometryType);
        if flag
            str1 = 'Des clicks normaux ajouteront des sommets au profil. Un shift-, right-, ou double-click ajoutera un point final au profil. Return ou Enter terminera le profil sans rajouter de point final. Backspace ou Delete supprimera le dernier point du profil.';
            str2 = 'Use normal button clicks to add points to the polyline. A shift-, right-, or double-click adds a final point and ends the polyline selection. Pressing Return or Enter ends the polyline selection without adding a final point. Pressing Backspace or Delete removes the previously selected point from the polyline.';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'HelpPolyline');
            this.TypeInteractionSouris = 'DrawHeight';
            this = traiter_typeInteractionSouris(this);
        end
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
end

this = modify_ihm_TypeInteraction(this);


function display_messageCleanData

clear str2
str2{1} = 'You can clean the data on the two curves AND on the image';
str2{end+1} = ' ';
str2{end+1} = 'Select the type of interaction on the external window (Rectangle / Freehand, ...)';
str2{end+1} = 'Give the replacement value (default : NaN)';
str2{end+1} = 'Click a first time to give the focus on the axe you wnt to use';
str2{end+1} = ' ';
str2{end+1} = 'For a rectangle.';
str2{end+1} = 'You then click (without releasing the button) on the first corner of the rectangle';
str2{end+1} = 'then you move the pointer to the opposite corner. All the values inside the rectangle';
str2{end+1} = 'will be removed of the image.';
str2{end+1} = ' ';
str2{end+1} = 'For a freehand patch.';
str2{end+1} = 'You then draw the patch. The end point will be connected to the first one.';
str2{end+1} = 'All the values inside the patch will be removed of the image.';
str2{end+1} = ' ';
str2{end+1} = 'For a polyline patch.';
str2{end+1} = 'Click left to give the points of the polyline, then click right to close the ployline.';
str2{end+1} = 'then double-click for validatation.';
str2{end+1} = ' ';
str2{end+1} = 'For Rubber.';
str2{end+1} = 'Click left near the curves, the nearest values will be removed.';
str2{end+1} = 'It works only on the curves for the moment.';
str2{end+1} = ' ';
str2{end+1} = 'When you click "Ctrl+left" or "Click right", you indicate to SonarScope that you want to change the current point.';

str1 = str2;

str3 = 'Aide � l''utilisation de "CleanData".';
str4 = 'How to use "Clean data"';
edit_infoPixel(Lang(str1,str2), [], 'PromptString', Lang(str3,str4))
