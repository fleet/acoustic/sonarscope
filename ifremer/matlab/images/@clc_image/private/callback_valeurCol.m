% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_valeurCol(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_valeurCol(this, msg)

%% Callback du composant

% this.ihm.IX.cpn = set(this.ihm.IX.cpn, 'increment',  max(1,this.NbVoisins)); % Commenté par JMA le 12/11/2018
this.ihm.IX.cpn = gerer_callback(this.ihm.IX.cpn, msg);

%% Récuperation de la valeur editée

value = get_value(this.ihm.IX.cpn);

%% Calcul des coordonnées (i,j) de la croix

x = this.cross(this.indImage, 1);
y = this.cross(this.indImage, 2);
[ix, iy] = xy2ij(this.Images(this.indImage), x, y); %#ok<ASGLU>

%% Substitution du numero de colonne

ix = value;

%% Calcul des coordonnées (x,y) a partir de (i,j)

[x, y] = ij2xy(this.Images(this.indImage), ix, iy);

%% Nouvelle valeur des coordonnées de la croix

this.cross(this.indImage, 1) = x;
this.cross(this.indImage, 2) = y;

%% Affichage des profils et des valeurs

this = show_profils(this);

%% Traitement des synchronisations

this = process_synchroX(this);
% this = process_synchroY(this);

%% Affichage des valeurs du voisinage de la croix

private_type_voisins_xy(this);
