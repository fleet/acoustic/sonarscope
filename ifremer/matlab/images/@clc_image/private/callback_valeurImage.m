% Traitement suite a une action sur la toolbar pour le profil x
% 
% Syntax
%   this = callback_valeurImage(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_valeurImage(this)

%% R�cup�ration de la valeur �dit�e

this.ihm.ValImage.cpn = gerer_callback(this.ihm.ValImage.cpn, this.ihm.ValImage.msg);
value = get_value(this.ihm.ValImage.cpn);
if isempty(value)
    return
end

ValNaN  = get(this.Images(this.indImage), 'ValNaN');
if ~isnan(ValNaN)
    if isnan(value)
        %             valV = ValNaN;
    end
end

%% Nouvelle valeur des coordonn�es de la croix

x = this.cross(this.indImage, 1);
y = this.cross(this.indImage, 2);
this.Images(this.indImage) = set_val_xy(this.Images(this.indImage), x, y, value);

%% Affichage des profils et des valeurs

this = show_image(this);
this = show_profils(this);
