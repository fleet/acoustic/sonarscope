% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_valeurLig(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_valeurLig(this, msg)

%% Callback du composant

% this.ihm.IY.cpn = set(this.ihm.IY.cpn, 'increment',  max(1,this.NbVoisins)); % Comment� par JMA le 12/11/2018
this.ihm.IY.cpn = gerer_callback(this.ihm.IY.cpn, msg);

%% R�cuperation de l'�tat du bouton

value = get_value(this.ihm.IY.cpn);

%% Calcul des coordonn�es (i,j) de la croix

x = this.cross(this.indImage, 1);
y = this.cross(this.indImage, 2);
[ix, iy] = xy2ij(this.Images(this.indImage), x, y); %#ok

%% Substitution du num�ro de colonne

iy = value;

%% Calcul des coordonn�es (x,y) a partir de (i,j)

[x, y] = ij2xy(this.Images(this.indImage), ix, iy);

%% Nouvelle valeur des coordonn�es de la croix

this.cross(this.indImage, 1) = x;
this.cross(this.indImage, 2) = y;

%% Affichage des profils et des valeurs

this = show_profils(this);

%% Traitement des synchronisations

this = process_synchroY(this);

%% Affichage des valeurs du voisinage de la croix

private_type_voisins_xy(this);
