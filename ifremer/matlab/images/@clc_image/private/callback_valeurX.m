% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_valeurX(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_valeurX(this)

%% R�cuperation de la valeur edit�e

this.ihm.ValX.cpn = gerer_callback(this.ihm.ValX.cpn, this.ihm.ValX.msg);
value = get_value(this.ihm.ValX.cpn);

%% Nouvelle valeur des coordonn�es de la croix

this.cross(this.indImage, 1) = value;

%% Affichage des profils et des valeurs

this = show_profils(this);

%% Traitement des synchronisations

this = process_synchroX(this);
