% Traitement suite a une action sur la toolbar pour le profil x
%
% Syntax
%   this = callback_valeurY(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_valeurY(this)

%% R�cuperation de la valeur edit�e

this.ihm.ValY.cpn = gerer_callback(this.ihm.ValY.cpn, this.ihm.ValY.msg);
value = get_value(this.ihm.ValY.cpn);

%% Nouvelle valeur des coordonn�es de la croix

this.cross(this.indImage, 2) = value;

%% Affichage des profils et des valeurs

this = show_profils(this);

%% Traitement des synchronisations

this = process_synchroY(this);
