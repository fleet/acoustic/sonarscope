% Traitement suite a une action sur la toolbar pour le profil x
% 
% Syntax
%   this = callback_videoInverse(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_videoInverse(this)

%% Num�ro de la table de couleurs

ColormapIndex = this.Images(this.indImage).ColormapIndex;

%% R�cuperation de l'etat du bouton Video

lastVideo           = get_etat(this.ihm.Video.cpn);
this.ihm.Video.cpn  = gerer_callback(this.ihm.Video.cpn, this.ihm.Video.msg);
Video               = get_etat(this.ihm.Video.cpn);

if Video == lastVideo
    return
end

%% Transmission des �tats dans l'instance cl_image

this.Images(this.indImage) = set(this.Images(this.indImage), ...
    'ColormapIndex', ColormapIndex, ...
    'Video',         Video+1);
map = this.Images(this.indImage).Colormap;

ColormapIndex    = this.Images(this.indImage).ColormapIndex;
SymetrieColormap = this.Images(this.indImage).SymetrieColormap;
if Video && (SymetrieColormap(ColormapIndex, 2) == 1)
    n = floor(size(map,1) / 2);
    map = [map(n+1:end,:) ; map(1:n, :)];
end

this = set_colormap(this, map);

