% Fonctionnalite callback_zoneEtude specifique au composant
%
% Syntax
%   a = callback_zoneEtude(a)
%
% Input Arguments
%   a   : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_zoneEtude(this, varargin)

if nargin == 1
    MessageTraitementDroite
    return
end

TagSynchroX = this.Images(this.indImage).TagSynchroX;
TagSynchroY = this.Images(this.indImage).TagSynchroY;

if isnumeric(TagSynchroX)
    TagSynchroX = num2str(TagSynchroX);
end

if isnumeric(TagSynchroY)
    TagSynchroY = num2str(TagSynchroY);
end

msg = varargin{1};
WorkInProgress(['SSc is working : ' msg], 'Gap', 0);
switch msg
    case 'TagZoneEtudeChoix'
        [flag, Reponse] = selectionZoneEtude(this);
        if ~flag
            return
        end
        this.ZoneEtudeInd = Reponse;
        if this.ZoneEtudeInd == 0
            return
        end
        XLim = this.ZoneEtude(this.ZoneEtudeInd).XLim;
        YLim = this.ZoneEtude(this.ZoneEtudeInd).YLim;
        
        %         this = callback_zoomDirect(this, 'QuickLook');  % Pour contrer un pb d'affichage que je n'arrive pas a resoudre
        
        set(this.hAxePrincipal, 'XLim', XLim);
        set(this.hAxePrincipal, 'YLim', YLim);
        %             w = [XLim YLim];
        %             axis(w);
        
        
    case 'TagZoneEtudeDefinition'
        [Name, XLim, YLim, CLim, Commentaire, flag] = paramsFctZoneEtudeDefinition(this);
        if ~flag
            return
        end
        this.ZoneEtude(end+1).Name = Name;
        this.ZoneEtude(end).XLim   = XLim;
        this.ZoneEtude(end).YLim   = YLim;
        this.ZoneEtude(end).CLim   = CLim;
        
        %         'ATTENTION DANGER ICI POUR RECUPERATION ANCIENS PROJETS'
        this.ZoneEtude(end).TagSynchroX = TagSynchroX;
        this.ZoneEtude(end).TagSynchroY = TagSynchroY;
        
        this.ZoneEtude(end).indImageDefinition   = this.indImage;
        this.ZoneEtude(end).DataTypeDefinition = this.Images(this.indImage).DataType;
        this.ZoneEtude(end).Commentaire = Commentaire;
        this.ZoneEtudeInd = length(this.ZoneEtude);
        
    case 'TagZoneEtudeCrop'
        [Name, XLim, YLim, CLim, Commentaire, flag] = paramsFctZoneEtudeDefinition(this, 'Crop', 1);
        if ~flag
            return
        end
        this.ZoneEtude(end+1).Name = Name;
        this.ZoneEtude(end).XLim   = XLim;
        this.ZoneEtude(end).YLim   = YLim;
        this.ZoneEtude(end).CLim   = CLim;
        
        this.ZoneEtude(end).TagSynchroX = TagSynchroX;
        this.ZoneEtude(end).TagSynchroY = TagSynchroY;
        
        this.ZoneEtude(end).indImageDefinition   = this.indImage;
        this.ZoneEtude(end).DataTypeDefinition = this.Images(this.indImage).DataType;
        this.ZoneEtude(end).Commentaire = Commentaire;
        this.ZoneEtudeInd = length(this.ZoneEtude);
        
    case 'TagZoneEtudeCropMultiImagesInt'
        [Name, XLim, YLim, CLim, Commentaire, flag] = paramsFctZoneEtudeDefinition(this, 'Crop', 2);
        if ~flag
            return
        end
        this.ZoneEtude(end+1).Name = Name;
        this.ZoneEtude(end).XLim   = XLim;
        this.ZoneEtude(end).YLim   = YLim;
        this.ZoneEtude(end).CLim   = CLim;
        
        this.ZoneEtude(end).TagSynchroX = TagSynchroX;
        this.ZoneEtude(end).TagSynchroY = TagSynchroY;
        
        this.ZoneEtude(end).indImageDefinition   = this.indImage;
        this.ZoneEtude(end).DataTypeDefinition = this.Images(this.indImage).DataType;
        this.ZoneEtude(end).Commentaire = Commentaire;
        this.ZoneEtudeInd = length(this.ZoneEtude);
        
    case 'TagZoneEtudeCropMultiImagesExt'
        [Name, XLim, YLim, CLim, Commentaire, flag] = paramsFctZoneEtudeDefinition(this, 'Crop', 3);
        if ~flag
            return
        end
        this.ZoneEtude(end+1).Name = Name;
        this.ZoneEtude(end).XLim   = XLim;
        this.ZoneEtude(end).YLim   = YLim;
        this.ZoneEtude(end).CLim   = CLim;
        
        this.ZoneEtude(end).TagSynchroX = TagSynchroX;
        this.ZoneEtude(end).TagSynchroY = TagSynchroY;
        
        this.ZoneEtude(end).indImageDefinition   = this.indImage;
        this.ZoneEtude(end).DataTypeDefinition = this.Images(this.indImage).DataType;
        this.ZoneEtude(end).Commentaire = Commentaire;
        this.ZoneEtudeInd = length(this.ZoneEtude);
        
    case 'TagZoneEtudeDelete'
        [flag, Reponse] = selectionZoneEtude(this, 'SelectionMode', 'Multiple');
        if ~flag || isempty(Reponse) || all(Reponse == 0)
            return
        end
        this.ZoneEtudeInd = Reponse;
        this.ZoneEtude(this.ZoneEtudeInd) = [];
%         this.ZoneEtudeInd = this.ZoneEtudeInd - 1;
        this.ZoneEtudeInd = []; % Modif JMA le 23/03/2021
        return
        
    case 'TagZoneEtudeExport'
        [flag, Reponse] = selectionZoneEtude(this, 'SelectionMode', 'Multiple');
        if ~flag || isempty(Reponse)
            return
        end
        
        ZoneEtude = this.ZoneEtude(Reponse);
        
        if length(Reponse) == 1
            nomFic = fullfile(this.repExport, ['StudyArea_' ZoneEtude.Name]);
        else
            nomFic = fullfile(this.repExport, 'StudyAreas.mat');
        end
        [flag, nomFic] = my_uiputfile('*.mat', 'Give a file name', nomFic);
        if ~flag
            return
        end
        this.repExport = fileparts(nomFic);
        
        save(nomFic, 'ZoneEtude')
        return
        
    case 'TagZoneEtudeImport'
        nomFicCompens = fullfile(this.repImport, '*.mat');
        [flag, nomFic] = my_uigetfile(nomFicCompens, 'Study Area');
        if ~flag
            return
        end
        ZoneEtude = loadmat(nomFic, 'nomVar', 'ZoneEtude');
        for k=1:length(ZoneEtude)
            ZoneEtude(k).TagSynchroX = TagSynchroX;
            ZoneEtude(k).TagSynchroY = TagSynchroY;
        end
        this.ZoneEtude = [this.ZoneEtude ZoneEtude];
        return
        
    otherwise
        SSc_InvalidMessage(msg, which(mfilename))
        return
end

this = callback_zoomDirect(this, 'Imposed', 'XLim', XLim, 'YLim', YLim);
