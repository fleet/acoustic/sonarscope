% Traitement du zoom
%
% Syntax
%   a = callback_zoom(a, hAxe, bbox)
%
% Input Arguments
%   a    : instance de clc_image
%   hAxe : handle de l axe de zoom
%   bbox : bounding box du zoom
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_zoom(this, hAxe, bbox)

if isempty(hAxe)
    return
end
if ~ishandle(hAxe)
    disp('hAxe defectueux dans callback_zoom')
    whos hAxe
    return
end

if ~((hAxe == this.hAxePrincipal) || (hAxe == this.hAxeProfilX) || (hAxe == this.hAxeProfilY))
    return
end

%% Traitement

this.ihm.zoom.cpn = appliquer(this.ihm.zoom.cpn, hAxe, bbox, 'recadrage');

%% Application du zoom graphique

lstCmd = get_lstcmd(this.ihm.zoom.cpn);
for i = 1:length(lstCmd)
    try
        lst = lstCmd{i};
        set(lst{:});
    catch %#ok<CTCH>
        return
    end
end

%% On m�morise les limites du zoom dans l'instance

XLimYlim = axis(this.hAxePrincipal);
switch hAxe
    case this.hAxePrincipal
        this.visuLim(this.indImage, :) = XLimYlim;
    case this.hAxeProfilX
        XLimYlim(3:4) = this.visuLim(this.indImage, 3:4);
        this.visuLim(this.indImage, 1:2) = XLimYlim(1:2);
    case this.hAxeProfilY
        XLimYlim(1:2) = this.visuLim(this.indImage, 1:2);
        this.visuLim(this.indImage, 3:4) = XLimYlim(3:4);
    otherwise
        my_warndlg('callback_zoom : On ne devrait pas passer par ici', 1);
end
this.visuLimHistory{this.indImage}{end+1} = this.visuLim(this.indImage, :);
this.visuLimHistoryPointer(this.indImage) = length(this.visuLimHistory{this.indImage});

%% Traitement des synchronisations

XLim = XLimYlim(1:2);
YLim = XLimYlim(3:4);
this.Images(this.indImage) = set_XLim(this.Images(this.indImage), XLim);
this.Images(this.indImage) = set_YLim(this.Images(this.indImage), YLim);

this = process_synchroX(this);
this = process_synchroY(this);

%% Mise � jour de la visualisation

this = show_image(this);
this = show_profils(this);
this = traiter_typeInteractionSouris(this);

%% Traitement des synchronisations en X et Y

this = process_synchroX(this);
this = process_synchroY(this);

%% Ajustement de la dynamique des profils

dynamique_profils(this);

% Pas sur que ces 2 lignes soient utiles
%         msgCallBack = built_cb(this, this.msgKeyPress);
%         set(this.hfig, 'KeyPressFcn', msgCallBack);

% Rajout� le 05/02/2011
this = traiter_typeInteractionSouris(this);

