% Traitement de la demande de centrage
% 
% Syntax
%   this = callback_zoomCentrage(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_zoomCentrage(this)

%% Détermination si le curseur est sur l'un des 3 axes

flag = test_curseur_sur_axes(this);
if ~any(flag)
    return
end

this = traiter_click_centre(this);
