% Traitement du zoom
%
% Syntax
%   a = callback_zoomDirect(a, Action)
%
% Input Arguments
%   a      : instance de clc_image
%   Action : Identifiant de l'action :
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = callback_zoomDirect(this, Action, varargin)

flagMiseAJourHistorique = 1;

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');

% set(this.hfig, 'CurrentAxes', this.hAxePrincipal);
w = axis(this.hAxePrincipal);

XLim = w(1:2);
YLim = w(3:4);
%     XLim = this.visuLim(this.indImage, 1:2);
%     YLim = this.visuLim(this.indImage, 3:4);
pasx = abs(x(2) - x(1));
pasy = abs(y(2) - y(1));
demi_pasx = pasx / 2;
demi_pasy = pasy / 2;

%%

switch Action
    case 'MoveUp'
        if this.Images(this.indImage).YDir == 2
            Action = 'MoveDown';
        end
    case 'MoveDown'
        if this.Images(this.indImage).YDir == 2
            Action = 'MoveUp';
        end
    case 'MoveLeft'
        if this.Images(this.indImage).XDir == 2
            Action = 'MoveRight';
        end
    case 'MoveRight'
        if this.Images(this.indImage).XDir == 2
            Action = 'MoveLeft';
        end
        
    case 'MoveUpDemi'
        if this.Images(this.indImage).YDir == 2
            Action = 'MoveDownDemi';
        end
    case 'MoveDownDemi'
        if this.Images(this.indImage).YDir == 2
            Action = 'MoveUpDemi';
        end
        
    case 'MoveTop'
        if this.Images(this.indImage).YDir == 2
            Action = 'MoveBottom';
        end
    case 'MoveBottom'
        if this.Images(this.indImage).YDir == 2
            Action = 'MoveTop';
        end
        
    case 'MoveLeftDemi'
        if this.Images(this.indImage).XDir == 2
            Action = 'MoveRightDemi';
        end
    case 'MoveRightDemi'
        if this.Images(this.indImage).XDir == 2
            Action = 'MoveLeftDemi';
        end
        
    case 'MoveLeftImage'
        if this.Images(this.indImage).XDir == 2
            Action = 'MoveRightImage';
        end
    case 'MoveRightImage'
        if this.Images(this.indImage).XDir == 2
            Action = 'MoveLeftImage';
        end
end

%%

switch Action
    case 'SamePosition'
        flagMiseAJourHistorique = 0;
        
    case 'Imposed'
        [varargin, XLim] = getPropertyValue(varargin, 'XLim', XLim);
        [varargin, YLim] = getPropertyValue(varargin, 'YLim', YLim); %#ok<ASGLU>
        
    case 'QuickLook'
        XLim = compute_XYLim(x);
        YLim = compute_XYLim(y);
        
    case 'Zoom1'
        UnitsAvant = get(this.hAxePrincipal, 'Units');
        set(this.hAxePrincipal, 'units', 'pixels');
        position = get(this.hAxePrincipal, 'Position');
        set(this.hAxePrincipal, 'units', UnitsAvant);
        newDiffXLim = position(3) * pasx;
        newDiffYLim = position(4) * pasy;
        meanXLim = mean(XLim);
        D  = newDiffXLim / 2;
        XLim = [meanXLim-D meanXLim+D];
        meanYLim = mean(YLim);
        D  = newDiffYLim / 2;
        YLim = [meanYLim-D meanYLim+D];
        
    case 'ZoomN'
        N = varargin{1};
        UnitsAvant = get(this.hAxePrincipal, 'Units');
        set(this.hAxePrincipal, 'units', 'pixels');
        position = get(this.hAxePrincipal, 'Position');
        set(this.hAxePrincipal, 'units', UnitsAvant);
        newDiffXLim = position(3) * pasx;
        newDiffYLim = position(4) * pasy;
        meanXLim = mean(XLim);
        D  = newDiffXLim / N;
        XLim = [meanXLim-D meanXLim+D];
        meanYLim = mean(YLim);
        D  = newDiffYLim / N;
        YLim = [meanYLim-D meanYLim+D];
        
    case 'ZoomInX'
        L = diff(XLim) / 4;
        M = mean(XLim);
        XLim = [M-L, M+L];
        
    case 'ZoomInY'
        L = diff(YLim) / 4;
        M = mean(YLim);
        YLim = [M-L, M+L];
        
    case 'ZoomInXY'
        L = diff(XLim) / 4;
        M = mean(XLim);
        XLim = [M-L, M+L];
        L = diff(YLim) / 4;
        M = mean(YLim);
        YLim = [M-L, M+L];
        
    case 'ZoomInXY*'
        L = diff(XLim) / 4;
        L = L * 2 / 1.25;
        M = mean(XLim);
        XLim = [M-L, M+L];
        L = diff(YLim) / 4;
        L = L * 2 / 1.25;
        M = mean(YLim);
        YLim = [M-L, M+L];
        
    case 'ZoomOutX'
        L = diff(XLim) / 4;
        M = mean(XLim);
        XLim = [M-3*L, M+3*L];
        XLim(1) = max(XLim(1), min(x)-demi_pasx);
        XLim(2) = min(XLim(2), max(x)+demi_pasx);
        
    case 'ZoomOutY'
        L = diff(YLim) / 4;
        M = mean(YLim);
        YLim = [M-3*L, M+3*L];
        YLim(1) = max(YLim(1), min(y)-demi_pasy);
        YLim(2) = min(YLim(2), max(y)+demi_pasy);
        
    case 'ZoomOutXY'
        L = diff(XLim);
        M = mean(XLim);
        XLim = [M-L, M+L];
        L = diff(YLim);
        M = mean(YLim);
        YLim = [M-L, M+L];
        
    case 'ZoomOutXY/'
        L = diff(XLim);
        L = L / 1.6;
        M = mean(XLim);
        XLim = [M-L, M+L];
        L = diff(YLim);
        L = L / 1.6;
        M = mean(YLim);
        YLim = [M-L, M+L];
        
    case 'MoveUp'
        L = diff(YLim);
        YLim = YLim + L;
        if YLim(2) > (max(y)+demi_pasy)
            YLim = [max(y)+demi_pasy-L max(y)+demi_pasy];
        end
        
    case 'MoveTop'
        L = diff(YLim);
        YLim = [max(y)+demi_pasy-L max(y)+demi_pasy];
        
    case 'MoveUpDemi'
        L = diff(YLim);
        YLim = YLim + L/2;
        if YLim(2) > (max(y)+demi_pasy)
            YLim = [max(y)+demi_pasy-L max(y)+demi_pasy];
        end
        
    case 'MoveDown'
        L = diff(YLim);
        YLim = YLim - L;
        if YLim(1) < (min(y)-demi_pasy)
            YLim = [min(y)-demi_pasy min(y)-demi_pasy+L];
        end
        
    case 'MoveBottom'
        L = diff(YLim);
        YLim = [min(y)-demi_pasy min(y)-demi_pasy+L];
        
    case 'MoveDownDemi'
        L = diff(YLim);
        YLim = YLim - L/2;
        if YLim(1) < (min(y)-demi_pasy)
            YLim = [min(y)-demi_pasy min(y)-demi_pasy+L];
        end
        
    case 'MoveLeft'
        L = diff(XLim);
        XLim = XLim - L;
        if XLim(1) < (min(x)-demi_pasx)
            XLim = [min(x)-demi_pasx min(x)-demi_pasx+L];
        end
        
    case 'MoveLeftImage'
        L = diff(XLim);
        XLim = [min(x)-demi_pasx min(x)-demi_pasx+L];
        
    case 'MoveLeftDemi'
        L = diff(XLim);
        XLim = XLim - L/2;
        if XLim(1) < (min(x)-demi_pasx)
            XLim = [min(x)-demi_pasx min(x)-demi_pasx+L];
        end
        
    case 'MoveRight'
        L = diff(XLim);
        XLim = XLim + L;
        if XLim(2) > (max(x)+demi_pasx)
            XLim = [max(x)+demi_pasx-L max(x)+demi_pasx];
        end
        
    case 'MoveRightImage'
        L = diff(XLim);
        XLim = [max(x)+demi_pasx-L max(x)+demi_pasx];
        
    case 'MoveRightDemi'
        L = diff(XLim);
        XLim = XLim + L/2;
        if XLim(2) > (max(x)+demi_pasx)
            XLim = [max(x)+demi_pasx-L max(x)+demi_pasx];
        end
        
    case 'AxisEqual'
        UnitsAvant = get(this.hAxePrincipal, 'Units');
        set(this.hAxePrincipal, 'units', 'pixels');
        position = get(this.hAxePrincipal, 'Position');
        set(this.hAxePrincipal, 'units', UnitsAvant);
        rapLargeur = position(3) / diff(XLim);
        rapHauteur = position(4) / diff(YLim);
        rapport = max(rapLargeur, rapHauteur);
        newDiffXLim = position(3) / rapport;
        newDiffYLim = position(4) / rapport;
        meanXLim = mean(XLim);
        D  = newDiffXLim / 2;
        XLim = [meanXLim-D meanXLim+D];
        meanYLim = mean(YLim);
        D  = newDiffYLim / 2;
        YLim = [meanYLim-D meanYLim+D];
        
    case 'ZoomPrecedent'
        this.visuLimHistoryPointer(this.indImage) = this.visuLimHistoryPointer(this.indImage) - 1;
        this.visuLimHistoryPointer(this.indImage) = max(1, this.visuLimHistoryPointer(this.indImage));
        visuLim = this.visuLimHistory{this.indImage}{this.visuLimHistoryPointer(this.indImage)};
        XLim = visuLim(1:2);
        YLim = visuLim(3:4);
        flagMiseAJourHistorique = 0;
        
    case 'ZoomSuivant'
        N = length(this.visuLimHistory{this.indImage});
        this.visuLimHistoryPointer(this.indImage) = this.visuLimHistoryPointer(this.indImage) + 1;
        this.visuLimHistoryPointer(this.indImage) = min(N, this.visuLimHistoryPointer(this.indImage));
        visuLim = this.visuLimHistory{this.indImage}{this.visuLimHistoryPointer(this.indImage)};
        XLim = visuLim(1:2);
        YLim = visuLim(3:4);
        flagMiseAJourHistorique = 0;
        
    otherwise
        SSc_InvalidMessage(Action, which(mfilename))
        return
end

%% Transmission des etats dans l'instance cl_image

this.Images(this.indImage) = set_XLim(this.Images(this.indImage), XLim);
this.Images(this.indImage) = set_YLim(this.Images(this.indImage), YLim);
this.visuLim(this.indImage, 1:2) = XLim;
this.visuLim(this.indImage, 3:4) = YLim;
if flagMiseAJourHistorique
    this.visuLimHistory{this.indImage}{end+1} = this.visuLim(this.indImage, :);
    this.visuLimHistoryPointer(this.indImage) = length(this.visuLimHistory{this.indImage});
end

%% Mise � jour de la visualisation

this = show_image(this, 'MajColorbar', 0);
this = show_profils(this);

% If faut remettre le zoom en �tat de marche ici
this = traiter_typeInteractionSouris(this);

%% Traitement des synchronisation en X et Y

this = process_synchroX(this);
this = process_synchroY(this);

if strcmp(Action, 'AxisEqual')
    axis(this.hAxePrincipal, 'equal')
end
