function this = changeStatusContrasteAuto(this, type)

global IfrTbxFlagContrasteAuto %#ok<GVMIS>

switch type
    case 0 % Pas de contraste automatique
        IfrTbxFlagContrasteAuto = 0;
        set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'Off');
        set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'Off');
    case 1 % [Min Max] Auto
        if IfrTbxFlagContrasteAuto == 1
            IfrTbxFlagContrasteAuto = 0;
            set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'Off');
            set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'Off');
        else
            IfrTbxFlagContrasteAuto = 1;
            set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'On');
            set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'Off');
        end
    case 2 % 0.5% Auto
        if IfrTbxFlagContrasteAuto == 2
            IfrTbxFlagContrasteAuto = 0;
            set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'Off');
            set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'Off');
        else
            IfrTbxFlagContrasteAuto = 2;
            set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'Off');
            set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'On');
        end
end
