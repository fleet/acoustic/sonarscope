% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also clc_image
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

if isempty(this)
    return
end

%% Traitement

str{end+1} = sprintf('visuGrid              <-> %s', BooleenDansListe2str(this.visuGrid));
str{end+1} = sprintf('visuCoupeHorz         <-> %s', BooleenDansListe2str(this.visuCoupeHorz));
str{end+1} = sprintf('visuCoupeVert         <-> %s', BooleenDansListe2str(this.visuCoupeVert));
str{end+1} = sprintf('visuColorbar          <-> %s', BooleenDansListe2str(this.visuColorbar));
str{end+1} = sprintf('identVisu             <-> %s', num2strCode(this.identVisu));
str{end+1} = sprintf('TypeInteractionSouris <-> %s', this.TypeInteractionSouris);
str{end+1} = sprintf('indImage              <-> %d', this.indImage);
str{end+1} = sprintf('visuLim               <-> %s', num2strCode(this.visuLim));
str{end+1} = sprintf('repImport             <-> %s', this.repImport);
str{end+1} = sprintf('repExport             <-> %s', this.repExport);

str{end+1} = sprintf('--- aggregation Images (cl_image) ---');
str{end+1} = char(this.Images);

str{end+1} = sprintf('--- heritage cl_component ---');
if isfield(this, 'cl_component')
    str{end+1} = char(this.cl_component);
end

%% Concatenation en une chaine

str = cell2str(str);
