% Construction de base de l IHM
% 
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% R�cup�ration du numero de la figure

this.hfig = gcf;
this.internalNumber = num2str(rand(1));

%% Cr�ation de la frame du composant

this = creer_frame_globale(this);

%% Positionnement des differents �l�ments graphiques

this = positionner(this);

%% Determination de la table de couleur

ColormapIndex = this.Images(this.indImage).ColormapIndex;
if ColormapIndex == 1
    this.colormap = this.Images(this.indImage).ColormapCustom;
else
    this.colormap = this.Images(this.indImage).Colormap;
end
this.colormap = double(this.colormap);
colormap(this.colormap);
indiquerColorTableUtilisee(this, ColormapIndex)

%% Mise en place de la callback sur le d�placement du curseur et sur le click

this = placer_callback_curseur(this);
this = placer_callback_click(this);
this = placer_callback_molette(this);

%% Pr�paration du zoom

this = placer_zoom(this);

%% Mise en place des callbacks correspondant � l'action

this = traiter_typeInteractionSouris(this);

%% Affichage des profils

this = show_profils(this);

%% Gestion des menus

this = gerer_menus(this);

%% Mise � jour de la visu (VideoInverse n'est pas g�r� si on ne fait pas cet appel

this = editer_image_selectionnee(this);

%% Mise � jour de la visibilite

if strcmp(get(this, 'componentVisible'), 'off')
    this.globalFrame = set(this.globalFrame, 'Visible', 'off');
end
