function this = conversionCoordonneesGeographiques(this, sens)

Carto = get_Carto(this.Images(this.indImage));
if isempty(Carto)
    Carto = get_Carto(cl_image);
end

GT = this.Images(this.indImage).GeometryType;

switch GT
    case cl_image.indGeometryType('LatLong')
        lon = this.cross(this.indImage,1);
        lat = this.cross(this.indImage,2);
        [x, y] = latlon2xy(Carto, lat, lon);
    case cl_image.indGeometryType('GeoYX')
        x = this.cross(this.indImage,1);
        y = this.cross(this.indImage,2);
        [lat, lon] = xy2latlon(Carto, x, y);
    otherwise
        x = 0;
        y = 0;
        lat = 0;
        lon = 0;
end

switch sens
    case 'xy2latlon'
        nomVar = {'X' ; 'Y'};
        value = {num2strPrecis(x) ; num2strPrecis(y)};
        [V, flag] = my_inputdlg(nomVar, value);
        if ~flag
            return
        end
        x = str2double(V(1));
        y = str2double(V(2));
        [lat, lon] = xy2latlon(Carto, x, y);

    case 'latlon2xy'
        nomVar = {'Latitude (Ex : -4.35 or S4 55.424)' ; 'Longitude (Ex : -6.54321 or W6 22.3344)'};
        value = {num2strPrecis(lat) ; num2strPrecis(lon)};
        [V, flag] = my_inputdlg(nomVar, value);
        if ~flag
            return
        end
        lat = str2double(V(1));
        if isnan(lat)
            lat = str2lat(V(1));
        end
        lon = str2double(V(2));
        if isnan(lon)
            lon = str2lon(V(2));
        end
        [x, y, flag] = latlon2xy(Carto, lat, lon);
        if ~flag
            return
        end
end

switch sens
    case 'xy2latlon'
        clear str
        str{1} = ' ';
        str{end+1} = Lang('Coordonn�es dans la projection (m)', 'Coordinates in the projection (m)');
        str{end+1} = sprintf('X : %s', num2strPrecis(x));
        str{end+1} = sprintf('Y : %s', num2strPrecis(y));
        str{end+1} = ' ';
        str{end+1} = Lang('Coordonn�es g�ographiques (degr�s d�cimaux)', 'Geographic coordinates (decimal degreess)');
        str{end+1} = sprintf('Lat : %s', num2strPrecis(lat));
        str{end+1} = sprintf('Lon : %s', num2strPrecis(lon));
        str{end+1} = ' ';
        str{end+1} = Lang('Coordonn�es g�ographiques (degr�s et minutes d�cimales)', 'Geographic coordinates (degrees & decimal minutes)');
        str{end+1} = sprintf('Lat : %s', lat2str6(lat));
        str{end+1} = sprintf('Lon : %s', lon2str6(lon));
        str{end+1} = ' ';
        str{end+1} = Lang('Coordonn�es g�ographiques (degr�s, minutes et secondes d�cimales)', 'Geographic coordinates (degrees, minutes, decimal seconds)');
        str{end+1} = sprintf('Lat : %s', lat2strdms(lat));
        str{end+1} = sprintf('Lon : %s', lon2strdms(lon));
    case 'latlon2xy'
        clear str
        str{1} = ' ';
        str{end+1} = Lang('Coordonn�es g�ographiques (deg)', 'Geographic coordinates (deg)');
        str{end+1} = sprintf('Lat : %s', num2strPrecis(lat));
        str{end+1} = sprintf('Lon : %s', num2strPrecis(lon));
        str{end+1} = ' ';
        str{end+1} = Lang('Coordonn�es g�ographiques (degr�s et minutes d�cimales)', 'Geographic coordinates (degrees & decimal minutes)');
        str{end+1} = sprintf('Lat : %s', lat2str6(lat));
        str{end+1} = sprintf('Lon : %s', lon2str6(lon));
        str{end+1} = ' ';
        str{end+1} = Lang('Coordonn�es g�ographiques (degr�s, minutes et secondes d�cimales)', 'Geographic coordinates (degrees, minutes, decimal seconds)');
        str{end+1} = sprintf('Lat : %s', lat2strdms(lat));
        str{end+1} = sprintf('Lon : %s', lon2strdms(lon));
        str{end+1} = ' ';
        str{end+1} = Lang('Coordonn�es dans la projection (m)', 'Coordinates in the projection (m)');
        str{end+1} = sprintf('X : %s', num2strPrecis(x));
        str{end+1} = sprintf('Y : %s', num2strPrecis(y));
end

for k=1:length(str)
    fprintf('%s\n', str{k});
end

switch GT
    case cl_image.indGeometryType('LatLong')
        this.cross(this.indImage,:) = [lon lat];
    case cl_image.indGeometryType('GeoYX')
        this.cross(this.indImage,:) = [x y];
end

%% Traitement des synchronisation en X et Y

this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
this = callback_profileHorz(this);
this = callback_profileVert(this, 'NoSelectionSignaVert', 1, 'DirectWay');
this = show_profils(this);
% this = process_synchroX(this);
% this = process_synchroY(this);

%% Traitement des synchronisation en X et Y

this = process_synchroX(this);
this = process_synchroY(this);

%% Cr�ation de l'�diteur de propri�t�s

com.mathworks.mwswing.MJUtilities.initJIDE;
Parent = java.util.ArrayList();
Fig = true;
p1 = com.jidesoft.grid.DefaultProperty();
p1.setName(Lang('Transformation de coordonn�es', 'Coordinates transformation'));
p1.setEditable(false);
Parent.add(p1);

%% Latitude degr�s d�cimaux
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Lat');
p2.setType(javaclass('char',1));
p2.setValue(num2strPrecis(lat));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('Lat (decimal degrees)');
p2.setDescription('Latitude.');
p2.setEditable(false);
p1.addChild(p2);

%% Longitude degr�s d�cimaux
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Lat');
p2.setType(javaclass('char',1));
p2.setValue(num2strPrecis(lon));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('Lon (decimal degrees)');
p2.setDescription('Longitude.');
p2.setEditable(false);
p1.addChild(p2);

%% Latitude degr�s et minutes d�cimales
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Lat');
p2.setType(javaclass('char',1));
p2.setValue(lat2str6(lat));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('Lat (degrees & decimal minutes)');
p2.setDescription('Latitude.');
p2.setEditable(false);
p1.addChild(p2);

%% Longitude degr�s et minutes d�cimales
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Lat');
p2.setType(javaclass('char',1));
p2.setValue(lon2str6(lon));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('Lon (degrees & decimal minutes)');
p2.setDescription('Longitude.');
p2.setEditable(false);
p1.addChild(p2);

%% Latitude degr�s, minutes et secondes d�cimales
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Lat');
p2.setType(javaclass('char',1));
p2.setValue(lat2strdms(lat));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('Lat(degrees, minutes & decimal seconds)');
p2.setDescription('Latitude.');
p2.setEditable(false);
p1.addChild(p2);

%% Longitude degr�s, minutes et secondes d�cimales
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Lat');
p2.setType(javaclass('char',1));
p2.setValue(lon2strdms(lon));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('Lon (degrees, minutes & decimal seconds)');
p2.setDescription('Longitude.');
p2.setEditable(false);
p1.addChild(p2);

%% X
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('X');
p2.setType(javaclass('char',1));
p2.setValue(num2strPrecis(x));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('X (m)');
p2.setDescription('Abscissa.');
p2.setEditable(false);
p1.addChild(p2);

%% Y
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Y');
p2.setType(javaclass('char',1));
p2.setValue(num2strPrecis(y));
p2.setCategory('Coordinates transformation');
p2.setDisplayName('Y (m)');
p2.setDescription('Ordinate.');
p2.setEditable(false);
p1.addChild(p2);

%% Carto
p2 = com.jidesoft.grid.DefaultProperty();
p2.setName('Carto');
p2.setEditable(false);
p2.setExpanded(false);
p2 = editProperties(Carto, 'Parent', p2);
p1.addChild(p2);
p2.setExpanded(false);

%% Figure
if Fig
    %% Prepare a properties table containing the Parent
    
    model = com.jidesoft.grid.PropertyTableModel(Parent);
    model.expandAll();
    grid = com.jidesoft.grid.PropertyTable(model);
    hPanelJava = com.jidesoft.grid.PropertyPane(grid);
    
    %% Display the properties hPanelJava on screen
    
    hFig = figure;
    Position = get(hFig, 'Position');
    hPanelMatlab = uipanel(hFig);
    javacomponent(hPanelJava, [5 5 Position(3:4)*0.98], hPanelMatlab);
    
    % Set the resize callback function of uihPanelMatlab container
    set(hPanelMatlab, 'ResizeFcn', {@UihPanelMatlabResizeCallback, hPanelMatlab, hPanelJava});   
end

%% Callback function for resize behavior of the uihPanelMatlab container
    function UihPanelMatlabResizeCallback(hObject, eventdata, handles, hMainPanel, varargin) %#ok<INUSD,INUSL>
        h = get(handles, 'Children');
        hFig = get(handles, 'Parent');
        Position = get(hFig, 'Position');
        set(h, 'Position', [5 5 Position(3:4)*0.98]);
    end
end
