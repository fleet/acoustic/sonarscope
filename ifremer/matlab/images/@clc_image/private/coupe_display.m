function this = coupe_display(this, Coupe)

for k=1:length(Coupe)
    hold on;
    this.coupe.handles(k) = plot(Coupe(k).XData, Coupe(k).YData);
    set(this.coupe.handles(k), 'Color', Coupe(k).Color)
    set(this.coupe.handles(k), 'LineWidth', Coupe(k).LineWidth)
    set(this.coupe.handles(k), 'Tag', 'ProfilOnTop')
    hold off
end
