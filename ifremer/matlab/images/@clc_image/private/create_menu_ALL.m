function cmenu = create_menu_ALL(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'ALL';

parent1 = 'ALL_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers Kongsberg ".all" ', '".all" files (Kongsberg)'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'Kongsberg_files.html');

%% Survey report

cmenu = addMenu_L1_SurveyReport(cmenu, parent1, msg);

%% Summary

cmenu = addMenu_L1_Summary(cmenu, parent1, msg);

%% Navigation

cmenu = addMenu_L1_Navigation(cmenu, parent1, msg);

%% Geometric Transformations

cmenu = addMenu_L1_GeometricTransformations(cmenu, parent1, msg);
   
%% Data cleaning

cmenu = addMenu_L1_DataCleaning(cmenu, parent1, msg);

%% Tide

cmenu = addMenu_L1_Tide(cmenu, parent1, msg);

%% AUV

cmenu = addMenu_L1_AUV(cmenu, parent1, msg);

%% Water-Column

cmenu = addMenu_L1_WaterColumn(cmenu, parent1, msg);

%% Special mosaics

cmenu = addMenu_L1_SpecialMosaics(cmenu, parent1, msg);

%% Statistics

cmenu = addMenu_L1_Statistics(cmenu, parent1, msg);

%% Calibration

% cmenu = addMenu_L1_Calibration(cmenu, parent1, msg);

%% Patch test

cmenu = addMenu_L1_PatchTest(cmenu, parent1, msg);

%% BS

cmenu = addMenu_L1_BS(cmenu, parent1, msg);

%% Absorption coefficient

% cmenu = addMenu_L1_AbsorptionCoefficient(cmenu, parent1, msg);

%% Incidence angle

% cmenu = addMenu_L1_IncidenceAngle(cmenu, parent1, msg);

%% Segmentation

cmenu = addMenu_L1_Segmentation(cmenu, parent1, msg);

%% Utilities

cmenu = addMenu_L1_Utilities(cmenu, parent1, msg);

%% R&D

cmenu = addMenu_L1_RetD(cmenu, parent1, msg);


function cmenu = addMenu_L1_Summary(cmenu, parent1, msg)

parent2 = 'ALL_MainMenuSummary';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('R�sum�', 'Summary'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SummaryLines_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Profils', 'Lines'));

msg{3} = 'ALL_SummaryDatagrams_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Datagrammes', 'Datagrams'));


function cmenu = addMenu_L1_Navigation(cmenu, parent1, msg)

parent2 = 'ALL_Nav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_PlotNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'ALL_PlotNavigationAndCoverage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation et de la couverture sondeur', 'Plot Navigation & Coverage'));

msg{3} = 'ALL_PlotNavigationAndSignal_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

%{
msg{3} = 'ALL_PlotSoundSpeedProfileOnNavigation';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Localisation des nouveaux profils de c�l�rit� sur la navigation', 'Location of sound speed profiles'));

msg{3} = 'ALL_PlotRuntimeParametersOnNavigation';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Localisation des modifications des runtime parameters sur la navigation', 'Location of runtime parameters'));
%}

msg{3} = 'ALL_ImportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

msg{3} = 'ALL_NavExport_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export de la navigation et de l''attitude pour GLOBE', 'Export Navigation and Attitude to GLOBE'));

msg{3} = 'ALL_NavGPSAccuracy_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la pr�cision de la mesure GPS', 'Estimation of the GPS accuracy'));

msg{3} = 'ALL_EditNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition de la navigation', 'Edit navigation'));


function cmenu = addMenu_L1_GeometricTransformations(cmenu, parent1, msg)

parent2 = 'ALL_TransGeometry';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Transformations g�om�triques', 'Geometric Transformations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'SonarAllComputegeometry12Geometry2', ...
    'label', 'Geometry1 -> Geometry2', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_PingBeam2LatLong_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'PingBeam  --> LatLong');

% msg{3} = 'ALL_PingSamples2LatLong';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
%     'label', 'PingSamples --> LatLong (Mosaic "Imagery Datagram")');

msg{3} = 'ALL_PingAcrossSample2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'PingAcrossSample --> LatLong');

% TODO : supprimer toutes les fonctions de PingSamples Simrad
% msg{3} = 'ALL_PingSamples2PingAcrossDist_CI0';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
%     'label', 'PingSamples --> PingAcrossDist');

if ~isdeployed
    % TODO : � faire
    msg{3} = 'ALL_PingAcrossSample2PingAcrossDist';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', 'PingAcrossSample--> PingAcrossDist');
end

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'SonarAllComputeLatLongLayersInCache', ...
    'label', 'On the move layers', ...
    'Separator', 'on', 'ForegroundColor', 'b');
%     'label', 'On the fly layers', 'Separator', 'on', 'ForegroundColor', 'b');
%     'label', 'Create layers in the cache', 'Separator', 'on', 'ForegroundColor', 'b');
%     'label', 'Intermediate layers', 'Separator', 'on', 'ForegroundColor', 'b');
%     'label', ' layers calcul�s � la vol�e', 'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_CacheComputeLatLongLayers_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'Create geographic coordinates in the cache');

msg{3} = 'ALL_CacheDeleteLatLongLayers_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'Disable geographic coordinates in the cache');



function cmenu = addMenu_L1_DataCleaning(cmenu, parent1, msg)

parent2 = 'ALL_DataCleaning';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Nettoyage de la donn�e', 'Data Cleaning'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

str1 = 'Nettoyage � partir de l''analyse histogrammique de "Range, Depth"';
str2 = 'On Depth & Range histograms';
msg{3} = 'ALL_CleanBathyAndRange_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, 'label', ...
    Lang(str1,str2));

msg{3} = 'ALL_CleanBathy_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, 'label', ...
    Lang('Sur la bathym�trie', 'On Bathymetry'));
cmenu = addMenu_L2_DataCleaning_FromDTM(cmenu, parent2, msg);

msg{3} = 'ALL_sonar_Simrad_CleanLayerUsingSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'On any layer using any signals'));

% Flags sur la r�flectivit�
cmenu = addMenu_L2_ALL_CleanReflectivity(cmenu, parent2, msg);

% Gestion des masks
cmenu = addMenu_L2_DataCleaning_GestionMasks(cmenu, parent2, msg);

% Import flags from other softwares
cmenu = addMenu_L2_DataCleaning_ImportFlags(cmenu, parent2, msg);

% Filtre spyke
cmenu = addMenu_L2_DataCleaning_FiltresSpyke(cmenu, parent2, msg);

% Nettoyage de Transducer depth
msg{3} = 'ALL_FilterTransducerDepth_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Filtrage de "Transducer depth" (AUV)', 'Filter Transducer depth (AUV)'));

% Pour Ridha
msg{3} = 'ALL_RidhaWaveFilter_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R&&D filtre ondulatrions Ridha', 'R&&D Ridha wave filter'));



function cmenu = addMenu_L2_DataCleaning_ImportFlags(cmenu, parent2, msg)

parent3 = 'ALL_ImportBathyFlags';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Importer des flags depuis un autre logiciel', 'Import flags from other softwares'));

baseMsg = msg{1};
cmenu = creer_menu_help(cmenu, parent3, baseMsg, 'SScDoc-Tutorial-ALL-DataCleaning.html', 'Tag', 'All');

msg{3} = 'ALL_ImportBathyFromCaris_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('CARIS', 'CARIS'));

msg{3} = 'ALL_ImportBathyFromQuimera_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Qimera', 'Qimera'));

msg{3} = 'ALL_ImportFlagsFromCaraibes_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('MBG de Caraibes', 'Caraibes MBG'));

msg{3} = 'ALL_ImportBathyFromMBSystem_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('MB-System', 'MB-System'));

msg{3} = 'ALL_ImportBathyFromXSF_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Ifremer XSF', 'Ifremer XSF'));


function cmenu = addMenu_L2_DataCleaning_FiltresSpyke(cmenu, parent2, msg)

parent3 = 'ALL_SpikeFilters';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Filtrage des spikes', 'spike filters'));

msg{3} = 'ALL_Spike2D_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Filtrage de spikes en 2D', '2D Spike filtering'));

msg{3} = 'ALL_Spike1D_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Filtrage de spikes en 1D', '1D Spike filtering'));


function cmenu = addMenu_L2_DataCleaning_GestionMasks(cmenu, parent2, msg)

parent3 = 'ALL_Mask';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Gestion des mask', 'Manage Masks'));

msg{3} = 'ALL_ResetMask_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RAZ des masques', 'Reset masks'));

msg{3} = 'ALL_ResetMaskPingsReflec_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RAZ des masques par pings de la r�flectivit�', 'Reset "ping masks" on reflectivity'));

msg{3} = 'ALL_BackupMask_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Sauvegarde des masques', 'Backup masks'));

msg{3} = 'ALL_RestoreMask_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�tablir les masques', 'Restore masks'));


function cmenu = addMenu_L2_DataCleaning_FromDTM(cmenu, parent2, msg)

parent3 = 'ALL_CleanFromDTM';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Par comparaison � un MNT existant', 'By comparison to a DTM'));

msg{3} = 'ALL_CleanFromDTM_Interactif_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Interactif', 'Manual'));

msg{3} = 'ALL_CleanFromDTM_Threshold_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Par seuillage', 'Using a threshold'));


function cmenu = addMenu_L2_ALL_CleanReflectivity(cmenu, parent2, msg)

parent3 = 'ALL_CleanReflectivity';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Sur la r�flectivit�', 'On Reflectivity'));

msg{3} = 'ALL_CleanReflectivity_MeanPerPing_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Using reflectivity per ping'));

msg{3} = 'ALL_CleanReflectivity_Histogram_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Using SSc'));

msg{3} = 'ALL_CleanReflectivity_HistogramMultifiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Using SSc Multifiles at once'));


function cmenu = addMenu_L1_Tide(cmenu, parent1, msg)

parent2 = 'ALL_Tide';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Mar�e', 'Tide'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_TideImport_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la mar�e', 'Import Tide'));

msg{3} = 'ALL_PlotTide_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation de la mar�e', 'Plot Tide'));

msg{3} = 'ALL_DraughtImport_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import du tirant d''eau (en travaux)', 'Import Draught (under work)'));

msg{3} = 'ALL_TrueHeaveImport_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import du "True Heave" (en travaux)', 'Import True Heave (under work)'));

cmenu = addMenu_L2_Tide_RTK(cmenu, parent2, msg);


function cmenu = addMenu_L1_AUV(cmenu, parent1, msg)

parent2 = 'ALL_AUV';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'AUV', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_AUVImportImmersion_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de l''imersion', 'Import Immersion'));

msg{3} = 'ALL_AUVImportOffsetImmersion_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import d''un offset en imersion', 'Import offest Immersion'));

msg{3} = 'ALL_AUVCleanBathyPerPing_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Mask data using the heading'));


function cmenu = addMenu_L2_Tide_RTK(cmenu, parent2, msg)

parent3 = 'ALL_RTK';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('RTK', 'RTK'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_RTK_Clean_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Contr�le de la hauteur RTK', 'Check RTK Height'));

msg{3} = 'ALL_RTK_ComputeTide_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la mar�e', 'Compute Tide'));

msg{3} = 'ALL_RTK_ExportTide_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export de la mar�e', 'Export Tide'));

msg{3} = 'ALL_RTK_Plot_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation des donn�es', 'Plot data'), ...
    'Separator', 'on');

msg{3} = 'ALL_RTK_Raz_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RAZ des donn�es', 'Reset RTK data and tide'));

msg{3} = 'ALL_RTK_plotGeoid_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� des donn�es Geoid', 'Plot Geoid'));


function cmenu = addMenu_L1_Utilities(cmenu, parent1, msg)

parent2 = 'ALL_Utilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% For all users

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'ALLUtilitiesForAllUsers', ...
        'label', 'For all users', ...
        'Separator', 'on', 'ForegroundColor', 'b');
 
cmenu = addMenu_L2_Utilities_CacheFilesALL(cmenu, parent2, msg);

msg{3} = 'ALL_Visu_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation des datagrammes autres que "depth" et "Seabed Image"', 'Plot Ship, Motion & MBES signals'));

cmenu = addMenu_L2_Utilities_SignalsExternal(cmenu, parent2, msg);
cmenu = addMenu_L2_Utilities_FileMarkers(cmenu, parent2, msg);
cmenu = addMenu_L2_Utilities_Datagrams(cmenu, parent2, msg);

msg{3} = 'ALL_ZipDir';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zip des r�pertoires', 'Zip directories'));
 
msg{3} = 'ALL_MoveFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�placement de fichiers .all dans un autre r�pertoire', 'Move some .all files in another directory'));
 
msg{3} = 'ALL_ExportPLY_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Export data into point cloud file format (.ply)'));
 
% --------------------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'ALLRobots', ...
        'label', 'Robots for running surveys', ...
        'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_Robot_CopyAndProcessSounderOutputFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pr�traitement des fichiers en temps quasi r�el', 'Preprocess files in quasi real time'));

msg{3} = 'ALL_Robot_MoveFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�placement des fichiers pr�trait�s', 'Move preprocessed files and directories'));
 
msg{3} = 'ALL_Robot_PlotKMZ_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Revisualisation des donn�es dans Google-Earth', 'Replay data in Google Earth'));

% --------------------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'ALLUtilitiesForParticularUsers', ...
        'label', 'For particular users', ...
        'Separator', 'on', 'ForegroundColor', 'b');

cmenu = addMenu_L3_Utilities_SPFE(cmenu, parent2, msg);

cmenu = addMenu_L3_Utilities_GeoscienceAzur(cmenu, parent2, msg);


function cmenu = addMenu_L1_WaterColumn(cmenu, parent1, msg)

parent2 = 'ALL_WaterColumn';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Colonne d''eau', 'Water Column'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

baseMsg = msg{1};
cmenu = creer_menu_help(cmenu, parent2, baseMsg, 'SScDoc-Tutorial-ALL-WaterColumn.html');

msg{3} = 'ALL_WCProcessEchograms_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'WC Echogrammes polaires en g�ometrie {Depth,AcrossDist}', ...
    'WC polar echograms in {Depth,AcrossDist} geometry'));

% Echo Integration in Water Column
cmenu = addMenu_L2_WaterColumn_EchoIntegration(cmenu, parent2, msg);

% msg{3} = 'ALL_WC3DMatrix_CI2'; % Curent Image 2 On pose la question plus tard si c'est le fichier s�lectionn� uniquement
msg{3} = 'ALL_WC3DMatrix_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�er de matrices 3D (Depth, AcrossDist, Ping#)', 'Create 3D Matrix (Depth, AcrossDist, Ping#)'));

% WC Appendices
cmenu = addMenu_L2_WaterColumn_Appendices(cmenu, parent2, msg);

msg{3} = 'ALL_WaterColumnConvertSsc2Hac_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Conversion au format HAC', 'Convert into HAC format'));

% Falkor
if ~isdeployed % Sp�cial Falkor (demande Henri)
    msg{3} = 'ALL_TrackBall_CI1';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('Poursuite de la bille (Falkor)', 'Track ball (Falkor)'));
end


function cmenu = addMenu_L2_WaterColumn_EchoIntegration(cmenu, parent2, msg)

parent3 = 'TagEchoIntegration';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Echo-int�gration', 'Echo Integration'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_WaterColumnEISampleBeam', ...
    'label', Lang('TODO', 'SampleBeam echograms ->  EI signal vs ping'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_WC_EI_SampleBeam_SignalVsPings_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Compute EI value per ping'));

msg{3} = 'ALL_WC_EI_SampleBeam_SignalSlicesVsPings_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Compute EI slice values per ping'));

msg{3} = 'ALL_WC_RazFlagPings_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Reset ping flags'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_WaterColumnEIPingBeam2PingAcrossDist', ...
    'label', Lang('TODO', 'DepthAcrossDist echograms -> PingAcrossDist'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_WC_EchoIntegration_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'Calcul EI au dessus du sp�culaire (ASC)', ...
    'Compute EI Above the Specular echo Circle (ASC)'));

msg{3} = 'ALL_WC_EchoIntegrationOverSeabed_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'Calcul EI au dessus du fond (ASF)', ...
    'Compute EI Above the SeaFloor (ASF)'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_WaterColumnEIPingPingAcrossDist2LatLong', ...
    'label', Lang('TODO', 'PingAcrossDist --> LatLong'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_WC_Mosaic_EI_SSc_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation d''une mosaique', 'Create a mosaic'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_WaterColumnEIPingAcrossDist', ...
    'label', Lang('TODO', 'PingAcrossDist ->  EI signal vs ping'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_WC_EI_PingAcrossDist_SignalVsPings_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Compute EI value per ping'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_WC_PlotNav_EI_SScPlus', ...
    'label', Lang('Outils secondaires', 'Secondary tools'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_WC_EI_MosFromMovies_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation de mosa�ques d''�cho-int�gration par faisceauxde Movies3D', 'Create a mosaic from Movies3D echo integration per beam'));


function cmenu = addMenu_L2_WaterColumn_Appendices(cmenu, parent2, msg)

parent3 = 'ALL_WaterColumnAppendices';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Annexes', 'Appendices'));

msg{3} = 'ALL_WC_ExportRawData23DV_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'WC Echogrammes polaires en g�ometrie {Sample,Beam}', ...
    'WC polar echograms in {Sample,Beam} geometry'));

msg{3} = 'ALL_WC_CreatePingBeamEI_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'Echo int�gration sp�ciale pour les coul�es de lave', ...
    'Special Echo Integration for lava flows'));

msg{3} = 'ALL_WC_ExportASCIIDepthAcrossDist_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export des donn�es au dessus du cercle en ASCII', 'Export WC-DepthAcrossDist over Specular circle in ASCII'));

% msg{3} = 'ALL_WaterColumnStatisticsSPFE';
% cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Calcul de statistiques pour le SPFE', 'Compute statistics for SPFE'));


function cmenu = addMenu_L1_SpecialMosaics(cmenu, parent1, msg)

parent2 = 'Simrad_MosAngularSectors';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Mosaiques sp�ciales', 'Special mosaics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_Mosaiques_Individuelles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosaiques individuelles par profils', 'Individual Mosaics per lines'));

msg{3} = 'ALL_Mosaiques_MergeIndividuelles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Assemblage des Mosaiques', 'Mosaics merging'));

msg{3} = 'ALL_Mosaiques_ParAngles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Assemblage des Mosaiques par secteurs angulaires', 'Mosaics merging per angular sectors'));


function cmenu = addMenu_L1_Statistics(cmenu, parent1, msg)

parent2 = 'ALL_Statistics';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'Statistics', ...
    'Separator', 'on', 'ForegroundColor', 'b');

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'ALL_Statistics_Default', ...
    'label', Lang('Classiques', 'Classics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_Statistics_Curves_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Courbes', 'Curves'));

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'ALL_Statistics_Histo2D_Header', ...
    'label', Lang('Histo2D', 'Histo2D'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_Statistics_Histo2D_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Histogrammes 2D sur R�flectivity & RxBeamAngles', '2D histograms on Reflectivity / RxBeamAngle'));

msg{3} = 'ALL_Statistics_Histo2D_ExtractCurves_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Extraction des courbes', 'Extract curves'));

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'ALL_Statistics_Specials', ...
    'label', Lang('Sp�ciales', 'Specials'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_Statistics_BathyMNT_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Bathy vs Reference DTM'));


function cmenu = addMenu_L2_Calibration(cmenu, parent1, msg)

parent2 = 'ALL_BSCalibration';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'BS Calibration', ...
    'Separator', 'on', 'ForegroundColor', 'b');

baseMsg = msg{1};
cmenu = creer_menu_help(cmenu, parent2, baseMsg, 'SScDoc-Tutorial-ALL-EM710BscorrSHOM.html', 'Tag', 'All');

cmenu = addMenu_L2_Statistics_Calibration(cmenu, parent2, msg);
cmenu = addMenu_L2_Statistics_TxBeamsDiagrams(cmenu, parent2, msg);


function cmenu = addMenu_L2_Statistics_TxBeamsDiagrams(cmenu, parent2, msg)

% TxBeams diagrams

parent3 = 'ALL_Statistics_Calibration_MBESStep2';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Etape 2 : Estimation de DiagTx', 'Step 2 : DiagTx Estimation'));

parent4 = 'ALL_Statistics_Calibration_MBESStep2a';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Etape 2 a) : Calcul', 'Step 2a) : Compute'));

msg{3} = 'ALL_Statistics_Calibration_DiagTxComputeAndModel_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 2 a) : Calcul et Mod�lisation', 'BSCorr technology (new sounders)'));

msg{3} = 'ALL_Statistics_Calibration_DiagTxComputeAndModel_EM3002_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 2 a) : Calcul pour EM3002', 'Compensation curves (old sounders)'));

msg{3} = 'ALL_Statistics_Calibration_DiagTxExport2_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 2b) : Exportation', 'Step 2 b) : Export'));

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_Statistics_Calibration_DiagTx_Utilities', ...
    'label', Lang('Autres', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_Statistics_Calibration_DiagTxRedoModel_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Refaire la mod�lisation si besoin', 'Redo Model'));


function cmenu = addMenu_L2_Statistics_Calibration(cmenu, parent2, msg)

% parent3 = 'ALL_Statistics_Calibration';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
%     'label', 'Calibration', 'Separator', 'on', 'ForegroundColor', 'b');

% Backscatter

parent3 = 'ALL_Statistics_Calibration_MBESStep1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Etape 1 : Estimation de la r�flectivit� angulaire', 'Step 1 : BS estimation'));

msg{3} = 'ALL_Statistics_Calibration_BSComputeAndModel_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 1 a) : Calcul et Mod�lisation', 'Step 1 a) : Compute & Model'));

% msg{3} = 'ALL_Statistics_Calibration_BSComputeAndModelNEW';
% cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Etape 1 a) : Calcul et Mod�lisation (new version) - under work)', 'Step 1 a) : Compute & Model (new version) - under work)'));

msg{3} = 'ALL_Statistics_Calibration_BSExport2_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 1 b) : Exportation BS', 'Step 1 b) : Export'));

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_Statistics_Calibration_BS_Utilities', ...
    'label', Lang('Autres', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_Statistics_Calibration_BSRedoModel_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Refaire la mod�lisation', 'Redo Model'));


function cmenu = addMenu_L1_PatchTest(cmenu, parent1, msg)

parent2 = 'ALL_PatchTest';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Patch test', 'Patch test'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_PatchTestRoll_CI0';
cmenu = ajouter(cmenu, 'parent', parent2,  'tag', msg{3}, 'message', msg, ...
    'label', Lang('Roulis', 'Roll'));


function cmenu = addMenu_L1_BS(cmenu, parent1, msg)

parent2 = 'ALL_BS';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('BS Absolu', 'Absolute BS'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Absorption coefficient

cmenu = addMenu_L2_AbsorptionCoefficient(cmenu, parent2, msg);

%% Incidence angle

cmenu = addMenu_L2_IncidenceAngle(cmenu, parent2, msg);

%% Best BS

cmenu = addMenu_L2_BestBS(cmenu, parent2, msg);

%% Calibration

cmenu = addMenu_L2_Calibration(cmenu, parent2, msg);


function cmenu = addMenu_L2_AbsorptionCoefficient(cmenu, parent1, msg)

parent2 = 'ALL_AbsorpCoeff';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Step 0 :Coefficient d''absorption (facultatif)', 'Step 0 : Absorption coefficient (Facultative)'));

msg{3} = 'ALL_AbsorpCoeffSSP_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('A partir des profils de c�l�rit�', 'From Sound Speed Profiles'));

msg{3} = 'ALL_AbsorpCoeffODAS_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('A partir de fichiers utilisateurs (Iso Temp & Sal)', 'From user files (Iso Temp & Sal for WC)'));

msg{3} = 'ALL_AbsorpCoeffSIS_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('A partir de fichiers .abs (D�livr�s pas SIS)', 'From .abs files (delivered by SIS)'));

msg{3} = 'ALL_AbsorpCoeff_Manual_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Valeurs manuelles', 'Manual values'));


function cmenu = addMenu_L2_IncidenceAngle(cmenu, parent1, msg)

parent2 = 'ALL_IncidenceAngle';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Step 1 : Angles d''incidence', 'Step 1 : Incidence angles'));

msg{3} = 'ALL_IncidenceAngleFromOneDTM_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('A partir d''un MNT g�n�ral', 'From a unique DTM'));


function cmenu = addMenu_L2_BestBS(cmenu, parent1, msg)

% parent2 = 'ALL_BestBS';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
%     'label', Lang('Best BS', 'Best BS'), 'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_BestBSCompute_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Step 2 : Calcul du meilleur BS', 'Step 2 : Compute best BS'));


function cmenu = addMenu_L1_Segmentation(cmenu, parent1, msg)

parent2 = 'ALL_Segmentation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Segmentation', 'Segmentation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SegmentationSamantha_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Courbes de r�flectivit� angulaire', 'Angular backscatter curves'));


function cmenu = addMenu_L1_RetD(cmenu, parent1, msg)

if ~isdeployed
    parent2 = 'Simrad_RetD';
    cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
        'label', 'R&&D', ...
        'Separator', 'on', 'ForegroundColor', 'b');

    msg{3} = 'ALL_RnD_BottomDetectionSynthese_CI0';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('D�tection du fond Phase & Amplitude', 'Bottom Detection Phase & Amplitude'));
end


function cmenu = addMenu_L2_Utilities_CacheFilesALL(cmenu, parent2, msg)
    
parent3 = 'ALLUtilitiesCacheOldFiles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
        'label', 'Change cache files');
%         'label', 'Cache for old files : Actions');

msg{3} = 'ALL_CacheXML2Netcdf_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Convert XML-Bin to Netcdf'));

msg{3} = 'ALL_CacheDelXML_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', 'Delete XML-Bin');

msg{3} = 'ALL_CacheXML2NetcdfAndDelXML_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Convert XML-Bin to Netcdf & delete XML-Bin'));

msg{3} = 'ALL_CacheDelXMLAndNetcdf_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Delete Netcdf & XML-Bin'));

  
function cmenu = addMenu_L2_Utilities_SignalsExternal(cmenu, parent2, msg)

% TODO : pourrait �tre mutualis� avec SDF_
parent3 = 'ALL_ManageSignals';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Gestion des signaux', 'Manage signals'));

% msg{3} = 'Help';
% msg{4} = 'ALL_ManageSignalsHelp.doc';
% cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Aide', 'Help'), 'ForegroundColor', 'b');
% msg(4) = [];

msg{3} = 'ALL_BackupSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Sauvegarde des signaux', 'Backup signals'));

msg{3} = 'ALL_RestoreSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�tablissement des signaux ', 'Restore signals'));


function cmenu = addMenu_L2_Utilities_FileMarkers(cmenu, parent2, msg)

parent3 = 'ALL_CreateFileMarkers';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Cr�ation de markers', 'Create Markers'));

msg{3} = 'ALL_CreateFileMarkersRuntime_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pour les datagrammes Runtime', 'For Runtime datagrams'));

msg{3} = 'ALL_CreateFileMarkersSounSpeedProfiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pour les datagrammes Sound Speed Profiles', 'For Sound Speed Profiles'));

msg{3} = 'ALL_CreateFileMarkersInstallationParameters_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pour les datagrammes Installation Parameters', 'For Installation Parameters'));


function cmenu = addMenu_L2_Utilities_Datagrams(cmenu, parent2, msg)

parent3 = 'ALL_Datagramms';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Travail sur les datagrammes', 'Work on datagrams'));

msg{3} = 'ALL_DatagramsSplit_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�coupe d''un fichier .all', 'Split (chop) .all file'));

msg{3} = 'ALL_DatagramsSplitFrequencies_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('S�paration des fr�quences d''un fichier .all', 'Split frequencies .kmall file'));

msg{3} = 'ALL_DatagramsWCSeparate_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('.all -> {.all, .wcd}', '.all -> {.all, .wcd}'), 'Enable', 'Off');

msg{3} = 'ALL_DatagramsWCUnionAllWcd_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('{.all, .wcd} -> .all', '{.all, .wcd} -> .all'));

msg{3} = 'ALL_AllMergeFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label',  Lang('{.all1, ..., .alln} -> .all', '{.all1, ..., .alln} -> .all'));

msg{3} = 'ALL_DatagramsSuppress_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Suppression de certains types de datagrammes', 'Remove some types of datagrams'));

msg{3} = 'ALL_DatagramsRepairAllComingFromKmall_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Repair .all files coming from .kmall'));


function cmenu = addMenu_L1_SurveyReport(cmenu, parent2, msg)

parent3 = 'ALL_SurveyReport';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label',Lang('Rapport de campagne', 'Survey Report'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

baseMsg = msg{1};
cmenu = creer_menu_help(cmenu, parent3, baseMsg, 'SScDoc-Tutorial-ALL-SurveyReport.html', 'Tag', 'ALL');

msg{3} = 'ALL_SurveyReportCreate_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Create a survey report framework'));

%% Data

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_SurveyReportData', ...
    'label', Lang('TODO', 'Data'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SurveyReportSummary_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', '1 - Add Summary of lines'));

msg{3} = 'ALL_SurveyReportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', '2 - Add Navigation plots'));

msg{3} = 'ALL_SurveyReportSSP_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', '3 - Add Sound Speed Profiles'));

msg{3} = 'ALL_SurveyReportSummaryNavSSP_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Make 1, 2 and 3 at once'));

%% DTM and Mosaic

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_SurveyReport_DTMAndMosaic', ...
    'label', Lang('TODO', 'DTM & Mosaic'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SurveyReportGlobalMosaic_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Compute a DTM & a Mosaic'));

%% Add data

msg{3} = 'ALL_SurveyReport_AddData';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, ...
    'label', Lang('TODO', 'Import Images and figures'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

parent4 = 'ALL_SurveyReport_AddImage';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('TODO', 'Add the current image'));

msg{3} = 'ALL_SurveyReport_AddLatLongBathymetry_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Special Bathymetry'));

msg{3} = 'ALL_SurveyReport_AddLatLongReflectivity_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Special Reflectivity'));

msg{3} = 'ALL_SurveyReport_AddLatLongEchoIntegration_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Special Echo Integration'));

msg{3} = 'ALL_SurveyReportAddFigures_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Add figures'));

%% Engineering

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_SurveyReport_Engineering', ...
    'label', Lang('TODO', 'Engineering'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SurveyReportPlotImageSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Plot Image and Signals'));

msg{3} = 'ALL_SurveyReportSonarEquation_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Check Sonar Equation'));

%% Utilities

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALL_SurveyReport_Utilities', ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SurveyReport_Extract_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Extract shortened version'));

msg{3} = 'ALL_SurveyReportCleanDirectories_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Clean up subdirectories'));

msg{3} = 'ALL_SurveyReportGroupShapeFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Group shape files'));


function cmenu = addMenu_L3_Utilities_SPFE(cmenu, parent2, msg)

parent3 = 'ALLUtilitiesForSPFE';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', 'For SPFE');

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALLUtilitiesForSPFETitle', ...
    'label', 'BS workflow', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SPFEBatch0_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE Batch 0 : Compute Incidence angle & ReflectivityBestBS'));

msg{3} = 'ALL_SPFEBatch1_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE Batch 1 : Uncalibrated BS, no DiagTx correction'));

msg{3} = 'ALL_SPFEBatch2_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE Batch 2 : Calibrated BS (BS curve from EK80) and DiagTx correction.'));

msg{3} = 'ALL_SPFEBatch3_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE Batch 3 : Calibrated BS + DiagTx correction + Flattening'));

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALLUtilitiesForSPFEOthers', ...
    'label', 'Other tools', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_ExportAttitude_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Export attitude data into .csv or NetCDF'));

msg{3} = 'ALL_ExportPingNumberAndUnixTime_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Export the ping number and the Unix time into .csv or NetCDF'));

msg{3} = 'ALL_ReduceCsvFile_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Reduce an EI .csv file'));


cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'ALLUtilitiesForSPFEOldBatchs', ...
    'label', 'Old Batchs', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ALL_SPFEBatch4_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE Batch 4 : Report'));

msg{3} = 'ALL_SPFEBatch5_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE Batch 5 : Create masks for Marc Roche'));




    
function cmenu = addMenu_L3_Utilities_GeoscienceAzur(cmenu, parent2, msg)

parent3 = 'ALLUtilitiesForGeoscienceAzur';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', 'For GeoscienceAzur');

msg{3} = 'ALL_SOSBeamPointingAngle_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('SOS signe des BeamPointingAngle', 'SOS Sign of BeamPointingAngles'));
 
msg{3} = 'ALL_SOSLevelArm_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Correction de bras de levier EM710 Pour GeoAzur', 'Level arm correction for EM710 GeoAzur'));
