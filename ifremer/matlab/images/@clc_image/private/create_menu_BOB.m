function cmenu = create_menu_BOB(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'BOB';

parent1 = 'BOB_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".bob" (Donn�es BOB de MOVIES)', '".bob" files (BOB Data from MOVIES)'));

%% Exports GLOBE

parent2 = 'BOB_3D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'GLOBE', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'BOB_Export3DViewer';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers GLOBE', 'Export to GLOBE'));

%% Processing CCU or/and RAW files

parent2 = 'TagBOBCCURAWFiles';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Traitement CCU et fichiers RAW', 'Processing CCU Log and RAW files'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'TagBOBGlobalProcessing', ...
    'label', Lang('Traitement global', 'Global processing'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'BOB_ComputeEIFromRawFiles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Echo Integration depuis les fichiers RAW', 'Processing EI from RAW Files'));

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'TagBOBStepProcessing', ...
    'label', Lang('Etapes de traitements', 'Processing Steps'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'BOB_AnalyseDataProcessing';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Comparaison des donn�es CCU/RawFiles', 'CCU Log Data/Raw Files comparison'));

msg{3} = 'BOB_DispatchRawFiles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Dispersion des fichiers RAW par secteur', 'Dispatch RAW Files by sector'));

msg{3} = 'BOB_ConvertRAW2HacFiles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Conversion des fichiers RAW en HAC', 'Convert RAW into HAC Files'));

msg{3} = 'BOB_DuplicateConfigXML';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Duplication de la configuration XML pour M3D', 'M3D-XML configuration duplication'));

msg{3} = 'BOB_ComputeEIFromHacFiles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Echo Integration depuis les fichiers HAC', 'Processing EI from HAC Files'));

msg{3} = 'BOB_CreationBOBFilesFor3DV';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Creation du fichier ".bob" for GLOBE', 'Creation ".bob" file for GLOBE'));

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'BOB_Displaying', ...
    'label', Lang('Etapes d''afichage', 'Displaying Steps'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'BOB_DisplaySummaryConfig';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�sum� de configuration nominale', 'Nominal Configuration summary'), ...
    'Separator', 'on');

msg{3} = 'BOB_DisplayGraphsCCU';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�sum� des angles CCU', 'CCU Angles summary'));

msg{3} = 'BOB_DisplayAnalyseDataProcessing';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Affichage de la comparaison CCU/RawFiles depuis un fichier CSV', 'CCU Log Data/Raw Files comparison from CSV file'));


%% Utilities

parent2 = 'TagBOBUtilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'BOB_ReadOnce';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture (non obligatoire)', 'Just read once (not obligatory)'));

msg{3} = 'BOB_ZipDir';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zippage des r�pertoires cache', 'Zip directories'));

msg{3} = 'BOB_DeleteCache';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

msg{3} = 'BOB_DeleteCacheOfSurveyProcessing';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope du traitement CCU/Raw Files', 'SonarScope cache erase of CCU/Raw Files processing'));

