function cmenu = create_menu_CAR(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'CAR';

parent1 = 'CAR_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers Caraibes ', 'Caraibes files'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'Caraibes_files.html');

%% Fichiers .nvi

parent2 = 'CAR_NVI';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Fichiers .nvi', '.nvi files'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'CAR_PlotNvi_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'CAR_PlotNviSignal_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'CAR_SummaryNvi_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�sum� des fichiers .nvi', 'Summary .nvi files'));

msg{3} = 'CAR_EditNvi_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition de la navigation', 'Edit navigation'));

%% Fichier .mbg

parent2 = 'CAR_MBG';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Fichiers .mbg', '.mbg files'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'CAR_MBG_PlotNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'CAR_PingBeam2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', '.mbg  --> LatLong');

msg{3} = 'CAR_SMbbSplit_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�coupage d''un fichier .mbb', ...
    'Split an .mbg file'));

%% Vieux trucs

parent2 = 'CAR_OldThings';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Vieux trucs', 'Old things'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'CAR_SonarLateralImportMbb_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('G�n�ration de fichiers .ers � partir d''un fichier .mbb', ...
    'Export .mbb -> {PingBeam_xxxx} // Sidescan files'));

msg{3} = 'CAR_ProjetArtistiqueUnivLaRochelle_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'Projet Artistique Univ La Rochelle');
