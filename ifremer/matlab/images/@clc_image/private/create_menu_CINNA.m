function cmenu = create_menu_CINNA(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'CINNA';

parent1 = 'CINNA_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers CINNA ".NA" ', 'CINNA ".NA"'));

%% Navigation

parent2 = 'CINNA_Nav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'CINNA_PlotNavigation';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'CINNA_PlotSignalOnNavigation';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

%% RTK Data

parent2 = 'CINNA_RTK';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('RTK', 'RTK'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'CINNA_RTK_Plot';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation des donn�es', 'Plot data'));

msg{3} = 'CINNA_RTK_Clean';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Contr�le de la hauteur RTK', 'Check RTK Height'));

msg{3} = 'CINNA_RTK_CreateTide';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul et export de la mar�e', 'Compute & Export Tide'));

msg{3} = 'CINNA_RTK_Raz';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RAZ des donn�es', 'Reset data'));
