function cmenu = create_menu_CacheFiles(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'CACHE';

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'ALL_UtilitiesCache', ...
    'label', 'Cache directories', ...
    'Separator', 'On', 'ForegroundColor', 'b');

parent1 = 'CACHE_Preferences';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', 'Preferences');

msg{3} = 'CACHE_CreateXMLBinOnly';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', 'Create XML-Bin only');

msg{3} = 'CACHE_CreateNetcdfAndKeepXMLBin';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', 'Create Netcdf and keep XML-Bin');

msg{3} = 'CACHE_CreateNetcdfAndDelXMLBin';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', 'Create Netcdf and delete XML-Bin');

% TODO : voir si opportun de ramener ALL_CacheDelXMLAndNetcdf_CI0 dans create_menu_CacheFiles ainsi que ALL_CheckAcquisitionDir
% et ALL_ZipDir � supprimer
