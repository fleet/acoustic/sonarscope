function cmenu = create_menu_GP_ADCP(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'ADCP_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0,  ...
    'label', Lang('Profileur ADCP', 'ADCP Profiler'));

msg{2} = 'ADCP_Export3DViewer_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Export vers 3DViewer', 'Export GLOBE'));
