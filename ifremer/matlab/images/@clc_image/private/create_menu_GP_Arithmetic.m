function cmenu = create_menu_GP_Arithmetic(~, baseMsg, cmenu)

%% Menu

msg{1} = baseMsg;
parent0 = 'GP_Arithmetic';
cmenu = ajouter(cmenu, 'tag', parent0, 'label', ...
    Lang('Fonctions Arithmetiques', 'Arithmetic functions'), ...
    'Separator', 'on');

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Arithmetic_functions.html');

%% Sous-Menus

cmenu = addMenu_L1_ArithmeticAddition(   cmenu, parent0, msg);
cmenu = addMenu_L1_ArithmeticSubtraction(cmenu, parent0, msg);
cmenu = addMenu_L1_ArithmeticMultiply(   cmenu, parent0, msg);
cmenu = addMenu_L1_ArithmeticDivide(     cmenu, parent0, msg);
cmenu = addMenu_L1_ArithmeticTrigo(      cmenu, parent0, msg);

msg{2} = 'GP_Arithmetic_Abs_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'abs');

msg{2} = 'GP_Arithmetic_Sqrt_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'sqrt');

msg{2} = 'GP_Arithmetic_Log10_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'log10');

msg{2} = 'GP_Arithmetic_10pow_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', '10^');

msg{2} = 'GP_Arithmetic_SetNaN_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mise � NaN des pixels (N)', 'Set all pixels to NaN (N)'));

msg{2} = 'GP_Arithmetic_Formula_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Formule', 'Formula'));

msg{2} = 'GP_Arithmetic_CompleteAWithB_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Compl�ter cette image par une autre', 'Complete this image with another one'), ...
    'Separator', 'On');

msg{2} = 'GP_Arithmetic_Union_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Union d''images', 'Images union'));


%% Multi images

msg{2} = 'Help';
msg{3} = 'TODO';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'HelpArithmeticMultiImages', 'message', msg, ...
    'label', Lang('Images multiples', 'Multi Images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');
msg(3) = [];

msg{2} = 'GP_Arithmetic_Mean_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyennage', 'Mean'));

msg{2} = 'GP_Arithmetic_Max_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Max', 'Max'));

msg{2} = 'GP_Arithmetic_Min_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Min', 'Min'));

msg{2} = 'GP_Arithmetic_Med_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('M�diane', 'Median'));

msg{2} = 'GP_Arithmetic_Mode_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mode', 'Mode'));

msg{2} = 'GP_Arithmetic_Std_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ecart-type', 'Std'));

msg{2} = 'GP_Arithmetic_Sum_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Somme', 'Sum'));

msg{2} = 'GP_Arithmetic_QuadMean_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyennage quadratique', 'Quadratic Mean'));



function cmenu = addMenu_L1_ArithmeticAddition(cmenu, parent0, msg)

parent1 = 'GP_Arithmetic_Add';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1,  ...
    'label', Lang('Addition', 'Add'), ...
    'Separator', 'on');

msg{2} = 'GP_Arithmetic_Add_Image';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D''une image', 'an image'));

msg{2} = 'GP_Arithmetic_Add_Scalar';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D''un scalaire', 'a scalar'));

msg{2} = 'GP_Arithmetic_Add_Vector';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D''un vecteur', 'a signal'));



function cmenu = addMenu_L1_ArithmeticSubtraction(cmenu, parent0, msg)

parent1 = 'GP_Arithmetic_Subtract';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Soustraction', 'Subtract'));

msg{2} = 'GP_Arithmetic_Subtract_Image';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D''une image', 'an image'));

msg{2} = 'GP_Arithmetic_Subtract_Scalar';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D''un scalaire', 'a scalar'));

msg{2} = 'GP_Arithmetic_Subtract_Vector';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D''un vecteur', 'a signal'));


function cmenu = addMenu_L1_ArithmeticMultiply(cmenu, parent0, msg)

parent1 = 'GP_Arithmetic_Multiply';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Multiplication', 'Multiply'));

msg{2} = 'GP_Arithmetic_Multiply_Image';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang( 'par une image', 'by an image'));

msg{2} = 'GP_Arithmetic_Multiply_Scalar';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('par un scalaire', 'by a scalar'));

msg{2} = 'GP_Arithmetic_Multiply_Vector';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('par un vecteur', 'by a signal'));


function cmenu = addMenu_L1_ArithmeticDivide(cmenu, parent0, msg)

parent1 = 'GP_Arithmetic_Divide';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Division', 'Divide'));

msg{2} = 'GP_Arithmetic_Divide_Image';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('par une image', 'by an image'));

msg{2} = 'GP_Arithmetic_Divide_Scalar';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('par un scalaire', 'by a scalar'));

msg{2} = 'GP_Arithmetic_Divide_Vector';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('par un vecteur', 'by a signal'));


function cmenu = addMenu_L1_ArithmeticTrigo(cmenu, parent0, msg)

parent1 = 'GP_Arithmetic_Trigo';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', 'Trigo');

msg{2} = 'GP_Arithmetic_Trigo_Cos';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Cos');

msg{2} = 'GP_Arithmetic_Trigo_AcosRd';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Acos (rd)');

msg{2} = 'GP_Arithmetic_Trigo_AcosDeg';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Acos (Deg)');

msg{2} = 'GP_Arithmetic_Trigo_Sin';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Sin');

msg{2} = 'GP_Arithmetic_Trigo_AsinRd';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Asin (rd)');

msg{2} = 'GP_Arithmetic_Trigo_AsinDeg';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Asin (Deg)');

msg{2} = 'GP_Arithmetic_Trigo_Tan';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Tan');

msg{2} = 'GP_Arithmetic_Trigo_AtanRd';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Atan (rd)');

msg{2} = 'GP_Arithmetic_Trigo_AtanDeg';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Atan (Deg)');
