function cmenu = create_menu_GP_Carto(~, baseMsg, cmenu)

parent0 = 'GP_Carto_MainMenu';
msg{1} = baseMsg;
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Cartographie', 'Cartography'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Cartography.html');

%% Menus

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'GP_Carto_Projection', ...
    'label', Lang('Image courante', 'Current Image'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_Carto_GeoYX2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'GeoYX -> LatLong');

msg{2} = 'GP_Carto_LatLong2GeoYX_CI1';
cmenu = ajouter(cmenu, 'parent', parent0,  'tag', msg{2}, 'message', msg, ...
    'label', 'LatLong  -> GeoYX');

if ~isdeployed
    msg{2} = 'GP_Carto_ChangeEllipsoid_CI1';
    cmenu = ajouter(cmenu, 'parent', parent0,  'tag', msg{2}, 'message', msg, ...
        'label', 'Change Ellipsoid');
end

%%

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'GP_Carto_ComputeCoordinates', ...
    'label', Lang('Conversions de coordonn�es', 'Conversion of coordinates'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_Carto_ComputeCoordinates_LatLong2GeoYX_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'LatLong  -> GeoYX');

msg{2} = 'GP_Carto_ComputeCoordinates_GeoYX2LatLong_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'GeoYX -> LatLong');

%%

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'GP_Carto_Appendices', ...
    'label', Lang('Annexes', 'Appendices'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_Carto_ImportWreckCoordinates_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Localisation d''une �pave', 'Locate a wreck'));

msg{2} = 'GP_Carto_ExtractProfile_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Extraction d''un profil le longs de segments', 'Extract profil along segments'));

msg{2} = 'GP_Carto_ImportANavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Surimpression d''une navigaton', 'Overplot a navigation'));
