function cmenu = create_menu_GP_Codage(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'GP_Codage_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Codage', 'Encoding'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Encoding.html');

%% Menus

parent1 = 'GP_Codage_Reflectivity';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Réflectivité', 'Reflectivity'));

msg{2} = 'GP_Codage_Reflectivity_Enr2dB_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Energie -> dB : 10.log10(Image)', 'Energy -> dB : 10.log10(Image)'), ...
    'Separator', 'on');

msg{2} = 'GP_Codage_Reflectivity_dB2Enr_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('dB -> Energie : 10^(Image/10)', 'dB -> Energy : 10^(Image/10)'));

msg{2} = 'GP_Codage_Reflectivity_Amp2dB_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Amplitude -> dB : 20.log10(Image)');

msg{2} = 'GP_Codage_Reflectivity_dB2Amp_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'dB -> Amplitude : 10^(Image/20)');

%% 

msg{2} = 'GP_Codage_Quantify_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Quantification', 'Quantization'));

msg{2} = 'GP_Codage_Binary_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Binarisation', 'Binarization'));

msg{2} = 'GP_Codage_SqrtAbs_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'sqrt(abs(z))*sign(z)');

msg{2} = 'GP_Codage_Log10Abs_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Log10(abs(z))*sign(z)');

msg{2} = 'GP_Codage_ToStats_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Change stats');

