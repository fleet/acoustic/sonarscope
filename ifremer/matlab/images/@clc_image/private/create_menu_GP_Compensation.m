function cmenu = create_menu_GP_Compensation(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'GP_Compensation_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Compensation', 'Compensation'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Compensation.html');

%% CompensationVerticale

cmenu = addMenu_L1_CompensationVerticale(cmenu, parent0, msg);

%% CompensationHorizontale

cmenu = addMenu_L1_CompensationHorizontale(cmenu, parent0, msg);

%% Others

msg{2} = 'GP_Compensation_Plan_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Plan inclin�', 'Tilted plan'));

msg{2} = 'GP_Compensation_Polynom_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Surface polynomiale', 'Polynomial surface'));

msg{2} = 'GP_Compensation_Curves_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Courbes conditionnelles', 'Conditional curves'));


function cmenu = addMenu_L1_CompensationVerticale(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_Compensation_Vert';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Direction Verticale', 'Vertical Direction'), ...
    'Separator', 'on');

%% Sous-menus

msg{2} = 'GP_Compensation_Vert_Mean_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur moyenne', 'By the average value'));

msg{2} = 'GP_Compensation_Vert_Med_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur m�diane', 'By the median value'));

msg{2} = 'GP_Compensation_Vert_Min_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur min', 'By the minimum value'));

msg{2} = 'GP_Compensation_Vert_Max_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur max', 'By the maximum value'));

msg{2} = 'GP_Compensation_Vert_Sum_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Division par la somme', 'Divide by the sum'), ...
    'Separator', 'On');


function cmenu = addMenu_L1_CompensationHorizontale(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_Compensation_Horz';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Direction Horizontale', 'Horizontal Direction'));

%% Sous-menus

msg{2} = 'GP_Compensation_Horz_Mean_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur moyenne', 'By the average value'));

msg{2} = 'GP_Compensation_Horz_Med_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur m�diane', 'By the median value'));

msg{2} = 'GP_Compensation_Horz_Min_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur min', 'By the minimum value'));

msg{2} = 'GP_Compensation_Horz_Max_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par la valeur max', 'By the maximum value'));

msg{2} = 'GP_Compensation_Horz_Sum_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Division par la somme', 'Divide by the sum'), ...
    'Separator', 'On');
