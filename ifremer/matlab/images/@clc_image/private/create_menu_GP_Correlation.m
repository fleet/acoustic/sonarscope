function cmenu = create_menu_GP_Correlation(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'GP_Correlation_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Correlation', 'Correlation'));

%% Help

cmenu = creer_menu_help(cmenu, parent0,baseMsg, 'Correlation.html');

%% Menus

msg{2} = 'GP_SelfCorrelation_ByFFT_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Autocorrelation', 'Self-correlation'), ...
    'Separator', 'on');

msg{2} = 'GP_CrossCorrelation_ByFFT_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Intercorrelation', 'Cross-correlation'));

parent1 = 'GP_CrossCorrelation_ShapeRecognation';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Reconnaissance de forme', 'Shape recognation'));

msg{2} = 'GP_CrossCorrelation_ShapeRecognationGeometricFrame_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Forme géométrique', 'Geometric shape'));

msg{2} = 'GP_CrossCorrelation_ShapeRecognationImageSnippet_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Imagette', 'Image snippet'));

msg{2} = 'GP_CrossCorrelation_GeometricFrameFromImageSnippet_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('TODO', 'Estimate geometric shape'), ...
    'Separator', 'on');

