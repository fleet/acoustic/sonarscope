function cmenu = create_menu_GP_Derivate(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'GP_Derivate_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Fonctions Deriv�es', 'Derived functions'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Derived_functions.html');

%% Gradients

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'GP_Derivate_Gradient', ...
    'label', Lang('Gradients', 'Gradients'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_Derivate_Gradient_Oriented_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Gradient orient�', 'Oriented gradient'));

msg{2} = 'GP_Derivate_Gradient_Greatest_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Norme du gradient', 'Greatest gradient'));

msg{2} = 'GP_Derivate_Laplacian_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('1/4 Laplacien', '1/4 Laplacian'));

%% Pentes de terrain

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'GP_Derivate_Slope', ...
    'label', Lang('Pentes de terrain', 'Terrain slopes'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_Derivate_Slope_Oriented_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Pente orientee', 'Oriented slope'));

msg{2} = 'GP_Derivate_Slope_Greatest_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Plus grande pente', 'Greatest slope'));

msg{2} = 'GP_Derivate_Slope_Direction_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Direction grande pente', 'Greatest slope direction'));

msg{2} = 'GP_Derivate_Slope_Rose_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Rosace des pentes', 'Rose of slopes'));

%% Ombrages

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'GP_Derivate_SunShading', ...
    'label', Lang('Ombrages', 'Shadings'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_Derivate_SunShading_Azimuth_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Image couleur ombr�e (S)', 'Color image - Sun illuminated (S)'));

msg{2} = 'GP_Derivate_SunShading_GreatestSlope_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Image ombr�e � partir de la plus grande pente', 'Shaded image from greatest slope'));

msg{2} = 'GP_Derivate_SunShading_AzimuthGrey_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Image ombr�e en teintes de gris', 'Sun illuminated image in gray levels'));

msg{2} = 'GP_Derivate_SunShading_Movie_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Film d''Images couleur ombr�es', 'Movie of Color images - Sun illuminated '));

