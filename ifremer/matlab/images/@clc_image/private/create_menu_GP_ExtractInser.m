function cmenu = create_menu_GP_ExtractInser(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'GP_ExtractInser_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Extraction / Insertion', 'Extract / Insert'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Extract-Insert.html');

%% Menus

msg{2} = 'GP_ExtractInser_Extraction_CI2'; % Current Image 2 : Possible uniquement en zoom
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Extraction', 'Extract'), ...
    'Separator', 'on');

msg{2} = 'GP_ExtractInser_Insertion_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Insertion dans l''image d''origine', 'Insert into the mother image'));

msg{2} = 'GP_ExtractInser_Interpolation_Sampling_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Reduction/Agrandissement de la taille par echantillonnage', 'Size reduction by sampling'));

msg{2} = 'GP_ExtractInser_Interpolation_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Reduction/Agrandissement de la taille par interpolation', 'Size reduction or dilate by averaging/interpolation'));

msg{2} = 'GP_ExtractInser_Crop_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Rognage des NaN', 'Crop'));
