function cmenu = create_menu_GP_Filter(~, baseMsg, cmenu)

msg{1} = baseMsg;

%% Menu Filtres

parent0 = 'GP_Filter_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Filtres', 'Filters'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Filters.html');

%% Menus Classical Filters

parent1 = 'GP_Filter_Classic';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Classique', 'Classical'), ...
    'Separator', 'on');

%% Help

cmenu = creer_menu_help(cmenu, parent1, msg{1}, 'Classical_filters.html');

%%  TagMenuFiltre classiques Passe-Bas

cmenu = addMenu_L2_FilterLinear(cmenu, parent1, msg);

%% Menus Filtres Edge

cmenu = addMenu_L2_FilterEdge(cmenu, parent1, msg);

%% Menus Others

cmenu = addMenu_L2_FilterOthers(cmenu, parent1, msg);

%%  TagMenuFiltre Speckle

cmenu = addMenu_L1_FilterSpeckle(cmenu, parent0, msg);



function cmenu = addMenu_L2_FilterLinear(cmenu, parent1, msg)

parent2 = 'GP_Filter_Linear';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Lin�aire', 'Linear'));

cmenu = creer_menu_help(cmenu, parent2, msg{1}, 'Linear.html');

msg{2} = 'GP_Filter_Linear_LowpassRectangle_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne Rectangle', 'Rectangle Average'));

msg{2} = 'GP_Filter_Linear_LowpassDisk_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne Disque', 'Circular Average'));

msg{2} = 'GP_Filter_Linear_LowpassGauss_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Gaussien', 'Gauss'));

msg{2} = 'GP_Filter_Linear_LowpassGaussOriented_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Gaussien orient�', 'Gauss oriented'));


function cmenu = addMenu_L2_FilterEdge(cmenu, parent1, msg)

%% Menu

parent2 = 'GP_Filter_Contours';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Contours', 'Edge'));

%% Help

cmenu = creer_menu_help(cmenu, parent2, msg{1}, 'Edge.html');

%% Sous-menus

msg{2} = 'GP_Filter_Contours_Sobel_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, ...
    'message', msg, 'label', 'Sobel');

msg{2} = 'GP_Filter_Contours_Prewitt_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, ...
    'message', msg, 'label', 'Prewitt');

msg{2} = 'GP_Filter_Contours_Roberts_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, ...
    'message', msg, 'label', 'Roberts');

msg{2} = 'GP_Filter_Contours_LaplacianOfGaussian_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, ...
    'message', msg, 'label', 'Laplacian Of Gaussian');

msg{2} = 'GP_Filter_Contours_ZeroCrossing_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, ...
    'message', msg, 'label', 'Zero crossing');

msg{2} = 'GP_Filter_Contours_Canny_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, ...
    'message', msg, 'label', 'Canny');


function cmenu = addMenu_L2_FilterOthers(cmenu, parent1, msg)

parent2 = 'GP_Filter_Others';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Autres', 'Other'));
cmenu = creer_menu_help(cmenu, parent2, msg{1}, 'Other.html');

msg{2} = 'GP_Filter_Others_Unsharp';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Adoucisseur', 'Unsharp'));

msg{2} = 'GP_Filter_Others_Median';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Median', 'Median'));

msg{2} = 'GP_Filter_Others_Wiener';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Wiener', 'Wiener'));

msg{2} = 'GP_Filter_Others_Wallis';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Wallis', 'Wallis'));

msg{2} = 'GP_Filter_Others_Laplacian_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Laplacien', 'Laplacian'));

msg{2} = 'GP_Filter_Others_Std_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ecart-type', 'std'));

msg{2} = 'GP_Filter_Others_Entropy_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Entropie', 'Entropy') );

msg{2} = 'GP_Filter_Others_Range_CI1';
cmenu = ajouter(cmenu, 'parent', parent2,  'tag', msg{2}, 'message', msg, ...
    'label', Lang('Rnage', 'Range'));



function cmenu = addMenu_L1_FilterSpeckle(cmenu, parent0, msg)

%% Menu

parent2 = 'GP_Filter_Speckle';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent2, ...
    'label', Lang('Speckle', 'Speckle'));

%% Help

cmenu = creer_menu_help(cmenu, parent2, msg{1}, 'Classical.html');

%% sous-Menus

msg{2} = 'GP_Filter_Speckle_Lee_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Lee', 'Lee'));

msg{2} = 'GP_Filter_Speckle_Wiener_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Wiener (essayez moi)', 'Wiener (pretty cool results)'));

msg{2} = 'GP_Filter_Speckle_TLineaire_CI1'; % TODO : supprimer cette fonction
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('TLineaire', 'TLineaire'));

msg{2} = 'GP_Filter_Speckle_Kuan_CI1';
cmenu = ajouter(cmenu, 'parent', parent2,  'tag', msg{2}, 'message', msg, ...
    'label', Lang('Kuan', 'Kuan'));

msg{2} = 'GP_Filter_Speckle_Frost_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Frost', 'Frost'));

msg{2} = 'GP_Filter_Speckle_MAP_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('MAP', 'MAP'));

msg{2} = 'GP_Filter_Speckle_EAP_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('EAP', 'EAP'));

msg{2} = 'GP_Filter_Speckle_Quantile_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Quantile', 'Quantile'));
