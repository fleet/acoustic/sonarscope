function cmenu = create_menu_GP_Fourier(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'GP_Fourier_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Fourier', 'TODO'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Fourier.html');

%% Menus

msg{2} = 'GP_Fourier_FFT_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('FFT', 'FFT'), ...
    'Separator', 'on');

msg{2} = 'GP_Fourier_Filter_Disk_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Filtre disque', 'Disk Filter'));

msg{2} = 'GP_Fourier_Filter_Gauss_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Filtre Gauss', 'Gaussian Filter'));

msg{2} = 'GP_Fourier_Filter_Threshold_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Filtre Seuil', 'Threshold Filter'));

msg{2} = 'GP_Fourier_IFFT_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('IFFT', 'IFFT'));

