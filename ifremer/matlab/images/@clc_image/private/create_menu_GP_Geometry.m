function cmenu = create_menu_GP_Geometry(~, baseMsg, cmenu)

parent0 = 'GP_Geometry_MainMenu';
msg{1} = baseMsg;
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Traitements g�om�triques', 'Geometric processing'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Geometric_processing.html');

%% Menus

msg{2} = 'GP_Geometry_VerticalFlip_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Retournement vertical', 'Vertical flip'), ...
    'Separator', 'on');

msg{2} = 'GP_Geometry_HorizontalFlip_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Retournement horizontal', 'Horizontal flip'));

msg{2} = 'GP_Geometry_RotXY_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Cr�ation de coordonn�es dans un rep�re orient�', 'Creation of rotated coordinates'));
