function cmenu = create_menu_GP_ImageType(~, baseMsg, cmenu)

parent0 = 'GP_ImageType_MainMenu';
msg{1} = baseMsg;
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Couleurs', 'Colors'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Colors.html');

%% Menus

msg{2} = 'GP_ImageType_RGB2Intensity_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('RGB -> Intensit�', 'RGB -> Intensity'), ...
    'Separator', 'on');

msg{2} = 'GP_ImageType_RGB2Indexed_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('RGB -> Index�e', 'RGB -> Indexed'));

msg{2} = 'GP_ImageType_RGB2HSV_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('RVB -> HSV', 'RGB -> HSV'));

msg{2} = 'GP_ImageType_Intensity2RGB_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Intensit� -> RGB', 'Intensity -> RGB'));

msg{2} = 'GP_ImageType_Intensity2Indexed_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Intensit� -> Index�e', 'Intensity -> Indexed'));

msg{2} = 'GP_ImageType_Indexed2RGB_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Index�e -> RGB', 'Indexed -> RGB'));

msg{2} = 'GP_ImageType_Indexed2Intensity_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Index�e -> Intensit�', 'Indexed -> Intensity'));

msg{2} = 'GP_ImageType_Indexed2IntensityDirect_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Index�e -> Intensit� (Change juste ImageType)', 'Indexed -> Intensity (Just change ImageType)'));

msg{2} = 'GP_ImageType_ChangeNaNColor_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Change NaN color');

msg{2} = 'GP_ImageType_RGB2IntensityWithGMTColomapLimits_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('intensit� -> RGB avec respect des limites d''une table de couleur GMT', 'intensity -> RGB with GMT colormap limits'));

