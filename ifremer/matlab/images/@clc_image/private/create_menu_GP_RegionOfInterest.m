function cmenu = create_menu_GP_RegionOfInterest(~, baseMsg, cmenu)

msg{1} = baseMsg;

parent0 = 'GP_RegionOfInterest_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('ROI / Masques', 'ROI / Masks'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Region_of_interest.html');

%% Byte support

cmenu = addMenu_L1_ByteSupport(cmenu, parent0, msg);

%% Op�rateurs de morphologie math�matique

cmenu = addMenu_L1_MorphologieMathematique(cmenu, parent0, msg);

%% Contours

cmenu = addMenu_L1_Contours(cmenu, parent0, msg);

%% Bits support

cmenu = addMenu_L1_BitsSupport(cmenu, parent0, msg);

%% Others

cmenu = addMenu_L1_Others(cmenu, parent0, msg);

%% Multi-images

cmenu = addMenu_L1_MultiImages(cmenu, parent0, msg);



function cmenu = addMenu_L1_ByteSupport(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_ROI_ByteSupport';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Valeurs enti�res', 'Byte support'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Help

cmenu = creer_menu_help(cmenu, parent1, msg{1}, 'Byte_support.html');

%% Sous-Menus

msg{2} = 'GP_ROI_Create_Manual_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Contourage interactif', 'Interactive delineation'));

msg{2} = 'GP_ROI_Create_Values_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�duite de l''image courante', 'Deduced from the displayed image'));

msg{2} = 'GP_ROI_Create_Histo_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�duite d''un nettoyage histogrammique', 'Deduced from histogram cleaning'));

msg{2} = 'GP_ROI_MaskImage_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Masquage', 'Masking'));

msg{2} = 'GP_ROI_CombineMasks_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Combiner des masques', 'Combine masks'));

msg{2} = 'GP_ROI_ColorizeImage_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Superposition � une image d''intensit�', 'Colorize an Intensity Image'));


function cmenu = addMenu_L1_MorphologieMathematique(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_ROI_MorphoMath';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Morphologie math�matique', 'Morphological Operations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Help

cmenu = creer_menu_help(cmenu, parent1, msg{1}, 'Morphological_operations.html');

%% Sous-Menus

tag = 'GP_ROI_MorphoMath_ModeProgram'; 
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', tag, ...
    'label', Lang('Mode programme', 'Program Mode'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_ROI_MorphoMath_And_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Et', 'And'));

msg{2} = 'GP_ROI_MorphoMath_Or_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ou', 'Or'));

msg{2} = 'GP_ROI_MorphoMath_Xor';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ou exclusif', 'Xor'));

msg{2} = 'GP_ROI_MorphoMath_Dilatation_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Dilatation');

msg{2} = 'GP_ROI_MorphoMath_Erosion_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Erosion');

msg{2} = 'GP_ROI_MorphoMath_Opening_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ouverture', 'Opening'));

msg{2} = 'GP_ROI_MorphoMath_Closing_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Fermeture', 'Closing'));

%%

tag = 'GP_ROI_MorphoMath_ModeResult'; 
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', tag, ...
    'label', Lang('Mode r�sultat', 'Result Mode'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'GP_ROI_MorphoMath_Contour_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Contour pixelis� d''un masque', 'Pixelized mask boundary'));

msg{2} = 'GP_ROI_MorphoMath_WrappingAroundMAsk_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Masque englobant', 'Wrapping-around mask'));


function cmenu = addMenu_L1_Contours(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_ROI_Contours';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Contours', 'Contours'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Help

cmenu = creer_menu_help(cmenu, parent1, msg{1}, 'Contours.html');

%% Sous-Menus

msg{2} = 'GP_ROI_Contours_Display';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Visualisation', 'Display'));

msg{2} = 'GP_ROI_Contours_Export_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Exportation', 'Export'));

msg{2} = 'GP_ROI_Contours_Import_CI0';
cmenu = ajouter(cmenu, 'parent', parent1,  'tag', msg{2}, 'message', msg, ...
    'label', Lang('Importation', 'Import'));

msg{2} = 'GP_ROI_Contours_Rename_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Changement des num�ros', 'Change numbers'));

msg{2} = 'GP_ROI_Contours_Delete_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Suppression', 'Delete'));

msg{2} = 'GP_ROI_Contours_RemoveCurvesOnCurrentImage_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Effacement des contours sur SonarScope', 'Delete contours on SonarScope'));

msg{2} = 'GP_ROI_Contours_Mask2Contours_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Extraction des contours d''un masque', 'Extract contours from Mask'));

msg{2} = 'GP_ROI_Contours_Contours2Mask_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Cr�ation d''un masque � partir de contours', 'Create a Mask from contours'));


function cmenu = addMenu_L1_BitsSupport(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_ROI_BitsSupport';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Valeurs binaires', 'Bits support'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Help

cmenu = creer_menu_help(cmenu, parent1, msg{1}, 'Bits_support.html');

%% Sous-Menus

msg{2} = 'GP_ROI_BitsSupport_MaskImage_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Masquage', 'Masking'));

msg{2} = 'GP_ROI_BitsSupport_Extraction_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Extraction', 'Extraction'));


function cmenu = addMenu_L1_Others(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_ROI_Others';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Autres', 'Others'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Help

cmenu = creer_menu_help(cmenu, parent1, msg{1}, 'Others.html');

%% Sous-Menus

msg{2} = 'GP_ROI_Others_Watershed_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Bassin versant', 'Watershed'));

msg{2} = 'GP_ROI_Others_GetCorners_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�tection des coins', 'Get corners'));

if ~isdeployed
    msg{2} = 'GP_ROI_Others_LevelSet_CI1';
    cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
        'label', Lang('LevelSet (en cours de test)', 'LevelSet (under work)'));
end


function cmenu = addMenu_L1_MultiImages(cmenu, parent0, msg)

%% Menu

parent1 = 'GP_ROI_MultiImages';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Images multiples', 'Multi Images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Help

% cmenu = creer_menu_help(cmenu, parent1, msg{1}, 'xxxx.html'); % TODO

%% Sous-Menus

msg{2} = 'GP_ROI_MultiImages_CreateMask_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Cr�ation de masques', 'Create masks'));

msg{2} = 'GP_ROI_MultiImages_MaskImage_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Masquage � partir d''un masque commun', 'Masking using a common mask'));

msg{2} = 'GP_ROI_MultiImages_MaskAppairedImages_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Masquage de chaque image avec son mask associ�', 'Masking each image with its own mask'));
