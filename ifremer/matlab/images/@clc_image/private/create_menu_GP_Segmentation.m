function cmenu = create_menu_GP_Segmentation(~, baseMsg, cmenu)

parent0 = 'GP_Segmentation_MainMenu';
msg{1} = baseMsg;
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Segmentation', 'Segmentation'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Texture.html');

%% Segmentation par rapport � des courbes statistiques(Samantha Dugelay)

msg{2} = 'GP_Segmentation_Samantha_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Segmentation / courbes statistiqus', 'Segmentation / statistic curves'), ...
    'Separator', 'on');

%% Segmentation Master IMEN

cmenu = addMenu_L1_ImenMaster(cmenu, parent0, msg);

%% Segmentation PhD IMEN

cmenu = addMenu_L1_ImenPhD(cmenu, parent0, msg);

%% Homogeneisation

msg{2} = 'GP_Segmentation_Homogenisation_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Homog�n�isation', 'Homogenization'));



function cmenu = addMenu_L1_ImenMaster(cmenu, parent0, msg)

parent1 = 'GP_Segmentation_ImenMaster';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Segmentation post Master I. Karoui', 'I. Karoui Post Master Segmentation'));

cmenu = addMenu_L2_ImenMasterApprentissage(cmenu, parent1, msg);

msg{2} = 'GP_Segmentation_ImenMaster_Compute_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Segmentation', 'Segmentation'));

msg{2} = 'GP_Segmentation_ImenMaster_OriginalSize_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Redimentionnement � la taille de l''image d''origine', ...
    'Resize to original image size'));

msg{2} = 'GP_Segmentation_ImenMaster_ColorizeImage_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Superposition � une image d''intensit�', 'Colorize an Intensity Image'));

msg{2} = 'GP_Segmentation_ImenMaster_Smooth_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Lissage des contours', 'Contour smoothing'));

msg{2} = 'GP_Segmentation_ImenMaster_KullbackDistance_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Visualisation de la distance de Kullback', 'Plot Kullback distance'));

msg{2} = 'GP_Segmentation_ImenMaster_HaralickParameters_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Visualisation des param�tres texturaux', 'Plot textural parameters'));



function cmenu = addMenu_L2_ImenMasterApprentissage(cmenu, parent1, msg)

parent2 = 'GP_Segmentation_ImenMaster_Training';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Apprentissage', 'Training'));

msg{2} = 'GP_Segmentation_ImenMaster_Training_Compute_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Calcul', 'Compute'));

msg{2} = 'GP_Segmentation_ImenMaster_Training_Display_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Visualisation', 'Display'));

msg{2} = 'GP_Segmentation_ImenMaster_Training_Delete_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Effacer', 'Delete'));

msg{2} = 'GP_Segmentation_ImenMaster_Training_Export_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Exportation', 'Export'));

msg{2} = 'GP_Segmentation_ImenMaster_Training_Import_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Importation', 'Import'));

msg{2} = 'GP_Segmentation_ImenMaster_Training_Fusion_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Fusion', 'Fusion'));

msg{2} = 'GP_Segmentation_ImenMaster_Training_DisplayKullbackDist_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Visualisation des distances de Kullback', 'Display Kullback Distances'));


function cmenu = addMenu_L1_ImenPhD(cmenu, parent0, msg)

parent1 = 'GP_Segmentation_ImenPhd';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Segmentation post doctoral I. Karoui', 'I. Karoui Post PhD Segmentation'));

parent2 = 'GP_Segmentation_ImenPhd_Training';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Apprentissage', 'Training'));

msg{2} = 'GP_Segmentation_ImenPhd_Training_Compute_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Calcul', 'Compute'));

msg{2} = 'GP_Segmentation_ImenPhd_Compute_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Segmentation', 'Segmentation'));
