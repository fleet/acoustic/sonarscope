function cmenu = create_menu_GP_Shift(~, baseMsg, cmenu)

msg{1} = baseMsg;
parent0 = 'GP_Shift_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Recalage d''image', 'Image shift'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Image_shift.html');

%% Menus

msg{2} = 'GP_Shift_Offsets';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Manuel (offsets � rentrer)', 'Manual (offsets to be entered)'), ...
    'Separator', 'on');

msg{2} = 'GP_Shift_Manual_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par point� de 2 points', 'Selection of 2 points'));

msg{2} = 'GP_Shift_Longitude_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Longitude', 'Longitude'));

msg{2} = 'GP_Shift_Compute_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Par intercorr�lation', 'By cross correlation'));
