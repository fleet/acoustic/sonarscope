function cmenu = create_menu_GP_Synthesis(~, baseMsg, cmenu)

parent0 = 'GP_Synthesis_MainMenu';
msg{1} = baseMsg;
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Synthese d''image', 'Image synthesis'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Image_synthesis.html');

%% Menus

msg{2} = 'GP_Synthesis_InPainting_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Inpainting', ...
    'Separator', 'on');

msg{2} = 'GP_Synthesis_2DGauss_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Gaussiennes 2D', '2D Gaussians'));

msg{2} = 'GP_Composit2freq_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Composit from 2 freq');
