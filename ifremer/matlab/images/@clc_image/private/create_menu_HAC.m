function cmenu = create_menu_HAC(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'HAC';

parent1 = 'HAC_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".hac" ', '".hac" files (Fisheries)'));

%% Plot navigation

parent2 = 'HAC_Nav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'HAC_PlotNavigationAndTime_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'HAC_PlotNavigationAndSignal_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

%% GLOBE

parent2 = 'HAC_3D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'GLOBE', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'HAC_Export3DViewer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers 3DViewer', 'Export to GLOBE'));

%% Utilitaires

parent2 = 'TagHacUtilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'HAC_Preprocessing_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture (non obligatoire)', 'Just read once (not obligatory)'));

%{
    % TODO : params_HAC_CleanSignals et HAC_CleanSignals � finir
msg{3} = 'HAC_CleanSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Pr�traitement des signaux', 'Preprocessing signals'));
%}
    
msg{3} = 'HAC_CompressDirectories_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zippage des r�pertoires cache', 'Zip directories'));

msg{3} = 'HAC_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

