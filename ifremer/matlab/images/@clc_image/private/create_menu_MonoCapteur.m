function cmenu = create_menu_MonoCapteur(~, baseMsg, cmenu, parent0)

% parent1 = 'TagSurveyOther0';
% cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
%     'label', Lang('Autres donn�es', 'Other data'), ...
%     'ForegroundColor', 'b', 'Separator', 'On');

msg{1} = baseMsg;
msg{2} = 'MonoCapteur';

% TODO : � d�placer dans creer_menu_SEGY ???
% % % cmenu = ajouter(cmenu,  'tag', 'TagSurveyOtherSeismic', 'Parent', parent0, ...
% % %     'label', Lang('Bloc Sismique', 'Seismic 3D block'), 'Separator', 'Off');
% % % msg{2} = 'TagExport3DBlocSismiqueV1';
% % % cmenu = ajouter(cmenu, 'parent', 'TagSurveyOtherSeismic', 'tag', msg{2}, 'message', msg, ...
% % %     'label', Lang('Convertir pour GLOBE V1 (M�me structure que les �chogrammes WC)', 'Convert for GLOBE V1 (Same structure as WC Echograms)'));
% % % 
% % % msg{2} = 'TagExport3DBlocSismiqueV2';
% % % cmenu = ajouter(cmenu, 'parent', 'TagSurveyOtherSeismic', 'tag', msg{2}, 'message', msg, ...
% % %     'label', Lang('Convertir pour GLOBE V2', 'Convert for GLOBE V2'));
% TODO : � d�placer dans creer_menu_SEGY ???

% -----------------------------
parent1 = 'MonoCapteur_Other1_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Donn�es le long de la route', 'Data along navigation'), ...
    'Separator', 'Off');

msg{3} = 'MonoCapteur_plot_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Importation et visualisation dans une figure', 'Import and display in a figure'));

% -----------------------------
parent1 = 'MonoCapteur_Other2';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Positionnement de pr�l�vements', 'Seabed samplings'));

msg{3} = 'MonoCapteur_Prelevements_plot_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label',  Lang('Importation et visualisation dans une figure', 'Import and display in a figure'));

