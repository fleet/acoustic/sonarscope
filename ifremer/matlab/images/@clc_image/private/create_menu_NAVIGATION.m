function cmenu = create_menu_NAVIGATION(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'NAVIGATION';

parent1 = 'NAV_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers de navigation', 'Navigation files'));

%% Navigation

% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'CINNA_Nav', ...
%     'label', Lang('Navigation', 'Navigation'), ...
%     'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'NAV_PlotNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac�', 'Plot'));

msg{3} = 'NAV_EditNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition', 'Edit'));
