function cmenu = create_menu_NORBIT(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'NORBIT';

parent1 = 'NORBIT_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Norbit', 'Norbit'));

%% Norbit

% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'CINNA_Nav', ...
%     'label', Lang('Navigation', 'Navigation'), ...
%     'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'NORBIT_CompositMosaic_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Image composite', 'Composit image from frequencies'));
