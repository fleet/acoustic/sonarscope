function cmenu = create_menu_ODV(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'ODV';

parent1 = 'ODV_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".odv" ', '".odv" files (Open Data View Files)'));

%% Plot navigation

parent2 = 'ODV_Nav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ODV_PlotNavigationAndTime_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Tracé de la navigation', 'Plot Navigation'));

%% GLOBE

parent2 = 'ODV_3D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'GLOBE', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'ODV_Export3DViewer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers GLOBE', 'Export to GLOBE'));

%% Utilitaires

parent2 = 'TagODVUtilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'ODV_Preprocessing_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Prétraitement des fichiers .odv', 'Preprocessing .odv files'));

msg{3} = 'ODV_PreprocessingSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Prétraitement des signaux', 'Preprocessing signals'));

msg{3} = 'ODV_CompressDirectories_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zippage des répertoires cache', 'Zip directories'));

msg{3} = 'ODV_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des répertoires de Cache de SonarScope', 'SonarScope cache erase'));

