function cmenu = create_menu_OTUS(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'OTUS';

parent1 = 'OTUS_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers images OTUS', 'OTUS image files'));

%% Help

% cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'OTUS_files.html');

%% Localisation des images

parent2 = 'OTUS_Geolocalisation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Localisation des images', 'Geolocation of the images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'OTUS_CreationXmlFromNMEA';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation du fichier de navigation-attitude � partir des trames NMEA', 'Navigation-Attitude creation from NMEA datagrams'));

% msg{3} = 'OTUS_CreationXml';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Cr�ation des fichiers de description .xml � partir de fichiers excel', 'XML description files creation from Excel files'));

%% Compensations

parent2 = 'OTUS_Compensations';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Compensations', 'Compensations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'OTUS_CreateCompensatedImages';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Adapt�es � chaque image', 'Adapted on every image'));

parent3 = 'OTUS_CompensationsMeanVal';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Unique pour l''ensemble des images', 'Unique for all images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'OTUS_CalculCompensation';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de l''image moyenne', 'Compute the mean image'));

msg{3} = 'OTUS_CompensMeanImage';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Compensation par l''image moyenne', 'Compensation by the mean image'));

%% Export vers 3DV

parent2 = 'OTUS_Exports';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Exports', 'Exports'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'OTUS_Movie';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation d''un film', 'Make a movie'));

msg{3} = 'OTUS_ExportRawImages3DV';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export des images vers GLOBE', 'Export Images to GLOBE'));

%% Cr�ation de mosaiques

parent2 = 'OTUS_Mosaiques';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Mosaiques', 'Mosaics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'OTUS_Mosaic';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosa�que', 'Mosaic'));

msg{3} = 'OTUS_MosaiquesIndividuelles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation de mosaiques individuelles', 'Individual mosaics'));

