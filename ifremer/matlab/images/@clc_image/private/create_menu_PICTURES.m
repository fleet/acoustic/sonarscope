function cmenu = create_menu_PICTURES(this, baseMsg, cmenu, parent0)

parent1 = 'Pictures_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Images du fond marin', 'Seabed Pictures'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Help

% cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'PICTURES_files.html');

cmenu = create_menu_OTUS(  this, baseMsg, cmenu, parent0);
cmenu = create_menu_SCAMPI(this, baseMsg, cmenu, parent0);

%% Localisation des images

cmenu = create_menu_PICTURES_Common( this, baseMsg, cmenu, parent0);
