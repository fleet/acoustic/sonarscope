function cmenu = create_menu_PICTURES_Common(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'OTUS';

parent1 = 'OTUS_Common_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', 'OTUS/SCAMPI/PAGURE/HROV common tools');

%% Help

% cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'OTUS_files.html');

%% Localisation des images

msg{3} = 'OTUS_PlotNavigation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la nav et visualisation des images', 'Plot Nav'));

msg{3} = 'OTUS_PlotNavigationAndImages';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la nav et visualisation des images', 'Plot Nav & display images'));

msg{3} = 'OTUS_PlotNavAndImagesInGoogleEarth';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la nav et visualisation des photos dans GE', 'Plot Nav & display pictures in Google Earth'));
