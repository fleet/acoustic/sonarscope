function cmenu = create_menu_RAW(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'ExRaw'; % Renommer en RAW, attention, nombreux side effects possibles

parent1 = 'RAW_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".raw" (EK60, ...)', '".raw" files (EK60, ...)'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'SScDoc-Tutorial-RAW.html');

%% Bases

parent2 = 'RAW_Bases';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Bases', 'Bases'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RAW_Preprocessing_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture', 'Just read once'));

msg{3} = 'RAW_CleanSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�tection de la hauteur', 'Height detection'));

msg{3} = 'RAW_TideImport_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la mar�e', 'Tide import'));

%% Plot navigation

parent2 = 'RAW_Nav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RAW_PlotNavigationAndTime_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'RAW_PlotNavigationAndSignal_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'RAW_ImportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

msg{3} = 'RAW_EditNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition de la navigation', 'Edit navigation'));

%% GLOBE

parent2 = 'RAW_3D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'GLOBE', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RAW_Export3DViewer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers GLOBE', 'Export to GLOBE'));

%% Utilitaires

parent2 = 'RAW_Utilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

% msg{3} = 'RAW_Concatain';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Conversion de petits fichiers EK80', 'Concatanate small EK80 files'));

msg{3} = 'RAW_ConvertSsc2Hac_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Conversion au format HAC', 'Convert into HAC format'));

% msg{3} = 'RAW_TideImport';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Import de la mar�e', 'Tide import'));

msg{3} = 'RAW_CompressDirectories_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zippage des r�pertoires cache', 'Zip directories'));

msg{3} = 'RAW_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

msg{3} = 'RAW_ExtractSeabedBS_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Extraction du BS du fond', 'EK60 seabed backscatter extraction'));

