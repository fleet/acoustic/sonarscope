function cmenu = create_menu_RDF(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'RDF';

parent1 = 'RDF_MainMenu'; % Attention : red�fini plus loin : TODO
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers GeoAcoustics ".rdf"', '".rdf" files (GeoAcoustics)'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'SScDoc-Tutorial-RDF.html');

%% Navigation

parent2 = 'TagSonarRDFNav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RDF_PlotNavigationAndTime_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'RDF_PlotNavigationAndSignal_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'RDF_PlotNavigationAndCoverage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation et de la couverture sondeur', 'Plot Navigation & Coverage'));

msg{3} = 'RDF_ImportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

%% Preprocessing

parent2 = 'TagSonarRDFPreprocessing';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Pr�traitement et nettoyage', 'Preprocessing & cleaning data'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RDF_PreprocessingStep1_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 1 (int�ractive)', 'Step 1 (interactive)'));

msg{3} = 'RDF_PreprocessingStep2_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 2 (automome)', 'Step 2 (autonomous)'));

msg{3} = 'RDF_PreprocessingStep3_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 3 (automome)', 'Step 3 (autonomous)'));

msg{3} = 'RDF_PreprocessingStep4_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 4 (optionel)', 'Step 4 (optional)'));

%% Changements de g�om�trie

parent2 = 'TagSonarRDFTransGeometry';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Transformations g�om�triques', 'Geometric Transformations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RDF_PingSamples2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosa�que', 'Gridding'));

%% Utilitaires

parent2 = 'RDF_Utilities'; % TODO : rappel� plus loin : renommer parent2 en parent3, etc ...
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

cmenu = addMenu_L2_Utilities_CacheFilesRDF(cmenu, parent2, msg);

msg{3} = 'RDF_Preprocessing_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture (non obligatoire)', 'Just read once (not obligatory)'));

msg{3} = 'RDF_ResetMask_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�initialisation du masque', 'Reset Mask'));


parent3 = 'RDF_Summary';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('R�sum�', 'Summary'));

msg{3} = 'RDF_SummaryLines_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Profils', 'Lines'));

msg{3} = 'RDF_SummaryDatagrams_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Datagrammes', 'Datagrams'));

msg{3} = 'RDF_CompressDirectories';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zippage des r�pertoires cache', 'Zip directories'));

msg{3} = 'RDF_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

msg{3} = 'RDF_DefineInstallationParameters_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�finition des param�tres d''installation', 'Define the Installation parameters'));

%% TODO : pourrait �tre mutualis� avec SDF_ ...

parent2 = 'RDF_Utilities';
parent3 = 'RDF_ManageSignals';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Gestion des signaux', 'Manage signals'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RDF_BackupSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Sauver les signaux comme "Originaux"', 'Save signals as "Source ones"'));

msg{3} = 'RDF_RestoreSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�tablissement des signaux d''origine', 'Restore "Source signals"'));

%% RTK Data

parent3 = 'RDF_RTK';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent3, ...
    'label', Lang('RTK', 'RTK'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

% RDF_RTK_Pospac ????????????????????????????????????????????????????

msg{3} = 'RDF_RtkClean_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Contr�le de la hauteur RTK', 'Check RTK Height'));

msg{3} = 'RDF_RtkComputeTide_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la mar�e', 'Compute Tide'));

msg{3} = 'RDF_RtkEditTide_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition de la mar�e', 'Edit Tide'));

msg{3} = 'RDF_RtkCreateTide_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export de la mar�e', 'Export Tide'));

msg{3} = 'RDF_RtkPlot_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation des donn�es', 'Plot data'), ...
    'Separator', 'on');

msg{3} = 'RDF_RtkReset_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RAZ des donn�es', 'Reset data'));

%% Special mosaics

parent3 = 'RDF_SpecialMosaics';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent3, ...
    'label', Lang('Mosaiques sp�ciales', 'Special mosaics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'RDF_Mosaiques_Individuelles_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosaiques individuelles par profils', 'Individual Mosaics per lines'));

msg{3} = 'RDF_MosaiquesSidescan_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosaiques "sidescan style"', '"sidescan style" mosa�c'));


function cmenu = addMenu_L2_Utilities_CacheFilesRDF(cmenu, parent2, msg)
    
parent3 = 'RDFUtilitiesCacheOldFiles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
        'label', 'Change cache files');
%         'label', 'Cache for old files : Actions');

msg{3} = 'RDF_Cache1_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Convert XML-Bin to Netcdf'));

msg{3} = 'RDF_Cache2_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', 'Delete XML-Bin');

msg{3} = 'RDF_Cache3_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Convert XML-Bin to Netcdf & delete XML-Bin'));

% msg{3} = 'RDF_CacheErase_CI0';
% cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('TODO', 'Delete Netcdf & XML-Bin'));

% msg{3} = 'RDF_Cache7_CI0';
% cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('TODO', 'Test Netcdf compression factors'));   

