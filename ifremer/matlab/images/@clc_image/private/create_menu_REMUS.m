function cmenu = create_menu_REMUS(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'REMUS';

parent1 = 'REMUS_MainMenu';
cmenu = ajouter(cmenu, 'Parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers images REMUS', 'REMUS image files'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'ErMapper_files.html');

%% Localisation des images

msg{3} = 'REMUS_ConvertFileFormat';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Conversion de format rn .ers', 'Convert file format into .ers'));

%% Navigation

parent2 = 'TagErsNav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'REMUS_PlotNavTime';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

if ~isdeployed % Fonction non termin�e
    msg{3} = 'REMUS_PlotNavSignal';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));
end

msg{3} = 'REMUS_ImportNavigation';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

%% Exports

parent2 = 'TagErsExports';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Exports', 'Exports'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'REMUS_Export3DViever';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers GLOBE', 'Export to GLOBE'));

msg{3} = 'REMUS_ExportGoogleEarth';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers Google-Earth', 'Export to Google-Earth'));

msg{3} = 'REMUS_ExportCaraibes';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers Caraibes', 'Export to Caraibes'));

%% Transformations g�om�triques

parent2 = 'TagErsGeometry';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Transformations g�om�triques', 'Geometric Transformations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'REMUS_PingRange2PingAcrossDist';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'PingSamples, PingRange --> PingAcrossDist');

msg{3} = 'REMUS_PingAcrossDist2LatLong';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'SideScan sonar PingAcrossDist --> LatLong');

msg{3} = 'REMUS_MBES_PingBeam2LatLong';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'MBES PingBeam --> LatLong');

%% Utilitaires

parent2 = 'TagErsUtilitaires';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'REMUS_CleanSignals';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('V�rification des signaux', 'Check signals'));

msg{3} = 'REMUS_SignalFiltre';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'Filter a signal');
