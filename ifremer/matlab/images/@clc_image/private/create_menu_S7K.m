% Cr�ation du menu Survey S7K (Reson .s7k files)
% 
% Syntax
%   cmenu = creer_menu_Survey_S7K(this, baseMsg, cmenu)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%   cmenu   : Instance de clc_menu
%
% Output Arguments
%   cmenu   : Instance de clc_menu
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------
 
function cmenu = create_menu_S7K(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'S7K';

parent1 = 'S7K_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers Reson ".s7k" ', '".s7k" files (Reson)'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'Reson_files.html');

%% Survey report

cmenu = addMenu_L2_SurveyReport(cmenu, parent1, msg);

%% Summary

cmenu = addMenu_L2_Summary(cmenu, parent1, msg);

%% Plot navigation

cmenu = addMenu_L2_Navigation(cmenu, parent1, msg);

%% Transformations g�om�triques

cmenu = addMenu_L2_TransGeometry(cmenu, parent1, msg);

%% Data cleaning

cmenu = addMenu_L2_DataCleaning(cmenu, parent1, msg);

%% Utilitaires

cmenu = addMenu_L2_Utilities(cmenu, parent1, msg);
    
%% Menu Water-Column

cmenu = addMenu_L2_WaterColumn(cmenu, parent1, msg);

%% Patch test

cmenu = addMenu_L2_PatchTest(cmenu, parent1, msg);

%% Statistics

cmenu = addMenu_L2_Statistics(cmenu, parent1, msg);

%% Menu Sidescan

cmenu = addMenu_L2_Sidescan(cmenu, parent1, msg);

%% Special mosaics

cmenu = addMenu_L2_SpecialMosaics(cmenu, parent1, msg);

%% Calcul de la bathy

cmenu = addMenu_L2_SHOM(cmenu, parent1, msg);

%% Menu R&D

cmenu = addMenu_L2_RnD(cmenu, parent1, msg);


%% Summary

function cmenu = addMenu_L2_Summary(cmenu, parent1, msg)

parent2 = 'S7K_MainMenuSummary';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('R�sum�', 'Summary'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_Summary_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Profils', 'Lines'));


%% Plot navigation

function cmenu = addMenu_L2_Navigation(cmenu, parent1, msg)

parent2 = 'ResonS7k_Navigation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_PlotNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation seule', 'Plot Navigation only'));

msg{3} = 'S7K_PlotNavigationAndSignal_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'S7K_PlotNavigationAndCoverage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation et de la couverture sondeur', 'Plot Navigation & Coverage'));

msg{3} = 'S7K_ImportCaraibesNvi_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Importation d''une nav', 'Import a navigation'));

msg{3} = 'S7K_EditNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition de la navigation', 'Edit navigation'));


%% Transformations g�om�triques

function cmenu = addMenu_L2_TransGeometry(cmenu, parent1, msg)

parent2 = 'ResonS7k_TransGeometry';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Transformations g�om�triques', 'Geometric transformation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_PingBeam2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'PingBeam --> LatLong');

if ~isdeployed
    msg{3} = 'S7K_PingSamples2LatLong_CI1';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', 'PingSnippets --> LatLong (Mosaic snippets")');
    
    msg{3} = 'S7K_PingSnippets2PingSamples_CI1';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', 'PingSnippets --> PingSamples');
end

% % ATTENTION : la m�thode Reson_PingSamples2PingAcrossDist.m existe dans
% % C:\IfremerToolbox\ifremer\acoustics\sounder\Reson mais je je sais plus si
% % c'est du code mort : a r�gler
% msg{3} = 'SonarResonPingSamples2PingAcrossDist';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', 'PingSamples --> PingAcrossDist');


%% Survey report

function cmenu = addMenu_L2_SurveyReport(cmenu, parent1, msg)

parent2 = 'S7K_SurveyReport';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label',Lang('Rapport de campagne', 'Survey Report'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

baseMsg = msg{1};
cmenu = creer_menu_help(cmenu, parent2, baseMsg, 'SScDoc-Tutorial-ALL-SurveyReport.html', 'Tag', 'S7K');

msg{3} = 'S7K_SurveyReport_Create_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Create a survey report framework'));

%% Data

msg{3} = 'S7K_SurveyReport_Data';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, ...
    'label', Lang('TODO', 'Data'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_SurveyReport_Summary_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', '1 - Add Summary of lines'), ...
    'Separator', 'on');

msg{3} = 'S7K_SurveyReport_Navigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', '2 - Add Navigation plots'));

msg{3} = 'S7K_SurveyReport_SummaryAndNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Make 1 and 2 at once'));

%% DTM and Mosaic

msg{3} = 'S7K_SurveyReport_DTMAndMosaic';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, ...
    'label', Lang('TODO', 'DTM & Mosaic'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_SurveyReport_GlobalMosaic_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Compute a DTM & a Mosaic'), ...
    'Separator', 'on');

%% Add data

msg{3} = 'S7K_SurveyReport__AddData';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, ...
    'label', Lang('TODO', 'Import Images and figures'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

parent3 = 'S7K_SurveyReport__AddImage';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('TODO', 'Add the current image'));

msg{3} = 'S7K_SurveyReport_AddLatLongBathymetry_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Special Bathymetry'));

msg{3} = 'S7K_SurveyReport_AddLatLongReflectivity_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Special Reflectivity'));

msg{3} = 'S7K_SurveyReport_AddLatLongEchoIntegration_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Special Echo Integration'));

msg{3} = 'S7K_SurveyReport_AddFigures_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Add figures'));

%% Engineering

msg{3} = 'S7K_SurveyReport__Engineering';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, ...
    'label', Lang('TODO', 'Engineering'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_SurveyReport_PlotSignalsVsTime_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Plot Signals Vs Time'));

msg{3} = 'S7K_SurveyReport_PlotImageSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Plot Image and Signals'));

%% Utilities

msg{3} = 'S7K_SurveyReport__Utilities';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_SurveyReport_Extract_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Extract shortened version'));

msg{3} = 'S7K_SurveyReport_CleanDirectories_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Clean up subdirectories'));


%% Data cleaning

function cmenu = addMenu_L2_DataCleaning(cmenu, parent1, msg)

parent2 = 'S7K_DataCleaning';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Nettoyage de la donn�e', 'Data Cleaning'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_CleanBathyAndRange_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('A partir de l''analyse histogrammique de "Range, Depth & AcrossDist"', 'On "Range, Depth & AcrossDist" histogram analysis'));

msg{3} = 'S7K_CleanDetectionType_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('A partir du type de d�tection', 'On DetectionType'));

msg{3} = 'S7K_CleanBathy_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('A partir de la bathy', 'On Bathy'));

% Clean from DTM
cmenu = addMenu_L3_S7K_CleanFromDTM(cmenu, parent2, msg);

% Flags sur la r�flectivit�
cmenu = addMenu_L3_S7K_CleanReflectivity(cmenu, parent2, msg);

% Import flags from other softwares
cmenu = addMenu_L3_S7K_ImportBathyFlags(cmenu, parent2, msg);

% Gestion des masks
cmenu = addMenu_L3_S7K_GestionMasks(cmenu, parent2, msg);


function cmenu = addMenu_L3_S7K_CleanReflectivity(cmenu, parent2, msg)

parent3 = 'S7K_CleanReflectivity';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Sur la r�flectivit�', 'On Reflectivity'));

msg{3} = 'S7K_CleanReflectivityMeanPerPing_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Using reflectivity per ping'));

msg{3} = 'S7K_CleanReflectivityRAZ_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RAZ des flags', 'Reset flags'));


%% Utilitaires

function cmenu = addMenu_L2_Utilities(cmenu, parent1, msg)

parent2 = 'ResonS7k_Utilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_Visu_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation du contenu des datagrammes', 'Plot datagram contents'));
%     'label', Lang('Visualisation des datagrams autres que "depth" et "Seabed Image"', 'Datagram display other than "depth" and "seabed image"'));

msg{3} = 'S7K_Preprocessing_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture (non obligatoire)', 'Just read once (not obligatory)'));

msg{3} = 'S7K_CheckNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nettoyage de la navigation', 'Navigation cleaning'));

% ----------------------------------------------------------
cmenu = addMenu_L3_Utilities_Datagrams(cmenu, parent2, msg);

parent3 = 'ResonS7k_Export_NavOrAtt';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Export Navigation ou Attitude', 'Navigation or Attitude export'));

msg{3} = 'S7K_ExportCaraibesNvi_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(  'Extraction de la navigation .s7k vers un fichier Caraibes .nvi"', ...
                    'Extract Navigation .s7k data in a Caraibes .nvi file'));

msg{3} = 'S7K_ExportAttitude_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(  'Extraction de l''attitude .s7k vers un fichier Ascii .txt"', ...
                    'Extract Attitude .s7k data in a .txt file'));

msg{3} = 'ResonS7K_DeleteCache';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label',  Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

% --------------------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent2, 'tag', 'S7KRobots', ...
        'label', 'Robots for running surveys', ...
        'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_Robot_CopyAndProcessSounderOutputFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pr�traitement des fichiers en temps quasi r�el', 'Preprocess files in quasi real time'));

msg{3} = 'S7K_Robot_MoveFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�placement des fichiers pr�trait�s', 'Move preprocessed files and directories'));

msg{3} = 'S7K_Robot_PlotKMZ_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Revisualisation des donn�es dans Google-Earth', 'Replay data in Google Earth'));


function cmenu = addMenu_L3_Utilities_Datagrams(cmenu, parent2, msg)

parent3 = 'S7K_Datagramms';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Travail sur les datagrammes', 'Work on datagrams'));

msg{3} = 'S7K_SplitFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�coupe de fichiers .s7k"', 'Split .s7k files'));

msg{3} = 'S7K_SplitFrequencies_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�coupe de fichiers .s7k par fr�quences"', 'Split .s7k frequencies'));

msg{3} = 'S7K_AllMergeFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', '{.s7k1, ..., .s7kn} -> .s7k');

%{
msg{3} = 'ALL_DatagramsSuppress_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Suppression de certains types de datagrammes', 'Remove some types of datagrams'));
%}
    
    
 %% Menu Water-Column
   
function cmenu = addMenu_L2_WaterColumn(cmenu, parent1, msg)

parent2 = 'ResonS7k_WaterColumn';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Colonne d''eau', 'Water Column'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_WC_ProcessEchograms_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'WC Echogrammes polaires en g�ometrie {Depth,AcrossDist}', ...
    'WC polar echograms in {Depth,AcrossDist} geometry'));

msg{3} = 'S7K_WC_ProcessEchogramsMatrix_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�er une matrice 3D (Depth, Across distance, Ping)', 'Create 3D Matrix (Depth, Across distance, Ping#)'));

%{
% Traitement sp�cifique donn�es USAN � bord du PP?
msg{3} = 'S7K_ComputePositionGazPlumes_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul des coordonn�es des panaches', 'Compute gaz plumes position'));
%}

msg{3} = 'S7K_WC_ConvertSsc2Hac_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Conversion au format HAC', 'Convert into HAC format'));

% msg{3} = 'ResonS7k_WCGroupEchograms';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Regroupement des fichiers d''Echogrammes', 'Group Echogram files'));

msg{3} = 'S7K_WC_ProcessEchogramsPhase_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation des "Echogrammes" de phase pour GLOBE', 'Create "Phase Echograms" for GLOBE'));

% Echo Integration in Water Column
cmenu = addMenu_L3_S7K_EchoIntegration(cmenu, parent2, msg);


%% Patch test

function cmenu = addMenu_L2_PatchTest(cmenu, parent1, msg)

parent2 = 'ResonS7k_PatchTest';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Patch test', 'Patch test'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_PatchTestRoll_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Roulis', 'Roll'));


%% Statistics

function cmenu = addMenu_L2_Statistics(cmenu, parent1, msg)

parent2 = 'ResonS7k_Statistics';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Statistiques', 'Statistics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_Statistics_BathyMNT_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Bathy vs Reference DTM'));


%% Menu Sidescan

function cmenu = addMenu_L2_Sidescan(cmenu, parent1, msg)

parent2 = 'ResonS7k_Sidescan';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Colonne d''eau du "Sidescan"', '"Sidescan" water column'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_CleanSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pr�traitement des signaux', 'Preprocessing Signals'));

msg{3} = 'S7K_WC_ExportWCSidescan3DViewer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers GLOBE', 'Export to GLOBE'));


%% Special mosaics

function cmenu = addMenu_L2_SpecialMosaics(cmenu, parent1, msg)

parent2 = 'ResonS7k_SpecialMosaics';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Mosaiques sp�ciales', 'Special mosaics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_MosaiquesIndividuelles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosaiques individuelles par profils', 'Individual Mosaics per lines'));


%% Calcul de la bathy

function cmenu = addMenu_L2_SHOM(cmenu, parent1, msg)

parent2 = 'ResonS7k_SHOM';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('SHOM', 'SHOM'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_MosaiquesComputeBathy_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la bathym�trie', 'Compute Bathymetry'));

msg{3} = 'S7K_MosaiquesNewNavFromDrift_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODo', 'Fix Rascas Navigation'));


%% Menu R&D

function cmenu = addMenu_L2_RnD(cmenu, parent1, msg)

if ~isdeployed
    parent2 = 'ResonS7k_RnD';
    cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
        'label', 'R&&D', ...
        'Separator', 'on', 'ForegroundColor', 'b');
    
    msg{3} = 'S7K_RnD_BottomDetectionSynthese_CI0';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('D�tection du fond', 'Bottom Detection'));
    
    msg{3} = 'S7K_RnD_CompleteFilesWithPDS_CI0';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('Fusion de fichiers Center et PDS', 'Fusion of Center & PDS files'));
    
    msg{3} = 'S7K_CreationSidescan_CI0';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('D�tection d''une image sidescan', 'Creation of a sidescan image'));
    
    msg{3} = 'S7K_CompareResultsBDA_CI0';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('Comparaison de r�sultats du BDA', 'Comparison of BDA results'));
end


function cmenu = addMenu_L3_S7K_ImportBathyFlags(cmenu, parent2, msg)

parent3 = 'S7K_ImportBathyFlags';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Importer des flags depuis un autre logiciel', 'Import flags from other softwares'));

baseMsg = msg{1};
cmenu = creer_menu_help(cmenu, parent3, baseMsg, 'SScDoc-Tutorial-ALL-DataCleaning.html', 'Tag', 'S7K');

msg{3} = 'S7K_ImportBathyFromCaris_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('CARIS', 'CARIS'));

msg{3} = 'S7K_ImportBathyFromCaraibes_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('MBG de Caraibes', 'Caraibes MBG'));

msg{3} = 'S7K_ImportBathyFromMBSystem_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('MB-System', 'MB-System'));


function cmenu = addMenu_L3_S7K_GestionMasks(cmenu, parent2, msg)

parent3 = 'S7K_Mask';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Gestion des mask', 'Manage Masks'));

msg{3} = 'S7K_ResetMask_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RAZ des masques', 'Reset masks'));


function cmenu = addMenu_L3_S7K_CleanFromDTM(cmenu, parent2, msg)

parent3 = 'S7K_CleanFromDTM';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Par comparaison � un MNT existant', 'By comparison to a DTM'));

msg{3} = 'S7K_CleanFromDTMInteractif_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Interactif', 'Manual'));

msg{3} = 'S7K_CleanFromDTMThreshold_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Par seuillage', 'Using a threshold'));


function cmenu = addMenu_L3_S7K_EchoIntegration(cmenu, parent2, msg)

parent3 = 'TagEchoIntegrationS7k';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Echo-int�gration', 'Echo Integration'));

% -------------------------------------------------------------------------

%{
% TODO : ce n'est qu'un d�but de mise en place. L'int�r�t de ce modile est
% discutable. Il serait certainement pr�f�rable de le g�n�raliser � partir
% d'un cache commun entre KM et S7K

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'S7K_WaterColumnEISampleBeam', ...
    'label', Lang('TODO', 'SampleBeam echograms ->  EI signal vs ping'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_WC_EI_SampleBeam_SignalVsPings_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Compute EI value per ping'));

msg{3} = 'S7K_WC_RazFlagPings_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Reset ping flags'));
%}

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'S7K_WaterColumnEIPingBeam2PingAcrossDist', ...
    'label', Lang('TODO', 'DepthAcrossDist echograms -> PingAcrossDist'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_WC_EchoIntegration_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'Calcul EI au dessus du sp�culaire (ASC)', ...
    'Compute EI above the specular echo circle (ASC)'));

msg{3} = 'S7K_WC_EchoIntegrationOverSeabed_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang(...
    'Calcul EI au dessus du fond (ASF)', ...
    'Compute EI Above the SeaFloor (ASF)'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'S7K_WaterColumnEIPingPingAcrossDist2LatLong', ...
    'label', Lang('TODO', 'PingAcrossDist --> LatLong'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_WC_Mosaic_EI_SSc_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation d''une mosaique', ...
    'Create a mosaic'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'S7K_WaterColumnEIPingAcrossDist', ...
    'label', Lang('TODO', 'PingAcrossDist ->  EI signal vs ping'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_WC_EI_PingAcrossDist_SignalVsPings_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Compute EI value per ping'));

% -------------------------------------------------------------------------

cmenu = ajouter(cmenu, 'parent', parent3, 'tag', 'S7K_WC_PlotNav_EI_SScPlus', ...
    'label', Lang('Outils secondaires', 'Secondary tools'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'S7K_WC_EI_MosFromMovies_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation de mosa�ques d''�cho-int�gration par faisceauxde MOvies3D', ...
    'Create a mosaic from Movies3D echo integration per beam'));
