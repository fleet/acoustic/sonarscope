function cmenu = create_menu_SAR(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SAR';

parent1 = 'SAR_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".im" ', '".im" files (SAR)'));

%% Navigation

parent2 = 'SAR_Navigation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SAR_PlotNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'SAR_PlotNavigationCoverage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation et de la couverture sondeur', 'Plot Navigation & Coverage'));

msg{3} = 'SAR_PlotSignalOnNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'SAR_ImportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

%% Grids

parent2 = 'TagSonarSARTransGeometry';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Transformations g�om�triques', 'Geometric Transformations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SAR_PingSamples2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosa�que de la r�flectivit�', 'Grid sidescan'));

msg{3} = 'SAR_CreatePseudoDTM_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('MNT � partir de l''immersion et de la hauteur', 'DTM using Immersion and Height'));

%% Exports GLOBE

parent2 = 'SAR_3D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'GLOBE', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SAR_SubbottomExport3DViewer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export Sondeur de S�diments d''Eau vers GLOBE', 'Export Subbottom to GLOBE'));

msg{3} = 'SAR_WC_Export3DViewer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export Colonne d''Eau vers GLOBE', 'Export WaterColumn to GLOBE'));

%% Utilities

parent2 = 'TagSarUtilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'SAR_ReadOnce_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture (non obligatoire)', 'Just read once (not obligatory)'));

msg{3} = 'SAR_PlotSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� des signaux', 'Plot signals'));

msg{3} = 'SAR_CleanSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pr�traitement des signaux', 'Preprocessing signals'));

msg{3} = 'SAR_Summary_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�sum� des fichiers .im', 'Summary .im files'));

msg{3} = 'SAR_BackupSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Sauver les signaux comme "Originaux"', 'Put signals as "source" ones'));

msg{3} = 'SAR_RestoreSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�tablissement des signaux d''origine', 'Restore source signals'));

msg{3} = 'SAR_ZipDirectories_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Compression des r�pertoires cache', 'Zip directories'));

msg{3} = 'SAR_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

%% Mosaiques particuli�res

parent2 = 'SAR_Mosaiques8Speciales';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Mosaiques sp�ciales', 'Special mosaics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SAR_Mosaiques_Individuelles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2,  'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mosaiques individuelles par fichier', 'Individual Mosaics per file'));
