function cmenu = create_menu_SCAMPI(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SCAMPI';

parent1 = 'SCAMPI_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers images SCAMPI', 'SCAMPI image files'));

%% Localisation des images

parent2 = 'SCAMPI_Geolocalisation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Localisation des images', 'Geolocation of the images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SCAMPI_CreationXmlFromNMEA';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation du fichier de navigation-attitude � partir des trames NMEA', 'Navigation-Attitude creation from NMEA datagrams'));

%% Compensations

parent2 = 'SCAMPI_Compensations';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Compensations', 'Compensations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SCAMPI_CreateCompensatedImages';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Adapt�es � chaque image', 'Adapted on every image'));

parent3 = 'SCAMPI_CompensationsMeanVal';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Unique pour l''ensemble des images', 'Unique for all images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SCAMPI_CalculCompensation';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de l''image moyenne', 'Compute the mean image'));

msg{3} = 'SCAMPI_CompensMeanImage';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Compensation par l''image moyenne', 'Compensation by the mean image'));

%% Exports

parent2 = 'SCAMPI_Exports';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Exports', 'Exports'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SCAMPI_Movie';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation d''un film', 'Make a movie'));

msg{3} = 'SCAMPI_ExportLatLonImages3DV';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export des mosa�ques vers GLOBE', 'Export Mosaics to GLOBE'));

if ~isdeployed
    % TODO : les images se retrouvent en Afrique et les profondeurs ne sont
    % pas g�r�es. Comment� pour le moment
    %{
    msg{3} = 'SCAMPI_ExportRawImages3DV';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('Export des images vers GLOBE', 'Export Images to GLOBE'));
    %}
end

%% Cr�ation de mosaiques

parent2 = 'SCAMPI_Mosaiques';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Mosaiques', 'Mosaics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SCAMPI_Mosaic';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Tuiles', 'Tiles'));

msg{3} = 'SCAMPI_MosaiquesIndividuelles';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Individuelles', 'Individual'));
