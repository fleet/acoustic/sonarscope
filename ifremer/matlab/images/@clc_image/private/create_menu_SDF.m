function cmenu = create_menu_SDF(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SDF';

parent1 = 'SDF_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers Klein ".sdf"', '".sdf" files (Klein)'));

%% Navigation

parent2 = 'TagSonarSDFChecks';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('V�rification des signaux', 'Check signals'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SDF_CheckHeights_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('V�rification de la hauteur', 'Check Heights'));

msg{3} = 'SDF_CheckFishNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('V�rification de la navigation du poisson', 'Check Fish navigation'));

msg{3} = 'SDF_CheckShipNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('V�rification de la navigation du bateau et des signaux', 'Check Ship navigation & signals'));

msg{3} = 'SDF_ComputeFishNavFromShipNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la navigation du poisson � partir de celle du bateau', 'Compute Fish nav from Ship nav'));

%% Navigation

parent2 = 'TagSonarSDFNav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SDF_PlotNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'SDF_PlotNavigationAndCoverage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation et de la couverture sondeur', 'Plot Navigation & Coverage'));

msg{3} = 'SDF_PlotNavigationAndSignal_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'SDF_ImportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

msg{3} = 'SDF_EditNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition de la navigation', 'Edit Navigation'));

%% Transformation g�om�triques

parent2 = 'TagSonarSDFTransGeometry';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Transformations g�om�triques', 'Geometric Transformations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SDF_PingSamples2LatLong_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('PingSample --> LatLong', 'PingSample --> LatLong'));

msg{3} = 'SDF_PingSamples2PingAcrossDist_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('PingSample --> PingAcrossDist', 'PingSample --> PingAcrossDist'));

%% Utilities

parent2 = 'TagSonarSDFUtilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SDF_ReadOnce_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture (non obligatoire)', 'Just read once (not obligatory)'));

% msg{3} = 'SDF_TideImport_CI0';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Import de la mar�e', 'Tide import'));

msg{3} = 'SDF_PlotSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation des signaux', 'Plot signals'));

msg{3} = 'SDF_Summary_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�sum�', 'Summary'));

parent3 = 'SDF_ManageSignals';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Gestion des signaux', 'Manage signals'));

msg{3} = 'SDF_BackupSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, 'label', 'Backup signals');

msg{3} = 'SDF_RestoreSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, 'label', 'Restore signals');

%% Utilities

% Work on Datagrams
if ~isdeployed
    parent3 = 'SDF_Datagrams';
    cmenu = ajouter(cmenu, 'parent', parent2, 'tag',parent3, ...
        'label', Lang('Travail sur les datagrammes', 'Work on datagrams'));

    msg{3} = 'SDF_DatagramsSplit_CI0';
    cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('En cours de d�v - D�coupe d''un fichier .sdf', 'In Work -- Split (chop) .sdf file'));
end

msg{3} = 'SDF_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

msg{3} = 'SDF_Rectification_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Recalage la navigation / mosa�que de r�f�rence', 'Correction of the navigation / Reference mosaic'));

%% Special mosaics

parent2 = 'SDF_SpecialMosaics';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Mosaiques sp�ciales', 'Special mosaics'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SDF_Mosaiques_Individuelles_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mosaiques individuelles par profils', 'Individual Mosaics per lines'));

