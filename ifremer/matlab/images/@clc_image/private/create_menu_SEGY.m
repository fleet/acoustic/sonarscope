function cmenu = create_menu_SEGY(~, baseMsg, cmenu, parent0)
 
msg{1} = baseMsg;
msg{2} = 'SEGY';

parent1 = 'SEGY_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".seg" ', '".seg" files (Seismics & sub-bottom)'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'SScDoc-Tutorial-SEG.html');

%% Bases

parent2 = 'SEGY_Bases';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Bases', 'Bases'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SEGY_ReadOnce_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture', 'Just read once'));

msg{3} = 'SEGY_ProcesChirp_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'Chirp processing'));

msg{3} = 'SEGY_CleanSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�tection de la hauteur', 'Height detection'));

msg{3} = 'SEGY_TideImport_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la mar�e', 'Tide import'));

%% Navigation

parent2 = 'SEGY_Nav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Navigation', 'Navigation'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

% msg{3} = 'SEGY_PreprocessingSignals';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Pr�traitement des signaux', 'Preprocessing Signals'));

msg{3} = 'SEGY_PlotNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'SEGY_PlotSignalOnNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'SEGY_ImportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

msg{3} = 'SEGY_EditNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Edition de la navigation', 'Edit navigation'));

%% Exports GLOBE

parent2 = 'SEGY_3D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'GLOBE', ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SEGY_Export3DViewer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers GLOBE', 'Export to GLOBE'));

%% Utilities

parent2 = 'TagSEGYUtilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'SEGY_PlotSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation des signaux', 'Plot signals'));

msg{3} = 'SEGY_CompressDirectories_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zippage des r�pertoires cache', 'Zip directories'));

msg{3} = 'SEGY_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

%% Export de Blocs

parent2 = 'SEGY_SurveyOtherSeismic';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Bloc Sismique', 'Seismic 3D block'), ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'SEGY_Export3DBlocSismiqueV1_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Convertir pour GLOBE V1 (M�me structure que les �chogrammes WC)', 'Convert for GLOBE V1 (Same structure as WC Echograms)'));

msg{3} = 'SEGY_Export3DBlocSismiqueV2_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Convertir pour GLOBE V2', 'Convert for GLOBE V2'));
