function cmenu = create_menu_SP_AireInso(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SP_AireInso';

%% MEnu

parent1 = 'SP_AireInso_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Aire insonifiée', 'Insonified area'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'Insonified_area.html');

%% Sous-menus

% ---------------------------
parent2 = 'SP_AireInso_Niveau';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Niveaux', 'Levels'));

msg{3} = 'SP_AireInso_Constructeur_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RT (Real Time)', 'RT (Real Time)'));

msg{3} = 'SP_AireInso_Ifremer_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('SSc', 'SSc'));

% ---------------------------------
parent2 = 'SP_AireInso_Compensation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Compensation', 'Compensation'));

msg{3} = 'SP_AireInso_NoCompens_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Aucune', 'None'));

msg{3} = 'SP_AireInso_CompensConstructeur_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RT (Real Time)', 'RT (Real Time)'));

msg{3} = 'SP_AireInso_CompensIfremer_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('SSc', 'SSc'));
