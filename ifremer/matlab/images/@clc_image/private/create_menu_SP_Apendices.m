function cmenu = create_menu_SP_Apendices(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_Apendices';

parent0 = msg{2};
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Annexes', 'Apendices'));

msg{3} = 'SP_Apendices_GeographicSituation_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Situation geographique', 'Geographical situation'));

msg{3} = 'SP_Apendices_Bathycelerimetrie_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Bathycelerimetrie', 'Sound speed profile'));

msg{3} = 'SP_Apendices_CreateDefaultSalinityFile_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�finir un fichier "DefaultSalinity.txt"', 'Define a "DefaultSalinity.txt" file'));

msg{3} = 'SP_Apendices_Fresnel_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zone de fresnel, champ lointain', 'Fresnel zone, farfield'));

msg{3} = 'SP_Apendices_ComputeHeading_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul du cap', 'Compute heading'));

msg{3} = 'SP_Apendices_ImportNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Importation de la navigation', 'Import Navigation'));

msg{3} = 'SP_Apendices_PlotNav_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trace de la navigation', 'Plot Navigation'));

msg{3} = 'SP_Apendices_ImportTide_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Importation de la mar�e', 'Tide import'));

msg{3} = 'SP_Apendices_MapPropagationAcoustique_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mapmonde des param�tres acoustiques', 'Map of acoustic parameters'));

msg{3} = 'SP_Apendices_ExportSignaux_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Exportation des signaux', 'Export signals'));

msg{3} = 'SP_Apendices_PlotWCFootprint_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de l''empreinte du ping', 'Plot ping footprint'));

msg{3} = 'SP_Apendices_Pames_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', 'PAMES');

msg{3} = 'SP_Apendices_plotGeographicPositionsInGoogleEarth_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export de points en Google Earth', 'Export data points into Google Earth'));

msg{3} = 'SP_Apendices_CreateMasksArounfGeographicPoints_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation de masques autour de coordonn�es g�ographiques', 'Create masks around geographic points'));
