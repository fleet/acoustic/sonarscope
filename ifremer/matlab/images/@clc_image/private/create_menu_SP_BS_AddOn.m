function cmenu = create_menu_SP_BS_AddOn(this, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SP_BS_AddOn';

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'SonarSonCalib_AddOn', ...
    'label', Lang('En plus', 'Add ons'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SP_BS_KMSpecularCompensation_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Compensation du spéculaire Kongsberg', 'Kongsberg Specular compensation'));

msg{3} = 'SP_BS_KMSpecularCompensationAngle_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Estimation de l''angle KM : acosd(r / rn)', 'Kongsberg angle estimation : acosd(r / rn)')); %, 'Separator', 'on'); % , 'ForegroundColor', 'b'
