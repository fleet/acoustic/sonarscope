function cmenu = create_menu_SP_BS_Analysis(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_BS_Analysis';

parent0 = 'SP_BS_Analysis_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('BS analysis', 'BS analysis'));

%% Courbes

msg{3} = 'SP_BS_Calcul_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul', 'Compute'));

msg{3} = 'SP_BS_Plot_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation', 'Display'));

msg{3} = 'SP_BS_Nettoyage_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nettoyage', 'Cleaning'));

msg{3} = 'SP_BS_Filter_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Filtrage', 'Filtre'));

msg{3} = 'SP_BS_Delete_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacer', 'Delete'));

msg{3} = 'SP_BS_Export_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Exportation', 'Export'));

msg{3} = 'SP_BS_Import_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Importation', 'Import'));

%{
    % Comment� par JMA le 05/11/2018 car partie correspondante comment�e dans callback_SP_BS_Analysis
msg{3} = 'SP_BS_Adjust_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Modelisation', 'Modeling'));
%}

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'SP_BS_AddOn', ...
    'label', Lang('En plus', 'Add ons'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SP_BS_ImageSynthesis_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Synth�se d''image de BS', 'BS image Syntehis'));
