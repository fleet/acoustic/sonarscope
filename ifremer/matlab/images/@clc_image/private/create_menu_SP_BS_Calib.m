function cmenu = create_menu_SP_BS_Calib(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SP_BS_Calib';

%% Menu

parent1 = 'SP_BS_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Retrodiffusion', 'Backscatter'));

%% Help

% TODO : retrouver la page (BSBackscatter.html dans .adoc !!!)
% cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'xxxx.html');

%% Sous-menus

% Niveaux

cmenu = addMenu_L2_Backscatter_Levels(cmenu, parent1, msg);

% Compensation

cmenu = addMenu_L2_Backscatter_Compensation(cmenu, parent1, msg);



function cmenu = addMenu_L2_Backscatter_Levels(cmenu, parent1, msg)

parent2 = 'SP_BS_Levels';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Niveaux', 'Levels'));

%% Curves

cmenu = addMenu_L3_Backscatter_Levels_Curves(cmenu, parent2, msg);

%% Image

cmenu = addMenu_L3_Backscatter_Levels_Image(cmenu, parent2, msg);


function cmenu = addMenu_L3_Backscatter_Levels_Curves(cmenu, parent2, msg)

parent3 = 'SP_BS_Curves';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Courbes', 'Curves'));

msg{3} = 'SP_BS_CurvesPartial_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Belle Image', 'Flattened Image'));

msg{3} = 'SP_BS_CurvesFull_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Compensation totale', 'Full compensation'));


function cmenu = addMenu_L3_Backscatter_Levels_Image(cmenu, parent2, msg)

parent3 = 'SP_BS_Image';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Image', 'Image'));

parent4 = 'SP_BS_Level';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Belle Image', 'Flattened Image'));

parent5 = 'SP_BS_Level_Lambert';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', parent5, ...
    'label', Lang('Lambert', 'Lambert'));

msg{3} = 'SP_BS_Level_LambertKM_CI1';
cmenu = ajouter(cmenu, 'parent', parent5, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Kongsberg', 'Kongsberg'));

msg{3} = 'SP_BS_Level_LambertAngles_CI1';
cmenu = ajouter(cmenu, 'parent', parent5, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Angles', 'Angles'));

msg{3} = 'SP_BS_Level_Lurton_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Lurton', 'Lurton'));

% ---------------------------
parent4 = 'SP_BS_Level_Full';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Compensation totale', 'Full compensation'));

msg{3} = 'SP_BS_Level_Func_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SP_BS_Level_Table_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Table', 'Table'));


function cmenu = addMenu_L2_Backscatter_Compensation(cmenu, parent1, msg)

parent2 = 'SP_BS_Compens';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Compensation', 'Compensation'));

msg{3} = 'SP_BS_Compens_None_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Aucune', 'None'));

% ------------------------------------
parent3 = 'SP_BS_FullCompens_Partial';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Belle Image', 'Flattened Image'));

parent4 = 'SP_BS_Compens_Lambert';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Lambert', 'Lambert'));

msg{3} = 'SP_BS_Compens_LambertKM_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Kongsberg', 'Kongsberg'));

msg{3} = 'SP_BS_Compens_LambertAngles_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Angles', 'Angles'));

msg{3} = 'SP_BS_Compens_Lurton_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Lurton', 'Lurton'));

% -----------------------------
parent3 = 'SP_BS_FullCompens_Full';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Compensation totale', 'Full compensation'));

msg{3} = 'SP_BS_FullCompens_Func_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SP_BS_FullCompens_Table_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));
