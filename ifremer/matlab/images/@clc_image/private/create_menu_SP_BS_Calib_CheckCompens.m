function cmenu = create_menu_SP_BS_Calib_CheckCompens(this, baseMsg, cmenu, parent0) %#ok

msg{1} = baseMsg;
msg{2} = 'SP_BS_Calib_CheckCompens';

str1 = 'V�rification des compensations';
str2 = 'Check Compensations';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'SP_BS_CheckCompensations', ...
    'label', Lang(str1,str2), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SP_BS_CheckCompensDiagTx_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Compensation de la Directivit�', 'Directivity Compensation'));

msg{3} = 'SP_BS_CheckFullCompens_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Compensation de la Directivit� et du BS', 'BS & Directivity Compensation'));

msg{3} = 'SP_BS_CheckCompens_SignalLevel_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Niveau de signal re�u', 'Received signal level'));
