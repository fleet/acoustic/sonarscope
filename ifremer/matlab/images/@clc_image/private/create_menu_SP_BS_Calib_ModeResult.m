function cmenu = create_menu_SP_BS_Calib_ModeResult(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SP_BS_Calib_ModeResult';

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, ...
    'label', Lang('Mode resultat MBES', 'Result Mode MBES'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Step 1

msg{3} = 'SP_BS_Calib_ModeResult_BSConstructeur_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 1 : Get BS Image', 'Step 1 : Get BS Image'));

%% Step 2

cmenu = addMenu_L1_CalibModeResul_Step2(cmenu, parent0, msg);

%% Step 3

msg{3} = 'SP_BS_Calib_ModeResult_DiagTx_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 3 : Get DiagTx Image', 'Step 3 : Get DiagTx Image'));

%% TxBeams diagrams

cmenu = addMenu_L1_CalibModeResul_Step4(cmenu, parent0, msg);


function cmenu = addMenu_L1_CalibModeResul_Step2(cmenu, parent0, msg)

parent1 = 'SP_BS_Calib_ModeResult_Step2';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Etape 2 : Estimation de la r�flectivit� angulaire', 'Step 2 : BS estimation'));

msg{3} = 'SP_BS_Calib_ModeResult_ComputeAndModel_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 2 a) : Calcul et Mod�lisation', 'Step 2 a) : Compute & Model'));

msg{3} = 'SP_BS_Calib_ModeResult_Export2_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 2 b) : Exportation BS', 'Step 2 b) : Export'));

% --------------------------------------------------------------------------------------
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'SP_BS_Calib_ModeResult_Utilities', ...
    'label', Lang('Autres', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

parent2 = 'SP_BS_Calib_ModeResult_ImportModel';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag',parent2, ...
    'label', Lang('Importer une mod�lisation', 'Import a Model'));

msg{3} = 'SP_BS_Calib_ModeResult_ImportModelMat_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fichier de courbes SSc (.mat)', 'SSc curve file (.mat)'));

msg{3} = 'SP_BS_Calib_ModeResult_ImportModelParameters_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Param�tres d''un mod�le', 'Model Parameters'));

msg{3} = 'SP_BS_Calib_ModeResult_ImportExternalMeasurements_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mesures externes', 'External measurements'));

msg{3} = 'SP_BS_Calib_ModeResult_RedoModel_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Refaire la mod�lisation', 'Redo Model'));

msg{3} = 'SP_BS_Calib_ModeResult_RedoModelAllCurvesTogether_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Refaire la mod�lisation � partir d''un ensemble de courbes', 'Redo Model from several curves'));

msg{3} = 'SP_BS_Calib_ModeResult_ExtractResiduals_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Extraction des r�sidus', 'Extract the residuals'));


function cmenu = addMenu_L1_CalibModeResul_Step4(cmenu, parent0, msg)

parent1 = 'SP_BS_Calib_ModeResult_Step4';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Etape 4 : Estimation de DiagTx', 'Step 4 : DiagTx Estimation'));

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxComputeAndModel_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 4 a) : Calcul et Mod�lisation', 'Step 4 a) : Compute & Model'));

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxExport2_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Etape 4 b) : Exportation', 'Step 4 b) : Export'));

% --------------------------------------------------------------------------------------------
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'SP_BS_Calib_ModeResult_DiagTXUtilities', ...
    'label', Lang('Autres', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxImportModel_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Importer une mod�lisation', 'Import a Model'));

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxRedoModel_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Refaire la mod�lisation', 'Redo Model'));

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxRedoModelAllCurvesTogether_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Refaire la mod�lisation � partir d''un ensemble de courbes', 'Redo Model from several curves'));

% -----------------------------------------------------------------------------------------
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'SP_BS_Calib_ModeResult_DiagTXBSCorr', ...
    'label', Lang('BSCorr', 'BSCorr'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxExportBSCorr_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Exportation du BSCorr.txt (Soundeurs KM)', 'Export BSCorr.txt (KM Sounders)'));

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxImportBSCorr_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Impotation d''un BSCorr.txt (Soundeurs KM)', 'Import a  BSCorr.txt (KM Sounders)'));

msg{3} = 'SP_BS_Calib_ModeResult_DiagTxComputeAndModelFactory_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mod�lisation avec formules construceteur (mode expert)', 'Compute new factory models (expert mode)'));

msg{3} = 'SP_BS_Calib_ModeResult_CreateEmptyEM304Bscorr_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Cr�ation d''un fichier BSCorr EM304 vide', 'Create an empty EM304 BScor.txt'));

msg{3} = 'SP_BS_Calib_ModeResult_RepairBscorr_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('R�paration d''un BSCorr.txt', 'Repair a BScor.txt'));
