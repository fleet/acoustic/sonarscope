function cmenu = create_menu_SP_BS_Calibration(this, baseMsg, cmenu)

parent0 = 'SP_BS_Calibration';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('BS calibration', 'BS calibration'));

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Sonar_calibration.html');

%% sous-Menus

%% Mode program

parent1 = 'SP_BS_CalibModeProgramme';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Mode programme', 'Program Mode'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

cmenu = create_menu_SP_TVG(        this, baseMsg, cmenu, parent0);
cmenu = create_menu_SP_DiagTx(   this, baseMsg, cmenu, parent0);
cmenu = create_menu_SP_DiagRx(  this, baseMsg, cmenu, parent0);
cmenu = create_menu_SP_AireInso(   this, baseMsg, cmenu, parent0);
cmenu = create_menu_SP_BS_Calib(this, baseMsg, cmenu, parent0);

%% Mode Result

cmenu = create_menu_SP_BS_Calib_ModeResult(this, baseMsg, cmenu, parent0);

%% Check compensations

cmenu = create_menu_SP_BS_Calib_CheckCompens(this, baseMsg, cmenu, parent0);

%% Check Kongsberg specular compensation

cmenu = create_menu_SP_BS_AddOn(this, baseMsg, cmenu, parent0);
