function cmenu = create_menu_SP_CreateLayers(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_CreateLayers';

parent0 = 'SP_CreateLayers_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Creation de layers', 'Layers creation'));

msg{3} = 'SP_CreateLayers_RxBeamAngles_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Angle d''Emission', 'Transmission angle'));

msg{3} = 'SP_CreateLayers_Incidence_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Angle d''Incidence', 'Incident angle'));

msg{3} = 'SP_CreateLayers_SecteurEmission_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Secteur d''Emission', 'Transmission sector'));

msg{3} = 'SP_CreateLayers_LatLon_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Latitude et Longitude', 'Latitude and longitude'));

msg{3} = 'SP_CreateLayers_XY_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Coordonnées métriques', 'Metric coordinates'));

msg{3} = 'SP_CreateLayers_TempsUniversel_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Temps universel', 'GMT Time'));

%% Multi-images

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'SP_CreateLayers_MultiImages', ...
    'label', Lang('Images multiples', 'Multi Images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SP_CreateLayers_MultiImagesIncidenceAngle_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Angle d''Incidence', 'Incident angle'));
