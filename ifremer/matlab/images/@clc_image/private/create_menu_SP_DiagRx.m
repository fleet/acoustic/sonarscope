function cmenu = create_menu_SP_DiagRx(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SP_DiagRx';

%% Menu

parent1 = 'SP_DiagRx_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Faisceaux de reception', 'Receive beams'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'Rx.html');

%% Sous-menus

% Levels

cmenu = addMenu_L1_Levels(cmenu, parent1, msg);

% Compensations

cmenu = addMenu_L1_Compensations(cmenu, parent1, msg);


function cmenu = addMenu_L1_Levels(cmenu, parent1, msg)

parent2 = 'SP_DiagRx_Niv';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Niveaux', 'Levels'));

parent3 = 'SP_DiagRx_NivCon';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('RT (Real Time)', 'RT (Real Time)'));

msg{3} = 'SP_DiagRx_PlotConstructeur_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Courbes', 'Curves'));

% -------------------------------
parent4 = 'SP_DiagRx_NivConImage';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Image', 'Image'));

msg{3} = 'SP_DiagRx_ConstructeurAnalytique_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SP_DiagRx_ConstructeurTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));

% --------------------------
parent3 = 'SP_DiagRx_NivIfr';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('SSc', 'SSc'));

msg{3} = 'SP_DiagRx_PlotIfremer_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Courbes', 'Curves'));

parent4 = 'SP_BDiagRx_NivIfrImage';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Image', 'Image'));

parent5 = 'SP_BDiagRx_IfremerAnalytique';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', parent5, ...
    'label', Lang('Fonctions', 'Functions'));

% --------------------------------------------
msg{3} = 'SP_BDiagRx_IfremerAnalytiqueTitre';
cmenu = ajouter(cmenu, 'parent', parent5, 'tag', msg{3}, ...
    'label', Lang('Origine des angles des faisceaux', 'Beams angles'' origins'));

msg{3} = 'SP_DiagRx_IfremerAnalytiqueTheorique_CI1';
cmenu = ajouter(cmenu, 'parent', parent5, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Theoriques', 'Theoretical'), ...
    'Separator', 'on');

msg{3} = 'SP_DiagRx_IfremerAnalytiqueLayer_CI1';
cmenu = ajouter(cmenu, 'parent', parent5, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Layer "RxBeamAngle"', 'Layer "RxBeamAngle"'));

msg{3} = 'SP_DiagRx_IfremerTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));



function cmenu = addMenu_L1_Compensations(cmenu, parent1, msg)

parent2 = 'SP_BDiagRx_Compens';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Compensation', 'Compensation'));

msg{3} = 'SP_DiagRx_CompensAucune_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Aucune', 'None'));

% ---------------------------------------
parent3 = 'SP_BDiagRx_CompensConstructeur';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('RT (Real Time)', 'RT (Real Time)'));

msg{3} = 'SP_DiagRx_CompensConstructeurAnalytique_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SP_DiagRx_CompensConstructeurTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));

% ----------------------------------
parent3 = 'SP_BDiagRx_CompensIfremer';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('SSc', 'SSc'));

msg{3} = 'SP_DiagRx_CompensIfremerAnalytique_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SP_DiagRx_CompensIfremerTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));
