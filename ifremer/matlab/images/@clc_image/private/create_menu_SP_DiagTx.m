function cmenu = create_menu_SP_DiagTx(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SP_DiagTx';

parent1 = 'SP_DiagTx_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Faisceaux d''emission', 'Transmit sectors'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'TxDiag.html');

%% Sous-menus

% Levels

cmenu = addMenu_L1_Levels(cmenu, parent1, msg);

% Compensation

cmenu = addMenu_L1_Compensation(cmenu, parent1, msg);

% Analysis

cmenu = addMenu_L1_Analysis(cmenu, parent1, msg);


function cmenu = addMenu_L1_Levels(cmenu, parent1, msg)

parent2 = 'SonarDiagTx_Niv';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Niveaux', 'Levels'));

parent3 = 'SonarDiagTx_NivCon';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('SSc', 'RT (Real Time)'));

msg{3} = 'SonarDiagTx_PlotConstructeur_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Courbes', 'Curves'));

% -------------------------------
parent4 = 'SonarDiagTx_NivConImage';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Image', 'Image'));

msg{3} = 'SP_DiagTx_ConstructeurAnalytique_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SP_DiagTx_ConstructeurTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));

% --------------------------
parent3 = 'SonarDiagTx_NivIfr';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('SSc', 'SSc'));

msg{3} = 'SonarDiagTx_PlotIfremer_CI0';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Courbes', 'Curves'));

% -------------------------------
parent4 = 'SonarDiagTx_NivIfrImage';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', parent4, ...
    'label', Lang('Image', 'Image'));

msg{3} = 'SonarDiagTx_IfremerAnalytique_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SonarDiagTx_IfremerTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent4, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));


function cmenu = addMenu_L1_Compensation(cmenu, parent1, msg)

parent2 = 'SonarDiagTx_Compens';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Compensation', 'Compensation'));

msg{3} = 'SonarDiagTx_CompensAucune_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Aucune', 'None'));

% ---------------------------------------
parent3 = 'SonarDiagTx_CompensConstructeur';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('SSc', 'RT (Real Time)'));

msg{3} = 'SonarDiagTx_CompensConstructeurAnalytique_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SonarDiagTx_CompensConstructeurTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));

% ----------------------------------
parent3 = 'SonarDiagTx_CompensIfremer';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('SSc', 'SSc'));

msg{3} = 'SonarDiagTx_CompensIfremerAnalytique_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Fonctions', 'Functions'));

msg{3} = 'SonarDiagTx_CompensIfremerTable_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nodes', 'Nodes'));


function cmenu = addMenu_L1_Analysis(cmenu, parent1, msg)

parent2 = 'SonarDiagTx_Analyse';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Analyse', 'Analysis'));

msg{3} = 'SP_DiagTx_Calcul_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul', 'Compute'));

msg{3} = 'SP_DiagTx_Plot_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Visualisation', 'Display'));

msg{3} = 'SP_DiagTx_Nettoyage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nettoyage', 'Cleaning'));

msg{3} = 'SP_DiagTx_Filter_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Filtrage', 'Filter'));

msg{3} = 'SP_DiagTx_Delete_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Delete', 'Delete'));

msg{3} = 'SP_DiagTx_Export_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Exportation', 'Export'));

msg{3} = 'SP_DiagTx_Import_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Importation', 'Import'));

msg{3} = 'SP_DiagTx_Ajust_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Modelisation', 'Modeling'));

msg{3} = 'SP_DiagTx_2Ifremer_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�finir comme mod�le SSc', 'Define as an SSc model'));
