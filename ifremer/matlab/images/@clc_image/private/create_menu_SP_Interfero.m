function cmenu = create_menu_SP_Interfero(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_Sidescan'; % TODO  Remplacer  par SP_Interfero' et créer callback interfero 

%% Menu

parent0 = 'SP_Sidescan_Interfero_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0,  ...
    'label', Lang('Sonar interférométrique', 'Interferometric sonar'));

%% Help

% cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'xxxxx.html');

%% Sous-Menus

msg{3} = 'SP_Sidescan_InterferoComputeHeight_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la hauteur', 'Compute height'));

msg{3} = 'SP_Sidescan_InterferoMaskWC_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Masquage de la colonne d''eau', 'Mask Water Column'));

msg{3} = 'SP_Sidescan_InterferoMaskComputeBathy_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calculer la bathymetrie', 'Compute the bathymetry'));

msg{3} = 'SP_Sidescan_InterferoPhaseRestaurationt_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Restauration de la phase', 'Phase restauration'));

