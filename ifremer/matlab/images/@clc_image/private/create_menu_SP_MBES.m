function cmenu = create_menu_SP_MBES(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_MBES';

parent0 = 'SP_MBES_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Sondeur multifaisceau', 'Multibeam echosounder'));

msg{3} = 'SP_MBES_ImportLayer_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', 'Import layers from PingBeam');

msg{3} = 'SP_MBES_ProjectionX_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', 'PingBeam, PingSamples --> PingAcrossDist');

msg{3} = 'SP_MBES_Range2Depth_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', '{Range, Angle} -> Depth');

% % TODO : bonne id�e � poursuivre, action R&D
% if ~isdeployed
%     msg{3} = 'SP_MBES_MasqueDepth_CI1';
%     cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
%         'label', Lang('Masque � partir de la distance', 'Mask from Depth'));
%     
%     msg{3} = 'SP_MBES_ProbaDepth_CI1';
%     cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
%         'label', Lang('Masque et Probabilit� � partir de la profondeur', 'Mask & Probability from Depth'));
% end

msg{3} = 'SP_MBES_SpikeRemovalJMA_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nettoyage automatique des donn�es erratiques', 'Spike removal'));

msg{3} = 'SP_MBES_MasqueDetection_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Masque � partir du type de d�tection', 'Mask from Detection Type'));

msg{3} = 'SonarRESON_PingSnippets2PingSamples';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', 'PingSnippets --> PingSamples');

msg{3} = 'SP_MBES_MaskDualRecover_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Masquage des faisceaux se recouvrant pour un sondeur dual', ...
    'Mask recovering beams for a dual sounder'));

msg{3} = 'SP_MBES_ImproveHeaveCorrection_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Am�lioration de la correction du "Heave"', ...
    'Improve Heave correction'));

%% Attitude analysis

msg = msg(1:2);
cmenu = addMenu_L1_AttitudeAnalysis(cmenu, parent0, msg);

%%

parent1 = 'SP_MBES_RetD';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', 'R&&D', ...
    'Separator', 'on');

if ~isdeployed
    msg{3} = 'SP_MBES_FilterConditional_CI1';
    cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('Filtrage conditionnel', 'Conditional filter'));
    
    msg{3} = 'SP_MBES_InterpolConditional_CI1';
    cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
        'label', Lang('Interpolation conditionnelle', 'Conditional interpolation'));
end

msg{3} = 'SP_MBES_MasqueNbBeams_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Masque � partir du nombre de faisceaux valides par ping', 'Mask from Nb of valid beams/ping'));

% msg{3} = 'SP_MBES_ProbaDetection_CI1';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Probabilit� � partir du facteur Qualit� Kongsberg', 'Probability from Kongsberg Quality factor'));

msg{3} = 'SP_MBES_MoustacheRemoval_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Suppression de la moustache', 'Moustache removal'));

msg{3} = 'SP_MBES_ComputeAcrossSlopeInPingBeam_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la pente transversale sur une bathy en PingBeam', ...
    'Compute Across Slope on a PingBeam Bathymetry Image'));

% % modif GLT 3/11/2010
% msg{3} = 'SP_MBES_WaveletFilter_CI1';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Filtrage d''attitude par ondelette', ...
%     'Ship attitude artefact filtering'));

% msg{3} = 'SP_MBES_MasqueAmplitudeNbsamples_CI1';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Masque � partir du nombre d''�chantillons d''amplitude', 'Mask from number of amplitude samples'));
% 
% msg{3} = 'SP_MBES_ProbaAmplitudeNbsamples'_CI1;
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Probabilit� � partir du nombre d''�chantillons d''amplitude', 'Probability from Amplitude Nbsamples'));


function cmenu = addMenu_L1_AttitudeAnalysis(cmenu, parent1, msg)

parent2 = 'SP_MBES_AttitudeAnalysis';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Analyse de la bathy vs l''attitude', 'Bathy vs Attitude analysis'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'SP_MBES_AttitudeAnalysisRoll_CI1';
cmenu = ajouter(cmenu, 'parent', parent2,  'tag', msg{3}, 'message', msg, ...
    'label', Lang('Roulis', 'Roll'));

msg{3} = 'SP_MBES_AttitudeAnalysisPitch_CI1';
cmenu = ajouter(cmenu, 'parent', parent2,  'tag', msg{3}, 'message', msg, ...
    'label', Lang('Tangage', 'Pitch'));

msg{3} = 'SP_MBES_AttitudeAnalysisHeave_CI1';
cmenu = ajouter(cmenu, 'parent', parent2,  'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pilonnement', 'Heave'));
