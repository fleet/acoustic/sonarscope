function cmenu = create_menu_SP_Resol(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_Resol';

parent0 = 'SP_Resol_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Resolution', 'Resolution'));

msg{3} = 'SP_Resol_Along_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Longitudinale', 'Alongtrack'));

parent1 = 'SP_ResolTrans';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Transversale', 'Acrosstrack'));

% ---------------------------
parent2 = 'SP_Resol_Image';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Imagerie', 'Imagery'));

msg{3} = 'SP_Resol_ImageAcross_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Image', 'Image'));

msg{3} = 'SP_Resol_AireImage_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Aire insonifiee', 'Insonified area'));

msg{3} = 'SP_SonarResol_PlotAireImage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Plot aire insonifiee', 'Plot insonified area'));

% -------------------------
parent2 = 'SP_Resol_Bathy';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Bathymetrie', 'Bathymetry'));

msg{3} = 'SP_Resol_BathyAcross_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Image', 'Image'));

msg{3} = 'SP_Resol_AireBathy_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Aire insonifiee Bathy', 'Bathymetry insonified area'));

msg{3} = 'SP_Resol_PlotAireBathy_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Plot aire insonifiee Bathy', 'Plot bathymetry insonified area'));
