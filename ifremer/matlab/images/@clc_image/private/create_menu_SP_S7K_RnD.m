function cmenu = create_menu_SP_S7K_RnD(~, baseMsg, cmenu)

if isdeployed
    return
end

msg{1} = baseMsg;
msg{2} = 'S7K_R&D';

parent0 = 'S7K_RnD_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'Reson R&&D');

msg{3} = 'S7K_RnD_BottomDetectionAmplitudePhase_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Détection du fond', 'Bottom Detection '));

msg{3} = 'S7K_RnD_MasqueAmplitude_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Masque sur l''Amplitude', 'Mask from Amplitude'));

msg{3} = 'S7K_RnD_PhaseUnwrapping_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Déroulement de la phase', 'Phase Unwrapping'));

msg{3} = 'ResonRnD_SplitFrequencies';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Séparation des fréquences', 'Split Frequencies'));

msg{3} = 'S7K_RnD_XL_JMA_FiltragePhase_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Filtrage de la phase', 'Phase filter'));
