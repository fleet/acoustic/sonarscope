function cmenu = create_menu_SP_SBP(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_SBP';

parent0 = 'SP_SBP_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0,  ...
    'label', Lang('Sondeur p�n�trateur de s�diments et EK60', 'Subbottom Profiler & EK60'));

parent1 = 'SP_SBP_ComputeHeight';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Calcul de la hauteur', 'Compute height'));

parent2 = 'SP_SBP_ComputeHeightSignalMontant';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Signal montant', 'Rising edge signal'));

msg{3} = 'SP_SBP_ComputeHeightSignalMontantDB_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('en dB', 'in dB'));

msg{3} = 'SP_SBP_ComputeHeightSignalMontantAmp_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('en Amplitude', 'in Amplitude'));

msg{3} = 'SP_SBP_ComputeHeightSignalMontantEnr_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('en Energie', 'in Energy'));

msg{3} = 'SP_SBP_ComputeHeightSignalAlternatif_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Signal alternatif', 'Alternating signal'));

msg{3} = 'SP_EK60_ComputeHeightMagPhase_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Mag & Phase', 'Mag & Phase'));

% if ~isdeployed
%     msg{3} = 'SP_SBP_Demodulate_CI1';
%     cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
%         'label', Lang('Demodulation du signal', 'Signal demodulation'));
% end

msg{3} = 'SP_SBP_SuppressWaterColumn_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Suppression de la colonne d''eau', 'Suppress Water Column'));

msg{3} = 'SP_SBP_KeepOnlyWaterColumn_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Conservation uniquement de la colonne d''eau', 'Keep only Water Column'));

msg{3} = 'SP_SBP_Export3DViewer_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export vers GLOBE', 'Export to GLOBE'));

msg{3} = 'SP_SBP_Processing_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Filtrages SBP - Ifremer', 'SBP Ifremer Filters'));

msg{3} = 'SP_SBP_ImmersionConstante_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Immersion constante', 'Constant immesrsion'));

msg{3} = 'SP_SBP_Envelope_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Enveloppe de signaux sismique', 'Envelope of seismic images'));

% if ~isdeployed 
%     msg{3} = 'Seismic_Projection'; % Plus utilis�
%     cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
%         'label', Lang('Projection sur un plan (R&&D Louis G�li)', 'Project on a plan (Louis G�li R&&D)'));
% end

%% Multi images

msg{2} = 'Help';
msg{3} = 'TODO';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'HelpArithmeticMultiImages', ...
    'label', Lang('Images multiples', 'Multi Images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');
msg(3) = [];

msg{1} = baseMsg;
msg{2} = 'SP_SBP';
msg{3} = 'SP_EK80_ConcatainImages_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Concat�nation d''images EK80', 'Concatenate EK80 images'));

