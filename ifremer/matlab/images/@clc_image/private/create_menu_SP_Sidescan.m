function cmenu = create_menu_SP_Sidescan(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_Sidescan';

%% Sonar lat�ral

parent0 = 'SP_Sidescan_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0,  ...
    'label', Lang('Sonar lateral', 'Side scan sonar'), ...
    'Separator', 'on');

%% Help

cmenu = creer_menu_help(cmenu, parent0, baseMsg, 'Side_scan_sonar.html');

%% Sous-Menus

msg{3} = 'SP_Sidescan_ComputeHeight_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Calcul de la hauteur', 'Compute height'), ...
    'Separator', 'on');

msg{3} = 'SP_Sidescan_PingRange2PingAcrossDist_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Correction de distance oblique (PingSample -> PingAcrossDist)', 'Slant range correction (PingSample -> PingAcrossDist)'));

msg{3} = 'SP_Sidescan_LayerDepth_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Creation d''un layer "Depth"', 'Create a "depth" layer'));

% Comment� car en doublon avec "SP_SBP_ImmersionConstante_CI1"
% msg{3} = 'SP_Sidescan_SedimentImmersion_CI1';
% cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Compensation de l''immersion du sondeur de sediments', 'Subbottom profiler immersion compensation'));

msg{3} = 'SP_Sidescan_ExtractWCD_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Extraction de la colonne d''eau', 'Extract Water Column'));
