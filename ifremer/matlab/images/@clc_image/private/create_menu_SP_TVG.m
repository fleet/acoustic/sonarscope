function cmenu = create_menu_SP_TVG(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'SP_TVG';

%% Menu

parent1 = 'SP_TVG_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('TVG', 'TVG'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'TVGcal.html');

%% Sous-menus

% ----------------------------
% Fonctions Sonar - TVG Niveau

parent2 = 'SP_TVG_Niveau';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Niveaux', 'Levels'));

msg{3} = 'SP_TVG_Courbes_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Courbes', 'Curves'));

parent3 = 'SP_TVG_Image';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', parent3, ...
    'label', Lang('Image', 'Image'));

msg{3} = 'SP_TVG_RT_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RT (Temps r�el)', 'RT (Real Time)'));

msg{3} = 'SP_TVG_SSc_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('SSc', 'SSc'));

msg{3} = 'SP_TVG_User_CI1';
cmenu = ajouter(cmenu, 'parent', parent3, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('User', 'User'));

% ----------------------------------
% Fonctions Sonar - TVG Compensation

parent2 = 'SP_TVG_Compensation';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Compensation', 'Compensation'));

msg{3} = 'SP_TVG_NoCompensation_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Aucune', 'None'));

msg{3} = 'SP_TVG_CompensationRT_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('RT (Temps r�el)', 'RT (Real Time)'));

msg{3} = 'SP_TVG_CompensationSSc_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('SSc', 'SSc'));

msg{3} = 'SP_TVG_CompensationUser_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('User', 'User'));
