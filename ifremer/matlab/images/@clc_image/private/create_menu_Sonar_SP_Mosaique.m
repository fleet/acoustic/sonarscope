function cmenu = create_menu_Sonar_SP_Mosaique(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_Mosaique';

parent0 = 'SP_Mosaique_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Cr�ation de cartes', 'Mapping'));

msg{3} = 'SP_Mosaique_Sonar_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Maillage de cette image', 'Grid / mosaic'));

msg{3} = 'SP_Mosaique_Assemblage_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Assemblage de grilles', 'Grids / mosaics merging'));

msg{3} = 'SP_Mosaique_Inv_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Maillage inverse', 'Inverse grid / mosaic'));

msg{3} = 'SP_Mosaique_Plot_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nuage de points', 'Point cloud'));

msg{3} = 'SP_Mosaique_ExportFly_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Export nuage de points en .ply', 'Export point cloud into .ply'));

msg{3} = 'SP_Mosaique_ExportSoundings_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Exportation des sondes dans GLOBE', 'Export soundings in GLOBE'));

msg{3} = 'SP_Mosaique_ChangeBasis_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Changement de rep�re 3D', 'Change 3D basis'));
