function cmenu = create_menu_WCD(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'WCD';

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'WCD_MainMenu', ...
    'label', Lang('Colonne d''eau', 'Water Column'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

%% Slices

cmenu = addMenu_L1_AlongDistance(cmenu, parent0, msg);

%% Horizontal slices

msg{3} = 'WCD_HorizontalSlices_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Tranches horizontales', 'Horizontal slices'));

%% Plume detection

msg{3} = 'WCD_PlumeDetection_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Détection de panache', 'Plume detection'));

%% Check

cmenu = addMenu_L1_Check(cmenu, parent0, msg);

%% Utilitaires

cmenu = addMenu_L1_Utilities(cmenu, parent0, msg);


function cmenu = addMenu_L1_Utilities(cmenu, parent0, msg)

parent1 = 'WCD_Utilities';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Utilitaires', 'Utilities'));

msg{3} = 'WCD_GroupEchograms_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label',  Lang('Regroupement des fichiers d''Echogrammes', 'Group Echogram files'));

msg{3} = 'WCD_ZipDirXml_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zip des répertoires XML de Water Column', 'Zip Water Column XML directories'));

% msg{3} = 'WC_CheckFiles';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
%     'label', Lang('Vérification des fichiers', 'Check the files'));


function cmenu = addMenu_L1_AlongDistance(cmenu, parent0, msg)

parent1 = 'WCD_Slices';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Coupes verticales', 'Vertical cutaways'));

cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'WC_AlongCreate', ...
    'label', Lang('Création', 'Create'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'WCD_SlicesAlongNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Latéralement à la navigation', 'Sidealong navigation'));

msg{3} = 'WCD_AlongPath_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Le long d''un chemin', 'Along a pre-defined track'));

%% Export

cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'WCD_ExportAlongTrack', ...
    'label', Lang('TODO', 'Export in ASCII'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{3} = 'WCD_ExportAlongTrackInASCII_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Coupe le long d''un chemin', 'Along path slice'));


function cmenu = addMenu_L1_Check(cmenu, parent0, msg)

parent1 = 'WCD_Check';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Vérification des résultats', 'Check results'));

msg{3} = 'WCD_WaterColumnCheckWCDepthAcrossDistPlayer_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Vérification des WC-DepthAcrossDist V1', 'Check WC-DepthAcrossDist (player)'));

msg{3} = 'WCD_WaterColumnCheckWCDepthAcrossDistNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Vérification des WC-DepthAcrossDist V2', 'Check WC-DepthAcrossDist (navigation)'));

msg{3} = 'WCD_CheckSlicesAlongNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Vérification des Coupes le long de la nav', 'Check Along navigation echograms'));

% Check 3DMatrix

parent2 = 'Check3DMatrixMovies';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Vérification des 3DMatrix', 'Check 3DMatrix'));

msg{3} = 'WCD_CheckMatrixMoviePings_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Film des pings', 'Movie of pings'));

msg{3} = 'WCD_CheckMatrixMovieVertSlices_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Film des slices longitudinales', 'Movie of along nav slices'));

msg{3} = 'WCD_CheckMatrixMovieHoriSlices_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Film des slices horizontales', 'Movie of horizontal slices'));

msg{3} = 'WCD_CheckMatrix_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Interactif', 'Interactive'));

msg{3} = 'WCD_CheckMatrixSPFEStep1_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE : Create figures of  WCD-Nadir part through time'));

msg{3} = 'WCD_CheckMatrixSPFEStep2_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('TODO', 'SPFE : Merge figures'));

% SampleBeam

msg{3} = 'WCD_CheckSampleBeam_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Vérification des WC-SampleBeam', 'Check WC-SampleBeam'));
