function cmenu = create_menu_XTF(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'XTF';
 
parent1 = 'XTF_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".xtf" ', '".xtf" files (Triton)'));

%% Help

cmenu = creer_menu_help(cmenu, parent1, baseMsg, 'xtf_files.html');

%% Plot navigation

parent2 = 'TagSonarXTFNav';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Trac� de la navigation', 'Navigation Display'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'XTF_PlotNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation', 'Plot Navigation'));

msg{3} = 'XTF_PlotNavigationCoverage_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� de la navigation et de la couverture sondeur', 'Plot Navigation & Coverage'));

msg{3} = 'XTF_PlotSignalOnNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� d''un signal sur la navigation', 'Plot Navigation & Signal'));

msg{3} = 'XTF_ImportNavigation_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Import de la navigation', 'Import Navigation'));

%% Transformations g�ographiques

parent2 = 'TagSonarXTFTransGeometry';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Transformations g�om�triques', 'Geometric Transformations'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'XTF_PingSamples2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'PingSample --> LatLong');

msg{3} = 'XTF_PingBeam2LatLong_CI1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', 'PingBeam  --> LatLong');

msg{3} = 'XTF_PingRange2PingAcrossDist_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('PingSample --> PingAcrossDist', 'PingSample --> PingAcrossDist'));

%% Utilitaires

parent2 = 'TagSonarXTFUtilities';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{3} = 'XTF_ReadOnce_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Premi�re lecture (non obligatoire)', 'Just read once (not obligatory)'));

msg{3} = 'XTF_CleanSignals_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pr�traitement des signaux', 'Preprocessing Signals'));

msg{3} = 'XTF_ComputeBathy_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Pr�traitement de la bathym�trie', 'Preprocessing Bathymetry'));

% msg{3} = 'XTF_PingSamples2PingAcrossDist';
% cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
%     'label', 'PingSamples --> PingAcrossDist');

msg{3} = 'XTF_PlotAppendices_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', 'Plot Ship, Motion & Sonar signals');

msg{3} = 'XTF_ZipDir_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Zip des r�pertoires', 'Zip directories'));

msg{3} = 'XTF_DeleteCache_CI0';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Effacement des r�pertoires de Cache de SonarScope', 'SonarScope cache erase'));

