function cmenu = create_menu_XYZ(~, baseMsg, cmenu, parent0)

msg{1} = baseMsg;
msg{2} = 'XYZ';

parent1 = 'XYZ_MainMenu';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Fichiers ".xyz" ', '".xyz" files (ASCII)'));

msg{3} = 'XYZ_ImportExport_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('D�codage de fichiers .xyz et export en .xml', 'Import from.xyz files and export in .xml'));
