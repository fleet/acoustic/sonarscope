% Creation d'une nouvelle vue
%
% Syntax
%   this = creation_nouvelle_image(this)
%
% Input Arguments
%   this     : Instance de clc_image
%   newImage : Nouvelle image
%   nomImage : Nom de la nouvelle image
%
% Name-Value Pair Arguments
%   x    :
%   y    :
%   subx :
%   suby :
%
% Output Arguments
%   this : Instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creation_nouvelle_image(this, newImage, nomImage, varargin)

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');

[varargin, subx] = getPropertyValue(varargin, 'subx', 1:length(x));
[varargin, suby] = getPropertyValue(varargin, 'suby', 1:length(y));

ValNaN  = get(this.Images(this.indImage), 'ValNaN');
[varargin, ValNaN] = getPropertyValue(varargin, 'ValNaN', ValNaN);

N = length(this.Images) + 1;

%% Mise � jour des coordonn�es

this.Images(N) = majCoordonnees(this.Images(this.indImage), subx, suby);

this.Images(N) = set(this.Images(N), 'Image', newImage, 'ValNaN', ValNaN);
this.Images(N).CLim = [];

val = this.Images(this.indImage).Unit;

[varargin, val] = getPropertyValue(varargin, 'Unit', val); %#ok<ASGLU>

this.Images(N) = set(this.Images(N), 'Unit', val);

this.cross(     N, :) = this.cross(     this.indImage, :);
this.identVisu( N)    = 1;
this.visuLim(   N, :) = this.visuLim(   this.indImage, :);
%     this.visuLimHistory{N}{1}     = this.visuLim(this.indImage, :);
%     this.visuLimHistoryPointer(N) = this.visuLimHistoryPointer(N) + 1;
this.visuLimHistory{N}        = this.visuLimHistory{this.indImage};
this.visuLimHistoryPointer(N) = this.visuLimHistoryPointer(this.indImage);
try
    this.valBeforeSetNaN(N) = this.valBeforeSetNaN(this.indImage);
catch %#ok<CTCH>
    'Message for JMA : Quelque chose qui va pas dans creation_nouvelle_image pour valBeforeSetNaN' %#ok<NOPRT>
end

TagSynchroContrast = num2str(rand(1));
this.Images(N) = set(this.Images(N), 'TagSynchroContrast', TagSynchroContrast);

%% Affichage du nom de l'image dans la liste

% str = listeLayers(this);
this.Images(N).Name = nomImage;
% str{N} = nomImage;

this.indImage = N;

this.ihm.NomImage.cpn = set_texte(this.ihm.NomImage.cpn, nomImage);
