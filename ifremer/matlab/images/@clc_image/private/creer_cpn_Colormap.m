% Creation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_Colormap(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_Colormap(this, baseMsg)

msg{1} = baseMsg;

userName  = get(this, 'componentUserName');
userCb    = get(this, 'componentUserCb');
objMenu = clc_uimenu( 'componentUserName' , userName, ...
    'componentUserCb'   , userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);
cmenu = objMenu;

% -------------------------- FonctionsExport --------------------------

cmenu = ajouter(cmenu, 'tag', 'TitreColormapUser', 'message', msg, ...
    'label', Lang('Perso', 'Personnal'),  ...
    'ForegroundColor', 'b');

msg{2} = '1';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'User');


cmenu = ajouter(cmenu, 'tag', 'TitreColormapCommon', 'message', msg, ...
    'label', Lang('Courantes', 'Usuals'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = '2';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Gray');

msg{2} = '3';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Jet');

msg{2} = '4';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Cool');

msg{2} = '5';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Hsv');

msg{2} = '6';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Hot');

msg{2} = '7';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Bone');

msg{2} = '8';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Copper');

msg{2} = '9';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Pink');

msg{2} = '10';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Flag');

msg{2} = '11';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'CNES');

msg{2} = '12';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'CNESjet');

msg{2} = '13';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'ColorSea');

msg{2} = '14';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'ColorEarth');

msg{2} = '18';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Haxby');

msg{2} = '20';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Catherine');

msg{2} = '21';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Seismics');

% Seismics SeismicsSym

%% Tables symétrisées

cmenu = ajouter(cmenu, 'tag', 'TitreColormapCentered', 'message', msg, ...
    'label', Lang('Centrées / 0', 'Centered / 0'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = '19';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'Becker');

msg{2} = '15';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'NIWA1');

msg{2} = '16';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'NIWA2');

msg{2} = '17';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'JetSym');

msg{2} = '22';
cmenu = ajouter(cmenu, 'tag', ['ColorTable' msg{2}], 'message', msg, ...
    'label', 'SeismicsSym');



cmenu = ajouter(cmenu, 'tag', 'TitreColormapModification', 'message', msg, ...
    'label', Lang('Modification', 'Modification'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = 'ContrasteChangeNumber';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Changement du nombre de couleurs', 'Change number of colors'));

msg{2} = 'ContrasteEditionTable';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Edition de la table de couleurs', 'Edit Colormap'));

msg{2} = 'ContrasteGetModifiedColormap';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Récupération de la table de couleurs modifiée', 'Get modified colormap'));


cmenu = ajouter(cmenu, 'tag', 'TitreColormapIO', 'message', msg, ...
    'label', Lang('E/S', 'I/O'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = 'ContrasteLoadColormap';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Importation d''une table de couleurs depuis un fichier', 'Load colormap from a file'));

msg{2} = 'ContrasteSaveColormap';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Sauvegarde de la table de couleurs dans un fichier', 'Save colormap in a file'));


cmenu = ajouter(cmenu, 'tag', 'TitreColormapDisplay', 'message', msg, ...
    'label', Lang('Visualisation', 'Display'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = 'ContrasteShowColormaps';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Visualisation de toutes les tables de couleurs disponibles', 'Show all colormaps'));

% -------------------------- Fonctions --------------------------

handle = get_handle(this.ihm.ColormapIndex.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end
