% Creation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_Compute(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_Compute(this, baseMsg, hGeneralProces)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
cmenu    = clc_uimenu('typeMenu', 'uimenu', 'componentUserName', userName, ...
    'componentUserCb',  userCb, 'componentInsetX', 3, 'componentInsetY', 3);

%% Help

cmenu = creer_menu_help(cmenu, [], baseMsg, 'General_processings.html');

%% Menus

cmenu = create_menu_GP_Arithmetic(this, baseMsg, cmenu);
cmenu = create_menu_GP_Derivate(     this, baseMsg, cmenu);
cmenu = create_menu_GP_Filter(      this, baseMsg, cmenu);
cmenu = create_menu_GP_Fourier(      this, baseMsg, cmenu);
cmenu = create_menu_GP_Compensation( this, baseMsg, cmenu);
cmenu = create_menu_GP_Correlation(  this, baseMsg, cmenu);
cmenu = create_menu_GP_Shift(     this, baseMsg, cmenu);
cmenu = create_menu_GP_Codage(       this, baseMsg, cmenu);
cmenu = create_menu_GP_ExtractInser(   this, baseMsg, cmenu);
cmenu = create_menu_GP_RegionOfInterest(this, baseMsg, cmenu);
cmenu = create_menu_GP_Segmentation(      this, baseMsg, cmenu);
cmenu = create_menu_GP_Synthesis(     this, baseMsg, cmenu);
cmenu = create_menu_GP_ImageType(      this, baseMsg, cmenu);
cmenu = create_menu_GP_Geometry(     this, baseMsg, cmenu);
cmenu = create_menu_GP_Carto(        this, baseMsg, cmenu);

% -------------------------- FonctionInterpolation --------------------------
msg{2} = 'GP_Interpolation_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Interpolation (i)');

% -------------------------- FonctionAnimation --------------------------
msg{2} = 'GP_Animation_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Animation', 'Movie'));

% -------------------------- FonctionHandCorrection --------------------------
msg{2} = 'GP_HandCorrection_CI0';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Correction manuelle', 'Hand correction'));

% -------------------------- ConversionType --------------------------

parent1 = 'GP_Cast';
cmenu = ajouter(cmenu, 'tag', parent1, ...
    'label', Lang('Conversion du type de stockage', 'Cast support data storage'));

msg{2} = 'GP_Cast_Uint8_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Uint8', 'Uint8'));

msg{2} = 'GP_Cast_Single_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Single', 'Single'));

msg{2} = 'GP_Cast_Double_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Double', 'Double'));

% -------------------------- ObjectAnalysis --------------------------

parent1 = 'GP_ObjectAnalysis';
cmenu = ajouter(cmenu, 'tag', parent1, ...
    'label', Lang('Analyse d''objet', 'Object Analysis'));

msg{2} = 'GP_ObjectAnalysis_QuadTree_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Quad tree', 'Quad tree'));

% -------------------------- Cr�ation layer � partir de signaux verticaux --------------------------

parent1 = 'GP_SignalVert';
cmenu = ajouter(cmenu, 'tag', parent1, ...
    'label', Lang('Cr�ation de layers � partir de signaux', 'Create layer from signals'));

msg{2} = 'GP_SignalVert_CreateImage_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Verticaux', 'Vertical'));

% -------------------------- Fonctions --------------------------

handle = get_handle(this.ihm.Compute.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
% associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end

%% Association des menu du bouton au menu de la figure

this.ihm.Handles = associer_menuFig(cmenu, [], hGeneralProces, handle, this.ihm.Handles);
