% Cr�ation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_ComputeSonar(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_ComputeSonar(this, baseMsg, hSonarProces) %, hSonarSurvey)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
cmenu    = clc_uimenu('typeMenu', 'uimenu', 'componentUserName', userName, ...
    'componentUserCb',  userCb, 'componentInsetX', 3, 'componentInsetY', 3);

%% Help

cmenu = creer_menu_help(cmenu, [], baseMsg, 'Sonar_specific_processings.html');
% 'tag', 'HelpSonarProcessing'

%% Menu Sonar Processings

cmenu = create_menu_SP_Sidescan(      this, baseMsg, cmenu);
cmenu = create_menu_SP_Interfero(    this, baseMsg, cmenu);
cmenu = create_menu_SP_MBES(         this, baseMsg, cmenu);
cmenu = create_menu_SP_CreateLayers(       this, baseMsg, cmenu);
cmenu = create_menu_SP_BS_Calibration(this, baseMsg, cmenu);
cmenu = create_menu_SP_BS_Analysis(   this, baseMsg, cmenu);
cmenu = create_menu_SP_Resol(   this, baseMsg, cmenu);
cmenu = creer_menu_Sonar_Speculaire(   this, baseMsg, cmenu);
cmenu = create_menu_Sonar_SP_Mosaique(     this, baseMsg, cmenu);
cmenu = create_menu_SP_WCD(  this, baseMsg, cmenu);
cmenu = create_menu_SP_SBP(  this, baseMsg, cmenu);
cmenu = create_menu_GP_ADCP(      this, baseMsg, cmenu);
cmenu = create_menu_SP_Apendices(      this, baseMsg, cmenu);
cmenu = create_menu_SP_S7K_RnD(    this, baseMsg, cmenu);

%% Menu Survey processings TODO : s�parer cette fonction en deux

str1 = 'Traitement d''une campagne';
str2 = 'Survey processings';
parent1 = 'FctSonSurveyProcessing';
msg{2} = parent1;
cmenu = ajouter(cmenu, [], 'tag', msg{2}, ...
    'label', Lang(str1,str2));

%% Help

cmenu = creer_menu_help(cmenu, msg{2}, baseMsg, 'Survey_specific_processings.html');

%% Cache preferences

cmenu = create_menu_CacheFiles(this, baseMsg, cmenu, parent1);

%% Menus

str1 = 'Donn�es sonar & MBES';
str2 = 'Sonar & MBES data';
msg{2} = 'FctSonSurveyProcessing1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag',  msg{2}, 'label', Lang(str1,str2), ...
    'Separator', 'On', 'ForegroundColor', 'b');

%% Status du cache

cmenu = create_menu_ALL(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_S7K(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_RDF(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_SDF(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_XTF(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_RAW(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_SEGY(       this, baseMsg, cmenu, parent1);
cmenu = create_menu_HAC(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_ODV(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_BOB(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_XYZ(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_SAR(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_WCD(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_PICTURES(   this, baseMsg, cmenu, parent1);

cmenu = ajouter(cmenu, 'parent', parent1, 'tag', 'TagOtherData', ...
    'label', 'Other data', ...
    'Separator', 'on', 'ForegroundColor', 'b');

cmenu = create_menu_REMUS(      this, baseMsg, cmenu, parent1);
cmenu = create_menu_CAR(        this, baseMsg, cmenu, parent1);
cmenu = create_menu_CINNA(      this, baseMsg, cmenu, parent1);
cmenu = create_menu_NAVIGATION( this, baseMsg, cmenu, parent1);
cmenu = create_menu_NORBIT(     this, baseMsg, cmenu, parent1);
cmenu = create_menu_MonoCapteur(        this, baseMsg, cmenu, parent1);
   
%% Cr�ation des menus

handle = get_handle(this.ihm.ComputeSonar.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end

%% Association des boutons aux menus de la figure

this.ihm.Handles = associer_menuFig(cmenu, [], hSonarProces, handle, this.ihm.Handles, 'ExcludeTag', parent1);
this.ihm.Handles = associer_menuFig(cmenu, [], this.hfig,    handle, this.ihm.Handles, 'UniqueTag',  parent1);
