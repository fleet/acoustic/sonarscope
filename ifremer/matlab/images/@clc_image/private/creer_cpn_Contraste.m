% Creation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_Contraste(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_Contraste(this, baseMsg)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
cmenu    = clc_uimenu( 'componentUserName', userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);

%% Saisie

msg{2} = 'ContrasteImageSaisieCLim';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Saisie des valeurs', 'Edit'), ...
    'Separator', 'on');

%% Image Enti�re

parent1 = 'ImageEntiere';
cmenu = ajouter(cmenu, 'tag', parent1, ...
    'label', Lang('Image entiere', 'Whole image'));

msg{2} = 'ContrasteImageEntiereMinMax';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('[Min Max]', '[Min Max]'));

msg{2} = 'ContrasteImageEntiereQuant_0p5pc';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Quantile 0.5%');

msg{2} = 'ContrasteImageEntiereQuant_1pc';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Quantile 1%', 'Quantile 1%'));

msg{2} = 'ContrasteImageEntiereQuant_3pc';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Quantile 3%', 'Quantile 3%'));

%% Image Zoom�e

parent1 = 'ImageZoomee';
cmenu = ajouter(cmenu, 'tag', parent1, ...
    'label', Lang('Image zoomee', 'Current extent'));

msg{2} = 'ContrasteImageZoomeeMinMax';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '[Min Max] (m)');

msg{2} = 'ContrasteImageZoomeeQuant_0p5pc';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Quantile 0.5% (c)');

msg{2} = 'ContrasteImageZoomeeQuant_1pc';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Quantile 1% (C)');

msg{2} = 'ContrasteImageZoomeeQuant_3pc';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Quantile 3%', 'Quantile 3%'));

%% Contraste Auto

parent1 = 'ContrasteAuto';
cmenu = ajouter(cmenu, 'tag', parent1, ...
    'label', Lang('Contraste Auto', 'Contrast Auto'), ...
    'Separator', 'on');

msg{2} = 'ContrasteImageZoomeeMinMaxAuto';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '[Min Max]');

msg{2} = 'ContrasteImageZoomee0p5PCAuto';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Quantile 0.5%');

%% Cr�ation de l'IHM

handle = get_handle(this.ihm.Contraste.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end
