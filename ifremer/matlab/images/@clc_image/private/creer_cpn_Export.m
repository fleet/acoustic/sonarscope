% Creation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_Export(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_Export(this, baseMsg, h0)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
cmenu    = clc_uimenu('typeMenu', 'uimenu', 'componentUserName' , userName, ...
    'componentUserCb' , userCb, 'componentInsetX', 3, 'componentInsetY', 3);

%% Help

cmenu = creer_menu_help(cmenu, [], baseMsg, 'Export_data.html');

%% Screendump

msg{2} = 'EXPORT_ScreenDump_CI0';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Copie d''ecran (s)', 'Screendump (s)'), ...
    'Separator', 'on');

%% XML

cmenu = addMenu_L0_XML(cmenu, msg);

%% TIF

cmenu = addMenu_L0_TIF(cmenu, msg);

%% Google Earth

msg{2} = 'EXPORT_GoogleEarth_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Google Earth (g)');

%% GLOBE

msg{2} = 'EXPORT_GLOBE_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'GLOBE');

msg{2} = 'EXPORT_GLOBEMultiData_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'GLOBE MultiData (underwork)');

%% Surfer

cmenu = addMenu_L0_Surfer(cmenu, msg);

%% GMT

cmenu = addMenu_L0_GMT(cmenu, msg);

%% ErMapper

cmenu = addMenu_L0_ErMapper(cmenu, msg);

%% Fledermaus

msg{2} = 'EXPORT_Fledermaus_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Fledermaus', 'Fledermaus'));

%% BAG format

msg{2} = 'EXPORT_BAG_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Bathymetry Attributed Grid', 'Bathymetry Attributed Grid'));

%% Export ArcGIS

cmenu = addMenu_L0_ArcGIS(cmenu, msg);

%% Export ASCII

cmenu = addMenu_L0_ASCII(cmenu, msg);

%% GXF-3

cmenu = addMenu_L0_GXF(cmenu, msg);

%% Collada
cmenu = addMenu_L0_3DMODEL(cmenu, msg);

%% Caraibes

msg{2} = 'EXPORT_Caraibes_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'IFREMER-CARAIBES');

%% Format d'origine

msg{2} = 'EXPORT_FicOrigine_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('R�pertoire de cache SonarScope', 'SonarScope cache directory'));

%% Matlab

cmenu = addMenu_L0_Matlab(cmenu, msg);

%% Utilities

cmenu = addMenu_L0_Utilitaires(cmenu, msg);

%% Cr�ation du menu

handle = get_handle(this.ihm.Export.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
% associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end

%% Association des menu du bouton au menu de la figure

[this.ihm.Handles, hMenuExport] = associer_menuFig(cmenu, Lang('Exportation', 'Export'), h0, handle, this.ihm.Handles);
setappdata(hMenuExport, 'IconData', 'File_Save.png');


function cmenu = addMenu_L0_Matlab(cmenu, msg)

cmenu = ajouter(cmenu, 'tag', 'EXPORT_Matlab', ...
    'label', 'Matlab',  ...
    'Separator', 'On', 'ForegroundColor', 'b');

parent0 = 'EXPORT_MatlabFigures';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'Figures');

msg{2} = 'EXPORT_MatlabFigure_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Image seule (f)', 'Image only (f)'));

parent1 = 'EXPORT_ImageSignals';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Image + signaux, ...', 'Image + signals, ...'));

msg{2} = 'EXPORT_MatlabImageSignalsY_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('En fonction des Y', 'Through Y'));

msg{2} = 'EXPORT_MatlabImageSignalsT_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('En fonction du temps', 'Through Time'));

msg{2} = 'EXPORT_MatlabImageSignalsYSignalDialog_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Dans SignalDialog', 'In SignalDialog'));

msg{2} = 'EXPORT_MatlabFigure3D_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Surf 3D', 'Surf 3D'));

% -------------------------------
parent0 = 'EXPORT_MatlabData';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'Data');

msg{2} = 'EXPORT_MatlabArray_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Matrice Matlab (x,y,Image)', 'Matlab Array (x,y,Image)'));

if ~isdeployed
    msg{2} = 'EXPORT_MatlabObjet_CI1';
    cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
        'label', Lang('Objet Matlab (cl_image)', 'Matlab object (cl_image)'));
end


function cmenu = addMenu_L0_Utilitaires(cmenu, msg)

parent0 = 'EXPORT_Utilities';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Utilitaires', 'Utilities'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = 'EXPORT_CompressDataFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Compression des fichiers d''exemple', 'Compression example data files'));

msg{2} = 'EXPORT_UncompressDataFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Compression des fichiers d''exemple', 'Uncompress example data files'));

msg{2} = 'EXPORT_Ram2VM_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Liberation m�moire', 'Free memory'));

msg{2} = 'EXPORT_VM2Ram_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ramener l''image courante en RAM', 'Put the current image in RAM'));

msg{2} = 'EXPORT_CompressRepertoire_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Compression d''un r�pertoire', 'Compress a directory'));

msg{2} = 'EXPORT_UncompressRepertoire_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�compression d''un r�pertoire', 'Uncompress a directory'));

msg{2} = 'EXPORT_CompressFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Compression d''un fichier particulier', 'Compress specific files'));

msg{2} = 'EXPORT_UncompressFiles_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�compression d''un fichier particulier', 'Uncompress specific files'));


function cmenu = addMenu_L0_GXF(cmenu, msg)

parent0 = 'EXPORT_GXF';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'GXF-3 (Grid eXchange File)');

msg{2} = 'EXPORT_GXF_3_uncompressed_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Brut', 'Raw'));

msg{2} = 'EXPORT_GXF_3_compressed_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Compress�', 'Compressed'));

msg{2} = 'EXPORT_GXF_3_repeatcompressed_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Super compress�', 'Highly compressed'));

msg{2} = 'EXPORT_GXF_3_kingdom_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('pour Kingdom�', 'for Kingdom�'));


function cmenu = addMenu_L0_3DMODEL(cmenu, msg)

parent0 = 'EXPORT_3DMODEL';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', '3D model');

msg{2} = 'EXPORT_VRML_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('VRML', 'VRML'));

msg{2} = 'EXPORT_ColladaDae_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('fichier COLLADA (*.dae)', 'COLLADA file (*.dae)'));

msg{2} = 'EXPORT_ColladaObj_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('fichier OBJ (*.obj)', 'OBJ file (*.obj)'));


function cmenu = addMenu_L0_ASCII(cmenu, msg)

parent0 = 'EXPORT_LayerASCII';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'ASCII');

msg{2} = 'EXPORT_XYZ_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'XYZ');

msg{2} = 'EXPORT_CarisAscii_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Caris');

msg{2} = 'EXPORT_AsciiMultiLayer_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Multi-layers');


function cmenu = addMenu_L0_ArcGIS(cmenu, msg)

parent0 = 'EXPORT_ArcGis';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'ArcGis');

msg{2} = 'EXPORT_ArcGisAscii_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'ASCII grid (*.asc)');

msg{2} = 'EXPORT_ArcGISBin_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'FLOAT grid (*.HDR + *.FLT)');

msg{2} = 'EXPORT_ImageGeotiff32BitsBis_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Geotif 32 bits', 'Geotif 32 bits'));


function cmenu = addMenu_L0_ErMapper(cmenu, msg)

parent0 = 'EXPORT_ERS';
cmenu = ajouter(cmenu,  'tag', parent0, ...
    'label', Lang('ErMapper', 'ErMapper'));

cmenu = creer_menu_help(cmenu, parent0, msg{1}, 'ErMapper.html');

msg{2} = 'EXPORT_ERS_Image_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Image courante (e)', 'Curent image (e)'), ...
    'Separator', 'on');

msg{2} = 'EXPORT_ERS_MultiImages_CI0';
cmenu = ajouter(cmenu, 'parent', parent0,  'tag', msg{2}, 'message', msg, ...
    'label', 'Multi images');

msg{2} = 'EXPORT_ERS_ImagesSynchro_CI1';
cmenu = ajouter(cmenu, 'parent', parent0,  'tag', msg{2}, 'message', msg, ...
    'label', 'Multi image, same limits as current one');


function cmenu = addMenu_L0_GMT(cmenu, msg)

parent0 = 'EXPORT_GMT';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'GMT');

cmenu = creer_menu_help(cmenu, parent0, msg{1}, 'GMT.html');

msg{2} = 'EXPORT_GMT1_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'GMT display', ...
    'Separator', 'on');

msg{2} = 'EXPORT_GMT2_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'Multilayer GMT display');


function cmenu = addMenu_L0_Surfer(cmenu, msg)

parent0 = 'EXPORT_Surfer';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'Surfer');

msg{2} = 'EXPORT_SurferASCII_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', 'ASCII');

parent1 = 'EXPORT_Surfer7Bin';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', 'Version 7');

msg{2} = 'EXPORT_Surfer7Current_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Current image');

msg{2} = 'EXPORT_Surfer7Multi_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'Multi images');


function cmenu = addMenu_L0_TIF(cmenu, msg)

parent0 = 'ExportTif';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'Image .tif, .jpg, .bnp, .gif, ...');

msg{2} = 'EXPORT_Raster_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Uniquement l''image courante (t)(j)(b)', 'Only this one (t)(j)(b)'));

msg{2} = 'EXPORT_TifImagesSynchro_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Images synchronis�es, ...', 'Synchronized Images, ...'));

% Gif Annim�e
msg{2} = 'EXPORT_ImageGifAnimated_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Image gif annim�e', 'GIF animated image'));

% Geotif 32 bits (pour ArcGIS)
msg{2} = 'EXPORT_ImageGeotiff32Bits_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Geotif 32 bits (pour ArcGIS)', 'Geotif 32 bits (for ArcGIS)'));


function cmenu = addMenu_L0_XML(cmenu, msg)

parent0 = 'EXPORT_XML';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', 'XML');

msg{2} = 'EXPORT_XML_Image_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Uniquement l''image courante (x)', 'Only this one (x)'));

msg{2} = 'EXPORT_XML_ImagesSynchro_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Images synchronis�es, ...', 'Synchronized Images, ...'));
