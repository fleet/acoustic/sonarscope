% Creation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_Import(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_Import(this, baseMsg, h0)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
cmenu    = clc_uimenu('typeMenu', 'uimenu', 'componentUserName' , userName, 'componentUserCb', userCb, 'componentInsetX', 3, 'componentInsetY', 3);

%% Help

cmenu = creer_menu_help(cmenu, [],baseMsg, 'Import_data.html');

%% Menus

msg{2} = 'ImportationFiles';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Fichiers de donn�es', 'Data Files'), 'IconData', 'File_New.png', ...
    'Separator', 'on');

%% Demo files

cmenu = addMenu_L0_DemoFiles(cmenu, msg);

%% Topo / Bathy

cmenu = addMenu_L0_Etopo(cmenu, msg);

%% Hydrology

cmenu = addMenu_L0_Hydro(cmenu, msg);

%% Complementary Layer

msg{2} = 'ImportationLayerComplementaire';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Layer Complementaire', 'Complementary Layer'));

%% Geoig

msg{2} = 'ImportationEGM96';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Geoid height from Earth Gravitational Model 1996');

%% WMS

cmenu = addMenu_L0_WMS(cmenu, msg);

%% Cr�ation de l'IHM

handle = get_handle(this.ihm.Import.cpn);
cmenu = associer(cmenu, h0);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end

%% Association des menus du bouton au menu de la figure

[this.ihm.Handles, hMenuImport] = associer_menuFig(cmenu, Lang('Importation', 'Import'), h0, handle, this.ihm.Handles);
setappdata(hMenuImport, 'IconData', 'File_Load.png');



function cmenu = addMenu_L0_WMS(cmenu, msg)

parent0 = 'WMS_Menu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Serveur de donn�es WMS', 'WMS server'));

msg{2} = 'ImportationWMSInfo';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�couvrie les serveurs de cartographie', 'Discover some WMS servers'));

msg{2} = 'ImportationWMS';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Serveur de donn�es WMS', 'WMS server'));



function cmenu = addMenu_L0_Hydro(cmenu, msg)

parent0 = 'ImportationHydro';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Bathy-C�l�rim�trie', 'Hydrology'));

%% Example Hydro

cmenu = addMenu_L1_Hydro_Levitus(cmenu, parent0, msg);

%% Example WOA

cmenu = addMenu_L1_Hydro_WOA(cmenu, parent0, msg);



function cmenu = addMenu_L0_Etopo(cmenu, msg)

parent0 = 'ImportationEtopo';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Topographie / Bathym�trie', 'Topography / Bathymetry'));

%% Example Etopo

msg{2} = 'ImportationExamplesETOPO_Earth';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Terre', 'Earth'));

msg{2} = 'ImportationExamplesETOPO_France';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('France', 'France'));

msg{2} = 'ImportationExamplesETOPO_MediterraneanOccidental';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mediterran�e Occidentale', 'Western Mediterranean'));

msg{2} = 'ImportationExamplesETOPO_MediterraneanOriental';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mediterran�e Orientale', 'Eastern Mediterranean'));

msg{2} = 'ImportationExamplesETOPO_AtlanticNorth';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Oc�an Atlantique Nord', 'North Atlantic'));

msg{2} = 'ImportationExamplesETOPO_AtlanticSouth';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Oc�an Atlantique Sud', 'South Atlantic'));

msg{2} = 'ImportationExamplesETOPO_IndianOcean';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Oc�an Indien / Mer Arabique', 'Indian Ocean / Arabic Sea'));


function cmenu = addMenu_L0_DemoFiles(cmenu, msg)

parent0 = 'ImportationDemoFiles';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Fichiers de d�mo', 'Demo Files'));

%% Demo Files

cmenu = addMenu_L1_DemoFiles_Extensions(cmenu, parent0, msg);

%% Demo Sounder

cmenu = addMenu_L1_DemoFiles_Sounder(cmenu, parent0, msg);

%% Demo Software

cmenu = addMenu_L1_DemoFiles_Software(cmenu, parent0, msg);



function cmenu = addMenu_L1_DemoFiles_Extensions(cmenu, parent0, msg)

parent1 = 'ImportationDemoFileExtensions';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Tri�s par Extensions', 'By Extensions'));

msg{2} = 'ImportationExamplesSimradAll';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.all (Kongsberg)', '*.all (Kongsberg)'));

msg{2} = 'ImportationExamplesBAG';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.bag ', '*.bag'));

msg{2} = 'ImportationExamplesERMAPPER1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.ers (Er-Mapper)', '*.ers (Er-Mapper)'));

msg{2} = 'ImportationExamplesIMAGE';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.tif, *.jpg, *.gif, ...', '*.tif, *.jpg, *.gif, ...'));

msg{2} = 'ImportationExamplesResonS7k';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '*.s7k (Reson)');

msg{2} = 'ImportationExamplesSonarImage';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.imo, *.mos (Caraibes)', '*.imo, *.mos (Caraibes)'));

msg{2} = 'ImportationExamplesSonarBathy';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.mbb, *.mbg (Caraibes)', '*.mbb, *.mbg (Caraibes)'));

msg{2} = 'ImportationExamplesSonarDTM';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.mnt, *.dtm, *.grid (Caraibes)', '*.mnt, *.dtm, *.grid (Caraibes)'));

msg{2} = 'ImportationExamplesHDR';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '*.hdr, *.flt (ESRI)');

msg{2} = 'ImportationExamplesMATLAB';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.mat (Matlab)', '*.mat (Matlab)'));

msg{2} = 'ImportationExamplesASCII_XYZ';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.xyz ou .txt (Ascii)', '*.xyz or .txt (Ascii)'));

msg{2} = 'ImportationExamplesFledermaus';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.sd (Fledermaus)', '*.sd (Fledermaus)'));

msg{2} = 'ImportationExamplesSRTM';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '*.hgt (SRTM - US shuttle DTM)');

msg{2} = 'ImportationExamplesSARIM';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.im (SARIM)', '*.im (SARIM)'));

msg{2} = 'ImportationExamplesRDF';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '*.rdf (GeoAcoustics GeoSwath)');

msg{2} = 'ImportationExamplesXTF';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '*.xtf (Triton Format)');

msg{2} = 'ImportationExamplesSEGY';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '*.seg (Seismic / Subbottom)');

msg{2} = 'ImportationExamplesBOB';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', '*.bob (BOB from MOVIES)');

msg{2} = 'ImportationExamplesADCPODV';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('*.odv *.dat *.xyz (ADCP - Open Data View Format)', '*.odv *.dat *.xyz (ADCP - Open Data View Format)'));



function cmenu = addMenu_L1_DemoFiles_Sounder(cmenu, parent0, msg)

parent1 = 'ImportationDemoFilesSounder';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Tri�s par Sondeurs', 'By Sounders'));

msg{2} = 'ImportationExamplesEM12D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM12D');

msg{2} = 'ImportationExamplesEM12S';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM12S');

msg{2} = 'ImportationExamplesEM1000';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM1000');

msg{2} = 'ImportationExamplesEM1002';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM1002');

msg{2} = 'ImportationExamplesEM300';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM300');

msg{2} = 'ImportationExamplesEM3000D';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM3000D');

msg{2} = 'ImportationExamplesEM3000S';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM3000S');

msg{2} = 'ImportationExamplesEM120';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'EM120');

msg{2} = 'ImportationExamplesDF1000';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'DF1000');

msg{2} = 'ImportationExamplesDTS1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'DTS1');

msg{2} = 'ImportationExamplesSAR';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'SAR');

msg{2} = 'ImportationExamplesCOSMOS';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'COSMOS');

msg{2} = 'ImportationExamplesRESON';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'RESON');

msg{2} = 'ImportationExamplesGeoacoustics';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', 'GeoAcoustics');



function cmenu = addMenu_L1_DemoFiles_Software(cmenu, parent0, msg)

parent1 = 'ImportationDemoFileSoftware';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Tri�s pas Logiciels', 'By Softwares'));

msg{2} = 'ImportationExamplesCARAIBES';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Caraibes(R)', 'Caraibes(R)'));

msg{2} = 'ImportationExamplesERMAPPER2';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Er-Mapper', 'Er-Mapper'));

msg{2} = 'ImportationExamplesIRAP';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('C-floor & Irap', 'C-floor & Irap'));

msg{2} = 'ImportationExamplesSURFER';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Surfer', 'Surfer'));

msg{2} = 'ImportationExamplesTRITON';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Poseidon & Triton', 'Poseidon & Triton'));

msg{2} = 'ImportationExamplesVMAPPERASCII';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('MapInfo', 'MapInfo'));

msg{2} = 'ImportationExamplesGMT';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('GMT', 'GMT'));

msg{2} = 'ImportationExamplesFLEDERMAUS';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Fledermaus', 'Fledermaus'));



function cmenu = addMenu_L1_Hydro_WOA(cmenu, parent0, msg)

parent1 = 'ImportationWOA';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('WOA13-NOAA', 'WOA13-NOAA'));

msg{2} = 'ImportationExamplesWOAAnnual';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Annuel', 'By Year'));

msg{2} = 'ImportationExamplesWOASeason';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Saison', 'By Season'));

msg{2} = 'ImportationExamplesWOAMonth';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mensuel', 'By Month'));


function cmenu = addMenu_L1_Hydro_Levitus(cmenu, parent0, msg)

parent1 = 'ImportationLevitus';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Levitus', 'Levitus'));

%% Example Levitus

msg{2} = 'ImportationExamplesLevitusAnnual';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Annuel', 'By Year'));

msg{2} = 'ImportationExamplesLevitusSeason';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Saison', 'By Season'));

msg{2} = 'ImportationExamplesLevitusMonth';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mensuel', 'By Month'));
