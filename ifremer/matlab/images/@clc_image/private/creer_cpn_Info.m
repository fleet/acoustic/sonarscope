% Creation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_Info(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_Info(this, baseMsg, hHelp)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
cmenu    = clc_uimenu('typeMenu', 'uimenu', 'componentUserName' , userName, ...
    'componentUserCb'   , userCb, 'componentInsetX', 3, 'componentInsetY', 3);

%% Help

cmenu = creer_menu_help(cmenu, [], baseMsg, 'Data_Information.html');

%% Get Current image

cmenu = addMenu_L0_InfoCurrentImage(cmenu, msg);

%% Set Current image

cmenu = addMenu_L0_SetCurrentImage(cmenu, msg);

%% All images

cmenu = addMenu_L0_SetAllImage(cmenu, msg);

%% SonarScope

cmenu = addMenu_L0_SonarScope(cmenu, msg);

%% Utilities

cmenu = addMenu_L0_Utilities(cmenu, msg);

%% Fonctions

handle = get_handle(this.ihm.Info.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
% associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end

%% Association des menus du bouton au menu de la figure

this.ihm.Handles = associer_menuFig(cmenu, [], hHelp, handle, this.ihm.Handles);

function rep = oppose(X)
if strcmpi(X, 'on')
    rep = 'Off';
else
    rep = 'On';
end


function cmenu = addMenu_L0_InfoCurrentImage(cmenu, msg)

parent0 = 'TagInfoCurrentImage';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Lecture param�tres Image courante', 'Get Current image'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

parent1 = 'TagInfoImage';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Propri�t�s de l''image', 'Properties'));

msg{2} = 'Info_ImageClassic';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Classique', 'Classic way'));

% msg{2} = 'Info_ImageXML';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
%     'label', Lang('Dans un fichier XML', 'In an XML file'));

msg{2} = 'Info_ImageTable';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Editeur de propri�t�s', 'Property editor'));

msg{2} = 'TagInfoFile';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Infos du fichier d''origine', 'Original file description'));

msg{2} = 'Info_SonarData';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Infos des donn�es Sonar', 'Sonar data description'));

msg{2} = 'Info_SonarDataEquation';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Etat de la r�flectivit�', 'Backscatter status'));

msg{2} = 'TagInfoSonarBathymetryStatus';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Etat de la bathymetry', 'Bathymetry status'));

parent1 = 'TagInfoSonarCharacteristics';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Infos des caracteristiques Sonar', 'Sonar characteristics'));

msg{2} = 'TagInfoSonarCharacteristicsLight';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Version simplifi�e', 'Light version'));

msg{2} = 'TagInfoSonarCharacteristicsMedium';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Version compl�te', 'Complete version'));

msg{2} = 'TagInfoSonarEmissionModelsConstructeur';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Modele constructeur des faisceaux d''emission', 'Tx sectors manufacturer model'));

msg{2} = 'TagInfoSonarEmissionModelsCalibration';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Modeles Ifremer des faisceaux d''emission', 'Tx sectors Ifremer model'));

msg{2} = 'TagInfoSonarReceptionModelsConstructeur';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Modele constructeur des faisceaux de reception', 'Rx beams manufacturer model'));

msg{2} = 'TagInfoSonarReceptionModelsCalibration';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Modele Ifremer des faisceaux de reception', 'Rx beams Ifremer model'));

msg{2} = 'TagInfoMapDimensions';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Dimensions d''un trac�', 'Paper dimensions'));



function cmenu = addMenu_L0_SetCurrentImage(cmenu, msg)

parent0 = 'TagInfoSetCurrentImage';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Modification param�tres Image courante', 'Set Current image'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'TagEditParametres';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Propri�t�s String', 'String Properties'));

msg{2} = 'TagEditParametresLists';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Propri�t�s Listes', 'List Properties'));

msg{2} = 'TagRedefineXYAxes';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Red�finition des axes X & Y', 'Redefine X & Y axes'));

msg{2} = 'TagEditCarto';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�finition des param�tres cartographiques', 'Cartographic parameters'));

msg{2} = 'TagEditSonarDescription';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Identification du sondeur', 'Sonar identification'));



function cmenu = addMenu_L0_SetAllImage(cmenu, msg)

parent0 = 'TagInfoAllImages';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Toutes les images', 'All images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'Info_TidyUpImages';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('R�ordonner les images', 'Tidy up images'));

msg{2} = 'Info_SummuryOfLoadedImages';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('R�sum�', 'Summary'));

msg{2} = 'SetPropertiesAllImages';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Modification des propri�t�s', 'Set some Properties'));


function cmenu = addMenu_L0_Utilities(cmenu, msg)

parent0 = 'TagInfoUtilities';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Utilitaires', 'Utilities'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'TagInfoCloseFigures';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Fermeture des figures', 'Close Windows'));

msg{2} = 'TagTipOfTheDay';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Astuce du jour', 'Tip of the day'));

msg{2} = 'TagEditBenchmark';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Performances du PC', 'Benchmark'));

msg{2} = 'TagCreateBlanckFigure';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Cr�ation d''une figure vierge', 'Create a blank figure'));

msg{2} = 'TagResetSizeSScWindow';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Taille par d�faut de la fen�tre de travail', 'Default size for SonarScope Work Window'));

msg{2} = 'CallbackInfoRenameFiles';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Renommage automatique d''un ensemble de fichiers', 'Rename a set of files'));

msg{2} = 'TagFindReplace';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Substitution d''une cha�ne de carat�res dans un fichier', 'Replace a string in a file'));

msg{2} = 'TagRenameWorkWindow';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Renommer cette fen�tre', 'Rename this Window'));

msg{2} = 'TagTestMail';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Tester l''envoi de mails', 'Test email to SSc support'));

%% Test new tools

parent1 = 'TagInfoNewDev';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Test des nouveaux d�veloppements', 'Test new tools'));

msg{2} = 'ExOptimDialogBackscatter';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mod�lisation BS', 'BS model'));

msg{2} = 'ExSignalDialogMultiSectors';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Nettoyage de courbes 1', 'Curves cleaner 1'));

msg{2} = 'ExSignalDialogMultiCurves';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Nettoyage de courbes 2', 'Curves cleaner 2'));

% msg{2} = 'ExSignalDialogMultiCurvesMultiSectors';
% cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
%     'label', Lang('Nettoyage de courbes 2', 'Curves cleaner 2'));

msg{2} = 'ExNavigationDialogAUV';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Nettoyage de la navigation AUV', 'AUV Navigation cleaner'));

msg{2} = 'ExSignalDialogAttitude';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Capteur d''attitude', 'Attitude sensor'));

msg{2} = 'ExSignalDialogSoundSpeedProfile';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Profil de c�l�rit�', 'Sound Speed Profil'));

msg{2} = 'ExNewPAMES';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Nouveau PAMES', 'New PAMES'));

if ~isdeployed
    msg{2} = 'ExNavigationDialogDaurade';
    cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
        'label', Lang('Nettoyage de la navigation Delphins', 'Delphins Navigation cleaner'));
end



function cmenu = addMenu_L0_SonarScope(cmenu, msg)

parent0 = 'TagInfoSonarScope';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('SonarScope', 'SonarScope'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'TagInfoShortcuts';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Raccourcis clavier', 'Shortcuts'));

cmenu = addMenu_L1_SonarScopePreferences(cmenu, parent0, msg);


function cmenu = addMenu_L1_SonarScopePreferences(cmenu, parent0, msg)

global IfrTbxUseClickRightForFlag %#ok<GVMIS>
global NUMBER_OF_PROCESSORS %#ok<GVMIS>

global AlgoSnippets2OneValuePerBeam

if isempty(AlgoSnippets2OneValuePerBeam)
    AlgoSnippets2OneValuePerBeam = 2;
end

parent1 = 'TagPreferences';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Pr�f�rences', 'Preferences'));

%% Language

parent2 = 'TagPreferencesLang';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'Language');

msg{2} = 'TagPreferencesLangFrench';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Fran�ais', 'French'));

msg{2} = 'TagPreferencesLangEnglish';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Anglais', 'English'));

%% GUI Selection Image

parent2 = 'TagPreferencesGUISelectImage';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', 'GUI Select Images');

msg{2} = 'TagPreferencesGUISelectImageClassical';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Classique', 'Classical'));

msg{2} = 'TagPreferencesGUISelectImageSimplified';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Simplifi�', 'Simplified'));

%% ClickRightForFlag

parent2 = 'TagPreferencesClickRightForFlag';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Activation du click droit sur les pixels', 'Click Right activation for pixels'));

msg{2} = 'TagPreferencesClickRightForFlagYes';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', 'Yes', 'Checked', IfrTbxUseClickRightForFlag);

msg{2} = 'TagPreferencesClickRightForFlagNo';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', 'No', 'Checked', oppose(IfrTbxUseClickRightForFlag));

%% Appel � des mex fonctions pour acc�l�rer les calculs

parent2 = 'TagPreferencesMexFiles';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Acc�l�ration logicielle', 'Software acceleration'));

NUMBER_OF_PROCESSORS = str2double(getenv('NUMBER_OF_PROCESSORS'));

% TODO : Test JM le 26/02/2012 : on garde un processeur pour avoir un peu la main
% sur les autres t�ches. A �valuer si c'est pertinent (Fledermaus fait
% comme �a)
% Si N=2 on fait quand m�me de l'acc�l�ration logicielle sur 2 processeurs
if NUMBER_OF_PROCESSORS > 2
    NUMBER_OF_PROCESSORS = NUMBER_OF_PROCESSORS - 1;
    %{
    % Ajout JMA le 03/07/2012 pour pouvoir profiter de la parallel toolbox
    % dans blockproc(..., 'UseParallel', 1) et avec le parfor
    try % Au cas o� SonarScope serait utilis� en version d�veloppement sans avoir la parralel toolbox (Ex au NIWA)
        flagPool = isempty(gcp('nocreate'));
        if (NUMBER_OF_PROCESSORS > 1) && flagPool
            %             matlabpool(NUMBER_OF_PROCESSORS) % Comment� pour le moment
            %             tant que c'est pas ma�tris�
        end
    catch %#ok<CTCH>
    end
    %}
end

msg{2} = 'TagPreferencesMexFilesYes';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Oui', 'Yes'), 'Checked', 'On');

msg{2} = 'TagPreferencesMexFilesNo';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Non', 'No'), 'Checked', 'Off');

%% IdentAlgoSnippets2OneValuePerBeam

parent2 = 'TagIdentAlgoSnippets2OneValuePerBeam';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Algo snippets vers valeur par faisceau', 'Snippets to PingBeam averaging method'));

msg{2} = 'TagIdentAlgoSnippets2OneValuePerBeam_dB';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', 'dB', 'Checked', 'Off');

msg{2} = 'TagIdentAlgoSnippets2OneValuePerBeam_Amp';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', 'Amp', 'Checked', 'Off');

msg{2} = 'TagIdentAlgoSnippets2OneValuePerBeam_Enr';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', 'Enr', 'Checked', 'Off');

msg{2} = 'TagIdentAlgoSnippets2OneValuePerBeam_Med';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', 'Median', 'Checked', 'Off');

%% LevelQuestions

parent2 = 'TagPreferencesLevelQuestions';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Niveau des questions', 'Questions level'));

msg{2} = 'TagPreferencesLevelQuestions1';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Simplifi�', 'Simplified'));

msg{2} = 'TagPreferencesLevelQuestions2';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Interm�diaire', 'Intermediate'));

msg{2} = 'TagPreferencesLevelQuestions3';
cmenu = ajouter(cmenu, 'parent', parent2, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Confirm�', 'Advanced'));

%% GraphicsOnTop

parent2 = 'TagPreferencesGraphicsOnTop';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Trac� des graphiques sur l''image', 'Plot graphics on Image'));

msg{2} = 'TagPreferencesGraphicsOnTop1';
cmenu = ajouter(cmenu,  'tag', msg{2}, 'message', msg, ...
    'parent', parent2, 'label', 'On');

msg{2} = 'TagPreferencesGraphicsOnTop2';
cmenu = ajouter(cmenu,  'tag', msg{2}, 'message', msg, ...
    'parent', parent2, 'label', 'Off');

%% UseLogFile

parent2 = 'TagPreferencesUseLogFile';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', parent2, ...
    'label', Lang('Fichier Log', 'Log file'));

msg{2} = 'TagPreferencesUseLogFileYes';
cmenu = ajouter(cmenu,  'tag', msg{2}, 'message', msg, ...
    'parent', parent2, 'label', 'On', 'Checked', 'Off');

msg{2} = 'TagPreferencesUseLogFileNo';
cmenu = ajouter(cmenu,  'tag', msg{2}, 'message', msg, ...
    'parent', parent2, 'label', 'Off', 'Checked', 'On');

%% Temporary Directory

msg{2} = 'TagPreferencesTempDir';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ref�finir le r�pertoire temporaire', 'Set temporary directory'));

msg{2} = 'TagModifySizePhysTotalMemory';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Modification de la taille maximale d''une image en m�moire.', 'Modify the maximum size on an image in memory.'));
