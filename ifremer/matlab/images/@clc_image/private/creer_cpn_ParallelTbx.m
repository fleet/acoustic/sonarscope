function this = creer_cpn_ParallelTbx(this, baseMsg)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
objMenu = clc_uimenu('componentUserName', userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);
cmenu = objMenu;

% -------------------------- Cr�ation du menu --------------------------

msg{2} = 'ParallelTbxOnMax';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Parallel Tbx On / maximum number of workers');

msg{2} = 'ParallelTbxOn';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Parallel Tbx On / select number of workers');

msg{2} = 'ParallelTbxOff';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Parallel Tbx Off');

% -------------------------- Fonctions --------------------------

handle = get_handle(this.ihm.ParallelTbx.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end
