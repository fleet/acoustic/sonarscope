function this = creer_cpn_QuestionLevel(this, baseMsg)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
objMenu = clc_uimenu('componentUserName', userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);
cmenu = objMenu;

% -------------------------- Cr�ation du menu --------------------------

msg{2} = 'QL1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Simplified');

msg{2} = 'QL2';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Intermediate');

msg{2} = 'QL3';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', 'Advanced');

% -------------------------- Fonctions --------------------------

handle = get_handle(this.ihm.QuestionLevel.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end
