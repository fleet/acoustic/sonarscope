% Creation du composant de menu des traitements
%
% Syntax
%   this = creer_cpn_Stats(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

% TODO : Renommer tous les Tag � la mani�re de StatsCurvesSpecificBackscatter

function this = creer_cpn_Stats(this, baseMsg, hStats)

msg{1} = baseMsg;

userName = get(this, 'componentUserName');
userCb   = get(this, 'componentUserCb');
cmenu    = clc_uimenu('typeMenu', 'uimenu', 'componentUserName' , userName, ...
    'componentUserCb'   , userCb, 'componentInsetX', 3, 'componentInsetY', 3);

%% Help

cmenu = creer_menu_help(cmenu, [], baseMsg, 'Statistics.html');

%% Statistiques �l�mentaires

cmenu = addMenu_L0_StatsImage(cmenu, msg);

%% Courbes

cmenu = addMenu_L0_Curves(cmenu, msg);

%% RMS

msg{2} = 'STATS_EQM_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Erreur Quadratique Moyenne', 'Root Mean Square error'), ...
    'Separator', 'on');

%% Histo 2D

msg{2} = 'STATS_Histo2D_CI1';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Histo bidimensionnel', 'Bidimensional Histogram'));

%% Locate particular values

cmenu = addMenu_L0_LocateValues(cmenu, msg);

%% Statistiques multi_images

cmenu = ajouter(cmenu, 'tag', 'STATS_MultiImages', ...
    'label', Lang('Multi images', 'Multi images'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'STATS_3MultiImagesBoxPlots_CI0';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Box plots', 'Box plots'));

msg{2} = 'STATS_3MultiImagesCurves_CI0';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Courbes', 'Curves'));

% if ~isdeployed
%     msg{2} = 'STATS_3MultiImagesPointAndClickCurve_CI0';
%     cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
%         'label', Lang('Associer le "Point & Click" � une courbe instantan�e', 'Link "Point & Click" to an instantanenous curve'));
% end

%% Edition

handle = get_handle(this.ihm.Stats.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
% associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end

%% Association des menu du bouton au menu de la figure

this.ihm.Handles = associer_menuFig(cmenu, [], hStats, handle, this.ihm.Handles);


function cmenu = addMenu_L0_LocateValues(cmenu, msg)

parent0 = 'STATS_SearchValues';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Localisation de valeurs particuli�res', 'Locate particular values'));

msg{2} = 'STATS_RechercheMin_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Recherche de la valeur minimale (<)', 'Search min value (<)'));

msg{2} = 'STATS_RechercheMax_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Recherche de la valeur maximale (>)', 'Search max value (>)'));

msg{2} = 'STATS_RechercheMedian_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Recherche de la valeur m�diane', 'Search median value'));


function cmenu = addMenu_L0_Curves(cmenu, msg)

parent0 = 'STATS8MultiCriteres';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Courbes multi-crit�res', 'Curves'));

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'TagStatsConditionComputation', ...
    'label', Lang('Calcul des courbes', 'Compute curves'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'STATS_ConditionCalculFull_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Classique', 'Classical'));

parent1 = 'TagStatsConditionCalculBiasChoix';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label',  Lang('Pour compensation', 'For compensation'));

msg{2} = 'STATS_ConditionCalculBias_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Valeur globale', 'One single value'));

msg{2} = 'STATS_ConditionCalculBiasMultiple_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Valeurs diff�renti�es par courbe', 'Multiple values'));

msg{2} = 'STATS_ConditionUnion_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyennage de plusieurs courbes', 'Mean curve from several ones'));

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'TagStatsConditionManagement', ...
    'label', Lang('Gestion des courbes', 'Manage curves'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

cmenu = addMenu_L1_CurvesPlot(cmenu,parent0,  msg);

msg{2} = 'STATS_ConditionClean_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Edition', 'Edit'));

% msg{2} = 'STATS_ConditionFilter_CI0';
% cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
%     'label', Lang('Filtrage', 'Filter'));

msg{2} = 'STATS_ConditionDelete_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Suppression', 'Delete'));

msg{2} = 'STATS_ConditionExport_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Exportation', 'Export'));

msg{2} = 'STATS_ConditionImport_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Importation', 'Import'));

msg{2} = 'STATS_ConditionRename_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Renommage', 'Rename'));

msg{2} = 'STATS_ConditionSoustraction_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Soustractions', 'Subtraction'));

msg{2} = 'STATS_StatsInfo_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Information', 'Info'));

%% Autres exports

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'STATS_Exports', ...
    'label', Lang('I/O externes', 'External I/O'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'STATS_ConditionExport_ASCII_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Exportation XML', 'Export XML'));

msg{2} = 'STATS_ConditionImport_ASCII_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Importation XML', 'Import XML'));

msg{2} = 'STATS_ConditionImport_CSV_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Import des donn�es � partir d''un .csv', 'Import values from a .csv'));

%% Courbes publiques

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'STATS_Public', ...
    'label', Lang('Courbes publiques', 'Public curves'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'STATS_PublicSet_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Publier', 'Publish'));

msg{2} = 'STATS_PublicDelete_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Suppression', 'Delete'));

%% Sp�cial Backscatter

cmenu = ajouter(cmenu, 'parent', parent0, 'tag', 'StatsCurvesSpecificBackscatter', ...
    'label', Lang('Sp�cial Backscatter & Bathy', 'Specific Backscatter & Bathy'), ...
    'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'STATS_PlotAzimuth_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Trac� des courbes / azimut', 'Plot curves / Azimuth'));

%% Aide pour la calibration

% cmenu = ajouter(cmenu, 'parent', parent0, 'tag',  'TagStatsConditionPlotSpecials', ...
%     'label', Lang('Aide � la calibration', 'Help for calibration'), ...
%     'Separator', 'on', 'ForegroundColor', 'b');

msg{2} = 'STATS_ConditionPlotPlusOffsets_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Trac� des courbes d�call�es d''un offset', 'Plot curves with offsets'));

msg{2} = 'STATS_ConditionReplaceCurveByOffsets_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Substitution des courbes par des offsets (Secteurs Kongsberg)', 'Replace curves by constants (useful for Kongsberg sectors)'));

msg{2} = 'STATS_BathyStats2QF_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Stats de bathy -> QF', 'Bathy Stats -> QF'));


function cmenu = addMenu_L0_StatsImage(cmenu, msg)

parent0 = 'STATS_Elementary';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Valeurs Intra-image', 'Current Image Values'), ...
    'Separator', 'on');

msg{2} = 'STATS_General_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne, m�diane, std, ...', 'Mean, median, std, ...'));

msg{2} = 'STATS_Histogramme_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Histogramme (H)', 'Histogram (H)'));

msg{2} = 'STATS_Hypsometric_CI1';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('courbe hypsometrique', 'Hypsometric curve'));

cmenu = addMenu_L1_StatsVert(cmenu, parent0, msg);
cmenu = addMenu_L1_StatsHorz(cmenu, parent0, msg);

msg{2} = 'STATS_RecomputeStats_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Recalcul des statstistiques', 'Reprocess statistics'));

cmenu = addMenu_L1_StatsOthers(cmenu, parent0, msg);


function cmenu = addMenu_L1_CurvesPlot(cmenu,parent0,  msg)

parent1 = 'STATS_Plots';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Visualisation', 'Display'));

msg{2} = 'STATS_ConditionPlot_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne uniquement', 'Mean only'));

msg{2} = 'STATS_ConditionPlotMedian_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('M�diane uniquement', 'Median only'));

msg{2} = 'STATS_ConditionPlotWithSigma_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne �  1*Ecart-type', 'Mean � 1*Standard deviation'));

msg{2} = 'STATS_ConditionPlotWith2Sigma_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne � 2*Ecart-type', 'Mean � 2*Standard deviation'));

msg{2} = 'STATS_ConditionPlotSigma_CI0';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Ecart-type uniquement', 'Standard deviation only'));


function cmenu = addMenu_L1_StatsVert(cmenu, parent0, msg)

parent1 = 'STATS_VertDirection';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Direction Verticale', 'Vertical Direction'));

msg{2} = 'STATS_MoyenneVert_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne (v)', 'Average (v)'));

msg{2} = 'STATS_MedianVert_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mediane', 'Median'));

msg{2} = 'STATS_NbPointsVert_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Nombre de pixels', 'Nb Pixels'));

msg{2} = 'STATS_MinVert_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Valeur Min', 'Min Val'));

msg{2} = 'STATS_MaxVert_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Valeur Max', 'Max Val'));


function cmenu = addMenu_L1_StatsHorz(cmenu, parent0, msg)

parent1 = 'STATS_HorzDirection';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Direction Horizontale', 'Horizontal Direction'));

msg{2} = 'STATS_MoyenneHorz_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Moyenne (h)', 'Average (h)'));

msg{2} = 'STATS_MedianHorz_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Mediane', 'Median'));

msg{2} = 'STATS_NbPointsHorz_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Nombre de pixels', 'Nb Pixels'));

msg{2} = 'STATS_MinHorz_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Valeur Min', 'Min Val'));

msg{2} = 'STATS_MaxHorz_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Valeur Max', 'Max Val'));


function cmenu = addMenu_L1_StatsOthers(cmenu, parent0, msg)

parent1 = 'STATS_Others';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', parent1, ...
    'label', Lang('Autres', 'Other'));

msg{2} = 'STATS_DensiteDeProbabilite_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Densit� De Probabilit�', 'Probability density'));

msg{2} = 'STATS_MelangeDeGaussiennes_CI1';
cmenu = ajouter(cmenu, 'parent', parent1, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('M�lange de Gaussiennes', 'Gaussian mixture model'));
