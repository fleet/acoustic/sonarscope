% Creation du composant de menu des traitements
% 
% Syntax
%   this = creer_cpn_TypeInteractionSouris(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_TypeInteractionSouris(this, baseMsg)

msg{1} = baseMsg;
msg{2} = 'TypeInteractionSouris';

userName  = get(this, 'componentUserName');
userCb    = get(this, 'componentUserCb');
objMenu = clc_uimenu( 'componentUserName' , userName, ...
    'componentUserCb'   , userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);
cmenu = objMenu;

% -------------------------- Cr�ation du menu --------------------------

msg{3} = 'PointClick';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', 'Point & Click (p)');

msg{3} = 'ZoomInteractif';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', 'Zoom (z)');

msg{3} = 'Centrage';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Centre', 'Center'));

msg{3} = 'Coupe';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Profil', 'Profile'));

msg{3} = 'SetNaN';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Set NaN', 'Set NaN'));

msg{3} = 'SetVal';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Set Val', 'Set Val'));

msg{3} = 'MoveProfils';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Profils mobiles', 'Move profils'));

msg{3} = 'DrawHeight';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Trac� du signal de hauteur', 'Draw height signal'));

msg{3} = 'CleanData';
cmenu = ajouter(cmenu, 'tag', msg{3}, 'message', msg, ...
    'label', Lang('Nettoyage', 'Clean data'));

% -------------------------- Fonctions --------------------------

handle = get_handle(this.ihm.TypeInteractionSouris.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end
