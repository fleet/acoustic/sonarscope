% Creation du composant de menu des traitements
% 
% Syntax
%   this = creer_cpn_ZoneEtude(this, baseMsg)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_ZoneEtude(this, baseMsg)

msg{1} = baseMsg;

userName  = get(this, 'componentUserName');
userCb    = get(this, 'componentUserCb');
objMenu = clc_uimenu( 'componentUserName', userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);

cmenu = objMenu;

cmenu = ajouter(cmenu, 'tag', 'TagZoneEtudeCurrentImage', ...
    'label', Lang('Image courante', 'Current Image'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = 'TagZoneEtudeDefinition';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('D�finition d''une zone d''�tude', 'Define a study zone'));

msg{2} = 'TagZoneEtudeCrop';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Encadrement de l''image', 'Crop image'));

msg{2} = 'TagZoneEtudeChoix';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Selection', 'Selection'));

msg{2} = 'TagZoneEtudeDelete';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Destruction de zones d''�tude', 'Delete study zones'));

msg{2} = 'TagZoneEtudeExport';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Exportation de zones d''�tude', 'Export study zones'));

msg{2} = 'TagZoneEtudeImport';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Importation de zones d''�tude', 'Import study zones'));

cmenu = ajouter(cmenu, 'tag', 'TagZoneEtudeAllImages', ...
    'label', Lang('Images multiples', 'Multi Images'),  ...
    'Separator', 'On', 'ForegroundColor', 'b');

msg{2} = 'TagZoneEtudeCropMultiImagesInt';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Plus petite zone commune', 'Smallest crop'));

msg{2} = 'TagZoneEtudeCropMultiImagesExt';
cmenu = ajouter(cmenu, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Plus grande zone commune', 'Largest crop'));

% -------------------------- Fonctions --------------------------

handle = get_handle(this.ihm.ZoneEtude.cpn);
cmenu = associer(cmenu, handle);
cmenu = editobj(cmenu);
cmenu = desassocier(cmenu);
associer(cmenu, handle);

for k=1:get_nbHandles(cmenu)
	this.ihm.Handles.(get_handleFieldName(cmenu, k)) = get_handleValue(cmenu, k);
end
