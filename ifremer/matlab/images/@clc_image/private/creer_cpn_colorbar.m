% Creation du composant colorbar
% 
% Syntax
%   this = creer_cpn_colorbar(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_colorbar(this)

tag1 = [get(this, 'componentName'), ' / clc_image / '];

%% Construction de l'instance

Tag = [tag1, 'ancre_Colorbar'];
this.ihm.colorbar.cpn = clc_colorbar([]);
this.ihm.colorbar.cpn = set_inset_x(      this.ihm.colorbar.cpn, 1);
this.ihm.colorbar.cpn = set_inset_y(      this.ihm.colorbar.cpn, 1);
this.ihm.colorbar.cpn = set_name(         this.ihm.colorbar.cpn, Tag);
this.ihm.colorbar.cpn = set_handleAnchor( this.ihm.colorbar.cpn, this.hColorBar);
% this.ihm.colorbar.cpn = set_user_name(    this.ihm.colorbar.cpn, get_user_name(this));
this.ihm.colorbar.cpn = set_user_name(    this.ihm.colorbar.cpn, get_user_name(this.composant));
% this.ihm.colorbar.cpn = set_user_cb(      this.ihm.colorbar.cpn, get_user_cb(this));
this.ihm.colorbar.cpn = set_user_cb(      this.ihm.colorbar.cpn, get_user_cb(this.composant));

%% Edition graphique de la toolbar

this.ihm.colorbar.cpn = editobj(this.ihm.colorbar.cpn);

%% Construction de la colorbar

set(this.hfig, 'CurrentAxes', this.hAxePrincipal);

hColorBar = colorbar('vert');
this.ihm.colorbar.cpn = copy(this.ihm.colorbar.cpn, hColorBar);
if ishandle(hColorBar)
    delete(hColorBar);
end
