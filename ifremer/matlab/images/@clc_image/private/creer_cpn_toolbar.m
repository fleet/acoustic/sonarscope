% Creation des composants de la toolbar
%
% Syntax
%   this = creer_cpn_toolbar(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% -------------------------------------------------------------------------

function this = creer_cpn_toolbar(this)

strRandom = this.internalNumber;
tag1      = [get(this, 'componentName'), ' / clc_image ' strRandom ' / '];
userName  = get(this, 'componentUserName');
userCb    = get(this, 'componentUserCb');

%% Create initialisation instances in order to avoid many repetitions

pushbutton = clc_pushbutton('componentUserName', userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 1, 'componentInsetY', 1);

togglebutton = clc_togglebutton('componentUserName', userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 1, 'componentInsetY', 1);

value = clc_label_value2('componentUserName' , userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);

texte = clc_edit_texte('componentUserName' , userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);

popup  = clc_popup('componentUserName' , userName, ...
    'componentUserCb', userCb, ...
    'componentInsetX', 3, 'componentInsetY', 3);

%% Create menus in the toolbar

hImportExport  = uimenu(this.hfig, 'Text', Lang('Fichier', 'File'));
hStats         = uimenu(this.hfig, 'Text', Lang('Statistiques', 'Statistics'));
hGeneralProces = uimenu(this.hfig, 'Text', Lang('Traitements g�n�raux', 'General processings'));
hSonarProces   = uimenu(this.hfig, 'Text', Lang('Traitements sp�cialis�s Sonar & MBES', 'Sonar processings'));

%% Create VisuImage

icone = get_icone(this, 'VisuImage');
msg = [tag1, 'VisuImage'];
a = pushbutton;
a = set_tooltip(a, Lang('Visualisation sous forme d''image', 'Visualization as an image'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_VisuImage';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.VisuImage.cpn = editobj(a);
this.ihm.VisuImage.msg = msg;
this = creer_cpn_TypeVisuImage(this, msg);

%% Create QuestionLevel 

icone = get_icone(this, 'QL2'); % TODO
msg = [tag1, 'QuestionLevel'];
a = pushbutton;
a = set_tooltip(a, Lang('Question level (1=Simplified / 2=Intermediate / 3=Advanced)', ...
    'Question level (1=Simplified / 2=Intermediate / 3=Advanced)'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_QuestionLevel';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.QuestionLevel.cpn = editobj(a);
this.ihm.QuestionLevel.msg = msg;
this = creer_cpn_QuestionLevel(this, msg);

%% Create ancre_ParallelTbx

icone = get_icone(this, 'ParallelTbxOn'); % TODO
msg = [tag1, 'ParallelTbx'];
a = pushbutton;
a = set_tooltip(a, 'Parallel Tbx On/Off');
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_ParallelTbx';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ParallelTbx.cpn = editobj(a);
this.ihm.ParallelTbx.msg = msg;
this = creer_cpn_ParallelTbx(this, msg);

%% Create Visu3D 

% icone = get_icone(this, '3D');
% msg = [tag1, 'Visu3D'];
% a = pushbutton;
% a = set_tooltip(a, Lang('Visualisation 3D', '3D visualization'));
% a = set_msg_click(a, msg);
% a = set(a, 'BusyAction', 'cancel');
% a = set_CData(a, IconeEncocheGauche(icone));
% a = set_name(a, 'ancre_cpn_Visu3D');
% this.ihm.Visu3D.cpn = editobj(a);
% this.ihm.Visu3D.msg = msg;

%% Create Clone 

icone = get_icone(this, 'Clone');
msg = [tag1, 'CloneWindow'];
a = pushbutton;
a = set_tooltip(a, Lang('Cl�nage de la fen�tre', 'Clone the window'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Visu3D';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.CloneWindow.cpn = editobj(a);
this.ihm.CloneWindow.msg = msg;

%% Create VisuPlot

icone = get_icone(this, 'VisuPlot');
msg = [tag1, 'VisuPlot'];
a = pushbutton;
a = set_tooltip(a, Lang('Courbes sur image zoom�e', 'Curves on current extent'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_VisuPlot';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.VisuPlot.cpn = editobj(a);
this.ihm.VisuPlot.msg = msg;

%% Create VisuPlotVoisinsY

icone1 = get_icone(this, 'VisuPlot');
icone = zeros(size(icone1), 'uint8') + 255;
icone(6:16,:,:) = icone1(1:2:end,:,:);
msg = [tag1, 'VisuPlotVoisinsY'];
a = pushbutton;
a = set_tooltip(a, Lang('Courbes sur les lignes du pixel', 'Curves on the neighboor rows of the pixel'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_VisuPlotVoisinsY';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.VisuPlotVoisinsY.cpn = editobj(a);
this.ihm.VisuPlotVoisinsY.msg = msg;

%% Create VisuPlotVoisinsX

icone1 = get_icone(this, 'VisuPlot');
icone = zeros(size(icone1), 'uint8') + 255;
icone(6:16,:,:) = icone1(1:2:end,:,:);
icone = my_transpose(icone);
msg = [tag1, 'VisuPlotVoisinsX'];
a = pushbutton;
a = set_tooltip(a, Lang('Courbes sur les colonnes du pixel', 'Curves on the neighboor columns of the pixel'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_VisuPlotVoisinsX';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.VisuPlotVoisinsX.cpn = editobj(a);
this.ihm.VisuPlotVoisinsX.msg = msg;

%% Create Import

icone = get_icone(this, 'Import');
msg   = [tag1, 'Import'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set_tooltip(a, Lang('Importation de donn�es', 'Import data'));
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Import';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Import.cpn = editobj(a);
this.ihm.Import.msg = msg;
this = creer_cpn_Import(this, msg, hImportExport);

%% Create Export

icone = get_icone(this, 'Export');
msg   = [tag1, 'Export'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set_tooltip(a, Lang('Exportation de donn�es', 'Export data'));
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Export';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Export.cpn = editobj(a);
this.ihm.Export.msg = msg;
this = creer_cpn_Export(this, msg, hImportExport);

%% Create Column

msgEdit  = [tag1, 'IX Edit'];
msgPlus  = [tag1, 'IX Plus'];
msgMoins = [tag1, 'IX Moins'];
a = clc_label_inc_value(...
    'componentInsetX',  0, ...
    'componentInsetY',  0, ...
    'titre',            Lang('Colonne', 'Column'),...
    'unite',            '',...
    'nbColTitre',       1, ...
    'nbColValue',       3, ...
    'nbColIncValue',    3, ...
    'nbColUnite',       0, ...
    'value',            1,...
    'format',           '%d', ...
    'minValue',         1, ...
    'maxValue',         this.Images(this.indImage).nbColumns, ...
    'tooltip',          Lang('Num�ro de colonne', 'Column number'),...
    'editable',         1,...
    'actionnable',      0, ...
    'msgEdit',          msgEdit, ...
    'msgPlus',          msgPlus, ...
    'msgMoins',         msgMoins, ...
    'tag',              'ancre label_inc_value', ...
    'componentUserName',    userName,...
    'componentUserCb',      userCb);
Tag = 'ancre_cpn_IX';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.IX.cpn = editobj(a);
this.ihm.IX.msgEdit  = msgEdit;
this.ihm.IX.msgPlus  = msgPlus;
this.ihm.IX.msgMoins = msgMoins;

%% Create Line

msgEdit  = [tag1, 'IY Edit'];
msgPlus  = [tag1, 'IY Plus'];
msgMoins = [tag1, 'IY Moins'];
a = clc_label_inc_value(...
    'componentInsetX',  0, ...
    'componentInsetY',  0, ...
    'titre',            Lang('Ligne', 'Line'),...
    'unite',            '',...
    'nbColTitre',       1, ...
    'nbColValue',       3, ...
    'nbColIncValue',    3, ...
    'nbColUnite',       0, ...
    'value',            1,...
    'format',           '%d', ...
    'minValue',         1, ...
    'maxValue',         this.Images(this.indImage).nbRows, ...
    'tooltip',          Lang('Num�ro de ligne', 'Line number'),...
    'editable',         1, ...
    'actionnable',      0, ...
    'msgEdit',          msgEdit, ...
    'msgPlus',          msgPlus, ...
    'msgMoins',         msgMoins, ...
    'tag',              'ancre label_inc_value', ...
    'componentUserName',    userName,...
    'componentUserCb',      userCb);
Tag = 'ancre_cpn_IY';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.IY.cpn = editobj(a);
this.ihm.IY.msgEdit  = msgEdit;
this.ihm.IY.msgPlus  = msgPlus;
this.ihm.IY.msgMoins = msgMoins;

%% Create NomImage

msg = [tag1, 'NomImage'];
a = texte;
a = set_texte(a, this.Images(this.indImage).Name);
a = set_tooltip(a, Lang('Le nom de l''image peut �tre modifi� ici.', 'The name of the image can be modified here'));
a = set_editable(a, 1);
a = set_msg_edit(a, msg);
Tag = 'ancre_cpn_NomImage';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.NomImage.cpn = editobj(a);
this.ihm.NomImage.msg = msg;

%% Create IdentImage

msg = [tag1, 'IdentImage'];
a = pushbutton;
a = set_texte(a, Lang('Choix de l''image', 'Displayed image selection'));
a = set_tooltip(a, Lang('S�lection de l''image courante', 'Select the image you want to display'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
Tag = 'ancre_cpn_IdentImage';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.IdentImage.cpn = editobj(a);
this.ihm.IdentImage.msg = msg;

%% Create ValX

msg = [tag1, 'ValX'];
a = value;
a = set_titre(a, this.Images(this.indImage).XLabel);
a = set_unite(a, this.Images(this.indImage).XUnit);
% a = set_format(a, '%1.2f');
a = set_nb_col_titre(a, 1);
a = set_nb_col_value(a, 2);
a = set_nb_col_unite(a, 1);
a = set_msg_edit(a, msg);
a = set_tooltip(a, Lang('Abscisse du point sur l''axe x', 'Point abscissa in x axis'));
Tag = 'ancre_cpn_ValX';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ValX.cpn = editobj(a);
this.ihm.ValX.msg = msg;

%% Create ValY

msg = [tag1, 'ValY'];
a = value;
a = set_titre(a, this.Images(this.indImage).YLabel);
a = set_unite(a, this.Images(this.indImage).YUnit);
% a = set_format(a, '%1.2f');
a = set_nb_col_titre(a, 1);
a = set_nb_col_value(a, 2);
a = set_nb_col_unite(a, 1);
a = set_msg_edit(a, msg);
a = set_tooltip(a, Lang('Ordonn�e du point sur l''axe y', 'Point ordinate in y axis'));
Tag = 'ancre_cpn_ValY';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ValY.cpn = editobj(a);
this.ihm.ValY.msg = msg;

%% Create ValImage

msg = [tag1, 'ValImage'];
a = value;
a = set_titre(a, 'Val');
% a = set_format(a, '%1.2f');
a = set_nb_col_titre(a, 1);
a = set_nb_col_value(a, 2);
a = set_nb_col_unite(a, 1);
a = set_msg_edit(a, msg);
a = set_tooltip(a, Lang('Valeur du point', 'Point value'));
a = set_unite(a, this.Images(this.indImage).Unit);
Tag = 'ancre_cpn_ValImage';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ValImage.cpn = editobj(a);
this.ihm.ValImage.msg = msg;

%% Create Delete

icone = get_icone(this, 'Delete');
msg = [tag1, 'Delete'];
a = pushbutton;
a = set_tooltip(a, Lang('Suppression de cette image ou d''autres images', 'Remove this image or another ones'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
%     a = set_texte(a, 'Delete');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Delete';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Delete.cpn = editobj(a);
this.ihm.Delete.msg = msg;

%% Create CoupeHorz

icone = get_icone(this, 'CoupeHorz');
msg = [tag1, 'CoupeHorz'];
a = togglebutton;
a = set_etat(a, this.visuCoupeHorz);
a = set_tooltip(a, Lang('Profil horizontal', 'Show or hide horizontal profile'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_CoupeHorz';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.CoupeHorz.cpn = editobj(a);
this.ihm.CoupeHorz.msg = msg;

%% Create CoupeVert

icone = get_icone(this, 'CoupeVert');
msg = [tag1, 'CoupeVert'];
a = togglebutton;
a = set_etat(a, this.visuCoupeVert);
a = set_tooltip(a, Lang('Profil vertical (V)', 'Show or hide vertical profile (V)'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_CoupeVert';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.CoupeVert.cpn = editobj(a);
this.ihm.CoupeVert.msg = msg;

%% Create NbVoisins

msg = [tag1, 'NbVoisins'];
a = value;
a = set_titre(a, Lang('Neighbourhood', 'Voisinage'));
a = set_value(a, this.NbVoisins);
a = set_min_value(a, 0);
a = set_format(a, '%d');
a = set_nb_col_titre(a, 0);
a = set_nb_col_value(a, 1);
a = set_nb_col_unite(a, 0);
a = set_msg_edit(a, msg);
a = set_tooltip(a, Lang('Nombre de voisins avant et apr�s le point courant sur les profils horizontaux et verticaux.', ...
    'Number of neigbours before and after horizontal and vertical profiles and pixel printing'));
Tag = 'ancre_cpn_NbVoisins';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.NbVoisins.cpn = editobj(a);
this.ihm.NbVoisins.msg = msg;

%% Create Stats

icone = get_icone(this, 'Stats');
msg    = [tag1, 'Stats'];
a = pushbutton;
a = set_tooltip(a, Lang('Traitements statistiques', 'Statistic processings'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_Stats';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));

this.ihm.Stats.cpn = editobj(a);
this.ihm.Stats.msg = msg;
this = creer_cpn_Stats(this, msg, hStats);

%% Create ComputeSonar

icone = get_icone(this, 'ComputeSonar');
msg = [tag1, 'CallbackSonar'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
%     a = set_texte(a, 'Traitements');
a = set_tooltip(a, Lang('Traitements sp�cialis�s Sonar & MBES', 'Sonar processings'));
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_ComputeSonar';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));

this.ihm.ComputeSonar.cpn = editobj(a);
this.ihm.ComputeSonar.msg = msg;
this = creer_cpn_ComputeSonar(this, msg, hSonarProces);

%% Create Compute

icone = get_icone(this, 'Compute');
msg = [tag1, 'Compute'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_tooltip(a, Lang('Traitements g�n�raux', 'General processings'));
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_Compute';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Compute.cpn = editobj(a);
this.ihm.Compute.msg = msg;
this = creer_cpn_Compute(this, msg, hGeneralProces);

%% Create ZoneEtude

icone = get_icone(this, 'ZoneEtude');
msg = [tag1, 'ZoneEtude'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set_tooltip(a, Lang('Zone d''�tude', 'Study area'));
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_ZoneEtude';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));

this.ihm.ZoneEtude.cpn = editobj(a);
this.ihm.ZoneEtude.msg = msg;
this = creer_cpn_ZoneEtude(this, msg);

%% Create XDir

icone = get_icone(this, 'XDir');
msg = [tag1, 'lr'];
etat = this.Images(this.indImage).XDir-1;
if etat == 1
    icone = my_fliplr(icone);
end
a = togglebutton;
a = set_etat(a, etat);
a = set_tooltip(a, Lang('Inversion gauche/droite', 'Left / Right inversion'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_XDir';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.XDir.cpn = editobj(a);
this.ihm.XDir.msg = msg;

%% Create YDir

icone = get_icone(this, 'YDir');
msg = [tag1, 'xy'];
etat = this.Images(this.indImage).YDir-1;
if etat == 1
    icone = my_flipud(icone);
end
a = togglebutton;
a = set_etat(a, etat);
a = set_tooltip(a, Lang('Inversion Haut/Bas', 'Top / Bottom inversion'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_YDir';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.YDir.cpn = editobj(a);
this.ihm.YDir.msg = msg;

%% Create ViewImage

msg = [tag1, 'ViewImage'];
if ~isdeployed
    icone = get_icone(this, 'ImageTool');
    a = togglebutton;
    a = set_etat(a, etat);
    a = set_tooltip(a, Lang('Edition de l''image avec le visualisateur "ViewImage"', 'Edit image with ViewImage'));
    a = set_msg_click(a, msg);
    a = set(a, 'BusyAction', 'cancel');
    a = set_CData(a, IconeEncocheGauche(icone));
    Tag = 'ancre_cpn_ViewImage';
    a = set_name(a, Tag);
    a = set_handleAnchor(a, get_handleAnchor(this, Tag));
    this.ihm.ViewImage.cpn = editobj(a);
end
this.ihm.ViewImage.msg = msg;

%% Create Video

icone = get_icone(this, 'Video');
msg = [tag1, 'Video'];
etat = this.Images(this.indImage).Video - 1;
a = togglebutton;
a = set_etat(a, etat);
a = set_tooltip(a, Lang('N�gatif couleur', 'Negate (colors)'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Video';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Video.cpn = editobj(a);
this.ihm.Video.msg = msg;

%% Create Contraste

icone = get_icone(this, 'Contraste');
msg = [tag1, 'Contraste'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set_tooltip(a, Lang('Rehaussement de contraste', 'Contrast enhancement on current extent'));
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheDroite(icone));
% a = set_CData(a, IconeEncocheGauche(IconeEncocheDroite(icone))); % Rajout� le 03/20/2013, recomment� le 16/10/2013
Tag = 'ancre_cpn_Contraste';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Contraste.cpn = editobj(a);
this.ihm.Contraste.msg = msg;
this = creer_cpn_Contraste(this, msg);

%% Create Grid

icone = get_icone(this, 'Grid');
msg = [tag1, 'Grid'];
a = togglebutton;
%     a = set_texte(a, 'Grid');
a = set_etat(a, this.visuGrid);
a = set_tooltip(a, Lang('Affichage d''une grille sur l''image et les axes horizontaux et verticaux', 'Show or hide grid on image and horz and vert profiles'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Grid';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Grid.cpn = editobj(a);
this.ihm.Grid.msg = msg;

%% Create ModePointAndClick

icone = get_icone(this, 'ModePointAndClick');
msg = [tag1, 'TypeInteractionSouris'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_tooltip(a, Lang('Type d''int�raction avec la souris', 'Mouse interaction type'));
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_TypeInteractionSouris';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.TypeInteractionSouris.cpn = editobj(a);
this.ihm.TypeInteractionSouris.msg = msg;
this = creer_cpn_TypeInteractionSouris(this, msg);

%% Create Colorbar

icone = get_icone(this, 'Colorbar');
msg = [tag1, 'Colorbar'];
a = togglebutton;
a = set_etat(a, this.visuColorbar);
a = set_tooltip(a, Lang('Affichage de la table de couleurs', 'Show or hide the colorbar'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Colorbar';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Colorbar.cpn = editobj(a);
this.ihm.Colorbar.msg = msg;

%% Create Colormap

icone = get_icone(this, 'Colormap');
msg = [tag1, 'ColormapIndex'];
a = pushbutton;
a = set_tooltip(a, Lang('S�lection de la palette de couleur', 'Color palette selection'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheDroite(icone));
Tag = 'ancre_cpn_ColormapIndex';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ColormapIndex.cpn = editobj(a);
this.ihm.ColormapIndex.msg = msg;
this = creer_cpn_Colormap(this, msg);

%% Create Info

hInfo = uimenu(this.hfig, 'Text', 'Info');
hHelp = uimenu(this.hfig, 'Text', Lang('Aide', 'Help'));
icone = get_icone(this, 'Info');
msg = [tag1, 'Info'];
a = pushbutton;
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_tooltip(a, Lang('Informations sur l''image', 'Image Information'));
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Info';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Info.cpn = editobj(a);
this.ihm.Info.msg = msg;
this = creer_cpn_Info(this, msg, hInfo);

%% Create Quick-Look

icone = get_icone(this, 'ZoomQL');
msg = [tag1, 'ZoomQL'];
a = pushbutton;
a = set_tooltip(a, Lang('Vue globale (q)', 'Full extent (q)'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_ZoomQL';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.QuickLook.cpn = editobj(a);
this.ihm.QuickLook.msg = msg;

%% Create AxisEqual

% icone = get_icone(this, 'AxisEqual');
% msg = [tag1, 'AxisEqual'];
% a = pushbutton;
% a = set_tooltip(a, Lang('Rep�re orthonorm�', 'Axis Ortho'));
% a = set_msg_click(a, msg);
% a = set(a, 'BusyAction', 'cancel');
% a = set_CData(a, IconeEncocheGauche(icone));
% Tag = 'ancre_cpn_AxisEqual';
% a = set_name(a, Tag);
% a = set_handleAnchor(a, get_handleAnchor(this, Tag));
% this.ihm.AxisEqual.cpn = editobj(a);
% this.ihm.AxisEqual.msg = msg;

%% Create ZoomPrecedent

icone = get_icone(this, 'ZoomPrecedent');
msg = [tag1, 'ZoomPrecedent'];
a = pushbutton;
a = set_tooltip(a, Lang('Zoom pr�c�dent', 'Previous zoom'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_ZoomPrecedent';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ZoomPrecedent.cpn = editobj(a);
this.ihm.ZoomPrecedent.msg = msg;

%% Create ZoomSuivant

icone = get_icone(this, 'ZoomSuivant');
msg = [tag1, 'ZoomSuivant'];
a = pushbutton;
a = set_tooltip(a, Lang('Zoom suivant', 'Next zoom'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_ZoomSuivant';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ZoomSuivant.cpn = editobj(a);
this.ihm.ZoomSuivant.msg = msg;

%% Create ZoomInX

icone = get_icone(this, 'ZoomInX');
msg = [tag1, 'ZoomInX'];
a = pushbutton;
a = set_tooltip(a, Lang('Zoom avant en x', 'Zoom In X'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_ZoomInX';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ZoomInX.cpn = editobj(a);
this.ihm.ZoomInX.msg = msg;

%% Create ZoomOutX

icone = get_icone(this, 'ZoomOutX');
msg = [tag1, 'ZoomOutX'];
a = pushbutton;
a = set_tooltip(a, Lang('Zoom arri�re en x', 'Zoom Out X'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_ZoomOutX';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ZoomOutX.cpn = editobj(a);
this.ihm.ZoomOutX.msg = msg;

%% Create ZoomInY

icone = get_icone(this, 'ZoomInY');
msg = [tag1, 'ZoomInY'];
a = pushbutton;
a = set_tooltip(a, Lang('Zoom avant en y', 'Zoom In Y'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_ZoomInY';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ZoomInY.cpn = editobj(a);
this.ihm.ZoomInY.msg = msg;

%% Create ZoomOutY

icone = get_icone(this, 'ZoomOutY');
msg = [tag1, 'ZoomOutY'];
a = pushbutton;
a = set_tooltip(a, Lang('Zoom arri�re en x', 'Zoom Out Y'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_ZoomOutY';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.ZoomOutY.cpn = editobj(a);
this.ihm.ZoomOutY.msg = msg;

%% Create MoveUp

icone = get_icone(this, 'MoveUp');
msg = [tag1, 'MoveUp'];
a = pushbutton;
a = set_tooltip(a, Lang('D�calage vers le haut', 'Shift up'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_MoveUp';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.MoveUp.cpn = editobj(a);
this.ihm.MoveUp.msg = msg;

%% Create MoveLeft

icone = get_icone(this, 'MoveLeft');
msg = [tag1, 'MoveLeft'];
a = pushbutton;
a = set_tooltip(a, Lang('D�calage vers la gauche', 'Shift left'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_MoveLeft';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.MoveLeft.cpn = editobj(a);
this.ihm.MoveLeft.msg = msg;

%% Create MoveRight

icone = get_icone(this, 'MoveRight');
msg = [tag1, 'MoveRight'];
a = pushbutton;
a = set_tooltip(a, Lang('D�calage vers la droite', 'Shift right'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_MoveRight';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.MoveRight.cpn = editobj(a);
this.ihm.MoveRight.msg = msg;

%% Create MoveDown

icone = get_icone(this, 'MoveDown');
msg = [tag1, 'MoveDown'];
a = pushbutton;
a = set_tooltip(a, Lang('D�calage vers le bas', 'Shift down'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_MoveDown';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.MoveDown.cpn = editobj(a);
this.ihm.MoveDown.msg = msg;

%% Create Zoom1

icone = get_icone(this, 'Zoom1');
msg = [tag1, 'Zoom1'];
a = pushbutton;
a = set_tooltip(a, Lang('1 pixel pour 1 pixel', '1 pixel for 1 pixel'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_Zoom1';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Zoom1.cpn = editobj(a);
this.ihm.Zoom1.msg = msg;

%% Create PrintPixel

icone = get_icone(this, 'point'); % TODO  : trouver une ic�ne ad�quate
msg = [tag1, 'PrintPixel'];
a = pushbutton;
a = set_tooltip(a, Lang('Valeurs du pixel et de ses voisins', 'Edit pixel values'));
a = set_msg_click(a, msg);
a = set(a, 'BusyAction', 'cancel');
a = set_CData(a, IconeEncocheGauche(icone));
Tag = 'ancre_cpn_PrintPixel';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.PrintPixel.cpn = editobj(a);
this.ihm.PrintPixel.msg = msg;

%% Create Messages

msg = [tag1, 'Messages'];
a = popup;
a = set_texte(a, {''});
a = set_UserData(a, 'ProcessingMessages');
a = set_tooltip(a, Lang('Messages lors des traitements', 'Processing messages'));
a = set_enable(a, 'on'); % set_cenable appelle la m�thode de clc_image ???
Tag = 'ancre_cpn_Messages';
a = set_name(a, Tag);
a = set_handleAnchor(a, get_handleAnchor(this, Tag));
this.ihm.Messages.cpn = editobj(a);
this.ihm.Messages.msg = msg;

%% Define mouse callback messages

this.msgMoveCurseur     = [tag1, 'msgMoveCurseur'];
this.msgClick           = [tag1, 'msgClick'];
this.msgClickCentre     = [tag1, 'msgClickCentre'];
this.msgClickCoupeDown  = [tag1, 'msgClickCoupeDown'];
this.msgClickHeightDown = [tag1, 'msgClickHeightDown'];
this.msgClickSetNaN     = [tag1, 'msgClickSetNaN'];
this.msgClickSetVal     = [tag1, 'msgClickSetVal'];
this.msgMoveProfils     = [tag1, 'msgMoveProfils'];
this.msgClickOnColorbar = [tag1, 'msgClickOnColorbar'];
this.msgCleanData       = [tag1, 'msgCleanData'];

%% Define keybbord callback messages

this.msgKeyPress = [tag1, 'msgKeyPress'];
this.msgMolette  = [tag1, 'msgMolette'];

%% Copy buttons properties in menus

this.ihm.HelpSession.msg = [tag1 'HelpSession'];
Callback = built_cb(this.composant, this.ihm.HelpSession.msg);
hMenu = uimenu(hImportExport, 'Text', Lang('Aide', 'Help'), 'Callback', Callback, ...
    'Separator', 'on', 'ForegroundColor', 'm'); %#ok<NASGU>
% setappdata(hMenu, 'IconData', '???.png');

this.ihm.SaveSession.msg = [tag1 'SaveSession'];
Callback = built_cb(this.composant, this.ihm.SaveSession.msg);
hMenu = uimenu(hImportExport, 'Text', Lang('Sauver la session', 'Save session'), 'Callback', Callback);
setappdata(hMenu, 'IconData', 'SaveSession.png');

this.ihm.RestoreSession.msg = [tag1 'RestoreSession'];
Callback = built_cb(this.composant, this.ihm.RestoreSession.msg);
hMenu = uimenu(hImportExport, 'Text', Lang('Restaurer la session', 'Restore session'), 'Callback', Callback);
setappdata(hMenu, 'IconData', 'RestoreSession.png');

hMenu = uimenu(hImportExport, 'Text', 'Exit', 'Callback', 'close(gcf)', ...
    'Separator', 'on');
setappdata(hMenu, 'IconData', 'File_Exit.png');

%% SonarScope Web site

% str1 = 'http://http://flotte.ifremer.fr/flotte/Presentation-de-la-flotte/Logiciels-embarques/SonarScope/Download';
% str2 = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/Logiciels-embarques/SonarScope/Download';
str1 = 'https://www.flotteoceanographique.fr/Nos-moyens/Logiciels-de-la-flotte/Analyse-et-traitement-de-l-information/SonarScope';
str2 = 'https://www.flotteoceanographique.fr/en/Facilities/Shipboard-software/Analyse-et-traitement-de-l-information/SonarScope';
Callback = sprintf('my_web(''%s'')', Lang(str1,str2));
uimenu(hHelp, 'Text', Lang('T�l�chargement d''une nouvelle version', 'Download a new release'), 'Callback', Callback);

%% SonarScope help

nomFic = getNomFicDatabase('Accueil.html', 'NoMessage');
if isempty(nomFic)
    str1 = 'La documentation ne semble pas �tre install�e ou c''est une ancienne version. Je vous conseille de t�l�charger la derni�re version du Setup et de la re-installer.';
    str2 = 'The documentation does not seem to be installed or it is an old version. Please download the last version and re-instal it.';
    my_warndlg(Lang(str1,str2), 1);
else
    Callback = sprintf('my_web(''%s'')', nomFic);
    uimenu(hHelp, 'Text', Lang('Aide de SonarScope', 'SonarScope Help'), 'Callback', Callback);
end

%% About SonarScope

Callback = @about_Sonarscope;
uimenu(hHelp, 'Text', Lang('A propos de', 'About SonarScope'), 'Callback', Callback);
