% Creation de la frame globale du composant
%
% Syntax
%   this = creer_frame_globale(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

N = 4;

tag1 = [get(this, 'componentName'), ' / clc_image / '];
axe  = [];
uic  = [];

%% Graphe principal

axe{end+1}.Lig = 1:N;
axe{end}.Col   = 1;

%% Graphe du profil selon x

axe{end+1}.Lig = 1:N;
axe{end}.Col   = 1;

%% Graphe du profil selon y

axe{end+1}.Lig  = 1:N;
axe{end}.Col    = 1;

%% Ancre d'insertion de la toolbar

tag = [tag1, 'ancre_Toolbar'];
uic{end+1}.Lig = N+1;
uic{end}.Col   = 1;
uic{end}.Style = 'frame';
uic{end}.Tag   = tag;

%% Ancre d'insertion de la colorbar

tag = [tag1, 'ancre_Colorbar'];
uic{end+1}.Lig = 1:N;
uic{end}.Col   = 1;
uic{end}.Style = 'frame';
uic{end}.Tag   = tag;

%% Creation de frame

this.globalFrame = cl_frame(...
    'handleFrame', get(this, 'componentAnchor'), ...
    'InsetX',      get(this, 'componentInsetX'), ...
    'InsetY',      get(this, 'componentInsetY'), ...
    'Visible',     'on');
this.globalFrame = set(this.globalFrame, 'ListeUicontrols', uic);
this.globalFrame = set(this.globalFrame, 'ListeAxes', axe);

%% Determination des acces rapides

handles = get(this.globalFrame, 'ListeAxes');
this.hAxePrincipal = handles(1);
this.hAxeProfilX   = handles(2);
this.hAxeProfilY   = handles(3);

handles = get(this.globalFrame, 'ListeUicontrols');
this.hToolBar  = handles(1);
this.hColorBar = handles(2);

%% Construction et affichage de l'image

this = show_image(this);

%% Mise en place du zoom (par utilisation de la souris)

this = creer_zoom(this);

%% Construction des composants toolbar et colorbar

this = creer_frame_toolbar(this);
this = creer_cpn_colorbar(this);

%% La frame de fond doit toujours �tre invisible, pour voir les axes !

set(get(this, 'componentAnchor'), 'Visible', 'off');

set(this.hAxeProfilX, 'YAxisLocation', 'right')
