% Creation de la frame toolbar
%
% Syntax
%   this = creer_frame_toolbar(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_frame_toolbar(this)

ihm = [] ;

%% Frame contenant les composants

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Import';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Export';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Info';

ihm{end+1}.Lig  = 4;
ihm{end}.Col    = 1;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Delete';

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 2:5;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_IX';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 2:5;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_IY';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 2:5;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_NomImage';

if isdeployed
    ihm{end+1}.Lig  = 4;
    ihm{end}.Col    = 2:5;
    ihm{end}.Style  = 'frame';
    ihm{end}.Tag    = 'ancre_cpn_IdentImage';
else
    ihm{end+1}.Lig  = 4;
    ihm{end}.Col    = 2:4;
    ihm{end}.Style  = 'frame';
    ihm{end}.Tag    = 'ancre_cpn_IdentImage';

    ihm{end+1}.Lig  = 4;
    ihm{end}.Col    = 5;
    ihm{end}.Style  = 'frame';
    ihm{end}.Tag    = 'ancre_cpn_ViewImage';
end

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 6:9;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ValX';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 6:9;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ValY';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 6:9;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ValImage';

% Icones

k = 5;

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Contraste';

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ColormapIndex';

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Video';

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Colorbar';






k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_XDir';

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_YDir';

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_CoupeHorz';

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_CoupeVert';

k = k+1;
ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_QuestionLevel';



k = k+1;
ihm{end+1}.Lig  = 1;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_VisuImage';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_NbVoisins';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Visu3D';

ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ParallelTbx';



k = k+1;
ihm{end+1}.Lig  = 1;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_VisuPlot';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_VisuPlotVoisinsY';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_VisuPlotVoisinsX';

ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Stats';

ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ComputeSonar';

ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Compute';

ihm{end+1}.Lig  = 4;
ihm{end}.Col    = k;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_PrintPixel';





% Boutons relatis au zoom
ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 10;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_TypeInteractionSouris';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 14;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Zoom1';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 14;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoneEtude';

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 14;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoomQL';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 10;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoomPrecedent';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 10;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoomSuivant';

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 11;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoomInX';

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 13;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoomOutX';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 11;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoomInY';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 13;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_ZoomOutY';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 12;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_AxisEqual';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 12;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_Grid';

ihm{end+1}.Lig  = 1;
ihm{end}.Col    = 12;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_MoveUp';

ihm{end+1}.Lig  = 3;
ihm{end}.Col    = 12;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_MoveDown';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 11;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_MoveLeft';

ihm{end+1}.Lig  = 2;
ihm{end}.Col    = 13;
ihm{end}.Style  = 'frame';
ihm{end}.Tag    = 'ancre_cpn_MoveRight';

%% Messages

ihm{end+1}.Lig = 5;
ihm{end}.Col   = 1:16;
ihm{end}.Style = 'frame';
ihm{end}.Tag   = 'ancre_cpn_Messages';

%% Cr�ation de la frame

tag1 = [get(this, 'componentName'), ' / clc_image / '];
tag  = [tag1, 'ancre_Toolbar'];

this.ihm.toolbar = cl_frame([]);
this.ihm.toolbar = set_nom_frame(     this.ihm.toolbar, tag);
this.ihm.toolbar = set_inset_x(       this.ihm.toolbar, 2);
this.ihm.toolbar = set_inset_y(       this.ihm.toolbar, 1);
this.ihm.toolbar = set_tab_uicontrols(this.ihm.toolbar, ihm);

%% Cr�ation

this = creer_cpn_toolbar(this);
