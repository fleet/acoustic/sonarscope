% Creation du menu Sonar speculaire
% 
% Syntax
%   cmenu = creer_menu_Sonar_Speculaire(this, baseMsg, cmenu)
%
% Input Arguments
%   this    : Instance de clc_image
%   baseMsg : Base du message
%   cmenu   : Instance de clc_menu
%
% Output Arguments
%   cmenu   : Instance de clc_menu
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function cmenu = creer_menu_Sonar_Speculaire(~, baseMsg, cmenu)

msg{1} = baseMsg;
msg{2} = 'SP_Speculaire';

parent0 = 'SP_Speculaire_MainMenu';
cmenu = ajouter(cmenu, 'tag', parent0, ...
    'label', Lang('Speculaire', 'Specular'));

msg{3} = 'SP_Speculaire_FiltreGauss_CI0';
cmenu = ajouter(cmenu, 'parent', parent0, 'tag', msg{2}, 'message', msg, ...
    'label', Lang('Filtrage gaussien', 'Gauss Filter'));
