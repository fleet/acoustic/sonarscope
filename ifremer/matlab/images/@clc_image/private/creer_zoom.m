% Creation et initialisation du zoom souris
%
% Syntax
%   a = creer_zoom(a)
%
% Input Arguments
%   a : instance de cli_exemple
%
% Output Arguments
%   a : instance de cli_exemple initialisee
%
% Examples
%
% See also cli_exemple Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_zoom(this)

%% Construction du tableau des handles des axes devant �tre asservis

h(1)   = get_handle_axe(this, 'principal');
h(2)   = get_handle_axe(this, 'profilX');
h(3)   = get_handle_axe(this, 'profilY');

fig = get(h(1), 'Parent');     % A defaut de mieux. Il serait judicieux de stocker
%             le numero de la figure soit dans le cl_component soit dans la classe

%% Construction du tableau d'asservissement du zoom

asserv = [3 1 2 ; 1 3 0; 2 0 3];

%% Construction de l'instance de zoom

this.ihm.zoom.cpn = cl_virtual_zoom('hFig'  , fig, ...
    'hAxes' , h, ...
    'asserv', asserv, ...
    'action', 'In', ...
    'cbObj' , get(this, 'componentUserName'), ...
    'cbName', get(this, 'componentUserCb'));

%% Suppression des icones de la figure

set(fig, 'toolbar', 'none');
