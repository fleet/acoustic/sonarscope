% Fonctionnalite de croix sur une position designee
%
% Syntax
%   a = croix(a, axe, X, Y)
% 
% Input Arguments
%   a       : instance de clc_image
%   axe     : axe de destination {'profilX', 'profilY', 'principal'}
%   X       : position de la croix en abscisse
%   Y       : position de la croix en ordonn�es
% 
% OUPUT PARAMETERS :
%   a : instance de clc_image
% 
% Remarks :
%   si une croix est deja tracee, suppression avant de tracer la nouvelle
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = croix(this, axe, X, Y)

%% Traitement du choix de l'axe et du dessin des infos

% hFigure     = gcf(this, get(this, 'componentAnchor'));
% currentAxe  = get(hFigure, 'CurrentAxes');
XLim = get(this.Images(this.indImage), 'XLim');
YLim = get(this.Images(this.indImage), 'YLim');

switch axe
    case 'profilX'

        % Suppression de l'ancienne croix si elle existe
        if ishandle(this.hCroixProfilX)
            delete(this.hCroixProfilX);
            this.hCroixProfilX = preallocateGraphics(0,0);
        end

        % Trac� de la nouvelle croix
        is_hold = ishold;
        hold(this.hAxeProfilX, 'on')
        this.hCroixProfilX(1) = plot(this.hAxeProfilX, X, Y, 'k+');
        Limites = get(this.hAxeProfilX, 'YLim');

        minval = min(this.profilX.val(:));
        maxval = max(this.profilX.val(:));
        if ~isempty(minval) && ~isnan(minval) && (minval ~= maxval)
            Limites = [minval maxval];
        end

        this.hCroixProfilX(2) = plot(this.hAxeProfilX, [X X], Limites, 'k');
        if get_etat(this.ihm.CoupeHorz.cpn)
            set(this.hCroixProfilX, 'Visible', 'on');
        else
            set(this.hCroixProfilX, 'Visible', 'off');
        end

        if ~is_hold
            hold(this.hAxeProfilX, 'off')
        end

        if this.Images(this.indImage).XDir == 1
            set(this.hAxeProfilX, 'XDir', 'normal');
        else
            set(this.hAxeProfilX, 'XDir', 'reverse');
        end

    case 'profilY'

        % Suppression de l'ancienne croix si elle existe
        if ishandle(this.hCroixProfilY)
            delete(this.hCroixProfilY);
        end
        this.hCroixProfilY = preallocateGraphics(0,0);
            
        % Trac� de la nouvelle croix
        is_hold = ishold;
        hold(this.hAxeProfilY, 'on')
        if strcmp(this.profilY.Type, 'Vertical profile') || strcmp(this.profilY.Type, 'Image profile')
            this.hCroixProfilY(2) = plot(this.hAxeProfilY, Y, X, 'k+');
        end
        Limites = get(this.hAxeProfilY, 'XLim');

        minval = min(this.profilY.val(:));
        maxval = max(this.profilY.val(:));
        if ~isempty(minval) && ~isnan(minval) && (minval ~= maxval)
            Limites = [minval maxval];
        end

        this.hCroixProfilY(1) = plot(this.hAxeProfilY, Limites, [X X], 'k');
        if get_etat(this.ihm.CoupeVert.cpn)
            set(this.hCroixProfilY,    'Visible', 'on');
        else
            set(this.hCroixProfilY,    'Visible', 'off');
        end

        if ~is_hold
            hold(this.hAxeProfilY, 'off')
        end

        if this.Images(this.indImage).YDir == 1
            set(this.hAxeProfilY, 'YDir', 'normal');
        else
            set(this.hAxeProfilY, 'YDir', 'reverse');
        end

    case 'principal'
        
        %% Trac� de la nouvelle croix
        
        %         pppp = this.hCroix
        %         whos pppp
        %         isHandle = ishandle(pppp)
        
        % TODO : Ca, c'est pas terrible du tout mais alors pas du tout !
        % Les handles ne sont pas sauv�es dans le UserData. Il faudrait les sauver localement (UserData de l'axe ? et les d�truire
        % if this.TypeInteractionSouris == 'MoveProfils' 8
        hTemp = findobj(this.hAxePrincipal, 'type', 'line', '-not', 'Tag', 'PlotHeightOnImage', '-not', 'Tag', 'ProfilOnTop', ...
            '-not', 'Tag', 'BottomDetector');
        delete(hTemp)
        
        if this.identVisu(this.indImage) ~= 2
            hTemp = findobj(this.hAxePrincipal, 'type', 'patch' , '-not', 'Tag', 'PlotRegionOfInterest');
            delete(hTemp)
        end
        % end
        
        if ~isempty(this.hCroix) && all(ishandle(this.hCroix))
            delete(this.hCroix)
        end
        
        is_hold = ishold;
        hold(this.hAxePrincipal, 'on')
        
        this.hCroix = preallocateGraphics(0,0);
        this.hCroix(1) = plot(this.hAxePrincipal, [X X], YLim, 'k');
        this.hCroix(2) = plot(this.hAxePrincipal, XLim, [Y Y], 'k');
        
        if this.NbVoisins >= 1
            [ix, iy] = xy2ij(this.Images(this.indImage), X, Y);
            [X, Y] = ij2xy(this.Images(this.indImage), ix, iy);
            [ix, iy] = xy2ij(this.Images(this.indImage), X, Y);
            step = this.NbVoisins;
            x1 = [ix-step ix-step ix+step ix+step];
            y1 = [iy-step iy+step iy+step iy-step];
            [X1, Y1] = ij2xy(this.Images(this.indImage), x1, y1);
            
            x = get(this.Images(this.indImage), 'x');
            XStep2 = get(this.Images(this.indImage), 'XStep');
            if (x(end) - x(1)) > 0
                X1(1:2) = X1(1:2) - XStep2 / 2;
                X1(3:4) = X1(3:4) + XStep2 / 2;
            else
                X1(1:2) = X1(1:2) + XStep2 / 2;
                X1(3:4) = X1(3:4) - XStep2 / 2;
            end
            
            y = get(this.Images(this.indImage), 'y');
            YStep2 = get(this.Images(this.indImage), 'YStep');
            if (y(end) - y(1)) > 0
                Y1(2:3)   = Y1(2:3) + YStep2 / 2;
                Y1([1 4]) = Y1([1 4]) - YStep2 / 2;
            else
                Y1(2:3)   = Y1(2:3) - YStep2 / 2;
                Y1([1 4]) = Y1([1 4]) + YStep2 / 2;
            end
            
            % Obligatoire car patch est bogu�, on ne peut pas lui passer le handle de l'axe
            figure(this.hfig)
            set(this.hfig, 'CurrentAxes', this.hAxePrincipal);
            
            % TODO / L'instruction suivante fait perdre le focus pourquoi ?
            %             this.hCroix(3) = patch(X1, Y1, [0.8 0.8 0.8], 'FaceAlpha', 0.5);
            
            % Le plot ne perd pas le focus !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            this.hCroix(3) = plot([X1 X1(1)], [Y1 Y1(1)]);
            
            % Nombreux tests pour essayer de percer le myst�re
            %             set(this.hCroix(3), 'HitTest', 'Off')
            %             set(this.hCroix(3), 'SelectionHighlight', 'Off')
            %             pppp=get(this.hCroix(3))
            
            %             figure(this.hfig)
            %             set(this.hfig, 'CurrentAxes', this.hAxePrincipal);
            
        else
            if (length(this.hCroix) == 3) && ishandle(this.hCroix(3))
                delete(this.hCroix(3))
            end
        end
        
        if ~is_hold
            hold(this.hAxePrincipal, 'off')
        end
        
        % Traitement de la visibilit� de la croix
        traiter_visibilite_croix(this);
        
    otherwise
        disp('clc_image/croix : Invalid arguments number');
end
