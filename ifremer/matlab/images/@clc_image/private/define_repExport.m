function repExport = define_repExport(this)

global SonarScopeData %#ok<GVMIS>

if isempty(this.repExport)
    InitialFileName = this.Images(this.indImage).InitialFileName;
    if isempty(InitialFileName)
        repExport = my_tempdir;
    else
        pathname = fileparts(InitialFileName);
        
        if  strcmp(pathname, fullfile(SonarScopeData, 'Public'))  || ...
            strcmp(pathname, fullfile(SonarScopeData, 'Sonar'))   || ...
            strcmp(pathname, fullfile(SonarScopeData, 'Levitus'))
            repExport = my_tempdir;
        else
            repExport = pathname;
        end
    end
else
    repExport = this.repExport;
end