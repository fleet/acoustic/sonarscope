function repImport = define_repImport(this)

if isempty(this.repImport)
    InitialFileName = this.Images(this.indImage).InitialFileName;
    if isempty(InitialFileName)
        repImport = my_tempdir;
    else
        pathname = fileparts(InitialFileName);
        repImport = pathname;
    end
else
    repImport = this.repImport;
end
