% D�sactivation des frames de la fen�tre
%
% this = desactiver_frames(this)
%
% Input Arguments
%    this : instance de clc_image
%
% Output Arguments
%    this : instance de clc_image
%
% See also clc_image
% Authors : DCF
% ----------------------------------------------------------------------------

function this = desactiver_frames(this)

this.globalFrame = set(this.globalFrame,  'enable', 'off');
this.ihm.toolbar = set(this.ihm.toolbar,  'enable', 'off');
desactiver(this.ihm.toolbar);
