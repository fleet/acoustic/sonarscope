% Ajustement de la dynamique des profils
%
% Syntax
%   dynamique_profils(a)
% 
% Input Arguments
%   a   : instance de clc_image
% 
% Output Arguments
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function dynamique_profils(this)

currentAxe = get(this.hfig, 'CurrentAxes');

%% Dynamique de l'axe X

sub = ((this.profilX.x >= this.visuLim(this.indImage, 1)) & ...
       (this.profilX.x <= this.visuLim(this.indImage, 2)));
val = this.profilX.val(:,sub);
subNonInf = ~isinf(val);
minval = min(val(subNonInf));
maxval = max(val(subNonInf));
if ~isempty(minval) && ~isnan(minval) && (minval ~= maxval)
    YLim = [minval maxval];
    YLim = majoration(YLim);
    set(this.hAxeProfilX, 'YLim', YLim);
end

if get_etat(this.ihm.CoupeHorz.cpn)
    if ((this.cross(this.indImage, 2) >= this.visuLim(this.indImage, 3)) && ...
        (this.cross(this.indImage, 2) <= this.visuLim(this.indImage, 4)))
        str = 'on';
    else
        str = 'off';
    end
else
    str = 'off';
end
if ishandle(this.hCourbeProfilX)
    set(this.hCourbeProfilX, 'Visible', str)
end

%% Dynamique de l'axe Y

sub = ((this.profilY.x >= this.visuLim(this.indImage, 3)) & ...
       (this.profilY.x <= this.visuLim(this.indImage, 4)));
%     val = this.profilY.val(sub, :);

% Bidouille a supprimer quand tous les signaux seront bien g�r�s pour tous les sondeurs
if size(this.profilY.val, 1) == 1
    this.profilY.val = this.profilY.val';
end

val = this.profilY.val(sub,:);
subNonInf = ~isinf(val);
minval = min(val(subNonInf));
maxval = max(val(subNonInf));
if ~isempty(minval) && ~isnan(minval) && (minval ~= maxval)
    XLim = [minval maxval];
    XLim = majoration(XLim);
    set(this.hAxeProfilY, 'XLim', XLim);
end

if get_etat(this.ihm.CoupeVert.cpn)
    if strcmp(this.profilY.Type, 'Vertical profile') || strcmp(this.profilY.Type, 'Image profile')
        if ((this.cross(this.indImage, 1) >= this.visuLim(this.indImage, 1)) && ...
                (this.cross(this.indImage, 1) <= this.visuLim(this.indImage, 2)))
            str = 'on';
        else
            str = 'off';
        end
    else
        str = 'on';
    end
else
    str = 'off';
end

if ishandle(this.hCourbeProfilY)
    set(this.hCourbeProfilY, 'Visible', str)
end

%% Retrouve l'axe courant

if ishandle(currentAxe)
    set(this.hfig, 'CurrentAxes', currentAxe);
end


function XLim = majoration(XLim)

Range = diff(XLim) * 0.6;
Milieu = mean(XLim);
XLim = [Milieu-Range Milieu+Range];
