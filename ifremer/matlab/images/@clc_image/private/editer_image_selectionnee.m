% Mise a jour du composant editer_image_selectionnee
%
% Syntax
%   this = editer_image_selectionnee(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = editer_image_selectionnee(this, varargin)

[varargin, MajColorbar] = getPropertyValue(varargin, 'MajColorbar', 1); %#ok<ASGLU>

if isempty(this.indImage)
    this.indImage = 1; % Bug apparu le 05/07/2013,sans doute en lien avec summary : GLU
end

%% Mise � jour du bouton NomImage

liste = listeLayers(this);
n = length(liste);
if this.indImage > n % Pour correction d'un bug de restauration session de Jean-Guy
    this.indImage = n;
end
set_texte(this.ihm.NomImage.cpn, liste{this.indImage});

%% Mise � jour de la toolbar en fonction des flags internes

this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn,   this.visuCoupeHorz);
this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn,   this.visuCoupeVert);
this.ihm.Colorbar.cpn  = set_etat(this.ihm.Colorbar.cpn,    this.visuColorbar);
this.ihm.Grid.cpn      = set_etat(this.ihm.Grid.cpn,        this.visuGrid);

%% Bouton recevant la valeur de l'abscisse

this.ihm.ValX.cpn = set_titre(this.ihm.ValX.cpn, this.Images(this.indImage).XLabel);
this.ihm.ValX.cpn = set_unite(this.ihm.ValX.cpn, this.Images(this.indImage).XUnit);

%% Bouton recevant le num�ro de colonne

this.ihm.IX.cpn = set(this.ihm.IX.cpn, 'value', 1, ...
    'maxValue', this.Images(this.indImage).nbColumns);

%% Bouton recevant le num�ro de ligne

this.ihm.IY.cpn = set(this.ihm.IY.cpn, 'value', 1, ...
    'maxValue', this.Images(this.indImage).nbRows);

%% Bouton recevant la valeur de l'ordonnee

this.ihm.ValY.cpn = set_titre(this.ihm.ValY.cpn, this.Images(this.indImage).YLabel);
this.ihm.ValY.cpn = set_unite(this.ihm.ValY.cpn, this.Images(this.indImage).YUnit);

%% Bouton recevant la valeur de l'image

this.ihm.ValImage.cpn = set_unite(this.ihm.ValImage.cpn, get(this.Images(this.indImage), 'Unit'));

%% Etc, etc, etc, ...

this.ihm.YDir.cpn  = set_etat(this.ihm.YDir.cpn,  this.Images(this.indImage).YDir  - 1);
this.ihm.XDir.cpn  = set_etat(this.ihm.XDir.cpn,  this.Images(this.indImage).XDir  - 1);
this.ihm.Video.cpn = set_etat(this.ihm.Video.cpn, this.Images(this.indImage).Video - 1);

%% Construction et affichage de l'image

[this, flag] = show_image(this, 'MajColorbar', MajColorbar);
if ~flag
    return
end
% this = process_synchroY(this);

ColormapIndex = this.Images(this.indImage).ColormapIndex;
if ColormapIndex == 1
    map = this.Images(this.indImage).ColormapCustom;
else
    map = this.Images(this.indImage).Colormap;
end
this = set_colormap(this, map);
indiquerColorTableUtilisee(this, ColormapIndex)

this = placer_zoom(this);
this = show_profils(this);
this = gerer_menus(this);

% this = process_synchroY(this);
