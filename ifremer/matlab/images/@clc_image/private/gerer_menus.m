% Gestion du menu
% 
% Syntax
%   this = gerer_menus(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialis�e
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = gerer_menus(this)

%% Mise a jour du menu "Traitements"

GeometryType   = this.Images(this.indImage).GeometryType;
ImageType      = this.Images(this.indImage).ImageType;
SpectralStatus = this.Images(this.indImage).SpectralStatus;

%% Tag Langage

maj_menu_Lang(this)

%% Tag GUISelectImage

maj_menu_GUISelectImage(this)

%% Tag LevelQuestions

maj_menu_LevelQuestions(this);

%% Tag CacheStatus

maj_menu_CacheDirectory(this);

%% Tag ParallelTbx

maj_menu_ParallelTbx(this);

%% Tag GraphicsOnTop

this = maj_menu_GraphicsOnTop(this);

%% TagIdentAlgoSnippets2OneValuePerBeam

this = maj_menu_IdentAlgoSnippets2OneValuePerBeam(this);

%% Tag UseLogFile

maj_menu_UseLogFile(this)

%% Tag Use ClickRightForFlag

flagUseClickRightForFlag = getUseClickRightForFlag;
if flagUseClickRightForFlag
    set(this.ihm.Handles.TagPreferencesClickRightForFlagYes, 'Checked', 'On');
    set(this.ihm.Handles.TagPreferencesClickRightForFlagNo,  'Checked', 'Off');
else
    set(this.ihm.Handles.TagPreferencesClickRightForFlagYes, 'Checked', 'Off');
    set(this.ihm.Handles.TagPreferencesClickRightForFlagNo,  'Checked', 'On');
end

%% ContrasteImageZoomeeMinMaxAuto

try
    flagContrasteAuto = getFlagContrasteAuto;
    if isempty(flagContrasteAuto)
        flagContrasteAuto = 0;
    end
    switch flagContrasteAuto
        case 0 % Ni [Min Max] ni 0.5% Auto
            set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'Off');
            set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'Off');
        case 1 % [Min Max] Auto
            set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'On');
            set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'Off');
        case 2 % 0.5% Auto
            set(this.ihm.Handles.ContrasteImageZoomeeMinMaxAuto, 'Checked', 'Off');
            set(this.ihm.Handles.ContrasteImageZoomee0p5PCAuto,  'Checked', 'On');
    end
catch
    my_breakpoint % Bug rencontr� parfois quand il y a une visu de navigation qui est ouverte
end

%% Menu export

switch GeometryType
    case {2,7}  % Metrique + PingBeam
%         set(this.ihm.Handles.h_TagExportArcGis, 'Enable', 'on');
        set(this.ihm.Handles.EXPORT_GMT1_CI1, 'Enable', 'off');
        set(this.ihm.Handles.EXPORT_GMT2_CI1, 'Enable', 'off');
    case 3 % G�ographique        
%         set(this.ihm.Handles.h_TagExportArcGis, 'Enable', 'off');        
        set(this.ihm.Handles.EXPORT_GMT1_CI1, 'Enable', 'on');
        set(this.ihm.Handles.EXPORT_GMT2_CI1, 'Enable', 'on');
    otherwise
%         set(this.ihm.Handles.h_TagExportArcGis, 'Enable', 'off');
        set(this.ihm.Handles.EXPORT_GMT1_CI1, 'Enable', 'off');
        set(this.ihm.Handles.EXPORT_GMT2_CI1, 'Enable', 'off');
end

%% Statistiques

if get_LevelQuestion >= 3
     set(this.ihm.Handles.STATS_ConditionCalculBiasMultiple_CI1, 'Enable', 'on');
else
     set(this.ihm.Handles.STATS_ConditionCalculBiasMultiple_CI1, 'Enable', 'off');
end

%% Texture

if get_LevelQuestion >= 3
     set(this.ihm.Handles.GP_Segmentation_ImenMaster_KullbackDistance_CI1,   'Enable', 'on');
     set(this.ihm.Handles.GP_Segmentation_ImenMaster_HaralickParameters_CI1, 'Enable', 'on');
else
     set(this.ihm.Handles.GP_Segmentation_ImenMaster_KullbackDistance_CI1,   'Enable', 'off');
     set(this.ihm.Handles.GP_Segmentation_ImenMaster_HaralickParameters_CI1, 'Enable', 'off');
end

%% Fourier / Pas Fourier

switch SpectralStatus
    case 1  % Normal
        set(this.ihm.Handles.GP_Fourier_FFT_CI1,              'Enable', 'on');
        set(this.ihm.Handles.GP_Fourier_Filter_Disk_CI0,      'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_Filter_Gauss_CI0,     'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_Filter_Threshold_CI0, 'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_IFFT_CI0,             'Enable', 'off');
    case 2  % Fourier
        set(this.ihm.Handles.GP_Fourier_FFT_CI1,              'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_Filter_Disk_CI0,      'Enable', 'on');
        set(this.ihm.Handles.GP_Fourier_Filter_Gauss_CI0,     'Enable', 'on');
        set(this.ihm.Handles.GP_Fourier_Filter_Threshold_CI0, 'Enable', 'on');
        set(this.ihm.Handles.GP_Fourier_IFFT_CI0,             'Enable', 'on');
    case 3  % Ondelettes
        set(this.ihm.Handles.GP_Fourier_FFT_CI1,              'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_Filter_Disk_CI0,      'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_Filter_Gauss_CI0,     'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_Filter_Threshold_CI0, 'Enable', 'off');
        set(this.ihm.Handles.GP_Fourier_IFFT_CI0,             'Enable', 'off');
end

%% Transformation de type d'image (intensite, RGB, indexee, Binaire)

switch ImageType
    case 1  % Image en intensite
        set(this.ihm.Handles.GP_ImageType_RGB2HSV_CI1,           'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2Intensity_CI1,     'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2Indexed_CI1,       'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Intensity2RGB_CI1,     'Enable', 'on');
        set(this.ihm.Handles.GP_ImageType_Intensity2Indexed_CI1, 'Enable', 'on');
        set(this.ihm.Handles.GP_ImageType_Indexed2RGB_CI1,       'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Indexed2Intensity_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2IntensityWithGMTColomapLimits_CI1, 'Enable', 'on');
        
    case 2  % Image en RGB
        set(this.ihm.Handles.GP_ImageType_RGB2HSV_CI1,           'Enable', 'on');
        set(this.ihm.Handles.GP_ImageType_RGB2Intensity_CI1,     'Enable', 'on');
        set(this.ihm.Handles.GP_ImageType_RGB2Indexed_CI1,       'Enable', 'on');
        set(this.ihm.Handles.GP_ImageType_Intensity2RGB_CI1,     'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Intensity2Indexed_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Indexed2RGB_CI1,       'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Indexed2Intensity_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2IntensityWithGMTColomapLimits_CI1, 'Enable', 'off');
        
    case 3  % Image indexee
        set(this.ihm.Handles.GP_ImageType_RGB2HSV_CI1,           'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2Intensity_CI1,     'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2Indexed_CI1,       'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Intensity2RGB_CI1,     'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Intensity2Indexed_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Indexed2RGB_CI1,       'Enable', 'on');
        set(this.ihm.Handles.GP_ImageType_Indexed2Intensity_CI1, 'Enable', 'on');
        set(this.ihm.Handles.GP_ImageType_RGB2IntensityWithGMTColomapLimits_CI1, 'Enable', 'off');
        
    case 4  % Image binaire
        set(this.ihm.Handles.GP_ImageType_RGB2HSV_CI1,           'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2Intensity_CI1,     'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2Indexed_CI1,       'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Intensity2RGB_CI1,     'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Intensity2Indexed_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Indexed2RGB_CI1,       'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_Indexed2Intensity_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_ImageType_RGB2IntensityWithGMTColomapLimits_CI1,  'Enable', 'off');
end

SonarDescription = get(this.Images(this.indImage), 'SonarDescription');
if isempty(SonarDescription)
    set(this.ihm.Handles.TagInfoSonarCharacteristics, 'Enable', 'off');
else
    set(this.ihm.Handles.TagInfoSonarCharacteristics, 'Enable', 'on');
end

Sonar = get(this.Images(this.indImage), 'Sonar');
if isempty(Sonar) %|| isempty(Sonar.Time)
    set(this.ihm.Handles.Info_SonarData, 'Enable', 'off');
else
    set(this.ihm.Handles.Info_SonarData, 'Enable', 'on');
end

%% Menu carto ?

switch GeometryType
    case 2  % Metrique
        set(this.ihm.Handles.GP_Carto_GeoYX2LatLong_CI1, 'Enable', 'on');
        set(this.ihm.Handles.GP_Carto_LatLong2GeoYX_CI1, 'Enable', 'off');      
    case 3  % Geometrique
        set(this.ihm.Handles.GP_Carto_GeoYX2LatLong_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_Carto_LatLong2GeoYX_CI1, 'Enable', 'on');        
    otherwise
        set(this.ihm.Handles.GP_Carto_GeoYX2LatLong_CI1, 'Enable', 'off');
        set(this.ihm.Handles.GP_Carto_LatLong2GeoYX_CI1, 'Enable', 'off');        
end

Sounder = get(this.Images(this.indImage), 'SonarDescription');
if isempty(Sounder)
    set(this.ihm.Handles.SP_Resol_Bathy, 'Enable', 'off');
else
    SonarFamily = get(Sounder, 'Sonar.Family');
    if SonarFamily == 1 % Sonar
        set(this.ihm.Handles.SP_Resol_Bathy, 'Enable', 'off');
    else    % Multibeam
        set(this.ihm.Handles.SP_Resol_Bathy, 'Enable', 'on');
    end
end

%% Menu Sonar TVG

if this.Images(this.indImage).DataType == cl_image.indDataType('Reflectivity')
    SonarTVG_etat    = get(this.Images(this.indImage), 'SonarTVG_etat');
    SonarTVG_origine = get(this.Images(this.indImage), 'SonarTVG_origine');
%     SonarTVG_ConstructTypeCompens   = get(this.Images(this.indImage), 'SonarTVG_ConstructTypeCompens');

    if SonarTVG_etat == 2
        flag1 = 'on';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'off';
    elseif (SonarTVG_etat == 1) && (SonarTVG_origine == 1)
        flag1 = 'off';
        flag2 = 'on';
        flag3 = 'off';
        flag4 = 'off';
    elseif (SonarTVG_etat == 1) && (SonarTVG_origine == 2)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'on';
        flag4 = 'off';
    elseif (SonarTVG_etat == 1) && (SonarTVG_origine == 3)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'on';
    end

    %         SonarTVG_ConstructTable   = get(this.Images(this.indImage), 'SonarTVG_ConstructTable');
    %         if isempty(SonarTVG_ConstructTable)
    %             flag4 = 'off';
    %         else
    %             flag4 = 'on'
    %         end
else
    flag1 = 'off';
    flag2 = 'off';
    flag3 = 'off';
    flag4 = 'off';
end

set(this.ihm.Handles.SP_TVG_NoCompensation_CI1,   'Checked', flag1);
set(this.ihm.Handles.SP_TVG_CompensationRT_CI1,   'Checked', flag2);
set(this.ihm.Handles.SP_TVG_CompensationSSc_CI1,  'Checked', flag3);
set(this.ihm.Handles.SP_TVG_CompensationUser_CI1, 'Checked', flag4);


%% Menu Sonar Diagramme Emission

if this.Images(this.indImage).DataType == cl_image.indDataType('Reflectivity')
    SonarDiagEmi_etat                 = get(this.Images(this.indImage), 'SonarDiagEmi_etat');
    SonarDiagEmi_origine              = get(this.Images(this.indImage), 'SonarDiagEmi_origine');
    SonarDiagEmi_ConstructTypeCompens = get(this.Images(this.indImage), 'SonarDiagEmi_ConstructTypeCompens');
    SonarDiagEmi_IfremerTypeCompens   = get(this.Images(this.indImage), 'SonarDiagEmi_IfremerTypeCompens');

    if SonarDiagEmi_etat == 2
        flag1 = 'on';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarDiagEmi_etat == 1) && (SonarDiagEmi_origine == 1) && (SonarDiagEmi_ConstructTypeCompens == 1)
        flag1 = 'off';
        flag2 = 'on';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarDiagEmi_etat == 1) && (SonarDiagEmi_origine == 1) && (SonarDiagEmi_ConstructTypeCompens == 2)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'on';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarDiagEmi_etat == 1) && (SonarDiagEmi_origine == 2) && (SonarDiagEmi_IfremerTypeCompens == 1)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'on';
        flag5 = 'off';
    elseif (SonarDiagEmi_etat == 1) && (SonarDiagEmi_origine == 2) && (SonarDiagEmi_IfremerTypeCompens == 2)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'on';
    end

else
    flag1 = 'off';
    flag2 = 'off';
    flag3 = 'off';
    flag4 = 'off';
    flag5 = 'off';
end

set(this.ihm.Handles.SonarDiagTx_CompensAucune_CI1,                 'Checked', flag1);
set(this.ihm.Handles.SonarDiagTx_CompensConstructeurAnalytique_CI1, 'Checked', flag2);
set(this.ihm.Handles.SonarDiagTx_CompensConstructeurTable_CI1,      'Checked', flag3);
set(this.ihm.Handles.SonarDiagTx_CompensIfremerAnalytique_CI1,      'Checked', flag4);
set(this.ihm.Handles.SonarDiagTx_CompensIfremerTable_CI1,           'Checked', flag5);

%% Menu Sonar Diagramme Reception

if this.Images(this.indImage).DataType == cl_image.indDataType('Reflectivity')
    SonarDiagRec_etat                 = get(this.Images(this.indImage), 'SonarDiagRec_etat');
    SonarDiagRec_origine              = get(this.Images(this.indImage), 'SonarDiagRec_origine');
    SonarDiagRec_ConstructTypeCompens = get(this.Images(this.indImage), 'SonarDiagRec_ConstructTypeCompens');
    SonarDiagRec_IfremerTypeCompens   = get(this.Images(this.indImage), 'SonarDiagRec_IfremerTypeCompens');

    if SonarDiagRec_etat == 2
        flag1 = 'on';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarDiagRec_etat == 1) && (SonarDiagRec_origine == 1) && (SonarDiagRec_ConstructTypeCompens == 1)
        flag1 = 'off';
        flag2 = 'on';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarDiagRec_etat == 1) && (SonarDiagRec_origine == 1) && (SonarDiagRec_ConstructTypeCompens == 2)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'on';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarDiagRec_etat == 1) && (SonarDiagRec_origine == 2) && (SonarDiagRec_IfremerTypeCompens == 1)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'on';
        flag5 = 'off';
    elseif (SonarDiagRec_etat == 1) && (SonarDiagRec_origine == 2) && (SonarDiagRec_IfremerTypeCompens == 2)
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'on';
    end

else
    flag1 = 'off';
    flag2 = 'off';
    flag3 = 'off';
    flag4 = 'off';
    flag5 = 'off';
end

set(this.ihm.Handles.SP_DiagRx_CompensAucune_CI1,                 'Checked', flag1);
set(this.ihm.Handles.SP_DiagRx_CompensConstructeurAnalytique_CI1, 'Checked', flag2);
set(this.ihm.Handles.SP_DiagRx_CompensConstructeurTable_CI1,      'Checked', flag3);
set(this.ihm.Handles.SP_DiagRx_CompensIfremerAnalytique_CI1,      'Checked', flag4);
set(this.ihm.Handles.SP_DiagRx_CompensIfremerTable_CI1,           'Checked', flag5);

%% Menu Aire insonifiee

if this.Images(this.indImage).DataType == cl_image.indDataType('Reflectivity')
    SonarAireInso_etat    = get(this.Images(this.indImage), 'SonarAireInso_etat');
    SonarAireInso_origine = get(this.Images(this.indImage), 'SonarAireInso_origine');

    if SonarAireInso_etat == 2 % Non compens�e
        flag1 = 'on';
        flag2 = 'off';
        flag3 = 'off';
    elseif (SonarAireInso_etat == 1) && (SonarAireInso_origine == 1) % Compens�e, RT
        flag1 = 'off';
        flag2 = 'on';
        flag3 = 'off';
    elseif (SonarAireInso_etat == 1) && (SonarAireInso_origine == 2) % Compens�e, SSc, Angle Earth
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'on';
    elseif (SonarAireInso_etat == 1) && (SonarAireInso_origine == 3) % Compens�e, SSc, Angle d'incidence
        flag1 = 'off';
        flag2 = 'off';
        flag3 = 'on';
    end

else
    flag1 = 'off';
    flag2 = 'off';
    flag3 = 'off';
end

set(this.ihm.Handles.SP_AireInso_NoCompens_CI1,           'Checked', flag1);
set(this.ihm.Handles.SP_AireInso_CompensConstructeur_CI1, 'Checked', flag2);
set(this.ihm.Handles.SP_AireInso_CompensIfremer_CI1,      'Checked', flag3);

%% Menu Backscatter

if this.Images(this.indImage).DataType == cl_image.indDataType('Reflectivity')
    SonarBS_etat              = get(this.Images(this.indImage), 'SonarBS_etat');
    SonarBS_origine           = get(this.Images(this.indImage), 'SonarBS_origine');
    SonarBS_origineBelleImage = get(this.Images(this.indImage), 'SonarBS_origineBelleImage');
    SonarBS_LambertCorrection = get(this.Images(this.indImage), 'SonarBS_LambertCorrection');
%     SonarBS_origineFullCompens = get(this.Images(this.indImage), 'SonarBS_origineFullCompens');

%     'Tests a revoir entierement, creation de nouvelles proprietes dans cl_image '
    if SonarBS_etat == 2 % Pas compens�e
        flag1 = 'on';
        flag2 = 'off'; flag6 = 'off';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarBS_etat == 1) && (SonarBS_origine == 1) && (SonarBS_origineBelleImage == 1) % Belle Image - Lambert
        flag1 = 'off';
        if SonarBS_LambertCorrection == 1
            flag2 = 'on'; flag6 = 'off';
        else
            flag2 = 'off'; flag6 = 'on';
        end
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarBS_etat == 1) && (SonarBS_origine == 1) && (SonarBS_origineBelleImage == 2) % Belle Image - Lurton
        flag1 = 'off';
        flag2 = 'off'; flag6 = 'off';
        flag3 = 'on';
        flag4 = 'off';
        flag5 = 'off';
    elseif (SonarBS_etat == 1) && (SonarBS_origine == 2) && (SonarBS_origineBelleImage == 1) % Full compensation - Analytique
        flag1 = 'off';
        flag2 = 'off'; flag6 = 'off';
        flag3 = 'off';
        flag4 = 'on';
        flag5 = 'off';
    elseif (SonarBS_etat == 1) && (SonarBS_origine == 2) && (SonarBS_origineBelleImage == 2) % Full compensation - Table
        flag1 = 'off';
        flag2 = 'off'; flag6 = 'off';
        flag3 = 'off';
        flag4 = 'off';
        flag5 = 'on';
    end

else
    flag1 = 'off';
    flag2 = 'off'; flag6 = 'off';
    flag3 = 'off';
    flag4 = 'off';
    flag5 = 'off';
end

set(this.ihm.Handles.SP_BS_Compens_None_CI1,          'Checked', flag1);
set(this.ihm.Handles.SP_BS_Compens_LambertKM_CI1,     'Checked', flag2);
set(this.ihm.Handles.SP_BS_Compens_LambertAngles_CI1, 'Checked', flag6);
set(this.ihm.Handles.SP_BS_Compens_Lurton_CI1,        'Checked', flag3);
set(this.ihm.Handles.SP_BS_FullCompens_Func_CI1,      'Checked', flag4);
set(this.ihm.Handles.SP_BS_FullCompens_Table_CI1,     'Checked', flag5);

maj_TooltipBoutonSelectionImage(this)
