  % Description
  %   Analyse du nom de la callback et renvoi si le traitement peut �tre
  %   r�alis� sur l'image enti�re ou le zoom actuel. Si le nom de la
  %   callback se termine par :
  %     _CI0 : traitement Partiel Image Impossible 
  %     _CI1 : traitement Partiel Image Possible 
  %     _CI2 : traitement sur Partiel Image seulement (impossible sur image enti�re) 
  %
  % Syntax
  %   [flag, subx, suby, nom, XLim, YLim, flagImageEntiere] = getSubxSubyContextuel(this, ...)
  %
  % Input Arguments
  %   this : clc_image instance
  %
  % Name-only Arguments
  %    : Extrapolation
  %
  % Output Arguments
  %   flag             : 0=KO, 1=OK
  %   subx             : subcolumns of the zoomed imaege
  %   suby             : sublines of the zoomed imaege
  %   nom              : Name of the zoom or area
  %   XLim             : Range in x
  %   YLim             : Range in y
  %   flagImageEntiere : Traitement sur image partielle possible si 1, impossible si 0 
  %
  % More About : Remarques particuli�res concernant la fonction
  %
  % Examples
  %   x = 0:pi/360:pi; y = 1;
  %   a = entete(x, y)
  %   [flag, subx, suby, nom, XLim, YLim, flagImageEntiere] = getSubxSubyContextuel(this, varargin)
  %   a = entete(x, y, 'DetailsOn', 1);
  %
  % More about : r�f�rences de l'article d'o� est tir�e cette fonction
  % Authors : JMA + YYY
  % See also toto titi
  %
  % Reference pages in Help browser
  %   entete
  %   You have to edit entete.m to see the syntax for reference help
  %-------------------------------------------------------------------------------

  % TODO : placer flag en premier argument

function [flag, subx, suby, nom, XLim, YLim, flagImageEntiere] = getSubxSubyContextuel(this, varargin)

flagImageEntiere = 0;

switch nargin
    case 1 % Only one input parameter (this) 
        msg = 'AuChoix';
    case 2 % Two input parameters : (this, msg)
        msg = varargin{1};
    case 3 % Three input parameters : (this, callback name, msg)
        switch varargin{1}
            case {'SP_Sidescan'
                    'SP_MBES'
                    'SP_BS_AddOn'
                    'SP_BS_Analysis'
                    'SP_BS_Calib'
                    'SP_BS_Calib_ModeResult'
                    'SP_BS_Calib_CheckCompens'
                    'SP_AireInso'
                    'SP_DiagTx'
                    'SP_DiagRx'
                    'SP_TVG'
                    'SP_Mosaique'
                    'SP_WCD'
                    'SP_SBP'
                    'SP_Resol'
                    'SP_CreateLayers'
                    'SP_Apendices'
                    'ALL'; 'SEGY'; 'BOB'; 'HAC'; 'ODV'; 'SAR'; 'XTF'; 'OTUS'; 'SCAMPI'; 'RDF'
                    'S7K'; 'S7K_R&D'; 'SDF'; 'WCD'; 'REMUS'; 'CINNA'; 'NAVIGATION'; 'CAR'
                    'XYZ'; 'ExRaw'
                    'MonoCapteur'
                    'Help'}
                msg = varargin{2}; % The message is taken in the third input parameter
            otherwise
                msg = varargin{1};
        end
    otherwise
        my_breakpoint
end

flag = 1;

%% Traitements qui peuvent se faire sur toute l'image ou l'image visualis�e

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');

XLimImage = compute_XYLim(x);
YLimImage = compute_XYLim(y);

if all(nearlyEqual(this.visuLim(this.indImage, :), [XLimImage YLimImage], 0.001)) % Apr�s le 27/01/2014 pour mosa�ques OTUS, mission BICOSE :
    % Attention cette modif peur avoir des effets de bords !
    ImageZoomee = 0;
else
    ImageZoomee = 1;
end

if strcmp(msg, 'CurrentExtent')
    XLim = this.visuLim(this.indImage, 1:2);
    YLim = this.visuLim(this.indImage, 3:4);
    subx = find((x >= XLim(1)) & (x <= XLim(2)));
    suby = find((y >= YLim(1)) & (y <= YLim(2)));
    % TODO : Attention ces noms sont test�s dans la fonction : paramsMosaiqueAssemblage
    % Si modification ne pas oublier de r�percuter la modif dans cette fonction
    nom = Lang('Cadrage actuel', 'Current extent');
    XLim = [min(x(subx)) max(x(subx))];
    YLim = [min(y(suby)) max(y(suby))];
    return
end

if ~isempty(varargin) && strcmp(varargin{1}, 'Help')
    traitementPartielImagePossible = 0;
else
    if contains(msg, '_CI0') || strcmp(msg, 'Help') % TODO : Help est en principe intercept� avant
        traitementPartielImagePossible = 0;
    elseif contains(msg, '_CI1')
        traitementPartielImagePossible = 1;
    elseif contains(msg, '_CI2')
        traitementPartielImagePossible = 2;
    else
        traitementPartielImagePossible = 1;
    end
end

if traitementPartielImagePossible == 0
    subx = 1:length(x);
    suby = 1:length(y);
    nom = Lang('Image enti�re', 'Whole image');
    XLim = [min(x) max(x)];
    YLim = [min(y) max(y)];
    flagImageEntiere = 1;
    return
end

if traitementPartielImagePossible == 2
    XLim = this.visuLim(this.indImage, 1:2);
    YLim = this.visuLim(this.indImage, 3:4);
    subx = find((x >= XLim(1)) & (x <= XLim(2)));
    suby = find((y >= YLim(1)) & (y <= YLim(2)));
    nom = Lang('Cadrage actuel', 'Current extent');
    XLim = [min(x(subx)) max(x(subx))];
    YLim = [min(y(suby)) max(y(suby))];
    return
end

if ImageZoomee && traitementPartielImagePossible
    str{1} = Lang('Image enti�re', 'Whole image');
    str{2} = Lang('Cadrage actuel', 'Current extent');
else
    str{1} = Lang('Image enti�re', 'Whole image');
end

% Ajout des Zones d'�tude
subZoneEtude = [];
for k=1:length(this.ZoneEtude)
    if isequal(this.ZoneEtude(k).TagSynchroX, this.Images(this.indImage).TagSynchroX) && ...
       isequal(this.ZoneEtude(k).TagSynchroY, this.Images(this.indImage).TagSynchroY)
        subZoneEtude(end+1) = k; %#ok<AGROW>
        
        str{end+1} = this.ZoneEtude(k).Name;  %#ok<AGROW>
        XLim = this.visuLim(this.indImage, 1:2);
        YLim = this.visuLim(this.indImage, 3:4);
        deltax = mean(diff(x));
        deltay = mean(diff(y));
        flag = compareZoneEtude_Zoom(this.ZoneEtude(k).XLim, this.ZoneEtude(k).YLim, XLim, YLim, deltax, deltay);
        if flag % L'image est positionnee sur la zone d'�tude k
            if ImageZoomee && traitementPartielImagePossible
                str{2} = ['Current extent = ' this.ZoneEtude(k).Name];
            else
                str{1} = ['Whole image = ' this.ZoneEtude(k).Name];
            end
        end
    end
end

if length(str) == 1
    PartieVisualisee = 1;
else
    str1 = 'Traitement partiel ou total ?';
    str2 = 'Partial or total processing';
    [PartieVisualisee, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
    if ~flag
        subx = [];
        suby = [];
        nom  = [];
        XLim = [];
        YLim = [];
        return
    end
    drawnow % Pour faire disparaitre la fen�tre
end

nom = str{PartieVisualisee};
if PartieVisualisee == 1
    subx = 1:length(x);
    suby = 1:length(y);
    XLim = [min(x(subx)) max(x(subx))];
    YLim = [min(y(suby)) max(y(suby))];
    flagImageEntiere = 1;
else
    if ImageZoomee && PartieVisualisee == 2
        XLim = this.visuLim(this.indImage, 1:2);
        YLim = this.visuLim(this.indImage, 3:4);
        subx = find((x >= XLim(1)) & (x <= XLim(2)));
        suby = find((y >= YLim(1)) & (y <= YLim(2)));
        XLim = [min(x(subx)) max(x(subx))];
        YLim = [min(y(suby)) max(y(suby))];
    else
        if ImageZoomee
            PartieVisualisee = PartieVisualisee - 2;
        else
            PartieVisualisee = PartieVisualisee - 1;
        end
        XLim = this.ZoneEtude(subZoneEtude(PartieVisualisee)).XLim;
        YLim = this.ZoneEtude(subZoneEtude(PartieVisualisee)).YLim;
        subx = find((x >= XLim(1)) & (x <= XLim(2)));
        suby = find((y >= YLim(1)) & (y <= YLim(2)));
    end
end
