% Examples
%   h = GUI_TypeIntecationCleanData('MotherFig', 1)
%   [flag, typeInteraction, Value] = getTypeIntecationCleanData(h)

function [flag, typeInteraction, Value] = getTypeIntecationCleanData(h)

typeInteraction = [];
Value = [];

flag = ishandle(h);
if ~flag
    return
end

hc    = get(h,      'Children');
hc1   = get(hc(1),  'Children');
Value = get(hc1(1), 'String');
hc2   = get(hc(2),  'Children');
for k=1:length(hc2)
    state(k) = get(hc2(k), 'Value'); %#ok<AGROW>
end
typeInteraction = length(hc2) - find(state) + 1;

Value = str2double(Value);

flag = 1;
