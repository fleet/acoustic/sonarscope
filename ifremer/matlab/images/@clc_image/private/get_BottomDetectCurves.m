function BD = get_BottomDetectCurves(this)

BD = [];
pppp = findobj(this.hAxePrincipal, 'type', 'line', 'Tag', 'BottomDetector');
for k=1:length(pppp)
    BD(k).Color = get(pppp(k), 'Color'); %#ok<AGROW>
    BD(k).LineStyle = get(pppp(k), 'LineStyle'); %#ok<AGROW>
    BD(k).LineWidth = get(pppp(k), 'LineWidth'); %#ok<AGROW>
    BD(k).Marker = get(pppp(k), 'Marker'); %#ok<AGROW>
    BD(k).MarkerSize = get(pppp(k), 'MarkerSize'); %#ok<AGROW>
    BD(k).XData = get(pppp(k), 'XData'); %#ok<AGROW>
    BD(k).YData = get(pppp(k), 'YData'); %#ok<AGROW>
    BD(k).UserData = get(pppp(k), 'UserData'); %#ok<AGROW>
    BD(k).TagSynchroYScale = getappdata(pppp(k), 'TagSynchroYScale'); %#ok<AGROW>
end
