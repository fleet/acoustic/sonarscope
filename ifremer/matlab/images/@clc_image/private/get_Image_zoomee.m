% Recuperation de la partie d'image zoomee
%
% Syntax
%   [subx, suby, x, y, I] = get_Image_zoomee(a)
%
% Input Arguments
%   a : instance de clc_image
%
% Name-Value Pair Arguments
%   indImage : Indice de l'image a extraire (this.indImage par defaut)
%
% Output Arguments
%   subx : Colonnes correspondant a l'extraction
%   suby : Lignes correspondant a l'extraction
%   x    : Abscisses
%   y    : Ordonn�es
%   I    : Partie de l'image visible sur l'�cran
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [subx, suby, x, y, I]  = get_Image_zoomee(this, varargin)

[varargin, indImage]      = getPropertyValue(varargin, 'indImage', this.indImage);
[varargin, UnderSampling] = getPropertyValue(varargin, 'UnderSampling', 0); %#ok<ASGLU>

%% R�cuperation de limites de la visualisation

currentAxe  = this.hAxePrincipal;
XLim = get(currentAxe, 'XLim');
YLim = get(currentAxe, 'YLim');

%% R�cuperation des valeurs des axes

x = get(this.Images(indImage), 'x');
y = get(this.Images(indImage), 'y');

%% Extraction de l'image

subx = find((x >= XLim(1)) & (x <= XLim(2)));
suby = find((y >= YLim(1)) & (y <= YLim(2)));

if nargout == 2
    return
end
% I = I(suby, subx);

if UnderSampling
    rap = (length(subx) * length(suby)) / 1e7; % SizePhysTotalMemory
    if rap > 1
        rap = ceil(sqrt(rap));
        subx = subx(1:rap:end);
        suby = suby(1:rap:end);
    end
end

x = x(subx);
y = y(suby);

if nargout == 5
    I = get_val_ij(this.Images(indImage), suby, subx);
end
