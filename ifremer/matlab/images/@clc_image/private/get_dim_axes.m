% Retourne les dimensions en w et h de l'axe specifie
%
% Syntax
%   dimAxe = get_dim_axes(a, axe)
% 
% Input Arguments
%   a   : instance de clc_image
%   axe : axe de destination {'profilX', 'profilY', 'principal'}
%
% Output Arguments
%   dimAxe   : w et h de l axe
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: get_dim_axes.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function dimAxe = get_dim_axes(this, axe)

% ----------------------
% Traitement selon l'axe

switch axe

    case 'profilX'
        currentUnits = get(this.hAxeProfilX, 'units');
        set(this.hAxeProfilX, 'units', 'pixels');
        p = get(this.hAxeProfilX, 'Position');
        set(this.hAxeProfilX, 'units', currentUnits);
        dimAxe = p(3:4);

    case 'profilY'
        currentUnits = get(this.hAxeProfilY, 'units');
        set(this.hAxeProfilY, 'units', 'pixels');
        p = get(this.hAxeProfilY, 'Position');
        set(this.hAxeProfilY, 'units', currentUnits);
        dimAxe = p(3:4);

    case 'principal'
        currentUnits = get(this.hAxePrincipal, 'units');
        set(this.hAxePrincipal, 'units', 'pixels');
        p = get(this.hAxePrincipal, 'Position');
        set(this.hAxePrincipal, 'units', currentUnits);
        dimAxe = p(3:4);

    otherwise
        disp('clc_image /get_dim_axes : Invalid axe name');
end
