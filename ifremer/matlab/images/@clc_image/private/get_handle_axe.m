% Retourne le handle matlab de l'axe indique
%
% Syntax
%   hAxe = get_handle_axe(a, axe)
% 
% Input Arguments
%   a   : instance de clc_image
%   axe : axe de destination {'profilX', 'profilY', 'principal'}
%
% Output Arguments
%   hAxe : handle matlab de l axe
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function hAxe = get_handle_axe(this, axe)

%% éparation cas non edité / cas edité

% if ~is_edit(this)
if ~is_edit(this.composant)
    hAxe = [];
else
    switch axe
        case 'profilX'
            hAxe = this.hAxeProfilX;
        case 'profilY'
            hAxe = this.hAxeProfilY;
        case 'principal'
            hAxe = this.hAxePrincipal;
        otherwise
            disp('clc_image/get_handle_axe : Invalid axe name');
    end
end
