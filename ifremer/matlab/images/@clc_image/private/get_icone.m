function I = get_icone(this, Tag) %#ok

switch Tag
    case 'VisuImage'
        I = imread(getNomFicDatabase('VisuImage.tif'));
    case 'VisuContours'
        I = imread(getNomFicDatabase('VisuContoursGael.tif'));
    case '3D'
        I = imread(getNomFicDatabase('Visu3D.tif'));
    case 'Clone'
        I = imread(getNomFicDatabase('CloneWindow.tif'));
    case 'VisuPlot'
        I = imread(getNomFicDatabase('VisuPlot.tif'));
        
    case 'Zoom1'
        I = imread(getNomFicDatabase('Zoom_1pour1.tif'));
    case 'ZoomInX'
        I = imread(getNomFicDatabase('ZoomInX.tif'));
    case 'ZoomOutX'
        I = imread(getNomFicDatabase('ZoomOutX.tif'));
    case 'ZoomInY'
        I = imread(getNomFicDatabase('ZoomInY.tif'));
    case 'ZoomOutY'
        I = imread(getNomFicDatabase('ZoomOutY.tif'));
    case 'ZoomPrecedent'
        I = imread(getNomFicDatabase('ZoomPrecedent.tif'));
    case 'ZoomSuivant'
        I = imread(getNomFicDatabase('ZoomSuivant.tif'));
    case 'MoveDown'
        I = imread(getNomFicDatabase('MoveDown.tif'));
    case 'MoveUp'
        I = imread(getNomFicDatabase('MoveUp.tif'));
    case 'MoveLeft'
        I = imread(getNomFicDatabase('MoveLeft.tif'));
    case 'MoveRight'
        I = imread(getNomFicDatabase('MoveRight.tif'));
    case 'AxisEqual'
        I = imread(getNomFicDatabase('AxisEqual.tif'));
    case 'ZoomQL'
        I = imread(getNomFicDatabase('ZoomQL.tif'));
    case 'Import'
        I = imread(getNomFicDatabase('Import.tif'));
    case 'Export'
        I = imread(getNomFicDatabase('Export.tif'));
    case 'Info'
        I = imread(getNomFicDatabase('Info.tif'));
    case 'Colormap'
        I = imread(getNomFicDatabase('Colormap.tif'));
    case 'Colorbar'
        I = imread(getNomFicDatabase('Colorbar.tif'));
    case 'ModePointAndClick'
        I = imread(getNomFicDatabase('ModePointAndClick.tif'));
    case 'ModeZoom'
        I = imread(getNomFicDatabase('ModeZoom.tif'));
    case 'ModeCentrage'
        I = imread(getNomFicDatabase('ModeCentrage.tif'));
    case 'ModeCoupe'
        I = imread(getNomFicDatabase('ModeCoupe.tif'));
    case 'ModeNaN'
        I = imread(getNomFicDatabase('ModeNaN.tif'));
    case 'Grid'
        I = imread(getNomFicDatabase('Grid.tif'));
    case 'Contraste'
        I = imread(getNomFicDatabase('Contraste.tif'));
    case 'Video'
        I = imread(getNomFicDatabase('VideoInverse.tif'));
    case 'ImageTool'
        I = imread(getNomFicDatabase('ImageTool.tif'));
    case 'XDir'
        I = imread(getNomFicDatabase('flechesLR.tif'));
    case 'YDir'
        I = imread(getNomFicDatabase('flechesUD.tif'));
    case 'ZoneEtude'
        I = imread(getNomFicDatabase('ZoneEtude.tif'));
    case 'Compute'
        I = imread(getNomFicDatabase('Compute.tif'));
    case 'ComputeSonar'
        I = imread(getNomFicDatabase('ComputeSonar.tif'));
    case 'Stats'
        I = imread(getNomFicDatabase('Stats.tif'));
    case 'CoupeHorz'
        I = imread(getNomFicDatabase('CoupeHorz.tif'));
    case 'CoupeVert'
        I = imread(getNomFicDatabase('CoupeVert.tif'));
    case 'Delete'
%         I = imread(getNomFicDatabase('Delete.tif'));
        I = imread(getNomFicDatabase('Poubelle.tif'));
    case 'csh_icon'
        I = imread(getNomFicDatabase('csh_icon.png'));
        I(I==0) = 179;
    case 'point'
        I = imread(getNomFicDatabase('point.png'));
        I = uint8(I/256);
        I(I==0) = 179;
    case 'tool_data_brush'
        I = imread(getNomFicDatabase('tool_data_brush.png'));
        I = uint8(I/256);
        I(I==0) = 179;
        
    case 'QL1'
        I = imread(getNomFicDatabase('QL1.tif'));
        I(I==255) = 179;
    case 'QL2'
        I = imread(getNomFicDatabase('QL2.tif'));
        I(I==255) = 179;
    case 'QL3'
        I = imread(getNomFicDatabase('QL3.tif'));
        I(I==255) = 179;
        
    case 'ParallelTbxOff'
        I = imread(getNomFicDatabase('ParallelTbxOff.tif'));
        I(I==255) = 179;
    case 'ParallelTbxOn'
        I = imread(getNomFicDatabase('ParallelTbxOn.tif'));
        I(I==255) = 179;

    otherwise
        str = sprintf('Pas d''icone definie pour Tag=%s', Tag);
        my_warndlg(str, 1);
end


if size(I, 3) == 4
    I = I(:,:,4);
    I(:,:,2) = I;
    I(:,:,3) = I(:,:,1);
end

if size(I,3) == 1
    I(:,:,2) = I(:,:,1);
    I(:,:,3) = I(:,:,1);
end

if max(I(:)) > 256
    % Icone pas comme les autres : action Gael
%     Tag
%     whos I
    I = uint8(floor(double(I) / 256));
end
% II = I;
% 
% 
% % ------------------------------
% % Reduction de lataille en 24x24
% 
% n = size(I,1);
% m = size(I,2);
% xi = linspace(1, m, 20);
% yi = linspace(1, n, 20);
% [xi,yi] = meshgrid(xi, yi);
% I = double(I);
% for i=1:3
%     I24(:,:,i) = interp2(I(:,:,i), xi, yi, 'linear');
% end
% I = uint8(I24);
% sub = find(I == 151);
sub = (I == 151);
I(sub) = 179;

I = uint8(I);
