% Indices en x et en y de l'image visualisee
%
% Syntax
%   [subx, suby, x, y] = get_subx_suby(this)
%
% Input Arguments
%   a : instance de clc_image
%
% Output Arguments
%   subx : Indices en x
%   suby : Indices en y
%
% Examples
%
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [subx, suby, x, y] = get_subx_suby(this)

%% Récuperation de limites de la visualisation

currentAxe  = this.hAxePrincipal;
XLim = get(currentAxe, 'XLim');
YLim = get(currentAxe, 'YLim');

%% Récuperation des valeurs des axes

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');

%% Extraction de l'image

subx = find((x >= XLim(1)) & (x <= XLim(2)));
suby = find((y >= YLim(1)) & (y <= YLim(2)));
x = x(subx);
y = y(suby);
