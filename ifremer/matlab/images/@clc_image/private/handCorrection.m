function this = handCorrection(this)

str1 = sprintf('Le m�me type de fonctionnalit� est maintenant disponible via le type d''interaction de souris "Nettoyage".\n\nATTENTION : ce processus modifie l''image courante, soyez s�r de pouvoir retrouver l''image originale soit en la sauvegardant en ".ers" soit en faisant une extraction (� moins que cette image provienne directement d''un fichier que vous pourrez � tout moment recharger).');
str2 = sprintf('You can also use mouse interaction = "Clean data" to perform this manual correction.\n\nWarning : this image will be modified. Be sure you can reload the original one. Save it in ".ers"  or do an "Extraction" otherwise.');
my_warndlg(Lang(str1,str2), 1);

%% R�cuperation des coordonn�es de l'image visualis�e

TypeInteractionSouris = this.TypeInteractionSouris;

if strcmp(TypeInteractionSouris, 'PointClick')
    this.TypeInteractionSouris = 'PointClick';
    this = traiter_typeInteractionSouris(this);
end

flagYes = 1;
while flagYes
    str1 = 'roipoly en cours, utilisez la souris pour contourer des r�gions.';
    str2 = 'Use the mouse to countour the regions.';
    disp(Lang(str1,str2))

    [this.Images(this.indImage), this.SetValue] = hand_correction(this.Images(this.indImage), this.SetValue, ...
        this.hfig, this.hAxePrincipal);

    %% Affichage des profils et des valeurs

    this = show_image(this);
    this = show_profils(this);

    %% On continue l'op�ration ?

    str1 = 'Voulez-vous d�finir une autre r�gion d''int�r�t ?';
    str2 = 'Another Region Of Interest ?';
    [choix, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    flagYes = (choix ==  1);
end

h = findobj(this.hAxePrincipal, 'Tag', 'PlotRegionOfInterest');
for k=1:length(h)
    delete(h(k));
end

if strcmp(TypeInteractionSouris, 'PointClick')
    this.TypeInteractionSouris = 'PointClick';
    this = traiter_typeInteractionSouris(this);
end
