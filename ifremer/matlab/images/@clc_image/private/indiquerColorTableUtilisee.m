% Indication de la table de couleur sélectionnée dans la liste des tables de couleur
%
% Syntax
%   indiquerColorTableUtilisee(this, Num)
%
% Input Arguments
%   this : Instance de clc_image
%   Num  : Numero de la table de couleur sélectionnée
%
% Examples
%   indiquerColorTableUtilisee(this, Num);
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function indiquerColorTableUtilisee(this, Num)

h = findobj(this.hfig, 'Tag', 'ColorTable1');
h = get(h, 'Parent');
h = get(h, 'children');
for i=1:length(h)
    set(h(i), 'Checked', 'Off');
end

% ColormapIndex = Num;
Tag = ['ColorTable' num2str(Num)];
set(this.ihm.Handles.(Tag), 'Checked', 'On')
