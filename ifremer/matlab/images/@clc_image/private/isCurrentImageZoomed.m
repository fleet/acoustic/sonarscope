function flag = isCurrentImageZoomed(this)

% ------------------------------------------------------------------------
% Traitements qui peuvent se faire sur toute l'image ou l'image visualisee

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');

XLimImage = compute_XYLim(x);
YLimImage = compute_XYLim(y);

% La visualisation ne correspond pas a l'image entiere (Pas de zoom)
%     if isequal(this.visuLim(this.indImage, :), [x(1) x(end) y(1) y(end)])
if isequal(this.visuLim(this.indImage, :), [XLimImage YLimImage])
    flag = 0;
else
    flag = 1;
end
