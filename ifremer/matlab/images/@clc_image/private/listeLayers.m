% Liste  de layers
% 
% nomsLayers = listeLayers(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   nomsLayers : Noms des images ...
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function nomsLayers = listeLayers(this, varargin)

for k=length(this.Images):-1:1
    nomsLayers{k} = this.Images(k).Name;
end
