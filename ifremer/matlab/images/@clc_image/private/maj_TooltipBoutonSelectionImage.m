function maj_TooltipBoutonSelectionImage(this)

h = get_handle(this.ihm.IdentImage.cpn);
str = get(h, 'Tooltip');
k = strfind(str, '(');
if ~isempty(k)
    str(k-1:end) = [];
end
str = sprintf('%s (%d/%d)', str, this.indImage, length(this.Images));
set(h, 'Tooltip', str)