function maj_menu_CacheDirectory(this)

global useCacheNetcdf %#ok<GVMIS>

if isempty(useCacheNetcdf)
    useCacheNetcdf = 3; % Modif JLA le 05/10/2020 : Netcdf and delete XMLBin par d�faut � partir du 05/10/2020
end

if useCacheNetcdf == 0 % TODO Trouver pourquoi initialis� � 0
    useCacheNetcdf = 3;
end

switch useCacheNetcdf
    case 1
        set(this.ihm.Handles.CACHE_CreateXMLBinOnly,          'Checked', 'On');
        set(this.ihm.Handles.CACHE_CreateNetcdfAndKeepXMLBin, 'Checked', 'Off');
        set(this.ihm.Handles.CACHE_CreateNetcdfAndDelXMLBin,  'Checked', 'Off');
    case 2
        set(this.ihm.Handles.CACHE_CreateXMLBinOnly,          'Checked', 'Off');
        set(this.ihm.Handles.CACHE_CreateNetcdfAndKeepXMLBin, 'Checked', 'On');
        set(this.ihm.Handles.CACHE_CreateNetcdfAndDelXMLBin,  'Checked', 'Off');
    case 3
        set(this.ihm.Handles.CACHE_CreateXMLBinOnly,          'Checked', 'Off');
        set(this.ihm.Handles.CACHE_CreateNetcdfAndKeepXMLBin, 'Checked', 'Off');
        set(this.ihm.Handles.CACHE_CreateNetcdfAndDelXMLBin,  'Checked', 'On');
end
