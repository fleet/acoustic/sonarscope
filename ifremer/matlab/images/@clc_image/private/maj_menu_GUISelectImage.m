function maj_menu_GUISelectImage(this)

global IfrTbxGUISelectImage %#ok<GVMIS>

if isempty(IfrTbxGUISelectImage)
    IfrTbxGUISelectImage = 1;
end

if IfrTbxGUISelectImage == 1
    set(this.ihm.Handles.TagPreferencesGUISelectImageClassical,  'Checked', 'On');
    set(this.ihm.Handles.TagPreferencesGUISelectImageSimplified, 'Checked', 'Off');
else
    set(this.ihm.Handles.TagPreferencesGUISelectImageClassical,  'Checked', 'Off');
    set(this.ihm.Handles.TagPreferencesGUISelectImageSimplified, 'Checked', 'On');
end
