function this = maj_menu_GraphicsOnTop(this)

global IfrTbxGraphicsOnTop %#ok<GVMIS>

EtatAvant = get(this.ihm.Handles.TagPreferencesGraphicsOnTop1, 'Checked');

if isempty(IfrTbxGraphicsOnTop)
    disp('Message for JMA : IfrTbxGraphicsOnTop is empty in maj_menu_GraphicsOnTop, forced to 2')
    IfrTbxGraphicsOnTop = '2';
end

switch IfrTbxGraphicsOnTop
    case '1'
        set(this.ihm.Handles.TagPreferencesGraphicsOnTop1, 'Checked', 'On');
        set(this.ihm.Handles.TagPreferencesGraphicsOnTop2, 'Checked', 'Off');
    case '2'
        set(this.ihm.Handles.TagPreferencesGraphicsOnTop1, 'Checked', 'Off');
        set(this.ihm.Handles.TagPreferencesGraphicsOnTop2, 'Checked', 'On');
end
EtatApres = get(this.ihm.Handles.TagPreferencesGraphicsOnTop1, 'Checked');

% if (IfrTbxGraphicsOnTop == 1) || ~strcmp(EtatAvant{1}, EtatApres{1})

% TODO : en attendant la remise en �tat des handles boutons et menus
if ischar(EtatAvant)
    EtatAvant = {EtatAvant};
end
if ischar(EtatApres)
    EtatApres = {EtatApres};
end
% if ~strcmp(EtatAvant{1}, EtatApres{1})
if ~isequal(EtatAvant{1}, EtatApres{1}) % Modif JMA le 16/06/2021
    this = show_image(this, 'MajColorbar', 0);
end

%{
% -----------------------------
% Synth�se de tous les contours

[this.Images(this.indImage), str] = ROI_summary(this.Images(this.indImage));

% -------------------------------------------------------------
% Lecture des r�gions d'int�r�t contenues dans l'image courante

RegionOfInterest = get(this.Images(this.indImage), 'RegionOfInterest');
if isempty(RegionOfInterest)
    if ~NoQuestion
        my_warndlg('No Region Of Interest in this image', 1);
    end
    return
end
%}

