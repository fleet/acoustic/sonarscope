function this = maj_menu_IdentAlgoSnippets2OneValuePerBeam(this)

global IdentAlgoSnippets2OneValuePerBeam %#ok<GVMIS>

if isempty(IdentAlgoSnippets2OneValuePerBeam)
%     disp('Message for JMA : IdentAlgoSnippets2OneValuePerBeam is empty in maj_menu_GraphicsOnTop, forced to 2')
    IdentAlgoSnippets2OneValuePerBeam = 2;
end

switch IdentAlgoSnippets2OneValuePerBeam
    case 1 % dB
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_dB,  'Checked', 'On');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Amp, 'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Enr, 'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Med, 'Checked', 'Off');
    case 2 % Amp
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_dB,  'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Amp, 'Checked', 'On');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Enr, 'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Med, 'Checked', 'Off');
    case 3 % Enr
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_dB,  'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Amp, 'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Enr, 'Checked', 'On');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Med, 'Checked', 'Off');
    case 4 % Median
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_dB,  'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Amp, 'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Enr, 'Checked', 'Off');
        set(this.ihm.Handles.TagIdentAlgoSnippets2OneValuePerBeam_Med, 'Checked', 'On');
end
