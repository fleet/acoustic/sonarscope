function maj_menu_Lang(this)

if strcmp(Lang('French', 'English'), 'French')
    set(this.ihm.Handles.TagPreferencesLangFrench,  'Checked', 'On');
    set(this.ihm.Handles.TagPreferencesLangEnglish, 'Checked', 'Off');
else
    set(this.ihm.Handles.TagPreferencesLangFrench,  'Checked', 'Off');
    set(this.ihm.Handles.TagPreferencesLangEnglish, 'Checked', 'On');
end
