function this = maj_menu_LevelQuestions(this, varargin)

global IfrTbxLevelQuestions %#ok<GVMIS>

switch IfrTbxLevelQuestions
    case '1'
        set(this.ihm.Handles.TagPreferencesLevelQuestions1, 'Checked', 'On');
        set(this.ihm.Handles.TagPreferencesLevelQuestions2, 'Checked', 'Off');
        set(this.ihm.Handles.TagPreferencesLevelQuestions3, 'Checked', 'Off');
        
    case '2'
        set(this.ihm.Handles.TagPreferencesLevelQuestions1, 'Checked', 'Off');
        set(this.ihm.Handles.TagPreferencesLevelQuestions2, 'Checked', 'On');
        set(this.ihm.Handles.TagPreferencesLevelQuestions3, 'Checked', 'Off');
        
    case '3'
        set(this.ihm.Handles.TagPreferencesLevelQuestions1, 'Checked', 'Off');
        set(this.ihm.Handles.TagPreferencesLevelQuestions2, 'Checked', 'Off');
        set(this.ihm.Handles.TagPreferencesLevelQuestions3, 'Checked', 'On');
end

switch IfrTbxLevelQuestions
    case {'QL1'; '1'}
        icone = get_icone(this, 'QL1');
        this.ihm.QuestionLevel.cpn = set_CData(this.ihm.QuestionLevel.cpn, IconeEncocheDroite(icone));
        
    case {'QL2'; '2'}
        icone = get_icone(this, 'QL2');
        this.ihm.QuestionLevel.cpn = set_CData(this.ihm.QuestionLevel.cpn, IconeEncocheDroite(icone));
        
    case {'QL3'; '3'}
        icone = get_icone(this, 'QL3');
        this.ihm.QuestionLevel.cpn = set_CData(this.ihm.QuestionLevel.cpn, IconeEncocheDroite(icone));
end
