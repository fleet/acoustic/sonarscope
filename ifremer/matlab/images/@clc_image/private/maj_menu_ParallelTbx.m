function maj_menu_ParallelTbx(this, varargin)

global useParallel %#ok<GVMIS>

if isempty(useParallel)
%     useParallel = 0; % Modif JMA le 06/01/2020
    useParallel = getNumberOfWorkers;
end

if useParallel
    icone = get_icone(this, 'ParallelTbxOn');
else
    icone = get_icone(this, 'ParallelTbxOff');
end

this.ihm.ParallelTbx.cpn = set_CData(this.ihm.ParallelTbx.cpn, IconeEncocheDroite(icone));
