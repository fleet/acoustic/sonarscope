function maj_menu_TempDir

global IfrTbxTempDir %#ok<GVMIS>

str1 = 'Choisissez votre r�pertoire de stockage temporaire par d�faut.';
str2 = 'Please, select your temporary default directory';
[flag, X] = my_uigetdir(my_tempdir, Lang(str1,str2));
if ~flag
    IfrTbxTempDir = X;
end
