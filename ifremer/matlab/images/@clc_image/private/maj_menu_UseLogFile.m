function maj_menu_UseLogFile(this)

global IfrTbxUseLogFile %#ok<GVMIS>

switch IfrTbxUseLogFile
    case 'On'
        set(this.ihm.Handles.TagPreferencesUseLogFileNo,  'Checked', 'Off');
        set(this.ihm.Handles.TagPreferencesUseLogFileYes, 'Checked', 'On');
    case 'Off'
        set(this.ihm.Handles.TagPreferencesUseLogFileNo,  'Checked', 'On');
        set(this.ihm.Handles.TagPreferencesUseLogFileYes, 'Checked', 'Off');
end
