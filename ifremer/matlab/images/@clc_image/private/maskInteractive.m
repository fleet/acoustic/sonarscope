function [this, a, flag] = maskInteractive(this, creationLayer, nomLayer, indLayerMask)
    
% R�cuperation des coordonn�es de l'image visualis�e
[subx, suby] = get_subx_suby(this);

TypeInteractionSouris = this.TypeInteractionSouris;

if ~strcmp(TypeInteractionSouris, 'PointClick')
    this.TypeInteractionSouris = 'PointClick';
    this = traiter_typeInteractionSouris(this);
    this = modify_ihm_TypeInteraction(this);
    this.TypeInteractionSouris = 'None';
    this = traiter_typeInteractionSouris(this);
    this = callback_typeInteractionSouris(this, 'PointClick');
%     set(this.hfig, 'Pointer', 'arrow')
% TODO : en fait, la modif n'est possible qu'� partir de cli_image !
end

[flag, a, this.Images(this.indImage)] = ROI_manual(this.Images(this.indImage), creationLayer, nomLayer, this.Images(indLayerMask), ...
                      this.hfig, this.hAxePrincipal, 'subx', subx, 'suby', suby);

h = findobj(this.hAxePrincipal, 'Tag', 'PlotRegionOfInterest');
for k=1:length(h)
    delete(h(k));
end

if ~creationLayer
    this.Images(indLayerMask) = a;
    flag = 0;
end

% if strcmp(TypeInteractionSouris, 'PointClick')
%     this.TypeInteractionSouris = 'PointClick';
%     this = traiter_typeInteractionSouris(this);
% end
this.TypeInteractionSouris = TypeInteractionSouris;
this = traiter_typeInteractionSouris(this);
this = modify_ihm_TypeInteraction(this);
