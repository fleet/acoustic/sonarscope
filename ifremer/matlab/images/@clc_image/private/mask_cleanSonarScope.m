% Suppression des courbes affich�es dans SonarScope sur l'image courante
%
% Syntax
%   mask_cleanSonarScope(this)
%
% Input Arguments
%   this : Instance de cl_image
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = mask_cleanSonarScope(this)

h = findobj(this.hAxePrincipal, 'Tag', 'PlotRegionOfInterest');
for k=1:length(h)
    delete(h(k));
end