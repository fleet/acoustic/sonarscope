function [flag, this] = modifyAttributsImages(this)

[flag, indImage, strOut] = summary(this.Images, 'ExportResults', 0, ...
    'SelectLayer', this.indImage, 'QueryMode', 'MultiSelect');
if ~flag
    % Bouton Cancel
    return
end
if isempty(indImage) || isempty(strOut)
    % Filtre total ou bouton Cancel
    return
end
% R�cup�ration des indices sur les layers d'origine.
for i=1:length(this.Images)
    strIn{i,1} = this.Images(i).Name; %#ok<AGROW>
end
% Remise en ordre par identification du nom du layer.
strOut = strOut(indImage,1);

% R�cup�ration des indices de tri.
[C,ia,ib] = intersect(strIn,strOut,'stable'); %#ok<ASGLU>
liste = ia';

if isempty(liste)
    flag = 0;
    return
end

% if any(this.indImage == liste)
liste = [this.indImage liste(liste ~= this.indImage)];
% end
strListeProperties = {'TagSynchroX'
    'TagSynchroY'
    'TagSynchroContrast'
    'Unit'
    'XLabel'
    'XUnit'
    'YLabel'
    'YUnit'
    'DataType'
    'GeometryType'
    'ColormapIndex'
    'Video'
    'CLim'
    'XDir'
    'YDir'};
str1 = 'Propri�t�s � modifier';
str2 = 'Properties to set';
[listeProperties, flag] = my_listdlg(Lang(str1,str2), strListeProperties, 'InitialValue', 1:2);
if ~flag
    return
end
if isempty(listeProperties)
    flag = 0;
    return
end

%% test get ", 'PropertyName')"          test set  ", 'PropertyName', "        verif "'PropertyName'"
value{1}  = this.Images(liste(1)).TagSynchroX;        % Fait pour get
value{2}  = this.Images(liste(1)).TagSynchroY;        % Fait pour get
value{3}  = this.Images(liste(1)).TagSynchroContrast; % Fait pour get
value{4}  = this.Images(liste(1)).Unit;               % Fait pour get
value{5}  = this.Images(liste(1)).XLabel;             % Fait pour get
value{6}  = this.Images(liste(1)).XUnit;              % Fait pour get
value{7}  = this.Images(liste(1)).YLabel;             % Fait pour get
value{8}  = this.Images(liste(1)).YUnit;              % Fait pour get
value{9}  = this.Images(liste(1)).DataType;           % Fait pour get
value{10} = this.Images(liste(1)).GeometryType;       % Fait pour get
value{11} = this.Images(liste(1)).ColormapIndex;      % Fait pour get
value{12} = this.Images(liste(1)).Video;              % Fait pour get
value{13} = this.Images(liste(1)).CLim;               % Fait pour get
value{14} = this.Images(liste(1)).XDir;               % Fait pour get
value{15} = this.Images(liste(1)).YDir;               % Fait pour get

if isempty(value{4}) % Unit
    value{4} = '';
end
if isempty(value{6}) % XUnit
    value{6} = '';
end
if isempty(value{8}) % YUnit
    value{8} = '';
end

sub = find(listeProperties <= 8);
if ~isempty(sub)
    [V, flag] = my_inputdlg(strListeProperties(listeProperties(sub)), value(listeProperties(sub)));
    if ~flag
        return
    end
    for kPropriete=1:length(sub)
        value{listeProperties(kPropriete)} = V{kPropriete};
    end
end

if any(listeProperties == 9) % DataType
    [flag, DataType] = cl_image.edit_DataType('DataType', this.Images(liste(1)).DataType);
    if ~flag
        return
    end
    value{9} = DataType;
end

if any(listeProperties == 10) % GeometryType
    [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', this.Images(liste(1)).GeometryType);
    if ~flag
        return
    end
    value{10} = GeometryType;
end

if any(listeProperties == 11) % ColormapIndex
    [flag, ColormapIndex] = edit_ColormapIndex(this.Images(liste(1)));
    if ~flag
        return
    end
    value{11} = ColormapIndex;
end

if any(listeProperties == 12) % Video
    [flag, Video] = edit_Video(this.Images(liste(1)));
    if ~flag
        return
    end
    value{12} = Video;
end

if any(listeProperties == 13) % CLim
    [flag, CLim] = edit_CLim(this.Images(liste(1)));
    if ~flag
        return
    end
    value{13} = CLim;
end

if any(listeProperties == 14) % XDir
    [flag, X] = edit_XDir(this.Images(liste(1)));
    if ~flag
        return
    end
    value{14} = X;
end

if any(listeProperties == 15) % YDir
    [flag, X] = edit_YDir(this.Images(liste(1)));
    if ~flag
        return
    end
    value{15} = X;
end

for kImage=1:length(liste)
    for kPropriete=1:length(listeProperties)
        PN = strListeProperties{listeProperties(kPropriete)};
        PV = value{listeProperties(kPropriete)};
        this.Images(liste(kImage)) = set(this.Images(liste(kImage)), PN, PV);
    end
end

this = process_synchroX(this);
this = process_synchroY(this);
