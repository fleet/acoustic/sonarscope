function this = modify_ihm_TypeInteraction(this)

switch this.TypeInteractionSouris
    case 'PointClick'
        icone = get_icone(this, 'ModePointAndClick');
        icone = IconeEncocheDroite(icone);
        this.Pointer = 'arrow';
        
    case 'ZoomInteractif'
        icone = get_icone(this, 'ModeZoom');
        icone = IconeEncocheDroite(icone);
        pointer_loupe(this.hfig);
        this.Pointer = 'custom';
        this.PointerShapeCData = get(this.hfig, 'PointerShapeCData');
        this.PointerShapeHotSpot = get(this.hfig, 'PointerShapeHotSpot');

    case 'Centrage'
        icone = get_icone(this, 'ModeCentrage');
        icone = IconeEncocheDroite(icone);
%         this.Pointer = 'fullcrosshair';
        this.Pointer = 'crosshair';
        
    case 'Coupe'
        icone = get_icone(this, 'ModeCoupe');
        icone = IconeEncocheDroite(icone);
        this.Pointer = 'crosshair';
        
    case 'SetNaN'
        icone = get_icone(this, 'ModePointAndClick');
        icone = icone / 2;
        icone = IconeEncocheDroite(icone);
        this.Pointer = 'arrow';
        
    case 'SetVal'
          icone = get_icone(this, 'ModePointAndClick');
          icone(:,:,2:3) = icone(:,:,2:3) / 2;
          icone = IconeEncocheDroite(icone);
          this.Pointer = 'arrow';

    case 'CleanData'
        icone = get_icone(this, 'tool_data_brush');
        this.Pointer = 'arrow';
        
    case 'MoveProfils'
        icone = get_icone(this, 'ModePointAndClick');
        icone = IconeEncocheDroite(icone);
        this.Pointer = 'arrow';
                
    case 'DrawHeight'
        icone = get_icone(this, 'ModeCoupe');
        icone = IconeEncocheDroite(icone);
        this.Pointer = 'crosshair';
        
    otherwise
% %         idle(this.hfig); % Curseur = fleche
end

if ismatrix(icone)
    icone(:,:,2) = icone(:,:,1);
    icone(:,:,3) = icone(:,:,1);
end

this.ihm.TypeInteractionSouris.cpn = set_CData(this.ihm.TypeInteractionSouris.cpn, icone);
