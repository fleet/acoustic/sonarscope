% Destruction graphique du composant
%
% Syntax
%   a = my_delete(a)
% 
% Input Arguments
%   a : instance de clc_image
%
% Remarks : doit etre surchargee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = my_delete(this)

%% Destruction si l'instance a une representation graphique

if is_edit(this)
    
    %% Destruction graphique de la super classe
    
    this.cl_component = delete(this.cl_component);
    
    %% Destruction des elements graphiques
    
    if ~isempty(this.globalFrame)
        this.globalFrame   = delete(this.globalFrame);
        this.globalFrame   = [];
        this.hAxePrincipal = [];
        this.hAxeProfilX   = [];
        this.hAxeProfilY   = [];
        this.hColorBar     = [];
        this.hToolBar      = [];
    end
end
