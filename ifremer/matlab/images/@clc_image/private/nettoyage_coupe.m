% Nettoyage des traces laissees par l'action" coupe dans l'image
% 
% Syntax
%   this = nettoyage_coupe(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: nettoyage_coupe.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = nettoyage_coupe(this)

ishandleFig = ishandle(this.coupe.fig);
if isempty(ishandleFig)
    ishandleFig = 0;
end

ishandleH = ishandle(this.coupe.handles);
if isempty(ishandleH)
    ishandleH = 0;
end

if ~ishandleFig
    this.coupe.fig = [];
end

if isempty(this.coupe.fig)
    if ishandleH
        delete(this.coupe.handles)
        this.coupe.handles = [];
        this.coupe.xyDeb = [];
        this.coupe.xyFin = [];
    end
end
