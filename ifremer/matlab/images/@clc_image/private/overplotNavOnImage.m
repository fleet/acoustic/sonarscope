function this = overplotNavOnImage(this, X, Y)

FigUtils.createSScFigure(this.hfig);
ColorOrder = get(gca, 'ColorOrder');

for k=1:length(this.coupe.handles)
    if ishandle(this.coupe.handles(k))
        delete(this.coupe.handles(k))
    end
end

hold on;
this.coupe.X = X;
this.coupe.Y = Y;

for k=1:length(X)
    iColor = 1 + mod(k-1, size(ColorOrder, 1));
    this.coupe.handles(k) = plot(X{k}, Y{k}, 'Color', ColorOrder(iColor,:));
    set(this.coupe.handles(k), 'LineWidth', 3)
    set(this.coupe.handles(k), 'Tag', 'ProfilOnTop')
end