% TODO : sortir cette fonction de clc_image : Pb : get_subx_suby

function [flag, this, nomFic, nbLigMax, nbColMax, valMask0, flagFillNaN] = paramsExportGoogleEarth(this)

nomFic      = [];
nbLigMax    = [];
nbColMax    = [];
valMask0    = 0;
flagFillNaN = [];

flag = testSignature(this.Images(this.indImage), 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

ImageName = code_ImageName(this.Images(this.indImage));
filtreKmz = fullfile(this.repExport, [ImageName '.kmz']);
[flag, nomFic] = my_uiputfile({'*.kmz'; '*.kml'}, 'Give a file name', filtreKmz);
if ~flag
    return
end
this.repExport = fileparts(nomFic);

[subc, subl] = get_subx_suby(this);
if isempty(subc)
    flag = 0;
    return
end

nbLigMax = min(256, length(subl));
nbColMax = min(256, length(subc));
if get_LevelQuestion >= 3
    str1 = sprintf('L''image est d�compos�e en pyramiode tuil�es.\nVous pouvez sp�cifier la hauteur et la largeur par vous-m�me.');
    str2 = sprintf('The image is written in tiles.\nYou can specify the tile''s height and width.');
    p    = ClParametre('Name', 'Height', ...
        'Unit', 'pixel', 'Value', nbLigMax, 'MinValue', 1, 'MaxValue', length(subl), 'Format', '%d');
    p(2) = ClParametre('Name', 'Width', ...
        'Unit', 'pixel', 'Value', nbColMax, 'MinValue', 1, 'MaxValue', length(subc), 'Format', '%d');
    a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;

%     a = set(a, 'color', get_ColorLevel(3));

    nbLigMax = val(1);
    nbColMax = val(2);
end


DataType = this.Images(this.indImage).DataType;
if DataType == cl_image.indDataType('Mask')
    [valMask0, flag] = my_questdlg(Lang('TODO', 'This image is a mask, do you consider value "0" as a "NaN" ?'));
    if ~flag
        return
    end
end

switch DataType
    case cl_image.indDataType('Segmentation')
        flagFillNaN = 0; % Mis � 0 car pas d'exemple me montrant qu'il faudrait faire le contraire
    case cl_image.indDataType('Mask')
        flagFillNaN = 0; % Mis � 0 car pas d'exemple me montrant qu'il faudrait faire le contraire
    otherwise
        if get_LevelQuestion == 3
            str1 = 'Voulez-vous combler les absences de valeurs sur les niveaux interm�diaires ?';
            str2 = 'Do you want to fill NaN on intermediate Values ?';
            [rep, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel',3 );
            if ~flag
                return
            end
            flagFillNaN = (rep == 1);
        else
            flagFillNaN = true;
        end
end
