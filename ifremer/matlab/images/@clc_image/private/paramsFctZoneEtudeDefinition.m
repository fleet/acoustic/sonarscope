function [Name, XLim, YLim, CLim, Commentaire, flag] = paramsFctZoneEtudeDefinition(this, varargin)

[varargin, flagCrop] = getPropertyValue(varargin, 'Crop', 0); %#ok<ASGLU>

Name = [];
XLim = [];
YLim = [];
Commentaire = [];

Unit  = this.Images(this.indImage).Unit;
XUnit = this.Images(this.indImage).XUnit;
YUnit = this.Images(this.indImage).YUnit;
CLim  = this.Images(this.indImage).CLim;

prompt   = {Lang('Nom de la zone d''�tude', 'Study zone name'),Lang('Commentaire', 'Comment')};
dlgTitle = Lang('D�finition d''une zone d''�tude', 'Define a study zone');

switch flagCrop
    case 0
        str = sprintf('Study Area %d', length(this.ZoneEtude));
    case 1
        str = sprintf('Crop %d', length(this.ZoneEtude));
    case 2
        str = sprintf('Crop Multi Smallest %d', length(this.ZoneEtude));
    case 3
        str = sprintf('Crop Multi Largest %d', length(this.ZoneEtude));
end

cmd = inputdlg(prompt, dlgTitle, [1;10], {str; ''});
if isempty(cmd)
    flag = 0;
    return
end

switch flagCrop
    case 0
        XLimYLim = this.visuLim(this.indImage, :);
        
    case 1
        [XLim, YLim] = getXLimYLimForCrop_image(this.Images(this.indImage), 'XLimYLim', 1);
        XLimYLim = [XLim YLim];
        
    case 2
        [flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage);
        if ~flag
            return
        end
        if isempty(indLayers)
            indLayers = this.indImage;
        else
            [flag, choix] = uiSelectImages('ExtensionImages', nomsLayers);
            if ~flag
                return
            end
            %         indLayers = indLayers(choix);
            indLayers = [this.indImage indLayers(choix)];
            if isempty(indLayers)
                return
            end
        end
        
        XLim = [-Inf Inf];
        YLim = [-Inf Inf];
        N = length(indLayers);
        str1 = 'Recherche des limites en cours';
        str2 = 'Searching limits';
        hw = create_waitbar(Lang(str1,str2), 'N', N);
        for k=1:N
            my_waitbar(k, N, hw)
            [XL, YL] = getXLimYLimForCrop_image(this.Images(indLayers(k)));
            XLim(1) = max(XLim(1), min(XL));
            XLim(2) = min(XLim(2), max(XL));
            YLim(1) = max(YLim(1), min(YL));
            YLim(2) = min(YLim(2), max(YL));
        end
        if (XLim(1) > XLim(2)) || (YLim(1) > YLim(2))
            str1 = 'Pas de zone commune entre toutes ces images.';
            str2 = 'No common area between all these images.';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            my_close(hw, 'MsgEnd');
            return
        end
        XLimYLim = [XLim YLim];
        my_close(hw, 'MsgEnd');
        
    case 3
        [flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage);
        if ~flag
            return
        end
        if isempty(indLayers)
            indLayers = this.indImage;
            CheckData = 0;
        else
            [flag, choix] = uiSelectImages('ExtensionImages', nomsLayers);
            if ~flag
                return
            end
            indLayers = [this.indImage indLayers(choix)];
            if isempty(indLayers)
                return
            end
            
            str1 = 'Voulez-vous �carter les zones d''images qui sont � NaN ?';
            str2 = 'Do you want to exclude the parts of images which are set to NaN ?';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            CheckData = (rep == 1);
        end
        
        XLim = [Inf -Inf];
        YLim = [Inf -Inf];
        N = length(indLayers);
        str1 = 'Recherche des limites en cours';
        str2 = 'Searching limits';
        hw = create_waitbar(Lang(str1,str2), 'N', N);
        for k=1:N
            my_waitbar(k, N, hw)
            [XL, YL] = getXLimYLimForCrop_image(this.Images(indLayers(k)), 'CheckData', CheckData);
            XLim(1) = min(XLim(1), min(XL));
            XLim(2) = max(XLim(2), max(XL));
            YLim(1) = min(YLim(1), min(YL));
            YLim(2) = max(YLim(2), max(YL));
        end
        XLimYLim = [XLim YLim];
        my_close(hw, 'MsgEnd');
end

Name        = cmd{1};
Commentaire = cmd{2};

p    = ClParametre('Name', Lang('D�but en X', 'X Begin'), ...
    'Unit', XUnit, 'Value', XLimYLim(1));
p(2) = ClParametre('Name', Lang('Fin en X', 'X End'), ...
    'Unit', XUnit, 'Value', XLimYLim(2));
p(3) = ClParametre('Name', Lang('D�but en Y', 'Y Begin'), ...
    'Unit', YUnit, 'Value', XLimYLim(3));
p(4) = ClParametre('Name', Lang('Fin en Y', 'Y End'), ...
    'Unit', YUnit, 'Value', XLimYLim(4));
p(5) = ClParametre('Name', Lang('Clim deb', 'Clim begin'), ...
    'Unit', Unit, 'Value', CLim(1));
p(6) = ClParametre('Name', Lang('CLim fin', 'Clim end'), ...
    'Unit', Unit, 'Value', CLim(2));
a = StyledSimpleParametreDialog('params', p, 'Title', ['You can modify the study zone "', Name, '"']);
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

XLim = val(1:2);
YLim = val(3:4);
