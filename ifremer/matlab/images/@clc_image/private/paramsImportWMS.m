% TODO : sortir cette fonction de clc_image : Pb : getSubxSubyContextuel(this
% TODO : il y a certainement bien mieux aujourd'hui dans la Tbx Carto

function [flag, wms, lon_range, lat_range, nx, ny] = paramsImportWMS(this)

GeometryType = this.Images(this.indImage).GeometryType;
if GeometryType == cl_image.indGeometryType('LatLong')
    [flag, subx, suby, nomZoneEtude] = getSubxSubyContextuel(this, 'ImportationWMS'); %#ok<ASGLU>
    if ~flag
        return
    end
    x = get(this.Images(this.indImage), 'x');
    y = get(this.Images(this.indImage), 'y');
    LonRange = [min(x(subx)) max(x(subx))];
    if diff(LonRange) > 360
        LonRange(2) = LonRange(1) + 360;
    end
    LatRange = [min(y(suby)) max(y(suby))];
else
    LatRange = [];
    LonRange = [];
end

wms.server = [];
wms.layer  = [];
wms.styles = [];
wms.format = [];
wms.srs    = [];
lon_range  = [];
lat_range  = [];
nx         = [];
ny         = [];

str1 = 'Choix d''un exemple';
str2 = 'Select an example';
str = {};
str{end+1} = 'STRM images (Topography from US Shuttle)';
str{end+1} = 'Earth mask & Boundaries';
str{end+1} = 'Earth, Earth mask & Boundaries';
str{end+1} = 'Wildfires in Africa';
str{end+1} = 'Batymetry Demis server';
% str{end+1} = 'http://www.ifremer.fr/services/wms1';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end

% WMS styles : 'short_int' | 'composite'
nomVar = {'WMS server'
    'WMS layer'
    'WMS styles'
    'WMS format'
    'WMS SRS'
    'Longitude'
    'Latitude'
    'nx'
    'ny'
    'transparent'};
switch rep
    case 1
        value = {'http://onearth.jpl.nasa.gov/wms.cgi?'
            'srtmplus'
            'short_int'
            'image/geotiff'
            'EPSG:4326'
            '[10 20]'
            '[40 46]'
            '1200'
            '720'
            'FALSE'};
        % http://onearth.jpl.nasa.gov/wms.cgi?request=GetMap&styles=short_int&layers=srtmplus&bbox=10,40,20,46&SRS=EPSG:4326&TRANSPARENT=FALSE&width=1200&height=720&format=image/geotiff&version=1.1.1
    case 2
        value = {'http://www2.demis.nl/wms/wms.ashx?Service=WMS&WMS=WorldMap&Version=1.1.0'
            'Countries,Borders,Coastlines'
            ''
            'image/gif'
            'EPSG:4326'
            '[-20 60]'
            '[-40 40]'
            '400'
            '400'
            ''};
        % http://www2.demis.nl/wms/wms.ashx?Service=WMS&WMS=WorldMap&Version=1.1.0&Request=GetMap&BBox=-20,-40,60,40&SRS=EPSG:4326&Width=400&Height=400&Layers=Countries,Borders,Coastlines&Format=image/gif
        
    case 3
        value = {'http://www2.demis.nl/wms/wms.ashx?Service=WMS&WMS=BlueMarble&Version=1.1.0'
            'Earth%20Image,Borders,Coastlines'
            ''
            'image/gif'
            'EPSG:4326'
            '[-20 60]'
            '[-40 40]'
            '400'
            '400'
            ''};
                
        % http://www2.demis.nl/wms/wms.ashx?Service=WMS&WMS=BlueMarble&Vers
        % ion=1.1.0&Request=GetMap&BBox=-20,-40,60,40&SRS=EPSG:4326&Width=4
        % 00&Height=400&Layers=Earth
        % Image,Borders,Coastlines&Format=image/gif

    case 4
        value = {'http://svs.gsfc.nasa.gov/cgi-bin/wms?SERVICE=WMS&VERSION=1.3.0'
            '2890_17402'
            'composite'
            'image/png'
            'CRS:84'
            '[-22 58]'
            '[-39 41]'
            '1024'
            '1024'
            ''};

% http://svs.gsfc.nasa.gov/cgi-bin/wms?SERVICE=WMS
% &VERSION=1.3.0&REQUEST=GetMap&LAYERS=2890_17402&FORMAT=image/png
% &WIDTH=1024&HEIGHT=1024&CRS=CRS:84&BBOX=-22,-39,58,41
% &STYLES=composite

    case 5
        value = {'http://www2.demis.nl/mapserver/request.asp?&Version=1.1.0'
            'Bathymetry'
            ''
            'image/png'
            'EPSG:4326'
            '[-22 58]'
            '[-39 41]'
            '1024'
            '1024'
            ''};

%     case 6
%         value = {'http://www2.demis.nl/mapserver/request.asp?&Version=1.1.0'
%             'Topography'
%             ''
%             'image/png'
%             'EPSG:4326'
%             '[-22 58]'
%             '[-39 41]'
%             '1024'
%             '1024'
%             ''};
        
%     case 7
%         value = {'http://www.ifremer.fr/services/wms1?SERVICE=WMS&VERSION=1.1.1'
%             'quadrige'
%             ''
%             'image/png'
%             'EPSG:4326'
%             '[-6 10]'
%             '[41 52]'
%             '800'
%             '600'
%             ''};
%         
%         %http://www.ifremer.fr/services/wms1?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&LAYERS=quadrige&SRS=EPSG:4326&BBOX=-6,41,10,52&WIDTH=800&HEIGHT=600&FORMAT=image/png     
%         
end


if ~isempty(LonRange)
    value{6} = num2strCode(LonRange);
end
if ~isempty(LatRange)
    value{7} = num2strCode(LatRange);
end

[rep, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
 
wms.server = rep{1};
wms.layer  = rep{2};
wms.styles = rep{3};
wms.format = rep{4};
wms.srs    = rep{5};
lon_range  = str2num(rep{6}); %#ok
lat_range  = str2num(rep{7}); %#ok
nx         = str2num(rep{8}); %#ok
ny         = str2num(rep{9}); %#ok
wms.transparent = rep{10};
