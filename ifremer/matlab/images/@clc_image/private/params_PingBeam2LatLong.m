function [flag, XLim, YLim] = params_PingBeam2LatLong(this)

if get_LevelQuestion >= 3
    if (this.Images(this.indImage).GeometryType == cl_image.indGeometryType('LatLong'))
        str1 = 'Limites de la mosa�que';
        str2 = 'Limits of the mosaic';
        str{1} = 'Full';
        str{2} = 'Current frame';
        [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        
        if rep == 1
            XLim = [];
            YLim = [];
        else
            XLim = this.visuLim(this.indImage, 1:2);
            YLim = this.visuLim(this.indImage, 3:4);
        end
    else
        XLim = [];
        YLim = [];
    end
else
    XLim = [];
    YLim = [];
end
flag = 1;
