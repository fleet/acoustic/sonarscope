function [flag, listLayers, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, NbSamplesMax, IdentPartData, ...
    TailleMax, Marge, Video, typeOutputFormat, repExport] = params_SEGY_Export3DViewer(this, listFileNames, repExport)

persistent persistent_CLimSubbottom persistent_CLimWC persistent_CLimBoth persistent_ColormapIndex
persistent persistent_NbSamples1 persistent_NbSamples2 persistent_NbSamples3 persistent_Marge persistent_Video

nomDirOut        = [];
OrigineCLim      = [];
CLim             = [];
Tag              = [];
ColormapIndex    = [];
NbSamplesMax     = 0;
IdentPartData    = [];
TailleMax        = Inf;
Marge            = 0;
Video            = 2;
typeOutputFormat = [];

S0 = cl_segy([]);

%% S�lection du layer (donn�es brutes / donn�es trait�es)

[flag, listLayers] = selectionLayers(S0, listFileNames{1});
if ~flag
    return
end

%% Type de donn�es � traiter

str1 = 'Choix de la partie � exporter.';
str2 = 'Select the part of the data you want to export.';
str{1} = Lang('TODO', 'Sub-bottom');
str{2} = Lang('Colonne d''eau', 'Water Column');
str{3} = Lang('Les deux', 'Both');
[IdentPartData, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% Origine du rehaussement de contraste

str1 = 'Origine du rehaussement de contraste :';
str2 = 'Contrast enhancement :';
[OrigineCLim, flag] = my_questdlg(Lang(str1,str2), ...
    Lang('Unique pour tous les fichiers', 'Unique for all files'), ...
    Lang('G�n�r�s pendant le Pr�processing des Signaux', 'Got from Preprocessing Signals'));
if ~flag
    return
end

%% Bornes de rehaussement de contraste

if OrigineCLim == 1
    switch IdentPartData
        case 1
            if isempty(persistent_CLimSubbottom)
                minVal = -1;
                maxVal = 1;
            else
                minVal = persistent_CLimSubbottom(1);
                maxVal = persistent_CLimSubbottom(2);
            end
            Unit = 'Amp';
        case 2
            if isempty(persistent_CLimWC)
                minVal = -1;
                maxVal = 1;
            else
                minVal = persistent_CLimWC(1);
                maxVal = persistent_CLimWC(2);
            end
            Unit = 'Amp';
        case 3
            if isempty(persistent_CLimBoth)
                minVal = -1;
                maxVal = 1;
            else
                minVal = persistent_CLimBoth(1);
                maxVal = persistent_CLimBoth(2);
            end
            Unit = 'Amp';
    end
    
    
    if isempty(persistent_NbSamples1)
        NbSamplesMax = 0;
    else
        NbSamplesMax = persistent_NbSamples1;
    end
    
    [flag, CLim] = saisie_CLim(minVal, maxVal, Unit, 'SignalName', 'SEGY data');
    if ~flag
        return
    end
    
    switch IdentPartData
        case 1
            persistent_CLimSubbottom = CLim;
        case 2
            persistent_CLimWC     = CLim;
        case 3
            persistent_CLimBoth   = CLim;
    end
end

switch IdentPartData
    case 1
        str9  = 'P�n�tration max';
        str10 = 'Max length of penetration';
        if isempty(persistent_NbSamples1)
            NbSamplesMax = 0;
        else
            NbSamplesMax = persistent_NbSamples1;
        end
    case 2
        str9  = 'Suppression des premiers �chos sur';
        str10 = 'Suppress first echos on';
        if isempty(persistent_NbSamples2)
            NbSamplesMax = 0;
        else
            NbSamplesMax = persistent_NbSamples2;
        end
    case 3
        str9  = 'Suppression des premiers �chos sur';
        str10 = 'Suppress first echos on';
        if isempty(persistent_NbSamples3)
            NbSamplesMax = 0;
        else
            NbSamplesMax = persistent_NbSamples3;
        end
end

str11 = 'Marge � laisser';
str12 = 'Margin to let';
if isempty(persistent_Marge)
    Marge = 0;
else
    Marge = persistent_Marge;
end

str1 = 'Limitations verticales';
str2 = 'Vertical limitations';
p    = ClParametre('Name', Lang(str9,str10), ...
    'Unit', 'samples', 'Value', NbSamplesMax, 'MinValue', 0);
p(2) = ClParametre('Name', Lang(str11,str12), ...
    'Unit', 'samples', 'Value', Marge, 'MinValue', 0);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Value = a.getParamsValue;

NbSamplesMax = Value(1);
Marge        = Value(2);

switch IdentPartData
    case 1
        persistent_NbSamples1 = NbSamplesMax;
    case 2
        persistent_NbSamples2 = NbSamplesMax;
    case 3
        persistent_NbSamples3 = NbSamplesMax;
end
persistent_Marge = Marge;

%% S�lection de la table de couleurs

if isempty(persistent_ColormapIndex)
    ColormapIndex = 2;
else
    ColormapIndex = persistent_ColormapIndex;
end
[flag, ColormapIndex] = edit_ColormapIndex(this, 'ColormapIndex', ColormapIndex);
if ~flag
    return
end
persistent_ColormapIndex = ColormapIndex;

%% Taille max des images support�e par GLOBE 
% D�pend des caract�ristiques de la carte graphique de la machine

pppp = opengl('data');

[flag, TailleMax] = inputOneParametre('Width/Height max size pixels (Cf. Graphic Card)', 'Value', ...
    'Value', pppp.MaxTextureSize, 'Unit', 'pixel', 'MinValue', 1, 'MaxValue', pppp.MaxTextureSize);
if ~flag
    return
end

%% Video

if isempty(persistent_Video)
    Video = 1;
else
    Video = persistent_Video;
end
[flag, Video] = edit_Video(this, 'Video', Video);
if ~flag
    return
end
persistent_Video = Video;

%% Type de fichier en sortie

str = {'XML-Bin'; 'Netcdf'};
[typeOutputFormat, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 1);
if ~flag
    return
end

