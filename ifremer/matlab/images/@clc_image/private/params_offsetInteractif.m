function [flag, this, XCroix, YCroix] = params_offsetInteractif(this)

XCroix = [];
YCroix = [];

this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
this.ihm.CoupeVert.cpn = set_etat(this.ihm.CoupeVert.cpn, 1);
this = callback_profileHorz(this);
this = callback_profileVert(this, 'NoSelectionSignaVert', 1, 'DirectWay');

str1 = 'Pour r�aliser cette fonction il vous faut avoir s�lectionn� au pr�alable un point particulier de l''image de r�f�rence, ensuite, avoir s�lectionn� l''image qui doit �tre recal�e et seulement lancer cette op�ration o� vous pourrez s�lectionner le point correspondant � la souris. Etes-vous pr�t � faire l''op�ration ?';
str2 = 'To realize this function, you must have clicked on a particular point on the reference image then hace selected the image that you want to move and only now you can lauch this procedure where you will have to click on the corresponding point. Are you ready to proceed ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag || (rep == 2)
    flag = 0;
    return
end

%% R�cup�ration des coordonn�es du point courant.

XCroix = get(this.hCroixProfilX(1), 'XData');
YCroix = get(this.hCroixProfilY(2), 'YData');
