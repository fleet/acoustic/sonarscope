function [flag, this, X, Y] = params_overplotNavOnImage(this, varargin)

[varargin, MoreExtensions] = getPropertyValue(varargin, 'MoreExtensions', []); %#ok<ASGLU>

nomFicNav = fullfile(my_tempdir, 'ExampleNav.txt');
createExampleFileNav(nomFicNav)
type(nomFicNav)

str1 = sprintf('Un exemple de fichier de navigation vient d''�tre cr��, voir "%s"..', nomFicNav);
str2 = sprintf('One example of navigation file has just been created, see "%s".', nomFicNav);
my_warndlg(Lang(str1,str2), 1);


X = {};
Y = {};

Ext{1} = '.txt';
Ext{end+1} = '.xyz';
if ~isempty(MoreExtensions) && ischar(MoreExtensions)
    MoreExtensions = {MoreExtensions};
end
for k=1:length(MoreExtensions)
    Ext{end+1} = MoreExtensions{k}; %#ok<AGROW>
end

[flag, listFileNames, lastDir] = survey_uiSelectFiles(this, 'ExtensionFiles', Ext, 'AllFilters', 1, 'RepDefaut', this.repImport);
if ~flag
    return
end
this.repImport = lastDir;
nbFics = length(listFileNames);
I0 = cl_image_I0;
X = cell(nbFics,1);
Y = cell(nbFics,1);
for k=1:nbFics
    [~, ~, ext] = fileparts(listFileNames{k});
    switch ext
        case '.nvi'
            isCaraibes = is_CDF(listFileNames{k});
            if isCaraibes
                plot_nvi(I0, listFileNames);
                [flag, Lat, Lon] = lecFicNav(listFileNames{k});
                if ~flag
                    return
                end
                X{k} = Lon;
                Y{k} = Lat;
            end
        case '.txt'
            % TODO : faire une version inspir�e de
            %             [flag, this, Carto, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, ...
            %     IndexColumns, DataType] = import_xyz(this, nomFic, varargin)%#ok
            
            
            X{k} = NaN;
            Y{k} = NaN;
            fid = fopen(listFileNames{k}, 'r');
            while 1
                tline = fgetl(fid);
                if ~ischar(tline)
                    break
                end
                mots = strsplit(tline);
                switch length(mots)
                    case 2
                        X{k}(end+1) = str2double(mots{1});
                        Y{k}(end+1) = str2double(mots{2});
                    case 4
                        if strcmpi(mots{1}(1), 'N') || strcmpi(mots{1}(1), 'S')
                            Y{k}(end+1) = str2lat([mots{1} ' ' mots{2}]);
                            X{k}(end+1) = str2lon([mots{3} ' ' mots{4}]);
                        else
                            X{k}(end+1) = str2lon([mots{1} ' ' mots{2}]);
                            Y{k}(end+1) = str2lat([mots{3} ' ' mots{4}]);
                        end
                    case 6
                        if strcmpi(mots{1}(1), 'N') || strcmpi(mots{1}(1), 'S')
                            Y{k}(end+1) = str2lat([mots{1} mots{2} ' ' mots{3}]);
                            X{k}(end+1) = str2lon([mots{4} mots{5} ' ' mots{6}]);
                        else
                            X{k}(end+1) = str2lon([mots{1} mots{2} ' ' mots{3}]);
                            Y{k}(end+1) = str2lat([mots{4} mots{5} ' ' mots{6}]);
                        end
                end
            end
            fclose(fid);
            X{k} = X{k}(2:end);
            Y{k} = Y{k}(2:end);
        otherwise
    end
end

if ~isempty(listFileNames)
    this.repImport = fileparts(listFileNames{1});
end
