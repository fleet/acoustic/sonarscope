function [flag, this, listFileNames, listeFicMBG] = params_sonarImportBathyFromCaraibesMBG(this, Ext, varargin)

[varargin, ExtMBG] = getPropertyValue(varargin, 'ExtMBG', '.mbg'); %#ok<ASGLU> 

listeFicMBG = [];

[flag, listFileNames, this.repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', Ext, 'AllFilters', 1, 'RepDefaut', this.repImport);
if ~flag
    return
end

[flag, listeFicMBG, this.repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', ExtMBG, 'AllFilters', 1, 'RepDefaut', this.repImport);
if ~flag
    return
end

%% Message //Tbx

% N     = length(listFileNames);
% N_MBG = length(listeFicMBG);
% if (N_MBG > 1) || (N > 1)
%     flag = messageParallelTbx(1);
%     if ~flag
%         return
%     end
% end
