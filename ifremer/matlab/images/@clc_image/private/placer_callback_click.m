% Met en place la callback le click
%
% Syntax
%   this = placer_callback_click(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% -------------------------------------------------------------------------

function this = placer_callback_click(this, varargin)

[varargin, AskWidthsIfBrush] = getFlag(varargin, 'AskWidthsIfBrush');

if ~strcmp(this.TypeInteractionSouris, 'CleanData') && (this.hauteurProfilX ~= 8)
    this.hauteurProfilX = 8;
    this.largeurProfilY = 4;
    this = callback_profileHorz(this);
    this = callback_profileVert(this, varargin{:});
end

%% Determination du message de callback

if ~ishandle(this.hfig)
    return
end

% zoom off
switch this.TypeInteractionSouris
    case 'PointClick'
%         msgCallBack = built_cb(this, this.msgClick);
        msgCallBack = built_cb(this.composant, this.msgClick);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        
        this = show_profils(this);
        this = process_synchroX(this);
        this = process_synchroY(this);
        
    case 'ZoomInteractif'
        set(this.hfig, 'WindowButtonDownFcn', []);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        
    case 'Centrage'
%         msgCallBack = built_cb(this, this.msgClickCentre);
        msgCallBack = built_cb(this.composant, this.msgClickCentre);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        
    case 'Coupe'
%         msgCallBack = built_cb(this, this.msgClickCoupeDown);
        msgCallBack = built_cb(this.composant, this.msgClickCoupeDown);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        
    case 'CleanData'
        if AskWidthsIfBrush
            this = paramsBrushSizeAxes(this);
        end
        
        this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
        this = callback_profileHorz(this);
        this = callback_profileVert(this, 'NoSelectionSignaVert', 1, 'DirectWay');
        
%         msgCallBack = built_cb(this, this.msgClick);
        msgCallBack = built_cb(this.composant, this.msgClick);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        
%         msgCallBack = built_cb(this, this.msgCleanData);
        msgCallBack = built_cb(this.composant, this.msgCleanData);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        idle(this.hfig) % TODO : comprendre pouquoi le pointeur ne s'est pas remis � arrow avant
        
    case 'SetNaN'
%         msgCallBack = built_cb(this, this.msgClickSetNaN);
        msgCallBack = built_cb(this.composant, this.msgClickSetNaN);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        idle(this.hfig)
        
    case 'SetVal'
%         msgCallBack = built_cb(this, this.msgClickSetVal);
        msgCallBack = built_cb(this.composant, this.msgClickSetVal);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        idle(this.hfig)
        
    case 'MoveProfils'
        this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
        this = callback_profileHorz(this);
        this = callback_profileVert(this, 'NoSelectionSignaVert', 1, 'DirectWay');
        
%         msgCallBack = built_cb(this, this.msgMoveProfils);
        msgCallBack = built_cb(this.composant, this.msgMoveProfils);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack); % Interruptible
        
%         msgCallBack = built_cb(this, this.msgClick);
        msgCallBack = built_cb(this.composant, this.msgClick);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        set(this.hfig, 'WindowButtonUpFcn',   []);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        
    case 'DrawHeight'
%         msgCallBack = built_cb(this, this.msgClickHeightDown);
        msgCallBack = built_cb(this.composant, this.msgClickHeightDown);
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
%         msgCallBack = built_cb(this, this.msgKeyPress);
        msgCallBack = built_cb(this.composant, this.msgKeyPress);
        set(this.hfig, 'KeyPressFcn', msgCallBack);
        
    otherwise
        'beurk : placer_callback_click' %#ok<NOPRT>
end

% Comment� le 28/05/2014 car l'image ombr�e par "S" est affich�e mais n'existe pas !
% Je ne me souviens plus pourquoi on avait mis ces linkaxes mais je me
% souviens que c'�tait n�cessaire !!!
% linkaxes([this.hAxePrincipal, this.hAxeProfilX], 'x', false)
% linkaxes([this.hAxePrincipal, this.hAxeProfilY], 'y', false)

