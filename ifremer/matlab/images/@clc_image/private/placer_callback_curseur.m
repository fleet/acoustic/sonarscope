% Met en place la callback sur le deplacement du curseur
% 
% Syntax
%   this = placer_callback_curseur(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = placer_callback_curseur(this)

%% Détermination du message de callback

switch this.TypeInteractionSouris
    case 'MoveProfils'
%         msgCallBack = built_cb(this, this.msgMoveProfils, 'Interruptible', 'on');
        msgCallBack = built_cb(this.composant, this.msgMoveProfils, 'Interruptible', 'on');
    otherwise
%         msgCallBack = built_cb(this, this.msgMoveCurseur, 'Interruptible', 'on');
        msgCallBack = built_cb(this.composant, this.msgMoveCurseur, 'Interruptible', 'on');
end

%% Mise en place de la callback sur le déplacement curseur

if ishandle(this.hfig)
    set(this.hfig, 'WindowButtonMotionFcn', msgCallBack);
else
    disp('clc_image/placer_callback_curseur : Invalid figure handle');
end
