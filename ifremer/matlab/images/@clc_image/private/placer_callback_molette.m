function this = placer_callback_molette(this)

%% Détermination du message de callback

% Methode qui ne permet pas d'interpréter le sens de rotation de la molette
% msgCallBack = built_cb(this, this.msgMolette);
msgCallBack = built_cb(this.composant, this.msgMolette);
set(this.hfig, 'WindowScrollWheelFcn', msgCallBack);

%% Callback

% Méthode qui permet d'interpréter le sens de rotation de la molette MAIS
% on ne sait pas sauver l'instance clc_image dans cli_image
set(this.hfig, 'WindowScrollWheelFcn', @figScroll);
    function figScroll(src, evnt) %#ok<INUSL>
        if evnt.VerticalScrollCount > 0
            cli_image.callback('NextImage');
        elseif evnt.VerticalScrollCount < 0
            cli_image.callback('PreviousImage');
        end
    end
end
