% Mise en place du zoom
% 
% Syntax
%   this = placer_zoom(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = placer_zoom(this)

%% Locales

hAxes    = [this.hAxePrincipal, this.hAxeProfilX, this.hAxeProfilY];
asserv   = [3, 1, 2; 1, 3, 0; 2, 0, 3];

%% Cr�ation de l'instance de gestion du zoom

if isempty(this.ihm.zoom.cpn)
    this.ihm.zoom.cpn = cl_zoom( ...
        'hFig'   , this.hfig, ...
        'hAxes'  , hAxes, ...
        'asserv' , asserv, ...
        'cbObj'  , get(this, 'componentUserName'), ...
        'cbName' , get(this, 'componentUserCb'), ...
        'action' , 'In');
end
