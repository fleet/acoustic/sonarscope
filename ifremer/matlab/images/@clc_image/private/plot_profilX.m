% Fonctionnalite plot specifique au composant
%
% Syntax
%   a = plot_profilX(a, X, V, iyDisplay)
% 
% Input Arguments
%   a : instance de clc_image
%   X : vecteur (1Xm) des abscisses
%   V : matrice (1Xm) des valeurs a presenter
% 
% Output Arguments
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = plot_profilX(this, x, val, iyDisplay)

if isempty(x) || ~ishandle(this.hAxePrincipal)
    return
end

switch this.Images(this.indImage).SpectralStatus
    case 1
    case 2
        val = 20 * log10(abs(val));
    case 3
        % A faire
end

%% Stockage des valeurs a tracer

this.profilX.x   = x;
this.profilX.val = val;

XLimG = get(this.hAxePrincipal, 'XLim');

delete(findobj(this.hAxeProfilX, 'Type', 'line')) % Rajout� le 08/04/2008

this.hCourbeProfilX = plot(this.hAxeProfilX, double(x), double(val));
N = length(this.hCourbeProfilX);
for k=1:N
    set(this.hCourbeProfilX(k), 'UserData', iyDisplay(k));
end

hold(this.hAxeProfilX, 'on')
for k=1:N
    subNonNaN = ~isnan(val(k,:));
    valIsoles = NaN(1, size(val,2));
    if ~any(subNonNaN) % Bug Lucie le 12/01/2021
        subIsoles = (~imerode(subNonNaN, [1 1 1]) & subNonNaN);
        valIsoles(subIsoles) =  val(k,subIsoles);
    end
    this.hCourbeProfilX(k+N) = plot(this.hAxeProfilX, double(x), double(valIsoles), '.');
    set(this.hCourbeProfilX(k+N), 'color', get(this.hCourbeProfilX(k), 'Color'))
    set(this.hCourbeProfilX(k+N), 'UserData', iyDisplay(k));
end
hold(this.hAxeProfilX, 'off')


set(this.hAxeProfilX, 'XLim', XLimG);
set(this.hAxeProfilX, 'YAxisLocation', 'right')

subNonInf = ~isinf(val);
minval = min(val(subNonInf));
maxval = max(val(subNonInf));
if ~isempty(minval) && ~isnan(minval) && (minval ~= maxval)
    set(this.hAxeProfilX, 'YLim', [minval maxval]);
end

if this.Images(this.indImage).XDir == 1
    set(this.hAxeProfilX, 'XDir', 'normal');
else
    set(this.hAxeProfilX, 'XDir', 'reverse');
end

%% Si axe designe, traitement du trace et de l'affichage

this = positionner(this, 'MajColorbar', 0);

%% Ajustement de la dynamique des profils

dynamique_profils(this);

%% Visibilite� de axe

if get_etat(this.ihm.CoupeHorz.cpn)
    set(this.hAxeProfilX,    'Visible', 'on');
    set(this.hCourbeProfilX, 'Visible', 'on');
else
    set(this.hAxeProfilX,    'Visible', 'off');
    set(this.hCourbeProfilX, 'Visible', 'off');
end
