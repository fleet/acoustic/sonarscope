% Fonctionnalite plot specifique au composant
%
% Syntax
%   a = plot_profilY(a, X, V, ixDisplay)
%
% Input Arguments
%   a : instance de clc_image
%   X : vecteur (1Xm) des abscisses
%   V : matrice (1Xm) des valeurs a presenter
%
% Output Arguments
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = plot_profilY(this, x, val, ixDisplay, subl)

if ~ishandle(this.hAxePrincipal)
    return
end

switch this.Images(this.indImage).SpectralStatus
    case 1
    case 2
        val = 20 * log10(abs(val));
    case 3
        % A faire
end

%% Traitement du choix de l'axe et du dessin des infos

% currentAxe = get(this.hfig, 'CurrentAxes');

%% Marquage de la hauteur sur image sonar 'SonarD'

if ~isempty(this.hHeight)
    sub = ishandle(this.hHeight);
    delete(this.hHeight(sub));
    this.hHeight = preallocateGraphics(0,0);
end
% set(this.hfig, 'CurrentAxes', this.hAxePrincipal);

GeometryType = this.Images(this.indImage).GeometryType;
switch GeometryType
    case cl_image.indGeometryType('PingRange')
        Height  = get(this.Images(this.indImage), 'Height');
        yHeight = get(this.Images(this.indImage), 'y')';
        hold(this.hAxePrincipal, 'on');
        %     this.hHeight(1) = plot(this.hAxePrincipal,  Height, yHeight, 'b');
        %     this.hHeight(2) = plot(this.hAxePrincipal, -Height, yHeight, 'b');
        
        % Modifi� le 15/01/2010
        if size(Height,1) == 1
            Height = Height';
        end
        if ~isempty(Height)
            this.hHeight(1) = plot(this.hAxePrincipal, double(Height(:,1)), double(yHeight), 'b');
            this.hHeight(2) = plot(this.hAxePrincipal, double(-Height(:,max(1,size(Height, 2)))), double(yHeight), 'b');
        end
        set(this.hHeight, 'Tag', 'PlotHeightOnImage')
        hold(this.hAxePrincipal, 'off');
        
    case cl_image.indGeometryType('PingSamples')
        if (this.Images(this.indImage).SpectralStatus == 1)
            Height  = get(this.Images(this.indImage), 'Height');
            yHeight = get(this.Images(this.indImage), 'y')';
            hold(this.hAxePrincipal, 'on');
            %     if ~isempty(this.hHeight)
            if size(Height,1) == 1
                Height = Height';
            end
            try
                this.hHeight(1) = plot(this.hAxePrincipal,  double(Height(:,1)), double(yHeight), 'b');
                this.hHeight(2) = plot(this.hAxePrincipal, double(-Height(:,1)), double(yHeight), 'b'); % TODO : indice 2 quand les signaux seront clairs
            catch ME %#ok<NASGU>
                'BUG ici : plot_profilY, gestion de "Height"' %#ok<NOPRT>
            end
            %     end
            set(this.hHeight, 'Tag', 'PlotHeightOnImage')
            hold(this.hAxePrincipal, 'off');
        end
        
    case cl_image.indGeometryType('PingDepth')
        if (this.Images(this.indImage).SpectralStatus == 1)
            Height  = get(this.Images(this.indImage), 'Height');
            yHeight = get(this.Images(this.indImage), 'y')';
            hold(this.hAxePrincipal, 'on');
            %     if ~isempty(this.hHeight)
            if size(Height,1) == 1
                Height = Height';
            end
            try
                this.hHeight(1) = plot(this.hAxePrincipal, double( Height(:,1)), double(yHeight), 'b');
                this.hHeight(2) = plot(this.hAxePrincipal, double(-Height(:,1)), double(yHeight), 'b'); % TODO : indice 2 quand les signaux seront clairs
            catch ME %#ok<NASGU>
                'BUG ici : plot_profilY, gestion de "Height"' %#ok<NOPRT>
            end
            %     end
            set(this.hHeight, 'Tag', 'PlotHeightOnImage')
            hold(this.hAxePrincipal, 'off');
        end
end

%% Positionnement sur l'axe

% set(this.hfig, 'CurrentAxes', this.hAxeProfilY);

%% Stockage des valeurs � tracer

this.profilY.x = x;
if isempty(x)
    return
end

YLimG = get(this.hAxePrincipal, 'YLim');

[flag, X, Unit] = getValSignalIfAny(this.Images(this.indImage), this.profilY.Type, 'suby', subl, 'val', val);
if flag
    val = X;
else
    this.profilY.Type = 'Image profile';
end
    
if islogical(val)
    val = uint8(val);
end

this.profilY.val  = val;
this.profilY.Unit = Unit;

%% Positionnement sur l'axe

delete(findobj(this.hAxeProfilY, 'Type', 'line')) % Rajout� le 08/04/2008

this.hCourbeProfilY = plot(this.hAxeProfilY, double(val), double(x));
if strcmp(this.profilY.Type, 'Vertical profile') || strcmp(this.profilY.Type, 'Image profile')
    N = length(this.hCourbeProfilY);
    for k=1:length(ixDisplay)
        set(this.hCourbeProfilY(k), 'UserData', ixDisplay(k));
    end
    
    hold(this.hAxeProfilY, 'on')
    for k=1:N
        subNonNaN = ~isnan(val(:,k));
        valIsoles = NaN(size(val,1), 1);
        if ~any(subNonNaN) % Bug Lucie le 12/01/2021
            subIsoles = (~imerode(subNonNaN, [1; 1; 1]) & subNonNaN);
            valIsoles(subIsoles) = val(subIsoles,k);
        end
        this.hCourbeProfilY(k+N) = plot(this.hAxeProfilY, double(valIsoles), double(x), '.');
        set(this.hCourbeProfilY(k+N), 'color', get(this.hCourbeProfilY(k), 'Color'))
        set(this.hCourbeProfilY(k+N), 'UserData', ixDisplay(k));
    end
    hold(this.hAxeProfilY, 'off')
end

set(this.hAxeProfilY, 'YLim', YLimG);

minval = min(val(:));
maxval = max(val(:));
if ~isempty(minval) && ~isnan(minval) && (minval ~= maxval)
    set(this.hAxeProfilY, 'XLim', [minval maxval]);
end

if this.Images(this.indImage).YDir == 1
    set(this.hAxeProfilY, 'YDir', 'normal');
else
    set(this.hAxeProfilY, 'YDir', 'reverse');
end
set(this.hAxeProfilY, 'XDir', 'reverse');

%% Si axe design�, traitement du trac� et de l'affichage

this = positionner(this, 'MajColorbar', 0);

%% Ajustement de la dynamique des profils

dynamique_profils(this);

if my_verLessThanR2014b
    set(this.hAxeProfilY, 'FontSize', 8)
else
    set(this.hAxeProfilY, 'FontSize', 8)
    set(this.hAxeProfilY, 'XTickLabelRotation', 45)
end

%% Mise � jour du titre

Titre = this.profilY.Type;
if strcmp(Titre, 'Image profile') || strcmp(Titre, 'Vertical profile')
    set(get(this.hAxeProfilY, 'Title'), 'String', Titre, 'Interpreter', 'none');
else
    pppp = this.Images(this.indImage).Name;
    if isempty(pppp)
        set(get(this.hAxeProfilY, 'Title'), 'String', 'Image profile');
    else
        set(get(this.hAxeProfilY, 'Title'), 'String', Titre, 'Interpreter', 'none');
    end
end