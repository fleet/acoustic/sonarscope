% Determine la bounding box de l axe indique
%
% Syntax
%   bbox = position(a, axe)
%   position(a, axe, bbox)
% 
% Input Arguments
%   a        : instance de clc_image
%   axe      : axe de destination {'profilX', 'profilY', 'principal'}
%   position : position en PIXELS de l axe
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = position(this, axe, varargin)

isOk      = 0;
varargout = {};

%% Traitement du choix de l axe et du dessin des infos

currentAxe  = get(this.hfig, 'CurrentAxes');

switch axe
    case 'profilX'

        % ----------------------------
        % Mise a jour du flag de trace

        isOk = 1;

        % ------------------------
        % Positionnement sur l'axe

        set(this.hfig, 'CurrentAxes', this.hAxeProfilX);

    case 'profilY'

        % ----------------------------
        % Mise a jour du flag de trace

        isOk = 1;

        % ------------------------
        % Positionnement sur l'axe

        set(this.hfig, 'CurrentAxes', this.hAxeProfilY);

    case 'principal'

        % ----------------------------
        % Mise a jour du flag de trace

        isOk = 1;

        % ------------------------
        % Positionnement sur l'axe

        set(this.hfig, 'CurrentAxes', this.hAxePrincipal);

    otherwise
        disp('clc_image/position : Invalid arguments number');
end

%% Si axe designe, suppression de son contenu

if isOk
    currentUnit = get(gca, 'units');
    set(gca, 'units', 'pixel');
    if isempty(varargin)
        varargout{end+1} = get(gca, 'Position');
    else
        set(gca, 'Position', varargin{1});
    end
    set(gca, 'units', currentUnit);
end

%% Retrouve l'axe courant

if ishandle(currentAxe)
    set(this.hfig, 'CurrentAxes', currentAxe);
end
