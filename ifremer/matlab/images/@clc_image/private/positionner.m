% Position et visibilite des elements graphiques du composant
% 
% Syntax
%   this = positionner(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = positionner(this, varargin)

[varargin, MajColorbar] = getPropertyValue(varargin, 'MajColorbar', 1); %#ok<ASGLU>

visibiliteProfilX      = 'off';
visibiliteProfilY      = 'off';
visibiliteColorBar     = 'off';
visibiliteAxePrincipal = 'off';
visibiliteToolBar      = 'off';
ligProfilX             = 0;
colProfilY             = 0;
hauteurAxePrincipal    = this.nbTotLignes;
largeurAxePrincipal    = this.nbTotColonnes;
hauteurProfilX         = 0;
hauteurToolBar         = 0;
hauteurLabelX          = 0;
largeurProfilY         = 0;
largeurLabelY          = 0;
largeurColorBar        = 0;

%% Mise � jour de la frame selon le nombre total de lignes et de colonnes

this.globalFrame = set(this.globalFrame, ...
    'MaxLig', hauteurAxePrincipal, ...
    'MaxCol', largeurAxePrincipal);

%% D�termination des dimensions de l'axe principal

if this.visuCoupeHorz
	hauteurProfilX      = this.hauteurProfilX;
    hauteurAxePrincipal = hauteurAxePrincipal - hauteurProfilX;
end

if this.visuCoupeVert
	largeurProfilY      = this.largeurProfilY;
    largeurAxePrincipal = largeurAxePrincipal - largeurProfilY;
end

if this.visuColorbar || this.fFixedColorBar
    largeurColorBar     = this.largeurColorBar;
    largeurAxePrincipal = largeurAxePrincipal - largeurColorBar;
end

if this.fToolBar
    hauteurToolBar      = this.hauteurToolBar;
    hauteurAxePrincipal = hauteurAxePrincipal - hauteurToolBar;
end

if this.fLabelX
    hauteurLabelX       = this.margeLabelX;
    hauteurAxePrincipal = hauteurAxePrincipal - hauteurLabelX;
end

if this.fLabelY
    largeurLabelY       = this.margeLabelY;
    largeurAxePrincipal = largeurAxePrincipal -largeurLabelY;
end

%% D�termination de la position et de la visibilit� de l'axe de
% profil selon x
% Application

if this.visuCoupeHorz && (hauteurProfilX > 0)
    visibiliteProfilX = 'on';

    ligProfilX = 1:hauteurProfilX;
    colProfilX = (largeurProfilY+1):(this.nbTotColonnes-largeurColorBar-largeurLabelY);

    this.globalFrame = replacer(this.globalFrame, 'axe', 2, ligProfilX, colProfilX);
end

all = [this.hAxeProfilX; get(this.hAxeProfilX, 'children')];
set(all, 'Visible', visibiliteProfilX);

%% D�termination de la position et de la visibilit� de l'axe de
% profil selon y
% Application

if this.visuCoupeVert && (largeurProfilY > 0)
    visibiliteProfilY = 'on';

    ligProfilY = (hauteurProfilX+1):(this.nbTotLignes-hauteurLabelX-hauteurToolBar);
    colProfilY = 1:this.largeurProfilY;

    this.globalFrame = replacer(this.globalFrame, 'axe', 3, ligProfilY, colProfilY);
end

all = [this.hAxeProfilY; get(this.hAxeProfilY, 'children')];
set(all, 'Visible', visibiliteProfilY);

%% Determination de la position et de la visibilit� de l'axe de colorbar
% Application
% Rq : (-1) pour etre sur une seule colonne quand la largeur d'une colonne
%      vaut 1!

if (this.visuColorbar || this.fFixedColorBar) && (this.largeurColorBar > 0)
    if this.visuColorbar
        visibiliteColorBar = 'on';
    else
        visibiliteColorBar = 'off';
    end

    ligColorBar = (max(ligProfilX)+1):(this.nbTotLignes-hauteurLabelX-hauteurToolBar);
    colColorBar = (this.nbTotColonnes-(largeurColorBar-1)):this.nbTotColonnes;

    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 2, ligColorBar, colColorBar);

    if MajColorbar
        move(this.globalFrame, this.hColorBar, ligColorBar, colColorBar);
    end
end

this.ihm.colorbar.cpn = set_visible(this.ihm.colorbar.cpn, visibiliteColorBar);

%% D�termination de la position et de la visibilit� de l'axe principal

if (hauteurAxePrincipal > 0) && (largeurAxePrincipal > 0)
    visibiliteAxePrincipal = 'on';
    ligAxePrincipal        = (max(ligProfilX)+1):(this.nbTotLignes-hauteurLabelX   - hauteurToolBar);
    colAxePrincipal        = (max(colProfilY)+1):(this.nbTotLignes-largeurColorBar - largeurLabelY);

    this.globalFrame = replacer(this.globalFrame, 'axe', 1, ligAxePrincipal, colAxePrincipal);
end

all = [this.hAxePrincipal; get(this.hAxePrincipal, 'children')];
set(all, 'Visible', visibiliteAxePrincipal);

%% D�termination de la position et de la visibilit� de la toolbar

if (this.hauteurToolBar > 0) && (this.fToolBar == 1)
    visibiliteToolBar = 'on';
    ligToolBar        = (this.nbTotLignes-(hauteurToolBar-1)):this.nbTotLignes;
    colToolBar        = 1:this.nbTotColonnes;

    this.globalFrame = replacer(this.globalFrame, 'uicontrol', 1, ligToolBar, colToolBar);
    if MajColorbar
        move(this.globalFrame, this.hToolBar, ligToolBar, colToolBar);
    end
end

set(this.hToolBar, 'Visible', visibiliteToolBar);
this.ihm.toolbar = set_visible(this.ihm.toolbar, visibiliteToolBar);

%% Mise � jour effective des positions

this.globalFrame = update(this.globalFrame);
if (this.hauteurToolBar > 0) && (this.fToolBar == 1)
    this.ihm.toolbar = update(this.ihm.toolbar);
end
if (this.largeurColorBar > 0) && (this.visuColorbar == 1)
    this.ihm.colorbar.cpn = update(this.ihm.colorbar.cpn);
    if MajColorbar
        this.ihm.colorbar.cpn  = update_colormap(this.ihm.colorbar.cpn);
    end
end

%% Mise � jour de la pr�sentation des diff�rents axes

this = presenter_axes(this);
