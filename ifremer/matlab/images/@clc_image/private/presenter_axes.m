% Mise a jour de la presentation des differents axes
% 
% Syntax
%   this = presenter_axes(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = presenter_axes(this)

%% Mise en forme fixe pour l'axe principal

if ~this.fLabelX
    set(this.hAxePrincipal, 'XTickLabel', []);
else
    set(this.hAxePrincipal, 'XTickMode', 'auto');
end

if ~this.fLabelY
    set(this.hAxePrincipal, 'YTickLabel', [], 'YAxisLocation', 'right');
else
    set(this.hAxePrincipal, 'XTickMode', 'auto', 'YAxisLocation', 'right');
end

%% Mise en forme fixe pour les axes secondaires

set(this.hAxeProfilX, 'XTickLabel', []);
set(this.hAxeProfilX, 'Box', 'on');

set(this.hAxeProfilY, 'YTickLabel', []);
XTickLabel = get(this.hAxeProfilY, 'XTickLabel');
if iscell(XTickLabel)
    % TODO : GLU R2014B
    %XTickLabel(2:end-1, :) = ' ';
else
    XTickLabel(2:end-1, :) = ' ';
end
set(this.hAxeProfilY, 'XTickLabel', XTickLabel);
% set(this.hAxeProfilY, 'XAxisLocation', 'top')
set(this.hAxeProfilY, 'XDir', 'reverse')
set(this.hAxeProfilY, 'Box', 'on');

%% Mise en place des grilles

if this.visuGrid
    set(this.hAxePrincipal, 'XGrid', 'on', 'YGrid', 'on');
    set(this.hAxeProfilX,   'XGrid', 'on', 'YGrid', 'on');
    set(this.hAxeProfilY,   'XGrid', 'on', 'YGrid', 'on');
else
    set(this.hAxePrincipal, 'XGrid', 'off', 'YGrid', 'off');
    set(this.hAxeProfilX,   'XGrid', 'off', 'YGrid', 'off');
    set(this.hAxeProfilY,   'XGrid', 'off', 'YGrid', 'off');
end
