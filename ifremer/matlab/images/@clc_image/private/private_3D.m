% Fonctionnalite private_3D specifique au composant
%
% Syntax
%   a = private_3D(a, axe, V)
%   a = private_3D(a, axe, X, Y, V)
%
% Input Arguments
%   a   : instance de clc_image
%   axe : axe de destination {'profilX', 'profilY', 'principal'}
%   V   : matrice (mXn) des valeurs a presenter
%   X   : vecteur (1Xm) des abscisses
%   Y   : vecteur (1Xn) des ordonnées
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = private_3D(this)

%% Positionnement sur l'axe

set(this.hfig, 'CurrentAxes', this.hAxePrincipal);

%% Si axe designe, traitement du trace et de l'affichage

[flag, x, y, indlayerDrapage, Colormap] = paramsSurf(this);
if ~flag
    return
end

CLim = this.Images(this.indImage).CLim;
if CLim(1) == CLim(2)
    CLim = [CLim(1)-1 CLim(2)+1];
end

if this.Images(this.indImage).ImageType == 2 % RGB
    nbSlides = 3;
else
    nbSlides = 1;
end
if nbSlides == 1
    I = get_val_xy(this.Images(this.indImage), x, y);
    I(I < CLim(1)) = CLim(1);
    I(I > CLim(2)) = CLim(2);

    I = double(I);

    SpectralStatus = this.Images(this.indImage).SpectralStatus;
    if SpectralStatus == 2
        I =  20 * log10(abs(I));
    end


    
    if isempty(indlayerDrapage)
        map = Colormap;

        C = [];
        flagColorbar = 1;
    else
        if this.Images(indlayerDrapage).ImageType == 2 % RGB
            nbSlides_C = 3;
        else
            nbSlides_C = 1;
        end
        ImageType = this.Images(indlayerDrapage).ImageType;
        if ImageType == 2   % '2=RGB'
            RGB = get_VRAIE_val_xy(this.Images(indlayerDrapage), x, y);
%             [C, map] = RGB2Indexed(RGB, 128);
% %             figure; imshow(X, map)
%             C = double(X);

            C = RGB;
            map = [];

            flagColorbar = 0;
             
        elseif nbSlides_C == 1
            CLim = this.Images(indlayerDrapage).CLim;
            C    = get_val_xy(this.Images(indlayerDrapage), x, y);
            C(C < CLim(1)) = CLim(1);
            C(C > CLim(2)) = CLim(2);
            C = double(C);

            if this.Images(indlayerDrapage).Video == 2
                C = CLim(1) + CLim(2) - C;
            end

            SpectralStatus = this.Images(indlayerDrapage).SpectralStatus;
            if SpectralStatus == 2
                C =  20 * log10(abs(C));
            end
            
            map = Colormap;
            flagColorbar = 1;
        end
    end
    
    V = version;
    if str2num(V(1)) < 7 %#ok
        figure;
        if isempty(C)
            surf(x, y, I);
            shading interp;
            if   flagColorbar
                colorbar
            end
        else
            surf(x, y, I, C);
            shading interp;
            colormap(map);
            if   flagColorbar
                colorbar
            end
        end

    else
%     if str2num(V(1)) >= 7
        GeometryType = this.Images(this.indImage).GeometryType;
        switch GeometryType
            case 1
                my_surf(x, y, I, 'Texture', C, 'map', map, 'flagColorbar', flagColorbar, 'axisEqual')
            case 2
                my_surf(x, y, I, 'Texture', C, 'map', map, 'flagColorbar', flagColorbar, 'axisEqual')
            case 3
                DataType = this.Images(this.indImage).DataType;
                if DataType == cl_image.indDataType('Bathymetry')
                    [rep, flag] = my_questdlg(Lang('Type de représentation 3D', 'Kind of 3D representation ?'), 'Cube', 'Sphere');
                    if ~flag
                        return
                    end
                else
                    rep = 1;
                end
                if rep == 1
                    LonLim = [x(1) x(end)];
                    LatLim = [y(1) y(end)];
                    my_surf(LonLim,  LatLim, I, 'Texture', C, 'map', map, 'flagColorbar', flagColorbar, 'axisGeo')
                else
                    str1 = 'Exagération verticale';
                    str2 = 'Vertical Exageration';
                    [flag, ExaVert] = inputOneParametre(Lang(str1,str2), 'Value', 'Value', 100);
                    if ~flag
                        return
                    end
                    Earth3D(x, y, I, 'Texture', C, 'map', map, 'ExaVert', ExaVert)
                end
            case 7
                my_surf(linspace(min(y), max(y), length(x)), y, I, 'map', map, 'flagColorbar', flagColorbar)
            case 5
                my_surf(linspace(min(y), max(y), length(x)), y, I, 'Texture', C, 'map', map, 'flagColorbar', flagColorbar)
        end
    end
end


function [flag, x, y, indlayerDrapage, Colormap] = paramsSurf(this)

indlayerDrapage = [];

Colormap = this.Images(this.indImage).Colormap;

x    = get(this.Images(this.indImage), 'x');
y    = get(this.Images(this.indImage), 'y');
XLim = this.visuLim(this.indImage, 1:2);
YLim = this.visuLim(this.indImage, 3:4);
% subx = find((x >= XLim(1)) & (x <= XLim(2)));
% suby = find((y >= YLim(1)) & (y <= YLim(2)));
subx = (x >= XLim(1)) & (x <= XLim(2));
suby = (y >= YLim(1)) & (y <= YLim(2));
x = x(subx);
y = y(suby);

nx = length(x);
ny = length(y);
nbPixels = nx * ny;
nbPixelsMax = 512^2;
if nbPixels > nbPixelsMax
    coeff = sqrt(nbPixels/nbPixelsMax);
    nxMin = ceil(nbPixelsMax * coeff / ny);
    nyMin = ceil(nbPixelsMax * coeff / nx);
    
    str1 = 'TODO';
    str2 = 'Sampling parameters';
    p    = ClParametre('Name', Lang('Nb points en x', 'Number of X points'), ...
        'MinValue', nxMin, 'MaxValue', nx, 'Value', nxMin, 'Format', '%d'); % '%d'
    p(2) = ClParametre('Name', Lang('Nb points en y', 'Number of Y points'), ...
        'MinValue', nyMin, 'MaxValue', ny, 'Value', nyMin, 'Format', '%d'); % '%d'
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 -1 -2 -1 -1 0 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    val = a.getParamsValue;
    nx = floor(val(1));
    ny = floor(val(2));
end

x = linspace(x(1), x(end), nx);
y = linspace(y(1), y(end), ny);

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage);
if ~flag
    return
end
if ~isempty(indLayers)
    str1 = 'Drapage d''une autre image ?';
    str2 = 'TODO';
    nomsLayers(2:end+1) = nomsLayers;
    nomsLayers{1} = 'Aucune';
    [choix, flag] = my_listdlg(Lang(str1,str2), nomsLayers, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    if choix ~= 1
        indlayerDrapage = indLayers(choix - 1);
        Colormap= this.Images(indlayerDrapage).Colormap;
    end
end
