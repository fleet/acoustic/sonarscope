% Fonctionnalite private_contour specifique au composant
%
% Syntax
%   a = private_contour(a)
% 
% Input Arguments
%   a   : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = private_contour(this)

%% Traitement du choix de l'axe et du dessin des infos

currentAxe = get(this.hfig, 'CurrentAxes');

%% Positionnement sur l'axe

set(this.hfig, 'CurrentAxes', this.hAxePrincipal);

%% Si axe designe, traitement du trace et de l'affichage

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');
[I, subx, suby] = get_Image_inXLimYLim(this.Images(this.indImage), 'nbColMax', 500, 'nbLigMax', 500);
c = get(this.Images(this.indImage), 'ContourValues');

MinI = min(I(:));
MaxI = max(I(:));
Step = c(2) - c(1);
c = centrage_magnetique((MinI-Step):Step:(MaxI+Step));

N = (numel(I) * length(c));
if N > 4e7 % SizePhysTotalMemory
    str1 = 'Trop de donn�es �) traiter. Je vous recommande de r�essayer avec moins de niveaux. Voulez-vous continuer ? (Je ne vous le recommande pas)';
    str2 = 'Too much data to process, il would be long to process, I suggest you begin again with less levels. Do you want to continue anayway ? (I do not recommand it))';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (rep == 2)
        return
    end
    drawnow
elseif N > 1e7 % SizePhysTotalMemory
    str1 = 'La quantit� de donn�es � afficher pour le trac� de contours commence � �tre important. Je vous recommande d''augmenter l''�cart entre contours.';
    str2 = 'The quantity of data for the contour plot begins to be high. I suggest you upper the step between the countours.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'TooMuchDataForContour', 'TimeDelay', 10);
    drawnow
end

if isnan(c)
    c = 0;
end
contour(x(subx), y(suby), I, c); % TODO ne fonctionne plus en R2016a !!!

%% Restriction du trace aux limites transmises

axis(this.visuLim(this.indImage, :))

%% Orientation verticale de l'image

if this.Images(this.indImage).YDir == 1
    set(this.hAxePrincipal, 'YDir', 'normal');
    set(this.hAxeProfilY,   'YDir', 'normal');
else
    set(this.hAxePrincipal, 'YDir', 'reverse');
    set(this.hAxeProfilY,   'YDir', 'reverse');
end

%% Orientation horizontale de l'image

if this.Images(this.indImage).XDir == 1
    set(this.hAxePrincipal, 'XDir', 'normal');
    set(this.hAxeProfilX,   'XDir', 'normal');
else
    set(this.hAxePrincipal, 'XDir', 'reverse');
    set(this.hAxeProfilX,   'XDir', 'reverse');
end

%% Remise � jour de la colorbar

this = callback_colorbar(this);

%% Retrouve l'axe courant

if ishandle(currentAxe)
    set(this.hfig, 'CurrentAxes', currentAxe);
end
