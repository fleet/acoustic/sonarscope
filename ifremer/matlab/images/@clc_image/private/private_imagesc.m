% Fonctionnalite private_imagesc specifique au composant
%
% Syntax
%   a = private_imagesc(a, axe, V)
%   a = private_imagesc(a, axe, X, Y, V)
%
% Input Arguments
%   a   : instance de clc_image
%   axe : axe de destination {'profilX', 'profilY', 'principal'}
%   V   : matrice (mXn) des valeurs a presenter
%   X   : vecteur (1Xm) des abscisses
%   Y   : vecteur (1Xn) des ordonn�es
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = private_imagesc(this, varargin)

[varargin, MajColorbar] = getPropertyValue(varargin, 'MajColorbar', 1); %#ok<ASGLU>

flag = 0;

%% Traitement du choix de l'axe et du dessin des infos

currentAxe = get(this.hfig, 'CurrentAxes');

%% Positionnement sur l'axe

set(this.hfig, 'CurrentAxes', this.hAxePrincipal);

%% Si axe designe, traitement du trace et de l'affichage

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');
ScreenSize = get(0, 'ScreenSize');
[I, subx, suby] = get_Image_inXLimYLim(this.Images(this.indImage), 'nbColMax', ScreenSize(3), 'nbLigMax', ScreenSize(4));
CLim = this.Images(this.indImage).CLim;
if CLim(1) == CLim(2)
    CLim = [CLim(1)-1 CLim(2)+1];
end

switch this.Images(this.indImage).SpectralStatus
    case 1
        if ~isreal(I)
            str1 = 'Impossible d''afficher l''image car les pixels sont complexes.';
            str2 = 'Can''t display image because complex values.';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
    case 2
        I = 20 * log10(abs(I));
    case 3
        % A faire
end

h = findobj(this.hAxePrincipal, 'type', 'image');
for k=1:length(h)
    delete(h(k));
end

h = findobj(this.hAxePrincipal, 'Tag', 'PlotRegionOfInterest');
for k=1:length(h)
    delete(h(k));
end

figure(this.hfig);
ImageType = this.Images(this.indImage).ImageType;
ImageName = this.Images(this.indImage).Name;
switch ImageType
    case 1  % Image d'intensite
        if any(isnan(CLim)) || any(isinf(CLim))
            CLim = [-1 1];
        end
        imagesc(x(subx), y(suby), I, CLim);
    case 2  % Image RGB
        if (isa(I, 'double') || isa(I, 'single'))
            flagMax = false;
            for k=1:size(I,2)
                J = I(:,k,:);
                if max(J(:)) > 1
                    flagMax = true;
                    break
                end
            end
            if flagMax
                I(isnan(I)) = 0;
                I = uint8(I);
            end
        end
        image(x(subx), y(suby), I);
    case 3  % Image indexee
        image(x(subx), y(suby), uint8(I));
    case 4  % Image binaire
        imagesc(x(subx), y(suby), I, CLim);
end
title(ImageName, 'Interpreter', 'none');


%% Restriction du trac� aux limites transmises

axis(this.visuLim(this.indImage, :));
verif(this.visuLim(this.indImage, :), x, y)

%% Orientation verticale de l'image

if this.Images(this.indImage).YDir == 1
    set(this.hAxePrincipal, 'YDir', 'normal');
    set(this.hAxeProfilY,   'YDir', 'normal');
else
    set(this.hAxePrincipal, 'YDir', 'reverse');
    set(this.hAxeProfilY,   'YDir', 'reverse');
end

%% Orientation horizontale de l'image

if this.Images(this.indImage).XDir == 1
    set(this.hAxePrincipal, 'XDir', 'normal');
    set(this.hAxeProfilX,   'XDir', 'normal');
else
    set(this.hAxePrincipal, 'XDir', 'reverse');
    set(this.hAxeProfilX,   'XDir', 'reverse');
end

%% Remise � jour de la colorbar

this = callback_colorbar(this, 'MajColorbar', MajColorbar);

%% Retrouve l'axe courant

if ishandle(currentAxe)
    set(this.hfig, 'CurrentAxes', currentAxe);
end

flag = 1;


%% A LAISSER QUELQUES TEMPS ET A SUPPRIMER ENSUITE

if ~isdeployed
    if ~isa(x, 'double')
        my_warndlg('A signaler � JMA : this.x is not double clc_image/private/private_imagesc', 0, 'Tag', 'xNonDouble');
    end
    
    if ~isa(y, 'double')
        my_warndlg('A signaler � JMA : this.y is not double clc_image/private/private_imagesc', 0, 'Tag', 'xNonDouble');
    end
end


function verif(XYLim, x, y)

XLim_min = min(XYLim(1:2));
XLim_max = max(XYLim(1:2));
YLim_min = min(XYLim(3:4));
YLim_max = max(XYLim(3:4));

if (YLim_min >= max(y)) ||  (YLim_max <= min(y)) || (XLim_min >= max(x)) ||  (XLim_max <= min(x))
    str1 = 'L''image est en dehors de la fen�tre, utilisez le "Quick-Look" pour vous repositionner';
    str2 = 'The image is out of display, click "Full Extent" to fit it.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImageEnDehorsDeLaFenetre', 'TimeDelay', 30);
end
