% Fonctionnalite private_plot specifique au composant
%
% Syntax
%   a = private_plot(a, axe, V)
%   a = private_plot(a, axe, X, Y, V)
%
% Input Arguments
%   a   : instance de clc_image
%   axe : axe de destination {'profilX', 'profilY', 'principal'}
%   V   : matrice (mXn) des valeurs a presenter
%   X   : vecteur (1Xm) des abscisses
%   Y   : vecteur (1Xn) des ordonn�es
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = private_plot(this, Region)

str1 = 'S�paration des courbes � partir d''une autre image';
str2 = 'Do you want to gather the curves according to another data ?';
strSecondeQuestion = Lang(str1,str2);
[flag, Selection] = selectionLayersSynchronises(this.Images, this.indImage, 'Max', 2, 'SignauxVert', 'QuestionY', strSecondeQuestion);
if ~flag
    return
end

if isempty(Selection)
    return
end

x = get(this.Images(this.indImage), 'x');
y = get(this.Images(this.indImage), 'y');
switch Region
    case 'CurrentExtent'
        XLim = this.visuLim(this.indImage, 1:2);
        subx = ((x >= XLim(1)) & (x <= XLim(2)));
        
        YLim = this.visuLim(this.indImage, 3:4);
        suby = ((y >= YLim(1)) & (y <= YLim(2)));
    case 'VoisinsY'
        % TODO : l'id�e ici serait de mettre en place un m�canisme de event / listener pour redessiner ces graphiques en permanence quand on est en mode "Move pointer"
        if this.NbVoisins == 0
            str1 = 'Le nombre de voisins de part et d''autre du pixel est �gal � 0, modifiez ce nombre si vous voulez repr�senter plusieurs lignes.';
            str2 = 'The numbre of neighbors is equal to 0, modify its value if you want to plot several rows.';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlotUneSeuleLigne');
        end
        XLim = this.visuLim(this.indImage, 1:2);
        subx = ((x >= XLim(1)) & (x <= XLim(2)));
        
        [~, iy]   = xy2ij(this.Images(this.indImage), x, this.cross(this.indImage, 2));
        ideb = iy - this.NbVoisins;
        ideb = max(ideb, 1);
        ifin = iy + this.NbVoisins;
        ifin = min(ifin, double(this.Images(this.indImage).nbRows));
        suby = ideb:ifin;
    case 'VoisinsX'
        if this.NbVoisins == 0
            str1 = 'Le nombre de voisins de part et d''autre du pixel est �gal � 0, modifiez ce nombre si vous voulez repr�senter plusieurs colonnes.';
            str2 = 'The numbre of neighbors is equal to 0, modify its value if you want to plot several columns.';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlotUneSeuleLigne');
        end
        YLim = this.visuLim(this.indImage, 3:4);
        suby = ((y >= YLim(1)) & (y <= YLim(2)));
        
        ix = xy2ij(this.Images(this.indImage), this.cross(this.indImage, 1), y);
        ideb = ix - this.NbVoisins;
        ideb = double(ideb);    % Sinon max pas content
        ideb = max(ideb, 1);
        ifin = ix + this.NbVoisins;
        ifin = double(ifin);    % Sinon max pas content
        ifin = min(ifin, double(this.Images(this.indImage).nbColumns));
        subx = ideb:ifin;
end
x = x(subx);
y = y(suby);

%% On rep�re si le trac� est de la bathy en fonction de AcrossDistance. Dans
% ce cas on propose un trac� de cercle

if (length(Selection) >= 1) && strcmp(Selection(1).TypeVal, 'Layer') && ...
    (this.Images(this.indImage).DataType == cl_image.indDataType('Bathymetry')) && ...
        (this.Images(Selection(1).indLayer).DataType == cl_image.indDataType('AcrossDist'))
    flagCercles = 1;
    Immersion = mean(get(this.Images(this.indImage), 'Immersion'), 'omitnan');
else
    flagCercles = 0;
end

Marker = {'+'; '*'; 'o'; 'x'; 'none'};
[rep, flag] = my_listdlg('Marker', Marker, 'SelectionMode', 'Single');
if ~flag
    return
end
Marker = Marker{rep};

if rep == 5
    InitialValue = 2;
else
    InitialValue = 1;
end
LineStyle = {'none'; '-'; '--'; '.-'};
[rep, flag] = my_listdlg('LineStyle', LineStyle, 'InitialValue', InitialValue, 'SelectionMode', 'Single');
if ~flag
    return
end
LineStyle = LineStyle{rep};
    

bins = [];
NomLayer = [];
xTitre = '';
J = [];
X = [];

for k1=1:length(Selection)
    if strcmp(Selection(k1).TypeVal, 'Axe')
        if strcmp(Selection(k1).Name(1:5), 'Axe X')
            x = get(this.Images(this.indImage), 'x');
            if k1 == 1
                bins{end+1} = []; %#ok<AGROW>
%                 Bins = [];
            else
                [Bins, flag] = saisieBinsAxe(x(subx), 'X', this.Images(this.indImage).XUnit);
                if ~flag
                    return
                end
                bins{end+1} = Bins; %#ok
            end

            NomLayer{end+1} = 'X'; %#ok

            J{end+1} = repmat(x, length(y), 1); %#ok
            X{end+1} = linspace(x(1), x(end), length(x)); %#ok
            xTitre = 'X';

        elseif strcmp(Selection(k1).Name(1:5), 'Axe Y')
            if k1 == 1
                bins{end+1} = []; %#ok
%                 Bins = [];
            else
                [Bins, flag] = saisieBinsAxe(y, 'Y', this.Images(this.indImage).YUnit);
                if ~flag
                    return
                end
                bins{end+1} = Bins; %#ok
            end
            NomLayer{end+1} = 'Y'; %#ok

            J{end+1} = repmat(y', 1, length(x)); %#ok
            X{end+1} = linspace(y(1), y(end), length(y)); %#ok
            xTitre = 'Y';
        end

    elseif strcmp(Selection(k1).TypeVal, 'Layer')
        k2 = Selection(k1).indLayer;
        if k1 == 1
            bins{end+1} = []; %#ok
            Bins = [];
        else
            [Bins, ~, flag] = saisieBinsHisto(this.Images(k2));
            if ~flag
                return
            end
            bins{end+1} = Bins; %#ok
        end
        
        NomLayer{end+1} = Selection(k1).Name; %#ok
        choixLayers = Selection(k1).indLayer;

        J{end+1} = get_val_xy(this.Images(choixLayers), x, y); %#ok<AGROW> 
        X{end+1} = Bins; %#ok
        if k1 < 2
            xTitre = [xTitre ' - ' this.Images(choixLayers).Name]; %#ok<AGROW> 
        end

    elseif strcmp(Selection(k1).TypeVal, 'SignalVert')
        Val = get(this.Images(this.indImage), Selection(k1).Name);
        
        if k1 == 1
            bins{end+1} = []; %#ok
            Bins = [];
        else
            [Bins, flag] = saisieBinsAxe(Val(suby), Selection(k1).Name, 'Unit');
            if ~flag
                return
            end
            bins{end+1} = Bins; %#ok
        end
        
        NomLayer{end+1} = Selection(k1).Name; %#ok

        J{end+1} = repmat(Val(suby)', 1, length(x)); %#ok
        X{end+1} = Bins; %#ok
        xTitre = Selection(k1).Name;
    end
end

%% Traitement du choix de l'axe et du dessin des infos

currentAxe = get(this.hfig, 'CurrentAxes');

%% Positionnement sur l'axe

set(this.hfig, 'CurrentAxes', this.hAxePrincipal);


if this.Images(this.indImage).ImageType == 2 % RGB
    nbSlides = 3;
else
    nbSlides = 1;
end
if nbSlides == 1
    I = get_val_xy(this.Images(this.indImage), x, y);
    SpectralStatus = this.Images(this.indImage).SpectralStatus;
    if SpectralStatus == 2
        I =  20 * log10(abs(I));
    end

    FigUtils.createSScFigure; hold on; grid on;

    f = uimenu('Text', 'SonarScope');
    uimenu(f, 'Text', 'Axis Normal', 'Callback', @AxisNormal);
    uimenu(f, 'Text', 'Axis Equal', 'Callback', @AxisEqual);
    if flagCercles
        uimenu(f, 'Text', 'Draw circle', 'Callback', @DrawCircle);
    end

    if length(J) == 1
        Titre = sprintf('%s\naccording to\n%s', this.Images(this.indImage).Name, Selection(1).Name);
        title(Titre, 'interpreter', 'none')
        Z = repmat(y', 1, size(I,2));
        h = PlotUtils.createSScPlot3(J{1}, I, Z);
        set(h, 'LineStyle', LineStyle, 'Marker', Marker);

    else
        K = J{2};
        if ~isa(K, 'double')
            K = single(K);
        end
        Z = repmat(y', 1, size(I,2));

        milieuClasse = bins{2};
        
        if length(milieuClasse) == 1
            title(['Colors from : ' Selection(2).Name], 'interpreter', 'none')
            h = PlotUtils.createSScPlot(J{1}, I);
            set(h, 'LineStyle', LineStyle, 'Marker', Marker);

            yTitre = this.Images(this.indImage).Name;
            xlabel(xTitre, 'Interpreter', 'none');
            ylabel(yTitre, 'Interpreter', 'none')
        else
            Pas = milieuClasse(2) - milieuClasse(1);
            milieuClasse(end+1) =  milieuClasse(end) + Pas;
            milieuClasse = milieuClasse - Pas/2;
            if length(milieuClasse) == 1
                milieuClasse = [bins{2}(1)-0.5 bins{2}(1)+0.5 Inf];
            end

%             nbColors = length(milieuClasse)-1;
            nbColors = min(128, length(milieuClasse)-1);
            couleurs = jet(nbColors);

            XLimJ(1) = min(J{1}(:));
            YLimI(1) = min(I(:));
            YLimI(2) = max(I(:));
            xI = linspace(XLimJ(1), XLimJ(1)+100*nbColors*eps(XLimJ(1)), 2);
            yI = linspace(YLimI(1), YLimI(2), nbColors);

            imagesc(xI, yI, repmat(milieuClasse',1,2)); colormap(couleurs);
            colorbar
            title(['Colors from : ' Selection(2).Name], 'interpreter', 'none')

            for k2=1:nbColors
                sub = find(K < milieuClasse(k2+1));
                h = PlotUtils.createSScPlot3(J{1}(sub), I(sub), Z(sub));
                set(h, 'Color', couleurs(k2,:));
                set(h, 'LineStyle', LineStyle, 'Marker', Marker);
                K(sub) = NaN;
            end
            hold off;
            if ~isdeployed
                try
%                     figure; ribbon(J{1}', I'); view(72, -64)
%                     yAll = I';
%                     figure; strips(yAll(:), size(I,2)); axis ij
                catch
                end
            end
        end
    end
    yTitre = this.Images(this.indImage).Name;
    xlabel(Selection(1).Name, 'Interpreter', 'none');
    ylabel(yTitre, 'Interpreter', 'none')
end

%% Retrouve l'axe courant

if ishandle(currentAxe)
    set(this.hfig, 'CurrentAxes', currentAxe);
    set(0, 'CurrentFigure', this.hfig);
end




%% Callback de sauvegarde des lignes affich�es dans un fichier

    function AxisNormal(varargin)
        axis normal
    end
    function AxisEqual(varargin)
        axis equal
    end
    function DrawCircle(varargin)
        w = axis(findobj(get(gca,'Parent'), 'Type', 'axes', '-not', 'Tag', 'Colorbar'));
        M = max(abs(w));
        if isnan(Immersion)
            Immersion = 0;
        end
        
        str1 = 'Param�tre du cercle.';
        str2 = 'Circle parameter.';
        p = ClParametre('Name', Lang('Rayon', 'Radius'), ...
            'Unit', 'm',  'MinValue', 0, 'MaxValue', M, 'Value', (M/2)+Immersion);
        a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
        % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
        a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        Rayon = a.getParamsValue;
        
        teta = 0:0.1:360;
        x = cosd(teta) * Rayon;
        y = Immersion + sind(teta) * Rayon;
        hold on; PlotUtils.createSScPlot(x,y,'k'); hold off;
        axis(w)
    end
end
