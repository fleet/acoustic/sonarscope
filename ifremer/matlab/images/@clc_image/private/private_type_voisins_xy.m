% Affichage des valeurs du voisinage de la croix
%
% Syntax
%   private_type_voisins_xy(this)
%
% Input Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function strOut = private_type_voisins_xy(this)

%% Traitement des synchronisations

% subx contient la liste des images ayant le meme tag de synchro en x, idem pour y
[this, subx]     = process_synchroX(this);
[this, suby]     = process_synchroY(this);
[this, subSonar] = process_synchroSonar(this);
sub = intersect(subx, suby);
sub = intersect(sub, subSonar);
sub(sub == this.indImage) = [];

%% Affichage des valeurs du voisinage du point sur la console

NbVoisins = max(this.NbVoisins, 2);

strOut = type_voisins_xy(this.Images, this.indImage, this.cross(this.indImage, 1), this.cross(this.indImage, 2), ...
    'NbVoisins', NbVoisins);
for k=1:length(sub)
    str = type_voisins_xy(this.Images, sub(k), ...
        this.cross(this.indImage, 1), ...
        this.cross(this.indImage, 2), 'LayerSeul', ...
        'NbVoisins', NbVoisins);
    strOut = [strOut str]; %#ok
end
