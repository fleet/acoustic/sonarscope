function this = process_pixel(this, varargin)

if ~getUseClickRightForFlag
    this = traiter_click_new(this, varargin{:});
    return
end

[flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);

%% Détermination si le curseur est sur l'un des 3 axes

if ~any(flag)
    return
end

cross = this.cross(this.indImage,:);

%% Position en X

if flag(1) || flag(2)
    if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3))

        % ------------------------------------------------------
        % Determination des limites de l'axe principal en x et y

        limites = axis(this.hAxePrincipal);

        % ---------------------------
        % Determination de l'abscisse

        if this.Images(this.indImage).XDir == 1
            cross(1) = limites(1) + (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        else
            cross(1) = limites(2) - (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        end
    end
end

%% Position en Y

if flag(1) || flag(3)
    if (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))

        % ------------------------------------------------------
        % Determination des limites de l'axe principal en x et y

        limites = axis(this.hAxePrincipal);

        % ---------------------------
        % Determination de l'ordonnee

        if this.Images(this.indImage).YDir == 1
            cross(2) = limites(3) + (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        else
            cross(2) = limites(4) - (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        end
    end
end

%% Mise a NaN de la valeur

[val, ix, iy] = get_val_xy(this.Images(this.indImage), cross(1), cross(2));

if length(this.valBeforeSetNaN) < this.indImage
    this.valBeforeSetNaN{this.indImage} = [ix, iy, double(val)];
    ValeurTrouvee = val;
else
    Trouve = false;
    for k=1:size(this.valBeforeSetNaN{this.indImage},1)
        if isequal(this.valBeforeSetNaN{this.indImage}(k,1:2), [ix, iy])
            Trouve = true;
            ValeurTrouvee = this.valBeforeSetNaN{this.indImage}(k,3);
            break
        end
    end
    if ~Trouve
        this.valBeforeSetNaN{this.indImage}(end+1,:) = [ix, iy, double(val)];
        ValeurTrouvee = val;
    end
end

ValNaN    = this.Images(this.indImage).ValNaN;
nbColumns = this.Images(this.indImage).nbColumns;
nbRows     = this.Images(this.indImage).nbRows;

ix = ix((ix >= 1) & (ix <= nbColumns));
iy = iy((iy >= 1) & (iy <= nbRows));

if isempty(ix) || isempty(iy)
    return
end

if ~isnan(ValeurTrouvee)
    valOther = ValeurTrouvee;
else
    if (isnan(ValNaN) && isnan(val)) || (~isnan(ValNaN) && (val == ValNaN))
        str1 = 'Aucune valeur initiale, voulez-vous copier la valeur d''un autre layer similaire ?';
        str2 = 'No initial value here, do you want to pick-up a pixel from another layer ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag || (rep == 2)
            return
        end
        
        [flag, indLayerOther, nomsLayerOther, DataTypeOther] = listeLayersSynchronises(this.Images, this.indImage, ...
            'OnlyOneLayer', 1, 'memeDataType'); %#ok<ASGLU>
        if flag && isempty(indLayerOther)
            str1 = 'Aucun layer trouvé.';
            str2 = 'No layer found.';
            my_warndlg(Lang(str1,str2), 1);
            valOther = [];
        elseif flag && ~isempty(indLayerOther)
            valOther = get_val_xy(this.Images(indLayerOther), cross(1), cross(2));
        else
            valOther = [];
        end
    else
        valOther = ValNaN;
    end
end

if isnan(ValNaN)
    if isnan(val)
        Value = valOther;
    else
        Value = NaN;
    end
else
    if val == ValNaN
        Value = valOther;
    else
        Value = ValNaN;
    end
end

if ~isempty(Value)
    this.Images(this.indImage) = set_val(this.Images(this.indImage), iy, ix, Value);
end

%% Affichage des profils et des valeurs

this = show_image(this);
this = show_profils(this);
