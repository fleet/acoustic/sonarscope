% Traitement des synchronisations de type de donn�es
%
% Syntax
%   [a, sub] = process_synchroSonar(a)
%
% Input Arguments
%   a    : instance de clc_image
%
% Output Arguments
%   a   : instance de clc_image initialisee
%   sub : liste des des images a synchroniser
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, sub] = process_synchroSonar(this)

if length(this.Images) == 1
    sub = 1;
else
    sub = [];
    GeometryType = this.Images(this.indImage).GeometryType;
    for k=1:length(this.Images)
        if this.Images(k).GeometryType == GeometryType
            sub(end+1) = k; %#ok
        end
    end
end
