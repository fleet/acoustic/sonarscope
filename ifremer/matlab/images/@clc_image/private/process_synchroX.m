% Process X synchronization
%
% Syntax
%   [this, sub] = process_synchroX(a)
%
% Input Arguments
%   this : clc_image instance
%
% Output Arguments
%   this : clc_image instance
%   sub  : list of images to synchronize
%
% Output Arguments
%   a : Instance of clc_image
%
% See also clc_image/process_synchroY Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, sub] = process_synchroX(this)

if length(this.Images) == 1
    sub = 1;
else
    sub = [];
    TagSynchroX      = this.Images(this.indImage).TagSynchroX;
    TagSynchroXScale = this.Images(this.indImage).TagSynchroXScale;
    XLim = get(this.Images(this.indImage), 'XLim');
    for k=1:length(this.Images)
        Tag   = this.Images(k).TagSynchroX;
        Scale = this.Images(k).TagSynchroXScale;
        if strcmp(Tag, TagSynchroX)
            if isempty(TagSynchroXScale) || isempty(Scale)
                this.visuLim(k, 1:2) = this.visuLim(this.indImage, 1:2);
                this.cross(k, 1)     = this.cross(this.indImage, 1);
                this.Images(k)       = set_XLim(this.Images(k), XLim);
            else
                ratio = TagSynchroXScale / Scale;
                this.visuLim(k, 1:2) = this.visuLim(this.indImage, 1:2) * ratio;
                this.cross(k, 1)     = this.cross(this.indImage, 1) * ratio;
                this.Images(k)       = set_XLim(this.Images(k), XLim * ratio);
            end
            sub(end+1) = k; %#ok<AGROW>
        end
    end
end
