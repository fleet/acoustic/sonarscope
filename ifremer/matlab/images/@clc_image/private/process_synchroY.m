% Process Y synchronization
%
% Syntax
%   [this, sub] = process_synchroY(a)
%
% Input Arguments
%   this : clc_image instance
%
% Output Arguments
%   this : clc_image instance
%   sub  : list of images to synchronize
%
% Output Arguments
%   a : Instance of clc_image
%
% See also clc_image/process_synchroX Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, sub] = process_synchroY(this)

if length(this.Images) == 1
    sub = 1;
else
    sub = [];
    TagSynchroY      = this.Images(this.indImage).TagSynchroY;
    TagSynchroYScale = this.Images(this.indImage).TagSynchroYScale;
    YLim = get(this.Images(this.indImage), 'YLim');
    for k=1:length(this.Images)
        Tag   = this.Images(k).TagSynchroY;
        Scale = this.Images(k).TagSynchroYScale;
        if strcmp(Tag, TagSynchroY)
            if isempty(TagSynchroYScale) || isempty(Scale)
                this.visuLim(k, 3:4) = this.visuLim(this.indImage, 3:4);
                this.cross(k, 2)     = this.cross(this.indImage, 2);
                this.Images(k)       = set_YLim(this.Images(k), YLim);
            else
                ratio = TagSynchroYScale / Scale;
                this.visuLim(k, 3:4) = this.visuLim(this.indImage, 3:4) * ratio;
                this.cross(k, 2)     = this.cross(this.indImage, 2) * ratio;
                this.Images(k)       = set_YLim(this.Images(k), YLim * ratio);
            end
            sub(end+1) = k; %#ok<AGROW>
        end
    end
end
