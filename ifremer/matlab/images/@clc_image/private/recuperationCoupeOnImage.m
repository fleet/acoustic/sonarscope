function Coupe = recuperationCoupeOnImage(this)

Coupe = [];
for k=1:length(this.coupe.handles)
    if ishandle(this.coupe.handles(k))
        Coupe(k).Color = get(this.coupe.handles(k), 'Color'); %#ok<AGROW>
        Coupe(k).LineWidth = get(this.coupe.handles(k), 'LineWidth'); %#ok<AGROW>
        Coupe(k).XData = get(this.coupe.handles(k), 'XData'); %#ok<AGROW>
        Coupe(k).YData = get(this.coupe.handles(k), 'YData'); %#ok<AGROW>
    end
end
