function [flag, this, OffsetX, OffsetY] = saisie_Offsets(this)

OffsetX = [];
OffsetY = [];

str1 = 'D�calage d''une image';
str2 = 'Image shift parameters';
XUnit = this.Images(this.indImage).XUnit;
YUnit = this.Images(this.indImage).YUnit;
p    = ClParametre('Name', 'Offset X', 'Unit', XUnit, 'Value', this.cross(this.indImage,1));
p(2) = ClParametre('Name', 'Offset Y', 'Unit', YUnit, 'Value', this.cross(this.indImage,2));
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
if ~flag
    return
end
OffsetX = val(1);
OffsetY = val(2);

this.cross(this.indImage,:) = this.cross(this.indImage,:) - [OffsetX OffsetY];
this.visuLim(this.indImage,1:2) = this.visuLim(this.indImage,1:2) - OffsetX;
this.visuLim(this.indImage,3:4) = this.visuLim(this.indImage,3:4) - OffsetY;
