% Saisie de la taille d'une fen�tre d'interpolation
%
% Syntax
%   [W, flag] = saisie_windowSetNaN(W, ...)
%
% Input Arguments
%   W : Initialisation de la fen�tre : [hauteur, Largeur]
%
% Name-Value Pair Arguments
%   maxvalue : Largeur et hauteurs max (11 par d�faut)
%   Titre    : Titre de la fen�tre
%
% Output Arguments
%   W    : Fen�tre saisie : [hauteur, Largeur]
%   flag : 1=OK, 0=KO
%
% Examples
%   [W, flag] = saisie_windowSetNaN([3 5])
%
% See also Authors

% Authors : JMA
%-------------------------------------------------------------------------------

 function [flag, W, Az] = saisie_windowSetNaN(this)
        
Titre = Lang('Param�tres de SetValue : Valeur & taille de la fenetre', 'Set Value parameters : Value & window size');

p    = ClParametre('Name', Lang('Hauteur', 'Height'), 'Unit', 'pix', 'Value', this.EreaseWindow(1),   'MinValue', 1,    'Format', '%d');
p(2) = ClParametre('Name', Lang('Largeur', 'Width'),  'Unit', 'pix', 'Value', this.EreaseWindow(2),   'MinValue', 1,    'Format', '%d');
p(3) = ClParametre('Name', Lang('Azimut', 'Azimuth'), 'Unit', 'deg', 'Value', this.EreaseWindowAngle, 'MinValue', -180, 'MaxValue', 360);
a = StyledSimpleParametreDialog('params', p, 'Title', Titre);
a.openDialog;
flag = a.okPressedOut;
if ~flag
    W = [];
    Az = [];
    return
end
X = a.getParamsValue;
if ~flag
    return
end

W  = X(1:2);
Az = X(3);
