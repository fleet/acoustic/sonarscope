function selectGUISelectImage(this, str)

global IfrTbxGUISelectImage %#ok<GVMIS>

switch str
    case 'On'
        IfrTbxGUISelectImage = 1;
    otherwise
        IfrTbxGUISelectImage = 2;
end

maj_menu_GUISelectImage(this)
