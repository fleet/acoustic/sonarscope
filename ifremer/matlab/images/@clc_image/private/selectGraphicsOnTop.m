function this = selectGraphicsOnTop(this, val)

global IfrTbxGraphicsOnTop %#ok<GVMIS>

IfrTbxGraphicsOnTop = val;
this = maj_menu_GraphicsOnTop(this);
