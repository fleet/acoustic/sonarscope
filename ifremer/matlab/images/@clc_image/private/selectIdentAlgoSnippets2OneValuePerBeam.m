function this = selectIdentAlgoSnippets2OneValuePerBeam(this, val)

global IdentAlgoSnippets2OneValuePerBeam %#ok<GVMIS>

IdentAlgoSnippets2OneValuePerBeam = val;
this = maj_menu_IdentAlgoSnippets2OneValuePerBeam(this);
