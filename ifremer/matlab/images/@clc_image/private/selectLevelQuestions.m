function this = selectLevelQuestions(this, val)

global IfrTbxLevelQuestions %#ok<GVMIS>

IfrTbxLevelQuestions = val;
this = maj_menu_LevelQuestions(this);
