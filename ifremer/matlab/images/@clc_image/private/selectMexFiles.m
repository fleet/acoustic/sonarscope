function selectMexFiles(this, val)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

if isnumeric(val) && (val == 1)
    set(this.ihm.Handles.TagPreferencesMexFilesYes, 'Checked', 'On');
    set(this.ihm.Handles.TagPreferencesMexFilesNo,  'Checked', 'Off');
    NUMBER_OF_PROCESSORS = str2double(getenv('NUMBER_OF_PROCESSORS'));
    
%     str1 = sprintf('Le multithreading n''est pas complètement opérationel.\nSi vous choisissez %d il est possibble que seulement 1/%d de l''image sera traitée.\nChoisissez combien de threads vous voulez utiliser.\nSi vous choisissez 1 vous accélérerez certains traitements mais ne profiterez pas de tous vos processeurs.', ...
%         NUMBER_OF_PROCESSORS, NUMBER_OF_PROCESSORS);
%     str2 = sprintf('The multitreading is not fully operatioal yet.\nIf you select %d it is possible that only 1/%d of the image will be processed.\nSelect how many threads you want to be used.\nIf you select 1 you will accelerate some processings but you will not take advantage of all your processoss.', ...
%         NUMBER_OF_PROCESSORS, NUMBER_OF_PROCESSORS);
%     [rep, flag] = my_questdlg(Lang(str1,str2), '1', num2str(NUMBER_OF_PROCESSORS));
%     if ~flag
%         return
%     end
%     if rep == 1
%         NUMBER_OF_PROCESSORS = 1;
%     end
else
    set(this.ihm.Handles.TagPreferencesMexFilesYes, 'Checked', 'Off');
    set(this.ihm.Handles.TagPreferencesMexFilesNo,  'Checked', 'On');
    NUMBER_OF_PROCESSORS = 0;
end
