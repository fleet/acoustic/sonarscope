function selectMultithreading(this, val)

if isnumeric(val) && (val == 1)
    set(this.ihm.Handles.PreferencesMultithreadingOne,  'Checked', 'On');
    set(this.ihm.Handles.PreferencesMultithreadingAuto, 'Checked', 'Off');
    maxNumCompThreads(1);
else
    set(this.ihm.Handles.PreferencesMultithreadingOne,  'Checked', 'Off');
    set(this.ihm.Handles.PreferencesMultithreadingAuto, 'Checked', 'On');
    maxNumCompThreads('automatic');
end
