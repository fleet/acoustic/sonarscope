function selectTempDir(~)

str1 = 'Le répertoire temporaire sera pris en compte quand vous relancerez SonarScope.';
str2 = 'Temporary directory will be changed at next SonarScope Launch.';
my_warndlg(Lang(str1,str2), 1);

maj_menu_TempDir
