function selectUseClickRightForFlag(this, str)

global IfrTbxUseClickRightForFlag %#ok<GVMIS>
IfrTbxUseClickRightForFlag = str;

if strcmpi(IfrTbxUseClickRightForFlag, 'on')
    str1 = 'Le click droite va maintenant autoriser une mise � NaN d''un pixel ou le retour de sa valeur pr�c�dente. Il ermet �galement d''aller aspirer la valeur d''un autre layer.';
    str2 = 'Right cliking on a pixel will set it to NaN or return its previsous value. It can also get the value from another layer.';
    my_warndlg(Lang(str1,str2), 1);
    set(this.ihm.Handles.TagPreferencesClickRightForFlagYes, 'Checked', 'On');
    set(this.ihm.Handles.TagPreferencesClickRightForFlagNo,  'Checked', 'Off');
else
    set(this.ihm.Handles.TagPreferencesClickRightForFlagYes, 'Checked', 'Off');
    set(this.ihm.Handles.TagPreferencesClickRightForFlagNo,  'Checked', 'On');
end



