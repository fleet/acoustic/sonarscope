function [nomFic, flag] = selectionFichierFiltre(filtre, varargin)

global IfrTbxResources %#ok<GVMIS>
global SonarScopeData %#ok<GVMIS>

[varargin, Exclude] = getPropertyValue(varargin, 'Exclude', []); %#ok<ASGLU>

if ~iscell(filtre)
    filtre = {filtre};
end

listeFicPerDir = {};
for k=1:length(filtre)
    listeFicPerDir{end+1} = findFicInDir(IfrTbxResources,                    filtre{k},  'Exclude', Exclude); %#ok
    listeFicPerDir{end+1} = findFicInDir(fullfile(SonarScopeData, 'Public',  filtre{k}), 'Exclude', Exclude); %#ok
    listeFicPerDir{end+1} = findFicInDir(fullfile(SonarScopeData, 'Sonar',   filtre{k}), 'Exclude', Exclude); %#ok
    listeFicPerDir{end+1} = findFicInDir(fullfile(SonarScopeData, 'Levitus', filtre{k}), 'Exclude', Exclude); %#ok
end
liste = {};
for k1=1:length(listeFicPerDir)
    for k2=1:length(listeFicPerDir{k1})
        liste{end+1} = listeFicPerDir{k1}{k2}; %#ok
    end
end

if isempty(liste)
    my_warndlg('No file exists. Are you sure you have installed the demonstration data directories ?', 1);
end

[rep, flag] = my_listdlg(Lang('Choisissez un fichier', 'Select a file'), liste);
if flag
    if length(rep) > 1
        nomFic = liste(rep);
    else
        nomFic = liste{rep};
    end
else
    nomFic = [];
end

