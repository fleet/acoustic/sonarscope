function [flag, sub] = selectionPublicCurves(this)

flag = 0;

nbCurves = length(this.PublicCurves);
if nbCurves == 0
    sub = [];
    return
end

str1 = 'S�lection des courbes';
str2 = 'Curves selection';
for k=1:length(this.PublicCurves)
    str{k} = this.PublicCurves(k).bilan{1}(1).nomZoneEtude; %#ok<AGROW>
end
[sub, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', []);
