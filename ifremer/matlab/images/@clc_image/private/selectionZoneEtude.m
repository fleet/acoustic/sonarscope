function [flag, ZoneEtudeInd] = selectionZoneEtude(this, varargin)

[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Single'); %#ok<ASGLU>

Names    = {};
subNames = [];
for k=1:length(this.ZoneEtude)
    if isnumeric(this.ZoneEtude(k).TagSynchroX)
        this.ZoneEtude(k).TagSynchroX = num2str(this.ZoneEtude(k).TagSynchroX);
    end
    if isnumeric(this.ZoneEtude(k).TagSynchroY)
        this.ZoneEtude(k).TagSynchroY = num2str(this.ZoneEtude(k).TagSynchroY);
    end
    
    if isequal(this.ZoneEtude(k).TagSynchroX, this.Images(this.indImage).TagSynchroX) && ...
       isequal(this.ZoneEtude(k).TagSynchroY, this.Images(this.indImage).TagSynchroY)
        Names{end+1} = this.ZoneEtude(k).Name; %#ok<AGROW>
        subNames(end+1) = k; %#ok<AGROW>
    end
end
if isempty(Names)
    str1 = 'Il faut d''abord d�finir une zone d''�tude.';
    str2 = 'First, define a study zone.';
    my_warndlg(Lang(str1,str2),1);
    ZoneEtudeInd = 0;
%     flag = 1;
    flag = 0; % Modif JMA le 23/03/2021
else
    str1 = 'Zone d''�tude';
    str2 = 'Study area';
    [rep, flag] = my_listdlg(Lang(str1,str2), Names, 'SelectionMode', SelectionMode);
    ZoneEtudeInd = subNames(rep);
end
