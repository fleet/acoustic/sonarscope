function this = session_restore(this)

% TODO : sortir uiSelectFiles de clc-image/private

% 'ChaineIncluse' =  'ession' car dans un premier temps c'�tait Session_SonarScope_xx.xml et maintenant Session_aaaammdd_hhmmss.xml
[flag, liste, lastDir] = uiSelectFiles('ExtensionFiles', '.xml', 'RepDefaut', this.repImport, 'ChaineIncluse', 'ession');
if ~flag
    return
end
this.repImport = lastDir;

for k=1:length(liste)
    [flag, this] = session_restore_unitaire(this, liste{k});
    if ~flag
        return
    end
end
this = editer_image_selectionnee(this);


function [flag, this] = session_restore_unitaire(this, nomFicXML)

%% Lecture du XML

XML = xml_read(nomFicXML);

%% Lecture des images

[pathname, nomFic] = fileparts(nomFicXML);
pathname = fullfile(pathname, nomFic);
% listeFicImagesXML = listeFicOnDir(pathname, '.xml');

a  = cl_image.empty;

if ischar(XML.ListeImagesXML)
    XML.ListeImagesXML = {XML.ListeImagesXML};
end
flag = 0;
N = length(XML.ListeImagesXML);
str1 = 'Restauration de la session en cours';
str2 = 'Restore session';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [~, nomFicImage] = fileparts(XML.ListeImagesXML{k});
    nomFicImage = fullfile(pathname, [nomFicImage '.xml']);
    
    if exist(nomFicImage, 'file')
        %% D�zippage �ventuel
        
        % TODO : faire un unzipAssociatedDirectotyToXmlFile pendant de zipAssociatedDirectotyToXmlFile
        [nomDirXml, nomFicSeul] = fileparts(nomFicImage);
        nomDirXml = fullfile(nomDirXml, [nomFicSeul '.zip']);
        my_unzip(nomDirXml)
        
        %% Importation de l''image
        
        [flag, b] = cl_image.import_xml(nomFicImage, 'Memmapfile', 1);
        if flag
            a(end+1) = b; %#ok<AGROW>
        else
            str1 = sprintf('L''mage "%s" could n''a pas pu �tre charg�e', nomFicImage);
            str2 = sprintf('Image "%s" could not be loaded', nomFicImage);
            my_warndlg(Lang(str1,str2), 0);
        end
    end
end
my_close(hw, 'MsgEnd')

if ~flag
    str1 = sprintf('L''import de la session "%s" ne s''est pas bien d�roul�.', nomFicXML);
    str2 = sprintf('The import of "%s" failed.', nomFicXML);
    my_warndlg(Lang(str1,str2), 1);
    return
end

NBefore = length(this.Images);
this = add_image(this, a);

this.indImage                         = NBefore + XML.indImage;
this.visuGrid                         = XML.visuGrid;
this.visuCoupeHorz                    = XML.visuCoupeHorz;
this.visuCoupeVert                    = XML.visuCoupeVert;
this.visuColorbar                     = XML.visuColorbar;
this.identVisu(NBefore+1:NBefore+N)   = XML.identVisu;
this.TypeInteractionSouris            = XML.TypeInteractionSouris;
% this.visuLim(NBefore+1:NBefore+N,:) = XML.visuLim;
this.NbVoisins                        = XML.NbVoisins;
% this.cross(NBefore+1:NBefore+N,:)       = XML.cross;
% this.visuLimHistory(NBefore+1:NBefore+N) = XML.visuLimHistory;
% this.visuLimHistoryPointer(NBefore+1:NBefore+N) = XML.visuLimHistoryPointer;

% TODO : Les zones d'int�r�t ne sont pas prises en compte pourquoi ???
if ~isempty(XML.ZoneEtude)
    if isempty(this.ZoneEtude)
        this.ZoneEtude = XML.ZoneEtude;
    else
        this.ZoneEtude(NBefore+1:NBefore+length(XML.ZoneEtude)) = XML.ZoneEtude;
    end
    this.ZoneEtudeInd = NBefore + XML.ZoneEtudeInd;
end

str1 = sprintf('L''import de la session "%s" est termin�.', nomFicXML);
str2 = sprintf('The import of "%s" is OK.', nomFicXML);
my_warndlg(Lang(str1,str2), 0);

%{
fLabelX: 1
fLabelY: 1
%}
