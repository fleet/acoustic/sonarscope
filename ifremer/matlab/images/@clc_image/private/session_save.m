function this = session_save(this)

%% Saisie du nom de la session

 filtre = my_tempname('DirName', this.repExport, '.xml', 'Prefixe', 'Session', 'NoRandom');
[flag, nomFicXML] = my_uiputfile('*_session.xml', Lang('Nom du projet', 'Give a project name'), filtre);
if ~flag
    return
end

%% V�rification si la session existe d�j�

[nomDir, nomFicSeul] = fileparts(nomFicXML);
nomFicCsv = fullfile(nomDir, nomFicSeul, [nomFicSeul '.csv']);
nomDirSave = fullfile(nomDir, nomFicSeul);
if exist(nomDirSave, 'dir')
    str1 = sprintf('Le r�pertoire "%s" n''est pas vide, �tes-vous s�r de vouloir l''effacer ? ?', nomDirSave);
    str2 = sprintf('The directory "%s" is not empty, are you sure you want to delete it ?', nomDirSave);
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag || (rep == 2)
        return
    end
    
    % Destruction du r�pertoire
    
    [flag, mes] = rmdir(nomDirSave, 's');%#ok
    if ~flag
        mes1 = sprintf('Le r�pertoire %s n''a pas pu �tre supprim�. Choisissez un autre nom de fichier .xls.', nomDirSave);
        mes2 = sprintf('Directory %s could not be removed from your computer. Select another .xls file.', nomDirSave);
        my_warndlg(Lang(mes1,mes2), 1);
        return
    end
end
this.repExport = nomDir;

%% Cr�ation du r�pertoire

[flag, mes] = mkdir(nomDirSave);
if ~flag
    my_warndlg(mes, 1);
    return
end

%% S�lection des images � sauver

N = length(this.Images);
NomImage = cell(N,1);
for k=1:N
    ImageName = this.Images(k).Name;
    NomImage{k} = rmblank(ImageName);
end
sub = find(~strcmp(NomImage, 'Earth'));
str1 = 'Liste des images � Sauver';
str2 = 'List of images to save';
[sub, flag] = my_listdlgMultiple(Lang(str1,str2), NomImage, 'InitialValue', sub);
if ~flag
    return
end
if isempty(sub)
    return
end

%% Sauvegarde des images individuelles

N = length(sub);
nomFicImage = cell(N,1);
str1 = 'Sauvegarde de la session en cours';
str2 = 'Save session';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    kImage = sub(k);
    ImageName = this.Images(kImage).Name;
    ImageName = rmblank(ImageName);
    [~, version] = cl_image.extract_ImageNameVersion(ImageName);
    this.Images(kImage) = update_Name(this.Images(kImage));
    ImageName = this.Images(kImage).Name;
    ImageName = rmblank(ImageName);

    
    % Rajout� le 10/10/2011 par JMA � cause de @#~&}{� d'images provenant
    % de l'ancien import .s7k, le nom de l'image �tait le nome du fichier
    % complet
    [~, ImageName] = fileparts(ImageName);
    
    if ~isempty(version)
        ImageName = ['(' version ')' ImageName]; %#ok<AGROW>
    end
    nomFicImage{k} = fullfile(nomDirSave, [ImageName '.xml']);
%     nomFicImage{k} = [ImageName '.xml'];
    this.Images(kImage) = set(this.Images(kImage), 'Name', ImageName);
    flag = export_xml(this.Images(kImage), nomFicImage{k});
    if flag
        flag = zipAssociatedDirectotyToXmlFile(nomFicImage{k}); %#ok<NASGU>
    end
end
my_close(hw, 'MsgEnd')

%% Sauvegarde du fichier XML

XML.indImage                = get(this, 'indImage');
XML.visuGrid                = get(this, 'visuGrid');
XML.visuCoupeHorz           = get(this, 'visuCoupeHorz');
XML.visuCoupeVert           = get(this, 'visuCoupeVert');
XML.visuColorbar            = get(this, 'visuColorbar');
XML.identVisu               = get(this, 'identVisu');
XML.TypeInteractionSouris   = get(this, 'TypeInteractionSouris');
XML.visuLim                 = get(this, 'visuLim');
XML.NbVoisins               = get(this, 'NbVoisins');
XML.repImport               = get(this, 'repImport');
XML.repExport               = get(this, 'repExport');
XML.fLabelX                 = get(this, 'fLabelX');
XML.fLabelY                 = get(this, 'fLabelY');
XML.ZoneEtude               = get(this, 'ZoneEtude');
XML.ZoneEtudeInd            = get(this, 'ZoneEtudeInd');
XML.cross                   = get(this, 'cross');
XML.visuLimHistory          = get(this, 'visuLimHistory');
XML.visuLimHistoryPointer   = get(this, 'visuLimHistoryPointer');
XML.ListeImagesXML          = nomFicImage;

XML.identVisu               = XML.identVisu(sub);
XML.visuLim                 = XML.visuLim(sub,:);
XML.cross                   = XML.cross(sub,:);
XML.visuLimHistory          = XML.visuLimHistory(sub);
XML.visuLimHistoryPointer   = XML.visuLimHistoryPointer(sub);

XML.indImage = min(XML.indImage, N);

xml_write(nomFicXML, XML);
flag = exist(nomFicXML, 'file');
if ~flag
    messageErreurFichier(nomFicXML, 'WriteFailure');
    return
end
% edit(nomFicXml)

%% Sauvegarde du r�sum� dans un fichier excel

[idx, filteredStr, str] = summary(this.Images(sub), nomDir, 'ExportResults', 0); %#ok<ASGLU>
flag = my_csvwrite(nomFicCsv, str);
if ~flag
%     my_warndlg(mes.message, 1);
end

str1 = sprintf('Le projet "%s" a �t� correctement sauv�.', [nomFicSeul '_session.xml']);
str2 = sprintf('Project "%s" has been correctly saved.', [nomFicSeul '_session.xml']);
my_warndlg(Lang(str1,str2), 1);
