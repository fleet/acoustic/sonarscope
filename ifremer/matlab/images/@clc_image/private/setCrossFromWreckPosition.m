function this = setCrossFromWreckPosition(this)

nomFicXml = fullfile(my_tempdir, 'Wrecks.xml');
createExampleFileWrecks(nomFicXml)

str1 = sprintf('Un exemple de fichier XML vient d''�tre cr��, voir "%s".\nVous pouvez le recopier et le compl�ter. Attention, le fichier d''exemple sera �cras� � la prochaine utilisation de cette fonction.', nomFicXml);
str2 = sprintf('One example of XML file has just been created, see "%s".\nYou can copy it and add you own wrecks.\nTake care "Wrecks.xml will be erased next time you will use this function.', nomFicXml);
my_warndlg(Lang(str1,str2), 1);

nomFicXml = SelectionFichier(fullfile(my_tempdir,'*xml'));
if isempty(nomFicXml)
    return
end
Wrecks = xml_read(nomFicXml);

str1 = 'S�lectionnez une �pave';
str2 = 'Select one wreck';
ListeEpaves = {Wrecks(:).Name};
[rep, flag] = my_listdlg(Lang(str1,str2), ListeEpaves, 'SelectionMode', 'Single');
if ~flag
    return
end

Carto = get_Carto(this.Images(this.indImage));
GT    = this.Images(this.indImage).GeometryType;

lon = Wrecks(rep).Lon;
lat = Wrecks(rep).Lat;

if isempty(Wrecks(rep).Ellips)
    str1 = 'Attention, l''ellipsoide n''est pas renseign�e pour cette �pave.';
    str2 = 'Take care, no ellipsoid id defined for this wreck.';
    my_warndlg(Lang(str1,str2), 1);
else
    strType = get(Carto, 'Ellipsoide.strType');
    Type = get(Carto, 'Ellipsoide.Type');
    if ~strcmpi(strType{Type}, Wrecks(rep).Ellips)
        str1 = sprintf('Attention, l''ellipsoide renseign� pour cette �pave est : "%s"\nL''ellipsoide d�finie dans les param�tres cartographique de l''image est "%s". Le positionnement n''est pas bon.', Wrecks(rep).Ellips, strType{Type});
        str2 = sprintf('Take care, the ellipsoide defined for this wrest is : "%s"\nThe ellipsoid define for the current image is  "%s".\nThe positioning is certainly not correct.', Wrecks(rep).Ellips, strType{Type});
        my_warndlg(Lang(str1,str2), 1);
    end
end

switch GT
    case cl_image.indGeometryType('LatLong')
        this.cross(this.indImage,:) = [lon lat];
    case cl_image.indGeometryType('GeoYX')
        [x, y] = xy2latlon(Carto, lat, lon);
        this.cross(this.indImage,:) = [x y];
end


%% Traitement des synchronisation en X et Y

this.ihm.CoupeHorz.cpn = set_etat(this.ihm.CoupeHorz.cpn, 1);
this = callback_profileHorz(this);
this = callback_profileVert(this, 'NoSelectionSignaVert', 1, 'DirectWay');
this = show_profils(this);
% this = process_synchroX(this);
% this = process_synchroY(this);

%% Affichage de la page web

if ~isempty(Wrecks(rep).web)
    try
        my_web(Wrecks(rep).web)
    catch %#ok<CTCH>
        messageErreurFichier(Wrecks(rep).web, 'ReadFailure');
    end
end

