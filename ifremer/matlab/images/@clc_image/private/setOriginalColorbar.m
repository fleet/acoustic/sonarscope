function this = setOriginalColorbar(this)

c = this.ihm.colorbar.cpn;

this.Images(this.indImage).OriginColorbar.Tick      =  get_tick(c);
this.Images(this.indImage).OriginColorbar.TickLabel = get_tick_label(c);

h = findobj(this.hfig, 'Type', 'axes');
GCO = setdiff(h, [this.hAxePrincipal this.hAxeProfilX this.hAxeProfilY]);

this.Images(this.indImage).OriginColorbar.YLim = get(GCO,'YLim');

% JMA : J'essaye de récupérer la colormap mais j'comprends rien
% GLU : le 26/04/2013 récupération de l'image. Cela semble fonctionner.
pppp = findobj(GCO(1), 'Type', 'image');

C = get(pppp, 'CData');
C = flipud(C);
%     C(:,1) = 0; % J'aurais voulu rendre l'effet d'ombrage sur la table de
%     couleur

this.Images(this.indImage).OriginColorbar.Image = C;
