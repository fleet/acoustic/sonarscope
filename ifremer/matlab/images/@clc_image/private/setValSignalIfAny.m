function [flag, this] = setValSignalIfAny(this, val, suby)

flag = 1;
switch this.profilY.Type
    case 'Time'
        Val = get(this.Images(this.indImage), 'Time');
        Val = Val.timeMat;
        Val(suby) = val;
        Val = cl_time('timeMat', Val);
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarTime', Val);
    case 'PingNumber'
        Val = get(this.Images(this.indImage), 'PingNumber');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarPingNumber', Val);
    case 'Immersion'
        Val = get(this.Images(this.indImage), 'Immersion');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarImmersion', Val);
    case 'Height'
        Val = get(this.Images(this.indImage), 'Height');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarHeight', Val);
    case 'CableOut'
        Val = get(this.Images(this.indImage), 'CableOut');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarCableOut', Val);
    case 'Speed'
        Val = get(this.Images(this.indImage), 'Speed');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarSpeed', Val);
    case 'Heading'
        Val = get(this.Images(this.indImage), 'Heading');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarHeading', Val);
    case 'Roll'
        Val = get(this.Images(this.indImage), 'Roll');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarRoll', Val);
    case 'Pitch'
        Val = get(this.Images(this.indImage), 'Pitch');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarPitch', Val);
    case 'Yaw'
        Val = get(this.Images(this.indImage), 'Yaw');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarYaw', Val);
    case 'SurfaceSoundSpeed'
        Val = get(this.Images(this.indImage), 'SurfaceSoundSpeed');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarSurfaceSoundSpeed', Val);
    case 'Heave'
        Val = get(this.Images(this.indImage), 'Heave');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarHeave', Val);
    case 'Tide'
        Val = get(this.Images(this.indImage), 'Tide');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarTide', Val);
    case 'PingCounter'
        Val = get(this.Images(this.indImage), 'PingCounter');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarPingCounter', Val);
    case 'SampleFrequency'
        Val = get(this.Images(this.indImage), 'SampleFrequency');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarSampleFrequency', Val);
    case 'SonarFrequency'
        Val = get(this.Images(this.indImage), 'SonarFrequency');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarFrequency', Val);
    case 'FishLatitude'
        Val = get(this.Images(this.indImage), 'FishLatitude');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarFishLatitude', Val);
    case 'FishLongitude'
        Val = get(this.Images(this.indImage), 'FishLongitude');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarFishLongitude', Val);
    case 'ShipLatitude'
        Val = get(this.Images(this.indImage), 'ShipLatitude');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarShipLatitude', Val);
    case 'ShipLongitude'
        Val = get(this.Images(this.indImage), 'ShipLongitude');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarShipLongitude', Val);
    case 'PortMode_1'
        Val = get(this.Images(this.indImage), 'PortMode_1');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarPortMode_1', Val);
%     case 'EM2040Mode2'
%         Val = get(this.Images(this.indImage), 'EM2040Mode2');
%         Val(suby) = val;
%         this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarEM2040Mode2', Val);
%     case 'EM2040Mode3'
%         Val = get(this.Images(this.indImage), 'EM2040Mode3');
%         Val(suby) = val;
%         this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarEM2040Mode3', Val);
    case 'PortMode_2'
        Val = get(this.Images(this.indImage), 'PortMode_2');
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarPortMode_2', Val);
    case 'StarMode_1'
        Val = get(this.Images(this.indImage), 'StarMode_1');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarStarMode_1', Val);
    case 'StarMode_2'
        Val = get(this.Images(this.indImage), 'StarMode_2');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarStarMode_2', Val);
    case 'Interlacing'
        Val = get(this.Images(this.indImage), 'Interlacing');
        Val(suby) = val;
        this.Images(this.indImage) = set(this.Images(this.indImage), 'Interlacing', Val);
        
        % TODO : a compl�ter avec tous les nouveaux signaux ou
        % plut�t automatiser tout ce binz
    otherwise
        flag = 0;
end