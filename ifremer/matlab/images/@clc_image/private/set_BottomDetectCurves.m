function this = set_BottomDetectCurves(this, BD)

ImageName = extract_ImageName(this.Images(this.indImage));
ImageName = strrep(ImageName, '_dB', '');
ImageName = strrep(ImageName, '_Amp', '');
if strcmp(ImageName(end), '_')
    ImageName(end) = [];
end
TagSynchroYScale = this.Images(this.indImage).TagSynchroYScale;
for k=1:length(BD)
    if strcmp(BD(k).UserData(end), '_')
        BD(k).UserData(end) = [];
    end
    if strcmp(ImageName, BD(k).UserData)
        hold on;
        
        if isempty(BD(k).TagSynchroYScale) || isempty(TagSynchroYScale)
            h = plot(BD(k).XData, BD(k).YData);
        else
            ratio = BD(k).TagSynchroYScale / TagSynchroYScale;
            h = plot(BD(k).XData, BD(k).YData * ratio);
        end
        
        set(h, 'Color',         BD(k).Color)
        set(h, 'LineStyle',     BD(k).LineStyle)
        set(h, 'LineWidth',     BD(k).LineWidth)
        set(h, 'Marker',        BD(k).Marker)
        set(h, 'MarkerSize',    BD(k).MarkerSize)
        set(h, 'Tag', 'BottomDetector', 'UserData', ImageName)
        setappdata(h, 'TagSynchroYScale', TagSynchroYScale)
        hold off
    end
end
