function this = set_PublicCurves(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>

C = get_CourbesStatistiques(this.Images(this.indImage), 'sub', sub);

this.PublicCurves = [this.PublicCurves; C(:)];

for k=1:length(this.Images)
    if (k ~= this.indImage)
        this.Images(k) = add_CourbesStatistiques(this.Images(k), C);
    end
end
