% Initialisation de l accessibilite du composant
% 
% Syntax
%   a = set_cenable(a, enable)
%
% Input Arguments
%   a      : instance de clc_image
%   enable : accessibilite du composant {'on', 'off'}
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_cenable(this, enable)

%% Test validite des arguments, initialisation

if ischar(enable)
    this.globalFrame  = set(this.globalFrame, 'Enable', enable);
    this.cl_component = set(this.cl_component, 'componentEnable', enable);
else
    disp('clc_image/set_cenable : Invalid value');
end
