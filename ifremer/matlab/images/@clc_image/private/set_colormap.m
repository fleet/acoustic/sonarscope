% Initialisation de la table de couleurs des differents axes
% 
% Syntax
%   a = set_colormap(a, map)
%
% Input Arguments
%   a   : instance de clc_image
%   map : table de couleurs
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_colormap(this, map)

this.colormap = map;

%% Mise � jour de l'instance

if is_edit(this.composant)
    set(this.hfig, 'CurrentAxes', this.hAxePrincipal);
    Video = this.Images(this.indImage).Video;
    this.colormap = double(this.colormap);
    this.colormap(this.colormap < 0) = 0;
    this.colormap(this.colormap > 1) = 1;
    if Video == 1
        colormap(this.colormap);
    else
        colormap(flipud(this.colormap));
    end
end
