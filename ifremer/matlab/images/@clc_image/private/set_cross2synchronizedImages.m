function this = set_cross2synchronizedImages(this)
        
TagSynchroX = this.Images(this.indImage).TagSynchroX;
for i=1:length(this.Images)
    if strcmp(TagSynchroX, this.Images(i).TagSynchroX)
        this.cross(  i, 1)   = this.cross(   this.indImage, 1);
        this.visuLim(i, 1:2) = this.visuLim( this.indImage, 1:2);
    end

    if strcmp(this.Images(this.indImage).TagSynchroY, this.Images(i).TagSynchroY)
        this.cross(i,2)     = this.cross(  this.indImage, 2);
        this.visuLim(i,3:4) = this.visuLim(this.indImage, 3:4);
    end
end