% Initialisation de la visibilite du composant
% 
% Syntax
%   a = set_cvisible(a, visible)
%
% Input Arguments
%   a       : instance de clc_image
%   visible : visibilite du composant {'on', 'off'}
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_cvisible(this, visible)

%% Test validite des arguments, initialisation

if ischar(visible)
    this.globalFrame  = set(this.globalFrame, 'Visible', visible);
    this.cl_component = set(this.cl_component, 'componentVisible', visible);
else
    disp('clc_image/set_cvisible : Invalid value');
end
