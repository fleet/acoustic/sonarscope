% Initialisation du flag de reservation de la place de la colorbar
% 
% Syntax
%   a = set_f_fixed_colorbar(a, fFixedColorBar)
%
% Input Arguments
%   a              : instance de clc_image
%   fFixedColorBar : flag d affichage de la colorbar
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_f_fixed_colorbar.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_f_fixed_colorbar(this, fFixedColorBar)

isOk = 1;

%% Test validite des arguments

if ~isnumeric(fFixedColorBar)
    isOk = 0;
    my_warndlg('clc_image:set_f_fixed_colorbar Invalid argument format', 0);
elseif (fFixedColorBar ~= 0) && (fFixedColorBar ~= 1)
    isOk = 0;
    disp('clc_image/set_f_fixed_colorbar : Invalid argument value');
end

%% Initialisation de l'instance

if isOk
    this.fFixedColorBar = fFixedColorBar;
else
    this.fFixedColorBar = 0;
end

%% Mise � jour de l'instance

if is_edit(this)
    hColorBar = colorbar;
    this.ihm.colorbar.cpn = copy(this.ihm.colorbar.cpn, hColorBar);
    if ishandle(hColorBar)
        delete(hColorBar);
    end
    this = positionner(this);
end
