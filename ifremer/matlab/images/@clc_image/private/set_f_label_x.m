% Initialisation du flag de presence des labels selon l'axe x
% 
% Syntax
%   a = set_f_label_x(a, fLabelX)
%
% Input Arguments
%   a       : instance de clc_image
%   fLabelX : flag d affichage du label en x (0 ou 1)
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_f_label_x(this, fLabelX)

isOk = 1;

%% Test validité des arguments

if ~isnumeric(fLabelX)
    isOk = 0;
    disp('clc_image/set_f_label_x : Invalid argument format');
elseif ((fLabelX ~= 0) && (fLabelX ~= 1))
    isOk = 0;
    disp('clc_image/set_f_label_x : Invalid argument value');
end

%% Initialisation de l'instance

if isOk
    this.fLabelX = fLabelX;
else
    this.fLabelX = 0;
end

%% Mise à jour de l'instance

% if is_edit(this)
if is_edit(this.composant)
    if this.fLabelX
        set(this.hAxePrincipal, 'XTickMode', 'auto');
        set(this.hAxePrincipal, 'XTickLabelMode', 'auto');
    else
        set(this.hAxePrincipal, 'XTick', []);
    end
    this = positionner(this);
end
