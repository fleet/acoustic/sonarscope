% Initialisation du flag de presence des labels selon l axe y
% 
% Syntax
%   a = set_f_label_y(a, fLabelY)
%
% Input Arguments
%   a       : instance de clc_image
%   fLabelY : flag d affichage du label en y (0 ou 1)
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_f_label_y(this, fLabelY)

isOk = 1;

%% Test validité des arguments

if ~isnumeric(fLabelY)
    isOk = 0;
    disp('clc_image/set_f_label_y : Invalid argument format');
elseif (fLabelY ~= 0) && (fLabelY ~= 1)
    isOk = 0;
    my_warndlg('clc_image:set_f_label_y Invalid argument value', 0);
end

%% Initialisation de l'instance

if isOk
    this.fLabelY = fLabelY;
else
    this.fLabelY = 0;
end

%% Mise à jour de l'instance

% if is_edit(this)
if is_edit(this.composant)
    if this.fLabelY
        set(this.hAxePrincipal, 'YTickMode', 'auto');
        set(this.hAxePrincipal, 'YTickLabelMode', 'auto');
    else
        set(this.hAxePrincipal, 'YTick', []);
    end
    this = positionner(this);
end
