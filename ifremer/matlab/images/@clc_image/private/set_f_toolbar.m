% Initialisation du flag de presence de la toolbar
% 
% Syntax
%   a = set_f_toolbar(a, fToolBar)
%
% Input Arguments
%   a        : instance de clc_image
%   fToolBar : flag d affichage de la toolbar
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_f_toolbar.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_f_toolbar(this, fToolBar)

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~isnumeric(fToolBar)
    isOk = 0;
    disp('clc_image/set_f_toolbar : Invalid argument format');
elseif (fToolBar ~= 0) && (fToolBar ~= 1)
    isOk = 0;
    disp('clc_image/set_f_toolbar : Invalid argument value');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.fToolBar = fToolBar;
else
    this.fToolBar = 0;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
