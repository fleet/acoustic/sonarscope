% Init flag de mise en place du zoom asservi
% 
% Syntax
%   a = set_f_zoom(a)
%
% Input Arguments
%   a    : instance de clc_image
%   flag : flag indiquant la mise en place, ou du retrait,
%                      du zoom asservi
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_f_zoom.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_f_zoom(this)

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = placer_zoom(this);
end
