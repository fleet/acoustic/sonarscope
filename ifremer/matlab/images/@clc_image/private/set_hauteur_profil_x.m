% Initialisation la hauteur du profil selon x (en nb de lignes)
% 
% Syntax
%   a = set_hauteur_profil_x(a, hauteurProfilX)
%
% Input Arguments
%   a              : instance de clc_image
%   hauteurProfilX : hauteur du profil en x
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_hauteur_profil_x.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   03/09/03 - JMA - creation
% ----------------------------------------------------------------------------

function this = set_hauteur_profil_x(this, hauteurProfilX)

% Maintenance Auto : try
    
    % -------
    % Locales
    
    isOk = 1;
    
    % ---------------------------
    % Test validite des arguments
    
    if ~isnumeric(hauteurProfilX)
        isOk = 0;
        disp('clc_image/set_hauteur_profil_x : Invalid argument format');
    elseif hauteurProfilX < 0
        isOk = 0;
        disp('clc_image/set_hauteur_profil_x : Invalid argument value');
    end
    
    % ----------------------------
    % Initialisation de l'instance
    
    if isOk
        this.hauteurProfilX = hauteurProfilX;
    else
        this.hauteurProfilX = 0;
    end
    
    % -------------------------
    % Mise a jour de l'instance
    
    if is_edit(this)
        this = positionner(this);
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('clc_image', 'set_hauteur_profil_x', lasterr);
% Maintenance Auto : end
