% Initialisation la hauteur de la toolbar (en nb de lignes)
% 
% Syntax
%   a = set_hauteur_toolbar(a, hauteurToolBar)
%
% Input Arguments
%   a              : instance de clc_image
%   hauteurToolBar : hauteur de la toolbar
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_hauteur_toolbar.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_hauteur_toolbar(this, hauteurToolBar)

% -------
% Locales

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~isnumeric(hauteurToolBar)
    isOk = 0;
    disp('clc_image/set_hauteur_toolbar : Invalid argument format');
elseif hauteurToolBar < 0
    isOk = 0;
    disp('clc_image/set_hauteur_toolbar : Invalid argument value');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.hauteurToolBar = hauteurToolBar;
else
    this.hauteurToolBar = 0;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
