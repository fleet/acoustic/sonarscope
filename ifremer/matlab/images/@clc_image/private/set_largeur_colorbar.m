% Initialisation la largeur de la colorbar (en nb de colonnes)
% 
% Syntax
%   a = set_largeur_colorbar(a, largeurColorBar)
%
% Input Arguments
%   a               : instance de clc_image
%   largeurColorBar : largeur de la colorbar
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_largeur_colorbar.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_largeur_colorbar(this, largeurColorBar)

% -------
% Locales

isOk = 1;

% ----------------------------
% Test validite des arguments

if ~isnumeric(largeurColorBar)
    isOk = 0;
    disp('clc_image/set_largeur_colorbar : Invalid argument format');
elseif largeurColorBar < 0
    isOk = 0;
    disp('clc_image/set_largeur_colorbar : Invalid argument value');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.largeurColorBar = largeurColorBar;
else
    this.largeurColorBar = 0;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
