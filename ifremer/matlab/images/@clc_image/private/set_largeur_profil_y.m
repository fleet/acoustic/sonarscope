% Initialisation la largeur du profil selon y (en nb de colonnes)
% 
% Syntax
%   a = set_largeur_profil_y(a, largeurProfilY)
%
% Input Arguments
%   a              : instance de clc_image
%   largeurProfilY : largeur du profil en y
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_largeur_profil_y.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_largeur_profil_y(this, largeurProfilY)

% -------
% Locales

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~isnumeric(largeurProfilY)
    isOk = 0;
    disp('clc_image/set_largeur_profil_y : Invalid argument format');
elseif largeurProfilY < 0
    isOk = 0;
    disp('clc_image/set_largeur_profil_y : Invalid argument value');
end

% -----------------------------
% Initialisation de l'instance

if isOk
    this.largeurProfilY = largeurProfilY;
else
    this.largeurProfilY = 0;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
