% Initialisation la marge pour le label x de l'axe principal
% 
% Syntax
%   a = set_marge_label_x(a, margeLabelX)
%
% Input Arguments
%   a           : instance de clc_image
%   margeLabelX : marge pour le label en x
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_marge_label_x.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_marge_label_x(this, margeLabelX)

% -------
% Locales

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~isnumeric(margeLabelX)
    isOk = 0;
    disp('clc_image/set_marge_label_x : Invalid argument format');
elseif margeLabelX < 0
    isOk = 0;
    disp('clc_image/set_marge_label_x : Invalid argument value');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.margeLabelX = margeLabelX;
else
    this.margeLabelX = 0;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
