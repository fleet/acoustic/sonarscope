% Initialisation la marge pour le label y de l axe principal
% 
% Syntax
%   a = set_marge_label_y(a, margeLabelY)
%
% Input Arguments
%   a           : instance de clc_image
%   margeLabelY : marge pour le label en y
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_marge_label_y.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_marge_label_y(this, margeLabelY)

% -------
% Locales

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~isnumeric(margeLabelY)
    isOk = 0;
    disp('clc_image/set_marge_label_y : Invalid argument format');
elseif margeLabelY < 0
    isOk = 0;
    disp('clc_image/set_marge_label_y : Invalid argument value');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.margeLabelY = margeLabelY;
else
    this.margeLabelY = 0;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
