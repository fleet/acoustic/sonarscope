% Initialisation la largeur totale en nb de colonnes
% 
% Syntax
%   a = set_nb_tot_colonnes(a, nbTotColonnes)
%
% Input Arguments
%   a             : instance de clc_image
%   nbTotColonnes : largeur totale en nb de colonnes
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_nb_tot_colonnes.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_nb_tot_colonnes(this, nbTotColonnes)

% -------
% Locales

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~isnumeric(nbTotColonnes)
    isOk = 0;
    disp('clc_image/set_nb_tot_colonnes : Invalid argument format');
elseif nbTotColonnes < 0
    isOk = 0;
    disp('clc_image/set_nb_tot_colonnes : Invalid argument value');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.nbTotColonnes = nbTotColonnes;
else
    this.nbTotColonnes = 1;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
