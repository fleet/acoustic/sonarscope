% Initialisation la hauteur totale en nb de lignes
% 
% Syntax
%   a = set_nb_tot_lignes(a, nbTotLignes)
%
% Input Arguments
%   a           : instance de clc_image
%   nbTotLignes : hauteur totale en nb de lignes
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_nb_tot_lignes.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_nb_tot_lignes(this, nbTotLignes)

% -------
% Locales

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~isnumeric(nbTotLignes)
    isOk = 0;
    disp('clc_image/set_nb_tot_lignes : Invalid argument format');
elseif nbTotLignes < 0
    isOk = 0;
    disp('clc_image/set_nb_tot_lignes : Invalid argument value');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.nbTotLignes = nbTotLignes;
else
    this.nbTotLignes = 1;
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this = positionner(this);
end
