% Initialisation du texte du composant de la valeur V
% 
% Syntax
%   a = set_txt_valeur_v(a, txtValeurV)
%
% Input Arguments
%   a          : instance de clc_image
%   txtValeurV : texte du composant de la toolbar
%
% Output Arguments
%   a : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% VERSION  : $Id: set_txt_valeur_v.m,v 1.1 2003/09/08 15:33:58 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_txt_valeur_v(this, txtValeurV)

isOk = 1;

% ---------------------------
% Test validite des arguments

if ~ischar(txtValeurV)
    isOk = 0;
    disp('clc_image/set_txt_valeur_v : Invalid argument format');
end

% ----------------------------
% Initialisation de l'instance

if isOk
    this.txtValeurV = txtValeurV;
else
    this.txtValeurV = '';
end

% -------------------------
% Mise a jour de l'instance

if is_edit(this)
    this.ihm.ValImage.cpn = set_texte(this.ihm.ValImage.cpn, this.txtValeurV);
end
