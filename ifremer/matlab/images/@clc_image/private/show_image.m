% Affichage de l'image
%
% this = show_image(this)
%
% Input Arguments
%   this : instance de cli_exemple
%
% Output Arguments
%   this : instance de cli_exemple initialisee
%
% Remarks : Cette methode devrait s'appeler imagesc tout simplement. Si on
% fait cela, matlab se plante, il faut alors dplacer cette mthode dans le
% repertoire clc_image
%
% See also cli_exemple Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [this, flag] = show_image(this, varargin)

[varargin, MajColorbar] = getPropertyValue(varargin, 'MajColorbar', 1); %#ok<ASGLU>

flag = 1;

CurrentFig = gcf; % Rajout� par JMA le 18/12/12 pour �viter que des figures soient cach�es par la fen�tre principale

if (this.identVisu(this.indImage) == 2) && (this.Images(this.indImage).ImageType == 2)
    this.identVisu(this.indImage) = 1;
    str1 = 'Une image RGB ne peut pas �tre repr�sent�e sous forme de contours.';
    str2 = 'A RGB image cannot be plot with contours.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImageRGBCannotBePlotInContours', 'TimeDelay', 60);
end

%% Repr�sentation

switch this.identVisu(this.indImage)
    case 1
        if get_GraphicsOnTop == 1
            % TODO : renommer recuperationCoupeOnImage et coupe_display
            Coupe = recuperationCoupeOnImage(this);
            BottomDetect = get_BottomDetectCurves(this);
        end
        [this, flag] = private_imagesc(this, 'MajColorbar', MajColorbar);
        if get_GraphicsOnTop == 1
            ROI_display(this, 'NoQuestion');
            this = coupe_display(this, Coupe);
            this = set_BottomDetectCurves(this, BottomDetect);
        end
        
    case 2
        this = private_contour(this);
        if get_GraphicsOnTop == 1
            ROI_display(this);
        end
        
    case 3
        this = private_surf(this);
        
    case 4
        this = private_plot(this, 'CurrentExtent');
        
    otherwise
        disp('cli_exemple/show_image : Invalid representation');
end

%% On redonne le focus sur la figure

if ~isempty(CurrentFig) && ishandle(CurrentFig)
    figure(CurrentFig) % Rajout� par JMA le 18/12/12 pour �viter que des figures soient cach�es par la fen�tre principale
end

