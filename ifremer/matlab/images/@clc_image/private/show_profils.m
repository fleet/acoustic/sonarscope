% Affichage des valeurs en x, y et des profils
%
% Syntax
%   this = show_profils(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = show_profils(this)

%% D�termination de la valeur represent�e pour x et y a la position du curseur

[valV, ix, iy] = get_val_xy(this.Images(this.indImage), ...
    this.cross(this.indImage, 1), this.cross(this.indImage, 2));
valV = double(valV);


SpectralStatus = this.Images(this.indImage).SpectralStatus;
switch SpectralStatus
    case 2
        valV = 20 * log10(abs(valV));
    case 3
        disp('Faut pas r�ver')
end

ImageType = this.Images(this.indImage).ImageType;

%% Mise � jour des valeurs indiqu�es par les composants de la toolbar

this.ihm.ValX.cpn     = set_value(this.ihm.ValX.cpn, this.cross(this.indImage, 1));
this.ihm.ValY.cpn     = set_value(this.ihm.ValY.cpn, this.cross(this.indImage, 2));
this.ihm.ValImage.cpn = set_value(this.ihm.ValImage.cpn, valV);
this.ihm.IX.cpn       = set_value(this.ihm.IX.cpn, ix);
this.ihm.IY.cpn       = set_value(this.ihm.IY.cpn, iy);

%% Pr�paratifs

x = get_x_inXLim(this.Images(this.indImage));
if isempty(x)
    return
end
y = get_y_inYLim(this.Images(this.indImage));
if isempty(x)
    return
end

%% Trac� du profil X

[ix, iy]   = xy2ij(this.Images(this.indImage), x, this.cross(this.indImage, 2));
ideb = iy - this.NbVoisins;
ideb = max(ideb, 1);
ifin = iy + this.NbVoisins;
ifin = min(ifin, double(this.Images(this.indImage).nbRows));
iyDisplay = ideb:ifin;
V = get_val_ij(this.Images(this.indImage), iyDisplay, ix);

if islogical(V)
    V = uint8(V);
end

% ATTENTION : d�but bidouille pour contourner pb insurmontable de cl_memmapfile
% (voir les lignes suivantes dans cl_memmapfile/getValue
%       pppp1 = obj.Size;
%       pppp2 = obj.m.Format{2};
%       if isequal(pppp1, fliplr(pppp2))
%           'ici';
%       end
if size(V,2) == 1
    V = my_transpose(V);
end
% Fin bidouille


if (ImageType == 2) && (size(V,3) == 3) % Cas d'une image RGB, on affiche l'intensite
    NTSC = rgb2ntsc(V);
    NTSC = NTSC(:,:,2);
    V = NTSC;
end
this = plot_profilX(this, x, V, iyDisplay);

%% Trac� du profil Y

[ix, subl] = xy2ij(this.Images(this.indImage), this.cross(this.indImage, 1), y);
ideb = ix - this.NbVoisins;
ideb = double(ideb);    % Sinon max pas content
ideb = max(ideb, 1);
ifin = ix + this.NbVoisins;
ifin = double(ifin);    % Sinon max pas content
ifin = min(ifin, double(this.Images(this.indImage).nbColumns));
ixDisplay = ideb:ifin;
V = get_val_ij(this.Images(this.indImage), subl, ixDisplay);

if islogical(V)
    V = uint8(V);
end

% ATTENTION : d�but bidouille pour contourner pb insurmontable de cl_memmapfile
% (voir les lignes suivantes dans cl_memmapfile/getValue
%       pppp1 = obj.Size;
%       pppp2 = obj.m.Format{2};
%       if isequal(pppp1, fliplr(pppp2))
%           'ici';
%       end
if size(V,1) == 1
    V = my_transpose(V);
end
% Fin bidouille

if (ImageType == 2) && (size(V,3) == 3) % Cas d'une image RGB, on affiche l'intensite
    NTSC = rgb2ntsc(V);
    
% Bidouille si image en memmapfile format ErMapper : TODO
% C'est cl_memmapfile, m�thode getValue instructions  
%   X  = obj.m.Data.x(varargin{2}, subCan(i), varargin{1});
%   Value(:,:,i) = (squeeze(X))'; %#ok<AGROW>
% qu'il faudrait retravailler : attention aux effets de bord
    if (ndims(NTSC) == 3) && (size(NTSC,1) == 1)
        NTSC = NTSC(:,:,2);
        V = NTSC';
    else
        NTSC = NTSC(:,:,2);
        V = NTSC;
    end
end
this = plot_profilY(this, y, V, ixDisplay, subl);

%% Croix

if ishandle(this.hAxePrincipal)
    this = croix(this, 'principal', this.cross(this.indImage, 1), this.cross(this.indImage, 2));
    this = croix(this, 'profilX',   this.cross(this.indImage, 1), valV);
    this = croix(this, 'profilY',   this.cross(this.indImage, 2), valV);
end
