function [flag, FileList, repImport, sameFile, subx, suby] = survey_uiSelectFiles(this, varargin)

[varargin, Ext]               = getPropertyValue(varargin, 'ExtensionFiles',    []);
[varargin, InitialFileFormat] = getPropertyValue(varargin, 'InitialFileFormat', []);

if isempty(InitialFileFormat)
    if ~iscell(Ext)
        Ext = {Ext};
    end
    for k=1:length(Ext)
        switch lower(Ext{k})
            case {'.all'; '.kmall'}
                InitialFileFormat = 'SimradAll';
                
            case '.xsf'
                InitialFileFormat = 'XSF';
                
            case '.rdf'
                InitialFileFormat = 'GeoSwathRDF';
                
            case '.sdf'
                InitialFileFormat = 'KleinSDF';
                
            case '.xtf'
                InitialFileFormat = 'XTF';
                
            case '.na'
                InitialFileFormat = 'CINNA';
                
            case '.raw'
                InitialFileFormat = 'ExRaw';
                
            case '.s7k'
                InitialFileFormat = 'ResonS7k';
                
            case '.ers'
                InitialFileFormat = 'ERS';
                
            case '.hac'
                InitialFileFormat = 'HAC';
                
            case '.odv'
                InitialFileFormat = 'ODV';
                
            case {'.seg'; '.sgy'; '.segy'}
                InitialFileFormat = 'SEGY';
                
            case {'.mbb'; '.mbg'}
                InitialFileFormat = 'CaraibesMbb';
                
            case '.im'
                InitialFileFormat = 'SAR';
                
            case '.nc'
                InitialFileFormat = 'NC';
                
            case '.csv'
                InitialFileFormat = 'CSV';
                
            case '.asvp'
                InitialFileFormat = 'SVP';
                
            case {'.geoid'; '.nvi'}
        end
            
    end
    if isempty(InitialFileFormat)
        str1 = sprintf('Message pour JMA : survey_uiSelectFiles pas renseign� pour l''extension %s', Ext{1});
        str2 = sprintf('Message for JMA : survey_uiSelectFiles not set for extention %s', Ext{1});
        my_warndlg(Lang(str1,str2), 1);
    end
end
% TODO Simrad_ExRaw 'EK60? ...'

repImport = this.repImport;
repImportOut = repImport;
FileList  = [];
sameFile  = 0;
subx      = [];
suby      = [];

FileInitialFileFormat = this.Images(this.indImage).InitialFileFormat;
InitialFileName       = this.Images(this.indImage).InitialFileName;
if isempty(InitialFileName)
    nomFicSeul = [];
    InitialExt = [];
else
    [~, nomFicSeul, InitialExt] = fileparts(InitialFileName);
end
if strcmpi(FileInitialFileFormat, InitialFileFormat) && exist(InitialFileName, 'file')
    str1 = 'S�lection des fichiers';
    str2 = 'Files selection ...';
    str3 = 'Choix avec un "browser" de fichiers';
    str4 = 'With a browser';
    str5 = ['Only   ' nomFicSeul, InitialExt];
    [Selection, flag] = my_questdlg(Lang(str1,str2), str5, Lang(str3,str4));
    if isempty(Selection) || ~flag
        return
    end
else
    Selection = 0;
end

if Selection == 1
    FileList = {InitialFileName};
    sameFile = 1;
    flag = testSignature(this.Images(this.indImage), 'GeometryType', 'PingXxxx', 'noMessage');
    if flag && (nargout == 6)
        [flag, subx, suby] = getSubxSubyContextuel(this);
        if ~flag
            return
        end
    end
else
    [flag, FileList, repImportOut] = uiSelectFiles('ExtensionFiles', Ext, varargin{:});
    if ~flag
        return
    end
end
repImport = repImportOut;
flag = 1;

drawnow % TODO : sais pas pourquoi cette instruction
