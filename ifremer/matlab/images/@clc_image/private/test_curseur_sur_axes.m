% Test si le curseur est sur un des 3 axes
% 
% Syntax
%   [flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   flag                 : [flagPrinipam flagX flagY ]
%                          1 le curseur est sur l'un des 3 axes (0 sinon)
%   positionCurseur      : Position du curseur par rapport a la figurel
%   positionAxePrincipal : Position de l'axe principal par rapport a la
%                          figure
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this)

flag = [0 0 0];

%% Determination de la position du curseur (en pixels)

currentUnits = get(0, 'units');
set(0, 'units', 'pixels');

if ~ishandle(this.hfig)
    flag = 0;
    positionCurseur = [];
    positionAxePrincipal = [];
    return
end

positionCurseurDansFigure = get(this.hfig, 'currentPoint');
set(0, 'units', currentUnits);

%% Détermination de la position de l'axe principal (en pixels)

currentUnits = get(this.hAxePrincipal, 'units');
set(this.hAxePrincipal, 'units', 'pixels');
positionAxePrincipal = get(this.hAxePrincipal, 'Position');
set(this.hAxePrincipal, 'units', currentUnits);

%% On teste si le curseur est dans l'axe principal

positionCurseur = positionCurseurDansFigure - positionAxePrincipal(1:2);
if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3)) && ...
        (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))
    flag(1) = 1;
    return
end

%% Détermination de la position de l'axe du profil horizontal

currentUnits = get(this.hAxeProfilX, 'units');
set(this.hAxeProfilX, 'units', 'pixels');
positionAxeProfilX = get(this.hAxeProfilX, 'Position');
set(this.hAxeProfilX, 'units', currentUnits);

%% On test si le curseur est dans l'axe de la coupe horizontale

positionCurseur = positionCurseurDansFigure - positionAxeProfilX(1:2);
if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxeProfilX(3)) && ...
        (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxeProfilX(4))
    flag(2) = 1;
    return
end

%% Détermination de la position de l'axe du profil vertical

currentUnits = get(this.hAxeProfilY, 'units');
set(this.hAxeProfilY, 'units', 'pixels');
positionAxeProfilY = get(this.hAxeProfilY, 'Position');
set(this.hAxeProfilY, 'units', currentUnits);

%% On test si le curseur est dans l'axe de la coupe verticale

positionCurseur = positionCurseurDansFigure - positionAxeProfilY(1:2);
if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxeProfilY(3)) && ...
        (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxeProfilY(4))
    flag(3) = 1;
    return
end

