function this = traiterProfilY(this, nomsLayers, indLayers, xyCross, indLayerCurrent, ix, varargin)

[varargin, Process] = getPropertyValue(varargin, 'Process', []); %#ok<ASGLU>

YLim = this.visuLim(this.indImage, 3:4);

y   = this.profilY.x;
sub = find((y >= YLim(1)) & (y <= YLim(2)));
y = y(sub);
val = this.profilY.val(sub);

OtherProfiles = struct([]);
for k=1:length(indLayers)
    yLayer = get(this.Images(indLayers(k)), 'y');
    suby = find((yLayer >= min(y)) & (yLayer <= max(y)));
    if isempty(suby)
        continue
    end
    OtherProfiles(end+1).nomLayer = nomsLayers{k}; %#ok<AGROW>
    OtherProfiles(end).Unit   = get(this.Images(indLayers(k)), 'Unit');
    OtherProfiles(end).xLayer = yLayer(suby);
    OtherProfiles(end).yLayer = get_val_xy(this.Images(indLayers(k)), xyCross(1), yLayer(suby));
    OtherProfiles(end).xUnit  = this.Images(indLayers(k)).YUnit;
    OtherProfiles(end).xName  = this.Images(indLayers(k)).YLabel;
    OtherProfiles(end).CurrentView  = 0;
end

yLayer = get(this.Images(this.indImage), 'y');
suby = find((yLayer >= min(y)) & (yLayer <= max(y)));

OtherProfiles(indLayerCurrent).CurrentView  = 1;
liste = get_liste_vecteurs(this.Images(this.indImage), 'VerticalOnly');
for k=2:length(liste)
    OtherProfiles(end+1).nomLayer = liste{k}; %#ok<AGROW>
    OtherProfiles(end).xLayer = y;
    
    % TODO : Apr�s refonte ClImage et ImageDialog : R�cup�rer les ClSignaux
    OtherProfiles(end).xUnit  = '';
    OtherProfiles(end).xName  = '';
    
    % TODO : ne plus utiliser le get g�n�ral mais un get_signalxxx et
    % sortir l'unit�
    yLayer = get(this.Images(this.indImage), liste{k});
    if isempty(yLayer)
        continue
    end
    
    if isvector(yLayer)
        yLayer = yLayer(:);
    end
%     try
        yLayer = yLayer(suby,:);
%     catch
%         yLayer = yLayer(suby);
%     end
    OtherProfiles(end).yLayer = yLayer;
    OtherProfiles(end).Unit = [];
    if strcmp(this.profilY.Type, liste{k})
        OtherProfiles(end).CurrentView  = 1;
    else
        OtherProfiles(end).CurrentView  = 0;
    end
end

if ~isempty(liste)
    OtherProfiles(end+1).nomLayer = 'MeanValueOfCurrentExtent';
    OtherProfiles(end).xLayer = y;
    OtherProfiles(end).xUnit  = this.Images(this.indImage).YUnit;
    OtherProfiles(end).xName  = this.Images(this.indImage).YLabel;

    x = get(this.Images(this.indImage), 'x');
    XLim = this.visuLim(this.indImage, 1:2);
    subx = find((x >= XLim(1)) & (x <= XLim(2)));
    yLayer = mean_lig(this.Images(this.indImage), 'subx', subx, 'suby', suby);
    OtherProfiles(end).yLayer = yLayer;
    OtherProfiles(end).CurrentView  = 0;
end

%% Action correspondant au choix de traitement

%         nomImage = sprintf('%s / x=%s / Colonne=%d', nomImage, num2str(xyCross(1)), ix);

GT = this.Images(this.indImage).GeometryType;
if (get_LevelQuestion >= 3) && strcmp(this.profilY.Type, 'Height') && ...
        ((GT == cl_image.indGeometryType('PingSamples')) ...
        || (GT == cl_image.indGeometryType('PingTravelTime')) ...
        || (GT == cl_image.indGeometryType('PingRange')) ...
        || (GT == cl_image.indGeometryType('PingDepth')))
    [rep, flag] = my_questdlg(Lang('Image en arri�re-plan ?', 'Image in background ?'), 'ColorLevel', 3);
    if ~flag
        return
    end
    if rep == 1
        [subxZ, subyZ, xZ, yZ, Background]  = get_Image_zoomee(this); %#ok<ASGLU>
        subxPositif  = find(xZ > 0);
        subxNegatif  = find(xZ < 0);
        if length(subxPositif) > length(subxNegatif)
            xZ = xZ(subxPositif);
            Background = Background(:,subxPositif);
        elseif length(subxPositif) < length(subxNegatif)
            xZ = -xZ(subxNegatif);
            Background = Background(:, subxNegatif);
        else % on peut choisir
            str1 = Lang('Quel c�t� ?', 'Which side ?');
            str2 = Lang('Babord', 'Portside');
            str3 = Lang('Tribord', 'Starboard');
            [rep, flag] = my_questdlg(str1,str2,str3);
            if ~flag
                return
            end
            if rep == 1
                xZ = -xZ(subxNegatif);
                Background = Background(:, subxNegatif);
            else
                xZ = xZ(subxPositif);
                Background = Background(:,subxPositif);
            end
        end
        Background  = Background';
        xBackground = yZ;
        yBackground = xZ;
    else
        Background  = [];
        xBackground = [];
        yBackground = [];
    end
else
    Background  = [];
    xBackground = [];
    yBackground = [];
end

ColormapBackground = this.Images(this.indImage).Colormap;
CLimBackground     = this.Images(this.indImage).CLim;
VideoBackground    = this.Images(this.indImage).Video;

[val, flag] = traiter_signal(y(:)', val(:)', this.profilY.Type, ...
    'Background', Background, 'xBackground', xBackground, 'yBackground', yBackground, ...
    'QuestionAxeReverse', 'Unit', this.profilY.Unit, 'repImport', this.repImport, 'repExport', this.repExport, ...
    'OtherProfiles', OtherProfiles, 'Process', Process, ...
    'colormapData', ColormapBackground, 'CLimBackground', CLimBackground, 'VideoBackground', VideoBackground == 2);
if ~flag
    return
end
val = val';

%% On sauve les donn�es dans l'image

[flag, this] = setValSignalIfAny(this, val, suby);
if ~flag
    subx = (ix-this.NbVoisins):(ix+this.NbVoisins);
    this.Images(this.indImage) = set_val(this.Images(this.indImage), suby, subx, val);
    this.Images(this.indImage) = compute_stats(this.Images(this.indImage));
end
