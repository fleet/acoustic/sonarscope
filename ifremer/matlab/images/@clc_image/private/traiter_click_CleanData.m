function this = traiter_click_CleanData(this, identAxe)

persistent persistent_widthToClean % persistent_heightToClean

WindowButtonMotionFcnBefore = get(this.hfig, 'WindowButtonMotionFcn');
WindowButtonDownFcnBefore   = get(this.hfig, 'WindowButtonDownFcn');
WindowButtonUpFcnBefore     = get(this.hfig, 'WindowButtonUpFcn');

%% Lecture du type d'interaction dans l'iHM externe

[flag, TypeInteraction, newVal, CleanMask, ValToBeCleaned, widthToClean, heightToClean] = this.hGUITypeInteractionSouris.getTypeInteractionCleanData(); %#ok<ASGLU>
if ~flag
    return
end
if this.Images(this.indImage).DataType ~= cl_image.indDataType('DetectionType')
    CleanMask = 0;
end

if ~isempty(persistent_widthToClean)
%     if (widthToClean ~= persistent_widthToClean) || (heightToClean ~= persistent_heightToClean)
        this = traiter_typeInteractionSouris(this, 'AskWidthsIfBrush');

        %{
        this = callback_profileHorz(this);
        this = callback_profileVert(this);
        %}
%     end
end
persistent_widthToClean  = widthToClean;
% persistent_heightToClean = heightToClean;

%% Cas o� l'op�ration de nettoyage a �t� effectu�e sur les signaux horizontaux

if identAxe(2)
    NbVoisins = this.NbVoisins;
    cross = this.cross(this.indImage,:);
    hAxe = gca;
    this = cleanDataAxeX(this, cross, NbVoisins, TypeInteraction, hAxe);
    return
end

%% Cas o� l'op�ration de nettoyage a �t� effectu�e sur les signaux verticaux

if identAxe(3)
    % TODO : DANGER : param�trage diff�rent si Fran�ais ou anglais : �
    % corriger au plus vite
    switch this.profilY.Type
        case {'Vertical profile'; 'Image profile'}
            NbVoisins = this.NbVoisins;
            cross = this.cross(this.indImage,:);
            hAxe = gca;
            this = cleanDataAxeY(this, cross, NbVoisins, TypeInteraction, hAxe);
            return
            
        case 'MeanValueOfCurrentExtent'
%             NbVoisins = this.NbVoisins;
            cross = this.cross(this.indImage,:);
            hAxe = gca;
            this = cleanDataAxeYMeanImage(this, cross, TypeInteraction, hAxe);
            return
            
        otherwise
            cross = this.cross(this.indImage,:);
            hAxe = gca;
            this = cleanDataAxeYSignal(this, cross, TypeInteraction, hAxe);
            return
    end
end

%% Cas o� l'op�ration de nettoyage a �t� effectu�e sur l'image

if identAxe(1)
    hAxe = gca;
    this = cleanDataAxeImage(this, TypeInteraction, hAxe, CleanMask, ValToBeCleaned);
    return
end

    function this = cleanDataAxeImage(this, TypeInteraction, hAxe, CleanMask, ValToBeCleaned)
        set(this.hfig, 'pointer', 'crosshair')
        msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
        set(this.hfig, 'WindowButtonDownFcn', []);
        
        figure(this.hfig)
%         hAxe = gca;
        switch TypeInteraction
            case 1 % Rectangle
                H = getrect(hAxe);
                if isempty(H)
                    message_rectangle
                    return
                end
                deltax = get(this.Images(this.indImage), 'XStep') / 2;
                deltay = get(this.Images(this.indImage), 'YStep') / 2;
                pX = [H(1)-deltax H(1)+H(3)+deltax H(1)+H(3)+deltax H(1)-deltax];
                pY = [H(2)-deltay H(2)-deltay      H(2)+H(4)+deltay H(2)+H(4)+deltay];
                
            case 2 % Free hand
                imfreehandH = imfreehand(hAxe);
                %         wait(imfreehandH)
                X = getPosition(imfreehandH);
                if isempty(X)
                    message_patch
                    return
                end
                pX = X(:,1);
                pY = X(:,2);
                
            case 3 % Polyline
                [~, pX, pY] = roipoly;
                
            case 4 % Rubber
                if isdeployed
                    str1 = 'Le nettoyage par gomme n''est pas encore op�rationnel en mode compil� (trop de bugs).';
                    str2 = 'The Data Cleaning by "Rubber" on ilage is not yed available (too much bugs).';
                    my_warndlg(Lang(str1,str2), 1);
                    return
                else
                    %                 GommeImageDown(this)
                    set(this.hfig, 'WindowButtonUpFcn',   @(src,event)GommeImageUp(this, src, event));
                    set(this.hfig, 'WindowButtonDownFcn', @(src,event)GommeImageDown(this, src, event));
                    return
                end
                
            case 5 % Rectangle autour du Point&Click
                messgae5
                % TODO : devra remplacer traiter_click_NaN quand IHM sera
                % compl�t�
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        end
        
        % [masq, pX, pY] = roipoly;
        % pause(1)
        
        
        % TODO : voir "ROI_manual" lignes 146:... pour gestion des
        % d�passements �ventuels des limites des images
        
        XLim = this.visuLim(this.indImage, 1:2);
        YLim = this.visuLim(this.indImage, 3:4);
        x = this.Images(this.indImage).x;
        y = this.Images(this.indImage).y;
        subx = find((x >= XLim(1)) & (x <= XLim(2)));
        suby = find((y >= YLim(1)) & (y <= YLim(2)));
        
        masq = roipoly(x(subx), y(suby), zeros(length(suby), length(subx), 'uint8'), pX, pY);
        
        val = get_val_ij(this.Images(this.indImage), suby, subx);
        
        if CleanMask % Rajout� par JMA mission BICOSE
            masq(val ~= ValToBeCleaned) = 0;
        end
        
        oldVal = val(masq);
        val(masq) = newVal;
        this.Images(this.indImage) = set_val(this.Images(this.indImage), suby, subx, val);
        
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        
        %% Affichage des profils et des valeurs
        
        this = show_image(this);
        this = show_profils(this);
        
        %% Validation
        
        undo = validateDataCleaning;
        if undo
            val(masq) = oldVal;
            this.Images(this.indImage) = set_val(this.Images(this.indImage), suby, subx, val);
            this = show_image(this);
            this = show_profils(this);
        end
        set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
        figure(this.hfig)
    end


    function this = cleanDataAxeX(this, cross, NbVoisins, TypeInteraction, hAxe)
        set(this.hfig, 'pointer', 'crosshair')
        that = this.Images(this.indImage);
%         ValNaN = get(that, 'ValNaN');
        x = get(that, 'x');
        
        [~, ~, iy] = get_val_xy(that, cross(1), cross(2));
        
        ideb = iy - NbVoisins;
        ideb = max(ideb, 1);
        ifin = iy + NbVoisins;
        ifin = min(ifin, double(that.nbRows));
        iyDisplay = ideb:ifin;
        
        val = get_val_ij(that, iyDisplay, []);
        nbY = length(iyDisplay);
        x = repmat(x, nbY, 1);
        Old = val;
        
        switch TypeInteraction
            case 1 % Rectangle
                H = getrect(hAxe);
                if isempty(H)
                    message_rectangle
                    return
                end
                pX = [H(1) H(1)+H(3)];
                pY = [H(2) H(2)+H(4)];
                Masque = ((x >= min(pX)) & (x <= max(pX)) & (val >= min(pY)) & (val <= max(pY)));
                val(Masque) = newVal;
                
            case 2 % Free hand
                nbY = 200;
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                imfreehandH = imfreehand(hAxe);
                
                % TODO : Le remplacement de imfreehand par drawfreehand
                % bugue pour l'instant. Il fauit r�nover compl�tement
                % SonarScope en ImageDialog !!! Le pb vient sans doute de
                % cl_frame/desactiver le zoom
%                 imfreehandH = drawfreehand(hAxe, 'InteractionsAllowed', 'none');
                
                %         wait(imfreehandH)
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                if isempty(imfreehandH)
                    return
                end
                X = getPosition(imfreehandH);
%                 X = selection.Position;
                if isempty(X)
                    message_patch
                    return
                end
                pX = X(:,1);
                pY = X(:,2);
                val = set_valX(that, val, pX, pY, nbY);
                
            case 3 % Polyline
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                
%                 [~, pX, pY] = roipoly; % Ne fonctionne pas si il n'y aa
%                 pas d'image
                
                [pX, pY] = inputPolyline;
                
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                Masque = ((x >= min(pX)) & (x <= max(pX)) & (val >= min(pY)) & (val <= max(pY)));
                val(Masque) = newVal;
                
            case 4 % Rubber
                GommeXDown(this)
                set(this.hfig, 'WindowButtonUpFcn',   @(src,event)GommeXUp(this, src, event));
                set(this.hfig, 'WindowButtonDownFcn', @(src,event)GommeXDown(this, src, event));
                return
                
            case 5 % Rectangle autour du Point&Click
                messgae5
                % TODO : devra remplacer traiter_click_NaN quand IHM sera
                % compl�t�
        end
        
        that = set_val(that, iyDisplay, [], val);
        this.Images(this.indImage) = that;
        
        this = show_image(this);
        this = show_profils(this);
        
        %% Validation
        
        undo = validateDataCleaning;
        if undo
            that = set_val(that, iyDisplay, [], Old);
            this.Images(this.indImage) = that;
            this = show_image(this);
            this = show_profils(this);
        end
    end

    function this = cleanDataAxeY(this, cross, NbVoisins, TypeInteraction, hAxe)
        set(this.hfig, 'pointer', 'crosshair')
        that = this.Images(this.indImage);
        
        y = get(that, 'y');
        
        [~, ix] = get_val_xy(that, cross(1), cross(2));
        
        ideb = ix - NbVoisins;
        ideb = max(ideb, 1);
        ifin = ix + NbVoisins;
        ifin = min(ifin, double(that.nbColumns));
        ixDisplay = ideb:ifin;
        nbY = length(ixDisplay);
        
        val = get_val_ij(that, [], ixDisplay);
        y = repmat(y', 1, nbY);
        Old = val;
        
        switch TypeInteraction
            case 1 % Rectangle
                H = getrect(hAxe);
                if isempty(H)
                    message_rectangle
                    return
                end
                pX = [H(1) H(1)+H(3)];
                pY = [H(2) H(2)+H(4)];
                Masque = ((y >= min(pY)) & (y <= max(pY)) & (val >= min(pX)) & (val <= max(pX)));
                val(Masque) = newVal;
                
            case 2 % Free hand
                nbY = 200;
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                imfreehandH = imfreehand(hAxe);
                %         wait(imfreehandH)
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                %         wait(imfreehandH)
                X = getPosition(imfreehandH);
                if isempty(X)
                    message_patch
                    return
                end
                pX = X(:,1);
                pY = X(:,2);
                %         figure; plot(pX, pY); grid on
                [val, Old] = set_valY(that, val, Old, pX, pY, nbY);
                
            case 3 % Polyline
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                [pX, pY] = inputPolyline;
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                Masque = ((y >= min(pY)) & (y <= max(pY)) & (val >= min(pX)) & (val <= max(pX)));
                val(Masque) = newVal;

            case 4 % Rubber
                GommeYDown(this)                
                set(this.hfig, 'WindowButtonUpFcn',   @(src,event)GommeYUp(this, src, event));
                set(this.hfig, 'WindowButtonDownFcn', @(src,event)GommeYDown(this, src, event));
                return
                
            case 5 % Rectangle autour du Point&Click
                messgae5
                % TODO : devra remplacer traiter_click_NaN quand IHM sera
                % compl�t�
                
        end
        
        that = set_val(that, [], ixDisplay, val);
        this.Images(this.indImage) = that;
        this = show_image(this);
        this = show_profils(this);
        
        undo = validateDataCleaning;
        if undo
            that = set_val(that, [], ixDisplay, Old);
            this.Images(this.indImage) = that;
            this = show_image(this);
            this = show_profils(this);
        end
    end


    function this = cleanDataAxeYMeanImage(this, cross, TypeInteraction, hAxe)
        set(this.hfig, 'pointer', 'crosshair')
        that = this.Images(this.indImage);
        
%         ValNaN = get(that, 'ValNaN');
        % x = get(that, 'x');
        y = get(that, 'y');
        
        [~, ideb] = get_val_xy(that, this.visuLim(this.indImage,1), cross(2));
        [~, ifin] = get_val_xy(that, this.visuLim(this.indImage,2), cross(2));
        
        ixDisplay = ideb:ifin;
        nbY = length(ixDisplay);
        
        val = get_val_ij(that, [], ixDisplay);
        y = repmat(y', 1, nbY);
        Old = val;
        
        switch TypeInteraction
            case 1 % Rectangle
                H = getrect(hAxe);
                if isempty(H)
                    message_rectangle
                    return
                end
                pX = [H(1) H(1)+H(3)];
                pY = [H(2) H(2)+H(4)];
                valMean = mean(val, 2, 'omitnan');
                valMean = repmat(valMean, 1, nbY);
                Masque = ((y >= min(pY)) & (y <= max(pY)) & (valMean >= min(pX)) & (valMean <= max(pX)));
                val(Masque) = newVal;
                
            case 2 % Free hand
                nbY = 200;
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                imfreehandH = imfreehand(hAxe);
                %         wait(imfreehandH)
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                %         wait(imfreehandH)
                X = getPosition(imfreehandH);
                if isempty(X)
                    message_patch
                    return
                end
                pX = X(:,1);
                pY = X(:,2);
                %         figure; plot(pX, pY); grid on
                valMean = mean(val, 2, 'omitnan');
                valMean = repmat(valMean, 1, size(val,2));
                [val, Old] = set_valYMean(that, valMean, val, Old, pX, pY, nbY);
                
            case 3 % Polyline
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                [pX, pY] = inputPolyline;
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                valMean = mean(val, 2, 'omitnan');
                valMean = repmat(valMean, 1, nbY);
                Masque = ((y >= min(pY)) & (y <= max(pY)) & (valMean >= min(pX)) & (valMean <= max(pX)));
                val(Masque) = newVal;

            case 4 % Rubber
                GommeYDown(this)                
                set(this.hfig, 'WindowButtonUpFcn',   @(src,event)GommeYUp(this, src, event));
                set(this.hfig, 'WindowButtonDownFcn', @(src,event)GommeYDown(this, src, event));
                return
                
            case 5 % Rectangle autour du Point&Click
                messgae5
                % TODO : devra remplacer traiter_click_NaN quand IHM sera
                % compl�t�
                
        end
        
        that = set_val(that, [], ixDisplay, val);
        this.Images(this.indImage) = that;
        this = show_image(this);
        this = show_profils(this);
        
        undo = validateDataCleaning;
        if undo
            that = set_val(that, [], ixDisplay, Old);
            this.Images(this.indImage) = that;
            this = show_image(this);
            this = show_profils(this);
        end
    end

    function this = cleanDataAxeYSignal(this, cross, TypeInteraction, hAxe)
        set(this.hfig, 'pointer', 'crosshair')
        that = this.Images(this.indImage);
        
        y = get(that, 'y');
        
        [~, ix] = get_val_xy(that, cross(1), cross(2));
        
        ixDisplay = ix;
        val = get_val_ij(that, [], ixDisplay);
        
        [flag, X] = getValSignalIfAny(that, this.profilY.Type, 'val', val);
        if flag
            val = X;
        else
            this.profilY.Type = 'Image profile';
        end

        y = y';
        Old = val;
        
        switch TypeInteraction
            case 1 % Rectangle
                H = getrect(hAxe);
                if isempty(H)
                    message_rectangle
                    return
                end
                pX = [H(1) H(1)+H(3)];
                pY = [H(2) H(2)+H(4)];
                Masque = ((y >= min(pY)) & (y <= max(pY)) & (val >= min(pX)) & (val <= max(pX)));
                val(Masque) = newVal;
                
            case 2 % Free hand
                nbY = 200;
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                imfreehandH = imfreehand(hAxe);
                %         wait(imfreehandH)
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                %         wait(imfreehandH)
                X = getPosition(imfreehandH);
                if isempty(X)
                    message_patch
                    return
                end
                pX = X(:,1);
                pY = X(:,2);
                %         figure; plot(pX, pY); grid on
                [val, Old] = set_valY(that, val, Old, pX, pY, nbY);
                
            case 3 % Polyline
                msgCallBack = get(this.hfig, 'WindowButtonDownFcn');
                set(this.hfig, 'WindowButtonDownFcn', []);
                [pX, pY] = inputPolyline;
                set(this.hfig, 'WindowButtonDownFcn', msgCallBack);
                Masque = ((y >= min(pY)) & (y <= max(pY)) & (val >= min(pX)) & (val <= max(pX)));
                val(Masque) = newVal;

            case 4 % Rubber
                GommeYDown(this)                
                %{
                % Test pour enlever le zoom perturbateur
this.ihm.zoom.cpn = set(this.ihm.zoom.cpn, 'action', 'Out');
this.ihm.zoom.cpn = desactiver(this.ihm.zoom.cpn);
this = placer_callback_curseur(this);
                %}
%                 set(this.hfig, 'pointer', 'circle') % Test
%                 set(this.hfig, 'WindowButtonMotionFcn', @(src,event)GommeX(this, src, event));

                set(this.hfig, 'WindowButtonUpFcn',   @(src,event)GommeYUp(this, src, event));
                set(this.hfig, 'WindowButtonDownFcn', @(src,event)GommeYDown(this, src, event));
                return
                
            case 5 % Rectangle autour du Point&Click
                messgae5
                % TODO : devra remplacer traiter_click_NaN quand IHM sera
                % compl�t�
                
        end
        
        suby = 1:length(y);
        subNaN    = find(isnan(val));
        subNonNaN = find(~isnan(val));
        val(subNaN) = my_interp1(subNonNaN, val(subNonNaN), subNaN);
        [~, this] = setValSignalIfAny(this, val, suby);

        this = show_image(this);
        this = show_profils(this);
        
        undo = validateDataCleaning;
        if undo
            [~, this] = setValSignalIfAny(this, Old, suby);
            this = show_image(this);
            this = show_profils(this);
        end
    end


    function message_rectangle
        str1 = 'Re-cliquez pour donner le focus et ensuite cliquez sur le d�part de votre rectangle tout en gardant la souris enfonc�e et d�placez la souris pour d�limiter le rectangle.';
        str2 = 'Click again on the figure to give the focus and then click on the first point of your patch and move the mous without releasing the left button.';
        my_warndlg(Lang(str1,str2), 1);
    end

    function message_patch
        str1 = 'Re-cliquez pour donner le focus et ensuite cliquez sur le d�part de votre patch tout en gardant la souris enfonc�e et d�ssinez moi un mouton.';
        str2 = 'Click again on the figure to give the focus and then click on the first point of your patch and move the mous without releasing the left button.';
        my_warndlg(Lang(str1,str2), 1);
    end

    function undo = validateDataCleaning
        str1 = 'Valider l''op�ration ? (Espace ou Return = Oui)';
        str2 = 'Validation ? (Space or Return = Yes)';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        undo = ~flag || (rep == 2);
    end

    function val = set_valX(that, val, pX, pY, nbY)
        %         % TODO : trouver comment faire simplement la d�termination des
        %         % points qui sont dans le patch. Tout ce qui suit est une merde
        %         % sans nom
        x = that.x;
        minPY = min(pY);
        maxPY = max(pY);
        y1 = linspace(minPY, maxPY, size(val,2));
        Masque = roipoly(x, y1, zeros(nbY, length(x), 'uint8'), [pX; pX(1)], [pY; pY(1)]);
        Masque = imdilate(Masque, strel('rectangle', [3 1]));
        
        for k=1:length(x)
            ival = 1 + floor((double(val(:,k))-minPY) * (nbY-1) / (maxPY-minPY));
            ival(ival < 1) = NaN;
            ival(ival > nbY) = NaN;
            subNonNaN = find(~isnan(ival));
            if ~isempty(subNonNaN)
                pppp = Masque(ival(subNonNaN),k);
                val(subNonNaN(pppp),k) = newVal;
            end
        end
    end

    function [val, Old] = set_valY(that, val, Old, pX, pY, nbY)
        y1 = that.y;
        %         suby1 = find((y1 >= min(pY)) & (y1 <= max(pY)));
        minPX = min(pX);
        maxPX = max(pX);
        x = linspace(minPX, maxPX, size(val,1));
        Masque = roipoly(x, y1, zeros(length(x), nbY, 'uint8'), [pX; pX(1)], [pY; pY(1)]);
        Masque = imdilate(Masque, strel('rectangle', [1 3]));
        
        for k=1:length(y1)
            ival = 1 + floor((double(val(k,:))-minPX) * (nbY-1) / (maxPX-minPX));
            ival(ival < 1) = NaN;
            ival(ival > nbY) = NaN;
            subNonNaN = find(~isnan(ival));
            if ~isempty(subNonNaN)
                pppp = Masque(k,ival(subNonNaN));
                Old(k,subNonNaN(pppp)) = val(k,subNonNaN(pppp));
                val(k,subNonNaN(pppp)) = newVal;
            end
        end
    end

    function [val, Old] = set_valYMean(that, valMean, val, Old, pX, pY, nbY)
        y1 = that.y;
        %         suby1 = find((y1 >= min(pY)) & (y1 <= max(pY)));
        minPX = min(pX);
        maxPX = max(pX);
        x = linspace(minPX, maxPX, size(val,1));
        Masque = roipoly(x, y1, zeros(length(x), nbY, 'uint8'), [pX; pX(1)], [pY; pY(1)]);
        Masque = imdilate(Masque, strel('rectangle', [1 3]));
        
        for k=1:length(y1)
            ival = 1 + floor((double(valMean(k,:))-minPX) * (nbY-1) / (maxPX-minPX));
            ival(ival < 1) = NaN;
            ival(ival > nbY) = NaN;
            subNonNaN = find(~isnan(ival));
            if ~isempty(subNonNaN)
                pppp = Masque(k,ival(subNonNaN));
                Old(k,subNonNaN(pppp)) = val(k,subNonNaN(pppp));
                val(k,subNonNaN(pppp)) = newVal;
            end
        end
    end

    function [pX, pY] = inputPolyline
        
%       [~, pX, pY] = roipoly; % Ne fonctionne pas si il n'y a pas d'image
        
        % TODO : GLU : essayer d'avoir le m�me comportement que pour la polyline
        % sur l'image (roipoly) (voir aussi getpts)
        but = 1;
        n = 0;
        pX = [];
        pY = [];
        hold on;
        hPoly = [];
        while but == 1
%             [xi, yi, but] = my_ginput(1); % my_ginput supprim� le 22/09/2020
            [xi, yi, but] = ginput(1);
            n = n+1;
            pX(n,1) = xi; %#ok<AGROW>
            pY(n,1) = yi; %#ok<AGROW>
            if ishandle(hPoly)
                set(hPoly, 'XData', [pX; pX(1)], 'YData', [pY; pY(1)])
            else
                hPoly = plot(pX, pY, '-ok');
            end
        end
        pause(0.2)
        if ishandle(hPoly)
            delete(hPoly)
        end
    end

    function GommeX(this, src, event) %#ok<INUSD>
        identAxe = test_curseur_sur_axes(this);
        if ~identAxe(2)
            return
        end
        
        button = get(this.hfig, 'SelectionType');
        if ~strcmp(button, 'normal')
%             WindowButtonMotionFcnBefore
            set(this.hfig, 'WindowButtonMotionFcn', WindowButtonMotionFcnBefore);
            set(this.hfig, 'WindowButtonDownFcn',   WindowButtonDownFcnBefore);
            set(this.hfig, 'WindowButtonUpFcn',     WindowButtonUpFcnBefore);
            set(this.hfig, 'pointer', 'arrow')
        end
        
        figure(this.hfig)
        GCA = gca;
        XLim = get(GCA, 'XLim');
        YLim = get(GCA, 'YLim');
        
        %% On prend la position cliqu�e
        
        positionCliquee = get(GCA, 'CurrentPoint');
        x = positionCliquee(1,1);
        y = positionCliquee(1,2);
        
        hc = get(GCA, 'Children');
        hc = setdiff(hc, this.hCroixProfilX);

        for k=1:length(hc)
            XData = get(hc(k), 'XData');
            YData = get(hc(k), 'YData');
            X = XData(:);
            Y = YData(:);
            
            if isempty(X)
                continue
            end
            if length(X) ~= length(Y)
                continue
            end
            
            deltaX = (X - x) / (XLim(end)-XLim(1));
            deltaY = (Y - y) / (YLim(end)-YLim(1));
            
            d = deltaX.^2 + deltaY.^2;
            
            %% Recherche des points contenus dans un rayon limite
            
            subInXY = find(d < 5e-4); % 0.03^2);
            if isempty(subInXY)
                continue
            end
            
            XData(subInXY) = newVal;
            YData(subInXY) = newVal;
            
            set(hc(k), 'XData', XData);
            set(hc(k), 'YData', YData);
        end
    end


    function GommeY(this, src, event) %#ok<INUSD>
        identAxe = test_curseur_sur_axes(this);
        if ~identAxe(3)
            return
        end
        
        button = get(this.hfig, 'SelectionType');
        if ~strcmp(button, 'normal')
            set(this.hfig, 'WindowButtonMotionFcn', WindowButtonMotionFcnBefore);
            set(this.hfig, 'WindowButtonDownFcn',   WindowButtonDownFcnBefore);
            set(this.hfig, 'WindowButtonUpFcn',     WindowButtonUpFcnBefore);
            set(this.hfig, 'pointer', 'arrow')
        end
        
        figure(this.hfig)
        GCA = gca;
        XLim = get(GCA, 'XLim');
        YLim = get(GCA, 'YLim');
        
        %% On prend la position cliqu�e
        
        positionCliquee = get(GCA, 'CurrentPoint');
        x = positionCliquee(1,1);
        y = positionCliquee(1,2);
        
        hc = get(GCA, 'Children');
        hc = setdiff(hc, this.hCroixProfilY);

        for k=1:length(hc)
            XData = get(hc(k), 'XData');
            YData = get(hc(k), 'YData');
            X = XData(:);
            Y = YData(:);
            
            if isempty(X)
                continue
            end
            if length(X) ~= length(Y)
                continue
            end
            
            deltaX = (X - x) / (XLim(end)-XLim(1));
            deltaY = (Y - y) / (YLim(end)-YLim(1));
            
            d = deltaX.^2 + deltaY.^2;
            
            %% Recherche des points contenus dans un rayon limite
            
            subInXY = find(d < 5e-4); % 0.03^2);
            if isempty(subInXY)
                continue
            end
            
            XData(subInXY) = newVal;
            YData(subInXY) = newVal;
            
            set(hc(k), 'XData', XData);
            set(hc(k), 'YData', YData);
        end
    end

    function GommeImage(this, src, event) %#ok<INUSD>
        
        identAxe = test_curseur_sur_axes(this);
        if ~identAxe(1)
            return
        end
        
        button = get(this.hfig, 'SelectionType');
        if ~strcmp(button, 'normal')
%         if ~strcmp(button, 'alt')
            set(this.hfig, 'WindowButtonMotionFcn', WindowButtonMotionFcnBefore);
            set(this.hfig, 'WindowButtonDownFcn',   WindowButtonDownFcnBefore);
            set(this.hfig, 'WindowButtonUpFcn',     WindowButtonUpFcnBefore);
            set(this.hfig, 'pointer', 'arrow')
            return
        end
        
        figure(this.hfig)
        GCA = gca;
        XLim = get(GCA, 'XLim');
        YLim = get(GCA, 'YLim');
        
        %% On prend la position cliqu�e
        
        positionCliquee = get(GCA, 'CurrentPoint');
        xMouse = positionCliquee(1,1);
        yMouse = positionCliquee(1,2);

        scalex = 0.01;
        scaley = 0.01;
        
        GCO = findobj(GCA, 'Type', 'Image');
        x = get(GCO, 'x');
        y = get(GCO, 'y');
        
        deltax = (XLim(2) - XLim(1)) * scalex;
        deltay = (YLim(2) - YLim(1)) * scaley;
        
        subx = find((x >= (xMouse-deltax)) & (x <= (xMouse+deltax)));
        suby = find((y >= (yMouse-deltay)) & (y <= (yMouse+deltay)));
        
        CData = get(GCO, 'CData');
        CData(suby,subx) = newVal; %#ok<FNDSB>
        set(GCO, 'CData', CData);
    end

    function GommeXDown(this, src, event) %#ok<INUSD>
        set(this.hfig, 'pointer', 'circle')
        set(this.hfig, 'WindowButtonMotionFcn', @(src,event)GommeX(this, src, event));
    end

    function GommeYDown(this, src, event) %#ok<INUSD>
        set(this.hfig, 'pointer', 'circle')
        set(this.hfig, 'WindowButtonMotionFcn', @(src,event)GommeY(this, src, event));
    end

    function GommeImageDown(this, src, event) %#ok<INUSD>
        set(this.hfig, 'pointer', 'circle')
        set(this.hfig, 'WindowButtonMotionFcn', @(src,event)GommeImage(this, src, event));%, ...
%              'Interruptible', 'Off', 'BusyAction', 'cancel');
    end

    function GommeXUp(this, src, event) %#ok<INUSD>
        set(this.hfig, 'pointer', 'arrow')
        msgCallBack = built_cb(this, this.msgMoveCurseur);
        set(this.hfig, 'WindowButtonMotionFcn', msgCallBack);
        cli_image.callback('GetModifiedImageAxeX');
        callback_CleanData(this);
        set(this.hfig, 'pointer', 'arrow') % test
    end

    function GommeYUp(this, src, event) %#ok<INUSD>
        set(this.hfig, 'pointer', 'arrow')
        msgCallBack = built_cb(this, this.msgMoveCurseur);
        set(this.hfig, 'WindowButtonMotionFcn', msgCallBack);
        cli_image.callback('GetModifiedImageAxeY');
        callback_CleanData(this);
        set(this.hfig, 'pointer', 'arrow') % test
    end

    function GommeImageUp(this, src, event) %#ok<INUSD>
        set(this.hfig, 'pointer', 'arrow')
        msgCallBack = built_cb(this, this.msgMoveCurseur);
        set(this.hfig, 'WindowButtonMotionFcn', msgCallBack);
        cli_image.callback('GetModifiedImageAxeImage');
        set(this.hfig, 'pointer', 'arrow') % test
    end

    function messgae5
        my_warndlg('Not available yet, will replace Set NaN and SetVal.', 1);
    end
end
