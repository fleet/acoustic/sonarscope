% Trac� d'une hauteur sur donn�e sonar Lat�ral ou subbottom ou sismique
%
% Syntax
%   this = traiter_click_Height(this)
%
% Input Arguments
%   this      : Instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = traiter_click_Height(this, varargin)

[varargin, X] = getPropertyValue(varargin, 'X', []);
[varargin, Y] = getPropertyValue(varargin, 'Y', []); %#ok<ASGLU>

%% Determination si le curseur est sur l'un des 3 axes

flag = test_curseur_sur_axes(this);
if ~flag(1)
    return
end

if ~isempty(this.coupe.handles)
    for i=1:length(this.coupe.handles)
        if ishandle(this.coupe.handles(i))
            delete(this.coupe.handles(i))
        end
    end
    this.coupe.handles = [];
end

if ~isempty(this.coupe.handlesPatch)
    for i=1:length(this.coupe.handlesPatch)
        if ishandle(this.coupe.handlesPatch(i))
            delete(this.coupe.handlesPatch(i))
        end
    end
    this.coupe.handlesPatch = [];
end

if isempty(X)
    [X,Y] = getline(gca);
else
    X = X{1}(:);
    Y = Y{1}(:);
end

if length(X) <= 1
    return
end

%% Cr�ation de la fen�tre de saisie des coordonn�es

this.coupe.X = X;
this.coupe.Y = Y;

% NbVoisins = this.NbVoisins;

H = get(this.Images(this.indImage), 'SonarHeight');
y = this.Images(this.indImage).y;
suby = find((y >= min(this.coupe.Y)) & (y<= max(this.coupe.Y)));
x = my_interp1(this.coupe.Y, this.coupe.X, y(suby));
H(suby) = -abs(x);

this.Images(this.indImage) = set(this.Images(this.indImage), 'SonarHeight', H);

this = show_profils(this);
