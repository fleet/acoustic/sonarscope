% Traitement des deplacements du curseur
%
% Syntax
%   this = traiter_click_NaN(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = traiter_click_NaN(this, flag, positionCurseur, positionAxePrincipal, EraseWindow, EraseAngle)

%% Détermination si le curseur est sur l'un des 3 axes

if ~any(flag)
    return
end

cross = this.cross(this.indImage,:);

%% Position en X

if flag(1) || flag(2)
    if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3))

        % ------------------------------------------------------
        % Détermination des limites de l'axe principal en x et y

        limites = axis(this.hAxePrincipal);

        % ---------------------------
        % Détermination de l'abscisse

        if this.Images(this.indImage).XDir == 1
            cross(1) = limites(1) + (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        else
            cross(1) = limites(2) - (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        end
    end
end

%% Position en Y

if flag(1) || flag(3)
    if (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))

        % ------------------------------------------------------
        % Détermination des limites de l'axe principal en x et y

        limites = axis(this.hAxePrincipal);

        % ---------------------------
        % Détermination de l'ordonnee

        if this.Images(this.indImage).YDir == 1
            cross(2) = limites(3) + (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        else
            cross(2) = limites(4) - (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        end
    end
end

%% Mise ŕ NaN de la valeur

[val, ix, iy] = get_val_xy(this.Images(this.indImage), cross(1), cross(2)); %#ok<ASGLU>
% ValNaN = get(this.Images(this.indImage), 'ValNaN');

H = floor(EraseWindow(1) / 2);
L = floor(EraseWindow(2) / 2);

nbColumns = this.Images(this.indImage).nbColumns;
nbRows     = this.Images(this.indImage).nbRows;

c = cosd(-EraseAngle);
s = sind(-EraseAngle);
Rot = [c s; -s c];
if H == 0
    H = 0.5;
end
if L == 0
    L = 0.5;
end
C = Rot * [L L -L-0.5 -L-0.5 L;-H H H -H -H] + repmat([ix;iy], 1, 5);
C = floor(C);
pX = C(1,:) + 1;
pY = C(2,:) + 1;
maskImage = poly2mask(pX-min(pX), pY-min(pY), max(pY)-min(pY)+1, max(pX)-min(pX)+1);
% figure; imshow(uint8(maskImage)*255);
ix = min(pX):max(pX);
iy = min(pY):max(pY);
subx = find((ix >= 1) & (ix <= nbColumns));
ix = ix(subx);
suby = find((iy >= 1) & (iy <= nbRows));
iy = iy(suby);
val = get_val_ij(this.Images(this.indImage), iy, ix);
maskImage = maskImage(suby,subx);
%     figure; imagesc(val); axis equal; axis tight;
% whos maskImage val
val(maskImage) = NaN;
% figure; imshow(val*20);
this.Images(this.indImage) = set_val(this.Images(this.indImage), iy, ix, val);

%% Affichage des profils et des valeurs

this = show_image(this);
this = show_profils(this);

idle(this.hfig)
