% Traitement des deplacements du curseur
% 
% Syntax
%   this = traiter_click_centre(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = traiter_click_centre(this)

%% Détermination si le curseur est sur l'un des 3 axes

[flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);
if ~any(flag)
    return
end

X = this.cross(this.indImage, 1);
Y = this.cross(this.indImage, 2);

%% Position en X

if flag(1) || flag(2)
    if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3))

        % ------------------------------------------------------
        % Determination des limites de l'axe principal en x et y

        limites = axis(this.hAxePrincipal);

        % ---------------------------
        % Determination de l'abscisse

        if this.Images(this.indImage).XDir == 1
            X = limites(1) + (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        else
            X = limites(2) - (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        end
    end
end

%% Position en Y

if flag(1) || flag(3)
    if (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))

        % ------------------------------------------------------
        % Determination des limites de l'axe principal en x et y

        limites = axis(this.hAxePrincipal);

        % ---------------------------
        % Determination de l'ordonnee

        if this.Images(this.indImage).YDir == 1
            Y = limites(3) + (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        else
            Y = limites(4) - (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        end
    end
end

XLimDebut = this.visuLim(this.indImage, 1:2);
XLim = XLimDebut + (X - sum(XLimDebut) / 2);
% XLimImage = get(this.Images(this.indImage), 'XLim');
x = get(this.Images(this.indImage), 'x');
pasx = abs(x(2) - x(1));
demi_pasx = pasx / 2;XLimImage = [x(1) x(end)];
if XLim(2) > XLimImage(2)
    XLim = XLim - (XLim(2) - XLimImage(2)) + demi_pasx;
end
if XLim(1) < XLimImage(1)
    XLim = XLim + (XLimImage(1) - XLim(1) - demi_pasx);
end
this.visuLim(this.indImage, 1:2) = XLim;

YLimDebut = this.visuLim(this.indImage, 3:4);
YLim = YLimDebut + (Y - sum(YLimDebut) / 2);
y = get(this.Images(this.indImage), 'y');
pasy = abs(y(2) - y(1));
demi_pasy = pasy / 2;YLimImage = [y(1) y(end)];
if diff(YLimImage) > 0
    if YLim(2) > YLimImage(2)
        YLim = YLim - (YLim(2) - YLimImage(2)) + demi_pasy;
    end
    if YLim(1) < YLimImage(1)
        YLim = YLim + (YLimImage(1) - YLim(1)) - demi_pasy;
    end
else
    if YLim(2) > YLimImage(1)
        YLim = YLim - (YLim(2) - YLimImage(1)) + demi_pasy;
    end
    if YLim(1) < YLimImage(2)
        YLim = YLim + (YLimImage(2) - YLim(1)) - demi_pasy;
    end
end

this.visuLim(this.indImage, 3:4) = YLim;
this.visuLimHistory{this.indImage}{end+1} = this.visuLim(this.indImage, :);
this.visuLimHistoryPointer(this.indImage) = length(this.visuLimHistory{this.indImage});

%% Affichage des profils et des valeurs si les limites ont change

if ~isequal(XLimDebut, XLim) || ~isequal(YLimDebut, YLim)
    set(this.hfig, 'CurrentAxes', this.hAxePrincipal);
    axis(this.visuLim(this.indImage, :))
    
    this = show_profils(this);

    % -------------------------------
    % Traitement des synchronisations

    this = process_synchroX(this);
    this = process_synchroY(this);
    
    % Contournement d'un bug matlab
%     disp('traiter_click_centre : ici on fait une bidouille pour eviter un bug matlab')
    this = callback_zoomDirect(this, 'ZoomPrecedent');
    this = callback_zoomDirect(this, 'ZoomSuivant');
end
