% Traitement des d�placements du curseur
%
% Syntax
%   this = traiter_click_coupe(this)
%
% Input Arguments
%   this      : Instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = traiter_click_coupe(this, varargin)

%# function derivateCourbe
%  function traiter_signal

    function saveTable(Fig, varargin)
        h = findobj(Fig, 'Type', 'uitable');
        Data = get(h, 'Data');
    end

    function closeFig(h, varargin)
        Fig = get(h, 'Parent');
        my_close(Fig)
    end

    function exportTable(Fig, varargin)
        Fig = get(get(Fig, 'Parent'), 'Parent');
        h = findobj(Fig, 'Type', 'uitable');
        Data = get(h, 'Data');
        
        filtre = fullfile(this.repExport, 'Polyline.csv');
        [flag, nomFic] = my_uiputfile('*.csv', 'Give a file name', filtre);
        if ~flag
            return
        end
        [pathname, nomFic] = fileparts(nomFic);
        this.repExport = pathname;
        nomFicCsv = fullfile(pathname, [nomFic '.csv']);
        this.repExport = pathname;
        
        N = size(Data,1);
        str = cell(N,2);
        for k1=1:N
            str{k1,1} = num2strPrecis(Data(k1,1));
            str{k1,2} = num2strPrecis(Data(k1,2));
        end
        flag = my_csvwrite(nomFicCsv, str);
        if ~flag
            %     my_warndlg(mes.message, 1);
        end
    end

    function importTable(Fig, varargin)
        Fig = get(get(Fig, 'Parent'), 'Parent');
        filtre = fullfile(this.repExport, 'Polyline.csv');
        [flag, nomFicCsv] = my_uigetfile('*.csv', 'Give a file name', filtre);
        if ~flag
            return
        end
        pathname = fileparts(nomFicCsv);
        this.repImport = pathname;
        Data = csvread(nomFicCsv);
        Data = Data(:,1:2);
        h = findobj(Fig, 'Type', 'uitable');
        set(h, 'Data', Data);
        set(Fig, 'Position', [100 100 400 (size(Data,1)+1)*23])
    end

[varargin, X] = getPropertyValue(varargin, 'X', []);
[varargin, Y] = getPropertyValue(varargin, 'Y', []); %#ok<ASGLU>

%% Determination si le curseur est sur l'un des 3 axes

flag = test_curseur_sur_axes(this);
if ~flag(1)
    return
end

if ~isempty(this.coupe.handles)
    for k=1:length(this.coupe.handles)
        if ishandle(this.coupe.handles(k))
            delete(this.coupe.handles(k))
        end
    end
    this.coupe.handles = [];
end

if ~isempty(this.coupe.handlesPatch)
    for k=1:length(this.coupe.handlesPatch)
        if ishandle(this.coupe.handlesPatch(k))
            delete(this.coupe.handlesPatch(k))
        end
    end
    this.coupe.handlesPatch = [];
end

if isempty(X)
    flagInteractif = 1;
    [X,Y] = getline(gca);
else
    X = X{1}(:);
    Y = Y{1}(:);
    flagInteractif = 0;
end

if length(X) <= 1
    return
end

% {
%% Cr�ation de la fen�tre de saisie des coordonn�es

Data = [Y X];

if flagInteractif
    Position = centrageFig(500, (length(X)+1)*23);
    str1 = ' - Coordonn�es des sommets de la ligne bris�e';
    str2 = ' - Coordinates of the polyline summits.';
    Fig = figure('Name', Lang(str1,str2), 'Position', Position, 'DeleteFcn', @saveTable);
    delete(get(Fig, 'Children'))
    
    f = uimenu('Text', 'SonarScope', 'Tag', 'sdfsdf');
    uimenu(f, 'Text', 'Export', 'Callback', @exportTable);
    uimenu(f, 'Text', 'Import', 'Callback', @importTable);
    
    % columnname =   {'X', 'Y'};
    GeometryType = this.Images(this.indImage).GeometryType;
    switch GeometryType
        case 3 % LatLong
            YLabel = 'Longitude';
            XLabel = 'Latitude';
        otherwise
            strGeometryType = get(this.Images(this.indImage), 'strGeometryType');
            strGeometryType = strGeometryType{GeometryType};
            if strcmp(strGeometryType(1:4), 'Ping')
                YLabel = strGeometryType(5:end);
                XLabel = 'Ping';
            else
                YLabel = this.Images(this.indImage).YLabel;
                XLabel = this.Images(this.indImage).XLabel;
            end
    end
    
    columnname = {XLabel YLabel};
    % columnformat = {'numeric', 'numeric'};
    columneditable = [true true];
    uitable('Units', 'normalized', 'Position',...
        [0.1 0.1 0.9 0.9], 'Data', Data,...
        'ColumnName', columnname,...
        'ColumnEditable', columneditable);
    
    str1 = 'Cliquez ici pour r�aliser le trac�.';
    str2 = 'Click here to plot the signal.';
    uicontrol(Fig, 'Style', 'pushbutton',...
        'String', Lang(str1,str2),...
        'Position', [250 20 250 length(X)*23],...
        'Callback', @closeFig);
    uiwait(Fig)
    % }
    
    %% Traitement de la polyline
    
    X = Data(:,2);
    Y = Data(:,1);
end

this.coupe.X = X;
this.coupe.Y = Y;

NbVoisins = this.NbVoisins;

[flag, indLayers, nomsLayers] = listeLayersSynchronises(this.Images, this.indImage, ...
    'ExcludeDataType', cl_image.indDataType('SunColor'), 'IncludeCurrentImage');
if ~flag
    return
end
if isempty(indLayers)
    return
end

for k=1:length(nomsLayers)
    nomsLayers{k} = rmblank(cl_image.extract_ImageNameTime(nomsLayers{k}));
end

% indLayers = [this.indImage indLayers];
% nomsLayers =  [this.Images(this.indImage).Name;  nomsLayers];
if length(indLayers) > 1
    ItemsOnRightPanel = find(indLayers == this.indImage);
    str1 = 'Choisissez d''autres images pour lesquelles vous voulez un trac� de profil.';
    str2 = 'Select other layers if you want profiles on them.';
    [flag, ~, choixLayers] = uiSelectItems('ItemsToSelect', nomsLayers, 'Titre', ['IFREMER - SonarScope : ' Lang(str1,str2)], ...
        'InitValue', [], 'ItemsOnRightPanel', ItemsOnRightPanel);
    if ~flag
        return
    end
else
    choixLayers = 1;
end
indLayers = indLayers(choixLayers);
nomLayersInvite = nomsLayers(choixLayers);
NInvites = length(indLayers);
nbPix = [];
for iInvite=1:NInvites
    [valInvite{iInvite}, xInvite{iInvite}, ~, ~, ~, ~, XPixels{iInvite}, YPixels{iInvite}, nbPix] = ...
        get_profile_xy(this.Images(indLayers(iInvite)), this.coupe.X, this.coupe.Y, NbVoisins, 'nbPix', nbPix); %#ok<AGROW>
    DataType(iInvite ) = this.Images(indLayers(iInvite)).DataType; %#ok<AGROW>
end

%% Faut-il regrouper les profils de m�me DataType sur les m�mes axes ?

uniqueDataType = unique(DataType);
nbDataTypes = length(uniqueDataType);
if nbDataTypes ~= NInvites
    str1 = 'Voulez-vous regrouper les profils de m�me type de donn�e sur un axe unique ?';
    str2 = 'Do you want to plot the profiles of same dataType onto the same axis ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    SameAxisForSameDtaType = (rep == 1);
else
    SameAxisForSameDtaType = false;
end

if SameAxisForSameDtaType
    nbAxis = nbDataTypes;
else
    nbAxis = NInvites;
end

%% Affichage

% TODO

[valV, x, longueur, Azimuth, distance_m, Cadre] = get_profile_xy(this.Images(this.indImage), this.coupe.X, this.coupe.Y, NbVoisins, 'nbPix', nbPix);
if length(valV{1}) <= 1
    return
end

this.coupe.fig = FigUtils.createSScFigure;

XUnit = this.Images(this.indImage).XUnit;
YUnit = this.Images(this.indImage).YUnit;
% ImageName = this.Images(this.indImage).Name;
str = nomLayersInvite{1};
if isempty(distance_m)
    if length(x) == 1
        str = sprintf('%s\nLength : %f  Azimuth : %f  Begin : [%s]  End : [%s]', ...
            str, longueur, Azimuth, num2str(this.coupe.X'), num2str(this.coupe.Y'));
    else
        str = sprintf('%s\nLength : %f', str, sum(longueur));
    end
    UniteLength = '';
else
    if length(x) == 1
        str = sprintf('%s\nLength (deg) : %s  Length (m) : %s  Azimuth : %s  X : [%s]  Y : [%s]', ...
            str, num2str(sum(longueur)), num2str(sum(distance_m)), num2str(Azimuth), num2str(this.coupe.X'), num2str(this.coupe.Y'));
    else
        str = sprintf('%s\nLength (deg) : %s  Length (m) : %s', ...
            str, num2str(sum(longueur)), num2str(sum(distance_m)));
    end
    UniteLength = '(m)';
end

flagFilled = false(nbAxis, 1);
xFin = 0;
haxe = preallocateGraphics(0,0);
NSegm = length(x);
str1 = 'Trac� des segments';
str2 = 'Plotting segments';
hw1 = create_waitbar(Lang(str1,str2), 'N', NSegm);
for iSegm=1:NSegm
    % TODO : a reprendre quand multi_waitbar sera plus sophistiqu�
    %     waitbar_mult(iSegm/N, axw(1), {sprintf('Finished computing part %d',iSegm)});
    my_waitbar(iSegm, NSegm, hw1)
    
    FigUtils.createSScFigure(this.coupe.fig);
    haxe(1) = subplot(nbAxis, 1, 1);
    ColorOrder = get(gca, 'ColorOrder');
    
    %     cmenu = uicontextmenu;
    hold on;
    iColor = 1 + mod(iSegm-1, size(ColorOrder, 1));
    %     hCourbe = PlotUtils.createSScPlot(x{iSegm}+xFin, valV{iSegm}, 'Color', ColorOrder(iColor,:));
    %     hCourbe = PlotUtils.createSScPlot(x{iSegm}+xFin, valInvite{1}{iSegm}, 'Color', ColorOrder(iColor,:));
    if ~isempty(valInvite{1}{iSegm})
        flagFilled(1) = flagFilled(1) | ~all(isempty(valInvite{1}{iSegm}));
        hCourbe = PlotUtils.createSScPlot(xInvite{1}{iSegm}+xFin, valInvite{1}{iSegm}, 'Color', ColorOrder(iColor,:));
                
        CoordPoints.Name       = nomLayersInvite{1};
        CoordPoints.indCurve   = 1;
        CoordPoints.indSegment = iSegm;
        GT = this.Images(this.indImage).GeometryType;
        switch GT
            case cl_image.indGeometryType('LatLong')
                CoordPoints.Latitude  = YPixels{1}{iSegm};
                CoordPoints.Longitude = XPixels{1}{iSegm};
                Carto = get_Carto(this.Images(this.indImage));
                if isempty(Carto)
                    [Carto, flag] = getDefinitionCarto(this.Images(this.indImage), 'CartoAuto', 1);
                    if ~flag
                        return
                    end
                end
                [CoordPoints.X, CoordPoints.Y] = latlon2xy(Carto, CoordPoints.Latitude, CoordPoints.Longitude);

            case cl_image.indGeometryType('GeoYX')
                CoordPoints.X = XPixels{1}{iSegm};
                CoordPoints.Y = YPixels{1}{iSegm};
                Carto = get_Carto(this.Images(this.indImage));
                [CoordPoints.Latitude, CoordPoints.Longitude] = xy2latlon(Carto, CoordPoints.X, CoordPoints.Y);
                
            otherwise
                CoordPoints.X = XPixels{1}{iSegm};
                CoordPoints.Y = YPixels{1}{iSegm};
                CoordPoints.Latitude  = [];
                CoordPoints.Longitude = [];
        end
        setappdata(hCourbe, 'CoordPoints', CoordPoints);
%         setappdata(hCourbe, 'ProfileXPixels', XPixels{1}{iSegm});
%         setappdata(hCourbe, 'ProfileYPixels', YPixels{1}{iSegm});
        
        cmenu = get(hCourbe, 'uicontextmenu');
        
        %     hCourbe = PlotUtils.createSScPlot(x{iSegm}+xFin, valV{iSegm}, 'Color', ColorOrder(iColor,:));
        
        UserData.X = X(iSegm:iSegm+1);
        UserData.Y = Y(iSegm:iSegm+1);
        set(hCourbe, 'UserData', UserData);
        
        grid on; axis tight;
%         title(nomLayersInvite{1}, 'Interpreter', 'none')
        title(str, 'Interpreter', 'none')
        
        deltax = this.coupe.X(iSegm+1) - this.coupe.X(iSegm);
        deltay = this.coupe.Y(iSegm+1) - this.coupe.Y(iSegm);
        str1 = cell(0);
        str1{end+1} = sprintf('Begin x : %f (%s)', this.coupe.X(iSegm),   XUnit); %#ok
        str1{end+1} = sprintf('End   x : %f (%s)', this.coupe.Y(iSegm),   YUnit); %#ok
        str1{end+1} = sprintf('Begin y : %f (%s)', this.coupe.X(iSegm+1), XUnit); %#ok
        str1{end+1} = sprintf('End   y : %f (%s)', this.coupe.Y(iSegm+1), YUnit); %#ok
        str1{end+1} = sprintf('Deltax  : %f (%s)', deltax, XUnit); %#ok
        str1{end+1} = sprintf('Deltay  : %f (%s)', deltay, YUnit); %#ok
        str1{end+1} = sprintf('Length  : %f %s', longueur(iSegm), UniteLength); %#ok
        if ~isempty(distance_m)
            str1{end+1} = sprintf('Length  : %f m', distance_m(iSegm)); %#ok
        end
        str1{end+1} = sprintf('Azimuth : %f', Azimuth(iSegm)); %#ok
        
        %     str2 = sprintf('disp(''-------------------------------------------------------------''); disp(''%s''); disp(''%s''); disp(''%s''); disp(''%s''); disp(''%s''); disp(''%s''); disp(''%s''); disp(''%s'');', ...
        %         str1{:});
        for iMenu=1:length(str1)
            uimenu(cmenu, 'Text', str1{iMenu});
        end
        set(hCourbe, 'uicontextmenu', cmenu);
    end
    
    str1 = 'Traitement des autres images';
    str2 = 'Processing the other images';
    hw2 = create_waitbar(Lang(str1,str2), 'N', NInvites);
    for iInvite=2:NInvites
        % TODO : a reprendre quand multi_waitbar sera plus sophistiqu�
        %         waitbar_mult(iInvite/NInvites, axw(2), {sprintf('Finished computing subpart %d', iInvite)});
        my_waitbar(iInvite, NInvites, hw2)
        
        if SameAxisForSameDtaType
            indSubplot = find(uniqueDataType == DataType(iInvite));
        else
            indSubplot = iInvite;
        end
        haxe(indSubplot) = subplot(nbAxis, 1, indSubplot); hold on;
        if ~isempty(valInvite{iInvite}{iSegm})
            flagFilled(indSubplot) = flagFilled(indSubplot) | ~all(isnan(valInvite{iInvite}{iSegm}));
            
            hCourbe = PlotUtils.createSScPlot(xInvite{iInvite}{iSegm}+xFin, valInvite{iInvite}{iSegm}, 'Color', ColorOrder(iColor,:));
            CoordPoints.Name       = nomLayersInvite{indSubplot};
            CoordPoints.indCurve   = iInvite;
%             CoordPoints.indSegment = iSegm;
            %{
            GT = this.Images(this.indImage).GeometryType;
            switch GT
                case cl_image.indGeometryType('LatLong')
                    CoordPoints.Latitude  = YPixels{iInvite}{iSegm};
                    CoordPoints.Longitude = XPixels{iInvite}{iSegm};
                    [CoordPoints.X, CoordPoints.Y] = latlon2xy(Carto, CoordPoints.Latitude, CoordPoints.Longitude);
                    
                case cl_image.indGeometryType('GeoYX')
                    CoordPoints.X = XPixels{iInvite}{iSegm};
                    CoordPoints.Y = YPixels{iInvite}{iSegm};
                    Carto = get_Carto(this.Images(this.indImage));
                    [CoordPoints.Latitude, CoordPoints.Longitude] = xy2latlon(Carto, CoordPoints.X, CoordPoints.Y);
                    
                otherwise
                    CoordPoints.X = XPixels{iInvite}{iSegm};
                    CoordPoints.Y = YPixels{iInvite}{iSegm};
                    CoordPoints.Latitude  = [];
                    CoordPoints.Longitude = [];
            end
        %}
            setappdata(hCourbe, 'CoordPoints', CoordPoints);
%             setappdata(hCourbe, 'ProfileXPixels', XPixels{iInvite}{iSegm});
%             setappdata(hCourbe, 'ProfileYPixels', YPixels{iInvite}{iSegm});
            grid on; axis tight;
            title(nomLayersInvite{indSubplot}, 'Interpreter', 'none')
            set(hCourbe, 'ButtonDownFcn', str2)
        end
    end
    my_close(hw2, 'MsgEnd')
    
    figure(this.hfig);
    hold on;
    this.coupe.handles(iSegm) = plot(  [this.coupe.X(iSegm) this.coupe.X(iSegm+1)], ...
        [this.coupe.Y(iSegm) this.coupe.Y(iSegm+1)], 'Color', ColorOrder(iColor,:));
    set(this.coupe.handles(iSegm), 'LineWidth', 3)
    set(this.coupe.handles(iSegm), 'Tag', 'ProfilOnTop')
    
    if NbVoisins > 0
        for k=1:length(Cadre)
            this.coupe.handlesPatch(iSegm) = patch(Cadre{k}.X, Cadre{k}.Y, [0.8 0.8 0.8], 'FaceAlpha', 0.5);
        end
    end
    
    xFin = x{iSegm}(end) + xFin ;
end
my_close(hw1, 'MsgEnd');

hold off;
if ~isempty(haxe)
    linkaxes(haxe(flagFilled), 'x');
end

%% Retour � l'image

if flagInteractif
    this.coupe.flagDebut = 0;
    FigUtils.createSScFigure(this.coupe.fig);
    this = traiter_typeInteractionSouris(this);
end

end
