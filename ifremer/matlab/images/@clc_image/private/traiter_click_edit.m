% Traitement des deplacements du curseur
%
% Syntax
%   this = traiter_click_edit(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = traiter_click_edit(this)

%% Détermination si le curseur est sur l'un des 3 axes

[flagAxe, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);
if flagAxe(1)
    this = process_pixel(this, 'CreateWindow', 1);
    return
end

%% Récupération du nom de l'image et des coordonnées de la croix

nomImage = this.Images(this.indImage).Name;
ImageName = extract_ImageName(this.Images(this.indImage));
xyCross  = this.cross(this.indImage, :);
[ix, iy] = xy2ij(this.Images(this.indImage), xyCross(1), xyCross(2));

[flag, indLayers, nomsLayers, DataTypes] = listeLayersSynchronises(this.Images, this.indImage, 'IncludeCurrentImage'); %#ok<ASGLU>
if ~flag
    return
end
indLayerCurrent = (indLayers == this.indImage);
% indLayerCurrent = find(indLayers == this.indImage);
% indLayers = indLayers([indLayerCurrent 1:indLayerCurrent-1 indLayerCurrent+1:end]);
% nomsLayers = nomsLayers([indLayerCurrent 1:indLayerCurrent-1 indLayerCurrent+1:end]);
            
%% Position en X

if flagAxe(2)
    if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3))
        x   = get(this.Images(this.indImage), 'x');
        XLim = this.visuLim(this.indImage, 1:2);
        subx = find((x >= XLim(1)) & (x <= XLim(2)));

        x   = this.profilX.x;
        sub = find((x >= XLim(1)) & (x <= XLim(2)));
        x = x(sub);
        val = this.profilX.val(sub);

        OtherProfiles = struct([]);
        indLayerCurrentOtherProfiles = 1; % TODO : A perfectionner
        for k=1:length(indLayers)
            xLayer = get(this.Images(indLayers(k)), 'x');
            subx = find((xLayer >= min(x)) & (xLayer <= max(x)));
            if isempty(subx)
                continue
            end
            OtherProfiles(end+1).nomLayer  = nomsLayers{k}; %#ok<AGROW>
            OtherProfiles(end).Unit   = get(this.Images(indLayers(k)), 'Unit');
            OtherProfiles(end).xLayer = xLayer(subx);
            OtherProfiles(end).yLayer = get_val_xy(this.Images(indLayers(k)), xLayer(subx), xyCross(2));
            OtherProfiles(end).xUnit  = this.Images(indLayers(k)).XUnit;
            OtherProfiles(end).xName  = this.Images(indLayers(k)).XLabel;
            
            OtherProfiles(end).CurrentView  = 0;
            if find(indLayerCurrent) == indLayers(k)
                indLayerCurrentOtherProfiles = length(OtherProfiles);
            end
        end
        OtherProfiles(indLayerCurrentOtherProfiles).CurrentView  = 1; % TODO : apparemment il faudrait +1
        
        %% Action correspondant au choix de traitement

        nomImage = sprintf('%s / y=%s / Ligne=%d', nomImage, num2str(xyCross(2)), iy);
        [val, flag] = traiter_signal(x(:)', val(:)', nomImage, 'OtherProfiles', OtherProfiles, 'Name', ImageName);
        if ~flag
            return
        end

        %% On sauve les données dans l'image

        suby = (iy-this.NbVoisins):(iy+this.NbVoisins);
        this.Images(this.indImage) = set_val(this.Images(this.indImage), suby, subx, val);
        this.Images(this.indImage) = compute_stats(this.Images(this.indImage));
    end
end

%% Position en Y

if flagAxe(3)
    if (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))
        this = traiterProfilY(this, nomsLayers, indLayers, xyCross, indLayerCurrent, ix);
    end
end

%% Affichage des profils

this = show_image(this);
this = show_profils(this);
