% Traitement des déplacements du curseur
%
% Syntax
%   this = traiter_click_new(this)
%
% Input Arguments
%   this : instance de clc_image
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = traiter_click_new(this, varargin)

[varargin, CreateWindow] = getPropertyValue(varargin, 'CreateWindow', 0); %#ok<ASGLU>

%% Détermination si le curseur est sur l'un des 3 axes

[flag, positionCurseur, positionAxePrincipal] = test_curseur_sur_axes(this);
if ~any(flag)
    return
end

%% Position en X

if flag(1) || flag(2)
    if (positionCurseur(1) > 0) && (positionCurseur(1) < positionAxePrincipal(3))
        
        % ------------------------------------------------------
        % Détermination des limites de l'axe principal en x et y
        
        limites = axis(this.hAxePrincipal);
        
        % ---------------------------
        % Détermination de l'abscisse
        
        if this.Images(this.indImage).XDir == 1
            this.cross(this.indImage, 1) = limites(1) + (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        else
            this.cross(this.indImage, 1) = limites(2) - (limites(2) - limites(1)) * positionCurseur(1) / positionAxePrincipal(3);
        end
    end
end

%% Position en Y

if flag(1) || flag(3)
    if (positionCurseur(2) > 0) && (positionCurseur(2) < positionAxePrincipal(4))
        
        % Détermination des limites de l'axe principal en x et y
        limites = axis(this.hAxePrincipal);
        
        % Détermination de l'ordonnée
        if this.Images(this.indImage).YDir == 1
            this.cross(this.indImage, 2) = limites(3) + (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        else
            this.cross(this.indImage, 2) = limites(4) - (limites(4) - limites(3)) * positionCurseur(2) / positionAxePrincipal(4);
        end
    end
end

%% Affichage des profils et des valeurs

this = show_profils(this);

%% Traitement des synchronisations

this = process_synchroX(this);
this = process_synchroY(this);

%% Affichage des valeurs du voisinage de la croix

PrintPixel(this, 'CreateWindow', CreateWindow);
