% Traitement de l'activation ou désactivation du zoom
% 
% Syntax
%   this = traiter_typeInteractionSouris(this, action)
%
% Input Arguments
%   this   : instance de clc_image
%   action : type d'action pour le zoom
%
% Output Arguments
%   this : instance de clc_image initialisee
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = traiter_typeInteractionSouris(this, varargin)

persistent LastTypeInteractionSouris
if isempty(LastTypeInteractionSouris)
    LastTypeInteractionSouris = 'None';
end

switch this.TypeInteractionSouris
    case 'ZoomInteractif'
        this.ihm.zoom.cpn = set(this.ihm.zoom.cpn, 'action', 'In');
        this.ihm.zoom.cpn = activer(this.ihm.zoom.cpn);
    otherwise
        this.ihm.zoom.cpn = set(this.ihm.zoom.cpn, 'action', 'Out');
        this.ihm.zoom.cpn = desactiver(this.ihm.zoom.cpn);
end

NoSelectionSignaVert = strcmp(LastTypeInteractionSouris, 'CleanData') ...
    || strcmp(LastTypeInteractionSouris, 'CleanData2') ...
    || strcmp(LastTypeInteractionSouris, 'CleanData2') ...
    || strcmp(LastTypeInteractionSouris, 'MoveProfils');
this = placer_callback_click(this, 'NoSelectionSignaVert', NoSelectionSignaVert, varargin{:});

this = placer_callback_curseur(this);

LastTypeInteractionSouris = this.TypeInteractionSouris;
