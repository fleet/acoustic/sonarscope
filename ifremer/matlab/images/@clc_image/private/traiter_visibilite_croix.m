% Traitement de la visibilite de la croix sur l'axe principal
% 
% Syntax
%   traiter_visibilite_croix(this)
%
% Input Arguments
%   this : instance de clc_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function traiter_visibilite_croix(this)

if ishandle(this.hCroix(2))
    if this.visuCoupeHorz
        set(this.hCroix(2), 'Visible', 'on');
    else
        set(this.hCroix(2), 'Visible', 'off');
    end
end

if ishandle(this.hCroix(1))
    if this.visuCoupeVert
        set(this.hCroix(1), 'Visible', 'on');
    else
        set(this.hCroix(1), 'Visible', 'off');
    end
end
