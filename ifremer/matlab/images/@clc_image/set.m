% Modification des attributs de l'instance
%
% Syntax
%   a = set(a, varargin)
%
% Input Arguments
%   a : instance de clc_image
%
% Name-Value Pair Arguments
%    fLabelX           : flag affichage des labels selon x, 0 ou 1
%    fLabelY           : flag affichage des labels selon y, 0 ou 1
%    visuGrid             : flag de mise en place de la grille, 0 ou 1
%    visuCoupeHorz          : flag affichage profil selon x, 0 ou 1
%    visuCoupeVert          : flag affichage profil selon y, 0 ou 1
%    visuColorbar         : flag affichage barre de couleurs
%    fToolBar          : flag affichage toolbarre
%    hauteurToolBar    : hauteur (en nb lignes) de la toolbar
%    hauteurProfilX    : hauteur (en nb lignes) du profil en X
%    largeurProfilY    : largeur (en nb colonnes) du profil en Y
%    largeurColorBar   : largeur (en nb colonnes) de la colorbar
%    margeLabelX       : marge (en nb lignes) du label x de l axe
%    margeLabelY       : marge (en nb colonnes) du label y de l axe
%    nbTotLignes       : nombre total de lignes
%    nbTotColonnes     : nombre total de colonnes
%    colormap          : colormap appliquee aux axes du composant
%    txtProfilX        : texte de toolbar pour profilX
%    txtProfilY        : texte de toolbar pour profilY
%    txtValeurX        : texte de toolbar pour X
%    txtValeurY        : texte de toolbar pour Y
%    txtValeurV        : texte de toolbar pour V
%
%    componentName     : nom associe a ce composant
%    componentVisible  : visibilite du composant    {'on', 'off'}
%    componentEnable   : accessibilite du composant {'on', 'off'}
%    componentUserName : nom de la classe utilisatrice du composant
%    componentUserCb   : nom de la methode de gestion des call back
%                        de la classe utilisatrice du composant
%    componentInsetX   : marge en X entre elements de ce composant
%    componentInsetY   : marge en Y entre elements de ce composant
%
% Output Arguments
%    instance : instance de clc_image initialisee
%
% Remarks :
%
% Examples
%    v = clc_image;
%    set (v, 'componentName', 'Un nom bien choisi');
%
% See also clc_image
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set(this, varargin)

global IfrTbxRepDefInput %#ok<GVMIS>
global IfrTbxRepDefOutput %#ok<GVMIS>

%% Lecture des arguments

[varargin, I] = getPropertyValue(varargin, 'Images', []);
if ~isempty(I)
    this.Images = I;
end
[varargin, this.indImage] = getPropertyValue(varargin, 'indImage', this.indImage);
[varargin, this.identVisu] = getPropertyValue(varargin, 'identVisu', this.identVisu);
% [varargin, this.synchroX] = getPropertyValue(varargin, 'synchroX', this.synchroX);
% [varargin, this.synchroY] = getPropertyValue(varargin, 'synchroY', this.synchroY);
% [varargin, this.synchroV] = getPropertyValue(varargin, 'synchroV', this.synchroV);

[varargin, this.visuGrid] = getPropertyValue(varargin, 'visuGrid', this.visuGrid);
[varargin, this.visuCoupeHorz] = getPropertyValue(varargin, 'visuCoupeHorz', this.visuCoupeHorz);
[varargin, this.visuCoupeVert] = getPropertyValue(varargin, 'visuCoupeVert', this.visuCoupeVert);
[varargin, this.visuColorbar] = getPropertyValue(varargin, 'visuColorbar', this.visuColorbar);
[varargin, this.NbVoisins] = getPropertyValue(varargin, 'NbVoisins', this.NbVoisins);
[varargin, this.repImport] = getPropertyValue(varargin, 'repImport', this.repImport);
[varargin, this.repExport] = getPropertyValue(varargin, 'repExport', this.repExport);

[varargin, this.visuLimHistoryPointer] = getPropertyValue(varargin, 'visuLimHistoryPointer', this.visuLimHistoryPointer);
[varargin, this.visuLimHistory] = getPropertyValue(varargin, 'visuLimHistory', this.visuLimHistory);

[varargin, cvisible] = getPropertyValue(varargin, 'componentVisible', []);
[varargin, cenable] = getPropertyValue(varargin, 'componentEnable', []);

[varargin, fLabelX] = getPropertyValue(varargin, 'fLabelX', []);
[varargin, fLabelY] = getPropertyValue(varargin, 'fLabelY', []);
[varargin, visuGrid] = getPropertyValue(varargin, 'visuGrid', []);
[varargin, visuCoupeHorz] = getPropertyValue(varargin, 'visuCoupeHorz', []);
[varargin, visuCoupeVert] = getPropertyValue(varargin, 'visuCoupeVert', []);
[varargin, visuColorbar] = getPropertyValue(varargin, 'visuColorbar', []);
[varargin, fToolBar] = getPropertyValue(varargin, 'fToolBar', []);
[varargin, hauteurToolBar] = getPropertyValue(varargin, 'hauteurToolBar', []);
[varargin, hauteurProfilX] = getPropertyValue(varargin, 'hauteurProfilX', []);
[varargin, largeurProfilY] = getPropertyValue(varargin, 'largeurProfilY', []);
[varargin, largeurColorBar] = getPropertyValue(varargin, 'largeurColorBar', []);
[varargin, nbTotLignes] = getPropertyValue(varargin, 'nbTotLignes', []);
[varargin, nbTotColonnes] = getPropertyValue(varargin, 'nbTotColonnes', []);
[varargin, colormap] = getPropertyValue(varargin, 'colormap', []);
[varargin, margeLabelX] = getPropertyValue(varargin, 'margeLabelX', []);
[varargin, margeLabelY] = getPropertyValue(varargin, 'margeLabelY', []);
[varargin, txtProfilX] = getPropertyValue(varargin, 'txtProfilX', []);
[varargin, txtProfilY] = getPropertyValue(varargin, 'txtProfilY', []);
[varargin, this.profilY.Type] = getPropertyValue(varargin, 'ProfilY.Type', []);

[varargin, this.cross] = getPropertyValue(varargin, 'cross', this.cross);
[varargin, this.visuLim] = getPropertyValue(varargin, 'visuLim', this.visuLim);
[varargin, this.ZoneEtude] = getPropertyValue(varargin, 'ZoneEtude', this.ZoneEtude);

%% Initialisation des attributs

[varargin, TypeInteractionSouris] = getPropertyValue(varargin, 'TypeInteractionSouris', []);
if ~isempty(TypeInteractionSouris)
    switch TypeInteractionSouris
        case 1
            this.TypeInteractionSouris = 'PointClick';
        case 2
            this.TypeInteractionSouris = 'ZoomInteractif';
        case 3
            this.TypeInteractionSouris = 'Centrage';
        case 4
            this.TypeInteractionSouris = 'Coupe';
        case 5
            this.TypeInteractionSouris = 'SetNaN';
        case 6
            this.TypeInteractionSouris = 'SetVal';
        case 7
            this.TypeInteractionSouris = 'MoveProfils';
        case 8
            this.TypeInteractionSouris = 'DrawHeight';
        case 9
            this.TypeInteractionSouris = 'CleanData';
        otherwise
            this.TypeInteractionSouris = TypeInteractionSouris;
    end
end

if isempty(this.repImport)
    if isempty(IfrTbxRepDefInput)
        this.repImport = my_tempdir;
    else
        this.repImport = IfrTbxRepDefInput;
    end
end
if isempty(this.repExport)
    if isempty(IfrTbxRepDefOutput)
        this.repExport = my_tempdir;
    else
        this.repExport = IfrTbxRepDefOutput;
    end
end

if isempty(this.profilY.Type)
    this.profilY.Type = 'Image profile'; % Special image sonar : 'Roll', 'Heading', ...
end

if ~isempty(cvisible)
    this = set_cvisible(this, cvisible);
end
if ~isempty(cenable)
    this = set_cenable(this, cenable);
end

if ~isempty(fLabelX)
    this = set_f_label_x(this, fLabelX);
end
if ~isempty(fLabelY)
    this = set_f_label_y(this, fLabelY);
end
if ~isempty(visuGrid)
    this = set_f_grid(this, visuGrid);
end
if ~isempty(visuCoupeHorz)
    this = set_f_profil_x(this, visuCoupeHorz);
end
if ~isempty(visuCoupeVert)
    this = set_f_profil_y(this, visuCoupeVert);
end
if ~isempty(visuColorbar)
    this = set_f_colorbar(this, visuColorbar);
end
if ~isempty(fToolBar)
    this = set_f_toolbar(this, fToolBar);
end
if ~isempty(hauteurToolBar)
    this = set_hauteur_toolbar(this, hauteurToolBar);
end
if ~isempty(hauteurProfilX)
    this = set_hauteur_profil_x(this, hauteurProfilX);
end
if ~isempty(largeurProfilY)
    this = set_largeur_profil_y(this, largeurProfilY);
end
if ~isempty(largeurColorBar)
    this = set_largeur_colorbar(this, largeurColorBar);
end
if ~isempty(nbTotLignes)
    this = set_nb_tot_lignes(this, nbTotLignes);
end
if ~isempty(nbTotColonnes)
    this = set_nb_tot_colonnes(this, nbTotColonnes);
end
if ~isempty(colormap)
    this = set_colormap(this, colormap);
end
if ~isempty(margeLabelX)
    this = set_marge_label_x(this, margeLabelX);
end
if ~isempty(margeLabelY)
    this = set_marge_label_y(this, margeLabelY);
end
if ~isempty(txtProfilX)
    this = set_txt_profil_x(this, txtProfilX);
end
if ~isempty(txtProfilY)
    this = set_txt_profil_y(this, txtProfilY);
end

%% Traitement des message de la super classe

if ~isempty(varargin)
%     this.cl_component = set(this.cl_component, varargin{:});
    this.composant = set(this.composant, varargin{:});
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
