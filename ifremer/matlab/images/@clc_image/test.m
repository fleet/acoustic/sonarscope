% Test graphique du composant
%
% Syntax
%   a = test(a)
% 
% Input Arguments
%   a : instance de clc_image
%
% Output Arguments
%   a : instance de clc_image
% 
% Examples
%    a = test(clc_image);
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = test(~)

%% Cr�ation d'une figure ainsi que de l'ancre du composant

figure;
uicontrol('Visible', 'on', ...
    'Tag', 'ancre_clc_image 1', ...
    'Position', [10 10 500 400]);

%% Cr�ation et initialisation du composant

a = cl_image('Image', Lena, 'Name', 'Lena', 'YDir', 2);
this = clc_image('Images', a);
this = set( this, ...
    'componentName',     'ancre_clc_image 1', ...
    'componentUserName', 'clc_image', ...
    'componentUserCb',   'callback');
this = set( this, ...
    'visuLim', [1 256 1 256]);

%% Cr�ation graphique du composant

this = editobj(this);
