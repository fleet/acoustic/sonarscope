% Ajout d'une image dans l'instance
% 
% Syntax
%   this = add_Image(this, ...)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Name-Value Pair Arguments
%   cf. cl_image
%
% Output Arguments
%   this : instance initialisee
%
% Examples
%   b = cli_image;
%   a = cl_image('Image', Lena, 'Name', 'Lena', 'YDir', 2);
%   b = add_Image(b, a);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = add_Image(this, Image)

N = length(this.Images);
for k=1:length(Image)
    i = N + k;
    this.Images(i) = Image(k);

    XLim = get(this.Images(i), 'XLim');
    YLim = get(this.Images(i), 'YLim');
    this.visuLim(i, 1:2) = XLim;
    this.visuLim(i, 3:4) = YLim;
    this.visuLimHistory{i}{1} = this.visuLim(i, :);
    this.visuLimHistoryPointer(i) = 1;
    this.Point(i, 1:2) = [sum(XLim)/2 sum(YLim)/2];
    this.valBeforeSetNaN{i} = [];

    if length(this.identVisu) < i
        this.identVisu(i) = 1;
    end
end

% --------------------
% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
