% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char_sonar(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cli_image
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   a = cli_image('Image', Lena);
%   str = char_sonar(a)
%
% See also cli_image Authors
% Authors : JMA
% VERSION  : $Id: char_sonar.m,v 1.1 2003/09/08 15:35:02 augustin Exp augustin $
% ----------------------------------------------------------------------------

function str = char_sonar(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char_sonar(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);

else
    % ---------------
    % Instance unique

    for i=1:length(this.Images)
        str{end+1} = sprintf('--- aggregation sonar (cl_sounder) ---'); %#ok<AGROW>
        str{end+1} = char_sonar(this.Images); %#ok<AGROW>
    end
    str = cell2str(str);

    str = [repmat(' ', size(str,1), 2) str];
end
