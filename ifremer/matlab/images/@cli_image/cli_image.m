classdef cli_image < handle
        
    properties(GetAccess = 'public', SetAccess = 'public')
        
        %% D�finition de la structure
        
        identVisu             = [];
        strVisu               = {'Image'; 'Contour'; 'surf'};
        Point                 = [];
        visuGrid              = 0;
        visuAxes              = 1;
        visuProfilX           = 0;
        visuProfilY           = 0;
        visuColorbar          = 1;
        visuLim               = [];
        visuLimHistory        = cell(0);
        visuLimHistoryPointer = 0;
        profilY               = struct('Type', []);
        valBeforeSetNaN       = cell(0);
        ZoneEtude             = [];
        Images                = cl_image.empty
        indImage              = 1;
        NbVoisins             = 0;
        repExport             = [];
        TypeInteractionSouris = 1;
        ihm                   = [];
    end
    
    methods(Access = 'public')
        function this = cli_image(varargin)
            this = set(this, varargin{:});
        end
    end
    
    methods (Static)
        function callback(msg, varargin)
            
            if isempty(msg)
                return
            end
            
            %% On recup�re l'instance dans le UserData de la figure
            
            % this = handle2obj(gcbf);
            this = getappdata(gcbf, 'Instance');
            if ~isa(this, 'cli_image')
                return
            end
            
            %% Message provenant du composant clc_entete
            
            if is_msg(this.ihm.entete.cpn, msg)
                this.ihm.entete.cpn = gerer_callback(this.ihm.entete.cpn, msg);
                return
            end
            
            %% Message provenant du bouton Save
            
            if strcmp(msg, 'Save')
                busy(this.ihm.figure)
                this.identVisu              = get(this.ihm.image.cpn, 'identVisu');
                this.visuGrid               = get(this.ihm.image.cpn, 'visuGrid');
                this.visuProfilX            = get(this.ihm.image.cpn, 'visuCoupeHorz');
                this.visuProfilY            = get(this.ihm.image.cpn, 'visuCoupeVert');
                this.visuColorbar           = get(this.ihm.image.cpn, 'visuColorbar');
                this.indImage               = get(this.ihm.image.cpn, 'indImage');
                this.Images                 = get(this.ihm.image.cpn, 'Images');
                this.Point                  = get(this.ihm.image.cpn, 'cross');
                this.visuLim                = get(this.ihm.image.cpn, 'visuLim');
                this.ZoneEtude              = get(this.ihm.image.cpn, 'ZoneEtude');
                this.visuLimHistory         = get(this.ihm.image.cpn, 'visuLimHistory');
                this.visuLimHistoryPointer  = get(this.ihm.image.cpn, 'visuLimHistoryPointer');
                this.NbVoisins              = get(this.ihm.image.cpn, 'NbVoisins');
                
                savedObj2handle(this, this.ihm.figure);
                uiresume(this.ihm.figure);
                idle(this.ihm.figure)
                return
            end
            
            %% Messages provenant du composant clc_image
            
            if is_msg(this.ihm.image.cpn, msg)
%               this.ihm.image.cpn = callback(this.ihm.image.cpn, msg, varargin{:});
                this.ihm.image.cpn = this.ihm.image.cpn.callback(msg, varargin{:});
            else
                switch msg
                    case 'Quit'
                        % Trait� plus loin
                    case 'PreviousImage'
                        msg = get_messageSelectionImage(this.ihm.image.cpn);
                        this.ihm.image.cpn = callback(this.ihm.image.cpn, msg, 'PreviousImage');
                    case 'NextImage'
                        msg = get_messageSelectionImage(this.ihm.image.cpn);
                        this.ihm.image.cpn = callback(this.ihm.image.cpn, msg, 'NextImage');
                    case 'GetModifiedImageAxeX'
                        this.ihm.image.cpn = callback(this.ihm.image.cpn, msg, 'GetModifiedImageAxeX');
                    case 'GetModifiedImageAxeY'
                        this.ihm.image.cpn = callback(this.ihm.image.cpn, msg, 'GetModifiedImageAxeY');
                    case 'GetModifiedImageAxeImage'
                        this.ihm.image.cpn = callback(this.ihm.image.cpn, msg, 'GetModifiedImageAxeImage');
                    otherwise
                        str = sprintf('Message ''%s'' non interprete', msg);
                        my_warndlg(['cli_image:callback ', str], 0);
                end
            end
            
            %% On replace l'instance dans le UserData de la figure
            % Sauf dans le cas du d�placement de la souris car on
            % passerait 90% du temps � copier l'objet dans le UserData
            
            if ~(is_msgMoveCurseur(this.ihm.image.cpn, msg) || is_msgMoveProfils(this.ihm.image.cpn, msg))
                if ishandle(this.ihm.figure)
                    setappdata(this.ihm.figure, 'Instance', this)
                end
            end
            
            %% Message provenant du bouton Quit
            
            if strcmp(msg, 'Quit') % TODO : prend �norm�ment de temps apr�s un restauration de session. Pourquoi ? c'est un mist�re
                str1 = 'Fermer cette fen�tre SonarScope ?';
                str2 = 'Exit this SonarScope window ?';
                [rep, flag] = my_questdlg(Lang(str1,str2));
                if flag && (rep == 1)
                    GCF = this.ihm.figure;
                    
                    ExitForbidenForTheMoment = getappdata(GCF, 'ExitForbidenForTheMoment');
                    if ~isempty(ExitForbidenForTheMoment)
                        str1 = 'Une action est en cours qui emp�che la fermeture de cette fen�tre.';
                        str2 = 'An action is runing, this window cannot be closed.';
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
                    
                    delete(get(this.ihm.image.cpn, 'Images'));
                    
                    set(GCF, 'CloseRequestFcn', 'close')
                    delete(GCF)
                    deleteFileOnListe(cl_memmapfile.empty, GCF)
                    
                    display_LogFile(GCF);
                end
            end
        end
    end
end
