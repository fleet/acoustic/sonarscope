% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(a)
%
% Input Arguments
%   a : instance de cli_image
%
% Examples
%   a = cli_image('Image', Lena);
%   a
%
% See also cli_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)
s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
