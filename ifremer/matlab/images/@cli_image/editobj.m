% Edition d'une instance de cli_image.
%
% Syntax
%   editobj(a)
%   a = editobj(a)
%
% Input Arguments
%   a : Un objet de type cli_image
%
% Name-Value Pair Arguments
%   ButtonSave : 0=non modal, 1=modal
%   FigureName : Name of the figure
%
% Output Arguments
%   [] : L'interface rend la main
%   a  : Instance de cli_image eventuellement modifiee
%
% Remarks : L'IHM rend la main si il n'y a pas d'argument de sortie
%
% Examples
%   a = cl_image('Image', Lena)
%   editobj(a)
%
% See also cli_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = editobj(this, varargin)

[varargin, ButtonSave] = getPropertyValue(varargin, 'ButtonSave', 0);
[varargin, FigureName] = getPropertyValue(varargin, 'FigureName', []); %#ok<ASGLU>

if isempty(this) || isempty(this.Images)
    % Test si les data et la doc sont bien install�es
    
    checkInstallationDataDocAndMex
    
    nomFic = getNomFicDatabase('TerreEtopo.mat');
    if isempty(nomFic)
        return
    end
    Z = loadmat(nomFic, 'nomVar', 'Image');
    Info = loadmat(nomFic, 'nomVar', 'Info');
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 1);
    a = cl_image('Image', Z, 'X', Info.x, 'Y', Info.y, 'Name', 'Earth', ...
        'ColormapIndex', 3, ...
        'GeometryType', cl_image.indGeometryType('LatLong'), ...
        'DataType', cl_image.indDataType('Bathymetry'), ...
        'XUnit', 'deg', 'YUnit', 'deg', 'Unit', 'm', ...
        'InitialImageName', 'Earth', ...
        'InitialFileName', nomFic, ...
        'Comments', 'Keep it blue', 'Carto', Carto);
    this = cli_image('Images', a);
end

%% Cr�ation et remplissage de la fen�tre

this = construire_ihm(this, 'FigureName', FigureName);
GCF = this.ihm.figure;

if nargout == 0
    
    %% Cas ou l'IHM ne rend pas la main. On ne peut pas recup�rer les
    % modifications. On rend le bouton save invisible
    
    h_Save = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Save');
    set(h_Save, 'Visible', 'off');
    
    h_Quit = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Quit');
    W_Quit = get(h_Quit, 'Position');
    W_Quit(3) = W_Quit(3) * 2;
    set(h_Quit, 'Position', W_Quit);
    
    if isdeployed
        set(findobj(this.ihm.figure, 'Tag', 'ancre cli_image Quit'), 'Visible', 'off');
    end
    
    if ~isdeployed
        disp('Ce mode d''edition ''editobj(a)'' ne permet pas de sauver les modifications.')
        disp('Preferez : ''a = editobj(a);'' si cela est votre intention')
    end
    
else
    
    if isdeployed && ~ButtonSave
        h_Save = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Save');
        set(h_Save, 'Visible', 'off');
        
        h_Quit = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Quit');
        W_Quit = get(h_Quit, 'Position');
        W_Quit(3) = W_Quit(3) * 2;
        set(h_Quit, 'Position', W_Quit);
    end
    
    %% Boucle d'attente
    
    h0 = GCF;
    UserData = get(h0, 'UserData');
    while ishandle(h0)
        uiwait(h0);
        if ishandle(h0)
            UserData = get(h0, 'UserData');
        end
    end
    
    %% R�cup�ration de l'instance sauv�e
    
    InstanceSauvee = getFromUserData('InstanceSauvee', UserData);
    if isempty(InstanceSauvee)
        varargout{1} = this;
    else
        varargout{1} = InstanceSauvee;
    end
end

deleteFileOnListe(cl_memmapfile.empty, GCF)
