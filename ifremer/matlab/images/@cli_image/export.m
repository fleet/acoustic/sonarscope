% Exportation des images en format Tif, jpeg, ...
%
% Syntax
%   export(a, indice)
%
% Input Arguments
%   a      : instance de cli_image
%   nomRep : Nom du repertoire
%
% Name-Value Pair Arguments
%   sub    : Indices de l'image (toutes par defaut)
%   nomDir : Non du repertoire de sortie
%   Format : {'mat'}, 'tif'; 'jpg'; 'bmp'; 'png'; 'hdf'; 'xwd'; 'ras';
%            'pbm'; 'pgm'; 'ppm'; 'pnm'
%
% Examples
%   b = cli_image;
%   a = cl_image('Image', Lena, 'Name', 'Lena', 'YDir', 2);
%   b = add_Image(b, a);
%
%   a = cl_image.import_Prismed;
%   b = add_Image(b, a);
%
%   export(b, 'Format', 'tif')
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function export(this, varargin)

[varargin, sub]    = getPropertyValue(varargin, 'sub', 1:length(this.Images));
[varargin, ext]    = getPropertyValue(varargin, 'Format', 'mat');
[varargin, nomDir] = getPropertyValue(varargin, 'nomDir', my_tempdir); %#ok<ASGLU>

for i=1:length(sub)
    a = get_Image(this, sub(i));
    
    [ix, iy] = xy2ij(a, this.visuLim(i,1:2), this.visuLim(i,3:4));
    nomFic = a.Name;
    nomFic = double(nomFic);
    
    nomFic(nomFic == 45) = [];
    nomFic(nomFic == 32) = 95;
    nomFic(nomFic == 91) = [];
    nomFic(nomFic == 93) = [];
    
    nomFic = char(nomFic);
    nomFic = fullfile(nomDir, nomFic);
    export(a, 'Format', ext, 'nomFic', nomFic, 'subx', ix(1):ix(2), 'suby', iy(1):iy(2));
end
