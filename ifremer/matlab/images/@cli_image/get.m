function varargout = get(this, varargin)

varargout = {};

for k=1:length(varargin)
    switch varargin{k}
        case 'visuAxes'
            varargout{end+1} = this.visuAxes;   %#ok<AGROW>
        case 'visuGrid'
            varargout{end+1} = this.visuGrid;   %#ok<AGROW>
        case 'visuProfilX'
            varargout{end+1} = this.visuProfilX;   %#ok<AGROW>
        case 'visuProfilY'
            varargout{end+1} = this.visuProfilY;   %#ok<AGROW>
        case 'visuColorbar'
            varargout{end+1} = this.visuColorbar;   %#ok<AGROW>
        case 'visuLim'
            varargout{end+1} = this.visuLim;   %#ok<AGROW>
        case 'ZoneEtude'
            varargout{end+1} = this.ZoneEtude;   %#ok<AGROW>
        case 'Images'
            varargout{end+1} = this.Images;   %#ok<AGROW>
        case 'indImage'
            varargout{end+1} = this.indImage;   %#ok<AGROW>
        case 'NbVoisins'
            varargout{end+1} = this.NbVoisins;   %#ok<AGROW>
        case 'repExport'
            varargout{end+1} = this.repExport;   %#ok<AGROW>
        case 'identVisu'
            varargout{end+1} = this.identVisu;   %#ok<AGROW>
        case 'Point'
            varargout{end+1} = this.Point;   %#ok<AGROW>

        otherwise
            varargout{end+1} = get(this, varargin{k});  %#ok<AGROW>
    end
end
