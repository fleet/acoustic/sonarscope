function h = getHandleAnchor(this, Tag)

ht = get_tab_uicontrols(this.ihm.frame.globale); 
for k=1:length(ht)
    if  strcmp(Tag, get(ht(k), 'Tag'))
        h = ht(k);
        return
    end
end
h = [];
