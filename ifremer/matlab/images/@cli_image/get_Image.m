% Recuperation d'une image particuliere dans une instance
% 
% Syntax
%   b = get_Image(a, indice)
%
% Input Arguments
%   a      : instance de cli_image
%   indice : Indice de l'image
%
% Output Arguments
%   b : instance de cl_image
%
% Examples
%   b = cli_image;
%   a = cl_image('Image', Lena, 'Name', 'Lena', 'YDir', 2);
%   b = add_Image(b, a);
%
%   a = cl_image.import_Prismed;
%   b = add_Image(b, a);
%
%   I = get_Image(b, 1);
%   imagesc(I);
%   J = get_Image(b, 2);
%   imagesc(J);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Image = get_Image(this, varargin)

if nargin == 1
    Image = this.Images;
else
    sub1 = varargin{1};
    sub2 = find(sub1 > length(this.Images), 1);
    if isempty(sub2)
        Image = this.Images(sub1);
    else
        Image = []; % TODO : modifier l'appel par [flag, Image] = 
    end
end
