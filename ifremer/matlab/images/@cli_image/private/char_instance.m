% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a) 
%
% Input Arguments
%   a : instance de cl_image
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this)

str = [];

if isempty(this)
    return
end

str{end+1} = sprintf('indImage      <-> %d', this.indImage);
str{end+1} = sprintf('visuAxes      <-> %s', BooleenDansListe2str(this.visuAxes));
str{end+1} = sprintf('visuGrid      <-> %s', BooleenDansListe2str(this.visuGrid));
str{end+1} = sprintf('visuProfilX   <-> %s', BooleenDansListe2str(this.visuProfilX));
str{end+1} = sprintf('visuProfilY   <-> %s', BooleenDansListe2str(this.visuProfilY));
str{end+1} = sprintf('visuColorbar  <-> %s', BooleenDansListe2str(this.visuColorbar));
str{end+1} = sprintf('visuColorbar  <-> %s', BooleenDansListe2str(this.visuColorbar));
str{end+1} = sprintf('NbVoisins     <-> %d', this.NbVoisins);
str{end+1} = sprintf('repExport     <-> %s', this.repExport);

str{end+1} = sprintf('TypeInteractionSouris <-> %d', this.TypeInteractionSouris);
str{end+1} = ' ';
for i=1:length(this.Images)
    str{end+1} = sprintf('identVisu(%d)  <-> %s', i, SelectionDansListe2str(this.identVisu(i),   this.strVisu, 'Num')); %#ok<AGROW>
end
for i=1:length(this.Images)
    str{end+1} = sprintf('Point(%d,:)    <-> %s', i, num2strCode(this.Point(i, :))); %#ok<AGROW>
end
for i=1:length(this.Images)
    str{end+1} = sprintf('visuLim(%d,:)  <-> %s', i, num2strCode(this.visuLim(i, :))); %#ok<AGROW>
end

str{end+1} = sprintf('--- aggregation Images (cl_image) ---');
str{end+1} = char(this.Images);

%% Concatenation en une chaine

str = cell2str(str);
