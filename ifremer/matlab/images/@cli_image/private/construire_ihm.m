% Creation de l'IHM d'une instance de cli_image
%
% Syntax
%   this = construire_ihm(this)
%
% Input Arguments
%    this : cli_image instance
%
% Name-Value Pair Arguments
%   FigureName : Name of the figure
%
% Output Arguments
%    this : cli_image instance
%
% Examples
%   a = cl_image('Image', Lena)
%   a = construire_ihm(a)
%
% See also cli_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = construire_ihm(this, varargin)

[varargin, FigureName] = getPropertyValue(varargin, 'FigureName', []); %#ok<ASGLU>

%% Cr�ation de la figure

this = creer_figure(this, 'FigureName', FigureName);

%% Mise en place des frames

this = creer_frame_globale(this);

%% Remplissage des frames

this = creer_cpn_entete(this);
this = creer_cpn_image(this);

%% Sauvegarde de l'objet dans le UserData de la figure

obj2handle(this, this.ihm.figure);
setappdata(this.ihm.figure, 'Instance', this)
