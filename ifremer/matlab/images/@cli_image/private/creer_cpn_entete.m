% Creation du composant entete
% 
% a = creer_cpn_entete(a)
%
% Input Arguments
%    a : instance de cli_image
%
% Output Arguments
%    a : instance de cli_image
%
% See also cli_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_cpn_entete(this)

Titre = 'SonarScope ';

%% Cr�ation de l'instance de frame de l'ent�te

this.ihm.entete.cpn = clc_entete([]);
this.ihm.entete.cpn = set_titre(this.ihm.entete.cpn, Titre);
Tag = 'ancre cli_image entete';
this.ihm.entete.cpn = set_name(this.ihm.entete.cpn, Tag);
this.ihm.entete.cpn = set_handleAnchor(this.ihm.entete.cpn, getHandleAnchor(this, Tag));

this.ihm.entete.cpn = set_user_name(this.ihm.entete.cpn, 'cli_image');
this.ihm.entete.cpn = set_user_cb(  this.ihm.entete.cpn, 'callback');
this.ihm.entete.cpn = set(this.ihm.entete.cpn, 'TitreTooltipString', 'To a SonarScope presentation');

%% Cr�ation graphique du composant

this.ihm.entete.cpn = editobj(this.ihm.entete.cpn);
