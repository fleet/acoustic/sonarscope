% Cretaes the figure for a cli_image instance
%
% Syntax
%   this = creer_figure(this) % TODO : renommer createFigure
%
% Input Arguments
%   this : cli_image instance
%
% Name-Value Pair Arguments
%   FigureName : Name of the figure
%
% Output Arguments
%   this : cli_image instance
%
% See also cli_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_figure(this, varargin)

[varargin, FigureName] = getPropertyValue(varargin, 'FigureName', []); %#ok<ASGLU>

if isempty(FigureName)
    FigureName = ['SonarScope Work Window' ' - R' version('-release')];
end

%% Cr�ation de la figure

this.ihm.figure = figure(...
    'NumberTitle',        'off', ...
    'name',               FigureName,...
    'PaperOrientation',   'landscape', ...
    'PaperPosition',      [18 180 576 432], ...
    'PaperUnits',         'points', ...
    'Position',           centrageFig(950, 760), ...
    'ToolBar',            'none',...
    'MenuBar',            'none');

set(this.ihm.figure, 'CloseRequestFcn', 'cli_image.callback(''Quit'')')
