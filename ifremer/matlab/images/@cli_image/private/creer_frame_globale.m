% Creation de la frame globale
%
% Syntax
%    a = creer_frame_globale(a)
%
% Input Arguments
%    a : instance de cli_image
%
% Output Arguments
%    a : instance de cli_image
%
% See also cli_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = creer_frame_globale(this)

LigSave = 1:3;
ColSave = 29;
LigQuit = 1:3;
ColQuit = 28;

ihm = [];

%% Ancre du composant entete

ihm{end+1}.Lig         = 1:3;
ihm{end}.Col           = 1:27;
ihm{end}.Style         = 'frame';
ihm{end}.Tag           = 'ancre cli_image entete';

%% Ancre du composant clc_image

ihm{end+1}.Lig		   = 5:40;
ihm{end}.Col		   = 1:28;
ihm{end}.Style	       = 'frame';
ihm{end}.Tag		   = 'ancre cli_image image';

%% Ancre du composant Save

ihm{end+1}.Lig         = LigSave;
ihm{end}.Col           = ColSave;
ihm{end}.String        = 'Save';
ihm{end}.Style         = 'pushbutton';
ihm{end}.Callback      = 'cli_image.callback(''Save'')';
ihm{end}.Tag		   = 'ancre cli_image Save';

%% Ancre du bouton Quit

ihm{end+1}.Lig         = LigQuit;
ihm{end}.Col           = ColQuit;
ihm{end}.String        = 'Quit';
ihm{end}.Style         = 'pushbutton';
ihm{end}.Callback      = 'cli_image.callback(''Quit'')';
ihm{end}.Tag		   = 'ancre cli_image Quit';

%% Cr�ation de la frame

this.ihm.frame.globale = cl_frame([]);
this.ihm.frame.globale = set_inset_x(this.ihm.frame.globale, 2);
this.ihm.frame.globale = set_inset_y(this.ihm.frame.globale, 2);
this.ihm.frame.globale = set(this.ihm.frame.globale, 'MaxCol', 29);
this.ihm.frame.globale = set_tab_uicontrols(this.ihm.frame.globale, ihm);

