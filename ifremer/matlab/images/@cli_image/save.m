function save(this, nomFic)

nomDir = fileparts(nomFic);
if ~exist(nomDir, 'dir')
    [parent, fils] = fileparts(nomDir);
    status = mkdir(parent, fils);
    if ~status
        str = sprintf('Erreur creation fichier %s', nomDir);
        my_warndlg(str, 1);
        return
    end
end

save(nomFic, 'this')

for i=1:length(this.Images)
    disp('Ameliorer ici qand y''aura le temps')
    
    a = get_Image(this, i);
    InMem = get(a, 'Memory');
    if ~strcmp(InMem, 'On RAM')
        [nomDir, nomProjet] = fileparts(nomFic);
        nomFicSauve = sprintf('%s%s%s_%02d.mem', nomDir, filesep, nomProjet, i);
        copyfile(nomFic, nomFicSauve)
    end
end
