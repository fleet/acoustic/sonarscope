% Acces en ecriture des propriétés d'une instance
% 
% Syntax
%   this = set(this, ...)
%
% Input Arguments
%   this : instance de clc_edit_texte
%
% Name-Value Pair Arguments
%   cf. cl_image
%
% Output Arguments
%   this : instance initialisee
%
% Examples
%   a = cli_image;
%   set(a, 'Image', Lena)
%   a
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

%% Traitement des message de la classe

[varargin, this.visuAxes]       = getPropertyValue(varargin, 'visuAxes',     this.visuAxes);
[varargin, this.visuGrid]       = getPropertyValue(varargin, 'visuGrid',     this.visuGrid);
[varargin, this.visuProfilX]    = getPropertyValue(varargin, 'visuProfilX',  this.visuProfilX);
[varargin, this.visuProfilY]    = getPropertyValue(varargin, 'visuProfilY',  this.visuProfilY);
[varargin, this.visuColorbar]   = getPropertyValue(varargin, 'visuColorbar', this.visuColorbar);
[varargin, this.visuLim]        = getPropertyValue(varargin, 'visuLim',      this.visuLim);
[varargin, this.ZoneEtude]      = getPropertyValue(varargin, 'ZoneEtude',    this.ZoneEtude);

[varargin, this.indImage]       = getPropertyValue(varargin, 'indImage',     this.indImage);
[varargin, this.identVisu]      = getPropertyValue(varargin, 'identVisu',    this.identVisu);
[varargin, this.NbVoisins]      = getPropertyValue(varargin, 'NbVoisins',    this.NbVoisins);
[varargin, this.repExport]      = getPropertyValue(varargin, 'repExport',    this.repExport);

[varargin, this.TypeInteractionSouris]  = getPropertyValue(varargin, 'TypeInteractionSouris',    this.TypeInteractionSouris);

[varargin, this.visuLimHistoryPointer]  = getPropertyValue(varargin, 'visuLimHistoryPointer',   this.visuLimHistoryPointer);
[varargin, this.visuLimHistory]         = getPropertyValue(varargin, 'visuLimHistory',          this.visuLimHistory);
[varargin, this.profilY.Type]           = getPropertyValue(varargin, 'profilY.Type',            this.profilY.Type);

[varargin, this.valBeforeSetNaN] = getPropertyValue(varargin, 'valBeforeSetNaN', this.valBeforeSetNaN);

[varargin, Cross] = getPropertyValue(varargin, 'Point',  this.Point);

[varargin, Images] = getPropertyValue(varargin, 'Images',  []); %#ok<ASGLU>

if ~isempty(Images)
    this.Images = Images;

    for k=1:length(this.Images)
        XLim = this.Images(k).XLim;
        YLim = this.Images(k).YLim;
        this.visuLim(k, 1:2) = XLim;
        this.visuLim(k, 3:4) = YLim;
        this.visuLimHistory{k}{1} = this.visuLim(k, :);
        this.visuLimHistoryPointer(k) = 1;

        if isempty(Cross)
            this.Point(k, 1:2) = [sum(XLim)/2 sum(YLim)/2];
        else
            this.Point(k, 1:2) = Cross;
        end

        if length(this.identVisu) < k
            this.identVisu(k) = 1;
        end
    end
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
