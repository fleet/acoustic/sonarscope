% Recuperation d'une image particuliere dans une instance
%
% Syntax
%   a = get_Image(a, indice, I)
%
% Input Arguments
%   a      : instance de cli_image
%   indice : Indice de l'image
%   I      : Instance de cl_image
%
% Output Arguments
%   a : instance de cli_image
%
% Examples
%   b = cli_image;
%   a = cl_image('Image', Lena, 'Name', 'Lena', 'YDir', 2);
%   b = add_Image(b, a);
%
%   a = cl_image.import_Prismed;
%   b = add_Image(b, a);
%
%   I = get_Image(b, 1);
%   b = set_Image(b, 2, I+1);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = set_Image(this, indice, Image)

this.Images(indice) = Image;
