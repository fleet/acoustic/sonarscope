% Table de couleur adaptee a la mer et a la terre pour representation de type CNES (couleurs bleutees)
%
% Syntax
%   c = CNESjet
% 
% Output Arguments 
%   c : Table de couleur 
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(CNESjet)
%
% See also CNESjet2 CNESjet3 CNES1 CNES3 ColorSea ColorSeaEarth1 ColorSeaEarth2 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------
 
function c = CNESjet(varargin)
c = ColorSeaEarth2(3, varargin{:});
