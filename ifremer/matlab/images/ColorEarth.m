% Table de couleur adaptee a la mer et a la terre pour representation de type CNES (couleurs bleutees)
%
% Syntax
%   c = ColorEarth
%   c = ColorEarth(nbCoul)
%
% Name-Value Pair Arguments
%   nbCoul : Nombre de couleurs (64 par defaut)
%
% Output Arguments
%   c : Table de couleur
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(ColorEarth)
%
% See also ColorSea CNES1 CNES2 CNES3 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------

function c = ColorEarth(varargin)

[varargin, indice] = getPropertyValue(varargin, 'indice', 1); %#ok<ASGLU>

switch indice
    case 1
        nomFic = getNomFicDatabase('chemin256_1024_150_60.byt');
    case 2
        nomFic = getNomFicDatabase('chemin64_256_180_60_6.byt');
    case 3
        nomFic = getNomFicDatabase('chemin256_2048_180_60_6.byt');
end

table = lecTableCNES(nomFic);
Ntable = size(table, 1);

NColors =64;
CnesTronque = zeros(NColors, 3);
for i=1:3
    pppp = interp1(1:Ntable, table(:,i), linspace(1, Ntable, NColors));
    CnesTronque(:,i) = pppp';
end
c = CnesTronque;
