% Table de couleur adaptee a la mer pour representation de type CNES (couleurs bleutees)
%
% Syntax
%   c = ColorSea
%   c = ColorSea(nbCoul)
% 
% Name-Value Pair Arguments 
%   nbCoul : Nombre de couleurs (64 par defaut)
%
% Output Arguments 
%   c : Table de couleur 
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   Z(Z > 0) = NaN; imagesc(Z); axis xy; colorbar
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(ColorSea)
%
% See also ColorSeaEarth1 ColorSeaEarth2 CNES1 CNES2 CNES3 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------

function c = ColorSea(varargin)

cjet = jet(64*4);
nbCoulIn = 28*4;
cjet = cjet(1:nbCoulIn,:);

if nargin == 0
    nbCoulOut = 128;%64;
else
    nbCoulOut = varargin{1};
end

c = zeros(nbCoulOut, 3);
for i=1:3
    map = interp1(1:nbCoulIn, cjet(:,i), linspace(1, nbCoulIn, nbCoulOut));
    c(:,i) = map';
end
