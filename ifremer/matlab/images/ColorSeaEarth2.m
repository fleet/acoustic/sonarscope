% Table de couleur adaptee a la mer et a la terre pour representation de type CNES (couleurs bleutees)
%
% Syntax
%   c = ColorSeaEarth2
%   c = ColorSeaEarth2(nbCoul)
% 
% Name-Value Pair Arguments 
%   nbCoul : Nombre de couleurs (64 par defaut)
%
% Output Arguments 
%   c : Table de couleur 
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(ColorSeaEarth2)
%
% See also ColorSea ColorSeaEarth1 CNES1 CNES2 CNES3 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------
 
function c = ColorSeaEarth2(varargin)

% str = 'This colormap is adapted to earth visualisation with a symetric contrast enhancement  Ex : [-6000 6000]. Change the values if necessary using "contrast enhancement" icone and "Edition" item.';
% my_warndlg(str, 0, 'Tag', 'SysmetricolorTable');

if nargin == 0
    indice = 1;
else
    indice = varargin{1};
end

switch indice
    case 1
        nomFic = getNomFicDatabase('chemin256_1024_150_60.byt');
        table = lecTableCNES(nomFic);
    case 2
        nomFic = getNomFicDatabase('chemin64_256_180_60_6.byt');
        table = lecTableCNES(nomFic);
    case 3
        nomFic = getNomFicDatabase('chemin256_2048_180_60_6.byt');
        table = lecTableCNES(nomFic);
    case 4
        table = flipud(gray(32));
    case 5
%         table = 1 * ones(32,3);
%         table = repmat([255 227 142]/255, 32, 1);
        table = repmat([255 255 230]/255, 32, 1);
end


cjet = jet(64);
Ncjet = size(cjet, 1);
Ntable = size(table, 1);

if length(varargin) == 2
    NColors = varargin{2} / 2;
else
    NColors = 32;
end

JetTronque  = zeros(NColors, 3);
CnesTronque = zeros(NColors, 3);
for i=1:3
    pppp = interp1(1:Ncjet, cjet(:,i), linspace(1, Ncjet, NColors));
    JetTronque(:,i) = pppp';
    
    pppp = interp1(1:Ntable, table(:,i), linspace(1, Ntable, NColors));
    CnesTronque(:,i) = pppp';
end
    
c = [JetTronque ; CnesTronque];
