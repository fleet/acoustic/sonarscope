% Lecture de la topographie terrestre par sous echantillonnage de etopo
%
% Syntax
%   [Z, lon, lat, Comment, flag] = Etopo(...)
%
% Name-Value Pair Arguments
%   LatLim : Limite en latitude  ([-90 90] par defaut)
%   LonLim : Limite en longitude ([-180 180] par defaut)
%
% Name-Value Pair Arguments
%   Max    : Taille maximale de la matrice Z (2000 par d�faut)
%
% Output Arguments
%   []      : Auto-plot activation
%   Z       : Topographie de Etopo (m)
%   lon     : Longitudes
%   lat     : Latitudes
%   Comment : Commentaire sur l'echantillonnage de la donnee
%
% Examples
%   Etopo;
%
%   [Z, lon, lat] = Etopo;
%   figure; imagesc(lon, lat, Z); colorbar; axis xy
%
%   [Z, lon, lat] = Etopo([43 50], [-5 8]);
%   figure; imagesc(lon, lat, Z, [0 500]); colorbar; axis xy
%
%   [Z, lon, lat] = Etopo([20 50], [-50 60]);
%   figure; imagesc(lon, lat, Z, [0 500]); colorbar; axis xy
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Z, lonOut, latOut, Comment, flag] = Etopo(varargin)

Z       = [];
lonOut  = [];
latOut  = [];
Comment = [];
flag    = 1;

[varargin, Max] = getPropertyValue(varargin, 'Max', 2000);

I0 = cl_image_I0;

if isempty(varargin)
    nomFic = getNomFicDatabase('TerreEtopo.mat');
    Info = loadmat(nomFic, 'nomVar', 'Info');
    Z = loadmat(nomFic, 'nomVar', 'Image');
    lonOut = Info.x;
    latOut = Info.y;
    Comment = 'Extraction etopo5';
else
    LatLim = varargin{1};
    LonLim = varargin{2};
    
    [flag, a] = import_etopo(I0, 'LatLim', LatLim, 'LonLim', LonLim, 'Max', Max);
    if ~flag
        return
    end
    
    Z = get(a, 'Image');
    lonOut = get(a, 'x');
    latOut = get(a, 'y');
    Comment = 'Extraction etopo2';
end

%% Par ici la sortie

if nargout == 0
    figure;
    imagesc(lonOut, latOut, Z); colormap(jet(256)); colorbar; axis xy;
end

