% Reduction automatique des couleurs d'une image en s'appuyant sur une table de couleur.
%
% Syntax
%   FicImageMap2FicImageRGB(ficIn, FicOut, map, 'Niveaux', 16)
% 
% Input Arguments 
%   ficIn  : Nom du fichier image a transformer
%   FicOut : Nom du fichier image resultat
%   map    : table de couleur a utiliser pour la reduction
%
% PROPERTY NAMES :
%  'Niveaux' : Nombre de niveaux (64 par defaut)
%
% Examples
%   ficIn = getNomFicDatabase('logoIfremer.tif');
%   FicOut = fullfile(my_tempdir, 'logoIfremerJet.tif');
%   FicImageMap2FicImageRGB(ficIn, FicOut, jet);
%   FicImageMap2FicImageRGB(ficIn, FicOut, jet, 'Niveaux', 16);
%
%   [x, map] = imread(ficIn);
%   figure; imshow(x, map);
%   rgb = imread(FicOut);
%   figure; imshow(rgb);
%
% See also imreadrgb imread rgb2ind ind2rgb Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function FicImageMap2FicImageRGB(ficIn, FicOut, map, varargin)
 
colormap(map);
rgb = imreadrgb(ficIn, varargin(:));
imshow(rgb)
imwrite(rgb, FicOut);
