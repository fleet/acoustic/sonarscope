% 
% for iSigma=1:3
%     cumul = 0;
%     for iFreq=1:3
%         for iTheta=1:8
%             [gb, str, x, y] = GaborRIF(iFreq, iTheta, iSigma, 500);
%             figure(1000+iSigma);
%             subplot(2,2,1); imagesc(x, y, real(gb)); axis equal; axis tight; title(str)
%             subplot(2,2,2); imagesc(x, y, imag(gb)); axis equal; axis tight; title(str)
% %             subplot(2,2,3); imagesc(x, y, abs(gb)); axis equal; axis tight; title(str)
%             igb = ifftshift(ifft2(gb));
%             subplot(2,2,3); imagesc(log10(abs(igb))); axis equal; axis tight;
% 
%             cumul = cumul + abs(igb);
%             subplot(2,2,4); imagesc(log10(cumul)); axis equal; axis tight;
%             pause(0.5)
%             drawnow
%         end
%     end
% end
% 
% 
% 
% nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
% nomFic = getNomFicDatabase('TexturesSonarMontage02.tif');
% nomFic = getNomFicDatabase('TexturesSonarMontage03.tif');
% I = imread(nomFic);
% figure; imagesc(I);
% 
% for iSigma=1:3 % 2:3
%     for iFreq=1:3
%         for iTheta=1:8
%             [GW, str, x, y] = GaborRIF(iFreq, iTheta, iSigma);
%             
%             figure(3340+iSigma);
%             subplot(4,8, (iFreq-1)*8+iTheta), imshow(real(GW) ,[]); axis on;% Show the real part of Gabor wavelets
%             
%             GG1 = filter2(GW, I);
%             GG1 = real(GG1) .^ 2 + imag(GG1) .^ 2;
%             
%             figure(3350+iSigma);
%             subplot(4,8, (iFreq-1)*8+iTheta), imagesc(GG1);
%             title(sprintf('f%d t%d s%d', iFreq, iTheta, iSigma))
%         end
% %         figure(3330+iSigma);
% %         subplot(1, 5, iFreq + 1); imshow(abs(GW),[]); axis on; % Show the magnitude of Gabor wavelets
%     end
% end


function [gb, str, x, y] = GaborRIF(iFreq, iTheta, iSigma, w)

sqrt2 = sqrt(2);
DeuxPi = 2*pi;
freq  = (1 / (sqrt2 * 2 ^ (iFreq-1)));
Theta = (iTheta -1) * (pi/8);

sigma = 2^iSigma * freq * (sqrt2-1);
sigma = 1 / sigma;
str = sprintf('iFreq=%d iTheta=%d iSigma=%d', iFreq, iTheta, iSigma);

if nargin == 3
    w = ceil(6*sigma)+1;
end

R = fix(w/2);
x = -R:R;
y = x;
[X, Y] = meshgrid(x,x);

sigma2 = sigma^2;
gb = (((1/(DeuxPi*sigma2)) * exp(-(X.^2+Y.^2)/(2*sigma2)))) .* exp((DeuxPi*1i*freq)*(X*cos(Theta)+Y*sin(Theta)));
% gb = ((1/(2*pi*sigma2)) * exp(-(X.^2+Y.^2)/(2*sigma2))) * (X*cos(Theta)+1i*Y*sin(Theta));


if nargin == 3
    X1 = sum(abs(gb), 1);
    X1 = cumsum(X1);
    X1 = X1 / max(X1);
    n1 = find(X1 > 0.01, 1, 'first');
    
    X2 = sum(abs(gb), 2);
    X2 = cumsum(X2);
    X2 = X2 / max(X2);
    n2 = find(X2 > 0.01, 1, 'first');
    
    R = length(x);
    sub1 = (n1:R-n1);
    sub2 = (n2:R-n2);
    gb = gb(sub1,sub2);
end
