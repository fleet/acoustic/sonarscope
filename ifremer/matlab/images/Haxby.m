% Table de couleur jet pour la mer et grise pour la terre
%
% Syntax
%   c = Haxby
% 
% Output Arguments 
%   c : Table de couleur 
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(Haxby)
%
% See also NIWA12 NIWA13 CNES1 CNES3 ColorSea ColorSeaEarth1 ColorSeaEarth2 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------

% varargin est obligatoire meme si pas utilise car cette fonction est
% appelee par evalin qui peut aussi appeler une table de couleurs
% necessitant un parametre

function c = Haxby(varargin)

if nargin == 0
    NColors = 256;
else
    NColors = varargin{1};
end

X = [...
0	10	0	121	1	10	0	121
1	40	0	150	2	40	0	150
2	20	5	175	3	20	5	175
3	0	10	200	4	0	10	200
4	0	25	212	5	0	25	212
5	0	40	224	6	0	40	224
6	26	102	240	7	26	102	240
7	13	129	248	8	13	129	248
8	25	175	255	9	25	175	255
9	50	190	255	10	50	190	255
10	68	202	255	11	68	202	255
11	97	225	240	12	97	225	240
12	106	235	225	13	106	235	225
13	124	235	200	14	124	235	200
14	138	236	174	15	138	236	174
15	172	245	168	16	172	245	168
16	205	255	162	17	205	255	162
17	223	245	141	18	223	245	141
18	240	236	121	19	240	236	121
19	247	215	104	20	247	215	104
20	255	189	87	21	255	189	87
21	255	160	69	22	255	160	69
22	244	117	75	23	244	117	75
23	238	80	78	24	238	80	78
24	255	90	90	25	255	90	90
25	255	124	124	26	255	124	124
26	255	158	158	27	255	158	158
27	245	179	174	28	245	179	174
28	255	196	196	29	255	196	196
29	255	215	215	30	255	215	215
30	255	235	235	31	255	235	235
31	255	255	255	32	255	255	255];

map = X(:,2:4);
N = size(map,1);
c = zeros(NColors, 3);
for i=1:3
    c(:,i) = interp1(1:N, map(:,i), linspace(1, N, NColors));
end
c = c/256;
