% Recuperation de l'image de l'epave ImageSonar (image Sonar klein du gesma)
%
% Syntax
%   [I, label] = ImageSonar(num)
%
% INTPUT PARAMETERS : 
%   num : Numero de l'image (1 a 8)
%
% Output Arguments 
%   []    : Auto-plot activation
%   I     : Image de ImageSonar
%   label : Label de l'image
%
% Examples
%   [I, label] = ImageSonar(1);
%   figure; imagesc(I); colormap(gray(256)); colorbar; title(label)
%
%   for i=1:8
%       [I, label] = ImageSonar(i);
%       figure; imagesc(I); colormap(gray(256)); colorbar; title(label)
%   end
%
% See also Lena Swansea Prismed ImageSonar Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [I, label] = ImageSonar(num)

for i=1:length(num)
    nomFic = sprintf('textureSonar%02d.png', num(i));
    nomFic = getNomFicDatabase(nomFic);

    I = imread(nomFic);
    I(I == 0) = 1;

    [~, label] = fileparts(nomFic);

    if nargout == 0
        figure; imagesc(I); colormap(gray(256)); colorbar; title(label)
    end
end
