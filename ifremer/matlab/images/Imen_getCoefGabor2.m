function [V, strV] = Imen_getCoefGabor2

flagVisu = false;

Teta = (0:7) * pi/8;
f0   = 1 ./ [4 8 16] * sqrt(2);
Sigma = 1 ./ [4 8 16 ] * sqrt(2);

nbTeta  = length(Teta);
nbF0    = length(f0);
nbSigma = length(Sigma);
flag = true(nbF0,nbTeta,nbSigma);

% flag(3,:,3) = false;
% flag(3,1:4:end,3) = true;
% 
% flag(3,:,2) = false;
% flag(3,1:3:end,2) = true;
% 
% flag(3,:,1) = false;
% flag(3,1:2:end,1) = true;

% flag(2,:,3) = false;
% flag(2,1:2:end,3) = true;

flag(3,:,3) = false;
flag(3,1:2:end,3) = true;

flag(3,:,2) = false;
flag(3,1:2:end,2) = true;

flag(3,:,1) = false;
flag(3,1:4:end,1) = true;

flag(2,:,2) = false;
flag(2,1:2:end,2) = true;

flag(2,:,1) = false;
flag(2,1:2:end,1) = true;

% flag(1,:,3) = false;
% flag(1,1:2:end,3) = true;


% flag(1,1,2) = false;
% flag(1,1,3) = false;
% flag(1,5,3) = false;
% flag(2,1,3) = false;
% flag(2,5,3) = false;


TetaCercle = 0:360;
xc = cosd(TetaCercle);
yc = sind(TetaCercle);
Coul = 'rgbkcmyr';

V = [];
strV = {};
for if0=1:nbF0
    if flagVisu
        Fig = figure;
    end
    for iTeta=1:nbTeta
        for iSigma=1:nbSigma
%             if flag(if0,iTeta,iSigma)
                V(end+1, :) = [f0(if0) Teta(iTeta) Sigma(iSigma)]; %#ok<AGROW>
                
                strV{end+1} = sprintf('if0=%d  iTeta=%d  iSigma=%d', if0, iTeta, iSigma); %#ok<AGROW>
                
                if flagVisu
                    x = (Sigma(iSigma) * xc) + cos(Teta(iTeta)) * f0(if0);
                    y = (Sigma(iSigma) * yc) + sin(Teta(iTeta)) * f0(if0);
                    figure(Fig); PlotUtils.createSScPlot(x,y,Coul(iSigma)); hold on; grid on; axis equal; title(sprintf('if0=%d', if0))
                end
%             end
        end
    end
end

size(V)

%{
Teta = (0:7) * (180 / 8);
Teta = [0 22.5 45 67.5 90 112.5 135 157.5]
Teta = [0 22.5 45 90 135]

f0 = [4 8 16] * sqrt(2);
Sigma = [4 8 16 32] * sqrt(2)


figure;
for iTeta=1:length(Teta)
    subplot(2,1,1)
    for if0=1:length(f0)
        plot(Teta(iTeta), f0(if0), '+b'); grid on; hold on; xlabel('Teta'), ylabel('f0');
    end
    subplot(2,1,2)
    for iSigma=1:length(Sigma)
        plot(Teta(iTeta), Sigma(iSigma), '+b'); grid on; hold on; xlabel('Teta'), ylabel('Sigma');
    end
end
%}
