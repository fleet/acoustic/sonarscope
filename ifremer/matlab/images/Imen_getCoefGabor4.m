function [V, strV] = Imen_getCoefGabor4

V = [];
strV = {};
for iSigma=1:2
    for iFreq=1:3
        for iTheta=1:8

% for iSigma=0:1
%     for iFreq=0:4
%         for iTheta=0:7
            V(end+1, :) = [iFreq iTheta iSigma]; %#ok<AGROW>
            strV{end+1} = sprintf('if0=%d  iTeta=%d  iSigma=%d', iFreq, iTheta, iSigma); %#ok<AGROW>
        end
    end
end
