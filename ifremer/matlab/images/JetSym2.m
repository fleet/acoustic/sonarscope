% TODO : � supprimer
% Table de couleur jet symetris�e
%
% Syntax
%   c = JetSym2
%
% Output Arguments
%   c : Table de couleur
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(JetSym2)
%
% See also NIWA12 NIWA13 CNES1 CNES3 ColorSea ColorSeaEarth1 ColorSeaEarth2 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------

% varargin est obligatoire meme si pas utilise car cette fonction est
% appelee par evalin qui peut aussi appeler une table de couleurs
% necessitant un parametre

function c = JetSym2(varargin)
c = jet(varargin{:});
c = flipud(c);
c = [c(end:-2:1,:) ; c(1:2:end, :)];
