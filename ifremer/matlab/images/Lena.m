% Recuperation de l'image de Lena
%
% Syntax
%   [I, label] = Lena
%
% Output Arguments 
%   [] : Auto-plot activation
%   I  : Image de Lena
%   label : Label de l'image
%
% Examples
%   I = Lena;
%   figure; imagesc(I); colormap(gray(256)); colorbar;
%
% See also Swansea Prismed ImageSonar Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = Lena

nomFic = getNomFicDatabase('lena.tif');
I = imread(nomFic);
label = 'Lena';

if nargout == 0
    a = cl_image('Image', I, 'Name', label, 'ColormapIndex', 2, 'GeometryType', cl_image.indGeometryType('Unknown'));
    b = cli_image('Images', a);
    editobj(b);
else
    varargout{1} = I;
    varargout{2} = label;
end
