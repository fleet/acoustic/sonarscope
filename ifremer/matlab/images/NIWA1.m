% Table de couleur jet pour la mer et grise pour la terre
%
% Syntax
%   c = NIWA1
%
% Output Arguments
%   c : Table de couleur
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(NIWA1)
%
% See also NIWA12 NIWA13 CNES1 CNES3 ColorSea ColorSeaEarth1 ColorSeaEarth2 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------

% varargin est obligatoire meme si pas utilise car cette fonction est
% appelee par evalin qui peut aussi appeler une table de couleurs
% necessitant un parametre

function c = NIWA1(varargin)
c = ColorSeaEarth2(4, varargin{:});
