function [flag, I, Distances, Unit, alreadyProcessed] = OTUS_ImageReadAndComp(nomFic, TypeCompensation, OrdrePolyCompens, nomDirImagesCompensee, Correction)

% Unit = [];
alreadyProcessed = 0;

[~, nomFic1, ext] = fileparts(nomFic);

%% Lecture de l'image

if TypeCompensation ~= 1
   	nomFicImageComp = fullfile(nomDirImagesCompensee, [nomFic1 ext]);
   	nomFicDistancesComp = fullfile(nomDirImagesCompensee, [nomFic1 '_Dist' ext]);
    if exist(nomFicImageComp, 'file') && exist(nomFicDistancesComp, 'file')
        I         = imread(nomFicImageComp);
        Distances = imread(nomFicDistancesComp);
    	Unit = 'dB';
        alreadyProcessed = 1;
        flag = 1;
        return
    end
     
    %{
    nomFicMat = fullfile(nomDirImagesCompensee, [nomFic1 '_Image.mat']);
    if exist(nomFicMat, 'file') % Cas o� on a s�lectionn� les .tif qui se trouvent dans le r�pertoire de sauvegarde
        % des donn�es compens�es. Dans ce cas, on prend les .mat
        nomFicComp = fullfile(nomDirImagesCompensee, [nomFic1 '_Image.mat']);
        [X, flag] = import_matlab(I0, nomFicComp, 'Memmapfile', 'No');
        if flag
            nomFicComp = fullfile(nomDirImagesCompensee, [nomFic1 '_Distances.mat']);
            [Y, flag] = import_matlab(I0, nomFicComp, 'Memmapfile', 'No');
            if flag
                Distances = get(Y, 'Image');
                Unit = 'dB';
                alreadyProcessed = 1;
            end
        end
        if alreadyProcessed
            switch nargout
                case {0; 1}
                    return
                case 2
                    I = get(X, 'Image');
                otherwise
                    I = get(X, 'Image');
                    Distances = get(Y, 'Image');
                    Unit = 'dB';
            end
            return
        end
    end
    %}
end

%% Cas o� la compensation n'a pas encore �t� faite

I = imread(nomFic);
% I = my_fliplr(I); % TODO : trouver pourquoi j'avais mis �a. OTUS ???
Distances = [];
Unit = [];

I = single(I(:,:,:));
[nbRows, nbCol, nbCan] = size(I); %#ok<ASGLU>

%% Compensation de l'image

switch TypeCompensation
    case 1 % On ne fait rien
        if isempty(Unit)
            Unit = 'Amp';
        end
    case 2 % Compensation par un fichier unique (utie lorsque les photos sont prises � partir d'un chassis pos� sur le fond)
        if ~isempty(Correction)
            I = I - Correction;
        end
        Unit = 'Amp';
    case 3
        a = cl_image('Image', I, 'Memmapfile', 'No');
        if a.ImageType == 2
            [a, flag] = RGB2Intensity(a);
            if ~flag
                return
            end
        end
        a.Unit = 'Enr';
        [a, flag] = reflec_Enr2dB(a);
        if ~flag
            return
        end
        a.ColormapIndex = 2;
        
        %             I = log10(I);
        %             a = cl_image('Image', I, 'ColormapIndex', 2);
        ValLim = [];
        [flag, b] = compensation(a, 'Type', 4, 'pX', OrdrePolyCompens, 'pY', OrdrePolyCompens, ...
            'dispCoeffPoly', 0, 'ValLim', ValLim, 'SeuilCompPoly', 3);
        if ~flag
            return
        end
        I = get(b(2), 'Image');
        Sigma = std(I(:), 'omitnan');
        I = I / Sigma;
        
        % Test pour voir si �a r�soud le pb de memmapfile qui reste
        clear a
        a = b;
        clear b
        Unit = 'dB';
end

%% Cr�ation de la matrice des distances servant au recouvrement des images lors du mosa�quage

if isempty(Distances)
    if TypeCompensation == 3
        Distances  = get(a(1), 'Image');
        StatValues = a(1).StatValues;
        Distances  = 1 - (Distances(:,:,1) / StatValues.Max);
    else
        Distances = NaN(nbRows, nbCol, 'single');
        x = (1:nbCol) - nbCol/2;
        y = (1:nbRows) - nbRows/2;
        for iLig = 1:nbRows
            Distances(iLig,:) = sqrt(y(iLig)^2 + x.^2);
        end
    end
    % figure; imagesc(Distances)
    % figure; imagesc(Distances)
end

%% Sauvegarde de l'image compens�e et de l'image servant au recouvrement des images lors du mosa�quage

if ~isempty(nomDirImagesCompensee)
    
    %% Quantification pour gagner en place lors de l'export en .mat
    
    Coef = 500;
    I = floor(I*Coef) / Coef;
    I = uint8(128+I*20);
    
    Distances = floor(Distances*Coef) / Coef; 
    Distances = uint8(128+Distances*250);
    
    %% Sauvegarde de l'image
    
    %{
    nomFicComp = fullfile(nomDirImagesCompensee, [nomFic1 '_Image.mat']);
    DataType = cl_image.indDataType('Reflectivity');
    GeometryType = cl_image.indGeometryType('OrthoYX');
    X = cl_image('Image', I, 'GeometryType', GeometryType, 'DataType', DataType, 'ColormapIndex', 2, 'Memmapfile', 'No');
    X = update_Name(X, 'Name',  nomFic1);
    export_matlab_array(X, nomFicComp)
    
    %% Sauvegarde de la matrice des distances
    
    nomFicComp = fullfile(nomDirImagesCompensee, [nomFic1 '_Distances.mat']);
    DataType = cl_image.indDataType('TxAngle');
    X = cl_image('Image', Distances, 'GeometryType', GeometryType, 'DataType', DataType, 'ColormapIndex', 3, 'Memmapfile', 'No');
    X = update_Name(X, 'Name',  nomFic1);
    export_matlab_array(X, nomFicComp)
    clear X
    
    nomFicComp = fullfile(nomDirImagesCompensee, [nomFic1 ext]);
    %}
    imwrite(I, nomFicImageComp)
    imwrite(Distances, nomFicDistancesComp)
end

flag = 1;
