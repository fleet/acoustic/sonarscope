function [flag, repImport] = Pelevements_plot(repImport)

%% Saisie des noms de fichiers image

[flag, ListeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.xyz'; '.*'}, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select some files');
if ~flag
    return
end

%% Lecture des prélévements

Prelevements = [];
for k=1:length(ListeFic)
    [flag, P] = lecFchierPrelevements(ListeFic{k});
    Prelevements = [Prelevements, P]; %#ok<AGROW>
end
Name = {Prelevements.Name};
Lat  = [Prelevements.Lat];
Lon  = [Prelevements.Lon];
Num  = [Prelevements.Num];
% Description = {Prelevements.Description};

%% Recherche des classes

[Classes, sub] = unique(Num);
maxClasses = max(Classes);
Legend = Name(sub);

%% Tracé graphique

figure; 
% colors = jet(maxClasses+1);
Colors = get(gca, 'ColorOrder');
for k=1:length(Classes)
    sub = (Num == k);
    if any(sub)
        hp = PlotUtils.createSScPlot(Lon(sub), Lat(sub), '*');
        set(hp, 'Color', Colors(k,:))
        grid on; hold on;
    end
end
colormap(Colors(1:maxClasses,:))
colorbar_imageIndexee('Offset', 0.5, 'Names', Legend)
