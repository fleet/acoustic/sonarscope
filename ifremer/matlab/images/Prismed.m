% DTM from Prismed survey, résolution 500 m (EM12, Jean-Mascle, Geosciences-Azur)
%
% Syntax
%   [z, x, y, LatLim, LonLim] = Prismed
%
% Output Arguments
%   []     : Auto-display
%   z      : Bathymetrie (m)
%   x      : Abscisses (m)
%   y      : Ordonnees (m)
%   LatLim : Limits in latitude
%   LonLim : Limits in longitude
%
% Examples
%   Prismed
%
%   [z, x, y, LatLim, LonLim] = Prismed;
%   figure; imagesc(x, y, z); axis xy; colormap(gray(256)); colorbar;
%
% See also Lena Swansea ImageSonar Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [z, x, y, LatLim, LonLim, nomFic, InitialFileFormat] = Prismed

z      = [];
x      = [];
y      = [];
LatLim = [];
LonLim = [];

msg_copyright('Prismed', 'Geosciences Azur')
nomFic = getNomFicDatabase('EM12D_PRISMED_125m_LatLong_Bathymetry.ers');
if isempty(nomFic)
    return
end
InitialFileFormat = 'ERS';

[flag, a] = cl_image.import_ermapper(nomFic);
if ~flag
    return
end

if nargout == 0
    SonarScope(a)
else
    z = get(a, 'Image');
    x = get(a, 'x');
    y = get(a, 'y');
    LatLim = get(a, 'YLim');
    LonLim = get(a, 'XLim');
end
