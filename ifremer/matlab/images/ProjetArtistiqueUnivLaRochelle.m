% nomDir = 'F:\UNIV-LR\mnt-individuels';
% listeFic = listeFicOnDir(nomDir, '.mnt');
% ProjetArtistiqueUnivLaRochelle(listeFic)

function [flag, M] = ProjetArtistiqueUnivLaRochelle(listeFic)

if ischar(listeFic)
    listeFic = {listeFic};
end

nomDir = fileparts(listeFic{1});

%% Lecture des fichiers

I0 = cl_image_I0;
nbFic = length(listeFic);
str1 = 'Importation des fichiers MNT';
str2 = 'Importing MNT files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw)
    [flag, a(k)] = import(I0, listeFic{k}); %#ok<AGROW>
    if ~flag
        return
    end
end
my_close(hw)
     
%% R�alisation de la mosa�que

[~, M] = mergeMosaics(a, 'Angle', [], 'CoveringPriority', 5, 'bufferZoneSize', 10);
M.ColormapIndex = 20;
MShade = sunShadingGreatestSlope(M, 1, 'subx');
% SonarScope([a M MShade])

%% Cr�ation des masques

str1 = 'Cr�ation des masques';
str2 = 'Creating masks';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw)
    b(k) = ROI_auto(a(k), 1, 'Mask', [], [], [-Inf Inf], 1); %#ok<AGROW>
end
my_close(hw)

%% Cr�ation des images

MImage = MShade;
str1 = 'Cr�ation des masques';
str2 = 'Creating images';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=(nbFic-1):-1:1
    my_waitbar(k, nbFic, hw)
    MImage = masquage(MImage, 'LayerMask', b(k), 'valMask', 1, 'zone', 2);
%     imagesc(MImage)
    nomFic = sprintf('MNT-%03d.jpg', k);
    nomFic = fullfile(nomDir, nomFic);
    export_gif(MImage, nomFic);
end
my_close(hw)
