% Export the Region(s) Of Interest of an image in a Google-Earth .kml or .kmz file
%
% Syntax
%   flag = ROI_export_mat(ROI, FileName)
%
% Input Arguments
%   ROI      : Structure containing the ROIs
%   FileName : Name of Matlab file containing the ROI
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     RegionOfInterest = a.RegionOfInterest;
%     nomFicOut = my_tempname('.mat')
%   flag = ROI_export_mat(RegionOfInterest, nomFicOut)
%     [flag, b] = import(cl_image, nomFic);
%     fig = imagesc(b);
%     [flag, b] = ROI_import_mat(b, nomFicOut);
%     ROI_plot(b, 'Fig', fig)
%
% See also ROI_export_xml ROI_export_mat ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = ROI_export_kml(ROI, nomFicSave)

flag = 0;

[nomDir, NomDocument, extNomFicSave]   = fileparts(nomFicSave);
nomFicKml    = fullfile(nomDir, [NomDocument '.kml']);
nomFicKmzZip = my_tempname;

[codes, codesInd] = unique([ROI(:).Num]);

for k=1:length(codes)
    C = floor( 255 * ROI(codesInd(k)).Color);
    CodeCouleur{k} = sprintf('%02x', fliplr(C)); %#ok<AGROW>
end

%% Cr�ation du fichier

fid = fopen(nomFicKml, 'wt');
if fid == -1
    messageErreurFichier(nomFicKml, 'WriteFailure');
    return
end
fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid, '<kml xmlns="http://earth.google.com/kml/2.0"\n');
fprintf(fid, 'xmlns:gx="http://www.google.com/kml/ext/2.2">   <!-- required when using gx-prefixed elements -->\n');
%fprintf(fid, '<kml xmlns="http://www.opengis.net/kml/2.2"
%xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n');
fprintf(fid, '<Document>\n');
fprintf(fid, '  <name>%s</name>\n', NomDocument);

%% D�finition des styles pour la navigation

for k=1:length(codes)
    fprintf(fid, '  <StyleMap id="poly_style_%d">\n', codes(k));
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>normal</key>\n');
    fprintf(fid, '      <styleUrl>#normal_polygon_%d</styleUrl>\n', codes(k));
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>highlight</key>\n');
    fprintf(fid, '      <styleUrl>#highlighted_polygon_%d</styleUrl>\n', codes(k));
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '  </StyleMap>\n');
    
    fprintf(fid, '  <Style id="highlighted_polygon_%d">\n', codes(k));
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    fprintf(fid, '      <width>3</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
    
    fprintf(fid, '  <Style id="normal_polygon_%d">\n', codes(k));
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k}); % 50% transparent
    fprintf(fid, '      <width>1</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    
%     fprintf(fid, '    <PolyStyle>\n');
%     fprintf(fid, '      <color>%s%s</color>\n', sprintf('%02x', floor(255*1)), CodeCouleur{k});
%     fprintf(fid, '    </PolyStyle>\n');
    
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <fill>0</fill>\n');
    fprintf(fid, '    </PolyStyle>\n');
    
    fprintf(fid, '  </Style>\n');
end

for k1=1:length(codes)
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>%s</name>\n', ROI(codesInd(k1)).Name );
    
    %% Trac� des couvertures
    ROI2write = ROI( [ROI(:).Num] == codes(k1) );
    
    for k2=1:length(ROI2write)
        d = [ROI2write(k2).pX; ROI2write(k2).pY];
        fprintf(fid, '  <Placemark>\n');
        fprintf(fid, '      <name>%s</name>\n', ROI2write(k2).Name);
        if isfield(ROI2write(k2), 'description')
            fprintf(fid, '      <description>%s</description>\n', ROI2write(k2).description);
        else
            fprintf(fid, '      <description>%s</description>\n', ROI2write(k2).Name);
        end
        fprintf(fid, '      <styleUrl>#poly_style_%d</styleUrl>\n', ROI2write(k2).Num);
        fprintf(fid, '      <Polygon>\n');
        fprintf(fid, '          <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n'); % Rajout� par JMA le 16/10/2018
        fprintf(fid, '          <extrude>0</extrude>\n');
        fprintf(fid, '          <outerBoundaryIs>\n');
        fprintf(fid, '              <LinearRing>\n');
        fprintf(fid, '                  <gx:altitudeMode>clampToSeaFloor</gx:altitudeMode>\n');
        fprintf(fid, '                  <coordinates>\n');
        fprintf(fid, '                      %.6f,%.6f,0.0\n', d);
        fprintf(fid, '                  </coordinates>\n');
        fprintf(fid, '              </LinearRing>\n');
        fprintf(fid, '          </outerBoundaryIs>\n');
        fprintf(fid, '      </Polygon>\n');
        fprintf(fid, '  </Placemark>\n');
    end
    fprintf(fid, '</Folder>\n');
end

%% Fermeture du fichier

fprintf(fid, '</Document>\n');
fprintf(fid, '</kml>\n');
fclose(fid);

%% Conversion du .kml en .kmz

if strcmp(extNomFicSave, '.kmz')
    
    % TODO : Tester zipKml2kmz
    % flag = zipKml2kmz(nomFicKmz, DirTemp)
    
    zip(nomFicKmzZip, nomFicKml)
    flag = copyfile([nomFicKmzZip, '.zip'], nomFicSave);
    if ~flag
        str = sprintf('Impossible to copy file %s to %s.', [nomFicSave, '.zip'], nomFicSave);
        my_warndlg(str, 1);
        return
    end
    delete([nomFicKmzZip, '.zip'])
    delete(nomFicKml)
    
    %% Visualisation du .kmz dans Google-Earth
    
    winopen(nomFicSave)
else
    winopen(nomFicKml)
end

flag = 1;
