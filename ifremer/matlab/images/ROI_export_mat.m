% Export the Region(s) Of Interest of an image in a Matlab file
%
% Syntax
%   flag = ROI_export_mat(ROI, FileName)
%
% Input Arguments
%   ROI      : Structure containing the ROIs
%   FileName : Name of Matlab file containing the ROI
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     RegionOfInterest = a.RegionOfInterest;
%     nomFicOut = my_tempname('.mat')
%   flag = ROI_export_mat(RegionOfInterest, nomFicOut)
%     [flag, b] = import(cl_image, nomFic);
%     fig = imagesc(b);
%     [flag, b] = ROI_import_mat(b, nomFicOut);
%     ROI_plot(b, 'Fig', fig)
%
% See also ROI_export_xml ROI_export_kml ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = ROI_export_mat(RegionOfInterest, nomFicSave)
save(nomFicSave, 'RegionOfInterest');
flag = 1;