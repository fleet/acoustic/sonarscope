% Export the Region(s) Of Interest of an image in a shape file (ESRI)
%
% Syntax
%   flag = ROI_export_shp(ROI, FileName)
%
% Input Arguments
%   ROI      : Structure containing the ROIs
%   FileName : Name of Matlab file containing the ROI
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     RegionOfInterest = a.RegionOfInterest;
%     nomFicOut = my_tempname('.mat')
%   flag = ROI_export_mat(RegionOfInterest, nomFicOut)
%     [flag, b] = import(cl_image, nomFic);
%     fig = imagesc(b);
%     [flag, b] = ROI_import_mat(b, nomFicOut);
%     ROI_plot(b, 'Fig', fig)
%
% See also ROI_export_xml ROI_export_kml ROI_export_mat Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = ROI_export_shp(ROI, nomFicSave)

[nomDir, NomDocument] = fileparts(nomFicSave);
nomFicShp = fullfile(nomDir, [NomDocument '.shp']);

%% D�finition de la structure

N = length(ROI);
for k1=N:-1:1
    XLim = [min(ROI(k1).pX) max(ROI(k1).pX)];
    YLim = [min(ROI(k1).pY) max(ROI(k1).pY)];
    shapes(k1).Geometry    = 'Polygon'; % PolyLine';
    shapes(k1).BoundingBox = [XLim' YLim'];
    shapes(k1).X           = ROI(k1).pX;
    shapes(k1).Y           = ROI(k1).pY;
%     shapes(k1).Lon           = ROI(k1).pX;
%     shapes(k1).Lat           = ROI(k1).pY;
    shapes(k1).Attributes(1).Name = 'Label';
    shapes(k1).Attributes(1).Type = 'double';
    shapes(k1).Label       = ROI(k1).Num;
%     shapes(k1).Name  = ROI(k1).Name;
%     shapes(k1).Color = num2strCode(ROI(k1).coul);
end

%% Cr�ation du fichier

oldWarn = warning('off', 'map:validate:unsupportedDataClass'); % Exception rajout�e par JMA le 24/06/2019
shapewrite(shapes, nomFicShp)
warning(oldWarn)

flag = exist(nomFicShp, 'file');
if ~flag
    messageErreur(nomFicShp)
    return
end
