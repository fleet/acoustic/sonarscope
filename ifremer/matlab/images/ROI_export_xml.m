% Export the Region(s) Of Interest of an image in a XML file
%
% Syntax
%   flag = ROI_export_xml(ROI, FileName)
%
% Input Arguments
%   ROI      : Structure containing the ROIs
%   FileName : Name of the Google-Earth file containing the ROI
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%     nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%     [flag, a] = import(cl_image, nomFic);
%     nomFicROI_kmz = getNomFicDatabase('ROI_EM12D_PRISMED.kmz');
%     [flag, a] = ROI_import_kml(a, nomFicROI_kmz);
%     RegionOfInterest = a.RegionOfInterest;
%     nomFicOut = my_tempname('.xml')
%   flag = ROI_export_xml(RegionOfInterest, nomFicOut)
%     [flag, b] = import(cl_image, nomFic);
%     fig = imagesc(b);
%     [flag, b] = ROI_import_xml(b, nomFicOut);
%     ROI_plot(b, 'Fig', fig)
%
% See also ROI_export_kml ROI_export_mat ROI_export_shp Authors
%    (class) ROI_manual ROI_auto ROI_plot ROI_export ROI_import
%            ROI_import_xml ROI_import_mat ROI_import_shp
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = ROI_export_xml(RegionOfInterest, nomFicSave)

if isempty(RegionOfInterest)
    str1 = 'L''image ne contient pas de r�gion d''int�r�t.';
    str2 = 'The image does not contain any Region Of Interest.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

k = 0;
for j=1:length(RegionOfInterest)
    k = k + 1;
    ROI(k).pX  = RegionOfInterest(j).pX; %#ok<AGROW>
    ROI(k).pY  = RegionOfInterest(j).pY; %#ok<AGROW>
    ROI(k).Num = RegionOfInterest(j).Num; %#ok<AGROW>
    if isfield(RegionOfInterest(j), 'Color')
        ROI(k).Color = RegionOfInterest(j).Color; %#ok<AGROW>
    end
    if isfield(RegionOfInterest(j), 'Name')
        ROI(k).Name = RegionOfInterest(j).Name; %#ok<AGROW>
    end
    if isfield(RegionOfInterest(j), 'Comment')
        ROI(k).Comment = RegionOfInterest(j).Comment; %#ok<AGROW>
    end
end

Data.Signature1 = 'SonarScope';
Data.Signature2 = 'ROI'; 
Data.ROI = ROI;

DPref.ItemName  = 'item';
DPref.StructItem = false;
DPref.CellItem   = false;
try
    xml_write(nomFicSave, Data, 'Stats', DPref);
    flag = 1;
catch
    flag = 0;
end
