% Table de couleur adapt�e � la sismique
%
% Syntax
%   c = Seismics
% 
% Output Arguments 
%   c : Table de couleur 
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(Seismics)
%
% See also NIWA12 NIWA13 CNES1 CNES3 ColorSea ColorSeaEarth1 ColorSeaEarth2 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------

% varargin est obligatoire meme si pas utilise car cette fonction est
% appelee par evalin qui peut aussi appeler une table de couleurs
% necessitant un parametre

function c = Seismics(varargin)

if nargin == 0
    NColors = 256;
else
    NColors = varargin{1};
end

M1 = jet(NColors);
M2 = gray(NColors);
M2 = my_flipud(M2);
c =  linspace(1,0,NColors)';
c = c .^ 0.75;
% figure(8786); plot(c); grid
for k=1:3
    M4(:,k) = (1-c) .* M1(:,k) + c .* M2(:,k); %#ok<AGROW>
end
c = M4;
