% Table de couleur adapt�e � la sismiquee
%
% Syntax
%   c = SeismicsSym
%
% Output Arguments
%   c : Table de couleur
%
% Examples
%   [Z, lon, lat] = Etopo;
%   Zmin = min(Z(:)); Zmax = max(Z(:)); Zlim = max(abs([Zmin Zmax]));
%   figure; imagesc(lon, lat, Z, [-Zlim Zlim]); axisGeo;
%   colormap(SeismicsSym)
%
% See also NIWA12 NIWA13 CNES1 CNES3 ColorSea ColorSeaEarth1 ColorSeaEarth2 SeaEarth colormap Authors
% Authors : JMA
% -------------------------------------------------------------------------------

% varargin est obligatoire meme si pas utilise car cette fonction est
% appelee par evalin qui peut aussi appeler une table de couleurs
% necessitant un parametre

function c = SeismicsSym(varargin)

c = Seismics(varargin{:});
c = [c(end:-2:1,:) ; c(1:2:end, :)];