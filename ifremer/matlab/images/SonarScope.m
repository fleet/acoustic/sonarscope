% Analyseur d'images sonar (editobj(cli_image))
%
% Syntax
%   b = SonarScope(...)
%
% Name-Value Pair Arguments
%   a : Objet Image
%
% Output Arguments
%   [] : Auto-plot activation
%   b  : Objet Image
%
% Examples
%   SonarScope
%   a = SonarScope;
%   SonarScope(a)
%   a = SonarScope(a);
%
% See also cli_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = SonarScope(varargin)

[varargin, ColormapIndex] = getPropertyValue(varargin, 'ColormapIndex', []);
[varargin, ButtonSave]    = getPropertyValue(varargin, 'ButtonSave',    []);

if isempty(ButtonSave)
    ButtonSave = 0;
    NARGIN = nargin;
else
    NARGIN = nargin - 2;
end

if isempty(ColormapIndex)
    ColormapIndex = 3;
else
    NARGIN = NARGIN - 2;
end

format long
deleteOldLogFiles
garbageCollector(cl_memmapfile.empty)

if isdeployed
    if NARGIN == 0
        try
            this = editobj(cli_image); %#ok<NASGU>
        catch ME
            msg = getReport(ME, 'extended', 'hyperlinks', 'off');
            fprintf('%s', msg);
            SendEmailSupport(msg)
        end
    else
        b = cl_image.empty;
        
        for k=1:length(varargin)
            if ischar(varargin{k})
                [pathname, filename, ext] = fileparts(varargin{k});
                if strcmp(ext, '.gz')
                    if exist(varargin{k}, 'file')
                        disp(['Uncompress : ' varargin{k} ' Please wait'])
                        gunzip(varargin{k}, pathname)
                        delete(varargin{k})
                    end
                    varargin{k} = fullfile(pathname, filename);
                end
                if strcmp(ext, '.zip')
                    if exist(varargin{k}, 'file')
                        disp(['Uncompress : ' varargin{k} ' Please wait'])
                        unzip(varargin{k}, pathname)
                        delete(varargin{k})
                    end
                    varargin{k} = fullfile(pathname, filename);
                end
                if exist(varargin{k}, 'file')
                    [flag, a] = import(cl_image, varargin{k});
                    if flag
                        b(end+1) = a; %#ok<AGROW>
                    end
                end
            else
                b(end+1) = cl_image('Image', varargin{k}); %#ok<AGROW>
            end
        end
        if ~isempty(b)
            try
                this = SonarScope(b, 'ButtonSave', ButtonSave); %#ok<NASGU>
            catch ME
                msg = getReport(ME, 'extended', 'hyperlinks', 'off');
                fprintf('%s', msg);
                SendEmailSupport(msg)
            end
        end
    end
    return
end

if NARGIN == 0
    if nargout == 0
%         editobj(cli_image([]));
        editobj(cli_image.empty());
    else
        varargout{1} = editobj(cli_image);
    end
else
    if nargout == 0
        if isa(varargin{1}, 'cl_image')
            SonarScope(varargin{:}); % , 'ButtonSave', ButtonSave
        elseif isa(varargin{1}, 'cli_image')
            editobj(varargin{:}, 'ButtonSave', ButtonSave);
        else
            switch NARGIN
                case 1
                    if ischar(varargin{1})
                        [flag, a] = import(cl_image, varargin{1});
                        if ~flag
                            return
                        end
                    else
                        if iscell(varargin{1}) % Pour un appel de type SonarScope({I I I});
                            for k=1:length(varargin{1})
                                ImageName = sprintf('Image %d', k);
                                a(k) = cl_image('Image', varargin{1}{k}, 'Name', ImageName); %#ok<AGROW>
                            end
                        else
                            Image =  varargin{1};
                            ImageName = inputname(1);
                            % GFORGE 347: Correction si l'image est charg�e directement :
                            % �vite les pbs de plantage lors de l'export de
                            % celle-ci.
                            if isempty(ImageName)
                                ImageName = 'Unknown';
                            end
                            sz = size(Image);
                            if (sz(1) == 1) || (sz(2) == 1)
                                str1 = 'La matrice ne comporte qu''une ligne st/ou une colonne, elle ne peut pas �tre repr�sent�e dans SonarScope.';
                                str2 = 'The matrix has only one raw and/or one column, it cannot be displayed with SonarScope.';
                                my_warndlg(Lang(str1,str2), 1);
                                return
                            end
                            
                            if (length(sz) == 3) && (sz(3) ~= 3)
                                TagSynchroX = num2str(rand(1));
                                TagSynchroY = num2str(rand(1));
                                TagSynchroContrast = num2str(rand(1));
                                for k=1:sz(3)
                                    a(k) = cl_image('Image', Image(:,:,k)); %#ok<AGROW>
                                    a(k) = set(a(k), 'Name', sprintf('Image - Channel %d', k)); %#ok<AGROW>
                                    a(k) = set(a(k), 'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY, 'TagSynchroContrast', TagSynchroContrast); %#ok<AGROW>
                                end
                            else
                                a = cl_image('Image', Image, 'Name', ImageName, 'ColormapIndex', ColormapIndex);
                            end
                        end
                    end
                    SonarScope(a, 'ButtonSave', ButtonSave);
                case 3
                    a = cl_image('Image', varargin{3}, 'x', varargin{1}, 'y', varargin{2});
                    SonarScope(a, 'ButtonSave', ButtonSave);
            end
        end
    else
        if isa(varargin{1}, 'cl_image')
            varargout{1} = SonarScope(varargin{:});
        elseif isa(varargin{1}, 'cli_image')
            varargout{1} = editobj(varargin{:});
        else
            switch NARGIN
                case 1
                    if iscell(varargin{1}) % Pour un appel de typr SonarScope({I I I});
                        for k=1:length(varargin{1})
                            ImageName = sprintf('Image %d', k);
                        	a(k) = cl_image('Image', varargin{1}{k}, 'Name', ImageName); %#ok<AGROW>
                        end
                    else
                        a = cl_image('Image', varargin{1}, 'ColormapIndex', ColormapIndex);
                    end
                    varargout{1} = SonarScope(a, 'ButtonSave', ButtonSave);
                case 3
                    a = cl_image('Image', varargin{3}, 'x', varargin{1}, 'y', varargin{2});
                    varargout{1} = SonarScope(a, 'ButtonSave', ButtonSave);
            end
        end
    end
end
