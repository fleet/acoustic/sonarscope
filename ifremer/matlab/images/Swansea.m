% Recuperation de l'image de l'epave Swansea (image Sonar klein du gesma)
%
% Syntax
%   [I, label] = Swansea
%
% Output Arguments 
%   [] : Auto-plot activation
%   I  : Image du Swansea
%   label : Label de l'image
%
% Examples
%   I = Swansea;
%   figure; imagesc(I); colormap(gray(256)); colorbar;
%
% See also Lena Prismed ImageSonar Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Image, ImageName] = Swansea

nomFic = getNomFicDatabase('Gesma_SwanseadB.ers');
courtesy(nomFic)

[flag, a] = cl_image.import_ermapper(nomFic);
if ~flag
    Image = [];
    ImageName = [];
end

if nargout == 0
    SonarScope(a);
else
    Image = get(a, 'Image');
    ImageName = a.Name;
end
