% Calcul de la table de couleur permettant de reduire une image ombree en une image indexee monocanal
%
% Syntax
%   mapOut = TabCoulOmbrage( mapIn )
% 
% Input Arguments
%   mapIn  : table des couleurs pures
%   mapOut : Table de couleurs modifiees
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   a = cl_car_ima(nomFic);
%   Depth = get(a, 'Depth');
%   ombreRGB = sunShading(Depth, 'az', 180);
%   figure; imshow(ombreRGB); axis xy;
%
%   mapOut = TabCoulOmbrage( jet(64) )
%   ombrePseudoColor = rgb2ind(ombreRGB, mapOut);
%   figure; imshow(ombrePseudoColor, mapOut); axis xy;
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function mapOut = TabCoulOmbrage( mapIn )

mapOut = [mapIn; mapIn; mapIn; mapIn];
NTSC = rgb2ntsc( mapOut );
for i=1:4
    coef = 1. - (i-1)/4;
    sub = 1 + (i-1) * 64 : i*64;
    NTSC(sub,1 ) = NTSC(sub,1 ) .* coef;
end
mapOut = ntsc2rgb( NTSC );
