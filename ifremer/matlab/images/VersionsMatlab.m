% Rend le numero de version de Matlab
%
% Syntax
%   [Version, NumVer] = VersionsMatlab
%
% Output Arguments
%   Version : Version
%   NumVer  : Version
%
% Examples
%   [Version, NumVer] = VersionsMatlab
%
% See also SonarScope Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [Version, NumVer] = VersionsMatlab

Version = ver;

% subGabor = strfind(Version(1).Name, 'Gabor');
% Version(subGabor) = [];
% clear subGabor
% Version = Version(1).Release;
% Version = Version(2:end-1);

for k=1:length(Version)
    if strcmp(Version(k).Name, 'MATLAB')
        NumVer = str2double(Version(k).Version);
        Version = Version(k).Release;
        Version = Version(2:end-1);
        return
    end
end

Version = [];
NumVer  = [];
