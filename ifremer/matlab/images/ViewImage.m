% Visualisateur d'un fichier tif, bmp, etc ...
%
% Syntax
%   ViewImage(...)
%
% Name-Value Pair Arguments
%   NomFic1 : Nom du premier fichier
%   NomFic2 : Nom du deuxieme fichier
%   ...
%
% Remarks : Remplace avantageusement ImageMagic et ErViewer
%
% Examples
%   nomFic = getNomFicDatabase('lena.tif');
%   ViewImage(nomFic)
%
%   nomFic2 = getNomFicDatabase('TexturesSonarMontage01.tif.gz')
%   ViewImage(nomFic, nomFic2)
%
% See also Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%--------------------------------------------------------------------------

function ViewImage(varargin)

if nargin == 0
    [flag, nomFic] = uiSelectFiles('ExtensionFiles', ...
        {'.ers'; '.tif';'.jpg';'.bmp';'.gif';'.png';'.hdf';'.pbm';'.pcx';'.pgm';'.pnm';'.xwd'
        '.imo'; '.mos'; '.mbb'; '.mbg'; '.mnt'; '.dtm'
        '.mat'; '.tomo'; '.txt'; '.xyz'; '.all'; '.s7k'; '.grd'; '.sd'}, ...
        'RepDefaut', pwd);
    if ~flag
        return
    end
else
    nomFic = varargin;
end

for i=1:length(nomFic)
    [pathname, filename, ext] = fileparts(nomFic{i});
    if strcmp(ext, '.ers')
        [flag, a] = cl_image.import_ermapper(nomFic{i});
        if ~flag
            return
        end
        ViewImage(a);
    else
        if strcmp(ext, '.gz')
            if exist(nomFic{i}, 'file')
                disp(['Uncompress : ' nomFic{i} ' Please wait'])
                gunzip(nomFic{i}, pathname)
                delete(nomFic{i})
            end
            nomFic{i} = fullfile(pathname, filename);
        end
        if strcmp(ext, '.zip')
            if exist(nomFic{i}, 'file')
                disp(['Uncompress : ' nomFic{i} ' Please wait'])
                unzip(nomFic{i}, pathname)
                delete(nomFic{i})
            end
            nomFic{i} = fullfile(pathname, filename);
        end
        if exist(nomFic{i}, 'file')
            try
                imfinfo(nomFic{i});
            catch %#ok<CTCH>
                str1 = 'TODO';
                str2 = sprintf('%s file not readable by matlab, try it with SonarScope', nomFic{i});
                [rep, validation] = my_questdlg(Lang(str1,str2));
                if validation && (rep == 1)
                    SonarScope(nomFic{i})
                end
                continue
            end
        else
            continue
        end

        my_imtool(nomFic{i});
    end
end
