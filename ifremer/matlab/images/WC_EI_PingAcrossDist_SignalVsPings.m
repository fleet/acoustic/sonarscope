function flag = WC_EI_PingAcrossDist_SignalVsPings(FileList, DataName, TypeUnit, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag)

a = cl_image.empty;
ImageName = {};
SonarIdent = [];
N = length(FileList);
str1 = 'Traitement des fichiers';
str2 = 'Processing files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, typeFichier, SonarIdent] = identifyFileFormat(FileList{k}, SonarIdent);
    if ~flag
        continue
    end
    switch typeFichier
        case 'XML'
            [flag, b] = cl_image.import_xml(FileList{k}, 'Memmapfile', 0);
            if ~flag
                continue
            end
            flag = testSignature(b, 'GeometryType', 'PingXxxx');
            if ~flag
                continue
            end
            
            if ~isempty(ALim)
                nomFicAngle = strrep(FileList{k}, 'Reflectivity', 'TxAngle');
                [flag, angle] = cl_image.import_xml(nomFicAngle, 'Memmapfile', 0);
                if flag
                    M = ROI_auto(angle, 1, 'Mask', [], [], ALim, 1);
                    b = masquage(b, 'LayerMask', M, 'valMask', 1, 'zone', 1, 'NoWaitbar', 1);
                end
            end
            
            T = get_SonarDatetime(b);
            Time{k}   = T'; %#ok<AGROW>
            Max{k}    = max_lig(b); %#ok<AGROW>
            Min{k}    = min_lig(b); %#ok<AGROW>
            Range{k}  = Max{k} - Min{k}; %#ok<AGROW>
            Median{k} = median_lig(b); %#ok<AGROW>
            Q1{k}     = quantiles_lig(b, 'Threshold', Quantile1); %#ok<AGROW>
            Q2{k}     = quantiles_lig(b, 'Threshold', Quantile2); %#ok<AGROW>
            QuantilesRange{k} = Q2{k} - Q1{k}; %#ok<AGROW>
            
            ImageNameInitial = extract_ImageName(b);
            switch TypeUnit
                case 'dB'
                    b2 = b;
                case 'Amp'
                    b2 = reflec_dB2Amp(b);
                case 'Enr'
                    b2 = reflec_dB2Enr(b);
            end
            b2.Name = ImageNameInitial;
            
            [Mean2, nbs] = mean_lig(b2);
            Std2  = std_lig(b2);
            
            switch TypeUnit
                case 'dB'
                    Mean{k} = Mean2; %#ok<AGROW>
                    Std{k}  = Std2; %#ok<AGROW>
                case 'Amp'
                    Mean{k} = reflec_Amp2dB(Mean2); %#ok<AGROW>
                    Std{k}  = (reflec_Amp2dB(Mean2+Std2) - reflec_Amp2dB(Mean2-Std2)) / 2; %#ok<AGROW>
                case 'Enr'
                    Mean{k} = reflec_Enr2dB(Mean2); %#ok<AGROW>
                    Std{k} = (reflec_Enr2dB(Mean2+Std2) - reflec_Enr2dB(Mean2-Std2)) / 2; %#ok<AGROW>
            end

            if ~isempty(DataName)
                switch DataName
                    case 'Max'
                        V = Max{k};
                        
                    case 'Min'
                        V = Min{k};
                        
                    case 'Range'
                        V = Range{k};
                        
                    case 'Mean'
                         V = Mean{k};
                         
                    case 'Median'
                        V = Median{k};
                        
                    case 'Std'
                        V = Std{k};
                        
                    case 'Quantile1'
                        V = Q1{k};
                        
                    case 'Quantile2'
                        V = Q2{k};
                        
                    case 'QuantilesRange'
                        V = QuantilesRange{k};
                end
                Value{k} = V'; %#ok<AGROW>
            end
            NbSamples{k} = nbs; %#ok<AGROW>
            Latitude{k}   = get(b, 'FishLatitude'); %#ok<AGROW>
            Longitude{k}  = get(b, 'FishLongitude'); %#ok<AGROW>
            
        otherwise
            [~, ~, ext] = fileparts(FileList{k});
            str1 = sprintf('Message pour JMA : Import pas encore d�velopp� dans plotNavigationFichiers pour extension "%s"', ext);
            str2 = sprintf('Message for JMA : Import not yet written in plotNavigationFichiers "%s"', ext);
            my_warndlg(Lang(str1,str2), 1);
            continue
    end
    a(end+1) = b; %#ok<AGROW>
    a(end).InitialFileName = FileList{k};
    ImageName{end+1} = extract_ImageName(a(end)); %#ok<AGROW>
end
my_close(hw)

%% Plot de la navigation

if isempty(DataName)
    plot_position(a);
else
    if isempty(CLim)
        %     CLim = [Inf -Inf];
        %     for k=1:N
        %         CLim(1) = min(CLim(1), min(Value{k}, [], 'omitnan'));
        %         CLim(2) = max(CLim(2), max(Value{k}, [], 'omitnan'));
        %     end
        X = [Value{:}];
        Val = stats(X); %, 'Seuil', [1 95]);
        if Val.NbData == 0
            msg = 'There is no data here, check your filters and try again.';
            my_warndlg(msg, 1);
            return
        end
        CLim = Val.Quant_01_99;
    end
    
    Name = DataName;
    if ~isempty(Quantile1)
        Name = [Name ':' num2strCode([Quantile1 Quantile1]) '%'];
    end
    if ~isempty(ALim)
        Name = [Name ' / Angle limits : ' num2strCode(ALim) ' (deg)'];
    end
    scatter_position(a, Value, 'CLim', CLim, 'Name', Name);
end

%% Edition des signaux

for k=1:length(Value)
    if ~isempty(Value{k})
        Title = ImageName{k}; %sprintf('Signal%d', k);
%             Title = sprintf('Signal %d', k);
    end
    xSample(k) = XSample('name', 'Pings', 'data', Time{k}); %#ok<AGROW>
    ySample(k) = YSample('Name', Title,   'data', Value{k}); %#ok<AGROW> %, 'marker', '.');
end
Title = sprintf('%s signal', DataName);
signal = ClSignal('Name', [Title '-Raw'],  'xSample', xSample,     'ySample', ySample);

str1 = 'Voulez-vous �diter les signaux afin de les despiker, nettoyer, filter ?';
str2 = 'Do you want to edit the signals (in order to despike it, clean it, smooth it, etc ...)';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2); %, 'WaitingTime', 10);
if flag && (rep == 1)
    str1 = 'TODO';
    str2 = 'Warning : Do not forget to close the SignalDialog window once you have finished the data cleaning in order to to continue the processing.';
    my_warndlg(Lang(str1,str2), 1);
    
    s = SignalDialog(signal, 'Title', Title, 'cleanInterpolation', 1);%, 'waitAnswer', false);
    s.openDialog();
    if s.okPressedOut
        for k=1:length(Value)
            X = Value{k};
            Value{k}  = ySample(k).data;
            subOKRaw{k} = (Value{k} == X); %#ok<AGROW>
        end
    else
        subOKRaw  = [];
    end
else
    subOKRaw  = [];
end
signal.plot;

%% Export des signaux

if ~isempty(nomDirCSVOut)
    VarNames = {'NbSamples'; 'Max'; 'Min'; 'Range'; 'Mean'; 'Median'; 'Std'; 'Quantile1'; 'Quantile2'; 'QuantilesRange'; 'Latitude'; 'Longitude'};
    for k=1:length(Value)
        % if ~isempty(Value{k})
        
        if isempty(subOKRaw)
            subOKRaw = 1:length(Time{k});
        end
        
        nomFicSeul = ImageName{k}; %sprintf('Signal%d', k);
        %             nomFicSeul = sprintf('%s-%s', nomFicSeul, str2FileNameCompatible(DataName));
        if isempty(Tag)
            nomFicWithTag = sprintf('%s-WC', nomFicSeul);
        else
            nomFicWithTag = sprintf('%s-%s-WC', nomFicSeul, Tag);
        end
        nomFicCompCsv{k} = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']); %#ok<AGROW>
        OriginFileName = ImageName{k};
        OriginFileName = strrep(OriginFileName, '_Raw_EI_MaskIn', '');
        flag = exportSignalInCsvFile(nomFicCompCsv{k}, Time{k}(subOKRaw{k}), ...
            NbSamples{k}(subOKRaw{k}), Max{k}(subOKRaw{k}), Min{k}(subOKRaw{k}), Range{k}(subOKRaw{k}), ...
            Mean{k}(subOKRaw{k}), Median{k}(subOKRaw{k}), Std{k}(subOKRaw{k}), ...
            Q1{k}(subOKRaw{k}), Q2{k}(subOKRaw{k}), QuantilesRange{k}(subOKRaw{k}), Latitude{k}(subOKRaw{k}), Longitude{k}(subOKRaw{k}), ...
            'VarNames', VarNames, 'OriginFileName', OriginFileName);
        if ~flag
            return
        end
        % end
    end
    
    %% Regroupement de tous les fichiers en un seul
    
    if isempty(Tag)
        nomFicWithTag = sprintf('AllFiles-WC-EI-Raw');
    else
        nomFicWithTag = sprintf('AllFiles-%s-WC-EI-Raw', Tag);
    end
    nomFicOut = fullfile(nomDirCSVOut, [nomFicWithTag 'aaaa.csv']);
    appendFiles(nomFicCompCsv, nomFicOut, 'NbHeaders', 1);
end
