% CONTRIBUTION :
% !! Fonctionne avec la librairie VLFEat V0.9.20 :
% @misc{vedaldi08vlfeat,
%  Author = {A. Vedaldi and B. Fulkerson},
%  Title = {{VLFeat}: An Open and Portable Library
%           of Computer Vision Algorithms},
%  Year  = {2008},
%  Howpublished = {\url{http://www.vlfeat.org/}}
%
% %% INIT pour VL_FEAT ajouter ceci :
% if isempty(which('vl_mser'))
%     run('C:\Users\agaillot\Documents\MATLAB\vlfeat-0.9.20\toolbox\vl_setup.m');
% end
%
% UTILISATION :
% f = WC_extractFeatures(wcImage, ...
%     'x',              xdata,...
%     'y',              ydata,...
%     'EllipseAngle',  15,...
%     'EllipseRatio',  3,...
%     'minClusterDist',20)
%
% AUTHOR : agaillot

function f = WC_extractFeatures(wcImage, varargin)

%% R�cup�ration param�tres

[varargin, x] = getPropertyValue(varargin, 'x', 1:size(wcImage, 2));
[varargin, y] = getPropertyValue(varargin, 'y', 1:size(wcImage, 1));

%% OPTIONS MSER (valid�es STEP5 et fichier JMA)

MSER_options = {...
    'Delta',        15, ...
    'MaxArea',      0.05, ...
    'MinArea',      0.0005, ...
    'MaxVariation', 0.2, ...
    'MinDiversity', 0.2, ...
    'BrightOnDark', 1, ...
    'DarkOnBright', 0};

%% Mise en forme de l'image

wcImage = wcImage - min(wcImage(:));
wcImage = wcImage * (255 / max(wcImage(:)));
Igr = uint8(wcImage);

%% CALCULS

try
    % extraction MSER
    [r, f] = vl_mser(Igr, MSER_options{:});
catch ME
    errordlg(ME.message, ME.identifier, 'modal');
    return
end

% Transposition des �llipses
f = vl_ertr(f);

% Validation des �llipses
[f, ref] = WC_validateFeature(f, x, y, varargin{:});

% Visu
%{
if ~isempty(f)
figure;
image(x, y, Igr); colormap(jet(256)); axis equal; axis xy
vl_plotframe(f)
%WC_plotEllipse(f); pour ne pas avoir les axes des ellipses
end
%}
