% plot des �llipses
%   f   : matrice des �llipses : pour chaque colonne [Xc, Yc, 4 �l�ments de la matrice de covariance]
%   ref : vecteurs des axes de l'ellipses
%
% AUTHOR : agaillot

function h = WC_plotEllipse(f, varargin)

if (nargin > 1) && (size(varargin{1}, 1) == 4)
    plotAxes = true;
    US = varargin{1};
else
    plotAxes = false;
end
    
np  = 40;
thr = linspace(0, 2*pi, np);
Xp  = [cos(thr); sin(thr)]; % cercle unit�
K	= size(f,2);

% allx et ally : tableau de points s�par�es par des NaNs
allx = NaN(1, np*K+(K-1));
ally = NaN(1, np*K+(K-1));

if plotAxes  
  allxf = NaN(1, 6*K);
  allyf = NaN(1, 6*K);
end

for k=1:K
    % Centre de l'�llipse
    xc = f(1,k);
    yc = f(2,k);
    
    % matrice de l'�llipse
    A = reshape(f(3:6,k), 2, 2);
    
    % Points le long de la p�riph�rie
    X = A * Xp;            % on d�forme le cercle unit�
    X(1,:) = X(1,:) + xc;  % on le centre
    X(2,:) = X(2,:) + yc;
    
    % On incr�mente le tableau de points
    allx((k-1)*(np+1) + (1:np)) = X(1,:);
    ally((k-1)*(np+1) + (1:np)) = X(2,:);    
   
    if plotAxes
        % On incr�mente le tableau des axes
        Ref = reshape(US(:,k), 2, 2);
        allxf((k-1)*6 + (1:5)) = xc + [0, Ref(1,1), NaN, 0, Ref(1,2)];
        allyf((k-1)*6 + (1:5)) = yc + [0, Ref(2,1), NaN, 0, Ref(2,2)];       
    end
end

% Trac�
if plotAxes
  h = line([allx NaN allxf], ...
           [ally NaN allyf], ...
           'Color', 'g', 'LineWidth', 2);
else
  h = line(allx, ally, ...
            'Color', 'g', 'LineWidth', 2);
end
