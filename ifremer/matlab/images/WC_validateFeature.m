% Validation des d�tections en deux passes
% 1 : suivant l'orientation et l'�longation de l'ellipse
% 2 : en supprimant les d�tections multiples (niveau proche d'intensit�)
% En sortie :
%   f : matrice des �llipses : pour chaque colonne [Xc, Yc, 4 �l�ments de la matrice de covariance]
%   ref : vecteurs des axes de l'ellipses
%
% AUTHOR : agaillot

function [f, ref] = WC_validateFeature(f, x, y, varargin)

%% r�cup�ration des param�tres de filtrage

[varargin, phi0] = getPropertyValue(varargin, 'EllipseAngle', 15);
[varargin, R0]   = getPropertyValue(varargin, 'EllipseRatio', 3);
[varargin, dx]   = getPropertyValue(varargin, 'minClusterDist', 20); %#ok<ASGLU>

X0    = min(x);
Y0    = min(y);
stepX = mean(diff(x));
stepY = mean(diff(y));


%% mise en forme des ellipses

f       = vl_frame2oell(f);
K       = size(f,2);
isValid = true(1,K);
ref     = ones(4, K);

%% tri par x croissant, puis y croissant

f = sortrows(f', [1 2])';

%% normalisation par la dimension de l'image et MaJ tableau ellipse
% centre de l'ellipse

dum = [f(1:2, :)', ones(K,1)] * [stepX, 0 , 0; 0 stepY, 0; X0, Y0, 0];
f(1:2, :) = dum(:,1:2)';

%% 1 filtrage sur inclinaison et ratio
for k=1:K
    % ellipse matrix
    A = reshape( f(3:6, k), 2, 2);
    
    % normalisation par la dimension de l'image et MaJ tableau ellipse
    A = A * diag([stepX, stepY]);
    f(3:6, k) = reshape( A, 4, 1);
    
    % D�composition pour calcul de l'inclinaison et du ratio
    [U, S] = svd(A);
    phi = atand(U(1,1) / U(2,1));
    R = S(1,1) / S(2,2);
    
    % On conserve les vecteurs
    ref(:, k) = reshape(U*S, 4, 1);
    
    if (abs(phi) >= phi0) || (R <= R0)
        isValid(k) = false;
    end
end

f = f(:,isValid);
ref = ref(:, isValid);

%% 2 suppression des d�tections multiples d'un m�me panache = centre ellipsoide proche

K = size(f,2);
isValid = true(1,K);

isSame = [true, diff(f(1,:)) < dx]; % on groupe par seuillage en x, dx en pixel
nb     = sum(~isSame)+1;            % Nbre de cluster
iDeb   = [1 find(~isSame)];         % indice d�but cluster
iFin   = [find(~isSame)-1 K];       % indice fin de cluster

for iC=1:nb
    if iDeb(iC) == iFin(iC)
        continue
    end
    fc   = f(:, iDeb(iC):iFin(iC));
    refc = ref(:, iDeb(iC):iFin(iC));
    
    diffCtr = abs( diff( fc(2,:)) );
    sumGdAx = sum( abs([refc(2, 2:end); refc(2, 1:end-1)]));
    
    isValid(iDeb(iC):iFin(iC)) = [true, diffCtr > (0.8*sumGdAx)];
end

f = f(:,isValid);
ref = ref(:, isValid);
