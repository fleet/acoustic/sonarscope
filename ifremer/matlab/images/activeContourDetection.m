% TODO : en test actuellement

function activeContourDetection(GCA, Coordonnees)

if ~isdeployed
    H = getrect(GCA); %  [xmin ymin width height]. 
    WC = getappdata(GCA, 'WC');
    I = get(WC, 'Image');
    K = false(size(I));
    x = WC.x;
    y = WC.y;
    subx = ((x >= H(1)) & (x <= (H(1)+H(3))));
    suby = ((x >= H(2)) & (x <= (H(2)+H(4))));
    K(suby,subx) = true;
    I(isnan(I)) = -120;
    I = flipud(I);
    
    I = quantify(I, 'rangeIn', WC.CLim);
    bw = activecontour(I, K, 300, 'edge');
    figure(5746); imagesc(x, y, I); colormap(jet(256));
    hold on; contour(x, y, bw, 'r'); hold off;
end
