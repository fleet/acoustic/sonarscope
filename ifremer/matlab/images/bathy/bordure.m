% Coordonnees des bords d'une image.
% Toute frontiere avec des NAN est consideree comme une frontiere.
% Cette fonction est assez rustique. On peur surement mieux faire
%
% SYNTAX
%   [limx, limy] = bordure(I)
%
% Input Arguments
%   I : Image
%
% Output Arguments
%   limx, limy : coordonnees (lignes, colonnes dans l'image
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   a = cl_car_ima(nomFic);
%   I = get(a, 'Depth');
%   [limx, limy] = bordure(Prismed);
%   imagesc(I); hold on; plot(limx, limy, '+');
%
% See also movie movieOmbrage movie2avi Authors
% Authors : JMA
% -------------------------------------------------------------------------------

function [limx, limy] = bordure(I)

ligne = NaN([1,size(I,2)]);
indLim = 1;
for k1=1:size(I,1)
    ligne(:) = 0;
    ligne(~isnan(I(k1,:))) = 1;
    difLigne = diff(ligne);
    sub01 = find(difLigne == 1);
    sub10 = find(difLigne == -1);
    for k2=1:length(sub01)
        limy(indLim) = k1; %#ok<AGROW>
        limx(indLim) = sub01(k2); %#ok<AGROW>
        indLim = indLim + 1;
    end
    for k2=1:length(sub10)
        limy(indLim) = k1;
        limx(indLim) = sub10(k2);
        indLim = indLim + 1;
    end
end
