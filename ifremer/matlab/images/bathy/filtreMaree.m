% Filtrage de la maree
%
% Syntax
%   mf = filtreMaree(t, m);
%
% Input Arguments
%   t : Temps
%   m : Maree
%
% Output Arguments
%   mf : Maree filtree
%
% Examples
%   nomFic = '/home4/doppler/MarcRoche/NewTides/2001-18.tide';
%   [t, m] = lecFicMareeSimrad(nomFic);
%   mf = filtreMaree(t, m);
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function varargout = filtreMaree(t, m)

N = length(m);
d = diff(t);
d_mean = mean(d);
subF = find(abs(d) > (2*d_mean));
subF = [1 subF' N];

if length(subF) ~= 2
    %         figure; coul = 'brgk';
    iF = 0;
    for k=1:(length(subF)-1)
        iD = iF + 1;
        iF = subF(k+1);
        sub = iD:iF;
        %             icoul = 1 + mod(k-1, length(coul));
        %             plot(sub, m(sub), coul(icoul)); grid on; hold on;
        if length(sub) > 6
            pppp = filtreMaree(t(sub), m(sub));
            mf(sub) = pppp; %#ok<AGROW>
        else
            mf(sub) = m(sub); %#ok<AGROW>
        end
    end
    if nargout == 0
        figure; plot(t, m);
        hold on; plot(t, mf, 'r'); grid on;
        figure; plot(m);
        hold on; plot(mf, 'r'); grid on;
    else
        varargout{1} = mf;
    end
    return
end

OrdreFiltre = 2;
if length(m) <= (3*OrdreFiltre)
    varargout{1} = m;
    return
end

Tr = 36000;    % Temps de reponse en secondes
difMoy = d_mean / 1000;  % Difference moyenne entre points de navigation en secondes
TrSample = Tr / difMoy;
wc = (2*pi) / TrSample;
[B,A] = butter(OrdreFiltre, wc);
mf = filtfilt(B, A, double(m));

NTransition = floor(TrSample/5);

sub = 1:NTransition;
subsub = find((sub >= 1) & (sub <= N));
sub = sub(subsub);
alpha = linspace(0, 1, NTransition);
mf(sub) = (1-alpha(subsub)) .* m(sub) + alpha(subsub) .* mf(sub);

sub = (N+1-NTransition):N;
subsub = find((sub >= 1) & (sub <= N));
sub = sub(subsub);
alpha = linspace(0, 1, NTransition);
mf(sub) = alpha(subsub) .* m(sub) + (1-alpha(subsub)) .* mf(sub);

if nargout == 0
    figure; plot(t, m);
    hold on; plot(t, mf, 'r'); grid on;
    figure; plot(m);
    hold on; plot(mf, 'r'); grid on;
else
    varargout{1} = mf;
end
