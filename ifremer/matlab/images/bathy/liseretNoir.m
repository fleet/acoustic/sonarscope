% Suppression du fond noir d'une image
%
% Syntax
%   rgbOut = liseretNoir( RGB )
%   rgbOut = liseretNoir( RGB, nbPixNoirsLaisses )
% 
% Input Arguments
%   RGB  : image 
%
% Optional Input Arguments
%   nbPixNoirsLaisses : Largeur du liseret (10 par defaut)
%
% Output Arguments
%   rgbOut : image avec le liseret noir
%
% Examples
%   RGB = sunShading(Prismed, 'az', 10);
%   figure; imshow(RGB)
%
%   RGBLiseret = liseretNoir(RGB, 20);
%   figure; imshow(RGBLiseret);
%	
% See also ombrage Authors
% Authors : JMA
%--------------------------------------------------------------------------------
 
function rgbOut = liseretNoir( RGB, varargin )

if nargin == 1
    nbNoirs = 10;
else
    nbNoirs = varargin{1};
end

% noir = 0;
if isa(RGB, 'uint8')
    blanc = 255;
else
    blanc = 1;
end

nbSlides = size(RGB,3);
if nbSlides == 1
    Present = ~isnan(RGB);
else
    Present = sum(RGB, nbSlides) ;
    Present(Present ~= 0) = 1;
end

if nbNoirs == 0
    sub = (Present == 0);
    rgbOut = RGB;
    rgbOut(sub) = blanc;
    return
end

[X,Y] = meshgrid(-nbNoirs:nbNoirs, -nbNoirs:nbNoirs);
cercle = sqrt((X.^2 + Y.^2));
cercle(cercle <= nbNoirs) = 0;
cercle(cercle > nbNoirs) = 1;

n = imdilate(Present, ~cercle);
I = (n  == 0);

rgbOut = RGB;
for k=1:nbSlides
    plan = rgbOut(:, :, k);
    plan(I) = blanc;
    rgbOut(:, :, k) = plan;
end
