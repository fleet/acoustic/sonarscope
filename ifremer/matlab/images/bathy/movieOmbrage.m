% Calcul d'une animation avec representation ombree
%
% SYNTAX
%   Film = movieOmbrage( image , 'az', az, 'xpentes', xpentes, 'map', map, 'resol', resol )
%
% Input Arguments
%   image   : image devant etre ombree
%
% Name-Value Pair Arguments
%   az      : Direction du soleil (deg)
%   xpentes : Exagration des pentes
%   map     : Table de couleurs (jet par defaut)
%   resol   : Taille du pixel en metres
%   CLim    : Limites des couleurs (Ex : [-60 -20] pour limites la palette de couleur de -60m a -20m)
%
% Examples
%   Depth = Prismed;
%   imagesc(Depth); axis xy; axis equal; axis tight;
%   Film = movieOmbrage(Depth, 'az', 0:2:259, 'xpentes', 8);
%   movie2avi( Film, '/tmp/prismed.avi' )
%
% See also ombrage movie2avi Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function Film = movieOmbrage( image, varargin )

figure;
[varargin, az]      = getPropertyValue(varargin, 'az',      0:30:259);
[varargin, xpentes] = getPropertyValue(varargin, 'xpentes', 8);
[varargin, map]     = getPropertyValue(varargin, 'map',     jet(256));
close


nA = length(az);
nE = length(xpentes);
N = max(nA,nE);
grid on
iA = 1;
iE = 1;

sunShading(image, az(iA), xpentes(iE), map);
axis xy; truesize;

% disp('Redimensionnez la fenetre eventuellement et return pour continuer');
% pause;

mapOut = TabCoulOmbrage(map);

Film = moviein(N);

i = 1;
hw = create_waitbar('Calcul du Film', 'N', N);
for k=1:N
    my_waitbar(i, N, hw);
    iA = mod(k-1, nA) + 1;
    iE = mod(k-1, nE) + 1;
    
    fprintf('Calcul image %d/%d\n', i, N);
    ombre = sunShading(image, 'az', az(iA), 'xpentes', xpentes(iE), 'map', map, varargin{:});
    
    % On reduit volontairement l'image de double en uint8 pour gagner de la place en memoire
    ombre = rgb2ind(ombre, mapOut);
    imshow(ombre, mapOut); axis xy;
    
    title(sprintf('Azimut : %4.1f degres - Exag. pentes : %4.1f', az(iA), xpentes(iE)));
    axis equal; axis tight;
    Film(:,i) = getframe;
    i = i + 1;
end
my_close(hw);
