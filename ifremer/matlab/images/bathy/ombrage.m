% Calcul d'une batymetrie ombree
%
% Syntax
%   ZOmbree = ombrage(Z, ...)
%
% Input Arguments
%   Z : Modele numerique de teerrain
%
% Name-Value Pair Arguments
%   az      : Azimuth (deg) (0 par defaut)
%   xpentes : Exageration des pentes (8 par defaut)
%   elev    : Elevation du soleil (deg) (30 deg par defaut)
%   map     : Table de couleurs, jet, ColorSea, etc... (jet par defaut)
%   resol   : Resolution de l'image (m)
%   CLim    : Limites des couleurs (Ex : [-60 -20] pour limiter la palette de couleur de -60m a -20m)
%
% Output Arguments
%   []      : Auto-plot activation
%   ZOmbree : image ombree
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%
%   a = import_CaraibesGrid(cl_image, nomFic)
%   Depth = get(a, 'Image');
%   ombrage(Depth);
%
%   [X, map] = rgb2ind(RGB, 256);
%   figure; imshow(X,map); title('Image en pseudo-couleur'); colorbar; axis xy;
%
% See also rotn90 movieOmbrage ombrageNB imageeq polyfitSurf ColorSea liseretNoir cl_image Authors
% Authors : XL + JMA
%--------------------------------------------------------------------------------

function ZOmbree = ombrage(Z, varargin)

[varargin, az]        = getPropertyValue(varargin, 'az', 0);
[varargin, xpentes]   = getPropertyValue(varargin, 'xpentes', 8);
[varargin, map]       = getPropertyValue(varargin, 'map', jet(64));
[varargin, CLim]      = getPropertyValue(varargin, 'CLim', []);
[varargin, resol]     = getPropertyValue(varargin, 'resol', []);
[varargin, elevation] = getPropertyValue(varargin, 'elevation', 30); %#ok<ASGLU>

%% Nombre de couleurs

NCoul = size(map, 1);

%% Limites de la table de couleurs

if isempty(CLim)
    CLim = [min(Z(:)) max(Z(:))];
end

ombre = ombrageNB(Z, 'az', az, 'xpentes', xpentes, 'resol', resol, 'elevation', elevation);

nl = size(Z, 1);
nc = size(Z, 2);

ZOmbree = zeros(nl, nc, 3, 'uint8');
pasl = min(floor((256*256 / nl)), nl);
for il=1:pasl:nl
    subl = il:min(il+pasl-1, nl);
    RGB = ind2rgb( gray2ind(mat2gray(double(Z(subl,:)), double(CLim)), NCoul), map);
    NTSC = rgb2ntsc( RGB );
    NTSC(:,:,1 ) = NTSC(:,:,1 ) .* (ombre(subl,:) + 1)/2;
    ZOmbree(subl,:,:) = round(255 * ntsc2rgb( NTSC ));
end

%% Par ici la sortie

if nargout == 0
    figure
    imshow(ZOmbree); axis on; axis xy;
    title(sprintf('Azimut=%f Exageration=%f', az, xpentes));
end

%{
%% Chargement du fichier

load Z
% Z = Depth;

%% Affichage de l'image

figure; imagesc(Z); axis xy; colorbar

%% Calcul de l'ombrage

sz = size(Z);
W = zeros(size(Z));

for iCol=1:(sz(2)-1)
for iLig=1:sz(1)
w  = (2*(Z(iLig,iCol)-Z(iLig,iCol+1)))-3*1;
w2 = w ;%./ (sqrt(w.^2+1)*sqrt(4+9));
%         W2 = [W2,w2];

W(iLig,iCol) = w2;
end
end

%% Affichage de l'ombrage

figure; imagesc(W, [-250 250]); axis xy; colormap(gray(256)); colorbar
% SonarScope

%% Transformation de l'image de bathym�trie en image 3 composantes : Rouge, vert, bleu

CLim = [-4000 -1000];
RGB = ind2rgb(gray2ind(mat2gray(Z, CLim), 128), jet(128));

%% M�lange de l'image couleur avec l'ombrage

Alpha = 1.5;
for i=1:3
p1 = RGB(:,:,i);
p2 = ((W+250) / 500);
Ombrage(:,:,i) = Alpha* (p1 .* p2);
end

%% Affichage de l'image couleur ombr�e

figure; imshow(Ombrage); axis xy

%}
