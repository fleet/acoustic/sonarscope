% Calcul d'une image ombree en noir et blanc
%
% Syntax
%   imageOut = ombrageNB(imageIn, 'az', az, 'xpentes', xpentes, 'resol', resol)
%
% Input Arguments
%   imageIn : Image devant etre ombree
%
% Name-Value Pair Arguments
%   az      : Azimuth (deg) (0 par defaut)
%   xpentes : Exageration des pentes (deg) (8 par defaut)
%   elev    : elevation du soleil [0 90] (30 deg par defaut)
%   resol   : Taille du pixel (m)
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_125m.mnt');
%   nomFic = getNomFicDatabase('EM12D_PRISMED_300m.mnt');
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   a = cl_car_ima(nomFic);
%   Depth = get(a, 'Depth');
%   resolution = get(a, 'Element_x_size') / 1000
%
%   ombrageNB(Depth);
%   imageOmbree = ombrageNB(Depth, 'az', 20, 'xpentes', 8, 'resol', resolution);
%   figure; imagesc(imageOmbree); colormap(gray(256)); zoom on; truesize;
%
% See also FilmombrageNB OmbrageCoul Authors
% Authors : XL + JMA
%--------------------------------------------------------------------------------

function varargout = ombrageNB(imageIn, varargin)

[varargin, az]        = getPropertyValue(varargin, 'az', 0);
[varargin, xpentes]   = getPropertyValue(varargin, 'xpentes', 8);
[varargin, resol]     = getPropertyValue(varargin, 'resol', []);
[varargin, elevation] = getPropertyValue(varargin, 'elevation', 30); %#ok<ASGLU>

az    = mod(-az + 360 + 180, 360) * pi / 180;
alpha = -sin(az);
beta  = cos(az);

% h = -fspecial('sobel');
h = [-1    -2    -1
    0     0     0
    1     2     1];
masque = alpha * h' + beta * h;
masque = xpentes * masque / sum(abs(h(:)));
imgradient = filter2(masque, imageIn);

if isempty(resol)
    m = mean(imgradient(:), 'omitnan');
    s = std( imgradient(:), 'omitnan');
    imgradient = (imgradient - m) / s;
else
    imgradient = imgradient / resol;
end

angle = ((90-elevation) * (pi / 180)) - imgradient;
angle = angle .* angle;
angle(angle < -pi) = -pi;
angle(angle >  pi) =  pi;
imageOmbree = cos( angle );

switch nargout
    case 0
        figure
        imagesc(imageOmbree); axis xy;
        colormap(gray(256)); zoom on; truesize;
    case 1
        varargout{1} = imageOmbree;
end
