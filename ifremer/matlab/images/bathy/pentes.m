% Calcul d'une image representant les angles en degres.
%
% Syntax
%   imageOut = pentes( imageIn, resol, 'az', az)
%
% Input Arguments
%   imageIn : Image devant etre ombree
%   resol   : Taille du pixel en metres
%
% PROPERTY VALUES :
%   az      : Azimuth (deg) (0 par defaut)
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_125m.mnt');
%   nomFic = getNomFicDatabase('EM12D_PRISMED_300m.mnt');
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   a = cl_car_ima(nomFic);
%   Depth = get(a, 'Depth'); resol = get(a, 'Element_x_size')*1e-3;
%   imagesc(Depth); axis xy; axis equal; axis tight;
%
%   imgradient = pentes(Depth, resol, 'az', 20);
%   figure;
%   imagesc(imgradient, [-30 30]);
%   colormap(jet(256)); axis xy; zoom on; truesize; colorbar;
%
% See also OmbrageNB Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function varargout = pentes( imageIn, resol, varargin )

[varargin, az] = getPropertyValue(varargin, 'az', 0); %#ok<ASGLU>

az = mod(-az + 360, 360) * pi / 180;
alpha = -sin(az);
beta  = cos(az);

h = -fspecial('sobel');
masque = alpha * h' + beta * h;
imgradient = filter2(masque, imageIn) / sum(abs(h(:)));

imgradient = imgradient / resol;
imgradient = (180 / pi) * atan(imgradient);

switch nargout
    case 0
        imagesc(imgradient);
        colormap(gray(256)); zoom on; truesize;
    case 1
        varargout{1} = imgradient;
end

