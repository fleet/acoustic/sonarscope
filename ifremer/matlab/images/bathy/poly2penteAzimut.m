% Modelisation d'une surface par une forme polynomiale
%
% Syntax
%   [Azimuth, pente, penteDeg] = poly2penteAzimut(C)
%
% Input Arguments
%   C       : Coefficients de la forme polynomiale :
%               C11 + C12 * x + C21 * y * x + 0
%
% Output Arguments
%   Azimuth   : Azimut de la pente (sens de la montee)
%   pente    : Pente du terrain
%   penteDeg : Pente du terrain en degre (sous reserve que C ait ete
%                                      calcule aavec x, y et z en m)
%
% Remarks : Cette fonction ne foctionne que si C est de taille 1x2 ou 2x1 ou 2x2 avec C22=0
%
% Examples
%   [Z, x, y] = Prismed;
%   subx = 190:290; suby = 240:400;
%   C = polyfitSurf(x(subx), y(suby), Z(suby, subx), 'Plan')
%   [Azimuth, pente, penteDeg] = poly2penteAzimut(C)
%
%   C = polyfitSurf(x(subx), y(suby), Z(suby, subx), 'pY', 0)
%   [Azimuth, pente, penteDeg] = poly2penteAzimut(C)
%
%   C = polyfitSurf(x(subx), y(suby), Z(suby, subx), 'pX', 0)
%   [Azimuth, pente, penteDeg] = poly2penteAzimut(C)
%	
% See also polyfitSurf polyvalSurf penteAzimut2poly Authors
% Authors : YHDR + JMA
%--------------------------------------------------------------------------------

function [Azimuth, pente, penteDeg] = poly2penteAzimut(C)

Azimuth = [];
pente   = [];

if length(C) > 2
    my_warndlg('bathy/poly2penteAzimut : Cette fonction ne foctionne que si C est de taille 1x2 ou 2x1 ou 2x2 avec C22=0', 1);
    return
end

pY = size(C,1)-1;
pX = size(C,2)-1;

if (pX == 1) && (pY == 1) && (C(4) == 0)
    NX = norm([C(3), C(2), 1], 2);
    pente = acos(1 / NX);
    pente = tan(pente);
    Azimuth = atan(C(2) / C(3)) + pi * (C(3)>0);
    Azimuth = mod(360 + (Azimuth * 180/pi), 360);
elseif (pX == 1) && (pY == 1) && (C(4) ~= 0)
    my_warndlg('bathy/poly2penteAzimut :C22 est different de 0', 1);
    return
elseif (pX == 1) && (pY == 0)
    pente = C(2);
    if pente >= 0
        Azimuth = 90;
    else
        Azimuth = 270;
    end
    pente = abs(pente);
elseif (pX == 0) && (pY == 1)
    pente = C(2);
    if pente >= 0
        Azimuth = 0;
    else
        Azimuth = 180;
    end
    pente = abs(pente);
else
    my_warndlg('bathy/poly2penteAzimut : beurk', 1);
    return
end
penteDeg = atan(pente) * 180/pi;


% % -----------------------------------
% 
% function LOut = fL(x, y, pX, pY, Plan)
% 
% nbPoints = length(x);
% LOut = zeros(nbPoints, (pX+1)*(pY+1));
% for k=1:nbPoints
%     for i=0:pX
%         X(i+1) = x(k) ^ i;
%     end
%     for i=0:pY
%         Y(i+1) = y(k) ^ i;
%     end
%     L = Y' * X;
%     LOut(k,:) = L(:)';
% end
% 
% if Plan
%     LOut(:,end) = 0;
% end
