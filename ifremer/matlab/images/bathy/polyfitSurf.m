% Modelisation d'une surface par une forme polynomiale
%
% Syntax
%   [flag, C, A, Eqm, ZFit, ZDif] = polyfitSurf(Z, ...)
%   [flag, C, A, Eqm, ZFit, ZDif] = polyfitSurf(x, y, Z, ...)
%
% Input Arguments
%   x : Abscisses (1:size(Z,2) sinon)
%   y : Ordonnees (1:size(Z,1) sinon)
%   Z : Surface a modeliser
%
% Name-Value Pair Arguments
%   flag   : 1=OK, 0=KO, -1=Matrice trop grosse
%   pX     : Degre du polynome en x (1 par defaut)
%   pY     : Degre du polynome en y (1 par defaut)
%   Masque : Masque des coefficient, . Permet d'eviter le calcul de coefficients
%            que l'on veut rendre obligatoirement egaux a 0(ones(pY+1,pX+1) par defaut)
%   FctIn  : Fonction a appliquer a Z avant ajustement
%   FctOut : Fonction a appliquer au modele pour se ramener a l'unite de Z
%
% Name-only Arguments
%   Plan : Modelisation d'un plan. (Estime C11 C12 et C21, C22=0)
%
% Output Arguments
%   []      : Auto-plot activation
%   C       : Coefficients de la forme polynomiale :
%               C11 * y^0 * x^0 + C12 * y^0 * x^1 + ... + C1pX * y^0 * y^pX +
%               C21 * y^1 * x^0 + C22 * y^1 * x^1 + ... + C2pX * y^1 * y^pX +
%               ... +
%               CpY1*y^pY * x^0 +CpY2 *y^pY*x^1 + ... + CpYpX*y^pY*y^pX
%   A       : Matrice interm�diaire qui sert uniquement � optimiser le
%             temps calcul lorsque cette fonction est utilis�e par blkproc
%   Eqm     : Ecart-type de la difference
%   ZFit    : Modele genere a partir des coordonnees de Z
%   ZDif    : Difference entre donnees et lodele : Z - modele
%
% Remarks : Si Z est une image les x et y (s'ils sont donnes) doivent etre
%           des vecteurs de longueur size(Z,2) et size(Z,1)
%           Si Z est un vecteur, alors x et y doivent etre donnes et etre
%           de meme longueur : length(Z)
%
% Examples
%   [flag, C, A, Eqm, ZFit, ZDif] = polyfitSurf([0 1; 0 1])
%
%   x = -5:0.1:5;
%   y = -15:0.1:15;
%   zx = x .^ 2;
%   zy = y .^ 2;
%   Z = zy' * zx;
%   [flag, C, A, Eqm] = polyfitSurf(x, y, Z, 'pX', 2, 'pY', 2)
%   [flag, C, A, Eqm] = polyfitSurf(x, y, Z, 'pX', 2, 'pY', 2, 'Mask', [0 0 0 ; 0 0 0 ; 0 0 1])
%
%   x = -5:0.1:5;
%   y = -15:0.1:15;
%   zx = exp(-(x.^2));
%   zy = exp(-(y.^2));
%   Z = zy' * zx;
%   [flag, C, A, Eqm] = polyfitSurf(x, y, log(Z), 'pX', 2, 'pY', 2)
%   [flag, C, A, Eqm] = polyfitSurf(x, y, Z, 'pX', 2, 'pY', 2, 'FctIn', @log, 'FctOut', @exp)
%   [flag, C, A, Eqm] = polyfitSurf(x, y, Z, 'pX', 2, 'pY', 2, 'FctIn', @log, 'FctOut', @exp, 'Mask', [0 0 1 ; 0 0 0 ; 1 0 0])
%
%   [Z, x, y] = Prismed;
%   subx = 150:350; suby = 200:450;
%   subx = 190:290; suby = 240:400;
%   polyfitSurf(Z(suby, subx))
%   polyfitSurf(Z(suby, subx), 'pY', 0)
%   polyfitSurf(Z(suby, subx), 'pX', 0)
%   polyfitSurf(Z(suby, subx), 'pX', 2, 'pY', 2)
%   polyfitSurf(Z(suby, subx), 'Plan')
%   [flag, C, A, Eqm] = polyfitSurf(x(subx), y(suby), Z(suby, subx), 'Plan');
%   [Azimuth, pente, penteDeg] = poly2penteAzimut(C)
%
%   % Generation d'un plan incline sur une plus grande surface
%   subx = 150:350; suby = 200:450;
%   P = polyvalSurf(C, x(subx), y(suby), '2D');
%   visuImages(Z(suby, subx), Z(suby, subx) - P)
%
% See also polyvalSurf poly2penteAzimut Authors
% Authors : YHDR + JMA
%--------------------------------------------------------------------------------

% TODO : faire le clair avec flag=-1, 0 ....

function [flag, C, AIn, Eqm, ZFit, ZDif] = polyfitSurf(varargin)

[varargin, AIn] = getPropertyValue(varargin, 'A', []);

flag = 1;
C    = [];
Eqm  = [];
ZFit = [];
ZDif = [];

[varargin, pX]     = getPropertyValue(varargin, 'pX',     1);
[varargin, pY]     = getPropertyValue(varargin, 'pY',     1);
[varargin, FctIn]  = getPropertyValue(varargin, 'FctIn' , []);
[varargin, FctOut] = getPropertyValue(varargin, 'FctOut', []);
[varargin, Masque] = getPropertyValue(varargin, 'Mask',   []);

[varargin, Plan] = getFlag(varargin, 'Plan');

if Plan
    pX = 1;
    pY = 1;
    Masque = [1 1; 1 0];
end

if isempty(Masque)
    Masque = ones(pY+1, pX+1);
end

switch length(varargin)
    case 1
        Z = varargin{1};
        if (size(Z, 1) == 1) || (size(Z, 2) == 1)
            my_warndlg('polyfitSurf : Z est un vecteur, vous devez passer obligatoirement x et y', 1);
            return
        else
            x = 1:size(Z,2);
            y = 1:size(Z,1);
        end
    case 3
        x = varargin{1};
        y = varargin{2};
        Z = varargin{3};
        if (size(Z, 1) == 1) || (size(Z, 2) == 1)
            if (length(Z) ~= length(x)) || (length(Z) ~= length(y))
                my_warndlg('polyfitSurf : Z est un vecteur, x et y doivent avoir la m�me longueur', 1);
                return
            end
        else
            if isvector(x)
                if size(Z, 1) ~= length(y)
                    my_warndlg('polyfitSurf : y n''a pas la m�me hauteur que Z', 1);
                    return
                end
                if size(Z, 2) ~= length(x)
                    my_warndlg('polyfitSurf : x n''a pas la m�me largeur que Z', 1);
                    return
                end
            else
                if ~isequal(size(x), size(Z))
                    my_warndlg('polyfitSurf : x n''a pas la m�me taille que Z', 1);
                    return
                end
            end
        end
    otherwise
        my_warndlg('polyfitSurf : 1 ou 3 param�tres d''entr�e svp', 1);
        return
end

nx = length(x);
ny = length(y);
coefReduc = sqrt((nx*ny) / 1e6);
if coefReduc > 1
    flag = -1;
    return
end

if isvector(x)
    [xg, yg] = meshgrid(x, y);
    nx = length(x);
    ny = length(y);
else
    xg = x;
    yg = y;
    nx = size(x,2);
    ny = size(y,1);
end

sub = find(~isnan(Z));
xg = xg(sub);
yg = yg(sub);

if isempty(AIn) || (length(sub) ~= numel(Z))
    AIn = fL(xg, yg, pX, pY);
end

subCoef0 = find(Masque);
A = AIn(:,subCoef0);

if isempty(FctIn)
    b = double(Z(sub));
else
    b = feval(FctIn, Z(sub));
end
if isa(b, 'uint8')
    b = double(b);
end

lastwarn('')
pasSample = 1;
flag = 1;
while flag
    subSample = 1:pasSample:length(b);
    
    if length(subSample) < length(subCoef0)
        flag = -1;
        return
    end
    
    % Premi�re m�thode
    %     lastwarn('')
    %     tic
    %     C = A(subSample,:) \ b(subSample);
    %     toc
    %     [warnmsg, msgid] = lastwarn;
    
    % Deuxi�me m�thode
    %     lastwarn('')
    %     tic
    %     C = inv(A'*A) * A' *b(subSample);
    %     toc
    %     [warnmsg, msgid] = lastwarn;
    
    % Troisi�me m�thode
    lastwarn('')
    %     tic
    C = pinv(A(subSample,:)) * b(subSample);
    %     toc
    [~, msgid] = lastwarn;
    
    if strcmp(msgid, 'MATLAB:rankDeficientMatrix') || strcmp(msgid, 'MATLAB:singularMatrix')
        pasSample = pasSample * 2;
        lastwarn('')
    else
        flag = 0;
    end
end

% ZFit = NaN(length(y), length(x), class(Z(1)));
ZFit = NaN(ny, nx, class(Z(1)));
ZFit(sub) = A * C;

if ~isempty(FctOut)
    ZFit = feval(FctOut, ZFit);
end

% V = fL(xg(1:10:end), yg(1:10:end), pX, pY, Plan) * C

COut = zeros(pY+1, pX+1);
COut(subCoef0) = C;
C = reshape(COut, pY+1, pX+1);

switch nargout
    case {1;2;3}
    case 0
        Eqm  = std(Z(sub) - ZFit(sub));
        ZDif =  Z - ZFit;
        str = sprintf('pX=%d  pY=%d', pX, pY);
        figure;
        subplot(2,3,1); imagesc(x, y, Z); colorbar; title('Z');
        axis equal; axis xy; axis tight
        I = ombrage(Z);
        subplot(2,3,4); imshow(x, y, I); axis xy;title('Z');
        
        subplot(2,3,2); imagesc(x, y, ZFit); colormap(jet(256)); colorbar;
        if Plan
            title('Plan incline');
        else
            title([str ' : Modele']);
        end
        axis equal; axis xy; axis tight
        
        subplot(2,3,3); imagesc(x, y, Z-ZFit); colorbar; title([str ' : Diff']);
        axis equal; axis xy; axis tight
        I = ombrage(Z-ZFit);
        subplot(2,3,6); imshow(x, y, I); axis xy;title([str ' : Diff'])
    case 4
        Eqm  = std(Z(sub) - ZFit(sub));
    case 5
        Eqm  = std(Z(sub) - ZFit(sub));
        ZDif = Z(sub) - ZFit(sub);
end


function LOut = fL(x, y, pX, pY)

nbPoints = length(x);
LOut = zeros(nbPoints, (pX+1)*(pY+1));
X = ones(1,pX+1);
Y = ones(1,pY+1);
for k=1:nbPoints
    for i=1:pX
        X(i+1) = x(k) ^ i;
    end
    for i=1:pY
        Y(i+1) = y(k) ^ i;
    end
    L = Y' * X;
    LOut(k,:) = L(:)';
end
