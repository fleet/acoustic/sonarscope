% Mod�lisation d'une surface par une forme polynomiale
%
% Syntax
%   [flag, C, A, Eqm, ZFit, ZDif] = polyfitSurf(Z, ...)
%   [flag, C, A, Eqm, ZFit, ZDif] = polyfitSurf(x, y, Z, ...)
%
% Input Arguments
%   x : Abscisses
%   y : Ordonnees
%   Z : Surface a modeliser
%   pX     : Degr� du polynome en x (1 par defaut)
%   pY     : Degr� du polynome en y (1 par defaut)
%   A       : Matrice interm�diaire qui sert uniquement � optimiser le
%             temps calcul lorsque cette fonction est utilis�e par blkproc
%
% Output Arguments
%   []      : Auto-plot activation
%   C       : Coefficients de la forme polynomiale :
%               C11 * y^0 * x^0 + C12 * y^0 * x^1 + ... + C1pX * y^0 * y^pX +
%               C21 * y^1 * x^0 + C22 * y^1 * x^1 + ... + C2pX * y^1 * y^pX +
%               ... +
%               CpY1*y^pY * x^0 +CpY2 *y^pY*x^1 + ... + CpYpX*y^pY*y^pX
%   A       : Matrice interm�diaire qui sert uniquement � optimiser le
%             temps calcul lorsque cette fonction est utilis�e par blkproc
%   Eqm     : Ecart-type de la difference
%   ZFit    : Mod�le g�n�r� � partir des coordonn�es de Z
%   ZDif    : Diff�rence entre donn�es et le mod�le : Z - modele
%
% Examples
%
% See also polyvalSurf poly2penteAzimut Authors
% Authors : YHDR + JMA
%--------------------------------------------------------------------------------

function [flag, C, AIn, Eqm, ZFit, ZDif] = polyfitSurf_direct(x, y, Z, pX, pY, AIn)

C    = [];
Eqm  = [];
ZFit = [];
ZDif = [];

N2 = (pY+1) * (pX+1);

nx = length(x);
ny = length(y);
coefReduc = sqrt((nx*ny) / 1e6);
if coefReduc > 1
    flag = -1;
    return
end

if isvector(x)
    [xg, yg] = meshgrid(x, y);
    nx = length(x);
    ny = length(y);
else
    xg = x;
    yg = y;
    nx = size(x,2);
    ny = size(y,1);
end

sub = find(~isnan(Z));
% xg = xg(sub);
% yg = yg(sub);

nZ = numel(Z);
if isempty(AIn) || (size(AIn,1) ~= nZ) %|| (length(sub) ~= nZ)
    AIn = fL(xg(:), yg(:), pX, pY);
end

A = AIn(sub,:);

b = double(Z(sub));

if isa(b, 'uint8')
    b = double(b);
end

lastwarn('')
pasSample = 1;
flag = 1;
while flag
    subSample = 1:pasSample:length(b);
    
    if length(subSample) < N2
        flag = -1;
        return
    end
    
% Premi�re m�thode
%     lastwarn('')
%     tic
%     C = A(subSample,:) \ b(subSample);
%     toc
%     [warnmsg, msgid] = lastwarn;
    
% Deuxi�me m�thode
%     lastwarn('')
%     tic
%     C = inv(A'*A) * A' *b(subSample);
%     toc
%     [warnmsg, msgid] = lastwarn;
    
% Troisi�me m�thode
    lastwarn('')
%     tic
    C = pinv(A(subSample,:))*b(subSample);
%     toc
    [~, msgid] = lastwarn;
    
    if strcmp(msgid, 'MATLAB:rankDeficientMatrix') || strcmp(msgid, 'MATLAB:singularMatrix')
        pasSample = pasSample * 2;
        lastwarn('')
    else
        flag = 0;
    end
end

ZFit = NaN(ny, nx, class(Z(1)));
ZFit(sub) = A * C;

COut = C;
C = reshape(COut, pY+1, pX+1);

switch nargout
    case {1;2;3}
    case 0
        Eqm  = std(Z(sub) - ZFit(sub));
        ZDif =  Z - ZFit;
        str = sprintf('pX=%d  pY=%d', pX, pY);
        figure;
        subplot(2,3,1); imagesc(xg(1,:), yg(:,1), Z); colorbar; title('Z');
        axis equal; axis xy; axis tight
        I = ombrage(Z);
        subplot(2,3,4); imshow(xg(1,:), yg(:,1), I); axis xy;title('Z');

        subplot(2,3,2); imagesc(xg(1,:), yg(:,1), ZFit); colormap(jet(256)); colorbar;
        title([str ' : Modele']);
        axis equal; axis xy; axis tight

        subplot(2,3,3); imagesc(xg(1,:), yg(:,1), Z-ZFit); colorbar; title([str ' : Diff']);
        axis equal; axis xy; axis tight
        I = ombrage(Z-ZFit);
        subplot(2,3,6); imshow(xg(1,:), yg(:,1), I); axis xy;title([str ' : Diff'])
    case 4
        Eqm  = std(Z(sub) - ZFit(sub));
    case 5
        Eqm  = std(Z(sub) - ZFit(sub));
        ZDif = Z(sub) - ZFit(sub);
end


function LOut = fL(x, y, pX, pY)

nbPoints = length(x);
LOut = zeros(nbPoints, (pX+1)*(pY+1));
X = ones(1,pX+1);
Y = ones(1,pY+1);
for k=1:nbPoints
    for i=1:pX
        X(i+1) = x(k) ^ i;
    end
    for i=1:pY
        Y(i+1) = y(k) ^ i;
    end
    L = Y' * X;
    LOut(k,:) = L(:)';
end
