% Modelisation d'une surface par une forme polynomiale
%
% Syntax
%   Z = polyvalSurf(C, x, y)
%
% Input Arguments
%   C       : Coefficients de la forme polynomiale :
%               C11 * y^0 * x^0 + C12 * y^0 * x^1 + ... + C1pX * y^0 * y^pX +
%               C21 * y^1 * x^0 + C22 * y^1 * x^1 + ... + C2pX * y^1 * y^pX +
%               ... +
%               CpY1*y^pY * x^0 +CpY2 *y^pY*x^1 + ... + CpYpX*y^pY*y^pX
%   x : Abscisses (1:size(Z,2) sinon)
%   y : Ordonnees (1:size(Z,1) sinon)
%
% Name-only Arguments
%   2D : Interprete x et y comme les coordonnees d'une surface
%
% Output Arguments
%   Z : Surface a modeliser
%
% Remarks : Si 2D n'est pas donne, S sera de meme longueur que x et y
%
% Examples
%   [Z, x, y] = Prismed;
%   subx = 190:290; suby = 240:400;
%   C = polyvalSurf(x(subx), y(suby), Z(suby, subx), 'Plan');
%   [Azimuth, pente, penteDeg] = poly2penteAzimut(C)
%
%   P = polyvalSurf(C, x, y, '2D');
%   figure; imagesc(x, y, P); colorbar;
%   figure; imagesc(x(subx), y(suby), Z(suby, subx)-P(suby, subx));
%   colorbar; axis xy;
%   figure; imagesc(x, y, Z-P); colorbar; axis xy;
%
% See also polyvalSurf poly2penteAzimut Authors
% Authors : YHDR + JMA
%--------------------------------------------------------------------------------

function [Z, A] = polyvalSurf(C, x, y, varargin)

[varargin, A]    = getPropertyValue(varargin, 'A', []);
[varargin, Type] = getPropertyValue(varargin, 'Type', 'single');

[varargin, flag] = getFlag(varargin, '2D'); %#ok<ASGLU>

pY = size(C,1)-1;
pX = size(C,2)-1;

Plan = 0;
if (pX == 1) && (pY == 1) && (C(4) == 0)
    Plan = 1;
end

if flag
    if isvector(x)
        N = length(x);
        M = length(y);
        [x, y] = meshgrid(x, y);
    else
        N = size(x,2);
        M = size(x,1);
    end
    
else
    if length(x) ~= length(y)
        my_warndlg('polyvalSurf : x et y doivent etre de meme taille', 1);
        Z = [];
        return
    end
end

if isempty(A)
    try
        A = fL(x(:), y(:), pX, pY, Plan, Type);
    catch %#ok<CTCH>
        A = fLOLD(x(:), y(:), pX, pY, Plan, Type);
    end
end
Z = A * C(:);

if flag
    Z = reshape(Z, M, N);
end


function LOut = fL(x, y, pX, pY, Plan, Type)

%{
nbPoints = length(x);
LOut = zeros(nbPoints, (pX+1)*(pY+1), Type);

n = length(x);
Xp = ones(n, pX+1);
Yp = ones(n, pY+1);
for i=1:pX
Xp(:,i+1) = x .^ i;
end
for i=1:pY
Yp(:,i+1) = y .^ i;
end

% TODO : trouver comment acc�l�rer ce truc
for k=1:nbPoints
L = Yp(k,:)' * Xp(k,:);
LOut(k,:) = L(:)';
end

if Plan
LOut(:,end) = 0;
end
%}

nbPoints = length(x);
LOut  = zeros(nbPoints, (pX+1)*(pY+1), Type);

n = length(x);
nx = pX+1;
ny = pY+1;
Xp = ones(n, nx);
Yp = ones(n, ny);
for i=1:pX
    Xp(:,i+1) = x .^ i;
end
for i=1:pY
    Yp(:,i+1) = y .^ i;
end

for k=1:nx
    sub = (1:ny) + (k-1)*ny;
    LOut(:,sub) =  repmat(Xp(:,k), 1, ny);
end

for k=1:ny
    sub = (0:(nx-1)) * ny + k;
    LOut(:,sub) =  LOut(:,sub) .* repmat(Yp(:,k), 1, nx);
end

if Plan
    LOut(:,end) = 0;
end

function LOut = fLOLD(x, y, pX, pY, Plan, Type)

nbPoints = length(x);
LOut = zeros(nbPoints, (pX+1)*(pY+1), Type);
X = ones(1, pX+1);
Y = ones(1, pY+1);
for k=1:nbPoints
    for i=1:pX
        X(i+1) = x(k) ^ i;
    end
    for i=1:pY
        Y(i+1) = y(k) ^ i;
    end
    L = Y' * X;
    LOut(k,:) = L(:)';
end

if Plan
    LOut(:,end) = 0;
end
