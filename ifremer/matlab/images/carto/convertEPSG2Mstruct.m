% Fonction convertEPSG2MStruct : inspir� de shaperead2 (https://github.com/gdherbert/MatLab_Shaperead2)
%
% GEOG790 Project, Fall 2012. Grant Herbert gherbert@niu.edu
%
% Cr�ation d'une structure mstruct soit par la lecture d'un fichier .proj
% associ� � un fichier soit par la r�f�rence � un code EPSG.
%
% Syntax
%
%   [flag, S] = convertEPSG2Mstruct(varargin);
%
% Input Arguments
%   /
%
% Name-Value Pair Arguments
%
%   filename        : nom du fichier WKT.
%   nameProjCS      : cha�ne de projection du syst�me cartographique
%
% Output Arguments
%   flag        : flag de comportement du fichier.
%   mstruct     : structure de type defaultm (Mapping Toolbox)
%   strProj4    : cha�ne de la librairie Proj 4.
%
% Examples
%   nomFicWkt = 'C:\SonarScopeTbx\TravauxEnCours\cl_carto_MappingTbx\EPSG2154_Lambert93.wkt';
%   [flag, S, DeltaToWGS84] = convertEPSG2Mstruct('nomFic', nomFicWkt);
%   ou
%   nomProjCS = 'RGF93 / Lambert-93';
%   [flag, S, DeltaToWGS84, strProj4] = convertEPSG2Mstruct('CsLabelName', nomProjCS);
%   ou
%   [flag, S, DeltaToWGS84, strProj4] = convertEPSG2Mstruct('CodeEPSG', '27561');
%
% See also cl_carto xy2latlon Authors
% References : cl_carto
% Authors : GLU
%-------------------------------------------------------------------------------

function [flag, mstruct, deltaWGS84, strProj4] = convertEPSG2Mstruct(varargin)

flag        = 0;
deltaWGS84  = [];
mstruct     = [];

if nargin ~= 2
    strFR = sprintf('SVP, v�rifier les param�tres d''entr�e de : %s.', mfilename);
    strUS = sprintf('Please, check input parameters for : %s.', mfilename);
    my_warndlg(Lang(strFR, strUS), 1);
    return
end

[varargin, nomFicWkt] = getPropertyValue(varargin, 'nomFic', []);
hasProjection = false;

if ~isempty(nomFicWkt)
    % Ouverture du fichier de projection.
    fidWkt = fopen(nomFicWkt);
    if (fidWkt == -1) % not found
        strFR = sprintf('Probl�me � l''ouverture du fichier : %s ', nomFicWkt);
        strUS = sprintf('Probl�me � l''ouverture du fichier : %s ', nomFicWkt);
        my_warndlg(Lang(strFR, strUS), 1);
    else
        % read WKT from shapefile projection file. Use textscan rather than
        % textread as latter will be deprecated
        P = textscan(fidWkt, '%c');
        P = P{:}'; %turn cell array into a more usable array string
        
        % check for ESRI projection type using first 6 characters of P
        if strncmpi(P, 'GEOGCS', 6) || strncmpi(P, 'PROJCS', 6)
            % get projection parameters
            [sEPSG, hasProjection] = getProj(P);
        else % neither - eg geocentric
            strFR = sprintf('Pas de projection g�r�e pour le fichier : %s ', nomFicWkt);
            strUS = sprintf('Unable to handle projection for : %s ', nomFicWkt);
            my_warndlg(Lang(strFR, strUS), 1);
        end
        fclose(fidWkt);
    end
else
    [flag, sEPSG, hasProjection] = getParamsProjFromESRIFile(varargin{:});
    strProj4 = sEPSG.strProj4;
end

% Si la projection est bien trouv�e parmi celles recens�es dans la liste
% des projections sous <matlabroot>\toolbox\map\mapproj\projdata\proj
if hasProjection == true
    [flag, mstruct, deltaWGS84] = createMstruct(sEPSG.projName, sEPSG.paramsProj);
end

flag = 1;


function [sEPSG, hasProjection] = getProj(P)
% Purpose: split WKT to get projection name, pass to function to look up
% projection parameters. Name is in first set of double quotes.
% Parameters: requires a WKT projection string or a projection name

[~, remain] = strtok(P, '"'); % split WKT prj string at first " delimiter
[pName, ~] = strtok(remain, '"'); % split remain to get just the name

% looking for special cases which need to be fixed as WKT differs
% from matlab esri file.

if strncmpi(pName, 'WGS_1984', 8) %start of UTM names
    pName = strrep(pName,'WGS_1984','WGS_84');
    
elseif strncmpi(pName, 'NAD_1983_CSRS', 13)
    pName = strrep(pName,'NAD_1983_CSRS','NAD83(CSRS98)');
    
elseif strncmpi(pName, 'GCS_WGS_1984', 12) %geographic
    pName = strrep(pName,'GCS_WGS_1984','WGS_84');
    
elseif strncmpi(pName, 'NAD_1983_StatePlane', 19) %US StatePlane special
    x = regexpi(pName, 'Feet'); % returns index
    
    if x > 0 % do nothing, has Feet in name so will match properly
    else % is NAD 83 but no 'Feet' in WKT
        pName = strrep(pName, 'NAD_1983_StatePlane', 'NAD_1983_HARN_StatePlane');
    end
end

% projN is the matched name,
% mProjString is a cell array of projection parts
[flag, sEPSG, ~] = getParamsProjFromESRIFile('CsLabelName', pName);
if ~flag
    return
end

if strcmpi(sEPSG.sProjN, '')
    strFR = sprintf('Projection %s non trouv�e.', pName);
    strUS = sprintf('Projection %s not fond.', pName);
    my_warndlg(Lang(strFR, strUS), 1);
    hasProjection = false;
elseif strcmpi(sEPSG.aProjParams,'')
    strFR = sprintf('Pas de valeurs de projection pour : %s.', pName);
    strUS = sprintf('No projection values exist for : %s', pName);
    my_warndlg(Lang(strFR, strUS), 1);
    hasProjection = false;
else
    hasProjection = true;
end
