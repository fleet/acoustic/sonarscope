% Fonction convertMstruct2EPSG : inspir� de shaperead2 (https://github.com/gdherbert/MatLab_Shaperead2)
%
% GEOG790 Project, Fall 2012. Grant Herbert gherbert@niu.edu
%
% Identification du code EPSG � partir d'une structure mstruct, objet
% structurant les Projections/Ellipsoides de la MApping Toolbox.
%
% Syntax
%
%   [flag, S] = convertMstruct2EPSG(varargin);
%
% Input Arguments
%   MStruct         :   objet d�crivant la structure de projection
%                       Cartographique.
%
% Name-Value Pair Arguments
%   /
%
% Output Arguments
%   flag        : flag de comportement du fichier.
%   mstruct     : structure de type defaultm (Mapping Toolbox)
%   strProj4    : cha�ne de la librairie Proj 4.
%
% Examples
%   nomFicWkt = 'C:\SonarScopeTbx\TravauxEnCours\cl_carto_MappingTbx\EPSG2154_Lambert93.wkt';
%   [flag, S, DeltaToWGS84] = convertMstruct2EPSG('nomFic', nomFicWkt);
%   ou
%   nomProjCS = 'RGF93 / Lambert-93';
%   [flag, S, DeltaToWGS84, strProj4] = convertMstruct2EPSG('CsLabelName', nomProjCS);
%   ou
%   [flag, S, DeltaToWGS84, strProj4] = convertMstruct2EPSG('CodeEPSG', '27561');
%
% See also cl_carto xy2latlon Authors
% References : cl_carto
% Authors : GLU
%-------------------------------------------------------------------------------

function [flag, CodeEPSG] = convertMstruct2EPSG(varargin)

flag  = 0;

if nargin ~= 2
    strFR = sprintf('SVP, v�rifier les param�tres d''entr�e de : %s.', mfilename);
    strUS = sprintf('Please, check input parameters for : %s.', mfilename);
    my_warndlg(Lang(strFR, strUS), 1);
    return
end

[varargin, MStruct] = getPropertyValue(varargin, 'MStruct', []); %#ok<ASGLU>

CodeEPSG = [];

if ~isempty(MStruct)
    switch lower(MStruct.mapprojection)
        case 'mercator'
            CodeEPSG = '54004';
            
        case 'ups'
            CodeEPSG = '54026';
            
        case 'lambertstd'
            if MStruct.nparallels == 2 % Identification de Lambert 93
                if MStruct.mparallels(1) == 44 && MStruct.mparallels(2) == 49
                    projName = 'RGF93 / Lambert-93';
                end
            else
                if nearlyEqual(MStruct.falsenorthing,200000, 0.01)
                    if nearlyEqual(MStruct.scalefactor, 0.999877341, 0.00001)
                        projName = 'NTF (Paris) / Lambert Nord France';
                    elseif nearlyEqual(MStruct.scalefactor, 0.99987742, 0.00001)
                        projName = 'NTF (Paris) / Lambert Centre France';
                    elseif nearlyEqual(MStruct.scalefactor, 0.999877499, 0.00001)
                        projName = 'NTF (Paris) / Lambert Sud France';
                    end
                elseif MStruct.falsenorthing == 185861.369
                    projName = 'NTF (Paris) / Lambert Corse';
                elseif MStruct.falsenorthing == 1200000
                    projName = 'NTF (Paris) / Lambert zone I';
                elseif MStruct.falsenorthing == 2200000
                    projName = 'NTF (Paris) / Lambert zone II';
                elseif MStruct.falsenorthing == 3200000
                    projName = 'NTF (Paris) / Lambert zone III';
                elseif MStruct.falsenorthing == 4185861.369
                    projName = 'NTF (Paris) / Lambert zone IV';
                end
                
            end
            [flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CsLabelName', projName); %#ok<ASGLU>
            CodeEPSG = structEPSG.code;
            
        case 'utm'
            Fuseau  = MStruct.zone(1:end-1);
            Hemi    = MStruct.zone(end);
            strProj = ['WGS 84 / UTM zone ' num2str(Fuseau) Hemi];
            [flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CsLabelName', strProj); %#ok<ASGLU>
            CodeEPSG = structEPSG.code;
            
        case 'eqdcylin'
            CodeEPSG = '54026';
            
            
        case 'longlat'
            % has no comparable matlab name, use pcarree
            CodeEPSG = '4326';
            
        otherwise
            strFR = sprintf('Rien de trouv� pour le code de projection %s', MStruct.mapprojection, ', on utilise par d�faut : 4326 ');
            strUS = sprintf('No match found for projection Code %s', MStruct.mapprojection, ', using default: 4326 ');
            my_warndlg(Lang(strFR, strUS), 1);
            CodeEPSG = 4326;
    end
    
end

flag        = 1;



