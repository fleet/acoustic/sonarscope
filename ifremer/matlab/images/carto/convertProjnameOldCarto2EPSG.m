function [flag, CodeEPSG] = convertProjnameOldCarto2EPSG(projName)

switch projName
    case 'Geodetic'
        CodeEPSG        = '4326';
        
    case 'Mercator'
        CodeEPSG        = '54004';
        
    case 'Stereographique'
        CodeEPSG        = '54026';
    
    % Cas des projections Lambert.
    case {'Lambert 93', 'Other Secant', 'Other Tangent'} % On apparente les Lambert non identifi�es � du Lambert.
        CodeEPSG        = '2154';
    case 'France 1'
        CodeEPSG        = '27561';
    case 'France 2'
        CodeEPSG        = '27562';
    case 'France 3'
        CodeEPSG        = '27563';
    case 'France 4'
        CodeEPSG        = '27564';

    case 'France 1 Etendu'
        CodeEPSG        = '27571';
    case 'France 2 Etendu'
        CodeEPSG        = '27572';
    case 'France 3 Etendu'
        CodeEPSG        = '27573';
    case 'France 4 Etendu'
        CodeEPSG        = '27574';

    otherwise
        CodeEPSG = [];
end
flag = 1;
