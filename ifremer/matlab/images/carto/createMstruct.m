function [flag, mstruct, deltaWGS84] = createMstruct(sProjN, aProjParams)

% Purpose: extract the parts from mProjString{1} and build the mstruct

% define default parameters
isOrigin        = false;
isEasting       = false;
isNorthing      = false;
isScaled        = false;
isGeoid         = false;
isZone          = false;
isParallels     = false;
isSemiMajorAxis = false;
isSemiMinorAxis = false;

sProj        = 'pcarree'; % default projection to use
aOrigin      = [0 0 0];
nFEasting    = 0.0;
nFNorthing   = 0.0;
aZone        = [];
aGeoid       = [1 0]; % default
aMapLimLat   = [0 0];
aMapLimLon   = [0 0];
nScaleFactor = 1.0;

deltaWGS84.DX = 0;
deltaWGS84.DY = 0;
deltaWGS84.DZ = 0;
deltaWGS84.RX = 0;
deltaWGS84.RY = 0;
deltaWGS84.RZ = 0;
deltaWGS84.M  = 0;

for k1=1:length(aProjParams{1})
    k2 = aProjParams{1}{k1};
    % split at '='
    splitJ = regexp(k2, '=', 'split');
    switch splitJ{1} % first parameter
        case '+proj' % projection
            switch splitJ{2} % change proj codes to matlab expected
                case 'tmerc'
                    sProj = 'tranmerc';
                case 'merc'
                    sProj = 'mercator';
                case 'lcc'
                    sProj = 'lambertstd';
                case 'laea'
                    sProj = 'eqaazim';
                case 'aea'
                    sProj = 'eqaconicstd';
                case 'aeqd'
                    sProj = 'eqdazim';
                case 'eqdc'
                    sProj = 'eqdconicstd';
                case 'stere'
                    sProj = 'stereo';
                case 'utm'
                    sProj = 'utm';
                case 'cass'
                    sProj = 'cassinistd';
                case 'mill'
                    sProj = 'miller';
                case 'poly'
                    sProj = 'polyconstd';
                case 'robin'
                    sProj = 'robinson';
                case 'sinu'
                    sProj = 'sinusoid';
                case 'longlat'
                    % has no comparable matlab name, use pcarree
                    sProj = 'mercator'; % 'pcarree';
                otherwise
                    strFR = sprintf('Rien de trouv� pour %s', splitJ{2}, ', on utilise par d�faut : %s ',sProj);
                    strUS = sprintf('No match found for %s',  splitJ{2}, ', using default: %s ',         sProj);
                    my_warndlg(Lang(strFR, strUS), 1);
            end
            % origin
            
        case '+lat_0'
            aOrigin(1) = str2double(splitJ{2});
            if ~isParallels
                % On a lu les lat_1 et lat_2 si elles sont d�clar�es.
                mapParallels = aOrigin(1);
                nParallels   = 1;
                isParallels  = true;
            end
            isOrigin = true;
            
        case '+lat_ts'
            aOrigin(1) = str2double(splitJ{2});
            isOrigin   = true;

        case '+lat_1'
            mapParallels(1) = str2double(splitJ{2});
            nParallels      = 1;
            isParallels     = true;

        case '+lat_2'
            mapParallels(2) = str2double(splitJ{2});
            nParallels      = 2;
            isParallels     = true;

        case '+lon_0'
            aOrigin(2) = str2double(splitJ{2});
            isOrigin   = true;
            
        case {'+k', '+k_0'}
            nScaleFactor = str2double(splitJ{2});
            isScaled     = true;
            
        case '+x_0'
            nFEasting = str2double(splitJ{2});
            isEasting = true;
            
        case '+y_0'
            nFNorthing = str2double(splitJ{2});
            isNorthing = true;
            % ellipsoid and adjustment for feet if included
            
        case '+ellps'
            % Ancien codage de Grant Herbert : aGeoid    = almanac('earth',splitJ{2}); % get values from ellipsoid name
            % Ancien codage de Grant Herbert : aGeoid(1) = 1000*aGeoid(1);
            switch lower(splitJ{2}) % change proj codes to matlab expected
                case 'grs80'
                    splitJ{2} = 'grs80';
                case 'wgs84'
                    splitJ{2} = 'wgs84';
                case 'everest'
                    splitJ{2} = 'everest';
                case 'bessel'
                    splitJ{2} = 'bessel';
                case 'airy'
                    splitJ{2} = 'airy1830';
                case 'clrk66'
                    splitJ{2} = 'clarke80';
                case 'clrk80'
                    splitJ{2} = 'clarke80';
                case 'intl'
                    splitJ{2} = 'international';
                case 'krass'
                    splitJ{2} = 'krasovsky';
                case 'wgs72'
                    splitJ{2} = 'wgs72';
                case 'wgs60'
                    splitJ{2} = 'wgs60';
                otherwise
                    strFR = sprintf('Rien de trouv� pour %s', splitJ{2}, ', on utilise par d�faut : %s ','wgs84');
                    strUS = sprintf('No match found for %s',  splitJ{2}, ', using default: %s ',         'wgs84');
                    my_warndlg(Lang(strFR, strUS), 1);
                    splitJ{2} = 'wgs84';
            end
            aGeoid  = referenceEllipsoid(splitJ{2}, 'meters');
            isGeoid = true;
            
        case '+a'
            SemiMajorAxis   = str2double(splitJ{2});
            isSemiMajorAxis = true;

        case '+b'
            SemiMinorAxis   = str2double(splitJ{2});
            isSemiMinorAxis = true;

        case '+to_meter'
            % projection in feet, need to convert semimajor axis as is metre for mstruct
            aGeoid(1) = str2double(splitJ{2})*aGeoid(1);
            
            
        case '+zone'
            suf   = sProjN(end:end); % zone code does not contain N or S!, use name to find out
            aZone = [splitJ{2},suf];
            [aMapLimLat, aMapLimLon] = utmzone(aZone); % get limit params for zone
            isZone = true;
            % not all proj4 parameters appear to be supported by mstruct
            
        case '+datum'
        case '+pm'
        case '+towgs84'
            pppp = regexp(splitJ{2}, ',', 'split');
            if numel(pppp) == 3
                deltaWGS84.X = str2double(pppp{1});
                deltaWGS84.Y = str2double(pppp{2});
                deltaWGS84.Z = str2double(pppp{3});
            elseif numel(pppp) == 7
                % Voir calcul sur http://trac.osgeo.org/proj/wiki/GenParms. 
                deltaWGS84.DX = str2double(pppp{1});
                deltaWGS84.DY = str2double(pppp{2});
                deltaWGS84.DZ = str2double(pppp{3});
                deltaWGS84.RX = str2double(pppp{4});
                deltaWGS84.RY = str2double(pppp{5});
                deltaWGS84.RZ = str2double(pppp{6});
                deltaWGS84.M  = str2double(pppp{7});
            end
    end
end
% add special cases not caught above here
switch sProjN
    case 'World Bonne'
        sProj = 'bonne';
    case 'World Plate Carree' % just in case the default is changed later
        sProj = 'pcarree';
end

% build projection from parameters, all optional so test
% Cr�tion de la structure
mstruct = defaultm(sProj);
if isGeoid
    mstruct.geoid = aGeoid;
    % disp('geoid set')
elseif isSemiMinorAxis && isSemiMajorAxis
    [flag, codeEllipsoid, aGeoid] = getParamsEllipsoidFromEllipsoidCsv('SemiMajorAxis', SemiMajorAxis, 'SemiMinorAxis', SemiMinorAxis);
    mstruct.geoid = aGeoid;    
end

if isOrigin
    mstruct.origin = aOrigin;
    % disp('origin set')
end

if isParallels
    mstruct.mapparallels = mapParallels;
    mstruct.nparallels   = nParallels;
    % disp('parallels set')
end

if isZone
    mstruct.zone = aZone;
    mstruct.maplatlimit = aMapLimLat;
    mstruct.maplonlimit = aMapLimLon;
    % disp('zone set')
end
if isEasting
    mstruct.falseeasting = nFEasting;
    % disp('falseeasting set')
end
if isNorthing
    mstruct.falsenorthing = nFNorthing;
    % disp('falsenorthing set')
end
if isScaled
    mstruct.scalefactor = nScaleFactor;
    % disp('scalefactor set')
end

mstruct = defaultm(mstruct);

% % Test JMA le 14/03/2019
% if strcmp(mstruct.mapprojection, 'utm') && contains(mstruct.zone, 'S')
%     mstruct.falsenorthing = 10000000;
% end

flag = 1;

end % Fin de CreateMstruct
