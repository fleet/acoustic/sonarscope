function [flag, mstruct, deltaToWGS84, strProj4, CodeEPSGLambert] = getLambertProjection(strProjection)

strProj = strProjection.Lambert.strType{strProjection.Lambert.Type};
[flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CsLabelName', strProj); %#ok<ASGLU>
CodeEPSGLambert = structEPSG.code;

% On rappelle la conversion du code EPSG vers mstruct.
[flag, mstruct, deltaToWGS84, strProj4] = convertEPSG2Mstruct('CodeEPSG', CodeEPSGLambert);
if strcmpi(CodeEPSGLambert, '2154')
    % Pour conserver la projection Lambert 93 en mode Tangent et ne pas
    % passer en mode bisecant par l'exploitation du proj.4.
    lat_0                 = 46.51943204;
    scalefactor           = 0.999051030;
    mstruct.mapparallels  = [lat_0 lat_0];
    mstruct.origin        = [lat_0 mstruct.origin(2:end)];
    mstruct.scalefactor   = scalefactor;
    mstruct.nparallels    = 1;
    mstruct.falsenorthing = 6.602157839000000e+06;
end
if ~flag || isempty(mstruct)
    str1 = 'Probl�me de positionnement de la projection via code EPSG.';
    str2 = 'Problem in EPSG assignment for projection';
    my_warndlg(Lang(str1,str2), 1);
    return
end
