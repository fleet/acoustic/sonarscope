function [flag, mstruct, deltaToWGS84, strProj4, CodeEPSGOther] = getOtherProjection(strProjection)

strProj = strProjection.strType{strProjection.Type};
[flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CsLabelName', strProj);

CodeEPSGOther = structEPSG.code;

% On rappelle la conversion du code EPSG vers mstruct.

% Si la projection est bien trouv�e parmi celles recens�es dans la liste
% des projections sous <matlabroot>\toolbox\map\mapproj\projdata\proj
if hasProjection == true
    [flag, mstruct, deltaToWGS84] = createMstruct(structEPSG.projName, structEPSG.paramsProj);
    strProj4 = structEPSG.strProj4;
end
if ~flag || isempty(mstruct)
    str1 = 'Probl�me de positionnement de la projection via code EPSG.';
    str2 = 'Problem in EPSG assignment for projection';
    my_warndlg(Lang(str1,str2), 1);
    return
end
