% R�cup�ration des param�tres d'ellipsoide dans le fichier ESRI de la
% Mapping Toolbox de MatLab :
% <matlabroot>\toolbox\map\mapproj\projdata\proj
%
%
% Syntax
%   [projName, projParams] =  = getParamsEllipsoidFromEllipsoidCsv(str)
%
% Input Arguments
%   SemiMinorAxis : demi petit-axe de l'ellipsoid
%   SemiMajorAxis : demi grand-axe de l'ellipsoid
%
% Output Arguments
%   codeEllipsoid    : cha�ne d'identification de la projection
%   paramsEllipsoid  : param�tres de projection
%
% Examples
%   [flag, codeEllipsoid, paramsEllipsoid] = getParamsEllipsoidFromEllipsoidCsv('SemiMajorAxis', 6378249.2, 'SemiMinorAxis', 6356515)
%   [flag, codeEllipsoid, paramsEllipsoid] = getParamsEllipsoidFromEllipsoidCsv('SemiMajorAxis', 6378137, 'InverseFlattening', 298.257222101)
%
% See also cl_carto cl_carto/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------
function [flag, codeEllipsoid, paramsEllipsoid] = getParamsEllipsoidFromEllipsoidCsv(varargin)

flag = 0;

[varargin, semiMinorAxis] = getPropertyValue(varargin, 'SemiMinorAxis', []);
[varargin, semiMajorAxis] = getPropertyValue(varargin, 'SemiMajorAxis', []);
[varargin, invFlattening] = getPropertyValue(varargin, 'InverseFlattening', []); %#ok<ASGLU>

codeEllipsoid   = '';
paramsEllipsoid = '';

%% MATLAB Ellipsoid.csv file location

ellipsoid_file = fullfile(matlabroot, 'toolbox', 'map', 'mapproj', 'projdata', 'epsg_csv', 'ellipsoid.csv');
if ~exist(ellipsoid_file, 'file')
    ellipsoid_file = getNomFicDatabase('ellipsoid.csv');
end
fidEllipsoid = fopen(ellipsoid_file);
if (fidEllipsoid == -1) % file not found
    strFR = sprintf('Le fichier ESRI "%s" de la Mapping Toolbox n''est pas trouv�', ellipsoid_file);
    strUS = sprintf('The Mapping Toolbox ESRI file "%s" was not found!', ellipsoid_file);
    my_warndlg(Lang(strFR, strUS), 1);
    return
end

formatSpec = '%s';
nbCols     = 13;
% Lecture du Header (bizarrement 13 colonnes suffisent alors que 14 sont
% n�cessaires pour les donn�es).
C_text  = textscan(fidEllipsoid, formatSpec, nbCols, 'delimiter', ','); %#ok<NASGU>

C_data0 = textscan(fidEllipsoid, '%d %s %f %d %f %f %d %s %s %s %s %s %s %d', 'delimiter', ',');

fclose(fidEllipsoid);

%% Recherche de l'ellipsoid par les propri�t�s Demi Grand Axe puis Demi Petit Axe

sub = find(C_data0{3} == semiMajorAxis);
dummy = [];
for k=1:numel(sub)
    dummy(end+1, :) = [double(C_data0{1,1}(sub(k))) double(C_data0{1,3}(sub(k))) double(C_data0{1,5}(sub(k))) double(C_data0{1,6}(sub(k)))]; %#ok<AGROW>
end

if ~isempty(semiMinorAxis)
    sub2 = find(dummy(:,4) == semiMinorAxis);
elseif  ~isempty(invFlattening)
    sub2 = find(dummy(:,3) == invFlattening);
end

if ~isempty(sub2)
    codeEllipsoid   = dummy(sub2,1);
    paramsEllipsoid = referenceEllipsoid(codeEllipsoid);
else
    strFR = sprintf('Ellipsoide non trouv�e dans le fichier Ellipsoid.csv pour la projection recherch�e.');
    strUS = sprintf('Ellipsoid not found for properties searched.');
    my_warndlg(Lang(strFR, strUS), 1);
    return
end

flag = 1;
