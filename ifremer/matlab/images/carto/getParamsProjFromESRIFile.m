% R�cup�ration des param�tres de projection dans le fichier ESRI de la
% Mapping Toolbox de MatLab :
% <matlabroot>\toolbox\map\mapproj\projdata\proj
%
% Fichier inspir� de : shaperead2 (https://github.com/gdherbert/MatLab_Shaperead2)
% par
% GEOG790 Project, Fall 2012. Grant Herbert gherbert@niu.edu
% NDLA :
%     Purpose: Look up the projection from the list of ESRI projections in
%     matlab esri file in map toolbox folder. Get the parameters and create
%     an mstruct matching them.
%     Parameters: requires a projection name to be passed in. Returns the
%     matching projection name (projName) and a cell array of projection
%     parameters (projString). These are proj4 parameters (eg +proj=tmerc)
%
%     ISSUE - the name in the MATLAB esri file does not always match the WKT names
%     in the ESRI projection string. Hence the string manipulation. In particular
%     we need to remove forward slashes when comparing. Compares the tline
%     value with the tName using length(tline). This avoids mismatches where
%     the start is the same eg WGS 84 matching to WGS 84 TM 116.
%
% Syntax
%   [projName, ParamsProj] =  = getParamsProjFromESRIFile(str)
%
% Input Arguments
%   /
%
% PROPERTIES :
%   Code EPSG       : code EPSG de la projection
%   CsLabelName     : nom du Cs recherch� (Datum ou Projection)
%
% Output Arguments
%   flag            :   flag de comportement de la fonction (0 = Erreur, 1 sinon)
%   structEPSG      :	structure d'identification EPSG de la projection
%                       comportant les champs suivants:
%   * projName      : cha�ne d'identification de la projection
%   * paramsProj    : param�tres de projection
%   * strProj4      : cha�ne Proj4 de la projection
%   * code          : code EPSG.
%   hasProjection	: bool�en de r�sultat recherche de la projection.
%
% Examples
%   [flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CodeEPSG', '27561')
%
%   [flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CsLabelName', 'NTF (Paris) / Lambert Nord France')
%
% See also cl_carto cl_carto/get Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, structEPSG, isFound] = getParamsProjFromESRIFile(varargin)

global IfrTbxResources %#ok<GVMIS>

[varargin, codeEPSG]        = getPropertyValue(varargin, 'CodeEPSG',       []);
[varargin, csLabelName]     = getPropertyValue(varargin, 'CsLabelName', []); %#ok<ASGLU>

flag = 0;

% Identifiation directe selon certains noms de projections en rapport avec
% le nommage des projections de l'ancienne classe cl_carto.
if ~isempty(csLabelName)
    [flag, codeEPSG] = convertProjnameOldCarto2EPSG(csLabelName);
    if ~isempty(codeEPSG)
        csLabelName = [];
    end
end

structEPSG.projName   = '';
structEPSG.paramsProj = '';
structEPSG.strProj4   = '';

% tidy up input
% replace underscore (not used in matlab esri projection file)
if ~isempty(csLabelName)
    csLabelName = strrep(csLabelName, '_', ' ');
    csLabelName = rmblank(csLabelName, 0);
end

% Localisation du fichier ESRI de r�f�rences des projections par MATLAB.
if isdeployed
    %     esri_proj = fullfile(matlabroot, 'toolbox', 'map', 'mapproj', 'projdata', 'proj', 'esri');
    %     if exist(esri_proj, 'file')
    %         my_warndlg(sprintf('ESRI file proj was found in "%s".', esri_proj), 0, 'FichierEsriWasFound');
    %     else
    esri_proj = getNomFicDatabase('esri');
    if exist(esri_proj, 'file')
        %             my_warndlg(sprintf('ESRI file proj was found in "%s".', esri_proj), 0, 'FichierEsriWasFound');
    else
        my_warndlg(sprintf('ESRI file proj "%s" was not found.', esri_proj), 1);
    end
    %     end
else
    esri_proj = fullfile(matlabroot, 'toolbox', 'map', 'mapproj', 'projdata', 'proj', 'esri');
end

if ~exist(esri_proj, 'file')
    esri_proj = fullfile(IfrTbxResources, 'MapTbx', 'esri');
end

fidESRI = fopen(esri_proj);
if fidESRI == -1 % file not found
    strFR = sprintf('Le fichier ESRI "%s" de la Mapping Toolbox n''est pas trouv�', esri_proj);
    strUS = sprintf('The Mapping Toolbox ESRI file "%s" was not found!', esri_proj);
    my_warndlg(Lang(strFR, strUS), 1);
    return
end

C = textscan(fidESRI, '%s', 'delimiter', '\n');
fclose(fidESRI);

% R�cup�ration de la projection depuis chaque cha�ne C.
codeProjC = regexp(C{1}, '<\w*>', 'match', 'once');

isNotEmptySearchProjName = ~isempty(csLabelName);
isNotEmptyCodeEPSG       = ~isempty(codeEPSG);

%% Exploitation de la structure lue

k = 1;
if isNotEmptySearchProjName
    while k < numel(C{1})
        if strncmpi(C{1}{k}, '# ', 2) % is a projection name
            % tline = strrep(tline,'/ ',''); % replace '/ ' as not in WKT
            tline = strrep(C{1}{k}, '# ', ''); % replace '# ' as not in WKT
            tline = rmblank(tline, 0);
            % On teste sur la cha�ne exacte du fichier WKT (diff�rence par
            % rapport au test d'origine de Grant Herbert).
            if strncmpi(tline, csLabelName, length(csLabelName))
                % tline2 = fgetl(fidESRI); % get next line
                if C{1}{k+1}(1) == '<' % if next line starts with a '<' then grab it
                    structEPSG.paramsProj = C{1}{k+1};
                    structEPSG.paramsProj = regexprep(structEPSG.paramsProj, '<(\w*)>', ''); % remove <> sections
                    structEPSG.paramsProj = strtrim(structEPSG.paramsProj);
                    % seperate the parts into cell array
                    structEPSG.paramsProj = textscan(structEPSG.paramsProj,'%s','delimiter',' ');
                    structEPSG.projName = tline;
                    structEPSG.strProj4 = C{1}{k+1};
                    break
                else % no projection parameters
                    structEPSG.paramsProj = '';
                end
            end
        end
        k = k +1;
    end
elseif isNotEmptyCodeEPSG
    while k < numel(C{1})
        if C{1}{k}(1) == '<' % if next line starts with a '<' then grab it
            if ~isempty(codeProjC{k})
                if strcmpi(codeProjC{k},['<' codeEPSG '>'])
                    structEPSG.paramsProj = regexprep(C{1}{k}, '<(\w*)>', ''); % remove <> sections
                    structEPSG.paramsProj = strtrim(structEPSG.paramsProj);
                    % seperate the parts into cell array
                    structEPSG.paramsProj = textscan(structEPSG.paramsProj,'%s','delimiter',' ');
                    structEPSG.strProj4   = C{1}{k};
                    structEPSG.projName   = C{1}{k-1};
                    break
                end
            end
        end
        k = k +1;
    end
end

% while k < numel(C{1})
%     if isNotEmptySearchProjName
%         if strncmpi(C{1}{k}, '# ', 2) % is a projection name
%             % tline = strrep(tline,'/ ',''); % replace '/ ' as not in WKT
%             tline = strrep(C{1}{k}, '# ', ''); % replace '# ' as not in WKT
%             tline = rmblank(tline, 0);
%             % On teste sur la cha�ne exacte du fichier WKT (diff�rence par
%             % rapport au test d'origine de Grant Herbert).
%             if strncmpi(tline, searchProjName, length(searchProjName))
%                 % tline2 = fgetl(fidESRI); % get next line
%                 if C{1}{k+1}(1) == '<' % if next line starts with a '<' then grab it
%                     structEPSG.paramsProj = C{1}{k+1};
%                     structEPSG.paramsProj = regexprep(structEPSG.paramsProj, '<(\w*)>', ''); % remove <> sections
%                     structEPSG.paramsProj = strtrim(structEPSG.paramsProj);
%                     % seperate the parts into cell array
%                     structEPSG.paramsProj = textscan(structEPSG.paramsProj,'%s','delimiter',' ');
%                     structEPSG.projName = tline;
%                     structEPSG.strProj4 = C{1}{k+1};
%                     break
%                 else % no projection parameters
%                     structEPSG.paramsProj = '';
%                 end
%             end
%     end
%     elseif isNotEmptyCodeEPSG
%         if C{1}{k}(1) == '<' % if next line starts with a '<' then grab it
%             if ~isempty(codeProjC{k})
%                 if strcmpi(codeProjC{k},['<' codeEPSG '>']);
%                     structEPSG.paramsProj   = regexprep(C{1}{k}, '<(\w*)>', ''); % remove <> sections
%                     structEPSG.paramsProj   = strtrim(structEPSG.paramsProj);
%                     % seperate the parts into cell array
%                     structEPSG.paramsProj   = textscan(structEPSG.paramsProj,'%s','delimiter',' ');
%                     structEPSG.strProj4     = C{1}{k};
%                     structEPSG.projName = C{1}{k-1};
%                     break
%                 end
%             end
%         end
%     end
% 	k = k +1;
% end

structEPSG.projName = strrep(structEPSG.projName, '# ', ''); % replace '# ' as not in WKT
structEPSG.projName = rmblank(structEPSG.projName, 0);

% Identification du code EPSG pour tous les cas de recherches fructueuses
pppp = regexp(structEPSG.strProj4, '<\w*>', 'match', 'once');
if ~isempty(pppp)
    structEPSG.code = regexprep(pppp, '<|>', '');
end

if strcmpi(structEPSG.paramsProj, '')
    strFR = sprintf('Projection non trouv�e pour le code EPSG : %s. Merci de tenter avec le label du CS.', codeEPSG);
    strUS = sprintf('Projection not found for EPSG Code %s. Please, test with CS label name', codeEPSG);
    % my_warndlg(Lang(strFR, strUS), 1);
    isFound = false;
else
    isFound = true;
end

flag = 1;
