function [flag, mstruct, deltaToWGS84, strProj4, CodeEPSGUTM] = getUTMProjection(strProjection)

if (strProjection.Type == 3) && (strProjection.UTM.Fuseau == 121) % NZGD2000
    CodeEPSGUTM = '2193';
    [flag, mstruct, deltaToWGS84, strProj4] = convertEPSG2Mstruct('CodeEPSG', CodeEPSGUTM);
    return
end

strProjUTM = ['WGS 84 / UTM zone ' num2str(strProjection.UTM.Fuseau) strProjection.UTM.Hemisphere];
[flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('CsLabelName', strProjUTM); %#ok<ASGLU>

CodeEPSGUTM = structEPSG.code;

% On rappelle la conversion du code EPSG vers mstruct.
[flag, mstruct, deltaToWGS84, strProj4] = convertEPSG2Mstruct('CodeEPSG', CodeEPSGUTM);
if ~flag || isempty(mstruct)
    str1 = 'Probl�me de positionnement de la projection via code EPSG.';
    str2 = 'Problem in EPSG assignment for projection';
    my_warndlg(Lang(str1,str2 ), 1);
    return
end
