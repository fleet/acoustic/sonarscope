function [flag, CodeEPSGEllipsoid] = identEllipsoidFromProjection(CodeEPSGProjection)

% Pour pouvoir trait� plus ais�ment les projections UTM.
if isa(CodeEPSGProjection, 'char')
    CodeEPSGProjection = str2num(CodeEPSGProjection);
end

% On est oblig� de traiter comme cela car les cha�nes PROJ4 de Lambert (au
% moins celles l�) ne pr�cise l'ellipsoide dans leur cha�ne Proj4.
switch CodeEPSGProjection
    case 4326' % 'Geodetic'
        CodeEPSGEllipsoid = '7030'; % wgs84

    case 54004' % 'Mercator'
        CodeEPSGEllipsoid = '7030'; % wgs84
    
    case 54026' % 'Stereographique'
        CodeEPSGEllipsoid = '7030'; % wgs84
    
    case num2cell(32630:32760) % UTM
        CodeEPSGEllipsoid  = '7030'; % wgs84

    % Cas des projections Lambert.
    case 2154 % 'Lambert 93'
        CodeEPSGEllipsoid = '7011';

     % 'Lambert France I � France iV �tendu'
    case {  27561; 27562; 27563; 27564; ...
            27571; 27572; 27573; 27574} 
        CodeEPSGEllipsoid = '7012';
            
    otherwise
        CodeEPSGEllipsoid = [];
end

flag = 1;