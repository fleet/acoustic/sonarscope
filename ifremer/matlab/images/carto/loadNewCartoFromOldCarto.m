% Fonction de chargement de l'ancienne structuration des donn�es dans la nouvelle forme de la classe cl_carto.
% On n'est entre deux classes (cl_carto et cl_carto), donc la
% fonction est stock�e dans une r�pertoire g�n�rique

function [flag, Carto] = loadNewCartoFromOldCarto(OldCarto)

if OldCarto.Projection.Type == 3 && ~isempty(OldCarto.Projection.UTM.Fuseau) && ~isempty(OldCarto.Projection.UTM.Hemisphere)
    [flag, mstruct, deltaToWGS84, strProj4, CodeEPSG] = getUTMProjection(OldCarto.Projection); %#ok<ASGLU>
elseif OldCarto.Projection.Type == 4
    [flag, mstruct, deltaToWGS84, strProj4, CodeEPSG] = getLambertProjection(OldCarto.Projection); %#ok<ASGLU>
else
    [flag, mstruct, deltaToWGS84, strProj4, CodeEPSG] = getOtherProjection(OldCarto.Projection); %#ok<ASGLU>
end

Carto = cl_carto('CodeEPSG', CodeEPSG);

%% Assignation des autres propri�t�s de la projection

Carto = set(Carto, 'Projection', OldCarto.Projection);

flag = 1;
