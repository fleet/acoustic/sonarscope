% Fonction de chargement de la structuration des donn�es dans la nouvelle forme de la classe cl_carto.
% On n'est entre deux classes (cl_carto et cl_carto), donc la
% fonction est stock�e dans une r�pertoire g�n�rique.
function [flag, Carto] = loadNewCartoFromSomeProperties(structCarto)

flag  = 0;
Carto = [];

try
    % Recr�ation de l'objet Geoid qui n'a pas pu �tre sauvegard� en tant qu'objet sous forme XML
        objGeoid = referenceEllipsoid(structCarto.MapTbx.Geoid.Code);
        structCarto.MapTbx = rmfield(structCarto.MapTbx, 'Geoid');
        structCarto.MapTbx.Geoid = objGeoid;
        if isnumeric(structCarto.MapTbx.Ellipsoide.CodeEPSG)
            structCarto.MapTbx.Ellipsoide.CodeEPSG = num2str(structCarto.MapTbx.Ellipsoide.CodeEPSG);
        end
        if isnumeric(structCarto.MapTbx.Projection.CodeEPSG)
            structCarto.MapTbx.Projection.CodeEPSG = num2str(structCarto.MapTbx.Projection.CodeEPSG);
        end
        
        % Retranscription des caract�res de contr�les
        if ~isempty(structCarto.MapTbx.Projection.Proj4)
            structCarto.MapTbx.Projection.Proj4 = regexprep(structCarto.MapTbx.Projection.Proj4, '-(', '<');
            structCarto.MapTbx.Projection.Proj4 = regexprep(structCarto.MapTbx.Projection.Proj4, ')-', '>');
        end
        
        Carto = cl_carto('Ellipsoide', structCarto.Ellipsoide, ...
                         'Projection', structCarto.Projection, ...
                         'MapTbx',     structCarto.MapTbx);
catch ME %#ok<NASGU>
    return
end
 
flag = 1;
                                