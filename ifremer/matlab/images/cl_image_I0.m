% Constructeur de cl_image_I0 : initialisation d'une instance 'neutre'
% qui permet l'acc�s aux m�thodes Static. La nouvelle �criture des objets 
% permettra d'all�ger ces aspects par lees attributs multiples de chaque m�thode.
% (Access, Static, Get/Set ...)
%
%
% Syntax
%   a = cl_image_I0(...)
% 
% Name-Value Pair Arguments
%  
% Output Arguments 
%   a : instance de cl_image
%
% Examples
%   I0 = cl_image_I0
%   methods(I0)
%
% See also cl_image/imagesc Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function I = cl_image_I0(varargin)

% TODO : remplacer cl_image_I0 par cl_image
% my_breakpoint

persistent I0

if isempty(I0)
%     I0 = cl_image([]);
%     I0 = cl_image.empty;
    I0 = cl_image;
else
    % Renouvellement des Tags lors de l'initialisation d'une instance. 
    I0.TagSynchroX        = num2str(rand(1));
    I0.TagSynchroY        = num2str(rand(1));
    I0.TagSynchroContrast = num2str(rand(1));
end

I = I0;
% On pourra passer un argument de type [] lors de la cr�ation de la classe avec �criture
% en nouvelle m�thodologie OOP de MatLab (variable de classe et non plus d'instance).
[~, flag] = getFlag(varargin, 'Empty');

% TODO : remplacer cl_image.empty par cl_image.empty

if flag
    I(:) = [];
end
