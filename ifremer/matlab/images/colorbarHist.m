% Creates a colorbar that displays the histogram of the data
%
% Syntax
%   h = colorbarHist(...)
%
% Output Arguments
%   h : Handle of the colorbar axe
%
% Examples
%   figure; imagesc(Lena); colormap(gray(256))
%   colorbarHist;
%
% See also colorbar my_hist
% Authors : RN JMA
%-------------------------------------------------------------------------------

function handle = colorbarHist(varargin)

NewNargin = nargin;

[varargin, x] = getPropertyValue(varargin, 'x', []);
[varargin, histogramme] = getPropertyValue(varargin, 'histo', []);

if isempty(x) || isempty(histogramme)
    Children = get(gca,'Children');
    
    Type = get(Children,'Type');
    isImage = strcmp(Type, 'image');
    
    if(length(Children) == 1)
        if isImage
            GCO = Children;
        else
            return
        end
    else
        sub = find(isImage == 1);
        GCO = Children(sub(1));
    end
    
    
    Image = get(GCO, 'CData');
    CLim = get(gca, 'CLim');
    m = CLim(1);
    M = CLim(2);
    N = 64 ;
    xCalcule = linspace(m,M,N);
    HistogrammeCalcule = my_hist(Image(:), xCalcule);
end

if isempty(x)
    x = xCalcule;
else
    NewNargin = NewNargin - 2;
end

if isempty(histogramme)
    histogramme = HistogrammeCalcule;
else
    NewNargin = NewNargin - 2;
end

% Creation des patchs pour le barregraphe

NbreNiveaux = length(histogramme);

m = x(1);
M = x(end);
pas1 = (x(2) - x(1)) ./ 2 ;
pas2 = (x(end) - x(end-1)) ./ 2 ;

bvx = [zeros(2, NbreNiveaux); repmat(histogramme, [2 1])];

bvy1 = x  - pas1 ;
bvy2 = x + pas2 ;
bvy = [bvy1; bvy2; bvy2; bvy1];

[~, Ind] = max(histogramme);

if (Ind == 1)
    Max = max(histogramme(2:end));
elseif (Ind == NbreNiveaux)
    Max = max(histogramme(1:end-1));
else
    Max = max(histogramme([1:Ind-1 Ind+1:end]));
end

% Affichage de l'histogramme (d'apres colorbar)

% changeNextPlot = 1;

if NewNargin < 1
    loc = 'vert';
else
    loc = varargin{1};
end

ax = [];

% Determine color limits by context.  If any axes child is an image
% use scale based on size of colormap, otherwise use current CAXIS.

ch = get(gcda,'children');
hasimage = 0;
% t = [];
cdatamapping = 'direct';

for i=1:length(ch)
    typ = get(ch(i),'type');
    if strcmp(typ,'image')
        hasimage = 1;
        cdatamapping = get(ch(i), 'CDataMapping');
    elseif strcmp(typ,'surface') && ...
            strcmp(get(ch(i),'FaceColor'),'texturemap') % Texturemapped surf
        hasimage = 2;
        cdatamapping = get(ch(i), 'CDataMapping');
    elseif strcmp(typ,'patch') || strcmp(typ,'surface')
        cdatamapping = get(ch(i), 'CDataMapping');
    end
end

if ( strcmp(cdatamapping, 'scaled') )
    % Treat images and surfaces alike if cdatamapping == 'scaled'
    %  t = caxis;
    %  d = (t(2) - t(1))/size(colormap,1);
    %  t = [t(1)+d/2  t(2)-d/2];
else
    if hasimage
        %        t = [1, size(colormap,1)];
    else
        %        t = [1.5  size(colormap,1)+.5];
    end
end

h = gcda;

if NewNargin==0
    % Search for existing colorbar
    ch = get(findobj(gcf,'type','image','tag','TMW_COLORHIST'),{'parent'}); ax = [];
    for i=1:length(ch)
        ud = get(ch{i},'userdata');
        d = ud.PlotHandle;
        if (numel(d) == 1) && isequal(d,h)
            ax = ch{i};
            pos = get(ch{i},'Position');
            if pos(3)<pos(4)
                loc = 'vert';
            else
                loc = 'horiz';
            end
            %             changeNextPlot = 0;
            % Make sure image deletefcn doesn't trigger a colorbar('delete')
            % for colorbar update
            set(get(ax,'children'),'deletefcn','')
            break;
        end
    end
end

origNextPlot = get(gcf,'NextPlot');
if strcmp(origNextPlot,'replacechildren') || strcmp(origNextPlot,'replace')
    set(gcf,'NextPlot','add')
end

if loc(1) == 'v' % Append vertical scale to right of current plot
    
    if isempty(ax)
        units = get(h,'units'); set(h,'units','normalized')
        pos = get(h,'Position');
        [az,el] = view;
        stripe = 0.075; edge = 0.02;
        if all([az,el]==[0 90])
            space = 0.05;
        else
            space = .1;
        end
        set(h,'Position',[pos(1) pos(2) pos(3)*(1-stripe-edge-space) pos(4)])
        rect = [pos(1)+(1-stripe-edge)*pos(3) pos(2) stripe*pos(3) pos(4)];
        ud.origPos = pos;
        
        % Create axes for stripe and
        % create DeleteProxy object (an invisible text object in
        % the target axes) so that the colorbar will be deleted
        % properly.
        ud.DeleteProxy = text('parent', h, 'visible', 'off',...
            'tag', 'ColorhistDeleteProxy',...
            'handlevisibility', 'off');
        %             'deletefcn','eval(''delete(get(gcbo,''''userdata''''))'','''')');
        ax = axes('Position', rect);
        setappdata(ax,'NonDataObject',[]); % For DATACHILDREN.M
        set(ud.DeleteProxy,'userdata',ax)
        set(h,'units',units)
    else
        axes(ax);
        ud = get(ax,'userdata');
    end
    
    % Create color stripe
    n = size(colormap,1);
    col = linspace(1,n, NbreNiveaux);
    %     fill(bvx, bvy, col, 'Tag','TMW_COLORHIST','deletefcn','colorbarHist(x,h,''delete'')')
    fill(bvx, bvy, col, 'Tag', 'TMW_COLORHIST')
    axis([0 Max+10 m M]);
    set(ax,'Ydir','normal')
    set(ax,'YAxisLocation','right')
    set(ax,'xtick',[])
    
    % set up axes deletefcn
    %     set(ax,'tag','Colorhist','deletefcn','colorbarHist(''delete'')')
    set(ax, 'tag', 'Colorhist')
    
elseif loc(1) == 'h' % Append horizontal scale to top of current plot
    
    if isempty(ax)
        units = get(h,'units'); set(h,'units','normalized')
        pos = get(h,'Position');
        stripe = 0.075; space = 0.1;
        set(h,'Position',...
            [pos(1) pos(2)+(stripe+space)*pos(4) pos(3) (1-stripe-space)*pos(4)])
        rect = [pos(1) pos(2) pos(3) stripe*pos(4)];
        ud.origPos = pos;
        
        % Create axes for stripe and
        % create DeleteProxy object (an invisible text object in
        % the target axes) so that the colorbar will be deleted
        % properly.
        ud.DeleteProxy = text('parent', h, 'visible', 'off',...
            'tag', 'ColorhistDeleteProxy',...
            'handlevisibility', 'off');
        %             'deletefcn','eval(''delete(get(gcbo,''''userdata''''))'','''')');
        ax = axes('Position', rect);
        setappdata(ax,'NonDataObject',[]); % For DATACHILDREN.M
        set(ud.DeleteProxy,'userdata',ax)
        set(h,'units',units)
    else
        axes(ax);
        ud = get(ax,'userdata');
    end
    
    % Create color stripe
    n = size(colormap,1);
    col = linspace(1,n, NbreNiveaux);
    %     fill(bvx, bvy, col, 'Tag','TMW_COLORHIST','deletefcn','colorbarHist(x,h,''delete'')');
    fill(bvx, bvy, col, 'Tag', 'TMW_COLORHIST');
    axis([0 Max+10 m M]);
    set(ax,'Ydir','normal')
    set(ax,'ytick',[])
    
    % set up axes deletefcn
    %     set(ax,'tag','Colorhist','deletefcn','colorbarHist(''delete'')')
    set(ax, 'tag', 'Colorhist')
    
else
    error('COLORHIST expects a handle, ''vert'', or ''horiz'' as input.')
end

if ~isfield(ud,'DeleteProxy'), ud.DeleteProxy = []; end
if ~isfield(ud,'origPos'), ud.origPos = []; end
ud.PlotHandle = h;
set(ax,'userdata',ud)
axes(h)
set(gcf, 'NextPlot', origNextPlot)
if ~isempty(legend)
    legend % Update legend
end
if nargout > 0
    handle = ax;
end


function h = gcda
%GCDA Get current data axes

h = datachildren(gcf);
if isempty(h) || any(h == gca)
    h = gca;
else
    h = h(1);
end

