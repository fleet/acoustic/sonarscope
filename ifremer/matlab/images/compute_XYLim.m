function XYLim = compute_XYLim(x)

switch length(x)
    case 0 % Pr�vu au cas o� mais jamais rencontr�
        XYLim(1) = NaN;
        XYLim(2) = NaN;
    case 1 % Cas rencontr� une fois mais probl�me r�solu en amont, je laisse au cas o�
        XYLim(1) = x;
        XYLim(2) = x;
    otherwise
        x = x(:);
        pasy = ((x(end)-x(1)) / (length(x) - 1)) / 2;
        pasy = abs(pasy);
        XYLim(1) = min(x) - pasy;
        XYLim(2) = max(x) + pasy;
end
