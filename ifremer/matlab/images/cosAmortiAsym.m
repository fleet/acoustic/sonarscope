% Generation d'une sinusoide amortie : y = A * exp(-x/Alpha) * cos(2*pi*x/Periode)
%
% Syntax
%   [y, composantes] = cosAmortiAsym(x, Parametres)
%
% Input Arguments 
%   x           : Abscisses
%   Parametres  : Tableau contenant les parametres du modele
%		A  : Amplitude a l'origine (x=0)
%		Alpha : Attenuation
%		Periode : Periode de la sinusoide
%
% Output Arguments
%   []          : Auto-plot activation
%   y           : Signal de synthese.
%   composantes : [lambert; speculaire; transitoire] ou [lambert; speculaire].
%
% Examples 
%   x = 0:250;
%   cosAmortiAsym(x, [1 50 100 0.3]);
%
%   y = cosAmortiAsym(x,  [1 50 100 0.3]);
%   plot(x, y); grid on; zoom on; hold on;
%
% See also cosAmortiAsym Authors
% Authors : JMA
% VERSION  : $Id: cosAmortiAsym.m,v 1.4 2002/06/17 16:21:37 augustin Exp $
%------------------------------------------------------------------------------

function varargout = cosAmortiAsym(x, Parametres)

Amplitude = Parametres(1);
Alpha     = Parametres(2);
Periode   = Parametres(3);
gamma     = Parametres(4);

nbCourbes = size(x, 1);
for i=1:nbCourbes
    composantes(1,:) = exp(-abs(x) / Alpha);


    y = 2 * pi * mod(x, Periode) .^ (1/gamma) / Periode .^ (1/gamma);
    composantes(2,:) = cos(y );
    y(i,:) = Amplitude * composantes(1,:) .* composantes(2,:);
end

% -----------------------------------------
% Sortie des parametre ou traces graphiques

if nargout == 0
    figure
    if nbCourbes == 1
        plot(x, y, 'k'); grid on; hold on;
        plot(x, composantes); grid on; hold off;
        xlabel('x'); ylabel('y');
        title('Modele cosAmortiAsym');
    else
        plot(x, y); grid on; hold on;
    end
else
    varargout{1} = y;
    varargout{2} = composantes;
end
