% Generation d'une sinusoide amortie : y = A * exp(-x/Alpha) * cos(2*pi*x/Periode)
%
% Syntax
%   [y, composantes] = cosinusAmorti(x, Parametres)
%
% Input Arguments 
%   x           : Abscisses
%   Parametres  : Tableau contenant les parametres du modele
%		A  : Amplitude a l'origine (x=0)
%		Alpha : Attenuation
%		Periode : Periode de la sinusoide
%
% Output Arguments
%   []          : Auto-plot activation
%   y           : Signal de synthese.
%   composantes : [lambert; speculaire; transitoire] ou [lambert; speculaire].
%
% Examples 
%   x = 0:250;
%   cosinusAmorti(x, [1 50 100]);
%
%   y = cosinusAmorti(x,  [1 50 100]);
%   plot(x, y); grid on; zoom on; hold on;
%
% See also cosinusAmorti Authors
% Authors : JMA
%------------------------------------------------------------------------------

function varargout = cosinusAmorti(x, Parametres)

Amplitude = Parametres(1);
Alpha     = Parametres(2);
Periode   = Parametres(3);
Delay     = Parametres(4);

if ~isfinite(Delay)
    Delay = 0;
end

nbCourbes = size(x, 1);
y = zeros(nbCourbes, length(x));
for k=1:nbCourbes
    composantes(1,:) = exp(-abs(x-Delay) / Alpha);
    composantes(2,:) = cos(2 * pi * (x-Delay) / Periode);
    y(k,:) = Amplitude * composantes(1,:) .* composantes(2,:);
end

%% Sortie des parametre ou trac�s graphiques

if nargout == 0
    figure
    if nbCourbes == 1
        plot(x, y, 'k'); grid on; hold on;
        plot(x, composantes); grid on; hold off;
        xlabel('x'); ylabel('y');
        title('Modele cosinusAmorti');
    else
        plot(x, y); grid on; hold on;
    end
else
    varargout{1} = y;
    varargout{2} = composantes;
end
