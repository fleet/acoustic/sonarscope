% Calcul de la matrice de couleur correspondant a un code de couleur asci
%
% Syntax
%   cOut = coul2mat(cIn)
%
% Input Arguments 
%   cIn : code de couleur ou matrice de 3 elements 'r' 'g' 'b' 'w' 'c' 
%          'm' 'y' 'k' 'gray'
%
% Examples 
%   coul2mat('r')
%   coul2mat([0 1 0])
%   coul2mat('[0 1 0]')
%   coul2mat('gray')
%
% See also Authors 
% Authors : JMA
%--------------------------------------------------------------------------------

function cOut = coul2mat(cIn)

rgbspec = [1 0 0; 0 1 0; 0 0 1; 1 1 1; 0 1 1; 1 0 1; 1 1 0; 0 0 0; 0.5 0.5 0.5];
cspec = 'rgbwcmyk';

% Deal with string color specifications.
if ischar(cIn)
    if isempty(cIn)
        %warndlg('Unknown color string.');
        cOut = [0 0 0];
    else
        k = find(cspec == cIn(1));
        if isempty(k)
            %warndlg('Unknown color string.');
            try
                cOut = eval(cIn);
            catch
                cOut = [];
            end
        else
            if length(cIn) == 1
                cOut = rgbspec(k,:);
            else
                if strcmpi(cIn(1:3), 'bla')
                    cOut = rgbspec(8,:);
                elseif strcmpi(cIn(1:3), 'blu')
                    cOut = rgbspec(3,:);
                elseif strcmpi(cIn(1:3), 'gre')
                    cOut = rgbspec(2,:);
                elseif strcmpi(cIn(1:3), 'gra')
                    cOut = rgbspec(9,:);
                % elseif strcmpi(cIn(1:3), 'gre')
                %     cOut = rgbspec(9,:);
                else
                    %warndlg('Unknown color string.');
                    cOut = [0 0 0];
                end
            end
        end
    end
else
    switch length(cIn)
        case 1
            kMod = 1 + mod(cIn-1, length(cspec));
            cOut = coul2mat(cspec(kMod));
        case 3
            cOut = cIn;
        otherwise
            cOut = cIn;
    end
end
