function [flag, mov, nomFicMovie] = create_movie_glu(nomFicMovie)

flag = false;
[nomDir, nomFic, ext] = fileparts(nomFicMovie);
for k=0:99
    if k > 0
        nomFic2 = sprintf('%s_%02d', nomFic, k);
        nomFicMovie = fullfile(nomDir, [nomFic2, ext]);
    end
    try
        mov = VideoWriter(nomFicMovie,'Motion JPEG AVI');
        mov.Quality   = 95; % seulement si Compression = 'Motion JPEG AVI'
        mov.FrameRate = 2;
        
        flag = true;
        break
    catch %#ok<CTCH>
        str1 = sprintf('Le fichier "%s" n''a pas pu �tre cr��, peut-�tre existe t''il d�j� ?', nomFicMovie);
        str2 = sprintf('File "%s" could not be created, already exists ?', nomFicMovie);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'nomFicMovieAlreadyExists');
    end
end
