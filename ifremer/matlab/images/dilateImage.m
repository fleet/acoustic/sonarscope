function Y = dilateImage(X, coef)

listCoef = unique(coef);
if length(listCoef) == 1
    Y = X;
    return
end

n1 = size(X,2);
n2 = floor(n1*max(coef));
x2 = linspace(1, n1, n2);
Y = NaN(size(X,1), n2, 'single');
x1 = 1:n1;
for k1=1:length(listCoef)
    sub = find(coef == listCoef(k1));
    if listCoef(k1) == 1
        Y(sub,x1) = X(sub,:);
    else
        for k2=1:length(sub)
            y1 = X(sub(k2),:);
            y2 = interp1(x1, y1, x2);
            Y(sub(k2),:) = y2;
        end
    end
end
s = sum(isnan(Y), 1);
subNaN = (s == size(Y,1));
Y(:,subNaN) = [];
