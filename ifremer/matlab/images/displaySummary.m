% Affichage du r�sum� des infos d'une s�rie d'images
%
% Syntax
%   liste = displaySummary(liste, ...)
%
% Input Arguments
%   Liste : Liste de layers Image
%
% Name-Value Pair Arguments
%   InitGeometryType : valeur du filtre Geometry Type par d�faut
%   InitDataType     : valeur du filtre Data Type par d�faut
%   OKCancel         : affichage ou non des boutons OK/Cancel
%   flagSelectLayer  : indice des layers s�lectionn�s par d�faut
%   strComments      : chaine de commentaires �volu�e � afficher dans le panneau haut
%   strQueryMode     : Mode d'affichage du module (Summary, UniSelect, MultiSelect)
%   Title            : Titre de la fen�tre.
%   strWindowStyle   : style Modal ou Normal.
%
% Output Arguments
%   flag       : appuie sur OK/Cancel
%   indexImage : Index des images s�lectionn�s
%   layerList  : Liste de layers r�sultants
%
% Examples
%     global IfrTbx %#ok<GVMIS>
%     load(fullfile(IfrTbx, 'Tests', 'Test_NewSummary.mat'));
%
%     dummy = displaySummary(str, 'FlagSelectLayer', 1);
%
%     dummy = displaySummary(str, 'FlagSelectLayer', 1, 'QueryMode', 'TidyUp');
%
%     dummy = displaySummary(str, 'FlagSelectLayer', 1, 'QueryMode', 'Summary');
%
%     dummy = displaySummary(str, 'FlagSelectLayer', 1, 'QueryMode', 'MultiSelect', 'InitGeometryType', 2);
%
%     dummy = displaySummary(str, 'FlagSelectLayer', 1, 'QueryMode', 'UniSelect');
%
%     dummy = displaySummary(str, 'FlagSelectLayer', 1, 'QueryMode', 'MultiSelect', ...
%                                               'InitGeometryType',     2, ...
%                                               'InitDataType',         2, ...
%                                               'Comments',             'Merci de s�lectionner les layers');
%
% See also summary Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [varargout] = displaySummary(str, varargin)

[varargin, initGeometryType] = getPropertyValue(varargin, 'InitGeometryType', 1);
[varargin, initDataType]     = getPropertyValue(varargin, 'InitDataType',     1);
[varargin, flagBtnOKCancel]  = getPropertyValue(varargin, 'OKCancel',         1);
[varargin, flagSelectLayer]  = getPropertyValue(varargin, 'flagSelectLayer',  0);
[varargin, strComments]      = getPropertyValue(varargin, 'Comments',         '');
[varargin, strQueryMode]     = getPropertyValue(varargin, 'QueryMode',        'Summary');
[varargin, Title]            = getPropertyValue(varargin, 'Title',            []);
[varargin, strWindowStyle]   = getPropertyValue(varargin, 'WindowStyle',      'normal'); %#ok<ASGLU>

% Pour �tre en mode Singleton
h = findobj(0, 'tag', 'tagFigSummary');
if ~isempty(h)
    varargout{1} = 0;
    varargout{2} = [];
    varargout{3} = [];
    return
end

%% Dessin du graphe.

if isempty(Title)
    if strcmp(strQueryMode, 'Summary')
        Title = Lang('R�sum� des images', 'Images summary');
    else
        Title = [Lang('R�sum� des images : ', 'Images summary : ') strQueryMode];
    end
end
hdlFig = figure('Position', [300 300 1400 400], ...
    'NumberTitle',     'Off', ...
    'tag',             'tagFigSummary', ...
    'Menubar',         'none',...
    'Units',           'normalized', ...
    'Name',            Title, ...
    'CloseRequestFcn', {@cbCloseFcn, []}, ...
    'Interruptible',   'off', ...
    'WindowStyle',     strWindowStyle);

% D�coupage en deux zones horizontales dont la 1�re est d�coup�e en trois sous-panneaux.
hH1Box    = uix.HBox('Parent', hdlFig, 'Tag', 'taghdlH1Box');
hH1V11Box = uix.VBox('Parent', hH1Box, 'Tag', 'tagV11Box');
hH1V12Box = uix.VBox('Parent', hH1Box, 'Tag', 'tagV12Box');
hPnlDroit = uipanel('Parent', hH1V12Box, 'Title','', 'Tag', 'tagPnlDroit');
% Les valeurs n�gatives sont vues comme des pourcentages.
% Ici, 100 pixels de haut, le reste est pris � 100% par le panel du milieu.
% set(hH1Box, 'Sizes', [-1 150], 'Spacing', 2)
set(hH1Box, 'Width', [-1 150], 'Spacing', 2)

% D�finition des Sous-panneaux du container Vertical.
if ~isempty(strComments)
    % Passage en HTML
    strComments = ['<div style="font-family:verdana;color:green;font-weight:bold;font-style:italic">' strComments '</div>'];
    hPnlComments = uipanel('Parent', hH1V11Box, 'Title',Lang('Commantaires','Comments'), 'Tag', 'tagPnlComment');
    % Les valeurs n�gatives sont vues comme des pourcentages.
    % Ici, 70 pixels de haut, le reste est pris � 100% par le panel du milieu.
    szv11Panels = [100 70 -1 40];
else
    % Les valeurs n�gatives sont vues comme des pourcentages.
    % Ici, 70 pixels de haut, le reste est pris � 100% par le panel du milieu.
    szv11Panels = [70 -1 40];
end
hPnlHaut   = uipanel('Parent', hH1V11Box, 'Title', Lang('Filter les �l�ments','Filter Items'), 'Tag', 'tagPnlHaut');
hPnlMedium = uipanel('Parent', hH1V11Box, 'Title', Lang('Liste de layers','Layers list'), 'Tag', 'tagPnlMedium');
hPnlBas    = uipanel('Parent', hH1V11Box, 'Title', '', 'Tag', 'tagPnlBas');

% set(hH1V11Box, 'Sizes', szv11Panels, 'Spacing', 2)
set(hH1V11Box, 'Heights', szv11Panels, 'Spacing', 2)

% Affichage de la table
hUiTable = uitable('parent', hPnlMedium, ...
    'Tag',                  'tagTableLayers', ...
    'RowStriping',          'on', ...
    'RearrangeableColumns', 'on', ...
    'Interruptible',        'off');

% Affectation par callback des items s�lectionn�s pour le cas du filtre.
set(hUiTable, 'CellSelectionCallback', @cbTabSelect);
set(hUiTable, 'Enable',                'On');

set(hUiTable, 'RearrangeableColumns', 'On')
set(hUiTable, 'Unit',                 'normalized',  ...
    'Position',             [0 0 1 1])

% nomFicMat = fullfile(my_tempdir, 'my_CustomTableSummary.mat');
% if exist(nomFicMat, 'file')
%     %     InfoSummary = xml_read(nomFicXml);
%     InfoColumn = loadmat(nomFicMat, 'nomVar', 'InfoColumn');
%     % Test d'int�grit� du fichier.
%     %     if isfield(InfoSummary, 'Column') && numel(InfoSummary.Column) == size(str,2)
%     if isfield(InfoColumn, 'Column') && numel(InfoColumn.Column) == size(str,2)
%         % Gestion de l'ordre de celles-ci.
%         % Inibition du message apparaissant en mode D�ploy� ou versions .
%         % http://www.mathworks.com/matlabcentral/answers/74169-unique-rows-cell-array-ignored
%         % Reclassement des donn�es � afficher
%         warning('off', 'MATLAB:INTERSECT:RowsFlagIgnored');
%         % R�cup�ration des indices de tri.
%         %         [C,ia,ib]   = intersect({InfoSummary.Column.Label}, str(1,:),'stable');  %#ok<ASGLU>
%         [C,ia,ib] = intersect({InfoColumn.Column.Label}, str(1,:),'stable');  %#ok<ASGLU>
%         warning('on', 'MATLAB:INTERSECT:RowsFlagIgnored');
%         %         try
%         %             str(:, :)   = str(:,ib);
%         %         catch % Rajout� JMA le 26/08/2013
%         %             str = str{:,ib};
%         %         end
%         if ~isempty(ib)
%             str(:, :) = str(:,ib);
%         end
%
%         %         columnWidth = {InfoColumn.Width};
%         columnWidth = {InfoColumn.Column.Width};
%     else
%         columnWidth = {400 'auto'};
%     end
% else
% A d�faut de fichier de personalisation, seule la 1�re colonne est
% customis�e.
columnWidth = {400 'auto'};
% end

% Recherche de l'index du champ Name
[C, idxFieldImageName, ib] = intersect(str(1,:), 'Name'); %#ok<ASGLU>
idxFieldImageName          = idxFieldImageName - 1;
setappdata(hUiTable, 'idxFieldImageName', idxFieldImageName);

% Assignation des attributs du tableau
currentLayers = str(2:end,:);
set(hUiTable,        'ColumnWidth', columnWidth)
set(hUiTable,        'ColumnName',  str(1,:))
set(hUiTable,        'Data',        currentLayers)
setappdata(hUiTable, 'OrigLayers',  currentLayers) % Conservation des layers de chargement de l'IHM

setappdata(hdlFig, 'LayersOutput', currentLayers)
setappdata(hdlFig, 'QueryMode',    strQueryMode)
sz          = size(str);
sTableObj   = [];
[flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hUiTable); %#ok<ASGLU>
if flagSelectLayer
    % Affectation de la liste d'indices s�lectionn�s si besoin.
    setappdata(hdlFig, 'IndexLayersOutput', flagSelectLayer);
    
    
    % Attributs de tri : les lignes suivantes proposent bien une gestion
    % des tris par colonnes mais inhibent la gestion des largeurs de
    % celles-ci
    % % % %     jTable.setSortable(true);		% or: set(jtable,'Sortable','on');
    % % % %     jTable.setAutoResort(true);
    % % % %     jTable.setMultiColumnSortable(true);
    % % % %     jTable.setPreserveSelectionsAfterSorting(true);
    
    % Attributs de s�lection.
    jTable.setColumnSelectionAllowed(true)
    jTable.setRowSelectionAllowed(true)
    jTable.setNonContiguousCellSelection(true)
    J = javaObject('javax.swing.DefaultListSelectionModel');
    jTable.setSelectionMode(int32(J.MULTIPLE_INTERVAL_SELECTION));
    %     pause(0.001); % N�cessaire si on utilise l'objet DefaultListSelectionModel ????.
    pause(0.1); % Modif JMA le 06/04/2016 au SHOM
    % toggle = false, extend = false (pour s�lectionner une 1�re cellule)
    % toggle = false, extend = true (pour �tendre l'inervalle)
    % toggle = true, extend = false (pour exclure la cellule l'inervalle)
    % Tentative de correction de l'instabilit�.
    try
        jTable.changeSelection(flagSelectLayer-1, idxFieldImageName, false, false);
    catch e
        % e.message
        if isa(e, 'java.lang.NullPointerException')
            % On retente le changeSelection
            jTable.changeSelection(flagSelectLayer-1, idxFieldImageName, false, false);
            ex = e.ExceptionObject;
            ex.printStackTrace;
        end
    end
    
    % For�age du r�affichage.
    jTable.repaint;
    jScroll.repaint
    
else
    % Affectation de la liste d'indices s�lectionn�s si besoin.
    setappdata(hdlFig,'IndexLayersOutput', []);
end
sTableObj.jTable  = jTable;
sTableObj.jScroll = jScroll;

% On ne dispose que d'une colonne : on ne fait que trier les layers.
if sz(2) == 1
    set(hUiTable, 'Unit', 'pixel');
    post = get(hUiTable, 'Position');
    set(hUiTable, 'Unit', 'normalized');
    set(hUiTable, 'ColumnWidth', {post(3)});
end

% Cr�ation des boutons lat�raux de d�placement des items.
if ~isempty(strComments)
    try
        flag = fcnInitPnlComments(hPnlComments, strComments); %#ok<NASGU>
    catch ME
        ME
    end
end

% Cr�ation des boutons lat�raux de d�placement des items.
flag = fcnInitPnlMoveItem(hPnlDroit); %#ok<NASGU>

% Cr�ation des boutons de filtrage des items.
flag = fcnInitPnlFilterItem(hPnlHaut, str, initGeometryType, initDataType); %#ok<NASGU>
% Cr�ation des boutons de validation ou annulation.
setappdata(hdlFig, 'flagOKCancel', 1);
flag = fcnInitPnlOKCancelClose(hPnlBas, sTableObj, flagBtnOKCancel); %#ok<NASGU>

% uiwait(hdlFig);
waitfor (hdlFig, 'WaitStatus', 'inactive');

% Traitement des variables en sortie.
if isappdata(hdlFig,'flagOKCancel')
    varargout{1} = getappdata(hdlFig,'flagOKCancel');
    rmappdata(hdlFig,'flagOKCancel');
end
if isappdata(hdlFig,'IndexLayersOutput')
    varargout{2} = getappdata(hdlFig,'IndexLayersOutput');
    rmappdata(hdlFig,'IndexLayersOutput');
end
if isappdata(hdlFig,'LayersOutput')
    % Dans les modes concern�s, choix de la pr�liste ou non par
    % l'op�rateur.
    if isappdata(hUiTable,'PrelistLayers')
        if varargout{1}
            str1 = 'Voulez-vous s�lectionner la pr�liste ou la s�lection de la liste courante';
            str2 = 'Do you want to use prelist or selection in current list';
            [flag, ~] = my_questdlg(Lang(str1,str2), 'Init', 2, 'WaitingTime', 10, ...
                Lang('Pr�liste', 'Prelist'), Lang('Liste affich�e', 'Displayed Liste'));
            if isempty(flag)
                listLayers = [];
            elseif flag == 1
                listLayers = getappdata(hUiTable,'PrelistLayers');
                % On r�impose les indices de s�lection par tout le contenu de la
                % pr�liste.
                varargout{2} = 1:size(listLayers, 1);
            else
                listLayers = getappdata(hdlFig,'LayersOutput');
            end
        else
            listLayers = getappdata(hdlFig,'LayersOutput');
        end
    else
        listLayers = getappdata(hdlFig,'LayersOutput');
    end
    % R�ordonnancement des n� de colonnes pour avoir tjs la cl� Name en 1er.
    if ~isempty(listLayers)
        idxFieldImageName            = idxFieldImageName + 1;
        idxFields                    = 1:sz(2);
        idxFields(idxFieldImageName) = [];
        newFields                    = [idxFieldImageName idxFields];
        listLayers                   = listLayers(:,newFields);
    end
    varargout{3} = listLayers;
    rmappdata(hdlFig,'LayersOutput');
end
delete(hdlFig);
end % NewSummary


%% Cr�ation des boutons des boutons de validation ou annulation.
function flag = fcnInitPnlOKCancelClose(hdlPnl, sTableObj, flagOKCancel)

flag = 0; %#ok<NASGU>

if flagOKCancel
    uicontrol(...
        'Parent',   hdlPnl, ...
        'Units',    'normalized', ...
        'Position', [0.0 0.0 0.5 1.0], ...
        'Tag',      'tagBtnOk', ...
        'String',   'OK', ...
        'Tooltip',  Lang('Valider','Validate'), ...
        'CallBack', {@cbBtnOK, sTableObj});
    
    uicontrol(...
        'Parent',   hdlPnl,...
        'Units',    'normalized', ...
        'Position', [0.5 0.0 0.5 1.0],...
        'Tag',      'tagBtnCancel',...
        'String',   Lang('Annuler','Cancel'), ...
        'Tooltip',  Lang('Annuler','Cancel'), ...
        'CallBack', {@cbBtnCancel, sTableObj});
else
    uicontrol(...
        'Parent',   hdlPnl, ...
        'Units',    'normalized',...
        'Position', [0.4 0.0 0.2 1.0], ...
        'Tag',      'tagBtnClose',...
        'String',   Lang('Fermer','Close'), ...
        'Tooltip',  Lang('Fermer','Close'), ...
        'CallBack', {@cbBtnCancel, sTableObj});
end
flag = 1;
end %fcnInitPnlOKCancelClose


%% Cr�ation des boutons lat�raux de d�placement des items
function flag = fcnInitPnlComments(hdlPnl, str)

% Chaine de test :
% htmlStr = ['<b><div style="font-family:impact;color:green">'...
%            'Matlab</div></b> GUI is <i>' ...
%            '<font color="red">highly</font></i> customizable'];

flag = 0; %#ok<NASGU>
% Merci  Yair Altman pour ces conseils :
% http://undocumentedmatlab.com/blog/rich-matlab-editbox-contents/
hEditbox = uicontrol(...
    'Parent',        hdlPnl,...
    'Units',         'normalized',...
    'Position',      [0.0 0.0 1.0 1.0],...
    'Tag',           'tagComments',...
    'Style',         'edit', ...
    'Max',           4, ...
    'Enable',        'on', ...
    'Interruptible', 'off',...
    'BusyAction',    'queue');
pause(0.1);

[flag, jTable, jScrollPane] = uiTable_fcnGetJTableHandle(handle(hEditbox)); %#ok<ASGLU>
jTable.setEditorKit(javax.swing.text.html.HTMLEditorKit);
jTable.setText(str)
jTable.setEditable(false);


flag = 1;
end


%% Cr�ation des boutons lat�raux de d�placement des items
function flag = fcnInitPnlMoveItem(hdlPnl)

flag         = 0; %#ok<NASGU>

hdlFig       = ancestor(hdlPnl,   'figure', 'toplevel');
strQueryMode = getappdata(hdlFig, 'QueryMode');

switch strQueryMode
    case 'TidyUp'
        flagEnableMoveBtn   = 'on';
        flagEnablePrelist   = 'off';
        flagEnableSelectBtn = 'on';
        
    case {'Delete', 'MultiSelect'}
        flagEnableMoveBtn   = 'off';
        flagEnablePrelist   = 'on';
        flagEnableSelectBtn = 'on';
        
    case 'UniSelect'
        flagEnableMoveBtn   = 'off';
        flagEnablePrelist   = 'off';
        flagEnableSelectBtn = 'off';
        
    case 'Summary'
        flagEnableMoveBtn   = 'off';
        flagEnablePrelist   = 'off';
        flagEnableSelectBtn = 'off';
    otherwise
        str = sprintf('strQueryMode=%s in display_summary/fcnInitPnlMoveItem : code not found.', strQueryMode);
        my_warndlg(str, 1);
end

bgColors = [153/255 153/255 153/255]; %#ok<NASGU>

nomFic          = getNomFicDatabase('MoveTop.tif');
Icon            = fcnIconize(imread(nomFic), 'sizeR', 28, 'sizeC', 38);
Icon(Icon == 255) = .8*255;
uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.895 0.6 0.1],...
    'Tag', 'tagBtnShiftTop',...
    'CData', Icon, ...
    'Enable', flagEnableMoveBtn, ...
    'Tooltip', Lang('S�lectionner l''item � d�placer en t�te de liste','Send selected file to the top of the list'), ...
    'Interruptible', 'off',...
    'BusyAction', 'queue',...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'CallBack', {@cbBtnMoveItem, 'ShiftTop'});

nomFic          = getNomFicDatabase('MoveUp.jpg');
Icon            = fcnIconize(imread(nomFic), 'sizeR', 28, 'sizeC', 38);
Icon(Icon == 255) = .8*255;
h = uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.79 0.6 0.1],...
    'Tag', 'tagBtnShiftUp',...
    'CData', Icon, ...
    'Enable', flagEnableMoveBtn, ...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Tooltip', Lang('Remonter d''un niveau l''item', 'Move selected file 1 place up'), ...
    'Interruptible', 'off',...
    'BusyAction', 'queue',...
    'CallBack', {@cbBtnMoveItem, 'ShiftUp'}); %#ok<NASGU>

nomFic          = getNomFicDatabase('MoveDown.jpg');
Icon            = fcnIconize(imread(nomFic), 'sizeR', 28, 'sizeC', 38);
Icon(Icon == 255) = .8*255;
uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.685 0.6 0.1],...
    'Tag', 'tagBtnShiftDown',...
    'CData', Icon, ...
    'Enable', flagEnableMoveBtn, ...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Tooltip', Lang('Descendre d''un niveau l''item','Move selected file 1 place down'), ...
    'Interruptible', 'off',...
    'BusyAction', 'queue',...
    'CallBack', {@cbBtnMoveItem, 'ShiftDown'});

nomFic          = getNomFicDatabase('MoveBottom.tif');
Icon            = fcnIconize(imread(nomFic), 'sizeR', 28, 'sizeC', 38);
Icon(Icon == 255) = .8*255;
uicontrol(...
    'Parent' ,hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.58 0.6 0.1],...
    'Tag', 'tagBtnShiftBottom',...
    'CData', Icon, ...
    'Enable', flagEnableMoveBtn, ...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Tooltip', Lang('Descendre les items en bas de liste','Send selected file to the bottom of the list'), ...
    'Interruptible', 'off',...
    'BusyAction', 'queue',...
    'CallBack', {@cbBtnMoveItem, 'ShiftBottom'});


% Gestion du panier.
nomFic          = getNomFicDatabase('Basket.png');
Icon            = fcnIconize(imread(nomFic), 'sizeR', 28, 'sizeC', 38);
Icon(Icon == 255) = .8*255;
uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.45 0.6 0.10],...
    'Tag', 'tagBtnBasket',...
    'CData', Icon, ...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Enable', flagEnablePrelist, ...
    'FontWeight', 'bold', ...
    'Tooltip', Lang('Cliquer pour ajouter � la pr�-liste','Click here to add in the prelist'), ...
    'Interruptible', 'off',...
    'BusyAction', 'queue',...
    'CallBack', @cbBtnPrelist);

nomFic          = getNomFicDatabase('ViewBasket.png');
Icon            = fcnIconize(imread(nomFic), 'sizeR', 28, 'sizeC', 38);
Icon(Icon == 255) = .8*255;
uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.34 0.6 0.10],...
    'Tag', 'tagBtnBasket',...
    'CData', Icon, ...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Enable', flagEnablePrelist, ...
    'FontWeight', 'bold', ...
    'Tooltip', Lang('Cliquer pour visualiser pr�-liste','Click here to view the prelist'), ...
    'Interruptible', 'off',...
    'BusyAction', 'queue',...
    'CallBack', @cbBtnViewPrelist);

% Affichage des boutons de (d�)s�lection totale.
if strcmp(flagEnableSelectBtn, 'on')
    FgColor = '#000000';
else
    FgColor = ['#' dec2hex(120) dec2hex(120) dec2hex(120)];
end

str      = Lang('S�lectionner','Select');
labelTop = ['<HTML><center><FONT color="' FgColor '">' str '</Font>'];
str      = Lang('tout','All');
labelBot = ['<HTML><center><FONT color="' FgColor '">' str '</Font>'];


uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.23 0.6 0.10],...
    'Tag', 'tagBtnSelectAll',...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Enable', flagEnableSelectBtn, ...
    'FontSize', 7, ...
    'FontWeight', 'bold', ...
    'String', [labelTop '<br>' labelBot], ...
    'Tooltip', Lang('S�lectionner tous les layers','Select All Layers'), ...
    'CallBack', @cbBtnSelectAll);

str      = Lang('D�s�lectionner','Unselect');
labelTop = ['<HTML><center><FONT color="' FgColor '">' str '</Font>'];
str      = Lang('tout','All');
labelBot = ['<HTML><center><FONT color="' FgColor '">' str '</Font>'];
uicontrol(...
    'Parent' ,hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.12 0.6 0.10],...
    'Tag', 'tagBtnUnselectAll',...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Enable', flagEnableSelectBtn, ...
    'FontSize', 7, ...
    'FontWeight', 'bold', ...
    'String', [labelTop '<br>' labelBot], ...
    'Tooltip', Lang('D�s�lectionner tous les layers','Unselect All Layers'), ...
    'CallBack', @cbBtnUnselectAll);

nomFic = getNomFicDatabase('disquette_CSV.png');
Icon   = fcnIconize(imread(nomFic), 'sizeR', 28, 'sizeC', 38);
Icon(Icon == 255) = .8*255;
uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'Position', [0.2 0.01 0.6 0.10],...
    'BackgroundColor', [153/255 153/255 153/255], ...
    'Tag', 'tagBtnExport',...
    'CData', Icon, ...
    'Tooltip', Lang('Export la liste dans un fichier CSV','Export Liste in CSV File'), ...
    'CallBack', @cbBtnExportTable);
flag = 1;
end % fcnInitPnlMoveItem


% === Fonction =============
% Cr�ation des boutons lat�raux
function flag = fcnInitPnlFilterItem(hdlPnl, str, initGeometryType, initDataType)

flag = 0; %#ok<NASGU>
hdlFig       = ancestor(hdlPnl,   'figure', 'toplevel');
strQueryMode = getappdata(hdlFig, 'QueryMode');

switch strQueryMode
    case 'TidyUp'
        flagEnableFiltre = 'on';
        
    case {'Delete', 'MultiSelect'}
        flagEnableFiltre = 'on';
        
    case 'UniSelect'
        flagEnableFiltre = 'on';
        
    case 'Summary'
        flagEnableFiltre = 'on';
end

% Contr�les pour le filtrage de nom
hBtn(1) = uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'HorizontalAlignment', 'left',...
    'Position', [0.06 0.6 0.10 0.4],...
    'String', Lang('Contenant la(es) chaine(s)','With string(s)'),...
    'FontWeight', 'bold',...
    'Style', 'text',...
    'Tag', 'tagLabelWithString');

hBtn(end+1) = uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'HorizontalAlignment', 'left',...
    'Position', [0.01 0.17 0.15 0.4],...
    'String', '',...
    'FontWeight', 'bold',...
    'BackgroundColor', 'white',...
    'String', '',...
    'Style', 'edit',...
    'Enable', flagEnableFiltre,...
    'Tooltip', Lang('Trouver les layers contenant la(es) chaine(s) associ�es par "|" ou "&". Casse prise en compte.','Find string(s) linked with "|" or "&". Beware of character case.'), ...
    'Tag', 'tagWithString',...
    'CallBack', @cbFilterType);

% Contr�les pour le filtrage de nom ("ne contenant pas")
hBtn(1) = uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'HorizontalAlignment', 'left',...
    'Position', [0.20 0.6 0.12 0.4],...
    'String', Lang('Sans la(es) chaine(s)','Without string(s)'),...
    'FontWeight', 'bold',...
    'Style', 'text',...
    'Tag', 'tagLabelWithoutString');

hBtn(end+1) = uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'HorizontalAlignment', 'left',...
    'Position', [0.17 0.17 0.15 0.4],...
    'String', '',...
    'FontWeight', 'bold',...
    'BackgroundColor', 'white',...
    'String', '',...
    'Style', 'edit',...
    'Enable', flagEnableFiltre,...
    'Tooltip', Lang('Trouver les layers ne contenant pas la(es) chaine(s) associ�es par "|" ou "&". Casse prise en compte.', 'Find layers not containing string(s) -with "|" or "&". Beware of character case.'), ...
    'Tag', 'tagWithoutString',...
    'CallBack', @cbFilterType);

% Contr�les pour le filtrage GeometryType
hBtn(end+1) = uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'HorizontalAlignment', 'left',...
    'Position', [0.42 0.6 0.095 0.4],...
    'String', Lang('Type de g�om�trie','Geometry Type'),...
    'FontWeight', 'bold',...
    'Style', 'text',...
    'Tag', 'tagLabelGeometryType');

[C, ia, ib]     = intersect(str(1,:), 'GeometryType'); %#ok<ASGLU>
strGeometryType = unique(str(2:end,ia));
strGeometryType = ['Unknown';strGeometryType];
if isempty(ia)
    % Pas de colonne Geometry Type dans str
    initGeometryType = 1;
end
hBtn(end+1) = uicontrol(...
    'Parent', hdlPnl,...
    'Units', 'normalized',...
    'HorizontalAlignment', 'left',...
    'Position', [0.37 0.2 0.2 0.4],...
    'Enable' , flagEnableFiltre, ...
    'String', strGeometryType,...
    'Value', initGeometryType, ...
    'Style', 'popupmenu',...
    'Tooltip', 'Filter layers from Geometry Types', ...
    'Tag', 'tagGeometryTypeFilter', ...
    'CallBack', @cbFilterType);


% Contr�les pour le filtrage de DataType
hBtn(end+1) = uicontrol(...
    'Parent',     hdlPnl,...
    'Units',      'normalized',...
    'HorizontalAlignment', 'left',...
    'Position',   [0.64 0.6 0.2 0.4],...
    'String',     Lang('Type de donn�e','Data Type'),...
    'FontWeight', 'bold',...
    'Style',      'text',...
    'Tag',        'tagLabelDataType');

[C, ia, ib] = intersect(str(1,:), 'DataType'); %#ok<ASGLU>
strDataType = unique(str(2:end,ia));
strDataType = ['Unknown'; strDataType];
if isempty(ia)
    % Pas de colonne Geometry Type dans str
    initDataType = 1;
end
hBtn(end+1) = uicontrol(...
    'Parent',   hdlPnl,...
    'Units',    'normalized',...
    'Position', [0.59 0.2 0.2 0.4],...
    'Enable',   flagEnableFiltre, ...
    'String',   strDataType,...
    'Value',    initDataType, ...
    'Style',    'popupmenu',...
    'Tooltip',  'Filter layers from Data Types', ...
    'Tag',      'tagDataTypeFilter', ...
    'CallBack', @cbFilterType);

% Contr�les pour le filtrage de DataType
hBtn(end+1) = uicontrol(...
    'Parent',     hdlPnl,...
    'Units',      'normalized',...
    'HorizontalAlignment', 'left',...
    'Position',   [0.86 0.6 0.10 0.4],...
    'String',     Lang('Tri des donn�e','Sort By'),...
    'FontWeight', 'bold',...
    'Style',      'text',...
    'Tag',        'tagLabelSortField');

strSortField = get(findobj(hdlFig, 'tag', 'tagTableLayers'), 'ColumnName');
strSortField = [' ';strSortField];

hBtn(end+1) = uicontrol(...
    'Parent',   hdlPnl,...
    'Units',    'normalized',...
    'Position', [0.84 0.2 0.10 0.4],...
    'String',   strSortField,...
    'Enable',   'on', ...
    'Value',    1, ...
    'Style',    'popupmenu',...
    'Tooltip',  'Sort layers from Data Types', ...
    'Tag',      'tagSortDataField', ...
    'CallBack', @cbListSortColumn);

% Assignation des modes de tri par d�faut  (croissant).
N = numel(strSortField);
modeSortField = ones(1,N);
setappdata(hBtn(end), 'ModeSortField', modeSortField);

nomFic          = getNomFicDatabase('SortAscend.png');
Icon            = fcnIconize(imread(nomFic));
Icon(Icon == 255) = .8*255;
hBtn(end+1)     = uicontrol(...
    'Parent',   hdlPnl,...
    'Units',    'normalized',...
    'Position', [0.94 0.10 0.03 0.55],...
    'Enable',   'on', ...
    'Tag',      'tagBtnAscendSort',...
    'CData',    Icon, ...
    'Tooltip',  Lang('Tri croissant','Ascend sort'), ...
    'CallBack', @cbBtnSortColumn);

nomFic          = getNomFicDatabase('SortDescend.png');
Icon            = fcnIconize(imread(nomFic));
Icon(Icon == 255) = .8*255;
hBtn(end+1)     = uicontrol(...
    'Parent',   hdlPnl,...
    'Units',    'normalized',...
    'Position', [0.97 0.10 0.03 0.55],...
    'Enable',   'on', ...
    'Tag',      'tagBtnDescendSort',...
    'CData',    Icon, ...
    'Tooltip',  Lang('Tri d�croissant','Descend sort'), ...
    'CallBack', @cbBtnSortColumn);  %#ok<NASGU>
flag = 1;
end % fcnInitPnlFilterItem


% = Callback ===============
% Callback de R�ordonnancement selon le champ s�lectionn�
function cbListSortColumn(hObject, event)  %#ok<INUSD>

hdlFig               = ancestor(hObject, 'figure');
hdlTableLayers       = findobj(hdlFig, 'tag', 'tagTableLayers');
listLayersBeforeSort = get(hdlTableLayers, 'Data');

callFromCallback = 1; % Pour forcer la mise � jour des attributs de tri.
[flag, modeSortField, idCol, listLayers] = fcnSortColumn(hdlFig, hdlTableLayers, callFromCallback, listLayersBeforeSort);
if ~flag
    return
end

% Mise en surbrillance du bouton de tri croissant selon le mode de tri.
hAscendBtnSort        = findobj(hdlFig, 'tag', 'tagBtnAscendSort');
hDescendBtnSort       = findobj(hdlFig, 'tag', 'tagBtnDescendSort');
% Tri croissant
if modeSortField(idCol)
    set(hAscendBtnSort, 'Backgroundcolor', [0.85 0.85 0.85]);
    % Mise en retrait du bouton de tri d�croissant
    set(hDescendBtnSort, 'Backgroundcolor', [1.0 1.0 1.0]);
    % Tri d�croissant
elseif ~modeSortField(idCol)
    set(hAscendBtnSort, 'Backgroundcolor', [1.0 1.0 1.0]);
    % Mise en retrait du bouton de tri d�croissant
    set(hDescendBtnSort, 'Backgroundcolor', [0.85 0.85 0.85]);
else
    my_warndlg(Lang('Erreur d''identification du mode de tri', 'Error in identification sort mode'), 1);
    return
end

% Assignation des indicateurs de tri croissants ou non et des datas �
% la table.

hSortDataField  = findobj(hdlFig, 'tag', 'tagSortDataField');
setappdata(hSortDataField, 'ModeSortField', modeSortField);
set(hdlTableLayers, 'Data', listLayers);

% Effacement des indices s�lectionn�s si ils ne sont plus dans la liste
% courante.
selectIdx   = getappdata(hdlFig, 'IndexLayersOutput');
pppp        = listLayersBeforeSort(selectIdx);
[C, ia, ib] = intersect(listLayers(:,1), pppp, 'stable'); %#ok<ASGLU>
if isempty(ia)
    setappdata(hdlFig, 'IndexLayersOutput', []);
else
    setappdata(hdlFig, 'IndexLayersOutput', ia);
end

% Mise en surlignage des layers d�j� s�lectionn�s.
ModePrelist = 0;
flag = fcnHighlightSelectLayers(ModePrelist, hdlFig, hdlTableLayers, listLayers); %#ok<NASGU>
end % cbListSortColumn


% = Callback ===============
% Callback de r�ordonnancement croissant des champs
function cbBtnSortColumn(hObject, event)  %#ok<INUSD>

hdlFig           = ancestor(hObject, 'figure');
hAscendBtnSort   = findobj(hdlFig, 'tag', 'tagBtnAscendSort');
hDescendBtnSort  = findobj(hdlFig, 'tag', 'tagBtnDescendSort');
hSortDataField   = findobj(hdlFig, 'tag', 'tagSortDataField');
hdlTableLayers   = findobj(hdlFig, 'tag', 'tagTableLayers');
listLayers       = get(hdlTableLayers, 'Data');
callFromCallback = 1; % Pour forcer la mise � jour des attributs de tri.

listLayersBeforeFilter      = get(hdlTableLayers, 'Data');

[flag, modeSortField, idCol, listLayers] = fcnSortColumn(hdlFig, hdlTableLayers, callFromCallback, listLayers);
if ~flag || isempty(idCol)
    return
end

% Pas de mise � jour si nous sommes d�j� dans le mode concern�.
% Le flag a �t� invers� dans la fonction fcnSortColumn appel�e pr�c�demment.
if (hObject == hAscendBtnSort) && ~modeSortField(idCol)
    return
elseif (hObject == hDescendBtnSort) && modeSortField(idCol)
    return
end

% Mise en surbrillance du bouton de tri croissant
if hObject == hAscendBtnSort
    set(hAscendBtnSort, 'Backgroundcolor', [0.85 0.85 0.85]);
    % Mise en retrait du bouton de tri d�croissant
    set(hDescendBtnSort, 'Backgroundcolor', [1.0 1.0 1.0]);
elseif hObject ==hDescendBtnSort
    set(hAscendBtnSort, 'Backgroundcolor', [1.0 1.0 1.0]);
    % Mise en retrait du bouton de tri d�croissant
    set(hDescendBtnSort, 'Backgroundcolor', [0.85 0.85 0.85]);
else
    my_warndlg(Lang('Erreur dans l''IHM Summary', 'Error in Summary GUI'), 1);
    return
end
% Assignation des indicateurs de tri croissants ou non et des datas �
% la table.

setappdata(hSortDataField, 'ModeSortField', modeSortField);
set(hdlTableLayers, 'Data', listLayers);

% Effacement des indices s�lectionn�s si ils ne sont plus dans la liste
% courante.
selectIdx   = getappdata(hdlFig, 'IndexLayersOutput');
pppp        = listLayersBeforeFilter(selectIdx);
[C, ia, ib] = intersect(listLayers(:,1), pppp, 'stable'); %#ok<ASGLU>
if isempty(ia)
    setappdata(hdlFig, 'IndexLayersOutput', []);
else
    setappdata(hdlFig, 'IndexLayersOutput', ia);
end

% Mise en surlignage des layers d�j� s�lectionn�s.
ModePrelist = 0;
flag = fcnHighlightSelectLayers(ModePrelist, hdlFig, hdlTableLayers, listLayers); %#ok<NASGU>
end % cbBtnSortColumn


% = Callback ===============
% Filtrage des items.
function cbFilterType(hObject, event) %#ok<INUSD>

% Identification de la fenetre.
hdlFig = ancestor(hObject, 'figure', 'toplevel');

% hObject d�signe l'item (GeometryType, Contenant du nom ...) de filtrage.
hdlTableLayers         = findobj(hdlFig, 'Tag', 'tagTableLayers');
listLayersBeforeFilter = get(hdlTableLayers, 'Data');

% Filtrage des layers.
[flag, listLayers] = fcnFilterLayers(hdlFig); %#ok<ASGLU>
set(hdlTableLayers,'Data', listLayers);

% Effacement des indices s�lectionn�s si ils ne sont plus dans la liste
% courante.
selectIdx   = getappdata(hdlFig, 'IndexLayersOutput');
pppp        = listLayersBeforeFilter(selectIdx);
[C, ia, ib] = intersect(listLayers(:,1), pppp, 'stable'); %#ok<ASGLU>
if isempty(ia)
    setappdata(hdlFig, 'IndexLayersOutput', []);
else
    setappdata(hdlFig, 'IndexLayersOutput', ia);
end

% Mise en surlignage des layers d�j� s�lectionn�s.
ModePrelist = 0;
flag = fcnHighlightSelectLayers(ModePrelist, hdlFig, hdlTableLayers, listLayers); %#ok<NASGU>
end %cbFilterType


% = Callback ===============
% Gestion des boutons de d�placement d'items.
function cbBtnExportTable(hObject, event) %#ok<INUSD>

hdlFig         = ancestor(hObject, 'figure', 'toplevel');
hdlTableLayers = findobj(hdlFig, 'Tag', 'tagTableLayers');
ListLayers     = get(hdlTableLayers,'Data');
Filtre = fullfile(my_tempdir, 'SummaryImages.csv');
[flag, nomFic] = my_uiputfile('*.csv', 'Give a file Title', Filtre);
if ~flag
    return
end
flag = my_csvwrite(nomFic, ListLayers);
if flag
    winopen(nomFic)
end
end % cbBtnExport

% = Callback ===============
% Gestion des boutons de d�placement d'items.
function cbBtnMoveItem(hObject, event, keyWord) %#ok<INUSL>

hdlFig         = ancestor(hObject, 'figure','toplevel');
hdlTableLayers = findobj(hdlFig, 'Tag', 'tagTableLayers');
ListLayers     = get(hdlTableLayers,'Data');
indices        = get(hdlTableLayers,'UserData');
if isempty(indices)
    str1 = 'S�lectionner un item avant de le d�placer';
    str2 = 'Please, select an item before move it.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
ToShift      = indices(:,1);
LengthLayers = size(ListLayers,1);
Ordre        = 1:LengthLayers;
IdxFocus     = [];

% Pour �tre assur� que l'utilisateur n'appuie pas trop rapidement sur le
% bouton qu'il vient d'activer. Cela aurait pour effet de d�caler l'item
% s�lectionn�.
set(hObject, 'Enable', 'off');

if LengthLayers > 1
    switch keyWord
        case 'ShiftUp'
            % On ne d�place rien si on a s�lectionn� les extr�mes.
            if ~isequal(ToShift',1:numel(ToShift))
                for i=1:length(ToShift)
                    if ToShift(i) > 1 % Elude les cas limites Min/Max
                        sub = [Ordre(ToShift(i)-1) Ordre(ToShift(i))];
                        Ordre((ToShift(i)-1):ToShift(i)) = fliplr(sub);
                    end
                end
                IdxFocus = ToShift' - 1;
                % Gestion de la s�lection du 1er �l�ment qui conserve son
                % n� de Focus.
                sub = IdxFocus(IdxFocus > 1);
                if IdxFocus(1) == 0
                    IdxFocus = [1;sub];
                end
            else
                IdxFocus = ToShift';
            end
            
        case 'ShiftDown'
            % On ne d�place rien si on a s�lectionn� les extr�mes.
            if ~isequal(ToShift',numel(Ordre)-numel(ToShift)+1:numel(Ordre))
                for i=length(ToShift):-1:1
                    if ToShift(i) < LengthLayers % Elude les cas limites Min/Max
                        sub = [Ordre(ToShift(i)) Ordre(ToShift(i)+1)];
                        Ordre(ToShift(i):(ToShift(i)+1)) = fliplr(sub);
                    end
                end
                IdxFocus = ToShift'+1;
                % Gestion de la s�lection du dernier �l�ment qui conserve son
                % n� de Focus.
                sub = IdxFocus(IdxFocus < numel(Ordre));
                if IdxFocus(end) == numel(Ordre) + 1
                    IdxFocus = [sub numel(Ordre)];
                end
            else
                IdxFocus = ToShift';
            end
            
        case 'ShiftTop'
            % On ne d�place rien si on a s�lectionn� les extr�mes.
            if ~isequal(ToShift',1:numel(ToShift))
                if ~isempty(ToShift)
                    Ordre       = [ToShift' setdiff(1:LengthLayers, ToShift')];
                    IdxFocus    = 1:numel(ToShift);
                end
                % Gestion de la s�lection du 1er �l�ment qui conserve son
                % n� de Focus.
                sub = IdxFocus(IdxFocus > 1);
                if IdxFocus(1) == 0
                    IdxFocus = [1;sub];
                end
            else
                IdxFocus = ToShift';
            end
            
        case 'ShiftBottom'
            % On ne d�place rien si on a s�lectionn� les extr�mes.
            if ~isequal(ToShift',numel(Ordre)-numel(ToShift)+1:numel(Ordre))
                if ~isempty(ToShift)
                    Ordre       = [setdiff(1:LengthLayers, ToShift') ToShift'];
                    IdxFocus    = sort((numel(Ordre) - (1:numel(ToShift)))+1);
                end
                % Gestion de la s�lection du dernier �l�ment qui conserve son
                % n� de Focus.
                sub = IdxFocus(IdxFocus < numel(Ordre));
                if IdxFocus(end) == numel(Ordre) + 1
                    IdxFocus = [sub;numel(Ordre)];
                end
            else
                IdxFocus = ToShift';
            end
    end
    ListLayers = ListLayers(Ordre,:);
    set(hdlTableLayers, 'Data', ListLayers);
    
    % Surlignage des lignes s�lectionn�es.
    % Focus sur la s�lection
    idxFieldImageName = getappdata(hdlTableLayers,'idxFieldImageName');
    flag = uiTable_fcnSetFocusOnIndices(hdlTableLayers, IdxFocus, idxFieldImageName); %#ok<NASGU>
    
    % Mise � jour graphique avant prise en compte du Focus.
    drawnow expose update;
    
    % R�activation du bouton.
    set(hObject, 'Enable', 'on');
end
end %cbBtnMoveItem


% = Callback ===============
function cbBtnSelectAll(hObject, event) %#ok<INUSD>
% S�lection de tous les items de la liste courante.
hdlFig         = ancestor(hObject, 'figure','toplevel');
hdlTableLayers = findobj(hdlFig, 'Tag', 'tagTableLayers');
ListLayers     = get(hdlTableLayers,'Data');

idx                = 1:numel(ListLayers(:,1));
% Positionne le focus sur les indices s�lectionn�s.
idxFieldImageName = getappdata(hdlTableLayers,'idxFieldImageName');
flag              = uiTable_fcnSetFocusOnIndices(hdlTableLayers, idx, idxFieldImageName);
if ~flag
    return
end
setappdata(hdlFig, 'IndexLayersOutput', idx);
end % cbBtnSelectAll


% = Callback ===============
function cbBtnUnselectAll(hObject, event) %#ok<INUSD>

hdlFig         = ancestor(hObject, 'figure','toplevel');
hdlTableLayers = findobj(hdlFig, 'Tag', 'tagTableLayers');

flag = uiTable_fcnClearSelection(hdlTableLayers); %#ok<NASGU>
setappdata(hdlFig, 'IndexLayersOutput', []);
end


% = Callback ===============
function cbBtnOK(hObject, event, sTableObj)

hdlFig         = ancestor(hObject,   'figure', 'toplevel');
hdlTableLayers = findobj(hdlFig,     'Tag',    'tagTableLayers');
strQueryMode   = getappdata(hdlFig,  'QueryMode');
listLayers     = get(hdlTableLayers, 'Data');

% On ne sort pas si on n'a pas s�lectionn� de layers dans les modes Delete,
% Select, ...
switch strQueryMode
    case 'UniSelect'
        idx = getappdata(hdlFig,'IndexLayersOutput');
        if numel(idx) > 1
            str1 = 'SVP, veuillez s�lectionner un seul layer.';
            str2 = 'Please, select only one layer.';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
end

% Stimulation de la Callback de fermeture
setappdata(hdlFig, 'flagOKCancel', 1);
cbCloseFcn(hdlFig, event, listLayers, sTableObj);

end %cbBtnOK


% = Callback ===============
function cbBtnCancel(hObject, event, sTableObj)

hdlFig = ancestor(hObject, 'figure','toplevel');
setappdata(hdlFig, 'flagOKCancel', 0);

% Stimulation de la Callback de fermeture
listLayers = [];
cbCloseFcn(hdlFig, event, listLayers, sTableObj);

end %cbBtnCancel


% = Callback ===============
function cbCloseFcn(hObject, event, ListLayers, sTableObj) %#ok<INUSL>

% hdlFig = ancestor(hObject, 'figure','toplevel');
% hTable = findobj(hdlFig, 'tag', 'tagTableLayers');
% R�cup�ration du composant Java de uiTable.
% [flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTable); %#ok<ASGLU>
% if ~flag
%     return
% end
try
    jTable = sTableObj.jTable;
catch
    return
end
% Sauvegarde dans le fichier XML de la customisation de la table.
jTableModel              = jTable.getColumnModel();
colNb                    = get(jTableModel, 'ColumnCount');
InfoColumn.Title         = Lang('Structuration de la table de R�sum�', 'Summary customisation');
InfoColumn.FormatVersion = '2013-07-06';

for k=1:colNb
    InfoColumn.Column(k).Width = jTableModel.getColumn(k-1).getWidth;
    InfoColumn.Column(k).Label = get(jTableModel.getColumn(k-1),'HeaderValue');
end

% nomFicMat = fullfile(my_tempdir, 'my_CustomTableSummary.mat');
% save(nomFicMat, 'InfoColumn');

% disp('Close request function...');
setappdata(hObject, 'LayersOutput', ListLayers);
uiresume(hObject);
end % cbCloseFcn


% = Callback ===============
function cbBtnPrelist(hObject, event)  %#ok<INUSD>

hdlFig         = ancestor(hObject,             'figure','toplevel');
hdlTableLayers = findobj(hdlFig,               'Tag', 'tagTableLayers');
PrelistLayers  = getappdata(hdlTableLayers,    'PrelistLayers');
% Ajout des indices s�lectionn�s si ils ne sont plus dans la liste
% du panier.
selectIdx    = getappdata(hdlFig, 'IndexLayersOutput');
listLayers   = get(hdlTableLayers, 'Data');
pppp         = listLayers(selectIdx);
[C1, ia, ib] = intersect(listLayers(:,1), pppp, 'stable'); %#ok<ASGLU>

if isempty(PrelistLayers)
    PrelistLayers = C1;
else
    [C2, ia]   = intersect(PrelistLayers, C1, 'stable');
    % Si pas d'intersections entre la liste courante et les nx items
    % s�lectionn�s.
    if isempty(ia)
        PrelistLayers = [PrelistLayers;C2];
    else
        % On ne retient que les layers qui ne sont pas d�j� dans la liste.
        for k=1:numel(C1)
            if ~ismember(C1(k), PrelistLayers)
                PrelistLayers = [PrelistLayers; C1(k)]; %#ok<AGROW>
            end
        end
    end
end
setappdata(hdlTableLayers, 'PrelistLayers', PrelistLayers);
end %cbBtnPrelist


% = Callback ===============
function cbBtnViewPrelist(hObject, event)  %#ok<INUSD>

hdlFig         = ancestor(hObject,          'figure', 'toplevel');
hdlTableLayers = findobj(hdlFig,            'Tag',    'tagTableLayers');
PrelistLayers  = getappdata(hdlTableLayers, 'PrelistLayers');

% Mise en surlignage des layers d�j� s�lectionn�s.
ModePrelist = 1;
flag        = fcnHighlightSelectLayers(ModePrelist, hdlFig, hdlTableLayers, PrelistLayers); %#ok<NASGU>
end %cbBtnViewPrelist


% = Fonction ===============
% Calcul du layer s�lectionn�.
function cbTabSelect(hObject, event)
if ~isempty(event)
    indices = event.Indices;
    if ~isempty(indices)
        hdlFig  = ancestor(hObject, 'figure');
        setappdata(hdlFig,'IndexLayersOutput',indices(:,1));
        set(hObject,'UserData',event.Indices);
    end
    drawnow update expose;
end
end %cbTabSelect


% = Fonction ===============
% Fonction de r�cup�ration des layers selon les items de filtres.
function [flag, ListLayers] = fcnFilterLayers(hdlFig)

flag = 0; %#ok<NASGU>

hdlTableLayers = findobj(hdlFig, 'Tag', 'tagTableLayers');
ListLayers     = getappdata(hdlTableLayers, 'OrigLayers');
strQueryMode   = getappdata(hdlFig, 'QueryMode');
currentLayers  = get(hdlTableLayers, 'Data');

% Identification des contr�les de filtrage.
hWithStringFilter        = findobj(hdlFig, 'Tag', 'tagWithString');
withStringInContentLayer = get(hWithStringFilter, 'String');

hWithoutStringFilter        = findobj(hdlFig, 'Tag', 'tagWithoutString');
withoutStringInContentLayer = get(hWithoutStringFilter, 'String');

GeometryTypeFilter = findobj(hdlFig, 'Tag', 'tagGeometryTypeFilter');
strGeometryType    = get(GeometryTypeFilter, 'String');
GeometryType       = get(GeometryTypeFilter, 'Value');

DataTypeFilter = findobj(hdlFig, 'Tag', 'tagDataTypeFilter');
strDataType    = get(DataTypeFilter, 'String');
DataType       = get(DataTypeFilter, 'Value');

% On ne fait pas le traitement de filtres si tout est neutre.
if ~isempty(withStringInContentLayer) || ~isempty(withoutStringInContentLayer) || ...
        ~strcmp(strGeometryType(GeometryType), 'Unknown') || ...
        ~strcmp(strDataType(DataType), 'Unknown')
    
    flagFilterControl = true;
    flagTidyUpControl = false;
    
    [C, ia, ib] = intersect(currentLayers(:,1), ListLayers(:,1), 'stable'); %#ok<ASGLU>
    % Pour le r�ordonnancement avant une nouvelle mise � jour.
    % Traitement du cas de figure : filtre puis ordonnancement puis non-filtre.
    if any(ia ~= ib)
        sub        = ones(size(ListLayers,1),1);
        sub(ib)    = 0;
        ListLayers = [ListLayers(ib,:);ListLayers((sub==1),:)];
    end
    
    % Filtrage sur le contenu de la chaine : �clatement des cha�nes �
    % rechercher (s�par�es par le d�limitateur saisi par l'op�rateur).
    if ~isempty(withStringInContentLayer)
        [flag, resultOK] = fcnFindOccursInListLayers(ListLayers, withStringInContentLayer);
        if ~flag
            return
        end
        ListLayers = ListLayers(resultOK == 1,:);
    end
    
    % Filtrage sur le contenu en exclusion de la chaine.
    if ~isempty(withoutStringInContentLayer)
        [flag, resultOK] = fcnFindOccursInListLayers(ListLayers, withoutStringInContentLayer);
        if ~flag
            return
        end
        resultOK   = ~resultOK;
        ListLayers = ListLayers(resultOK == 1,:);
    end
    
    % Filtrage sur crit�re GeometryType
    ColumnName = get(findobj(hdlFig, 'Tag', 'tagTableLayers'), 'ColumnName');
    idCol      = strcmp(ColumnName, 'GeometryType');
    [idCol, ~] = find(idCol == 1);
    if GeometryType ~= 1
        % Recalcul en cas de filtrage par le champ pr�c�dent.
        N  = size(ListLayers,1);
        OK = zeros(N,1);
        for k=1:N
            if strcmp(ListLayers{k,idCol}, strGeometryType{GeometryType})
                OK(k) = 1;
            end
        end
        ListLayers = ListLayers(OK == 1,:);
    end
    
    % Filtrage sur crit�re DataType
    idCol      = strcmp(ColumnName, 'DataType');
    [idCol, ~] = find(idCol == 1);
    if (DataType ~= 1)
        % Recalcul en cas de filtrage par le champ pr�c�dent.
        N  = size(ListLayers,1);
        OK = zeros(N,1);
        for k=1:N
            if strcmp(ListLayers{k,idCol}, strDataType{DataType})
                OK(k) = 1;
            end
        end
        ListLayers = ListLayers(OK == 1,:);
    end
else
    flagFilterControl = true;
    flagTidyUpControl = true;
end

% Gestion de l'�tat Enable des contr�les Filter and TidyUp.
if strcmp(strQueryMode, 'TidyUp')
    flag = fcnUpdateTidyAndFilterControl(hdlFig, flagTidyUpControl, flagFilterControl); %#ok<NASGU>
end

% Tri final sur les donn�es
callFromCallback = 0; % Pour ne pas forcer la mise � jour des attributs de tri.
[flag, modeSortField, idCol, ListLayers] = fcnSortColumn(hdlFig, hdlTableLayers, callFromCallback, ListLayers); %#ok<ASGLU>

setappdata(hdlFig, 'LayersOutput', ListLayers);
flag = 1;
end % fcnFilterLayers


% = Fonction ===============
function flag = fcnHighlightSelectLayers(modePrelist, hFig, hTable, ListLayers)

flag = 0;

% Conservation des indices avant filtrage.
currentLayers = get(hTable, 'Data');
if modePrelist
    preListLayers  = getappdata(hTable, 'PrelistLayers');
    % Correspondance pour recherche des indices entre la liste courante et le panier.
    if ~isempty(currentLayers)
        [C, ia, ib] = intersect(currentLayers(:,1), preListLayers, 'stable'); %#ok<ASGLU>
    end
else
    selectIdx = getappdata(hFig, 'IndexLayersOutput');
    if ~isempty(currentLayers) && ~isempty(selectIdx)
        currentLayers = currentLayers(:,1);
        selectLayers  = currentLayers(selectIdx);
        % Recherche du nouvel index des layers d�j� s�lectionn�s.
        [C, ia, ib] = intersect(ListLayers(:,1), selectLayers, 'stable'); %#ok<ASGLU>
    else
        return
    end
end

if ~isempty(ia)
    ia = sort(ia, 'ascend');
    
    % Positionne le focus sur les indices s�lectionn�s.
    idxFieldImageName = getappdata(hTable, 'idxFieldImageName');
    flag = uiTable_fcnSetFocusOnIndices(hTable, ia, idxFieldImageName);
    if ~flag
        return
    end
end
flag = 1;
end % fcnHighlightSelectLayers


% = Fonction ===============
% R�ordonnancement des champs
function [flag, modeSortField, idxCol, ListLayers] = fcnSortColumn(hdlFig, hdlTableLayers, callFromGUI, ListLayers)

flag          = 0;
modeSortField = [];

% ---------------------------------
% Filtrage sur crit�re DataType
strSortField = get(hdlTableLayers, 'ColumnName');

DataFieldSort = findobj(hdlFig, 'Tag', 'tagSortDataField');
strFieldData  = get(DataFieldSort, 'String');
if strcmp(strFieldData, ' ')
    return
end
DataField   = get(DataFieldSort, 'Value');
idxCol      = strcmp(strSortField, strFieldData{DataField});
[idxCol, ~] = find(idxCol == 1);
if isempty(idxCol)
    return
end

% R�cup�ration des indcateurs de tri croissants ou non.
hSortDataField = findobj(hdlFig, 'tag', 'tagSortDataField');
modeSortField  = getappdata(hSortDataField, 'ModeSortField');
if callFromGUI
    modeSortField(idxCol) = ~modeSortField(idxCol);
end

if modeSortField(idxCol)
    ListLayers = sortrows(ListLayers, idxCol);
else
    ListLayers = sortrows(ListLayers, -idxCol);
end

flag = 1;
end % fcnSortColumn


% = Fonction ===============
% Gestion des �tats de contr�les de filtrage;
function flag = fcnUpdateTidyAndFilterControl(hFig, flagTidyUp, flagFilter)  %#ok<INUSD>

flag = 0; %#ok<NASGU>
hdlObj(1)     = findobj(hFig, 'Tag', 'tagBtnShiftUp');
hdlObj(end+1) = findobj(hFig, 'Tag', 'tagBtnShiftDown');
hdlObj(end+1) = findobj(hFig, 'Tag', 'tagBtnShiftTop');
hdlObj(end+1) = findobj(hFig, 'Tag', 'tagBtnShiftBottom');
for k=1:numel(hdlObj)
    if flagTidyUp
        set(hdlObj(k), 'Enable', 'on');
    else
        set(hdlObj(k), 'Enable', 'off');
    end
end
flag = 1;
end % fcnUpdateTidyAndFilterControl


% = Fonction ===============
% Fonction iconize pour int�grer un fichier type jpg en icone.
% Inspir�e de la fonction iconize de iofun.
function outIcon = fcnIconize(a, varargin)

[varargin, sizeR] = getPropertyValue(varargin, 'sizeR', 18);
[varargin, sizeC] = getPropertyValue(varargin, 'sizeC', 18); %#ok<ASGLU>

% Find the size of the acquired image and determine how much data will need
% to be lost in order to form a 18x18 icon
[r,c,d] = size(a); %#ok
r_skip = ceil(r/sizeR);
c_skip = ceil(c/sizeC);

% Create the 18x18 icon (RGB data)
outIcon = a(1:r_skip:end,1:c_skip:end,:);
if ismatrix(outIcon)
    outIcon(:,:,2) = outIcon(:,:,1);
    outIcon(:,:,3) = outIcon(:,:,1);
end
end % fcnIconize


% = Fonction ===============
% Fonction de recherche d'une liste d'occurrences dans les noms des layers.
function [flag, resultOK] = fcnFindOccursInListLayers(ListNameLayers, strPattern)
flag        = 0; %#ok<NASGU>

% Recherche possible avec op�rateur ET (|) / AND (&).
delimiter   = regexp(strPattern, '&', 'once');
if isempty(delimiter)
    flagAnd   = 0;
    delimiter = '\|';
else
    flagAnd   = 1;
    delimiter = '&';
end
% Suppression des blancs avant �clatement des �l�ments
strPattern = regexprep(strPattern,[' ' delimiter ' '],delimiter);
if ~isempty(strPattern)
    splitStr = regexp(strPattern,['' delimiter],'split');
    N        = size(ListNameLayers,1);
    resultOK = zeros(N,1);
    for k=1:N
        % Recherche des diff�rentes cha�nes dans les noms des layers.
        % Mot-cl� once obligatoire pour ne rechercher qu'une des
        % occurrences possibles d'un des pattern.
        resultSearch   = regexp(ListNameLayers(k,1), splitStr, 'once');
        nbResultSearch = length(cell2mat(resultSearch));
        if flagAnd == 1
            if nbResultSearch == numel(splitStr)
                resultSearch = 1;
            else
                resultSearch = 0;
            end
        else
            if nbResultSearch >= 1
                resultSearch = 1;
            else
                resultSearch = 0;
            end
        end
        if resultSearch
            resultOK(k) = 1;
        end
    end
end
flag = 1;
end % fcnFindOccursInListLayers
