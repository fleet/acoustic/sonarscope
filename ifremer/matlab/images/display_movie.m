function display_movie(nomFicMovie, varargin)

if isdeployed
    winopen(nomFicMovie)
else
    my_implay(nomFicMovie, varargin{:})
end
