% Reduction d'une image
%
% Syntax
%   I = rand(256);
%   J = downsize(I, ...)
%
% Input Arguments
%   I     : Image de depart
%
% Name-Value Pair Arguments
%   pas_y : facteur de reduction en ligne
%   pas_x : facteur de reduction en colonne
%   Method : {'nearest'} | 'mean' | 'sum'
%
% Output Arguments
%   J : Image d'arrivee
%
% Examples
%   I = Lena;
%   figure; imagesc(I); colorbar
%
%   J = downsize(I, 'pasy', 1, 'pasx', 4);
%   figure; imagesc(J); colorbar
%
%   J = downsize(I, 'pasy', 4, 'pasx', 4, 'Method', 'mean');
%   figure; imagesc(J); colorbar
%
%   J = downsize(I, 'pasy', 4, 'pasx', 4, 'Method', 'sum');
%   figure; imagesc(J); colorbar
%
% Remarks : Si Method='mean' le coefficient de r�duction doit �tre entier et la taille de l'image un multiple
%
% See also stretchmat Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function I = downsize(I, varargin)

if ~(isa(I,'double') || isa(I,'single'))
    I = single(I);
end

[varargin, pas_x]  = getPropertyValue(varargin, 'pasx',   []);
[varargin, pas_y]  = getPropertyValue(varargin, 'pasy',   []);
[varargin, Nx]     = getPropertyValue(varargin, 'Nx',     []);
[varargin, Ny]     = getPropertyValue(varargin, 'Ny',     []);
[varargin, Method] = getPropertyValue(varargin, 'Method', 'nearest'); %#ok<ASGLU>

% if (pas_x < 1) || (pas_y < 1)
%     Method = 'nearest';
% end

switch Method
    case 'linear'
        % On s'assure que l'image traitee aura la meme taille en "nearest"
        % que en "mean"
        %         nbCol = pas_x * floor(size(I,2) / pas_x);
        %         nbRows = pas_y * floor(size(I,1) / pas_y);
        
        if isempty(pas_x)
            nbCol = size(I,2);
            x2 = linspace(1, nbCol, Nx);
        else
            if pas_x == 1
                nbCol = size(I,2);
                x2 = 1:nbCol;
            else
                nbCol = round(pas_x * floor(size(I,2) / pas_x));
                if nbCol == 0
                    return
                end
                x2 = 1:pas_x:nbCol;
                x2(end+1) = 2*x2(end) - x2(end-1);
            end
        end
        
        if isempty(pas_y)
            nbRows = size(I,1);
            y2 = linspace(1, nbRows, Ny);
        else
            if pas_y == 1
                nbRows = size(I,1);
                y2 = 1:nbRows;
            else
                nbRows = round(pas_y * floor(size(I,1) / pas_y));
                if nbRows == 0
                    return
                end
                y2 = 1:pas_y:nbRows;
                y2(end+1) = 2*y2(end) - y2(end-1);
            end
        end
        
        x1 = 1:size(I,2);
        y1 = 1:size(I,1);
        
        if (length(x1) < 2) ||(length(y1) < 2)
            return
        end
        
        J = NaN(length(y2), length(x2), class(I));
        for k=1:500:length(y2)
            sub = k:(k+500-1);
            sub = sub((sub >= 1) & (sub <= length(y2)));
            J(sub,:) = interp2(x1, y1', I, x2, y2(sub)', 'linear');
        end
        I = J;
        
    case 'nearest'
        % On s'assure que l'image trait�e aura la m�me taille en "nearest"
        % que en "mean"
        nbCol = pas_x * floor(size(I,2) / pas_x);
        nbRows = pas_y * floor(size(I,1) / pas_y);
        y = floor(1:pas_y:nbRows);
        x = floor(1:pas_x:nbCol);
        I = I(y, x);
        
    case 'mean'
        if pas_x ~= 1
            nbCol = pas_x * floor(size(I,2) / pas_x);
            if (nbCol == 0) || isinf(pas_x)
                I = [];
                return
            end
            I = I(:,1:nbCol);
            [n,m] = size(I);
            I = my_nanmean(reshape(I', pas_x, n*m/pas_x));
            I = reshape(I, m/pas_x, n)';
        end
        
        if pas_y ~= 1
            nbRows = pas_y * floor(size(I,1) / pas_y);
            if nbRows == 0
                I = [];
                return
            end
            I = I(1:nbRows, :)';
            [n,m] = size(I);
            I = my_nanmean(reshape(I', pas_y, n*m/pas_y));
            I = reshape(I, m/pas_y, n);
        end
        
    case 'sum'
        if pas_x ~= 1
            nbCol = pas_x * floor(size(I,2) / pas_x);
            if nbCol == 0
                I = [];
                return
            end
            I = I(:,1:nbCol);
            [n,m] = size(I);
            I = my_nansum(reshape(I', pas_x, n*m/pas_x));
            I = reshape(I, m/pas_x, n)';
        end
        
        if pas_y ~= 1
            nbRows = pas_y * floor(size(I,1) / pas_y);
            if nbRows == 0
                I = [];
                return
            end
            I = I(1:nbRows, :)';
            [n,m] = size(I);
            I = my_nansum(reshape(I', pas_y, n*m/pas_y));
            I = reshape(I, m/pas_y, n);
        end
end
