% Reduction d'une image
%
% Syntax
%   I = rand(256);
%   J = downsize(I, ...)
%
% Input Arguments
%   I     : Image de depart
%
% Name-Value Pair Arguments
%   pas_x : facteur de reduction en colonne
%   Method : {'nearest'} | 'mean' | 'sum'
%
% Output Arguments
%   J : Image d'arrivee
%
% Examples
%   I = rand(1,1024);
%
%   J = downsize_vector(I, 'pasx', 4);
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%   J = downsize_vector(I, 'pasx', 1/4);
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%   J = downsize_vector(I, 'pasx', 4,   'Method', 'mean');
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%   J = downsize_vector(I, 'pasx', 1/4, 'Method', 'mean');
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%   J = downsize_vector(I, 'pasx', 4,   'Method', 'sum');
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%   J = downsize_vector(I, 'pasx', 1/4, 'Method', 'sum');
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%   J = downsize_vector(I, 'pasx', 4,   'Method', 'linear');
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%   J = downsize_vector(I, 'pasx', 1/4, 'Method', 'linear');
%   figure; subplot(2,1,1); plot(I); grid on; axis tight; subplot(2,1,2); plot(J); grid on; axis tight;
%
%
% Remarks : Si Method='mean' le coefficient de r�duction doit �tre entier et la taille de l'image un multiple
%
% See also stretchmat Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function I = downsize_vector(I, varargin)

if ~(isa(I,'double') || isa(I,'single'))
    I = single(I);
end

[varargin, pas_x]  = getPropertyValue(varargin, 'pasx',   []);
[varargin, Method] = getPropertyValue(varargin, 'Method', 'nearest'); %#ok<ASGLU>

switch Method
    case 'linear'
        % On s'assure que l'image traitee aura la meme taille en "nearest" que en "mean"
        %         nbCol = pas_x * floor(size(I,2) / pas_x);
        
        if pas_x == 1
            return
        elseif pas_x > 1
            nbCol = pas_x * floor(length(I) / pas_x);
            x = floor(1:pas_x:nbCol);
            I = I(x);
        else
            m = length(I);
            newx = 1:pas_x:m;
            I = my_interp1_NoSort(1:m, I, newx);
        end

    case 'nearest'
        % On s'assure que l'image trait�e aura la m�me taille en "nearest" que en "mean"
        nbCol = pas_x * floor(length(I) / pas_x);
        x = floor(1:pas_x:nbCol);
        I = I(x);
        
    case 'mean'
        if pas_x == 1
            return
        elseif pas_x > 1
            nbCol = pas_x * floor(size(I,2) / pas_x);
            if (nbCol == 0) || isinf(pas_x)
                I = [];
                return
            end
            I = I(1:nbCol);
            m = length(I);
            I = my_nanmean(reshape(I', pas_x, m/pas_x));
        else
            m = length(I);
            newx = 1:pas_x:m;
            I = my_interp1_NoSort(1:m, I, newx);
        end

    case 'sum'
        if pas_x == 1
            return
        elseif pas_x > 1
            nbCol = pas_x * floor(size(I,2) / pas_x);
            if nbCol == 0
                I = [];
                return
            end
            I = I(:,1:nbCol);
            [n,m] = size(I);
            I = my_nansum(reshape(I', pas_x, n*m/pas_x));
            I = reshape(I, m/pas_x, n)';
        else % On ne devrait pas passer par ici en principe
            m = length(I);
            newx = 1:pas_x:m;
            I = my_interp1_NoSort(1:m, I, newx);
        end
end
