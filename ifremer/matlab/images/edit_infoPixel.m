% Affichage d'un texte dans une fen�tre avec possibilit� d'archiver ce texte dans un fichier
%
% Syntax
%   edit_infoPixel(str, NomFicPoints)
%
% Input Arguments
%   str          : Tableau de cellules contenant le texte
%   NomFicPoints : Nom du fichier de sauvegarde
%
% Examples
%   str{1} = 'Hello world';
%   str{2} = ' ';
%   str{3} = 'My name is SonarScope.';
%   str{4} = 'I thank you to have chosen the BEST software in the world.';
%   edit_infoPixel(str, [])
%   NomFicPoints = my_tempname('.dat')
%   edit_infoPixel(str, NomFicPoints)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function fig = edit_infoPixel(str, NomFicPoints, varargin)

% GCF = gcf;
fig = infoPixel_1(str, NomFicPoints, varargin{:});
% set(0, 'CurrentFigure', GCF)


function [fig, rep, validation] = infoPixel_1(ListString, NomFicPoints, varargin)

[varargin, Tag]          = getPropertyValue(varargin, 'Tag', []);
[varargin, PromptString] = getPropertyValue(varargin, 'PromptString', 'Information about the pixel you have clicked on'); %#ok<ASGLU>

msg = 'SonarScope - IFREMER';

if ischar(ListString)
    for k=1:size(ListString,1)
        str{k} = ListString(k,:); %#ok
    end
    ListString = str;
end

N = length(ListString);
if N == 0
    rep = [];
    validation = 0;
    return
end

H = 22*(1+N);
ScreenSize = get(0, 'ScreenSize');
H = min(H, ScreenSize(4)-30*10);


L = 7*length(msg);
for k=1:N
    L = max(L, 6*(10+length(ListString{k})));
end
% L = max(L, 250);
L = min(L, ScreenSize(3)-60);
ListSize = [L H];

% GCF = gcf;
[fig, rep, validation] = infoPixel_2(NomFicPoints, Tag, ...
    'Name', msg, ...
    'PromptString', PromptString, ...
    'ListString',  ListString, ...
    'ListSize',      ListSize);
% set(0, 'CurrentFigure', GCF);


function [fig, selection, value] = infoPixel_2(NomFicPoints, Tag, varargin)

ListeFig = findobj(0, 'type', 'figure', 'Tag', Tag);
if isempty(ListeFig) || isempty(Tag)
    Position = [];
else
    ListeFig = ListeFig(1);
    Position = get(ListeFig, 'Position');
end

windowstyle = 'normal';
narginchk(1,inf)

figname      = '';
smode        = 2; %1;   % (multiple)
promptstring = {};
ListString   = [];
listsize     = [160 300];
initialvalue = [];
fus          = 8;
ffs          = 8;
uh           = 22;

for k=1:2:length(varargin)
    switch lower(varargin{k})
        case 'name'
            figname = varargin{k+1};
        case 'promptstring'
            promptstring = varargin{k+1};
        case 'selectionmode'
            switch lower(varargin{k+1})
                case lower('Single')
                    smode = 1;
                case lower('Multiple')
                    smode = 2;
            end
        case 'listsize'
            listsize = varargin{k+1};
        case 'liststring'
            ListString = varargin{k+1};
        case 'initialvalue'
            initialvalue = varargin{k+1};
        otherwise
            my_warndlg(['Unknown parameter name passed to LISTDLG.  Name was ' varargin{k}], 1);
    end
end

if ischar(promptstring)
    promptstring = cellstr(promptstring);
end

if isempty(initialvalue)
    initialvalue = 1;
end

if isempty(ListString)
    error('ListString parameter is required.')
end

ex = get(0,'defaultuicontrolfontsize') * 1.7;  % height extent per line of uicontrol text (approx)
if isempty(Position)
    fp = get(0, 'defaultfigureposition');
    h = 2 * ffs + 6 * fus + ex * length(promptstring) + listsize(2) + uh + (smode==2) * (fus+uh);
    fp = [fp(1) fp(2)+fp(4)-h 1980 h];  % keep upper left corner fixed
else
    fp = Position;
end
fp(3) = max(400, floor(listsize(2) / 2)); %1220;

fig_props = { ...
    'name'                   figname, ...
    'color'                  get(0,'defaultUicontrolBackgroundColor'), ...
    'resize'                 'on', ...
    'numbertitle'            'off', ...
    'menubar'                'none', ...
    'windowstyle'            windowstyle, ...
    'visible'                'on', ...
    'createfcn'              '',    ...
    'position'               fp,   ...
    'closerequestfcn'        'delete(gcbf)'};
ListString = cellstr(ListString);

fig = figure(fig_props{:});
if ~isempty(Tag)
    set(fig, 'Tag', Tag);
end


% Position = centrageFig(1980, 1200);
% set(fig, 'Position', Position);


if ~isempty(promptstring)
    prompt_text = uicontrol('style', 'text', 'string', promptstring,...
        'horizontalalignment', 'left',...
        'position', [ffs+fus fp(4)-(ffs+fus+ex*length(promptstring)) ...
        listsize(1) ex*length(promptstring)]); %#ok<NASGU>
end

btn_wid = (fp(3)-2*(ffs+fus)-fus) + 2;

listbox = uicontrol('style', 'listbox', ...
    'position', [ffs+fus ffs+uh+4*fus listsize],...
    'string',   ListString, ...
    'backgroundcolor', 'w', ...
    'max',     smode, ...
    'tag',     'listbox', ...
    'value',    initialvalue, ...
    'callback', @doListboxClick);

frameh = uicontrol('style', 'frame', ...
    'position', [ffs+fus-1 ffs+fus-1 btn_wid uh+2], ...
    'backgroundcolor', 'k'); %#ok<NASGU>

if isempty(NomFicPoints)
    Close_btn = uicontrol('style', 'pushbutton', ...
        'string', 'Close',...
        'position', [ffs+fus ffs+fus btn_wid uh], ...
        'callback', {@doClose, listbox}); %#ok<NASGU>
else
    Close_btn = uicontrol('style', 'pushbutton',...
        'string', 'Close', ...
        'position', [ffs+fus ffs+fus btn_wid/4 uh], ...
        'callback', {@doClose, listbox}); %#ok<NASGU>
    
    strSave =  ['Save Point in ' NomFicPoints];
    Save_btn = uicontrol('style','pushbutton',...
        'string', strSave, ...
        'position', [ffs+fus+btn_wid/4 ffs+fus btn_wid*3/4 uh], ...
        'callback', {@doSave, NomFicPoints, ListString}); %#ok<NASGU>
end

if isempty(Position)
    Position = get(fig, 'Position');
    centrageFig(Position(3), Position(4), 'Fig', fig);
end

% make sure we are on screen
movegui(fig)
set(fig, 'visible', 'on');

Position = get(fig, 'Position');
% Position(3) = max(Position(3), 830); % Modif JMA le 18/01/2021
set(fig, 'Position', Position)


% set(fig, 'Resize', 'on')

try
    if strcmp(windowstyle, 'modal')
        uiwait(fig);
    end
catch %#ok<CTCH>
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'ListDialogAppData')
    ad = getappdata(0, 'ListDialogAppData');
    selection = ad.selection;
    value = ad.value;
    rmappdata(0, 'ListDialogAppData')
else
    % figure was deleted
    selection = [];
    value = 0;
end

function doClose(Close_btn, evd, listbox) %#ok<INUSL>
ad.value = 1;
ad.selection = get(listbox,'value');
setappdata(0,'ListDialogAppData',ad)
delete(gcbf);

function doSave(Save_btn, evd, NomFicPoints, ListString) %#ok<INUSL>
[fid, message] = fopen(NomFicPoints, 'a');
if fid == -1
    my_warndlg(message, 1);
end
for k=1:length(ListString)
    X = ListString{k};
    for k2=1:size(X,1)
        fprintf(fid, '%s\n', X(k2,:));
    end
end
fclose(fid);

function doClick(listbox)
selection = get(listbox,'value');
String = get(listbox, 'String');
disp(String{selection})


function doListboxClick(listbox, evd, selectall_btn) %#ok<INUSL>
if strcmp(get(gcbf,'SelectionType'),'open')
    doClick(listbox);
elseif nargin == 3
    if length(get(listbox,'string'))==length(get(listbox,'value'))
        set(selectall_btn,'enable','off')
    else
        set(selectall_btn,'enable','on')
    end
end
