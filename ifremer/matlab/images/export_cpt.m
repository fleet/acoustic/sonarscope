% nomFic = getNomFicDatabase('DEM_screen.cpt');
% [flag, map] = import_cpt(nomFic, 'NColors', 256)
% nomFicOut = [tempname '.cpt']
% flag = export_cpt(map, nomFic)

function flag = export_cpt(map, nomFic, varargin)

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []); %#ok<ASGLU>

flag = -1;
fid = fopen(nomFic, 'w+');
if fid == -1
    return
end

if isempty(CLim)
    Deb = 1:(size(map, 1)-1);
    Fin = Deb + 1;
else
    X = linspace(CLim(1), CLim(2), size(map, 1));
    Deb = X(1:end-1);
    Fin = X(2:end);
end

for k=1:size(map, 1)-1
    str = sprintf('%f %f %f %f %f %f %f %f', Deb(k), map(k,:), Fin(k), map(k+1,:));
    fprintf(fid, '%s\n', str);
end
fclose(fid);
flag = 1;
