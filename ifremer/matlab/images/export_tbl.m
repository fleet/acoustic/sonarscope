% Export d'une table de couleur au format Caraibes
%
% Syntax
%   flag = export_tbl(map, nomFic, varargin)
%
% Input Arguments
%   map    : matrice n*3
%   nomFic : nom du fichier Fledermaus
%
% Name-Value Pair Arguments
%
% Name-only Arguments
%
% Output Arguments
%   flag
%
% Examples
%
% See also export_cpt.m, callback_ColormapIndex.m
% Authors : GLT
% -------------------------------------------------------------------------

function flag = export_tbl(map, nomFic, varargin)

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []); %#ok<ASGLU>

flag = -1;
fid = fopen(nomFic, 'w+');
if fid == -1
    return
end

if isempty(CLim)
    Deb = 1;
    Fin = size(map, 1);
else
    X = linspace(CLim(1), CLim(2), size(map, 1));
    Deb = X(1);
    Fin = X(end);
end

%% Ecriture
fprintf(fid, ' Minimum {%.3f}\n', Deb);
fprintf(fid, ' Maximum {%.3f}\n', Fin);
fprintf(fid, ' Subdivision {%d}\n', length(X));
fprintf(fid, ' Colors { %.3f {%d %d %d}\n', X(1), floor(map(1,:)*256));

for i = 2 : size(map, 1)-1
    fprintf(fid,' %.3f {%d %d %d}\n', X(i), floor(map(i,:)*256));
end
fprintf(fid, '}');
fclose(fid);
flag = 1;
