% Generation d'une sinusoide amortie : y = A * exp(-x/Alpha) * cos(2*pi*x/Periode)
%
% Syntax
%   y = fctSinc(x, Parametres)
%
% Input Arguments 
%   x           : Abscisses
%   Parametres  : Tableau contenant les parametres du modele
%		Periode : Periode de la sinusoide
%
% Output Arguments
%   []          : Auto-plot activation
%   y           : Signal de synthese.
%
% Examples 
%   x = 0:250;
%   fctSinc(x, [100]);
%
%   y = fctSinc(x,  [100]);
%   plot(x, y); grid on; zoom on; hold on;
%
% See also fctSinc Authors
% Authors : JMA
% VERSION  : $Id: fctSinc.m,v 1.4 2002/06/17 16:21:37 augustin Exp $
%------------------------------------------------------------------------------

function varargout = fctSinc(x, Parametres)

Periode   = Parametres(1);

nbCourbes = size(x, 1);
y = zeros(nbCourbes, length(x));
for i=1:nbCourbes
    y(i,:) = sinc(2 * pi * x / Periode);
end
composantes = y;

% -----------------------------------------
% Sortie des parametre ou traces graphiques

if nargout == 0
    figure
    if nbCourbes == 1
        plot(x, y, 'k'); grid on;
        xlabel('x'); ylabel('y');
        title('Modele fctSinc');
    else
        plot(x, y); grid on;
    end
else
    varargout{1} = y;
    varargout{2} = composantes;
end
