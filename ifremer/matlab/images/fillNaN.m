% Interpolates NaN values using a specific method. The value is computed from the valid pixels in the vicinity defined bay the sliding window.
%
% Syntax
%   Y = fillNaN_mean(X, window, ...)
%
% Input Arguments
%   X      : Matrix containing the image with missing values
%   window : Size of the sliding window (Default : [3 3])
%
% Name-Value Pair Arguments
%   method : {'mean'}, 'min', 'max', ...
%
% Output Arguments
%   Y : Matrix interpolated
%
% Examples
%   s = mod(0:2:1800, 360);
%   X = repmat(s, length(s), 1);
%   X(100:250,100:250) = NaN;
%   N = floor(length(s)^2 / 2);
%   d = randi(length(s), [N 2]);
%   for k=1:size(d,1)
%       X(d(k,2),d(k,1)) = NaN;
%   end
%   figure; imagesc(X)
%
%   Y = fillNaN(X, [3 3], 'method', 'min');
%   figure; imagesc(Y)
%
% See also fillNaN_mean cl_image/WinFillNaN Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function I = fillNaN(x, window, varargin)

[varargin, method] = getPropertyValue(varargin, 'method', 'mean'); %#ok<ASGLU>

switch method
    case {'min'; 'max'}
        flagAccelerationCalcul = false;
    otherwise
        flagAccelerationCalcul = true;
end
method = str2func(method);

% disp('Attention, ici changement j''ai supprimé un truc qui peut engendrer une mauvaise interpolation des bords de l''image. Merci de me signaler si il est souhaitable de remettre cette fonctionnalité en service')
% x = windowEnlarge(x, window, []);

wl = floor(window(1) / 2);
wc = floor(window(2) / 2);

BW = ~isnan(x);
% figure; imagesc(BW, [0 1]); axis xy; colorbar

% se = strel('line', LEN, -45)
se = ones(window);
BWD = imdilate(BW, se);
% figure; imagesc(BWD, [0 1]); axis xy; colorbar

BWI = ~BW & BWD;
% figure; imagesc(BWI, [0 1]); axis xy; colorbar

I = x;
for il=(wl+1):(size(x,1)-wl)
    subl = (il-wl):(il+wl);
    for ic=(wc+1):(size(x,2)-wc)
        if BWI(il, ic)
            subc = (ic-wc):(ic+wc);
            subVal = BW(subl,subc);
            if sum(subVal) == 0
                continue
            else
                xExtract = x(subl,subc);
                X = xExtract(subVal);
                if flagAccelerationCalcul
                    I(il, ic) = method(X(:), 1);
                else
                    I(il, ic) = method(X(:));
                end
            end
        end
    end
end
% clear x
% x = windowRetreci(I, window);
