% Interpolates NaN heading values. The values are interpolated if they are
% in the vicinity of one or more valid pixels
%
% Syntax
%   Y = fillNaN_heading(X, window)
%
% Input Arguments
%   X      : Matrix containing the heading with missing values
%   window : Size of the sliding window (Default : [3 3])
%
% Output Arguments
%   Y : Matrix interpolated
%
% Examples
%   s = mod(0:2:1800, 360);
%   X = repmat(s, length(s), 1);
%   X(100:250,100:250) = NaN;
%   N = floor(length(s)^2 / 2);
%   d = randi(length(s), [N 2]);
%   for k=1:size(d,1)
%       X(d(k,2),d(k,1)) = NaN;
%   end
%   figure; imagesc(X)
%
%   Y = fillNaN_heading(X, [3 3]);
%   figure; imagesc(Y)
%
% See also fillNaN_mean cl_image/WinFillNaN Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Y = fillNaN_heading(X, window)

X = X(:,:) * (pi/180);
cosx = fillNaN_mean(cos(X), window);
sinx = fillNaN_mean(sin(X), window);
clear X
Y = atan2(sinx, cosx) * (180/pi);
