% Interpolates NaN values using a mean value of valid pixels in the vicinity defined bay the sliding window.
%
% Syntax
%   Y = fillNaN_mean(X, window)
%
% Input Arguments
%   X      : Matrix containing the image with missing values
%   window : Size of the sliding window (Default : [3 3])
%
% Output Arguments
%   Y : Matrix interpolated
%
% Examples
%   s = mod(0:2:1800, 360);
%   X = repmat(s, length(s), 1);
%   X(100:250,100:250) = NaN;
%   N = floor(length(s)^2 / 2);
%   d = randi(length(s), [N 2]);
%   for k=1:size(d,1)
%       X(d(k,2),d(k,1)) = NaN;
%   end
%   figure; imagesc(X)
%
%   Y = fillNaN_mean(X, [3 3]);
%   figure; imagesc(Y)
%
% See also fillNaN_heading cl_image/WinFillNaN Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function I = fillNaN_mean(x, window)

% Modif JMA le 26/11/2020

% I = fillNaN_mean_matlab(x, window);
    
% {
global NUMBER_OF_PROCESSORS %#ok<GVMIS>

if NUMBER_OF_PROCESSORS == 0
    I = fillNaN_mean_matlab(x, window);
    fprintf('Message for JMA : appel �  fillNaN_mean_matlab car NUMBER_OF_PROCESSORS=0\n');
else
    switch class(x)
        case 'single'
            try
                I = fillNaN_mean_mexmc(x, double(window), double(NUMBER_OF_PROCESSORS));
            catch
                fprintf('Message for JMA : fillNaN_mean_mexmc failed (pb de DLL) - appel forc� de fillNaN_mean_matlab\n');
                I = fillNaN_mean_matlab(x, window);
            end
        case 'double'
            try
                I = fillNaN_mean_double_mexmc(x, double(window), double(NUMBER_OF_PROCESSORS));
            catch % TODO GLU : A garder pour l'instant tant que les pbs d'instal de la dll ne sont pas finalis�s
                fprintf('Message for JMA : fillNaN_mean_double_mexmc failed (pb de DLL) - appel forc� de fillNaN_mean_matlab\n');
                I = fillNaN_mean_matlab(x, window);
            end
        otherwise
            I = fillNaN_mean_matlab(x, window);
            fprintf('Message for JMA : appel �  fillNaN_mean_matlab car donn�e n''est pas single ou double\n');
    end
end
% }
