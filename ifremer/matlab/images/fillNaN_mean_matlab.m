function I = fillNaN_mean_matlab(x, window)

wl = floor(window(1) / 2);
wc = floor(window(2) / 2);

BW = ~isnan(x);
% figure; imagesc(BW, [0 1]); axis xy; colorbar

% se = strel('line', LEN, -45) 
se = ones(window);
BWD = imdilate(BW, se);
% figure; imagesc(BWD, [0 1]); axis xy; colorbar

BWI = ~BW & BWD;
% figure; imagesc(BWI, [0 1]); axis xy; colorbar

I = x;

%% Nouvelle méthode, version classique

% {
M = BWI((wl+1):(size(x,1)-wl) , (wc+1):(size(x,2)-wc));
[YI,XJ] = ind2sub(size(M), find(M));
NP = length(YI);
for k=1:NP
    il = YI(k) + wl;
    ic = XJ(k) + wc;
    subl = (il-wl):(il+wl);
    subc = (ic-wc):(ic+wc);
    subVal = BW(subl,subc);
    if sum(subVal) == 0
        continue
    else
        xExtract = x(subl,subc);
        X = xExtract(subVal);
        I(il, ic) = sum(X(:)) / length(X);
    end
end
% }

%% Nouvelle méthode, version parallélisée

%{
M = BWI((wl+1):(size(x,1)-wl) , (wc+1):(size(x,2)-wc));
[YI,XJ] = ind2sub(size(M), find(M));
NP = length(YI);
V = NaN(1, NP, class(I));
parfor k=1:NP
    il = YI(k) + wl;
    ic = XJ(k) + wc;
    subl = (il-wl):(il+wl);
    subc = (ic-wc):(ic+wc);
    subVal = BW(subl,subc);
    if sum(subVal) == 0
        continue
    else
        xExtract = x(subl,subc);
        X = xExtract(subVal);
        V(k) = sum(X(:)) / length(X);
    end
end

for k=1:NP
    il = YI(k) + wl;
    ic = XJ(k) + wc;
    I(il, ic) = V(k);
end
%}
