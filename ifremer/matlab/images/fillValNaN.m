% Filtre moyenneur intervenant uniquement sur les valeurs NaN
%
% Syntax
%   y = fillValNaN(x, window, ValNaN)
%
% Input Arguments
%   x      : Image a traiter
%   window : Taille de la fenetre d'analyse
%   ValNaN : Valeur representant les non-valeurs
%
% Output Arguments
%   y     : Image filtree
%
% EXAMPLE
%   nomFic = getNomFicDatabase('EM300_ExMos.imo');
%   a = cl_car_ima(nomFic);
%   R = get(a, 'REFLECTIVITY');
%   figure; imagesc(R, [-42 -16]); colorbarHist;
%   Rint = fillValNaN(R, [5 5], NaN);
%   figure; imagesc(Rint, [-42 -16]); colorbarHist;
%   colormap(gray(256))
%
% See also nlfilter NoyauFillNan Authors
% Authors : JMA
%-------------------------------------------------------------------------------

% ATTENTION : M�me si ValNaN n'est pas utilis� il faut le laisser car cette fonction est appel�e par eval
function I = fillValNaN(x, window, ValNaN, varargin)

[varargin, method] = getPropertyValue(varargin, 'method', 'mean'); %#ok<ASGLU>

method = str2func(method);

TypeVar = class(x(1));
if isa(TypeVar, 'uint8')
    x = single(x);
end

% disp('Attention, ici changement j''ai supprim� un truc qui peut engendrer une mauvaise interpolation des bords de l''image. Merci de me signaler si il est souhaitable de remettre cette fonctionnalit� en service')
% x = windowEnlarge(x, window, []);

wl = floor(window(1) / 2);
wc = floor(window(2) / 2);

if isnan(ValNaN)
    BW = ~isnan(x);
else
%     BW = (x ~= ValNaN);
    BW = ~((x == ValNaN) | isnan(x)); % Modif JMA le 30/01/2019 pour masques Marc
end
% figure; imagesc(BW, [0 1]); axis xy; colorbar

% se = strel('line', LEN, -45)
se = ones(window);
BWD = imdilate(BW, se);
% figure; imagesc(BWD, [0 1]); axis xy; colorbar

BWI = ~BW & BWD;
% figure; imagesc(BWI, [0 1]); axis xy; colorbar

I = x;

% N = size(x,1);
for il=(wl+1):(size(x,1)-wl)
    subl = (il-wl):(il+wl);
    for ic=(wc+1):(size(x,2)-wc)
        if BWI(il, ic)
            subc = (ic-wc):(ic+wc);
            xExtract = x(subl,subc);
            subVal = BW(subl,subc);
            I(il, ic) = method(xExtract(subVal));
        end
    end
end

% x = windowRetreci(I, window);

if isa(TypeVar, 'uint8')
    I = uint8(floor(I));
end
