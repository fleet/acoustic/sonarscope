% Filtre moyenneur intervenant uniquement sur les valeurs NaN
%
% Syntax
%   y = fillValNaN(x, window, ValNaN)
%
% Input Arguments 
%   x      : Image a traiter
%   window : Taille de la fenetre d'analyse
%   ValNaN : Valeur representant les non-valeurs
%
% Output Arguments 
%   y     : Image filtree
%
% EXAMPLE
%   nomFic = getNomFicDatabase('EM300_ExMos.imo');
%   a = cl_car_ima(nomFic)
%   R = get(a, 'REFLECTIVITY');
%   figure; imagesc(R, [-42 -16]); colorbarHist;
%   Rint = fillValNaN(R, [5 5], NaN);
%   figure; imagesc(Rint, [-42 -16]); colorbarHist;
%   colormap(gray(256))
%
% See also nlfilter NoyauFillNan Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function x = fillValNaN_median(x, window)

TypeVar = class(x(1));
if isa(TypeVar, 'uint8')
    x = single(x);
end

x = windowEnlarge(x, window, []);

wl = floor(window(1) / 2);
wc = floor(window(2) / 2);

BW = ~isnan(x); % cette expression semble donner de meilleur resultats que la suivante
% BW = (x ~= ValNaN);
% figure; imagesc(BW, [0 1]); axis xy; colorbar

% se = strel('line', LEN, -45) 
se = ones(window);
BWD = imdilate(BW, se);
% figure; imagesc(BWD, [0 1]); axis xy; colorbar

BWI = ~BW & BWD;
% figure; imagesc(BWI, [0 1]); axis xy; colorbar

I =x;
N = size(x,1);
hw = create_waitbar('Ca roule', 'N', N);
for il=(wl+1):(size(x,1)-wl)
    my_waitbar(il, N, hw);
    
    subl = (il-wl):(il+wl);
    for ic=(wc+1):(size(x,2)-wc)
        if BWI(il, ic)
            subc = (ic-wc):(ic+wc);
            xExtract = x(subl,subc);
            subVal = BW(subl,subc);
            I(il, ic) = median(xExtract(subVal));
        end
    end
end
my_close(hw)

x = windowRetreci(I, window);

if isa(TypeVar, 'uint8')
    x = uint8(floor(x));
end
