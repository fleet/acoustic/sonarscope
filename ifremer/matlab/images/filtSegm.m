% Filtre d'une image segmentee
%
% Syntax
%   y = filtSegm(x)
%
% Input Arguments 
%   x     : Image a traiter
%
% Output Arguments 
%   y     : Image filtree
%
% Examples
%   [X, map] = imread('/tmp/pppp.tif');
%   imagesc(X); colormap(map(1:6, :)); colorbar; truesize;
%   Y = nlfilter(X, [5 5], 'filtSegm');
%   figure; imagesc(Y); colormap(map(1:6, :)); colorbar; truesize;
%   imwrite(Y+1, map(1:6, :), '/tmp/qqqq.tif', 'tif')
%   [Z, map] = imread('/tmp/qqqq.tif');
%   figure; imagesc(Z); colormap(map(1:6, :)); colorbar; truesize;
%
% See also nlfilter Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y = filtSegm(x)

x = double(x(:));
xmin = min(x);
xmax = max(x);

if xmin == xmax
    y = xmin;
    return
end

sub = xmin:xmax;
h = my_hist(x, sub);

[~, I] = sort(h);
y = sub(I(end));