% Spike filter on tiles of size (20 20] with a shift of 15 between tiles.
% This function estimates for every tile of size [20 20] the
% polynomial surface fitting the data with a minimum error quadratic mean,
% computes the standard deviation and reject points that are upper than
% Seuil*std
%
% Syntax
%   R = filterSpikeTile(I, ...)
%
% Input Arguments
%   I         : Input image of dimension [N M P] P can  be different from 1 if one
%               want to get additional data as polysurf and residuals
%   Threshold : Threshold to consider a pixel is a spike. Threshold is a
%               multiplier of the standard deviation of the residual
%   Restore   : Restoration value (the points that were filtered by the
%               spike filter that are inferior to this threshold are restored).
%               This is usefuf when the data is very clean.
%   Margin : Margin around the image to set to NaN (Advise : 0)
%   Fig1   : Figure number if you want to display the histogram of normalized residuals
%   Fig2   : Figure number if you want to display the histogram of residuals (in the unit of I)
%
% Name-Value Pair Arguments
%   subx : Sub-sampling in abscissa
%   suby : Sub-sampling in ordinates
%
% Output Arguments
%   R : Filtered image and residuals : R is a matrix of dimension [N M 3]
%       R(:,:,1) : Filtered image
%       R(:,:,2) : PolySurf
%       R(:,:,3) : Residuals (I-PolySurf)
%
% Examples
%   I = Prismed;
%   [N, M] = size(I);
%   Np = floor(N*M / 10);
%   xd = randi(M, [Np 1]);
%   yd = randi(N, [Np 1]);
%   Noise = rand([Np 1]) * 1000;
%   for k=1:Np
%       I(yd(k),xd(k)) = I(yd(k),xd(k)) + Noise(k);
%   end
%   figure; imagesc(I);
%
%   R = filterSpike(I, 1.5, 0, 0, [], []);
%   figure; imagesc(R(:,:,1)); title('Filtered image')
%   figure; imagesc(R(:,:,2)); title('Polysurf image')
%   figure; imagesc(R(:,:,3)); title('Residuals image')
%
%   R = filterSpike(I, 1.5, 50, 0, [], []); % Restore = 50 m
%
%   R = filterSpike(I, 1.5, 0, 20, [], []); % Margin = 20
%   figure; imagesc(R(:,:,1)); title('Filtered image')
%
% See also filterSpikeTile cl_image/filterSpikeBathy Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Image = filterSpike(I, Threshold, Restore, Margin, Fig1, Fig2, varargin)

[varargin, flagOtherImages] = getPropertyValue(varargin, 'OtherImages', 1); %#ok<ASGLU>

% En attendant de faire ce qu'il faut pour mettre les coordonn�es m�triques
% de chaque sonde si on est en PingXxxx
[NbLig, NbCol] = size(I);
if flagOtherImages
    I(:,:,3) = repmat((1:NbLig)', 1,     NbCol);
    I(:,:,2) = repmat( 1:NbCol,   NbLig, 1);
end
NMax     = 20;
Decalage = 5;

W = warning;
warning('off');

% profile on

useParallel = get_UseParallelTbx;

fun = @(block_struct) filterSpikeTile(block_struct.data, Threshold);
try
    Image = blockproc(I, [NMax-2*Decalage NMax-2*Decalage], fun, 'BorderSize', [Decalage Decalage], 'UseParallel', useParallel);
catch
    Image = blockproc(I, [NMax-2*Decalage NMax-2*Decalage], fun, 'BorderSize', [Decalage Decalage]);
end

if isa(I, 'single')
    Image = single(Image);
end

if ~isempty(Fig1)
    str1 = 'Histogramme des r�sidus normalis�s';
    str2 = 'Histogram of normalized residus';
    histo1D(Image(:,:,3), 'Titre', Lang(str1,str2), 'Fig', Fig1)
end

% TODO : remettre en service apr�s analyse de ce que �a demande en entr�e
% Image(:,:,1) = rehabilitation(I(:,:,1), Image(:,:,1), Restore, Fig2); % TODO : retrouver ce qui �tait programm� avant

warning(W);

% figure;
if ~isempty(Margin) && (Margin ~= 0) %&& isequal(subx, 1:this.nbColumns)
    for k=1:NbLig
        Ligne = Image(k,:,1);
        sub = isnan(Ligne);
        deb = find(~sub, 1, 'first');
        fin = find(~sub, 1, 'last');
        sub = [deb:(deb+Margin-1) (fin-Margin+1):fin];
        Image(k,sub,1) = NaN;
        %         plot(Ligne); grid on; hold on; plot(Image(k,:,1), 'r');
        %         hold off
        %         drawnow
    end
    for k=1:NbCol
        Ligne = Image(:,k,1);
        sub = isnan(Ligne);
        deb = find(~sub, 1, 'first');
        fin = find(~sub, 1, 'last');
        sub = [deb:(deb+Margin-1) (fin-Margin+1):fin];
        Image(sub,k,1) = NaN;
        %         plot(Ligne); grid on; hold on; plot(Image(k,:,1), 'r');
        %         hold off
        %         drawnow
    end
end

%{
function Image = rehabilitation(I, Image, Restore, Fig)

if Restore == 0
    return
end

subNaN = find(isnan(Image) & ~isnan(I));
ImageInterpolee = fillNaN_mean(Image, [5 5]);
Diff = abs(I(subNaN) - ImageInterpolee(subNaN));

if ~isempty(Fig)
    histo1D(Diff, 'Titre', Lang('Histogramme des diff�rences de MNT', 'Histogram of DTM differences'), ...
        'Fig', Fig)
end

sub = (Diff < Restore);
Image(subNaN(sub)) = I(subNaN(sub));
%}
