% Spike filter kernel for filterSpike function. This function estimates the
% polynomial surface fitting the data with a minimum error quadratic mean,
% computes the standard deviation and reject points that are upper than
% Seuil*std
%
% Syntax
%   R = filterSpikeTile(I, ...)
%
% Input Arguments
%   I : Input image of dimension [N M P] P can  be different from 1 if one
%       want to get additional data as polysurf and residuals
%
% Name-Value Pair Arguments
%   seuil : Threshold to consider a pixel is a spike. Threshold is a
%           multiplier of the standard deviation of the residual
%
% Output Arguments
%   R : Filtered image and residuals : R is a matrix of dimension [N M P]
%       R(:,:,1) : Filtered image         (if P = 1)
%       R(:,:,2) : PolySurf               (if P = 2)
%       R(:,:,3) : Residuals (I-PolySurf) (if P = 3)
%
% Remarks : This function is made to be called by an upper function using
% blockproc (see filterSpike)
%
% Examples
%   I = Prismed;
%   figure; imagesc(I);
%
%   R = filterSpikeTile(I, 1.5);
%   figure; imagesc(R);
%
% See also filterSpike cl_image/filterSpikeBathy Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function J = filterSpikeTile(I, seuil)

persistent x1 y1 APolyVal x y APolyFit subx0 suby0

J = double(I);
K = J(:,:,1);

subx = ~all(J(:,:,1)==0, 1);
K = K(:,subx);

suby = ~all(J(:,:,1)==0, 2);
K = K(suby,:);


% Comment� par JMA le 07/03/2019 pour donn�es Haliotis 
%{
if sum(isnan(K(:))) > (numel(K) / 2)
    J(:) = NaN;
    return
end
%}


% x0 = 1:size(J, 2);
% y0 = 1:size(J, 1);
x0 = linspace(0,1,size(J, 2));
y0 = linspace(0,1,size(J, 1));
if ~isequal(x0, x1) || ~isequal(y0, y1)
    x1 = x0;
    y1 = y0;
    [x, y] = meshgrid(x1, y1);
    APolyVal = [];
end

if ~isequal(subx0, subx) || ~isequal(suby0, suby)
    APolyFit = [];
end
subx0 = subx;
suby0 = suby;

% [flag, C, APolyFit] = polyfitSurf(x(suby,subx), y(suby,subx), double(K), ...
%     'pX', 2, 'pY', 2, 'A', APolyFit); %#ok<NASGU>
try
    [flag, C, APolyFit] = polyfitSurf_direct(x(suby,subx), y(suby,subx), double(K), 2, 2, APolyFit);
catch %#ok<CTCH>
    return
end

try
    [PolySurf, APolyVal] = polyvalSurf(C, x, y, 'A', APolyVal, '2D');
catch %#ok<CTCH>
    return
end

Residu = abs(J(:,:,1) - PolySurf);
K = Residu(suby,subx);
K = K(~isnan(K(:)));
Moyenne = mean(K);

if Moyenne == 0
    return
end
    
Residu = Residu / Moyenne;
L = J(:,:,1);
subRejected = (Residu > seuil);
L(subRejected) = NaN;

%{
L(subRejected) = PolySurf(subRejected);
subNaN = isnan(L);
L(subNaN) = PolySurf(subNaN);
%}

J(:,:,1) = L;
if size(J,3) >= 2
    J(:,:,2) = PolySurf;
end
if size(J,3) >= 3
    J(:,:,3) = Residu;
end
