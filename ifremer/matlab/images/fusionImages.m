% Composition d'une mosaique d'image par assemblage
%
% Syntax
%   [I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(I1, I2, ...)
% 
% Input Arguments 
%   I1 : Premire image
%   I2 : Deuxieme image 
% 
% Name-Value Pair Arguments 
%   I3 : Troisime image
%   ......
%
% Output Arguments 
%   I    : Image composee
%   Cdeb : Colonnes debut et fin ou sont placees
%   Cfin : les images dans l'image resultante
%   Ldeb : Lignes debut et fin ou sont placees
%   Lfin : les images dans l'image resultante
%
% Examples
%   nomFic1 = getNomFicDatabase('texture01.png');
%   [pathFic, Label1] = fileparts(nomFic1);
%   nomFic2 = getNomFicDatabase('texture02.png');
%   [pathFic, Label2] = fileparts(nomFic2);
%   nomFic3 = getNomFicDatabase('texture03.png');
%   [pathFic, Label3] = fileparts(nomFic3);
%   nomFic4 = getNomFicDatabase('texture04.png');
%   [pathFic, Label4] = fileparts(nomFic4);
% 
%   I1 = imread(nomFic1);
%   I2 = imread(nomFic2);
%   I3 = imread(nomFic3);
%   I4 = imread(nomFic4);
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages(I1,I2);
%   imagesc(I); colormap(gray(256));
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages(I1(1:100,:),I2(1:100,:));
%   imagesc(I); colormap(gray(256));
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages(I1(:,1:100),I2(:,1:100));
%   imagesc(I); colormap(gray(256));
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages(I1,I2,I3,I4);
%   imagesc(I); colormap(gray(256));
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(varargin)

J = [];
for k=1:2:nargin
    if size(varargin{k}, 1) < size(varargin{k}, 2)
        J{end+1} = [varargin{k}; varargin{k+1}]; %#ok<AGROW>
        
        Cdeb(k)   = 1; %#ok<AGROW>
        Cdeb(k+1) = 1; %#ok<AGROW>
        Cfin(k)   = size(varargin{k}, 2); %#ok<AGROW>
        Cfin(k+1) = size(varargin{k+1}, 2); %#ok<AGROW>
        
        Ldeb(k)   = 1; %#ok<AGROW>
        Ldeb(k+1) = 1+size(varargin{k}, 1); %#ok<AGROW>
        Lfin(k)   = size(varargin{k}, 1); %#ok<AGROW>
        Lfin(k+1) = size(varargin{k}, 1) + size(varargin{k+1}, 1); %#ok<AGROW>
    else
        J{end+1} = [varargin{k} varargin{k+1}]; %#ok<AGROW>
        
        Cdeb(k)   = 1; %#ok<AGROW>
        Cdeb(k+1) = 1+size(varargin{k}, 2); %#ok<AGROW>
        Cfin(k)   = size(varargin{k}, 2); %#ok<AGROW>
        Cfin(k+1) = size(varargin{k}, 2) + size(varargin{k+1}, 2); %#ok<AGROW>

        Ldeb(k)   = 1; %#ok<AGROW>
        Ldeb(k+1) = 1; %#ok<AGROW>
        Lfin(k)   = size(varargin{k}, 1); %#ok<AGROW>
        Lfin(k+1) = size(varargin{k+1}, 1); %#ok<AGROW>
    end
end

I = [];
if size(J{1}, 1) <= size(J{1}, 2)
    for k=1:length(J)
        Ldeb((k-1)*2+1) = size(I,1) + 1;
        Ldeb((k-1)*2+2) = size(I,1) + 1;
        
        Lfin((k-1)*2+1) = Lfin((k-1)*2+1) + size(I,1);
        Lfin((k-1)*2+2) = Lfin((k-1)*2+2) + size(I,1);
        
        subl = 1:size(J{k},1);
        subc = 1:size(J{k},2);
        I(end+subl, subc) = J{k}; %#ok<AGROW>
        
    end
else
    for k=1:length(J)
        Cdeb((k-1)*2+1) = size(I,2) + 1;
        Cdeb((k-1)*2+2) = size(I,2) + 1;
         
        Cfin((k-1)*2+1) = Cfin((k-1)*2+1) + size(I,2);
        Cfin((k-1)*2+2) = Cfin((k-1)*2+2) + size(I,2);
        
        subl = 1:size(J{k},1);
        subc = 1:size(J{k},2);
        I(subl, end+subc) = J{k}; %#ok<AGROW>
    end
end
