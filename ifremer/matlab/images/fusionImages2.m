% BIDOUILLE POUR L'INSTANT. A FINALISER POUR REMPLACER fusionImages
% Composition d'une mosaique d'image par assemblage
%
% Syntax
%   [I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages2(I1, I2, ...)
%
% Input Arguments
%   I1 : Premire image
%   I2 : Deuxieme image
%
% Name-Value Pair Arguments
%   I3 : Troisime image
%   ......
%
% Output Arguments
%   I    : Image composee
%   Cdeb : Colonnes debut et fin ou sont placees
%   Cfin : les images dans l'image resultante
%   Ldeb : Lignes debut et fin ou sont placees
%   Lfin : les images dans l'image resultante
%
% Examples
%   nomFic1 = getNomFicDatabase('texture01.png');
%   [pathFic, Label1] = fileparts(nomFic1);
%   nomFic2 = getNomFicDatabase('texture02.png');
%   [pathFic, Label2] = fileparts(nomFic2);
%   nomFic3 = getNomFicDatabase('texture03.png');
%   [pathFic, Label3] = fileparts(nomFic3);
%   nomFic4 = getNomFicDatabase('texture04.png');
%   [pathFic, Label4] = fileparts(nomFic4);
%
%   I1 = imread(nomFic1);
%   I2 = imread(nomFic2);
%   I3 = imread(nomFic3);
%   I4 = imread(nomFic4);
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages2(I1,I2);
%   imagesc(I); colormap(gray(256));
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages2(I1(1:100,:),I2(1:100,:));
%   imagesc(I); colormap(gray(256));
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages2(I1(:,1:100),I2(:,1:100));
%   imagesc(I); colormap(gray(256));
%
%   [I, Cdeb, Cfin, Ldeb, Lfin]  = fusionImages2(I1,I2,I3,I4);
%   imagesc(I); colormap(gray(256));
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [I, xCdeb, xCfin, xLdeb, xLfin] = fusionImages2(varargin)

[varargin, Localisation] = getPropertyValue(varargin, 'Localisation', []);
if isempty(Localisation)
    nbArgin = nargin;
    xCdeb{1} = 1;
    xCfin{1} = 1;
    xLdeb{1} = 1;
    xLfin{1} = 1;
else
    nbArgin = nargin - 2;
    xCdeb = Localisation{1};
    xCfin = Localisation{2};
    xLdeb = Localisation{3};
    xLfin = Localisation{4};
end

switch nbArgin
    case 1
        I = varargin{1};
        xCdeb = 1;
        xCfin = size(I,2);
        xLdeb = 1;
        xLfin = size(I,1);
    case 2
        if size(varargin{1}, 1) > size(varargin{1}, 2)
            I = varargin{1};
            I(1:size(varargin{2},1), end+1:end+size(varargin{2},2)) = varargin{2};
            
            Cdeb(1) = 1;
            Cfin(1) = size(varargin{1},2);
            Ldeb(1) = 1;
            Lfin(1) = size(varargin{1},1);
            
            Cdeb(2) = Cfin(1) + 1;
            Cfin(2) = Cfin(1) + size(varargin{2},2); %#ok<NASGU>
            Ldeb(2) = 1;
            Lfin(2) = size(varargin{2},1); %#ok<NASGU>
            
        else
            I = varargin{1};
            I(end+1:end+size(varargin{2},1), 1:size(varargin{2},2)) = varargin{2};
            
            Cdeb(1) = 1;
            Cfin(1) = size(varargin{1},2);
            Ldeb(1) = 1;
            Lfin(1) = size(varargin{1},1);
            
            Cdeb(2) = 1;
            Cfin(2) = size(varargin{2},2);
            Ldeb(2) = Lfin(1) + 1;
            Lfin(2) = Lfin(1) + size(varargin{2},1);
            
            for i=1:length(xCdeb)
                xCdeb{i} = xCdeb{i} + Cdeb - 1; %#ok<AGROW>
                xCfin{i} = xCfin{i} + Cfin - 1; %#ok<AGROW>
                xLdeb{i} = xLdeb{i} + Ldeb - 1; %#ok<AGROW>
                xLfin{i} = xLfin{i} + Lfin - 1; %#ok<AGROW>
            end
            
        end
        for i=1:length(xCdeb)
            xCdeb{i}    = xCdeb{i}   + Cdeb(i) - 1; %#ok<AGROW>
            xCfin{i}    = xCfin{i}   + Cdeb(i) - 1; %#ok<AGROW>
            xLdeb{i}    = xLdeb{i}   + Ldeb(i) - 1; %#ok<AGROW>
            xLfin{i}    = xLfin{i}   + Ldeb(i) - 1; %#ok<AGROW>
        end
    otherwise
        k = 1;
        for i=1:2:nbArgin
            if i== nbArgin
                I{k} = varargin{i}; %#ok<AGROW>
                
                Cdeb{k} = 1; %#ok<AGROW>
                Cfin{k} = size(I{k},2); %#ok<AGROW>
                Ldeb{k} = 1; %#ok<AGROW>
                Lfin{k} = size(I{k},1); %#ok<AGROW>
                
            else
                if size(varargin{i}, 1) > size(varargin{i}, 2)
                    I{k} = varargin{i}; %#ok<AGROW>
                    I{k}(1:size(varargin{i+1},1), end+1:end+size(varargin{i+1},2)) = varargin{i+1}; %#ok<AGROW>
                    
                    Cdeb{k}(1) = 1; %#ok<AGROW>
                    Cfin{k}(1) = size(varargin{1},2); %#ok<AGROW>
                    Ldeb{k}(1) = 1; %#ok<AGROW>
                    Lfin{k}(1) = size(varargin{1},1); %#ok<AGROW>
                    
                    Cdeb{k}(2) = Cfin{k}(1) + 1; %#ok<AGROW>
                    Cfin{k}(2) = Cfin{k}(1) + size(varargin{2},2); %#ok<AGROW>
                    Ldeb{k}(2) = 1; %#ok<AGROW>
                    Lfin{k}(2) = size(varargin{2},1); %#ok<AGROW>
                    
                else
                    J = varargin{i};
                    J(end+1:end+size(varargin{i+1},1), 1:size(varargin{i+1},2)) = varargin{i+1};
                    I{k} = J; %#ok<AGROW>
                    
                    Cdeb{k}(1) = 1; %#ok<AGROW>
                    Cfin{k}(1) = size(varargin{1},2); %#ok<AGROW>
                    Ldeb{k}(1) = 1; %#ok<AGROW>
                    Lfin{k}(1) = size(varargin{1},1); %#ok<AGROW>
                    
                    Cdeb{k}(2) = 1; %#ok<AGROW>
                    Cfin{k}(2) = size(varargin{2},2); %#ok<AGROW>
                    Ldeb{k}(2) = Lfin{k}(1) + 1; %#ok<AGROW>
                    Lfin{k}(2) = Lfin{k}(1) + size(varargin{2},1); %#ok<AGROW>
                    
                end
            end
            k = k+1;
        end
        %    for i=1:length(xCdeb)
        %         xCdeb{i}    = xCdeb{i}   + Cdeb(i) - 1;
        %         xCfin{i}    = xCfin{i}   + Cdeb(i) - 1;
        %         xLdeb{i}    = xLdeb{i}   + Ldeb(i) - 1;
        %         xLfin{i}    = xLfin{i}   + Ldeb(i) - 1;
        %     end
        
        if ~isempty(I)
            [I, xCdeb, xCfin, xLdeb, xLfin] = fusionImages2(I{:}, 'Localisation', {Cdeb, Cfin, Ldeb, Lfin});
        end
end

if iscell(xCdeb)
    xCdeb = [xCdeb{:}];
    xCfin = [xCfin{:}];
    xLdeb = [xLdeb{:}];
    xLfin = [xLfin{:}];
end
