% Description
%   This function is helpful to clen up some data using its histogram. The
%   use can invalidate some bins (generally heads and tails that correspond
%   to outliers). The function delivers the bins that were flagged.
%
% Syntax
%   sub = getFlaggedBinsOfAnHistogram(bins, N)
%
% Input Arguments
%   bins : Bins of the histogram
%   N    : Values of the histogram
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%   N = rand(1,100); bins = linspace(0, 100, length(N));
%   sub = getFlaggedBinsOfAnHistogram(bins, N)
%
% Authors  : JMA
% See also : SignalDialog
%-------------------------------------------------------------------------------

function sub = getFlaggedBinsOfAnHistogram(bins, N, varargin)

% [varargin, Flag] = getPropertyValue(varargin, 'Flag', []);

%     a = cl_data('XData', bins', 'YData', N');
%     set(a, 'Flag', ~p);
%     editobj(a);
%     p = get(a, 'Flag');
%     sub = find(p == 0);

% TODO : voir avec MHO si on peut envoyer cette initialisation du
% nettoyage dans SignaDialog : set(a, 'Flag', ~p);
% Serait nécessaire dans cleanHisto

xSample = XSample('name', 'bins',      'data', bins);
ySample = YSample('name', 'Histogram', 'data', N);
signal  = ClSignal('name', 'Histogram', 'xSample', xSample, 'ySample', ySample);
a = SignalDialog(signal, 'Title', 'Data cleaning based on histogram cleaning', 'CleanInterpolation', false);
a.openDialog();
if a.okPressedOut
	newval = ySample.data;
    sub = find(~isfinite(newval));
else
    sub = [];
end
