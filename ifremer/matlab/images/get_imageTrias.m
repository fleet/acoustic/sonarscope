% Importation d'une image au format TRIAS. Une conversion en .jpg est son
% affichage dans le visualisateur externe sont r�alis�s automatiquement.
%
% Syntax
%   a = get_imageTrias(nomFic)
%
% Input Arguments
%    nomFic : Nom du fichiert Trias
%
% Input Arguments
%    a : [] ou instance de cl_image
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function a = get_imageTrias(nomFic)

I = lecFicGringo(nomFic);

a = cl_image('Image', I, 'ColormapIndex', 2);
