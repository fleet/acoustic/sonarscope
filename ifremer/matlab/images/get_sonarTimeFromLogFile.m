function [flag, NbPings, SplitTime] = get_sonarTimeFromLogFile(NomFicPoints, repImport, repExport)

NbPings   = [];
SplitTime = [];

I0 = cl_image_I0;

[rep, flag] = my_questdlg(Lang('Coupures d�finies par :', 'Cuts defines by :'), ...
    Lang('Nombre max de pings', 'Maximum number of pings'), ...
    Lang('Heures d�finies dans un fichier ASCII', 'Time defined in an ascii file'));
if ~flag
    return
end

if rep == 1
    str1 = 'TODO';
    str2 = 'Split .all files parametrers';
    p = ClParametre('Name', Lang('Nombre max de points','Maximim number of points'), ...
        'Unit', '%',  'MinValue', 100, 'MaxValue', 32767, 'Value', 5000, 'Format', '%d');
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -3 -1 -2 -1 -1 0 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    NbPings = a.getParamsValue;
else
    if exist(NomFicPoints, 'file')
        str{1} = NomFicPoints;
        str{2} = 'Another file';
        str1 = 'Fichier en entr�e';
        str2 = 'Input file';
        [rep, flag] = my_listdlg(Lang(Lang(str1,str2)), str, 'SelectionMode', 'Single');
        if ~flag
            return
        end

        if rep == 2
            nomFic = fullfile(repExport, '*.txt');
            [flag, NomFicPoints] = my_uigetfile(nomFic, 'File name');
            if ~flag
                return
            end
        end
    else
        str1 = 'Nom du fichier';
        str2 = 'File name';
        [flag, NomFicPoints] = my_uigetfile(fullfile(repImport, '*.txt'), Lang(str1,str2));
        if ~flag
            return
        end
    end

    [flag, SplitTime] = decode_SonarTime(I0, NomFicPoints);
    if ~flag || isempty(SplitTime)
        return
    end

    SplitTime = unique(SplitTime);
    str = t2str(SplitTime);
    str = str2cell(str);
    str1 = 'S�lection des points de coupure';
    str2 = 'Select cutting points';
    [rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:length(str));
    if ~flag
        return
    end

    SplitTime = SplitTime(rep);
end
