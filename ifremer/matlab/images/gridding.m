function [J, N] = gridding(X1, Y1, A, Lon, Lat, varargin)

[varargin, NoWaitbar] = getPropertyValue(varargin, 'NoWaitbar', 0);
[varargin, Method]    = getPropertyValue(varargin, 'Method',    'linear'); %#ok<ASGLU>

nLon = length(Lon);
nLat = length(Lat);
subLon = 1:nLon;
subLat = 1:nLat;
J = zeros(nLat, nLon, class(A(1)));
N = zeros(nLat, nLon, class(A(1)));
str1 = 'Maillage';
str2 = 'Gridding';
nA = size(A,1);
hw = create_waitbar(Lang(str1,str2), 'N', nA, 'NoWaitbar', NoWaitbar);
for k=1:nA
    my_waitbar(k, nA, hw)
    x = X1(k,:);
    y = Y1(k,:);
    a = double(A(k,:));
    sub = ~isnan(a) & (y >= Lat(1)) & (y <= Lat(end)) & (x >= Lon(1)) & (x <= Lon(end));
    x = x(sub);
    if ~isempty(x)
        y = y(sub);
        a = a(sub);
        
        ix = interp1(Lon(subLon), subLon, x);
        iy = interp1(Lat(subLat), subLat, y);
        ixInf = floor(ix);
        iyInf = floor(iy);
        ixSup = ixInf+1;
        iySup = iyInf+1;
        
        sub1 = sub2ind(size(J), iyInf, ixInf);
        sub2 = sub2ind(size(J), iyInf, ixSup);
        sub3 = sub2ind(size(J), iySup, ixInf);
        sub4 = sub2ind(size(J), iySup, ixSup);
        
        Deltax = ix - ixInf;
        Deltay = iy - iyInf;
        
        UnMoinsDeltax = (1-Deltax);
        UnMoinsDeltay = (1-Deltay);
        A1 = Deltax        .* Deltay;
        A2 = UnMoinsDeltax .* Deltay;
        A3 = Deltax        .* UnMoinsDeltay;
        A4 = UnMoinsDeltax .* UnMoinsDeltay;
        
        sub = (ixInf >= 1) & (ixInf <= nLon) & (iyInf >= 1) & (iyInf <= nLat);
        sub1 = sub1((sub));
        
        sub = (ixSup >= 1) & (ixSup <= nLon) & (iyInf >= 1) & (iyInf <= nLat);
        sub2 = sub2((sub));
        
        sub = (ixInf >= 1) & (ixInf <= nLon) & (iySup >= 1) & (iySup <= nLat);
        sub3 = sub3((sub));
        
        sub = (ixSup >= 1) & (ixSup <= nLon) & (iySup >= 1) & (iySup <= nLat);
        sub4 = sub4((sub));
        
        switch Method
            case 'nearest' % TODO : toujours pas top pour les cartes marines
                J(sub1) = a;
                % Ou
                [~, pppp] = min([A1;A2;A3;A4]);
                pppp1 = (pppp == 1);
                pppp2 = (pppp == 2);
                pppp3 = (pppp == 3);
                pppp4 = (pppp == 4);
                J(sub1(pppp1)) = a(pppp1);
                J(sub2(pppp2)) = a(pppp2);
                J(sub3(pppp3)) = a(pppp3);
                J(sub4(pppp4)) = a(pppp4);
            case 'linear'
                J(sub1) = J(sub1) + a .* A4;
                N(sub1) = N(sub1) + A4;
                J(sub2) = J(sub2) + a .* A3;
                N(sub2) = N(sub2) + A3;
                J(sub3) = J(sub3) + a .* A2;
                N(sub3) = N(sub3) + A2;
                J(sub4) = J(sub4) + a .* A1;
                N(sub4) = N(sub4) + A1;
            otherwise
                SSc_InvalidMessage(Method, which(mfilename))
        end
    end
end
my_close(hw)

if strcmp(Method, 'linear')
    % sub = (N == 0);
    sub = (N <= 0.1); % pour �viter des artefacts
    J(sub) = NaN;
    J(~sub) = J(~sub) ./ N(~sub);
end
