function [J, N] =  griddingOLD(X1, Y1, A, Lon, Lat)
nLon = length(Lon);
nLat = length(Lat);
subLon = 1:nLon;
subLat = 1:nLat;
J = zeros(nLat, nLon, class(A(1)));
N = zeros(nLat, nLon, class(A(1)));
for i=1:size(A,1)
    x = X1(i,:);
    y = Y1(i,:);
    a = A(i,:);
    sub = ~isnan(a) & (y >= Lat(1)) & (y <= Lat(end)) & (x >= Lon(1)) & (x <= Lon(end));
    x = x(sub);
    if ~isempty(x)
        y = y(sub);
        a = a(sub);
        
        ix = interp1(Lon(subLon), subLon, x, 'nearest');
        iy = interp1(Lat(subLat), subLat, y, 'nearest');
        sub = sub2ind(size(J), iy, ix);
        
        J(sub) = J(sub) + a;
        N(sub) = N(sub) + 1;
    end
end
sub = (N == 0);
J(sub) = NaN;
J(~sub) = J(~sub) ./ N(~sub);
