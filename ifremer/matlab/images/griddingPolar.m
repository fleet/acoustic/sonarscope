% TODO : attention, LayerMask n'est cr�� que si on passe un deuxi�me
% layer : il faut am�liorer �a car ce n'est pas tr�s heureux

function [Layer, LayerMask, N, BeamAngleOut, RangeMeter, BeamIndexOut, RangeSamples, Layer2] = griddingPolar(X1, Y1, BeamIndexIn, SampleIndex, ...
    BeamAngleIn, Reflectivity, RMasque, Across, Depth, Detection, MaskBeyondDetection, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

% [varargin, widthInterpol] = getPropertyValue(varargin, 'widthInterpol', 10);
[varargin, subLayers]     = getPropertyValue(varargin, 'subLayers',     []);
[varargin, otherImage]    = getPropertyValue(varargin, 'otherImage',    []); %#ok<ASGLU>

nLon      = length(Across);
nLat      = length(Depth);
subAcross = 1:nLon;
subDepth  = 1:nLat;
Layer     = zeros(nLat, nLon, class(Reflectivity(1)));

if isempty(RMasque)
    LayerMask = [];
else
    LayerMask = zeros(nLat, nLon, class(RMasque(1)));
end
Layer2 = zeros(nLat, nLon, class(Reflectivity(1)));
N      = zeros(nLat, nLon, 'single');

if any(subLayers == 1)
    BeamAngleOut = NaN(nLat, nLon, 'single');
else
    BeamAngleOut = [];
end
if any(subLayers == 2)
    RangeMeter = NaN(nLat, nLon, 'single');
else
    RangeMeter = [];
end
if any(subLayers == 3)
    BeamIndexOut = NaN(nLat, nLon, 'single');
else
    BeamIndexOut = [];
end
if any(subLayers == 4)
    RangeSamples = NaN(nLat, nLon, 'single');
else
    RangeSamples = [];
end
% if any(subLayers == 5)
%     RxGain = NaN(nLat, nLon, 'single');
% else
%     RxGain = [];
% end


iBeam1 = find(BeamAngleIn > 0, 1, 'first');
if isempty(iBeam1)
    iBeam1 = length(BeamAngleIn);
end
iBeam2 = find(BeamAngleIn < 0, 1, 'last');
if isempty(iBeam2)
    iBeam2 = 1;
end

% warning('off', 'MATLAB:interp1:NaNinY')
nA = size(Reflectivity,2);
for k=1:(iBeam1-1)
    if isnan(Reflectivity(1,k))
        fin = find(~isnan(Reflectivity(:,k)), 1, 'first');
        Reflectivity(1:fin-1,k) = 0;
    end
    
    x = X1(:,k);
    y = Y1(:,k);
    reflec = Reflectivity(:,k);
    
    if isempty(RMasque)
        reflecMasque = [];
    else
        reflecMasque = RMasque(:,k);
    end
    
    if isempty(otherImage)
        reflec2 = [];
    else
        reflec2 = otherImage(:,k);
    end
    
    BeamAngle  = BeamAngleIn(k);
    beamNumber = BeamIndexIn(k);
    
    %  figure(86867); plot(X1(:,k), Y1(:,k), 'k'); grid on; hold on; plot(X1(:,k+1), Y1(:,k+1), 'b'); plot(x, y, 'r'); hold off;
    [Layer, LayerMask, Layer2, N, BeamAngleOut, RangeMeter, BeamIndexOut, RangeSamples] = gridBeam2(x, y, reflec, reflecMasque, reflec2, Layer, LayerMask, Layer2, N, BeamAngleOut, ...
        RangeMeter, BeamIndexOut, RangeSamples, ...
        Across, Depth, BeamAngle, beamNumber, SampleIndex, subAcross, subDepth);
end

for k=iBeam2:(nA-1)
    if isnan(Reflectivity(1,k))
        fin = find(~isnan(Reflectivity(:,k)), 1, 'first');
        Reflectivity(1:fin-1,k) = 0;
    end
    
    x = X1(:,k);
    y = Y1(:,k);
    reflec = Reflectivity(:,k);
    
    if isempty(RMasque)
        reflecMasque = [];
    else
        reflecMasque = RMasque(:,k);
    end
    
    if isempty(otherImage)
        reflec2 = [];
    else
        reflec2 = otherImage(:,k);
    end
    
    BeamAngle  = BeamAngleIn(k);
    beamNumber = BeamIndexIn(k);
    
    % figure(86867); plot(X1(:,k), Y1(:,k), 'k'); grid on; hold on; plot(X1(:,k+1), Y1(:,k+1), 'b'); plot(x, y, 'r'); hold off;
    [Layer, LayerMask, Layer2, N, BeamAngleOut, RangeMeter, BeamIndexOut, RangeSamples] = gridBeam2(x, y, reflec, reflecMasque, reflec2, Layer, LayerMask, Layer2, N, BeamAngleOut, ...
        RangeMeter, BeamIndexOut, RangeSamples, ...
        Across, Depth, BeamAngle, beamNumber, SampleIndex, subAcross, subDepth);
end

sub = (N == 0);
Layer( sub) = NaN;
Layer(~sub) = Layer(~sub) ./ N(~sub);

if ~isempty(LayerMask)
    LayerMask( sub) = NaN;
    LayerMask(~sub) = LayerMask(~sub) ./ N(~sub);
end

if isempty(otherImage)
    Layer2 = [];
else
    Layer2( sub) = NaN;
    Layer2(~sub) = Layer2(~sub) ./ N(~sub);
end
% warning('on', 'MATLAB:interp1:NaNinY')

%% Calcul des indices de la profondeur pour masquage en dessous de la d�tection

% if MaskBeyondDetection % Comment� le 11/09/2017 sinon indDetection pas cr�e pour maskImage
Z = my_interp1(Detection.X, Detection.Z, Across);
indDetection = zeros(1,length(Across));
for k1=1:length(Across)
    if ~isnan(Z(k1))
        [~, indDetection(k1)] = min(abs(Depth - Z(k1)));
    end
end
% end

%% Interpolation horizontale des layers

if ~isempty(Layer)
    Layer = fillNaN_mean(Layer, [5 5]); % Rajout� le 02/08/2012 pour donn�es WC du PP?
    if NUMBER_OF_PROCESSORS == 0
        Layer = interpHorzImage(Layer);
    else
        Layer = interpHorzImage_mexmc(single(Layer), NUMBER_OF_PROCESSORS);
    end
    %     qqqq = interpHorzImageRange(Layer, widthInterpol);
    %     rrrr = interpHorzImageRange_mexmc(Layer, widthInterpol);
    
    %{
figure; imagesc(Across, Depth, Layer); colormap(jet(256)); colorbar; axis xy;
hold on; plot(Detection.X, Detection.Z, 'k')
    %}
    Layer = maskImage(Layer, MaskBeyondDetection, indDetection);
end

if ~isempty(LayerMask)
    LayerMask = fillNaN_mean(LayerMask, [5 5]); % Rajout� le 02/08/2012 pour donn�es WC du PP?
    if NUMBER_OF_PROCESSORS == 0
        LayerMask = interpHorzImage(LayerMask, 'method', 'nearest');
    else
        LayerMask = interpHorzImage_mexmc(single(LayerMask), NUMBER_OF_PROCESSORS, int32(1));
    end
    % 	LayerMask = interpHorzImageRange(LayerMask, widthInterpol, 'method', 'nearest'); % Ajout� le 01/04/2015
    LayerMask = maskImage(LayerMask, 1, indDetection);
end

if ~isempty(BeamAngleOut)
    if NUMBER_OF_PROCESSORS == 0
        BeamAngleOut = interpHorzImage(BeamAngleOut);
    else
        BeamAngleOut = interpHorzImage_mexmc(single(BeamAngleOut), NUMBER_OF_PROCESSORS);
    end
    BeamAngleOut = maskImage(BeamAngleOut, MaskBeyondDetection, indDetection);
end

if ~isempty(RangeMeter)
    if NUMBER_OF_PROCESSORS == 0
        RangeMeter = interpHorzImage(RangeMeter);
    else
        RangeMeter = interpHorzImage_mexmc(single(RangeMeter), NUMBER_OF_PROCESSORS);
    end
    RangeMeter = maskImage(RangeMeter, MaskBeyondDetection, indDetection);
end

if ~isempty(BeamIndexOut)
    if NUMBER_OF_PROCESSORS == 0
        BeamIndexOut = interpHorzImage(BeamIndexOut, 'method', 'nearest');
    else
        BeamIndexOut = interpHorzImage_mexmc(single(BeamIndexOut), NUMBER_OF_PROCESSORS, int32(1));
    end
    BeamIndexOut = maskImage(BeamIndexOut, MaskBeyondDetection, indDetection);
end

if ~isempty(RangeSamples)
    if NUMBER_OF_PROCESSORS == 0
        RangeSamples = interpHorzImage(RangeSamples, 'method', 'nearest');
    else
        RangeSamples = interpHorzImage_mexmc(single(RangeSamples), NUMBER_OF_PROCESSORS, int32(1));
    end
    RangeSamples = maskImage(RangeSamples, MaskBeyondDetection, indDetection);
end

if ~isempty(Layer2)
    Layer2 = fillNaN_mean(Layer2, [5 5]); % Rajout� le 02/08/2012 pour donn�es WC du PP?
    if NUMBER_OF_PROCESSORS == 0
        Layer2 = interpHorzImage(Layer2);
    else
        Layer2 = interpHorzImage_mexmc(single(Layer2), NUMBER_OF_PROCESSORS);
    end
    %     Layer2 = interpHorzImageRange(Layer2, widthInterpol); % Ajout� le 01/04/2015
    Layer2 = maskImage(Layer2, MaskBeyondDetection, indDetection);
end


function [Layer, LayerMask, Layer2, N, BeamAngleOut, RangeMeter, BeamIndexOut, RangeSamples] = gridBeam2(x, y, reflec, reflecMasque, reflec2, Layer, LayerMask, Layer2, N, BeamAngleOut, ...
    RangeMeter, BeamIndexOut, RangeSamples, ...
    Across, Depth, BeamAngle, beamNumber, SampleIndex, subAcross, subDepth)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

% subIndex = ~isnan(reflec) & (y >= Depth(1)) & (y <= Depth(end)) & (x >= Across(1)) & (x <= Across(end));

subNonNaN = ~isnan(x) & ~isnan(y) & ~isnan(reflec);
x = x(subNonNaN);
if isempty(x)
    return
end

y = y(subNonNaN);
reflec = reflec(subNonNaN);

n = length(x);
if n <= 1
    return
end

sub1 = 1:n;
sub2 = sub1;

sub = (y >= Depth(1))  & (y <= Depth(end)) & (x >= Across(1)) & (x <= Across(end));

x = x(sub);
if length(x) <= 1
    return
end
y = y(sub);

reflec = reflec(sub);

if ~isempty(reflecMasque)
    reflecMasque = reflecMasque(subNonNaN);
    subMasqueNonNaN = ~isnan(reflecMasque);
    if NUMBER_OF_PROCESSORS == 0
        if sum(subMasqueNonNaN) > 2
            reflecMasque = interp1(sub1(subMasqueNonNaN), reflecMasque(subMasqueNonNaN), sub2);
        else
            reflecMasque = NaN(size(sub2), 'single');
        end
    end
    reflecMasque = reflecMasque(sub);
end

if ~isempty(reflec2)
    reflec2 = reflec2(subNonNaN);
    reflec2 = reflec2(sub);
end

% if NUMBER_OF_PROCESSORS == 0
%     ix = interp1(Across(subAcross), subAcross, x, 'nearest');
% else
%     ix = interp1Nearest_mex(Across(subAcross), subAcross, double(x));
% end
ydeb = subAcross(1);
xdeb = Across(ydeb);
a = (subAcross(end) - ydeb) / (Across(subAcross(end)) - xdeb);
ix = round(a * (x - xdeb) + ydeb);
if isempty(ix)
    return
end

% whos beamNumber Depth subDepth y NUMBER_OF_PROCESSORS
% TODO : bug sur PC Herv� Bisquay : Error using +, Matrix dimension must
% agree � l'appel de interp1Nearest_mex, m�me bug sans acc�l�ration
% logicielle
% if NUMBER_OF_PROCESSORS == 0
%     iy = interp1(Depth(subDepth), subDepth, y, 'nearest');
% else
%     iy = interp1Nearest_mex(Depth(subDepth), subDepth, double(y));
% end
ydeb = subDepth(1);
xdeb = Depth(ydeb);
a = (subDepth(end) - ydeb) / (Depth(subDepth(end)) - xdeb);
iy = round(a * (y - xdeb) + ydeb);
if isempty(iy)
    return
end

%{
x1 = Depth(subDepth);
y1 = subDepth;
x2 = y;
y2m = interp1(x1, y1, x2, 'nearest');
y2c = interp1Nearest_mex(x1, y1, x2);
save C:\Temp\PourRoger4.mat x1 y1 x2 y2m y2c
figure; plot(x1, y1, 'xk'); grid on; hold on
plot(x2, y2m, '+r')
plot(x2, y2c, 'or')
[min(x1) max(x1)]
[min(x2) max(x2)]
%}

sub = sub2ind(size(Layer), iy, ix);

% % Ancien code : seul le dernier �chantillon arrivant sur un pixel �tait conserv�
% Layer(sub) = Layer(sub) + reflec;
% if ~isempty(LayerMask)
%     LayerMask(sub) = LayerMask(sub) + reflecMasque(:);
% end
% if ~isempty(reflec2)
%     Layer2(sub) = Layer2(sub) + reflec2;
% end
% N(sub) = N(sub) + 1;

% Nouveau code : moyenne de tous les �chantillons qui se supperposent. Modif JMA 27/01/2021

LayerSub      = Layer(sub);
Layer2Sub     = Layer2(sub);
NSub          = N(sub);
LayerMasSub   = LayerMask(sub);
flagLayerMask = ~isempty(LayerMask);
flagReflec2   = ~isempty(reflec2);

% for k=1:length(sub)
% % parfor k=1:length(sub) % Beaucoup plus lent que le for !!!
%     LayerSub(k) = LayerSub(k) + reflec(k);
%     if flagLayerMask
%         LayerMasSub(k) = LayerMasSub(k) + reflecMasque(k);
%     end
%     if flagReflec2
%         Layer2Sub(k) = Layer2Sub(k) + reflec2(k);
%     end
%     NSub(k) = NSub(k) + 1;
% end

LayerSub = LayerSub + reflec;
if flagLayerMask
    LayerMasSub = LayerMasSub + reflecMasque(:);
end
if flagReflec2
    Layer2Sub = Layer2Sub + reflec2(:);
end
NSub = NSub + 1;

Layer(sub) = LayerSub;
N(sub)     = NSub;
if flagReflec2
    Layer2(sub) = Layer2Sub;
end
if flagLayerMask
    LayerMask(sub) = LayerMasSub;
end

if ~isempty(BeamAngleOut)
    BeamAngleOut(sub) = BeamAngle;
end
if ~isempty(BeamIndexOut)
    BeamIndexOut(sub) = beamNumber;
end
if ~isempty(RangeMeter)
    RangeMeter(sub) = linspace(sqrt(x(1)*x(1)+y(1)*y(1)), sqrt(x(end)*x(end)+y(end)*y(end)), length(sub));
end
if ~isempty(RangeSamples)
    %         Resol = Depth(2)-Depth(1);
    %         SampleIndex sub1 sub2
    RangeSamples(sub) = linspace(SampleIndex(1), SampleIndex(end), length(sub));
end


function Layer = maskImage(Layer, MaskBeyondDetection, indDetection)
if MaskBeyondDetection
    for k1=1:length(indDetection)
        Layer(1:(indDetection(k1)-1),k1) = NaN;
    end
end
