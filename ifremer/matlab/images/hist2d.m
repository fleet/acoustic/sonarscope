% Calcul d'un histogramme bidimentionnel
%
% Syntax
%   H = hist2d(X, Y, m, n)
%
% Input Arguments 
%   X : 
%   Y : 
%   m : Nombre de classes en X
%   n : Nombre de classes en Y
%
% Output Arguments 
%   H : Histogramme bidimentionnel
%
% EXAMPLE
%
% See also Authors
% Authors : JMA
% VERSION  : $Id: hist2d.m,v 1.2 2002/06/06 13:35:51 augustin Exp $
%-------------------------------------------------------------------------------

%----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%            - JMA  - creation
%   13/12/00 - EL   - normalisation
%   30/03/01 - JMA - Mise en ref.
%----------------------------------------------------------------------------

function H = hist2d(X, Y, m, n)

X = X(:);
Y = Y(:);
mX = min(X);
MX = max(X);
mY = min(Y);
MY = max(Y);
H = zeros(m,n);

X = ceil((m / (MX - mX))*(X - mX));
% J = find(X<1);
J = (X<1);
X(J) = 1;
% J = find(X>m);
J = (X>m);
X(J) = m;

Y = ceil((n / (MY - mY))*(Y - mY));
% J = find(Y<1);
J = (Y<1);
Y(J) = 1;
% J = find(Y>m);
J = (Y>m);
Y(J) = n;

for i=1:m
    for j=1:n
        H(n - j +1,i) = sum((X ==i) & (Y ==j));
    end
end

