% Egalisation de l'histogramme d'une image.
%
% Syntax
%   [XEqual, mapEqual, sub] = histoeq(x, Histo)
%
% Input Arguments
%   x     : Abscisses de l'histogramme
%   Histo : Histogramme
%
% Name-Value Pair Arguments
%   equal : Terme de rappel vers l'histogramme d'origine
%           0=image d'origine, 1 = image egalisee, 0.5 egalisation
%           partielle
%   map   : Table de couleur utilisee (jet par defaut)
%
% Output Arguments
%   mapEqual : Table de couleur "egalisee"
%   sub      : Table de correspondance des couleurs
%
% Examples
%   nomFic = getNomFicDatabase('sismique.mat');
%   X = loadmat(nomFic, 'nomVar', 'trace1');
%   map = gray;
%   figure; imagesc(X); colorbar; colormap(map); title('Image egalisee');
%
%   [h, x] = histo1D(X);
%
%   [mapEqual, sub] = histoeq(x, h, 'map', map);
%   figure; imagesc(X); colorbar; colormap(mapEqual);
%   title('Image egalisee');
%
%   [mapEqual, sub] = histoeq(x, h, 'map', map, 'equal', 0.5);
%   figure; imagesc(X); colorbar; colormap(mapEqual);
%   title('Image egalisee');
%
% See also imageeq histo1D Authors
% Authors : JMA
% VERSION  : $Id: histoeq.m,v 1.1 2003/09/02 09:53:02 augustin Exp $
%------------------------------------------------------------------------------

function [mapEqual, sub] = histoeq(x, Histo, varargin)

[varargin, coefEqual] = getPropertyValue(varargin, 'equal', 1);
[varargin, map]       = getPropertyValue(varargin, 'map', jet(256)); %#ok<ASGLU>

nbCoul = size(map,1);

HistoCumule = cumsum(Histo);

% ----------------------------------
% Recherche des extremums de l'image

xmin = min(x(:));
xmax = max(x(:));
xReduit = (x - xmin) / (xmax-xmin);

% -------------------------------------
% Calcul de l'histogramme cumule reduit

minHistoCumule = min(HistoCumule);
maxHistoCumule = max(HistoCumule);
HistoReduit = (HistoCumule - minHistoCumule) / (maxHistoCumule-minHistoCumule);

% --------------------------------------
% On pondere le deplacement des couleurs

if ~isempty(coefEqual)
    HistoReduit = HistoReduit + (1 - coefEqual) * (xReduit - HistoReduit);
end

% ---------------------------------------
% Calcul de la Look-Up Table pour l'image

% yIm = HistoReduit * (xmax-xmin) + xmin;

% -----------------------------------------------------
% Calcul de la Look-Up Table pour la table des couleurs

n = size(map,1);
x = n * (1:size(HistoReduit,2)) / size(HistoReduit,2);
yMap = interp1(x, HistoReduit, 1:n, 'linear', 'extrap');
% xMap = (0:(n-1)) / n;

sub = 1+(yMap * (nbCoul-1));
sub(sub < 1) = 1;
sub(sub > nbCoul) = nbCoul;
mapEqual = zeros(n,3);
mapEqual(:,1) = interp1(1:n, map(:,1), sub, 'linear')';
mapEqual(:,2) = interp1(1:n, map(:,2), sub, 'linear')';
mapEqual(:,3) = interp1(1:n, map(:,3), sub, 'linear')';

sub = round(sub);
