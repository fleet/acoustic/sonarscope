% Egalisation de l'histogramme d'une image.
%
% Syntax
%   [XEqual, mapEqual, sub] = imageeq(X)
%
% Input Arguments
%   X : image a egaliser
%
% Name-Value Pair Arguments
%   colorTable : Table de couleur utilisee (jet par defaut)
%   equal      : Terme de rappel vers l'histogramme d'origine
%                0=image d'origine, 1 = image egalisee, 0.5 egalisation
%                partielle
%
% Output Arguments
%   []          : Auto-plot activation
%   XEqual      : image egalisee
%   mapEqualmap : Table de couleur "egalisee"
%   sub         : Table de correspondance des couleurs
%
% Examples
%   nomFic = getNomFicDatabase('sismique.mat');
%   X = loadmat(nomFic, 'nomVar', 'trace1');
%
%   figure; imagesc(X); colorbar; colormap(gray)
%   title('Image originale avec la table des couleurs originale');
%
%   [XEqual, mapEqual, sub] = imageeq(X, 'colorTable', gray);
%
%   figure; imagesc(X); colorbar; colormap(mapEqual);
%   title('Image originale avec la table des couleurs modifiee');
%
%   figure; imagesc(XEqual); colorbar; colormap(gray);
%   title('Image modifiee avec la table des couleurs originale');
%
%   imageeq(X);
%   imageeq(X, 'colorTable', gray, 'equal', 0.5);
%
% See also histo1D Authors
% Authors : JMA
%------------------------------------------------------------------------------

function varargout = imageeq(X, varargin)

[varargin, coefEqual]  = getPropertyValue(varargin, 'equal',      1);
[varargin, colorTable] = getPropertyValue(varargin, 'colorTable', jet(256)); %#ok<ASGLU>

if ~isa(X, 'double')
    X = double(X);
end

% Recherche des extremums de l'image
% ----------------------------------

xmin = min(X(:));
xmax = max(X(:));

% -----------------------
% Calcul de l'histogramme

[y, x] = histo1D(X, 'Cumul');

% -------------------------------------
% Calcul de l'histogramme cumule reduit

ymin = min(y);
ymax = max(y);
y = (y - ymin) / (ymax-ymin);
xequal = (x - xmin) / (xmax-xmin);

% ---------------------------------------
% On pondere le deplacement des couleurs

if ~isempty(coefEqual)
    y = y + (1 - coefEqual) * (xequal - y);
end

% ---------------------------------------
% Calcul de la Look-Up Table pour l'image

yIm = y * (xmax-xmin) + xmin;

% ---------------------------------------------------------------
% Precaution a prendre pour les non-valeurs
% (Valide si et seulement si la donnee ne peut pas contenir de 0)
% (Il vaudrait mieux trouver une autre solution)

X(isnan(X)) = 0;

% --------------------------
% Calcul de l'image egalisee

XEqual = interp1(x, yIm, X, 'linear');

% -----------------------------------------------------
% Calcul de la Look-Up Table pour la table des couleurs

n = size(colorTable,1);
x = n * (1:size(y,2)) / size(y,2);
yCT = interp1(x, y, 1:n, 'linear');
% xCT = (0:(n-1)) / n;

sub = 1+(yCT * 63);
sub(sub < 1) = 1;
sub(sub > 64) = 64;
mapEqual = zeros(n,3);
mapEqual(:,1) = interp1(1:n, colorTable(:,1), sub, 'linear')';
mapEqual(:,2) = interp1(1:n, colorTable(:,2), sub, 'linear')';
mapEqual(:,3) = interp1(1:n, colorTable(:,3), sub, 'linear')';


if nargout == 0
    figure; imagesc(X); colorbar; colormap(colorTable)
    title('Image originale avec la table des couleurs originale'); drawnow;
    
    % Affichage de l'image avec la table des couleurs modifiee
    figure; imagesc(X); colorbar; colormap(mapEqual);
    title('Image originale avec la table des couleurs modifiee'); drawnow;
    
    % Affichage de l'image modifiee avec la table des couleurs originale
    figure; imagesc(XEqual); colorbar; colormap(colorTable);
    title('Image modifiee avec la table des couleurs originale'); drawnow;
    
    histo1D(X); title('Histogramme image originale')
    histo1D(XEqual); title(['Histogramme image egalisee equal=' num2str(coefEqual)])
else
    varargout{1} = XEqual;
    varargout{2} = mapEqual;
    varargout{3} = round(sub);
end
