%  imagesc_scroll(Lena)

function imagesc_scroll(varargin)

%% Create a scroll panel.
  
hFig = figure;
hIm = imagesc(varargin{:});

%{
Warning: IMSCROLLPANEL currently requires default XData and YData.
HIM has non-default XData, resetting to [1 5097].
HIM has non-default YData, resetting to [1 1260]. 
> In imscrollpanel>validateXYData at 1408
  In imscrollpanel at 236
  In imagesc_scroll at 9
% TODO : ce truc n'accepte que les XDAta=1:length(x) et YDAta=1:length(y),
% il change nos valeutrs et �a fout la merde dans cl_image/imagesc
% instruction "axis([YLim XLim])"
%} 
  
hSP = imscrollpanel(hFig, hIm);
set(hSP,'Units','normalized', 'Position',[0 .1 1 .9])

%% Add a Magnification Box and an Overview tool.

hMagBox = immagbox(hFig,hIm);
pos = get(hMagBox, 'Position');
posFig = get(hFig, 'Position');
set(hMagBox, 'Position', [posFig(3)-pos(3) 0 pos(3) pos(4)])
imoverview(hIm)

set(hFig, 'Toolbar', 'figure', 'Menubar', 'figure')

%% Get the scroll panel API to programmatically control the view.

api = iptgetapi(hSP);

%% Get the current magnification and position.

mag = api.getMagnification();
r = api.getVisibleImageRect();

impixelinfo;

return



%% View the top left corner of the image.

api.setVisibleLocation(0.5,0.5)

%% Change the magnification to the value that just fits.

api.setMagnification(api.findFitMag())

%% Zoom in to 1600% on the dark spot.

api.setMagnificationAndCenter(16,306,800)
