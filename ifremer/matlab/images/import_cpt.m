% Read a GMT colormap file (.cpt)
%
% Syntax
%  [[flag, map, CLim, Deb, Fin, B, F, N] = import_cpt(nomFic, ...)
%
% Input Arguments
%   nomFic : CPT file name
%
% Name-Value Pair Arguments
%   NColors     : Number of colors
%   CLim        :
%   AskQuestion :
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   map  : Colormap
%   CLim :
%   Deb  :
%   Fin  :
%   B    :
%   F    :
%   N    :
%
% Examples
%   nomFic = getNomFicDatabase('DEM_screen.cpt');
%   [flag, map] = import_cpt(nomFic, 'NColors', 256)
%
% See also export_cpt Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, map, CLim, Deb, Fin, B, F, N] = import_cpt(nomFic, varargin)

[varargin, NColors]     = getPropertyValue(varargin, 'NColors', []);
[varargin, CLimOrig]    = getPropertyValue(varargin, 'CLim', []);
[varargin, AskQuestion] = getPropertyValue(varargin, 'AskQuestion', 1); %#ok<ASGLU>

map = [];
bornes = [];
Deb = [];
Fin = [];

B   = [];
F   = [];
N   = [];

fid = fopen(nomFic);
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = -1;
    return
end

TypeDef = 0;
InHSV = 0;
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    if isempty(tline)
        continue
    end
    if strcmp(tline(1), '#')
        if contains(tline, 'COLOR_MODEL')
            if contains(tline, 'HSV')
                InHSV = 1;
            end
        end
        continue
    end
    
    if strcmp(tline(1), 'B')
        B = sscanf(tline(2:end), '%f', 8);
    elseif strcmp(tline(1), 'F')
        F = sscanf(tline(2:end), '%f', 8);
    elseif strcmp(tline(1), 'N')
        N = sscanf(tline(2:end), '%f', 8);
    else
        X = sscanf(tline, '%f', 8);
        %         Deb(end+1,:) = X(1); %#ok<AGROW>
        %         map(end+1,:) = X(2:4); %#ok<AGROW>
        %         Fin(end+1,:) = X(5); %#ok<AGROW>
        %         map(end+1,:) = X(6:8); %#ok<AGROW>
        
        bornes(end+1,:) = X(1); %#ok<AGROW>
        map(end+1,:)    = X(2:4); %#ok<AGROW>
        %         bornes(ind+1,:) = X(5); %#ok<AGROW>
        %         map(ind+1,:)    = X(6:8); %#ok<AGROW>
        TypeDef = 1;
    end
end
fclose(fid);
flag = 1;

if TypeDef
    bornes(end+1,:) = X(5);
    map(end+1,:)    = X(6:8);
end

%% mise en ordre des bornes pour eviter les diff nulles

for k=3:2:length(bornes)
    if bornes(k) == bornes(k-1)
        bornes(k) = bornes(k) + 1e-3; %#ok<AGROW>
    end
end

KeepBounds = 0;
% KeepNaNs = 0;

%% Prise en compte des bornes du fichier

if AskQuestion
    [rep, validation] = my_questdlg(Lang(...
        sprintf('Voulez-vous conserver les bornes du fichier CPT ([%f : %f])', min(bornes), max(bornes)),...
        sprintf('Keep the CPT colormap limits ([%f : %f])', min(bornes), max(bornes))));
    if validation && rep ==1
        KeepBounds = 1;
        CLim    = [min(bornes), max(bornes)];
    else
        CLim = CLimOrig;
        %NColors = min(size(map,1), NColors);
    end
else
    CLim = CLimOrig;
end


% %% Prise en compte des non-valeurs
% if ~isempty(N)
%     [rep, validation] = my_questdlg(Lang(...
%         sprintf('Voulez-vous conserver la couleur pour les "non-valeurs" ([%d %d %d])', N),...
%         sprintf('Keep the CPT defined color for NaNs ([%d %d %d])', N)));
%     if validation && rep ==1
%         KeepNaNs = 1;
%         CLim    = [min(Deb), max(Fin)];  % on augmente les bornes � la limite de la pr�cision machine
%         %NColors = min(size(map,1), NColors);
%     else
%         CLim = CLimOrig;
%         %NColors = min(size(map,1), NColors);
%     end
% else
%     CLim = CLimOrig;
%     %NColors = min(size(map,1), NColors);
% end

%% if InHSV
% figure; rgbplot(map); grid on; title(nomFic)
%     if max(map(:)) > 1
%         map = map / 256;
%     end
%     map(map > 1) = 1;
% end



%%

if InHSV
    map = hsv2rgb(map);
else
    maxmap = max(map(:));
    if maxmap > 1
        map = map / 256;
    end
    maxmap = max(map(:));
    if maxmap > 1
        map = map / maxmap;
    end
end

if isempty(NColors)
    %     NColors = min(size(map,1), 256);
    c = map;
else
    if KeepBounds
        % Interpolation des couleurs suivant les bornes comprises dans le
        % fichier CPT
        %sz1 = size(map,1);
        c = zeros(NColors,3);
        for k=1:3
            c(:,k) = interp1(bornes, map(:,k), linspace(bornes(1), bornes(end), NColors));
        end
    else
        
        sz1 = size(map,1);
        c = zeros(NColors,3);
        for k=1:3
            c(:,k) = interp1(1:sz1, map(:,k), linspace(1, sz1, NColors));
        end
        %     Deb = interp1(1:length(Deb), Deb, linspace(1, sz1, NColors-1));
        %     Fin  = interp1(1:length(Fin), Fin, linspace(1, sz1, NColors-1));
    end
end

map = c;
