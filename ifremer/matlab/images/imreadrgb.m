% Lecture d'une image avec reduction automatique des couleurs.
%
% Syntax
%   rgb = imreadrgb( nom, ... )
%
% Input Arguments
%   nom : Nom du fichier image
%
% PROPERTY NAMES :
%   Niveaux : Nombre de niveaux (64 par defaut)
%
% Output Arguments
%   rgb : image en RGB.
%
% Examples
%   nomFic = getNomFicDatabase('logoIfremer.tif');
%   rgb = imreadrgb(nomFic);
%   imshow(rgb);
%   rgb = imreadrgb(nomFic, 'Niveaux', 16);
%   imshow(rgb);
%
% See also imread rgb2ind ind2rgb FicImageMap2FicImageRGB Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function rgb = imreadrgb(nom, varargin)

[varargin, Niveaux] = getPropertyValue(varargin, 'Niveaux', 64); %#ok<ASGLU>

%% Lecture de l'image dans le fichier

[image, map] = imread(nom);

%% Si l'image est une image indexee, on la transforme en RGB

if( size(image,3) == 1)
    image = ind2rgb(image, map);
end

%% Transformation de l'image RGB en image indexee en reduisant le nombre de couleurs

[x, map] = rgb2ind(image, colorcube(Niveaux));

%% Transformation de l'image indexee en RGB

rgb = ind2rgb(x, map);
