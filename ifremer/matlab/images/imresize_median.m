%   X = Lena;
%   Y = imresize_median(X, size(X)/2);

function Y = imresize_median(X, newSize)

    function y = medianWithoutNaN(x)
        y = median(x, 'omitnan');
    end

c = class(X(1));
switch c
    case {'single'; 'double'}
        Y = NaN(newSize, class(X(1)));
    otherwise
        Y = zeros(newSize, class(X(1)));
end

oldSize = size(X);
step = (oldSize ./ newSize);

if isequal(step, floor(step))
    z = colfilt(X, step, [1024 1024], 'sliding', @medianWithoutNaN);
    Y = z(1:step(1):end,1:step(1):end);
elseif (step(1) < 1) && (step(2) < 1)
    k2y = 1;
    for k2=1:step(2):(oldSize(2)-step(2))
        k1y = 1;
        sub2 = floor(k2:(k2+step(1)));
        for k1=1:step(1):(oldSize(1)-step(1))
            sub1 = floor(k1:(k1+step(1)));
            x = X(sub1, sub2);
            Y(k1y,k2y) = x;
            k1y = k1y + 1;
        end
        k2y = k2y + 1;
    end
else
    step = floor(step);
    k2y = 1;
    for k2=1:step(2):(oldSize(2)-step(2))
        k1y = 1;
        sub2 = floor(k2:(k2+step(1)));
        for k1=1:step(1):(oldSize(1)-step(1))
            sub1 = floor(k1:(k1+step(1)));
            x = X(sub1, sub2);
            x = median(x(:), 'omitnan');
            Y(k1y,k2y) = x;
            k1y = k1y + 1;
        end
        k2y = k2y + 1;
    end
end
end
