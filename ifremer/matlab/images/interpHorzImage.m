%   Interpolates NaN values along vertical dimension
%  
% Syntax
%     Y = interpHorzImage(X, varargin)
%  
% Input Arguments
%     X      : Matrix containing the image with missing values
%  
% Output Arguments
%     Y : Matrix interpolated
%  
% Examples
%     s = mod(0:2:1800, 360);
%     X = repmat(s, length(s), 1);
%     X(100:250,100:250) = NaN;
%     N = floor(length(s)^2 / 2);
%     d = randi(length(s), [N 2]);
%     for k=1:size(d,1)
%         X(d(k,2),d(k,1)) = NaN;
%     end
%     figure; imagesc(X)
%  
%     Y = interpHorzImage(X, 'linear');
%     figure; imagesc(Y)
%  
% interpHorzImage.m fillNaN_heading cl_image/WinFillNaN Authors
% Authors  : JMA
%   ----------------------------------------------------------------------------

function Y = interpHorzImage(X, varargin)

[varargin, method] = getPropertyValue(varargin, 'method', 'linear'); %#ok<ASGLU>

Y = X;
for k=1:size(X,1)
    x = X(k,:);
    subNonNaN = find(~isnan(x));
    if (length(subNonNaN) > 4) %% TODO : JMA : A faire valider par Carla && (length(subNonNaN) < 50)
        kdeb = subNonNaN(1);
        kfin = subNonNaN(end);
        subNaN = find(isnan(x(kdeb+1:kfin-2)));
        subNaN = subNaN + kdeb;
        Y(k,subNaN) = interp1(subNonNaN, x(subNonNaN), subNaN, method);
    end
end
% SonarScope(Y)
