%   Interpolates NaN values along vertical dimension
%  
% Syntax
%     Y = interpHorzImageRange(X, varargin)
%  
% Input Arguments
%     X      : Matrix containing the image with missing values
%  
% Output Arguments
%     Y : Matrix interpolated
%  
% Examples
%     s = mod(0:2:1800, 360);
%     X = repmat(s, length(s), 1);
%     X(100:250,100:250) = NaN;
%     N = floor(length(s)^2 / 2);
%     d = randi(length(s), [N 2]);
%     for k=1:size(d,1)
%         X(d(k,2),d(k,1)) = NaN;
%     end
%     figure; imagesc(X)
%  
%     Y = interpHorzImageRange(X, 5);
%     figure; imagesc(Y)
%  
% See also fillNaN_heading cl_image/WinFillNaN Authors
% Authors  : JMA
%   ---------------------------------------------------------------------------- 

function Y = interpHorzImageRange(X, range, varargin)

[varargin, method] = getPropertyValue(varargin, 'method', 'linear'); %#ok<ASGLU>

SE = true(1,1+2*range);
Y = X;
for k=1:size(X,1)
    x = X(k,:);
    
    subNaN = isnan(x);
%     subFar = imopen(isnan(x), SE);
    subFar = imdilate(isnan(x), SE);
    subNaN(subFar) = 0;
    subNaN = find(subNaN);
    
    subNonNaN = find(~isnan(x));
    if length(subNonNaN) > 1
        Y(k,subNaN) = interp1(subNonNaN, x(subNonNaN), subNaN, method);
    end
end
% SonarScope(Y)
