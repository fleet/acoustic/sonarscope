function rep = isGeotiff(nomFic)

try
    info = geotiffinfo(nomFic);
    names = fieldnames(info.GeoTIFFCodes);
    empty = logical(size(names));
    for k=1:numel(names)
        empty(k) = isempty(info.GeoTIFFCodes.(names{k}));
    end
    if all(empty)
        warning(Lang(sprintf('Le fichier "%s" contient des balises GeoTIFF, mais elles sont VIDES !!!', nomFic),...
                sprintf('File "%s" contains GeoTIFF tags, but they are all EMPTY !!!', nomFic)));
        rep = 0;
    else
        rep = 1;
    end
catch ME
%     disp(ME.message); %% Comment� par JMA le 26/03/2014 car plein de messages inutiles lors de lectures d'images SCAMPI
    rep = 0;
end