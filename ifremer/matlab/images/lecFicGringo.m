% Lecture d'une image au format Gringo
%
% Syntax
%   I = lecFicGringo( nomFic )
%
% Input Arguments 
%   nomFic : Nom du fichier image au format Gringo
%
% Output Arguments
%   I : image.
% 
% Examples
%   nomFic = '/home6/uranie/SAME/bruno/bruno01.imo';
%   I1 = lecFicGringo(nomFic);
%   % imagesc(I1); colormap(flipud(gray)); colorbar; truesize
%   imwrite(I1, '/home6/uranie/SAME/bruno/bruno01.jpg', 'jpg');
%
%   nomFic = '/home1/ocean/RECUPTAHITI/Edrezen/Boite2/hudsar1/z1/mos_z101.imo';
%   I2 = lecFicGringo(nomFic);
%   % imagesc(I2); colormap(flipud(gray)); colorbar; truesize
%   imwrite(I2, '/tmp/mos_z101.jpg', 'jpg');
%
% See also imwrite Authors
% Authors : JMA
% VERSION  : $Id: lecFicGringo.m,v 1.2 2002/06/06 13:35:51 augustin Exp $
%-------------------------------------------------------------------------------

function I = lecFicGringo( nomFic )
  
% fid = fopen(nomFic, 'r'); %, 'l');
fid = fopen(nomFic, 'r', 'b');
if fid == -1
	disp('Fichier non trouve')
	I = [];
	return
end
entete = fread(fid, 64, 'int16');
% entete'
nbCol = entete(38);
nbRows = entete(36);
fseek(fid, nbCol, 'bof');
nbCol = ceil(nbCol / 4) * 4;

I = fread(fid, [nbCol nbRows], 'uint8=>uint8');
fclose(fid);
