% Lecture d'une table de couleur definie par le CNES
%
% Syntax
%   table = lecTableCNES(nomFic)
% 
% Input Arguments 
%   nomFic : Nom du fichier CNES
%
% Name-Value Pair Arguments 
%   nbCoul : Nombre de couleurs (table CNES par defaut)
%
% Output Arguments 
%   Table : Table de couleur 
%
% Examples
%   nomFic = getNomFicDatabase('chemin256_1024_150_60.byt');
%   nomFic = getNomFicDatabase('chemin64_256_180_60_6.byt');
%   nomFic = getNomFicDatabase('chemin256_2048_180_60_6.byt');
%
%   table = lecTableCNES(nomFic);
%   imagesc(rand(20)); colorbar;
%   colormap(table)
%
% See also ColorSeaEarth1 ColorSeaEarth2 colormap Authors
% Authors : JMA
% VERSION  : $Id: lecTableCNES.m,v 1.5 2002/06/06 13:35:51 augustin Exp $
% -------------------------------------------------------------------------------
 
function table = lecTableCNES(nomFic, varargin)

fid = fopen(nomFic, 'r', 'b');
table = fread(fid, [3 inf], 'uint8');
fclose(fid);
table = table';
if any(table(1,:))
    table = flipud(table);
end

maxTable = max(table(:));
table = table / maxTable;

if nargin == 2
    nbCoulIn = size(table, 1);
    nbCoulOut = varargin{1};
    tableOut = zeros(nbCoulOut, 3);
    for i=1:3
        pppp = interp1(1:nbCoulIn, table(:,i), linspace(1, nbCoulIn, nbCoulOut));
        tableOut(:,i) = pppp';
    end
    table = tableOut;
end