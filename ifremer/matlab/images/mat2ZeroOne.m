function I = mat2ZeroOne(X, CLim)

if ~isa(X, 'double')
    X = single(X);
end
CLim = double(CLim);

I = (X - CLim(1)) / diff(CLim);
%     sub = find(I < 0);
sub = (I < 0);
I(sub) = 0;
%     sub = find(I > 1);
sub = (I > 1);
I(sub) = 1;
