% Conversion d'une image donn�e dans une matrice en image 8 bits
% avec possibilit� de rehaussement de contraste automatique
% lors de la conversion
%
% Syntax
%   im8 = mat2image(im)
%   im8 = mat2image(im, low )
%   im8 = mat2image(im, low, high )
%
% Input Arguments
%   im		: matrice contenant l'image
%   low		: valeur qui sera ramenee a 0
%   high	: valeur qui sera ramenee a 1
%             low et high peuvent etre donnes dans l'unite de l'image
%             ou en pourcentage de saturation : Ex : 5%
% 
% Examples 
%   imo8 = mat2image( rand(50, 50) );				% Equivalent a imo8 = uint8(rand(50, 50));
%   imo8 = mat2image( rand(50, 50), 0.4, 0.6 );		% Rehaussement lineaire entre valeurs
%   imo8 = mat2image( rand(50, 50), '25%', '75%' );	% Rehaussement lineaire automatique
%
% See also stats LecFicLayerCaraibes Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function im8 = mat2image(im, low, high)

if nargin == 1
    low = min(im(:));
    high = max(im(:));
end

if ischar(low) || ischar(high)
    xmin = min(im(:));
    xmax = max(im(:));

    xpas    = (xmax - xmin) / 100;
    x       = xmin:xpas:xmax;
    h       = cumsum(my_hist(im(:), x));   
    h       = h / h(end);
end

if ischar(low)
    low = sscanf(low,'%f') / 100.;
    i = find(h < low);
    low = x(i(end));
end

fprintf('Borne min du rehaussement : %f\n', low);

if ischar(high)
    high = sscanf(high,'%f') / 100.;
    i = find(h < high);
    high = x(i(end));
end

fprintf('Borne max du rehaussement : %f\n', high);

im8 = (im - low) / (high - low);
%     sub = find(im8 < 0);
sub = (im8 < 0);
im8(sub) = 0;
%     sub = find(im8 > 1);
sub = (im8 > 1);
im8(sub) = 1;

im8 = uint8((1. - im8) * 255);
