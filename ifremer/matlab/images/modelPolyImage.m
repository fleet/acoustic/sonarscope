function [flag, cor, C2] = modelPolyImage(I, x, y, ValNaN, pX, pY, varargin)

[varargin, ValLim]        = getPropertyValue(varargin, 'ValLim', []);
[varargin, SeuilCompPoly] = getPropertyValue(varargin, 'SeuilCompPoly', []); %#ok<ASGLU>

if ~isempty(ValLim)
    I(I < ValLim(1)) = ValNaN;
    I(I > ValLim(2)) = ValNaN;
end

cor = [];
C2  = [];

[I, subNaN] = windowEnlarge(I, [0 0], ValNaN);
I(subNaN) = NaN;

if ~isa(I, 'double')
    I = single(I);
end
J = I;

% D�but Modif pour diminuer des pb num�riques rencontr�s sur les images OTUS ou X et Y varient de 1 � 1024
%         XData = this.x(subx);
%         YData = this.y(suby);
XData = linspace(0, 1, length(x));
YData = linspace(0, 1, length(y));
% Fin Modif pour diminuer des pb num�riques rencontr�s sur les images OTUS ou X et Y varient de 1 � 1024

X = XData;
Y = YData;

flag = -1;
while flag == -1
    if numel(J) > 1e6
        J = impyramid(J, 'reduce');
        X = impyramid(X, 'reduce');
        Y = impyramid(Y, 'reduce');
    else
        flag = 1;
    end
end

flag = -1;
while flag == -1
    try
        [flag, C] = polyfitSurf(X, Y, J, 'pX', pX, 'pY', pY);
    catch %#ok<CTCH>
        flag = -1;
    end
    if flag == -1
        str1 = 'L''image est trop grosse pour faire cette operation, SonarScope va r�duire le nombre de points intervenant dans ce calcul par �chantillonnage et va retenter l''op�ration.';
        str2 = 'This data is too big to be processed, SonarScope is going to subsample it and is going to retry the processing, be patient ...';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'polyfitSurfReductionImage');
        
        J = impyramid(J, 'reduce');
        
        X = impyramid(X, 'reduce');
        if  length(X) <= pX
            flag = 0;
            return
        end
        Y = impyramid(Y, 'reduce');
        if  length(Y) <= pY
            flag = 0;
            return
        end
    end
end

cor = polyvalSurf(C, XData, YData, 'Type', class(I), '2D');
%             figure; imagesc(x, y, cor); colorbar;


% Test pour donner l'�quation : on recalcule le polynome sur les
% vraies coordonn�es en se basant sur le polynome et en se limitant
% � un faible nombre de points
nx = length(x);
ny = length(y);
sx = 1:max(1,floor(nx/8)):nx;
sy = 1:max(1,floor(ny/8)):ny;
[flag, C2] = polyfitSurf(x(sx), y(sy), cor(sy,sx), 'pX', pX, 'pY', pY);

%% Test de limitation de la correction si division

if ~isempty(SeuilCompPoly)
    sub = (cor < SeuilCompPoly);
    
    if sum(sub(:)) < (nx*ny/2)
        I(sub) = NaN;
        [flag, cor, C2] = modelPolyImage(I, XData, YData, ValNaN, pX, pY);
        
        N = 4;
        cor(sub) = (N*SeuilCompPoly + cor(sub)) / (N+1);
    end
end

