% fonction de selection du chevauchement lors du mozaiquage
% Compensation d'une image
%
% Syntax
%   im_sortie = profilsOverlapGeometrie(za, zb)
%
% Input Arguments
%   za     : 
%   zb     : 
%   ValNaN : 
%
% Output Arguments
%   im_sortie : 
%
% Examples 
%
%
% See also cl_image Authors
% Authors : CB
% VERSION  : $Id: profilsOverlapGeometrie.m,v 1.1 2003/08/28 14:49:35 augustin Exp augustin $
% ----------------------------------------------------------------------------

function im_sortie = montageProfils(za, zb)

% figure(1); imagesc(za); title('za')
% figure(2); imagesc(zb); title('zb')
%     display('taille de za et zb');
%     size(za)
%     size(zb)


index_a = ~isnan(za);
index_b = ~isnan(zb);

% figure(3); imagesc(index_a); title('index_a')
% figure(4); imagesc(index_b); title('index_b')

% figure;
% imagesc(index_a);

im = index_a & index_b;
im = double(im);

%figure; imagesc(im); title('im');

%reduction de la taille de la matrice a parcourir
stats = regionprops(im, 'BoundingBox');
% stats = my_imfeature(im, 'BoundingBox');

if isempty(stats)
    im_sortie = zb;
    return
end

mat_corners = round(stats.BoundingBox);

im_2 = im(mat_corners(2):(mat_corners(2) + mat_corners(4) - 1) , mat_corners(1):(mat_corners(1) + mat_corners(3)) - 1);
%     display('detail du rectangle de im selectionne');
%     mat_corners(2)
%     (mat_corners(2) + mat_corners(4) - 1)
%     mat_corners(1)
%     (mat_corners(2) + mat_corners(3)) - 1


%figure; imagesc(im_2); title('im_2')

%parcourir des colonnes et separation en deux zones de meme longueur
[ll_3,col_3] = size(im_2);
im_3 = zeros(size(im_2));
milieu = zeros(1,col_3);

for j=1:col_3
    %j
    [u,v] = find(im_2(:,j) == 1); %#ok
    if ~isempty(u)
        milieu(j) = round(0.5*(u(1)+u(end)));
        im_3(milieu(j):ll_3,j) = ones((size((milieu(j):ll_3),2)),1);
    end
end

%     figure;
%     imagesc(im_3);

im_res_ptt_low  = im_2 & im_3;
im_res_ptt_high = im_2 & ~im_3;

%     display('taille des sous_umage apres selection des deux ss fauchees')
%     size(im_res_ptt_low)
%     size(im_res_ptt_high)
%
%     figure(7); imagesc(im_res_ptt_low);  title('im_res_ptt_low')
%     figure(8); imagesc(im_res_ptt_high); title('im_res_ptt_high')

%retrouver la matrice initiale en remplissant
im_fin_low  = zeros(size(im));
im_fin_high = zeros(size(im));
im_fin_low( mat_corners(2):(mat_corners(2) + mat_corners(4) - 1) , mat_corners(1):(mat_corners(1) + mat_corners(3) - 1)) = im_res_ptt_low;
im_fin_high(mat_corners(2):(mat_corners(2) + mat_corners(4) - 1) , mat_corners(1):(mat_corners(1)+  mat_corners(3) - 1)) = im_res_ptt_high;


% figure(9);  imagesc(im_fin_low); title('im_fin_low')
% figure(10); imagesc(im_fin_high); title('im_fin_high')

index_im_low  = index_a & ~im_fin_high;
index_im_high = index_b & ~im_fin_low;


im_sortie = za;
im_sortie(index_b) = zb(index_b);
im_sortie(index_im_low)  = za(index_im_low);
im_sortie(index_im_high) = zb(index_im_high);


%     display('taille de im_sortie');
%     size(im_sortie)
% figure(11); imagesc(im_sortie); title('im_sortie')
