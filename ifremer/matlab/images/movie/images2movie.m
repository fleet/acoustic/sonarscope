% Construction d'un film a partir des images mosaiques
%
% Syntax
%   film = images2movie(images)
%
% Input Arguments
%   images   : Images individuelles
%   varargin
%
% Name-only Arguments
%   Colormap : Table de couleurs a utiliser (defaut : jet)
%   Clim     : Bornes de regaussement de couleur (defaut : limites de chaque image)
%   SizeFig  : Taille de la figure
%
% Output Arguments
%   film : film matlab
%
% Examples
%   images = rand(100,100,10);
%   Film = images2movie(images)
%   my_implay(Film); %, 'Nb', 10, 'Freq', 2 );
%
% See also Authors
% Authors : JMA
%-----------------------------------------------------------------------------------

function film = images2movie(images, varargin)

[varargin, Colormap] = getPropertyValue(varargin, 'Colormap', jet(256));
[varargin, Clim]     = getPropertyValue(varargin, 'Clim',     [min(images(:)) max(images(:))]);

imagesc(images(:,:,1)); colormap(Colormap); axis xy; truesize;
Position = get(gcf, 'Position');

[varargin, SizeFig] = getPropertyValue(varargin, 'SizeFig', Position(3:4)); %#ok<ASGLU>

ScreenSize = get(0, 'ScreenSize');
gauche = (ScreenSize(3) -SizeFig(1) ) / 2;
haut   = (ScreenSize(4) -SizeFig(2) ) / 2;

nbImages = size(images, 3);
film = moviein(nbImages);
for i=1:nbImages
    I = squeeze(images(:,:,i));
    imagesc(I, Clim); colormap(Colormap); axis tight;
    set(gcf, 'Position', [gauche haut SizeFig]);
    film(:,i) = getframe;
end
