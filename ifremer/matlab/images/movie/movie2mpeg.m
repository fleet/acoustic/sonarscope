% Sauvegarde d'un film matlab en fichier MPEG.
% Cette fonction active le logiciel "imagemagik".
%
% SYNTAX
%   movie2mpeg( film, nomFicMpeg )
%
% Input Arguments
%   film       : Film matlab
%   nomFicMpeg : Nom du fichier mpeg
%
% Name-Value Pair Arguments
%   TempDir    : Nom du repertoire de stockage des fichiers temporaires (/tmp/ par defaut)
%
% Remarks : ATTENTION : Cette fonction efface tous les fichiers *.jpg presents dans le
%           repertoire TempDir.
%           Cette fonction ne fonctionne que sur unix. Si la commande convert n'est pas accessible,
%           veuillez ajouter ce chemin a votre path : /home/services_SV/bibli/xwindow/X11R5/bin
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_300m.mnt');
%   a = cl_car_ima(nomFic);
%   Film = movieOmbrage(a, 'az', 0:30:259, 'xpentes', 8);
%   movie2mpeg( Film, '/tmp/prismed.mpg' )
%   movie2mpeg( Film, '/tmp/prismed.mpg', 'TempDir', '/tmp/')
%
% See also movie movie2avi Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function movie2mpeg( film, nomFicMpeg, varargin )

if ~isstruct(film)
    return
end

names = fieldnames(film);
if ~strcmp(names{1}, 'cdata') || ~strcmp(names{2}, 'colormap')
    return
end

[varargin, DirTemp] = getPropertyValue(varargin, 'TempDir', my_tempdir); %#ok<ASGLU>

format = 'jpg'; % 'bmp';

% -------------------------------------------------------------
% Effacement de tous les fichiers jpg (ou bmp) qui trainent sur
% le repertoire /tmp.

commande =  sprintf('! \\rm %s*.%s', DirTemp, format);
disp(commande)
eval(commande);

% ---------------------------------------------------------
% Sauvegarde de toutes les images du film sur le repertoire

for i=1:length(film)
    nomFicJpg = sprintf('%s%03d.%s', DirTemp, i, format);
    if length(size(film(i).cdata)) == 3
        imwrite(film(i).cdata, nomFicJpg, format);
    else
        imwrite(film(i).cdata, film(i).colormap, nomFicJpg, format);
    end
end

% -------------------------------------------------------------------
% Lancement de imagemagik pour transformer les images en fichier MPEG

% convert se trouve sous /home/services_SV/bibli/xwindow/X11R5/bin
commande =  sprintf('! convert %s*.%s %s', DirTemp, format, nomFicMpeg);
disp(commande)
eval(commande);

% -------------------------------------------------------------
% Effacement de tous les fichiers jpg (ou bmp) qui trainent sur
% le repertoire /tmp.

commande =  sprintf('! \\rm %s*.%s', DirTemp, format);
disp(commande)
eval(commande);
