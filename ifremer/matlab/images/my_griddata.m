% Description
%   2D interpolation of a set of points defined by {x, y, v}. This method
%   differs from the griddata matlab function that interpolates all ths
%   points. Here, only the grid cells "touched" by the {x, y} coordinates
%   are filled.
%
% Syntax
%   [vq, nbq] = my_griddata(x, y, v, xq, yq, TypeAlgo)
%
% Input Arguments
%   x  : Abscissa
%   y  : Ordinates
%   v  : Values 
%   xq : Abscissa of the interpolated grid
%   yq : Ordinates of the interpolated grid
%
% Name-Value Pair Arguments
%   TypeAlgo : 1=Nearest, 2=Linear (Default : 2)
%
% Output Arguments
%   vq : Values on the interpolated grid
%   nq : Number of salples used to interpolated every grid cell
%
% Examples
%   % TODO
%   [vq, nbq] = my_griddata(x, y, v, xq, yq, TypeAlgo);
%     figure; scatter(x, y, [], v);         colormap jet; colorbar, axis('tight'); xlabel('x');  ylabel('y');  title('v'); 
%     figure; imagesc(xq, yq, vq); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('vq from my_griddata'); 
%
% Authors : JMA 
% See also griddata import_xyz
%-------------------------------------------------------------------------------


function [vq, nbq] = my_griddata(x, y, v, xq, yq, varargin)

[varargin, TypeAlgo] = getPropertyValue(varargin, 'TypeAlgo', 2); %#ok<ASGLU>

nbColumns = length(xq);
nbRows    = length(yq);

N = length(x);

xmin = min(xq);
ymin = min(yq);
pasx = median(diff(xq), 'omitnan');
pasy = median(diff(yq), 'omitnan');

%% Création de l'image

nbq = zeros(nbRows, nbColumns, 'single');
vq  = zeros(nbRows, nbColumns, 'single');
WorkInProgress(Lang('Maillage en cours', 'Griding data'));
% Pour info : j'ai dû supprimer le my(waitbar car il prenait 99% du temps

switch TypeAlgo
    case 1 % Plus proche voisin
        for k=1:N
            ix = 1 + floor((x(k) - xmin) / pasx);
            iy = 1 + floor((y(k) - ymin) / pasy);
            
            if (iy >= 1) && (iy <= nbRows) && (ix >= 1) && (ix <= nbColumns)
                nbq(iy,ix) = nbq(iy,ix) + 1;
                vq(iy,ix) = vq(iy,ix) + v(k);
            end
        end
        sub = (nbq ~= 0);
        vq(sub) = vq(sub) ./ nbq(sub);
        sub = find(nbq == 0);
        vq(sub)  = NaN;
        nbq(sub) = NaN;
        clear sub
        
    case 2 % Linéaire
        for k=1:N
            xp = (x(k) - xmin) / pasx;
            yp = (y(k) - ymin) / pasy;
            ix1 = 1 + floor(xp);
            iy1 = 1 + floor(yp);
            xp = xp +1;
            yp = yp + 1;
            ix2 = ix1 + 1;
            iy2 = iy1 + 1;
            
            if ((iy1 >= 1) && (iy1 <= nbRows) && (ix1 >= 1) && (ix1 <= nbColumns) && ...
                (iy2 >= 1) && (iy2 <= nbRows) && (ix2 >= 1) && (ix2 <= nbColumns))
                xalpha = ix2 - xp;
                yalpha = iy2 - yp;
                
                A1 =     xalpha * yalpha;
                A2 = (1-xalpha) * yalpha;
                A3 = (1-xalpha) * (1-yalpha);
                A4 =     xalpha * (1-yalpha);
                Vali = v(k);
                
                nbq(iy1,ix1) = nbq(iy1,ix1) + A1;
                vq(iy1,ix1) = vq(iy1,ix1) + A1 * Vali;
                
                nbq(iy1,ix2) = nbq(iy1,ix2) + A2;
                vq(iy1,ix2) = vq(iy1,ix2) + A2 * Vali;
                
                nbq(iy2,ix2) = nbq(iy2,ix2) + A3;
                vq(iy2,ix2) = vq(iy2,ix2) + A3 * Vali;
                
                nbq(iy2,ix1) = nbq(iy2,ix1) + A4;
                vq(iy2,ix1) = vq(iy2,ix1) + A4 * Vali;
            end
        end
        sub = (nbq ~= 0);
        vq(sub) = vq(sub) ./ nbq(sub);
        sub = find(nbq == 0);
        vq(sub) = NaN;
        nbq(sub) = NaN;
        clear sub
        
    case 3 % Fenêtre de pondération gaussiène
        % TODO : à poursuivre
        sigma = 0.75;
        n = floor(2 * sigma);
        window = [max(3,2*n+1) max(3,2*n+1)];
        w = window(1);
        %         y = normpdf(x,mu,sigma)
        h = 100 * fspecial('gaussian', [1 w*100*2], sigma*100);
        h = h(floor(length(h)/2):end);
        %     figure(20081008); plot(h); grid on; title('Gaussian coefficient filter');
        subh = -n:n;
        IX = repmat(subh,  w, 1);
        IY = repmat(subh', 1, w);
        %         tic
        for k=1:N
            Vali = v(k);
            xp = (x(k) - xmin) / pasx;
            yp = (y(k) - ymin) / pasy;
            ix = 1 + floor(xp);
            iy = 1 + floor(yp);
            
            ix = IX + ix;
            iy = IY + iy;
            
            sub = find((iy >= 1) & (iy <= nbRows) & (ix >= 1) & (ix <= nbColumns));
            
            for k2=1:length(sub)
                kk = sub(k2);
                xh = xp+1-ix(kk);
                yh = yp+1-iy(kk);
                %             g = normpdf(sqrt(xh*xh+yh*yh),0,sigma);
                g = h(1+floor(100*sqrt(xh*xh+yh*yh)));
                kx = ix(kk);
                ky = iy(kk);
                nbq(ky,kx) = nbq(ky,kx) + g;
                vq(ky,kx) = vq(ky,kx) + g * Vali;
            end
        end
        nbq(nbq < 0.5) = NaN;
        sub = (nbq ~= 0);
        vq(sub) = vq(sub) ./ nbq(sub);
        sub = find(nbq == 0);
        vq(sub) = NaN;
        nbq(sub) = NaN;
        clear sub
end

% figure; scatter(x, y, [], v); colormap jet; colorbar, axis('tight'); xlabel('x'); ylabel('y'); title('v'); 
% figure; imagesc(xq, yq, vq); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('vq from my_griddata'); 

% Z2 = griddata(x, y, v, xq, yq');
% figure; imagesc(xq, yq, Z2); axis xy; colormap jet; colorbar, axis('tight'); xlabel('xq'); ylabel('yq'); title('vq from griddata'); 
