function my_implay(V, varargin)

if ischar(V)
    Dir = pwd;
    [nomDir, nomFic, ext] = fileparts(V);
    if ~isempty(nomDir)
        cd(nomDir)
    end
    
    try
        implay([nomFic ext], varargin{:})
    catch %#ok<CTCH>
        str1 = sprintf('La visualisation du film a beugu� pour "%s"', nomFic);
        str2 = sprintf('Mofie display bugged for "%s"', nomFic);
        my_warndlg(Lang(str1,str2), 1);
    end
    cd(Dir)
else
    if isdeployed
        % TODO : r�aliser un .avi � partir et display_movie
    else
        implay(V, varargin{:})
    end
end