function Y = my_impyramid(X)

[nbRows, nbCol, ~] = size(X);
nbRows = 2 * floor(nbRows/2);
nbCol = 2 * floor(nbCol/2);
jLig = 1 + floor((0:(nbRows-1))/2);
jCol = 1 + floor((0:(nbCol-1))/2);

% Y = NaN([jLig(end) jCol(end) nbCan], 'single');
%  for iCan=1:nbCan
%      for iLig = 1:2:nbRows
%         I = X(iLig:iLig+1, :, iCan);
%         for iCol=1:2:nbCol
%             x = I(:, iCol:iCol+1);
%             x = x(~isnan(x));
%             n = length(x);
%             switch n
%                 case 0
%                 case 1
%                     Y(jLig(iLig),jCol(iCol),iCan) = x;
%                 otherwise
%                     Y(jLig(iLig),jCol(iCol),iCan) = sum(x) / n;
%             end
%         end
%     end
%  end




% %En test
% if size(X,3) == 3
%     pppp = sum(X,3);
%     subNaN = isnan(pppp);
%     subNaN = repmat(subNaN, [1,1,3]);
%     X(subNaN) = NaN;
%     subNaN = (pppp == 0);
%     subNaN = repmat(subNaN, [1,1,3]);
%     X(subNaN) = NaN;
% end



 Y = imresize(X, [jLig(end) jCol(end)]);
 sub = isnan(Y);
 if all(~sub(:))
     return
 else
     K = imresize(X, [jLig(end) jCol(end)], 'nearest'); % bilinear : je ne vois pas de diff�rence visuelle entre les 2 m�thodes si ce n'est les bords qui sont r�tract�s pour bilinear
     Y(sub) = K(sub);
 end


