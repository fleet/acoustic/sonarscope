function h = my_imtool(varargin)

if isdeployed
    % Juste pour supprimer un warning lors de la compilation
else
    h = imtool(varargin{:});
    % h = imageViewer(varargin{:});
end