function Y = my_rot90(X, varargin)

if nargin == 1
    Rotation = 1;
else
    Rotation = varargin{1};
end

sz = size(X);
if length(sz) == 2
    Y = rot90(X, Rotation);
else
    Y = zeros(sz([2 1 3:end]), class(X));
    for k=1:sz(3)
        Y(:,:,k) = rot90(X(:,:,k), Rotation);
    end
end
