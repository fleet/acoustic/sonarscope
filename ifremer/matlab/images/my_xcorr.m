% Intercorrelation de deux signaux et recherche du maximum
% Autocorrelation si un seul signal
%
% Syntax
%   [C, x, Delay] = my_xcorr(I1)
%   [C, x, Delay] = my_xcorr(I1, I2)
%
% Input Arguments
%   I1 : Premier signal
%   I2 : Deuxime signal
%
% Name-Value Pair Arguments
%   resol : resolution de I1
%
% Output Arguments
%   []    : Auto-plot activation
%   C     : Matrice d'intercorrelation
%   x     : Abscisses de C
%   Delay : Abscisse du maximum de C
%
% Examples
%   I = Lena;
%   sub1 = 80:150;
%   sub2 = sub1 + 3;
%   I1 = I(140, sub1);
%   I2 = I(140, sub2);
%   figure; plot(I1); hold on; plot(I2); grid on;
%
%   my_xcorr(I1, I2);
%   [C, x, Delay, yModele, strModel]  = my_xcorr(I1, I2);
%   Delay
%   figure; plot(x, C); grid on;
%   hold on; plot([-Delay -Delay], [min(C) max(C)], 'k'); title(['Shift : ' num2str(-Delay)])
%   hold on; plot(x, yModele, 'r'); title(strModel, 'Interpreter', 'latex')
%
% See also my_xcorr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function [C, x, Delay, yModele, strModel, XDataFlags] = my_xcorr(I1, varargin)

% Fonction � revoir pour supprimer la maladresse Nargin = Nargin - 2;

[varargin, UnitX] = getPropertyValue(varargin, 'UnitX', 'Samples');
[varargin, UnitY] = getPropertyValue(varargin, 'UnitY', '');

Nargin = nargin;

[varargin, flag_time] = getPropertyValue(varargin, 'cl_time', []);
if isempty(flag_time)
    flag_time = 0;
else
    Nargin = Nargin - 2;
end

[varargin, resol] = getPropertyValue(varargin, 'resol', []);
if isempty(resol)
    resol = 1;
else
    Nargin = Nargin - 2;
end

[varargin, fig] = getPropertyValue(varargin, 'fig', []);
if ~isempty(fig)
    Nargin = Nargin - 2;
end

[varargin, nomSignal] = getPropertyValue(varargin, 'nomSignal', []);
if isempty(nomSignal)
    if isempty(varargin)
        Nargin = 1;
        nomSignal = 'Signal';
    else
        if Nargin == 1
            nomSignal = 'Signal';
        else
            nomSignal = 'Signaux';
        end
    end
else
    Nargin = Nargin -2;
end

if ~isa(I1, 'double')
    I1 = double(I1);
end
I1_Origine = I1;

I1 = I1 - mean(I1(:), 'omitnan');
I1(isnan(I1)) = 0;

if Nargin == 1
    I2 = I1;
    I2_Origine = I1_Origine;
else
    I2 = varargin{1};
    if ~isa(I2, 'double')
        I2 = double(I2);
    end
    I2_Origine = I2;
    I2 = I2 - mean(I2(:), 'omitnan');
    I2(isnan(I2)) = 0;
end

%     I1 = I1 / std(I1);
%     I2 = I2 / std(I2);

n1 = length(I1);
n2 = length(I2);
% [ppp, ind] = min([n1 n2]);

% FigUtils.createSScFigure; PlotUtils.createSScPlot(I1, 'b'); grid on; hold on; PlotUtils.createSScPlot(I2, 'r')
C = xcorr(I1, I2);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(C); grid on;

E(1) = sum(I1(:) .^ 2);
E(2) = sum(I2(:) .^ 2);
C = C / sqrt(prod(E));

milieu = max(n1, n2);
[~, CMax] = max(abs(C));
Delay = milieu - CMax;
x = resol * ((1:length(C)) - milieu);

[Params, XDataFlags]  = optimCosinusAmorti(x, C, 'Edit', 1, 'UnitX', UnitX, 'UnitY', UnitY);
yModele = cosinusAmorti(x, Params);

str1 = sprintf('Le d�calage trouv� est de %s', num2str(Delay));
str2 = sprintf('Offset is %s', num2str(Delay));

if Nargin == 1
    strModel = 'self-correlation';
else
    strModel = 'Cross-correlation';
end
Amplitude   = Params(1);
Attenuation = Params(2);
Periode     = Params(3);
Delay       = Params(4);

S = '$A{e^{-{\frac {x}{a}}}}\cos \left( 2\,{\frac {\pi \,x}{T}} \right) $';
if flag_time % L'axe des X est un temps (unite matlab)
    strModel = sprintf('%s : y=A*exp(-x/Alpha)*cos(2*pi*x/T) : A=%s Alpha=%s T=%s (%s s) Delay=%s', ...
        strModel, num2str(Amplitude), num2str(Attenuation), num2str(Periode), num2str(Periode * (24*3600)), num2str(Delay));
else
    strModel = sprintf('%s : %s : T=%s Alpha=%s A=%s Delay=%s', ...
        strModel, S, num2str(Periode), num2str(Attenuation), num2str(Amplitude), num2str(Delay));
end

if nargout == 0
    if isempty(fig)
        figure
    else
        figure(fig)
    end
    subplot(2,1,1);
    xg = 1:length(I1_Origine);
    yyaxis left;  hold off; plot(xg,       I1_Origine, 'b');
    yyaxis right; hold on;  plot(xg-Delay, I2_Origine, 'r');
    title('First signal and scd with delay applied', 'Interpreter', 'none'); grid on; axis tight;
    
    h2 = subplot(2,1,2); hold off;
    PlotUtils.createSScPlot(x, C, '*'); grid on; axis xy; axis tight;
    disp(Lang(str1,str2))
    hold on; PlotUtils.createSScPlot(x, yModele, 'k');

    h = title(strModel);
    set(h, 'Interpreter', 'latex', 'fontsize', 14)
    
%     h = text(n/5, 0.6, S);
%     set(h, 'Interpreter', 'latex', 'fontsize', 12)

    sub = find(XDataFlags == 1);
    set(h2, 'XLim', x(sub([1 end])));
end
