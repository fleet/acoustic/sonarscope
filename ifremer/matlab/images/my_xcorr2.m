% TODO : voir si on peut rafiner la recherche du max par interpolation2D et
% améliorer le "Comments" de crossCorrelation
%
% Crosscorrelation of two images and estimates the shifts
%
% Syntax
%   [C, x, y, ixMaxC, iyMaxC] = my_xcorr2(I1, I2)
%
% Input Arguments
%   I1 : First image 
%   I2 : Second image 
% 
% Output Arguments
%   []     : Auto-plot activation
%   C      : Crosscorrelation matrix
%   x      : Abscissa of C
%   y      : Ordinates of C
%   ixMaxC : Absicca of the maximum of C
%   iyMaxC : Ordinate of the maximum of C
% 
% Examples 
%   I = Lena;
%   subx = 110:150; suby = 120:150;
%   I1 = I(suby, subx);
%   I2 = I(suby-4, subx+3);
%   figure; subplot(1,2,1); imagesc(I1); subplot(1,2,2); imagesc(I2); colormap(gray(256))
%
%   my_xcorr2(I1, I2);
%   [C, x, y, ixMaxC, iyMaxC]  = my_xcorr2(I1, I2);
%   ixMaxC, iyMaxC
%   figure; imagesc(x, y, C); axis xy;
%
% See also my_xcorr2_byFFT cl_image/crossCorrelation Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function [C, x, y, ixMaxC, iyMaxC] = my_xcorr2(I1, I2)

if ~isa(I1, 'double')
    I1 = double(I1);
end

if ~isa(I2, 'double')
    I2 = double(I2);
end

I1 = I1 - mean(I1(:), 'omitnan');
I2 = I2 - mean(I2(:), 'omitnan');

I1(isnan(I1)) = 0;
I2(isnan(I2)) = 0;


C = xcorr2(I1,I2);
[~, imax] = max(C(:));
[LMax, CMax] = ind2sub(size(C), imax);

[~, nbcI1] =  size(I1);
[nblI2, nbcI2] =  size(I2);

milieu = max(nbcI1, nbcI2);
ixMaxC = milieu - CMax;
x = (1:size(C,2)) - milieu;

milieu = nblI2;
iyMaxC = milieu - LMax;
y = (1:size(C,1)) - milieu;

if nargout == 0
    figure; subplot(1,2,1); imagesc(I1); subplot(1,2,2); imagesc(I2); colormap(gray(256))
    figure; imagesc(x, y, C); grid on; axis xy;
    title('Intercorrelation entre les 2 images')
    str1 = sprintf('Le decalage trouve est de %s en x et %s en y', num2str(ixMaxC), num2str(iyMaxC));
    str2 = sprintf('Offset is %s in x and %s in y', num2str(ixMaxC), num2str(iyMaxC));
    disp(Lang(str1,str2))
end
