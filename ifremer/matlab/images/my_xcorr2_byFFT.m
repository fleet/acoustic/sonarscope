% TODO : voir si on peut rafiner la recherche du max par interolation2D et
% améliorer le "Comments" de crossCorrelationByFFT
% Renommer en my_xcorr2ByFFT
% Faire le clair sur les signes des décalages
%
% Crosscorrelation of two images and estimates the shifts using FFT
%
% Syntax
%   [C, x, y, ixMaxC, iyMaxC] = my_xcorr2_byFFT(I1, I2)
%
% Input Arguments
%   I1 : First image 
%   I2 : Second image 
% 
% Output Arguments
%   []     : Auto-plot activation
%   C      : Crosscorrelation matrix
%   x      : Abscissa of C
%   y      : Ordinates of C
%   ixMaxC : Absicca of the maximum of C
%   iyMaxC : Ordinate of the maximum of C
% 
% Examples 
%   I = Lena;
%   subx = 110:150; suby = 120:150;
%   I1 = I(suby, subx);
%   I2 = I(suby-4, subx+3);
%   figure; subplot(1,2,1); imagesc(I1); subplot(1,2,2); imagesc(I2); colormap(gray(256))
%
%   my_xcorr2_byFFT(I1, I2);
%   [C, x, y, ixMaxC, iyMaxC]  = my_xcorr2_byFFT(I1, I2);
%   ixMaxC, iyMaxC
%   figure; imagesc(x, y, C); axis xy;
%
% See also my_xcorr2 cl_image/crossCorrelationByFFT Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function [C, x, y, ixMaxC, iyMaxC] = my_xcorr2_byFFT(I1, I2)

if ~isa(I1, 'double')
    I1 = double(I1(:,:));
end

if ~isa(I2, 'double')
    I2 = double(I2(:,:));
end

I1 = I1 - mean(I1(:), 'omitnan');
I2 = I2 - mean(I2(:), 'omitnan');

I1(isnan(I1)) = 0;
I2(isnan(I2)) = 0;

I1_FFT = fft2(I1);
I2_FFT = conj(fft2(I2));
C = ifft2(I1_FFT .* I2_FFT);
C = real(fftshift(C));

[~, imax] = max(C(:));
[LMax, CMax] = ind2sub(size(C), imax);

[nblI1, nbcI1] =  size(I1);

milieu = floor(nbcI1 / 2) + 1;
ixMaxC = milieu - CMax;
x = (1:size(C,2)) - milieu;

milieu = floor(nblI1 / 2) + 1;
iyMaxC = milieu - LMax;
y = (1:size(C,1)) - milieu;

if nargout == 0
    figure; subplot(1,2,1); imagesc(I1); subplot(1,2,2); imagesc(I2); colormap(gray(256))
    figure; imagesc(x, y, C); grid on; axis xy; colormap(jet(256))
    title('Intercorrelation entre les 2 images')
    str1 = sprintf('Le décalage trouve est de %s pixelsen x et %s pixels en y', num2str(ixMaxC), num2str(iyMaxC));
    str2 = sprintf('Offset is %s pixels in x and %s pixels in y', num2str(ixMaxC), num2str(iyMaxC));
    fprintf('%s\n', Lang(str1,str2));
end
