% Contourage d'une image contenant des valeurs entieres
%
% Syntax
%   [xy, labels] = mycontourc(I, niveaux)
%   [xy, labels] = mycontourc(X, Y, I, niveaux)
% 
% Input Arguments 
%   I       : Image
%   niveaux : Niveaux
%   X       : Abscisses de l'image
%   Y       : Ordonnees de l'image
%
% Output Arguments 
%   xy     : Coordonnees des contours dans un tableau de cellules
%   labels : Niveaux associes a chaque contour
%
% Examples
%   x = -2:.2:2;
%   y = -2:.2:3;
%   [X,Y] = meshgrid(x,y);
%   Z = 10 * X .* exp(-X.^2-Y.^2);
%   [C,h] = contour(X,Y,Z);
%   clabel(C,h)
%   colormap cool
%
%   niveaux = -4:2:4;
%   mycontourc(x, y, Z, niveaux)
%   [xy, labels] = mycontourc(x, y, Z, niveaux)
%     figure;
%     imagesc(x, y, Z); colorbar; colormap cool; hold on;
%     for i=1:length(xy)
%         disp(sprintf('%d %d', i, labels(i)))
%         c = xy{i};
%         plot(c(1,:), c(2,:)); hold on; grid on;
%     end
%
% See also contourc Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = mycontourc(varargin)

switch nargin
    case 0

        return
    case 2
        I = varargin{1};
        niveaux = varargin{2};
        X = 1:size(I,2);
        Y = 1:size(I,1);
    case 4
        X = varargin{1};
        Y = varargin{2};
        I = varargin{3};
        niveaux = varargin{4};
    otherwise
        return
end

%% Contourage de l'image

xy = cell(0);
levels = [];
for i=1:length(niveaux)
    ILevel = double((I >= (niveaux(i) - 0.5)) & (I < (niveaux(i) + 0.5)));
    C = contourc(X, Y, ILevel, 1);
    while ~isempty(C)
        levels(end+1) = niveaux(i); %#ok<AGROW>
        N = C(2,1);
        xy{end+1} = C(:,2:(N+1)); %#ok<AGROW>
        C(:,1:(N+1)) = [];
    end
end

%% Par ici la sortie

if nargout == 0
    figure;
    %         imagesc(X, Y, I); colorbar; hold on;
    for i=1:length(xy)
        c = xy{i};
        plot(c(1,:), c(2,:)); grid on; hold on;
    end

else
    varargout{1} = xy;
    varargout{2} = levels;
end
