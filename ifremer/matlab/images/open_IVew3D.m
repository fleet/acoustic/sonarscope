function open_IVew3D(nomFic)

global Visualisateur3D %#ok<GVMIS> 

%% Affichage dans iView3D

try
    winopen(nomFic)
catch %#ok<CTCH>
    % winopen ne marche pas sur le PC d'Alain Normand, on
    % passe la commande en sp�cidfiant "-data"
    try
        if isempty(Visualisateur3D)
            loadVariablesEnv;
        end
        cmd = sprintf('!"%s" -data "%s"', Visualisateur3D, nomFic);
        eval(cmd);
    catch %#ok<CTCH>
        Visualisateur3D = 'C:\Program Files\iView3D\iview3d.exe';
        cmd = sprintf('!"%s" -data "%s"', Visualisateur3D, nomFic);
        eval(cmd);
    end
end
