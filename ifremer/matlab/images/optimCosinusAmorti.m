% Recherche des parametres du modele sinusoide amortie sur des donnees
% Params tels que eqm(data, y) soit minimal : y = A * exp(-x/Alpha) * cos(2*pi*x/Periode)
%
% TODO : TROUVER POURQUOI CA NE MARCHE PAS SUR UNE SINUSOIDE PURE
%
% Syntax
%   Parametres = optimCosinusAmorti(x, y)
%
% Input Arguments
%   x           : Abscisses
%   y           : Valeur des points
%
% Output Arguments
%   []          : Auto-plot activation
%   Parametres  : Tableau contenant les parametres du modele
%       A       : Amplitude a l'origine (x=0)
%       Alpha   : Attenuation
%       Periode : Periode de la sinusoide
%
% Examples
%   x = 0:250;
%   y = cosinusAmorti(x, [1 50 100]) + 0.1*rand(size(x));
%   optimCosinusAmorti(x,y)
%   optimCosinusAmorti(x,y, 'Edit', 1)
%
%   Parametres = optimCosinusAmorti(x,y)
%
% See also optimCosinusAmorti Authors
% Authors : JMA
%------------------------------------------------------------------------------

function [Parametres, XDataFlags] = optimCosinusAmorti(x, y, varargin)

%# function cosinusAmorti

[varargin, UnitX] = getPropertyValue(varargin, 'UnitX', 'Samples');
[varargin, UnitY] = getPropertyValue(varargin, 'UnitY', '?');
[varargin, Edit]  = getPropertyValue(varargin, 'Edit', 0); %#ok<ASGLU>

N = length(x);
resol = mean(diff(x));

params(1) = ClParametre('Name', 'Amplitude', 'Unit', UnitY, ...
    'MinValue', -1, ...
    'MaxValue', 1, ...
    'Value',    1);

params(2) = ClParametre('Name', 'Attenuation', 'Unit', UnitX, ...
    'MinValue', 1*resol, ...
    'MaxValue', max(N,50)*resol, ...
    'Value',    50*resol);

params(3) = ClParametre('Name', 'Periode', 'Unit', UnitX, ...
    'MinValue', 4*resol, ...
    'MaxValue', max(N,10)*10*resol, ...
    'Value',    10*resol);

delay = floor(N / 10);
params(4) = ClParametre('Name', 'Delay', 'Unit', UnitX, ...
    'MinValue', -delay, ...
    'MaxValue', delay, ...
    'Value',    0);

composantes = {'Exponentielle'; 'Cosinusoide'};
model = ClModel('Name', 'cosinusAmorti', 'Params', params, ...
    'XData', x, 'YData', y, ...
    'xlabel', 'x', 'xUnit', '', ...
    'ylabel', 'y', 'yUnit', '', ...
    'WeightsData', 1 ./ (1 + 0.1 * abs(x)), ...
    'ComponentsName', composantes);
% model.plot();
% model.editProperties();

optim = ClOptim('XData', x, 'YData', y, ...
    'xlabel', 'x', ...
    'ylabel', 'y', ...
    'Name', 'Cross correlation', ...
    'AlgoType', 1, 'UseSimAnneal', 0, 'models', model);
% optim.editProperties();
% optim.plot
optim.optimize();
% optim.plot

%% Edition de l'objet

if Edit
    a = OptimUiDialog(optim, 'Title', 'Optim Cosinus Amorti', 'windowStyle', 'normal');
    a.openDialog();
end
Parametres = optim.getParamsValue();

XDataFlags = a.optim.models.XDataFlags;

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    plot(optim, 'DrawComponents', 0)
    %     cosinusAmorti(x, Parametres)
    %     hold on; plot(x, y, 'k'); hold off;
    %     xlabel('x'); ylabel('y');
    %     title('Optimisation par optimCosinusAmorti');
end
