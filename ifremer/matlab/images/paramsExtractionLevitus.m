function [flag, nomFicNc, subTime, subDepth] = paramsExtractionLevitus(Type)

nomFicNc = {};
subTime  = [];
subDepth = [];

switch Type
    case 'Annual'
        strMonth = {'Annual'};
        indTemps = 1;
    case 'Season'
        strMonth = {'Winter'; 'Spring'; 'Summer'; 'Autumn'};
        [indTemps, flag] = my_listdlg(Lang('Saison', 'Season'), strMonth);
        if ~flag
            return 
        end
    case 'Month'
        strMonth = {'January'; 'February'; 'March'; 'April'; 'May'; 'Juin'; 'July'; 'August'; 'Sptember'; 'October'; 'November'; 'December'};
        [indTemps, flag] = my_listdlg(Lang('Mois', 'Month'), strMonth);
        if ~flag
            return
        end
 end

strTypeData = {'Temperature'; 'Salinity'};
[indParam, flag] = my_listdlg(Lang('Param�tre', 'Parametre'), strTypeData);
if ~flag
    return
end
strTypeParametre = {'Temp'; 'Psal'};

str2 = {};
flagQuestionDepth = 1;
for iP=1:length(indParam)
    NomParametre = strTypeData{indParam(iP)};
    NomFicParametre = strTypeParametre{indParam(iP)};
    nom = ['Levitus1x1' Type NomFicParametre(1:4) '.mat'];
    nomComplet = getNomFicDatabase(nom);
    if ~isempty(nomComplet)
        
        if flagQuestionDepth
            m = matfile(nomComplet);
            Depth = m.Z;

            for iDepth=1:length(Depth)
                strDepth{iDepth} = sprintf('%3d meters', Depth(iDepth)); %#ok<AGROW>
            end
            [subD, flag] = my_listdlg(Lang('Profondeurs', 'Depths'), strDepth);
            if ~flag
                return
            end
            Depth = Depth(subD);
            flagQuestionDepth = 0;
        end
        
        for iT=1:length(indTemps)
            for iDepth=1:length(Depth)
                nomFicNc{end+1} = nomComplet; %#ok<AGROW>
                subTime(end+1)  = indTemps(iT); %#ok<AGROW>
                subDepth(end+1) = subD(iDepth); %#ok<AGROW>
                str2{end+1} = sprintf('%s %3d m %s', NomParametre, Depth(iDepth), strMonth{indTemps(iT)}); %#ok<AGROW>
            end
        end
    end
end
