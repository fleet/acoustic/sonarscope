function [flag, nomFicNc, subDepth] = paramsExtractionWOA13(period)

nomFicNc = {};
subDepth = [];

switch period
    case 'Annual'
        indTemps = 0;
        
    case 'Season'
        strPeriod = {'Winter';'Spring'; 'Summer'; 'Autumn'};
        [indTemps, flag] = my_listdlg(Lang('Saison', 'Season'), strPeriod, 'SelectionMode', 'Single');
        % WOA13 : indice �quivalent : 13 14 15 16
        indTemps = 13 + indTemps;
        if ~flag
            return 
        end
    case 'Month'
        strPeriod = {'January'; 'February'; 'March'; 'April'; 'May'; 'Juin'; 'July'; 'August'; 'Sptember'; 'October'; 'November'; 'December'};
        [indTemps, flag] = my_listdlg(Lang('Mois', 'Month'), strPeriod, 'SelectionMode', 'Single');
        if ~flag
            return
        end
 end

strTypeData = {'Temperature'; 'Salinity'};
[indParam, flag] = my_listdlg(Lang('Param�tre', 'Parametre'), strTypeData);
if ~flag
    return
end
strTypeParametre = {'t'; 's'};

flagQuestionDepth = 1;
flag = 0;
for iP=1:length(indParam)
    NomFicParametre = strTypeParametre{indParam(iP)};
    nom = sprintf('woa13_decav_%s%02d_04v2.mat', NomFicParametre, indTemps);
    
    fileName = fullfile('DataForDemo', 'WOA13', nom);
    nomComplet = FtpUtils.importDataForDemoFromSScFTP(fileName);
    if iscell(nomComplet)
        nomComplet = nomComplet{1};
    end

%     nomComplet = getNomFicDatabase(nom);
    if isempty(nomComplet)
        continue
    end
    
    flag = 1;
    if flagQuestionDepth
        m = matfile(nomComplet);
        Depth = m.Z;
        
        for iDepth=1:length(Depth)
            strDepth{iDepth} = sprintf('%3d meters', Depth(iDepth)); %#ok<AGROW>
        end
        [subD, flag] = my_listdlg(Lang('Profondeurs', 'Depths'), strDepth);
        if ~flag
            return
        end
        Depth = Depth(subD);
        flagQuestionDepth = 0;
    end
    
    for iT=1:length(indTemps)
        for iDepth=1:length(Depth)
            nomFicNc{end+1} = nomComplet; %#ok<AGROW>
            if indTemps == 0
                indTemps = indTemps + 1;
            end
            subDepth(end+1) = subD(iDepth); %#ok<AGROW>
        end
    end
end
