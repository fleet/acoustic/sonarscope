function [flag, nomFicNav, repExport] = paramsFonctionImportNav(repExport)

cd(repExport)

[flag, nomFicNav] = my_uigetfile( ...
{'*.nvi;*.mat', 'Navigation Files (*.nvi,*.mat)';
   '*.nvi',  'Caraibes Navigation files (*.nvi)'; ...
   '*.mat', 'SonarScope Navigation files (*.mat)'; ...
   '*.*',  'All Files (*.*)'}, ...
   'Pick a file');
if ~flag
    return
end
repExport = fileparts(nomFicNav);
flag = 1;
