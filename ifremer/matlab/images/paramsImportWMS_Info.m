function flag = paramsImportWMS_Info

str1 = 'Avant de tenter une interrogation d''un serveur WMS vous pouvez obtenir une information sur ce que sont ces serveurs. Choisizzez un exemple de serveur pour le d�couvrir';
str2 = 'Before requesting an image on WMS server you can inform you on what WMS servers are. Select a web site if so.';
str = {};
str{end+1} = 'http://topex.ucsd.edu/WWW_html/srtm30_plus.html';
str{end+1} = 'http://svs.gsfc.nasa.gov/';
str{end+1} = 'http://svs.gsfc.nasa.gov/vis/a000000/a000000/a000073/index.html';
str{end+1} = 'http://www.demis.nl/home/pages/wms/demiswms.htm';
% edit_infoPixel(str, [], 'PromptString', 'Some WMS servers you should visit')
[rep, flag] = my_listdlg(Lang(str1,str2 ), str, 'SelectionMode', 'Single');
if flag && ~isempty(rep)
    my_web(str{rep})
end
