% Input a radius
%
% Syntax
%   [flag, Rayon] = paramsMasqueEnglobant
%
% Output Arguments
%   flag   : 1=OK, 0=KO
%   Radius : Radius of a structurent element
%
% Examples
%   [flag, Radius] = paramsMasqueEnglobant
%
% See also cl_image/smoothContours Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Radius] = paramsMasqueEnglobant

% TODO : mettre l'unit�

str1 = 'Rayon de l''�l�ment structurant';
str2 = 'Radius of structurent element';
p = ClParametre('Name',  Lang('Rayon','Radius'),  ...
    'MinValue', 0, 'MaxValue', 100, 'Value', 2, 'Format', '%d');
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-1 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    Radius = [];
    return
end
Radius = a.getParamsValue;
