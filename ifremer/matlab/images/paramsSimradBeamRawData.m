function [flag, DepthMin, DepthMax, DisplayLevel] = paramsSimradBeamRawData
    
DepthMin = [];
DepthMax = [];
DisplayLevel = [];
            
%% Saisie de la distance maximale recherchée

str1 = 'TODO';
str2 = 'Please give min and max Depth values if you can do it, keep initial values if not.';
p(1) = ClParametre('Name', Lang('Profondeur Min', 'Minimal Depth'), ...
    'Unit', 'm',  'MinValue', 1, 'MaxValue', 12000, 'Value', 1);
p(2) = ClParametre('Name', Lang('Profondeur Max', 'Maximal Depth'), ...
    'Unit', 'm',  'MinValue', 10, 'MaxValue', 12000, 'Value', 12000);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
DepthMax = -max(val);
DepthMin = -min(val);

%% Saisie du niveau de visualisation

[flag, DisplayLevel] = question_DetailLevelBDA('Default', 2);
if ~flag
    return
end
