function [flag, Seuil] = paramsWatershed

persistent Seuil_persistent

if isempty(Seuil_persistent)
    Seuil = 0;
else
    Seuil = Seuil_persistent;
end

str1 = 'Seuil de prise en compte des minimas locaux';
str2 = 'Local minima threshold';
[flag, Seuil] = inputOneParametre(Lang(str1,str2), Lang('Seuil','Threshold'), ...
    'Value', Seuil, 'MinValue', 0);
if ~flag
    return
end
Seuil_persistent = Seuil;
