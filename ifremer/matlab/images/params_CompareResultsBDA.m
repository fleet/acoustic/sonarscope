function [flag, nomDirMatlab, nomDirC, repImport, repExport] = params_CompareResultsBDA(repImport, repExport)

nomDirC = []; 

str1 = 'Nom du r�pertoire les r�sultats du BDA "Matlab".';
str2 = 'Name of the "Matlab" BDA results.';
[flag, nomDirMatlab] = my_uigetdir(repImport, Lang(str1,str2));
if ~flag
    return
end
repImport = nomDirMatlab;


str1 = 'Nom du r�pertoire les r�sultats du BDA "C".';
str2 = 'Name of the "C" BDA results.';
[flag, nomDirC] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirC;
