function [flag, string1, string2, string3, string4, suffix] = params_FindAndReplace

persistent persistent_suffix persistent_string1  persistent_string2  persistent_string3  persistent_string4

suffix  = [];

% str1 = 'Rechercher string1 :';
% str2 = 'Find string1 : (Ex : ALL\\)';
% str3 = 'Remplacer par string1 :';
% str4 = 'Replace string1 by : (Ex : ALL\\SScProc\\WC-DepthAcrossDist\\)';
% str5 = 'Rechercher string2 :';
% str6 = 'Find string2 : (Ex : .all)    (let it empty if only one string has to be replaced)';
% str7 = 'Remplacer string2 par :';
% str8 = 'Replace string2 by : (Ex : PolarEchograms.g3d.nc)';
str1 = 'Rechercher string1 :';
str2 = 'Find string1 :';
str3 = 'Remplacer par string1 :';
str4 = 'Replace string1 by :';
str5 = 'Rechercher string2 :';
str6 = 'Find string2 : (let it empty if only one string has to be replaced)';
str7 = 'Remplacer string2 par :';
str8 = 'Replace string2 by :';
nomVar = {Lang(str1,str2) ; Lang(str3,str4); Lang(str5,str6) ; Lang(str7,str8)};

value = {'' ; '' ; '' ; ''};

if ~isempty(persistent_string1)
    value{1} = persistent_string1;
end
if ~isempty(persistent_string2)
    value{2} = persistent_string2;
end
if ~isempty(persistent_string3)
    value{3} = persistent_string3;
end
if ~isempty(persistent_string4)
    value{4} = persistent_string4;
end

[value, flag] = my_inputdlg(nomVar, value);
if flag
    string1 = value{1};
    string2 = value{2};
    string3 = value{3};
    string4 = value{4};
    if isempty(string1)
        flag = 0;
        return
    end
    persistent_string1 = string1;
    persistent_string2 = string2;
    persistent_string3 = string3;
    persistent_string4 = string4;
else
    string1 = [];
    string2 = [];
    string3 = [];
    string4 = [];
    return
end

str1 = 'Cr�ation d''un nouveau fichier ASCII ?';
str2 = 'Create a new ASCII file ?';
[ind, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end

if ind == 1
    if isempty(persistent_suffix)
        suffix = {'-New'};
    else
        suffix = persistent_suffix;
    end
    [suffix, flag] = my_inputdlg({'Suffix'}, suffix);
    if ~flag
        return
    end
    suffix = suffix{1};
    persistent_suffix = suffix;
end
