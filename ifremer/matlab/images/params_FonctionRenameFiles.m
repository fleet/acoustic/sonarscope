function [flag, motif1, motif2] = params_FonctionRenameFiles

persistent persistent_value

motif1 = [];
motif2 = [];

nomVar{1} = Lang('Motif � remplacer', 'String to be replaced (Ex : tan1806-TANGAROA-EM2040-20180512-2018-???-)');
nomVar{2} = Lang('Motif de remplacement', 'Replacing string (Ex : EMPTY STRING)');

if isempty(persistent_value)
    value = {'' ; ''};
else
    value = persistent_value;
end

[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
motif1 = value{1};
motif2 = value{2};

persistent_value = value;
