function [flag, listeFic, nomFicOut, repImport, repExport] = params_GroupEchograms(repImport, repExport)

persistent persistent_Titre

nomFicOut = [];

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.xml', ...
    'RepDefaut', repImport, 'ChaineIncluse', '_PolarEchograms');
if ~flag
    return
end

if isempty(persistent_Titre)
    filename = 'Line_xxx';
else
    filename = persistent_Titre;
end

filtre = fullfile(repExport, [filename '.xml']);
[flag, nomFicOut] = my_uiputfile('*.xml', 'Give a file name', filtre);
if ~flag
    return
end
[pathname, filename] = fileparts(nomFicOut);
persistent_Titre = filename;
repExport = pathname;
