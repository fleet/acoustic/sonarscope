function [flag, ModeDependant, LayerName, Window, flagTideCorrection, SameReflectivityStatus, InfoCompensationCurve, ...
    InfoMos, repImport, repExport] = params_MBES_IndividualMosaics(this, listFileNames, repImport, repExport, varargin)

[varargin, TypeMos] = getPropertyValue(varargin, 'TypeMos', 1); %#ok<ASGLU>

ModeDependant          = [];
LayerName              = [];
% selectionData        = [];
Window                 = [3 3];
SameReflectivityStatus = 0;
flagTideCorrection     = 0;
InfoCompensationCurve  = [];
InfoMos                = [];

E0 = cl_ermapper([]);

if TypeMos == 2
    useParallel = get_UseParallelTbx;
    if useParallel
        str1 = 'La //Tbx est activ�e, c''est une bonne id�e. Attention toutefois � l''usage de cette fonctionnalit� pour la g�n�ration de mosa�ques, cela peut �tre contre-productif.';
        str2 = 'The //Tbx is activated, that is a good idea. Nevertheless, take care to this option here, it can be contreproductive.';
        my_warndlg(Lang(str1,str2), 1);
    end
end

%% V�rification des fichiers et extraction de quelques param�tres

[flag, allKM, isDual, DataInstallation] = ALL.checkFiles(listFileNames); %#ok<ASGLU>
if ~flag
    return
end

%% Choix du layer

% {
listeLayers = ALL.getListeLayers;
str1 = 'Layer � mosa�quer';
str2 = 'Layer to mosaic';
[choixLayer, flag] = my_listdlg(Lang(str1,str2), listeLayers,...
    'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag
    return
end
LayerName = listeLayers{choixLayer};
% }

%{
listeData{1} = 'Reflectivity';
listeData{2} = 'Bathymetry';
listeData{3} = 'IncidenceAngle';
listeData{4} = 'RxTime';
str1 = 'Choix du layer :';
str2 = 'Select the layer :';
[selectionData, flag] = my_listdlg(Lang(str1,str2), listeData, 'SelectionMode', 'Single');
if ~flag
    return
end
%}

%% Mosaique globale ou mosa�ques individuelles ?

if isempty(TypeMos)
    str1 = 'Type de mosa�que';
    str2 = 'Type of mosaic';
    [rep, flag] = my_questdlg(Lang(str1,str2), Lang('Unique', 'Unique'), Lang('Individuelles', 'One per file'));
    if ~flag
        return
    end
    InfoMos.TypeMos = rep;
else
    InfoMos.TypeMos = TypeMos;
end

%% Type de grille

if InfoMos.TypeMos == 1
    InfoMos.TypeGrid = 1;
else
    str1 = 'Type de pas de la grille';
    str2 = 'Type of grid size';
    [rep, flag] = my_questdlg(Lang(str1,str2), Lang('Unique', 'Unique'), Lang('Estim� sur chaque image', 'Estimated on each image'));
    if ~flag
        return
    end
    InfoMos.TypeGrid = rep;
end

%% Question masquage des faisceaux qui se recouvrent si sondeur dual

if any(isDual)
    str1 = 'Masquage des faisceaux redondants si sondeur dual ?';
    str2 = 'Mask overlapping beams if dual sounder ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    InfoMos.flagMasquageDual = (rep == 1);
else
    InfoMos.flagMasquageDual = 0;
end

%% Question for Mask

[flag, rep] = question_UseMask([], 'QL', 3);
if ~flag
    return
end
InfoMos.Import.MasqueActif = (rep == 1);

%% Utilisation d'un fichier de d�coupe GLT

[flag, SlotFilename, SonarTime] = question_TimeSlots('QL', 3);
if ~flag
    return
end
InfoMos.Import.SlotFilename = SlotFilename;
InfoMos.Import.SonarTime    = SonarTime;

%% Gestion du recouvrement entre profils

[flag, Priority, SpecularLimitAngle] = question_LinesOverlapping;
if ~flag
    return
end
InfoMos.Covering.Priority           = Priority;
InfoMos.Covering.SpecularLimitAngle = SpecularLimitAngle;

%% Limites angulaires

[flag, LimitAngles] = question_AngularLimits('QL', 2);
if ~flag
    return
end
InfoMos.LimitAngles = LimitAngles;

%% Backup frequency

if InfoMos.TypeMos == 1
    [flag, Backup] = saisie_backupFrequency(length(listFileNames));
    if ~flag
        return
    end
    InfoMos.Backup = Backup;
end

%% Pas de la grille

if InfoMos.TypeGrid == 1
    [flag, gridSize] = get_gridSize();
    if ~flag
        return
    end
    InfoMos.gridSize = gridSize;
else
    InfoMos.gridSize = [];
end

%% Si Mosaique de Depth, on propose de faire la correction de mar�e

flagTideCorrection = any((choixLayer == 1) | (choixLayer == 24));
if flagTideCorrection
    [flag, flagTideCorrection] = question_TideCorrection;
    if ~flag
        return
    end
end

%% If Reflectivity, ask if the reflectivity has to be set as the current one (same status for BS, DiagTx, etc ...)

% TODO : rajouter test si image � r�aliser est de type Reflectivity
if testSignature(this, 'DataType', cl_image.indDataType('Reflectivity'), 'GeometryType', 'PingXxxx', 'noMessage')
    [flag, SameReflectivityStatus] = question_SameReflectivityStatus;
    if ~flag
        return
    end
end

%% Compensation statistique

[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport);
if ~flag
    return
end
InfoCompensationCurve.FileName      = CorFile;
InfoCompensationCurve.ModeDependant = ModeDependant;
InfoCompensationCurve.UseModel      = UseModel;
InfoCompensationCurve.PreserveMeanValueIfModel = PreserveMeanValueIfModel;

%% Noms de fichiers des mosa�ques

if InfoMos.TypeMos == 1
    
    %% Noms des fichiers de sauvegarde des mosa�ques
    
    % {
    [DataType, ~, NomGenerique] = ALL.identSonarDataType(LayerName);
    % }
    %{
    switch selectionData
        case 1 % 'Reflectivity'
            DataType = cl_image.indDataType('Reflectivity');
            NomGenerique = 'MOS';
        case 2 % 'Bathymetry'
            DataType = cl_image.indDataType('Bathymetry');
            NomGenerique = 'DTM';
        case 3 % 'IncidenceAngle'
            DataType = cl_image.indDataType('IncidenceAngle');
            NomGenerique = 'INC';
        case 4 % 'RxTime'
            DataType = cl_image.indDataType('RxTime');
            NomGenerique = 'T';
        otherwise
            'oups'
    end
    %}
    
    [nomFicErMapperLayer, flag] = initNomFicErs(E0, NomGenerique, cl_image.indGeometryType('LatLong'), DataType, repExport, ...
        'resol', InfoMos.gridSize);
    if ~flag
        return
    end
    InfoMos.Unique.nomFicErMapperLayer = nomFicErMapperLayer;
    
    [repExport, TitreDepth] = fileparts(nomFicErMapperLayer);
    DataType = cl_image.indDataType('RxAngleEarth'); % TxAngle
    [nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, repExport, 'confirm', (get_LevelQuestion >= 3));
    if ~flag
        return
    end
    InfoMos.Unique.nomFicErMapperAngle = nomFicErMapperAngle;
    %     [~, TitreAngle] = fileparts(nomFicErMapperAngle);
    
    %% R�cup�ration �ventuelle des fichiers mosa�que si reprise d'une mosa�que existante
    
    if exist(nomFicErMapperLayer, 'file') || exist(nomFicErMapperAngle, 'file')
        [flag, rep] = question_CompleteExistingMosaic;
        if ~flag
            return
        end
        InfoMos.Unique.CompleteExistingMosaic = (rep == 1);
    else
        InfoMos.Unique.CompleteExistingMosaic = 0;
    end
    
else
    %% Nom du r�pertoire des mosaiques individuelles
    
    [flag, nomDir] = question_NomDirIndividualMosaics(repExport);
    if ~flag
        return
    end
    InfoMos.DirName = nomDir;
    repExport  = nomDir;
    
    %% skipExistingMosaic
    
    str1 = 'Ne pas refaire les mosaiques existantes ?';
    str2 = 'Skip existing mosaics ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    InfoMos.skipExistingMosaic = (rep == 1);
end
