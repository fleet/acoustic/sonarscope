function [flag, SE] = params_MorphoMath

SE = [];
str1 = 'Type d''opérateur';
str2 = 'Type of operator';
liste = {'cross'; 'square'; 'disk'; 'rectangle'; 'octagon'; 'diamond'; 'lineV'; 'lineH'};
[choix, flag] = my_listdlg(Lang(str1,str2), liste, 'SelectionMode', 'Single', 'InitialValue', 1);
if ~flag
    return
end

MaxWidth = 50;
switch choix
    case 1 % cross
        SE = [0 1 0; 1 1 1; 0 1 0];
        
    case 2 % square
        [flag, w] = inputOneParametre('Width of structurant element', 'Width', ...
            'Value', 3, 'Unit', 'Pixel', 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        if ~flag
            return
        end
        SE = strel('square', w);
        
    case 3
        [flag, R] = inputOneParametre('Mask processing parametres', 'Radius', ...
            'Value', 3, 'Unit', 'Pixel', 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        if ~flag
            return
        end
        SE = strel('disk', R, 4);
        
    case 4
        p    = ClParametre('Name', 'Height', ...
            'Unit', 'Pixel', 'Value', 3, 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        p(2) = ClParametre('Name', 'Width', ...
            'Unit', 'Pixel', 'Value', 3, 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        a = StyledSimpleParametreDialog('params', p, 'Title', 'Mask processing parametres');
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        MN = a.getParamsValue;
        SE = strel('rectangle', MN);
        
    case 5 % octagon
        [flag, R] = inputOneParametre('Length of structurant element', 'Width', ...
            'Value', 3, 'Unit', 'Pixel', 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        if ~flag
            return
        end
        SE = strel('octagon', R);
        
    case 6 % diamond
        [flag, R] = inputOneParametre('Length of structurant element', 'Width', ...
            'Value', 3, 'Unit', 'Pixel', 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        if ~flag
            return
        end
        SE = strel('diamond', R);
        
    case 7 % Ligne horizontale
        [flag, len] = inputOneParametre('Length of structurant element', 'Width', ...
            'Value', 3, 'Unit', 'Pixel', 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        if ~flag
            return
        end
        SE = strel('line', len, 0);
        
    case 8 % Ligne verticale
        [flag, len] = inputOneParametre('Length of structurant element', 'Width', ...
            'Value', 3, 'Unit', 'Pixel', 'MinValue', 3, 'MaxValue', MaxWidth, 'Format', '%d');
        if ~flag
            return
        end
        SE = strel('line', len, 90);
end
