function [flag, ListeFic, NomGenerique, AngleLim, repImport, repExport] = params_MosaicsPerAngularSectors(repImport, repExport)

persistent persistentAngleDeb persistentAngleFin persistentPasAngulaire persistentNomGenerique

AngleLim = [];
NomGenerique = 'Assemblage';

[flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.ers', ...
    'RepDefaut', repImport, ...
    'InitGeometryType', cl_image.indGeometryType('LatLong'), ...
    'InitDataType', cl_image.indDataType('Reflectivity'));
if ~flag
    return
end
repImport = lastDir;

%% Secteurs angulaires

if isempty(persistentAngleDeb)
    AngleDeb        = -70;
    AngleFin        = 70;
    PasAngulaire    = 10;
else
    AngleDeb        = persistentAngleDeb;
    AngleFin        = persistentAngleFin;
    PasAngulaire    = persistentPasAngulaire;
end

str1 = 'Param�tres de la mosa�que par pas angulaire';
str2 = 'Angulat step Mosaic parameters';
p    = ClParametre('Name', Lang('Angle final', 'Stop angle'), ...
    'Unit', 'Deg', 'Value', AngleFin, 'MinValue', -90, 'MaxValue', 90);
p(2)    = ClParametre('Name', Lang('Angle d�but', 'Start angle'), ...
    'Unit', 'Deg', 'Value', AngleDeb, 'MinValue', -90, 'MaxValue', 90);
p(3) = ClParametre('Name', Lang('Pas angulaire', 'Step'), 'MaxValue', 90, ...
    'Unit', 'Deg', 'Value', PasAngulaire, 'MinValue', 1);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

AngleFin = val(1);
AngleDeb = val(2);
PasAngulaire = val(3);

Teta = AngleDeb:PasAngulaire:(AngleFin-1000*eps);
AngleLim = [Teta' , Teta'+PasAngulaire];

if isempty(AngleLim)
    str1 = 'Les valeurs sont mal d�finies.';
    str2 = 'The values are not set properly';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
persistentAngleDeb        = AngleDeb;
persistentAngleFin        = AngleFin;
persistentPasAngulaire    = PasAngulaire;
persistentNomGenerique    = NomGenerique;

%% Nom g�n�rique

if isempty(persistentNomGenerique)
    NomGenerique = 'Assemblage';
else
    NomGenerique = persistentNomGenerique;
end
nomVar = {Lang('Nom g�n�rique des images form�es :', 'Generic name of the angular mosaics :')};
value = {NomGenerique};
[NomGenerique, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
NomGenerique =  NomGenerique{1};

%% Nom du r�pertoire des mosaiques individuelles

str1 = 'S�lectionnez le r�pertoire o� seront cr��s les mosa�ques angulaires.';
str2 = 'Selec the output directory for the angular mosaiks. ';
[flag, nomDir] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDir;
