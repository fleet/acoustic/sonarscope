function [flag, nomFicNav, listeFicImages, decalageHoraire, DisplayFullFile, KmzFilename, ...
    repImport] = params_OTUS_PlotNavAndImagesInGoogleEarth(repImport, varargin)

[varargin, AskTimeOffset] = getPropertyValue(varargin, 'AskTimeOffset', 1); %#ok<ASGLU>

DisplayFullFile = [];
KmzFilename     = [];

[flag, nomFicNav, listeFicImages, decalageHoraire, repImport] ...
    = params_OTUS_plotNavAndImages(repImport, 'AskTimeOffset', AskTimeOffset);
if ~flag
    return
end

%% Contr�le si le nombre de fichiers est raisonnable

nbImages = length(listeFicImages);
if nbImages > 200
    Title = sprintf('The number of images (%d) to put in the KMZ file is quite high. It is recommanded to be less ambitions.', nbImages);
%     [rep, flag] = my_questdlg(Title);
    str = {'Take all the files'; 'Take one over N'; 'Select the files in a list'};
    [rep, flag] = my_listdlg(Title, str);
    if ~flag
        return
    end
    switch rep
        case 2
            p  = ClParametre('Name', 'Subsampling value', 'MinValue', 1, 'MaxValue', nbImages, 'Value', floor(nbImages/200), 'Format', '%d');
            a = StyledSpinnerParametreDialog('params', p, 'Title', 'Limitation of the number of images for Google Earth');
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
            n = a.getParamsValue;
            listeFicImages = listeFicImages(1:n:nbImages);
        case 3
            [rep, flag] = my_listdlgMultiple('List of words', listeFicImages);
            if ~flag
                return
            end
            listeFicImages = listeFicImages(rep);
    end
end

%% DisplayFullFile

str = {'Source files'; 'Displayed by Matlab'};
[rep, flag] = my_listdlg('What images do you want to display', str, 'SelectionMode', 'Single');
if ~flag
    return
end
DisplayFullFile = (rep == 1);

%% Nom du fichier KMZ

str1 = 'Export de la navigation au format Google-Earth, donnez le nom du fichier :';
str2 = 'Export of navigation into Google-Earth .kmz format, give the file name :';
extField = {'*.kmz'; '*.kml'};
[nomDirGE, nomFicSeul] = fileparts(nomFicNav);
KmzFilename = fullfile(nomDirGE, [nomFicSeul '.kmz']);
[flag, KmzFilename] = my_uiputfile(extField, Lang(str1,str2), KmzFilename);
