function [flag, nomFicNav, repImport] = params_OTUS_plotNav(repImport)

%% Saisie du nom du fichier XML d�crivant les positions

str1 = 'Fichier de navigation Pagure';
str2 = 'Select the .txt navigation file';
[flag, nomFicNav] = my_uigetfile({'*.txt*;*.xml;','Position Files (*.txt*,*.xml)';
            '*.txt*',  'ASCII file (*.txt*)'; ...
            '*.xml',  'XML (*.xml)'; ...
            '*.*',  'All Files (*.*)'}, Lang(str1,str2), repImport);
if ~flag
    return
end
repImport = fileparts(nomFicNav);
