function [flag, nomFicNav, listeFicImages, decalageHoraire, repImport] ...
    = params_OTUS_plotNavAndImages(repImport, varargin)

[varargin, AskTimeOffset] = getPropertyValue(varargin, 'AskTimeOffset', 1); %#ok<ASGLU>

listeFicImages  = [];
decalageHoraire = [];

persistent persistent_decalageHoraire

%% Saisie du nom du fichier XML d�crivant les positions

[flag, nomFicNav, repImport] = params_OTUS_plotNav(repImport);
if ~flag
    return
end

%% Saisie des noms des images

[flag, listeFicImages, repImport] = uiSelectFiles('ExtensionFiles', {'.jpg', '.tif'},  'AllFilters', 1, ...
    'RepDefaut', fileparts(nomFicNav), ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tif image files');
if ~flag
    return
end

%% D�calage horaire

if AskTimeOffset
    if isempty(persistent_decalageHoraire)
        decalageHoraire = 0;
    else
        decalageHoraire = persistent_decalageHoraire;
    end
    [flag, decalageHoraire] = inputOneParametre('Time offset (Nav minus Images)', 'Nb of hours', ...
        'Value', decalageHoraire, 'Format', '%d');
    if ~flag
        return
    end
    persistent_decalageHoraire = decalageHoraire;
end
