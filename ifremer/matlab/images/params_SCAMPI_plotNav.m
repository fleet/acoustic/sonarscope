function [flag, nomFicNav, listeFicImages, decalageHoraire, repImport] = params_SCAMPI_plotNav(repImport)

listeFicImages  = [];
decalageHoraire = [];

%% Saisie du nom du fichier XML d�crivant les positions

str1 = 'Fichier de navigation Pagure';
str2 = 'Select the .xml navigation file';
[flag, nomFicNav] = my_uigetfile(fullfile(repImport, '*.xml'), Lang(str1,str2));
if ~flag
    return
end

%% Saisie des noms des images

[flag, listeFicImages, repImport] = uiSelectFiles('ExtensionFiles', '.jpg',  'AllFilters', 1, ...
    'RepDefaut', fileparts(nomFicNav), ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tif image files');
if ~flag
    return
end

%% D�calage horaire

[flag, decalageHoraire] = inputOneParametre('Time offset (Nav minus Images)', 'Nb of hours', 'Value', 0, 'Format', '%d');
if ~flag
    return
end
