function [flag, nbGaussiennes] = params_STATS_MelangeGaussiennes

persistent persistent_nbGaussiennes

if isempty(persistent_nbGaussiennes)
    nbGaussiennes = 2;
else
    nbGaussiennes = persistent_nbGaussiennes;
end

str1 = 'Modélisation d''une DDP par un mélanfe de gaussiennes';
str2 = 'DDP fitting with multiple separated gaussian ddp';
str3 = 'Nombre de gaussiennes';
str4 = 'Number of gaussians';
[flag, nbGaussiennes] = inputOneParametre(Lang(str1,str2), Lang(str3,str4), ...
    'Value', nbGaussiennes, 'MinValue', 1, 'MaxValue', 10, 'Format', '%d');
if ~flag
    nbGaussiennes = [];
    return
end
persistent_nbGaussiennes = nbGaussiennes;
