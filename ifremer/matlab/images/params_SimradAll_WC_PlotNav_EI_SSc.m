function [flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag, maskOverlapingBeams, repImport] ...
    = params_SimradAll_WC_PlotNav_EI_SSc(repImport, varargin)

persistent persistent_Quantile1 persistent_Quantile2 persistent_CLim persistent_ALim persistent_SelectionDataName

[varargin, askCorrectionTVG]       = getPropertyValue(varargin, 'askCorrectionTVG',       0);
[varargin, Ext]                    = getPropertyValue(varargin, 'Ext',                    '.xml');
[varargin, flagQuestion]           = getPropertyValue(varargin, 'AskQuestion',            1);
[varargin, askMaskOverlapingBeams] = getPropertyValue(varargin, 'askMaskOverlapingBeams', 1);

DataName            = [];
TypeUnit            = [];
CorrectionTVG       = [];
Quantile1           = [];
Quantile2           = [];
CLim                = [];
ALim                = [];
nomDirCSVOut        = [];
Tag                 = [];
maskOverlapingBeams = [];

%% Liste des fichiers .xml

[flag, listFileNames, repImport] = uiSelectFiles('ExtensionFiles', Ext, repImport, ...
    'RepDefaut', repImport, varargin{:});
if ~flag || isempty(listFileNames)
    flag = 0;
    return
end

%% Superposition d'une donn�e sur la nav ?

if flagQuestion
    str1 = 'Voulez-vous visualiser une donn�e au dessus de la navigation ?';
    str2 = 'Do you want to display a data over the navigation ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 2
        return
    end
end

%% 20, 30, 40 log ou Ts ou Sv

if askCorrectionTVG
    [flag, CorrectionTVG] = question_CompensationTVG_WC;
    if ~flag
        return
    end
else
    CorrectionTVG = 30;
end

%% S�lection de la donn�e � superposer

if isempty(persistent_SelectionDataName)
    InitialValue = 8;
else
    InitialValue = persistent_SelectionDataName;
end
listDataNames{1}     = 'Max';
listDataNames{end+1} = 'Min';
listDataNames{end+1} = 'Range';
listDataNames{end+1} = 'Mean';
listDataNames{end+1} = 'Median';
listDataNames{end+1} = 'Std';
listDataNames{end+1} = 'Quantile1';
listDataNames{end+1} = 'Quantile2';
listDataNames{end+1} = 'QuantilesRange';
str1 = 'S�lectionnez la donn�e � superposer � la navigation';
str2 = 'Select the data you want to plot over the navigation';
[repDataName, flag] = my_listdlg(Lang(str1,str2), listDataNames, 'SelectionMode', 'Single', 'InitialValue', InitialValue);
if ~flag
    return
end
DataName = listDataNames{repDataName};
persistent_SelectionDataName = repDataName;

%% Saisie des quantiles

switch repDataName
    case 8
        if isempty(persistent_Quantile1)
            Quantile2 = 99;
        else
            Quantile2 = persistent_Quantile1;
        end
        
        p = ClParametre('Name', 'Quantile2', 'Unit', '%', 'MinValue', 0, 'MaxValue',  100, 'Value',  Quantile2);
        a = StyledParametreDialog('params', p, 'Title', 'Quantile values');
        a.sizes = [0 -2  -1  -2 -1 -1 -1 25];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        Quantile2 = a.getParamsValue;
        persistent_Quantile1 = Quantile2;
        Quantile1 = 1;
        
    case 9
        if isempty(persistent_Quantile1)
            Quantile1 = 1;
        else
            Quantile1 = persistent_Quantile1;
        end
        if isempty(persistent_Quantile2)
            Quantile2 = 1;
        else
            Quantile2 = persistent_Quantile2;
        end
        
        p    = ClParametre('Name', 'Quantile 1', 'Unit', '%', 'MinValue', 0, 'MaxValue',  99, 'Value', Quantile1);
        p(2) = ClParametre('Name', 'Quantile 2', 'Unit', '%', 'MinValue', 1, 'MaxValue', 100, 'Value', Quantile2);
        a = StyledParametreDialog('params', p, 'Title', 'Quantile values');
        a.sizes = [0 -2  -1  -2 -1 -1 -1 25];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        X = a.getParamsValue;
        Quantile1 = X(1);
        Quantile2 = X(2);
        persistent_Quantile1 = Quantile1;
        persistent_Quantile2 = Quantile2;
end

%% Masquage par les angles ?

str1 = 'Voulez-vous limiter le calcul du signal par les angles ?';
str2 = 'Do you want to limitate the signals computation by the angles ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    if isempty(persistent_ALim)
        ALim = [-30 30];
    else
        ALim = persistent_ALim;
    end
    [flag, ALim] = saisie_CLim(ALim(1), ALim(2), 'deg', 'SignalName', 'Angles');
    if ~flag
        return
    end
    persistent_ALim = ALim;
end

%% maskOverlapingBeams

if askMaskOverlapingBeams
    str1 = 'TODO';
    str2 = 'Do you want to mask the overlapping beams if three sounder is dualRx ?';
    [ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    maskOverlapingBeams = (ind == 1);
end

%% D�finition des limites de couleur

% if get_LevelQuestion >=3
    str1 = 'Limites des couleurs du trac�';
    str2 = 'Color limits of the plot';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Auto', 'Manual');
    if ~flag
        return
    end
    if rep == 2 %% Saisie de CLim
        if isempty(persistent_CLim)
            CLim = [-Inf Inf];
        else
            CLim = persistent_CLim;
        end
        [flag, CLim] = saisie_CLim(CLim(1), CLim(2), 'dB', 'SignalName', DataName);
        if ~flag
            return
        end
        persistent_CLim = CLim;
    end
% end

%% Type de donn�e pour le calcul de la moyenne

str = {'dB'; 'Amp'; 'Enr'};
[rep, flag] = my_listdlg('WC reflectivity status :', str, 'SelectionMode', 'Single');
if ~flag
    return
end
TypeUnit = str{rep};

%% Save results

str1 = 'Voulez-vous sauver les r�sultats dans des fichiers .csv ?';
str2 = 'Do you want to export the results in .csv files ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    str1 = 'S�lectionnez le r�pertoire de sauvegarde des fichiers .csv';
    str2 = 'Select the folder where the .csv files will be saved';
    [flag, nomDirCSVOut] = my_uigetdir(repImport, Lang(str1,str2));
end
if ~flag
    return
end

%% Tag

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end
