function [flag, AngleLim, CorrectionTVG, TypeMeanUnit] = params_SummedWaterColumn(varargin)

persistent persistent_AngleLim persistent_indTVG persistent_TypeMeanUnit

[varargin, indTVG]       = getPropertyValue(varargin, 'indTVG',       []);
[varargin, TypeMeanUnit] = getPropertyValue(varargin, 'TypeMeanUnit', []);
[varargin, AngleLim]     = getPropertyValue(varargin, 'AngleLim',     []); %#ok<ASGLU>

%% Secteur angulaire pour r�aliser le moyennage

if isempty(AngleLim)
    if isempty(persistent_AngleLim)
        AngleLim = [-90 90];
    else
        AngleLim = persistent_AngleLim;
    end
end
str1 = 'Limites du secteur angulaire � consid�rer';
str2 = 'Angular limit of WC averaging';
p    = ClParametre('Name', Lang('Valeur minimale', 'Min value'), 'Unit', 'deg', 'Value', AngleLim(1));
p(2) = ClParametre('Name', Lang('Valeur maximale', 'Max value'), 'Unit', 'deg', 'Value', AngleLim(2));
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
AngleLim = a.getParamsValue;
persistent_AngleLim = AngleLim;

%% Choix de la TVG � itiliser

if isempty(indTVG)
    if isempty(persistent_indTVG)
        indTVG = 2;
    else
        indTVG = persistent_indTVG;
    end
end
% TODO limit� aux corrections log pour le moment car il faut am�liorer import_SummedWaterColumn_All 
[flag, CorrectionTVG, indTVG] = question_CompensationTVG_WC('InitialValue', indTVG, 'LogOnly', true);
if ~flag
    return
end
persistent_indTVG = indTVG;

%% Type d'unit� pour r�aliser le moyennage

if isempty(TypeMeanUnit)
    if isempty(persistent_TypeMeanUnit)
        TypeMeanUnit = 1;
    else
        TypeMeanUnit = persistent_TypeMeanUnit;
    end
end
str = {'dB'; 'Amp'; 'Enr'};
[TypeMeanUnit, flag] = my_listdlg('WC reflectivity status :', str, 'SelectionMode', 'Single', 'InitialValue', TypeMeanUnit);
if ~flag
    return
end
persistent_TypeMeanUnit = TypeMeanUnit;
