function [flag, listeFic, pathname, repImport, repExport] = params_WCExportAlongTrackInASCII(repImport, repExport)

pathname = [];

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.xml', ...
    'RepDefaut', repImport, 'ChaineIncluse', 'WCPath');
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, pathname] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = pathname;
