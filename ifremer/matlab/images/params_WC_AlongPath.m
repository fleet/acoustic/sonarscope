function [flag, listeFic, pathname, listFicCSV, Tag, typeOutputFormat, repImport, repExport] =  params_WC_AlongPath(repImport, repExport)

listFicCSV       = [];
pathname         = [];
Tag              = [];
typeOutputFormat = [];

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, ...
    'RepDefaut', repImport, 'ChaineIncluse', '_WCCube', ...
    'Entete', 'IFREMER - SonarScope - Select WC 3D matrix file', 'AllFilters', 1);
if ~flag
    return
end

%% Select a path file

% listFicCSV{1} = 'Y:\private\ifremer\Carla\s7k\P162\Export_profile_P162\Export_profile_3DV_P162.csv';
[flag, listFicCSV, repImport] = uiSelectFiles('ExtensionFiles', '.csv', 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select the path file');
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers';
str2 = 'Select or create the directory in which the files will be created';
[flag, pathname] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = pathname;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% Type de fichier en sortie

str = {'XML-Bin'; 'Netcdf'};
[typeOutputFormat, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
