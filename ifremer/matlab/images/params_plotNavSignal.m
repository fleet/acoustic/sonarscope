function [flag, signalName, Mute, modePlotData, step] = params_plotNavSignal(listSignals)

Mute         = [];
modePlotData = [];

[flag, ~, signalName, step] = question_SelectASignal(listSignals);
if ~flag
    return
end

[modePlotData, flag] = my_listdlg('Mode of signal display (Scatter plots allow to modify the colors using menu "Add-on / Axis / CLim for scatter plots". (Warning : Scatter plots use more memory)', {'Classical'; 'Scatter plots'}, 'SelectionMode', 'Single');
if ~flag
    return
end

[rep, flag] = my_listdlg('Color limits of the signals', {'Manual'; 'Auto'}, 'SelectionMode', 'Single');
if ~flag
    return
end
Mute = (rep == 2);
