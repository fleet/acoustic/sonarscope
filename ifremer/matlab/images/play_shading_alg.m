% Modification des paramètres d'un fichier .alg
%
% Syntax
%   play_shading_alg(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_ermapper_ers
%
% Name-Value Pair Arguments
%   Azimuth   : Azimuth (deg) (220 par defaut)
%   Elevation : Elevation (deg) (80 par defaut)
%
% Examples
%   nomFic = 'C:\TEMP\ppppp_shading.alg';
%   play_shading_alg(nomFic, 'Azimuth', 0, 'Elevation', 45)
%
% See also cl_ermapper_ers cl_ermapper_ers/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function play_shading_alg(nomFicAlg, varargin)

[varargin, Azimuth]   = getPropertyValue(varargin, 'Azimuth',   220);
[varargin, Elevation] = getPropertyValue(varargin, 'Elevation', 80);
[varargin, CLim]      = getPropertyValue(varargin, 'CLim',      []); %#ok<ASGLU>

fid = fopen(nomFicAlg, 'r');
if fid == -1
    return
end
Lignes = [];
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    if contains(tline, 'SunAzimuth')
        tline = sprintf('SunAzimuth = %d:0:0', floor(Azimuth));
    end
    if contains(tline, 'SunAngle')
        tline = sprintf('SunAngle = %d:0:0', floor(Elevation));
    end
    if contains(tline, 'MinimumInputValue')
        tline = sprintf('MinimumInputValue = %f', CLim(1));
    end
    if contains(tline, 'MaximumInputValue')
        tline = sprintf('MaximumInputValue = %f', CLim(2));
    end
    Lignes{end+1} = tline; %#ok<AGROW>
end
fclose(fid);

fid = fopen(nomFicAlg, 'w+');
for k=1:length(Lignes)
    fprintf(fid, '%s\n', Lignes{k});
end
fclose(fid);

winopen(nomFicAlg)
