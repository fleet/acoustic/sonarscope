function flag = process_WaterColumnPlumeDetection_ping(DataEchogram, nomDirRaw, k, nbPings, ...
    ParamsOut, flagDisplay, Fig, hAxe, nomFicRaw, nomDirOut, Tag, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

[~, nomSimple] = fileparts(nomDirRaw);
fprintf('Processing ping %5d/%5d of %s\n', k, nbPings, nomSimple);

iPing = DataEchogram.iPing(k);
xBab  = DataEchogram.xBab(k);
xtri  = DataEchogram.xTri(k);
Depth = DataEchogram.Depth(k);

%% Lecture de l'�chogramme

if isfield(DataEchogram, 'Immersion')
    Immer = DataEchogram.Immersion(k);
else
    Immer = 0;
end
if isnan(Immer)
    Immer = 0; % Au cas o�
end

nomSousRepertoire = sprintf('%03d', floor(iPing/100));
nomDir3 = fullfile(nomDirRaw, nomSousRepertoire);
nomFic1 = sprintf('%05d.png', iPing);
nomFic2 = fullfile(nomDir3, nomFic1);
if ~exist(nomFic2, 'file')
    flag = 0;
    return
end

[I, map] = imread(nomFic2); %#ok<ASGLU>
xEIImage = linspace(double(xBab), double(xtri), size(I,2));
z = linspace(double(Immer), double(Depth), size(I,1));

DT = cl_image.indDataType('Reflectivity');
subNaN = (I == 0);
I(subNaN) = 1;
WC = cl_image('Image', I, 'x', xEIImage, 'y', z, 'DataType', DT, 'GeometryType', cl_image.indGeometryType('DepthAcrossDist'));

%% Shape recognation

[flag, a] = crossCorrelationMoving(WC, ParamsOut);
if ~flag
    return
end
J = get_Image(a);
J(subNaN) = 0;

%% Affichage du r�sultat

switch ParamsOut.TypeAlgo
    case 1 % RMS
        CLim = a.CLim;
        CLim = [CLim(2)-(CLim(2)-CLim(1))/8 CLim(2)];
    case 2 % Crosscorrelation
        CLim = [-8*1000 0];
end


if flagDisplay
    figure(Fig)
    set(Fig, 'CurrentAxes', hAxe(1))
    Title = sprintf('%s - %d/%d - Ping=%d', nomFicRaw, k, nbPings, iPing);
    imagesc(xEIImage, z, I); colormap(jet(256)); colorbar
    axis xy; axis equal; axis tight; title(Title, 'Interpreter', 'none');
    set(Fig, 'CurrentAxes', hAxe(2))
    Title = sprintf('%s - %d/%d - Ping=%d', nomFicRaw, k, nbPings, iPing);
    imagesc(xEIImage, z, J, CLim); colorbar
    axis xy; axis equal; axis tight; title(Title, 'Interpreter', 'none');
end

%% Export du r�sultat

if ~exist(nomDirOut, 'dir')
    status = mkdir(nomDirOut);
    if ~status
        messageErreur(nomDirOut)
        flag = 0;
        return
    end
end

nomDir = fullfile(nomDirOut, [nomFicRaw Tag]);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

nomDir1 = sprintf('%03d', floor(iPing/100));
nomDir = fullfile(nomDir, nomDir1);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

nomFic1 = sprintf('%05d.png', iPing);
nomFic2 = fullfile(nomDir, nomFic1);

[Indexed, flag] = Intensity2Indexed(a, 'CLim', CLim, 'NoStats');
if ~flag
    return
end
Indexed.Image(subNaN) = 255;
flag = export_gif(Indexed, nomFic2);
if ~flag
    return
end

% J = mat2ind(J, CLim, 64);
% imwrite(J, jet, nomFic2) % CLim

%% The end

flag = 1;
