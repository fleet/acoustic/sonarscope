% Quantification d'une image definie par les centres de classe
%
% Syntax
%   Y = quantify_byBins(X, bins)
%
% Input Arguments
%   X    : Image de depart
%   bins : Centres des classes
%
% Output Arguments
%   Y    : Image de d'arrivee
%
% Examples
%
% See also quantify Authors
% Authors : JMA
%--------------------------------------------------------------------------

function Y = quantify_byBins(X, bins)

% Y = NaN(size(X), 'single');
% X = single(X);
% bins =[bins(1:end-1)+diff(bins)/2 Inf];
% for i=1:length(bins)
%     sub = find(X <= bins(i));
%     Y(sub) = i;
%     X(sub) = NaN;
% end

Y = NaN(size(X), 'single');
X = single(X(:,:));
sub = (X <= bins(1));
X(sub) = NaN;
for i=2:length(bins)
    sub = find(X <= bins(i));
    Y(sub) = i-1;
    X(sub) = NaN;
end
% sonarScope(Y)
