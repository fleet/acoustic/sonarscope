% Recherche du decalage d'une image par rapport a une autre
% Les images peuvent etre fournies de 2 manieres differentes :
%   - directement sous forme de matrice
%   - dans une figure ou un axe. Dans ce cas, l'operation se fait par
%     rapport a la partie affichee (zoom active sur la partie interessante)
%
% Syntax
%   [dL, dC, dx, dy] = register(I1, I2)
% 
% Input Arguments 
%   I1 : Image de reference
%   I2 : Deuxieme image 
% 
% Name-only Arguments
%   2D : Recherche deu decalage par intercorrelation 2D (methode efficace mais couteuse)
%
% Output Arguments 
%   lD : Decalage en ligne
%   cD : Decalage en colonne
%   dx : Decalage en colonne dans l'unite de l'axe (cas d'une image recuperee dans une figure)
%   dy : Decalage en ligne dans l'unite de l'axe (cas d'une image recuperee dans une figure)
%
% Remarks : Si l'on ne precise pas l'option '2D', la recherche du decalage se fait
%           separement sur les axes horizontaux et vertivcaux en sommant l'image verticalement et
%           horizontalement. Cette mthode convient sur certaines images du
%           genre "objet pose sur un fond uniforme"
%
% Examples
%   I = Lena;
%   subl = 120:150; subc = 120:150;
%   I1 = I(subl, subc);
%   I2 = I(subl+4, subc-2);
%   figure, imagesc([I1;I2]); colormap(gray(256))
%
%   [dL, dC, dx, dy] = register(I1, I2, 'Method', 1)
%   [dL, dC, dx, dy] = register(I1, I2, 'Method', 2)
%
%   fig1 = figure;
%   imagesc(I); colormap(gray(256))
%   axis([123 184 108 190])
%   fig2 = figure;
%   imagesc(I); colormap(gray(256))
%   axis([128 165 166 185])
%   axis([125 188 116 208])
%
%   [dL, dC, dx, dy] = register(fig1, fig2)
%   [dL, dC, dx, dy] = register(fig1, fig2, '2D')
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [dL, dC, dx, dy] = register(I1, I2, varargin)

[varargin, ValNanI1] = getPropertyValue(varargin, 'ValNanI1', NaN);
[varargin, ValNanI2] = getPropertyValue(varargin, 'ValNanI2', NaN);
[varargin, Method]   = getPropertyValue(varargin, 'Method', 1); %#ok<ASGLU>

%% R�cup�ration de l'image de r�f�rence si c'est un handle qui est pass�

if numel(I1) == 1
    if ishandle(I1)
        if isAGraphicElement(I1,'figure') || isAGraphicElement(I1, 'axes')
            hColorbar = findobj(I1, 'Tag', 'TMW_COLORBAR');
            hImages = findobj(I1, 'type', 'image');
            hI1 = setdiff(hImages, hColorbar);
        else
            if ~isAGraphicElement(I1, 'image')
                my_warndlg('images/register : La premiere variable doit etre soit une matricxe soit le numero d''une figure ou le handle d''une image ou d''un axe', 1);
                return
            end
        end
    else
        my_warndlg('images/register : La premiere variable doit etre soit une matricxe soit le numero d''une figure', 1);
        return
    end

    I1 = get(hI1, 'CData');
    XLim1 = get(get(hI1, 'Parent'), 'XLim');
    YLim1 = get(get(hI1, 'Parent'), 'YLim');

    XData1 = get(hI1, 'XData');
    if length(XData1) == 2
        XData1 = linspace(XData1(1), XData1(2), size(I1,2));
    end

    YData1 = get(hI1, 'YData');
    if length(YData1) == 2
        YData1 = linspace(YData1(1), YData1(2), size(I1,1));
    end

    subX = find((XData1 >= XLim1(1)) & (XData1 <= XLim1(2)));
    subY = find((YData1 >= YLim1(1)) & (YData1 <= YLim1(2)));
    I1 = I1(subY,subX);
    XData1 = XData1(subX);
    YData1 = YData1(subY);
else
    XData1 = 1:size(I1,2);
    YData1 = 1:size(I1,1);
end

%% R�cuperation de l'image 2 si c'est un handle qui est passe

if numel(I2) == 1
    if ishandle(I2)
        if isAGraphicElement(I2, 'figure') || isAGraphicElement(I2, 'axes')
            hColorbar = findobj(I2, 'Tag', 'TMW_COLORBAR');
            hImages = findobj(I2, 'type', 'image');
            hI2 = setdiff(hImages, hColorbar);
        else
            if ~isAGraphicElement(I2, 'image')
                my_warndlg('images/register : La premiere variable doit etre soit une matricxe soit le numero d''une figure ou le handle d''une image ou d''un axe', 1);
                return
            end
        end
    else
        my_warndlg('images/register : La premiere variable doit etre soit une matricxe soit le numero d''une figure', 1);
        return
    end

    I2 = get(hI2, 'CData');
    XLim2 = get(get(hI2, 'Parent'), 'XLim');
    YLim2 = get(get(hI2, 'Parent'), 'YLim');

    XData2 = get(hI2, 'XData');
    if length(XData2) == 2
        XData2 = linspace(XData2(1), XData2(2), size(I2,2));
    end

    YData2 = get(hI2, 'YData');
    if length(YData2) == 2
        YData2 = linspace(YData2(1), YData2(2), size(I2,1));
    end

    subX = find((XData2 >= XLim2(1)) & (XData2 <= XLim2(2)));
    subY = find((YData2 >= YLim2(1)) & (YData2 <= YLim2(2)));
    I2 = I2(subY,subX);
    XData2 = XData2(subX);
    YData2 = YData2(subY);
else
    XData2 = 1:size(I2,2);
    YData2 = 1:size(I1,1);
end

%% Calcul du d�calage

if Method == 2 % 2D
    I1 = singleUnlessDouble(I1, ValNanI1);
    [px, py] = gradient(I1, 1, 1);
    I1 = sqrt(px .^ 2 + py .^ 2);

    I2 = singleUnlessDouble(I2, ValNanI2);
    [px, py] = gradient(I2, 1, 1);
    I2 = sqrt(px .^ 2 + py .^ 2);

    [C, x, y, dC, dL]  = my_xcorr2_byFFT(I1, I2); %#ok<ASGLU>
%     figure; imagesc(x, y, C); grid on; axis xy;
%     figure(9999)
%     subplot(2,2,1); imagesc(I1); grid on
%     subplot(2,2,2); imagesc(I2); grid on
%     subplot(2,2,3); imagesc(x, y, C); grid on

else % 2x1D
    % Recherche du d�calage horizontal

    sumI1 = mean(I1, 1, 'omitnan');
    sumI2 = mean(I2, 1, 'omitnan');
    [C, x, dC] = my_xcorr(diff(sumI1), diff(sumI2)); %#ok<ASGLU>
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(sumI1, 'b'); grid on; hold on; plot(sumI2, 'r');

    % Recherche du d�calage vertical

    sumI1 = mean(I1, 2, 'omitnan');
    sumI2 = mean(I2, 2, 'omitnan');
    [C, x, dL]  = my_xcorr(diff(sumI1), diff(sumI2)); %#ok<ASGLU>
%     figure; plot(sumI1, 'b');  hold on; plot(sumI2, 'r'); grid on;
end

dx = (XData1(1) - XData2(1)) + dC * (XData1(2)-XData1(1));
dy = (YData1(1) - YData2(1)) + dL * (YData1(2)-YData1(1));
