function w = robustWeight(d , varargin)

[varargin, type] = getPropertyValue(varargin, 'type', 1);
[varargin, S]    = getPropertyValue(varargin, 'S',    1); %#ok<ASGLU>

d2 = d.^2;
S2 = S^2;
epsilon = 0.001;

switch type
    case 1 %tukey
        w = (1-d2 ./ S2).^2 .*( d2 < S2 );
    case 2 %welsch
        w  = exp(-0.5 * d2 ./ S2);
    case 3 %Geman-McClure
        w  = 1 ./ (1+ d2 ./ S2).^2;
    case 4 %Cauchy
        w  = 1 ./ (1+ d2 ./ S2);
    case 5 %~L1
        w  = 1 ./ sqrt( epsilon + d2 ./ S2);
end

