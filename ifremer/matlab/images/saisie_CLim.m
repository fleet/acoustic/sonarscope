% [flag, ZLim] = saisie_CLim(1, 2, 'm', 'SignalName', 'Height', 'hFigHisto', 87698)

function [flag, ZLim] = saisie_CLim(MinValue, MaxValue, Unit, varargin)

ZLim = [];

[varargin, SignalName] = getPropertyValue(varargin, 'SignalName', []);
[varargin, hFigHisto]  = getPropertyValue(varargin, 'hFigHisto', []); %#ok<ASGLU>

if ishandle(hFigHisto)
    figure(hFigHisto)
end

if isempty(SignalName)
    str1 = 'Saisie des limites de la table de couleurs';
    str2 = 'Select colortable limits"';
else
    str1 = sprintf('Saisie des limites de la table de couleurs pour "%s"', SignalName);
    str2 = sprintf('Select colortable limits for "%s"', SignalName);
end
p    = ClParametre('Name', Lang('Valeur maximale', 'Max value'), ...
    'Unit', Unit, 'Value', MaxValue);
p(2) = ClParametre('Name', Lang('Valeur minimale', 'Min value'), ...
    'Unit', Unit, 'Value', MinValue);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;

my_close(hFigHisto);

flag = a.okPressedOut;
if ~flag
    return
end
ZLim = a.getParamsValue;
ZLim = sort(ZLim);
