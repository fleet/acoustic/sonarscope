function [flag, StopRegul] = saisie_MarkovRegularization

persistent persistent_StopRegul

if isempty(persistent_StopRegul)
    StopRegul = 0.02;
else
    StopRegul = persistent_StopRegul;
end

str1 = 'Crit�re � utiliser pour mettre fin � la r�gularisation';
str2 = 'Criteria to use for the regularization termination';
p = ClParametre('Name', Lang('Pourcentage de pixels commut�s','Pourcentage of muted pixels'), ...
    'Unit', '%',  'MinValue', 0.01, 'MaxValue', 1, 'Value', StopRegul);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
StopRegul = a.getParamsValue;

persistent_StopRegul = StopRegul;
