% [flag, Backup] = saisie_backupFrequency(22)
% [flag, Backup] = saisie_backupFrequency(1)

function [flag, Backup] = saisie_backupFrequency(nbProfils, varargin)

persistent lastBackup

[varargin, maxvalue] = getPropertyValue(varargin, 'maxvalue', Inf);
[varargin, QL]       = getPropertyValue(varargin, 'QL', 2); %#ok<ASGLU>

flag = 1;

if isempty(lastBackup)
    Default = 10;
else
    Default = lastBackup;
end

if nbProfils > 1
    if get_LevelQuestion >= QL
        str1 = 'Frequence de sauvegarde de la mosa�que sur disque.';
        str2 = 'Backup frequency of the mosaic on disk.';
        p = ClParametre('Name', Lang('Nombre de fichiers','Number of files'), ...
            'MinValue', 1, 'MaxValue', maxvalue, 'Value', Default, 'Format', '%d');
        a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
        % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
        a.sizes = [0 -2 0 0 0 -1 0 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        Backup = a.getParamsValue;
        lastBackup = Backup;
    else
        Backup = Default;
    end
else
    Backup = 1;
end
