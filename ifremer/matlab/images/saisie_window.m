% Saisie de la taille d'une fen�tre d'interpolation
%
% Syntax
%   [flag, W] = saisie_window(W, ...)
%
% Input Arguments
%   W : Initialisation de la fen�tre : [hauteur, Largeur]
%
% Name-Value Pair Arguments
%   maxvalue : Largeur et hauteurs max (11 par d�faut)
%   Titre    : Titre de la fen�tre
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   W    : Fen�tre saisie : [hauteur, Largeur]
%
% Examples
%   [flag, W] = saisie_window([3 5])
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, W] = saisie_window(W, varargin)

[varargin, maxvalue] = getPropertyValue(varargin, 'maxvalue', [11 11]);
[varargin, Titre]    = getPropertyValue(varargin, 'Titre',    'IFREMER-SonarScope:Input window size'); %#ok<ASGLU>

if isempty(Titre)
    str1 = 'Fen�tre de traitement';
    str2 = 'Window processing';
    Titre = Lang(str1,str2);
end

p(1) = ClParametre('Name', Lang('Hauteur', 'Height'), 'Unit', 'Pixel', 'MinValue', 1, 'MaxValue', maxvalue(1), 'Value', W(1), 'Format', '%d');
p(2) = ClParametre('Name', Lang('Largeur', 'Width'),  'Unit', 'Pixel', 'MinValue', 1, 'MaxValue', maxvalue(2), 'Value', W(2), 'Format', '%d');
a = StyledSimpleParametreDialog('params', p, 'Title', Titre);
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    W = [];
    return
end
X = a.getParamsValue;
W = X(1:2);
