% Colorisation d'une image sonar.
%
% Syntax
%   ImageCoul = segmOnImage(I, Segm, ...)
%
% Input Arguments
%   I        : image sonar ou handle de la figure contenant cette image
%   Segm     : Image segmentee ou handle de la figure contenant cette image
%   colorMap : Table de couleur associee l'image segmentee
%
% Name-Value Pair Arguments
%   Map   : Table de couleur (jet par defaut)
%   alpha : Facteur de dosage de la couleur
%   CLim  : Bornes de rehaussement de contraste
%
% Output Arguments
%   [] : Auto-plot activation
%   ImageCoul : image sonar colorisee
%
% Examples
%   nomFic = getNomFicDatabase('RidesDeSable.mat')
%
%   I = loadmat(nomFic, 'nomVar', 'I');
%   S = loadmat(nomFic, 'nomVar', 'S');
%
%   segmOnImage(I, S, jet);
%   ImageCoul = segmOnImage(I, S, jet);
%   imwrite(ImageCoul, fullfile(my_tempdir, 'pppp.tif'))
%
%   fig1 = figure; imagesc(I); colormap(gray(256));
%   fig2 = figure; imagesc(S); colormap(jet(256));
%   segmOnImage(fig1, fig2, jet);
%
% See also Authors
% Authors : JMA
%------------------------------------------------------------------------------

function varargout = segmOnImage(I, Segm, varargin)

[varargin, Map]   = getPropertyValue(varargin, 'Map', jet(256));
[varargin, CLim]  = getPropertyValue(varargin, 'CLim', []);
[varargin, alpha] = getPropertyValue(varargin, 'alpha', 0.9); %#ok<ASGLU>

%% On teste si on a donn� l'image ou le numero de figure contenant l'image

if numel(I) == 1
    fig1 = I;
    h = findobj(fig1, 'Type', 'image');
    subColorbar = [];
    for i=1:length(h)
        if strcmp(get(h(i), 'Tag'), 'TMW_COLORBAR')
            subColorbar(end+1) = 1; %#ok<AGROW>
        end
    end
    h(subColorbar) = [];
    I = get(h(1), 'CData');
else
    fig1 = figure;
end

if numel(Segm) == 1
    fig2 = Segm;
    h = findobj(fig2, 'Type', 'image');
    subColorbar = [];
    for i=1:length(h)
        if strcmp(get(h(i), 'Tag'), 'TMW_COLORBAR')
            subColorbar(end+1) = 1; %#ok<AGROW>
        end
    end
    h(subColorbar) = [];
    
    Segm = get(h(1), 'CData');
else
    fig2 = figure;
end

%% Mise en conformite des tailles d'images

if ~isequal(size(I), size(Segm))
    Segm = stretchmat( Segm, size(I), 'nearest');
end
I = double(I);
Segm  = double(Segm);

%% Conversion de l'image segmentee en image 3 composantes (RVB)

C = gray2ind(mat2gray(Segm, [min(Segm(:)) max(Segm(:))]));
C = ind2rgb(C, Map);

%% On ram�ne l'image sonar a une dynamique comprise entre 0 et 1

if isempty(CLim)
    minImageSonar = min(I(:));
    maxImageSonar = max(I(:));
    I = (I - minImageSonar) / (maxImageSonar - minImageSonar);
else
    minImageSonar = CLim(1);
    maxImageSonar = CLim(2);
    I = (I - minImageSonar) / (maxImageSonar - minImageSonar);
    I(I < 0) = 0;
    I(I > 1) = 1;
end

%% Composition de l'image sonar color�e

ImageCoul = zeros([size(I), 3]);
for i=1:3
    ImageCoul(:,:,i) = alpha * I + (1-alpha) * C(:,:,i);
end

%% Auto-plot activation

if nargout == 0
    figure(fig1)
    hImage1 = imagesc(I); colorbar; colormap(gray(256));
    hAxe1 = get(hImage1, 'Parent');
    set(hAxe1, 'Tag', 'ImageSonar');
    title('Image sonar');
    
    figure(fig2)
    hImage2 = imagesc(Segm); colorbar; colormap(Map);
    hAxe2 = get(hImage2, 'Parent');
    set(hAxe2, 'Tag', 'Segm');
    title('Image segmentee');
    
    figure;
    hImage3 = imagesc(ImageCoul);
    hAxe3 = get(hImage3, 'Parent');
    set(hAxe3, 'Tag', 'ImageCoul');
    title('Image sonar coloree');
    
    commande = sprintf('set(gca, ''XLim'', get(findobj(%d, ''Tag'', ''ImageSonar''), ''XLim''), ''YLim'', get(findobj(%d, ''Tag'', ''ImageSonar''), ''YLim''))', ...
        fig1, fig1);
    set(hImage2, 'ButtonDownFcn', commande)
    set(hImage3, 'ButtonDownFcn', commande)
    
    helpdlg('Tu peux zoomer sur l''image sonar et cliquer sur les autres pour les asservir en position', 'Y''a du bon')
else
    varargout{1} = ImageCoul;
end

