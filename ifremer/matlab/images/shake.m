% Melange aleatoire des pixels d'une image
%
% Syntax
%   I2 = shake(I1)
%
% Input Arguments
%   I1 : Image originale
%
% Output Arguments
%   I2 : Image melangee
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   I1 = imread(nomFic);
%   figure; imagesc(I1); colorbar; colormap(gray(256));
%   histo1D(I1)
%
%   I2 = shake(I1);
%   figure; imagesc(I2); colorbar; colormap(gray(256));
%   histo1D(I2)
%
% See also stats LecFicLayerCaraibes Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function [I2, ind] = shake(I1)

k = rand(size(I1));
[~, ind] = sort(k(:));
I2 = reshape(I1(ind), size(I1));
