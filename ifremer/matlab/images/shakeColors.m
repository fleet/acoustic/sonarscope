% Melange aleatoire des couleurs (des valeurs) d'une image
%
% Syntax
%   I2 = shakeColors(I1)
%
% Input Arguments
%   I1 : Image originale
% 
% Output Arguments
%   I2 : Image melangee
% 
% Examples 
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m_LatLong_Bathymetry.ers');
%   [flag, a] = cl_image.import_ermapper(nomFic);
%   b = watershed(a, 'Seuil', 10);
%   I1 = get(b(1), 'Image');
%   figure; imagesc(I1); colorbar; colormap(jet(256));
%
%   I2 = shakeColors(I1);
%   figure; imagesc(I2); colorbar; colormap(jet(256));
%
% See also shake Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function I2 = shakeColors(I1)

listVal = unique(I1(:));
sub = ~isnan(listVal);
listVal = listVal(sub);
 
k = rand(length(listVal),1);
[~, ind] = sort(k(:));

I2 = I1;
sub = ~isnan(I1);
I2(sub) = listVal(ind(I1(sub)));
