% Filtre "Hard Threshold" sur une décomposition en ondelettes
%
% Syntax
%   x = HardThresh(y, t)
%
% Input Arguments
%   x : Noisy Data
%   t : Threshold
%
% Output Arguments
%   x  : Filtered Data
%
% Remarks : Part of WaveLab Version 802
%       Built Sunday, October 3, 1999 8:52:27 AM
%       This is Copyrighted Material
%       For Copying permissions see COPYING.m
%       Comments? e-mail wavelab@stat.stanford.edu
%
% Examples
%   x = HardThresh(y, t);
%
% See also toto titi Authors
% References : Copyright (c) 1993-5.  Jonathan Buckheit, David Donoho and Iain Johnstone
% Authors : AI
%-------------------------------------------------------------------------------

function x = HardThresh(y, t)
x   = y .* (abs(y) > t);
