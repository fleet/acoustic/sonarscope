% Noyau du filtre moyenneur intervenant uniquement sur les valeurs NaN
%
% Syntax
%   y = NoyauFillNan(x)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central si celui-ci n'est pas NaN
%       Moyenne des valeurs non NaN de x si x est lui-meme NaN.
% 
% Examples
%   NoyauFillNan([1 2 3; 4 pi 5; 6 7 8])
%   NoyauFillNan([1 2 3; 4 NaN 5; 6 7 8])
%   NoyauFillNan([NaN 2 3; 4 NaN 5; NaN NaN 8])
%
% See also fillNaN Authors
% Authors : JMA
% VERSION  : $Id: NoyauFillNan.m,v 1.4 2003/12/15 16:11:34 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFillNan(x)

x = x(:);
i = ceil(length(x) / 2);
if isnan(x(i))
    y = my_nanmean(x);
else
    y = x(i);
end
