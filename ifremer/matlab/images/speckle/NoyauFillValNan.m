% Noyau du filtre du speckle TLineaire
%
% y = NoyauFillValNaN(x, ValNaN)
%
% Input Arguments 
%   x      : Vecteur d'entree
%   ValNaN : Valeur representant les non-valeurs
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFillValNaN([1 2 3; 4 pi 5; 6 7 8], 1, 1)
%   NoyauFillValNaN([1 2 3; 4 NaN 5; 6 7 8], 1, 2)
%   NoyauFillValNaN([NaN 2 3; 4 NaN 5; NaN NaN 8], 1, 3)
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFillValNaN.m,v 1.1 2002/03/01 16:38:12 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFillValNan(x, ValNaN)

x = x(:);
i = ceil(length(x) / 2);
if x(i) == ValNaN
    sub = find(x ~= ValNaN);
    if isempty(sub)
        y = ValNaN;
    else
        y = mean(x(sub));
    end
else
    y = x(i);
end
