% Noyau du filtre du speckle EAP
%
% y = NoyauFiltreEAP(x, L)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreEAP([1 2 3; 4 pi 5; 6 7 8],1)
%   NoyauFiltreEAP([1 2 3; 4 NaN 5; 6 7 8],2)
%   NoyauFiltreEAP([NaN 2 3; 4 NaN 5; NaN NaN 8],3)
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreEAP.m,v 1.1 2002/03/01 16:38:12 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreEAP(x,L)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);

if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);
        m = mean(x);
        v = var(x);
        u = ((1+1/L)/(v/m^2-1/L));
        if u >= 0
            io = 4*L*u*I/m;
            f = sqrt(io)*besselk(u+1-L,sqrt(io))*m/(besselk(u-L,sqrt(io))*2*u);
            if f > (10*m)
                y = m;
            else
                if isnan(f)
                    y = m;
                else
                    y = f;
                end
            end
        else
            y = m;
        end
    end
end