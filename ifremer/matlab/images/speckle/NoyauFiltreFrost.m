% Noyau du filtre du speckle de Frost
%
% y = NoyauFiltreFrost(x, L)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreFrost([1 2 3; 4 pi 5; 6 7 8],1)
%   NoyauFiltreFrost([1 2 3; 4 NaN 5; 6 7 8],2)
%   NoyauFiltreFrost([NaN 2 3; 4 NaN 5; NaN NaN 8],3)
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreFrost.m,v 1.1 2002/03/01 16:38:12 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreFrost(x, L)

%seuil = 10^-1;

x = abs(x(:));
n = ceil(length(x) / 2);
I = x(n);
if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);

        m = max(mean(x));%,seuil); %%% correction 4/01/2005
        v = var(x);
        p = length(x);
        a = sqrt(p);        %taille de la fenetre
        
        d = zeros(1,p);
        M = zeros(1,p);
        Xn = zeros(1,p);
        for n=1:p
            d(n)  = sqrt((a+1-(fix((n-1)/(2*a+1))+1))^2 + (a+1-(rem(n-1,(2*a+1))+1))^2);
            M(n)  = exp(-L * sqrt(v) * d(n) / m);
            Xn(n) = x(n) * M(n);
        end
        y = sum(Xn) / sum(M);
    end
end