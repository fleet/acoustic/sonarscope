% Noyau du filtre du speckle de Kuan
%
% y = NoyauFiltreKuan(x, L)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreKuan([1 2 3; 4 pi 5; 6 7 8],1)
%   NoyauFiltreKuan([1 2 3; 4 NaN 5; 6 7 8],2)
%   NoyauFiltreKuan([NaN 2 3; 4 NaN 5; NaN NaN 8],3)
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreKuan.m,v 1.1 2002/03/01 16:38:12 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreKuan(x, L)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);
if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);
        m = mean(x);
        v = var(x);
        gammai2 = (v / m^2);
        gammas2 = (1/L);
        
        if gammai2 == 0
            gammai2 = gammas2;
        end
        
        k = (1 - gammas2/gammai2) / (1 + gammas2);
        
        if (k>  0) && (k <= 1)
            y = m + k *(I - m);
        else
            y = m;
        end
    end
end