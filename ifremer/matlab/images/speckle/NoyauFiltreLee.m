% Noyau du filtre du speckle de Lee
%
% y = NoyauFiltreLee(x, L)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreLee([1 2 3; 4 pi 5; 6 7 8],1)
%   NoyauFiltreLee([1 2 3; 4 NaN 5; 6 7 8],2)
%   NoyauFiltreLee([NaN 2 3; 4 NaN 5; NaN NaN 8],3)
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreLee.m,v 1.1 2002/03/01 16:38:12 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreLee(x, L)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);
if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);
        
        % % Avant
        %  m = mean(x);
        %  v = var(x);
        
        % Modifi� le 24/11/2010
        n = length(x);
        m = sum(x) / n;
        x = x-m;
        v = sum(x.*x) / (n-1); % (n-1) for unbiaised estimator
        
        
        if v == 0
            k = 0;
        else
            k = (1 - (m*m)/(L*v));
        end
        
        if (k > 0) && (k <= 1)
            y = m + k *(I - m);
        else
            y = m;
        end
    end
end
