% Noyau du filtre du speckle MAP
%
% y = NoyauFiltreMAP(x, L)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreMAP([1 2 3; 4 pi 5; 6 7 8],1)
%   NoyauFiltreMAP([1 2 3; 4 NaN 5; 6 7 8],2)
%   NoyauFiltreMAP([NaN 2 3; 4 NaN 5; NaN NaN 8],3)
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreMAP.m,v 1.1 2002/03/01 16:38:12 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreMAP(x,L)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);

if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);
        m = mean(x);
        v = var(x);
        u = (1+1/L)/(v/m^2-1/L);
        if u>0
            y=1/(2*u)*(m*(u-1-L)+ sqrt(m^2*(u-1-L)^2+4*L*u*I*m));
        else
            y=m;
        end
    end
end