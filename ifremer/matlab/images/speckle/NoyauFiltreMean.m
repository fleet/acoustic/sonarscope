% Noyau du filtre du speckle moyenneur
%
% Syntax
%   y = NoyauFiltreMean(x)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreMean([1 2 3; 4 pi 5; 6 7 8])
%   NoyauFiltreMean([1 2 3; 4 NaN 5; 6 7 8])
%   NoyauFiltreMean([NaN 2 3; 4 3 5; NaN NaN 8])
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreMean.m,v 1.1 2002/04/03 15:28:59 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreMean(x)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);
if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);
        y = mean(x);
    end
end