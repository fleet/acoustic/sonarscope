% Noyau du filtre "moyenne du quantile 25 a 75"
%
% Syntax
%   y = NoyauFiltreMoyQuant25_75(x)
%
% Input Arguments 
%   x : Vecteur d'entree
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreMoyQuant25_75([1 2 3; 4 pi 5; 6 7 8])
%   NoyauFiltreMoyQuant25_75([1 2 3; 4 NaN 5; 6 7 8])
%   NoyauFiltreMoyQuant25_75([NaN 2 3; 4 3 5; NaN NaN 8])
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreMoyQuant25_75.m,v 1.1 2002/04/03 15:28:59 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreMoyQuant25_75(x)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);
if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);
        q = quantiles(x, [25 75]);
        sub = ((x >= q(1)) & (x <= q(2)));
        y = mean(x(sub));
    end
end