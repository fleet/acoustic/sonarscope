% Noyau du filtre du speckle TLineaire
%
% y = NoyauFiltreTLineaire(x, L, t)
%
% Input Arguments 
%   x : Vecteur d'entree
%   L : 
%   t : 
%
% Output Arguments
%   y : Valeur du point central filtre
% 
% Examples
%   NoyauFiltreTLineaire([1 2 3; 4 pi 5; 6 7 8], 1, 1)
%   NoyauFiltreTLineaire([1 2 3; 4 NaN 5; 6 7 8], 1, 2)
%   NoyauFiltreTLineaire([NaN 2 3; 4 NaN 5; NaN NaN 8], 1, 3)
%
% See also Authors
% Authors : JMA SB
% VERSION  : $Id: NoyauFiltreTLineaire.m,v 1.1 2002/03/01 16:38:12 augustin Exp $
%-------------------------------------------------------------------------------

function y = NoyauFiltreTLineaire(x, L, t)

x = x(:);
n = ceil(length(x) / 2);
I = x(n);
if isnan(I)
    y = NaN;
else
    sub = find(~isnan(x));
    if isempty(sub)
        y = NaN;
    else
        x = x(sub);
        xt = x .^ t;
        xt1 = x .^ (t+1);
        m = mean(x);
        mt = mean(xt);
        mt1 = mean(xt1);
%         v = var(x);
        vt = var(xt);
        
        if vt == 0
            vt = mt;
        end
        
        at = (mt1   *L / (L+t) - mt * m) / vt;
        if at<0
            y = at * I ^ t + m - at * mt;
        else
            y = m;
        end
    end
end