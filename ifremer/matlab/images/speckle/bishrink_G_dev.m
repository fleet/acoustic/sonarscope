% le filtre bishrink qui corresponde a une densite de probabilite de l'image utile Gaussienne
% et qui utilise une double estimation de la varience de l'image utile
% Utilisation :
%      [w1] = bishrink(y1,c_dev)
% ENTREE :
%      y1 - la valeur d'un coefficient bruite
%      c_dev - le coefficient de multiplication du y1
% SORTIE :
%      w1 - le coefficient debruite

function [w1] = bishrink_G_dev(y1,c_dev)
w1 = y1 .* c_dev;