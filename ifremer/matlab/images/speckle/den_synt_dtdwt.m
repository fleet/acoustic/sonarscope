% Debruitage adapte a l'homogeneite de la region. On fait un premier debruitage a l'aide de la TOCAD et d'une variante 
% de filtre bishrink. On fait la segmentation du resultat obtenu en regions qui correspondent aux differentes valeurs 
% de la variance locale du resultat. On filtre chaque telle region de l'image initiale avec une combinaison differente 
% de variantes de filtre bishrink.
%
% Syntax
%   [y, Nsig] = den_synt_dtdwt(x, status, ...)
%
% Input Arguments
%   x      : A noisy image
%   status : 1=Apprentissage et traitement, 2=Traitement
%
% Output Arguments
%   y    : The corresponding denoised image
%   Nsig : Ecart-type
%
% Examples
%   [y, Nsig] = denoising_dtdwt2(x, Nsig);
%
% See also Authors
% References : 
% Authors : AI
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [y, Nsig] = den_synt_dtdwt(x, status, varargin)

M = size(x, 1);

if status == 1 % Learning
    % Run local adaptive image denoising algorithm using dual-tree DWT with AntonB and the bishrink (only)
    [y1, Nsig1] = denoising_dtdwt(x, []);
    % Run local adaptive image denoising algorithm using dual-tree DWT with AntonB and the bishrink with gl est va + Wiener+ht
    [y2, Nsig2] = denoising_dtdwt1_mix_Wi_ht(x, []);
    % Run local adaptive image denoising algorithm using bishrink with global estimation of the variance and Anton_B
    [y3, Nsig3] = denoising_dtdwt_est_glo_var(x, []);
    % Run local adaptive image denoising algorithm using dual-tree DWT with Farras and the bishrink with gl est var+ Wiener+ht
    [y4, Nsig4] = denoising_dtdwt2_mix_Wi_ht(x, []);
    % Run local adaptive image denoising algorithm using dual-tree DWT with Farras and the bishrink (only)
    [y5, Nsig5] = denoising_dtdwt2(x, []);
    % Run local adaptive image denoising algorithm using dual-tree DWT with Farras and bishrink est gl va
    [y6, Nsig6] = denoising_dtdwt2_est_glo_var(x, []);
    % Synthesis of a new result
    % [yi,y1t,y2t,y3t,y4t,y5t,y6t] = synthesis(y1,y2,y3,y4,y5,y6,x,M);
    yi = synthesis(y1, y2, y3, y4, y5, y6, x, M);
    % Corrections
    y = fin_cor(y3, yi, M);

    Nsig = [Nsig1 Nsig2 Nsig3 Nsig4 Nsig5 Nsig6];
    
else % Process
    Nsig = varargin{1};
    % Run local adaptive image denoising algorithm using dual-tree DWT with AntonB and the bishrink (only)
    y1 = denoising_dtdwt(x, Nsig(1));
    % Run local adaptive image denoising algorithm using dual-tree DWT with AntonB and the bishrink with gl est va + Wiener+ht
    y2 = denoising_dtdwt1_mix_Wi_ht(x, Nsig(2));
    % Run local adaptive image denoising algorithm using bishrink with global estimation of the variance and Anton_B
    y3 = denoising_dtdwt_est_glo_var(x, Nsig(3));
    % Run local adaptive image denoising algorithm using dual-tree DWT with Farras and the bishrink with gl est var+ Wiener+ht
    y4 = denoising_dtdwt2_mix_Wi_ht(x, Nsig(4));
    % Run local adaptive image denoising algorithm using dual-tree DWT with Farras and the bishrink (only)
    y5 = denoising_dtdwt2(x, Nsig(5));
    % Run local adaptive image denoising algorithm using dual-tree DWT with Farras and bishrink est gl va
    y6 = denoising_dtdwt2_est_glo_var(x, Nsig(6));
    % Synthesis of a new result
    % [yi,y1t,y2t,y3t,y4t,y5t,y6t] = synthesis(y1,y2,y3,y4,y5,y6,x,M);
    yi = synthesis(y1, y2, y3, y4, y5, y6, x, M);
    % Corrections
    y = fin_cor(y3, yi, M);
end
