% Local Adaptive Image Denoising Algorithm with controlled double estimation of the variance of
% the useful image, 2 models of the details pdfs and hard_thresholding. At the first 3 iterations
% is used the bishrink with decv, at the forth iteration is used the classical zero order Wiener
% filter and at the last iteration (the fifth) is used the hard
% thresholding with 3 sigma
%
% Syntax
%   [y, Nsig] = denoising_dtdwt1_mix_Wi_ht(x, Nsig)
%
% Input Arguments
%   x    : A noisy image
%   Nsig : [] ou Ecart-type
%
% Output Arguments
%   y : The corresponding denoised image
%   Nsig : Ecart-type
%
% Remarks : Si Nsig=[] sa valeur est estimée sinon c'est la valeur
% transmise qui est proise en compte
%
% Examples
%   [y, Nsig] = denoising_dtdwt2(x, Nsig);
%
% See also Authors
% References : 
% Authors : AI
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [y, Nsig] = denoising_dtdwt1_mix_Wi_ht(x, Nsig)

% Initialisation
windowsize = 7;
windowfilt = ones(1,windowsize)/windowsize;
J = 6;
I = sqrt(-1);

% Extension symetrique
L = length(x); % la dimension de l'image originale.
% N = L+2^J;     % la dimension apres l'extension.
x = symextend(x,2^(J-1));

nomFic = getNomFicDatabase('nor_dualtree.mat');
nor = loadmat(nomFic, 'nomVar', 'nor');    % run normaliz_coefcalc_dual_tree to generate this mat file.

% Calcul de la transformee en ondelettes directe
% Either FSfarras or AntonB function can be used to compute the stage 1 filters
%[Faf, Fsf] = FSfarras;
[Faf, Fsf] = AntonB;
[af, sf] = dualfilt1;
W = cplxdual2D(x, J, Faf, af);
W = normcoef(W,J,nor);

% L'estimation de la varience du bruit

if isempty(Nsig)
    tmp = W{1}{1}{1}{1};
    Nsig = median(abs(tmp(:))) / 0.6745;
end

% La construction de l'image des coefficients parents
for scale = 1:J-1
    for dir = 1:2
        for dir1 = 1:3

            % Le calcul des coefficients complexes
            % La partie reelle
            Y_coef_real = W{scale}{1}{dir}{dir1};
            % La partie imaginaire
            Y_coef_imag = W{scale}{2}{dir}{dir1};
            % Les coefficients parents correspondents
            %La partie reelle
            Y_parent_real = W{scale+1}{1}{dir}{dir1};
            %La partie imaginaire
            Y_parent_imag = W{scale+1}{2}{dir}{dir1};
            % L'extension de l'image des coefficients parents
            Y_parent_real  = expand(Y_parent_real);
            Y_parent_imag   = expand(Y_parent_imag);

            % L'estimation de la varience de l'image utile et de l'image des parents
            Wsig_real = conv2(windowfilt,windowfilt,(Y_coef_real).^2,'same');
%             Wpar_real= conv2(windowfilt,windowfilt,(Y_parent_real).^2,'same');
            Ssig_real = sqrt(max(Wsig_real-Nsig.^2,eps));
%             Spar_real = sqrt(max(Wpar_real-Nsig.^2,eps));
            Wsig_imag = conv2(windowfilt,windowfilt,(Y_coef_imag).^2,'same');
%             Wpar_imag= conv2(windowfilt,windowfilt,(Y_parent_imag).^2,'same');
            Ssig_imag = sqrt(max(Wsig_imag-Nsig.^2,eps));
%             Spar_imag = sqrt(max(Wpar_imag-Nsig.^2,eps));
%             Ssignou_real=(Ssig_real+Spar_real./2)./2;
%             Ssignou_imag=(Ssig_imag+Spar_imag./2)./2;
%             Ssignou=(Ssignou_real+Ssignou_imag)./2;
            Ssig=(Ssig_real+Ssig_imag)./2;

            % La selection des seuils
            % Modele 1
            T11 = Nsig^2 ./ Ssig;
            T3 = sqrt(3)*T11;
            % Modele 2
            %Ro=Ssignou.*Ssignou;
            Ro=Ssig .* Ssig;
            c_dev=Ro./(Ro+Nsig.^2);

            % Les filtres bishrink
            Y_coef = Y_coef_real+I*Y_coef_imag;
            Y_parent = Y_parent_real + I*Y_parent_imag;
            if scale < 4
                Y_coef=bishrink(Y_coef,Y_parent,T3);
                W{scale}{1}{dir}{dir1} = real(Y_coef);
                W{scale}{2}{dir}{dir1} = imag(Y_coef);
            end
            if scale==4
                Y_coef = bishrink_G_dev(Y_coef,c_dev);
                W{scale}{1}{dir}{dir1} = real(Y_coef);
                W{scale}{2}{dir}{dir1} = imag(Y_coef);
            end
            if scale==5
                Y_coef = HardThresh(real(Y_coef),3.*Nsig) + I .* HardThresh(imag(Y_coef),3.*Nsig);
                W{scale}{1}{dir}{dir1} = real(Y_coef);
                W{scale}{2}{dir}{dir1} = imag(Y_coef);
            end
        end
    end
end
% Le calcul de la transformee en ondelettes inverse
W = unnormcoef(W,J,nor);
y = icplxdual2D(W, J, Fsf, sf);
% L'extraction du resultat
ind = 2^(J-1)+1:2^(J-1)+L;
y = y(ind,ind);