% Calculeaza varianta locala a imaginii utile tinand seama atat de coef reali
% cat si de cei imaginari
% Foloseste pentru bishrink dualfilt1 si AntonB
% Local Adaptive Image Denoising Algorithm
%
% Syntax
%   y = denoising_dtdwt_est_glo_var(x)
%
% Input Arguments
%   x : A noisy image
%   Nsig : [] ou Ecart-type
%
% Output Arguments
%   y : The corresponding denoised image
%   Nsig : Ecart-type
%
% Remarks : Si Nsig=[] sa valeur est estim�e sinon c'est la valeur
% transmise qui est proise en compte
%
% Examples
%   [y, Nsig] = denoising_dtdwt_est_glo_var(x, Nsig);
%
% See also Authors
% References : 
% Authors : AI
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [y, Nsig] = denoising_dtdwt_est_glo_var(x, Nsig)

% Set the windowsize and the corresponding filter
windowsize  = 7;
windowfilt = ones(1,windowsize)/windowsize;

% Number of Stages
J = 6;
I=sqrt(-1);

% symmetric extension
L = length(x); % length of the original image.
% N = L+2^J;     % length after extension.
x = symextend(x,2^(J-1));

nomFic = getNomFicDatabase('nor_dualtree.mat');
nor = loadmat(nomFic, 'nomVar', 'nor');    % run normaliz_coefcalc_dual_tree to generate this mat file.

% Forward dual-tree DWT
% Either FSfarras or AntonB function can be used to compute the stage 1 filters  
%[Faf, Fsf] = FSfarras;
[Faf, Fsf] = AntonB;
[af, sf] = dualfilt1;
W = cplxdual2D(x, J, Faf, af);
W = normcoef(W,J,nor);


% Noise variance estimation using robust median estimator..
if isempty(Nsig)
    tmp = W{1}{1}{1}{1};
    Nsig = median(abs(tmp(:)))/0.6745;
end

for scale = 1:J-1
    for dir = 1:2
        for dir1 = 1:3
            
            % Noisy complex coefficients
            %Real part
            Y_coef_real = W{scale}{1}{dir}{dir1};
            % imaginary part
            Y_coef_imag = W{scale}{2}{dir}{dir1};
            % The corresponding noisy parent coefficients
            %Real part
            Y_parent_real = W{scale+1}{1}{dir}{dir1};
            % imaginary part
            Y_parent_imag = W{scale+1}{2}{dir}{dir1};
            % Extend noisy parent matrix to make the matrix size the same as the coefficient matrix.
            Y_parent_real  = expand(Y_parent_real);
            Y_parent_imag   = expand(Y_parent_imag);
            
            % Signal mean estimation
            % Real coeficients
            Msig_real= conv2(windowfilt,windowfilt,Y_coef_real,'same');
%             Mparent_real= conv2(windowfilt,windowfilt,Y_parent_real,'same');
            % Imaginary coefficients
            Msig_imag= conv2(windowfilt,windowfilt,Y_coef_imag,'same');
%             Mparent_imag= conv2(windowfilt,windowfilt,Y_parent_imag,'same');
            % Signal variance estimation
            % Real coefficients
            Wsig_real = conv2(windowfilt,windowfilt,(Y_coef_real).^2,'same');
            Ssig_real = sqrt(max(Wsig_real-Nsig.^2-Msig_real.^2,eps));
%             Wparent_real = conv2(windowfilt,windowfilt,(Y_parent_real).^2,'same');
%             Sparent_real = sqrt(max(Wparent_real-Nsig.^2-Mparent_real.^2,eps));
            % Imaginary coefficients
            Wsig_imag = conv2(windowfilt,windowfilt,(Y_coef_imag).^2,'same');
            Ssig_imag = sqrt(max(Wsig_imag-Nsig.^2-Msig_imag.^2,eps));
%             Wparent_imag = conv2(windowfilt,windowfilt,(Y_parent_imag).^2,'same');
%             Sparent_imag = sqrt(max(Wparent_imag-Nsig.^2-Mparent_imag.^2,eps));
            % Estimarea variantei din fereastra dupa ce a fost eliminat pixelul din centrul ferestrei
            % Ssig_fpc=sqrt(Ssig.^2-(Y_coef_real.^2)./49);
            Ssig=(Ssig_real+Ssig_imag)./2;
%             Sparent=(Sparent_real+Sparent_imag)./2;
%             Ssignou=(Ssig+Sparent./2)./2;
            %contor=0;
            %if abs(Y_coef_real) > Nsig % & (Ssig_fpc./Ssig) < 1
            %    Y_coef_real=Msig;
            %    Y_coef_imag=Msig;
            %    contor=contor+1;
            % end
            % Threshold value estimation
            T = sqrt(3)*Nsig^2./Ssig;
            
            % Bivariate Shrinkage
            Y_coef = Y_coef_real+I*Y_coef_imag;
            Y_parent = Y_parent_real + I*Y_parent_imag;
            Y_coef = bishrink(Y_coef,Y_parent,T);
            W{scale}{1}{dir}{dir1} = real(Y_coef);
            W{scale}{2}{dir}{dir1} = imag(Y_coef);
            
        end
    end
end

% Inverse Transform
W = unnormcoef(W,J,nor);
y = icplxdual2D(W, J, Fsf, sf);

% Extract the image
ind = 2^(J-1)+1:2^(J-1)+L;
y = y(ind,ind);