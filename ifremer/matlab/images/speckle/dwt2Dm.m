% Transform�e en ondelette discr�te
%
% Syntax
%   w = dwt2Dm(x, stages, f)
%
% Input Arguments
%   x - matrice de taille N
%       1) N est une puissance de 2
%       2) N >= 2^(J-1)*length(f)
%   stages : Nombre d'iterations
%   f      : Filtre
%
% Output Arguments
%   w - cell array des coefficients de la TOD
%
% Examples
%   f = makeonfilter('Daubechies',16);
%   x = rand(128,128);
%   J = 6;
%   w = dwt2Dm(x,J,f);
%   y = idwt2Dm(w,J,f);
%   err = x - y; 
%   max(max(abs(err)))
%
% See also Authors
% References : 
% Authors : AI
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%--------------------------------------------------------------------------
%-----

function w = dwt2Dm(x, J, f)

% L'ientification de la taille de l'image a traiter
ma = size(x, 1);
% Le calcul de la TOD
cw = FWT2_PO(x,J,f);
% L'identification des sous-images d'approximation de details horisontaux,
% verticaux et diagonaux
for k = 1:J
    w{k}{1} = cw(1:ma / (2.^k),ma/(2.^k)+1:ma/(2.^(k-1))); %#ok<AGROW>
    w{k}{2} = cw(ma/(2.^k)+1:ma/(2.^(k-1)),1:ma/(2.^k)); %#ok<AGROW>
    w{k}{3} = cw(ma/(2.^k)+1:ma/(2.^(k-1)),ma/(2.^k)+1:ma/(2.^(k-1))); %#ok<AGROW>
end
w{J+1} = cw(1:ma/(2.^J),1:ma/(2.^(J)));
