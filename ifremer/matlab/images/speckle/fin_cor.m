% Final correction
%
% Syntax
%   uzc = fin_cor(res_par_3, synt_imag, dim)
%
% Input Arguments
%   res_par_3 : image filtered with bishrink_A with global estimation of the variance
%   synt_imag : synthetised image
%   dim      :  images dimension
%
% Output Arguments
%   uzc : uniforme zone corrected image
%
% Examples
%   uzc = fin_cor(res_par_3, synt_imag, dim)
%
% See also Authors
% References : references de l'article d'ou est tire cette fonction
% Authors : AI
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%--------------------------------------------------------------------------
%-----

function uzc = fin_cor(res_par_3, synt_imag, dim)

windowsize1 = 3;
windowfilt1 = ones(1,windowsize1)/windowsize1;
medy1 = conv2(windowfilt1,windowfilt1,synt_imag,'same');
windowsize2 = 5;
windowfilt2 = ones(1,windowsize2)/windowsize2;
medy2 = conv2(windowfilt2,windowfilt2,synt_imag,'same');
windowsize3 = 7;
windowfilt3 = ones(1,windowsize3)/windowsize3;
medy3 = conv2(windowfilt3,windowfilt3,synt_imag,'same');
sigma = conv2(windowfilt3,windowfilt3,res_par_3.^2,'same');
med   = conv2(windowfilt3,windowfilt3,res_par_3,'same');
sd = sqrt(max(sigma-med.^2,eps));
sd_max=max(max(sd));
uzc = zeros(dim, dim);
for i=1:dim
    for j=1:dim
        sdij = sd(i,j);
        if (sdij <= 0.075*sd_max) && (sdij > 0.05*sd_max)
            uzc(i,j)=medy1(i,j);
        else
            uzc(i,j)=synt_imag(i,j);
        end
        if (sdij <= 0.05*sd_max) && (sdij > 0.025*sd_max)
            uzc(i,j)=medy2(i,j);
        end
        if sdij <= 0.025*sd_max
            uzc(i,j)=medy3(i,j);
        end
    end
end
