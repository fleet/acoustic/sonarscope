% TODO
%
% Utilisation:
%   y = idwt2Dm(w, J, f)
% ENTREE:
%   w - coefficent de la TOD
%   J - caracterise le nombre d'iterations
%   f - filtre
% SORTIE:
%   y - array de sortie
% Voir dwt2Dm
%

function y = idwt2Dm(w, J, f)

o = w{J+1};
for k = J:-1:1
   o = [o, w{k}{1}; w{k}{2},w{k}{3}]; %#ok<AGROW>
end
y = IWT2_PO(o,J,f);