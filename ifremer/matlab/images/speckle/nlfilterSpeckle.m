% Fonction de filtrage du speckle
%
% b = nlfilterSpeckle(a, N, nhood, fun, ...)
%
% Input Arguments
%   a     : Image a filtrer
%   N     : Matrice du nombre de points moyennes par pixel (mettre une valeur estimee si elle n'existe pas)
%   nhood : Taille de la fenetre de filtrage
%   fun   : Nom du noyau du filtre a utiliser
%
% Name-Value Pair Arguments
%   params : parametre suplementaire pour le filtre NoyauFiltreTLineaire (t)
%
% Output Arguments
%   b : Image filtree
%
% Examples
%   I1 = Swansea;
%   I2 = nlfilterSpeckle(I1, 1, [3 3], 'NoyauFiltreMoyQuant25_75', []);
%   figure; imagesc(I1),; colorbar; colormap(gray(256)); axis xy;
%   figure; imagesc(I2),; colorbar; colormap(gray(256)); axis xy;
%
%   nomFic1 = getNomFicDatabase('EM1000_rectiligne_0.15.imo');
%   nomFic1 = getNomFicDatabase('EM1000_rectiligne_1.2.imo');
%   nomFic2 = [my_tempdir 'testSpeckle.imo'];
%   copyfile(nomFic1, nomFic2)
%   a = cl_car_ima(nomFic2);
%   R1 = get(a, 'REFLECTIVITY');
%   N1 = get(a, 'AVRG_PIX_NB');
%
%   figure;
%   subplot(2,1,1); imagesc(R1, [-50 -10]); colorbar; colormap(gray(256));
%   R1 = reflec_dB2Enr(R1);
%   R2 = nlfilterSpeckle(R1, N1, [7 7], 'NoyauFiltreLee' );
%   R2 = nlfilterSpeckle(R1, N1, [7 7], 'NoyauFiltreTLineaire' ,3 );
%
%   R2 = reflec_Enr2dB(R2);
%   subplot(2,1,2); imagesc(R2, [-50 -10]); colorbar; colormap(gray(256));
%
% See also Authors
% Authors : JMA SB
%-------------------------------------------------------------------------------

function b = nlfilterSpeckle(a, N, nhood, fun, varargin)

% ------------------------------------------------------------------------
% Cr�ation de 2 matrices de taille celle de a et N + largeur de la fenetre
% de filtrage (pour mettre des nan sur les bords)

[ma,na] = size(a);
aa = NaN( size(a)+nhood-1);
aa(floor((nhood(1)-1)/2)+(1:ma),floor((nhood(2)-1)/2)+(1:na),:) = a;

rows = 0:nhood(1)-1;
cols = 0:nhood(2)-1;
b = NaN(size(a));

hw = create_waitbar('Applying neighborhood operation', 'N', ma);
switch fun
    case 'NoyauFiltreLee'
        bb = aa;
        bb(floor((nhood(1)-1)/2)+(1:ma),floor((nhood(2)-1)/2)+(1:na),:) = N;
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                L = bb(I,J);
                p = L((nhood(1)+1)/2, (nhood(2)+1)/2);  % extraction du nb de moyennage du pixel a filtrer
                if p <= 0     % si le nb de moyennage est negatif ou nul(extrapolation) on force ce nombre a 1
                    p = 1;
                end
                b(kLig,kCol) = abs(NoyauFiltreLee(x, p));
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreKuan'
        bb = aa;
        bb(floor((nhood(1)-1)/2)+(1:ma),floor((nhood(2)-1)/2)+(1:na),:) = N;
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                L = bb(I,J);
                p = L((nhood(1)+1)/2, (nhood(2)+1)/2);
                if p <= 0
                    p = 1;
                end
                b(kLig,kCol) = abs(NoyauFiltreKuan(x, p));
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreStd'
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                b(kLig,kCol) = abs(NoyauFiltreStd(x));
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreEntropy'
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                b(kLig,kCol) = entropy(x);
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreMoyQuant25_75'
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                b(kLig,kCol) = NoyauFiltreMoyQuant25_75(x);
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreFrost'
        bb = aa;
        bb(floor((nhood(1)-1)/2)+(1:ma),floor((nhood(2)-1)/2)+(1:na),:) = N;
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                L = bb(I,J);
                p = L((nhood(1)+1)/2, (nhood(2)+1)/2);
                if p <= 0
                    p = 1;
                end
                b(kLig,kCol) = abs(NoyauFiltreFrost(x,p));
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreMAP'
        bb = aa;
        bb(floor((nhood(1)-1)/2)+(1:ma),floor((nhood(2)-1)/2)+(1:na),:) = N;
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                L = bb(I,J);
                p = L((nhood(1)+1)/2, (nhood(2)+1)/2);
                if p <= 0
                    p = 1;
                end
                b(kLig,kCol) = abs(NoyauFiltreMAP(x,p));
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreEAP'
        bb = aa;
        bb(floor((nhood(1)-1)/2)+(1:ma),floor((nhood(2)-1)/2)+(1:na),:) = N;
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                L = bb(I,J);
                p = L((nhood(1)+1)/2, (nhood(2)+1)/2);
                if p <= 0
                    p = 1;
                end
                b(kLig,kCol) = abs(NoyauFiltreEAP(x, p));
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'NoyauFiltreTLineaire'
        bb = aa;
        bb(floor((nhood(1)-1)/2)+(1:ma),floor((nhood(2)-1)/2)+(1:na),:) = N;
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                L = bb(I,J);
                p = L((nhood(1)+1)/2, (nhood(2)+1)/2);
                if p <= 0
                    p = 1;
                end
                b(kLig,kCol) = abs(NoyauFiltreTLineaire(x, p, varargin{1}));
            end
            my_waitbar(kLig, ma, hw)
        end

    case 'wallisaugustin'
        for kLig=1:ma
            for kCol=1:na
                I = kLig + rows;
                J = kCol + cols;
                x = aa(I,J);
                b(kLig,kCol) = wallisaugustin(x, varargin{2:end});
            end
            my_waitbar(kLig, ma, hw)
        end

    otherwise
        my_warndlg('images/speckle/nlfilterSpeckle : NoyauFiltrexxx non programme', 1);
end
my_close(hw, 'MsgEnd');
