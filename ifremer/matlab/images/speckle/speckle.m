% Generation d'un bruit de Rayleigh correle
%
% Syntax
%   x = speckle(m, n)
%   x = speckle([m n])
% 
% Input Arguments 
%   nomFic : Nom du fichier .mat
%   
% 
% Output Arguments 
%   x : Variable lue
%
% Examples
%     nomFic = getNomFicDatabase('MIR.bmp')
%     R1 = double(imread(nomFic));
%     R1 = double(Lena);
%     my_hist(R1(:), 0:500)
%     figure; imagesc(R1); colormap(gray(256)); colorbar;
%     x = speckle(size(R1));
%     figure; my_hist(x(:), 0:0.01:5)
%
%     figure; imagesc(x); colormap(gray(256)); colorbar;
%     R2 = R1 .* x;
%     figure; imagesc(R2); colormap(gray(256)); colorbar;
%     figure; my_hist(R2(:), 0:1000)
%
% See also Authors
% Authors : SB + JMA
%-------------------------------------------------------------------------------

function x = speckle(varargin)

%% Gestion des parametres d'entree

switch nargin
case 0
    return
case 1
    sz = varargin{1};
    n = sz(1);
    if length(sz) == 2
        m = sz(2);
    else
        m = n;
    end
case 2
    n = varargin{1};
    m = varargin{2};
end
n2 = m * n;

%% Génération de deux gaussiennes

%x1 = normrnd(0, 1, n2, 1);
x1 = randn(n2, 1);
x1 = reshape(x1, m, n);

%x2 = normrnd(0, 1, n2, 1);
x2 = randn(n2, 1);
x2 = reshape(x2, m, n);

%% On calcule la loi de Rayleigh

y = abs(x1 + 1i*x2);
m = mean2(y);
x = y / m;
