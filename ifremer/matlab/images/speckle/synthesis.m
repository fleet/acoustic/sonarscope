% The synthesis of a new result from the intermediare ones.
% Usage :
% [synt_res, trunc_sig_1, trunc_sig_2, trunc_sig_3, trunc_sig_4, trunc_sig_5, trunc_sig_6]=synthesis(res_par_1,res_par_2,res_par_3,res_par_4,res_par_5,res_par_6, orig_imag,dim)       
% INPUT :
%        res_par_1 - image filtered with bishrink_A
%        res_par_2 - image filtered with bishrink_A with gl est va + Wiener+ht
%        res_par_3 - image filtered with bishrink_A with global estimation of the variance
%        res_par_4 - image filtered with bishrink_F with gl est var+ Wiener+ht
%        res_par_5 - image filtered with bishrink_F 
%        res_par_6 - image filtered with bishrink_F with est gl va
%        orig_imag - original image
%        dim - images dimension
% OUTPUT :
%        synt_res - the synthesized image
%        trunc_sig_1 - truncated result for the first region of sigma
%        trunc_sig_2 - truncated result for the second region of sigma
%        trunc_sig_3 - truncated result for the third region of sigma
%        trunc_sig_4 - truncated result for the forth region of sigma
%        trunc_sig_5 - truncated result for the fifth region of sigma
%        trunc_sig_6 - truncated result for the sixth region of sigma

function [synt_res, trunc_sig_1, trunc_sig_2, trunc_sig_3, trunc_sig_4, trunc_sig_5, trunc_sig_6]=synthesis(res_par_1,res_par_2,res_par_3,res_par_4,res_par_5,res_par_6, orig_imag,dim)

windowsize = 7;
windowfilt = ones(1,windowsize)/windowsize;

% The proptotype is the bi_est_gl_va
sigma = conv2(windowfilt, windowfilt, res_par_3.^2, 'same');
med = conv2(windowfilt, windowfilt, res_par_3, 'same');
sd = sqrt(max(sigma - med.^2, eps));
sd_max = max(max(sd));
synt_res = zeros(dim, dim);
trunc_sig_1 = zeros(dim, dim);
s1t = zeros(dim, dim);
y1bt = zeros(dim, dim);
yb1t = zeros(dim, dim);

trunc_sig_2 = zeros(dim, dim);
s2t = zeros(dim, dim);
yb2t = zeros(dim, dim);

trunc_sig_3 = zeros(dim, dim);
s3t = zeros(dim, dim);
yb3t = zeros(dim, dim);

trunc_sig_4 = zeros(dim, dim);
s4t = zeros(dim, dim);
yb4t = zeros(dim, dim);

trunc_sig_5 = zeros(dim, dim);
s5t = zeros(dim, dim);
yb5t = zeros(dim, dim);

trunc_sig_6 = zeros(dim, dim);
s6t = zeros(dim, dim);
yb6t = zeros(dim, dim);

sd_max0_25  = 0.250*sd_max;
sd_max0_1   = 0.100*sd_max;
sd_max0_075 = 0.075*sd_max;
sd_max0_05  = 0.050*sd_max;
sd_max0_025 = 0.025*sd_max;

for i=1:dim
    for j=1:dim
        sdij = sd(i,j);
        if sdij > sd_max0_25 % adapté aux contours
            synt_res(i,j)=res_par_3(i,j);
            trunc_sig_1(i,j)=synt_res(i,j);
            s1t(i,j)=orig_imag(i,j);
            yb1t(i,j)=res_par_1(i,j);
        else
            trunc_sig_1(i,j)=0;
            s1t(i,j)=0;
            y1bt(i,j)=0;
        end
        
        if (sdij <= sd_max0_25) && (sdij > sd_max0_1) % adapté aux textures bien marquées
            synt_res(i,j)=(res_par_2(i,j)+res_par_3(i,j))./2;
            trunc_sig_2(i,j)=synt_res(i,j);
            s2t(i,j)=orig_imag(i,j);
            yb2t(i,j)=res_par_1(i,j);
        else
            trunc_sig_2(i,j)=0;
            s2t(i,j)=0;
            yb2t(i,j)=0;
        end
        
        if (sdij <= sd_max0_1) && (sdij > sd_max0_075) % adapté aux textures
            synt_res(i,j)=(res_par_1(i,j)+res_par_2(i,j)+res_par_3(i,j))./3;
            trunc_sig_3(i,j)=synt_res(i,j);
            s3t(i,j)=orig_imag(i,j);
            yb3t(i,j)=res_par_1(i,j);
        else
            trunc_sig_3(i,j)=0;
            s3t(i,j)=0;
            yb3t(i,j)=0;
        end
        
        if (sdij <= sd_max0_075) && (sdij > sd_max0_05) % adapté aux textures faiblement marquées
            synt_res(i,j)=(res_par_1(i,j)+res_par_2(i,j)+res_par_3(i,j)+res_par_5(i,j))./4;
            trunc_sig_4(i,j)=synt_res(i,j);
            s4t(i,j)=orig_imag(i,j);
            yb4t(i,j)=res_par_1(i,j);
        else
            trunc_sig_4(i,j)=0;
            s4t(i,j)=0;
            yb4t(i,j)=0;
        end
        
        if (sdij <= sd_max0_05) && (sdij > sd_max0_025) % adapté aux textures très faiblement marquées
            synt_res(i,j)=(res_par_1(i,j)+res_par_2(i,j)+res_par_3(i,j)+res_par_4(i,j)+res_par_5(i,j))./5;
            trunc_sig_5(i,j)=synt_res(i,j);
            s5t(i,j)=orig_imag(i,j);
            yb5t(i,j)=res_par_1(i,j);
        else
            trunc_sig_5(i,j)=0;
            s5t(i,j)=0;
            yb5t(i,j)=0;
        end
        
        if sdij <= (sd_max0_025) % adapté aux régions homogènes
            synt_res(i,j)=(res_par_1(i,j)+res_par_2(i,j)+res_par_3(i,j)+res_par_4(i,j)+res_par_5(i,j)+res_par_6(i,j))./6;
            trunc_sig_6(i,j)=synt_res(i,j);
            s6t(i,j)=orig_imag(i,j);
            yb6t(i,j)=res_par_1(i,j);
        else
            trunc_sig_6(i,j)=0;
            s6t(i,j)=0;
            yb6t(i,j)=0;
        end
    end
end
