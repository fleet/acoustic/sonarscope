% Representation 3D (surf ameliore)
%
% Syntax
%   hAxe = surf_ifr(x, y, z, ...)
%
% Input Arguments
%   x : Abscisses OU Limites (XLim) OU []
%   y : Ordonnees OU Limites (YLim) OU []
%   z : Donn�e � repr�senter en 3D
%
% Name-Value Pair Arguments
%   GeometryType : {1=Unknown} , ...
%   hAxe            : Handle de l'axe ([] par defaut)
%   Azimut          : Azimut (deg) (180 par defaut =vue du sud)
%   Elevation       : Elevation (deg)  (90 par defaut = vue du dessus)
%   Exageration     : Exageration verticale (1 par defaut)
%   Light           : Type d'�clairage : {1=none}, 2=flat, 3=gouraud, 4=phong
%   Texture         : Image de texture a plaquer sur z ([] par defaut)
%   map             : Table de couleur (jet(256) pardefaut)
%   TypeMaterial    : Type de mat�riau : {1=shiny}, 2=dull, 3=metal
%   Box             : {1=Affichage des axes}, 0=pas d'affichage
%   ZLim            : Limites de l'axe des z ([min max] de z par defaut)
%   CLim            : Rehaussement de contraste de la texture ([min max] de Texture par defaut)
%
% Output Arguments
%   hAxe  : Handle de l'axe
%
% Examples
%   [X,Y,Z] = peaks(100);
%   surf_ifr([], [], Z, 'Azimut', 200, 'Elevation', 45, 'Exageration', 40, 'Light', 4);
%   surf_ifr(X(1,:), Y(:,1), Z, 'Azimut', 200, 'Elevation', 45, 'Exageration', 4, 'Light', 4);
%   surf_ifr(X, Y, Z, 'Azimut', 200, 'Elevation', 45, 'Exageration', 4, 'Light', 4);
%
% See also my_surf cl_image/surf Authors
% Authors : JMA
%--------------------------------------------------------------------------

function hAxe = surf_ifr(x, y, z, varargin)

[varargin, GeometryType] = getPropertyValue(varargin, 'GeometryType', cl_image.indGeometryType('Unknown'));
[varargin, hAxe]         = getPropertyValue(varargin, 'hAxe',         []);
[varargin, Azimut]       = getPropertyValue(varargin, 'Azimut',       180);
[varargin, Elevation]    = getPropertyValue(varargin, 'Elevation',    90);
[varargin, Exageration]  = getPropertyValue(varargin, 'Exageration',  1);
[varargin, Light]        = getPropertyValue(varargin, 'Light',        1);
[varargin, Texture]      = getPropertyValue(varargin, 'Texture',      []);
[varargin, map]          = getPropertyValue(varargin, 'map',          jet(256));
[varargin, TypeMaterial] = getPropertyValue(varargin, 'TypeMaterial', 1);
[varargin, Box]          = getPropertyValue(varargin, 'Box',          0);
[varargin, ZLim]         = getPropertyValue(varargin, 'ZLim',         []);
[varargin, CLim]         = getPropertyValue(varargin, 'CLim',         []); %#ok<ASGLU>

%% Cas o� on a passe XLim

if length(x) == 2
    x = linspace(x(1), x(2), size(z,2));
end

%% Cas o� on a passe YLim

if length(y) == 2
    y = linspace(y(1), y(2), size(z,1));
end

%% Cas o� on a pas defini x (passage de [])

if isempty(x)
    x = 1:size(z,2);
    GeometryType = cl_image.indGeometryType('Unknown');
end

%% Cas o� on a pas defini y (passage de [])

if isempty(y)
    y = 1:size(z,1);
    GeometryType = cl_image.indGeometryType('Unknown');
end

XLim = compute_XYLim(x);
YLim = compute_XYLim(y);

Azimut         = 180 - Azimut;
LightAzimut    = Azimut;
LightElevation = Elevation;

%% Affichage de la surface

if isempty(hAxe)
    figure
    hAxe = axes;
else
    if ~ishandle(hAxe)
        figure
        hAxe = axes;
    end
end

if ~isempty(ZLim)
    z(z < ZLim(1)) = NaN;
    z(z > ZLim(2)) = NaN;
end

if ~isempty(CLim)
    Texture(Texture < CLim(1)) = CLim(1);
    Texture(Texture > CLim(2)) = CLim(2);
end

if isempty(Texture)
    %     surf(hAxe, x, y, double(z));
    surf(hAxe, x, y, double(z), 'faceColor', 'Texture', 'EdgeColor', 'none');
else
    if size(Texture,3) == 1
        surf(hAxe, x, y, double(z), Texture, 'faceColor', 'Texture', 'EdgeColor', 'none');
    else
        surf(hAxe, x, y, double(z), double(Texture/256), 'faceColor', 'Texture', 'EdgeColor', 'none');
    end
end

if ~isempty(map)
    colormap(map)
end
set(hAxe, 'View', [Azimut Elevation]);

% ---------------------------------------------------------
% Si z contient plus de 400 elements, on supprime la grille

if numel(z) > 20^2
    shading(hAxe, 'flat')
end

% ----------------------------------
% Gestion de l'exageration verticale

switch GeometryType
    case 1  % Unknown
        ZLim = get(hAxe, 'ZLim');
        DataAspectRatio(1) = 1;
        DataAspectRatio(2) = 1;
        DataAspectRatio(3) = (diff(ZLim) / length(x) * 100) / Exageration;
    case cl_image.indGeometryType('GeoYX')  % Coordonnees metriques
        DataAspectRatio(1) = 1;
        DataAspectRatio(2) = 1;
        DataAspectRatio(3) = 1 / Exageration;
    case cl_image.indGeometryType('LatLong')  % Coordonnees geographiques en deg et altitude en m
        ZLim = get(hAxe, 'ZLim');
        LatMilieu = mean(YLim);
        Arc = 40000000000 * pi / 180;
        DataAspectRatio(1) = 1 / cos(LatMilieu * pi / 180);
        DataAspectRatio(2) = 1;
        DataAspectRatio(3) = (Arc / diff(ZLim)) / Exageration;
end
set(hAxe, 'DataAspectRatio', DataAspectRatio)
set(hAxe, 'XLim', XLim)
set(hAxe, 'YLim', sort(YLim))

% ----------------------------------------
% Masquage eventuel de la boite englobante

if Box
    axis(hAxe, 'on')
    set(hAxe, 'Box', 'on')
else
    axis(hAxe, 'off')
    set(hAxe, 'Box', 'off')
end

% -----------------------------
% Traitement du type de lumi�re

switch Light
    case 1  % None
    case 2  % flat
        hl = light;
        lightangle(hl, LightAzimut, LightElevation)
        lighting flat
    case 3  % gouraud
        hl = light;
        lightangle(hl, LightAzimut, LightElevation)
        lighting gouraud
    case 4  % phong
        hl = light;
        lightangle(hl, LightAzimut, LightElevation)
        lighting phong
end

% ------------------------------
% Traitement du type de mat�riau

switch TypeMaterial
    case 1
        material shiny
    case 2
        material dull
    case 3
        material metal
end

% ----------------------------------
% On force l'affichage immediatement

drawnow
