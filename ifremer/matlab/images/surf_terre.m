% Representation sous forme de sphere de la Terre
%
% Syntax
%   hAxe = my_surf(x, y, z, ...)
%
% Input Arguments
%   lon      : longitudes (deg)
%   lat      : Latitudes (deg)
%   Altitude : Topographie / niveau marin (m)
%
% Name-Value Pair Arguments
%   Texture      : Image de texture a plaquer sur z (z par defaut)
%   map          : Table de couleur (jet(256) par defaut)
%   Azimut       : Azimut (deg) (180 par defaut =vue du sud)
%   Elevation    : Elevation (deg)  (90 par defaut = vue du dessus)
%   Exageration  : Exageration verticale (100 par defaut)
%   Light        : Type d'�clairage : {1=none}, 2=flat, 3=gouraud, 4=phong
%   TypeMaterial : Type de mat�riau : {1=shiny}, 2=dull, 3=metal
%   hAxe         : Handle de l'axe ([] par defaut)
%
% Output Arguments
%   hAxe  : Handle de l'axe
%
% Examples
%   [Altitude, lon, lat] = Etopo;
%   surf_terre(lon, lat, Altitude, 'Light', 4);
%
%   hAxe = [];
%   for i=0:359
%       hAxe = surf_terre(lon, lat, Altitude, 'Light', 4, 'Azimut', i, 'hAxe', hAxe);
%   end
%
% See also my_surf cl_image/surf Authors
% Authors : JMA
%--------------------------------------------------------------------------

function hAxe = surf_terre(lon, lat, Altitude, varargin)

[varargin, hAxe]         = getPropertyValue(varargin, 'hAxe',         []);
[varargin, Azimut]       = getPropertyValue(varargin, 'Azimut',       0);
[varargin, Elevation]    = getPropertyValue(varargin, 'Elevation',    0);
[varargin, Exageration]  = getPropertyValue(varargin, 'Exageration',  100);
[varargin, Light]        = getPropertyValue(varargin, 'Light',        1);
[varargin, Texture]      = getPropertyValue(varargin, 'Texture',      []);
[varargin, map]          = getPropertyValue(varargin, 'map',          jet(256));
[varargin, TypeMaterial] = getPropertyValue(varargin, 'TypeMaterial', 1); %#ok<ASGLU>

R = 6400*1e3 + Exageration * Altitude;
nbLon = length(lon);
nbLat = length(lat);
Lon = repmat(lon, nbLat, 1);
Lat = repmat(lat', 1, nbLon);
Z = R .* sin(Lat*(pi/180));
X = R .* cos(Lat*(pi/180)) .* cos(Lon*(pi/180));
Y = R .* cos(Lat*(pi/180)) .* sin(Lon*(pi/180));

if isempty(hAxe)
    figure
    hAxe = axes;
else
    if ~ishandle(hAxe)
        figure
        hAxe = axes;
    end
end

if isempty(Texture)
    surf(X, Y, Z, double(Altitude));
else
    surf(X, Y, Z, double(Texture));
end
axis equal; axis off; shading flat
colormap(map)

view(90+Azimut, Elevation)


%% Traitement du type de lumi�re

switch Light
    case 1  % None
    case 2  % flat
        hl = light;
        lightangle(hl, 90+Azimut, Elevation)
        lighting flat
    case 3  % gouraud
        hl = light;
        lightangle(hl, 90+Azimut, Elevation)
        lighting gouraud
    case 4  % phong
        hl = light;
        lightangle(hl, 90+Azimut, Elevation)
        lighting phong
end

%% Traitement du type de mat�riau

switch TypeMaterial
    case 1
        material shiny
    case 2
        material dull
    case 3
        material metal
end

drawnow
