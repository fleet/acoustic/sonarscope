function [flag, Carto, nomFicCarto] = test_ExistenceCarto_Folder(nomDir)

flag = 0;
Carto = [];
nomFicCarto = [];

C0 = cl_carto([]);

if ~verLessThan('matlab', '8.1') % R2013a
    liste = listeFicOnDir(nomDir, '.def', 'FiltreExclude', '_Folder');
else
    liste = listeFicOnDir(nomDir, '.def', 'FiltreExclude', '_Directory');
end
if isempty(liste) || (length(liste) > 1)
    return
end

Carto = import_xml(C0, liste{1});
if isempty(Carto)
    
    % Au cas o� le fichier soit � l'ancienne m�thode
    
    Carto = import(C0, liste{1});
    flag = export_xml(Carto, liste{1});
    if flag
        %                 delete(nomFic)
    end
end
nomFicCarto = liste{1};
flag = 1;

