function [Tx, Ty, TxEntiers, TyEntiers] = calcul_deplacement_directions(this)

nbDeplacements = length(this.Deplacements);
for iDirection=1:length(this.Directions)
    switch this.Directions(iDirection)
        case 1 % Deplacement horizontal
            Tx{iDirection} = this.Deplacements; %#ok
            Ty{iDirection} = zeros(1,nbDeplacements); %#ok
            TxEntiers{iDirection} = Tx{iDirection}; %#ok
            TyEntiers{iDirection} = Ty{iDirection}; %#ok
        case 3
            Tx{iDirection} = zeros(1,nbDeplacements); %#ok<AGROW>
            Ty{iDirection} = this.Deplacements; %#ok<AGROW>
            TxEntiers{iDirection} = Tx{iDirection}; %#ok
            TyEntiers{iDirection} = Ty{iDirection}; %#ok
        case 2
            [x, xEntiers] = calculPiquets(this.Deplacements);
            Tx{iDirection} = -x; %#ok<AGROW>
            Ty{iDirection} = x; %#ok<AGROW>
            TxEntiers{iDirection} = -xEntiers; %#ok
            TyEntiers{iDirection} =  xEntiers; %#ok
        case 4
            [x, xEntiers] = calculPiquets(this.Deplacements);
            Tx{iDirection} = x; %#ok<AGROW>
            Ty{iDirection} = x; %#ok<AGROW>
            TxEntiers{iDirection} = xEntiers; %#ok<AGROW>
            TyEntiers{iDirection} = xEntiers; %#ok<AGROW>
    end
end
