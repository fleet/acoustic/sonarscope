% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : Une instance de cl_cooc
%
% Output Arguments
%   str : une chaine de caracteres representative d'une instance de cl_cooc
%
% Examples
%   nomFic = getNomFicDatabase('Texture01.png');
%   img = imread(nomFic);
%
%   a = cl_cooc('Image', img ,'Deplacements', 10);
%   str = char(a);
%
% See also cl_cooc cl_cooc/display Authors
% Authors : FM + JMA
% VERSION  : $Id: char.m,v 1.1 2002/10/21 11:59:51 emenut Exp emenut $
% ----------------------------------------------------------------------------

function str = char(this)

% ----------------------------------------------------------------------------
% Initialisation de la variable retournee

str = [] ;

% ----------------------------------------------------------------------------
% Taille de la matrice donnee en argument

sz = size(this) ;

% ----------------------------------------------------------------------------
% Matrice non reduite a un element : traitement pour chaque element

if prod(sz) > 1
    for i=1:sz(1)
        for j=1:sz(2)
            % appel recursif de cette methode sur l element courant, resultat
            % sauve dans une liste de chaine
            str{end+1} = sprintf('-------------\nElement (%d,%d)', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = sprintf('%s\n', str1); %#ok<AGROW>
        end
    end
    % concatenation de toutes les chaines de la liste de chaines
    str = sprintf('%s\n', str{:});

else
    nbDeplacements  = length(this.Deplacements);  % Nombre de deplacements
    nbDirections    = length(this.Directions);
    nbParams        = length(this.ListeParams);

    % -----------------------------------
    % Attributs presentes a l'utilisateur

    str{end+1} = sprintf('          Label <-> %s', this.Label);
    str{end+1} = sprintf('          Image <-> %s', num2strCode(double(this.Image)));
    if isempty(this.Image)
        str = sprintf('%s\n', str{:});
        return
    end

    str{end+1} = sprintf('   Deplacements <-> %s', num2strCode(this.Deplacements));
    str{end+1} = sprintf('    Directions  <-> %s', num2strCode(this.Directions));
    str{end+1} = sprintf('');
    str{end+1} = sprintf('  WindowAnalyse <-> %s', num2strCode(this.WindowAnalyse));
    choix = {   'decalage WindowAnalyse'
        'decalage max(Deplacements)'
        'decalage WindowTypeShift'};
    str{end+1} = sprintf('WindowTypeShift <-> %s', SelectionDansListe2str( this.WindowTypeShift, choix, 'Num'));
    str{end+1} = sprintf(' WindowValShift <-> %s', num2strCode(this.WindowValShift));
    str{end+1} = sprintf('         ligDeb <-- %s', num2strCode(this.ligDeb));
    str{end+1} = sprintf('         ligFin <-- %s', num2strCode(this.ligFin));
    str{end+1} = sprintf('         colDeb <-- %s', num2strCode(this.colDeb));
    str{end+1} = sprintf('         colFin <-- %s', num2strCode(this.colFin));
    str{end+1} = sprintf('            nbL <-- %d', this.nbL);
    str{end+1} = sprintf('            nbC <-- %d', this.nbC);
    str{end+1} = sprintf('');

    s = [];
    for i=1:length(this.NomParams)
        s{i} = func2str(this.NomParams{i}); %#ok<AGROW>
    end
    %         str{end+1} = sprintf('     NomParams  <-- %s', sprintf('''%s'' ', s{:}));
    str{end+1} = sprintf('     NomParams  <-- ');
    for i=1:length(this.NomParams)
        str{end+1} = sprintf('                %d : %s', i, s{i}); %#ok<AGROW>
    end

    str{end+1} = sprintf('   ListeParams  <-> %s', num2strCode(this.ListeParams));

    str{end+1} = sprintf('');
    str{end+1} = sprintf('         Params <-- Array size : (%d,%d,%d,%d,%d) : nbDep nbDir nbPar nbL nbC', ...
        nbDeplacements, nbDirections, nbParams, this.nbL, this.nbC);


    str{end+1} = sprintf('      FlagProny <-> %d', this.FlagProny);

    str{end+1} = sprintf('   PronyReduits <-- Array size : (%d,%d,2,%d,%d)  : nbDir nbPar 2 nbL nbC', ...
        nbDirections, nbParams, this.nbL, this.nbC);

    str{end+1} = sprintf('          Style <-> instance de cl_style :');
    s = char(this.Style);
    for i=1:size(s,1)
        str{end+1} = sprintf('                                        %s', s(i,:)); %#ok<AGROW>
    end

    % -----------------------------------------------------------------
    % Concatenation du tableau de cellules en une matrice de caracteres

    str = sprintf('%s\n', str{:});

end
