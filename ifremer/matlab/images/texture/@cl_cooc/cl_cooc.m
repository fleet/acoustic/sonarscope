% Constructeur de cl_cooc (Analyse de texture d'une image par matrices de cooccurrence)
%
% Syntax
%   this = cl_cooc(...)
% 
% Name-Value Pair Arguments
%   Label           : Description de l'image
%   Image           : Image de base
%   Deplacements    : Deplacements (1:10 par defaut)
%   Directions      : Orientations 1=0 deg, 2=45 deg,
%                     3=90 deg, 4=135 deg ([1 2 3 4] par defaut
%   NomParams       : Liste des parametres texturaux :
%                     {'contraste'; 'directivite';'entropie';'local_homog';'uniformite';
%                      'correlation';'homogeneite';'meanImage';'stdImage'};
%   ListeParams     : Indices des parametres texturaux (1=contraste, 2=directivite, etc...)
%   Params          : Parametres texturaux
%   FlagProny       : 1=Modelisation de Prony | {0=pas de modelisation de Prony}
%   ParamsReduits   : Periode et amplitude obtenus  partir de la modelisation de Prony
%   WindowAnalyse   : Taille d'analyse [0 0]=Fenetre entiere sinon largeur de la fenetre
%   WindowTypeShift : Type de decalage (si WindowAnalyse # [0 0]
%                     {1=decalage donne par WindowAnalyse} |
%                      2=decalage donne par le max du vecteur deplacement  | 
%                      3=decalage donne par l'utilisateur
%   WindowValShift  : Decalage donne par l'utilisateur si WindowTypeShift=3
%   ligDeb, ligFin, colDeb, colFin : Coordonnees des images de calcul
%                                   (depend de WindowAnalyse, WindowTypeShift et WindowValShift)
%   nbL, nbC : Nombre d'imagettes en lignes et en colonnes
%  
% Output Arguments 
%   this : instance de cl_cooc
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   I1 = imread(nomFic);
%   [ls , Label1] = fileparts(nomFic);
% 
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   I2 = imread(nomFic);
%   [pathFic, Label2] = fileparts(nomFic);
% 
%   Par = 1:10; Dir = 1; W = [32 32];
% 
%   Style = cl_style('Color', 'b');
%   a(1) = cl_cooc( 'Image', I1, 'Label', Label1, ...
%                   'Directions', Dir, 'ListeParams', Par, 'Style', Style);
%   Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
%   a(2) = cl_cooc( 'Image', I1, 'Label', Label1, ...
%                   'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
%                   'WindowAnalyse', W);
% 
%   imagesc(a);
%   plot(a);
%   plot(a, 'subPar', [1 2 5])
% 
%   histo(a, 'subPar', 1, 'subDep', 1:6)
%   plotNuages(a, 'subdep', [1 6], 'subParX', 1:8, 'subParY', 1:8, 'ellipsesSeules')
% 
%   P1 = get(a(1), 'Params')
%   P1=squeeze(P1(:,1,1))
%   figure; plot(P1); grid on;
%
% See also cli_cooc
% Authors : FM + JMA
% VERSION  : $Id: cl_cooc.m,v 1.1 2002/10/21 11:59:51 emenut Exp emenut $
% ----------------------------------------------------------------------------

function this = cl_cooc(varargin)

%% D�finition de la structure

this.Label            = '';
this.Image            = [];
this.Deplacements     = 1:20;
this.Directions       = 1:4;
this.WindowAnalyse    = [0 0];
this.WindowTypeShift  = 1;
this.WindowValShift   = [1 1];
this.ligDeb           = [];
this.ligFin           = [];
this.colDeb           = [];
this.colFin           = [];
this.nbL              = 1;
this.nbC              = 1;
this.NomParams        = {@coocContraste
    @coocDirectivite
    @coocEntropie
    @coocLocalHomog
    @coocUniformite
    @coocCorrelation
    @coocHomogeneite
    @coocDirectiviteJMA
    @meanImage
    @stdImage};

%                                 @coocDissimilarity
%                                 @coocSDC
%                                 @coocRegularity
%                                 @coocSigmaPx
%                                 @coocSigmaPy
%                                 @coocPeriodicity
%                                 @skewnessImage
%                                 @kurtosisImage
%                                 @coocSkewnessPx
%                                 @coocSkewnessPy
%                                 @coocKurtosisPx
%                                 @coocKurtosisPy];

this.ListeParams      = 1:length(this.NomParams);
this.Params           = [];
this.FlagProny        = 0;
this.PronyReduits     = [];
this.Style            = []; % L'initialisation a cl_style a cet endroit etait tres penalisant en temps de calcul

%% Cr�ation de l'instance

this = class(this, 'cl_cooc');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this.Style = cl_style;
this = set(this, varargin{:});
