% Segmentation par EQM
%
% Syntax
%   S = segmentation(this, Moyenne, EcartType, Ponder, Masque)
%
% Input Arguments
%   A : Classes d'apprentissage
%
% Examples
%
% See also cl_cooc cl_cooc/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [tabClass,this]= clusterisationEM(this,nbClasses, varargin)

nbDeplacements = length(this.Deplacements);
nbDirections   = length(this.Directions);
nbParams       = length(this.ListeParams);
nbL            = this.nbL;
nbC            = this.nbC;

[varargin, subDep] = getPropertyValue(varargin, 'subDep', 1:nbDeplacements);
[varargin, subDir] = getPropertyValue(varargin, 'subDir', 1:nbDirections);
[varargin, subPar] = getPropertyValue(varargin, 'subPar', 1:nbParams); %#ok<ASGLU>


this.Deplacements   = this.Deplacements(subDep);
this.Directions     = this.Directions(subDir);
this.ListeParams    = this.ListeParams(subPar);
this.Params         = this.Params(subDep, subDir, subPar, : , :);

% -----------------------------------
% Recuperation des donnes a segmenter

nbDeplacements = length(this.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(this.Directions);
if nbDirections == 0
    return
end

nbParams = length(this.ListeParams);

tableauParams=zeros(1,nbParams);

imagettes=this.Params;
for i=1:nbL
    for j=1:nbC
        
        d=imagettes(:,:,:,i,j);
        d(:);
        
        data=reshape(d,nbDeplacements,nbParams);
        moyP=mean(data);
        tableauParams = [tableauParams; moyP]; %#ok<AGROW>
    end
end
tableauParams(1,:)=[];
tabDistances=pdist(tableauParams ,'mahalanobis');
tabLink=linkage(tabDistances,'complete');
figure;
dendrogram(tabLink,0);
tabClass=cluster(tabLink,nbClasses);
