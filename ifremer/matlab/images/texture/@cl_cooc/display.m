% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments
%   this : une instance de cl_cooc
%
% Examples
%   this = cl_cooc('Image', Lena);
%   display(a);
%
% See also cl_cooc cl_cooc/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
fprintf('%s\n', char(this));
