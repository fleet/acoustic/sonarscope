% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_cooc
%
% Name-Value Pair Arguments
%  Cf. cl_cooc
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = getNomFicDatabase('texture01.png');
%   img = imread(nomFic);
%
%   a = cl_cooc('Image', img)
%
%   Image           = get(a, 'Image');
%   Deplacements    = get(a, 'Deplacements')
%   Directions      = get(a, 'Directions')
%   ListeParams     = get(a, 'ListeParams')
%   NomParams       = get(a, 'NomParams')
%   ParamsProny     = get(a, 'ParamsProny')
%   ParamsReduits   = get(a, 'ParamsReduits')
%   s               = get(a, 'Style')
%   FlagProny       = get(a, 'FlagProny')
%
%   [Image, Deplacements, Directions] = get(a, 'Image', 'Deplacements', 'Directions');
%
% See also cl_cooc cl_cooc/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'Label'
            varargout{end+1} = this.Label; %#ok<AGROW>
        case 'Image'
            varargout{end+1} = this.Image; %#ok<AGROW>
        case 'Deplacements'
            varargout{end+1} = this.Deplacements; %#ok<AGROW>
        case 'Directions'
            varargout{end+1} = this.Directions; %#ok<AGROW>
        case 'ListeParams'
            varargout{end+1} = this.ListeParams; %#ok<AGROW>
        case 'NomParams'
            varargout{end+1} = this.NomParams; %#ok<AGROW>
        case 'Params'
            varargout{end+1} = this.Params; %#ok<AGROW>
        case 'PronyReduits'
            varargout{end+1} = this.PronyReduits; %#ok<AGROW>
        case 'WindowAnalyse'
            varargout{end+1} = this.WindowAnalyse; %#ok<AGROW>
        case 'WindowTypeShift'
            varargout{end+1} = this.WindowTypeShift; %#ok<AGROW>
        case 'WindowValShift'
            varargout{end+1} = this.WindowValShift; %#ok<AGROW>
        case 'Style'
            varargout{end+1} = this.Style; %#ok<AGROW>
        case 'nbL'
            varargout{end+1} = this.nbL; %#ok<AGROW>
        case 'nbC'
            varargout{end+1} = this.nbC; %#ok<AGROW>
        case 'ligDeb'
            varargout{end+1} = this.ligDeb; %#ok<AGROW>
        case 'ligFin'
            varargout{end+1} = this.ligFin; %#ok<AGROW>
        case 'colDeb'
            varargout{end+1} = this.colDeb; %#ok<AGROW>
        case 'colFin'
            varargout{end+1} = this.colFin; %#ok<AGROW>
        case 'FlagProny'
            varargout{end+1} = this.FlagProny; %#ok<AGROW>

        otherwise
            w = sprintf('cl_cooc/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
    end
end
