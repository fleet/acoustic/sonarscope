% Affichage des parametres de texture
%
% Syntax
%   histo(a)
%
% Input Arguments
%   a : Une instance de cl_cooc
%
% Name-Value Pair Arguments
%   subDep : indices des deplacements a visualiser
%   subDir : indices des directions a visualiser
%   subPar : indices des parametres a visualiser
%
% Examples
%   nomFic = getNomFicDatabase('texture01.png');
%   img = imread(nomFic);
%   a = cl_cooc('Image', img)
%
%   histo(a)
%   histo(a, 'subDir', [1 3])
%   histo(a, 'subDir', 1, 'subPar', [1 2 5])
%   histo(a, 'subDir', 1, 'subPar', 1:2, 'subDep', 1:10)
%
% See also cl_cooc cl_cooc/plot cl_cooc/plotNuages Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function histo(this, varargin)

[varargin, subDep] = getPropertyValue(varargin, 'subDep', 1:length(this(1).Deplacements));
[varargin, subDir] = getPropertyValue(varargin, 'subDir', 1:length(this(1).Directions));
[varargin, subPar] = getPropertyValue(varargin, 'subPar', 1:length(this(1).ListeParams)); %#ok<ASGLU>

% ---------------------
% Creation de la figure

hPgraphics = figure('NumberTitle', 'off','Name', 'Parametres');

nbImages = numel(this);
for iImage=1:nbImages
    b = this(iImage);
    
    % ---------------------------------
    % Reduction des donees a visualiser
    
    
    b.Deplacements   = b.Deplacements(subDep);
    b.Directions     = b.Directions(subDir);
    b.ListeParams    = b.ListeParams(subPar);
    b.Params         = b.Params(subDep, subDir, subPar, : , :);
    
    if isempty(b.Image)
        continue
    end
    
    if b.FlagProny
        figure;
        imagesc(b.PronyReduits(:,:,1,1,1)'); colorbar; pixval;
        title(['Priode pour ' b.Label]); xlabel('Directions'); ylabel('Parametres texturaux')
    end
    
    ivisu = 0;
    
    nbDeplacements = length(b.Deplacements);  % Nombre de deplacements
    if nbDeplacements == 0
        return
    end
    
    nbDirections = length(b.Directions);
    if nbDirections == 0
        return
    end
    nbParams = length(b.ListeParams);
    
    figure(hPgraphics)
    for iParam=1:nbParams
        for iDirection=1:nbDirections
            ivisu = ivisu + 1;
            subplot(nbParams, nbDirections, ivisu)
            hold on;
            for subLig = 1:b.nbL
                for subCol = 1:b.nbC
                    X = b.Deplacements;
                    Y = b.Params(:,iDirection,iParam,subLig,subCol);
                    
                    cmenu = uicontextmenu;
                    h = plot(X, Y, 'UIContextMenu', cmenu);
                    uimenu(cmenu, 'Text', b.Label);
                    
                    apply(h, b.Style)
                end
            end
            
            for ideplacement=1:nbDeplacements
                Y = b.Params(ideplacement,iDirection,iParam,:,:);
                Y = Y(~isnan(Y));
                if length(Y) > 1
                    [n_normalise1, x] = histo1D(Y, 20, 'Ddp');
                    mu = mean(Y);
                    sigma = std(Y);
                    coef = max(n_normalise1) * 2;
                    
                    %                     cmenu = uicontextmenu;
                    %                     h = plot(b.Deplacements(ideplacement)+n_normalise1/coef, x, 'k+', 'UIContextMenu', cmenu);
                    %                     uimenu(cmenu, 'Text', b.Label);
                    %                     apply(h, b.Style)
                    
                    cmenu = uicontextmenu;
                    h = plot(b.Deplacements(ideplacement) + normpdf(x,mu,sigma)/coef, x,'k', 'UIContextMenu', cmenu);
                    uimenu(cmenu, 'Text', b.Label);
                    Style = set(b.Style, 'Marker', 'none', 'LineStyle', '-');
                    apply(h, Style)
                    
                    cmenu = uicontextmenu;
                    h = plot([b.Deplacements(ideplacement) b.Deplacements(ideplacement)+0.5], [mu mu], '--k', 'UIContextMenu', cmenu);
                    uimenu(cmenu, 'Text', b.Label);
                    apply(h, Style)
                end
            end
            
            
            kParam = b.ListeParams(iParam);
            str = func2str(b.NomParams{kParam});
            angle = (b.Directions(iDirection) -1) * 45;
            grid on;
            title(sprintf('%s  - Direction %d (%d deg) - W=%s', ...
                str, b.Directions(iDirection), angle, num2strCode(b.WindowAnalyse)))
        end
    end
    %     legende{iImage} = b.Label;
end
%     legend(legende, -1)

% for iParam=1:nbParams
%     for iDirection=1:nbDirections
%         for iDeplacement=1:nbDeplacements
%             for iImage = 1:nbImages;
%                 b = this(iImage);
%                 x = squeeze(b.Params(iDeplacement,iDirection,iParam,:,:));
%                 x = x(:);
%                 X(1:length(x),iImage) = x;
%                 G(iImage) = mean(x);
%             end
%             b = sum(G);
%             xm = b / 2;
%             D(iDeplacement,iDirection,iParam) = sum((G - xm) .^ 2) .^ 0.5 / xm;
%         end
%     end
% end
