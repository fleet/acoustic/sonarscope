% Affichage des images
%
% Syntax
%   imagesc(a, ...) 
%
% Input Arguments 
%   a : Une instance de cl_cooc
%
% Name-Value Pair Arguments 
%   Parametres transmis a imagesc
%
% Examples
%   nomFic = getNomFicDatabase('texture01.png');
%   img = imread(nomFic);
%   a = cl_cooc('Image', img)
%
%   imagesc(a)
%
% See also cl_cooc cl_cooc/set Authors
% Authors : JMA
% VERSION  : $Id: imagesc.m,v 1.1 2002/10/21 11:59:51 emenut Exp emenut $
% ----------------------------------------------------------------------------

function imagesc(this, varargin)

nbImages = numel(this);
for iImage = 1:nbImages
    b = this(iImage);

    if isempty(b.Image)
        continue
    end

    figure('NumberTitle', 'off','Name', ['Image : ' b.Label])
    imagesc(b.Image, varargin{:}); colorbar; colormap(gray(256));
    title(b.Label)
end
