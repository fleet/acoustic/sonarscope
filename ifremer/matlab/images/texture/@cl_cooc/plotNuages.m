% Affichage des parametres de texture
%
% Syntax
%   plotNuages(a)
%
% Input Arguments
%   a : Une instance de cl_cooc
%
% Name-Value Pair Arguments
%   subDep  : Indices des deplacements a visualiser
%   subDir  : Indices des directions a visualiser
%   subParX : Indices des parametres a visualiser en abscisses
%   subParY : Indices des parametres a visualiser en ordonnees
%
% Name-only Arguments
%   newFig         : Creation d'une figure pour chaque couple de parametres
%   ellipsesSeules : Visualiation uniquement des ellipses
%
% Examples
%   nomFic = getNomFicDatabase('texture01.png');
%   img = imread(nomFic);
%   a = cl_cooc('Image', img)
%
%   plotNuages(a, 'subdep', [1 6], 'subParX', 6, 'subParY', [1:5 7 11:16])
%   plotNuages(a, 'subdep', [1 6])
%   plotNuages(a, 'subdep', 1, 'newFig')
%
% See also cl_cooc cl_cooc/plot cl_cooc/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plotNuages(this, varargin)

[varargin, Callback] = getFlag(varargin, 'Callback');
[varargin, visuDistance] = getFlag(varargin, 'visuDistance');
if ~Callback
    [varargin, subDep]  = getPropertyValue(varargin, 'subDep', 1:length(this(1).Deplacements));
    [varargin, subDir]  = getPropertyValue(varargin, 'subDir', 1:length(this(1).Directions));
    [varargin, subParX] = getPropertyValue(varargin, 'subParX', 1:length(this(1).ListeParams));
    [varargin, subParY] = getPropertyValue(varargin, 'subParY', 1:length(this(1).ListeParams));
    
    [varargin, newFig]         = getFlag(varargin, 'newFig');
    [varargin, ellipsesSeules] = getFlag(varargin, 'ellipsesSeules'); %#ok<ASGLU>
    
    Deplacements  = this(1).Deplacements(subDep);
    Directions    = this(1).Directions(subDir);
    ListeParamsX  = this(1).ListeParams(subParX);
    ListeParamsY  = this(1).ListeParams(subParY);
    nomParams     = this(1).NomParams;
    
    nbImages        = numel(this);
    nbDeplacements  = length(Deplacements);
    nbDirections    = length(Directions);
    nbParamsX       = length(ListeParamsX);
    nbParamsY       = length(ListeParamsY);
    
    if nbDeplacements == 0
        return
    end
    if nbDirections == 0
        return
    end
    
    %% Cr�ation de la figure
    
    for iDirection=1:nbDirections
        angle = (Directions(iDirection) -1) * 45;
        for iDep=1:nbDeplacements
            str = sprintf('Direction %d (%d deg) - Deplacement %d', ...
                Directions(iDirection), angle, Deplacements(iDep));
            
            if ~newFig
                figure('NumberTitle', 'off', 'Name', str);
            end
            ivisu = 0;
            
            for iParamx=1:nbParamsX
                nomParamx = func2str(nomParams{ListeParamsX(iParamx)});
                for iParamy=1:nbParamsY
                    ivisu = ivisu + 1;
                    if ~newFig
                        %                             if newFig
                        %                                 if iParamy >= iParamx
                        %                                     continue
                        %                                 end
                        %                             else
                        %                                 if iParamy > iParamx
                        %                                     continue
                        %                                 end
                        %                             end
                    end
                    nomParamy = func2str(nomParams{ListeParamsY(iParamy)});
                    
                    if newFig
                        figure('NumberTitle', 'off', 'Name', str);
                    else
                        subplot(nbParamsY, nbParamsX, ivisu)
                        
                        %                             if (nbParamsX > 1) | (nbParamsY > 1)
                        %                                 commande = sprintf('plotNuages(cl_cooc([]),''Callback'', %d, %d, %d, %d)', ...
                        %                                     subDep(iDep), subDir(iDirection), ...
                        %                                     subParX(iParamx), subParY(iParamy));
                        %                                 set(gca, 'ButtonDownFcn', commande)
                        %                             end
                        
                        if (nbParamsX == 1) || (nbParamsY == 1)
                            if ~visuDistance
                                commande = sprintf('plotNuages(cl_cooc([]),''Callback'', %d, %d, %d, %d, ''visuDistance'')', ...
                                    subDep(iDep), subDir(iDirection), ...
                                    subParX(iParamx), subParY(iParamy));
                                set(gca, 'ButtonDownFcn', commande)
                            end
                        else
                            commande = sprintf('plotNuages(cl_cooc([]),''Callback'', %d, %d, %d, %d)', ...
                                subDep(iDep), subDir(iDirection), ...
                                subParX(iParamx), subParY(iParamy));
                            set(gca, 'ButtonDownFcn', commande)
                        end
                    end
                    
                    if ~ellipsesSeules
                        plotPatch(this, nbImages, subDep, iDep, subDir, iDirection, ...
                            subParX, iParamx, subParY, iParamy,visuDistance );
                    end
                    
                    grid on;
                    for iImage=1:nbImages
                        b = this(iImage);
                        if isempty(b.Image)
                            continue;
                        end
                        Y = squeeze(b.Params(subDep(iDep), ...
                            subDir(iDirection), ...
                            subParX(iParamx), : , :));
                        X = squeeze(b.Params(subDep(iDep), ...
                            subDir(iDirection), ...
                            subParY(iParamy), : , :));
                        hold on;
                        
                        if ~ellipsesSeules
                            h = plot(X, Y);
                            apply(h, b.Style);
                        end
                        
                        %                             [XEllipse, YEllipse] = fitEllipse(X, Y);
                        
                        
                        Couleur(iImage,:)  = get(b.Style, 'Color'); %#ok<AGROW>
                        %                             plot(XEllipse, YEllipse, 'k'); grid on
                        
                        if length(X) > 1
                            subNonNaN = ~isnan(X) & ~isnan(Y);
                            ellipse = dist1mahal([X(subNonNaN) Y(subNonNaN)]);
                            plot(ellipse(:, 1), ellipse(:, 2), 'Color', Couleur(iImage,:)); grid on
                        end
                        
                        if length(X) == 1
                            singleton(iImage) = 1; %#ok
                        else
                            singleton(iImage) = 0; %#ok
                        end
                    end
                    if nbParamsX > 1 || nbParamsY > 1
                        if iParamy == 1
                            ylabel(nomParamx);
                        end
                        if iParamx == nbParamsX
                            xlabel(nomParamy);
                        end
                    else
                        ylabel(nomParamy);
                        xlabel(nomParamx);
                    end
                end
            end
            if ~newFig
                UserData = [];
                UserData = putInUserData( 'Objet', this, UserData);
                set(gcf, 'UserData', UserData)
            end
        end
    end
    axis tight;
else
    UserData = get(gcf, 'UserData');
    this    = getFromUserData( 'Objet', UserData);
    iDep       = varargin{1};
    iDirection = varargin{2};
    iParamx    = varargin{3};
    iParamy    = varargin{4};
    if visuDistance
        plotNuages(this, ...
            'subDep',  iDep, ...
            'subDir',  iDirection, ...
            'subParX', iParamx, ...
            'subParY', iParamy, 'visuDistance')
    else
        plotNuages(this, ...
            'subDep',  iDep, ...
            'subDir',  iDirection, ...
            'subParX', iParamx, ...
            'subParY', iParamy)
    end
end


function plotPatch(this, nbImages, subDep, iDep, subDir, iDirection, ...
    subParX, iParamx, subParY, iParamy, visuDistance)

minX = Inf;
maxX = -Inf;
minY = Inf;
maxY = -Inf;

for iImage=1:nbImages
    b = this(iImage);
    if isempty(b.Image)
        continue;
    end
    Y = squeeze(b.Params(subDep(iDep), ...
        subDir(iDirection), ...
        subParX(iParamx), : , :));
    X = squeeze(b.Params(subDep(iDep), ...
        subDir(iDirection), ...
        subParY(iParamy), : , :));
    Couleur(iImage,:)  = get(b.Style, 'Color'); %#ok<AGROW>
    hold on;
    
    Z{iImage} = [X(:), Y(:)]; %#ok
    minX = min(minX, min(X(:)));
    maxX = max(maxX, max(X(:)));
    minY = min(minY, min(Y(:)));
    maxY = max(maxY, max(Y(:)));
    
    if length(X) > 1
        ellipse = dist1mahal([X(:) Y(:)]);
        minX = min(minX, min(ellipse(:,1)));
        maxX = max(maxX, max(ellipse(:,1)));
        minY = min(minY, min(ellipse(:,2)));
        maxY = max(maxY, max(ellipse(:,2)));
    end
    
    if length(X) == 1
        singleton(iImage) = 1; %#ok
    else
        singleton(iImage) = 0; %#ok
    end
end

xg = linspace(minX, maxX, 100);
yg = linspace(minY, maxY, 100);
[Xg,Yg] = meshgrid( xg, yg);

DMahal = zeros(100, 100, nbImages ) * Inf;
for iImage=1:nbImages
    if ~singleton(iImage)
        ZI = Z{iImage};
        subNonNaN = ~isnan(sum(ZI,2));
        D = mahalanobis([Xg(:) Yg(:)], ZI(subNonNaN,:));
        N = sqrt(size(D,1));
        D = reshape(D, N, N);
        DMahal(:,:,iImage) = D;
        %                                 figure; imagesc(xg, yg, D'); axis xy
    end
end
[DMin, subDMin] = min(DMahal,[], 3);

if visuDistance
    imagesc(xg, yg, DMin);
    colormap(jet(256)); colorbar
else
    Coul = Couleur;
    for i=1:size(Coul,1)
        if isequal(Coul(i,:), [0 0 0])
            Coul(i,:) = [0.75 0.75 0.75];
        else
            Coul(i,:) = rgb2hsv(Coul(i,:));
            Coul(i,2) = Coul(i,2) * 0.25;
            Coul(i,:) = hsv2rgb(Coul(i,:));
        end
    end
    
    h = imagesc(xg, yg, subDMin);
    set_HitTest_Off(h)
    colormap(Coul);
end
