function [x, xEntiers] = calculPiquets(Deplacements)

x = Deplacements /sqrt(2);
xEntiers = unique([floor(x) (floor(x)+1) ceil(x)]);
xEntiers(xEntiers == 0) = [];

% Verrue pour corriger le probl�me pour un deplacement de 1
if length(xEntiers) == 1
    xEntiers = [xEntiers xEntiers+1];
end
