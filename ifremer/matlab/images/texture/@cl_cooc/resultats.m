function Res = resultats(this,varargin)

nbDeplacements = length(this.Deplacements);
nbDirections   = length(this.Directions);
nbParams       = length(this.ListeParams);
nbL            = this.nbL;
nbC            = this.nbC;

[varargin, subDep] = getPropertyValue(varargin, 'subDep', 1:nbDeplacements);
[varargin, subDir] = getPropertyValue(varargin, 'subDir', 1:nbDirections);
[varargin, subPar] = getPropertyValue(varargin, 'subPar', 1:nbParams); %#ok<ASGLU>


this.Deplacements = this.Deplacements(subDep);
this.Directions   = this.Directions(subDir);
this.ListeParams  = this.ListeParams(subPar);
this.Params       = this.Params(subDep,1, subPar, : , :);

% -----------------------------------
% Recuperation des donnes a segmenter

nbDeplacements = length(this.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(this.Directions);
if nbDirections == 0
    return
end

nbParams = length(this.ListeParams);

Res=zeros(1, nbParams);
imagettes=this.Params;
for i=1:nbL
    for j=1:nbC
        d = imagettes(:,:,:,i,j);
        
        data = reshape(d,nbDeplacements,nbParams);
        Res = [Res; data]; %#ok<AGROW>
    end
end
Res(1,:) = [];
