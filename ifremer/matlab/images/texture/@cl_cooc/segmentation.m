% Segmentation par EQM
%
% Syntax
%   S = segmentation(this, Moyenne, EcartType, Ponder, Masque)
%
% Input Arguments
%   A : Classes d'apprentissage
%
% Examples
%
% See also cl_cooc cl_cooc/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Segm = segmentation(this, Moyenne, EcartType, varargin)

nbDeplacements  = length(this.Deplacements);
nbDirections    = length(this.Directions);
nbParams        = length(this.ListeParams);

[varargin, subDep] = getPropertyValue(varargin, 'subDep', 1:nbDeplacements);
[varargin, subDir] = getPropertyValue(varargin, 'subDir', 1:nbDirections);
[varargin, subPar] = getPropertyValue(varargin, 'subPar', 1:nbParams);
[varargin, Ponder] = getPropertyValue(varargin, 'Ponder', []);
[varargin, Masque] = getPropertyValue(varargin, 'Mask', []); %#ok<ASGLU>

if isempty(Ponder)
    Ponder = ones(nbDeplacements, nbDirections, nbParams);
    charPonder = '';
else
    charPonder = 'Ponder';
end

if isempty(Masque)
    Masque = ones(nbDeplacements, nbDirections, nbParams);
    charMasque = '';
else
    charMasque = 'Mask';
end

this.Deplacements   = this.Deplacements(subDep);
this.Directions     = this.Directions(subDir);
this.ListeParams    = this.ListeParams(subPar);
this.Params         = this.Params(subDep, subDir, subPar, : , :);
Moyenne             = Moyenne(  subDep, subDir, subPar,:);
EcartType           = EcartType(subDep, subDir, subPar,:);
Ponder              = Ponder(subDep, subDir, subPar);
Masque              = Masque(subDep, subDir, subPar);

% -----------------------------------
% Recuperation des donnes a segmenter

nbDeplacements = length(this.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(this.Directions);
if nbDirections == 0
    return
end

nbParams = length(this.ListeParams);


% ------------
% Segmentation

if any(EcartType(:))
    Algo = 2;
    if ~any(Ponder(:))
        Segm = [];
        my_warndlg('cl_cooc:apprentissage C''est pas possible de segmenter avec des donnees aussi pourries. Si tu veux vraiment le faire, il faut faire Masque(:)=1 et Ponder(:)=1', 0);
        return
    end
else
    Algo = 1;
end
nbA = size(Moyenne, 4);

k = 0;
nbImagettes = this.nbL * this.nbC;
hw = create_waitbar('Segmentation en cours', 'N', nbImagettes);
for subLig =1:this.nbL
    for subCol = 1:this.nbC
        k = k+1;
        my_waitbar(k, nbImagettes, hw)
        switch Algo
            case 1
                for iDirection=1:nbDirections
                    for iParam=1:nbParams
                        %                         Masque(:,iDirection,iParam);
                        Signal = this.Params(:,iDirection,iParam,subLig,subCol);
                        [erreur, indMin] = eqm(Signal', squeeze(Moyenne(:,iDirection,iParam,:))');
                        Segm.Image(subLig,subCol) = indMin;
                        Segm.probas(subLig,subCol,:) = erreur;
                        
                        
                        kParam = this.ListeParams(iParam);
                        Segm.Titre = ['From ' func2str(this.NomParams{kParam})];
                        
                        %                 r = corrcoef([Signal' squeeze(Moyenne(:,iParam,:))]);
                        %                 [pppp, indMin] = min(r(2:end, 1));
                        %                 Segm.Image(subLig,subCol) = indMin;
                    end
                end
            case 2
                for iDirection=1:nbDirections
                    for iParam=1:nbParams
                        sub = find(Masque(:,iDirection,iParam));
                        if isempty(sub)
                            continue
                        end
                        
                        Signal = this.Params(sub,iDirection,iParam,subLig,subCol);
                        
                        % On tente quelque chose
                        %                         essaiMax = max(squeeze(Moyenne(sub,iDirection,iParam,:))');
                        %                         essaiMin = min(squeeze(Moyenne(sub,iDirection,iParam,:))');
                        %                         for iSub = 1:length(sub)
                        %                             if Signal(iSub) < essaiMin(iSub)
                        %                                 Signal(iSub) = essaiMin(iSub);
                        %                             end
                        %                             if Signal(iSub) > essaiMax(iSub)
                        %                                 Signal(iSub) = essaiMax(iSub);
                        %                             end
                        %                         end
                        
                        for iImage = 1:nbA
                            m = squeeze(Moyenne(sub,iDirection,iParam,iImage))';
                            s = squeeze(EcartType(sub,iDirection,iParam,iImage))';
                            
                            P = exp(-((Signal' - m) ./ s) .^ 2);
                            %                             P = 1./((Signal' - m) ./ s) .^ 2;
                            P(isnan(P)) = 0;
                            
                            proba(sub,iDirection,iParam,iImage)  = P; %#ok
                            ponder(sub,iDirection,iParam,iImage) = Ponder(sub,iDirection,iParam); %#ok
                        end
                    end
                end
                
                q = sum(sum(sum(proba .* ponder,1),2),3);
                
                %                 logProba = log10(proba);
                %                 logProba(proba == 0) = 0;
                %                 q = sum(sum(sum(logProba .* ponder,1),2),3);
                
                [erreur, indMax] = max(q);
                Segm.Image(subLig,subCol) = indMax;
                Segm.probas(subLig,subCol,:) = q;
        end
    end
end
my_close(hw, 'MsgEnd')

titre = sprintf('Dep=%s Dir=%s Par=%s %s %s', ...
    num2strCode(this.Deplacements), ...
    num2strCode(this.Directions), ...
    num2strCode(this.ListeParams), ...
    charPonder, charMasque);
Segm.Titre = titre;
