% Segmentation par EQM
%
% Syntax
%   [Moyenne, EcartType, Ponder, Masque] = apprentissage(A, ...)
%
% Input Arguments
%   A : Classes d'apprentissage :tableau d'instances de type cl_texture
%
% Name-Value Pair Arguments
%   typeDistInterClasses : {1=moyenne} | 2=median | 3=somme
%                          moyenne : Distance inter-classes calculee par calcul de proba des moyennes
%                                       d'une classe par rapport aux autres
%                          median  : Distance inter-classes calculee par calcul de proba de la valeur mediane
%                                       d'une classe par rapport aux autres
%                          somme   : Distance inter-classes calculee par calcul de la somme des probas
%                                       d'une classe par rapport aux autres
%   typePonder  :  {1=Ponderation exponentielle} | 2=Ponderation par l'inverse de la distance
%   SeuilMasque : Seuil a appliquer a ponder pour obtenir le masque
%   coefMagique : Coefficient a estimer par essai-erreurs (1 par defaut)
%
% Output Arguments
%   [] : Auto-plot activation
%   Moyenne   :
%   EcartType :
%   Ponder    :
%
% Examples
%
% See also cl_texture cl_texture/segmentationEqm Authors
% Authors : JMA
% VERSION  : $Id: apprentissage2.m,v 1.1 2002/11/25 16:54:08 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [Moyenne, EcartType, Ponder, Masque] = apprentissage(A, varargin)

[varargin, typeDistInterClasses] = getPropertyValue(varargin, 'typeDistInterClasses', 1);
[varargin, typePonder]           = getPropertyValue(varargin, 'typePonder', 1);
[varargin, SeuilMasque]          = getPropertyValue(varargin, 'SeuilMasque', 0.1);
[varargin, coefMagique]          = getPropertyValue(varargin, 'coefMagique', 1); %#ok<ASGLU>

% ---------------------------------------------------------------------
% On recupere le premier element de A uniquement pour obtenir le nombre
% de parametres, le nombre de deplacements et le nombre de directions

b = A(1);
nbParams = length(b.ListeParams);

nbDeplacements = length(b.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(b.Directions);
if nbDirections == 0
    return
end

% ----------------------------------------
% Recuperation des donnees d'apprentissage

nbA = length(A);
Moyenne = zeros(nbDeplacements, nbDirections, nbParams, nbA);
EcartType            = zeros(nbDeplacements, nbDirections, nbParams, nbA);


for iImage = 1:nbA
    b = A(iImage);
    nbParams = length(b.ListeParams);
    
    nbDeplacements = length(b.Deplacements);  % Nombre de deplacements
    if nbDeplacements == 0
        return
    end
    
    nbDirections = length(b.Directions);
    if nbDirections == 0
        return
    end
    
    nbImagettes = b.nbL*b.nbC;
    if nbImagettes == 1
        % Cas ou on l'apprentissage s'est fait de maniere globale sur
        % une image. On lit donc directement la valeur moyenne
        Algo = 1;
        for iParam=1:nbParams
            for iDirection=1:nbDirections
                Moyenne(:,iDirection, iParam,iImage) = b.Params(:,iDirection,iParam,1,1)';
            end
        end
    else
        % Cas ou l'apprentissage s'est fait sur une decomposition en
        % imagettes. On peut donc calculer la valeur moyennet et la
        % variance
        Algo = 2;
        for iParam=1:nbParams
            for iDirection=1:nbDirections
                for subLig = 1:b.nbL
                    for subCol = 1:b.nbC
                        Moyenne(:,iDirection,iParam,iImage) = ...
                            Moyenne(:,iDirection,iParam,iImage) ...
                            + b.Params(:,iDirection,iParam,subLig,subCol);
                        EcartType(:,iDirection,iParam,iImage) = ...
                            EcartType(:,iDirection,iParam,iImage) ...
                            + (b.Params(:,iDirection,iParam,subLig,subCol)).^2;
                    end
                end
                Moyenne(:,iDirection,iParam,iImage) = ...
                    Moyenne(:,iDirection,iParam,iImage) / nbImagettes;
                EcartType(:,iDirection,iParam,iImage) = ...
                    EcartType(:,iDirection,iParam,iImage) / nbImagettes;
                EcartType(:,iDirection,iParam,iImage) = ...
                    (EcartType(:,iDirection,iParam,iImage) ...
                    - Moyenne(:,iDirection,iParam,iImage) .^ 2) .^ 0.5;
            end
        end
    end
end


%----------------------------
% Calcul ACP
%----------------------------
%     for i=1:nbDeplacements
%         for j=1:nbDirections
%             for k = 1:nbA
%                 A=Moyenne(i,j,:,k)
%             end
%         end
%     end
%

if Algo == 1
    return
end

% -----------------------
% Calcul des ponderateurs


for iParam=1:nbParams
    for iDirection=1:nbDirections
        for iDeplacement=1:nbDeplacements
            P1 = 0; P2 = 0;
            for iImage1 = 1:nbA
                b1 = A(iImage1);
                x1 = squeeze(b1.Params(iDeplacement,iDirection,iParam,:,:));
                x1 = x1(:);
                %                     X1(:,iImage1) = x1;
                m1 = Moyenne(iDeplacement,iDirection,iParam,iImage1);
                s1 = EcartType(iDeplacement,iDirection,iParam,iImage1);
                
                for iImage2 = 1:nbA
                    if iImage1 == iImage2
                        continue
                    end
                    b2 = A(iImage2);
                    x2 = squeeze(b2.Params(iDeplacement,iDirection,iParam,:,:));
                    x2 = x2(:);
                    %                         X2(:,iImage) = x2;
                    m2 = Moyenne(iDeplacement,iDirection,iParam,iImage2);
                    s2 = EcartType(iDeplacement,iDirection,iParam,iImage2);
                    
                    switch typeDistInterClasses
                        case 1  % Distance inter-classes calculee par calcul de proba des moyennes
                            % d'une classe par rapport aux autres
                            P1 = P1 + exp(-((m1 - m2) ./ s2) .^ 2);
                            P2 = P2 + exp(-((m2 - m1) ./ s1) .^ 2);
                        case 2  % Distance inter-classes calculee par calcul de proba de la valeur mediane
                            % d'une classe par rapport aux autres
                            P1 = P1 + median(exp(-((x1' - m2) ./ s2) .^ 2));
                            P2 = P2 + median(exp(-((x2' - m1) ./ s1) .^ 2));
                        case 3  % Distance inter-classes calculee par calcul de la somme des probas
                            % d'une classe par rapport aux autres
                            P1 = P1 + sum(exp(-((x1' - m2) ./ s2) .^ 2)) / nbDeplacements;
                            P2 = P2 + sum(exp(-((x2' - m1) ./ s1) .^ 2)) / nbDeplacements;
                    end
                end
            end
            P1 = P1 / nbA;
            P2 = P2 / nbA;
            switch typePonder
                case 1  % Ponderation exponentielle
                    Ponder(iDeplacement,iDirection,iParam) = exp(-((P1 + P2)*coefMagique).^2); %#ok<AGROW> % Pourquoi 2000 ?
                case 2 % Ponderation par l'inverse de la distance
                    Ponder(iDeplacement,iDirection,iParam) = 1 ./(P1 + P2); %#ok<AGROW>
                case 3 % Baccacharia
                    % ???
            end
        end
    end
end
Ponder(isnan(Ponder)) = 0;

Masque = zeros(size(Ponder));
sub = find(Ponder >= SeuilMasque);
if isempty(sub)
    my_warndlg('cl_texture:apprentissage C''est pas possible de segmenter avec des donnees aussi pourries. Si tu veux vraiment le faire, il faut faire Masque(:)=1 et Ponder(:)=1', 0);
    
    %         return
end
Masque(sub) = 1;
for iDirection=1:nbDirections
    sub = (1:nbDeplacements) + (iDirection-1)*(nbDeplacements);
    separation(iDirection) = sub(1); %#ok<AGROW>
    for iParam=1:nbParams
        imagePonder(iParam,sub) = Ponder(:,iDirection,iParam)'; %#ok<AGROW>
        imageMasque(iParam,sub) = Masque(:,iDirection,iParam)'; %#ok<AGROW>
    end
end

% --------------------
% Auto-plot activation

if (nargout == 0) && (Algo == 2)
    X = (1:size(imagePonder, 2)) + 0.5;
    Y = (1:size(imagePonder, 1)) + 0.5;
    figure('NumberTitle', 'off','Name', 'Ponderation')
    imagesc(X, Y, imagePonder); colorbar; pixval;
    xlabel('Deplacements x Directions')
    ylabel('Parametres')
    title('Ponderation')
    hold on;
    for iDirection=1:nbDirections
        plot([separation(iDirection) separation(iDirection)], [1 nbParams+1], 'k', 'LineWidth', 0.5)
    end
    
    
    figure('NumberTitle', 'off','Name', 'Mask')
    imagesc(X, Y, imageMasque); colorbar; pixval;
    xlabel('Deplacements x Directions')
    ylabel('Parametres')
    hold on;
    for iDirection=1:nbDirections
        plot([separation(iDirection) separation(iDirection)], [1 nbParams+1], 'k', 'LineWidth', 0.5)
    end
    
    nbParamCalcules = sum(imageMasque(:)) / numel(imageMasque) * 100;
    nbMatCalculees  = sum(sum(imageMasque) ~= 0) / size(imageMasque,2) * 100;
    
    title(sprintf('Masque - Seuil=%f - NbParams=%5.1f%c - nbMatrices=%5.1f%c', ...
        SeuilMasque, nbParamCalcules, '%', nbMatCalculees, '%'))
    
    
    % --------------------------------
    % Affichage des moyennes calculees
    
    figure('NumberTitle', 'off','Name', 'Moyennes');
    ivisu =0;
    for iParam=1:nbParams
        for iDirection=1:nbDirections
            ivisu = ivisu + 1;
            subplot(nbParams, nbDirections, ivisu)
            hold on;
            for iImage=1:nbA
                b = A(iImage);
                X = b.Deplacements;
                Y = Moyenne(:,iDirection,iParam,iImage);
                h = plot(X, Y);
                s = set(b.Style, 'LineStyle', '-', 'Marker', 'none');
                apply(h, s)
                
                Sig = EcartType(:,iDirection,iParam,iImage);
                h = plot(X, Y-Sig);
                s = set(b.Style, 'LineStyle', '--', 'Marker', 'none');
                apply(h, s)
                h = plot(X, Y+Sig);
                s = set(b.Style, 'LineStyle', '--', 'Marker', 'none');
                apply(h, s)
                
                
                kParam = b.ListeParams(iParam);
                str = func2str(b.NomParams(kParam));
                angle = (b.Directions(iDirection) -1) * 45;
                grid on;
                title(sprintf('%s  - Direction %d (%d deg) - W=%s', ...
                    str, b.Directions(iDirection), angle, num2strCode(b.WindowAnalyse)))
            end
        end
    end
    
    
    % ----------------------------------
    % Affichage des ecart-types calcules
    
    %         figure('NumberTitle', 'off','Name', 'Ecart-Types');
    %         ivisu =0;
    %         for iParam=1:nbParams
    %             for iDirection=1:nbDirections
    %                 ivisu = ivisu + 1;
    %                 subplot(nbParams, nbDirections, ivisu)
    %                 hold on;
    %                 for iImage=1:nbA
    %                     b = A(iImage);
    %                     X = b.Deplacements;
    %                     Y = EcartType(:,iDirection,iParam,iImage);
    %                     h = plot(X, Y);
    %                     s = set(b.Style, 'LineStyle', '-', 'Marker', 'none');
    %                     apply(h, s)
    %                     kParam = b.ListeParams(iParam);
    %                     str = func2str(b.NomParams(kParam));
    %                     angle = (b.Directions(iDirection) -1) * 45;
    %                     grid on;
    %                     title(sprintf('%s  - Direction %d (%d deg) - W=%s', ...
    %                         str, b.Directions(iDirection), angle, num2strCode(b.WindowAnalyse)))
    %                 end
    %             end
    %         end
end

% Maintenance Auto : catch
% Maintenance Auto :     err ('cl_texture', 'apprentissage', '');
% Maintenance Auto : end

