% Segmentation par EQM
%
% Syntax
%   [Moyenne, EcartType, Ponder, Masque] = apprentissage(A, ...)
%
% Input Arguments
%   A : Classes d'apprentissage :tableau d'instances de type cl_texture
%
% Name-Value Pair Arguments
%   typeDistInterClasses : {1=moyenne} | 2=median | 3=somme
%                          moyenne : Distance inter-classes calculee par calcul de proba des moyennes
%                                       d'une classe par rapport aux autres
%                          median  : Distance inter-classes calculee par calcul de proba de la valeur mediane
%                                       d'une classe par rapport aux autres
%                          somme   : Distance inter-classes calculee par calcul de la somme des probas
%                                       d'une classe par rapport aux autres
%   typePonder  :  {1=Ponderation exponentielle} | 2=Ponderation par l'inverse de la distance
%   SeuilMasque : Seuil a appliquer a ponder pour obtenir le masque
%   coefMagique : Coefficient a estimer par essai-erreurs (1 par defaut)
%
% Output Arguments
%   [] : Auto-plot activation
%   Moyenne   :
%   EcartType :
%   Ponder    :
%
% Examples
%
% See also cl_texture cl_texture/segmentationEqm Authors
% Authors : JMA
% VERSION  : $Id: apprentissageEM.m,v 1.1 2002/11/25 16:54:08 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [newdat, pcs, variance,t2,g ,mo, st,l1 ] = apprentissageAcp4C(A,cl1,cl2,cl3,cl4, direction,nbCl, varargin) %#ok<INUSL>

% [varargin, typeDistInterClasses] = getPropertyValue(varargin, 'typeDistInterClasses', 1);
% [varargin, typePonder] = getPropertyValue(varargin, 'typePonder', 1);
% [varargin, SeuilMasque] = getPropertyValue(varargin, 'SeuilMasque', 0.1);
% [varargin, coefMagique] = getPropertyValue(varargin, 'coefMagique', 1);

% ---------------------------------------------------------------------
% On recupere le premier element de A uniquement pour obtenir le nombre
% de parametres, le nombre de deplacements et le nombre de directions

b = A(1);
nbParams = length(b.ListeParams);

nbDeplacements = length(b.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(b.Directions);
if nbDirections == 0
    return
end

% ----------------------------------------
% Recuperation des donnees d'apprentissage

%nbA = length(A);
nbA=4;
% nbImagettes=A(1).nbL*A(1).nbC;
NBL=A(1).nbL;
NBC=A(1).nbC;

for iImage = 1:nbA
    
    if iImage==1
        b = A(cl1);
    end
    
    if iImage==2
        b = A(cl2);
    end
    
    if iImage==3
        b = A(cl3);
    end
    
    if iImage==4
        b = A(cl4);
    end
    
    
    nbParams = length(b.ListeParams);
    
    nbDeplacements = length(b.Deplacements);  % Nombre de deplacements
    if nbDeplacements == 0
        return
    end
    
    nbDirections = length(b.Directions);
    if nbDirections == 0
        return
    end
    for i=1:NBL
        for j=1:NBC
            datu((iImage-1)*NBL*NBC+(j+(i-1)*NBC),:,:,:)=squeeze((b.Params(:,1,:,i,j)));%#ok<AGROW> %%%direction
        end
    end
end
l1=zeros(1,nbParams);
for iImage = 1:nbA*NBL*NBC
    
    l=squeeze(datu(iImage,:,:,:));
    l1=[l1; l]; %#ok<AGROW>
end
l1(1,:)=[];


st=std(l1);

% BB=repmat(st,nbA*NBL*NBC*nbDeplacements,1);
% L2=l1./BB;
mo=mean(l1);

[pcs,newdat,variance,t2] = pca(l1);

nbIpC=nbA/nbCl;
dv=nbIpC*nbDeplacements;
for l=1:nbA*nbDeplacements
    g(l)=ceil(l/dv); %#ok<AGROW>
end
