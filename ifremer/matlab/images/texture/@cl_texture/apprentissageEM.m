% Segmentation par EQM
%
% Syntax
%   [Moyenne, EcartType, Ponder, Masque] = apprentissage(A, ...)
%
% Input Arguments
%   A : Classes d'apprentissage :tableau d'instances de type cl_texture
%
% Name-Value Pair Arguments
%   typeDistInterClasses : {1=moyenne} | 2=median | 3=somme
%                          moyenne : Distance inter-classes calculee par calcul de proba des moyennes
%                                       d'une classe par rapport aux autres
%                          median  : Distance inter-classes calculee par calcul de proba de la valeur mediane
%                                       d'une classe par rapport aux autres
%                          somme   : Distance inter-classes calculee par calcul de la somme des probas
%                                       d'une classe par rapport aux autres
%   typePonder  :  {1=Ponderation exponentielle} | 2=Ponderation par l'inverse de la distance
%   SeuilMasque : Seuil a appliquer a ponder pour obtenir le masque
%   coefMagique : Coefficient a estimer par essai-erreurs (1 par defaut)
%
% Output Arguments
%   [] : Auto-plot activation
%   Moyenne   :
%   EcartType :
%   Ponder    :
%
% Examples
%
% See also cl_texture cl_texture/segmentationEqm Authors
% Authors : JMA
% VERSION  : $Id: apprentissageEM.m,v 1.1 2002/11/25 16:54:08 augustin Exp augustin $
% -------------------------------------------------------------------------

function [newdat, pcs, g ,mo, st ] = apprentissageEM(A, varargin)

% [varargin, typeDistInterClasses] = getPropertyValue(varargin, 'typeDistInterClasses', 1);
% [varargin, typePonder] = getPropertyValue(varargin, 'typePonder', 1);
% [varargin, SeuilMasque] = getPropertyValue(varargin, 'SeuilMasque', 0.1);
% [varargin, coefMagique] = getPropertyValue(varargin, 'coefMagique', 1);

% ---------------------------------------------------------------------
% On recupere le premier element de A uniquement pour obtenir le nombre
% de parametres, le nombre de deplacements et le nombre de directions

b = A(1);
nbParams = length(b.ListeParams);

nbDeplacements = length(b.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(b.Directions);
if nbDirections == 0
    return
end

% ----------------------------------------
% Recuperation des donnees d'apprentissage

nbA = length(A);
% Moyenne = zeros(nbDeplacements, nbDirections, nbParams, nbA);
%EcartType            = zeros(nbDeplacements, nbDirections, nbParams, nbA);


for iImage = 1:nbA
    b = A(iImage);
    nbParams = length(b.ListeParams);
    
    nbDeplacements = length(b.Deplacements);  % Nombre de deplacements
    if nbDeplacements == 0
        return
    end
    
    nbDirections = length(b.Directions);
    if nbDirections == 0
        return
    end
    
    nbImagettes = b.nbL*b.nbC;
    for iParam=1:nbParams
        for iDirection=1:nbDirections
            Moyenne(:,iDirection, iParam,iImage) = b.Params(:,iDirection,iParam,1,1)'; %#ok<AGROW>
        end
    end
    if nbImagettes == 1
        % Cas ou on l'apprentissage s'est fait de maniere globale sur
        % une image. On lit donc directement la valeur moyenne
        %         Algo = 1;
        for iParam=1:nbParams
            for iDirection=1:nbDirections
                Moyenne(:,iDirection, iParam,iImage) = b.Params(:,iDirection,iParam,1,1)';
            end
        end
    end
end

GM=zeros(nbDeplacements,nbDirections,nbA,nbParams);
% Tab=zeros(nbDeplacements,nbDirections,nbA,nbParams);
for iParam=1:nbParams
    for iDirection=1:nbDirections
        
        
        for iImage1 = 1:nbA
            for iDeplacement=1:nbDeplacements
                GM (iDeplacement,iDirection,iImage1,iParam)= Moyenne(iDeplacement,iDirection,iParam,iImage1);
                %s1 = EcartType(iDeplacement,iDirection,iParam,iImage1);
                
                
            end
            
            
        end
    end
end
% DEP=(1:nbDeplacements);
% DEP=DEP';
% PCS=zeros(1:nbParams);
%PCS=PCS';
for iDirection=1:nbDirections
    
    % for iImage1 = 1:nbA
    l=GM(:,iDirection,:,:);
    % l(:);
    l1=reshape(l,nbA*nbDeplacements,nbParams);
    st=std(l1);
    
    BB=repmat(st,nbA*nbDeplacements,1);
    BBB=l1./BB;
    mo=mean(BBB);
    moy = repmat(mo,nbA*nbDeplacements,1);
    BBB = BBB-moy;
    pcs = pca(l1);
    
    %           end
    figure
    hold on;
    %plot(newdat((1:9),1),newdat((1:9),2),'*b')
    %plot(newdat((10:18),1),newdat((10:18),2),'*r')
    %                plot(newdat((19:27),1),newdat((19:27),2),'*y')
    %                plot(newdat((28:36),1),newdat((28:36),2),'*g')
    
    %               plot(newdat((nbDeplacements+1:2*nbDeplacements+1,1),newdat((nbDeplacements+1:2*nbDeplacements+1),2),'*r')
    %                plot( newdat( (2*(nbDeplacements+1):3*nbDeplacements+2,1),newdat((1:nbDeplacements),2),'*b')
    %               plot(newdat((nbDeplacements+1:end),1),newdat((nbDeplacements+1:end),2),'*r')
    %               plot(newdat((1:nnbDeplacements),1),newdat((1:nbDeplacements),2),'*b')
    %               plot(newdat((nbDeplacements+1:2*nbDeplacements+1,1),newdat((nbDeplacements+1:2*nbDeplacements+1),2),'*r')
    %                plot( newdat( (2*(nbDeplacements+1):3*nbDeplacements+2,1),newdat((1:nbDeplacements),2),'*b')
    %               plot(newdat((nbDeplacements+1:end),1),newdat((nbDeplacements+1:end),2),'*r')
    hold off;
    %               JJ=l1(1:nbDeplacements,:);
    % st=std(JJ);
    %                BJ=repmat(st,nbDeplacements,1);
    %                BJJ=JJ./BJ;
    %                [m,n] = size(BJJ);
    %               r = min(m-1,n);     % max possible rank of x
    %avg = mean(BJJ);
    % centerx = (BJJ - mo(ones(m,1),:));
    
    for nbd=1:nbDeplacements*nbA
        C=BBB(nbd,:);
        C=C(:);
        
        tb(nbd,1)=sum(C.*pcs(:,1)); %#ok<AGROW>
        tb(nbd,2)=sum(C.*pcs(:,2)); %#ok<AGROW>
    end
    %plot(tb((1:nbDeplacements),1),tb((1:nbDeplacements),2),'*g')
    figure
    hold on;
    % plot(tb((1:9),1),newdat((1:9),2),'*b')
    %plot(tb((10:18),1),newdat((10:18),2),'*r')
    %                plot(tb((19:27),1),newdat((19:27),2),'*y')
    %                plot(tb((28:36),1),newdat((28:36),2),'*g')
    
end

%g=zeros(length(training);
for i=1:nbA
    for j=1:nbDeplacements
        g(j,i)=i; %#ok<AGROW>
    end
end
G=g(:);
g=G;
newdat=tb;

