% Segmentation par EQM
%
% Syntax
%   [Moyenne, EcartType, Ponder, Masque] = apprentissage(A, ...)
%
% Input Arguments
%   A : Classes d'apprentissage :tableau d'instances de type cl_texture
%
% Name-Value Pair Arguments
%   typeDistInterClasses : {1=moyenne} | 2=median | 3=somme
%                          moyenne : Distance inter-classes calculee par calcul de proba des moyennes
%                                       d'une classe par rapport aux autres
%                          median  : Distance inter-classes calculee par calcul de proba de la valeur mediane
%                                       d'une classe par rapport aux autres
%                          somme   : Distance inter-classes calculee par calcul de la somme des probas
%                                       d'une classe par rapport aux autres
%   typePonder  :  {1=Ponderation exponentielle} | 2=Ponderation par l'inverse de la distance
%   SeuilMasque : Seuil a appliquer a ponder pour obtenir le masque
%   coefMagique : Coefficient a estimer par essai-erreurs (1 par defaut)
%
% Output Arguments
%   [] : Auto-plot activation
%   Moyenne   :
%   EcartType :
%   Ponder    :
%
% Examples
%
% See also cl_texture cl_texture/segmentationEqm Authors
% Authors : JMA
% VERSION  : $Id: apprentissageEM.m,v 1.1 2002/11/25 16:54:08 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [newdat, pcs, variance,t2,g ,mo, st ] = apprentissageEM2(A, direction,nbCl, varargin)

% [varargin, typeDistInterClasses] = getPropertyValue(varargin, 'typeDistInterClasses', 1);
% [varargin, typePonder] = getPropertyValue(varargin, 'typePonder', 1);
% [varargin, SeuilMasque] = getPropertyValue(varargin, 'SeuilMasque', 0.1);
% [varargin, coefMagique] = getPropertyValue(varargin, 'coefMagique', 1);

% ---------------------------------------------------------------------
% On recupere le premier element de A uniquement pour obtenir le nombre
% de parametres, le nombre de deplacements et le nombre de directions

b = A(1);
nbParams = length(b.ListeParams);

nbDeplacements = length(b.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(b.Directions);
if nbDirections == 0
    return
end

% ----------------------------------------
% Recuperation des donnees d'apprentissage

nbA = length(A);



for iImage = 1:nbA
    b = A(iImage);
    nbParams = length(b.ListeParams);
    
    nbDeplacements = length(b.Deplacements);  % Nombre de deplacements
    if nbDeplacements == 0
        return
    end
    
    nbDirections = length(b.Directions);
    if nbDirections == 0
        return
    end
    
    nbImagettes = b.nbL*b.nbC;
    for iParam=1:nbParams
        for iDirection=1:nbDirections
            Moyenne(:,iDirection, iParam,iImage) = b.Params(:,iDirection,iParam,1,1)'; %#ok<AGROW>
        end
    end
    if nbImagettes == 1
        % Cas ou on l'apprentissage s'est fait de maniere globale sur
        % une image. On lit donc directement la valeur moyenne
        %         Algo = 1;
        for iParam=1:nbParams
            for iDirection=1:nbDirections
                Moyenne(:,iDirection, iParam,iImage) = b.Params(:,iDirection,iParam,1,1)';
            end
        end
    end
end

GM=zeros(nbDeplacements,nbDirections,nbA,nbParams);
% Tab=zeros(nbDeplacements,nbDirections,nbA,nbParams);
for iParam=1:nbParams
    for iDirection=1:nbDirections
        for iImage1 = 1:nbA
            for iDeplacement=1:nbDeplacements
                GM (iDeplacement,iDirection,iImage1,iParam)= Moyenne(iDeplacement,iDirection,iParam,iImage1);
            end
        end
    end
end
% DEP=(1:nbDeplacements);
% DEP=DEP';
% PCS=zeros(1:nbParams);

%for iDirection=1:nbDirections
iDirection=direction;

l=GM(:,iDirection,:,:);
% l(:);
l1=reshape(l,nbA*nbDeplacements,nbParams);
st=std(l1);
%
%                 BB=repmat(st,nbA*nbDeplacements,1);
%                 L2=l1./BB;

mo=mean(l1);
for i=1:nbA*nbDeplacements
    l2(i,:)=l1(i,:)-mo; %#ok<AGROW>
end
%               moy=repmat(mo,nbA*nbDeplacements,1);
%               BBB=L2-moy;
[pcs,newdat,variance,t2] = pca(l1);

for nbd=1:nbDeplacements*nbA
    C=l2(nbd,:);
    C=C(:);
    
    tb(nbd,1)=sum(C.*pcs(:,1)); %#ok<AGROW>
    tb(nbd,2)=sum(C.*pcs(:,2)); %#ok<AGROW>
end
% newda=tb;

nbIpC=nbA/nbCl;
dv=nbIpC*nbDeplacements;
for l=1:nbA*nbDeplacements
    g(l) = ceil(l/dv); %#ok<AGROW>
end
g=g';

