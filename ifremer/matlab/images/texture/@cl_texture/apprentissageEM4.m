% Segmentation par EQM
%
% Syntax
%   [Moyenne, EcartType, Ponder, Masque] = apprentissage(A, ...)
%
% Input Arguments
%   A : Classes d'apprentissage :tableau d'instances de type cl_texture
%
% Name-Value Pair Arguments
%   typeDistInterClasses : {1=moyenne} | 2=median | 3=somme
%                          moyenne : Distance inter-classes calculee par calcul de proba des moyennes
%                                       d'une classe par rapport aux autres
%                          median  : Distance inter-classes calculee par calcul de proba de la valeur mediane
%                                       d'une classe par rapport aux autres
%                          somme   : Distance inter-classes calculee par calcul de la somme des probas
%                                       d'une classe par rapport aux autres
%   typePonder  :  {1=Ponderation exponentielle} | 2=Ponderation par l'inverse de la distance
%   SeuilMasque : Seuil a appliquer a ponder pour obtenir le masque
%   coefMagique : Coefficient a estimer par essai-erreurs (1 par defaut)
%
% Output Arguments
%   [] : Auto-plot activation
%   Moyenne   :
%   EcartType :
%   Ponder    :
%
% Examples
%
% See also cl_texture cl_texture/segmentationEqm Authors
% Authors : JMA
% VERSION  : $Id: apprentissageEM.m,v 1.1 2002/11/25 16:54:08 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [newdat, pcs, variance,t2,g ,mo, st,l1 ] = apprentissageEM4(A, direction,nbCl, varargin)

% [varargin, typeDistInterClasses] = getPropertyValue(varargin, 'typeDistInterClasses', 1);
% [varargin, typePonder] = getPropertyValue(varargin, 'typePonder', 1);
% [varargin, SeuilMasque] = getPropertyValue(varargin, 'SeuilMasque', 0.1);
% [varargin, coefMagique] = getPropertyValue(varargin, 'coefMagique', 1);

% ---------------------------------------------------------------------
% On recupere le premier element de A uniquement pour obtenir le nombre
% de parametres, le nombre de deplacements et le nombre de directions

b = A(1);
nbParams = length(b.ListeParams);

nbDeplacements = length(b.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(b.Directions);
if nbDirections == 0
    return
end

% ----------------------------------------
% Recuperation des donnees d'apprentissage

nbA = length(A);



for iImage = 1:nbA
    b = A(iImage);
    nbParams = length(b.ListeParams);
    
    nbDeplacements = length(b.Deplacements);  % Nombre de deplacements
    if nbDeplacements == 0
        return
    end
    
    nbDirections = length(b.Directions);
    if nbDirections == 0
        return
    end
    
    b=A(iImage);
    data(iImage,:,:,:) = (b.Params(:,direction,:)); %#ok<AGROW>
end
l1=zeros(1,nbParams);
for iImage = 1:nbA
    l=squeeze(data(iImage,:,:));
    l1 = [l1; l]; %#ok<AGROW>
end
l1(1,:) = [];


st=std(l1);

% BB=repmat(st,nbA*nbDeplacements,1);
% L2=l1./BB;

mo = mean(l1);
%mo=mean(L2);
for i=1:nbA*nbDeplacements
    %  l2(i,:)=L2(i,:)-mo;
    l2(i,:)=l1(i,:)-mo; %#ok<AGROW>
end
%               moy=repmat(mo,nbA*nbDeplacements,1);
%               BBB=L2-moy;
%[pcs,newdat,variance,t2]=pca(l2);
[pcs,newdat,variance,t2] = pca(l1);

for nbd=1:nbDeplacements*nbA
    C=l2(nbd,:);
    C=C(:);
    
    tb(nbd,1)=sum(C.*pcs(:,1)); %#ok<AGROW>
    tb(nbd,2)=sum(C.*pcs(:,2)); %#ok<AGROW>
end

nbIpC=nbA/nbCl;
dv=nbIpC*nbDeplacements;
for l=1:nbA*nbDeplacements
    g(l)=ceil(l/dv); %#ok<AGROW>
end
