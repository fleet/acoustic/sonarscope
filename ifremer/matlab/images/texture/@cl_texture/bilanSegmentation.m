% Bilan de la segmentation
% Composition d'une mosaique d'image par assemblage
%
% Syntax
%   TauxErreur = bilanSegmentation(cl_texture([]), Segm, Cdeb, Cfin, Ldeb, Lfin, W)
% 
% Input Arguments 
%   this : instance creuse
%   Segm : Segmentation
%   Cdeb : Colonnes debut et fin ou sont placees
%   Cfin : les images dans l'image resultante
%   Ldeb : Lignes debut et fin ou sont placees
%   Lfin : les images dans l'image resultante
%   W    : Taille de la fenetre d'analyse
%    
% Output Arguments
%   TauxErreur :  Taux d'erreur pour les differentes segmentation
%
% Examples
%
% See also Authors
% Authors : JMA
% VERSION    : $Id: bilanSegmentation.m,v 1.1 2002/10/21 11:59:51 emenut Exp emenut $
%-------------------------------------------------------------------------------

function TauxErreur = bilanSegmentation(this, Segm, W, varargin) %#ok<INUSL>

if nargin == 7
    Cdeb = varargin{1};
    Cfin = varargin{2};
    Ldeb = varargin{3};
    Lfin = varargin{4};
else
    if ~isempty(Segm)
        X = 1:size(Segm.Image, 2);
        Y = 1:size(Segm.Image, 1);
        X = 1 + (X-1) * W(2);
        Y = 1 + (Y-1) * W(1);
        
        figure('NumberTitle', 'off','Name', 'Segmentation')
        imagesc(X, Y, Segm.Image); title(['From ' Segm.Titre]);
    end
    TauxErreur = [];
    return
end

SegmRef = NaN * zeros(size(Segm.Image));
for i=1:length(Cdeb)
    cdeb = 1 + floor(Cdeb(i)/W(2));
    cfin =     floor(Cfin(i)/W(2));
    ldeb = 1 + floor(Ldeb(i)/W(1));
    lfin =     floor(Lfin(i)/W(1));
    SegmRef(ldeb:lfin , cdeb:cfin) = i;
end
% sz = size(SegmRef);

if ~isempty(Segm)
    sub = find(~isnan(SegmRef));
    difference = abs(Segm.Image(sub) - SegmRef(sub));
    TauxErreur = 100 * sum(difference) / length(sub);
    
    figure('NumberTitle', 'off','Name', 'Segmentation')
    titre = sprintf('From %s - Taux erreur=%5.1f%c', Segm.Titre, TauxErreur, '%');
    X = 1:size(Segm.Image, 2);
    Y = 1:size(Segm.Image, 1);
    X = 1 + (X-1) * W(2);
    Y = 1 + (Y-1) * W(1);
    imagesc(X,Y,Segm.Image); title(titre); colorbar; pixval;
    
    for i=1:size(Segm.probas, 3)
        figure('NumberTitle', 'off','Name', ['Probas Label=' num2str(i)])
        titre = sprintf('From %s - Taux erreur=%5.1f%c', Segm.Titre, TauxErreur, '%');
        X = 1:size(Segm.Image, 2);
        Y = 1:size(Segm.Image, 1);
        X = 1 + (X-1) * W(2);
        Y = 1 + (Y-1) * W(1);
        imagesc(X, Y, Segm.probas(:,:,i)); title(titre); colorbar; pixval;
    end
end
