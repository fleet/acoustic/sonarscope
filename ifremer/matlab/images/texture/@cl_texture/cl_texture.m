% Constructeur de cl_texture (Analyse de texture d'une image par matrices de cooccurrence)
%
% Syntax
%   this = cl_texture(...)
% 
% Name-Value Pair Arguments
%   Label           : Description de l'image
%   Image           : Image de base
%   Deplacements    : Deplacements (1:10 par defaut)
%   Directions      : Orientations 1=0 deg, 2=45 deg,
%                     3=90 deg, 4=135 deg ([1 2 3 4] par defaut
%   NomParams       : Liste des parametres texturaux :
%                     {'contraste'; 'directivite';'entropie';'local_homog';'uniformite';'correlation';'homogeneite'};
%   ListeParams     : Indices des parametres texturaux
%   Params          : Parametres texturaux
%   FlagProny       : 1=Modelisation de Prony | {0=pas de modelisation de Prony}
%   ParamsReduits   : Periode et amplitude obtenus  partir de la modelisation de Prony
%   WindowAnalyse   : Taille d'analyse [0 0]=Fenetre entiere sinon largeur de la fenetre
%   WindowTypeShift : Type de decalage (si WindowAnalyse # [0 0]
%                     {1=decalage donne par WindowAnalyse} |
%                      2=decalage donne par le max du vecteur deplacement  | 
%                      3=decalage donne par l'utilisateur
%   WindowValShift  : Decalage donne par l'utilisateur si WindowTypeShift=3
%   ligDeb, ligFin, colDeb, colFin : Coordonnees des images de calcul
%                                   (depend de WindowAnalyse, WindowTypeShift et WindowValShift)
%   nbL, nbC : Nombre d'imagettes en lignes et en colonnes
%  
% Output Arguments 
%   this : instance de cl_texture
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   I1 = imread(nomFic);
%   [ls , Label1] = fileparts(nomFic);
% 
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   I2 = imread(nomFic);
%   [pathFic, Label2] = fileparts(nomFic);
% 
%   Par = 1:10;
%   Dir = 1;
%   W = [32 32];
% 
%   Style = cl_style('Color', 'b');
%   a(1) = cl_texture( 'Image', I1, 'Label', Label1, ...
%                   'Directions', Dir, 'ListeParams', Par, 'Style', Style);
%   Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
%   a(2) = cl_texture( 'Image', I1, 'Label', Label1, ...
%                   'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
%                   'WindowAnalyse', W);
% 
%   Style = cl_style('Color', 'r');
%   a(3) = cl_texture( 'Image', I2, 'Label', Label2, ...
%                   'Directions', Dir, 'ListeParams', Par, 'Style', Style);
%   Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
%   a(4) = cl_texture( 'Image', I2, 'Label', Label2, ...
%                   'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
%                   'WindowAnalyse', W);
%   imagesc(a);
%   plot(a);
%   plot(a, 'subPar', [1 2 5])
%   plot(a, 'subPar', 1:2, 'subDep', 1:10)
% 
%   histo(a, 'subPar', 1, 'subDep', 1:6)
%   plotNuages(a, 'subdep', [1 6], 'subParX', 1:8, 'subParY', 1:8, 'ellipsesSeules')
% 
%   P1 = get(a(1), 'Params')
%   P1=squeeze(P1(:,1,1))
%   figure; plot(P1); grid on;
%
% See also cli_cooc
% Authors : FM + JMA
% VERSION  : $Id: cl_texture.m,v 1.1 2002/10/21 11:59:51 emenut Exp emenut $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   29/05/2002 - FM - creation
% ----------------------------------------------------------------------------

function this = cl_texture(varargin)

% Maintenance Auto : try

    % --------------------------
    % Definition de la structure
    
    Struct.Label            = '';
    Struct.Image            = [];
    Struct.Deplacements     = 1:20;
    Struct.Directions       = 1:4;
Struct.Cmat             = [];
Struct.distCmatRef      = [];
    Struct.WindowAnalyse    = [0 0];
    Struct.WindowTypeShift  = 1;
    Struct.WindowValShift   = [1 1];
    Struct.ligDeb           = [];
    Struct.ligFin           = [];
    Struct.colDeb           = [];
    Struct.colFin           = [];
    Struct.nbL              = 1;
    Struct.nbC              = 1;
    Struct.NomParams        = { @coocContraste
                                @coocDirectivite
                                @coocEntropie
                                @coocLocalHomog
                                @coocUniformite
                                @coocCorrelation
                                @coocHomogeneite
                                @coocDirectiviteJMA
                                @meanImage
                                @stdImage
                                @coocDissimilarity
                                @coocSDC
                                @coocRegularity
                                @coocSigmaPx
                                @coocSigmaPy
                                @coocPeriodicity
                                @skewnessImage
                                @kurtosisImage
                                @coocSkewnessPx
                                @coocSkewnessPy
                                @coocKurtosisPx
                                @coocKurtosisPy};
                            
    Struct.ListeParams      = 1:length(Struct.NomParams);
    Struct.Params           = [];
    Struct.FlagProny        = 0;
    Struct.PronyReduits     = [];
    Struct.Style            = []; % L'initialisation a cl_style a cet endroit etait tres penalisant en temps de calcul
        
    % ----------------------
    % Creation de l'instance
    
    this = class(Struct, 'cl_texture') ;
    
    % -----------------------------------------------------------------------------------
    % On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe
    
    if (nargin == 1) && isempty(varargin{1})
        return
    end
    
    % ---------------------------------------------------------------------
    % Initialisation des valeurs transmises lors de l'appel au constructeur
    
    this.Style = cl_style;
    this = set(this, varargin{:});
    
% Maintenance Auto : catch
% Maintenance Auto :     err('cl_texture', 'cl_texture', 'pas de bol, bonhomme') ;
% Maintenance Auto : end
