% Affichage des images
%
% Syntax
%   imagesc(a, ...) 
%
% Input Arguments 
%   a : Une instance de cl_texture
%
% Name-Value Pair Arguments 
%   Parametres transmis a imagesc
%
% Examples
%   nomFic = getNomFicDatabase('texture01.png');
%   img = imread(nomFic);
%   a = cl_texture('Image', img)
%
%   imagesc(a)
%
% See also cl_texture cl_texture/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function imagesc(this, varargin)

% Maintenance Auto : try
    nbImages = length(this);
    for iImage = 1:nbImages
        b = this(iImage);
        
        if isempty(b.Image)
            continue;
        end
        
        figure('NumberTitle', 'off','Name', ['Image : ' b.Label])
        imagesc(b.Image, varargin{:}); colorbar; colormap(gray(256));
        title(b.Label)
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('cl_texture', 'imagesc', '');
% Maintenance Auto : end

