% Initialisation de l'instance (calcul des matrices de cooccurrence et des paaraametres texturaux)
%
% Syntax
%   this = init(this) 
%
% Input Arguments 
%   this : Une instance de cl_texture
%
% Output Arguments
%   this : L'instance initialisee
%
%
% See also cl_texture Authors
% Authors : FM
% VERSION  : $Id: init.m,v 1.11 2003/02/06 09:43:08 augustin Exp $
% ----------------------------------------------------------------------------

function this = init(this, Masque, flagWaitBar, flagCmat, CmatRef) %#ok<INUSL>

nbNiveaux = 256;

nbDeplacements = length(this.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(this.Directions);
if nbDirections == 0
    return
end

%% Calcul des deplacelments en x et y poue des deplacements donnes en unite pixel

for iDirection=1:nbDirections
    switch this.Directions(iDirection)
        case 1 % Deplacement horizontal
            Tx{iDirection} = this.Deplacements; %#ok
            Ty{iDirection} = zeros(1,nbDeplacements); %#ok
        case 3
            Tx{iDirection} = zeros(1,nbDeplacements); %#ok<AGROW>
            Ty{iDirection} = this.Deplacements; %#ok<AGROW>
        case 2
            [x, xEntiers] = calculPiquets(this.Deplacements);
            Ty{iDirection} =  x; %#ok<AGROW>
            TxEntiers{iDirection} = -xEntiers; %#ok
            TyEntiers{iDirection} =  xEntiers; %#ok
        case 4
            [x, xEntiers] = calculPiquets(this.Deplacements);
            Ty{iDirection} =  x; %#ok<AGROW>
            TxEntiers{iDirection} = xEntiers; %#ok<AGROW>
            TyEntiers{iDirection} = xEntiers; %#ok<AGROW>
    end
end

%% D�coupage de l'image en imagettes

[ligDeb, ligFin, colDeb, colFin, nbL, nbC] = decoupage(size(this.Image), this.WindowAnalyse, ...
    this.WindowTypeShift, this.WindowValShift, max(this.Deplacements));
this.ligDeb = ligDeb;
this.ligFin = ligFin;
this.colDeb = colDeb;
this.colFin = colFin;
this.nbL    = nbL;
this.nbC    = nbC;


%% Construction du tableau de reception des valeurs des parametres

nbParams = length(this.ListeParams);
this.Params = zeros(nbDeplacements, nbDirections, nbParams);

%     if isempty(Masque)
%         Masque = ones(nbDeplacements,nbDirections,nbParams);
%     end

%% Test si il y a besoin de calculer les moyenne et les ecart-types des
% imagettes

flagMeanImage     = 0;
flagStdImage      = 0;
flagSkewnessImage = 0;
flagKurtosisImage = 0;
for iParam=1:nbParams
    kParam = this.ListeParams(iParam);
    nomFonction = this.NomParams{kParam};
    switch func2str(nomFonction)
        case 'meanImage'
            flagMeanImage = 1;
        case 'stdImage'
            flagStdImage = 1;
        case 'skewnessImage'
            flagSkewnessImage = 1;
        case 'kurtosisImage'
            flagKurtosisImage = 1;
    end
end

%% Calcul des donnees utiles dans le calcul des parametres
% (On veut eviter de les recalculer a chaque fois)

Before = 0;
if Before
    nomFicDist = getNomFicDatabase('TextureDistances.mat');
    if isempty(nomFicDist)
        this(:) = [];
        return
    end
    
    DistDiag{1} = loadmat(nomFicDist, 'nomVar', 'DistDiag1');
    DistDiag{2} = loadmat(nomFicDist, 'nomVar', 'DistDiag2');
else
    DistDiag{1} = [];
    DistDiag{2} = ponderateurContraste(256);
end

%% Calcul des matrices de cooccurrence et des parametres texturaux

nbImagettes = this.nbL * this.nbC;

if (nbImagettes ~= 1) && ~flagWaitBar
    affichageWaitBar = 1;
else
    affichageWaitBar = 0;
end

if affichageWaitBar
    hw = create_waitbar(['Calcul des matrices et des parametres texturaux - ' this.Label ' ...'], 'N', nbImagettes);
    iImagette = 0;
end
for subLig=1:this.nbL
    for subCol = 1:this.nbC
        if affichageWaitBar
            iImagette = iImagette + 1;
            my_waitbar(iImagette, nbImagettes, hw)
        end

        Image = this.Image(ligDeb(subLig):ligFin(subLig),colDeb(subCol):colFin(subCol));
        %             Image = double(Image);
        %             Image = Image - floor(my_nanmean(Image(:))) + 128;

        if flagMeanImage
            meanImage = mean(double(Image(:)), 'omitnan');
        end
        if flagStdImage
            stdImage = std(double(Image(:)), 'omitnan');
        end
        if flagSkewnessImage
            skewnessImage = my_skewness(double(Image(:)));
        end
        if flagKurtosisImage
            kurtosisImage = my_kurtosis(double(Image(:)));
        end

        for iDirection=1:nbDirections
            switch this.Directions(iDirection)
                case {1 3}
                    for iDeplacement=1:nbDeplacements
                        % On regarde si il y a au moins 1 parametre qui doit etre calcule.
                        % Dans ce cas, on est contraint de calculer la matrice de cooccurrence
                        %                         if any(Masque(iDeplacement,iDirection,:))

                        Cmat  = coocMat(Image, ...
                            Tx{iDirection}(iDeplacement), ...
                            Ty{iDirection}(iDeplacement), ...
                            'nbNiveaux', nbNiveaux);

                        if flagCmat
                            CmatCentree = centreCmat(Cmat);
                            this.Cmat(:,:,iDeplacement,iDirection) = CmatCentree;
                        end
                        if ~isempty(CmatRef)
                            CmatCentree = centreCmat(Cmat);
                            for iCmatRef=1:length(CmatRef)
                                CmatDif = CmatRef(iCmatRef).Cmat(:,:,iDeplacement,iDirection) - CmatCentree;
                                this.distCmatRef(iCmatRef,iDeplacement,iDirection,subLig,subCol) = sum(CmatDif(:) .^2);
                            end
                        end

                        % Rotation de la matrice pour pouvoir optimiser
                        % le temps calcul des indices travaillant sur
                        % la deuxime diagonale

                        if ~isempty(intersect(this.ListeParams, 14:17))
                            C45 = imrotate(Cmat, 45, 'nearest', 'crop');
                            Cmat(:,:,2) = C45 / sum(sum(C45));
                        end

                        for iParam=1:nbParams
                            % On regarde si ce parametre qui doit etre calcule
                            %                                 if Masque(iDeplacement,iDirection,iParam)
                            kParam = this.ListeParams(iParam);
                            nomFonction = this.NomParams{kParam};
                            switch func2str(nomFonction)
                                case 'meanImage'
                                    this.Params(iDeplacement,iDirection,iParam,subLig,subCol) = meanImage;
                                case 'stdImage'
                                    this.Params(iDeplacement,iDirection,iParam,subLig,subCol) = stdImage;
                                case 'skewnessImage'
                                    this.Params(iDeplacement,iDirection,iParam,subLig,subCol) = skewnessImage;
                                case 'kurtosisImage'
                                    this.Params(iDeplacement,iDirection,iParam,subLig,subCol) = kurtosisImage;
                                otherwise
                                    this.Params(iDeplacement,iDirection,iParam,subLig,subCol) = ...
                                        feval(nomFonction, Cmat, DistDiag);
                            end
                            %                                 end
                        end
                        %                         end
                    end
                case {2 4}
                    ParamsEntiers = zeros(nbParams,length(TxEntiers{iDirection}));

                    % On calcule les deplacements entiers encadrant les
                    % deplacements reels
                    %                     [IParam, Idep] = find(squeeze(Masque(:,iDirection,:)));
                    Idep = 1:nbDeplacements;
                    [x2, xEntiers2] = calculPiquets(this.Deplacements(Idep));

                    for iDeplacement=1:length(TxEntiers{iDirection})
                        % On regarde si la matrice de cooccurrence doit
                        % etre calculee
                        flag = intersect(xEntiers2, TyEntiers{iDirection}(iDeplacement));
                        if flag
                            Cmat  = coocMat(Image, ...
                                TxEntiers{iDirection}(iDeplacement), ...
                                TyEntiers{iDirection}(iDeplacement), ...
                                'nbNiveaux', nbNiveaux);

                            if flagCmat
                                CmatCentree = centreCmat(Cmat);
                                this.Cmat(:,:,iDeplacement,iDirection) = CmatCentree;
                            end
                            if ~isempty(CmatRef)
                                CmatCentree = centreCmat(Cmat);
                                for iCmatRef=1:length(CmatRef)
                                    CmatDif = CmatRef(iCmatRef).Cmat(:,:,iDeplacement,iDirection) - CmatCentree;
                                    this.distCmatRef(iCmatRef,iDeplacement,iDirection,subLig,subCol) = sum(CmatDif(:) .^2);
                                end
                            end

                            % Rotation de la matrice pour pouvoir optimiser
                            % le temps calcul des indices travaillant sur
                            % la deuxime diagonale

                            if ~isempty(intersect(this.ListeParams, 14:17))
                                C45 = imrotate(Cmat, 45, 'nearest', 'crop');
                                Cmat(:,:,2) = C45 / sum(sum(C45));
                            end

                            for iParam=1:nbParams
                                % On regarde si ce parametre qui doit etre calcule
                                %                                 if Masque(iDeplacement,iDirection,iParam)
                                kParam = this.ListeParams(iParam);
                                nomFonction = this.NomParams{kParam};
                                switch func2str(nomFonction)
                                    case 'meanImage'
                                        ParamsEntiers(iParam, iDeplacement) = meanImage;
                                    case 'stdImage'
                                        ParamsEntiers(iParam, iDeplacement) = stdImage;
                                    case 'skewnessImage'
                                        ParamsEntiers(iParam, iDeplacement) = skewnessImage;
                                    case 'kurtosisImage'
                                        ParamsEntiers(iParam, iDeplacement) = kurtosisImage;
                                    otherwise
                                        ParamsEntiers(iParam, iDeplacement) = ...
                                            feval(this.NomParams{kParam}, Cmat, DistDiag);
                                end
                                %                                 end
                            end
                        end
                    end
                    for iParam=1:nbParams
                        %                         sub = find(Masque(:,iDirection,iParam));
                        sub = 1:nbDeplacements;
                        %                         if ~isempty(sub)
                        this.Params(sub,iDirection,iParam,subLig,subCol) = interp1(...
                            TyEntiers{iDirection}, ParamsEntiers(iParam,:), ...
                            Ty{iDirection}(sub), ...
                            'linear', 'extrap');
                        %                         end
                    end
            end
        end
    end
end
if affichageWaitBar
    my_close(hw)
end

%% Mod�lisation de Prony

flagProny = 0;
if flagProny
    if affichageWaitBar %#ok<UNRCH>
        hw = create_waitbar('Modelisation de Prony', 'N', nbImagettes);
        iImagette = 0;
    end
    for subLig = 1:this.nbL
        for subCol = 1:this.nbC
            if affichageWaitBar
                iImagette = iImagette + 1;
                my_waitbar(iImagette, nbImagettes, hw)
            end
            for iParam=1:nbParams
                for iDirection=1:nbDirections
                    x = this.Deplacements;
                    y = this.Params(:,iDirection,iParam,subLig,subCol);
                    [Params, Cst, correla] = optimProny(x(2:end), y(2:end));
                    this.PronyReduits(iDirection,iParam,:,subLig,subCol) = reducParam(x, Params, this.Deplacements(end));
                end
            end
        end
    end
    if affichageWaitBar
        my_close(hw)
    end
end




% ------------------------------------
function PReduit = reducParam(x, P, N)
A     = P(1,1);
alpha = P(1,2);
T     = P(1,3);

PReduit(1) = T * (x(2)-x(1));
PReduit(2) = A * exp(-N/(2*alpha));



% -------------------------------------------------------------------------
function [ligDeb, ligFin, colDeb, colFin, nbL, nbC] = decoupage(sz, WindowAnalyse, WindowTypeShift, WindowValShift, dmax)

nbRows = sz(1);
nbCol = sz(2);
if all(WindowAnalyse)

    % Travail sur des imagettes

    switch WindowTypeShift
        case 1  % decalage donne par WindowAnalyse
            ligPas = WindowAnalyse(1);
            colPas = WindowAnalyse(2);
        case 2  % decalage donne par le max du vecteur deplacement
            ligPas = dmax;
            colPas = dmax;
        case 3  % decalage donne par l'utilisateur
            ligPas = WindowValShift(1);
            colPas = WindowValShift(2);
    end

    ligDeb = 1:ligPas:nbRows;
    ligFin = ligDeb - 1 + WindowAnalyse(1);
    sub = find(ligFin > nbRows | ligDeb > nbRows-WindowAnalyse(1));
    if ~isempty(sub)
        ligFin(sub(1)) = nbRows;
        ligDeb(sub(1)) = nbRows - WindowAnalyse(1) + 1;
        if length(sub) >= 2
            ligFin(sub(2:end)) = [];
            ligDeb(sub(2:end)) = [];
        end
    end

    colDeb = 1:colPas:nbCol;
    colFin = colDeb - 1 + WindowAnalyse(2);
    sub = find(colFin > nbCol | colDeb > nbCol-WindowAnalyse(2));
    if ~isempty(sub)
        colFin(sub(1)) = nbCol;
        colDeb(sub(1)) = nbCol - WindowAnalyse(2) + 1;
        if length(sub) >= 2
            colFin(sub(2:end)) = [];
            colDeb(sub(2:end)) = [];
        end
    end


else

    % Travail sur toute l'image

    ligDeb = 1;
    ligFin = sz(1);
    colDeb = 1;
    colFin = sz(2);
end

nbL = length(ligDeb);
nbC = length(colDeb);








% --------------------------------------------------
function [x, xEntiers] = calculPiquets(Deplacements)

x = Deplacements /sqrt(2);
xEntiers = unique([floor(x) (floor(x)+1) ceil(x)]);
xEntiers(xEntiers == 0) = [];

% Verrue pour corriger le probl�me pour un deplacement de 1
if length(xEntiers) == 1
    xEntiers = [xEntiers xEntiers+1];
end



% -------------------------------------
function CmatCentree = centreCmat(Cmat)
CmatCentree = Cmat;
% figure; imagesc(CmatCentree); colorbar
% figure; surf(CmatCentree); shading flat

% CmatCentree = zeros(512);
%
% Py  = sum(Cmat);
% tab = 1:256;
% nuy = sum(tab .* Py);
% nuy = round(nuy);
%
% sub = (256 - nuy):(511 - nuy);
% CmatCentree(sub,sub) = Cmat;
%
% % figure; imagesc(CmatCentree); colorbar
