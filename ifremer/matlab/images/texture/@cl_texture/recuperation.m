function [valeurs ] = recuperation(A,direction, varargin)

% [varargin, typeDistInterClasses] = getPropertyValue(varargin, 'typeDistInterClasses', 1);
% [varargin, typePonder] = getPropertyValue(varargin, 'typePonder', 1);
% [varargin, SeuilMasque] = getPropertyValue(varargin, 'SeuilMasque', 0.1);
% [varargin, coefMagique] = getPropertyValue(varargin, 'coefMagique', 1);

% ---------------------------------------------------------------------
% On recupere le premier element de A uniquement pour obtenir le nombre
% de parametres, le nombre de deplacements et le nombre de directions

b = A(1);
nbParams = length(b.ListeParams);

nbDeplacements = length(b.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(b.Directions);
if nbDirections == 0
    return
end

% ----------------------------------------
% Recuperation des donnees d'apprentissage

nbA = length(A);



for iImage = 1:nbA
    b = A(iImage);
    nbParams = length(b.ListeParams);
    
    nbDeplacements = length(b.Deplacements);  % Nombre de deplacements
    if nbDeplacements == 0
        return
    end
    
    nbDirections = length(b.Directions);
    if nbDirections == 0
        return
    end
    
    nbImagettes = b.nbL*b.nbC;
    for iParam=1:nbParams
        for iDirection=1:nbDirections
            Moyenne(:,iDirection, iParam,iImage) = b.Params(:,iDirection,iParam,1,1)'; %#ok<AGROW>
        end
    end
    if nbImagettes == 1
        % Cas ou on l'apprentissage s'est fait de maniere globale sur
        % une image. On lit donc directement la valeur moyenne
        %         Algo = 1;
        for iParam=1:nbParams
            for iDirection=1:nbDirections
                Moyenne(:,iDirection, iParam,iImage) = b.Params(:,iDirection,iParam,1,1)';
            end
        end
    end
end

GM = zeros(nbDeplacements,nbDirections,nbA,nbParams);
% Tab=zeros(nbDeplacements,nbDirections,nbA,nbParams);
for iParam=1:nbParams
    for iDirection=1:nbDirections
        for iImage1 = 1:nbA
            for iDeplacement=1:nbDeplacements
                GM (iDeplacement,iDirection,iImage1,iParam)= Moyenne(iDeplacement,iDirection,iParam,iImage1);
            end
        end
    end
end
% DEP=(1:nbDeplacements);
% DEP = DEP';
% PCS = zeros(1:nbParams);

iDirection = direction;
l = GM(:,iDirection,:,:);
% l(:);
valeurs = reshape(l, nbA*nbDeplacements, nbParams);
