function [valeurs, nbL,nbC ] = recuperation2(A, direction, varargin) %#ok<INUSL>

% ---------------------------------------------------------------------
% On recupere le premier element de A uniquement pour obtenir le nombre
% de parametres, le nombre de deplacements et le nombre de directions
nbDeplacements = length(A.Deplacements);
nbDirections   = length(A.Directions);
nbParams       = length(A.ListeParams);
nbL            = A.nbL;
nbC            = A.nbC;

[varargin, subDep] = getPropertyValue(varargin, 'subDep', 1:nbDeplacements);
[varargin, subDir] = getPropertyValue(varargin, 'subDir', 1:nbDirections);
[varargin, subPar] = getPropertyValue(varargin, 'subPar', 1:nbParams); %#ok<ASGLU>


A.Deplacements = A.Deplacements(subDep);
A.Directions   = A.Directions(subDir);
A.ListeParams  = A.ListeParams(subPar);
A.Params      = A.Params(subDep, subDir, subPar, : , :);

% -----------------------------------
% Recuperation des donnes a segmenter

nbDeplacements = length(A.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(A.Directions);
if nbDirections == 0
    return
end

nbParams = length(A.ListeParams);
valeurs=zeros(1,nbParams*nbDirections);  %%%%%%%%%%%%%%%%%%modif
for i=1:nbL
    for j=1:nbC
        val=zeros(nbDeplacements,1);
        for k=1:nbDirections
            A.Params(:,k,:,i,j);
            valeurImagette=squeeze(A.Params(:,k,:,i,j));
            val = [val, valeurImagette'];  %#ok<AGROW> % ajout du transpose
        end
        val(:,1)=[];
        valeurs = [valeurs; val]; %#ok<AGROW>
    end
end
valeurs(1,:)=[];
