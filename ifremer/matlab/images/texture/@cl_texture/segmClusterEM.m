% Segmentation par EQM
%
% Syntax
%   S = segmentation(this, Moyenne, EcartType, Ponder, Masque)
%
% Input Arguments
%   A : Classes d'apprentissage
%
% Examples
%
% See also cl_texture cl_texture/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Segm = segmClusterEM(this, newdat, pcs,g ,mo,st,varargin)

nbDeplacements = length(this.Deplacements);
nbDirections   = length(this.Directions);
nbParams       = length(this.ListeParams);
nbL            = this.nbL;
nbC            = this.nbC;

[varargin, subDep] = getPropertyValue(varargin, 'subDep', 1:nbDeplacements);
[varargin, subDir] = getPropertyValue(varargin, 'subDir', 1:nbDirections);
[varargin, subPar] = getPropertyValue(varargin, 'subPar', 1:nbParams); %#ok<ASGLU>

this.Deplacements = this.Deplacements(subDep);
this.Directions   = this.Directions(subDir);
this.ListeParams  = this.ListeParams(subPar);
this.Params       = this.Params(subDep, subDir, subPar, : , :);

% -----------------------------------
% Recuperation des donnes a segmenter

nbDeplacements = length(this.Deplacements);
if nbDeplacements == 0
    return
end

nbDirections = length(this.Directions);
if nbDirections == 0
    return
end

nbParams = length(this.ListeParams);
g=g(:);

imagettes=this.Params;
for i=1:nbL
    for j=1:nbC
        d=imagettes(:,:,:,i,j);
        d(:);
        
        data=reshape(d,nbDeplacements,nbParams);
        %               st=std(data);
        tst=repmat(st,nbDeplacements,1);
        datar=data./tst;
        [m,n] = size(datar);
        %               r = min(m-1,n);     % max possible rank of x
        avg = mo;
        centerx = (datar - avg(ones(m,1),:));
        
        %
        %               mo=mean(datar);
        %               tmoy=repmat(mo,nbDeplacements,1);
        %               datarc=datar-tmoy;
        
        for nbd=1:nbDeplacements
            
            C=centerx(nbd,:);
            C=C(:);
            
            tb(i,j,nbd,1)=sum(C.*pcs(:,1)); %#ok<AGROW>
            tb(i,j,nbd,2)=sum(C.*pcs(:,2));     %#ok<AGROW>
            plot(tb(i,j,nbd,1),tb(i,j,nbd,2),'.')
        end
        p=(length(g)-1);
        G=zeros((size(g)*nbDeplacements));
        for id=1:p+1
            h = (id-1)*nbDeplacements;
            
            for jd=1:(nbDeplacements)
                G(h+(jd-1)+1)=g(id);
            end
        end
        GG=G(:,1);
        m=tb(i,j,:,:);
        n=squeeze(m);
        app=newdat(:,1:2);
        class=classify(n,app,GG);
        
        Segm.Image(i,j) = round(sum(class)/nbDeplacements);
    end
end
hold off;
