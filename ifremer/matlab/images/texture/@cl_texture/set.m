% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_texture
%
% Name-Value Pair Arguments
%  Cf. cl_texture
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = getNomFicDatabase('texture01.png');
%   img = imread(nomFic);
%   a = cl_texture('Image', img);
%
%
% See also cl_texture cl_texture/get Authors
% Authors : FM + JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

Init = 0;   % Flag indiquant si il faut repasser par la methode d'initialisation

% -----
% Label

[varargin, this.Label] = getPropertyValue(varargin, 'Label', this.Label);

% -----
% Style

[varargin, this.Style] = getPropertyValue(varargin, 'Style', this.Style);


% -----
% FlagProny

[varargin, this.FlagProny] = getPropertyValue(varargin, 'FlagProny', this.FlagProny);


% --------------
% Masque

[varargin, Masque] = getPropertyValue(varargin, 'Mask', []);

% --------------------------------------
% Possibilite de modification de l'image

[varargin, Image] = getPropertyValue(varargin, 'Image', []);
if ~isempty(Image)
    if ~isequal(Image, this.Image)
        this.Image = Image;
        Init = 1;
    end
end

% ------------
% Deplacements

[varargin, Deplacements] = getPropertyValue(varargin, 'Deplacements', []);
if ~isempty(Deplacements)
    if ~isequal(Deplacements, this.Deplacements)
        this.Deplacements = Deplacements;
        Init = 1;
    end
end

% ----------
% Directions

[varargin, Directions] = getPropertyValue(varargin, 'Directions', []);
if ~isempty(Directions)
    if ~isequal(Directions, this.Directions)
        this.Directions = Directions;
        Init = 1;
    end
end

% -----------
% ListeParams

[varargin, ListeParams] = getPropertyValue(varargin, 'ListeParams', []);
if ~isempty(ListeParams)
    if ~isequal(ListeParams, this.ListeParams)
        this.ListeParams = ListeParams;
        Init = 1;
    end
end

% -------------
% WindowAnalyse

[varargin, WindowAnalyse] = getPropertyValue(varargin, 'WindowAnalyse', []);
if ~isempty(WindowAnalyse)
    if ~isequal(WindowAnalyse, this.WindowAnalyse)
        this.WindowAnalyse = WindowAnalyse;
        Init = 1;
    end
end

% ---------------
% WindowTypeShift

[varargin, WindowTypeShift] = getPropertyValue(varargin, 'WindowTypeShift', []);
if ~isempty(WindowTypeShift)
    if ~isequal(WindowTypeShift, this.WindowTypeShift)
        this.WindowTypeShift = WindowTypeShift;
        Init = 1;
    end
end

% --------------
% WindowValShift

[varargin, WindowValShift] = getPropertyValue(varargin, 'WindowValShift', []);
if ~isempty(WindowValShift)
    if ~isequal(WindowValShift, this.WindowValShift)
        this.WindowValShift = WindowValShift;
        Init = 1;
    end
end

% ----------------------------------------------------------
% Drapeau qui indique si on ne veut pas afficher la waitbars

[varargin, flagWaitBar] = getFlag(varargin, 'noWaitbar');

% ----------------------
% Matrices de references

[varargin, CmatRef] = getPropertyValue(varargin, 'CmatRef', []);

% ---------------------------------------------------------
% Drapeau qui indique si on veut conserver les matrrices de
% cooccurrence

[varargin, flagCmat] = getFlag(varargin, 'Cmat'); %#ok<ASGLU>

% ---------------------------------------------------------
% Pre-nitialisation des champs en fonction du nom du modele

if Init
    this = init(this, Masque, flagWaitBar, flagCmat, CmatRef);
end

% ------
% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
