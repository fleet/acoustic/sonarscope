% Cr�ation de l'image de la figure 2.9 page 31 de la th�se d'Imen

I1 = imread('C:\Brodatz\D15.gif');
I2 = imread('C:\Brodatz\D53.gif');
I3 = imread('C:\Brodatz\D55.gif');

sub = 1:128;
I = [I1(sub,sub); I2(sub,sub); I3(sub,sub)];

I(I == 0) = 1;
figure; imagesc(I); colormap(gray(256));

imwrite(I, 'C:\Brodatz\Fig2-10.gif')
