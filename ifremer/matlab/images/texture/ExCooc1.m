% Ex calcul matrice de cooccurrence et parametres texturaux
%
% ExCooc1
%
% Examples
%   ExCooc1
%
% See also coocEx2 coocMat coocContraste Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

% -----------------------------------------------------
% Lecture d'un fichier d'exemple d'image sonar texturee

nomFic = getNomFicDatabase('textureSonar02.png');
img = imread(nomFic);

% ------------------------------------------
% Affichage de l'image et de son histogramme

figure; imagesc(img); colorbar; colormap(gray(256));
histo1D(double(img), 1:255)

% ------------------------------------
% Calcul d'une matrice de cooccurrence

coocMat(img, 1, 0);
coocMat(img, 10, 0);
% [Cmat, nbtr] = coocMat(img, 10, 0);

% ---------------------------------------------------------------
% Creation d'un montage photo de la matrice de cooccurrence pour des
% deplacements de 1 a 16

close all
coocMatMontage(img, 1:16, 0);

% ---------------------------------------------------------------
% Creation d'une animation de la matrice de cooccurrence pour des
% deplacements de 1 a 10

close all
coocMatMovie(img, 1:10, 0);

% ------------------------------------
% Somme sur les axes et les diagonales

coocMat(img, 1, 0);
[Cmat, nbtr] = coocMat(img, 1, 0); %#ok<ASGLU>
figure; plot(sum(Cmat), 'b'); grid on;
hold on;plot(sum(Cmat, 2),'r'); title('projections axes')
J = imrotate(Cmat,45);
figure; plot(sum(J), 'b'); grid on;
hold on;plot(sum(J, 2),'r'); title('projections diagonales');

coocMat(img, 5, 0);
[Cmat, nbtr] = coocMat(img, 5, 0); %#ok<ASGLU>
figure; plot(sum(Cmat), 'b'); grid on;
hold on;plot(sum(Cmat, 2),'r'); title('projections axes')
J = imrotate(Cmat,45);
J=J(:, 170:180);
figure; plot(170:180, sum(J), 'b'); grid on;
hold on;plot(sum(J, 2),'r'); title('projections diagonales');


% -----------------------------------------------
% Calcul et illustration des parametres texturaux

[Cmat, nbtr] = coocMat(img, 1, 0);
coocEntropie(Cmat)
coocHomogeneite(Cmat)
coocCorrelation(Cmat)
coocUniformite(Cmat)
coocDirectiviteJMA(Cmat)
coocDirectivite(Cmat)
coocContraste(Cmat)
coocLocalHomog(Cmat)
