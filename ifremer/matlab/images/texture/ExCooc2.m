% Classe cl_cooc : calcul des parametres texturaux
% 
% ExCooc2
%
% Examples 
%   ExCooc2
%
% See also ExCooc2 coocMat coocContraste Authors
% Authors : JMA
% VERSION  : $Id: ExCooc2.m,v 1.1 2002/12/20 10:53:05 augustin Exp $
%------------------------------------------------------------------------------------------

% -----------------------------------------------------
% Lecture d'un fichier d'exemple d'image sonar texturee

nomFic = getNomFicDatabase('textureSonar02.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonar01.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);


% ---------------------------------------------
% Utilisation de tous les parametres implicites

a = cl_cooc('Image', I1, 'Label', 'Tous les parametres');
imagesc(a);
plot(a);


% Illustration des fonctionnalites de la methode plot
plot(a, 'subDir', [1 3])
plot(a, 'subDir', 1)
plot(a, 'subDir', 1, 'subPar', [1 2 5])
plot(a, 'subDir', 1, 'subPar', 1:2, 'subDep', 1:10)
 

% -------------------
% Une seule direction

a = cl_cooc('Image', I1, 'Label', 'Une seule direction', 'Directions', 1);
plot(a);


% ------------------------------------
% Une seule direction, 10 deplacements

a = cl_cooc('Image', I1, 'Label', '1 dir, 10 deplacements', 'Directions', 1, 'Deplacements', 1:10);
plot(a);


% ------------------------------------------------------------
% Une seule direction, 10 deplacements, 2 parametres texturaux

a = cl_cooc('Image', I1, 'Label', '1 dir, 10 dep, 2 parametres', ...
            'Directions', 1, 'Deplacements', 1:10, 'ListeParams', 1:2);
plot(a);


% -------------------------------------------------------------------
% Calcul des paramatres texturaux sur l'image decomposee en imagettes


Par = 1:10;
Dir = 1;
W = [32 32];
a = cl_cooc( 'Image', I1, 'Label', 'Decoupe de l''image en imagettes', ...
                'Directions', Dir, 'ListeParams', Par, ...
                'WindowAnalyse', W);
plot(a);

% Changement de style
Style = cl_style('LineStyle', 'none', 'Marker', '+');
a = set(a, 'Style', Style);
plot(a)


% -----------------------------------------
% Comparaison detaillee de 2 textures sonar

Par = 1:10;
Dir = 1;
W = [32 32];

clear a
Style = cl_style('Color', 'b');
a(1) = cl_cooc( 'Image', I1, 'Label', Label1, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
a(2) = cl_cooc( 'Image', I1, 'Label', Label1, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'r');
a(3) = cl_cooc( 'Image', I2, 'Label', Label2, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
a(4) = cl_cooc( 'Image', I2, 'Label', Label2, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);
imagesc(a);
plot(a);
plot(a, 'subPar', [1 2 5])
plot(a, 'subPar', 1:2, 'subDep', 1:10)

histo(a, 'subPar', 1, 'subDep', 1:6)
plotNuages(a([2 4]), 'subdep', [1 6], 'subParX', 1:8, 'subParY', 1:8, 'ellipsesSeules')
plotNuages(a([2 4]), 'subdep', 1, 'subParX', 1, 'subParY', 2, 'ellipsesSeules')

for i=1:10
    histo(a, 'subPar', i, 'subDep', 1:4)
end


% ---------------------------
% Recuperation des parametres

b = a(2);
P1 = get(b, 'Params');
size(P1)
PDep1Dir1Par1Imagettes = squeeze(P1(1,1,1,:,:));

% Tentative de visualisation des parametres calcules sur les imagettes
% regroupes par deplacement en x et direction en y

nbL = get(b, 'nbL');
nbC = get(b, 'nbC');
nbDep = size(P1, 1);
nbDir = size(P1, 2);
nbPar = size(P1, 3);
NomParams = get(a(1), 'NomParams');
for iPar = 1:nbPar
    ImageParametre = zeros(nbDir*nbL, nbDep*nbC);
    for iDir = 1:nbDir
        ideb = 1 + (iDir-1) * nbL;
        subl = ideb:(ideb+nbL-1);
        for iDep = 1:nbDep
            ideb = 1 + (iDep-1) * nbC;
            subc = ideb:(ideb+nbC-1);
            ImageParametre(subl, subc) = squeeze(P1(iDep, iDir, iPar, :,:));
        end
    end
    figure; imagesc(ImageParametre); title(func2str(NomParams{iPar}));
end

% -----------------------------------
% Comparaison des textures de Brodatz

clear a
Couleur = 'rbkym';
for i=1:30
    Style = cl_style('Color', Couleur(1+mod(i-1, 5)));
    nomtexture = sprintf('texture%02d.png', i);
    nomFic = getNomFicDatabase(nomtexture);
    img = imread(nomFic);
    a(i) = cl_cooc( 'Image', img, 'Label', nomtexture, 'Style', Style, 'Directions', 1); %#ok<SAGROW>
end
plot(a);
imagesc(a);
for i=1:10
    plot(a, 'subPar', i);
end


