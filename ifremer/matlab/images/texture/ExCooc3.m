% Analyse de texture sur des images avec et sans speckle
% 
% ExCooc3
%
% Examples 
%   ExCooc3
%
% See also coocEx2 coocMat coocContraste Authors
% Authors : JMA
% VERSION  : $Id: ExCooc3.m,v 1.1 2002/12/20 11:05:59 augustin Exp $
%------------------------------------------------------------------------------------------

% -----------------------------
% Lecture de la donnee EdgeTech

addpath('/home1/doppler/augustin/MatlabToolboxIfremer/sonars')

nomFic = '/home/descartes/Reflacou/Exploitation/EdgeTech/Zone4_18_08_01/Imo/Rectiligne/DF1000_REFLACOU_Zone4_P18_0067.00.imo';
aFic = cl_car_sonar('nomFic', nomFic);
Image = get(aFic, 'grWrkImage_dB');

figure; imagesc(Image); colorbar; colormap(gray(256));

N = 256;
I1dB = Image(1:N, 1:N);             Label1 = 'Homogene';
I2dB = Image(600:600+N-1, 1:N);     Label2 = 'Rides';
figure; imagesc(I1dB); colorbar; colormap(gray(256));
figure; imagesc(I2dB); colorbar; colormap(gray(256));


% ----------------------------------------------
% Conditionnement d'une image en niveaux de gris

I1 = coocInitImage(I1dB, 'Imin', -60, 'Imax', 0);
figure; imagesc(I1); colorbar; colormap(gray(256));
histo1D(double(I1), 1:255)

I2 = coocInitImage(I2dB, 'Imin', -60, 'Imax', 0);
figure; imagesc(I2); colorbar; colormap(gray(256));
histo1D(double(I2), 1:255)


% ------------------------------------------------
% Calcul des parametres texturaux sur ces 2 images

Deplacements = 1:20;
Par = 1:10;
Dir = 1;
W = 32;

clear a
Style = cl_style('Color', 'b');
a(1) = cl_cooc( 'Image', I1, 'Label', 'Homogene brute', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r');
a(2) = cl_cooc( 'Image', I2, 'Label', 'Rides brutes', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
a(3) = cl_cooc( 'Image', I1, 'Label', 'Homogene brute', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', [W W]);
Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
a(4) = cl_cooc( 'Image', I2, 'Label', 'Rides brute', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', [W W]);


% -------------------
% Filtrage du speckle

R1 = reflec_dB2Enr(I1dB);
R2 = nlfilterSpeckle(R1, 1, [3 3], 'NoyauFiltreLee' );
R2dB = reflec_Enr2dB(R2);
I1Filtre = coocInitImage(R2dB, 'Imin', -60, 'Imax', 0);

R1 = reflec_dB2Enr(I2dB);
R2 = nlfilterSpeckle(R1, 1, [3 3], 'NoyauFiltreLee' );
R2dB = reflec_Enr2dB(R2);
I2Filtre = coocInitImage(R2dB, 'Imin', -60, 'Imax', 0);


figure;
subplot(2,2,1); imagesc(I1); colorbar; colormap(gray(256));
subplot(2,2,2); imagesc(I1Filtre); colorbar; colormap(gray(256));
subplot(2,2,3); imagesc(I2); colorbar; colormap(gray(256));
subplot(2,2,4); imagesc(I2Filtre); colorbar; colormap(gray(256));

% ----------------------------------
% Analyse de la texture des filtrees

clear b
Style = cl_style('Color', 'b');
b(1) = cl_cooc( 'Image', I1Filtre, 'Label', 'Homogene filtree', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r');
b(2) = cl_cooc( 'Image', I2Filtre, 'Label', 'Rides filtree', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
b(3) = cl_cooc( 'Image', I1Filtre, 'Label', 'Homogene filtree', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', [W W]);
Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
b(4) = cl_cooc( 'Image', I2Filtre, 'Label', 'Rides filtree', ...
                'Deplacements', Deplacements, 'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', [W W]);

plot(a)
plot(b)


% -----------------------------------------------------------
% Exploration du contraste pour des deplacements de 1 et de 6

Cmat = coocMat(I1, 1, 0);
coocContraste(Cmat)
Cmat = coocMat(I1Filtre, 1, 0);
coocContraste(Cmat)

Cmat = coocMat(I1, 6, 0);
coocContraste(Cmat)
Cmat = coocMat(I1Filtre, 6, 0);
coocContraste(Cmat)

Cmat = coocMat(I1, 12, 0);
coocContraste(Cmat)
Cmat = coocMat(I1Filtre, 12, 0);
coocContraste(Cmat)


Cmat = coocMat(I1, 1, 0);
coocDirectivite(Cmat)
Cmat = coocMat(I1Filtre, 1, 0);
coocDirectivite(Cmat)

Cmat = coocMat(I1, 6, 0);
coocDirectivite(Cmat)
Cmat = coocMat(I1Filtre, 6, 0);
coocDirectivite(Cmat)

Cmat = coocMat(I1, 12, 0);
coocDirectivite(Cmat)
Cmat = coocMat(I1Filtre, 12, 0);
coocDirectivite(Cmat)
