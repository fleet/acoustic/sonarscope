% Segmentation d'images texturees
% 
% coocEx4
%
% Examples 
%   ExCooc4
%
% See also coocEx1 coocEx2 coocEx3 coocMat coocContraste Authors
% Authors : JMA
% VERSION  : $Id: ExCooc4.m,v 1.1 2002/12/20 12:44:39 augustin Exp $
%------------------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   25/11/02 - JMA - creation
% ----------------------------------------------------------------------------



% -----------------------------------------------------
% Lecture d'un fichier d'exemple d'image sonar texturee

nomFic = getNomFicDatabase('textureSonar02.png');
I1Fic = imread(nomFic);
[ls , Label1] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonar01.png');
I2Fic = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);


% --------------------
% Parametres de calcul

Directions   = 1; %#ok
Directions   = [1 3];
Deplacements = 1; %#ok
Deplacements = 1:20;
ListeParams  = 1; %#ok
ListeParams  = [1 2 6]; %#ok
ListeParams  = [1 2 3 4 5 6 7 8];
W = 32; N = 256; %#ok
W = 25; N = 250;

% On reduit les images pour obtenir un multiple de W
I1 = I1Fic(1:N,1:N);
I2 = I2Fic(1:N,1:N);



% --------------------------------------------------------
% Segmentation par EQM, apprentissage sur images entieres

clear a TauxErreur
Style = cl_style('Color', 'b');
a(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Style', Style, ...
                'Directions', Directions, ...
                'ListeParams', ListeParams, ...
                'Deplacements', Deplacements);

Style = cl_style('Color', 'r');
a(2) = cl_cooc( 'Image', I2, 'Label', Label2, 'Style', Style, ...
                'Directions', Directions, ...
                'ListeParams', ListeParams, ...
                'Deplacements', Deplacements);
imagesc(a);
plot(a);

% Apprentissage
[Moyenne, EcartType] = apprentissage(a) %#ok

% Fusion des Images
[I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(I1,I2);

% Calcul des parametres sur l'image cible
Style = cl_style('Color', 'g', 'LineStyle', 'none', 'Marker', 'o');
b = cl_cooc( 'Image', I, 'Label', 'Melange', 'Style', Style, ...
                'Directions', Directions, ...
                'ListeParams', ListeParams, ...
                'Deplacements', Deplacements, ...
                'WindowAnalyse', [W W]);
imagesc(b);
plot(b);

% Segmentation
Segm = segmentation(b, Moyenne, EcartType);

% Bilan de la segmentation
TauxErreur = bilanSegmentation(cl_cooc([]), Segm, [W W], Cdeb, Cfin, Ldeb, Lfin) %#ok



% -------------------------------------------------------------
% Segmentation par proba ponderees, apprentissage sur imagettes

clear a b TauxErreur
Style = cl_style('Color', 'b');
a(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Style', Style, ...
                'Directions', Directions, ...
                'ListeParams', ListeParams, ...
                'Deplacements', Deplacements, ...
                'WindowAnalyse', [W W]);

Style = cl_style('Color', 'r');
a(2) = cl_cooc( 'Image', I2, 'Label', Label2, 'Style', Style, ...
                'Directions', Directions, ...
                'ListeParams', ListeParams, ...
                'Deplacements', Deplacements, ...
                'WindowAnalyse', [W W]);

imagesc(a);
plot(a);

% Changement de presentation 

Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
a(1)  = set(a(1), 'Style', Style) %#ok
Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
a(2)  = set(a(2), 'Style', Style) %#ok
plot(a);

% Apprentissage
apprentissage(a, 'coefMagique', 2000);
[Moyenne, EcartType, Ponder, Masque] = apprentissage(a, 'coefMagique', 2000);


% Fusion des Images
[I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(I1,I2);

% Calcul des parametres sur l'image cible
Style = cl_style('Color', 'g', 'LineStyle', 'none', 'Marker', 'o');
b = cl_cooc( 'Image', I, 'Label', 'Melange', 'Style', Style, ...
                'Directions', Directions, ...
                'ListeParams', ListeParams, ...
                'Deplacements', Deplacements, ...
                'WindowAnalyse', [W W]);

% Segmentation avec masque et ponderation
Segm = segmentation(b, Moyenne, EcartType, 'Ponder', Ponder, 'Mask', Masque);
TauxErreur = bilanSegmentation(cl_cooc([]), Segm, [W W], Cdeb, Cfin, Ldeb, Lfin) %#ok

% Segmentation sans masque et avec ponderation
Segm = segmentation(b, Moyenne, EcartType, 'Ponder', Ponder);
TauxErreur = bilanSegmentation(cl_cooc([]), Segm, [W W], Cdeb, Cfin, Ldeb, Lfin) %#ok

% Segmentation sans masque et sans ponderation
Segm = segmentation(b, Moyenne, EcartType);
TauxErreur = bilanSegmentation(cl_cooc([]), Segm, [W W], Cdeb, Cfin, Ldeb, Lfin) %#ok



% Segmentation sur un ensemble de parametres restreints

Segm = segmentation(b, Moyenne, EcartType, 'Ponder', Ponder, 'Mask', Masque, 'subPar', [3 7 8]) %#ok
TauxErreur = bilanSegmentation(cl_cooc([]), Segm, [W W])  %#ok

Segm = segmentation(b, Moyenne, EcartType, 'Ponder', Ponder, 'Mask', Masque, 'subPar', [3 7 8], 'subDir', 1) %#ok
TauxErreur = bilanSegmentation(cl_cooc([]), Segm, [W W])  %#ok

Segm = segmentation(b, Moyenne, EcartType, 'Ponder', Ponder, 'Mask', Masque, 'subPar', [3 7 8], 'subDir', 1, 'subDep', 1:2) %#ok
TauxErreur = bilanSegmentation(cl_cooc([]), Segm, [W W])  %#ok


