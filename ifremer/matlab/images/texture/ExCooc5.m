% Analyse du comportement des parametres texturaux
%
% ExCooc5
%
% Examples
%   ExCooc5
%
% See also coocEx2 coocMat coocContraste Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

% -----------------------------------------------------
% Lecture d'un fichier d'exemple d'image sonar texturee

nomFic = getNomFicDatabase('textureSonar02.png');
I1 = double(imread(nomFic));

bleu  = cl_style('Color', 'b');
rouge = cl_style('Color', 'r');

% -----------------------
% Comparaison de I et I/2

clear a
a(1) = cl_cooc('Image', I1,          'Label', 'I',   'Style', bleu);
a(2) = cl_cooc('Image', floor(I1/2), 'Label', 'I/2', 'Style', rouge);
imagesc(a);
plot(a);
% Conclusion : la correlation reste inchangee


% -----------------------
% Comparaison de I et I+constante

clear a
a(1) = cl_cooc('Image', floor(I1/2),    'Label', 'I/2',    'Style', bleu);
a(2) = cl_cooc('Image', floor(I1/2)+64, 'Label', 'I/2+64', 'Style', rouge);
imagesc(a);
plot(a);
% Conclusion : Tous les parametres sont egaux (suf la moyenne bien sur)


% -----------------------------
% Effet d'une derive sur l'image

q = stretchmat( linspace(0, 60, size(I1,2)), size(I1) );
clear a
a(1) = cl_cooc('Image', floor(I1/2),     'Label', 'I/2',       'Style', bleu);
a(2) = cl_cooc('Image', floor(I1/2 + q), 'Label', 'I/2+rampe', 'Style', rouge);
imagesc(a);
plot(a);
% Conclusion : Cerains parametres sont sensibles a la derive de niveau de gris
% Tres Sensibles        : coocEntropie coocUniformite coocCorrelation
%                         coocHomogeneite coocDirectiviteJMA meanImage stdImage
% Moyennement sensibles : coocContraste coocDirectivite coocLocalHomog


% -------------------------
% Effet d'un BS sur l'image

angles = linspace(-80, 80, size(I1,2));
BS = BSLurton( angles, [-5, 5, -30, 2.] );

q = stretchmat( BS+50, size(I1) );
clear a
a(1) = cl_cooc('Image', floor(I1/2),     'Label', 'I/2',       'Style', bleu);
a(2) = cl_cooc('Image', floor(I1/2 + q), 'Label', 'I/2+rampe', 'Style', rouge);
imagesc(a);
plot(a);
% Conclusion : Cerains parametres sont sensibles a la derive de niveau de gris
% Tres Sensibles        : coocEntropie coocUniformite coocCorrelation
%                         coocHomogeneite  meanImage stdImage
% Moyennement sensibles : coocContraste coocDirectivite coocLocalHomogc oocDirectiviteJMA
