% Example : Show the Gabor Wavelet
% Author : Chai Zhi  
% e-mail : zh_chai@yahoo.cn

nomFic = getNomFicDatabase('TexturesSonarMontage01.tif');
nomFic = getNomFicDatabase('TexturesSonarMontage02.tif');
nomFic = getNomFicDatabase('TexturesSonarMontage03.tif');
I = imread(nomFic);
figure; imagesc(I);

% Parameter Setting

Delt = 2*pi;

% Show the Gabor Wavelets
for iSigma=1:3 % 2:3
    for iFreq=1:4
        for iTheta=1:8
            [GW, str, x, y] = GaborRIF(iFreq, iTheta, iSigma);
            
            figure(3340+iSigma);
            subplot(4,8, (iFreq-1)*8+iTheta), imshow(real(GW) ,[]); axis on;% Show the real part of Gabor wavelets
            
            GG1 = filter2(GW, I);
            GG1 = real(GG1) .^ 2 + imag(GG1) .^ 2;
            
            figure(3350+iSigma);
            subplot(4,8, (iFreq-1)*8+iTheta), imagesc(GG1);
            title(sprintf('f%d t%d s%d', iFreq, iTheta, iSigma))
        end
%         figure(3330+iSigma);
%         subplot(1, 5, iFreq + 1); imshow(abs(GW),[]); axis on; % Show the magnitude of Gabor wavelets
    end
end
