% Create the Gabor Wavelet Filter
% Author : Chai Zhi  
% e-mail : zh_chai@yahoo.cn

function GW = GaborWavelet(iTheta, iFreq, iSigma)
Delt = pi * 2^iSigma;
f = sqrt(2);
Delt2 = Delt * Delt;
k = ((pi/2) / (f^iFreq)) * exp(1i * (iTheta * pi/8));% Wave Vector
kn2 = abs(k)^2;
R = ceil(Delt2 * 1.5);
GW = zeros(R);
Rs2 = R/2;
for m = (-Rs2+1):Rs2
    for n = (-Rs2+1):Rs2
        GW(m+Rs2,n+Rs2) = (kn2 / Delt2) ...
            * exp(-0.5 * kn2 * (m^2 + n^2) / Delt2) ...
            * (exp(1i*(real(k)*m + imag(k)*n)) - exp(-0.5 * Delt2));
    end
end

X1 = sum(abs(GW), 1);
X1 = cumsum(X1);
X1 = X1 / max(X1);
n1 = find(X1 > 0.01, 1, 'first');

X2 = sum(abs(GW), 2);
X2 = cumsum(X2);
X2 = X2 / max(X2);
n2 = find(X2 > 0.01, 1, 'first');

sub1 = (n1:R-n1);
sub2 = (n2:R-n2);
GW = GW(sub1,sub2);

