% Segmentation par EQM
%
% Syntax
%   S = segmentation(this, Moyenne, EcartType, Ponder, Masque) 
%
% Input Arguments 
%   A : Classes d'apprentissage
%
% Examples
%
% See also cl_cooc cl_cooc/set Authors
% Authors : JMA
% VERSION  : $Id: segmEM.m,v 1.1 2002/11/25 16:54:08 augustin Exp augustin $
% ----------------------------------------------------------------------------

function Resultats= calculDist(tab, newdat, pcs,g ,mo,st,niap,nbcl,nbDir,nbDeplacements,nbPar,nbL,nbC,varargin) %#ok<INUSL>

%       for i=1:nbL
%           for j=1:nbC
%               point=i*nbC+j;
%               data=tab((1+(point-1)*nbDeplacements):point*nbDeplacements,:);


moy = repmat(mo,nbDeplacements*nbL*nbC,1);
centerx = tab - moy;

for nbd=1:nbDeplacements*nbL*nbC
    C=centerx(nbd,:);
    C=C(:);

    tb(nbd,1)=sum(C.*pcs(:,1)); %#ok<AGROW>
    tb(nbd,2)=sum(C.*pcs(:,2)); %#ok<AGROW>
end
figure
hold on
for i=1:nbDeplacements*nbL*nbC
    plot(tb(i,1),tb(i,2),'*r');
end
figure;hold on
index=0;
for i=1:nbDeplacements:nbDeplacements*nbL*nbC
    index=index+1;
    mes = tb(i:i+nbDeplacements-1,:);
    for k=1:nbDeplacements
        plot(mes(k,1),tb(i,2),'*r');
    end
    m = mean(mes);
    plot(m(1),m(2),'Pb');

    for j=1:3*nbDeplacements
        apprent=newdat(j,1:2);
        for k=1:nbDeplacements
            dist(k,j)=((mes(k,1)-apprent(1))^2+(mes(k,2)-apprent(2))^2)^0.5; %#ok
        end

    end
    min1=min(min(dist));
    for j=1:3*nbDeplacements
        apprent=newdat(j+3*nbDeplacements ,1:2);
        for k=1:nbDeplacements
            dist(k,j)=((mes(k,1)-apprent(1))^2+(mes(k,2)-apprent(2))^2)^0.5; 
        end

    end
    min2 = min(min(dist));
    Resultats(index,1) = min1; %#ok<AGROW>
    Resultats(index,2) = min2; %#ok<AGROW>
end
hold off
