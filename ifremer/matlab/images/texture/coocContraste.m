% Calcul du parametre textural "contraste"
% Chaque terme de la matrice de cooccurrence est pondere par sa distance quadratique a la diagonale.
% On obtient un indice correspondant a la notion usuelle du contraste, il est eleve quand 
% les termes eloignes de la diagonale sont eleves, cad qd on passe svt d'un pixel tres clair 
% a un pixel tres fonce ou inversement.
%
% Syntax
%   CON = coocContraste(Cmat, ...)
% 
% Input Arguments 
%   Cmat     : Matrice de co-occurrence.
%
% Input Arguments 
%   DistDiag2 : Matrice des distances quadratiques par rapport a la diagonale
%               (Ce parametre est calcule a l'interieur de la fonction. 
%                La classe cl_cooc le calcule une seule fois et le passe en argument
%                pour gagner en performances)
%
% Output Arguments 
%   []  : Auto-plot activation
%   CON : contraste
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   CON = coocContraste(Cmat)
%   coocContraste(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocContraste.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function  CON = coocContraste(Cmat, varargin)

switch nargin
    case 0
        return
    case 1
        nomFicDist = getNomFicDatabase('TextureDistances.mat');
        DistDiag2 = loadmat(nomFicDist, 'nomVar', 'DistDiag2');
    case 2
        DistDiag = varargin{1};
        DistDiag2 = DistDiag{2};
end

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end

CON = sum(sum(Cmat .* DistDiag2)) / 65025; % 65025 = 255^2;

%% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Contraste : CON = sum(Cmat * DistDiag2)');
    subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,3,2); imagesc(DistDiag2); axis equal; axis tight; title('DistDiag2'); colorbar;
    subplot(1,3,3); imagesc(Cmat .* DistDiag2); axis equal; axis tight; title('Cmat * DistDiag2'); colorbar;
    str1 = 'Le contraste est la somme de Cmat .* DistDiag2';
    str2 = 'Contraste is computed as is : Cmat .* DistDiag2';
    fprintf(1,'%s\n', Lang(str1,str2));
end
