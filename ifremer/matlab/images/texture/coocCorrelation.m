% Calcul du parametre textural "correlation"
% Cet attribut rend compte de la correlation entre les lignes et les colonnes
% de la GLCM, c.a.d entre le niveau de gris du pixel d'origine et celui d'arrivee 
% de la translation.
%
% Syntax
%   CORR = coocCorrelation(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   CORR : Correlation
% 
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   CORR = coocCorrelation(Cmat)
%   coocCorrelation(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocCorrelation.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function CORR = coocCorrelation(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end

% -----------------------
% Calcul des distributions

Px  = sum(Cmat, 2);
Py  = sum(Cmat);
tab = 1:256;

% ---------------------------------------------------
% Calcul de la moyenne ponderee des lignes de la GLCM

nux = sum(tab' .* Px);

% -----------------------------------------------------
% Calcul de la moyenne ponderee des colonnes de la GLCM

nuy = sum(tab .* Py);

% -------------------
% variance des lignes

varx = sum(((tab - nux) .^2 )' .* Px);

% ---------------------
% variance des colonnes

vary = sum(((tab - nuy) .^2 ) .* Py);

% ------------------------
% Calcul de la correlation

X = ((tab - nux)' * (tab - nuy)) / (sqrt(varx) * sqrt(vary));
CORR = sum(X(:) .* Cmat(:));

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Correlation : CORR = sum(Masque * Cmat)');
    subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,3,2); imagesc(X); axis equal; axis tight; title('Mask'); colorbar;
    subplot(1,3,3); imagesc(X .* Cmat); axis equal; axis tight; title('Masque * Cmat'); colorbar;
    disp('La correlation est la somme de Masque .* Cmat')
end
