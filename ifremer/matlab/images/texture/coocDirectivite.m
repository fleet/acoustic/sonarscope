% Calcul du parametre textural "directivite"
% La directivite est importante si il y a des pixels de meme niveau de gris
% separes par la translation (Tx,Ty)
%
% Syntax
%   direc = coocDirectivite(Cmat);
% 
% Input Arguments 
%   Cmat   : Matrice de co-occurrence.
%
% Output Arguments 
%   []    : Auto-plot activation
%   direc : directivite
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   direc = coocDirectivite(Cmat)
%   coocDirectivite(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocDirectivite.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%--------------------------------------------------------------------------

function  direc = coocDirectivite(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end
direc = sum(diag(Cmat));

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Directivite : direc = sum(Diag1)');
    subplot(1,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    CLim = get(gca, 'CLim');
    subplot(1,2,2); imagesc(diag(Cmat), CLim); title('Diag1'); colorbar;
    disp('La directivite est la somme de Diag1')
end
