% Calcul du parametre textural "directiviteJMA"
% la directiviteJMA est importante si il y a des pixels de meme niveau de gris
% separes par la translation (Tx,Ty)
%
% Syntax
%   direc = coocDirectiviteJMA(Cmat);
% 
% Input Arguments 
%   Cmat   : Matrice de co-occurrence.
%
% Output Arguments 
%   []    : Auto-plot activation
%   direc : directiviteJMA
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   direc = coocDirectiviteJMA(Cmat)
%   coocDirectiviteJMA(Cmat)
%
% See also coocMat  cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA
% VERSION    : $Id: coocDirectiviteJMA.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%--------------------------------------------------------------------------

function  direc = coocDirectiviteJMA(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end
N = 4;
B = spdiags(Cmat, -N:N);
direc = sum(B(:));

%% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'DirectiviteJMA : direc = sum(DiagN)');
    subplot(1,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    CLim = get(gca, 'CLim');
    subplot(1,2,2); imagesc(B, CLim); title('DiagN'); colorbar;
    disp('La directiviteJMA est la somme de DiagN')
end
