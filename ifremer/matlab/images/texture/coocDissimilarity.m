% Calcul du parametre textural "dissimilarity"
% Chaque terme de la matrice de cooccurrence est pondere par sa distance a la diagonale.
% On obtient un indice correspondant a la notion approchee du contraste, il est eleve quand 
% les termes eloignes de la diagonale sont eleves, cad qd on passe svt d'un pixel tres clair 
% a un pixel tres fonce ou inversement.
%
% Syntax
%   Dis = coocDissimilarity(Cmat, ...)
% 
% Input Arguments 
%   Cmat     : Matrice de co-occurrence.
%
% Input Arguments 
%   DistDiag1 : Matrice des distances par rapport a la diagonale
%               (Ce parametre est calcule a l'interieur de la fonction. 
%                La classe cl_cooc le calcule une seule fois et le passe en argument
%                pour gagner en performances)
%
% Output Arguments 
%   []  : Auto-plot activation
%   Dis : contraste
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   Dis = coocDissimilarity(Cmat)
%   coocDissimilarity(Cmat)
%
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocDissimilarity.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function Dis = coocDissimilarity(Cmat, varargin)

switch nargin
    case 0
        return
    case 1
        nomFicDist = getNomFicDatabase('TextureDistances.mat');
        DistDiag1 = loadmat(nomFicDist, 'nomVar', 'DistDiag1');
    case 2
        DistDiag1 = varargin{1};
        DistDiag1 = DistDiag1{1};
end

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end

Dis = sum(sum(Cmat .* DistDiag1)) / 65025; % 65025 = 255^2;

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Contraste : Dis = sum(Cmat * DistDiag1)');
    subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,3,2); imagesc(DistDiag1); axis equal; axis tight; title('DistDiag1'); colorbar;
    subplot(1,3,3); imagesc(Cmat .* DistDiag1); axis equal; axis tight; title('Cmat * DistDiag1'); colorbar;
    disp('La dissimilarite est la somme de Cmat .* DistDiag1')
end
