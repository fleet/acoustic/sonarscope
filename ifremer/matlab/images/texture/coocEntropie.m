% Calcul du parametre textural "entropie"
% Le parametre Entropie caracterise l'organisation generale du nuage de points.
% Elle est minimale pour des textures uniformes
% l'entropie est faible si on a souvent le meme couple de pixels, forte si chaque couple
% est peu represente. Elle fournit un indicateur du desordre que peut presenter la structure.
%
% Syntax
%   ENT = coocEntropie(Cmat)
% 
% Input Arguments
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []  : Auto-plot activation
%   ENT : Entropie
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   ENT = coocEntropie(Cmat)
%   coocEntropie(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocEntropie.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function ENT = coocEntropie(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,2);
end
C = Cmat(Cmat ~= 0);
x = (C .* log(C)) ;
ENT = -sum(sum(x))./ log(2);

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Entropie : ENT = -sum(Cmat .* log(Cmat))./ log(2)');
    Cmat(Cmat == 0) = NaN;
    X = Cmat .* log(Cmat);
    subplot(2,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(2,2,2); imagesc(X); axis equal; axis tight; title('Cmat .* log(Cmat)'); colorbar;
    subplot(2,2,3); imagesc(log10(Cmat)); axis equal; axis tight; title('log10(Cmat)'); colorbar;
    subplot(2,2,4); imagesc(abs(log10(X))); axis equal; axis tight; title('log10(Cmat .* log(Cmat))'); colorbar;
    disp('L''entropie est la somme de Cmat .* log(Cmat)')
end
