% Calcul du parametre textural "correlation"
% Cet attribut rend compte de la correlation entre les lignes et les colonnes
% de la GLCM, c.a.d entre le niveau de gris du pixel d'origine et celui d'arrivee 
% de la translation.
%
% Syntax
%   CORR = coocEnveloppe(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   CORR : Correlation
% 
% Examples
%   I = [1 2 3; 1 3 4; 1 3 5];
%   Cmat = coocMat(I, 1, 0);
%   CORR = p_correlation(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocEnveloppe.m,v 1.1 2002/11/25 17:42:54 augustin Exp $
%-------------------------------------------------------------------------------


function ENV = coocEnveloppe(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end

% -------------------------------------------------------
% Calcul des moyennes et des variances associes a la GLCM

% -----------------------
% Calcul des distributions

Px  = sum(Cmat, 2);
%     Py  = sum(Cmat);
tab = 1:256;
% figure
plot(Px,'r');
[phat, pci] = gamfit(Px); %#ok<ASGLU>
p = gamrnd(phat(1),phat(2),1,0);
plot(p,'g');
%      p=poisspdf(tab,LAMBDAHAT);

%     % ---------------------------------------------------
%     % Calcul de la moyenne ponderee des lignes de la GLCM
%
nux = sum(tab' .* Px); %#ok<NASGU>
%
%     % -----------------------------------------------------
%     % Calcul de la moyenne ponderee des colonnes de la GLCM
%
%     nuy = sum(tab .* Py);
%
%     % -------------------
%     % variance des lignes
%
%     varx = my_nansum(((tab - nux) .^2 )' .* Px);
%
%     % ---------------------
%     % variance des colonnes
%
%     vary = my_nansum(((tab - nuy) .^2 ) .* Py);
%
%     % ------------------------
%     % Calcul de la correlation
%
%     X = (tab - nux)' * (tab - nuy);
%     CORR = sum(sum(X .* Cmat));
%     CORR = abs(CORR) / (sqrt(varx) * sqrt(vary));
ENV=0;
