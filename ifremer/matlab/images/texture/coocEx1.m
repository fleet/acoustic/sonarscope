% Exemple de calcul de la matrice de cooccurrence
% 
% ExTexture1
%
% Examples 
%   ExTexture1
%
% See also ExTexture2 Authors
% Authors : JMA
% VERSION  : $Id: coocEx1.m,v 1.1 2002/11/25 17:42:54 augustin Exp $
%------------------------------------------------------------------------------------------

% -----------------------------------------------------
% Lecture d'un fichier d'exemple d'image sonar texturee

nomFic = getNomFicDatabase('textureSonar02.png');
img = imread(nomFic);
figure; imagesc(img); colorbar; colormap(gray(256));
histo1D(double(img), 256)

% ---------------------------------------------------
% Conditionnement de l'image pour entrer dans cl_cooc

I = coocInitImage(img);
figure; imagesc(I); colorbar; colormap(gray(256));
histo1D(double(I), 256)

% ---------------------------------------------------------------------------
% Calcul d'une instance de classe cl-cooc avec tous les parametres implicites

a = cl_cooc( 'Image', I, 'Label', 'textureSonar01');
imagesc(a)
plot(a)


Style = cl_style;
a(1) = cl_cooc( 'Image', img, 'Label', 'textureSonar01', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
