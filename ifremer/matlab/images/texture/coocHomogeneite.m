% Calcul du parametre textural "Homogeneite"
% Valeur d'autant plus elevee que l'on retrouve souvent le meme couple de pixels,
% ce qui est le cas quand le niveau de gris est uniforme ou quand il y a
% periodicite dans le sens de la translation
%
% Syntax
%   homog = coocHomogeneite(Cmat);
%
% Input Arguments 
%   Cmat   : Matrice de co-occurrence.
%
% Output Arguments 
%   []    : Auto-plot activation
%   homog : homogeneite
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   homog = coocHomogeneite(Cmat)
%   coocHomogeneite(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocHomogeneite.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%--------------------------------------------------------------------------

function homog = coocHomogeneite(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end
homog = sum(Cmat(:) .^ 2);

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Homogeeite : homog = sum(Cmat ^ 2)');
    Cmat(Cmat == 0) = NaN;
    X = Cmat .^ 2;
    subplot(2,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(2,2,2); imagesc(X); axis equal; axis tight; title('Cmat \^ 2'); colorbar;
    subplot(2,2,3); imagesc(log10(Cmat)); axis equal; axis tight; title('log10(Cmat)'); colorbar;
    subplot(2,2,4); imagesc(log10(X)); axis equal; axis tight; title('log10(Cmat \^ 2)'); colorbar;
    disp('L''homogeneite est la somme de Cmat .^ 2')
end
