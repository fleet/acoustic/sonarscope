% Preparation d'une image pour calcul d'une matrice de cooccurrence
% Cette fonction quantifie l'image en valeurs entieres comprise entre
% 1 et Ng par une loi lineaire. Les non-valeurs de J sont codes en 0.
% Cette fonction n'est pas utile pour les fichiers de texture deja
% conditionnees (fichiers de la base de donnee SonarScopeData/public)
%
% Syntax
%   J = coocInitImage(I, ...)
%
% Input Arguments
%   I : Image
%
% Name-Value Pair Arguments
%   Ng     : Nombre de niveaux de gris (255 par defaut)
%   Imin   : valeur qui sera ramenee a 1 (min de I par defaut)
%   Imax   : valeur qui sera ramenee a Ng (max de I par defaut)
%   ValNan : Valeur dans I indiquant les non-valeurs (NaN par defaut)
%
% OUPUT PARAMETERS :
%   J : Image en niveaux de gris (uint8). les 0 representent les non-valeurs
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   I = imread(nomFic);
%   figure; imagesc(I); colorbar; colormap(gray(256))
%   histo(double(I), 1:255)
%
%   J = coocInitImage(I);
%   figure; imagesc(J); colorbar; colormap(gray(256))
%   histo(double(J), 1:255)
%
%   J = coocInitImage(I, 'Ng', 32);
%   figure; imagesc(J); colorbar; colormap(gray(256))
%   histo(double(J), 1:255)
%
%   J = coocInitImage(I, 'Ng', 8);
%   figure; imagesc(J); colorbar; colormap(gray(256))
%   histo(double(J), 1:255)
%
%   J = coocInitImage(I, 'Imax', 130);
%   histo(double(J), 1:255)
%
% See also cooc_mat Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function J = coocInitImage(I, varargin)

[varargin, Ng]     = getPropertyValue(varargin, 'Ng', 255);
[varargin, ValNan] = getPropertyValue(varargin, 'ValNan', NaN);
[varargin, Imin]   = getPropertyValue(varargin, 'Imin', []);
[varargin, Imax]   = getPropertyValue(varargin, 'Imax', []); %#ok<ASGLU>

if ~isa(I, 'double')
    I = double(I);
end

if isempty(Imin)
    Imin = min(I(:));
end

if isempty(Imax)
    Imax = max(I(:));
end

if isnan(ValNan)
    sub = isnan(I);
else
    sub = I == ValNan;
end

I(sub) = NaN;

J = (I - Imin) / (Imax - Imin);
J = uint8(1 + floor(J * Ng));
