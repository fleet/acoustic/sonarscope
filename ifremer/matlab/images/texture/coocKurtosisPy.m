% Calcul du parametre textural "regularite"
% Cet attribut rend compte de ???.
%
% Syntax
%   kurtosisPy = kurtosisPy(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   kurtosisPy : Correlation
% 
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   kurtosisPy = kurtosisPy(Cmat)
%   kurtosisPy(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: kurtosisPy.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function kurtosisPy = kurtosisPy(Cmat, varargin) %#ok
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

% -----------------------------------------------
% Recuperation de la matrice tournee de 45 degres

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,2);
else
    C45 = imrotate(Cmat, 45, 'nearest', 'crop');
    Cmat = C45 / sum(sum(C45));
end

% -----------------------
% Calcul des distributions

Py  = sum(Cmat, 2)';
tab = 1:256;


% ---------------------------------------------------
% Calcul de la moyenne ponderee des lignes de la GLCM

nuy = sum(tab .* Py);

% -------------------
% variance des lignes

vary = sum(((tab - nuy) .^ 2) .* Py);
sigmaPy = sqrt(vary);

% -------------------
% skewness des lignes

kurtosisPy = sum(((tab - nuy) .^ 4) .* Py);
kurtosisPy = kurtosisPy / (vary .^ 2);

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'kurtosisPy');
    titre = sprintf('PX (kurtosisPy = %f)', kurtosisPy);
    subplot(1,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat  45 deg'); colorbar;
    subplot(1,2,2); plot(tab, Py); title(titre); grid on;
    hold on; plot(tab, normpdf(tab,256-nuy,sigmaPy), 'r'); hold off;
    fprintf('La valeur du sigma est %f', kurtosisPy)
end
