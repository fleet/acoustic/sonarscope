% Calcul du parametre textural "homogeneite locale"
% On l'appele aussi moment inverse (IDM)
% Ce parametre traduit l'existence de plages uniformes de texture
%
% Syntax
%   localHomo = coocLocalHomog(Cmat, ...)
% 
% Input Arguments 
%   Cmat     : Matrice de co-occurrence.
%
% Input Arguments 
%   DistDiag2 : Matrice des distances quadratiques par rapport a la diagonale
%               (Ce parametre est calcule a l'interieur de la fonction. 
%                La classe cl_cooc le calcule une seule fois et le passe en argument
%                pour gagner en performances)
%
% Output Arguments 
%   []        : Auto-plot activation
%   localHomo : Homogeneite locale
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   localHomo = coocLocalHomog(Cmat)
%   coocLocalHomog(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocLocalHomog.m,v 1.3 2003/01/07 14:48:46 augustin Exp $
%-------------------------------------------------------------------------------

function localHomo = coocLocalHomog(Cmat, varargin)

switch nargin
    case 0
        return
    case 1
        nomFicDist = getNomFicDatabase('TextureDistances.mat');
        DistDiag2 = loadmat(nomFicDist, 'nomVar', 'DistDiag2');
    case 2
        DistDiag = varargin{1};
        DistDiag2 = DistDiag{2};
end

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end
localHomo = sum(sum(Cmat ./ (1 + DistDiag2)));

%% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'LocalHomog : localHomo = sum(Cmat / (1 + DistDiag2))');
    subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,3,2); imagesc(1 ./ (1 + DistDiag2)); axis equal; axis tight; title('1 / (1 + DistDiag2)'); colorbar;
    subplot(1,3,3); imagesc(Cmat ./ (1 + DistDiag2)); axis equal; axis tight; title('Cmat / (1 + DistDiag2)'); colorbar;
    str1 = 'L''homogeneite locale est la somme de Cmat ./ (1 + DistDiag2)';
    str2 = 'Homogeneity is computed as is : Cmat ./ (1 + DistDiag2)';
    fprintf(1,'%s\n', Lang(str1,str2));
end
