% Calcul du parametre textural inspire de "homogeneite locale"
% Ce parametre traduit l'existence de plages uniformes de texture
%
% Syntax
%  localHomo1  = coocLocalHomog1(Cmat, ...)
% 
% Input Arguments 
%   Cmat     : Matrice de co-occurrence.
%
% Input Arguments 
%   DistDiag1 : Matrice des distances par rapport a la diagonale
%               (Ce parametre est calcule a l'interieur de la fonction. 
%                La classe cl_cooc le calcule une seule fois et le passe en argument
%                pour gagner en performances)
%
% Output Arguments 
%   []         : Auto-plot activation
%   localHomo1 : Homogeneite locale bdouillee
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   localHomo1 = coocLocalHomog1(Cmat)
%   coocLocalHomog1(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocLocalHomog1.m,v 1.3 2003/01/07 14:48:46 augustin Exp $
%-------------------------------------------------------------------------------

function localHomo1 = coocLocalHomog1(Cmat, varargin)

switch nargin
    case 0
        return
    case 1
        nomFicDist = getNomFicDatabase('TextureDistances.mat');
        DistDiag1 = loadmat(nomFicDist, 'nomVar', 'DistDiag1');
    case 2
        DistDiag1 = varargin{1};
end

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end
localHomo1 = sum(sum(Cmat ./ (1 + DistDiag1)));

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'LocalHomog : localHomo1 = sum(Cmat / (1 + DistDiag1))');
    subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,3,2); imagesc(1 ./ (1 + DistDiag1)); axis equal; axis tight; title('1 / (1 + DistDiag1)'); colorbar;
    subplot(1,3,3); imagesc(Cmat ./ (1 + DistDiag1)); axis equal; axis tight; title('Cmat / (1 + DistDiag1)'); colorbar;
    disp('L''homogeneite locale de premier ordre est la somme de Cmat ./ (1 + DistDiag1)')
end
