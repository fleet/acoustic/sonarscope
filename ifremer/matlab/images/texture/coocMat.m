% Calcul d'une matrice de co-occurrence
%
% Syntax
%   [Cmat, nbtr] = coocMat(Image, Tx, Ty, ...)
%
% Input Arguments
%   Image : Image a analyser (les 0 sont consideres comme des NaN)
%   Tx    : Translation(s) horizontale
%   Ty    : Translation(s) verticale
%
% Name-Value Pair Arguments
%   nbNiveaux : Nombre de niveaux de gris (256 par defaut)
%
% Name-only Arguments
%   Log    : Flag pour afficher la matrice en prenant le log des valeurs
%            (utile uniquement en "Auto-plot"
%   Filtre : Flag pour calculer la matrice filtree
%
% Output Arguments
%   []          : Auto-plot activation
%   Cmat        : Matrice de co-occurrence.
%   nbtr        : Nombre total de transitions
%
% Remarks : La valeur 0 de Image est considérée comme "NaN".
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%
%   figure; imagesc(img); colorbar; colormap(gray(256));
%   histo1D(img, 1:255)
%
%   coocMat(img, 1, 0);
%   coocMat(img(1:32,1:32), 1, 0);
%   [Cmat, nbtr] = coocMat(img, 1, 0);
%   coocMat(floor(double(img)/4), 1, 0, 'nbNiveaux', 64);
%
% See also coocInitImage coocMatMovie cl_cooc Authors
% Authors : FM + JMA + EM
%-------------------------------------------------------------------------------

function varargout = coocMat(Image, Tx, Ty, varargin)

[varargin, nbNiveaux] = getPropertyValue(varargin, 'nbNiveaux', 256);
[varargin, Sigma]     = getPropertyValue(varargin, 'Sigma', []);

[varargin, Log]    = getFlag(varargin, 'Log');
[varargin, Filtre] = getFlag(varargin, 'Filtre');

% -----------------------------------------------------
% On controle si tous les arguments ont ete interpretes

if ~isempty(varargin)
    my_warndlg(['images:texture:coocMat L''un des arguments n''a ete interprete ', num2str(varargin{1})], 0);
end

% ------------------------------------------
% Test de conformite de la taille de l'image

if ~ismatrix(Image)
    my_warndlg('images:texture:coocMat L''image doit etre de dimension 2 ...', 0);
    return
end

% ----------------------------------------------------------------
% on initialise la matrice de co-occurrence. C'est une matrice N*N

Cmat = zeros(nbNiveaux, 'single');

% ------------------------------------------------
% On peut meme utiliser des translations negatives

if (Tx >= 0) && (Ty >= 0)                    % Tx =0 && Ty >0 => dep vert
    I1 = Image(floor(1:end-Ty), floor(1:end-Tx));
    I2 = Image(floor(1+Ty:end), floor(1+Tx:end));
elseif (Tx < 0) && (Ty >= 0)
    I1 = Image(floor(1:end-Ty), floor(end:-1:1-Tx));
    I2 = Image(floor(1+Ty:end), floor(end+Tx:-1:1));
elseif (Tx >= 0) && (Ty < 0)
    I1 = Image(floor(end:-1:1-Ty), floor(1:end-Tx));
    I2 = Image(floor(end+Ty:-1:1), floor(1+Tx:end));
else
    I1 = Image(floor(end:-1:1-Ty), floor(end:-1:1-Tx));
    I2 = Image(floor(end+Ty:-1:1), floor(end+Tx:-1:1));
end

I1 = I1(:);
I2 = I2(:);

sub = (I1~=0) & (I2~=0) & ~isnan(I1) & ~isnan(I2);
I1 = I1(sub);
I2 = I2(sub);
nbtr = length(I1);
if nbtr == 0
    varargout{1} = Cmat;
    varargout{2} = nbtr;
    return
end
if nbtr < min(16^2 / 2, numel(Image)/2)
    varargout{1} = Cmat;
    varargout{2} = nbtr;
    return
end

increment = 1 / nbtr;
NbPix = length(I1);
for i=1:NbPix
    x = I1(i);
    y = I2(i);
    Cmat(x,y) = Cmat(x,y) + increment;
end

% Filtrage de la matrice de co-occurrence pour estimer la densite de
% probabilite jointe

if Filtre
    if isempty(Sigma)
        S2 = std(double(I1));
        Sigma = max(0.1, 0.5*log2(227850 / NbPix) * sqrt(S2/14.35));
    end
    
    largeurFiltre = 1 + 2 * floor(2*Sigma);
    h = fspecial('gaussian', largeurFiltre, Sigma);
    Cmat = imfilter(Cmat, h);
end

% -----------------
% Par ici la sortie

if nargout == 0
    figure
    
    if ~Log
        titre = sprintf('%s. Dep : [%d %d]', Lang('Matrice de cooccurence','Co-occurence matrix'), Tx, Ty);
        imagesc(Cmat(:,:,1)); colorbar; title(titre);
        axis equal; axis tight; axis xy;
    else
        Cmat(Cmat == 0) = NaN;
        titre = sprintf('%s (log10). Dep : [%d %d]', Lang('Matrice de cooccurence','Co-occurence matrix'), Tx, Ty);
        pcolor(log10(Cmat(:,:,1))); colorbar; title(titre);
        shading flat; axis xy;
    end
else
    varargout{1} = Cmat;
    varargout{2} = nbtr;
end
