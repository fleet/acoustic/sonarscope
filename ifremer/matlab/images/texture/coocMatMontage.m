% Calcul d'une matrice de co-occurrence
%
% Syntax
%   coocMatMontage(Image, Tx, Ty)
% 
% Input Arguments 
%   Image : Image a analyser (les 0 sont consideres comme des NaN)
%   Tx    : Translation(s) horizontale
%   Ty    : Translation(s) verticale
%
% Remarks : Tx et Ty peuvent etre des tableaux de translations differentes,
%           dans ce cas, Cmat est un tableau de cellules
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   figure; imagesc(img); colorbar; pixval on; colormap(gray(256));
%
%   coocMatMontage(img, 1:16, 0);
%
% See also coocMat coocInitImage cl_cooc Authors
% Authors : FM + JMA + EM
%-------------------------------------------------------------------------------

function coocMatMontage(Image, Tx, Ty)

if length(Tx) == 1
    Tx = ones(size(Ty)) * Tx;
end
if length(Ty) == 1
    Ty = ones(size(Tx)) * Ty;
end

Tx = Tx(:);
Ty = Ty(:);

N = length(Tx);
hw = create_waitbar('Processing', 'N', N);
for i=1:N
    my_waitbar(i, N, hw)
    Mat = coocMat(Image, Tx(i), Ty(i));
    MatBrute(:,:,i) = Mat(:,:,1); %#ok<AGROW>
%     if size(Mat, 3) == 2
%         MatFiltree(:,:,i) = Mat(:,:,2);
%     end
end
my_close(hw, 'MsgEnd')


% -----------------
% Par ici la sortie

figure;
NImages = size(MatBrute, 3);
N = ceil(sqrt(NImages));
M = ceil(NImages / N);
for i=1:NImages
    subplot(M,N,i);
    imagesc(MatBrute(:,:,i)); axis equal; axis tight;
    titre = sprintf('Dep : [%d %d]', Tx(i), Ty(i));
    title(titre)
end
