% Calcul d'une matrice de co-occurrence
%
% Syntax
%   [FilmMat, FilmMatFiltre] = coocMatMovie(Image, Tx, Ty)
% 
% Input Arguments 
%   Image : Image a analyser (les 0 sont consideres comme des NaN)
%   Tx    : Translation(s) horizontale
%   Ty    : Translation(s) verticale
%
% Output Arguments 
%   []            : Auto-plot activation
%   FilmMat       : Film de la matrice de co-occurrence.
%   FilmMatFiltre : Film de la matrice de co-occurrence filtree.
%
% Remarks : Tx et Ty peuvent etre des tableaux de translations differentes,
%           dans ce cas, Cmat est un tableau de cellules
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   figure; imagesc(img); colorbar; pixval on; colormap(gray(256));
%
%   coocMatMovie(img, 1:10, 0);
%   [FilmMat, FilmMatFiltre] = coocMatMovie(img, 1:10, 0);
%
%   my_implay(FilmMatFiltre); % , 'Nb', 10, 'Freq', 2 );
%
%   coocMatMovie(img(1:32,1:32), 1:10, 0);
%   [FilmMat, FilmMatFiltre] = coocMatMovie(img, 1:10, 0);
%
% See also coocMat coocInitImage cl_cooc Authors
% Authors : FM + JMA + EM
%-------------------------------------------------------------------------------

function varargout = coocMatMovie(Image, Tx, Ty)

if length(Tx) == 1
    Tx = ones(size(Ty)) * Tx;
end
if length(Ty) == 1
    Ty = ones(size(Tx)) * Ty;
end

Tx = Tx(:);
Ty = Ty(:);

N = length(Tx);
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    Mat = coocMat(Image, Tx(k), Ty(k), 'Filtre');
    MatBrute(:,:,k)   = Mat(:,:,1); %#ok<AGROW>
    if size(Mat, 3) == 2
        MatFiltree(:,:,k) = Mat(:,:,2); %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')

%% Creation des films

FilmMat = images2movie(MatBrute);
if size(Mat, 3) == 2
    FilmMatFiltre = images2movie(MatFiltree);
end

%% Par ici la sortie

if nargout == 0
    my_implay(FilmMat); % , 'Nb', 10, 'Freq', 2 );
    if size(Mat, 3) == 2
        my_implay(FilmMatFiltre); % , 'Nb', 10, 'Freq', 2 );
    end
else
    varargout{1} = FilmMat;
    if size(Mat, 3) == 2
        varargout{2} = FilmMatFiltre;
    end
end
