% Calcul d'une matrice de co-occurrence, variante de coocMat pour raison
% d'efficacité temps calcul
%
% Syntax
%   [Cmat, nbtr] = coocMatNbNiveaux(Image, Tx, Ty, nbNiveaux)
%
% Input Arguments
%   Image     : Image a analyser (les 0 sont consideres comme des NaN)
%   Tx        : Translation(s) horizontale
%   Ty        : Translation(s) verticale
%   nbNiveaux : Nombre de niveaux de gris (256 par defaut)
%
% Output Arguments
%   []          : Auto-plot activation
%   Cmat        : Matrice de co-occurrence.
%   nbtr        : Nombre total de transitions
%
% Remarks : La valeur 0 de Image est considérée comme "NaN".
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%
%   figure; imagesc(img); colorbar; colormap(gray(256));
%   histo1D(img, 1:255)
%
%   [Cmat, nbtr] = coocMatNbNiveaux(img, 1, 0, 64);
%   figure; imagesc(Cmat); colorbar
%
% See also coocMat coocInitImage coocMatMovie cl_cooc Authors
% Authors : FM + JMA
%-------------------------------------------------------------------------------

function [Cmat, nbtr] = coocMatNbNiveaux(Image, Tx, Ty, nbNiveaux)

% ----------------------------------------------------------------
% on initialise la matrice de co-occurrence. C'est une matrice N*N

Cmat = zeros(nbNiveaux, 'single');

% ------------------------------------------------
% On peut meme utiliser des translations negatives

if (Tx >= 0) && (Ty >= 0)                    % Tx =0 && Ty >0 => dep vert
    I1 = Image(floor(1:end-Ty), floor(1:end-Tx));
    I2 = Image(floor(1+Ty:end), floor(1+Tx:end));
elseif (Tx < 0) && (Ty >= 0)
    I1 = Image(floor(1:end-Ty), floor(end:-1:1-Tx));
    I2 = Image(floor(1+Ty:end), floor(end+Tx:-1:1));
elseif (Tx >= 0) && (Ty < 0)
    I1 = Image(floor(end:-1:1-Ty), floor(1:end-Tx));
    I2 = Image(floor(end+Ty:-1:1), floor(1+Tx:end));
else
    I1 = Image(floor(end:-1:1-Ty), floor(end:-1:1-Tx));
    I2 = Image(floor(end+Ty:-1:1), floor(end+Tx:-1:1));
end

I1 = I1(:);
I2 = I2(:);

sub = (I1~=0) & (I2~=0) & ~isnan(I1) & ~isnan(I2);
I1 = I1(sub);
I2 = I2(sub);
nbtr = length(I1);
if nbtr == 0
    return
end
if nbtr < min(16^2 / 2, numel(Image)/2)
    return
end

increment = 1 / nbtr;
NbPix = length(I1);
for i=1:NbPix
    x = I1(i);
    y = I2(i);
    Cmat(x,y) = Cmat(x,y) + increment;
end

