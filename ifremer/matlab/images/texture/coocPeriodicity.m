% Calcul du parametre textural "regularite"
% Cet attribut rend compte de ???.
%
% Syntax
%   CON2 = coocPeriodicity(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   CON2 : Correlation
% 
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   CON2 = coocPeriodicity(Cmat)
%   coocPeriodicity(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocPeriodicity.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function CON2 = coocPeriodicity(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

% -----------------------------------------------
% Recuperation de la matrice tournee de 45 degres

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,2);
end

% -----------------------
% Calcul des distributions

Px  = sum(Cmat, 2);
Py  = sum(Cmat);
tab = 1:256;


% ---------------------------------------------------
% Calcul de la moyenne ponderee des lignes de la GLCM

nux = sum(tab' .* Px);

% -----------------------------------------------------
% Calcul de la moyenne ponderee des colonnes de la GLCM

nuy = sum(tab .* Py);

% -------------------
% variance des lignes

varx = sum(((tab - nux) .^2 )' .* Px);

% ---------------------
% variance des colonnes

vary = sum(((tab - nuy) .^2 ) .* Py);


CON2 = sqrt(varx / vary);

% --------------------
% Auto-plot activation

%     if nargout == 0
%         figure('NumberTitle', 'off','Name', 'Correlation : CON2 = sum(Masque * Cmat)');
%         subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
%         subplot(1,3,2); imagesc(D); axis equal; axis tight; title('Mask'); colorbar;
%         subplot(1,3,3); imagesc(D .* Cmat); axis equal; axis tight; title('Masque * Cmat'); colorbar;
%         disp('La contraste V2 est la somme de Masque .* Cmat')
%     end
