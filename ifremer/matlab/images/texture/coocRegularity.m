% Calcul du parametre textural "regularite"
% Cet attribut rend compte de la regularite d'une texture.
%
% Syntax
%   CON2 = coocRegularity(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   CON2 : Correlation
% 
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   CON2 = coocRegularity(Cmat)
%   coocRegularity(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocRegularity.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function CON2 = coocRegularity(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

% -----------------------------------------------
% Recuperation de la matrice tournee de 45 degres

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,2);
else
    C45 = imrotate(Cmat, 45, 'nearest', 'crop');
    Cmat = C45 / sum(sum(C45));
end

% -----------------------
% Calcul des distributions

Px  = sum(Cmat);
tab = 1:256;


% -----------------------------------------------------
% Calcul de la moyenne ponderee des colonnes de la GLCM

nux = sum(tab .* Px);

% ---------------------
% variance des colonnes

varx = sum(((tab - nux) .^2 ) .* Px);


% ------------------------
% Calcul de la correlation

D = repmat(abs(tab-nux)/sqrt(varx), 256,1);
D = 1 ./ (1 + 5*D);

% ------------------------
% Calcul de la correlation

%     CON2 = sum(sum(Cmat .* D)) ;%%%%%%%/ sum(sum(D)); % 65025 = 255^2;
CON2 = sum(sum(Cmat .* D)) / sum(sum(D)); % 65025 = 255^2;

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Correlation : CON2 = sum(Masque * Cmat)');
    subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,3,2); imagesc(D); axis equal; axis tight; title('Mask'); colorbar;
    subplot(1,3,3); imagesc(D .* Cmat); axis equal; axis tight; title('Masque * Cmat'); colorbar;
    disp('La contraste V2 est la somme de Masque .* Cmat')
end
