% Calcul du parametre textural "????"
% Cet attribut rend compte de la variance de l'ecart-type
% des colonnes de matrice de cooccurrence.
%
% Syntax
%   SDC = coocSDC(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   SDC : Correlation
% 
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   SDC = coocSDC(Cmat)
%   coocSDC(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocSDC.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function SDC = coocSDC(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end

% -----------------------
% Calcul des distributions

sigmay = std(Cmat);
SDC = var(sigmay,1);

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Correlation : SDC = sum(Masque * Cmat)');
    subplot(1,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,2,2); plot(sigmay); grid on; title('std(Cmat)');
    disp('Le SDC est la variance du signal "std(Cmat)"')
end
