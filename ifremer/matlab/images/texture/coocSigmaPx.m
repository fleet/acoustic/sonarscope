% Calcul du parametre textural "regularite"
% Cet attribut rend compte de ???.
%
% Syntax
%   sigmaPx = coocSigmaPx(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   sigmaPx : Correlation
% 
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   sigmaPx = coocSigmaPx(Cmat)
%   coocSigmaPx(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: coocSigmaPx.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function sigmaPx = coocSigmaPx(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

% -----------------------------------------------
% Recuperation de la matrice tournee de 45 degres

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,2);
else
    C45 = imrotate(Cmat, 45, 'nearest', 'crop');
    Cmat = C45 / sum(sum(C45));
end

% -----------------------
% Calcul des distributions

Px  = sum(Cmat);
tab = 1:256;


% -----------------------------------------------------
% Calcul de la moyenne ponderee des colonnes de la GLCM

nux = sum(tab .* Px);

% ---------------------
% variance des colonnes

varx = sum(((tab - nux) .^2 ) .* Px);
sigmaPx = sqrt(varx);


% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'coocSigmaPx');
    titre = sprintf('Px (sigmaPx = %f)', sigmaPx);
    subplot(1,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,2,2); plot(tab, Px); title(titre); grid on;
    hold on; plot(tab, normpdf(tab,nux,sigmaPx), 'r'); hold off;
    fprintf('La valeur du sigma est %f', sigmaPx)
end
