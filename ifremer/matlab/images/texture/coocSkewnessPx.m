% Calcul du parametre textural "regularite"
% Cet attribut rend compte de ???.
%
% Syntax
%   skewnessPx = skewnessPx(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   skewnessPx : Correlation
% 
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   skewnessPx = skewnessPx(Cmat)
%   skewnessPx(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA + EM
% VERSION    : $Id: skewnessPx.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function skewnessPx = skewnessPx(Cmat, varargin) %#ok
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

% -----------------------------------------------
% Recuperation de la matrice tournee de 45 degres

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,2);
else
    C45 = imrotate(Cmat, 45, 'nearest', 'crop');
    Cmat = C45 / sum(sum(C45));
end

% -----------------------
% Calcul des distributions

Px  = sum(Cmat);
tab = 1:256;


% -----------------------------------------------------
% Calcul de la moyenne ponderee des colonnes de la GLCM

nux = sum(tab .* Px);

% ---------------------
% variance des colonnes

varx = sum(((tab - nux) .^2 ) .* Px);
sigmaPx = sqrt(varx);

% -------------------
% skewness des lignes

skewnessPx = sum(((tab - nux) .^ 3) .* Px);
skewnessPx = skewnessPx / (sigmaPx .^ 3);


% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'skewnessPx');
    titre = sprintf('Px (skewnessPx = %f)', skewnessPx);
    subplot(1,2,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    subplot(1,2,2); plot(tab, Px); title(titre); grid on;
    hold on; plot(tab, normpdf(tab,nux,sigmaPx), 'r'); hold off;
    fprintf('La valeur du sigma est %f', skewnessPx)
end
