% Calcul du parametre textural "uniformite"
% Valeur d'autant plus importante qu'un seul niveau de gris apparait souvent
% dans la direction de la translation
%
% Syntax
%   unif = coocUniformite(Cmat)
% 
% Input Arguments 
%   Cmat : Matrice de co-occurrence.
%
% Output Arguments 
%   []   : Auto-plot activation
%   unif : uniformite
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   Cmat = coocMat(img, 1, 0);
%
%   unif = coocUniformite(Cmat)
%   coocUniformite(Cmat)
%
% See also coocMat cl_cooc Authors
% References : analyse d'image:filtrage et segmentation, JP Cocquerez
% Authors : FM + JMA
% VERSION    : $Id: coocUniformite.m,v 1.2 2002/12/20 12:57:44 augustin Exp $
%-------------------------------------------------------------------------------

function  unif = coocUniformite(Cmat, varargin)
% varargin est la parce-que d'autres fonctions ont besoin des parametres
% supplementaire et que l'appel de toutes ces fonctions se fait de maniere
% automatique

if size(Cmat, 3) == 2
    Cmat = Cmat(:,:,1);
end
B = spdiags(Cmat, -1:1);
unif = sum(B(:) .^ 2);

% --------------------
% Auto-plot activation

if nargout == 0
    figure('NumberTitle', 'off','Name', 'Uniformite : unif = sum(Diag3 ^ 2)');
    subplot(1,3,1); imagesc(Cmat); axis equal; axis tight; title('Cmat'); colorbar;
    CLim = get(gca, 'CLim');
    subplot(1,3,2); imagesc(B, CLim); title('Diag3'); colorbar;
    subplot(1,3,3); imagesc(B .^ 2); title('Diag3 \^ 2'); colorbar;
    disp('L''uniformite est la somme de Diag3 .^ 2')
end
