nomFic = getNomFicDatabase('textureSonar02.png');
I = imread(nomFic);
I=double(I)-mean2(I) + 100;
mean2(I)
min(min(I))
max(max(I))
imwrite(I/256, 'textureSonar08.png')


nomFic = getNomFicDatabase('textureSonarMean100_01.png');
nomFic = getNomFicDatabase('textureSonarMean100_02.png');
nomFic = getNomFicDatabase('textureSonarMean100_03.png');
nomFic = getNomFicDatabase('textureSonarMean100_04.png');
I = imread(nomFic);
I = double(I);
M = mean(I(:)) %#ok<*NOPTS>
S = std(I(:))
I = M + 16 * (I -M) / S;
min(I(:))
max(I(:))
mean(I(:))
std(I(:))
imwrite(I/256, 'textureSonarMean100Std16_04.png')

% -----------------------------------------------------------------------
clear a

Dir = 1;
Par = 1:10;
W = [32 32];
subPar = [6 9 10 13];
Deplacements = 1:20;
% Deplacements = 1:5;


Dir = 1;
Par = 1:10;
W = [32 32];
Deplacements = 1:12;
% Deplacements = 1:5;

nomFic = getNomFicDatabase('textureSonar02.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonar01.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonar03.png');
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonar04.png');
I4 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);

m1 = mean(I1(:));
s1 = std(double(I1(:)));
m2 = mean(I2(:));
s2 = std(double(I2(:)));
m3 = mean(I3(:));
s3 = std(double(I3(:)));
m4 = mean(I4(:));
s4 = std(double(I4(:)));
fprintf('m1=%5.1f S1=%5.1f m2=%5.1f S3=%5.1f m4=%5.1f S4=%5.1f m4=%5.1f S4=%5.1f \n', m1, s1, m2, s2, m3, s3, m4, s4)

Style = cl_style('Color', 'b');
a(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
a(2) = cl_cooc( 'Image', I1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'r');
a(3) = cl_cooc( 'Image', I2, 'Label', Label2, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
a(4) = cl_cooc( 'Image', I2, 'Label', Label2, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'k');
a(5) = cl_cooc( 'Image', I3, 'Label', Label3, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'k', 'LineStyle', 'none', 'Marker', 'x');
a(6) = cl_cooc( 'Image', I3, 'Label', Label3, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'm');
a(7) = cl_cooc( 'Image', I4, 'Label', Label4, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'm', 'LineStyle', 'none', 'Marker', 'x');
a(8) = cl_cooc( 'Image', I4, 'Label', Label4, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);
plot(a,  'subPar', [1 3 5]);
% imagesc(a);
plot(a);
% plot(a, 'subPar', 13);

% for i=1:13
%     plot(a, 'subPar', i);
% end




nomFic = getNomFicDatabase('textureSonarMean100_01.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100_02.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100_03.png');
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100_04.png');
I4 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);

m1 = mean(I1(:));
s1 = std(double(I1(:)));
m2 = mean(I2(:));
s2 = std(double(I2(:)));
m3 = mean(I3(:));
s3 = std(double(I3(:)));
m4 = mean(I4(:));
s4 = std(double(I4(:)));
fprintf('m1=%5.1f S1=%5.1f m2=%5.1f S3=%5.1f m4=%5.1f S4=%5.1f m4=%5.1f S4=%5.1f \n', m1, s1, m2, s2, m3, s3, m4, s4)


Style = cl_style('Color', 'b');
aMean100(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
aMean100(2) = cl_cooc( 'Image', I1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'r');
aMean100(3) = cl_cooc( 'Image', I2, 'Label', Label2, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
aMean100(4) = cl_cooc( 'Image', I2, 'Label', Label2, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'k');
aMean100(5) = cl_cooc( 'Image', I3, 'Label', Label3, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'k', 'LineStyle', 'none', 'Marker', 'x');
aMean100(6) = cl_cooc( 'Image', I3, 'Label', Label3, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'm');
aMean100(7) = cl_cooc( 'Image', I4, 'Label', Label4, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'm', 'LineStyle', 'none', 'Marker', 'x');
aMean100(8) = cl_cooc( 'Image', I4, 'Label', Label4, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);
plot(aMean100);
% plot(aMean100, 'subPar', 13);

% for i=subPar
%     plot(aMean100(5:end), 'subPar', i);
% end


            
            
            
      
nomFic = getNomFicDatabase('textureSonarMean100Std16_01.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_02.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_03.png');
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_04.png');
I4 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);

m1 = mean(I1(:));
s1 = std(double(I1(:)));
m2 = mean(I2(:));
s2 = std(double(I2(:)));
m3 = mean(I3(:));
s3 = std(double(I3(:)));
m4 = mean(I4(:));
s4 = std(double(I4(:)));
fprintf('m1=%5.1f S1=%5.1f m2=%5.1f S3=%5.1f m4=%5.1f S4=%5.1f m4=%5.1f S4=%5.1f \n', m1, s1, m2, s2, m3, s3, m4, s4)


Style = cl_style('Color', 'b');
aMean100Std16(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
aMean100Std16(2) = cl_cooc( 'Image', I1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'r');
aMean100Std16(3) = cl_cooc( 'Image', I2, 'Label', Label2, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
aMean100Std16(4) = cl_cooc( 'Image', I2, 'Label', Label2, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'k');
aMean100Std16(5) = cl_cooc( 'Image', I3, 'Label', Label3, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'k', 'LineStyle', 'none', 'Marker', 'x');
aMean100Std16(6) = cl_cooc( 'Image', I3, 'Label', Label3, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'm');
aMean100Std16(7) = cl_cooc( 'Image', I4, 'Label', Label4, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'm', 'LineStyle', 'none', 'Marker', 'x');
aMean100Std16(8) = cl_cooc( 'Image', I4, 'Label', Label4, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);
            
subPar = [11 6 13:16];    
plot(aMean100Std16,  'subPar', subPar);
plot(aMean100Std16);
histo(aMean100Std16(2:2:end), 'subPar', 16, 'subDep', 6);
% plot(aMean100Std16, 'subPar', 13);

for i=1:16 
    plot(aMean100Std16, 'subPar', i);
end
for i=1:16  %length(subPar)
    plot(aMean100Std16, 'subPar', subPar(i));
end
for i=1:4
    histo(aMean100Std16(2:2:end), 'subPar', 3, 'subDep', 1);
end
histo(aMean100Std16([2 4]), 'subPar', 16, 'subDep', 1);

subPar = 1:4;
plotNuages(aMean100Std16(2:2:end), 'subdep', [1 6], ...
    'subParX', subPar, 'subParY', subPar)

subPar = 5:8;
plotNuages(aMean100Std16(2:2:end), 'subdep', [1 6], ...
    'subParX', subPar, 'subParY', subPar)

subPar = 9:12;
plotNuages(aMean100Std16(2:2:end), 'subdep', [1 6], ...
    'subParX', subPar, 'subParY', subPar)

subPar = 13:16;
plotNuages(aMean100Std16(2:2:end), 'subdep', [1 6], ...
    'subParX', subPar, 'subParY', subPar)

subPar = [6 13:16];
plotNuages(aMean100Std16(2:2:end), 'subdep', [1 6], ...
    'subParX', subPar, 'subParY', subPar)

subPar = [1:8 11:16];
plotNuages(aMean100Std16(2:2:end), 'subdep', 1, ...
    'subParX', subPar, 'subParY', subPar, 'newFig')

plotNuages(aMean100Std16(2:2:end), 'subdep', 1, 'newFig', ...
    'subParX', 6, 'subParY', [1:5 7 11:16])

plotNuages(aMean100Std16(2:2:end), 'subdep', 1, 'newFig', ...
    'subParX', 13, 'subParY', [1:5 7 11:16])

subPar = [1 3 6 8 13:16];
plotNuages(aMean100Std16(2:2:end), 'subdep', 6, ...
    'subParX', subPar, 'subParY', subPar)
plotNuages(aMean100Std16(2:2:end), 'subdep', 1, 'subParX', subPar, 'subParY', subPar)
plotNuages(aMean100Std16(2:2:end), 'subdep', 1, 'subParX', subPar, 'subParY', subPar, 'ellipsesSeules')
plotNuages(aMean100Std16(2:2:end), 'subdep', 6, 'newFig')
plotNuages(aMean100Std16(2:2:end), 'subdep', 6)

subDep = 1;
subPar1 = 1:11;
subPar2 = 12:22;
plotNuages(aMean100Std16(2:2:end), 'subdep', subDep, 'ellipsesSeules', ...
            'subParX', subPar1, 'subParY', subPar1)
plotNuages(aMean100Std16(2:2:end), 'subdep', subDep, 'ellipsesSeules', ...
            'subParX', subPar1, 'subParY', subPar2)
plotNuages(aMean100Std16(2:2:end), 'subdep', subDep, 'ellipsesSeules', ...
            'subParX', subPar2, 'subParY', subPar1)
plotNuages(aMean100Std16(2:2:end), 'subdep', subDep, 'ellipsesSeules', ...
            'subParX', subPar2, 'subParY', subPar2)


         
        
        
plotNuages(aMean100Std16(2:2:end), 'subdep', 1, 'ellipsesSeules')
subPar2 = [1 3 6 7 12 13 16 17 18]
plotNuages(aMean100Std16(2:2:end), 'subdep', 2, 'ellipsesSeules', ...
            'subParX', subPar2, 'subParY', subPar2)

        
        

d = get(aMean100Std16(2), 'Deplacements');
P = get(aMean100Std16(2), 'Params'); P = squeeze(P);
N = length(d);

% ------------------------------------------------
% Modelisation de Prony du parametre "correlation"

correlation = P(:,6);
optimProny(d, correlation)

% ------------------------------------------------
% Modelisation de Prony du parametre "contraste"

contraste = P(:,1);
optimProny(d, contraste)

% ------------------------------------------------
% Modelisation de Prony du parametre "directivite"

directivite = P(:,2);
optimProny(d, directivite)

% ------------------------------------------------
% Modelisation de Prony du parametre ""

uniformite = P(:,5);
optimProny(d, uniformite)



Q = P(:,13);
optimProny(d, Q)





















nomFic = getNomFicDatabase('textureSonar04.png');
I4 = imread(nomFic);
C = coocMat(I4, 1, 0);
coocContraste2(C)

nomFic = getNomFicDatabase('textureSonarMean100_04.png');
I4 = imread(nomFic);
CMean100 = coocMat(I4, 1, 0);
coocContraste2(CMean100)

nomFic = getNomFicDatabase('textureSonarMean100Std16_04.png');
I4 = imread(nomFic);
CMean100Std16 = coocMat(I4, 1, 0);
coocContraste2(CMean100Std16)


plot(a,  'subPar', [1 6 11 17]);
histo(a, 'subPar', 1, 'subDep', 1);


% Comparaison contraaste / dissimilarite
histo(a, 'subPar', 1,  'subDep', 1);
histo(a, 'subPar', 11, 'subDep', 1);
histo(a, 'subPar', 1,  'subDep', 6);
histo(a, 'subPar', 11, 'subDep', 6);
% Conclusion : vive la dissimilarite

nomFic = getNomFicDatabase('textureSonar02.png');
I1a = imread(nomFic);
nomFic = getNomFicDatabase('textureSonar01.png');
I2a = imread(nomFic);
nomFic = getNomFicDatabase('textureSonar03.png');
I3a = imread(nomFic);
nomFic = getNomFicDatabase('textureSonar04.png');
I4a = imread(nomFic);

C1 = coocMat(I1a, 1, 0);
coocContraste(C1)
coocTruc3(C1)
C6 = coocMat(I1a, 6, 0);
coocContraste(C6)
coocTruc3(C6)

C1 = coocMat(I2a, 1, 0);
coocContraste(C1)
coocTruc3(C1)
C6 = coocMat(I2a, 6, 0);
coocContraste(C6)
coocTruc3(C6)

figure; imagesc([I1a I2a ; I3a I4a]); colorbar; colormap(gray(256));


nomFic = getNomFicDatabase('textureSonarMean100Std16_01.png');
I1c = imread(nomFic);
nomFic = getNomFicDatabase('textureSonarMean100Std16_02.png');
I2c = imread(nomFic);
nomFic = getNomFicDatabase('textureSonarMean100Std16_03.png');
I3c = imread(nomFic);
nomFic = getNomFicDatabase('textureSonarMean100Std16_04.png');
I4c = imread(nomFic);

figure; imagesc([I1c I2c ; I3c I4c]); colorbar; colormap(gray(256));




Sin1 = 1 + 0.5 * repmat(sin(linspace(0,20*pi,256)), 256, 1);
Sin1 = Sin1 .* speckle(size(Sin1));
Sin1 = 254 * Sin1 / max(Sin1(:));
Sin1 = 1 + floor(Sin1);
mean(Sin1(:))
min(Sin1(:))
max(Sin1(:))
std(Sin1(:))
figure; imagesc(Sin1); colorbar;

clear aSin1
Style = cl_style('Color', 'b');
aSin1(1) = cl_cooc( 'Image', Sin1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
            
Sin1 = floor(100 + 22.7 * repmat(sin(linspace(0,20*pi,256)), 256, 1));
Style = cl_style('Color', 'b');
aSin1(2) = cl_cooc( 'Image', Sin1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
            
Sin1 = speckle(size(Sin1));
Sin1 = 254 * Sin1 / max(Sin1(:));
Sin1 = 1 + floor(Sin1);
Style = cl_style('Color', 'g');
aSin1(3) = cl_cooc( 'Image', Sin1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);

plot(aSin1,  'subPar', [1 6 11 16]);
C1 = coocMat(Sin1, 1, 0);
coocTruc3(C1)
C6 = coocMat(Sin1, 6, 0);
coocTruc3(C6)














nomFic = getNomFicDatabase('textureSonarMean100_01.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);
I1S = floor(double(I1) + 20*rand(size(I1))-0.5);
figure; imagesc([I1 I1S]); colorbar; colormap(gray(256));

nomFic = getNomFicDatabase('textureSonarMean100_02.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);
I2S = floor(double(I2) + 20*rand(size(I2))-0.5);
figure; imagesc([I2 I2S]); colorbar; colormap(gray(256));

nomFic = getNomFicDatabase('textureSonarMean100_03.png');
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);
I3S = floor(double(I3) + 20*rand(size(I3))-0.5);
figure; imagesc([I3 I3S]); colorbar; colormap(gray(256));

nomFic = getNomFicDatabase('textureSonarMean100_04.png');
I4 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);
I4S = floor(double(I4) + 20*rand(size(I4))-0.5);
figure; imagesc([I4 I4S]); colorbar; colormap(gray(256));


Style = cl_style('Color', 'b');
q(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'b', 'LineStyle', '--');
q(2) = cl_cooc( 'Image', I1S, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'r');
q(3) = cl_cooc( 'Image', I2, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r', 'LineStyle', '--');
q(4) = cl_cooc( 'Image', I2S, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'k');
q(5) = cl_cooc( 'Image', I3, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'k', 'LineStyle', '--');
q(6) = cl_cooc( 'Image', I3S, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'm');
q(7) = cl_cooc( 'Image', I4, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'm', 'LineStyle', '--');
q(8) = cl_cooc( 'Image', I4S, 'Label', Label1, 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);

plot(q,  'subPar', [1 6 11 16]);


% --------------------------------------------------
% Imagesd'apprentissage de la grande image Edge-Tech

I1 = loadmat('/home/doppler/tmsias/emenut/tmp/I1I2I3I4.mat', 'nomVar', 'I1');
I2 = loadmat('/home/doppler/tmsias/emenut/tmp/I1I2I3I4.mat', 'nomVar', 'I2');
I3 = loadmat('/home/doppler/tmsias/emenut/tmp/I1I2I3I4.mat', 'nomVar', 'I3');
I4 = loadmat('/home/doppler/tmsias/emenut/tmp/I1I2I3I4.mat', 'nomVar', 'I4');
figure; imagesc([I1 I2; I3 I4]); colormap(gray(256))

Deplacements = 1:6;
Dir = 1:4;
Par = 1:8;
W   = [32 32];

clear a
Style = cl_style('Color', 'b', 'LineStyle', 'none', 'Marker', '+');
a(1) = cl_cooc( 'Image', I1, 'Label', 'I1', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'r', 'LineStyle', 'none', 'Marker', 'x');
a(2) = cl_cooc( 'Image', I2, 'Label', 'I2', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'k', 'LineStyle', 'none', 'Marker', 'x');
a(3) = cl_cooc( 'Image', I3, 'Label', 'I3', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);

Style = cl_style('Color', 'm', 'LineStyle', 'none', 'Marker', 'x');
a(4) = cl_cooc( 'Image', I4, 'Label', 'I4', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style, ...
                'WindowAnalyse', W);
            
        

clear b
Style = cl_style('Color', 'b');
b(1) = cl_cooc( 'Image', I1, 'Label', 'I1', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'r');
b(2) = cl_cooc( 'Image', I2, 'Label', 'I2', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'k');
b(3) = cl_cooc( 'Image', I3, 'Label', 'I3', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
Style = cl_style('Color', 'm');
b(4) = cl_cooc( 'Image', I4, 'Label', 'I4', 'Deplacements', Deplacements, ...
                'Directions', Dir, 'ListeParams', Par, 'Style', Style);
plot(b);

plot(a);
subPar = 1:6;
plotNuages(a, 'subdep', 1, 'ellipsesSeules', ...
            'subParX', subPar, 'subParY', subPar)

        
        
        
% ------------------------------------------   
% Essai conservation matrice de cooccurrence      
        
nomFic = getNomFicDatabase('textureSonar02.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);
nomFic = getNomFicDatabase('textureSonar01.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);


Par = 1;
Dir = 1;
Dep = 1:2;
W = [32 32];

clear a
Style = cl_style('Color', 'b');
a(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'r');
a(2) = cl_cooc( 'Image', I2, 'Label', Label2, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
            
% Fusion des Images
[I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(I1,I2);

% Calcul des parametres sur l'image cible
Style = cl_style('Color', 'g', 'LineStyle', 'none', 'Marker', 'o');
b = cl_cooc( 'Image', I, 'Label', 'Melange', 'Style', Style, ...
                'Directions', Dir, ...
                'ListeParams', Par, ...
                'Deplacements', Dep, ...
                'WindowAnalyse', [W W], 'CmatRef', a);
            
imagesc(b);


d = get(b,'distCmatRef')
figure; imagesc(squeeze(d(1,1,1,:,:))); colorbar
figure; imagesc(squeeze(d(2,1,1,:,:))); colorbar
figure; imagesc(squeeze(d(1,2,1,:,:))); colorbar
figure; imagesc(squeeze(d(2,2,1,:,:))); colorbar

d1 = squeeze(d(:,1,1,:,:))
sz = size(d1)
d1 = reshape(d1, sz(1), prod(sz(2:3)))
[dmin, indmin] = min(d1);

indmin = reshape(indmin, sz(2:3))

figure; imagesc(indmin); colorbar




nomFic = getNomFicDatabase('textureSonarMean100Std16_01.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_02.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_03.png');
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_04.png');
I4 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);

Par = 1;
Dir = 1;
Dep = 1:2;
W = [32 32];

clear a
Style = cl_style('Color', 'b');
a(1) = cl_cooc( 'Image', I1, 'Label', Label1, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'r');
a(2) = cl_cooc( 'Image', I2, 'Label', Label2, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
            
Style = cl_style('Color', 'k');
a(3) = cl_cooc( 'Image', I3, 'Label', Label2, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
            
Style = cl_style('Color', 'm');
a(4) = cl_cooc( 'Image', I4, 'Label', Label2, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
            
% Fusion des Images
[I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(I1,I2,I3,I4);

% Calcul des parametres sur l'image cible
Style = cl_style('Color', 'g', 'LineStyle', 'none', 'Marker', 'o');
b = cl_cooc( 'Image', I, 'Label', 'Melange', 'Style', Style, ...
                'Directions', Dir, ...
                'ListeParams', Par, ...
                'Deplacements', Dep, ...
                'WindowAnalyse', [W W], 'CmatRef', a);
            
% imagesc(b);
% 
% d = get(b, 'distCmatRef');
% figure; imagesc(squeeze(d(1,1,1,:,:))); colorbar
% figure; imagesc(squeeze(d(2,1,1,:,:))); colorbar
% figure; imagesc(squeeze(d(1,2,1,:,:))); colorbar
% figure; imagesc(squeeze(d(2,2,1,:,:))); colorbar

d = get(b, 'distCmatRef');
d1 = squeeze(d(:,1,1,:,:));
sz = size(d1);
d1 = reshape(d1, sz(1), prod(sz(2:3)));
[dmin, indmin] = min(d1);
indmin = reshape(indmin, sz(2:3));
figure; imagesc(indmin); colorbar

          
            
            
nomFic = '/home/doppler/tmsias/augustin/tmp/rides.png';
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);
I1 = I1(1:256,1:256);

nomFic = '/home/doppler/tmsias/augustin/tmp/uniforme.png';
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);
I2 = I2(1:256,1:256);

nomFic = '/home/doppler/tmsias/augustin/tmp/nonuni.png';
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);
I3 = I3(1:256,1:256);

nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides1.png';
I41 = imread(nomFic);
nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides2.png';
I42 = imread(nomFic);
nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides3.png';
I43 = imread(nomFic);
nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides4.png';
I44 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);
I4 = [I41(:,:,1) I42(:,:,1); I43(:,:,1) I44(:,:,1)];

nomFic = '/home/doppler/tmsias/augustin/tmp/Zone4_P18.png';
Is = imread(nomFic);
Is1 = Is(1201:2750,1:800);
Is2 = Is(1351:2900,1200:1999);
I= [Is1 Is2];

