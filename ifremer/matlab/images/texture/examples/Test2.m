nomFic = getNomFicDatabase('textureSonarMean100Std16_01.png');
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_02.png');
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_03.png');
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);

nomFic = getNomFicDatabase('textureSonarMean100Std16_04.png');
I4 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);

Par = 2;
Dir = 1;
Dep = 1;
W = [32 32];

clear a
Style = cl_style('Color', 'b');
a(1) = cl_texture( 'Image', I1, 'Label', Label1, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);

Style = cl_style('Color', 'r');
a(2) = cl_texture( 'Image', I2, 'Label', Label2, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
            
Style = cl_style('Color', 'k');
a(3) = cl_texture( 'Image', I3, 'Label', Label2, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
            
Style = cl_style('Color', 'm');
a(4) = cl_texture( 'Image', I4, 'Label', Label2, 'Cmat', ...
                'Directions', Dir, 'Deplacements', Dep, 'ListeParams', Par, 'Style', Style);
            
% Fusion des Images
[I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(I1,I2,I3,I4);

% Calcul des parametres sur l'image cible
Style = cl_style('Color', 'g', 'LineStyle', 'none', 'Marker', 'o');
b = cl_texture( 'Image', I, 'Label', 'Melange', 'Style', Style, ...
                'Directions', Dir, ...
                'ListeParams', Par, ...
                'Deplacements', Dep, ...
                'WindowAnalyse', [W W], 'CmatRef', a);
            
% imagesc(b);
% 
% d = get(b, 'distCmatRef');
% figure; imagesc(squeeze(d(1,1,1,:,:))); colorbar
% figure; imagesc(squeeze(d(2,1,1,:,:))); colorbar
% figure; imagesc(squeeze(d(1,2,1,:,:))); colorbar
% figure; imagesc(squeeze(d(2,2,1,:,:))); colorbar

d = get(b, 'distCmatRef');
d1 = squeeze(d(:,1,1,:,:));
sz = size(d1);
d1 = reshape(d1, sz(1), prod(sz(2:3)));
[dmin, indmin] = min(d1);
indmin = reshape(indmin, sz(2:3));
figure; imagesc(indmin); colorbar
    dmin = reshape(dmin, sz(2:3));
    figure; imagesc(dmin); colorbar


% ------------------------------
% Essai sur l'image sonar reelle

            
nomFic = '/home/doppler/tmsias/augustin/tmp/rides.png';
I1 = imread(nomFic);
[ls , Label1] = fileparts(nomFic);
I1 = I1(1:256,1:256);

nomFic = '/home/doppler/tmsias/augustin/tmp/uniforme.png';
I2 = imread(nomFic);
[pathFic, Label2] = fileparts(nomFic);
I2 = I2(1:256,1:256);

nomFic = '/home/doppler/tmsias/augustin/tmp/nonuni.png';
I3 = imread(nomFic);
[pathFic, Label3] = fileparts(nomFic);
I3 = I3(1:256,1:256);

nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides1.png';
I41 = imread(nomFic);
nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides2.png';
I42 = imread(nomFic);
nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides3.png';
I43 = imread(nomFic);
nomFic = '/home/doppler/tmsias/augustin/tmp/petiterides4.png';
I44 = imread(nomFic);
[pathFic, Label4] = fileparts(nomFic);
I4 = [I41(:,:,1) I42(:,:,1); I43(:,:,1) I44(:,:,1)];

I = [I1 I2;I3 I4];
figure; imagesc(I); colormap(gray(256)); colorbar

nomFic = '/home/doppler/tmsias/augustin/tmp/Zone4_P18.png';
Is = imread(nomFic);
Is1 = Is(1201:2750,1:800);
Is2 = Is(1351:2900,1200:1999);
I= [Is1 Is2];


I1 = floor(double(I1) / 4);
I2 = floor(double(I2) / 4);
I3 = floor(double(I3) / 4);
I4 = floor(double(I4) / 4);
I  = floor(double(I)  / 4);
