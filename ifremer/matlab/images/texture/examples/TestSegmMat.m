nomFic1 = getNomFicDatabase('textureSonar02.png');
nomFic2 = getNomFicDatabase('textureSonar01.png');
nomFic3 = getNomFicDatabase('textureSonar03.png');
nomFic4 = getNomFicDatabase('textureSonar04.png');
I1 = double(imread(nomFic1));
I2 = double(imread(nomFic2));
I3 = double(imread(nomFic3));
I4 = double(imread(nomFic4));
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))

CmatI1 = coocMat(I1, 1, 0, 'Filtre');
CmatI2 = coocMat(I2, 1, 0, 'Filtre');
CmatI3 = coocMat(I3, 1, 0, 'Filtre');
CmatI4 = coocMat(I4, 1, 0, 'Filtre');

nomFic5 = getNomFicDatabase('textureSonarMean100Std16_01.png');
nomFic6 = getNomFicDatabase('textureSonarMean100Std16_02.png');
nomFic7 = getNomFicDatabase('textureSonarMean100Std16_03.png');
nomFic8 = getNomFicDatabase('textureSonarMean100Std16_04.png');
I5 = double(imread(nomFic5));
I6 = double(imread(nomFic6));
I7 = double(imread(nomFic7));
I8 = double(imread(nomFic8));
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))

CmatI5 = coocMat(I5, 1, 0, 'Filtre');
CmatI6 = coocMat(I6, 1, 0, 'Filtre');
CmatI7 = coocMat(I7, 1, 0, 'Filtre');
CmatI8 = coocMat(I8, 1, 0, 'Filtre');

sub=50:150  %#ok<*NOPTS>
maxCmat = max([max(CmatI5(:)) max(CmatI6(:)) max(CmatI7(:)) max(CmatI8(:))])
figure; imagesc(CmatI5(sub,sub), [0 maxCmat]); title('1'); axis xy;
figure; imagesc(CmatI6(sub,sub), [0 maxCmat]); title('2'); axis xy;
figure; imagesc(CmatI7(sub,sub), [0 maxCmat]); title('3'); axis xy;
figure; imagesc(CmatI8(sub,sub), [0 maxCmat]); title('4'); axis xy;

CmatImagertte = coocMat(I5(1:32,1:32), 1, 0, 'Filtre');
figure; imagesc(CmatImagertte(sub,sub), [0 maxCmat]); title('Imagette'); axis xy;

max2 = (maxCmat/4)^2
figure; imagesc(abs(CmatI5(sub,sub)-CmatImagertte(sub,sub)).^2, [0 max2]); title('1-Imagette'); axis xy;
figure; imagesc(abs(CmatI6(sub,sub)-CmatImagertte(sub,sub)).^2, [0 max2]); title('2-Imagette'); axis xy;
figure; imagesc(abs(CmatI7(sub,sub)-CmatImagertte(sub,sub)).^2, [0 max2]); title('3-Imagette'); axis xy;
figure; imagesc(abs(CmatI8(sub,sub)-CmatImagertte(sub,sub)).^2, [0 max2]); title('4-Imagette'); axis xy;



[segmKul_14_32_0, distKul_14_32_0, segmEqm_14_32_0, distEqm_14_32_0] = ...
    segmMat({CmatI1, CmatI2, CmatI3, CmatI4}, I14, 1, 0, [32 32], 0);
[NEqm_14_32_0, PEqm_14_32_0] = matConfusion([1 2; 3 4], segmEqm_14_32_0)
[NKul_14_32_0, PKul_14_32_0] = matConfusion([1 2; 3 4], segmKul_14_32_0)

[segmKul_14_16_0, distKul_14_16_0, segmEqm_14_16_0, distEqm_14_16_0] = ...
    segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NEqm_14_16_0, PEqm_14_16_0] = matConfusion([1 2; 3 4], segmEqm_14_16_0)
[NKul_14_16_0, PKul_14_16_0] = matConfusion([1 2; 3 4], segmKul_14_16_0)

[segmKul_14_08_0, distKul_14_08_0, segmEqm_14_08_0, distEqm_14_08_0] = ...
    segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NEqm_14_08_0, PEqm_14_08_0] = matConfusion([1 2; 3 4], segmEqm_14_08_0)
[NKul_14_08_0, PKul_14_08_0] = matConfusion([1 2; 3 4], segmKul_14_08_0)

[segmKul_14_04_0, distKul_14_04_0, segmEqm_14_04_0, distEqm_14_04_0] = ...
    segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NEqm_14_04_0, PEqm_14_04_0] = matConfusion([1 2; 3 4], segmEqm_14_04_0)
[NKul_14_04_0, PKul_14_04_0] = matConfusion([1 2; 3 4], segmKul_14_04_0)

save


[segmKul_58_32_0, distKul_58_32_0, segmEqm_58_32_0, distEqm_58_32_0] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NEqm_58_32_0, PEqm_58_32_0] = matConfusion([1 2; 3 4], segmEqm_58_32_0)
[NKul_58_32_0, PKul_58_32_0] = matConfusion([1 2; 3 4], segmKul_58_32_0)

[segmKul_58_16_0, distKul_58_16_0, segmEqm_58_16_0, distEqm_58_16_0] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NEqm_58_16_0, PEqm_58_16_0] = matConfusion([1 2; 3 4], segmEqm_58_16_0)
[NKul_58_16_0, PKul_58_16_0] = matConfusion([1 2; 3 4], segmKul_58_16_0)

[segmKul_58_08_0, distKul_58_08_0, segmEqm_58_08_0, distEqm_58_08_0] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NEqm_58_08_0, PEqm_58_08_0] = matConfusion([1 2; 3 4], segmEqm_58_08_0)
[NKul_58_08_0, PKul_58_08_0] = matConfusion([1 2; 3 4], segmKul_58_08_0)

[segmKul_58_04_0, distKul_58_04_0, segmEqm_58_04_0, distEqm_58_04_0] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NEqm_58_04_0, PEqm_58_04_0] = matConfusion([1 2; 3 4], segmEqm_58_04_0)
[NKul_58_04_0, PKul_58_04_0] = matConfusion([1 2; 3 4], segmKul_58_04_0)

save




[segmKul_14_32_1, distKul_14_32_1, segmEqm_14_32_1, distEqm_14_32_1] = ...
    segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 1);
[NEqm_14_32_1, PEqm_14_32_1] = matConfusion([1 2; 3 4], segmEqm_14_32_1)
[NKul_14_32_1, PKul_14_32_1] = matConfusion([1 2; 3 4], segmKul_14_32_1)

[segmKul_14_16_1, distKul_14_16_1, segmEqm_14_16_1, distEqm_14_16_1] = ...
    segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 1);
[NEqm_14_16_1, PEqm_14_16_1] = matConfusion([1 2; 3 4], segmEqm_14_16_1)
[NKul_14_16_1, PKul_14_16_1] = matConfusion([1 2; 3 4], segmKul_14_16_1)

[segmKul_14_08_1, distKul_14_08_1, segmEqm_14_08_1, distEqm_14_08_1] = ...
    segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 1);
[NEqm_14_08_1, PEqm_14_08_1] = matConfusion([1 2; 3 4], segmEqm_14_08_1)
[NKul_14_08_1, PKul_14_08_1] = matConfusion([1 2; 3 4], segmKul_14_08_1)

[segmKul_14_04_1, distKul_14_04_1, segmEqm_14_04_1, distEqm_14_04_1] = ...
    segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 1);
[NEqm_14_04_1, PEqm_14_04_1] = matConfusion([1 2; 3 4], segmEqm_14_04_1)
[NKul_14_04_1, PKul_14_04_1] = matConfusion([1 2; 3 4], segmKul_14_04_1)


save


[segmKul_58_32_1, distKul_58_32_1, segmEqm_58_32_1, distEqm_58_32_1] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 1);
[NEqm_58_32_1, PEqm_58_32_1] = matConfusion([1 2; 3 4], segmEqm_58_32_1)
[NKul_58_32_1, PKul_58_32_1] = matConfusion([1 2; 3 4], segmKul_58_32_1)

[segmKul_58_16_1, distKul_58_16_1, segmEqm_58_16_1, distEqm_58_16_1] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 1);
[NEqm_58_16_1, PEqm_58_16_1] = matConfusion([1 2; 3 4], segmEqm_58_16_1)
[NKul_58_16_1, PKul_58_16_1] = matConfusion([1 2; 3 4], segmKul_58_16_1)

[segmKul_58_08_1, distKul_58_08_1, segmEqm_58_08_1, distEqm_58_08_1] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 1);
[NEqm_58_08_1, PEqm_58_08_1] = matConfusion([1 2; 3 4], segmEqm_58_08_1)
[NKul_58_08_1, PKul_58_08_1] = matConfusion([1 2; 3 4], segmKul_58_08_1)

[segmKul_58_04_1, distKul_58_04_1, segmEqm_58_04_1, distEqm_58_04_1] = ...
    segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 1);
[NEqm_58_04_1, PEqm_58_04_1] = matConfusion([1 2; 3 4], segmEqm_58_04_1)
[NKul_58_04_1, PKul_58_04_1] = matConfusion([1 2; 3 4], segmKul_58_04_1)

save


figure;
str = sprintf('I14 32 0')
subplot(2,1,1); imagesc(segmEqm_14_32_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_32_0); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I14 16 0')
subplot(2,1,1); imagesc(segmEqm_14_16_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_16_0); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I14 8 0')
subplot(2,1,1); imagesc(segmEqm_14_08_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_08_0); colorbar; title([str '  par Kullback'])

figure;
str = sprintf('I14 4 0')
subplot(2,1,1); imagesc(segmEqm_14_04_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_04_0); colorbar; title([str '  par Kullback'])




figure;
str = sprintf('I58 32 0')
subplot(2,1,1); imagesc(segmEqm_58_32_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_32_0); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I58 16 0')
subplot(2,1,1); imagesc(segmEqm_58_16_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_16_0); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I58 8 0')
subplot(2,1,1); imagesc(segmEqm_58_08_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_08_0); colorbar; title([str '  par Kullback'])

figure;
str = sprintf('I58 4 0')
subplot(2,1,1); imagesc(segmEqm_58_04_0); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_04_0); colorbar; title([str '  par Kullback'])










figure;
str = sprintf('I14 32 1')
subplot(2,1,1); imagesc(segmEqm_14_32_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_32_1); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I14 16 1')
subplot(2,1,1); imagesc(segmEqm_14_16_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_16_1); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I14 8 1')
subplot(2,1,1); imagesc(segmEqm_14_08_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_08_1); colorbar; title([str '  par Kullback'])

figure;
str = sprintf('I14 4 1')
subplot(2,1,1); imagesc(segmEqm_14_04_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_14_04_1); colorbar; title([str '  par Kullback'])




figure;
str = sprintf('I58 32 1')
subplot(2,1,1); imagesc(segmEqm_58_32_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_32_1); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I58 16 1')
subplot(2,1,1); imagesc(segmEqm_58_16_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_16_1); colorbar; title([str '  par Kullback'])


figure;
str = sprintf('I58 8 1')
subplot(2,1,1); imagesc(segmEqm_58_08_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_08_1); colorbar; title([str '  par Kullback'])

figure;
str = sprintf('I58 4 1')
subplot(2,1,1); imagesc(segmEqm_58_04_1); colorbar; title([str '  par EQM'])
subplot(2,1,2); imagesc(segmKul_58_04_1); colorbar; title([str '  par Kullback'])





        for k=1:4
            %         histo1D(distEqm(:,:,k));
            %         histo1D(distKul(:,:,k));
            figure;
            subplot(2,1,1); imagesc(distEqm_14_32_1(:,:,k)); colorbar
            subplot(2,1,2); imagesc(distKul_14_32_1(:,:,k)); colorbar
        end


        
        
        
        
figure;
n = 4;
str = sprintf('I14 32 0')
subplot(n,2,1); imagesc(segmEqm_14_32_0); axis off; title([str '  par EQM'])
subplot(n,2,2); imagesc(segmKul_14_32_0); axis off; title([str '  par Kullback'])

str = sprintf('I14 16 0')
subplot(n,2,3); imagesc(segmEqm_14_16_0); axis off; title([str '  par EQM'])
subplot(n,2,4); imagesc(segmKul_14_16_0); axis off; title([str '  par Kullback'])

str = sprintf('I14 8 0')
subplot(n,2,5); imagesc(segmEqm_14_08_0); axis off; title([str '  par EQM'])
subplot(n,2,6); imagesc(segmKul_14_08_0); axis off; title([str '  par Kullback'])

str = sprintf('I14 4 0')
subplot(n,2,7); imagesc(segmEqm_14_04_0); axis off; title([str '  par EQM'])
subplot(n,2,8); imagesc(segmKul_14_04_0); axis off; title([str '  par Kullback'])



figure;
n = 4;
str = sprintf('I58 32 0')
subplot(n,2,1); imagesc(segmEqm_58_32_0); axis off; title([str '  par EQM'])
subplot(n,2,2); imagesc(segmKul_58_32_0); axis off; title([str '  par Kullback'])

str = sprintf('I58 16 0')
subplot(n,2,3); imagesc(segmEqm_58_16_0); axis off; title([str '  par EQM'])
subplot(n,2,4); imagesc(segmKul_58_16_0); axis off; title([str '  par Kullback'])

str = sprintf('I58 8 0')
subplot(n,2,5); imagesc(segmEqm_58_08_0); axis off; title([str '  par EQM'])
subplot(n,2,6); imagesc(segmKul_58_08_0); axis off; title([str '  par Kullback'])

str = sprintf('I58 4 0')
subplot(n,2,7); imagesc(segmEqm_58_04_0); axis off; title([str '  par EQM'])
subplot(n,2,8); imagesc(segmKul_58_04_0); axis off; title([str '  par Kullback'])



figure;
n = 4;
str = sprintf('Filtrage : I14 32 1')
subplot(n,2,1); imagesc(segmEqm_14_32_1); axis off; title([str '  par EQM'])
subplot(n,2,2); imagesc(segmKul_14_32_1); axis off; title([str '  par Kullback'])

str = sprintf('Filtrage : I14 16 1')
subplot(n,2,3); imagesc(segmEqm_14_16_1); axis off; title([str '  par EQM'])
subplot(n,2,4); imagesc(segmKul_14_16_1); axis off; title([str '  par Kullback'])

str = sprintf('Filtrage : I14 8 1')
subplot(n,2,5); imagesc(segmEqm_14_08_1); axis off; title([str '  par EQM'])
subplot(n,2,6); imagesc(segmKul_14_08_1); axis off; title([str '  par Kullback'])

str = sprintf('Filtrage : I14 4 1')
subplot(n,2,7); imagesc(segmEqm_14_04_1); axis off; title([str '  par EQM'])
subplot(n,2,8); imagesc(segmKul_14_04_1); axis off; title([str '  par Kullback'])



figure;
n = 4;
str = sprintf('Filtrage : I58 32 1')
subplot(n,2,1); imagesc(segmEqm_58_32_1); axis off; title([str '  par EQM'])
subplot(n,2,2); imagesc(segmKul_58_32_1); axis off; title([str '  par Kullback'])

str = sprintf('Filtrage : I58 16 1')
subplot(n,2,3); imagesc(segmEqm_58_16_1); axis off; title([str '  par EQM'])
subplot(n,2,4); imagesc(segmKul_58_16_1); axis off; title([str '  par Kullback'])

str = sprintf('Filtrage : I58 8 1')
subplot(n,2,5); imagesc(segmEqm_58_08_1); axis off; title([str '  par EQM'])
subplot(n,2,6); imagesc(segmKul_58_08_1); axis off; title([str '  par Kullback'])

str = sprintf('Filtrage : I58 4 1')
subplot(n,2,7); imagesc(segmEqm_58_04_1); axis off; title([str '  par EQM'])
subplot(n,2,8); imagesc(segmKul_58_04_1); axis off; title([str '  par Kullback'])

