cd /home1/doppler/IfremerToolbox/ifremer/matlab/images/texture
% load ResultatTQuantification

nomFic1 = getNomFicDatabase('textureSonar02.png');
nomFic2 = getNomFicDatabase('textureSonar01.png');
nomFic3 = getNomFicDatabase('textureSonar03.png');
nomFic4 = getNomFicDatabase('textureSonar04.png');
I1 = imread(nomFic1);
I2 = imread(nomFic2);
I3 = imread(nomFic3);
I4 = imread(nomFic4);
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))




[segmKul_14_32_256, distKul_14_32_256] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0);
[NKul_14_32_256, PKul_14_32_256] = matConfusion([1 2; 3 4], segmKul_14_32_256) %#ok<*NOPTS>

[segmKul_14_16_256, distKul_14_16_256] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NKul_14_16_256, PKul_14_16_256] = matConfusion([1 2; 3 4], segmKul_14_16_256)

[segmKul_14_08_256, distKul_14_08_256] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NKul_14_08_256, PKul_14_08_256] = matConfusion([1 2; 3 4], segmKul_14_08_256)

[segmKul_14_04_256, distKul_14_04_256] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NKul_14_04_256, PKul_14_04_256] = matConfusion([1 2; 3 4], segmKul_14_04_256)

save ResultatTQuantification


I1 = ceil(double(I1) / 2);
I2 = ceil(double(I2) / 2);
I3 = ceil(double(I3) / 2);
I4 = ceil(double(I4) / 2);
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))

[segmKul_14_32_128, distKul_14_32_128] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0);
[NKul_14_32_128, PKul_14_32_128] = matConfusion([1 2; 3 4], segmKul_14_32_128)

[segmKul_14_16_128, distKul_14_16_128] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NKul_14_16_128, PKul_14_16_128] = matConfusion([1 2; 3 4], segmKul_14_16_128)

[segmKul_14_08_128, distKul_14_08_128] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NKul_14_08_128, PKul_14_08_128] = matConfusion([1 2; 3 4], segmKul_14_08_128)

[segmKul_14_04_128, distKul_14_04_128] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NKul_14_04_128, PKul_14_04_128] = matConfusion([1 2; 3 4], segmKul_14_04_128)


I1 = ceil(double(I1) / 2);
I2 = ceil(double(I2) / 2);
I3 = ceil(double(I3) / 2);
I4 = ceil(double(I4) / 2);
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))

[segmKul_14_32_64, distKul_14_32_64] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0);
[NKul_14_32_64, PKul_14_32_64] = matConfusion([1 2; 3 4], segmKul_14_32_64)

[segmKul_14_16_64, distKul_14_16_64] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NKul_14_16_64, PKul_14_16_64] = matConfusion([1 2; 3 4], segmKul_14_16_64)

[segmKul_14_08_64, distKul_14_08_64] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NKul_14_08_64, PKul_14_08_64] = matConfusion([1 2; 3 4], segmKul_14_08_64)

[segmKul_14_04_64, distKul_14_04_64] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NKul_14_04_64, PKul_14_04_64] = matConfusion([1 2; 3 4], segmKul_14_04_64)
save ResultatTQuantification


I1 = ceil(double(I1) / 2);
I2 = ceil(double(I2) / 2);
I3 = ceil(double(I3) / 2);
I4 = ceil(double(I4) / 2);
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))

[segmKul_14_32_32, distKul_14_32_32] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0);
[NKul_14_32_32, PKul_14_32_32] = matConfusion([1 2; 3 4], segmKul_14_32_32)

[segmKul_14_16_32, distKul_14_16_32] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NKul_14_16_32, PKul_14_16_32] = matConfusion([1 2; 3 4], segmKul_14_16_32)

[segmKul_14_08_32, distKul_14_08_32] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NKul_14_08_32, PKul_14_08_32] = matConfusion([1 2; 3 4], segmKul_14_08_32)

[segmKul_14_04_32, distKul_14_04_32] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NKul_14_04_32, PKul_14_04_32] = matConfusion([1 2; 3 4], segmKul_14_04_32)
save ResultatTQuantification



I1 = ceil(double(I1) / 2);
I2 = ceil(double(I2) / 2);
I3 = ceil(double(I3) / 2);
I4 = ceil(double(I4) / 2);
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))

[segmKul_14_32_16, distKul_14_32_16] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0);
[NKul_14_32_16, PKul_14_32_16] = matConfusion([1 2; 3 4], segmKul_14_32_16)

[segmKul_14_16_16, distKul_14_16_16] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NKul_14_16_16, PKul_14_16_16] = matConfusion([1 2; 3 4], segmKul_14_16_16)

[segmKul_14_08_16, distKul_14_08_16] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NKul_14_08_16, PKul_14_08_16] = matConfusion([1 2; 3 4], segmKul_14_08_16)

[segmKul_14_04_16, distKul_14_04_16] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NKul_14_04_16, PKul_14_04_16] = matConfusion([1 2; 3 4], segmKul_14_04_16)
save ResultatTQuantification



I1 = ceil(double(I1) / 2);
I2 = ceil(double(I2) / 2);
I3 = ceil(double(I3) / 2);
I4 = ceil(double(I4) / 2);
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))

[segmKul_14_32_8, distKul_14_32_8] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0);
[NKul_14_32_8, PKul_14_32_8] = matConfusion([1 2; 3 4], segmKul_14_32_8)

[segmKul_14_16_8, distKul_14_16_8] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NKul_14_16_8, PKul_14_16_8] = matConfusion([1 2; 3 4], segmKul_14_16_8)

[segmKul_14_08_8, distKul_14_08_8] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NKul_14_08_8, PKul_14_08_8] = matConfusion([1 2; 3 4], segmKul_14_08_8)

[segmKul_14_04_8, distKul_14_04_8] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NKul_14_04_8, PKul_14_04_8] = matConfusion([1 2; 3 4], segmKul_14_04_8)
save ResultatTQuantification



I1 = ceil(double(I1) / 2);
I2 = ceil(double(I2) / 2);
I3 = ceil(double(I3) / 2);
I4 = ceil(double(I4) / 2);
I14 = [I1 I2; I3 I4];
figure; imagesc(I14); colormap(gray(256))

[segmKul_14_32_4, distKul_14_32_4] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0);
[NKul_14_32_4, PKul_14_32_4] = matConfusion([1 2; 3 4], segmKul_14_32_4)

[segmKul_14_16_4, distKul_14_16_4] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0);
[NKul_14_16_4, PKul_14_16_4] = matConfusion([1 2; 3 4], segmKul_14_16_4)

[segmKul_14_08_4, distKul_14_08_4] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0);
[NKul_14_08_4, PKul_14_08_4] = matConfusion([1 2; 3 4], segmKul_14_08_4)

[segmKul_14_04_4, distKul_14_04_4] = segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0);
[NKul_14_04_4, PKul_14_04_4] = matConfusion([1 2; 3 4], segmKul_14_04_4)
save ResultatTQuantification



figure; n = 7;

str = sprintf('I14 32 256')
subplot(n,4,1); imagesc(segmKul_14_32_256); title(str); axis off;

str = sprintf('I14 16 256')
subplot(n,4,2); imagesc(segmKul_14_16_256); title(str); axis off;

str = sprintf('I14 8 256')
subplot(n,4,3); imagesc(segmKul_14_08_256); title(str); axis off;

str = sprintf('I14 4 256')
subplot(n,4,4); imagesc(segmKul_14_04_256); title(str); axis off;


str = sprintf('I14 32 128')
subplot(n,4,5); imagesc(segmKul_14_32_128); title(str); axis off;

str = sprintf('I14 16 128')
subplot(n,4,6); imagesc(segmKul_14_16_128); title(str); axis off;

str = sprintf('I14 8 128')
subplot(n,4,7); imagesc(segmKul_14_08_128); title(str); axis off;

str = sprintf('I14 4 128')
subplot(n,4,8); imagesc(segmKul_14_04_128); title(str); axis off;


str = sprintf('I14 32 64')
subplot(n,4,9); imagesc(segmKul_14_32_64); title(str); axis off;

str = sprintf('I14 16 64')
subplot(n,4,10); imagesc(segmKul_14_16_64); title(str); axis off;

str = sprintf('I14 8 64')
subplot(n,4,11); imagesc(segmKul_14_08_64); title(str); axis off;

str = sprintf('I14 4 64')
subplot(n,4,12); imagesc(segmKul_14_04_64); title(str); axis off;


str = sprintf('I14 32 32')
subplot(n,4,13); imagesc(segmKul_14_32_32); title(str); axis off;

str = sprintf('I14 16 32')
subplot(n,4,14); imagesc(segmKul_14_16_32); title(str); axis off;

str = sprintf('I14 8 32')
subplot(n,4,15); imagesc(segmKul_14_08_32); title(str); axis off;

str = sprintf('I14 4 32')
subplot(n,4,16); imagesc(segmKul_14_04_32); title(str); axis off;


str = sprintf('I14 32 16')
subplot(n,4,17); imagesc(segmKul_14_32_16); title(str); axis off;

str = sprintf('I14 16 16')
subplot(n,4,18); imagesc(segmKul_14_16_16); title(str); axis off;

str = sprintf('I14 8 16')
subplot(n,4,19); imagesc(segmKul_14_08_16); title(str); axis off;

str = sprintf('I14 4 16')
subplot(n,4,20); imagesc(segmKul_14_04_16); title(str); axis off;



str = sprintf('I14 32 8')
subplot(n,4,21); imagesc(segmKul_14_32_8); title(str); axis off;

str = sprintf('I14 16 8')
subplot(n,4,22); imagesc(segmKul_14_16_8); title(str); axis off;

str = sprintf('I14 8 8')
subplot(n,4,23); imagesc(segmKul_14_08_8); title(str); axis off;

str = sprintf('I14 4 8')
subplot(n,4,24); imagesc(segmKul_14_04_8); title(str); axis off;



str = sprintf('I14 32 4')
subplot(n,4,25); imagesc(segmKul_14_32_4); title(str); axis off;

str = sprintf('I14 16 4')
subplot(n,4,26); imagesc(segmKul_14_16_4); title(str); axis off;

str = sprintf('I14 8 4')
subplot(n,4,27); imagesc(segmKul_14_08_4); title(str); axis off;

str = sprintf('I14 4 4')
subplot(n,4,28); imagesc(segmKul_14_04_4); title(str); axis off;




%         for k=1:4
%             figure;
%             imagesc(distKul_14_08_256(:,:,k)); colorbar
%         end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        

nomFic5 = getNomFicDatabase('textureSonarMean100Std16_01.png');
nomFic6 = getNomFicDatabase('textureSonarMean100Std16_02.png');
nomFic7 = getNomFicDatabase('textureSonarMean100Std16_03.png');
nomFic8 = getNomFicDatabase('textureSonarMean100Std16_04.png');
I5 = imread(nomFic5);
I6 = imread(nomFic6);
I7 = imread(nomFic7);
I8 = imread(nomFic8);
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))


[segmKul_58_32_256, distKul_58_32_256] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NKul_58_32_256, PKul_58_32_256] = matConfusion([1 2; 3 4], segmKul_58_32_256)

[segmKul_58_16_256, distKul_58_16_256] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NKul_58_16_256, PKul_58_16_256] = matConfusion([1 2; 3 4], segmKul_58_16_256)

[segmKul_58_08_256, distKul_58_08_256] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NKul_58_08_256, PKul_58_08_256] = matConfusion([1 2; 3 4], segmKul_58_08_256)

[segmKul_58_04_256, distKul_58_04_256] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NKul_58_04_256, PKul_58_04_256] = matConfusion([1 2; 3 4], segmKul_58_04_256)
save ResultatTQuantification


I5 = ceil(double(I5) / 2);
I6 = ceil(double(I6) / 2);
I7 = ceil(double(I7) / 2);
I8 = ceil(double(I8) / 2);
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))

[segmKul_58_32_128, distKul_58_32_128] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NKul_58_32_128, PKul_58_32_128] = matConfusion([1 2; 3 4], segmKul_58_32_128)

[segmKul_58_16_128, distKul_58_16_128] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NKul_58_16_128, PKul_58_16_128] = matConfusion([1 2; 3 4], segmKul_58_16_128)

[segmKul_58_08_128, distKul_58_08_128] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NKul_58_08_128, PKul_58_08_128] = matConfusion([1 2; 3 4], segmKul_58_08_128)

[segmKul_58_04_128, distKul_58_04_128] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NKul_58_04_128, PKul_58_04_128] = matConfusion([1 2; 3 4], segmKul_58_04_128)
save ResultatTQuantification


I5 = ceil(double(I5) / 2);
I6 = ceil(double(I6) / 2);
I7 = ceil(double(I7) / 2);
I8 = ceil(double(I8) / 2);
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))

[segmKul_58_32_64, distKul_58_32_64] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NKul_58_32_64, PKul_58_32_64] = matConfusion([1 2; 3 4], segmKul_58_32_64)

[segmKul_58_16_64, distKul_58_16_64] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NKul_58_16_64, PKul_58_16_64] = matConfusion([1 2; 3 4], segmKul_58_16_64)

[segmKul_58_08_64, distKul_58_08_64] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NKul_58_08_64, PKul_58_08_64] = matConfusion([1 2; 3 4], segmKul_58_08_64)

[segmKul_58_04_64, distKul_58_04_64] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NKul_58_04_64, PKul_58_04_64] = matConfusion([1 2; 3 4], segmKul_58_04_64)
save ResultatTQuantification


I5 = ceil(double(I5) / 2);
I6 = ceil(double(I6) / 2);
I7 = ceil(double(I7) / 2);
I8 = ceil(double(I8) / 2);
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))

[segmKul_58_32_32, distKul_58_32_32] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NKul_58_32_32, PKul_58_32_32] = matConfusion([1 2; 3 4], segmKul_58_32_32)

[segmKul_58_16_32, distKul_58_16_32] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NKul_58_16_32, PKul_58_16_32] = matConfusion([1 2; 3 4], segmKul_58_16_32)

[segmKul_58_08_32, distKul_58_08_32] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NKul_58_08_32, PKul_58_08_32] = matConfusion([1 2; 3 4], segmKul_58_08_32)

[segmKul_58_04_32, distKul_58_04_32] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NKul_58_04_32, PKul_58_04_32] = matConfusion([1 2; 3 4], segmKul_58_04_32)
save ResultatTQuantification



I5 = ceil(double(I5) / 2);
I6 = ceil(double(I6) / 2);
I7 = ceil(double(I7) / 2);
I8 = ceil(double(I8) / 2);
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))

[segmKul_58_32_16, distKul_58_32_16] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NKul_58_32_16, PKul_58_32_16] = matConfusion([1 2; 3 4], segmKul_58_32_16)

[segmKul_58_16_16, distKul_58_16_16] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NKul_58_16_16, PKul_58_16_16] = matConfusion([1 2; 3 4], segmKul_58_16_16)

[segmKul_58_08_16, distKul_58_08_16] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NKul_58_08_16, PKul_58_08_16] = matConfusion([1 2; 3 4], segmKul_58_08_16)

[segmKul_58_04_16, distKul_58_04_16] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NKul_58_04_16, PKul_58_04_16] = matConfusion([1 2; 3 4], segmKul_58_04_16)
save ResultatTQuantification



I5 = ceil(double(I5) / 2);
I6 = ceil(double(I6) / 2);
I7 = ceil(double(I7) / 2);
I8 = ceil(double(I8) / 2);
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))

[segmKul_58_32_8, distKul_58_32_8] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NKul_58_32_8, PKul_58_32_8] = matConfusion([1 2; 3 4], segmKul_58_32_8)

[segmKul_58_16_8, distKul_58_16_8] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NKul_58_16_8, PKul_58_16_8] = matConfusion([1 2; 3 4], segmKul_58_16_8)

[segmKul_58_08_8, distKul_58_08_8] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NKul_58_08_8, PKul_58_08_8] = matConfusion([1 2; 3 4], segmKul_58_08_8)

[segmKul_58_04_8, distKul_58_04_8] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NKul_58_04_8, PKul_58_04_8] = matConfusion([1 2; 3 4], segmKul_58_04_8)
save ResultatTQuantification



I5 = ceil(double(I5) / 2);
I6 = ceil(double(I6) / 2);
I7 = ceil(double(I7) / 2);
I8 = ceil(double(I8) / 2);
I58 = [I5 I6; I7 I8];
figure; imagesc(I58); colormap(gray(256))

[segmKul_58_32_4, distKul_58_32_4] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
[NKul_58_32_4, PKul_58_32_4] = matConfusion([1 2; 3 4], segmKul_58_32_4)

[segmKul_58_16_4, distKul_58_16_4] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0);
[NKul_58_16_4, PKul_58_16_4] = matConfusion([1 2; 3 4], segmKul_58_16_4)

[segmKul_58_08_4, distKul_58_08_4] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0);
[NKul_58_08_4, PKul_58_08_4] = matConfusion([1 2; 3 4], segmKul_58_08_4)

[segmKul_58_04_4, distKul_58_04_4] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0);
[NKul_58_04_4, PKul_58_04_4] = matConfusion([1 2; 3 4], segmKul_58_04_4)
save ResultatTQuantification



figure; n = 7;

str = sprintf('I58 32 256')
subplot(n,4,1); imagesc(segmKul_58_32_256); title(str); axis off;

str = sprintf('I58 16 256')
subplot(n,4,2); imagesc(segmKul_58_16_256); title(str); axis off;

str = sprintf('I58 8 256')
subplot(n,4,3); imagesc(segmKul_58_08_256); title(str); axis off;

str = sprintf('I58 4 256')
subplot(n,4,4); imagesc(segmKul_58_04_256); title(str); axis off;


str = sprintf('I58 32 128')
subplot(n,4,5); imagesc(segmKul_58_32_128); title(str); axis off;

str = sprintf('I58 16 128')
subplot(n,4,6); imagesc(segmKul_58_16_128); title(str); axis off;

str = sprintf('I58 8 128')
subplot(n,4,7); imagesc(segmKul_58_08_128); title(str); axis off;

str = sprintf('I58 4 128')
subplot(n,4,8); imagesc(segmKul_58_04_128); title(str); axis off;


str = sprintf('I58 32 64')
subplot(n,4,9); imagesc(segmKul_58_32_64); title(str); axis off;

str = sprintf('I58 16 64')
subplot(n,4,10); imagesc(segmKul_58_16_64); title(str); axis off;

str = sprintf('I58 8 64')
subplot(n,4,11); imagesc(segmKul_58_08_64); title(str); axis off;

str = sprintf('I58 4 64')
subplot(n,4,12); imagesc(segmKul_58_04_64); title(str); axis off;


str = sprintf('I58 32 32')
subplot(n,4,13); imagesc(segmKul_58_32_32); title(str); axis off;

str = sprintf('I58 16 32')
subplot(n,4,14); imagesc(segmKul_58_16_32); title(str); axis off;

str = sprintf('I58 8 32')
subplot(n,4,15); imagesc(segmKul_58_08_32); title(str); axis off;

str = sprintf('I58 4 32')
subplot(n,4,16); imagesc(segmKul_58_04_32); title(str); axis off;


str = sprintf('I58 32 16')
subplot(n,4,17); imagesc(segmKul_58_32_16); title(str); axis off;

str = sprintf('I58 16 16')
subplot(n,4,18); imagesc(segmKul_58_16_16); title(str); axis off;

str = sprintf('I58 8 16')
subplot(n,4,19); imagesc(segmKul_58_08_16); title(str); axis off;

str = sprintf('I58 4 16')
subplot(n,4,20); imagesc(segmKul_58_04_16); title(str); axis off;



str = sprintf('I58 32 8')
subplot(n,4,21); imagesc(segmKul_58_32_8); title(str); axis off;

str = sprintf('I58 16 8')
subplot(n,4,22); imagesc(segmKul_58_16_8); title(str); axis off;

str = sprintf('I58 8 8')
subplot(n,4,23); imagesc(segmKul_58_08_8); title(str); axis off;

str = sprintf('I58 4 8')
subplot(n,4,24); imagesc(segmKul_58_04_8); title(str); axis off;



str = sprintf('I58 32 4')
subplot(n,4,25); imagesc(segmKul_58_32_4); title(str); axis off;

str = sprintf('I58 16 4')
subplot(n,4,26); imagesc(segmKul_58_16_4); title(str); axis off;

str = sprintf('I58 8 4')
subplot(n,4,27); imagesc(segmKul_58_08_4); title(str); axis off;

str = sprintf('I58 4 4')
subplot(n,4,28); imagesc(segmKul_58_04_4); title(str); axis off;



