dX = 1; dY = 0; W = [16 16];

nomFic = getNomFicDatabase('textureSonar02.png');
I1 = imread(nomFic);

nomFic = getNomFicDatabase('textureSonar01.png');
I2 = imread(nomFic);

nomFic = getNomFicDatabase('textureSonar03.png');
I3 = imread(nomFic);

nomFic = getNomFicDatabase('textureSonar04.png');
I4 = imread(nomFic);

% Calcul des matrices de reference
Cmat1 = coocMat(I1, dX, dY, 'Filtre');
Cmat2 = coocMat(I2, dX, dY, 'Filtre');
Cmat3 = coocMat(I3, dX, dY, 'Filtre');
Cmat4 = coocMat(I4, dX, dY, 'Filtre');

% Fusion des Images
[I, Cdeb, Cfin, Ldeb, Lfin] = fusionImages(I1,I2, I3, I4);
figure; imagesc(I); colormap(gray(256));

nbl = ceil(size(I,1) / W(1));
nbc = ceil(size(I,2) / W(2));
N = nbl*nbc;
hw = create_waitbar('Processing', 'N', N);
eqm = zeros(nbl, nbc, 4);
k = 0; 
for i=1:nbl
    subl = (1+(i-1)*nbl):min(i*nbl, size(I,1));
    for j=1:nbc
        my_waitbar(k, N, hw);
        k = k + 1;
        subc = (1+(j-1)*nbc):min(j*nbc, size(I,2));
        Cmat = coocMat(I(subl,subc), dX, dY, 'Filtre');
        eqm(i, j, 1) = sum(sum((Cmat1 - Cmat) .^ 2));
        eqm(i, j, 2) = sum(sum((Cmat2 - Cmat) .^ 2));
        eqm(i, j, 3) = sum(sum((Cmat3 - Cmat) .^ 2));
        eqm(i, j, 4) = sum(sum((Cmat4 - Cmat) .^ 2));
    end
end
my_close(hw, 'MsgEnd')

figure; imagesc(eqm(:,:,1)); colorbar
figure; imagesc(eqm(:,:,2)); colorbar
figure; imagesc(eqm(:,:,3)); colorbar
figure; imagesc(eqm(:,:,4)); colorbar

[eqmMin, indMin] = min(eqm, [], 3);
figure; imagesc(indMin); colorbar;

