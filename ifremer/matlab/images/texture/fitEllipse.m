% P = get(aMean100Std16(2), 'Params');
% P = squeeze(P);
% P1 = P(1,1,:,:);
% P2 = P(2,1,:,:);
% P1 = squeeze(P1);
% P2 = squeeze(P2);
% [X, Y] = fitEllipse(P1, P2)

function [X, Y] = fitEllipse(P2, P1)

P1 = P1(:);
P2 = P2(:);

minP1 = min(P1);
maxP1 = max(P1);
minP2 = min(P2);
maxP2 = max(P2);
P1 = (P1 - minP1) / (maxP1 - minP1);
P2 = (P2 - minP2) / (maxP2 - minP2);

Pol = polyfit(P1, P2, 1);

angle = atan(Pol(1));

c = cos(angle);
s = sin(angle);

data = [P1(:) P2(:)] * [c -s;s c];
XRot = data(:,1);
YRot = data(:,2);
sXRot = std(XRot);
sYRot = std(YRot);

Teta = linspace(0, 2*pi, 360);
XRotEllipse = mean(XRot) + 2*sXRot * cos(Teta);
YRotEllipse = mean(YRot) + 2*sYRot * sin(Teta);

XY = [XRotEllipse', YRotEllipse'] * [c s;-s c];

Y = minP1 + XY(:,1)' * (maxP1 - minP1);
X = minP2 + XY(:,2)' * (maxP2 - minP2);
