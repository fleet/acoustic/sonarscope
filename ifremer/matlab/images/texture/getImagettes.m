function [indAngle, iC, iL] = getImagettes(QI, QV)

% [nbL, nbC] = size(I);
[nbLI, nbCI] = size(QI);

nbImagettes = sum(QV(:));

iC = zeros(nbImagettes, 1);
iL = zeros(nbImagettes, 1);

k = 1;
indAngle = zeros(nbImagettes, 1);

% Note pour GLU : j'ai invers� les boucles pour que dans le cas o� la
% matrice image est un memmapfile (pointant sur une image de type ErMapper)
% on puisse lire des paquets de lignes sans redondance de lecture.

hw = create_waitbar(Lang('Etape 1 de la segmentation', 'Segmentation processing step 1'), 'N', nbImagettes);
for i=1:nbLI
    for j=1:nbCI
        my_waitbar(k, nbImagettes, hw);
        if QV(i,j)
            indAngle(k) = QI(i,j);
            iC(k) = j;
            iL(k) = i;
            k = k + 1;
        end
    end
end
my_close(hw, 'MsgEnd')

% BUG incomprehensible
if length(indAngle) > nbImagettes
    indAngle = indAngle(1:nbImagettes);
end
