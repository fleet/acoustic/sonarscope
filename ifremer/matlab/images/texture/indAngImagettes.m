function [Q, QV, nbLI, nbCI] = indAngImagettes(Image, W, MasqueImageValide)

[nbL, nbC] = size(Image);
nbLI = 1 + floor((nbL-1) / W(1));
nbCI = 1 + floor((nbC-1) / W(2));

nbLNew = nbLI * W(1);
nbCNew = nbCI * W(2);

ANew = Image;
ANew(nbLNew,nbCNew) = 0;
ANew = reshape(ANew, W(1), nbLI, W(2), nbCI);

Q = squeeze(myMedian(ANew));
Q = shiftdim(Q,1);
Q = squeeze(myMedian(Q))';

ANew = MasqueImageValide;
ANew(nbLNew,nbCNew) = 0;
ANew = reshape(ANew, W(1), nbLI, W(2), nbCI);

QV = squeeze(any(ANew));
QV = shiftdim(QV,1);
QV = squeeze(any(QV))';
