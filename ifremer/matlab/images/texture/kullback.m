% Calcul de la distance inter-probabilite de Kullback-Leibler
% distance de la densite de probabilite P1 par rapport a la densite de proba p2 
%
% Syntax
%   dist = kullback(p1, p2)
% 
% Input Arguments 
%   P1 : densite de proba
%   p2 : densite de proba
%
% Output Arguments 
%   dist : distance de probabilite
%
% Examples
%   p1 = rand(64);
%   p2 = rand(64);
%   dist = kullback(p1, p2)
%
% See also eqm Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function dist = kullback(p1, p2, varargin)

if nargin == 3
    nbProcessors = varargin{1};
else
    nbProcessors = 0;
end
% P1 = p1;
% P2 = p2;
if nbProcessors == 0
    p1 = p1(:);
    p2 = p2(:);
    
    sub = (p1 == 0);
    p1(sub) = [];
    p2(sub) = [];
    
%     p1 = double(p1(:)) + 0.00001;
%     p2 = double(p2(:)) + 0.00001;
    p1 = double(p1(:)) + eps;
    p2 = double(p2(:)) + eps;
% if nbProcessors == 0
    dist = sum(p1 .* log2(p1 ./ p2));
else
    dist = kullback_mexmc(double(p1), double(p2), varargin{:});
end

% if single(dist1) ~= single(dist2)
%     dist1
%     dist2
% end
% dist = dist1;
%{
figure; plot(p1,'b'); grid on; hold on; plot(p2,'r'); title(sprintf('dist = %f', dist))
%}

