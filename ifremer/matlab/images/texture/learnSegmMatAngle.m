function varargout = learnSegmMatAngle(I, A, masque, N)

% Maintenance Auto : try
    labels = unique(masque(masque ~= 0));
    CmatModeleAngle = cell(length(labels), N);
    
    hw = create_waitbar(Lang('Apprentissage en cours', 'Learning signatures'), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        [Ia, Ja] = find(A == k);
        
        minIa = min(Ia);
        maxIa = max(Ia);
        
        minJa = min(Ja);
        maxJa = max(Ja);
        
        subIa = minIa:maxIa;
        subJa = minJa:maxJa;
        II = I(subIa, subJa);
        MM = double(masque(subIa, subJa));
        CmatModele = learnSegmMatMasque(II, MM, labels);
        
        CmatModeleAngle(:,k) = CmatModele;
    end
    my_close(hw, 'MsgEnd')
    
     if nargout == 0
        figure;
        k = 1;
        nbLabels = length(labels);
        for iModel=1:nbLabels
            for iAngle=1:N
                subplot(nbLabels, N, k);
                imagesc(CmatModeleAngle{iModel,iAngle}); axis xy; axis off;
                k = k + 1;
            end
        end
    else
        varargout{1} = CmatModeleAngle;
        varargout{2} = labels;
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err('texture', 'learnSegmMatAngle', lasterr)
% Maintenance Auto : end
