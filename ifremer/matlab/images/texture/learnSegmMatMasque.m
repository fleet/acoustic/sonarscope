function CmatModele = learnSegmMatMasque(I, masque, labels)

nbLabels = length(labels);
Ref = cell(nbLabels, 1);
for i=1:nbLabels
    [Im, Jm] = find(masque == labels(i));
    subI = min(Im):max(Im);
    subJ = min(Jm):max(Jm);
    if ~isempty(subI) && ~isempty(subJ)
        Ref{i} = I(subI,subJ ) .* double(masque(subI,subJ) == labels(i));
    end
end

CmatModele = learnSegmMat(Ref, 1, 0);
