% Calcul de la matrice de confusion d'une segmentation / verite terrain
%
% Syntax
%   [N, P, V] = matConfusion(verite, segm)
% 
% Input Arguments 
%   verite : Verite
%   segm   : Segmentation
%
% Output Arguments 
%   [] : Auto-plot activation
%   N  : Matrice de confusion
%   P  : Pourcentage de reussite
%   V  : Ensemble des valeurs prises par "verite"
%
% Remarks : La  taille des 2 matrices peut etre differente
%
% Examples
%   segm = [1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2
%           1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2
%           1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2
%           1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2
%           1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2
%           1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2
%           1  2  1  1  1  1  1  1  2  2  2  2  2  2  2  2
%           1  1  1  3  1  1  1  1  2  2  2  2  2  2  2  2
%           3  3  3  3  3  3  3  3  4  4  4  4  4  4  4  4
%           3  3  3  3  3  3  3  3  4  4  4  4  4  4  4  4
%           3  3  3  3  3  3  3  3  4  4  4  4  4  4  4  4
%           3  3  3  3  3  3  3  3  4  4  4  4  4  4  4  4
%           3  3  3  3  3  3  3  3  4  4  4  4  4  4  4  4
%           3  3  3  1  3  3  3  3  4  4  4  4  4  4  4  4
%           3  3  3  3  3  3  3  3  4  4  4  4  4  4  4  4
%           3  3  3  3  3  3  3  3  4  4  4  4  4  4  4  4];
%   matConfusion([1 2; 3 4], segm)
%   [N, P, V] = matConfusion([1 2; 3 4], segm)
%
% See also histo1D Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.1 2002/04/03 08:53:52 augustin Exp $
%-------------------------------------------------------------------------------

function [N, P, binsx] = matConfusion(verite, segm)

verite = stretchmat( verite, size(segm), 'nearest');
binsx  = unique(verite)';
N      = histo2D(verite, segm, binsx, binsx);

P = 100 * sum(diag(N)) / sum(N(:));

if nargout == 0
    figure; imagesc(N); axis xy; colorbar;
    xlabel('Reference');
    ylabel('Segmentation');

%     M = [0 binsx ; binsx' N];
end
