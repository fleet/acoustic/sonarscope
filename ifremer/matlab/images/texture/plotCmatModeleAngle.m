function plotCmatModeleAngle(CmatModeleAngle)

cb = 'eval(''axis(setdiff(findobj(gcf, ''''Type'''', ''''axe''''), gca), axis)'')';

fig = figure;
k = 1;
nbLabels = size(CmatModeleAngle,1);
N = size(CmatModeleAngle, 2);
for iModel=1:nbLabels
    for iAngle=1:N
        haxe(k) = subplot(nbLabels+1, N, k); %#ok<AGROW>
        imagesc(CmatModeleAngle{iModel,iAngle}); axis xy; axis off;
        k = k + 1;
    end
end
% coul = 'brgymkc';
coul = 'byckmrg';
for iAngle=1:N
    haxe(k) = subplot(nbLabels+1, N, k); 
    for iModel=1:nbLabels
        contour(CmatModeleAngle{iModel,iAngle}, 1, coul(iModel));
        axis([1 255 1 255]); axis off;
        hold on
    end
    k = k + 1;
end
zoom

set(findobj(fig, 'Type', 'axe'),   'ButtonDownFcn', cb)
set(findobj(fig, 'Type', 'image'), 'ButtonDownFcn', cb)
set(findobj(fig, 'Type', 'line'),  'ButtonDownFcn', cb)


fig = figure;
k = 1;
sqrN1 = ceil(sqrt(N));
sqrN2 = sqrN1 -1;
if (sqrN1*sqrN2) < N
    sqrN2 = sqrN1;
end
    
for iAngle=1:N
    haxe(k) = subplot(sqrN1, sqrN2, k); 
    for iModel=1:nbLabels
        contour(CmatModeleAngle{iModel,iAngle}, 1, coul(iModel));
        axis([1 255 1 255]);
        hold on
    end
    title(num2str(iAngle))
    k = k + 1;
end
zoom

set(findobj(fig, 'Type', 'axe'),   'ButtonDownFcn', cb)
set(findobj(fig, 'Type', 'image'), 'ButtonDownFcn', cb)
set(findobj(fig, 'Type', 'line'),  'ButtonDownFcn', cb)

figure;
k = 1;
for iModel=1:nbLabels
    for iAngle=1:N
        px = sum(CmatModeleAngle{iModel,iAngle});
        x = 0:(length(px)-1);
        courbeG(iAngle) = sum(x .* px); %#ok<AGROW>
        [~, ind] = max(spdiags(CmatModeleAngle{iModel,iAngle},1));
        courbeMax(iAngle) = ind; %#ok<AGROW>
    end
    plot(courbeG, coul(iModel)); grid on; hold on;
%     plot(courbeMax, coul(iModel)); grid on; hold on;
    plot(courbeMax, ['--' coul(iModel)]); grid on; hold on;
    str{k} = sprintf('Label %d Barycentre', iModel); %#ok
    k = k + 1;
    str{k} = sprintf('Label %d Max', iModel); %#ok
    k = k + 1;
end
legend(str)


