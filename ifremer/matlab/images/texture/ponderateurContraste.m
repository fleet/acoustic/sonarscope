function X = ponderateurContraste(N)

% N = floor(N/2);
% ValyDecale = -N:(N+1);
% pppp = ValyDecale' * ValyDecale;
% V = abs(ValyDecale);
% V2 = V .^ 2;
% % Vm1 = 1 ./ (1 + V);
% X = 1 ./ (1 + V2);

sub = (-N+1):0;
X = zeros(N,N,'single');
for k=1:N
    X(k,:) = sub + k;
end
X = X .* X;
