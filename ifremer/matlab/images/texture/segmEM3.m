% Segmentation par EQM
%
% Syntax
%   S = segmentation(this, Moyenne, EcartType, Ponder, Masque)
%
% Input Arguments
%   A : Classes d'apprentissage
%
% Examples
%
% See also cl_cooc cl_cooc/set Authors
% Authors : JMA
% VERSION  : $Id: segmEM.m,v 1.1 2002/11/25 16:54:08 augustin Exp augustin $
% ----------------------------------------------------------------------------

function Segm = segmEM3(tab, newdat, pcs,g ,mo,st,niap,nbcl,nbDir,nbDeplacements,nbPar,nbL,nbC,varargin) %#ok<INUSL>

g = g(:);

for i=1:nbL
    for j=1:nbC
        point=i*nbC+j;
        data=tab((1+(point-1)*nbDeplacements):point*nbDeplacements,:);

        avg = mo; %#ok<NASGU>
        moy=repmat(mo,nbDeplacements,1);
        centerx = (data - moy);

        for nbd=1:nbDeplacements
            C=centerx(nbd,:);
            C=C(:);

            tb(i,j,nbd,1)=sum(C.*pcs(:,1)); %#ok<AGROW>
            tb(i,j,nbd,2)=sum(C.*pcs(:,2)); %#ok<AGROW>
        end
        m=tb(i,j,:,:);
        n=squeeze(m);
        app=newdat(:,1:2);

        class=classify(n,app,g,'mahalanobis');
        %'mahalanobis',
        Segm.Image(i,j) = round(sum(class)/nbDeplacements);
        choix = round(sum(class)/nbDeplacements);
        compteur=0;
        for u=1:nbDeplacements
            if class(u)==choix
                compteur=compteur+1;
            end
        end
    end
    Segm.probas(i,j,:) = round(compteur/nbDeplacements);

end
figure;
title(' Repartition des imagettes dans la nouvelle base');
hold on
grid on

for i=1:nbL
    for j=1:nbC
        for k=1:nbDeplacements
            plot(tb(i,j,k,1),tb(i,j,k,2),'.g')
        end
    end
end
for i=1:nbcl

    for j=1+(i-1)*(niap/nbcl)*nbDeplacements:i*(niap/nbcl)*nbDeplacements
        if i==1

            plot(app(j,1),app(j,2),'Pr-')
        end
        if i==2
            plot(app(j,1),app(j,2),'Pk-')
        end
    end
end

hold off
grid off


%       hold on
%       for i=1:2*nbDeplacements
%                  plot(app(i,1),app(i,2),'*r')
%              end
%    hold off;
