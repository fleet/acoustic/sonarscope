% nomFic1 = getNomFicDatabase('textureSonar02.png');
% nomFic2 = getNomFicDatabase('textureSonar01.png');
% nomFic3 = getNomFicDatabase('textureSonar03.png');
% nomFic4 = getNomFicDatabase('textureSonar04.png');
% I1 = imread(nomFic1);
% I2 = imread(nomFic2);
% I3 = imread(nomFic3);
% I4 = imread(nomFic4);
% I14 = [I1 I2; I3 I4];
% figure; imagesc(I14); colormap(gray(256))
% 
% nomFic5 = getNomFicDatabase('textureSonarMean100Std16_01.png');
% nomFic6 = getNomFicDatabase('textureSonarMean100Std16_02.png');
% nomFic7 = getNomFicDatabase('textureSonarMean100Std16_03.png');
% nomFic8 = getNomFicDatabase('textureSonarMean100Std16_04.png');
% I5 = imread(nomFic5);
% I6 = imread(nomFic6);
% I7 = imread(nomFic7);
% I8 = imread(nomFic8);
% I58 = [I5 I6; I7 I8];
% figure; imagesc(I58); colormap(gray(256))
% 
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 0)
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 0)
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 0)
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 0)
%
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0)
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 0)
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 0)
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 0)
%
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [32 32], 1)
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [16 16], 1)
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 8  8], 1)
% segmMat({I1, I2, I3, I4}, I14, 1, 0, [ 4  4], 1)
%
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 1)
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [16 16], 1)
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 8  8], 1)
% segmMat({I5, I6, I7, I8}, I58, 1, 0, [ 4  4], 1)
%
% [segmKul, distKul, segmEqm, distEqm] = segmMat({I5, I6, I7, I8}, I58, 1, 0, [32 32], 0);
% figure; imagesc(segmEqm); colorbar; title('par EQM')
% figure; imagesc(segmKul); colorbar; title('par Kullback')
% figure; imagesc(distKul(:,:,1)); colorbar; title('distKul')
%
% [N, P] = matConfusion([1 2; 3 4], segmEqm)

function varargout = segmMat(CmatModele, I, dX, dY, W, Filtre)

warning('off')

nbl = ceil(size(I,1) / W(1));
nbc = ceil(size(I,2) / W(2));
distEqm = zeros(nbl, nbc, length(CmatModele));
distKul = zeros(nbl, nbc, length(CmatModele));
ih = 0;
N = nbl * nbc;
hw = create_waitbar('Processing', 'N', N);
for i=1:nbl
    subl = (1+(i-1)*W(1)):min(i*W(1), size(I,1));
    for j=1:nbc
        my_waitbar(ih, N, hw);
        ih = ih + 1;
        subc = (1+(j-1)*W(2)):min(j*W(2), size(I,2));

        Imagette = I(subl,subc);
        if sum(Imagette(:)) == 0
            distEqm(i, j, :) = NaN;
            distKul(i, j, :) = NaN;
        else

            if Filtre
                Cmat = coocMat(Imagette, dX, dY, 'Filtre');
            else
                Cmat = coocMat(Imagette, dX, dY);
            end
            Cmat = single(Cmat);

            for k=1:length(CmatModele)
                CModele = single(CmatModele{k});
                distEqm(i, j, k) = sum(sum((CModele - Cmat) .^ 2));
                distKul(i, j, k) = kullback(Cmat, CModele);
            end
        end
    end
end
my_close(hw, 'MsgEnd')

[distKulMin, segmKul] = min(distKul, [], 3);
[distEqmMin, segmEqm] = min(distEqm, [], 3);

sub = isnan(distKulMin(:,:,1));
segmKul(sub) = NaN;
segmEqm(sub) = NaN;

if nargout == 0
    for k=1:length(CmatModele)
        %         histo1D(distEqm(:,:,k));
        %         histo1D(distKul(:,:,k));
        figure;
        subplot(2,1,1); imagesc(distEqm(:,:,k)); colorbar
        subplot(2,1,2); imagesc(distKul(:,:,k)); colorbar
    end

    str = sprintf('dX=%d  dY=%d  W=%s  Filtre=%d', dX, dY, mat2str(W), Filtre);
    figure;
    subplot(2,1,1); imagesc(segmEqm); colorbar; title([str '  par EQM'])
    subplot(2,1,2); imagesc(segmKul); colorbar; title([str '  par Kullback'])
else
    varargout{1} = segmKul;
    varargout{2} = distKul;
    varargout{3} = segmEqm;
    varargout{4} = distEqm;
end
