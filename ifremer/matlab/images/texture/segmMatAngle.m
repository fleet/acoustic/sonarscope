function [segm, dist, dist12] = segmMatAngle(I, W, indAngleImagette, Texture, nbLI, nbCI, iClique, masqueImagetteValide, ...
    iC, iL, SeuilConfusion, SeuilRejet, FilterCoocMat) %#ok<INUSL>

CmatModeleAngle = Texture.CmatModeleAngle;
Clique          = Texture.cliques(iClique,:);
NbNiveaux       = Texture.binsImage.NbNiveaux;

dX = Clique(1);
dY = Clique(2);

nbFacies = length(CmatModeleAngle);
listeFaciesPossibles = [];
for iFacies=1:nbFacies
    CmatFacies = CmatModeleAngle{iFacies};
    if ~isempty(CmatFacies)
        listeFaciesPossibles(end+1) = iFacies; %#ok<AGROW>
    end
end
nbFaciesPossibles = length(listeFaciesPossibles);
if isempty(listeFaciesPossibles)
    disp('beurk')
end

% sz = size(Imagettes);

nbImagettes = length(indAngleImagette);
segm        = zeros(nbLI, nbCI, 'uint8');
dist        = NaN(nbLI, nbCI, nbFacies, 'single');
dist12      = NaN(nbLI, nbCI, 'single');

iLPre = -1;
[nbL, nbC] = size(I);
hw = create_waitbar(Lang('Etape 2 de la segmentation', 'Segmentation processing step 2'), 'N', nbImagettes);
for i=1:nbImagettes
	my_waitbar(i, nbImagettes, hw);
    
    subc = (1:W(2)) + (iC(i)-1) * W(2);
    subc(subc > nbC) = [];

    subl = (1:W(1)) + (iL(i)-1) * W(1);
    subl(subl > nbL) = [];

%     Imagette = I(subl, subc);

% Message pour GLU : au cas o� l'image est un cl_memmapfile pointant sur
% une image de type ErMapper on �vite de relire x fois le m�me paquet de
% lignes en passant par un buffer interm�diaire.
    if iL(i) ~= iLPre
        ImageTemp = I(subl, :);
        iLPre = iL(i);
    end
    Imagette = ImageTemp(:, subc);
    
%     if sum(isnan(Imagette(:))) == numel(Imagette)
%         continue
%     end
    if sum(Imagette(:)) == 0
        continue
    end
    
    
    iAngle = indAngleImagette(i);

    if (iAngle == 0) || isnan(iAngle)
        continue
    end

    if sum(Imagette(:)) == 0
        dist(iL(i),iC(i),:) = NaN;
    else

        if FilterCoocMat
            Cmat = coocMatNbNiveauxFiltre(Imagette, dX, dY, NbNiveaux);
        else
            Cmat = coocMatNbNiveaux(Imagette, dX, dY, NbNiveaux);
        end

        for k=1:nbFaciesPossibles
            iFacies = listeFaciesPossibles(k);
            CmatFacies = CmatModeleAngle{iFacies}(iClique,iAngle);
            CmatFacies = CmatFacies{1};
            if isempty(CmatFacies)
                dist(iL(i),iC(i),iFacies) = Inf;
            else
                dist(iL(i),iC(i),iFacies) = kullback(Cmat, CmatFacies);
            end
        end
    end
%     [d, s] = min(dist(iL(i),iC(i),:));
%     segm(iL(i),iC(i)) = s;
    
    [d, ordre] = sort(squeeze(dist(iL(i),iC(i),:)));
    separabilite = abs(d(1)-d(2));
    if d(1) == 0
        continue
    elseif d(1) > SeuilRejet
        segm(iL(i),iC(i)) = nbFaciesPossibles + 2;
    elseif separabilite < SeuilConfusion
        segm(iL(i),iC(i)) = nbFaciesPossibles + 1;
    else
        segm(iL(i),iC(i)) = ordre(1);
    end
    dist12(iL(i),iC(i)) = separabilite;
end
my_close(hw, 'MsgEnd');

dist = min(dist, [], 3);

