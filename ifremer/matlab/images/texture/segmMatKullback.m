function [segm, dist] = segmMatKullback(Imagettes, CmatModele, labels, nbLI, nbCI)

dX=1; dY=0;


nbLabels = length(labels);
nbImagettes = size(Imagettes, 3);
segm = NaN(nbLI, nbCI, 'single');
dist = NaN(nbLI, nbCI, nbLabels, 'single');
i = 0;
hw = create_waitbar('Segmentation en cours', 'N', nbImagettes);
for iC=1:nbCI
    for iL=1:nbLI
        i = i + 1;
        my_waitbar(i, nbImagettes, hw)
        I = Imagettes(:,:,i);
        %     drawnow
        %     imagesc(I, [0 255]); colorbar; colormap(gray(256));
        
        if sum(I(:)) == 0
            dist(iL,iC,:) = NaN;
        else
            
            Cmat = coocMat(I, dX, dY);
            
            for k=1:nbLabels
                dist(iL,iC,k) = kullback(Cmat, CmatModele{k});
            end
        end
        [d, s] = min(dist(iL,iC,:));
        if isnan(d)
            segm(iL,iC) = 0;
        else
            segm(iL,iC) = s;
        end
    end
end
my_close(hw, 'MsgEnd')
