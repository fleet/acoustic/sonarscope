function [segm, dist] = segmMatKullback2(Image, CmatModele, labels, W, Pas)

dX=1; dY=0;


nbLabels = length(labels);
nbCI = floor((size(Image,2) - W(1)) / Pas(1));
nbLI = floor((size(Image,1) - W(2)) / Pas(2));
nbImagettes = nbLI*nbCI;
segm = NaN(nbLI, nbCI, 'single');
dist = NaN(nbLI, nbCI, nbLabels, 'single');
subc = 0:(W(2)-1);
subl = 0:(W(2)-1);
i = 0;
hw = create_waitbar('Segmentation en cours', 'N', nbImagettes);
for iC=1:nbCI
    for iL=1:nbLI
        i = i + 1;
        my_waitbar(i, nbImagettes, hw)
        
        ldeb = 1 + (iL-1) * Pas(2);
        cdeb = 1 + (iC-1) * Pas(1);
        I = Image(subl+ldeb, subc+cdeb);
        
        if sum(I(:)) == 0
            dist(iL,iC,:) = NaN;
        else
            
            Cmat = coocMat(I, dX, dY);
            
            for k=1:nbLabels
                dist(iL,iC,k) = kullback(Cmat, CmatModele{k});
            end
            
            [d, s] = min(dist(iL,iC,:));
            if isnan(d)
                segm(iL,iC) = 0;
            else
                segm(iL,iC) = s;
            end
        end
    end
end
my_close(hw, 'MsgEnd');
