function [segm_1, dist_1] = segmMatMasque(I, masque, CmatModeleAngle, W) %#ok<INUSL>

[segm_1, dist_1] = segmMat(CmatModeleAngle, I, 1, 0, W, 0);
