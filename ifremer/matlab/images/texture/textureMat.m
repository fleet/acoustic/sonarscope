% Calcul d'une matrice de co-occurrence
%
% Syntax
%   params = textureMat(Image, Tx, Ty, ...)
% 
% Input Arguments 
%   Image : Image a analyser (les 0 sont consideres comme des NaN)
%   Tx    : Translation(s) horizontale
%   Ty    : Translation(s) verticale
% 
% Name-Value Pair Arguments 
%   Log    : Flag pour afficher la matrice en prenant le log des valeurs
%            (utile uniquement en "Auto-plot"
%   Filtre : Flag pour calculer la matrice filtree
%
% Output Arguments 
%   []     : Auto-plot activation
%   params : Parametres de texture.
%
% Remarks : La valeur 0 de Image est considérée comme "NaN".
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%
%   figure; imagesc(img); colorbar; colormap(gray(256));
%   histo(double(img), 1:255)
%
%   textureMat(img, 1, 0);
%
%   textureMat(img(1:32,1:32), 1, 0);
%
%   params = textureMat(img, 1, 0);
%
% See also coocInitImage textureMatMovie cl_cooc Authors
% Authors : FM + JMA + EM
%--------------------------------------------------------------------------

function varargout = textureMat(Image, Tx, Ty, varargin)

% -----------------------
% Récuperation des options

[varargin, Log]    = getFlag(varargin, 'Log');
[varargin, Filtre] = getFlag(varargin, 'Filtre'); %#ok<ASGLU>

% ------------------------------------------
% Test de conformité de la taille de l'image

if ismatric(Image) ~= 2
    my_warndlg('images:texture:textureMat L''image doit etre de dimension 2 ...', 0);
    return
end

% -------------------------------------------------------------------
% n'est le nombre de ligne de la matrice , m est le nombre de colonne

Image = double(Image);
% [n, m] = size(Image);


% ----------------------------------------------------------------
% on initialise la matrice de co-occurrence. C'est une matrice N*N

Cmat        = zeros(256, 256);
CmatDecale  = zeros(256, 511);


% ------------------------------------------------
% On peut meme utiliser des translations negatives

if (Tx >= 0) && (Ty >= 0)                    % Tx =0 && Ty >0 => dep vert
    I1 = Image(1:end-Ty,1:end-Tx);
    I2 = Image(1+Ty:end,1+Tx:end);
elseif (Tx < 0) && (Ty >= 0)
    I1 = Image(1:end-Ty,end:-1:1-Tx);
    I2 = Image(1+Ty:end,end+Tx:-1:1);
elseif (Tx >= 0) && (Ty < 0)
    I1 = Image(end:-1:1-Ty,1:end-Tx);
    I2 = Image(end+Ty:-1:1,1+Tx:end);
else
    I1 = Image(end:-1:1-Ty,end:-1:1-Tx);
    I2 = Image(end+Ty:-1:1,end+Tx:-1:1);
end

I1 = I1(:);
I2 = I2(:);

sub = (I1~=0) & (I2~=0);
I1 = I1(sub);
I2 = I2(sub);
nbtr = length(I1);
if nbtr == 0
    varargout{1} = Cmat;
    varargout{2} = nbtr;
    return
end

% ------------------------------------
% Calcul de la matrice de cooccurrence

increment = 1 / nbtr;
NbPix = length(I1);
for i=1:NbPix
    x = I1(i);
    y = I2(i);
    Cmat(x,y) = Cmat(x,y) + increment;
end


% Trouvaille de Ronan pour resoudre la normalisation de l'entropie,
% de l'homogeneite et de l'uniformite, on
% filtre la matrice de cooccurrence au prorata
% de la taille de l'imagette analysee.
% A EVALUER

if Filtre
    %         sig = 80 / sqrt(prod(size(Image)));
    %         largeurFiltre = 1 + 2 * floor(2*sig);
    largeurFiltre = 7; sig=2;
    h = fspecial('gaussian', largeurFiltre, sig);
    Cmat = imfilter(Cmat, h);
end

% ------------------------------------------------
% Generation de la matrice de cooccurrence decalee
% Ceci permet de ramener la diagonale de la matrice de cooccurrence
% une verticale. Les formes 2D utilises pour les ponderations
% deviennent des formes 1D. Le calcul est don plus simple pour beaucoup
% de parametres.

sub = 256:511;
for i=1:256
    CmatDecale(i,sub) = Cmat(i,:);
    sub = sub - 1;
end

% ----------------------------------------------------
% Calcul de la projection suivant la diagonale de Cmat

PyDecale = sum(CmatDecale);

% -----------------------------------------------------------------
% Calcul des ponderateurs utiles au calcul des parametres texturaux

ValyDecale = -255:255;
V = abs(ValyDecale);
V2 = V .^ 2;
% Vm1 = 1 ./ (1 + V);
Vm2 = 1 ./ (1 + V2);

% -----------------------
% Calcul des distributions

Px  = sum(Cmat, 2);
Py  = sum(Cmat);

% ---------------------------------------------------
% Calcul de la moyenne ponderee des lignes de la GLCM

Valx = 1:256;
nux = sum(Valx' .* Px);

% -----------------------------------------------------
% Calcul de la moyenne ponderee des colonnes de la GLCM

nuy = sum(Valx .* Py);

% -------------------
% variance des lignes

varx = sum(((Valx - nux) .^2 )' .* Px);
% sigmaPx = sqrt(varx);

% ---------------------
% variance des colonnes

vary = sum(((Valx - nuy) .^2 ) .* Py);
% sigmaPy = sqrt(vary);

% -------------------------------
% Calcul des parametres texturaux

%     param = zeros(1,22);
param = zeros(1,10);

% Contraste
param(1) = sum(PyDecale .* V2) / 65025; % 65025 = 255^2;

% Directivite
param(2) = PyDecale(256);

% Entropie
C = Cmat(Cmat ~= 0);
x = (C .* log(C)) ;
param(3) = -sum(sum(x))./ log(2);

% Homogeneite locale
param(4) = sum(PyDecale .* Vm2);

% Uniformite
param(5) = sum(sum(CmatDecale(:,256-1:256+1) .^ 2));

% Correlation
X = ((Valx - nux)' * (Valx - nuy)) / (sqrt(varx) * sqrt(vary));
param(6) = sum(X(:) .* Cmat(:));

% Homogeneite
param(7) = sum(Cmat(:) .^ 2);

% coocDirectiviteJMA
param(8) = sum(PyDecale(256-4:256+4));

% meanImage
param(9) = (nux + nuy) / 2;

% stdImage
param(10) = sqrt((varx + vary) / 2);

%     % Dissimilarite
%     param(11) = 0;  % sum(PyDecale .* V) / 65025; % 65025 = 255^2;
%
%     % coocSDC
%     %sigmay = std(Cmat);
%     param(12) = 0;  % var(sigmay,1);
%
%     % coocRegularity
%     param(13) = 0;
%
%     % coocSigmaPx
%     param(14) = 0;  %sigmaPx;
%
%     % coocSigmaPy
%     param(15) = 0;  %;  sigmaPy;
%
%     % coocPeriodicity
%     param(16) = 0;  % sigmaPx / sigmaPy;
%
%     % skewnessImage
%     param(17) = 0;  %skewness(Image(:));
%
%     % kurtosisImage
%     param(18) = 0;  %kurtosis(Image(:));
%
%     % coocSkewnessPx
%     % skewnessPx = sum(((Valx' - nux) .^ 3) .* Px);
%     param(19) = 0;  %skewnessPx / (sigmaPx .^ 3);
%
%     % coocSkewnessPy
%     % skewnessPy = sum(((Valx - nuy) .^ 3) .* Py);
%     param(20) = 0;  %skewnessPy / (sigmaPy .^ 3);
%
%     % coocKurtosisPx
%     % kurtosisPx = sum(((Valx' - nux) .^ 4) .* Px);
%     param(21) = 0;  %kurtosisPx / (varx .^ 2);
%
%     % coocKurtosisPy
%     % kurtosisPy = sum(((Valx - nuy) .^ 4) .* Py);
%     param(22) = 0;  %kurtosisPy / (vary .^ 2);
%
%     % localHomo1
% %     param(23) = sum(PyDecale .* Vm1);
%

% -----------------
% Par ici la sortie

if nargout == 0
    figure
    if ~Log
        titre = sprintf('%s. Dep : [%d %d]', Lang('Matrice de cooccurence','Co-occurence matrix'), Tx, Ty);
        imagesc(Cmat(:,:,1)); colorbar; title(titre);
        axis equal; axis tight; pixval on;
    else
        Cmat(Cmat == 0) = NaN;
        titre = sprintf('%s (log10). Dep : [%d %d]', Lang('Matrice de cooccurence','Co-occurence matrix'), Tx, Ty);
        pcolor(log10(Cmat(:,:,1))); colorbar; title(titre);
        shading flat; axis ij;
    end
else
    varargout{1} = param;
end
