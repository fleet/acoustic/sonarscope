% Traitement generique d'un signal
%
% Syntax
%   [val, flag, Process, FlagSamples] = traiter_signal(x, val, nomSignal, ...)
%
% Input Arguments
%   x         : Abscisses du signal
%   y         : Signal
%   nomSignal : Nom du signal
%
% Name-Value Pair Arguments
%   ValNaN  : Valeur utilisee pour les valeurs manquantes
%   Process : 1=Edition le signal, 2=Histogramme et statistiques,
%             3=Edition de l'histogramme, '4=Mise a NaN, 5=Autocorrelation
%             6=Edition le signal et interpolation, 7=Filtrage
%
% Output Arguments
%   val        : Signal traite
%   flag : 1 si op�ration reussie
%   Process    : Idem input
%
% Examples
%   x = 0:pi/360:5*pi;
%   y = sin(x) + 0.1 * rand(size(x));
%   y = traiter_signal(x, y, 'test');
%   y = traiter_signal(x, y, 'test', 'Process', 1);
%   y = traiter_signal(x, y, 'test', 'Process', 2);
%   y = traiter_signal(x, y, 'test', 'Process', 3);
%   y = traiter_signal(x, y, 'test', 'Process', 4);
%   y = traiter_signal(x, y, 'test', 'Process', 5);
%
% See also clc_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [val, flag, Process, FlagSamples, A, B] = traiter_signal(x, val, nomSignal, varargin)

[varargin, Item]            = getPropertyValue(varargin, 'Item',            1);
[varargin, Background]      = getPropertyValue(varargin, 'Background',      []);
[varargin, xBackground]     = getPropertyValue(varargin, 'xBackground',     []);
[varargin, yBackground]     = getPropertyValue(varargin, 'yBackground',     []);
% [varargin, Unit]          = getPropertyValue(varargin, 'Unit',            []);
[varargin, repImport]       = getPropertyValue(varargin, 'repImport',       my_tempdir);
[varargin, repExport]       = getPropertyValue(varargin, 'repExport',       my_tempdir);
[varargin, OtherProfiles]   = getPropertyValue(varargin, 'OtherProfiles',   []);
[varargin, colormapData]    = getPropertyValue(varargin, 'colormapData',    []);
[varargin, VideoBackground] = getPropertyValue(varargin, 'VideoBackground', []);
[varargin, CLimBackground]  = getPropertyValue(varargin, 'CLimBackground',  []);
[varargin, ImageName]       = getPropertyValue(varargin, 'Name',       []);

[varargin, QuestionAxeReverse] = getFlag(varargin, 'QuestionAxeReverse');

if isempty(colormapData)
    colormapData = gray(256);
end

%% Valeur representant les non-valeurs

[varargin, ValNaN]    = getPropertyValue(varargin, 'ValNaN',  NaN);
[varargin, flag_time] = getPropertyValue(varargin, 'cl_time', 0);

Flag = [];
FlagSamples = [];
A = [];
B = [];
strProcess1 = {'Edition'
    'Histogram & statistiques'
    'Edition de l''histogramme'
    'Mise � NaN'
    'Auto-correlation'
    'Edition + Interpolation'
    'Filtre'
    'Sauver'
    'Charger'
    'Trac� dans une figure'
    'Copie de l''axe dans une figure'
    'Ajouter un offset sur le signal'
    'Multiplier le signal par un coefficient'
    'Supression de spikes'};
strProcess2 = {'Edit'
    'Histogram & statistics'
    'Edit Histogram'
    'Put NaN'
    'Self Correlation'
    'Edit + Interpolation'
    'Filter'
    'Save'
    'Load'
    'Plot in a figure'
    'Extract axe in a new figure'
    'Add an offset on signal'
    'Apply a scale factor on signal'
    'Spike filter'};
strProcess = Lang(strProcess1, strProcess2);

[varargin, Process] = getPropertyValue(varargin, 'Process', []); %#ok<ASGLU>

if isempty(Process)
    str1 = 'Traitement d''une courbe';
    str2 = 'Processing a curve';
    [Process, flag] = my_listdlg(Lang(str1,str2), strProcess, 'InitialValue', 10, 'SelectionMode', 'Single');
    if ~flag
        val = [];
        return
    end
else
    if (Process >= 1) && (Process <= length(strProcess))
        flag = 1;
    else
        val = [];
        flag = 0;
        return
    end
end

if iscell(val)
    val = val{Item};
end
if iscell(x)
    x = x{Item};
end

% sz  = size(val);

if isa(x, 'datetime')
    x = datenum(x-x(1)) * (24*3600);
    fprintf('Abscissa is converted in seconds\n')
end

if ~isnumeric(x)
    str1 = 'La donn�e "x" n''est pas num�rique';
    str2 = 'X data is not numeric';
    my_warndlg(Lang(str1,str2), 1);
    return
end

x   = double(x);
val = double(val);

switch Process
    case {1, 6} % Edition du signal
        if strcmp(nomSignal, 'Height') && ~isempty(Background)
            val = -abs(val);
            yBackground = -yBackground;
            imageData = ClImageData('cData', Background, 'xData', xBackground, 'yData', yBackground, ...
                'colormapData', colormapData, 'cLim', CLimBackground, 'videoInverse', VideoBackground);
        else
            imageData = ClImageData.empty();
        end
        
        valBefore  = val;
 
        xSample = XSample('name', 'Pings',  'data', x);
        ySample = YSample('name', nomSignal, 'data', val, 'lineWidth', 1, 'color', 'r');
        signal = ClSignal('xSample', xSample, 'ySample', ySample, 'isVertical', false);
        
        a = SignalDialog(signal, 'Title', 'Edit signal', 'imageDataList', imageData, 'cleanInterpolation', Process == 6);
        a.openDialog();
        
        % Set modified data in val
        val = ySample.data;
        
        if  isequal(val, valBefore)
            flag = 0;
        else
            flag = 1;
        end
     
    case 2 % Histogramme et statistiques
        histo1D(val, 'ValNaN', ValNaN);
        flag = 0;
        
    case 3 % Edition de l'histogramme
        [N, bins, ValBins, SubBins] = histo1D(val, 'ValNaN', ValNaN); %#ok<ASGLU>
        sub = getFlaggedBinsOfAnHistogram(bins, N);
        
        % -----------------------------
        % On r�cup�re les modifications
        
        flag = 0;
        for k=1:length(sub)
            subx = SubBins{sub(k)};
            val(subx) = ValNaN;
            flag = 1;
        end
        
    case 4 % Mise a NaN
        val(:) = ValNaN;
        flag = 1;
        
    case 5 % Autocorrelation
        if size(val, 1) == 1
            s = val;
        else
            s = mean(val);
        end
        
        resol = mean(diff(x));
        my_xcorr(s, 'nomSignal', nomSignal, 'resol', resol, 'cl_time', flag_time);
        flag = 0;
        
    case 7 % Filtrage
        OK = 0;
        while ~OK
            [flag, OrdreFiltre, Wc] = SaisieParamsButterworth(2, 0.1);
            if ~flag
                flag = 0;
                return
            end
            
            [B,A] = butter(OrdreFiltre, Wc);
            
            subNaN = find(isnan(val));
            subNonNaN = find(~isnan(val));
            val(subNaN) = interp1(x(subNonNaN), val(subNonNaN), x(subNaN), 'linear', 'extrap');
            
            if strcmp(nomSignal, 'Heading')
                % Filtrage d'un cap en degres
                valFiltree = val * (pi/180);
                valFiltree = unwrap(valFiltree);
                valFiltree1 = filtfilt(B, A, double(valFiltree));
                valFiltree1 = valFiltree1 * (180/pi);
                valFiltree1 = mod(valFiltree1+360, 360);
            else
                valFiltree1 = filtfilt(B, A, double(val));
            end
            
            fig = FigUtils.createSScFigure('Name', 'Destroy the figure when inspection is finished');
            plot(x, val, 'k'); grid on; hold on; plot(x, valFiltree1, 'b'); zoom on;
            uiwait(fig)
            
            [rep, flag] = my_questdlg(Lang('TODO', 'Was the blue curve correct ?'));
            if ~flag
                return
            end
            
            switch rep
                case 2  % 'NONE'
                case 1  % 'Blue'
                    OK = 1;
                    val(:) = valFiltree1;
            end
            %             delete(fig)
        end
        flag = 1;
        
    case 8  % Save
        str1 = 'TODO';
        str2 = 'If your intention is to correct the signal and re-import it with an external software be sure to export the file in "Full Extent" mode otherwise you would not be able to import it. Do you want to continue ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
        if rep == 1
            [flag, nomFic] = my_uiputfile('*.txt', 'Give a file name', fullfile(repExport, [nomSignal '.txt']));
            if ~flag
                return
            end
            fid = fopen(nomFic,'w+');
            fprintf(fid, 'x\t%s\n', nomSignal);
            for k=1:length(x)
                fprintf(fid, '%f\t%f\n', x(k), val(k));
            end
            fclose(fid);
        end
%         flag = 0;
        
    case 9  % Load
        PWD = pwd;
        cd(repImport)
        [flag, nomFic] = my_uigetfile({'*.txt';'*.*'}, 'Nom du fichier');% , fullfile(repImport, [nomSignal '.txt']));
        cd(PWD)
        if ~flag
            return
        end
        %             nomSignalLoaded = loadmat(nomFic, 'nomVar', 'nomSignal');
        %             valLoaded       = loadmat(nomFic, 'nomVar', 'val');
        %             xLoaded         = loadmat(nomFic, 'nomVar', 'x');
        
        fid = fopen(nomFic,'r');
        A = fgetl(fid);
        mots = strsplit(A);
        str = '';
        for k = 1:(length(mots)-1)
            str = [str mots{k+1} ' ']; %#ok
        end
        nomSignalLoaded = str(1:end-1);
        xLoaded = zeros(size(x));
        valLoaded = zeros(size(x));
        for k=1:length(x)
            A = fgetl(fid);
            mots = strsplit(A);
            xLoaded(k)   = str2num(mots{1});%#ok
            valLoaded(k) = str2num(mots{2});%#ok
        end
        fclose(fid);
        
        if ~strcmp(nomSignalLoaded, nomSignal)
            fprintf('The names of sigals differ. Select the signal %s and try again.\n', nomSignal);
            flag = 0;
            return
        end
        
        if ~isequal(length(val), length(valLoaded))
            my_warndlg('The imported signal and the resident signal have not the same length. Go in "Full Extent" mode and try again ?', 1);
            flag = 0;
            return
        end
        
        if ~isequal(x, xLoaded)
            my_warndlg('The imported signal and the resident signal have not the same length. Go in "Full Extent" mode and try again ?', 1);
            flag = 0;
            return
        end
        
        val = valLoaded;
        flag = 1;
        
    case 10 % plot
        %         fig = gcf;
        if isempty(OtherProfiles)
            choixOtherLaters = [];
        else
            str1 = 'Layers synchronis�s';
            str2 = 'Synchronized layers';
            CurrentView = find([OtherProfiles.CurrentView]);
            [choixOtherLaters, flag] = my_listdlgMultiple(Lang(str1,str2), {OtherProfiles.nomLayer}, 'InitialValue', CurrentView);
            if ~flag || isempty(choixOtherLaters)
                return
            end
        end
        
        if QuestionAxeReverse
            [rep, flag] = my_listdlg('Orientation of the figure', {'Normal'; 'Rotated'}, 'InitialValue', 1);
            if ~flag
                return
            end
            if any(rep == 1)
                FigUtils.createSScFigure;
                %                 h(1) = subplot(1+length(choixOtherLaters), 1, 1);
                %                 PlotUtils.createSScPlot(x, val);
                %                 grid on;
                %                 title(Titre, 'Interpreter', 'none');
                %                 axis tight;
                
                Time = [];
                for k=1:length(OtherProfiles)
                    if strcmp(OtherProfiles(k).nomLayer, 'Time')
                        Time = OtherProfiles(k).yLayer;
                    end
                end
                
                for k=1:length(choixOtherLaters)
                    S = OtherProfiles(choixOtherLaters(k));
                    h(k) = subplot(length(choixOtherLaters), 1, k); %#ok<AGROW>
                    
                    hc = PlotUtils.createSScPlot(S.xLayer, S.yLayer);
                    set(hc, 'UserData', Time);
                    grid on;
                    
                    if isempty(S.xUnit)
                        xLabel = S.xName;
                    else
                        xLabel = sprintf('%s (%s)', S.xName, S.xUnit);
                    end
                    xlabel(xLabel);
                    
                    if isempty(S.Unit)
                        Titre = S.nomLayer;
                    else
                        Titre = [S.nomLayer ' (' S.Unit ')'];
                    end
                    title(Titre, 'Interpreter', 'none');
                    axis tight;
                end
                linkaxes(h,'x')
            end
            if any(rep == 2)
                FigUtils.createSScFigure;
                %                 h(1) = subplot(1, 1+length(choixOtherLaters), 1);
                %                 PlotUtils.createSScPlot(val, x);
                %                 grid on;
                %                 title(Titre, 'Interpreter', 'none');
                %                 axis tight;
                %                 set(gca, 'XDir', 'Reverse')
                
                for k=1:length(choixOtherLaters)
                    h(k) = subplot(1, length(choixOtherLaters), k);
                    PlotUtils.createSScPlot(OtherProfiles(choixOtherLaters(k)).yLayer, OtherProfiles(choixOtherLaters(k)).xLayer);
                    grid on;
                    if isempty(OtherProfiles(choixOtherLaters(k)).Unit)
                        Titre = OtherProfiles(choixOtherLaters(k)).nomLayer;
                    else
                        Titre = [OtherProfiles(choixOtherLaters(k)).nomLayer ' (' OtherProfiles(choixOtherLaters(k)).Unit ')'];
                    end
                    ylabel(Titre, 'Interpreter', 'none');
                    axis tight;
                    set(gca, 'XDir', 'Reverse')
                end
                linkaxes(h,'y')
            end
        else
            FigUtils.createSScFigure;
            %             h(1) = subplot(1+length(choixOtherLaters), 1, 1);
            %             PlotUtils.createSScPlot(x, val);
            %             grid on;
            %             title(Titre, 'Interpreter', 'none');
            %             axis tight;
            for k=1:length(choixOtherLaters)
                h(k) = subplot(length(choixOtherLaters), 1, k); %#ok<AGROW>
                S = OtherProfiles(choixOtherLaters(k));
                PlotUtils.createSScPlot(S.xLayer, S.yLayer);
                grid on;
                
                if isempty(S.xUnit)
                    xLabel = S.xName;
                else
                    xLabel = sprintf('%s (%s)', S.xName, S.xUnit);
                end
                xlabel(xLabel);
                
                
                if isempty(S.Unit)
                    Titre = S.nomLayer;
                else
                    Titre = [S.nomLayer ' (' S.Unit ')'];
                end
                title(Titre, 'Interpreter', 'none');
                axis tight;
            end
            linkaxes(h,'x')
        end
        %         FigUtils.createSScFigure(fig)
        flag = 0; % Pour ne rien faire apr�s
        
    case 11 % Mise a NaN
        GCA = gca;
        hCourbes = findobj(GCA, 'Type', 'line');
        fig = FigUtils.createSScFigure;
        for k=1:length(hCourbes)
            cloneCourbe(hCourbes(k), 'Fig', fig)
        end
        %         FigUtils.createSScFigure(fig)
        flag = 0; % Pour ne rien faire apr�s
        
    case 12 % Add an offset on value
        str1 = 'Ajout d''un offset au signal';
        str2 = 'Add an offset on signal';
        [flag, offset] = inputOneParametre(Lang(str1,str2), 'Scale Factor', 'Offset', 0);
        if ~flag
            flag = 0;
            return
        end
        val = val + offset;
        flag = 1;
        
    case 13 % Apply a scale factor on signal
        str1 = 'Multiplication du signal par un coefficient';
        str2 = 'Apply a scale factor on signal';
        [flag, scaleFactor] = inputOneParametre(Lang(str1,str2), 'Scale Factor', 'Value', 1);
        if ~flag
            flag = 0;
            return
        end
        val = val * scaleFactor;
        flag = 1;
        
    case 14 % Spike filter
        val = signal_spike_filter(val, nomSignal);
        flag = 1;
        
end

FlagSamples = ~Flag;
if (Process == 6) && flag
    subValNaN = find(~Flag');
    if any(subValNaN)
        subVal = find(Flag');
        if ~isempty(subVal)
            x1 = x(subVal);
            y1 = val(subVal);
            x2 = x(subValNaN);
            sub1 = ~isnan(y1);
            y2 = interp1(x1(sub1), y1(sub1), x2, 'linear', 'extrap');
            val(subValNaN) = y2;
        end
    end
end
