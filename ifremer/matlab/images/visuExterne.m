% Visualisation d'un fichier image par un visualisateur externe
%
% Syntax
%   visuExterne(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier image
%
% Remarks : Cette fonction utilise un visualisateur qui doit etre declare
%           dans le fichier startup
%
% Examples
%   nomFic = getNomFicDatabase('lena.tif');
%   visuExterne(nomFic)
%
% See also cl_image/export Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ViewerSelectionne = visuExterne(nomFic, varargin)

[varargin, ViewerSelectionne] = getPropertyValue(varargin, 'ViewerSelectionne', []); %#ok<ASGLU>

global VisualisateurImageExterne %#ok<GVMIS> 
global IfrTbx %#ok<GVMIS>
global IfrTbxResources %#ok<GVMIS>
if isempty(VisualisateurImageExterne)
    nomFicIni = fullfile(IfrTbxResources, 'Resources.ini');
    %verifNameLength(nomFicIni)
    if exist(nomFicIni, 'file')
        loadVariablesEnv;
    end
end

if ischar(nomFic)
    nomFic = {nomFic};
end

Viewer = VisualisateurImageExterne;
if ~iscell(Viewer)
    Viewer = {Viewer};
end

if ~isdeployed
    Viewer = [Viewer {'ViewImage'}];
end

switch length(Viewer)
    case 0
        winopen(nomFic{:})
        return
    case 1
        ViewerSelectionne = Viewer{1};
    otherwise
        if isempty(ViewerSelectionne)
            str1 = 'S�lection du visualisateur';
            str2 = 'Viewer selection';
            [k, validation] = my_listdlg(Lang(str1,str2), Viewer, 'SelectionMode', 'Single');
            if ~validation
                return
            end
            ViewerSelectionne = Viewer{k};
        end
end
if strcmp(ViewerSelectionne, 'ViewImage')
    ViewImage(nomFic{:})
    return
end

try
    if isdeployed
        for i=1:length(nomFic)
            cmd = sprintf('!winopen "%s" "%s"', ViewerSelectionne, nomFic{i});
            eval(cmd);
        end
    else
        for i=1:length(nomFic)
            % MAINTENANCE : FAIRE DU MENAGE ICI
            try
                try
                    cmd = sprintf('!%s\\Installation\\Viewers\\winopen.exe "%s" "%s"', IfrTbx, ViewerSelectionne, nomFic{i});
                    eval(cmd);
                catch %#ok<CTCH>
                    cmd = sprintf('!%s\\winopen.exe "%s" "%s"', IfrTbxResources, ViewerSelectionne, nomFic{i});
                    eval(cmd);
                end
            catch %#ok<CTCH>
                cmd = sprintf('!winopen.exe "%s" "%s"', ViewerSelectionne, nomFic{i});
                eval(cmd);
            end
            %winopen(nomFic{:})
        end
    end
    %         my_warndlg('Beware', 1);
    %         eval(cmd)
catch %#ok<CTCH>
end
