% Visualisation d'une suite d'images synchronisees en x et y
%
% Syntax
%   visuImages(I1, ...)
%
% Input Arguments
%   I1 : Premiere image
%   I2 : Deuxieme image
%   I3 : ...
%
% Examples
%   Z = Prismed;
%   visuImages(Z, -Z)
%	
% See also cli_image Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function visuImages(varargin)

b = cl_image('TagSynchroX', 'XY1', 'TagSynchroY', 'XY1', 'ColormapIndex', 3);

c = cli_image;

for k=1:length(varargin)
    d = set(b, 'Image', varargin{k}, 'Name', ['Image ' num2str(k)]);
    c = add_Image(c, d);
end

editobj(c);
