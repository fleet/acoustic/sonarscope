% Wallis filter (cf. M.-C. Mouchot - ENSTB). This creates an enhanced image
% where the mean value is forced towards a given value with a given
% wheight, same for std
%
% Syntax
%   y = wallis(x, wMean, wStd, ToMean, ToStd)
%
% Input Arguments 
%   x      : Image
%   wMean  : Weight on mean
%   wStd   : Weight on std (0=no std processing)
%   ToMean : Mean value
%   ToStd  : Standard deviation 
%
% Output Arguments 
%   y : Enhanced image
%
% Remarks : This function must be associated to nlfilter
%
% Examples
%   nomFicIn = getNomFicDatabase('TexturesSonarMontage01.tif');
%   [x, map] = imread(nomFicIn);
%   x = double(x);
%   figure; imagesc(x); colormap(gray(256)); colorbarHist;
%   wMean = 0.9;
%   wStd = 1 ;
%   y = nlfilter(x, [5 5], 'wallis', wMean, wStd, 128, 76);
%   figure; imagesc(y); colormap(gray(256)); colorbarHist;
%
% See also wallisaugustin wallisMoyenne nlfilter nlfilterFic Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y = wallis(x, wMean, wStd, ToMean, ToStd)

if ~isa(x, 'double')
  x = double(x) ;
end

[n,m] = size(x) ;
ctr = round(n*m / 2) ;
x0 = x(ctr);

if isnan(x0)
	y = NaN;
	return
end

extr = ~isnan(x);
x = x(extr);

% Moyenne
moy = mean(x) ;

y = wMean * ToMean + (1-wMean) * moy ;

if wStd ~= 0
  % Ecart type
  ect = std(x) ;
  y = y + ( (wStd * ToStd) / (wStd*ect + ToStd ) ) * (x0 - moy) ;
end


