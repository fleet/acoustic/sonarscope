% Filtre de wallis approximatif (cf. M.-C. Mouchot - ENSTB)
%
% Syntax
%   y = wallisMoyenne(x, alpha, moy_ref)
%
% Input Arguments
%   x     : Image a traiter
%   alpha : Ponderateur sur le role de la moyenne
%   beta  : Ponderateur sur le role de l'ecart-type
%   NB    : beta = 0 ==> pas de prise en compte de l'ect
%
% Output Arguments
%   x     : Image filtree
%
% EXAMPLE
%   nomFicIn = getNomFicDatabase('COSMOS_BLANES.bmp');
%   [x, map] = ASimread(nomFicIn, 'Lig', 3400:2:4400, 'Col', 3400:2:4400);
%   x = double(x); x(find(x == 127)) = NaN;
%   imagesc(x); colorbarHist;
%   alpha = 0.8 ;
%   y = nlfilter(x, [5 5], 'wallisMoyenne', alpha, 128);
%   figure; imagesc(y); colormap(gray(256)); set(gca,'CLim', [95 130]); colorbarHist;
%
% See also wallis nlfilterFic Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y = wallisMoyenne(x, alpha, moy_ref)

[n,m] = size(x) ;
ctr = round(n*m / 2) ;
x0 = x(ctr);

if isnan(x0)
    y = NaN;
    return
end

extr = ~isnan(x);
x = x(extr);

% Moyenne
moy = mean(x) ;
y = x0 + alpha * (moy_ref - moy) ;
