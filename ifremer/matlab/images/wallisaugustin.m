% Filtre de wallis approximatif (cf. M.-C. Mouchot - ENSTB)
%
% Syntax
%   y = wallisaugustin(x, alpha, beta, moy_ref, ect_ref)
%
% Input Arguments 
%   x     : Image a traiter
%   alpha : Ponderateur sur le role de la moyenne
%   beta  : Ponderateur sur le role de l'ecart-type
%           beta = 0 ==> pas de prise en compte de l'ect
%
% Output Arguments 
%   x     : Image filtree
%
% EXAMPLE
%   nomFicIn = getNomFicDatabase('COSMOS_BLANES.bmp');
%   [x, map] = ASimread(nomFicIn, 'Lig', 3400:2:4400, 'Col', 3400:2:4400);
%   x = double(x); x(find(x == 127)) = NaN;
%   imagesc(x); colorbarHist;
%
%   y = nlfilter(x, [5 5], 'wallisaugustin', 0.8, 0, 128, 50);
%   figure; imagesc(y); colormap(gray(256)); colorbarHist;
%
%   y = nlfilter(x, [5 5], 'wallisaugustin', 0.8, 0.5, 128, 50);
%   figure; imagesc(y); colormap(gray(256)); colorbarHist;
%
%   y = nlfilter(x, [5 5], 'wallisaugustin', 0.8, 1, 128, 50);
%   figure; imagesc(y); colormap(gray(256)); colorbarHist;
%
% See also wallis nlfilterFic Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function y = wallisaugustin(x, alpha, beta, moy_ref, ect_ref)

if ~isa(x, 'double')
    x = double(x);
end

[n,m] = size(x);
ctr = round(n*m / 2);
x0 = x(ctr);

if isnan(x0)
    y = NaN;
    return
end

extr = ~isnan(x);
x = x(extr);

% Moyenne
moy = mean(x);

if beta == 0
    y = x0 + alpha * (moy_ref - moy);
else
    % Ecart type
    ect = std(x);
    
    y = x0 + alpha * (moy_ref - moy) + beta * (ect_ref / ect) * (x0 - moy);
end



