% Elargissement d'une image par duplication de pixels pour pr�parer une operation de filtrage
% Les op�rations de filtrage agrandissent les images en mettant des zeros,
% cette fonction recopie les pixels les plus proches sur les bords.
% Si on passe ValNaN non vide, la fonction interpole les non-valeurs afin d'�viter la propagation des
% NaN dans l'op�ration de filtrage. Cette fonction ne doit �tre utilis�e
% que pour une utilisation par filter2 ; Cf cl_image/filterRectangle
%
% Syntax
%   J = windowEnlarge(I, win, ValNaN)
%
% INTPUT PARAMETERS :
%   I      : Image
%   win    : Hauteur x Largeur de la fenetre glissante
%   ValNaN : {[] | NaN | x} : Valeur representant les non-valeurs si on veut les remplacer
%            par la moyenne.
%
% Output Arguments
%   J : Image elargie et intertpolee
%
% Examples
%   [I, label] = ImageSonar(1);
%   figure; imagesc(I); colormap(gray(256)); colorbar;
%   J = windowEnlarge(I, [9 9], 1);
%   figure; imagesc(J); colormap(gray(256)); colorbar;
%   K = windowEnlargeOff(J, [9 9]);
%   figure; imagesc(K); colormap(gray(256)); colorbar;
%   whos I J K
%
% See also Authors windowEnlargeOff
% Authors : JMA
% ----------------------------------------------------------------------------

function [J, subNan] = windowEnlarge(I, win, ValNaN)

if isempty(ValNaN)
    subNan = [];
    ValNaN = NaN;
else
    if isnan(ValNaN)
        sub    = find(~isnan(I));
        subNan = find( isnan(I));
    else
        sub    = find(I ~= ValNaN);
        subNan = find(I == ValNaN);
    end

    if ~isempty(subNan)
        if isempty(sub)
            J = ones(size(I) + (win-1), 'single') * ValNaN;
            my_warndlg('images:windowEnlarge Aucun pixel dans l''image', 0);
            return
        end
    end
    I(subNan) = mean(I(sub));
end

if win(1) < 1
    win(1) = 1;
end
if win(2) < 1
    win(2) = 1;
end

J = ones(size(I) + (win-1), class(I)) * ValNaN;

suby = (1:size(I,1)) + floor(win(1)/2);
subx = (1:size(I,2)) + floor(win(2)/2);
J(suby,subx) = I;

N = floor(win(1) / 2);
for i=1:N
    J(i,:) = J(N+1,:);
    J(end-i+1,:) = J(end-N,:);
end
N = floor(win(2) / 2);
for i=1:N
    J(:,i) = J(:,N+1);
    J(:,end-i+1) = J(:,end-N);
end
