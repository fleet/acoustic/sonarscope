% Anulation de l'�largissement d'une image par duplication de pixels pour pr�parer une operation de filtrage
% Les op�rations de filtrage agrandissent les images en mettant des zeros,
% cette fonction recopie les pixels les plus proches sur les bords.
% Si on passe ValNaN non vide, la fonction interpole les non-valeurs afin d'�viter la propagation des
% NaN dans l'op�ration de filtrage. Cette fonction ne doit �tre utilis�e
% que pour une utilisation par filter2 ; Cf cl_image/filterRectangle
%
% Syntax
%   J = windowEnlargeOff(I, win, ValNaN)
%
% INTPUT PARAMETERS :
%   I      : Image
%   win    : Hauteur x Largeur de la fenetre glissante
%   ValNaN : {[] | NaN | x} : Valeur representant les non-valeurs si on veut les remplacer
%            par la moyenne.
%
% Output Arguments
%   J : Image elargie et intertpolee
%
% Examples
%   [I, label] = ImageSonar(1);
%   figure; imagesc(I); colormap(gray(256)); colorbar;
%   J = windowEnlarge(I, [9 9], 1);
%   figure; imagesc(J); colormap(gray(256)); colorbar;
%   K = windowEnlargeOff(J, [9 9]);
%   figure; imagesc(K); colormap(gray(256)); colorbar;
%   whos I J K
%
% See also Authors windowEnlarge
% Authors : JMA
% ----------------------------------------------------------------------------

function J = windowEnlargeOff(I, win)

if win(1) < 1
    win(1) = 1;
end
if win(2) < 1
    win(2) = 1;
end

nl = floor(win(1)/2);
nc = floor(win(2)/2);
% nl = win(1);
% nc = win(2);
J = I((nl+1):(end-nl), (nc+1):(end-nc));
