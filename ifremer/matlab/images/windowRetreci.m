% Retrecissement d'une image apres une operation de filtrage
%
% Syntax
%   J = windowRetreci(I, win)
%
% INTPUT PARAMETERS : 
%   I      : Image
%   win    : Hauteur x Largeur de la fenetre glissante
%
% Output Arguments 
%   J : Image retrecie
%
% Examples
%   [I, label] = ImageSonar(1);
%   figure; imagesc(I); colormap(gray(256)); colorbar;
%   J = windowRetreci(I, [9 9], 1);
%   figure; imagesc(J); colormap(gray(256)); colorbar;
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function I = windowRetreci(I, win)

if win(1) < 1
    win(1) = 1;
end
if win(2) < 1
    win(2) = 1;
end

suby = (1:(size(I,1)-win(1)+1)) + floor(win(1)/2);
subx = (1:(size(I,2)-win(2)+1)) + floor(win(2)/2);

I = I(suby,subx);
