% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_netcdf
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   fileName = getNomFicDatabase('example.nc');
%   a = cl_netcdf('fileName', fileName);
%   str = char(a)
%
% See also cl_netcdf Authors
% Authors : JMA
% VERSION  : $Id: char.m,v 1.2 2003/09/04 15:41:55 augustin Exp augustin $
% ----------------------------------------------------------------------------

function str = char(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i = 1:1:m
        for j = 1:1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);

else
    if (m*n) == 0
        str = '';
        return
    end

    % ---------------
    % Instance unique

    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
