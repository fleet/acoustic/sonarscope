% Writer for netcdf files from a structured generic stored data
%
% Syntax
%   a = cl_netcdf(...)
% 
% Name-Value Pair Arguments
%   fileName : File name
%   skeleton : Structure contenant la description du fichier
%
% Output Arguments 
%   a : instance of cl_netcdf
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%
% See also cl_netcdf/read cl_netcdf/write
% Authors : JMA Authors
% ----------------------------------------------------------------------------

function this = cl_netcdf(varargin)

%% Définition de la structure

this.fileName = '';
this.skeleton = [];

%% Creation de l'instance

this = class(this, 'cl_netcdf');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});

