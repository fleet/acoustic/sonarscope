% Liste des attributs globaux d'un fichier netcdf de Caraibes
%
% Syntax
%   disp_list_att(a, ...)
%
% Input Arguments
%   a : Instance de cl_netcdf
%
% Name-Value Pair Arguments
%   Level : Niveau de d�tail : (1, 2 ou 3, 1 par d�faut)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   disp_list_att(a, 'Level', 1)
%   disp_list_att(a, 'Level', 2)
%   disp_list_att(a  'Level', 3)
%
% See also disp_list_dim cdf_disp_list_var cdf_disp_list_att cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function disp_list_att(this, varargin)

cdf_disp_list_att(this.skeleton, varargin{:})