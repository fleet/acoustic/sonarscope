% Liste des dimensions d'un fichier netcdf de Caraibes
%
% Syntax
%   disp_list_dim(a, ...)
%
% Input Arguments
%   a : Instance de cl_netcdf
%
% Name-Value Pair Arguments
%   Level : Niveau de d�tail : (1, 2 ou 3, 1 par d�faut)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   list_dim(a, 'Level', 1)
%
% See also disp_list_att cdf_disp_list_var cdf_disp_list_dim cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function disp_list_dim(this, varargin)

cdf_disp_list_dim(this.skeleton, varargin{:})
