% Liste des variables d'un fichier netcdf de Caraibes
%
% Syntax
%   disp_list_var(a, ...)
%
% Input Arguments
%   a : Instance de cl_netcdf
%
% Name-Value Pair Arguments
%   Level    : Niveau de d�tail : (1, 2 ou 3, 1 par d�faut)
%   VarListe : Liste des variables (toutes par d�faut)
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex2.mnt')
%   a = cl_netcdf('fileName', nomFic);
%   disp_list_var(a, 'Level', 1)
%   disp_list_var(a, 'Level', 2)
%   disp_list_var(a, 'Level', 3)
%   disp_list_var(a, 'Level', 3, 'VarListe', 2)
%
% See also cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function disp_list_var(this, varargin)

cdf_disp_list_var(this.skeleton, varargin{:})
