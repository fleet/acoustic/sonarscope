% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments
%   this : instance of cl_netcdf
%
% Examples
%   fileName = getNomFicDatabase('example.nc');
%   a = cl_netcdf('fileName', fileName);
%   display(a)
%
% See also cl_netcdf
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

% --------------------------------------------
% Lecture et affichage des infos de l'instance

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
