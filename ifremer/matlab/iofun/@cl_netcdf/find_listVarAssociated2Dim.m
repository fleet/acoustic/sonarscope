% Liste des variables associ�es � une dimension
%
% Syntax
%   [listVarNum, listVarName] = find_listVarAssociated2Dim(a, dimName)
%
% Input Arguments
%   a       : Instance de cl_netcdf
%   dimName : Nom(s) de la dimension
%
% Output Arguments
%   listVarNum  : Liste des num�ros des variables associ�es � dimName
%   listVarName : Liste des noms des variables associ�es � dimName
%
% Examples
%   nomFic = getNomFicDatabase('')
%   a = cl_netcdf('fileName', nomFic);
%   [listVarNum, listVarName] = find_listVarAssociated2Dim(a, dimName)
%
% See also cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function [listVarNum, listVarName] = find_listVarAssociated2Dim(this, dimName)
[listVarNum, listVarName] = cdf_find_listVarAssociated2Dim(this.skeleton, dimName);
