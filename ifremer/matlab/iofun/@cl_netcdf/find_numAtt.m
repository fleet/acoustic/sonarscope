% Recherche du numero d'un attribut
%
% Syntax
%   numAtt = find_numAtt(a, nomAtt)
%
% Input Arguments 
%   a      : instance of cl_netcdf
%   nomAtt : Nom de la variable
%
% Output Arguments 
%   numAtt : Numero de la variable ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   numAtt = find_numAtt(a, 'grWrkImage')
%
% See also cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function numAtt = find_numAtt(this, nomAtt)

numAtt = [];
for i=1:length(this.skeleton.att)
    if strcmp(this.skeleton.att(i).name, nomAtt)
        numAtt = i;
        return
    end
end
