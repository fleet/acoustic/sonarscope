% Recherche du numero d'une dimension d'un fichiet netcdf
%
% Syntax
%   numDim = find_numDim(sk, nomDim)
%
% Input Arguments 
%   a      : instance of cl_netcdf
%   nomDim : Nom de la dimension
%
% Output Arguments 
%   numDim : Numero de la dimension ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   cdf_disp_list_dim(a, 'Level', 1)
%   numDim = find_numDim(a, 'grWrkImage_NbLin')
%
% See also cdf_find_numAtt cdf_find_numVar cdf_find_numVarAtt cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function [numDim, value] = find_numDim(this, nomDim)

numDim = [];
value  = [];
for i=1:length(this.skeleton.dim)
    if strcmp(this.skeleton.dim(i).name, nomDim)
        numDim = i;
        value = this.skeleton.dim(i).value;
        return
    end
end
