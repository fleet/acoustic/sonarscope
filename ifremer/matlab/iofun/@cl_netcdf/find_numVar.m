% Recherche du numero d'une variable
%
% Syntax
%   [numVar, nomVar] = find_numVar(a, nomVar)
%
% Input Arguments 
%   a      : instance of cl_netcdf
%   nomVar : Nom de la variable
%
% Output Arguments 
%   numVar    : Numero de la variable ([] si pas trouvee)
%   nomVarFic : Nom exact de la variable (respect des majuscules/minuscules)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   numVar = find_numVar(a, 'grWrkImage')
%
% See also cl_netcdf
% Authors : JMA
% ----------------------------------------------------------------------------

function [numVar, nomVarFic] = find_numVar(this, nomVar)

numVar = [];
nomVarFic = [];
for i=1:length(this.skeleton.var)
    if strcmp(this.skeleton.var(i).name, nomVar)
        numVar = i;
        nomVarFic = this.skeleton.var(i).name;
        return
    end
end
if isempty(numVar)
    for i=1:length(this.skeleton.var)
        if strcmpi(this.skeleton.var(i).name, nomVar)
            numVar = i;
            nomVarFic = this.skeleton.var(i).name;
            return
        end
    end
end