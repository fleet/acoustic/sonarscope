% Recherche du numero d'un attribut d'une variable
%
% Syntax
%   numVar = find_numVarAtt(a, numVar, nomAtt)
%
% Input Arguments 
%   a      : instance of cl_netcdf
%   numVar : Numero de la variable
%   nomAtt : Nom de l'attribut
%
% Output Arguments 
%   numAtt : Numero de l'atribut de la variable ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   numVar = find_numVar(a, 'grWrkImage')
%   numAtt = find_numVarAtt(a, numVar, 'add_offset')
%
% See also cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function numAtt = find_numVarAtt(this, numVar, nomAtt)

if ischar(numVar)
    numVar = find_numVar(this, numVar);
end

numAtt = [];
for i=1:length(this.skeleton.var(numVar).att)
    if strcmp(this.skeleton.var(numVar).att(i).name, nomAtt)
        numAtt = i;
        return
    end
end
