% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_netcdf
%
% Name-Value Pair Arguments
%   fileName : File name
%   skeleton : Structure contenant la description du fichier
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passees en argument
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   val = get_value(a, 1);
%   val = get_value(a, 'lat');
%
% See also cl_netcdf cl_netcdf/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% On v�rifie que l'instance est bien de demension 1

if length(this) ~= 1
    my_warndlg('cl_netcdf/get : L''instance doit etre de dimension 1', 1);
    return
end

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'fileName'
            varargout{end+1} = this.fileName; %#ok<AGROW>
        case 'skeleton'
            varargout{end+1} = this.skeleton; %#ok<AGROW>
        otherwise
            str = sprintf('cl_netcdf/get %s non pris en compte', varargin{k});
            my_warndlg(str, 1);
    end
end
