% Lecture d'une variable
%
% Syntax
%   value = get_CodedValue(a, ident, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Soit le numero de la variable, soit son nom
%
% Name-Value Pair Arguments
%   sub1 : Echantillonnage de la premiere dimension
%   sub2 : Echantillonnage de la deuxieme dimension
%   ...
%
% Output Arguments
%   value : Valeur du parametre ramene a l'unite
%
% Examples
%   fileName = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   fileName = getNomFicDatabase('EM300_Ex1.mbb')
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   val = get_CodedValue(a, 1)
%   val = get_CodedValue(a, 'lat')
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function value = get_CodedValue(this, ident, varargin)

[varargin, sub1] = getPropertyValue(varargin, 'sub1', []);
[varargin, sub2] = getPropertyValue(varargin, 'sub2', []);
[varargin, sub3] = getPropertyValue(varargin, 'sub3', []);
[varargin, sub4] = getPropertyValue(varargin, 'sub4', []);
[varargin, sub5] = getPropertyValue(varargin, 'sub5', []);
[varargin, sub6] = getPropertyValue(varargin, 'sub6', []);
[varargin, sub7] = getPropertyValue(varargin, 'sub7', []);
[varargin, sub8] = getPropertyValue(varargin, 'sub8', []); %#ok<ASGLU>

% ---------
% Open file

if exist(this.fileName, 'file')
    idFile = mexCD_OpenFile(this.fileName, 'ReadOnly');
    if isempty(idFile)
        value = [];
        return;
    end
else
    value = [];
    return
end

if ischar(ident)
    for i = 1:mexCD_NbVar(idFile)
        varName  = mexCD_NameVar(idFile, i);
        if strcmp(varName, ident)
            ident = i;
            break
        end
    end
elseif isnumeric(ident)
else
    value = [];
    return
end

[Var.name type nDimVar dimVar Var.value]  = mexCD_GetVar(idFile, ident); %#ok<NCOMMA>
%GLUGLUGLU : transposition des cha�nes de caract�res !!!
sz = size(Var.value);
if all(sz ~= 1) && strcmp(type, 'char')
    Var.value = my_transpose(Var.value);
end

% ------------------
% Calcul des indices

if isempty(sub1)
    if length(this.skeleton.var(ident).dim) >= 1
        nomDim = this.skeleton.var(ident).dim(1).name;
        sub1 = 1:get_valDim(this, nomDim);
    else
        sub1 = 1;
    end
end

if isempty(sub2)
    if length(this.skeleton.var(ident).dim) >= 2
        nomDim = this.skeleton.var(ident).dim(2).name;
        sub2 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub3)
    if length(this.skeleton.var(ident).dim) >= 3
        nomDim = this.skeleton.var(ident).dim(3).name;
        sub3 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub4)
    if length(this.skeleton.var(ident).dim) >= 4
        nomDim = this.skeleton.var(ident).dim(4).name;
        sub4 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub5)
    if length(this.skeleton.var(ident).dim) >= 5
        nomDim = this.skeleton.var(ident).dim(5).name;
        sub5 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub6)
    if length(this.skeleton.var(ident).dim) >= 6
        nomDim = this.skeleton.var(ident).dim(6).name;
        sub6 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub7)
    if length(this.skeleton.var(ident).dim) >= 7
        nomDim = this.skeleton.var(ident).dim(7).name;
        sub7 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub8)
    if length(this.skeleton.var(ident).dim) >= 8
        nomDim = this.skeleton.var(ident).dim(8).name;
        sub8 = 1:get_valDim(this, nomDim);
    end
end


% ---------------------------------
% Lecture de la valeur du parametre

if isempty(sub2) && isempty(sub3) && isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = Var.value(sub1);
elseif isempty(sub3) && isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = Var.value(sub1,sub2);
elseif isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = Var.value(sub1,sub2,sub3);
elseif isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = Var.value(sub1,sub2,sub3,sub4);
elseif isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = Var.value(sub1,sub2,sub3,sub4,sub5);
elseif isempty(sub7) && isempty(sub8)
    value = Var.value(sub1,sub2,sub3,sub4,sub5,sub6);
elseif isempty(sub8)
    value = Var.value(sub1,sub2,sub3,sub4,sub5,sub6,sub7);
else
    value = Var.value(sub1,sub2,sub3,sub4,sub5,sub6,sub7,sub8);
end

% --------------------
% Fermeture du fichier

mexCD_CloseFile(idFile);
