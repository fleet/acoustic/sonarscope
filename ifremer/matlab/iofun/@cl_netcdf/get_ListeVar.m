% Lecture des dimensions d'une variable
%
% Syntax
%   ListeVar = get_ListeVar(this)
%
% Input Arguments
%   a : Une instance de la classe cl_netcdf

%
% Output Arguments
%   ListeVar : Liste des variables
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   ListeVar = get_ListeVar(a)
%
% See also cl_netcdf Authors
% Authors : JMA
% VERSION  : $Id: get.m,v 1.3 2003/09/02 09:59:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

function ListeVar = get_ListeVar(this)

for i=1:length(this.skeleton.var)
    ListeVar{i} = this.skeleton.var(i).name; %#ok<AGROW>
end
