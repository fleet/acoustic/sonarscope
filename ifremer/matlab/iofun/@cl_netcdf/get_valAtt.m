% Lecture de la valeur d'un attribut
%
% Syntax
%   value = get_valAtt(a, ident, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Numero ou nom de l'attibut
%
% Output Arguments
%   value : Valeur de l'attribut
%
% Examples
%   fileName = getNomFicDatabase('REFCar_EM1000.mbg')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   val = get_valAtt(a, 47)
%   val = get_valAtt(a, 'mbCycleCounter')
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function value = get_valAtt(this, ident)

if length(this) ~= 1
    my_warndlg('cl_netcdf/get : L''instance doit etre de dimension 1', 1);
    return
end

if isnumeric(ident)
    value = this.skeleton.att(ident).value;
elseif ischar(ident)
    for j=1:length(this.skeleton.att)
        attName = this.skeleton.att(j).name;
        if strcmp(attName, ident)
            value = this.skeleton.att(j).value;
            return
        end
    end
    value = [];
else
    value = [];
end
