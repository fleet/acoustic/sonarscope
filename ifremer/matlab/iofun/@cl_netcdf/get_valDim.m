% Lecture de la valeur d'une dimension
%
% Syntax
%   value = get_valDim(a, ident, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Numero ou nom d'une dimension
%
% Output Arguments
%   value : Valeur du parametre ramene a l'unite
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   val = get_valDim(a, 1)
%   val = get_valDim(a, 'lat')
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function value = get_valDim(this, ident)

if isnumeric(ident)
    value = this.skeleton.dim(ident).value;
elseif ischar(ident)
    for k=1:length(this.skeleton.dim)
        dimName = this.skeleton.dim(k).name;
        if strcmp(dimName, ident)
            value = this.skeleton.dim(k).value;
            return
        end
    end
else
    value = [];
end
