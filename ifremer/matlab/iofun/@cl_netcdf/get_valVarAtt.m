% Lecture de la valeur d'une dimension
%
% Syntax
%   value = get_valVarAtt(a, ident, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Numero ou nom d'une dimension
%
% Output Arguments
%   value : Valeur du parametre ramene a l'unite
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   cdf_disp_list_var(sk, 'Level', 1)
%   valVarAtt = get_valVarAtt(a, 81, 'Commentaire')
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA
% VERSION  : $Id: get.m,v 1.3 2003/09/02 09:59:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

function valVarAtt = get_valVarAtt(this, numVar, nomAtt, varargin)

% -------------------------------------------------
% On verifie que l'instance est bien de demension 1

if length(this) ~= 1
    my_warndlg('cl_netcdf/get : L''instance doit etre de dimension 1', 1);
    return
end

if ischar(numVar)
    numVar = find_numVar(this, numVar);
end
valVarAtt = cdf_get_valVarAtt(this.skeleton, numVar, nomAtt, varargin{:});

