% Lecture d'une variable
%
% Syntax
%   value = get_value(a, ident, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Soit le numero de la variable, soit son nom
%
% Name-Value Pair Arguments
%   sub1 : Echantillonnage de la premiere dimension
%   sub2 : Echantillonnage de la deuxieme dimension
%   ...
%
% Output Arguments
%   value : Valeur du parametre ramene a l'unite
%
% Examples
%   fileName = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   fileName = getNomFicDatabase('EM300_Ex1.mbb')
%   fileName = getNomFicDatabase('RefCar_Em300.mbb')
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   val = get_value(a, 1)
%   val = get_value(a, 'lat')
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function value = get_value(this, ident, varargin)

VarType = get_varType(this, ident);
if isempty(VarType)
    value = [];
    return
end

% if ~strcmp(VarType,'ncbyte')
%     try
%         value = get_value_java(this, ident, varargin{:});
%         return
%     catch
%         disp('Netcdf Java reading has failed we try to do it with another way')
%     end
% end

[varargin, Convert]            = getPropertyValue(varargin, 'Convert', 'single');
[varargin, MinMaxvalue]        = getPropertyValue(varargin, 'MinMaxvalue', []);
[varargin, CheckCIB_BLOCK_DIM] = getPropertyValue(varargin, 'CheckCIB_BLOCK_DIM', 1);
[varargin, sub1]               = getPropertyValue(varargin, 'sub1', []);
[varargin, sub2]               = getPropertyValue(varargin, 'sub2', []);
[varargin, sub3]               = getPropertyValue(varargin, 'sub3', []);
[varargin, sub4]               = getPropertyValue(varargin, 'sub4', []);
[varargin, sub5]               = getPropertyValue(varargin, 'sub5', []);
[varargin, sub6]               = getPropertyValue(varargin, 'sub6', []);
[varargin, sub7]               = getPropertyValue(varargin, 'sub7', []);
[varargin, sub8]               = getPropertyValue(varargin, 'sub8', []);

[varargin, CharIsByte] = getFlag(varargin, 'CharIsByte');
[varargin, noMessage]  = getFlag(varargin, 'noMessage');

if ~isempty(varargin)
    my_warndlg('cl_netcdf/get_value : Input argument not interpreted', 1);
end

%% Open file

if exist(this.fileName, 'file')
    idFile = netcdf.open(this.fileName, 'NC_NOWRITE');
    if isempty(idFile)
        value = [];
        return;
    end
else
    value = [];
    return
end

if ischar(ident)
    identVar = [];
    for k1=1:length(this.skeleton.var)
        varName = this.skeleton.var(k1).name;
        if strcmp(varName, ident)
            identVar = k1;
            break
        end
    end
    if isempty(identVar)
        for k1=1:length(this.skeleton.var)
            varName = this.skeleton.var(k1).name;
            if strcmpi(varName, ident)
                identVar = k1;
                break
            end
        end
    end
    if isempty(identVar)
        if ~noMessage
            str1 = ['Variable ''' ident ''' inexistante.'];
            str2 = ['Variable ''' ident ''' does not exist.'];
            my_warndlg(Lang(str1,str2), 1);
            value = [];
            return
        end
    end
    ident = identVar;
elseif isnumeric(ident)
else
    value = [];
    return
end

if ischar(ident) || isempty(ident)
    if ~noMessage
        str1 = ['Variable ''' ident ''' inexistante'];
        str2 = ['Variable ''' ident ''' does not exist'];
        my_warndlg(Lang(str1,str2), 1);
    end
    value = [];
    return
end

% Recherche de la dimension CIB_BLOCK_DIM dans la variable
if CheckCIB_BLOCK_DIM
    if length(this.skeleton.var(ident).dim) >= 1
        for k1=1:length(this.skeleton.var(ident).dim)
            nomDim = this.skeleton.var(ident).dim(k1).name;
            if strcmp(nomDim, 'CIB_BLOCK_DIM')
                str1 = 'Arr�t du traitement. La variable dispose de la dimension CIB_BLOCK_DIM qui n''est pas trait�e dans SonarScope';
                str2 = 'Process Stop. The variable has the dimension CIB_BLOCK_DIM which is not processed in SonarScope.';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'CIB_BLOCK_DIM');
                % Fermeture du fichier
                netcdf.close(idFile);
                value = [];
                return
            end
        end
    end
end

% % Appel de la lecture des donn�es par les m�thodes CIB_DOU (dll)
% Var.value = netcdf.getVar(idFile, ident-1);
% % pppp=Var.value;
% % whos pppp
Var.type  = this.skeleton.var(ident).type;
Var.name  = this.skeleton.var(ident).name;

%% Calcul des indices

if isempty(sub1)
    if length(this.skeleton.var(ident).dim) >= 1
        nomDim = this.skeleton.var(ident).dim(1).name;
        sub1 = 1:get_valDim(this, nomDim);
    else
        sub1 = 1;
    end
end

if isempty(sub2)
    if length(this.skeleton.var(ident).dim) >= 2
        nomDim = this.skeleton.var(ident).dim(2).name;
        sub2 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub3)
    if length(this.skeleton.var(ident).dim) >= 3
        nomDim = this.skeleton.var(ident).dim(3).name;
        sub3 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub4)
    if length(this.skeleton.var(ident).dim) >= 4
        nomDim = this.skeleton.var(ident).dim(4).name;
        sub4 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub5)
    if length(this.skeleton.var(ident).dim) >= 5
        nomDim = this.skeleton.var(ident).dim(5).name;
        sub5 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub6)
    if length(this.skeleton.var(ident).dim) >= 6
        nomDim = this.skeleton.var(ident).dim(6).name;
        sub6 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub7)
    if length(this.skeleton.var(ident).dim) >= 7
        nomDim = this.skeleton.var(ident).dim(7).name;
        sub7 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub8)
    if length(this.skeleton.var(ident).dim) >= 8
        nomDim = this.skeleton.var(ident).dim(8).name;
        sub8 = 1:get_valDim(this, nomDim);
    end
end

%% Lecture de la valeur du param�tre
% NB : les traitements pour les donn�es � plus de 2 dimensions ne sont pas valid�s.

if isempty(sub2) && isempty(sub3) && isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = readValue(idFile, ident, sub1);
elseif isempty(sub3) && isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = readValue(idFile, ident, sub1, sub2);
    value = value';
elseif isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = readValue(idFile, ident, sub1, sub2, sub3);
elseif isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = readValue(idFile, ident, sub1, sub2, sub3, sub4);
    value = squeeze(value);
    value = my_transpose(value);
elseif isempty(sub6) && isempty(sub7) && isempty(sub8)
    value = readValue(idFile, ident, sub1, sub2, sub3, sub4, sub5);
    value = squeeze(value);
    value = my_transpose(value);
elseif isempty(sub7) && isempty(sub8)
    value = readValue(idFile, ident, sub1, sub2, sub3, sub4, sub5, sub6);
    value = squeeze(value);
    value = my_transpose(value);
elseif isempty(sub8)
    value = readValue(idFile, ident, sub1, sub2, sub3, sub4, sub5, sub6, sub7);
    value = squeeze(value);
    value = my_transpose(value);
else
    value = readValue(idFile, ident, sub1, sub2, sub3, sub4, sub5, sub6, sub7, sub8);
    value = squeeze(value);
    value = my_transpose(value);
end
Var.value = [];
clear sub1 sub2

% ---------------------------
% Transformation dans l'unite

add_offset    = [];
scale_factor  = [];
missing_value = [];
maximum       = [];

for k2=1:length(this.skeleton.var(ident).att)
    s.varIdent.att(k2).name  = this.skeleton.var(ident).att(k2).name;
    s.varIdent.att(k2).value = this.skeleton.var(ident).att(k2).value;
    
    if strcmp((s.varIdent.att(k2).name), 'add_offset')
        add_offset = s.varIdent.att(k2).value(:);
    end
    
    if strcmp((s.varIdent.att(k2).name), 'scale_factor')
        scale_factor = s.varIdent.att(k2).value(:);
    end
    
    if strcmp((s.varIdent.att(k2).name), 'missing_value')
        missing_value = s.varIdent.att(k2).value(:);
    end
    
    if strcmp((s.varIdent.att(k2).name), 'maximum')
        maximum = double(s.varIdent.att(k2).value(:));
    end
end

CharIsByteAuto = ~isempty(maximum);
CharIsByte = CharIsByte || CharIsByteAuto;

if (strcmp(Var.type, 'NC_CHAR') || strcmp(Var.type, 'NC_BYTE')) && CharIsByte
    value = uint16(value);
end

if ~isempty(missing_value)
    if strcmp(Convert, 'double')
        if ~isa(value, 'double')
            value = double(value);
        end
    else
        if ~isa(value, 'single')
            % Transcodage des valeurs type Char en unicode pour �viter les
            % d�bordements (d�codage sup�rieur � 255).
            if ischar(value)
                sz = size(value);
                value = unicode2native(value(:));
                value = reshape(value,sz);
            end
            value = single(value);
        end
    end
    if ischar(value)
        value = single(value);
    end
    subNaN = (value == missing_value);
    value(subNaN) = NaN;
end

if ~isempty(maximum)
    if (strcmp(Var.type, 'NC_CHAR') || strcmp(Var.type, 'NC_BYTE')) && CharIsByte
        sub = (value > 127);
        if maximum > 127
            value(sub) = value(sub) + (maximum - 2^16);
        else
%           value(sub) = value(sub) + (maximum - 128 - 2^16); % C'est a peu pr�s �a
            value(sub) = value(sub) + (maximum - 127 - 2^16); % Modif JMA le 06/02/2019
        end
    end
end

% ----------------------------------------------------
% Verrue CARAIBES, merci Marie-Paule pour l'explication

valid_maximum = cdf_get_valVarAtt(this.skeleton, ident, 'valid_maximum', 'noMessage');
if isempty(valid_maximum) && ~isempty(MinMaxvalue)
    valid_maximum = MinMaxvalue(2);
end
if ~isempty(valid_maximum)
    switch this.skeleton.var(ident).type
        case 'NC_SHORT'
            if valid_maximum > (2^15 - 1)
                sub = (value < 0);
                value(sub) = (2^16 + value(sub));
            end
        case 'NC_LONG'
            if valid_maximum > (2^31 - 1)
                sub = (value < 0);
                value(sub) = (2^32 + value(sub));
            end
        case 'NC_BYTE'
            %             value = value - 2^16;
            %             if valid_maximum > (2^7 - 1)
            %                 sub = (value < 0);
            %                 value(sub) = (2^8 + value(sub));
            %             end
        case 'NC_CHAR'
            %             disp('Pour JMA : 154756248')
            %             % IL FAUT TESTER SI CE CAS RESOUDRAIT TOUTES LES BIDOUILLES QUE
            %             % NOUS AVONS DU FAIRE POUR DECODER LES CHAR QUI SONT EN FAIT
            %             % DES BYTES / ON PEUT PEUT-ETRE SUPPRIMER LES "CarIsByte"
            if valid_maximum > (2^7 - 1)
                sub = (value < 0);
                value = single(value);
                value(sub) = uint8((2^8 + value(sub)));
            end
        otherwise
            %             valid_maximum;
    end
end






% % CARAIBES : X  = get_value(a_ref, nomVar); ne fonctionne pas
% % correctement : missing_value est bien trouv� dans les attributs
% % de la variable mais la valeur correspondant aux non-valeurs est
% % en r�alit� la valeur 0.
% % C:\IfremerToolboxDataPublicSonar\Level1\REFCar_EM300.mbg
if exist('varName', 'var') && strcmp(varName, 'mbDepth')
    if ~isempty(missing_value)
        if strcmp(Convert, 'double')
            if ~isa(value, 'double')
                value = double(value);
            end
        else
            if ~isa(value, 'single')
                value = single(value);
            end
        end
        if ischar(value)
            value = single(value);
        end
        subNaN = (value == 0);
        value(subNaN) = NaN;
    end
end
% % CARAIBES

if ~isempty(scale_factor)
    if strcmp(Convert, 'double')
        if ~isa(value, 'double')
            value = double(value);
        end
    else
        if ~isa(value, 'single')
            value = single(value);
        end
    end
    if scale_factor ~= 1
        value = value * double(scale_factor);
    end
end
if ~isempty(add_offset)
    if strcmp(Convert, 'double')
        if ~isa(value, 'double')
            value = double(value);
        end
    else
        if ~isa(value, 'single')
            value = single(value);
        end
    end
    if add_offset ~= 0
        value = value + double(add_offset);
    end
end

% Traitement des chaines de type Char � traiter comme des Char
% contrairement � des variables comme mbSQquality par ex. de CARAIBES.
if strcmp(Var.type, 'char') || strcmp(Var.type, 'NC_BYTE')
    if ~CharIsByte
        sz = size(value);
        value = native2unicode(value(:));
        value = reshape(value,sz);
        value = char(value);
    end
end

if isa(value, 'char') && ~strcmp(Var.type, 'byte')      % Cha�nes vraiment de type char.
    sz = size(value);
    if ((length(sz) == 2) &&((sz(1) == 1) || (sz(2) == 1)))
        value = my_transpose(value);
    end
else
    % Typage obligatoire (Nan est tjs single ou double)
    % Certaines donn�es ne disposent pas des attributs add_offset et
    % missing_value (voir plut�t A_RESOL et B_RESOL, ...).
    if strcmp(Convert, 'double')
        if ~isa(value, 'double')
            value = double(value);
        end
    else
        if ~isa(value, 'single')
            value = single(value);
        end
    end
    
    sz = size(value);
    if ((length(sz) == 2) &&((sz(1) == 1) || (sz(2) == 1)))
        value = my_transpose(value);
    end
end

%% Fermeture du fichier

netcdf.close(idFile);



function value = readValue(idFile, ident, varargin)

N = length(varargin);

if N == 0
    value = netcdf.getVar(idFile, ident-1);
    return
end

if N >= 1
    sub1 = varargin{1};
    Start(1)  = sub1(1);
    Count(1)  = length(sub1);
    if length(sub1) == 1
        Stride(1) = 1;
    else
        Stride(1) = sub1(2) - sub1(1);
    end
end

if N >= 2
    sub2 = varargin{2};
    Start(2)  = sub2(1);
    Count(2)  = length(sub2);
    if length(sub2) == 1
        Stride(2) = 1;
    else
        Stride(2) = sub2(2) - sub2(1);
    end
end

if N >= 3
    sub3 = varargin{3};
    Start(3)  = sub3(1);
    Count(3)  = length(sub3);
    if length(sub3) == 1
        Stride(3) = 1;
    else
        Stride(3) = sub3(2) - sub3(1);
    end
end

if N >= 4
    sub4 = varargin{4};
    Start(4)  = sub4(1);
    Count(4)  = length(sub4);
    if length(sub4) == 1
        Stride(4) = 1;
    else
        Stride(4) = sub4(2) - sub4(1);
    end
end

if N >= 5
    my_warndlg(Lang('Cas pas encore trait� dans cl_netcdf/readValue', 'TODO'), 1);
    value = [];
    return
end

[varname, xtype, dimids] = netcdf.inqVar(idFile, ident-1); %#ok<ASGLU>
if isempty(dimids)
    SizeVar = Count;
else
    for k1=1:length(dimids)
        [dimname, dimlen] = netcdf.inqDim(idFile, dimids(k1)); %#ok<ASGLU>
        SizeVar(k1) = dimlen; %#ok<AGROW>
    end
end

if isequal(Start, 1) && isequal(Stride, 1) && isequal(Count, 1) && isequal(Count, SizeVar)
    value = netcdf.getVar(idFile, ident-1);
else
    value = netcdf.getVar(idFile, ident-1, Start-1, Count, Stride);
end
